Napoleon and Samantha
{{Infobox film
| name           = Napoleon and Samantha
| image          = Napoleon_and_Samantha film.jpg
| image_size     =
| caption        = Napoleon and Samantha Theatrical Release Poster
| director       = Bernard McEveety
| producer       = Winston Hibler
| writer         = Stewart Raffill
| narrator       =
| starring       = Michael Douglas  Jodie Foster Johnny Whitaker Buddy Baker
| cinematography = Monroe P. Askins
| editing        = Robert Stafford Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 92 min.
| country        = United States
| language       = English
| budget         =
}} adventure drama film directed by Bernard McEveety and written by Stewart Raffill. Filmed in and around John Day, Oregon, it stars Michael Douglas, Johnny Whitaker and Jodie Foster.

==Plot==
Eleven-year-old Napoleon (Johnny Whitaker) lives with his grandfather (Will Geer). He and his grandfather adopt a lion named Major when by chance they meet an old clown who cannot take him back to Europe. The old lion has bad teeth and only drinks milk so they put Major in the chicken cage to look after him. When Napoleons grandfather dies of old age, Napoleon asks a young grad student named Danny (Michael Douglas) to help bury his grandfather. Uncertain about his future Napoleon runs off with the lion, a pet rooster, and his friend Samantha (Jodie Foster) to try to find Danny, now a goat herder who lives in the mountains, and so Napoleon can avoid being sent to an orphanage.
 bear that psychopath and escapes to rescue them. He steals a motorcycle and the police chase him all the way back to his cabin where they find and arrest the wanted man. When things are back to normal, Napoleon takes Major and tries to run away again to live with the Indians but Danny catches up. Danny explains that the Indians dont really live out in the wild anymore and that Napoleon should give foster care a try with a promise that Major could stay in the mountains and live with him. Napoleon agrees and they go back to Dannys cabin.
 Mike Henry television series MGM logos Leo the Lion since 1957.

==Cast==
*Michael Douglas as Danny
*Johnny Whitaker as Napoleon Wilson
*Jodie Foster as Samantha
*Will Geer as Grandpa
*Major Leo McTavish as Major the Lion 
*Arch Johnson as Chief of Police Henry Jones as Mr. Gutteridge
*Vito Scotti as Clown Dimetri John Crawford as Desk Sergeant
*Mary Wickes as Clara
*Ellen Corby as Gertrude
*Rex Holman as Mark
*Claude Johnson as Gary
*John Lupton as Pete James MacDonald as the Bear

==Mauling incident==
Foster was mauled by a substitute lion used in the movie, named Zambo, on the set and still has scars on her back and stomach. "I was walking ahead of him. He was on an invisible leash, some piano wire. He got sick of me being slow, picked me up and held me sideways and shook me like a doll."  "I was in shock and thought it was an earthquake. I turned around and saw the entire crew running off in the other direction. The trainer then said, Drop it and he opened his mouth and dropped me."   Retrieved 2 April 2008  The incident left her with a lifelong fear of cats. 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 