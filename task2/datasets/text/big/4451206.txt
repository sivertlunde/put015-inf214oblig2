The Hillz
{{Infobox Film
| name           = The Hillz
| image          = Thehillz.jpg
| image_size     = 
| caption        = Promotional poster
| director       = Saran Barnun
| producer       = Saran Barnun
| writer         = Saran Barnun	
| narrator       = 
| starring       = Rene Heger, Jesse Woodrow, Paris Hilton, Jason Shaw, Eric Priestley, James DeBello, Vince Rimoldi Melissa Smith
| music          = Peter Karr
| cinematography = Alice Brooks
| editing        = Don Adams, Harry James Picardi	
| distributor    = 
| released       = 2004
| runtime        = 91 min
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Hillz is a 2004 film written and directed by Saran Barnun and starring Rene Heger, Jesse Woodrow, and Paris Hilton. It centers around four kids from Beverly Hills who form a ruthless gang. The entire film features Steves (Woodrow) feeble attempts to obtain Heather Smith (Hilton) as his girlfriend, and a ruthless rebel child with some clear psychological disorders named Duff and his friend T trying to earn respect in their gang.

==Plot==

The film follows the experiences of a promising athlete named Steve and his friends who live in a small southern California town. Steves three friends, Duff, Seb, and T, all come from wealthy families and spend most of their time getting high, drinking at parties, and harassing Ahkmed, a convenience store clerk. After finding a gun in a girls house, Duff starts violently pursuing a life of crime with T, the loudmouthed instigator with a Napoleon complex, at his side.  

After Duff kills a kid who volunteers at the police department, the story fades away and we come back to find Steve returning home from college a year later just after being successful in college baseball.  Steve came back to get his dream girl, Heather Smith, who told him that if he made it as a baseball player she would consider dating him.  Steve runs into Duffs gang. Things spiral out of control for Steve as we learn that Duff and T have become submerged in what they think is one of the hardest gangs ever.  In fact, shortly after Steve joins up with Duff and T, he witnesses them kill a man in broad daylight in the middle of the street over $80. 

As the story unfolds, it is clear that Heather is being used and abused by her new boyfriend Todd, whom she is reluctant to leave for Steve. Later Steve convinces Seb, who broke his ties with Duffs crew, to come out and party with Duff and the old gang for a night. T insults two Korean men by calling them Gooks, who then fire an Uzi into Duffs car, killing Seb. Duff and T were later sold out to a crooked undercover cop and the older brother of the man who was previously shot over $80. They killed T and then a narcotics officer, and his team busted the door down and were all killed. 

The end of the movie has Steve and Duff driving along the street when a couple of young kids throw something at Duffs car causing the two get to out of the car and chase after them.  As they get near the kids, one of them turns, pulls a gun, and shoots Duff and Steve, killing them both.

==External links==
*  
*  

 
 
 
 