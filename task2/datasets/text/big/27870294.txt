Land of No Return
{{Infobox film
| name           = Land of No Return
| image          = 
| image_size     = 
| caption        = 
| director       = Kent Bateman
| producer       = Kent Bateman
| writer         = Kent Bateman
| narrator       = Frank Ray Perilli
| starring       = Mel Torme William Shatner Donald Moffat
| music          = Ralph Geddes
| cinematography = João Fernandes
| editing        = 
| distributor    = The International Picture Show Company
| released       = 1978
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 thriller film Jason and Justine Bateman. The film stars Mel Torme and William Shatner.

The film was shot in Utah and released theatrically by The International Picture Show Company, whose president at the time was legendary B-movie filmmaker Bill Rebane.

Alternate titles for the film include Challenge to Survive and Snowman.

== Plot == plane in Denver for the flight to Burbank, but en route at night over the Utah wastelands they encounter a sudden blizzard. When Zaks radio and engine fail, he guides the craft down to a crash landing. All three passengers survive, but the plane is destroyed and a struggle for survival begins.

==External links==
* 

Not to be confused with the 2016 feature film which also has a plane crash in it titled: The Land of No Return
== Plot ==


Greed and betrayal are Derek Donaldsons forte. In an effort to take over as majority stockholder of the company he and his sister share, Derek wants his sister dead. As an animal rights activist, Katherine has planned a trip to an animal refuge not far from the most treacherous jungle terrain on the continent. The jungle is called Biame-po-tucco, translating to The Land Of No Return. Derek enlists his henchmen, Kurt Ludlow and Jake Bell to make sure Katherines plane crashes and no one, including the FAA will know where. When his plans are foiled, Derek hires world-renowned jungle guide Austin Saunders to lead a search party to the plane. Unaware that anyone survived the plane crash Saunders is duped into taking the job under false pretense and an offer he cant refuse. Gathering up his party of packers Saunders has Ludlow and Bell in tow to find the plane. When Saunders and his party reach the wreckage, it is obvious Saunders has been deceived. The script takes a new direction as Saunders and Katherine make a dangerous escape through the jungle. All of Saunders skills are put to the test.
- Written by Nancy Savage 
Executive Producer Rod Harrell

 

 
 
 
 
 


 