Jai Lalitha
{{Infobox film name           = Jai Lalitha image          =  director       = Pon Kumaran producer       = Indira Arun Kumar Manjula Shankar writer         = Udayakrishna-Siby K. Thomas based on       = Mayamohini starring  Sharan Disha Pandey Aishwarya Devan music          = Sridhar V. Sambhram cinematography = Karunakar  editing        =  distributor    = Skyline Productions released       =   runtime        =  country        = India  language       = Kannada 
}}
 2014 Kannada Kannada comedy Malayalam film Dileep in the lead role and directed by Jose Thomas. Jai Lalitha released on June 27, 2014.

Previously titled Smt. Jayalalitha, the film met with controversies over the name coinciding with the name of the then former Chief Minister of Tamil Nadu, Jayalalithaa.  Later, it was renamed as Jai Lalitha.  However, the director clarified that the name has no connection with the chief minister and instead signifies the strong embodiment of the woman-in-disguise character that Sharan was supposed to play. 

==Cast== Sharan as Jayaraj
* Ravishankar Gowda as Lakshmikantha
* Disha Pandey as Disha
* Aishwarya Devan as Lalitha
* T. S. Nagabharana as Srikantaiah
* Harish Raj as Yogi
* Sayaji Shinde as Raghav

==Production==
Pon Kumaran, who tasted success with Only Vishnuvardhana|Vishnuvardhana and Chaarulatha, planned to remake the Malayalam hit film Mayamohini in Kannada. He approached actor Sharan and narrated the story to him while on the sets of Kempegowda. Sharan was initially reluctant to appear in a female get-up. Later, he agreed upon the role and shed around 11 kilos in preparation.   To get the external appearance right, he went through beauty treatments including facial, bleaching and threading.  He stated that he lived like a woman for four months, trying to adopt feminine mannerisms by shutting himself at home and wearing his wifes clothes to practice.  

Jai Lalitha was shot in Bangalore, Mysore and other areas of Karnataka. 

==Soundtrack==
The audio was launched on 26 May 2014 at hotel Citadel in Bangalore. Popular music director V. Harikrishna and director Dinakar Thoogudeep were present as the chief guest.  Actor Upendra and director Yogaraj Bhat have recorded their voices for the soundtrack of the film. The music is composed by Sridhar V. Sambhram consisting of six tracks and all songs are written by Lokesh Krishna.

{{Track listing
| total_length   =
| all_lyrics     = Lokesh Krishna
| extra_column	 = Singer(s)
| title1	= Enivaaga Naanu Kannadiga
| lyrics1 	= 
| extra1        = Vijay Prakash, Yogaraj Bhat
| length1       = 
| title2        = Dilge Dilge
| lyrics2 	= 
| extra2        = Ankit Tiwari, Supriya Lohith
| length2       = 
| title3        = Sadarame
| lyrics3 	= 
| extra3        = Shankar Mahadevan
| length3       = 
| title4        = Bedi Hachko
| lyrics4       = 
| extra4        = Apoorva Sridhar, Sneha Ravindra
| length4       =
| title5        = Thorisabedammi
| lyrics5       =
| extra5        = Upendra, Apoorva Sridhar
| length5       = 
| title6        = Kannada Savi Kannada
| lyrics6       = 
| extra6        = 
| length6       = 
}}

===Release=== Darshan and his brother Dinakar.  Jai Lalitha was given a U/A certificate by the Regional Censor Board.  

===Critical reception===
The Times of India gave 3 stars out of 5 and wrote, "Director P Kumar has selected a good subject with a focus on comedy. Even the script too deserves a pat. But as story opens up, there are too many characters and sequences that give new twists to the story. Though it is appreciable as all of them form part of the script, it is difficult to follow the sequences suddenly too many turns to the story crop up. Once the movie ends, it takes sometime for the audience to recap the narration. But director has mainatined   the comedy to some extent in all sequences with dialogues and Sharan at his best".  The New Indian Express wrote, "P Kumar seems to have definitely done justice to this remake of the Malayalam comedy flick Mayamohini. Sure to tickle your funny bones, Jai Lalitha is a one-time watch".  

Deccan Chronicle wrote, "Though the hard work towards looking ‘like’ a woman reflects well in the posters, it does not replicate much in terms of performance and makes it a painful watch for over two hours. It is yet again a simple gimmick to draw the audience to theatres while the screenplay and other vital aspects lack substance for a decent entertainment". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 