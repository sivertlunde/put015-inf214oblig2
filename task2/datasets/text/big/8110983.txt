An Unreasonable Man
{{Infobox film
| name           = An Unreasonable Man
| image          = Unreasonable man.jpg
| image_size     = 200px
| caption        = Promotional movie poster for the film
| director       = Henriette Mantel Steve Skrovan
| producer       = Kevin ODonnell
| writer         = 
| narrator       = 
| starring       = Ralph Nader Pat Buchanan Phil Donahue William Greider Eric Alterman James Ridgeway
| music          = 
| cinematography = Mark Raker
| editing        = Beth Gallagher Alexis Provost
| distributor    = IFC Films
| released       =      
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = 
}} 2000 U.S. presidential election

The first half of the film examines Naders advocacy for auto safety features, such as federally mandated seat belts and air bags, as well as his rise to national prominence following an invasion of privacy lawsuit against General Motors. It also examines the formation of independent advocacy groups (termed "Naders Raiders") during the 1970s; organizations which carried out independent research on various federal agencies, such as the Federal Trade Commission and the Food and Drug Administration. Over the next thirty years, the film argues, Nader "built a legislative record that would be the envy of any modern president."
 grassroots form third party spoiler in 2000 presidential election.

The film presents interviews with current and former members of Naders Raiders, including Joan Claybrook and Robert Fellmeth, as well as politicians and political analysts such as Phil Donahue, Pat Buchanan, and Eric Alterman. The film takes its name from the George Bernard Shaw quote, "The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the world to himself. Therefore all progress depends on the unreasonable man."

==Response==
 
The documentary has an aggregate of 92% (54/59) on Rotten Tomatoes and a score of 75% on Metacritic.  

==Festival screenings==
* 
*Sundance Film Festival

==Notes==
 

==External links==
*  PBS
* 
*  
* 
* 

 

 
 
 
 
 
 
 
 
 