Black Holiday
 
{{Infobox film
| name           = Black Holiday
| image          = Villeggiatura adalberto maria merli marco leto 006 jpg circ.jpg
| caption        = Film poster
| director       = Marco Leto
| producer       = Enzo Giulioli
| writer         = Lino Del Fra Marco Leto Cecilia Mangini
| narrator       =
| starring       = Adolfo Celi
| music          =Egisto Macchi
| cinematography = Volfango Alfi
| editing        = Giuseppe Giacobino
| distributor    =
| released       =  
| runtime        = 112 minutes
| country        = Italy
| language       = Italian
| budget         =
}} political drama film directed by Marco Leto and starring Adolfo Celi.   

==Cast==
* Adolfo Celi as Commissioner Rizzuto
* Adalberto Maria Merli as Franco Rossini
* John Steiner as Scagnetti
* Luigi Uzzo as Massanesi
* Aldo De Correllis as Prisoner
* Gianfranco Barra as Priest
* Silvio Anselmo as Inventor
* Vito Cipolla as Renzetti
* Roberto Herlitzka as Guasco
* Biagio Pelligra as Mastrodonato
* Giuliano Petrelli as Nino
* Nello Riviè as Prisoner
* Milena Vukotic as Daria Rossini

==References==
 

==External links==
* 

 
 
 
 
 
 
 