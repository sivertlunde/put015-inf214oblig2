Kashmir Ki Kali
{{Infobox film
| name           = Kashmir Ki Kali
| image          = Kashmir Ki Kali 1964 film poster.jpg
| image_size     = 
| caption        = Film poster
| director       = Shakti Samanta
| producer       = Shakti Samanta
| writer         = Ranjan Bose Ramesh Pant
| narrator       =  Pran Nazir Hussain
| music          = O. P. Nayyar S.H. Bihari (lyrics)
| cinematography = V. N. Reddy
| editing        = Dharamvir
| distributor    = Shakti Films
| released       = 1964
| runtime        = 168 minutes
| country        = India Hindi
| budget         = 
}}
 1964 Cinema Indian film directed by Shakti Samanta. It stars Shammi Kapoor, Sharmila Tagore in her debut role, Pran (actor)|Pran, Dhumal (actor)|Dhumal, Nazir Hussain and Anoop Kumar. The music was composed by O.P. Nayyar and the lyrics penned by S.H. Bihari. It became the sixth-highest grossing film of the year and was declared a hit at the box office. The film was remade in Telugu as Shrungara Ramudu.

==Plot==
Wealthy and pampered Rajiv Lal (Shammi Kapoor) is the only son of widowed Rani Maa. At the silver jubilee anniversary of the mill that his father, the late Sanjiv Lal opened, he announces a bonus of 5 lakh rupees for all the workers, annoying his mother. A servant at the house, Karuna (Mridula Rani), suggests marrying him off to stop his immature antics, to which Rani Maa agrees. Her manager, Shyamlal, arranges for her to meet some girls, as she insists that Rajiv will marry a girl of her choice. Rajiv returns home and sees all the girls - he correctly surmises that his mother is getting him married, and he pretends to be a mute who walks with a limp in order to chase all the prospective suitors off. His plan succeeds, although he is in a dilemma as his mother is furious at him. One of his friends suggests that he should go away to their bungalow in Kashmir - after all, Rani Maa never goes there.

Upon arrival in Kashmir, he finds that his estate manager, Bholaram (Dhumal (actor)|Dhumal), has made a hotel out of their bungalow and has rented out some rooms for the season. He tells the tenants that he is Rajiv Lal, although when he meets Bholaram, the latter informs him that Rani Maa had called him, and told him not to let Rajiv stay at the bungalow, should he come. Rajiv manages to convince Bholaram to tell Rani Maa that he is not there. Bholaram tells the tenants in the house that Rajiv is mad, and in his madness, thinks that he is the real owner of the estate.

The next day, Rajiv meets Champa (Sharmila Tagore), a girl who sells flowers for a living. He buys her flowers for 20 rupees when they are priced at 5, and tells her that he is not the owner, but the driver. Soon enough, after a few misadventures, they fall in love and decide to marry, but there are a few obstacles. First of all, Mohan (Pran (actor)|Pran), the forest manager, has loaned out some money to Champas father, Dinu (Nazir Hussain), and he insists on marrying Champa if Dinu cannot repay the debt.

Things become complicated when Mohan finds out that Dinu is not Champas real father, and he blackmails Dinu into getting Champa married to him, or else he will let out the secret to Champa. Champa still continues to meet Rajiv on the sly, but all of that comes to an end when three girls, who are tenants in his house, reveal in her presence that he is not the driver, but the owner of the estate. Feeling betrayed, Champa runs off. Before Rajiv can go after her, he receives a trunk call from his mother, and he is informed that Karuna is seriously ill.

After telling Bholaram to explain everything to Champa, he leaves for Bombay, and finds Karuna on her deathbed, with Rani Maa at her side. Karuna tells Rajiv that he is not the real son of Rani Maa, and that his biological father is none other than her brother, Dinu. Many years ago, Dinu had sold him off to Karuna for alcohol, and Rani Maa had taken him in. Soon after, Rani Maa gave birth to a baby girl, but Dinu abducted her, intending to kill her so that Rajiv will be the sole heir to Rani Maas wealth.

However, on the way, Dinu tripped and fell, and loses his eyesight, so he does not kill the girl. Rajiv is shocked at the revelation, but more so is Rani Maa, who had assumed that her baby girl was still-born. Before Karuna can say who the girl is, she dies. Shyamlal informs the two that the day Karuna fell ill, a man from Kashmir had come to see her and asked about Rani Maas girl, but Karuna hadnt told him anything. The man turns out to be Mohan. Rajiv and Rani Maa head back to Kashmir to find out who the girl is, and Rajiv, acting on a tip from Bholaram, goes to confront Mohan.

He beats Mohan unconscious, and Mohans friend tells Rajiv who the girl is - Champa. He goes to find her, and finds out that she is getting married to Mohan in a few days. Dinu returns home and Champa questions him. He lies to her at first, but when Rajiv drops a hint that he is the son that Dinu had sold all those years ago, the latter hugs him and admits that Champa is not his daughter. Rani Maa comes in and finds Champa, and the two embrace. Mohan arrives with his goons and decides to forcibly marry Champa.

Dinu tries to stop him, but Mohan beats him badly. The goons tie Rajiv up, and then take Rani Maa and Champa to the wedding venue forcibly. Back at Rajivs bungalow, Chander (Anoop Kumar), Rajivs friend, and Shyamlal decide to go after them and see why they are taking so long. Rajiv manages to escape from his captors, but Dinu is still unconscious. He meets with Chander and Shyamlal on the way, and tells them to get the police while he goes after Mohan.

He breaks into Mohans hideout and stops the wedding ceremony. A fight ensues, and Mohan runs off, Rajiv hot on his tail. The police arrives soon after, and arrests all of Mohans goons. Meanwhile, Rajiv and Mohan get into a fight, and the police come to take the latter away. The film ends with a shot of Rajiv and Champa, happily married, driving away in his car.

==Cast==

* Shammi Kapoor ...  Rajiv Lal
* Sharmila Tagore ...  Champa Chameli Pran ...  Mohan
* Nazir Hussain ...  Dinu
* Dhumal (actor)|Dhumal...  Bholaram
* Anoop Kumar ...  Chander
* Madan Puri ...  Shyamlal
* Bir Sakuja ...  Colonel Sunder ...  Lala
* Mridula Rani ...  Karuna
* Tun Tun ...  Rama Devi
* Padma Chavan
* Padmadevi
* Sujata
* Neeta Samar Chatterjee
* Aruna
* Hameeda
* Sabita
* Pradeep Chaudhry
* Shukla

==Production==
The films shooting was stalled for 21 days due to incessant heavy rains in Kashmir. After weather cleared, it didnt rain for the next 25 days, and the entire Kashmir schedule was completed. Recommended by films lead Shammi Kapoor himself, originally duo Shankar-Jaikishan were to compose films music, however veteran composer OP Nayyar invited Samanta to listen to some tunes, which he liked. Eventually 12 were selected for the film, and the remainder were used in Sawan Ki Ghata (1966).   

== Music ==
{{Infobox album  
| Name        = Kashmir Ki Kali
| Type        = Soundtrack
| Artist      = O.P. Nayyar
| Cover       =
| Released    = 1964 (India)
| Recorded    = 1963
| Genre       = Film soundtrack|
| Length      = 35:19
| Label       = Sa Re Sa Ma 
| Producer    = O.P. Nayyar
| Last album  = Phir Wohi Dil Laya Hoon (1963)
| This album  = Kashmir Ki Kali  (1964)
| Next album  = Mere Sanam  (1965)        
|}}

The films music was composed by O.P. Nayyar, and the lyrics were penned by S.H. Bihari. Mohammed Rafi sang four solos for Shammi Kapoor, and three duets with Asha Bhosle. Most of the songs are romantic and were picturized on both Shammi Kapoor and Sharmila Tagore.

===Soundtrack===
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s)!! Picturized on !! Length
|-
| 01. Kahin Na Kahin Dil Lagana Padega
| Mohammed Rafi
| Shammi Kapoor
| 4:57
|-
| 02. Taarif Karoon Kya Uski (Yeh Chand Sa Roshan Chehra)
| Mohammed Rafi
| Shammi Kapoor and Sharmila Tagore
| 5:10
|-
| 03. Isharon Isharon Men Dil Lenewale
| Mohammed Rafi and Asha Bhosle
| Shammi Kapoor and Sharmila Tagore
| 4:46
|-
| 04. Subhanallah Haseen Chehra
| Mohammed Rafi
| Shammi Kapoor and Sharmila Tagore
| 3:37
|-
| 05. Meri Jaan Balle Balle
| Mohammed Rafi and Asha Bhosle
| Shammi Kapoor and Sharmila Tagore
| 6:46
|-
| 06. Diwana Hua Badal
| Mohammed Rafi and Asha Bhosle
| Shammi Kapoor and Sharmila Tagore
| 5:36
|-
| 07. Hai Duniya Usiki Zamana Usika
| Mohammed Rafi
| Shammi Kapoor
| 5:55
|-
| 08. Balma Khuli Hawa Mein
| Asha Bhosle
| Sharmila Tagore
| 3:36
|}

== References ==

 
== External links ==
*  

 
 
 
 
 
 