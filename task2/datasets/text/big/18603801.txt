Lo vedi come sei... lo vedi come sei?
{{Infobox film
| name           = Lo vedi come sei... lo vedi come sei?
| image          =
| caption        =
| director       = Mario Mattoli
| producer       = Steno
| starring       = Erminio Macario
| music          =
| cinematography = Ugo Lombardi
| editing        = Fernando Tropea
| distributor    =
| released       = December 1939
| runtime        = 75 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

Lo vedi come sei... lo vedi come sei? is a 1939 Italian comedy film directed by Mario Mattoli and starring Erminio Macario.

==Cast==
* Erminio Macario - Michele Bernisconi (as Macario)
* Franca Gioieta - Rosetta
* Amleto Filippi - Tommaso Bernisconi
* Enzo Biliotti - Il notaio Cassetta
* Lina Tartara Minora - Adelaide (as Paola Minora)
* Carlo Rizzo - Il venditore di cravatte
* Vinicio Sofia - Il sindaco del paese
* Carlo Campanini - Il postino
* Greta Gonda - Emily
* Armando Migliari - Limbonitore al lunapark

==External links==
* 

 

 
 
 
 
 
 
 
 
 