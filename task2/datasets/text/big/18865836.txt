Pot o' Gold (film)
{{Infobox film
| name           = Pot o Gold
| image          = Pot o Gold- 1941- Poster.png
| image_size     =
| caption        = 1941 Theatrical poster George Marshall
| producer       = James Roosevelt
| based on       =  
| writer         = Andrew Bennison Monte Brice Mary Gordon
| music          = Louis Forbes
| cinematography = Hal Mohr
| editing        = Lloyd Nosler
| studio         =
| distributor    = United Artists
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}}
 1941 United American romance romantic musical musical comedy George Marshall, Pot o Gold. The film was released April 3, 1941, eight months before the NBC radio series came to an end. Paulette Goddards singing voice was dubbed by Vera Vann.
The film is also known as Jimmy Steps Out (American alternative title) and as The Golden Hour (in the United Kingdom).

== Background ==
 
Pot o Gold was radios first big-money giveaway program, garnering huge ratings within four weeks of its 1939 debut. The programs success prompted production of the film. The premise of the radio program, created by Ed Byron, was that any person who picked up the telephone when host   from September 26, 1939 to December 23, 1941 and later a new show by the same name from October 2, 1946 to March 26, 1947 on ABC.

== Plot ==
 

The film tells of a couple romantically involved despite family feuds.

Jimmy Haskell (James Stewart) is the former owner of a defunct music store. His uncle, C.J. Haskell (Charles Winninger), dislikes music and has long wanted Jimmy to join him in his health food business. Jimmy only agrees after his music store is closed. When Jimmy arrives at his uncles place, he is confronted by members of the McCorkle family, who play in Heidts band and often practice outside C.J.s business. As C.J. hates music, he is infuriated and attempts to stop the band using the police. Unsuccessful, he is thrown a tomato by Jimmy, unintentionally. Jimmy is then made a hero by the band and the McCorkles, who do not know his true identity. Molly McCorkle (Paulette Goddard) falls in love with him.

When Jimmy substitutes for C.J. on the Haskell radio program, two band members find out his identity. They work together to devise a scheme to persuade C.J. to take a vacation. In the meantime, Jimmy takes over the operation of the business and invites Heidts band to play on the radio. Molly learns Jimmys identity, and in anger, she says the Haskell program will give away $1000 every week. Jimmy has no choice but to find a way to hand out the cash, and a federal investigator reminds him that using lottery to give the 1000 dollars is illegal.

Jimmy plans to use phone books and a roulette-style game to find winners. The Haskell program grows immensely popular and attracts lucrative advertising contracts. This reconciles the Haskell and McCorkles, paving the way for the marriage between Jimmy and Molly.

== Cast ==
* James Stewart as James Hamilton "Jimmy" Haskell
* Paulette Goddard as Molly McCorkle
* Horace Heidt as Himself
* Charles Winninger as Charles "C.J." Haskell Mary Gordon as Mom McCorkle Frank Melton as Jasper Backus
* Jed Prouty as J.K. Louderman
* Charles Arnt as Parks (butler)
* Dick Hogan as Willie McCorkle James Burke as Police Lt. Grady
* Donna Wood as Donna McCorkle
* Larry Cotton as Larry Cotton, Vocalist
* Art Carney as Radio Announcer

== Soundtrack == Henry Russell and Louis Forbes)
* Paulette Goddard (dubbed by Vera Vann) with Horace Heidt & His Musical Knights - "Pete the Piper" (Written by Henry Russell) Fred Rose)
* Horace Heidt & His Musical Knights - "A Knife, a Fork and a Spoon" (Written by Dave Franklin)
* Larry Cotton with Horace Heidt & His Musical Knights - "Do You Believe in Fairy Tales?" (Music by Vee Lawnhurst, lyrics by Mack David)
* Paulette Goddard (dubbed by Vera Vann) with Horace Heidt & His Musical Knights - "Broadway Caballero" (Written by Henry Russell)

== Reception ==
Stewart later referred to the film as the worst one he ever appeared in. 

Nine years later, Stewart did another movie about a big-money radio show, The Jackpot (1950).

==References==
{{reflist|refs=
 {{Citation
 | first1    = Leonard
 | last1     = Maltin
 | first2    = Luke
 | last2     = Sader
 | title     = Leonard Maltins Movie Guide: 2009 Edition
 | publisher = Penguin
 | page      = 1089
 | year      = 2008
 | isbn      = 0452289785
 | url       = http://books.google.com/books?id=6EgPDierNGUC&pg=PA1089
 | postscript= .
}} 
}}

== External links ==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 