Deadly Pursuit
 

{{Infobox film
| name           = Deadly Pursuit
| image          = Deadly Pursuit poster.jpg
| caption        = Promotional poster for Deadly Pursuit
| director       = Russ Diaper
| producer       = Scott Livingstone
| writer         = Russ Diaper  Damian Morter
| starring       = Russ Diaper Damian Morter Kellymarie Kerr Paul Kelleher
| music = Benson Taylor
| cinematography = Russ Diaper
| editing        = Russ Diaper
| distributor    = Sledge Films
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Deadly Pursuit is a 2008 crime thriller film directed by Russ Diaper.

==Plot==
LA is a tough place to be a cop but things get even tougher for detective Michael Denison when terrorists target him and his unit in the hope of taking over LA. On top of these he must fend off an FBI agent who is trying to put him in jail as his demons and closet skeletons come back to haunt him while he tries to cover for his cocaine-addicted ex-partner and friend Nick. As the plot thickens, guns are loaded, bombs are planted and the terrorists move in closer, Denison will learn that friendship is thrown out the window and survival is key. The law has two sides and rules WILL be broken.also

==Cast==
*Russ Diaper as Michael Denison
*Damian Morter as Nick Lane
*Kim Sønderholm as FBI Agent Shaun Johansson
*Paul Kelleher as Captain Jack Stone
*Kelly-Marie Kerr as Sandra Gilmore
*Alan Cunningham as Assistant Chief Rosenthal
*Margaret Ann Bain as Caitlin
*Scott Livingstone as Brent
*Reena Lalbihari as Midnight
*Mitch Powell as Joe
*Nils Reucker as Phillip Weston
*Lee Cheney as Terrorist Suspect
*Felix John Fraser as Snake
*John Chisem as Barfly
*Alina-Jane Lovell as Barlady
*Andrew Axton as Lorenzo
*Andre Marshall as Terrorist Daniel Bennett as Walter Fritz

== Release ==
Deadly Pursuit was released on June 2008 worldwide.

==References==
 

==External links==
*  

 
 
 

 