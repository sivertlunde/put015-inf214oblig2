Coração de Gaúcho
{{Infobox film
| name           = Coração de Gaúcho
| image          =
| caption        =
| director       = Luiz de Barros
| producer       = Luiz de Barros
| writer         = José de Alencar (novel)
| starring       = Manuel F. Araujo
| cinematography = João Stamato Luiz de Barros
| editing        = Luiz de Barros
| distributor    = Guanabara Filmes
| released       =  
| runtime        =
| country        = Brazil
| language       = Silent
| budget         =
}} 1920 Brazilian silent drama film directed by and starring Luiz de Barros. The film is based on the novel O gaúcho by José de Alencar.

The film premiered on April 26, 1920 in Rio de Janeiro and stars Manuel F. Araujo and António Silva (actor)|António Silva.

==Cast==
*Manuel F. Araujo  
*Luiz de Barros   
*Antônia Denegri   
*Alvaro Fonseca   
*Cândida Leal   
*António Silva (actor)|António Silva

==External links==
*  

 

 
 
 
 
 
 
 


 
 