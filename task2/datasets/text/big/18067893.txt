Silent Gunpowder
{{Infobox film
| name           = Gluvi barut (Silent Gunpowder)
| image          =
| writer         = Branko Ćopić (novel) Bato Čengić (screenplay)
| starring       = Mustafa Nadarević Branislav Lečić Fabijan Šovagović Mira Furlan Boro Stjepanović Josip Pejaković Zijah Sokolović
| producer       = Mirza Pašić
| director       = Bato Čengić
| cinematography = Božidar Nikolić Tomislav Pinter
| editing        = Andrija Zafranović
| distributor    =
| released       =  
| runtime        = 116 minutes
| country        = Yugoslavia
| language       = Serbo-Croatian
| budget         =
| music          = Goran Bregović
}} Yugoslavian war film directed by Bato Čengić, starring Mustafa Nadarević, Branislav Lečić, Fabijan Šovagović, Mira Furlan, Boro Stjepanović and Josip Pejaković.

== Plot == Bosnia and ideological lines, Royal Army officer Radekić (Branislav Lečić). Španac sees Radekić as the cause of villagers resistance to the new, Communist, ideology and so the main plot axis is the conflict between them.

== Awards ==
*At the 1990 Pula Film Festival (the Yugoslavian version of the Academy Awards), the film won the Big Golden Arena for Best Film, as well as the awards for Best Actor in a Leading Role (Branislav Lečić), Best Film Score (Goran Bregović), and Best Makeup (Snježana Tomljenović).
*The film was also shown at the 17th Moscow International Film Festival, where both Branislav Lečić and Mustafa Nadarević won the Silver St. George Award for their performances.   

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 
 