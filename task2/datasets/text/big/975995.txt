Mrs. Parker and the Vicious Circle
 
{{Infobox film
| name        = Mrs. Parker and the Vicious Circle
| image       = Mrs. Parker and the Vicious Circle.jpg
| caption     = Theatrical release poster
| director    = Alan Rudolph
| producer    = Robert Altman
| writer      = Alan Rudolph Randy Sue Coburn
| music       = Mark Isham
| starring    = Jennifer Jason Leigh Campbell Scott Matthew Broderick
| studio      = Miramax Films
| distributor = Fine Line Features
| released    =  
| runtime     = 126 minutes
| country     = United States
| language    = English
| budget      = $7,000,000
| gross       = $2,144,667
| awards      =
}}

Mrs. Parker and the Vicious Circle is a 1994 American film scripted by screenwriter/director Alan Rudolph and former Washington Star reporter Randy Sue Coburn. Directed by Rudolph, it starred Jennifer Jason Leigh as the writer Dorothy Parker and depicted the members of the Algonquin Round Table, a group of writers, actors and critics who met almost every weekday from 1919 to 1929, at Manhattans Algonquin Hotel.

The film was an Official Selection at the 1994 Cannes Film Festival and was nominated for the Palme dOr. The film was a critical but not a commercial success. 

Peter Benchley, who played editor Frank Crowninshield, was the grandson of Robert Benchley, the humorist who once worked underneath Crowninshield. Actor Wallace Shawn was the son of William Shawn, the longtime editor of The New Yorker.

==Cast==
===The Vicious Circle===
*Jennifer Jason Leigh played Dorothy Parker 
*Campbell Scott played Robert Benchley 
*Martha Plimpton played Jane Grant 
*Sam Robards played Harold Ross 
*Lili Taylor played Edna Ferber 
*Stanley Tucci played Fred Hunter
*James LeGros played Deems Taylor  Robert Sherwood  David Thornton played George S. Kaufman 
*Tom McGowan played Alexander Woollcott 
*Chip Zien played Franklin Pierce Adams 
*Gary Basaraba played Heywood Broun  Jane Adams Ruth Hale 
*Matt Malloy played Marc Connelly 
*Rebecca Miller played Neysa McMein 
*Jake Johannsen played John Peter Toohey 
*David Gow played Donald Ogden Stewart 
*Leni Parker played Beatrice Kaufman 
*J.M. Henry played Harpo Marx

 
 

===Husbands, wives, lovers and friends of the Round Table===
*Jennifer Beals played Gertrude Benchley 
*Peter Benchley played Frank Crowninshield  
*Matthew Broderick played Charles MacArthur 
*Keith Carradine played Will Rogers 
*Amelia Campbell played Mary Brandon Sherwood 
*Jon Favreau played Elmer Rice Alan Campbell 
*Malcolm Gets played F. Scott Fitzgerald 
*Heather Graham played Mary Kennedy Taylor 
*Andrew McCarthy played Edwin Pond Parker II
*Gisèle Rousseau played Polly Adler
*Randy Lowell played Alvan Barach
 

===Fictional characters===
*Stephen Baldwin played Roger Spalding
*Gwyneth Paltrow played Paula Hunt 
*Wallace Shawn played Horatio Byrd
*Evan Bailey played Paul Ryan
 

==Development==
Director Alan Rudolph was fascinated with the Algonquin Round Table as a child when he discovered Gluyas Williams illustrations in a collection of Robert Benchleys essays.    After making The Moderns, a film about American expatriates in 1920s Paris, Rudolph wanted to tackle a fact-based drama set in the same era. He began work on a screenplay with novelist and former Washington Star journalist Randy Sue Coburn about legendary writer Dorothy Parker. In 1992, Rudolph attended a Fourth of July party hosted by filmmaker Robert Altman who introduced him to actress Jennifer Jason Leigh. Rudolph was surprised by her physical resemblance to Parker and was impressed by her knowledge of the Jazz Age. Leigh was so committed to doing the film that she agreed to make it for "a tenth of what I normally get for a film". 

The screenplay originally focused on the platonic relationship between Parker and Robert Benchley, but this did not appeal to any financial backers.  There still was no interest even when Altman came on board as producer. The emphasis on Parker was the next change to the script, but Rudolph still had no luck finding financing for "a period biography of a literate woman."  Altman used his clout to persuade Fine Line Features and Miramax – two studios he was making films for – to team up, with the former releasing Mrs. Parker and the Vicious Circle domestically and the latter handling foreign distribution.  Altman claimed that he forced the film to be made by putting his own money into it and "I put other projects of mine hostage to it. I did a lot of lying".    

Rudolph shot the film in Montreal because the building facades in its old city most closely resembled period New York City.  Full financing was not acquired until four weeks into principal photography.   

The films large cast followed Leighs lead and agreed to work for much lower than their usual salaries. Rudolph invited them to write their own dialogue, which resulted in a chaotic first couple of days of principal photography. Actor Campbell Scott remembered, "Everyone hung on to what they knew about their characters and just sort of threw it out there."  Actress Jennifer Beals discussed this in her appearance on the Jon Favreau documentary program Dinner for Five, wherein she stated that much dialogue was improvised in the style of the real-life characters actors were playing, but that many of those characters were not integral to the plot. As such, many of the actors had much larger parts that were edited down to nearly nothing. The cast trusted their director during the 40-day shoot. They stayed in a run-down hotel dubbed Camp Rudolph and engaged in all-night poker games. Leigh chose not to participate in these activities, preferring to stay in character on and off camera. She did a great amount of research for the role and said, "I wanted to be as close to her as I possibly could."  To this end, Leigh stayed for a week at the Algonquin Hotel and read Parkers entire body of work. In addition, the actress listened repeatedly to the two existing audio recordings of Parker in order to perfect the writers distinctive voice. Leigh found that Parker "had a sensibility that I understand very, very well. A sadness. A depression." 

==Reaction==
A rough cut of Mrs. Parker and the Vicious Circle was screened at the 1993 Cannes Film Festival where it divided film critics. It was rumored afterwards that Leigh re-recorded several scenes that were too difficult to understand because of her accent but she denied that this happened.  The film was an Official Selection at the 1994 Cannes Film Festival and was nominated for the Palme dOr.    The film currently holds a 74% rating on Rotten Tomatoes based on 31 reviews.

For her performance in the film, Jennifer Jason Leigh was nominated for both a Golden Globe Award for Best Actress – Drama and an Independent Spirit Award for Best Female Lead.

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 