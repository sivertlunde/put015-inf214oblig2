Maciste in the Lion's Cage
{{Infobox film
| name           = Maciste in the Lions Cage
| image          =
| caption        =
| director       = Guido Brignone 
| producer       = 
| writer         = Guido Brignone 
| starring       = Bartolomeo Pagano   Elena Sangro   Luigi Serventi
| music          = 
| cinematography = Anchise Brizzi   Massimo Terzano
| editing        = 
| studio         = Cinès-Pittaluga 
| distributor    = 
| released       = 10 February 1926
| runtime        = 
| country        = Italy
| awards         =
| language       = Silent   Italian intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent adventure film directed by Guido Brignone  and starring Bartolomeo Pagano, Elena Sangro and Luigi Serventi. It was part of the popular Maciste series of films. It was the penultimate film of the silent series, followed by The Giant of the Dolomites (1927)

==Synopsis==
Maciste is sent to Africa by a circus showman to capture some lions.

== Cast ==
* Bartolomeo Pagano as Maciste 
* Elena Sangro as Sarah, la cavallerizza 
* Luigi Serventi as Strasser 
* Mimi Dovia as Seida 
* Umberto Guarracino as Sullivan 
* Oreste Grandi as Karl Pommer
* Alberto Collo as Giorgio Pommer 
* Giuseppe Brignone as Bob, il vecchio clown 
* Andrea Habay
* Vittorio Bianchi 
* Augusto Bandini 
* Franz Sala

== References ==
 

== Bibliography ==
* Brunetta, Gian Piero. The History of Italian Cinema: A Guide to Italian Film from Its Origins to the Twenty-first Century. Princeton University Press, 2009. 
* Moliterno, Gino. Historical Dictionary of Italian Cinema. Scarecrow Press, 2008.
* Ricci, Steven. Cinema and Fascism: Italian Film and Society, 1922–1943. University of California Press, 2008.

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 