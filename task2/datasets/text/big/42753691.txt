Cartoonists - Foot Soldiers of Democracy
 
{{Infobox film
| name           = Cartoonists - Foot Soldiers of Democracy
| image          = 
| caption        = 
| director       = Stéphanie Valloatto
| producer       = Radu Mihăileanu André Logie 
| writer         = Stéphanie Valloatto Radu Mihăileanu
| starring       = Plantu
| cinematography = Cyrille Blanc 
| editing        = Marie-Jo Audiard 
| studio         = Oï Oï Oï Productions Panache Productions
| distributor    = EuropaCorp. Distribution
| released       =  
| runtime        = 106 minutes
| country        = France 
| language       = French
| budget         = 
| gross          = 
}}

Cartoonists - Foot Soldiers of Democracy ( ) is a 2014 documentary film directed by Stéphanie Valloatto about 12 cartoonists around the world who risk their lives to defend democracy. The film premiered in the Special Screenings section at the 2014 Cannes Film Festival.   

==Featuring==
* Plantu, French cartoonist  
* Jeff Danziger, American cartoonist  
* Rayma Suprani, Venezuelan cartoonist 
* Angel Boligan, Cuban-Mexican cartoonist 
* Mikhail Zlatkovsky, Russian cartoonist 
* Michel Kichka, Belgian-Israeli cartoonist 
* Baha Boukhari, Palestinian cartoonist 
* Zoho (Lassane Zohore), Ivorian cartoonist 
* Damien Glez, Franco-Burkinabé cartoonist 
* Willis from Tunis (Nadia Khiari), Tunisian cartoonist
* Slim (Menouar Merabtene), Algerian cartoonist
* Pi San (Wang Bo), Chinese cartoonist

==Release==
Cartoonists - Foot Soldiers of Democracy premiered at the 2014 Cannes Film Festival on 19 May. A public preview screening of the film was held at the Place de la République in Paris on 23 May 2014, before its theatrical release on 28 May. 

On 7 January and 9 January 2015, the film was broadcast on Canal+ and France 3 respectively, in tribute to victims of the Charlie Hebdo attack.  
 VOD on 2 December 2014. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 