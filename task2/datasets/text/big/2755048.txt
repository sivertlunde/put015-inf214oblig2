Royal Wedding
 
{{Infobox film
| name = Royal Wedding
| image = Fred_Astaire_and_Jane_Powell_in_Royal_Wedding.jpg
| image_size = 215px
| alt = 
| caption = Astaire and Powell in Royal Wedding
| director = Stanley Donen
| producer = Arthur Freed
| screenplay = Alan Jay Lerner
| story = Alan Jay Lerner Sarah Churchill Peter Lawford
| music = Burton Lane Uncredited: Albert Sendrey
| cinematography = Robert Planck
| editing = Albert Akst
| studio = Metro-Goldwyn-Mayer
| distributor= Loews Inc. 
| released =  
| runtime = 93 minutes 
| country = United States
| language = English
| budget = $1,661,000  . 
| gross = $3,902,000 
}} MGM Musical musical comedy film starring  Fred Astaire and Jane Powell, with music by Burton Lane and lyrics by Alan Jay Lerner. The film was directed by Stanley Donen; it was his second film and the first he directed on his own. It was released as Wedding Bells in the United Kingdom. 
 song and dance duo, echoing the real-life theatrical relationship of Fred and Adele Astaire.
 lapsed into public domain on their 29th anniversary due to failure to renew the copyright registration. 

==Plot== Broadway success. imminent royal wedding.

On the ship, Ellen meets and quickly falls in love with the impoverished but well-connected Lord John Brindale. Whilst casting the show in London, Tom falls in love with a newly engaged dancer, Anne Ashmond. Tom assists Anne to reconcile her estranged parents and also asks his agent to locate Annes supposed fiancé in Chicago – only to discover that hes married.

Carried away by the emotion of the wedding, the two couples decide that they will also be married that day.

==Cast==
* Fred Astaire as Tom Bowen
* Jane Powell as Ellen Bowen Sarah Churchill as Anne Ashmond
* Peter Lawford as Lord John Brindale
* Keenan Wynn as Irving Klinger/Edgar Klinger
* Albert Sharpe as James Ashmond

 , Jane Powell and Fred Astaire in Royal Wedding]]

==Production==
Stanley Donen and Jane Powell were not part of the films original crew and cast; former dancer Charles Walters was the films original director, with June Allyson as Astaires co-star.  Judy Garland was then signed as Ellen, over the objection of Walters who had spent a "year-and-a-half nurturing her through her previous film, Summer Stock"; instead of listening to Walters objection, Arthur Freed brought in Donen as director; Garland, who during rehearsal worked only half-days, starting calling in sick as principal photography was to begin. That prompted Freed to replace her, which in turn caused MGM to cancel her contract with the studio, one that had lasted 14 years. 

Principal photography occurred in 1950, from July 6-August 24; retakes took place in mid-October. 

The scene featuring the song "Youre All the World to Me" was filmed by building a set inside a revolving barrel and mounting the camera and its operator to an ironing board which could be rotated along with the room. 

==Notable songs and dance routines==
 
* "Evry Night At Seven": Astaire pretends to be a bored king alongside a lively Powell.
  Hermes Pan. The Nicholas Brothers in The Pirate.  The fame of the dance rests on Astaires ability to animate the inanimate. The solo takes place in a ships gym, where Astaire is waiting to rehearse with his partner Powell, who doesnt turn up, echoing Adele Astaires attitude toward her brothers obsessive rehearsal habits to which the lyrics (unused and unpublished) also made reference.  In 1997, Astaires widow Robyn authorized Dirt Devil to use a digitally altered version of the scene where Astaire dances with a hatstand in a commercial; Astaires daughter Ava objected publicly to the commercial, implying they had "tarnish  his image" and saying it was "the antithesis of everything my lovely, gentle father represented" 
* "Open Your Eyes": This waltz is sung by Powell at the beginning of a romantic routine danced by Powell and Astaire in front of an audience in the ballroom of a transatlantic liner. Soon, a storm rocks the ship and the duet is transformed into a comic routine with the dancers sliding about to the ships motions. This number is based on a real-life incident which happened to Fred and Adele Astaire as they traveled by ship to London in 1923. 
* "The Happiest Days of My Life": Powells character sings this ballad to Lawfords, with Astaire sitting at the piano. Easter Parade.  Here, for the second time in the film, he seems to parody Gene Kelly by wearing the latters trademark straw boater and employing the stomps and splayed strides that originated with George M. Cohan and were much favored in Kellys choreography. 
* "Too Late Now": Powell sings her third ballad, this time an open declaration of love, to Lawford.
 
* "Youre All the World to Me": In one of his best-known solos, Astaire dances on the walls and ceilings of his room because he has fallen in love with a beautiful woman who also loves to dance. The idea occurred to Astaire years before and was first mentioned by him in the MGM publicity publication Lions Roar in 1945. 
* "I Left My Hat in Haiti": This number, essentially the work of dance director Nick Castle,  involves Powell, Astaire, and chorus in a song and dance routine with a Latin theme.

==Reception==
According to MGM records, the film earned $2,548,000 in the US and Canada and $1,354,000 elsewhere, resulting in a profit to the studio of $584,000. 

Upon its release, Bosley Crowther said it had "a lively lot of dancing and some pleasantly handled songs"; according to Crowther, "Mr. Astaire has fared better in his lifetime-and he has also fared much worse." 

"Too Late Now" was nominated for an Academy Award for Best Original Song at the 24th Academy Awards, losing the award to "In the Cool, Cool, Cool of the Evening" by Hoagy Carmichael and Johnny Mercer, that had been featured in Here Comes the Groom.

==Home video and other formats==
In 2007,   and The Pirate. 
 episode of Cinema Insomnia.  It is also distributed through Corinth Films. 
 above were long play record recorded at 33 1/3 RPM (MGM E-543).
 David Byrne the live film of his band, Talking Heads.  "Sunday Jumps" was also parodied by Kermit the Frog in The Great Muppet Caper. 

==Notes==
{{Reflist|3|refs=

 The Top Box Office Hits of 1951, Variety, January 2, 1952  -->

    -->

   

   

   

   

   

    

   

   

}}

==References==
 
*  
*  

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 