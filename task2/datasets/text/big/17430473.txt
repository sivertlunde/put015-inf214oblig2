The Somme – From Defeat to Victory
 
{{Infobox television film
| bgcolour =
| name =    
| image = BBC Somme DVD Cover.jpg
| caption = BBC DVD Cover
| format = Docudrama
| creator = 
| director = Detlef Siebert
| producer =  
| writer = Detlef Siebert
| narrator = Alisdair Simpson
| starring =  
| music = Alasdair Reid
| country = United Kingdom
| language = English
| distributor = BBC
| released = 2 July 2006
| first_aired = 
| last_aired =
| runtime =   58 min
| num_episodes = 
}}
The Somme – From Defeat to Victory is a 2006 BBC documentary film made to commemorate the 90th anniversary of the Battle of the Somme.

==Production==
The film was produced in conjunction with the Open University and was intended to go beyond the standard histories that end with the British defeat at the end of the first day to demonstrate how the British learnt from their failures and developed radical new tactics that would help the allies to win the war. The film mixes dramatic re-enactments and archive footage augmented by readings from the diaries, letters and reports of the men involved.

==Plot== Lord Kitchener’s call to make up the bulk of the British Army. To relieve the French at Verdun, an Anglo-French diversionary attack is to be launched at the River Somme. German divisional commander Gen. Baron Franz von Soden relies on the experience of veterans such as Cpl. Friedrich Hinkel against the biggest British military deployment in the war thus far. The British go over-the-top at   on 1 July expecting little resistance after a 7 days artillery bombardment of enemy positions but are met by machine-gun fire within minutes.

Cpl. Hinkel faces the 36th (Ulster) Division, which is quickly forced into retreat while   away Cpt. Thomas Tweed leads the 2nd Salford Pals’ B-Company in an attack on the Thiepval Plateau that sees the death of Mellor. The Ulster division regroup to take the stronghold of the Schwaben Redoubt and Maj-Gen. Sir Edward Percival recommends committing the reserves to secure the position and take Thiepval from the north but corps commander Lt.-Gen. Sir Thomas Morland rejects the new plan. With two-thirds of his company dead or wounded Tweed takes refuge for two hours behind a bank in no-man’s land. Sharples disappears attempting to capture the enemy machine gun nest and Fiddis is wounded taking a message to battalion requesting withdrawal. Moorland, some   from the front, follows the failure of the first and second attacks on Thiepval by sticking to the battle plan and ordering a third. The more adaptive German commanders retake the Redoubt rescuing Hinkel’s position and forcing the Ulstermen into a bloody retreat.
 Salford but Frank Maxwell V.C. The artillery fires the new creeping barrage with the 12th Battalion, Middlesex Regiment following immediately behind, easily to overrun the first German trenches but failing to keep up; Maxwell’s men comes under fire from Infantry Regiment 180. A slaughter is averted when British tanks arrive, forcing the terrified Germans into retreat only to be ditched and disabled a short time later.
 Frederick Edwards to secure a British victory. Plessen waits for six hours at Soden’s HQ for news of the attack, by which time it is too late to order a counterattack and Thiepval is lost. The victory allows the British to secure all their objectives from 1 July and the French at Verdun are able to launch a counter-attack to push back the Germans. Thiepval is now the site of the Thiepval Memorial to the Missing of the Somme commemorating the   the   casualties with no known grave including Sharples, Mellor and Fiddes.

==Cast==
{| style="width:100%" border="0" cellpadding="5" cellspacing="0" |- 
| width="50%" valign="top" |
* Iain McKee as Fiddes Chris Hannon as Mellor
* Peter Barich as Sharples
* Rüdiger Kuhlbrodt as General von Soden
* Steffan Boje as Hinkel Mike Rogers as Captain Tweed
* Crispin Redman as Francis Aylmer Maxwell|Lt-Col. Maxwell
* Eric Carte as Lt-Gen. Morland Martin Turner as Lt-Gen. Percival
* John Vine as Brig-Gen. Shoubridge
* Wolf Kahler as General Hans von Plessen
| width="50%" valign="top" |
* Adam Webb as Edwards
* John Shortell as Stammler
* Georg Alfred Wittner as Captain Fischer Peter Stark as Knaupp
* Benedict Sandiford as Captain Johnston
* Erich Redman as Hässler
* Christopher Nabb as Routledge
* Ben Goddard as Lt. Walton
* Thorston Manderlay as Schmitt
* Darren Black as Maxwells ADC
|}

==Media information==
The film was released on Region 2 DVD by BBC Video on 3 July 2006.   

==References==
 

==External links==
 
*  at Open University
*  at BBC History
*    }}

 
 
 
 
 