Canaries Sometimes Sing
{{Infobox film
  | name           = Canaries Sometimes Sing
  | image          = 
  | image_size     = 
  | caption        = 
  | director       = Tom Walls
  | producer       = Herbert Wilcox
  | writer         = W. P. Lipscomb Frederick Lonsdale (play)
  | starring       = Tom Walls Cathleen Nesbitt Athole Stewart Yvonne Arnaud
  | music          =
  | cinematography = Bernard Knowles Freddie Young
  | editing        = 
  | studio         = British & Dominions Film Corporation
  | distributor    = Woolf & Freedman Film Service
  | released       = 1930
  | runtime        = 80 minutes
  | country        = United Kingdom English
  }} 1930 British Globe Theatre in 1929, with Stewart and Arnaud cast in the roles which they would recreate in the film.  A surviving review of the film notes favourably: "Glittering, superficial, but very skilful...superbly played." 

==Plot==
Over the course of their marriage, Geoffrey Lymes (Walls) has become increasingly exasperated by the shallowness and superficiality of his wife Anne (Nesbitt).  He despairs of her ridiculous affectations, social-climbing aspirations and constant embarrassing attempts in company to show herself as an elegant, cultured sophisticate.  He feels trapped in a relationship where, as he observes, a wife "does nothing to entitle her husband to divorce her, but a thousand things that entitle him to murder her".

Geoffreys old college friend Ernest Melton (Stewart) and his French wife Elma (Arnaud) arrive at the Lymes country home for a weekend visit.  Ernest is an archetypal upper-class twit, wealthy but not overly bright, and completely cowed and dominated by the self-assured and outspoken Elma.  He too finds married life less than satisfactory.

Anne immediately goes into full desperate-to-impress mode and, to Geoffreys amusement, Ernest seems completely charmed and captivated by her ludicrous airs and pretentions, while Anne is thrilled to have found an appreciative audience.  Geoffrey meanwhile is strongly attracted to the feisty Elma, and his interest is apparently reciprocated.  As the pair discuss their respective spouses with withering scorn, they realise that all four are married to the wrong person.  They hatch a plan to throw Anne and Ernest together as much as possible in the hope that they will compromise themselves.

Matters reach a head when it appears that Ernest and Anne are about to run away together.  Geoffrey sees this as the perfect opportunity to achieve his aims without any blame attaching to himself or Elma.  He confronts Ernest in feigned outrage, expressing his shock and disgust at his friends conduct, while slyly stressing that if Ernest and Anne wish to be together, he can do nothing to prevent it and will give Anne a divorce.  To Geoffreys astonishment, the confused Ernest says that while he finds Anne pleasant and amusing, he does not love her and there has never been any question of the two eloping.  The tables are turned, as Geoffrey is forced to admit to Ernest that he and Elma are in love.

==Cast==
* Tom Walls as Geoffrey Lymes
* Cathleen Nesbitt as Anne Lymes
* Athole Stewart as Ernest Melton
* Yvonne Arnaud as Elma Melton

==References==
 

== External links ==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 
 