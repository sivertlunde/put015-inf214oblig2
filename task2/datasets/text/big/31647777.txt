Devi (1972 film)
{{Infobox film 
| name           = Devi
| image          =
| caption        =
| director       = KS Sethumadhavan
| producer       = MO Joseph
| writer         = K Surendran
| screenplay     = KS Sethumadhavan Madhu Sheela Adoor Bhasi
| music          = G. Devarajan
| cinematography = Melli Irani
| editing        = MS Mani
| studio         = Manjilas
| distributor    = Manjilas
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by KS Sethumadhavan and produced by MO Joseph. The film stars Prem Nazir, Madhu (actor)|Madhu, Sheela and Adoor Bhasi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir Madhu
*Sheela
*Adoor Bhasi
*Sankaradi Khadeeja
*Meena Meena
*Paravoor Bharathan
*Philomina
*Rani Chandra Sujatha
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chandrakiranam chaalicheduthoru || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Karutha Sooryanudichu || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Punarjanmam Ithu || P Jayachandran, P. Madhuri, Chorus || Vayalar Ramavarma || 
|-
| 4 || Saamyamakannorudyaaname || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 