Race (2008 film)
 
 
{{Infobox film
| name           =  Race  
| image          = Race-wallpaper-123860-5641.jpg
| caption        = Theatrical release poster
| director       = Abbas-Mustan Ramesh S. Kumar S. Taurani
| writer         = Jitendra Parmar Anurag Prapanna  (Dialogue) 
| story          = Shiraz Ahmed
| screenplay     = Shiraz Ahmed
| starring       = Anil Kapoor Saif Ali Khan Akshaye Khanna Bipasha Basu Katrina Kaif Sameera Reddy
| music          = Songs:  
| cinematography = Ravi Yadav
| editing        = Hussain A. Burmawala
| studio         = Tips Music Films
| distributor    = UTV Motion Pictures
| released       =  
| runtime        = 155 minutes
| country        = India
| language       = Hindi
| budget         =    
| gross          =   
}}
 Race film series.
 fourth highest Tamil as Telugu as Race Telugu. The films sequel Race 2 was released in 2013, and was also a success.

== Plot ==
A voice-over by RD ( ) who runs a successful business he inherited from his father. His younger brother Rajiv Singh (Akshaye Khanna) who mooches off of Ranvir and is an alcoholic. Ranvir is dating an upcoming model Sonia (Bipasha Basu), while his personal assistant Sophia (Katrina Kaif) appears to be secretly in love with him. Ranvir is also involved in an intense competition with a rival horse-owner Kabir (Dilip Tahil). The film opens with a murder plot involving a car accident which Ranvir narrowly survives. When Ranvir loses money in a race because his jockey had been bribed by Kabir, Ranvir plants a bomb in the jockeys car and kills him, showing Ranvir to be a ruthless businessman.

In a drunken stupor, Rajiv confesses to Ranvir that he likes Sonia and that he would quit drinking if he could spend his life with a woman like her. Ranvir immediately stops dating Sonia so that Rajiv can have her. Rajiv and Sonia begin dating and appear happy until, in a twist, Rajiv reveals he knows of Sonias shady past and has a plan to use Sonia to his advantage. Rajiv reveals that his father had secured identical life-insurance policies for huge sums on each of the sons. He explains that he wants to kill Ranvir in what would appear to be an accident and inherit $100 million in insurance payments and wants Sonia to play along. Sonia agrees to help in exchange for $20 million. They pretend to get married and Rajiv ignores Sonia in this fake-marriage while continuing to play an alcoholic. As per his instructions, Sonia seduces Ranvir who confesses he loved her all along. Rajiv reveals the rest of his plan: to threaten to commit suicide in response to Sonia and Ranvirs affair by leaping off a tall building, have Ranvir appear on the terrace, and get Sonia to push Ranvir off. However, it turns out Ranvir was aware of the plan all along as Sonia, who had apparently fallen in love with Ranvir, had been keeping him updated.

Things go according to plan but Sonia double-crosses Ranvir and pushes him off, saying she loved him, but the money was more important. Inspector RD appears at this point to investigate the death, accompanied by his idiotic assistant Mini (Sameera Reddy). Upon his arrival, he immediately suspects foul-play in the death. During his investigation, Ranvirs assistant Sophia reveals that she was married to Ranvir and produces a legitimate marriage certificate. Sonia is shocked that their plan has been thwarted as Sophia is now the heir to the insurance money. In another twist, Sophia was in on the plan all along and is secretly dating Rajiv. Rajiv plans to bump off Sonia after he and Sophia get the insurance money. However, RD figures out during his investigation that Sophia and Rajiv had faked her marriage with Ranvir and that he had been tricked into signing the marriage certificate. RD confronts Rajiv about this and agrees to remain silent about it in exchange for $25 million.

Rajiv hires the same hitman who had attempted to murder Ranvir at the beginning to kill Sonia, revealing that he had been behind the first murder attempt, too. Just as the hitman is about to kill Sonia, Ranvir reappears and rescues her. Ranvir then confronts Rajiv and explains that he had overheard Rajiv discussing the failed murder attempt with the hitman and had been playing along the whole time so he could get the insurance money from his own fake-death and the insurance money from killing Rajiv. He allows Rajiv one last chance to win by agreeing to a car race. Ranvir shows up to the race in a sports car, whilst Rajiv doesnt. When Rajiv protests, he switches cars with Ranvir. This turns out to be a trick as Rajiv had tampered with the brakes on his own car. Ranvir, in turns, lies to Rajiv, claiming he had planted a bomb in his car, similar to the one that killed the jockey, and that it would be detonated if Rajiv ever slowed down below 100&nbsp;mp/h. This deceives Rajiv into crashing into a petroleum tank, killing him and Sophia, who had been riding with him. Ranvir narrowly escapes death himself. Ranvir then says that there was no bomb in the car, and that Rajiv had "killed himself".

In the end, Ranvir collects the insurance money from Rajiv and Sophias death, along with the money from his own fake-death. Upon fleeing the city, he is stopped by Inspector RD, who had been in on the plan, too, and had helped Ranvir fake his death and get the insurance money. Inspector RD takes his another $25 million, only to find a bomb in the bag along with the money. The bomb does not detonate, and Ranvir reveals that it was a mere precaution to see if RD would try to betray him and take all the money. Ranvir rewards RD for his relative honesty by allowing him to live, and the two drive off.

== Cast ==
*Anil Kapoor as Robert DCosta aka R.D.: A private detective with the deceptive air of an idiot. He is constantly eating fruits while solving a case. He is sharp, resourceful and quick. His appearance in the story thickens the plot as he unravels one lie after the other.
*Saif Ali Khan as Ranvir Singh: The older of the two brothers and a ruthless business man who will stop at nothing to win the game. Playing with danger is his passion and outsmarting his opponent is his pastime.
*Akshaye Khanna as Rajeev Singh: The younger of the two brothers and a spoilt, laid back and happy man. But underneath the surface lurks an anger which he tries to control with liquor. The anger and liquor get the better of him and end up controlling him and he always loses everything.
*Bipasha Basu as Sonia: A glamorous model with a shady past. She is looking for a man who will keep her in his heart, hoping that it will be one of the brothers, before being secretly in love with Ranvir.
*Katrina Kaif as Sophia: Ranvir Singhs secretary who shadows him around managing all his important matters. She is indispensable to Ranvir who trusts her completely but does not reciprocate her love. Sophia then dates Ranvirs brother, Rajiv.
*Sameera Reddy as Mini: The attractive assistant of Robert DCosta who is idiotic. She constantly asks stupid questions and is forever relegated to cutting fruits or doing menial jobs for her boss.
*Dalip Tahil as Kabir: Ranvirs rival in horse races and business.
*Johnny Lever as Max (special appearance): The marriage bureau chief who provides comic relief and a vital clue which leads to solving the case.

== Music ==
{{Album ratings
| rev1 = Bollywood Hungama
| rev1Score =   
| rev2 = Planet Bollywood
| rev2Score =   
| rev3 = Planet Bollywood
| rev3Score =   
}}

The music for Race was composed by Pritam with lyrics by Sameer (lyricist)|Sameer. It got favourable reviews with the song "Pehli Nazar Mein" sung by Atif Aslam as the biggest hit of the album along with "Zara Zara Touch Me".  The song "Khwab Dekhe (Sexy Lady)" was recorded after the release of the soundtrack. It replaced the original song "Mujh Pe Toh Jadoo" in the film. Tips Music then re-distributed the soundtrack, adding "Khwab Dekhe (Sexy Lady)" after the release of the film.

{{Infobox album
| Name = Race
| Artist = Various Artists
| Type = soundtrack
| Label = Tips Music Films
| Music Directer = Pritam
| Last album = Dhan Dhana Dhan Goal (2007)
| This album = Race (2008)
| Next album = Jannat (2008)
}}

{| class="wikitable"
|- style="background:#ccc; text-align:center;"
! Track # !! Song !! Singer(s)!! Duration
|-
| 1
| "Race Saanson Ki"
| Neeraj Shridhar, Sunidhi Chauhan
| 04:50
|-
| 2
| "Pehli Nazar Mein"
| Atif Aslam, Neeraj Shridhar(Background Vocals)
| 05:14
|-
| 3
|"Dekho Nashe Mein"
| Krishnakumar Kunnath|KK, Shaan (singer)|Shaan, Sunidhi Chauhan
| 04:11
|-
| 4
| "Mujh Pe Toh Jadoo"
| Taz (singer)|Taz, Apache Indian, Sunidhi Chauhan
| 04:41
|-
| 5
| "Zara Zara Touch Me"
| Monali Thakur
| 04:32
|-
| 6
| "Race Is on My Mind"
| Neeraj Shridhar, Sunidhi Chauhan
| 05:00
|-
| 7
|"Dekho Nashe Mein (Latin Fiesta Mix)"
| KK (singer)|KK, Shaan, Sunidhi Chauhan
| 04:22
|-
| 8
| "Zara Zara Touch Me (Asian Rnb Mix)"
| Monali Thakur
| 04:47
|-
| 9
| "Khwab Dekhe (Sexy Lady)"
| Neeraj Shridhar, Monali Thakur
| 04:40
|-
|} Deep Within a Bamboo Grove".  Sony BMG Taiwan has issued a legal letter to Tips Industries claiming damages for the song "Zara Zara Touch Me".  

==Reception==

===Critical reception===
 
Race opened to mixed response from critics. Raja Sen gave the film 1.5 out of 5 stars, saying it was a "carnival of twists".  However, Taran Adarsh gave the film 4 out of 5 stars, stating that "Race is a superb entertainer all the way", and that it "has not just style, but a lot of substance too". 

==Box office==
Race went on to become the fourth biggest Bollywood film of 2008 behind Ghajini (2008 film)|Ghajini, Rab Ne Bana Di Jodi and Singh is Kinng in terms of gross.  The film opened with a total of   in India,  900,000 in the United Kingdom and  840,000 in the United States.  It netted  620&nbsp;million in India,    before collecting $5 million from overseas. Race had grossed   in its opening weekend,  before collecting a worldwide gross of  1.06&nbsp;billion.   

==Sequel==
  John Abraham, Deepika Padukone, Jacqueline Fernandez and Ameesha Patel were new additions to the cast. Bipasha Basu was the only other returning member and appeared in a cameo role as Sonia.      The filming for Race 2 started on 5 October 2011.  It was released on 25 January 2013, and was another success, grossing over   domestically. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 