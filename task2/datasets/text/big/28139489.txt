Pimp (film)
{{Infobox film
| name = Pimp
| image = Pimp film.jpg
| director = Robert Cavanah
| producer =  
| writer =  
| starring =  
| music = Tom Hodge
| cinematography = Steve Annis
| editing = Rob Redford
| studio =  
| distributor =  
| released =  
| runtime = 91 minutes
| country = United Kingdom
| language = English
| gross = £205 http://industry.bfi.org.uk/article/16802/UK-Box-Office-21---23-May-2010 
}}
 thriller film Man Bites Billy Boyd, Martin Compston, Scarlett Alice Johnson, Barbara Nedeljakova, Robert Fucilla and Danny Dyer.

==Cast==
*Robert Cavanah as Woody Billy Boyd as Chief
*Martin Compston as Zeb
*Gemma Chan as Bo
*Corey Johnson (actor) as Axel Braun
*Scarlett Alice Johnson as Lizzy
*Barbara Nedeljakova as Petra
*Robert Fucilla as Vincent
*Danny Dyer as Stanley
*Wil Johnson as Byron  

==Plot==
A week in the life of a Soho pimp - Woody - (  webcast appears, showing a former employee being murdered, with another potential webcast seemingly impending.

==Reception==

===Critical response===
Pimp has been panned by critics. It holds a rare 0% approval rating on Rotten Tomatoes, based on 13 reviews, with an average score of 2.2 out of 10.  Mark Kermode gave the film a scathing review, noting that "staggeringly, Danny Dyer is miscast" as a mob boss and said that his performance would be "funny if it wasnt so pathetic and tragic."  Cath Clarke in The Guardian described Pimp as "snoringly predictable...With nil insight – into the sex industry or anything else – you might conclude Pimp is a film for men who get their kicks watching Dyer strut around leering at topless women who – in the parlance of the film – look like "the basic pleasure model".  
  Cath Clarke, The Guardian, , Thursday 20 May 2010.
Retrieved 13 April 2014.   Ellen E. Jones
in Total Film stated : "You wouldnt think a film could actually be both very boring and very offensive. 
Pimp is that paradox made flesh."    Total
Film, May 14th 2010. Retrieved January 28th 2014. 

British film historian I.Q. Hunter, discussing the question "What is the worst British film ever made?",
listed Pimp as one of the candidates for that title. 

===Box office===
The film only grossed £205  and was pulled from cinemas after only one screening on its opening day.

==Home media==
The film was released on DVD and Blu-ray on 24 May 2010,   just four days after it was released in cinemas.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 