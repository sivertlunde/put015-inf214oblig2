Porky's Bear Facts
{{multiple issues|
 
 
 
}}
{{Infobox Hollywood cartoon|
| cartoon_name = Porkys Bear Facts
| series = Looney Tunes (Porky Pig) Manuel Perez| voice_actor = Mel Blanc | musician = Carl Stalling
| producer = Leon Schlesinger Warner Bros. Pictures
| release_date = March 29, 1941
| color_process = Black and white
| runtime = 7 mins
| movie_language = English
}}

Porkys Bear Facts is a Looney Tunes cartoon animated short starring Porky Pig. Released March 29, 1941, the cartoon is directed by Friz Freleng. The voices were performed by Mel Blanc.

This short is an adaptation of the Aesop fable "The Ant and the Grasshopper."

==Plot==
 

 

The attitudes of the two central characters in this cartoon short - Porky Pig and an unnamed bear - form the main plot of this Aesop fable adaptation, with Porky taking the role of the tireless, hard-working ant and the bear the role of the grasshopper, the lazy indignant who would rather do nothing.

The short opens on Porky plowing his land, whistling a happy, carefree tune, "As Ye Sow So Shall Ye Reap." The animals similarly work hard, with several spot gags providing these examples.
 Ferdinand the Bull" and a mouse reading "Of Mice and Men"—have taken up the lazy farmers habits; the dog is lying asleep at his side.

The months pass, and in January a fierce blizzard strikes the area. The scene shifts to the bears shack, and he quickly realizes he has no food. After rummaging through the house to find so much as a morsel, he finds nothing in his cupboards. After describing a delicious feast, the bears dog finds empty cans, prompting both the bear and his canine companion to harriedly search the cans for food. They find one bean in a can, but just as they are saying grace, the mouse steals the bean. The bear cries and bemoans his fate as the dog remarks, "I wouldnt be surprised if he tries to eat me!" Just as he says that, the bear has silverware in hand and goes after the dog.

The bear stalks his pet dog outside, the dog begging off ... until both walk past Porkys window and see that he and his dog have sat down to dinner. The bear and his pet knock on the door and ask to join Porky for dinner, but the pig slams the door on them, saying, "Youve buttered your bed, now sleep in it!" Just as he heads back to the table, he sees the "love thy neighbor" sign at the door, and he feels obligated to invite his lazy neighbors in. The bear quickly feasts at the table.

At the end, the bear remarks that he has learned his lesson and vows not to be hungry again next winter. Then, he spots spring about to arrive ... the bear sprints back to his porch, singing "Working Can Wait."

==Succession==
 
{{succession box |
before= Porkys Snooze Reel |
title= Porky Pig filmography |
years= 1941 |
after= Porkys Preview|}}
 

==References==
* Friedwald, Will and Jerry Beck. "The Warner Brothers Cartoons." Scarecrow Press Inc., Metuchen, N.J., 1981. ISBN 0-8108-1396-3.

==External links==
*  

 
 
 
 
 
 
 
 