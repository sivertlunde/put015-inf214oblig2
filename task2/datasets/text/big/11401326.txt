Three Sappy People
{{Infobox Film |
  | name           = Three Sappy People|
  | image          = ThreeSappyPeopleTITLE.jpg |
  | caption        =  |
  | director       = Jules White |
  | writer         = Clyde Bruckman |
  | starring       = Moe Howard Larry Fine Curly Howard Lorna Gray Don Beddoe Bud Jamison Ann Doran Richard Fiske|
  | producer       = Jules White |
  | cinematography = George Meehan | Charles Nelson |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 17 17"  |
  | country        = United States |
  | language       = English |
}}

Three Sappy People is the 43rd short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
This short begins in the home of Wealthy J. Rumsford Rumford (Don Beddoe). The guests of the party are remarking as to why his wife, Sherry Rumford (Lorna Gray), hasnt appeared yet. Rumsford is growing impatient.  His psychiatrist friend calms him down.  Then Sherry arrives by driving her car into the middle of the living room.  Rumsford asks his psychiatrist friend for help, so he recommends Doctors Ziller, Zeller, and Zoller, who he claims are very expensive and have very peculiar methods, but get results.  Rumsford then has his butler, Williams (Bud Jamison), call their office.

The Stooges are phone repairmen who come to repair the telephone switchboard in the office of the psychiatrists.  Curly accidentally destroys the glass window and water dispenser with a ladder before they can start their work.  After a few blunders, Curly admits that hes nervous because someone he knows is expecting.  He admits not saying anything because hes broke.  Moe is determined to help.  The opportunity comes when Rumsford Rumford, upon the recommendation of a doctor friend of his, calls the office looking for the psychiatrists to treat his impetuous young wife, Sherry, who is always running off on her guests for one crazy reason or another. (She becomes bored at the party and wants to leave for a submarine ride.)  Moe agrees to the job then explains to Larry and Curly that they can make quick money taking the place of the doctors, Drs. Z. Ziller (Curly), X. Zeller (Moe), and Y. Zoller (Larry).  Their ruse works and Sherry enjoys having the Stooges in the house.  After a few antics, the guests try to have dinner.  The Stooges ruin their clients dinner party in their usual style, by starting a pastry fight, but because their antics so amuse his wife, her husband believes that she is cured and the Stooges are paid handsomely for their efforts.
 ). Lorna Gray (center) looks on]]

==Production notes==
Three Sappy People was filmed on April 6–10, 1939.    The films title is a parody of the song title "Two Sleepy People." The short is also the sixth of sixteen Stooge shorts with the word "three" in the title.    

There is an on-going legend that during the pastry fight, 22-year-old Lorna Gray had to be treated on the set after a cream puff became lodged in her throat. However, in an interview later in her life, Ms. Gray actually stated that she was not in any danger and that it was instead director Jules White who was so concerned that he nearly ruined the take. 

Two side-gags that frequently appear in Stooges shorts appear in this short.  First, their usual gag of mixing a bunch of things together to make a drink. Such as their attempt to mix medicine in their short Men in Black (1934 film).  In another scene, there is a living actor playing the part in a piece of art, in this case, a statue. (Paintings in the shorts Three Little Pirates and Heavenly Daze are played by live actors.)

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 