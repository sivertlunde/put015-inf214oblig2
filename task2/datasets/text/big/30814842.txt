Bonjour Balwyn
 
{{Infobox film
| image          =
| name           = Bonjour Balwyn
| director       = Nigel Buesst
| producer       = Nigel Buesst
| writer         = Nigel Buesst John Duigan John Romeril
| starring       = John Duigan Peter Cummins John Romeril Tom Cowan
| editing        = Nigel Buesst Peter Tammer
| music          = Carrl Myriad Janie Myriad
| distributor    =
| released       = October 1971
| runtime        = 55 minutes
| country        = Australia
| language       = English
}}

Bonjour Balwyn is a 1971 Australian independent film directed by Nigel Buesst. It was one of the most notable films of the "Carlton Wave" of filmmaking. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p276 

==Plot==
Kevin Agar is a Carlton, Victoria|Carlton-based owner of a fledgling magazine who struggles to make ends meet. As his financial situation turns desperate, he finds work assisting a television repair man with repossessions.       Agars parents live in the suburb of Balwyn.

==Cast==
* John Duigan as Kevin Agar
* Peter Cummins as TV repairman
* John Romeril as Alan
* Patricia Condon as secretary Barbara Stephens as Christine
* Reg Newson as theatre producer
* Camilla Rountree as Rhonda
* Marcel Cugola Jim Nicholas Alan Finney
* Peter Carmody
* Geoff Gardener   

==Production==
The film was shot on 16mm with funds from the Experimental Film and Television Fund. The original running time was 70 minutes but it was cut down to under an hour to qualify for the short fiction competition at the Sydney Film Festival. 

It was not seen widely outside Melbourne. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, pp.&nbsp;260–261 

==See also==
*Cinema of Australia
*Australian films of 1971

==References==
 

==External links==
*  
*  at Australian Screen Online
*  at Oz Movies
 
 
 
 
 


 
 