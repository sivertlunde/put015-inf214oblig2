Black Orchids (film)
 
{{Infobox film
| name           = Black Orchids
| image          = Black Orchids.jpg
| alt            = 
| caption        = Film poster
| film name      =  Rex Ingram
| producer       = 
| writer         = Rex Ingram
| screenplay     = 
| story          = 
| based on       =  Richard La Reno
| music          = 
| cinematography = Duke Heyward
| editing        = 
| studio         = Bluebird Photoplays Universal Film Manufacturing Company
| released       =  
| runtime        = 5 reels
| country        = United States Silent
| budget         = 
| gross          = 
}} silent drama Rex Ingram. Richard La Reno. The movie was Ingram’s directorial debut, and he later remade the film as Trifling Women in 1922. Black Orchids is considered to be a lost film.    

==Synopsis==
Novelist Emile De Severac warns his daughter, Marie, against treating her fiancé, George Renoir, in fickle fashion. He illustrates his warning by telling her the story of Black Orchids. The heroine of the tale is Zoraida, a beautiful courtesan and fortune teller, possessed of magical powers. She has an ape for a familiar. Zoraida ensnares and ruins many lovers. Sebastian De Maupin becomes jealous of his son Ivan’s success with Zoraida and has him sent to the battlefront. Zoraida wins the Marquis De Chantal, whom the elder De Maupin plans to poison, but is himself done to death when Zoraida has the ape switch the goblets. She weds the marquis in the hope of inheriting his vast fortune. Ivan returns from the war. Zoraida leads him to fight a duel with her husband. The marquis is seriously wounded, but feigns death. His wife puts black orchids on his tomb. The marquis decoys his faithless wife and her lover into an air-tight vault in his chateau, where both perish miserably while De Chantal expires on the threshold. The narration of the tragedy cures Marie of her folly and she is reconciled to her lover. 

==Cast==
*Cleo Madison as Marie de Severac/Zoraida
*Francis McDonald as George Renoir/Ivan Richard La Reno as Emile de Severac
*Wedgwood Nowell as Marquis de Chantal
*Howard Crampton as Baron de Maupin John George as Ali Bara William J. Dyer as Proprietor of l’Hirbour Blanc
*Joe Martin as Haitim-Tai
*Jean Hersholt

==Reviews and reception==
A write-up on the film in the Exhibitor’s Trade Review stated: "Indeed, were it not for this method of picturing the adventures of the man-enslaving Zoraida, one can hardly imagine the film being offered as screen entertainment, in this country at least".  Edward Weitzel wrote in his review of the film in the Motion Picture World, that the drama "deals almost exclusively with open defiance of all moral law, but which nevertheless holds the spectator’s undivided attention to the end of the last reel." Weitzel also commented on Ingram’s direction, saying: "While no points of the plot are glossed over or left in doubt, there is no undue stress put upon any of the incidents and the atmosphere which surrounds the entire story belongs to it by right of birth." 

==See also==
*Lost film
*List of lost films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 