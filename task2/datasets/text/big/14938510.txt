Class Trip
{{Infobox film
| name           = Class Trip
| image          = La Classe de neige.jpg
| image_size     = 
| caption        = 
| director       = Claude Miller
| producer       = 
| writer         = Emmanuel Carrère
| narrator       = 
| starring       = Clement Van Den Bergh
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 16 May 1998 (premiere at 1998 Cannes Film Festival|Cannes) 23 September 1998 (France) 22 January 1999 (UK) 
| runtime        = 96 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1998 cinema French film. Its original French title is La Classe de neige, which is the name given to class trips in the snow. It tells the story of a young boy on a school skiing trip who suffers anxiety attacks that bring on disturbing nightmares.

==Synopsis==
The story begins with Nicolas at ten years old and dreading the school ski trip. Nicolas was worried that he wouldnt fit in with the other boys. His overprotective father decides to take Nicolas by himself instead of going with the group. En route, Nicolas and his father pass the aftermath of an apparently fatal car accident. Seeing police ahead while the traffic had stopped, Nicolas father becomes nervous and conceals all loose items within the car in the trunk – and we see that Nicolas travel bag is next to several matching briefcases. Nicolas subsequently forgets his bag in his fathers car, increasing his awkwardness. 
 wetting his bed, and the situation gets worse when he gets a bed just above Hodkann. At night Nicolas tries to stay awake but falls asleep, waking up from a wet dream but believing he has wet his pants. He leaves his bed to clean his clothes and while returning he discovers it snowing outside. Nicolas goes outside into the freezing cold and ends up locked out, and in desperation curls up inside the car of his ski instructor, Patrick. Before he drifts off to sleep he sees a van driving away. He is found in the morning by Patrick and is taken in to a study with a fever. 
 caterpillar while happens to children in public places when their parents lose sight of them. At the end of the story Nicolas finds that his father was not in an accident as he had day dreamed, but rather that his father was a "monster" and possibly abused children. (Although it is never explained why the father has been arrested). 

The movie ends with Nicolas being driven to his mothers home by Patrick, with the fate of his father and the friendship between Nicolas and Hodkann left to the viewers imagination.

==Cast==
* Clément van den Bergh as Nicolas
* Lokman Nalcakan as Hodkann
* François Roy as The Father
* Yves Verhoeven as Patrick

==Music==
"Laguna Veneta" • "Jeudi" • "Indians" • "Samedi soir" • "Dont but ivory anymore" • "Dimanche soir" • "Laguna laita" • "Lundi".
Compositor - Henri Texier

"Mashala".
Compositor - Bojan Zulfikarpasic

"La Salsa du demon".
"King Kong five".
"Petite Messe Solennelle" - Rossini.
"So tell the girls that Im back in town" - Jay Jay Johansson.

==Awards== Jury Prize.   

==References==
 

== External links ==
*  

 
 

 
 
 
 
 