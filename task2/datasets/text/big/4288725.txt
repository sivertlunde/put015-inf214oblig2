Lisbon Story (1994 film)
{{Infobox film
  | name = Lisbon Story
  | image    = Lisbon story.jpg
  | director = Wim Wenders
  | producer = Paulo Branco Ulrich Felsberg João Canijo Wim Wenders
  | writer = Wim Wenders
  | starring       = Rüdiger Vogler Patrick Bauchau
  | music = Jürgen Knieper Madredeus
  | cinematography = Liza Rinzler
  | editing = Peter Przygodda Anne Schnee
  | distributor =Axiom Films   1994
  | runtime = 100 minutes
  | country = Germany Portugal France Spain German / Portuguese / English
    }}
 1994 film directed by Wim Wenders. It was screened in the Un Certain Regard section at the 1995 Cannes Film Festival.    Wenders, along with three Portuguese film-makers, had been invited by the City of Lisbon to make a documentary about the city, as part of their programme as the European City of Culture in 1994; but the result was the fictional Lisbon Story. 

==Plot== The State Portuguese folk music group Madredeus and Manoel de Oliveira, who at that time was already the oldest living active film director in the world.

==Homage to The Road Movie Trilogy==
During the mid-1970s, Wim Wenders made three films which he named The Road Movie Trilogy. Lisbon Story pays subtle homage to these films. The sound engineer in Lisbon Story, Philip Winter, has the same name and is played by the same actor (Rüdiger Vogler) as the lead character in Alice in the Cities (1974), though the character Phil Winter was a writer in the first film. The name Winter is repeated in Kings of the Road (1976), also starring Vogler, although his full name in Kings is Bruno Winter and he is a projection-equipment mechanic.

==Featured Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Rüdiger Vogler || Philip Winter
|-
| Patrick Bauchau || Friedrich Monroe
|-
| Vasco Sequeira || Truck Driver
|-
| Canto e Castro || Barber
|-
| Viriato José da Silva || Shoemaker
|-
| João Canijo || Crook
|-
| Ricardo Colares || Ricardo
|-
| Joel Cunha Ferreira || Zé
|-
| Sofia Bénard da Costa || Sofia
|-
| Vera Cunha Rocha || Vera
|-
| Elisabete Cunha Rocha || Beta
|-
| Teresa Salgueiro || Herself (Madredeus)
|-
| Pedro Ayres Magalhães || Himself (Madredeus)
|-
| Rodrigo Leão || Himself (Madredeus)
|-
| Gabriel Gomes || Himself (Madredeus)
|-
| José Peixoto || Himself (Madredeus)
|-
| Francisco Ribeiro || Himself (Madredeus)
|-
| Manoel de Oliveira || Himself
|}

==References==
 

==External links==
* 
*  
*  at AllMovie
*  at Rotten Tomatoes
*  

 

 
 
 
 
 
 
 