Lee Jang-ho's Baseball Team
 
{{Infobox film name           = Lee Jang-hos Baseball Team  image          =  director       = Lee Jang-ho  producer       = Lee Jang-ho writer         = Chi Sang-hak based on       =   starring       = Ahn Sung-ki   Lee Bo-hee   Choi Jae-sung music          = Jeong Sung-jo cinematography = Park Seung-bae editing        = Hyeon Dong-chun distributor    =  released       =   runtime        = 125 minutes country        = South Korea language       = Korean
}}
 comic Alien Baseball Team .  

==Plot==
Hye-sung grew up poor but he has a gifted talent for baseball. Eom-ji has watched over him since they were young. Hye-sung falls in love with Eom-ji but when she transfers to another school, they dont see each other for years. Hye-sung and Eom-ji meet again at a baseball field but she is now the girlfriend of the exceptional hitter of high school, Ma Dong-tak. Hye-sung competes endlessly with Dong-tak over Eom-ji. But he ends up with a serious shoulder injury and gives up baseball. Then baseball manager Sohn Byung-ho gathers up dismissed baseball players and forms a team. Manager Sohn puts his team through extreme training and Hye-sung returns to the baseball world. He competes once more with Dong-tak, who has Eom-ji by his side.

==Cast==
*Ahn Sung-ki as Hye-sung
*Lee Bo-hee as Eom-ji
*Choi Jae-sung as Ma Dong-tak
*Maeng Sang-hoon
*Kwon Young-woon
*Shin Chung-shik
*Park Jung-ja
*Park Am
 
==References==
 

==External links==
* 
* 

 
 
 
 