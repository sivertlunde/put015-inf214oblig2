The Last Horror Movie
 
 
 
{{Infobox film
| name           = The Last Horror Movie
| image          = TheLastHorrorMovie.jpg
| caption        = Julian Richards Julian Richards
| writer         = James Handel
| starring       = Kevin Howarth Mark Stevenson
| music          = Simon Lambros
| cinematography = Chris St. John-Smith
| editing        = Claus Wehlisch
| distributor    =
| released       =  
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         =
}} found footage Julian Richards. On August 24, 2003 it premiered at the London FrightFest Film Festival and stars Kevin Howarth and Mark Stevenson. The Last Horror Movie was released onto DVD through Fangoria (magazine)|Fangorias Gore Zone label on December 7, 2004.  

==Plot==
 
 ) who became part of the movie.]]
 tape in order to record the proceedings, breaking the fourth wall and insinuating that the copy of the film being watched is the only existing version of the tape. Throughout the film Max uses meta-references in order to show off his gruesome activities as a serial killer. The film raises questions surrounding visceral pleasure, this can be seen in one scene in particular during which the audience cannot see the victims (two at once) being murdered, Max Parry then asks the audience "I bet you wanted to see that, and if you didnt, why are you still watching?" 

At the end of the film the audience is left to believe that since they are watching the only copy of the film, that they will potentially become one of Maxs victims.

==Cast==
*Kevin Howarth as Max
*Mark Stevenson as The Assistant
*Antonia Beamish as Petra
*Christabel Muir as Sam
*Jonathan Coote as John
*Rita Davies as Grandma
*Joe Hurley as Ben (as Joe Morley)
*Jamie Langthorne as Nico
*John Berlyne as Phil
*Mandy Gordon as Sarah
*Jim Bywater as Bill
*Lisa Renée as Waitress Christopher Adamson as Killer (as Chris Adamson)
*Adrian Johnson as Kelly
*John MacCrossan as Groom

==Development== Stephen Kings Danse Macabre.    Richards was also inspired by "the idea of using horror fiction to help people explore their anxieties about difficult issues", as he has the main character of Max Parry using it as a way to "justify his crimes to the world".  The movie was filmed with a small crew on a limited budget, with most of the films issues stemming from the prosthetic make-up effects, as they "had to work real time whilst remaining hidden from the camera". 

==Release==
 
==Reception==
 
Critical reception for The Last Horror Movie was mixed and as of September 2013 the film holds a rating of 56% on review aggregator Rotten Tomatoes based on nine reviews.  Reel Film criticized the movie as being "repetitive" and that the film would have worked better as a short.  In contrast, Dread Central gave a more positive review and stated that "Its a movie that makes you think, and thats far too rare nowadays." 

==Awards==
* 2005 - Buenos Aires Rojo Sangre Film Festival - Best Film - Winner
* 2005 - Buenos Aires Rojo Sangre Film Festival - Best Actor (Kevin Howarth) - Winner Espoo Ciné - Méliès dArgent, Grand Prize of European Fantasy Film in Silver - Winner
* 2004 - Fantasporto - Critics Award - Winner
* 2004 - Fantasporto - International Fantasy Film Award - Nominee only
* 2004 - New York City Horror Film Festival - Best Actor (Kevin Howarth) - Winner
* 2004 - New York City Horror Film Festival - Best Feature Film - Winner
* 2004 - Rhode Island International Horror Film Festival - Best Director (Julian Richards) - Winner
* 2003 - Festival of Fantastic Films (UK) - Best Independent Feature Award -Winner
* 2003 - Raindance Film Festival - Jury Prize for Best UK Feature - Winner
* 2003 - Festival de Cine de Sitges - Best Film - Nominee Only

==Sequel==
Richards first expressed interest about creating a sequel in 2003, where he remarked that if it was created, the film would either remain in the found footage video diary format of its predecessor or be a "more conventional slasher movie".  In 2012 Richards confirmed that he is actively developing a sequel and that it would be set several years after the events of the first film.    The sequel would have Max living in Los Angeles and showing an obsession with social networking sites, which he uses to select his victims.  J
==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 