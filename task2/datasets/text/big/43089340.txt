Before the Postman
{{Infobox film
| name =   Before the Postman
| image =
| image_size =
| caption =
| director = Mario Bonnard
| producer =  Giuseppe Amato 
| writer =  Aldo Fabrizi    Piero Tellini    Federico Fellini   Cesare Zavattini    Mario Bonnard
| narrator =
| starring = Aldo Fabrizi   Andrea Checchi   Adriana Benetti   Virgilio Riento
| music = Giulio Bonnard  
| cinematography = Vincenzo Seratrice 
| editing = Maria Rosada
| studio =    Società Italiana Cines ENIC
| released =  30 April 1942 
| runtime = 90 minutes
| country = Italy Italian
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Before the Postman (Italian:Avanti cè posto) is a 1942 Italian comedy film directed by Mario Bonnard and starring Aldo Fabrizi, Andrea Checchi and Adriana Benetti.  It was made at  Cinecittà in Rome.

==Cast==
*    Aldo Fabrizi as Cesare Montani  
* Andrea Checchi as Bruno Bellini 
* Adriana Benetti as Rosella  
* Virgilio Riento as Il controllore 
* Carlo Micheluzzi as Angelo Pandolin  
* Cesira Vianello as Cecilia Pandolin  
* Jone Morino as La signora svanita  
* Pina Gallini as La padrona di Rosella  
* Gioconda Stari as Teresa 
* Arturo Bragaglia as Tullio 
* Giulio Battiferri as Pietro, un tranviere  
* Giulio Calì as Un passegero  
* Wanda Capodaglio as La signora Camilla 
* Olga Capri 
* Giuseppe Ciabattini 
* Enrico Luzi as Il passaggero intrigante  
* Vinicio Sofia as Il portiere dellalbergo  
* Anna Maria Zuti

== References ==
 

== Bibliography ==
* Moliterno, Gino. A to Z of Italian Cinema. Scarecrow Press, 2009.
* Reich, Jacqueline & Garofalo, Piero. Re-viewing Fascism: Italian Cinema, 1922-1943. Indiana University Press, 2002.

== External links ==
* 

 
 
 
 
 
 
 
 

 

 