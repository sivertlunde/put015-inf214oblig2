Dirigible (film)
{{Infobox film
| name           = Dirigible
| image          = Dirigible (1931).jpg
| image_size     = 225px
| caption        = Theatrical Film Poster
| director       = Frank Capra
| producer       = Harry Cohn Frank Fouce
| writer         = Frank Wead (story) Dorothy Howell Jo Swerling
| narrator       = Jack Holt Ralph Graves Fay Wray Hobart Bosworth
| music          =
| cinematography = Maurice Wright Joseph Walker
| distributor    = Columbia Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $650,000
| gross          =
| website        =
}} Jack Holt, Ralph Graves and Fay Wray.  The picture focuses on the competition between naval fixed-wing and airship pilots to reach the South Pole by air.

The female lead is assigned to Fay Wray, while the action scenes are handled by stars Jack Holt and Ralph Graves, who also played fliers two years earlier in Capras 1929 airborne adventure, Flight (1929 film)|Flight. This film had been intended to emulate the success of 1927s Wings (1927 film)|Wings, another production with a very similar plot. Dirigible was characterized as "marginally science fictional" by scifilm.org. (Capra later planned to make a fully science fictional movie but was never able to.) 

==Plot==
When famed explorer Louis Rondelle (Hobart Bosworth) requests the U.S. Navys assistance in reaching the South Pole, officer Jack Bradon (Jack Holt) convinces Rear Admiral John S. Martin (Emmett Corrigan) to offer his dirigible, the USS Pensacola, for the attempt. 
 carried on the airship. Frisky, who is adventurous to the point of recklessness, is eager to go even though he has just completed a record-setting coast-to-coast flight and has barely spent any time with his wife Helen (Fay Wray). Basking in the acclaim, he has even forgotten to read the sealed love letter she gave him to open when he arrived.

Helen loves Frisky, but cannot make him believe how much the risks he takes hurt her. She sees Jack without her husbands knowledge and begs him to drop Frisky from the expedition, and for the sake of their marriage, not tell him why. Jack, who also loves her, agrees. Frisky assumes Jack does not want to share the fame, and ends their friendship.
  
The expedition soon ends in disaster: the Pensacola breaks in two and crashes into the ocean during a storm. Frisky participates in their rescue by aircraft carrier. He now gets a leave of absence from the navy to pilot a Fokker Trimotor transport aircraft for Rondelles next attempt at the South Pole. This proves too much for Helen. When she is unable to get Frisky to change his mind, she gives him another sealed letter (to be read when he reaches the Pole), but this time it says that she is divorcing him and will ask Jack to marry her.
 Harold Goodwin) reach the South Pole. When Frisky suggests landing on the snow, Rondelle accepts his judgment that there will be no danger. But in fact the aircraft flips over and bursts into flames, destroying most of their supplies. Rondelles leg is broken and Socks foot is injured. 

After radioing their base camp, they attempt to walk the 900 miles back to it, dragging Rondelle on a sled. Rondelle soon dies and is buried. Later, Frisky has to amputate Socks foot. When Sock realizes he is too much of a burden, he drags himself away to die while the other two are sleeping. They carry on, but Hansen breaks down when he finds they have been going in a circle and have returned to Rondelles grave. Frisky refuses to give up and forces Hansen to continue on.

When Helen hears the news of the crash, she realizes no longer wants a divorce and wishes she could go to Frisky. Jack realizes he can, and talks Rear Admiral Martin into letting him attempt a rescue with his new dirigible, the USS Los Angeles. The two survivors are found and rescued. On the way back, Frisky remembers that he has again forgotten to read Helens letter, but he has snow blindness and asks Jack to read it to him. Jack quickly substitutes his own improvised version, in which Helen is proud of his accomplishment and waiting for her husband with undiminished love. He then destroys the letter. When they return, Frisky uncharacteristically skips a ticker tape parade through New York City to be with his wife. He is the first to mention the contents of the letter; to Helens great relief, she realizes that Jack has not only brought Frisky back to her, but also saved their marriage.

==Cast==
As appearing in Dirigible, (main roles and screen credits identified):   Turner Classic Movies. Retrieved: 6 January 2013.  Jack Holt as Jack Bradon
*Ralph Graves as Frisky Pierce
*Fay Wray as Helen Pierce
*Hobart Bosworth as Louis Rondelle
*Roscoe Karns as Sock McGuire Harold Goodwin as Hansen
*Clarence Muse as Clarence
*Emmett Corrigan as Rear Admiral John S. Martin

==Production== USS Los Frank "Spig" Wead, a former pilot, was given the story credit and stayed on as a technical consultant.

As production began, the old Arcadia airfield was converted into a set, complete with "artificial snow, fake ice mounds and painted backdrop attached to the back side of the dilapidated Army barracks."  With principal photography slated for September, dry ice in metal containers stuffed in actors mouths sufficed for the usual Arctic breath.
 King Kong.

===Aviation aspects=== USS Lexington, with her 8-inch guns in the background during a takeoff of an aircraft.

The aerial cinematography was coordinated by Elmer Dyer.

==Reception==
Dirigible was Capras and Columbias first film to be given prominence with a premiere at Graumans Chinese Theatre, but despite high hopes, the film received lukewarm reviews.   Variety called it "unconvincing."   A more recent review noted that "the odd mix of romantic cliches, nascent disaster elements, and adventurism ..." "It works, partly because Capra intermingles so much documentary-styled footage of the airship and Antarctic expedition."  

==See also==
*With Byrd at the South Pole (1929)
*The Lost Zeppelin (1929)

==References==
===Notes===
 
===Bibliography===
 
* Capra, Frank. Frank Capra, The Name Above the Title: An Autobiography. New York: The Macmillan Company, 1971. ISBN 0-306-80771-8.
* Joseph McBride (writer)|McBride, Joseph. Frank Capra: The Catastrophe of Success. New York: Touchstone Books, 1992. ISBN 0-671-79788-3.
* Scherle, Victor and William Levy. The Films of Frank Capra. Secaucus, New Jersey: The Citadel Press, 1977. ISBN 0-8065-0430-7.
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 