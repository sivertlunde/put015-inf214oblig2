Crude (2009 film)
 
{{Infobox film
| name           = Crude
| image          = Crude filmstill1.jpg
| alt            =  
| caption        = Film still
| director       = Joe Berlinger
| producer       = Joe Berlinger Michael Bonfiglio J.R. DeLeon Richard Stratton
| writer         = 
| screenplay     = 
| story          = 
| starring       = 
| music          = Wendy Blackstone
| cinematography = Pocho Alvarez Joe Berlinger Michael Bonfiglio Juan Diego Pérez
| editing        = Alyse Ardell Spiegel
| studio         = 
| distributor    = Entendre Films Radical Media Red Envelope Entertainment Third Eye Motion Picture First Run Features
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Crude is a 2009 American documentary film directed and produced by Joe Berlinger.  It follows a two-year portion of an ongoing class action lawsuit against the Chevron Corporation in Ecuador.

==Synopsis==
The film follows the progress during 2006 and 2007 of a $27 billion legal case brought against the Chevron Corporation following the drilling of the Lago Agrio oil field, a case described by activists as an “Amazon Chernobyl”. 
 Vanity Fair, Sting and his wife Trudie Styler, the power of multinational corporations, the shifting power in Ecuadorian politics, and rapidly disappearing indigenous cultures explored in the movie.

The film ends with a prediction the lawsuit will not be resolved for another decade or so unless an out of court settlement is arranged.

==Interviews==
* Juan Diego Perez
* Pocho Alvarez
* Joe Berlinger (producer of Crude)
* Michael Bonfiglio Sting (activist, artist and co-founder of Rainforest Foundation Fund)
* Trudie Styler (activist, producer, and co-founder of Rainforest Foundation Fund with her husband Sting)
* Adolfo Callejas (Ecuadorian lawyer on behalf of Chevron-Texaco)
* Steven Donziger (American lawyer on behalf of the plaintiffs)
* Pablo Fajardo (Ecuadorian lawyer on behalf of the plaintiffs)
* Diego Larrea (Ecuadorian Lawyer on behalf of Chevron-Texaco)
* Rafael Correa (President of the Republic of Ecuador)
* Sara McMillen (Chief Environmental Scientist for Chevron)
* Ricardo Reis Veiga (Corporate counsel for Chevron Latin America)

==Subpoena of footage== Lewis Kaplan sided with a petition submitted by Chevron and ruled that Berlinger turn over more than 600 hours of original footage created during the films production. Chevron had sought to subpoena the footage as part of the ongoing lawsuit discussed in the film. Berlingers legal team has maintained that the footage is protected by reporters privilege and appealed the courts decision. 

In his appeal, Berlinger noted that he was a highly awarded and respected independent filmmaker, that he was independent of Donziger, and argued that he had in fact gone to great lengths to make Crude a balanced portrayal.  Beringer acknowledged that he had “tweaked” some scenes at Donziger’s request.  The scene showing the environmental scientist at the legal strategy session had been cut from the theatrical release (but not the online version for Netflix) after one of the plaintiff lawyers objected to Berlinger that showing the scene was “so serious we could lose everything.” The US 2nd Circuit Court of Appeals narrowed the scope slightly (Berlinger had to turn over 500 hours of outtakes, rather than 600), but in 2011 upheld the lower court ruling against Berlinger, Judge Pierre Leval writing for the court: “Those who do not retain independence as to what they will publish but are subservient to the objectives of others who have a stake in what will be published have either a weaker privilege or none at all,”   

The evidence provided by the film outtakes played an important role in Chevron obtaining a 2014 US court ruling that the American lawyers for the plaintiffs had used fraud and corruption in obtaining the $19 billion Ecuadorian court judgment against Chevron. 

==Reception==

===Critical response===
Crude has been reviewed in the New York Times,  in LA Weekly,  AlterNet,  and by Howard Zinn. 

As of 23 March 2010, the movie review aggregator Rotten Tomatoes gave Crude a 95% "fresh" rating.

==Release==
Crude premiered on January 18, 2009 at the Sundance Film Festival and had its theatrical premiere on September 9, 2009, at the IFC Center in New York City. 

===Film festivals (partial list)===
* Sundance Film Festival, United States
* San Francisco International Film Festival, United States
* SILVERDOCS: AFI/Discovery Channel Documentary Festival, United States
* Sydney Film Festival, Australia
* One World Film Festival, Czech Republic	
* Thessaloniki Documentary Festival, Greece
* Independent Film Festival of Boston, United States
* Cleveland International Film Festival, United States
* True/False Film Festival, United States
* Nashville Film Festival, United States
* Newport Beach Film Festival, United States
* Little Rock Film Festival, United States
* Jacksonville Film Festival, United States
* Sarasota Film Festival, United States
* Lake Placid Film Festival, United States
* Sidewalk Moving Picture Festival, United States

==See also==
* Amazonia for Sale
* The Coconut Revolution
*   "Up to $16 billion fine against Chevron advised", April 8, 2008.

==References==
 

==External links==
*  
*   Sundance Channel
*   court order video report at Democracy Now!

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 