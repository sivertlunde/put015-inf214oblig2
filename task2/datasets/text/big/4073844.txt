These Old Broads
 
{{Infobox film
| name           = These Old Broads
| image          = TheseOldBroads-2001.jpg
| caption        = DVD cover
| director       = Matthew Diamond
| producer       =
| writer         = Carrie Fisher Elaine Pope
| narrator       =
| starring       = Shirley MacLaine Debbie Reynolds Joan Collins Elizabeth Taylor Jonathan Silverman
| music          =
| cinematography =
| editing        =
| distributor    = Sony Pictures Television
| released       =  
| runtime        = 100 minutes
| country        = United States English
| budget         =
| gross          =
}}
These Old Broads is a 2001 television film written by Carrie Fisher and starring her mother Debbie Reynolds, as well as Shirley MacLaine, Joan Collins, and Elizabeth Taylor in her final film role. In a 2001 BBC Omnibus_(UK_TV_series)|Omnibus documentary about Elizabeth Taylor, Shirley MacLaine says that Julie Andrews and Lauren Bacall were originally planned to be in the movie. The role of Miriam Hodges was originally offered to June Allyson. 

== Plot summary ==
Network television executive Gavin (Nestor Carbonell) hopes to reunite celebrated Hollywood stars Piper Grayson (Reynolds), Kate Westbourne (MacLaine), and Addie Holden (Collins) in a TV special after their 1960s movie musical Boy Crazy is re-released to wide public acclaim in the 1990s. Though the three women share the same agent, Beryl Mason (Taylor), Gavins seemingly insurmountable obstacle is that they all cannot stand each other.

== Cast ==
* Shirley MacLaine as Kate Westbourne
* Debbie Reynolds as Piper Grayson
* Joan Collins as Addie Holden
* Elizabeth Taylor as Beryl Mason
* Jonathan Silverman as Wesley Westbourne
* Pat Crawford Brown as Miriam Hodges (Addies mother)
* Nestor Carbonell as Gavin Peter Graves as Bill
* Gene Barry as Mr. Stern

==Notes==
 

==External links==
* 
* Scandals History for These Old Broadshttp://articles.latimes.com/2001/feb/12/entertainment/ca-24245

 

 
 
 
 
 
 