Prison (1988 film)
{{Infobox film
| name           = Prison
| image          = Prison1988film.jpg
| caption        = Poster
| director       = Renny Harlin
| producer       = Charles Band Irwin Yablans
| writer         = Irwin Yablans C. Courtney Joyner
| starring       = Viggo Mortensen Chelsea Field Lane Smith Lincoln Kilpatrick
| music          = Richard Band Christopher L. Stone
| cinematography = Mac Ahlberg
| editing        = Andy Horvitch
| distributor    = Empire Pictures 1988
| runtime        = 102 min
| country        = USA English
| budget         = $4,000,000  
| gross          = $354,704 (United States|US) 
| preceded_by    = 
| followed_by    = 
}}
Prison is a 1988 horror film starring Viggo Mortensen. It was filmed at the Old State Prison in Rawlins, Wyoming, with many of its residents on the cast and crew.

==Plot==

In 1964, inmate Charlie Forsythe swallowed 60,000 volts of electricity for a murder he did not commit.

When Creedmore Prison is reopened after thirty years, it has not been standing empty. Charlie Forsythe is back – still charged with electric heat. Waiting for Eaton Sharpe (Lane Smith) – the man who stood by as Forsythe rode the electric chair.

Forsythe quickly makes up for lost time as his vengeance rises to a fever pitch of violent fury. Burke (Viggo Mortensen) and the other inmates soon realize that they will all be slaughtered unless Forsythe is allowed to repay his long-standing debt. With the lives of Creedmore in the balance, Sharpe and Forsythe are finally brought face-to-rotting-face in a duel that will pit Forsythes supernatural rage against Sharpes bloodthirsty instinct for survival.


==Trivia==

The execution chamber shown in the film is actually the real Rawlins prison gas chamber. The chamber was never used for electrocutions in real life.

==Cast==
Prison Staff
* Chelsea Field as Katherine Walker
* Lane Smith as Warden Eaton Sharpe
* Arlen Dean Snyder as Captain Carl Horton
* Hal Landon Jr. as Wallace
* Matt Kanen as Johnson

Prisoners
* Viggo Mortensen as Burke/Charlie Forsythe
* Lincoln Kilpatrick as Cresus
* Tom Everett as Rabbitt
* Ivan Kane as Joe Lasagna Lazano
* André DeShields as Sandor
* Tommy Lister as Tiny Stephen Little as Rhino Reynolds
* Mickey Yablans as Brian Young
* Larry Jenkins as Hershey
* Kane Hodder as Charlie Forsythe Joseph Garcia as inmate getting hair cut

==Release==
The film was given a limited theatrical release in the United States by the Eden Distributing Company in March 1988.  It grossed $354,704 at the box office.   

The film was released in 1988 on VHS by New World Pictures. It had originally been released on DVD overseas, but not in the United States, save for bootlegs.  However, on February 19, 2013, Shout! Factory released the first official Blu-ray Disc and DVD and the first through their new subdivision Scream Factory. 

==External links==
* 
* 
*  

== References ==
 

 

 
 
 
 
 
 
 
 
 
 


 