Adam Surat
{{Infobox film
| name           = Adam Surat   The Inner Strength
| image          = File:Adam Surat (1989).jpg
| image_size     = 220px
| border         = 
| alt            = 
| caption        = Poster of Adam Surat
| film name      = 
| director       = Tareque Masud
| producer       = Tareque Masud
| writer         = 
| screenplay     = 
| story          =  Sheikh Mohammed Sultan (painter)
| narrator       = 
| starring       = SM Sultan
| music          = 
| cinematography = 
| editing        = Catherine Masud
| studio         = Audiovision
| distributor    = 
| released       =  
| runtime        = 54 minutes
| country        = Bangladesh Bengali English English
| budget         = 
| gross          = 
}}
 Sheikh Mohammed Sultan, directed by Tareque Masud.

== Synopsis ==

Adam Surat is the first film of Masud. It is a documentary about Bangladeshi painter Sheikh Mohammed Sultan (well known as "SM Sultan"). Masud started the film in 1982 and completed after seven years later. By that time, he had met and married the Chicago-born Catherine Shapere (well known as Catherine Masud), with whom he formed a close working relationship still death. 

== Digitized ==

The 54 minute documentary film was taken in 16&nbsp;mm film. The film had been converted into the digital format at a studio in New York. The film would soon be released all over Bangladesh except Dhaka. 

== References ==

 

== External links ==

* 

 