Working Girls (1931 film)
{{Infobox film
| name           = Working Girls
| image          = 
| alt            = 
| caption        = 
| director       = Dorothy Arzner
| producer       = 
| screenplay     = Zoë Akins
| starring       = Judith Wood Dorothy Hall Charles Buddy Rogers Paul Lukas Stuart Erwin Frances Dee
| music          = Ralph Rainger
| cinematography = Harry Fischbeck 
| editing        = Jane Loring 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Working Girls is a 1931 American drama film directed by Dorothy Arzner and written by Zoë Akins. The film stars Judith Wood, Dorothy Hall, Charles Buddy Rogers, Paul Lukas, Stuart Erwin and Frances Dee. The film was released on December 12, 1931, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Judith Wood as June Thorpe
*Dorothy Hall as Mae Thorpe
*Charles Buddy Rogers as Boyd Wheeler
*Paul Lukas as Dr. Joseph Von Schrader
*Stuart Erwin as Pat Kelly
*Frances Dee as Louise Adams
*Mary Forbes as Mrs. Johnstone
*Frances Moffett as Lou Hollings
*Claire Dodd as Jane
*Dorothy Stickney as Loretta
*Alberta Vaughn as Violet 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 