Venus Talk
{{Infobox film name           = Venus Talk  image          = File:Venus_Talk_poster.jpg director       = Kwon Chil-in producer       = Jaime Shim    Lee Eun writer         = Lee Soo-ah starring       = Uhm Jung-hwa   Moon So-ri   Jo Min-su  music          = Park In-yeong cinematography = Lee Hyeong-deok editing        = Kim Sang-beom   Kim Jae-beom studio         = Myung Films  distributor    = Lotte Entertainment released       =   runtime        = 109 minutes country        = South Korea language       = Korean
}}
 romantic dramedy is directed by Kwon Chil-in,  and the screenplay by Lee Soo-ah won the Grand Prize at the 1st Lotte Entertainment Script Contest.   It was released in theaters on February 13, 2014. 

==Plot==
Shin-hye (Uhm Jung-hwa) is a capable, successful TV producer, sometimes derided as a "gold miss" for still being single in her forties. Her protege-turned-longtime boyfriend, the current chief of their TV network, just left her for a younger woman who also happens to be Shin-hyes junior colleague.
 Lee Sung-min), and Hae-young (Jo Min-su), a soft-hearted, divorced single mother who wants her grown daughter Soo-jung (Jeon Hye-jin) to move out so she can have more time with her widowed boyfriend, carpenter Sung-jae (Lee Geung-young).

Soon, life gets better for the three friends. Shin-hye begins a thrilling relationship with a younger man (Lee Jae-yoon), another TV producer who is 17 years her junior; she tries to keep the relationship casual but cant stop herself from falling for him. Mi-yeon and Jae-ho enjoy a second honeymoon after their daughter leaves to study abroad and he starts taking Viagra. Soo-jung finally gets married upon her unexpected pregnancy and moves out of Hae-youngs home. But things take a turn when Hae-young is diagnosed with cancer, and Mi-yeon finds out her husband had an affair.  

==Cast==
*Uhm Jung-hwa as Jung Shin-hye    
*Moon So-ri as Jo Mi-yeon
*Jo Min-su as Lee Hae-young
*Lee Geung-young as Choi Sung-jae Lee Sung-min as Lee Jae-ho
*Lee Jae-yoon as Hwang Hyun-seung
*Jeon Hye-jin as Kim Soo-jung
*Choi Moo-sung as Lee Sung-wook 
*Kwon Hae-hyo as Representative Park 
*Kim Ho-jin as Gu Dong-wook 
*Jin Seon-gyu as Kim Dae-ri 
*Lee So-yoon as Lee Se-young 
*Seol Ji-yoon as Cafe owner
*Jang Hyuk-jin as Detective
*Kim Yong-jun as Dermatologist
*Kim Si-jeong as Internal medicine doctor
*Jo Woo-jin as PD Choi
*Jang Seo-yi as Masseuse
*BoA as Song Beom-sik (cameo appearance|cameo)

==Awards and nominations== 2014 Baeksang Arts Awards
*Nomination: Best Supporting Actor - Lee Geung-young

==References==
 

==External links==
*   
* 
* 
* 

 
 
 
 


 