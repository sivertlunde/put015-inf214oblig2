White Light/Black Rain: The Destruction of Hiroshima and Nagasaki
{{Infobox film
| name = White Light/Black Rain: The Destruction of Hiroshima and Nagasaki
| image = White Light Poster.jpg
| caption = The films poster.
| director = Steven Okazaki
| writer = Steven Okazaki
| producer = Steven Okazaki
| distributor = Home Box Office
| budget =
| released =  
| runtime = 86 minutes
| country = United States English
}}
 directed and produced by Steven Okazaki and was released on August 6, 2007, on HBO, marking the 62nd anniversary of the first atomic bombing. The film features interviews with fourteen Japanese survivors and four Americans involved in the 1945 atomic bombings of Hiroshima and Nagasaki.

== Interviews ==
=== Japanese survivors ===
  Japanese survivors of the bombings and collected over 100 interviews before settling on the fourteen subjects featured in the film. They were, in order of appearance, including age at the time of the bombings:
*Shigeko Sasamori, 13 years old. Sasamori came to the United States in 1955 to undergo reconstructive plastic surgery as part of a group of women called the Hiroshima Maidens.
*Keiji Nakazawa, 6 years old. Nazakawa lost most of his family in the bombing and later recounted his story in the Barefoot Gen series of comic books.
*Yasuyo Tanaka and Chiemi Oka, 9 and 10 years old. Tanaka and Oka were the only survivors among 20 children housed at a Catholic orphanage in Nagasaki.
*Sakue Shimohira, 10 years old. Shimohira survived along with her sister, but lost her mother and brother to the bombing. Her sister later committed suicide.
*Kyoko Imori, 11 years old. Imori and her friend were the only survivors out of 620 students attending a Hiroshima school, although her friend died a week later from radiation poisoning.
*Katsuji Yoshida, 13 years old. Yoshida incurred several injuries in the blast, including the right side of his face, which was disfigured by a severe burn.
*Sunao Tsuboi, 20 years old. At the time of the bombing, Tsuboi majored in science at a Hiroshima University.
*Shuntaro Hida, 28 years old. Military doctor who treated Hiroshima survivors after the bombing.
*Satoru Fukahori, 11 years old. Orphaned
*Pan Yeon Kim, 8 years old. Prior to the bombing her family immigrated to Japan from Korea to escape starvation.
*Etsuko Nagano, 16 years old. Nagano lost her brother and sister to the bombing.
*Senji Yamaguchi, 14 years old. During his lengthy hospitalization Yamaguchi started a survivors group to petition the Japanese government to provide medical care to victims of the bombings.
*Sumiteru Taniguchi, 16 years old. Taniguchi was a mail carrier and incurred heavy burns during the blast.

=== American personnel === Theodore "Dutch" Lawrence Johnston Los Alamos who claims to be the only person to have witnessed the Trinity test as well as the atomic bombings of Hiroshima and Nagasaki.

== Recognition == Best Documentary Producers Guild Awards and the Grand Jury Prize at the 2007 Sundance Film Festival. It did win the 2008 "Exceptional Merit in Nonfiction Filmmaking" Primetime Emmy Award.

== See also ==
*Atomic bombings of Hiroshima and Nagasaki
*Atomic Bomb Casualty Commission
*Hibakusha
*Hiroshima Peace Memorial
*Hiroshima (documentary)|Hiroshima (BBC documentary)

== References ==
 

== External links ==
*    (  from the original on 2010-01-09).
*  

 
 
 
 
 
 
 
 
 