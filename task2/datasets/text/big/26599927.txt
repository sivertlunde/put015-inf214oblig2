Utharaswayamvaram
 
{{Infobox film
| name           = Utharaswayamvaram
| image          = Utharaswayamvaram.jpg
| caption        = 
| director       = Remakanth Sarjju
| producer       = Santhosh Pavithram
| writer         = 
| narrator       =  Roma  Lalu Alex
| music          = M. Jayachandran
| cinematography =
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Utharaswayamvaram is a 2009 Malayalam film by Remakanth Sarjju starring Jayasurya and Roma (actress)|Roma. M. Jayachandran did the music for this film.

== Plot ==
The story is about Prakash (Jayasurya) whose father, Sreedhara Menon, runs a supermarket. Prakash’s father wants him to become an officer but Prakash’s interests lie elsewhere. After graduation, he is more interested in the activities of the village Arts club, than in looking for a job. He falls for a village girl, Uthara (Roma). But her rich father, Mahadevan fixes her marriage with another rich boy. The story builds up from here on.

Lots of twists and turns later, Prakashan marries his dream-girl, but their marriage doesnt last even for a day.

== Cast ==
* Jayasurya ..... Prakash Roma  ..... Uthara a.k.a. Ponnu
* Lalu Alex ..... Ponnuveettil Mahadevan
* Balachandra Menon ..... Sreedhara Kurup
* Lakshmipriya ..... 
* Sukumari
* Bijukuttan Janardhanan ..... Vasu
* Harisree Ashokan ..... Sarassappan
* Geetha Vijayan
* Indrans ..... Chellappan
* Sona Nair
*Narayanankutty
* Suraj Venjaramood ..... Pathalam Shaji Saikumar ..... Dr. Thomas Paul
* Sudheesh ..... Tony
* Appa Haja
* Shobha Mohan
* Kiran Raj
* Varadha ..... Prakashs sister

== References ==
 

== External links ==
* http://popcorn.oneindia.in/title/4925/utharaswayamvaram.html
* http://entertainment.oneindia.in/malayalam/top-stories/2009/utharaswayamvaram-jayasurya-100809.html
* http://www.cinecurry.com/movie/malayalam/utharaswayamvaram
* http://sify.com/movies/malayalam/review.php?id=14918559&ctid=5&cid=2428

 
 
 


 