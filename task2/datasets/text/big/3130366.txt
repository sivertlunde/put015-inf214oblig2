Blame It on Rio
{{Infobox film
| name           = Blame it on Rio
| image          = Blame It on Rio poster.jpg
| caption        = Theatrical release poster
| director       = Stanley Donen
| producer       = Stanley Donen Larry Gelbart  Robert E. Relyea
| writer         = Charlie Peters Larry Gelbart
| starring = {{Plainlist|
* Michael Caine
* Joseph Bologna
* Valerie Harper Michelle Johnson
* Demi Moore
}}
| music          = Kenneth Wannberg
| cinematography = Reynaldo Villalobos
| editing        = George Hively Richard Marden
| studio         = Sherwood Productions
| distributor    = 20th Century Fox 
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $18,561,998 (USA)
}}
 Michelle Johnson, Demi Moore, José Lewgoy and Valerie Harper.   The film was nominated for a Razzie Awards including Worst New Star for Johnson. 
 On the Town, Seven Brides for Seven Brothers and Charade (1963 film)|Charade.

== Plot ==
 
Michael Caine is Matthew, who is married to Valerie Harper as Karen, and has a daughter, Nikki, played by Demi Moore. His marriage is not going well for reasons not explained, certainly not by Karen, who lies in bed sighing in discontent, but passive-aggressively refuses to express whats wrong, except to issue snide answers like "Nothing. Thats whats always been going on in our marriage... nothing." She announces, on the moment they are to leave for a trip to Rio, that she is not going to Rio, but going on vacation by herself, to think. Victor (Joseph Bologna), Matthews boss, is going through a nasty divorce and constantly complaining about how his wife has consigned him to hours of paperwork and is taking half of what he has worked for.
 Michelle Johnson). The film presents lots of "fabulous vacation" type footage, and the fathers and daughters settle into a beautiful home. Jennifer and Nikki share a room, and before bed she says to Nikki "Your father is so sweet... I used to have a crush on him."

Throughout the film, we are interrupted every few minutes by interview segments in which Matthew or Jennifer speak directly to the camera. Matthew says that his wife is away thinking and an older friend says "Thinking? Thats how I lost my wife!" During a walk on the beach, Victor and Matthew pass numerous women walking around topless. The fathers spot their daughters in the distance, and as they wave to get their attention, the girls turn around to reveal that they too have taken off their bikini tops and are walking about with their breasts exposed. Jennifer and Nikki drag Victor into the water to play. Matthew does not join them, feeling uncomfortable with the partial nudity displayed by Jennifer, Nikki, and the other women on the beach.

After dropping the girls off at a Brazilian wedding, the men visit a local pub in search of dates. After Victor pairs off with a local cigar-smoking divorcée, Matthew winds up at a Brazilian wedding, where he runs into the amorous Jennifer. While watching the festivities together, Matthew and Jennifer start to feel some chemistry develop between them. The ceremony involves the guests stripping mostly naked to wander into the surf with the bride and groom, and Jennifer once again strips down to bikini bottoms and allows Matthew to see her exposed figure. Not surprisingly, they eventually share a passionate kiss – much to the chagrin of Nikki, who inadvertently sees whats going on between her father and her best friend. Jennifer is coming onto him very aggressively, breathing "make love to me." Then Matthew and Jennifer have sex on the beach, followed by some "comedy" as Matthew is almost caught by a couple of acquaintances and covers his naked body up with sand so they will not realize what he has been doing. Matthew stresses to Jennifer that it can never happen again.
 Polaroid of herself and then gives it to Matthew as they are riding in a cable car, where a Japanese tourist notices the photo and tries to take a picture of it.  Matthew mentions having held and kissed Jennifer as an infant.

Jennifer later tearfully confesses to Victor that she had an affair with an "older man", but does not reveal who he is. Naturally, Victor becomes furious and sets out to hunt down the mystery man, expecting Matthew to help – unaware, of course, that Matthew was the culprit the whole time. Matthew is reluctant to help Victor in his quest but goes through with it anyway, for the benefit of their friendship.

In the meantime, Matthew tries to talk Jennifer into ending their relationship, but she is determined to never give him up. Jennifer goes to a medicine woman to get a love spell, and is soon screaming "You love me!" to Matthew on a beach not far from her father. She gives him a small wooden idol she received from the medicine women, which has a massive erection. 

Eventually, Matthew discloses to Victor that it was he with whom Jennifer had the affair, but only after Victor caused a couple of brawls targeting innocent men. However, Victor is not as angry as Matthew expects, because it is soon discovered that Victor had also been having an affair--with Matthews wife Karen.

As the truth comes into light, Jennifer tries to commit suicide with an overdose of birth control pills, but survives. The incident brings everyone closer together, although the men constantly bicker with each other over each others sexual misconduct. Karen and Matthew decide to work on their marital problems, Jennifer begins dating a young male nurse whom she met while recuperating in the hospital, and Victor remarries his estranged wife.

== Cast ==
* Michael Caine as Matthew Hollis
* Joseph Bologna as Victor Lyons Michelle Johnson as Jennifer Lyons
* Demi Moore as Nicole (Nikki) Hollis
* Valerie Harper as Karen Hollis
* José Lewgoy as Eduardo Marques
* Lupe Gigliotti as Signora Botega
* Nelson Dantas as Doctor

== Production ==
The film was shot on location in Rio de Janeiro.    Johnson, who was 17 at the time of filming, received permission from a judge to film her nude scenes.

== References ==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 