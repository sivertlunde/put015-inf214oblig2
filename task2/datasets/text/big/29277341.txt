Escalier de service
{{Infobox film
| name           = Escalier de service
| image          = 
| caption        = 
| director       = Carlo Rim
| producer       = S.N.E.G Gaumont - Gaumont Actualité
| writer         = Carlo Rim
| starring       = Etchika Choureau Louis de Funès Marthe Mercadier
| music          = Georges Van Parys
| cinematography = 
| editing        = 
| distributor    = Gaumont
| released       = 16 August 1954 (France)
| runtime        = 94 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy Drama drama film from 1954, directed and written by Carlo Rim, starring Etchika Choureau and Louis de Funès. 

== Cast ==
* Etchika Choureau : Marie-Lou
* Danielle Darrieux : Béatrice Berthier
* Robert Lamoureux : François Berthier
* Sophie Desmarets : Madame Dumény
* Jean Richard : Jules Béchard
* Saturnin Fabre : Monsieur Delecluze
* Mischa Auer : Nicolas Pouchkoff
* Louis de Funès : Cesare Grimaldi, the father, Italian artist
* Marc Cassot : Benvenuto Grimaldi, the son, brilliant painter
* Anne Caprile : Carlotta Grimaldi, the daughter, prostitute
* Andréa Parisy : the second girl Grimaldi
* Marthe Mercadier : Hortense Van de Putte
* Anne Roudier : Madame Grimaldi
* Albert Michel : the sexton, friend of "Grimaldi"
* Fernand Sardou : Scarfatti, the conservative of du Louvre
* Claire Gérard : Victorine, the mechanical street-sweeper of Louvre
* René Hell : a sexton
* Sylvain Levignac : a guest in engagements
* Palmyre Levasseur : a guest in engagements
* Rudy Lenoir : inspector
* René Lefebvre-Bel : a restaurateur of pictures

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 
 


 