Going Back (film)
{{Infobox Film |
| name = Going Back |
| image = Goingback_dvdpic.jpg
| caption = Going Back DVD cover image, with Bruce Campbell (left) and Christopher Howe
| director = Ron Teachworth
| producer = Ron Teachworth Jill Teachworth
| writer = Ron Teachworth
| starring = Bruce Campbell Christopher Howe Perry Mallette Susan Waderlow Yamasaki Vern Teachworth
| distributor = Bifrost Distribution (DVD)
| released =  Vestron 1984 (United States|USA)
| runtime = 79 min.
| country = United States English
}}

Going Back is Bruce Campbells second feature film, produced shortly after The Evil Dead and released in 1983. The film had been extremely rare to acquire for a number of years, due to contract disputes between the director, producer, and the bankrupt original distributor. It was finally re-released on DVD in October 2006. The DVD release features an additional audio commentary track by Campbell, director Ron Teachworth, and cinematographer John Prusak.

== Plot summary ==
In 1964, two high school friends, Brice (Bruce Campbell) and Cleveland (Christopher Howe), leave their suburban neighborhood near Detroit, Michigan to Hitchhiking|hitch-hike their way to the countryside before going off to college. They are befriended by a lonely farmer, Jack Bodell (Perry Mallette), who offers them a place to stay. As days pass, Cleveland helps Jack around the farm and finds in him the father figure he lacks, while Brice falls in love with a local girl named Cindy (Susan Waderlow-Yamasaki). Four years later, Brice and Cleveland meet up in their senior year of college and decide to "go back" to Jacks farm, where they find much has changed in just a few years.

==Cast==
{| class="wikitable"
|-
! Actor
! Character
|-
| Bruce Campbell
| Brice Chapman
|-
| Christopher Howe
| Cleveland "Clee" Neal
|-
| Perry Mallette
| Jack Bodell
|-
| Susan Waderlow-Yamasaki
| Cindy
|-
| Vern Teachworth
| Cindys Father
|-
|}

== Film facts ==
* The movie was filmed in Cass City, Michigan, Rochester Hills, Michigan, and parts of Tennessee.
* Several of the stories told in the movie by the Brice and Jack characters actually happened to director Ron Teachworth.
* The mother kissing the Cleveland character good-bye at the beginning of the film is the actors actual mother, Noralee Howe.

==Reception==
Going Back received mixed reviews from critics and audiences. 

== External links ==
*  
*   - Contains synopsis, history of film, production stills, link to trailer
*   - All About Bruce Campbell
*   - About writer/director/artist Ron Teachworth

 
 
 
 
 
 
 