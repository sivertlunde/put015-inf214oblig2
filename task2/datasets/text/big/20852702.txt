Beyond Our Differences
{{Infobox Film
| name           = Beyond Our Differences
| image          =
| image_size     = 
| caption        = 
| director       = Peter Bisanz
| producer       = Vladimir Truschenkov, Tonko Soljan, Catherine Tatge, Karin Chien, Michael Griffiths
| writer         = 
| narrator       = Sue Hansen-Styles
| starring       = 
| music          = Anton Sanko
| cinematography = John Mans, Luke Geissbuhler, Daniel Marracino
| editing        = Tom Haneke, Lars Woodruffe
| distributor    = PBS
| released       = 2008
| runtime        = 74 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Beyond Our Differences is a documentary film aired December 26, 2008 on the Bill Moyers Journal on PBS. 

A production of New York documentary film company Entropy Films, Beyond Our Differences has been featured at film festivals including the Non-Violence International Film Festival, Global Peace Film Festival, West Hollywood International Film Festival, and many others. Entropy Films sold the 74 minute film to PBS and PBS International in late 2008 for worldwide distribution. 

The film features interviews with His Holiness The Dalai Lama, Archbishop Desmond Tutu, Professor Robert Thurman, Congressman Brian Baird, Former President of Iran Mohammad Khatami, Peter Gabriel, Paulo Coelho, Karen Armstrong, Ela Gandhi, Klaus Schwab, Sadhguru Jaggi Vasudev, Ambassador Andrew Young, Marianne Williamson, and many other politicians, kings, queens, heads of state, and social activists.

Beyond Our Differences was produced by Peter Bisanz, Tonko Soljan, Vladimir Trushchenkov and Catherine Tatge.

==Awards==
*Winner, Golden Eagle, CINE Golden Eagle, 2008
*Winner, Silver Remi, Worldfest Houston, 2008
*Winner, Best Spiritual Documentary, New York International Independent Film Festival, 2008
*Winner, Accolade Award, 2009
*Official Selection, Peace on Earth Film Festival, 2008
*Official Selection, Non-Violence International Film Festival, 2008
*Official Selection, Sun Valley Spiritual Film Festival, 2008
*Official Selection, Global Peace Film Festival, 2008
*Official Selection, Indianapolis International Film Festival, 2009
*Official Selection, West Hollywood International Film Festival, 2009
*Official Selection, DocuWest, 2009
*Official Selection, Atlanta International Documentary Film Festival, 2009

==External links==
*http://www.beyondourdifferences.com/ homepage
* 

 
 
 
 

 