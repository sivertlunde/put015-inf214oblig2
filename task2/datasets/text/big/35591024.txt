DoReMi
 
 
{{Infobox film
| name           = Do Re Mi (Donna Regine & Mikee)
| image          = Doremimovieposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Ike Jarlego, Jr.
| producer       = Vic del Rosario, Jr.,  Eric M. Cuatico
| writer         = Mel Mendoza-del Rosario
| starring       =  
| music          = Margot M. Gallardo
| cinematography = Ding Austria
| editing        = Marya Ignacio
| studio         =  Neo Films
| released       =  
| runtime        = 109 minutes
| country        = Philippines
| language       = Tagalog (Filipino) English
| budget         = 
| gross          =
}}
 Filipino Tagalog musical comedy Neo Films. It stars Donna Cruz, Regine Velasquez, and Mikee Cojuangco. The movie is considered to be the most successful Filipino musical comedy of all time. The films title is a play between the solfège and a portmanteau using the first two letters of Donna Cruz, Regine Velasquez, and Mikee Cojuangcos first names.
 
The film was the biggest musical comedy film released in the Philippines.

==Plot==
The film uses the concept of three female singers who, "as they sing their way through lifes ups and downs, they build a friendship strong enough to last a lifetime." 
 breadwinner of bar where they can perform to sustain their daily needs and expenses. Their successful stint as vocal performers consistently draw huge crowds, and their popularity in the area attracted several record executives to sign them for a recording contract.

On the day of the groups contract signing, both Donnette and Mikki back out from signing the record deal, leaving a disheartened Reggie as a solo act. As Reggie pursued a career as a successful solo singer, Donnette channeled her efforts to help orphans and other abused children in social welfare, and Mikki, started her own family, and food business with Toto (Gary Estrada), whom she met and fell in love with in the province. In a charitable gala where all three of them unknowingly participate in, the girls are brought together once more as they perform the song they co-wrote together.

==Planning and production==
According to a retrospective by ABS-CBN, the film "was originally titled Sanay Wala Nang Wakas 2," based loosely on the 1986 film starring Sharon Cuneta, Cherie Gil, and Dina Bonnevie. 

==Promotion== ABC5 for several interviews in various talk and variety shows. For the film and soundtracks press release, Cruz, Velasquez, and Cojuangco performed the songs in a promotional mini-concert.

Despite being part of the movie, Velasquez vocals were not part in recording of the movies soundtrack, as it was an then-upcoming VIVA Records singer named Krystine, who sang vocals on her part.  In the movie version, Velasquez was able to record her vocals just in time before the movie was released. 

==Reception==
The film was released to critical acclaim, with majority of the critics praising the chemistry and comic timing of Cruz, Velasquez and Cojuangco altogether, while still establishing character distinction as individuals.  The movie became the most successful Filipino musical comedy film of the nineties earning ₱170 million in its one month theatrical run. The song "I Can", which was the main theme of the movie, also became a number-one hit in the Philippines.

==Cast==

===Main Cast===
 
; Primary
* Donna Cruz as Donnette Legaspi
* Regine Velasquez as Reggie Mendoza
* Mikee Cojuangco as Mikki Tolentino
;Supporting Gloria Romero as Reggies Lola
* Gary Estrada as Toto
* Berting Labra as Mr. Tolentino
* Evangeline Pascual as Mrs. Legaspi
* Ramil Rodriguez as Mr. Legaspi
* Anthony Cortez as Julio
* Melisse Santiago as Nene
* Lee Robin Salazar as Joey
* Lorli Villanueva as Mrs. Tolentino
* Ricky Rivero as Boyet
* Gerard Faisan as Victor
* Carlos Ramirez I as Kambal I
* Carlos Ramirez II as Kambal II
* Elaine Kemuel as Dorothy
* Archie Adamos as Nenes Father
; Cameos
* Cesar Montano
* Ruby Rodriguez
* Menchu Macapagal
 

==Soundtrack==
{{Infobox album  
| Name        = DoReMi: Music from the Motion Picture
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       =
| Released    = 1996
| Recorded    =  Pop Dance-pop
| Length      =  Viva Records
| Producer    = Vic del Rosario, Jr.  Louie J. Ocampo  Margot M. Gallardo
| Reviews     = 
| Last album  = 
| This album  = 
| Next album  = 
| Misc        = 
}}
 Viva Records. All of the songs were produced by Margot M. Gallardo, with the albums main theme, "I Can," being co-produced by Louie M. Ocampo.

===Track listing===
{{Track listing
| extra_column    = Performer(s)
| writing_credits = yes
| title1          = I Can (Main Theme)
| writer1         = Louie M. Ocampo, Edith M. Gallardo
| extra1          = Donna Cruz, Krystine Marcaida, Mikee Cojuangco
| length1         = 3:34
| title2          = A Little Love Goes a Long Way
| writer2         = Moy Ortiz, Gallardo
| extra2          = Cruz, Marcaida, Cojuangco
| length2         = 3:10
| title3          = Sharing the Same Dreams
| writer3         = Ortiz, Gallardo
| extra3          = Cruz, Marcaida, Cojuangco
| length3         = 4:00
| title4          = Akalain Ko Ba
| writer4         = Babsie Molina, Gallardo
| extra4          = Cojuangco
| length4         = 3:31
| title5          = Stop! In the Name of Love
| writer5         = Holland–Dozier–Holland
| extra5          = Vanna Vanna
| length5         = 3:28
| title6          = I Can (Instrumental)
| writer6         = Ocampo
| extra6          = 
| length6         = 3:36
| title7          = Ive Got the Best in You
| writer7         = Vehnee Saturno
| extra7          = Cruz
| length7         = 4:10
| title8          = A Little Love Goes a Long Way (Instrumental)
| writer8         = Ortiz
| extra8          = 
| length8         = 3:14
| title9          = I Can (Karaoke Scene)
| writer9         = Ocampo, Gallardo
| extra9          = Cruz, Marcaida, Cojuangco
| length9         = 2:34
}}

==References==
 

== External links ==
*  

 
 
 
 
 
 