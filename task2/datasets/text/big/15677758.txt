Veerasamy
{{Infobox Film |
| name = Veerasamy
| image = 
| caption = T. Rajendar in Veerasamy
| director = T. Rajender
| lightsman = 
| Makeup man = 
| Choreography = T. Rajender
| Audiography = 
| Sound Engineering = 
| writer = T. Rajender
| starring = T. Rajender
| producer = T. Rajender
| music = T. Rajender
| lead singer = 
| choreographer = 
| special effects = 
| editor = 
| released = 2007
| runtime = 200 minutes  Tamil
| imdb_id =
}}

Veerasamy ( ) is a Tamil action film released in 2007. The film was fully made by actor and producer T. Rajendar. Mumtaj and Meghana Naidu starred in the movie alongside him.  

==Story==
Rajendar plays Veerasamy, a Member of the Legislative Assembly (MLA) who tries to do good things to people. His sister falls in love with a pimps brother, and a neighbor, played by Mumtaj, falls in love with Veerasamy. Veerasamy then has to handle family conflicts as well as working to win the local elections. 

Due to some false witness, Veerasamy is found guilty of involvement in corruption. On hearing this he dies of a heart attack. When he dies, Mumtaj also drops dead.

==Songs==
All the song tracks and lyrics for the songs were composed by T.Rajendar himself.

==Production==
The film began in late 2003 and progressed through production slowly.

==Soundtrack==
The music was composed by Vijaya T. Rajendar along with the lyrics. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Oru Vennilavu || Prasanna, Amrutha || Vijaya T. Rajendar || 05.41
|-
| 2 || Hallo Kadhal || Silambarasan, Prem Gi Amaren || Vijaya T. Rajendar || 03.17
|-
| 3 || Vadi En Kadhal || Manicka Vinayagam || Vijaya T. Rajendar || 05.07
|-
| 4 || Yelelo Veerasamy || Vijaya T. Rajendar || Vijaya T. Rajendar || 04.36
|-
| 5 || Madhana Kama Rasa || Mathangi || Vijaya T. Rajendar || 05.34
|-
| 6 || Sollaiye Kadhal || Vijaya T. Rajendar || Vijaya T. Rajendar || 03.37
|-
| 7 || Dai Dai Dai I Love you || Febi || Vijaya T. Rajendar || 04.12
|-
| 8 || Varamparu || Chandrakanth, Vijaya T. Rajendar || Vijaya T. Rajendar || 05.53
|-
| 9 || Marakka Mudiyale || Vijaya T. Rajendar || Vijaya T. Rajendar || 06.36
|-
| 10 || Entha Kadhal Pollathathu || Prasanna || Vijaya T. Rajendar || 05.07
|-
| 11 || Vachirukken || Vijaya T. Rajendar, Anuradha Sriram || Vijaya T. Rajendar || 06.22
|}

==References==
 

 

 
 
 