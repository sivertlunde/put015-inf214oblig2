The Right of the Unborn
{{Infobox film
| name           = The Right of the Unborn
| image          = 
| image_size     = 
| caption        = 
| director       = Adolf Trotz
| producer       = 
| writer         = Herbert Juttke   Georg C. Klaren   Ruth Schering
| narrator       = 
| starring       = Maly Delschaft   Elizza La Porta   Hans Adalbert Schlettow   Wolfgang Zilzer
| music          = Walter Ulfig
| editing        =
| cinematography = Robert Lach
| studio         = Detro-Film
| distributor    = Filmhaus Bruckmann
| released       = 7 June 1929
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent drama Weimar tradition of Enlightenment films. It examines the question of abortion of unborn children. Unlike several other German films of the era, it is generally anti-abortion.  The films art direction is by Hans Jacoby.

==Cast==
* Maly Delschaft as Elsa Lohrmann
* Elizza La Porta as Elli
* Hans Adalbert Schlettow as Rolf Stürmer
* Wolfgang Zilzer as Fredy
* Fritz Kampers as Peter Mahler
* Iwa Wanja as Anni, Fredys Freundin
* Robby Roberts as Fritzchen
* Curt Cappi as Frauenarzt Dr. Wehner
* Eva Speyer

==References==
 

==Bibliography==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910–1933. Berghahn Books, 2005.

==External links==
* 

 
 
 
 
 
 
 
 


 
 