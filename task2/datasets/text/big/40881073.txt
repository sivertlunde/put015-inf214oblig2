Nuts (2012 film)
{{Infobox film
| name = Nuts
| image =Ouf 2013 bbbbrtg.jpg
| caption = Film poster
| director = Yann Coridian
| producer = Tonie Marshall
| screenplay = Yann Coridian Sophie Fillières
| starring = Éric Elmosnino Sophie Quinton Valeria Golino 
| music = Lilly Wood and the Prick
| cinematography = Guillaume Deffontaines
| editing = Laurence Briaud
| studio = Tabo Tabo Films
| distributor = MK2 Diffusion
| released =  
| runtime = 85 minutes
| country = France
| language = French
| budget = 
| gross =
}}
Nuts ( ) is a 2012 French comedy film directed by Yann Coridian. Also known under the title Nuts, it stars Éric Elmosnino, Sophie Quinton and Valeria Golino. 

==Plot==
At the age of 41 François seems to be perfectly happy. He is married to his first and only love Anne and together they have two children. When a relatively minor incident triggers a tantrum, François finds himself soon in an asylum for the mentally insane. After his release he is shunned by Anne. Desperate to get Anne back he tries to redeem himself but his parents as well as his best friend are less supportive than he hoped. 

==Cast==
* Éric Elmosnino as François
* Sophie Quinton as Anna
* Valeria Golino as Giovanna
* Luis Rego as Françoiss father
* Evelyne Buyle as Françoiss mother
* Anémone as Dr. Vorov
* Brigitte Sy as Mme Herschel Luce as Solveig
* Michaël Abiteboul as Françoiss brother
* Suliane Brahim as Soraya
* Partha Pratim Majumder as Cadress
* Jean-Louis Coulloch as Abdel
* Marie Denarnaud as La copine dAnna 
* Gustave Kervern as Bertrand

==Production==
It is the first film directed by Yann Coridian. It is also the debut film of singer Luce (singer)|Luce.

==References==
 

==External links==
*  

 
 
 
 
 
 

 