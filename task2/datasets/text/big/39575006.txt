The Escape to Nice
{{Infobox film
| name           = The Escape to Nice
| image          = 
| image_size     = 
| caption        = 
| director       = James Bauer 
| producer       =
| writer         = Dolly Bruck (novel)   Walter Schlee   Walter Wassermann
| narrator       =  Fritz Fischer   Georg Alexander   Else Elster   Betty Bird
| music          = Franz Doelle
| editing        = Martha Dübber
| cinematography = Otto Kanturek   Bruno Timm
| studio         = Renaissance-Film 
| distributor    = 
| released       = 14 June 1932
| runtime        = 91 minutes
| country        = Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German comedy comedy crime Fritz Fischer, Georg Alexander and Else Elster. The film is based on the novel Orje Lehmann wird Detektiv by Dolly Bruck. It premiered on 14 June 1932. 

==Cast== Fritz Fischer as Hugo Lehmann 
* Georg Alexander as Fritz Butenschön 
* Else Elster as Irene Dippel 
* Gerhard Dammann as Ihr Vater 
* Betty Bird as Ramona Novalez 
* Hedwig Wangel as Ramonas Mutter 
* Max Gülstorff as Kommissar Grimm 
* Egon Brosig as Herr Harnisch 
* Theo Lingen as Herr Bock 
* Hermann Picha as Herr Fiedler 
* Philipp Manning as Der Inspektor 
* Angelo Ferrari   
* Erich Fiedler   
* Gerti Ober   
* Hans Ritter   
* Alexandra Schmitt   
* Lotte Steinhoff 

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 