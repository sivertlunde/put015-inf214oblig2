Badiaga
{{Infobox film
| name         = Badiaga
| director     = Jean-Pierre Dikongué Pipa
| producer     = Cameroun Spectacles
| writer       = Jean-Pierre Dikongué Pipa
| starring     = Justine Sengue  Alexandre Zanga
| music        =
| sound editor =
| photography  =
| editing      =
| distributor  = Marfilmes
| released     =  
| runtime      = 101 minutes
| country      = Cameroon
| language     = French and Pidgin
}}
 1987 drama film, directed by Jean-Pierre Dikongué Pipa.

==Synopsis==
Badiaga follows the rules of a classical tragedy: a three-year-old girl abandoned in a food market is sheltered and raised by a deaf and dumb vagrant. They develop a very strong bond. Badiaga dreams of becoming a famous singer and listens in total fascination to the artists who sing in the different cafes where she wanders.
One day she has the chance of singing on the radio a song which becomes a national hit. From that moment onwards she holds a nonstop succession of concerts. In love with her career, she refuses any romantic relations and searches desperately for her origins.
This story was inspired by the life of Beti Beti (Béatrice Kempeni),  a legendary Cameroon singer.

==Bibliography==
Mbaku, John Mukum, Culture and customs of Cameroon, Greenwood Press, 2005

==References==
 

==External links==
*  - IMDb page about Badiaga
*   on  All Africa
*  in Encyclocine
*  in Africultures
*  
* 

 
 
 


 
 