Naran (film)
{{Infobox film
| name           = Naran
| image          = Naran film.jpg
| caption        = 100 days poster
| writer         = Ranjan Pramod Innocent Madhu Madhu Siddique Siddique Jagathy Devayani Bhavana Bhavana Mamukoya Maniyanpilla Raju Joshi
| producer       = Antony Perumbavoor
| distributor    = Central Pictures
| studio         = Aashirvad Cinemas
| editing        = Ranjan Abraham 
| cinematography = Shaji Kumar
| released       =  
| runtime        = 165 minutes 
| country        = India
| language       = Malayalam
| music          = Songs:  
| budget         =      
| gross          =   
}}
Naran ( : The Man) is a 2005 Action Malayalam Film written by Ranjan Pramod and directed by Joshi (director)|Joshi. The film tells the story of Mullankolli Velayudhan (Mohanlal), a homeless ruffian with a good heart.  

It was one of the Onam releases in 2005.The film got positive reviews from the critics and overwhelming positive response from the audiences.It was one of the highest grossing movie of that year.

== Plot ==

Mullankolli is a rustic remote village in northern Kerala. Velayudhan (Mohanlal) is an orphan who came floating during floods and he was brought up by Valiya Nambiar (Madhu (actor)|Madhu) a do-gooder and a feudal landlord of the area.

Velayudhan grows up into a riff-raff, drunkard and a local rowdy with a good heart. He hates injustice and has set his own rules for the villagers who are scared of him including local moneylender and politician - Member Kurupu (Jagathy Sreekumar|Jagathy) and Gopinathan Nambiar (Siddique (actor)|Siddique), son-in-law of Valiya Nambiar who wants to rule over the village in his own way.

When Velayudhan plants a staff, it means that it has his stamp of authority. The only man who he listens to is Valiya Nambiar and to a certain extent Kelappan (Innocent (actor)|Innocent) who looked after him as a child. The villagers including Kurupu and Gopinathan regularly bring goondas from outside the village to eliminate Velayudhan but they fail. There are three women in his life - a prostitute Kunnummel Shantha (Sona Nair) old flame Janaki(Devayani) and Kelappans daughter Leela (Bhavana Menon|Bhavana). But things turn bleak for Velayudhan due to the machinations of Gopinathan which leads to the climax.

== Cast ==
* Mohanlal ... Mullankolli Velayudhan Madhu ... Valiya Nambiar Siddique ... Gopinathan Nambiar Innocent ... Kelappan
* Jagathy Sreekumar	... Member Kuruppu
* Bindu panicker...Narayani amma
* Mamukoya ... Ahammedikka Bhavana ...	Leela Saikumar ... Police Inspector
* Salim Kumar ... Idimutt Rajappan
* Bheeman Raghu ...	Keeri Raghavan
* V. K. Sreeraman ... Hajiyar
* Devayani ... Janaki
* Maniyanpilla Raju ... Krishnan
* Manikandan Pattambi
* Vijeesh ... Gopikuttan	
* Sona Nair ... Kunnummel Shantha
* Lakshmipriya
* Baby Niranjana ... Janakis Daughter
* Ottapalam Pappan

==Soundtrack==
{{Infobox album 
| Name        = Naran
| Type        = Soundtrack
| Artist      = Deepak Dev
| Cover       =
| Background  = Ouseppachan
| Recorded    = 
| Released    = 2005 
| Genre       = Soundtrack 
| Length      =  
| Label       = Manorama Music
| Producer    = Aashirvad Cinemas
| Reviews     = 
| Last album  = Naa Oopiri (Telugu)
| This album  = Naran Ben Johnson
|}}

The songs of this movie has composed by Deepak Dev and lyrics were penned by Kaithapram Damodaran Namboothiri|Kaithapram.

{| class="wikitable"
! Song !! Singers
|- Gayatri
|-
| Velmuruka || M. G. Sreekumar
|-
| Naran || K. S. Chithra, Vineeth Sreenivasan
|-
| Minnedi Minnedi || K. S. Chithra
|-
| Ponnaaryan || K. J. Yesudas|Dr. K. J. Yesudas
|- Gayatri
|-
| Velmuruka   || M. G. Sreekumar
|}

==Reception==
Naran released during Onam season and became a "super hit", collecting a distributor share of   in two weeks from Kerala.    Sify stated that the film as "Paisa Vasool" and wrote "Veteran director and his script writer Ranjan Pramod uses Mohanlals larger-than-life image and blends it with the milieu, successfully in this action adventure film laced with sentiments. It is old wine in a new tetra pack with hardly any story or twists but the great man simply lights up the screen with his sheer presence." 

==References==
 

== External links ==
*  

 

 
 
 
 


 