Amador (film)
{{Infobox film
| name           = Amador
| image          = Amador (movie poster).jpg
| alt            =  
| caption        = 
| director       = Fernando León de Aranoa
| producer       = Mediapro
| writer         = Fernando León de Aranoa
| starring       =  Magaly Solier Celso Bugallo Pietro Sibille Sonia Almarcha Fanny de Casto Antonio Durán Eleazar Ortiz Raquel Pérez Manolo Solo
| music          = Lucio Godoy
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 481.100 euros 
}}
Amador is a 2010 film directed by Fernando León de Aranoa.

==Plot==
Marcela, an immigrant woman with grave economic problems, finds a job as the caretaker of Amador, an old man who can not move from his bed. Day by day, Marcela earns money that she desperately needs while Amador enjoys company that his own family has denied him. Through this, they develop a special connection with each other. However, this connection is prematurely cut when Amador dies. Marcela, desperate to keep her job, finds herself in a difficult moral dilemma.  

==References==
 

==External links==
* 

 
 
 
 

 