The Jimmy Show
{{Infobox film
| name           = The Jimmy Show
| image          = 
| caption        = 
| director       = Frank Whaley
| producer       = Beni Tadd Atoori Mary Jane Skalski
| writer         = Frank Whaley (screenplay) Jonathan Marc Sherman (play)
| starring       = Frank Whaley Carla Gugino Ethan Hawke
| music          = Georg Brandl Egloff
| cinematography = Michael Mayers
| editing        = Miran Miosic
| distributor    = First Look Studios
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Jimmy Show is a 2001 drama written and directed by Frank Whaley, based on the Off-Broadway play Veins and Thumbtacks by Jonathan Marc Sherman.  The film stars Whaley, Carla Gugino, and Ethan Hawke.

==Synopsis==
The film centers on the life of Jimmy OBrien (Frank Whaley), a supermarket clerk by day and a standup comedian by night. He lives with his wife (Carla Gugino), daughter, and a disabled grandmother. Jimmys aspiration to succeed contrasts sharply with the numbness of his pothead friend Ray (Ethan Hawke), who is just trying to get by.

==Cast==
*Frank Whaley as Jimmy OBrien
*Carla Gugino as Annie
*Ethan Hawke as Ray
*Lynn Cohen as Ruth
*Jillian Stacom as Wendy
*Spelman M. Beaubrun as Claude
*Sheila Kay Davis as The social worker

==Filming locations==
*Staten Island - this being Whaleys second movie filmed here after his 1999 drama Joe the King. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 