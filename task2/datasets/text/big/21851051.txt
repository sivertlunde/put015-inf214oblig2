The Secrets of Jonathan Sperry
{{Infobox film
| name = The Secrets of Jonathan Sperry
| image = The Secrets of Jonathan Sperry.jpg
| caption =
| director = Rich Christiano
| producer = Rich Christiano Chad Gundersen
| writer = Dave Christiano Rich Christiano
| starring = Gavin MacLeod Jansen Panettiere Frankie Ryan Manriquez Robert Guillaume
| music = Jasper Randall
| cinematography = Philip Hurn
| editing = Jeffrey Lee Hollis
| studio = Five & Two Pictures
| distributor = Freestyle Releasing 
| released =  
| runtime = 96 minutes
| country = United States
| language = English
| budget = $1 million
| gross = $1,264,275
}}
The Secrets of Jonathan Sperry is a 2008 Christian film, released to theaters on September 18, 2009. It was directed by Rich Christiano, and the majority of the film was filmed in Holley, New York,  beginning August 18, 2007.  Its world premier was at the Merrimack Valley Christian Film Festival.   Gavin MacLeod, who also starred in The Love Boat and The Mary Tyler Moore Show plays the lead role in the film. 

== Plot ==
Dustin (Jansen Panettiere), Albert (Frankie Ryan Manriquez) and Mark (Allen Isaacson) are 12-year-old friends looking forward to Summer fun in 1970. Dustin likes a girl named Tanya (Bailey Garno), and so he wants to ask her on a date, but he is nervous because he has never asked anyone out before. Dustins main obstacle is the town Bullying|bully, Nick (Taylor Boggan), who also likes Tanya. When Dustin mows the lawn of 75-year-old Jonathan Sperry (Gavin MacLeod ), a man he has seen at church, the two become friends.

Mr. Sperry begins a Bible study with Dustin and his friends, and encourages them to be kind to Nick. Sperry also pays Dustin to mow the lawn of a stubborn, elderly neighbor, Mr. Barnes (Robert Guillaume), although Dustin is told not to let Mr. Barnes know the benefactor of this kindness. Throughout the summer, many other boys in the neighborhood get involved in the Bible study, including Nick, who is remorseful and turns from his bullying ways after a couple of encounters with Mr. Sperry. Meanwhile, Dustin finally gets the courage to tell Tanya how much he likes her, but instead of asking her to be his girlfriend, he suggests she start reading the Bible.

Then one day, Dustin rides his bicycle by Mr. Sperry’s house and discovers a crowd has gathered there and Mr. Sperry has died. After the funeral, Mr. Barnes visits Dustin to thank him for mowing his lawn, and to tell him he figured out Mr. Sperry was his motivation. Mr. Barnes tearfully calls Mr. Sperry a great example of a Christian man. After this visit, Dustin’s mother reveals the secret background between Mr. Sperry and Mr. Barnes. Mr. Sperry’s wife had died four years earlier in a car accident caused by Mr. Barnes who was drunk. Inspired by Mr. Sperry’s kindness after such heartbreak, Dustin decides to continue the Bible study with the neighborhood boys, repeating one of Mr. Sperry’s first lessons with Dustin and his two friends.

== Cast ==
 
*Gavin MacLeod as Jonathan Sperry
*Jansen Panettiere as Dustin
*Robert Guillaume as Mr. Barnes
*Frankie Ryan Manriquez as Albert
*Allen Isaacson as Mark
*Bailey Garno as Tanya 
*Taylor Boggan as Nick
*Mary Jean Bentley as Dustins Mother
*Joshua Montague as Extra at Sperrys funeral (uncredited)
*John Montague as Extra at Sperrys funeral (uncredited)
 

== Soundtrack ==
The soundtrack, composed by Jasper Randall, won Best Original Score in the 2009 San Antonio Independent Christian Film Festival. 

== Release ==
The Secrets of Jonathan Sperry was released theatrically on September 18, 2009 in 118 theaters. By November 2009, a total of 240 theaters had been booked,  each sponsored by a church or Christian group. Each group paid $2,000 to get the film into a theater, and were given back the investment if the film grossed $4,500 in their individual theatre. 

In its opening weekend, The Secrets of Jonathan Sperry made $258,400 domestically. It had strong box office holds for several weeks, accumulating $1,169,828 to date.  Randall Christy of the Gospel Station Network stated the film was number one at the Ada, Oklahoma box office over the November 6–8, 2009 weekend. 

=== Reception ===
Christian actor Kirk Cameron said, "The Secrets of Jonathan Sperry moved me with its message of eternity and how one faithful man can make a difference in the lives of many. I recommend this inspiring movie for any family to watch together."  Common Sense Media gave the film only two out of five stars, citing its "very thin story," "amateurish performances by the young actors," and "unquestioning point of view."   

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 ]
 
 