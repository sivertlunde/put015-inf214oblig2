The Devil's 8
{{Infobox film
| name           = The Devils 8
| image          = 
| image_size     =
| alt            =
| caption        = 
| director       = Burt Topper
| producer       = 
| writer         = John Milius Willard Huyck James Gordon White
| based on       = story by Larry Gordon
| narrator       = Fabian
| music          = 
| cinematography = 
| editing        = 
| studio         = American International Pictures
| distributor    = 
| released       = 1969
| runtime        = 
| country        = United States
| language       = English
| budget         =
| gross          = 
| preceded_by    =
| followed_by    =
}}

The Devils 8 is a 1969 film from American International Pictures. 

==Plot==
A Federal agent (Christopher George) recruits six convicts to bust a moonshine ring. 

==Cast==
*Christopher George as Faulkner
*Ralph Meeker as Burl
*Ron Rifkin as Martin Fabian as Sonny
*Leslie Parrish as Cissy
*Cliff Osmond as Bubba
*Larry Bishop as Chandler
*Tom Nardini as Billy Joe
*Robert DoQui as Reed

==Production==
This film is the first feature screenplay credit for John Milius and Willard Huyck, who had gotten a summer job working in the story department of AIP after studying at USC. Milius says they were given two weeks to write it and they did it in ten days. "I dont think we ever thought it was our best work. It was pretty good; it was funny... a lot of noise but not very good action."  Milius says the film was a deliberate attempt to copy The Dirty Dozen. "It was called The Devils 8 because they didnt have enough money for a full dozen." 

The cast included Larry Bishop, son of Joey Bishop, who had signed a five year contract with AIP. During filming the movie was known as Inferno Road. 

Shooting started 15 October 1968 and mostly took place at Pinecrest Camp in the San Bernardino Mountains outside Los Angeles. Stafford Signed for Topaz
Martin, Betty. Los Angeles Times (1923-Current File)   24 Sep 1968: f17.   Devils Eight Opening Citywide on Wednesday
Los Angeles Times (1923-Current File)   05 Apr 1969: b9.  

This was Fabians last film billed as "Fabian". After this movie he was known as "Fabian Forte". Fabian Makes It Legal--Its Fabian Forte
Los Angeles Times (1923-Current File)   07 June 1969: a9.  

==Reception==
The New York Times said "the carnage among these unshaven hard guys is continuous, as is the action, under rudimentary direction." 
==References==
  Patrick McGilligan, Uni of California 2006 p 274-316

==External links==
* 
 
 

 
 
 
 
 
 
 
 


 