Once Upon a Time in the Midlands
{{Infobox film
| name           = Once Upon a Time in the Midlands
| image          = Once_upon_a_time_in_the_midlands.jpg
| caption        = UK Theatrical release poster
| director       = Shane Meadows
| producer       = Andrea Calderwood
| writer         = Paul Fraser Shane Meadows
| starring       = Robert Carlyle Rhys Ifans Kathy Burke Ricky Tomlinson Shirley Henderson
| music          = John Lunn
| cinematography = Brian Tufano
| editing        = Peter Beston Trevor Waite Film Four
| released       =  
| runtime        = 104 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
Once Upon a Time in the Midlands is a 2002 British romantic comedy film written and directed by Shane Meadows, starring Robert Carlyle, Rhys Ifans, Kathy Burke, Ricky Tomlinson and Shirley Henderson. It is set in Nottingham in the northeast English Midlands|Midlands.

==Plot== Midlands to try to track him down. In the end, Shirley refuses to go with Jimmy and professes her love for Dek after Marlene refuses to have anything to do with Jimmy and is only ready to accept Dek as the father figure.

==Cast==
*Robert Carlyle – Jimmy, Carols foster brother, Shirleys ex-husband and Marlenes father
*Vanessa Feltz – Herself
*Ricky Tomlinson – Charlie, Carols estranged husband
*Kathy Burke – Carol, Jimmys foster sister
*Vicki Patterson – Audience Guest
*Shirley Henderson – Shirley, Jimmys ex-wife and Marlenes mother
*Finn Atkins – Marlene, Jimmy and Shirleys daughter
*Kelly Thresher – Donna, Carols daughter 
*Rhys Ifans – Dek, Shirleys boyfriend
*Andrew Shim – Donut, Donnas boyfriend
*Ryan Bruce	– Emerson, Carol and Charlies son and Lake and Donnas brother
*Eliot Otis Brown Walters – Lake, Carol and Charlies son and Emerson and Donnas brother
*Anthony Strachan – Jumbo (as Antony Strachan)
*David McKay – Dougy (as David Mckay)
*James Cosmo – Billy

==Production== Hamish Macbeth Cracker and third in The 51st State).

==Awards==
;Gijón International Film Festival 2002
*Nominated: Best Feature (Grand Prix Asturias) – Shane Meadows

==External links==
*  Sony Pictures
* . The Guardian|Guardian film of the week.

 

 
 
 
 
 
 
 
 
 
 