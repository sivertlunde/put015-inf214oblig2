Cigano (film)
 

{{Infobox film
| name           = Gypsy \ Cigano
| image          =
| caption        =
| director       = David Bonneville
| producer       = Bárbara Valentina Fernando Vendrell
| writer         = David Bonneville, Diego Rocha
| starring       = Jaime Freitas Tiago Aldeia
| music          = Buraka Som Sistema
| cinematography = Vasco Viana
| editing        = Mariana Gaivão
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = Portugal
| language       = Portuguese
| budget         =
| gross          =
}} RTP and produced by Fernando Vendrell.

The film tells us the story of a wealthy young man called Sebastian. He finds out he has a flat tyre and ends up accepting help from a Gypsy passer-by. In return Sebastian will have to give him a ride home... but they won’t reach their expected destination.

==Cast==
*Jaime Freitas
*Tiago Aldeia

==Festivals Selection Highlights==
* Shortcutz Xpress Viseu (March Award and Audience Award for Best Film)
* Shortcutz Lisboa (February Award for Best Film)
* CBA Awards (Best Short Film Nominee)
* CinEuphoria Awards (National Competition: Top Shorts of the Year; Best Actor Award for Tiago Aldeia; Best DOP Award for Vasco Viana. Nominee for Best Director, Best Screenplay, Best Costume Design, Best Make-up)
* Shortcutz Porto (January Award for Best Film)
* Festival Caminhos do Cinema Português (Best Supporting Actor Award for Jaime Freitas)
* Aesthetica Short Film Festival (ASFF)
* Izmir International Short Film Festival (Golden Cat Award for Best International Fiction)
* QueerLisboa18 International Film Festival (Best Short Film - Audience Award)
* Shortcutz Xpress Viseu (Fortnight Award and September Award for Best Film)
* Faro International Short Film Festival (Prize for Best Fiction Film)
* Festival der Nationen (Lenzing Award in Silver)
* Rhode Island Film Festival (Nominated)
* London ShortsOnTap (Jury Award - Best Film; Top 7 films of 2014)
* Toronto WildSounds Film Festival (Best Global Performance Award and Best Cinematography Award)
* Palm Springs International Film Festival (Live Action Award nominee)
* SXSW Film Festival (Grand Jury Award nominee)
* Tampere Film Festival (Official competition)
* Guadalajara Mexico Film Festival (Iberoamerican Shorts Competition)
* Cork Film Festival, Ireland
* Istambul Short Film Festival, Turkey
* Brest European Film Festival, France
* Adana Golden Boll Film Festival (Competition Section)
* Leuven International Short Film Festival (Nomination) 
* Adana Golden Boll International Film Festival (Nomination)
* Kolkata Shorts International Film Festival (Nomination: Best Film)
* Hamptons International Film Festival (1 of the 5 Golden Starfish Award nominees)
* Locarno International Film Festival (International premiére)
* Curtas Vila do Conde Short Film Festival (National premiére)
* Festival de Cannes Court Métrage, SFC market (Distinction Coup de Coeur des Programmateurs Internationaux)

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 


 