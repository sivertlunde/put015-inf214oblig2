Ulzana's Raid
{{Infobox film
| name           = Ulzanas Raid
| image          = UlzanasRaid.jpg
| caption        = Film poster
| director       = Robert Aldrich
| producer       = Carter De Haven Jr. Harold Hecht Burt Lancaster
| writer         = Alan Sharp
| starring       = Burt Lancaster Bruce Davison Richard Jaeckel Jorge Luke Joaquín Martínez
| music          = Frank De Vol
| cinematography = Joseph F. Biroc
| editing        = Michael Luciano
| studio         = Associates and Aldrich Co.
| distributor    = Music Corporation of America|MCA/Universal Pictures
| released       =  
| runtime        = 105 min. English
| budget         = $2.8 million Alain Silver and James Ursini, Whatever Happened to Robert Aldrich?, Limelight, 1995 p 284  gross = 414,559 admissions (France)   at Box Office Story 
}}
Ulzanas Raid is a 1972 revisionist Western starring Burt Lancaster, Richard Jaeckel, Bruce Davison and Joaquin Martinez. The film, which was filmed on location in Arizona, was directed by Robert Aldrich based on a script by Alan Sharp. Emanuel Levy summarizes the film, "Ulzanas Raid, one of the best Westerns of the 1970s, is also one of the most underestimated pictures of vet director Robert Aldrich, better known for his sci-fi and horror flicks, such as Kiss Me Deadly and What Ever Happened to Baby Jane." 
 Chiricahua Apaches against European settlers. nihilistic tone showing U.S. Cavalry|U.S. troops chasing an elusive but murderous enemy has been seen as allegorical to the United States participation in the Vietnam War. 

==Plot== San Carlos war party. Army Reconnaissance|scout Apache scout Ke-Ni-Tay (Luke). Ke-Ni-Tay knows Ulzana, as their wives are sisters.

The cavalry troop leaves Fort Lowell and soon finds evidence of the activities of the Apache war party. The film then focuses on the soldiers reality, facing a merciless enemy with far better local skills. The young officer, shocked and then hardened by the cruelty and harshness around him, struggles with his Christian conscience and view of humanity. MacIntosh and Ke-Ni-Tay attempt to outthink and outfight their enemies, while advising the lieutenant. DeBuin cautiously accepts their guidance though remaining mistrustful of the Apache scout. Ulzana and most of his men abandon their horses to be led circuitously by two other warriors in an attempt to tire the pursuers heavily loaded mounts. Ke-Ni-Tay notices that the trail is now of unladen horses, and Macintosh works out a plan that leads to the loss of the horses and the death of their two Apache escorts, who include Ulzanas son. The lieutenant prevents his men from mutilating the dead boy. 

The raiders attack a nearby farm, burning the homesteader to death and seizing two horses. McIntosh realizes that the remaining Apaches physically and psychologically need horses and will try to obtain them by raiding the troop. The woman of the burned-out farm, instead of being raped to death, has been left alive so that the cavalry will be forced to send her to the fort with an escort. By splitting the troop, Ulzana hopes to successfully attack the escort and seize its horses. McIntosh suggests a decoy plan to make Ulzana falsely believe that his tactics are successful.

Ulzanas warriors ambush the small escort detachment, obtaining all of its horses and killing the sergeant and his soldiers before DeBuin can arrive with the rest of his force. McIntosh is fatally wounded. Only the woman survives unharmed though now apparently crazed by her experiences. Ke-Ni-Tay scatters the captured horses as bugle calls from the cavalry ineptly alert the Apaches to DeBuins approach. Ulzana flees on foot as the remnants of his band are killed. Ke-Ni-Tay confronts him and shows him the Army bugle taken from the body of his son. Ulzana puts down his weapons and sings his death song before the Apache scout kills him. A corporal suggests that Ulzana, or at least his head, should be taken back to the fort. The lieutenant however orders him to be buried, a task that Ke-Ni-Tay insists on carrying out himself. MacIntosh knows that he will not survive the journey back to the fort, and chooses to stay behind to die alone.

==Cast==
*Burt Lancaster as McIntosh
*Bruce Davison as Lt. Garnett DeBuin
*Jorge Luke as Ke-Ni-Tay (army scout)
*Richard Jaeckel as Sergeant
*Joaquín Martínez as Ulzana (as Joaquin Martinez)
*Lloyd Bochner as Capt. Charles Gates
*Karl Swenson as Willy Rukeyser (settler)
*Douglass Watson as Maj. Cartwright (CO, Ft. Lowell)
*Dran Hamilton as Mrs. Riordan John Pearce as Corporal
*Gladys Holland as Mrs. Rukeyser
*Margaret Fairchild as Mrs. Abbie Ginsford
*Aimée Eccles as McIntoshs Indian woman (as Aimee Ecclés) Richard Bull as Ginsford (settler)
*Otto Reichow as Dutch Steegmeyer (Indian Agent, San Carlos Reservation)
*George Aguilar as Indian Brave

==Production notes==

The film was shot on location in the United States southeast of Tucson, Arizona at the Coronado National Forest and in Nogales, Arizona as well as the Valley of Fire State Park, Nevada.

There are two cuts of the film because Burt Lancaster helped to produce the movie. One version was edited under the supervision of Aldrich, the other by Lancaster. There are many subtle differences between the two although the overall running times are similar and most of the changes involve alterations of shots or lines of dialogue within scenes.

==See also==

*Vigilante film

==References==
 

==Further reading==
*  Encyclopedia entry that discusses Ulzanas Raid and other westerns in the context of the ongoing US-Vietnam war.
*  An extended review of Ulzanas Raid in the context of Aldrichs career.

==External links==
* 

 

 
 
 
 
 
 
 
 
 