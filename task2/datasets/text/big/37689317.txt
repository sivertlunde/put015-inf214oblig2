The Man Who Loved Cat Dancing (film)
{{Infobox film
| name           = The Man Who Loved Cat Dancing
| image          = Tmwlcd.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Richard C. Sarafian
| producer       = Eleanor Perry Martin Poll
| screenplay     = Eleanor Perry William W. Norton (uncredited)
| based on       =   
| narrator       = 
| starring       = Burt Reynolds Sarah Miles Lee J. Cobb
| music          = John Williams Michel Legrand
| cinematography = Harry Stradling Jr.
| editing        = Tom Rolf
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       = 28 June 1973 (USA)
| runtime        = 114 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $3,600,000 (US/ Canada rentals) 
}} 1973 film novel of the same name by Marilyn Durham.

==Plot== George Hamilton). Catherine is abducted by Dawes and Billy, but Grobart protects her from them.  Meanwhile, Lapchance (Lee J. Cobb), a veteran railroad detective with a posse, is on their trail for the train robbery.  Accompanying the posse is Catherines husband, an arrogant millionaire, who is obsessed with retrieving his wife, even though he knows that she does not love him. As Grobart and Catherine get to know each other, they find themselves falling in love, and despite his criminal past, she admires him for avenging the death of the woman he loved. Billy is killed by marauding Indians on the trail.  Dawes runs away during the raid.  Grobart kills the marauders in an intense battle.  He and Catherine continue their journey.  Catherine admits her feelings for Grobart and they make love.  Dawes attempts to ambush them for the money and is killed by Grobart for attempting to double cross them.  Grobart and Catherine travel to the Shoshone village where Grobart lived with Cat Dancing and their children.  Grobart and Catherine arrive at the village where his son and daughter have been raised by his late wifes brother.  He discovers that his children have bonded with his late wifes tribe and wish to remain there.  Grobart leaves Catherine and the railroad money at the village and departs not wanting to place her in further danger.  The posse arrives and retrieves the money.  Crocker insists on pursuing Grobart in order to kill him.  The posse spends the night at the village.  That evening Catherine and Grobarts son, Dream Speaker, leave to find Grobart.  Dream Speaker guides them to a cave in the hills where Grobart is camped out.  Grobart bids his son farewell and reunites with Catherine.  The following morning as they prepare to leave Crocker arrives and shoots Grobart from the tree line.  Grobart is wounded and Catherine rushes to his aid.  Catherine grabs Grobarts pistol from his holster and shoots Crocker dead as he charges them.  Lapchance orders his men to put Crockers body on a pack horse and leaves Grobart  Crockers horse.  Grobart pulls himself to his feet and Catherine and he embrace.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Burt Reynolds || Jay Grobart 
|-
| Sarah Miles || Catherine Crocker 
|-
| Lee J. Cobb || Harvey Lapchance 
|-
| Jack Warden || Dawes 
|- George Hamilton || Willard Crocker 
|-
| Bo Hopkins || Billy Bowen
|-
| Robert Donner || Dub 
|-
| Jay Silverheels || The Chief 
|- James Hampton || Jimmy
|}

==Production==
The movie was filmed on location in Bryce Canyon National Park, Utah; Gila Bend, Arizona; Kanab, Utah; and Old Tucson, Arizona.

During the shoot, Sarah Miles personal assistant, David A. Whiting, was found dead under mysterious circumstances in his hotel room. His death was ruled suicide; however, there were rumours of foul play.   accessed 19 June 2014  It was later revealed that Miles and Whiting had been having an affair, and this, together with the resulting publicity, contributed to the disintegration of her marriage to Robert Bolt. 

Original screenwriter Eleanor Perry later claimed much of her work was rewritten. 

==Critical reception==
Roger Greenspun of The New York Times did not care for the film:
 

In contrast, Charles Champlin of the Los Angeles Times liked the movie. In his review for the John Williams Web Page, he noted the complications in making the picture. He concluded his review: "In spite of the difficulties faced by the actors and filmmakers, The Man Who Loved Cat Dancing boasts gorgeous widescreen location photography, an interesting feminist spin on traditional western formulas — with Miles strong-willed Catherine Crocker an engaging screen presence throughout — and strong support from virtually the entire cast; in particular, the film proved once and for all that Burt Reynolds was capable of handling a straight dramatic role as well as a lightweight comic one." 


==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 