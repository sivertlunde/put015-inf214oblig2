The Story of an African Farm (film)
{{Infobox film
| name = The Story of an African Farm
| image = Africanfarmposter.jpg
| caption        = David Lister
| producer = Ross Garland Bonnie Rodini Cindy Rodkin
| writer = Thandi Brewer Bonnie Rodini Olive Schreiner (novel)
| starring =Richard E. Grant Armin Mueller-Stahl
| music = J.B. Arthur
| cinematography = Peter Tischhauser
| editing = Josh Galvin
| distributor = Freestyle Releasing (US)
| released =  
| runtime = 97 minutes
| country = South Africa
| language = English
}} David Lister novel by South African author Olive Schreiner.

==Plot==
The setting is a farm on the slopes of a Karoo Kopje, South Africa, during the 1870s. Fat Tant Sannie (Karin van der Laag) looks after her charges, the sweet Em (Anneke Weidemann) and the independent Lyndall (Kasha Kropinski), with a strict Biblical hand - it was Ems fathers dying wish. Gentle Otto (Armin Mueller-Stahl), the farm manager, runs the farm and cares for Waldo, his son. Waldo (Luke Gallant) is bright, and busy building a model of a sheep-shearing machine that he hopes will make them all rich. Things change when the sinister, eccentric Bonaparte Blenkins (Richard E. Grant) with bulbous nose and chimney pot hat arrives. Their childhood is disrupted by the bombastic Irishman who claims blood ties with Wellington and Queen Victoria and so gains uncanny influence over the girls gross stupid stepmother, Tant Sannie.

As the story of Lyndall, Em and Waldo unfolds to its touching end, we learn not merely of a backwater in colonial history, but of the whole human condition.
 Emily Brontës Wuthering Heights. Wildly controversial at publication (1883) because of its feminist sentiments, the story has remained a touching and often wickedly funny portrayal of life on a late Victorian farm in South Africa.

==References==
 

==External links==
*  Retrieved 2011-08-06
* 

 

 
 
 


 