UFO Abduction (film)
  found footage/thriller thriller film.

== Production ==
Ufo Abduction was written, directed, filmed, and produced by Dean Alioto, through IndieSyndicate Productions.       The film is a mockumentary similar in presentation to 1999s The Blair Witch Project, which presents the final recordings and last known whereabouts of a Connecticut family named the Van Heeses just before they are abducted by extraterrestrials.  
Dean Alioto produced the no budget film using $6,500.00 from the company IndieSyndicate Productions.   

== Release ==
The film had a limited release through Axiom Films. Created to appear as a genuine 1983 home video recording, the film is widely believed (erroneously) by Axiom Films, to depict a real alien abduction of a Connecticut family named "the McPhersons" as they celebrate their relatives 5th birthday.     The film is widely debated, despite evidence that the film is a work of fiction. 

== Remake ==
Dean Alioto and  . Dean Alioto has since discussed his two films. 

== Cast ==
The Van Heese family and aliens: 

* Tommy Giavocchini – Eric Van Heese
* Patrick Kelley – Jason Van Heese
* Shirly McCalla – Ma Van Heese 
* Stacey Shulman – Renee Reynolds 
* Christine Staples – Jamie Van Heese 
* Laura Tomas – Michelle Van Heese (the birthday girl) 
* Dean Alioto – Michael Van Heese (cinematographer) 
* Kay Parten – Alien 1 
* Ginny Kleker – Alien 2 
* Rose Schneider – Alien 3

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 