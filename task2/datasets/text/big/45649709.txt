The Gracie Allen Murder Case (film)
{{Infobox film
| name           = The Gracie Allen Murder Case
| image          = The Gracie Allen Murder Case poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Alfred E. Green
| producer       = George M. Arthur Alfred E. Green 
| screenplay     = Nat Perrin
| based on       =  
| starring       = Gracie Allen Warren William Ellen Drew Kent Taylor Judith Barrett Donald MacBride Jed Prouty
| music          = Gerard Carbonara Leo Shuken
| cinematography = Charles Lang
| editing        = Paul Weatherwax
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

The Gracie Allen Murder Case is a 1939 American mystery film directed by Alfred E. Green and written by Nat Perrin. The film stars Gracie Allen, Warren William, Ellen Drew, Kent Taylor, Judith Barrett, Donald MacBride and Jed Prouty. The film was released on June 2, 1939, by Paramount Pictures.  

==Plot==
 

==Cast==
*Gracie Allen as Gracie Allen
*Warren William as Philo Vance
*Ellen Drew as Ann Wilson
*Kent Taylor as Bill Brown
*Judith Barrett as Dixie Del Marr
*Donald MacBride as Dist. Atty. John Markham
*Jed Prouty as Uncle Ambrose
*Jerome Cowan as Daniel Mirche
*H. B. Warner as Richard Lawrence
*William Demarest as Police Sgt. Ernest Heath
*Sam Lee as Thug 
*Al Shaw as Thug
*Richard Denning as Fred
*Irving Bacon as Hotel Clerk 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 