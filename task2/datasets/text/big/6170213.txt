Main Khiladi Tu Anari
{{Infobox Film 
| name           = Main Khiladi Tu Anari 
| image          = Main Khiladi Tu Anari.jpg
| caption        = Theatrical release poster
| director       = Sameer Malkan 
| producer       = Champak Jain 
| story          = Sachin Bhowmick
| screenplay     = Sachin Bhowmick
| starring       = Akshay Kumar Saif Ali Khan Shilpa Shetty Raageshwari Shakti Kapoor Kader Khan
| music          = Anu Malik Akram Khan 
| editing        = Suresh Chaturvedi 
| distributor    = United Seven Creations
| released       = September 23, 1994
| language       = Hindi
| runtime        = 184 minutes
| country        = India
| budget         =  
| gross          =  
}} action comedy film directed by Sameer Malkan. The films screenplay is by Sachin Bhowmick. Starring Akshay Kumar, Saif Ali Khan and Shilpa Shetty in pivotal roles, the film went on to become one of the highest grossing movie of the year and was declared a "SUPER HIT" at the end of its theatrical run.  It was the second installment in the Khiladi (film series). It was the first Bollywood action film to be reviewed by martial arts critic Albert Valentin on KungFuCinemas. 

==Plot==

One of the most respected inspectors, Arjun Joglekar (Mukesh Khanna) is killed by drug dealer and gangster Goli (Shakti Kapoor). Mona (Shilpa Shetty), a cabaret singer and Golis mistress, agrees to testify against Goli and is placed into witness protection by Inspector Karan (Akshay Kumar), Arjuns younger brother, who aims to fight any injustice and avenge his brothers murder. When Goli finds out about Monas forthcoming testimonial which would expose his real identity, he tracks her down and kills her.

Deepak Kumar (Saif Ali Khan), who is the most romantic actor around, is frustrated with his roles and being type-caste as a romantic hero. He would like to do something different and bring some change and excitement to his dull and boring existence. To change his monotonous life, he gets drunk and drives around. He is arrested by the police, and brought to his rowdy producer. This is where he meets Karan and is very impressed with his assertiveness, courage, and honesty; Deepak would like to study his behaviour so that he can use this as a background for his next movie.

Karan meets Basanti who is looks identical to Mona. He promises to let Deepak hang around him if Deepak can get Basanti to act as Mona. Deepak trains Basanti and presents the new "Mona" to Karan. Karan places Basanti at the Moonlight Hotel as an amnesiac Mona, where she can report on Golis criminal activities. Thats how they uncover Goli eventually. While Karan and Basanti fall in love with each other, Deepak falls in love with Karans sister, Shivangi.

At the end, Karan avenges the death of his brother by killing Goli.

==Cast==

* Akshay Kumar as Karan Joglekar 
* Saif Ali Khan as Deepak Kumar 
* Shilpa Shetty as Mona / Basanti 
* Rageshwari as Shivangi 
* Shakti Kapoor as Goli 
* Kader Khan as Ram Lal / Commissioner 
* Johny Lever as Dhansukh 
* Mukesh Khanna as Arjun Joglekar
* Raveena Tandon (Special Appearance)

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
!#!!Song !! Singer(s)!! Notes
|-
| 1
| "Chura Ke Dil Mera"
| Kumar Sanu & Alka Yagnik
| Picturised on Akshay Kumar & Shilpa Shetty
|-
| 2
| "Zara Zara"
| Kumar Sanu & Alka Yagnik 
| Picturised on Saif Ali Khan & Rageshwari
|-
| 3
| "Zuban Khamosh"
| Kumar Sanu & Alka Yagnik 
| Picturised on Akshay Kumar & Shilpa Shetty 
|-
| 4
| "Lakhon Haseen"
| Asha Bhosle & Kumar Sanu
| Not included in the film.
|-  Akshay Kumar, Shilpa Shetty
| 5
| "Hoton Pe Tera Naam"
| Pankaj Udhas
|
|-
| 6
| "Dil Ka Darwaza"
| Alka Yagnik 
| Picturised on Shilpa Shetty 
|-
| 7
| "My Adorable Darling"
| Anu Malik & Alisha Chinai
| Picturised on Saif Ali Khan & Raveena Tandon
|-
| 8
| "Main Khiladi Tu Anari"
| Abhijeet Bhattacharya, Udit Narayan & Anu Malik
| Picturised on Akshay Kumar & Saif Ali Khan
|}

==References==
 

==External links==
* 
 

 
 
 
 