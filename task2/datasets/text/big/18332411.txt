Biola Tak Berdawai
 
{{Infobox film
| name = Biola Tak Berdawai
| image = Biola Tak Berdawai.jpg
| caption =
| director = Sekar Ayu Asmara
| producer = Seto Harjojudanto Sekar Ayu Asmara Nia Dinata Afi Shamara
| writer = Sekar Ayu Asmara
| starring = Ria Irawan Nicholas Saputra Jajang C. Noer
| music = Addie MS
| cinematography = German G. Mintapradja
| editing = Dewi S. Alibasah
| distributor = Warner Indonesia
| released =  
| runtime = 97 minutes
| country = Indonesia Indonesian
| budget =
}} Indonesian film Best Foreign Language Film.

==Plot==
Renjani (Ria Irawan), a former ballerina and rape victim who aborted the resulting foetus, and Wid (Jajang C. Noer), a doctor and daughter of a prostitute, are two women who volunteer at an orphanage for children with several disabilities in Yogyakarta. One day, while practicing her dancing, Renjani notes that Dewa (Dicky Lebrianto), a tiny eight-year-old with autism and brain damage, is responding. This convinces her that music therapy could promote healing. Renjani later asks Bhisma (Nicholas Saputra), a violinist that she met by chance, to assist him; they later fall in love. After Bhisma writes a song, "Biola Tak Berdawai", for Dewa, he prepares for a recital. Renjani and Dewa attend the recital, but Renjani faints and is rushed to the hospital; the cause is later discovered to be cervical cancer. After a week in a coma, Renjani dies; at her grave, Dewa plays "Biola Tak Berdawai" as Bhisma listens.

==Production==
Biola Tak Berdawai was directed by Sekar Ayu Asmara, a Jakarta-born self-taught director.  It was her directorial debut.  Asmara also produced the film and wrote the lyrics to the theme song. 

According to Asmara, Ria Irawan, Nicholas Saputra and Jajang C. Noer immediately accepted the offer to play in the film. However, the role of Dewa was filled by Dicky Lebrianto after casting in both Yogyakarta and Jakarta; she notes that it was difficult as the person who played him had to be a non-handicapped child capable of acting like he had a handicap. 

Addie MS, a musician cum conductor from the Twilite Orchestra and Asmaras long-time friend, was chosen to compose the films music.   The Victorian Philharmonic Orchestra in Melbourne, Australia, provided the music. 

The movie was shot in Yogyakarta and the surrounding area over a period of 25 days in 2002. 

==Themes==
According to Asmara, Biola Tak Berdawai is "a metaphor for the multiple- handicapped babies" that symbolizes the souls of people who have never known love, and those whose hearts have been scarred by it.   She also notes that the film conveys the message that one must be ready to sacrifice things for love. 

==Release==
Biola Tak Berdawai premiered on 22 March 2003,  with wider release on 4 April.  It garnered the Best New Director award at the Cairo International Film Festival for Asmara.  It later became Indonesias submission to the 76th Academy Awards for the Academy Award for Best Foreign Language Film. However, it was not accepted as a nominee.  The film was later novelised by Seno Gumira Ajidarma. 

Hera Diani of The Jakarta Post gave the movie two and a half out of four stars, writing that Noers character was "ridiculous", with "bad makeup, unnecessary histrionics and absurd statements on how life and everything in it are a riddle". Irawans character was described as "sometimes unconvincing". Regarding the film itself, she wrote that the dialogue was at times pat and some scenes were illogical. However, she praised the cinematography and score, as well as Lebriantos portrayal of Dewa. 

==See also==
*List of Indonesian submissions for the Academy Award for Best Foreign Language Film

==References==
;Footnotes
 

;Bibliography
 
*  |archiveurl=http://web.archive.org/web/20070516210221/http://www.kalyanashira.com/btb/notes.html |language=Indonesian |archivedate=16 May 2007}}
*  |date=26 October 2003 |accessdate=17 September 2011}}
*  |date=6 April 2003 |accessdate=17 September 2011}}
* {{cite book
| last = Imanjaya
| first = Ekky
| year = 2006
| title = A to Z about Indonesian film
| location = Bandung
| publisher = DAR! Mizan
| language = Indonesian
| isbn = 978-979-752-367-1
| url = http://books.google.ca/books?id=sl92GYNaKJIC 
| accessdate = 
| ref = harv
  }}
*  |archiveurl=http://web.archive.org/web/20070516154910/http://www.kalyanashira.com/btb/production.html |language=Indonesian|archivedate=16 May 2007}}
* |author=Stokoe, Sara|date=23 January 2004 |accessdate=17 September 2011}}
*  |archiveurl=http://web.archive.org/web/20070516154848/http://www.kalyanashira.com/btb/theatical.html |language=Indonesian |archivedate=16 May 2007}}
 

==External links==
* 
*  (in Indonesian)

 

 
 
 
 
 
 
 
 