Shrek the Third
 
 
{{Infobox film
| name           = Shrek the Third
| image          = Shrek the third ver2.jpg
| caption        = Theatrical release poster
| border         = yes Chris Miller Raman Hui
| producer       = Aron Warner Denise Nolan Cascino
| story          = Andrew Adamson
| based on       =  
| screenplay     = Jeffrey Price Peter S. Seaman Chris Miller Aron Warner
| starring       = Mike Myers Eddie Murphy Cameron Diaz Antonio Banderas Julie Andrews John Cleese
| music          = Harry Gregson-Williams
| editing        = Joyce Arrastia Michael Andrews
| studio         = DreamWorks Animation Pacific Data Images
| distributor    = Paramount Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $160 million   
| gross          = $799 million 
}}
 fantasy comedy DreamWorks Pictures first film). Although the film received mixed reviews from critics, it grossed $798 million, making it a commercial success.

It was produced with the working title of Shrek 3, the name being changed to avoid potential confusion with Shrek 4-D. Like the first two Shrek films, the film is based on fairy tale themes. It was nominated for Best Animated Movie at the 2008 Kids Choice Awards, but lost to Ratatouille (film)|Ratatouille. It was also nominated for the BAFTA Award for Best Animated Film at the 61st British Academy Film Awards. This film also pairs former Monty Python members Eric Idle and John Cleese for the first time since 1993s Splitting Heirs (Idle plays Merlin, Cleese plays King Harold).

==Plot==
   Donkey and Puss in Boots set out to retrieve Arthur; as they are sailing away, Fiona yells to Shrek that she is pregnant, leaving Shrek somewhat shocked.

The trio journey to Worcestershire Academy, an elite boarding school, where they discover Arthur ("Artie", as he prefers to be called) is a scrawny 16-year old underachiever picked on by everyone. At the school pep rally, Shrek tells Artie he is going to be king of Far Far Away. Artie is excited until Donkey and Puss inadvertently frighten him by discussing the responsibilities of being king. Artie tries taking control of the ship and crashes it on an island, where they meet Arties retired wizard teacher, Merlin (Shrek)|Merlin.

Charming and the other villains attack the castle, but Wolfie, Pinocchio, Gingy, the Three Little Pigs and the Blind Mice stall them long enough for Fiona and her mother Queen Lilian to escape along with Snow White, Rapunzel, Cinderella (Shrek)|Cinderella, Sleeping Beauty and Doris the Ugly Stepsister. Unfortunately, one of the Pigs accidentally blurts out that Shrek has gone off to bring in Arthur, and Prince Charming sends off Captain Hook and his pirates to track down Shrek. Even worse, the ladies are captured when Rapunzel betrays them because she is in love with Charming.

Captain Hook and his pirates track Shrek and his friends to Merlins island, where they attempt to capture Shrek alive and kill the others. Shrek and Artie send the villains running, but not before Hook mentions Charming and the takeover of Far Far Away. Concerned for his wife and future children, Shrek urges Artie to return to Worcestershire. Instead, Artie cons Merlin into using his magic to send them all to Far Far Away. The spell works, but accidentally causes Puss and Donkey to switch bodies. They find Charming and learn that he plans to kill Shrek in a play that night. Charmings men arrive, but Artie tricks the knights into not taking them into custody. They break into the castle during rehearsals for the play. Caught in Charmings dressing room, the four are taken captive.
 Dragon and Donkeys children. They encounter Artie, and Puss and Donkey explain to him that Shrek lied so Charming would not kill him.
 musical in which he rescues Rapunzel. Just as Charming is about to kill Shrek, Fiona, along with Puss, Donkey, the princesses and the fairy tale characters confront the villains. Artie convinces the villains to give up their evil ways, saying that just because they are being treated like losers doesnt mean that they have be losers.
 ogre triplets.

==Cast==
 
{{multiple image
| direction = vertical
| image1    = MikeMyersJune07.jpg
| width1    = 135
| image2    = CameronDiazJune07crop.jpg
| width2    = 135
| image3    = AntonioBandaresJune07 (crop).jpg
| width3    = 135
| image4    = JustinTimberlakeJune07 crop.jpg
| width4    = 135
| footer    = Mike Myers, Cameron Diaz, Antonio Banderas and Justin Timberlake at the films British premiere in London.
}}

===Main characters=== Shrek
* Eddie Murphy as Donkey (Shrek)|Donkey: Shreks friendly but garrulous sidekick.
* Cameron Diaz as Princess Fiona: Shreks wife. Puss in Boots: Another friend of Shrek and Donkey. Queen Lillian: Mother of Fiona. King Harold: Father of Fiona.

===Other characters=== Arthur "Artie" Pendragon: Fionas cousin and second heir to the throne of Far, Far, Away. Prince Charming: The villain who bands all the other fairy tale villains together to take over Far, Far, Away so he can be king.
* Eric Idle as Merlin (Shrek)|Merlin: An eccentric ex-teacher at Worcestershire Academy. Gingerbread Man, Headless Horseman Three Pigs, Ogre Baby and Bohort Doris the Ugly Stepsister The Three Blind Mice Snow White Rapunzel
* Cinderella
* Wolf
* Sleeping Beauty and Actress
* Ian McShane as Captain Hook Evil Queen Mabel the Ugly Stepsister
* Mark Valley as Cyclops Chris Miller Puppet Master
* Seth Rogen as Ship Captain
* Tom Kane as Guard #1
* Kari Wahlgren as Old Lady
* John Lithgow as Lord Farquaad (seen in a flashback by Gingy)
* John Krasinski as Lancelot

==Production==
Following the success of  , on board as a consultant.  Unlike the first two films, the film was not directed by Andrew Adamson; "No, I cant. Theyre actually working on the story right now. Im staying involved, but I cant do that and this." Adamson said. 

The film was originally going to be released in November 2006, however, in December 2004, the date was changed to May 2007; "The sheer magnitude of the Shrek franchise has led us to conclude that a May release date, with a DVD release around the holiday season, will enable us to best maximize performance and increase profitability, thereby generating enhanced asset value and better returns for our shareholders." Katzenberg said. 

==Reception==

===Critical reception===
Critical reaction to Shrek the Third was generally mixed, in contrast to the critical acclaim achieved by the previous films. On Rotten Tomatoes, it states that 40% of critics gave a positive review, with an average score of 5.4 out of 10, based on 205 reviews.  The film also has an average score of 58 out of 100 on Metacritic, based on 35 reviews, indicating "mixed or average reviews". 
 Live and Let Die. Er ... huh? Because its kind of sad and it has "die" in the title?"  The Times newspaper also rated it 2 out of 5. 

In contrast, writers such as A. O. Scott from The New York Times, held that the film "seem  at once more energetic and more relaxed  , less desperate to prove its cleverness and therefore to some extent, smarter." 

===Box office=== Harry Potter 44th highest-grossing 9th highest-grossing animated film. Compared to its predecessors and successor, the film also had an unusually short box office lifespan; Shrek the Third spent only 12 weeks in theaters, while Shrek, Shrek 2, and Shrek Forever After were in release for 29, 21, and 16 weeks, respectively. 

===Awards and nominations===
{| class="wikitable"
|-
! Awards
! Category
! Recipient
! Result
|- Annie Awards 
| Directing in an Animated Feature Production
| Chris Miller, Raman Hui
| rowspan=5  
|- BAFTA Awards 
|  Best Animated Film
|
|-
| Golden Reel Award 
| Best Sound Editing in Feature Film: Animated
|
|- Kids Choice Awards  
| Favorite Animated Movie
|
|-
| rowspan="3" | Favorite Voice From an Animated Movie
| Cameron Diaz
|-
| Eddie Murphy
|  
|-
| Mike Myers
|  
|-
| 34th Peoples Choice Awards|Peoples Choice Awards 
| Favorite Family Movie
|
|  
|-
| rowspan="2" | VES Awards 
| Outstanding Effects In An Animated Motion Picture
| Matt Baer, Greg Hart, Krzysztof Rost, Anthony Field
| rowspan=2  
|-
| Outstanding Performance by an Animated Character in an Animated Motion Picture
| John Cleese, Guillaume Aretos, Tim Cheung, Sean Mahoney
|}

==Soundtrack==
 

==Home media==
The film was released on both   to   ).  The HD DVD and DVD special features include several deleted scenes, features, trailers, commentary, music videos,   and exclusively on the HD DVD version, some web enabled and HDi Interactive Format features such as a special trivia track, a film guide, and an interactive coloring book which can be downloaded as of street date. 

The film and special features on the HD DVD version were presented in   widescreen high definition 1080p and feature a Dolby Digital Plus 5.1 audio soundtrack.  In addition, this film was released on Blu-ray Disc on September 16, 2008.  It was re-released on Blu-ray on August 30, 2011.  And on Blu-ray 3D on November 1, 2011 as a Best Buy exclusive. 

As of August 30, 2014, DVD sales gathered revenue of $176,661,204 from about 11,863,374 units sold. 

==Merchandise==
 
Many toys, games, books, clothes and other products have made their way to stores. For the first time, a Princess Fiona doll has been released, featured an Ogre face mask, and "Kung Fu" Leg action. Sleeping Beauty, Cinderella, Rapunzel and Snow White Dolls will also become available.
 A video game based on the film has been released for the Wii, PlayStation 2, Xbox 360, Game Boy Advance, PlayStation Portable, Microsoft Windows|PC, and Nintendo DS.

In May 2007, Shrek The Third was made into a mobile video game, developed by Gameloft. 

Shrek n Roll, an action puzzle game featuring licensed Shrek characters from the film was released for the Xbox 360 via Xbox Live Arcade on November 14, 2007.
A pinball machine based on the film has also been produced by Stern Pinball. 

==Controversy==
In the beginning of the film, in Prince Charmings dinner theater, coconuts are used for horses hoof beats. This same technique was used in Monty Python and the Holy Grail, which also starred John Cleese and Eric Idle. Idle claimed to be considering suing the producers of Shrek for the unauthorised use of this gag, while the producers claim they were honoring Idle and Cleese by putting the part in. 

==Satirical marketing effort==
Adult Swim comedy team Tim and Eric, annoyed by the volume of advertisement they had witnessed in the months approaching the release of the film, decided to independently "promote"  Shrek 3 in a series of internet videos 
as well as appearances on television and radio  to encourage people to see the film.

==Sequels== Puss in Boots was confirmed. The spin-off took place before the Shrek films.

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 