The Guy with the Secret Kung Fu
 
 
{{Infobox film
| name           = The Guy with the Secret Kung Fu
| director       = Joe Law
| producer       = Tan Siu-lin C.P. Keung
| writer         = Lie Ge-show
| narrator       = Meng Fei Sha-li Chen
| music          = Chou Fu-liang
| cinematography =
| editing        = Chiang I-hsiung
| distributor    = L & T Films Corporation Ting Ping Films Co. Mill Creek Entertainment
| released       = 1981
| runtime        = 86 mins
| country        = Hong Kong
| language       = Cantonese
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1981 Cinema Hong Kong martial art film. The film has passed into public domain and can be found on several martial arts film compilations produced by Mill Creek Entertainment. 

==Plot==
Two young freedom fighters, Hung Wen-ting and Hu Ah-piao, spend their days traveling around China and beating up evildoers.  Their sense of adventure is piqued when they are offered the chance to take down the Dragon Gang.  A first attempt to defeat the boss of the Dragon Gang, through a disguised entry into her bedchamber as a prospective husband, is foiled because the two brave souls are too young to take her on.  Later, the martial arts experts encounter an evil sorcerer and his very powerful demon, who has completely no brain.  Foiled by the demon, they come up with a new plan.  Accompanied by hoedown music, they set out for the Dragon Gangs lair a second time.  A deadly powder eliminates the demon, and the sorcerer is defeated by a toe to the torso.  Finally the showdown arrives.  One brother takes on the beautiful, cold boss of the Dragon Gang, while the other fights the secret Boss, trusted by the Emperor.  The final boss is defeated by skillful use of a coffin.

==External links==
*  IMDb database entry
* 

 
 
 
 

 
 