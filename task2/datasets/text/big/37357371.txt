To Market To Market (film)
 
{{Infobox film
| name           = To Market To Market
| image          = 
| image size     =
| caption        = 
| director       = Virginia Rouse
| producer       =
| writer         = Virginia Rouse
| based on = 
| narrator       =
| starring       = Philip Quast
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 1987
| runtime        = 
| country        = Australia English
| budget         = A $600,000 "Australian Productions Top $175 million", Cinema Papers, March 1986 p64 
| gross = 
| preceded by    =
| followed by    =
}}
To Market To Market is a 1987 Australian political film directed by Virginia Rouse who was once an assistant to Paul Cox. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p225 

The movie was made with funding from the Australian Film Commission and Film Victoria. 

==References==
 

==External links==
*  at IMDB
*  at Philip Quast Website

 
 
 

 