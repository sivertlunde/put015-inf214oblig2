Happy Days (2007 film)
 
 

 
{{Infobox film name = Happy Days image = Happy Days (2007 film).jpg writer = Sekhar Kammula starring = Varun Sandesh Tamanna Bhatia Nikhil Siddharth Sonia Deepti director = Sekhar Kammula producer = Sekhar Kammula distributor = Amigos Creations released = 28 September 2007  (United States)    October 2007  (India)  cinematography = Vijay C Kumar country = India Telugu Malayalam Malayalam
|music = Mickey J Meyer budget =   gross = 
}}
Happy Days  is a 2007  . http://www.idlebrain.com/news/2000march20/nandiawards2007.html  The Hindi remake of Happy Days will be directed by Sekhar himself,and it will be co-produced by Salman Khan.   

==Synopsis==
Happy Days tells us a story that explores the lives of eight friends through the four years of their engineering course. Each one of them joins Chaitanya Bharathi Institute of Technology with their own set of idiosyncrasies, eccentricities, beliefs and ideals. What starts off as a journey of individuals, slowly becomes a collective one over time. They bond over as they go through bullying seniors, stringent professors, intense examinations, over the top celebrations, love, betrayal, sacrifice and every other possible emotion that an individual can experience in a college. The movie portrays the mindset of a typical engineering student which helped youth to connect with the characters involved.

The film is a true representation of the wonderful & formative years in college. It depicts the stories of concerned parents and inspiring professors. It captures the raw energy in college going students. Full of dance, romance, music and masti… It has all the class rooms tiffs, mandatory fights, hot looking professors, love at first sights, crushes, break ups, first kisses, hostel humor, bikes, friendships and betrayals, birthday parties, prom nights, achievements, failures, exams, summers, picnics and parties, careers and true loves…. The film brings back the nostalgia of college days which are the most memorable days of everyones lives. 

==Cast==
* Varun Sandesh as Chandrasekar/Chandu
* Tamanna Bhatia as Madhumati/Madhu
* Nikhil Siddharth as Rajesh
* Gayatri Rao as Aparna (Appu)
* Raahul as Arjun (Tyson)
* Sonia Deepti as Shravanthi (Shravs)
* Vamsee Krishna as Shankar
* Monali Chowdhary as Sangeetha
* Gururaj Manepalli as College Principal 
* Kamalinee Mukherjee as Shreya Miss (Guest Appearance)
* Krishnudu
* Ranadheer Reddy as Naveen Vamshi as Paidithalli Aadarsh Balakrishna as Sanjay
* Adithya as Praveen
* Srikanth
* Nachiketh
* Satya
* Dileep
* Chaitanya Chitrada
* Kamal
* Praveen
* Suresh
* Krishna
* Sowjanya
* Priyanka
* Hima Bindu
* Deepthi
* Shuruthi as Prasanti

==Production==

===Casting===
Sekhar Kammula held a talent search at Big FM and idlebrain.com and managed to select 7 of the 8 lead actors. He had to cast   Video.

===Location=== Anand to the chairman of CBIT and its board members to convince them for an on-site shoot. Apparently, after watching Anand, the board granted their permission to shoot the film. 

===Cost cutting measures===
Sekhar Kammula cut costs of producing the film though various methods. By casting newcomers, he cut expenses to a reasonable level. As most of the shooting took place in the college, there was no need to construct sets. By partnering with Pantaloon Group, he saved another Rs 1&nbsp;million in the costumes budget. 

==Release==
Happy Days was released on 28 September 2007 in the United States and 2 October 2007 in India. In addition, it was dubbed into Malayalam. This film was super hit in Kerala too. This film is one of the highest grossing films in 2007 in Kerala.

==Response==
Though the film featured mostly new-comers, it became a commercial hit throughout India. It collected   180&nbsp;million (180 million) in 50 days.  The film generally received positive reviews.    

==Awards==
; Filmfare Awards South – 2008  Best film – Sekhar Kammula Best Director – Sekhar Kammula Best Supporting Actress – Sonia Deepti Best Music Director – Mickey J. Meyer Best Lyricist – Vanamali (Areyrey Areyrey) Best Male Karthik (Areyrey Areyrey)

; Nandi Awards – 2008  Best film (Silver) – Sekhar Kammula Best Music Director – Mickey J. Meyer Best Male Playback Singer – Karthik (Oh My Friend)

; CineMAA Awards – 2008
* Best Film – Sekhar Kammula
* Best Director – Sekhar Kammula
* Best Story – Sekhar Kammula
* Best Screenplay – Sekhar Kammula
* Best Music – Mickey J. Meyer
* Best Lyricist – Vanamali (Areyrey Areyrey)
* Best Lyricist – Veturi
* Best Male Singer – Karthik (Areyrey Areyrey)
* best actress - tamanna

==Music==
The film has seven songs composed by Mickey J. Meyer.

===Telugu=== Karthik
* Jill Jill Jinga – Krishna Chaitanya, Kranti, Aditya Siddharth & Sashi Kiran
* Happy Days Rock – Naresh Iyer & Mickey J. Meyer
* Ya Kundendu – Pranavi
* O My Friend – Karthik Ranjith & Sunitha Sarathy
* Happy Days – Harshika & Mickey J. Meyer

===Malayalam=== Karthik
* Unarukayaay – Vidhu Prathap, Chorus
* Sayanora – Manjari, Ranjith Govind
* Manassinnu Marayilla – Ajay Sathyan
* Vidachollan – Manjari, Shankar Mahadevan

==Trivia== Malayalam as Kannada as Tamil remake, Inidhu Inidhu,  was directed by noted cinematographer K V Guhan under actor Prakash Rajs Duet Movies banner, which flopped at the box-office due to its lack of richness and fresh feel. The first schedule was completed in Vellore Institute of Technology, Vellore (also called VIT University) and featured newcomers as protagonists. Mickey J. Meyer was retained as the music director and the project was his first venture in Tamil.

==References==
 

==External links==
*  

 
 
{{succession box
| title=Filmfare Best Film Award (Telugu)
| years=2007
| before=Bommarillu
| after=Gamyam}}
 
 
 

 
 
 
 
 