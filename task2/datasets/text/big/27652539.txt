Cedar Rapids (film)
{{Infobox film
| name           = Cedar Rapids
| image          = Cedar rapids film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Miguel Arteta
| producer       =  
| writer         = Phil Johnston
| starring       =  
| music          = Christophe Beck
| cinematography = Chuy Chávez
| editing        = Eric Kissack
| studio         = Ad Hominem Enterprises
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $6,861,102 (US) 
}} Black List, a Hollywood list of the most popular unproduced screenplays of the year.   

==Plot==
Naive and idealistic insurance agent Tim Lippe has led a sheltered life in the fictional town of Brown Valley, Wisconsin (north of Rhinelander, Wisconsin). At the beginning, he is shown welcoming a former teacher into his home, addressing her as Mrs. Vanderhei, and promptly having sex with her before reminiscing about his experiences as her student. In his position as an insurance agent, he is sent to represent his company (Brownstar Insurance) at a regional conference in Cedar Rapids, Iowa as a replacement for his co-worker, Roger Lemke, who dies in an Erotic asphyxiation|auto-erotic asphyxiation accident. Tim idolized Roger, believing that his death was merely an unfortunate accident, and that he embodied all it was to be decent, honest, caring to the community and most importantly Christian, as these are the key criteria used to judge the winner of the coveted "Two Diamonds" award, which Roger had won three years in a row. 

Tim is under pressure from his boss, Bill, to ensure they win again and keep the small company afloat. At the conference, Lippe meets fellow insurance agents Ronald Wilkes, Dean Ziegler from Stevens Point, Wisconsin and Joan Ostrowski-Fox, or "Ronimal", "Dean-Z" and "O-Fox" respectively. He also meets Bree, a sex worker who works the parking lot in front of the hotel. She affectionately calls him "Butterscotch" after he offers her a piece of butterscotch candy. Initially wary of almost everyone at the conference, he spends more time with Ron, Dean and Joan, and starts to develop genuine friendships and even a crush on Joan. All the insurance agents participate in a Scavenger Hunt, with Tim being paired with Joan, and although no one manages to complete the final task, they come farthest and thus win the contest and a gift card to a local restaurant. Tim, Joan and Dean become rather intoxicated through the night and end up in the hotel swimming pool, Tim and Joans sexual tension builds to a head and, after making out in the pool, they make love (later, in Joans hotel room).  Regrettably, they were all seen in the pool by ASMI president Orin Helgesson (Kurtwood Smith).

The next morning, Tim is guilt-ridden and calls his older girl friend (and ex-teacher) Macy to confess, before desperately asking her to marry him, she takes this opportunity to explain that as a recently divorced woman she just wants to have fun, so she too has been sleeping with other people and tells him that perhaps "its time for him to fly away from the nest and start a new life." Tim returns to Joan, who attempts to comfort him by telling him what Roger Lemke (the man he idolizes) was really like: that she was his lover but left him after his sexual appetites became a bit too twisted for her, and that he bribed Helgesson for each one of his Two Diamond awards. Tim refuses to believe this, and flees Joans room, accusing her of being a "prostitute" sent to destroy his life.

He runs into Dean and accidentally lets it slip that Lemke had bought all of his Two Diamonds. Dean swears to not tell anyone, it becoming apparent that he already sees Tim as a true friend. After some words of advice from Dean, Tim goes to Helgesson for his assessment: it does not go well, and under the pressure he ends up also bribing Helgesson for the award, leaving him penniless and ashamed. He later comes across Bree and accompanies her to a party, which ends up with Tim getting high on crystal methamphetamine and inadvertently starting a fight. Ron, Joan and Dean show up just in time to rescue him, as well as Bree who claims she is in love with Tim.  

The night ends with Bill appearing at Tims door to inform him that with the successful acquisition of another Two Diamond award, he has received a generous offer for the company and despite it meaning the branchs closure, he has chosen to sell. The day of the Diamond awards comes and while Bill is formally announcing the sale, Tim bursts in and takes over the podium, revealing that his company has unethically acquired the award every year by bribing Helgesson and confesses to doing so himself. Helgesson flees the room, his reputation in tatters, and a  furious Bill confronts Tim, his revelations having cost Bill the sale of his company. Tim responds by announcing his intentions to leave the company and start another with his clients from Brownstar, 17 of which have agreed to stay with him. Bill storms off, dumbfounded.

As the four friends say their goodbyes and prepare to see each other next year, we see Joan and Tim are happy as friends and Dean invites Ron and Tim to stay at a wealthy friends cabin in Canada for the summer, both of them surprising Dean by gleefully accepting, their shared experiences having clearly awarded genuine friendships and personal growth to them all.

It is revealed during the credits that the three of them went on to start their own company together called Top Notch, with Joan involved as well.

==Cast==
* Ed Helms as Tim Lippe
* John C. Reilly as Dean Ziegler
* Anne Heche as Joan Ostrowski-Fox
* Isiah Whitlock Jr. as Ronald Wilkes
* Stephen Root as Bill Krogstad
* Kurtwood Smith as Orin Helgesson
* Alia Shawkat as Bree
* Rob Corddry as Gary
* Mike OMalley as Mike Pyle
* Sigourney Weaver as Macy Vanderhei Thomas Lennon as Roger Lemke
* Inga R. Wilson as Gwen Lemke
* Welker White as Diane Krogstad
* Steve Blackwood as Lindy
* Lise Lacasse as Lila
* Mike Birbiglia as Trent

==Production== on location Cedar Rapids, Iowa, itself. {{Citation 
  | last = Nollen
  | first = Diana
  | title = ‘Cedar Rapids’ movie debuts tonight at Sundance
  | pages = 
  | newspaper = Cedar Rapids Gazette
  | location = 
  | date = 23 January 2011
  | url =http://thegazette.com/2011/01/23/cedar-rapids-movie-debuts-tonight-at-sundance/
  | accessdate =24 January 2011 }} 
Michigan provided a 42% tax rebate to movies filmed in specified "core communities" in the state (such as Ann Arbor). 

In the film, Ronald Wilkes (Isiah Whitlock, Jr.) is a self-described fan of the television series The Wire and does an impersonation of one of its most popular characters, Omar Little. Whitlock was involved in the HBO series portraying character Clay Davis, but has said the references to the show were written in before he was cast as Wilkes. {{Citation 
  | last = Rich
  | first = Katey
  | title = Interview: The Wires Isiah Whitlock Jr. Plays Against Type In Cedar Rapids
  | pages = 
  | newspaper = CinemaBlend.com
  | location = 
  | date = 10 February 2011
  | url =http://www.cinemablend.com/new/interview-the-wire-s-isiah-whitlock-jr-plays-against-type-in-cedar-rapids-23100.html
  | accessdate =21 February 2011 }}  Whitlock filmed a separate promotion for the film, where Wilkes is seen in an insurance office reading lines from The Wire.  , Fox Searchlight official channel via YouTube.com, Feb 7, 2011. 

== Reception==

=== Critical response ===
Reviews for Cedar Rapids have been mostly positive.   setting, but Cedar Rapids boasts a terrific cast and a script that deftly blends R-rated raunch and endearing sweetness."  At NoobMovies, the film has a score of a 9/10. 

At Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 70% based on 38 reviews. 

=== Box office===
As of August 22, 2011, the film grossed a total of $6,861,102 in the US.   

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 