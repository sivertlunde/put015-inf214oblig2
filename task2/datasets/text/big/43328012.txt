Goodbye, Franziska (1957 film)
{{Infobox film
| name =  Goodbye, Franziska
| image =
| image_size =
| caption =
| director = Wolfgang Liebeneiner
| producer =  Heinz Abel   Artur Brauner   Utz Utermann
| writer =  Heinrich Heining  (novel)   George Hurdalek   Curt Johannes   Helmut Käutner
| narrator =
| starring = Ruth Leuwerik   Carlos Thompson   Josef Meinrad   Friedrich Domin
| music = Franz Grothe   
| cinematography = Werner Krien  
| editing =  Margot von Schlieffen       CCC
| distributor = Gloria Film
| released = 5 September 1957 
| runtime = 105 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} film of the same name.

==Cast==
*    Ruth Leuwerik as Franziska  
* Carlos Thompson as Stefan Roloff 
* Josef Meinrad as Dr. Leitner  
* Friedrich Domin as Professor Thiemann  
* Jochen Brockmann as Mr. Blacky White  
* Nadja Regin as Helen Philipps 
* Gisela Trowe as Gusti  
* Siegfried Schürenberg as Harris  
* Peter Elsholtz as Anwalt 
* Else Ehser as Kathrin  

== References ==
 

== Bibliography ==
* Parish, James Robert. Film Actors Guide: Western Europe. Scarecrow Press, 1977. 

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 