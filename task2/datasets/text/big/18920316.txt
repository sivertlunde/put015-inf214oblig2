Yesterday's Enemy
 
 
{{Infobox film
  | name = Yesterdays Enemy
  | image = Yesenpos.jpg
  | caption = Original American release film poster
  | director = Val Guest
  | producer = Michael Carreras
  | writer = Peter R. Newman  Gordon Jackson
  | music = None Arthur Grant
  | distributor    = Columbia Pictures Hammer Films
  | country = United Kingdom
  | language = English
  | editing =
  | released = 1959
  | runtime = 95 min
}} Gordon Jackson Burma Campaign Gordon Jackson repeated his role from the BBC teleplay as Sgt. Ian Mackenzie.  Columbia Pictures co-produced the film with Hammer Films in an agreement for five co-production (filmmaking)|co-productions a year with Columbia providing half the finance.  The film was shot on indoor sets in black and white and Megascope.  The film has no musical score. Director Val Guest later said that Yesterdays Enemy was one of his films of which he was the most proud.  In 2013, film magazine Total Film included Yesterdays Enemy in their list of 50 Amazing Films Youve Probably Never Seen. 

The TV play was reportedly based on a war crime perpetrated by a British army captain in Burma in 1942. Marcus Hearn, The Hammer Vault, Titan Books, 2011 p. 28 

==Plot==
The lost remnants of a British Army Brigade headquarters make their way through the Burmese jungle, retreating from the Japanese. The group, numbering over thirty, is led by Captain Langford as the most senior officer, the Brigadier, is one of several who are wounded. Others in the group include nervous young Lieutenant Hastings, a civilian correspondent named Max along with the Doctor and the Padre, and Sgt McKenzie who is Langfords most reliable man. The exhausted group arrives at a small village which is enemy-occupied. After a short but fierce battle, the 10-strong group of Japanese in the village is wiped out, although several British and Burmese villagers are also killed. Amongst the Japanese dead is a full Colonel, an un-usually high-ranking officer to be with such a small group. The dead officer possesses a map with unknown markings. A Burmese man is caught trying to flee and a villager who speaks English says that the former does not belong there. The man is revealed to be an informer employed by the Japanese. Langford interrogates the man about the dead Colonel and the map and when the latter refuses to talk, he selects two adult males from amongst the villagers, saying he will have them both executed if the informer does not co-operate. The villagers plead for mercy and the Doctor, Max and the Padre angrily protest at Langfords decision but the Captain is un-moved. The two hostages are killed by Langfords men, prompting the informant to begin devulging what he knows. The dead Colonel was carrying a map on which are marked plans for a major Japanese flanking attack which aims to cut off the British army from its supply lines and leave it surrounded. Langford is anxious to send a warning back to British lines but the groups radio has been damaged.

Langford orders McKenzie to execute the informer and then announces that the British wounded are to be left behind so as not to impede the groups progress back to Allied territory. The Doctor, along with Max and the Padre are enraged by the decision but the dying Brigadier and the other wounded agree to remain in the village. A pair of enemy scouts approach the village, killing two of Langfords men. Mckenzie shoots one of them but the other escapes. Knowing that the Japanese are now aware of their location, Langford decides to send Sgt Mckenzie, the Doctor and two others to head back to British HQ to raise the alarm, thinking a smaller group will have a better chance of getting through whilst the remainder of the group will remain to defend the village and delay the enemy as long as possible. Langford offers Max and the Padre the chance to go with them but the latter both refuse, suggesting that another two men go in their place. Lt Hastings asks permission to go but Langford angrily refuses. Mckenzie, the Doctor and the other four men head for Allied lines but they are soon ambushed and all are killed. 

Langford takes a party of men out to ambush the approaching Japanese, leaving Hastings and the others to defend the village. The remaining Burmese evacuate, the English-speaking woman remarking bitterly to Hastings, Japanese, British- all the same. After a bloody engagement, Langfords group are all killed or captured. The enemy, using the POWs as a human shield, approach the village but Langford shouts at Hastings to open fire. Just before the village falls, the radio operators managed to send out a weak signal from the repaired set to alert British HQ of the enemys plans, although it is not made clear if the message gets through. The handful of surviving British, including Langford, Hastings, the Padre & Max, are now all POWs. The Japanese commander, Major Yamazaki, who speaks English, demands to know about the missing Colonel and the map, suspecting that Langford knows about the attack plans.

Yamazaki lines up all of the prisoners in front of a firing squad and informs Langford that unless he agrees to talk, the Major will order his troops to shoot them. Given just two minutes to make his choice, Langford bolts towards the transmitter in an attempt to signal HQ but he is shot dead. Impressed by Langfords courage, Yamazaki bows to his corpse, saying I would have done the same whilst outside, the Padre calmly leads the other prisoners in the Lords Prayer as they await their execution. The final image is a silent shot of the Btitish war memorial in Burma.

==Other information== Kohima epitaph:   the epitaph Simonides to honour the Spartans who fell at the Battle of Thermopylae in 480 BC. 

==Cast==
* Stanley Baker as Captain Langford  
* Guy Rolfe as Padre  
* Leo McKern as Max   Gordon Jackson as Sergeant MacKenzie  
* David Oxley as Doctor  
* Richard Pasco as 2nd Lieutenant Hastings 
* Philip Ahn as Yamazuki  
* Bryan Forbes as Dawson  
* Wolfe Morris as The informer  David Lodge as Perkins   Percy Herbert as Wilson
* Russell Waters as Brigadier
* Barry Lowe as Turner
* Burt Kwouk as Japanese Soldier 
* Timothy Bateson as Simpson (uncredited) 
* Edwina Carroll as Suni (uncredited)
* Alan Keith as Bendish (uncredited)
* Arthur Lovegrove as Patrick (uncredited)

==Critical Response==
Terence Pettigrew (writing in 1982) wrote "Yesterdays Enemy was criticised at the time for its depiction of British Army cruelty to the natives in a progressively desperate fight to survive. Nothing is done to soften the harshness of armed conflict on all concerned. and the film delivers its strong anti-war message without flinching from the task." 

Andrew Spicer (writing in 2001) wrote (Stanley) Bakers officer hero Langford in Yesterdays Enemy is no gentleman. Langfords dilemma is that he feels he must break the Geneva Convention and kill civilians in order to obtain the information that may save many lives. Langfords men dislike him, the padre and the liberal war correspondent denounce him, but they all know he is their only chance of survival. 
 Peckinpah anti-hero, hell commit war crime for the greater good of the operation.....but hell risk his life to save men hes never civil to. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 

 