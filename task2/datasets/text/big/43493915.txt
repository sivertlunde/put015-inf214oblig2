The Perfect Guy (2015 film)
{{Infobox film
| name           = The Perfect Guy
| image          = 
| alt            = 
| caption        =  David M. Rosenthal
| producer       = Tommy Oliver Darryl Taja 
| writer         = Tyger Williams Barbara Curry 
| starring       = Sanaa Lathan Michael Ealy Morris Chestnut
| music          = 
| cinematography = Peter Simonite   
| editing        =
| studio         = 
| distributor    = Screen Gems
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 David M. Rosenthal and written by Tyger Williams. The film stars Sanaa Lathan, Michael Ealy, Morris Chestnut, Charles S. Dutton, Rutina Wesley, Kathryn Morris and Holt McCallany. The film is scheduled to be released in North America on September 11, 2015. 

==Synopsis==
"Leah Vaughn (Sanaa Lathan|Lathan) appears to have the ideal life. She enjoys a challenging, fast-paced career as a lobbyist; Dave (Morris Chestnut|Chestnut), her longterm boyfriend loves her. And yet … at 36, shes ready to move to the next phase. Marriage and a family seem a logical and welcome step. Dave is not so sure. A bit commitment phobic, his misgivings lead to a painful break up. Enter Carter Duncan (Michael Ealy|Ealy), a handsome, charming stranger whose path keeps crossing with Leahs. Caring and solicitous of Leah and her family and friends, (Charles S. Dutton|Dutton, Rutina Wesley|Wesley, Kathryn Morris|Morris) their relationship rapidly progresses. It seems Leah has met The Perfect Guy. But if it seems too good to be true … soon his protective nature morphs into something more sinister. Its clear Leah has to end this new relationship and when she does, her onetime lover becomes her ultimate enemy. It will take every bit of her cunning and resolve to escape and outwit him." 

== Cast ==
*Sanaa Lathan as Leah Vaughn
*Michael Ealy as Carter Duncan
*Morris Chestnut as Dave
*Charles S. Dutton
*Rutina Wesley
*Kathryn Morris
*Holt McCallany

== Production == David wanted the film to be dark,” according to the films director of photography Peter Simonite. The Perfect Guy wrapped up its 34-day LA shoot in mid September 2014. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 