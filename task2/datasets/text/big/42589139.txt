Alien Abduction (2014 film)
{{Infobox film
| name           = Alien Abduction
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Matty Beckerman
| producer       = Matty Beckerman Cathy Beckerman Lawrence Bender
| writer         = Robert Lewis
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Katherine Sigismund, Corey Eid, Riley Polanski
| music          = Ben Weinman
| cinematography = Luke Geissbuhler
| editing        = Steve Mirkovich
| studio         = Exclusive Media Group, Big Picture, Next Entertainment
| distributor    = IFC Midnight
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} VOD on autistic 11-year-old boy who records his ordeal as an alien abductee.

==Plot==
The film begins with a dim, out-of-focus corridor with machinery sounds and screams, an opening disposal chute, and the camera falling back to Earth where it is later recovered by the US Air Force. Information on-screen then reveals the recording is being used to to review the footage for Project Blue Book...

Riley Morris keeps a video journal while on a family camping trip in Brown Mountain, North Carolina. On the last night of their trip, Riley is startled by flashes of light outside of the tent he shares with his older teenage brother and sister, Corey (Corey Eid) and Jillian (Jillian Clare). Riley wakes them and urges them to investigate. The siblings spot three distinct star-like objects in the sky which maneuver abruptly and vanish.   

The following morning, while driving down the mountain en route to the highway, Rileys father, Peter (Peter Holden) is sidetracked when their GPS mysteriously misdirects them to an isolated route. Rileys mother, Katie (Katherine Sigismund), senses something is wrong when they lose their cellphone signals. The family starts to panic when Peter announces the car is running low on gas. A crow unexpectedly falls besides the car, dead.

They reach a tunnel thinking it will lead to the highway but see it is blocked with recently abandoned vehicles - including a police car. Peter, Corey and Riley leave their car to investigate the tunnel while Katie stays behind with Jillian. Personal belongings are scattered around some of the cars and it appears the missing occupants were forcefully removed from their vehicles, which troubles Riley. Peter—who is further ahead—sees a silhouette at the end of the tunnel and calls out. Upon realizing the non-human nature of the Alien approaching figure, Peter instructs Corey to escape with Riley while he attempts to create a diversion. Peter becomes the first to be abducted by the Aliens.

The boys return to the car with the women, explaining what was happened. Suddenly a bundle of crows fall on their car, resulting in the car dying. They hear a blaring sound and run in fear. They retrace their route back to a cabin they recall passing. The cabins owner, Sean (Jeff Bowser), is a rustic recluse who is armed and initially hostile towards the family. He changes his mind when he hears his guard dog being attacked by an alien that followed them from the tunnel. Sean and the family barricade themselves for the evening. Sean says that the Brown Mountain Lights and abductions have been a local recurrence for centuries. And, upon hearing a radio message from his missing brother, Sean goes out to fight the Aliens.

The cabin is eventually breached and in an effort to save his family, Corey hides the others in the cellar, blocking the door – he is, however, quickly abducted by the aliens. The remaining members of the family are soon saved by a returned Sean who takes them to his truck, planning to take them back to town down the mountain. His plan is a quick failures as the lights, and thus the aliens, find them and in a last effort attempt at saving the family, he tells them to head to a nearby barn and take refuge. 

Sean mysteriously returns back to them after a dramatic escape and the group, believing theyre safe, attempt to leave the barn – Katie, however is killed and abducted, along with Sean. Riley and Jillian take refuge deep in the woods for the night, hiding in silence and in the dark from close-by aliens. 

Climbing down the mountain at dawn, they see a town in the valley below and soon find a road they are confident will lead back to civilization but it leads back to the tunnel where their father was abducted, instead. Jillian collapses in despair, at the same moment a police car arrives (to search for the prior missing police car). It doesn’t take long for the aliens to abduct the policeman, Jillian, and Riley, who is still clinging to the camera – the remaining footage shows a rapid ascension into orbit, and an in-time repeat of the same footage from the beginning of the film. Two men in respirators and Hazmat suits are seen taking the camera away in a van from the US Air Force.

In a mid-credits scene, Peter Morris is found one year later by a North Carolina police officer. He is huddling on a bridge, naked, dishevelled and in a state of shock. The fate of the rest of the family is uncertain, though comments made by the police officer on his radio suggest that Peter is not the only member of the family to have been found.

==Cast==
*Katherine Sigismund as Katie Morris
*Corey Eid as Corey Morris
*Riley Polanski as Riley Morris
*Jillian Clare as Jillian Morris
*Jeff Bowser as Sean
*Peter Holden as Peter Morris
*Jordan Turchin as Officer James
*Kelley Hinman as Park Ranger

==Production== Burke County, Avery County, Watauga County, Bryson City. 

==Reception==
Critical reception for Alien Abduction has been negative; the film holds a rating of 47 on Metacritic (based on 9 reviews) and 25% on Rotten Tomatoes (based on 14 reviews).   Common praise for the film centered upon Rileys use of his camcorder as a coping mechanism for an autistic boy, as reviewers felt that it gave a good rationale for Riley to continue filming "when any sane person would stop shooting and start running".   Criticism for the movie centered upon what the reviewers saw as an overly generic plotline and an overabundance of jump scares.   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 