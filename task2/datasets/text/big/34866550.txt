Charlie at the Sydney Show
 
 
{{Infobox film
| name = Charlie at the Sydney Show
| image =
| caption = John Gavin
| producer =
| writer = Agnes Gavin
| based on =
| narrator =
| starring = Ern Vockler Arthur Tauchert
| cinematography = Alf Moulton
| editing =
| distributor =
| studio =
| released =  
| runtime = 25 minutes
| country = Australia
| language = Silent film
}}
Charlie at the Sydney Show is a 1916 Australian short film starring Charlie Chaplin impersonator Ern Vockler. 

Unlike many Australian films from the silent era, a copy of this survives almost in its entirety. 

==Plot==
A series of adventures happen to "Charlie" as he visits the Sydney Royal Easter Show: he chases a couple of larrikins who have picked the pocket of a man visiting the show, encounters various side-show acts, fights a boxing lady, meets a confidence man, and chases girls. The movie ends with a chance and Charlie driving off with a couple in a motor car. 

==Production==
The movie was shot at the Sydney show. It contains the first film appearance of Arthur Tauchert, who was almost killed in an accident during filming. 

==Reception==
The critic from Punch praised the film saying:
 Mr. Gavin had unearthed a comedian of undoubted talent, who, with only slightly more experience in "Movie" acting, will soon eclipse the majority of the efforts, of comedians playing in imported films, and even aspire to be the equal of the great Charlie Chaplin himself. Mr. Gavin is to he congratulated on giving the public a decided novelty in films, and also on the extraordinarily heavy bookings he had already received for the exhibition of the picture in the N.S.W. shows. The scenario... contains many humorous situations... whilst the photo graphy... is quite equal to anything yet screened in Sydney.  

==References==
 

==External links==
*   at IMDB

 

 


 