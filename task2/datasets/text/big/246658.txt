Bwana Devil
{{Infobox film
| name           = Bwana Devil
| image          = Bwanadevil3.jpg
| caption        = Theatrical release poster
| director       = Arch Oboler
| producer       = Producer: Arch Oboler Associate producer: Sidney W. Pink
| writer         = Arch Oboler
| starring       = Robert Stack Barbara Britton Nigel Bruce Ramsay Hill Paul McVey Hope Miller
| music          = Gordon Jenkins
| cinematography = Joseph F. Biroc
| editing        = John Hoffman
| distributor    = Limited release: Arch Oboler Productions Wide release: United Artists
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = $323,000 NEW OBOLER FILM TO OPEN TUESDAY: 1 PLUS 1 IS THE WRITERS FIRST MOVIE IN NINE YEARS
By HOWARD THOMPSON. New York Times (1923–Current file)   04 Nov 1961: 14. 
| gross = $5 million 
}}

Bwana Devil is a 1952 U.S. adventure film based on the true story of the Tsavo maneaters and filmed with the Natural Vision 3-D system. The film is notable for sparking the first 3-D film craze in the motion picture industry, as well as for being the first 3-D feature film entirely in color and the first 3-D sound feature in English. Bwana Devil was written, directed and produced by Arch Oboler and stars Robert Stack, Barbara Britton and Nigel Bruce.  

The advertising tagline was: The Miracle of the Age!!!  A LION in your lap!  A LOVER in your arms!

==Plot==
The film is set in British East Africa in the early 20th century. Thousands of workers are building the Uganda Railway, Africas first railroad, and intense heat and sickness make it a formidable task. Two men in charge of the mission are Jack Hayward and Dr. Angus Ross. A pair of man-eating lions are on the loose and completely disrupt the undertaking. Hayward desperately attempts to overcome the situation, but the slaughter continues.

Britain sends three big-game hunters to kill the lions. With them comes Jacks wife. After the game hunters are killed by the lions, Jack sets out once and for all to kill them.  A grim battle between Jack and the lions endangers both Jack and his wife.  Jack kills the lions and proves he is not a weakling.

==Historic background==
The plot was based on a well-known historical event, that of the Tsavo maneaters, in which many workers building the Uganda Railway were killed by lions. These incidents were also the basis for the book The Man-eaters of Tsavo (1907), the true story of the events written by John Henry Patterson (author)|Lt. Col. J. H. Patterson, the British engineer who killed the animals. The story was also the basis for the film The Ghost and the Darkness (1996) with Michael Douglas and Val Kilmer.

==Cast==
{|class=wikitable
! Role !! Actor
|- Robert Stack 
|- Nigel Bruce
|- Barbara Britton 
|- Ramsay Hill
|- Paul McVey 
|}

==Natural Vision==
By 1951 film attendance had fallen dramatically from 90 million in 1948 to 46 million. Television was seen as the culprit and Hollywood was looking for a way to lure audiences back. Cinerama had premiered on September 30, 1952, at the Broadway Theater in New York and was a success there, but its bulky and expensive three-projector system and huge curved screen were impractical, if not impossible, to duplicate in any but the largest theaters.
 Columbia and Paramount passed it up. 
 Lights Out radio show. Oboler was impressed enough to option it for his next film project.

==Production==
Oboler and co-producer Sid Pink scrapped ten days of footage they had shot for The Lions of Gulu and started over using the Natural Vision process. 

The title of Obolers film was ultimately changed to Bwana Devil.
 Paramount Ranch, now located in The Santa Monica Mountains National Recreation Area, sat in for an African savanna. There is now a hiking trail in the area named "The Bwana Trail" to denote the locations used in Bwana Devil. Authentic African footage shot by Arch Oboler in 1948 (in 2-D) was incorporated into the film. Ansco Color film was used, instead of the more expensive and cumbersome Technicolor process.

==Release== Polaroid filters. anaglyph color filter method was only used for a few short films during these years. The two-strip Natural Vision projection system required making substantial alterations to a theaters projectors and providing its screen with a special non-depolarizing surface.
 Bob Clampett and featuring Beany and Cecil, was screened preceding the film. Long thought lost, the short rejoined Bwana Devil for screenings at the Egyptian Theater in 2003 and 2006.

 . The courts decided in Obolers favor, as Alpersons claim was unsubstantiated and "under the table".
 House of Wax. It premiered on April 10, 1953, and was advertised as "the first 3-D release by a major studio".  In truth, Columbia had trumped them by two days with their release of Man in the Dark on April 8, 1953.

==Commercial Reception==
The movie earned $2.7 million in rentals in North America in 1953.  United Artists ultimately ended up recording a loss of $200,000 on the film. 
==Reviews==
*Bosley Crowther of The New York Times said it was "a clumsy try at an African adventure film, photographed in very poor color in what appear to be the California hills". 
*Variety (magazine)|Variety summed up the process: "This novelty feature boasts of being the first full-length film in Natural Vision 3-D. Although adding backsides to usually flat actors and depth to landscapes, the 3-D technique still needs further technical advances."  The Saturday Review wrote on March 14, 1953, "It is the worst movie in my rather faltering memory, and my hangover from it was so painful that I immediately went to see a two-dimensional movie for relief. Part of the hangover was undoubtedly induced by the photography process itself. To get all the wondrous effects of the stereoscopic motion picture one has to wear a pair of polaroid glasses, made—so far as I could determine—from tinted cellophane and cardboard. These keep slipping off, hanging from one ear, or sliding down the nose, all the while setting up extraneous tickling sensations. And once you have them adjusted and begin looking at the movie, you find that the tinted cellophane (or whatever it is) darkens the color of the screen, so that everything seems to be happening in late afternoon on a cloudy day. The people seem to have two faces, one receding behind the other; the screen becomes unaccountably small, as though one is peering in at a scene through a window. Everything keeps getting out of proportion. Nigel Bruce will either loom up before you or look like a puppet. Sometimes there is depth and sometimes there isnt. One thing is certain: it was all horribly unreal."  

==In popular culture== The Society of the Spectacle, 1973]]
 3D glasses at the premiere. One of the photos was published as a full page in the magazine and has become iconic. It was also used as the cover of Life, The Second Decade, 1946–1955, a book published in conjunction with an exhibition of photographs from Life held at the International Center of Photography, New York. 
 

==Availability==
*   on September 13, 2006 in two strip polarized 3-D at the   in Hollywood, Ca.
*The film has not been released on VHS or DVD, but is available on Amazon Instant Video (as of May 2014).

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 