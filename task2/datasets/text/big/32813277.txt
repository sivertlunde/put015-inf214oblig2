The Ambassador's Daughter (1956 film)
{{Infobox film
| name           = The Ambassadors Daughter
| image_size     =
| image	         = The Ambassadors Daughter FilmPoster.jpeg
| caption        =
| director       = Norman Krasna
| producer       = Norman Krasna
| writer         = Norman Krasna
| narrator       =
| starring       = Olivia de Havilland John Forsythe
| music          =
| cinematography =
| editing        =
| studio         = Norman Krasna Productions
| distributor    = United Artists
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.5 million (US)  
}}
The Ambassadors Daughter is a 1956 romantic comedy film starring Olivia de Havilland and John Forsythe.

==Plot==
When a visiting American senator decides to make Paris off-limits to enlisted military personnel, the daughter of the U.S. Ambassador to France decides to show him that American servicemen can be gentlemen by dating one of them without revealing her lofty social status.  Sergeant Sullivan takes Joan to colorful nightclub cabarets, and on a comical trip up the Eiffel Tower, all the time believing her to be a Dior fashion model.  Thinking she has an emergency back in America, Sullivan offers to buy her an airline ticket, for which she is grateful, until she hears that counterfeit plane tickets are a common scam used by American servicemen to impress girls. Sullivans friend, the homespun Corporal OConnor, all the while is a guest of the Ambassadors family and other top brass, and tries to alert Sullivan as to Joans true identity, but is unable to contact Sulllivan (and is sworn to secrecy).  When Sullivan drops into the Dior fashion show one day to look for Joan, the staff have never heard of her, however he sees her observing the show with her fathers friend the Senator, whom he mistakenly assumes must be her sugar daddy. On their last dinner date, Joan walks out on Sullivan, when he accidentally spills wine on her and offers to take her to his hotel room, thinking he is dishonorable.  Finally, one evening Sullivan and the Ambassadors family, by coincidence, separately attend the same ballet performance of Swan Lake, where during the intermission Sullivan learns her true identity and their misunderstanding is resolved.

==Cast==
*Olivia de Havilland as Joan Fisk
*John Forsythe as Sergeant Danny Sullivan
*Myrna Loy as Mrs. Cartwright
*Adolphe Menjou as Senator Jonathan Cartwright
*Tommy Noonan as Corporal Al OConnor, Dannys friend
*Francis Lederer as Prince Nicholas Obelski Edward Arnold as Ambassador William Fisk
*Minor Watson as General Andrew Harvey. This was Watsons last film.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 