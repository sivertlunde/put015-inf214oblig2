Babar: The Movie
 
 
{{Infobox film

 | name = Babar: The Movie
 | 
 | director = Alan Bunce Michael Hirsh Patrick Loubert Clive A. Smith
 | writer = Original   and   Michael Hirsh Patrick Loubert Screenplay: Peter Sauder J.D. Smith John de Klein Raymond Jafelice Alan Bunce
 | starring =
 | music = Milan Kymlicka
 | editing = Evan Landis Nelvana Limited Ellipse Programmé
 | distributor = United States:   United States VHS:  
 | released = 28 July 1989
 | runtime = 76 minutes
 | country = Canada France
 | language = English-French
 | gross = $1,305,187 
 | image= Babar- The Movie VideoCover.png
}} eponymous childrens first season TV series.

==Plot==
On the night of Elephantlands Victory Parade, Babar tells his four children the story of his first days as King of the elephants.

On his first day as king, he is asked to choose a name for Elephantlands Annual Parade.  Babar promptly selects one, but is informed by the bureaucratic-minded lords that the matter must be thoroughly examined by committee.  Babars cousin, Celeste, then interrupts to tell Babar that her home has been attacked by Rataxes, the rhinoceros lord, and his horde.  The chancellors scoff and rubuff her, but Babar orders an elephant army to be called up immediately to defeat the rhinos, if partly because he wants to impress Celeste.

But, due to the heel-dragging of his ultra-conservative ministers, Babar learns that the muster will take at least three days.  Not willing to wait any longer, Babar tells his cousin Arthur, Celestes brother, to take care of his job as King while he ventures off on his own to help their mother, amid dangerous jungle.  He finds Celestes village aflame; the rhinos are taking the adult elephants as slaves so that they can work on building a rhino city.  Babar tries to intervene, but is knocked senseless for his trouble.

When he comes to, Babar rescues Celeste out of the town well, and they set off to rescue her mother, and the other pachyderms, from Rataxes wrath.   Along the way, they meet a monkey named Zephir, who gives them the location of the rhinos base.  The two come face to face with Rataxes himself, who plans to invade Babars kingdom by twilight, and are put in jail, but they both escape along with Zephir, and race back to Elephantland to save it.

Heading into the rhinos tents, they disguise themselves as one of the warriors, asking for "special detail" of their plans for attack, but to no avail.  They get away from Rataxes quickly, launching from a catapult and landing in a fountain, much to the surprise of Babars advisors.

The evil rhino proclaims Elephantland will be destroyed in an hour, absent unconditional surrender.  To buy time, Babar orders the two ministers to distract Rataxes with their "committee" procedure.  The elephant army takes some action into their hands, and a giant elephant float, built by Babar and company, scares off Rataxes soldiers.

At sunrise, Babars friends congratulate him on saving the day and his town, but are surprised to learn that their very first Victory Parade will be held during the afternoon.  It has gone by that name ever since, the older Babar recalls, because the committee could not find any other name for it.

As Babar finishes his tale, he finds that his children have all gone to sleep.  Right after he closes the door, they re-enact scenes from the story, until their father tells them to get back to bed.

==Cast==
* Gordon Pinsent - King Babar
* Elizabeth Hanna - Queen Celeste/The Old Lady
* Lisa Yamanaka - Isabelle
* Marsha Moreau - Flora
* Bobby Becken - Pom
* Amos Crawley - Alexander
* Gavin Magrath - Young Babar
* Sarah Polley - Young Celeste
* Stephen Ouimette - Pompadour
* Chris Wiggins - Cornelius John Stocker - Zephir
* Charles Kerr - Rataxes/Guard Rhinos
* Stuart Stone - Young Arthur
* Carl Banas - Old Tusk
* Ray Landry - Croc
* Angela Fusco - Celestes Mother
* Christopher Andrande - Additional Voices Christopher Britton - Additional Voices
* Scott Brunt - Additional Voices
* Jason Burke - Additional Voices
* Katie Coristine - Additional Voices
* Barbara Mantini - Additional Voices
* Frank Perry - Additional Voices
* Chris Robson - Additional Voices
* Norm Spencer - Additional Voices
* Lea-Helen Weir - Additional Voices

==Release==
In May 1989, the Toronto-based animation studio   deemed it a box-office flop, although the film did regain its losses though the home video release.   It was the last animated feature production by Nelvana until 1997s  .

A book adaptation of the movie, written by Cathy East Dubowski and illustrated by Renzo Barto, was published by Random House in November 1989.

==Soundtrack== John Stocker. 

The songs are (in order of appearance):

1. Elephantland March - Written by Maribeth Solomon; Performed by The Nylons, Judy Tate, Debbie Fleming (as Debbie Flemming), John Rutledge, and Neil Donell

2. The Committee Song  - Written by Philip Balsam (creditied as Phil Balsam); Performed by Stephen Ouimette, Chris Wiggins, and The Nylons

3. The Best We Both Can Be - Written by Maribeth Solomon; Performed by Molly Johnson

4. Monkey Business - Written by Maribeth Solomon; Performed by John Stocker, Judy Tate, Debbie Fleming (as Debbie Flemming), John Rutledge, Neil Donell, and The Nylons

5. Rataxes - Song Written by Kevan Staples, Marvin Dolgay and Carole Pope for Tambre Productions; Performed by Charles Kerr

==Reception==
The film was given mixed reviews from critics.  

==References==
 

==External links==
 
*  
*  
*  
*   at Keyframe: The Animation Resource

 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 