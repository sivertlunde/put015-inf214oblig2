Two Minutes Silence
 
 
 
{{Infobox film
| name           = Two Minutes Silence
| image          = 
| image_size     = 
| caption        = 
| director       = Paulette McDonagh
| producer       = Paulette McDonagh Phyllis McDonagh 
| writer         = Les Haylen
| based on      = play by Les Haylen
| narrator       = 
| starring       = Frank Bradley Campbell Copelin Marie Lorraine
| music          = 
| cinematography = James Grant
| editing        = 
| studio         = McDonagh Productions
| distributor  = Universal 
| released       = 18 October 1933 (Canberra premiere) 2 February 1934 (Sydney)
| runtime        = 75 mins
| country        = Australia
| language       = English
| gross = £225 (Australia) "Counting the Cash in Australian Films", Everyones 12 December 1934 p 19 
}} McDonagh sisters, Paulette, Isobel and Phyllis, who called it "by far the best picture we produced".  It is considered Australias first anti-war movie and is a lost film. 

==Synopsis==
On Armistice Day, some years after World War I has ended, four people gather in General Greshams London drawing room. As the clock strikes eleven, they think back to their experiences of the way. Mrs Trott (Ethel Gabriel), a charlady, recalls hearing her son has died in action. The general (Frank Bradley) remembers making an error of judgement that led to the death of his men. Denise (Marie Lorraine), the French governess to the generals grandchildren, relives the return of her war hero lover, Pierre (Campbell Copelin), from the front; Pierre could not forgive Denise for having a child to a German officer who had raped her. The generals butler, James (Leonard Stephens), recollects living as a beggar on the Thames embankment after the war and seeing the suicide of an ex-soldier.

==Cast==
 
*Frank Bradley as General Gresham
*Campbell Copelin as Pierre
*Marie Lorraine as Denise
*Frank Leighton as Captain Lessups
*Leo Franklyn as Private Simpson
*Ethel Gabriel as Mrs Trott
*Victor Gouriet as Corporal Smith
*Leonard Stephens as James
*Eva Moss as Miss Tremlitt
*Peggy Pryde as Mrs Breen
*Arthur Greenaway as cure
*Frank Hawthorne as Reverend Thomas
*Bert Barton as Toby
*Katie Towers as nun
*Fred Kerry as flower seller
*Hope Suttor
*Dorothy Dunkley
*Peggy Pryde
 

==Original Play==
The play was written by Les Haylen, an Australian journalist who later became an MP. It was originally produced in 1930 at the Community Playhouse in Sydney and ran for 26 performances over 13 weeks.    The McDonagh sisters saw the play and bought the film rights before the run ended.  There was also a production in Townsville the following year, and another one in Sydney in 1939.  

The play was published in 1933 featuring an introduction from Billy Hughes. 

==Production==
Filming took place  in a studio in Centennial Park, Sydney in late 1932. It was logistically difficult because of the difficulty sourcing sound recording equipment and two of the cast, Leo Franklyn and Frank Leighton, had to juggle shooting with their theatre commitments for J.C. Williamson Ltd. In addition, Campbell Copelin was still recovering from his plane crash the previous year.  Ethel Gabriel reprised her role from the play version.  

"Two Minutes Silence, I think, was the hardest job weve ever tackled", said Paulette McDonagh at the time. "Obstacle after obstacle crowded in on us. ... at times it seemed almost impossible to continue. But we realised we had to, and..  well... we just did.
Difficulties and delay were a nightmare. But we managed to pull through." 

The McDonagh sisters promoted the movie by speaking at special luncheons about the filmmaking process.  There was a private preview in Sydney in January 1933 which was introduced by Ted Theodore. 

==Release==
The film had trouble getting distribution but was eventually picked up by Universal, who arranged for it to be re-cut.  The world premiere took place in Canberra in 1933, in front of Governor General Sir Isaac Isaacs and various parliamentarians.  Initial critical reception was generally positive,  The Canberra Times praising it as "a welcome... in the subjects adopted by Australian film producers.  and it received a minor release in Sydney the following year. 

The critic from the Sydney Morning Herald was less enthusiastic than his earlier colleagues, calling it:

 A production in which the skill of the acting far exceeds the value of the dramatic material on which it has been bestowed...  even when allowance is made for the cramped studio conditions under which the film has been prepared... Apart from the scenes from war-time newsreels which have been interpolated, there are only five or six settings; and these are of the most limited nature. The camera once having been established, the actors begin, and go through their scenes without interruption, as though they were on a theatrical stage. There are no changes of angle, and very few changes of distance, to give variety... The weakness of Mr. Haylens play is that it revives all that is dreary and horrible in wartime memories without developing any significant leading idea... Mrs. Gabriel... represents a cockney charwoman with a realism and a persistent sympathy which almost triumphs over the emptiness of the dramatic situation... Mr. Franklyn... has succeeded in toning down his exuberant stage methods into the greater restraint demanded by the camera. Mr. Leighton and Mr Bradleys parts give the same impression of ease and naturalness which these actors always convey in the theatre. Mr Campbell Copelin and Miss Marie Lorraine both show the same weakness – a colourless, monotonous method of delivering their lines.  

The reviewer from the Australian Womens Weekly said:

 That the characters chosen by the author were not Australian is a defect, inasmuch as he constantly betrays his ignorance of their natural speech and behaviour. The film adaptation has removed some incongruities, but not all. And to make a really successful film much more action and movement is required to break up the rather lengthy conversational passages between characters grouped in the same small scene. In fact, the direction as a whole is too amateurish for so ambitious a scheme, and the same thing might be said of some of the acting. Marie Lorraine, in particular, is miscast.  

The movie only enjoyed a short run.  It was the final film made by the McDonagh sisters. Isobel (Marie Lorraine) moved to London to marry and Phyllis went to work as a journalist in New Zealand. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p162-163  "We gave up because we thought wed done our bit and we wanted to move on to other things separately", said Phyllis later.   

Plans by the McDonaghs to make a short about Canberra called The Heart of a Nation appear to have not come to fruition. 

==References==
 

==External links==
*  in the Internet Movie Database
*  at National Film and Sound Archive
*  at Oz Movies
 

 
 
 
 
 
 
 
 
 