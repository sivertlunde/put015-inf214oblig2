The Fog
 
{{Infobox film
| name           = The Fog
| image          = The fog 1980 movie poster.jpg
| caption        = Original theatrical poster
| director       = John Carpenter
| producer       = Debra Hill
| writer         = John Carpenter Debra Hill
| starring       = Adrienne Barbeau 
Jamie Lee Curtis  Tom Atkins 
John Houseman 
Janet Leigh 
Hal Holbrook
| music          = John Carpenter
| cinematography = Dean Cundey
| editing        = Charles Bornstein Tommy Lee Wallace AVCO Embassy Pictures
| released       =  
| runtime        = 89 min.
| country        = United States
| language       = English
| budget         = $1 million 
| gross          = $21,378,000  (domestic)  
}} Tom Atkins, Janet Leigh and Hal Holbrook. It tells the story of a strange, glowing fog that sweeps in over a small coastal town in California, bringing with it the vengeful ghosts of mariners who were killed in a shipwreck there exactly 100 years earlier. 
 remake of the film was made in 2005.

==Plot==
   Californian coastal colony near Antonio Bay. One foggy night, the six conspirators lit a fire on the beach near treacherous rocks. The ships crew, deceived by the false beacon, crashed into the rocks and perished. The conspirators were motivated by both greed and disgust at the notion of having a leper colony nearby. Antonio Bay and its church were then founded with the gold plundered from the ship.

A supernaturally glowing fog appears over the sea, moving against the wind. Three local fishermen are out at sea when the fog envelops their Fishing trawler|trawler. An old clipper ship pulls alongside their trawler. It is the Elizabeth Dane carrying the vengeful ghosts of Blake and his crew, who have come back on the hundredth anniversary of the shipwreck and the founding of the town. The three fishermen are slaughtered. At the same time, town resident Nick Castle is driving home and picks up a young hitchhiker named Elizabeth Solley. As they drive towards town, all the trucks windows inexplicably shatter.

The following morning, local radio DJ Stevie Wayne is given a piece of driftwood inscribed with the word "DANE" that was found on the beach by her young son Andy. Intrigued, Stevie takes it with her to the lighthouse she broadcasts her radio show from. She sets the wood down next to a tape player that is playing, but the wood inexplicably begins to seep water causing the tape player to short out. A mysterious mans voice emerges from the tape player swearing revenge, and the words "6 Must Die" appear on the wood before it bursts into flames. Stevie quickly extinguishes the fire, but then sees that the wood once again reads "DANE" and the tape player begins working normally again.

After locating the missing trawler, Nick and Elizabeth find the corpse of Dick Baxter with his eyes gouged out. The other two fishermen are nowhere to be found. The town coroner, Dr. Phibes, is perplexed by the bodys advanced state of decomposition considering Baxter died only hours earlier. While Elizabeth is alone in the autopsy room, Baxters corpse rises from the autopsy table and approaches her. As Elizabeth screams, Nick and Phibes rush back into the room where they see the corpse lifeless again on the floor, appearing to have scratched the number "3" onto the floor with a scalpel. Baxter was the third victim to die. Meanwhile, Kathy Williams, the organizer of the towns centennial, visits Father Malone. He reads the journal to her, revealing how the town was founded and that their celebration is honoring murderers.

That evening, the towns celebrations begin. Local weatherman Dan calls Stevie at the radio station to tell her that another fog bank has appeared and is moving towards town. As they are talking, the fog gathers outside the weather station and Dan hears a knock at the door. He answers it and is killed by the ghosts as Stevie listens in horror. As Stevie continues her radio broadcast, the fog begins moving inland and neutralizes the towns phone and electricity lines. Using a back-up generator, Stevie begs her listeners to go to her house and save her son when she sees the fog closing in from her lighthouse vantage point. At Stevies home, her sons babysitter, Mrs. Kobritz, is killed by the ghosts as the fog envelops the house. The ghosts then pursue Andy, but Nick arrives just in time to rescue him.

As the towns celebration comes to an end, Kathy and her assistant Sandy are driving home when they turn on the car radio and hear Stevie warning people about the dangerous fog sweeping through town. Stevie advises everyone to go to the church, which appears to be the only safe place. Nick, Elizabeth, and Andy hear the same message and the group gather there. Once inside, they and Father Malone take refuge in a small back room as the fog begins to appear outside. Inside the room, they find a large gold cross buried in the walls, made of the gold that was stolen from Blake a century earlier. As the ghosts begin their attack, Malone takes the gold cross out into the chapel. Knowing he is a descendant of one of the conspirators, Malone confronts Blake and offers himself to save everyone else. Back at the lighthouse, more ghosts attack Stevie, trapping her on the roof. Inside the church, Blake seizes the gold cross which begins to glow. Nick pulls Malone away from the cross only seconds before it disappears in a blinding flash of light, along with Blake and his crew. The ghosts attacking Stevie at the lighthouse also disappear, and the fog vanishes. Later that night, Malone is alone in the church pondering why Blake did not kill him and thus take six lives. The fog then reappears inside the church along with the ghosts, and Blake decapitates Malone.

==Cast==
 
* Adrienne Barbeau as Stevie Wayne
* Jamie Lee Curtis as Elizabeth Solley
* Janet Leigh as Kathy Williams
* John Houseman as Mr. Machen Tom Atkins as Nick Castle
* James Canning as Dick Baxter
* Charles Cyphers as Dan OBannon Nancy Loomis as Sandy Fadel
* Ty Mitchell as Andy Wayne
* Hal Holbrook as Father Malone
* John F. Goff as Al Williams
* George Buck Flower as Tommy Wallace
* Darwin Joston as Dr. Phibes
* Rob Bottin as Blake
* John Carpenter as Bennett
 

==Production==
===Development===
 , where many of Adrienne Barbeaus scenes were shot.]] Assault on Precinct 13, Carpenter and Hill visited the site in the late afternoon one day and saw an eerie fog in the distance. In the DVD audio commentary for the film, Carpenter noted that the story of the deliberate wreckage of a ship and its subsequent plundering was based on an actual event that took place in the 19th century near Goleta, California   (this event was portrayed more directly in the 1975 Tom Laughlin film, The Master Gunfighter). The premise also bears strong resemblances to the John Greenleaf Whittier poem The Wreck of the Palatine which appeared in The Atlantic Monthly in 1867, about the wreck of the ship Princess Augusta in 1738, at Block Island, within Rhode Island.

The Fog was part of a two-picture deal with AVCO-Embassy, along with   (interior scenes) and on location at Point Reyes, California, Bolinas, California, Inverness, California, and the Episcopal Church of the Ascension in Sierra Madre, California.
 Ghost Story). Carpenter added several other new scenes and re-shot others in order to make the film more comprehensible, more frightening, and gorier. Carpenter and Debra Hill have said the necessity of a re-shoot became especially clear to them after they realized that The Fog would have to compete with horror films that had high gore content.  Approximately one-third of the finished film is the newer footage.

===Casting===
Cast as the female lead was Adrienne Barbeau, Carpenters then-wife, who had appeared in Carpenters TV movie Someones Watching Me! in 1978. Barbeau would subsequently appear in Carpenters next film, Escape from New York (1981).

  (1982), which was produced and scored by Carpenter. 

Jamie Lee Curtis, who was the main star of Carpenters 1978 hit Halloween (1978 film)|Halloween, appeared as Elizabeth. Commenting on the role and on appearing in another of Carpenters films, she said "Thats what I love about John. Hes letting me explore different aspects of myself. Im spoiled rotten now. My next director is going to be almost a letdown." 
 Tom Atkins all appeared in Creepshow shortly thereafter.

===Play on character names and other references===
Besides the fact that many of the actors in The Fog also appeared in Halloween (and other later Carpenter films), several characters in The Fog are named after people that Carpenter had collaborated with on previous films.   Dark Star (1974). Michael Myers in Halloween (1978 film)|Halloween (1978).
* Tommy Wallace has worked with Carpenter as an editor, art designer, and sound designer on several of his films in the 1970s and 1980s.
* Richard Kobritz, the producer of Carpenters 1978 TV film Someones Watching Me! inspired the name of the character Mrs. Kobritz. 
 Arkham Reef; the titular character of the horror films starring Vincent Price from the early 1970s.

==Reception==
 
  
The film was greeted with mixed reviews when it was initially released, but was a commercial success.  It later came to be considered, as Carpenter once called it, "a minor horror classic". Carpenter himself stated that this is not his overall favorite film due to re-shoots and low production values. This is one of the reasons he agreed to the 2005 remake.  

Roger Ebert gave the film two out of four stars, commenting, "This isnt a great movie but it does show great promise from Carpenter".   The review aggregator website Rotten Tomatoes reported a 69% approval rating with an average rating of 6.2/10 based on 39 reviews. 

Zombiemania: 80 Movies to Die For author Arnold T. Blumberg wrote that the film was "a very effective small scale chiller" and "an attempt to capture the essence of a typical spooky American folktale while simultaneously paying homage to the EC Comics of the 1950s and the then very recent Italian zombie influx." 
 Time Out conducted a poll of over 100 authors, directors, actors and critics who have worked within the horror genre to vote for their top horror films.  The Fog placed at number 91 on their top 100 list. 

==Novelization==
In the same year as the movie was released, a novelization was published written by Dennis Etchison. The novel clarifies the implication in the film that the six who must die were not random but in fact descendants of the six original conspirators.

==Home media==
 
The film has been released on various home video formats since the early 1980s, including video cassette and laserdisc. It was released on DVD in 2002 complete with extra features including two documentaries and an audio commentary by John Carpenter and Debra Hill as well as trailers and galleries. Shout! Factory released the film on Blu-ray in 2013, which included the previous extra features as well as a new audio commentary by actors Adrienne Barbeau and Tom Atkins and production designer Tommy Lee Wallace, a new interview with Jamie Lee Curtis, and an episode of Horrors Hallowed Grounds which revisits the films locations.

==Remake==
  remade under the direction of Rupert Wainwright with a screenplay by Cooper Layne and starring Tom Welling and Maggie Grace. Though based on Carpenter and Hills original screenplay, the remake was made more in the vein of a "teen horror film" and given a PG13 rating (the original film was rated Motion Picture Association of America film rating system|R). Green-lit by Revolution Studios with just eighteen pages of script written, the film was almost universally panned for its poor script and acting and has a Rotten Tomatoes rating of 4%. 

==See also==
*List of ghost films

==References==
{{Reflist|refs=
 Gilles Boulenger, John Carpenter: Prince of Darkness, Los Angeles: Silman-James Press, 2003. ISBN 1-879505-67-3 
   
 Paul Scanlon,  The Fog: A Spook Ride on Film", Rolling Stone, June 28, 1979.    at the TheOfficialJohnCarpenter.com.  Retrieved 2007-11-17. 
 Audio commentary by John Carpenter and Debra Hill in The Fog, 2002 special edition DVD. 
   
   
 Roger Ebert,  , Chicago Sun-Times, February 5, 1980.  Retrieved 2007-11-17. 
   
   
   
   
}}

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 