Vamban
{{Infobox film 
| name           = Vamban
| image          =
| caption        =
| director       = Hassan
| producer       =
| writer         =
| screenplay     = Rohini Kalaranjini Anuradha
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Karthikeya Films
| distributor    = Karthikeya Films
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Anuradha in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Ratheesh Rohini
*Kalaranjini Anuradha
*Ranipadmini
*TG Ravi

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by KG Menon and Arifa Hassan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Malare || Vani Jairam || KG Menon, Arifa Hassan || 
|}

==References==
 

==External links==
*  

 
 
 

 