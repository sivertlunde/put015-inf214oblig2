Hero (1983 film)
{{Infobox film
| name           = Hero
| image          = Hero1983.jpg
| caption        = Theatrical poster
| director       = Subhash Ghai
| producer       = Subhash Ghai
| writer         = Mukta Ghai Subhash Ghai Ram Kelkar
| narrator       = 
| starring       = Jackie Shroff Meenakshi Sheshadri
| music          = Laxmikant-Pyarelal
| cinematography = Kamalakar Rao
| editing        = Waman Bhonsle Gurudutt Shirali
| studio         = 
| distributor    = Mukta Arts Ltd.
| released       =  
| runtime        = 173 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          =  12,50,00,000    
}} 1983 Indian Hindi film Miss India in 1981 and played the lead female role gained popularity through this movie.
 remake is being filmed by Nikhil Advani. 

==Plot==
The story starts off with Pasha (Amrish Puri) being taken to prison. To get out of the situation, he writes to his best man, Jackie (Jackie Shroff). Jackie goes to Shrikanth Mathur (Shammi Kapoor) and warns him. He then kidnaps Shrikanths daughter Radha (Meenakshi Sheshadri). He tells her that he is a police officer and they fall in love; however, she finds out that he is a goon.  Nevertheless, she does not leave him but urges him to surrender. Transformed by true love, Jackie surrenders himself to the police and is imprisoned for two years. 

Back home, Radha tells her brother Daamodar (Sanjeev Kumar) the whole truth. To keep Radha from getting married to somebody else, he calls his friend Jimmy (Shakti Kapoor) to put on a show that Radha and Jimmy love each other. Jimmy misunderstands the situation and falls in love with Radha. When Jackie comes back, he starts working in a garage and tries to reform himself. Despite everything, Shrikanth kicks him out of his life. After many days and events that follow, Daamodar finds out that Jimmy is a drug smuggler. After getting released from prison, Pasha desires revenge against both Shrikanth and Jackie, so he kidnaps Radha, Shrikanth and Daamodar. Jackie comes at the last moment and frees all of them. As a happy ending, Shrikanth lets Radha marry Jackie.

==Cast==
* Jackie Shroff as Jackie Dada / Jaikishan
* Meenakshi Sheshadri as Radha Mathur
* Sanjeev Kumar as Damodar Mathur
* Shammi Kapoor as Retd. IGP Shrikanth Mathur
* Amrish Puri as Pasha
* Madan Puri as Bharat Bindu as Jamuna (Radhas widowed aunt)
* Bharat Bhushan as Ramu
* Shakti Kapoor as Jimmy Thapa
* Urmila Bhatt as Sandhya Mathur
* Shaikh azad  as Shaikh faruk

==Reception==
The film was declared "Super Hit" on Indian Box Office,  with Jackie Shroff and Meenakshi Sheshadri becoming huge stars after its release. Both actors became in demand after Heros release and thus began a long association with Subhash Ghai. The younger audiences enjoyed the performances, and critics applauded Ghai for using fresh faces at a time when Amitabh Bachchan was the dominant star in Bollywood.

==Tracks list==
Composed by the musical duo Laxmikant Pyarelal the lyrics of the songs are penned by Anand Bakshi.
{{Track listing
| extra_column = Singer(s)
| title1 = Ding Dong | extra1 = Anuradha Paudwal, Manhar  Udhas  | length1 = 
| title2 = Lambi Judai | extra2 = Reshma  | length2 = 
| title3 = Nindya Se Jaagi Bahaar | extra3 = Lata Mangeshkar | length3 = 
| title4 = Pyar Karne Wale Kabhi Darte Nahi | extra4 = Lata Mangeshkar, Manhar  Udhas | length4 = 
| title5 = Tu Mera Hero | extra5 =  Anuradha Paudwal, Manhar  Udhas | length5 = 
| title6 = Mahobbat ye mahobbat | extra6 = Suresh Wadkar, Lata Mangeshkar | length6=
}}

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
|-
| 1986
|Vikram (1986 Telugu film)|Vikram Cinema of Telugu
| Akkineni Nagarjuna, Sobhana
| V. Madhusudhan Rao
|-
| 1988
|Ranadheera Cinema of Kannada
|V. Ravichandran, Kushboo, Ananth Nag
| V. Ravichandran
|-
| 2013
| Hero (2014 film)|Hero Hindi
| Govinda
| Filming 
|-
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 