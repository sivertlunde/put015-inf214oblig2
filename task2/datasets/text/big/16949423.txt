Behind That Curtain (film)
 
{{Infobox film
| name           = Behind That Curtain
| image	         = Behind That Curtain FilmPoster.jpeg
| caption        = Film poster
| director       = Irving Cummings William Fox George Middleton Wilbur Morse Jr. Clarke Silvernail
| starring       = Warner Baxter Donald Keith
| music          =
| cinematography = Conrad Wells Vincent J. Farrar Dave Ragin
| editing        = Alfred DeGaetano Fox Film Corporation
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
}}
 Fox Studios. novel of the same name. Charlie Chan is played by Korean American actor E. L. Park, gets one mention early in the film, then makes a few momentary appearances after 75 minutes. 
 William Fox chose this film to open the palatial  Fox Theatre in San Francisco on June 28, 1929. It was a sound film.

==Cast==
* Warner Baxter as Col. John Beetham
* Lois Moran as Eve Mannering Durand
* Gilbert Emery as Sir Frederick Bruce (as Gilbert Emory) Claude King as Sir George Mannering
* Philip Strange as Eric Durand
* Boris Karloff as Beethams Manservant
* Jamiel Hasson as Sahib Hana
* Peter Gawthorne as British Police Inspector John Rogers as Alf Pornick
* Edgar Norton as Hilary Galt
* Frank Finch Smiles as Galts Clerk
* Mercedes De Valasco as Nuna Police Insp. Charlie Chan
* Kathrin Clare Ward as Eves Landlady (uncredited)

==See also==
* List of American films of 1929
* Boris Karloff filmography

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 