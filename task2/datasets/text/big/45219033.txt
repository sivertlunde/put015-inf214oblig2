Tatamma Kala
{{Infobox film
| name           = Tatamma Kala
| image          =
| caption        =
| writer         = D. V. Narsaraju  
| story          = N. T. Rama Rao
| screenplay     = N. T. Rama Rao
| producer       = N. T. Rama Rao
| director       = N. T. Rama Rao
| starring       = N. T. Rama Rao  Bhanumathi Ramakrishna   Nandamuri Balakrishna
| music          = S. Rajeswara Rao
| cinematography = J. Satyanarayana
| editing        = G. D. Joshi
| studio         = Ramakrishna Cine Studios   
| released       =  
| runtime        = 2:38:00
| country        =   India
| language       = Telugu
| budget         =
| gross          = 
}}

Tatamma Kala (  drama film produced and directed by N. T. Rama Rao by his Ramakrishna Cine Studios. The film stars N. T. Rama Rao, Bhanumathi Ramakrishna, Nandamuri Balakrishna in lead roles and music composed by S. Rajeswara Rao.     This film was Balakrishnas debut.     

==Cast==
 
*N. T. Rama Rao as Ramaiah & Musaliah (Duel role)
*Bhanumathi Ramakrishna as Ravamma
*Nandamuri Balakrishna as Balakrishna 
*Nandamuri Harikrishna as Venkatesam
*Ramana Reddy as John  Raja Babu as Hare Ram 
*Mada as Bhusaiah
*P. J. Sarma as Venkaiah 
*Chalapathi Rao 
*Bhima Raju as Ratnam 
*Balakrishna 
*Jaya Bhaskar Kanchana as Seeta
*Roja Ramani as Rama  Vijayalalitha as Mala
*Subha as Saru
 

==Soundtrack==
{{Infobox album
| Name        = Tatamma Kala
| Tagline     = 
| Type        = film
| Artist      = S. Rajeswara Rao
| Cover       = 
| Released    = 1974
| Recorded    = 
| Genre       = Soundtrack
| Length      = 38:29
| Label       = 
| Producer    = S. Rajeswara Rao
| Reviews     =
| Last album  = 
| This album  = 
| Next album  = 
}}

Music composed by S. Rajeswara Rao. Lyrics written by C. Narayana Reddy.  
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Evaranukunnaru Bhanumathi Ramakrishna
|3:03
|- 2
|Ayyalali Muddulayyalali Bhanumathi Ramakrishna
|4:02
|- 3
|Emandi Vadinagaru
|L. R. Eswari
|3:13
|- 4
|Korameesam Kurroda Ghantasala (singer)|Ghantasala,Bhanumathi Ramakrishna
|5:38
|- 5
|Sanagapoola Ghantasala
|4:26
|- 6
|Pandavulu Pandavulu SP Balu,P. Susheela
|4:17
|- 7
|Sye Annanura Kovela Shantha
|3:30
|- 8
|Ye Manishi SP Balu
|5:30
|- 9
|Taaru Roadlape Madhavapeddi Satyam
|4:50
|}

==Others== Hyderabad

==References==
 

 
 
 


 