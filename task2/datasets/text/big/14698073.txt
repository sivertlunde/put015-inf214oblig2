Inch'Allah Dimanche
 
{{Infobox film  image           = Inchallah dimanche.JPG
| caption        =
| name           = InchAllah dimanche 
| writer         =Yamina Benguigui
| starring       =Fejria Deliba 		
| director       =Yamina Benguigui
| released       =September 14, 2001
| runtime        =96 min.
| country        =France Algeria
| budget         =
| cinematography =Antoine Roch	  French
| producer       =Bachir Deraïs   Philippe Dupuis-Mendel	
| distributor    =ARP Sélection   Divisa Home Video (Spain)   Film Movement (USA) 
|
}} 2001 France|French/Algerian movie by Yamina Benguigui about the life of an Algerian immigrant woman in France. Though this is Beguiguis first feature-length fiction film it is largely descriptive of her familys experience moving to France and the struggles for autonomy Algerian women continue to face even today. The film won a variety of international awards, including the 2001 International Critics Award at the Toronto International Film Festival.  Although Benguigui was urged to change the name of the film after the September 11 attacks, she chose to keep the original title, a portion of which is in Algerian Arabic. This film explores the complexities of immigration and the role of women in Algerian society. 

 

==Cast==
*Fejria Deliba – Zouina
*Rabia Mokeddem – Aïcha, mother
*Amina Annabi – Malika
*Anass Behri –  Ali
*Hamza Dubuih – Rachid
*Zinedine Soualem – Ahmed
*France Darry – Mrs. Donze (neighbor)
*Roger Dumas – Mr. Donze (neighbor)
*Marie-France Pisier – Manant
*Mathilde Seigner – Nicole Briat
*Jalil Lespert – Bus driver

==Plot synopsis==
Zouinas husband, Ahmed, left Algeria in the 1970s to work in France. As part of the French governments Family Reunification law passed by President Jacques Chirac in 1974, Zouina is allowed to move to France from Algeria in order to join her husband, Ahmed. After tearfully leaving her mother behind, Zouina, her mother-in-law, Aicha, and their three children. Zouina struggles to cope with life in a new country and different culture but becomes a prisoner to the tyranny of Aicha and her husbands failures to protect her. Zouina also encounters a host of neighbors, some of which intensify the alienation she feels in her new home but many who extend their hand in friendship. Sunday, when her Ahmed routinely takes his mother out for the day, Zouina and the children are able to explore and search for other another Algerian family and genuine human contact. Zouina ultimatlley finds this family after three weeks but suffers a regection that mirrors being ripped from her home in Algeria and general rejection from her new home in France. Through her journey Zouina gains her own strength, revels in the community of women she finds home in and is comforted by the emerging feminist dialogue she receives through radio talk shows like Ménie Grégoire. 


==Music==
The film contains a variety of French, Algerian Arabic, and Kabyle language music. Many of the tracks are performed by Algerian musician Idir. 
#"Ageggig" – Idir (A. Mouhed, Idir)
#"Al Laïl" – Alain Blesing (Alain Blesing)
#"Apache (The Shadows song)|Apache" – The Shadows (Jerry Lordan)
#"Isefra" – Idir (M. Benhammadouche, Idir)
#"Djebel" – Aziz Bekhti
#"Cenud" – Nourredine Chenoud
#"Snitraw" – Idir
#"Le Premier Bonheur du Jour" – Françoise Hardy (Franck Gerald, Jean Renard)
#"Djin" – Alain Blesing
#"Temzi (Mon Enfance)" – Hamou (Hamou, Ben Mohamed, Eric Amah, Caroline Pascaud-Blandin)
#"Sssendu" – Idir
#"Raoul" – Souad Massi

==Awards==

Winner - FIPRESCI Award (Best Film) - Toronto Intl Film Festival  

Winner - Audience Choice Award - Bordeaux Intl Festival of Women in Cinema  

Winner - Best Actress - Bordeaux Intl Festival of Women in Cinema  

Winner - Golden Star - Marrakech Intl Film Festival  

Winner - OCIC Award - Amiens Intl Film Festival  

Official Selection - Reel Dame Film Festival  

Official Selection - Crossroad Intl Film Festival  

Nominated - Golden Pyramid - Cairo Intl Film Festival  
 

==Reception==

=== Critical response ===
InchAllah Dimanche received mixed reviews. Rotten Tomatoes gives the film a score of 71% based on reviews from 13 critics, with an average score of 3.6/5. While some audience members found it to be a strong representation of both an assimilation story and a womans ability to transcend obstacles others found the film to be well meaning but missing the point.

On the films massage and efficacy Don Houston of DVD Talk notes "Immigration affects all of us in one way or another, no matter where you live or work. If this movie can spur some thoughtful discussion on the matter, it will have done us all a great service. Im not sure if I agree with all the conclusions the director came to but I can appreciate that she walked the walk and now talks the talk to the extent that she could so readily outline many of the issues that impact us all."

Lisa Nesselson of Variety.com praises the film by saying "Narrative is often bittersweet but never dreary. Nicely rendered period design jolts the viewer with reminders that provincial France in the mid-70s was still closer to WWII than to the present and that todays relatively harmonious multicultural society was hard won indeed."


==References==
 
 
 

== External links ==
*  

 
 

 
 
 
 
 
 
 