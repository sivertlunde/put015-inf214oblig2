Paid in Full (1950 film)
{{Infobox film
| name           = Paid in Full
| image          = Paid in Full (1950 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = William Dieterle
| producer       = Hal B. Wallis
| screenplay     = Robert Blees Frederick M. Loomis Charles Schnee Ray Collins Frank McHugh Stanley Ridges
| music          = Victor Young
| cinematography = Leo Tover
| editing        = Warren Low
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Ray Collins, Frank McHugh and Stanley Ridges. The film was released on February 15, 1950, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Robert Cummings as Bill Prentice
*Lizabeth Scott as Jane Langley
*Diana Lynn as Nancy Langley
*Eve Arden as Tommy Thompson Ray Collins as Dr. Fredericks
*Frank McHugh as Ben
*Stanley Ridges as Dr. P.J. Phil Winston
*Louis Jean Heydt as Dr. Carter
*John Bromfield as Dr. Clark
*Kristine Miller as Miss Williams, Bridesmaid
*Kasey Rogers as Tina
*Lora Lee Michel as Betsy 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 