The Wild North
{{Infobox film
| name           = The Wild North
| image          =
| caption        =
| director       = Andrew Marton
| producer       =
| writer         =
| starring       = Stewart Granger Bronislau Kaper
| cinematography = Robert L. Surtees John D. Dunning
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $1,282,000 The Eddie Mannix Ledger’, Margaret Herrick Library, Center for Motion Picture Study, Los Angeles 
| gross          = $4,007,000 
}} American western film directed by Andrew Marton and starring Stewart Granger, Wendell Corey and Cyd Charisse. 

== Plot == trapper goes on the run in the Canadian wilderness, pursued by a lawman.

== Cast ==
* Stewart Granger - Jules Vincent
* Wendell Corey - Constable Pedley
* Cyd Charisse - Indian girl
* Morgan Farley - Father Simon
* J.M. Kerrigan - Callahan
* Howard Petrie - Mike Brody
* Houseley Stevenson - Old Man Lewis Martin - Sergeant
* John War Eagle - Indian Chief
* Ray Teal - Ruger
* Clancy Cooper - Sloan

==Production==
The film was known at one stage as Constable Pedley. 

==Reception==
The film earned an estimated $2 million at the North American box office in 1952.  MGM records puts this figure at $2,111,000 with earnings of $1,896,000 elsewhere, leading to a profit of $806,000. 

In France, the film recorded admissions of 1,746,799.   at Box Office Story 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 