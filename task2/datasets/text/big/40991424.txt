Il sergente Rompiglioni
{{Infobox film
 | name =Il sergente Rompiglioni
 | image =  Il sergente Rompiglioni.jpg
 | caption = Pier Giorgio Ferretti
 | writer =  Roberto Gianviti   Fiorenzo Fiorentini
 | starring =   
 | music = Berto Pisano
 | cinematography = Sergio Rubini
 | editing =  
 | producer = 
 | distributor =
 | released = 1973
 | runtime =
 | awards =
 | country =
 | language =  Italian
 | budget =
 }} 1973 Cinema Italian comedy film directed by Giuliano Biagetti (here credited Pier Giorgio Ferretti).  The film got a great commercial success, grossing over one billion lire. Franco e Ciccio Superstar", Cine70, Volume 4, Coniglio Editore, 2003.  It was the greatest commercial success of Franco Franchi in his solo career after the split from Ciccio Ingrassia.  The film has a sequel,  , still starred by Franchi in the title role.

== Cast ==

*Franco Franchi: Sgt. Francesco Garibaldi Rompiglioni
*Francesca Romana Coluzzi: Semiramide
*Mario Carotenuto: Colonel Guglielmo
*Corinne Cléry: The daughter of the Colonel (credited as Corinne Picolo)
*Enzo Andronico: The Lieutenant
*Nino Terzo: Lance Corporal Baffo

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 