Numafung
{{multiple issues|
 
 
 
}}

{{Infobox film
| name           = Numafung
| image          =  
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = 
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}

Numafung is a 2004 Limbu language film that portrays the culture and tradition of the Limbu community.   It is directed by Nabin Subba. The film was screened at the Thirteenth Finnish Indigenous Film Festival.  The lead characters in the movie are Anupama Subba, Niwahangma Limbu, Prem Subba, Alok Nembang and Ramesh Singhak. Numafung mean a "beautiful flower" in the Limbu language. The majority of shooting took place at Panchthar, Nepal and Sikkim.

==References==
 

==External links==
*  
* http://xnepali.net/movies/nepali-movies/nepli-movie-numafung/

 
 
 

 