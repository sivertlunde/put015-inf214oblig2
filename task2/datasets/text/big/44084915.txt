Kochu Thampuratti
{{Infobox film 
| name           = Kochu Thampuratti
| image          =
| caption        =
| director       = Rochy Alex
| producer       =
| writer         = Purushan Alappuzha
| screenplay     = Purushan Alappuzha Vincent
| music          = A. T. Ummer
| cinematography = PN Sundaram
| editing        = NP Suresh
| studio         = Panchami Pictures
| distributor    = Panchami Pictures
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, Vincent in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Cochin Haneefa
*Sharmila Vincent

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Bharanikkavu Sivakumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Poonilaappakshi || Ambili, Karthikeyan || Bharanikkavu Sivakumar || 
|-
| 2 || Raginee nee || KP Brahmanandan || Bharanikkavu Sivakumar || 
|}

==References==
 

==External links==
*  

 
 
 

 