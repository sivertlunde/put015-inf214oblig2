Green Grow the Rushes (film)
{{Infobox film
| name           = Green Grow the Rushes
| image          = 
| image_size     = 
| caption        = 
| director       = Derek N. Twist
| producer       = John W. Gossage
| writer         = Derek N. Twist Howard Clewes
| narrator       = 
| starring       = Roger Livesey Richard Burton Honor Blackman
| music          = Lambert Williamson
| cinematography = Harry Waxman
| editing        = 
| distributor    = British Lion Films
| released       = November 1951
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} ACT Films Ltd. The British Film Catalogue, 11606.  Monthly Film Bulletin, 1951 page 371.  The film was produced by John Gossage and funded by the National Film Finance Corporation and the Co-operative Wholesale Society Bank. 

==Plot==
Three British government bureaucrats arrive in Kent to inquire as to why the coastal Anderida marsh is not being cultivated. The reason is that most of the local people know about or are involved in the liquor smuggling scheme operated by Captain Biddle and his accomplice Robert (Richard Burton), who is posing as a fisherman when he is seen by the newspaper editor and his journalist daughter Meg.

Robert persuades them not to report it in the newspaper, and tells Biddle about his encounter with them. Biddle does not like the idea of any local “Lily White” knowing about their illegal activity; he was once married to a Lily White. The smugglers’ next cargo gets caught in a violent storm, and their boat washes inland, settling in the meadow of a farmer whose wife Polly happens to be Biddle’s ex-wife.

==Cast==
* Richard Burton - Robert Hammond
* Honor Blackman - Meg Cuffley
* Roger Livesey - Captain Cedric Biddle
* Frederick Leister - Colonel Gill
* Arnold Ridley - Tom Cuffley
* John Salew - Herbert Finch
* Colin Gordon - Roderick Fisherwish
* Geoffrey Keen - Spencer Prudhow
* Russell Waters - Joseph Bainbridge
* Vida Hope - Polly Cyril Smith - Constable Hewitt
* Jack McNaughton - Sgt. Edgar Rigby
* Eliot Makeham - James Urquhart (Coast Guard)
* Gilbert Davis - Whitley
* Harcourt Williams - Chairman of the Bench Archie Duncan - Constable Pettigrew
* Bryan Forbes - Fred Starling (Biddle’s crewman) Harold Goodwin - Gosling (Biddle’s crewman)
* Henrik Jacobsen - Sigismund (Biddle’s crewman)

==Background==
Based on the 1949 novel Green Grow the Rushes by Howard Clewes. The title, at least, is inspired by the 18th-century folk song "Green Grow the Rushes, O", in which each of the 12 verses after the first has the penultimate line, “Two, two, the lily-white boys, clothed all in green O.” The song is not heard in the movie, nor is there any hint as to how the Lily White people Biddle talks about are different from anyone else.

==Production==
The film was filmed on the coastal Romney Marsh around the town of New Romney, with some scenes set around the church at St Mary in the Marsh.

==Subsequent release==
The film was re-released in 1954 under the alternative title Brandy Ashore. 

==See also==
* Variety (magazine)|Variety (weekly) 21 November 1951.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 