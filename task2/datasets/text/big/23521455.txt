July 14th (film)
 

{{infobox film name        =   July 14th director    =   Govind K Balaji country     =    India language  Tamil
}} Tamil short film, directed by Govind K Balaji, and produced by Youth Film Makers starring Fareed and Shabana. The film had many debutants and was one of the most successful short films of the year in Tamil cinema. 

== Plot ==
Its a story of a guy called Cheran (Fareed), who goes through several stages of life to secure future for his widow mother and a younger sister. This film involves true friendship, love and affection. It shows as how important is life insurance for everyone.

== Telecast ==
July 14th was telecast on Tamilzan channel on August 15, 2004 and because of the public demand the movie was re-telecast after two months on Makkal TV.

== Cast ==
* Fareed - Cheran
* Shabana - Kousalya
*Dr. Vikram krishnan - Friend of Cheran 

 
 
 


 