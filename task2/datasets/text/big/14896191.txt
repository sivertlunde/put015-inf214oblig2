Flower Island
 
{{Infobox film name           = Flower Island image          = Flower Island.jpg caption        = Poster to Flower Island director       = Song Il-gon  producer       = Chang Yoon-hyun writer         = Song Il-gon narrator       =  starring       = Seo Joo-hee Im Yoo-jin music          = Roh Young-sim Lee Jae-jun Jeong Jae-il cinematography = Kim Myung-jun editing        = Moon In-dae distributor    = C&Film Production released       =   runtime        =  country        = South Korea language       = Korean budget         =  gross          = 
}}
Flower Island ( ) is a 2001 South Korean film directed by Song Il-gon. This was Songs first feature-length film after directing several award-winning short films.

==Plot==
The film is in three parts. The first introduces three women who are separately suffering from their own psychological injuries. In the second part, the three women encounter each other and other people as they are each on a journey to an island reputed to have healing powers. The last third of the film deals with the characters on a boat headed to the island. 

==Awards==
* 2002 Fribourg International Film Festival: FIPRESCI Prize, "For its sensitive portrait of three human destinies, within an accomplished and mature cinematographic grammar." 
* 2002 Fribourg International Film Festival: Special Mention: Song Il-gon
* 2001 Directors Cut Awards: Best New Director, Song Il-gon
* 2001 Pusan International Film Festival: FIPRESCI Prize New Currents, "For its remarkable direction in creating the interior universe of three women, and for its fine performances."
* 2001 Pusan International Film Festival: New Currents Award, Song Il-gon
* 2001 Pusan International Film Festival: PSB Audience Award, Song Il-gon
* 2001 Venice Film Festival CinemAvvenire Award Best First Film, Song Il-gon

==Notes==
 

==Bibliography==
*  
*  
*  
*  

 
 
 


 