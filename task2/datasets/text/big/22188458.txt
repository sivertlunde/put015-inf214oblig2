The Sunday Woman (film)
 
{{Infobox film
| name           = The Sunday Woman
| image          = The Sunday Woman (film).jpg
| caption        = Film poster
| director       = Luigi Comencini
| producer       = Marcello DAmico
| writer         = Carlo Fruttero Agenore Incrocci Franco Lucentini Furio Scarpelli
| starring       = Marcello Mastroianni
| music          = Ennio Morricone
| cinematography = Luciano Tovoli
| editing        = Antonio Siciliano
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Italy France
| language       = Italian
| budget         = 
}}
 same name.

==Cast==
* Marcello Mastroianni as Commissioner Salvatore Santamaria
* Jacqueline Bisset as Anna Carla Dosio
* Jean-Louis Trintignant as Massimo Campi
* Aldo Reggiani as Lello Riviera
* Maria Teresa Albani as Virginia Tabusso
* Omero Antonutti as Benito
* Gigi Ballista as Vollero
* Fortunato Cecilia as Nicosia (as Renato Cecilia)
* Claudio Gora as Garrone
* Franco Nebbia as Bonetto
* Lina Volonghi as Ines Tabusso
* Pino Caruso as De Palma
* Mario Ferrero
* Giuseppe Anatrelli as Commissario
* Antonio Orlando as Salvatore

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 

 
 