Obscurity (film)
 
 
 
{{Infobox film
 | name           = Obscurity
 | image          = Obscurity_Poster.png
 | caption        = Final release poster
 | director       = George Grant
 | producer       = 9eight Films
 | writer         = George Grant
 | starring       = {{Plainlist|
*George Grant
*Joe Saunders
*Kelsey Lawther
*Nikk Uttley
*Ben Major
*Sam Easterby
}}
 | music          = 
| editing        = Matt Filer
 | studio         = {{Plainlist|
*Matt Filer Films
*9eight Films
*Grant Productions }}
 | distributor    = Matt Filer Films
 | released       =   }}
 | runtime        = 41 minutes
 | country        = United Kingdom
 | language       = English
}}

Obscurity, is a 2014 drama film written by, directed by and starring George Grant and filmed and edited by Matt Filer.      
The film is the first feature to be created by George and Matt after their success of short films before it. Obscurity was written, shot and edited over about 5 months and premiered at Priory Community School to a positive response on 7 February 2014 before being published to YouTube on 10 February.   

The feature was created to raise money for the charity Weston Hospicecare. It has been praised for its fight choreography and action sequences.      

== Plot ==



The film follows the life of 15 year old boy called Josh who is bullied by his older brother and morally broken down by the gang of youths that hang around outside his house. The film follows him over a few days where his relationships with friends and family break down which leads up to an ending where his life is changed forever.

The film starts out with a sequence showing Josh looking through draws frantically trying to find a knife to self-harm with. After he finds a knife, he bottles it, and throws a glass bottle across the room. At this point he is met with Jonah, his older brother who is immediately incredibly angry at Josh, indicating that Joshs attempt to self harm-could have been caused by something his older brother had previously done to him. Jonah bullies Josh to the point where he runs upstairs to his room where he slams the door shut to reveal the title sequence.
 outcast and decided to join the gang that hang out in Joshs road. This upsets Josh because it makes him feel like Max isnt friends with him anymore, although he still wants to be, however having Max inside the group works well for Josh as he can find out what they are planning against him. Josh turns to Danny for support when Max wont listen, although they get into an argument and go their separate ways after being beaten up by two of the gang members outside of school.

After coming home to find Jonah attacking Kyle, Josh finally has enough and storms out. When he is confronted by the gang outside again he snaps and has a fight with them before running off back into his house. This angers the group and they begin plotting the attack which ends the film.

In the final part of the film, the gang outside launch an attack on Josh, breaking into his house. He manages to fight them off, killing some of them. The few that he doesnt kill walk off defeated but are confronted by Jonah who is just coming back to the house. Jonah and the two remaining gang members have a fight outside the house, watched by Jonah and Kyle. 

== Cast ==

*George Grant as Josh, the protagonist, a 15 year old boy who the film centres around.
*Joe Saunders as Jonah, the antagonist, who bullies Josh and Kyle and is the main cause of Joshs anger problems.
*Kelsey Lawther as Kyle, Joshs brother who is also bullied by Jonah. outcast him.
*Ben Major as Danny, Joshs friend who stops talking to him after he finds out that Josh is trying to be killed by the gang.
*Nikk Uttley as Marcus, the gang leader who attacks Josh multiple times during the film.

== Production ==

Obscurity was filmed and edited in just under 40 days and written within about a month. The film had such a quick turnaround because of the small scale of the production team.

Since its quick turnaround was mostly editing and writing, the filming was only over about 11 days which meant that some scenes were rushed and because of this, in some scenes you can see the camera or lighting equipment. The location for the film proved a challenge, since it was filmed in a real school and a real house which left little room for equipment or crew. Another problem was caused by the mirrors in the house, since the filming time was so limited the house was only slightly modified for the filming. Work was done in post production to remove reflections of the crew from mirrors which added extra time to the production.
 key framing transitions and Motion 5 outside of the films editing software Final Cut Pro X.   

== Release ==

 

The film was released on 7 February 2014 to a limited number of viewers at Priory Community School where money was taken on the door that went to Weston Hospicecare. On 10 February 2014 the film was released worldwide on YouTube.     

== Reception ==

The film has had positive reviews praising its fight choreography and final action sequence as well as the main storyline.   
It has however been criticised for its presentation of the gang throughout the film, saying that the gang should have played a more dominant or threatening role throughout more of the film.     The film was praised for its music and lensing. 

== References ==

 

== External links ==

*  
*  
*  
*  
*  
*  

 
 
 
 
 
 