Quemar las Naves
{{Infobox Film  |
  name     = Quemar las naves |
  image          = |
  producer       = Laura Imperiale, Maria Navarro & Francisco Franco Alba |
  director       = Francisco Franco Alba |
  writer         = Maria Reneé Prudencio & Francisco Franco Alba |
  starring       = Irene Azuela Ángel Onésimo Nevárez Claudette Maillé |
  music          =  Alejandro Giacomán, Joselo Rangel|
  cinematography =  Erika Licea|
  editing        = Sebastián Garza|
  distributor    = Instituto Mexicano de Cinematografía (Imcine)|
  released       = October 2007 |
  runtime        = 104 min. |
  language       = Spanish |
  budget         = 
}}

Quemar las naves (English title: Burn the Bridges) is a 2007 Mexican film directed by Francisco Franco Alba, from an original script co-written by Franco and actress Maria Reneé Prudencio. The film was shot in the Mexican state of Zacatecas and addresses issues such as self-assertion, loss, adolescence, and sexual relations. It officially premiered in October 2007 at the Festival Internacional de Cine de Morelia. The title refers to a saying, which means "to cut all ties holding someone to something or someone", widely known in Mexico due to the fact that Spanish conqueror Hernan Cortes did it literally when he and his men set foot for the first time in continental America, in order to avoid mutiny or desertion.

==Plot==
Helena (Irene Azuela) and Sebastián (Ángel Onésimo Nevárez) are a teenaged brother and sister who have been reunited at their home while looking after their ailing mother, singer Eugenia Díaz (Claudette Maille). Eugenia has been diagnosed with cancer and doesnt have long to live, and it has fallen to Helena and Sebastián to care for her in her last months, with Helena (who is older) spending her days with her mother while Sebastian goes to school and looks in on Eugenia in the evenings. Helena is obsessively caring for Sebastián, but while he loves his sister and mother, he seems detached from the affairs of his family. Hes more drawn to his roughneck schoolmate Juan (Bernardo Benítez). As the reality of Eugenias fate becomes clearer, Helena and Sebastiáns confusion about love, mortality and family becomes more acute, and circumstances become even more muddled when they take in a boarder, Aurora (Jessica Segura), who falls in Sebastiáns wealthy schoolmate Ismael (Ramón Valdez Urtiz).

==Cast==
*Irene Azuela as Helena
*Ángel Onésimo Nevárez as Sebastián
*Claudette Maillé as Eugenia
*Bernardo Benítez as Juan
*Ramón Valdez Urtiz as Ismael
*Juan Carlos Barreto as Efraín
*Jessica Segura as Aurora

==Awards==
The film won two Ariel awards in 2008: for Best Actress (Irene Azuela) and for Best Original Music (Alejandro Giacomán, songs by Joselo Rangel).

==External links==
*  

==References== El Universal
* , Anodis
* 

 
 
 
 
 
 