Bobby (2006 film)
 
{{Infobox film
| name           = Bobby
| image          = Bobby poster.jpg
| image_size     = 220px
| alt            = 
| caption        = Theatrical release poster
| director       = Emilio Estevez
| producer       = Michael Litvak Holly Wiersma
| writer         = Emilio Estevez Heather Graham Freddy Rodriguez Martin Sheen Christian Slater Sharon Stone Mary Elizabeth Winstead Elijah Wood 
| music          = Mark Isham Michael Barrett
| editing        = Richard Chew
| studio         = Holly Wiersma Productions Bold Films
| distributor    = The Weinstein Company  
| released       =  
| runtime        = 116 minutes  
| country        = United States
| language       = English Spanish
| budget         = $14 million 
| gross          = $20.7 million 	
}} shooting of The Ambassador 1968 Democratic Party presidential primary in California.

==Plot== senator intercut Grand Hotel, and later used by Robert Altman in Nashville (film)|Nashville.
 deployed to battlefields of switchboard operator acid trip they take with the help of drug dealer Fisher; married socialites and campaign donors Samantha and Jack; campaign manager Wade and staffer Dwayne, who is in a love interest with Angelas colleague, Patricia; and Czechoslovak reporter Lenka Janáčková, who is determined to get an interview with Kennedy.
 City Club of Cleveland, Ohio, is played over a montage of the reaction of those present to the assassination. Several of the characters get wounded, such as William, Jimmy & Cooper, Samantha and Daryl. At the end of the film we are told that they all survived and Bobby died the next day, with his wife, Ethel, at his side. 

==Cast==
 
* Harry Belafonte as Nelson
* Joy Bryant as Patricia
* Nick Cannon as Dwayne Clark
* Emilio Estevez as Tim Fallon
* Laurence Fishburne as Edward&nbsp;Robinson
* Lindsay Lohan as Diane Howser
* Dave Fraunces as Robert F. Kennedy
* Jeridan Frye as Ethel Kennedy
* Spencer Garrett as David
* Brian Geraghty as Jimmy Heather Graham as Angela
* Anthony Hopkins as John Casey
* Helen Hunt as Samantha
* Joshua Jackson as Wade Buckley
* David Kobzantsev as Sirhan Sirhan
* David Krumholtz as Agent Phil
* Ashton Kutcher as Fisher
* Shia LaBeouf as Cooper
* William H. Macy as Paul Ebbers
* Svetlana Metkina as Lenka
* Demi Moore as Virginia Fallon Freddy Rodriguez as José Rojas
* Martin Sheen as Jack
* Christian Slater as Daryl Timmons
* Sharon Stone as Miriam Ebbers
* Jacob Vargas as Miguel
* Mary Elizabeth Winstead as Susan&nbsp;Taylor
* Elijah Wood as William Avary
 

==Production==

===Development===
In Bobby: The Making of an American Epic, screenwriter/director Emilio Estevez discusses the problems he had developing his script. Suffering from writers block, he checked into a motel in Pismo Beach where he hoped, free from interruption, he could make some headway with his writing. While talking to the woman working at the front desk, he discovered she had been in the Ambassador Hotel on the evening Kennedy was shot, and later married two young men to help them avoid the draft. Estevez used her experience to mold the character of Diane, and the rest of the story fell into place.

The only other character based on a real person is busboy José, who represents Juan Romero, the young man who was photographed cradling Kennedys body immediately after he was shot. The character of José has tickets to the Los Angeles Dodgers game in which Don Drysdale is expected to set the record of six consecutive shutouts, but is obliged to work a double shift, forcing him to miss the game. Drysdale did in fact achieve his sixth shutout on June 4, 1968, and was congratulated by Kennedy during the victory speech he delivered just before being shot. 

===Filming===
  Ambassador Hotel had shuttered its doors and put its fixtures and furnishings up for auction during the films pre-production period, giving production designer Patti Podesta the opportunity to purchase much of the furniture and decorative pieces used in the film. The hotel was scheduled for demolition one week after filming was scheduled to begin. Estevez got permission to film in and around the building during that period, adding to the films authenticity.

===Music===
 
The film score was composed by Mark Isham, with "Never Gonna Break My Faith" written by Bryan Adams and performed by Aretha Franklin, Mary J. Blige, and the Boys Choir of Harlem, which was played during the closing credits. As well, a newly recorded version of "Louie Louie" was performed in character by Demi Moore for the film.
 I Was Season of Jack Jones, "Magic Moments" by Perry Como, "Pata Pata" by Miriam Makeba, and "Initials" from the musical Hair (musical)|Hair.

The soundtrack album Bobby features The Supremes, Shorty Long, Hugh Masekela, The Moody Blues and Los Bravos.

==Release==
  for Bobby s North American debut.]] premiere at AFI Fest before going into limited release in the US on November 17, 2006, and a wide release in the subsequent week.

===Box office===
Playing on two screens, it grossed $69,039 during its opening weekend. It eventually earned $11,242,801 in North America and $9,461,790 in other territories for a worldwide box office of $20,704,591. 

===Critical reception===
Bobby received mainly mixed reviews from critics, who praised its ensemble cast and the direction of Emilio Estevez but criticized the film for having too many plot points and characters. It has a rating of 46% on Rotten Tomatoes, based on 169 reviews, with an average score of 5.6 out of 10. The consensus states, "Despite best intentions from director Emilio Estevez and his ensemble cast, they succumb to a script filled with pointless subplots and awkward moments working too hard to parallel contemporary times."  The film also has a score of 54 out of 100 on Metacritic, based on 31 critics, indicating mixed or average reviews. 

A. O. Scott of The New York Times said, "Emilio Estevez . . . sets himself a large and honorable task. It is important to appreciate this in spite of his movies evident shortcomings. Intentions do count for something, and Mr. Estevezs seem to me entirely admirable . . . The actors seem more like very special guest stars than like real, 1968-vintage Americans, and their period-appropriate get-ups . . . are more distracting than convincing . . . Some of the stories feel too obviously melodramatic, while others are vague to the point of inscrutability. In the Vietnam- and drug-related plots, the point is hammered home too hard . . . while other narratives wind toward no discernible point at all. Nonetheless the ambition behind Bobby is large and serious." 
 Altmanesque interplay of Nashville (film)|Nashville or Short Cuts but instead feels like one of those 70s disaster epics such as Earthquake (film)|Earthquake or The Towering Inferno, in which a star-studded cast endures melodramatic story lines as the audience awaits the inevitable momentous event and tries to guess who will be around at the finish . . . Its easy to become swept up in the palpable enthusiasm Estevez shows toward his subject, but the pedestrian and overly expositional dialogue of the films characters proves to be as stifling as the excerpts from Kennedys speeches are stirring." 

Deborah Young of Variety (magazine)|Variety said of Estevez, "Stepping up as writer and director in a way he never has before,   successfully pulls together a complexly designed narrative," and added the film "carries an eerie topicality that makes many of its insights instantly click."  Armond White of New York Press wrote that the film "has a humane sweetness", and that it "literally and vividly unites different ethnic groups, labor strata and social castes" in a way that "is not schematic—its exactitude and believability has a Tocquevillian brilliance." 

Steve Persall of the St. Petersburg Times graded the film C, calling it "a misguided jumble of too much fiction, few facts and zero speculation" and Estevez "a mediocre filmmaker."  Michael Medved, who was in the Ambassador ballroom (20 feet from the podium) the night Kennedy was shot, awarded the film three out of four stars and called it "intriguing but imperfect." He added, "Emilio Estevez gets most of the feelings of the occasion right. But, the melodramatic, multi-character format proves somewhat uneven and distracting." 

Richard Roeper said, "Estevez writes and directs with lots of passion, not so much subtlety . . .   wants the movie to be on the level of a Robert Altman film like Nashville but falls short."  Peter Travers of Rolling Stone gave the film one star and called it "trite fiction" and a work of "insipid ineptitude." He ranked it among the worst films of 2006, as did Lou Lumerick of the New York Post, who dubbed it an "ambitious, but utterly wrong-headed trivialization." 

==Awards and nominations==
* ALMA Award for Outstanding Motion Picture (nominee)
* ALMA Award for Outstanding Director - Motion Picture (nominee)
* ALMA Award for Outstanding Screenplay - Motion Picture (nominee)
* Broadcast Film Critics Association Award for Best Cast (nominee)
* Golden Globe Award for Best Motion Picture — Drama (nominee)
* Golden Globe Award for Best Original Song ("Never Gonna Break My Faith" by Bryan Adams, Eliot Kennedy, and Andrea Remanda, nominee)
* Hollywood Film Festival Award for Best Ensemble Cast (winner)
* Hollywood Film Festival Award for Best Breakthrough Actress (Lindsay Lohan, winner)
* NAACP Image Award for Outstanding Supporting Actor in a Motion Picture (Harry Belafonte, nominee)
* Phoenix Film Critics Society Award for Breakout Performance of the Year - Director (winner)
* Screen Actors Guild Award for Outstanding Performance by a Cast in a Motion Picture (nominee)
* Venice Film Festival Biografilm Award (Emilio Estevez, winner)
* Venice Film Festival Golden Lion (Estevez, nominee)

==See also==
 
* 2006 in film
* Cinema of the United States
* List of American films of 2006
* JFK (film)|JFK, a 1991 film

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 