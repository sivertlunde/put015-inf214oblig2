Z for Zachariah (film)
{{Infobox film
| name           = Z for Zachariah
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Craig Zobel
| producer       = Skuli Fr. Malmquist  Thor Sigurjonsson   Sigurjón Sighvatsson  Tobey Maguire    Matthew Plouffe   Sophia Lin
| writer         = Nissar Modi
| based on       =  
| starring       = Margot Robbie   Chiwetel Ejiofor   Chris Pine 
| music          = Heather McIntosh
| cinematography = Tim Orr
| editing        = Jane Rizzo 
| studio         = Zik Zak Filmworks    Material Pictures   Lucky Hat Entertainment 
| distributor    = Roadside Attractions
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 book of Robert C. OBrien, directed by Craig Zobel and written by Nissar Modi. The film stars Margot Robbie, Chris Pine and Chiwetel Ejiofor.

== Cast ==
* Margot Robbie as Ann Burden 
* Chris Pine as Caleb 
* Chiwetel Ejiofor as Loomis 

== Production ==

=== Casting ===
On May 15, 2013, actors Amanda Seyfried, Chris Pine and Chiwetel Ejiofor joined the cast of the film, Seyfried playing lead character Ann Burden, a village girl who survived a nuclear war and Ejiofor playing John Loomis, a coldly rational scientist.    On January 18, 2014, it was announced that actress Margot Robbie had replaced Seyfried in the cast.   

=== Filming ===
The shooting of the film Z for Zachariah began on January 27, 2014 in Canterbury Region|Canterbury, New Zealand around the city of Christchurch   On March 1, 2014, Pine was charged with driving drunk after attending an end-of-filming party at a pub in Methven, New Zealand|Methven, Canterbury. He was subsequently disqualified from driving for six months and ordered to pay New Zealand dollar|NZ$93 in reparation.  

==Release==
The film premiered at the Sundance Film Festival on January 24, 2015. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 


 