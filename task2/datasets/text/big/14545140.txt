Calendar (1993 film)
 
{{Infobox film
| name = Calendar
| image_size =
| image	= Calendar FilmPoster.jpeg
| caption =
| director = Atom Egoyan
| producer = Atom Egoyan Arsinée Khanjian Robert Lantos
| writer = Atom Egoyan
| starring = Arsinée Khanjian Ashot Adamyan Atom Egoyan
| music = Eve Egoyan Djivan Gasparian Hovhanness Tarpinian John Grimaldi
| cinematography = Atom Egoyan Norayr Kasper
| editing = Atom Egoyan
| distributor = Zeitgeist Films
| released =  
| runtime = 74 minutes
| country = Canada Germany Armenia Armenian
| budget =
| gross =
}}
Calendar ( ) is a 1993 drama film directed by Atom Egoyan.

==Plot==
A photographer is sent to Armenia to take pictures of churches for a calendar. He slowly begins to realise that his wife, an Armenian translator, is falling in love with their driver and unofficial guide, Ashot. They grow more and more distant from each other and finally separate. Later, at his home in Toronto, he uses an escort agency to invite a number of women to dinner, finally settling on the one who looks and sounds most like his wife.

==Style==
The film is narrated by the photographer. Interactions between the photographer, his wife, and their driver were largely improvised. {{cite news
 | last = Sheley
 | first = Aaron
 | title = Art Film – Atom Egoyans Calendar
 | publisher = Entertainment Today
 | date = 2007-01-18
 | url = http://entertainmenttoday.net/content/view/67/
 | accessdate = 2007-12-03 }} 

===Locations===
The story is told almost entirely from only three locations: In Armenia, at the photographers dining room in Toronto, and by the photographers answering machine.

====Armenia====
Every scene in Armenia is viewed from behind a camera as the photographer prepares to take pictures of the churches (including a moment where the photographer and his wife mistake the pagan temple of Garni for a church); his wife and driver speak to him while looking directly at the camera. The scenes were shot with a video camera.

====Dining room====
The scenes in the dining room feature the photographer having dinner with women from the escort agency. Each date follows almost exactly the same pattern: The photographer and his date converse briefly, the photographer pours the wine, and the date excuses herself to use the telephone in the next room while the photographer listens. It is revealed on the last date that this pattern was set up prior to each date, and that this is his way of finding a woman who sounds like his wife, although his motives for doing so are left ambiguous.

====Answering machine====
The photographers answering machine sits beside the Armenian calendar, which marks the passage of time throughout the movie. We learn of the state of his marriage through the messages left by his estranged wife.

==Critical reception==
Despite its limited release, Calendar received mostly positive reactions. It has a 100 percent rating at Rotten Tomatoes  and was nominated for Best Achievement in Direction and Best Screenplay at the 1993 Genie Awards. {{cite web
  | title = Awards for Calendar
  | publisher = IMDb
  | url = http://www.imdb.com/title/tt0106504/awards
  | accessdate = 2007-12-03 }}  Stephen Holden of the New York Times said of the movie,
 If Calendar, like such earlier Egoyan films as The Adjuster and Speaking Parts, has to be pieced together backward, it is so finely constructed and beautifully acted a movie that its game of detective is quite enticing. Seamlessly edited, the film sustains a visual rhythm that is as confident as it is edgy. {{cite news
 | last = Holden
 | first = Stephen
 | title = Technology, a Tripod, A Romantic Triangle
 | publisher = New York Times
 | date = 1993-10-16
 | url = http://movies.nytimes.com/movie/review?_r=2&res=9F0CE6D6143AF935A25753C1A965958260&oref=slogin&oref=login
 | accessdate = 2007-12-03 }} }}

However, not all reviews were positive. Rita Kempley of the Washington Post said, "  approach remains far too cerebral to evoke more than intellectual interest". {{cite news
 | last = Kempley
 | first = Rita
 | title = Calendar
 | publisher = Washington Post
 | date = 1994-05-27
 | url = http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/calendarpgkempley_a0a462.htm
 | accessdate = 2007-12-03 }} 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 