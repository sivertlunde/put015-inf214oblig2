Dolemite
 
{{Infobox film
| name           = Dolemite
| image          = Dolemite.jpg
| image_size     =
| caption        = Original one-sheet poster for Dolemite
| director       = DUrville Martin
| producer       = Rudy Ray Moore
| writer         = Rudy Ray Moore (story) Jerry Jones (screenplay)
| narrator       =
| starring       = Rudy Ray Moore DUrville Martin Jerry Jones Lady Reed Hy Pyke West Gale John Kerry Vainus Rackstraw
| music          = Don Cornelius
| cinematography = Nicholas Josef von Sternberg
| editing        = Rex Lipton Dimension Pictures (1975, original) Xenon Pictures (2005, DVD)
| released       = US July 1, 1975
| runtime        = 90 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 toast by a local homeless man about an urban hero named Dolemite, and decided to adopt the persona of Dolemite as an alter-ego in his act. 
 Billboard charts. He released several more comedy albums using this persona. In 1975, Moore decided to create a film about Dolemite, using many of his friends and fellow comedians as cast and crew. The film was directed by DUrville Martin, who appears as the villain Willie Green.

==Plot==
Dolemite is a pimp who was set up by Willie Greene and the cops, who have planted drugs, stolen furs, and guns in his trunk and got him sentenced to 20 years in jail. One day, Queen B and a warden planned to get him out of jail and get Willie Green and Mitchell busted for what they did to Dolemite. However, Dolemite is no stupid man and has a lot of "warriors" backing him, such as his call girls, who are karate experts, and many more.

==Cast== flashback of detectives examining the trunk of his car in which stolen fur coats and drugs were found. It is obvious Dolemite was frameup|framed, but he spends time in jail nevertheless. Dolemite is later pardoned and released. Throughout the movie, as Dolemite attempts to rekindle his reputation on the streets and reclaim his pride and joy (the club referred to as "The Total Experience", which was a real-life club owned by music executive Lonnie Simmons) from Willie Green. He is constantly having run-ins with a pair of detectives, Mitchell and White, who framed Dolemite before (as seen in the flashback), and who are hell-bent on getting Dolemite back into prison.
*Queen Bee (Lady Reed): runs a whorehouse that references Dolemite as the part-time owner on several occasions throughout the movie. Queen Bee is the only woman in Dolemites household whom Dolemite speaks to as an equal, rather than a pimp. While Queen Bee is very emotional about having Dolemite return home after time in jail, no reference of an intimate relationship is ever made during the film.
*Willie Green (DUrville Martin): the antagonist. Willie Green is seen in the initial flashback as having a leading part in the framing of Dolemite. Willie Green takes over Dolemites club "The Total Experience" while Dolemite is serving hard time. Willie Green and the citys mayor, Mayor Daley, have a peculiar partnership. The mayor will abuse his office in helping Willie Green avoid problems with the law, while Willie Green promises black votes for the mayors upcoming re-election.
*Reverend Gibbs (West Gale): a black separatist with many connections, the Reverend leads a radical church in the "Fourth Ward."  He tips off Dolemite regarding who set him up two years prior, as well as who is supplying drugs to the community.
*Mitchell (John Kerry): a corrupt detective who, under the direction of Mayor Daley and Willie Green, frames Dolemite and sends him to prison.  When Dolemite is released, he and his partner White attempt to frame him again.
*Blakely (Jerry Jones): an FBI man who lurks in the shadows, and knows why Dolemite is out on the street.  When the time comes, Blakely apprehends the corrupt detectives Mitchell and White and the corrupt mayor.
*Creeper (Vainus Rackstraw): better known as the Hamburger Pimp, he is recognized by his dingy "white-T", characteristic pimp stroll (as he pulls up his pants), and constant begging for spare change and free food. The Creeper takes Dolemite to his humble abode and is assassinated after explaining the murder of Dolemites nephew Little Jimmy.

==Follow-ups==
A sequel, The Human Tornado, was released in 1976.
A second sequel The Return of Dolemite was released in 2002 and was later renamed The Dolemite Explosion for DVD release. 
A quasi-sequel Shaolin Dolemite starring Rudy Ray Moore as Monk Ru-Dee was released in 1999.

==DVD / Bluray==
Dolemite was released to DVD on September 13, 2005 by Xenon Pictures and also as part of a box set (The Dolemite Collection) on the same date.  A widescreen, high definition remastered version, from an original print of the film, will be released on Bluray disc in late-2015 by cult film preservationists, Vinegar Syndrome  

==References==
 

==External links==
* 
*  
* 
* 

 
 
 
 
 
 