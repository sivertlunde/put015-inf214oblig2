Ingeborg Holm
{{Infobox film
| name = Ingeborg Holm
| image = 
| caption = 
| director = Victor Sjöström 
| producer = 
| writer = Nils Krok (also play) Victor Sjöström
| starring = Hilda Borgström
| music = 
| cinematography = Henrik Jaenzon 
| editing = 
| distributor = 
| released = 27 October 1913
| runtime = 96 min.
| country = Sweden
| language = Silent film
| budget = 
}}
 1913 cinema Swedish social 1906 play by Nils Krok. It has been called "the first realistic feature film".  It caused great debate in Sweden about social security, which led to changes in the poorhouse laws. It is said to be based on a true story. 

==Synopsis==
Sven Holm and his wife Ingeborg are happily married with three children, and are about to open a shop in Stockholm. They open the shop, but Sven contracts tuberculosis and dies. Ingeborg initially tries to run the shop by herself, but when she fails and develops a debilitating ulcer, she turns to the poorhouse for help. The poorhouse board does not grant her enough assistance to survive outside the workhouse. She has to sell the shop, her house, and board the three children out to foster families.

After some time, Ingeborg reads in a letter that her daughter, Valborg, is sick. The poorhouse cant finance a visit, but the determined Ingeborg escapes at night and, after being chased by police, gets to see the child. When she returns to the poorhouse, the manager is furious that they must pay a fine for the trouble she caused.

Later on, Ingeborg is offered a chance to see her youngest son, this time with the poorhouses approval. When the child doesnt recognize her, she is devastated. She tries to make a doll from her scarf and play with it, but the baby cries and turns to the foster mother.

This hits Ingeborg so hard that she loses her sanity. She is relegated to the insane womens ward of the workhouse, cradling a plank of wood as if her own child. After fifteen years, her oldest son, Erik, now a sailor, visits her without any knowledge of his mothers psychosis. He becomes desperate when Ingeborg doesnt recognize him&mdash;but when he shows her a youthful photograph of herself, which features the inscription "To Erik from mother," her sanity returns. With the return of her family comes the return of Ingeborgs self.

==Cast==
* Hilda Borgström as Ingeborg Holm
* Aron Lindgren as Sven Holm / Erik Holm as an adult
* Erik Lindholm as Employee in Shop
* Georg Grönroos as Poorhouse Superintendent
* William Larsson as Police Officer
* Richard Lund as Doctor
* Carl Barcklind as House Doctor
* Bertil Malmstedt as Erik Holm as a child

==References==
 

==External links==
*  
*  
* Ingmar Bergman on  

 

 
 
 
 
 
 