Janeane from Des Moines
{{Infobox film
| image          =
| alt            =
| caption        = Grace Lee
| producer       = {{plainlist|
* Grace Lee
* Jane Edith Wilson
}}
| writer         = {{plainlist|
* Grace Lee
* Jane Edith Wilson
}}
| starring       = Jane Edith Wilson
| music          = Ceiri Torjussen
| cinematography = Jerry A. Henry
| editing        = Aldo Velasco
| studio         = {{plainlist|
* Wilsilu Pictures
* Open Pictures
}}
| distributor    =
| released       =   }}
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Grace Lee, 2012 Republican Party primary in Iowa.  It mixes elements of mockumentary and real-life interviews with Republican politicians conducted in-character without their knowledge.

== Plot == Republican Party Republican Party primary in an effort to find a politician who can offer a solution to the problems she faces.  Her situation deteriorates quickly: her husband loses his job and comes out as gay, and she is diagnosed with cancer.  These and other issues drive her to question her long-held beliefs and attempt to find a politician who can offer more than sound bites.

== Cast ==
* Jane Edith Wilson as Janeane
* Michael Oosterom as Fred
* Melanie Merkovsky as Lissi

=== Interviews ===
* Mitt Romney
* Newt Gingrich
* Rick Santorum
* Michele Bachmann
* Rick Perry
* Anita Perry

== Production == narrative filmmaking techniques.  The politicians and media were unaware that Janeane is a fictional character, and some media outlets reported on her story.   

== Release ==
Janeane from Des Moines premiered at the 2012 Toronto International Film Festival.   It had a limited release in October 2012. 

== Reception ==
Dennis Harvey of Variety (magazine)|Variety wrote, "Grace Lees film can be billed (or dismissed) as a stunt, but it admirably refuses to go the predictable route of punking the candidates for easy satire or cheap laughs."   Frank Scheck of The Hollywood Reporter called it a "toothless satire" that "fails to make any compelling points."   Daniel M. Gold of The New York Times wrote that Wilson has a "rare talent for staying in character".  Gold identifies the major theme as "the immense gap between a desperate citizen and the politics she had hoped might help her."   Claude Peck of the Star Tribune rated it 2.5/4 stars and wrote, "This cinema demi-verité is a fascinating, discomfiting hybrid of real candidates and a fake constituent, but director Grace Lees own politics and tactics are something of a muddle."   Nick McCarthy of Slant Magazine rated it 1.5/4 stars and wrote, "Yet another entry in the canon of pandering pablum that audiences can expect in a presidential election year, Grace Lees Janeane from Des Moines would be an intriguing depiction of personal and political disillusionment if its conceit wasnt so transparent and lopsided." 

== References ==
 

== External links ==
*  

 
 
 
 
 
 