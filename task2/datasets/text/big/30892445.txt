Ennennum Kannettante
{{Infobox film
| name           = Ennennum Kannettante
| image          = EnnennumKannettante_DVDCover.jpg
| caption        = Ennennum Kannettante DVD cover
| alt            =  Fazil
| producer       = Sajan
| writer         = Madhu Muttam (story)  Fazil (screenplay, dialogues)
| starring       = Sangeeth  Sonia
| music          = Jerry Amaldev
| cinematography = Vipindas
| editing        = T. R. Shekhar
| studio         = Saj Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Ennennum Kannettante ( ) is 1986 Malayalam romance film written and directed by Fazil (director)|Fazil, starring newcomers Sangeeth and Sonia in the lead roles.  The film won the Kerala State Film Award for Best Popular Film for the year 1986. 
{{cite web
|url=http://www.prd.kerala.gov.in/stateawards2.htm
|title=State Film Awards: 1969 - 2008
|publisher=Information and Public Relations Department of Kerala
|accessdate=19 February 2011
}}  Srividya won the Kerala State Film Award for Second Best Actress also for this film.  Despite all the critical acclaim, the film was a box office failure. 
{{cite web
|url=http://www.hinduonnet.com/thehindu/fr/2005/12/23/stories/2005122301990100.htm
|title=His experiments with cinema
|author=Prema Manmadhan
|date=23 December 2005
|publisher=The Hindu
|accessdate=19 February 2011
|quote=My film Ennennum Kannettante was a failure at the box office, ironically it got the award for the best film with popular appeal. Tamil Fazil by the Karthick and Kushboo and was a huge hit at the Box-office.

==Plot==
Ennennum Kannettante is a teenage love story. It tells of the incomplete love of 19-year-old Kannan (Sangeeth) and 16-year-old Radhika (Sonia).

Kannan comes from Trivandrum to his ValiyaKoikkal tharavdu (ancestral house) with his mother (Srividya) to spend vacation and to attend the festival in their family temple, the Pookkulangara Devi temple. There are his relatives like grandfather Parameswara Kuruppu (Thilakan), grandmother (Sukumari), head of the family (Nedumudi Venu) and his aunt Vijayalakshmi (Jalaja) and her children. Everybody likes Kannan very much, and he has a good time along with his friend Gopu (Appa Haja). 

But with the arrival of his uncles daughter (murappennu) Radhika (Sonia G Nair) from Bangalore after a gap of nine years to perform her dance debut (Arangettam) in the temple, everyones attention turns to her. Kannan initially felt sad and angry but, once he sees Radhika, immediately falls in love with her. Kannan and Radhika were childhood friends and both have memories of their childhood. Radhika also likes Kannan but was afraid to reveal it to Kannan. To know whether Radhika loves him, Kannan plans to ask her secretly and hides himself in the bathroom. Radhika saw him, cries aloud. The whole family learns of the incident, and Kannan was blamed by all, including his mother. She sent message to Kannans father to take him back to Trivandrum for his exams. Meanwhile, Radhika revealed to Kannan that she loves him as much as he loved her. Soon, Kannans father took him back to the city.
 USA with her parents. Kannan rushed to the station. As he reached it, the train left and he couldnt meet Radhika one last time, even though he chased her train. Radhika left a souvenir for him in the platform, but the heartbroken Kannan didnt see it.

==Cast==
* Sangeeth as Kannan
* Sonia G Nair as Radhika
* Appa Haja as Gopu
* Nedumudi Venu as Tharavadu Karanavar
* Thilakan as Parameswara Kuruppu (Kannans grandfather)
* K. P. Ummer as Kannans father
* Srividya as Kannans mother
* Jagathy Sreekumar as Padmanabha Pilla
* Jalaja as Vijayalakshmi (Viji)
* Sukumari
*Meenakumari as Maheswari

==Soundtrack==
The soundtrack features seven songs composed by Jerry Amaldev with lyrics penned by Kaithapram Damodaran Namboothiri. It was the first work of noted poet Kaithapram as a lyricist.  Kaithapram used to write poems in Mathrubhumi, and this brought him to the notice of playback singer K. J. Yesudas, who asked him to write lyrics for his Tarangini Cassettes. Fazil heard some of these, and asked Kaithapram to write the lyrics for the film. 
{{cite web
|url=http://www.hinduonnet.com/thehindu/thscrip/print.pl?file=2008103150910300.htm&date=2008/10/31/&prd=fr&
|title=Artist of many dimensions 
|author=Suganthy Krishnamachari
|date=31 October 2008
|publisher=The Hindu
|accessdate=19 February 2011
}} 
# "Bhaagavatham" — M. G. Radhakrishnan  (Traditional lyrics by Ezhuthachan) 
# "Devadundhubhi" — K. J. Yesudas
# "Devadundhubhi" — K. J. Yesudas, Satheesh Babu & Sunanda
# "Kaakkem Keekkem" — Padma Subramaniam, Sunanda  (Lyrics by Madhu Muttam) 
# "Nizhalaay Pozhiyum" — K. J. Yesudas
# "Onnaanam Kulakkadavil" — Chorus
# "Poovattaka Thattichinni" — K. J. Yesudas, K. S. Chithra & Chorus
# "Thankathala Thaalam" — K. J. Yesudas & Chorus

==References==
 

==External links==
*  
*   at the Malayalam Movie Database AMMA Official website

 

 
 
 
 
 
 