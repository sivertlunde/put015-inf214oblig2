Dragnet (1987 film)
{{Infobox film
| name           = Dragnet
| image          = Dragnet movie.jpg
| caption        = Theatrical poster
| director       = Tom Mankiewicz
| producer       = Bernie Brillstein David Permut Robert K. Weiss
| writer         = Dan Aykroyd Alan Zweibel Tom Mankiewicz
| starring       = Dan Aykroyd Tom Hanks 
| music          = Ira Newborn
| cinematography = Matthew F. Leonetti
| editing        = William D. Gordeon Richard Halsey
| distributor    = Universal Pictures
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $20 million And now its Mankiewicz the director
Boedeker, Hal. Chicago Tribune (1963-Current file)   16 July 1987: D8.  
| gross          = $66,673,516 
}} buddy cop crime drama of the same name starring Jack Webb. The screenplay was written by Dan Aykroyd and Alan Zweibel. The original music score by Ira Newborn.
 homage to the long-running television series, Aykroyd plays Joe Friday (nephew of the original series star) while Hanks plays Pep Streebek, his new partner.  Harry Morgan reprises his role from the television series as Bill Gannon, now a Captain and Fridays and Streebeks boss.

==Plot== LAPD Sgt. Joe Fridays nephew and namesake (Aykroyd), whose anachronistic views reflect those of his late uncle, is involuntarily assigned a smart-alecky, streetwise new partner, Pep Streebek (Hanks). Their contrasting styles clash at first, Friday disapproving of his young partners attitude, hair, and wardrobe, but they gradually bond over their first case as a team, an investigation of a series of bizarre thefts involving items as disparate as a tanker full of chemicals, police, fire and paramedic vehicles, a wedding dress, a snake, a lions mane, and the entire monthly print run of the pornographic magazine "Bait".

They follow the trail to an Orange County-based cult calling itself P.A.G.A.N. (People Against Goodness And Normalcy). Friday and Streebek focus on one of the cults henchmen, a brutish limousine driver named Emil Muzz (Jack OHalloran).

Friday and Streebek disguise themselves and sneak into a secret P.A.G.A.N. ceremony where they witness the masked leader attempting to sacrifice a virgin, Miss Connie Swail (Alexandra Paul). Friday and Streebek disrupt the ceremony and save Swail. In the process, Swail and Friday discover a mutual attraction

An informer tells them that a local milk factory is being used by the P.A.G.A.N.s to mass-produce a toxic gas made from the chemicals they stole. Friday and Streebek commandeer a police tank and use it for a raid on the factory, which turns out to be producing only milk - the real gas factory being next door.

Friday invites Connie to join him at a birthday dinner for his maternal grandmother, Mrs. Grace Mundy. To Fridays chagrin, Streebek crashes the dinner party.

At the restaurant, Connie identifies a fellow patron, the televangelist Jonathan Whirley (Plummer), as the P.A.G.A.N. leader. Whirley is a highly respected community leader, who is dining with Capt. Bill Gannon (Morgan), Friday and Streebeks boss, and Police Commissioner Kirkpatrick (Ashley). Despite Streebeks warnings, Friday tries to arrest Whirley, leading Kirkpatrick to suspend Friday and order Gannon to take Streebek off the case. Connie and Friday are kidnapped by Muzz and taken to the Griffith Observatory.  

Whirley plans to use a party at Caesars mansion to kill Caesar and the mayor, using the stolen chemicals. 

Friday leads a SWAT team on a raid on Caesars mansion. During the ensuing shootout between the P.A.G.A.N.s and the police, Whirley takes Connie to the airport, where he escapes in his private Learjet. His getaway ends when the now reinstated Friday pursues him in a Northrop F-5 police jet, forcing him to return to Los Angeles and land.

An epilogue reveals that Friday is still working with Streebek and dating Connie.

==Cast==
* Dan Aykroyd as Sgt. Joe Friday
* Tom Hanks as Det. Pep Streebek
* Christopher Plummer as Reverend Jonathan Whirley
* Harry Morgan as Captain Bill Gannon
* Alexandra Paul as Connie Swail
* Jack OHalloran as Emil Muzz
* Elizabeth Ashley as Commissioner Jane Kirkpatrick
* Dabney Coleman as Jerry Caesar
* Kathleen Freeman as Enid Borden
* Bruce Gray as Mayor Peter Parvin
* Lenka Peterson as Granny Mundy
* Juliana Donald as the Zookeeper
* Nina Arvesen as Lady Motor Cop

==Production==
The script had been written by Dan Aykroyd and Alan Zweibel. Tom Mankiewicz, who had a deal at Universal, was brought in to work on it with them. Ted Kotcheff was originally attached to direct but did not like the draft the three writers had come up with so Frank Price at Universal suggested Mankiewicz direct. Tom Mankiewicz, My Life as a Mankiewicz: An Insiders Journey Through Hollywood (with Robert Crane) University Press of Kentucky 2012 p 284-285  JUST THE FACTS, MANK: Director Tom Mankiewicz Is Dragnets Top Cop
Goldstein, Patrick. Los Angeles Times (1923-Current File)   22 Mar 1987: K3.  

Aykroyd originally wanted Jim Belushi to play opposite him but he was unavailable and Tom Hanks was cast instead. 

The title credits featured an update to the series original theme by the British group   with soundbites such as Fridays "Just the facts, maam" timed to the music.

The soundtrack includes an original song, "City of Crime." The track features a hip-hop style collaboration between Aykroyd and Hanks that is performed with bassist/vocalist Glenn Hughes and guitarist Pat Thrall. The track is played over the films closing credits. 

==Reception==
The film received mixed reviews from critics according to Rotten Tomatoes, as Dragnet has a 46% composite score based on 28 reviews with the consensus: "While its sporadically funny and certainly well-cast, Dragnet is too clumsy and inconsistent to honor its classic source material". 

===Box office===
Dragnet performed well at the box office, grossing $57.4 million domestically with an additional $9.3 million internationally, for a total of $66.7 million worldwide.

==References==
 

==External links==
 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 