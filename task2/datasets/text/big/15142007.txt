The Merry Widow (1952 film)
{{Infobox film
| name           = The Merry Widow
| image          = The Merry Widow 1952.jpg
| image_size     = 
| caption        = 
| director       = Curtis Bernhardt
| producer       = Joe Pasternak Leo Stein (libretto) Sonya Levien William Ludwig
| starring       = Lana Turner Fernando Lamas
| music          = Jay Blackton (uncredited)
| cinematography = Robert Surtees   
| editing = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $2,417,000  . 
| gross          = $4,500,000  
}}
 1952 film operetta of the same name by Franz Lehár. It starred Lana Turner (singing voice was dubbed by Trudy Erwin) and Fernando Lamas.

The film received two  , Paul Groesse, Edwin B. Willis, Arthur Krams) and Best Costume Design, Color.   

==Synopsis==
The young widow Crystal Radek is invited to Marshovia, a small European kingdom, to attend the unveiling of a statue in honour of her deceased husband. The royal coffers are seriously in need of her money so the king sends out count Danilo to seduce her.

==Cast==
*Lana Turner as Crystal Radek
*Fernando Lamas as Count Danilo 1934 film version.
*Richard Haydn as Baron Popoff
*Thomas Gomez as the King of Marshovia John Abbott as the Marshovian ambassador
*Marcel Dalio as the police sergeant

==Reception==
According to MGM records the film made $2,232,000 in the US and Canada and $2,268,000 overseas resulting in a profit of $27,000. 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 

 