Skyggen (film)
 
{{Infobox Film name = Skyggen director   = Thomas Borch Nielsen producer   = Søren Juul Petersen writer     = Thomas Borch Nielsen starring   = Lars Bom Puk Scharbau Jørgen Kiil Karin Rørbeck music          = Arne Schultz cinematography = Lars Beyer editing        = Kasper Leick
|distributor= Scanbox (Denmark) Sterling Home Entertainment (USA)
|released= 1998 (Denmark) country        = Denmark runtime    = 97 mins language = Danish
|}}

Skyggen (1998) is a Danish science fiction/cyberpunk/comedy-film directed by Thomas Borch Nielsen and released in Germany and the U.S. as Webmaster. 

It stars Danish actor Lars Bom as the cerebral, machine-like hacker-turned-webmaster J.B., who performs his job while hanging upside down, wearing virtual reality goggles, his mind busy deep inside cyberspace. Upon witnessing a murder, he teams up with the impulsive, energetic Miauv (Puk Scharbau).

Lars Bom won the Best Actor award at the Italian Fantafestival,  where the film also won for Best Special Effects.

The film furthermore won a Silver Grand Prize at the 1999 Brussels International Festival of Fantasy Film, and a Danish Robert Award  for Best Production Design.

The movie was financed by the Danish bank "Forstædernes Bank"   and the final production cost was not disclosed by neither producers nor the director. When the movie was released in Denmark in 1998 it was met with mixed reviews and was released on VHS, never appearing in Danish movie theaters. 

==External links==
* 
* 
* 

==References==
 

 
 
 
 
 


 