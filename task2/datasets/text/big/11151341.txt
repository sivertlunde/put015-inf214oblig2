Jurmana
{{Infobox film
| name           =Jurmana
| image          = Jurmana.jpg
| image_size     = 
| caption        = 
| director       =Hrishikesh Mukherjee
| producer       = Debesh Ghosh Shri Lokenath Chitramandir
| writer         = Rahi Masoom Reza (dialogue)
| screenplay     = Bimal Dutta
| story          = Bimal Dutta
| dialogue       = Rahi Masoom Reza
| narrator       = 
| starring       = Amitabh Bachchan Raakhee Vinod Mehra
| music          = Rahul Dev Burman Anand Bakshi (lyrics)
| cinematography = Jaywant Pathare
| editing        = Subhash Gupta
| distributor    = Shemaroo Video Pvt. Ltd. (2005, India, DVD)
| released       =  
| runtime        = 139 min
| country        = India
| language       = Hindi
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}

Jurmana is a List of Bollywood films of 1979 | 1979 Hindi film. It was produced by Debesh Gosh and directed by Hrishikesh Mukherjee. The film stars Amitabh Bachchan, Raakhee, Vinod Mehra, Shreeram Lagoo, A. K. Hangal, Asrani, Farida Jalal, Keshto Mukherjee and Asit Sen. The lyrics were written by Anand Bakshi and the music was composed by R. D. Burman.

==Plot==
Inder Saxena is a building contractor from Delhi with an attitude that money is the foremost thing in the world. His maternal uncle persuades a reluctant Inder to visit and supervise a projects progress at Pratapgadh. Once there, he meets his college mate Prakash (Vinod Mehra), his professor Daya Shankar Sharma (Shreeram Lagoo) and his daughter Rama (Raakhee). Inder sets his eyes on Rama but Prakash, secretly admiring Rama, tells Inder (Amitabh Bachchan) that she is the kind of girl who cant be lured with the power of money. But, Inder has firm perception that women want only money, bets Prakash that he will lure Rama with his money. After each of his bag of tricks fail, he publishes a book of her poems and gifts it to her. This gesture surprises Rama and is very delighted. One day Inder asks Rama to accompany him. Rama promises him and lies to her father about seeing off Laila (Farida Jalal). Upon learning the truth, Daya Shankar unexpectedly drops in with Prakash at Inders house, finds Rama in his bedroom and scorns her for lying. Rama leaves the house forever and while heading towards Delhi, is robbed for which she has to get down the train. Distressed Rama gets the help of Station Master Nandalal (Asrani (actor)|Asrani), who shelters her in his house. Coincidentally, the same train meets with an accident. Assuming an unrecognizable damaged body to be Rama, the police sends a letter to Daya Shankar about her death, shocking everyone. Inder, feeling guilty for the incident, believes that Rama is alive and searches for her. Meanwhile, Rama learns singing to forget her sorrows and becomes a renowned singer. One day Inder hears her singing on radio, finds her out and informs Prakash. Eventually with all misunderstandings and differences settled, Rama and Inder come together.

==Cast==
*Amitabh Bachchan - Inder Saxena
*Raakhee - Rama Sharma
*Shreeram Lagoo|Dr. Shreeram Lagoo - Prof. Dayashankar Sharma
*Asrani - Nandlal Chaturvedi
*A. K. Hangal - Pt. Prabhakar Chaturvedi, Nandlals Mamaji
*Farida Jalal - Laila Ashit Sen - Dr. Kabir
*Keshto Mukherjee - Babu Ram
*Vinod Mehra - Prem Prakash Trivedi
*Yunus Parvez - Lalaji
*Moolchand - Lalas Brother-In-Law
*Brahm Bhardwaj - Inders Mamaji Manju Bansal Meenaxi Anand Jagjit
*Rajan Raj Verma
*Ahmer Azmi

==Crew==
*Director - Hrishikesh Mukherjee
*Story - Bimal Dutta
*Screenplay - Bimal Dutta
*Dialogue - Rahi Masoom Reza
*Producer - Debesh Ghosh
*Production Company - Shri Lokenath Chitramandir
*Cinematographer - Jaywant Pathare
*Editor - Subhash Gupta
*Art Director - Ajit Banerjee
*Costume and Wardrobe - M. R. Bhutkar, Babu Ghanekar
*Choreographer - Satyanarayan
*Music Director - Rahul Dev Burman
*Lyricist - Anand Bakshi
*Playback Singers - Asha Bhosle, Rahul Dev Burman, Manna Dey, Lata Mangeshkar

==Soundtrack==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      = Anand Bakshi
| all_music       = Rahul Dev Burman

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Sawan Ke Jhoole Pade Hai
| note1           = Sad
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Lata Mangeshkar
| length1         = 5:25

| title2          = Ai Sakhi Radhike Bawri Ho Gai
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Lata Mangeshkar, Manna Dey
| length2         = 6:10

| title3          = Chhoti Si Ek Kali Khili Thi
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Lata Mangeshkar
| length3         = 5:10

| title4          = Nachoon Main Gao Tum
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Asha Bhosle, Rahul Dev Burman
| length4         = 5:55

| title5          = Sawan Ke Jhoole Pade Hai
| note5           = Sad
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Lata Mangeshkar
| length5         = 3:50

| title6          = Chhotisi Ek Kali
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          = Instrumental
| length6         = 5:15

| title7          = Title Music
| note7           =
| writer7         =
| lyrics7         =
| music7          =
| extra7          = Instrumental
| length7         = 1:30
}}

==External links==
*  

 

 
 
 
 
 