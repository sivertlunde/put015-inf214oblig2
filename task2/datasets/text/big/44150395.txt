Raajavembaala
{{Infobox film
| name           = Raajavembaala
| image          =
| caption        =
| director       = KS Gopalakrishnan
| producer       = KS Gopalakrishnan
| writer         = KS Gopalakrishnan Kaval Surendran (dialogues)
| screenplay     = KS Gopalakrishnan Anuradha Balan K Nair
| music          = KJ Joy
| cinematography = KS Mani
| editing        = A Sukumaran
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Anuradha and Balan K Nair in lead roles. The film had musical score by KJ Joy.   

==Cast==
*Ratheesh
*Kalaranjini Anuradha
*Balan K Nair
*C. I. Paul
*Kuthiravattam Pappu
*Ravi Menon
*Sathyakala
*TG Ravi Vincent

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Chunakkara Ramankutty. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Angemalavaazhunna || Vani Jairam, KP Brahmanandan, Chorus || Chunakkara Ramankutty || 
|-
| 2 || Lahari lahari || Anitha Reddy || Chunakkara Ramankutty || 
|-
| 3 || Malakale , malarukale || P Susheela || Chunakkara Ramankutty || 
|}

==References==
 

==External links==
*  

 
 
 

 