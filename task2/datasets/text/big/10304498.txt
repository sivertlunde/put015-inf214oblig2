Incendiary (film)
{{Infobox film
| name           = Incendiary
| image          =
| image_size     =
| caption        =
| director       = Sharon Maguire
| producer       = Andy Paterson Adrienne Maguire Kami Naghdi (co-producer)
| writer         = Chris Cleave (novel) Sharon Maguire (screeenplay)
| narrator       = Michelle Williams Ewan McGregor  Matthew Macfadyen
| music          = Ben Davis
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United Kingdom
| language       = English
| budget         =
}} football match. Michelle Williams, of the same name by Chris Cleave.

==Plot== Michelle Williams) is married to bomb-disposal officer Lenny (Nicholas Gleaves); they have a four-year old son (Sidney Johnston).

 
 football match.  Both Jasper and Lennys boss, Terrence Butcher (Matthew Macfadyen), who is in charge of the anti-terrorist division, try to comfort the mother; both are also romantically interested in her.

Throughout Jaspers investigations, the mother discovers the identity of one of the terrorists. She befriends his teenage son (Usman Khokhar), who only knows that his father is missing since the attacks. When he finds out what his father did, he panics and runs, causing the police to suspect him to be a terrorist. When he tries to take something out of his pocket they think he has a gun or wants to trigger a bomb; they shoot at him, but he is unharmed. The mother, who tried to protect him, is wounded, but not severely. Later, the terrorists wife and son apologize to the mother for his part in the killings.

Terrence confesses to the mother that he knew that a suicide attack was going to happen and could have stopped it, but he did not in order to be able to continue the investigations; he did not know in which stadium it would happen, and also thought it would be of a smaller scale. Although he knew Lenny and his son would be going, he did not warn them.

Sometimes the mother is confused, thinking that nothing has happened to her son. Another time, for therapeutic reasons she writes a letter addressed to Osama bin Laden, who is assumed to be responsible for the attack.

In the films final scenes, the mother has another son by Jasper, who is seen running to the hospital.

== Cast == Michelle Williams - Young Mother
* Ewan McGregor - Jasper Black
* Matthew Macfadyen -  Terrence Butcher
* Nicholas Gleaves - Lenny
* Sidney Johnston - The Boy
* Usman Khokhar - The Bombers Son
* Sasha Behar - Mrs. Ghorbani, the Bombers Wife
* Ed Hughes - Danny Walsh (as Edward Hughes)
* Alibe Parsons - Pearl
* Stewart Wright - Charlie
* Al Ashton - Male Survivor (as Al Hunter Ashton)
* Benjamin Wilkin - Young Policeman
* Robin Berry - Dazed Supporter
* Mercy Ojelade - Nurse Mena
* Joe Marshall - Gary / VT Man

== Production== St Albans School, and Westminster Lodge. 

The production also visited The Metropolitan Training College facilities near Gravesend to shoot the scenes at the football stadium, after the bomb has exploded. 

== Reception ==
Generally, the film received poor reviews. Tom Charity, after viewing the film at the 2008 Sundance Film Festival, gave it one star out of five and called it an "ambitious/opportunistic effort that misses the mark, from the one-dimensional characters to the craven plotting and sentimental tone." 

Philip French called it an "ambitious British picture on an urgent topical subject   is torpedoed by a poor script." 
 Time Out Bridget Joness Diary in 2001 that its hard to know where to start, but the fatal problem is that this is a film with an identity crisis"; the film at times seems like a "study of guilt and grief" and at other times a "conspiracy thriller" but "ends up being a compendium of bizarre diversions, most of which are utterly surplus to the film’s half-cocked desire to stick with the experience and emotions of its main character." 

==References==
 

== External links ==
 
* 
 
 

 
 
 
 
 
 
 
 
 