Maybe It's Love
{{Infobox film
| name           = Maybe Its Love
| image          = Maybe_Its_Love_1930_Poster.jpg
| imagesize      =
| caption        =
| director       = William A. Wellman
| producer       = Warner Brothers Joseph Jackson (screenplay) James Hall
| music          = Erno Rapee Louis Silvers
| cinematography = Robert Kurrle
| editing        = Edward M. McDermott
| distributor    = Warner Bros.
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
}} James Hall. story that filmed in 1915 and starred Ethel Clayton. The film is preserved in the Library of Congress and occasionally is broadcast on Turner Classic Movies.   

==Synopsis==
George Irving, the president of Upton College, is in serious danger of losing his job. For the last twelve years Upton has lost the annual football match against rival Parsons College. The trustees of Upton insist that Irving must resign if Upton fails to win the upcoming football match. Joan Bennett, who is Irvings daughter, overhears the threat of the trustees and tells her friend Joe E. Brown, who is a star football player. Together they come up with a scheme to get some of the best football players around to sign up to play for Upton. Bennett completely changes her appearance to vamp the various men into thinking she will be interested in them if they attend Upton in the following season and play for the football team. One by one they all fall for the scheme and sign up for Upton. Irving, however, refuses to admit James Hall into the college because of his poor performance in academics. Because of the coachs insistence on needing him to win the game, Bennett helps Hall sign up under a fictitious name and credentials. All is well until Hall finds out about Bennett scheme and tells the rest of the team. Just before the game, the Upton team pretends to be drunk in order to teach Bennett a lesson. Just as the game is about to begin, the team decides to forgive Bennett and they win the game for Upton. 

==Cast==
*Joan Bennett - Nan Sheffield
*Joe E. Brown - Yates James Hall - Tommy Nelson
*Laura Lee - Betty
*Sumner Getchell - Whiskers George Irving - President Sheffield

==Songs==
* "Maybe Its Love"
* "I Love to Do It" (cut from domestic release prints)
* "The All American"
* "Keep It Up for Upton"

==Production==
Originally planned as a full scale musical, much of the music was removed before release because of the publics apathy and aversion towards musicals in the autumn of 1930. A longer musical version may have been released in countries outside the United States where a backlash against musicals never occurred. It is unknown whether a copy of this fuller version still exists.

==Preservation== Warner Archive on DVD.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 