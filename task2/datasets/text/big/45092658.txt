A Mouse Tale
{{Infobox film
| name           = A Mouse Tale
| image          = 
| alt            = 
| caption        = 
| director       = David Bisbano
| producer       = 
| writer         = David Bisbano   Raquel Faraoni   Mychal Simka Tom Arnold
| music          = 
| cinematography = 
| editing        = 
| studio         = Grindstone Entertainment Group   Red Post Studio
| distributor    = Grindstone Entertainment Group   Lionsgate Home Entertainment
| released       =  
| runtime        = 
| country        = Peru   Argentina
| language       = English
| budget         = 
| gross          =  
}}

A Mouse Tale is a straight-to-DVD computer-animated film which was released on February 10, 2015.

==Plot==
To save their kingdom from evil rodents, two young mice go on a quest to a forbidden world to find a legendary magic crystal that has the power to restore order to their kingdom.

==Cast==
* Miranda Cosgrove as Samantha
* Drake Bell as Sebastian
* Jon Lovitz as the King of Mice
* Cary Elwes as Sir Thaddeus Tom Arnold as Dalliwog the Wizard
* Dallas Lovato as the Queen of Mice
* Savannah Hudson as Mozzarella
* Brandon Hudson as Provolone

==External links==
* 

 
 
 
 
 
 
 
 
 

 