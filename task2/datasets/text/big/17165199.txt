Notes to You
 
{{Infobox Hollywood cartoon
|cartoon_name=Notes to You
|series=Looney Tunes (Porky Pig)
|image=NotesToYou-01.jpg
|caption=Colorized Title Card to Notes To You
|director=I. Freleng
|story_artist=Michael Maltese Manuel Perez
|voice_actor=Mel Blanc Carl W. Stalling
|producer=Leon Schlesinger Leon Schlesinger Productions
|distributor=Warner Bros. Pictures
|release_date=September 20, 1941 (USA)
|color_process=Black and White
|runtime=7:00
|movie_language=English
}} alley cat Sylvester as the musical cat.

==Plot== prototypical version of Sylvester the cat) starts singing in his back yard. Porky then starts throwing objects at the cat. Porky finally hits him with a vase. The cat starts singing When Irish Eyes Are Smiling at Porky, and Porky shuts the window. Porkys phone rings and he answers it. Its the cat finishing the song. Furious, Porky grabs a shotgun and claims "Ill get rid of that c-cat once and for all!" and lays a saucer of milk on the porch, Porky then falls asleep as the cat drinks the milk and wakes Porky up.
 Jeepers Creepers. Porky chases him out again. When the cat is outside singing Make Love With a Guitar, Porky grabs his gun and shoots the cat, who manages to gasp out a chorus of Aloha Oe, and dies. As Porky feels guilt over the cats death, hes startled to hear the cats nine lives outside his window singing the Sextet from the opera Lucia di Lammermoor.

As the picture irises out, a crash is heard (presumably, Porky, at his wits end, jumped out of the window).

==Availability==
* This cartoon has been presented in its 1968 colorized form on many public domain video compilations, most notably the 2004 Genius Entertainment DVD release of Porkys Café (where its image was grayscaled to look more like the original version). It was released in its original B/W form in 1999 on a Columbia House Looney Tunes Collectors Edition VHS Tape entitled Musical Masterpieces.

==External links==
*  
*   in black & white

 
 
 
 
 
 
 
 

 