The King (2002 film)
{{Infobox film
| name           = The King Ο Βασιλιάς
| image          =
| caption        =
| director       = Nikos Grammatikos
| producer       =
| writer         = Nikos Grammatikos Nikos Panayotopoulos
| starring       = Vangelis Mourikis Marilita Lambropoulou Minas Hatzisavvas
| music          = Thanasis Papakonstantinou
| cinematography = Giannis Daskalothanasis
| editing        = Yannis Sakaridis
| distributor    =
| released       = 2002
| runtime        = 
| country        = Greece
| language       = Greek
| gross          =
}} 2003 Cairo International Film Festival as well as four awards in Greek State Film Awards.

==Plot==
A young man recently released from prison, returns to his hometown in order to escape from his past. There, he is confronted with suspiciousness by the local people who face him with hostility. The only person who sees him friendly is the local policeman. On the contrary, the president of the community dislikes the new resident. After arriving his former girlfriend, he meets again his past and the collision with the local people goes to extremes. 

==Cast==
*Vangelis Mourikis
*Marilita Lambropoulou
*Minas Hatzisavvas
*Babis Giotopoulos
*Vivi Koka
*Tasos Nousias

==Awards==
{| class="wikitable"
|+ List of awards and nominations   
! Award !! Category !! Recipients and nominees !! Result
|- Cairo International 2003 Cairo Golden Pyramid||Nikos Grammatikos|| 
|- Greek State 2002 Greek Third Best Nikos Grammatikos|| 
|- Best Actor||Vangelis Mourikis|| 
|- Best Set Aglaia Zoiopoulou|| 
|- Best Sound||Antonis Samaras  || 

|}

==References==
 

==External links==
* 

 
 
 

 