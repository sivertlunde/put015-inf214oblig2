Yeh Jawaani Hai Deewani
 
 
 
{{Infobox film
| name           = Yeh Jawaani Hai Deewani
| image          = Yeh jawani hai deewani.jpg
| caption        = Theatrical release poster
| director       = Ayan Mukerji
| producer       = Karan Johar Hiroo Yash Johar
| writer         = Hussain Dalal  (Dialogue) 
| story          = Ayan Mukerji
| screenplay     = Ayan Mukerji
| music          = Pritam
| cinematography  = V. Manikandan
| editing         = Akiv Ali
| studio          = Dharma Productions
| distributor     = UTV Motion Pictures
| released        =  
| runtime         = 161 minutes
| country         = India
| language        = Hindi English
| budget          =  
| gross           =  
| starring       = Ranbir Kapoor Deepika Padukone Aditya Roy Kapur Kalki Koechlin      
}} 2013 Bollywood|Indian seventh highest ninth highest grossing Bollywood film in overseas markets.         

==Plot==
Naina (Deepika Padukone) is a shy and nerdy medical student. She studies constantly and always tops her classes, but she feels like an outcast. An encounter with an old classmate, Aditi (Kalki Koechlin), makes her realize that she wants more from life than high marks. Thus, she makes an impulsive decision to follow Aditi on a commercial hiking trip into the Himalayas, up to Manali. During the hike, she renews her friendship with other former classmates, Kabir Thapar aka "Bunny" (Ranbir Kapoor) and Avi (Aditya Roy Kapur). Bunny is a handsome charmer whose dream is to wander and discover the world. He does not plan to marry or settle down. During their trip, seeing Aditis silent reactions to Avis constant flirting with other girls, Bunny and Naina realize that she has a secret crush on Avi, but do not bring the matter to surface.

In the course of the mountain trek, Bunny and Naina draw closer to each other. Bunny teaches Naina to laugh, to play, and more importantly to love. At the end of the trip, she is close to telling Bunny how she feels but is interrupted by Avi. He accidentally finds a letter accepting Bunny to graduate school in journalism, at Northwestern University in Chicago. Bunny explains that the trip is his last hometown adventure before he leaves. He will be sad to leave his friends but happy to start a career that will lead to the travels of which he dreams. Naina realises that love and marriage have no part in Bunnys plans and remains silent about her feelings but is happy for him as he is following his passion.

Eight years pass. Naina finishes medical school and works in a clinic while Bunny works as a videographer for a travel show and travels the world, as he had planned. They meet in Udaipur, for Aditis lavish wedding to Taran (Kunaal Roy Kapur), an awkward but sweet engineer. At the wedding, Bunny and Avi renew their friendship.

Naina and Bunny are strongly attracted to each other again. Both resist love – Bunny because he is not able to figure out that he is in love with Naina and Naina as she doesnt want to get her heart broken again. A day before the wedding, Bunny sees Naina with another man, Vikram (Rana Daggubati), who is the wedding photographer. He is Nainas friend, but Bunny assumes him to be her boyfriend, feels jealous and sends him away. Bunny and Naina meet, argue, and reveal their feelings by a kiss. Yet neither of them is willing to give up a career to follow the other (Bunny is leaving for Paris on the night of Aditis wedding for his dream job as a travel show host). It seems that their romance is over. Bunny also confronts Aditi on her past crush on Avi. She admits that she had feelings for Avi but says that she has gotten over him and is happy with Taran.

On New Years Eve, as Naina sits alone at home along with her pug dog, someone knocks on her door: Its Bunny. He surprises her with a kiss. He has turned down his dream job to be with Naina and proposes, but Naina fears that he might regret his decision in the future. However, Bunny says that he is happy with Billi and wants to continue travelling but with her. He argues that somehow they can make a life together. Naina agrees and love wins the day. Bunny and Naina then get engaged and finally declare their love properly for one another. They have a conference call between Aditi and Avi. Avi is at his bar and Aditi is at the airport as a newlywed leaving for her honeymoon with Taran. The friends find out about Bunny and Naina and are overwhelmed. Aditi states that she always kind of knew about the sparks between them. They all wish each other a Happy New Year and the film ends with Bunny and Naina smiling and embracing each other.

==Cast==
* Ranbir Kapoor as Kabir "Bunny" Thapar, a travel-lover who doesnt take love seriously until he falls for Naina.
* Deepika Padukone as Naina Talwar, a nerdy topper who falls for Bunny.
* Aditya Roy Kapur as Avinash "Avi" Yog, a fun, flirtatious guy with always a drink in his hand. He is Bunny & Aditis best friend.
* Kalki Koechlin as Aditi Mehra, a tomboy, childish girl but matured by time, Aditi is close friends with Naina and best friends with Bunny and Avi.
* Kunaal Roy Kapur as Taran, Aditis husband who is awkward, weird and strange but good at heart. 
* Evelyn Sharma as Lara, a hot, dumb, and ditzy bimbo. She consistently flirts with Bunny. She is a cousin of Taran.
* Farooq Sheikh as Bunnys father, who instilled moral values in Bunny, which Bunny remembers even after his death.
* Tanvi Azmi as Bunnys step-mother. Kabir hates her and her sharbat, but she loves him dearly like her own child and encourages him to accept his love for Naina.
* Dolly Ahluwalia as Nainas mother (guest appearance). She doesnt like Aditi initially.
* Poorna Jagannathan as Riana (guest appearance). The co-host and a close friend of Bunny. She offers Bunny his dream job, that of a travel show host
* Naveen Kaushik as Sumer, camp head
* Omar Khan as Dev
* Anisa Butt as Preeti, Laras Friend
* Rana Daggubati as Vikram(cameo). The photographer at Aditis wedding. He likes Naina and flirts with her by sending her messages with lyrics of Bollywood romantic songs.
* Madhuri Dixit-Nene as Mohini. Special appearance in the song "Ghagra"
* Mayank Saxena
* Mokshad Dodwani

==Production==
Yeh Jawaani Hai Deewani was produced by Hiroo Yash Johar and Karan Johar under the banner of Dharma Productions. It was co-produced by Apoorva Mehta andritten by Hussain Dalal based on a story by Ayan Mukherjee.   The title  was taken from a hit song by the same title, with music by R. D. Burman from the film Jawani Diwani (1972). In December 2011, Pritam was chosen for first time to do the music for a Dharma Productions film, steering away from the usual Vishal-Shekhar and Shankar-Ehsaan-Loy, who had previous scored director Ayan Mukherjees debut, Wake Up Sid (2009).      

===Filming=== Hadimba Temple, Gulaba, Banjaar, Hamta, and Naggar. Some shots were taken at 14,000 feet above sea level and the crew shot at a temperature as low as −10 degrees Celsius. In May 2012, the cast filmed in Udaipur, where Kapoor and Padukone also shot a romantic song. 
 Rue Mouffetard,  a street market. Other scenes were shot in Filmistan, Mumbai. On 26 January, the crew visited Kashmir to shoot a musical sequence and other scenes. They filmed in locations such as Kongdori in Gulmarg, Pahalgam and Srinagar. 

===Casting===
Ranbir Kapoor was the first to sign onto the film and was under Ayan Mukerjis direction for the second time following Wake Up Sid.  Kapoor pierced his ears for his role.  There was a lot of speculation as to who would play the female lead, with Katrina Kaif and Anushka Sharma all rumored to be considered.  Eventually, Deepika Padukone was chosen in September 2011.   Aditya Roy Kapur and Kalki Koechlin were later signed on for pivotal roles.  Koechlin and Kapur play Aditi and Avi, respectively. Evelyn Sharma had confirmed that she will play a ditzy character. 

==Soundtrack==
{{Infobox album
| Name        = Yeh Jawaani Hai Deewani
| Type        = Soundtrack
| Artist      = Pritam Chakraborty
| Cover       = Yeh Jawaani Hai Deewani 2013.jpg
| Released    =  
| Recorded    = 2013 Feature film soundtrack
| Length      = 38:29
| Label       = T-Series
| Producer    = Karan Johar
| Last album  = I Love New Year  (2013)
| This album  = Yeh Jawaani Hai Deewani (2013)
| Next album  = Once Upon a Time in Mumbaai Dobara (2013)
}}
The soundtrack and background score was composed by Pritam, with lyrics penned by Amitabh Bhattacharya and Kumaar(Dilli Wali Girlfriend). Two songs — "Badtameez Dil", sung by Benny Dayal and Shefali Alvares, and "Balam Pichkari" sung by Vishal Dadlani and Shalmali Kholgade — were seen in the trailer. The promo video for "Badtameez Dil" was released on 5 April, the video for "Balam Pichkari" on 10 April, and the video for "Dilliwaali Girfriend" on 25 April. The complete soundtrack was released on 29 April 2013. The "Yeh Jawaani Hai Deewani Mash Up" was made by DJ Chetas.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| collapsed = no
| title1 = Badtameez Dil
| lyrics1 = Amitabh Bhattacharya
| extra1 = Benny Dayal, Shefali Alvares
| length1 = 4:12
| title2 =  Balam Pichkari
| lyrics2 = Amitabh Bhattacharya
| extra2 = Vishal Dadlani, Shalmali Kholgade
| length2 = 4:49 Ilahi (Reprise)
| lyrics3 =
| extra3 = Mohit Chauhan
| length3= 3:33 Kabira
| lyrics4 =
| extra4 = Rekha Bhardwaj, Tochi Raina
| length4= 3:43
| title5 = Dilliwaali Girlfriend
| lyrics5 = Kumaar
| extra5 = Arijit Singh, Sunidhi Chauhan
| length5= 4:20
| title6 = Subhanallah
| lyrics6 =
| extra6 = Mynampati Sreerama Chandra|Sreeram, Shilpa Rao
| length6= 4:09
| title7 = Ghagra
| lyrics7 =
| extra7 = Vishal Dadlani, Rekha Bhardwaj
| length7= 5:04 Kabira (Encore)
| lyrics8 =
| extra8 = Arijit Singh, Harshdeep Kaur
| length8= 4:29 Ilahi
| lyrics9 =
| extra9 = Arijit Singh
| length9= 3:52
| title10 = Yeh Jawaani Hai Deewani Mash Up
| extra10 = Various Artists- DJ Chetas
| length = 5: 32
}}

===Reception===
Shresht Poddar of Score Magazine gave the album 3.5 out of 5 stars and said, "With this soundtrack, Pritam manages to strike a fine balance between energetic dance numbers and soulful melodies. Every song in this album is highly youth friendly. Moreover, Amitabh Bhattacharyas powerful lyrics make the songs even more appealing. This album is a must-hear!" 

==Marketing== Nokia India announced its association with Dharma Productions for the film.  The official trailer was released by Ayan Mukerji, Karan Johar, Ranbir Kapoor and Deepika Padukone on 19 March 2013 at a press conference in Mumbai. Before its release, Ranbir Kapoor invited all of his fans to watch the trailer.   

==Pre-release business==
{| class="wikitable sortable"  style="margin:auto; margin:auto;"
|+Yeh jawaani hai deewani Pre-release business 
! Territories and ancillary revenues
! Price
|-
| Domestic Distribution Rights (UTV)
|  
|- Overseas Distribution Rights 
| 
|-
|  Satellite rights with a TV channel
|  
|-
| Music rights (T~Series)
| 
|- Total
| 
|-
|}
* The figures dont include the Print and Advertising (P&A) costs.

==Release==
Preview screenings of Yeh Jawaani Hai Deewani were held on 30 May 2013. The film was released on 31 May 2013 worldwide and on around 3100 screens in India.    The trailers of Once Upon a Time in Mumbaai Again, Satyagraha – Democracy Under Fire and Bhaag Milkha Bhaag was attached with the film prints.  
 Eros distributed Hebrew and released on 31 May 2013.  The last Bollywood film released in Israel was Devdas (2002 Hindi film)|Devdas. 

Hindu College organised a special screening of Yeh Jawaani Hai Deewani to create a secure and safe environment for female students on 31 May 2013. 

===Critical reception===
Yeh Jawaani Hai Deewani received some critical acclaim. Giving 4 out of 5 stars, Taran Adarsh of Bollywood Hungama|bollywoodhungama.com noted that it "is a revitalizing take on romance and relationships. A wonderful cinematic experience, this one should strike a chord with not just the youth, but cineastes of all age-groups." 
 Indian Express with a negative tone stated that, "Yeh Jawaani Hai Deewani is a been-here, seen-this, much-too-long glossy creature, and not much else."  Saibal Chaterjee of NDTV rated it 3.5/5 and opined that, "Overlong, sluggish and fluffy, it meanders through varied locations as the young lovers/friends seek to reconnect with each other after a few years of being apart." 
Sukanya Verma of Rediff.com gave it 3.5/5 stars and judged, "Yeh Jaawani Hai Deewani is totally worth it!"    Alisha Coelho of in.com praised tKapoors "ebullient" performance and gave it 3.5 stars out of 5.    Rajeev Masand of CNN-IBN gave the film 3 out of 5 stars, concluding "If you are seeking light-hearted mush, youre looking in the right place".  Ananya Bhattacharya of Zee News|ZEENEWS.com said, "Yeh Jawaani Hai Deewani is no heavy or preachy business. It is meant to be enjoyed and it does its job well. Apart from a few parts where the pace of the film drops badly, Ayan Mukherjis handiwork is a breezy, enjoyable one." She also gave it 3 out of 5 stars.  Tushar Joshi of Daily News and Analysis|DNA opined, "YJHD is a well written film that should be watched for its direction, treatment and some remarkable performances." 

Anupama Chopra of the Hindustan Times gave 2.5 out of 5 stars and wrote, "there is enough eye-candy in Yeh Jawaani Hai Deewani to see you through, but I wish the film had more meat and less dressing. Im disappointed because there is a truckload of talent here. What rankles is what might have been."  Raja Sen of Rediff.com noted that the film was good-looking but lacked a good story. He gave it 2 out of 5 stars. 

==Controversies== Manali while they were filmed in north Kashmirs Gulmarg.   He stated that apart from the temple and Span resort, all scenes were shot in Gulmarg.  Many scenes and song sequences of the movie were shot at locations in the Kashmir Valley including the famous ski-resort of Gulmarg and Pahalgam.  Karan Johars production house later said that the opening credit slate clearly mentions Chief Minister Omar Abdullah, Tourism Minister GA Mir and Inspector-General of Police SM Sahal. The names of the entire crew in Kashmir are mentioned in the film credits.   

The films television release has been stalled. The Delhi High Court on 11 June 2013 issued a ruling restraining the TV release of Yeh Jawaani Hai Deewani for allegedly using objectionable dialogues in context of the brand Rooh Afza.    

==Box office==
 

===India===
Yeh Jawaani Hai Deewani opened strongly with around 100% occupancy at multiplexes and in the range of 60–70% at single screens where it set the biggest opening for a Ranbir Kapoor film.   It holds the records for the fourth biggest opening of all time and highest for a non-holiday release after collecting   on its first day.     The film remained strong on Saturday and grossed  .  It made   on its first weekend, breaking the 3-day weekend box office record previously held by Dabangg 2.   The film remained strong on Monday and grossed the second highest ever Monday collection of  .  It continued its successful run on Tuesday collecting   and Wednesday collecting  , which is the highest ever for any film on that day, bringing its six-day total to  .  

Yeh Jawaani Hai Deewani grossed   in its first week, becoming the second fastest Bollywood film to cross   behind Ek Tha Tiger.   It grossed   worldwide in the first seven days.  The film made  , in its second weekend, which is the second highest of all time, making its ten-day gross to  .    It had grossed   in its second week, bringing its total to  .    It also had a very good third weekend gross of   taking its 17 days total to  .    It grossed   in its third week making its total to  .    Yeh Jawaani Hai Deewani grossed   after its fourth weekend.  Yeh Jawaani Hai Deewani added   taking its four-week total to  .    Due to a huge reduction of shows from the fifth week, its lifetime total finished around  . 

===Overseas===
Yeh Jawaani Hai Deewani collected around $4 million on its international opening weekend and is ranked No. 2 in 2013.   The film collected $1,568,677 at the US box office, opening in at No. 9 position with an average of $9,743 from 161 theatres.   It continued its successful run and netted around $7.5 million in ten days.  Yeh Jawaani Hai Deewani grossed $8.75 million in 17 days in overseas.  It grossed $9.50 million in 24 days in overseas.   Yeh Jawaani Hai Deewani is the seventh highest grossing film overseas of all time with it set to finish at $10.50 million approx.  

==References==
 

==External links==
 
*  
*   Internet Movie Database

 
 

 
 
 
 
 
 
 
 
 