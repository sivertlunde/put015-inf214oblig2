On Broadway (film)
{{Infobox film |
  name           = On Broadway |
  image          = Onbroadwayposterxb2.jpg |
  caption        = Promotional poster for On Broadway |
  producer       = Charlie Harrington Donna Bertolon Lance Greene Kris Meyer  |
  director       = Dave McLaughlin |
  writer         = Dave McLaughlin (story & screenplay) |
  starring       = Joey McIntyre Eliza Dushku Sean Lawlor Amy Poehler Mike OMalley |
  cinematography = Terrence Hayes |
  editing        = Julie Garces |
  released       =   |
  runtime        = 98 minutes |
  country        = United States |
  language       = English |
  gross          =  $23,968  |
}}
On Broadway is an independent film, shot in Boston in May 2006, starring Joey McIntyre, Jill Flint, Eliza Dushku, Mike OMalley, Robert Wahlberg, Amy Poehler and Will Arnett.

==Plot== Boston carpenter Jack OToole (McIntyre) writes a play inspired by the mans wake. When nobody will produce the play, Jack quits his job to produce it himself, imagining that this play will give a new start to the strained relationship Jack has with his father. But the only stage Jack can afford is in the back room of a neighborhood pub. In this humble environment, Jack pulls together a theater company of sorts and brings his story to the stage, and in the process he brings together his family and friends and helps them move beyond their loss. 

==Credits==
The film was written and directed by playwright/screenwriter Dave McLaughlin    and shot by cinematographer Terrence Fitzgerald Hayes. The film was produced by Charlie Harrington
 

Additional cast members included Vincent Dowling, Sean Lawlor, Lucas Caleb Rooney, Lance Greene, Andrew Connolly, Peter Giles, Dossy Peabody, Will Harris and Nancy E. Carroll.

==Release== Napa and Northampton, Massachusetts|Northampton. It won an award for Best Feature in the Galway Film Fleadh, another for Audience Best Feature in the Woods Hole Film Festival, the Grand Jury Prize for Best Film in the New Hampshire Film Festival and had six nominations in the Hoboken Film Festival.  At the 2007 Phoenix Film Festival On Broadway won the Sundance Channel Audience Award for Best Film, and Joey McIntyre won the Best Breakthrough Performance Award at the same festival.

On Broadway had a theatrical release on March 14, 2008 in Boston at the Somerville Theater, West Newton Cinema, Sharon Cinemas 8 and Dedham Community Theater.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 