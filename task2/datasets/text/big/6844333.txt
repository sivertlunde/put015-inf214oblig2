The Big Buy: Tom DeLay's Stolen Congress
The Big Buy: Tom DeLays Stolen Congress is a 2006 documentary by Mark Birnbaum and Jim Schermbeck that follows the rise of Tom DeLay from a Texas businessman to the Majority Leader of the United States House of Representatives. The movie examines the controversial 2003 Texas redistricting engineered by DeLay and his organization Texans for a Republican Majority, and DeLays ties to other Congressional figures and businesses.

The conservative National Review referred to the documentary as The Ronnie Earle Movie.  

The filmmakers were interviewed on The Big Buy DVD; they noted that they had not been given access to secret testimony, evidence, or anything  that was not publicly accessible. Earle gave them "extraordinary access" to personal interviews but DeLay reportedly refused to meet with the filmmakers.  

==Screenings and Awards==
*Grand Festival Award, Documentary  — Berkeley Film and Video Festival
*Official Selection  — Hot Springs Documentary Film Festival, Spindletop Film Festival, Dallas Video Festival

==References==
 

==External links==
* , tomdelaymovie.com; accessed November 19, 2014.
* , robertgreenwald.org; accessed November 19, 2014. 
* , imdb.com; accessed November 19, 2014. 

 
 
 
 
 

 

 