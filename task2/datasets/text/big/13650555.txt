Umar Marvi (film)
{{Infobox Film
| name           = Umar Marvi
| image          = 
| image_size     = 
| caption        = 
| director       = Shaikh Hassan
| producer       = Syed Hussain Ali Shah Fazlani, Fazlani Films
| writer         = Qazi Abdur Rahim
| lyrics         = Rashid Lashari
| starring       = Syed Hussain Ali Shah Fazlani Nigat Sultana Noor Muhammad Charlie
| music          = Ghulam Nabi Abdul Lateef
| cinematography = Suhail Hashmi
| editing        = 
| released       = March 12, 1956
| runtime        = 113 minutes
| country        = Pakistan Sindhi
| budget         = 
| domestic gross = 
| preceded_by    = 
| followed_by    =
}}

Umar Marvi (  feature film made in Pakistan.

==Plot== Sindhi folk tale, on which the poet Shah Abdul Latif Bhittai based one of the surs of his Shah Jo Risalo|Risalo.  milk siblings, thus ruling out any possibility of marriage between them. Umar then hands Marvi back to her people, but Khet and the Marus suspect her chastity. Upon hearing the news, Umar goes for Malir to defend Marvis honor. Both have to undergo a trial to prove their innocence by walking through a fire holding a red-hot iron rod. Umar and Marvi come out of the pyre unhurt. In the end, Umar accepts his mistake and blesses Marvi and Khet as they finally marry.

==Reception and awards==
The film met with wide popular success both in India and Pakistan. After its release in Pakistan, the rights of the movie were bought by an Indian distributor named T.M. Bihari. 
Suhail Hashmi and M. Iqbal received civil awards from the President of Pakistan (sadarti awards) for their role, respectively, as director of photography and art director. 

==Cast==
Syed Hussain Ali Shah Fazlani - Umar 
Nigat Sultana - Marvi 
Noor Muhammad Charlie - Phog

==See also==
* Umar Marvi
* Sindhi folklore
* Sindhi cinema
* List of Sindhi-language films

==References==
 

==Further reading==
* Gazdar, Mushtaq. 1997. Pakistan Cinema, 1947-1997. Karachi: Oxford University Press.
* Levesque, Julien & Bui, Camille. 2014. "Umar Marvi and the Representation of Sindh: Cinema and Modernity in the Margins". Bioscope: South Asian Screen Studies, vol. 5, n°2, July 2014  .

==External links==
*  

 
 
 

 