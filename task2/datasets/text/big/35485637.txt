Gokulam
{{Infobox film
| name = Gokulam
| image = Gokulam DVD cover.svg
| image_size =
| caption = DVD cover
| director = Vikraman
| producer = R. B. Choudary
| writer = Vikraman
| starring =  
| music = Sirpy
| cinematography = M. S. Annadurai
| editing = K. Thanikachalam Super Good Films
| studio = J. K. Combines
| released = 11 June 1993
| runtime = 140 minutes
| country = India
| language = Tamil
| budget =
| preceded_by =
| followed_by =
| website =
}}
 1993 Tamil language drama film directed by Vikraman. The film features Bhanupriya, Jayaram and Arjun Sarja in lead roles. The film had musical score by Sirpy and was released on 11 June 1993.  

==Plot==
Chellappa (Jayaram), an orphan, runs a photo studio in deficit with his friends (Chinni Jayanth and Vadivelu). Mary (Bhanupriya) comes to his village to learn singing to a guru (Kalyan Kumar) and she helps her gurus family in many ways. Kannan (Arjun Sarja), her gurus son, is supposed to be in the city. Chellappa falls in love with Mary and he proposes to marry her. Mary reveals that she was married and she tells him her past.

Her real name was Gayatri, she was a rich girl and she was in love with Kannan. Kannan had lot of responsibility and had to help his poor family. Gayatris guardian (Jaishankar) accepted for their marriage but Vasanth (S. N. Vasanth), his son, also wanted to marry her and sent henchmen to kill him. Kannan died at Gayatris birthday party where her guardian had arranged for their engagement after Gayatri promised to help his family. Vasanths father sent his son to jail and he apologized to Gayatri. She went to her friend Marys village instead of her.

Chellappa promises to hide her real identity. Gayatri decides to find a good bride for Uma, Kannans sister. Uma claims that she was in love and her ex-lover (Raja Ravinder) goes after her until now. Kannans family thinks that Gayatri was a fraud. Gayatri convinces Umas ex-lover to forget Uma but he attempts to rape Gayatri and Chellappa saves her. To save Umas marriage, she hides the truth and she is insulted by the villagers. Gayatri is humiliated at Umas wedding.

Gayatri decides to leave the village. Chellappa proves to Kannans family that Gayatri was Kannans lover and that she helped them. Gayatri leaves the village and will send money under Kannans name. Kannans father, who waited for his son return, will wait for Gayatri.

==Cast==
* Bhanupriya as Mary / Gayatri
* Jayaram as Chellappa
* Arjun Sarja as Kannan (guest appearance)
* Kalyan Kumar as Kannans father
* Vadivelu as Raju, Chellappas friend
* Chinni Jayanth as Chellappas friend
* Major Sundarrajan
* Jaishankar as Gayatris guardian
* S. N. Vasanth as Vasanth
* Janaki as Kannans mother
* Sindhu as Mary
* Raja Ravinder
* Yuvasri
* Singamuthu
* V. S. Gopalakrishnan
* Oru Viral Krishna Rao
* Ramesh Khanna as Ramesh (guest appearance)

==Soundtrack==
{{Infobox album  
| Name = Gokulam
| Type = soundtrack
| Artist = Sirpy
| Cover = 
| Released = 1993
| Recorded = 1993 Feature film soundtrack
| Length = 
| Producer = Sirpy
| Label = Lahari Music
}}

The film score and the soundtrack were composed by film composer Sirpy and lyrics written by Pazhani Bharathi. The soundtrack, released in 1993, features 8 tracks. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration
|- 1 || Chittada Rakkai || Swarnalatha || 4:49
|- 2 || Nane Madai Mela || Swarnalatha || 4:12
|- 3 || Puthu Roja Puthiruchu || Mano (singer)|Mano, Swarnalatha|| 3:11
|- 4 || Antha Vanam  || K. S. Chithra || 3:55
|- 5 || Chevanthi Poo  || Unni Menon, P. Susheela || 5:03
|- 6 || Sujatha || 1:32
|- 7 || Pon Malayil || Uma Ramanan || 3:12
|- 8 || Therekke Adikkuthu || K. S. Chithra || 4:45
|}

==References==
 

 
 

 
 
 