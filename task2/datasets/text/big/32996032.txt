Padmasree Bharat Dr. Saroj Kumar
{{Infobox film
| name           = Padmasree Bharat Dr. Saroj Kumar
| image          = Padmasree Bharat Dr. Saroj Kumar.jpg
| caption        = Teaser poster
| alt            =
| director       = Sajin Raaghavan
| producer       = Vaishaka Rajan Sreenivasan
| starring       = Sreenivasan Mamta Mohandas Vineeth Sreenivasan Fahadh Faasil
| music          = Songs:  
| cinematography = S. Kumar
| editing        = Kevin Thomas
| studio         =
| distributor    = Vaishaka Cinema & PJ Entertainments UK
| released       =  
| runtime        = 147 minutes
| country        = India
| language       = Malayalam
| budget         = 3.5 crore
| gross          = 
}}
Padmasree Bharat Dr. Saroj Kumar ( ) is a 2012   and Kochi.   

==Plot==
Saroj Kumar is a matinée idol and megastar in Kerala churning out hits one after the other. But a few of his recent movies, such as Vekkeda Vedi, turns out to be big flops. His new film, directed by Alex Samuel, gets cancelled on the first day of shooting when Saroj slaps fellow actor Shyam for some silly reasons. Pachalam Bhasi, the producer, tries for a compromise, but Saroj is not ready to apologise. 

Alex Samuel and Pachalam Bhasi starts their new film with Shyam in the lead. Saroj bribes film associations and gets it banned. He gets the rank of Colonel in the Army and shows of with it. This is when Babykuttan, another producer, comes into scene. He helps Alex Samuel and Pachalam Bhasi to resume their project. Saroj Kumar tries to cancel the film in many ways but it is successfully completed and is well received by the audiences. 

Meanwhile the income tax department raids his premises and starts prosecution proceedings for tax evasion. Saroj is tense about this, and he drinks a lot acting in a different manner. Many truths regarding Sarojs personal life are revealed towards the end.

==Cast== Sreenivasan as Megastar Padmasree Bharat Dr. Saroj Kumar
* Vineeth Sreenivasan as Shyam 
* Mamta Mohandas as Neelima 
* Fahadh Faasil as Alex Samuel
* Jagathy Sreekumar as Pachalam Bhasi
* Suraj Venjarammoodu as Muttathara Babu
* Salim Kumar as Rafeek Mukesh as Babykkuttan
* Manikuttan as Rajesh Sandhya (special appearance)
* Meera Nandan (special appearance) Sarayu (special appearance)
* Roopa (special appearance)
* Nimisha Suresh (special appearance)
* Kollam Thulasi as Union Leader
* Apoorva Bose as Lekha
* Sajitha Betti as Neelimas friend
* Ponnamma Babu as Neelimas friend Shari as Shyams mother
* Manoj Kumar as Shyams neighbour
* Cherthala Lalitha as Shyams neighbour
* Deepika Mohan as Servant

==Production==
About the idea of making Padmasree Bharat Dr. Saroj Kumar, Sreenivasan says: 

 The thought of the film came to us when we started thinking about what should be happening to a superstar like Saroj Kumar at this point of time. He is getting old and the most challenging aspect about stardom is that once you have got it, the next question is how long it will last. Some people can accept the changes in life in a mature way but for most others, it is never easy. This superstar is wary of new thoughts and new experiments because he knows he wont fit into the changing scheme of things. It is easy to be a hero in films as even a weak person can fight with any number of goons and get applause from viewers. But Dr Saroj Kumar forces the directors to cater to his thoughts. All this makes the character very exciting.  , Rediff.com, 
January 17, 2012  

As the characters are already known to the viewers, the expectations could be huge. The film was mainly shot in Chennai and Kochi.

==Release==

===Critical reception===
The film released on 14 January 2012  to mainly negative to mixed reviews from critics. Paresh C Palicha of Rediff.com concluded his review saying, "On the whole, Padmasree Bharat Dr. Saroj Kumar is disappointing, to say the least."  Hari Prasad of Yentha.com said that "the film, despite having a good theme, suffers from a messy script."  Sify.com and Oneindia.in also published negative reviews saying that it is "disappointing."  

===Controversies===
Although Sreenivasan says that the characterisation of Saroj Kumar is not at all based on any existing superstars, it has been accused that Sreenivasan is drastically mimicking actor Mohanlal. Many comments are made in the film about real happenings including Mohanlals derogatory comments against Sukumar Azhikode, his Lieutenant Colonel honour, income tax raids and recovering of elephant tusks from his house, and his famous ad campaigns. S. Kumar, the cinematographer of the film, claimed live on T.V. that he was threatened by phone by Antony Perumbavoor, a producer and old driver of Mohanlal. 

===Soundtrack===

Music:  
{{Track listing
| extra_column = Singer(s)
| title1 = Mozhikalum
| extra1 = Haricharan, Manjari
| title2 = Iniyoru Chalanam
| extra2 = Shaan Rahman, Vineeth Sreenivasan 
| title3 = Kesu
| extra3 = Vineeth Sreenivasan, Shweta Mohan
| title4 = Mozhikalum
| extra4 = Haricharan
}}

==See also==
* Udayananu Tharam

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 