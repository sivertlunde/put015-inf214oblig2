Simply Irresistible (film)
{{Infobox film
| name           = Simply Irresistible
| image          = Simply irresistible.jpg
| caption        = One-sheet poster
| director       = Mark Tarlov
| producer       =  
| writer         = Judith Roberts
| narrator       =
| starring       = Sarah Michelle Gellar Sean Patrick Flanery Patricia Clarkson Dylan Baker Christopher Durang Larry Gilliard, Jr. Betty Buckley
| music          = Gil Goldstein
| cinematography = Robert M. Stevens
| editing        = Paul Karasick
| studio         = Regency Enterprises
| distributor    = 20th Century Fox
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $6 million
| gross          = $4,389,989
}}
 1999 Cinema American romantic Judith Roberts.

==Plot==
 
Amanda Shelton (Sarah Michelle Gellar) inherits her late mothers restaurant, but lacks her mothers ability to cook. The restaurant is failing when Amanda meets a mysterious and possibly magical man at the local market. He introduces himself as Gene OReilly and claims to be an old friend of her mothers. He sells her crabs, one of which escapes cooking to become her personal mascot. This special crab is magical and it casts spells, with a wave of its claw. Amanda meets her love interest at the market, Tom Bartlett (Sean Patrick Flanery), a department store manager at Henri Bendel on Fifth Avenue, who is opening an ambitious new restaurant inside his store. It is never explicitly explained why, but this eventful day transforms Amanda into a miraculous food witch; people who now eat her impressive new dishes fall under her accidental spells. These are inspired by her emotions and created with the help of her magic crab. Amanda saves her restaurant overnight, and her relationship with Tom blossoms just as fast. However, Tom, being a career-minded control freak, panics when he realizes that not only could she be a witch who could be casting spells on him, but that his own emotions are getting the best of him, and he promptly dumps her. When Amanda goes to confront Tom one last time at his office, she witnesses the violent tantrum and resignation of a celebrity French chef hired for the opening of Toms new restaurant. When it is discovered that Amanda is in fact the hot new chef in town everyone is talking about, she is hired on the spot, despite Toms protests. Once Amanda overcomes her self-doubts and insecurities, she reaches her full potential as a chef, and the opening is a complete success. Though Tom refuses to taste Amandas food during the opening, he eventually admits to himself he was wrong to reject Amanda because she made him feel emotional. He finally decides to embrace his feelings for her and goes after her. At the last minute, he reaches her with his own personal magic (a paper airplane), and the two reconcile on the dance floor.

==Cast==
* Sarah Michelle Gellar as Amanda Shelton
* Sean Patrick Flanery as Tom Bartlett
* Patricia Clarkson as Lois McNally
* Dylan Baker as Jonathan Bendel
* Christopher Durang as Gene OReilly
* Larry Gilliard Jr. as Nolan Traynor
* Betty Buckley as Aunt Stella
* Amanda Peet as Chris
* Małgorzata Zajączkowska as Mrs. Mueller

==Reception==
Simply Irresistible was poorly received by critics, as it holds a 13% rating on Rotten Tomatoes based on 30 reviews.

===Box office===
The film opened at #9 at the North American box office making $2.2 million USD in its opening weekend.

==Soundtrack==
 
#"Little King" – The Hollowbodies
#"Busted" – Jennifer Paige
#"Falling" – Donna Lewis
#"Got the Girl (Boy from Ipanama)" – Reiss
#"The Angel of the Forever Sleep" – Marcy Playground
#"Take Your Time" – Lori Carson
#"Beautiful Girls" – Chris Lloyd
#"Once in a Blue Moon" – Sydney Forest
#"Parkway" – The Hang Ups
#"Our Love Is Going to Live Forever" – Spain
#"Bewitched, Bothered and Bewildered" – Katalina
#"That Old Black Magic" (Harold Arlen) – Jessica 

==Astaire references==
The film contains interesting references to four musical films of Fred Astaire:

*  .

* Yolanda and the Thief (1946 in film|1946)
** The flooring which Flanery selects for his new restaurant is a copy of that used in the "Coffee Time" number. 
** Dylan Baker comments: "This looks like something out of an MGM musical film|musical".

*   in the Shall We Dance finale.

*  Swing Time (1936 in film|1936): The layout of the restaurant at the end of the movie closely resembles the restaurant and night club, the "Silver Sandal."

== External links ==
 
* 
* 

 
 
 
 
 
 
 
 