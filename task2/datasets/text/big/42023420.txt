Just for Fun (film)
{{Infobox film
| name           = Just for Fun
| image          = "Just_for_Fun"_(1963).jpg
| caption        = Original lobby card of Bobby Vee
| director       = Gordon Flemyng 
| producer       = Max Rosenberg Milton Subotsky
| writer         = Milton Subotsky
| narrator       =
| starring       = Mark Wynter Cherry Roland Alan Caddy
| music          = Tony Hatch
| cinematography = Nicolas Roeg
| editing        = Raymond Poulton
| studio         = Amicus Productions
| distributor    = Columbia Pictures
| released       = 1963
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}

Just for Fun is a 1963 British black and white musical film directed by Gordon Flemyng.  It was written by Amicus co-founder Milton Sobotsky. Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 15  

==Plot==
When English teenagers win the right to vote, the established political parties compete for their support. However, when the Prime Minister cuts the amount of Pop music allowed on TV, young Mark and Cherry start their own Teenage Party and use some of Englands pop singers to help.

==Cast==
 
*Mark Wynter as Mark
*Cherry Roland as Cherry
*Richard Vernon as Prime minister
*Reginald Beckwith as Opposition leader John Wood as Official
*Jeremy Lloyd as Prime ministers son
*Harry Fowler as Interviewer
*Edwin Richfield as Man with a CND badge
*Alan Freeman as Narrator David Jacobs as Disc Jockey
*Jimmy Savile as Disc Jockey
*Irene Handl as Housewife
*Hugh Lloyd as Plumber
*Dick Emery
*Mario Fabrizi
*Ken Parry
*Gary Hope
*Douglas Ives Ian Gray
*John Martin Jack Bentley Frank Williams
*Gordon Rollings
*Bobby Vee
*The Crickets
*Freddy Cannon
*Johnny Tillotson
*Ketty Lester Joe Brown and the Bruvvers
*Karl Denver
*Kenny Lynch
*Jet Harris
*Tony Meehan Cloda Rodgers
*Louise Cordet
*Lyn Cornell
*The Tornados
*The Springfields
*The Spotnicks Jimmy Powell
*Brian Poole and the Tremeloes
*Sounds Incorporated
*The Vernon Girls
 

==Critical reception== TCM wrote, "episodic in the extreme, Just for Fun plays like an evening of Vaudeville, with the various singing acts punctuated by broad comic bits that are more miss than hit but retain, at least at this distance, an undeniable vintage charm...sweet relief comes in the form of the assembled musical talent, whose contributions are well-staged by director Gordon Flemyng." 

==References==
 

==External links==
*   at TCMDB
*   at IMDB
*  
*  

 

 
 
 
 
 
 

 