Dakota Incident
{{Infobox film | name =Dakota Incident
 | image =
 | caption =
 | director = Lewis R. Foster Michael Baird
 | writer = Frederick Louis Fox
 | starring =Dale Robertson Linda Darnell
 | cinematography = Ernest Haller
 | music = R. Dale Butts
 | editing = Howard A. Smith
 | distributor = Republic Pictures
 | released = July 23, 1956 (U.S. release)
 | runtime = 88 min
 | language = English
 | budget =
 }} Western produced by Republic Pictures. The film stars Dale Robertson and Linda Darnell.

==Plot==
Rick Largo and the Banner brothers hold up a bank. Frank is persuaded to shoot brother Johnny by the greedy Largo, so the loot can be split just two ways.

Johnny, left for dead, recovers and rides to town. He challenges his brother to a showdown, then spares his life but orders him out of town. He gives no such chance to Largo, gunning him down.

Deciding to leave for Wyoming to buy a ranch, Johnny is asked to drive the stagecoach through dangerous Indian territory when no one else will. Aboard are saloon singer Amy Clarke, her piano player Minstrel, gold speculator Chester, bank clerk Hamilton and a senator named Blakeley who is sympathetic toward the Indians in spite of all the violence.

Franks lifeless body is found on the trail, filled with arrows. A wheel breaks, forcing the travelers to stop for repairs. Chester is killed in an Indian ambush. Hamilton, who was falsely suspected in the bank job Johnny pulled, nearly dies before Johnny saves his life.

Heat and thirst get to the survivors as the Indians wait them out. Minstrel sees a mirage, wanders off and is shot. Blakeley is mocked by Amy for his kindness toward the Indians, so he ventures out, hoping to reason with them. He, too, is killed.

Hamiltons life is slipping away, so Johnny makes a try for a canteen left out by the Indians as bait. He is attacked by a Cheyenne brave and spares his life, telling him to return to his people and admit being saved by a white man. Hamilton expires, but the Indian returns with fresh horses for Johnny and Amy, who ride back toward town so Johnny can turn himself in for the robbery.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Linda Darnell || Amy Clarke
|-
| Dale Robertson || John Banner
|- John Lund || John Carter (aka Hamilton)
|-
| Ward Bond || Sen. Blakely
|-
| Regis Toomey || Minstrel
|-
| Skip Homeier || Frank Banner
|-
| Irving Bacon || Tully Morgan
|-
| John Doucette || Rick Largo
|-
| Whit Bissell || Mark Chester
|- William Fawcett || Matthew Barnes
|}

==External links==
* 

 

 
 
 
 
 
 
 

 