Diary of a Shinjuku Thief
{{Infobox Film
| name           = Diary of a Shinjuku Thief
| image          = 
| image_size     = 
| caption        = 
| director       = Nagisa Oshima
| producer       = 
| writer         = Nagisa Oshima Mamoru Sasaki Masao Adachi Takeshi Tamura
| narrator       = 
| starring       = Tadanori Yokoo Rie Yokoyama Kei Satō Jūrō Kara Moichi Tanabe Tetsu Takahashi
| music          = 
| cinematography = Sēzō Sengen Yasuhiro Yoshioka
| editing        = Nagisa Oshima
| distributor    = Sōzōsha Art Theatre Guild
| released       =  
| runtime        = 96 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 1968 Japanese New Wave film directed by Nagisa Oshima.

==Synopsis==
The film centers around Birdie, a young Japanese book thief who is caught by a store clerk named Umeko.  As their encounters grow increasingly fraught with tension and desire, the two become lovers and begin committing thefts together.

==Cast==
* Tadanori Yokoo
* Rie Yokoyama
* Kei Satō
* Jūrō Kara - Himself / Singer
* Moichi Tanabe
* Tetsu Takahashi
* Fumio Watanabe - Himself

==Trivia==
Also featured in this film is Jūrō Kara who sings a song accompanied with a guitar. As the film climaxes we are transported from a rough life of crime to odd world of such oddities as Birdie chasing Umeko with a dildo, only to conclude with her being raped and him being beaten by the offenders. 

==Reception==
The film was described by Ronald Bergan of The Guardian as "an explosive agit-prop movie equating sexual liberation with revolution, whose impact has cooled only marginally." 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 


 