Secret Valley (film)
{{Infobox film
| name           = Secret Valley
| image          = Willie Fung-Virginia Grey in Secret Valley.jpg 
| image_size     =
| caption        = Willie Fung and Virginia Grey in the film
| director       = Howard Bretherton
| producer       = Sol Lesser
| writer         = Paul Franklin (adaptation) Daniel Jarrett (screenplay) Earle Snell (screenplay) Harold Bell Wright (story)
| narrator       =
| starring       = See below
| music          =
| cinematography = Charles Edgar Schoenbaum
| editing        = Charles Craft
| distributor    =
| released       = 15 January 1937
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Secret Valley is a 1937 American film directed by Howard Bretherton.

The film is also known as Gangsters Bride and Gangsters Valley in the United Kingdom.

== Cast ==
*Richard Arlen as Lee Rogers
*Virginia Grey as Jean Carlo
*Jack Mulhall as Russell Parker
*Norman Willis as Slick Collins aka Howard Carlo
*Syd Saylor as Paddy Russell Hicks as Austin Martin
*Willie Fung as Tabasco
*Maude Allen as Mrs. Hogan

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 