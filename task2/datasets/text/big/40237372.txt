Pusthakamlo Konni Pageelu Missing
{{Infobox film
| name = Pusthakamlo Konni Pageelu Missing
| image = Pusthakamlo Konni Pageelu Missing poster.jpg
| border=yes
| story = Balaji Tharaneetharan
| starring = Sree Supraja Rahul Satish Masth Ali
| director = Sajid Qureshi
| cinematography = Martin Joe
| producer = Mohammad Sohail Ansari
| editing = Mohan - Rama Rao
| studio = Blockbuster Studio
| country = India
| released =  
| runtime = 109 min
| language = Telugu
| music = Gunwanth Sen
| budget = 
| gross = 
}} Telugu comedy film directed by Sajid Qureshi and Produced by Mohammad Sohail Ansari. The film is a remake of Balaji Tharaneetharans 2012 comedy film Naduvula Konjam Pakkatha Kaanom starring Vijay Sethupathi and Gayathrie Shankar in the lead roles and this film stars Sree, Supraja, Rahul, Satish and Masth Ali in vital roles. Gunwanth Sen scored the Music. The film released on August 9, 2013 and did decent business at the Box office in spite of mixed reviews from the critics. 

==Plot==
Vijay Kumar (Sri), Siva, Saleem and Balaji are good friends. Vijay happens to meet Sandhya (Supraja) and he falls in love with her. After a brief resistance, the parents of Vijay and Sandhya accept their love and agree to get them married.
Just a day before the wedding, Vijay gets injured while playing cricket. He takes a blow on his head and ends up with temporary memory loss. He loses all memory of events that happened in the last one year.
As the wedding hour approaches, Vijay’s friends do everything and anything possible to bring back his memory. Will they succeed?
Will Vijay marry the girl he loves? That is the story of ‘Pusthakam lo Konni Pagelu Missing’.

==Cast==
* Sri as Vijay
* Supraja as Sandhya
* Rahul as Shiva
* Mast Ali as Saleem
* Satish as Balaji

==Critical reception==
* Telugumirchi wrote:"Director Sajid’s inexperience is evident. It’s very difficult to deal stories like this. Scenes have to be written and narrated very carefully. Movie would have been better if director had taken care in that respect. Many scenes create laughs but would have been better if director was strong on story. As a whole ‘pustakamlo konni pageelu missing’ is for fun viewing". 
* Indiaglitz wrote:"On the face of it, this film is a faithful remake of the Tamil original.  A little more imagination could have made it a memorable film.  Watch it for the neat humour" 
* Times of India wrote:"Its funny, but only in pieces, so a cheerful disposition might come in very handy to survive the not-so-funny parts, which is what most of the movie is filled with" 
* Deccan Chronicle wrote:"In fact, there is nothing more to the film than its trailer and promos. Too many small plots and repetitive dialogues put you off, So, give this film a miss". 
* 123Telugu wrote:"‘Pusthakam lo Konni Pagelu Missing’ falls just short. Its appeal at the Box Office will be quite limited. It might cater to people who like to watch unconventional films. Some boring moments and the absence of strong performers in the cast work against the film". 
* Telugucinema wrote:"An off-beat jolly comedy film. It truly has different plot and is very engrossing till the end though a little long. Watch it for its novel concept". 

==Soundtrack==
Music is composed by Gunwanth Sen. 

* Ra Ra - Deepu
* Band Baaja - Indhu Sonali
* Ayyo Ramare - Gaana Bala
* Saradaga - Chinna Ponnu
* Rabba - Deepu

==References==
 

 
 
 
 