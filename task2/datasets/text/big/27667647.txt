Pelli Naati Pramanalu
{{Infobox film
| name           = Pelli Naati Pramanalu పెళ్లినాటి ప్రమాణాలు வாழ்க்கை ஒப்பந்தம்
| image          = Pellinati Pramanalu.jpg
| image_size     =
| caption        =
| director       = Kadiri Venkata Reddy Pattabhirama Reddy 
| writer         = Pingali Nagendra Rao   (story, dialogues and lyrics)  
| narrator       =
| starring       = Akkineni NageshwaraRao Jamuna Ramanarao Rajasulochana S. V. Ranga Rao Rajanala Nageswara Rao Ramana Reddy Chaya Devi
| music          = Ghantasala Venkateswara Rao
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 17 December 1958
| runtime        =
| country        = India Telugu
| budget         =
}}

Pelli Naati Pramanalu or Pellinati Pramanalu (  directed and co-produced by Kadiri Venkata Reddy. The film was made simultaneously with Tamil as Vaazhkai Oppandham (Tamil: வாழ்க்கை ஒப்பந்தம்) and released in 1959.

It is a musical hit film with lyrics by Pingali Nagendra Rao and music score by Ghantasala Venkateswara Rao. There is a patriotic song Sreemanturalivai Cheluvondu Maata Mammu Deevimpuma Maa Andhramata by P. Leela is memorable song.  It was remade in 2001 as Sardukupodaam Randi directed by S.V. Krishna Reddy starring Jagapati Babu, Soundarya, Asha Saini.

==Plot==
Bheemasena Rao (SV Rangarao) is a dignified and respectful man. His son (R. Nageswara Rao) works in Military services. His daughter is Rukmini (Jamuna). His close friend and relative Salahala Rao (Ramana Reddy) is trying to arrange marriage to his daughter. Krishna Rao (Akkineni) is his relative of Salahala Rao. He was sent to meet Bheemasena Rao with recommendation letter. During the meantime he attracts Rukmini with his good manners. Bheemasena Rao arranged marriage between Krishna Rao and Rukmini and a political leader takes Marriage Vows (Pellinati Pramanalu) from them that they should not cheat each other.

Krishna Rao starts new family in the town. He finds a job in a company "Andalu and Alankaralu". During the next seven years, they had four children. Subsequently Krishna Rao started getting attracted to other women. He gets close to his new typist Radha Rani (Rajasulochana). He even goes to her house during the absence of his wife. After knowing about the affair, Rukmini and Salahala Rao plays a small drama and let Krishna Rao know about his mistake. Brother of Rukmini marries the pious Radha Rani.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Akkineni Nageshwara Rao ||  Krishna Rao
|-
| Jamuna Ramanarao || Rukmini
|-
| S. V. Ranga Rao || Bheemasena Rao
|-
| Rajasulochana || Radha Rani
|-
| R. Nageswara Rao || Bheemasena Raos son
|-
| Ramana Reddy || Salahala Rao
|-
| Chaya Devi || Salahala Raos wife
|-
| Allu Ramalingaiah || Prakatanalu
|-
| Peketi Sivaram || M.V.Tesam
|-
| Chadalavada Kutumba Rao || Ammakalu
|-
| Sivaramakrishnaiah || Nandaji
|-
| Balakrishna || Office Peon
|-
| Surabhi Kamalabai || Yerukala Subbi
|}

==Soundtrack==
There are about 9 songs and padyams in the film penned by Pingali Nagendra Rao and music score provided by Ghantasala Venkateswara Rao. 
* Arana Ana Aina Sarasamaina Beramaya Mallepoola Dandalaya (Singer: Jikki Krishnaveni)
* Brundavana Chandamama Endukoyi Tagavu (Singer: Ghantasala and P. Leela; Cast: Jamuna and Akkineni)
* Challaga Choodali Poolanu Andukupovali Devi (Singer: Ghantasala Venkateswara Rao)
* Edo Teliyaka Pilichitinoyi Meedikirakoyi Krishna (Singer: P. Susheela)
* Laali Maa Papayi Ananda Laali (Singer: P. Leela group)
* Neetone Lokamu Neetone Swargamu (Singers: Ghantasala and P. Leela)
* Raave Muddula Radha Naa Premarasivi Neeve (Singers: Ghantasala and P. Susheela)
* Sreemanturalivai Cheluvondu Maata Mammu Deevimpuma Maa Andhramata (Singer: P. Leela; Cast: Jamuna)
* Sura Yaksha Gandharva (Padyam) (Singer: Ghantasala)
* Vennelalone Vedi Yelano Vedimilone Challa Nelano (Singers: Ghantasala and P. Leela)

==Awards== National Film Awards    1959 - National Film Award for Best Feature Film in Telugu

==References==
 

==External links==
*  
 

 
 
 
 
 

 