Welcome (2007 film)
 
 

 

{{Infobox film
| name           = Welcome
| image          = Welcome_poster_2007.jpg
| caption        = Theatrical release poster
| director       = Anees Bazmee
| producer       = Firoz A. Nadiadwala
| story          = Praful Parikh Rajiv Kaul Anees Bazmee
| screenplay     = Praful Parikh Rajiv Kaul Anees Bazmee
| narrator       = Om Puri
| starring       = Akshay Kumar Nana Patekar Anil Kapoor Feroz Khan Katrina Kaif Malika Sherawat Paresh Rawal
| music          = Anand Raj Anand Himesh Reshammiya Sajid Wajid
| cinematography = Sanjay F. Gupta
| editing        = Ashfaque Makrani
| studio         = 
| distributor    = Base Industries Group
| released       =  
| runtime        = 159 minutes
| country        = India
| language       = Hindi
| budget         =  
| gross          =      
}}
Welcome (     comedy film directed by Anees Bazmee and produced by Firoz A. Nadiadwala & Ronnie Screwvala under the banner of UTV Motion Pictures.  The film features a large ensemble cast of Akshay Kumar, Anil Kapoor, Nana Patekar, Katrina Kaif, Paresh Rawal, Malika Sherawat    and Feroz Khan in last film appearance,  whilst Sunil Shetty makes a guest appearance and Malaika Arora Khan appears in an item number. 

Welcome released worldwide on 21 December 2007, to mixed critical reviews and despite initial competition from Taare Zameen Par,   was a strong financial success at the box office both in India  and overseas. 

==Plot==
Uday (Nana Patekar), a criminal don, takes it upon himself to get his kindhearted sister, Sanjana (Katrina Kaif) married, but he is unsuccessful since no one wants to be associated with a crime family. Dr. Ghunghroo (Paresh Rawal) has also been trying to get his nephew, Rajiv (Akshay Kumar), married but due to his condition—the alliance must be with a purely decent family—he is also unsuccessful.

When Rajiv jumps into a burning building to save Sanjana, he is smitten by her. Uday and Majnu (Anil Kapoor), Udays friend gangster hatch a plan for an alliance with Dr. Ghunghroo. The plan works and Dr. Ghunghroo confirms the alliance, thinking that Uday is a very decent man. But when he is later told that Uday and Majnu are mobsters, he quickly takes his family and flees to Sun City, South Africa to escape. However, Majnu and Sanjana have come to Sun City as well. Rajiv meets Sanjana again and the two fall in love.

Dr. Ghunghroo reconciles with Uday and Majnu and finally agrees to the alliance. Uday and Majnu invite a powerful don of the underworld, RDX (Feroz Khan) to the engagement. At the party, a girl named Ishika (Malika Sherawat) arrives, claiming to be Rajivs childhood betrothed. Ishika is actually the sister-in-law of Dr.Ghunghroo that Dr. Ghunghroo asked to come and try to break off the engagement. Ishika manages to do so, leaving Rajiv and Sanjana heartbroken. Dr. Ghunghroo reveals that he did this for Rajivs mother, who had married into a crime family and was harassed and tortured, thus telling Dr. Ghunghroo to raise Rajiv away from crime when he was born. Dr. Ghunghroo decides he will agree to the marriage only if Uday and Majnu give up their life of crime. Rajiv and Sanjana do this by reawakening Udays love for acting and encouraging Majnu to pursue his love for painting. With these things keeping them busy, Uday and Majnu have no time for crime any more.

Rajivs actions anger RDXs son, Lucky, who attempts to shoot Rajiv. Sanjana gets hold of the gun and fires a shot that hits Lucky, causing him to go unconscious. RDX is informed of his sons death and comes to attend the cremation. However Lucky, who is still alive, escapes, trying to show his father that hes actually alive. RDX sets the pile of wood on fire, believing he is cremating his sons body. However Lucky, who had been hiding under the wood, jumps out upon realising the wood is on fire, and the truth is revealed to RDX. Rajiv, Ghunghroo, his wife, Ishika, Uday, Majnu and Sanjana are captured by RDX and brought to a cabin set next to a cliff. The frightened group is forced to play "Hot Potato" with a globe—but the one who ends up with the globe must jump off the cliff. When Rajiv refuses to pass the globe to Sanjana, Lucky angrily yanks it out of his hands, just as the music stops. Now that his son has the globe, RDX figures the only way he can maintain his image is by killing everyone. Before he can, several government brokers sneak up and cut the footings of the cabin, causing the house to start falling over the cliff, with everyone trapped inside. However, the cabin is suspended by only one column. Hilarious chaos ensues as the group tries to balance the cabin together and keep it from falling off the cliff. Rajiv finds a rope and the group uses it to get back onto stable ground. But to everyones shock, the floor broke and Lucky was hanging on the edge of the cabin. While Rajiv is trying to rescue him, Sanjana reveal the truth to everyone that she was the one who shot Lucky, but Rajiv blamed himself so that Sanjana wont get in trouble. After Rajiv rescued Lucky, the cabin he is standing on fell off a cliff. Sanjana kept crying for him thinking he is dead. However, Rajiv survived the event and is reunited with Sanjana and his family. Lucky and RDX are grateful to Rajiv for saving their lives and RDX gives up his life of crime, allowing Rajiv and Sanjana to finally get married.

==Cast==
* Akshay Kumar as Rajiv Saini
* Katrina Kaif as Sanjana Shankar Shetty
* Paresh Rawal as Dr. Ghungroo
* Nana Patekar as Uday Shankar Shetty
* Anil Kapoor as Sagar Majnu Pandey
* Mallika Sherawat as Ishika/Isha
* Feroz Khan as Ranvir Dhanraj Xaja RDX
* Supriya Karnik as Mrs. Ghungroo
* Sherveer Vakil as Lucky
* Malaika Arora Khan in item number
* Asrani as fake Producer
* Charlie as The Municipal worker
* Snehal Dabi as Sagars goon
* Paresh Ganatra as Purushottam (Pappu), The Municipal officer
* Adi Irani as Udays Lawyer
* Mushtaq Khan as Ballu, Udays man
* Sanjay Mishra (actor) as the Pandit
* Om Puri as Narrator
* Vijay Raaz as fake Director
* Ranjeet as Mr. Kapoor
* Sunil Shetty as Himself (Special Appearance)

==Soundtrack==
The soundtrack was composed by Anand Raj Anand, Himesh Reshammiya & Sajid Wajid. The lyrics were penned by Anand, Sameer (lyricist)|Sameer, Shabbir Ahmed, Ibrahim Ashq and Anjaan Sagiri.  

===Track listing===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centre"
! Song !! Singer(s) !! Music Director
|-
|Honth Rasiley
| Shankar Mahadevan, Anand Raj Anand, Shreya Ghosal
| Anand Raj Anand and Aadesh Shrivastava(music arrangement)
|-
|Insha Allah
| Krishna, Shaan (singer)|Shaan, Akruti Kakkar, Himesh Reshammiya
| Himesh Reshammiya
|-
| Kola Laka Vellari
| Himesh Reshammiya
| Himesh Reshammiya
|-
|Kiya Kiya
| Anand Raj Anand, Shweta Pandit
| Anand Raj Anand
|-
|Uncha Lamba Kad
| Anand Raj Anand, Kalpana Patowary
| Anand Raj Anand
|-
| Welcome Wajid
| Wajid
|-
|}

==Reception==

=== Critics rating===
The film received mixed reviews from critics and holds an average rating of 6.2 on IMDb out of 10.

==Box office==
At the box office, Welcome opened to a massive response locally, by grossing   on its opening week.  Despite facing competition from the popular Aamir Khan-starrer, Taare Zameen Par which was also a huge success, Welcome grossed   domestically, and was elevated to "Blockbuster" verdict. 
Soon enough, it became the second highest grossing film of 2007, just behind Om Shanti Om. The worldwide gross was around  . 

==Sequel== Welcome Back John Abraham, Anil Kapoor, Nana Patekar, Paresh Rawal and Shruti Haasan. Earlier reported to release on 19 December 2014, the project has been pushed forward and is now expected to release on 29th May 2015. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 