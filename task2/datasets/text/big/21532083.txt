Padai Veetu Amman
 

{{Infobox film
| name           = Padai Veetu Amman
| image          = 
| image_size     =
| caption        = 
| director       = Pugazhmani
| producer       = Televista Digitals Limited
| writer         = Pugazhmani Meena (actress)|Meena|Devayani Devayani
|Ravali|Vinu Senthil (actor)|Senthil|Nizhalgal Ravi}}
| music          = S. A. Rajkumar
| cinematography = Baby Philips
| editing        = R. K. Baburaj
| distributor    =
| studio         = Televista Digitals Limited
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}}
 2002 Tamil religious film Devayani and Ravali in lead roles. The film, produced by Televista Digitals Limited, had musical score by S. A. Rajkumar and was released on 4 November 2002.

==Plot==

Meena plays the dual role of Goddess Padai Veettu Amman, and the neighbouring deity Muthu Maariamman, cutting a pretty picture in all her finery. Devayani plays Chamundi, a devotee, and the film opens with her being killed brutally on the eve of her marriage by the henchmen of the local bigwig. The latters daughter was in love with the intended bridegroom Shankar and the fond father had wanted to remove the only hurdle in the way. The bigwig, in order to get his hands on a hidden treasure, had also taken the help of an evil tantrik. The presiding deity Muthu Maariamman is helpless, bound as she was by a vow she had made to Chamundi. That she wouldnt step out of her abode till Chamundi herself asked her to. It is time for the neighbouring deity Padai Veetu Amman to come to the rescue of the distressed village.

==Cast==

*Ramki Meena
*Devayani Devayani
*Ravali
*Vinu Chakravarthy Senthil
*Nizhalgal Ravi
*P. R. Varalakshmi
*K. R. Vatsala
*Baby Akshaya
*Vellai Subaiah
*Bayilvan Ranganathan
*Suryakanth

 
 
 
 


 