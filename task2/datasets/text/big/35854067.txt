Semana santa
{{Infobox film
| name           = Semana santa
| image          = 
| image_size     = 
| caption        = 
| director       = Pepe Danquart
| producer       = Paul Berrow   Philippe Guez David Hewson      Roy Mitchell   
| starring       = Mira Sorvino   Olivier Martinez 
| music          = Andrea Guerra (composer)| Andrea Guerra
| cinematography = Ciro Cappellari
| editing        = Peter R. Adam   Joëlle Hache
| distributor    = MGM 
| released       =  
| runtime        = 89 minutes
| country        = International   co-production
| language       = English
| budget         = 
}}
 mystery thriller film directed by Pepe Danquart. It is notable for its international cast.

==Plot==
During the Spanish Civil War the 14-years-old Doña Catalina (Yohana Cobo) is captured by war criminal Antonio Alvarez. She is forced to witness how her father is murdered with a traditional bullfighting weapon known as „rejón de muerte“. Following that her fathers murderer rapes her. After this traumatic experience she can get hold of a knife and attempts to take revenge. But she is grabbed by one of Alvarez accomplices who also tries to abuse her. She stabs him and escapes. A friendly couple saves her. Doña Catalina recovers but turns out being pregnant. The birth is difficult and the couple tells Doña her boy had died. Later they disappear and take the allegedly dead boy with them.

The war criminal who raped her remains unpunished. More than that, after the war he gets more and more powerful as a local politician. He keeps on befathering teenage girls, always getting away by making presents and paying child support. Yet a Christian brotherhood expels him.

Forty years later, during the Semana Santa, again people get killed in the same manner Alvarez once murdered Doñas father. Police detective Quemada and his new colleague Maria Delgado try to find a trace. They interview the leader of a Christian brotherhood and later on an expert for bullfighting. Finally it is revealed that  the late Antonio Alvarez had advised his illegitimate sons to stick together and had hereby created a still existing network. 

The sons believe they had to get back at their fathers former enemies. Doñas lost son is on top of this Conspiracy (crime)| conspiracy. As a high-ranked local policeman he is a superior of Quemada and Delgado. In order to oppress the truth he doesnt hesitate to kill his subordinates or even his half-brothers.

In the end Doña saves Maria Delgados life by firing a gun at her lost son.

==Cast==
* Alida Valli as Doña Catalina
* Yohana Cobo as young Doña Catalina
* Mira Sorvino as Maria Delgado
* Olivier Martinez as Quemada
* Féodor Atkine as Torillo
* Peter Berling as Castaneda
*Luis Tosar as Antonio

==Reception==
Critics found occasionally the film adaptation of  David Hewsons novel „Santa Semana“ was not completely logical.  {{Cite web|url= http://www.dvdfuture.com/review.php?id=728
|title= Though I seem to have only praise for "Angel Of Death" there is a downside. Like a lot of European slasher pictures the logic does not follow through. |accessdate=2012-05-17}}  

==References==
 

==External links==
* 
*  
*   by Randy at  

 
 
 
 
 
 
 