Bluff (film)
{{Infobox film
 | name = Bluff
 | image = Bluff VideoCover.jpeg
 | caption = 
 | director = Simon Olivier Fecteau Marc-André Lavoie
 | producer = Marc-André Lavoie Jean-René Parteneau Simon Olivier Fecteau Pierre Brousseau (executive producer)
 | writer = Marc-André Lavoie Simon Olivier Fecteau
 | narrator =  Isabelle Blais Emmanuel Bilodeau Marie-Laurence Moreau
 | music = Frédéric Bégin
 | cinematography = Marc-André Lavoie (director of photography)
 | editing = 
 | distributor = Seville Pictures
 | released =  
 | runtime = 88 minutes
 | country = Canada
 | language = French
 | budget =
 | gross = 
}}
Bluff is a 2007 Canadian comedy film. It was directed, written and produced by Simon Olivier Fecteau and Marc-André Lavoie.

== Plot == building inspector finds a shocking discovery in the basement to a building that is about to be destroyed. He contacts the landlord and, as the pair wait for the police to show up, the story of this discovery comes uncovered.

== Recognition == Best Original Screenplay at the 28th Genie Awards.

== External links ==
*  
*  

 
 
 
 
 
 