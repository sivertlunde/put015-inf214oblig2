The Gospel According to St. Matthew (film)
 
 
{{Infobox film
| name = The Gospel According to St. Matthew
| image = Pasolini Gospel Poster.jpg
| image_size = 215px
| alt = 
| caption = Original Italian release poster
| director = Pier Paolo Pasolini
| producer = Alfredo Bini
| based on = Gospel of Matthew 
| writer = Pier Paolo Pasolini
| starring = Enrique Irazoqui
| music =  
| cinematography = Tonino Delli Colli
| editing = Nino Baragli
| distributor = Titanus Distribuzione S.p.a.
| released =  
| runtime = 137 minutes  
| country = Italy
| language = Italian
}} biographical drama Nativity through the Resurrection of Jesus|Resurrection.
 John was Mark too Luke too sentimental." 

==Plot==
In Palestine during the Roman Empire, Jesus Christ of Nazareth travels around the country with his disciples preaching to the people about God and salvation of their souls. He is the son of God and the prophesied messiah, but not everyone believes his tale. He is arrested by the Romans and crucified. He rises from the dead after three days.

==Cast== Christ
** Enrico Maria Salerno as Christs voice Mary (younger)
** Susanna Pasolini as older Mary Joseph
** Gianni Bonagura as Josephs voice
* Mario Socrate as John the Baptist
** Pino Locchi as Johns voice Peter
* Andrew
* James
* John
* Phillip
* Bartholomew
* Thomas
* Matthew
* Marcello Galdini as James, son of Alphaeus Thaddaeus
* Simon
* Otello Sestili as Judas Iscariot
* Juan Rodolfo Wilcock as Caiaphas
* Alessandro Clerici as Pontius Pilate
* Amerigo Bevilacqua as Herod the Great
* Francesco Leonetti as Herod Antipas Herod
* Paola Tedesco as Salome
* Rossana Di Rocco as Angel of the Lord Possessed one
* Eliseo Boschi as Joseph of Arimathea
* Natalia Ginzburg as Mary of Bethany
* Ninetto Davoli (uncredited) as a shepherd
* Umberto Bevilacqua (uncredited) as a soldier

==Production==
===Background and pre-production=== four Gospels biblical account an amalgam The Gospel of John). Pasolini stated that he decided to "remake the Gospel by analogy" and the films sparse dialogue all comes directly from the Bible. Wakeman. pp. 746. 

Given Pasolinis well-known reputation as an atheist, a homosexual, and a Marxist, the reverential nature of his film was surprising , especially after the controversy of La ricotta. At a press conference in 1966, Pasolini was asked why he, an unbeliever, had made a film which dealt with religious themes; his response was, "If you know that I am an unbeliever, then you know me better than I do myself. I may be an unbeliever, but I am an unbeliever who has a nostalgia for a belief."  The film begins with an announcement that it is "dedicato alla cara, lieta, familiare memoria di Giovanni XXIII" ("dedicated to the dear, joyous, familiar memory of Pope John XXIII"), as John XXIII was indirectly responsible for the films creation, but had died before its completion.

===Filming and style=== mother of Jesus. The cast also included noted intellectuals such as writers Enzo Siciliano and Alfonso Gatto, poets Natalia Ginzburg and Juan Rodolfo Wilcock, and philosopher Giorgio Agamben. In addition to the original biblical source, Pasolini used references to "2,000 years of Christian painting and sculptures" throughout the film. The look of the characters is also eclectic and, in some cases, anachronistic, resembling artistic depictions of different eras (the costumes of the Roman soldiers and the Pharisees, for example, are influenced by Renaissance art, whereas Jesus appearance has been likened to that in Byzantine art as well as the work of Expressionist artist Georges Rouault).  Pasolini later described the film as "the life of Christ plus 2,000 years of storytelling about the life of Christ." Wakeman. pp. 747. 

Pasolini described his experience filming The Gospel According to St. Matthew as very different from his previous films. He stated that while his shooting style on his previous film Accattone was "reverential," when his shooting style was applied to a biblical source it "came out rhetorical. ... And then when I was shooting the baptism scene near Viterbo I threw over all my technical preconceptions. I started using the zoom, I used new camera movements, new frames which were not reverential, but almost documentary   an almost classical severity with the moments that are almost Jean-Luc Godard|Godardian, for example in the two trials of Christ shot like "cinema verite." ... The Point is that ... I, a non-believer, was telling the story through the eyes of a believer. The mixture at the narrative level produced the mixture stylistically." 

===Music===
The score of the film is Eclecticism in music|eclectic, ranging from Johann Sebastian Bach (e.g. Mass in B Minor and St Matthew Passion) to Odetta ("Sometimes I Feel Like a Motherless Child"), to Blind Willie Johnson ("Dark Was the Night, Cold Was the Ground"), to the Jewish ceremonial declaration "Kol Nidre" and the "Gloria in Excelsis Deo|Gloria" from the Congolese Missa Luba. Pasolini stated that all of the films music was of a sacred or religious nature from all parts of the world and multiple cultures or belief systems. 

==Reception== Alexander Walker said that "it grips the historical and psychological imagination like no other religious film I have seen. And for all its apparent simplicity, it is visually rich and contains strange, disturbing hints and undertones about Christ and his mission." 
 the loaves Christ walking on water are disgusting Pietism." He also stated that the film was "a reaction against the conformity of Marxism. The mystery of life and death and of suffering — and particularly of religion ... is something that Marxists do not want to consider. But these are and have always been questions of great importance for human beings." 

The Gospel According to St. Matthew was ranked number 10 (in 2010) and number 7 (in 2011) in the Arts and Faith websites Top 100 Films,  also is in the Vaticans list of films|Vaticans list of 45 great films and Roger Eberts Great Movies list.  

The film currently has an approval rating of 94% on the review aggregator Rotten Tomatoes, with 30 "fresh" and 2 "rotten" reviews. 

==Awards==
At the 1964 Venice Film Festival, The Gospel According to St. Matthew was screened in competition for the Golden Lion, and won the OCIC Award and the Special Jury Prize. At the films premiere, a crowd gathered to boo Pasolini but cheered him after the film was over. The film later won the Grand Prize at the International Catholic Film Office. 
 Costume Design (Danilo Donati), and Academy Award for Original Music Score|Score. {{cite web|url=http://movies.nytimes.com/movie/review?_r=1&res=9800E2D9143CE53BBC4052DFB466838D679EDE|title=Movie Review -
  The Gospel According to St Matthew - Screen:The Life of Jesus:Pasolinis Film Opens at the Fine Arts - NYTimes.com|publisherMovies.nytimes.com=|accessdate=3 January 2015}} 

==Alternate versions==
The 2007 Region 1 DVD release from Legend Films features a Film colorization|colorized, English-dubbed version of the film, in addition to the original, black-and-white Italian-language version. (The English-dubbed version is significantly shorter than the original, with a running time of 91 minutes — roughly 40 minutes shorter than the standard version.)

==References==
 

;Sources
* Bart Testa, "To Film a Gospel ... and Advent of the Theoretical Stranger," in Patrick Rumble and Bart Testa (eds.), Pier Paolo Pasolini: Contemporary Perspectives. University of Toronto Press, Inc., 1994, pp.&nbsp;180–209. ISBN 0-8020-7737-4.
* "Pasolini, Il Cristo dellEresia (Il Vangelo secondo Matteo). Sacro e censura nel cinema di Pier Paolo Pasolini (Edizioni Joker, 2009) by Erminia Passannanti, ISBN 978-88-7536-252-2

==External links==
*  
*  
*  
*  

 
 
{{succession box Special Jury Prize, Venice
| years=1964 tied with Hamlet (1964 film)|Hamlet
| before=The Fire Within tied with Introduction to Life
| after=Simon of the Desert, I Am Twenty and Modiga Mindre Män}}
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 