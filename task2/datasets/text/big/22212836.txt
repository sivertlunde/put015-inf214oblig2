Daddy Long Legs (1931 film)
{{Infobox film
| name           = Daddy Long Legs
| image          = Daddy Long Legs (1931) Movie Poster.jpg
| image size     = thumb
| alt            = 
| caption        = Daddy Long Legs (1931) Movie Poster
| director       = Alfred Santell
| producer       =  Sol M. Wurtzel
| writer         = Novel:      Sonya Levien   Alfred Santell
| narrator       = 
| starring       = Janet Gaynor   Warner Baxter   Una Merkel   John Arledge
| music          = Hugo Friedhofer
| cinematography = Lucien N. Andriot
| editing        = Ralph Dietrich
| studio         = Fox Film Corporation
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1.5 million   accessed 19 April 2014 
| preceded by    = 
| followed by    = 
}}

Daddy Long Legs (1931) is a film about an orphan who is taken under the wing of a wealthy benefactor. 
 original story, written in 1912 by Jean Webster, compared the childhood of the wealthy to those in the orphanage.

Although, suffering under a tough matron, Judy Abbott (Janet Gaynor) manages to cope and help the other orphans, through intelligence and hard work.

Wealthy Jervis Pendleton (Warner Baxter), who is the benefactor, cant help admiring his young charge, though she doesnt know that hes sponsoring her schooling.

She does not realize he has been sponsoring her, even when they meet, through Jervis niece, who is Judys room mate; and, the girl who was once alone, has to choose between their growing affection, and a younger suitor. 

  ]]

==Cast==
*Janet Gaynor ...  Judy Abbott
*Warner Baxter ...  Jervis Pendleton
*Una Merkel ...  Sally McBride
*John Arledge ...  Jimmy McBride
*Claude Gillingwater ...  Riggs (as Claude Gillingwater Sr.)
*Effie Ellsler ...  Mrs. Semple
*Kendall McComas ...  Freddie Perkins
*Kathlyn Williams ...  Mrs. Pendleton Elizabeth Patterson ...  Mrs. Lippett (matron)
*Louise Closser Hale ...  Miss Pritchard
*Sheila Bromley ...  Gloria (as Sheila Mannors)
*Billy Barty ...  Orphan (uncredited)
*Edith Fellows ...  Orphan (uncredited)
*Clarence Geldart ...  Minister at Commencement (uncredited)
*Edwin Maxwell ...  Wykoff (uncredited)
*Steve Pendleton ...  Sallys Beau (uncredited)
*Martha Lee Sparks ...  Katie (uncredited)

==Production==
Janet Gaynor had won the first Academy Award for Best Actress in 1928 and Warner Baxter had won the second Academy Award for Best Actor for his portrayal of the Cisco Kid in In Old Arizona by the time Daddy Long Legs was released.

The screenplay was based on the stage play, Daddy Long-Legs by Jean Webster. 

Fox made a French and Italian dubbed version of the film; and, successfully sued a Dutch company, for making a film based on the same material. 

==References==
 

==See also==
Other versions of the Jean Webster novel:
*Daddy-Long-Legs (1919 film) with Mary Pickford
*Curly Top (film), 1935 film with Shirley Temple based on the novel
*Daddy Long Legs (1938 film), Dutch film with Lily Bouwmeester
*Daddy Long Legs (1955 film) with Fred Astaire and Leslie Caron
*My Daddy Long Legs (1990) Japanese anime TV series
*Daddy-Long-Legs (2005 film), Korean film

==External links==
* 


 
 
 
 
 
 