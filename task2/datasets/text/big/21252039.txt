Windfall in Athens
 
{{Infobox film
| name           = Windfall in Athens
| image          =
| caption        =
| director       = Michael Cacoyannis
| producer       =
| writer         = Michael Cacoyannis
| starring       = Ellie Lambeti
| music          =
| cinematography = Alevise Orfanelli
| editing        =
| distributor    =
| released       =  
| runtime        = 95 minutes
| country        = Greece
| language       = Greek
| budget         =
}}

Windfall in Athens ( ,  ) is a 1954 Greek comedy film directed by Michael Cacoyannis. It was entered into the 1954 Cannes Film Festival.   

==Plot==
Mina, a beautiful, care-free salesgirl for a milliners shop, is robbed by two urchins while she is enjoying a Sunday morning swim.  The lottery ticket she had in her purse ends up being bought by a charming, penniless young musician, and eventually wins the lottery.  The two engage in a legal battle for the lottery money, but end up falling in love.

==Cast==
* Ellie Lambeti - Mina Labrinou
* Dimitris Horn - Alexis
* Giorgos Pappas - Pavlos
* Tasso Kavadia - Liza
* Margarita Papageorgiou - Irini
* Sapfo Notara - Miss Ketty
* Theano Ioannidou
* Hrysoula Pateraki
* Kiki Persi
* Thanasis Veggos

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 