Wah-Wah (film)
{{Infobox film
  | name = Wah-Wah
  | image = Wah wah.jpg
  | caption = Theatrical release poster
  | director = Richard E. Grant
  | producer = Marie-Castille Mention-Schaar Jeff Abberley Pierre Kubel
  | writer = Richard E. Grant
  | starring = Nicholas Hoult Gabriel Byrne Emily Watson Miranda Richardson Julie Walters
  | music = Patrick Doyle
  | cinematography = Pierre Aim
  | editing = Isabelle Dedieu
  | studio = Scion Films IMG Productions Lorna Nasha Reeleyes Film Redbus Film Distr.   
  | released =  
  | runtime = 97 min.
  | country = UK France South Africa
  | language = English
  | budget = $7 million
}} British actor Richard E. Grant and loosely based on his childhood in Swaziland.  It stars  Nicholas Hoult, Gabriel Byrne, Emily Watson, Miranda Richardson and Julie Walters.

Filmed and set in Swaziland, the film was first shown at the Cannes Film Market on 13 May 2005 and premiered at the Edinburgh International Film Festival on 17 August 2005. It then toured to various festivals before receiving a limited release in the United States on 5 May 2006, followed by its release in the United Kingdom on 2 June 2006. 

==Plot==
With this semi-autobiographical tale of his childhood in Swaziland during the last days of the British Empire in Africa in 1960s, Grant relates the story of Ralph Compton, whose family’s disintegration mirrors the end of British rule.  After witnessing his mothers adultery with his fathers best friend, Ralph must survive not only boarding school but also his beloved fathers remarriage to Ruby, a fast-talking American Airlines stewardess, and his fathers gradual descent into alcoholism.

==History==

===Development and pre-production===
Grant initially wrote the film loosely based on his own childhood experiences after a screenwriter recommended he write a screenplay after reading his memoirs of his Withnail and I experiences. The first meeting with a producer took place in 1999 and took almost seven years to complete.   Grant initially had trouble securing actors; Rachel Weisz, Toni Collette, Meg Ryan, Emmanuelle Béart, Ralph Fiennes and Jeremy Irons all turned down roles.  Julie Walters was eventually the first actor to be signed.  Grant intended for the part of Ralph to be played by two actors but the casting director, Celestia Fox, insisted on one actor.  During the casting sessions, Grant noticed the similarity between Zac Fox and Nicholas Hoult and persuaded Celestia Fox to cast them.  The part of the father was meant to be younger but, as all the actors  asked in that age range passed on the role, an older actor (Gabriel Byrne) was chosen.  After the original producer left the project due to a career change, Grant was approached by Marie-Castille Mention-Schaar to produce the project, a decision he later came to regret. 

=== Production and aftermath ===
The film was the first to have been shot in Swaziland. Filming began in July 2004 and took place over seven weeks and post-production took place in England and France. 

Wah-Wah received its premiere at the Edinburgh International Film Festival and received a special Toronto Film Festival screening in September 2005.  Grant also kept a diary of his experiences of the film, later published as a book, (The Wah-Wah Diaries), which was met positively by the critics, many of whom were impressed at the honesty of the tale, especially in regard to his frictional relationship with the "inexperienced" producer.     Grant mentioned in subsequent interviews that she was a "control freak out of control" and that he would "never see her again as long as   lives".   In a BBC interview, he again mentioned his "disastrous" relationship with Mention-Schaar, relating that he received five emails in the last two months of pre-production from her, that she rarely turned up on the set and failed to obtain clearance both for the song rights and to actually film in Swaziland (without the knowledge of Grant, who eventually was forced to meet the King of Swaziland to seek clemency).

===Main cast===
*Gabriel Byrne as Harry
*Miranda Richardson as Lauren
*Emily Watson as Ruby
*Nicholas Hoult as Ralph (older)
*Zac Fox as Ralph (younger)
*Julie Walters as Gwen
*Celia Imrie as Lady Hardwick
*Julian Wadham as Charles
*Fenella Woolgar as June Broughton
*Sid Mitchell as Vernon

==Critical reception== Variety said White Mischief")."  

===Box office===
Due to the films limited cinema release, Wah-Wah only earned about $234,750 in the United States and totalled around $2,846,148 world wide.   

== References ==
 

== External links ==
*  – official site
* 
*  interview on Australian Broadcasting Corporation|ABCs Enough Rope

 
 
 
 
 
 
 
 
 