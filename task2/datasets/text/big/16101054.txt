Walking (film)
{{Infobox film
| name = Walking
| image = walking_film_screenshot.jpg
| caption = Screenshot from Walking
| director = Ryan Larkin
| producer = Ryan Larkin
| writer = Ryan Larkin
| starring =
| music = David Fraser Pat Patterson Christopher Nutter
| cinematography =
| editing =
| distributor = National Film Board of Canada 1968
| runtime = 5 minutes
| country = Canada
| language =
| budget = $21,676   
| gross =
}}
 1968 Canada|Canadian animated short film directed and produced by Ryan Larkin for the National Film Board of Canada, composed of animated vignettes of how different people walk.   
 In the Labyrinth for Expo 67, Larkin submitted a proposal to the NFB for a short film based on sketches of people walking. It took him two years to make the film—twice as long as expected—as he was perfecting new ink wash painting techniques in order to not repeat his earlier films. He was also absorbed in exploring human movements and behaviour, even setting up mirrors in his small studio to study his own motions.   

It was nominated for an Academy Award for Animated Short Film at the 42nd Academy Awards. Excerpts from the film also appear in the Oscar-winning short about Larkin, Ryan (film)|Ryan.

Walking was one of seven NFB animated shorts acquired by the American Broadcasting Company, marking the first time NFB films had been sold to a major American television network. It aired on ABC in the fall of 1971 as part of the children’s television show Curiosity Shop, executive produced by Chuck Jones.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 
 