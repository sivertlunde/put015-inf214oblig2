The House of the Angel
{{Infobox film
| name           = The House of the Angel
| image          =
| image_size     =
| caption        =
| director       = Leopoldo Torre Nilsson
| producer       =
| writer         = Beatriz Guido Martín Rodríguez Mentasti Leopoldo Torre Nilsson
| narrator       =
| starring       = Elsa Daniel
| music          =
| cinematography = Aníbal González Paz
| editing        = Jorge Gárate
| distributor    =
| released       = 11 July, 1957
| runtime        = 76 minutes
| country        = Argentina
| language       = Spanish
| budget         =
| preceded_by    =
| followed_by    =
}} Argentine film directed by Leopoldo Torre Nilsson from a novel by Beatriz Guido. It was entered into the 1957 Cannes Film Festival.   

== Cast ==
*Elsa Daniel - Ana
*Lautaro Murúa - Pablo Aguirre
*Guillermo Battaglia - Dr. Castro, Father of Ana
*Berta Ortegosa - Señora de Castro, Mather of Ana
*Yordana Fain - Naná
*Bárbara Mujica - Vicenta
*Alejandro Rey - Julian
*Lili Gacel	- Julieta

==References==
 

== External links ==
* 

 
 
 
 
 
 
 
 