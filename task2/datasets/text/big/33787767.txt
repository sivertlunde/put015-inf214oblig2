Arunachal Pradesh to Thimphu
{{Infobox film
| name           = Arunachal Pradesh to Thimphu
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jigme Ring
| producer       = Kinden Entertainment
| writer         = Jigme Ring
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Jigme Ring
| music          = Tandin Dorji
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2008 
| runtime        = 
| country        = Bhutan
| language       = Dzongkha
| budget         = 
| gross          = 
}}

Arunachal Pradesh To Thimphu is a 2008 Bhutanese, Dzongkha language film written and directed by Jigme Wangchuk,  known by his stage name Jigme Ring.

== Songs ==
The film included the following songs:
#Ko Ko Lay Ko (singers: Jigme Nidup and Jigme Ring; Lyrics: Jigme Ring)
#Tawang Bazar (singers: Dechen Pem and Namgay Jigs; Lyrics: Jigme Ring and Sang Tashi)
#Sho Sho (singer: Namgay Jigs; Lyrics: Jigme Ring and Sang Tashi)
#Kayllin (singers: Jigme Nidup and Dechen Pem; Lyrics: Jigme Ring and Sang Tashi)
#Ya Nam Kha Ma So (singers: Tashi Gyeltshen and Dechen Pem; Lyrics: Jigme Ring and Sang Tashi)
#True Love (singers: Tenzin Wangdi and Jamyang Zangmo; Lyrics: Jigme Ring and Sang Tashi)
#So Ya So Ya (singers: Dechen Pem and Namgay Jigs; Lyrics: Tenzin Wangdi)
#Ha Hu... (singers: Dechen Pem and Namgay Jigs; Lyrics: Jigme Ring)

== Awards ==
"Best debutante actor award", 8th National Bhutanese Film Festival 

== References ==
 

== External links ==
* 
* 

 
 
 
 