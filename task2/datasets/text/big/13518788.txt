Deewangee
 
{{Infobox film
| name           = Deewangee
| image          = Deewangee.jpg
| image_size     =
| caption        = DVD Cover
| director       = Anees Bazmee
| producer       = Nitin Manmohan
| story          = Anees Bazmee
| screenplay     = Anees Bazmee
| narrator       =
| starring       = Ajay Devgan Akshaye Khanna Urmila Matondkar
| music          = Ismail Darbar
| cinematography =
| editing        =
| distributor    = Neha Arts
| released       =  
| runtime        = 169 minutes
| country        = India
| language       = Hindi
| budget         = 15 cr
| gross          = 84 cr
}}
 Indian psychological thriller film directed by Anees Bazmee.  This movie is loosely based on the 1996 Richard Gere movie Primal Fear (film)|Primal Fear. The film stars Ajay Devgan, Akshaye Khanna and Urmila Matondkar in leading roles.  The film premiered on 25 October 2002. This was Devgans first negative role.

==Plot==
Raj Goyal (Akshaye Khanna), a young and successful criminal lawyer, famous for never having lost a case, is introduced to popular singer Sargam (Urmila Matondkar) by music magnate Ashwin Mehta (Vijayendra Ghatge). The following day, Ashwin is brutally murdered in his own house. The murderer, Taran Bharadwaj (Ajay Devgan), who is Sargams childhood friend and current mentor, is caught red-handed at the crime scene. He claims he is innocent and Sargam, who believes in Tarans innocence, approaches Raj to defend him, which he accepts after meeting Taran.

Raj finds out that Taran suffers from split personality disorders and wins Tarans case by proving the same. Immediately after Taran is acquitted, Raj discovers that the split personality was an act put up by Taran. Raj wants to put Taran in prison by re-opening the case.

==Cast==
* Ajay Devgn as Taran/Ranjit Bharadwaj
* Akshaye Khanna as Raj Goyal
* Urmila Matondkar as Sargam
* Farida Jalal as Mrs. Goyal (Rajs mother)
* Tiku Talsania as Ratan (Rajs uncle)
* Suhasini Mulay as a Judge
* Seema Biswas as a Psychiatrist
* Rana Jung Bahadur
* Vijayendra Ghatge as Ashwin Mehta
* Mohan Kapoor
* Nirmal Pandey as Abhijeet Mehta
* Anil Nagrath
* Tanaz Currim as Yana
* Nishigandha Wad
* Suresh Oberoi as Mr. Bhullar
* Madan Joshi
* Neelam Kothari
* Sonu
* Deepak Bhatia
* Mazhar Khan

==Awards==
Won
*Filmfare Best Villain Award - Ajay Devgan
*Star Screen Award Best Villain - Ajay Devgan
*Zee Cine Award Best Actor in a Negative Role - Ajay Devgan
Nominated
*Star Screen Award for Best Actor - Akshaye Khanna

== Music ==
{{Infobox album   Name        = Deewangee Type        = Album Artist      = Ismail Darbar Cover       = Released    =   24 September 2002 (India) Recorded    = Genre  Feature film soundtrack Length      =  Label       =  T-Series Producer    = Ismail Darbar Reviews     =  Last album  = Shakti - The Power (2002) This album  = Deewangee (2002) Next album  = Baaz (2003)
}}

The soundtrack of the film contains 9 songs. The music is conducted by composer Ismail Darbar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)
|-
|"Ai Ajnabi"
| Sunidhi Chauhan
|-
| "Deewangee"
| Krishnakumar Kunnath|KK, Mahalaxmi Iyer
|-
| "Ye Taazgi Yeh Saadgi"
| Sunidhi Chauhan
|-
| "Dholi O Dholi"
| Babul Supriyo, Kavita Krishnamurthy
|-
| "Hai Ishq Khata"
| Jaspinder Narula
|-
| "Pyar Se Pyare Tum Ho"
| Sonu Nigam, Kavita Krishnamurthy
|-
| "Saasein Saasein Hain"
| Sonu Nigam, Kavita Krishnamurthy
|-
| "Pyar Se Pyare Tum Ho (sad)"
| Sonu Nigam
|-
| "Saat Suron Ka"
| Udit Narayan, Kavita Krishnamurthy
|}

==Box Office==

The film collected Rs.84 crore at the box office and was rated a super hit.

==External links==
* 

 

 
 
 