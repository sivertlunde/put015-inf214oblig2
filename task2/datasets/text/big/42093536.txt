Tifosi (film)
 
{{Infobox film
 | name = Tifosi
 | image = Tifosi_(film).jpg
 | caption =
 | director =Neri Parenti
 | writer = Enrico Vanzina & Carlo Vanzina, Neri Parenti, Fausto Brizzi, Marco Martani
 | starring = Christian De Sica, Massimo Boldi, Diego Abatantuono, Enzo Iacchetti, Maurizio Mattioli, Nino DAngelo & Diego Armando Maradona
 | music =   Bruno Zambrini
 | cinematography =  Gianlorenzo Battaglia
 | editing =  Alberto Gallitti 
 | producer = Aurelio De Laurentiis 
 | language = Italian 
 | country = Italy
 | released = 1999
 | runtime = 120 min
 }}
Tifosi (also known as Fans) is a 1999 Italian comedy film directed by Neri Parenti. 

== Plot == Interist Carlo, hooligan "Zebrone", Napoli and Atalanta B.C.|Atalanta.

== Cast ==
* Massimo Boldi: Silvio Galliani
* Christian De Sica: Cesare Proietti
* Diego Abatantuono: "Zebrone"
* Enzo Iacchetti: Carlo Colombo
* Maurizio Mattioli: Nando
* Nino DAngelo: Gennaro Scognamiglio
* Diego Armando Maradona: himself
* Angelo Bernabucci: Fabio
* Peppe Quintale: Ferdinando
* Alessandra Bellini: Marta Proietti
* Giulio Brunetti: Fabrizio Colombo
* Patrizia Loreti: Maria Luigia, detta "Topa Gigia"
* Victor Poletti: Callisto Culatello
* Alessandro Bettocchi: Pierpaolo Culatello
* Giorgio Roma: Poldo Culatello
* Antonio Allocca: Zio Gaetano
* Massimo Ferrante: Zebrino Andrea Moretti: Zebrino
* Maurizio Mosca: himself
* Edrissa Sanneh|Idris: himself
* Massimo Caputi:himself
* Giacomo Bulgarelli:himself
* Ela Weber:himself
* Giampiero Galeazzi:himself
* Fabio Fazio: himself
* Franco Baresi:himself
* Pasquale Bruno:himself
* Erika Bernardi:hostess
* Antonio Spinnato:
* Bruno Gambarotta:
* Franco Neri: 
* Peppe Lanzetta:

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 