Rechukka
{{Infobox film
| name           = Rechukka நாட்டிய தாரா
| image          =
| image_size     =
| caption        =
| director       = P. Pullaiah
| producer       = Ghantasala Krishnamurthy
| writer         = Ghantasala Balaramaiah   (story)  Malladi Ramakrishna Sastry   (screenplay, dialogues and lyrics) 
| narrator       =
| starring       = N.T. Rama Rao Anjali Devi Devika Akkineni Nageswara Rao Mukkamala Nagabhushanam Peketi Sivaram
| music          = Ashwathama
| cinematography = P. L. Roy
| editing        = G. D. Joshi
| studio         = Revathi studios
| distributor    =
| released       = May 23, 1954
| runtime        =
| country        = India Telugu
| budget         = Rs. 3-4 lakhs
}}
Rechukka is a 1954 Telugu film directed by Ghantasala Balaramaiah and P. Pullaiah and produced by Ghantasala Krishnamurthy. N. T. Rama Rao acted in the lead role as prince. Devika debuted in the film as Princess. The story was inspired by The Prince Who Was a Thief (1951) starring Tony Curtis. 
 Tamil by Kalyanaraman entitled Naattiya Thara ( ). It was also a commercial hit.

==Plot==
The story is set in a kingdom. The chief minister wants his daughter to marry the Kings only son, the future prince. The King refused the idea, and the angry minister captures and arrests the King and runs the Kingdom. A loyal countryman Veeranna (Nagabhushanam) takes the Prince and leaves him in the forest away from the evil ministers hands, but gets captured himself. The Prince, grows up as the forest chiefs adopted son Kannayya (N.T. Rama Rao). Veerannas daughter is Nana (Anjali Devi). Both of them grows up as thieves. The old King escapes finally and stays at Nanas house. Nana steals the Princess Lalita Devis (Devika) necklace and Kannayya returns it to her. The princess falls in love with him. Nana also falls for the Kannayya too. He gets to know his true identity, defeats his enemies and gets back his Kingdom.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Nandamuri Taraka Rama Rao || Kannaiah / Yuvaraju
|-
| Anjali Devi || Nana, daughter of Veeranna
|-
| Mukkamala Krishna Murthy || King
|-
| Prameela (Devika) || Lalita Devi
|-
| Nagabhushanam || Chitti Veeranna
|-
| Sadashiva Rao || Minister
|-
| Peketi Sivaram || Annayya
|-
| Jogarao || Chandrayya
|-
| Y. V. Raju || Nagulu
|-
| Akkineni Nageshwara Rao || (Guest actor)
|-
| Kanchi Narasimham
|-
| Lingam Subba Rao
|-
| Kakinada Rajaratnam
|}

==Production==
The Rechukka film is an outcome of Ghantasala Balaramaiah, Telugu film producer of Pratibha Pictures and General Manager and Production Executive of the company Thopalli Venkata Sundara Shivarama Sastry (better known as Pratibha Sastry). Sastry has seen the English film entitled A Prince Who Was a Thief in Mount Road. Balaramaiah prepared a story to suit local audience. The story was given to Malladi Ramakrishna Sastry, who wrote the screenplay, dialogues and lyrics for the film.

The shooting was started with N. T. Rama Rao, Anjali Devi and supporting staff and Ashwathamma as Music director. After shooting three reels including two songs, Ghantasala Balaramaiah died suddenly due to Heart attack on October 29, 1953. Ghantasala Krishna Murthy, the elder son of Balaramaiah has taken the charge of Pratibha pictures. The film is completed with the financial involvement of Sunderlal Nahata and director P. Pullaiah.

Most of the film was shot at Revathi Studios at Madras. Prakash Studios was used for few important scenes. The forest scenes are pictured at Jamal Gardens (Now Vijaya Gardens).

For the role of Lalitha Devi, they wanted to take new actress. They have chosen Prameela, the grand daughter of Raghupathi Venkaiah to portray the character besides N.T.Ramarao. She changed her name as Devika and became a big star later years.

==Songs==
* Aa Manasemo Aa Sogasemo (Lyrics: Malladi Ramakrishna Sastry; Singer: Jikki Krishnaveni; Cast: Prameela)
* Aaye Sambarame (Lyrics: Malladi Ramakrishna Sastry; Singer: P. Leela; Cast: Anjali Devi)
* Ayyo Bangaru Saami (Lyrics: Malladi Ramakrishna Sastry; Cast: NTR and Anjali Devi)
* Bhale Bhala Paavurama (Lyrics: Malladi Ramakrishna Sastry; Singer: Ghantasala Venkateswara Rao; Cast: NTR and Anjali Devi)
* Ekkadidi Ee Andam Evvaridi Ee Andam (Lyrics: Malladi Ramakrishna Sastry; Singer: Jikki Krishnaveni; Cast: Prameela and others)
* Etu Choosina Bootakale Evaraadina Naatakale (Lyrics: Malladi Ramakrishna Sastry; Singer: P. Leela; Cast: Anjali Devi, Akkineni and NTR)
* Neesari Nevanamma Vayyari (Lyrics: Malladi Ramakrishna Sastry;  Cast: Prameela, Anjali Devi and Others)
* Ontarontariga Poyedana (Lyrics: Malladi Ramakrishna Sastry; Singer: Ghantasala Venkateswara Rao; Cast: NTR and Anjali Devi)

==Box-office==
The film was commercial hit and ran for more than 100 days in three centres in Andhra Pradesh. Centenary celebrations are organized at Vijayawada.

==References==
 

==External links==
*  

 
 
 