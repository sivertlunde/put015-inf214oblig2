The Possessed (1988 film)
 
{{Infobox film
| name           = The Possessed
| image          = Les possédés 1988.jpg
| caption        = French poster
| director       = Andrzej Wajda
| producer       = Margaret Ménégoz
| writer         = Jean-Claude Carrière, Fyodor Dostoyevsky
| starring       = Isabelle Huppert
| music          = Zygmunt Konieczny
| cinematography = Witold Adamek
| editing        = Halina Prugar-Ketling Gaumont
| released       =  
| runtime        = 116 minutes
| country        = France
| language       = French
| budget         = 
}}

The Possessed ( ) is a 1988 French drama film directed by Andrzej Wajda and starring Isabelle Huppert.    It was entered into the 38th Berlin International Film Festival.   

==Cast==
* Isabelle Huppert as Maria Sjatov
* Jutta Lampe as Maria Lebjadkin
* Philippine Leroy-Beaulieu as Lisa
* Bernard Blier as Le gouverneur
* Jean-Philippe Écoffey as Peter Verchovenskij
* Laurent Malet as Kirillov
* Jerzy Radziwiłowicz as Sjatov
* Omar Sharif as Stepan
* Lambert Wilson as Nikolaj Stavrogin
and
* Philippe Chambon as Chigalev
* Jean-Quentin Châtelain as Virguinski
* Rémi Martin as Erkel
* Serge Spira as Fedka
* Wladimir Yordanoff as Lebjadkin
* Zbigniew Zamachowski as Liamchine
with
* Piotr Machalica as Maurice
* Bozena Dykiel as Virginska
* Krzysztof Kumor as Aide de campe
* Witold Skaruch as Secrétaire du Couverneur
* Tadeusz Łomnicki as Captaine

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 