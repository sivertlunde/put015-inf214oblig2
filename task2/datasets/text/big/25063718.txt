Before I Self Destruct (film)

{{Infobox film
| name = Before I Self Destruct
| image = Before-I-Self-Destruct-2009.jpg
| caption = Curtis Jackson
| producer = Curtis Jackson  (also executive producer)  J. Jesses Smith Ken Kushner Frank Mosca  (associate producer) 
| writer = Curtis Jackson
| starring = Curtis Jackson Clifton Powell Elijah Williams Gabriel Ellis Sasha Delvalle
| music = Curtis Jackson Ky Miller Victor Bruno Stephen Tubin
| cinematography =
| editing = Stephen Franciosa Jr Ned Silhavy Jimmy Higgins  (co-editor)  Vilan Trub  (post-production supervisor) 
| distributor = Cheetah Vision
| studio = Kreative Film Empire
| released =  
| runtime = 79 Minutes
| country = United States
| language = English
| budget =
}} American crime Curtis "50 album of the same name. It was released on November 23, 2009 when the album was released 14 days earlier.

==Plot==
The film is about Clarence Jenkins (Curtis "50 Cent" Jackson), who lives with his mom and his gifted brother Shocka (Elijah "Strong-E" Williams). Clarence loves basketball and had dreamed on making the pros in the championship. However, due to breaking his knee in the end, his basketball career ended before it begins. Basically, he has no way to earn a living and ends up working in a supermarket where he longs after pretty girls that he feels are beyond his reach as a stock person, where he also checks out a woman named Princess (Sasha Delvalle). 

After his mother, Donna Jenkins (Kar) was accidentally shot and killed by a gunman named Tiny (Shorty Red) in a drive-by shooting when she was leaving a local bodega, Clarence has no idea what he can do to take care of his brother and prevent him from going into foster care when he got fired from his job, after getting caught for stealing a can of cookies that was caught on camera. He is stuck trying to find a way to take care of Shocka, who had already been accepted to all eight Ivy League universities.

In anger and frustration, he finds Tiny and kills him with a revolver that came from inside a shoe box, which was covered over a shirt, that was under Clarences bed at his hotel room, after coming back from the local bodega to find an eviction notice on the hotel door. Clarence visits Sean (Clifton Powell), who is the local crime boss, to inform him that he killed Tiny who had previously worked for Sean. Sean decides that he likes Clarences style and way he carries himself, so he hires Clarence to be his new hitman. 

After he has money, he runs into Princess, the same woman from the supermarket, who he originally felt was out of his reach. Now that he makes hundreds of thousands of dollars and can support his brother, life is easier and so is the money, once he gets past his original shock at being a hired gun. He falls in love with Princess and thinks she feels the same, but Princess is just a user and a fake bitch. Clarence bestows gold, diamonds and payback when Princesss ex-boyfriend Rafael (Gabriel Ellis) gets released from prison.

Rafael tracks her down by using her moms phone to call Princess and she answers the phone while in bed with Clarence pretending it is her mom. Rafael asks her where is the money he gave her for safekeeping and says he wants his "paper". She meets up with him and has sex and when he again asks her for his "paper" Princess stays silent. When Rafael gets angry and demands an answer, the only thing she says is "Clarence", intimating that she gave Clarence Rafaels money, which of course is not true. Clarence is keeping her like a queen and given her everything she wants. 

Rafael thinks that Clarence has taken his money from Princess so he has her call Clarence and ask him to come to her moms house to help move furniture. Princess and Rafael wait in his car across the street from her moms house. As soon as Clarence steps out of the car, Rafael starts shooting Clarence with a USP (Standard), leading to a slow, painful death. When Clarences brother Shocka realized what was going on, when Princess arrives and steals all of Clarences paper out of the drawer, Shaka tells Princess he will find her if its the last thing he does if she hurt his brother, kind of hinting that Shocka may also follow the wrong path to seek revenge for Clarence.

==Cast== Curtis "50 Cent" Jackson as Clarence Jenkins, Shockas elder brother and the main protagonist in the film
*Clifton Powell as Sean, Clarences local crime boss
*Elijah "Strong E" Williams as Shocka, Clarences 13-year-old brother
*Gabriel Ellis as Rafael, Princesss ex-boyfriend
*Sasha Delvalle as Princess
*Shorty Red as Tiny, a gunman who shot Clarences mother Angel as Mona Anthony Treach Criss as Cedrick Michael Wright as the first victim who got killed in a lift
*Lloyd Banks as Shockas school teacher Gabrielle Casseus as Freddie
* Kar as Donna Jenkins, Clarence and Shockas mother
* Jaquan K.R. Cobb as Bobby, Shockas best friend at school
*Taquan K.R. Cobb as Kevin
* Eve Lora as Mrs. Ortiz, Princesss mother

== Music ==
The music for the film is composed by G-Unit frontman 50 Cent and Ky Miller (who both also served as music supervisors; Miller also served as ADR recordist and record producer) along with Victor Bruno (who also served as an ADR recordist; he also did the audio mix and design, and the sound design with Tom Gambale) and Stephen Tubin. The first hit single "Ok, Youre Right" from 50 Cents fourth studio album of the same name was contributed to, and played in, Before I Self Destruct. The first single from the album was produced by Dr. Dre (who also served as 50 Cents executive producer for the album).

 , and "Pray for Me" by R&B singer Gasner Hughes.
 Prodigy contributed The Alchemist. Get Up" (produced by Scott Storch), which was replaced by the albums second single "Baby by Me" for the album of the same name, "Try Me" and "Cold Blooded" (both produced by Ky Miller).
==Sequel==
50 Cent revealed on Twitter that he is planning on making a sequel to Before I Self Destruct.

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 