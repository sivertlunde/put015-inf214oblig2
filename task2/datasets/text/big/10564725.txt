The Last Days of Pompeii (1913 film)
 
 
{{Infobox film name           = Ultimi giorni di Pompei, Gli image          = The Last Days of Pompeii (1913 film).jpg caption        = Poster to the U.S. theatrical release of The Last Days of Pompeii director       = {{plainlist|
* Mario Caserini
* Eleuterio Rodolfi
}} writer         = Mario Caserini based on       =   starring       = cinematography = distributor    = George Kleine Amusements released       =   runtime        = 56 minutes (VHS) 88 minutes (Kino DVD)  country        = Italy language  Silent
}}
  black and white silent film directed by Mario Caserini and Eleuterio Rodolfi.
 novel of the same name, the film is set during the final days leading up to the Mount Vesuvius eruption in Pompeii in 79 AD.

== Plot ==
In Pompeii 79AD, Glaucus and Jone are in love with each other. Arbace, Egyptian High Priest is determined to conquer her. Glaucus buys the blind slave Nidia who is mishandled by her owner.

Nidia falls in love with him and asks Arbace for his help. He gives her a potion to make Glaucus fall in love with him. In fact it is a poison which will destroy his mind. Arbaces disciple Apoecides threatens to reveal publicly his wrongdoings. Arbace kills him and accuses Glaucus of the crime. He locks Nidia in a cellar to prevent her from speaking.

Glaucus is condemned to be thrown to the lions. Nidia manages to escape and tells Glaucus friend Claudius what happened. Claudius rushes to the Circus to accuse Arbace and the crowd decides that Arbace and not Glaucus should be thrown to the lions.

The Vesuvius starts erupting and a widespread panic ensues. Under the shock, Glaucus recovers his mind. Blind Nidia, the only one to find her way in the darkness caused by the rain of ashes, leads Glaucus and Jone to safety and finds peace by drowning herself. 

== Cast ==
* Fernanda Negri Pouget as Nidia
* Eugenia Tettoni Fior as Jone
* Ubaldo Stefani as Glaucus
* Antonio Grisanti as Arbace
* Cesare Gani Carini as Apoecides
* Vitale Di Stefano as Claudius

==Production==
The film was produced by Ambrosio Film|Società Anonima Ambrosio

==Distribution==
The film was released in Italy on 24 August 1913, distributed by Giuseppe Barattolo. It was distributed in the US by the Kleine Optical Company under the name George Kleine Attractions.

==References==
 

== External links ==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 