The Jackal (1997 film)
{{Infobox film name            = The Jackal image           = Jackal_film.jpg caption         = Theatrical release poster director        = Michael Caton-Jones producer        = {{Plain list|
* James Jacks
* Sean Daniel
* Michael Caton-Jones
* Kevin Jarre
}} screenplay      = Chuck Pfarrer story           = Chuck Pfarrer based on        =   starring        = {{Plain list|
* Bruce Willis
* Richard Gere
* Sidney Poitier
* Diane Venora
}} music           = Carter Burwell cinematography  = Karl Walter Lindenlaub editing  Jim Clark studio          = Mutual Film Company Alphaville Films distributor  Universal Pictures released        =   runtime         = 124 minutes country      = United States United Kingdom Canada   France Germany Japan language        = English Russian budget          = $60 million    gross           = $159 million 
|}} 1997 American spy action action thriller The Day of the Jackal, involves the hunt for a paid assassin.

==Plot== American Federal FBI and MVD leads Azerbaijani Russian mafia (David The Jackal" (Bruce Willis) to kill an unseen target. The Jackal demands $70 million for the job, to which the mobster reluctantly agrees. Meanwhile, the MVD capture one of the mafias henchmen. During interrogation by torture, the henchman reveals the name "Jackal." This coupled with the documents recovered from the henchmans briefcase lead the FBI and MVD to assume the target for the retaliatory hit is FBI Director Donald Brown (John Cunningham).
 Russian Police Major Valentina Koslova (Diane Venora) turn to a former Irish Republican Army sniper named Declan Mulqueen (Richard Gere), who had a relationship with a ETA terrorist named Isabella Zancona (Mathilda May), who they believe can identify The Jackal. Mulqueen eventually agrees to help in exchange for their best efforts to get him released from prison.

It becomes apparent that Mulqueen has a personal motive for hunting the Jackal: the assassin wounded Zancona while she was pregnant with Mulqueens child, causing a miscarriage. Zancona provides information that can help identify the Jackal, including the fact that he is American and that he had acquired military training in El Salvador. Meanwhile, the Jackal arrives in Montreal to pick up the weapon he intends to use for the assassination, evading a group of hijackers in the process, and hires gunsmith Ian Lamont (Jack Black) to design and build a mount for it. Underestimating the danger and ruthlessness of the Jackal, Lamont demands more money in exchange for keeping quiet; the Jackal responds by brutally murdering Lamont with the very equipment Lamont built. The FBI discovers Lamonts body and, with the help of Mulqueen, deduce that the Jackal intends to utilise a long-range heavy machine gun for the assassination.

With the help of a Russian mole in the FBI, the Jackal realizes he is being tracked by Mulqueen with assistance from Zanconia, and he infiltrates Zanconas house after receiving a FBI access code from his insider. Instead of Zancona, however, he finds Koslova and Agents Witherspoon (J.K. Simmons) and McMurphy (Richard Lineback), promptly killing McMurphy and Witherspoon and mortally wounding Koslova. The Jackal gives Koslova a threatening message regarding Mulqueen—"He cant protect his women"—which she delivers to Mulqueen moments before her death.
 First Lady (Tess Harper), who is due to give a major public speech. The Jackal, now disguised as a police officer, plans to shoot the First Lady from his weapon mounted inside his minivan via remote control. Arriving just in time, Mulqueen successfully disables the Jackals weapon, while Preston saves the First Lady from a volley of gunfire. After a cat and mouse chase through the subway and subway tunnels the Jackal takes a young girl hostage, only releasing her when he has Mulqueen at his mercy. Unbeknownst to the Jackal, however, Mulqueen has summoned Zancona, who along with Mulqueen shoots the assassin dead.

A few days later, Preston and Mulqueen stand as the only witnesses to the Jackals burial in an unmarked grave. Preston reveals that he is going back to Russia to pursue the mobsters who hired the Jackal. It is revealed that Mulqueens request to be released was denied, but that he will likely be moved to a minimum security prison. Prestons heroics in saving the First Lady have made him a golden boy in the FBI: he can now "screw everything else up for the rest of his life and still be untouchable", for which he credits Mulqueen. After exchanging a farewell, and knowing his current clout will prevent any real backlash against him, Preston turns his back on Mulqueen, allowing him to go free.

==Cast==
*Bruce Willis as The Jackal
*Richard Gere as Declan Joseph Mulqueen FBI Deputy Deputy Director Carter Preston MVD
*Mathilda May as Isabella Zanconia
*J. K. Simmons as FBI Agent Timothy I. Witherspoon
*Richard Lineback as FBI Agent McMurphy John Cunningham FBI Director Donald Brown
*Jack Black as Ian Lamont First Lady Emily Cowan
*Leslie Phillips as Woolburton
*Stephen Spinella as Douglas
*Sophie Okonedo as Jamaican Girl
*David Hayman as Terek Murad
*Steve Bassett as George Decker Yuri Stepanov as Viktor Politovsky
*Ravil Isyanov as Ghazzi Murad
*Walt MacPherson as Dennehey
*Maggie Castle as 13 Year Old Girl
*Daniel Dae Kim as Akashi
*Michael Caton-Jones as Man in Video Peter Sullivan as Vasilov
*Richard Cubison as General Belinko
*Serge Houde as Beaufres
*Ewan Bailey as Prison Guard
*Jonathan Aris as Alexander Radzinski
*Edward Fine as Bill Smith
*Larry King as Himself

==Production== The Day Kenneth Ross."   Retrieved 2011-08-30 

Much of the film was shot on location in Richmond, Virginia, but some scenes were also shot on location in Montreal (and its subway stations), as well as Helsinki and Porvoo, Finland.

==Reception==

===Critical response===
The film received mostly negative reviews from critics. Roger Ebert of the Chicago Sun-Times called it a "glum, curiously flat thriller";  Ruthe Stein of the San Francisco Chronicle called it "more preposterous than thrilling";  and Russell Smith of the Austin Chronicle called it "1997s most tedious movie".  The Jackal currently holds a 15% rating on Rotten Tomatoes based on 27 reviews.

===Box office=== gross $159,330,280 worldwide. Against its $60m budget, the movie was a financial success.

==Soundtrack & Original motion picture score==
 
Original motion picture score "The Jackal" composed by Carter Burwell:
# Pier Chase
# The Drop Box - The Ferry - A Mole Revealed
# Highjackers And Race Through Garage
# Montreal Airport
# No Clown
# End Credits

==See also==
  The Jackal (The Day of the Jackal)

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 