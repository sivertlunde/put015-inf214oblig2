My Brother's Keeper (1948 film)
 
 
 
{{Infobox film
| name           = My Brothers Keeper
| image          = mbkposter1948.jpg
| image_size     =
| caption        = UK release poster
| director       = Alfred Roome
| producer       = Sydney Box Frank Harvey Jack Warner George Cole
| music          = Clifton Parker
| cinematography = Gordon Lang
| editing        = Esmond Seal
| studio         = Gainsborough Pictures
| released       = 9 August 1948
| runtime        = 96 min.
| country        = UK
| language       = English
}}
 1948 British Jack Warner George Cole and was produced by Sydney Box.

==Plot== Jack Warner) George Cole) are two newly convicted criminals being transported to prison.  Martin is a hardened, cynical career criminal, while Stannard is a naïve, rather dull-witted youth who has never previously been in trouble with the law, maintains his innocence of the rape for which he has been convicted and is terrified by the prospect of prison.  During the journey the pair manage to escape.  Martin steals an army officers uniform and passes Stannard off as a deserter in his charge, being returned to face a military tribunal.
 garage run by his mistress Nora Lawrence (Jane Hylton), who provides the pair with overnight shelter.  The following day Martin and Stannard take refuge in a derelict isolated cottage.  While trying to file their handcuffs apart they are surprised by a hunting man with a gun.  A struggle ensues, during which Martin shoots and kills the man.  Shortly thereafter they manage to separate the handcuffs and Martin abandons Stannard, going on the run alone while Stannard gives himself up and is promptly charged with murder.

Martin manages to contact his wife in London, asking if she can find a way to get money to him.  She arranges to travel by taxi to the woods in which he is hiding.  Just as she arrives, the police have tracked Martin down and have him cornered.  Rather than give himself up, Martin makes a final doomed attempt to escape through a signed minefield, watched by police, reporters, his wife and mistress and a crowd of sensation-seeking gawkers.

==Cast== Jack Warner as George Martin
* Jane Hylton as Nora Lawrence
* David Tomlinson as Ronnie Waring Bill Owen as Syd Evans George Cole as Willie Stannard
* Yvonne Owen as Meg Waring
* Raymond Lovell as Bill Wainwright
* Brenda Bruce as Winnie Foreman
* Susan Shaw as Beryl
* Beatrice Varley as Jenny Martin
* Garry Marsh as Brewster
* Maurice Denham as Superintendent Trent
* Frederick Piper as Gordon
* Wilfrid Hyde-White as Harding John Boxer as Police Sergeant Bert Foreman 
* Amy Veness as Mrs. Gully  Fred Groves as Crown Hotel landlord 
* Arthur Hambling as Edward Hodges 
* Valentine Dyall as Inspector at Milton Wells  George Merritt as Constable at Milton Wells 
* Jack Raine as Chief Constable Col. Heatherly Ben Williams as Policeman at Noras Garage
* Christopher Lee as Second Constable

==Reception==
My Brothers Keeper is a well-regarded film, with a reputation as a tight, tense and fast-moving thriller with Roomes previous editing experience being well utilised.  The characterisation of the two main protagonists is praised for going deeper than the stereotypes of the tough, reckless criminal and the dim, hapless innocent.  Via the 1950 film The Blue Lamp, and Dixon of Dock Green, the TV series developed from it which ran until the mid-1970s, Warner became forever engrained on the British consciousness as George "Evenin all" Dixon, the avuncular upholder of law and order.  My Brothers Keeper is often cited as an example of the dramatic range of which Warner was capable, before he became typecast.  Coles performance too is credited as one of the factors in his unusually smooth transition from child star to adult actor.  The films main weakness is cited as the interpolation of a pseudo-comic and largely irrelevant subplot involving a newspaper reporter trying to cover the story while on honeymoon in the area.

==Location filming==
My Brothers Keepers exterior location sequences were filmed in the Buckinghamshire/Oxfordshire border area, including scenes shot at the now abandoned Aston Rowant railway station.

==See also==
* The Defiant Ones

==References==
 
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 