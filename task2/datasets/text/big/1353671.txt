Chalte Chalte (2003 film)
 
 
{{Infobox film
| name           = Chalte 
| image          = chaltechalte.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Aziz Mirza
| producer       = Juhi Chawla Shah Rukh Khan Aziz Mirza
| story          = Aziz Mirza Robin Bhatt
| screenplay     = Aziz Mirza Robin Bhatt
| starring       = Shah Rukh Khan Rani Mukerji Satish Shah
| music          = Jatin-Lalit Aadesh Shrivastava
| cinematography = Ashok Mehta
| editing        = Amitabh Shukla
| distributor    = Dreamz Unlimited
| released       = 13 June 2003
| runtime        = 168 mins
| country        = India
| language       = Hindi
| budget         =     
| gross          =   
| preceded_by    = 
| followed_by    = 
}}
Chalte Chalte is a 2003 Bollywood movie starring Shah Rukh Khan and Rani Mukerji, directed by Aziz Mirza. The film was screened at the Casablanca Film Festival. Box Office India declared it hit, making it the first film by Dreamz Unlimited to be a box office success.

== Plot ==
The film begins in a bowling alley, where a group of friends are awaiting the arrival of Salim. Meanwhile, Deepak arrives with his fiancée Sheetal. When a conversation about love arises, Deepak and the rest find it necessary to make Sheetal aware that true love does exist. They tell her the story of Raj (Shah Rukh Khan) and Priya (Rani Mukerji), two of their closest friends. Raj Mathur is the owner of Raj Transport, a small trucking company he founded himself. Raj is a carefree man who is unorganised, messy, lazy and late all the time. Although he isnt the richest, hes always happy.

On the other hand, Priya Chopra is a successful fashion designer whose family is quite wealthy. Priya, originally from Greece, lives with her aunt Anna, who wants to give her the best in life (wealth and success). Priya has her life all planned out – the complete opposite of Raj. These two completely different people meet in a car accident, when Priya crashes her car into Rajs truck. Though the two get off to a rocky start, they meet again at the wedding of Salim and Farah, eventually becoming friends. The two arrange to meet again and slowly fall in love. Raj receives Priyas phone number but carelessly loses it. He then sets out to find her and he does find her.

However, Raj learns that Priyas engaged. Desperate, he follows Priya to Greece, where he continues to woo her. When its time to part company, Priya realises that she is in love with Raj. They get married after persuading Priyas parents, and arrive in Mumbai, where Raj welcomes Priya into their home. He then makes an oath to his holy talisman, wishing it to protect Priya if he is not around, because he loves her immensely.

Back to the bowling alley in the present: Sheetal is in tears, for she has never heard a more beautiful story. Salim arrives along with Farah and announce that today was Raj and Priyas first wedding anniversary, so they were planning a surprise party. Excited, Sheetal cant wait to meet the famous Raj and Priya, but when they do arrive, theyre nothing like what they seemed. A year has changed both Raj and Priya, and the two cant stop arguing. When they do find some common ground and happiness, it doesnt last long. Raj feels the pressure to meet the expectations of Priyas family, and is struggling financially. His company has gone bankrupt, and he has many bills and loans to pay. When he is insulted by a bank official, he returns home crying.

Priya cannot stand to see her husband in this state, and resorts to asking her former fiancé for money. When Raj asks her about the source of money, she lies. But when he throws a celebration party, he learns the truth. Angry at Priya, he becomes destructive, and Priya, heartbroken, runs away to her parents. Raj, realizing his error, goes after Priya, only to be insulted by her family. Priya decides to leave India because she cant stay with Raj but cant stay without him. When Raj is informed of this, he races to the airport to stop Priya, just as he did the year before. He says he will change and that he has a dream of the two of them starting a family together. Although Priya expresses that she has the same dream, she feels it is impossible for them to be together, so she boards the plane. Giving up, Raj remembers his oath and sends his talisman to Priya. When Priya receives the talisman, she too recalls Rajs confession. Raj returns home to find Priya waiting for him. She says that the only way to make their dream come true is to be together. In a playful manner, the two get into another argument but they say that this is how they express their love. They continue to argue as the credits roll.

==Cast==

* Shah Rukh Khan as Raj Mathur
* Rani Mukerji as Priya Mathur (née Chopra)
* Jas Arora as Sameer
* Satish Shah as Manubhai
* Lilette Dubey as Anna Mausi (Priyas aunt)
* Johny Lever as Nandu (the roadside drunk)
* Vishwajeet Pradhan as a Friend
* Suresh Bhagwat as a Dhobi
* Dinyar Tirandaz as Irani
* Rajeev Verma as Mr. Chopra
* Jayshree T. as Mrs. Manubhai
* Suresh Menon as a Shopkeeper
* Masood Akhtar as a Paanwala
* Gagan Gupta as Tambi
* Vani Tripathi as a Friend
* Sushmita Daan as a Flower girl
* Arun Singh as a Vegetable seller
* Akhtar Nawaz as a Milkman
* Kamini Khanna as Female plane passenger
* Bobby Darling as a Friend
* Madhavi Chopra as a Friend
* Ashish Kapoor
* Aditya Pancholi as a Hostile businessman
* Jameel Khan as a Traffic Policeman

== Development ==
This was the third film produced by Shahrukh Khan and Juhi Chawlas company, Dreamz Unlimited.

According to Khan, Rani Mukherjee was asked to play the heroine but she was committed to other films and had to turn it down. Aishwarya Rai was signed for the role, but had to leave the set after some problems with her then boyfriend, star Salman Khan who is said to have disrupted filming. This caused considerable delay in the progress of the film until Mukherjee agreed to take over the role and save the film. 

Rani Mukherjee had a complete makeover for the role. Her new look was also widely noted. 

== Music ==
{{Infobox album |  
| Name           = Chalte Chalte (2003 film)  
| Cover          = 
| Caption        = 
|Type=soundtrack
| Artist         = Jatin Lalit & Aadesh Shrivastava 
| Released       = 2003  (India) 
| Recorded       =  Feature film soundtrack 
| Length         = 44:28
| Label          = T-Series
| Producer       = 
| Certification  = 
| Chronology     =  Jatin Lalit
| Last album     = "Waah! Tera Kya Kehna" (2002)
| This album     = "Chalte Chalte" (2003)
| Next album     = "Haasil" (2003)
| Misc           = {{Extra chronology
| Artist         = Aadesh Shrivastava 
| Type           = albums
| Last album     = "Kash Aap Hamare Hote" (2003)
| This album     = "Chalte Chalte" (2003)
| Next album     = "Baghban (film)|Baghban"  (2003)
}}}}
{{Album ratings
| rev1 = Bollywood Hungama
| rev1score =     
}}

The music of the film has been conducted by the duo Jatin-Lalit and Aadesh Shrivastava.

Joginder Tuteja of Bollywood Hungama gave the album 3 stars and stated "Chalte Chalte is a romantic album that should go well with both class as well as mass". 

{| class="wikitable"
|- style="background:#d1e4fd;"
! # !! Song !! Artist !! Lyrics !! Composer!! Length
|-
| 1 || "Chalte Chalte" || Alka Yagnik & Abhijeet Bhattacharya || Javed Akhtar || Jatin-Lalit || 06:33
|-
| 2 || "Tauba Tumhare Yeh Ishaare" || Alka Yagnik & Abhijeet Bhattacharya || Javed Akhtar || Jatin-Lalit || 05:15
|-  Babbu Maan || Aadesh Shrivastava || 05:44
|-
| 4 || "Gum Shuda" || Sonu Nigam || Javed Akhtar || Aadesh Shrivastava || 05:00
|-
| 5 || "Dagariya Chalo" || Alka Yagnik & Udit Narayan || Javed Akhtar || Jatin-Lalit || 06:16
|-
| 6 || "Chalte Chalte – Instrumental" || || || Jatin-Lalit || 04:57
|-
| 7 || "Tujhpar Gagan Se"|| Sukhwinder Singh, Preeti & Pinky || Javed Akhtar || Aadesh Shrivastava || 05:23
|-
| 8 || "Suno Na Suno Na" || Abhijeet Bhattacharya || Javed Akhtar || Aadesh Shrivastava || 05:20
|-
| 9 || "Music Piece" || Chorus || || Jatin-Lalit || 01:46
|-
| 10 || "Music Piece With Flute" || || || Jatin-Lalit || 01:46
|}

== Reception ==

===Box office===
Chalte Chalte grossed   in India. http://www.boxofficeindia.com/showProd.php?itemCat=209&catName=MjAwMw==  The film released with around 220 prints in India and 170 abroad and  had a good opening at the box office of nearly 60% occupancy in its first 3 days, and went on to gross 140&nbsp;million net at the end of its theatrical run in India and was declared "Above Average" by Box Office India. The film was in the top ten highest grossed domestically in the year of its release.  Chalte Chalte entered the top 10 in the United Kingdom box office charts and registered the third biggest opening ever for a Bollywood film.  The film was declared a hit in the overseas after netting over 120&nbsp;million. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 