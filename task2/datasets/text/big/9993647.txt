Ongka's Big Moka
 
 
 
{{Infobox television film
| image 
| image_size = 
| caption = 
| genre = Documentary
| distributor = 
| director = Charlie Nairn
| producer = Charlie Nairn
| writer = 
| starring = Ongka
| music = 
| cinematography = Ernest Vincze
| editing = 
| studio = 
| budget = 
| country = England
| language = English
| network = 
| released = 1976
| runtime = 60 min.
| website = 
}}
Ongkas Big Moka: The Kawelka of Papua New Guinea is a 1976 documentary film, part of Granada Televisions Disappearing World Series which ran from 1969-1993. 

==Overview== Big man, is seen preparing a Moka exchange. The film follows Ongkas struggles to accumulate huge numbers of pigs and other items of value to present at a Moka ceremony to another tribe. The objective of Ongkas Moka is to build status, prestige and fame for his tribe and for himself. Ongka uses Moka to elevate his status as the "Big Man" of his tribe.  The film shows clearly that there are strong elements of competition and one-upmanship in giving Moka, setting the ceremonys date, and also - despite the fact that they never filmed it - in the actual Moka ceremony itself in which the participants wear traditional and magnificent Highland "decoration". Giving Moka can bring the giver close to ruin but the political and social gains from increased prestige can outweigh this. 

Traditionally, Moka was an exchange of pigs.  The "Big Man" hires a speaker to go down the line of pigs, stopping before each one to sing its praises and describe its lineage.  This scene is one of the highlights of the film along with extraordinary footage of a neighboring "Big Mans" funeral and his tribes reaction to his death and its probable causes.  The film also shows that expensive modern consumer items have also become part of the Moka exchange.

This film has become a staple in college anthropology programs due to its excellent illustration of gift based economies as well as an example of the role of Big Men.

The Moka was delayed, and the filmmakers left before it occurred. When the Moka finally did occur, it contained 600 pigs, $10,000 Australian dollars, 12 cassowaries, 8 cows, a motorbike and a pick-up truck. In his speech at the Moka, Ongka says, "Now that I have given you these things, I have won. I have knocked you down by giving so much."

==References==
 
 

==External links==
* 

 
 
 
 
 
 
 


 