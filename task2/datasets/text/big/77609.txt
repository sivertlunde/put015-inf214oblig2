Sergeant York (film)
{{Infobox film
| name           = Sergeant York
| image          = Sergeant York 1941 Poster.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = Howard Hawks Howard Koch Tom Skeyhill Sam Cowan
| starring       = Gary Cooper Walter Brennan
| producer       = Howard Hawks Jesse L. Lasky Hal B. Wallis William Holmes
| distributor    = Warner Brothers
| cinematography = Sol Polito
| music          = Max Steiner
| released       =  
| runtime        = 134 minutes
| country        = United States
| language       = English
| budget         = $1.4 million   Box Office Mojo. Retrieved February 1, 2013. 
| gross          = $16,361,885 
}}
 Alvin York, one of the most-decorated American soldiers of World War I. It was directed by Howard Hawks and was the highest-grossing film of the year. FILM MONEY-MAKERS SELECTED BY VARIETY:  Sergeant York Top Picture, Gary Cooper Leading Star
New York Times (1923-Current file)   31 Dec 1941: 21. 
 Howard Koch, and Sam Cowan (uncredited).  York refused, several times, to authorize a film version of his life story, but finally yielded to persistent efforts in order to finance the creation of an interdenominational Bible school.  The story that York insisted on Gary Cooper for the title role derives from the fact that producer Jesse L. Lasky recruited Cooper by writing a plea that he accept the role and then signed Yorks name to the telegram. 
 Best Film its 100 50 heroes in American cinema.

In 2008, Sergeant York was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot==
Alvin York (Gary Cooper), a poor young Tennessee hillbilly, is an exceptional marksman, but a neer-do-well prone to drinking and fighting, which does not make things any easier for his patient mother (Margaret Wycherly). He changes when he meets Gracie Williams (Joan Leslie), and works hard to become a good provider for her.

After he is struck by lightning during a late-night rainstorm he undergoes a religious awakening. York vows never to get angry at anyone ever again.
 conscientious objector Camp Gordon for basic training. His superiors discover that he is a phenomenal marksman and decide to promote him to Corporal#United States|corporal.
 Buxton (Stanley Ridges), his sympathetic commanding officer, tries to change Yorks mind, citing sacrifices made by others all throughout the history of the United States. He gives York a leave to go home and think it over. He promises York a recommendation for his exemption as a conscientious objector if York remains unconvinced. While York is fasting and pondering, the wind blows his Bible open to the verse "Render therefore unto Caesar the things which are Caesars; and unto God the things that are Gods." York reports back for duty and tells his superiors that he can serve his country, despite not having everything figured out to his satisfaction, leaving the matter in Gods hands.
 Charles Esmond) at gunpoint to order the Germans still fighting in another section of the line to also surrender. He and the handful of other survivors end up with 132 prisoners. York becomes a national hero and is awarded the Medal of Honor. When Major Buxton asks him why he did what he did, York explains that he was trying to save the lives of his men.

Arriving in New York City, York receives a ticker tape parade and a key to the city.  He is impressed with the Waldorf-Astoria hotel and its indoor electricity.  Congressman Cordell Hull guides him through the city and informs him that he has been offered opportunities to commercialize on his fame.  York rejects the offers saying that he was not proud of what he did in the war, but it had to be done. He tells Hull he wants to go home. He returns to Tennessee. The people of his home state have purchased for him the bottomland farm he wanted and paid for a house to be built on the land.

==Cast==
* Gary Cooper as Alvin York
* Walter Brennan as Pastor Rosier Pile
* Joan Leslie as Gracie Williams
* George Tobias as "Pusher" Ross, a soldier from New York City; Alvins friend
* Stanley Ridges as Major Buxton
* Margaret Wycherly as Mother York
* Ward Bond as Ike Botkin
* Noah Beery Jr. as Buck Lipscomb
* June Lockhart as Rosie York, Alvins sister Dickie Moore as George York, Alvins younger brother
* Clem Bevans as Zeke
* Howard Da Silva as Lem
* Charles Trowbridge as Cordell Hull
* Harvey Stephens as Captain Danforth David Bruce as Bert Thomas
* Carl Esmond as German Major (as Charles Esmond)
* Joe Sawyer as Sergeant Early Pat Flaherty as Sergeant Harry Parsons
* Robert Porterfield as Zeb Andrews
* Erville Alderson as Nate Tomkins

==Reception==
Sergeant York was a spectacular success at the box office and became the 1941 in film|highest-grossing film of 1941. It remains one of the highest-grossing films of all time when adjusted for inflation.  It benefited from the attack on Pearl Harbor, which occurred while the film played in theaters. The films patriotic theme helped recruit soldiers; young men sometimes went directly from the movie theater to military enlistment offices.    

At the 14th Academy Awards, the film won two Oscars:    Best Actor - Gary Cooper Best Film William Holmes

It was also nominated for:    Outstanding Motion Picture - Warner Bros. (Hal B. Wallis and Jesse L. Lasky Producers) Best Director - Howard Hawks Best Writing Howard Koch Best Supporting Actor - Walter Brennan Best Supporting Actress - Margaret Wycherly Best Art John Hughes, Fred M. MacLean Best Cinematography (Black-and-White) - Sol Polito Best Music (Score of a Dramatic Picture) - Max Steiner Best Sound Recording - Nathan Levinson

==References==
 

==Further reading==
* Michael E. Birdwell, Celluloid Soldiers: The Warner Bros. Campaign against Nazism (NY: New York University Press, 1999)
* McCarthy, Todd, Howard Hawks: The Grey Fox of Hollywood (NY: Grove Press, 1997), ch. 22: "Sergeant York"
* Robert Brent Toplin, History by Hollywood: The Use and Abuse of the American Past (Chicago: University of Illinois Press, 1996)

==External links==
 
*  
*  
*  
*  
*  
*  , by Sam Cowan, 1922, from Project Gutenberg
*  , by Douglas Mastriano, Military History magazine, Sept 2006. (Corporal Yorks actions as seen from the German perspective.)
*  , Discovered 21 October 2006 by the Sergeant York Discovery Expedition.
* {{cite web| url=http://www.iht.com/articles/2006/10/26/news/york.php/ |publisher=International Herald Tribune | 
archiveurl= http://web.archive.org/web/20090221001052/http://www.iht.com/articles/2006/10/26/news/york.php/ | archivedate= 2009-02-21 | title= Proof offered of Sergeant Yorks war exploits| date= October 26, 2006| author= Craig S. Smith| accessdate=2011-11-22}}

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 