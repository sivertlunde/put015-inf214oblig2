Parinay
{{Infobox film
| name           = Parinay
| image          = Parinay.jpg
| image_size     = 250px
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Kantilal Rathod
| producer       = Samantar Chitra
| writer         =  
| screenplay     =  
| story          = 
| based on       =  
| narrator       =  Romesh Sharma|
Shabana Azmi
Asha Sachdev
Dinesh Thakur
Priti Ganguly
T.P. Jain}}
| music          = Jaidev
| cinematography = K.K. Mahajan
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Parinay (literally Espousal) is a 1974 Hindi film directed by Kantilal Rathod and produced by Samantar Chitra. The film starred Romesh Sharma and Shabana Azmi as the leading male and female players respectively, whereas Asha Sachdev, Dinesh Thakur, Priti Ganguly and T.P. Jain were in secondary roles.    Released on 1 January 1974, this film was associated with Saregama (known as "The Gramophone Company of India Limited" at that time).  The screenplay was by Rathod and Shukla, whereas the cinematography was by K.K. Mahajan.  Jaidev was the music director; Anup Jalota, Manna Dey and Vani Jairam were the singers.  The lyricists were Ramanand Sagar and Naqsh Lyallpuri. 

Written by the trio of Vinay Shukla, Rathod and Harin Mehta, Parinay won the 1974 Nargis Dutt Award for Best Feature Film on National Integration (known as the "Rajat Kamal Special Award for the Best Feature Film on National Integration" at that time) presented by the Directorate of Film Festivals (DFF).    The DFF also gave Samantar Chitra Private Limited a Rajat Kamal (Silver Lotus),  , and a certificate; Rathod got a Rajat Kamal,  , and a certificate.  Azmi and Sharma got a medallion each by the DFF. 

==See also==
*List of Bollywood films of 1974

==References==
 

==External links==
* 

 

 
 
 

 