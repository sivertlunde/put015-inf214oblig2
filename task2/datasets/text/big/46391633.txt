Maa (1992 Bengali film)
 
 
{{Infobox film
| name           = Maa
| image          =
| caption        =
| director       = Prashanta Nanda
| producer       = Prashanta Nanda
| story          = Basant Nayak
| screenplay     = Prashanta Nanda
| writer         =
| choreographey  = Shiv Shankar
| dialogue       = Bijaya Mishra
| starring       = Prosenjit Chatterjee Satabdi Roy Abhishek Chatterjee Rakhee Gulzar Biplab Chatterjee Uttam Mohanty Aparajita Mohanty Bijoy Mohanty Ritu Das Hemanta Das 
| studio         = Aradhana Films
| editing        = Prashant Nanda
| cinematography = V. Prabhakar
| released       =  
| runtime        =
| country        = India Bengali
| music          = R. D. Burman
| lyrics         = Swapan Chakraborty
| awards         =
| budget         =
}}
 Bengali film directed & produced by Prashanta Nanda.The film music composed by R. D. Burman.The Film starring Prosenjit Chatterjee & Satabdi Roy in lead roles.

==Cast==
* Prosenjit Chatterjee
* Satabdi Roy
* Rakhee Gulzar
* Abhishek Chatterjee
* Biplab Chatterjee
* Hemanta Das
* Ritu Das
* Aparajita Mohanty
* Uttam Mohanty
* Ajit Das
* Hara Patnaik
* Premanjana
* Arabinda Sadhangi
* Tushar Panigrahi
* Deepa Sahu
* Namrta Das
* Raimohan Parida
* Braja Singh
* Debu Bramha
* Suresh Bal
* Shankar Dutt
* Munu
* Rubi Parida 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Duration !! Music  !! Lyricist
|-
| 1
| Cholo Ghora Dhuke 

| Asha Bhosle, Abhijeet Bhattacharya
| 4:27
| R. D. Burman
| Swapan Chakraborty 
|-
| 2
| Jar Porashe Jibon Juray
| Kavita Krishnamurthy
| 4:36
| R. D. Burman
| Swapan Chakraborty
|-
| 3
| Sikha O Sikha
| Amit Kumar
| 5:59
| R. D. Burman
| Swapan Chakraborty
|-
| 4
| Sobi Dharme-1
| Babla Mehta
| 2:07
| R. D. Burman
| Swapan Chakraborty
|-
| 5
| Sobi Dharme-2
| Babla Mehta, Alka Yagnik
| 6:05
| R. D. Burman
| Swapan Chakraborty
|-
| 6
| Taka Taka Joy Taka
| Amit Kumar, Jolly Mukherjee
| 6:12
| R. D. Burman
| Swapan Chakraborty
|-
| 7
| Sara Sahar Jane
| Asha Bhosle
| 4:38
| R. D. Burman
| Swapan Chakraborty 
|-
|}

==References==
 

==External links==
 
*   at the Gomolo
*  

 
 
 


 