Three to Tango
 

{{Infobox film
| name = Three to Tango 
| image = Threetotango01.jpg 
| caption = 
| alt = 
| director = Damon Santostefano 
| writer = Rodney Patrick Vaccaro  Bobby Newmyer Susan E. Novick  Keri Selig  Jeffrey Silver  Bettina Sofia Viviano
| starring = Matthew Perry  Neve Campbell  Dylan McDermott  Oliver Platt  Cylk Cozart 
| editing = Stephen Semel 
| studio = Village Roadshow Pictures 
| distributor = Warner Bros. Roadshow Entertainment    
| released =  
| runtime = 98 minutes 
| language = English
| budget = $20,000,000
| gross    = $10,570,375 (Domestic) {{cite web
| title = Three to Tango at Box Office Mojo
| url = http://www.boxofficemojo.com/movies/?id=threetotango.htm
| accessdate = 2010-09-29
}}
 
}}

Three to Tango is a 1999 romantic comedy film starring Matthew Perry, Neve Campbell, Dylan McDermott and Oliver Platt.

== Plot ==

Set amidst Chicagos swing music revival of the late 1990s, Oscar Novak (Perry), an aspiring architect and his business partner, Peter Steinberg (Platt), have just landed a career-making opportunity with a Chicago tycoon Charles Newman (McDermott) who has chosen them to compete for the design of a multimillion dollar cultural center. In a ploy for publicity, Newman has pitched Oscar and Peter in a neck-and-neck competition with their archrivals and former colleagues, the hugely successful (and equally ruthless) Decker and Strauss. In a comic twist Oscar is mistaken for a gay man when meeting with Charles Newman (made even more humorous by the fact that Peter is genuinely gay, with Oscars comments leading Newman to think that Peters the straight one). Under the mistaken impression that Oscar is homosexual, and therefore a safe companion for his girlfriend Amy (Campbell), he asks Oscar to keep an eye on her for him and make sure that she doesnt talk to his wife. Oscar falls for Amy virtually on sight, but she thinks hes gay. He is forced to maintain the charade to avoid getting into trouble with Newman, and losing the commission.

Matters become complicated when a news article about Oscar and Peters homosexual status is published in the Business paper, leaving Oscar in the increasingly frustrating position of having to fend off advances from various gay men while convincing his friends and family that he is simply pretending to be gay; Amy even sets him up on a date with her ex-boyfriend, football player Kevin Cartwright, but Oscar manages to defuse the situation by saying that hes in love with someone else. Despite the embarrassing misconceptions, Oscar forms a close bond with Amy as they continue to spend time together-to the extent that Amy moves in with him after she is kicked out of her apartment-Amy sharing various personal stories with Oscar. At the final presentation for the cultural center, Oscar and Peter receive the commission, but Oscar is simultaneously told that he has won the award for Gay Professional Man of the Year, with Newman deciding that he will reveal his decision after the ceremony.

After an awkward meeting between Amy and Newmans wife at the party, she and Oscar go to a bar, but Amy leaves in frustration after she nearly kisses him, prompting a brief argument between her and Oscar where Oscar states that her relationship with Newman has no future, with the only reason they havent argued after over a year being that Newman doesnt care enough to fight with her, while Amy counters that Oscar is hardly in a position to give her advice on romance, having simply been playing it safe by spending time with her as he hasnt been on a date since she met him. After spending the day alone, Oscar attends the award ceremony for Gay Professional Man of the Year. Although he initially continues his charade, while looking out at the people before him, he instead makes a passionate speech about how he admires all the men and women here who were able to tell the truth to their families about how they feel, ending the speech by "coming out of the closet" as he admits that hes straight and in love with Amy; even if he simultaneously destroys any hope of being with her by doing so, he felt that everyone in the room deserves the same kind of honesty that they have given their own families. As he is applauded for having the courage to admit the truth, he runs after Amy, only for her to punch him and Newman before walking out of the theatre in a rage, leaving Peter to accept a date with Kevin. However, as Oscar sits in a restaurant where he and Amy ate together on the night they met, Amy comes to see him, admitting that she loves him too, followed by their first kiss.

In a post-credit sequence, Newmans wife convinces him to go with Oscar and Peters design despite his own claims to go with the other firm, revealing that she knew about him and Amy and informing him bluntly that Oscar and Peter did the better job.

== Cast and characters   ==
* Matthew Perry as Oscar Novak
* Neve Campbell as Amy Post
* Dylan McDermott as Charles Newman
* Oliver Platt as Peter Steinberg
* Cylk Cozart as Kevin Cartwright

=== Other characters ===
* John C. McGinley as Strauss
* Bob Balaban as Decker
* Deborah Rush as Lenore
* Kelly Rowan as Olivia Newman
* Rick Gomez as Rick
* Patrick Van Horn as Zack David Ramsey as Bill
* Barbara Gordon as Jenny Novak
* Roger Dunn as Edward Novak

== Reception ==
Rotten Tomatoes gives the film a rating of 29% based on 63 reviews.  .   

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 