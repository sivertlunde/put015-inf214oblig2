Buddhanum Chaplinum Chirikkunnu
{{Infobox film
| name           = Buddhanum Chaplinum Chirikkunnu
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = Buddhanum Chaplinum Chirikkunnu
| director       = R. Sarath
| producer       = Jyothikrishnan  Dr.Sumithran Lakshmipriya
| writer         = R. Sarath
| screenplay     = R. Sarath
| story          = R. Sarath
| based on       = 
| narrator       = 
| starring       = Nedumudi Venu Indrans Jagadheesh Nandhu Praveena Malavika Menon
| music          = Isaac Thomas Kottukappalli
| cinematography = Sajan Kalathil
| editing        = 
| studio         = 
| distributor    = Epic Cinema
| release date       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Buddhanum Chaplinum Chirikkunnu (  directed by R. Sarath. The film stars Indrans in the lead role and co-stars Jagadheesh, Nedumudi Venu, Nandhu, P. Balachandran, Praveena, Sharvari Jamenis, and Malavika Menon in supporting roles.    

==Plot==
It is a hundred years since Charlie Chaplin, artist nonpareil, appeared on the silver screen mesmerising audiences across the globe. His signature moustache and derby that have left their indelible imprint on the annals of film history. Buddhanum Chaplinum Chirikkunnu is a film that pays homage to Chaplin at the centennial commemoration of his acting career. The film portrays an Indian comedian, Indragupthan, who idolises Chaplin. He believes his life resembles Chaplins and nurtures the desire to depict his hero on the big screen. However, he rules out a blind imitation of the late legend. The narrative revolves around his disintegrating family life and his quest for true love. The dilemma that ensues is the crux of the story. In the struggle to discover himself Indragupthan is entrapped in a world of make belief. The film is a unique marriage of the serious and the comic - one complementing the other. Indragupthan realises finally that the wheel of life has left him stranded in the twilight zone between real and unreal. 

==Cast==
* Indrans as Indraguptan
* Jagadheesh
* Nedumudi Venu
* Nandhu
* P. Balachandran
* Munshi Byju
* Krishnaprasad
* K C Baby
* Dr C Unnikrishnan
* Fr George Rebeiro
* Ananthan Sumithran
* Praveena as wife of Indraguptan
* Sharvari
* Lakshmi Menon
* Sona Nair
* Malavika Menon
* Amrita Anil
* Jayasree
* Gayathri
* Abhay
* Paramesh
* Nanda

==Production==
It was in news that actresses Asha Sarath and Lakshmi Gopalaswamy has denied to play the female lead role in the film opposite Indrans. Later the director R. Sarath himself clarified that it was just gossip.   Later Praveena was confirmed to play the role.   The shooting of the film is progressing at Sasthamkotta and Kollam. 

==References==
 
   
==External links==
*  
*  

 
 
 
 