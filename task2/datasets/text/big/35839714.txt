Rhythm of the Saddle
{{Infobox film
| name           = Rhythm of the Saddle
| image          = Rhythm_of_the_Saddle_Poster.jpg
| border         = Yes
| caption        = Theatrical release poster
| director       = George Sherman
| producer       = Harry Grey
| writer         = Paul Franklin
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* Pert Kelton
}}
| music          = Raoul Kraushaar (musical director)
| cinematography = Jack A. Marta
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = English
}} Western film directed by George Sherman and starring Gene Autry, Smiley Burnette, and Pert Kelton. Written by Paul Franklin, the film is about the foreman at a ranch owned by a wealthy rodeo owner who will lose her rodeo contract unless sales improve.   

==Plot==
The owner of the Silver Shadow ranch, Maureen McClune (Peggy Moran), runs the Frontier Week rodeo every year, relying on the financial success of the event to support the ranch. The current rodeo is the most profitable in the events history, but Maureen is told by the rodeo organizers that she must do even better if she hopes to get her contract renewed. Maureens main competition is Jack Pomeroy (LeRoy Mason), who owns a rival ranch and a local nightclub and gambling house.

Following a series of "accidents" apparently caused by negligence during the rodeo, Maureens foreman, Gene Autry (Gene Autry), sets out to prove that Pomeroy is responsible. Maureens Aunt Hattie (Pert Kelton) wins some money at a roulette table at Pomeroys club, thanks to Genes disabling of the rigged mechanisms. Returning home, they are ambushed by Pomeroys men. Later, Gene breaks into Pomeroys office to get additional proof of his guilt.

On the last day of the Frontier Week rodeo, Gene rides against one of Pomeroys men in the final event, a stagecoach race. Aunt Hattie bets everything she has on Gene, hoping to save the ranch. When Gene discovers his friend, Frog Milhouse (Smiley Burnette), making a recording of a proposal to Hattie, he realizes that Frogs recorder could entrap Pomeroy. He instructs Frog to place the device below Pomeroys seats at the rodeo just before the start of the race.

Pomeroy persuades the sheriff that Gene has committed a murder, but Gene is able to escape. With Frogs help, Gene is able to make it to the race on time. While Gene rides furiously, nearly losing his life, Frog records Pomeroy and his men discussing the "accidents" they created during the rodeo. Gene ends up winning the race, Hattie wins her bet, and Pomeroy and his henchmen are arrested. With their financial worries behind them, Gene and Maureen are free to marry, as are Frog and Hattie.

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Milhouse
* Pert Kelton as Aunt Hattie
* Peggy Moran as Maurine McClune
* LeRoy Mason as Jack Pomeroy
* Arthur Loft as Cylde Chase
* Ethan Laidlaw as Tex Robinson
* Walter De Palma as Leach, the tall henchman
* Arch Hall Sr. as Rusty, the henchman in the stage
* Eddie Hart as Alex, the henchman
* Eddie Acuff as Dixie Erwin
* Champion as Genes Horse (uncredited)   

==Production==

===Stuntwork===
* Bill Yrigoyen
* Joe Yrigoyen 

===Filming locations===
* Corriganville Movie Ranch, Simi Valley, California, USA 
* Iverson Ranch, 1 Iverson Lane, Chatsworth, Los Angeles, California, USA   

===Soundtrack===
* "Merry Go Roundup" (Gene Autry, Johnny Marvin, Fred Rose) by Gene Autry (vocal and guitar)
* "Shell Be Comin Round the Mountain" (Traditional) by Smiley Burnette (a cappella vocal) 
* "Oh! Ladies" (Gene Autry, Johnny Marvin, Fred Rose) by Gene Autry (vocal) and Smiley Burnette (vocal and guitar)
* "When Mother Nature Sings Her Lullaby" (Larry Yoell, Glenn Brown) by Gene Autry (vocal and guitar) and the all-girl nightclub orchestra
* "The Old Trail" (Gene Autry, Johnny Marvin, Fred Rose) by Gene Autry and Smiley Burnette with the cowhand musicians
* "Let Me Call You Sweetheart" (Leo Friedman, Beth Slater Whitson) by Gene Autry and Smiley Burnette with an orchestral background   

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 