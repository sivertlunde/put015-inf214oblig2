Kingdom of Heaven (film)
 
{{Infobox film
| name           = Kingdom of Heaven
| image          = KoHposter.jpg
| image_size     = 220px
| alt            = 
| caption        = Theatrical release poster
| director       = Ridley Scott
| producer       = Ridley Scott
| writer         = William Monahan
| starring       = {{Plain list | 
* Orlando Bloom
* Ghassan Massoud
* Eva Green
* Jeremy Irons
* David Thewlis
* Brendan Gleeson
* Marton Csokas
* Liam Neeson
* Edward Norton
}}
| music          = Harry Gregson-Williams John Mathieson
| editing        = Dody Dorn
| studio         = {{Plain list |
* Scott Free Productions
* Inside Track Studio Babelsberg Motion Pictures GmbH 
}}
| distributor    = 20th Century Fox
| released       =   
| runtime        = 144 minutes   194 minutes  
| country        = {{Plain list |
* United Kingdom 
* Germany
* United States
}}
| language       = {{Plain list | 
* English Arabic
* Italian
* Latin
}}
| budget         = $130 million 
| gross          = $211,652,051 http://www.boxofficemojo.com/movies/?id=kingdomofheaven.htm 
}}
 epic Historical historical drama directed and produced by Ridley Scott and written by William Monahan. It stars Orlando Bloom, Eva Green, Jeremy Irons, David Thewlis, Brendan Gleeson, Marton Csokas, Liam Neeson, Edward Norton, Ghassan Massoud, and  Alexander Siddig.
 French village Ayyubid Muslim sultan Saladin, who is battling to reclaim the city from the Christians leading to the Battle of Hattin. The film script is a heavily fictionalized portrayal of the life of Balian of Ibelin (ca. 1143–93).
 Black Hawk Down. Filming also took place in Spain, at the Loarre Castle (Huesca), Segovia, Ávila, Spain|Ávila, Palma del Río and Casa de Pilatos in Sevilla. 

==Plot==
  Balian (Orlando Bloom), a blacksmith, is haunted by his wifes recent suicide. A group of Crusaders arrives in his village; one of them introduces himself as Balians father, Baron Godfrey of Ibelin (Liam Neeson). Godfrey asks Balian to return with him to the Holy Land, but Balian declines and the Crusaders leave. The town priest, Balians half-brother (Michael Sheen), reveals that he ordered Balians wife beheaded before burial. In a fit of rage, Balian kills his brother and flees the village.
 weakening him.

In Messina, Godfrey knights Balian and orders him to serve the King of Jerusalem and protect the helpless, then succumbs to his injuries. During Balians journey to Jerusalem his ship runs aground in a storm, leaving Balian the only survivor. Balian is confronted by a Muslim cavalier, who attacks him over his horse. Balian reluctantly slays the cavalier but spares the mans apparent servant (Alexander Siddig), asking him to guide him to Jerusalem. Upon arriving, Balian releases him, and the man tells Balian that his deed will gain him fame and respect among the Saracens.
 leper King Tiberias (Jeremy Princess Sibylla (Eva Green); and her husband Guy de Lusignan (Marton Csokas), who supports the anti-Muslim activities of brutal factions like the Knights Templar. After Baldwins death, Guy intends to break the fragile truce with the sultan Saladin and make war on the Muslims.
 Kerak in Imad ad-Din. Imad ad-Din releases Balian in repayment of the earlier debt. Saladin arrives with his army to besiege Kerak, and Baldwin meets it with his. They negotiate a Muslim retreat, and Baldwin swears to punish Raynald, though the exertion of these events weakens Baldwin. In his camp, Saladin assures his impatient generals that he will claim Jerusalem, but only when he is confident of victory.

Baldwin asks Balian to marry Sibylla and take control of the army, knowing they have affection for each other, but Balian refuses the offer because it will require Guys execution. After Baldwin dies, Sibylla succeeds her brother, and Guy becomes king. Guy releases Raynald, asking him to give him a war, which Raynald does by murdering Saladins sister. Sending the heads of Saladins emissaries back to him, Guy declares war on the Saracens. Guy sends three Teutonic knights  to assassinate Balian, the most strident voice against war, though Balian survives the attempt.
 the ensuing siege that lasts three days, a frustrated Saladin parleys with Balian. When Balian reaffirms that hell let the city burn before surrendering, Saladin agrees to allow the Christians to leave safely in exchange for Jerusalem—though he ponders if it would be better if there were nothing left to fight over.
 to retake the crusader that he is merely a blacksmith again, and they depart. Balian is joined by Sibylla, and they pass by the grave of Balians wife as they ride toward a new life together. An epilogue notes that "nearly a thousand years later, peace in the Holy Land still remains elusive."

==Cast==
Many of the characters in the film are fictionalized versions of historical figures:
* Orlando Bloom as Balian of Ibelin|Balian.
* Eva Green as Sibylla of Jerusalem|Sibylla.
* Jeremy Irons as Raymond III of Tripoli|Tiberias.
* David Thewlis as the Knights Hospitaller|Hospitaler.
* Brendan Gleeson as Raynald of Châtillon|Raynald. King Baldwin. Guy de Lusignan.
* Michael Sheen as Priest.
* Liam Neeson as Godfrey de Bouillon|Godfrey.
* Ghassan Massoud as Saladin.
* Alexander Siddig as Imad ad-Din al-Isfahani|Nasir. Khaled Nabawy as Mullah.
* Kevin McKidd as English Sergeant.
* Velibor Topic as Almaric.
* Jon Finch as Patriarch Heraclius of Jerusalem|Patriarch. Templar Master.
* Nikolaj Coster-Waldau as Village Sheriff.
* Martin Hancock as Gravedigger.
* Nathalie Cox as Balians Wife.
* Eriq Ebouaney as Firuz.
* Jouko Ahola as Odo.
* Philip Glenister as Squire.
* Bronson Webb as Apprentice.
* Steven Robertson as Angelic Priest.
* Alexander Potts as Baldwin V in Directors Cut
* Moustapha Touki as Saracen Knight.
* Michael Shaeffer as Young Sergeant.
* Nasser Memarzia as Muslim Grandee.
* Lotfi Yahya Jedidi as Old Ibelin Housekeeper.
* Samira Draa as Sibyllas Maid. Matthew Rutherford as Rider. Michael Fitzgerald as Humphrey.
* Karim Saleh as Saracen Messenger.
* Shane Attwooll as	Reynalds Templar Knight.
* Giannina Facio as Saladins Sister.
* Emilio Doorgasingh as Saracen Engineer.
* Peter Cant as Peasant Boy. Angus Wright as Richards Knight. Richard Coeur de Lion.

==Historical accuracy== French "Balian") Maria Comnena, Dowager Queen of Jerusalem and Lady of Nablus. Nablus, rather than Ibelin, was Balians fief at the time of Jerusalems fall.

The Old French Continuation of William of Tyre (the so-called Chronicle of Ernoul) claimed that Sibylla had been infatuated with Balians older brother Baldwin of Ibelin, a widower over twice her age, but this is doubtful; instead, it seems that Raymond of Tripoli attempted a coup to marry her off to him to strengthen the position of his faction; however, this legend seems to have been behind the films creation of a romance between Sibylla and a member of the Ibelin family. 

  discovers Baldwin IVs leprosy; his accounts form the historical basis for much of the film.]]
King   (who is unnamed until late in the movie), she chose to crown Guy as her consort. Raymond of Tripoli was not present, but was in Nablus attempting a coup, with Balian of Ibelin, to raise her half-sister (Balians stepdaughter), princess Isabella of Jerusalem, to the throne; however, Isabellas husband, Humphrey IV of Toron, refusing to precipitate a civil war, swore allegiance to Guy. 

Raymond of Tripoli was a cousin of  , where the Crusader force wandered around the desert for three days without water before being ambushed is consistent with the known facts. The scene in the film where Saladin hands Guy a cup of iced water (which in the Muslim world was a sign that the victor intended to spare the life of his prisoner), and then notes that he did not hand Raynald the cup (indicating that Raynald was to be executed) is supported by the Persian historian Imad ad-Din al-Isfahani who was present with Saladin after the Battle of Hattin.
 Tyre and siege of Jerusalem, Saladins forces breached the wall, but the defenders held out until the tenth day, when Balian surrendered the city to Saladin. However, the Christians of the city were made to ransom themselves, and Balian was unable to raise the funds to ransom all the citys poor; thousands marched out into safety and thousands into slavery. 

Balian and Sibylla remained in the Holy Land during the events of the Third Crusade. Sibylla was one victim of an epidemic during the Siege of Acre (1189–1191). Balians relations with Richard I of England were far from amicable, because he supported the claim to kingship of Conrad of Montferrat against Richards vassal Guy. He and his wife Maria arranged her daughter Isabellas forcible divorce from Humphrey of Toron so she could marry Conrad. Ambrose the poet|Ambroise, who wrote a poetic account of the crusade, called Balian "more false than a goblin" and said he "should be hunted with dogs". 
 The History Channels series History vs. Hollywood analyzed the historical accuracy of the film. This program and a Movie Real (a series by A&E Network) episode about Kingdom of Heaven were both included on the DVD release...

==Production==

===Cinematography===
The visual style of Kingdom of Heaven emphasizes set design and impressive cinematography in almost every scene. It is notable for its "visually stunning cinematography and haunting music". {{cite web 
| author = Richard J. Radcliff | title = Movie Review: Kingdom of Heaven 
| date = May 29, 2005 | work = BlogCritics.org 
| url = http://blogcritics.org/archives/2005/05/29/093635.php  
| quote = visually and sonically beautiful; visually stunning cinematography
     and haunting music. 
}} 
Cinematographer John Mathieson created many large, sweeping landscapes, {{cite web 
| author = Stephanie Zacharek | title = Kingdom of Heaven – Salon | date = May 6, 2005 | work = Salon.com 
| url = http://dir.salon.com/story/ent/movies/review/2005/05/06/kingdom/index.html 
| quote = Cinematographer John Mathieson gives us lots of great, sweeping landscapes. 
}} 
where the cinematography, supporting performances, and battle sequences are meticulously mounted. {{cite web 
| author = Carrie Rickey | title = Epic Kingdom has a weak link | work = Philadelphia Inquirer | date = May 6, 2005 
| url = http://ae.philly.com/entertainment/ui/philly/movie.html?id=306852 
| quote = cinematography, supporting performances and battle sequences
     are so meticulously mounted.
}} 
The cinematography and scenes of set-pieces have been described as "ballets of light and color" (as in films by Akira Kurosawa). 
     Uncut, Review of Kingdom of Heaven, Uncut, 2005-07-01, page 129, web:
      :
     noted "Where Scott scores is in the cinematography and set-pieces,
     with vast armies surging across sun-baked sand in almost
     Kurosawa-like ballets of light and color."
 
Director Ridley Scotts visual acumen was described as the main draw of Kingdom of Heaven with the stellar, stunning cinematography and "jaw-dropping combat sequences" based on the production design of Arthur Max. {{cite web 
| author = Nix | title = Kingdom of Heaven (2005) | work = BeyondHollywood.com 
| url = http://www.beyondhollywood.com/reviews/kingdomofheaven.htm 
| quote = "Scotts visual acumen is the main draw of Kingdom of Heaven" and
     "stunning cinematography and jaw-dropping combat sequences" or "stellar cinematography."
}}  {{cite web 
| author = Roger Ebert | authorlink = Roger Ebert | title = Kingdom of Heaven (review) | work = SunTimes.com | date = May 5, 2005 
| url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20050505/REVIEWS/50426001 
| quote = Ebert noted "Whats more interesting is Ridley Scotts visual style,
     assisted by John Mathiesons cinematography and the production design
     of Arthur Max. A vast set of ancient Jerusalem was constructed to
     provide realistic foregrounds and locations, which were then enhanced
     by CGI backgrounds, additional horses and troops, and so on."
}} 

===Visual effects===
British visual effects firm   also contributed to complete the CGI work on the film.   

===Music===
  Patrick Cassidy and Hans Zimmer), sung by Danielle de Niese and Bruno Lazzaretti, were used as replacements for original music by Gregson-Williams.

==Reception==
 

===Critical response===
Upon its release, it was met with a mixed reception, with many critics being divided on the film. Critics such as Roger Ebert, found the films message to be deeper than Scotts Gladiator (2000 film)|Gladiator. 
 Princess Sibylla "with a measure of cool that defies her surroundings",  and Jeremy Irons. 

Lead actor Blooms performance generally elicited a lukewarm reception from American critics, with the Boston Globe stating Bloom was "not actively bad as Balian of Ibelin", but nevertheless "seems like a man holding the fort for a genuine star who never arrives".  One critic conceded that Balian was more of a "brave and principled thinker-warrior"  rather than a strong commander, and Balian used brains rather than brawn to gain advantage in battle.   

Bloom had gained 20 pounds for the part,  and the Extended Directors Cut (detailed below) of Kingdom of Heaven reveals even more complex facets of Blooms role, involving connections with unknown relatives. Despite the criticism, Bloom won two awards for his performance.

Online, general criticism has been also divided, but leaning towards the positive. Review aggregation website Rotten Tomatoes gives the film a score of 39% based on reviews from 185 critics.  Review aggregator Metacritic gives the film a 63/100 rating, indicating "generally favorable reviews" according to the websites weighted average system.  As of early 2006, the Yahoo! Movies rating for Kingdom of Heaven was a "B" from the critics (based on 15 Reviews). This rating equates to "good" according to Yahoo! Movies rating system. 
 Islamic fundamentalists". The Talisman, published in 1825 and now discredited by academics."     Fellow Crusade historian Jonathan Phillips also spoke against the film. Paul Halsall defended Ridley Scott, claiming that "historians cant criticize filmmakers for having to make the decisions they have to make...   not writing a history textbook". 

Thomas F. Madden, Director of Saint Louis Universitys Center for Medieval and Renaissance Studies, criticized the films presentation of the Crusades:

 

Scott himself defended this depiction of the Muslim-Christian relationship in footage on the DVD version of the movies extra features. Scott sees this portrayal as being a contemporary look at the history. He argued that peace and brutality are concepts relative to ones own experience, and since contemporary society is so far removed from the brutal times in which the movie takes place, he told the story in a way that he felt was true to the source material, yet was more accessible to a modern audience. In other words, the "peace" that existed was exaggerated to fit modern ideas of what such a peace would be. At the time, it was merely a lull in Muslim-Christian violence compared to the standards of the period. The recurring use of "Assalamu Alaikum", the traditional Arabic greeting meaning "Peace be with you", is spoken both in Arabic and English several times.

The "Directors Cut" of the film is a four-disc set, two of which are dedicated to a feature-length documentary called "The Path to Redemption". This feature contains an additional featurette on historical accuracy called "Creative Accuracy: The Scholars Speak", where a number of academics support the films contemporary relevance and historical accuracy. Among these historians is Dr. Nancy Caciola, who said that despite the various inaccuracies and fictionalized/dramatized details she considered the film a "responsible depiction of the period." 

Screenwriter William Monahan, who is a long-term enthusiast of the period, has said "If it isnt in, it doesnt mean we didnt know it... What you use, in drama, is what plays. Shakespeare did the same." 

Caciola agreed with the fictionalization of characters on the grounds that "crafting a character who is someone the audience can identify with" is necessary in a film. She said that "I, as a professional, have spent much time with medieval people, so to speak, in the texts that I read; and quite honestly there are very few of them that if I met in the flesh I feel that I would be very fond of." This appears to echo the sentiments of Scott himself.
 cross back on top of a table after it had fallen during the three-day siege of the city. 

===Academic criticism===
In the time since the films release, scholars have offered analysis and criticisms through a lens situating Kingdom of Heaven within the context of contemporary international events and religious conflict, including: broad post-9/11 politics, neocolonialism, Orientalism, the Western perspective of the film, and the detrimental handling of differences between Christianity and Islam.   

=== Box office ===
The film was a box office flop in the U.S. and Canada, earning $47 million against a budget of around $130 million, but did better in Europe and the rest of the world, with the worldwide box office earnings totaling at $211,643,158.  It was also a big success in Arabic-speaking countries, especially Egypt. Scott insinuated that the U.S. disaster of the film was the result of bad advertising, which presented the film as an adventure with a love story rather than as an examination of religious conflict.   Its also been noted that the film was altered from its original version to be shorter and follow a simpler plot line. This "less sophisticated" version is what hit theaters, although Scott and some of his crew felt it was watered down, explaining that by editing, "Youve gone in there and taken little bits from everything". 

Like some other Scott films, Kingdom of Heaven found success on DVD in the U.S., and the release of the Directors Cut has reinvigorated interest in the film. Nearly all reviews of the 2006 Directors Cut have been positive, including a four-star review in the British magazine Total Film and a perfect ten out of ten from IGN DVD.   

===Accolades===
  
; Awards
European Film Awards:
* Audience Award – Best Actor - Orlando Bloom
Satellite Awards:
* Outstanding Original Score - Harry Gregson-Williams
VES Awards:
* Outstanding Supporting Visual Effects in a Motion Picture - Wes Sewell, Victoria Alonso, Tom Wood, and Gary Brozenich

; Nominations 
Satellite Awards:
* Outstanding Actor in a Supporting Role, Drama - Edward Norton
* Outstanding Art Direction & Production Design - Arthur Max
* Outstanding Costume Design - Janty Yates
* Outstanding Visual Effects - Tom Wood
Teen Choice Awards:
*  
* Choice Movie Actor: Action/Adventure/Thriller - Orlando Bloom
* Choice Movie Liplock - Eva Green and Orlando Bloom
* Choice Movie Love Scene - Eva Green and Orlando Bloom (Balian and Sibylla kiss)

==Extended directors cut== STV on the occasion of the extended editions UK release, when he discussed the motives and thinking behind the new version. 

The Directors Cut (DC) has received a distinctly more positive reception from film critics than the theatrical release, with some reviewers suggesting it is the most substantial Directors Cut of all time  and a title to equal any of Scotts other works,  offering a much greater insight into the motivations of individual characters.

==See also==
 
* List of historical drama films
* Battle of Hattin
* Siege of Jerusalem (1187)

==References==
 
 
*  
*  
*  
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 