Don't Give Up the Ship (film)
 
{{Infobox film
| name        = Dont Give Up the Ship
| image       = dontgiveuptheship.jpg
| director    = Norman Taurog Herbert Baker Edmund Belion Henry Garson
| starring    = Jerry Lewis Dina Merrill Diana Spencer Claude Akins Robert Middleton Gale Gordon Mickey Shaughnessy
| music       = Walter Scharf
| cinematography = Haskell B. Boggs
| producer    = Hal B Wallis
| distributor = Paramount Pictures
| released    =  
| runtime     = 85 minutes
| country     = USA
| language    = English
| gross       = $3.5 million (est. US/ Canada rentals)  1,735,230 admissions (France)   at Box Office Story 
}}

Dont Give Up the Ship is a comedy directed by Norman Taurog and starring Jerry Lewis. It was filmed from October 21, 1958 to January 30, 1959, and released on July 3, 1959 by Paramount Pictures.

==Plot==
On his wedding day, John Paul Steckler VII (Jerry Lewis), is tracked down by Navy officers for having misplaced the navy destroyer-escort, USS Kornblatt, following World War II.  He is charged with finding it, and through a series of comedic capers told through flashbacks, the viewer follows him along during his pursuit.

==Production== USS Vammen was used to portray the fictional ship the USS Kornblatt. USS Stembel (DD-644) was listed as the ship portraying the Kornblatt, but this was an error. The Stembel was a Fleet Destroyer, the Vammen (DE-644) a Destroyer-Escort.

==Re-release==
The film was re-released on a double bill with another Jerry Lewis film, Rock-A-Bye Baby (film)|Rock-A-Bye Baby in 1962.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 