Redeeming Love (film)
{{Infobox film
| name           = Redeeming Love
| image          = Redeeming Love poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = William Desmond Taylor
| producer       = Oliver Morosco
| screenplay     = Gardner Hunting L.V. Jefferson 
| starring       = Kathlyn Williams Thomas Holding Wyndham Standing Herbert Standing Jane Keckley Helen Jerome Eddy
| music          = 
| cinematography = Homer Scott
| editing        = 
| studio         = Oliver Morosco Photoplay Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by William Desmond Taylor and written by Gardner Hunting and L.V. Jefferson. The film stars Kathlyn Williams, Thomas Holding, Wyndham Standing, Herbert Standing, Jane Keckley and Helen Jerome Eddy. The film was released on December 28, 1916, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Kathlyn Williams as Naomi Sterling
*Thomas Holding as John Bancroft
*Wyndham Standing as Hugh Wiley
*Herbert Standing as James Plymouth
*Jane Keckley as Aunt Jessica
*Helen Jerome Eddy as Katie
*Don Bailey as McCarthy

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 