Aniyathi
{{Infobox film
| name           = Aniyathi
| image          =Aniyathi.jpg
| image_size     =
| caption        =
| director       = M Krishnan Nair
| producer       = P Subramaniam
| writer         = M Kunchacko
| screenplay     = TN Gopinathan Nair
| music          = Br Lakshmanan
| released       =  
| cinematography = NS Mani
| editing        = KD George
| studio         = Neela
| lyrics         =
| distributor    = Kumaraswamy & Co
| starring       = Prem Nazir, Miss Kumari
| country        = India Malayalam
}}
 Indian Malayalam Malayalam film, directed by M Krishnan Nair and produced by P Subramaniam.  The film stars Prem Nazir, Kumari Thankam in lead roles. The film had musical score by Br Lakshmanan.  Popular song Kunkuma chaaraninju is from this movie. 

==Cast==
* Prem Nazir as Appu
* Miss Kumari as Ammini
* Kumari Thankam as Jayanthi
* Jose Prakash  as Doctor
* Kottarakkara Sreedharan Nair as Pachu Kurup
* TS Muthaiah as Bhargavan
* SP Pillai as SP
* Aranmula Ponnamma as Kalyaniyamma
* TK Balachandran as Babu
* Bahadoor as  Bhasi
* TN Gopinathan Nair as Amminis father

==References==
 

==External links==
*  

 
 
 


 