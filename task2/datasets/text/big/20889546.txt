The Sunshine Boys (1975 film)
 
{{Infobox film
| name           = The Sunshine Boys 
| image          = Sunshine boys.jpeg
| caption        = Theatrical release poster
| director       = Herbert Ross
| producer       = Ray Stark 
| screenplay     = Neil Simon
| based on       =  
| starring       = Walter Matthau   George Burns   Richard Benjamin  Lee Meredith
| cinematography = David M. Walsh
| editing        = John F. Burnett
| studio         = Rastar
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
}} play of I Ought to Be in Pictures with Ann-Margret and Dinah Manoff.
 1996 television adaptation. 
 
Initially, Bob Hope and Bing Crosby were proposed for the leads, but Simon was opposed to the idea, as he felt the roles required Jewish comedians.    Several actors, including Groucho Marx and Phil Silvers were considered and the roles eventually were given to real-life vaudevillian veterans Red Skelton and Jack Benny. 

Benny was forced to withdraw after being diagnosed with the pancreatic cancer that would soon claim him and recommended his friend and fellow real-life vaudevillian veteran Burns, who had not been in a film since 1939, for the role.  Skelton declined after realizing his income was higher performing his stand-up comedy than what he was offered for the film; he was replaced by the younger Matthau.  Burns Academy Award-winning role revived his career and redefined his popular image as a remarkably active, older comedy star. 

==Plot==
Al Lewis (George Burns) and Willy Clark (Walter Matthau) are two old comedians who were once a popular vaudeville comedy act known as "Lewis and Clark" and also called the Sunshine Boys.  After 43 years together, they parted ways 11 years ago on unfriendly terms and have not spoken to each other since then.  The break-up was due, in part, to Als intent to retire and Willys desire to continue performing.  Willys nephew, Ben (Richard Benjamin), a talent agent, tries finding work for Willy, which proves difficult due to Willys age and blustery temperament.

When one of the major networks decides to air a program on the history of comedy and wants the Sunshine Boys to reunite for the show, Ben tries securing the duos cooperation one last time.  Ben tries managing the individual quirks of two old men in their twilight years, including omitting the abuse and insults each used in discussing the other with him, and diplomatically portraying each as anxious to do the "Doctor Sketch" for an ABC special to give the appearance of harmony.

An attempt to rehearse the Doctor Sketch at Willys apartment starts with the two grudgingly getting re-acquainted, but goes only as far as Al entering the doctors (Willys) office, before Willy decides to change the scripted long-established "Come in" to "Enter!"  This results in a loud shouting argument and Als stormy departure.

Ben has to patch up and salvage the situation, despite the objections of Als daughter to her father being bothered any more about the special and manages to get them in the studio.  In the dressing room, they do not speak to each other as persons, just like they did in the last year they did their sketches.  There is unpleasantness when Willy carelessly dumps makeup jars on Al, followed by Willys usual trouble with doors, in the dressing room.

After Phyllis Diller finishes her scene and Steve Allen speaks his introduction, the Doctor Sketch starts.  It flows smoothly until Willy starts shouting about Als spitting on him and poking him in the chest.  Despite Ben and the staff trying to restore order, Willy is finally storming off the set, shouting accusations and abuse, and Al is also leaving (finding it impossible to work with the man).  In the stairwell, Willys ongoing temper tantrum results in him being felled by a serious heart attack.

Willy recovers, first in the hospital and then at home with a private nurse, with whom he argues.  Ben visits and tells him that he has to retire now.  He has in mind an actors retirement home.  Al is also going to move into the same retirement home, as his daughter is going to have a baby, and she will need his room.  The two meet again at Willys apartment, in attempts to finally establish a friendship.

==Cast==
* Walter Matthau as Willy Clark
* George Burns as Al Lewis
* Richard Benjamin as Ben Clark
* Lee Meredith as Nurse in Sketch (Miss McIntosh)
* Carol Arthur as Doris Green Rosetta Le Noire as Odessa
* F. Murray Abraham as Mechanic
* Howard Hesseman as Mr. Walsh
* James Cranna as Mr. Schaeffer
* Ron Rifkin as TV floor manager Jennifer Lee as Helen Clark
* Fritz Feld as Mr. Gilbert
* Jack Bernardi as Man at audition
* Garn Stephens as Eddie the stage manager
* Santos Morales as Desk clerk
* Steve Allen (uncredited) as Himself
* Phyllis Diller (uncredited) as Herself

==Awards and nominations==
The film won an Academy Award and was nominated for three others:   
* Academy Award for Best Supporting Actor (Burns, winner)
* Academy Award for Best Actor (Matthau, nominee)
* Academy Award for Best Adapted Screenplay (Simon, nominee)
* Academy Award for Best Art Direction (Albert Brenner, Marvin March, nominees)
* BAFTA Award for Best Actor in a Leading Role (Matthau, nominee) 
* BAFTA Award for Best Screenplay (Simon, nominee)
* Golden Globe Award for Best Motion Picture – Musical or Comedy (winner)
* Golden Globe Award for Best Actor – Motion Picture Musical or Comedy (Burns, winner)
* Golden Globe Award for Best Actor – Motion Picture Musical or Comedy (Matthau, winner)
* Golden Globe Award for Best Supporting Actor – Motion Picture (Benjamin, winner)
* Golden Globe Award for Best Screenplay (Simon, nominee)
* Writers Guild of America Award for Best Comedy Adapted from Another Medium (winner)

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 