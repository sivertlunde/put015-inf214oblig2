Silks and Satins
{{infobox film
| title          = Silks and Satins
| image          = Silks and Satin.jpg
| imagesize      =
| caption        = period lobby poster
| director       = J. Searle Dawley
| producer       = Adolph Zukor Hugh Ford
| starring       = Marguerite Clark
| music          =
| cinematography = H. Lyman Broening
| editing        =
| distributor    = Paramount Pictures
| released       = June 12, 1916
| runtime        = 5 reels
| country        = USA
| language       = Silent film..(English intertitles)

}}
Silks and Satins is a 1916 silent film produced by the Famous Players Film Company and distributed  by Paramount Pictures. It starred Marguerite Clark and was directed by J. Searle Dawley. 

Filmed at Palisades, New Jersey. A preserved film at the British Film Institute, London. 

==Cast==
*Marguerite Clark - Felicite
*Vernon Steele - Jacques Desmond
*Clarence Handyside - Marquis
*William A. Williams - Henri(as W.A. Williams)
*Thomas Holding - Felix Breton
*Fayette Perry - Annette

==References==
 

==External links==
* 
* 

 
 
 
 
 


 