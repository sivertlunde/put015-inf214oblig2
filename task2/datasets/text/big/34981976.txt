State of Violence
{{Infobox film
| name           = State of Violence
| image          = StateOfViolence2010Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Khalo Matabane
| producer       = Michelle Wheatley, Jeremy Nathan
| writer         = 
| screenplay     = Khalo Matabane
| story          = 
| starring       = Fana Mokoena Presley Chweneyagae Neo Ntlatleng Lindi Matshikiza Vusi Kunene
| music          = 
| cinematography = Matthys Mocke
| editing        = Audrey Maurion
| studio         = 
| distributor    = 
| released       =   
| runtime        = 79
| country        = France South Africa
| language       = 
| budget         = 
| gross          = 
}} 2010 film by South African director Khalo Matabane, featuring Fana Mokoena, Presley Chweneyagae, Neo Ntlatleng, Lindi Matshikiza, and Vusi Kunene.

==Synopsis==
Bobedi has just been made CEO of a large mining company in Johannesburg. After celebrating with his wife, Joy, and friends, he and his wife are victims of a violent attack, and Joy is murdered. Frustrated by the pace of justice, Bobedi decides that revenge is his only option. When he eventually catches the murderer, Bobedi must face the terrible secret that connects them across time and history. He must now choose between continuing the never-ending cycle of violence, or stopping it right then and there.

==Awards==
* Milan 2011
* Saeseeseoaana Junior Poutoa

==References==
 
*  

 
 
 
 
 
 
 


 