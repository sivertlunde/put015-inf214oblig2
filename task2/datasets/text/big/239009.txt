To Catch a Thief
 
{{Infobox film
| name           = To Catch a Thief
| image          = To Catch a Thief.jpg
| caption        = Original film poster
| director       = Alfred Hitchcock
| producer       = Alfred Hitchcock
| screenplay     = John Michael Hayes
| based on       =   John Williams Charles Vanel Brigitte Auber
| music          = Lyn Murray
| cinematography = Robert Burks
| editing        = George Tomasini
| distributor    = Paramount Pictures
| released       =  
| runtime        = 106 minutes
| language       = English
| country        = United States
| budget         = $2.5 million
| gross          = $4.5 million (US rentals original release)   $8.75 million
}}
 romantic thriller thriller directed novel of David Dodge.    The film stars Cary Grant  as a retired cat burglar  who has to save his reformed reputation by catching a new "cat" preying on the wealthy tourists of the French Riviera. Grace Kelly  stars opposite him as his romantic interest in her final film with Hitchcock.

==Plot==
John Robie (Cary Grant) is a retired infamous jewel thief or "cat burglar", nicknamed "The Cat", who now tends to his vineyards in the French Riviera. The modus operandi of a recent series of robberies leads the police to believe that the Cat is involved; they attempt to arrest him, and he adeptly gives them the slip.

He seeks refuge with the old gang from his French Resistance days, a group paroled based on patriotic war work as long as they keep clean. Bertani, Foussard, and the others blame Robie while they are all under suspicion while the Cat is at large. Still, when the police arrive at Bertanis restaurant, Foussards daughter Danielle (Brigitte Auber) spirits her old flame to safety.
 John Williams), who reluctantly obtains a list of the most expensive jewels currently on the Riviera. Jessie Stevens (Jessie Royce Landis) and her daughter Frances (Grace Kelly) top the list. Robie strikes up an acquaintance with them—delighting Jessie even as Frances offers a pretense of modesty.  Robie and Frances meet Danielle at the beach, and Robie must keep up the pretense of being a rich American despite Danielles preference that he be separated from Frances.

 
 
 
 

Frances is not afraid of a little mischief. Although she sees through Robies cover as an American industrialist, the considerable charms of this thief are worth catching. She dangles before him her jewels, teases him with steamy tales of rooftop escapades, exposes herself as a feline of a special breed: an accomplice who might share his passion and be available to his sordid desires. Fireworks fill the night sky.

The next morning, Jessie discovers her jewels are gone. Robie is accused by Frances of being used to steal her mothers jewelry. The police are called but he is back on the lam.

To catch the new burglar Robie stakes out an estate at night and finds himself struggling with an attacker who loses his footing and tumbles over a cliff. It is Foussard, who dies in the fall. The police chief publicly announces that Foussard was the jewel thief, but, as Robie points out to him in the presence of the abashed Hughson, this would have been impossible: Foussard had a prosthetic leg and could not possibly climb on rooftops.

Foussards funeral is marred by Danielles open accusation with Robies quiet attendance that he is responsible for her fathers death. Outside the graveyard, Frances apologizes to Robie and confesses for him her love. Robie wants to continue his search, for the Cat, and asks that she arrange his attendance at the masquerade ball the coming weekend during which he believes will strike again.

At the ball, Frances is resplendent in a gold gown, Robie unrecognizable behind the mask of a Moor. The police hover nearby. Upstairs, the cat burglar silently cleans out several jewel boxes. When Jessie asks the Moor to go get her "heart pills", Robies voice tips off his identity to the authorities. Upon his return the police wait out Frances and the Moor as they dance the night away. Finally, Frances and the Moor go to her room, and the mask is removed: it is Hughson, switched to conceal Robies exit.

On the rooftop Robie lurks. His patience is finally rewarded when he is joined by another figure in black. But just as his pursuit begins, the police throw a spotlight on him and demand he halt. He flees as they shoot at him, but he manages to corner his foe with jewels in hand. Unmasked, his nemesis turns out to be Foussards daughter, Danielle.  She slips off the roof, but Robie grabs her hand before she can fall. He forces her to confess to the police of the father-daughter involvement and that Bertani was the ringleader of this gang.

Robie speeds back to his vineyard and Frances races after to convince him that she has a place in his life. He agrees, but seems less than thrilled that she intends to include her mother.

==Cast==
* Cary Grant as John Robie ("The Cat")
* Grace Kelly as Frances Stevens
* Jessie Royce Landis as Jessie Stevens John Williams as H. H. Hughson
* Charles Vanel as Monsieur Bertani
* Brigitte Auber as Danielle Foussard
* Jean Martinelli as Foussard, Danielles father
* Georgette Anys as Germaine, housekeeper
* René Blancard as Commissaire Lepic 
 signature cameo approximately ten minutes in as a bus passenger sitting next to Cary Grant.

Despite Brigitte Aubers character being referred to as a "girl" or "teenager", compared to Grace Kellys supposedly more mature character, Auber was actually 26 years old at the time of filming and more than a year and a half older than Kelly.

==Production==
This was Hitchcocks first of five films in the widescreen process VistaVision,  and final film wiith Grace Kelly. The film also led to another successful collaboration with Cary Grant, the 1959 classic North by Northwest (also about a man with a mistaken identity who goes on a breakneck adventure to prove his innocence).

The costumes were by Edith Head, including Kellys memorable golden gown for the films costume ball.

==Distribution== Paramount that is still owned and controlled by the company. The others were sold to Hitchcock in the early 1960s and are currently distributed by Universal Studios.

==Awards==
The film won an Academy Award and was nominated in another two categories:   
; Won Best Cinematography (Robert Burks)

; Nominated Best Art Direction (Hal Pereira, Joseph McMillan Johnson, Samuel M. Comer, Arthur Krams) Best Costume Design (Edith Head)

==Bibliography==
*  
*  
*  

==Further reading==
* "Two Interviews About To Catch a Thief  by Tifenn Brisset,   conducted September 2011 and actress Brigitte Auber, September 2011, March 2013, regarding their work on the film and with Cary Grant and Alfred Hitchcock. Discussion of a different ending and script differences. Twelve color photographs, nine pages.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 