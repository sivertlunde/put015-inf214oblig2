Charity Castle
{{Infobox film
| name = Charity Castle
| image = Charity Castle 1918 newspaperad.jpg
| image_size =
| caption = Newspaper advertisement.
| director = Lloyd Ingraham
| producer =
| writer = Doty Hobart
| starring = Mary Miles Minter
| cinematography =
| editing =
| distributor = Mutual Film
| released =  
| runtime =
| country = United States Silent English intertitles
| budget =
}}
 silent comedy-drama film directed by Lloyd Ingraham. The film is believed to be Lost film|lost.

==Plot==
Charity takes her little brother and hides in an abandoned mansion. Burglar Lucius Garrett accompanies them and they all decide to restore the place. Suddenly, the son of the owner, Merlin Durand turns up. He falls in love with Charity and they soon marry. 

==Cast==
* Mary Miles Minter - Charity
* Clifford Callis - The Prince
* Allan Forrest - Merlin Durand
* Eugenie Forde - Zelma Verona
* Henry A. Barrows - Simon Durand
* Ashton Dearholt - Elmer Trent
* Robert Klein - Graves
* Spottiswoode Aitken - Lucius Garrett

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 