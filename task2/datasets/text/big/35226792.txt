Kanni Rasi
{{Infobox film
| name = Kanni Rasi
| image = Kanni Rasi DVD cover.svg
| image_size =
| caption = DVD cover
| director = Pandiarajan
| producer = S. Shanmugarajan D. Manickavasagam
| writer = Pandiarajan 
| story = Pandiarajan Livingston
| starring =  
| music = Ilaiyaraaja Ashok Kumar
| editing = V. Rajagopal
| distributor = Vigranth Creation
| studio = Vigranth Creation
| released = 15 February 1985
| runtime = 130 minutes
| country = India
| language = Tamil
| budget =
| preceded_by =
| followed_by =
| website =
}}
 1985 Tamil Prabhu and Revathi in lead roles. The film, produced by S. Shanmugarajan and D. Manickavasagam, had musical score by Ilaiyaraaja and was released on 15 February 1985.   

==Plot==
Lakshmipathi (Prabhu (actor)|Prabhu) is a carefree youth who searches a job. His sister Kasthuri (Sumithra (actress)|Sumithra) sends him a letter from the city to rejoin her and the villagers compel him to leave the village, he leaves the village to rejoin his sister. His brother-in-law (Goundamani) finds a job as watchman for him. There, Dhanalakshmi (Revathi), his sisters daughter, and him fall in love with each other. Kasthuri decides to meet some astrologers and they say that if Lakshmipathi marries Dhanalakshmi, he will certainly die. Kasthuri evicts him from her house and Sivaraman (Janagaraj (actor)|Janagaraj), who is also in love with Dhanalakshmi, accommodates him without knowing his identity. Kasthuri convinces her daughter to forget him and later, Dhanalakshmi criticizes Lakshmipathi that he was not a well-educated. Sivaraman promises  Dhanalakshmi that he will get them together. Meanwhile, Lakshmipathi returns to his village. Dhanalakshmi sends a letter, mentioning that she will marry another man. Lakshmipathi stops her marriage, convinces his sister and marry her but she dies immediately because she had taken poison by then.

==Cast== Prabhu as Lakshmipathi
* Revathi as Dhanalakshmi Sumithra as Kasthuri
* Goundamani as Kasthuris husband Janagaraj as Sivaraman
* Venniradai Moorthy as a professors
* S. N. Lakshmi as Lakshmipathis mother Senthil as Lakshmipathis friend
* Mayilsamy as a dry-fruit seller
* Pandiarajan in a cameo appearance

==Soundtrack==
{{Infobox album |  
 Name = Kanni Rasi |
 Type = soundtrack |
 Artist = Ilaiyaraaja |
 Cover = |
 Released = 1985 |
 Recorded = 1985 | Feature film soundtrack |
 Length = 18:05 |
 Label = |
 Producer = Ilaiyaraaja |
 Reviews = |
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1985, features 4 tracks with lyrics written by Vaali (poet)|Vaali, Vairamuthu, Gangai Amaran and Kuruvikkarambai Shanmugam .  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Lyrics !! Duration
|- 1 || Vaali || 4:37
|- 2 || Sorunna Satti || Ilaiyaraaja, T.K.S. Kalaivanan, Krishnachandar, Deepan Chakravarthy || Vairamuthu || 4:22
|- 3 || Sugaraagame || Malaysia Vasudevan, Vani Jairam || Kuruvikkarambai Shanmugam || 4:51
|- 4 || Kaathaliley Thozhvi || Malaysia Vasudevan, Gangai Amaran || Gangai Amaran || 4:15
|}

==References==
 

==External links==
*  

 

 
 
 
 
 