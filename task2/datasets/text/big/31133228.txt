Keli (film)
{{Infobox film
| name           = Keli
| image          = Keli film.jpg
| image size     = 
| alt            = 
| caption        = CD Cover
| director       = Bharathan
| producer       = Ganga Movie Makers John Paul
| based on       =  
| narrator       =  Innocent Murali Murali Nedumudi Venu Johnson  (background score)  Venu
| editing        = N. P. Suresh
| studio         = Ganga Movie Makers
| distributor    = 
| released       = 1991
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Murali and Nedumudi Venu in pivotal roles, the film had songs composed by Bharathan himself.

==Plot==
The film is about a handicapped young man Narayanankutty, who owns a stationery shop which is gifted by Lazer. He falls in love with a school teacher Sridevi. The film takes a turn when Hema, another teacher at the same school, is murdered. Narayanankutty is accused in relation with the case, being trapped by Lazer, a filthy businessman and politician.

==Cast==
* Jayaram as Narayanankutty
* Charmila as Sridevi Innocent as Lazer
* Nedumudi Venu as Romance Kumaran Murali as Appootty
* K. P. A. C. Lalitha as Seemanthini
* Sukumari as Muthassi
* Balan K. Nair as Ramankutty Nair
* Unnimary as Mrs. Menon
* Shyama as Hema
* Adoor Bhawani as Paruvamma
* Abubakkar as Chettiyar
* Mala Aravindan as Commentator

==Soundtrack==
There are two songs the film, composed by director Bharathan and lyrics penned by Kaithapram Damodaran Namboothiri.
#"Olelam Paadi" — Lathika
#"Thaaram Valkkannadi Nokki" — K. S. Chithra
The song "Thaaram Valkkannadi Nokki" is composed in Bharathans favourite Carnatic raga Hindolam.

==External links==
*  
*   at the Malayalam Movie Database

 
 
 
 
 


 