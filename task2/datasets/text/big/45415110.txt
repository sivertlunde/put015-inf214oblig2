Batman Unlimited: Animal Instincts
{{Infobox film
| name        = Batman Unlimited: Animal Instincts
| image       = Batman-unlimited-animal-instincts-blu.jpg
| caption     = 
| director = Butch Lukic
| producer = Butch Lukic
| screenplay = Heath Corson
| based on = 
| starring = Roger Craig Smith Dana Snyder Chris Diamantopoulos
| music = Kevin Riepl
| cinematography =
| editing = Bruce A. King
| studio = Warner Bros. Animation DC Entertainment
| distributor = Warner Home Video
| released =   }}
| runtime = 
| country = United States
| language = English
| budget =
| gross =
}}
Batman Unlimited: Animal Instincts is a direct-to-video animated superhero film. It will be released on May 12, 2015 on Blu-ray, DVD and Digital HD.

==Synopsis== Red Robin, Flash must band together to stop them.

==Cast==
* Roger Craig Smith - Batman|Batman/Bruce Wayne    The Penguin/Oswald Cobblepot  Green Arrow/Oliver Queen  Laura Bailey - Cheetah (comics)|Cheetah/Barbara Minerva,    Newcaster Killer Croc/Waylon Jones 
* Will Friedle - Dick Grayson|Nightwing/Dick Grayson  Red Robin/Tim Drake 
* Phil LaMarr - Man-Bat|Man-Bat/Kirk Langstrom Flash
* Keith Szarabajka - Silverback  Commissioner James Gordon
* Alastair Duncan - Alfred Pennyworth
* Amanda Troop - Gladys Windsmere, Pretty Girl
* Matthew Mercer - Mech Guard 1, Wealthy Jock
* Eric Bauza - Punk #1, Rookie Cop
* Mo Collins - Dispatch, Distinguished Woman
* Keith Ferguson - Gruff Cop, Distinguished Man

==Production==
The films name and character designs are taken from Mattels toy line. 

It will be followed by a sequel, which is to be released later in 2015, and 22 short cartoons. A mobile application will be released as well. 

==References==
 

 
 

 
 
 
 
 
 
 
 


 
 