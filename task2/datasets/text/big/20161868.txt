The Legend of the Evil Lake
{{Infobox film
| name           = The Legend of the Evil Lake
| image          = The Legend of the Evil Lake.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         = 천년호
 | hanja          =  
 | rr             = Cheonnyeonho
 | mr             = Ch‘ŏnnyŏnho}}
| director       = Lee Kwang-hoon
| producer       = Kim Hyeong-joon
| writer         = Hong Joo-ri
| starring       = Jung Joon-ho Kim Hye-ri Kim Hyo-jin Lee Han-gal Kang Shin-il Park Dong-bin
| music          = Lee Dong-joon
| cinematography = Lü Yue Kim Yoon-su
| editing        = Lee Hyun-mi
| distributor    = Cinema Service
| released       =  
| runtime        = 92 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} 2003 South Korean fantasy/horror film directed by Lee Kwang-hoon. It is a remake of Shin Sang-oks 1969 film A Thousand Year-Old Fox.   The film was not particularly well received and was seen by 101,478 people at Seoul theaters. 

==Plot== Park Hyeokgeose and his technologically advanced army. The Auta tribal chief in death throes vows vengeance against Silla, and their blood and resentment fill the place where the Sacred Tree was standing, and turns into a lake. To seal off the Auta tribes force of sorcery, he drives his Holy Sword deep into the Sacred Tree.

The narrative resumes nearly one millennium later, circa A.D. 896, as Silla is showing symptoms of strain and the court is beset by constant rebellions. Queen Jinseong (Kim Hye-ri), troubled by the threats to the throne, leans on General Biharang (Jung Joon-ho) against the counsels of her ministers. Biharang strives to save the country, but he is also weary of the endless battles. He, however, spurns the Queen’s romantic attentions in favor of his betrothed Jaunbi (Kim Hyo-jin), the daughter of an executed rebel, with whom he wishes to settle down and lead a quiet, peaceful life.

While Biharang has left for the country’s border to quench the rebels, Jaunbi is chased by assassins who were apparently ordered by the Queen to kill her. In panic, Jaunbi removes the Sword from the Sacred Tree thus releasing the soul of Auta tribal chief. But the assassins easily overpower her and try to rape her. Jaunbi somehow slips away from their hands and reaches at the edge of the Evil Lake. She throws herself into the Lake before leaving her necklace there as a sign for Biharang that she is dead.

The spirit of the Auta tribe who are resentful of the Silla kingdom use Jaunbi’s body to take their revenge on Silla. She is transformed into a flying phantasm with superpowers and seeks to lay waste to Seorabeol, capital of Silla. When the moon soaked with a thousand years’ resentment shines its rays upon the dark earth, an age-old revenge begins, and love turns to tragedy.

==Cast==
* Jung Joon-ho ... General Biharang
* Kim Hyo-jin ... Jaunbi
* Kim Hye-ri ... Queen Jinseong
* Won-seok Choi  ... Master Myo-hyeon
* San Ho ... General Bang Oh-rang
* Jang Hyun-Sung
* Lee Han-gal ... Talwi
* Kang Shin-il ... Minister Mun-su
* Ho-san
* Park Dong-bin
* Jung Joo-Hwan

==Trivia==
* In India, the film was released in Hindi-dubbed version with the title Karo Ya Maro: Part 2 - Pratishodh.

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 