Sikka (film)
{{Infobox film
| name           = Sikka
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Kovelamudi Bapayya|K. Bapaiah
| producer       = Shrikant Nahata	
| writer         = M.D. Sunder
| screenplay     = M.D. Sunder	
| story          = 
| based on       =  
| narrator       = 
| starring       = Dharmendra Jackie Shroff Moushumi Chatterjee Dimple Kapadia
| music          = Bappi Lahiri
| cinematography = A. Venkatesh	 
| editing        = Venkataratnam D.
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Kovelamudi Bapayya|K. Bapaiah and produced by Shrikant Nahata. It stars Dharmendra, Jackie Shroff, Moushumi Chatterjee and Dimple Kapadia in pivotal roles.

==Plot==
Two close and very wealthy friends, Vijay and Pranlal, decide to cement their friendship to a relationship when Vijay proposes marriage on behalf of his sister, Shobha, with Pranlals son, Amar. Pranlal agrees, and preparations are on for the marriage to take place. Vijay finds out that Pranlal and his men are using their business as a front for drugs and smuggling, and he decides to cancel the wedding. Pranlal is angry with Vijay, but refuses to give up his criminal career. When Amar finds out about his dads spurious activities, he kills himself. Pranlal, humiliated and angered, vows to avenge Amars death, and wants Shobha to be Amars widow for the rest of her life, which is not acceptable to Vijay. Then Shobha meets with a handsome stranger named Jackie, who rescues her. She offers him employment with her brothers firm, and they fall in love with each other. When Vijay finds out he is upset that his sister has chosen a mere employee for a life-partner, and decides to oppose this marriage at all costs. But both Shobha and Jackie are adamant, and refuse to budge, and finally Vijay gives in, and soon both are married. After the marriage, Jackie starts to show his true colors, and becomes very abusive. Shobha then finds out the terrible truth behind Jackies hatred, a truth that will change her and Vijays lives forever.

==Cast==
* Dharmendra...Vijay
* Jackie Shroff...Jai Kishan Jackie
* Moushumi Chatterjee...Laxmi (Vijays Wife)
* Dimple Kapadia...Shobha
* Asrani...	Pyarelal
* Kader Khan...Daruka
* Aruna Irani...Dulari
* Prem Chopra...Pranlal Sinha
* Mahesh Anand...George (Pranlals Henchman)
* Bharat Kapoor...Gupta (Pranlals Agent)

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tu Husnki Sarkar"
| Sudesh Bhosle, Alisha Chinai
|-
| 2
| "Jab Tak Hai Dum Mein Dum"
| Vijay Benedict, Asha Bhosle
|-
| 3
| "Aadhi Raat Ko Aankh Khuli"
| Kishore Kumar, Asha Bhosle
|-
| 4
| "Kothi Hillela"
| Amit Kumar, Alka Yagnik
|}

==External links==
* 

 
 
 

 