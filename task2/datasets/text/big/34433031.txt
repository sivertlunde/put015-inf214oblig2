Kanoon Apna Apna
{{Infobox film
| name           = Kanoon Apna Apna
| image          = Kanoon-Apna-Apna-1989.jpg
| alt            =  
| caption        = DVD Cover
| director       = B. Gopal
| producer       = A.S.R. Anjaneyulu 
| writer         = A. Pushpanand   Ganapathi Rao Kommanapalli (Story)   Sachin Bhowmick (Screenplay)   Kader Khan (Dialogue)
| based on       =  
| starring       = Dilip Kumar   Sanjay Dutt 
| music          = Bappi Lahiri
| cinematography = Gopal Reddy S.   
| editing        = V.R. Kotagiri
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1989 Hindi action-drama film starring Dilip Kumar and Sanjay Dutt in lead roles. It was boxoffice flop earned merely 2 Crore Nett in 1989. Movie was not well received by audience and critics alike.
==Plot==
The Collector of Anokhapur, Jagatpratap Singh (Dilip Kumar) urges on law and order. However his son Ravi (Sanjay Dutt) believes that sometimes illegal means should be adopted to bring control. This causes discomfort between father and son. Bhushannath Dharmendra Bhadbhole (Kader Khan) is a corrupt Minister of Fisheries. His son Kailash (Gulshan Grover) and Kabza Kanhaiyalal (Anupam Kher) rape and kill Jagatprataps maid-servant and also kill Ravis friend. But they are acquitted because of a faked alibi. Ravi decides to put these criminals to punishment which they deserve. But his fathers legalism causes problems to him and he decides to leave the home and fight. Ravi for this purpose becomes a police inspector. But this does not heal the father-son bond as he still has different rules of justice. On the statement that the law is for human beings but not for monsters, the father finally manages to join his son in his fight against the corrupt minister.

==Cast==
* Dilip Kumar as Collector Jagatpratap Singh 
* Nutan as Lakshmi 
* Sanjay Dutt as Ravi Kumar Singh
* Kader Khan as Bhushannath Dharmendra Bhadbhole 
* Gulshan Grover as Kailash Bhadbhole 
* Anupam Kher as Kabza Kanhaiyalal 
* Madhuri Dixit as Bharathi 
* Satyendra Kapoor as Editor Ramprasad 
* Tej Sapru as Prakash K. Kanhaiyalal 
* Mayur Verma as Satyen (Ramprasads son)
* Raza Murad as Dr. Mathur 
* Jayshree Gadkar as College Principal 
* Disco Shanti as Dancer / Singer 

==Music==
The songs of the film are written by Indeevar and composed by Bappi Lahiri.
{{Track listing
| extra_column = Singer(s)
| title1 = Pehli Nazar Mein Ho Gaye Hum Toh Aapke Deewane| extra1 = S.P. Balasubramaniam, Asha Bhosle | length1 = 
| title2 = Gai Gai Gai Gai Main Toh Gai| extra2 = S.P. Balasubramaniam, Asha Bhosle | length2 = 
| title3 = A B C D E F G| extra3 =Alka Yagnik | length3 = 
| title4 = Eini Mini Aai Aai Yoh| extra4 = | length4 = 
}}

== References ==
 

== External links ==
*  

 
 
 