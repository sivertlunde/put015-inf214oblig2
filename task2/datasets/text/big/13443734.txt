Kings (film)
{{Infobox Film
| name           = Kings
| image          = 
| caption        = Kings film poster Tom Collins
| producer       = Tom Collins Jackie Larkin
| based on       =  
| writer         = Tom Collins
| starring       = Colm Meaney Donal OKelly Brendan Conroy Donncha Crowley
| music          = Pol Brennan
| cinematography = P.J. Dillon
| editing        = Dermot Diskin
| distributor    = High Point Film and Television Ltd
| released       =  
| runtime        = 88 minutes
| country        = Ireland
| language       = Irish English
| budget         = 
| gross          = 
}} Irish film Tom Collins Jimmy Murphys Irish and English dialogues. 2008 Academy best foreign-language film category.  The film tells the story of a group of Irish friends who, after emigrating to England 30 years previously, meet for the funeral of a friend. In 2008, the Irish postal service, An Post, issued a series of stamps honouring the Irish film industry. Colm Meaney, as Joe Mullan, was featured on the 55 cent stamp.

==Plot==
In the mid 1970s a group of young men leave the Connemara Gaeltacht, bound for London and filled with ambition for a better life. After thirty years, they meet again at the funeral of their youngest friend, Jackie. The film intersperses flashbacks of a lost youth in Ireland with the harsh realities of modern life.

For some the thirty years has been hard, working in building sites across Britain. Slowly the truth about Jackies death become clear and the friends discover they need each other more than ever. However, by the end, the friends split up for good, going their separate ways.

==Cast==
{| class="wikitable"
|-
! Actor
! Role
|- Joe Mullan
|-
| Donal OKelly || Jap Kavanagh
|- Git
|- Donncha Crowley||Shay 
|-
| Barry Barnes ||Máirtín
|- Joe Jnr
|- Christopher Greene||Jap Jnr
|-
|}

== Awards ==
In 2007, Tom Collins won the Directors Guild of America / Ireland New Finders Award. The film itself was nominated for a record 14 Irish Film and Television Awards (IFTAs) in 2008, – going on to win 5 IFTAs, including Best Irish Language Film

IFTA Awards (2008)-

Won – 

*Best Actor in a Supporting Role in a Feature Film – Brendan Conroy
*Best Editing – Dermot Diskin
*Best Original Score – Pol Brennan
*Best Sound – Ken Galvin, Ronan Hill, Dominic Weaver
*Special Irish Language Award

Nominated – 

*Best Actor in a Lead Role in a Feature Film – Colm Meaney
*Best Actor in a Supporting Role in a Feature Film – Donal OKelly
*Best Costume Design – Maggie Donnelly
*Best Director of Photography – P.J. Dillon
*Best Hair & Make-Up – Muriel Bell, Pamela Smyth
*Best Production Design – David Craig Tom Collins Tom Collins Tom Collins

== References ==
 

== External links ==
*  
*  

 
 
 
 