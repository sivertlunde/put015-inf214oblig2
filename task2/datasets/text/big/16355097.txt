Achchamundu! Achchamundu!
 
{{Infobox film
| name = Achchamundu! Achchamundu!
| image = achchamundu.jpg
| director = Arun Vaidyanathan
| writer = Arun Vaidyanathan Prasanna Sneha Sneha John Shea  Akshaya Dinesh
| producer = Arun Vaidyanathan Asma Hashmi Ananth Govinda P. Srinivasan Ramzan Lakhani
| music = Karthik Raja
| cinematography = Chris Freilich
| released = 17 July 2009
| country = India United States
| runtime = 135 minutes Tamil
| budget =
}}
 Tamil social Sneha and Red One Telugu dubbed version is to be released as Bhayam.. This film was reportedly one of the rare main stream Tamil films to deal with the subject of pedophiles. The film was released on 17 July 2009 worldwide, despite  receiving  highly positive reviews it was a commercial failure.

==Plot==
Senthil Kumar (Prasanna (actor)|Prasanna) and Malini (Sneha (actress)|Sneha) are a happily married couple in New Jersey, living life like any other born-in-India, arrived-in-the-US couple do. He submerges himself in the office and eats sambhar rice at home and she never misses a bhajan at the temple and shops at Indian stores. They have a daughter Rithika (Akshaya Dinesh), 10 years old, the apple of their eyes (their cars registration number carries the name of their daughter.)
 viboothi (holy ash, usually applied on forehead) and takes his family on weekend trips and birthday parties. It is a normal, happy life in the US until Robertson (John Shea) arrives, to paint the basement. Now, things are never the same at home again.

Robertsons unusually kind behaviour towards children sets off warning bells in your head especially when he is shown to be a pedophile who exercises like mad within the confines of his home, always moving on towards his next target. How the couple save their child forms the rest of the story.

==Cast== Prasanna as Senthil Kumar Sneha as Malini Kumar
*John Shea as Theodore Robertson
*Akshaya Dinesh as Rithika

==Production==

===Filming=== Red One camera was used for filming. The film was shot in New Jersey and New York during early 2008. Color correction and sound mixing were done in Los Angeles.

The film was premiered in New Jerseys Garden State Film Festival, and shared Best Homegrown Feature Film award with Dorothy Lymans Split Ends.

The film was selected for the Shanghai International Film Festival in International Panorama Category. The films audio and trailer were launched by Academy Award winner Resul Pookutty. The movie was released on 17 July 2009 in Chennai and USA.

==Reception==
* Shanghai International Film Festival
The film was officially selected for screening at the 12th Shanghai International Film Festival, where Academy Award-winner Danny Boyle headed the jury. The film was selected under the International Panorama category.

The film had a successful reception and the audience shared their appreciation with director Arun Vaidyanathan in a Q & A session. 

* United States
The film received the Best Homegrown Feature Film award at the Garden State Film Festival, in Asbury Park, New Jersey. The film also won critical acclaim at New York on its official release. 

* Digital Cinema Film Festival, Japan
The film was selected for the Digital Cinema Film Festival in Japan and was well received. 

* Cairo International Film Festival
Achchamundu Achchamundu was selected for screening at the 33rd Cairo International Film Festival.

* Goa Film Festival
It was selected for screening in the Goa film festival on 23 November 2009, along with the only Tamil film Pasanga.

* Chennai International Film Festival
AA was selected for screening in the Chennai International Film Festival along with Ayan (film)|Ayan, Thiru Thiru Thuru Thuru and Pokkisham. It won best feature film award. Pasanga won the second best feature film award.

* Awards won in Tamil Nadu
1. The director of the movie Arun Vaidhyanadhan received a Media Guild Award for creating public awareness of child sex abuse.  Sneha won Edison Award for Best Actress.  Filmfare Best Actress Award Category held on August 7, 2010, in Chennai.

* Rated as Top 100 movies must watch in the past decade - Behindwoods

* Music
Karthik Rajas music for the film was well received. 

==Soundtrack==
The soundtrack was composed by Karthik Raja.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length
|- Krish || 4.18
|-
| 2|| "Kannil Dhaagam" || S. Sowmya || 3.42
|-
| 3|| "Parvai Vali Sumandhal" || Rahul Nambiar || 1.25
|-
| 4|| "Theme Music" || || 3.35
|-
|}

==Reviews==
* 
* 
* 
* 
* 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 