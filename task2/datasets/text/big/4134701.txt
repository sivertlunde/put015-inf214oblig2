Objective, Burma!
 
{{Infobox film
| name           = Objective, Burma!
| image          = Objective burma.jpg
| image_size     =
| caption        = Original film poster
| director       = Raoul Walsh
| producer       = Jerry Wald
| writer         = Alvah Bessie (story)
| screenplay     = Ranald MacDougall Lester Cole Truman Bradley James Brown
| music          = Franz Waxman
| cinematography = James Wong Howe
| editing        = George Amy
| distributor    = Warner Bros.
| released       =  
| runtime        = 142 min
| country        = United States English
| budget         = gross = 2,635,192 admissions (France)   at Box Office Story 
}}

Objective, Burma! is a 1945 war film which was loosely based on the six-month raid by Merrills Marauders in the Burma Campaign during the Second World War. The film, made by Warner Brothers immediately after the raid, was directed by Raoul Walsh and starred Errol Flynn.

==Plot==
  playing Captain Nelson, the groups heroic leader. ]] United States Japanese Army Chinese Army Captain and an older war correspondent (Henry Hull) whose character is used to explain various procedures to the audience.

The mission is an overwhelming success as the 36-man team quickly take out the station and its personnel. But when the airborne troops arrive at an old airstrip to be taken back to their base, they find the Japanese waiting for them at their rendezvous site. Captain Nelson makes the hard decision to call off the rescue planes, and hike out on foot.

To reduce the likelihood of detection, the group then splits up into two smaller units to meet up at a deserted Burmese village. But when Nelson arrives at the meeting place, he finds that the other team had been captured, tortured and mutilated by the Japanese. Only Lt. Jacobs survives, and he too dies after telling Nelson what had happened.  The surviving soldiers are then attacked and are forced again to retreat into the jungle. The men must then cross the swamps in their attempt to make it back to safety through enemy-occupied jungle.
 British Chindits#Operation 1944 aerial invasion of Burma.

==Cast==
*Errol Flynn as Captain Nelson James Brown as SSgt. Treacy William Prince as  Lt. Sid Jacobs
*George Tobias as Cpl. Gabby Gordon
*Henry Hull as Mark Williams (American News correspondent) 503rd Parachute Infantry) John Alvin as Hogan Mark Stevens (credited as Stephen Richards) as Lt. Barker
*Richard Erdman as Pte. Nabraska Hooper General Stilwell, (uncredited)
*Rodd Redwing as Gurkha Sergeant, (uncredited)
*Hugh Beaumont as Captain Hennessey (uncredited)

==Production==
Producer Ranald MacDougall had been a creator and co-writer of the CBS radio series The Man Behind the Gun that was awarded a 1942 Peabody Award.   He had been contracted to Warner Brothers, with this his second film after uncredited work on Pride of the Marines.

Jerry Wald later claimed he got the idea for doing a film set in Burma in Christmas 1943, feeling that that theatre of the war would soon be active, and hoping the movie could be made and released before then. OUT OF THE HOLLYWOOD HOPPER: MGMs Musical Splurge -- O & J Hit the Jackpot -- Other Items
By FRED STANLEYHOLLYWOOD.. New York Times (1923-Current file)   07 May 1944: X3.   The film was announced in January 1944, with Wald and Walsh attached. Errol Flynn was already being discussed as the star. SCREEN NEWS HERE AND IN HOLLYWOOD: Cary Grant to Star in Farce at RKO -- Song of Bernadette Opens Today at Rivoli
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   26 Jan 1944: 23.  Franchot Tone was mentioned as a possible co star. Looking at Hollywood
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   14 Apr 1944: 20.   Filming began in April 1944. SCREEN NEWS HERE AND IN HOLLYWOOD: M-G-M to Co-Star Garson and Tracy -- Three Theatres to Show Premieres Today
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   25 Apr 1944: 16.   By this stage the Allied campaign had already started in Burma, meaning Wald was unable to do a Casablanca style cashing in on the films release. 
 Northwest Passage. 

===Location===
Exteriors were shot at the Los Angeles County Arboretum and Botanic Garden, California. Filming began on May 1, 1944 and was scheduled for 60 days. But shooting required more than 40 extra days due to bad weather and constant script changes.

The movie also contains a large amount of actual combat footage filmed by U.S. Army Signal Corps cameramen in the China-Burma-India theatre  as well as New Guinea. 

==Reception==
The film was one of the most popular movies of 1945 in France. 
===Banned===
Even though it was based on the exploits of Merrills Marauders, Objective Burma was withdrawn from release in the United Kingdom after it infuriated British Prime Minister Winston Churchill and drew protests about the Americanization of an almost entirely British, Indian and Commonwealth conflict.  WARNERS SUSPEND BURMA IN BRITAIN: Film Criticized by Public on Ground That It Ignored the Fourteenth Army
Br Wireless to THE NEW YORK TIMES.. New York Times (1923-Current file)   26 Sep 1945: 27.    An editorial in The Times said: 
 
It is essential both for the enemy and the Allies to understand how it came about that the war was won&nbsp;... nations should know and appreciate the efforts other countries than their own made to the common cause.
 
The film was not put on general release in the UK until 1952 when it was shown with an accompanying apology. The movie was also banned in Singapore although it was seen in Burma and India.  Thomas et al. 1969, p. 140. 

There were also objections to Errol Flynn playing the hero as, despite being Australian, he had stayed in Hollywood during the war, unlike actors like David Niven or James Stewart.    Flynn, however, had tried to enlist but had been declared medically unfit for military service.  His studio suppressed the news of his medical problems to preserve his public image.

Reviews of Objective, Burma! were generally favorable, with Variety noting: "The film has considerable movement, particularly in the early reels and the tactics of the paratroopers are authentic in their painstaking detail. However, while the scripters have in the main achieved their purpose of heightening the action, there are scenes in the final reels that could have been edited more closely." 

== Nominations ==
The movie was nominated for three Academy Awards in 1945: Film editing - George Amy Original Music Score - Franz Waxman Best Story - Alvah Bessie

==References==

===Notes===
 

===Bibliography===
 
* Dunning, John. On the Air: The Encyclopedia of Old Time Radio. Oxford, UK: Oxford University Press, 1998. ISBN 978-0-19507-6-783.
* Hobbes, Nicholas. Essential Militaria: Facts, Legends, and Curiosities About Warfare Through the Ages. London: Atlantic Books, 2003. ISBN 978-1-84354-229-2.
* Holston, Kim R. The English-Speaking Cinema: An Illustrated History, 1927-1993. Jefferson, North Carolina: McFarland & Company, 1994. ISBN 978-0-89950-8-580.
*  Thomas, Tony, Rudy Behlmer and Clifford McCarty. The Films of Errol Flynn. Sacramento, California: Citadel Press, 1969.
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 