The Pixar Story
{{Infobox film
| name = The Pixar Story
| director = Leslie Iwerks
| image = The Pixar Story Poster.jpg
| writer = Leslie Iwerks
| producer = Leslie Iwerks
| music = Jeff Beal
| narrator = Stacy Keach
| editing = Leslie Iwerks   Stephen Myers, A.C.E Leslie Iwerks Productions Walt Disney Studios Motion Pictures
| released =  
| runtime = 87 minutes
| language = English
}}
 Pixar Animation Starz cable network in the United States.
 Pixar films. It was then included as a special feature on the WALL-E special edition DVD and Blu-ray releases, which were launched on November 18, 2008.

The film premiered on BBC in the United Kingdom on August 24. 

==Synopsis==
The success story of Pixar Animation Studios from the ground up.

==Notable interviewees==
* Tim Allen – Actor, voiced Buzz Lightyear
* Brad Bird – Director
* Edwin Catmull – First Chief Technical Officer
* Billy Crystal – Actor, voiced Mike in Monsters, Inc. and Monsters University
* Michael Eisner – ex-CEO of The Walt Disney Company
* Tom Hanks – Actor, voiced Sheriff Woody
* Bob Iger – CEO of The Walt Disney Company
* Steve Jobs – Principal Investor & co-founder of Pixar 
* John Lasseter – Pixar co-founder & visionary
* George Lucas – Owner of Lucasfilm, Pixars original foundation

==Reception==
The film was given generally positive reviews, receiving an aggregated score of 86% from Rotten Tomatoes, based on 7 reviews. 

==See also==
*Waking Sleeping Beauty, a 2009 documentary film chronicling the history of Walt Disney Animation Studios.

==References==
 

==External links==
*   
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 