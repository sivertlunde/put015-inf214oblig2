The Recruiter (2008 film)
{{Infobox film
| name           = The Recruiter
| image          =
| caption        =
| director       = Edet Belzberg
| producer       =
| writer         = Edet Belzberg
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Recruiter is a 2008 documentary film directed by Edet Belzberg. 
 economic crisis is giving way to more willing recruits and many of them have one of the key measures to join, a high school diploma. Still the ineligibility rates some military leaders say are, “a matter of national security.” 

The Recruiter takes viewers to the Louisiana coast, where they witness firsthand Sergeant First Class Clay Usie’s struggle to enlist new soldiers into the United States Army|U.S. Army in his hometown of Houma, Louisiana|Houma, LA. Sgt. Usie believes that every American should serve their country and he sets his sights on Lauren, Matt, Bobby, and Chris, four teenagers who think that the Army is the answer for them.
 boot camp.  After graduation, the film follows three of the four teenagers to basic training where they make the transition from civilians to soldiers. (The fourth teenager, Chris, was shipped out to Iraq early due to high casualties in the war.) The hardships that come with being in the army begin to arise at basic training.  One girl has a massive panic attack, while some of the boys try to fake being gay in order to be discharged under “don’t ask, don’t tell.” Once basic training is complete, the film follows Matt and Lauren back to Houma for their one-month leave before they ship out to Iraq and/or Afghanistan. Bobby, who was selected to join Special Forces, continues his training. The Recruiter wraps up with epilogues of where each soldier is now.

== Release ==
It competed in the Documentary Competition at the 2008 Sundance Film Festival under the title An American Soldier.   

== References ==
 

== External links ==
*  
*  
*   from HBO
*   from NPR

 
 
 
 
 

 