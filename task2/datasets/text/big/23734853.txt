Somewhere (film)
 
{{Infobox film
| name           = Somewhere
| image          = Somewhere_Poster.jpeg
| caption        = Film poster
| director       = Sofia Coppola
| producer       = {{Plainlist|
* Sofia Coppola
* Roman Coppola
* G. Mac Brown}}
| writer         = Sofia Coppola
| starring       = {{Plainlist|
* Stephen Dorff
* Elle Fanning
* Michelle Monaghan
* Chris Pontius
* Simona Ventura}} Phoenix
| cinematography = Harris Savides
| editing        = Sarah Flack
| studio         = Pathé Medusa Film Tohokushinsha Film American Zoetrope
| distributor    = Focus Features
| released       =  
| runtime        = 98 minutes 
| country        = United States
| language       = English
| budget         = $7 million   
| gross          = $13.9 million   
}} ennui among Hollywood stars, the father–daughter relationship and offers an oblique comedy of show business, particularly Hollywood film-making and the life of a "star".

Somewhere premiered at the 67th Venice International Film Festival where it received the Golden Lion award for best picture. Critical opinion was mildly positive. Reviewers praised the patience of the films visual style and its empathy for a handful of characters, but some found Somewhere to be too repetitive of themes in Coppolas previous work, or did not sympathize with the protagonist because of his relative success. It was released to select theaters in the United Kingdom and Ireland on December 10, 2010, and in the United States on December 22, 2010.

==Synopsis==
As the film opens a black  ), gives interviews to the press, and attends an award ceremony in Italy. Despite drinking and socializing occasionally with Sammy ( , December 21, 2010. Retrieved January 10, 2010. 

He receives an unexpected visit from his 11-year-old daughter, Cleo (Elle Fanning).    Her stay changes his lifestyle little at first, including his indulging an overnight visitor, a blonde woman. Johnny and his daughter spend time together in his hotel suite and he brings her with him on his daily routine and on a publicity trip to Milan (where he is awarded with a "Telegatto", in a show with local celebrities playing themselves), and through preparations for her departure to summer camp. As their time together grows, Johnnys fatherly emotions emerge and force him to re-assess his otherwise "successful" life. After Cleo leaves for camp Johnny calls his ex-wife and tearfully breaks down admitting his unhappiness at his empty life. His ex-wife is indifferent to his pain and declines his request to come see him. At the end, Johnny checks out of the hotel promising not to return, and drives his Ferrari into the countryside. He randomly stops and gets out, leaving the keys in the ignition, and walks down the highway smiling.

==Themes==

===Celebrity ennui=== Lost in Marie Antoinette (2006), a stylized biopic of the eponymous queen, examined her loneliness. Somewhere examines similar themes of success and isolation, but from a male perspective. The film explores Marcos seclusion and depression despite his outward professional success and the resulting wealth and fame. He appears to suffer from anhedonia, the inability to feel pleasure, but the film itself is reticent in suggesting its causes. "He believes hes nothing", summarized film critic Roger Ebert, "and it appears hes correct".  The films opening shot, a Ferrari circling a race-track in and out of a stationary camera position, its whine and roar rising and falling, establishes the theme of ennui. The sequences length also offers a visual cue from Coppola to relax, observe and withhold expectations.  Coppola said she wanted to hint at this with a simple camera set-up, "so youre alone with this guy and not aware that its a movie. But I hope its a welcome contrast to the style of most movies out there. Something that gives you a chance to take a breath". 

The Chateau Marmont, a well-known retreat for Hollywood celebrities, is the films setting and can be "either a paradise of easy wish-fulfillment or a purgatory of celebrity anomie" ( , December 21, 2010  Coppola has stayed at the hotel, and said "Ive seen a few Johnny Marcos"; in contrast, writing the part of the daughter, she drew on childhood experiences with her director father,  , January 4, 2011 

===Parenthood===
Coppola mentioned that the parental angle was inspired by the birth of her second child.  As the film progresses the "tender and temporary" father–daughter relationship comes to the fore.  Marco has partial custody of his daughter from a failed marriage. Ebert speculates that she probably understands the reasons for the split better than he, and wonders why the child must suffer his hedonism and "detached attempts at fatherhood".  In some ways Cleo—having grown up inside the Hollywood bubble—mothers her father, cooking for him and being more worldly aware, but she also watches him with the wide-eyed adoration of a child. 

===Comedy of show-business===
Coppola comes from a family of film-makers and actors, and she has said how childhood memories of living in hotels partially inspired her choice of setting. Somewhere presents a detailed portrait of life in that industry and charts its existential and emotional boundaries. While celebrity gossip websites inform us of the shallowness of much of "star life", Coppolas feature differs in its emotional depth.  She wanted to depict Marco working, but not on a film set. Instead he is shown giving interviews, being photographed, attending an awards ceremony in Italy, and having special effects make-up applied. When Marco attends the special-effects department his face is covered in    Marco is obliged to use his "star" recognition to help promote his new film, when his publicist calls he becomes passive and mechanically takes the arranged chauffeured car and speaks to the press.  In part, the humor derives from Marcos subdued response to these attentions, and Coppolas off-beat observation of people around him. McNab, G.   The Independent, (London:September 4, 2010)  At the Venice film festival, critics highlighted the repetition of characters in a cloistered existence in Coppolas films, to which she responded "I feel like everyone should tell what they know in the world that they know". Lim, Dennis   The New York Times, December 10, 2010 

==Production==
  Toby Dammit (1968)  has also been noted as an influence. Meanwhile, the parental focus of the film developed because Coppola had recently had her second child.  Coppola said that she thought of Dorff to play Marco early while writing the film, because he had an aura of "the bad-boy actor," but also "this really sweet, sincere side." 
 Bruce Webers Hollywood portraits and Helmut Newtons photographs of models at the Chateau Marmont, and Jeanne Dielman, 23 quai du Commerce, 1080 Bruxelles (1975), a film by Chantal Akerman about the routine of a Belgian housewife, with Harris Savides, the cinematography|cinematographer. He said, "The main thing was to tell the story really simply and let it play out in long beats and have the audience discover the moment."  Coppola used the lenses that her father had used to film Rumble Fish (1983) in an effort to give the film a more period look, although it is set in the present.   
 Paper Moon (1973) during production. 

Filming took place in Los Angeles and Italy in June and July 2009.   , December 15, 2010 

==Release==
Somewhere premiered at the 67th Venice International Film Festival on September 3, 2010,  and it was released in Italy on the same day. At the festivals close (September 11), the jury unanimously awarded Somewhere the Golden Lion, the festivals prize for the best overall film.     Quentin Tarantino, president of the jury, said the film "grew and grew in our hearts, in our minds, in our affections" after the first screening.  Focus Features distributed Somewhere in North America and most other territories. Pathé released the film in France on January 5, 2011, while Tohokushinsha distributed it in Japan. Medusa Film has rights in Italy. Somewhere was released on December 10, 2010 in both Ireland and the United Kingdom, and on December 22 in North America. 

===Critical response===
Somewhere received positive reviews and has a "certified fresh" score of 72% on   based on 40 reviews. 
 National Board of Review Awards, Sofia Coppola was given the Special Filmmaking Achievement Award for writing, directing and producing Somewhere. 

Roger Ebert, writing in the Chicago Sun-Times, awarded the film four out of four stars and praised the detail in the portrait of Johnny Marco, saying "Coppola is a fascinating director. She sees, and we see exactly what she sees."  A.O. Scott in The New York Times called the film "exquisite, melancholy and formally audacious" and said "This is not a matter of imitation, but rather of mastery, of finding — by borrowing if necessary — a visual vocabulary suited to the story and its environment. If you pay close attention, "Somewhere" will show you everything."  Peter Bradshaw disagreed in The Guardian, awarding the film two stars from five. He praised the cinematic technique but said that the film resembled Lost in Translation too closely, lacked emotional depth and that even on second viewing "the question of why we should really care or be interested remains tantalisingly unanswered"; the final shot failed to solve any emotional problems and "really is one of the daftest things I have seen for a long time." Bradshaw, Peter   The Guardian December 9, 2010  

AlloCiné|Allociné, a French cinema website, calculated a score of 2.9 stars out of 5 from twenty-six press reviews.  French newspaper Le Monde gave the film a positive review, saying Somewhere was Coppolas most minimalist and daring film. Coppolas films, it said, deal with "the delicate irony of the delinquency of a universe of the happy few", which is both to her credit and a ghost which haunts her, a loyalty ensnaring her.  France 24 said the "virtuosity of Coppola is also in her keeping empathy for the characters without pouring out mushy sentiment."  Richard Roeper said that Somewhere is one of the best films of 2010, ranking it number 10. 

===Box office=== Marie Antoinette The Virgin Suicides (1999). However, it was a lesser total taking.  In France, Somewhere earned   in the three weeks to January 25, 2011. 

==Soundtrack==
Phoenix (band)|Phoenix, a French rock band, contributed the films score. Coppola is married to Thomas Mars, the bands singer;  she liked the songs "Love Like a Sunset Part I" and "Love Like a Sunset Part II" and requested the band do similar music for the film.  
In 2010 the film score for Somewhere was announced, but remains unreleased.  Except for The Strokes song during the poolside scene, the score is Diegetic#In film|diegetic. For example, Cleo ice-skates to Gwen Stefani, and the twins pole-dance to the Foo Fighters. 

===Track listing=== Phoenix
#"Gandhi Fix" – William Storkson My Hero" – Foo Fighters
#"So Lonely" – The Police
#"1 Thing" – Amerie
#"20th Century Boy" – T. Rex (band)|T. Rex
#"Cool (Gwen Stefani song)|Cool" – Gwen Stefani
#"Che si fa" – Paolo Jannacci
#"Teddy Bear" – Romulo Kiss
#"Ill Try Anything Once" – Julian Casablancas
#"Look" – Sebastien Tellier
#"Smoke Gets In Your Eyes" – Bryan Ferry
#"Massage Music" – William Storkson
#"Love Like a Sunset Part II" – Phoenix

==References==
 

==External links==
* 
*  
*  
*  
*   at The Numbers
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 