Andaz Apna Apna
 
 
{{Infobox film
| name           = Andaz Apna Apna
| image          = Andaz Apna Apna.jpg
| caption        = Poster
| director       = Rajkumar Santoshi
| producer       = Vinay Kumar
| screenplay     = Rajkumar Santoshi  (screenplay & dialogues)  Dilip Shukla  (dialogues) 
| story          = Yaser Ranjha
| starring       = Salman Khan Aamir Khan Raveena Tandon Karisma Kapoor Paresh Rawal Shakti Kapoor
| music          = Tushar Bhatiah
| cinematography = Ishwar Bidri
| editing        = V. N. Mayekar
| distributor    = Vinay Pictures
| released       = 11 April 1994 
| runtime        = 177 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          =     
}}
 1994 Indian Govinda and Juhi Chawla make guest appearances. The film was released on 11 April 1994. The film was remade in Tamil as Ullathai Allitha. Even though the movie was an average grosser when it released, it has emerged as a cult film over the years.  The film was nominated for the 1995 Filmfare Awards in four categories; Best Film (Vinay Kumar Sinha), Best Director (Rajkumar Santoshi), Best Actor (Aamir Khan) and Best Comedian (Shakti Kapoor). 

==Plot==
Its a story of two men competing for the hand of an heiress get drawn into love – and into trying to save her from an evil criminal. Amar ( ), daughter of Ram Gopal Bajaj (Paresh Rawal). The guys run into each other on a bus and soon realise that they have a common goal. The duo fail at various attempts to woo the lady.

Finally, they decide to insinuate themselves in her house. Amar pretends to be a guy who has lost his memory after getting hit by Raveena, while Prem pretends to be a doctor. The boys dont know that Raveenas secretary Karishma (Karisma Kapoor) is the real Raveena and Raveenas actual name is Karishma. Raveena switched her identity because she wanted to find a boy who will love her, not her money.

Nobody is aware that Ram Gopal has a twin brother called Shyam Gopal Bajaj aka Teja. Teja is a criminal who has taken lots of money from Crime Master Gogo (Shakti Kapoor). Teja hopes to land the riches himself by kidnapping his brother and posing as Ram. He has also planted his cronies Robert (Viju Khote) and Bhalla (Shehzad Khan) in the household.

Ram arrives in India & Teja plans to steal Rams money converted in diamonds. Here, Ram sees through the real nature of Amar & Prem, thus declining Raveenas marriage to either of them. The duo plan to fake a kidnapping where they will heroically "rescue" Ram. Unknown to them, Teja has planned to kidnap Ram as well.

Teja succeeds in having Ram kidnapped. The boys go to rescue Ram, but Teja makes them believe that he is Ram and finally enters
Rams household. Initially, nobody suspects a thing, but the girls soon smell a rat. The boys have discovered real identities of the girls. Prem has fallen for real Raveena while Amar has fallen for real Karishma.

The girls tell their suspicions to the boys. The boys tail Teja and soon find out the truth. Here, Ram tricks Teja and escapes the prison. However, the boys mistake him for Teja, resulting in Ram being imprisoned again – with Amar & Prem. However, Amar & Prem succeed in convincing Robert & Bhalla that Ram is actually Teja.

The boys, along with Robert & Bhalla, stop Teja. However, Ram is kidnapped along with Raveena & Karishma. The climax takes place in Gogos lair, where the boys try to control the situation along with Ram. In a comic standoff, the real motive of each villain is revealed. However, due to the smartness of the boys, police raid Gogos lair, thus rounding up all the criminals. Ram finally decides to have the girls married to Amar & Prem.

==Cast and crew==

===Cast===
* Aamir Khan as Amar Manohar
* Salman Khan as Prem Bhopali
* Raveena Tandon as Karishma/Raveena
* Karishma Kapoor as Raveena/Karishma
* Paresh Rawal as Teja aka Shyam Gopal Bajaj/Ram Gopal Bajaj
* Shakti Kapoor as Crime Master Gogo
* Viju Khote as Robert
* Shehzad Khan as Vinod Bhalla
* Jagdeep as Bankelal Bhopali (Prems dad)
* Deven Verma as Murli Manohar (Amars dad) Mehmood as Johnny from Wah-Wah Productions Javed Khan as Anand Akela
* Ali Jut as Inspector ali
* Tiku Talsania as Inspector
* Harish Patel as SevaRam ji
* Juhi Chawla as herself (Guest appearance) Govinda as himself (Guest appearance)

===Crew===
* Singers – Asha Bhosle, Sadhana Sargam, Sapna Mukherjee, Abhijeet Bhattacharya, S.P. Balasubramaniam, Vicky Mehta, Mangal Singh
* Lyricist – Majrooh Sultanpuri
* Music Director – Tushar Bhatia
* Choreography – Saroj Khan

==Reception==
The film didnt do well at box-office, it recovered its cost mainly due to business from big cities, but didnt turn out to be a massive hit contrary to everyones expectations. The major reason was the film was way ahead of its time and mid 90s was not a time for start to finish full comedy movies that involves no emotion, no drama or no seriousness at all. Also, it faced tough competition from other movies of that year which included classic blockbusters like   etc. All of these movies were very strong in their box office presence and Andaz Apna Apna, belonging to a different genre, could not compete well. However, in subsequent years, it has achieved a cult classic status among Indian Hindi audiences.  Each dialogue and character of the movie are immensely likeable and have terrific recall. On the video circuit and TV, it has become one of the most popular Indian movies over the years. Its hard too believe that the film was failure at the box despite its strong script.Its all time film of bollywood film industries.

Such is the cult following of this movie that many people have memorized the dialogues of this movie like "Teja main hu. Mark Idhar hai", " Crime Master Gogo, Mogambo ka Bhatija", " Do dost ek cup me chay piyenge" etc. Many times people use the dialogues from this movie to prepare trolls and publish them on social networking sites. In fact there is a hotel in the city of Pune, Maharashtra by the names of Andaz Apna Apna (where Andaz is plural for the word Anda which means egg in Hindi. Even the menu of this popular eatery is named after famous dialogues of the movie. The hotel serves around 40+ dishes of egg and is very popular among the College going as well as working crowd of the city. 

==Soundtrack==
The film has four songs composed by Tushar Bhatia with lyrics by Majrooh Sultanpuri.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s) !! Length
|-
| 1
| "Do Mastanae"
| S. P. Balasubrahmanyam, Debashish Dasgupta
| 06:04
|-
| 2
| "Dil Karta Hai" Mangal Singh
| 04:59
|-
| 3
| "Ye Raat Aur Ye Doori"
| S. P. Balasubrahmanyam, Asha Bhosle
| 05:12
|-
| 4
| "Ello Ello"
| Vicky Mehta, Behroze Chaterjee
| 05:54
|-
| 5
| "Jaana Tune Jaana Nahin" * 
| Abhijeet Bhattacharya, Sadhana Sargam
| 04:35
|-
| 6
| "Shola Shola Tu Bhadke" * 
| S. P. Balasubrahmanyam, Sapna Mukherjee
| 05:02
|}

 *  Song was not included in the movie though appearing on the soundtrack.

==Remake==
Andaz Naya Naya is a Bollywood animated 3D film, which is a official remake of Andaz Apna Apna, but it has been shelved due to some reason.  Producer Sidharth Jain began to question if the film would do any good in the box office because Indian animated films do not get good market, so Jain shelved the film after being 35% done. 

==Sequel==
On the completion of 20 years of Andaz Apna Apna, director Rajkumar Santoshi said in an interview that he is considering sequel of Andaz Apna Apna as demand for sequel is unbelievable. He said that bringing Aamir Khan and Salman Khan together may be economically and logistically impossible, so he may take young actors to reprise roles of Amar and Prem. 

But On 20 June 2014  Director Rajkumar Santoshi, who is working on the
sequel of Andaz Apna Apna, says both Salman Khan and Aamir Khan are ready to work in the
second installment, and that he will repeat them if they produce the movie. 

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 