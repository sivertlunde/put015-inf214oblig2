Boxes (film)
{{Infobox film
| name           = Boxes
| image          =Boxesboiteposter.jpg
| caption        =Promotional poster
| director       =Jane Birkin
| producer       = Emmanuel Giraud	
| writer         = Jane Birkin
| starring       = Geraldine Chaplin Michel Piccoli Jane Birkin Natacha Régnier Lou Doillon Adèle Exarchopoulos John Hurt Maurice Bénichou Tchéky Karyo
| music          = Frank Eulry
| cinematography = François Catonné
| editing        = Marie-Josée Audiard	 		 	
| distributor    = 
| released       =France 6 June 2007
| runtime        = 95 min
| country        =France
| language       = French English
| budget         =
| gross          =
}} French film and the directorial debut of Jane Birkin. Birkin also stars alongside Geraldine Chaplin and Michel Piccoli. The film is based on  Birkins own family life, chronicling three marriages and the three children she bore from these marriages. The title alludes to the way in which she compartmentalises  these relationships and stages of her life.  The film was nominated for the Grand Prix at the Bratislava International Film Festival. The film was screened in Un Certain Regard at the 2007 Cannes Film Festival on 21 May.  It was released in France on 6 June 2007.

==Plot==
In Brittany, a middle-aged woman, Anna lives in a rambling home with her sometime dead father (Piccoli), her opinionated mother (Chaplin) and the memories of her three grown-up daughters. As Anna struggles with her mid-life crisis, the possessions and photographs in the home begin to spark her memories of childhood and earlier adulthood.  
 English father (Hurt) failed and as a consequence, Fanny (Régnier) barely knows him. Fannys half-sister is Camille (Doillon), who Anna had with Camilles now dead father, Max (Benichou). There is also her third husband, Jean (Karyo), with whom she had Lilly (Exarchopoulos), but he left to pursue affairs. 

==Cast==
*Geraldine Chaplin as Mother
*Michel Piccoli as Father
*Jane Birkin as Anna
*Natacha Régnier as Fanny
*Lou Doillon as Camille
*John Hurt as Fannys father
*Maurice Bénichou as Max
*Tchéky Karyo as Jean
*Adèle Exarchopoulos as Lilli
*Annie Girardot as Joséphine

==Production==
Birkin wrote the script for Boxes in 1995, shortly after her fathers death.  The film was shot in Birkins own family home in Landéda.  Birkin offered the main role of Anna to Chaplin, but Chaplin turned this down, feeling that as the role was written for a 45-year old woman, she would be better suited to the role of Annas mother. 

==Reception==
Le Figaro praised the "worrying, unpredictable and touching sincerity" of the film.  Michèle Levieux of LHumanité said that the film was so well-written, it deserved to be adapted for the stage. Levieux, Michèle. Quest-ce qui fait courir Jane ?. LHumanité. 28 December 2007 

==References==
 

==External links==
*  

 
 
 
 
 