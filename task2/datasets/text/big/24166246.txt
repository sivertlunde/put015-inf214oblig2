Song of Paris
{{Infobox film
| name           = Song of Paris
| image          =
| image_size     =
| caption        =
| director       = John Guillermin
| writer         = Allan MacKinnon
| narrator       =
| starring       = Dennis Price
| music          =
| cinematography =
| editing        = Sam Simmonds
| distributor    =
| released       = 11 February 1952 (UK)   17 April 1953 (US)
| runtime        = 83 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}} 1952 Cinema British comedy film directed by John Guillermin and starring Dennis Price, Anne Vernon and Hermione Baddeley. It is also known as Bachelor in Paris.

==Cast==
* Dennis Price as Matthew Ibbetson  
* Anne Vernon as Clementine  
* Mischa Auer as Comte Marcel de Sarliac  
* Hermione Baddeley as Mrs. Ibbetson  
* Joan Kenny as Jenny Ibbetson   Brian Worth as Jim Barrett   Michael Ward as Waterson  
* Richard Wattis as Carter  
* Kynaston Reeves as Vicar   Roger Maxwell as Weldon
* Bernard Rebel as Lebrun
* Tessa Prendergast as Seven Veils dancer

==External links==
* 

 

 
 
 
 
 
 

 