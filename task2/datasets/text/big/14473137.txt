Pretty Bird
{{Infobox film
| name           = Pretty Bird
| caption        = 
| image	         = Pretty Bird FilmPoster.jpeg
| alt            =  Paul Schneider
| producer       = 
| writer         = Paul Schneider 
| starring       = {{Plainlist | 
* Billy Crudup
* Paul Giamatti
* Kristen Wiig
* David Hornsby
* Garret Dillahunt
* Denis OHare
}}
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = United States
| budget         = $3.75 million
| gross          = 
}}
Pretty Bird is a 2008 American comedy film. It competed in the Dramatic Competition at the 2008 Sundance Film Festival.   

==Plot== RB2000 Rocket Belt in 1995.

==Reception==
  
Rotten Tomatoes gives the film a score of 20% based on reviews from 5 critics. 
 Variety criticized the film as being "overly calculated" and predictable, noting that the only bright spot in the film was its soundtrack.   Scott Weinberg of Cinematical writes that the movies ending is anything but "pat, obvious, or predictable," and goes on to say that it is worth seeing on the strength of the actors performances alone. 

  
It was nominated for the Grand Jury Prize at Sundance Film Festival|Sundance,  but did not win.

==Cast==
* Billy Crudup - Curtis Prentiss
* Paul Giamatti - Rick Honeycutt
* David Hornsby - Kenny Owenby
* Kristen Wiig - Mandy Riddle
* Elizabeth Marvel - Tonya Honeycutt
* Denis OHare - Chuck Stutters
* Garret Dillahunt - Carson Thrash
* James Wetzel - Dennis
* Anna Camp - Becca French
* Nate Mooney - Randy Pendler
* Aasif Mandvi - Ted the Banker
* Adam Lefevre - Phil the Neighbor
* Lennon Parham - Woman with Beef

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 