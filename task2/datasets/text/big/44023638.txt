Sethubandhanam (film)
{{Infobox film 
| name           = Sethubandhanam
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = R Somanathan
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi
| starring       = Prem Nazir Sukumari Jayabharathi Adoor Bhasi
| music          = G. Devarajan
| cinematography = JG Vijayam
| editing        = K Sankunni
| studio         = Soorya Pictures
| distributor    = Soorya Pictures
| released       =  
| country        = India Malayalam
}}
 1974 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by R Somanathan. The film stars Prem Nazir, Sukumari, Jayabharathi and Adoor Bhasi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir
*Sukumari
*Jayabharathi
*Adoor Bhasi Prema
*Sumathi Baby Sumathi
*Bahadoor Meena
*Sadhana Sadhana

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kasthoori Gandhikal (Om Namo Narayananaya) || K. J. Yesudas, P. Madhuri, Ayiroor Sadasivan || Sreekumaran Thampi || 
|-
| 2 || Manjakkilee Swarnakkilee || Latha Raju || Sreekumaran Thampi || 
|-
| 3 || Munkopakkaari || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 4 || Pallavi Paadi || K. J. Yesudas, P. Madhuri || Sreekumaran Thampi || 
|-
| 5 || Pidakkozhi Koovunna || K. J. Yesudas, Chorus || Sreekumaran Thampi || 
|-
| 6 || Pinchu Hridayam || P. Madhuri, Chorus || Sreekumaran Thampi || 
|-
| 7 || Pinchuhridayam || Latha Raju || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 