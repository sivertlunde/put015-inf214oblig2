Winter in the Woods
 
{{Infobox film
| name           = Winter in the Woods
| image          =
| caption        =
| director       = Wolfgang Liebeneiner
| producer       = Willie Hoffmann-Andersen Fritz Hoppe Alf Teichs
| writer         = Frank Dimen Alf Teichs Werner P. Zibaso
| starring       = Claus Holm
| music          =
| cinematography = Bruno Mondi
| editing        = Martha Dübber
| distributor    =
| released       = 26 March 1956
| runtime        = 97 minutes
| country        = Germany
| language       = German
| budget         =
}}

Winter in the Woods ( ) is a 1956 German drama film directed by Wolfgang Liebeneiner and starring Claus Holm.   

==Cast==
* Claus Holm - Martin
* Sabine Bethmann - Marianne
* Rudolf Forster - Baron Malte
* Helen Thimig - Baronin Henny
* Willy A. Kleinau - Verwalter Stengel
* Susanne Cramer - Inge Sternitzke
* Gert Fröbe - Gerstenberg
* Klaus Kinski - Otto Hartwig
* Ilse Steppat - Frieda Stengel
* Erica Beer - Simone
* Herbert A.E. Böhme - Sternitzke
* Beppo Brem - Huber
* Karl Hellmer - Kruttke
* Margarete Haagen - Kraeutermarie
* Alexander Engel - Seifert
* Otz Tollen - Diener Baumann
* Joachim Boldt - Kuhn
* Uwe Witt - Franz Sternitzke Fritz Wagner

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 