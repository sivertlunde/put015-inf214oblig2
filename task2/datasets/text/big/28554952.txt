Unos Pocos con Valor
 
{{Infobox film
| name           = Unos Pocos con Valor
| image          = Unospocosconvalor.jpg
| caption        = Theatrical release poster
| director       = Douglas Martin
| producer       = Ismael Bevilacqua
| writer         = Mario Berrios
| starring       = Alvaro Matute Oscar Izaguirre Jenifer Sosa Anuar Vindel Jose Moncada Jakeline Salgado Jorge Colindres Jose Barrientos
| music          = Luis Fernando Brenes
| cinematography = 
| editing        = 
| distributor    =
| released       =  
| runtime        =
| country        = Honduras
| language       = Spanish
| budget         = 
| gross          = 
}}
Unos Pocos con Valor ( ) is a Honduran film based on the book Los Pájaros de Belén ( ) by Mario Berrios, released in Honduras on August&nbsp;20th 2010. Based on real life events, the story follows a group of special forces police officers who come up against the most dangerous gang in Honduras.

Directed by Costa Rican Douglas Martin, the films executive producers were Ismael Bevilacqua and Marisol Santos alongside Associate Producer Kattia Dominguez. Sixteen foreigners were in charge of camera direction and special effects and it was shot on location in San Pedro Sula, Tegucigalpa, Siguatepeque, La Lima and El Progreso, Yoro.

The whole post-production took place in Costa Rica, on the now closed post company called Dart, everything was done on Final Cut Pro and Later moved to an Avid DS for its mastering.

Something interesting on this process is that the post, including finishing the edit, color correction, sound design and mastering was done in a period of 5 days by a small post team (3 people).

The main Honduran cast consisted of 13 policemen, 13 criminals and eight women representing the wives of criminals, victims of abduction and secretaries, along with 21 supporting actors and four extras.

==Cast==
* Alvaro Matute as Mauricio Barrientos - Pajaro 10 &ndash; Honduran Police Captain.
*Oscar Izaguirre as Moises Bustamante &ndash; A delinquent.
* Juan Funes as Pájaro 70 &ndash; Honduran Police.
*Angel Funes as Chekko &ndash; Young Delinquent.
*Juan Funes Padre as Andres &ndash; Abducted sons father.
*Jenifer Sosa as Nadia Dipalma &ndash; Abductee
*Anuar Vindel as Agent &ndash; from San Pedro Sula.
*Jose Moncada as Eleazar Torres &ndash; Specialist 1.
*Jakeline Salgado as Marcia Sosa De Uribe.
*Jorge Colindres as Chele Dinamita &ndash; A delinquent.
*Jose Barrientos as Rodriguez &ndash; Deputy Commissioner.
*Mauricio Alberto Sanchez Velez &ndash; A delinquent.
*Thania Zabala as Maria Isabel Maurtua Dayani.
*Javier Mugartegui as Saul Lozano &ndash; A lame man.
*Victor Fajardo as Salomon Bustamante &ndash; A delinquent.
*Xiomara Mejia as Yolanda Paz &ndash; Yoli.
*Henry Quiroz as Ramos &ndash; Pajaro 20.
*Federico Valdez as Francisco Caceres &ndash; Don Lente
*Andrea Reyes as Paola Robleda &ndash; Daughter. 
*Daniel Fernandez as Abraham Bastamente &ndash; A delinquent.
*Carmen Pineda as pregnant woman &ndash; Victim in the opening scene.

==References==
  
 
 
 
 

==External links==
*  

 
 
 
 
 

 