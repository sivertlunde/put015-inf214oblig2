Kaalam Kaathu Ninnilla
{{Infobox film 
| name           = Kaalam Kaathu Ninnilla
| image          =
| caption        =
| director       = AB Raj
| producer       = TK Balachandran
| writer         = Thoppil Bhasi
| screenplay     = Thoppil Bhasi
| starring       = Prem Nazir Jayabharathi Sankaradi
| music          = A. T. Ummer
| cinematography = PB Mani
| editing        = K Narayanan
| studio         = Teakebees
| distributor    = Teakebees
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by AB Raj and produced by TK Balachandran. The film stars Prem Nazir, Jayabharathi and Sankaradi in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Prem Nazir
*Jayabharathi
*Sankaradi

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Mankombu Gopalakrishnan and Chirayinkeezhu Ramakrishnan Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bhagavathippattuduthu || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-
| 2 || Kanwa kanyake || Jolly Abraham || Chirayinkeezhu Ramakrishnan Nair || 
|-
| 3 || Manjalakalil || Vani Jairam || Mankombu Gopalakrishnan || 
|-
| 4 || Mavelippaattinte || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-
| 5 || Punchiriyo || P Jayachandran || Mankombu Gopalakrishnan || 
|-
| 6 || Swargamundenkil || Vani Jairam || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 