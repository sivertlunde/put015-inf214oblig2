Dummy (film)
{{Infobox film 
| name = Dummy 
| image = Dummyposter.jpg
| caption = Theatrical release poster
| director = Greg Pritikin 
| writer = Greg Pritikin 
| starring = Adrien Brody Milla Jovovich Illeana Douglas Vera Farmiga Jared Harris 
| producer = Bob Fagen Richard Temtchine
| music = Paul Wallfisch
| cinematography = Horacio Marquinez
| editing = William Henry Michael Palmerio
| studio = Quadrant Entertainment Dummy Productions LLC
| distributor = Artisan Entertainment
| released =  
| runtime = 91 minutes 
| language = English 
| gross = $71,646   
}}
Dummy is a 2002 comedy-drama film written and directed by Greg Pritikin. It stars Adrien Brody, Milla Jovovich, Illeana Douglas, Vera Farmiga and Jared Harris. The film had its premiere on February 21, 2002, and was released to theaters in the United States on September 12, 2003. It was released to home media by Lionsgate Home Entertainment on February 17, 2004.

==Plot summary==
Steven Schoichet is a recently unemployed neer-do-well who has difficulty expressing himself. Steven finds he has a knack for ventriloquism. Stevens best friend is Fangora "Fanny" Gurkel, an aspiring punk rock singer who, along with Steven, is just looking for her niche. Eventually, Fanny takes a shine to klezmer music when she learns of an opportunity to get an actual gig. Through his newfound talent, Steven discovers that he is able to overcome his social problems through his dummy and decides to try impressing and winning the heart of Lorena Fanchetti.

==Main cast==
* Adrien Brody as Steven Schoichet
* Milla Jovovich as Fangora "Fanny" Gurkel
* Illeana Douglas as Heidi Schoichet
* Vera Farmiga as Lorena Fanchetti
* Jessica Walter as Fern Schoichet
* Ron Leibman as Lou Schoichet
* Jared Harris as Michael Foulicker
* Helen Hanft as Mrs. Gurkel
* Lawrence Leritz as The Groom
* Poppi Kramer as The Bride
* Mirabella Pisani as Bonnie

==Production==

===Development===
The film was written and directed by filmmaker Greg Pritikin. It was produced by Bob Fagan, Richard Temtchine, Dummy Productions LLC and Quadrant Entertainment. Artisan Entertainment distributed the film to theaters and Lionsgate Home Entertainment distributed the film through home media releases. 

===Casting===
In May 2000, it was reported by Variety (magazine)|Variety that Adrien Brody and Milla Jovovich had been cast in the film, Brody as the lead character of Steven, and Jovovich as the best friend of Brodys character.  The following month, in June 2000, it was announced that Illeana Douglas had joined the cast in a supporting role.  Vera Farmiga, Ron Leibman, Jared Harris and Jessica Walter were also cast in supporting roles.

===Filming===
Principal photography took place in New York City, Wayne, New Jersey, and Long Island, New York in July and August 2000.

==Reception==

===Box office===
The film was given a limited release in the United States. Dummy made $30,130 from 5 theaters in its opening weekend.  It made an additional $41,516 at the box office for a total domestic gross of $71,646. 

===Critical reception===
Dummy received mostly positive reviews from film critics. Review aggregator website   based on 12 critical reviews. 

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 
 