Normal Adolescent Behavior
{{Infobox Film
| name           = Normal Adolescent Behavior
| image          = Normal Adolescent Behavior.jpg
| image_size     = 
| caption        = 
| director       = Beth Schacter
| producer       = 
| writer         = Beth Schacter
| narrator       = 
| starring       = Amber Tamblyn Ashton Holmes Raviv Ullman
| music          = Craig Deleon
| cinematography = Harlan Bosmajian
| editing        = Benjamin Meyer
| distributor    = New Line Cinema
| released       =  
| runtime        = 95 min.
| country        = United States
| language       = English]
| budget         = 
}}
Normal Adolescent Behavior is a 2007 drama film written and directed by Beth Schacter. The film was an official selection of the 2007 Tribeca Film Festival.

The film is the story of a group of best friends, all of whom are in a six-way polyfidelitous relationship. They feel that being with this group—and only this group—is more fulfilling and overall better than conventional teenage dating. However, Wendy (Amber Tamblyn) begins to question the arrangement after meeting the boy next door.
 Lifetime Television on September 1, 2007. The film was run throughout the month and was still being aired in 2011.

==Plot==
Wendy, Billie, and Ann are seniors at an alternative private school; they spend all their time with fellow students Jonah, Price, and Robert. The six have been friends since elementary school and their friendship has become a six-person polyamorous relationship. They swap sex partners each week; their loyalty is to the group, not to one person. 

After school orientation, Wendy meets Sean, a new senior who moved from Chicago; he finds out about her "inner geek", and she quickly recognizes a kindred spirit. Soon their friendship becomes romantic, and Wendy is torn between her genuine affection and desire for Sean, and her commitment and belief in the group. 

Wendy starts to test the boundaries of her vow to her friends, and Billie realizes that she is going to have to fight to keep her best friend; which shouldnt be a problem, since each of the teens has collected a box full of mementos from their sexual experience. 

If Wendy tries to leave, Billie can destroy her reputation and relationship in a heartbeat. While Wendy is trying to decide between Sean and her friends, Wendys younger brother Nathan meets Seans mother, Helen, who is waiting a long three weeks to start a new job. Nathan develops a huge crush on her, and he uses his considerable cooking skills to try and woo her.

Then Wendy spends another Saturday night with the group, and she is unable to "cheat" on Sean; her hesitation is all that Billie needs to accuse her best friend. After the girls fight, Wendy takes out her frustration on a random couple who keep making out in front of her house. Ryan, hearing that Wendy is out of the group, tries to be friends, but Wendy doesnt like what Ryan has become—a subservient wet dishrag of a girlfriend. 

Sean and Wendy try to be a "normal" couple, but the group quickly tests the relationship: Billie puts the box of memorabilia in Seans locker. Wendy claims she isnt scared; she knows that Sean loves her. Maybe so, but when he sees the photos that document a lifetime of sexual experimentation, he is fed up and offers an ultimatum: Wendy can burn the box, leave the group, and be a normal girl, or they can break up. Wendy has gone from one ultimatum to another. 

When Wendy sees Billie being tortured for being alone by other students, she wants to comfort her friend. Billie invites Wendy back to the group; such is the depth of Billies love for her. When Billie realizes that Wendy really might leave, she tests her—pushing Wendy to publicly ridicule Ryan, a girl who left the group last year along with her boyfriend, Aaron. 

Sean witnesses the brutal scene, where Ryans past is used against her. Wendy is devastated that she allowed herself to be pushed around by Billie. But Wendy makes a braver and stronger decision—to be alone, along the way revealing that all of the boys in the group had cheated on the girls at some point. Price went away for Spring Break and slept with someone else, subsequently giving the entire group crabs. 

Jonah and Robert are in love with each other, and have been sleeping together for three years. The girls in the story—Wendy, Ryan, Ann, and Billie—all must face the next chapter of their lives alone, and for the first time, at least Wendy sees the possibilities this offers.

==Partial cast==
* Amber Tamblyn as Wendy Bergman
* Ashton Holmes as Sean Mayer
* Raviv Ullman as Price
* Kelli Garner as Billie
* Daryl Sabara as Nathan Bergman
* Hilarie Burton as Ryan
* Stephen Colletti as Robert
* Kelly Lynch as Helen
* Julia Garro as Ann
* Edward Tournier as Jonah

==Alternate title==
 
On October 16, 2007,  .  In television airings, and many foreign DVD releases, the original title was used unchanged, including airings after the Region 1 DVD release.

In response to the name change, the director deleted both the films promotional website, and its MySpace page. 

==References==
 

==External links==
*  

 
 
 
 
 