Bhaaratheeyam
{{Infobox film
| name = Bhaaratheeyam
| image =
| caption =
| director = Suresh Krishnan
| producer =
| writer = Suresh Krishnan
| screenplay = Madhupal
| starring = Suresh Gopi Kalabhavan Mani Maniyanpilla Raju Suhasini
| music = Berny-Ignatius
| cinematography = MJ Radhakrishnan
| editing = N Gopalakrishnan
| studio = Payva International
| distributor = Payva International
| released =  
| country = India Malayalam
}}
  1997 Cinema Indian Malayalam Malayalam film, directed by Suresh Krishnan. The film stars Suresh Gopi, Kalabhavan Mani, Maniyanpilla Raju and Suhasini in lead roles. The film had musical score by Berny-Ignatius.  
 

==Cast==
  
*Suresh Gopi 
*Kalabhavan Mani 
*Maniyanpilla Raju 
*Suhasini 
 

==Soundtrack==
The music was composed by Berny-Ignatius. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kukku Kukku Kookippaadan || M. G. Sreekumar, Chorus || Gireesh Puthenchery || 
|- 
| 2 || Makayiram || Swarnalatha || Gireesh Puthenchery || 
|- 
| 3 || Saayaahna Meghathin   || KS Chithra || Gireesh Puthenchery || 
|- 
| 4 || Saayaahna Meghathin   || Ravi Krishnan || Gireesh Puthenchery || 
|- 
| 5 || Swathanthra Bhaaratha Dharani || M. G. Sreekumar, Chorus || Gireesh Puthenchery || 
|}

==References==
 

==External links==
*  

 
 
 


 