The Queens (film)
{{Infobox film
| name     = The Queens
| image    = The Queens film poster.jpg
| caption  =  traditional = 我是女王
|                          simplified = 我是女王
|                              pinyin = Wǒ Shì Nǔwāng}}
| director = Annie Yi
| producer = Annie Yi Ann An
| writer   = Annie Yi Wang Anan Gu Yi
| starring = Song Hye-kyo   Shawn Dou   Joe Chen   Tony Yang   Vivian Wu   Jiang Wu   Joe Cheng   Qin Hao   Annie Yi
| music    = 
| cinematography = Mark Lee Ping Bin
| editing  = 
| studio   = DSIM
| distributor = DSIM Levp
| released =  
| runtime = 106 minutes
| country = China
| language = Mandarin
| budget = 
| gross = US$2.46 million (China)   

}}
The Queens is an upcoming 2015 Chinese romance film based on the novel of the same name. The film is directed by Annie Yi and produced by Annie Yi and Ann An.  It stars Song Hye-kyo, Shawn Dou, Joe Chen, Tony Yang, Vivian Wu, Jiang Wu, Joe Cheng, Qin Hao, and Annie Yi. The Queens is scheduled for release in 16 April 2015.  

==Cast==
* Song Hye-kyo as Annie 
* Shawn Dou as Mark
* Joe Chen as Candy 
* Tony Yang as Tony
* Vivian Wu as Tina
* Jiang Wu as Jiawei
* Joe Cheng as Wang Ziyu
* Qin Hao as Zhang Yi
* Annie Yi as Melissa

==Production==
Filming took place in Beijing and Shanghai.

==Release==
The film was originally to be released on 7 November 2014 but was pushed back to 16 April 2015. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 
 