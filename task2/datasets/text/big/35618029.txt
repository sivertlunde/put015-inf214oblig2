No (2012 film)
 
{{Infobox film
| name           = No
| image          = No (2012 film).jpg
| caption        = Theatrical release poster
| director       = Pablo Larraín
| producer       = {{Plain list | 
* Daniel Marc Dreifuss
* Pablo Larraín
* Juan de Dios Larraín
}}
| screenplay     = Pedro Peirano
| based on       =  
| starring       = Gael García Bernal
| music          = Carlos Cabezas
| cinematography = Sergio Armstrong
| editing        = Andrea Chignoli
| studio         = Participant Media
| distributor    = Sony Pictures Classics  
| released       =  
| runtime        = 118 minutes  
| country        = {{Plain list | 
* Chile
* France
* United States
}}
| language       = Spanish
| budget         = 
| gross          = $2.4 million 
}} 1988 plebiscite of the Chilean citizenry over whether dictator Augusto Pinochet should stay in power for another eight years.
 Best Foreign Language Film Oscar.   

==Plot== national plebiscite of 1988 on whether General Augusto Pinochet should stay in power for another eight years or whether there should be an open democratic presidential election the next year.

René Saavedra, a successful advertisement creator, is approached by the "No" side committee to consult on their proposed advertising. Behind the back of his politically conservative boss, Saavedra agrees to come and finds that the advertising in question is a dourly unappealing litany of the regimes abuses created by an organization that has no confidence in its efforts. Enticed with this marketing challenge and his own loathing of Pinochets tyranny, he proposes with the advertising subcommittee to take a lightheartedly upbeat promotional approach stressing abstract concepts like "happiness" to challenge concerns that voting in a referendum under a notoriously brutal military junta would be politically meaningless and dangerous.

Although Saavedra, his son and his comrades are eventually targeted for intimidation by the authorities while the unorthodox marketing theme is dismissed by some No members as a facile dismissal of the regimes horrific abuses, the proposal is approved for the campaign. Eventually, Saavedras boss, Lucho, finds out about his employees activities, but when Saavedra refuses an offer to become a partner if he withdraws, Lucho goes to head the "Yes" campaign as a matter of survival.

The campaign took place in 27 nights of television advertisements, in which each side had 15 minutes per night to present its point of view. Over that month, the "No" campaign, created by the majority of Chiles artistic community, proves effective with a series of entertaining and insightful presentations that have an irresistible cross-demographic appeal. By contrast, the "Yes" campaigns advertising, having only dry positive economic data in its favor and few creative personnel on call, is derided even by government officials as repellently crass and heavy-handed.

Although the government tries to interfere with the "No" side with further intimidation and blatant censorship, Saavedra and his team use those tactics to their favor in their marketing and public sympathy shifts to them. As the campaign heats up in the concluding days with the "No" following up with international Hollywood celebrity spots and wildly popular street concert rallies, even police attacks cannot discourage the "No" campaign while the "Yes" side is reduced to desperately parodying the "No" ads.

On the day of the referendum, it momentarily appears that the "Yes" vote has the lead, but the final result turns out to be firmly on the "No" side. The final proof only comes when the troops surrounding the No headquarters strangely withdraw as the news of the Chilean senior military command forcing Pinochet to concede comes through. After the success, Saavedra, undecided as to what to think about it, and his boss resume their normal advertising business with a new Chile being born.

==Cast==
* Gael García Bernal as René Saavedra
* Alfredo Castro as Luis "Lucho" Guzmán
* Luis Gnecco as José Tomás Urrutia
* Antonia Zegers as Verónica Carvajal
* Marcial Tagle as Costa
* Néstor Cantillana as Fernando Arancibia
* Jaime Vadell as Sergio Fernández
* Sergio Hernández
* Alejandro Goic as Ricardo
* Diego Muñoz
* Paloma Moreno
* Richard Dreyfuss, Jane Fonda, Christopher Reeve, and Augusto Pinochet as themselves in archive footage
* Patricio Aylwin, Patricio Bañados, Carlos Caszely and Florcita Motuda acting as themselves and also appearing in archival footage

==Release==
At the Telluride Film Festival, the film was shown outdoors and was rained on.  It was also screened at the Locarno Film Festival in Switzerland.  No played as a Spotlight selection at the Sundance Film Festival.  Gael García Bernal attended the Toronto International Film Festival where No was screened. 
 Network Releasing on 8 February 2013. 

==Reception==

===Praise===
Review aggregation website Rotten Tomatoes gives the film a 93% rating based on 117 reviews. 
 Time Out New York critic David Fear called No "the closest thing to a masterpiece that Ive seen so far here in Cannes".  Variety (magazine)|Variety reviewer Leslie Felperin felt the film had the "potential to break out of the usual ghettos that keep Latin American cinema walled off from non-Hispanic territories. ....with the international success of Mad Men, marketing campaigners should think about capitalizing on viewers’ fascination everywhere with portraits of the advertising industry itself, engagingly scrutinized here with a delicious, Matthew Weiner-style eye for period detail." 

One of the unique features of the film was Larraíns decision to use low definition, ¾ inch Sony U-matic magnetic tape, which was widely used by television news in Chile in the 80s. The Hollywood Reporter argues that this decision probably lessened the films chances "commercially and with Oscar voters."    The Village Voice reviewer commented that the film "allows Larrains new material to mesh quite seamlessly with c. 1988 footage of actual police crackdowns and pro-democracy assemblages, an accomplishment in cinematic verisimilitude situated anxiously at the halfway point between Medium Cool and Forrest Gump." 

===Criticism===
The film received a mixed reception in Chile.  Several commentators, including Genaro Arriagada, who directed the "No" campaign, accused the film of simplifying history and in particular of focusing exclusively on the television advertising campaign, ignoring the crucial role that a grassroots voter registration effort played in getting out the "No" vote. Larraín defended the film as art rather than documentary, saying that "a movie is not a testament. It’s just the way we looked at it." 

In another criticism, a Chilean political science professor asked if one should really celebrate the moment that political activism turned into marketing, rather than a discussion of principles. 

===Accolades===
  
When screened  at the 2012 Cannes Film Festival,   No won the Art Cinema Award,  the top prize in the Directors Fortnight section. 
 nominated on 10 January 2013.      

At the 2012 Abu Dhabi Film Festival, Bernal won the award for Best Actor.   

{| class="wikitable" 
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Accolades
|- style="background:#ccc; text-align:center;"
! Award / Film Festival
! Category
! Recipients
! Result
|- Academy Awards Academy Award Best Foreign Language Film Pablo Larraín
| 
|- Cannes Film Festival International Confederation Art Cinema Award Pablo Larraín
| 
|- Cinema for Cinema for Peace Awards Cinema for Peace Award for Justice Pablo Larraín
| 
|- Hamburg Film Festival Art Cinema Award Pablo Larraín
| 
|- Havana Film Festival Best Film Pablo Larraín
| 
|- BFI London Film Festival Best Film Pablo Larraín
| 
|- National Board of Review Top Five Foreign Language Films
|
| 
|- Films from the South Best Feature Pablo Larraín
| 
|- Abu Dhabi Film Festival Best Actor Gael García-Bernal
| 
|-
|São Paulo International Film Festival Best Foreign Language Film Pablo Larraín
| 
|- Thessaloniki International Film Festival Open Horizons Pablo Larraín
| 
|- Tokyo International Film Festival Tokyo Grand Prix Pablo Larraín
| 
|- Altazor Award Best Fiction Director Pablo Larraín
| 
|- Best Actor Jaime Vadell
| 
|- Best Screenplay Pedro Peirano
| 
|-
|St. Louis Gateway Film Critics Association Awards 2013|St. Louis Gateway Film Critics Association Awards Best Foreign Language Film
|
| 
|-
|}

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Chilean submissions for the Academy Award for Best Foreign Language Film
* Cinema of Chile

==References==
 

==External links==
*  
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 