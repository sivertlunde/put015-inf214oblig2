Deadtime Stories (film)
 
This article is about the 1986 American film directed by Jeffrey Delman.  For other uses, see Deadtime Stories (disambiguation).

{{Infobox film
| name           = Deadtime Stories
| image_size     = 
| image	=	Deadtime Stories FilmPoster.jpeg
| caption        = 
| director       = Jeffrey Delman
| producer       = Jeffrey Delman (producer) William J. Links (executive producer) Steven D. Mackler (executive producer) William Paul (producer)
| writer         = Jeffrey Delman (writer) J. Edward Kiernan (writer) Charles F. Shelton (writer)
| narrator       =  Scott Valentine Melissa Leo
| music          = Larry Juris
| cinematography = Daniel B. Canton
| editing        = William Szarka
| distributor    = Cinema Group
| released       = November 28, 1986
| runtime        = 93 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = $2,750,741
| preceded_by    = 
| followed_by    = 
| website        = 
}}
Deadtime Stories is a 1986 American film directed by Jeffrey Delman.

The film is also known as Freaky Fairytales (in the United Kingdom), The Griebels from Deadtime Stories (in the Netherlands), and The Griebels (European DVD English title).

The film has developed a small cult following over the years. 

==Plot summary== Goldi Lox" and the three bears.

==Cast== Scott Valentine as Peter
*Nicole Picard as Rachel (Red Riding Hood)
*Matt Mitler as Willie (Werewolf)
*Cathryn de Prume as Goldi Lox
*Melissa Leo as Judith "MaMa" Baer
*Kathy Fleig as Miranda
*Phyllis Craig as Hanagohl
*Michael Mesmer as Uncle Mike
*Brian DePersia as Little Brian
*Kevin Hannon as Beresford "Papa" Baer
*Timothy Rule as Wilmont "Baby" Baer
*Anne Redfern as Florinda
*Casper Roos as Vicar
*Barbara Seldon as Seductress
*Leigh Kilton as Seductress
*Lesley Sank as Reviving Magoga
*Lisa Cain as Living Magoga
*Jeffrey Delman as Strangling Man
*Michael Berlinger as Greg
*Fran Lopate as Grandma
* John Bachelder as Drugstore Clerk
*Caroline Carrigan as Nurse
*Oded Carmi as Groundskeeper / Postman
*Heather L. Baley as Girl in Store
* Thea as Dog
*Bob Trimboli as Lt. Jack B. Nimble
*Harvey Pierce as Capt. Jack B. Quick
*Rondell Sheridan as Looney Bin Guard
*Beth Felty as Reporter
*Patrick McCord as Anchor
*Michele Mars as Waitress
*Ron Bush as Bank Guard
*Bryant Tausek as Man At Car
*Suzanna Vaucher as Weather Girl
*Leif Wennestrom as Dead Body
*Jim Nocell as Dead Body
*Evan L. Delman as Police Sergeant

==Availability==
8 Months after the films theatrical run, the film was released on videocassette in 1987 by Continental Video in the U.S. and in Canada by Cineplex Odeon. The film was first released on DVD through Mill Creeks Chilling Classics 50 Movie Pack. That box was later discontinued when it was revealed that Deadtime Stories was not in the Public Domain. In 2007, the film was released on a bargain DVD by Image Entertainment under license from Cinevision International. The DVD runs 12 minutes shorter than the home video release since it stops and returns to the DVD menu in the middle of the end credits.

==Soundtrack==
* "Bedtime Tales" (Lyrics by Jeffrey Delman, music by Larry Juris)
* "Looney Tune" (Lyrics by Jeffrey Delman, music by Larry Juris)
* "Baby, No Maybe" (Lyrics by Jeffrey Delman, music by Larry Juris)
* "The Wolfs Lament" (Lyrics by Jeffrey Delman, music by Larry Juris)

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 