The Blue Bird (1910 film)
{{Infobox film
| name     = The Blue Bird
| image    =
| director =
| writer   = Maurice Maeterlinck (play)
| starring = Pauline Gilmer Olive Walter
| released =  
| country  = United Kingdom
| runtime  = 420 meters (about 14 minutes) English intertitles
}}
 1910 silent play by Maurice Maeterlinck and starring Pauline Gilmer as Mytyl and Olive Walter as Tytyl.  It was filmed in England.

== Cast (in credits order)==
*Pauline Gilmer as Mytyl
*Olive Walter as Tytyl
*Margaret Murray as Mummy Tyl
*E.A. Warburton as Daddy Tyl
*Ernest Hendrie as Tylo
*Norma Page as Tylette
*Carlotta Addison as The Fairy
*Edward Rigby as Bread
*H.R. Hignett as Sugar
*Doris Lytton as Milk
*Saba Raleigh as Cow
*C.V. France as Time
*Roy Travers as Cow

==External links==
*  

 

 
 
 
 
 
 
 

 