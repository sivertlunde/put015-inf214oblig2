Il bidone
{{Infobox film
| name           = Il bidone
| image          = Ilbidone.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Federico Fellini
| producer       = Mario Derecchi
| screenplay     = Federico Fellini Ennio Flaiano Tullio Pinelli
| starring       = Broderick Crawford Richard Basehart Giulietta Masina
| music          = Nino Rota
| studio         =  Titanus Société Générale de Cinématographie
| cinematography = Otello Martelli
| editing        = Mario Serandrei Giuseppe Vari
| distributor    = Titanus Distribuzione
| released       =  
| runtime        = 109 minutes
| country        = Italy
| language       = Italian
| budget         =
| gross          = 
}} Italian film directed by Federico Fellini. It features Broderick Crawford, Richard Basehart and Giulietta Masina.

Released one year after the directors internationally successful La Strada, Il bidone continues with many of the same socially conscious, Italian Neorealism|neorealist-inspired themes while minimizing the poetic realism and extravagant vitality, that is today known as "felliniesque", in favor of a more pointed political stance. 

==Plot==
The film follows the exploits of a group of swindlers, focusing on their aging leader Augusto, as they go about their "business," reaping both rewards and consequences.

It is episodic, with scenes of various swindles (such as dressing up as clergy to cheat an old woman out of her money, or pretending to be government workers to pull a con amongst the poor and homeless) followed by scenes of the aftermath.

==Cast==
* Broderick Crawford as Augusto
* Giulietta Masina as Iris
* Richard Basehart as Picasso
* Franco Fabrizi as Roberto
* Sue Ellen Blake as Anna
* Irene Cefaro as Marisa
* Alberto De Amicis as Rinaldo
* Lorella De Luca as Patrizia
* Giacomo Gabrielli as Il Baron Vargas Riccardo Garrone as Riccardo

==Reception==

===Critical response===
Film critic Bosley Crowther gave the film a mixed review, calling it "a cheap crime thriller."  He added, "For this film, which is often mentioned in estimations of the masters works, is notable as a false step in his movement toward the development of a type of story material ... But it contains some very strong Fellini phases and accumulations of moods that make it well worth seeing. And it is generally well played ... Broderick Crawfords performance as the swindler is heavy and sodden, with a particular flair for postured histrionics in the swindle scenes." 
The film also received 5 stars, a rare 100% favourable review, on the very reliable film review site, Rotten Tomatoes from a cross section of film reviewers.

===Awards===
Nominations
* Venice Film Festival: Golden Lion, Federico Fellini; 1955.

==Influence==
Il bidone had a notable influence on Giuseppe Tornatores 1995 film, The Star Maker (1995 film)|LUomo delle Stelle — about a con man who defrauds Sicilian peasants with dreams of becoming movie stars in Rome.

==References==
 

==External links==
*  
*   (Italian)
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 