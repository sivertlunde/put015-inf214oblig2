List of lesbian, gay, bisexual or transgender-related films of 1961
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1961. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==1961==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|-
|Accattone|| 1961 ||   ||   || Drama ||
|- The Childrens Hour. Audrey Hepburn and Shirley MacLaine play school teachers falsely accused of having lesbian relationship; one of them realizes that she is in love with the other. 
|- La Fille aux yeux dor|| 1961 ||   ||    || Drama || AKA The Girl with the Golden Eyes 
|- Lover Come Back|| 1961 ||   ||   || Romantic, comedy||
|-
| || 1961 ||   ||   || Drama||
|-
|Victim (1961 film)|Victim|| 1961 ||   ||   || Crime, drama||
|}

==References==
 

 

 
 
 