Dharam Sankat
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Dharam Sankat
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = N.D. Kothari
| producer       = Umesh N. Kothari	
| writer         = Achala Nagar	 	
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Vinod Khanna  Amrita Singh
| music          = Kalyanji Anandji
| cinematography = W.B. Rao
| editing        = 
| studio         = 
| distributor    = 
| released       =    
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by N.D. Kothari and produced by Umesh N. Kothari. It stars Vinod Khanna and Amrita Singh in pivotal roles.

==Cast==
* Vinod Khanna...Birju
* Amrita Singh...Madhu
* Dara Singh ... Daaku Dara Singh (Birjus Father)
* Rohini Hattangadi ... Durga Devi (Birjus Mother)
* Amrish Puri...Jagira
* Raj Babbar...Police Officer Gopal
* Shakti Kapoor...Insp. Heeralal
* Asrani...	Const. Pannalal
* Sahila Chaddha...Sohn Kanwar

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chhoti Mirch Badi Tej Balma"
| Alka Yagnik
|-
| 2
| "Enam Maro Deti Ja"
| Kumar Sanu, Sonali Vajpayee
|-
| 3
| "Hum Tum Kahan Nahin Hai"
| Manhar Udhas, Sadhana Sargam
|-
| 4
| "Kar Lo Itne Bulund"
| Mohammed Aziz, Alka Yagnik, Sadhana Sargam
|-
| 5
| "Nathaniya Main Na Pehnoon"
| Sapna Mukherjee
|}

==External links==
* 

 
 
 


 