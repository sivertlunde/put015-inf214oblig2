Sunken Silver
 
{{Infobox film
| name           = Sunken Silver
| image          =
| caption        =
| director       = Spencer Gordon Bennet George B. Seitz
| producer       =
| writer         = Frank Leon Smith Walter Miller
| cinematography =
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 10 episodes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}

Sunken Silver is a 1925 American film serial directed by Spencer Gordon Bennet and George B. Seitz. The serial is preserved at UCLA Film & Television Center. 

==Cast==
* Allene Ray - Claire Standish Walter Miller - Gavin Brice
* Frank Lackteen - Rodney Hade
* Albert Roccardi - Conch Leader
* Jean Bronte - Bobby Burns
* Spencer Gordon Bennet - Doug (as Gordon Bennett)
* Charles Fang - Sato
* Ivan Linow - Rako
* Frank Wunderlee - Milo Standish

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 