Ohm Krüger
{{Infobox film
| name = Ohm Krüger
| image = Ohm-Krueger.jpg
| caption = 
| director = Hans Steinhoff
| producer = Emil Jannings
| writer = Harald Bratt Kurt Heuser  Ernst Schröder - Adrian Krüger Elisabeth Flickenschildt - Frau Kock Ferdinand Marian - Cecil John Rhodes
| music = Theo Mackeben   
| cinematography = Fritz Arno Wagner   
| editing = Martha Dübber Hans Heinrich
| studio = Tobis Filmkunst
| distributor =
| released = April 4, 1941 (Nazi Germany)  October 1, 1941 (France) March 15, 1942 (Finland)
| runtime = 135 min.
| country = Germany
| language = German RM 
| gross = 
}}

Ohm Krüger (English: Uncle Krüger) is a 1941 German biographical film directed by Hans Steinhoff and starring Emil Jannings, Lucie Höflich and Werner Hinz. It was one of a series of propaganda films produced in Nazi Germany attacking the British. The film depicts the life of the South African politician Paul Kruger and his eventual defeat by the British during the Boer War.

It was the first film to be awarded the Film of the Nation award. It was re-released in 1944.

==Plot== Paul Krüger (Emil Jannings) speaking about his life to his nurse in a Geneva hotel. The rest of the film is told in Flashback (narrative)|flashback.

Cecil Rhodes (Ferdinand Marian) has a great desire to acquire land in the region of the Boers for its gold deposits. He sends Dr Jameson (Karl Haubenreißer) there to provoke border disturbances, and secures support from  Joseph Chamberlain (Gustaf Gründgens). When Chamberlain seeks the support of Queen Victoria (Hedwig Wangel) and her son Edward VII|Edward, Prince of Wales (Alfred Bernau), she initially refuses but changes her mind when informed of the gold in the region. She invites Paul Krüger to London, and believes she is tricking him into signing a treaty.

Krüger, being suspicious of the British, has his own plans. Krüger signs the treaty which gives the British access to the gold; however, he imposes high taxes and establishes a monopoly over the sale of TNT which forces the British to buy explosives at high prices. Hence, ultimately, Krüger tricks the British by signing of the treaty. This impresses some of the British as they find Krüger is their equal in matters of cunning, which is supposed to be the defining characteristic of the British. Having been outmaneuvered, Rhodes tries to buy Krügers allegiance. Krüger and his wife Sanna (Lucie Höflich), however, are incorruptible. After being rejected, Rhodes shows Krüger a long list of members of the Boer council who work for the British. Krüger then becomes convinced that war is inevitable if the Boers are to keep their land. He declares war.
 Lord Kitchener (Franz Schafheitlin) as Supreme Commander of the armed forces. Kitchener launches an attack on the civilian population, destroying their homes, using some as human shields and placing the women and children in concentration camps, in an attempt to damage the morale of the Boer Army.
 Oxford education, visits a concentration camp to find his wife, Petra (Gisela Uhlen). He is caught and hanged, with his wife watching. When the women respond in anger, they are massacred.

The flashback concludes in the Geneva hotel room. A dying Krüger prophesies the destruction of Britain by major world powers, which will make the world a better place to live in.

==Propaganda message== propaganda feature planned invasion Carl Peters, after Hitler came to the conclusion that no separate peace with Britain was possible.   It depicts the British as seeking gold, symbolic of barrenness and evil, in contrast to the Boers who raised crops and animals, reinforced by showing the British as prurient, and having the heros son be brought to obey Kruger only after his wife has been raped. 

Publicity material which accompanied the film particularly drew attention to the role of Winston Churchill in the Boer Wars, during which he served as a journalist.  Tobis also advised the press to emphasise what Churchill learnt in the Boer War:

 
The same Churchill who in South Africa saw his ideas about exterminating the Boers followed throughout, as the English rulers, voicing polished humanitarian slogans, while driven by mere greed, unleashed the most contemptible actions on a people under attack.  he same Churchill is now Albions prime minister. 
 

British concentration camps were attacked in the film as inhumane.  These attacks coincided with a major expansion of the Nazi camp system. 

Parallels were drawn between the Boer War and the Second World War, and between Paul Krüger and Adolf Hitler.

Several key British figures are demonised in the film, including Joseph Chamberlain and the Prince of Wales. Queen Victoria is presented as a drunkard and Winston Churchill is portrayed as a concentration camp commandant, responsible for the killing of female inmates.
 Carl Peters. 

==Production==
The first outline for Ohm Krüger was begun in September 1940 by Hans Steinhoff and Harald Bratt. 
 Eigruber is also present and very enthusiastic".   The movie was produced using 4000 horses, about 200 oxen, 180 ox wagons, 25 000 soldiers and 9000 women  

==Reception==

===Publicity and press coverage===
Directives were issued to the press by the RMVP about how to cover the film. They were instructed to draw attention to the significance of the film, but to emphasise its aesthetic rather than its political content. 

===Audience response===
The film had its première on 4 April 1941, two days after being passed by the Censor.  It was well-received, attracting a quarter of a million viewers in four days upon its initial release, largely as a result of the high expectations generated by the propaganda press campaign, with word-of-mouth recommendations also being important in the films popularity. 

The Sicherheitsdienst (SD; Nazi intelligence service) reported that the film exceeded expectations, with audiences particularly praising the unity of political conviction, artistic expression and acting performances. The public were also reportedly impressed by the fact that a film of Ohm Krügers quality could be produced in wartime.  The film was particularly popular with young audiences, according to both SD reports and film surveys. 

Some, however, did question the authenticity of the film. 
 occupied zone, later also in Vichy France). 

===Awards and honours===
Ohm Krüger won the Mussolini Cup for best foreign film at the 1941 Venice Film Festival, at which the Italian Minister for Popular Culture, Alessandro Pavolini, praised particularly the films propaganda value and the role of Emil Jannings. 
 Reich Propaganda Ministry Censorship Office.  Only three other films received this rating, namely Heimkehr (1941), The Great King (1942) and Die Entlassung (1942).  Joseph Goebbels also presented Emil Jannings with the Ring of Honour of the German Cinema. 

===Re-release===
The success of the film led Goebbels to re-release it in October 1944, as inspiration for the Volkssturm.  On 31 January 1945, the film was banned, for fear that the morale of German audiences would be harmed by images of Boer refugees of whose houses had been destroyed - images that by the time replicated the harsh realities of everyday life in Germany. 

==References==
 

==Bibliography==
* Jo Fox|Fox, Jo, Film Propaganda in Britain and Nazi Germany
* Hake, Sabine, German National Cinema
* Hallstein, C.W., Ohm Kruger: The Genesis of a Nazi Propaganda Film, Literature Film Quarterly (2002) Jew Süss, Carl Peters, and Ohm Krüger, New German Critique, 74 (1998)
*Taylor, Richard, Film Propaganda: Soviet Russia and Nazi Germany
* Vande Winkel, R, Ohm Krügers Travels: a Case Study in the Export of Third-Reich Film Propaganda, Historical Reflections / Réflexions Historiques, 35:2 (2009), pp.&nbsp;108–124.
* David Welch (historian)|Welch, David, Propaganda and the German Cinema, 1939-1945

==External links==
* 
*  at the Internet Archive

 
 
 
 
 
 
 
 
 
 
 
 
 