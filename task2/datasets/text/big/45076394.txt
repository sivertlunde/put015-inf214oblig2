Dharani (film)
{{Infobox film
| name           = Dharani
| image          =
| alt            =  
| caption        = 
| director       = Guhan Sambandham
| producer       = VGN Nagendran
| writer         = Guhan Sambhandham Aari Amy Sandra Ajay Krishna Elango Kumaravel
| music          = Ensone Bakkiyanathan
| cinematography =  Vinoth Gandhi R.Prakash
| studio   = Melody Movies
| distributor    = Dream Factory
| released       = 30 January 2015
| country        = India
| language       = Tamil
}} Tamil film written and directed by Guhan Sambandham and produced by V.G.S Nagendran under the banner of Melody Movies.  The film has an ensemble cast, which includes Aari (actor)|Aari, Ajay Krishna, Elango Kumaravel and Amy Sandra. R. Prakash and Vinoth Gandhi jointly handled the cinematography, while Ensone scored the music for this film. The film which began production in 2009, released across Tamil Nadu on 30 January 2015. 

==Plot==
The plot is set in a rural backdrop where three middle class characters running a hotel come across three different circumstances facing three important life decision that is going to change their lives forever. The film charts the path taken by them to achieve their goal and details how they traverse through to nowhere. 

==Cast== Aari as Sekhar
* Elango Kumaravel as Mahesh
* Ajay Krishna as Kathiresan
* Amy Sandra
* Jeyakumar
* Varunika

==Production== Aari signed on to appear in the project in 2009, the first feature film he had signed, but delays meant production only began in the middle 2010.  The film progressed slowly through production since 2010, and readied for a release, nearly five years after production began.  Following the success of Nedunchaalai (2014), production restarted and Aari agreed to spare dates to help complete the film by late 2014. 

== Music ==
B. Ensone, who earlier worked as a conductor for various music directors of Tamil film industry has made his debut as music director with this film and composed the films soundtrack album and background score. The album consists of 6 songs with one theme music instrumental track all composed by Ensone. Kavingar Muthulingam, Palani Bharathi, and Pulavar S.M, Tiruvenkadam penned the lyrics for the songs. Another highlight of the album is two koothu songs were sung by Veeramanidasan and Pushpavanam Kuppusamy. The soundtrack album was released on 19 December 2014 in the presence of K. Bhagyaraj, Mysskin, and D. Imman.
{{track listing
| extra_column    = Singer(s)
| lyrics_credits  = yes

| title1     = Vazhkai Enbathu Vaanavilla
| extra1     = Balram
| lyrics1    = Kavingar Muthulingam
| length1    = 05:52

| title2     = Thiraikadal Olipol Yarkum
| extra2     = Veeramanidasan, Pushpavanam Kuppusamy
| lyrics2    = Pulavar Se.Mu.Thiruvengadam
| length2    = 04:51

| title3     = Malapola Unna Sumanthenae
| extra3     = Vijay Yesudas, Chinmayi
| lyrics3    = Palani Bharathi
| length3    = 05:35

| title4     = Yar Kobam
| extra4     = Bhavan, Murali, Sundhararajan, Chandran
| lyrics4    = Palani Bharathi
| length4    = 02:56

| title5     = Kanavae Kanavae
| extra5     = Unni Menon
| lyrics5    = Palani Bharathi
| length5    = 05:11

| title6     = Manida Payalay Rama
| extra6     = Veeramanidasan, PushpavanamKuppusamy
| lyrics6    = Pulavar Se.Mu.Thiruvengadam
| length6    = 04:58

| title7     = Battle Field (Theme)
| extra7     = “Singing Violin” M. Kalyan and Chennai Strings
| length7    = 04:41
}}

==References==
 

==External links==
 

 

 
 
 
 
 