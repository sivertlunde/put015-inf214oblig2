Flowers of the Motherland
{{multiple issues|
 
 
}}

Flowers of the Motherland ( ) is a 1955 Chinese black-and-white film, which produced by Changchun Film Group Corporation. The movie follows the primary students life after Chinas founding.

== Story == Chinese Young Pioneers; except two students: Jiang Lin (江林), who is mischievous and not hard-working, and Yang Yongli (楊永麗), who is arrogant, anti-social and doesnt care about others.

On June 1, students get together with the home-bound Peoples Volunteer Army in Zhongshan Park. Volunteer Yang Zhiping (楊志平) notices that two students dont wear Red scarfs, which symbolize the Young Pioneers. Before he leaves, he encourages the students to study hard, help each other, and try to become a model class. Student Liang Huiming (梁慧明) was touched by his words and tries to help the two, but they do not appreciate it. This make Liang feel sad. Classmates sympathize with Liang and lose faith in the two.

The head teacher Feng encourages students to have patience. Later, classmates invite Jiang Lin to do an oxygen experiment, to make him interested in studying. While Yang Yonglis foot is scalded, they helped her make up homework, so that she can take  exams with the class. In classmates helping, Yang and Jiang were moved, they progress together in summer vacation.

After summer vacation, the class became to grade six. Jiang Lin and Yang Yongli become Young Pioneers for their progress.

== Cast ==

* Zhang Yunying (张筠英) - Yang Yongli (楊永麗)
* Li Xixiang (李錫祥) － Jiang Lin (江林)
* Zhao Weiqing (趙維勤) － Liang Huiming (梁慧明)
* Guo Yuntai (郭允泰) － Yang Zhiping (楊志平)
* Shi Ling (石靈) － Yang Yonglis mother
* Lü Dayu (呂大渝) - Liu Jü (劉菊)

== Legacy ==
* Due to the movie follows children life, so Chinese people usually use the movie title "Flowers of the Motherland" to praise children.
* The interlude song "Let us Sway Twin Oars" (讓我們蕩起雙槳) was included in Chinas primary school music textboox, and becomes a famous childrens song.

 

 
 
 
 

 
 