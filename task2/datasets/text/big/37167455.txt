Veeram
 
 
{{Infobox film
| name           = Veeram
| image          = Veeram poster.jpg
| caption        = Veeram Promotional Poster Siva
| producer       = Bharathi Reddy
| writer         = Bharathan-Siva (dialogues) Siva
| story          = Bhoopati Raja-Siva Bala Santhanam Santhanam
| music          = Devi Sri Prasad
| cinematography = Vetri
| editing        = Mu. Kasi Viswanathan Vijaya Productions
| distributor    = Vijaya Pictures
| released       =  
| runtime        = 2 hours 36 minutes
| country        = India
| language       = Tamil
| budget         =   
| gross          =    
}}
 Siva and Pradeep Rawat and Abhinaya (actress)|Abhinaya, among others. Pre-production works had been ongoing since December 2011, with shooting starting in April 2013. The film was released on 10 January 2014. The film received  positive reviews from critics.  

==Plot==
Vinayagam (Ajith) is a brave person living in a village called Oddanchatram, who lives with his four brothers Murugan (Bala), Shanmugam (Vidharth), Kumaran (Suhail) and Senthil (Munish). The elder brother loves the younger ones to the core, and sacrifices his happiness for their good. They are often caught in fights and they are proud of it. Advocate Bail Perumal (Santhanam), who bails them out whenever legal issues arise because of their brawls. Vinayagam hates the idea of marriage as he feels that his wife might create disharmony among brothers. Though the four younger brothers say that they dont want to either fall in love or get married, when Vinayagam is not around, they all have their secret lovers. Now, in order to get the green light for their love stories, they learn through his brothers childhood friend Collector Subbu (Ramesh Kanna) that Vinayakam, in school, was in love with a girl named Koppuram Devi (fondly called Kopu) and his brothers hatch a plan to find her and reintroduce her to Vinayakam, so he can fall for her all over again. But shes married now and has kids. So the brothers conspire to do the most logical thing, which is to find another woman named Koppuram Devi (fondly called Kopu), because, you see, Vinayakam was not in love with that girl so much as her name, and when he meets another (completely random) girl with that name, he is sure to lose his heart to her, just because she bears that name. Though initially reluctant later Vinayakam himself falls in love with her. He clashes with a goon called Vanangamudi (Pradeep Rawat) to take care of market in the village. Vanangamudi tries to kill Vinayagams brothers while Vinayagam kidnaps Vanangamudis son (Amit Kumar Tiwari) after clashing, Vinayagam orders Vanangamudi to leave this village.

While travelling in train to Koppus village, Koppu narrates her family background to Vinayak. Koppus father Nallasivam (Nassar) is a respected man in the village who hates violence, while his son (Pawan) is exact opposite who kills people but when he is killed, Nallasivam refuse to bury his body and decided to make his village with peace and harmony. Few goons enter into train but Vinayagam bashes all the rowdies, Koppu is shocked to see Vinayagam whom she thought as a non-violent person. Vinayagam and his brothers arrive at Koppus village with clean shaven look, he says that he has changed and he would never harbor violence, he and his brothers are welcomed and respected by her family. Vinayagam is touched and impressed by their love, affection and hospitality. Vinayagam comes to know that a goon named Aadalarasu (Atul Kulkarni) wants to kill Nallasivam and his family, reason is revealed through flashback that Aadalarasus father Aavudaiyapan (Avinash) is responsible for blast of matchstick factory. Nallasivam complains against Aavudaiyappan. Aavudaiyappan who is arrested, commits suicide by getting hit by a lorry.

Aadalarasu swears revenge against Nallasivam. Vinayagam finishes all the goons and solves all their problems without the knowledge of Nallasivam and family. When Nallasivams granddaughter finds sickle under Vinayagams jeep, Nallasivam orders Vinayagam to go out of the town. Aadalarasu who escapes from death sentence arrives to kill Nallasivam but Vinayagam keeps him and his family in a safe place. Aadalarasu informs Vinayagam that he had kidnapped one of his brothers Kumaran, Vinayagam arrives at the nick of time and saves his brother but instead gets attacked by Aadalarasu. Nallasivam and his family who arrives at the place comes to know about risk taken by Vinayagam to save him from the mess. Brutally attacked Vinayagam rises steadily, kills Aadalarasu and his henchmen. Nallasivam who is impressed with Vinayagams valour decided to give his hand to his daughter Koppu. The film ends with the marriage of Vinayagam and his brothers.

==Cast==
 
* Ajith Kumar as Vinayagam
* Tamannaah as Koppuram Devi
* Vidharth as Shanmugam Veeram (DVD): opening credits from 1.14 to 1.28  Bala as Murugan  Santhanam as Bail Perumal 
* Nassar as Nallasivam 
* Munish as Senthil 
* Suhail Chandhok as Kumaran 
* Thambi Ramaiya as Savarimuthu 
* Appukutty as Mayilvaganam 
* Ramesh Khanna as District Collector Subbu 
* Ilavarasu as Azhagappan 
* Atul Kulkarni as Aadalarasu  Pradeep Rawat as Vanangamudi 
* Avinash as Aavudaiyappan 
* Mayilsamy as Marikolunthu 
* Shanmugarajan 
* Amit Kumar Tiwari as Vanangamudis son  Abhinaya as Poongothai 
* Manochitra as Anita 
* Suza Kumar as Senbagam 
* Vidyullekha Raman as Alamelu 
* Rohini Hattangadi as Koppuram Devi 
* Devadarshini as District Collector Subbus wife  Sumithra 
* Kalairani 
* Stunt Silva as Henchman
* Crane Manohar as College Peon
* Paravai Muniyamma
* Yuvina Parthavi as Nallasivams granddaughter  Pawan as Nallasivams son
* Periyardasan as Bhai
* R. N. R. Manohar as Maanikkam
* Paruthi Veeran Sujatha as Nallasivams younger sister 
 

==Production==

===Development===
The potential collaboration between Vijaya Productions and Ajith Kumar was reported in November 2011, with speculation suggesting that Venkata Ramana Reddy wanted to commemorate his late father B. Nagi Reddys 100th year since birth with a film project. Siva (director)|Siva, who had made his debut in Tamil films with the 2011 film Siruthai, was noted as a potential candidate to direct the film, while Anushka Shetty was initially reported to play the female lead role.  Subsequently, in early December 2011, Ajith signed on to play the lead role in the venture, with a poster released confirming Sivas participation, noting that Vetri and Yuvan Shankar Raja would be cinematographer and music composer, respectively.  

===Casting=== Abhinaya signed for another lead role.  Vidyullekha Raman signed to be Santhanams pair for a third film.  Jayaprakash, Jayaram and Rajeev Govinda Pillai were also initially reported to be a part of the cast, though a later press release did not confirm their participation.  Kannada actor Avinash agreed to play a character "who, due to poverty, has become vengeful and is willing to do anything wrong for his own benefit". 

Bhoopathi Raja has written the script along with Siva and Bharathan has written the dialogues. 

===Filming=== Hyderabad and went on for two weeks and more. The film was initially rumoured to be titled as Vetri Kondaan or Vinayagam Brothers before finally confirming Veeram. The shoot was completed on 2 November 2013. 

Ajith had participated in a stunt sequence where he had to hang outside the train. The sequence was shot in a location situated in the Orissa border. 

==Themes and influences== Murattu Kaalai (1980), Annamalai (film)|Annamalai (1992) and Baashha (1995). 

==Soundtrack==

{{Infobox album 
| Name = Veeram
| Longtype = to Veeram
| Type = Soundtrack
| Artist = Devi Sri Prasad
| Cover =
| Border = yes
| Alt =
| Released = 20 December 2013   
| Recorded = 2013 Feature film soundtrack Tamil
| Length = 19:33
| Label = Junglee Music
| Producer = Devi Sri Prasad
| Last album = 1 (2014 film)|1 (2014)
| This album = Veeram (2013)
| Next album = Brahman (2013 film)|Brahman (2014)
}}

The films soundtrack was composed by Devi Sri Prasad.  The music rights were purchased by Junglee Music.    The album, consisting of 5 tracks, was scheduled to be released on 20 December 2013 but the tracks leaked on the Internet on 18 December 2013. The music was officially launched on 20 December followed by airplay of the songs on FM stations.  Behindwoods gave the soundtrack a rating of 2.75 out of 5 and India Glitz gave 2.5.  

{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    = 19:33
| all_lyrics      = Viveka
| title1          = Nallavannu Solvaanga
| extra1          = Devi Sri Prasad
| length1         = 4:36
| title2          = Ival Dhaana
| extra2          = Sagar, Shreya Ghoshal
| length2         = 4:14
| title3          = Thangamae Thangamae
| extra3          = Adnan Sami, Priyadarshini
| length3         = 4:06
| title4          = Jing Chikka Jing Chikka
| extra4          = Pushpavanam Kuppusamy, Magizhini Manimaaran
| length4         = 4:30
| title5          = Veeram
| extra5          = Anand, Koushik, Deepak, Jagadish
| length5         = 2:48
}}

==Release== Sun TV.  Central Board of Film Certification gave the film "U" certificate. 

The film was released in 400 screens in Tamil Nadu, http://www.behindwoods.com/tamil-movies-cinema-news-14/ajiths-veeram-screen-counts-from-around-the-world.html  120 screens in Kerala,  50 screens in Karnataka,  and in 220 screens overseas. 

A first-look teaser was released on 7 November 2013.  The second teaser was released on 5 December 2013 with a dialogue "Enna Naan Solrathu".  Veeram s trailer was released by Sun TV as a New Year special at 10 am.  

===Distribution===
Across Tamil Nadu, Veeram was distributed by various parties: in Chennai City by Sri Thenandal Films, in NSC area by Sri Thenandal Films, in T.T (Trichy and Thanjavur) by Green Screens, in M.R (Madurai, Ramanadhapuram) are by Gopuram Films and in T. K. (Thirunelveli, Kanyakumari) are by Sri Mookambiga Films. Vendhar Movies released the film in Coimbatore and surroundings through Vel Films and in Salem and surroundings through G Film. 

==Reception==
===Critical reception=== Filmibeat (11 August 2014). Retrieved on 22 October 2014. 

The Times of India gave 3 out of 5 stars and wrote, "The film is a template masala film – a superhuman hero, his cronies who will sing his praises all the time, a beautiful-looking heroine, a raging villain. It has no room for logic, moving from one hero-worshipping scene to the next and is aimed at our visceral tastes. The story is in service of its star and, it is unapologetic about it".  Rediff gave 2.5 stars out of 5 and stated, "Veeram is a treat for Ajith fans".  Sify gave 4 stars out of 5 calling it a "Mass Entertainer" and writing, "The film works mainly due to Ajiths charisma and Sivas script. It is a formula film where essential masala has been correctly mixed in the right proposition. On the whole, Veeram is an enjoyable fun ride".  Baradwaj Rangan wrote, "A family with five brothers, another family with representatives from three generations, annan-thambi sentiment, amma sentiment, appa sentiment, and in the middle of all this, a big star giving his fans what they want – if Faazil and Vikraman collaborated on a mass masala movie, it might end up looking like Sivas Veeram. Veeram is about the hero. Everything else – the crude dramatics, the piles of clichés, the characters (especially the bad guys) who come and go as they please – is secondary".  Bangalore Mirror gave 3.5 out of 5 and called it "a festival cracker for the fans of Thala". 
 IANS gave 2.5 stars out of 5 and wrote, "Veeram is a classic example, which elevates Ajith and leaves the rest of the ensemble cast behind. The film is dedicated to Ajith, who is as charismatic as ever in his role. However, he is unfortunately not complemented by a strong screenplay". 

===Box office===
;India

In its first weekend, Oneindia declared the film a box office success.  After its first week, Veeram grossed ca.   worldwide. 

;Overseas

Veeram was released on 24 screens in the UK. The film made   there in the first weekend. Veeram was reduced to 13 screens and had made  . Veeram, running in three screens, made  . 

==References==
 

==External links==
*  

 

 
 
 
 
 
 