Thuntata (film)
{{Infobox film
| name       = Thuntata
| image      = Thuntata Kannada Movie.jpg
| director   = Indrajith Lankesh
| producer   = C. Nagaraju
| story      = 
| screenplay = 
| starring   =  
| music      = Gurukiran
| cinematography = A.V. Krishna Kumar
| editing    = 
| studio     = 
| distributor = 
| released   =  
| country    = India
| language   = Kannada
| runtime    = 
| budget     = 
| gross      = 
| website    =  
}}
 Kannada romantic-comedy film directed by Indrajit Lankesh featuring Anirudh, Rekha Vedavyas and Chaya Singh. The film released on 24 May 2002. 

==Cast==
* Anirudh ...
* Rekha Vedavyas ... 
* Chaya Singh ... 
* Sudeep ... (Guest Appearance)
* Umashree ... 
* Tennis Krishna ...
* Kashi ...
* and others ... 

==Soundtrack==
The film features background score and soundtrack composed by Gurukiran and lyrics by Nagathihalli Chandrashekhar, K. Kalyan and V. Manohar. 
{{Infobox album
| Name = Thuntata
| Type = soundtrack
| Artist = Gurukiran Kannada
| Label = Ashwini Audio
| Producer = 
| Cover = 
| Released    =   Feature film soundtrack
| Last album = 
| This album = 
| Next album = 
}}

==References==
 

==External links==
*  

 
 
 
 


 