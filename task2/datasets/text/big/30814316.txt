Chitram Bhalare Vichitram
{{Infobox film
| name = Chitram Bhalare Vichitram
| image =
| caption =
| director = P. N. Ramachandra Rao
| producer =R. V. V. Vijaya Kumar
| writer = P. Sambasiva Rao (story) Madhu Thotapalli (dialogue)
| screenplay =P. N. Ramachandra Rao Naresh Brahmanandam Jayaprakash Reddy Kota Srinivas Rao Rajeevi Bindu Ghosh Thulasi Jaya Latha Pradeep Shakti Chittibabu M. S. Rayudu Sudhakar Reddy Shyam Babu Radha Kumari Athili Lakshmi Vijji Vijaya Krishna Vidyasagar
| cinematography =Babjee
| editing =B. Shankar S. Ramesh
| distributor =Sri Sairam Films
| released = 1992
| runtime =
| country = India
| language = Telugu
| budget =
| preceded by =
| followed by =
}}
Chitram! Bhalare Vichitram!! is a 1992 Telugu cinema|Telugu, comedy film. The ensemble cast blockbuster film was directed by P. N. Ramachandra Rao.   Upon release the film received very positive reviews, and remained a cult classic. The film was later remade into Tamil as Aanazhagan.    

==Plot==
Raja (Naresh), Sudhakar (Subhalekha Sudhakar), Raghava (Maharshi Raghava), Boondi Brahmanandam (Brahmanandam) are good friends stay together as tenats at Gorojanala Garudachalam(Kota Srinivasa Rao), after being thrown out of the house they start searching new house for rent but no one are ready to give their house for bachelors.

Finally they land up in a place where the landlady insists on having a family as her tenants. The four bachelors dress up to be a man (Sudhakar), his retarded brother (Raghava), his father(Brahmanandam) and his wife (Naresh being the wife). Now Nareshs (called "Prema" and who happens to be in a womans attire) lady love happens to be the landladys daughter and all hell breaks loose.

In womans disguise, Naresh looked very much pretty. Actor Brahmanandam steals the show in the movie as the person who is a cook with a great appetite.

==Soundtrack==
Vidyasagar, a relatively newcomer at that time, gave a few really good tunes with the notable ones being the "Seetalu Yerugani" and "Maddela Daruvei". The melodious former shot on the occasion of Premas Seemantam ceremony is composed in Kalyani Raagam and it is still being played at Seemantam functions in Hyderabad!!
* "Brahmachaarulam"
* "Navvukune Mana"
* "Aho Priya Mettaga"
* "Maddela Daruvei"
* "Seetalu Yerugani"

==Dubbing for the disguised characters== Naresh (as "Prema") was given by the veteran dubbing artiste Roja Ramani which enhanced the said role and became one of the major highlights of the movie. She so excellently modulated her voice that it perfectly suited the role and all the scenes involving Prema became invariably hilarious and haunting. Naresh also did great justice to the voice by his brilliant acting and beautifully getting into the skin of the character without any inhibitions usually associated with such roles that militate against natural performance. The voice for the old disguise character of Brahmanandam was given by veteran actor Nagabhushanam.

==Awards==
;Nandi Awards Naresh (1992) 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 