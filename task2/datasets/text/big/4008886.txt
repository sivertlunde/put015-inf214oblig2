Ratboy
 
{{Infobox film|
  name    = Ratboy|
  image          = Poster of the movie Ratboy.jpg|
  caption = Cover of VHS release|
    producer         = Fritz Manes |
  director       = Sondra Locke  | Rob Thompson | Robert Townsend John Witherspoon Ross White |
  music         = Lennie Niehaus |
  cinematography = Bruce Surtees |
  editing         = Joel Cox |
  distributor    = Warner Bros. |
  released   = October 17, 1986 (USA) |
  runtime        = 104 min. | English |
  budget =  |
  gross = |
}}

Ratboy is a film released in 1986 and directed by and starring Sondra Locke.   
 Rick Baker.

Ratboy did however receive better reviews in European countries, especially France, winning the Deauville American Film Festival.

==Plot==
A failed window dresser named Nikki (Sondra Locke) overhears of a mysterious Ratboy named Eugene (Sharon Baird, billed in the credits as "S. L. Baird") while dumpster diving at a dump. After finding and befriending him, Nikki makes several attempts at exploiting his uniqueness to the public. At the same time, Eugene wishes to avoid public attention.  There is never any explanation for Ratboys existence as a human/rat hybrid.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 