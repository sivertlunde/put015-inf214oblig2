Empire State (2013 film)
{{Infobox film
| name           = Empire State
| image          =  
| caption        = DVD cover
| director       = Dito Montiel
| producer       = Mark Stewart
| writer         = Adam Mazer
| starring       = Liam Hemsworth Dwayne Johnson Emma Roberts
| studio         = Grindstone Entertainment Group Emmett/Furla/Oasis Films|Emmett/Furla Films Cheetah Vision Voltage Pictures
| distributor    = Lionsgate Films
| music          = 
| cinematography = Dana Gonzales
| released       = September 3, 2013
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $11 million 
| gross          = $3,682,418 (DVD Sales) 
}}
Empire State is a 2013 American crime drama film based on a true story,   centered on two childhood friends who rob an armored car depository, and the NYPD officer who stands in their way. Directed by Dito Montiel and starring Liam Hemsworth, Dwayne Johnson and Emma Roberts, the film was released straight to DVD and Blu-ray on September 3, 2013.

==Plot==
After failing to get into the police academy, Chris Potamitis (Liam Hemsworth), settles for a security guard job with the EMPIRE STATE Armored Truck Company. Chris makes the mistake of mentioning the companys lax security to his best friend, Eddie (Michael Angarano), and is soon unwittingly drawn into an elaborate scheme to rob the abundant amounts of cash being stored there - resulting in the largest cash heist in U.S. History. As the stakes continue to rise, Chris and Eddie must outwit James Ransone (Dwayne Johnson), the veteran NYPD Detective that is hot on their trail, as well as the local crime bosses that want to know who pulled a job on their turf, or suffer the consequences.

==Cast==
*Liam Hemsworth as Chris Potamitis 
*Dwayne Johnson as Detective James Ransome
*Emma Roberts as Nancy Michaelides
*Lydia Hull as Maria
*Nikki Reed as Lizzette
*Michael Angarano as Eddie
*Shenae Grimes as Eleni
*Chris Diamantopoulos as Spyro
*Jerry Ferrara as Jimmy
*James Ransone as Agent Nugent 
*Gia Mantegna as Vicky
*Michael Rispoli as Tony
*Paul Ben-Victor as Tommy
*Sharon Angela as Dina
*Roger Guenveur Smith as Agent Marichal
*Wayne Pére as Williams
*Tony Bentley as Sam Wallace
*Karen Clark as Karen
*Mikko Macchione as Mr. Liakos
*Jesse Pruett as Doorman
*Frank L. Ridley as Steve Marks

==Production==
The film was set and filmed in Toronto, Queens, and New Orleans between May 2012 and June 2012.

==Soundtrack==
The soundtrack to Empire State was released on September 3, 2013. 

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 34:57 

| title1          = Opening
| length1         = 0:53
| extra1          = David Wittman

| title2          = Bathroom
| length2         = 1:27
| extra2          = David Wittman

| title3          = Family
| length3         = 1:17
| extra3          = David Wittman

| title4          = Want Ads
| length4         = 0:32
| extra4          = David Wittman

| title5          = Dog In There
| length5         = 1:05
| extra5          = David Wittman

| title6          = Dad Got Fired
| length6         = 1:07
| extra6          = David Wittman

| title7          = OTB
| length7         = 1:08
| extra7          = David Wittman

| title8          = Kids Clean
| length8         = 0:47
| extra8          = David Wittman

| title9          = Driving
| length9         = 1:03
| extra9          = David Wittman

| title10         = Finding Letter
| length10        = 1:30
| extra10         = David Wittman

| title11         = Broomstick
| length11        = 2:57
| extra11         = David Wittman

| title12         = TV Boom
| length12        = 0:27
| extra12         = David Wittman

| title13         = Shootout
| length13        = 6:33
| extra13         = David Wittman

| title14         = Jimmy Shot
| length14        = 0:35
| extra14         = David Wittman

| title15         = Eddie Breaks In
| length15        = 1:12
| extra15         = David Wittman

| title16         = After the Heist
| length16        = 0:30
| extra16         = David Wittman

| title17         = Interrogating Chris
| length17        = 1:20
| extra17         = David Wittman

| title18         = Go For a Walk
| length18        = 2:25
| extra18         = David Wittman

| title19         = Eddie Plots 
| length19        = 2:07
| extra19         = David Wittman

| title20         = Interrogating Chris II
| length20        = 1:53
| extra20         = David Wittman

| title21         = Interrogating Eddie
| length21        = 1:35
| extra21         = David Wittman

| title22         = Need Some Money
| length22        = 1:45
| extra22         = David Wittman

| title23         = End Theme
| length23        = 0:49
| extra23         = David Wittman

}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 