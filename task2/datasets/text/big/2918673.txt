Cast a Deadly Spell
{{Infobox film
| name = Cast a Deadly Spell
| image = CastaDeadlySpell.jpg
| caption =
| writer = Joseph Dougherty David Warner Charles Hallahan Alexandra Powers
| director = Martin Campbell
| producer = Gale Anne Hurd
| editing = Dan Rae
| distributor = HBO
| released =  
| runtime = 96 min.
| country = United States
| language = English
| music = Curt Sobel
| budget = 6,000,000 $
}}
 detective HBO David Warner and Clancy Brown. It was directed by Martin Campbell and written by Joseph Dougherty. The original music score was composed by Curt Sobel.

==Plot== magic is real, monsters and mythical beasts stalk the back alleys, zombies are used as cheap labor, and everyone—except Lovecraft—uses magic every day. Yet, cars, telephones and other modern technology also exist in this world.

==H.P. Lovecraft influence==
  Dunwich Room. Frequent references are made to the Necronomicon, the Great Old Ones, Cthulhu and Yog Sothoth throughout the film.

While the film clearly does not take place in Lovecrafts world, it does reference Lovecraft extensively (including the name of the main character), though there are many obvious differences. The most obvious difference is that in Cast a Deadly Spell, magic is omnipresent – everybody uses it. In Lovecrafts stories magic is almost unknown, except to a select few.

== Sequel == Witch Hunt. Witch Hunt takes place in the 1950s during the red scare, in which magic is substituted for communism. Dennis Hopper played Lovecraft in place of Fred Ward. Additionally, many characters have different background stories than in Cast a Deadly Spell. For example, Lovecraft refuses to use magic in Cast a Deadly Spell on principle, and because of a bad experience in Witch Hunt.

== Cast ==
*Fred Ward – Harry Philip Lovecraft
*Julianne Moore – Connie Stone David Warner – Amos Hackshaw
*Alexandra Powers – Olivia Hackshaw
*Clancy Brown – Harry Bordon
*Charles Hallahan - Detective Morris Bradbury
*Arnetia Walker - Hypolite Kropotkin
*Raymond OConnor - Tugwell
*Peter Allas - Detective Otto Grimaldi
*Lee Tergesen - Larry Willis/Lilly Sirwar
*Michael Reid MacKay - Gargoyle
*Curt Sobel - Band Leader

== Awards ==
*Saturn Award - Best Genre Television Series (Nomination)
*CableACE Award - Best Original Score (Nomination)
*Emmy Award - Outstanding Music & Lyrics (Won)
*Emmy Award - Outstanding Sound Editing (Nomination)

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 