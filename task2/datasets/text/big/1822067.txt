The Fugitive (1993 film)
{{Infobox film name           = The Fugitive image          = The Fugitive movie.jpg caption        = Theatrical release poster alt            = A black poster. Above in bold white letters are the lines: "A murdered wife." "A one-armed man." "An obsessed detective." "The chase begins." In the middle is a picture of an older male with brown hair wearing a white t-shirt, black zippered jacket, black coat, and white pants; running parallel to a train on a subway platform. Below in large white typeface is the line: "Harrison Ford". Below that in smaller white typeface reads the line: "is" with a larger white typeface next to it reading: "The Fugitive". The film credits appear underneath it in a small grey typface, along with a line that reads: "August 6" in a larger white typeface. director  Andrew Davis producer       = Anne Kopelson Arnold Kopelson screenplay  Jeb Stuart David Twohy story          = David Twohy based on The Fugitive by Roy Huggins starring       = {{Plain list|
* Harrison Ford
* Tommy Lee Jones
}}
| music          = James Newton Howard Michael Chapman
| editing        = {{Plain list|
*Dennis Virkler
*David Finfer
*Dean Goodhill
*Don Brochu
*Richard Nord
*Dov Hoenig
}} distributor    = Warner Bros. released       =   runtime        = 130 minutes country        = United States language       = English budget         = $44 million  gross          = $368.9 million   
}}
 1960s television Andrew Davis and stars Harrison Ford and Tommy Lee Jones. After being wrongfully convicted for the murder of his wife, Dr. Richard Kimble (Ford) escapes from custody and is declared a fugitive. He sets out to prove his innocence and bring those who were responsible to justice while being pursued relentlessly by a team of United States Marshals Service|U.S. Marshals, led by Deputy Samuel Gerard (Jones).
 Best Picture, Best Supporting Actor. It presently holds a 96% score on Rotten Tomatoes and a rating of "universal acclaim" from Metacritic.      

On August 31, 1993, the original motion picture soundtrack was released by the Elektra Records music label. The soundtrack was composed and orchestrated by musician James Newton Howard. The independent music label La-La Land Records would also later release a limited edition 2-CD set soundtrack as well.  The film spawned a sequel, U.S. Marshals (film)|U.S. Marshals, in which Jones reprised his role as Gerard. The sequel was directed by Stuart Baird, acquiring mostly negative critical reviews but earning over $100 million at the box office.

==Plot== vascular surgeon, first degree murder. On his way to death row aboard a bus, his fellow prisoners attempt an escape. The pandemonium results in a number of dead and wounded, causing the bus to fall down a ravine and into the path of an oncoming train. Kimble escapes the destructive collision and flees the scene. Deputy U.S. Marshal Samuel Gerard (Jones), and his colleagues Renfro (Pantoliano), Biggs (Roebuck), Newman (Wood) and Poole (Caldwell), arrive at the crash site and formulate a search plan to apprehend the escaped convicts. Kimble sneaks into a hospital to treat his wounds and alter his appearance. He eludes the authorities and makes a getaway. Kimble is eventually cornered by Gerard at the leading edge of a storm drain flowing into a dam. Kimble leaps from the viaduct into the raging water and escapes.
 Cook County Hospitals prosthetic department to obtain a list of people who had their prosthetic arm repaired shortly after his wifes murder. Following a police lead confirming Kimbles recent whereabouts, Gerard speculates that Kimble must be looking for the one-armed man. Later, Kimble breaks into the residence of one of the people on the list; a former police officer named Fredrick Sykes (Katsulas). Not only does Kimble confirm that Sykes is the murderer, but he also discovers that Sykes is employed by a pharmaceutical company, Devlin MacGregor, which is scheduled to release a new drug called Provasic. Kimble had investigated the drug and revealed that it caused liver damage, which would have prevented it from being approved by the Food and Drug Administration|FDA. He also deduces that Nichols, who is leading the drugs development, arranged a cover-up, and ordered Sykes to kill him; his wifes death was incidental. Gerard follows Kimbles lead to Sykes home and draws the same conclusion.

As Kimble takes an elevated train to confront Nichols at the drugs presentation in a hotel, Sykes appears and attacks him. In the ensuing struggle, Sykes shoots a transit cop before being subdued and handcuffed to a pole by Kimble. Kimble arrives at the pharmacon conference and interrupts Nicholss speech, accusing him of falsifying his medical research and orchestrating his wifes consequent murder. They begin to fight while being chased by the marshals and police. Kimble, Nichols, Renfro and Gerard find themselves in the hotels laundry room. Gerard calls out to Kimble and says that he is aware of the conspiracy fabricated by Nichols. Hearing this, Nichols knocks out Renfro and takes his gun. Nichols then attempts to shoot Gerard, but Kimble attacks him from behind, rendering him unconscious. Kimble surrenders to Gerard, who escorts him out of the hotel. Nichols and Sykes are arrested, while Kimble is exonerated and driven away from the crime scene by Gerard.

==Cast==
 .]]
* Harrison Ford as Dr. Richard Kimble
* Tommy Lee Jones as Deputy U.S. Marshal Samuel Gerard
* Sela Ward as Helen Kimble
* Joe Pantoliano as Deputy U.S. Marshal Cosmo Renfro
* Andreas Katsulas as Fredrick Sykes
* Jeroen Krabbé as Dr. Charles Nichols			
* Daniel Roebuck as Deputy U.S. Marshal Robert Biggs Tom Wood as Deputy U.S. Marshal Noah Newman
* L. Scott Caldwell as Deputy U.S. Marshal Erin Poole
* Julianne Moore as Dr. Anne Eastman
* Ron Dean as Detective Kelly
* Joseph Kosala as Detective Rosetti
* Jane Lynch as Dr. Kathy Wahlund

==Production==

===Casting===
Actor Harrison Ford was not originally cast for the role of Dr. Kimble.    Instead, a number of actors were auditioned for the part, including Alec Baldwin, Nick Nolte, Kevin Costner, and Michael Douglas.  Nolte in particular felt he was too old for the role despite only being a year older than Ford. Although the role of Gerard went to actor Tommy Lee Jones, both Gene Hackman and Jon Voight were considered for the part.  The character of Dr. Nichols was recast for actor Jeroen Krabbé after the original actor who won the role, Richard Jordan, fell ill and later died during the early phases of production. 

===Filming=== Deals Gap was the location of the scene where Kimble jumps from the dam.
 Chicago freight Pullman neighborhood of Chicago. Harrison Ford used the pay phone in the Pullman Pub, at which point he climbs a ladder and runs down the roofline of the historic rowhouses.  During the St. Patricks Day Parade chase scene, Mayor Richard M. Daley and Illinois Attorney General Roland Burris are briefly shown as participants. 

===Music===
The films musical score was composed by James Newton Howard; Janet Maslin of The New York Times called the music "hugely effective".  Elektra Records released an album featuring selections from the score on August 31, 1993. La-La Land Records later released a 2-disc, expanded and remastered edition of the score, featuring over an hour of previously unreleased music, tracks from the original soundtrack, and alternate cues. 
 
{{Infobox album
| Name = The Fugitive: Limited Edition Expanded Archival Collection Disc 1
| Type = Film score
| Artist = James Newton Howard
| Cover = TheFugitiveSound.jpg
| Released = 
| Length = 64:52
| Label = La-La Land Records
| Reviews =
}}

{{Track listing
| collapsed       = no
| headline        = The Fugitive: Limited Edition Expanded Archival Collection Disc 1
| total_length    = 64:52
| title1          = Main Title
| length1         = 3:50
| title2          = The Trial
| length2         = 4:31
| title3          = The Bus
| length3         = 4:56
| title4          = The Hand/The Hunt/The Tow truck
| length4         = 4:04
| title5          = The Hospital
| length5         = 4:06
| title6          = Helicopter Chase
| length6         = 4:49
| title7          = The Sewer
| length7         = 4:24
| title8          = Kimble in the River
| length8         = 1:52
| title9          = The Dream/Kimble Dyes his Hair
| length9         = 2:45
| title10         = Copeland Bust
| length10        = 1:59
| title11         = Kimble Calls his Lawyer/No Press
| length11        = 1:57
| title12         = Kimble Returns to Hospital
| length12        = 3:06
| title13         = The Montage/Cops Bust the Boys/Computer Search
| length13        = 6:50
| title14         = Kimble Saves the Boy
| length14        = 2:54
| title15         = Gerard Computes
| length15        = 1:49
| title16         = The Courthouse/Stairway Chase
| length16        = 6:13
| title17         = Cheap Hotel/Sykes Apartment
| length17        = 4:37
}}

{{Infobox album
| Name = The Fugitive: Limited Edition Expanded Archival Collection Disc 2
| Type = Film score
| Artist = James Newton Howard
| Cover = 
| Released = 
| Length = 61:29
| Label = La-La Land Records
| Reviews =
}}

{{Track listing
| collapsed       = yes
| headline        = The Fugitive: Limited Edition Expanded Archival Collection Disc 2
| total_length    = 61:29
| title1          = Kimble Calls Gerard
| length1         = 2:37
| title2          = Memorial Hospital/Its Not Over Yet
| length2         = 3:03
| title3          = See a Friend/Sykes Marks Kimble
| length3         = 2:12
| title4          = This is My Stop/El Train Fight
| length4         = 4:02
| title5          = The Hotel
| length5         = 2:42
| title6          = Roof Fight Pt. 1/Roof Fight Pt. 2/Nichols Reappears
| length6         = 3:52
| title7          = The Elevator/The Laundry Room
| length7         = 4:58
| title8          = Its Over/End Credits
| length8         = 5:40
| title9          = The Fugitive Theme
| length9         = 3:04
| title10         = Kimble Dyes His Hair
| length10        = 4:23
| title11         = No Press
| length11        = 4:57
| title12         = No Press (Alternate)
| length12        = 0:45
| title13         = No Press (No Sax)
| length13        = 1:31
| title14         = Cops Bust The Boys (Alternate)
| length14        = 1:09
| title15         = Computer Search (No Sax)
| length15        = 2:49
| title16         = Roof Fight Pt. 1 (Less Percussion)
| length16        = 1:57
| title17         = Roof Fight Pt. 2 (Less Orch Verb)
| length17        = 1:17
| title18         = Helicopter Chase/The Sewer (Synth Demos)
| length18        = 7:44
| title19         = Piano End credits
| length19        = 2:47
}}

==Release==

===Home media=== Region 1 HD version on May 23, 2006.  Concurrently, on September 8, 2009, a widescreen repackaged variant was also released.  Special features for the DVD included behind-the-scenes documentaries, audio commentary by Tommy Lee Jones and director Andrew Davis, an introduction with the films stars and creators, and the theatrical trailer.

A restored widescreen hi-definition Blu-ray Disc|Blu-ray version was released on September 26, 2006. Special features include commentary by Tommy Lee Jones and director Andrew Davis, two documentaries, and the theatrical trailer.  On September 3, 2013, a 20th Anniversary Blu-ray edition for the film was released with a new digital transfer along with DTS-HD Master Audio tracking among other features. 

===Novelization===
Jeanne Kalogridis wrote a mass-market paperback novelization of the film.  She worked from the original screenplay, which characterizes a doctor unjustly accused of a crime, while being pursued relentlessly by federal authorities.

==Reception==

===Critical response=== weighted average out of 100 to critics reviews, The Fugitive received a score of 88 based on 11 reviews. 

{{quote box|quote=
Like the cult television series that inspired it, the film has a Kafkaesque view of the world. But it is larger and more encompassing than the series: Davis paints with bold visual strokes so that the movie rises above its action-film origins and becomes operatic.|source=—Roger Ebert, writing for the Chicago Sun-Times |align=left|width=40%|fontsize=85%|bgcolor=#FFFFF0|quoted=2}} Desson Howe, writing in The Washington Post, channeled with an upbeat sentiment affirming, "A juggernaut of exaggeration, momentum and thrills — without a single lapse of subtlety — "Fugitive" is pure energy, a perfect orchestration of heroism, villainy, suspense and comic relief. Ford makes the perfect rider for a project like this, with his hangdog-handsome everyman presence. Hes one of us — but one of us at his personal best. Its great fun to ride along with him."  Left impressed, Rita Kempley also writing in The Washington Post, surmised how the filmed contained "Beautifully matched adversaries" deducing, "One represents the law, the other justice — and its the increasingly intimate relationship between them that provides the tension. Otherwise, "The Fugitive" would be little more than one long chase scene, albeit a scorchingly paced and innovative one."  In a mixed to positive review, Marc Savlov of The Austin Chronicle figured, "Director Davis valiantly tries to keep the breakneck, harried pace of an actual flight going throughout, and only occasionally drops the ball (the films convoluted conspiracy ending is the first example to beat me about the face and neck just now — others will crop up after deadline, Im sure)." On the lead actors performance he said, "Ford may be the closest thing we have these days to a Gary Cooper, but really, wheres David Janssen when we really need him?"  Owen Gleiberman of Entertainment Weekly, emphatically expressed that the film was about "two chases, two suspense plots running on parallel — and finally convergent — tracks. Kimble and Gerard spend the entire film on opposite sides of the law. Before long, though, we realize were rooting for both of them; theyre both protagonists, united in brains, dedication, superior gamesmanship. The films breathless momentum springs from their jaunty competitive urgency." 

The film however was not without its detractors. A columnist writing under the pseudonym GA for  . Retrieved 2012-12-23.  Rating the film with three stars, James Berardinelli of ReelViews professed, "Following the opening scenes, were treated to over a half-hour of nonstop action as Gerard and his men track down Kimble. Directed and photographed with a flair, this part of the movie keeps viewers on the edges of their seats. Most importantly, when on the run, Kimble acts like an intelligent human being. Equally as refreshing, the lawmen are his match, not a bunch of uniformed dunces being run around in circles." 

 
For the most part, satisfied with the quality of the motion picture, Jonathan Rosenbaum of the Chicago Reader said that "The mystery itself is fairly routine, but Joness offbeat and streamlined performance as a proudly diffident investigator helps one overlook the mechanical crosscutting and various implausibilities, and director Andrew Davis does a better-than-average job with the action sequences."  Leonard Klady writing in Variety (magazine)|Variety exclaimed, "This is one film that doesnt stint on thrills and knows how to use them. It has a sympathetic lead, a stunning antagonist, state-of-the-art special effects, top-of-the-line craftsmanship and a taut screenplay that breathes life into familiar territory."  Film critic Chris Hicks of the Deseret News accounted for the fact that the film "has holes in its plotting that are easy to pick apart and characters that are pretty thin, bolstered by the performances of seasoned vets who know how to lend heft to their roles." But in summary he stated, "the film is so stylish, so funny and so heart-stopping in its suspense that the audience simply doesnt care about flaws." 

===Box office===
The Fugitive opened strongly at the U.S. box office, grossing $23,758,855 in its first weekend and holding the top spot for six weeks.   It eventually went on to gross an estimated $183,875,760 in the U.S., and $185,000,000 in foreign revenue for a worldwide total of $368,875,760.  

===Accolades=== 100 most thrilling American Films. 

{|class="wikitable" border="1"
|- 
! Award
! Category
! Nominee
! Result
|- 1994 66th Academy Awards  Academy Award Best Picture Arnold Kopelson, Producer
| 
|- Academy Award Best Supporting Actor Tommy Lee Jones
| 
|- Academy Award Best Cinematography Michael Chapman
| 
|- Academy Award Best Film Editing Dennis Virkler, David Finfer, Dean Goodhill, Don Brochu, Richard Nord, Dov Hoenig
| 
|- Academy Award Best Original Score James Newton Howard
| 
|- Academy Award Best Sound Donald O. Mitchell, Michael Herbick, Frank A. Montaño, Scott D. Smith
| 
|- Academy Award Best Sound Editing John Leveque, Bruce Stambler
| 
|- 1994 American Annual ACE Eddie Awards  Best Edited Feature Film (Dramatic) Dennis Virkler, Don Brochu, Dean Goodhill, Richard Nord, David Finfer
| 
|- 1993 American 8th Annual ASC Awards  Theatrical Release Michael Chapman
| 
|- 1994 American ASCAP Film & Television Music Awards  Top Box Office Films James Newton Howard
| 
|- 1994 Japan Japan Academy Prize  Best Foreign Film 
|align="center" |————
| 
|- 1993 47th British Academy Film Awards  Sound
|align="center" John Leveque, Bruce Stambler, Becky Sullivan, Scott D. Smith, Donald O. Mitchell, Michael Herbick, Frank A. Montaño
| 
|- Actor in a Supporting Role Tommy Lee Jones
| 
|- Editing
|align="center" Dennis Virkler, David Finfer, Dean Goodhill, Don Brochu, Richard Nord, Dov Hoenig
| 
|- Achievement in Special Effects William Mesa, Roy Arbogast
| 
|- 1993 Chicago 6th Annual Chicago Film Critics Awards  Best Picture
|align="center" |————
| 
|- Best Director  Andrew Davis
| 
|- Best Supporting Actor Tommy Lee Jones
| 
|- 1993 Cinema Audio Society Awards  Outstanding Achievement in Sound Mixing for a Feature Film Donald O. Mitchell, Michael Herbick, Frank A. Montaño, Scott D. Smith 
| 
|- Directors Guild of America Awards 1993  Outstanding Directorial Achievement Andrew Davis
| 
|- 1994 Edgar Awards  Best Motion Picture Jeb Stuart, David Twohy
| 
|- 1994 51st Golden Globe Awards  Best Director - Motion Picture Andrew Davis
| 
|- Best Performance by an Actor in a Motion Picture - Drama Harrison Ford
| 
|- Best Performance by an Actor In A Supporting Role in a Motion Picture Tommy Lee Jones
| 
|- Kansas City Film Critics Circle Awards 1993  Best Supporting Actor Tommy Lee Jones
| 
|- 19th Annual Los Angeles Film Critics Association Awards 1993  Best Supporting Actor Tommy Lee Jones
| 
|- 1994 MTV Movie Awards  Best Movie
|align="center" |————
| 
|- Best Male Performance Harrison Ford
| 
|- Best On-Screen Duo Harrison Ford, Tommy Lee Jones
| 
|- Best Action Sequence Train Wreck
| 
|- National Society of Film Critics Awards 1993  Best Supporting Actor Tommy Lee Jones
| 
|- Southeastern Film Critics Association Awards 1993  Best Supporting Actor Tommy Lee Jones
| 
|- 1994 Writers Guild of America Award  Best Adapted Screenplay Jeb Stuart, David Twohy
| 
|-
|}

==Sequel==
Jones returned as Gerard in a 1998 sequel, U.S. Marshals (film)|U.S. Marshals. It also incorporates Gerards team hunting an escaped fugitive, but does not involve Harrison Ford as Kimble or the events of the initial 1993 feature. 

Also in 1998, a parody film   and Titanic (1997 film)|Titanic, the storyline revolves around Nielsens character being framed for a murder, as he escapes from federal custody to seek out the real suspect behind the crime. 

==See also==
 
* The Fugitive (TV series)|The Fugitive (TV series)
* 1993 in film

==References==
;Footnotes
 

;Further reading
 
* 
* 
* 
* 
 

==External links==
 
*   
*  
*  
*  
*  
*  

 

 
 
   
 
 
   
   
 
 
 
 
 
 
  
 
 
 
 
 
 
 
 
 