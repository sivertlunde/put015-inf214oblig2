Le désordre et la nuit
 
{{infobox film
| name = Le désordre et la nuit
| image = Le désordre et la nuit film poster.jpg
| caption = French theatrical release poster
| director = Gilles Grangier
| producer = Lucien Viard
| writer = Michel Audiard Gilles Grangier Jacques Robert
| based on =  
| starring = Jean Gabin Danielle Darrieux
| music = Henri Contet Jean Yatove
| cinematography = Louis Page
| editing = Jacqueline Sadoul
| runtime = 93 minutes
| released =  
| country = France
| language = French
}} French for "The disorder and the night") is a 1958 French crime film directed by Gilles Grangier and starring Jean Gabin and Danielle Darrieux. The screenplay is based on the novel Le désordre et la nuit by Jacques Robert. The film was released in the United States as Night Affair.

==Plot==
Georges Vallois (Gabin), a vice inspector for the Paris police, takes special interest in the plight of drug-addicted Lucky (Tiller), whom he considers to be more victim than criminal. Taking it upon himself to wean Lucky away from narcotics, Vallois also wins her love -- and, incidentally, smashes the dope ring responsible for her addiction.

==Cast==
* Jean Gabin as Inspecteur Georges Vallois
* Danielle Darrieux as Thérèse Marken
* Nadja Tiller as Lucky Fridel
* Hazel Scott as Valentine Horse Robert Manuel as Blasco
* Robert Berri as Marquis
* Harald Wolff as Herr Fridel
* Paul Frankeur as Inspecteur Chaville
* François Chaumette as Commissioner Janin
* Louis Ducreux as Henri Marken
* Roger Hanin as Simoni
* Gabriel Gobin as Inspecteur Rocard

==External links==
*  
*  
*   (French)
*   Last accessed: July 3, 2014.

 
 
 
 
 
 
 