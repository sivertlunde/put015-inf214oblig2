Dyden går amok
 
{{Infobox film
| name           = Dyden går amok
| image          = Dyden går amok 1966 Sven Methling poster Aage Lundvald.jpg
| caption        = Poster by Aage Lundvald
| director       = Annelise Reenberg
| producer       = Ditte Restorff
| writer         = Peer Guldbrandsen
| starring       = Birgit Sadolin
| music          = Sven Gyldmark
| cinematography = Aage Wiltrup
| editing        = Maj Soya
| distributor    = Saga Studio
| released       =  
| runtime        = 106 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}
 1966 film written and directed by Sven Methling.   

== Cast ==
* John Hahn-Petersen&nbsp;– Edward
* Birgitte Federspiel&nbsp;– Ina (Edwards hustru)
* Axel Strøbye&nbsp;– N.O.
* Louis Miehe-Renard&nbsp;– Joachim
* Bodil Steen&nbsp;– Andrea (Joachims hustru)
*  &nbsp;– Anna (serveringsdame i HU-Kroen)
* Morten Grunwald&nbsp;– Niels (fisker)
* Birgit Sadolin&nbsp;– Bertha (Niels kone)
* Carl Ottosen&nbsp;– Monni (fisker)
* Lise Thomsen&nbsp;– Elly (Monnis kone)
* Arthur Jensen&nbsp;– Ivar (fisker)
* Lily Broberg&nbsp;– Mille (Ivars kone)
*  &nbsp;– Marinus (fisker)
* Gunnar Lemvigh&nbsp;– Overbetjent Dørup
* Gabriel Axel&nbsp;– Pastor Deje
* Ole Monty&nbsp;– Stationsforstander
*  
* Bjørn Puggaard-Müller&nbsp;– Rektor

== References ==
 

== External links ==
*  
*  

 
 
 
 


 