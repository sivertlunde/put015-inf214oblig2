Minding the Baby
 

{{Infobox Film
| name           = Minding the Baby
| image          = 
| image_size     = 
| caption        = 
| director       = Dave Fleischer
| producer       = Max Fleischer
| animator       = Al Eugster 
| writer         = 
| narrator       = 
| starring       = Mae Questel
| music          = 
| cinematography = 
| editing        = 
| distributor    = Paramount Publix Corporation
| released       = 26 September 1931
| runtime        = 7 min 
| country        =    English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Minding the Baby is a 1931 Fleischer Studios Talkartoon animated short film starring Betty Boop and Bimbo.

==Synopsis==
Bimbos mom has fallen out with her husband and has had an affair with the ice man. Bimbos mother goes shopping and leaves Bimbo in charge of his baby brother, Aloysius. Betty Boop wants Bimbo to come over to her house to play. Bimbo then sneaks over to Bettys. Aloysius misbehaves while Bimbo is over at Bettys apartment. Aloysius then uses a vacuum cleaner and vacuums Betty and Bimbo from next door and his mother from off the street. Bimbos mother is furious, as Betty, Bimbo and Aloysius hide behind a chair in Bimbos apartment. Aloysius starts to cry, Bimbo then zips his brothers mouth shut.

==External links==
* 
* 

 
 
 
 
 


 