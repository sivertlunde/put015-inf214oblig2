Verónico Cruz (film)
{{Infobox film
| name           = Verónico Cruz
| image          = veronicocruzdvd.jpg
| image_size     =
| alt            =
| caption        = DVD cover Miguel Pereira
| producer       = Julio Lencina Sasha Menocki
| screenplay     = Miguel Pereira Eduardo Leiva Muller
| story          = Fortunato Ramos Gonzalo Morales
| music          = Jaime Torres
| cinematography = Gerry Feeny
| editing        = Gerry Feeny
| studio         = British Film Institute Mainframe Entertainment Channel Four Films Yacoraite Film Limitada
| released       =  
| runtime        = 96 minutes
| country        = Argentina United Kingdom
| language       = Spanish
| budget         =
}} Argentine and British drama Miguel Pereira, Gonzalo Morales, among others.  The author of the book, Fortunato Ramos, appears in the film in the opening scenes as Verónicos father. 

The drama tells the story of an Argentine elementary-school teacher sent by the government to a rural hamlet located in the northwestern province of Jujuy. It shows how he touches the lives of the villagers, especially the young and impressionable boy Verónico, whose mother died and father left to seek work when he was an infant.  The film is based on a non-fiction book written by Fortunato Ramos, a rural teacher in northwest Argentina, that discusses his teaching experiences.

==Plot==
The film is set in the mid 1970s and ends at the time of the 1982 Falklands War ( ) between Great Britain and Argentina.
 Gonzalo Morales) indigenous Argentine Andean highlands. hamlet in the Jujuy Province.

One day Mr. Lehrer (Juan José Camero) arrives in Chorcán to take the job as the new school teacher.  Verónico comes to idolize his new teacher, who is also known as el maestro as a sign of respect and affection.

At one point in the film el maestro takes Verónico on his first road trip to San Salvador de Jujuy, the capital of the Jujuy Province, to look for Verónicos father, who the boy has never met.  While there Lehrer is interrogated harshly by government authorities and discovers Castulo Cruz is considered a subversive by the military and that he probably has become a desaparecidos|desaparecido.

As Verónico Cruz learns about the outside world from his teacher, so too does Lehrer come to understand and appreciate the indigenous people who live in northwest rural Argentina and their history.

Lehrer, near the end of the film, is given a promotion and leaves Verónicos small village to teach at a larger school far away.
 navy and Argentine troops invade the Falkland Islands.

El maestro later gets a letter from Verónico, that he is serving at the ARA Belgrano as a crewman, at the same time that he gets news that the ARA Belgrano has been sunk by a British submarine.

==Cast==
* Juan José Camero as Lehrer, the teacher who befriends Verónico. Hes known as el maestro by the village folk. Gonzalo Morales ARA General Belgrano during the Falklands War.
* Fortunato Ramos as Castulo Cruz, who is Verónicos father and appears in the opening sequence and is seen leaving the hamlet in order to find work and feed his family.  He finds political trouble in the large city and never returns.  The film is based on Mr. Ramos work in northwest Argentina.
* Anna Maria Gonzales as the Grandmother, who is fearful for Verónico to learn about the cruel world and prefers that he not go to school. Yet she changes her mind and allows Verónico to attend classes.
* Juanita Caceres as Juanita, is Verónicos young love-interest.
* Don Leopoldo Aban as Don Domingo
* Guillermo Delgado as Policeman
* Rene Olaguivel as The Commissioner
* Titina Gaspar as Verónicos Mother
* Raul Calles as Officer
* Leo Salgado as Police Officer
* Luis Uceda as Soldier

==Production==
 

===Politics in Argentine films=== last military dictatorship in 1982.
 Night of the Pencils (1986), dealt frankly with the repression, the tortures, and the disappearances during Argentinas Dirty War in the 1970s. This second group of films uses metaphor and hints at wider socio-political issues.  

===Casting===
Miguel Pereira, in neorealism (art)|neo-realist fashion, used extras and bit players when he filmed in the Jujuy Province and Chorcán.

===Filming locations===
Filming locations include; Chorcán, Humahuaca Department (country subdivision)|Department; San Salvador de Jujuy; both in the Jujuy Province, Argentina. Jujuy Province is located in the northwest part of Argentina, high in the Andes. 

===Controversy in Britain===
According to the New Internationalist some of the English press was critical of the films funding.  The British Film Institute and Channel Four partly funded the film and the English provincial press called their action "treasonable."  The Falklands War was, after all, between Great Britain and Argentina they argued. 

==Distribution==
The film was first featured at the 38th Berlin International Film Festival in February 1988 where it won many awards, including the Silver Berlin Bear.    It opened in Argentina on August 4, 1988. The film was also shown at the Toronto Film Festival, Canada, on September 13, 1988.

In the United States the movie opened in New York City on January 5, 1990 and Los Angeles in March 1990.
 Foreign Language film category.

==Reception==

===Critical response=== British politically progressive magazine the New Internationalist, liked the film and wrote "  strung together like beads on a rosary, a slow-paced but highly polished series of tableaux...  beautifully shot with a lovely use of available light and low camera angles." 

The film critic for The New York Times, Janet Maslin, especially liked the look of the motion picture, and wrote, "Chorcán, where Verónico lives, is captured in all its bleak beauty...The film has been handsomely photographed in sunlight so clear that the shadows of clouds moving across hillsides stand out in sharp relief."  She was, however, disappointed by the slow pace of the film. 

===Accolades===
Wins
* 38th Berlin International Film Festival: Interfilm Award, Honorable Mention; OCIC Award, Honorable Mention; Silver Berlin Bear.  All for Miguel Pereira, 1988. 
* Bogota Film Festival: Special Prize, Miguel Pereira, 1989.
* Argentine Film Critics Association Awards: Silver Condor, Best Film, 1989.

Nominations
* Berlin International Film Festival: Golden Berlin Bear, Miguel Pereira, 1988.

==References==
 

==External links==
*  
*  
*   detailed plot summary at Epinions
*   at the cinenacional.com  
*   film clip at YouTube (El maestro arrives at the school)
*   biographical video at YouTube  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 