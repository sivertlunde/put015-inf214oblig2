Josh (2000 film)
{{Infobox film
| name           = Josh जोश  
| image          = Josh2000.jpg
| caption        = Movie poster
| writer         = Mansoor Khan
| starring       = Shahrukh Khan Aishwarya Rai  Chandrachur Singh
| director       = Mansoor Khan
| producer       = Ganesh Jain,  Ratan Jain,   Balwant Singh
| editing        = K Dilip
| distributor    = United Seven Creations
| cinematography = K. V. Anand
| released       = June 9, 2000
| runtime        =
| language       = Hindi
| music          = Anu Malik
| awards         = 2
| country        = India
| budget         =  
| gross          =    
| preceded by    =
| followed by    =
}}
 action crime film directed by Mansoor Khan. The film stars Shahrukh Khan and Aishwarya Rai in the lead roles, along with Chandrachur Singh, Sharad Kapoor, Priya Gill and Vivek Vaswani in supporting roles. The film released on 9 June 2000, and became the fourth highest-grossing film in India in 2000.    The film is a remake of West Side Story.

==Plot==

The film is set in a town called Vasco in Goa, named after Alberto Vasco, a Portuguese noble who owned most of the town during the colonial era. There are two rival gangs in Vasco, called Bichhoo (Hindi: Scorpion), and Eagles.

Bichoo gangs leader is Prakash (Sharad Kapoor), while the Eagles are led by Max (Shah Rukh Khan). They are all enemies, they fight, run, chase, and beat. But they are usually stopped from fights by Father Jacob, or the Town Inspector (Sharat Saxena). One day Prakashs brother Rahul (Chandrachur Singh) comes to visit after 2 years, who is a master-chef working in Mumbai. Rahul loves Goa the more he sees of it, and as he tours around Vasco, he meets Shirley (Aishwarya Rai), the twin sister of Max and he falls in love with her straightaway.

Knowing about Eagles and Bicchhoo rivalry, Rahul keeps his mouth closed. He opens a pastry shop Treat House and settles in Vasco. Meanwhile, he becomes part of the Eagles-Bichchoo rivalry, and gets closer to Shirley. The whole situation is unprecedented in this city, which gets all the more intriguing when Rahul comes across a secret about Max and Shirleys past; a secret that will change their lives forever, and that, which leads to a death.
He comes to know that Max and Shirley were actually the illegitimate children of Alberto Vasco, the founder of the town. Rahul wants to reveal this to Shirley in a letter but due to circumstances it falls in the hands of Prakash. Prakash plans to murder Max, to get his fathers lands worth Rupees 20 lacks. This leads to fight sequence between both, where Max accidentally shoots Prakash while defending himself. Max is arrested and his trial puts a rift between Shirley and Rahul. Max is about to go to the gallows, but the truth is finally revealed by Rahul and he acknowledges his brothers mistake.

The story ends with a happy note while Rahul and Shirley are married, Max too marries his love interest Roseanne.

==Cast==
{| class="wikitable"
|-
! Actor/Actress !! Role
|- Shah Rukh Khan || Max Dias
|- Aishwarya Rai || Shirley Dias
|- Chandrachur Singh || Rahul Sharma
|- Sharad Kapoor || Prakash Sharma
|- Priya Gill || Rosanne
|- Vivek Vaswani || Savio
|- Sharat Saxena || Inspector
|- Puneet Vashist || Michael
|- Sushant Singh || Ghotya
|- Nadira (actress)|Nadira || Mrs. Louise
|- Suhas Joshi || Rahuls mother
|-Vivekanand Yadav]] Vivek Don
|}

==Music==

A. R. Rahman was signed in as the music director first, but he opted out, due to time constraints and other commitments.  After Rahman opted out, Mansoor Khan roped in Anu Malik to complete the music fast, and release the film on time.
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| Apun Bola
| Hema Sardesai, Shahrukh Khan
| 04:25
|-
| 2
| Hai Mera Dil
| Udit Narayan, Alka Yagnik
| 04:08
|-
| 3
| Hum To Dil Se Haare
| Udit Narayan, Alka Yagnik
| 05:06
|-
| 4
| Mere Khayalon Ki Malika Abhijeet Bhattacharya
| 04:50
|-
| 5
| Zinda Hain Hum To
| Abhijeet Bhattacharya, Jolly Mukherjee, Hema Sardesai
| 04:44
|-
| 6
| Sailaru Sailare
| Mano (singer)|Mano, Suresh Peters
| 05:25
|-
| 7
| Hai Mera Dil (Instrumental)
|
| 04:08
|-
| 8
| Hum To Dil Se Haare (Instrumental)
|
| 05:07
|}

==Reception==
The   gave the film 7.5/10 and wrote "Mansoor succeeds as a director in keeping the movie fast and interesting, though it is the characters who come out much stronger than the movie".  Taran Adarsh from Bollywood Hungama noted "On the whole, JOSH is a well made film with great performances and a hit musical score. But the Goan ambience will restrict its prospects in some states due to lack of identification. Also, an average second half and a weak climax are major limitations".  Upon release, the film tasted the success at the box office collecting   33,00,00,000 

==References==
 

==External links==
*  

 
 
 
 