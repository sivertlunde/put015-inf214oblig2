Whistling in the Dark (1941 film)
{{Infobox film
| name           = Whistling in the Dark
| image_size     =
| image	         = Whistling in the Dark FilmPoster.jpeg
| caption        =
| director       = S. Sylvan Simon
| producer       = George Haight
| based on       =  
| writer         = Robert MacGunigle Harry Clork Albert Mannheimmer Eddie Moran (uncredited) Elliott Nugent (uncredited)
| narrator       =
| starring       = Red Skelton Conrad Veidt Ann Rutherford Virginia Grey
| music          = Bronislau Kaper Sidney Wagner
| editing        = Frank E. Hull
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Broadway play film adaptation of the same name.

The two sequels are Whistling in Dixie (1942) and Whistling in Brooklyn (1943).

==Plot==
Wally Benton (Red Skelton) is the star of a mystery series on radio, The Fox, which he writes himself, inventing for each episode a crime that has "only one loophole" so the criminal can be caught. He is about to elope with Carol Lambert (Ann Rutherford), but his agent (Eve Arden) tells him he must go out with the sponsors daughter, Fran Post (Virginia Grey), or risk his show being canceled.

Outside the city, at a mansion called Silver Haven, Joseph Jones (Conrad Veidt) runs a cult that promises serenity and contact with the dead, but is really just a scam to collect donations. He learns that a member has died and bequeathed a life interest in $1,000,000 to her nephew Harvey Upshaw (Lloyd Corrigan), with the principal going to Silver Haven on Upshaws death. Jones, angry that she did not leave the money directly to his cult, determines to have Upshaw killed at once, but he must avoid suspicion. Some of Joness henchmen are fans of The Fox and admire Bentons plots, so he gets the idea to kidnap Benton and make him invent a real crime with no loopholes. Learning that two different women are expecting to meet Benton that night and one is his fiancée, he has Lambert and Post kidnapped as well.
 substitutes another harmless one. However, this gets lost and another packet is made up with real poison: Upshaw is in real danger. And so are Benton, Lambert, and Post, as Jones will have no reason to let them live once the plot succeeds.
 simulate dialing 0, but cannot talk to the operator without a telephone. But, noticing a radio in the room, he uses his knowledge of radio to improvise a way to use it as a phone: the loudspeaker will double as a microphone if they swap the right wires back and forth each time he starts or finishes talking.

He reaches the telephone operator and tries to get a message sent to Upshaws flight, but Joness man on the flight finds a way to disable the aircraft radio, and contact is lost. Benton then has the operator contact his radio station, where it is almost time for The Fox. They patch his phone call through to the transmitter and he begins telling all his listeners what has been happening.

But they have to speak loudly, and the one dim-witted guard who was left outside the door bursts in. They save themselves by convincing him they are just passing the time by pretending to do a radio show. He finds the idea fun and even participates, adding useful information such as the mansions address. Unfortunately, the local police assume it is fiction, like the recent The War of the Worlds (radio drama)|War of the Worlds broadcast, so the three hostages are not saved until Bentons friends from the radio station arrive with city police.
 take them out".
 

==Cast==
* Red Skelton as Wally Benton
* Conrad Veidt as Joseph Jones
* Ann Rutherford as Carol Lambert
* Virginia Grey as "Fran" Post
* Rags Ragland as Sylvester (as "Rags" Ragland)
* Henry ONeill as Philip Post
* Eve Arden as "Buzz" Baker Paul Stanton as Jennings
* Don Douglas as Gordon Thomas
* Don Costello as "Noose" Green William Tannen as Robert Graves
* Reed Hadley as Beau Smith
* Mariska Aldrich as Hilda
* Lloyd Corrigan as Harvey Upshaw
* George M. Carleton as Deputy Commissioner ONeill (as George Carleton)
* Will Lee as Herman
* Ruth Robinson as Mrs. Robinson

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 