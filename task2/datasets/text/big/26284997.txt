The Lady Confesses
{{Infobox film
| name           = The Lady Confesses
| image          =Poster of the movie The Lady Confesses.jpg
| image_size     =
| caption        =
| director       = Sam Newfield
| producer       = Alfred Stern (producer)
| writer         = Irwin Franklyn (story) Helen Martin (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Jack Greenhalgh
| editing        = Holbrook N. Todd
| distributor    = Producers Releasing Corporation
| released       = May 16, 1945
| runtime        = 64 minutes
| country        = USA
| language       = English
| budget         =

}}

The Lady Confesses is a 1945 American Film noir directed by Sam Newfield.

==Plot summary==
While on the verge of being divorced, Norma Craig disappears. Seven years later, when her husband, Larry Craig, plans to marry a girl Vicki McGuire, Norma returns and tells Vicki that she nor anybody else can marry Larry. Soon both the girl and her fiance find themselves mixed up with a crooked nightclub owner, gangsters and murder  

==Cast==
*Mary Beth Hughes as Vicki McGuire
*Hugh Beaumont as Larry Craig
*Edmund MacDonald as Lucky Brandon
*Claudia Drake as Lucile Compton
*Emmett Vogan as Police Capt. Brown Barbara Slater as Norma Craig Edward Howard as Detective Harmon
*Dewey Robinson as Steve

==Soundtrack==
* "Dance Close To Me, Darling" (Written by Robert Unger and Al Seaman)
* "Its All Your Fault" (Written by Cindy Walker)
* "Its A Fine Old World" (Written by Smith, Kuhstos and Blonder)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 