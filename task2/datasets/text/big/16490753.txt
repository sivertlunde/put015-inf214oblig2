Nandalala
{{Infobox film
| name = Nandalala
| image = nandalala.jpg
| director = Mysskin
| producer = K. Karunamoorthy C. Arunpandian
| writer = Mysskin
| starring = Mysskin Snigdha Akolkar Ashwath Ram
| music = Ilayaraaja
| cinematography = Mahesh Muthuswamy
| studio =  Ayngaran International
| distributor = 
| runtime = 125 minutes
| released =  
| language = Tamil
| country = India
| budget = 
}}
Nandalala ( ) is a 2010 Tamil drama film written and directed by Mysskin. His himself plays the lead role, alongside newcomer Ashwath Ram and Snigdha Akolkar. The film, which is produced by Ayngaran International and features a highly acclaimed musical score by Ilaiyaraaja, is loosely based on the 1999 Japanese film Kikujiro,     and partly inspired from Myshkins life.    

The film illustrates the road journey of two people, a mentally challenged adult and an eight-year-old schoolboy, both in search of their respective mothers. Myshkin began developing Nandalala in 2006, penning the scriptment for eight months. Supposed to be filmed, after the release of Myshkins debut film Chithiram Pesuthadi (2006), the film was shelved since no producer came forward to fund the film, and was launched only in June 2008,    after the release of Myshkins second film Anjathe (2008). It was completed by December 2008,  but got stuck in development hell later, with no distributors willing to release the film. Following numerous preview shows and screenings at several film festivals, the film eventually released on November 26, 2010, opening to very positive reviews and garnering critical acclaim.          

== Cast == Myshkin as Bhaskar Mani
* Ashwath Ram as Akhilesh
* Snigdha Akolkar as Anjali Rohini
* Nassar
* Kalaiyarasan
* Leena Maria Paul

== Production ==

=== Development === Myshkin had Saran was subsequently approached by Myshkin to produce the film, but as he wasnt impressed by the storyline and suggested some changes, which Myshkin didnt agree to, he, too, dropped the film.  Myshkin decided to postpone the project, since "nobody was interested",    and instead wrote a new story and commenced a new project, which itself was a result of Myshkins anger.     Myshkin later revealed, that in spite of demands from the producers, he didnt make any compromises in this film, in contrast to his previous ventures, as Nandhalala was his "dream project". 
 Rohini was trailer was premiered in July 2009 along Sundar C.s Aiyantham Padai. 

=== Music ===
{{Infobox album 
| Name = Nandalala
| Type = Soundtrack
| Artist = Ilaiyaraaja
| Cover = 
| Released = January 14, 2009
| Recorded = 2008 Feature film soundtrack
| Length = Ayngaran Music An Ak Audio
| Label =
| Producer = Ilaiyaraaja
| Last album = Naan Kadavul (2009)
| This album = Nandalala (2009)
| Next album = Bhagyadevatha (2009)
}} gypsy woman called Saroja Ammal in her mother tongue.  Apart from Saroja Ammal, five lyricists had penned the songs, namely Na. Muthukumar, Mu. Mehta, Muthulingam, Kabilan and Pazhani Bharthi.
 Hungarian artists from the Budapest Festival Orchestra were invited.  

{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    = 
| lyrics_credits  = yes
| title1          = Melle Oorndhu Oorndhu
| lyrics1         = Na. Muthukumar
| extra1          = Ilaiyaraaja
| length1         = 
| title2          = Onnukkonnu
| lyrics2         = Muhammed Metha
| extra2          = K. J. Yesudas
| length2         = 
| title3          = Thalaattu Ketka Nanum
| lyrics3         = Ilaiyaraaja
| extra3          = Ilaiyaraaja
| length3         = 
| title4          = Kai Veesi
| lyrics4         = Pazhani Bharathi
| extra4          = Vijay Yesudas, Swetha Mohan, Madhu Balakrishnan, Raagul, Chandrasekar
| length4         =
| title5          = Oru Vandu Koottame
| lyrics5         = Kabilan Vairamuthu
| extra5          = Ilaiyaraaja, Master Yatheeswaran
| length5         = 
| title6          = Elilea Elilea
| lyrics6         = Saroja Ammal
| extra6          = Saroja Ammal
| length6         = 
}}

== Release ==
After the films post-production works were finished in early 2009, Nandalala was evading release dates for over a year, mainly since distributors were fearing and unwilling to distribute the film due to its offbeat theme. Finally Ayngaran International was able to release the film on 26 November 2010, one-a-half years after its completion, facing opposition from newcomer Sripathys Kanimozhi (film)|Kanimozhi. Nandalala took the better opening at the Chennai box office, opening at second position, ranking just behind Mynaa, while earning   20,89,502 from 146 screens.  The film was subsequently crowded out at the box-office by new releases, becoming an average grosser.

=== Reviews ===
Before the wide theatrical release on 26 November, the film was screened several times at film festivals and as preview shows to prominent filmmakers, actors and distributors. Following one such preview show, actor-director Kamal Haasan had described the film as an "excellent, must-watch film", recommending Sun Pictures inter alia to distribute the film.  At the Norway Film Festival, the film had won the Critics as well as the Peoples Choice Award. 

After its theatrical release, the film gained mostly highly positive reviews and praise, with most critics claiming that Myshkin had made a meaningful film that marks a milestone in Tamil cinema.    The Hindu s Malathi Rangarajan, too gave a very positive verdict, calling it the "real road film" that is made "for seekers of worthy cinema". Further praising the director, she cites that Myshkin "rises high above the standards he set with the earlier quality cocktails" and that "Tamil cinema needs more creators like him".  Pavithra Srinivasan of Rediff rated the film four out five describing Nandalala as "brilliant",  eventually finding a place in the list of her "Best Tamil films of 2010" article,  while a review from Indiaglitz labelled the film as "mindblowing and marvellous", further claiming that Myshkin "seems to have taken Tamil cinema to a new height". 

A Behindwoods reviewer, too, gave the film four out of five, describing the film as "Picture Perfect" that "transcends genres and even barriers of language".  Top10Cinema s reviewer labelled the film as Mysskins "yet another trademark",  while, as per Sify.com, a "popular English daily" had given it four and a half out five, the highest rating it has given for any Tamil film.  Meanwhile, Sify critic Sreedhar Pillai in his review described Nandalala as a "another heart wrenching story" that was "slow and melancholic", claiming it was for people "like films that explores the realms of anguish and despair". 

=== Plagiarism ===
The film also drew scattered negative reviews for its inspiration from Kikujiro. Whilst the Deccan Chronicle review tributed Ilayaraaja for an "outstanding" background score, it criticized Myshkin for replicating "not just the concept, but almost the entire graph of the plot and narration" of the original, labelling it as "blatant plagiarism". The critic added that it is "an embarrassment to the maker, but to the viewer too". 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 