The Young Fritz
{{Infobox film
| name           = The Young Fritz
| image          = 
| image_size     = 
| caption        = 
| director       = Grigori Kozintsev Leonid Trauberg
| producer       = 
| writer         = Samuil Marshak
| narrator       = 
| starring       = Mikhail Zharov
| music          = Lev Shvarts
| cinematography = Andrei Moskvin	 	
| editing        = 
| distributor    = 
| studio         = Lenfilm
| released       =  
| runtime        = 30 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}
 Soviet film directed by Grigori Kozintsev and Leonid Trauberg based on a short satiric poem by Samuil Marshak. The film was banned by censors, never released, and is believed to be lost. 

==Cast==
* Mikhail Zharov - Fritz
* Maksim Shtraukh - Examining professor
* Mikhail Astangov - Teacher
* Vsevolod Pudovkin - Officer
* Yanina Zhejmo
* Mikhail Vysotsky - Father
* Lydia Atmanaki - Mother
* Lyudmila Shabalina - Tour guide in Museum of the Future
* Konstantin Sorokin - Franz

==External links==
* 

 

 
 
 
 
 
 
 
 

 