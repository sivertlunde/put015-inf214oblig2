Imitation (film)
{{Infobox film
| name           = Imitation 
| image          = Imitation-by-federico-hidalgo.jpg
| caption        = Theatrical release poster
| director       = Federico Hidalgo
| producer       = Pascal Maeder
| writer         = Federico Hidalgo Paulina Robles
| starring       = Vanessa Bauche Jesse Aaron Dwyre Conrad Pla
| music          = Robert M. Lepage
| cinematography = Jean-Pierre St-Louis
| editing        = Tony Asimakopoulos Atopia
| released       =  
| runtime        = 87 minutes
| country        = Canada
| language       = English
}} Atopia

==Plot==
When Teresa (played by Vanessa Bauche) was left in Mexico by a husband who hiked it up to Montreal, Canada, she followed in search of him with the help of Fenton (played by Jesse Aaron Dwyre), a grocery-store stock boy, but little does her new companion know of the mystery surrounding this mistress and her equally enigmatic ex-suitor. This emotionally ambitious and frequently comedic feature follows the two as they travel across Montreal, often encountering colorful characters who lead them to the final denouement, where Teresa will have more explaining to do than she ever expected.

==External links==
* 

 
 
 
 
 
 
 
 


 
 