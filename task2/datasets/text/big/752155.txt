Runaway Jury
 
{{Infobox film
| name              = Runaway Jury
| image             = Runaway_jury.jpg
| caption           = Theatrical release poster
| director          = Gary Fleder
| producer          = Gary Fleder Christopher Mankiewicz Arnon Milchan Matthew Chapman
| based on          =  
| starring          = John Cusack Gene Hackman Dustin Hoffman Rachel Weisz
| music             = Christopher Young
| cinematography    = Robert Elswit
| production design = Nelson Coates
| editing           = William Steinkamp Jeff Williams
| studio            = Regency Enterprises
| distributor       = 20th Century Fox
| released          =  
| runtime           = 127 minutes
| country           = United States
| language          = English
| budget            = $60 million   Box Office Mojo 
| gross             = $80.2 million 
}}
 thriller film directed by Gary Fleder and starring John Cusack, Gene Hackman, Dustin Hoffman, and Rachel Weisz. It is an adaptation of John Grishams novel The Runaway Jury. 

==Plot==
In New Orleans, a failed day trader at a stock brokerage firm shows up at the office and opens fire on his former colleagues, then kills himself. Among the dead is Jacob Wood. Two years later, with pro bono attorney Wendell Rohr, Jacobs widow Celeste takes Vicksburg Firearms to court on the grounds that the companys gross negligence led to her husbands death.
 
During jury selection, jury consultant Rankin Fitch and his team communicate background information on each of the jurors to lead defense attorney Durwood Cable in the courtroom through electronic surveillance.
 Marine veteran, takes an instant dislike to him.

A woman named Marlee makes an offer to Fitch and Rohr: she will deliver the verdict to the first bidder. Rohr dismisses the offer, assuming it to be a tactic by Fitch to obtain a mistrial. But Fitch asks for proof that she can deliver, which Nick provides. Fitch orders Nicks apartment searched, but finds nothing. Marlee retaliates by getting one of Fitchs jurors bounced. Nick shows the judge surveillance footage of his apartment being searched and the judge orders the jury sequestered. Fitch then goes after three jurors with blackmail, leading one, Rikki Coleman, to attempt suicide.

Rohr loses a key witness due to harassment, and after confronting Fitch, decides that he cannot win the case. He asks his firms partners for $10&nbsp;million. Fitch sends an operative, Janovich, to kidnap Marlee, but she fights him off and raises Fitchs price to $15&nbsp;million. On principle, Rohr changes his mind and refuses to pay. Fitch agrees to pay Marlee to be certain of the verdict.

Fitchs subordinate Doyle travels to Gardner, Indiana, where he discovers that Nick is really Jeff Kerr, a law school drop-out, and that Marlees real name is Gabby Brandt. Gabbys sister died in a school shooting. The town sued the gun manufacturer and Fitch helped the defense win the case. Doyle concludes that Nick and Marlees offer is a set-up, and he calls Fitch, but it is too late.

Nick receives confirmation of receipt of payment and he steers the jury in favor of the plaintiff, much to the chagrin of Herrera, who launches into a rant against the plaintiff, which undermines his support. The gun manufacturer is found liable, with the jury awarding $110&nbsp;million in general damages to Celeste Wood.

After the trial, Nick and Marlee confront Fitch with a receipt for the $15&nbsp;million bribe and demand that he retire. They inform him that the $15&nbsp;million will benefit the shooting victims in Gardner.

==Cast==
 
* John Cusack as Nicholas Easter
* Gene Hackman as Rankin Fitch
* Dustin Hoffman as Wendall Rohr
* Rachel Weisz as Marlee
* Jeremy Piven as Lawrence Green
* Bruce Davison as Durwood Cable
* Bruce McGill as Judge Frederick Harkin
* Marguerite Moreau as Amanda Monroe
* Nick Searcy as Doyle
* Leland Orser as Lamb
* Lori Heuring as Maxine
* Nestor Serrano as Janovich
* Joanna Going as Celeste Wood
* Dylan McDermott as Jacob Wood (uncredited)
* Stanley Anderson as Henry Jankle
* Celia Weston as Mrs. Brandt
* Stuart Greer as Kincaid
* Gerry Bamman as Herman Grimes
* Bill Nunn as Lonnie Shaver
* Cliff Curtis as Frank Herrera
* Nora Dunn as Stella Hulic
* Rusty Schwimmer as Millie Dupree
* Jennifer Beals as Vanessa Lembeck
* Guy Torry as Eddie Weese
* Henry Darrow as Sebald 
* Ed Nelson as George Dressler 
* Orlando Jones as Russell
* Gary Grubbs as Dobbs
* Marco St. John as Daley
* Rhoda Griffis as Rikki Coleman
* Luis Guzmán as Jerry Hernandez 
* Corri English as Lydia Deets
 

==Production==
The film had been in pre-production since 1997. Directors slated to helm the picture included   was released, necessitating a plot change from tobacco to gun companies. 

==Revenue==
The film grossed $49,440,996 in the United States and $80,154,140 worldwide.   

==Reception==
Runaway Jury received generally positive reviews from critics, garnering a 73&nbsp;percent rating on Rotten Tomatoes, with the site calling the film "An implausible but entertaining legal thriller." 
Roger Eberts critique of this film stated that the plot to sell the jury to the highest-bidding party was the most ingenious device in the story because it avoided pitting the "evil" and the "good" protagonists directly against each other in a stereotypical manner, but it plunged both of them into a moral abyss. 
John Grisham said it was a "smart, suspenseful" movie, and was disappointed it made so little money. 

==References==
 

==External links==
 
*  
*  
*  
*  
*   at The Numbers

 
 

 
 
 
 
 
 
 
 
 
 
 
 