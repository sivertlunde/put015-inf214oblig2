Retour à l'aube
{{Infobox film
|image=1938 Retour a l aube.jpg
|caption=Theatrical poster
|director=Henri Decoin
|writer=Henri Decoin   Vicki Baum  Pierre Wolff
|starring=Danielle Darrieux
|music=Paul Misraki
|cinematography=Léonce-Henri Burel    
|released=1938
|runtime=80 min
|country=France
|language=French
}} 1938 cinema French drama film starring Danielle Darrieux, and was directed by Henri Decoin, who co-wrote screenplay with Vicki Baum and Pierre Wolff. The music score is by Paul Misraki. It was filmed in Budapest, Hungary.

==Plot==
It tells the story of a stationmasters wife, who must go to Budapest to get an inheritance, but is late for a train and experiences much adventure during one night.

==External links==
* 
* 
*  at filmsdefrance.com

 
 
 
 
 


 
 