Earth Girls Are Easy
 
{{Infobox film
| name           = Earth Girls Are Easy
| image          = Earth Girls Are Easy.jpg
| caption        = Theatrical release poster
| director       = Julien Temple
| producer       = Tony Garnett Duncan Henderson Terrence E. McNally Charlie Coffey Terrence E. McNally
| starring       = Geena Davis Jeff Goldblum Damon Wayans Jim Carrey
| music          = Ray Colcord Nile Rodgers Julie Brown
| cinematography = Oliver Stapleton
| editing        = Richard Halsey
| studio         = De Laurentiis Entertainment Group
| distributor    = Vestron Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $10 million (estimated)
| gross          = $3,916,303
}}
 1988 American musical romantic comedy film directed by Julien Temple and stars Geena Davis, Julie Brown, Jeff Goldblum, Damon Wayans, and Jim Carrey.  The plot is based on the song "Earth Girls Are Easy" from Julie Browns 1984 mini-album Goddess In Progress. 

== Plot ==
The film begins with three furry aliens--the blue Mac (Goldblum), the yellow Zeebo (Wayans), and the red Wiploc (Carrey)--traveling in a space ship. Its been a long time since theyve had female companionship, and they receive a broadcast showing human females.  They are titillated by these "hairless", shapely creatures and discover that the broadcast came from Earth, so they set off toward Earth and Southern California.
 manicurist at the "Curl Up & Dye" hair salon. When she feels her cold fiancé Dr. Ted Gallagher (Charles Rocket) is slipping away, she decides to seduce him with a new look; by stripping off her clothes to her white corset, suspenders, underwear, stockings and pink high heels. Instead, she catches him cheating on her with his nurse. She kicks him out, smashes his stuff and refuses to see him again. The next day, she is sunbathing when the aliens spaceship crash lands in her pool. 
She befriends them and calls her friend Woody (Michael McKean) to come and drain the pool so the aliens can work on their ship and get it flying again. Meanwhile, she brings them into her home; and, though there is a language barrier at first, the aliens prove to be quick learners and absorb American pop culture and language by watching television.

Wanting them to blend in to their surroundings, Valerie takes them to her friend Candy Pink (Julie Brown|Brown, co-writer and co-producer) at the salon. After shaving off the aliens fur, they turn out to be human looking and attractive. They all go out; and party at Los Angeles nightclubs where their looks, athleticism and incredibly long tongues soon make them the envy of every female in the place. Valerie and Mac begin to fall for each other and go back to Valeries place. There, they find out that they are anatomically compatible and make love.

The next day the pool is drained, and Zeebo and Wiploc are working on their ship when Woody stops by and offers to take them to the beach. They agree; and, after accidentally holding up a convenience store, Zeebo and Wiploc are soon driving down the L.A. Freeway the wrong way, in reverse, with the police in pursuit.

Mac finds out his crew mates are in trouble and goes to help and gets arrested along with Woody in a case of mistaken identity. Valerie smashes the police vehicle to get arrested, too, so she can go with Mac.

The police pursuit ends in a crash, and Zeebo and Wiploc are taken to the Emergency Room at the local hospital. There, they are examined by Ted, who discovers they have two hearts. While he is envisioning achieving fame and fortune from his discovery, Valerie and Mac elude the police and enter the E.R. disguised as a doctor and a nurse; they manage to convince Ted he is delusional. They then escape back to Valeries place where work continues on the space ship.  Meanwhile, Valerie and Ted reconcile and plan to go to Las Vegas to get married right away.

Mac is heartbroken and prepares the ship for take-off. Valerie comes out to say good-bye, followed by Ted, who discovers the ship. While she is struggling to keep him from calling the authorities, Valerie comes to the realization that its really Mac she loves. She gets in the ship, and off they head into the sky.

==Cast==
*Geena Davis as Valerie Gail, a manicurist who lives in the San Fernando Valley in Southern California
*Jeff Goldblum as Mac, the chief alien
*Jim Carrey as Wiploc, an alien
*Damon Wayans as Zeebo, an alien
*Julie Brown as Candy Pink, Valeries friend and co-worker
*Michael McKean as Woody, Valeries pool boy, an aging surf bum
*Charles Rocket as Dr. Ted Gallagher, Valeries philandering fiancé, a medical doctor
*Larry Linville as Dr. Bob
*Rick Overton as Dr. Rick
*Angelyne as herself

==Production== Absolute Beginners, Madonna  and Molly Ringwald,  but when they rejected it, WB dropped the project. Several other studios expressed interest in producing the movie, but none wanted Temple to direct.  Ultimately French bank Crédit Lyonnais agreed to finance the film with Temple at the helm (if $4 million was shaved off of the films estimated $14 million budget)  and the De Laurentiis Entertainment Group agreed to distribute it. 

Principal photography was finally underway in late 1987    and Temple brought his own ideas to the table, including peppering the background with then modern sounding pop songs,    featuring an homage to The Nutty Professor    and using iconic model/actress Angelyne in a brief cameo (the director declared her "the patron Saint of Los Angeles" ), but Temples studious eye for detail caused delays on the set  and according to producer Tony Garnett, "The first cut we had of the picture was a problem."  The film underwent more than five months of post-production tinkering,  including the removal of numerous scenes and the production number "I Like em Big and Stupid" (a different version of the song plays in the club; the deleted sequence appears on the DVD extras) and reshoots later commenced (the song "Cause Im a Blonde" was injected into the film late in the game), by which time the De Laurentiis Entertainment Group had filed for bankruptcy. 
 New World, MGM and 20th Century Fox, but ultimately Vestron Pictures picked up the distribution rights.  The film debuted at the Toronto Film Festival in September 1988  and was slated to be released the following February,  but legal entanglements delayed its release until May 1989. 

==Reception==
The film received generally mixed, but mostly positive reviews. Roger Ebert concluded, "Earth Girls Are Easy is silly and predictable and as permanent as a feather in the wind, but I had fun watching it."   Leonard Maltin called it an "infectiously goofy musical"    and went on to cite some "good laughs and an endearing performance by Davis."  Pittsburgh Post-Gazette editor George Anderson gushed over the absurdity of the movie, declaring the film "is so cheerful about so many stupid things that you cannot, in good conscience, endorse it, but you may be tempted to adopt it."   Some criticized the film for being "less a movie than a stretched-out, padded   video."   Box office returns were low; the film garnered only a little over a third of the $10 million production cost —but the movie ultimately developed a cult following, mainly due to Jim Carreys later success as a film comedian, which strongly renewed interest in his earlier films. Earth Girls Are Easy currently holds a 66% rating on Rotten Tomatoes based on 32 reviews.

==Award nominations==
 
Fantasporto
*Nominated: Best Film, Julien Temple (1990)

Independent Spirit Awards
*Nominated: Best Cinematography, Oliver Stapleton (1990)

Golden Raspberry Awards
*Nominated: Worst Supporting Actress, Angelyne (1990) (To note, Angelyne appears in the film for less than two minutes.)

==Soundtrack==
 soundtrack album cassette  CD  by Sire Records on May 9, 1989    to coincide with the May 12 release of the film.  Most of the songs on the album are different mixes than were heard in the movie, several songs from the film were omitted altogether and Geena Daviss song "The Ground You Walk On" was replaced with a rendition by Jill Jones. The album is out of print.
 The N. 

{{tracklist
| collapsed       = 
| headline1       = 
| extra_column    = Performer
| writing_credits = yes
| title1          = Love Train 
| writer1         = Kenny Gamble, Leon Huff
| extra1          = Hall & Oates
| length1         = 3:45
| title2          = Baby Gonna Shake
| writer2         = Stephen Bray, Linda Mallah
| extra2          = Royalty
| length2         = 4:24
| title3          = Hit Me 
| writer3         = Paul Robb Information Society
| length3         = 5:08
| title4          = The Ground You Walk On 
| writer4         = Billy Steinberg, Tom Kelly
| extra4          = Jill Jones
| length4         = 4:15
| title5          = Earth Girls Are Easy 
| writer5         = Julie Brown, Charles Coffey, Terrence McNally, Sterling Smith The N
| length5         = 3:43
| title6          = (Shake That) Cosmic Thing
| writer6         = Kate Pierson, Cindy Wilson, Fred Schneider, Keith Strickland
| extra6          = The B-52s
| length6         = 3:51 Route 66 (The Nile Rodgers Mix)  
| writer7         = Bobby Troup
| extra7          = Depeche Mode
| length7         = 4:09 Who Do You Love?
| writer8         = Ellas McDaniel
| extra8          = The Jesus and Mary Chain
| length8         = 4:04
| title9          = Throb 
| writer9         = Stewart Copeland
| extra9          = Stewart Copeland
| length9         = 2:09
| title10         = Brand New Girl 
| writer10        = Julie Brown, Charles Coffey, Dennis Poore
| extra10         = Julie Brown
| length10        = 3:42
| title11         = Cause Im a Blonde
| writer11        = Julie Brown, Charles Coffey, Dennis Poore
| extra11         = Julie Brown
| length11        = 2:15
}}

==Stage show==
Beginning on September 16, 2001,    there were several staged reading/performances of a musical play version of the film. Based on the movies screenplay and written by Charlie Coffey and Michael Herrmann, Julie Brown reprised her role of Candy, Kristin Chenoweth took over the role of Valerie, Marc Kudisch assumed the role of Ted and Hunter Foster was cast as Mac.   Although costumes and props were utilized, there were no sets and the actors carried their scripts around the stage—these stagings were merely devised to find investors for the show. 

The play did not feature any original songs; the performers sang renditions of 80s pop songs  along with several numbers from the film. The play followed the films story and scenes pretty closely, but a lot of new dialogue was written, a few characters were omitted and there were some other slight deviations here and there. Audio and video recordings of the September 30, 2002 staging are circulating, and several video clips from this performance have surfaced on YouTube.

Despite positive reaction,  the timing of the initial staging was bad (coming mere days after the September 11 attacks), and even after subsequent readings, the show never attained the investors needed to become a full-blown production.

=== Cast ===
*Kristin Chenoweth as Valerie Gail
*Julie Brown as Candy Pink
*Marc Kudisch as Dr. Ted Gallagher
*Hunter Foster as Mac
*Deven May as Wiploc
*Lisa Capps as Nurse/Ensemble
*Roxanne Barlow as Security Guard/Ensemble
*Steve Wilson as Security Guard/Ensemble

===Musical numbers===
 
 
;Act I
* "(Shake That) Cosmic Thing" - The B-52s (heard on radio)
* "Earth Girls Are Easy" - The N (heard on radio) Eternal Flame" - Valerie
* "Brand New Girl" - Candy, Valerie & Ensemble
* "Physical (Olivia Newton-John song)|(Lets Get) Physical" - Ted and Nurse Heart of Glass" - Valerie
* "California Girls" (partial) - Mac and Wiploc
* "Funkytown" - Ensemble
* "I Like em Big and Stupid" - Candy

 
;Act II True Colors" - Mac
* "Atomic (song)|Atomic" - Valerie
* "Roam" - Entire Cast
* "Just Like Fred Astaire" - Mac & Valerie
* "Cause Im a Blonde" - Candy
* "Should Have Known Better" - Ted
* "Moonblind" - Mac and Valerie
* "Scary Kisses" - Valerie, Mac & Cast

 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 