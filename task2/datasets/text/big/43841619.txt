Beat the Band (film)
{{Infobox film
| name           = Beat the Band
| image          = Beat the Band poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = John H. Auer
| producer       = Michael Kraike 
| screenplay     = Lawrence Kimble Clarence Kimble Arthur A. Ross
| based on       =  
| starring       = Frances Langford Ralph Edwards Phillip Terry Gene Krupa June Clayworth
| music          = Leigh Harline
| cinematography = Frank Redman 
| editing        = Samuel E. Beetley 
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Beat the Band is a 1947 American musical film directed by John H. Auer and written by Lawrence Kimble, Clarence Kimble and Arthur A. Ross. The film stars Frances Langford, Ralph Edwards, Phillip Terry, Gene Krupa and June Clayworth.    The film was released on February 19, 1947, by RKO Pictures.

==Plot==
 

== Cast ==
*Frances Langford as Ann Rogers
*Ralph Edwards as Eddie Martin
*Phillip Terry as Damon Dillingham
*Gene Krupa as Himself
*June Clayworth as Willow Martin
*Mabel Paige as Mrs. Peters
*Andrew Tombes as Professor Enrico Blanchetti / Mr. Dillingham
*Donald MacBride as P. Aloysius Duff
*Mira McKinney as Mrs. Elvira Rogers
*Harry Harvey, Sr. as Mr. Rogers
*Grady Sutton as Harold

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 