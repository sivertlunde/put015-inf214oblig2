Must Love Dogs
{{Infobox film
| name           = Must Love Dogs
| image          = Must_Love_Dogs.jpg
| caption        = Theatrical Poster
| director       = Gary David Goldberg
| producer       = Polly Cohen Brad Hall Ronald G Smith
| writer         = Claire Cook Gary David Goldberg
| narrator       =
| starring       = Diane Lane John Cusack Elizabeth Perkins Christopher Plummer Stockard Channing Ali Hillis Dermot Mulroney Colin Egglesfield Jordana Spiro  Craig Armstrong Susie Suh Vinnie Zummo John Bailey
| editing        = Roger Bondelli Eric A. Sears
| studio         =
| distributor    = Warner Bros.
| released       = July 29, 2005
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $30 million   
| gross          = $58,405,313 
}} 2005 romantic comedy film based on Claire Cooks eponymous 2002 novel. Starring Diane Lane and John Cusack, it is the third film directed and written by Gary David Goldberg and was produced on a budget of $30 million.  The film focuses on a womans struggle with divorce and meeting new people afterward.

Production started on October 12, 2004  with a final release date of July 29, 2005.  Critics opinions were mostly negative but indicated that the actors were not to blame.  Must Love Dogs took the fifth spot on its opening weekend and has grossed more than $58 million worldwide.  The film was released on VHS and DVD on December 20, 2005.

==Plot==
Sarah Nolan, a 40-year-old divorced preschool teacher, is urged by her family to date more. Although they show her photos of men they want to set her up with, Sarah does not seem interested in pursuing any relationships.  Jake Anderson, another recent divorcee, finds himself in a similar position; his lawyer, Charlie, wants to set him up with a woman named Sherry.  However, Jake would rather focus on creating his handcrafted boats.
 Newfoundland dog, "Mother Teresa", while he goes through his own marital problems). Sarah proceeds to suffer through several disastrous dates with men who cannot stop crying, are criminal, or like girls who are barely legal.

Jake is confronted by Sherry at an art gallery; she is curious why he did not call her, but again Jake does not seem interested. Charlie then hands him a printout of Sarahs dating profile and tells Jake he has a date with her the next day at a   and offends Sarah when he begins to analyze her profile. Even worse, he reveals that the dog is not really his. When she accuses him of being deceptive he points out that the requirement was "Must love dogs," not "Must own a dog."  Sarah leaves abruptly but agrees to see him again.

Sarah and Jake go on a dinner date where he asks her why she is not with her husband anymore. She explains that he just stopped loving her and that he was never ready to have children. Sarah acknowledges that her ex-husband is now with a woman fifteen years younger than her with a baby on the way. The date progresses back to Sarahs house where they discover that neither has a condom. They hastily drive around but when they finally find protection neither of them is in the mood.

Jake and Charlie are discussing Sarah when he admits that she intrigued him. That night, Jake tries to call Sarah. Meanwhile Sarah has connected with Bob Connor. She checks to see if Bob is home but discovers he is with June, one of her co-workers. Sarah assumes they are on a date, and while fleeing the scene she drops her phone. Sarah arrives home to find her brother Michael, who is drunkenly dealing with his own marital problems, and Jake, who has been taking care of him. Jake takes Sarah rowing and they share a kiss. Afterward he takes Michael home while Sarah lights candles and sets the mood. But Bob shows up instead of Jake. He returns her phone, explains that he and June are not involved, and then kisses Sarah just as Jake gets back. Jake leaves, upset.
 Thanksgiving Sarah Doctor Zhivago. Sarah notices Jake leaving the theater afterward, but while discussing the film with him she notices Sherry and realizes the two of them are on a date. Sarah flees again, and when Sherry asks Jake up to her apartment, he declines and instead walks home.
 crew team to take her out to him. Eventually she dives in and swims over to his boat. After climbing in Sarah tells Jake how she feels about him and they kiss. Later, when telling the story of how they met, they mention in unison that they found each other at a dog park.

==Cast==
*  that Cusack added to the film. When Cusack tried to make her improv she thought, "I don’t feel that confidence. I start blushing profusely and I get all sweaty and, I don’t know. Old school."   
*John Cusack as Jake Anderson: Cusack was about to do another film in Europe but it fell through at the last minute.  He met with Goldberg and after reading the script they asked him to be in the film. Impressed with the actors that were already signed on, Cusack thought, "thats a pretty great pedigree, so I was kind of happy to be asked to join such a great group."  Cusack had always wanted to work with Lane and had been following her career for some time.   
*  was originally cast in the part. 
*Brad William Henke as Leo: Henke took the role because he "fell in love with the fact that it wasn’t a stereotypical character."  The crew did not mind what the character looked like and he was able to play a normal guy. 
*Stockard Channing as Dolly
*Christopher Plummer as Bill Nolan
*Colin Egglesfield as David
*Ali Hillis as Christine Nolan
*Dermot Mulroney as Bob Connor
*Victor Webster as Eric
*Julie Gonzalo as June
*Jordana Spiro as Sherry
*Glenn Howerton as Michael
*Krikor Satamian as Armenian Restaurant Owner

==Production==
Goldberg was first interested in starting this project when he found Cooks book.  He thought it had a lot of humor and started working on getting the film rights.     Goldberg worked closely with Cook, sharing all the draft copies with her and asking for input.  He even included her in the casting process.  Even though Cook only made it on set twice she was "so pleased with what they’ve done. Its really such a tribute to the book and just a great movie in its own right." 

Goldbergs "process is to just get an actor and then write and re-write and work on the set."  Goldberg was constantly bringing new pages to the set while Cusack contributed ideas for him to work with.  Cusack also performed in takes where Goldberg allowed him to say whatever came out of his mouth. 

===Mother Teresa===
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"The dog in Claires book wasnt a Newfoundland, but Im crazy for Newfies; they have such sweet natures and their eyes are so expressive."
&mdash;	Gary David Goldberg, director 
|} Newfoundland to play the part, even though it was a different breed in the novel.  Mother Teresa was played by two females, Molly and Mabel. Lane explained, "Like with kids, they prefer hiring twins."   The dogs were chosen when they were only puppies and were trained by Boone Narr for several months before filming.  During filming the dogs were still puppies, being 6 months old and 80 pounds.  When production ended Goldberg adopted both dogs.  Part of the training of the dogs was to follow the commands of the trainer who was situated off-camera while focusing attention on the actor. 

==Release==

===Critical reception===
The film received mostly negative reviews, receiving a 46/100 on Metacritic and falling in the "mixed or average reviews" category.  On Rotten Tomatoes it received a "rotten" rating with 35% based on 142 reviews and a general consensus that "Despite good work from its likable leads, the romantic comedy Must Love Dogs is too predictable." 

Roger Ebert thought that although Lane and Cusack are "two of the most likable actors in the movies", they "deserve characters that the movie takes more seriously and puts at more risk", giving the film two out of four stars.    Stephen Holden of The New York Times gave the film a negative review, wondering how the actors were "bamboozled into lending their talents to the project."  He added that the film has "contrived little incidents", "is so clueless", and is "hopelessly clichéd and out of date". 

A more positive review from Rolling Stone said that the film had a "great title and appealing performances" from Lane and Cusack. Giving the film three out of four stars, the review also said that "Just when you think you have Goldberg figured, he springs fresh surprises."   Ann Hornaday from The Washington Post noted the film "works because Lane is one of those actresses who can do just about anything and still earn the audiences undying love."  Hornaday mentioned that the movie "features an enormously appealing supporting cast" and commented on how well Lane and Cusack worked together on the film. 

===Box office===
Validating the critics views, the film opened in the number five spot with $12.8 million in 2,505 theaters with an $5,131 average per theater.   Must Love Dogs stayed in the theater for twelve weeks, staying in the top 10 for its first three weeks.   The film grossed $58,405,313, placing it at number 66 for all films released in 2005. 

===Home media===
Must Love Dogs was released on December 20, 2005 on DVD and VHS.   The video "is lush but soft, and some artificial sharpening only adds insult to injury, doing nothing to alleviate the overall lack of fine detail."  The audio is very standard and "wouldnt sound any different through a pair of headphones".   The extras are composed of four additional scenes lasting for about eight minutes with optional commentary from Goldberg.  There is also a gag reel titled "Pass the Beef" which lasts for about one minute. 

==Soundtrack==
{{Infobox album|
 | Name        = Must Love Dogs - Music from the Motion Picture
 | Type        = Soundtrack
 | Artist      = Various Artists
 | Cover       =
 | Released    =  
 | Length      = 42:44
 | Label       = Sony
 | Producer    =}}
{{Album ratings|title=Soundtrack
| rev1      = Allmusic
| rev1Score =    
}}
{| class=wikitable width="51%" 
|-
! width=1% |  Track # 
! width=20% | Title
! width=20% | Performer Length (M:SS)
|-
| 1
| "Brown Penny"
| Christopher Plummer
| 1:03
|-
| 2 When Will I Be Loved?"
| Linda Ronstadt
| 3:29
|-
| 3
| "The First Cut Is the Deepest"
| Sheryl Crow
| 3:47
|-
| 4
| "Hey There Lonely Girl"
| Eddie Holman
| 3:35
|-
| 5
| "Dont It Feel Good"
| Stephanie Bentley
| 3:29
|-
| 6
| "I Never"
| Rilo Kiley
| 4:31
|-
| 7
| "Id Rather Be in Love with You"
| Susan Haynes
| 2:48
|-
| 8
| "Dance All Night"
| Ryan Adams
| 3:14
|-
| 9
| "Shell"
| Susie Suh
| 4:29
|-
| 10
| "What Kind of Love"
| Rodney Crowell
| 3:59
|-
| 11
| "Prelude/Laras Theme|Laras Theme from Dr. Zhivago"
| Erich Kunzel, Cincinnati Pops Orchestra
| 5:47
|-
| 12 This Will Be (An Everlasting Love)"
| Natalie Cole
| 2:52
|-
| 13
| The Partridge Family|"Cmon Get Happy!"
| Diane Lane, Dermot Mulroney, Stockard Channing, Elizabeth Perkins, Ali Hillis
| 1:03
|}

==References==
 

==External links==
 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 