Midnight Ride (film)
{{Infobox Film
| name           = Midnight Ride
| image          = Midnight Ride (film).jpg
| caption        = Somewhere round the next bend, lies the road to terror
| director       = Bob Bralver
| producer       = Ovidio G. Assonitis
| writer         = Russell V. Manzatt/Bob Bralver (as Robert Bralver)  
| starring       = Mark Hamill  Robert Mitchum  Michael Dudikoff 
| music          = Carlo Maria Cordio
| cinematography = Roberto DEttorre Piazzoli
| editing        = Claudio M. Cutry
| distributor    = Cannon Pictures Inc./Cannon Film Distributors/Motion Picture Company of Australia Limited/Warner Home Video
| released       = July 20, 1990
| runtime        = 93 min
| country        = USA English
}}
 thriller (with slasher elements) shrink who impulsive (he tended to violently attack those who hurt him physically/verbally).

==Synopsis==
 mournful hitchhiker Justin Mckay, desperately searching for a ride. Her offer of a lift to him plunges her into a night of pure terror as Justin is seriously disturbed, twisted by a tortured childhood which ended in being made to see his little sisters shocking murder and mutilation at the hands of his brutal alcoholic mother (who took a butcher knife to her head and used it like a comb) who systematically slays anyone who harms or offends him on a murderous impulse (psychology)|impulse, as he captures their dying moments on his Polaroid camera. As Lawson struggles to follow Lara despite a leg in a cast, he is left for dead by our Justin, but recovers and now must search the steadily darkening roads for Lara, while her deeply troubled captor Justin continues his uncontrollable Spree killer|slaughter-spree, rampaging through the night leaving behind carnage and fiery devastation on his path of madness. As soon as Justin and Lara reach the hospital, Justin pretends Lara is paranoid and soon encounter Dr. Hardy, Justins doctor who tried to help Justin when he first met him. While Lawson arrives at the hospital, Justin forces Dr. Hardy to give Lara the treatment of electric shocks. As much as Dr. Hardy tries to persuade Justin not to he ignores him and gives Lara electric shocks, thus trying to kill her. Lawson comes right out of a ventalation shaft into the room stopping Justin from his insane doing and pursues him down to the engineers room tackling Justin down and throwing him right into a current, electrifying him to death. Lawson and Lara in the end head to the elevator, which Lawson forgives Lara and says that she is more important than his work. Not realizing that Justin survived the incident and secretly dresses up as a patient and is in the elevator with them and grabs a knife to kill them both, but Lawson grabs his gun and shots him directly to the head leading him to his real death, thus ending the film.

==Cast==
{| class="wikitable"
|-
! Actor / Actress
! Character
|-
| Michael Dudikoff
| Lawson
|-
| Mark Hamill
| Justin Mckay
|-
| Savina Gersak
| Lara
|-
| Robert Mitchum
| Dr. Hardy
|- Timothy Brown
| Jordan
|-
| R.A. Rondell
| Officer Baker
|-
| Mark A. Pierce
(as Mark Pierce)
| Policeman
|-
| Bob Bralver
| Policeman
|- Michael Crabtree
| Policeman
|-
| Louis Schwiebert
| Policeman
|-
| Lezlie Deane
| Joan
|-
| Steve Ingrassia	
| Man with Joan
|-
| Cynthia Szigeti 	
| Mrs. Egan
|- Dee Dee Rescher	
| Receptionist 
|-
| Pamela Ludwig
| Rental agent
|}

==See also==
*Mark Hamill
*Robert Mitchum
*Slasher film
*Thriller film
*Child abuse
*Spree killer

==External links==
 
* 

 
 
 