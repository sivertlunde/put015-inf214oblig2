Aalayamani
{{Infobox film
| name =     Aalayamani
| image = Aalayamani poster.jpg
| caption = Official poster
| director       = K. Shankar
| producer       = P. S. Veerappa
| writer         = Javar Seetharaman
| screenplay     = Javar Seetharaman
| story          = G. Balasubramaniam
| starring       = Sivaji Ganesan S. S. Rajendran B. Saroja Devi M. R. Radha C. R. Vijayakumari
| music          = Viswanathan–Ramamoorthy
| cinematography = Thambu
| editing        = K. Shankar K. Narayanan
| studio         = P. S. V. Pictures
| distributor    = P. S. V. Pictures
| released       =   
| runtime        = 154 mins
| country        =   India Tamil
}}
 
 1962 Tamil language drama film directed by K. Shankar. The film features Sivaji Ganesan, S. S. Rajendran, B. Saroja Devi, C. R. Vijayakumari and M. R. Radha in lead roles. 
The film, produced by P. S. Veerappa, had musical score by Viswanathan–Ramamoorthy and was released on 23 November 1962. 
The film was remade in Telugu as Gudi Gantalu (1964) and in Hindi as Aadmi (1968 film)|Aadmi (1968).

==Plot==
Aalayamani is a tale of possessiveness and distrust towards ones partner and the eventual redemption by sacrifice. The story is an essential interplay of feelings and the shadows cast upon by darker feelings of one person and the effect it produces on himself and in his immediate circle of friends and family.

The Hero of the story is Thyagarajan (Sivaji Ganesan), a lonely rich bachelor, subjected to a deep seated trauma in the form of witnessing the death of a childhood playmate which is caused by his possessiveness. This is recurring theme in Thyagarajans life leading to disastrous results.

The movie starts off with an imposing bell being rung, a voice narrates to us the story of a poor man, Arumugam Pillai, who was about to commit suicide on the death rock but was stopped by the sound of the divine bell from the temple. He decides to take another chance with his life and soon he becomes a rich man and his son is none other than Thyagu, our hero!

Thyagarajan meets and becomes close friends with a virtuous man Sekhar (S. S. Rajendran) and there is a well grown sense of mutual admiration and fondness amongst the friends. Sekhar and Meena(B. Saroja Devi) are in love and sekar promises he will ask her hand in marriage. Unbeknownst to both of them, Thyagu comes across Meena, whose father (Chittor V. Nagaiah|Nagaiah) works in Thyagus estate and impressed by her vivacity Thyagu falls head over heels for her. This has all the makings of a typical love triangle.

Meena has a sister Pushpalatha who falls in love with the villains (M. R. Radha) son who demands big amount in dowry. Nagaiah is unable to rustle up the requisite cash and is now in doldrums concerned about the fate of his elder daughters Marriage. Thyagu comes helping with the cash and they are wedded. Shortly thereafter Thyagu proposes to marry Meena and sends Sekhar as his messenger.

Except Sekhar and Meena, to their obvious shock and despair, it looks to the rest of the relatives Thyagu and Meena as a pair made for each other. Sekhar realizing the extent of love and the depth of feelings of Thyagu asks Meena to forgo their feelings and that she should go ahead and marry Thyagu. The family members of both the families get together and agree to the arrangement. By this time, Thyagu is injured in a serious accident loses the use of both his legs and ends up being a cripple for life. The loyal partner Meena pitches in the effort to care for Thyagu and spends all her time in his care.

Sekhar couldnt forget Meena and tries to be close with Meena, while she honors her commitment to Thyagu and keeps Sekhar at a distance. Thinking Sekhar as a single, Prema (C. R. Vijayakumari), daughter of the villain expresses her feelings for Sekhar and he fails to respond in kind.

All these pent up emotions of Love, Friendship, Loyalty, Jealousy, physical debilitation boils over at one point and things take a turn for the worse for all three. Goaded by the feelings of inadequacy, Thyagu builds in his mind a picture, that his dear friend Sekhar is out to get his love,  decides to take matters in his hands and get rid of Sekhar. The lingering doubts about his friend and lover is now fueled by his restive passions which is burning brightly.

Thyagu plans that the appropriate location for revenge is the Death rock known for its enchanting scenery and dangerous cliffs, where once his father attempted suicide. He was to drive Sekhar to the edge and push him to death from the face of the cliff. Thyagu leads Sekhar to the cliff and reveals his anger and frustration about their perceived relationship, questioning the sincerity of his friend and his lover. A visibly angered Sekhar repulses by revealing the magnitude of his sacrifice he made for his close friend and to prove his innocence Sekhar swears to take his own life from the same Death rock. Truth dawns on Thyagu, now deeply humiliated jumps from the cliff to the tumultuous  sea below.

While Sekhar and Meena mourning for their loss, Thyagu gets rescued by a fisherman and the shock of falling from the cliff has given back Thyagu the partial use of his legs. Meena now deems her as a widow and convinces Sekhar to marry the girl who loves him. On the wedding day, Thyagu now dressed as a nobody shows up for the wedding and happens to overhear a conversation of Meena declaring that there is no life and happiness left due to Thyagus death. This convinces Thyagu to reveal his identity and console Saroja, while the villain with an eye on Thyagus money thinks otherwise. Thyagu is hit on the head and falls unconscious, the marriage completes and Meena decides to take her life from the same cliff Thyagu plunged to death and seen running towards the cliff. Conscious now, Thyagu goes after Meena to stop her from committing suicide, calling out loud to stop and limping all the way behind her.

Meena sees Thyagu is alive and changes her mind and the lovers unite. In the background the song Ponnai virumbum boomiyile (meaning: In a world of wealth lovers, the dear one who loved me) being played, a happy Thyagu and Meena waves us a farewell, the movie ends.

==Cast==
*Sivaji Ganesan
*S. S. Rajendran
*B. Saroja Devi
*M. R. Radha
*C. R. Vijayakumari
*M. V. Rajamma
*Pushpalatha
*P. S. Veerappa
*T. R. Ramachandran
*Chittor V. Nagaiah
*K. Natarajan

==Crew==
*Producer: P. S. Veerappa
*Production Company: P. S. V. Pictures
*Director: K. Shankar
*Music: Viswanathan–Ramamoorthy
*Lyrics: Kannadasan
*Story: G. Balasubramaniam
*Screenplay: Javar Seetharaman
*Dialogues: Javar Seetharaman
*Art Direction: A. Balu
*Editing: K. Shankar & K. Narayanan
*Choreography: S. M. Rajkumar
*Cinematography: Thambu
*Stunt: None
*Audiography: T. S. Rangasamy
*Dance: None

==Soundtrack==

The music composed by Viswanathan–Ramamoorthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 05:00
|-
| 2 || Kannana Kannanukku || Sirkazhi Govindarajan, P. Susheela || 05:10
|-
| 3 || Karunai Magan || M. S. Viswanathan || 00:57
|-
| 4 || Maanattam || P. Susheela || 03:36
|-
| 5 || Ponnai Virumbum || T. M. Soundararajan || 04:03
|-
| 6 || Satti Sutthadhada || T. M. Soundararajan || 04:21
|-
| 7 || Thookkam Un Kangalai || S. Janaki || 03:22
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 