Catchfire
 
{{Infobox film
| name = Catchfire
| image = File:Film_Poster_for_Catchfire.jpg
| caption = Film Poster for Catchfire: Directors Cut
| director = Dennis Hopper (as Alan Smithee)
| producer = Dick Clark
| writer = Rachel Kronstadt Mann Ann Louise Bardach Alex Cox Tod Davies
| starring = Jodie Foster Dennis Hopper Fred Ward
| music = Michel Colombier
| cinematography = Edward Lachman
| editing = David Rawlins
| distributor = Vestron Pictures
| released =  
| runtime = Unreleased cut 180 min. Directors Cut (cable) 116 min. Theatrical release 98 min.
| country = United States
| language = English
| budget = $10 million
| gross = $5,008,276
}}
 action Thriller thriller film starring Jodie Foster, Dennis Hopper, and Fred Ward. Several other notable actors have Cameo appearance|cameos. The film was disowned by its director, Hopper, before release and he is therefore credited under the fictional pseudonym Alan Smithee. The original screenplay was written by Rachel Kronstadt Mann, then re-written by Ann Louise Bardach, who was hired by Dennis Hopper and producer Steven Reuther. During the 1988 Writers Guild of America strike, Hopper retained Alex Cox to do another polish while the film was shooting.

Hopper released a directors cut of the film in the United States on cable television titled Backtrack, which runs 18 minutes longer than the theatrical version.

==Plot==
Conceptual artist Anne Benton (Jodie Foster) creates electronic pieces that flash evocative statements, and her work has begun to attract major media attention.

Driving home one night, Anne suffers a blowout on a deserted road and, while looking for help, witnesses a mafia hit supervised by Leo Carelli (Joe Pesci). Leo spots Anne, but she escapes and goes to the police.

They offer her a place in the federal witness protection program, but mob boss Lino Avoca (Vincent Price), Carellis boss, sends top-of-the-line hitman Milo (Dennis Hopper) and his partner Pinella (John Turturro) to silence her. Pinella kills her boyfriend Bob (Charlie Sheen), but she escapes.

Months pass; Anne has severed all ties with her past and re-established herself in Seattle as an advertising copywriter. Milo, who never gives up, recognizes the text of a lipstick ad as one of Annes catchphrases, and tracks her down.

She flees again, to New Mexico, and he finds her again. But this time he offers her a deal: hell let her live, if shell do anything and everything he asks.

Milos interest in Anne, it turns out, is more than professional, but not exactly what she thinks. He doesnt want her to be his sex slave, though sex is part of the equation.

A man obsessed, Milo has fallen in love with Anne. And he has no idea how to cope with the unfamiliar emotion. Astonishingly, after a rocky start, Anne realizes that she has also fallen for him.

By failing to kill Anne as he was hired to do, Milo has marked himself for death, and the two flee together to an isolated farm that Milo owns.

Avocas men track them there, and they realize that in order to be free, they must return and confront their pursuers. The plan that they concoct works, leaving Avoca, Carelli, and their men dead.

Anne and Milo escape together to a new life.

==Cast==
*Jodie Foster as Anne Benton
*Dennis Hopper as Milo
*Dean Stockwell as John Luponi
*Vincent Price as Mr. Avoca
*John Turturro as Pinella
*Fred Ward as Pauling
*Julie Adams as Martha

===Cameos===
*Catherine Keener as Truckers girl
*Charlie Sheen as Bob
*Burke Byrnes as Fed #1 
*Bob Dylan (uncredited) as Artist
*Joe Pesci (uncredited) as Leo Carelli
*Alex Cox (uncredited) as D.H. Lawrence
*Toni Basil (uncredited)

==Alternate versions==
There also exists a 180 minutes long original cut which remains unreleased.

==External links==
*  

 
 

 
 
 
 
 
 
 
 