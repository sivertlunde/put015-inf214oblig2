Zameer (1975 film)
 

{{Infobox film
| name           = Zameer
| image          = Zameer 1975.jpg
| image_size     = 
| caption        = 
| director       = Ravi Chopra
| producer       = B. R. Chopra
| writer         = 
|story= C. J. Pavri
|screenplay= B. R. Films story Department Akhtar ul Iman  (dialogue) 
| narrator       = 
| starring       = Amitabh Bachchan Saira Banu Shammi Kapoor
| music          = Sapan Chakraborty
| cinematography = Dharam Chopra
| editing        = Pran Mehra
| distributor    = B. R. Films
| released       = 1975
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}

Zameer(Hindi: ज़मीर) is a 1975 Hindi action-drama film directed by Ravi Chopra, and produced by B. R. Chopra for B. R. Films. It stars Amitabh Bachchan, Saira Banu, Shammi Kapoor, Madan Puri and Vinod Khanna. The music for this film was composed by Sapan Chakraborty. It was a remake of 1960 film Bombai Ka Baboo starring Dev Anand. It was a hit in the Indian box office, in an years of big hits Deewaar (1975 film)|Deewaar, Sholay, Dharmatma and Julie (1975 film)|Julie.  
  

==Plot==

Maharaj Singh is a proud owner of several derby-winning stallions, and lives in a palatial farmhouse with his wife, Rukmini and young son, Chimpoo. One day dacoits attack his farmhouse with a view of stealing the stallions, but Maharaj fights them, killing the son of the leader of the dacoits, Maan Singh. Maan Singh swears to avenge the death of his son, and abducts Chimpoo. Years later, a servant of Maharaj, Ram Singh, brings a young man named Badal into the Singhs lives, and tells them that he is their missing son Chimpoo. Both Maharaj, Rukmini, and their daughter, Sunita, are delighted at having Chimpoo back in their lives. Then Badal and Sunita fall in love with each other. It is then Badal confesses to Maharaj that he is not Chimpoo, but a former convicted jailbird, who was asked to impersonate him by an embittered Ram Singh. Maharaj does not want to relay this information to an ailing Rukmini, and decides to keep it quiet for the rest of their lives. But sooner or later Rukmini is bound to find out - especially when Badal and Sunita openly show their love - will this shock of an intimate brother and a sister spare her or has fate something else in store for her?

==Cast==
*Amitabh Bachchan ...Badal
*Saira Banu ...Sunita Singh
*Shammi Kapoor ...Thakur Maharaj Singh
*Madan Puri ...Daaku Maan Singh
*Vinod Khanna ...Special Appearance (Suraj)
*Indrani Mukherjee...Rukmini

==Soundtrack== Autumn Leaves.
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Phoolon Ke Dere Hai"
| Kishore Kumar
|-
| 2
| "Tum Bhi Chalo Hum Bhi Chale"
| Kishore Kumar, Asha Bhosle
|-
| 3
| "Aanka Baanka Tali Talaka"
| Kishore Kumar, Manna Dey
|-
| 4
| "Tum Bhi Chalo"
| Kishore Kumar
|-
| 5
| "Zindagi Hansne Gane Ke Liye Hai"
| Kishore Kumar
|-
| 6
| "Ab Yahan Koi Nahin"
| Kishore Kumar
|}

==References==
 

==External links==
*  

 
 
 
 

 