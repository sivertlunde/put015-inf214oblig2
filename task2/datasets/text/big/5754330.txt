Simon Says (film)
{{Infobox film
| name           = Simon Says
| image          = Simon Says Movie Poster.jpg
| director       = William Dear Bill Dear
| starring       = Crispin Glover Margo Harshman Carrie Finklea Artie Baxter Greg Cipes
| producer       = Ernie Lively
| music          =  
| cinematography = Bryan Greenberg
| editing        = Chris Conlee
| studio         = Blue Cactus Pictures Dark Moon Pictures Simon Says Productions
| distributor    =  
| released       =  
| runtime        =
| language       = English
| budget         =
}}
 Angels in the Outfield, Harry and the Hendersons) and stars Crispin Glover and Margo Harshman. It was premiered at Fantastic Fest on 24 September 2006 and on DVD in the U.S. 26 June 2009.

==Plot==

Five teenagers — Kate (Margo Harshman), Zack (Greg Cipes), Vicky (Carrie Finklea), Riff (Artie Baxter), and Ashley (Kelly Vitz) — are on their way to pan for gold during Spring Break when they make a wrong turn and wind up at a grave site. They encounter two strange older men, who warn them to return home, saying there have been murders in the area. The teens insist, asking for directions, and are directed towards a large store and told that it is town. Making their way into town, four of the teens head inside the store while Zack puts gas in their van. Inside, the four teens meet the brothers Simon (Crispin Glover), who flees from them, and Stanley.

Stanley is unhelpful, denying the teens requests to buy anything, before intimately caressing the Kates hand. Zack abruptly walks in, asking if he can access the cooler, only to be told there is no beer inside. Spotting a family photo on the wall, Zach begins to talk about Billy the Kid, causing Stanley react strangely. Frustrated, Riff tosses some money on the counter and the group leave, Zach and Vicky urging Ashley on, referring to her as doll.

Finally finding a campsite, the teens split-up: Ashley going for a jog, Vicky following Riff out to chop wood for the campfire, and Zach driving back to the store for propane, leaving Kate alone. Vicky seduces Riff, but they are interrupted by Ashley, causing them to head back to the campsite. Kate complains about being left alone, causing Vicky to instruct Riff to stay with her while she goes to find Ashley. Unknown to the pair, they are being observed as Riff begins kissing Kate, but they too are interrupted by Ashley running past.

The observer, dressed in a ghillie suit, follows Ashley as she continues her run, attacking and chasing her through the woods when he realises that she is not dream girl. Having murdered Ashley, her attacker - revealed to be Simon - begins to cut up her body, calling her doll and saying that he will make her into a present for dream girl. His work is interrupted by the arrival of a small dog which steals Ashleys severed hand. Giving chase, Simon encounters a group playing paintball, who attempt to defend themselves, but are murdered as well.

Back at the store, Zach arrives to find it closed and empty. He lets himself in, searching the cooler for beer, but also discovers numerous bodies hanging from the ceiling. Shocked and thinking no one will believe him, he pulls several newspaper articles off the bodies and flees. Meanwhile, as Simon is hanging the small dogs owner near the road, he notices the van approaching. He swings the woman into the path of the oncoming vehicle, killing her. Uncertain of what has happened and swinging body now out of sight, Zach sees the dogs body on the road and assumes that is what he hit. 

Now well after dark, Riff, Kate, and Vicky are surprised by Stanley pulling up in his truck, telling them that Simon is missing. When they say that Ashley is also missing, he suggests that the teen might be with his brother, and urges them to come with him to look for them. At this moment, a terrified Zach arrives and manages to convince Stanley to leave. He takes the others into a tent and shows them the newspaper articles, immediately afterward they heard a noise and come out to find the vans tyres have been punctured, stranding them.

The group gather their torches and phones and head into the forest to look for Ashley, eventually hearing music playing from somewhere in the woods. At the source of the music, they discover that Simon has made a doll out of Ashleys head, hands, and feet. As they run into the forest, they are watched by Simon, who kills Riff with a contraption that flings pickaxes. Vicky stumbles upon the paintballers campsite, only to be discovered by Stanley, who promises that he wont allow Simon to hurt her and promptly hangs her next to the owner of the dog.

Kate is stopped on the road by Stanley in his truck, who tells her that the others are safe at his store and that he will give her a ride. She eventually decides to trust him as, unknown to both, Zach sneaks up and climbs into the truck bed. Instead of the store, Stanley takes Kate to an old campsite where two long-decayed bodies are sitting at the picnic table. He ties her to the table and offers her food that has long since gone bad, when Simon appears in his ghillie suit and demands to also be fed. Suspicious, Stanley asks Simon to sit at the table and put his hand out, chopping off several fingers with a cleaver and revealing that it is actually Zach in the suit.

As Stanley tortures Zach, the youth confesses his love to Kate. The lower half of the ghillie suit catches fire and Zach is immolated. Turning back to Kate, Stanley announces that he is also Simon, having taken over his brothers identity after murdering their parents while the real Simon lay in a coma. Echoing Stanleys sentiment that Simon was the favourite, Kate seduces Stanley, distracting him while she grabs the cleaver. However, he deflects her attack, but accidentally frees her in the ensuing struggle, allowing her to disappear into the woods.

Returning to Zachs charred body, Kate promises to kill Stanley. Moments later, Stanley also approaches the body, vowing to kill her. Kate bursts from Zachs corpse where she was hiding, bringing the cleaver down on Stanleys head. He falls to the ground, but when she looks down at where his corpse should be, he is gone. A hand is seen reaching for her just before the film cuts to black.

The film ends with another group of youths arriving at the store looking for supplies and directions somewhere nearby they can camp. Stanley/Simon raps his knuckles against his skull, revealing a metal plate. The teens leave and Stanley/Simon goes inside, opening a trapdoor where Kate his being held captive with infant twins.

==Cast==
*Margo Harshman as Kate
*Crispin Glover as Simon/Stanley
*Greg Cipes as Zack
*Carrie Finklea as Vicky
*Kelly Vitz as Ashley
*Artie Baxter as Riff
*Blake Lively as Jenny
*Erica Hubbard as Sommer
*Lori Lively as Lani
*Daniella Monet as Sarah
*Kelly Blatz as Will
*Robyn Lively as Leanne
*Ernie Lively as Pig
*Bart Johnson as Garth
*Chad Cunningham as Young Stanley
*Chris Cunningham as Young Simon Brad Johnson as Quinn

==References==
 

==External links==
*  
*  
*  
*   on Variety.com

 

 
 
 
 
 
 