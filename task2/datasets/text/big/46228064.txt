Ruby Blue (film)
{{Infobox film
| name           = Ruby Blue
| image          = 
| caption        = 
| director       = Jan Dunn
| producer       = Elaine Wickham Jeremy Burdek Mark Foligno Nadia Khamlichi Justin Lanchbury James ODonnell Adrian Politowski Karl Richards Charles Finch Sally Greene Alison Rayson Frances Patterson
| writer         = Jan Dunn
| based on       = 
| starring       = Bob Hoskins Josiane Balasko
| music          = Janette Mason
| cinematography = Ole Bratt Birkeland
| editing        = Emma Collins
| distributor    = Guerilla Films
| studio         = Medb Films E-MOTION Target Entertainment
| released       =  
| runtime        = 106 minutes
| country        = United Kingdom
| language       = English
| budget         = $350,000 
| gross          = 
}}
Ruby Blue is a 2008 British drama directed by Jan Dunn. 

==Plot==
Jack sinks into depression following the death of his wife. The arrival of a mysterious French in the neighborhood will change his life. Jack will then gradually discover the truth about this égnimatique woman.

==Cast==
* Bob Hoskins as Jack
* Josiane Balasko as Stephanie
* Jody Latham as Ian
* Josef Altin as Frankie
* Jessica Stewart as Florrie
* Shannon Tomkinson as Stacey
* Sean Wilton as Dick
* Angelica OReilly as Rosie
* Ashley McGuire as Debbie
* Chloe Sirene as Cecile
* Michael Mills as Joey
* Sam Talbot as Sean
* Nicola Stewart as Seans wife
* Corinna Powlesland as Suzy
* Joel Clark Ackerman as Tony
* Lisa Payne as The policewoman
* James ODonnell as The policeman
* Rebecca Clow as DCI Cartwright

==Production==
The movie was presented in several Film Festivals like the Dinard Festival of British Cinema (France), the Warsaw International FilmFest (Poland), the Cinequest International Film Festival (USA), the London Independent Film Festival (UK) and the Frameline Film Festival (USA).

==Accolades==
{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Recipient !! Result
|- 2008
| Chicago Gay and Lesbian International Film Festival
| Best Feature Length Narrative
| Jan Dunn
|  
|-
| London Independent Film Festival
| Best British Film
| Jan Dunn
|  
|-
| Moondance International Film Festival
| Best Director
| Jan Dunn
|  
|-
| Oxford International Film Festival
| Best Actor
| Bob Hoskins
|  
|-
| DC Independent Film Festival
| Best Feature
| Jan Dunn
|  
|-
|}

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 