Maduve Madi Nodu
{{Infobox film 
| name           = Maduve Madi Nodu
| image          =  
| caption        = 
| director       = Hunsur Krishnamurthy
| producer       = Nagi Reddy Chakrapani
| writer         = 
| screenplay     = Chakrapani Rajkumar R. Narasimharaju
| music          = Ghantasala
| cinematography = Madhava Bulubule
| editing        = G Kalyana Sundaram D G Jayaram
| studio         = Vijaya Productions Ltd
| distributor    = 
| released       =  
| country        = India Kannada
}}
 1965 Cinema Indian Kannada Kannada film, Narasimharaju in lead roles. The film had musical score by Ghantasala.  

==Cast==
  Rajkumar
*R. Nagendra Rao
*Udaykumar Narasimharaju
*K. S. Ashwath
*Ramachandra Shastry
*Rathnakar
*Hanumanthachar
*Dwarakish
*Chellapilla Satyam
*Ganapathi Bhat
*Master Basavaraj Leelavathi
*Vandana
*B. Jayashree
*Ramadevi
*Rama
 

==Soundtrack==
The music was composed by Ghantasala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maduve Maadikondu || Ghantasala || Hunsur Krishna Murthy || 05.27
|- Susheela || Hunsur Krishna Murthy || 03.01
|}

==References==
 

==External links==
*  
*  

 

 
 
 


 