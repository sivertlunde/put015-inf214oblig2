The Magic of Herself the Elf
{{Infobox Film
| name           =  The Special Magic of herself the Elf
| image          =  
| image_size     = 
| caption        =   
| director       =   Raymond Jafelice Michael Hirsh Patrick Loubert Clive A. Smith
| writer         =   Diane Dixon
| narrator       =   
| starring       =   Denny Dillon Georgia Engel
| music          =   Judy Collins Patricia Cullen
| cinematography =   
| editing        =   Cathy Hunt Tom Joerin Rob Kirkpatrick Don Lauder
| studio         =   Nelvana  
| distributor    =   
| released       =   July 30, 1983
| runtime        =   22 minutes
| country        =     Canada    English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}	 1983 animated television special produced by the Canadian animation company, Nelvana Limited. Directed by John Celestri (credited under first name Gian) and Raymond Jafelice, it stars the voices of Jerry Orbach, Georgia Engel and Priscilla Lopez. The music was sung and performed, though not written, by Judy Collins.

The special is based on the American Greetings/Mattel property, Herself the Elf, and was followed by a 1990s spinoff series of the same name starring Terri Hawkes. It was released once on video by Scholastic Press|Scholastic/Lorimar (later Karl-Lorimar).

==Cast==
*Denny Dillon  as Meadow Morn (voice)
*Georgia Engel  as Willow Song (voice)
*Ellen Greene  as Creeping Ivy (voice)
*Terri Hawkes  as Snow Drop (voice)
*Jim Henshaw  as Wilfie - a Wood Sprite (voice)
*Priscilla Lopez  as Herself the Elf (voice)
*Jerry Orbach  as King Thorn (voice)
*Susan Roman  as Wood Pink (voice)

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 


 
 