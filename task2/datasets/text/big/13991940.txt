Eisenstein (film)
{{Infobox Film
| name           =  Eisenstein
| image          =  
| image_size     = 
| caption        =  
| director       =  Renny Bartlett
| producer       =  Martin Paul-Hus Regine Schmid
| writer         =  Renny Bartlett
| narrator       =   
| starring       =  Simon McBurney Raymond Coulthard Jacqueline McKenzie
| music          =  Alexander Balanescu
| cinematography =  Aleksei Rodionov
| editing        =  Wiebke von Carolsfeld
| studio         =   
| distributor    =   
| released       =   
| runtime        =  99 minutes
| country        =  Germany Canada
| language       =  English
| budget         =   
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Canadian film nominated for Best Picture, Best Director, Best Screenplay. 
 New York Times reviewer Stephen Holden wrote that the film was "a series of loosely connected (and unevenly acted) theatrical sketches whose central theme is the directors shifting relationship with the Soviet government", and that "as dazzingly played" by McBurney, the title character "bears resemblances to everyone from the punk rocker Johnny Rotten to the contemporary director Peter Sellars."  The Independent called it "engagingly irreverent take on an important, tragic story".   John Petrakis of the Chicago Tribune commented that director Bartlett seemed "less interested in Eisenstein the filmmaker than he is in Eisenstein the political animal, gay man, Jewish target and artistic rebel", and described the overall result as "a clever pseudo-bio that manages to have a good time as it doles out pieces of the famous directors life".  Ed Gonzales of Slant Magazine gave the film a less favorable review, calling it "efficient and concise but . . . lifeless". 

==References==
 

==External links==
* 
*  , May 13, 2002

 

 