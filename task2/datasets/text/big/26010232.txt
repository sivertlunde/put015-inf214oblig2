Tora-san's Forbidden Love
{{Infobox film
| name = Tora-sans Forbidden Love
| image = Tora-sans Forbidden Love.jpg
| caption = Theatrical poster 
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Reiko Ōhara
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 107 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  is a 1984 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Reiko Ōhara as his love interest or "Madonna".  Tora-sans Forbidden Love is the thirty-fourth entry in the popular, long-running Otoko wa Tsurai yo series.

==Plot==
In the midst of Japans rising economy of the mid-1980s, the itinerant Tora-san becomes drunk with a hard-working company section chief. After an hour commute, the two sleep off their nights revelry at the section chiefs home in Ibaraki Prefecture. When the section chief disappears due to the pressure of his job, Tora-san helps his wife to find the man, while secretly hoping they do not, as he has fallen in love with her.      

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Reiko Ōhara as Fujiko Tominaga
* Masakane Yonekura as Kenkichi Tominaga
* Jun Miho as Akemi
* Shimojo Masami as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hidetaka Yoshioka as Mitsuo Suwa
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō

==Critical appraisal== Japan Academy kaiju eiga, or monster films, with footage from Shochikus entry in this genre, The X from Outer Space, employed. This was meant to reference Godzillas return from retirement in The Return of Godzilla (1984), which had been released just before the Tora-san film.  The German-language site molodezhnaja gives Tora-sans Forbidden Love three and a half out of five stars.   

==Availability==
Tora-sans Forbidden Love was released theatrically on December 28, 1984.  In Japan, the film has been released on videotape in 1987 and 1996, and in DVD format in 2002 and 2008. 

==References==
 

==Bibliography==
===English===
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 
 