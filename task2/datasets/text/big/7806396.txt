Kardia
 
 
{{Infobox film
| name           =  Kardia
| image          = Kardia FilmPoster.jpeg
| caption        = 
| director       = Su Rynard
| producer       = Paul Barkin
| writer         = Su Rynard
| starring       = Mimi Kuzyk Peter Stebbings
| music          = Philip Strong
| cinematography = Kim Derko
| editing        = Michele Francis 
| studio         = Alcina Pictures
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} operation she underwent as a child has mysteriously linked her life with another. To unlock the secret of her past, Hope revisits the curious tale of her childhood and explores the landscape of love, loss, and the human heart.

The film was produced by Paul Barkin and Larissa Giroux of Alcina Pictures in Toronto, Canada.

==Cast==
*Mimi Kuzyk  as Hope
*Peter Stebbings  as Dad
*Kristin Booth  as Sally
*Ariel Waller  as Young Hope
*Stephen Lobo  as Sanjay
*Donna Goodhand  as Auntie Florrie
*Emma Campbell  as Nurse
*Steve Cumyn  as Surgeon
*Nancy McAlear  as Scrub Nurse
*Dylan Trowbridge  as Reporter
*Nancy E.L. Ward  as OR Surgery Team Member

==Awards==
*The film won the 2005 Alfred P. Sloan Foundation Feature Film Prize in Science and Technology at the Hamptons International Film Festival

==External links==
 
*  
*  

 
 
 
 
 
 
 