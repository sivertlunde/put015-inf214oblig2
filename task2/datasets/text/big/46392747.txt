Madonna of the Sleeping Cars (1955 film)
{{Infobox film
| name = Madonna of the Sleeping Cars 
| image =
| image_size =
| caption =
| director = Henri Diamant-Berger
| producer =  Henri Diamant-Berger
| writer =  Maurice Dekobra  (novel)   Henri Diamant-Berger
| narrator =
| starring = Giselle Pascal   Jean Gaven   Erich von Stroheim 
| music = Louiguy   
| cinematography = Léonce-Henri Burel   
| editing =    Hélène Basté  
| studio = Le Film dArt 
| distributor = La Société des Films Sirius
| released = 21 June 1955
| runtime = 96 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} novel of the same title by Maurice Dekobra. 

==Cast==
*  Giselle Pascal as Lady Diana Wyndham  
* Jean Gaven as Don Armando Félix  
* Erich von Stroheim as Dr. Siegfried Traurig  
* Philippe Mareuil as Gérard Dextrier  
* Denise Vernac as Anna  
* Berthe Cardona
* Fernand Rauzéna as Le Queledec  
* Jacqueline Dane as Clara  
* Joé Davray as Lingénieur  
* Jackie Blanchot 
* Jacques Jouanneau as Henri - le chauffeur  
* Robert Burnier as Commodore Felton  
* Katherine Kath as Irena 
* Lucien Callamand as Lactionnaire   Paul Demange as Le voyageur 
* Raymond Ménage 
* Betty Phillippsen 
* René Worms as Arthur  

== References ==
 

== Bibliography ==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

== External links ==
*  

 
 
 
 
 
 

 