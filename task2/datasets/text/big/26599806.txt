The Book and the Sword (1960 film)
 
{{Infobox film name = The Book and the Sword image = caption = traditional = 書劍恩仇錄 simplified = 书剑恩仇录}} director = Lee Sun-fung producer = story = Louis Cha screenplay = Lee Sun-fung starring =  music = cinematography = editing = studio = Emei Film Company distributor =  released =   (Part 1)   (Part 2) runtime = country =  language = Cantonese budget =  gross =
}} Louis Chas novel The Book and the Sword. Directed and written by Lee Sun-fung, the film was divided into two parts, respectively released on May 4 and June 9 in 1960 in Hong Kong.

==Cast==
: Note: Some of the characters names are in Cantonese romanisation.

* Cheung Ying as Chan Ka-lok / Qianlong Emperor|Kin-lung Emperor
* Yung Siu-yi as Princess Fragrance
* Tsi Law-lin as Fok-ching-tung
* Chan Kam-tong as Man Tai-loi
* Leung So-kam as Lok Bing
* Shek Yin-tsi as Yu Yu-tung
* Ma Kam-ling as Lei Yuen-chi
* Lam Kau as Tsui Tin-wang
* Sheung-koon Kwan-wai as Chow Yee
* Shih Kien as Cheung Chiu-chung
* Yeung Yip-wang as Luk Fei-ching
* Ho Siu-hung as Muk-cheuk-lun
* Wong Chor-san as Chow Chung-ying
* Lee Yuet-ching as Mrs Chow
* Chan Tsui-ping as Yuk Yu-yee
* Siu Hon-sang as Pak Chun
* Cheung Sing-fai
* Liu Jialiang
* Yuen Siu-tien
* Chow Chung
* Wah Wan-fung

==External links==
* 
*  at the Hong Kong Movie Database
*  at the Hong Kong Movie Database

 

 
 
 
 
 
 
 
 