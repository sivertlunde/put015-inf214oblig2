The Brothers Karamazov (1958 film)
 
 
{{Infobox film
| name = The Brothers Karamazov
| image = Brothers Karamazov.jpeg
| image size = 
| caption = Theatrical release poster
| director = Richard Brooks
| producer = Pandro S. Berman
| screenplay = Julius J. Epstein  Philip G. Epstein   Richard Brooks
| based on =  
| starring = Yul Brynner  Maria Schell Lee J. Cobb Albert Salmi Richard Basehart
| music = Bronislau Kaper
| cinematography = John Alton
| editing = John D. Dunning
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 145 minutes
| country = United States
| language = English
| budget =$2,727,000  gross = $5,440,000 
}}

The Brothers Karamazov is a 1958 film made by Metro-Goldwyn-Mayer|MGM, based on Fyodor Dostoevskys novel The Brothers Karamazov. It was directed by Richard Brooks and produced by Pandro S. Berman. The screenplay was by Julius J. Epstein, Philip G. Epstein and Richard Brooks. It was entered into the 1958 Cannes Film Festival.    The brothers are played by Yul Brynner, Richard Basehart and William Shatner in his film debut. 

==Plot==
The story follows Fyodor Karamazov|Fyodor, the patriarch of the Karamazov family and his sons. When he tries to decide an heir, the tensions between the brothers of the film run high, leading to infighting and murder. 

==Cast== Dmitri Karamazov Grushenka
* Katya
* Lee J. Cobb as Fyodor Karamazov
* Albert Salmi as Smerdyakov Alexey Karamazov Ivan Karamazov

==Production==
Carroll Baker was meant to play the female lead but Warner Bros put her on suspension after she refused to play Diana Barrymore and would not loan her out. Maria Schell stepped in instead. WARNERS TO HOLD ACTRESS TO PACT: Studio Halts Deal Between Carroll Baker and M-G-M for Karamazov Movie Maria Schell Sought
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   03 May 1957: 20.  
==Reception==
According to MGM records the film made $2,390,000 in the US and Canada and $3,050,000 elsewhere resulting in a profit of $441,000.  . 

==Awards and nominations== Best Supporting Actor (nomination) - Lee J. Cobb

==References==
 

==External links==
 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 