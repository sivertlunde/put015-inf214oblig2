Within the Law (1923 film)
{{infobox film
| name           = Within the Law
| image          = 
| caption        =  
| director       = Frank Lloyd
| producer       = Joseph Schenck
| based on       =  
| writer         = Frances Marion (scenario)
| starring       = Norma Talmadge
| music          =
| cinematography = Tony Gaudio Norbert Brodine
| editing        = Associated First National
| released       =  
| runtime        = 8 reel#Motion picture terminology|reels; 8,034 feet
| country        = United States
| language       = Silent film (English intertitles)
}} silent film Bayard Veillers play of the same name about a young woman who is sent to prison and comes out seeking revenge.  

==Cast==
*Norma Talmadge – Mary Turner
*Lew Cody – Joe Garson
*Jack Mulhall – Richard Gilder, his son
*Eileen Percy – Aggie Lynch
*Joseph Kilgour – Edward Gilder
*Arthur Stuart Hull – George Demarest (billed as Arthur F. Hull)
*Helen Ferguson – Helen Morris
*Lincoln Plumer – Sergeant Cassidy(billed as Lincoln Plummer)
*Tom Ricketts – General Hastings (billed as Thomas Ricketts)
*Ward Crane – English Eddie
*Catherine Murphy – Gilders Secretary
*DeWitt Jennings – Inspector Burke (DeWitt C. Jennings)
*Lionel Belmore – Irwin, his attorney
*Eddie Boland – Darcy

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 