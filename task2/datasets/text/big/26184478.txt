Tonny
{{Infobox film
| name           = Tonny
| image          = 
| image size     = 
| caption        = 
| director       = Nils R. Müller Per Gjersøe
| producer       = Sverre Gran
| writer         = Jens Bjørneboe Sverre Gran
| narrator       = 
| starring       = Per Christensen
| music          = 
| cinematography = Hans Nord
| editing        = Olaf Engebretsen
| distributor    = 
| released       = 22 January 1962
| runtime        = 86 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
| preceded by    = 
| followed by    = 
}}

Tonny is a 1962 Norwegian drama film directed by Nils R. Müller and Per Gjersøe. It was entered into the 12th Berlin International Film Festival.   

==Cast==
* Per Christensen - Tonny
* Wenche Foss - Tonnys mor
* Liv Ullmann - Kari
* Joachim Calmeyer - Rørleggeren
* Rolf Daleng - Alfred
* Finn Kvalem - Rødtopp
* Ola B. Johannessen - Politimann
* Henny Skjønberg - Dame med parasoll
* Helga Backe
* Finn Bernhoft
* Erik Melbye Brekke
* Synnøve Gleditsch
* Rolf Nannestad
* Alfred Solaas

==References==
 

==External links==
* 

 
 
 
 
 
 
 