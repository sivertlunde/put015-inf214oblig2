Thaskara Lahala
 
{{Infobox film
| name           = Thaskara Lahala
| image          = 
| image_size     = 
| director       = Ramesh Das
| producer       = Azeez Kadalundi, Raji. P. K. Kollam
| writer         = T. V. Balakrishnan
| narrator       = 
| starring       = Suraj Venjaramood Salim Kumar Bheeman Raghu Lakshmi Sharma
| music          = Shyam Dharman
| cinematography = Anandakkuttan
| editing        = 
| distributor    = Beeyem Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
}}
Thaskara Lahala ( ) is a 2010 Malayalam-language comedy film directed by Ramesh Das starring Suraj Venjaramood in the title role. This is the second movie of the director whose first film Thattakam was a box-office flop. This is also the second movie in which comedy artist Suraj Venjaramood is playing the title role. Palunku fame Lakshmi Sharma is the heroine of the film. She will be playing the role of a police officer.

==Cast==
*Suraj Venjaramood
*Salim Kumar
*Bheeman Raghu
*Jaffer Idukki
*Nelson Sooranad
*Jagathi Sreekumar
*Lakshmi Sharma
*Shivaji Guruvayoor
*Meghanadhan
*Chembil Ashokan
*Geetha Vijayan
*Mamukkoya
*Sajitha Betti
*Subi
*Suresh
*Narayanankutty
*Thesni Khan
*Sinjili
*Kulappulli Leela
*Thara Kalyan
*Manka Mahesh
*Deepika Mohan
*Denise Sofia (Foreign Buyer - European)
*Santiago Torres Florez (Foreign Buyer - Colombian)

== External links ==
* http://www.nowrunning.com/movie/7648/malayalam/thaskara-lahala/index.htm
* http://www.indiaglitz.com/channels/malayalam/preview/11979.html
* http://popcorn.oneindia.in/title/8243/thaskara-lahala.html
* http://sify.com/movies/malayalam/review.php?cid=2428

 
 
 

 