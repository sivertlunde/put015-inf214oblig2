I due maghi del pallone
{{Infobox film
 | name =I due maghi del pallone
 | image =I due maghi del pallone.jpg
 | caption =
 | director = Mariano Laurenti
 | writer =  Roberto Gianviti
 | starring = Franco Franchi   Ciccio Ingrassia 
 | music =  Bruno Nicolai
 | cinematography =  	Tino Santoni
 | editing =   Giuliana Attenni
 | producer =  Leo Cevenini   Vittorio Martino
 | language = Italian  
 | released =  
 | country = Italy
 | runtime = 95 min
 }} 1970 Cinema Italian comedy film directed by Mariano Laurenti.         

== Plot summary ==
Ciccio Ingrassetti is the new manager of a small football team in a village in Sicily. Young players, however, have difficulty in asserting their rights, and so Ciccio decides to look for a so-called "magician" of the ball. Ciccio meets the villain sorcerer K.K., who really believes he can do a spell to heal team members. When K.K. understands that he should be the coach of the team, he gets angry with Ciccio, but this promises him a good sum of money, if K.K. will manage to win from the boys.

== Cast ==

*Franco Franchi: K.K.
*Ciccio Ingrassia: Ciccio Ingrassetti
*Lionello (singer)|Lionello: Tonino
*Karin Schubert: Gretel
*Elio Crovetto: Baldinotti 
*Umberto DOrsi: Cazzaniga 	
*Tiberio Murgia: Concettino Lo Brutto
*Enzo Andronico: Major of Pizzusiccu
*Paola Tedesco: Daughter of the Major
*Luca Sportelli: President of Ghiandineddese  
*Dada Gallotti: Adalgisa 
*Nino Vingelli: Don Alfio
*Ignazio Balsamo: Manager of Ghiandineddese

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 
 