College Swing
{{Infobox film
| name           = College Swing
| image          = College Swing poster.jpg
| image_size     =
| caption        = theatrical poster
| director       = Raoul Walsh
| producer       = Lewis E. Gensler
| writer         = Frederick Hazlitt Brennan (adaptation) Walter DeLeon Francis Martin Preston Sturges (uncredited)
| narrator       =
| starring       = George Burns Gracie Allen Martha Raye Bob Hope Betty Grable
| music          = Lyrics:     John Leipold Victor Young
| cinematography = Victor Milner
| editing        = LeRoy Stone
| distributor    = Paramount Pictures
| released       = April 29, 1938 (US)
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Jerry Colonna.

==Plot==
Its 1738, and Gracie Alden (Gracie Allen) of the powerful Alden family fails to graduate from the college founded by her grandfather for the ninth year in a row, so he leaves it in his will to the first female of the family to graduate within 200 years.  At the deadline, in 1938, another Gracie Alden, the last girl of the line, is having trouble with her studies, so she hires fast-talking Bud Brady (Bob Hope) to help her.  Her efforts are opposed by woman-hating professor Hubert Dash (Edward Everett Horton) and his secretary George Jones (George Burns), who dont want to see their beloved college fall into the hands of an empty-headed nit-wit like Gracie.

When by hook and by crook Gracie manages to pass her exam and becomes the owner of the college, she does away with entrance exams, hires a bunch of incompetent but kooky teachers, and turns the place into a jumpin jitterbugging joint complete with swing bands and remote radio broadcasts.  

==Cast==
*George Burns as George Jonas
*Gracie Allen as Gracie Alden
*Martha Raye as Mable
*Bob Hope as Bud Brady
*Edward Everett Horton as Hubert Dash
*Florence George as Ginna Ashburn
*Ben Blue as Ben Volt
*Betty Grable as Betty
*Jackie Coogan as Jackie John Payne as Martin Bates
*Cecil Cunningham as Dean Sleet
*Robert Cummings as Radio Announcer
*Skinnay Ennis as Skinnay Jerry Colonna as Prof. Yascha Koloski (uncredited)
*The Slate Brothers as themselves
*Robert Mitchell and St. Brendans Choristers as themselves

Cast notes:
*Mary Livingstone, who became well known as the wife of Jack Benny, appears in a small uncredited part as an usherette.
*Jackie Coogan and Betty Grable were married at the time they made this film together.

==Songs==
*"The Old School Bell" - sung by Robert Mitchell and St. Brendans Choristers
*"Moments Like This" - by Burton Lane (music) and Frank Loesser (words), performed by Florence George John Payne
*"College Swing" - by Hoagy Carmichael (music) and Frank Loesser (words), sung by Betty Grable and Skinnay Ennis, danced by Betty Grable and Jackie Coogan
*"Howja Like to Love Me?" - by Burton Lane (music) and Frank Loesser (words), performed by Martha Raye and Bob Hope
*"What Did Romeo Say To Juliet?" - by Burton Lane (music) and Frank Loesser (words), performed by John Payne and Florence George
*"What a Rumba Does to Romance" - by Manning Sherwin (music) and Frank Loesser (lyrics), performed by Martha Raye with Ben Blue, danced by Betty Grable and Jackie Coogan, George Burns and Gracie Allen, The Slate Brothers, and unidentified extras
*"Youre a Natural" - by Manning Sherwin (music) and Frank Loesser (lyrics), performed by Gracie Allen
*"Irish Washerwoman" - traditional Irish jig, danced by Gracie Allen Jerry Colonna.  

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 