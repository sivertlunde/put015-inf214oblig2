My Demon Lover
{{Infobox film
| name           = My Demon Lover
| image          = My Demon Lover poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Charlie Loventhal
| producer       = Gerald T. Olson Robert Shaye
| writer         = Leslie Ray  Scott Valentine Arnold Johnson David Newman
| cinematography = Jacques Haitkin
| editing        = Ronald Roose 
| studio         = New Line Cinema
| distributor    = New Line Cinema
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $3,922,273 
}}
 Scott Valentine, Arnold Johnson. The film was released on April 24, 1987, by New Line Cinema.  

==Plot==
 

==Cast== Scott Valentine as Kaz
*Michele Little as Denny 
*Robert Trebor as Charles
*Gina Gallego as Sonia
*Alan Fudge as Phil Janus
*Calvert DeForest as Man in Health Food Store Arnold Johnson as Fixer
*Donovan Baker as Burly Cop
*Teresa Bowman as Well Dressed Woman in Subway
*Dan Patrick Brady as Chip
*David S. Cass Sr. as Gradys Partner
*Eva Charney as Grady
*Betsy Chasse as Bibi
*Marsha Clark as Reporter #2 in Police Station
*Frank Colangelo as Bum on Dannys Street
*Ellen Gerstein as Policewoman
*James Gleason as Alert Cop
*Steven Hutchins as Young Kaz
*Franis James as Mrs. Szegulesco
*Karl Johnson as Sharpshooter
*Kelly McCullough as Blonde on Street
*Luce Morgan as Woman from Jersey
*James Morrissey as Monsters
*Raymond Patterson as Reporter #3 in Police Station
*Virginia Peters as Frumpy Woman
*Peewee Piemonte as Charles Monster
*Leslie Ray as Leggy Redhead
*Hank Rolike as Doctor
*Lin Shaye as Anemic Counter Girl
*Tasia Valenza as Miguela
*Kym Valentine as Reporter #1 in Police Station
*Daniel Zippi as Nerd 

==Reception==
The film grossed $1,815,583 in its opening weekend. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 