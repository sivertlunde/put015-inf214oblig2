Terror Trail
 
{{Infobox film
| name           = Terror Trail
| image          = Terror Trail (1921) - 1.jpg
| caption        = Still from the production with Sedgwick on the window ledge
| director       = Edward A. Kull
| producer       =  John Grey Edward A. Kull George H. Plympton
| starring       = Eileen Sedgwick George Larkin
| cinematography = 
| editing        =  Universal Film Manufacturing Co.
| released       =  
| runtime        = 18 episodes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 Western film serial directed by Edward A. Kull. It is considered to be a lost film.   

==Cast==
* Eileen Sedgwick as Vera Vernon / Elaine Emerson
* George Larkin as Bruce Barnes
* Theodore Brown as Bertram Russell
* Albert J. Smith as Hunch Henderson
* Barney Furey as Holmes
* Pierre Couderc

==See also==
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 