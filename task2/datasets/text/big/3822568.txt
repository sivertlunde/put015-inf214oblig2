Creation (1931 film)
Creation unfinished 1931 animator Willis storyline to King Kong.

==Plot==

40 degrees south of the equator, off the coast of southern Chile, not far from Patagonia, the luxury yacht Titan rests at anchor. On board ship is wealthy American tycoon Thorton Armitage along with his young son Billy, his socialite daughter Elaine, her large soft aunt Louise, and Billys personal tutor Steve, who will eventually emerge as the hero. Steve is teaching little Billy about evolution and Billy compares each of his family to different dinosaur, his aunt reminds him of a Brontosaurus, his father a T.Rex. Meanwhile, below deck the seductive Elaine enters and flirts with Steve; thrilled at the thought that Elaine loves him, Steve grabs her in his arms. Elaines fiancee, Ned Hallet, watches from a distance as the two kiss. When Steve looks to confess his love for Elaine, he is stunned as she explains that the kiss meant nothing. Embarrassed and enraged, Steve threatens to quit the crew, While above deck the passengers can see ominous storm clouds are brewing.

With freak suddenness a tempest is upon them, through whirling seas they see a submarine dispatched from the Chilean navy, in an effort to save the Armitage party, the Chilean captain enters and encourages the crew of the Titan to board the submarine. All of sudden a massive waterspout forms, tears the Titan from its anchorage, and hurls it away. From the churning waters, a massive earthquake thrusts up a huge rocky promontory. The beautiful yacht is then dashed upon the new island and is destroyed just as the submarine dives beneath the surging waves. The submarine is pulled downward by the rise of the rocky headland and spirals out of control. Buffeted by the surging currents and thrown off course the stunned crew and passengers find themselves inside a deep underwater ravine. The submarines crew look through the port hole to see green water filled with strange monsters, their amazement is cut short with a sudden jolt as the sub runs aground on a volcanic shelf, the submarine then surfaces to find itself on a tropical lake surrounded by steep cliffs. They have traveled through deep underwater caverns and have surfaced in the cauldron of an extinct volcano.

The crew soon discover that dinosaurs exist on the island and promptly escape from a stampede of Brontosaurs. Later on shore, the Chilean crew are attacked by a prehistoric rhinoceros-like mammal known as an Arsinotherium while trying to cross a log bridge. The Arsinotherium gores many of the sailors to death, before knocking the fleeing sailors from the log bridge into the raging river below; weak with horror, Steve stumbles through the jungle back to the others and just avoids been killed by a bull Woolly Mammoth.
The survivors assuming that they will never see civilization again, build a shelter to live in on a high cliff over looking the lush valley. After Ned Hallet shoots a Apatosaurus|Brontosaurus (now Apatosaurus), the sauropod retaliates by destroying one of the shelters with its long neck, and it is Elaine that saves everyone by striking a burning torch into the beasts mouth. Everyone is furious with Hallet for putting them in danger; Hallet leaves the camp in anger after being scolded and takes his frustration out on a baby Triceratops. The mother rushes to the aid of the dying infant and gores Hallet to death.

Steve then takes command over the group and decides to repair the radio from the sub, but needs a replacement leyden jar for it to work. While the survivors are exploring ancient ruins, looking for a replacement leyden jar, a block gives way, causing Elaine to fall. Elaine is attacked by a Pteranodon which is driven off by Steve. Shortly afterwards, the crew are chased into a temple by an aggressive Stegosaurus. The crew is then trapped between the Stegosaurus and a Tyrannosaurus which lives in the temple. Rather than kill the crew, the two dinosaurs fight one another. In the end, the Tyrannosaurus kills the Stegosaurus and feeds it to its infant as the crew escapes.

After searching the temple, the crew discover a type of metal jar which they could use to get their radio working. The crew try to send out an S.O.S., but the volcano begins to erupt, causing mass hysteria among the dinosaurs. A Pteranodon crashes into the tower used to conduct the signal, leaving the crew stranded to be killed by the erupting volcano. Facing death, Elaine and Steve finally confess their love for one another. As Billy faints from heat exhaustion, then rescue planes arrive and land on the boiling lake and save the crew from certain doom.

Aboard the rescue ship, Thorton Armitage boasts of his groups adventures. Everyone is sceptical about his claims, when suddenly a Pteranodon exhausted from its long flight, falls aboard the ships deck, proving his story to be true. The film ends with Steve and Elaine sharing a laugh as they look forward to their new life together.

==Legacy== King Kong. The Most Dangerous Game, was reportedly salvaged from this movie. 
 The Land That Time Forgot.

In the remaining footage, two infant Triceratops play tug of war with a vine before their mother separates the two. The loser of the conflict wanders off into the jungle where it views a chimpanzee, a jaguar, and eventually an armed sailor identified by the script as Ned Hallet. Hallet stumbles upon the infant Triceratops and shoots it through the eye. The dying infant screams out to its mother who charges to the rescue, chasing Hallet through the jungle. The final scene in which Ned Hallet is gored to death by the vengeful mother is lost. Merian C. Cooper intended to use the footage in King Kong, but felt that it didnt fit in with the rest. An altered Triceratops scene does appear in the 1932 Kong screenplay and the film-novelization.

Parts of the remaining footage can also be seen in the computer game "Dinosaur Museum" by Perspective Visuals and the educational video "More Dinosaurs".

Highlights included a dinosaur stampede, an attack from an Arsinoitherium, an attack from a Pteranodon, a battle between a Tyrannosaurus and Stegosaurus, and a volcanic eruption.

Some of the other creatures planned for the movie were an Agathaumas and a Styracosaurus.

==Influence on Kong==
* The scene in King Kong in which Kong shakes sailors off of a log bridge was inspired by the scene in Creation in which the Arsinoitherium knocks sailors from a log.
* Ann Darrows abduction by the Pteranodon was heavily inspired by a scene in Creation in which a Pteranodon attempted to abduct Elaine, Steves love interest.
* All of the dinosaur models featured in King Kong were originally designed for Creation.

==Characters in Creation==
*Steve &ndash; Billy Armitages tutor, the storys main protagonist
*Thorton Armitage &ndash; an American Tycoon
*Elaine Armitage &ndash; Thorton Armitages socialite daughter
*Billy Armitage &ndash; Thorton Armitages youngest child
*Ned Hallet &ndash; Elaines fiancee (played by Ralf Harolde, the only known cast member)
*Louise &ndash; Billy and Elaines friendly and overweight aunt
*Benny &ndash; A Jewish chef
*Chico &ndash; Elaines pet monkey

==Bestiary==

===Dinosaurs===
*Apatosaurus|Brontosaurus (now Apatosaurus)
*Triceratops (with young)
*Stegosaurus
*Tyrannosaurus (with youth)

===Other prehistoric animals===
*Arsinoitherium
*Woolly Mammoth
*Elasmosaurus
*Pteranodon

===Modern animals===
*Jaguar
*Cheetah
*Chimpanzee

==See also==
*List of stop-motion films

==References==
 

==Sources==
*  , 1975) contains information about and stills from this unfinished film.
* RKO Production 601: The Making of Kong, The Eighth Wonder of the World (Willis OBrien and Creation)

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 