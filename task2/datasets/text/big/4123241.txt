Nola (film)
{{Infobox Film
 | name = Nola
 | director = Alan Hruska
 | producer = Jill Footlick Rachel Peters
 | writer = Alan Hruska
 | starring = Emmy Rossum James Badge Dale Mary McDonnell Steven Bauer
 | music = Edmund Choi
 | cinematography = Horacio Marquínez
 | editing = Peter C. Frank
 | country = United States
 | language = English
 | runtime = 97 minutes
 }}

Nola is a 2003 American romantic comedy film written and directed by Alan Hruska. It depicts the struggle of a young woman trying to survive in New York City while looking for her birth father. It premiered in New York City on July 23, 2004.

== Plot ==
After fleeing from her abusive stepfather, Nola (Emmy Rossum) travels to New York City searching for her biological father. She spends her first night sleeping in Central Park, but her luck changes when she is hired by the owner of a small diner. She ends up staying with the frycook/law school student Ben (James Badge Dale) until the real owner of the diner, Bens landlady Margaret (Mary McDonnell), hires Nola as her assistant for her escort service.

Things go well at the escort service until Niles, a billionaire client of Margarets service, has a bad session. Niles likes to receive rough physical activity from men cross-dressing as women, but only to a point. Wendy, one of Niless favorites, went a little too far and sent Niles into a rage. Niles demands Margaret rough Wendy up or else he will have it done, along with inducing the police to investigate the escort service. Nola attempts to help by making up Wendy to look battered and bruised, documenting it with photos, then sending her out of the country until Niles can calm down. Niless informants spot Wendy, no longer wearing the bruise makeup, trying to flee. Niles responds by arranging a subpeona for Margaret to appear before a grand jury and calling Nola directly, threatening her by revealing detailed information about her upbringing.

Further events lead Nola closer to finding her real father, but not without the help of a journalist (Steven Bauer), who is in need of a story on escort services.

==Featured cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- 
| Emmy Rossum || Nola
|- 
| Steven Bauer || Leo
|- 
| James Badge Dale || Ben
|- 
| Thom Christopher || Niles Sterling
|- 
| Sam Coppola || Gus
|- 
| Adam LeFevre || Sam
|- 
| Michael Cavadias || Wendy
|-
| Damian Young || Maitre D
|- 
| Mary McDonnell || Margaret Langworthy
|}

==Release==
Nola received a limited release on August 29, 2003.   

===Reception===
On Rotten Tomatoes, the film has a 0% approval rating from critics, based on 13 reviews. 

===Box office===
On its opening weekend, the film grossed $6,010 at 19 theaters, with a per-theater average of $316. The film grossed a total of $10,550. 

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  

 
 
 