Man and Beast
 
{{Infobox film
| name           = Man and Beast
| image          = 
| caption        = 
| director       = Edwin Zbonek
| producer       = Artur Brauner
| writer         = Sigmund Bendkover Sveta Lukic
| starring       = Götz George
| music          = 
| cinematography = Nenad Jovicic
| editing        = Mirjana Mitic
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = West Germany Yugoslavia
| language       = German
| budget         = 
}}

Man and Beast ( ,  ) is a 1963 CCC Film German-Yugoslavian war film directed by Edwin Zbonek. It was entered into the 13th Berlin International Film Festival.   

==Cast==
* Götz George – Häftling Franz
*   – Willi Köhler
* Katinka Hoffmann – Vera
* Helmut Oeser – Deserteur
* Herbert Kersten – Lederer
* Kurt Sobotka – SS-Man Kasche
* Alexander Allerson – SS-Man Goldap
* Petar Banićević – Stani
* Stanko Buhanac – Blanchi
* Nada Kasapić – Mother
* Marijan Lovrić - Rademacher
* Nikola Popović – Albert

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 