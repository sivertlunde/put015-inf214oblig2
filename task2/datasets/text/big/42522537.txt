A Girl at My Door
 
{{Infobox film
| name           = A Girl at My Door
| image          = A Girl at My Door poster.jpg
| caption        = Film poster
| director       = July Jung
| producer       = Lee Chang-dong   Lee Joon-dong
| writer         = July Jung
| starring       = Bae Doona   Kim Sae-ron   Song Sae-byeok
| music          = Jang Young-gyu   Han Hee-jung
| cinematography = Kim Hyun-seok
| editing        = Lee Young-lim CGV Movie Collage   CJ Entertainment
| released       =  
| runtime        = 119 minutes
| country        = South Korea
| language       = Korean
| budget         =     
| gross          =   
}}

A Girl at My Door ( ) is a 2014 South Korean drama film directed by July Jung, starring Bae Doona and Kim Sae-ron.      It screened in the Un Certain Regard section at the 2014 Cannes Film Festival.      

Due to the ambiguous relationship between the two main characters, the film was largely financed by the Korean Film Council. Because of this, the budget was limited to   and Bae and Kim agreed to not be paid. 

==Plot== undocumented immigrants of Southeast Asian and Korean-Chinese descent, many of whom are underpaid or not paid at all. The rest of the population are largely aging natives, as their children all leave for cities once grown.

Young-nam meets Seon Do-hee (Kim Sae-ron), a timid and withdrawn 14 year-old girl, covered with cuts and bruises; Do-hee is bullied by classmates in school and physically abused by her violent stepfather Yong-ha since Do-hees mother abandoned them. Most of the locals, however, keep silent about the injustices that Do-hee and the migrant workers endure, as Yong-ha helps them make money. After the dead body of Yong-has mother is found floating in the water, ruled an accidental death caused by the drunk older woman crashing her motorbike into the sea, Yong-ha attacks Do-hee yet again. Concerned for Do-hees safety, Young-nam offers to let the teenager stay at her house during the summer vacation, despite scrutiny from the villagers about their unusual living arrangements. For the first time in her life, Do-hee has someone who does not hit her and who actually calls her by her name (hence the Korean title Dohee-ya), instead of swearing at her, and the two wounded women offer each other friendship and solace. But this happy period abruptly ends when Yong-ha finds out Young-nams secrets, which are linked to why she was demoted back in Seoul, and uses them against her.   

==Cast==
* Bae Doona as Inspector Lee Young-nam
* Kim Sae-ron as Seon Do-hee
* Song Sae-byeok as Park Yong-ha
* Kim Jin-gu as Park Jeom-soon, Yong-has mother
* Son Jong-hak as Captain Eom
* Na Jong-min as Kim, policeman
* Gong Myeong as Kwon, policeman
* Kim Jong-gu as Boss Choi
* Park Jin-woo as Chief detective
* Arvind Alok as Bakim, foreign worker
* Robin Shiek as Salam, foreign worker
* Pokhrel Barun as Hoang, foreign worker
* Kang In-young as Hairdresser
* Lee Hyeon-jeong as Young-nams landlady
* Moon Sung-keun as Nam Gyeong-dae, police superintendent in Seoul
* Jang Hee-jin as Eun-jeong, Young-nams ex-lover
* Kim Min-jae as Jun-ho, policeman

==Reception==
At the films official Cannes screening at the Theatre Debussy, the audience gave it a three-minute standing ovation and it received mostly positive reviews from the media.    
 Screen Daily abuse that starts off as a seemingly familiar domestic drama before spiralling off into something more unnerving and vaguely disturbing. It is driven by a strong cast and makes the most of it rural location that should be an idyllic but in fact its surface beauty covers up a series of rather dysfunctional people. The film is given heart and soul by a magnetic performance by the excellent Doona Bae." 

Variety (magazine)|Variety wrote that the "wrenching drama" is a "layered expose of violence and bigotry in provincial Korean society" powered by "mesmerizing" performances by Bae Doona and Kim Sae-ron. It described Baes portrayal as "both towering and frail," while "Kim is electrifying." 

Twitch Film praised it as "Korean cinema at its finest," "gripping from start to finish   with its fair share of high drama, but unlike most of its Korean compatriots, it never overplays its hand and treats its audience with respect," and "so well-wrought that one cant help but be swept up in its artistry, which effortlessly plunges us into an intellectual reverie." 
 migration and labor," and praised "the three lead actors, especially Bae," who "offer memorable performances as troubled, lonely and searching souls." 

  and employ multiple threads are very admirable, the result is a mild-mannered piece short of a sufficiently substantial exposition of its plethora of characters and the problem they face. All this leads to Bae giving an internalized performance dangerously close to blankness; fortunately, Kim is on hand with a turn that suitably brings to the screen the psychotic state of her battered character." 

Film Business Asia also singled out Kim as "the standout performance in a generally impressive first feature by 34-year-old writer-director July Jung, that could have been even better with one more script revision and more animated playing by Bae. Despite that, its still an involving drama with few dull moments, continually shifting the power balance between the three main protagonists.   All of this is a rich concoction that Jung generally brings off. But there are also annoying loose ends that weaken its dramatic impact,   the backgrounds of the three leads are thinly drawn; and the final act has a slightly too manufactured feel. The film could easily lose 10 minutes to its benefit, by shortening or eliminating the repetitive scenes of the girls beatings." 

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan=10| 2014 Golden Rooster and   Hundred Flowers Awards  
| Best Actress in a Foreign Film
| Bae Doona
|  
|-
| rowspan=4|   23rd Buil Film Awards 
| Best Actress
| Bae Doona
|  
|-
| Best Supporting Actor
| Song Sae-byeok
|  
|-
| Best Supporting Actress
| Kim Sae-ron
|  
|-
| Best New Director
| July Jung
|  
|-
|   25th Stockholm International Film Festival 
| Best First Film
| A Girl at My Door
|  
|-
| rowspan=2|   51st Grand Bell Awards
| Best New Director
| July Jung
|  
|-
| Best New Actress
| Kim Sae-ron
|  
|-
|   15th Women in Film Korea Awards
| Best Director/Screenwriter
| July Jung
|  
|-
|   35th Blue Dragon Film Awards
| Best New Actress
| Kim Sae-ron
|  
|-
| rowspan=15| 2015
| rowspan=2|   20th Chunsa Film Art Awards
| Best Actress
| Bae Doona
|  
|-
| Best New Director
| July Jung
|  
|-
|   9th Asian Film Awards   Best Actress
| Bae Doona
|  
|-
| rowspan=7|  2nd Wildflower Film Awards  
| Best Director (Narrative Film)
| July Jung
|  
|-
| Best Actor
| Song Sae-byeok
|  
|- Best Actress
| Bae Doona
|  
|-
| Kim Sae-ron
|  
|-
| Best Screenplay
| July Jung
|  
|-
| Best Cinematography
| Kim Hyun-seok
|  
|-
| Best New Director
| July Jung
|  
|-
| rowspan=5|   51st Paeksang Arts Awards
| Best Film
| A Girl at My Door
|  
|- Best Actress
| Bae Doona
|  
|-
| Kim Sae-ron
|  
|-
| Best Supporting Actor
| Song Sae-byeok
|  
|-
| Best New Director
| July Jung
|  
|-
|}

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 
 
 
 
 
 