Yukon Gold (1952 film)
{{Infobox film
| name =  Yukon Gold 
| image =
| image_size =
| caption = Frank McDonald
| producer = William F. Broidy  William Raynor
| narrator =
| starring = Kirby Grant   Martha Hyer   Harry Lauter   Philip Van Zandt
| music = Edward J. Kay  
| cinematography = John J. Martin 
| editing = Ace Herman 
| studio = Monogram Pictures
| distributor = Monogram Pictures
| released = August 31, 1952
| runtime = 62 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} series of ten films featuring Kirby Grant as a Canadian Mountie. 

==Cast==
* Kirby Grant as RCMP Corporal Rod Webb
* Martha Hyer as Marie Briand 
* Harry Lauter as Ace Morgan 
* Philip Van Zandt as Clint McClay 
* Frances Charles as Nan Duval, Saloon Owner 
* Mauritz Hugo as Jud Powers  
* James Parnell as Renault  
* Sam Flint as Boat Captain  
* Roy Gordon as Inspector  
* Hal Gerard as Sam  
* I. Stanford Jolley as Charlie  
* Ward Blackburn as Henchman   Chinook as Chinook, Webbs Dog

==References==
 

==Bibliography==
* Drew, Bernard. Motion Picture Series and Sequels: A Reference Guide. Routledge, 2013. 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 