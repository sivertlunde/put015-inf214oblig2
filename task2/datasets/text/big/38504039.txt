The Witness (2000 film)
{{Infobox film
| name           = The Witness
| image          = Film_poster_for_the_movie_The_Witness_made_in_2000.jpg
| alt            = 
| director       = Jenny Stein
| producer       = James LaVeck
| editing        = Jenny Stein
| released       =  
| runtime        =  43 minutes
| country        = United States
| language       = English
}}

The Witness is a film made in 2000 by James LaVeck    and Jenny Stein.    The name of LaVeck and Steins non-profit organization is Tribe of Heart.   

The film depicts the transformation of a New York City construction contractor with an upbringing where animals were treated with suspicion or indifference to becoming an animal lover and animal rights activist.

Jason Longo was the cinematographer, and Jane Greenberg did the sound. 

The Witness has been translated into Spanish, Russian, Polish and other languages as well.

==Synopsis==

The film begins with scenes showing the day-to-day life of a construction contractor by the name of Eddie Lama. Footage shows Lama discussing a construction job with one of his employees, and Lama also talks about how various different racial and ethnic groups who work at his firm are able to get along together. A scene is shown with Lama petting two dogs, and there is also footage showing that Lama keeps multiple cats in his office. 

Lama talks about how his family did not have any animals; his mother was instinctively frightened and fearful of animals. He also talks about how kids would sometimes chase cats in alleyways or harm animals when he was growing up in Brooklyn. 

After an attractive female friend asks Lama to take care of a kitten, his perspective on animals begins to change. He grows to love the kitten, and he agrees when his friend asks whether he would or would not like to take care of another cat.

Lama then talks about petting a cat and feeling its lower legs; he then associates it with a drumstick. When his brother invites him over for a chicken dinner, he does not eat it, and his interest in the welfare of animals continues to grow. 

Footage shows how Lama has rigged the vehicles in his business with posters depicting an animal that has been stripped of its fur to make a fur coat.  

The next segment of the movie has Lama discussing the treatment of pigs as they are brought to slaughter while the camera shows a television playing undercover footage of a factory farm. Lama discusses the treatment that pigs get on the way to slaughter. 

The final segment of the movie has Lama discussing the fur business. Lama talks about how the animal must be either trapped in the wild or raised in captivity. Undercover footage is shown of animals who have been trapped in the wild, but the trap did not kill instantly. As a result, the animal must suffer for hours or days until the trapper kills it. 

Footage is then shown of animals kept in captivity on a fur farm, and Lama discusses the conditions they are kept in and the method in which they are slaughtered. 

The undercover video comes from Farm Sanctuary, Humane Society of the United States, People for the Ethical Treatment of Animals, International Fund for Animal Welfare, World Society for the Protection of Animals (Now known as World Animal Protection), and others. 

The final scenes of the movie show how Lama has outfitted a truck with an audio/visual system that can show the undercover footage of animals that have been trapped and animals raised in captivity.
 Sarah McLachlans Angel is heard in the background. 
 Angel for spokesperson for the ASPCA. 

==Reviews==

Pulitzer Prize–winning journalist Howard Rosenberg praised the film writing in that, "The importance of The Witness lies in its subject matter combined with its unconventional voice that denounces cruelty."  
  Rosenberg contrasted The Witness with such television shows as   calling Irwin a buffoon, and stating such shows exploit animals.

Reviewer Marianne Eimer, writing for Library Journal, stated that the film "should be considered a tool to promote kindness toward animals," noting however, that it was "not for the faint of heart." 

Reviewer Syd Baumel writing for The Aquarian also praised the film writing "though this is a film about heartbreaking tragedy, The Witness is infused with warmth and humour. Interspersed with his surprisingly erudite (. . . for a goodfella) tutorials on animal rights issues, Lama tells a frequently comical story of his awakening."   

Other publications that reviewed the film include Booklist, The Ann Arbor News, Santa Cruz Sentinel, Santa Cruz Good Times, Spirituality and Health, and Satya (now no longer publishing ).  

==Awards==

The film has won various awards:

*2007—STEPS International Rights Film Festival -- Free Thinker Award  

*2007—Open Your Eyes! International Film Festival -- Best Documentary Award 
 Artivist Film Festival -- Best Short Film Award
 New Jersey International Film Festival -- Best Documentary Award

*2000 -- Brooklyn Film Festival -- Best Documentary Feature (Jury)

*2000 -- Brooklyn Film Festival -- Best Documentary Feature (Audience)

*2000—Crested Butte Reel Fest -- Best of Festival Award 

*2000—Crested Butte Reel Fest -- Best Documentary Award

*2000—Canyonlands Film Festival -- Best Documentary Award
 Pare Lorentz Award. 

In the area of animal rights activism, the film won the Search for Excellence Award from the Latham Foundation.  

===Endorsements===

Individuals and entities that endorsed The Witness: 

*Actor William Baldwin 
*Primatologist Jane Goodall|Dr. Jane Goodall
*Writer Susan McElroy (author of Animals as Teachers and Healers: True Stories and Reflections, All My Relations: Living with Animals As Teachers and Healers, and other books) 
*Actor Alicia Silverstone
*Individuals affiliated with the California Teachers Association, United Federation of Teachers, and the Stanford Center for Biomedical Ethics.

===Broadcasts===

The Witness has been broadcast on various PBS stations and also on LinkTV and Free Speech TV. It is also available to watch for free on the Internet.   

== Future Work by Tribe of Heart ==
 early version was released in 2004, but is no longer offered by Tribe of Heart). The duo has  also created a website called HumaneMyth.org which advocates the view that it is not possible to humanely use animals for food. 

==References==

 

 

 
 
 
 