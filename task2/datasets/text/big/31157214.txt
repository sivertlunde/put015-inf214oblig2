With Love... from the Age of Reason
{{Infobox film
 | name        = With Love... from the Age of Reason
 | image       = WithLoveFromTheAgeOfReason2010Poster.jpg
 | caption     = Theatrical release poster
 | director    = Yann Samuell
 | writer      = Yann Samuell
 | starring    = Sophie Marceau Marton Csokas Michel Duchaussoy
 | producer    = Christophe Rossignon
 | distributor = Mars Distribution
 | budget      = €8,000,000 (estimated)
 | released    =  
 | runtime     = 97 minutes
 | country     = France Belgium
 | language    = French
 | gross       = 
}} age of Catholic tradition, and to remind her of what she wants to become.

==Plot==
Margaret is a beautiful and successful businesswoman selling power plants to the Chinese. With an adoring English lover, she appears to have everything going for her. On her fortieth birthday, Margaret receives the first bundle of letters she wrote to herself when she was seven years old. A jumble of colorful collages, photographs, and wildly creative puzzles seem to have come from a different girl entirely. In a letter the seven-year-old Margaret writes, "Dear me. Today I am seven years old and Im writing you this letter to help you remember the promises I made when I was 7, and also to remind you of what I want to become..." As her letters to herself keep arriving, Margaret finds herself becoming disenchanted. The letters evoke long-forgotten memories and cast doubt on many of the choices she made in her life. In many ways shes become the opposite of what she hoped to become as a child. Margaret visits her childhood village and, by reconnecting with people who see in her the girl they once knew, she starts finding her way to the woman she vowed to become.

==Cast==
* Sophie Marceau as Margaret 
* Marton Csokas as Malcolm
* Michel Duchaussoy as Mérignac
* Jonathan Zaccaï as Philibert
* Emmanuelle Grönvold as De Lorca 
* Juliette Chappey as Marguerite  
* Thierry Hancisse as Mathieu
* Roméo Lebeaut as Philibert (child)
* Jarod Legrand as Mathieu (child)
* Alexis Michalik as Margarets assistant
* Raphaël Devedjian as Simon
* Déborah Marique as Marguerites mother
* Emmanuel Lemire as Marguerites father
* Christophe Rossignon as Huissier
* Mireille Séguret as Madame Vermier
* Bernard Gerland as Old man   

==Production==
;Filming locations
* Cité Scolaire Internationale de Lyon, Lyon, Rhône, Rhône-Alpes, France 
* Morocco 
* Saou, Drôme, France   

==References==
 

==External links==
*  
*   at uniFrance

 
 
 