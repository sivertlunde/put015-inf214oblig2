The Three Weird Sisters
 
{{Infobox film
| name           = The Three Weird Sisters
| image          = 3weirdsistersfilm.jpg
| image_size     = 
| caption        = UK release poster
| director       = Daniel Birt
| producer       = Louis H. Jackson
| writer         = Dylan Thomas Louise Birt
| starring       = Nancy Price Mary Clare Mary Merrall Raymond Lovell
| music          = Hans May Ernest Palmer Moray Grant
| editing        = Monica Kimick
| distributor    = Associated British-Pathé
| released       = 1948
| runtime        = 82 minutes
| country        =   English
}}
 1948 British Gothic influences, directed by Daniel Birt and starring Nancy Price, Mary Clare, Mary Merrall and Raymond Lovell.  The screenplay was adapted by Dylan Thomas and Louise Birt from the novel The Case of the Weird Sisters by Charlotte Armstrong.  The film was Birts directorial debut, while marking the last screen appearance of Nova Pilbeam who retired from the acting profession after it was completed.

==Plot==
The elderly Morgan-Vaughan sisters Gertrude (Price), Maude (Clare) and Isobel (Merrall) live in a decaying and claustrophobic mansion in a Welsh mining village.  Gertrude is blind, Maude almost deaf and Isobel crippled by arthritis.  The local coal mine out of which the family made their fortune is almost worked-out and its tunnels and shafts are dangerously unstable.  When a section of the underground workings collapses catastrophically, destroying a row of local cottages and unsettling the foundations of the mansion, the sisters feel honour-bound to finance repairs but have no capital with which to do so.

The sisters younger half-brother Owen (Lovell), who left the village as a young man to pursue his education and has subsequently become a wealthy businessman in London, is sent for on the assumption that he will agree to underwrite the necessary finances from his own personal funds.  Owen and his secretary Claire (Pilbeam) arrive from London, and the sisters are disconcerted to discover that his view on the matter is informed by capitalism rather than altruism, he has no sense of responsibility towards either them or the community and he feels no obligation to throw good money after bad by restoring what he considers outdated.

As the conflict between the sisters sense of tradition and Owens modernity grows, strange events start to happen which eventually convince Claire that the sisters are plotting to murder Owen in order to lay hands on his money.  She tries to alert members of the local community to her suspicions, but at first is not taken seriously.  Gradually however the local doctor (Anthony Hulme) comes round to Claires point of view and deduces that there is indeed a plot under way, instigated by the dominant Maude to restore things to their former state by the convenient death of Owen.

==Cast==
* Nancy Price as Gertrude Morgan-Vaughan
* Mary Clare as Maude Morgan-Vaughan
* Mary Merrall as Isobel Morgan-Vaughan
* Raymond Lovell as Owen Morgan-Vaughan
* Nova Pilbeam as Claire Prentiss
* Elwyn Brook-Jones as Thomas
* Edward Rigby as Waldo
* Hugh Griffith as Mabli Hughes
* Marie Ault as Beattie
* Anthony Hulme as David Davies
* Doreen Richards as Mrs. Probert
* Bartlett Mullins as Dispenser

==References==
 
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 