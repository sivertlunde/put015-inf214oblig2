Distancias
{{Infobox film
| name           = Distancias
| image          = 
| caption        = 
| director       =  
| producer       = Estudi Playtime S. L.
| writer         = 
| starring       = Apollinaire, Emmanuel, Martin, Mama
| distributor    = 
| released       = 2008
| runtime        = 28 minutes
| country        = Spain
| language       = 
| budget         = 
| gross          = 
| screenplay     = Pilar Monsell
| cinematography = Pilar Monsell
| editing        = Pilar Monsell
| music          = 
}}
:This article is about the documentary. For the album, see Distancias (album).
Distancias is a Spanish 2008 short documentary film.

== Synopsis ==
A group of refugees from the Congo are stuck in Rabat, Morocco. The Spanish borderline stops them from entering Europe. In a room on the outskirts of the city they rehearse a theatrical piece based on their own experiences. An imperfect, unfinished staging. Real life and the stage are all mixed together. Apollinaire tells us, face to face, about the never-ending journey with an uncertain ending. Television images show us how many emigrants are expelled into the desert, forced to start their journey once more. Faces whose names are lost. Behind them, the empty spaces of what has already happened. Silent tracks that tell a story the same as ours.

== Awards ==
* Audience Award - 49 Festival dei Popoli, Florence 2008

== Festivals ==

Rompan Límites – Muestra de Cine Experimental de Maldonado (Uruguay).
“Border, displacement and creation. Questionen the contemporary” (Oporto).
I Festival de Cine Migrante de Bogotá.
Festival Migrant Scène de Toulouse y Burdeos.
I Festival de Cine Migrante de Buenos Aires.
IV Festival Sinima de Soria.
XV MedFilm Festival – Cine del Mediterráneo de Roma.
14 Festival Internacional de Cine de Ourense.
Perspektive 09 - Festival Internacional de Cine Documental de Nuremberg.
VI Festival de Cine Africano de Tarifa.
Muestra Ovni 2009, CCCB.
VI Documenta Madrid – Festival Internacional de Documentales.

== Website ==

http://distancias.es

== References ==
 

 
 
 
 
 
 
 
 
 
 


 
 