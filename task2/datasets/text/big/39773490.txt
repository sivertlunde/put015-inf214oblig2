Rooms to Let
{{Infobox film
| name           = Rooms to Let
| image          = 
| image_size     = 
| caption        = 
| director       = Carl Wilhelm
| producer       = Erich Engels  
| writer         = Bobby E. Lüthge   Carl Wilhelm 
| narrator       = 
| starring       = Lucie Englisch   Elisabeth Pinajeff   Kurt Vespermann   Ida Wüst
| music          = Paul Dessau
| editing        = 
| cinematography = Gustave Preiss 
| studio         = Erich Engels-Film 
| distributor    = Erich Engels-Film 
| released       = 14 January 1930
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent comedy sound was taking place.

==Cast==
* Lucie Englisch as Lotte
* Elisabeth Pinajeff as Bella Donna 
* Kurt Vespermann as Dr. Hans Weber
* Ida Wüst as Gattin
* Luise Bonn as Lola 
* Henry Bender as Theodor Kannebach
* Johanna Ewald as Frau Piefke 
* Heinrich Gotho as Herr Piefke 
* Ellinor Gynt as Amalie Wasserstoff 
* Ida Perry as Frau Amberg  Fritz Schulz as Fritz Blitz 
* Emmy Wyda as Fräulein Schmitz 

==References==
 

==Bibliography==
*Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910–1933. Berghahn Books, 2005.

==External links==
* 

 
 
 
 
 
 
 

 