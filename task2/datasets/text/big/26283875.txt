New * Desired Sister-in-Law: Immoral Relations
{{Infobox film
| name = New * Desired Sister-in-Law: Immoral Relations
| image = New * Desired Sister-in-Law Immoral Relation.jpg
| image_size = 190px
| caption = Theatrical poster 
| director = Toshiya Ueno 
| producer = 
| writer = Masahiro Kobayashi
| narrator = 
| starring = Hiromi Miyagawa Yumeka Sasaki
| music = Isao Yamada
| cinematography = Yasumasa Konishi
| editing = Naoki Kaneko
| distributor = Kokuei Shintōhō
| released =  
| runtime = 60 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  aka    is a 2001 Japanese pink film directed by Toshiya Ueno. It won the Silver Prize at the Pink Grand Prix ceremony.

==Synopsis==
The film follows the romantic entanglements of a married woman whose brother-in-law is an actor. 

==Cast==
* Hiromi Miyagawa ( ) 
* Yumeka Sasaki ( )
* Hidehisa Ebata ( )
* Mikio Satō ( )
* Daisuke Iijima ( )
* Toshimasa Niiro ( )

==Background==
New * Desired Sister-in-Law: Immoral Relations is the fourth film in the  , or Lustful Sister-in-Law series of pink films directed by   (1999), Lecherous Older Sister (2000), and New * Desired Sister-in-Law: Immoral Relations (2001) - are regarded as minor classics of modern pink for their "mordant black humor". 

==References==
;Notes
 

;Bibliography
*  
*  
*  
*  

 
 

 
 
 
 
 
 


 
 