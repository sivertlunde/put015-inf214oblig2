Devaraya (film)
{{Infobox film
| name = Devaraya
| image = Devaraya Movie Wallpaper.jpg
| writer = Veerababu Basina
| screenplay = Ravireddy Mallu Srikanth Meenakshi Vidisha
| director = Nani Krishna
| cinematography = Poorna
| producer = Kiran Jakkam Shetty Nani Krishna
| editing = 
| fights = Ravi Varma
| distributor = Nani Gadi Cinema Sunray International Cinema
| country = India
| released =  
| runtime = 
| language = Telugu Chakri
| budget = 
| gross = 
}} Vidisha in Chakri provided the music.  The film was released worldwide on 7 December 2012. 

==Cast== Srikanth as Sri Krishna Devaraya / Dora Babu
* Meenakshi Dixit as Sunanda Vidisha as Swapna
* Jaya Prakash Reddy
* M. S. Narayana
* Mannava Balayya|M. Balayya as Mahamamthri Thimmarusu Jeeva
* Sivaji Raja Ranganath
* santosh (Hero)Santosh

==Production== Srikanth will be seen in a Double role of Sri Krishna Devaraya, a Famous Indian Emperor known for his Contributions in terms of Arts and Literature and Dora Babu, a reckless, irresponsible, hedonistic person in a nondescript village.  Meenakshi Dixit who was last seen in an item song in Mahesh Babus Dookudu is playing the female lead in the film. She appears as a princess alongside Srikanth in it and She felt lucky to essay this role.  Kanishka Soni sizzled in item song Bavlu in this movie. 

==Soundtrack==
{{Infobox album
| Name = Devaraya
| Longtype = to Devaraya
| Type = Soundtrack Chakri
| Cover =
| Released = 12 September 2012
| Recorded = 2011-2012 Feature film soundtrack
| Length =  Telugu
| Label = Aditya Music Chakri
| Reviews =
| Last album = Srimannarayana (2012)
| This album = Devaraya (2012)
| Next album = Denikaina Ready (2012) 
}}
The audio release event of Devaraya was a grand event, which witnessed the presence of Meenakshi Deekshith, Vidisha, Kottaplly Subbaraidu, Prasanna Kumar, Sagar, Tammareddy Bharadwaja, Girish Reddy, Poorna, Sammetta Gandhi, Madhav, Ravi Kumar Chavali, Chakri, Sanvi, Puppala Ramesh, Sivaji Raja, KVV Satyanarayana and many others.

Prominent actor Pawan Kalyan attended the audio launch of Devaraya, which was held at a private auditorium in Hyderabad on September 12, 2012. Pawan Kalyan unveiled the audio CDs amidst huge fanfare and handed it over to AP State minister Ghanta Srinivasa Rao. Later, Pawan Kalyan addressed the music launch function of Devaraya and said that Srikanth is very close to his brother Chiranjeevi and he has very high regard for him. He had seen the trailers of the movie and very impressed with it. He wished the entire team good luck and hoped that the music of the film will be a chartbuster. 
{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Sri Krishnaraya
| note1           = 
| lyrics1         = Venigalla Rambabu Malavika
| length1         = 02:38
| title2          = Halale Halale
| note2           =  
| lyrics2         = Kandikonda
| extra2          = Vinod Kaapu, Adarshini 
| length2         = 04:28
| title3          = Nickker Vesinappudu
| note3           = 
| lyrics3         = Paidisetti
| extra3          = Chakri (music director)|Chakri, Anjana Sowmya
| length3         = 04:37
| title4          = Bavalu
| note4           = 
| lyrics4         = Balaji
| extra4          = Neha
| length4         = 03:39
| title5          = Guchi Guchi
| note5           = 
| lyrics5         = Praveen Lakma
| extra5          = Hari, Kousalya
| length5         = 05:37
| title6          = Chakkerakeli
| note6           = 
| lyrics6         = Karunakar
| extra6          = Venu Srirangam, Malavika, Phani, Simha
| length6         = 04:20
| title7          = Neerajanam
| note7           = 
| lyrics7         = Venigalla Rambabu
| extra7          = Kousalya
| length7         = 02:14
}}

==References==
 

 