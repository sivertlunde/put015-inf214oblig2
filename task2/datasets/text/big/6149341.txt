Behind Enemy Lines II: Axis of Evil
{{Infobox film
| name           = Behind Enemy Lines II: Axis of Evil
| image          = bel2ae.jpg
| caption        = DVD cover James Dodson
| producer       = James Dodson Roee Sharon
| writer         = James Dodson
| starring       = Nicholas Gonzalez Matt Bushell Bruce McGill Ben Cross Glenn Morshower Keith David Peter Coyote Joseph Steven Yang
| music          = Pinar Toprak
| cinematography = Lorenzo Senatore
| editing        = Ethan Maniquis
| distributor    = 20th Century Fox
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
}} Behind Enemy James Dodson, starring Nicholas Gonzalez, Matt Bushell,  Keith David, Bruce McGill, and Peter Coyote. Despite its title, the film does not follow the first part, and was released direct-to-video on October 17, 2006.

==Plot==
The story is not linked to the first part of the series. Instead, it focuses on a fictional explanation for the Ryanggang explosion in 2004, in which an unexplained mushroom cloud occurred in North Korea.
 nuclear weapon in North Korea, which can strike anywhere in the continental United States, a fictional United States President Adair T. Manning (Peter Coyote) orders a team of U.S. Navy SEALs to destroy the missile and the launch site. The team is led by Lieutenant Robert James (Nicholas Gonzalez).

The Pentagon aborts the mission after it receives new information, but by the time the abort order is sent, two SEALs have already parachuted into North Korean territory. James stops the third SEAL from deploying, accidentally knocking the mans helmet against the status indicator mounted near the door. The lieutenant steps onto the makeshift ramp to peer outside, returning to the doorway to inform the rest of men of the abort. The high-speed winds from outside rip the indicator loose and send it flying into the lieutenants face. Stumbling backwards, James loses his balance and is sucked out of the plane. Callaghan disobeys orders to stand fast, strikes his commanding officer, and follows the first three, taking a radio with him. When North Korean forces led by Commander Hwang (Joseph Steven Yang) find the SEALs, two of the Navy SEALs are killed in a gun battle, and James and Callaghan are captured and tortured by Hwang and his men.

After South Korean special forces rescue James and Callaghan, President Manning and the South Korean government send the SEALs and South Korean special forces to destroy the missile site. But after losing radio contact with the SEALs, the President and his top advisers believe that they have been captured again. Under pressure from his military advisor, General Norman Vance (Bruce McGill), the President decides to send B-2 Spirit|B-2 stealth bombers to destroy the site, which would start a full-scale war against North Korea. The SEALs and the South Korean special forces are almost recaptured by Hwang, but he is shot by a defecting officer. James and the South Koreans destroy the missile silo with a bomb before the bombers reach the missile site, which averts the bombing and prevents a full-scale war.

A tribunal convicts Callaghan of striking an officer (1 year) and disobeying an officer (10 years). Due to the "black op" nature of the mission, the transcript of the hearing is deemed classified and the charges are expunged from his record, leaving him free to return to his family.

Meanwhile James meets the president in a classified meeting, bringing his mentor Master Chief Scott Boytano (Keith David) as witness to James receiving of an award.

The film closes with Boytano telling James he wasnt red flagged because Boytano had never seen anyone who desired so badly as James did to be a SEAL.
During the credits there is a news report on the Ryanggang explosion.

==Cast==
*Nicholas Gonzalez as Lieutenant Robert James
*Matt Bushell as Master Chief Neil T. "Spaz" Callaghan  
*Bruce McGill as General Norman T. Vance
*Peter Coyote as U.S. President Adair T. Manning  
*April Grace as Secretary of State Ellie Brilliard
*Glenn Morshower as Admiral Henry D. Wheeler
*Joseph Steven Yang as Commander Hwang
*Kenneth Choi as South Korean Ambassador Li Sung Park
*Keith David as Master Chief Scott Boytano

==Precursor== Behind Enemy Lines, starring Owen Wilson. Although the basic theme is credited to the first film, the plot and setting are not interlinked between either films.

==Sequels==
It was followed by two direct-to-video sequels,  , which stars the professional wrestler  , which stars Tom Sizemore.

==Reception==
Axis of Evil was received poorly in the industry and the directors were criticized for "put  a bad name to a well renowned title", and as an "expectedly inferior sequel" in contrast to its predecessor. It did not come close to the original films success at the Box Office. It currently has a 4.5/10 rating on The Internet Movie Database.

==See also==

* List of films featuring the United States Navy SEALs

==External links==
 
* 

 

 
 
 
 
 
 