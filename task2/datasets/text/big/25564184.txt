Decalogue V
{{Infobox film
| name = Decalogue V
| image = Dvd parts5-6.png
| caption =
| director = Krzysztof Kieślowski
| producer = Ryszard Chutkovski
| writer = Krzysztof Kieślowski Krzysztof Piesiewicz
| starring = Miroslaw Baka Jan Tesarz Krzysztof Globisz
| music = Zbigniew Preisner
| cinematography = Slawomir Idziak
| editing = Ewa Smal
| art director = Halina Dobrowolska
| sound = Malgorzata Jaworska
| distributor = Polish Television
| released = 1988
| runtime = 57 min.
| country = Poland Polish
| budget = $10,000
| gross = 
}}
 The Decalogue by Polish director Krzysztof Kieślowski, connected to the sixth imperative of the Ten Commandments: "Thou dost not murder".

A brutal and seemingly motiveless murder brings together a young drifter, Jacek (Mirosław Baka), a taxi driver (Jan Tesarz), and an idealistic lawyer, Piotr (Krzysztof Globisz). This is the only one of the films with an explicit political stance, reflecting Kieślowskis opposition to the death penalty. An expanded 84-minute cinema version of this episode was released as Krótki film o zabijaniu (A Short Film About Killing). 

==Plot==
 
The film begins with Piotr Balicki (Krzysztof Globisz), a young and idealistic lawyer who is about to take the bar exam. Jacek Łazar (Mirosław Baka) is a 20 year-old man, coming from the countryside. He wanders the streets of Warsaw and has apparently nothing to do. He keeps asking about a taxi stand but the first one he finds is very busy. Waldemar Rekowski (Jan Tesarz), a middle-aged taxicab driver, overweight, cruel-looking, lives in the Dekalog apartment block. He enjoys the freedom of his profession, with a wage and the power to ignore people whom he does not want to take in his taxi, as well as ogling young ladies.
 blown up and then goes to a cafe. The taxi driver wanders around the city looking for a client. Jacek keeps a length of rope in his bag and a stick; he wraps a bit of rope around his hand but stops when he spots two girls playing at the other side of the window at the cafe and he engages in a game with them. He then goes to a taxi stop and jumps into a taxi, mendaciously declining to cede his taxi to other people who seem much busier. The taxi driver happens to be Waldemar Rekowski. Jacek asks to be driven to a part of the city near the countryside. There, Jacek kills the driver using the rope, in a brutal and extended scene in which he has to finish his killing using a big stone as Rekowski begs for mercy. He then takes the taxi to the river and dumps the body. Jacek starts eating a sandwich made by taxi drivers wife. At the same time, he turns on the radio and hears a childrens song about a young lion learning to be brave.  Upset, Jacek rips the car radio from the dashboard and casts it in the river.

We cut to Jacek in court, having just been convicted of robbery and murder.  Jaceks lawyer is Piotr, in his first case after finishing law school. Piotr, who earlier argued the immorality of the death penalty, is distressed at having failed to save his client from a death sentence, and enters the judges chambers to ask if a more experienced or articulate lawyer might have succeeded.

Before the execution Jacek asks to speak one last time to Piotr, who we learn is a new father.  Piotr tells Jacek that it was not he but his deed that was condemned; Jacek sees no distinction. Jacek reveals that five years earlier his beloved 12-year-old sister was killed by a drunk driver who had been drinking vodka with his friend—Jacek. Jacek wonders if he would have lived a better life if his sister hadnt died.  He asks to be buried alongside his sister and father, although there is only one remaining burial spot which his mother would have to agree to relinquish. Jacek asks Piotr to give the enlarged photo of the girl to his mother.

The time for the execution arrives, and Jacek is held down by several policemen and restrained. He is hanged with violent abruptness, with Piotr a horrified witness.

The final scene finds Piotr sitting in his car in the countryside, repeating in anguish, "I abhor it, I abhor it."    

==Cast==
 
* Mirosław Baka - Jacek
* Krzysztof Globisz - Piotr
* Jan Tesarz - taxi driver
* Zbigniew Zapasiewicz - police inspector
* Barbara Dziekan-Wajda - cashier

* In other roles
** Aleksander Bednarz, Jerzy Zass, Zdzisław Tobiasz,
Artur Barciś, Krystyna Janda, Olgierd Lukaszewicz

===Cast notes===
* From actors, who appeared in other episodes, we can briefly see the pair from Decalogue II, Krystyna Janda (Dorota) and Olgierd Lukaszewicz (Andrzej)

==References==
 

 

 
 
 
 
 