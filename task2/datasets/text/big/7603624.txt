Godspell (film)
 
{{Infobox film
| name           = Godspell
| image          = Godspellmoviep.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster David Greene Edgar Lansbury
| writer         = David Greene John-Michael Tebelak
| starring       = Victor Garber David Haskell Stephen Schwartz
| cinematography = Richard Heimann
| editing        = Alan Heim
| distributor    = Columbia Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English Hebrew Spanish
| budget         = $1.3 million
| gross = $1,200,000 (US/ Canada rentals) 
}} musical Godspell Stephen Schwartz. David Greene with stars Victor Garber as Jesus and David Haskell as Judas Iscariot|Judas/John the Baptist, the film is set in contemporary New York City. John-Michael Tebelak is credited as co-writer of the screenplay and served as the creative consultant, although director David Greene said Tebelak did not write the screenplay.  

==Plot==
The structure of the musical is, in large part, retained: a series of parables from the gospel of Matthew, interspersed with musical numbers. Many of the scenes take advantage of well-known sites around an empty, still New York City. John the Baptist gathers a diverse band of youthful disciples to follow and learn from the teachings of Jesus. These disciples then proceed to form a roving acting troupe that enacts Jesuss parables through the streets of New York. They often make references to vaudeville shtick.

==Cast==
 
* Victor Garber as Jesus Christ
* Katie Hanley as Katie, a diner waitress
* David Haskell as John The Baptist & Judas Iscariot  
* Merrell Jackson as Merrell, a garment trader
* Joanne Jonas as Joanne, a ballet dancer
* Robin Lamont as Robin, a window shopper
* Gilmer McCormick as Gilmer, a model
* Jeffrey Mylett as Jeffrey, a taxi driver
* Jerry Sroka as Jerry, a parking attendant
* Lynne Thigpen as Lynne, a college student

==Musical numbers==
# "Prepare Ye the Way of the Lord" - John the Baptist
# "Save the People" - Jesus Day by Day" - Robin
# "Turn Back, O Man" - Joanne
# "Bless the Lord" - Lynne
# "All for the Best" - Jesus, Judas
# "All Good Gifts" - Merrell
# "Light of the World" - Jerry, Gilmer, Jeffrey, Robin
# "Alas for You" - Jesus, (Tebelak voices the Pharisee Monster)

# "By My Side" - Katie
# "Beautiful City" - Company
# "Beautiful City" (Instrumental Reprise)
# "On the Willows"
# "Finale" - Jesus

==Differences from the musical== The Prodigal Son.

While the play requires very little stage dressing, the film places emphasis on dramatic location shots in Manhattan. (Except for the opening scenes and the very last scene, the city streets and parks are devoid of people other than the cast.) Locations include the following:
* The Brooklyn Bridge, where John the Baptist walks down the pedestrian walkway while humming "Prepare Ye the Way of the Lord" in the opening sequence;
* Bethesda Fountain in Central Park, in which the new disciples are baptized by John while singing "Prepare Ye";
* The Andrew Carnegie Mansion, in which they sing "Turn Back O Man";
* Times Square, where Jesus and John are silhouetted by a screen of lights as they dance to "All For the Best";
* The central fountain at Lincoln Center, where Jesus and Judas dance on the fountains stone lip as they sing "All for the Best"; North Tower  of the World Trade Center, which was nearing completion at the time of the films production;  Riverside Drive; Tishman Building  during "Beautiful City"; musical version The Prodigal Son is told.
* The long approaches of the Hell Gate Bridge on Randalls Island, where the disciples switch to their colourful clothing after baptism near the start of the film, and where they carry Jesus body through the arches, creating a barrel-vaulted cathedral effect, after his crucifixion on a chain-link fence.

Vocally, the chorus is very much in the same style, but solo parts are, at times, more lyrical. Notably, in "All Good  Gifts", whereas Lamar Alford had used a dramatic tenor voice, Merrell Jackson uses a lighter voice and falsetto for the high ornament which creates a joyous effect.
 Ricky Shutter on drums and percussion. Reviewer William Ruhlmann explains that by having a larger budget than had been available for the stage, Schwartz was able to expand the line-up by adding key studio personnel like lead guitarist Hugh McCracken (on "Prepare Ye (The Way of the Lord)") and keyboardist Paul Shaffer, a horn section, and six strings. {{cite web
 |last1=Ruhlmann
 |first1=William
 |title=Overview: Godspell: Original Motion Picture Soundtrack
 |url=http://www.allmusic.com/album/godspell-original-motion-picture-soundtrack-mw0000314966
 |website=Allmusic.com
 |accessdate=April 22, 2015}} 

Ruhlmann describes Schwartz as being "better able to realize the scores pop tendencies than he had on the cast album... this was a less complete version of the score, but it was much better performed and produced, making this a rare instance in which the soundtrack album is better than the original cast album. 

==Reception==
The film was entered into the 1973 Cannes Film Festival.   

Godspell received generally positive reviews in 1973. Allmovie Guide currently gives the film a three out of five rating. Various bands have covered songs from the film/musical.
<!--Deleted because this Wikilink redirects back here  ==See also==
* Godspell (album)-->

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 