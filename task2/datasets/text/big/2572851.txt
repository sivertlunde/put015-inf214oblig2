Beowulf (2007 film)
{{Infobox film
| name           = Beowulf
| image          = beowolfposter.jpg
| caption        = US theatrical release poster
| director       = Robert Zemeckis
| producer       = Robert Zemeckis Steve Bing Jack Rapke Steve Starkey
| screenplay     = Neil Gaiman Roger Avary
| based on       =   Robin Wright Penn Brendan Gleeson Crispin Glover Alison Lohman Angelina Jolie
| music          = Alan Silvestri
| cinematography = Robert Presley
| editing        = Jeremiah ODriscoll
| studio         = Shangri-La Entertainment ImageMovers Warner Bros. Pictures  (international) 
| released       =  
| runtime        = 115 min.
| country        = United States
| language       = English
| budget         = $150 million
| gross          = $196.3 million
}} Old English epic poem of the The Polar Robin Wright IMAX 3D, RealD Cinema|RealD, Dolby 3D and standard 2D format.

==Plot== Beowulf (Ray King Hrothgar Robin Wright Penn), who does not love her husband and reciprocates Beowulfs interest.

Beowulf and his men celebrate in Heorot to lure Grendel out. When the beast does attack, Beowulf engages him unarmed and naked, determining that since Grendel fights with no weapon or armor he shall face him as equal. During the melee, Beowulf discovers that Grendel has hypersensitive hearing, which is why he interrupts Hrothgars celebrations – the noise they make is physically painful to him. After his eardrum is ruptured by Beowulf, he attempts to escape (having shrunk in size due to the injury). Beowulf manages to restrain Grendel and severs his arm using the door. In thanks for freeing his kingdom from the monster, Hrothgar gives Beowulf his golden drinking horn, which commemorates Hrothgars victory over the mighty dragon Fafnir.

Returning to his cave, the dying Grendel tells his mother what was done to him and by whom, and she swears revenge. She travels to Heorot in the night and slaughters Beowulfs men while they were sleeping. Hrothgar tells both Beowulf and Wiglaf, who had been sleeping outside the hall during the attack, that it was the work of Grendels mother, the last of the Water Demons, who was thought by Hrothgar to have left the land. Beowulf and Wiglaf travel to the cave of Grendels mother to slay her. Only Beowulf enters the cave where he encounters Grendels mother (Angelina Jolie), who takes the form of a beautiful woman. She offers to make him the greatest king who ever lived if he will agree to give her a son to replace Grendel and let her keep the golden drinking horn. Beowulf agrees to the deal and returns, claiming to have killed her. Hrothgar, however, realizes the truth after hearing Beowulf describe her as a "hag" and a "witch." He tells Beowulf indirectly that, much like Beowulf, he was also seduced by Grendels mother; Grendel was the result of their tryst. Hrothgar names Beowulf his successor as king, much to the dismay of his royal advisor, Unferth (John Malkovich), who was hoping to take the throne. Hrothgar then commits suicide by jumping from the castle parapet onto the beach below. A wave momentarily engulfs Hrothgars body, there is a golden flash underwater, and the body is gone.

Years later, the elderly Beowulf is married to Wealtheow. Over the years they had grown apart, husband and wife in name alone. Beowulf takes a mistress, Ursula (Alison Lohman). One day, Unferths slave Cain (Dominic Keating) finds the golden drinking horn in a swamp near Grendels cave and brings it back to the kingdom. That night, a nearby village is destroyed by a dragon, which leaves Unferth alive in order to deliver a message to King Beowulf: the dragon is Beowulfs son born to Grendels mother. Removing the horn has voided the agreement between Beowulf and Grendels mother, who has now sent their son, the dragon, to destroy his kingdom.

Beowulf and Wiglaf go to the cave once again and Beowulf goes into the cave alone. When Grendels mother appears, Beowulf gives her the golden horn to convince her to stop the attack. Grendels mother considers it too late for any kind of agreement. She releases the dragon to attack Beowulfs kingdom again. Beowulf goes to great lengths to stop the monster, even severing his own arm, and ultimately kills the dragon by ripping its heart out. The dragons fall mortally wounds Beowulf, but he lives long enough to watch the carcass of the dragon transform into the humanoid body of his son before it is washed out to sea. Beowulf insists on speaking the truth about his affair with Grendels mother but Wiglaf insists on keeping Beowulfs legacy intact. As the new king, Wiglaf gives Beowulf a Norse funeral and watches as Grendels mother appears and gives Beowulf a final kiss before his ship sinks into the sea. Wiglaf sees the golden horn in the sand while Grendels mother floats in the sea, looking at him seductively. The movie ends ambiguously with Wiglaf holding the horn and staring back at her.

==Cast==
The cast members of Beowulf were filmed on a motion capture stage. They were altered on screen using computer-generated imagery, but their animated counterparts bear much resemblance to themselves.
 title character ITV serial Henry VIII.  On the topic of the original poem, Winstone commented during an interview, "I had the beauty of not reading the book, which I understand portrays Beowulf as a very one-dimensional kind of character; a hero and a warrior and that was it. I didnt have any of that baggage to bring with me."    Winstone enjoyed working with motion capture, stating that "You were allowed to go, like theater, where you carry a scene on and you become engrossed within the scene. I loved the speed of it. There was no time to sit around. You actually cracked on with a scene and your energy levels were kept up. There was no time to actually sit around and lose your concentration. So, for me, I actually really, really enjoyed this experience." Unlike some of his castmates, Winstones animated counterpart bears little resemblance to the actor who was in his early 50s when he filmed the role; Winstone noted that his computer-generated counterpart resembled himself at the age of eighteen, although the filmmakers did not have a photo for reference.    Winstone also played a dwarf performer, and the "Golden Man"/Dragon. 
* The antagonists Grendel and Grendels mother are portrayed by Crispin Glover and Angelina Jolie, respectively. Glover had previously worked with Zemeckis in Back to the Future, when he portrayed George McFly. Zemeckis had found Glover tiresome on set, because of his lack of understanding of shooting a film, but realized this would not be a problem as on a motion capture film he could choose his angles later.  Glovers dialogue was entirely in Old English.  Jolie had wanted to work with Zemeckis, and had read the poem years before but could not remember it well until she read the script and was able to recall basic themes. The actress recounted her first impression of her characters appearance by saying "...I was told I was going to be a lizard. Then I was brought into a room with Bob, and a bunch of pictures and examples, and he showed me this picture of a woman half painted gold, and then a lizard. And, I’ve got kids and I thought Thats great. Thats so bizarre. Im going to be this crazy reptilian person and creature." Jolie filmed her role over two days when she was three months pregnant. She was startled by the characters nude human form, stating that for an animated film "I was really surprised that I felt that exposed." 
*  , I’m not sure, but I couldnt hack it, and I tend to like to just go with the script if its a good script." 
* Unferth is portrayed by John Malkovich. Malkovich became involved in the project because one of his friends, who had worked with Zemeckis, "spoke very highly of him. I had always found him a very interesting and innovative filmmaker. I liked the script very much and I liked the group involved and the process interested me a great deal also." He found the experience of working with motion capture to be similar to his experiences working in the theater. He also found the process intriguing: "Say you do a normal day of filmmaking. Sometimes that’s 1/8 of a page, sometimes it’s 3/8th of a page, normally let’s say it’s 2½ pages, maybe 3. Now it’s probably a little more than it used to be but not always. So you may be acting for a total of 20 minutes a day. In this, you act the entire day all the time except for the tiny amount of time it takes them to sort of coordinate the computer information, let’s say, and make sure that the computers are reading the data and that you’re transmitting the data. It interests me on that level because I’m a professional actor so I’d just as soon act as sit around." Malkovich also recalled that he studied the original poem in high school, and that “I think we got smacked if we couldn’t recite a certain number of stanzas. It was in the Old English class and I think my rendition was exemplary." 

The cast also includes:

* Brendan Gleeson as Wiglaf, Beowulfs lieutenant Robin Wright Queen Wealtheow
* Alison Lohman as Ursula, Beowulfs concubine when he is an old king
* Costas Mandylor as Hondshew Sebastian Roche as Wulfgar Greg Ellis Garmund
* Tyler Steelman as Young Cain, Unferths disabled slave
* Dominic Keating as Adult Cain
* Rik Young as Eofor
* Charlotte Salt as Estrith
* Leslie Harter Zemeckis as Yrsa

==Production==

===Development=== The Sandman turnaround after an adaptation digitally enhanced live action, was worth relinquishing the directorial reins.   Zemeckis did not like the poem, but enjoyed reading the screenplay. Because of the expanded budget, Zemeckis told the screenwriters to rewrite their script, because "there is nothing that you could write that would cost me more than a million dollars per minute to film. Go wild!" In particular, the entire fight with the dragon was rewritten from a talky confrontation to a battle spanning the cliffs and the sea. 

===Animation=== Jerome Chen overseeing creative and technical development for the project. Animation supervisor Kenn MacDonald explained that Zemeckis used motion capture because “Even though it feels like live action, there were a lot of shots where Bob cut loose. Amazing shots. Impossible with live action actors. This method of filmmaking gives him freedom and complete control. He doesn’t have to worry about lighting. The actors don’t have to hit marks. They don’t have to know where the camera is. It’s pure performance." A 25&nbsp;×&nbsp;35-foot stage was built, and it used 244 Vicon MX40 cameras. Actors on set wore seventy-eight body markers. The cameras recorded real time footage of the performances, shots which Zemeckis reviewed. The director then used a virtual camera to choose camera angles from the footage which was edited together. Two teams of animators worked on the film, with one group working on replicating the facial performances, the other working on body movement. The animators said they worked very closely on replicating the human characters, but the character of Grendel had to be almost reworked, because he is a monster, not human.   
 golden color scheme, because they are all related. Grendel has patches of gold skin, but because of his torment, he has shed much of his scales and exposed his internal workings. He still had to resemble Crispin Glover though: the animators decided to adapt Glovers own parted hairstyle to Grendel, albeit with bald patches. 

Robert Zemeckis insisted that the character Beowulf resemble depictions of Jesus Christ, believing that a correlation could be made between Christs face and a universally accepted appeal.  Zemeckis used Alan Ritchson for the physical model, facial image and movement for the title character of Beowulf. 

===Visual effects=== The Polar Imageworks generated keyframe animation cached data. In the cases that using cached data was not possible, the scenes were rendered using foreground occlusion, which involves the blurring of different overlays of a single scene in an attempt to generate a single scene film. 
 Ghost Rider Johnny Blaze. 
 processors so graphic artists would be able to work with the scenes when they arrived. 

So much data was produced in the course of the creation of the movie, the studio was forced to upgrade all of its processors to multicore versions, which run quicker and more efficiently. The creation of additional rendering nodes throughout Culver City, California was necessitated by the movies production. 
 Monster House, was Senior Character Animator for the film.

===Music===
 
 Robin Wright Penn and Idina Menzel performed several songs in the soundtracks score.>

==Differences from the poem==
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "It occurred to me that Grendel has always been described as the son of Cain and Abel|Cain, meaning half-man, half-demon, but his mother was always said to be full demon. So whos the father? It must be Hrothgar, and if Grendel is dragging men back to the cave then it must be for the mother, so that she can attempt to sire another of demonkind."
|-
| style="text-align: right;" | — Roger Avary 
|}
One objective of Neil Gaiman and Roger Avary was to offer their own interpretation for motivations behind Grendels behavior and for what happened when Beowulf was in the cave of Grendels mother. They justified these choices by arguing that Beowulf acts as an unreliable narrator in the portion of the poem in which he describes his battle with Grendels mother.  These choices also helped them to better connect the third act to the second of their screenplay, which is divided in the poem by a 50-year gap. 

Some of the changes made by the film as noted by scholars include:

* the portrayal of Beowulf as a flawed man 
* the portrayal of Hrothgar as a womanizing alcoholic
* the portrayal of Unferth as a Christian
* the portrayal of Grendel as a fragile childlike creature rather than savage demon monster
* Beowulfs funeral the 1999 film Beowulf, with the exception that the dragon is entirely absent there) Geatland   

The relationship of the dragon Fafnir to Grendel and his mother, in this version, is not explicitly stated. Fafnir could have been Grendels mothers original mate but since her son by Beowulf is also a dragon Fafnir may have actually been her son. When Hrothgar kills Fafnir he goes on to sire Grendel. Grendel is a hideously deformed creature and this may be either a result of the immoral character of his father, Hrothgar, or simply that Hrothgar is not great enough for her to sire a true and powerful monster. This latter explanation may be more reasonable as after Beowulf slays Grendel he goes on to sire the final offspring of Grendels mother, a half demon who can take the form of a gargantuan golden dragon or of a perfectly formed human male.
 Beowulf and Grendel, where Grendels mother was linked with the ancient Germanic fertility goddess Nerthus. 
 Smoke and Mirrors, the poem Bay Wolf is a retelling of Beowulf in a modern day setting. In this story, Beowulf as the narrator is ambiguous about what happened between Grendels mother and himself.
 John Gardners novel Grendel (novel)|Grendel). The blame for Grendels violence is shifted to the humans, who sinned against him earlier and brought the vengeance upon themselves. The only real monsters, in this tradition, are pride and prejudice. In the film, Grendel is even visually altered after his injury to look like an innocent, albeit scaly, little child. In the original Beowulf, the monsters are outcasts because theyre bad (just as Cain and Abel|Cain, their progenitor, was outcast because he killed his brother), but in the film Beowulf the monsters are bad because theyre outcasts   Contrary to the original Beowulf, the new film wants us to understand and humanize our monsters."   

==Release== Monster House, Chicken Little, and 3-D re-release of The Nightmare Before Christmas, but on a larger scale than previous films. Beowulf would additionally be released in 35mm alongside the 3-D projections. 
 Steven Bing did not finalize a deal and instead arranged with Paramount Pictures for U.S. distribution and Warner Bros. for international distribution.  Beowulf was set to premiere at the 2007 Venice Film Festival, but was not ready in time.  The films world premiere was held in Westwood, Los Angeles, California on November 5, 2007. 

Critics and even some of the actors expressed shock at the British Board of Film Classification rating of the film — 12A — which allowed children under twelve in Britain to see the film if accompanied by their parents. Angelina Jolie called it "remarkable it has the rating it has", and said she would not be taking her own children to see it.  In the United States, the Motion Picture Association of America rated the film as PG-13 ("Parents Strongly Cautioned: Some material may be inappropriate for children under 13").

===Marketing===
To promote the film, a novelization of the film, written by Caitlín R. Kiernan, was published in September 2007.  This was followed by a four-issue comic book adaptation by IDW Publishing released every week in October 2007. 

A   was released on  . 

Several cast members, including director Robert Zemeckis gave interviews for the film podcast Scene Unseen in August 2007. This is noteworthy especially because it marks the only interview given by Zemeckis for the film.

===Home media=== Region 1 on DVD February 26, 2008. A directors cut was also released as both a single-disc DVD and two-disc HD DVD alongside the theatrical cut. The theatrical cut includes A Heros Journey: The Making of Beowulf while the single disc directors cut features four more short features. The HD DVD contains eleven short features and six deleted scenes. 

The directors cut was released on Blu-ray Disc in the United Kingdom on March 17, 2008 and in the United States on July 29, 2008.  The Blu-ray edition includes a "picture-in-picture" option that allows one to view the films actors performing their scenes on the soundstage, before animation was applied (a notable exception to this is Angelina Jolie, whose scenes are depicted using storyboards and rough animation rather than the unaltered footage from the set).

==Reception==

===Box office===
Beowulf ranked #1 in the United States and Canada box office during its opening weekend date of November 18,  grossing $27.5 million in 3,153 theaters. 

As of April 27, 2008, the film has grossed an estimated domestic total of $82,195,215 and a foreign box office total of $113,954,447 for a worldwide gross of $196,149,662. 

===Critical response===
The film was met with positive  reviews. As of November 1, 2014, on the review aggregator Rotten Tomatoes, Beowulf received a rating of 71% certifying it as "Fresh", based upon 195 reviews.  On Metacritic, the film had an average score of 59 out of 100, based on 35 reviews, indicating "mixed or average" reviews. 
 Pulp Fictions Roger Avary, will have you jumping out of your skin and begging for more... Ive never seen a 3-D movie pop with this kind of clarity and oomph. Its outrageously entertaining." 
 creepy dead Polar Express." He also argues that "Zemeckis prioritizes spectacle over human engagement, in his reliance on a medium that allows for enormous range and fluidity in its visual effects yet reduces his characters to 3-D automatons. While the technology has improved since 2004s Polar Express (particularly in the characters more lifelike eyes), the actors still dont seem entirely there." 
 New York Times compared the poem with the film stating that, "If you dont remember this evil babe from the poem, its because shes almost entirely the invention of the screenwriters Roger Avary and Neil Gaiman and the director Robert Zemeckis, who together have plumped her up in words, deeds and curves. These creative interventions arent especially surprising given the source material and the nature of big-studio adaptations. Theres plenty of action in Beowulf, but even its more vigorous bloodletting pales next to its rich language, exotic setting and mythic grandeur." 

==See also==
* List of historical drama films
* Late Antiquity
* Germanic Heroic Age

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
* Nick Haydock, "Making Sacrifices: Beowulf and Film,"   27 (2012).
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 