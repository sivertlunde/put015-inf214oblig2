Peter Andersen (film)
{{Infobox Film
| name           = Peter Andersen
| image          = 
| image size     = 
| caption        = 
| director       = Svend Methling
| producer       = Tage Nielsen
| writer         = Holger Boëtius Axel Østrup
| narrator       = 
| starring       = Carl Alstrup
| music          = 
| cinematography = Karl Andersson	
| editing        = Edith Schlüssel
| distributor    = 
| released       = 8 December 1941
| runtime        = 92 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| preceded by    = 
| followed by    = 
}}

Peter Andersen is a 1941 Danish family film directed by Svend Methling and starring Carl Alstrup.

==Cast==
* Carl Alstrup - Peter Andersen
* Erika Voigt - Fru Ebba Andersen
* Poul Reichhardt - Harald Andersen
* Inger Lassen - Jenny Andersen
* Asta Hansen - Benna
* Gudrun Stephensen - Gertrud
* Edgar Hansen - Artist Ewald Madsen
* Victor Montell - Sagfører Sigurd Jonsen
* Ingrid Matthiessen - Hushjælpen Agda
* Marie Niedermann - Fru Rasmussen
* Aage Fønss - Grosserer Helge Appel
* Bjarne Forchhammer - Læge Jens Broby
* Sigurd Langberg - Direktør Andersen
* Anna Henriques-Nielsen - Rengøringskone
* Aage Redal
* Jens Henriksen
* Harald Holst

==External links==
* 

 
 
 
 
 
 
 