Doctor Fischer of Geneva
 
 
{{Infobox book |  
| name         = Doctor Fischer of Geneva or The bomb party
| orig title   = Doctor Fischer of Geneva or The bomb party
| translator   =
| image        = Doctor Fischer small.JPG
| image_size = 200px
| caption= First edition cover
| author       = Graham Greene
| cover_artist =
| country      = England
| language     = English
| series       =
| genre        = Novel
| publisher    = The Bodley Head
| release_date = 1980
| media_type   = Print (hardback)
| pages        = 140 pp
| isbn         = 0-370-30316-4
| dewey= 823/.912
| congress= PZ3.G8319 Do 1980b
| oclc= 6273193
| preceded_by  = The Human Factor
| followed_by  =
}}

Doctor Fischer of Geneva or The bomb party (1980) is a novel by the English novelist Graham Greene. The eponymous party has been examined as an example of a statistical search problem. 

==Plot summary==
The story is narrated by Alfred Jones, a translator for a large chocolate company in Switzerland. Jones, in his 50s, lost his left hand while working as a fireman during The Blitz. Jones is a widower when he meets the young Anna-Luise Fischer in a local restaurant. Jones is surprised to learn that Anna-Luise is the daughter of Dr. Fischer, who has become rich after inventing a perfumed toothpaste and whose dinner parties are famous (or infamous) around Geneva. After a brief courtship, the two are married.

Anna-Luise is estranged from her father, the Dr. Fischer of the books title. Jones goes to see Dr. Fischer to inform him that he and Anna-Luise are married, but Dr. Fischer is indifferent to the information. Later, however, he invites Jones to one of his dinner parties; Anna-Luise warns Jones not to go, saying that these parties are nothing more than an opportunity for her father to humiliate the rich sycophants (whom she calls “the Toads,” her malapropism for “toadies”) in his coterie. Jones goes anyway when Anna-Luise relents, saying that one dinner party can’t corrupt him.

At the party, Dr. Fischer and his guests explain some of the rules: If a guest follows all the rules, he or she receives a present (or prize) at the end of the meal. The presents are usually tailored to each guest and are worth a substantial amount of money. However, the rules include complete submission to the humiliations of Dr. Fischer, which always include barbed verbal taunts that focus on each guest’s failings or insecurities.

At this particular party, the dinner consists solely of porridge. One guest asks for sugar, but Dr. Fischer only provides salt. Dr. Fischer explains to Jones that the guests must eat the porridge to receive their presents, and that this is all part of his experiment to see how far the rich will go to debase themselves for more riches. The guests all eat the porridge except for Jones, who earns himself the enmity of the Toads by abstaining. Jones doesn’t receive another invitation for some time.

Anna-Luise fills Jones in on the dissolution of her parents’ marriage. Her mother had developed a friendship with an employee of Mr. Kips, one of the Toads, based on their mutual love of Mozart. When Dr. Fischer found out, he paid Kips’ firm fifty thousand francs to fire the man, and then hounded his wife until she "willed herself" to die. Jones and Anna-Luise encounter the man, Steiner, in a local record shop, and Anna-Luises resemblance to her mother (Anna) gives Steiner a heart attack.

Meanwhile, he and Anna-Luise discuss having children, but she says she would prefer to wait until after the skiing season is over because she wouldn’t want to ski while pregnant. The two go on a skiing trip, and while Jones (who doesn’t ski) waits in the lodge, Anna-Luise collides with a tree after swerving to avoid a young boy who had sprained his ankle while skiing a course that was too tough for him. She suffers a severe head injury and bleeds enough to stain the front of her white sweater red. She later dies at the hospital, leaving Jones broken and lonesome. He attempts suicide by drinking whiskey laced with aspirin, but it only leaves him drowsy.

The next day he responds to an invitation to visit Dr. Fischer in his office. Dr. Fischer offers to give Jones the money held in trust for Anna-Luise, but Jones refuses it. Fischer is surprised, and asks Jones to attend his next dinner party with the Toads, which he promises will be the last.

This party – the "Bomb Party" of the novels alternative title – fills the longest chapter of the book. The party is held outside sometime around New Years Day, and the guests are kept warm via enormous bonfires around Dr. Fischers lawn. The meal is exquisite. Following dinner, Dr. Fischer explains the rules for that nights experiment. He has hidden six crackers in a bran tub. Inside five of them are cheques for two million francs apiece, with the name left blank. Inside the sixth is a small bomb. The guests are expected to draw crackers and open them one by one.

One of the Toads, a stooped man named Kips, says that gambling is immoral and refuses to take part, leaving the party instead, and leaving Mr. Jones to consider that it is only Mr. Kips and himself who take the Doctors threat of a bomb in the last cracker seriously; the other Toads seem to be disbelieving, especially Mrs. Montgomery, who passes off Fischers bomb threat as playful, untrue banter. The other Toads begin to take the crackers; a hack actor named Deane, immediately goes into a role from one of his movies as a soldier volunteering for a dangerous mission, rambling dialogue to himself while he stands near the bucket. Two other Toads, the widow Mrs. Montgomery and the accountant Belmont, rush up and draw their crackers, realising that the odds favour the earlier selectors. Both draw crackers with cheques inside. Deane finally snaps out of his delusion long enough to draw a cracker, and when he finds a cheque inside, he passes out from either shock or inebriation.

This leaves just Jones and the retired military officer, the Divisionnaire. The Divisionnaire takes a cracker but won’t open it. Jones, still considering suicide as a way to avoid his lonely future, takes a cracker, opens it, and finds a cheque. The Divisionnaire remains paralysed by fear, so Jones roots around for the last cracker (which would have gone to Kips) and opens it as well, finding the last cheque, meaning that the Divisionnaire must hold the bomb. While Dr. Fischer torments the Divisionnaire for his cowardice, Jones offers to buy the Divisionnaires cracker for two million francs. Over Dr. Fischers objections, Jones takes the fatal cracker and runs off into the snow, where he opens the cracker to find nothing. Steiner suddenly wanders up to Jones, saying he came to confront Dr. Fischer and to spit in his face. Dr. Fischer arrives and after a brief conversation about whether he has achieved his goals with his experiment, says that it is "time to sleep" but heads away from the house. A few moments later, Jones and Steiner hear a crack, and rush off to find Dr. Fischer, who has shot himself with a revolver.

The novel ends with Jones saying that he is no longer considering suicide and has even struck up a small friendship with Steiner where the two meet for coffee and mourn their lost loves. Jones says he rarely sees any of the Toads and avoids Geneva for the most part; he did once see Mrs. Montgomery, who called him “Mr. Smith,” allowing Jones to pretend he didn’t hear her and walk away.

==Characters==

*Alfred Jones: The narrator, a widower in his 50s with a glove over his artificial left hand. He marries Anna-Luise Fischer.

*Anna-Luise Fischer: The daughter of the title’s Dr. Fischer and wife of narrator Alfred Jones. She despises her father for the way he treats people, especially how he treated her late mother.

*Dr. Fischer: A fabulously wealthy man who made his fortune via the invention of perfumed toothpaste. Fischer is a widower who throws dinner parties to humiliate his rich guests.

*Mrs. Montgomery: A wealthy widow who can never remember Jones’ name and keeps calling him “Smith.” She is the only female guest at the dinner parties.

*Deane: A former pin-up actor whose looks are fading. He is a guest at the dinner parties.

*The Divisionnaire: A retired Swiss military officer, sometimes mistakenly called “The General” by his fellow dinner party guests.

*Belmont: A tax accountant and guest at the dinner parties.

*Kips: A secretive man with a severely deformed spine that causes him to stoop so far that he faces the ground. He appears to be involved in arms smuggling.

*Steiner: The former love interest of Mrs. Fischer, now a clerk in a Geneva record store. Steiner was fired by Mr. Kips after Dr. Fischer found out about his friendship with Mrs. Fischer.

==Film, TV or theatrical adaptations==
The novel was made into a TV film, Dr. Fischer of Geneva,  in 1985, starring James Mason (in his last role) as Dr. Fischer, Alan Bates as Alfred Jones, and Greta Scacchi as Anna-Luise. It was directed by Michael Lindsay-Hogg.

==References==
 

 

 
 
 
 
 
 