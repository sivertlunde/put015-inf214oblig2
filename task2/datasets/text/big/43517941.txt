School for Danger
{{Infobox film
| name           = School for Danger
| image          =
| caption        =
| director       = Teddy Baird
| producer       = 
| writer         = 
| starring       = Harry Rée Jacqueline Nearne Teddy Baird
| music          = 
| cinematography = 
| editing        =  RAF Film Unit Central Office of Information
| distributor    = 
| released       = 1947
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British docudrama film directed by  Teddy Baird depicting the training and deployment of agents of the Special Operations Executive during the Second World War. The film stars real-life SOE agents Captain Harry Rée and Jacqueline Nearne.

==Plot==
Felix and Cat are recruited and trained to be secret agents and are sent to occupied France where they organise resistance, carrying out sabotage and helping airmen get back to the UK.

==Main cast==
* Captain Harry Rée - Felix
* Jacqueline Nearne - Cat

==References==
 

==External links==
*  

 
 
 
 

 