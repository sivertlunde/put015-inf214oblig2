Earth II
{{multiple issues|
 
 
 
}} Earth 2.

Earth II was a 1971 pilot, aired November 28 (and released theatrically outside North America), for a television series about a colony established in orbit around the Earth. A WABE Production in association with Metro-Goldwyn-Mayer Television, it starred Gary Lockwood, Anthony Franciosa, Lew Ayres and Mariette Hartley. The film was written and produced by William Read Woodfield and Allan Balter, and directed by Tom Gries.

==Plot== Apollo style Red Chinese" agent is killed in the water nearby before he can sabotage the rocket launch.

The president (Lew Ayres) of the United States announces that the three men and their ship will be the nucleus of a new nation, and asks Americans to turn their lights on that night to show support for the project. The astronauts take photos of the Earths surface as they orbit, to be processed later to determine the level of public support for the idea. The results indicate widespread support for a new nation in outer space.

The movie skips ahead several years to show a shuttle approaching a space station, a huge, rotating city known as Earth II, with technology at its disposal that makes it fairly easy to maneuver around the city and supply it and the thousands - from many nations - now living aboard. There is a family aboard the shuttle — Frank, Lisa and Matt Karger — who are new immigrants to the colony of many national origins.
 UN (of which Earth II is a member nation); at the meeting, the PRC refuses to remove the weapon and threatens to detonate it if it is tampered with.

Earth II has a direct democracy process known as a "D&D" - Discussion and Decision. The newly arrived Frank Karger (Franciosa) initiates a D&D to decide on dealing with the warhead directly. After some of the citizens, including Russian emigre Ilyana Kovalefskii (Inga Swenson) make their statements, the vote is to act.

Two men, including Ilyanas husband Anton (Edward Bell) go out in a tug to deactivate the warhead, but Anton is electrocuted when the Chinese activate the weapon. The weapon does not explode because Kovalefskii had already cut several wires in the arming device. The tug operator rescues the technician, discards his tool caddies, secures the bomb and brings both back to Earth II. Ilyana is told that Anton will be disabled for life as a result of his injuries unless surgery is attempted. Ilyana approves the surgery.

Meanwhile, Lisa Karger (Hartley) doesnt want the bomb aboard, and is alarmed at Franks intent to initiate a D&D on Earth II becoming a nuclear power. When someone tells her one way to dispose of the bomb is to fire it at the sun, she blows the hatch cover on the holding bay, waits for the sun to come into the view, and launches the bomb at it. The Earths gravitational pull is greater however, and the bomb falls toward the Earth and will detonate over the Great Lakes region.

Ilyana has observed the operation on her husband - in an operating room with medical personnel standing on both the floor and ceiling - but after it is completed, Anton abruptly dies.

The tug operator retrieves the warhead, but not before the warheads casing disintegrates in the Earths atmosphere. The bomb is brought back and put into the same holding bay, and station rotation is slowed as much as possible while a crew works to permanently deactivate the bomb. The city is still rotating enough to bring the sun into view through the opening and the temperature is rising, threatening to melt safeties and detonate the bomb.  A pilot takes a tug out and puts its nose against one of the main struts of the city, then fires its engines to stop rotation altogether.

The suns light is partly shining into the holding bay, the temperature at a crucial level.  The technicians find melted metal that makes it necessary to drill in order to remove some components.  The disarming is completed, the rotation of the city is restarted, Karger reverses his decision regarding a D&D on Earth IIs nuclear weapons status, and the bomb is launched toward the sun for disposal.

==Notes==
We see in the movie that even toy guns are prohibited on the station, and that the communications port in an apartment is used for both the television set and the telephone - Frank has to mute the TV to answer the phone right next to the TV set. The operating room in the hospital has support staff standing on the ceiling to aid the doctors.

Very shortly after this American-produced movie was produced, and 34 days before it was broadcast, the United Nations, with a membership in which the majority of nations now recognized Communist China, voted October 25 to expel the representatives of the Taiwan-based Republic of China (an original member since 1945) and admit the Beijing-based representatives of the Peoples Republic of China (founded 1949) in its place.  Thus, the movie was outdated when it was broadcast.  Eight years later, the United States itself shifted recognition to Communist China while maintaining unofficial relations with Nationalist China.

In a 1998 interview with Sci-Fi Channel Magazine promoting the 30th Anniversary of  , Gary Lockwood stated that he hated working on the Earth II production, due to its complexity. 

The map used in the control center is a Dymaxion Map, designed by R. Buckminster Fuller, who was also listed in the credits as "Technical Advisor for Earth."

In 2010 Film Score Monthly released Lalo Schifrins score as part of their five-disc collection of MGM television music, TV Omnibus: Volume One (1962-1976).

==See also==

* List of films featuring space stations

==External links==
 

 
 
 

 