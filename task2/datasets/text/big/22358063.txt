Elise, or Real Life
{{Infobox Film
| name           = Elise, or Real Life
| image          = 
| image size     = 
| caption        = 
| director       = Michel Drach
| producer       = 
| writer         = Michel Drach Claire Etcherelli Claude Lanzmann
| narrator       = 
| starring       = Marie-José Nat
| music          = 
| cinematography = Claude Zidi
| editing        = Carlos de los Llanos
| distributor    = 
| released       = 25 November 1970
| runtime        = 104 minutes
| country        = France
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}}

Elise, or Real Life ( ) is a 1970 French drama film directed by Michel Drach. It was entered into the 1970 Cannes Film Festival.   

==Cast==
* Marie-José Nat - Elise Le Tellier
* Mohamed Chouikh - Arezki
* Bernadette Lafont - Anna
* Jean-Pierre Bisson - Lucien Le Tellier
* Catherine Allégret - Didi
* Mustapha Chadly - Mustapha
* Alice Reichen - La grand-mère
* Martine Chevallier - Marie-Louise, la belle-soeur / Sister-in-law (as Martine Chevalier)
* Jean-Pierre Darras - Le commissaire
* Yves Barsacq - Un policier

==References==
 

==External links==
* 

 
 
 
 
 
 