Elite Squad: The Enemy Within
{{Infobox film
| name = Elite Squad: The Enemy Within
| image = The Elite Squad 2.jpg
| director = José Padilha
| producer = Marcos Prado
| writer = José Padilha Bráulio Mantovani
| starring = Wagner Moura Irandhir Santos André Ramiro Milhem Cortaz André Mattos Maria Ribeiro
| music = Pedro Bromfman
| cinematography = Lula Carvalho
| editing = Daniel Rezende
| studio = Zazen Produções
| distributor = Zazen Produções   Variance Films  
| released =  
| country = Brazil
| language = Portuguese
| runtime = 116 minutes
| budget =   9,509,65) 
| gross  = $63,027,681   
}} BOPE ( Rio de Military Police, analogous of the American SWAT, with a focus on the relationship between law enforcement and politics. The film was released in Brazil on October 8, 2010.   
 Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.   

Elite Squad: The Enemy Within was released in the United States by Variance Films on November 11, 2011.

==Plot==
 
The film starts in medias res, 13 years after Elite Squad|Baianos death. Roberto Nascimento (Wagner Moura) is shown leaving a hospital, and a man reports this on his radio chatter. Soon after, his car is blocked and gunned down by unknown shooters.
 Bangu Penitentiary Complex to put down a prison riot. After killing his rivals and taking a number of corrupt prison guards hostage (as a part of a deal), gang boss Beirada (Seu Jorge) demands that history teacher, human rights activist and strong opposer of Nascimentos methods Diogo Fraga ( ) is brought in to negotiate. After Fraga convinces Beirada to release the hostages, Captain André Matias (André Ramiro) storms in and execute Beirada, though Nascimento ordered him to wait. Afterwards, Fraga, who is also the new husband of Nascimentos ex-wife Rosana (Maria Ribeiro), expresses his outrage over the situation to the media, which sparks global media attention since he was wearing a "Human Rights" shirt, sparkled with blood.

When Nascimento hears that the Military Police commander Formoso (Rodrigo Candelot) will fire him over the controversy, he goes to personally confront him at a restaurant, only to be greeted by cheering diners who approved the execution. Guaracy (Adriano Garib), the state secretary of Public Safety, uses the situation and promotes Nascimento to Undersecretary of Public Safety for Intelligence. Matias, however, is dismissed from BOPE and demoted to the ordinary police. Nascimento promises Matias to settle his situation and goes to his supervisor about this, but Matias goes to polemic tabloid journalist Clara Vidal (Tainá Müller) and gives her an interview about disregard of BOPE and the corruption of the governor, only to be sent to prison for his complaints. Nascimento confronts Matias about this, and Matias calls him a traitor for taking his position in politics.
 State Assembly and, much to Nascimentos disappointment, his criticism keeps Nascimento away from his son Rafael, who has become scared of his fathers line of work, and his ex wife also wants him to meet with Rafael to explain to him who he is and what is his job.

Nascimento uses his position to turn BOPE into a powerful military police, increasing it to 360 officers, and also gets armored vehicles and s helicopter, personally monitoring the raids from it while BOPE is wiping out the drug dealers (and therefore the corrupt officers bribes) from the favelas in complex planned operations. However, a group of corrupt cops, led by then Major Rocha (Sandro Rocha), learn that the favela dwellers also make money by other means including illegal TV and internet connections. Rochas group form a militia and monopolize the favelas black market, setting up an extremely profitable protection and extortion racket that also has the favor of local politicians wanting to keep drug dealers out of the favelas.
 Tanque (the last drug dealing reduct in the west side of Rio) and steal a number of guns. It is part of a plan of the corrupt officers and politicians to create an excuse to raid the slum and get rid of the local dealers. Nascimento assures the Secretary that there is no evidence that the dealers have taken the weapons after listening to 300 hours worth of phone calls. Corrupt Lieutenant Colonel Fabio Barbosa (Milhem Cortaz), however, states that a "reliable source" blamed the dealers, and the raid is authorized by the governor.

Matias, who was brought back into BOPE by Rocha, knows that the dealers always flee the slum running past the police station, whose corrupt officers will do nothing. Therefore, he and his BOPE men replace the corrupt officers to catch the fleeing dealers by surprise. The next day, BOPE team arrives and attacks the dealers, who are shot dead when they pass by the station. Local drug lord Pepa tries to escape, but is ultimately cornered by Matias, who thinks hes got the guns and tortures him into talking. Rocha, along with Fabio and other corrupt officers, arrives and kills Pepa, enraging Matias, who has his BOPE soldiers carry away the dealers body, and then confronts Rocha about the situation, only to be shot dead in the back by one of Rochas men, under Fabios protests.

Promising himself to find out the truth about Andres death, Nascimento uses his power to tap Fragas telephone, knowing that he has been investigating the militias as well after hearing it from Rafael, who is an intern in Fragas office. Meanwhile, Clara tries to make a story, and goes into one of Rochas favelas with a photographer to collect information about corrupt officers. However, just as Clara tells Fraga by telephone what she just discovered, they are caught by Rocha and his group, and Fraga hears Rochas voice before Rocha has Clara and the photographer murdered, who will now have to put Fraga down since he is now a witness.

Due to the tap, Nascimento also hears her final phone talk and deduces Fraga is the next target. Since Fraga is with Rosane and Rafael, he records the data, takes it with him and goes after his family. Just after he leaves, Formoso finds out about his wiring and warns the corrupt officers about this. Nascimento heads for Fragas apartment while trying to phone Rosane, but she deliberately ignores the calls due to him not giving attention to their son. Fraga, Rafael and Rosane arrive, and two men in a motorcycle try to execute Fraga, but end up shooting Rafael in the chest after Nascimento guns them.

After Rafael is taken to the hospital, Nascimento leaves and brutally beats up the secretary for hurting his family. The next day, the President of the Legislative Assembly decides to open an investigation over Claras possible murderer (due to the lack of bodies, the police is treating the case as kidnapping), and Nascimento is expelled from the Secretary and the military police for illegally tapping Fragas phone in the effort to have Nascimento humiliated. However, the secretary keeps him alive, because if he is murdered before the court, he will become a matyr for human rights, causing a national scandal and Fraga would accuse the secretary for the murder, causing his party to lose the elections, but Rocha, knowing that, if Nascimento testifies, all the incoming trouble will be blamed on the militia and therefore Rocha, so he decides to go with the murder.

That night, Nascimento arrives at the hospital, where Rafael is slowly recovering after his surgery. He tells Rosane to call him anytime she needs him, and then leaves the hospital, showing the opening scene again. As he leaves, a man on his voice chatter informs Rocha about his departure, while another car follows Nascimento. Rocha arrives with his convoy and starts shooting at Nascimento, but the other cars occupants exit and start shooting at Rochas men, revealing themselves to be BOPE officers, as Nascimento predicted the attempt on his life. Overwhelmed by BOPEs officers, Rocha escapes.
 Federal Chamber of Deputies, as well as Fraga, as the two start a rivalry. Nascimento ends his narration with a reflection over the influence of politicians on the social issues of Brazil, while images of Brasília are shown. In the end, he arrives at the hospital, where his son finally wakes up.

==Cast== PMERJ and Undersecretary of Rio de Janeiro Public Safety for Intelligence. Former BOPE officer for the last 20 years, he has been promoted to Undersecretary simply to satisfy the public. His tactics to destroy the drug dealers were efficient, but caused the current leading party to take over Rio de Janeiro.
* Irandhir Santos as Diogo Fraga, a History teacher who becomes a State Representative after the Bangu I controversy. Rosanes new husband, he is a left-wing politician who tries to enrol himself in the Federal Chamber of Deputies.
* André Ramiro as Captain André Matias, a BOPE commander.
* Maria Ribeiro as Rosane, ex-wife of Nascimento and now married to Fraga.
*   as Captain/Major Rocha, the main antagonist of the film. Rocha had a small role in the first film, being one of the seargants under Fabios control.
* Milhem Cortaz portrays Lieutenant Colonel/Colonel Fabio Barbosa. Still very corrupt, as a competitor and also as an accomplice of Major Rocha. Still friends with André Mathias, who saved his life thirteen years ago (the first film in the series).
* Tainá Müller plays Clara, a journalist. In the film, Mathias reads her interview on the scrapping of the BOPE, after being expelled from the same battalion because of the events in the rebellion in Bangu I and then assists the Deputy Diogo Fraga in the investigation of the militias.
* Seu Jorge as Beirada, a criminal responsible for the Bangu I uprising.
* André Mattos plays Fortunato, State Representative and host of a tabloid TV show.
* Emilio Neto Orciollo Valmir plays a cop who works in the administrative area.

== Production ==

===Casting and characters=== City of God) was invited by director José Padilha to act as one of the antagonists, Beirada; Maria Ribeiro is also back as Roseane, who is no longer married to Nascimento, but to a left wing National Congress of Brazil|Congressman. Tainá Müller, who was not in the first film, plays Clara, representing the media. Sandro Rocha, who had little on-screen time during the first film, has a bigger role now, playing Major Rocha, known as Russo (Russian), boss of the militia.

=== Cast preparation === boot camp in Rio, coordinated by Paulo Storani, security adviser, and with the participation of members from BOPE and CATI. André Ramiro said, "They treated us like real cops. No Im-too-important. I also had to say No, sir. Yes, sir". Wagner Moura had Brazilian Jiu-Jitsu|Jiu-Jitsu lessons from fighter Rickson Gracie. The training helped bring a degree of reality to the film; actors had to learn the proper techniques to handle weapons and also action strategies in risk-zones, besides a strong fitness program, laid heavy on those who werent in the first movie.

===Filming and post-production=== Bangu 1 House of Congress in Brasília, but production couldnt get permission. A fake Ethics Committee was built inside the Federation of Industries of Rio de Janeiro, recording took place at April 15. Filming was done in the same month and the film went to post-production.

== Release ==

=== Strategy against leaks ===
After the first film leaked and got in the hands of millions of people before the official release date,  the Elite Squad 2 crew created a strategy to avoid the same thing happening to the sequel. Before production began, Marcos Prado informed that, "This time we will have a special security scheme. We will have a security team watching over the editing, transporting the reels, and paying close attention to every little detail. The raw material will be preserved". Director José Padilha concluded, "We wont outsource any of the steps. We are doing the whole post-production at our company, inside our caveirão". The script was sent to the National Agency of Cinema under the title "Organized crime" and was printed with red ink, to avoid photocopying. The production unit rented an apartment, monitored by cameras, where editing took place. Four people had access to the place, only through passwords, and no means of accessing the Internet. Another strategy used was the marking of each copy sent to the theaters; that way if any of the copies was illegally recorded, they would know where it came from. Military policemen from São Paulo also helped in avoiding the leak. The final picture, still with no sound, was put in a safe.

=== Reception ===
Elite Squad: The Enemy Within was critically acclaimed. Based on 42 reviews, the film received a 93% approval rating on Rotten Tomatoes.  At Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 71 based on 18 reviews, indicating "generally favorable reviews".

=== Domestic release ===
Elite Squad: The Enemy Within broke national opening weekend records in Brazil, with more than 1.25 million spectators during its first weekend.  This opening was the 5th biggest in Brazilian history and the biggest one for a Brazilian film, so far. A total of 696 theaters are screening the film about 8 times each day. In three weeks, the film surpassed the mark of BRL|R$ 60 million in gross revenue and over six million viewers  becoming the most successful Brazilian film of the 21st century so far on local theaters. In just nine weeks, the film surpassed 10 million viewers on theaters throughout the country becoming the highest box office of a local film in Brazilian history.

=== United Kingdom release ===
Elite Squad: The Enemy Within was released in the UK by Revolver Entertainment     on August 12, 2011.

=== United States release ===
The film held its United States premiere in January 2011 at the Sundance Film Festival. Since, it has screened at several festivals, including Austins Fantastic Fest. The film was released in New York City on November 11, 2011, and in Los Angeles on November 18, 2011.

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Brazilian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*    
*    
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 