So Bright Is the View
{{Infobox film
| name           = So Bright Is the View
| image          = File:Romanian_Poster_for_So_Bright_Is_the_View.jpg
| caption        = 
| director       = Joël Florescu, Michaël Florescu
| producer       = Ronella Sezonov
| writer         = Joël Florescu, Michaël Florescu
| starring       = Bianca Valea,  
| cinematography = Ronella Sezonov
| editing        = Alice Gheorghiu
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Romania
| language       = Romanian
}}

So Bright Is the View ( .      

==Plot==

Present day Bucharest. Estera is pursuing a job opportunity in Atlanta. She puts her hope in a "friend-interview" with Mike, a Romanian-American entrepreneur who reveals himself to be a domineering wreck with issues of his own.

==Cast==

* Bianca Valea as Estera
*   as Mike
* Robi Urs as Vlad
* Valentina Popa as Raluca
* Dana Apostol as Rivka
* Cristina Olteanu as Oana
* Dana Cucos as Luminita

==Filmmakers==

Joel Florescu and Michael Florescu, the writers and directors, are twin brothers of Franco-Romanian origin who began their cinematographic projects in Bucharest as part of the Romanian Independent Film Collective. 

==Reception==
 David Walsh  has dubbed So Bright Is the View "a serious film" and noted the films "sociological significance", indicating the emergence of a new generation of filmmakers in Romania that critically addresses contemporary socioeconomic realities. Walsh praised the Florescu brothers "obvious talent" and "ability to present the drama of everyday life", while critiquing the films aesthetic "formalism". 

==Distribution==

The film was acquired for distribution in North America by IndiePix Films. 

==References==
 

==External links==
*  

 
 
 