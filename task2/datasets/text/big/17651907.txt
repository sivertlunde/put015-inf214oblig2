Tekken (2009 film)
  }}
{{Infobox film
| name           = Tekken
| image          = Tekkenmovie.jpg
| caption        = Japanese film poster
| director       = Dwight H. Little
| producer       = Steven Paul Benedict Carver Iddo Lampton Enochs
| screenplay     = Michael Colleary Alan B. McElroy Mike Werb
| based on       = Tekken by Namco  Kelly Overton Cary-Hiroyuki Tagawa Ian Anthony Dale Tamlyn Tomita Candice Hillebrand Luke Goss Gary Daniels
| narrator       = Jon Foo Kelly Overton
| music          = Homario Suby
| cinematography = Brian J. Reynolds
| editing        = David Checel
| studio         = Crystal Sky Pictures
| distributor    = Anchor Bay (DVD)  Warner Bros. Pictures (Japan)
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $30 million 
| gross          = $967,369    
}} series of Jun Kazama (Tamlyn Tomita), by confronting his father, Kazuya Mishima (Ian Anthony Dale) and his grandfather, Heihachi Mishima (Cary-Hiroyuki Tagawa), the latter of who he thought was responsible for her death.

On November 5, 2009 Tekken was shown at American Film Market. On January 14, 2010, an international trailer was released,  and the film premiered in Japan on March 20, 2010.

Tekken is followed by the 2014 prequel  .

==Plot==
 

===Setting===

In the late 2010s, after the Terror War has destroyed much of civilization, 8 megacorporations survived and divided up the world around them; the biggest being Tekken Corporation, which controls North America. In order to placate the masses, the corporations Chairman, Heihachi Mishima, sponsors the King of Iron Fist Tournament, or Iron Fist - in which fighters from the 8 corporations battle until one is left standing and receives a lifetime of stardom and wealth. In contrast to the rich and lust Tekken City, there is the slum area surrounding it, referred to as the Anvil.

===Story===

Jin Kazama has been raised by his mother, Jun. She has trained him in martial arts and has been a mentor, yet she never speaks of Jins father, claiming he is dead. In 2039, Jin is now a rebellious 19-year old teenage fighter and contraband runner who lives in the Anvil, and usually goes into fights and cooperates with the resistance groups to earn money to provide food (fruit, coffee and chocolate have become expensive and rare commodities in the Anvil). One night, Jin is targeted by the Jackhammers, the elite specs group that patrols the Anvil and ensures the safety of Tekken City, for cooperating with the resistance groups. He tries to return to his home, only to find his mother, Jun, killed by the Jackhammers bombarding the house.
 Marshall Law, Steve Fox, sponsor of the Open Call and former Iron Fist fighter, and is hailed "The Peoples Choice", who takes him to Tekken City.
 Raven overpower Capoeira fighter Eddy Gordo in the first match, Jin wins the match against Miguel Caballero Rojo, nearly killing him in a fit of rage. Heihachis son, Kazuya Mishima, is impressed and offers Jin a place in Tekken Corp., but Jin refuses. Later that night, after sneaking out with Christie and visiting a nightclub, Jin is attacked by masked assassins, on the orders of Kazuya, who is scheming to take over Tekken and sees Jin as a possible obstacle since he is hailing as Peoples Choice. Jin survives the assassination attempt, thanks to Christies interference. Steve and Christie attempt to discourage Jin from continuing in the tournament, but Jin vows to win Iron Fist and kill Heihachi, and he gives Fox Juns ID, he realizes he is her son, telling him that he knew her. Meanwhile, Kazuya blackmails the tournaments champion, Bryan Fury, into killing Jin in a match or be exposed as a partial cyborg, banning him from the tournament for life (cyborgs are banned from fighting).

During the quarter-finals, Christie is paired with Nina Williams. But before the fight, Jin notices Ninas face, she was one of the assassins. After Christie defeats Nina, Jin is matched up with an elite swordsman named Yoshimitsu. Heihachi, liking this young fighter, deems that this match be reserved for the semi-finals and attempts to change the order, but Kazuya, who controls the Jackhammers, stops him. Kazuya then has Heihachi imprisoned and orders the match to begin, effectively seizing control of Tekken. Jin narrowly kills Yoshimitsu, thanks to Heihachi tripping a security alert in the arena. 

Following the match, Kazuya orders all of the fighters to be detained. He tells them that the rules have changed, and so they must now fight to the death. Jin, Christie, and Steve try to escape, along with Raven, leaving Nina & Anna Williams and Sergei Dragunov behind, since they were in a separate cell. Kazuya notices them escaping and brings down some guards, causing a firefight. Steve, Christie, and Raven cover Jin but he walks across Heihachis cell. Angry, he tries to taunt him, saying he is responsible for killing his mother. However, because Heihachi is their only chance of escaping Tekken, Steve frees Heihachi and joins the group. On the gunfight, Raven is wounded and recaptured, while the others make it out to the Anvil.

In the warehouse that Jin uses as protection, Heihachi reveals to Jin the true nature of his origin. 20 years ago, Jun was fighting in the first Iron Fist tournament placed by the Tekken Corporation after the war, and she impressed Kazuya, who raped her, making him Jins father, and left her for dead. She survived the assault and Heihachi took her out of Tekken City to the Anvil to keep her alive. Heihachi also tells Jin that since he is Heihachis grandson, he could become the next Chairman. He also states the corporations true purpose is to restore order to the world, though Jin cant believe what he is being told, believing that the corporation is spreading terror and fear to its residents. Heihachi entrusts Jin with the task of defeating Kazuya. Later on, the group is located by Jackhammers, who kill Steve Fox in a firefight and recapture the rest of the escapees. Before taking them back to Iron Fist, Kazuya orders the Jackhammers to execute Heihachi.

Back in Tekken City, Raven comforts a dispirited Jin, saying that he saw what Jin did to Miguel Rojo and reminding him that anger doesnt fuel the soul, but incinerates it. Raven tells Jin that because there are many people depending on him, he can become champion without letting anger take control of his body. In the Finals, Jin is forced to fight against Bryan Fury, who had already killed Sergei Dragunov in a death match, while Kazuya holds Christie in the control room. At first he is outmatched, but remembering his mothers teachings, Jin kills Bryan. 

Angered about Jins victory, Kazuya enters the tournament himself, armed with half moon axes, and begins the final match. The weaponless Jin is battered and is about to lose. He is saved, though, when Christie escapes by shooting the Jackhammers guarding her, creating a distraction. This allows Jin to wound and pin Kazuya, who baits Jin by claiming that he remembers how Jun "put up quite a fight." Kazuya taunts him into inheriting the Mishima Curse (Heihachi imprisoned and killed his father and Kazuya presumably murdered Heihachi), but Jin refuses to kill his father, stating that it is not his curse.

Christie comes to the stage and declares Jin the new Champion of Tekken. Elated, the crowd both in and outside the arena cheer for him. When Christie asks where he will go, he replies that he will go home to the Anvil, since he doesnt want to run the corporation. He walks out of Tekken Citys gate and is saluted by the Jackhammers - symbolizing his new role as CEO of Tekken Corp. In a voiceover, Christie explains that Jins victory made the Kazama family name synonymous with hope in the Anvil, but that the true legacy of Tekken is only beginning.

After the credits, the scene shifts to Kazuya, who walks out of the arena and realizes that he lost control of the Jackhammers, who refuse to salute him. Back at Heihachis execution, he kneels in front of a Jackhammer, who is holding him at gunpoint. His final words are: "I am Heihachi Mishima. I...am...Tekken. You will obey."  The Jackhammer lowers down his gun and obeys his command.

==Cast==
 
*Jon Foo as Jin Kazama
**Jason Del Rosario as Young Jin Kazama
**Dallas James Liu as Jin Kazama, age 6 Kelly Overton as Christie Monteiro
*Cary-Hiroyuki Tagawa as Heihachi Mishima
*Ian Anthony Dale as Kazuya Mishima Marshall Law Raven
*Luke Steve Fox
*Tamlyn Tomita as Jun Kazama
*Candice Hillebrand as Nina Williams Anna Williams
*Gary Daniels as Bryan Fury
*Gary Stearns as Yoshimitsu Miguel Rojo
*Lateef Crowder as Eddy Gordo
*Anton Kasabov as Sergei Dragunov
*Mircea Monroe as Kara John Pyper Ferguson as Bonner
*Kiko Ellsworth as Denslow Michael Showers as Police Officer #1
*Louis Herthum as Police Officer #2
*Jourdan Lee Khoo as Sportscaster #1
*Andrew Wei Lin as Sportscaster #2
*Jonathan Kowalsky as Vosk
*Sharron Leigh as Night Club Waitress
*Monica Mal as Night Club Hot Girl
*Alan McElroy as Arena Attendant
*Jevon Miller as Open Call Reject
*Randal Reeder as Open Call Scared Man
*Jason Richter as Bonners Associate
*Blake Shields as Hansu
*Brett Wagner as Quid
*Cassue S. Watson as Med-Tech

==Release==
The film was screened at the Manns Criterion Theatre in Santa Monica on November 5, 2009, as part of the AFM Film Festival to find a solid distributor.  It was released in Japan on March 20, 2010 through Warner Bros. Pictures (Japan).  The film also premiered on July 27, 2010 in Singapore and August 4, 2010 in the Philippines (via Pioneer Films).    One week before the Philippine premiere, Jon Foo visited Manila to promote the film.   Due to its poor reception, the film never saw a wide theatrical release in the United States, and was released direct-to-video instead.

The film was released on DVD and Blu-ray Disc in Japan on August 11, 2010. In the United Kingdom, Optimum Released and distributed the film on May 2, 2011.  Anchor Bay Entertainment released the film in the United States on DVD and Blu-ray Disc on July 19, 2011. 

==Reception==
Katsuhiro Harada, director of the Tekken video game series, criticized the film: "That Hollywood movie is terrible. We were not able to supervise that movie; it was a cruel contract. Im not interested in that movie."    Reacting to Haradas comments, Nick Chester of Destructoid said the film is "not great, but terrible is a stretch," saying that it "does a decent job of trying to stay true to the look and feel of the  " and that "the fight scenes werent bad." 

Brian Orndorf of DVD Talk gave the film two stars out of five, writing: "Tekken is a failure on many levels, but it does make a plucky attempt to replicate the flippy-floppy nature of the fighting elements, creating a limb-snapping effort of escapism surrounded by bland writing and sleepy performances." He opined that director Dwight H. Little "show  off an impressive spectrum of fighting styles and intensity, though he goes a little crazy with trendy cinematographic choices and hyperactive editing." 
 Mortal Kombat. Had it embraced its roots more openly, the film may well have offered more excitement. As it is, Tekken is just an average action flick, with nothing to distinguish it from the rest of the crowd." 

==Prequel==
 
 Wych Kaos and released on DVD on August 12, 2014. 

==References==
 

==External links==
 
*    
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 