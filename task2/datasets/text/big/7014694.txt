Ernest Goes to Africa
{{Infobox film
| name = Ernest Goes to Africa
| image = Ernest goes to africa.jpg
| image size = 190px
| caption = DVD Cover
| director = John R. Cherry III
| writer = John R. Cherry III
| starring = Jim Varney Linda Kash
| producer = Kenneth M. Badish John R. Cherry III Emshell Producers
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget =
}}
Ernest Goes to Africa is a 1997 comedy film written and directed by John R. Cherry III. It stars Jim Varney, and is the ninth film to feature the character of Ernest P. Worrell. In this film, Deacon County, Ohio resident Ernest unknowingly comes into the possession of some stolen jewels and is kidnapped and brought to Africa where he must rescue the woman he loves. The film was shot entirely in Johannesburg, South Africa.

==Plot synopsis==
 
Ernest P. Worrell (Varney) has been fired from his job due to accidentally crushing a ladys car. He goes to a local restaurant and asks his crush, Rene Loomis (Kash) to go on a date with him. He is turned down by her because she wants to date somebody more adventurous. Ernest decided to buy her a gift to show that he really cares for her. He goes to a flea market where he buys 2 jewels unaware that they are the "Eyes of Igoli" stolen from the Sinkatutu tribe in Africa by a runaway man named Mr. Rabhas who is being chased by 2 henchmen of Prince Kazim. He is cornered by the men but rescued by a man named Thompson (Bartlett) and his strong African bodyguard, Bazu. 

Threatening to kill him if he doesnt tell so he can steal them himself, Rabhas reveals where he stashed the Eyes of Igoli. Thompson walks away and Bazu takes a bag of deadly snakes and dumps it on Rabhas, leaving him to die. Meanwhile, Ernest creates a yo-yo made of the Eyes of Igoli. He does his around-the-world and crashes his fishs tank. He puts him in the sink but he flows down the drain. Meanwhile, Thompson eventually finds out that Ernest took the Eyes of Igoli. He spies on him at the restaurant Rene works in. Ernest gives Rene the yo-yo only to be called a small-town ordinary schmoe by her. 

Meanwhile, the bad guys kidnap Rene and Ernest come to save her after a phone call. Thompson kidnaps him too when he gets there and puts them on a flight to Africa. After shutting Rene up in a room with Bazu, an old woman named Auntie Nelda comes in and explains to Bazu about how her husband died. She then throws ashes in his face and rescues Rene, knowing that it is Ernest. They escape in a golf cart and encounter many obstacles from getting simple firewood to Ernest disguising as a girl and getting kissed by the prince to striking down the bad guys with ostrich eggs. Meanwhile, Thompson and Bazu look for Ernest and Rene. They walk down the river and encounter the Sinkatutu tribe who wants to eat them for lunch. 

Ernest empties his pockets when they tell him to and yo-yos the yo-yo one last time impressing the tribe, especially the Sinkatutu chief. He does tricks which easily turns the tribe to like them. Just as soon as the Chief is about to give him a "booster surgery", Thompson comes along by himself. He had kicked Bazu out. He suddenly blames Ernest of stealing the eyes. Thompson requests a battle of truth. Ernest has to fight Thompson in order to save Rene from becoming cooked...literally! When Ernest hears the challenge he states "On second thought, I think I might have the booster." Thompson changes into a black warrior suit and pulls out his weapons. Ernest does the same, only his are little items. Yet, he successfully fights Thompson using them. All of a sudden, Thompson punches Ernest and knocks him out. But Ernest awakens and hears Rene calling him to use his yo-yo. 

Ernest puts his fighting skills and yo-yo skills together and he does an around the world which knocks Thompson out cold and breaks the Yo-yo to reveal the Eyes of Igoli. The tribe rushes toward them as Rene compliments Ernest on how hes her "Knight in Shining Armor". A few weeks later, Ernest and Rene are about to go on a date. Ernest even paints an Ostrich egg and gives it to her as a gift. Sadly, she tells Ernest that the date is off because hes too adventurous for her. Ernest tells Rene he recalls her calling him an ordinary Schmoe. Rene tells him not to let anyone call him an "ordinary Schmoe" because she thinks he is a dynamic schmoe. Ernest makes a speech on how he is bold and adventurous and then, in conclusion, puts on his hat heroically, forgetting he had set it on the table and put the ostrich egg in it! His only response is "Eeee-heh-hew! Ew! Ew!"

==Cast==
*Jim Varney as Ernest P. Worrell (a.k.a. Agent 32), Hey-Yu, Auntie Nelda,African woman dancer
*Linda Kash as Rene Loomis
*Jamie Bartlett as Mr. Thompson
*Fela Kuti as Himself
*Claire Marshall as Betty, Renes serving mate
*Washington Xisolo as Sinkatutu Chief
*Robert Whitehead as Prince Kazim
*Zane Meas as Jameen
*Charles Pillai as Kareem

==DVD availability==
This film had its first DVD release from  .

== External links ==
* 

 
 

 
 
 
 
 
 
 
 
 