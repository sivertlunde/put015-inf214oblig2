Ethir Kaatru
{{Infobox film
| name           = Ethir Kaatru
| image          = EthirKaatrufilm.jpg
| image_size     =
| caption        = LP Vinyl Records Cover
| director       = Mukta S. Sundar
| producer       = Muktha Srinivasan Subha
| screenplay     = Muktha Srinivasan Subha
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Mukta S. Sundar 
| editing        = P. Mohanraj
| distributor    =
| studio         = Muktha Films
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1990 Tamil Tamil thriller Karthik and Kanaka in lead roles. The film, produced by Muktha Srinivasan, had musical score by Ilaiyaraaja and was released on 7 December 1990. 

==Plot==

Ram Narendran (Karthik (actor)|Karthik), an orphan youth, decides to put an end to his life. Waiting in the railway track, he sees another man there who tries to jump on the railway track and Ram Narendran saves him. This person is Jana (Anand Babu), he lives with his parents (Ra. Sankaran and M. N. Rajam) and his little sister Geetha (Chithra (actress)|Chithra).

He was a jobless youth and he got a job as a manager in a chit fund company with Madhavans help (a family friend) and paid a big amount to join the company. Later, the company ripped off his clients : Madhavan cheated him. The clients threatened the innocent Jana and they put pressure on him to pay back them. Angry, Jana killed Madhavan, so he decided to commit suicide.

Ram Narendran decides to surrender instead of him and goes to the jail happily. Jana has guilty conscience and he appoints S. Chandrasekaran (V. K. Ramasamy (actor)|V. K. Ramasamy), a lawyer, to bail Narendran. However, Narendran would rather sacrifice his life without goals than Janas life.

Jana meets Devaraj ( ), a politicians cousin,  and a police officers son (Crazy Venkatesh). Jana challenges Devaraj to punish them. The next day, Janas death body is found in a railway track.

S. Chandrasekaran publishes Ram Narendrans book and it turn out to be a huge success. Anita (Kanaka (actress)|Kanaka), a journalist, tries to meet him. Anita and S. Chandrasekaran bail Ram Narendran.

Ram Narendran cannot accept that Jana committed suicide. Anita and he investigate about Janas death.

==Cast==
 Karthik as Ram Narendran Kanaka as Anita
*Anand Babu as Jana (Janardan)
*V. K. Ramasamy (actor)|V. K. Ramasamy as S. Chandrasekaran Chithra as Geetha
*Anandaraj as Devaraj Livingston as Maasi
*Crazy Venkatesh Manorama
*Ra. Sankaran
*M. N. Rajam
*Vathiyar Raman as Kamalakannan
*Venniradai Moorthy
*V. Gopalakrishnan
*Oru Viral Krishna Rao
*Santhana Bharathi
*Delhi Ganesh as Sundaramoorthy
*K. Nataraj

==Soundtrack==

{{Infobox Album |  
| Name        = Ethir Kaatru
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1990
| Recorded    = 1990 Feature film soundtrack |
| Length      = 23:49
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1990, features 5 tracks with lyrics written by Ilaiyaraaja and Vaali (poet)|Vaali.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Ingu Irukkum || Uma Ramanan || 4:56
|- 2 || Koondai Vittu || K. J. Yesudas || 4:43
|- 3 || Nee Ulla Poranthu || Malaysia Vasudevan || 4:48
|- 4 || Raja Illa || Arunmozhi, Uma Ramanan || 4:57
|- 5 || Saamiyaaraa Ponavanukku || Ilaiyaraaja || 4:25
|}

==References==

 

 
 
 
 
 