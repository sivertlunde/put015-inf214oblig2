Tera Mera Vaada
{{Multiple issues|
 
 
 
}}

{{Infobox film
| name           = Tera Mera Vaada
| image          = Tera Mera Vaada film poster.jpg
| caption        = Theatrical release poster
| director       = Bhaal Singh Balhara
| producer       = Ravinder Singh JD
| starring       = Ankit Balhara Neetu Singh
| editor         = Ankit Bidyadhar
| music          = Sanchit Balhara
| distributor    = JD Films Entertainment Pvt Ldt
| released       =  
| country        = India
| language       = Haryanvi
}}
Tera Mera Vaada is a 2012 Haryanvi film  directed by Bhaal Singh Balhara and produced by Ramender Singh JD. The film stars Ankit Balhara and Neetu Singh.  The trailer was unveiled on 8 June 2012 and the film was released on 29 June 2012. The film was shot within 45 days.

==Plot==
 
The movie starts with freshmen students living their new college life.  Abhimanyu(Ankit Balhara) who comes from a village of Haryana to study in city. He is the only child of his village head. Meenal(Neetu Singh)is a modern girl whose father(Shamsher Singh) is a high ranking police officer in Haryana. They start dating but are continuously nagged by the Vicky, who is their college mate. In one instance, Vicky tries to molest Neetu, when Abhimanyu rescues her bravely. That point onwards, Abhimanyu and Vicky become enemies. The movie goes on around their college life till interval.

Abhimanyu studies hard and gets selected to a good post in Haryana Police. Working under Meenals father, he tries to eliminate crime from the streets of Haryana. Then comes Lucky Raam(Vickys father), who is a local goon and produces fake ghee(edible oil) and poisonous liquor. Abhimanyu, as a true police officer starts to destroy the son-father duos illegal business. He then arrests Vicky with his consignment of fake ghee, but was released on bail later. Somehow, Vicky manages some video clip of Meenal having indecent content. He threatens Shamsher Singh to stop Abhimanyu from catching their supply else this video will surface on the Internet. Shamsher Singh tells Meenal not to see Abhimanyu anymore. Startled by her strange behaviour, Abhimanyu couldnt understand whats going on with her. Vickys business starts to flourish again. 37 people die due to consumption of liquor supplied by Vicky. When they lost the video clip, they kidnap Meenal and threaten Shamsher Singh to kill her, if he doesnt stop Abhimanyu from arresting them. Later on, Abhimanyu rescues her and they get married.

Haryanvi culture is introduced in the movie with modern-ship of youngsters. It is the first Haryanvi movie in which music is given by foreign artists.   

==Soundtrack==

The music is composed by Sanchit Singh Balhara and foreign artists of England.  The tracklist comprises 7 tracks and features singers such as Ankit and Antra Mitra. The soundtrack received positive reviews from critics.

==References==
 

 
 
 