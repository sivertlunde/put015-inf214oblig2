Love Over Night
{{infobox film
| name           = Love Over Night
| image          =
| imagesize      =
| caption        =
| director       = Edward H. Griffith E. J. Babille (assistant)
| producer       = Hector Turnbull
| writer         = George Dromgold(writer) Sanford Hewitt(writer) John W. Krafft (intertitles)
| starring       = Rod La Rocque
| music          =
| cinematography = John J. Mescall Burnett Guffey(asst camera)
| editing        = Harold McLernon 
| distributor    = Pathe Exchange
| released       = November 25, 1928
| runtime        = 60 minutes; 6 reels
| country        = United States
| language       = Silent(English intertitles)
}}
Love Over Night is a 1928 silent film comedy produced and distributed by Pathe Exchange and starring Rod La Rocque. The film was directed by Edward H. Griffith.  

A print is said to be held at the Museum of Modern Art, New York. 

==Cast==
*Rod La Rocque - Richard Hill
*Jeanette Loff - Jeanette Stewart Richard Tucker - Richard T. Thorne Tom Kennedy - Detective
*Mary Carr - Grandmother

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 