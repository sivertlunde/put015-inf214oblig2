Vampire Bats (film)
{{multiple issues|
 
 
 
 
}}
{{Infobox television film |
  name     = Vampire Bats |
  image          = Vampire Bats.jpg|
  writer         = Doug Prochilo |
  starring       = Lucy Lawless Dylan Neal Timothy Bottoms Craig Ferguson Jessica Stroup Liam Waite|
  director       = Eric Bross | Christopher Morgan |
  distributor    = Sony Pictures Television |
  released   =   |
  runtime        = 120 minutes |
  language = English |
  budget         = |
  gross           = |
      prequel = Locust!(film)|
}}
Vampire Bats is a 2005 film directed by Eric Bross and starring Lucy Lawless.

==Synopsis== Doctor Maddy Rierdon and her students from colleges work together to kill the bats before the whole city is destroyed.

The film begins with an abandoned house, inhabited by a group of vampire bats, which seem to be mutations of the local population.

Later, in the forests nearby, people begin to find many dead corpses of deer and local animals, with multiple bites on their bodies and their blood completely drained. While the corpses are studied, one student from the class of Physician/Doctor Maddy Rierdon (Lucy Lawless) gets chased in the forest by something that knocks him down and kills him. When he is found, his blood has been drained. Police suspect two of his friends of being the killers and put them under arrest. The case causes Maddys attention, as she wants to prove her students innocence. She soon finds out that her dead student must have been killed by something that moves in a pack and is very fast.

At night, two more fishermen are attacked and killed while fishing on their boat. The next morning, police find the drained bodies and, with Maddys presence at the scene, they are able to recognize some excreta of bats. They then change their opinion and started to believe that bats were the cause of these deaths. The police send out many agents to search around the city for signs of the vampire bats.

After several negative events, including a teenage girl who gets bitten by a bat during sleep and acquires rabies, and a party on board a ship, which is attacked by the bats, Maddy begins to think that the bats have mutated somehow, which she reports to group of agents she is working with. She states that the bats could have been affected by some artificial environmental impacts. To verify her statement, she arrives at the river where the fishermen were killed to set a trap with the help of some of her students. They successfully capture some bats using nets, and Maddy realizes that this population of vampire bats has developed eight upper-jaw fangs instead of two, which makes them hungrier for blood and able to drink more blood every time they bite. They bring the other trapped bats back to the lab, where Maddy informs the police of this mutation. Suggestions are given as how to exterminate these killer bats, and one of them is to spray poisons over the captured bats and release them after sticking GPS locators onto their bodies, letting them fly back to their habitats; the bats will infect each other by licking and grooming. The plan works successfully, but it only kills a small group of them. The other groups remain unaffecteded. Meanwhile, one more student is killed in the pool by the bats. Maddy rushes to find a solution while learning that the bats mutation is an indirect result of toxic waste that contaminated the local water source, creating mutations in the deer that the bats feed on, thus making the bats too mutants.

Meanwhile another group of bats is found in a church (yet still not the whole population.) Maddys students accidentally find out that the bats are attracted to loudspeakers or anything that emits sounds similarly, as they mistake those sounds as reflected ultrasounds. They decide to set a trap in the underground basement of their school, intending to lure the bats in and heat them to death by sending in all the heat from the schools exhausts. As the plan commences, the bats are lured to the area by loudspeakers. A problem is encountered as Maddy is attacked in the underground by one of the policeman, who turns out to be an agent working for the company which has been contaminating the water sources and is therefore responsible for the bats mutation. He tries to kill Maddy in order to eliminate witnesses of his companys work, but Maddy knocks him out instead and escapes when the bats fly into the underground; they devour the agent just before they get heated to death.

Three months later, Maddy is married and sitting with her children in front of the house where they eliminated the first population of the vampire bats. She suddenly looks nervous and seems to be scared by something, but she quickly turns back to play with her kids.

==Cast==
* Lucy Lawless as Maddy Rierdon 
* Dylan Neal as Dan Dryer 
* Liam Waite as Game Warden 
* Timothy Bottoms as Hank Poelker 
* Craig Ferguson as Fisherman  Brett Butler as Shelly Beaudraux 
* Tony Plana as Sheriff Herbst 
* Jessica Stroup as Eden
* Bobby Campo as Don
* Stephanie Honoré as Jossie

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 