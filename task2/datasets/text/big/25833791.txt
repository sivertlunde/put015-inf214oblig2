Tora (film)
{{Infobox film
| name           = Tora
| image          = tora_screenshot.jpg
| alt            = 
| caption        = A Screenshot
| director       = Jahnu Barua 
| producer       = Childrens Film Society, India
| writer         = Jahnu Barua
| starring       = Mrigakshi  Anup Hazarika  Atul Pachani
| music          = Reeta Das Y. S. Moolky
| cinematography = P. Rajan
| editing        = Shivaji Choudhury
| studio         = 
| distributor    = Childrens Film Society of India
| released       =  
| runtime        = 63 minutes
| country        = India
| language       = Assamese
| budget         = 
| gross          = 
}}
Tora (  children’s film directed by Jahnu Barua and produced by the Childrens Film Society, India. The film was released in 2004.    The film received the  Best Children’s Film award in the 51st National Film Awards for the year 2003.  

==Synopsis==
The film centred on two neighboring families in a village in Assam. The two families share a very amicable relation. Tora, the protagonist is a seven-year-old girl with her parents Purna (father) and Jonaki (mother). Naba and Daba are two brothers of the other family with their ailing bedridden mother. One day a dispute arises over a piece of land. Whilst the adults quarrel, Toras voice is the only significant factor that can resolve the matter. 

==Film dedicated to Dhemaji victims==
Jahnu Barua, the director of Tora, dedicated his film on August 19, 2004 to the children killed in the 2004 Dhemaji bombing carried out by ULFA militants on Independence Day. 

==Casts==
*Mrigakshi
*Anup Hazarika
*Indrani Chetia
*Pratibha Choudhury
*Atul Pasoni
*Dina Baishya
*Abatosh Bhuyan
*Sukanya Kharghoria
*Arun Hazarika

==Awards==
*National award - Best Children Film (2003)

==See also==
*Jollywood

==References==
 

==External links==
* 

 
 

 
 
 
 


 
 