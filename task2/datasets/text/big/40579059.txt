Gumnutz: A Juicy Tale
 
{{Infobox film name = Gumnutz: A Juicy Tale image = caption = director = Clive Harrison   Margaret Parkes   Jane Schneider producer = John Cook writer = Phil Sanders music = Clive Harrison starring = Keith Scott   Shane Withington studio = Flying Bark Productions   Big Pix Production distributor = Film Finace Corporation Australia released =   runtime = 80 mins country = Australia language = English budget = 
}}

Gumnutz: A Juicy Tale is an 80 minute length animated film directed by Clive Harrison and released in 2007. It was nominated for APRA Music Awards of 2008 as the Best Music for Childrens Television but lost to Animalia (TV series)|Animalia. 

==Characters==
*Claude - A neurotic and sometimes clumsy orphaned Numbat, who is keen on insects.
*Pepper - A spunky and sporty Numbat and Claudes older sister, who is a champion at tennis and then hockey.
*Uncle Calvin - A Koala and Claude and Peppers uncle, who have been in his care since their parents death. He is the owner of the Gumnut juice factory and wishes to pass his entrepreneurship to Claude.
*Larry - A genius lizard. Claude and Peppers friend who works as a mechanic then gets a job of running the Gumnut juice factory machinery.
*Peer Point J. Marino - A ram. A business man who places tall orders for Gumnut juice.
*Hot Spot Charlie - A crafty, slick Fox out to steal and monopolise Calvins business. He is revealed to be the cause of Claude and Peppers parents deaths.
*Frankie - Hot Spots spider servant.
*Percy - A Pelican, who works as a lifeguard, detective, policeman and road official.
*Ghoulie Gilly - A Scottish mouse who lives in a creepy forest and is eventually requested by Claude to help rescue Calvin.
*Granpoala - Claude and Peppers grandfather who has a short memory for various things.

==Plot==
One morning it is Claudes first day to work at his Uncle Calvins Gumnut juice factory. Claude doesnt feel up to it at first. Calvin receives an order for Gumnut juice from the business man Peer Point J. Marino which if successful will grant Calvins company a contract. The rival Hot Spot Charlie hoping to beat Calvin to the contract first, sends Frankie to steal the secret sweetening formula to fulfill his quota.

Hot Spot has the apothecarie Cobra analyse the ingredients of the secret formula, but without any success. Hot Spot sends his goons to kidnap Calvin. Claude finds a lead to Bad Manor, but Calvin is well hidden. Hot Spot gets hold a riddle book which has a clue to the secret formula. Catching wind of this, Claude and Pepper sneak into Bad Manor and copy the riddle, Hot Spot also getting the knowledge.

Claude and Pepper follow the riddle to Darwin Platypus who tells them the secret formula, Hot Spot listening in. Hot Spot traps Claude, Pepper and Darwin Platypus, but Cobra defects and rescues them and they return to the Gumnut juice factory to begin a large production. Claude is briefly separated from the others, but uses Calvins special instrument to rally all the Cicadas for the essential ingredient of the formula.

Claude, his friends and a swarm of Cicadas thwart Hot Spots attempt to destroy the Gumnut juice factory, then follow him to Bad Manor to rescue Calvin helped by Ghoulie Gilly. Once Calvin is rescued, Cobra blows Bad Manor with Hot Spot and his goons to the Arctic. After that Calvin is able to meet Marinos quota and get the contract.

==References==
 

==External links==
* 

 
 
 
 
 