God Reward You
{{Infobox film
| name           =God Reward You
| image          =1948 Dios se lo pague.jpg
| image_size     =
| caption        =
| director       = Luis César Amadori
| producer       =
| writer         = Joracy Camargo (play), Tulio Demicheli (writer)
| narrator       =
| starring       =
| music          =Juan Ehlert 	
| cinematography =Alberto Etchebehere 
| editing        =Jorge Gárate
| distributor    =
| released       =March 11, 1948
| runtime        = 120 minutes
| country        = Argentina Spanish
| budget         =
}}
 1948 Argentina|Argentine drama film directed by Luis César Amadori and starring Arturo de Córdova and Zully Moreno.   It won the Silver Condor Award for Best Film, given by the Argentine Film Critics Association in 1949 for the best picture of the previous year. 

==Cast==
*Arturo de Córdova
*Zully Moreno
*Florindo Ferrario
*Enrique Chaico
*Federico Mansilla
*Zoe Ducós
*José Comellas
*Roberto Bordoni
*Ramón Garay
*Tito Grassi
*Adolfo Linvel
*José Antonio Paonessa
*Enrique de Pedro
*Nicolás Taricano
*Aída Villadeamigo

== 1981 telenovela ==
 ATC (Argentina Televisora Color) broadcast the telenovela Dios se lo pague adapted by Vicente Sesso and directed by Alberto Rinaldi. The telenovela was starred by Víctor Hugo Vieyra, Leonor Benedetto, Federico Luppi, Gloria Antier, Susy Kent, Gianni Lunadei and Juan Peña.

==References==
 

==External links==
*  
*  at the Universidad de Antioquia website  

 
 
 
 
 

 