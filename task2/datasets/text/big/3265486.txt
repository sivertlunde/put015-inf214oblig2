Everynight ... Everynight
 
{{Infobox film
| name           = Everynight ... Everyday
| image          = EverynightEverynightDVDCover.jpg
| image_size     = 
| caption        = 
| director       = Alkinos Tsilimidos
| producer       = Alkinos Tsilimidos
| writer         = Ray Mooney Alkinos Tsilimidos
| narrator       =  David Field, Bill Hunter, Robert Morgan Paul Kelly
| cinematography = Toby Oliver
| editing        = Cindy Clarkson
| distributor    = Siren Visual Entertainment
| released       = 1994
| runtime        = 92 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Everynight ... Everynight is an Australian drama film directed by  . Retrieved 18 January 2008  Filming was undertaken at HM Prison Geelong.

==Plot==
Dale, a remandee, is awaiting a court hearing and yet to be sentenced, highlighting the horrific injustice of the repeated beatings hes subjected to. Although the first depicted beating lasts less than 3 minutes of screen time, the actual beating it was based on allegedly lasted a gruelling 7 hours.

Initially Dale submits to the psychological and physical traumas of his situation. By day they attempt to break his spirit and sanity by forcing him to smash blue-stone with a pick, invoking images of Australias convict heritage, while another inmate is compelled to perform more demeaning behaviour such as licking faeces off toilet doors. Gradually Dale becomes indifferent to the bashings and horrors of prison life and develops an alternative, subversive way to exist and express his rage. 

At one point Dale is depicted pacing his cell naked and mumbling incoherently. It seems as if the ego shattering experience has forced him to the verge of insanity. Its not until he claims : "Ive resigned from this life" and urges the other inmates to do so as well, that we see method in his madness. By refusing to play the dehumanising prison game anymore the guards have lost their threat of psychological and physical suppression over him and he in turn has reaffirmed the power of the simple utterance of which he can never be deprived. Although contact between the inmates is strictly forbidden at night they manage to shout and finally communicate through the prison walls. "Unity in adversity!", Dale shouts beginning a chant which reverberates throughout the cells. Meanwhile Berriman, realising the threat of pure violence or psychological abuse is no longer effective starts to panic. 

As Dale walks defiantly from the prison in the last scene to be tried, the failure of the correctional system to produce docile, disciplined bodies pulls its last punch. Even if the system has enframed Dale he has maintained his sanity and his voice.

==Cast== David Field as Dale Bill Hunter as Berriman Robert Morgan as Kert
*Phil Motherwell as Bryant Jim Daly as Barrett Jim Shaw as Governor
*Billy Tisdall as Gilchrist
*Simon Woodward as Gaunt
*Theodore Zakos as Driscoll

==Awards==
 Best Director Best Screenplay at the 1994 Australian Film Institute Awards  and a nomination for the Bronze Horse award at the 1994 Stockholm International Film Festival. 

==References==
 

==External links==
*  
*   at Allmovie
*   at Screen Australia
*   at New York Times
*   at Film.com

 

 
 
 
 
 
 
 
 
 
 