Lilla Märta kommer tillbaka
{{Infobox film
| name           = Lilla Märta kommer tillbaka
| image          =
| caption        =
| director       = Hasse Ekman
| producer       =
| writer         = Hasse Ekman 
| narrator       =
| starring       = Stig Järrel Hasse Ekman Tollie Zellman Sten Larsson
| music          = Sune Waldimir, Kai Gullmar
| cinematography = 
| editing        = 
| distributor    =
| released       =  
| runtime        = 80 minutes
| country        = Sweden Swedish
| budget         =
| gross          =
}}

Lilla Märta kommer tillbaka or Grevinnans snedsteg eller Den vilda jakten efter det hemliga dokumentet is a 1948 Swedish comedy film directed by Hasse Ekman and starring Stig Järrel and Hasse Ekman.

== Cast ==
*Stig Järrel as Sture Letterström/Ms. Märta Letterström
*Hasse Ekman as Curt "Kurre" Svensson/Ms. Elvira Pettersson 
*Hugo Jacobson as Pontus Bruzell 
*Tollie Zellman as Tora, his wife
*Brita Borg as Inga Bruzell, Pontus and Toras daughter
*Sten Larsson as doctor Gotthard Vogel, nazi 
*Douglas Håge as Peter Sonne, f.d. Pettersson, nazi 
*Charlie Almlöf as Charles-Emile Högquist
*Benkt-Åke Benktsson as doktor Bauerbrecht, nazi
*Hjördis Petterson as Fräulein Schultze, nazi
*Ernst Brunman as police Karlsson, nazi
*Gunnar Björnstrand as captain
*Harald Emanuelsson as the captains chauffeur
*Margit Andelius as Ms. Synnergren, teacher, nazi

== External links ==
*  

==References==
 

 

 
 
 
 
 
 

 