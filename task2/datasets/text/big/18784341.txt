The City Stands Trial
 
{{Infobox film
| name           = The City Stands Trial
| image          = 
| caption        = 
| director       = Luigi Zampa
| producer       = 
| writer         = Suso Cecchi dAmico Diego Fabbri Ettore Giannini Francesco Rosi Turi Vasile
| starring       = Amedeo Nazzari
| music          = 
| cinematography = Enzo Serafin
| editing        = Eraldo Da Roma
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

The City Stands Trial ( ) is a 1952 Italian drama film directed by Luigi Zampa and starring Amedeo Nazzari. It was entered into the 3rd Berlin International Film Festival.   

==Cast==
* Amedeo Nazzari - Judge Spicacci
* Silvana Pampanini - Liliana Ferrari
* Paolo Stoppa - Perrone
* Dante Maggio - Armando Capezzuto
* Franco Interlenghi - Luigi Esposito
* Irène Galter - Nunziata
* Gualtiero Tumiati - Consigliere Capo
* Tina Pica - Restaurants cook
* Turi Pandolfini - Don Filippetti
* Mariella Lotti - Elena  
* Franca Tamantini
* Bella Starace Sainati
* Agostino Salvietti
* Mimi Ferrari - Cantante
* Viviane Vallé - Adelina Leonardi
* Vittorio André - Spinelli
* Tina Castigliano - Tenutaria

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 