Sometimes They Come Back... Again
{{Infobox television film
| name           = Sometimes They Come Back... Again
| image          = Cover of the movie Sometimes They Come Back... Again.jpg
| caption        = 
| director       = Adam Grossman
| producer       = Mark Amin Barry Barnholtz Michael Meltzer
| writer         = Stephen King  (characters) , Guy Riedel, Adam Grossman Michael Gross Alexis Arquette Hilary Swank
| music          = Peter Manning Robinson
| cinematography = Christopher Baffa
| editing        = Stephen Myers
| distributor    = Trimark Pictures  (video) 
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $3,000,000  (est)  
| preceded_by    = Sometimes They Come Back
| followed_by    = Sometimes They Come Back... for More
}} Sometimes They Come Back.  It was directed by Adam Grossman. It stars Michael Gross, Alexis Arquette, and Hilary Swank.

== Plot ==
 Michael Gross) learns that his mother has just mysteriously fallen to her death. Jon and his teenage daughter Michelle (Hilary Swank) return to Jons hometown of Glenrock for his mothers funeral. Once there, painful memories return. Thirty years earlier, when Jon was a child, he witnessed the brutal murder of his older sister Lisa (Leslie Danon), who was stabbed to death in a cave by a thug named Tony Reno (Alexis Arquette) and his two friends Vinnie (Bojesse Christopher) and Sean (Glen Beaudin). But Jon managed to throw an electrical wire into a puddle of bloody water they were standing in, killing all three of them.

Michelle becomes friends with mentally retarded gardener Steve (Gabriel Dell Jr.), as well as two girls, boy-crazy Maria (Jennifer Aspen), and Marias psychic best friend, Jules (Jennifer Elise Cox), who used to clean her grandmothers house. The night after the funeral, they invite Michelle to go to the dinner with them, saying they would like to get to know her before she goes home for her 18th birthday. At the dinner, the girls are greeted by a boy who looks a lot like Tony Reno, even with the same name. While Maria develops a crush on him, he seems to be attracted to Michelle. He gives Michelle an old wristwatch as an early birthday present, then leaves.

Meanwhile, Jon is pestered by Father Archer Roberts (W. Morgan Sheppard), a priest he came to when Lisa was murdered. He tells Jon his mothers death was not an accident.

==Sequel==
The video was followed by another straight-to-video sequel in 1998 titled, Sometimes They Come Back... for More.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 


 