Zombie Lake
{{Infobox film
| name           = Zombie Lake
| image          = Zombie lake poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = French film poster for Zombie Lake
| film name      = 
| director       = {{plainlist|
*Julian de Laserna
*Jean Rollin}}
| producer       = Daniel Lesoeur
| writer         = {{plainlist|
*Julián Esteban
*Marius Lesoeur}}
| starring       = {{plainlist|
*Howard Vernon}}
| music          = Daniel White 
| cinematography = Max Monteillet
| editing        = 
| studio         = {{plainlist|
*J.E. Films
*Eurociné   }}
| distributor    = Eurociné
| released       =  
| runtime        = 90 minutes 
| country        = {{plainlist|*Spain
*France}}
| language       = 
| budget         = 
| gross          = 
}}

Zombie Lake ( ) is a 1981 Spanish-French horror film directed by Jean Rollin and Julian de Laserna. The film stars Howard Vernon as the mayor of a small French town that is plagued by Nazi zombies who were killed by the towns villagers ten years earlier.
 Shock Waves (1977).

==Plot== mayor of the town (Howard Vernon) refuses to take action against the zombie attacks until the reporter Katya Moore (Marcia Sharif) arrives to investigate.

After Moore returns a book to the mayor, he discusses with her the history of the town during the German occupation. His story is about a young Nazi soldier who protected a local woman from enemy gunfire. He was nursed back to health by the woman, who offered him her pendant and had sex with him. Returning to the woman later, the soldier finds her dying after giving birth to their daughter Helena. The soldier and his full squadron are then killed by a team of townspeople lead by the mayor. Their bodies were disposed of in the lake.

The mayor says that he believes the zombies are the soldiers returning from the dead. Later, a female basketball team visiting the town is attacked by the zombies. Given the scale of the tragedy, the mayor calls for police assistance who send two detectives over to investigate. They too are killed by the zombies. The mayor then devises a plan to use the zombies relationship with Helena by having her lure them into a mill. The zombies enter the mill, which is then destroyed by the villagers using flame-throwers.

==Production==
Zombie Lake was initially going to be directed by Jesus Franco.  Franco left the project after having arguments with the films distributor Eurociné.     Eurociné had director Jean Rollin direct the film. He entered production with only a few days notice.  The films final product is credited to J.A. Lazer.  Julian de Laserna directed parts of the film under the supervision of Jean Rollin. The film credits them both under the pseudonym J.A. Laser.  Rollin appears in the film as Inspector Spitz.   

The film was written by Julián Esteban and Eurociné producer Marius Lesoeur.  Lesoeur was credited under the pseudonym of A.L. Mariaux.   
 Daniel White was described by Tim Lucas in Video Watchdog as "taken from at least four other movies". 

==Release==
Zombie Lake was released in France on May 13, 1981.  The film was shown for one week in Paris where it sold 3740 tickets. 

===Home media===
Zombie Lake was released by Wizard Home Video on VHS.    The film was released on DVD by Image Entertainment as part of their Euro Shock collection on March 27, 2001.       The film was released on DVD in the by Arrow Films on February 9, 2004. 

The film was released on Blu-ray Disc and DVD by Kino Film on February 26, 2013.     The transfer of the Kino discs of the Zombie Lake were mastered to have scenes that are set at night but shot in the daytime appear as if it is night.   

==Reception==
  gave the film a two out of five rating, praising it as a non-typical zombie film but noting cheap effects and calling it "crappy and terribly slow".  Online film database Allmovie gave the film one star out of five, stating that "those looking for a better treatment of the same plot should consider Ken Wiederhorns Shock Waves instead". 

==Notes==
 

===References===
*  
*  

==See also==
*List of French films of 1981
*List of horror films of 1981
*List of Spanish films of 1981

==External links==
* 
* 

 

 
 
 
 
 
 