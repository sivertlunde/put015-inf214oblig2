Bandits in Milan
 
{{Infobox film
| name           = Bandits in Milan
| image          = TheViolentFour-Poster.jpg
| alt            = 
| caption        = Film poster under alternative English title
| film name      = Banditi a Milano
| director       = Carlo Lizzani
| producer       = Dino De Laurentiis Curti, 2013. p. 11 
| screenplay     = {{plainlist|
* Massimo De Rita
* Dino Maiuri
* Carlo Lizzanii }}
| story          = Carlo Lizzani 
| based on       =  
| starring       = {{plainlist|
* Tomas Milian
* Gian Maria Volonté}}
| narrator       =  
| music          = Riz Ortolani 
| cinematography = Giuseppe Ruzzolini 
| editing        = Franco Fraticelli 
| studio         =  
| distributor    = Paramount 
| released       =  
| runtime        = 
| country        = Italy 
| language       = 
| budget         = 
| gross          = ₤1.768 billion 
}}

Bandits in Milan ( ; also known as The Violent Four) is a 1968 Italian crime film directed by Carlo Lizzani. It was listed to compete at the 1968 Cannes Film Festival,    but the festival was cancelled due to the events of May 1968 in France. 

It is the debut film of Agostina Belli.    

==Production==
Like director Carlo Lizzanis previous film Wake Up and Die is based off of a real life event, specifically a bank robbery that went wrong in Milan on September 25, 1967. 

==Cast==
* Tomas Milian as Commissario Basevi
* Gian Maria Volonté as Piero Cavallero
* Don Backy as Sante Notarnicola Ray Lovelock as Donato Tuccio Lopez
* Ezio Sancrotti as Adriano Rovoletto Bartolini
* Piero Mazzarella as Piva
* Laura Solari as Tuccios Mother
* Pietro Martellanza as The Protector (as Peter Martell) Margaret Lee as Prostitute
* Carla Gravina as Anna
* Luigi Rossetti as Robber
* María Rosa Sclauzero as Pieros Secretary
* Ida Meda as Moglie di Piero
* Tota Ruta as Club Hitman
* Evi Rossi Scotti
* Gianni Bortolotti
* Agostina Belli as Ragazza in ostaggio
* Pupo De Luca as Uomo della 1100
* Giovanni Ivan Scratuglia (as Ivan Scratuglia)
* Gianni Pulone
* Umberto Di Grazia
* Enzo Consoli

==Release==
Bandits in Milan was released on March 30, 1968.  It grossed just over ₤1.768 billion in Italy. 
As of 2013, the film has never been released on home video. 

==Notes==
 

===References===
*  


==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 
 