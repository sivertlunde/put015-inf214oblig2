Five from the Jazz Band
{{Infobox film
| name =  Five from the Jazz Band 
| image =
| image_size =
| caption =
| director = Erich Engel
| producer =  Hans Conradi   Joe Pasternak
| writer =  Felix Jackson  (play)   Curt Alexander    Henry Koster
| narrator =
| starring = Jenny Jugo   Rolf von Goth   Fritz Klippel   Karel Stepanek
| music = Theo Mackeben   
| cinematography = Reimar Kuntze   
| editing =  Andrew Marton      
| studio = Deutsche Universal-Film 
| distributor = Deutsche Universal-Film 
| released = 19 March 1932
| runtime = 88 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} musical comedy film directed by Erich Engel and starring Jenny Jugo, Rolf von Goth and Fritz Klippel.  It is based on a 1927 play of the same name by Felix Jackson, and was produced by the German subsidiary of Universal Pictures. The films sets were designed by art director Erich Czerwonski. 

==Cast==
* Jenny Jugo as Jessie  
* Rolf von Goth as Jim 
* Fritz Klippel as Moritz  
* Karel Stepanek as Jean  
* Günther Vogdt as Bill  
* Theo Shall as Martin 
* Werner Pledath as Director  
* Arthur Mainzer as Sasse  
*   Heinrich Gretler as Judge 
* E. Helmke-Dassel as Erika  
* Peter Ihle as Bühnenmaler  
* Fritz Melchior as Stage Manager  
* Vera Spohr as Chambermaid  
* Charles Puffy as Ansager beim Ringkampf  
* Peter Lorre as Car thief  
* Willi Schur as Bestohlener in Kneipe  
* Gustav Püttjer as Bühnenarbeiter  
* Theo Mackeben as Kapelmeister 
* Walter Steinbeck as Spinner  
* Henry Pleß 
* Karl Hannemann
* Robert Klein-Lörk 
* Ludwig Rüth
* Gerhard Bienert

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009. 
 
== External links ==
*  

 

 
 
 
 
 
 
 
 
 