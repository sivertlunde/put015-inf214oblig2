The Turn of the Screw (1974 film)
 
{{Infobox television film
| name           = The Turn of the Screw
| image          = The Turn of the Screw (1974 film).jpeg
| image_size     =
| alt            =
| caption        = 
| director       = Dan Curtis Tim Steele
| writer         = William F. Nolan
| based on       = The Turn of the Screw by Henry James
| narrator       =
| starring       = Lynn Redgrave
| music          = Bob Cobert
| cinematography = Ben Colman
| editing        = Dennis Virkler
| studio         = 
| distributor    = 
| released       = April 15, 1974
| runtime        = 120 minutes
| country        =  
| language       = English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}} novel of the same name by Henry James.

==Plot==
An English governess is hired to take care of two adorable orphans, who turn out to be not exactly what they seem to be.

==Cast==
* Lynn Redgrave as Miss Jane Cubberly John Barron as Mr. Fredricks
* Eva Griffith as Flora
* Jasper Jacob as Miles
* Megs Jenkins as Mrs. Grose
* Anthony Langdon as Luke (credited as Anthony Lagdon)
* James Laurenson as Peter Quint
* Kathryn Leigh Scott as Miss Jessel
* Benedict Taylor as Timothy

==Production==
The film was shot in London, England. 

==Broadcast==
The film was first broadcast in the USA on April 15, 1974.

==External links==
*  

==References==
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 