Visions of Light
{{Infobox film
| name           = Visions of Light
| image          = Visionsoflightposter.jpg
| caption        = Publicity poster
| alt            = 
| director       = Arnold Glassman Todd McCarthy Stuart Samuels
| producer       = Terry Lawler Yoshiki Nishimuri
| writer         = Todd McCarthy
| narrarator     =
| cinematography = Nancy Schreiber
| editing        = Arnold Glassman Kino International
| released       =  
| runtime        = 92 minutes
| country        = Japan United States
| language       = English
| budget         =
| gross          =
}}
Visions of Light is a 1992 documentary film directed by Arnold Glassman, Todd McCarthy and Stuart Samuels. The film is also known as Visions of Light: The Art of Cinematography. 

The film covers the art of cinematography since the conception of cinema at the turn of the 20th century.  Many filmmakers and cinematographers present their views and discuss why the art of cinematography is important within the craft of filmmaking.

 

==Synopsis==
The film is the equivalent of a walk through a cinema museum. The doc interviews many modern-day directors of photography and they illustrate via examples their best work and the scenes from films that influenced them to pursue their art.
 John Bailey, Michael Chapman, László Kovács (cinematographer)|László Kovács, and others.

They discuss their craft and pay homage to the cinema pioneers like Gregg Toland, Billy Bitzer, James Wong Howe and John Alton.  The practitioners also explain the origins behind many of their most indelible images in cinema history.

==Cinematographer interviews==
* Ernest Dickerson Michael Chapman
* Allen Daviau
* Caleb Deschanel
* Conrad Hall
* William A. Fraker John Bailey
* Néstor Almendros
* Vilmos Zsigmond
* Stephen H. Burum
* Charles Lang
* Sven Nykvist
* László Kovács (cinematographer)|László Kovács
* James Wong Howe
* Haskell Wexler
* Vittorio Storaro
* John A. Alonzo
* Victor J. Kemper
* Owen Roizman
* Gordon Willis Bill Butler
* Michael Ballhaus
* Frederick Elmes

==Filmography==
The filmmakers discuss the following films:
 
 
* Dickson Experimental Sound Film (1895)
* Repas de bébé (1895)
* LArrivée dun Train en Gare de la Ciotat|LArrivée dun train à la Ciotat (1895) The Kiss (1896)
* Le Spectre rouge (1907)
* The Birth of a Nation (1915)
* Intolerance (film)|Intolerance (1916)
* The Cabinet of Dr. Caligari (1920)
* Way Down East (1920) Der Letzte Mann (1924)
* Ben-Hur (1925 film)|Ben-Hur (1925)
* Napoléon (1927 film)|Napoléon (1927)
*   (1927) The Crowd (1928)
* The Cameraman (1928)
* The Cocoanuts (1929)
* On the Waterfront (1954)
* Applause (1929 film)|Applause (1929)
* The Locked Door (1929)
* Possessed (1931 film)|Possessed (1931)
* Dr. Jekyll and Mr. Hyde (1931 film)|Dr. Jekyll and Mr. Hyde (1931) Shanghai Express (1932) As You Desire Me (1932)
* What Price Hollywood? (1932)
* Red Dust (1932) Mystery of the Wax Museum (1933)
* Gold Diggers of 1933 (1933) Queen Christina (1933) Becky Sharp (1935)
* Peter Ibbetson (1935)
* Desire (1936 film)|Desire (1936)
* Camille (1936 film)|Camille (1936)
* Jezebel (film)|Jezebel (1938) The Adventures of Robin Hood (1938)
* Midnight (1939 film)|Midnight (1939)
* The Story of Vernon and Irene Castle (1939) The Wizard of Oz (1939) Gone with the Wind (1939) The Grapes of Wrath (1940)
* Rebecca (1940 film)|Rebecca (1940) The Sea Hawk (1940)
* The Long Voyage Home (1940)
* Citizen Kane (1941) How Green Was My Valley (1941) The Magnificent Ambersons (1942)
* Meet Me in St. Louis (1944) Mildred Pierce (1945) The Killers (1946)
* Out of the Past (1947)
* T-Men (1947)
 
* The Naked City (1948) Oliver Twist (1948)
* She Wore a Yellow Ribbon (1949) Young Man with a Horn (1950) Sunset Boulevard (1950)
* The Big Combo (1955) The Night of the Hunter (1955)
* Picnic (1955 film)|Picnic (1955)
* Sweet Smell of Success (1957)
* Touch of Evil (1958)
* Jules et Jim (1962) Lawrence of Arabia (1962)
* Hud (film)|Hud (1963)
* Whos Afraid of Virginia Woolf? (film)|Whos Afraid of Virginia Woolf? (1966) The Professionals (1966)
* Cool Hand Luke (1967) In Cold Blood (1967)
* The Graduate (1967)
*   (1968)
* Rosemarys Baby (film)|Rosemarys Baby (1968)
* Easy Rider (1969)
* Midnight Cowboy (1969) The Conformist (1970)
* McCabe & Mrs. Miller (1971) The French Connection (1971)
* The Godfather (1972) Fat City (1972)
* Chinatown (1974 film)|Chinatown (1974)
* The Godfather Part II (1974) The Day of the Locust (1975)
* Jaws (film)|Jaws (1975)
* Dog Day Afternoon (1975)
* Taxi Driver (1976)
* Eraserhead (1977)
* Annie Hall (1977)
* Days of Heaven (1978)
* Apocalypse Now (1979)
* Raging Bull (1980)
* E.T. the Extra-Terrestrial (1982)
* Blade Runner (1982)
*   (1985) Blue Velvet (1986)
* The Last Emperor (1987) Empire of the Sun (1987) The Unbearable Lightness of Being (1988) The Last Temptation of Christ (1988)
* Do the Right Thing (1989)
* Goodfellas (1990)
 

==Reception==
Visions of Light has an overall approval rating of 94% on Rotten Tomatoes. 

==Awards==
Wins
* New York Film Critics Circle Awards: NYFCC Award; Best Documentary; 1993.
* Boston Society of Film Critics Awards: BSFC Award Best Documentary; 1993.
* National Society of Film Critics Awards: NSFC Award; Best Documentary; 1994.

Nominations
* American Cinema Editors: Eddie; Best Edited Documentary, Arnold Glassman; 1994.

==See also==
* Cinematographer Style (2006)
*   (2006)

==References==
 

==External links==
*  
*  
*   Siskel and Ebert and The Movies 
*   at Box Office Mojo
*  

 
 
 
 
 
 
 