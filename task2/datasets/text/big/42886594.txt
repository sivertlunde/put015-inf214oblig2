Diary of a Bachelor
{{Infobox film
| name           = Diary of a Bachelor
| image          =
| image_size     =
| caption        =
| director       =Sandy Howard 
| producer       =
| writer         = Ken Barnett
| based on       =
| starring       = William Traylor Joe Silver Dagne Crane
| music          = Jack Pleis 
| cinematography =
| editing        =
| studio         =
| distributor    =American International Pictures (US)
| released       = 1964
| runtime        = 
| country        = USA
| language       = English
| budget         =
| gross          =
}}
Diary of a Bachelor is a 1964 film.  It features an earlier appearance by Dom De Luise.

==Synopsis==
Joanne, a young bride-to-be has found the diary of her intended, Skip OHara, a man notorious for being a playboy. Sure that shell read about his various exploits, she flips through the diary but is shocked to find that he is not exactly the type of person everyone believes him to be. She discovers he has been having affairs with numerous young women and leaves Skip.

An airline hostess who features strongly in diary, Nancy Feather, arrives at Skips apartment. Joanne decides to return to Skip and apologise but when she finds Nancy with him, she leaves for good.

A year later, Skip and Nancy are married and Skip has settled down. But Nancy continues to see old boyfriends.

==Cast==
*William Traylor as Skip OHara
*Joe Silver as Charlie Barrett
*Dagne Crane as Joanne
*Denise Lor as Jane Woods
*Jan Crockett as Jennifer Watters
*Susan Dean as Barbara
*Eleni Kiamos as Angie Pisano
*Arlene Golonka as Lois
*Joan Holloway as Nancy Feather
*Mickey Deems as Barney Washburn
*Paula Stewart as Carlotta Jones
*Dom DeLuise as Marvin Rollins
*Jackie Kannon sd Bob Haney

==Reception==
Critical reception has been mixed.  DVD Talk gave a mixed review, writing " A breezy little dirty joke with just a smidgeon of dirt in it, Diary of a Bachelor could have been a lot more fun had it fully embraced its latent nudie cutie urges, or if it had concentrated on a more interesting bachelor here, played by the marvelous Joe Silver."  TV Guide panned the film and gave it one star, stating "Told in a series of redundant flashbacks, the clumsy hand of producer/director Howard fails to keep interest for long. An unsympathetic characters life told in an uncaptivating style doesnt leave much room for praise."  Variety (magazine)|Variety also criticized the film as a "tame tale" with the byline "lack of names and generally pallid comedy make it candidate for second billing". 

==References==
 

==External links==
*  
*  at TCMDB

 
 
 


 