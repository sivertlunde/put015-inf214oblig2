The Atomic Kid
{{Infobox film
| name           = The Atomic Kid
| image          = The Atomic Kid poster.jpg
| caption        = Australian theatrical release poster
| director       = Leslie H. Martinson
| producer       = Maurice Duke Mickey Rooney
| writer         = Blake Edwards Benedict Freedman John Fenton Murray Robert Strauss
| music          = Van Alexander John L. Russell Fred Allen
| distributor    = Republic Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| preceded by    = 
| followed by    = 
}}

The Atomic Kid is a 1954 black-and-white science fiction film|science-fiction comedy film starring Mickey Rooney and directed by Leslie H. Martinson. While a uranium prospector is in the desert he is accidentally exposed to radiation from an atomic bomb test. He becomes radioactive and is recruited by the FBI to help break up a spy ring.

Based on a story by Blake Edwards, the film version was adapted into a comedy. 

In this film, Rooneys character "Blix" Waterberry wanders into an atomic test site, and, as one reviewer describes, "Mannequins are depicted sitting around the dinner table in front of their plastic meal, awaiting the predetermined bomb drop... Rooney remains with the mannequin family and discovers at the last minute that an atomic bomb will be detonated over his head.  In a deliberately humorous scene, Rooney frantically tries to find a place to hide from the approaching explosion, only to close his eyes and stick his fingers in his ears as the bomb goes off." 

Nurse Audrey Nelson (Elaine Devry), who marries "Blix" at the end, is the only female in the entire picture. At the time it was filmed, she was married to Mickey Rooney in real life. This fact appears in the opening credits and on promotional posters. Devry was billed as Elaine Davis.

==Cast==
* Mickey Rooney as Barnaby Blix Waterberry Robert Strauss as Stan Cooper
* Elaine Devry as Audrey Nelson 
* Bill Goodwin as Dr. Rodell
* Robert Emmett Keane as Mr. Reynolds
* Whit Bissell as Dr. Edgar Pangborn
* Joey Forman as MP in hospital
* Dan Riss as Jim, FBI Chief Agent
* Peter Leeds as FBI Agent Bill
* Hal March as FBI Agent Ray
* George E. Mather as 1st Sergeant
* Fay Roope as Gen. Lawlor
* Bill Welsh as Commentator Stanley Adams as Wildcat Hooper Robert Nichols as Bob, technician in Blixs room
* Paul Dubov as Anderson, advertising agent
* Peter Brocco as Comrade Mosley
* Trustin Howard as Corporal
* Charles J. Conrad as Scientist
* Sig Frohlich as Photographer
* Milton Frome as Communications man

==In popular culture==
* This is the film showing at the Town Theater in 1955 Hill Valley in the 1985 science fiction comedy Back to the Future.  In the original BTTF novel, the character Marty McFly decides to take in a movie when he finds himself transported back to 1955 and chooses The Atomic Kid as opposed to Cattle Queen of Montana at the nearby Essex Theater.

==References==
 

===Bibliography===
* Ted Okuda, "The Atomic Kid: Radioactivity Finds Andy Hardy" in Science Fiction America: Essays on SF Cinema (edited by David J. Hogan; McFarland, 2006), pp. 120–129.
* David Wingrove, Science Fiction Film Source Book (Longman Group Limited, 1985).

==External links==
*  

 

 
 
 
 
 
 
 