Mr. India (1987 film)
 
 
 
 
{{Infobox film 
| name           = Mr. India
| image          = Mr. India VideoCover.jpeg
| director       = Shekhar Kapur
| producer       = Boney Kapoor
| writer         = Salim-Javed
| starring       = {{Plain list|
* Anil Kapoor
* Sri Devi
* Satish Kaushik
* Amrish Puri
* Annu Kapoor
}}
| music          = Laxmikant-Pyarelal Javed Akhtar (lyrics) 
| cinematography = Baba Azmi
| editing        = Waman Bhonsle Gurudutt Shirali
| distributor    = Narsimha Enterprises
| released       =  
| runtime        = 179 minutes
| country        = India
| language       = Hindi
| gross          =  120&nbsp;million 
}}
 fantasy superhero film directed by Shekhar Kapur. It stars Sridevi, Anil Kapoor, and Amrish Puri in lead roles
 Indiatimes Movies ranks the movie amongst the Top 25 Must See Bollywood Films.  This was the last film that the writing duo Salim-Javed wrote together. They had split up earlier in 1982, but came back for one last film.

On the centenary of Indian Cinema, Mr. India has been declared one of the 100 Greatest Indian Films of All Time.  Interestingly Rajesh Khanna was offered the lead role, but he turned it down as he couldnt relate with the invisible hero.  

== Plot ==
The film opens with the revelation that violence has escalated in India, in which the central villain is Mogambo (Amrish Puri). Mogambo is a brilliant yet insane General whose goal is to conquer India. From his island, modelled after the island of Dr. No (film)|Dr. No, Mogambo monitors the evil-doings perpetrated by his henchmen. All his subordinates know the formal salute "Hail Mogambo!", emphasising his complete authority over his minions.

Arun Verma (Anil Kapoor) is an orphan and a street-walking violinist who rents a large, old house. There, he houses a dozen or so orphaned children and takes care of them with the help of his cook and caretaker Calendar (Satish Kaushik). Arun is poor, owes debts to the local food merchant Roopchand (Harish Patel), and is overdue to pay the landlord Maniklal (Yunus Parvez). In spite of these problems, Arun tries to look on the bright side of every situation. As time goes by, Calendar reminds Arun that the cost of running the home continues to rise. Arun decides to rent out the room on the first floor. When he goes to the local newspaper to run an advertisement for the room, he meets Seema Sahni (Sridevi) and makes her his tenant. They get off to a rocky start but Seema eventually becomes friends with everyone.

One day, Arun receives a mysterious letter from a family friend, Dr. Sinha ( .

Mr. India later defeats Mogambos men in several encounters, foiling their criminal plans. Mogambos computer-aided intelligence teams reveal that Mr. Indias specific actions against the gambling den bring benefit to Arun Verma. Mogambo has bombs disguised as toys, planted in places where children can find them. One of the bombs kills Aruns youngest and dearest charge, Tina, driving Arun to immense grief. Arun, Seema, Calendar, and the surviving children are brought before Mogambo. Mogambo tortures them so that they reveal Mr. Indias true identity and the location of the invisibility device. Arun eventually volunteers to this when Mogambo threatens to drop two children into a pit of acid; but because Arun has dropped the device, he cannot become invisible to prove himself. Frustrated, Mogambo has them sent into the dungeons.

However, they are all able to escape. Mogambo activates four ICBMs, which are poised to destroy all of India. Arun confronts him, and the two fight. When Arun has overcome Mogambo, he deactivates the launch which culminates in the missiles detonating on the launch-pad. Arun, Seema, Calendar, and the children escape, while Mogambos fortress is destroyed. Mogambo probably dies inside the big fireball caused by the explosion of the missiles on the launch-pad but the same is still remaining unconfirmed. 

Aruns dual identity remains a secret to most people, while he returns to his former life

== Cast ==
* Anil Kapoor as Arun Varma / Mr. India
* Sridevi as Seema Sahni
* Amrish Puri as Mogambo
* Satish Kaushik as Calender the Cook
* Annu Kapoor as Mr. Gaitonde, the Newspaper Editor
* Sharat Saxena as Daaga
* Ajit Vachani as Teja
* Ashok Kumar as Professor Sinha
* Anjan Srivastav as Baburam
* Bob Christo as Mr. Wolcott
* Harish Patel as Roopchand
* Aftab Shivdasani as Orphan
* Karan Nath as Orphan   Ahmed Khan as Orphan
 

==Soundtrack==
{{Infobox album |
 Name = Mr. India |
 Cover = Mr india anil.jpg |
 Type = Soundtrack |
 Artist = Laxmikant-Pyarelal |
 Recorded = | Hindi Film Soundtrack |
 Length = |
 Label =  T-Series |
 Producer = Laxmikant-Pyarelal | 
 Reviews = |
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! Track # !! Song !! Singer(s)
|-
| 1
| "Hawa Hawaii"
| Kavita Krishnamurthy
|-
| 2
| "Karte Hain Hum Pyaar Mr. India Se"
| Kishore Kumar, Kavita Krishnamurthy
|-
| 3
| "Kate Nahi Katate Ye Din Ye Raat"
| Kishore Kumar, Alisha Chinai
|- 
| 4
| Parody Song
| Anuradha Paudwal, Shabbir Kumar
|- 
| 5
| "Zindagi Ki Yahin"
| Kishore Kumar
|- 
| 6
| "Zindagi Ki Yahin" (Sad)
| Kavita Krishnamurthy
|}

== Reception ==

Described by Rediff as "one of the most iconic films of its time",    it became one of the highest grossing hits of the year  and also found a place in Hindustan Times list of Top 10 Patriotic Films of Hindi Cinema.  While the trade famously joked that the film should have been named Miss. India,  Rediff also stated that "Sri was a complete show-stealer in the film".  Sridevis iconic imitation of Charlie Chaplin in the film was described by Times of India as "the most hilarious act she has ever done".  Rediff also featured Sridevi in its list of Super Six Comic Heroines stating that "her mobile face expressions could give Jim Carrey sleepless nights" and that "her biggest plus point is her ability to be completely uninhibited in front of the camera".  The famous Hawa Hawai dance, cited by Times of India as "one of the unforgettable numbers of Sridevi",  also became a popular nickname for the actress.    Besides comedy, Anil Kapoor and Sridevi gave Hindi Cinema one of its sexiest rain songs  in the films chart-buster Kaate nahin kat te where Filmfare described Sridevi as "truly a goddess in a blue sari".  Times of India included the number in its Top 10 Hot n Sexy Songs countdown for featuring a "stunning Sridevi sizzling in a wet blue saree" and being "a trendsetter for erotic numbers to follow in Bollywood"  while iDiva described the song as "unparalleled in Hindi Cinema".  Rediff also featured the song in its list of Top 25 Sari Moments praising Sridevis "ability to look erotic even when shes covered from head to toe".  Box Office India states that with the success of Mr. India, Sridevi "continued her domination" over her contemporaries Jaya Prada and Meenakshi Sheshadri. 

== Awards ==
The movie could not be awarded that year since no award ceremony took place in the years 1987 and 1988. However, in 2013, Sridevi, the lead actress of the film, was given the Filmfare Special Award for her performances in Mr India as well as Nagina (film)|Nagina (1986).

== Sequel ==
In February 2012, it was announced that producer Boney Kapoor is planning a sequel to Mr. India. In March 2012, reports gave out that the film will be titled Mr. India 2 and features Anil Kapoor and Sridevi reprising their roles from the original film. Boney Kapoor also announced that Salman Khan and Arjun Kapoor had also joined the cast in negative roles. Mr. India 2 is to be shot in 3D film|3D. Filming has not yet begun, and as of March 2013, is in the scripting stage. 

== Popular culture ==
The movie is a recurring reference in the ongoing Indian soap  Madhubala - Ek Ishq Ek Junoon, which airs on Colors channel. Since the soap is partly a tribute to 100 years of cinema and Bollywood, there are various references to Bollywood films and influences. Mr India, in particular, is a great favourite with the protagonists, Superstar Rishabh Kundra (a.k.a. RK) and his wife Madhubala, and is used recurrently to depict the bond between them.

== Remakes == Tamil as En Rathathin Rathame, starring K. Bhagyaraj.  in Kannada as Jai Karnataka, starring Ambareesh.

==See also==
* Science fiction film of India

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 