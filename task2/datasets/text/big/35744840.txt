Yeh Khula Aasmaan
 
{{Infobox film
| name = Yeh Khula Aasmaan
| image = Yeh Khula Aasmaan.jpg
| caption = Theatrical release poster
| director = Gitanjali Sinha
| producer = Hemendra Aran
| writer = Gitanjali Sinha & Samarth Lahri
| music = Anand Milind Yashpal_Sharma Raj Tandon Anya Anand Manjusha Godse Kishor Nadalskar Nitin Kerur Aditya Siddhu Gulshan Pandey 
| released =  
}} Yashpal Sharma, Raj Tandon and Anya Anand.

== Plot ==
Avinash, a young intelligent boy experiences an extremely challenging phase of life due to his academic failures. He feels an immense vacuum which unfortunately his busy parents are unable to fill. In desperation, he visits his grandfather Dadu after several years.

Being the best kite runner of his era, Dadu uses the kite to subtly impart the lessons of life and prepares Avinash to face life.

The Movie is inspired by a True Story.

== Cast ==
*Raghubir Yadav Yashpal Sharma
*Raj Tandon
*Anya Anand
*Manjusha Godse
*Kishor Nadalskar
*Nitin Kerur
*Aditya Siddhu
*Gulshan Pandey

== Music ==

The Music composed by Anand-Milind, lyrics by Ravi Chopra was launched on April 25, 2012, a month before the films release at a star-studded gathering in Mumbai.  

The vocals on the various tracks are provided by Kunal Ganjawala, Suresh Wadekar, Swapnil Bandodkar, Gayatri Ganjawala, Raghubir Yadav, Soham and Amey Daate.

The album also marks a comeback for musical duo Anand-Milind(4 time Filmfare awardees) whose first release Yeh Khula Aasmaan is, in over 6 years.

== Awards ==

This award winning film is the sensitive story of a young boys journey as he faces up to the challenges life sends his way. As he turns to his grandfather, he discovers a bond that will change his life forever.

Kids First! Film and Video Festival - Winner 

International Film Festival South Africa - Selected

International Film Festival Ireland - Selected

Heart of England International Film Festival  - Selected

International Youth Film Festival - Selected

== References ==
  
 

== External links ==
*  
*  

 
 