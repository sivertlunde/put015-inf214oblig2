The Fortune Buddies
 
 
{{Infobox film
| name           = The Fortune Buddies
| image          = The Fortune Buddies poster.jpg
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 勁抽福祿壽
| simplified     = 劲抽福禄寿
| pinyin         = Jìng Chōu Fú Lù Shòu
| jyutping       = Ging6 Cau1 Fuk1 Luk6 Sau6}}
| director       = Chung Shu Kai
| producer       = Eric Tsang Tommy Leung
| writer         = Wong Yeung Tat Maggie Cheung Fala Chen Pauline Wong Michael Tse
| music          = 
| cinematography = 
| editing        =  Television Broadcasts Limited Sil-Metropole Organisation Sun Wah Media Group
| distributor    = Intercontinental Film Distributors
| released       =  
| runtime        = 92 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          =
}} Maggie Cheung, Television Broadcasts Limited and Sil-Metropole Organisation. A production start ceremony was held on 17 June 2011 at the Kowloonbay International Trade & Exhibition Centre. {{cite web|url=http://hongkongmorning.com/the-fortune-buddies-starts-production.html|title=
The Fortune Buddies starts production}}  The film was released on 11 August 2011.

==Cast==
{|class="wikitable" width="70%"
|-
! Cast !! Role !! Description
|-
| Eric Tsang || || Hoi Keis father
|-
| Fiona Sit || Hoi Kei (Fiona) 薛琪 || Real estate broker Eric Tsang and Pauline Wongs daughter Wong Luk Lams girlfriend Mak Ling Lings subordinate
|-
| Louis Yuen || Yuen Fuk Cheung 阮福祥 || Wong Luk Lam and Lee Chit Saus friend Sister Mans ex-husband Cheung Chais father
|-
| Wong Cho-lam || Wong Luk Lam (Blue) 王祿藍 || Hoi Keis boyfriend Yuen Fuk Cheung and Lee Chit Saus friend
|-
| Johnson Lee || Lee Chit Sau 李捷壽 || Yuen Cheung Fuk and Wong Luk Lams friend Chi King Haps boyfriend
|- Maggie Cheung || Maggie || Eric Tsangs ex-girlfriend WWV International Wrestling Organization representative
|-
| Fala Chen || Sister Man 蚊姐 || Yuen Fuk Cheungs ex-wife Cheung Chais mother
|-
| Pauline Wong || || Hoi Keis mother
|- Turning Point
|-
| Bosco Wong || Ben ||
|-
| Richard Ng || || Lee Chit Saus father
|-
| Kristal Tin || || Portland Street Film Queen
|-
| Mak Ling Ling || || Hoi Keis superior
|-
| Michelle Lo || || Kee Wah Bakery clerk
|-
| Lam Suet || ||
|-
| King Kong || || Japanese general
|-
| Tin Kai-Man || || Labor Department chairman
|-
| Susan Tse || ||
|-
| Henry Lo || ||
|-
| Bob Lam || ||
|-
| Oscar Leung || || Labor Department job seeker
|-
| Macy Chan || || Maggies assistant
|-
| Sire Ma || || Labor Department worker
|-
| Joe Yau || || Brother Wahs assistant
|-
| Samantha Ko || Chi King Hap 紫荊俠 || Lee Chit Saus girlfriend
|-
| Auston Lam || ||
|-
| Oil Chan || ||
|-
| Tina Shek || ||
|-
| Evergreen Mak || ||
|}

==References==
 

 
 
 
 
 
 
 
 

 
 