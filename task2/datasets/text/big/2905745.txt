Kya Kehna
{{Infobox film
| name = Kya Kehna
| image = Kyakehna.jpg
| director = Kundan Shah
| producer = Kumar S Taurani Ramesh S Taurani
| writer = Honey Irani
| screenplay = Honey Irani
| starring = Preity Zinta Saif Ali Khan Chandrachur Singh Anupam Kher Farida Jalal
| lyricist = Majrooh Sultanpuri
| music = Rajesh Roshan
| distributor = Tips Music Films
| released =  
| runtime = 155 min
| language = Hindi
| country = India
| budget =
}} 2000 Bollywood film starring Saif Ali Khan, Preity Zinta, Chandrachur Singh, Anupam Kher and Farida Jalal. It was directed by Kundan Shah and premiered on 19 May 2000.

The film was a hit  and established Zintas career as an actress. The film dealt with the taboo issue of pre-marital pregnancy and the views of society.

==Plot==
The story is about Priya Bakshi (Preity Zinta), the daughter of Gulshan (Anupam Kher) and Rohini Bakshi (Farida Jalal). Priya has her parents, brothers and best friend, Ajay (Chandrachur Singh) who all love and support her. She enters her first year in college and catches the eye of Rahul (Saif Ali Khan) who quickly becomes attracted to her. She succumbs to his charm but her brothers and parents are not sure about Rahul. His womanizing nature worries them and they tell her to stay away from him. However, Priya is unable to leave Rahul. She convinces her parents to meet him but when they talk about marriage he backs off and he leaves Priya. Priya is devastated but moves on with her life. In one sweep she learns that she is pregnant with Rahuls child. Her parents go to Rahul to talk about marriage but to their shock he refuses to marry her. Priya is faced with a decision, and she chooses to keep the child. Her decision prompts her father to chuck her out the house. Alone and neglected Priya is devastated, but her parents and brothers find it hard to live without her and they bring her home. She now has to face society and try to overcome the disdain of her neighbors, among others. Society virtually ostracizes her and her family. She eventually gains the support of those who had earlier disrespected her. At the same time she manages to change Rahuls views as well and he decides to marry her. However, Priya has already given her heart to Ajay and they get married.

==Cast==
* Preity Zinta as Priya Bakshi
* Saif Ali Khan as Rahul Modi
* Chandrachur Singh as Ajay
* Anupam Kher as Gulshan Bakshi 
* Farida Jalal as Rohini Bakshi
* Puneet Vashist as Priyas brother
* Nivedita Bhattacharya as Priyas Sister-In-Law

==Music==
{{Infobox album 
| Name        = Kya Kehna
| Type        = soundtrack
| Artist      = Rajesh Roshan
| Cover       = KyaKehnaCD.jpg
| Released    =  
| Recorded    = 
| Genre       = Film soundtrack
| Length      = 
| Label       = Tips Music Films
| Producer    = Rajesh Roshan
}}
The soundtrack of the film contains 8 songs. The music is conducted by the award-winning composer Rajesh Roshan, with lyrics by Majrooh Sultanpuri. The title song is a copy of the 1960 song Oh! Carol by Neil Sedaka.

{{Track listing
| extra_column = Artist(s)

| title1 = Ae Sanam Meri Bahon  | extra1 = Alka Yagnik, Kumar Sanu | length1 = 06.13
| title2 = Dekhiye Aji Jaaneman | extra2 = Alka Yagnik Udit Narayan | length2 = 05:30 Hariharan | length3 = 06:15
| title4 = In Kadmon Ke Neeche  | extra4 = Alka Yagnik, Kumar Sanu | length4= 05:19
| title5 = Jaaneman Jaane Jaan  | extra5 = Alka Yagnik, Sonu Nigam | length5= 05:35
| title6 = Kya Kehna            | extra6 = Kavita Krishnamurthy, Hariharan | length6= 04:27
| title7 = O Soniye Dil Jaaniye | extra7 = Alka Yagnik, Kumar Sanu, Sonu Nigam | length7=  05:41
| title8 = Pyara Bhaiya Mera    | extra8 = Alka Yagnik, Kumar Sanu | length8 = 04:40
}}

==Reception== The Tribune wrote, "The movie itself has several flaws but the issues that it tackles are real. The director takes a straight, hard look at the problems of today and holds them up for exhibition. The Indian tendency is to sweep uncomfortable issues under the carpet but if films like Kya Kehna continue to be made, at least, well be forced to confront them."  Mimmy Jain of The Indian Express, in a positive review, commeded Shah for making "a sensitive film, on a sensitive subject", calling Kya Kehna "a film that should have been made years ago". She further noted Zinta as being "dazzlingly good" in her part. 

Filmfare, in a three-star review, wrote that the film is "worth a watch" and attributed its success to Zintas "very convincing performance".  Sharmila Taliculam from Rediff.com was critical of the film, concluding, "Apart from the climax, the film is no big deal." 

==Awards==
* Filmfare Best Story Award - Honey Irani IIFA Award for Technical Excellence - Best Screenplay - Honey Irani
* Sansui Best Actress Award - Preity Zinta

==References==
 

== External links ==
*  

 

 
 
 
 