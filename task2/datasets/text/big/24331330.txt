Tales of Hoffman (film)
 
{{Infobox film
| name           = Tales of Hoffman
| image          =
| caption        =
| director       = Richard Oswald
| producer       =
| writer         =  }} E. T. A. Hoffmann Richard Oswald
| starring       = Kurt Wolowsky
| cinematography = Ernst Krohn
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Germany
| language       = Silent with German intertitles
| budget         =
}}
 silent German drama film directed by Richard Oswald. An incomplete print exists. 

==Cast==
* Kurt Wolowsky as young Hoffmann
*   as Uncle
* Paula Ronay as Aunt
* Werner Krauss as Conte Dapertutto
* Friedrich Kühne as Coppelius, eyeglass merchant
* Lupu Pick as Spalanzani, Museum Director Ernst Ludwig as Counsillor Crespel
* Nelly Ridon as Mrs Crespel
* Ruth Oswald as little Antonia
* Andreas von Horn as Dr. Mirakel
* Erich Kaiser-Titz as E.T.A. Hoffmann
* Ferdinand Bonn as Municipal Counsillor Lindorf
*   as Actress
* Alice Hechy as Olympia, Automaton (as Alice Scheel-Hechy)
* Thea Sandten as Giulietta
* Louis Neher as Schlemihl

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 
 