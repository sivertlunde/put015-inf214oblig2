Jeete Hain Shaan Se
{{Infobox film
| name           = Jeete Hain Shaan Se
| image          = JeeteHainShaanSefilm.jpg
| image_size     = 
| caption        = 
| director       = Kawal Sharma
| producer       = Smt P. Bhagyam
| writer         = Abhilash, Raaj Jallandhary
| narrator       =  Govinda Mandakini Mandakini
| music          = Anu Malik
| cinematography = S. Pappu 
| editing        = Vijay Pande, A. Sanjeevi    
| distributor    = Shree Bhagyalakshmi Movies
| released       = 4 August 1988
| runtime        = 
| country        =  Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
  Govinda in leading roles. The movie was hit.
==Plot==
Johnny (Mithun Chakraborty), Govinda (Sanjay Dutt) and Iqbal Ali (Govinda (actor)|Govinda) are close friends who live in a small community in Bombay. The threesome get together to help each other and other needy people in the community and, hence, get in the bad books of a local gangster who calls himself DK. Tragedy strikes Govinda when his mom passes away. At the time of the funeral, Govinda is reunited with his dad, Advocate Verma, who has been missing for years. Then a police inspector gets Johny to meet his biological mother, Mary. 

The three friends rejoice over this good fortune. Then Johny finds out that Verma was behind the atrocities that were inflicted on his mom years ago, and he goes to confront Verma, only to come against an enraged Govinda, who cannot believe his father is capable of committing any atrocities against anyone. It is now up to Iqbal to find a solution to end the enmity between Govinda and Johny, before DK takes advantage of this situation.

==Cast==
*Mithun Chakraborty as Johnny
*Sanjay Dutt as Govinda  Govinda as  Iqbal Ali  Mandakini as  Julie 
*Vijeta Pandit as  Kiran  
*Danny Denzongpa as  Balwant 
*Paintal as  Drunk 
*Narendra Nath   
*Tiwari   
*Monty Nath as  Dawood  
*Sudhir as  Babu 
*Rajeev   
*Satyendra Kapoor as  Advocate Verma  
*Ashalata as Geeta Verma 
*Gita Siddharth as  Mary 
*C.S. Dubey   
*Yunus Parvez as the Defence Council 
*Viju Khote as the Drunk 
*Mushtaq Merchant   
*Raj Tilak   
*Krishnakant as Rahim Bhai 
*Manik Irani as Gullu  
*Gur Bachchan Singh as Goga  
*Pramod A. Dubey

==Trivia==
The famous song in the film, "Julie Julie Johnny ka dil tujh pe aiya julie", is sung by notable Music Director Anu Malik and singer Kavita Krishnamurthy.

== External links ==
*  

 
 
 

 