100 Days Before the Command
{{Infobox Film
| name           = 100 Days Before the Command
| image          = 100DaysBefore.jpg
| caption        = DVD cover
| director       = Hussein Erkenov
| producer       = Aleksandr Zosimenko
| writer         = Vladimir Kholodov Yuri Polyakov
| narrator       =
| starring       = Vladimir Zamansky Armen Dzhigarkhanyan Oleg Vasilkov Roman Grekov Valeri Troshin Aleksandr Chislov Mikhail Solomatin
| music          =
| cinematography = Vladislav Menshikov
| editing        = Galina Dmitriyeva Vladimir Portnov
| distributor    = Peccadillo Pictures
| released       =  
| runtime        = 67 minutes
| country        = Soviet Union
| language       = Russian
| studio         = Gorky Film Studio
| budget         =
}} 1990 drama film by Hussein Erkenov.

Made in the final months of the Soviet Union, the film follows three young Red Army recruits, Zyrin, Belikov and Elin. The film has no narrative structure and rather than telling a story uses vignettes to show the conditions in which Soviet army recruits lived. The film is often homoerotic, the soldiers are shown to have very little privacy and are forced into such intimate acts as washing each other. 

==Cast==
* Vladimir Zamansky as The Unknown Man
* Armen Dzhigarkhanyan as Brigade Commander
* Oleg Vasilkov as Elin
* Roman Grekov as Zub
* Valeri Troshin as Kudrin
* Aleksandr Chislov as Zyrin
* Mikhail Solomatin as Belikov
* Sergei Romantsov as Titarenko
* Sergei Bystritsky as Senior Lieutenant
* Yelena Kondulainen as Death
* Oleg Khusainov as Angel Sergei Semyonov as Captain

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 
 