Wayne's World (film)
 
{{Infobox film
| name           = Waynes World
| image          = Waynes world ver2.jpg|
| caption        = Theatrical poster
| alt            = 
| director       = Penelope Spheeris
| screenplay     = {{Plain list | 
* Mike Myers Bonnie Turner Terry Turner
}}
| based on       =  
| starring       = {{Plain list | 
* Mike Myers
* Dana Carvey
* Rob Lowe
* Tia Carrere
}}
| producer       = Lorne Michaels
| music          = J. Peter Robinson
| cinematography = Theo van de Sande Malcolm Campbell NBC Films
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States English  Cantonese
| budget         = $20 million
| gross          = $183,097,323
}} sketch of the same name on NBCs Saturday Night Live.  
 films based on Saturday Night Live skits. It was filmed in 34 days. 

Waynes World was Myers feature film debut. The film also featured  ), Chris Farley, Ed ONeill, Ione Skye, Meat Loaf, and Alice Cooper. Upon its release, Waynes World received generally positive reviews and was a box office success. A sequel, Waynes World 2, was released on December 10, 1993. In 1993, readers of Total Film magazine voted Waynes World the 41st-greatest comedy film of all time.

==Plot==
 
In Aurora, Illinois, twentysomething rock and roll enthusiasts Wayne Campbell and Garth Algar host a cable access television show called Waynes World, in which they lampoon eccentric locals and discuss topics of interest that include music and beautiful women; in spite of the shows low budget, it has proven quite popular in the Aurora area. One day, Benjamin Kane, a television producer, discovers Waynes World while visiting his girlfriend; after learning of the shows popularity, he has his assistant Russell Finley track down where Waynes World is taped.
 talks to the audience and admits his doubts on the shady deal, but he is too shy to say anything to Wayne, and the pair accept the offer. Using their newfound wealth, Wayne and Garth attend a local night club, where they avoid Waynes psychotic ex-girlfriend Stacy, who continually tries to get back together with him, and Wayne sees Cassandra Wong, the lead vocalist and guitarist of the band Crucial Taunt, who are playing on stage. He instantly becomes smitten with her and the two hit it off. In order to impress Cassandra, Wayne learns to speak Cantonese and the two share a conversation as Stacy, still following him tries to make him jealous by being with another man, which ends up in disaster. He also purchases a guitar he had been eyeing for a long time.

Benjamin also becomes attracted to Cassandra, and uses his wealth and good looks to try to win her over. Using a pair of tickets to an Alice Cooper concert to get them out of the way, he offers Cassandra a role in a music video. At the concert, Wayne and Garth meet the bodyguard of Frankie Sharpe, producer of Sharpe Records, and gain information which they could possibly use later. The two of them are initially fazed when they go to their first recording of Waynes World at a television studio, continually implanting the films product endorsements as Benjamin talks to them. When the show starts, they are required to speak with their big sponsor Noah Vanderhoff, who owns a franchise of video arcade|arcades, which true to form, Wayne breaks Benjamins rules by ridiculing Vanderhoff with a series of notes written on the backs of his cards. Wayne is called up to the booth and is fired on the spot from his own show. Wayne leaves, and Garth, developing a case of stage fright freezes up on camera. Following that, Wayne and Garth get into a heated argument, causing a rift in their friendship. Wayne also becomes jealous of Benjamin moving in on Cassandra and tries to forbid her from participating in the music video. Furious at not trusting her, Cassandra breaks up with Wayne.
 rectal search on him. Broadcasting from Waynes house, Waynes World successfully hacks into Sharpes satellite television and he listens to Cassandra play. As her song comes to an end, Sharpe and Benjamin converge in Waynes basement; unfortunately, Sharpe, while impressed by Cassandra is unable to sign her on, giving Benjamin a cruel satisfaction as Cassandra breaks up with Wayne again and leaves with Benjamin to a tropical resort. Meanwhile, Stacy admits she has been so moody because she is pregnant with Waynes child, and a fire starts amongst the camera equipment and burns the house down while Wayne carries Garth from the inferno. Unsatisfied with the "bad ending" they reenact the scene again, only this time they unmask Benjamin as "Old Man Withers" in a Scooby Doo ending. They then reenact it again in a "Mega Happy Ending" where Cassandra is signed on to a record contract, Russell learns that "platonic love can exist between two grown men" and Benjamin discovers that "money, great hair, and a perfect body can get you far in America; almost to the top, but it cant get you everything." before they all break out in "fish face" and begin having fun.

==Cast==
* Mike Myers as Wayne Campbell
* Dana Carvey as Garth Algar 
* Tia Carrere as Cassandra Wong 
* Rob Lowe as Benjamin Kane
* Lara Flynn Boyle as Stacy, Waynes crazy ex-girlfriend
* Michael DeLuise as Alan, one of Wayne and Garths crew
* Lee Tergesen as Terry, Wayne and Garths head cameraman
* Dan Bell as Neil, Waynes and Garths other crewman
* Sean Gregory Sullivan as Phil, Wayne and Garths perpetually wasted friend who works at an auto repair shop
* Brian Doyle-Murray as Noah Vanderhoff, a video arcade magnate
* Colleen Camp as Mrs. Vanderhoff
* Kurt Fuller as Russell Finley, Benjamins assistant
* Chris Farley as the unnamed well-informed security guard at the back of Alice Cooper concert
* Meat Loaf as Tiny
* Frank DiLeo as rock promoter Frankie Mr. Big Sharp
* Ed ONeill as Glen, the manager at Stan Mikitas Donuts
* Mike Hagerty as Davey, a controller at the Cable 10 television station whom Benjamin and Russell ask for help, he later claims that he got laid off while at Stan Mikitas.
* Frederick Coffin as Officer Koharski
* Donna Dixon as Garths dream woman
* Ione Skye as Elyse, Benjamins girlfriend, who introduces him to Waynes World
* Robin Ruzan as a waitress
* Charles Noland as Ron Paxton, who tries to market his invention, the "Suck-Kut", to Garth
* Carmen Filpi as Old Man Withers
* Chaz Healy as Concert-goer
* Robert Patrick as T-1000
* Alice Cooper with Pete Friesen, Derek Sherinian, Stef Burns,   and Jimmy DeGrasso as themselves.

==Reception==
The film received positive reviews. Rotten Tomatoes gives a "Certified Fresh" score of 85% based on reviews from 46 critics. 

  

=== Awards ===
  
American Film Institute recognition:
* AFIs 100 Years... 100 Laughs - Nominated 
* AFIs 100 Years...100 Movie Quotes:
** "Schwing!" - Nominated 
** "Were not worthy. Were not worthy." - Nominated 
* AFIs 100 Years... 100 Songs:
** Bohemian Rhapsody - Nominated  

===Box office===
The movie was a box office success debuting at No.1.   The films final domestic gross was $121,697,323. 

==Effect on pop culture==
 

Filled with pop culture references, the sketches and film started catchphrases such as "Schwing!" and "Sarcasm|Schyea", as well as popularizing "Thats what she said", "Party on!" and the use of "...Not!" after apparently affirmative sentences in order to state the contrary. 

The film frequently breaks the fourth wall, with Wayne, Garth, and others on occasion speaking directly to the audience and even the cameraman. Parts of the story are carried by Waynes narration to the camera, in which he offers his thoughts on whats happening in the film. Despite Wayne, Garth, Cassandra, Glen, and Ben addressing the viewer, no one else seems aware that they are in a film.
 

===Video games===
 
In 1993, Waynes World video games were released for the Nintendo Entertainment System, Super Nintendo Entertainment System, the Sega Mega Drive, and the Game Boy. The plot of the games  differs from the film. In the Super NES and Mega Drive versions, the player controls Wayne as he goes on a mission throughout Aurora &ndash; visiting The Gas Works, Stan Mikitas, and Cassells Music, the music store from the "No Stairway" scene, among other locations &ndash; to rescue Garth from inside the "Zantar the Gelatinous Cube" arcade game mentioned in the film.

Alternatively, an adventure game version of Waynes World was released around the same time for DOS. The plot involves Wayne and Garth trying to raise money to save their show by holding a "pizza-thon".
 Sonic the Hedgehog playing behind Noah Vanderhoff, the owner of the Noahs Arcade franchise.

In addition,   and Grand Theft Auto V feature a car based on the AMC Pacer named "Rhapsody" in reference to the famed scene from the film. In The Lost and Damned, if the player zooms in on the dashboard with the sniper rifle, they can see a pixelated photograph resembling Wayne and Garth.

===Music===
 
 
 soundtrack album reached number one in the Billboard magazine|Billboard album charts. The studio wanted to use a Guns N Roses track for the scene but Myers fought extremely hard to feature the Queen song, even threatening to quit the production unless it got in.   Freddie Mercury, the lead singer of Queen, died of bronchial pneumonia resulting from AIDS a few months before the films release.

* Gary Wright re-recorded "Dream Weaver" for this film and it was used whenever Wayne looked at Cassandra.

* Tia Carrere sang all her own vocals on songs she performed in the film, and her cover version of songs, such as The Sweet|Sweets "The Ballroom Blitz", were included in the films original soundtrack recording.

* Myers originally wanted Alice Coopers " . 

==See also==
  
* Recurring Saturday Night Live characters and sketches
* List of Saturday Night Live feature films

==References==
 

==External links==
 
 
*  
*  
*   at Rotten Tomatoes

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 