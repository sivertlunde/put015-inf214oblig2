Young Fury
{{Infobox film
| name           = Young Fury
| image          = Young Fury poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Christian Nyby
| producer       = A. C. Lyles Steve Fisher
| story          = Steve Fisher A. C. Lyles
| starring       = Rory Calhoun Virginia Mayo William Bendix Lon Chaney Jr. Richard Arlen John Agar
| music          = Paul Dunlap
| cinematography = Haskell B. Boggs 
| editing        = Marvin Coil 
| studio         = A.C. Lyles Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film Steve Fisher. The film stars Rory Calhoun, Virginia Mayo, William Bendix, Lon Chaney Jr., Richard Arlen and John Agar. The film was released in February 1965, by Paramount Pictures.  

==Plot==
 

== Cast ==	
*Rory Calhoun as Clint McCoy
*Virginia Mayo as Sara McCoy
*William Bendix as Blacksmith, Joe
*Lon Chaney Jr. as Bartender, Ace 
*Richard Arlen as Sheriff Jenkins
*John Agar as Dawson
*Preston Pierce as Tige McCoy
*Linda Foster as Sally Miller
*Robert Biheller as Biff Dane
*Jody McCrea as Stone
*Merry Anders as Alice Marc Cavell as Pancho
*Jerry Summers as Gabbo
*Jay Ripley as Slim
*Kevin ONeal as Curley
*Dal Jenkins as Sam
*Fred Alexander as Pony
*Rex Bell Jr. as Farmer
*Joan Huntington as Kathy
*Regis Parton as Deputy Willie
*William Wellman Jr. as Peters
*Steve Condit as Tim
*Sailor Vincent as Merchant 
*Jorge Moreno as Mexican
*Bill Clark as Brady
*Dave Dunlop as Smith
*Jesse Wayne as Member of The Hellion Gang
*Bob Miles as Member of The Hellion Gang 
*Eddie Hice as Member of The Hellion Gang
*Fred Krone as Member of The Hellion Gang
*Joe Finnegan as Member of The Hellion Gang
*Kent Hays as Member of The Hellion Gang 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 