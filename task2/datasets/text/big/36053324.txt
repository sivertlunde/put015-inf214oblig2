Rajakali Amman
{{Infobox film
| name           = Rajakali Amman
| image          =
| director       = Rama Narayanan
| producer       = Kavithalayaa Productions Pushpa Kandaswamy
| writer         =
| narrator       =
| starring       = Karan (actor) Ramya Krishnan Kausalya (actress) Nizhalgal Ravi Vadivelu Y.Vijaya Saranraj Shivani
| music          = S. A. Rajkumar
| cinematography = N.K. Viswanathan
| editing        = Raajgeerthi
| distributor    = Aadhi Bhagavan Films
| released       =  
| runtime        = 119 minutes
| country        = India Tamil
| budget         =
}} Tamil language released in 2000. Singers for this soundtrack include Mano, K.S. Chitra, Swarnalatha, Krishnaraj and Actor Vadivelu. This movie has extensive graphics to recreate the mythological story of Hindu Goddess Kali or Amman in Tamil.

== Plot ==
A simple minded women named Meena her brother Gopal and their pet snake, lives will soon be changed when they look out for the village goddess who is forced out of her temple in a nearby village. They all flourish because of the blessings of the maa but these days are over when Meena weds an evil man named Ramesh, who is not who he really says he is, and without the goddesses blessings.

== Cast ==
* Ramya Krishnan as Devi Maa Durga / Devi Maa Kali Karan as Shankar/Ramesh Kausalya as Meena
* Vadivelu as Gopal
* Vijaya Y. as Sandya
* Rama Narayanan

== Soundtrack ==

{{Infobox album  
| Name        = Raja Kaliamman
| Type        = soundtrack
| Artist      = S. A. Rajkumar
| Cover       =
| Caption     = Raja Kaliamman
| Released    =  
| Recorded    = 
| Genre       = Film Soundtrack
| Length      =  Tamil
| Label       = 
| Music by    = S. A. Rajkumar
}}
Kalyanam  
Om Sakthi  
Sandhana  
Santhana Malligai  
Thangkachi

== External links ==
* 

 

 
 
 
 
 
 

 