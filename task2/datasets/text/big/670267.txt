Suburbia (film)
 
{{Infobox film
| name           = Suburbia
| image          = Suburbia (film).jpg
| caption        = Video release cover
| director       = Penelope Spheeris
| producer       = Bert Dragin Roger Corman
| writer         = Penelope Spheeris Chris Pedersen Derek OBrien Michael "Flea" Balzary 
| music          = Alex Gibson
| cinematography = Timothy Suhrstedt
| editing        = Ross Albert
| distributor    = New World Pictures
| released       =  
| runtime        = 94 minutes
| rating         = R
| country        = United States
| awards         =
| language       = English
| budget         = $500,000 Chris Nashawaty, Crab Monsters, Teenage Cavemen and Candy Stripe Nurses - Roger Corman: King of the B Movie, Abrams, 2013 p 189 
}} punks who punk lifestyle Chris Pedersen, Bill Coyne, Timothy OBrien and Red Hot Chili Peppers bassist Flea (musician)|Flea, amongst others. 

Director Penelope Spheeris recruited street kids and punk rock musicians to play each role, rather than hire young actors to portray punk rockers.
 Downey and Norwalk in CA 42 was to its north, with Alondra Bl to its south.
The entire area was Eminent Domain starting in the late 1960s / early 1970s, wherein it sat mostly-vacant until its demolition in c.1990; some houses still had inhabitants up until c.1980. This was a gang-infested area; many abandoned houses were "drug houses", or, just as in the film, "crash houses" (e.g. the "T.R. House" ). Interstate 105 (California)|I-105 now occupies most of the property, and has since the early 90s.  
 Over the Edge." 

The movie contains live footage of D.I. (band)|D.I. performing "Richard Hung Himself", T.S.O.L. performing "Wash Away" and "Darker My Love," and The Vandals performing "The Legend of Pat Brown". In turn, the movie inspired the Pet Shop Boys song "Suburbia (song)|Suburbia."

The film is a part of Shout Factorys Roger Corman Cult Classics series, reissued on DVD in May 2010.

==Plot==
A hitchhiking teenage runaway, Sheila (Jennifer Clay), is picked up on Interstate 605 in the Greater Los Angeles Area by a woman with a toddler. When the car gets a flat tire, they find a telephone booth on the edge of an abandoned tract housing district. While the mother is on the phone, the toddler is attacked and killed by a stray dog.
 Chris Pedersen) branded with the letters T.R., for "The Rejected", but winds up coming back and accepting the brand after discovering his father is homosexual. He begins to form a romantic relationship with Sheila, who has also moved into the house.

The next morning, several men from "Citizens Against Crime", including Jim Tripplett (Lee Frederick) and Bob Skokes (Jeff Prettyman), drive through the neighborhood shooting at the packs of wild dogs that roam the area. T.R. kids Razzle (Flea (musician)|Flea) and Skinner (Timothy OBrien) confront them, but the situation is defused by Jacks African-American stepfather, police officer Bill Rennard (Donald Allen). Jack, Evan, and Skinner steal food for the house by raiding the garages of a nearby suburban neighborhood, and make further enemies of Jim and Bob by disrupting their garage sale. When Evan sees on the news that his mother has been arrested for drunk driving, he collects his younger brother Ethan (Andrew Pece) and brings him to live at T.R. House, where Sheila gives him a mohawk hairstyle|mohawk. Sheila admits to Joe that she was physically and sexually abused by her father.

During a T.S.O.L. concert, the T.R. gang get into a fight defending Skinner. The men they were fighting with enter the concert and stab a security guard, framing the T.R. kids for the crime by using the knife to hang a flier with "T.R." written in blood. Jim and Bob next witness the T.R. crew vandalizing a convenience store. At a meeting of the Citizens Against Crime, they accuse Bill and the rest of the police of not doing enough to curb the teenagers criminal behavior, declaring their willingness to take the law into their own hands. Bill goes to T.R. House and implores the teens to stay out of trouble. That night, Jim and Bob invade the house and threaten the teens, assaulting Sheila in the process. The next morning, the kids find that Sheila has killed herself by overdosing on Keefs drugs. Not knowing what to do, they bring her body back to her parents. When the T.R. kids come to the funeral, Sheilas father (J. Dinan Myrtetus) insists that they leave. Joe reveals his knowledge of Sheilas abuse, and a fight breaks out, hospitalizing Sheilas father.
 Vandals concert that night, Bill shows up and warns the T.R. kids to clear out of T.R. house immediately, before their actions bring the Citizens Against Crime down on their heads, but they decide to stay. Learning of the violence at the funeral, Jim and Bob show up at the house and are attacked by the teens, who drive them off. They bring their car back around for another pass, accidentally running over and killing Ethan. Bill arrives, but is too late to prevent the tragedy.

==References==
 

== External links ==
*  
*  

 

 
 

 
 