The List (2012 film)
 
{{Infobox film
| name           = The List
| director       = Beth Murphy
| producer       = Beth Murphy  Sean Flynn
| music          = John Califra
| cinematography = Kevin Belli
| editing        = Kevin Belli
| completed       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
}}

The List is a 2012 documentary film produced and directed by Beth Murphy. The List premiered at the Tribeca Film Festival on April 21, 2012.

==Synopsis==
The List is a modern-day Oskar Schindler story that focuses on Kirk W. Johnson, a young American fighting to save thousands of Iraqis whose lives are in danger because they worked for the U.S. to help rebuild Iraq. After leading reconstruction teams in Baghdad and Fallujah, Kirk returns home only to discover that many of his former Iraqi colleagues are being killed, kidnapped or forced into exile by radical militias. Frustrated by a stagnating government bureaucracy in the U.S. that has failed to protect its Iraqi allies, Kirk begins compiling a list of their names and helps them find refuge and a new life in America.

Over the course of 4 years, the film traces the evolution of Kirk’s unlikely humanitarian mission, while revealing the personal struggles of several Iraqis on Kirk’s List who are living in hiding and stranded in countries that don’t want them. Yaghdan and Ibrahim, two of Kirk’s friends and former colleagues who fled Iraq after receiving death threats, are among the first of several hundred Iraqis to make it to the United States. As Kirk travels throughout the Middle East with refugee lawyer Chris Nugent, we hear from a chorus of other U.S.-affiliated Iraqis who share heartbreaking stories of sacrifice and betrayal.

In the end, Kirk has helped more than a thousand “Iraqi allies” reach the safety of American shores. But with thousands remaining on his list, he is left to confront the limitations of humanitarian action. How many people can he save? Where does his commitment to the Iraqi people end? When can he leave the war behind? 

==References==
 
 

==See also==
*List Project

==External links==
* http://thelistproject.org/
* http://www.thelistfilm.com

 
 
 
 
 
 
 
 


 