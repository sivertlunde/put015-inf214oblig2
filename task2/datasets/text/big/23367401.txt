The Beloved Vagabond (1936 film)
{{Infobox film
| name = The Beloved Vagabond
| image =
| image_size =
| caption =
| director = Curtis Bernhardt
| producer = Ludovico Toeplitz 
| writer = William J. Locke (novel)   Greta Heller   Wells Root    Arthur Wimperis   Walter Creighton   Hugh Mills   Curtis Bernhardt
| narrator =
| starring = Maurice Chevalier   Betty Stockfeld   Margaret Lockwood   Desmond Tester
| music = Darius Milhaud
| cinematography = Franz Planer
| editing = Douglas Myers
| studio = Toeplitz Productions  Associated British (UK)   Columbia Pictures (US)
| released = 25 August 1936  
| runtime = 78 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} British musical musical drama independent producer Ludovico Toeplitz.

==Cast==
* Maurice Chevalier as Gaston de Nerac Paragot  
* Betty Stockfeld as Joanna Rushworth  
* Margaret Lockwood as Blanquette  
* Desmond Tester as Asticot  
* Austin Trevor as Count de Verneuil  
* Peter Haddon as Major Walters   Charles Carson as Charles Rushworth  
* Cathleen Nesbitt as Mme. Boin  
* Barbara Gott as Concierge  
* Amy Veness as Cafe Owner  
* D.J. Williams (actor)|D.J. Williams as Undertaker  
* C. Denier Warren as Railway Clerk

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==References==
 

==External links==
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 