The Tenth Straw
 

 
{{Infobox film
| name           = The Tenth Straw
| image          = 
| image_size     =
| caption        = 
| director       = Robert G. McAnderson
| producer       = 
| writer         = 
| based on       = 
| narrator       =
| starring       = 
| music          =
| cinematography = Claud C. Carter
| editing        = 
| studio = Pacific Films
| distributor    = 
| released       = 27 March 1926 
| runtime        = six reels
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Tenth Straw is a 1926 Australian silent film heavily inspired by the novel For the Term of His Natural Life. Little is known of the director and cast, but most of the film survives today. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 130. 

==Plot==
Aristocrat Bruce Lowe is convicted for a crime he did not commit and is transported from England to Australia. An army officer, Matthew Marr, pretends to be a friend of Lowes to gain access to his fortune and seduce his sister, Marie. On board ship, Lowe stands up to a bully and gains a friend in Richard Groves. Lowe escapes from prison, and heads to the bush. Some aboriginals discover a goldfield. Lowe proves his innocence, and Marr is arrested. 

==Cast==
*Peggy Paul as Marie Lowe
*Ernest Lauri as Bruce Lowe
*James Cornell as Matthew Marr
*Jack Fisher as Richard Groves
*Syd Everett as Bully Carey
*Robert Ball as Tiddley Harris
*Robert G. McAnderson as Major Orville

==Reception==
Contemporary reviews noted the similarities of the story to For the Term of His Natural Life. 

==References==
 

==External links==
* 
*  at the National Film and Sound Archive

 
 
 
 
 
 


 