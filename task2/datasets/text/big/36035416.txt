Decoy (1995 film)
{{Infobox film
| name         = Decoy
| image        = Decoy 1995 Film Poster.jpg
| image_size   = 
| caption      = American poster for "Decoy"
| genre        = Action, Thriller
| runtime      = 90&nbsp;minutes
| creator      = 
| director     = Vittorio Rambaldi
| producer     = Antonio F. Cortese Gary D. Kaufman Barry L. Collier (executive producer) Carlo Rambaldi (executive producer) Kevin DeWalt (associate producer, co-producer) Rob King (co-producer) Vincent Di Paolo (associate producer) Lloyd Martell (associate producer)
| writer       = Robert Sarno
| starring     = Peter Weller Robert Patrick
| editing      = Frank Irvine Jay Miracle
| music        = Mark Adler
| studio       = F.Y.D.O.E. Films, Floyd Group, Minds Eye Entertainment, Minds Eye Pictures
| distributor  = Prism Entertainment Corporation, Turner Home Video (USA VHS), Videosonic Arts 
| budget       = $4,000,000 (estimated)
| country      = United States
| language     = English
| network      = 
| released     = 1993
}}
Decoy is an action film starring Peter Weller, and Robert Patrick, directed by Vittorio Rambaldi. The film was released in 1995. 

==Background==
The film stars Peter Weller as Baxter and Robert Patrick as Travis. Others in the film included Charlotte Lewis as Katya, Darlene Vogel as Diana, Peter Breck as Wellington, Scott Hylands as Jenner and Vladimir Kulich as Daniel. 

With an estimated budget of $4,000,000, the filming dates ranged between July 19, 1994 and August 17, 1994.  The films video premiere was in late 1995 for both Japan and America, whilst in 1996, the film premiered in the UK and Australia. 

The film was filmed around La Ronge and Regina, Saskatchewan|Regina, both in Saskatchewan, Canada. 

Today, the film remains out-of-print on VHS, where it was released in both America and the UK.   An official Polish import DVD was released at a later date. 

The films two taglines read "In a world of deception and betrayal, who is the target and who is the decoy?" and "Death is their business." 

==Plot==
When a rival businessman Jenner threatens his daughter Diana, tycoon John Wellington hires Secret Serviceman Jack Travis to protect her. In turn, Travis hires the eccentric mercenary Baxter, also a former SS agent, to assist. So, Travis and Baxter are hired to protect his daughter.It all seems simple enough at the beginning, but shortly after the bodyguards meet Diana they quickly find themselves in an increasingly complex and deadly situation in which almost no one is exactly who she or he seems to be.   

==Cast==
* Peter Weller as Baxter
* Robert Patrick as Travis
* Charlotte Lewis as Katya
* Darlene Vogel as Diana
* Peter Breck as Wellington
* Scott Hylands as Jenner
* Vladimir Kulich as Daniel
* Zoltan Buday as Gunther
* Blaine Hart as Spence
* Patricia Drake as Madelaine Phil Hayes as Mick

==Reception==
<!--
  
 
 
 
 
-->
Sandra Brennan of Allmovie gave the film one star out of five.  The Fort Scott Tribune gave the film two stars out of five after the film was broadcast on American TV on June 12, 1997. 

Dragan Antulov of rec.arts.movies.reviews gave two out of ten, stating "Italian visual effects expert Carlo Rambaldi is always going to be remembered for various impressive bad guys he had helped create for films like King Kong or Alien. When he was executive producer for Decoy, Canadian 1995 action thriller, the result was something which was only bad and hardly the stuff he is going to be remembered for. Cynics would say that Peter Weller and Robert Patrick seem like an excellent choice for the roles of mercenaries because they played killing machines in their best known films. Author of this review tends to agree with that, but their presence alone is not enough to save Decoy from well-deserved road to "straight-to- video" oblivion. Everything else is a complete mess - the plot is incoherent, the action scenes look terrible, so in the end Decoy sinks below the standards, which are lowest, even for B movies. The only thing that is relatively pleasing to the eye is the use of Saskatchewan locations but the viewers might do themselves a great service if they watch some documentary instead of this." 

Both books Video Source Book: Video Program Listings A-I  and VideoHounds Golden Movie Retriever gave the film two and half out of five stars.  The book DVD & Video Guide 2005 (Ballantine Books) gave the film three out of five. 

==References==
 

==External links==
*  

 
 
 
 