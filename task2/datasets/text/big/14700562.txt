The Opium War (film)
{{Infobox film name = The Opium War image = The_Opium_War_Poster.jpg alt =  caption = Film poster traditional = 鴉片戰爭 simplified = 鸦片战争 pinyin = Yāpiàn Zhànzhēng}} director = Xie Jin producer = Chen Zhigu writer = Zhu Sujin Ni Zhen Zong Fuxian Mai Tianshu starring = Simon Williams Shao Xin Su Min Gao Yuan music = Jin Fuzai Huang Hanqi cinematography = Hou Yong Shang Yong editing = David Wu Qian Lili Zhang Longgen studio = Emei Film Studio Xie Jin / Heng Tong Film & TV Co. distributor = Golden Harvest released =   runtime = 150 minutes country = China language = Mandarin English budget =  gross = 
}} Golden Rooster Cannes and Montreal Film Festival|Montreal. The film tells the story of the Opium War between China in the waning days of the Qing dynasty and the British Empire, through the eyes of key figures such as the fiercely nationalistic Lin Zexu and the British naval diplomat Charles Elliot.

Unlike many of its contemporaries, The Opium War was strongly supported by the state apparatus.  Despite its clear political message, many Western commentators found the treatment of the historical events to be generally even-handed. 

At the time of its release, The Opium War, with a budget of US$15 million, was the most expensive film produced in China.  It was released to coincide with the Hong Kong handover ceremony in July 1997.

==Cast==
* Bao Guoan as Lin Zexu Qishan
* Sihung Lung as He Jingrong
* Shao Xin as He Shanzi
* Su Min as Daoguang Emperor
* Gao Yuan as Ronger
* Bob Peck as Denton (based on Lancelot Dent) Simon Williams as Charles Elliot
* Debra Beaumont as Queen Victoria
* Emma Griffiths as Mary Denton Philip Jackson as Captain White
* Garrick Hagon as missionary Sidon Laughton
* Robert Freeman as Hill Yishan
* Jiang Hua as Guan Tianpei
* Zhou Chuanyi as Yiliang
* Li Weixin as Deng Tingzhen
* Li Shilong as Han Zhaoqing
* Liu Zhongyuan as Lü Zifang
* Shi Yang as Lin Sheng
* Kong Xianzhu as He Rengui
* Chang Xueren as San
* Li Shaoxiong as Yao Huaixiang
* Yang Heping as Qianzong
* Wang Fen as Qiuping
* Liang Yang as Baihe
* He Qingqing as Lanrui
* Gu Lan as official
* Yang Zhaoquan as blind musician
* Zhang Wanwen as brothel manager
* Dominic Jephcott as British Member of Parliament
* Jamie Wilson as artillery captain Paul Artuard
* Nigel Davenport
* Oliver Cotton

==Production==
The film was shot in Hengdian World Studios, Zhejiang, a common filming site for historical films dubbed "Chinawood." In order to recreate the streets of 19th century Guangzhou, nearly 120 construction teams from surrounding villages were assembled. 
In contrast, nearly all post-production took place in Japan. 

==Reception==
Despite its clear political background (and its release on the eve of the return of Hong Kong to China), the film was generally well received by Western critics as a workable example of the big-budget historical film. Variety (magazine)|Variety, in one review, begins with the fact that despite the films "unashamedly political message," The Opium War was nevertheless "comparatively even-handed," while the film itself had excellent production values.    The Guardian recognised that the film, despite its official backing, "was relatively nuanced," and praised the performance of Bob Peck as the venal opium trader Denton. 
 Golden Rooster for Best Film.

==References==
 

==External links==
*  
*  
*  
*   at the Chinese Movie Database

 
 
 
 
 
 
 
 
 