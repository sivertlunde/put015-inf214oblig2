Body Language (1992 film)
{{Infobox film
| name           = Body Language
| image          = 
| image_size     =
| caption        =
| director       = Arthur Allan Seidelman
| producer       = Robert M. Rolsky
| writer         = Dan Gurskis Brian L. Ross
| narrator       = 
| starring       = Heather Locklear Linda Purl
| music          = Misha Segal
| cinematography = Hanania Baer
| editing        = Bert Glatstein
| studio         =
| distributor    = USA Network
| released       = July 15, 1992
| runtime        = 93 minutes USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1992 television film written and directed by Arthur Allan Seidelman. The thriller, starring Heather Locklear as a successful business executive and Linda Purl as her assistant who is trying to take over her life, was produced for USA Network, and was released on VHS in 1998.

==Cast==
*Heather Locklear as Betsy Frieze
*Linda Purl as Norma Suffield
*James Acheson as Victor
*Edward Albert as Charles Stella

==Reception==
The New York Times wrote: "With two screenwriters, youd think that the producers could have come up with something less contrived and cliched." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 

 
 