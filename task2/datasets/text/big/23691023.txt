In Old Cheyenne (1941 film)
{{Infobox film
| name           = In Old Cheyenne
| image_size     = 
| image	=	In Old Cheyenne FilmPoster.jpeg
| caption        = 
| director       = Joseph Kane
| producer       = Joseph Kane (associate producer)
| writer         = John W. Krafft (story) and Olive Cooper (screenplay)
| narrator       = 
| starring       = Roy Rogers
| music          =  William Nobles
| editing        = Charles Craft
| studio         = 
| distributor    = 
| released       = 1941
| runtime        = 58 minutes (original version) 54 minutes (edited version)
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 American film directed by Joseph Kane and starring Roy Rogers.

== Plot summary ==
 

== Cast ==
*Roy Rogers as Steve Blane
*George "Gabby" Hayes as Arapaho Brown
*Joan Woodbury as Della Casey / Dolores Casino
*J. Farrell MacDonald as Tim Casey
*Sally Payne as "Squeak" Brown
*George Rosener as Sam Drummond
*William Haade as Henchman Davidge
*Hal Taliaferro as Henchman Pete
*Jack Kirk as Henchman Rufe

== Soundtrack ==
* "Bonita" (Music by Jule Styne, lyrics by Sol Meyer)
* "Linda Flor" (Written by Rudy Sooter and Aaron González)

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 