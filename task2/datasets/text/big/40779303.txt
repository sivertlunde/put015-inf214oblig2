Rule No. 1
 
 
 
 

{{Infobox film
| name           = Rule #1
| image          = RuleNo1.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 第一誡
| simplified     = 第一诫
| pinyin         = Dì Yī Jiè
| jyutping       = Dai6 Jat1 Gaai3 }}
| director       = Kelvin Tong
| producer       = Lam Tak Candy Leung
| writer         = 
| screenplay     = Kelvin Tong
| story          = Kelvin Tong John Powers
| based on       = 
| narrator       = 
| starring       = Shawn Yue Ekin Cheng Stephanie Che Fiona Xie
| music          = Joe Ng Alex Oh
| cinematography = Venus Keung
| editing        = Azrael Chung Fortune Star Entertainment Mediacorp Raintree Pictures Scorpio East Pictures Dream Movie Entertainment Overseas Boku Films
| distributor    = Fortune Star
| released       =  
| runtime        = 95 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = $932,596 
}}
 2008 Cinema Hong Kong horror film directed by Kelvin Tong and starring Shawn Yue, Ekin Cheng, Stephanie Che and Fiona Xie.

==Plot==
While on duty during an assignment, young sergeant Lee Kwok Keung (Shawn Yue) shot and killed serial killer Chan Fuk Loi (Ben Yuen) after he had his four limbs crippled by Chan. Forty nine days later, Lee woke up in the hospital but his testimony was not recognized by the police because of his description which contains some supernatural things. His superior then transferred him to the Miscellaneous Affairs Department to let him rest.

Unlike the heroic duty of fighting crime and protecting citizens, the Miscellaneous Affairs deals with unusual cases, which is intriguing and makes people feel lost. The head of the Miscellaneous Affairs, Inspector Wong Yiu Fai (Ekin Cheng), is an eccentric and beer-guzzling man. Wong and Lee work together for the Miscellaneous Affairs to solve an unusual request, to eliminate the fear of every citizen seeking help. While carrying out their duties, Wong pretentiously tells Lee the first commandment of the Miscellaneous Affairs: There are no ghost in this world! In fact, this first commandment just a lie. The primary task for the Miscellaneous Affairs of dealing unusual cases is to conceal supernatural incidents to the community to reduce social panic.

Just when Wong and Lee take over the Saint Austina High School Massacre case, the ghost of Chan Fuk possess into the body of Lees wife May (Fiona Xie), while Lee was also possessed later. The possessed Lee shoots and kills Wong and other colleagues and then makes up his own story to deceive the police.

==Cast==
*Shawn Yue as Sergeant Lee Kwok Keung
*Ekin Cheng as Inspector Wong Yiu Fai
*Stephanie Che as Esther
*Fiona Xie as May
*Renee Lee as Siu Man
*Bill Liu as Telephone
*Gloria Wong as Woman on bus
*Tsui Hing Wah as Man on bus
*Ben Yuen as Chan Fuk Loi
*Leung Tse Yan as Girl in car trunk
*Ng Siu Kong as DCP Lau
*Alex To as Harold
*Poon Cheuk Ming as Swimming Pool Guard
*Yu Sin Man as Ghost in swimming pool
*Choi Ka Po as Lam Siu Yuk
*Tsang Yuk Kuen as Nursing Home Matron
*Chow Chi Sing as Bartender
*Tsui Kwai San as Cinema Caretaker
*Ng Fung Ming as Real Estate Agent
*Chan Ching Yee as Winnie Lai
*Lee Pui San as Gillian
*Cheng Lai Pung as Doctor Au Chi Wai
*Ivy Lau as Nurse
*Jordon Ho as Coroner
*Ho Chi Wai as Rude Policeman
*Wong Kam Tong as Pig Farmer
*Leung Suk Fun as Pig Farmers wife
*Wong Lo Yiu as Kwok Siu Lan
*Chow Suk Wai as Policewoman
*Helen Lee as Teacher Lee
*Vanessa Tuan as School girl # 1
*Mo Kai Lai as School girl # 2
*Chan Yu Yan as School girl # 3

==Awards and nominations==
*2008 Puchon International Fantastic Film Festival
**Won: Best Actor (Shawn Yue, Ekin Cheng)
**Nominated: Best of Puchon (Kelvin Tong)

*2009 Singapore International Film Festival
**Won: Silver Screen Award for Best Singapore Film (Kelvin Tong)

==See also==
*Ekin Cheng filmography

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 
 
 
 
 
 
 
 
 
 
 
 