The Invisible Wall (1991 film)
{{Infobox film
 | name =  The Invisible Wall 
 | image = Il muro di gomma.jpg
 | caption =
 | director = Marco Risi
 | writer =  	Andrea Purgatori  Sandro Petraglia   Stefano Rulli
 | starring =  Corso Salani
 | music = Francesco De Gregori
 | cinematography =  Mauro Marchetti
 | editing =  
 | producer =
 | distributor = 
 | released =1991
 | language =   Italian
 }} 1991 Cinema Italian drama film directed by Marco Risi. The film, which deals with the Aerolinee Itavia Flight 870 disaster, entered the competition at the 48th Venice International Film Festival. 

== Cast ==
*Corso Salani: Rocco Ferrante
*Angela Finocchiaro: Giannina
*Ivano Marescotti: Giulio 
*Antonello Fassari: Franco
*Carla Benedetti: Sandra
*Pietro Ghislandi: Corrà
*David Zard: Agente Segreto
*Mario Patané: Paolo
*Eliana Miglio: Anna
*Gianfranco Barra: Minister of Defence
* Ivo Garrani: Admiral Chief of the Defence Staff
* Sergio Fiorentini: General Chief of Staff of the Air Force
* Luigi Montini: General Air Force Spokesman
* Tony Sperandeo: Air Force NCO

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 