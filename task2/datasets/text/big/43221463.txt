Sicario (film)
{{Infobox film
| name           = Sicario
| image          = 
| alt            = 
| caption        = 
| director       = Denis Villeneuve
| producer       = Basil Iwanyk   Molly Smith   Trent Luckinbill   Thad Luckinbill   Edward McDonnell
| writer         = Taylor Sheridan
| starring       = Emily Blunt   Benicio del Toro   Josh Brolin   Jon Bernthal
| music          = Jóhann Jóhannsson
| cinematography = Roger Deakins
| editing        = 
| studio         = Black Label Media   Thunder Road Pictures Lionsgate
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 crime drama film directed by Denis Villeneuve and written by Taylor Sheridan. The film stars Emily Blunt, Benicio del Toro, Josh Brolin and Jon Bernthal. It has been selected to compete for the Palme dOr at the 2015 Cannes Film Festival.   
 Lionsgate will have limited release of the film in the United States on September 18, 2015 and a wide release on September 25, 2015.

== Plot ==
A police officer (Emily Blunt) from Tucson, Arizona travels across the border to Mexico with a pair of mercenaries to track down a drug lord.

== Cast ==
* Emily Blunt as Kate Macy 
* Benicio del Toro as Alejandro 
* Josh Brolin as Matt 
* Jon Bernthal as Ted 
* Daniel Kaluuya as Reggie Wayne 
* Maximiliano Hernández as Silvio 
* Jeffrey Donovan as Steve Forsing 

== Production == Lionsgate acquired the US rights to the film, while Lionsgate International would handle the foreign sales.  On May 23, it was announced that the Director of Photography Roger Deakins would be shooting Denis Villeneuves Sicario after working with the director on 2013 film Prisoners (2013 film)|Prisoners.  On May 29, Jon Bernthal joined the cast of the film to play Ted.    On May 30, Josh Brolin joined the film to star along Benicio Del Toro and Emily Blunt, he would play Matt, a CIA guy who runs the task force that recruits Blunts character Kate Macy from a SWAT squad out of Tucson.    On June 6, actor Daniel Kaluuya signed on to star in the film as Reggie Wayne, an FBI agent partnered with Blunts character.    On June 24, Maximiliano Hernández was added to play Silvio, a hard-drinking drug smuggling cop in Mexico.    On July 21, Jeffrey Donovan joined the cast of the film to play Steve Forsing, a no-nonsense DEA Agent who engages in an all-out firefight at the Juarez border while transferring a prisoner.   

=== Filming ===
The filming of Sicario began on June 30, 2014 in Albuquerque, New Mexico|Albuquerque, New Mexico.  

=== Music ===
On August 27, 2014, Jóhann Jóhannsson was hired to compose the music for the film. 

== Release == Lionsgate set the film for a limited release in the United States on September 18, 2015 and a wide release on September 25, 2015. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 