Monster from the Ocean Floor
{{Infobox Film
| name           = Monster from the Ocean Floor
| caption        = 
| image	=	Monster from the Ocean Floor FilmPoster.jpeg
| director       = Wyott Ordung
| producer       = Roger Corman
| writer         = Bill Danch
| starring       = Anne Kimbell Stuart Wade Roger Corman
| music          = Andre Brummer
| cinematography = Floyd Crosby
| editing        = Edward Sampson
| distributor    = Lippert Pictures
| released       = 21 May 1954
| runtime        = 64 minutes
| country        =  
| language       = English
| budget         = $30,000 Alan Frank, The Films of Alan Frank: Shooting My Way Out of Trouble, Bath Press, 1998 p 15 
| gross = $850,000  Mark Thomas McGee, Talks Cheap, Actions Expensive: The Films of Robert L. Lippert, Bear Manor Media, 2014 p 156-159 
| preceded_by    = 
| followed_by    = 
}}

Monster from the Ocean Floor is a 1954 science fiction film about a sea monster that terrorizes a cove in Mexico. The film was directed by Wyott Ordung and starred Anne Kimbell and Stuart Wade.

It was the first film produced by Roger Corman (although he had previously written Highway Dragnet).

==Plot==
Julie Blair (Kimbell) is an American vacationing at a sea-side village in Mexico. She hears stories about a man-eating creature dwelling in the cove.  She meets Dr. Baldwin (Dick Pinner), a marine biologist, and they fall for one another. The mysterious death of a diver interests Julie in investigating, but Baldwin is very skeptical.  She sees a giant amoeba rising from the ocean.

==Cast==
*Anne Kimbell (Julie Blair)
*Stuart Wade (Steve Dunning)
*Dick Pinner (Dr. Baldwin)
*Wyott Ordung (Pablo)
*Inez Palange (Tula)
*Jonathan Haze (Joe) David Garcia
*Roger Corman (Tommy)

==Production== Allied Artists), $5,000 in deferment from Consolidated Labs, and money raised privately by selling $500 and $1,000 shares. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p15-20 

Wyott Ordong later claimed he hocked his life insurance and sold his apartment to raise $15,000 to pay for the movie. 

==Reception==
The film was sold to Lippert Pictures for $110,000. Corman received a $60,000 advance for this which enabled him to make his next film. 
==References==
 

==External links==
* 
*  

 
 
 
 
 
 


 