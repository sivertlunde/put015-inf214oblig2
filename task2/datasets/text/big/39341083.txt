Corianton: A Story of Unholy Love
Corianton: Alma in the Book of Mormon.

The films existence can be traced to the 1889 novel Corianton by B. H. Roberts.  That novel, along with A Ship of Hagoth by Julia A. MacDonald, was made into a play by Orestes U. Bean in 1902.  With the financial backing of George Elias Blair, Bean presented the play in Salt Lake City with Joseph Hawthorn in the title role and Rose Agnes Lane in a major female part.  It was criticized by critics as too long, but was a theatrical success.  It failed when it went on the road to non-Mormon audiences but was then successfully revived in Salt Lake City and other smaller Utah communities.
 Broadway with music by Harold Orlob.  It had six performances but was resoundingly rejected by critics.

In the late 1920s Lester Park and his brother Byron Park.  The Parks recruited Edgar Stillman Kelly to write the music.  When it was released in 1931 it was the first commercially produced, Mormon themed film.

==References==
*Truman G. Madsen. Defender of the Faith: The Life of B. H. Roberts. Salt Lake City: Bookcraft, 1980. p.&nbsp;296-297.
* 
* 
 
 
 
 
 

 