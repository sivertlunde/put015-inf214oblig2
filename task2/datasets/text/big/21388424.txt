Brewster's Millions (1945 film)
{{Infobox film
| name           = Brewsters Millions
| image_size     =
| image	         = Brewsters Millions FilmPoster.jpeg
| caption        =
| director       = Allan Dwan John E. Burch (assistant)
| producer       = Edward Small
| based on       = the 1902 novel Brewsters Millions by George Barr McCutcheon and the 1906 stage play of hte same name by Byron Ongley and Winchell Smith
| writer         = Sig Herzig Wilkie C. Mahoney Winchell Smith
| narrator       = Eddie "Rochester" Anderson
| music          = Hugo Friedhofer
| cinematography = Charles Lawton Jr.
| editing        = Richard Heermance
| studio         = Edward Small Productions
| distributor    = United Artists
| released       =  
| runtime        = 79 minutes
| country        = United States English
| budget         =
| gross          =
}}
 novel of the same name by George Barr McCutcheon. In the original Brewsters Millions, the hero was a stockbroker; in this version, Brewster is a returning soldier.
 Eddie "Rochester" Anderson, was treated too well.  
 Academy Award for Best Music, Scoring of a Dramatic or Comedy Picture.

==Plot==
Montague L. Brewster (Dennis OKeefe), a newly discharged U.S. soldier back from fighting in Europe during World War II, rushes home to marry his sweetheart, Peggy Gray (Helen Walker). However, he has to postpone the wedding after he learns of a strange windfall.

His deceased uncle has left him $8 million, but he can inherit the money only if he can spend a million of it before his 30th birthday, only two months away, without keeping any assets. The lawyer explains that Brewsters relative hoped it would make him so sick of spending that the rest of the fortune would not be wasted. The conditions include not telling anyone what he is doing. Brewster reluctantly agrees.

Brewster sets up his own investment company called Brewster & Company and hires his wartime buddies Hacky Smith (Joe Sawyer) and Noppy Harrison (Herbert Rudley) as vice presidents and Peggy as his private secretary. However, despite his best efforts, most of his schemes to lose money become profitable.

Worse, Peggy becomes jealous of Brewster spending a great deal of time with first socialite Barbara Drew (Gail Patrick), then showgirl Trixie Summers (June Havoc), even though he is only using them to help squander the million. Smith and Harrison (mistakingly thinking that Brewster has gone crazy due to his spending sprees), begin to thwart all of his spending schemes. At the same time, Peggy breaks up with Brewster, but her wise mother (Nana Bryant) persuades her to go on a costly cruise with him and the cast of a failed play he financed after Smith and Harrison close it down.
 salvage fee of over a few hundred-thousand dollars (which Brewster offers to double the amount asked by the foreign ship). He learns that this is all of the last amount of money to spend.

In the final scene, set a few days later as the deadline approaches, Brewster thinks he has met his goal, only to have his friends present him with $40,012 that they have recovered from his failed ventures. Luckily as the clock to the deadline strikes 12 noon, he is able to get rid of the money by paying the executors fee, an old $10 debt, and the last $2 for a cab fare, just before time runs out. Having secured his inheritance of $7,000,000, Brewster then takes Peggy out saying that they have to go downtown to the nearest justice of the peace to get married right away.  On the way out the door he is confronted by a door-to-door salesman.  The salesman is trying to sell the item for 2 cents more than it can be bought at the store.  For this reason, Brewster throws him out, thus proving that his actions over the last 60 days had not changed him.

==Cast==
* Dennis OKeefe as Montague L. Brewster
* Helen Walker as Peggy Gray
* June Havoc as Trixie Summers Eddie "Rochester" Anderson as Jackson, the Grays servant
* Gail Patrick as Barbara Drew
* Mischa Auer as Michael Michaelovich
* Nana Bryant as Mrs. Gray
* John Litel as Swearengen Jones, a lawyer
* Joe Sawyer as Hacky Smith Neil Hamilton as Mr. Grant
* Herbert Rudley as Nopper Harrison
* Thurston Hall as Colonel Drew, Barbaras banker father

==Production==
Edward Small originally wanted to film another farce, Are You a Mason? and bought the rights off Paramount in 1942 intending to make a vehicle for Jack Benny. However there was confusion over European rights so he decided to adapt Brewsters Millions instead. HOLLYWOOD AWAKENS TO THE SHORTS: One and Two Reel Films Regaining Popularity -- Love Wins as Usual
By FRED STANLEYHOLLYWOOD.. New York Times (1923-Current file)   25 June 1944: X3.   He bought the rights in June 1944. Brewsters Millions
The Christian Science Monitor (1908-Current file)   19 June 1944: 4. 

Garry Moore was originally cast but was replaced after one day of filming by Mischa Auer. Andy Russell Touted as Mexicos Sinatra: Bing Crosby Will Fill Guest-Star Spot in Filmization of Duffys Tavern
Schallert, Edwin. Los Angeles Times (1923-Current File)   01 Sep 1944: 10.  
==Remake==
Small remade the film in 1961 as Three on a Spree. In 1970 he announced he wanted to make a TV series based on the film but it did not result. Cloris Leachman Signs Pact
Martin, Betty. Los Angeles Times (1923-Current File)   22 May 1970: g18.  

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 