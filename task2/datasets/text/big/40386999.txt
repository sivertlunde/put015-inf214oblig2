Stricken (2009 film)
{{Infobox film
| name     = Stricken
| image    =
| director = Reinout Oerlemans
| writer   = 
| producer = Reinout Oerlemans
| Script = 
| starring = Barry Atsma Carice van Houten
| cinematography = 
| music = 
| country = Netherlands
| language = Dutch
| runtime = 105 minutes
| released =  
}}
Stricken ( ) is a 2009 Dutch drama film directed and produced by Reinout Oerlemans and based on the book by the same name by writer Ray Kluun.

==Plot==
Stijn (Barry Atsma) is a rich, handsome and self-centered advertising executive who lives in a hedonistic world. Everything revolves around him. He has a successful career and a loving marriage with Carmen (Carice van Houten). Occasionally he cheats, but it is tolerated due to his enthusiastic and impetuous character. Fate strikes as it turns out that Carmen is suffering from breast cancer. He supports her, but as it can sometimes be difficult, he will often go between meeting with Roos (Anna Drijver), with whom he has a tempestuous affair, and lying to Carmen.  suicide by injection from the doctor, and finally dies. After the death of Carmen, Stijn goes even further in his relationship with Roos.

==Cast==
* Barry Atsma - Stijn
* Carice van Houten - Carmen
* Anna Drijver - Roos
* Pierre Bokma - Huisarts
* Sacha Bulthuis - Oncoloog

==References==
 

== External links ==
* 

 
 
 


 