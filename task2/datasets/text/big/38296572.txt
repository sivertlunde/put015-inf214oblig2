Peter in Magicland
{{Infobox film
| name           = Peter in Magicland
| image          =
| caption        = Theatrical poster
| director       = Wolfgang Urchs
| producer       =
| writer         = Alexander Kekulé Wolfgang Urchs
| narrator       =
| starring       = André Schmidtsdorf Nathalie del Castillo Manfred Lichtenfeld Friedrich W. Bauschulte
| music          =
| editing        =
| studio         = TC-Film Trickfilmstudio A. Film A/S
| distributor    = Iduna Film ZDF
| released       =  
| runtime        = 80 minutes
| country        = West Germany
| language       = German
| budget         =
| gross          =
}}
Peter in Magicland (  animated film, based on Gerdt von Bassewitzs book Peter and Annelis Journey to the Moon. The film was released on November 29, 1990 in Germany.

==Plot==
 

==Cast==
* André Schmidtsdorf – Peterchen
* Nathalie del Castillo – Anneliese
* Manfred Lichtenfeld – Sumsemann
* Friedrich W. Bauschulte – Sand God
* Wolfgang Hess – Moon Devil
* Nathalie del Castillo – Sun Goddess
* Dagmar Heller – Dark Goddess
* Fritz von Hardenberg – Storm God
* Udo Wachtveitl – Rain God
* Michael Habeck – Galaxy God
* Monika John – Lightning Goddess
* Manfred Erdmann – Thunder God
* Martina Duncker – Wind Goddess
* Doris Jensen – Cloud Goddess
* Arne Elsholtz – Water God
* Manfred Erdmann – Ice God
* Walter Reichelt – Santa Claus
* Willi Roebke – Gingerbread Man

==References==
 

==External links==
*   at Internet Movie DataBase

 
 
 
 
 
 
 
 
 
 
 
 


 
 