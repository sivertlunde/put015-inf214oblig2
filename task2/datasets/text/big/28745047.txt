Tiffany Jones
{{Infobox Film
| name           = Tiffany Jones
| image          = "Tiffany_Jones"_film_poster.jpg
| caption        =  Pete Walker
| producer       = Pete Walker
| writer         = Alfred Shaughnessy
| narrator       =  Ray Brooks Eric Pohlmann
| music          = Cyril Ornadel
| cinematography =  Peter Jessop	
| editing        = 
| distributor    = Hemdale
| released       =   12 April 1973
| runtime        = 90 min.
| country        = UK English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}  British comedy Pete Walker Ray Brooks and Eric Pohlmann.  It was based on a comic strip that had featured in the Daily Mail. The main character, Tiffany Jones, a leading model, also works as a secret agent. She encounters in this film both good and bad men alike. 

This film appeared on screen and later on cable TV in the 1970s and 1980s. Then star Anouska Hempel bought the UK rights to the film. She owns the distribution rights, and the film has not been shown or distributed since.

==Cast==
* Anouska Hempel ...  Tiffany Jones Ray Brooks ...  Guy
* Susan Sheers ...  Jo
* Eric Pohlmann ...  President Boris Jabal Martin Benson ...  Petcek
* Damien Thomas ...  Prince Salvator Alan Curtis ...  Marocek
* Bill Kerr ...  Morton
* Richard Marner ...  Vorjak
* John Clive ...  Stefan
* Geoffrey Hughes ...  Georg
* Ivor Salter ...  Karatik
* Lynda Baron ...  Anna Karokin
* Nick Zaran ...  Anton
* Martin Wyldeck ...  Brodsky
* Tony Sympson ... Prim man

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 
 