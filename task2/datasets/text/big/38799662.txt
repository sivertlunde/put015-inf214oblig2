Women Must Dress
{{Infobox film
| name = Women Must Dress
| image =
| caption =
| director = Reginald Barker
| producer = Dorothy Davenport
| writer = Dorothy Davenport Edmund Joseph Frank Farnsworth Gavin Gordon Paul Ellis Arthur Lake Jon Hall
| music =
| cinematography = Milton R. Krasner
| editing = Jack Ogilvie
| distributor = Monogram Pictures
| released =  
| runtime =
| country = United States
| language = English
| budget =
| gross =
}} Gavin Gordon. Jon Hall, albeit under his birth name Charles Locher.

==Plot Outline==
Linda Howard discovers that her husband is having an affair and takes their young adult daughter Janet away to start a new life. Linda becomes a successful fashion designer but begins to re-examine her own life choices when Janet forsakes her successful-but-bland doctor boyfriend for a faster, more loose set of companions.

==Availability==
Copyright has lapsed since the original theatrical run of Women Must Dress, and the film is now in public domain.

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 

 