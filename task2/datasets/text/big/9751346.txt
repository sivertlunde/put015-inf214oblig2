El Cantante
 
{{Infobox film
| name           = El Cantante
| image          = Finalposter wiki.jpg
| caption        = 
| alt            = 
| starring       = Marc Anthony Jennifer Lopez
| director       = Leon Ichaso
| producer       = Julio Caro Simon Fields Jennifer Lopez David Maldonado
| writer         = Leon Ichaso David Darmstaeder Todd Anthony Bello  Picturehouse
| released       =  
| runtime        = 106 minutes
| gross          = $24,521,532 (including DVD sales) 
| language       = English / Spanish
| music          = Marc Anthony  Jennifer Lopez
}} salsa singer Héctor Lavoe, who is portrayed by Anthony.  The film is told from the viewpoint of Puchi, Hectors wife, portrayed by Lopez. Initially debuting at the Toronto International Film Festival on September 12, 2006, El cantante was released on August 3, 2007.

Distributed by Picturehouse (company)|Picturehouse, the film runs for 106 minutes and is rated R for language, sexuality and drug use. 

==Plot ==
Puchi (Jennifer Lopez) talks about her late husband, salsa legend Héctor Lavoe (Marc Anthony), during a 2002 interview, as she reminisces about memories from the 1960s and 70s, following his arrival in New York City. She discusses their life together and his downfall, which was caused by drugs, alcohol and Depression (mood)|depression. Scenes of their meeting and courtship are inter-cut with Héctor performing, while Puchi stands on the side of the stage, dancing to the music.

Héctors collaborations with salsa musician Willie Colón (John Ortiz) and the Fania All Stars are shown, as well as his ventures as a solo artist. Notable events including the birth of their son, Héctor Jr, his infidelities, and their break-up. Puchi finally becomes tired of his constant partying, cheating, and use of drugs. Although she had pushed him to get professional help, and he did so on occasion, she was unable to persuade him to stop his addictions.

==Cast==
*Marc Anthony as Héctor Lavoe
*Jennifer Lopez as Nilda Georgina "Puchi" Roman
*Federico Castelluccio as Jerry Masucci
*Vincent Laresca as Ralph Mercado
*Víctor Manuelle as Rubén Blades
*John Ortiz as Willie Colón

== Release ==
El cantante, which has an MPAA rating of R, was first premiered at the Toronto International Film Festival which took place in September 2006.  It was then released to over 542 cinemas on August 3, 2007.     The film opened at No. 12 at the U.S. Box Office, grossing $3,202,035 during its opening weekend. It averaged around $6,000 per theater. The following week, it grossed $1,401,148 and fell to No. 16. It left the chart at No. 115 on the week of September 28, 2007, after grossing $1,465.  El cantante grossed $7,556,712 domestically and $354,820 overseas, totaling $7.9 million. 

Charlotte OSullivan of the Evening Standard said the "film did not go down well" at the Box Office, crediting its lack of commercial success to Lopez and Anthony "know nothing about salsa" and its "negative view of the Latino community by focusing on Lavoe’s drug use and death from Aids", which had sparked El cantante#critism|controversy.  The films DVD was released on October 30, 2007. It became a hit, ranking at No. 8 on the DVD/Home Rentals chart at Box Office Mojo, and had grossed $16.61 million in DVD sales as of December 23, 2007. 

===Critical reception===
Following its Toronto International Film Festival premiere, El Cantante earned mostly negative reviews from critics and currently holds a 25% rating on Rotten Tomatoes based on 106 reviews.

Robert Koehler of Variety (magazine)|Variety felt that the biopic, which contained "many standard-issue biopic montage sequences", was only "geared for the bigscreen, but the fairly bland visual design will make pic more than suitable to be seen on the tube". Koehler was critical of both performances, feeling that while Lopez brought "plenty of ferocity" to her character, there wasnt "shape" or "power" to her "wrath", and labeled Anthony "the dullest of movie drug addicts". 

 s portrayal of Lavoe was met with a mixed reaction from film critics.]]

A.O. Scott of The New York Times felt that Anthony as Lavoe was hidden behind "his high, delicate cheekbones and tinted glasses" but "Whenever Héctor takes the stage, however, Mr. Anthony unleashes his charisma, and shows that, whatever his limitations as an actor, he is a brilliant performer". Scott praised Lopezs performance, noting that it had "a lot of fight" with "a hard, skeptical edge" that made Puchi "a more interesting and plausible character than her husband".  Ed Gonzalez of Slant Magazine gave the film and Anthonys performance a positive review, but was critical of Lopez, calling her performance "predictably self-conscious" but said her sequences "complements Puchis own".  Renee Schonfield of Common Sense Media gave El cantante a negative review, "brings the magic of salsa music to the screen; unfortunately, it also brings the audience another bleak story of a flameout singer bent on self-destruction". 

Kevin Maher of The Times was negative, stating that the "soft-pedalled account of Lavoes rise to fame and his drug-related downfall that has more in common with the biopic parody Walk Hard than anything as muddy as real life".  Claudia Puig of USA Today said Anthony "gives a fine and impassioned performance" in a story that "relies on formula and clichés of the genre" which meant "we dont learn enough about what caused the Puerto Rican-born Lavoes downward spiral into drug use, promiscuity and suicide attempts after he gained fame in New York City". Additionally, Puig criticized Lopezs airtime by stating "the film has far too much of her and not enough of Anthony". 

Peter Bradshaw of The Guardian, in a review based primarily on Lopez, panned her performance and said "There is something entirely dead about Lopezs performance. No matter how superficially lively she makes it, she is always simply mouthing the lines".  On the other hand, Charlotte OSullivan of the Evening Standard praised Lopez and her character, "Lopez deserves praise for pushing this project. The recently deceased Puchi (who helped generate the script and pushed for Lopez to play her) is not the kind of character you see on screen every day. She has not been whitewashed and neither, thank goodness, has her husband".   

=== Criticism ===
The film was criticized, and accused of "usurping barrio culture and exploiting Lavoes memory", with salsa singer Ismael Miranda condemning it for "focusing too much on the tragic artists drug abuse, which eventually led to his death from AIDS complications". Other celebrities who criticized the film were singer Domingo Quiñónez, and vocalist Cheo Feliciano.  Willie Colón, Lavoes long time friend and musical partner, was critical of the film. Although he had been hired as a consultant for El cantante, he was not pleased with the end result, stating:   
 "The creators of El Cantante missed an opportunity to do something of relevance for our community. The real story was about Hector fighting the obstacles of a nonsupportive industry that took advantage of entertainers with his charisma and talent. Instead they did another movie about two Puerto Rican junkies". 
Additionally, Colón blamed Lopez and Anthony: 
 "Its difficult to comprehend how two individuals who are in the music business like Marc and Jennifer are not aware of the damage and the consequences of promoting only the negative side of our Latin music culture".  And while the film was predominantly a love story, Colón also noted that in real life, he believed Puchi caused Lavoes drug problems, but was canonized "so that Jennifer can play her".  David L. Coddon of U-T San Diego said following the films release, "Latino pop cultures highest-profile couple is taking heat from salsa purists who complain that the film, about salsa legend Hector Lavoe, is a distortion, even an exploitation", while also stating Lopez is "one of the most famous women in the world, and theres nothing her critics can do to change that". 
 Oscar nomination and blamed the Academy of Motion Picture Arts and Sciences for overlooking both the film and her performance. 

=== Accolades ===

{| class="wikitable sortable"
|-
!scope="col" style="width=2| Award
!scope="col" style="width=2| Work  
!scope="col" style="width=3% | Result 
|- Outstanding Performance of a Lead Latino /
a Cast in a Motion Picture]] 
|rowspan="2"|El cantante
| 
|- Premios Juventud|Premios Juventud for Best Movie
|  
|- Premios Juventud|Premios Juventud for Best Actress Jennifer Lopez
|  
|- Latin Grammy Latin Grammy Award for Best Salsa Album  Marc Anthony
| 
|}

== References ==
 

== External links ==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 