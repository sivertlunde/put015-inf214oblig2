The Lesson (2014 Latvian film)
{{Infobox film
| name = The Lesson
| image =  
| alt = 
| caption = 
| film name = The Lesson (2014)
| director = Andris Gauja
| producer =  
* Andris Gauja
* Guna Stahovska 
* Ingrīda Nagle
 
| writer =  
* Lauris Gundars 
* Andris Gauja
* Aleksandrs Grebņevs
 
| starring =  
* Inga Alsiņa
* Mārcis Klatenbergs
* Andrejs Smoļakovs 
* Gatis Gāga
* Liena Šmukste 
* Marina Janaus
* Edgars Siliņš
* Ieva Apine
* Elza Feldmane
* Agirs Neminskis
 
| music = 
| cinematography = 
| editing = 
| studio =       
* Riverbed (Latvia)
* Horosho Production (Russia)
 
| distributor = 
| released =     
| runtime = 
| country = Latvia
| language = Latvian
| budget = 
| gross =        
}}
The Lesson ( ) is a 2014 Latvian narrative feature film by Latvian director Andris Gauja. This is Andris Gaujas debut movie and is real Latvian success story.
Movie tells a story of a young teacher and her relationships with her class, class parents and her beloved ones. .

== Plot ==
An attractive Russian language teacher takes a new job in the Latvian city of Riga mentoring an unruly group of graduating seniors. After wooing them with drunken parties at her apartment, she shrewdly takes control of the class—until one of her male students begins to pursue her. Superbly directed with subtlety, empathy and suspense, director Andris Gauja lets this surprising and satisfying character-driven story unfold with all the messiness of real life.

== Success Story ==
Initially no one dared dreaming that the movie would become wildly renown, as the style and the plot of a movie was meant to be more film festival-like, rather than for common public, however, that just did not work. The movie became incredibly popular amongst young people. 
On October 15, 2014, "Izlaiduma gads" became the most popular movie in Latvia, for a while beating Hollywood blockbusters, becoming also the most viewed movie at that time.

== Festivals ==

=== List of festivals ===

* Montreal World Film Festival - First Films World Competition Program 
* Bergen International Film Festival - Extraordinary Films Program 
* "Kinoshock" Film Festival (Russia) 
* Chicago International Film Festival - World Cinema Program 
* Connecting Cottbus Film Festival (Germany) - Focus "Queer East"


=== Chicago ===
Chicago International Film Festival

=== Anapa ===

=== Bergen ===

=== Montreal ===

=== Cottbus ===

== Cast ==
* Inga Alsina as Zane
* Marcis Klatenbergs as Max
* Ieva Apine as Inta
* Gatis Gaga as Uldis
* Ivars Auzins as Eriks (Zanes husband)
* Andrey Smalyakov as Georgiy (Maxs father)
* Marina Janaus as School Director
* Aigars Ligers as Olafs (Uldiss son)
* Liena Smukste as Inara
* Edgars Silins as Kristaps
* Elza Feldmane as Evita
* Agirs Neminskis as Alex

==Production==

===Development===
The film project started in late 2010 when Andris Gauja with Aleksandrs Grebnevs decided to continue their collaboration in movie industry and make a new film after their award winning film Family Instinct

===Screenplay===
Andris Gauja wrote the screenplay himself together with Lauris Gundars

===Casting===
Casting for this movie was especially interesting as the vision was to make a movie that would reflect the reality as realistically as possible. 
For this reason there were very few professional actors that were accepted. 

Most of the actors were not professional and had no previous experience whatsoever.  

===Filming===
The film was shot in very various locations non of them in a pavilion. 

Most of the movie has been shot in Latvia, Riga, including 2 schools - Jāņa Poruka vidusskola and Friča Brīvzemnieka panmatskola

===Post-production===


===Soundtrack===
Original music was written by  Andris Gauja and others.

==Release==
"The Lesson" received its first screening at the Montreal Film festival and subsequently was played in some other film festivals in Europe including Bergen International Film Festival participating in "Extraordinary Films Program"; Kinoshock Film Festival (Russia); Chicago International Film Festival participating in "World Cinema Program"; Connecting Cottbus Film Festival (Germany) in "Focus "Queer East"" and some others.

===Critical reception===
Many people who had viewed the film, especially ladies in their late 50s-60s, were shocked and said that the movie is outrageous and should not even be available for public viewings, as the film illustrates only the worst parts about school and often even things that according to their opinion is untrue and unrealistic. 

Nevertheless there were some individuals amongst younger people who agreed, mostly younger people said that on a contrary the situation has not been reflected harshly enough. That the real life situation is much worse and much more intense. 

Generally people were very influenced by the movie and always had something strong to say - either good or bad.

===Home media===
The film was released to cinemas on Blu-ray, DVD and DCP, non of them available for public. 
After many complications with the physical disks, authors of the film started to insist on switching to digital formats.


===Awards and nominations===
Despite being incredibly and unexpectedly successful film in Latvia, The Lesson never got nominated for anything in Latvia. 

== Marketing ==
In spite of different financial obstacles the movie even before its premiere in Latvia, is already renowned and highly discussed amongst young people.

== References ==
 
 
 
 
 
 

 
 
 
 