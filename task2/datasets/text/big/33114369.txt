Seven Keys to Baldpate (1916 film)
 
{{Infobox film
  | name     = Seven Keys to Baldpate
  | image    = 
  | caption  = 
  | director = Monte Luke
  | producer = 
  | writer   = Alex C. Butler
  | based on = play by George M. Cohan from the novel by Earl Derr Biggers Charles Villers Alex C. Butler Fred Maguire
  | music    = 
  | cinematography = Maurice Bertel
  | editing  = 
  | studio = J.C. Williamson Ltd (film company)|J.C. Williamson Ltd
  | distributor = 
  | released = 24 May 1916 
  | runtime  = 4 reels 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 popular play 1935 and Seven Keys to Baldpate (1947 film)|1947.

Three reels of the film survive today.

==Plot==
A novelist makes a bet he can complete a book within 24 hours. He goes to write it at a mountain resort which he thinks is deserted but is disturbed by a series of visitors.

==Cast==
*Dorothy Brunton as Mary Norton
*Fred Maguire as Magee 
*J Plumpton Wilson as Peters the hermit
*Agnes Keogh as Myra Thornhill
*Alex C Butler as Jim Cargan
*Gerald Harcourt as Lou Max Charles Villiers as Thomas Hayden
*James Hughes as Jim Kennedy
*Marion Marcus Clarke as Mrs Rhodes
*Monte Luke as Hal Bentley

==Production==
This was the fourth and final stage adaptation from J.C. Williamson Studios. Unlike the others the bulk of it was not shot at J.C. Williamsons studio facility but in the Theatre Royal. It is most likely this was because only the Theatre Royal was large enough to fit the set. Marsden, Ralph. The Old Tin Shed in Exhibition Street: The J.C. Williamson Studio, Melbournes Forgotten Film Factory  . Metro Magazine: Media & Education Magazine, No. 157, 2008: 144-153. Availability:   ISSN: 0312-2654.  . 

It was announced in 1 June 1916 that the screenplay had been completed and Dorothy Brunton was to star. 

This was the first of only two film appearances by popular stage star Dorothy Brunton.  She and co star Fred Maguire were appearing in musical comedies in the evening at Her Majestys Theatre while filming during the day. 

Monte Luke had taken publicity stills of the Australian 1914-15 production of the play.   
 

==Reception==

===Critical===
The film was not highly regarded by critics Ross Cooper and Andrew Pike who wrote "the production was static and graceless, with flimsy canvas sets an lengthy mute dialogues that relied heavily on the audiences familiarity with the play." 

Hal Carleton from Motion Picture News stated called the film "the finest attempt yet from "J.C.W." in the way of local production, although it is a noticeable fact that the artists appearing in the picture are off the legitimate stage." 

The critic from the Kalgoorie Miner said the film "represents quite an exceptional story of magnetic power and infinite charm and grace. Brilliant acting, fine staging and beautiful scenery obtain all throughout." 

Theatre historian Ralph Marsden later wrote that:
 Although the film is statically photographed against the usual canvas walled stage sets, the photography is slightly more imaginative than in Officer 666, with a greater variety of shots, use of close-ups and, on occasion, some atmospheric lighting. There are also tentative advances in pictorial composition, with figures set both near and far within the frame, rather than simply lined up on a single plane, as if on stage. The adaptation fails, once again, because the scenario makes no attempt to properly interpret the lengthy dialogue exchanges for the silent screen. The few titles try to précis the plot and only occasionally quote dialogue verbatim.  

===Box Office===
The film does not appear to have been a box office success, never been given a city screening in Sydney or Melbourne. 

After production finished, Williamson sent Monte Luke to Hollywood to study production. After witnessing filming on Intolerance (film)|Intolerance (1916), Luke returned to Australia and recommended that Williamson abandon film production, which he did. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 64 

==References==
 

==External links==
*  
*  at National Film and Sound Archive
*  
 

 
 
 
 
 
 
 
 
 
 


 