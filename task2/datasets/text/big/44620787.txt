Harivu (film)
{{Infobox film
| name           = Harivu
| image          = Movie_Poster_of_Harivu.jpg 
| caption        = Theatrical release poster
| director       = Mansore (Manjunatha Somashekara Reddy)
| producer       = Avinash U. Shetty
| writer         = Mansore (Manjunatha Somashekara Reddy)
| screenplay = HA Anilkumar
| starring       = Sanchari Vijay Shveta Desai Aravind Kuplikar Madhushree Master Shoib M.C. Anand Chethan Sheshan M.P. 
| music          = Charan Raj
| cinematography = Anand Sundaresha
| editing        = Avinash U. Shetty
| studio         = Om Studios
| released       = 
| runtime        = 1:52:44
| country        = India Kannada
| budget         = 
| gross          =  
}}

Harivu ( ;  ) is a 2014 Indian Kannada language movie written & directed by debutant Manjunatha Somashekara Reddy (S Manjunath / Mansore).    A real life incident documented in a popular Kannada news paper written by Dr. Asha Benakappa        gave thoughts for making this movie.  

Inspired by a true incident that took place in a Government hospital in Bengaluru in the recent years, Harivu speaks volumes about the relation between urbanization and alienation.

Harivu has won 62nd National Award for "Best Kannada Film" from Directorate of Film Festivals.      

==Plot==
The story is based on a real-life incident. A farmer from north Karnataka brings his ailing son to Bangalore for treatment but the son dies. The farmer then faces a dilemma on how to take the body to his hometown.

==Cast==

* Sanchari Vijay
* Shweta Desai
* Aravind Kuplikar
* Madhushree
* Master Shoib
* M.C. Anand
* Chethan
* Sheshan M.P.

==Screening==
*The movie has won the best Kannada Film at 62nd National Film Awards (India) for 2014 announced on 24 March 2015 Bengaluru International Film Festival 2014.    It was also selected in the Kannada Competition category.
* Screened in  Delhi International Film Festival 2014.   

==References==
 

==External links==
*  

 

 
 
 


 