Bound and Gagged (serial)
 
{{Infobox film
| name           = Bound and Gagged
| image          =
| caption        =
| director       = George B. Seitz
| producer       =
| writer         = Frank Leon Smith
| starring       = Marguerite Courtot George B. Seitz Nellie Burt Harry Semels Frank Redman
| music          =
| cinematography =
| editing        =
| distributor    = Pathé
| released       =  
| runtime        = 10 chapters
| country        = United States
| language       = English
| budget         =
| gross          =
}} silent Serial film serial George B. spoof of cliched melodramatic serials of the era.

It was shot in Fort Lee, New Jersey when many early film studios in Americas first motion picture industry were based there at the beginning of the 20th century.   

Four episodes survive in the Library of Congress film archive. 

==Cast==
*Marguerite Courtot as Princess Istra
*George B. Seitz as Archibald A. Barlow
*Nellie Burt as Margaret Hunter
*Harry Semels as Don Esteban Carnero
*Frank Redman as Roger Kipley John Reinhardt as Oscar Ben Glade
*Tom Goodwin as Willard Hunter
*Joe Cuny as Barcelona Ben
*Harry Stone
*Bert Starkey

==Chapter Titles==
# The Wager
# Overboard
# Help! Help!
# An Unwilling Princess
# Held For Ransom
# Out Again, In Again
# The Fatal Error
# Arrested
# A Harmless Princess
# Hopley Takes The Liberty

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 


 