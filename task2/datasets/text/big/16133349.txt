Fade to Black (2006 film)
{{Infobox Film name = Fade to Black image = FadetoBlack2006.jpg director = Oliver Parker producer = James Spring   Jonathan Olsberg Barbary Thompson writer = Oliver Parker   Davide Ferrario (book) starring = Danny Huston   Diego Luna   Paz Vega   Christopher Walken music = Charlie Mole editing = Guy Bensley cinematography = John de Borman distributor = Mirimax Records   Pandora Cinema                             released = 9 October 2006 (Hamburg Film Festival) 30 March 2007 (Spain) 25 May 2007 (Serbia) runtime =  104 minutes country = United Kingdom language = English 
}} thriller film directed by Oliver Parker and starring Danny Huston as Orson Welles.

==Synopsis== Black Magic, an actor is murdered on set and Welles finds himself allured by the deceaseds beautiful stepdaughter (Paz Vega). Soon Welles becomes embroiled in dangerous political games as the run-up to post-war elections surfaces.

==Cast==
* Danny Huston as Orson Welles
* Diego Luna as Tommaso Moreno 
* Paz Vega as Lea Padovani 
* Christopher Walken as Brewster
* Nathaniel Parker as Viola
* Anna Galiena as Aida Padovani 
* Violante Placido as Stella
* Pino Ammendola as Grisha

==External links==
* 
*Ken Russell,  , Times of London review, March 6, 2008.
 

 
 
 
 
 
 
 
 
 

 