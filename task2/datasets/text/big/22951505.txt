Grendizer, Getter Robo G, Great Mazinger: Kessen! Daikaijuu
{{Infobox film
| name           = Grendizer, Getter Robo G, Great Mazinger: Kessen! Daikaijuu
| image          = Grendizer Getter Robot G Great Mazinger - Kessen! Daikaijuu (1976).jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Masayuki Akehi
| producer       = Chiaki Imada
| writer         = Susumu Takaku
| screenplay     = 
| story          =
| based on       =  
| narrator       = 
| starring       = 
| music          = Shunsuke Kikuchi, Michiaki Watanabe
| cinematography = 
| editing        = 
| studio         = Toei Doga, Dynamic Planning
| distributor    = 
| released       =  
| runtime        = 31 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

  is an animated film produced by  ,   and some parts of Canada.

==Story== oil around the planet, until it starts to head for Tokyo. However, despite this Japan has some aces in his sleeves: Grendizer, Great Mazinger and Getter Robo G, which are immediately sent to stop the monstrous animal. Unfortunately, the awkward Boss Borot joins the team and ends up being swallowed by the Dragosaurus rendering all more difficult. Boss Borot eventually manages to escape the creatures stomach thanks to Great Mazinger. As the three super robots try to fight off Dragonsaurus the animal makes its way to Tokyo and goes on a rampage. Dragonaurus is eventually killed off after Getter Robo G uses the Shine Spark to ignite the large quantities of oil in its body, saving the city.

===Dragonsaurus=== regeneration that allows it to regrow its seven serpent-like tentacles in a matter of seconds and is strong enough to make it extremely resistant if not immune to electric attacks like Double Spazers Cyclone Beam and acid attacks like Grendizers Melt Shower. Dragonsauruss body also has a high resistance to beams and absorbs projectiles such as missiles through its skin where they fail to detonate. To move on land Dragonsaurus possesses the ability to levitate, but also uses its tentacles to create hurricane force winds.

Dragonsaurus later appears in various Super Robot Wars titles as a boss and in Super Robot Wars Reversal and Super Robot Wars MX is given a powerful sonar attack.

==Staff==
*Production: Studio: Toei Doga, Dynamic Planning
*Original work: Go Nagai, Ken Ishikawa, Dynamic Production
*Director: Masayuki Akechi
*Assistant director: Kazumi Fukushima
*Animation director: Tatsuji Kino
*Scenario: Susumu Takahisa
*Planning: Ken Ariga, Katsuya Oda
*Producer: Chiaki Imada
*Executive producer: Saburo Yokoi
*Art director: Mataharu Urata
*Music: Shunsuke Kikuchi, Michiaki Watanabe
*Theme Song:   (lyrics by Kogo Hotomi)
*Theme Song Performance: Isao Sasaki, Columbia Yurikago-kai
*Cast: Kei Tomiyama (Daisuke Umon/Duke Fleed), Hiroya Ishimaru (Koji Kabuto), Keiichi Noda (Tetsuya Tsurugi), Akira Kamiya (Ryo Nagare), Junji Yamada (Hayato Jin), Joji Yanami (Dr. Umon / Dr. Yumi), Kosei Tomita (Dr. Saotome), Minori Matsushima (Sayaka Yumi), Rihoko Yoshida (Michiru Saotome)

==See also==
*UFO Robot Grendizer
*Getter Robo G
*Great Mazinger

==External links==
* 
*    at allcinema
*  at Animemorial
*  at Toeis corporate website
*    at the EncicloRobopedia website

 
 

 
 
 
 
 
 