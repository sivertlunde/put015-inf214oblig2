Long Lost Father
{{infobox film
| name           = Long Lost Father
| image          = 
| imagesize      =
| caption        =
| director       = Ernest B. Schoedsack
| producer       = Merian C. Cooper Kenneth Macgowan Dwight Taylor(screenplay)
| starring       = John Barrymore Helen Chandler
| music          = Alberto Columbo Max Steiner Harold Adamson Roy Webb
| cinematography = Nicholas Musuraca
| editing        = Paul Weatherwax
| distributor    = RKO
| released       = January 19, 1934
| runtime        = 7 reels
| country        = United States
| language       = English
}} 1934 drama Donald Cook, Alan Mowbray, and Doris Lloyd. It was directed by Ernest B. Schoedsack. 

==Cast==
*John Barrymore - Carl Bellairs
*Helen Chandler - Lindsey Lane Donald Cook - Bill Strong
*Alan Mowbray - Sir Tony Gelding Claude King - The Inspector
*E. E. Clive - Spot Hawkins
*Reginald Sharland - Lord Vivian
*Ferdinand Gottschalk - The Lawyer
*Natalie Moorhead - Phyllis Mersey-Royds
*Doris Lloyd - The Blonde Wido
*Phyllis Barry - Party Guest
*Tempe Pigott - Mrs. Gamp - The Old Woman
*Herbert Bunston - the Bishop
*Charles Irwin - Mr. Chisholm
*John Rogers - Mr. Arno

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 