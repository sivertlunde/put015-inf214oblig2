Een Jongmensch...
 

{{Infobox film
| name           = Een Jongmensch...
| image          =
| image_size     =
| caption        =
| director       = Willy Mullens
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =  
| cinematography =
| editing        =
| distributor    =
| released       = 1907
| runtime        = Netherlands
| language       = Silent
| budget         =
| amg_id         =
}} 1907 Netherlands|Dutch silent film directed by Willy Mullens.

==Plot==
The film relates the short story of a young man who is suspended from a tree in Muiden Forest after being rejected by the girl of his dreams and decides to himself to hang from the highest tree in the local park. Soon he is discovered by the park and a great fuss is created for him to come down. The young man survives the ordeal..

In France a film also circulated  with the same plot twist called Le Pendu.

==See also==
* List of Dutch films before 1910

 
 
 
 
 


 