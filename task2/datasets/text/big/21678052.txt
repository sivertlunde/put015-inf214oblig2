The Twilight Saga: Eclipse
 
{{Infobox film
| name           = The Twilight Saga: Eclipse
| image          = Eclipse Theatrical One-Sheet.jpg
| caption        = Theatrical release poster
| alt            =  
| director       = David Slade
| producer       = Wyck Godfrey Karen Rosenfelt
| screenplay     = Melissa Rosenberg
| based on       = Eclipse (Meyer novel)|Eclipse  by Stephenie Meyer Billy Burke Dakota Fanning
| music          = Howard Shore
| cinematography = Javier Aguirresarobe
| editing        = Nancy Richardson  Art Jones
| studio         = Temple Hill Entertainment Maverick Films Imprint Entertainment Sunswept Entertainment
| distributor    = Summit Entertainment
| released       =  
| runtime        = 123 minutes 
| country        = United States
| language       = English
| budget         = $68 million   
| gross          = $698.4 million 
}}
 greenlit the film in February 2009.  Directed by David Slade, the film stars Kristen Stewart, Robert Pattinson, and Taylor Lautner, reprising their roles as Bella Swan, Edward Cullen, and Jacob Black, respectively.    Melissa Rosenberg, who penned the scripts for both Twilight and New Moon, returned as screenwriter. 
Filming began on August 17, 2009, at Vancouver Film Studios,    and finished in late October, with post-production began early the following month.    Bryce Dallas Howard was cast as Victoria (Twilight)|Victoria, replacing Rachelle Lefevre who previously played her.

The film was released worldwide on June 30, 2010 in theatres, and became the first Twilight film to be released in  s $62 million.    Eclipse has also become the film with the widest independent release, playing in over 4,416 theaters, surpassing its predecessor, The Twilight Saga: New Moon, which held the record since November 2009. 

==Plot== Victoria (Bryce Riley Biers Charlie Swan Billy Burke) investigates the disappearance of Riley Biers, Edward suspects his disappearance was caused by the newborn vampires, furthering his suspicions is Rileys intrusion into Bellas room.

Although Edward fears for her safety, Bella insists that Jacob Black (Taylor Lautner) and the rest of the werewolf pack would never harm her, but Edward is still not convinced. Bella goes to La Push to see Jacob, and returns home unharmed. During one of her visits, Jacob confesses that he is in love with Bella, and forcefully kisses her. Furious, she punches him and sprains her hand, and Edward later threatens Jacob and tells him to only kiss her if she asks him to. Bella even revokes the invitations of Jacob and his pack members to her graduation party, but when Jacob apologizes for his behavior, she forgives him.
 Alice (Ashley Quil (Tyson Embry (Kiowa Jasper (Jackson Rathbone) explains to Bella that he was created by a vampire named Maria to control a newborn army. He hated his original existence and upon meeting Alice, joined the Cullens with her. Bella sees the true bond between a mated vampire pair and begins to understand Jasper better. Despite her reluctance to marry, Bella realizes that spending eternity with Edward is more important to her than anything else and agrees to marry him. Edward and Bella camp up in the mountains to hide Bella from the bloodthirsty newborns. During the night, Bella overhears a conversation between Edward and Jacob, in which they temporarily put aside their hatred towards each other. In the morning, Jacob overhears Edward and Bella discussing their engagement and storms off, angrily. Bella desperately asks him to kiss her, and she realizes that she has fallen in love with him. Edward finds out about the kiss but is not upset, as Bella says she loves him more than Jacob.
 Bree Tanner (Jodelle Ferland), who had refused to fight and surrendered to Carlisle. Jane (Dakota Fanning) tortures Bree to get information, then instructs Felix to kill her, despite the Cullens efforts to spare her. When Jane notes that Caius will find it interesting that Bella is still human, Bella informs her that the date for her transformation has been set. Bella visits the injured Jacob to tell him that even though she is in love with him, she has chosen to be with Edward. Saddened by her choice, Jacob reluctantly agrees to stop trying to come between her and Edward.

Bella and Edward go to their meadow, where she tells him she has decided to do things his way: get married, make love, then be transformed into a vampire. She also explains that she never has been normal and never will be; shes felt out of place her entire life, but when she is in Edwards world she feels stronger and complete. At the end of the story they decide they need to tell Charlie about their engagement.

==Cast==
 
;Main cast
* Kristen Stewart as Bella Swan, who finds herself surrounded by danger and targeted by the vengeful vampire Victoria. In the meantime, she must choose between her love for vampire Edward Cullen and her friendship with werewolf Jacob Black. 
* Robert Pattinson as Edward Cullen, Bellas vampire boyfriend who is capable of reading minds, except for Bellas. In New Moon, Edward left Bella, and now he has returned to try to stay a part of her life. 
* Taylor Lautner as Jacob Black,  a werewolf in whom Bella found solace during Edwards absence in New Moon. Now, Edward has returned to Bellas life permanently, and Jacob is looking for ways to prove that he is a better choice for her.

;Supporting characters
* Peter Facinelli as Carlisle Cullen, a compassionate doctor who acts as a father figure to the Cullen coven. He is the one that created the Cullen family except for Alice and Jasper. 
* Elizabeth Reaser as Esme Cullen, a loving mother figure of the Cullen coven.       Alice Cullen, a member of the Cullen family who can see "subjective" visions of the future and who is close friends with Bella.   
* Kellan Lutz as Emmett Cullen, the strongest member of the Cullen family, and provides comic relief.   
* Nikki Reed as Rosalie Hale, who was raped by her fiancé and left to die before she became a vampire. She also feels that Bella is making a mistake for choosing to live the life of a vampire before she could live a full human life.   
* Jackson Rathbone as Jasper Hale, a civil war fighter who was turned into a vampire to train newborns. Hes also a member of the Cullen coven who trains his family to fight newborn vampires and can feel/control/manipulate emotions.    Billy Burke Charlie Swan, Bellas father and Forks Chief of Police. Burke admits he has not read the Twilight books, saying, "We cant make the book, were making the movie", and that he works from the scripts. 
* Bryce Dallas Howard as Victoria (Twilight)|Victoria, a vampire who wants to kill Bella to avenge her mate, James, whom she supposes to be killed by Edward in the first Twilight film.
* Dakota Fanning as Jane, loyal servant to the Volturi.
* Cameron Bright as Alec, Janes Twin, loyal servant to the Volturi. Riley Biers, the one Victoria changed to help her form an army of newborn vampires.
* Jodelle Ferland as Bree Tanner a newborn vampire created to fight the Cullens, in the newborn army.
* Sarah Clarke as List of Twilight characters#Renée Dwyer|Renée Dwyer, Bellas mother who lives in Jacksonville, Florida with her husband Phil.
* Anna Kendrick as Jessica Stanley, one of Bellas friends in Forks. Michael Welch as Mike Newton, one of Bellas friends in Forks. Mike has a crush on Bella, and does not like Edward.
* Catalina Sandino Moreno as Maria, the vampire that turns Jasper into a vampire.

==Production==

===Development===
 

In early November 2008, Summit announced that they had attained the rights to the remaining books in  ,   would be in post-production for New Moon when Eclipse began shooting, he would not be directing the third film.   Instead, the film would be helmed by director David Slade, with Melissa Rosenberg returning as screenwriter. David Slade dove right into the project, interviewing cast members individually between two and three times to discuss characters and the plot. 

===Casting===
Summit Entertainment revealed that they would replace Rachelle Lefevre, who played an evil vampire named Victoria, with Bryce Dallas Howard in late July 2009. They attributed the change to scheduling conflicts, and Lefevre responded by saying she was "stunned" and "greatly saddened" by the decision.  Howard had previously rejected the role of Victoria as "too small of a part" when she was approached to play her in Twilight. 
 Silent Hills Bree Tanner. Seth Clearwater. 

Actors who auditioned for the various roles were not given a script to work from. Instead, actress Kirsten Prout mentioned, "they made the scenes exact transcripts from the book.... They didnt give the screenplay out. So, the audition side was just reading a page of Twilight and reading the lines that were interspersed between the descriptions."   

===Filming and post-production=== Billy Burke, wrapped up on October 29, 2009, while post-production began in late November.  Slade published multiple updates on his Twitter account proclaiming that editing was going well.  He said the "story and the way   approached the film calls for a more realistic approach."  In April 2010, it was revealed that reshoots to the film were needed. Both Slade and Stephenie Meyer were present at the shoot along with the three main stars. 

In January 2010, an early draft of the films script was leaked on the Internet.    The script presumably belonged to star Jackson Rathbone, as his name was watermarked across each page. 

===Music===
 
 The Aviator. Chop Shop label.  The lead single from the soundtrack is "Neutron Star Collision (Love Is Forever)", performed by the British band Muse (band)|Muse.   

On May 11, 2010, MySpace announced that the full Eclipse soundtrack listing would be unveiled starting at 8 a.m. the following morning every half-hour, totaling six hours.  The album debuted at #2 on Billboard 200. 

==Distribution==

===Marketing===
On November 5, 2009, the   was released on DVD and Blu-ray; the Walmart Ultimate Fan Edition includes a 7-minute first look at Eclipse.  On March 23, the second poster for the film was released.  The final Eclipse trailer debuted on The Oprah Winfrey Show, and in promotion for the movie, Robert Pattinson, Kristen Stewart, Taylor Lautner, and Dakota Fanning made a guest appearance on the show May 13; the audience also viewed a version of the film.  On June 6, 2010, a sneak peek of the film was shown at the 2010 MTV Movie Awards; that same week, more clips and TV spots were released also. 

In order to tie in the lunar eclipse on June 26, 2010, Summit Entertainment hosted screenings of the first two films in The Twilight Saga (film series)|The Twilight Saga film series in twelve cities throughout the United States. The event was streamed live from Philadelphia and San Diego, and included cast member appearances and special previews of Eclipse. 

Nordstrom and Summit Entertainment joined together to sell a fashion collection inspired by the film, as was done for the previous installment. Created by Awake Inc., the collection is based on Ashley Greenes character, Alice, and Kristen Stewarts character, Bella. The Eclipse collection became available on June 4, 2010.  In a similar style to its New Moon marketing, Burger King started promoting the film on Monday, June 21, 2010. Their promotion heavily focuses on the "Team Jacob vs. Team Edward" aspect of the film. 

===Release=== Los Angeles Nokia Theatre.   Fans had the option of lining up starting on June 21, 2010, at the Nokia Plaza in Los Angeles before changing location on June 23.  An official United Kingdom premiere was held in Leicester Square, London on July 1, 2010. However, Kristen Stewart, Robert Pattinson and Taylor Lautner were not present. 
  (Syracuse, Kansas) in August 2010.]]
Eclipse opened in 4,416 theaters and 193 IMAX screens. With that, early predictions forecasted the film will gross anywhere from $150 million to $180 million within its first six days of release, putting the record set by  s in danger of being broken.   Eclipse accounted for 82 percent of Fandangos online ticket sales, reaching the top five on May 14, 2010.  MovieTickets.com stated that Eclipse was the top advance ticket seller on its site, with more than 50 percent of daily ticket sales.  The film was the top advance ticket seller as of June 2010.  Early ticket sales for the film also have broken records for Gold Class Cinemas, where more than 8,500 Twilight fans have reserved tickets; the Fairview, TX location sold out their showings of Eclipse for June 30. 

The film was re-released into theaters on September 13, 2010 in recognition of lead character Bella Swans birthday. 

===Home media===
The Twilight Saga: Eclipse was released on   and   and commentaries by Kristen Stewart and Robert Pattinson, Stephenie Meyer and Wyck Geoffery.  It was released on December 1, 2010 in New Zealand and Australia. There is also a "gift set" Two-Disc Collectors Edition which features a unique packaging and 6 collectible photo cards.  In North American DVD sales, the film has currently grossed $164,676,695 and has sold more than 9,424,505 units. 

==Reaction==

===Box office===
Eclipse set a new record for the biggest midnight opening in the United States and Canada in  , with $26.3 million in 3,514 theaters.  It held the record until the summer of 2011, when it was broken by   ($30.3 million).  The movie also surpassed   in total grosses for a midnight screening in IMAX. Eclipse garnered more than $1 million at 192 theaters, while Revenge of the Fallen earned $959,000,  until it was beaten five months later by Harry Potter and the Deathly Hallows - Part 1 with $1.4 million.  The film grossed $68.5 million on its opening day in the United States and Canada, becoming the biggest single-day Wednesday opening over Revenge of the Fallens $62 million,  and the third biggest single-day opening ever at the time.  As of 2011, the film has the third highest opening day gross of the series behind New Moon ($72.7 million) and Breaking Dawn - Part 1 ($72.0 million).  Furthermore, the film earned $9 million at various IMAX locations during its first week. 
 Independence Day with a total of $176.4 million, including $64.8 million during its first weekend.  In its second weekend, the film fell 51%, a better standing than its predecessors, grossing an estimated $31.7 million. 

The film opened overseas with $16.2 million, beating records set by the films predecessor in Russia with an estimated $3.9 million (since surpassed by   which earned $5 million), in Italy with an estimated $3.1 million, in the Philippines, grossing $1.2 million, and in Belgium, where it grossed an estimated $1.1 million. It is the third-best opening day ever in Italy; in the Philippines, Eclipse topped Spider-Man 3 for best opening day ever, and was the highest opening day ever in Belgium.  In three days, Eclipse topped the box office with $121.3 million  and during its first weekend it earned $71.3 million.

Overseas in its second weekend, the film grossed $70.6 million from 9,440 screens in 63 markets, a 1% drop from its first weekend. The film opened in the United Kingdom at number one, grossing $20.7 million from 523 locations (including previews), the markets biggest opening of 2010 (until  s ($15.4 million). The film opened at number one in South Korea with $4.9 million. 

The film ended its box-office run in the U.S.A. and Canada on October 21, 2010 having grossed $300,531,751, surpassing its predecessor   which grossed $296,623,634 a few months before, to become the highest-grossing film of the  s $413,203,156. Therefore, internationally, Eclipse remains the second highest-grossing film in the franchise with $693,579,566 against New Moons $709,826,790.  Eclipses highest-grossing markets except the U.S.A. and Canada are the United Kingdom|UK, Ireland and Malta ($45,709,785), Germany ($33,087,955), France and the Maghreb region ($32,987,421), Italy ($19,984,000), Brazil ($30,499,010) and Australia ($28,566,737). 

===Critical response===
 

Reviews for the film were mixed, but more favorable than  .  Review aggregation website Rotten Tomatoes gives the film a score of 49% based on 229 reviews.    The sites general consensus is that, "Stuffed with characters and overly reliant on uninspired dialogue, Eclipse wont win The Twilight Saga many new converts, despite an improved blend of romance and action fantasy."    Review aggregation website Metacritic, which assigns a weighted mean rating out of 100 reviews from film critics, the film holds a rating score of 58/100 based on 38 reviews, indicating "mixed or average reviews". 

The Hollywood Reporter posted a positive review of Eclipse, saying the film "nails it".  Peter Debruge of Variety (magazine)|Variety reports that the film "finally feels more like the blockbuster this top-earning franchise deserves". 

Rick Bentley of  . Hes got a real challenge to make movies as good as Eclipse."  The New York Times praised David Slades ability to make an entertaining film, calling it funny and better than its predecessors, but wrote that the acting has not improved much.  Giving the film 4.5 out of 5 stars, Betsey Sharkey from The Los Angeles Times praised David Slades method of blending his previous works to form a funny movie. She stated, "Eclipse eclipse  its predecessors."  
   The film was also listed in 49th place by Moviefone on its list of the 50 best movies of 2010. 

Roger Moore of the Orlando Sentinel gave the film 2.5 out of 4 stars, stating, "The dullness of the performances really stands out when somebody like Bryce Dallas Howard, or Anna Kendrick turn up and liven up their scenes." While calling the film "too chatty and too long", he did compliment David Slades directing and noted that the movie will please the fans.  Michael Phillips of the Chicago Tribune gave the film 2 out of 5 stars, stating that David Slades pacing is "everything like molasses running uphill". He also criticized the characters, the actors portraying them, the big close-ups of hand-held devices, and called Howard Shores score "gunk".  Wesley Morris from the Boston Globe stated, "If the first two movies were "get a room," part three is "get a therapist". He said the second and third film "repeat that discovery   without truly deepening it...the movies are interesting without ever being good." 

A mixed review said that while "Eclipse restores some of the energy New Moon zapped out of the franchise and has enough quality performances to keep it involving", the film "isnt quite the adrenaline-charged game-changer for love story haters that its marketing might lead you to believe. The majority of the action remains protracted and not especially scintillating should-we-or-shouldnt-we conversations between the central triangle."  that guy on TV who shows you how to paint stuff like that." He also predicted that a lack of understanding for the film series in general would not bode well with the audience, stating, "I doubt anyone not intimately familiar with the earlier installments could make head or tails of the opening scenes." He gave the film 2 stars out of 4.  Steve Persall of the St. Peterburg Times called the movie "just monstrously bad", and said, "Eclipse leaves the sputtering story arc in idle, with only an uneasy truce between the vampire and werewolf clans amounting to anything new" and rating it grade C-.  The Guardians columnist Peter Bradshaw gave the film a one-star rating in a review that lampooned Bellas continued abstinence, among other plot elements. Bradshaw, dubbing the series "The epic of the unbroken duck", wrote that "Bella Swan is starting to make Doris Day look like the nympho from hell", and concluded that "it could be time to sharpen the wooden stake." 

===Accolades===
{| class="wikitable"
|-
!Year
!Ceremony
!Award
!Result
|- 2010
||National Movie Awards Most Anticipated Movie Of The Summer
|rowspan=5  
|- Teen Choice Awards Choice Summer: Movie
|- Choice Summer Movie Star - Female: Kristen Stewart
|- Choice Summer Movie Star - Male: Robert Pattinson
|- Choice Movie Actor: Fantasy : Taylor Lautner
|- Choice Summer Movie Star - Male: Taylor Lautner
|rowspan=3  
|- Choice Love Song: Neutron Star Collision (Love Is Forever)
|- Scream Awards The Ultimate Scream
|- Best Fantasy Movie
|rowspan=3  
|- Best Fantasy Actress: Kristen Stewart
|- Best Fantasy Actor: Robert Pattinson
|- Best Fantasy Actor: Taylor Lautner
|rowspan=2  
|- Best Breakthrough Performance - Male: Xavier Samuel
|- Nickelodeon Australian Kids Choice Awards 2010 Favourite Movie
| 
|- Favorite Movie Star: Kristen Stewart
|rowspan=6  
|- Favorite Movie Star: Robert Pattinson
|- Favorite Movie Star: Xavier Samuel
|- Hottest Hottie: Taylor Lautner
|- Fave Kiss: Kristen Stewart & Robert Pattinson
|- Fave Kiss: Kristen Stewart & Taylor Lautner
|- Nickelodeon (Brazil)|Brazilian Kids Choice Awards 2010 Couple of the Year : Kristen Stewart & Robert Pattinson
| 
|- American Music Awards Favorite Soundtrack
|rowspan=3  
|- Satellite Awards Best Original Song: Eclipse (All Yours)
|- Best Original Song: What Part of Forever
|- 2011
|rowspan=6|Peoples Choice Awards Favorite Movie
|rowspan=3  
|- Favorite Drama Movie
|- Favorite Movie Actress: Kristen Stewart
|- Favorite Movie Actor: Robert Pattinson
|rowspan=2  
|- Favorite Movie Actor: Taylor Lautner
|- Favorite On-Screen Team: Kristen Stewart, Robert Pattinson and Taylor Lautner
| 
|- Grammy Awards Best Compilation Soundtrack Album for a Motion Picture, Television, or Other Visual Media
|rowspan=9  
|- Golden Raspberry Awards Worst Picture
|- Worst Director: David Slade
|- Worst Actor: Taylor Lautner
|- Worst Actor: Robert Pattinson
|- Worst Actress: Kristen Stewart
|- Worst Screenplay: Melissa Rosenberg
|- Worst Prequel, Remake, Rip-Off or Sequel
|- Worst Screen Ensemble
|- Worst Supporting Actor: Jackson Rathbone
| 
|- Nickelodeon Kids Choice Awards|Kids Choice Awards Favorite Movie Actress: Kristen Stewart
| 
|- MTV Movie Awards Best Movie
|rowspan=3  
|- Best Female Performance: Kristen Stewart
|- Best Male Performance: Robert Pattinson
|- Best Male Performance: Taylor Lautner
|rowspan=2  
|- Best Breakout Star: Xavier Samuel
|- Best Fight: Robert Pattinson, Bryce Dallas Howard and Xavier Samuel
|rowspan=2  
|- Best Kiss: Kristen Stewart & Robert Pattinson
|- Best Kiss: Kristen Stewart & Taylor Lautner
|rowspan=5  
|- 37th Saturn Awards Best Fantasy Film
|- Teen Choice Awards
| 
|- Choice Movie Actress: Sci-Fi/Fantasy: Kristen Stewart
|-
| : Robert Pattinson
|- Choice Movie Actor: Sci-Fi/Fantasy: Taylor Lautner
| 
|-
| 
| 
|-
| 
|rowspan=2  
|-
| 
|- Choice Movie Liplock: Kristen Stewart & Robert Pattinson
|rowspan=3  
|- Choice Movie Liplock: Kristen Stewart & Taylor Lautner
|- Choice Movie Breakout Star - Male: Xavier Samuel
|- Choice Vampire: Robert Pattinson
| 
|- Choice Vampire: Nikki Reed
|rowspan=3  
|- Choice Male Hottie: Robert Pattinson
|- Choice Male Hottie: Taylor Lautner
|}

==Sequels==
 

  was released on November 18, 2011 and   on November 16, 2012   with Bill Condon directing, and author Stephenie Meyer co-producing.   

==See also==
* Vampire film

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 