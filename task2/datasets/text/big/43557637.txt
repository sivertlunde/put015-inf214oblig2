Pink Bomb
 
 
{{Infobox film
| name           = Pink Bomb
| image          = PinkBomb.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 人生得意衰盡歡
| simplified     = 人生得意衰尽欢
| pinyin         = Rén Shēng Dé Yì Shuāi Jìn Huān
| jyutping       = Jan4 Sang1 Dak1 Ji3 Seoi1 Zeon6 Fun1 }}
| director       = Derek Chiu
| producer       = Chow Wah Yu
| writer         = 
| screenplay     = Dayo Wong
| story          = 
| based on       =  Lau Ching Rachel Lee Fennie Yuen
| narrator       = 
| music          = Wong Yiu Kwong
| cinematography = Ally Wong Yuen Kwok Fai
| editing        = Cheng Keung
| studio         = Super Power Motion Picture Mandarin Films
| released       =  
| runtime        = 95 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$51,329
}}
 Hong Kong Lau Ching Rachel Lee and Fennie Yuen.

==Plot== Rachel Lee), Lau Ching Wan) and former triad member Rascal King (Dayo Wong) goes to Thailand for vacation led by tour guide Graham (Waise Lee), a born again Christian. There, they rescue a prostitute Ann (Gloria Yip) who was beaten up by brothel frequenters for scamming their money. Ann stole a case of money which turns out to be US$3 million counterfeit currency. During the tour, they were constantly being chased by gangsters wanting to silence them. During this critical moment, Graham reveals his true identity as a secret agent.

==Cast==
*Waise Lee as Graham
*Cynthia Khan as Leung Chi Kwan Rachel Lee as Lee Sin Yin Lau Ching Wan as Daniel
*Fennie Yuen as Yip Yuk Hing
*Dayo Wong as Rascal King
*Cheung Kwok Leung as Fake Cop
*Gloria Yip as Ann
*Fung Hak On as Brother Black
*Helen Poon
*Baby Bo
*Ken Lok
*Lau Siu Kwan
*Ma On
*Garry Chan
*Jack Wong
*Ling Lai Man
*Sung Boon Chung
*Leung Kai Chi
*William Chu as Siu Ming
*Hui Sze Man as Siu Mings mother
*San Tak Kan
*Ah Sing

==Reception==
===Critical===
  gave the a mixed review and describes it as "Amusing, but only occasionally. And probably not worth breaking your back to find."    also gave the film a mixed review and writes "Pink Bomb throws so much nonsense at you, you become mesmerized." 

===Box office===
The film grossed HK$51,329 at the Hong Kong box office during its theatrical run from 27 to 31 March 1993 in Hong Kong.

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 