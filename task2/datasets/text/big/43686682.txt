My Voice, My Life
 
 
 
{{Infobox film
| image          =  
| name           = My Voice, My Life 《爭氣》／《争气》
| director       = Ruby Yang
| producer       = Ruby Yang Ada Ho
| music          = Brian Keane Robert Ellis-Geiger
| cinematography = Siu Ki Yip
| editor         = Man Cheung Ma 
| released       =  
| runtime        = 91 minutes
| country        = Hong Kong
| language       = Cantonese
}}

My Voice, My Life 《爭氣》is a feature-length documentary film directed by Oscar-winning Chinese-American filmmaker Ruby Yang.  It tells the poignant stories of a group of under-privileged Hong Kong youngsters who underwent six months of vigorous trainings to produce a musical on stage.  Through their trials and tribulations, the students challenge parents, teachers and policy makers to reflect on our way of nurturing the next generation.

The film is presented by the  , in association with the  .

Ruby Yangs recent works have mainly focused on social issues in mainland China, such as The Blood of Yingzhou District, which won the 2006 Academy Award for Best Documentary Short Subject and The Warriors of Qiugang, which was nominated Academy Award for Best Documentary Short Subject in 2011.

My Voice, My Life premiered on 7 October 2014. It opened one week later in 15 theaters in Hong Kong and Macau.

==Plot summary==
"My Voice, My Life" follows an unlikely group of misfit students from four Hong Kong schools cast together for a musical theater performance. From low self- esteem to blindness, each student confronts unique personal challenges in the process of developing his or her character. Teachers and administrators question whether this ragtag band will be able to work together, much less put on a successful show.

Many of the musical theater troupe’s students come from Hong Kong’s least-desirable, "Band 3" secondary schools,  which admit the territory’s academically under-performing students. Others come from a school for the blind that seeks to teach its students how to perform basic tasks and function in the sighted world. Brought together to sing, dance, and act, the students question their own abilities and balk at the spotlight.

==External links==
* , official website.
* , Facebook page.
* 
* 

==See also==
*Hong Kong Education System - Hong Kong Education Bureau  
*The education system in Hong Kong - Introduction  
*The long and winding road of Hong Kong’s education changes  
*香港中學學位分配辦法  
*中學「統一派位」重點分析  

==References==
 

 
 
 
 
 
 
 