Superman and the Mole Men
{{Infobox film
| name           = Superman and the Mole Men
| image          = Supermanmolemen.jpg
| caption        = Theatrical release poster
| director       = Lee Sholem
| producer       = Barney A. Sarecky
| writer         = Richard Fielding
| based on       =   Joe Shuster}}
| starring       = George Reeves Phyllis Coates
| music          = Darrell Calker Walter Greene
| cinematography = Clark Ramsey
| editing        = Albrecht Joseph
| distributor    = Lippert Pictures Inc.
| released       =  
| runtime        = 58 minutes
| country        =  
| awards         = 
| language       = English
| budget         = 
}} serial format. Robert Maxwell Adventures of Superman, for which it became the only two-part episode, entitled "The Unknown People".

==Plot==
Mild-mannered reporter Clark Kent and Lois Lane are sent to the small town of Silsby for the inauguration of the worlds deepest oil well. Unbeknownst to the drillers, however, the drill shaft has penetrated the underground home of the "Mole Men", a race of small, furry (though bald-headed) beings. The Mole Men come up through the shaft at night to explore the surface. When the creatures first emerge from the shaft, their sudden appearance scares the elderly night watchman to death. Lois Lane and Clark Kent soon arrive at the oil well and find the dead watchman. Subsequently, help arrives and while Clark Kent and the foreman are exploring the surrounding area for signs of intruders; Lois sees one of the creatures and screams in terror. But, no one believes her when she tells them what she saw.

After the medical examiner who was summoned to the scene leaves with Lois, Clark stays behind to confront the foreman. The foreman confesses that the well was closed down for fear that they had struck radium and not oil. The foreman proceeds to show Clark ore samples that were collected during different stages of drilling; all of these glow brightly.  

Their peculiar appearance, plus the fact that everything they touch then becomes phosphorescent and glows in the dark combined with the creatures being spotted by several townspeople, scares the townspeople into forming an angry mob, led by the violent Luke Benson, in order to kill the "monsters". Superman is the only one able to resolve the conflict, saving one of the creatures from falling into the towns water supply after he has been shot by taking him to hospital, while the other is chased away. 

Later a doctor reveals that unless the creature undergoes surgery to remove the bullet, he will die. Clark Kent is forced to assist the doctor when the nurse refuses to out of fear of the creature. Soon afterward, Bensons mob shows up at the hospital demanding to have the creature turned over to them, leading Superman to stand guard outside the hospital. Lois Lane stands at Supermans side, until a shot is fired from the mob narrowly missing Lois. Superman sends her inside and begins to relieve the mob of their guns, sending the mob away. Later three more mole creatures emerge from the drill shaft, this time bearing a weapon. They make their way to the hospital. Benson and his mob see the creatures and Benson goes after them alone, but when the creatures see him they use their weapon and fire on him. Superman sees this and quickly jumps in front of the blast, saving Bensons life, which Superman says is "More than you deserve!". He fetches the wounded creature and returns him and his companions to the shaft. Soon after they destroy the shaft completely, making certain that no one or thing can come up or go down it ever again. Lois observes, "Its almost as if they were saying, You live your lives....and well live ours".

==Cast==
 
*George Reeves as Clark Kent / Superman
*Phyllis Coates as Lois Lane
*Jeff Corey as Luke Benson Walter Reed as Bill Corrigan
*J. Farrell MacDonald as Pop Shannon
*Stanley Andrews as The Sheriff
*Ray Walker as John Craig
*Hal K. Dawson as Chuck Weber
*Phil Warren as Deputy Jim
*Frank Reicher as Hospital Superintendent
*Beverly Washburn as Child
*Billy Curtis as a Mole-Man
*Jerry Maren as a Mole-Man
*John T. Bambury as Mole-Man; uncredited

==Themes==
As with many of the early episodes of the Adventures of Superman, the film is adult-themed, with a good deal of conflict and violence, or the threat thereof, and is played with total seriousness by all the actors; Reevess Superman, in particular, is all business, displaying none of the humor that the character would develop over time in the TV series.

The sympathetic view of the strangers in this film, and the unreasoning fear on the part of the citizenry, has been compared by author Gary Grossman to the panicked public reaction to the peaceful alien Klaatu in the film The Day the Earth Stood Still, which was released the same year. Both films have been seen retrospectively as a product of (and a reaction to) the "Red Scare" of post-World War II. Grossman also cites a later film perhaps inspired by this one, called The Mole People.

==Production notes==
*The central image of Reeves and Coates, on the poster shown here, is a painting derived from flipping a publicity photo of the two, with the "S" shield flipped back in order to read correctly. The photo depicts the final scene, shown in the screen capture at right.
*Some elements were trimmed from the film when it was converted into "The Unknown People", including some portions of a lengthy chase scene, and all references to the term "Mole Men".
*The theme music used for this film had a generic "sci-fi sound", with nothing suggesting a specific "Superman theme". The title cards were similarly generic, with low-grade animation of Saturn-like ringed planets and comets sailing by.
*The score was also changed when the film was re-cut into its two-part TV episode. The film featured an original score by Darrell Calker (Woody Woodpecker), which was removed and replaced with music from the production music library used for the first television seasons presentation.
*The laser weapon shown in the poster, which the Mole Men brought up from their subterranean home in order to defend themselves and retrieve their injured comrade, was a prop constructed from an Electrolux vacuum cleaner.

==Home video releases==
The film was released on  . Subsequently, it was included in a similar Blu-Ray edition.

==In popular culture==
* During the DC vs. Marvel crossover event, Marvel supervillain the Mole Man and his minions attempt to take over the Batcave, only to be opposed by the Incredible Hulk.  Superman then joins the fight, prompting Hulk to comment "Superman versus the Mole Men.  This should be interesting."

==References==
*Superman: Serial to Cereal, by Gary Grossman, 1976.

== External links ==
*  

 
 

 
 

 
 
 
 
 
 
 
 