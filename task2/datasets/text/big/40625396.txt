In My Skin
{{Infobox film
| name           = In My Skin
| image          = InMySkinPoster.jpg
| caption        = 
| director       = Marina de Van
| producer       = Laurence Farenc
| writer         = Marina de Van
| starring       = Marina de Van Laurent Lucas
| music          = Esbjorn Svensson
| cinematography = Pierre Barougier
| editing        = Mike Fromentin
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
In My Skin (  film written by, directed by, and starring Marina de Van.  It details the downward mental spiral of Esther, a woman (played by Marina de Van) who engages in increasingly destructive acts of self-harm|self-mutilation following an accident that injures her leg at a party.

==Synopsis==
For public relations employee Esther (Marina de Van), life seems to be perfect. Not only is her boyfriend willing to take their relationship to the next step, but shes likely to gain a promotion and a prestigious new client. This seemingly perfect life is shattered when she receives a horrible leg injury at a party. While she does go to a doctor, Esther chooses not to let her wound heal cleanly and as a result, her leg becomes horribly scarred. As time passes Esther begins to notice something strange with her arm and begins a downward spiral that threatens to ruin her entire life.

==Cast==
*Marina de Van as Esther
*Laurent Lucas as Vincent
*Léa Drucker as Sandrine
*Thibault de Montalembert as Daniel
*Dominique Reymond as La cliente
*Bernard Alane as Le client
*Marc Rioufol as Henri
*François Lamotte as Pierre
*Adrien de Van as Linterne
*Alain Rimoux as Le pharmacien

==Reception==
Critical reception for In My Skin was mixed to positive and the film currently holds a rating of 68 on Metacritic based on 18 reviews.  The movie also holds a rating of 63% "fresh" on Rotten Tomatoes based on 41 reviews.  

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 


 