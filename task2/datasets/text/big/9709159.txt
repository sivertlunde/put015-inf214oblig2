The Policeman
 
{{Infobox film
| name           = The Policeman
| image          = The Policeman.jpg
| caption        = Hebrew-language theatrical poster
| director       = Ephraim Kishon
| producer       = Ephraim Kishon  Itzik Kol  Mati Raz
| writer         = Ephraim Kishon
| starring       = Shaike Ophir  Zaharira Harifai  Joseph Shiloach
| music          = Nurit Hirsh
| cinematography = David Gurfinkel
| editing        = Anna Gurit
| distributor    = Cinema 5 Distributing
| released       =  
| runtime        = 87 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = 
}} Hebrew title title character is played by Shaike Ophir (credited as Shay K. Ophir), in what is considered one of his finest performances.

The film was nominated for the 1972 Academy Award for Best Foreign Language Film,    and won the Golden Globe in the same category. It won several other awards, such as best foreign film in the Barcelona film festival and best director in the Monte Carlo festival. In Israel it is considered a cinematic classic. 

==Plot==
Officer Avraham Azoulay is a patrolman in Tel-Avivs district of Jaffa. He is an honest man, though extremely naive, and because of his character, has never been promoted during his twenty years in the force. He is married to a dull woman (played by veteran actress Zaharira Harifai); the couple have no children. 

His superiors, Captain Levkovich and First Sergeant Bejerano, decide not to renew his contract, though they feel sorry for him. In the meantime, he falls in love with the simple but charming prostitute Mimi, and removes her photograph from the arrests billboard. His wife finds the photo and tears it to pieces, which Azoulay secretly glues together again. Nevertheless, this love will not be realized as Azoulay refuses to divorce his wife, claiming that "it will destroy her". In addition, being a Kohen, he cannot marry a prostitute according to Halakha. 

Azoulay shows some success at dispersing a demonstration without resorting to violence because of his knowledge in the bible and in Yiddish; he also charms a group of visiting French policemen who adore the French-speaking policeman; in an Arab-speaking club house he gives an unaware speech in Arabic. Azoulay is able to see people for what they are and not what they represent. None of these events, however, help to change his superiors decision to dismiss him. Azoulay forms a friendship with Amar, unaware that he is a notorious criminal. The criminal and his associates decide to fake a crime and allow Azoulay to catch them in the act so that he receives a promotion and regain his contract. They finally decide upon stealing ritual objects, including a large golden cross, from a monastery in the neighborhood. Azoulay manages to catch the criminal in the act and is finally promoted to the rank of a sergeant, but his contract is not renewed and he is forced to retire from the police. 

In the final scene officer Azoulay leaves the precinct with his new rank and policemen practicing marches in the courtyard all turn to salute him for the first and last time in his life. The final shot of the film presents Azoulay saluting the marching policemen as his eyes fill with tears. This image became one of the most memorable in Israeli cinema.

==Cast==
* Shaike Ophir as Constable Sgt. Abraham Azulai
* Zaharira Harifai as Betty Azulai
* Avner Hizkiyahu as Capt. Lefkowitch
* Itzko Rachamimov as Senior Sgt. Bejerano
* Joseph Shiloach as Amar
* Nitza Saul as Mimi
* Gabi Amrani as The Yemenite
* Arieh Itzhak as Zion
* Abraham Celektar as Cactus
* Efraim Stan as Horovitz

==See also==
* List of submissions to the 44th Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
  
 