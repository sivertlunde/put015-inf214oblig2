Baat Bann Gayi
 
 
{{Infobox film
| name           = Baat Bann Gayi
| image          = Baat Bann Gayi.Jpeg
| caption        = Official Poster
| director       = Shuja Ali
| producer       = Sayed Asif Jah Megha Agarwal
| starring       = Ali Fazal Gulshan Grover Anisa Butt Amrita Raichand Razak Khan Akshay Singh
| music          = Harpreet Singh
| lyricists      = Ubaid Azam Azmi A M Turaz  Rana
| cinematography = Deepak Pandey
| studio         = Jaypeeco Infotainment ASR Media
| released       =  
| runtime        = 110 minutes
| country        = India
| language       = Hindi
| budget         = 4cr
| gross          = story = Syed Asif Jah writer = Akshay Singh Shuja Ali
}}
Baat Bann Gayi is a Hindi comedy film directed by Shuja Ali, presented by Vibhu Agarwal and produced by Sayed Asif Jah & Megha Agarwal. The film features Ali Fazal, Gulshan Grover, Anisa, Amrita Raichand, Razak Khan and Akshay Singh. This movie was filmed at Mumbai, Maharastra, India. 

==Cast==
*Ali Fazal as Kabir
*Gulshan Grover as Laxmi Nivas
*Anisa Butt as Rachna
*Amrita Raichand as Sulochna
*Razak Khan as Carlos Rehbar Pasha 
* Akshay Singh

== Plot==
A successful author (Ali Fazal) pretends to be a geek in order to impress his girlfriends (Anisa) brother (Gulshan Grover), because he expects his brother-in-law to be intelligent.Anyway, while the heros almost on the verge of winning over his girls bhaiya, his rowdie lookalike shows up, adding to the drama. Thats not all. Even the bhaiya has a lookalike which doubles the drama and confusion.

==Critical Reception==
The major issue with this movie is the fact that it borrows heavily from films like Chupke Chupke, Golmaal and Seeta Aur Geeta. Even if we ignore the lack of originality, the formula that worked in the 1970s looks way too outdated for today. Besides, those films had a solid script and some great comic timing. Here, barring Gulshan Grover, everyone else looks like they are trying too hard to make you laugh.Blame it on the poor writing.

What disturbed us the most were the terrible sound effects (e.g., toilet flush sounds) which hound you throughout the film. Thats another reason why the film puts you off. In a desperate bid to entertain the audience, we are bombarded with these fake and supposedly funny noises throughout.

Razzak Khan looks disinterested in the film while the heroine and her sister-in-law are made to look plain silly. Ali Fazal rises above the writing but eventually, the film lets him down. Gulshan Grover underplays his homosexual character well.

==References==
 
3. http://timesofindia.indiatimes.com/entertainment/hindi/movie-review/Baat-Bann-Gayi-Baat-Bann-Gayi-movie-review/movie-review/23984504.cms

==External links==
*  
* 

 
 
 
 


 