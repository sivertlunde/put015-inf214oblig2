Revenge (1990 film)
{{Infobox film
| name           = Revenge
| image          = RevengePoster.jpg
| caption        = Theatrical release poster
| director       = Tony Scott
| producer       = {{plainlist|
* Stanley Rubin
* Hunt Lowry
}}
| writer         = {{plainlist|
* Jim Harrison
* Jeffrey Alan Fiskin
}}
| based on       =  
| starring       = {{plainlist|
* Kevin Costner
* Anthony Quinn
* Madeleine Stowe
* Miguel Ferrer
* John Leguizamo
}}
| music          = Jack Nitzsche
| cinematography = Jeffrey L. Kimball
| editing        = {{plainlist|
* Chris Lebenzon
* Michael Tronick
}}
| studio         = Rastar
| distributor    = {{plainlist|
* Columbia Pictures
* New World Pictures
}}
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = $20 million  
| gross          = $15,535,771 (US) 
}}
Revenge is a 1990 romantic thriller film directed by Tony Scott, starring Kevin Costner, Anthony Quinn, Madeleine Stowe, Miguel Ferrer and Sally Kirkland. Some scenes were filmed in Mexico. The movie is a production of New World Pictures and Rastar Films and was released by Columbia Pictures. Revenge also features one of John Leguizamos earliest film roles.  The film is based on a novella by Jim Harrison, who co-wrote the script.

== Plot ==
Michael J. "Jay" Cochran is a retiring United States Naval Aviator who, after 12 years in the service, wants to relax and live one day at a time. He goes to Mexico, accepting a matched pair of Beretta shotguns and an invitation from his wealthy friend Tiburon "Tibby" Mendez to spend time at his hacienda in Mexico. Tibby is a powerful crime boss, constantly surrounded by bodyguards.

While trying to find the hacienda, Cochran meets a beautiful young woman riding a horse. She is Tibbys wife, Miryea, who lives in lavish surroundings but is unhappy because her much-older husband does not want children, feeling pregnancy would spoil her looks.

Jay is a welcome guest and presents his friend with a Navy G-1 leather flight jacket. But he rubs Tibbys suspicious right-hand man Cesar the wrong way by behaving independently and not acting like an employee. After a dinner Tibby conducts a private meeting with business associates while Miryea gets acquainted with Jay. She becomes attracted to him. The next day they share a private walk on the beach.

During a party, with Tibby and his men nearby, Jay and Miryea secretly have sex. Tension grows, as Jay is worried that Tibby will become aware of the situation. Miryea begs Jay not to leave. They arrange a secret rendezvous, Miryea pretending that she will be visiting a friend in Miami when she actually is going to accompany Jay to a remote cabin.

Cesar overhears a conversation in which Miryea asks her Miami friend to lie for her. Tibby visits Jay and invites him on a trip to the Beretta factory to custom-fit his new guns, but Jay declines. Tibby tells his friend goodbye. At the airport, Tibby also gives Miryea one last kiss. Jay is secretly waiting to drive her to the cabin.

At their hideaway, they are surprised by Tibby and his men. Jays beloved dog is shot dead. Calling Miryea a "faithless whore," Tibby strikes her and cuts her face with a knife, as Tibbys henchmen viciously beat and kick Jay. After setting fire to the cabin, they dump Jay in the desert, and leave him to die.

Miryea is placed in a whorehouse, where she is drugged, abused, and relegated to "common use". The young man responsible for keeping her drugged has AIDS. As Miryea no longer wishes to live the life of a common prostitute, she persuades him to share a needle with her, thus infecting her.

Jays unconscious body is discovered by a Mexican Good Samaritan, who slowly nurses him back to health. He returns to the burnt cabin and retrieves some money he had stashed away. His rescuer drives Jay to town and gives him a knife to "cut the balls off your enemy". He encounters a Texan delivering a horse, who offers Jay a ride in his car. Inside a cantina, Jay notices one of the thugs who had thrashed him; he follows him into the mens room and cuts his throat.

After a day on the road, the Texan, clearly ill, trades his horse to a wealthy man, who recognizes Jay from an afternoon at Tibbys estate. The friendly Texan dies in his car, apparently of tuberculosis.

At a motel, Jay runs across Amador, the cousin of the Mexican Good Samaritan who had saved his life. Amador and his quiet friend, Ignacio, have their own issues with Tibby Mendez and are willing to help. They capture another of Tibbys henchmen, who tells them where Miryea can be found. Jay barges into the brothel to rescue her, only to find that she has been moved. No one but Tibby knows where she is.

Jay, Amador, and Ignacio ambush Tibby and his bodyguard on a trail. Jay is here for revenge, but first Tibby requests that Jay ask forgiveness for having stolen his wife.

Miryea is in a convent hospice, dying of AIDS. By the time Jay arrives, it is too late. He carries her outside, where she dies in his arms.

== Cast ==
* Kevin Costner as Michael J. "Jay" Cochran
* Anthony Quinn as Tiburon "Tibby" Mendez
* Madeleine Stowe as Miryea Mendez
* Tomas Milian as Cesar
* James Gammon as The Texan
* Jesse Corti as Madero
* Sally Kirkland as The Rock Star
* Miguel Ferrer as Amador
* John Leguizamo as Ignacio

== Production ==
Harrison began writing the screenplay shortly after the novella was published, but the project languished in development hell for eleven years, during which John Huston and Orson Welles were attached to direct.   Costner had an interest in the novella from the mid-1980s; producer Ray Stark eventually acquired the rights, and Costner used his celebrity to help get the film made.  Costner would serve as executive producer and take an especial interest in the script.  Shooting took place in several Mexican cities, including Puerto Vallarta and Mexico City.   Production completed on December 14, 1988. 

== Release ==
Columbia released Revenge on VHS in August 1990.   The version included on the 2007 DVD and Blu-ray releases is Tony Scotts shorter directors cut, running 104 minutes. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 33% of fifteen surveyed critics gave the film a positive review; the average rating was 4.1/10.   Variety (magazine)|Variety wrote, "This far-from-perfect rendering of Jim Harrisons shimmering novella has a romantic sweep and elemental power that ultimately transcend its flaws."   Owen Gleiberman of Entertainment Weekly rated it D and called it a vanity project for Costner.   Roger Ebert, writing for The Chicago Sun-Times, rated it 2.5/4 stars and wrote that the film "plays like a showdown between its style and its story."   Vincent Canby of The New York Times described it as "soft and aimless   the performances are without conviction."   Hal Hinson of The Washington Post wrote that the story becomes so cynical that nothing has meaning. 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 