Joe Dirt 2: Beautiful Loser
{{Infobox film
| name           = Joe Dirt 2: Beautiful Loser
| image          = Joe Dirt 2 Beautiful Loser poster.jpg
| alt            = 
| caption        = Poster Fred Wolf
| producer       = Amy S. Kim 
| writer         = David Spade Fred Wolf
| starring       = David Spade Brittany Daniel Dennis Miller Adam Beach Christopher Walken Mark McGrath Patrick Warburton
| music          = 
| cinematography = Timothy A. Burton 
| editing        = Joseph McCasland 	
| studio         = Happy Madison Productions Crackle
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Fred Wolf Crackle on July 16, 2015.

== Cast ==
*David Spade as Joe Dirt 
*Brittany Daniel as Brandy
*Dennis Miller as Zander Kelly 
*Adam Beach as Kickin Wing Gert B. Frobe
*Mark McGrath
*Patrick Warburton
*Charlotte McKinney

== Production ==
On April 30, 2014,   would direct the film, with filming set to start in November 2014.  Principal photography began on November 17, 2014.  On January 13, 2015, it was announced that Christopher Walken, Dennis Miller, Brittany Daniel and Adam Beach will all reprise their roles from the first film, alongside series newcomers Mark McGrath and Patrick Warburton. 

==Release== Crackle on July 16, 2015. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 