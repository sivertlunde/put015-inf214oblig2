Speak White
{{Infobox film
| name           = Speak White
| image          =
| image_size     =
| caption        =
| director       = Pierre Falardeau Julien Poulin
| producer       =
| writer         = Michèle Lalonde (poem)
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = Short: 6 min.
| country        = Canada French
| budget         =
}}
Speak White is a French language poem composed by French-speaking Quebecer|Québécois writer Michèle Lalonde in 1968. It was first recited in 1970 and was published in 1974 by Editions de lHexagone, Montreal.  It denounced the poor situation of francophone|French-speakers in Quebec and takes the tone of a collective complaint against English-speaking Quebecers. {{cite encyclopedia
  | last = 
  | first = 
  | authorlink = 
  | title = Canadian literature: The "Quiet Revolution"
  | encyclopedia = Encyclopædia Britannica Online
  | volume = 
  | pages = 
  | publisher = 
  | location = 
  | year = 2007
  | url = http://www.britannica.com/eb/article-42340/Canadian-literature#915409.hook British and American references Potomac and Wall Street as its symbols of linguistic oppression.

In 1980, Speak White was made into a short motion picture by polemicists Pierre Falardeau and Julien Poulin, the six-minute film featured actress Marie Eykel reading Lalondes poem. It was released by the National Film Board of Canada.
 allophone immigrants as the same oppressed class as the French-speaking Quebecer|Québécois in Quebec, and calling for a more inclusive society. {{cite encyclopedia
  | last = 
  | first = 
  | authorlink = 
  | title = Canadian Literature: The cosmopolitan culture of French Canada and Quebec
  | encyclopedia = Encyclopædia Britannica Online
  | volume = 
  | pages = 
  | publisher = 
  | location = 
  | year = 2007
  | url =http://www.britannica.com/eb/article-258879/Canadian-literature#915553.hook
  | accessdate = 2007-09-08}} 

==Origin==
"Speak White" is a racist insult used by English-speaking Canadians against those who speak other languages in public.    that English Canadians would hurl the phrase at French Canadians outside Quebec, and speculated that it was borrowed from the Southern United States. {{cite book
  | last = Monière
  | first = Denis
  | authorlink = Denis Monière
  | coauthors =
  | title = André Laurendeau et le destin d’un peuple
  | publisher = Bibliothèque Paul-Émile-Boulet de lUniversité du Québec à Chicoutimi
  | year = 1983
  | location = Saguenay, Canada
  | url = http://classiques.uqac.ca/contemporains/moniere_denis/andre_laurendeau/andre_laurendeau.doc
  | doi =
  | id =  
  | isbn =
  | page = 324 }}   Anecdotal evidence also suggests that the phrase was used against immigrants.  

Although the expression has never been directly quoted and attributed to an individual, the expression continues to enter the public sphere in the course of rhetorical political debate.  {{cite web
  | last = Comeau
  | first = Gerald
  | authorlink =
  | title = The Late Honourable Louis J. Robichaud, P.C., Q.C., C.C.
  | publisher = Debates of the Senate (Hansard): 1st Session, 38th Parliament,
Volume 142, Issue 29
  | date = 2005-02-01
  | url = http://www.parl.gc.ca/38/1/parlbus/chambus/senate/deb-e/029db_2005-02-01-E.htm?Language=E&Parl=38&Ses=1
  | doi =
  | accessdate =  2007-09-08}} 

==References==
 

==External links==
*  translated by Albert Herring
* 
* 
 
 

 
 
 
 
 
 
 
 
 
 
 