Divorce (film)
{{Infobox film
| name           = Divorce
| image_size     =
| image	         = Divorce FilmPoster.jpeg
| caption        =
| director       = William Nigh
| producer       = Jeffrey Bernerd Kay Francis
| writer         = Sidney Sutherland (story and screenplay) Harvey Gates (screenplay)
| narrator       =
| starring       = Kay Francis Bruce Cabot Helen Mack
| music          = Edward J. Kay
| cinematography = Harry Neumann
| editing        = Richard C. Currier
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Divorce is a 1945 drama film about a much-divorced woman who sets her sights on her married childhood friend. It stars Kay Francis, Bruce Cabot, and Helen Mack.

==Plot summary==
 

==Cast==
* Kay Francis as Diane Carter
* Bruce Cabot as Bob Phillips
* Helen Mack as Martha Phillips
* Jerome Cowan as Jim Driscoll Craig Reynolds as Bill Endicott
* Ruth Lee as Liz Smith
* Jean Fenwick as June Endicott Mary Gordon as Ellen Larry Olsen as Michael Phillips
* Johnny Calkins as Robby Phillips
* Jonathan Hale as Judge Conlon
* Addison Richards as Plummer
* Leonard Mudie as Harvey Hicks
* Reid Kilpatrick as Dr. Andy Cole
* Virginia Wave as Secretary

==References==
 	

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 


 