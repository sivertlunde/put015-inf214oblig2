A Girl and a Dolphin
{{Infobox film
| name           = A Girl and a Dolphin
| image          = 
| caption        = 
| director       = Rosalia Zelma
| producer       = 
| writer         = L. Los A.Kondratyev (lyrics)
| cinematography = Vladimir Milovanov
| starring       = Olga (Sergeevna) Rozhdestvenskaya (song)
| music          = Eduard Artemyev
| editing        = V. Konovalova
| distributor    = Studio Ekran
| released       =  
| runtime        = 10 minutes 9 seconds
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}}
 TV film studio Ekran in Moscow. The film is a poetic sketch cartoon about the friendship of a girl and a dolphin. The cartoon is distinguished by a deep level of romance and moral purity. A great musical arrangement by Eduard Artemyev and the song were used, which precisely reflect the meaning of the story.

== Plot ==
The girl is playing with the ball at the beach. Suddenly, the ball falls into the sea. The girl tries to get it, but not knowing how to swim, she starts to sink. And then the Dolphin appears and saves the girl. They become friends. The girl starts to come to the sea everyday and the dolphin teaches her to swim. There comes a day when the girl is able to swim freely. Then one day people has caught the dolphin and taken it to the Dolphinarium to participate in the shows with other trained dolphins. However, the dolphin remains completely indifferent to new environment and the shows, because it cannot live without its element — the sea. Then at night the girl penetrates into the Dolphinarium and opens the lattice, thereby helping the dolphin to escape. No matter how it is, but the dolphin has to leave the habitual seaside. The girl sits on the rock drenched in sunset light and gazes into the sea, reminiscing of beautiful times spent with the dolphin.

== Creators ==
{| class="wikitable"
|-
! !! English !! Russian
|-
| Director
| Rosalia Zelma
| Розалия Зельма
|-
| Scenario
| L. Los
| Л. Лось 
|-
| Song Lyrics
| A.Kondratyev
| А. Кондратьев
|-
| Art Directors
| Tatiana Abalakina Rosalia Zelma
| Татьяна Абалакина Розалия Зельма
|-
| Animators 
|V. Vyshegorodtsev V. Sporykhin A. Levchik S. Sichkar N. Grachyova M. Pershyn
|В. Вышегородцев В. Спорыхин А. Левчик С. Сичкарь Н. Грачева М. Першин
|-
| Camera Operator
| Vladimir Milovanov
| Владимир Милованов
|-
| Composer
| Eduard Artemyev
| Эдуард Артемьев
|-
| Sound Operator
| Oleg Solomonov
| Олег Соломонов
|-
| Executive Producer
| L. Varentsova
| Л. Варенцова
|-
| Editor
| V. Konovalova
| В. Коновалова
|-
| Script Editor
| L. Georgiyeva
| Л. Георгиева
|-
| Assistants
| I. Degtyaryova T. Grosheva E. Lopatnikova E. Zvereva N. Druzhynina
| И. Дегтярева Т. Грошева Е. Лопатникова Е. Зверева Н. Дружинина
|-
| Voice Actor (song)
| Olga (Sergeevna) Rozhdestvenskaya
| Рождественская Ольга Сергеевна
|}

== External links ==
*   video.
*   at Animator.ru

 
 
 
 
 
 
 

 
 