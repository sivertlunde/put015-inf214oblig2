Angadi (film)
{{Infobox film
| name           = Angadi
| image          = 
| director       = I. V. Sasi
| producer       = P. V. Gangadharan
| writer         = T. Damodaran
| starring       = {{Plainlist|
* Jayan Seema
* Sukumaran
}} Shyam
| cinematography = Chandramohan, Babu
| editing        = K. Narayanan
| studio         = Grihalakshmi Productions
| distributor    = Kalpaka Films Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 Seema and Sukumaran in main roles. Directed by I. V. Sasi, its screenplay was handled by T. Damodaran. The film was well received by the audience.        

==Cast==
 
*Jayan Seema
*Jose Jose
*Sankaradi Raghavan
*Prathapachandran
*Sukumaran Ambika
*Balan K Nair
*Bhaskara Kurup
*KPAC Sunny Kunchan
*Kunjandi
*Kuthiravattam Pappu
*Kuttyedathi Vilasini
*Nanditha Bose Ravikumar
*Santha Devi Suchitra
*Surekha
*Vasu Pradeep
 

==Soundtrack== Shyam and lyrics was written by Bichu Thirumala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Allaane Umma   || K. J. Yesudas || Bichu Thirumala ||
|-
| 2 || Kannippalunke Ponninkinaave || P Susheela, Chorus || Bichu Thirumala ||
|-
| 3 || Kannum kannum thammil || K. J. Yesudas, S Janaki || Bichu Thirumala ||
|-
| 4 || Life is just like || S Janaki, Jomon || Bichu Thirumala ||
|-
| 5 || Onavillin Thaalavum || Vani Jairam || Bichu Thirumala ||
|-
| 6 || Paavaada venam melaada venam || K. J. Yesudas || Bichu Thirumala ||
|}

==Review==

Angadi, a record-breaker in box-office receipts, also proved as the greatest accomplishment for the films director, I.V. Sasi. The film also reached its peak with the male lead Jayans superb acting. He was able to win the heart of cine-goerss with his very catchy monologue, "We are not Beggars".   

The songs from the film also made it big for their melodious renditions. With the music composed by Shayam, the films duet, "Kannum Kannum Thammil Thammil" sung by K. J. Yesudas and S. Janaki and "Paavada Venam" sung by K. J Yesudas were instant hits. The lyrics by Bichu Thirumala was also very memorable.   

==References==
 

==External links==
*  

 
 
 


 