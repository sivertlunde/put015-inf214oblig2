My Lucky Stars
 
 
 
{{Infobox film
| name           = My Lucky Stars
| image          = MyLuckyStars.jpg
| caption        = HK film poster
| film name = {{Film name| traditional    = 福星高照
 | simplified     = 福星高照
 | pinyin         = Fú Xīng Gáo Zhào
 | jyutping       = Fuk1 Sing1 Gou1 Ziu3}}
| director       = Sammo Hung
| producer       = Leonard Ho
| writer         = Barry Wong Szeto Chuek-hon
| starring       = Sammo Hung Jackie Chan Yuen Biao Eric Tsang Richard Ng Charlie Chin Stanley Fung Sibelle Hu
| music          = 
| cinematography =Arthur Wong
| editing        = Golden Harvest Media Asia
| released       =  
| runtime        = 96 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
}} Hong Kong action film directed by Sammo Hung, who also starred in the film. The film co-stars Jackie Chan and Yuen Biao. It is the second film in the Lucky Stars series. The film is a semi-sequel to Winners and Sinners, with many of the same actors returning as the "Five Lucky Stars" troupe, albeit with different character names and slightly different roles. 

The film was also released under the following alternative titles:
* Tokyo Powerman (Germany)
* Le flic de Hong Kong (France)
* Winners & Sinners 2 (Sweden)
* Winners & Sinners 2: My Lucky Stars

==Plot summary==
Undercover cop Muscles (Jackie Chan) enlists his childhood friends, the "Five Lucky Stars", to travel to Japan to help him catch a Yakuza group.

A corrupt Hong Kong cop (Lam Ching Ying) flees to Tokyo to join his fellow mobsters, whose headquarters are secretly built under an amusement park (filmed in Fuji-Q Highland).  Two loyal cops, Ricky (Yuen Biao) and Muscle (Jackie Chan), travel there to apprehend him and uncover the mobsters’ lair, but Ricky is kidnapped in a fight.  Muscle goes into hiding and calls his supervisor to send help; since the mobsters already have information on the officers of the Hong Kong Royal Police Force, Muscle asks to send his orphanage friends, nicknamed the Five Lucky Stars, over.  The supervisor agrees and collects the five friends, who are all either petty criminals or low-wage workers.  They refuse to aid the police, but the supervisor cunningly sets up a false story in the media that accuses the five of robbing a bank of millions of dollars, blackmailing them into helping.  They ultimately agree when the supervisor teams them up with a rookie policewoman, Swordflower, who becomes an object of lustful target to the five.  They travel to Tokyo and that night, Kidstuff (Sammo Hung), who is the Stars most rational and talented member, and Swordflower go to Muscle’s apartment.  After defeating some thugs, Muscle reunites with Kidstuff.  The operation is to send phony money to the mobsters to allow the five to enter their lair, and that way they can get closer to freeing Ricky and apprehending the criminals.  After a prolonged battle at the bowels of the amusement park, the criminals lose and the Lucky Stars receive a place to live back at Hong Kong as their reward.

The fight scene between Swordflower (Sibelle Hu) and a Japanese gangster (Michiko Nishiwaki) is one of the most memorable not just in this movie but in many Hong Kong action movies.  Swordflower attempts to unlock the door to Rickys cell not realising that a kimono-clad woman is slowly walking towards her.  Ricky notices and alerts Swordflower to this.  After opening the cell door, Ricky gets ready to confront the woman but Swordflower tells him to let her deal with the woman and Ricky goes to help the others.

The woman has a cold, emotionless look in her eyes and walks very slowly towards Swordflower, stopping a short while to step out of her slippers.  As she walks towards Swordflower she starts slowly removing the belt of her kimono.  Seeing this, Swordflower untucks her blouse, briefly showing a little of her stomach, and gets ready for action.  Suddenly the woman charges towards Swordflower, slips behind her and starts choking Swordflower with the kimono belt and then throwing her to the ground.  With the belt still wrapped around Swordflowers neck, the woman pulls Swordflower up with the belt, hits her face and flips Swordflower to the ground.

Swordflower gets up and the woman quickly whips off her kimono and holds it in front of her to cover her body.  She drops the kimono to the ground and does a double bicep pose.  The woman is dressed in black one piece suit and has extremely muscular arms with rock solid biceps.  A look of shock is on Swordflowers face (eyes wide and mouth open) as she sees how muscular and strong her opponent looks.  The woman turns her back to Sunflower and now she does a back double bicep pose, showing off not only her arms but also her muscular back.  Swordflower realises shes in trouble but does not back down.

Both ladies charge towards each other and start exchanging blows.  Both manage to block the others blows in a flurry of shots.  The woman kicks Swordflower who manages to block but the impact of the kick sends Swordflower back a few steps.  The woman walks towards Swordflower and executes several kicks including a roundhouse kick.  Swordflower manages to block all the kicks but her arms are hurting and she tries shaking the pain away.

The woman approaches and again starts attacking with kicks which are again blocked.  Swordflower manages to step on the womans foot, which temporary immobilises her, and she follows up with an upper cut followed by a hard forearm shot to the womans stomach.  The woman stumbles back, holding her stomach in pain.  Swordflower has a defiant look on her face as though saying "I can defeat you".  The woman recovers almost instantly and is back on the attack.  She executes a high kick (which Swordflower blocks) but it turns out that kick was merely a distraction.  As Swordflower blocked that high kick, the woman starts attacking Swordflowers legs.  Four extremely hard kicks to Swordflowers left thigh causes her left leg to buckle.  As Swordflower falls to one knee, the woman hits her with a hard punch that sends her to the ground.

The camera angle cuts to another scene and when it returns to this fight, the womans head recoils as though she had just been hit in the face by Swordflower.  Swordflower attempts to hit the woman again but she blocks it, pulls Swordflowers head backwards and hits Swordflowers stomach with a hard forearm smash.  Swordflower screams in pain and the woman grabs Swordflowers hair with both hands.  Swordflower tries to break the hold by smashing her forearms against the inside of the womans forearms several times but the woman is just too strong and Swordflower is unable to break the hold.  Therefore she also goes for the hair and both women are dragging each other to and fro by the hair.

Swordflower manages to fling the woman against the side of the wall which collapses since its only made of paper.  However as the woman was grabbing Swordflowers hair, both of them went crashing through the wall.  Kidstuff arrives on the scene to see both women still grabbing each others hair.  As she is much stronger, the woman manages to break Swordflowers hold by smashing her forearms against Swordflowers forearms.  She follows this up with an extremely hard punch which sends Swordflower flying to the ground.  

Kidstuff checks on Swordflower.  Bleeding from the mouth and in obvious pain, she tells him that "shes really tough".  Kidstuff approaches the lady who is in a defensive stance.  She looks absolutely intimidating with her emotionless eyes and her chest, arms and forearms rippling with muscles.  Kidstuff approaches the lady who is ready to attack however he hits her with a quick punch which knocks her out.  A some what ridiculous ending however many would agree this is one of the best female fighting scenes in the Hong Kong film industry.

==Cast==
Despite being billed as one of the stars, Jackie Chans role in the film is relatively minor until the final half hour. The major star of the film is Chans longtime associate and former member of the Peking Opera School, Sammo Hung. The film also features another of that troupe, Yuen Biao.

In the first film, Winners and Sinners, Stanley Fung played an undercover policeman who was posing as the leader of the Lucky Stars gang. In My Lucky Stars and the third film, Twinkle, Twinkle Lucky Stars, Hungs character is the nominal leader, and Fungs character is not a cop.
 political activist. He is replaced in this film by Eric Tsang.

As with Heart of Dragon, Sammo Hungs real-life brother makes a cameo appearance as a henchman.

* Sammo Hung - "Eric" / "Kidstuff" / "Fastbuck" / "Chi Koo Choi"
* Sibelle Hu - "Barbara Woo" / "Swordflower" / "Ba Wong Fa"
* Richard Ng - "Sandy" / "Dee"/ "Dai Sang Dei"
* Charlie Chin - "Herb" (as Charlie Ching) "American Ginseng"/"Fa Kei Sam"
* Eric Tsang - "Roundhead" / "Buddha Fruit" / "Lo Hon Gwo"
* Stanley Fung - "Rawhide" / "Rhinohide" / "Sai Ngau Pei"
* Jackie Chan - "Muscles"
* Yuen Biao - "Ricky Fung"
* Michiko Nishiwaki - Japanese Gang fighter
* Cho Tat-wah - "Supt. Walter Tsao"
* Paul Chang - Gang Chief
* Dick Wei - gang member
* Lam Ching-ying - gang member / Renegade Cop
* Lau Kar-wing - gang member James Tien - Parole Officer
* Teresa Ha - Ping
* Huang Ha - Prisoner No. 6377 (uncredited)
* Tai San - Prisoner
* San Kuai - Prisoner No. 5734
* Bolo Yeung - Millionaire Chan (uncredited) Alice Lau - Millionaire Chans wife 
* Ho Kai-law - Dentist
* Fruit Chan - Card Player
* Dick Tso - Card Player
* Billy Lau - Mental patient on Bench
* Lau Chau-sang - Bus Driver with Funny Hair
* Yu Chi-ming - Man with Funny Hair
* Chu Tau - Bus Driver with Funny Hair
* Teddy Yip Wing-cho - Mental Patient with Sandy
* Yuen Miu - Amusement Park Ninja
* Chow Kam-kong - Gang Thug
* Johnny Cheung - Gang Thug
* Yuen Wah - Gang Thug (uncredited)
* Lee Chi-kit - Gang Thug
* Chin Kar-lok - Gang Thug / Masked Ninja at Muscles home
* Siu Tak-foo - Gang Thug
* Chow Kong - Gang Thug (uncredited)
* Pang Yun-cheung - Gang Thug
* Wellson Chin - Dumb Gang Thug
* Wong Kim-ban
* Lung Ying
* Wu Ma
* Tau Cheung-yeung
* Ng Hoi-ti

==Box office==
During its Hong Kong theatrical run, My Lucky Stars grossed HK $30,748,643.  It was the first film to pass the HK $30 million mark in Hong Kong.

==Awards and nominations==
*1986 Hong Kong Film Awards
**Nomination: Best Action Choreography (Yuen Biao, Lam Ching-ying)

==DVD release== UK in Region 2.

==See also==
*Jackie Chan filmography
*Sammo Hung filmography
*Yuen Biao filmography

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 