Shakti (1982 film)
{{
Infobox film
| name           = Shakti
| image          = Shakti.jpg
| image_size     = 
| caption        = 
| director       = Ramesh Sippy
| producer       = Mushir Alam Mohammad Riaz
| writer         = Salim-Javed
| narrator       = 
| starring       = Dilip Kumar Amitabh Bachchan Raakhee Smita Patil Kulbhushan Kharbanda Amrish Puri Anil Kapoor BP Saxena
| music          = R.D. Burman
| cinematography = S.M. Anwar
| editing        = M. S. Shinde
| studio         = 
| distributor    = 
| released       =  
| runtime        = 167 mins
| country        = India Hindi
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
Shakti ( ;  ) is a 1982 Bollywood film directed by Ramesh Sippy and starring Dilip Kumar, Amitabh Bachchan, Raakhee, Smita Patil and Amrish Puri with Anil Kapoor in a special appearance. It was written by the famous writing duo Salim-Javed and produced by Mushir-Riaz. Shakti was notable for being the first and only film to feature veteran actors Dilip Kumar and Amitabh Bachchan together on screen.  Considered to be one of the greatest films in the history of Indian cinema, it went on to win several Filmfare awards among which were Best Film and Best Actor which was awarded to Dilip Kumar.

==Synopsis==

An ex-cop Ashwini Kumar learns that his grandson Anil Kapoor wants to follow his footsteps. Kumar thinks that his grandson is taking this decision in a flow of emotions, without taking into consideration its implications. To warn his grandson about the dangers and pitfalls of the profession, Kumar decides to tell his own story. How the grandson makes his decision that would affect his whole life is the crux of the story.

This is a remake of 1974 Tamil Movie "Thanka Pathakkam" (meaning Gold Medal) starring Sivaji Ganesan, K.R. Vijaya, Shrikanth & others

==Plot==

Retired Commissioner Ashwini Kumar (Dilip Kumar) is on a railway station to receive his teenager grandson Ravi (Anil Kapoor). On being asked by his grandfather about his future plans, Ravi promptly replies that he wants to become a police officer and serve his country, just like his grandfather. Kumar tells him that the journey of a police personal life is fraught with many challenges and so he must reconsider his decision. Kumar starts by explaining his own story.

Kumar goes in flashback, back to the days when he was an Inspector. Kumar had a happy family consisting, him his wife Sheetal (Raakhee) and son Vijay. Ashwini takes cudgels against a dreaded gangster named J.K. Verma (Amrish Puri). He arrests a key henchman of J.K. and is about to challenge J.K.s supremacy. J.K. takes the matters into his own hands and abducts Vijay.

J.K. tries to strike a deal with Kumar: set his henchman free and he will spare Vijays life. Kumar, an honest cop who will give any sacrifice tells J.K. on the phone that even if his only son is killed in the process, he will not betray the law. Unknown to him, an abducted Vijay is also listening to this conversation. He is shocked and pained to hear his fathers words.

At that time, K.D.Narang(Kulbhushan Kharbanda), another goon in J.K.s gang, helps Vijay to escape after he has a fight with his boss. Soon the police find J.K.s hideout where he hid Vijay, but Vijay is missing. J.K. and K.D. become sworn enemies. Vijay returns home, but his actions indicate that the damage has already been done.

As Vijay grows, his hatred for his father grows too. A young Vijay (Amitabh Bachchan) is an antithesis to his father, espousing every thing his father dislikes. His father always thinks his son is going the wrong direction. But the pains that the son received in his childhood makes Vijay stronger and against his father. He finally has a fall-out with his father and leaves home. Vijay soon becomes a known name in the underworld. By chance or destiny, Vijay joins K.D., who has become a rival, an equal for J.K.

Vijay gladly accepts to work with the man he perceives as the person to whom he owes his life. With K.D.s help, Vijay starts climbing the crime ladder even faster. J.K. cannot help but notice that Kumars son has become a notorious gangster, just as he cannot help noticing that the alliance of Vijay and K.D. has made his life of crime even harder. J.K. decides to remove his thorns before they suck the blood out of him and his business.

Meanwhile, Vijay meets and falls in love with Roma Devi (Smita Patil), a single woman. They start living together, without marriage. Sheetal tries to persuade her son to leave the path of wrong, but in vain. Later, Roma reveals to Vijay that she is pregnant with his child. Vijay decides to marry her.

J.K. hires a sharpshooter to kill Kumar. Instead, Sheetal is killed trying to shield her husband. Vijay is enraged and starts hunting for J.K.s head. J.K. has already perceived this and made all arrangements to leave the country. K.D. too plans to leave the country with Vijay. K.D. convinces Vijay that if he kills J.K., the whole citys police will start hunting him and so he should get out of the country as fast as he can.

Kumar has learnt about Roma and her pregnancy from Sheetal before her death. Kumar and his team learn of J.K. and K.D.s plans. Vijay kills J.K.s goons who try to kill him and ultimately succeeds in killing J.K. before he leaves the country. Meanwhile, Kumar succeeds in foiling K.D.s attempt to flee the country. At this time, he learns that Vijay was going after JK.

Kumar succeeds to track down Vijay, who has just killed JK. Kumar tries to stop him, but in vain. A teary-eyed Kumar lifts his pistol and pulls the trigger. Vijay falls down just before he can reach the plane stairs. Kumar rushes towards him and in a tearful conversation, Vijay realizes his mistake and asks forgiveness. Vijay dies in Kumars arms. Kumar later takes Roma with him.

Kumar cuts to the present and asks again to Ravi whether he wants to follow that dream. Ravi, on hearing the story, firmly says that his answer would still be "yes". Kumar and Roma give Ravi their blessings. Ravi takes a train and leaves, signifying that he would become an officer in the future.

==Cast==
* Dilip Kumar  as  DCP Ashwini Kumar
* Amitabh Bachchan  as  Vijay Kumar
* Raakhee  as  Sheetal Kumar
* Smita Patil  as  Roma Devi
* Amrish Puri  as  J.K.Verma
* Kulbhushan Kharbanda  as  K.D.Narang
* Dalip Tahil  as  Ganput Rai
* Ashok Kumar  as  Police Commissioner
* Satish Shah  as  Satish
* Anil Kapoor  as  Ravi Kumar, Vijay & Romas son, Guest appearance
* Brijendra Prasad Saxena  as  Professor Saxena

==Awards==
Both Dilip Kumar and Amitabh Bachchan were nominated for the Best Actor award but it was Dilip Kumar who eventually won it. Amitabh Bachchan had a total of 3 nominations in the category that year.

The film was critically acclaimed and won many accolades listed below:
 
|- 1983
| Mushir Alam, Mohammad Riaz
| Filmfare Award for Best Film
|  
|-
| Dilip Kumar
| Filmfare Award for Best Actor
|  
|-
| Amitabh Bachchan
| Filmfare Award for Best Actor
|  
|-
| Raakhee
| Filmfare Award for Best Actress
|  
|-
| Ramesh Sippy
| Filmfare Award for Best Director
|  
|}

==Soundtrack==
The song "Jane Kaise Kab Kahan" is an evergreen hit.
{{Track listing
| extra_column    = Singer(s)
| all_writing     = 
| all_lyrics      = Anand Bakshi
| all_music       = R.D. Burman
| title1          = Ae Aasman Bata | extra1 = Mahendra Kapoor
| title2          = Hamne Sanam Ko Khat Likha | extra2 = Lata Mangeshkar
| title3          = Jane Kaise Kab Kahan | extra3 = Kishore Kumar, Lata Mangeshkar
| title4          = Mangi Thi Ek | extra4 = Mahendra Kapoor
}}

==Notes==
 

==External links==
* 

 

 
 
 
 