The Chapman Report
{{Infobox film
| name           = The Chapman Report 
| image          = The Chapman Report.jpg
| caption        = Theatrical poster
| director       = George Cukor
| producer       = Darryl F. Zanuck Richard D. Zanuck
| based on       =   
| writer         = Wyatt Cooper Don Mankiewicz Gene Allen Grant Stuart
| starring       = Shelley Winters Jane Fonda Claire Bloom Glynis Johns Efrem Zimbalist Jr Frank Perkins Max Steiner
| cinematography = Harold Lipstein 
| editing        = Robert L. Simpson   
| distributor    = Warner Bros. Pictures
| released       =     
| runtime        = 125 minutes 
| country        = United States
| language       = English
| budget         = 
}} Frank Perkins main title design by George Hoyningen-Huene, and the costume design by Orry-Kelly.

==Plot summary==
  football player alcoholic nymphomania|nymphomaniac who takes up with an unsavory jazz musician; and Kathleen Barclay (Jane Fonda), a young widow who thinks she is frigidity|frigid. 

==Cast==
* Efrem Zimbalist Jr as Paul Radford
* Jane Fonda as Kathleen Barclay
* Claire Bloom as Naomi Shields
* Shelley Winters as Sarah Garnell
* Glynis Johns as Teresa Harnish
* Ray Danton as Fred Linden
* Ty Hardin as Ed Kraski
* Andrew Duggan as Dr. George C. Chapman
* John Dehner as Geoffrey Harnish
* Harold J. Stone as Frank Garnell
* Corey Allen as Wash Dillon
* Jennifer Howard as Grace Waterton
* Cloris Leachman as Miss Selby
* Chad Everett as Bob Jensen, Water Delivery Boy
* Henry Daniell as Dr. Jonas

==Production== The Longest Day at the same time. When Fox would not do the film, Zanuck offered the property, his son the producer, director Cukor and the female stars to his friend and rival Jack Warner. http://www.emanuellevy.com/article.php?articleID=2895   

Warner Brothers replaced the films planned male leads with their own Warner Brothers Television contract leads who received no extra money to do the film. Warner Brothers felt that casting these performers would attract their fans to the film, while at the same time pleasing the stars who had requested more interesting and different material than they had at Warners.
 adulterous middle-aged housewife having an affair with artist Ray Danton; Glynis Johns as a trendy older woman infatuated with athletic young beach boy Ty Hardin; and Claire Bloom as a Hypersexuality|nymphomaniac. http://www.sensesofcinema.com/2004/great-directors/cukor/   

Costume designer Orry-Kelly dressed each of the different female characters in only one color throughout the film. 

As many as seven different writers worked on the film  with Gene Allen, who was contracted to Cukors organisation delivering the final screenplay. The film attracted much criticism during its production by the Legion of Decency amongst others.

==Reception==
After a screening at San Francisco where Cukor claimed the audience liked the film, the studio recut the film.   At the Legion of Decencys  insistence, Jack Warner had Michael A. Hoey reedit the film  and wrote a different ending  with Zimbalist and Duggan saying that American women were rather normal sexually, a message at odds with the rest of Cukors film.  A different director was brought in to reshoot it. 

Upon the films general release, The New York Times said "the four adapters use four case histories of abnormal sexual behavior of upper middle-class women of a Los Angeles suburb who subject themselves to the testing of a psychologists team of investigators. They touch, unfortunately only superficially, on a frigid type, a nymphomaniac-alcoholic, a confused, bored mother and a gay, flighty intellectual seeking enlightenment in romance. The interplay and lack of depth in the treatment of these glimpses at the intimate life sometimes appear more prurient than scientific. And a viewers emotions rarely, if ever, are fully engaged in following the affairs." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 