The Two Paths
{{Infobox film
| name           = The Two Paths
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = 
| starring       = Dorothy Bernard
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = Biograph Company
| released       =  
| runtime        = 17 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short silent silent drama film directed by D. W. Griffith, starring Dorothy Bernard and featuring Blanche Sweet. A print of the film survives in the film archive of the Museum of Modern Art.   

==Cast==
* Dorothy Bernard as Florence
* Wilfred Lucas as Nellies Husband
* Adolph Lestina as Temptor
* Clara T. Bracy as Mother
* Donald Crisp as Footman
* Blanche Sweet

==See also==
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 