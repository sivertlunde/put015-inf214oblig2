Sorry, Thanks
{{Infobox film|
  name           = Sorry, Thanks |

| Dia Sokol | Dia Sokol, Lauren Veloski |
  starring       = Wiley Wiggins Kenya Miles Andrew Bujalski Donovan Baddley Ia Hernandez |
  | cinematography = Matthias Grunsky
  | producer       = Lauren Veloski |
  
  language       = English |
}}

Sorry, Thanks (2009) is an independent feature film by filmmaker Dia Sokol, who previously produced Mutual Appreciation and the MTV reality series 16 and Pregnant.

Sorry, Thanks premiered at the 2009 South by Southwest (SXSW) film festival in the Emerging Visions category, and was shown at the Mill Valley Film Festival in October 2009.

==Plot summary==
An ensemble comedy, the plot begins when Kira, reeling from a brutal break-up, sleeps with Max, a charming but disheveled wreck already committed to longtime girlfriend Sara. Max becomes obsessed, mostly with Kira, but vaguely with his curious lack of conscience as well. Kira, fighting to win a job she hates and running aimless romantic loops, faces the precarious double challenge of choosing a next step and charting a course back to sanity.

==Production== Mission District in the fall of 2007. The film is built on idiosyncratic performances from 30 mostly nonprofessional actors, cast locally in San Francisco, with the exception of Wiley Wiggins and Andrew Bujalski.

==External links==
* 
* 

 
 
 
 
 
 
 


 