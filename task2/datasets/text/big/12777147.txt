Harlem on the Prairie
{{Infobox film name            = Harlem on the Prairie image           = harlem_on_the_prairie.jpg caption         = Harlem on the Prairie Movie Poster studio          = Associated Features director        = Sam Newfield Jed Buell (co-director) producer        = Jed Buell writer  Flournoy E. Miller Fred Myton starring  Herbert Jeffrey Spencer Williams Connie Harris Mantan Moreland George Randol music           = Lew Porter cinematography  = William Hyer editing         = Robert Jahns distributor     = Sack Amusements released        =   runtime         = 57 min. language  English
|country         = United States budget          = $50,000
|}}
 western Musical film|musical.  The movie reminded audiences that there were black cowboys and corrected a popular Hollywood image of an all-white Old West. 
  Paramount Theatre Rialto Theatre on Broadway. The company had offices at 937 N. Sycamore Ave., Hollywood, California, and the officers of the company were Jed Buell, president; Bert Sternbach, vice president; and Sabin W. Carr, secretary-treasurer.

Harlem on the Prairie was filmed on location at Murrays Dude Ranch, Apple Valley, California. The ranch was founded by N.B. Murray, a black businessman from Los Angeles.  President and chief producer Jed Buell, spent less than $50,000 on this picture. 

==Plot summary==
The story concerns events in the life of Doc Clayburn, who returns with his medicine show and young daughter, Carolina, to the country where 20 years before he had been a rider with a gang of outlaws and assisted in a gold robbery. The gold was hidden when all but Doc were killed in a fight with a posse, and never recovered. When Doc is on his way to recover the gold and wipe out the memory of those early days and his straying from the straight and narrow, his caravan, which had been trailed by a rival gang, is attacked and he is mortally wounded. Just before he dies, Doc gives a map of the gold cache to Jeff Kincaid, a younger rider whom he entrusts with the plan of finding the gold and restoring it to its rightful owners. In doing this, Jeff encounters the heavies, and Mistletoe and Crawfish supply the comedy relief.

==Cast== Herbert Jeffrey: Jeff Kincaid Spencer Williams: Doc Clayburn
*Connie Harris: Carolina, Docs daughter
*George Randol: Sheriff
*Maceo Bruce Scheffield: Wolf Cain
*Mantan Moreland: Mistletoe Flournoy E. Miller: Crawfish (is credited with some of the writing) 
*Lucius Brooks: Musician (as The Four Tones)
*Leon Buck: Musician (as The Four Tones)
*Ira Hardin: Musician (as The Four Tones)
*Rudolph Hunter: Musician (as The Four Tones)

==Background== manage to Herbert Jeffrey, Earl “Fatha” Hines Band,  initially conceived of making an all-black cowboy picture. He intended to distribute the film to the hundreds of movie houses across the South that catered exclusively to black audiences, as the strict racial segregation in effect in the South at the time forbade blacks and whites from being in the same theater at the same time. However, with the help of Gene Autry, another well-known screen cowboy, Jeffrey made a deal with Dallas-based Sack Amusements for national distribution. Berry, S. Torriano. The 50 Most Influential Black Films (2001), page 45 

The film was so successful that Sack Amusements executive Richard C. Kahn approached Jeffrey about continuing the saga of the black cowboy. Since rights to the original character of Jeff Kincaid were tied up with the original producer, Jed Buell, Kahn and Jeffrey created the character of Bob Blake and introduced his trusty horse Stardusk. The first film produced through this new partnership was Two-Gun Man from Harlem (1938).  

A 1940 newspaper credited the film with holding the largest box-office profits of any all-African-American film. 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 