The Passing of the Third Floor Back (1918 film)
(For the 1935 film starring Conrad Veidt see The Passing of the Third Floor Back)
{{infobox film
| name           = The Passing of the Third Floor Back
| image          =
| imagesize      =
| caption        =
| director       = Herbert Brenon
| producer       = Herbert Brenon First National Pictures
| producer       = Herbert Brenon
| writer         = Jerome K. Jerome(play & ?scenario)
| starring       = Johnston Forbes-Robertson
| cinematography = J. Roy Hunt
| editing        = Associated First National
| released       = May 1918
| runtime        =
| country        = UK
| language       = Silent film (English intertitles)
}} 1918 British/American silent allegorical film based on the 1908 play The Passing of the Third Floor Back by Jerome K. Jerome and directed by Herbert Brenon. The star of the film is Sir Johnston Forbes-Robertson, a legendary Shakespearean actor, who starred in the 1909 Broadway presentation of the play and its 1913 revival. Forbes-Robertson had been knighted by King George V in 1913 and had retired from acting in theatre that same year. In his retirement Forbes-Robertson had only dabbled in film acting making a 1913 film version of Hamlet, the most famous role he had played on the stage. Filmed in 1916, released in 1918.   

==Cast==
*Sir Johnston Forbes-Robertson - The Stranger, (aka The Third Floor Back)
*Molly Pearson - Stasia(*Molly Pearson played this part in the 1909 Broadway play)
*Ketty Galanta -
*Augusta Haviland - David Powell in the play) (*Le Guerre unbilled)
*Alfred Hickman
*Victory Bateman - Miss De Hooley, a snob (*unbilled)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 

 