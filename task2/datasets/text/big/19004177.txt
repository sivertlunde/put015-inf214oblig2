Mohabbat Zindagi Hai (1975 film)
{{Infobox film
| name           = Mohabbat zindagi hai
| image          =
| image_size     =
| caption        =
| director       = Iqbal Akhtar
| producer       = Begum Riaz
| writer         =
| narrator       = Mohammad Ali Zeba Waheed Murad Mumtaz Lehri Qavi Saqi Nayyar Sultana
| music          = M. Ashraf
| cinematography =
| editing        =
| distributor    =
| released       = 6 June 1975
| runtime        = approx. 3 hours
| country        = Pakistan
| language       = Urdu
| budget         =
}}
 Mohammad Ali, Zeba, Waheed Murad, Mumtaz, Nayyar Sultana, Qavi, Saqi and Lehri.  Waheed Murad played a secondary role and Zeba played a lead role against Mohammad Ali.

==Release==
MZH was released by Ayaz Films on 6 June 1975 in Pakistani cinemas. The film completed 12 weeks on main cinemas and 59 weeks on other cinemas in Karachi and, thus, became a Golden Jubilee film.
t

==Music==
The music of the film is composed by M. Ashraf and the songs are written by Masroor Anwar. Playback singers are Ahmed Rushdi, Mehdi Hassan and Nahid Akhtar. The songs of the film became very popular esp. Dil ko jalana hum ne chor diya....

===Songography===
*Dil ko jalana hum ne chor diya... by Ahmed Rushdi
*Mashriqi rang ko chor kar... by Ahmed Rushdi
*Main shair to nahin... by Mehdi Hassan

==References==
 
 

 
 
 
 
 

 