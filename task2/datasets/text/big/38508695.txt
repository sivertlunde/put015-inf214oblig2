The Brass Teapot
{{Infobox film
| name           = The Brass Teapot
| image          = The Brass Teapot Official Poster.jpeg
| caption        = 
| director       = Ramaa Mosley
| producer       = 
| writer         = Tim Macy
| starring       = Juno Temple Michael Angarano Alexis Bledel Alia Shawkat Bobby Moynihan
| music          = Andrew Hewitt 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  $6,997  
}}
The Brass Teapot is a 2012 American film directed by Ramaa Mosley.  The movies script was written by Tim Macy, who also wrote the short story on which the movie is based.  The movie premiered at the Toronto International Film Festival on September 8, 2012 and was released into theaters and video on demand on April 5, 2013. 

==Synopsis==
The movie follows John (Michael Angarano) and Alice (Juno Temple), a down on their luck couple that comes across a magical brass teapot capable of providing them with money. The only catch is that they must harm themselves in order for the teapot to provide. They must then decide what they are willing to do in order to gain financial security.

==Cast==
* Juno Temple as Alice
* Michael Angarano as John
* Alexis Bledel as Payton
* Alia Shawkat as Louise
* Bobby Moynihan as Chuck
* Ben Rappaport as Ricky
* Billy Magnussen as Arnie Steve Park as Dr. Li Ling
* Lucy Walters as Mary
* Claudia Mason as Donna
* Debra Monk as Trudy

==Reception==
Critical reception for the film was poor, with The Brass Teapot holding a rating of 33% "rotten" on Rotten Tomatoes based on 30 reviews.  The New York Times criticized the film, saying that while the two lead characters were interesting, the "movie’s best bits lose out to the requisite moral turnaround".  The Film School Rejects commented that the darker points of the films story line were "ill fitting" in contrast with the predominantly "comically light and slapsticky" tone of the overall movie.  In contrast, Variety gave a more positive review for the film, saying that Mosely "makes her low-budget enterprise look as slick as most midrange studio comedies, demonstrating herself a director with both imagination and technical ingenuity". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 