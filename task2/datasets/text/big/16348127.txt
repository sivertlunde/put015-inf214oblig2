Tokyo Chorus
{{Infobox film name           = Tokyo Chorus image          = producer       = Shochiku Kinema director       = Yasujirō Ozu writer         = Komatsu Kitamura (adapation) Kōgo Noda (scenario) starring       = Tokihiko Okada Emiko Yagumo music          = cinematography = Hideo Shigehara distributor    = Shochiku Company (1931, 1982, 2003) released       =   country        = Japan runtime        = 90 min. language  Japanese intertitles
}}
 The Crowd.   Most of the film takes place in Tokyo during a depression-like time in the beginning of the Shōwa period.

==Plot== scooter for his son, who is immediately disappointed and throws a tantrum.  His wife Tsuma Sugako (Emiko Yaguma) returns from the market and tries to calm the boy while Choujo tells her what happened.
 rickshaw to take them all to the hospital.  At the hospital Miyoko recovers apparently quickly, but they are forced to sell Sugakos kimono to pay the bill. 
 curry rice.  He offers Okajimo a temporary job holding a banner and passing out flyers; the very job Douryou ended up with earlier in the film after he was fired.  Okajima is disappointed as he feels it is beneath him; but takes it for his family. Sugako is distraught at the thought of her husband working such a degrading job, but decides to join them at the restaurant.

One day soon afterward, Sugako, Tsuma, Mr. and Mrs Omura (Choko Iida) are cooking up big plates of curry rice.  Omura invites his schoolmates to the restaurant for a meeting.  The class sits at the table and drinks happily.  As they eat a letter arrives from the Ministry of Education; it is a notification of a job for Okajima, teaching English in a small rural town at a girls school.  After discussing the matter, Okajima and his wife return to the dining room and the final student arrives "late as always".  Everyone celebrates and breaks out into song.

==Release==
The film was originally released in Japan in 1931.  It did not see a US release until 1982. It was released in Japan on DVD as part of a box set from Shochiku Company in 2003. It was finally released with a new score by Donald Sosin on DVD in the US in 2008 via The Criterion Collection. 

==Cast==
*Tokihiko Okada as Shinji Okajima
*Emiko Yagumo as Sugako, Shinjis wife
*Hideo Sugawara as  their seven-year-old son
*Hideko Takamine as Miyoko, their daughter
*Tatsuo Saitō as Ōmura Sensei, a teacher
*Chōko Iida as Mrs Ōmura
*Takeshi Sakamoto as Yamada, an elderly employee
*Reikō Tani as the Company President
*Kenichi Miyajima as the Presidents secretary
*Kanji Kawara as the doctor
*Isamu Yamaguchi as Shinjis colleague

==References==
 

==External links==
* 
*  
* 

 

 
 
 
 
 
 
 
 