Dame Chance
{{infobox film
| name           = Dame Chance
| image          =
| imagesize      =
| caption        =
| director       = Bertram Bracken
| producer       = David Hartford
| writer         = Frances Nordstrom (story-adaptation)
| starring       = Julanne Johnston Robert Frazer Gertrude Astor
| cinematography = Walter L. Griffin
| editing        =
| distributor    = American Cinema Associates
| released       =   reels (6,769 feet)
| country        = United States
| language       = Silent (English titles)
}} silent film romantic drama produced and released by independent companies David Hartford Productions and American Cinema Associates respectively. The stars are Julanne Johnston, Robert Frazer, Gertrude Astor and Mary Carr. Copies of the film are held at the Library of Congress and the BFI British Film Institute.   

==Cast==
*Julanne Johnston - Gail Vernon
*Gertrude Astor - Nina Carrington
*Robert Frazer - Lloyd Mason
*David Hartford - Craig Stafford
*Lincoln Stedman - Bunny Dean
*Mary Carr - Mrs. Vernon
*John T. Prince - Sims

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 
 