Alex (1992 film)
 
 
{{Infobox film
| name           = Alex
| image size     = 
| image	=	
| caption        = 
| director       = Megan Simpson
| producer       = Tom Parkinson Phil Gerlach
| writer         = Ken Catran
| based on       = the novel by Tessa Duder
| starring       = Lauren Jackson Chris Haywood Josh Picker
| music          = Todd Hunter
| cinematography = Donald Duncan
| editing        = Tony Kavanagh
| distributor    = 
| studio         = Isambard Productions Total Film and Television New Zealand Film Commission Australian Film Finance Corporation
| released       = 1992
| runtime        = 105 mins
| country        = New Zealand Australia
| language       = English
| budget         = 
}}
Alex is a 1992 Australian-New Zealand film based on a popular young adult novel by Tessa Duder. This drama-sport film was never released theatrically in Australia, but shown in some foreign territories and went straight to video. Ed. Scott Murray, Australia on the Small Screen 1970-1995, Oxford Uni Press, 1996 p4 

==Plot==
A young headstrong New Zealand womans (Lauren Jackson) quest against the setbacks, intense rivalry and personal tragedy to win selection for the 1960 Rome Olympic Games in the womens 100m Freestyle swimming|freestyle.

== Cast ==
 
* Lauren A Jackson as Alex Archer
* Chris Hayward as Mr Jack
* Josh Picker as Andy
* Cathy Godbold as Maggie Benton
* Elizabeth Hawthorne as Mrs Benton
* Bruce Phillips as Mr Archer
* May Lloyd as Mrs Archer
* Patrick Smith as Mr Benton
* Rima te Wiata as Female Commentator
* Mark Wright as Male Commentator
* Grant Tilly as Mr Upjohn
* Greg Johnson as Male Journo
* Alison Bruce as Female Journo
 

==References==
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p165 (1997, Oxford University Press, Auckland) ISBN 019 558336 1 
 

==External links==
*  at IMDB
*  at NZ on Screen
*  at NZ Videos
*  at The Film Archive

 
 
 
 


 