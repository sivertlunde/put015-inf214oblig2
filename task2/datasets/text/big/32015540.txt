Connected: An Autoblogography About Love, Death & Technology
 
{{multiple issues|
 
 
}}

	
{{Infobox film
| name           = Connected: An Autoblogography About Love, Death & Technology
| image          = Connected_The_Film_Poster.png
| caption        = Connected poster
| director       = Tiffany Shlain
| producer       = Tiffany Shlain Carlton Evans
| writer         = Tiffany Shlain Carlton Evans Sawyer Steele Ken Goldberg
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| music          = Gunnard Doboze
| cinematography = 
| editing        = Dalan McNabola Tiffany Shlain Sawyer Steele
| studio         = 
| distributor    = Paladin Films (US) ro*co films (outside US)
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Connected: An Autoblogography About Love, Death, & Technology (2011 in film|2011) is an autobiographical documentary film directed by Tiffany Shlain. {{cite web
|url=http://www.imdb.com/title/tt1727491/fullcredits#cast
|title=Connected: An Autoblogography About Love, Death & Technology |accessdate=7 Nov 2011}}   The film unfolds during a year in which technology and science literally become a matter of life and death for the director. As Shlain’s father battles brain cancer and she confronts a high-risk pregnancy, her very understanding of connection is challenged. Using a mix of animation, archival footage, and home movies, Shlain attempts to reveal the ties that link us not only to the people we love but also to the world at large. Connected explores how, after centuries of declaring our independence, it may be time for us to declare our interdependence instead. 

==Release==
The film premiered at the Sundance Film Festival in January 2011,  and opened theatrically in 11 cities including San Francisco,    Marin, Berkeley, Monterey, Seattle, Denver, Portland,  LA  and New York    in the fall of 2011 in an exclusive release theatrical tour. In 2012 Connected was selected by the U.S. State Department to tour with The American Film Showcase to represent America.  With the American Film Showcase, the film was sent to embassies around the world  and Director Tiffany Shlain traveled to South Africa  and Israel to screen the film and teach filmmaking workshops. In Tiffany Shlains AOL Original series, The Future Starts Here there is an episode in Season 2 called Punk Rock Diplomacy that takes you behind the scenes on her tour with the American Film Showcase.  Connected aired on KQED in 2013 and is now available on DVD and digital platforms including iTunes,  Netflix  and more. 

==Reception==
Critical reception has been mixed. The film received a score of 5.6/10   on IMDB and 38% positive reviews on the aggregator Rotten Tomatoes. 

==Awards & Festivals==
*Connected: An Autoblogography about Love, Death & Technology (2011)
**Sundance Film Festival in 2011
**2013: Best Documentary - Mumbai Womens International Film Festival
**2013: Best Feature Film - Big Easy Intl Film and Music Festival
**2012: Disruptive Innovation Award - Tribeca Film Festival
**2012: Award of Excellence - Accolade Competition
**2012: Selected by the United States Department of State & University of Southern California for the 2012 American Filmmaker Showcase
**2012: Best Documentary Feature - Atlanta International Documentary Film Festival
**2012: 4 Awards from The Los Angeles Movie Awards (Best in Show, Best Feature Doc, Best Director, Best Visual Effects) 
**2012: Broadcast on Australian TV
**2011: Interdependence Film Prize - Berlin Film Festival & The Interdependence Movement
**2011: Women in Film Award - All Roads Grant National Geographic Society / Sundance Film Festival
**2011: Women of Vision Nomination - LOréal / Entertainment Weekly / Sundance Film Festival
**2011: Best Documentary Audience Award - Maui Film Festival
**2011: Best of the Fest Documentary Award - Portland Maine Film Festival
**2011: Metta Media Award - Dallas Video Fest
**2011: Honorable Mention - Jerusalem Film Festival
**Connected screened at over 100 festivals between 2011 and 2014, including Sundance Film Festival, Ashland Independent Film Festival, Maui Film Festival, Jerusalem Film Festival, Cleveland International Film Festival, and Nashville Film Festival Margaret Herrick Library of the Academy of Motion Picture Arts and Sciences acquired the script for Connected for their permanent collection

==References==
{{Reflist|refs=
<!-- Unused citations
 {{Citation  | last = Shaywitz  | first = David  | title = Finding beauty and hope at a personalized medicine conference  | work = Forbes Magazine  | date = April 29, 2011  | url = http://blogs.forbes.com/sciencebiz/2011/04/29/finding-beauty-and-hope-at-a-personalized-medicine-conference/  | accessdate =June 6, 2011
}} 
 {{Citation  | last = Dunaway  | first = Michael  | title = Sundance Documentary Reviews  | work = Paste Magazine | date = February 2, 2011  | url = http://www.pastemagazine.com/articles/2011/02/sundance-documentary-reviews-reagan-connected-page.html  | accessdate =June 6, 2011
}} 
 {{Citation  | last = Greenberg  | first = James  | title = SUNDANCE REVIEW: Energetic Connected: An Autobiography about Love, Death and Technology Upbeat About Internet Age  | work = The Hollywood Reporter |  date = January 29, 2011  | url = http://www.hollywoodreporter.com/review/sundance-review-energetic-connected-autobiography-94238  | accessdate =June 6, 2011
}} 
 {{Citation  | last = Macaulay  | first = Scott  | title = EDITORS NOTE  | work = Filmmaker Magazine |  date = January 25, 2011  | url = http://www.filmmakermagazine.com/newsletter/2011_1_28.php  | accessdate =June 6, 2011
}} 
-->
}}

==External links==
* 
* 
* 

 
 
 
 
 
 