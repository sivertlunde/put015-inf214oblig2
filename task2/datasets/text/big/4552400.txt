Saint Jack
{{Infobox film
| name          = Saint Jack
| image         = SaintJack.jpg Paul Bacon   
| director      = Peter Bogdanovich
| producer      = Hugh M. Hefner Edward L. Rissien
| writer        = Peter Bogdanovich Howard Sackler Paul Theroux
| starring      = Ben Gazzara Denholm Elliott George Lazenby 
| cinematography = Robby Müller
| editing       = William C. Carruth
| distributor   = New World Pictures
| released      = 27 April 1979 (New York City, USA)
| runtime       = 112 min.
| language      = English
| budget        = $2 million 
| music         =
}} Chinese Triad triad members in the process.
==Film==
Cybill Shepherd had sued Playboy magazine after they published photos of her from The Last Picture Show. As part of the settlement, she got the rights to the novel Saint Jack which she had wanted to make into a film ever since Orson Welles gave her a copy. The Upside-Down Views of Cybill Shepherd
Mann, Roderick. Los Angeles Times (1923-Current File)   21 May 1978: n37.  

Ben Gazzara stars as Flowers in the film, directed by Peter Bogdanovich.

=== Controversy about the film === Empress Place hawker centre (now demolished) and Bugis Street. The local authorities knew about the book, hence the foreign production crew did not tell them that they were adapting it, fearing that they would not be permitted to shoot the film. Instead, they created a fake synopsis for a film called "Jack Of Hearts", (what the director called "a cross between Love is a Many Splendored Thing and Pal Joey" Bogdanovichs Picture Show
Lee, Grant. Los Angeles Times (1923-Current File)   10 Aug 1979: e16. ) and most of the Singaporeans involved in the production believed this was what they were making.

The film was banned in Singapore and Malaysia on 17 January 1980. Singapore banned it "largely due to concerns that there would be excessive edits required to the scenes of nudity and some coarse language before it could be shown to a general audience," and lifted the ban only in March 2006.    It is now an M18-rated film.

Saint Jack was re-released in North America on DVD in 2001.

In an interview with The New York Times on 15 March 2006, Bogdanovich said, "Saint Jack and They All Laughed were two of my best films but never received the kind of distribution they should have." 

==Cast==
* Ben Gazzara as Jack Flowers
* Denholm Elliott as William Leigh
* James Villiers as Frogget
* Joss Ackland as Yardley
* Rodney Bewes as Smale
* Mark Kingston as Yates
* Lisa Lu as Mrs. Yates
* Monika Subramaniam as Monika
* Judy Lim as Judy
* George Lazenby as Senator
* Peter Bogdanovich as Eddie Schuman
* Joseph Noël as Gopi

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 