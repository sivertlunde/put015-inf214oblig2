Les Grands Ducs
{{Infobox film
| name           = Les Grands Ducs
| image          = 
| image_size     = 
| caption        = 
| director       = Patrice Leconte
| producer       = Thierry de Ganay   Monique Guerrier
| writer         = Patrice Leconte   Serge Frydman
| narrator       =  Catherine Jacob Michel Blanc
| music          = Angélique Nachon Jean-Claude Nachon
| cinematography = Eduardo Serra
| editing        = Joëlle Hache
| distributor    = Bac Films
| released       = February 21, 1996
| runtime        = 85 minutes
| country        = France
| language       = French
| budget         = $6,300,000
| gross          = $3,755,175 
}} Catherine Jacob.

==Plot==
George Cox, Victor Vialat and Eddie Carpentier are old and shabby broke actors. They will resume unexpectedly with panache but three small roles in a light comedy, bound for a tour. Shapiron, producer, ruined, will do anything to sabotage the show and thus get the insurance but the three actors will not let the last chance of life.

==Cast==
* Jean-Pierre Marielle as George Cox
* Philippe Noiret as Victor Vialat
* Jean Rochefort as Eddie Carpentier Catherine Jacob as Carla Milo
* Michel Blanc as Shapiron
* Clotilde Courau as Juliette
* Pierre-Arnaud Juin as Pat
* Jacques Mathou as Janvier
* Marie Pillet as Clémence
* Jacques Nolot as Francis Marceau
* Jean-Marie Galey as Markus
* Dominique Besnehard as Atlass Boss

== References ==
 

==External links==
* 

 
 
 
 

 
 