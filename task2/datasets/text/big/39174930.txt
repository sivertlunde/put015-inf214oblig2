The Gilded Cage (2013 film)
{{Infobox film
| name           = The Gilded Cage
| image          = 
| caption        = 
| director       = Ruben Alves
| writer         = Ruben Alves Hugo Gélin Jean-André Yerles
| producer       = Danièle Delorme Laetitia Galitzine Hugo Gélin 
| starring       = Rita Blanco Joaquim de Almeida Roland Giraud
| music          = 
| cinematography = André Szankowski 
| editing        = Nassim Gordji Tehrani 
| studio         = Zazi Films
| distributor    = Pathé
| released       =  
| runtime        = 90 minutes
| country        = France Portugal
| language       = French Portuguese English
| budget         = $5,6 million
| gross          = $19,243,892  
}}
The Gilded Cage ( ) is a French-Portuguese comedy film released in 2013. It won the "Peoples Choice Award" at the 26th European Film Awards.  It was the film with the most admissions at the Portuguese box office in 2013, with 755 000. 

The film won the Canadian Broadcasting Corporation|Radio-Canada Audience Award at the 2014 edition of the Cinéfranco film festival.

==Cast==
* Rita Blanco as Maria Ribeiro
* Joaquim de Almeida as José Ribeiro
* Roland Giraud as Francis Cailaux
* Chantal Lauby as Solange Cailaux
* Barbara Cabrita as Paula Ribeiro
* Lannick Gautry as Charles Cailaux Maria Vieira as Rosa
* Jacqueline Corado as Lourdes
* Jean-Pierre Martins as Carlos
* Alex Alves Pereira as Pedro Ribeiro
* Catarina Wallenstein as Fadosinger
* Alice Isaaz as Cassiopée 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 