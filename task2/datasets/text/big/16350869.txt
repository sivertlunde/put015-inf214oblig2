Soccer Mom (film)
{{Infobox film
| name           = Soccer Mom
| image          =Soccer Mom Poster.png
| caption        =
| director       = Greg McClatchy Michael Hennessy Jonathan Bogner
| writer         = Frederick Ayeroff Kristen Wilson Elon Gold Master P Gerald Brunskill
| cinematography = Jeffrey Venditti
| editing        = Jim Makiej
| studio         = Ladies Home Journal Bogner Entertainment, Inc. Blueprint Entertainment
| distributor    = Anchor Bay Entertainment
| released       = September 30, 2008
| rating         = rated PG
| runtime        = 91 minutes
| country        = United States English
| budget         = $3,500,000
| gross          =
}}

Soccer Mom is an American 2008 direct-to-video film starring Missi Pyle and Emily Osment.

==Cast==
===Leads===
*Missi Pyle as Wendy Handler, Beccas mother. A former dancer, she started working at a hair salon after her husband died. To spare her daughter yet another disappointment, she decides to pretend to be Italian soccer superstar Lorenzo Vincenzo and coach her team.
*Emily Osment as Becca Handler, Wendys daughter and an avid soccer player whos been having a hard time ever since her father and former coach died.
*Dan Cortese as Lorenzo Vincenzo, a famous Italian soccer player. Known for his anger on the field, he is also a persistent womanizer. Kristen Wilson as Dee Dee Mar Vista Galaxy team.
*Master P as Wally
===Supporting players===
*Ellery Sprayberry as Kelci Handler, Wendys younger daughter
*Dylan Sprayberry as Sam Handler, Wendys son
*Robert Cavanah as Harry Malibu Majestics who likes to give Becca a hard time and is shown to have a difficult relationship with her mother.
*Jennifer Sciole as Patti Duchamps
*Courtney Brown as Laurie
*Clint Culp as Rick
*Eugene Osment as Marty
*Karen Masumoto as Sierra
*Steve Hytner as Coach Kenny
*Monika Jolly as Waitress #1
*Victoria Jackson as Dr. Renneker William OLeary as Harley the Tournament Director
*Lou Volpe as Italian National
*Joy Fawcett as Herself
*John David Smith as Referee
*Hymnson Chan as Bell Boy

==Plot== Mar Vista Galaxy, then struggles frantically to keep her wacky charade going long enough to see the girls win their big tournament.

==Production==
The film was shot in, and takes place in, Los Angeles and its surrounding areas between February 12 and March 10, 2008, on a budget of 3.5 million dollars.

==Reception==
The film received average, mostly positive reviews from critics, most of whom praised the performances of star Missi Pyle and Emily Osment and compared the film to similar fare, notably the 1993 classic Mrs. Doubtfire.
 The Tribune, Freaky Friday and Superdad, but is still harmless fun", while praising Pyles performance and those of Steve Hytner and Dan Cortese, though not being impressed with Osment and giving the film 3 stars out of 5.  The same rating was given by Joly Herman for Common Sense Media, who found the film to be "pretty light fare" and "some of the plot twists   a little predictable" but liked Pyles and Osments performances as well as the focus on girls sports and opined that "moms and daughters will enjoy watching it together".  On DVD Verdict, Erich Asperschlager was critical of how Pyle looked in man makeup, but wrote that "once you get past the premise and the makeup, Soccer Mom is a middle-of-the-road kids flick with a good message about family—even if its the same message found in plenty of better movies", going to praise Pyle, Osment, Cortese as well as Cassie Scerbo and concluding that the film "succeeds more than it fails, and ultimately fares better than a lot of its family movie competition".  Home Theater Info, finally, only awarded the film 4 out of 10 stars, but remained mostly positive, concluding that "this is fun on a juvenile level but just perfect for that rainy Saturday afternoon when the kids are stuck in the house". 

==See also==
Mrs. Doubtfire – a 1993 comedy in which a parent also dresses up in the other gender to grow closer to his children and which reviewers compared Soccer Mom to.

==References==
 

==External links==
* 
*  

 
 
 
 
 