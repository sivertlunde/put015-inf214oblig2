Pokkisham
{{Infobox film
| name = Pokkisham
| image = Pokisham.jpg Cheran
| writer = Cheran Padmapriya Aryan Rajesh
| producer = P. Shanmugam
| music = Sabesh-Murali
| distributor =
| released =  
| runtime = 174 minutes
| country = India
| language = Tamil
| budget =
| gross =  7.1 crore
}}
 Tamil film Meena and Prasanna (actor)|Prasanna. 
 The Bridges of Madison County, directed by Clint Eastwood.

== Plot ==
In 1970, a marine engineer Lenin (Cheran (director)|Cheran) from Calcutta comes across a Nagore girl Nadira (Padmapriya). Started with friendship, later seeded with love, Lenin mails a letter once a month. When Lenin came to Nagore, he was disappointed and sad as Nadira and her family left the place after selling their home. Years later, Mahesh (Aryan Rajesh), son of Lenin, found his fathers hidden life from reading his diary. With help from his friend Shamsudeen (Ajai.R), He marches his way to find Nadira and deliver the letter to her. He comes to know that Nadira has shifted to Malaysia after marriage. He goes to Malaysia to meet her. Once he reaches her house, he is greeted by Nadiras son who welcomes him to the house. Nadira is now very old and asks Mahesh as to who he is and how does he know her? He replies saying he is Lenins son and also informs her that his father died few years back. Nadira is grief-stricken. Mahesh gives her Lenins last few letters which he could not post because he did not have Nadiras address. In those letters, Lenin has spoken about his journey to find Nadira after she and her family shifted from Nagore. He describes his pain and grief over not knowing about her situation during those days, and also about his father who coaxed him to marry another girl (played by Anupama Kumar), who is now his wife and is very understanding. Nadira then drops Mahesh to the airport and while on the way she request him to let her talk to his mother. Nadira and Maheshs mother exchange good notes and then she finally drops him to the airport. While coming back, she is talking to her herself as well as to Lenin saying that she showed her anguish and punished her father by denying to marry anyone. She currently lives with her sister and her lovable family and apologises and promises Lenin that she will join him soon and never separate.   

== Cast == Cheran as Lenin Padmapriya as Nadira (Dubbing voice by Meena Durairaj|Meena)
* Aryan Rajesh as Mahesh (Dubbing voice by Prasanna (actor)|Prasanna) Vijayakumar
* Anupama Kumar as Cherans wife
* Ajai.R as Shamsudeen
* Mahesh Manoharan as jeeva
* Bindu Madhavi In a special appearance

== Soundtrack ==
The soundtrack album was composed by the duo Sabesh-Murali.

{{tracklist	
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length =

| title1       = Aalagu mugam
| extra1       = V. Prasanna
| lyrics1      = Yugabharathi
| length1      =

| title2       = Nila Ne Vaanam
| extra2       = Chinmayee, Vijay Yesudas
| lyrics2      = Yugabharathi
| length2      =

| title3       = Anjal Petti
| extra3       = Karthik
| lyrics3      = Yugabharathi
| length3      =

| title4       = Aaj Monee
| extra4       = Vijay Yesudas, Ujjaini
| lyrics4      = Yugabharathi
| length4      =

| title5       = Ohh Ohh
| extra5       = Vijay Yesudas
| lyrics5      = Yugabharathi
| length5      =

| title6     = Varum Vazhi Engum
| extra6       = Ranjani
| lyrics6       = Yugabharathi
| length6     =

| title7      = Mudru Naal Aagume
| extra7      = Karthik
| lyrics7     = Yugabharathi
| length7     =

| title8      = Ulagam Ninaivil Illai
| extra8      =Mahathi, V.Prasanna
| lyrics8     =Yugabharathi
| length8     =

| title9      =Mozhi illamalae
| extra9      =Madhu Balakrishnan
| lyrics9     =Yugabharathi
| length9     =

| title10     =Kanavu sila samayam
| extra10     =V. Prasanna
| lyrics10    =Yugabharathi
| length10    =

| title11     =Siru Punnagai
| extra11     =V. Prasanna
| lyrics11    =Yugabharathi
| length11    =
}}

== Awards ==
=== Wins ===
* Vijay Award for Best Art Director - Vairabalan
* South Scope Special Jury Award for Best Actress - Padmapriya

=== Nominations ===
* Vijay Awards

** Best Director Padmapriya
** Best Cinematographer - Rajesh Yadhav
** Best Make Up - Sasi
** Best Male Playback Singer - Vijay Yesudas

* Filmfare Awards South
 Cheran
** Padmapriya

== References ==
 

== External links ==
*  

 

 
 
 
 