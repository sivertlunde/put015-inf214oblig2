Sahasa Simha
{{Infobox film 
| name           = Sahasa Simha
| image          =  
| caption        = 
| director       = Joe Simon
| producer       = M. Pandurangan M. Ramalingam
| story          = Manu (Based on Novel Chakravyuha)
| writer         = Kunigal Nagabhushan (dialogues)
| screenplay     = Joe Simon Vishnuvardhan Kajal Rajya Lakshmi Vajramuni
| music          = Chellapilla Satyam
| cinematography = H. G. Raju
| editing        = P. Venkateshwara Ravu
| studio         = Sri Lakshmi Cine Productions
| distributor    = Sri Lakshmi Cine Productions
| released       =  
| runtime        = 164 min
| country        = India Kannada
}}
 1982 Cinema Indian Kannada Kannada film, Rajya Lakshmi and Vajramuni in lead roles. The film had musical score by Chellapilla Satyam.  

==Cast==
  Vishnuvardhan
*Kajal Kiran Rajya Lakshmi
*Vajramuni
*Dheerendra Gopal
*Thoogudeepa Srinivas
*Shakthi Prasad
*Tiger Prabhakar
*Sudheer
*Arikesari
*Udaykumar in Guest Appearance
*Chethan Ramarao in Guest Appearance
*Ramanujam in Guest Appearance
*Nithyanand in Guest Appearance
*Vinodkumar
*Dr Rudresh
*Neegro Johny
*Gopalakrishna
*Jr Narasimharaju
 

==Soundtrack==
The music was composed by Satyam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Mareyada Nenapanu || S. P. Balasubrahmanyam || R. N. Jayagopal || 04.24
|- Janaki || Chi. Udaya Shankar || 04.23
|-
| 3 || Bittare Sigalaara || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 03.42
|-
| 4 || Hegiddaru || Vishnuvardhan (actor)|Vishnuvardhan, Renuka || Chi. Udaya Shankar || 03.46
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 