Tilly of Bloomsbury (1931 film)
 Tilly of Bloomsbury}}
{{Infobox film
| name           = Tilly of Bloomsbury 
| image          =
| caption        =
| director       = Jack Raymond
| producer       = Jack Raymond
| writer         = Ian Hay (play)   W. P. Lipscomb Richard Bird Edward Chapman
| music          = 
| cinematography = Freddie Young
| editing        = Thorold Dickinson
| studio         = Sterling Films
| distributor    = Sterling Films
| released       = 1931
| runtime        = 70 minutes
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} Richard Bird Edward Chapman. Tilly of aristocrat and tries to convince his parents that she is herself wealthy. 

==Cast==
* Sydney Howard as Samuel Stillbottle 
* Phyllis Konstam as Tilly Welwyn  Richard Bird as Dick Mainwaring  Edward Chapman as Percy Welwyn 
* Ellie Jeffreys as Lady Marion Mainwaring 
* Marie Wright as Mrs. Banks 
* Mabel Russell as Mrs. Welwyn 
* H. R. Hignett as Lucius Welwyn 
* Ena Grossmith as Amelia Welwyn 
* Sebastian Smith as Abel Mainwaring 
* Leila Page as Sylvia 
* Olwen Roose as Constance Damery 
* T. Gordon Blythe as Metha Ram

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 