Bandham
{{Infobox film
| name = Bandham
| image =
| image_size =
| caption =
| director = K. Vijayan
| producer = Anandavalli Balaji
| writer =
| screenplay =
| starring = Sivaji Ganesan Kajal Kiran Shalini Anand Babu
| music = Shankar Ganesh
| cinematography = Thiwari
| editing =
| studio = Sujatha Cine Arts
| released =  
| country = India Tamil
}}
 1985 Cinema Indian Tamil Tamil film, directed by K. Vijayan and produced by Anandavalli Balaji. The film stars Sivaji Ganesan, Kajal Kiran, Shalini and Anand Babu in lead roles. The film had musical score by Shankar Ganesh.    

==Cast==
*Sivaji Ganesan 
*Kajal Kiran 
*Shalini 
*Anand Babu 
*Jaishankar  Manorama

==Soundtrack==
The music was composed by M. S. Viswanathan.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Janaki || Vaali || 
|-  Janaki || Vaali || 
|-  Vaali || 
|-  Vaali || 
|-  Vaali || 
|}

==References==
 

==External links==
*  

 
 
 
 
 


 