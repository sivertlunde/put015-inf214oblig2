The Envy of Gods
The Envy of Gods (  - Zavist Bogov) is a 2000 Russian drama film by Vladimir Menshov.

== Plot ==
In the "History (Herodotus)|History" Herodotus says, "The gods do not like happy people". In ancient times, people knew why they can not demonstrate their good fortune and happiness: it can cause jealousy and anger of the gods, because they believed that happiness is their, gods, privilege, and daring people who want the same thing, should be punished.
 war in incident with the South Korean "Boeing", etc.).

The main heroine of the picture Sonia (Vera Alentova) is editor in television, she has a son, a graduate of the school, and her husband Sergey (Alexander Feklistov) is quite a successful Soviet writer. One day to visit them coming Frenchman Bernard (Gérard Depardieu), with whom he met Sergey while working on a book about the French Resistance, accompanied by a French translator of Russian descent named André (Anatoly Lobotsky). André also have a family in Paris. However, the meeting turned their lives. André falls in love with Sonia at first sight and emphatically looking for a new meeting with her. Sonia, a faithful wife, educated in the Soviet tradition of chastity, at first refuses; moreover that employees of Soviet television are not allowed to enter unsupervised contact with foreigners. But André still gets her, and Sonia also possessed a violent passion. She discovers such strong feelings about the possibility of which she previously doent know. A few days of his life and Sonia and Andre were absolutely happy. But they soon had to separate because their countries belong to different worlds, divided by the Iron Curtain.

== Actors ==
* Vera Alentova - Sonia
* Anatoly Lobotsky - André
* Alexander Feklistov - Sergei, Sonias husband
* Gérard Depardieu - Bernard
* Alexander Voroshilo - Krapivin
* Vladlen Davydov - Sonias father
* Marina Dyuzheva - Natasha, Sonias friend
* Lyudmila Ivanova - Sonias mother-in-law
* Yuri Kolokolnikov - Sonias son
* Victor Pavlov - Vilen
* Irina Skobtseva - Sonias mother
* Leonid Trushkin - Vadim
* Larissa Udovichenko - Irina Vladimir Eremin - Natashas husband
* Igor Kirillov - cameo
* Anna Shatilova - cameo
* Valery Prokhorov - trucker
* Daniil Spivakovsky - taxi driver
* Natalia Khorokhorina - administrator of movie theatre
* Valentinа Ushakovа - railway worker

== Sources ==
*   at IMDB
*   at kino-teatr.ru

 
 
 
 
 


 