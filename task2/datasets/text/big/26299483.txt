Der Weibsteufel (1966 film)
{{Infobox film
| name           = Der Weibsteufel
| image          = 
| caption        = 
| director       = Georg Tressler
| producer       = Otto Dürer
| writer         = Adolf Opel Karl Schönherr Wilhelm Sorger Georg Tressler
| starring       = Maria Emo
| music          = 
| cinematography = Sepp Riff
| editing        = Hermine Diethelm
| distributor    = 
| released       = 28 April 1966
| runtime        = 91 minutes
| country        = Austria
| language       = German
| budget         = 
}}

Der Weibsteufel is a 1966 Austrian drama film directed by Georg Tressler. It was entered into the 16th Berlin International Film Festival.   

==Cast==
* Maria Emo
* Sieghardt Rupp
* Hugo Gottschlich
* Vera Complojer (as Vera Comployer)
* Richard Tomaselli
* Margarete Reimann
* Gottfried Rieder

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 