Pooja (film)
{{Infobox film 
| name           = Pooja
| image          =
| caption        =
| director       = P Karamachandran
| producer       = P Karamachandran
| writer         = P Karamchandran
| screenplay     = P Karamchandran
| starring       = Prem Nazir Sheela Sukumari T. S. Muthaiah
| music          = G. Devarajan
| cinematography = RN Pillai
| editing        = NM Victor
| studio         = Shivachandra Productions
| distributor    = Shivachandra Productions
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, directed by P Karamachandranand produced by P Karamachandran. The film stars Prem Nazir, Sheela, Sukumari and T. S. Muthaiah in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir
*Sheela
*Sukumari
*T. S. Muthaiah
*Bahadoor GK Pillai
*Kottarakkara Sreedharan Nair
*Panjabi
*Prathapan
*Vijayanirmala

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maanasa Saarasamalar (F) || S Janaki || P. Bhaskaran || 
|-
| 2 || Maanasa Saarasamalarmanjariyil   || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Maavin Thayyinum || P Susheela || P. Bhaskaran || 
|-
| 4 || Olakkathaaliyum || P Susheela || P. Bhaskaran || 
|-
| 5 || Oru Kochu Swapnathinte  || P. Leela || P. Bhaskaran || 
|-
| 6 || Swargeeya Sundaranimisham || S Janaki || P. Bhaskaran || 
|-
| 7 || Vanachandrikayude || P. Leela || P. Bhaskaran || 
|-
| 8 || Vidoorayaya Thaarake || S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 