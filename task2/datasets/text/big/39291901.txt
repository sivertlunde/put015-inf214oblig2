The Gordon Sisters Boxing
{{Infobox film
| name           = The Gordon Sisters Boxing
| image          =
| image size     =
| caption        =
| director       = Thomas A. Edison Edison Manufacturing Co.
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = May 6, 1901
| runtime        =
| country        = United States
| language       = Silent film
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 American short short black-and-white silent film directed by Thomas A. Edison. It is one of the earliest female boxing movies. Edison’s film catalogue describes the film as follows: “Champion lady boxers of the world. Here we depict two female pugilists that are really clever. They are engaged in a hot and heavy one-round sparring exhibition, which is photographed against a very pleasing background, consisting of a park, with marble entrance and walk, and beautiful trees and shrubbery. The exhibition is very lively from start to finish; the blows fall thick and fast, and some very clever pugilistic generalship is exhibited.” 

In an analysis of boxing in the context of modernism, Irene Gammel argues that the scene’s “symmetry and beauty   towards the artfulness of boxing as a cultivated sport.” In addition, she argues that the women’s choreographed movement shows “their boldly modern female physicality and sportsmanship.” 

==References==
 

== External links ==
* 
*  on  

 
 
 
 
 
 
 


 