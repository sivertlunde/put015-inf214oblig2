The Young Land
{{Infobox film
| name           = The Young Land
| image          = TheYoungLand-1.jpg
| image size     = 200px
| alt            = 
| caption        = The Young Land DVD Cover
| director       = Ted Tetzlaff
| producer       = Patrick Ford Lowell J. Farrell Cornelius Vanderbilt Whitney
| writer         = John H. Reese Norman S. Hall
| narrator       = 
| starring       = Patrick Wayne Dennis Hopper Yvonne Craig Dan OHerlihy Ken Curtis Pedro Gonzalez-Gonzalez
| music          = Dimitri Tiomkin  Henry Sharp
| editing        = Tom McAdoo
| studio         = C. V. Whitney Pictures Columbia Pictures Corporation VCI Entertainment (DVD)
| released       = May 1, 1959
| runtime        = 89 minutes USA
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} American Western Western drama Columbia Pictures Corporation.
 The Searchers in 1956 with John Wayne and directed by John Ford, the second being The Missouri Traveler in 1958 with Brandon deWilde and Lee Marvin.

Having previously been featured in a number of his fathers films, this was the 20-year-old Waynes attempt at a leading role while he was still enrolled at Loyola Marymount University, graduating from there in 1961.

The film was scored by Dimitri Tiomkin who earned a nomination for Academy Award for Best Original Song for "Strange Are the Ways of Love" (The Young Land theme) with lyrics by Ned Washington and sung by Randy Sparks.

==Plot== American gunslinger Mexican man United States marine and now sheriff with neither a gun nor a badge. Appointed by  prominent local businessman and politico Don Roberto de la Madrid (Roberto De La Madrid), Ellison has designs on de la Madrids spoiled daughter, Elena (Yvonne Craig). By the book Judge Millard Isham (Dan OHerlihy) arrives in town with Deputy Marshal Ben Stroud (Cliff Ketchum) to conduct the trial. Wanted tough guy Lee Hearn (Ken Curtis) has problems of his own with the law, but is willing to help Ellison as a deputy. Carnes is then placed on trial in the new Mexican Cession territory with the Hispanic populace waiting to learn if American justice will convict Carnes.

==Cast==
*Patrick Wayne (as Pat Wayne) as Sheriff Jim Ellison
*Dennis Hopper as Hatfield Carnes
*Yvonne Craig as Elena de la Madrid
*Dan OHerlihy as Judge Millard Isham
*Roberto de la Madrid as Don Roberto de la Madrid
*Cliff Ketchum as Deputy Marshal Ben Stroud
*Ken Curtis as Lee Hearn
*Pedro Gonzalez-Gonzalez as Deputy Santiago
*Ed Sweeney (as Edward Sweeney) as Sully / Kelly
*John Quijada as Vasquero
*Miguel Comacho as Miguel
*Tom Tiner as Court Clerk Charlie Higgins Carlos Romero as Francisco Quiroga
*Eddie Juaregui (as Edward Juaregui) as Drifter
*Los Reyes De Chapala as Mariachis

==Home media==
No less than six different releases of The Young Land were produced on VHS. Similarly, several DVD versions were released over the years, mainly as an inclusion in a multiple film "western pack". Presently, The Young Land is available on Region 0 DVD through VCI Entertainment and video on demand in standard non-widescreen format.

==Trivia==
Roberto de la Madrid, who appeared in the film as the father of Yvonne Craigs character, was later elected governor of Baja California.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 