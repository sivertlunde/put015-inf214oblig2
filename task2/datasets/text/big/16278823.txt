The Country Mouse and the City Mouse: A Christmas Tale
{{Infobox television film |
  name         = The Country Mouse and the City Mouse: A Christmas Tale |
  format       = Animated / Childrens / Holiday |
  runtime      = 25 min. |
  director     = Michael Sporn |
  producer     = Michael Sporn |
  writer       =   Teleplay: Maxine Fisher |
  starring     = John Lithgow Crystal Gayle |
  music        = David Evans |
  network      = HBO |
  released     = December 8, 1993 |
  country      =   |
  language     = English }} fable that is set around Christmastime.

The specials two characters, Emily and Alexander, were voiced respectively by Crystal Gayle and John Lithgow. These two cousins would appear in the animated series The Country Mouse and the City Mouse Adventures, also on HBO (owned by Time Warner).

==Synopsis==
At a house in the country, a female mouse named Emily, whose existence is known to the two children living there, decides to go into the city to visit her cousin Alexander for Christmas. However, the chef the restaurant Alexander lives in has set a variety of anti-mice precautions, thus scaring the two cousins out of the restaurant. The two mice return to the country house to celebrate Christmas together.

==External links==
* 
* 
*  at Variety (magazine)|Variety

 
 
 
 
 
 


 