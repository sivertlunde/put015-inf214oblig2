9 Days in Summer
{{Infobox film name            =9 Days in Summer image           = caption         = director  Philip Bond producer        =Norman Vigars writer  Philip Bond starring        =Jim Clark Graham Hill Colin Chapman Keith Duckworth Walter Hayes Harley Copp music           =Jeff Wayne cinematography  =Dennis Ayling Hilton Craig editing         =Peter Gold distributor  Ford Film Unit Philip Bond and Partners released        =1967 runtime         =48 min. country         =United Kingdom language        =English budget          =
|}}
9 Days in Summer is a promotional documentary film made by Ford that tells the story of the development of the Ford-funded Cosworth DFV and Lotus 49, the title coming from the nine Formula One races the car took part  in 1967 Formula One season|1967, which are also extensively featured in the film.

==Background== Climax V8s BRM and their heavy, unreliable H16 engine Chapman persuaded Ford to fund the development of a new 3 litre V8 produced by Cosworth. The Ford Cosworth DFV was an innovative engine designed to form part of the chassis of the car, and went on to become the most successful engine in Formula 1 history amassing an incredible 155 wins. 

==Plot==
 The film opens showing Cosworth designer Keith Duckworth silently working away at his drawing board, then cuts to footage of the 1967 Dutch Grand Prix, over which the credits are superimposed, incorporated into a series of stylised gauges and dials. Jim Clark is shown winning the engines début race.
 Belgian Grand Prix is then shown, Graham Hill and Clark both retiring.
 French Grand Prix is seen, where again both cars retire.
 British Grand Prix but Hill retires and is shown doing an impromptu interview where he praises the new car.
 Lotus factory where the Lotus 49 chassis is shown at an early stage of production. The chassis is finished and the engine mated to it for the first time, and Chapman explains the theory behind the new car. In 1967 German Grand Prix|Germany, Hills car is shown being lifted onto a trailer after a practise shunt. The leading drivers are shown and named, and the race gets under way. Several cars are shown getting airborne over the Nürburgrings jumps. Both Lotuses retire with suspension trouble.

A Team Lotus transporter arrives at Snetterton Motor Racing Circuit|Snetterton, where Graham Hill tests the completed car for the first time. "Its got some poke, not a bad old tool" is Grahams verdict. To Canada, where the race is wet. Various double-exposure scenes of cars splashing around the wet circuit are shown. Clark retires, Hill finishes fourth.
 Surtees wins "by one frame of our camera, thats a forty-eighth of a second" over Jack Brabham|Brabham.
 Watkins Glen, then fades to the start of the 1967 United States Grand Prix|race. Clark and Hill lead, while various other cars are shown encountering problems. Chapman cheers his cars home in first and second place.
 Hulme is shown sharing the victors laurel with Clarke, and the film ends showing a model of the Lotus 49 with five trophies in the background. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 