Samurai Sentai Shinkenger vs. Go-onger: GinmakuBang!!
{{Infobox film
|name=Samurai Sentai Shinkenger vs. Go-onger: GinmakuBang!!
| film name = {{Film name| kanji = 侍戦隊シンケンジャーVSゴーオンジャー 銀幕BANG!!
| romaji = Samurai Sentai Shinkenjā tai Gōonjā Ginmakuban}}
|image=Samurai Sentai Shinkenger vs. Go-onger - GinmakuBang!!.jpg
|director=Shōjirō Nakazawa
|writer=Yasuko Kobayashi
|starring= 
|music=  cinematography = Fumio Matsumura editing = Ren Sato
|distributor= 
|released=  runtime = 64 minutes
|country=Japan
|language=Japanese
}}
 }} is a Japanese film released in theaters on January 30, 2010, featuring a crossover between the Samurai Sentai Shinkenger and Engine Sentai Go-onger casts and characters. The heroes of Tensou Sentai Goseiger also make a cameo appearance in the film.  The catchphrase for the movie is  . It opened at #5 its opening weekend and earned the equivalent of US $5 millions.  Its footage was used for the Power Rangers Samurai team up episode "Clash of the Red Rangers - The Movie".

==Plot== ditch Shinken Gold as they retreat into the Sanzu River while he fights the Ugatz before Go-on Red and the other Shinkengers arrive to support him. The battle ends with the vassals getting sucked into a dimensional rift to save Shinken Red and Go-on Red from Batcheeds attack. While Genta and Mako meet the Sutō Siblings in Christmas World, Kotoha and Chiaki are jailed with Renn and Gunpei by DaiGoyou in Samurai World, and Ryunosuke ends up in Junk World alone until he is found by Saki and Hant as they cheer him up.

Back in the Human World, as Takeru and Sōsuke manage to resolve their differences, the two learn that Hikoma Kusakabe and Bomper have been kidnapped and are being held at Mount Aguruma where an entire army of Nanshi and Ugatz under Batcheed await them. However, when Takeru disregards the safety of the hostages for the sake of the world, Go-on Red is forced to fight Shinken Red. As Batcheed watches on that his enemies are fighting each other, he soon discovers that the fight is only a diversion to allow the Sisi Origami to help Jii and Bomper escape as the Engines arrive with the other Go-ongers and Shinkengers. Together, the two Super Sentai teams defeat the grunts while Shinken Red and Go-on Red take to the road to battle Homurakogi. Though Akumaro attempts to get involved in the fight alongside Juzo and Dayu, the three are driven off by the Goseigers. Giving Go-on Red the Kyoryu Disk so he can become Hyper Go-on Red as Shinken Red becomes Super Shinken Red, the two are joined by the others as they slay Homurakogi. Enlarging with his revived aide, Batcheed escapes to the Moon where the Batchrium Plant is, using Homurakogi as a shield to escape the Engine/Origami team attack as they form Samuraihaoh and Engine-Oh G12 while he hooks himself up to his Batchrium Plant to achieve his full power. The two teams are powerless until the combinations reconfigure themselves into Samurai Formation 23 to destroy Batcheed before performing a victory clap. Soon after, the Go-ongers head off to the other Braneworlds to find any more surviving Gaiark officers, wishing the Shinkengers luck in keeping their world safe.  In the ending theme, we get to see the Shinkengers origami in "soul form" and Shinkengers dance number in Samurai World.

==Cast==
* Takeru Shiba/Shinken Red:  
* Ryunosuke Ikenami/Shinken Blue:  
* Mako Shiraishi/Shinken Pink:  
* Chiaki Tani/Shinken Green:  
* Kotoha Hanaori/Shinken Yellow:  
* Genta Umemori/Shinken Gold:  
* Hikoma Kusakabe:  
* Juzo Fuwa:  
* Sōsuke Esumi/Go-on Red:  
* Renn Kōsaka/Go-on Blue:  
* Saki Rōyama/Go-on Yellow:  
* Hant Jō/Go-on Green:  
* Gunpei Ishihara/Go-on Black:  
* Hiroto Sutō/Go-on Gold:  
* Miu Sutō/Go-on Silver:  
* Kegalesia:  
* Child Santa:  
* Guard Santa:  
* Okappikki:  

===Voice actors===
* Narrator, Sushi Changer, Inromaru:  
* DaiGoyou:  
* Doukoku Chimatsuri:  
* Dayu Usukawa:  
* Shitari of the Bones:  
* Akumaro Sujigarano:  
* Homurakogi:  
* Engine Speedor:  
* Engine Bus-on:  
* Engine Bear RV:  
* Engine Birca:  
* Engine Gunpherd:  
* Engine Carrigator:  
* Engine Toripter:  
* Engine Jetras:  
* Engine Jum-bowhale:  
* Bomper:  
* Yogostein:  
* Kitaneidas:  
* Batcheed:  
* Gosei Red (Alata):  
* Gosei Pink (Eri):  
* Gosei Black (Agri):  
* Gosei Yellow (Moune):  
* Gosei Blue (Hyde):  
* Tensouder:  

==Theme song==
*   
** Lyrics: Shoko Fujibayashi and Mike Sugiyama
** Composition: Kenichiro Ōishi, Psychic Lover|YOFFY, and Hideaki Takatori
** Arrangement: Project.R (Kenichiro Ōishi)
** Artist: YOFFY, Sister MAYO, and Hideaki Takatori (Project.R)  

==Notes==
{{reflist|group=Note|refs=
 The subtitle   is a pun as it is pronounced the same as  . The Go-onger film   was similarly subtitled   which was a pun on the word  . 
}}

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 