Out of an Old Man's Head
{{Infobox Film
| name = Out of an Old Mans Head 
| image = Huvet affisch.gif
| caption = Theatrical release poster
| producer = Gunnar Karlsson
| writer = Hans Alfredson Tage Danielsson
| starring = Hans Alfredson Karl-Axel Forssberg
| director = Per Åhlin (animation) Tage Danielsson (live action)
| distributor =
| released = 6 December 1968
| runtime = 77 min.
| country = Sweden
| language = Swedish
| music = 
| editing = 
| budget = 
| box office = 
| preceded_by = 
| followed_by = 
}} Swedish film urban renewal of the Klara quarters in Stockholm, and could be seen as a comment both on the demolishing of the old buildings and on the welfare state in general.

Out of the total running time of 77 minutes, about 50 minutes are animation|animated. It is considered to be the first Swedish animated feature film, although the first one that was fully animated was Agaton Sax and the Bykoebing Village Festival.

==Plot==
On the way home to the run-down shack he lives in with a can of dog food meant for himself, Johan Björk is accidentally covered by a pile of car tires thrown by a man who fails to notice him. His cries for help arent perceived by anyone except one drunk, who is caught by the police when calling for assistance. Finally, at night, a young couple making love on the pile of tires notice him. They help him to get up and assist him to his home. He is hurt in the leg and understands that he cant take care of himself anymore, so he is put in a retirement home.

At the home, Johan is sitting in a wheelchair staring out the window, mumbling for himself about his past to an animated Flashback (narrative)|flashback. Johan grew up on the countryside, when the grass was always green. His father was strong, his mother liked to bake. He was also harassed by a boy named Evert, while at the same time being in love with Everts cute sister. At his confirmation, Evert tackled him so that all the girls dresses were ruined by the sacramental wine. This made Everts sister laugh at him, and with his heart broken he decided to run away to Stockholm.

Johan started to work as a masonry, and carried bricks and bottles of beer to a foreman who consumed one bottle for ever brick he laid. In his loneliness, Johan fantasized about women, and made his first rousing visit to a brothel. He grew a moustache and visited an amusement park with two friends he had made, Sven and a man from Värmland who he has a hard time remembering the name of. They flirted with girls, and everybody laughed when Johan failed at kicking a can in order to camouflage a fart.

A nurse brings Johan back to reality to give him his medicine. Johan fantasizes about constructing a pyramid scheme that will give him thousands of whiskey bottles. He imagines a life of luxury in Monte Carlo, where he is also being chased by gangsters.

Once again Johan goes back to thinking about his past. He is struck by death angst while thinking of his dead parents. Johan also receives a letter from his son who ran away to America many years ago, since Johan wasnt a particularly successful father. The son is asking for money, while Johan is imagining what the American things mentioned in the letter might look like.

Johan is thinking about the modern day. The blocks he was involved in building are being demolished to give room for modernistic city planning. He summarizes his failed life for himself: poverty ruined his relationship, his son ran away and the wife died. He befriends Sven, another man at the retirement home, and together they drink to make the memories fade.

==Cast==
* Hans Alfredson as Johan Björk
* Karl-Axel Forssberg as Sven Juhlin
* Monica Ekman as Female student
* Gösta Ekman as Male student
* Rolf Bengtsson as Drunk man
* Fatima Ekman as Foreign lady
* Nurse Ester as Herself
* Nurse Alise as Herself
* Tage Danielsson as Car driver
* Ernst Günther as Man throwing car tires

==Production==
The live-action scenes were shot on     

==Release== SEK during the theatrical run in Sweden.  It is yet to be released on home media.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 