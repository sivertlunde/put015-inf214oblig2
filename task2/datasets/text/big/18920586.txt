William Comes to Town
{{Infobox film
| name = William Comes to Town
| image = "William_Comes_to_Town"_(1948).jpg
| caption = U.S. lobby card
| director = Val Guest
| producer = 
| writer = Richmal Crompton Val Guest
| starring = William Graham  Garry Marsh Jane Welsh
| music   = Robert Farnon
| cinematography = Bert Mason	
| editing  = Carmen Beliaeff
| distributor = United Artists Corporation
| released = 31 December 1948	(London)  (UK)
| runtime = 89 min.
| language = English
| budget =
| music =
| awards =
| followed_by =
}}
  British comedy Just William Just Williams Luck. It is also known by its U.S. alternative title William Goes to the Circus.  

==Plot==
William Brown and his gang the Outlaws visit the Prime Minister in Downing Street to demand shorter school hours and better pay for kids. The newspaper publicity caused by their visit lands William and his friends in trouble with their parents. William almost ruins his chances of going to the circus (his parents made him promise to stay out of trouble), but somehow he finally finds his way there.

==Cast==
* William Graham - William Brown 
* Garry Marsh - Mr. Brown 
* Jane Welsh - Mrs. Brown 
* Hugh Cross - Robert Brown 
* Kathleen Stuart - Ethel Brown 
* Muriel Aked - Emily, the maid 
* A. E. Matthews - Minister for Economic Affairs 
* Brian Weske - Henry 
* James Crabbe - Douglas  Brian Roper - Ginger  Michael Balfour - Stall-holder 
* Michael Medwin - Reporter 
* Jon Pertwee - Circus Superintendent 
* David Page - Hubert Lane (as David Paige) 
* Norman Pierce - Police Sergeant 
* Eve Mortimer - Postmistress 
* John Powe - Glazier 
* Mary Vallange - Maid 
* Peter Butterworth - Postman 
* Donald Clive - Ethels boyfriend 
* John Warren - 2nd Circus official 
* Alan Goford - 1st Circus official 
* Basil Gordon - 3rd Circus official 
* Claude Bonsor - 4th Circus official 
* Ivan Craig - 1st Carter 
* John Martell - 2nd Carter 
* Pinkie Hannaford - Small boy 
* Edward Malin - Toy Shop Man 
* Slim Rhyder - Tramp Cyclist
* Arthur Stanley - Oldest Inhabitant

==Critical reception==
Allmovie called the film, "one of the better efforts in this off-and-on series."  

==Bibliography==
* Collins, Fiona & Ridgman, Jeremy. Turning the Page: Childrens Literature in Performance and the Media. Peter Lang, 2006.

==References==
 

==External links==
 

 
 

 
 
 
 
 