The Return of Peter Grimm
{{Infobox film
| name           = The Return of Peter Grimm
| image          = 
| alt            =
| caption        = 
| director       = George Nicholls, Jr. Doran Cox (assistant)
| producer       = Kenneth Macgowan
| writer         = Francis Edwards Faragoh
| based on       = The Return of Peter Grimm, a play by David Belasco New York, 1911 Edward Ellis Donald Meek
| music          = Alberto Colombo
| cinematography = Lucien Andriot Arthur Schmidt RKO Radio Pictures
| distributor    = 
| released       =   }}
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
 Edward Ellis, and Donald Meek. 
 The Return of Peter Grimm.

==Cast==
*Lionel Barrymore - Peter Grimm
*Helen Mack - Catherine Edward Ellis - Dr. Andrew Macpherson
*Donald Meek - Everett Bartholomew
*George Breakston - William
*Allen Vincent - Frederik
*James Bush - James
*Ethel Griffies - Mrs. Martha Bartholomew
*Lucien Littlefield - Colonel Tom Lawton
*Greta Meyer - Marta
*Ray Mayer - The Clown

==See also==
*Lionel Barrymore filmography

==References==
 

==External links==
* 
* 


 
 
 