A Study in Scarlet (1933 film)
{{Infobox film
| name           = A Study in Scarlet
| image          = OwenAsHolmes.jpg
| image_size     =
| caption        = Reginald Owen as Sherlock Holmes
| director       = Edwin L. Marin
| producer       = Samuel Bischoff
| writer         = Robert Florey (screenplay) Arthur Conan Doyle  (novels title)
| narrator       =
| starring       = Reginald Owen Anna May Wong
| music          =
| cinematography = Arthur Edeson
| editing        = Rose Loewinger
| distributor    = Sono Art-World Wide Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States English
| budget         =
}}
 of the Holmes series, but the screenplay by Robert Florey was original.  
 Sherlock Holmes the previous year; Owen was one of only four actors to play both Holmes and Watson (Jeremy Brett played Watson on stage in the United States prior to adopting the mantle of Holmes on British television, Carleton Hobbs played both roles in British radio adaptations while Patrick Macnee played both roles in US television movies). Warburton Gamble, the actor cast as Watson in this film, physically more closely resembles Doyles description of Holmes, fueling speculation that the actors might have switched roles prior to production, especially since Owen had played Watson the year before in the series previous film.

==Cast==
* Reginald Owen - Sherlock Holmes
* Anna May Wong - Mrs. Pyke
* June Clyde - Eileen Forrester
* Alan Dinehart - Thaddeus Merrydew
* Alan Mowbray - Lestrade
* Warburton Gamble - John Watson (Sherlock Holmes)|Dr. Watson
* Wyndham Standing - Captain Pyke
* Halliwell Hobbes - Malcolm Dearing
* Doris Lloyd - Mrs. Murphy 

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 

 