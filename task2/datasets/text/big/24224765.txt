Six-Gun Rhythm
{{Infobox film
| name           = Six-Gun Rhythm
| image          =
| image_size     =
| caption        =
| director       = Sam Newfield
| producer       = Norman Haskall (associate producer) Sam Newfield (producer)
| writer         = Ted Richmond (original story) Fred Myton (screenplay)
| narrator       =
| starring       = See below
| music          = Johnny Lange Lew Porter Arthur Reed
| editing        = Robert O. Crandall
| distributor    =
| released       = 17 February 1939
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Six-Gun Rhythm is a 1939 American film directed by Sam Newfield.

== Cast ==
*Tex Fletcher as Tex Fletcher
*Joan Barclay as Jean Harper Ralph Peters as Spud Donovan
*Reed Howes as Jim Davis
*Malcolm Bud McTaggart as Don Harper Ted Adams as Sheriff Joe
*Walter Shumway as Henchman Bart
*Kit Guard as Henchman Pat
*Carl Mathews as Henchman Jake Art Davis as Henchman Mike
*Robert Frazer as Lem Baker
*Jack McHugh as Teammate Butch James Sheridan as Henchman Slim

== Soundtrack ==
* Tex Fletcher - "Theres a Cabin in the Valley" (Written by Johnny Lange and Lew Porter)
* Tex Fletcher - "When I Get Back on the Range" (Written by Johnny Lange and Lew Porter)
* Tex Fletcher - "They Wont Stretch My Neck if I Know It" (Written by Johnny Lange and Lew Porter)
* "Serenade to a Lovely Senorita" (Johnny Lange and Lew Porter)
* Tex Fletcher - "Git Along Little Doggies" (Traditional)
* Tex Fletcher - "Lonesome Cowboy" (Written by Johnny Lange and Lew Porter)

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 