Fish & Cat
{{Infobox film
| name           = Fish & Cat
| image          = Mahi_Va_Gorbeh_Poster.jpg
| alt            = 
| caption        = English poster of the film
| director       = Shahram Mokri
| producer       = Sepehr Seifi Kanoon Iran Novin
| writer         = Shahram Mokri
| starring       = {{Plain list | 
* Babak Karimi
* Saeed Ebrahimifar
* Siavash Cheraghipoor
}}
| music          = Christophe Rezai
| cinematography = Mahmoud Kalari
| editing        = 
| studio         =
| distributor    = Filmiran (Iran)  Iranian Independents (International)
| released       =  
| runtime        = 134  minutes
| country        = Iran
| language       = Persian
| budget         = 
| gross          = 
}} Iranian film directed by Shahram Mokri. The film narrated in 140 minutes tells the story of a group of university students camping at a lakeside for Kite-running competitions, all through a without-a-cut single shot.    The film was first premiered in Venice Film Festival in 6 September 2013.  The film won the Special Award in 2013 Venice Film Festival and the FIPRESCI award in 2014 Fribourg International Film Festival.   

Fish & Cat is inspired by the true story of a restaurant that served human flesh in northern Iran at late 90s. Iranian actors Babak Karimi and Saeid Ebrahimifar along with some other young theatre artists have acted in the film.   
 Azadi cinema complex and Kourosh Complex in Tehran and Howeyzeh cinema complex in Mashhad. 

==Plot==
A number of students have traveled to the Caspian region in order to participate in a Kite-Flying|kite-flying event during the winter solstice. Next to their camp is a small hut occupied by three cooks who work at a nearby restaurant.

==Cast==
 
*Abed Abest as Parviz
*Mona Ahmadi as Nadia 
*Ainaz Azarhoush	as Parvaneh
*Nazanin Babaei	as Shirin
*Mohammad Berahmani as Boy
*Siavash Cheraghipoor as Father
*Saeed Ebrahimifar as Saeed
*Alireza Esapoor	as Guard
*Neda Jebraeili as Mina
*Shadi Karamroudi as Maral
*Babak Karimi as Babak
*Mohammad Reza Maleki as Jamshid
*Faraz Modiri as Kambiz
*Milad Rahimi as Shahrooz
*Arnavaz Safari as Asal
*Nima Shahrabi as Twin
*Pouya Shahrabi as Twin
*Khosrow Shahraz as Hamid
*Pedram Sharifi as Pedram
*Parinaz Tayyeb as Maryam
*Samaneh Vafaiezadeh as Ladan
 

==Production==
{{Quote box quote  = Fish & Cat is a film about time. How you can make a perspective in it and break that. This film was interesting for me to make and that was because of its method of production. Insisting on narrating a story in a single shot and try to break the time which goes ahead. source = Shahram Mokri  width  = 50% align  = right
}}

===Technic===
{{Quote box quote  his paintings. source = Shahram Mokri,  in an interview with The Hindu    width  = 50% align  = left 
}}
Gradually subverting a gruesome premise drawn from a real-life case of a backwoods restaurant that served human flesh, the film builds an atmosphere of tension as a menacing pair descend on a campsite where a group of college kids have gathered for a kite-flying festival. But as the camera doubles back and criss-crosses between characters in real time, subtle space-time paradoxes suggest that something bigger is going on.   

===Music=== horror Z-movies style of music, so he asked Kristoph Rezaee to compose it.   

==Critical reception==
  — the cinematographer is Mahmoud Kalari, who shot “A Separation” — and as quietly political as it is brazenly cinematic."  

Steve MacFarlane from Slant Magazine wrote: "The film is a game: Shahram Mokri challenges his viewers to grip parallel narrative threads in what feels like suspiciously real time, rather than to assemble or contextualize any metaphorical ones."   
 Variety wrote: "“A highly original, compelling feature, filmed in one long,bravura shot establishing Shahram Mokri as a distincive talent.”    

==Awards==
*Dubai International Film Festival 2013 - Muhr AsiaAfrica Special Jury Prize
*Fribourg International Film Festival 2014 - FIPRESCI Prize and Youth Jury Award    Venice Film Festival 2013 - Venice Horizons Award - Special Prize  
*13th Lisbon International Film Festival - Best Film award 
*13th If Istanbul Independent Film Festival - Best Film Award

==References==
 

==External links==
* 
* 

 
 
 
 
 