Breaking Point (2009 film)
{{Infobox film
| name           = Breaking Point
| image          = Breakingpointmovie.jpg
| image size     =
| caption        =
| director       = Jeff Celentano
| producer       = Evolving Productions
| writer         = Vincent Campanella
| starring       = Tom Berenger Armand Assante Busta Rhymes Sticky Fingaz Musetta Vander Frankie Faison Robert Capelli Jr. Pinar Toprak
| cinematography = Emmanuel Vouniozos Doug Crise Ryan Folsey
| distributor    = Cinema Epoch
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
}}
Breaking Point is a 2009 action-drama film starring Tom Berenger, Busta Rhymes, Musetta Vander and Sticky Fingaz. It is directed by Jeff Celentano with a screenplay written by Vincent Campanella. The film was showcased in Cannes and was released theatrically on December 4, 2009. 

==Cast==
* Tom Berenger as Steven Luisi 
* Busta Rhymes as Al Bowen 
* Armand Assante as Marty Berlin 
* Musetta Vander as Celia Hernandez 
* Frankie Faison as Judge Green 
* Sticky Fingaz as Richard Allen (as Kirk Sticky Fingaz Jones) 
* Curtiss Cook as Byron Young

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 