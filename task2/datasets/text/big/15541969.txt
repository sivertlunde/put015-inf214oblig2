Fashion (film)
 
 
 
{{Infobox film
| name           = Fashion
| image          = Fashion film.jpg
| caption        = Theatrical release poster
| alt            = The poster depicts three women standing at a ramp, looking forward with confidence. Text at the top of the poster reveals the title and production credits.
| director       = Madhur Bhandarkar
| producer       = Madhur Bhandarkar Deven Khote Ronnie Screwvala Zarine Mehta
| story          = Ajay Monga
| screenplay     = Ajay Monga Madhur Bhandarkar Anuraadha Tewari
| narrator       = Priyanka Chopra Arbaaz Khan Arjan Bajwa
| music          = Salim-Sulaiman
| cinematography = Mahesh Limaye
| editing        = Deven Murdeshwar
| distributor    = UTV Motion Pictures Bhandarkar Entertainment
| released       = 29 October 2008
| runtime        = 161 minutes  
 
| country        = India
| language       = Hindi
| budget         =    
 
| gross          =     
}}

Fashion is a 2008 Indian drama film directed and co-produced by Madhur Bhandarkar. The films screenplay was co-written by Ajay Monga, Bhandarkar and Anuraadha Tewari, and principal photography occurred in  Mumbai and Chandigarh. Its music was composed by Salim-Sulaiman and the songs lyrics were written by Irfan Siddiqui and Sandeep Nath.
 Arbaaz Khan in supporting roles. The cast also features several professional fashion models playing themselves.
 Best Director Best Actress Best Supporting Best Actress Best Supporting Actress award for Ranaut at the 56th National Film Awards . Several publications listed Fashion as one of the "best women-centric films in Bollywood".      

==Plot==
 
Aspiring women model Meghna Mathur (Priyanka Chopra) wants to go to Mumbai and become a supermodel. Against her fathers wishes, Meghna leaves her home to find success in the modelling world. Meghna meets an old acquaintance, Rohit (Ashwin Mushran) a gay, aspiring designer who assists Vinay Khosla (Harsh Chhaya). Meghna experiences difficulties during her early days; she auditions several times and is rejected. Meghna meets and befriends another struggling model, Maanav (Arjan Bajwa).

At Rahul Aroras (Samir Soni) fashion show, Meghna meets supermodel Shonali Gujral (Kangna Ranaut) and is ridiculed by the shows choreographer, Christine, and told to have her portfolio photographed by Kartik (Rohit Roy). To afford Kartiks fees, Meghna shoots a lingerie advertisement; she is mentored by another aspiring model, Janet (Mugdha Godse). Meghnas lingerie photos appear on the cover of a magazine; her relatives in Mumbai see the cover and ask her to leave the house. She shares an apartment with Manav. Meghna is noticed by  Anisha Roy (Kitu Gidwani), an executive, of Panache, a major modeling agency. Anisha introduces her to her superior, Abhijit Sarin (Arbaaz Khan), who is impressed by Meghnas ambition and confidence. Panaches model is Shonali, but her drug abuse becomes problematic. Abhijit include Meghna in a fashion show organised by Vinay Khosla, but is replaced after a misunderstanding. Abhijit consoles her, and decides to replace Shonali with Meghna as Panaches new model.

Meghna becomes an overnight success, ends her relationship with Maanav and begins an affair with Abhijit. Meanwhile, Janet goes to work for designer Rahul Arora. Rahuls mother becomes increasingly concerned about his sexual orientation; he asks Janet to marry him. Shonalis drug abuse worsens and she attends a rehabilitation clinic. Meghna pays the price of her increasing fame by losing her close friends due to her changed attitude towards them. She becomes pregnant with Abhijits child, and reluctantly has an abortion due to conditions in her contract. After realizing that she was deceived in her relationship, Meghna tells Abhijits wife about their relationship, and Abhijit ends Meghnas contract with Panache. Upset at the turn of events, Meghna descends into alcoholism; at a rave party she uses cocaine and unwittingly has a one-night stand. When sober, she feels guilty and returns to her parents in Chandigarh.

Broken and depressed, Meghna lives in Chandigarh for over a year. Her father encourages her to return to Mumbai; Meghna rekindles her friendship with Janet and models for Rohits show, where she freezes on the ramp after seeing the lights and cameras. She visits Maanav (now an established model) to apologize and learns that he is engaged. Shonali appears on television as a mentally ill, homeless alcoholic; Meghna takes her in and tries to rehabilitate her. Meghna accepts an offer from Rahul to model, but a day before the show Shonali vanishes. Just before Meghna walks the ramp, she receives a call from the police telling her that Shonali is dead from a drug overdose. Meghna freezes; overcoming her grief, she walks the ramp, reviving her career and restoring her self-confidence. Meghna gives up drinking and smoking, and as the film closes she walks the ramp in Paris.

==Cast==
* Priyanka Chopra as Meghna Mathur, a small-town girl who becomes a successful model
* Kangna Ranaut as Shonali Gujral, a successful model who experiences a downfall
* Mugdha Godse as Janet Sequeira, a model
* Arjan Bajwa as Maanav, a struggling model who eventually becomes an established model
* Samir Soni as Rahul Arora, a designer
* Ashwin Mushran as Rohit Khanna, an aspiring designer
* Kitu Gidwani as Anisha Roy, head of modelling agency Panache Arbaaz Khan as Abhijit Sarin, a fashion tycoon
* Suchitra Pillai-Malik as Avantika Sarin, wife of Abhijit Sarin
* Rohit Roy as Kartik Suri, a photographer
* Raj Babbar as Meghnas father
* Kiran Juneja as Meghnas mother
* Chitrashi Rawat as Shomu Manini Mishra as Sheena Bajaj
* Harsh Chhaya as Vinay Khosla
* Konkona Sen Sharma (cameo) as herself
* Ranvir Shorey (cameo) as himself
* Wendell Rodricks (cameo) as himself
* Manish Malhotra (cameo) himself
* Karan Johar (cameo) as himself
* Madhur Bhandarkar (cameo) as himself
* Diandra Soares (cameo) as herself
* Kanishtha Dhankhar (cameo) as herself
* Aanchal Kumar (cameo) as herself
* Usha Bachchani as Sheetal
* Rakshanda Khan (cameo) as a reporter
* Rohit Verma as Viren (special appearance)
* Atul Kasbekar (Cameo) as a photographer

==Production==

===Development===
In a 2006 interview with CNN-IBN, Madhur Bhandarkar said that he was preparing to make a film about the fashion industry; he thought the lack of Indian films on this subject prompted him to do so.    He said he was fascinated with the industry, the models and their lives outside the ramp walks, saying "I was amazed at the overwhelming attention given to the fashion Industry by the media and the public. I thought the idea of delving deep into the fashion world was very interesting and intriguing."     He was excited about depicting the people behind the ramp because most people are unfamiliar with  the fashion world. 

Bhandarkar, who is known for studying his subject matter to make his films as realistic as possible, researched the fashion industry for nearly eight months, attending fashion events and shows.   He spoke with designers, buyers, choreographers and models.        Several media publications reported that the film was inspired by the lives of fashion models such as Shivani Kapur, Geetanjali Nagpal, Kate Moss and Alicia Raut; however, Bhandarkar stated that the story is fictional and does not resemble anyones life.   The media also reported that the film revolved around two homosexual fashion designers; Bhandarkar denied these rumours, saying the film did not revolve around male characters but had female protagonists. 

===Casting and characters===
{{multiple image
 
| align     = right
| direction = vertical
| footer    =  Chopra (top), Ranaut (middle)  and Godse (bottom) were cast in the central roles to portray models in the film
| width     = 
 
| image1    = Priyanka nikon event1.jpg
| width1    = 180
| alt1      = a woman looking forward, posing for the camera
| caption1  =

 
| image2    = Kangana Ranaut.jpg
| width2    = 180
| alt2      = a woman looking forward, posing for the camera
| caption2  =

 
| image3    = Mugdha Godse supporting Sandip Soparrkars Dance for a cause.jpg
| width3    = 180
| alt3      = a woman looking forward, posing for the camera
| caption3  = 
}}

Casting began in October 2007, when Priyanka Chopra signed to play the lead role in the film.  Chopra initially refused the role, feeling she would not be able to do it justice. Six months later, she agreed to appear in the film because of Bhandarkars confidence in her.    The media reported that Chopra was unhappy with the first draft of the film and wanted script changes before signing the film. Bhandarkar was also unhappy with the first draft and agreed to rewrite the script.  Kangna Ranaut was cast as the films second lead; she was pleased with the script and found it challenging, interesting and exciting.     In December 2007, Arbaaz Khan and Arjan Bajwa were cast opposite Chopra.   After confirming the principal cast, the media reported that Bhandarkar was having difficulty casting the male roles, including that of a homosexual clothing designer.  Samir Soni and Harsh Chhaya were cast as the homosexual designers,  and model Mugdha Godse was cast in a supporting role.  According to Godse, Bhandarkar offered her a part when they were shooting a jewellery advertisement; a year later, she agreed to appear in the film.    The Hindustan Times said that several models and designers played themselves, giving the film the appearance of a documentary.  

Chopra said she studied method acting to make her character seem real.  The actress said that her character is determined and added, "While her father wants her to become an accountant she aspires to become the leading model in fashion industry".     Addressing rumours that the film was an exposé of the fashion industry, she said, "It is nothing like that. Whatever problems that come up to my character in the film are not because of the profession but because of the choices that my character makes in her life".  Ranaut said, "In this script my character is such that you get to express you emotions, emotions that you have never delivered before in front of the camera. So when I heard the script, it was something new and much more challenging, more interesting and exciting."  Godse said that she accepted her role in the film because she identified with her character, Janet Sequira, whom she described  as "outgoing, vibrant, bold and outspoken".  Arjan Bajwa described his character, an aspiring fashion model, as uncompromising, achieving success in his own way.  Samir Soni found his role as a closeted fashion designer very challenging, requiring research to flesh out the character and his personality. Sonis greatest challenge was to avoid a stereotypical homosexual character, instead subtly expressing his sexuality in his personality and relationships with the films other characters. 

===Styling===
Rita Dhody and Narendra Kumar were chosen to style the films characters.  Dhody said it took nearly two months for UTV and Bhandarkar to persuade her to help design the film; she found the job challenging because she had to make everything as authentic as possible.  Chopra had to wear 137 costumes in the film because her character passed through five phases.      Initially, she was a "hep" character with brightly coloured, matching attire. Her make-up becomes lighter as she becomes a model and later a supermodel. When her career falters, Chopras look becomes softer, demonstrating her characters lack of concern about her appearance before she returns to Mumbai.  According to Dhody, Chopras look is "very contemporary and a very much todays look, but my idea was to streamline and take it to the classic side   than making it trend-based". 

Ranauts character was designed to make audiences feel sympathy for her.  Dhody gave her short dresses with low necklines, conveying the image of a supermodel comfortable with her body; during her low phase, she is shown in long t-shirts. 

Godse, whose character is a failed model, has a carefree look. Dhody said, "Godses look in the film is a fusion of rock and a grunge to give her the survivor look as a B-grade model, making her more chic and glamorous."  She wears low-waisted trousers, chains and ankle boots. Details of jewellery, clothes, colour and accessories outline the characters; everything had to be sychronised and complement each other to create an authentic look.  Dhody said, "We didnt try to sensationalise it by making it more bling- or costume-based. I had to restrain myself from going overboard due to Bhandarkars reservations".  The films narrative included 11 character-based ramp shows. 

===Filming===
Principal photography commenced in December 2007,  and the film was shot extensively in Mumbai.  The scene with Ranauts wardrobe malfunction during a ramp walk was filmed in late January 2008 at Mehboob Studios, Bandra. While filming the scene, only essential cast and crew members included Bhandarkar and his cinematographer were present; according to Bhandarkar, it was a sensitive scene and a sensitive issue.    Chopra had to gain   as a girl from the Punjab, and then lose it as a model; she had five "looks" in the film, connoting the phases through which her character passes.  Although the media spread rumours that Chopra was starving herself to reduce her weight to that of a supermodel, she said that her appearance was achieved with two months of training and a disciplined diet.  The films climactic scene in which Chopra walks the ramp at Paris Fashion Week was supposed to be filmed in Paris, with the Eiffel Tower as a backdrop,    but Bhandarkar could not shoot there.  Instead, the scene uses some footage from the actual fashion show.  In mid-August 2008, key scenes with Chopra, Raj Babbar and Kiran Juneja Chopras characters parents were filmed in Chandigarh for authenticity; Bhandarkar said, "we wanted to bring the essence of the city alive in the scenes".  The director said the film took about 106 days to complete, compared with his 45-day average. 

==Themes and allusions==
 

Fashion is Bhandarkars fourth womans film|women-centric film after Chandni Bar (2001), Page 3 (film)|Page&nbsp;3 (2005) and Corporate (film)|Corporate (2006) to earn widespread praise for their strong, independent female characters.  According to The Times of India, Fashion "is not about cliches alone and manages to transcend them with its moving tale of three women who try to maintain their honour, dignity,   identity in a cruel world that spills over with grime behind the glamour. The film not only takes you behind the psychedelic ramp, it travels through the dark inner recesses of the three lead characters, laying bare their strengths and weaknesses; their fears and hopes; their dreams and nightmares".    Ananya Bhattacharya of Zee News said that the film "went on to speak volumes about women, their aspirations and the dark side-effects of over-ambition".  In an interview with the Hindustan Times, Chopra said that the film is "an emotional tale and the story of three girls&nbsp;...&nbsp;  shows female bonding in a beautiful way&nbsp;...&nbsp;". 

Reviewer Kriti Verma of Headlines India said that the movie "shows every bit about the life of models and bring the inside secrets of   fashion industry. It has everything fashion weeks, casting couch, drugs, abuses, gay designers, bitching."    Taran Adarsh from Bollywood Hungama wrote, "the narrative is inevitably filled with lots of glam-n-glitz moments. The fashion shows, the stunning models, the superb styling, the vibrant colours and the behind the scenes drama is worth every penny spent on the ticket."    A reviewer from Sify.com found the films depiction of fashion industry "superficial"; noting that the film does not talk about the clothing, the creativity, or the passion for work in the fashion industry.    However, according to Bhandarkar the film was "not about   fashion industry as such. It only has fashion as a backdrop. The film is more about the story of models, designers and people. And it is on aspects like the personal story, the individual story, their ups and downs, their journey that I tried to capture."  Critics also said that the film explores drug abuse, homosexuality and gay relationships.   Designer Vineet Bahl complained about the male characters, most of whom are portrayed as "gay designers and exploitative agency owners". He said, "Bhandarkar could have done without gross generalisations. It makes the movie shallow and over-dramatic". 

The film alludes several times to actual incidents, including a scene where Bhandarkar appears at a fashion show to research his upcoming film on the fashion industry. One of the models looking at him says, "Oh thats Madhur Bhandarkar, the realistic filmmaker. Now he is going to expose the fashion world."    It is taken from the Bhandarkars experiences while researching the subjects of his films.   The Indian Express said that Chopras character, who indulges in drug and alcohol abuse and undergoes rehabilitation, resembles the life of British-educated model Shivani Kapur, who had similar experiences.  The scene in which Ranauts characters top slips off during a ramp walk resembles the experience of Carol Gracias at the 2006 Lakme Fashion Week. 

==Soundtrack==
{{Infobox album  
| Name        = Fashion
| Type        = Soundtrack
| Artist      = Salim-Sulaiman
| Cover       = Fashion_new_2.jpg
| Border      = yes
| Alt         = The poster showing three women, looking forward in confidence, with light pink themed colour in the background
| Caption     = Soundtrack cover
| Released    =  
| Recorded    = 2008 Feature film soundtrack
| Length      =   Hindi
| Label       = T-Series
| Last album  = Roadside Romeo (2008)
| This album  = Fashion (2008)
| Next album  = Rab Ne Bana Di Jodi (2008)
}}
 Siddhivinayak Temple in Mumbai. The cast played the films title song, offering their first CD to Ganesha.  

Fashion s soundtrack received favourable reviews from critics. The Hindustan Times rated the album 3.5 out of 5 and said, "Its very hard to find anything wrong with the album. Salim-Suleiman have done a wonderful job as composers and have brought out the essence of the movie in a remarkable way. Irfan has come out with great lyrics and every singer has given his or her best shot".  Joginder Tuteja of Bollywood Hungama described the album as "a winner all the way", and said, "Madhur Bhandarkar films havent been known for their music, even though the albums of Corporate and Page 3 still saw some sales on the stands. However, Fashion is all set to break the jinx while turning out to be the first Madhur Bhandarkar film ever to boast of a truly mass as well as class appealing score". 
Sukanya Verma of Rediff.com rated the album three out of five, and said that the film offers a well-designed mix of trendy tunes and refined melodies.  
 

{{track listing
| headline       = Track listing
| extra_column   = Singer(s)
| lyrics_credits = yes
| title1         = Fashion Ka Jalwa
| extra1         = Sukhwinder Singh, Satya Hinduja, Robert Bob Omulo
| music1         = Salim-Sulaiman
| lyrics1        = Sandeep Nath
| length1        = 4:43
| title2         = Mar Jawan
| extra2         = Salim Merchant, Shruti Pathak
| music2         = Salim-Sulaiman
| lyrics2        = Irfan Siddiqui
| length2        = 4:01
| title3         = Kuch Khaas
| extra3         = Mohit Chauhan, Neha Bhasin
| music3         = Salim-Sulaiman
| lyrics3        = Irfan Siddiqui
| length3        = 5:03
| title4         = Aashiyaana
| extra4         = Salim Merchant
| music4         = Salim-Sulaiman
| lyrics4        = Irfan Siddiqui
| length4        = 5:29
| title5         = Fashion Ka Jalwa (Remix)
| extra5         = Sukhwinder Singh, Satya Hinduja, Robert Bob Omulo
| music5         = Salim-Sulaiman
| lyrics5        = Sandeep Nath
| length5        = 4:40
| title6         = Mar Jawan (Remix)
| extra6         = Salim Merchant, Shruti Pathak
| music6         = Salim-Sulaiman
| lyrics6        = Irfan Siddiqui
| length6        = 4:26
| title7         = Kuch Khaas (Remix)
| extra7         = Mohit Chauhan, Neha Bhasin
| music7         = Salim-Sulaiman
| lyrics7        = Irfan Siddiqui
| length7        = 4:17
| title8         = Theme of Fashion (Remix)
| extra8         = Various artists
| music8         = Karsh Kale and Medeival Punditz
| lyrics8        = Irfan Siddiqui
| length8        = 6:15
| lyrics9        = Irfan Siddiqui
| title9         = Aashiyaana (Remix)
| extra9         = Salim Merchant
| music9         = DJ Amyth
| length9        = 5:50
| title10        = Theme of Fashion
| extra10        = Various artists
| music10        = Salim-Sulaiman
| lyrics10       = Irfan Siddiqui
| length10       = 4:02
}}

==Marketing and release== LG Electronics. Products from these brands were showcased in the film and the brands promoted the film in their advertising. 
 Censor Board for Film Certifications decision to grant the film an "Central Board of Film Certification#Current certificates|A" certificate meant for viewers 18 years or older instead of the "Central Board of Film Certification#Current certificates|U/A" certificate that he had expected. His previous films received U/A certification despite adult content. According to them, their new guidelines did not allow Fashion a U/A certification. The board was willing to issue a U/A certificate after several cuts; Bhandarkar refused and accepted the A certificate since the scenes in question were crucial to the narrative.  Before its release, the Delhi Commission for Women (DCW) asked Bhandarkar to screen the film before its release due to its concern that Ranauts character was based on Geetanjali Nagpal a former high-profile fashion model found begging, bedraggled and mentally unbalanced on the streets of Delhi in 2007.    Nagpal was helped by the DCW, which did not want anyone profiting from her life.  Bhandarkar narrated a summary of the films plot at the DCW office, which satisfied the commission that there was no reference to Nagpal. A screening of the film was also held for the commission. 
 Moser Baer Home Videos, one disc contains the film; the other disc has additional content on the making of the film and interviews with the stars and director.  VCD and Blu-Ray versions were released at the same time.  The films television premiere took place on UTV Movies on 15 March 2009. 

==Reception==
Fashion garnered generally positive reviews from critics, who praised its direction, screenplay, cinematography, music and performances. Kriti Verma of Headlines India rated the film 4 out of 5, describing it as "an outstanding film and a must watch for people who visit a theatre for something more than entertainment".  Zee News gave the film a rating of 4 out of 5, called it "a must watch" and praised the sets, music, performances and storyline.  Nikhat Kazmi rated the film 3.5 out of 5, describing it as " a sensitive parable on bonding and empowerment".  Taran Adarsh rated the film 3.5 out of 5, praised the cinematography, screenplay, dialogue, costumes and styling, and called Fashion a "hard-hitting film that enlightens you, moves you, motivates and deflates you, even shocks and surprises you and of course, entertains you".  Rajeev Masand gave the film a rating of 3 out of 5, and wrote, "Fashion is an easy watch because the very subject lends itself to so much interest. Like the director’s own film Page 3 it is mostly sensational and on several occasions compromises authenticity for the sake of exaggerated drama". He praised the performances of the lead cast, writing that "Chopra turns in a respectable performance, one that will inevitably go down as her best. The truth is, it’s one of those by-the-book characters that she understands only too well and performs just as easily. Ditto for Kangana Ranaut who plays the eccentric superdiva with such a practiced hand, it almost seems like an effortless delivery. Its&nbsp;...&nbsp;Godse who invests such sincerity into her role, that she makes the character a real flesh and blood person. Kitu Gidwani, as the agency head who mentors our protagonist, Kitu strikes all the right notes, never once slipping out of character." 

Subhash K. Jha of the Indo-Asian News Service rated the film 3 out of 5, calling it "a truly outstanding film". He said, "Bhandarkar takes us through a labyrinth of emotions, some devastating in their gut-level directness. But at the end, we come away with a film that gives us something to hold on to permanently even as the characters on screen lose practically everything worth holding on to". Jha also wrote that the sequences between Chopra and Ranaut as the highlights of the film.  Gaurav Malani from The Economic Times rated the film a 3 out of 5, praised Chopras performance and wrote, "Chopras character graph   skillfully sketched from an aspiring model to an ambitious showstopper to a brash supermodel and subsequently to a faded manikin, desperate to make her comeback. In each phase her character exudes   contrasting gamut of emotions".  Khalid Mohamed rated Fashion 3 out of 5, called the film a "must-try", and wrote, "Excelling on the catwalk as well as in the moments of self-destruction, Ranaut’s is the terrific, show-stopping performance".  Sonia Chopra of Sify criticised the films superficial portrayal of the fashion industry and called the film a superficial take on the lives of models and a dumbed-down version of the workings of the fashion industry. However, she praised the performances. 

Fashion opened well at the box office, filling 90–100 percent of seats in most Indian multiplexes.    The film competed with the opening of a popular comedy, Golmaal Returns, but both films were box-office successes.  Fashion earned approximately   on its opening day, {{cite web|title=Golmaal Returns, Fashion light up box office this Diwali
|url=http://businessofcinema.com/bollywood-news/golmaal-returns-fashion-light-up-box-office-this-diwali/25554|author=Bhandari, Rohini|publisher=Business Of Cinema|date=30 October 2008|accessdate=27 April 2013}}  the highest for a Madhur Bhandarkar film to date.  The film went on to gross   at the box office.  Fashion is noted for being a commercial success despite being a women-centric film without a male lead. 
 Ministry of Information and Broadcasting of the Government of India), which aims to safeguard the heritage of Indian cinema, preserved Fashion with others directed by Bhandarkar, including Chandni Bar (2001) and Page 3 (2005).    In 2013, The Times of India included the film on its list of "Top 10 path breaking women oriented films of Bollywood".  For International Womens Day 2013, Zee News included the film on its list of best women-centric films. 

==Awards and nominations==
  Best Supporting Best Director Best Screenplay Best Actress Best Supporting Actress (Ranaut).   It was nominated for six awards at the fourth Apsara Film & Television Producers Guild Awards, winning three: Best Actress (Chopra), Best Supporting Actress (Ranaut) and Best Female Debut (Godse).  

==References==
 

==External links==
* 
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 