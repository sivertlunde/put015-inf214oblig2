Yashoda Krishna
 
{{Infobox film
| name           = Yasoda Krishna
| image          = Yashoda Krishna.jpg
| image_size     =
| caption        =
| director       = C. S. Rao
| producer       = Mahija Prakasa Rao
| writer         = Arudra
| narrator       = Jamuna Baby Krishna Kumari S. Varalakshmi
| music          = Saluri Rajeswara Rao
| cinematography = G. K. Ramu
| editing        = R. Surendranath Reddy
| studio         = Premier Studios, Mysore
| distributor    =
| released       = January 20, 1975
| runtime        =
| country        = India Telugu
| budget         =
}}
 Jamuna as Yasoda and S. V. Ranga Rao as Kamsa.

==Plot==
The film picturised the some events in the life of Lord Krishna. It begins with the marriage of Devaki and Vasudev, birth of Krishna, Krishna Leelalu and killing of Kamsa by Krishna.

==Credits==
===Cast===
* Jamuna Ramanarao	 as 	Yasoda
* S.V. Ranga Rao	as 	Kamsa
* Baby Sridevi	as 	Young Lord Krishna
* Ramakrishna	as 	Lord Krishna
* S. Varalakshmi
* Gummadi Venkateswara Rao  as    Nanda
* Sridhar    as   Vasudev Krishna Kumari   as   Devaki Chandra Mohan as Narada
* Manju Bhargavi   as   dancer

===Crew===
* Director: C. S. Rao
* Producer: Mahija Prakasa Rao
* Production company: Venus Maheejaa Pictures
* Production executive: B. Renuka Prasad
* Writer: Arudra
* Film Editing: R. Surendranath Reddy
* Art director: S. Krishna Rao
* Director of Photography: G. K. Ramu
* Stills: Radhakrishna Murthy
* Choreography: Pasumarthi Krishna Murthy
* Music Director: Saluri Rajeswara Rao
* Lyrics: Arudra, Kosaraju, Dr. C. Narayana Reddy, Srirangam Srinivasa Rao
* Playback singers: Ghantasala Venkateswara Rao, P. Susheela, V. Ramakrishna, Madhavapeddi Satyam, A. P. Komala, B. Vasantha and Vijayalakshmi Sharma

==Soundtrack==
{{Infobox album  
| Name        = Yasoda Krishna
| Type        = soundtrack
| Artist      = Saluri Rajeswara Rao
| Cover       =
| Caption     =
| Released    = 1975
| Recorded    =
| Genre       =
| Length      = Telugu
| Label       =
| Producer    = Mahija Prakasha Rao
| Reviews     =
| Compiler    =
| Misc        =
  DIRECTOR    = KAMALAKARA KAMESWARA RAO
}}
* Anna Chamimpu Manna (Singer: P. Susheela)
* Kalayo Vaishanava Mayayo (Singer: P. Susheela)
* Kalyaana Vaibhogame Ilalo Kannula Vaikuntamu (Singers: P. Susheela, B. Vasantha)
* Manasu Doche Doravu Neeve (Singers: P. Susheela and Vijayalaxmi Sharma)
* Nallani Vaadu Padma Nayanammula Vaadu (Singers: P. Susheela, B. Vasantha)
* Nela Moodu Vaanalu Nilichi Kuriseyi (Singers: V. Ramakrishna, B. Vasantha)
* Nomu Pandindi Maa Nomu Pandindi (Singer: P. Susheela)
* Oogindi Nalo Ananda Dola (Singers: P. Susheela, B. Vasantha)
* Paaliya Vachina Padathi Pootana (Singers: Madhavapeddi Satyam and P. Susheela)
* Ponnalu Virise Velalo Vennela Kuruse Relalo (Singers: B. Vasantha and Vijayalaxmi Sharma)
* Sarasarammulu Gadiche
* Srungara Vathulara Siggela (Singer: P. Susheela)

==External links==
*  

 
 
 