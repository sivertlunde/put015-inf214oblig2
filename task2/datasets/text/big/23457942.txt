Naturally Obsessed
{{Infobox film name = Naturally Obsessed image = Rob_at_Tank_Horizontal.jpg caption = director = Richard Rifkind  Carole Rifkind assistant producer = Hayley Downs cinematography = Buddy Squires editing = Richard Hankin  Lisa Palatella music = David Majzlin released =   runtime = 60 minutes country = United States language = English
|
}} AMPK protein molecule, revealing a new path towards the treatment of diabetes and obesity.  The focus of the film, however, is mostly on the human side of doing science.  Following the emotional ups and downs of three graduate students (Robert Townley, Kilpatrick Caroll and Gabrielle Cummberley ) guided by their professor and laboratory head along the challenging and uncertain journey to the PhD degree, it highlights the qualities that contribute to the making of a scientist, such as persistence, skill in asking the right questions, mastering the needed technology, mentoring and being mentored, collaborating and facing competition.

==Background==
The film was produced by the not-for-profit ParnassusWorks, headed by Dr. Richard Rifkind, formerly director of research and Chairman of the Sloan-Kettering Institute in New York City, where he also was responsible for graduate student education.  Long concerned with public understanding of science, upon his retirement from SKI, Rifkind turned to filmmaking in order to illuminate, for a general public, what research is all about—how new scientists are trained, the critical role that students play in the process of discovery, and the drama of research that he, himself, experienced during his 50 years as a bench scientist.

==Critical reception==
Reviewers in the popular and science press have highlighted the veracity of the film:

Ann Hornday, The Washington Post:
  "Naturally Obsessed," a funny, lively, thoroughly absorbing documentary … moves viewers with ease and intimacy through a world too often portrayed as boring, nerdy and filled with indecipherable jargon.”   

Karen A. Frenkel, TALKING SCIENCE:
  “Much of the film is concerned not so much with the specifics of research, but with what sacrifices people are willing to make for the sake of discovery. This is as it should be; the public needs to better understand what goes on in the minds of those working in Ivory Towers.”   

Emily D. Slutsky and Lauren E. Wool, BioTechniques:
  “’Naturally Obsessed’ makes it clear that there is no way to make science easy: as a whole, it is nothing less than an ongoing lesson in dedication and persistence.”    

Christopher Mims, POP-SCI.COM:
  “’Naturally Obsessed: The Making of a Scientist’ is the best film ever to depict what goes on inside a real science lab -- period.   

==Production==
The production team also includes co-producer/director Carole Rifkind; Academy Award-nominated and Emmy award winner cinematographer, Buddy Squires; award winning editors Richard Hankin and Lisa Palatella; Academy Award animators, Caroline and Frank Mouris; composer David Majzlin; music supervisor, Susan Jacobs; consulting producer, Marion Swaybill; associate producer, Hayley Downs.

==Screenings and Festivals==
In 2009, Naturally Obsessed was screened at universities, research institutes, high schools, cultural organizations, government agencies, and corporations nationwide. The film was also screened at the following film festivals:

*2009 Atlanta Film Festival
*2009 Big Sky Film Festival
*2009 DC Environmental Film Festival
*International Science Film Festival in Athens, 2009
*18th Woods Hole Film Festival
*World Science Festival/Global Nomads multi-school screening

==References==
 

==External links==
* 
* 
* 


 
 
 
 
 