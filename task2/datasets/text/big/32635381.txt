Red Dog (film)
 
{{Infobox film
| name           = Red Dog
| image          = Red Dog (movie poster).jpg
| caption        = Theatrical release poster
| director       = Kriv Stenders
| producer       = Julie Ryan Nelson Woss
| writer         = Daniel Taplitz
| based on       =   Koko Josh John Batchelor
| music          = Cezary Skubiszewski
| cinematography = Geoffrey Hall
| editing        = Jill Bilcock Roadshow Film Distributors
| released       =  
| runtime        = 92 minutes 
| country        = Australia English Italian Italian Serbian Serbian Russian Russian Polish Polish
| budget         = A$8.5 million 
| gross          = A$21 million 
}}
 Red Dog Red Dog. AACTA Awards Best Film.

==Plot== Luke Ford) arrives in Dampier, Western Australia|Dampier, Western Australia, late one night. Upon entering the town pub he sees the silhouettes of a group of men, one of whom is holding a gun. Believing it is a murder, he rushes into the next room, where he sees that the men are trying to put down an apparently sick dog (Koko (dog)|Koko). Unable to bring themselves to carry out the euthanasia, the men, with Thomas, retreat to the bar.
 Red Dog and narrates his story. Upon arriving in Dampier, the dog befriends many of the employees of Hamersley Iron, who have a major iron ore excavation in progress. Various miners relate their stories of Red Dog to Thomas, but state that, while Red Dog was a dog for everyone, he had no real master.

The men then tell of an American named John Grant (Josh Lucas), who became Reds true master. John, a bus driver for Hamersley Iron, starts dating a woman named Nancy (Rachael Taylor), who is a secretary at Hamersley Iron. After living in Dampier for two years, John proposes to Nancy. On the night of the engagement, John tells Red Dog to stay until he returns from Nancys caravan. Early the next morning, John rides his motorcycle from Nancys caravan, but is killed in an accident on the way

In the shock of Johns accident, Nancy and the Hamersley men forget about Red Dog. Three days after the funeral, they find him still waiting where John told him to stay. After three weeks Red dog decides to look for John, first at Hamersley Iron, then the bar and other places where John was known to go, until all of Dampier is explored. He then continues across much of the Australian North West Pilbara region from Perth to Darwin. He is even rumoured to have caught a ship to Japan in search of John. Finally, the grief catches up to him, and he decides to return to Dampier. When he arrives, he returns to Nancy at the caravan park where she is staying, and she is overwhelmed to see him. The caretakers of the caravan park, however, do not allow dogs in the park, and threaten to shoot Red Dog. Nancy and Johns friends at Hamersley then travel to the community of Dampier in support of Red Dog and, after a "civilised chat" with some of the miners, the caretaker and his wife leave, leaving their cat, Red Cat, behind. A great fight between Red Dog and Red Cat ensues, and in the end, they resolve their differences and become mates.

Back in the present day, miner Jocko (Rohan Nichol) asks the gathered crowd why they should have a statue of a man (William Dampier) set in their town when all he did in relation to the place was say that there were too many flies, and suggests that they should instead erect a statue of someone who represents the town – Red Dog. During the celebrations that follow, Red Dog gets up and walks out of the bar, unnoticed by everyone. Upon realising that the sick dog has left, everyone in the town begins looking for him, eventually finding him lying dead in front of Johns grave.

One year later, Thomas once again drives up to Dampier with a new puppy, a new Red Dog and the whole town unveils a statue of Red Dog, a statue which still stands today.

==Cast== John Batchelor, Kriv Stenders, Nelson Woss and Daniel Taplitz at 1st AACTA Awards 2012]] Koko as Red Dog
*Josh Lucas as John Grant
*Rachael Taylor as Nancy Grey John Batchelor as Peeto
*Noah Taylor as Jack Collins
*Keisha Castle-Hughes as Rosa
*Loene Carmen as Maureen Collins Luke Ford as Thomas
*Neil Pigot as Rick
*Rohan Nichol as Jocko
*Tiffany Lyndall-Knight as Patsy
*Costa Ronin as Dzambaski
*Eamon Farren as Dave
*Arthur Angel as Vanno

===Cameos=== Bill Hunter as Jumbo Smelt
*The Snowdroppers as a band

==Story basis==
 
Red Dog (c. 1971 – 21 November 1979) was a Australian Kelpie|Kelpie/cattle dog cross who was well known for his travels through Western Australias Pilbara region. There is a statue in his memory in Dampier, Western Australia|Dampier, which is one of the towns to which he often returned.      
 Paraburdoo in 1971    and had a variety of names to those who knew him, including: Bluey, Tally Ho, and Dog of the Northwest.   
 Red Dog  as did Beverly Duckett in her 1993 book Red Dog: The Pilbara Wanderer. 
 British author Red Dog.    A Four-wheel drive|four-wheel-drive club has been named in his honour.   

== DVD and related media ==
Red Dog was officially released on DVD, Blu-ray Disc and digital download on 1 December 2011 in Australia.

The Red Dog DVD is the biggest-selling Australian DVD of all time.  The DVD is also the third-highest selling DVD of all time in Australia behind Avatar (first) and Finding Nemo (second).

== Overseas performance ==
The film has not been as successful overseas as it was in Australia.
The film opened at #25 in the United Kingdom, earning just £24,727 from 56 screens ( 24–26 February 2012)  and opened at #5 at the New Zealand box office, earning NZ$124,447 from 72 screens.   The film has been a DVD only release in territories such as Germany and Argentina  but has been acquired by independent distributor Arc Entertainment in a deal for all media in the United States, though there is "no word on a theatrical release date or strategy" in the announcement. 

==Reception==
===Commercial response===
 , the film made more than  21 million at the Australian box office since opening in August 2011.   

Red Dog is ranked eighth in the list of (Cinema of Australia) highest-grossing Australian films of all time. 11 days after opening, Red Dog became the highest-grossing Australian film of 2011. 

===Critical response===
Critical response has also been more mixed overseas than in Australia.

The Guardians Peter Bradshaw (U.K.) asked "Is it a childrens story for adults? Or an adults story for children?" and stated that the film "comes across like a well-meaning PG-certification of the real world."   The Hollywood Reporters Megan Lehmann (U.S.A.) called the film "genial but unsophisticated fare, with plenty of hammy acting and broad humor" and that it "borders on naivety, and some of the more roughly drawn characters begin to grate early" though stated that the film "achieves a kind of existential purity. But then the two-legged actors butt in and the moment is lost. 

Review aggregator Rotten Tomatoes reported that 81% of critics reviewed the film positively, with an average score of 6.6/10. 

Phillip French of The Guardian said that the film is "guaranteed to bring tears and laughter". 
 War Horse within a day of each other, and felt that Red Dog achieved much of what Spielbergs film was aiming at, with much less sentimentality, anthropomorphism and self-importance, more laughs and with an hours less running time." 

Mark Adams of the Sunday Mirror gave Red Dog a three star rating and said, "this canine true story is an engaging, feel-good Australian family drama about a dog." Adams opined that it boasted a strong cast and felt that overall it was "clichéd but charming". 
 SBS awarded Bran Nue Dae". 

===Awards and nominations===

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards|AACTA Award  (1st AACTA Awards|1st)  AACTA Award Best Film Julie Ryan
| 
|- Nelson Woss
| 
|- AACTA Award Best Direction Kriv Stenders
| 
|- AACTA Award Best Adapted Screenplay Daniel Taplitz
| 
|- AACTA Award Best Editing Jill Bilcock
| 
|- AACTA Award Best Cinematography Geoffrey Hall
| 
|- AACTA Award Best Original Music Score Cezary Skubiszewski
| 
|- AACTA Award Best Production Design Ian Gracie
| 
|- Golden Collar Golden Collar Award Best Dog in a Foreign Film Koko (dog)|Koko
| 
|- Heartland Film Festival Grand Prize Award Best Narrative Feature
| 
|- Inside Film Inside Film Award Best Feature Film Julie Ryan
| 
|- Nelson Woss
| 
|- Best Actor Josh Lucas
| 
|- Best Actress Rachael Taylor
| 
|- Best Direction Kriv Stenders
| 
|- Best Script Daniel Taplitz
| 
|- Best Editing Jill Bilcock
| 
|- Best Cinematography Geoffrey Hall
| 
|- Best Music Cezary Skubiszewski
| 
|- Best Production Design Ian Gracie
| 
|- Best Box Office Achievement
| 
|}

The film missed winning Best Editing and Best Production Design at the Inside Film Awards. 

  was played at the ceremony because he was unable to attend the event.

== Film festivals ==
Red Dog has screened at numerous film festivals around the world including:
 
* Berlin International Film Festival 2011
* Heartland Film Festival 2011
* Israeli Film Festival 2011
* Sheffield Showcomotion Young Peoples Film Festival 2011
* Melbourne International Film Festival 2011
* Inverness Film Festival 2011
* Busan Film Festival 2011
* Hawaii Film Festival 2011
* Tallinn Black Nights Film Festival 2011
* Santa Barbara Film Festival 2012
* Beijing International Film Festival 2012
* Rincon Puerto Rico Film Festival 2012
* Cannes Cinephiles 2012

==Adaptations==
===Musical===
In March 2012 it was announced  that the Red Dog film would be developed into a stage musical. The musical is being developed by Australian theatre producer   and Red Dog producer Nelson Woss.

==Prequel==
A prequel, titled Blue Dog, is set to begin filming in early 2015 for six weeks. The film will explore Red Dogs earlier days, as well as delving in to the history of the Pilbara region. Due to Koko (dog)|Kokos death, Red Dog will be recast. 

==References==
 

==External links==
 
*  
* 

 
 
 

 
 
 
 