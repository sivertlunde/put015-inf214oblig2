Dorm of the Dead
{{Infobox film
| image          =
| alt            =
| caption        =
| director       = Donald Farmer
| producer       = Jackie Napoli
| writer         = Donald Farmer
| starring       = Jackey Hall Andrea Brooke Ownbey Tiffany Shepis Ciara Richards Adrianna Eder
| music          =
| cinematography = Donald Farmer Chris Watson
| editing        = Allan McCall
| studio         =
| distributor    = Under the Bed Films
| released       =   }}
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Dorm of the Dead is a 2007 American horror film directed and written by Donald Farmer.  It stars Jackey Hall, Ciara Richards, and Adrianna Eder as college students who must survive a zombie attack.

== Plot ==
A professor returns to Arkham University with the blood of a zombie.  As he tests it on the students, Amy accidentally initiates a zombie attack.  During the confusion, bully Clare targets goths Sarah and Allison for involuntary conversion into zombies.

== Cast ==
* Jackey Hall as Clare
* Andrea Brooke Ownbey as Julie
* Ciara Richards as Sarah
* Adrianna Eder as Allison
* Tiffany Shepis as Amy
* Dukey Flyswatter as Alf

== Release ==
Dorm of the Dead was released on home video September 18, 2007. 

== Reception ==
Melissa Bostaph of Dread Central rated the film 1.5/5 stars and called it a "bullshit film" that had no effort put into it.   Brandon Ciampaglia of IGN rated the film 0/10 stars and wrote, "It is in our professional opinion that this catastrophe never be watched by anyone – ever."   Nick Lyons of DVD Talk rated the film 0.5/5 and called it an insult to the word film.   Mac McEntire of DVD Verdict described the film as "amateur hour" and technically incompetent.   Peter Dendle called it a "boring, amateurish production". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 