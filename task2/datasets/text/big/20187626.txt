Irish Twins
{{Infobox film
| name           = Irish Twins 
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Rider Strong Shiloh Strong 
| producer       = 
| writer         = Rider Strong Shiloh Strong
| screenplay     = 
| story          = 
| starring       = Rider Strong Shiloh Strong 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2008
| runtime        = 20 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Irish Twins is a 2008 film which was written and directed by brothers Rider Strong and Shiloh Strong. The lead roles were performed by Rider and Shiloh. The film also features John Di Maggio, Al Vicente, Devon McGinn, and Chris Solari. 

Rider and Shiloh won the DC Shorts Film Festival Best First Time Director Award, and an Action/Cut Special Jury Prize. They also won both the Audience Award and Jury Award for Best Dramatic Short at the Woods Hole Film Festival. 

The film premiered at the Tribeca Film Festival, and was an official selection of the Los Angeles International Shorts, Edmonton International Film Festival, Ischia Film Festival, The Raindance Film Festival, Ojai Film Festival, Bend Film Festival, Eugene Film Festival, DC Shorts, Woods Hole Film Festival, Denver Film Festival, and the St. Louis International Film Festival.

The film was produced by Toby Wilkins and Jay Chapman. The soundtrack features music by Marwood (band)|Marwood, The Ebbs, Brian Jonestown Massacre, Q Brothers, Common Rotation and other artists.

==References==
 

==External links==
* 

 
 
 
 
 
 