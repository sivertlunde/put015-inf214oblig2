Mutant Aliens
 
{{Infobox film
| name = Mutant Aliens
| image = Mutant Aliens.jpg
| caption = 
| director = Bill Plympton
| producer = Bill Plympton
| writer = Bill Plympton
| starring = Dan McComas Francine Lobis George Casden Matthew Brown Jay Cavanaugh Amy Allison Christopher Schukai Kevin Kolack Vera Beren Bill Plympton
| music = Hank Bones Maureen McElheron
| cinematography = John Donnelly
| editing = Anthony Arcidi
| studio = Plymptoons
| distributor = 
| released =  
| runtime = 81 minutes
| country = United States
| language = English
| budget = 
| gross = 
}} spoof of B Horror monster movies, featuring Plymptons own distinctive animation style and gratuitous sex and violence.

Mutant Aliens tells the story of an American astronaut, Earl Jensen, who is stranded in space intentionally by the head of the Department of Space. Years later, he manages to return to Earth. To gain the peoples trust, he tells a touching story of the time he has spent on a planet of mutant Extraterrestrial life|aliens. Most of the aliens in this story are oversized human body parts. It is later revealed that Jensen has really spent his time in space crossbreeding animals to create an army of mutants, in order to exact his revenge on the corrupt Department of Space head. Mutant Aliens has shown at a number of animation festivals, but has never had a wide theatrical release.

==External links==
* 

 

 
 
 
 
 
 
 


 