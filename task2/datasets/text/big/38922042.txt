Inshallah, Kashmir
{{Infobox film
| name           = Inshallah Kashmir
| image          = Inshallah, Kashmir.jpg
| alt            =  
| caption        = Poster
| director       = Ashvin Kumar
| producer       = Ashvin Kumar
| writer         = Ashvin Kumar
| based on       =  
| starring       =  
| music          = 
| cinematography = 
| editing        = Ashvin Kumar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = India
| language       = 
| budget         = 
| gross          = 
}}
Inshallah, Kashmir (also known as Inshallah, Kashmir: Living Terror) is a 2012 documentary film directed, produced and written by Ashvin Kumar. It is the story of contemporary Kashmir. A series of counterpointed testimonies, the heartbreaking coming-of-age of ordinary people; warped and brutalised by two decades of militancy and its terrible response. Tehelka says, "Although the camera and narrator usually provide the impartial eye in a documentary, stitching the story together, in Inshallah, Kashmir it is the Kashmiris who weave their deadpan narrative into a cohesive picture. Their matter-of-fact monotone says more than an entire valley of screams could." 

==Plot==
The film opens with ex-militants describing the torture they underwent when captured by the army. A Hindu describes his sentiment on being a part of the minority in the region at the height of militancy, when his grandfather was shot dead by militants. A politician and her husband describe the horror of being kidnapped and in captivity for over a month – and despite that, forming a human bond with the militants, and helping them escape when the army closed in on them. One understands from this section that militancy was not binary in nature. It was a dynamic and complex, resulting from various socio-political, economic and religious issues.

Disappearances and fake encounters led to the creation of mass graves, hidden away in sensitive border areas that civilians and journalists are not permitted to access in the name of national security. Human rights lawyer and activist Parvez Imroz reveals to us the presence of almost a thousand such graves in the valley. Rape victims from Kunan Poshpora describe the trauma they went through at the hands of the army and the stigma that they still face due to the incident.

The film then leads us to ‘normalcy’ or the social ramifications the last twenty years of devastation brought to the valley. Militancy in Kashmir resulted in the Government of India deploying tens of thousands of armed troops in the region. We hear the story of one boy who lost his leg because he was caught in crossfire. The film ends on a poignant note with a young artist saying I need my space.

==Cast==
* Omar Abdullah as himself
* Bashir Baba as himself
* Wajahat Habibullah as himself

==Online release==
On 18 January, Alipur Films uploaded the first seven minutes of the film Inshallah Kashmir : Living Terror online.  Social networking sites like Facebook and Twitter picked it up and the link had over a ten thousand hits in a week and generated curiosity and contempt alike. Views had crossed over fifty thousand within a week.

The idea of releasing Inshallah Kashmir online and free of charge was to take the film to the masses, and to make it accessible to as many people as possible – in Kashmir, within India and around the world.  The full film went online on 26 January 2012, on the Indian Republic day and had over almost fifteen thousand hits that day.

Kumar plans to apply for a censor certificate and let his audience decide the fairness of the judgment of the Indian Censor Board, by making public all the details of the application along the way.

==Awards== National Film Best Investigative Film  

==See also== Haider

==References==
 

==External links==
 

 
 
 
 
 
 
 
 
 
 
 
 