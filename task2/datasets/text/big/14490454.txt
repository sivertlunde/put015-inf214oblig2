Heart & Soul: The Life and Music of Frank Loesser
{{multiple issues|
 
 
}}

{{Infobox film
| name = Heart & Soul: The Life and Music of Frank Loesser
| caption = "Heart & Soul” theatrical poster
| director = Walter J. Gottlieb
| producer = Walter J. Gottlieb Final Cut Productions James F. Cooney Frank Loesser Enterprises
| writer = Walter J. Gottlieb
| music = Jeff Gruber
| cinematography = Martin Andrews
| editor = Michael Gehman
| runtime = 88 minutes
| country = United States
| language = English
}}
 American documentary Guys and Broadway actress German Jewish Heart and Soul" (with Hoagy Carmichael), "On a Slow Boat to China", and "Baby Its Cold Outside".

Inspired by daughter Susan Loessers biography, "A Most Remarkable Fella", the film traces the artistic arc of Loessers career from his early days on Tin Pan Alley, to his success in Hollywood as a lyricist and (later) composer/lyricist, to his triumphs as composer/lyricist on Broadway. It also highlights Loesser as a member of that elite club of songwriters who successfully wrote both music and lyrics for Broadway, placing him in the company of Irving Berlin, Cole Porter, and (later) Stephen Sondheim. One commentator in the film calls him, "Cole Porter, without the martini in his hand," because of his ability to capture everyday American speech and emotions in music and lyrics—a songwriter whose common mans touch belied the sophistication of his music.
A significant amount of screen time is spent detailing Loessers work on Guys and Dolls, considered by many to be the best Broadway musical ever. The Guys and Dolls segment focuses on Loessers collaboration with co-book writer Abe Burrows and his use of diverse musical forms to craft what one interviewee characterizes as the first "truly American musical."

Loessers temperament is also explored, including a perfectionist streak, volatile temper, and colorful lexicon (according to his daughter, he never met a four-letter word he didnt like), improbably combined with a generous heart, a wicked sense of humor, and a commitment to mentoring the next generation of American songwriters. Loesser abhorred amplification in Broadway theaters and favored singers who could belt. According to the film, he even had a sign printed up during rehearsals for his operatic musical The Most Happy Fella declaiming his favorite slogan, "Loud is Good!"
 Broadway and Stephen Schwartz, Maury Yeston, Jerry Herman, Richard Adler, Samuel Goldwyn, Jr., Isabel Bigley, Charles Nelson Reilly, Robert Morse, and Mathew Broderick, among others. Frank Loesser was a chronic cigarette smoker and died from lung cancer at 59. Cine Golden Eagle Award in the non-fiction category. The film was re-broadcast on   (TCM) on June 29, 2010, the centennial of Loessers birth.

==External links==
*  
*   - the films official website
*   - Frank Loessers estates official website
*   at the Internet Movie Database
*   Frank Loessers entry at the Internet Movie Database
*   - article on the debut
*   - a review of the broadcast at the New York Post
*   - a review of the DVD
*   Listing at Turner Classic Movies

 
 