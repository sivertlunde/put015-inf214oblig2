Fanie Fourie's Lobola
 
 
 
{{Infobox film
| name           = Fanie Fouries Lobola 
| image          = Fanie_Fouries_Lobola_2013_film_poster.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Henk Pretorius
| producer       = Out of Africa Entertainment
| writer         = 
| screenplay     = Henk Pretorius, Janine Eser
| story          = Romantic comedy
| based on       = FILL OUT  
| starring       = 
| narrator       = 
| music          = Adam Schiff
| cinematography = Trevor Calverley
| editing        = Avril Beukes
| studio         = 
| distributor    = Indigenous Film Distribution
| released       =   
| runtime        = 90 minutes
| country        = South Africa Afrikaans
 (English subtitles provided) 
| budget         = 
| gross          = 
}}

Fanie Fouries Lobola is a 2013 South African romantic comedy based on the novel (of the same name) by Nape à Motana. The film features a culturally diverse cast of actors as well as a production team. The main focus of the film is cross-cultural relationships and the challenges associated with such relationships.

The film was shot in Johannesburg and Pretoria, both in Gauteng, South Africa.

== Plot summary ==
 
 

Fanie and Sarel are young Afrikaners; Fanie a custom car designer, Sarel a successful pop star. On a dare, Fanie invites a beautiful Zulu woman, Dinky Magubane, to come as his date to Sarels wedding. Dinky agrees, provided Fanie pretend to be her boyfriend so she can evade her familys pressure towards marriage. Despite the deceitful intent, the two begin to genuinely fall in love.

== Critical reception ==
* Daniel Dercksen from The Writing Studio awarded Fanie Fouries Lobola a rating of 5/5 in his review. Daniel sums the film up in the following brief conclusion  : "It is one of this films you can share with others who might not see the world you do, and at the end of the screening it is guaranteed that you will have made new friends. Fanie Fouries Lobola is a film that everyone can enjoy with its English subtitles and message that transcends our borders." 
* Nontsikelelo Mpulo from The Drum had the following brief conclusion in her review: "The film does not shy away from dealing with the racial prejudices that still thrive in South African society.  Fanie is a stereotypical boere seun who knows very little about Zulu culture and so he blunders into situations that have the potential to create serious offence but the script is so well written that you’ll find yourself laughing out loud at some of his ludicrous antics." 
* The website SPLING awarded the film a 7/10 in their review that came to the conclusion: "Fanie Fouries Lobola is a crowd-pleaser - a romantic comedy thats fresh, entertaining, heartwarming, street smart and sweet. Its bold to tackle an interracial romance, its bolder to turn it into a comedy. Henk Pretorius and his team have really turned Nape A Motanas story into a fun-loving and well-balanced feature film that proudly showcases our music, our diversity and our people. The bottom line: Charming" 
* Reney Warrington from Litnet came to the following conclusion in her review: "FFL pokes fun at our cultural differences, and at how little we know about the different cultures in our country, but it does so with respect, without an accusatory tone. 10 out of 10 just for that! It goes beyond poking fun. It asks questions around racism, patriarchy and sexism and even goes so far as to suggest we should give the younger generation some room to start their own traditions." 

== Awards ==

* 2013 Golden Space Needle Audience Award, Best Film, Seattle International Film Festival.   
* Audience Choice Award at the second annual Jozi Film Festival.

== Cast ==

* Eduan van Jaarsveldt as Fanie Fourie
* Zethu Dlomo as Dinky Magubane
* Jerry Mofokeng as Dumisane Magubane
* Marga van Rooy as Louise Fourie
* Motlatsi Mafatshe as Mandla
* Chris Chameleon as Sarel Fourie
* Yule Masiteng as Petrus Mabuza
* Richard van der Westhuizen as Oom Kobus
* Connie Chiume as Zinzi
* Lillian Dube as Auntie Nomagugu
* Cherie van der Merwe as Mariaan Prinsloo
* Angelique Pretorius as Santjie
* Nina Marais as Tanya
* Khumbuzile Maphumulo as Patricia Mabuza (as Khumbuzile Kgomotso)
* Leon van Nierop as Presenter
* Morne du Toit as Raucous friend

== References ==

 

== External links ==

*  
* 

== See also ==

* Klein Karoo, Regardt van den Bergh
* Semi Soet, Joshua Rous
* Paradise Stop, Jann Turner
* List of South African films

 

 
 
 