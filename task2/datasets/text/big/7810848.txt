A Chef in Love
 
{{Infobox film

 | name = A Chef in Love
 | image size     =
 | caption = 
 | director = Nana Dzhordzhadze
 | producer = Temur Babluani Alexander Rodnyansky Thomas Bauermeister Marc Ruscart
 | writer = Irakli Kvirikadze André Graill
 | narrator = 
 | starring = Pierre Richard Micheline Presle Nino Kirtadze Jean-Yves Gautier
 | music = Goran Bregovic
 | cinematography = Giorgi Beridze
 | editing = Guili Grigoriani Vessela Martschewski
 | distributor = CTV International Sony Pictures Classics
 | released =   (United States|U.S.)
 | runtime = 100 minutes
 | country = Georgia French / Georgian
 | budget = 17,000,000 French franc|₣ (est.)
 | gross = 
 | preceded by    =
 | followed by    =
 | image = A Chef in Love VideoCover.jpeg
}} Georgian film directed by Nana Dzhordzhadze. It stars Pierre Richard and Nino Kirtadze.

==Synopsis== Bolshevik coup attempt of Georgia (1920), the chef refuses to emigrate and endures the brutalities of the new regime.

==Awards== Best Foreign Language Film at the 69th Academy Awards in 1997. A Chef in Love was among the final nominees for the award, but did not win. 

==External links==
* 
*  

==References==
 

 
 
 
 
 
 
 
 
 
 


 
 