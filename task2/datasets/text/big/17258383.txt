Peggy, the Will O' the Wisp
 
{{Infobox film
| name           = Peggy, the Will O the Wisp
| image          = 
| image size     = 
| caption        = 
| director       = Tod Browning
| producer       = B. A. Rolfe
| writer         = Katharine Kavanaugh
| starring       = Mabel Taliaferro Thomas Carrigan
| music          = 
| cinematography = John M. Bauman
| editing        = Ben Weiss
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}

Peggy, the Will O the Wisp is a lost  1917 American drama film directed by Tod Browning.   

==Plot==
As described in a film magazine review,  Neil Dacey (Carrigan) loves Peggy Desmond (Taliaferro). Terence OMalley (Sack), nephew of Squire OMalley (Ryan), is anxious to win Peggy. Terence and his uncle have a quarrel because Terence cannot win Peggy, and the squire is killed. Terence does the killing with Neils gun, so Neil is held for the murder. Peggy, to save her fiance, dresses as the will-o-the-wisp, and this results in a confession by Terence.

==Cast==
* Mabel Taliaferro - Peggy Desmond
* Thomas Carrigan - Captain Neil Dacey (as Thomas J. Carrigan)
* William J. Gross - Anthony Desmond (as W.J. Gross)
* Sam Ryan - Squire OMalley (as Sam J. Ryan)
* Nathaniel Sack - Terence OMalley (as Nathan Sack)
* Thomas OMalley - Shamus Donnelly (as Thomas F. OMalley)
* Florence Ashbrooke - Sarah
* Clara Blandick - Mrs. Donnelly
* John J. Williams - Muldoon (as J.J. Williams)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 