The Queen of Sheba's Pearls
  2004 Cinema British drama film set in England post-World War II|WWII. Helena Bergström plays Nancy Ackerman who mysteriously arrives on the Pretty familys doorstep. Ackerman looks eerily similar to Jack Bradleys (James Hawkins) mother who inexplicably died in an WWII-related accident 8 years prior. As the film progresses, it becomes clear that Ackerman is the identical twin separated from Jacks mother at birth. Meanwhile Jacks father, a former marine, has clouded feelings for Ackerman; he cannot decide if his feelings for her are growing on their own merits or are based on those towards his deceased wife. Ackerman also proves to be an unsettling force in Jacks life by offering advice that turns out to be eerily omniscient and fairy godmother-like. The movie was produced, directed and written by Colin Nutley who is married to the films star, Helena Bergström. 

==Cast==
* Helena Bergström ...  Nancy Ackerman/Emily Bradley
* Lorcan Cranitch ...  Harold Bradley
* Lindsay Duncan ...  Audrey Pretty
* Tim Dutton ...  Father Talbot
* Rolf Lassgård ...  Deafy
* Natasha Little ...  Peggy Pretty
* Elizabeth Spriggs ...  Laura Pretty
* Peter Vaughan ...  Edward Pretty
* Rollo Weeks ...  Jack Bradley
* Eileen Atkins ...  School matron
* Marc Pickering ...  Dinger Bell
* Simon Day ...  Rektorn
* Bohdan Poraj ...  Teacher
* John Joe Regan ...  Geoffrey Thicket
* James Hawkins ...  Young Jack Bradley
* Alexander Goggins ...  Mr Jenkins

==Awards and recognition==
According to IMDb, Jens Fischer was nominated for the Silver and Gold Frogs and won the Silver Frog at the 2005 Camerimage Festival for his work as the lighting director for the film. The Guldbagge Film Festival awarded Lasse Liljeholm and Eddie Axberg Best Achievement for sound editing and Jens Fischer was also awarded Best Cinematography. Finally, Colin Nutley was nominated for the Crystal Globe at the Karlovy Vary International Film Festival. 

==Reception and reviews==
A number of positive reviews followed the release of The Queen of Shebas Pearls. Variety (magazine)|Variety magazine called the film "unquestionably   most substantial movie to date." 
Sandra Hall, a British critic, provides a more clouded review of the film, saying that while Nutley is "not great on plot mechanics and narrative dovetailing. Hes into vignettes rather than the big picture. Yet in its anecdotal way, the film somehow hangs together, shaping itself into a benign and sunny take on a plot device weve often seen before in films - about a fraying household rejuvenated by the presence of a seductive stranger."  However, while critics, film analysts, and awards committees agreed that the film has superb casting and behind the scenes elements it is not especially popular or well-known with the general public as evidenced by the lack of comments and support on a number of public review sites including Flixster  and Rotten Tomatoes. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 