Churchill: The Hollywood Years
 
 
{{Infobox film
| name           = Churchill: The Hollywood Years
| image          = Churchill the hollywood years poster.jpg
| image_size     =
| caption        = UK Theatrical release poster Peter Richardson Jonathan Cavendish Steve Christian Brian Donovan James Mitchell
| writer         = Peter Richardson Pete Richens
| narrator       =
| starring       = Christian Slater Neve Campbell Miranda Richardson
| music          = Simon Boswell Rod Melvin
| cinematography = Cinders Forshaw
| editing        = Geoff Hogg Duncan Shepherd John Wilson
| studio         =
| distributor    = Pathe Films
| released       =   (UK)
| runtime        = 84 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          = £148,326
}}
 Peter Richardson. It stars Christian Slater as Winston Churchill, and Neve Campbell as Elizabeth II. Miranda Richardson and Antony Sher also co star.
 Pearl Harbor (where American participation in the Battle of Britain was exaggerated).

==Plot== British court and war government consist mainly of idiots and traitors. Adolf Hitler moves into Buckingham Palace and plans to marry into the British Royal Family|Windsors. A US Army officer claims the cigar-smoking iconic PM was an actor named Roy Bubbles however, he was actually USMC lieutenant Winston Churchill who had stolen an enigma code machine and then almost single-handedly won a very alternative battle for Britain

==Production==
It was filmed between 24 March and 12 May 2003.
Mainly filmed at the Royal William Yard, Stonehouse, Plymouth

* Oldway Mansion doubles as Buckingham Palace
 
Oldway Mansion is in Paignton, Devon and was the home of Isaac Singer, the sewing machine magnate

*Powderham castle, Exeter

*The old fish quay at Brixham, Devon doubles as Plymouth Docks.

==Cultural references==
* The scene between Charoo and the waitress in a station tearoom, and Elizabeths response on Churchills arrival there, are parodies of scenes from Brief Encounter, between Stanley Holloway and Joyce Carey, and Trevor Howard and Celia Johnson, respectively.
* The cab driver and the King mistake Adolf Hitler for Charlie Chaplin, who played a spoof of Hitler in the satirical film The Great Dictator.
* The "Siegfried Line" rap takes its title and (loosely) some of its lyrics from the British wartime song "Were Going to Hang out the Washing on the Siegfried Line". The introduction to the song is a reference to Top Gun.
* The song "Hitler Has Only Got One Ball" is frequently referenced, including once where it is delivered by Tommy Trinder.
* The presence of "Irish Cockneys" is a reference to the steerage passengers in Titanic (1997 film)|Titanic. Pearl Harbor.
* Brian Perkins commentary on Hitler and Elizabeths wedding is a parody of Richard Dimblebys hushed radio commentaries of royal events.
* Eva Braun is shown listening to the end of an episode of The Archers, even though it did not start until six years after the war ended.
* Jim Jim Charoo takes his name from a song Dick van Dyke sings in Mary Poppins (he also lives on "Ye Olde Dick Van Dyke Street")

==Cast==

===Historical characters===
* Christian Slater &ndash; Winston Churchill Princess Elizabeth
* Miranda Richardson &ndash; Eva Braun
* Antony Sher &ndash; Adolf Hitler
* Harry Enfield &ndash; King George VI
* Jessica Oyelowo &ndash; Princess Margaret
* Henry Goodman &ndash; Franklin D. Roosevelt
* Jon Culshaw &ndash; Tony Blair Denzil Eisenhower David Schneider &ndash; Joseph Goebbels
* Phil Cornwell &ndash; Martin Bormann Steve ODonnell &ndash; Hermann Göring John Fabian &ndash; Victor Sylvester

===Other===
* Rik Mayall &ndash; Baxter
* Bob Mortimer &ndash; Potter
* Vic Reeves &ndash; Bendle
* Ashleigh Caldwell &ndash; Bette
* Sally Phillips &ndash; Waitress
* Steve Pemberton &ndash; Chester
* Hamish McColl &ndash; Captain Davies (present day)
* Leslie Phillips &ndash; Lord Wruff
* Mackenzie Crook &ndash; Jim Jim Charoo
* Brian Perkins &ndash; Radio Presenter
* James Dreyfus &ndash; Raymond Bessone|Mr. Teasy-Weasy

==Reception==
Phillip French writing in The Observer called the film "a hit and miss affair" 
Peter Bradshaw in The Guardian gave it three stars and said "Its wildly uneven and very broad, but there are some laughs in Peter Richardsons Comic Strip fantasy of Churchills real life as a kickass action hero."  However Nev Peirce on the BBCs website panned the film, saying "Sadly, Peter Richardson suffers the fate of many satirists; in trying to mock bad movies, hes simply made a bad movie"   The film grossed only £148,326 on its opening weekend across 170 screens in the UK 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 