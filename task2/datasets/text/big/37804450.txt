Horror vacui (film)
{{Infobox film
| name           = Horror Vacui
| image          = Horror vacui.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Rosa von Praunheim
| producer       = Rosa von Praunheim
| writer         = 
| screenplay     = Cecil Brown  Marianne Enzensberger  Rosa von Praunheim	 	
	
| story          = 
| based on       =  
| narrator       = 
| starring       = Lotti Huber  Folkert Milster	 Joaquin La Habana  Ingrid van Bergen		
| music          = Marran Gosov	 	
| cinematography = Elfi Mikesch
| editing        = Mike Shephard  Rosa von Praunheim	 	

| studio         = Rosa Von Praunheim Filmproduktion
| distributor    = 
| released       = 5 October 1984
| runtime        = 85 minutes
| country        = West Germany
| language       = 
| budget         = DEM 350,000
| gross          = 
}} German film directed by Rosa von Praunheim. Murray, Images in the Dark, p. 109  Shot in a neo-expressionist style, the film is a satire on cults of any kind. The plot follows Frankie and Hannes, a young gay couple living in Berlin. One is studying art and the other medicine. Their happy life is disrupted when Frankie attends a lecture and quickly becomes involved in a sinister cult operating as a self-help group called “Optimal Optimism”. Madame C, a former Nazi  party member, is the leader of Optimal Optimism. When the cult members discovers that Frankie is gay, he is repeatedly raped by both men and women of the group. Hannes must find a way to rescue him. 

==Notes==
 

== References ==
*Murray, Raymond. Images in the Dark: An Encyclopedia of Gay and Lesbian Film and Video. TLA Publications, 1994, ISBN 1880707012

==External links==
* 

 
 
 
 
 


 