Sorrow and Joy
 
{{Infobox film
| name     = Sorrow and Joy
| image    = Sorrow and Joy.jpg
| caption  = Film poster
| director = Nils Malmros
| producer = 
| writer   = 
| starring = Jakob Cedergren   Helle Fagralid
| cinematography = 
| music = 
| country = Denmark
| language = Danish
| runtime = 107 minutes
| released =  
}}
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.   

==Cast==
*Jakob Cedergren - Johannes
*Helle Fagralid - Signe
*Ida Dwinger - Else
*Kristian Halken - Laurits
*Nicolas Bro - Birkemose
*Helle Hertz - Johannes mother
*Niels Weyde - Johannes father
*Søren Pilmark - Advokat

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 