Spain Again
{{Infobox film
| name           = Spain Again
| image          = 
| caption        = 
| director       = Jaime Camino
| producer       = Jaime Fernández-Cid
| writer         = Román Gubern Jaime Camino Alvah Bessie
| starring       = Manuela Vargas
| music          = 
| cinematography = Luis Cuadrado
| editing        = Teresa Alcocer
| distributor    = 
| released       = 3 February, 1969
| runtime        = 108 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 41st Academy Awards, but was not accepted as a nominee. 

==Cast==
* Manuela Vargas - María Mark Stevens - Dr. David Foster
* Marianne Koch - Kathy Foster
* Enrique Giménez El Cojo - Maestro Miguel (as Enrique el cojo)
* Luis Serret - Manuel Oliver
* Luis Ciges - Padre Jacinto (as Luis Cijes)
* Joaquín Pujol - Hijo de Manuel
* Alberto Berco - Dr. Gavotty Alberto Puig - Dr. Tomás (as Alberto Puig Palau)
* Flor de Bethania Abreu - Teresa (as Flor de Bethania A.T.C.)
* Manuel Muñiz - Hombre Extraño (as Pajarito)

==See also==
* List of submissions to the 41st Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 