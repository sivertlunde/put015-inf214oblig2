Jolly Life
{{Infobox film
| name           = Jolly Life
| image          = JollyLifeTheatricalPoster.jpg
| alt            =
| caption        = Jolly Life Theatrical Poster
| director       = Yılmaz Erdoğan
| producer       = Necati Akpınar
| writer         = Yılmaz Erdoğan
| starring       =   
| music          =   
| cinematography = Uğur İçbak
| editing        =   
| studio         = BKM Film
| distributor    = Cine Film
| released       =  
| runtime        = 100 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
}}
Jolly Life ( ) is a 2009  Turkish comedy film, written and directed by Yılmaz Erdoğan, about a working class Turkish man who accepts a job as a Mall Santa. The film, which went on nationwide general release across Turkey on  , was screened in competition at the 29th International Istanbul Film Festival and was nominated in several categories at the 3rd Yeşilçam Awards. Writer, director and star Erdoğan says that the main character is between the worlds of the Westernized upper class Turks and the working class Turks in the east of the country. 

== See also ==
* 2009 in film
* Turkish films of 2009

==References==
  

==External links==
*  
*  

 
 
 
 
 
 

 
 