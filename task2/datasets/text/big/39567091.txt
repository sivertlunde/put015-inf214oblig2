Tarmina
{{Infobox film
| name           = Tarmina
| image          = Tarmina ad.jpg
| alt            = 
| caption        = Advertorial material
| director       = Lilik Sudjio
| producer       = 
| writer         = Astaman
| starring       = {{plainlist|
* Fifi Young
*A. Hadi
*Astaman
*Endang Kusdiningsih
}}
| music          = 
| cinematography = 
| editing        = 
| studio         =Persari
| distributor    =
| released       =   
| runtime        = 
| country        = Indonesia
| language       = Indonesian
| budget         = 
| gross          = 
}} Best Film, Best Director Best Leading Best Leading Supporting Actress for Endang Kusdiningsih.

==Plot==
After her husband Hadi loses all of his money, Tarmina divorces him and abandons their family, including a young daughter. She quickly remarries, taking a rich tycoon as her second husband, but when he has an accident when Hadi is nearby she accuses her former husband of the deed. Ultimately Tarminas second husband leaves her, despising her cruelty. When Hadi is released from jail and returns to their daughter, Tarmina wants to ask him to take her back. However, she realises that she has ruined her own life and commits suicide, throwing herself into a river. {{cite web
  | title = Tarmina
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-t007-54-932169_tarmina
  | work = Filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 18 April 2013
  | archiveurl = http://www.webcitation.org/6FyNSTHh9
  | archivedate = 18 April 2013
  }} 

==Production==
Tarmina was written by Astaman, a former stage star and active film actor. It was the directorial debut of Astamans son, Lilik Sudjio.  The black-and-white film was produced by Persari, a film studio owned by producer Djamaluddin Malik. {{cite web
  | title = Kredit Tarmina |trans_title=Credits for Tarmina
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-t007-54-932169_tarmina/credit
  | work = Filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 4 June 2013
  | archiveurl =http://www.webcitation.org/6H6uyVgiM
  | archivedate = 4 June 2013
  }} 

The film starred Fifi Young, A Hadi, Djauhari Effendi, Astaman, and Endang Kusdiningsih. 
==Release and reception== Best Film, Best Director Best Leading Best Leading Supporting Actress for Endang Kusdiningsih.    These awards were mostly shared with Usmar Ismails Lewat Djam Malam, produced by Persari in collaboration with its competitor PERFINI. Critics, however, disagreed with the selection. They wrote that Lewat Djam Malam was easily the stronger of the two works and suggested that Djamaluddin Malik had influenced the jurys decision. He had previously influenced a contest for favourite actress in 1954, ensuring that an actress from his company was chosen. {{cite book
  | title = Profil Dunia Film Indonesia
  | trans_title=Profile of Indonesian Cinema
  | language = Indonesian
  | last = Said
  | first = Salim
  | publisher = Grafiti Pers
  | location = Jakarta
  | year = 1982
  | oclc = 9507803
  | page = 43
  }} 

The Indonesian film scholar Ekky Imanjaya positions the controversy amidst a dialogue between idealism and commercialism in the domestic film industry. He writes that Ismail had been representative of the idealistic filmmaker, who viewed cinema as a form of art. Meanwhile, he considers Djamaluddin Malik to have been firmly commercially oriented. He notes that, despite these conflicting viewpoints, the two directors remained friendly and were members of the same political party, the Nahdlatul Ulama, and later collaborated on an Islamic-themed film entitled Tauhid.   

==References==
 

 
 
 
 