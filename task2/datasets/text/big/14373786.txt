Portrait of Chieko
{{Infobox film
| name           = Portrait of Chieko
| image          = 
| caption        = 
| director       = Noboru Nakamura
| producer       = 
| writer         = Noboru Nakamura Kōtarō Takamura
| starring       = Shima Iwashita
| music          = 
| cinematography = Hiroshi Takemura
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 125 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
}}

  is a 1967 Japanese drama film directed by Noboru Nakamura and based on a poem by the Japanese poet and sculptor Kōtarō Takamura. It was nominated for the Academy Award for Best Foreign Language Film.   

==Cast==
* Shima Iwashita as Chieko Takamura
* Tetsurō Tanba as Kotaro Takamura
* Jin Nakayama as Toshu Takamura
* Yōko Minamida as Kazuko
* Eiji Okada as Tsubaki
* Mikijiro Hira as Ishii
* Kaori Shima
* Takamaru Sasaki as Koun Takamura
* Tetsuo Ishidate as Taro
* Kinuko Obata as Osato
* Yoshi Katō as Sokichi Naganuma
* Poems read by Hiroshi Akutagawa

==See also==
* List of submissions to the 40th Academy Awards for Best Foreign Language Film
* List of Japanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 