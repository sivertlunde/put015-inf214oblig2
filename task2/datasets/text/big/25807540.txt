The Shooting of Dan McGrew (1924 film)
{{Infobox film
| name           = The Shooting of Dan McGrew
| image          = The Shooting of Dan McGrew poster.jpg
| image size     =
| caption        = Theatrical poster
| director       = Clarence G. Badger
| producer       =
| writer         = Robert W. Service (poem) Winifred Dunn
| narrator       =
| starring       = Barbara La Marr Lew Cody Mae Busch
| cinematography = Rudolph J. Bergquist
| editing        =
| distributor    = Metro Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States Silent English English intertitles
| budget         =
}} 1924 silent silent drama 1907 poem of the same name written by Robert W. Service. The film is preserved.  

==Plot==
A dancer known as Lou Lorraine feels her life is going nowhere. She is married to Jim, who is working as a pianist at the same cabaret in a small village Lou is working at. One day, a man nicknamed "Dangerous Dan" McGrew promises to make a big star on Broadway out of her, after which she immediately leaves with him. She swears on staying faithful to her husband, promising to earn money to have Jim and her son sent to New York. Jim, however, does not trust Dan and follows them to New York, where everything goes out of hand. 

==Cast==
*Barbara La Marr as The lady known as Lou Lorraine
*Lew Cody as Dangerous Dan McGrew
*Mae Busch as Flo Dupont
*Percy Marmont as Jim, Lous husband Max Asher as Isadore Burke Fred Warren as The Ragtime Kid
*George Siegmann as Jake Hubbel
*Nelson McDowell as Sea Captain
*Philippe De Lacy as Little Jim

==See also==
*List of films based on poems

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 