Daughters of Pleasure
{{Infobox film
| name           = Daughters of Pleasure
| image          =
| imagesize      =
| caption        =
| director       = William Beaudine
| producer       = B.F. Zeidman
| writer         = Eve Unsell Harvey F. Thew (titles)
| based on       =  
| starring       = Marie Prevost Monte Blue Clara Bow
| cinematography = Charles Van Enger
| editing        = Edward C McDermott
| distributor    = Principal Distributors
| released       =  
}}
 silent romantic comedy film directed by William Beaudine and starring Marie Prevost and Monte Blue. Based on a story by Caleb Proctor, the film features an early appearance by Clara Bow who plays a supporting role. 

An incomplete print of the film is housed at the Library of Congress with two of the six reels missing.  

==Cast==
* Marie Prevost - Marjory Hadley
* Monte Blue - Kent Merrill
* Clara Bow - Lila Millas
* Edythe Chapman - Mrs. Hadley
* Wilfred Lucas - Mark Hadley
* Edwin B. Tilton - Uncredited role

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 