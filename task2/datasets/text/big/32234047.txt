Africa United (2010 film)
{{Infobox film
| name           = Africa United
| image          = Africa United.jpg
| border         = yes
| caption        = Cinema release poster Deborah Debs Gardner-Paterson
| producer       = Mark Blaney  Jackie Sheppard   Eric Kabera
| writer         = Rhidian Brook
| starring       = Emmanuel Jal Eriya Ndayambaje Roger Nsengiyumva Sanyu Joanita Kintu Sherrie Silver Yves Dusenge
| music          = Bernie Gardner
| cinematography = Sean Bobbitt
| editing        = Victoria Boydell
| studio         = Pathé Productions Footprint Films Link Media Productions Out of Africa Entertainment BBC Films
| distributor    = Warner Bros. Pathé
| released       =  
| runtime        = 88&nbsp;minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British comedy-drama adventure film Deborah Debs South African World Cup.

==Synopsis==
The story begins in Rwanda where a football fanatic Dudu Kayenzi is teaching a group of children how to make an "organic football" using a UN condom, plastic bag and string. Meanwhile, his best friend and football prodigy Fabrice longs to pursue his ambition of becoming an association football legend and is seen breaking the local record for the most continuous football kick-ups. When Fabrice is offered the chance to audition for the opening ceremony of the 2010 Football World Cup in South Africa, he fabricates a plan to sneak past his strict, wealthy parents with Dudu and Dudus sister Beatrice. Beatrice wishes to become a doctor and find a cure for AIDS. Fabrice tries to express his love of football to his mother, but she simply replies "Africa doesnt need dreams, it needs to wake up".

The following day Fabrice plucks up the courage to sneak out of home early and, with Dudu and Beatrice, masquerade as part of a womans family in order to get on a bus to Kigali. However, they miss their stop whilst having to hide during a ticket inspection, ending up in the Democratic Republic of Congo.
 panther in the toilet.

Dudu, Beatrice, Fabrice and George arrive at the shore of Lake Tanganyika, where they buy a boat to get to  Burundi on the other side. It is here when Dudu begins to narrate a fictional story of a kid and his sister who are commissioned by God to design a football that never busts (much like his makeshift ones). They will require rubber from a tree, plastic from "Shit Mountain" and string from a lake guarded by a fierce fish.

Fabrices mother texts and calls him on his mobile (cell) phone, but he finally has enough and throws the phone into the middle of the lake. Eventually they arrive on the other side of the lake at a posh beach resort, where Celeste, a sex slave, is working for the white owner by selling beverages. She catches the four children and insists they go back, but George flashes his bag of money for bribery and so the owner lets them stay and play in the pool and be waited on. Fabrice has time to play with Dudus "organic footballs". As George falls asleep, the owner steals his bag of money and throws them out of the villa; however, they soon retrieve the bag and the money and Celeste joins them.

Using Dudus tactical skills, they find various and bizarre methods of pursuing their journey to South Africa, including an ox-drawn cart. Celeste reveals she ran away from her tribal village to avoid an arranged marriage, and Fabrice sees George throw away his gun, which he used to shoot his fellow soldiers earlier on.

The team runs out of money, so Dudu insists they can earn some by giving blood at a local medical center. Everyone passes the blood test except for Dudu, who already knows he is HIV-positive, although he does not reveal this to the others. After crossing the border into Zimbabwe, Dudu exchanges the Zambian kwacha for Zimbabwean dollars, but Celeste explains the currency is defunct and that he has been scammed. He tries to collect the money after it gets thrown into the water by an angry Fabrice, and he is helpless as it spills over the Victoria Falls.

During the night, in which the gang sleep outside a wildlife reserve and Dudu and Fabrice compare footballers to animals, Dudu develops TB and in the morning he is rushed to a local mission hospital. There is a school attached to the hospital, and a school teacher (Leleti Khumalo) notices Beatrices intelligence and offers her a place at the school to study for free. The doctor soon explains that, although the medical team have been able to stabilise Dudus tuberculosis, he has a low CD4 T-cell count (i.e. he is HIV-positive) and needs medication that is out of stock and wont be available for another three days. It is only now the others realise Dudu is HIV positive. However, Dudu decides to persevere on the journey and to continues with Fabrice, George and Celeste towards the stadium: Beatrice decides to stay behind at the school to have an education and so try to fulfill her dream of becoming a doctor and maybe find a cure for AIDS for Dudus sake.

The team arrive at the border with South Africa, where an official insists that they are nothing but refugees. The security guards take Dudus ball but again the team works together to retrieve it. Fabrices football skills impress the guards, who then agree to rush the children to Soccer City in Johannesburg for the Cup. Dudu falls ill again and is rushed to the medical facility at the stadium, where he completes his fictional story by saying Fabrice carries the ball God gave to the people of Africa. Except for Dudu, the team carry the ball off into the now roaring crowd.

At the end, Dudu is seen walking off into a light at the end of the Soccer city stadium tunnel, carrying his briefcase & custom-made ball suggesting that he has died of HIV–AIDS. The song in the end credits is a rendition of Bob Marleys One Love/People Get Ready.

==Reception==

The Times described the film as a "small budget film with a big heart". "When did you last watch a kids film about Africa that left you laughing, punching the air and weeping discreetly into your popcorn?" asked Kate Muir, giving the film 4 stars.  The Financial Times and Metro (British newspaper)|Metro gave it 4 stars, with the former describing it as a "terrific road movie" and the latter, "the feelgood film of the year".  The Daily Express also described it as "the feelgood film of the year" noting that, "there is something infectious about the optimism".
 Time Out Empire Online all gave the film three stars out of five.          The Guardians Xan Brooks found it "irksome and endearing by turns",  while Dave Calhoun in Time Out thought it was "well-meaning but scrappy" and likened it to "an inferior, kiddie spin on the exotic high jinks and low lives of Slumdog Millionaire". 

==References==
*  
*  
*  
*  
* Kate Muir, The Times, Africa United Review, 22 October 2010

* Antonia Quirke, The Financial Times, Africa United Review, 23 October 2010

* Simon Edge, Daily Express, Africa United article, 20 October 2010

 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 