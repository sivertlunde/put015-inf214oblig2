Imani (film)
{{Infobox film
| name           = Imani
| image          = 
| alt            =  
| caption        = 
| director       = Caroline Kamya
| producer       = iVAD International Ltd Cinepost Studios Filmpool Nord Liudbang Rode Orm Film
| writer         = 
| screenplay     = Agnes Nasozi Kamya
| story          = 
| starring       = Ocen Stephen Rehema Nanfuka Philip Buyi Roy
| music          = Ragnar Grippe
| cinematography = Andrew Mark Coppin
| editing        = Carolina Kamya
| studio         = 
| distributor    = 
| released       =   
| runtime        = 82
| country        = Sweden Uganda
| language       = 
| budget         = 
| gross          = 
}}

Imani is a 2010 film.

== Synopsis ==
The film takes place during a normal day in the Ugandan capital of Kampala in the province of Gulu. For three people however, this day will not be an ordinary one. Mary, who works in the house of an upper class lady, faces serious difficulties when she has no choice but to pay a bribe to save her sister from the clutches of the police. 12-year-old Olweny, a former child soldier, leaves the rehabilitation center to return to his parent’s village, destroyed by the war. And break-dancer Armstrong has to put together a performance due to take place that same evening.

Imanis UK television premiere was on The Africa Channel International (Sky 209 & Virgin Media 828) on Wednesday 20 February 2013. 

== References ==
 
 
*  

 
 
 


 
 