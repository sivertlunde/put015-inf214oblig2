Jagirdar (film)
{{Infobox film
| name           = Jagirdar
| image          = 
| image_size     = 
| caption        = 
| director       = Mehboob Khan
| producer       = Sagar Movietone
| writer         = Babubhai Mehta 
| narrator       =  Surendra Motilal Yakub
| Anil Biswas
| cinematography = Keki Mistry Faredoon Irani
| editing        = 
| distributor    =
| studio         = Sagar Movietone
| released       = 1937
| runtime        = 166 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1937 Urdu/Hindi romantic melodrama film directed by Mehboob Khan,    and starring Bibbo, Motilal (actor)|Motilal, Surendra (actor)|Surendra, Yakub (actor)|Yakub, Pande, Sankatha Prasad, Pesi Patel and Maya Banerjee.   
 Anil Biswas replaced Ashok Ghosh as an acclaimed composer continuously producing a range of successful music.    The story was written by Babubhai Mehta with dialogues credited to Zia Sarhadi. The lyricists were Zia Sarhadi and Pandit Indra Sharma. 

==Plot==
Neela (Bibbo) and Jagirdar (Surendra) marry each other without anyone’s knowledge. However, Jagirdar goes missing at sea presumed dead. Neela realises she’s pregnant and when the child is born he’s called illegitimate. Shripat (Pande), a poor farmer marries her and helps take care of her son Ramesh. On his return Jagirdar is angry to find Neela married to Shripat and a fight ensues. When Shripat is killed by Banwarilal everyone assumes Jagirdar is the murderer. Soon with Ramesh’s help they fight the villains with the truth revealed. Ramesh finally accepts Jagirdar as his father. 

==Cast== Bibbo as Neela Surendra as Jagirdar Motilal as Ramesh Yakub as Narayanlal
*Pande as Shripat
*Zia Sarhadi
*Bhudo Advani
*Maya Banerjee
*Sankatha Prasad
*Ram Marathe

==Production== Hitchcock films.    The film starred Motilal as the newcomer Surendras son. Motilals "casual style of acting" was appreciated by critics.    The directors of photography were Keki Mistry and Faredoon Irani.        

==Songs==
Songlist   
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Pujari More Mandir Mein Aao 
| Surendra, Bibbo
|-
| 2
| Nadi Kinare Baith Ke Ao 
| Surendra, Maya Banerjee
|-
| 3
| Agar Deni Thi Humko Hooro Jannat Toh Yahan Dete
| Surendra
|-
| 4
| Auron Ke Kyun Pag Padta Hai
| Rajkumari
|-
| 5
| Baanke Bihaari Bhool Na Jaana
| Motilal, Maya Banerjee
|-
| 6
| Jinke Nainon Mein Rehte Hain Taare
| Surendra
|-
| 7
| Jeevan Yun Beet Na Jaaye
| Bibbo
|-
| 8
| Kyun Chale Gaye Us Paar
| Rajkumari
|-
| 9
| Tera Hai Sansar
| Motilal
|-
| 10
| Wohi Purane Khel Jagat Ke
| Anil Biswas
|-
|}

==References==
 

==External links==
* 

 

 
 
 