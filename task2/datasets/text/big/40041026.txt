Journey to Portugal (film)
 
{{Infobox film
| name           = Journey to Portugal
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = Viagem a Portugal
| director       = Sergio Tréfaut
| producer       = Sergio Tréfaut
| writer         = Sergio Tréfaut
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Maria de Medeiros, Isabel Ruth, Makena Diop
| music          = Gyorgy Ligeti
| cinematography = Edgar Moura
| editing        = Sergio Trefaut, Goncalo Soares, Pedro Marques, Maria Gaivao
| studio         = 
| distributor    = 
| released       = June 16, 2011
| runtime        = 75 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
| gross          = 
}}
Journey to Portugal ( ) is a 2011 Portuguese film directed and written by Sergio Tréfaut, based on a true story. It was Tréfauts first feature-length fiction film and stars Maria de Medeiros, Isabel Ruth, and Makena Diop.

==Synopsis==
Maria, a Ukrainian doctor, comes to Portugal to spend a year with Greco, her husband who is also a doctor. Upon arrival at Faro airport she is the only person from Kiev approached by agents of Immigration and Customs that lead her to a room of interrogation, without any explanations. All this occurs because the authorities suspect that something illegal should be behind her trip, since she is from Eastern Europe and her husband is Senegalese.

==Awards and nominations==
Caminhos do Cinema Português 2011 (Portugal)
{| class="wikitable"
! Category !! Result
|-
| Best film ||  
|-
| Best supporting actress — Isabel Ruth ||  
|}
 Golden Globes 2012 (Portugal)
{| class="wikitable"
! Category !! Result
|-
| Best film ||  
|-
| Best actress — Maria de Medeiros ||  
|}

==External links==
*  
*  

 
 
 
 

 