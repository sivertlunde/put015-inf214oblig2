The New York Idea (1920 film)
{{infobox film
| name           = The New York Idea
| image          = The New York Idea (1920) - Brady & Sherman.jpg
| image_size     = 
| caption        = Film still with Alice Brady and Lowell Sherman
| director       = Herbert Blache Marcel Del Sano (asst. director)
| producer       = Realart
| writer         = Langdon Mitchell (play) Mary Murillo (scenario)
| based on        =  
| cinematography = Jacques Bizeul
| editing        =
| distributor    = Realart Pictures Corporation
| released       = November 27, 1920
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}} 1920 American silent comedy film directed by Herbert Blache and starring Alice Brady. The film was produced and distributed by Realart Pictures, an Adolph Zukor affiliate of his bigger Paramount Pictures.
 play by Langdon Mitchell that starred Mrs. Fiske and George Arliss. Prints of the film exist at the International House of Photography, George Eastman House and the British Film Institute, London.   

==Cast==
*Alice Brady - Cynthia Karslake
*Lowell Sherman - John Karslake
*Hedda Hopper - Vida Phillimore
*George Howell - Judge Philip Phillimore
*Lionel Pape - Sir Wilfrid Darby
*Margaret Linden - Caroline Dwight
*Edwards Davis - Bishop Matthew Phillimore (billed as Edward Davis)
*Harry Hocky - Tim Fiddler
*Nina Herbert - Mrs. Fiddler
*Emily Fitzroy - Grace Phillimore Julia Hurley - Mrs. Phillimore
*Marie Burke - Miss Heneage
*Robert Vivian - Brooks
*Edgar Norton - Thomas
*George Stevens - Butler

==References==
 

==External links==
 
* 
* 
*Stills:   (with Lionel Pape, Alice Brady, Hedda Hopper, and Lowell Sherman), and   (with Alice Brady and Lionel Pape)
* 

 
 
 
 
 
 
 
 


 