Até que a Sorte nos Separe 2
{{Infobox film
|  name           = Até que a Sorte nos Separe 2 |
  image          = Até que a Sorte nos Separe 2.jpg|
  caption        = Theatrical release poster|
  director       = Roberto Santucci|
  writer         = Paulo Cursino Chico Soares|
  starring       = Leandro Hassum Camila Morgado Jerry Lewis Anderson Silva|
  producer       = |
  music          = |
  cinematography =  Juarez Pavelak|
  studio         =  Globo Filmes  Gullane|
  editing        =  Leo Clark|
  distributor    = Paris Filmes Downtown Filmes|
  released       =  |
  runtime        =  |
  country       = Brazil |
  language       = Portuguese |
  budget         = BRL|R$ 7,000,000 |
  gross          = 
}}

Até que a Sorte nos Separe 2 (English: Till Luck Do Us Part 2)  is a 2013 Brazilian comedy film, directed by Roberto Santucci and written by Paulo Cursino and Chico Soares. It is a sequel of the 2012 film Até que a Sorte nos Separe. Leandro Hassum who had played the protagonist in the previous film, back to reprise his role, while Danielle Winits who had played Jane in the previous film, was replaced by Camila Morgado. The film feature a special participation of American actor Jerry Lewis.

==Plot==
After losing everything, Tino (Leandro Hassum) is more cash-strapped than ever, until he receives the news of the death of Olavo, the uncle of his wife Jane (Camila Morgado) and discover that he left an  inheritance of a hundred million Brazilian real|reais, and then make a trip to Las Vegas. One night after he discovers that have lost everything and that owes money to the Mexican mafia.

==Cast==
* Leandro Hassum as Tino
* Camila Morgado as Jane
* Jerry Lewis as Bellboy
* Rita Elmôr as Laura
* Anderson Silva as Andrew Silver
* Júlia Dalávia as Teté
* Henry Fiuka as Juninho

==Release==
===Box Office===
In its first weekend in theaters, the film took almost 550,000 people to theaters, the largest opening of a Brazilian production in 2013. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 