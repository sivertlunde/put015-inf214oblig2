Trimmed
 Trim}}
 
{{Infobox film
| name           = Trimmed
| image          = Trimmed (1922) - Ad 1.jpg
| caption        = Sample ad for film
| director       = Harry A. Pollard
| producer       = 
| writer         = Wallace Clifton Habsburg Liebe Arthur F. Statter
| starring       = Hoot Gibson
| cinematography = Sol Polito
| editing        = 
| distributor    = 
| released       =  
| runtime        = 50 minutes
| country        = United States  Silent (English intertitles)
| budget         = 
}}
 Western film directed by Harry A. Pollard and featuring Hoot Gibson. Its survival status is classified as unknown,  which suggests that it is a lost film. 

==Cast==
* Hoot Gibson as Dale Garland
* Patsy Ruth Miller as Alice Millard
* Alfred Hollingsworth as John Millard
* Fred Kohler as Young Bill Young
* Otto Hoffman as Nebo Slayter
* Dick La Reno as Judge William Dandridge
* Hugh Sutherland as Lem Fyfter (credited as R. Hugh Sutherland)

==See also==
* Hoot Gibson filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 