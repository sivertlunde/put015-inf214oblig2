Web of Suspicion
{{Infobox film
| name           = Web of Suspicion
| image          = "Web_of_Suspicion"_(1959).jpg
| image_size     =
| caption        = British theatrical poster
| director       = Max Varnel
| producer       = Edward J. Danziger Harry Lee Danziger
| writer         = Brian Clemens Eldon Howard
| narrator       =
| starring       = Philip Friend Susan Beaumont
| music          =  James Wilson
| editing        =
| studio         = Danziger Productions  (UK)
| distributor    = 
| released       = May 1959 (UK)
| runtime        = 70 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
Web of Suspicion is a 1959 British crime film from the Danzigers, directed by Max Varnel, and starring Philip Friend and Susan Beaumont. 

==Plot==
Schoolteacher Bradley Wells is wrongly accused of murdering a girl pupil, and is nearly lynched by angry townspeople. But with the help of his art teacher girlfriend Janet he discovers the real murderer, and works with Janet to clear his name and save the school from another tragedy.

==Cast==
*Bradley Wells - 	Philip Friend
*Janet Shenley - 	Susan Beaumont
*Eric Turner - 	 John Martin Peter Sinclair
*Inspector Clark - 	Robert Raglan
*Watson - 	 Peter Elliott Ian Fleming
*Holt - 	John Brooking
==References==
 
==External links==
*  at IMDB
 