Kaithappoo
{{Infobox film
| name = Kaithappoo
| image =
| caption =
| director = Raghuraman
| producer =
| writer = George Onakkoor
| screenplay = George Onakkoor Madhu KPAC Lalitha Manavalan Joseph Pattom Sadan Shyam
| cinematography =
| editing = G Venkittaraman
| studio = Uma & Sunitha Combines
| distributor = Uma & Sunitha Combines
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed Raghuraman. The film stars Madhu (actor)|Madhu, KPAC Lalitha, Manavalan Joseph and Pattom Sadan in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
   Madhu 
*KPAC Lalitha 
*Manavalan Joseph 
*Pattom Sadan  Raghavan 
*Prathapachandran 
*Adoor Bhavani 
*Alummoodan 
*Anandavally 
*Aranmula Ponnamma 
*Aryad Gopalakrishnan
*Baby Sreekala Baby Sumathi 
*KPAC Sunny  Khadeeja 
*Kuthiravattam Pappu 
*Master Natarajan Meena 
*Rani Chandra 
*S. P. Pillai  Sudheer 
*Veeran 
 

==Soundtrack== Shyam and lyrics was written by Bichu Thirumala. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kaatte Vaa Kaatte Vaa || S Janaki, P Susheela, Chorus || Bichu Thirumala || 
|- 
| 2 || Kaatte Vaa Kaatte Vaa   || P Susheela || Bichu Thirumala || 
|- 
| 3 || Malayalame || P Susheela || Bichu Thirumala || 
|- 
| 4 || Pularikalum Poomanavum || P Susheela || Bichu Thirumala || 
|- 
| 5 || Sarigama Padunna || S Janaki, P Susheela || Bichu Thirumala || 
|- 
| 6 || Shaanthathayengum || K. J. Yesudas || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 


 