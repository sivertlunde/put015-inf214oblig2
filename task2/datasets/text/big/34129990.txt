Bella Donna (1915 film)
{{infobox film
| name           = Bella Donna
| image          = Belladonna-newspaperad-1915.jpg
| imagesize      =
| caption        = Contemporary newspaper advertisement. Hugh Ford
| producer       = Adolph Zukor Charles Frohman
| based on       =    
| writer         =  
| starring       = Pauline Frederick
| cinematography =
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 mins.
| country        = United States Silent English intertitles
}}
 
 silent drama film produced by Famous Players-Lasky and the Charles Frohman Company, starring Pauline Frederick, and based on the play Bella Donna by James Bernard Fagan adapted from the novel of the same name by Robert Smythe Hichens. 
 filmed in 1923, starring Pola Negri.  

==Cast==
*Pauline Frederick - Bella Donna
*Thomas Holding - Nigel Armine
*Julian LEstrange - Baroudi
*Eugene Ormonde - Doctor Isaacson
*George Majeroni - Ibraham
*Edmund Shalet - Hamza
*Helen Sinnott - The Maid

==Preservation status==
The 1915 version is now considered a lost film. 

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
*  at SilentEra

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 