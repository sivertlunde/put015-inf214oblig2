Helpless (2012 film)
{{Infobox film name           = Helpless image          = File:Helpless_(2012)_poster.jpg
| film name =   hanja          =   rr             = Hwacha mr             = Hwach‘a }} director       = Byun Young-joo producer       = Shin Hye-eun   Oh Ki-min writer         = Byun Young-joo  based on       =   starring       = Kim Min-hee   Lee Sun-kyun   Jo Sung-ha music          = Kim Hong-jib cinematography = Kim Dong-young  editing        = Park Gok-ji studio = Filament Pictures distributor    = CJ E&M released       =   runtime        = 117 minutes country        = South Korea language       = Korean budget         =  gross          =      
}}
Helpless ( ) is a 2012 South Korean psychological mystery/thriller written and directed by Byun Young-joo based on the bestselling novel   by Japanese writer Miyabe Miyuki.   

A man searches for his fiancée who vanished without a trace, only to discover dark, shocking truths about her and that she wasn’t the person he thought he knew.   

==Plot==
South Korea, 2009. A few days before their wedding, veterinarian Jang Mun-ho (Lee Sun-kyun) and his fiancee Kang Seon-yeong (Kim Min-hee) pull over for coffee at a motorway rest stop on the way to visiting his parents in Andong, southeast of Seoul. However, when Mun-ho returns to the car, Seon-yeong has disappeared and is not reachable on her mobile phone. All he can find is a hairpin in the rest stops toilet. From the mess at her flat in Seoul, it looks as if there has been a break-in. Mystified, Mun-ho then learns from a banker friend, Dong-woo (Kim Min-jae), that Seon-yeong had earlier applied for a bank account but had been turned down when it was discovered she had a history of personal bankruptcy dating back to 2007. Investigating her debt history, Mun-ho finds she had been using someone elses name and identity. He persuades his cousin, Kim Jong-geun (Jo Sung-ha), a former police detective sacked for taking bribes, to help find her. Examining her flat, Jong-geun finds she left no fingerprints, had no friends and claimed her mother died two years ago. It then turns out that the woman (Cha Soo-yeon) whose identity she assumed two years ago had a debt history and has since vanished. Visiting Seon-yeongs hometown, Jong-geun hears rumors she killed her mother for her insurance money. Seon-yeongs real name is, in fact, Cha Gyeong-seon, and Jong-geun and Mun-ho realize she is now looking to take on another womans identity. They think they know her possible target. 

==Cast==
*Kim Min-hee ... Kang Seon-yeong/Cha Gyeong-seon   
*Lee Sun-kyun ... Jang Mun-ho  
*Jo Sung-ha ... Kim Jong-geun
*Kim Byul ... Han-na
*Cha Soo-yeon ... the real Kang Seon-yeong
*Choi Deok-mun ... Police detective Ha Seong-shik
*Lee Hee-joon ... Noh Seung-ju, Gyeong-seons ex-husband
*Kim Min-jae ... Lee Dong-woo
*Choi Il-hwa ... Mun-hos father
*Bae Min-hee ... Client from veterinary clinic
*Im Ji-kyu ... Stalker

==Box office==
Helpless debuted at No. 1 on the weekend box office, only three days after its premiere on March 8, attracting 607,463 moviegoers and grossing   between March 9 and 11.  It topped the chart for two consecutive weeks, selling 561,666 tickets between March 16 and 18, according to KOBIS (Korean Box Office Information System).  It was the twelfth most-watched Korean film of 2012, with 2,436,400 tickets sold.  

==Awards and nominations== 2012 Baeksang Arts Awards
*Best Director - Byun Young-joo  
*Nomination - Best Film
*Nomination - Best Actress - Kim Min-hee

2012 Buil Film Awards
*Best Actress - Kim Min-hee
*Nomination - Best Film
*Nomination - Best Director - Byun Young-joo
*Nomination - Best Supporting Actor - Jo Sung-ha
 2012 Blue Dragon Film Awards
*Nomination - Best Actress - Kim Min-hee
*Nomination - Best Supporting Actor - Jo Sung-ha

2012 Korean Culture and Entertainment Awards
*Excellence Award, Actor in a Film - Jo Sung-ha

2012 Women in Film Korea Awards
*Woman Filmmaker of the Year - Byun Young-joo  

==References==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 