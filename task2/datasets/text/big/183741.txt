To Each His Own (film)
{{Infobox film 
| name           = To Each His Own
| image          = ToeachhisownPOSTER.jpg
| caption        = Theatrical release poster
| director       = Mitchell Leisen	
| producer       = Charles Brackett		
| writer         = Charles Brackett (story)   Jacques Théry   Dodie Smith (uncredited) Mary Anderson John Lund
| music          =Victor Young	
| cinematography = Daniel L. Fapp 
| editing        = Alma Macrorie
| distributor    = Paramount Pictures  
| released       =  
| runtime        = 122 minutes
| language       = English
| country        = United States
}}
 Mary Anderson, John Lund in his first on-screen appearance, where he played dual roles as father and son. The screenplay was written by Charles Brackett and Jacques Théry. A young woman bears a child out of wedlock and has to give him up.
 Best Writing, Original Story.

==Plot==
In World War II London, fire wardens Josephine "Jody" Norris (Olivia de Havilland) and Lord Desham (Roland Culver) keep a lonely vigil. When Jody saves Deshams life, they become better acquainted. With a bit of coaxing, the ageing spinster tells the story of her life, leading to a Flashback (narrative)|flashback.

Jody is the belle of her small American hometown of Piersen Falls. Both Alex Piersen (Phillip Terry) and traveling salesman Mac Tilton (Bill Goodwin) propose to her. However, she turns them both down. A disappointed Alex marries Corinne (Mary Anderson). When handsome US Army Air Service fighter pilot Captain Bart Cosgrove (John Lund) flies in to promote a World War I bond drive, he and Jody quickly fall in love, though they have only one night together.

A pregnant Jody is advised (out of town) that her life is in danger and she needs an operation. She agrees, though she would lose her unborn child. However, when she learns that Bart has been killed in action, she changes her mind. She secretly gives birth to their son in 1919. She tries to arrange it so that she can "adopt" the boy without scandal by having him left on the doorstep of a family with too many children already, but the scheme backfires. Corrine loses her own newborn that same day, but is consoled by Jodys. Jody has to love her son, named Gregory or "Griggsy", from afar. 

Her father dies, forcing her to sell the family drug store. When Jody asks to become Griggsys nurse, Corrine turns her down; she has suspected all along that Jody is the boys real mother. Knowing that her husband never loved her, Corrine is determined to keep the one person who does.

Jody moves to New York City to work for Mac. She discovers to her surprise that he is a Rum-running|bootlegger, using a cosmetics business as a front. The same day, the place is raided by the police, leaving Mac with nothing but the cosmetics equipment. Jody persuades him to make cold cream; with her drive and determination, she builds up a thriving business, and they become rich. 

In 1924, she forces Corrine to give her Griggsy by threatening to block a desperately needed bank loan for Alexs failing business. After two months, however, the four-year-old (played by Billy Ward) is still so miserably homesick, Jody gives up and sends the boy back.
 WREN fiancée without the customary delay. After some broad hints from Desham, Lieutenant Pierson finally realizes why Jody has been so helpful and asks his mother (by that title) for a dance.

==Home media==
*  
*   

==Influences==
The film was remade in India as the Hindi hit film Aradhana (1969 film)|Aradhana (1969)   which made Rajesh Khanna a major star.  

==Critical reception==
ALTFG said "To Each His Own is surprisingly direct in its handling of an unwed mother, paralleling Jodys increasing coldness with the detached – but honest – flashbacks that comprise the bulk of the film."  One reviewer at Cinescene said " In To Each His Own, the sufferer is able to learn something from her mistakes and misfortunes, growing past her grief and distress into a kind of wisdom. The picture has style, but also a sincerity of sentiment that gives it distinction."  Another wrote "Forthrightly feminist avant la lettre, the film is conscious of constraints, but committed to its movement forward: less resentful than resourceful, and more stalwart than strident, yet angry and determined nonetheless". 

ClassicFilmGuide deemed it "A marvelous sentimental (and now dated) story ".  JigsawLounge wrote "Bracketts screenplay is a wonder of intricate construction, with pretty much every minor detail of character and plot introduced for a reason which "pays off" much later in the script. On sober reflection, it is a rather tall tale – and more than the usual degree of disbelief-suspension may be required here and there. But this should prove a very simple task for all but the most hard-headed of audiences: To Each His Owns combination of emotional resonance and a lively wit is potent, and enduring."  San.beck said "This maudlin drama explores the loneliness of a woman who is successful in business but has only one relative she rarely sees. The world wars made for some quick marriages and many widows."  NicksFlickPicks gave the film a rating of 3 stars out of 5.  TV Guide said "What might have been a trite soap opera is elevated to the status of superior emotional drama by a wise script, sensitive direction, and an Oscar-winning performance by de Havilland". 

===Awards===
{| class="wikitable"
|-
! Year !! Award !! Recipient !! Result
|- 2nd place
|-
| 1947|| Academy Award for Best Actress in a Leading Role || Olivia de Havilland ||  
|-
| 1947|| Academy Award for Best Writing, Original Story|| Charles Brackett ||  
|}

==References==
 

==Further reading ==
*   Shadoian is a film scholar who wrote the monograph Dreams and Dead Ends: The American Gangster Film (1978, 2003).

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 