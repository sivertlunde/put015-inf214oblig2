Sadhurangam (2011 film)
{{Infobox film
| name = Sathurangam
| image =
| caption =
| director =Karu Pazhaniappan
| producer = S. S. Durairaj
| writer = Karu Pazhaniappan Srikanth Sonia Agarwal Vidyasagar
| cinematography = R. Divakaran
| editing = Suresh Urs
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
}}
 Srikanth and Sonia Agarwal. The films background score was composed by Vidyasagar (music director)|Vidyasagar, and his soundtrack was released in 2004. Filming was completed in 2006 but the film remained unreleased for five years.  The project was revived in September 2011,  and released on 7 October 2011.   Srikanths character in the film is named after Thirupathisamy, the late director of the films Azad (film)|Azad and Narasimha (2001 film)|Narasimha, who was Karu Pazhaniappans close friend.   

==Cast== Srikanth as Thirupathisamy
*Sonia Agarwal as Sandhya
*Ganesh Yadav
*Mahadevan Murali
*Ilavarasu
*Bose Venkat
*Manobala
*Saranya Ponvannan
*Manivannan Sriman
*Vinodhini

==Production==
After the success of Parthiban Kanavu, Karu Pazhaniyappan and Srikanth reunited again for a project called "Sadhurangam". Sonia Agarwal was selected to play the heroine. Srikanths character in the film is named after Thirupathisamy, the late director of the films Azad (film)|Azad and Narasimha (2001 film)|Narasimha, who was Karu Pazhaniappans close friend. 

==Critical reception==
Behindwoods wrote:"Sathurangam is proof of the fact that a good story never gets outdated".  Nowrunning wrote:"A perfect thriller with its heart in the right place". 

==References==
 

==External links==
*  

 

 
 
 
 


 