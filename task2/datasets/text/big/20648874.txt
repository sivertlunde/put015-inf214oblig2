Swing it, magistern!
{{Infobox Film
| name           = Swing it, magistern!
| image          = Swing it, magistern!.jpg
| image_size     = 
| caption        = Swedish DVD-cover.
| director       = Schamyl Bauman
| producer       = 
| writer         = Schamyl Bauman Hasse Ekman
| narrator       = 
| starring       = Adolf Jahr Alice Babs
| music          = Sten Axelsson Eskil Eckert-Lundin Thore Ehrling Kai Gullmar
| cinematography = Hilmer Ekdahl
| editing        = Rolf Husberg
| distributor    = 
| released       =  
| runtime        = 92 min
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Swing it, magistern! is a 1940 Swedish film directed by Schamyl Bauman and starring Adolf Jahr and Alice Babs.

== Synopsis ==
  and Alice Babs.]]
Young student Inga Danell secretly performs at nightclubs as Linda Loy.

== Cast ==
*Adolf Jahr as "Susen" Bergman
*Alice Babs as Inga Danell / Linda Loy (credited as Alice "Babs" Nilson)
*Solveig Hedengran as Lena Larsson
*Carl Hagman as William, principal (credited as Quarl Hagman)

== Soundtrack ==
*"Quick-foxtrot" composed by Eskil Eckert-Lundin, Instrumental.
*"Videvisan" composed by Alice Tegnér, lyrics by Zacharias Topelius
*"Svarta Rudolf" composed by Robert Norrby, lyrics by Erik Axel Karlfeldt, performed by Bert Sorbon Svasse Bergquist Prins Gustav, lyrics by Herman Sätherberg, performed by Åke Johansson Swing it, magistern!" composed by Kai Gullmar, lyrics by Hasse Ekman, performed by Alice Babs and Adolf Jahr
*"Min kärlek till dej" composed by Sten Axelson
*"Swing Ling Lej" composed by Thore Ehrling and Eskil Eckert-Lundin, lyrics by Hasse Ekman, performed by Alice Babs
*"Gymnastikrepetition" composed by Eskil Eckert-Lundin, Instrumental.
*"Regntunga skyar" composed by Thore Ehrling and Eskil Eckert-Lundin, lyrics by Hasse Ekman, performed by Alice Babs and Adolf Jahr
*"Fria preludier" composed by Adolf Jahr, instrumentalist Adolf Jahr (the piano)
*"Paris" composed by Ingrid Muschkin
*"Swing" composed by Thore Ehrling
*"Oh Boy, Oh Boy" composed by Sten Axelson, lyrics by Hasse Ekman, performed by Alice Babs
*" Engelbrektsmarschen", Instrumental.
*"Pojkmarsch" composed by Harry Lundahl, Instrumental.
*"Là ci darem la mano" from Don Giovanni composed by Wolfgang Amadeus Mozart, performed by Ragnar Planthaber and Ulla Hodell.

== Awards and honors ==
In 1941 Schamyl Bauman won a commendation at the Venice Film Festival for this film and for Magistrarna på sommarlov.

== External links ==
*  
*  

 
 
 
 
 
 


 
 