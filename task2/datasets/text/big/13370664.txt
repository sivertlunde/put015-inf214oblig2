Music Within
 
{{Infobox Film
| name           = Music Within
| image          = Music within post.jpg
| caption        = Theatrical release poster
| producer       = Brett Donowho Bruce Wayne Gillies Oli Laperal Jr. Steven Sawalich
| director       = Steven Sawalich
| writer         = Bret McKinney, Mark Andrew Olsen and Kelly Kennemer
| starring       = Ron Livingston Melissa George Michael Sheen
| music          = James T. Sale
| cinematography = Irek Hartowicz
| editing        = Timothy Alverson
| studio         = The Weinstein Company Lionsgate
| distributor    = Articulus Entertainment Quorum Entertainment MGM|Metro-Goldwyn-Mayer
| released       = October 26, 2007
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         =
| gross          = $153,205 (domestic)   
| followed_by    =
}}
Music Within is a 2007 drama film directed by Steven Sawalich and starring Ron Livingston, Melissa George, Michael Sheen, Rebecca De Mornay and Marion Ross. The film tells the true story of Richard Pimentel, a respected public speaker whose hearing disability attained in the Vietnam War drove him to become an activist for the Americans with Disabilities Act. 

The film, which takes place in Portland, Oregon, was filmed on and around the Portland State University campus.

==Plot synopsis==
The true story of Richard Pimentel, a brilliant public speaker with a troubled past. As a young man, Pimentel (Ron Livingston) realized he had a remarkable gift for public speaking. Pimentels idol is College Bowl founder Dr. Padrow (Hector Elizondo), but upon trying out for Dr. Padrow, the ambitious young speaker is informed that he wont have anything to talk about until he has lived a full life.

Realizing that there is some merit to Dr. Padrows observation, Pimentel subsequently enlists in the military and prepares for duty in Vietnam. Later, while fighting on the battlefield, Pimentel loses most of his hearing and is left with permanent tinnitus. He returns home frustrated. When others inform him that he will never achieve his dreams because he is deaf, the determined veteran makes it his mission to prove them wrong.
 Americans With Disabilities Act, and finally discover his inner music.

==Awards==
Movie director Steven Sawalich won an Audience Award at the AFI Dallas International Film Festival for best narrative feature film.

==Home video== Region 1 DVD on April 8, 2008.

==Primary Cast==
* Ron Livingston as Richard Pimentel
* Melissa George as Christine
* Michael Sheen as Art Honeyman
* Yul Vazquez as Mike Stolz
* Rebecca De Mornay as Richards Mom
* Hector Elizondo as Ben Padrow
* Leslie Nielsen as Bill Austin
* Ridge Canipe as Young Richard
* Paul Michael as Joe
* Clint Jung as Richards Dad John Livingston as Mr. Parks

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 