Block Busters
{{Infobox film
| name           = Block Busters
| image          = Bowery Champs.png
| image_size     = 
| caption        = 
| director       = Wallace Fox Barney A. Sarecky (associate producer)
| writer         = Houston Branch (writer)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Marcel Le Picard
| editing        = Carl Pierson
| distributor    = 
| studio         = Monogram Pictures
| released       = July 22, 1944
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Block Busters is a 1944 American film directed by Wallace Fox.

==Plot==
After an afternoon of playing baseball, Muggs McGinnis (Leo Gorcey) and the East Side Kids gang arrive at the door of their clubhouse, where a man named Higgins (silent comedian Harry Langdon, in one of his last film appearances) is removing their "East Side Club" sign. Higgins explains that the owner of the place plans to rent it to some "respectable" tenants. When Muggs learns that the new tenants are due to examine the place at noon the following day, he plans to frighten them away by picking a fight with Butch (Billy Benedict) and the Five Pointers, a rival gang. 

The next day, Glimpy (Huntz Hall) and Pinky (Gabriel Dell), scribble a challenge to the Five Pointers on the sidewalk. When Butch and his gang read the message, "The East Siders dare you to fight," they seek out their challengers. Meanwhile, Muggs and the gang see Higgins supervising the delivery of some window boxes that he ordered to replace the weather-beaten pots that are lining the street. Pretending to be helpful, the gang offers to dispose of the old pots, but instead, stack them against a nearby wall. 
 Noah Berry, Sr. tells Muggs that he will hold each one accountable for the others behavior. 

Later, Jean goes to the clubhouse to make sure that Muggs is staying out of trouble, and the gang teaches him some American games. Afterward, Jean invites the gang over for tea, and they meet snobby Irma Treadwell (Kay Marvis) and her mother Virginia. When Muggs and Glimpy see a black sedan pick up Jean, who is dressed like Count Dracula, they decide to follow him. The car takes Jean to a costume party at a chic club, where Muggs wins best costume for being dressed as a Bowery tough. 

Meanwhile Tobey Dunn (Bill Chaney), an ailing member of Muggss baseball team, is told by his doctor (Robert F. Hill) that a stay in the country would cure him, but unfortunately, Tobeys family cannot afford the trip. Later, Danny (Jimmy Strand), sees his girl friend Jinx (Roberta Smith) dancing with Jean at a party, so the gang decides to crash it. When Glimpy tells Danny that he saw Jinx riding on the back of Jeans bicycle, Danny tries to fight with his rival, but Muggs intervenes. 

The gang then goes to the field to play baseball, and Jean quickly learns the game. At the clubhouse, Amelia thanks the gang for allowing Jean to play with them. During the teams next game, Lippman (Bernard Gorcey), the teams sponsor, tells the gang that if they win, he will send them all to summer camp in the Catskill Mountains. With the bases loaded, Jean hits a home run and wins the game, and Tobey is awarded his much-needed trip to the country.

==Production==
*This was one of the few East Side Kids movies in which Gabriel Dell plays a member of the gang, and where Billy Benedict plays a different character.

*Bill Chaneys only film as an East Side Kid.

*Harry Langdons last movie released during his lifetime.

*Buddy Gorman is absent.
 Jimmie Noone and His Orchestra and The Ashburns.

*Leo Gorceys wife, Kay Marvis, has a supporting role as Irma Treadwell. In addition, his father, Bernard Gorcey has a role as the gangs baseball team sponsor, Lippman.

== Cast and characters ==

===The East Side Kids===
*Leo Gorcey as Ethelbert Muggs McGinnis
*Huntz Hall as Glimpy
*Gabriel Dell as Skinny (a.k.a. Pinky)
*Jimmy Strand as Danny
*Bill Chaney as Tobey

===Additional cast===
*Billy Benedict as Butch
*Fred Pressel as Jean
*Roberta Smith as Jinx
*Noah Beery as Judge
*Harry Langdon as Higgins
*Minerva Urecal as Amelia Rogiet
*Jack Gilman as Batter
*Kay Marvis as Irma Treadwell
*Tom Herbert as Meyer
*Bernard Gorcey as Lippman Charles Murray Jr. as Umpire
*The Ashburns as Themselves (uncredited)
*Jimmie Noone as Himself (uncredited)
*Robert F. Hill as Doctor (uncredited)

== External links ==
* 
* 

 

 
 
 
 
 
 
 