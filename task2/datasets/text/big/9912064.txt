The Voice of My City
 

{{Infobox film
| name           =La Voz de mi ciudad
| image          =The Voice of My City.jpg
| image_size     =
| caption        =
| director       =Tulio Demicheli
| producer       =Eduardo Bedoya
| writer         =Tulio Demicheli
| narrator       =
| starring       =
| music          =Mariano Mores 	
| cinematography =Francis Boeniger 	
| editing      =Ricardo Rodríguez Nistal, Atilio Rinaldi 	
| distributor    = Artistas Argentinos Asociados
| released       = 1953
| runtime        =110 minutes
| country        = Argentina Spanish
| budget         =
}}
 1953 Argentina|Argentine musical comedy film directed by Tulio Demicheli and starring Mariano Mores and
Diana Maggi. 

==Plot==
Roberto Moran (Mariano Mores) has just arrived in Buenos Aires from the provinces to work in a foundry. He can play the bandoneón by ear but wants to have proper training in music at a conservatory run by a frustrated old musician, Don Matias (Ricardo Galache). The old man first rejects both Roberto and his instrument, but after hearing him play, he changes his mind and takes him on, though forbidding him to play popular music, which he despises.

Roberto becomes a great classical pianist, but as he acquires proficiency, he secretly composes tangos too. One of the best sequences shows him informally playing the Argentine "Taquito Militar" accompanied by the other students playing their classical instruments (violin, clarinet, harp).

Roberto wins a scholarship to go to Europe to further his success as a pianist but he turns it down, preferring to compose "music that reveals the soul of the city". "The day will come," he tells the conductor Aquiles Baldi (Orestes Soriani), "when your great orchestra will play this kind of music."

A conflict arises with the arrival of factory owner, Francisco Romani (Santiago Gómez Cou), a calculating authoritarian who admires the United States. Roberto and Romani are both interested in Isabel (Diana Maggi), the conservatory directors daughter, who is torn between staying with the infatuated young musician who loves her or opting for the affluence of his more mature opponent. At the end of the film the roles are reversed: while Roberto achieves both popular success and the support of the "cult", showing his old master he can indeed express the "voice of the city", the entrepreneur is finally revealed to be a worthy suitor, full of feeling, who can succeed in winning the heart of the young woman.

==Cast==
*  Mariano Mores, as Roberto Morán
*  Diana Maggi, as Isabel, daughter of the director of the Conservatory
*  Santiago Gómez Cou as Francisco Romani
*  Ricardo Galache as Don Matías, director of the Conservatory
*  Virginia de la Cruz as Dorita, girlfriend of Jorge
*  Enrique Lucero as Jorge, brother of Roberto
*  Orestes Soriani as Maestro Aquiles Baldi
*  Susana Vargas
*  Alberto Dalbes as band leader
*  Domingo Mania as secretary of Don Romani
*  Jorge Leval
*  Tito Grassi
*  Mario Fortuna as Martín Baigorria, clarinetist
*  Lois Blue
*  Rodolfo Salerno as Juan the violinist
*  Elena Cruz as Angélica, the harpist
*  Aldo Vega as violinist
*  Carlos Escobares
*  Francisco Canaro 
*  Juan DArienzo 
*  Lois Blue Faure

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 