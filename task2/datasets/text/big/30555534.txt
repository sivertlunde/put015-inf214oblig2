Montevideo, God Bless You!
{{Infobox film
| name           = Montevideo, Bog te Video!
| image          = Montevideo-Bog-te-video.jpg
| alt            = 
| caption        = Official Poster
| director       = Dragan Bjelogrlić
| produced by    =   
| producer       = Dejan Petrović
| exec. producer = Goran Bjelogrlić
| writer         = Srđan Dragojević Ranko Božić
| based on       =  
| starring       = Miloš Biković Petar Strugar Nina Janković Danina Jeftić Magnifico
| cinematography = Goran Volarević
| editing        = Marko Glušac
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = Serbia
| language       = Serbian
| budget         = 
| gross          = $2,192,931  (Serbia) 
}} the first FIFA World Cup in Montevideo, Uruguay in July 1930. The film gained considerable media attention throughout 2010 and achieved significant box office success in Serbia since its release on December 21, 2010.  The entire project has been hugely successful regionally thus far. More than 520,000 people in Serbia saw the first film, which won numerous awards. 
 The Game of Their Lives. 
 Best Foreign Language Film at the 84th Academy Awards,       but did not make the final shortlist.   

==Plot==
 ) and playboy superstar Moša (Petar Strugar).

The two young men eventually become friends when theyre thrown together on the front line of the struggling local team, Belgrade Sports Club - BSC. As the club hierarchy is faced with the challenge of keeping the squad afloat, the opportunity arises to create a national team. However, team unity is strained when Tirke and Moša clash over beautiful women. Rosa (Danina Jeftić), the voluptuous, small-town innocent who adores Tirke, but her soccer-mad uncle conspires to set her up with Moša; and vampish Valeria (Nina Janković), a free-spirited bohemian who seduces Moša and finds much fun in pitting him against Tirke. The initial doubt that surrounded their personal and professional lives is transformed into a shared ambition to prove themselves in Montevideo; as a result, a story about friendship, enthusiasm, persistence and love for the game is unraveled.

==Cast== Aleksandar "Tirke" Tirnanić Blagoje "Moša" Marjanović
* Nina Janković as Valerija, an eccentric Belgrade artist and painter, who hangs out among Belgrades elite.
* Danina Jeftić as Rosa, Tirkes love interest, who works at her uncle Rajkos tavern.
* Mima Karadžić as Rajko, owner of the local tavern.
* Branimir Brstina as Bogdan Brica
* Vojin Ćetković as Mihajlo "Andrejka" Andrejević
* Nebojša Ilić as Boško Simonović|Boško "Dunster" Simonović, who becomes the national teams coach later in the film.
* Nikola Ðuričko as Živković
* Sergej Trifunović as Načelnik Komatina Milutin "Milutinac" Ivković,
* Predrag Vasić as Little Stanoje, a shoeshine boy, he considers himself as Tirkes best friend, and he lives in poor conditions and has a leg deformity.
* Srđan Todorović as Bora Jovanović Boda Ninković as Kustodić
* Marko Živić as Isak Milorad "Balerina" Arsenijević
* Uroš Jovicić as Đorđe Vujadinović|Đorđe "Ðokica Nosonja" Vujadinović
* Bojan Krivokapić as Momčilo Đokić|Momčilo "Gusar" Đokić Milovan "Jakša" Jakšić Dragoslav "Vampir" Mihajlović
*   as Ljubiša Stevanović|Ljubiša "Leo" Stevanović
* Rade Ćosić as Teofilo Spasojević Branislav "Bane" Sekulić
* Tamara Dragićević as Eli King Alexander

==Accolades==
{| class="wikitable"
|-
! Event !! Category !! Winner/Nominee !! Result
|-
|rowspan="9"|  
|- Best Actor
|Miloš Biković
| 
|- Best Actress Danina Jeftić
| 
|- Best Actress Nina Janković
| 
|- Best Picture Intermedia Network
| 
|- Best Director Dragan Bjelogrlić
| 
|- Best Director of Photography Goran Volarević
| 
|- Best Script 
|Srđan Dragojević and Ranko Božić
| 
|- Best Production Design  Nemanja Petrović
| 
|-
|   || Best Picture || Intermedia Network || 
|-
|  || Project of the Year || Intermedia Network || 
|-
|  || Best Film || Intermedia Network || 
|- Moscow International 33rd Moscow International Film Festival || Audience Choice Award || Intermedia Network || 
|- Pula Film 58th International Pula Film Festival || Special Mention || Intermedia Network || 
|-
|  || Best Production Design || Nemanja Petrovic || 
|- Lipetsk International Sports Film Festival ATLANT 2011 || Best Feature Film About The Sport || Intermedia Network || 
|- Leskovac International Film Festival LIFFE 2011 || Audience Award for Best Picture || Intermedia Network || 
|- Intermedia Network || 
|-
|  || Gran Prix || Intermedia Network || 
|-
|  || Guirlande DHonneur || Intermedia Network || 
|- Nis Film Festival 2011 
|- Best Actor - Constantine The Great Award
|Miloš Biković
| 
|- Best Debut Petar Strugar
| 
|- FIPRESCI Award Viktor Savic
| 
|- Best Comic Role Branimir Brstina
| 
|- Actor of the Evening Predrag Vasic
| 
|}

==DVD and TV series==
The movie was released on DVD during 2011.

On 1 January 2012 it was broadcast on RTS1, achieving stellar ratings with over 3.1 million viewers. 

The extended version of the film, including 5 hours of footage unseen in the theatrical cut, began to be broadcast as the eponymous television series on RTS starting 13 February 2012,  and onwards weekly every Monday in the 8pm prime time slot. Nine episodes aired,  with the first season concluding on 9 April 2012.

The second series, titled Na putu za Montevideo, (season two) began on 31 December 2012, this time airing Sundays. The plot now moved to the preparations for the long trip to Uruguay. Nine more episodes were shown concluding on 10 March 2013.

In July 2013, the first series began airing in Croatia on RTL Televizija. 

On 3 August 2013, Montevideo, God Bless You!, aired on the Chinese CCTV-6 network. 

On 15 January 2014, the sequel feature film See You in Montevideo|Montevideo, vidimo se! got released in theaters.

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Serbian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
{{s-ttl|title= Serbian Oscar Of Popularity The Movie of the Year
|years=2010}}
 
 

 
 
 
 
 
 
 
 
 
 
 
 