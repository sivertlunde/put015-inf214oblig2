Godmother (film)
{{Infobox film
| name           = Godmother
| image          = Godmother_(film).jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Vinay Shukla
| producer       = Rajat Sengupta
| writer         = Vinay Shukla
| narrator       = 
| starring       =Shabana Azmi Milind Gunaji  Nirmal Pandey 
| music          = Vishal Bhardwaj
| cinematography = Rajan Kothari
| editing        = Renu Saluja
| studio         = 
| distributor    = Yash Raj Films
| released       =    
| runtime        = 150 min
| country =  
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Godmother is a 1999 Hindi biographical drama film directed by Vinay Shukla released in 1999, and ostensibly inspired by the life of Santokben Jadeja, who ran the Mafia operations of Porbandar, Gujarat in the late 1980s and early 1990s and later turned politician. 

==Cast==
* Shabana Azmi - Rambhi
* Milind Gunaji - Veeram
* Nirmal Pandey - Jakhra
* Govind Namdeo	- Kesubhai
* Vinit Kumar	- Lakhubhai
* Loveleen Mishra - Ramdes Wife
* Raima Sen - Sejal
* Sharman Joshi - Karsan

==Music==
Vishal Bhardwaj for the music & Javed Akhtar for lyrics grabbed the National Awards for this movie. The songs in the movie were as below.
* "Gunje Gagan Gunje Lalkaren Ham" (Singer:Roop Kumar Rathod) 
* "Matee Re Matee Re"  (Singer: Lata Mangeshkar) 
* "Raja Kee Kahanee Puranee Ho Gayee"  (Singer: Usha Uthup, Rekha, Kavita Krishnamurthy )
* "Suno Re Suno Re Bhayila"  (Singer: Sanjeev Abhyankar )
* "Tum Agar Yeh Mujhse Puchho"  (Singer: Abhijeet )

==Awards==

===National Film Awards===
* National Film Award for Best Feature Film in Hindi
*  
*  
*  
*  
*  

===Filmfare Awards===
*  

===IIFA Awards===
*  

==References==
 

==External links==
* 
*   at Yashraj Films

 

 
 
 
 
 
 
 
 
 


 
 