I. Y. Yunioshi
 Breakfast at Tiffanys.]] Breakfast at Breakfast at Tiffanys has been the subject of extensive critical commentary and review since 1990.

==Critical response==
The 1961 New York Times review of the film said that "Mickey Rooneys bucktoothed, myopic Japanese is broadly exotic."  In 1990, The Boston Globe described Rooneys portrayal as "an irascible bucktoothed nerd and an offensive ethnic caricature".    In 1993, the Los Angeles Daily News wrote that the role "would have been an offensive stereotype even played by an Asian; the casting of Mickey Rooney added insult to injury". 

More recent characterizations include "cringe-inducing stereotype",  "painful, misguided",  "overtly racist",  "one of the most egregiously horrible comic impersonations of an Asian (Mr. Yunioshi) in the history of movies",  and a portrayal "border  on offensive" that is a "double blow to the Asian community – not only is he fatuous and uncomplimentary, but he is played by a Caucasian actor in heavy makeup."   
 Asian man. 
 animated film Ratatouille (film)|Ratatouille after protests about the Yunioshi character. The protest was led by Christina Fa of the Asian American Media Watch.    
 New York Daily News by columnist Jeff Yang offered an alternative view regarding the protests: "Far from boycotting the movie or even begrudgingly accepting it, I think it should be mandatory viewing for anyone who wants to fully understand who we are as a culture, how far weve come and how far we still need to go." 

==Response to criticism==
In a 2008 interview about the film, the then 87-year-old Rooney said he was heartbroken about the criticism: "Blake Edwards...wanted me to do it because he was a comedy director. They hired me to do this overboard, and we had fun doing it....Never in all the more than 40 years after we made it – not one complaint. Every place Ive gone in the world people say, God, you were so funny. Asians and Chinese come up to me and say, Mickey you were out of this world." Rooney also said that if hed known people would be so offended, "I wouldnt have done it". 
 yellow face". {{Cite web
  | last = Bell
  | first = Robert
  | authorlink = 
  | coauthors = 
  | title = DVD Review: Breakfast at Tiffanys - Centennial Collection
  | work = 
  | publisher = The Trades
  | date = January 12, 2009
  | url = http://www.the-trades.com/article.php?id=10826
  | format = 
  | doi = 
  | accessdate = September 24, 2011}} 

==See also==
* Portrayal of East Asians in Hollywood
 
 

==References==
 

 
 
 
 
 