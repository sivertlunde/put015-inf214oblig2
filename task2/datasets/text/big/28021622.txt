Bangaru Panjaram
{{Infobox film
| name           = Bangaru Panjaram
| image          = Bangaru Panjaram.jpg
| image_size     =
| caption        =
| director       = B. N. Reddy
| producer       = 
| writer         = 
| narrator       = Sriranjani Vanisri
| music          = Saluri Rajeswara Rao
| cinematography = B. N. Konda Reddy
| editing        = 
| studio         =
| distributor    =
| released       = March 19, 1969
| runtime        =
| country        = India Telugu
| budget         =
}} Telugu drama film directed by B. N. Reddy. It is a musical film with lyrics written by Devulapalli Krishnasastri.
 
 

==Cast==
* Shobhan Babu	... 	Venu Sriranjani		
* Vanisree	... 	Neela

==Crew==
* Director : B. N. Reddy	 	
* Original music : Saluri Rajeshwara Rao	 	
* Cinematography : B. N. Konda Reddy	 	
* Art direction : A. K. Sekhar
* Lyrics : Devulapalli Krishnasastri	 	
* Playback singers : S. Janaki, Ghantasala Venkateswara Rao

==Soundtrack==
The lyrics were penned by Devulapalli Krishnasastri. The musical score was composed by Saluri Rajeswara Rao and voiced by S. Janaki. 
* "Gattu Kaada Evaro Chettu Needa Evaro Nallakanula Nagasvaramu Ooderu Evaro" (Lyrics: Devulapalli Krishnasastri; Singer: S. Janaki)
* "Jo Kodutu Katha Chebite Oo Kodutu Vintava" (Lyrics: Devulapalli Krishnasastri)
* "Kondala Konala Sooreedu Kurise Bangaru Neeru" (Lyrics:  ; Cast: Vanisri)
* "Manishey Maareraa Raaja Manase Maareraa" (Lyrics: Devulapalli Krishnasastri; Singer: S. Janaki)
* "Nee Padamule Chaalu Raama Nee Pada Dhoolule Padivelu" (Lyrics: Devulapalli Krishnasastri)
* "Pagalaite Doravera Raatiri Naa Raajuvula" (Singer: S. Janaki; Cast: Sobhan Babu and Vanisri)
* "Srisaila Bhavana Bhramaramba Ramana" (Lyrics: Devulapalli Krishnasastri; Singers: Ghantasala and S. Janaki)

==Awards==
* The film won Filmfare Best Film Award (Telugu) (1969)
* The film won Nandi Award for Best Feature Film - Kamsya (Bronze) (1969)
*Sobhan Babu received the Rashtrapati Award in acting 1966.  

==References==
 
 

 
 
 
 


 