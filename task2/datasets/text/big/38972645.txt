Ikan Doejoeng
 
{{Infobox film
| name           = Ikan Doejoeng
| image          = Ikan Doejoeng ad.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Newspaper ad, Surabaya
| director       = Lie Tek Swie
| producer       = 
| screenplay     = 
| based on       = 
| narrator       = 
| starring       ={{plain list|
*Asmanah
*Soerjono
}}
| music          = 
| cinematography = 
| editing        = 
| studio         = Standard Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies
| language       = Indonesian
| budget         = 
| gross          = 
}}
Ikan Doejoeng (  directed by Lie Tek Swie and starring Asmanah and Soerjono. Following star-crossed lovers, the film was the first released by Standard Film. Likely targeted at the educated elite, it is now probably lost film|lost.

==Plot==
Asmara is in love with Sanusi, but told to marry Harun. Unknown to her, Harun is already in a romantic relationship with Emi. As a means of escape, Asmara imagines herself to be a mermaid. Meanwhile, Sanusi must face bandits. 

==Production==
Ikan Doejoeng was directed by Lie Tek Swie and produced by Touw Ting Iem (or James), an English-trained sound technician.  It was the first production of Standard Film, which Lie had established together with the Tan brothers (Khoen Yauw and Khoen Hian) earlier that year; in the early 1930s he had directed several films for them, most recently Melati van Agam (Jasmine of Agam) in 1932. Touw, however, was titularly head of the Jakarta|Batavia-based company.  Ikan Doejoeng was shot in black-and-white. Much of the filming was conducted on or underwater. 

The film starred Asmanah and Soerjono. It also featured Achmad Thys, Poniman, Momo, Nawi Ismail, and A. Rasjid.   

==Release and reception==
  native intellectual class, citing the plot. He writes, however, that the Ikan Doejoeng  technical quality was on par with films aimed at uneducated audiences.  The film was advertised as having a "deeply poignant theme of pure love", which would give viewers "a good perspective on the Eastern mentality."  

Lie would go on to direct one last film for Standard, Siti Noerbaja, in 1942; he ultimately left the company. Standard closed later that year, having released three films: the last, Selendang Delima (Pomegranate Shawl), was directed by Henry L. Duarte. 

The film is likely lost film|lost. The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==Notes==
 

==References==
 

==Works cited==
 
* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
* {{cite web
  | title = Ikan Doejoeng
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-i012-41-300584_ikan-doejoeng
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 26 July 2012
  | archiveurl = http://www.webcitation.org/69QyiGu7E
  | archivedate = 26 July 2012
  | ref =  
  }}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011177108%3Ampeg21%3Ap008%3Aa0086
 |work=De Sumatra Post
 |accessdate=31 March 2013
 |publisher=J. Hallermann
 |date=27 September 1941
 |location=Medan
 |ref= 
 |page=8
}}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011121997%3Ampeg21%3Ap008%3Aa0108
 |work=Soerabaijasch Handelsblad
 |accessdate=31 March 2013
 |publisher=Kolff & Co
 |date=31 December 1941
 |location=Surabaya
 |ref= 
 |page=8
}}
 

 

 
 
 
 