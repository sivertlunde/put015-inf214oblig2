List of Bollywood films of 1992
 
 
A list of films produced by the Bollywood film industry based in Mumbai in 1992.
==Film list==
{| class="wikitable"
! Title !! Director !! Cast !! Genre !! Music Director
|-
|   | 1992 in film|1992
|-
|Aaj Ka Goonda Raaj || Ravi Raja ||Chiranjeevi, Meenakshi Sheshadri, Raj Babbar || Action, Thriller, Family|| Anand-Milind
|-
|Aaj Ki Taaqat || Anil Nagrath || Sunil Puri, Upasana Singh, Jaya Mathur || Action ||
|- Pankaj Parashar || Abhishek Chitlangia, Raghuvir Yadav, Anupam Kher|| ||
|-
|Adharm (1992 film)|Adharm|| Aziz Sajawalq || Shabana Azmi, Shatrughan Sinha, Sanjay Dutt ||Thriller|| Anand-Milind
|-
|Agar Aap Chahein || Mazahir Rahim || Makarand Deshpande || Family||
|- Drama ||
|-
|Angaar || Shashilal. K. Nair || Jackie Shroff, Dimple Kapadia, Om Puri || Action, Drama || Laxmikant-Pyarelal
|- Ramesh Modi  || Arman Kohli, Ayesha Jhulka, Kiran Kumar||Action, Drama, Thriller ||
|-
|Apradhi (1992 film)|Apradhi || K. Ravi Shankar|| Anil Kapoor, Shilpa Shirodkar || ||
|-
|Baaz || S. Subhash || Govinda, Sonam (actress)|Sonam, Tinu Anand, Archana Puran Singh || Drama || Anand-Milind
|-
|Balwaan || Deepak Anand || Sunil Shetty, Divya Bharati, Danny Denzongpa || Action || Mahesh Kishore
|-
|Basanti Tangewali || Kranti Shah|| Ashrat Ali, Sadashiv Amrapurkar, Avtar Gill || ||
|- Rahul Rawail|| Kajol, Kamal Sadanah, Ajay Mankotia, Tanuja, Vijayendra Ghatge||Romance|| Nadeem-Shravan
|- Indra Kumar Anil Kapoor, Drama || Anand-Milind
|- Vivek Mushran, Pran ||Romance ||
|-
|Bol Radha Bol || David Dhawan || Rishi Kapoor, Juhi Chawla, Kader Khan || Comedy, Drama || Anand-Milind
|- Rajiv Mehra||Shah Rukh Khan, Urmila Matondkar, Naseeruddin Shah, Shammi Kapoor||Fantasy, Comedy || Anu Malik
|- Girish Karnad || Girish Karnad, Sonali Kulkarni, Prashant Rao|| ||
|- Om Puri, Deepti Naval, Shreeram Lagoo|| Drama||
|-
|Daku Aur Police || || Naseeruddin Shah || ||
|-
|Daulat Ki Jung || S. A. Kadher || Aamir Khan, Juhi Chawla, Shafi Inamdar || Action, Drama || Anand-Milind
|-
|Deedar (1992 film)|Deedar || Pramod Chakravarthy || Akshay Kumar, Karishma Kapoor || Action, Romance || Anand-Milind
|- Raj Kanwar Rishi Kapoor, Romance || Nadeem-Shravan
|-
|Dharavi || Sudhir Mishra || Shabana Azmi, Pramod Bala, Madhuri Dixit|| Crime, Drama||
|- Divya Bharti, Romance || Anand-Milind
|-
|Dil Hi To Hai || Asrani || Jackie Shroff, Divya Bharti, Shilpa Shirodkar || Drama || Laxmikant-Pyarelal
|-
|Dil Ka Kya Kasoor|| Lawerence DSouza ||Prithvi, Divya Bharti||Romance|| Nadeem-Shravan
|-
|Dilwale Kabhi Na Hare || V. Menon || Rahul Roy, Prithvi, Nagma|| Romance||
|- Anupam Kher, Beena, Kanchan, Kiran Kumar, Mohan Kumar, Pran, Reema Lagoo|| ||
|- Gurudeep Sing, Ram Gopal Varma  || Nagarjuna Akkineni, Urmila Matondkar, Danny Denzongpa|| ||
|-
|Dushman Zamana || || Divya Bharti, Arman Kohli, Paresh Rawal|| ||
|- Lawrence DSouza Salman Khan, Thriller || Anand-Milind
|-
|Ganga Bani Shola || ||Jamuna, Shakti Kapoor, Kader Khan || ||
|- Gulshan Ashte Avtar Gill, Gulshan Grover, Dinesh Hingoo || Action||
|-
|Geet (1992 film)|Geet || Partho Ghosh|| Avinash Wadhavan, Divya Bharti, Laxmikant Berde|| ||
|- Ghar Jamai Rita Bhaduri, Mithun Chakraborty, Varsha Usgaonkar, Shakti Kapoor, Prem Chopra || Drama, Comedy, Family || Anand-Milind
|- Ranjeet || Rahul Roy, Anu Agrawal, Tanuja|| Family||
|- Farogh Siddiqui  || Ajay Devgn, Karisma Kapoor, Madhavi|| ||
|- Heer Ranjha || Harmesh Malhotra || Anil Kapoor, Sridevi, Anupam Kher || Romance || Laxmikant-Pyarelal
|- Rishi Kapoor, Varsha Usgaonkar, Ashwini Bhave ||Comedy, Drama ||
|-
|Humlaa || N. Chandra || Ashok Kumar, Dharmendra, Anil Kapoor, Kimi Katkar || Action || Laxmikant-Pyarelal
|-
|Humshakal || Kalptaru || Shammi Kapoor, Vinod Khanna, Nirupa Roy, Meenakshi Sheshadri|| ||
|- I Love Romance ||
|- Action ||
|- Horror ||
|- Rishi Kapoor, Sushma Seth ||Drama, Family, Musical, Romance||
|- Kalidas  || Aamir Khan, Farha Naaz, Asrani, Pran (actor)|Pran|| Family, Comedy, Drama ||
|- Suresh Krissna||Salman Khan, Karisma Kapoor, Ashok Saraf, Prem Chopra, Raza Murad || Crime drama|| Anand-Milind
|-
|Jaan Se Pyaara|| Anand ||Govinda, Divya Bharti ||Crime, Drama || Anand-Milind
|- Deepak Balraj Ronit Roy, Sulbha Deshpande||Romance|| Nadeem-Shravan
|- Vikram Bhatt Rahul Roy, Pooja Bhatt, Paresh Rawal || Romance||
|- Saeed Jaffrey, Jeetendra, Hema Malini || Thriller||
|-
|Jeena Marna Tere Sang || Vijay Reddy || Sanjay Dutt, Raveena Tandon, Javed Jaffrey || Romance || Nadeem-Shravan
|-
|Jigar || Farogh Siddique || Ajay Devgan, Karishma Kapoor, Gulshan Grover || Action, Romance || Anand-Milind
|- Mansoor Khan Aamir Khan, Ayesha Jhulka, Pooja Bedi, Mamik Singh, Kiran Zaveri ||Drama, Romance || Jatin Lalit
|-
|Junoon (1992 film)|Junoon || Mahesh Bhatt || Rahul Roy, Pooja Bhatt, Tom Alter, Rakesh Bedi || Horror, Thriller || Nadeem-Shravan
|-
|Kal Ki Awaz ||B.R. Chopra, Ravi Chopra  || Dharmendra, Raj Babbar, Amrita Singh|| Action||
|- Raj Babbar, Dimple Kapadia ||Action, Drama ||
|-
|Kasak (1992 film)|Kasak ||K. Bapaiah  || Rishi Kapoor, Neelam, Chunky Pandey|| ||
|-
|Khel (1992 film)|Khel || Rakesh Kumar || Anil Kapoor, Madhuri Dixit, Sonu Walia || Romance || Rajesh Roshan
|-
|Khiladi || Abbas Mustan || Akshay Kumar, Ayesha Jhulka, Deepak Tijori, Sheeba || Romance, Drama, Thriller || Jatin Lalit
|- Mukul Anand Amitabh Bachchan, Sridevi, Nagarjuna Akkineni, Shilpa Shirodkar ||Drama, Romance || Laxmikant-Pyarelal
|- Action ||
|- Mahendra Sandhu || Mahendra Sandhu|| ||
|- Sunil Agnihotri  || Jackie Shroff, Neelam|| Action, Crime, Drama, Thriller||
|- Sharad Chaudhary  || Kabir Bedi, Madhavi||Action, Crime, Drama ||
|- Ajay Kashyap  || Jeetendra, Jayapradha, Aruna Irani|| ||
|- Vinod Khanna, Hema Malini, Dimple Kapadia || Drama||
|- Heemayun Mirza, Mahrukh Mirzaa  || Ayub Khan, Ayesha Jhulka, Kiran Kumar, Pran (actor)|Pran|| Drama, Romance||
|- Avinash Wadhavan, Ashwini Bhave, Pran || ||
|- Rama Jit Juneja, Gurdeep Singh ||Alpana, Mohnish Bahl, Beena Banerjee || Romance||
|-
|Mera Dil Tere Liye || S.A. Chandrashekhar || Gopi Bhalla, Dinesh, Aruna Irani|| Romance||
|- Rajesh Vakil  || Mithun Chakraborty, Juhi Chawla, Shanti Priya|| Drama, Musical||
|-
|Mr Bond || Raj Sippy || Akshay Kumar, Sheeba || Action, Romance || Anand-Milind
|-
|Muskurahat || Priyadarshan || Jay Mehta, Revathi Menon, Annu Kapoor, Amrish Puri || Drama || Ram Laxman
|- Neelima Azim, Goga Kapoor, Shakti Kapoor || Fanatasy, Musical||
|- Kalpataru  || Chunky Pandey, Nutan, Farha Naaz|| ||
|-
|Nishchaiy || Esmayeel Shroff || Vinod Khanna, Salman Khan, Karisma Kapoor|| Action, Crime, Drama||
|-
|Paayal || Mahendra Shah || Himalaya, Bhagyashree, Aruna Irani || Romance || Nadeem Shravan
|- Naseeruddin Shah, Pran || ||
|- Yash Chopra||Sunil Neelam ||Romance || Shiv - Hari
|-
|Parasmani || Mohan T. Gehani || Siddhant, Madhuri, Goga Kapoor|| ||
|- Kiran Kumar, Meena, Chunky Pandey || ||
|- Raaj Kumar, Vinod Khanna, Meenakshi Sheshadri, Nagma || Action, Musical, Romance||
|-
|Police Officer || Ashok Gaekwad || Jackie Shroff, Karishma Kapoor, Gulshan Grover || Action || Anu Malik
|-
|Prem Deewane || Sachin || Jackie Shroff, Madhuri Dixit, Pooja Bhatt || Romance ||  Laxmikant-Pyarelal
|-
|Pyar Hua Chori Chori || K. Bapaiah, J. Hemambar W|| Mithun Chakraborty, Gautami, Shikha Swaroop|| ||
|-
|Qaid Mein Hai Bulbul || Ravindra Peepat || Rana Jung Bahadur, Bhagyashree, Sudha Chandran|| ||
|-
|Raat (film)|Raat || Ram Gopal Varma || Revathi Menon, Om Puri, Rohini Hattangadi || Horror, Thriller || Mani Sharma
|-
|Radha Ka Sangam || Kirti Kumar ||Govinda, Juhi Chawla|| ||
|- Aziz Mirza||Shah Rukh Khan, Juhi Chawla, Amrita Singh ||Romance|| Jatin Lalit
|- Yugo Sako, Ram Mohan || Amrish Puri || Epic ||
|-
|Rishta To Ho Aisa || Kalpataru|| || ||
|- Mani Ratnam Arvind Swamy, Madhoo, Pankaj Kapoor || Social || A. R. Rahman
|-
|Saatwan Aasman || Mahesh Bhatt|| Vivek Mushran, Pooja Bhatt, Avtar Gill|| ||
|- Ajay Kashyap Sanjay Dutt, Neelam, Aditya Pancholi ||Action, Drama, Romance ||
|-
|Sanam Tere Hain Hum ||V. Menon || || ||
|-
|Sangeet (1992 film)|Sangeet || K. Vishwanath || Madhuri Dixit, Jackie Shroff, Nitish Bhardwaj || Drama || Anand-Milind
|- Lawrence DSouza Jackie Shroff, Dimple Kapadia, Karisma Kapoor, Rahul Roy || Romance||
|-
|Sarphira || Ashok Gaikwad|| Sanjay Dutt, Madhavi, Kimi Katkar|| Action||
|- Shola Aur David Dhawan|| Comedy || Bappi Lahiri
|- Sukhwant Dhadda  || Kumar Gaurav, Kimi Katkar, Shashi Ranjan||Action, Drama ||
|- Rajit Kapur, Rajeshwari Sachdev, Pallavi Joshi, Neena Gupta, Amrish Puri || Drama || Vanraj Bhatia
|-
|Suryavanshi (1992 film)|Suryavanshi || Rakesh Kumar || Salman Khan, Amrita Singh, Kader Khan, Sheeba || Drama || Anand-Milind
|-
|Tahalka || Anil Sharma|| Dharmendra, Naseeruddin Shah, Mukesh Khanna, Shammi Kapoor, Ekta Sohini, Sonu Walia, Pallavi Joshi||Action||
|-
|Tilak || Yash Chauhan || Rita Bhaduri, Goga Kapoor, Kiran Kumar|| Action||
|- Time Machine Amrish Puri, Rekha, Naseeruddin Shah ||Drama, Science Fiction ||
|- Mehul Kumar||Raaj Kumar, Nana Patekar ||Action|| Laxmikant-Pyarelal
|-
|Tulsidas || Atma Ram|| || ||
|-
|Tyaagi || K. C. Bokadia || Vikas Anand, Rajinikanth, Bhagyashree, Jayapradha || Action || Bappi Lahiri
|- Vikas Anand, Bhagyashree, Birbal || ||
|- Akshay Anand, Chandni, Gulshan Grover || Comedy, Drama, Romance||
|-
|Vansh || Pappu Verma || Sudesh Berry, Siddharth, Anupam Kher|| Thriller||
|- Rajkumar Kohli  || Dharmendra, Poonam Dhillon, Sunil Dutt, Roopali Ganguly|| ||
|- Rajiv Rai||Sunny Deol, Naseeruddin Shah, Sonam (actress)|Sonam, Chunkey Pandey, Divya Bharati ||Crime, Action || Viju Shah
|- Action ||
|-
|Yaad Rakhegi Duniya || Deepak Anand|| Aditya Pancholi, Rukhsar, Tinnu Anand|| ||
|-
|Yalgaar (India film)|Yalgaar || Feroz Khan || Sanjay Dutt, Manisha Koirala, Naghma, Kabir Bedi, Deepti Naval || Drama, Action || Chinni Singh
|- Sadashiv Amrapurkar, Jeetendra, Meenakshi || ||
|-
|Yudhpath || Ravi Ravan|| Sudesh Berry, Ekta Sohini, Mohnish Bahl|| Action, Crime, Drama||
|-
|Zakhmi Sipahi ||T.L.V. Prasad ||  Mithun Chakraborty, Rituparna Sengupta||Action||
|-
|Zindagi Ek Juaa || Prakash Mehra || Anil Kapoor, Madhuri Dixit, Amrish Puri || Drama || Bappi Lahiri
|- Bharat Rangachary  ||Dharmendra, Moushumi Chatterjee, Govinda, Kimi Katkar || ||
|}

==References==
 

==External links==
*   at the Internet Movie Database


 
 
 

 
 
 