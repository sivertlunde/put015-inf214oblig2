Kampen om Næsbygård
{{Infobox film
| name           = Kampen om Næsbygård
| image          = 
| caption        = 
| director       = Ib Mossin Alice OFredericks
| producer       = Finn Aabye Just Betzer Henning Karmark
| writer         = Morten Korch Ib Mossin Alice OFredericks
| starring       = Asbjørn Andersen
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen Henning Kristiansen
| editing        = Lars Brydesen
| distributor    = ASA Film
| released       = 18 December 1964
| runtime        = 108 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

Kampen om Næsbygård is a 1964 Danish family film directed by Ib Mossin and Alice OFredericks.

==Cast==
* Asbjørn Andersen - Godsejer Martin
* Karen Berg - Helene
* Ole Wisborg - Torben
* Poul Reichhardt - Præsten Prip
* Inger Stender - Præstekonen Anna
* Agnes Rehni - Tante Thyra
* Ib Mossin - Anker
* Jane Thomsen - Præstedatteren Rosa
* Ole Neumann - Martin Jr.
* Helga Frier - Kokkepigen Marie
* Marie-Louise Coninck - Stuepigen Erna
* Einar Juhl - Sagføreren
* Knud Schrøder - Bookmaker
* Holger Vistisen - Bookmaker
* Yvonne Ekmann - Torbens kæreste
* Palle Huld - Lægen
* Peter Malberg - Gartneren Ole
* Christian Arhoff - Gartneren Nick
* Gunnar Hansen - TV reporter

==External links==
* 

 

 
 
 
 
 
 


 