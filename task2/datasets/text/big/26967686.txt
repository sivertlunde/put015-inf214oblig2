On Tour (2010 film)
 
{{Infobox film
| name           = On Tour
| image          = Tournee-poster.png
| caption        = Theatrical release poster by Christophe Blain
| director       = Mathieu Amalric
| producer       =  
| writer         =  
| starring       = Mathieu Amalric
| music          = 
| cinematography = Christophe Beaucarne
| editing        = Annette Dutertre
| studio         = Les Films du Poisson
| Distributed by = 20th Century Fox (USA)
| distributor    = Le Pacte
| released       =  
| runtime        = 111 minutes
| country        = France
| language       =  
| budget         = €3.52 million
| gross          = $7 million  
}} Dirty Martini, Julie Atlas Muz, Evie Lovelle and Roky Roulette. In a road movie narrative, the plot follows the troupe as they tour French port cities with their show, which was performed for actual audiences during the production. The inspiration for the film was a book by Colette about her experience from music halls in the early 20th century, and a part of Amalrics aim was to translate the sentiment of the book to a modern setting.
 FIPRESCI Award, Best Director Award.

==Plot==
Formerly successful television producer Joachim Zand returns from America to his native France, where he previously has left everything behind, including friends, enemies and his own children. In his company is a burlesque striptease troupe whom he has promised a grand performance in Paris.

Together they tour the French port cities, staying at cheap hotels and making success along the way. Old conflicts are however reignated upon the return to the French capital. Joachim is betrayed by people from his past, making him lose the venue where they were to perform, and the Paris finale comes to nothing. 

==Cast==
* Mathieu Amalric as Joachim Zand
* Miranda Colclasure as Mimi Le Meaux
* Suzanne Ramsey as Kitten on the Keys
* Linda Maracini as Dirty Martini
* Julie Ann Muz as Julie Atlas Muz
* Angela de Lorenzo as Evie Lovelle
* Alexander Craven as Roky Roulette
* Damien Odoul as François
* Ulysse Klotz as Ulysse
* Simon Roth as Baptiste
* Joseph Roth as Balthazar
* André S. Labarthe as cabaret manager
* Pierre Grimblat as Chapuis
* Julie Ferrier as Julie Ferrier

==Production==
The idea for the film came from the 1913 book The Other Side of Music-Hall by Colette, a collection of texts written for a newspaper about her life during a music hall tour in the French provinces. The project started around the same time as the suicide of independent film producer Humbert Balsan, which also had made an impression on Amalric.    "Im fascinated by producers. I always wonder how they manage to keep going and take such responsibility. ... So these different themes came together and I invented a story about a French TV producer and the women who were courageous enough to come to France with him."  In the early drafts of the screenplay, Amalric struggled with the context of the story, figuring whether he would be able to put early 20th century vaudeville in a present-day setting, or attach the sentiment of Colettes book to modern striptease. Then he read an article in Libération about the American Neo-Burlesque movement, where performers mix striptease with comedy and a resistance to social pressures, and Amalric saw a connection to what Colette had been doing.    The narrative was written before any further research was made, as Amalric did not want the film to be too much like a documentary. The first time he saw a Neo-Burlesque show was in 2007, in Nantes. He says that he did not mention the film project to the performers, but spent the following three days in their company.  Later on he went to the United States to see as many shows as possible and study the movement in detail. 
 Arte France, German company Neue Mediopolis and an advance on receipts from the National Center of Cinematography and the moving image.  The director originally envisioned Portuguese producer Paulo Branco in the role of Joachim, but decided to cast himself only weeks before filming started. Amalric still wore a moustache throughout the film that was based on Brancos facial hair. 

Filming started in April 2009 and lasted two months.     The troupe went on an actual tour along French port cities in order to provide the necessary footage. Hundreds of local extras performed as themselves as audiences.  Locations were used in Le Havre, La Rochelle, Nantes, Rochefort, Charente-Maritime|Rochefort, and Paris.  For the visual style Amalric drew inspiration from American cinema of the 1970s, and in particular The Killing of a Chinese Bookie by John Cassavetes. 

==Release==
 
On Tour premiered on 13 May at the 2010 Cannes Film Festival as the first film to be screened in the main competition.  At the press conference following the screening, Amalric talked about how excited the performers were about the festival: "It’s the opposite of what happens in our movie. In the story, they were promised Paris, and got nothing. But in this story, they were promised nothing, and they’re all at Cannes". 

The French theatrical premiere followed on 30 June through Le Pacte, who launched it on 159 screens.  On Tour had an attendance of 172,154 during the first week and thereby entered the French box office chart at number five.  One week later the number of prints had been increased to 275 and the film climbed one position.  After two months at the box office, the film had a total of 479,000 admissions in France.  The U.S. premiere is scheduled for 5 May 2011 as the closing night film of the 54th San Francisco International Film Festival.

==Reception==
The immediate reception in Cannes was somewhat mixed. In   summarized his verdict in The Hollywood Reporter. 
 Best Director FIPRESCI Award for best film in the main competition.  The FIPRESCI Award is handed out by an international group of film critics, and with the mixed response from festival reviews, Variety (magazine)|Variety wrote that the win came as a surprise: "  ranked second to last through Thursday on Cannes main critics poll." 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 