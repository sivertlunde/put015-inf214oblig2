Old Mother Riley Overseas
{{Infobox film
| name           = Old Mother Riley Overseas
| image          = "Old_Mother_Riley_Overseas"_(1943).jpg
| image_size     =
| caption        = ABC cinemas British movie card
| director       = Oswald Mitchell
| producer       = Oswald Mitchell
| writer         = H. Fowler Mear Arthur Lucan (additional scenes) 
| story          = L.S. Deacon Albert Mee
| narrator       = 
| starring       = Arthur Lucan   Kitty McShane   Morris Harvey 
| music          = Percival Mackey Gerald Gibbs
| editing        = Lito Carruthers
| studio         = British National Films
| distributor    = Anglo-American Film Corporation
| released       = 
| runtime        = 80 minutes	
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Anthony Holles.  Old Mother Riley relocates to Portugal.

==Plot==
Mother Riley is tricked out of her licence for a pub, and heads for Portugal to find her daughter who is working in the wine business. Along the way she is somehow mistaken for a famous pianist, but arrives in Portugal just in time to prevent her daughter from being kidnapped. She also manages to retrieve stolen port wine.  

==Cast==
* Arthur Lucan ...  Mrs. Riley
* Kitty McShane ...  Kitty Riley
* Morris Harvey ...  Barnacle Bill
* Fred Kitchen ...  Pedro Quentos Sebastian Cabot ...  Bar Steward
* Magda Kun Anthony Holles
* Ferdy Mayne
* Freddie William Breach
* Bob Lloyd
* Paul Erickson
* Eda Bell
* Ruth Meredith

==Critical reception==
TV Guide said, "following the successful formula of Old Mother Riley in Paris, this less impressive series entry has Lucan traveling to Portugal."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 