No Room for the Groom
{{Infobox film
| name           = No Room for the Groom
| image          = No Room for the Groom.jpg
| image size     =
| caption        = 
| director       = Douglas Sirk
| producer       = 
| executive producer = 
| writer         =  starring        = Tony Curtis Piper Laurie Don DeFore
| music          = 
| cinematography = 
| editing        = 
| distributor    = Universal Pictures
| released       = June 13, 1952
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    =
| followed by    =
}}
No Room for the Groom is a 1952 comedy directed by Douglas Sirk, starring Tony Curtis and Piper Laurie.  The screenplay is based on the novel "My True Love" by Darwin Teilhet.

==Plot==
Vineyard owner Alvah Morrell and his girl Lee Kingshead elope to Las Vegas before he must return to active military duty. They are unable to have a honeymoon because Alvah comes down with a case of chicken pox and Lee must be quarantined from him.

Alvah leaves for 10 months. During this time, Lee finds no suitable way or time to tell her manipulative mother about the marriage. Mama pretends to have fainting spells and hides her personal foibles, which include smoking and gambling.

Mamas goal is to marry Lee to the wealthy Herman Stouple, who owns a thriving cement business. Mama hopes to keep the married couple apart so that their union will never be consummated and can be legally annulled.

By the time Alvah returns, so many people are pressuring him to sell his vineyard land and home to Herman that he feels alone, particularly when others conspire to have Alvah declared mentally ill and unable to conduct his own affairs. He must trust Lee to do the right thing, and soon theyre finally spending their first night together.

==Cast==
* Tony Curtis as Alvah
* Piper Laurie as Lee
* Don DeFore as Herman
* Spring Byington as Mama

==References==
 

==External links==
*  at IMDB
*  

 

 
 