Narasinha Avatar
{{Infobox film
| name           = Narasinha Avatar 
| image          = 
| image_size     = 
| caption        = 
| director       = Sohrab Modi
| producer       = Sohrab Modi
| writer         = S. A. Shukla 
| narrator       =  Mahipal Shobhana Samarth Niranjan Sharma  Hari Shivdasani
| music          = Vasant Desai Narendra Sharma  (lyrics) 
| cinematography = Y. D. Sarpotdar 
| editing        = 
| distributor    =
| studio         = Minerva Movietone
| released       = 7 October 1949
| runtime        = 148 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1949 Hindi mythological film produced and directed by Sohrab Modi.    
Made under the Minerva Movietone banner, it had music composed by Vasant Desai with lyrics by Narendra Sharma.   The story was written by S. A. Shukla, and the film starred Mahipal (actor)|Mahipal, Shobhana Samarth, Niranjan Sharma, Hari Shivdasani, Tiwari (actor)|Tiwari, S. N. Tripathi and Kamal. 

The film is based on the Bhagavata Purana story of the fourth Avatar of the Hindu God Vishnu (Dashavatara#Popular list|Dashavatara) in the form of Narsimha. The half man, half lion Narsimha, destroys the Asura King Hiranyakashipu and instates his devotional son Prahlad as the rightful king.   

==Plot==
The Asura King Hiranyakashipu, having been granted the boon of immortality by Brahma, projects himself as Supreme Being, even beyond Vishnu, whom he wants to avenge for killing his brother Hirnayaksha. After years of penance Brahma grants Hiranyakashipu his boon where he asks not to be killed by man or animal, on earth or space, inside the house or outside, and neither at dawn or at night.  In his arrogance he aims for filicide when his son Prahlada rejects him as the supreme power, worshipping only Vishnu as the true God. He makes several attempts at killing Prahlad, one of them by burning him on the pyre. Prahlad is inadvertently saved when Holikas, (sister of Hiranyakashipu) fire-proof veil lands on him and Holika gets burnt instead. Hiranyakashipu is further angered when Prahlad, on being asked if Vishnu exists in the pillar of the palace too, answers He is everywhere. Hiranyakashipu smashes the pillar and a half man half lion form appears. It is Narsimha (Vishnu’s Avatar). The enraged Narsimha stands on the threshold and places the King on his thighs. It is twilight time indicating neither day nor night making Hiranyakashipu no longer invincible. With his talons he rips Hiranyakashipu’s abdomen apart, thereby killing him. Before leaving he installs Prahlad as the new King.

==Cast== Mahipal as Narad
* Shobhana Samarth as Kayadhu
* Niranjan Sharma as Hiranyakashipu
* Hari Shivdasani
* S. N. Tripathi
* Lakshman Tiwari
* Kanta Kumari
* Kamal

==Review And Remakes==
Amrit Gangar in his book states that the film was hailed as a "mass entertainer" according to Filmindia 1949. He cites another reviewer of the time who pointed out the discrepancy in Holikas name, quoting H. H. Wilson (1840) that according to the Puranas, Hiranyakashipus sisters name was Sinhika, and she was married to Viprachitta.   
 Bhakta Prahlad Telugu version cited as the best remake.   

==Soundtrack==
The music direction was by Vasant Desai with lyrics by Narendra Sharma. Lalita Deulkar, Lata Mangeshkar, Mohammed Rafi and Manna Dey provided the main playback singing. 
===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Hari Ko Bisaro Na, Hari Na Bisarenge
| Lata Mangeshkar
|-
| 2
| Charan Tumhare Phul Hamare 
| Lata Mangeshkar
|-
| 3
| Jagmag Taare Jhalke 
| Lata Mangeshkar
|-
| 4
| Bhaj Re Madhur Madhur Naam 
| Lata Mangeshkar, Manna Dey
|-
| 5
| Binti Suno Meri Door Hai Naino Ka Tara 
| Lalita Devulkar
|-
| 6
| Saaj Solah Kalao Se Chand Uga
| Lalita Devulkar
|-
| 7
| Bhajo Vishnu Naam Khade Hari Mandir Dware 
| Lata Mamgeshkar, Mohammed Rafi
|-
| 8
| Hey Mangalmay Bhagwan Karo Tum Sab Jag Ka Kalyan
| 
|-
| 9
| Narayan Jaago Jaago Karuna Nidhaan Jaago 
| Mohammed Rafi
|-
| 10
| Sevak Inka Devendra Indra 
|
|-
| 11
| Latpat Ke Pat Pehne Vikraal
| Lata Mangeshkar, Manna Dey
|}

==References==
 

==External links==
* 

 

 
 
 
 
 