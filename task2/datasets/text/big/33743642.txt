Afghan Luke
{{Infobox film
| name           = Afghan Luke
| image          = AfghanLuke2011Poster.jpg
| alt            =  
| caption        = Coming soon poster
| director       = Mike Clattenburg
| producer       = Barrie Dunn Michael Volpe Mike Clattenburg
| writer         = Patrick Graham Douglas Bell Mike Clattenburg Barrie Dunn
| starring       = Nick Stahl Nicolas Wright Stephen Lobo Vik Sahay
| music          = Blain Morris
| cinematography = Jeremy Benning
| editing        = Roger Matiussi
| studio         = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}

Afghan Luke is a 2011 Canadian film directed by Mike Clattenburg.  The central character, Luke Benning (Nick Stahl), is a journalist investigating the possible mutilation (by Canadian snipers) of corpses in Afghanistan, a country that appears increasingly incomprehensible and surreal as Luke undergoes a series of bizarre adventures.

==Plot==
Disheartened when his story about Canadian snipers possibly mutilating corpses in Afghanistan is buried, Luke (Nick Stahl) quits his job but is even more determined to return to Afghanistan to get the real story. With his offbeat buddy, Tom (Nicolas Wright), tagging along, Luke returns to Afghanistan and intends to gather enough evidence to get his old story into print. But he soon finds that the country is an even more dangerous place than when he left. To make matters worse, his old friend and fixer, Mateen (Stephen Lobo) has been hired away by Lukes journalistic nemesis, Imran Sahar (Vik Sahay). Soon the trip for Luke and Tom in Afghanistan turns into a surreal and perilous adventure, a journey into an alternate reality, filtered through a haze of gun smoke. They encounter Taliban raids, bombings and unfinished business with an Afghanistan businessman. Luke still makes his way through the wilderness with Canadian troops and Arabian guides to find out if his story is true or not. In the end, he realises that the rumour about Canadians mutilating fingers is a lie and that his people still have some morality in this war torn land. Just as he is about to leave, He and Sgt. Rick (a Canadian soldier who is the leader of the squad with whom he had been travelling) come under fire from a Taliban sniper, but the Taliban runs out of bullets and walks off, while Luke thinks he killed him. While they move back to the base, Luke runs into Matteen, where he finds out that they really do share a friendship. The last scene is where Luke decides to go home. 

==Cast==
*Nick Stahl as Luke
*Nicolas Wright as Tom
*Stephen Lobo as Mateen
*Vik Sahay as Imran Sahar
*Pascale Hutton as Elita 
*Steve Cochrane as Sgt. Rick Cahoon
*Torrance Coombs as Pvt. Davey 
*Ali Liebert as Miss Freedom
*Colin  Cunningham as Lt. Christer
*Parm Soor as  Ustad Mir
*Lewis Black as Lewis Black

==Nomination==
The film was nominated for a Writers Guild of Canada 2012 Screenwriting Award  and has appeared at the Shanghai International Film Festival,  the Toronto Film Festival,  Cinéfest,  and the Atlantic Film Festival.   

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 

 
 
 
 
 
 
 