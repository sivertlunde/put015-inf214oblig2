Go Fly a Kit
 
{{Infobox Hollywood cartoon
| series = Looney Tunes 
| image = 
| caption = 
| director = Chuck Jones
| story_artist = Michael Maltese Richard Thompson 
| voice_actor = Mel Blanc (uncredited) Daws Butler (uncredited)
| musician = Milt Franklyn
| producer = 
| studio = Warner Bros. Cartoons
| distributor = Warner Bros. Pictures
| release_date = February 23, 1957 (USA)
| color_process = Technicolor
| runtime =
| movie_language = English
}}
Go Fly a Kit is a 1957  . The title is a pun on the phrase "Go fly a kite."

== Plot ==
At the airport, a business man notices a red cat seemingly waiting for something or someone. When he asks the steward, he tells the story of a flying cat. As a kitten, he was adopted by a mother eagle. He quickly learned to fly and had to say goodbye to his mother like all eagles. One day, he noticed a bulldog chasing a female cat. He saved her from the dog, using his ability to fly. The cats fall in love with each other. Every winter, the flying cat flies south. At the end, he returns to his girlfriend, along with their flying kittens (two red and two black).

==Note==
Some people confuse Hector, who plays the antagonist, with Marc Anthony. In other cartoons, Marc Anthony is Pussyfoots acting guardian.

== Availability ==
*Go Fly a Kit is available on  , Disc 4.

 
 
 
 
 
 


 