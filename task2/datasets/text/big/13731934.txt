Buffalo Bill (film)
 
{{Infobox film
| name           = Buffalo Bill
| image          =
| caption        =
| director       = William A. Wellman
| producer       = Harry Sherman Darryl F. Zanuck John Larkin (uncredited) Thomas Mitchell
| music          = David Buttolph
| cinematography = Leon Shamroy James B. Clark
| distributor    = Twentieth Century Fox Film Corporation
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 biographical western Western about the life of the legendary Buffalo Bill|frontiersman, starring Joel McCrea and Maureen OHara with Linda Darnell and Anthony Quinn in supporting roles.

==Plot== William F. "Buffalo Bill" Cody, a hunter and Army Scout who rescues a US Senator and his beautiful daughter, Louisa Frederici; Federici eventually becomes his devoted wife. Cody is portrayed as someone who admires and respects the Native Americans|Indians.  He is a good friend of Yellow Hand, who will eventually become Chief of the Cheyenne. Public opinion is against the Indians, and military leaders, politicians and businessmen are prepared to take their lands and destroy their hunting grounds for their own profit. Cody is eventually forced to fight the Cheyenne on their behalf. He meets a writer, Ned Buntline, whose accounts of Codys exploits make him a sensation in the eastern United States and Europe. He establishes a wild west show that becomes an international sensation. His career as a performer is threatened when he takes a stand against the mistreatment of the Native American population.

==Cast==
*Joel McCrea as Buffalo Bill Cody
*Maureen OHara as Louisa Frederici Cody
*Linda Darnell as Dawn Starlight Thomas Mitchell as Ned Buntline
*Edgar Buchanan as Sergeant Chips McGraw
*Anthony Quinn as Chief Yellow Hand
*Moroni Olsen as Senator Frederici Frank Fenton as Murdo Carvell
*Matt Briggs as General Blazier
*George Lessey as Schyler Vandervere
*Frank Orth as Sherman - Shooting Gallery Owner

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 