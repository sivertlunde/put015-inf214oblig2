My Tutor Friend
{{Infobox film name           = My Tutor Friend image          = My Tutor Friend.jpg caption        = My Tutor Friend poster (2003) producer       = Jang Young-gwon   Lee Seo-yeol   Heo Dae-yeong director       = Kim Kyeong-hyeong writer         = Park Yeon-seon  based on       = Su-wan-ee, My Tutor Friend by Choi Su-wan starring       = Kim Ha-neul Kwon Sang-woo music          = Lee Kyung-sub cinematography = Ji Kil-woong   Na Seung-yong editing        = Ko Im-pyo distributor    = CJ Entertainment released       =   runtime        = 110 minutes country        = South Korea language       = Korean budget         = gross          = $17,703,856 
| film name = {{Film name hangul         =     hanja          =  내기  하기 rr             = Donggabnaegi gwaoehagi mr             = Tonggapnaegi kwaoehagi context        =}}
}} 2003 South Korean romantic comedy film starring Kim Ha-neul and Kwon Sang-woo. Unglamorous Su-wan is a sophomore in college who gets hired to tutor rich troublemaker Ji-hoon, who is repeating his senior year of high school for the third time.

Released on February 7, 2003, the film topped the box office for five consecutive weeks and sold 4,809,871 tickets, making it the third best selling Korean film of 2003.  . Koreanfilm.org. Retrieved 2012-06-04. 

This was Kim Kyeong-hyeongs directorial debut. Park Yeon-seons screenplay was based on a series of stories posted online in 2000 by a real-life English literature major named Choi Su-wan about her experiences in tutoring a high schooler of her same age. 

== Plot ==
This movie is about Kim Ji-hoon (Kwon Sang-woo) and Choi Su-wan (Kim Ha-neul), both 21 years old. Su-wan is in her second year at university while Ji-hoon is still in the process of repeating his senior year of high school. 

Su-wan’s mother works in their family business, selling fried chicken. To help out, Su-wan earns money by tutoring other children, but often gets fired after hitting the students. Her mother has a rich friend with a son in need of a tutor. The friend, Kim Ji-hoon’s mother, wants a tutor that will make her son study so that he can pass his high school graduation exams. The salary offered is enough to pay for an entire semester of Su-wan’s college education. 

Ji-hoon is always able to attract the girls and draw attention through his love of fighting. He has recently transferred to Surim High School, and manages to offend Lee Jong-soo (Gong Yoo), "king" and best fighter there. Jong-soos girlfriend, Yang Ho-kyung (Kim Ji-woo) also sets her eye on him. He continues to brawl at Surim, including beating up a member of the high school gang for stealing from his younger brother Se-hoon. Apart from studying, Ji-hoon is actually fairly well rounded. As a child, he was sent to America to study. Because of his parents decision, the relationship between them and Ji-hoon is strained. Ji-hoon’s father, Kim Bong-man threatens to send him back to the US if he continues to get into trouble with his fighting and bad grades. To avoid being sent back to America, Ji-hoon needs to get at least a 50% on the upcoming mid-term exams. The only problem is that his usual average mark is much lower than that. 

Su-wan is persistent in tutoring Ji-hoon despite his rudeness to her, and eventually their relationship improves. At the end of the term, Ji-hoon receives a barely passing grade (50.1), but passing nonetheless. According to the terms of a bet they made, Su-wan must dance in front of a crowd during a college festival. While embarrassed and shy at first, she eventually really gets into dancing and the audience cheers her on. Ji-hoon realizes his growing feelings for Su-wan when he gets jealous and angry at seeing all the guys watching her running up to the stage. 

Then Su-wan’s boyfriend comes back home. While spying on the couple, Ji-hoon learns that Su-wan’s boyfriend is breaking up with her. The next day, Ji-hoon shows up at his house and beats him up for hurting her. However, he finds out that the reason he was leaving Su-wan was because he was entering the priesthood. After the ordination, Ji-hoon lets Su-wan vent her feelings by hitting him for a while. Once she settles down, he decides to take her sky sailing. While in the air, Ji-hoon tells Su-wan that he likes her. Su-wan does not know how to respond and pretends that she couldnt hear him over the earpiece. Later on, they go to an amusement park. Just as Ji-hoon is about to give Su-wan a present, Su-wan runs into a female college friend, whos accompanied by her boyfriend. Su-wan had previously confided in the friend about her difficulties in tutoring Ji-hoon. Su-wan’s friend initially thinks that Ji-hoon is her boyfriend, but Su-wan denies it, saying she would never go out with a high school student. This angers Ji-hoon and he throws her present into the fountain and leaves.
 gang members confront him while holding Su-wan hostage. Ji-hoon and Su-wan manage to run away, but they eventually get cornered and Ji-hoon has to fight everyone on the beach. Once all of the other gang members are down, Ji-hoon faces the mafias best fighter. Ji-hoon is at the man’s mercy, but Su-wan saves Ji-hoon by kicking the man in the groin. Then Su-wan and Ji-hoon drive away on her chicken delivery scooter, with Su-wan in the drivers seat and Ji-hoon behind her, his arms around her waist. At the end, Ji-hoon and Su-wan are now a couple, and she is tutoring his younger brother Se-hoon.

== Cast ==
* Kim Ha-neul - Choi Su-wan
:A sophomore at college who comes from a modest, lower-middle-class family that owns a fried chicken restaurant. To cover her expenses, she works as a private tutor. She is hired to tutor Ji-hoon in order for him to complete his high school requirements and graduate.
* Kwon Sang-woo - Kim Ji-hoon
:A wealthy and troubled young man who is repeating his senior year of high school for the third time.
* Gong Yoo - Lee Jong-soo
:Formerly the "best fighter" at Surim High School, until Ji-hoon came along.
* Kim Ji-woo - Yang Ho-kyung
:The most popular girl at Surim, who transfers her affections from Jong-soo to Ji-hoon.
* Baek Il-seob - Kim Bong-man, Ji-hoons father
: He sent his son to America as a child and now threatens to send him back there if he does not turn around his bad behavior and begin to get good grades.
* Kim Ja-ok - Su-wans mother, who runs a fried chicken restaurant.
* Kim Hye-ok - Ji-hoons mother
* Oh Seung-geun - Su-wans father
* Seo Dong-won - Chang-hee, high school minion
* Park Hyo-jun - Hyeok-jae, high school minion
* Jung Woo - thug
* Lee Seung-won - Su-wans first love who becomes a priest
* Uhm Seong-mo - Kim Se-hoon, Ji-hoons younger brother

== Source material ==
In 1999, Choi Su-wan was a 19-year-old college student majoring in English and Korean languages, when she agreed to tutor a rich delinquent her own age named Kim Ji-hoon. She wrote about her experiences in June 2000, posting on a community website in a series of twenty installments.  Titled Su-wan-ee, My Tutor Friend, it attracted a large number of page views, with each installment viewed by an average of 15,000 netizens. The series was then featured in www.puha.co.kr, a website specializing in web comics. Due to its popularity, it first appeared in print in the monthly issues of a local magazine, under the title He and I written by Shim Hye-jin. This was then published in one volume in 2001 by Daewon C.I. That same year, film company Corea Entertainment purchased the rights, and screenwriter Park Yeon-seon adapted it into a screenplay.

== Critical reception == 
Comparisons were made to My Sassy Girl (2001), another successful romantic comedy about young people who came from different backgrounds, that was originally serialized on the Internet.  Though criticized for its over-long second half and using predictable rom-com tropes,  the film was praised for its witty dialogue and avoiding melodrama, and the chemistry and charismatic performances of its two lead actors.   

== Spin-off == Korean to a Koreans in Japan|Korean-Japanese exchange student (Lee Chung-ah). 

== References ==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 