Roundhay Garden Scene
{{Infobox film
| name           = Roundhay Garden Scene
| image          = Roundhay Garden Scene.ogg
| caption        = The worlds earliest surviving motion-picture film
| director       = Louis Le Prince
| producer       = 
| writer         = 
| narrator       = 
| starring       = {{Plainlist |
* Harriet Hartley
* Adolphe Le Prince
* Joseph Whitley
* Sarah Whitley
}}
| music          = 
| cinematography = Louis Le Prince
| editing        = Louis Le Prince
| distributor    = 
| released       =  
| runtime        = 2.11 seconds
| country        = United Kingdom France
| language       = Silent
}}
 short silent frames per oldest surviving Guinness Book of Records. 

==Overview==
According to Le Princes son, Adolphe, the film was made at Oakwood Grange, the home of Joseph and Sarah Whitley, in Roundhay, Leeds, West Riding of Yorkshire , on October 14, 1888.   Roundhay Garden Scene 

It features Adolphe Le Prince,  Sarah Whitley, Joseph Whitley and Harriet Hartley in the garden, walking around. Note that Sarah is walking backwards as she turns around, and that Josephs coat tails are flying as he also is turning.  Sarah Whitley was Le Princes mother-in-law, being the mother of his wife, Elizabeth. Sarah Whitley died ten days after the scene was taken.

==Remastered footage== National Science Eastman Kodak paper base photographic film through Le Princes Louis Le Prince#LePrince Cine Camera-Projector types|single-lens combi camera-projector. Le Princes son, Adolphe, stated that the Roundhay Garden movie was shot at 12 frame rate|frames/s (and the second movie, Traffic Crossing Leeds Bridge, at 20 frames/s), however the later digital remastered version of Roundhay Garden produced by the National Media Museum, Bradford, comprises 52 frames and is only 2.11 seconds long, as the film runs at 24.64 frames/s, the modern cinematographic frame-rate. The National Science Museum copy has 20 frames; at 12 frames/s, this would produce a run time of 1.66 seconds.

== References ==
 

== External links ==
 
*  
*  
*   on YouTube
*   University of Leeds. (The University is near to the site of Le Princes former workshop which was located at the junction of Woodhouse Lane and Blackman Lane).
*  . Details of memorial for Sarah (died October 24, 1888) and Joseph Whitley (died January 12, 1891) at Beechwood, Roundhay, Leeds. ( ),  

 
 
 
 
 
 
 
 
 