War of the Worlds 2: The Next Wave
{{Infobox film
| name           = War of the Worlds 2: The Next Wave
| image          = War_of_the_Worlds_2-_The_Next_Wave.jpg
| caption        = DVD cover
| director       = C. Thomas Howell
| producer       = David Michael Latt, David Rimawi, Paul Bales
| screenplay     = Eric Forsberg
| story          = Steve Bevilacqua
| based on       = The War of the Worlds by H. G. Wells
| narrator       = C. Thomas Howell
| starring       = C. Thomas Howell Christopher "Kid" Reid  Kim Little  Fred Griffith  Dashiell Howell
| music          = Ralph Rieckermann Mark Atkins
| editing        = Ross H. Martin
| distributor    = The Asylum
| released       =  
| runtime        = 85 minutes
| language       = English
| budget         = $500,000 (estimated)
}}

War of the Worlds 2: The Next Wave is a 2008 direct-to-DVD science fiction film starring and directed by C. Thomas Howell. The film was produced and distributed independently by The Asylum.
 novel and adaptation of Independence Day Battlefield Earth. 
 extraterrestrial invasion aliens attempt alien invaders while trying to rescue his abducted son, while American pilots take the battle to the aliens homeworld, Mars.

== Plot ==
 aliens were Tripods land in the city. People are struck by a Heat-Ray. Shackleford takes a sample of Sissys blood, with which he injects himself.

In Washington, American society has not recovered from the invasion. George Herbert recognises a familiar disturbance on the radio, as the same heard during the first invasion and he reveals to Major Kramer and a team of scientists that his studies show that the aliens are creating a wormhole between Earth and Mars for another wave of attacks. A fleet of fighter jets, which appear to have deep-space flight capabilities, thereby raid the planet Mars. George returns home for his son Alex, only to find a Tripod outside his home, which abducts Alex. He escapes to an abandoned city and wakes the next morning to find a man named Pete running from a Tripod. George throws himself before the machine, and wakes inside the machine with Pete. Both escape with Sissy, while the Martians begin a second invasion, attacking London and Paris. Major Kramer leads the fleet of jets to chase the alien mothership back to Mars.

George, Pete, and Sissy find themselves in the town from the start of the film; where Shackleford reveals that the town is created by the Tripods for humans captured by Tripods to live on Mars. Shackleford wants to destroy the aliens in the same way bacteria did them in during the first invasion. Shackleford and Sissy are dying of a virus lethal to the Tripods, and he convinces George to inject his infected blood into himself. George and Pete are kidnapped again and arrive inside the mothership, where they find Alex in a cocoon. There, George injects his infected blood into a pod holding a brain telepathically connected to all of the Tripods, and thus deactivates the invasion. George, Pete, and Alex escape just as the mothership explodes. George survives the infection, and the humans celebrate while listening to the radio, which undergoes some static interference, indicating a third invasion, and the characters spend a few moments in silence before the film ends.

==Cast==
*C. Thomas Howell as George Herbert (a play on H.G. Wells first two names, Herbert George) Christopher Reid as Peter Silverman
*Dashiell Howell as Alex Herbert Fred Griffith as Major Kramer

==Similarities with the novel==
War of the Worlds 2 is notably different in tone to its predecessor. While War of the Worlds kept relatively faithful to the novel with a focus on realism and humanity rather than the aliens themselves, War of the Worlds 2 features considerably more outlandish science-fiction elements, discarding the horror treatment of the first film for an action/adventure premise. The films plot is mostly original, its characters entirely so, though it does feature elements from The War of the Worlds which were excluded from the first film.

*Unlike the novel, humans are shown as having considerable effect and competence in the fight against the aliens. The character of the Artilleryman entertains the possibility of taking down and studying a fighting-machine and building new ones with human pilots to repel the invaders. Humans are seen using jets which have been upgraded with the aliens technology to fight against the invaders.
*The first film made effort to disguise the Martian heritage of the aliens (though director David Michael Latt confirmed that the aliens were indeed Martians). In War of the Worlds 2, it has become common-knowledge between events that the aliens originated from Mars - though instead of being launched straight from the planet like in the novel, their ships appear to be transported to near-earth space by a wormhole.
*In the novel, the Martians are seen abducting humans, carrying them and placing them in metal cages carried by the machines themselves. In War of the Worlds 2, the Tripods use a teleportation-gun to place their prisoners inside them.

==Soundtrack==
The films music was composed by Ralph Reickermann, a former composer for The Asylum. The film features the single "You Came into my Life" which featured the vocals of singer John Brown Reese.

== External links ==
* 
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 