Psychopathia Sexualis (film)
 
{{Infobox film
| name = Psychopathia Sexualis
| image = 
| alt = 
| caption = 
| director = Bret Wood
| producer = Tracy Martin
| writer = Bret Wood
| starring = Ted Manson
| music = Paul Mercer
| cinematography = David Bruckner
| editing = Bret Wood
| studio = Illustrated Films Kino International Gravitas Ventures
| released =  
| runtime = 98 minutes
| country = United States
| language = English
| budget = 
| gross = $4,012 
}} vignettes are sexual perversity study of Richard von Krafft-Ebing, who is portrayed in the film by Ted Manson.

==Cast==
* Ted Manson as Professor Richard von Krafft-Ebing
* Kristi Casey as Blood woman
* David Weber as Blood man
* Zoë Cooper as Shepherdess
* Patrick L. Parker as Emil Fourquet
* Daniel Thomas May as J.H.
* Patricia French as Antoinette
* Daniel Pettrow as Xavier

==Production==
Psychopathia Sexualis was shot in Atlanta|Atlanta, Georgia. 

==Release==
The film was given a limited release on June 8, 2006,  opening in three North American theaters. It grossed $1,612 in its opening weekend, averaging $537 per theater,  and, by the end of its four-week run, made $4,012.   

==Critical reception==
Rotten Tomatoes, a review aggregator, reports that 23% of 13 surveyed critics gave the film a positive review.  On Metacritic, the film has an assigned 40/100 rating based on 10 critics, indicating "mixed or average reviews". 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 