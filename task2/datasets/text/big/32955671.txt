My Friend Pinto
 
 
{{Infobox film
| name           = My Friend Pinto
| image          = My Friend Pinto.jpg
| caption        = Theatrical release poster
| director       = Raaghav Dar
| producer       = Sanjay Leela Bhansali Ronnie Screwvala
| story          = Raaghav Dar
| screenplay     = Raaghav Dar
| starring       = Prateik Babbar Kalki Koechlin Arjun Mathur Shruti Seth Makrand Deshpande Raj Zutshi Divya Dutta
| music          = Ajay-Atul
| editing        = Shan Mohammed Dipika Kalra
| distributor    = SLB Films
| released       =  
| gross          =  
| budget         =  
| country        = India
| language       = Hindi
}}
My Friend Pinto is a 2011 Bollywood film shot directed by Raaghav Dar and produced by Sanjay Leela Bhansali and starring Prateik Babbar, Kalki Koechlin, Arjun Mathur, Shruti Seth, Makrand Deshpande, Raj Zutshi and Divya Dutta. The film is also produced by Ronnie Screwvala . The film was released on 14 October 2011.  

==Synopsis==

Twenty-something Michael Pinto (Prateik) has grown up in a small Goan village, believing everyone in the world to be simple, kind and honest, just like him. His world revolves around a doting mother, a passion for music and the memories of a childhood friend, Sameer, who left Goa for Mumbai many years ago and hasnt replied to his frequent letters ever since. Pintos talent for music is only matched by his knack for finding trouble in the unlikeliest of places often resulting in the most outlandish and hilarious situations.

When Pintos idyllic world is shattered by the sudden demise of his mother he decides to head to Mumbai in search of the only friend he knows. Will Pinto find Sameer or will trouble find him first? Will Pinto and his crazy antics survive the big bad city or more importantly, will Mumbai survive a phenomenon called Pinto?

==Cast==
* Prateik Babbar as Michael Pinto
* Kalki Koechlin as Maggie
* Arjun Mathur as Sameer
* Shruti Seth as Suhani
* Makrand Deshpande as Don
* Raj Zutshi
* Divya Dutta as Reshma
*  
==References==
 

==External links==
*  
*   at Bollywood Hungama
*   at Hindustan Times

 

 
 
 

 