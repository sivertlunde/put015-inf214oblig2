New Rose Hotel (film)
{{Infobox film
| name = New Rose Hotel
| image = New rose hotel-dvd.jpg
| image_size =
| caption = DVD cover for New Rose Hotel
| director = Abel Ferrara
| producer = Edward R. Pressman
| writer = Abel Ferrara Christ Zois
| based on =   William Gibson
| starring = Christopher Walken Willem Dafoe Asia Argento
| music = Schoolly D
| cinematography = Ken Kelsch
| editing = Jim Mol Anthony Redman
| distributor =
| released =  
| runtime = 93 min
| country = United States
| language = English
| budget =
| gross = $21,521
}} director Abel story of the same name, starring Christopher Walken, Willem Dafoe and Asia Argento.

==Plot synopsis==
Fox (Walken) and X (Dafoe) are corporate extraction specialists, half headhunters, half kidnappers, who specialise in helping R&D scientists relocate from corporations who would rather see them dead than working for their competitors. Fox is obsessed with one Hiroshi (played by Final Fantasy artist Yoshitaka Amano), a paradigm-shattering super-genius who is currently working for Maas, the corporation (Gibson employs the pre-WWII term zaibatsu) who crippled him. To that end, Fox and X employ Sandii (Argento), a "Shinjuku-girl", or small-time hustler/call girl, to help "persuade" Hiroshi to defect to Hosaka, another zaibatsu to which Fox is somewhat warmer. Fox is responsible for brokering the deal with Hosaka, Sandii for getting Hiroshi to fall in love with her and defect to a Hosaka lab in Marrakech (Fox and X are based in Tokyo, hence their ability to pick up a Shinjuku girl), and X is responsible for teaching Sandii how to make Hiroshi melt.

Sandii disappears, Fox is killed, and X retreats to the safest place he knows, the New Rose Hotel, a derelict capsule hotel.

==Cast==
*Christopher Walken .... Fox
*Willem Dafoe .... X
*Asia Argento .... Sandii
*Annabella Sciorra .... Madame Rosa
*John Lurie .... Distinguished Man
*Kimmy Suzuki .... Asian Girl #1
*Miou .... Asian Girl #2
*Yoshitaka Amano .... Hiroshi
*Gretchen Mol .... Hiroshis Wife
*Ryuichi Sakamoto .... Hosaka Executive

==External links==
*  
*  
*  
*   on YouTube

 
 

 
 
 
 
 
 
 
 
 


 
 