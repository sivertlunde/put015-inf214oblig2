Song of Old Wyoming
{{Infobox film
| name           = Song of Old Wyoming
| image_size     =
| image	=	Song of Old Wyoming FilmPoster.jpeg
| caption        =
| director       = Robert Emmett Tansey
| producer       = Robert Emmett Tansey
| writer         = Frances Kavanaugh (writer) Robert Emmett Tansey (story)
| narrator       =
| starring       = See below
| music          =
| cinematography = Marcel Le Picard
| editing        = Hugh Winn
| distributor    =
| released       = 12 October 1945
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Western film directed by Robert Emmett Tansey.

== Cast == Eddie Dean as Eddie
*Sarah Padden as Kate Ma Conway
*Ian Keith as Lee Landow
*Lash LaRue as Cheyenne Kid
*Jennifer Holt as Vicky Conway, adopted
*Emmett Lynn as Uncle Ezra Robert Barron as Jesse Dixon
*Gene Alsace as Henchman Ringo
*Don Williams as Cowhand Slim - Musician
*Johnny Carpenter as Cowhand Buck
*Horace Murphy as Editor Timothy Meeks

== Soundtrack ==
* Eddie Dean - "Hills of Old Wyoming" (Written by Ralph Rainger and Leo Robin)
* Eddie Dean - "My Herdin Song" (Written by Eddie Dean and Milt Mabie)
* Eddie Dean - "Wild Prairie Rose" (Written by Eddie Dean and Carl Hoefle)

== External links ==
 
* 
* 

 

 
 
 
 
 
 
 
 


 