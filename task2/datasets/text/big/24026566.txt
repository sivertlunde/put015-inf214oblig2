Don't Be Afraid of the Dark (2010 film)
{{Infobox film
| name           = Dont Be Afraid of the Dark
| image          = Dont be afraid of the dark poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Troy Nixey Mark Johnson   Stephen Jones Matthew Robbins
| based on       =   Jack Thompson Alan Dale Garry McDonald Julia Blake
| music          = Marco Beltrami Buck Sanders
| cinematography = Oliver Stapleton
| editing        =
| studio         = Miramax Necropia Gran Via
| distributor    = FilmDistrict
| released       =  
| runtime        = 99 minutes
| country        = United States Mexico
| language       = English
| budget         = $25 million 
| gross          =  $36,993,168    
}}
 horror film Matthew Robbins ABC television horror film of the same name that starred Kim Darby.   

==Plot==
At Blackwood Manor in Providence County, Rhode Island, renowned wildlife painter Lord Blackwood summons his housekeeper into the basement where he reluctantly kills her with a hammer and chisel. He removes her teeth, as well as his own, and offers them to mysterious creatures down an ash pit within an old fireplace; however, the creatures demand the teeth of children. Blackwood begs for them to give back his kidnapped son, only to be dragged down the ash pit by the creatures.

In the present day, 8-year old Sally Hurst arrives in Rhode Island to live with her father Alex and his girlfriend Kim, both restoring Blackwood Manor to put it on the market for their client Mr. Jacoby. Sally is depressed due to her mother forcefully putting her in Alexs care and giving her copious amounts of Adderall. On the first night of her stay, the melodious tune from a carousel-styled nightlight awakens the creatures in the ash pit. The next day, Sally wanders the grounds and finds the hidden basements skylight. One of the workmen restoring the house, Mr. Harris, warns her, Alex and Kim not to venture into the basement, although they do regardless. Sally takes interest in the sealed fireplace where she hears the creatures calling her name and follows the mysterious voices. "BE AFRAID" is written in runes above it.

Sally opens the fireplace to meet the creatures and finds one of the old housekeepers teeth. The creatures quickly prove to be hostile, stealing Alexs razor and shredding Kims clothes. Alex immediately blames Sally and finds a 19th-century silver coin in her possession, which she found under her pillow after the tooth disappeared. Alex and Kim head into town on a business trip and Sally sneaks to the basement to talk with the creatures, but Harris sends her away and tries to seal the fireplace. The creatures emerge and brutally wound him with his own tools and he is hospitalized. Sallys increasingly frightening encounters with the creatures prompt Alex to call a therapist to talk to Sally, who draws a sketch of one of the creatures that attacked her under her bedsheets.
 tooth fairies, which every now and again turns a human into one of their own. Kim races home as Sally is attacked again by the creatures while having a bath, the lead creature being a transformed Lord Blackwood who proclaims the creatures will make Sally one of their own. Kim finds an undiscovered mural painted by Lord Blackwood in the basement, depicting his son being taken below ground by the creatures. Kim confronts Alex who is more interested in hosting a dinner for Mr. Jacoby and friends. However, he finally realizes what is happening when Sally is trapped in the library by the creatures, but she fends them off by using her camera flash to distract them.

Alex and Kim decide to flee the house with Sally, but both are ambushed by the creatures and knocked out, Sally tries to wake Kim up but also gets ambushed by the creatures and is knocked unconscious. When Sally wakes up, her feet have been tied up with rope, and the creatures are starting to drag her to the basement for her transformation. Kim awakens and goes to basement confronting the creatures, cutting the rope around Sallys feet but only to get herself in the ropes and her leg broken by it as she struggles to get free. The creatures drag Kim into the fireplace, as a distraught Sally crushes the creature who used to be Lord Blackwood to death with a large flashlight. Alex arrives just as Kim disappears, and the father and daughter mourn their loss.

Some time later, both return to the abandoned mansion to leave a drawing of Kim there, but a gust of wind blows the drawing into the creatures lair, where the transformed Kim is heard convincing the creatures to stay where they are and just go deeper into the basement – for they will forget in time, and others will come – claiming they have "all the time in the world".

==Cast==
* Bailee Madison as Sally Hurst
* Katie Holmes as Kim
* Guy Pearce as Alex Hurst Jack Thompson as William Harris
* Alan Dale as Charles Jacoby
* Julia Blake as Mrs. Underhill Garry McDonald as Emerson Blackwood
* Nicholas Bell as Psychiatrist
* Trudy Hellier as Evelyn Jacoby James Mackay as Librarian
* Terry Kenwrick as Bill
* Emelia Burns as Caterer
* Eddie Ritchard as Housekeeper
* Libby Gott as Nurse
* Lance Drisdale as Policeman
* Carolyn Shakespeare-Allen as Airport Cart Driver
* Bruce Gleeson as Buggy Driver
* David Tocci as Workman
* Abbe Holmes as Joanne Hurst (Voice)
* Grant Piro, Todd Macdonald, Angus Smallwood, Dylan Young and Guillermo del Toro as The Creature Voices

==Production==
Del Toro chose Troy Nixey to direct the film after seeing Nixeys short film Latchkeys Lament. For the design of the creatures in the film, Nixey drew inspiration from pictures of mole rats. 

===Influences ===
Del Toro has attributed the idea of giving the creatures in the film a fairy origin to the work of the writer   (Pans Labyrinth) and   both of which also feature fairy creatures.  The name of "Emerson Blackwood", the character who built the mansion in the film, is a tribute to Algernon Blackwood, another writer of supernatural horror stories.

==Release==
This picture, which was developed with Miramax in the wake of the divisions closure and sale, was released by FilmDistrict, and was rated R despite filmmaker ambitions to the contrary.  "We originally thought we could shoot it as PG-13 without compromising the scares," Del Toro said.  "And then the MPAA came back and gave us a badge of honor. They gave us an R for Violence and Terror. We asked them if there was anything we could do, and they said, Why ruin a perfectly scary movie?" 
 2010 San Diego Comic-Con International.   
The initial release date was scheduled for January 21, 2011.   Due to the sale of Miramax by Disney on December 3, 2010 (but Disney kept the pre-2010 Miramax Films library), the release was put on hold until the sale was finalized.  The film was released on August 26, 2011. 

==Home media==
It was released on DVD and Blu-ray on January 3, 2012 and February 20, 2012 in the UK.

==Reception== weighted average score out of 100 to reviews from mainstream critics, calculated an average score of 56/100 based on 35 reviews, indicating "mixed or average reviews".  Roger Ebert of the Chicago Sun-Times gave the film 3½ stars out of 4, calling it "a very good haunted house film" and adding that it "milks our frustration deliciously."  Bailee Madisons acting was generally well received by critics, who have also praised the idea of turning the protagonist into a little girl, as opposed to an adult in the original film.

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 