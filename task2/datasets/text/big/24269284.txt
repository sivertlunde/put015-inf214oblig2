The Memory Thief
{{Infobox Film
| name           = The Memory Thief
| image          = The Memory Thief.jpg
| image_size     = 
| caption        = 
| director       = Gil Kofman
| producer       = Gil Kofman Marika Van Adelsburg Amy Ziering
| writer         = Gil Kofman
| narrator       =  Mark Webber Rachel Miner Jerry Adler Allan Rich Peter Jacobson Douglas Spain
| music          = Ted Reichman
| cinematography = Richard Rutkowski
| editing        = Curtiss Clayton
| distributor    = Seventh Art Releasing
| released       =   (Cambridge Film Festival): 12 July 2007
| runtime        = 95 minutes
| country        = United States 
| language       = English
| budget         = 
| gross          = 
}} American Independent independent Digital DV drama Mark Webber and Rachel Miner in the leading roles. The film chronicles the experiences of a young man who becomes involved in documenting the experiences of survivors of the Holocaust, as his commitment turns into obsession and madness. Critics were generally favourable of the movie, which was Kofmans debut as a feature director.

==Plot== Mark Webber) concentration camps. Watching the tape, Lukas becomes captivated, not less so when he spots the old mans obituary in the newspaper, and decides to attend the funeral. He is confronted by Mira (Rachel Miner), a young medical student, for attending a funeral without knowing the deceased. They argue. Lukas shows her the witness tape.

While visiting his mother in the hospital, he again meets the medical student Mira, whose father (Jerry Adler) is also a camp survivor. Lukas asks Mira to go out with him. She agrees, but is upset by his obsession with holocaust memories, especially since he is not Jewish. She wonders if he is a voyeur. He insists the memories are important. She asks him about his own childhood, and Lukas replies that he cannot remember one good memory from his own childhood.

  identification numbers from the camps. He writes long letters to the filmmaker Horowitz, who has made a movie about the Holocaust. He gives a pink triangle to his transgender co-worker, Dominique. Eventually he persuades Miras father to record an interview. The Holocaust foundation fires Lukas for doing an interview before he is trained. The burden of recalling the memories is too much for Miras father, who kills himself. Mira blames Lukas for her fathers death, and Lukas is devastated.

Lukas buys his own camera and wanders the streets pushing the camera in peoples faces, asking them if they are Jewish, and telling them that they are lucky to be alive. He demands that all German cars use a different lane at his tollbooth. His erratic behaviour gets him fired. He comes to believe that he himself is the last Holocaust survivor. He shaves his head and gets an identification number tattooed on his arm. He picks a fight with a group of White power skinhead|neo-Nazi skinheads, and is beaten. The woman in the next bed to his mother in the hospital confronts him, accusing him of not being her son at all, and he does not deny it. He gives away his shoes to his co-worker, and dons a home made concentration camp prisoner uniform, as he embarks on, what he describes as, a death march.

==Cast== Mark Webber &ndash; Lukas
*Rachel Miner &ndash; Mira
*Jerry Adler &ndash; Mr. Zweig
*Allan Rich &ndash; Zvi Birnbaum
*Peter Jacobson &ndash; Mr. Freeman
*Douglas Spain &ndash; Dominic
*Stella Hudgens &ndash; Amanda Carlos Gomez &ndash; Amandas father
*Karen Landry &ndash; Mother
*Mary Pat Gleason &ndash; Hospital patient (Mary)
*Richard Riehle &ndash; Judaica store clerk
*Carlucci Weyant &ndash; Chris

== Production ==
The Memory Thief was the first feature film directed by the Nigerian-born playwright Gil Kofman.   
Kofman himself had married into a family of Holocaust survivors, so the topic of the film related to his personal life.   
In the interview sequences shown, the movie makes use of the testimonies of actual Holocaust survivors. 

== Reception ==
The Memory Thief received mostly favorable reviews from critics. The review aggregate website Rotten Tomatoes gave the movie a 79% fresh rating, based on 14 reviews.    Jeannette Catsoulis, writing for The New York Times, called it "a strange and melancholy journey to the heart of madness," and she commended the way the movie presented a counterbalance to formulaic Hollywood movies about the Holocaust.  Maureen M. Hart of the Chicago Tribune wrote that Kofman had "crafted an unusual tale of post-traumatic stress and pain and the ownership thereof," and she singled out for praise Jerry Adlers performance as an old Holocaust survivor.  Leba Hertz, on the other hand, in a review for the San Francisco Chronicle, complained that "something rings false" about the movie. For instance, she had difficulties understanding why a girl like Mira should be attracted to someone such as Lukas.   

== References ==
 

==External links==
* 
* 
* 

 
 
 
 
 