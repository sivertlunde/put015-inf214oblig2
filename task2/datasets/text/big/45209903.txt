Don't Call Me Little Girl
{{Infobox film
| name           = Dont Call Me Little Girl
| image          = Dont Call Me Little Girl (1921) - 1.jpg
| alt            = 
| caption        = Newspaper ad
| director       = Joseph Henabery
| producer       = 
| screenplay     = Catherine Chisholm Cushing Edith M. Kennedy 
| starring       = Mary Miles Minter Winifred Greenwood Ruth Stonehouse Jerome Patrick Edward Flanagan Fanny Midgley
| music          = 
| cinematography = Faxon M. Dean 
| editing        = 
| studio         = Realart Pictures Corporation Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent comedy film directed by Joseph Henabery and written by Catherine Chisholm Cushing and Edith M. Kennedy. The film stars Mary Miles Minter, Winifred Greenwood, Ruth Stonehouse, Jerome Patrick, Edward Flanagan, and Fanny Midgley. The film was released in June 1921 by Paramount Pictures.   It has a survival status classification of unknown,  which suggests that it is a lost film.
 
==Plot==
 

== Cast ==
*Mary Miles Minter as Jerry
*Winifred Greenwood as Harriet Doubleday
*Ruth Stonehouse as Joan Doubleday
*Jerome Patrick as Monty Wade
*Edward Flanagan as Peter Flagg
*Fanny Midgley as Mrs. Doubleday 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 