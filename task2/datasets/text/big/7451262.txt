Baby Be Good
{{Infobox Hollywood cartoon|
| cartoon_name = Baby Be Good
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist =
| animator = Edward Nolan Myron Waldman
| voice_actor = Mae Questel George Steiner (uncredited)
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = January 18, 1935
| color_process = Black-and-white
| runtime = 8 minutes
| movie_language = English
| preceded_by = When My Ship Comes In
| followed_by = Taking the Blame
}}

Baby Be Good is a 1935 Fleischer Studios animated short film starring Betty Boop.   

==Synopsis==
Betty Boop is putting her nephew, Junior, to bed, but he is not ready to sleep. He jumps on the bed then uses a tube of toothpaste to put stripes on the cat. When Betty catches him, she tells him a fairy tale about a naughty boy. The boy in the story ties a can on a tail of a puppy resembling Pudgy the Puppy, scares a hen and prematurely frees her chick, shakes apples off a tree and cuts it down, throws bricks at a greenhouse, knocks clean laundry into the mud, and shaves the head of the barber. When he teases a lion at a nearby circus by taking its bone out of its cage, the lion escapes its cage and the boy has to be rescued by a magic fairy (Betty Boop with fairy wings, wearing a star-covered black evening dress). The fairy rescues the little boy but makes him promise to undo all his mischief. This is done rather easily by the animators reversing the sequence of scenes from the first part of the story. Junior is not certain that he believes the story, but at last he is ready to go to sleep and undoes his mischief with the same reversal of sequences, and hides under the covers in fear, likely because of the lion.

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 


 