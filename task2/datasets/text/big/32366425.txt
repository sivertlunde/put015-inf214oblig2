Veettilekkulla Vazhi
{{Infobox film
| name           = Veettilekkulla Vazhi
| image          = VeettilekullaVazhi.jpg
| caption        = Poster
| director       = Dr. Biju
| producer       = B. C. Joshi
| writer         = Dr. Biju
| based on       =
| starring       = Prithviraj Sukumaran Indrajith Sukumaran Malavika Master Govardhan Dhanya Mary Varghese Kiran Raj Vinay Forrt
| music          = Ramesh Narayan
| cinematography = M. J. Radhakrishnan
| editing        = Manoj Kannoth
| studio         = Soorya Cinema
| distributor    =
| released       =  
| runtime        = 95 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} adventure drama Indrajith and Master Govardhan in the lead roles.
 Best Feature Film in Malayalam at the 58th National Film Awards and the NETPAC Award. It also received accolades at the Zanzibar International Film Festival and Imagine India Film Festival, Spain. The film released in theatres in Kerala on August 5, 2011.

==Plot==
The films story revolves around a doctor (Prithviraj Sukumaran|Prithviraj) with a haunting past. He witnessed his wife (Malavika) and five-year-old son die in an explosion at a market in Delhi.

Now working at a prison hospital, the doctor is assigned the case of a woman in critical condition, a surviving member from a suicide squad of the ‘Indian Jihadi’, a notorious terrorist group.
 
Despite the doctor’s best efforts, the woman dies. But before dying, she entrusts him to find her five-year-old son and unite him with his father. The father is revealed to be Abdul Zuban Tariq, head of the terrorist group.
    
Finding the boy in Kerala, the doctor and child set out on a journey to find his father. The journey follows the contemporary and mysterious path of the terrorist network in the vast country through various Indian states and with many unexpected incidents.
  
Veettilekkulla Vazhi is an adventure drama highlighting human relationships. It emphasizes the path of love, survival, innocence and humanity, exploring a bloodstained facet of contemporary terrorism in India. The film is a travelogue through the most beautiful landscapes of India.

==Cast==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Actor !! Role !! Notes
|- Prithviraj || Doctor  || 
|-
| Master Govardhan || Kid  ||
|-
| Malavika || Wife of doctor  || 
|- Indrajith || Rassaq || Tamil Terrorist
|- Irshad || Abdulla  || 
|-
| Vinay Fortt || Terrorist Leader in Ajmeer  || 
|-
| S.Saji || Malayali Terrorist  || 
|-
| Melwyn || Sardarji Truck Driver  || 
|-
| Kiran Raj || Najeem  || 
|-
| Azim || Terrorist Leader at Kashmir ||
|-
| Lakshmipriya || Rashida || Kids mother  
|-
| Dhanya Mary Varghese || Teacher  || 
|}

==Production==
Veettilekkulla Vazhi is the third directorial venture of Dr. Biju whose previous films are Saira (film)|Saira and Raman. Veettilekkulla Vazhi is an adventure drama that wants to explore the bloodstained facets of present-day terrorism. Produced by B. C. Joshi under the banner of Soorya Cinema, the film was mainly shot from Ladakh, Kashmir, Jaisalmer, Jodhpur, Bikkneer, Ajmer, Pushkarand, Delhi and Kerala. 

==Festival screenings==
The film was screened at 28 international film festivals including: 
* 12th Mumbai Film Festival (Mumbai, India; 2010) as the opening film in the Indian Frames section.
* 34th Cairo International Film Festival (Egypt; 2010) in the Out of Competition section.
* 15th International Film Festival of Kerala (Trivandrum, India; 2010)
* 3rd Jaipur International Film Festival (Jaipur, India; 2011)
* 5th Chennai International Film Festival (Chennai, India; 2011)
* 10th Imagine India International Film Festival (Madrid, Spain; 2011) in competition section.
* 11th New York Indian Film Festival (New York, USA; 2011) in competition section
* Zanzibar International Film Festival (Zanzibar, Tanzania; 2011)
* London Indian Film Festival (London, UK; 2011)
* Bollywood and Beyond Film Festival (Germany; 2011)
* 38th Telluride Film Festival (Telluride, USA; 2011)
* New Generation Film Festival (Frankfurt, Germany; 2011)
* Third Eye Asian Film Festival (Mumbai, India; 2011)
* Seattle Film Festival (Seattle, USA; 2011)

==Accolades==
{| class="wikitable" style="font-size: 95%;"
|- align="center"
! style="background:#B57EDC;" | Award
! style="background:#B57EDC;" | Ceremony
! style="background:#B57EDC;" | Category
! style="background:#B57EDC;" | Recipients and nominees
! style="background:#B57EDC;" | Outcome
|-
| rowspan="3"| Imagine India Film Festival  
| rowspan="3"| 10th Imagine India Film Festival   (2011; Spain)  
| Best Film
| Veettilekkulla Vazhi
|  
|-
| Best Director
| Dr. Biju
|  
|-
| Best Music Director
| Ramesh Narayan
|  
|-style="border-top:2px solid gray;"
| rowspan="1"| International Film Festival of Kerala 
| rowspan="1"| 15th International Film Festival of Kerala   (2010; India)  
| NETPAC Award for Best Malayalam Film
| Veettilekkulla Vazhi
|  
|-style="border-top:2px solid gray;"
| rowspan="2"| Kerala State Film Awards 
| rowspan="2"| Kerala State Film Awards   (2011; India)   Best Cinematography
| M. J. Radhakrishnan
|  
|- Best Processing Lab
| 
|  
|-style="border-top:2px solid gray;" National Film Awards 
| rowspan="1"| 58th National Film Awards   (2011; India)   Best Feature Film in Malayalam
| Veettilekkulla Vazhi
|  
|-style="border-top:2px solid gray;"
| rowspan="2"| Zanzibar International Film Festival  
| rowspan="2"| 14th Zanzibar International Film Festival   (2011; Tanzania)  
| Signis Award (Commendation)
| Dr. Biju
|  
|-
| Industry Award for Best Cinematography
| M. J. Radhakrishnan
|  
|-
|}

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
 

 
 
 
 