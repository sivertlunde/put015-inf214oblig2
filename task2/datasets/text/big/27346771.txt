All I Desire
{{Infobox film
| name           = All I Desire
| image_size     =
| image	         = All I Desire FilmPoster.jpeg
| caption        =
| director       = Douglas Sirk
| producer       = Ross Hunter
| writer         = Carol Ryrie Brink (novel) Robert Blees James Gunn Gina Kaus
| narrator       = Richard Carlson
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = Universal-International
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.2 million (US) 
}}
All I Desire is a 1953 drama film directed by Douglas Sirk, starring Barbara Stanwyck as an actress who returns to visit her husband and children after having run off years before. It is based on the novel Stopover by Carol Ryrie Brink.

==Plot==
Naomi Murdoch abandons her husband and children in Wisconsin, setting off to become an actress and also to flee from an aggressive former suitor, Dutch Heineman. She decides to return 10 years later at the invitation of daughter Lily, who is appearing in a school play and about to graduate.

Lily is delighted, as are townspeople who mistakenly believe Naomi has become a great success on stage. But her schoolteacher husband Henry is unsure how he feels about Naomi being back, as is daughter Joyce, still bitter about her mothers long absence.

At the school play, Dutch cant take his eyes off Naomi. A teacher, Sara Harper, now loves Henry but can tell he still has feelings for his long-absent wife.

Dutch turns up when Naomi goes for a horseback ride with Joyce and boyfriend Russ. Joyce and her boyfriend leave Naomi by the lake and Dutch shows up tries to embrace Naomi.  Naomi refuses his advances and rides home alone. Henry and Naomi reconcile.  But Dutch signals that he wants her to meet him at the lake.  Naomi goes to tell Dutch that he must leave her alone because she is going to stay with Henry. Dutch says he has been to good to her and tries force himself on her to fend him off, Naomi uses a whip, and struggles with him. During the struggle Dutchs Rifle falls and Dutch is shot Naomis son Ted helps take Dutch to a doctor, he fears his mother might have had a romantic rendezvous arranged with Dutch that day.

Everyone agrees it would be better if Naomi went away. Lily wants to go along, in order to become a famed actress like her mother, whereupon Naomi confesses that her career has actually been a failure. Henry can see from Dutchs wounds that Naomi wanted nothing more to do with him. He prevents Naomi from leaving, wanting to give their life together another try.

The director, Douglas Sirk, originally shot a darker, sadder ending, but the producer, Ross Hunter, substituted a happier one  

==Cast==
*Barbara Stanwyck as Naomi Murdoch Richard Carlson as Henry Murdoch
*Lyle Bettger as Dutch Heineman
*Marcia Henderson as Joyce Murdoch
*Lori Nelson as Lily Murdoch
*Maureen OSullivan as Sara Harper Richard Long as Russ Underwood Billy Gray as Ted Murdoch
*Dayton Lummis as Col. Underwood
*Lotte Stein as Lena Maria Svenson
*Fred Nurney as Hans Peterson

==References==
 

==External links==
* 
* 
* 

 
 


 
 
 
 
 
 
 


 