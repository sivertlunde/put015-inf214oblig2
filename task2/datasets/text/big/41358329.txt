Babe's & Rickey's Inn
{{Infobox film
| name           = Babe&#39;s &#38; Rickey&#39;s Inn
| image          = 
| alt            = 
| caption        = 
| director       = Ramin Niami
| producer       = {{Plainlist| 
* Ramin Niami
* Behrooz Arshadi
* Karen Robson
* Azita Shahryarinejad}}
| writer         = Ramin Niami
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Sideshow Releasing, Inc. Cinedigm
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Babes & Rickys Inn is a documentary film directed by Ramin Niami about the famed blues club, Babes & Rickeys Inn. The film premiered April 5, 2013 at Laemmle Monica in Santa Monica, California.            

==Plot==

The film chronicles the last days of one of the most unique and vibrant blues clubs in the world, 
Babe’s and Ricky’s Inn, located surprisingly to some, in South Central, Los Angeles. For 53 years a woman from Mississippi, Laura Mae Gross (aka "Mama Laura") brought musicians together, regardless of race, age, or gender, in a place where only the music mattered. The club was originally located on legendary Central Ave, in South Central LA. -- a club where everyone was welcome, and great live blues could be heard every night. Musicians youd recognize (like Johnny Lee Hooker, B.B. King, Albert King, Eric Clapton, Keb’ Mo’, Zac Harmon) used to drop in to the club and jam with musicians you should know and the film features original music by some of the most important blues artists alive. Stunning guitar performances and personal stories about the hard blues life come together in a film about what it means to devote your life to music.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 