Vera (film)
{{Infobox film
| name           = Vera
| image          = Vera-sergio-toledo.jpg
| caption        = Theatrical release poster
| director       = Sérgio Toledo
| producer       = Sérgio Toledo
| writer         = Sérgio Toledo Felipe Daviña André Macedo
| starring       = Ana Beatriz Nogueira Norma Blum Raul Cortez Carlos Kroeber
| music          = Arrigo Barnabé
| cinematography = Luciana de Francesco
| editing        = Tércio G. da Mota
| studio         = Nexus Filmes Embrafilme
| distributor    = Embrafilme
| released       =   }}
| runtime        = 87 minutes
| country        = Brazil
| language       = Portuguese
}}
 transsexual most known as the author of poems book A queda para o alto (Descending Upwards). 

==Plot==
Anderson (birth name "Vera") is a transsexual man who lives in a correctional facility for young people. After a book of verses about his life as a young troubled youth, he meets a benevolent, educated man, who helps him, even allowing him to spend some time at his home, and arranges for a job for him as an intern in his office.

He comes into his gender identity and begins to dress as a man, eventually falling in love with a woman and passing as cisgender to her family.

The film succeeds in focusing on Andersons personality and feelings until his tragic death.

==Cast==
* Ana Beatriz Nogueira as Vera
* Norma Blum
* Raul Cortez as Eduardo Suplicy
* Carlos Kroeber
* Adriana Abujamra
* Cida Almeida
* Liana Duval
* Abrahão Farc
* Aida Leiner
* Imara Reis

==Reception==
In 1986, at the Festival de Brasília it won the awards for Best Actress (Nogueira), Best Soundtrack (Arrigo Barnabé), and Best Sound (José Luiz Sasso).  In 1987, Nogueira won the Silver Bear for Best Actress at the  37th Berlin International Film Festival, where Vera was nominated for Best Film.    At the Three Continents Festival Nogueira received a Honourable Mention. 

== See also ==
* Rosely Roth

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 


 
 