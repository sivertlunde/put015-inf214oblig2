Rocket Squad
{{Infobox Hollywood cartoon|
| cartoon_name = Rocket Squad
| series = Merrie Melodies / Daffy Duck & Porky Pig
| image = Rocket_Squad_Title_Card.jpg
| caption = The Rocket Squad title card
| director = Chuck Jones
| story_artist = Tedd Pierce Richard Thompson   Abe Levitow   Ben Washam 
| layout_artist =Ernie Nordli
| background_artist = Philip De Guard
| voice_actor = Mel Blanc
| musician = Milt Franklyn
| producer = Edward Selzer (uncredited)
| distributor = Warner Bros. Pictures   The Vitaphone Corporation
| release_date = March 10, 1956 (USA)
| color_process = Technicolor
| runtime = 6 minutes 35 seconds
| movie_language = English
}}

Rocket Squad is a 1956 Warner Bros. theatrical cartoon short produced by Eddie Selzer.  It was directed by Chuck Jones and written by Tedd Pierce starring Daffy Duck and Porky Pig as futuristic space cops who patrol the Milky Way Galaxy.  It is a parody of Dragnet (TV series)|Dragnet.

Rocket Squad appears on the Looney Tunes Golden Collection Volume 3 four-disc DVD box set.

==Summary==

In this futuristic setting, Daffy Duck is Sgt. Joe Monday and narrator of the story.  His partner, Det. Schmoe Tuesday (Porky Pig), and himself are returning from a routine investigation of a 712-malicious mischief (School children blew one of the rings off of Saturn.  When will parents learn to keep uranium out of their childrens reach?) in the Big Dipper area, when they receive a call from the police chief. They return to headquarters and await the police chiefs call (10:52: Back at the old desk, waiting for a call from the Chief. Half a cops life is spent in waiting. 10:53: the Chief called.).  We then hear Daffy exclaiming that the police chief is asking him to pursue the Flying Saucer Bandit, but see the police chief on his knees begging and crying for Daffy to take the case.  Upon telling the viewers that he consents to take the case, we see the police chief kissing his feet.

Sgt. Monday and Det. Tuesday then report to the scene of the crime.  The crime robots are already there picking up clues to aide Daffy and Porky in their ability to identify the Flying Saucer Bandit.  They return to the station with a bag of clues, and empty them into the Univac clue machine, which pops out a music sheet for a piano.  The music sheet is placed into a piano, which plays out the song Mother Machree, identified by Daffy.  They proceed to the George Mother Machree file where they begin their pursuit of the Flying Saucer Bandit.

They learn that the Flying Saucer Bandit is at a restaurant ordering a sandwich, to which they hurry because he is known as a notoriously fast eater.  Upon arriving, they realize they have missed the bandit, and continue their chase (1:07: He left the Blast-Inn. 1:08: We arrived at the Blast-Inn. Our deductions later proved that we had missed him by one minute. 1:09: We set out in pursuit.)  We see Porky and Daffy in hot pursuit of Mother Machree when they lose him in a smog bank over Los Angeles.  Porky points out a smog cloud flying away, when they pull up and tell Machree to come out, to which Daffy informs us he has an elaborate alibi already cooked up; it turns out that Machree is innocent, evidenced by his confession of I didnt do nothing... I didnt do nothing! Like I said, I didnt do nothing!. This leads into the conclusion of the cartoon in which Daffy and Porky are sentenced to twenty years in prison for false arrest (thirty years actually, as Porky state so afterwards).

==Production details==

*That is a futuristic parody of the classic television shows Dragnet (series)|Dragnet and Racket Squad.
*Daffy and Porkys characters first names put together are Joe Schmoe (Sgt. Joe Monday and Det. Schmoe Tuesday ("He always follows me," declares Joe.)
*Much of the imagery, particularly entering the police station and the evaporators, is reused from Jones previous short, Duck Dodgers in the 24½th Century.
*Among the names appearing on the buttons of the Criminal Detector Set are John Burton Jr., Tedd Pierce, Chuck Jones|C.M. Jones, Mel Blanc, Norman Moray and Eddie Selzer, all of whom had a hand in the making of the cartoon (Moray was a key executive with The Vitaphone Corporation, Warner Bros. short-subject division).
*Porky and Daffy losing the Flying Saucer Bandit in a smog bank over Los Angeles is in reference to Los Angeles being considered the Smog Bank Capital of the world in the early to late 1950s.
*This cartoon has been shown (in clips) on the PBS special "Chuck Jones: Extremes and Inbetweens--A Life in Animation" as part of the section about Jones using Porky and Daffy in cartoon shorts that parody popular movie and TV show genres of the era.

==See also== List of cartoons featuring Daffy Duck  List of cartoons featuring Porky Pig

==External links==
* 
* 

 

 
 
 
 