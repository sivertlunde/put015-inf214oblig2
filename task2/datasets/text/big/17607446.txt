Fixer Dugan
{{Infobox film
| name           = Fixer Dugan
| image          =
| caption        = James Anderson (assistant)
| producer       =
| writer         =
| starring       = Lee Tracy Virginia Weidler Peggy Shannon
| music          =
| cinematography =
| editing        =
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States English
| budget         =
| gross          =
}}
Fixer Dugan is a 1939 drama film starring Lee Tracy as a circus promoter who decides to help out an orphaned girl, played by Virginia Weidler. The film was directed by Lew Landers, released by RKO Radio Pictures and is based on the play Whats a Fixer For? by H.C. Potter.

== Plot ==
Charlie Dugan is the "fixer" who keeps Barvins Greater Shows, a struggling traveling circus, going. He is glad to welcome back lion tamer Aggie Moreno, as her act is a popular one. However, she and top-billed high wire artist Pat OConnell loathe each other, and thats a feud that Aggie extends to include Pats 10-year-old daughter Terry. However, when Pat falls to her death during a performance, Dugan persuades Aggie to take charge of the orphan girl. After a while, Aggie finds she likes Terry.

One night, Terry overhears Frank Darlow (the son of a rival circus owner) and Jake talking about how to take possession of Aggies lions. Darlows father had tricked Aggie into signing a bill of sale for them. When Terry is unable to interrupt Aggies performance to warn her, she sneaks through the lions entrance into the cage. This disturbs the lions, and Aggie is barely able to control them and get Terry out of danger. The audience, thinking this is all part of the act, is thrilled. Dugan asks Darlow to meet him in a few hours to pick up the lions, but when Darlow shows up, the circus has already left. Darlow and Jake give chase.

Dugan keeps Terry in the lion taming act, which becomes so popular that A. J. Barvin tells her that she has saved his circus. Dugan keeps outsmarting Darlow, but finally Darlow brings the local sheriff to take custody of Terry; Dugan does not have a permit for the underage girl to be working. Terry is put in a childrens home run by Mrs. Fletcher.

Having been rained out at the next scheduled location, Dugan persuades Barvin to put on a performance at the childrens home instead. Mrs. Fletcher tells Aggie that any attempt to adopt Terry would be rejected. Terry stows away on one of the trucks when the circus leaves.

Darlow shows up with a policeman, but Dugan dupes him into signing a bill of sale, returning the lions to Aggie. Meanwhile, Aggies assistant, thinking the lions are going to be taken away, lets one of them out before anybody can stop him. The lion stalks Terry, but Aggie manages to hold it off until it is netted. Mrs. Fletcher witnesses this and tells Aggie that she has changed her mind and would approve an adoption.

== Cast ==
*Lee Tracy as Charlie "Fixer" Dugan
*Virginia Weidler as Ethel Myrtle "Terry" OConnell
*Peggy Shannon as Aggie Moreno
*Bradley Page as A. J. Barvin
*William Edmunds as Smiley
*Edward Gargan as Jake
*Vinton Hayworth as Frank Darlow (credited as Jack Arnold)
*Rita La Roy as Patricia "Pat" OConnell
*Irene Franklin as Jane
*John Dilson as Steve
*Edythe Elliott as Mrs. Fletcher

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 