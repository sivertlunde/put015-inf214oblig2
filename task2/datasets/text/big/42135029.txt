Burning Paradise
 
 
{{Infobox film
| name           = Burning Paradise
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Ringo Lam
| producer       = Tsui Hark 
| writer         = {{plainlist|
*Nam Yin
*Wong Wan-choi
}} 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{plainlist|
*Willie Chi
*Carman Lee}}
| music          = Mak Chun-hung 
| cinematography = Gao Ziyi 
| editing        = 
| studio         = DLO Films Production Limited 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hong Kong 
| language       = Cantonese 
| budget         = 
| gross          = HK$ 1,819,697
}}

Burning Paradise (Foh Siu Hung Lin Ji 火燒紅蓮寺) is a 1994 Hong Kong action film directed by Ringo Lam. The film is set in the Qing dynasty and stars  Willie Chi Tian-sheng as Fong Sai Yuk  and Yang Sheng as Hung Hei-kwun as Yang Sheng. 

The film was a box office failure in Hong Kong. 

==Release== Time Out London referred to the film as a "A box office disaster in Hong Kong".  After its release in Hong Kong on 27 March 1994, it grossed a total of HK$1,819,69.    The film was the 145th highest grossing film in Hong Kong for 1994. 

The film was released direct-to-video in the United Kingdom.  The film was released on DVD on 29 June 2010. The DVD contains an interview with Tsui Hark and the films trailer. 

==Reception== The Adventurers. 

==See also==
 
* Hong Kong films of 1994
* List of action films of the 1990s

==Notes==
 

===References===
*  

==External links==
*  

 

 
 
 
 
 