A Blue Gum Romance
 
 
{{Infobox film
| name           = A Blue Gum Romance
| image          = 
| image_size     =
| caption        = 
| director       = Franklyn Barrett
| producer       = 
| writer         = Franklyn Barrett
| narrator       =
| starring       = Tien Hogue
| music          =
| cinematography = Franklyn Barrett
| editing        = 
| studio   = Fraser Film Release and Photographic Company
| distributor    = 
| released       = 20 September 1913
| runtime        =
| country        = Australia English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}

A Blue Gum Romance is a 1913 Australian silent film directed by Franklyn Barrett.  It is considered a lost film.

==Production==
The film was set in the timber industry area near Gosford, although interiors were shot in Sydney. The aboriginal characters were played by white actors in blackface. 

==Cast==
*Tien Hogue as heroine
*Tom Middleton as hero
*Douglas Lotherington as aboriginal chief
==Production==
Filming was completed by July 1913.  

It was the first narrative film from the Fraser Film Release and Photographic Company.
==Reception==
The film was popular at the local box office and screened in England and the USA. 
==References==
 

==External links==
* 
*  at National Film and Sound Archive
 

 
 
 
 
 
 

 