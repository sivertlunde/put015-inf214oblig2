Painters Painting
Painters Painting: The New York Art Scene 1940-1970 is a 1972 documentary directed by Emile de Antonio.  It covers American art movements from abstract expressionism to pop art through conversations with artists in their studios.  Artists appearing in the film include Willem de Kooning, Jasper Johns, Andy Warhol, Robert Rauschenberg, Helen Frankenthaler, Frank Stella, Barnett Newman, Hans Hofmann, Jules Olitski, Philip Pavia, Larry Poons, Robert Motherwell, and Kenneth Noland.

==Further reading==

* Emile de Antonio and Mitch Tuchman.  Painters Painting: A Candid History of the Modern Art Scene, 1940-1970. Abbeville Press, 1984. (ISBN 0-89659-418-1) Compilation based on transcripts of interviews from the film.

==External links==
* 

 
 
 
 
 
 
 
 

 