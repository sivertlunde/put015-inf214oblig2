Groupie Girl
{{Infobox Film
| name           = Groupie Girl
| image          = Groupiegirlposter.jpg
| caption        = American release poster
| director       = Derek Ford
| producer       = Stanley Long
| writer         = Derek Ford Stanley Long Flanagan Mary Collinson Madeleine Collinson 
| music          = Opal Butterfly English Rose
| cinematography = Stanley Long
| editing        = Tony Hawk
| distributor    = 
| released       = June 1970
| runtime        = 87 minutes
| country        = United Kingdom English
| budget         = £16,000 Simon Sheridan, Keeping the British End Up: Four Decades of Saucy Cinema, Titan Books 2011 p 70-71 
| gross          = £50,000 (US only) 
| preceded_by    = 
| followed_by    = 
}}

Groupie Girl is a 1970 British drama film about the rock music scene, directed by Derek Ford and starring Esme Johns, Donald Sumpter and the band Opal Butterfly.  The film was written by Ford and former groupie Suzanne Mercer.

Ford later complained to Cinema X magazine "we were shooting in a discotheque one Saturday night and my ears rang right through to Monday morning.  I was sick -physically sick- on Sunday from the noise level we suffered".

The film was released in America in December 1970 by American International Pictures as I am a Groupie, and in France in 1973- with additional sex scenes- as Les demi-sels de la perversion (The Pimps of Perversion).  The film was later re-released in France in 1974 as Les affamées du mâle (Man-Hungry Women) this time with hardcore inserts credited to ‘Derek Fred’.

Groupie Girl was released on UK DVD in January 2007 on the Slam Dunk Media Label as part of the ‘Saucy Seventies’ series (the earlier US DVD release on the Jeff films label is an unauthorized bootleg.)

==Cast==
* Billy Boyle as Wesley
* Richard Shaw as Morrie
* Donald Sumpter as Steve
* Esme Johns as  The Groupie - Sally
* James Beck as Brian Paul Bacon as Alfred
* Neil Hallett as Detective Sergeant Flanagan as Thief
* Eliza Terry as Suzy
* Belinda Caren as Pat
* Ken Hutchison as Colin
* Jimmy Edwards as Bob

==Soundtrack listing==
(Original album)

Side 1
#You’re A Groupie Girl (Opal Butterfly)
#To Jackie (English Rose)
#Four Wheel Drive (The Salon Band)
#Got A Lot Of Life (Virgin Stigma)
#I Wonder Did You (Billy Boyle)
#Gigging Song (Opal Butterfly)
#Disco 2 (The Salon Band)
#Now Your Gone, I’m A Man (Virgin Stigma)
Side 2
#Yesterdays Hero (English Rose)
#Love Me, Give A Little (Virgin Stigma)
#Looking For Love (Billy Boyle)
#Sweet Motion (The Salon Band)
#Love’s a Word Away (English Rose)
#True Blue (The Salon Band)
#Groupie Girl, It Doesn’t Matter What You Do (Virgin Stigma)

* English Rose were Lynton Guest, Jimmy Edwards and Paul Wolloff, who also have minor roles in the film. Guest later became a sports journalist and recently authored the book “The Trials of Michael Jackson”.
==References==
 
==External links==
*  

 
 
 
 
 
 
 