Tom Waits for No One
 
{{Infobox film
|name=Tom Waits for No One
|starring=Tom Waits
|country=United States
|language=English
|released=1979 John Lamb
}}
 rotoscoped short film starring Tom Waits, singing "The One That Got Away" to an apparition.
 John Lamb of Lyon Lamb, it was amongst the first music videos of its kind, and nearly two years before the advent of MTV. The film, inspired by a performance of Waits at the Roxy in May 1977, captured a first place award at the first Hollywood Erotic Film and Video Festival in 1980. The film never saw commercial release and sat in obscurity for 30 years, when it went quietly viral on YouTube.

Filmed live at the La Brea Stage in Hollywood in 6 takes and edited down to five and a half minutes, the live frames were then traced using a "video rotoscope" and then converted by hand into animation. This particular combination of rotoscoping and pencil test, originally developed for Ralph Bakshis American Pop, was considered innovative at the time, and assisted in winning Lyon Lamb a 1980 Academy Award for Scientific and Technical Achievement. 

The films production team consisted of an amazing host of talents which includes:
• David Silverman-animator: David was the first animator on the Simpsons, working with Klasky-Csupo on Simpson interstitials for The Tracey Ullman Show, later on to produce the Simpsons, The Wild Thorneberrys and Monsters Inc.
• Keith Newton-rotoscope and character design: Keith moved on to Disney doing backgrounds and character design for Pocahontas, portrait artist on site at Epcot Center and Disney World in Florida.
• Micheal Cressey-inker: Micheal went into a distinguished career of childrens book publishing, writing 8 books and receiving the coveted Caldecott Award twice, among numerous others. Garrett Smith-live action camera: Garrett moved into an illustrious career at Paramount Studios and became an integral part in the development of HD with Texas Instruments. 
• Gary Beydler-live action camera: Gary was an esteemed film maker, photographer and fine artist with works in the MMA, Moca and a Newsweek cover article.
• Donna Marie Gordon-dancer/model: Donna became a choreographic trainer for Las Vegas chorus girls for 18 years .
• Ray Roberts extended his fine art talents and today is a renowned Plein Air painter and multiple award winner for his depictions of the Southwest.
• Garrett Smith served as vice president, production technology and digital mastering operations at Paramount Pictures. He was actively involved in the development of DVD, HDTV and a member of Digital Cinema Initiatives. Today, Smith is a member of the Academy of Motion Picture Arts and Sciences, and serves on the Science and Technology Council as Co-Chair of the Next Generation Technology Project.
• John Lamb received an Academy Award for Scientific and Technical Achievement in 1980 for co-invention of the Lyon Lamb Video Animation System (VAS). Lamb also developed and manufactured the first video rotoscope system in 1979. Continuously working in the graphic arts and animation over the last three decades, Lamb recently opened his latest art project "Blast from the Past" in Oceanside, Ca.

Recently, a cel from the production became part of the Tom Waits permanent exhibit at the Rock and roll Hall of Fame in Cleveland, Ohio.

== References ==
 

== External links ==
*  
*  
*  

 
 
 


 