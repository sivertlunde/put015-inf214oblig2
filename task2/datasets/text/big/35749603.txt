Rape Culture (film)
 
{{Infobox film
| name           = Rape Culture
| image          = Opening Title - Rape Culture - 1975 Film.png
| image_size     = 220px
| border         = 
| alt            = 
| caption        = Opening title
| director       = 
| producer       = Margaret Lazarus Renner Wunderlich
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Cambridge Documentary Films
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}

Rape Culture is a 1975 film by Cambridge Documentary Films, produced by Margaret Lazarus and Renner Wunderlich. It was updated in 1983.   

In January 1975, Judy Norsigan outlined how the film illustrated "rape culture", through the voices of men and women, including rapists, victims, prisoners, rape crisis workers, and the media.   

The film featured prisoners of Lorton Reformatory, Virginia, "Prisoners Against Rape Inc" (PAR), a not-for-profit organisation founded by William Fuller and Larry Cannon on September 9, 1973 in conjunction with women fighting rape.    The prison administration "approved" self-help status. 

PAR was set up after Fuller wrote to the DC Rape Crisis Centre in 1973 and asked for assistance. The DC Rape Crisis Centre had opened in 1972 in response to the high incidence of rape against women of color. Fuller acknowledged his history of rape, murder, and prison rape. He wanted to stop being a rapist. This resulted in a co-operative effort.   

The women from the DC Rape Crisis Centre who initiated work with PAR were Loretta Ross, Yulanda Ward and Nkenge Toure.  Ross has said that whilst the relationship was seen initially as controversial, it was one of the more interesting aspects of her work at the DC Rape Crisis Centre in the 1970s and 80s. In interview with Joyce Follet, Ross observed that in the work of the DC Rape Crisis centre they could bandage up women all they wanted to, but if they did not stop rape what was the point.  Maragaret lazarus, the films producers said of this relationship that the work was "groundbreaking".   

The film featured Mary Daly, radical feminist philosopher, academic, and theologian, and Author and Artist Emily Culpepper. They discussed rapism as an intellectual concept, and phallocentric morality and "its unholy trinity of rape, genocide and war.".  

Doreen McDowell, a rape victim, talked of her experience, how sex fantasies play a part in rape, and how male identified behaviour in women maintained a "state of siege". Powerful statistical evidence, refuting rape myths, law enforcement and legislative views of rape were presented by Joanna Morris, author and statistical co-ordinator for rape crisis centres across the USA.
 Gone with the Wind, Alfred Hickcocks film Frenzy, and Hustler magazine were some of the media used to illustrate the normalisation of rape.

In describing the film, the producers say that it attempts to give real and accurate limits to rape and expand societys narrow and sexist concepts of rape. 

Lazarus has said of the films title that it came from long discussion about what the film was trying to illustrate. She has also expressed the view that the film is the first time "rape culture" was used in its widest accepted sense.  A mention of the film in the Congressional Record in January 1978 is the first known occurrence of the term rape culture in national-level American politics.   

==See also==
*Rape culture

== References ==
 

==External links==
* 
*  from Cambridge Documentary Films
* 

 
 
 
 
 
 