Mehndi (1958 film)
 
{{Infobox film
| name           = Mehndi
| image          = Mehndi Hindi film poster.jpg
| caption        = Film poster
| director       = S.M. Yusuf
| producer       = A.A. Nadiadwala
| screenplay     = 
| story =  Ajit Jayashree Ravi
| cinematography = 
| editing        = 
| distributor    =
| studio         = 
| released       =  
| runtime        = 
| country        =  India
| language       = Hindi
| budget         =
}} 1958 Hindi Ajit and Jayashree in lead roles.

==Cast== Ajit 
* Jayashree
* Veena Sapru
* M. Kumar
* Mirza Musharaf
* Balam
* Lalita Pawar
* Krishnakant
* Anwaribai Muzaffar Adeeb

==Soundtrack== Ravi with lyrics penned by S. H. Bihari, Khumar Barabankvi, Kamil Rashid and Sarvar.
{| class="wikitable"
|-
! # !! Title !! Singer(s) !! Lyrics
|- Hemant Kumar, Lata Mangeshkar || S. H. Bihari
|-
| 2 || "Meri Dulhan Bareli Se Aai Re" || Asha Bhosle, Usha Mangeshkar || Khumar Barabankvi
|-
| 3 || "Bedard Zamana Tera" || Hemant Kumar, Lata Mangeshkar || S. H. Bihari
|}

==External links==
*  
*  

 
 
 

 