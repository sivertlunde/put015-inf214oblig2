Home Alone 2: Lost in New York
 
 
{{Infobox film
| name           = Home Alone 2: Lost in New York
| image          = Home Alone 2.jpg
| caption        = Theatrical release poster Chris Columbus John Hughes
| writer         = John Hughes Daniel Stern John Heard Tim Curry Brenda Fricker Catherine OHara 
| music          = John Williams
| cinematography = Julio Macat
| editing        = Raja Gosnell Hughes Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $358.9 million 
}} Christmas Family family comedy John Hughes Chris Columbus. Daniel Stern John Heard, Tim Curry, and Brenda Fricker are also featured.

The movie was filmed in Winnetka, Illinois, OHare International Airport in Chicago, Miami, and New York City (which was star Culkins hometown at the time). The exterior of Duncans Toy Chest in New York City was filmed outside of the Rookery Building in downtown Chicago. Despite receiving mixed reviews from critics, the film became the second most financially successful film of 1992, earning over $173 million in revenue in the United States and over $359 million worldwide against a budget of $20 million.

Home Alone 3 followed five years later in 1997, Home Alone 4 followed in 2002, and
  followed in 2012. Culkin did not appear in any of them nor did the rest of the cast.

==Plot==
 

One year after the events of the first film, and the night before their flight to Miami for Christmas, the McCallister family gathers at the Winnetka, Illinois home of Peter and Kate McCallister. Their son Kevin sees Florida as contradictory to Christmas because he thinks there are no Christmas trees in Florida. During the school Christmas pageant, Kevins older brother Buzz humiliates him during his solo, causing Kevin to retaliate, thereby ruining the performance. Kevin refuses to apologize for his actions, still unhappy with his familys decision to go to Florida, and he storms up to his third floor bedroom of the house. During the night, Peter unknowingly resets the alarm clock and causes the family to oversleep. In the confusion and rush to reach the airport on time, Kevin boards a flight bound for New York City, carrying Peters bag containing his wallet and a large amount of cash; the family is unaware that he is missing until after they land in Miami. In New York, Kevin tours the city and convinces the staff at the Plaza Hotel into giving him a room using Peters credit card. During a visit to Central Park, Kevin is scared by the appearance of a homeless woman tending to pigeons.

On Christmas Eve, Kevin tours the city in a limousine and visits a toy store where he meets its philanthropic owner, Mr. Duncan. Kevin learns that the proceeds from the stores Christmas sales will be donated to a childrens hospital. Duncan gives Kevin a pair of ceramic turtledoves as a gift, instructing him to give one to another person as a sign of eternal friendship. After encountering the Wet Bandits Harry and Marv (who recently escaped from prison and are now called the Sticky Bandits), Kevin retreats to the hotel. The hotels concierge Mr. Hector confronts Kevin about the credit card which has been reported stolen. Kevin flees after evading Mr. Hector, but is captured by Harry and Marv. The duo discuss plans for robbing the toy store that night, before Kevin escapes.

The McCallisters travel to New York after tracking the whereabouts of the stolen credit card and Kate searches the city for Kevin. Meanwhile, Kevin goes to his uncle Robs townhouse only to find the house vacant and undergoing renovations while Rob and his family are in Paris. In Central Park, he comes across the pigeon lady. When Kevin gets his foot caught while running away, she frees him. The pair watch an orchestra perform while Kevin discovers how her life has fallen apart and how she dealt with it by taking care of the pigeons in the park. Kevin vows to the pigeon lady that he will be her friend.

Kevin later returns to the townhouse and rigs it with numerous booby traps. Kevin arrives at the toy store during Harry and Marvs robbery, throws a brick through the window, setting off the stores alarm, and leads the pair to the townhouse, springing the traps. When the duo close in on Kevin, he flees the townhouse and calls the police. Harry and Marv are able to catch Kevin and attempt to kill him in Central Park. The pigeon lady incapacitates the duo with her pigeons and Kevin sets off fireworks he had bought earlier to signal the police. Shortly after, Harry and Marv are arrested. While at the toy store, Mr. Duncan finds a note from Kevin attached to the brick explaining what happened.

Meanwhile, Kate remembers Kevins fondness for Christmas trees. After observing Kevin making a wish at the Rockefeller Center Christmas Tree, Kate meets him there and they reconcile. On Christmas Day, a truckload of gifts arrive at the McCallisters hotel room from the toy store. Kevin and Buzz reconcile and Buzz allows him to open up the first present. Kevin goes to Central Park to give the pigeon lady the second turtledove. At the hotel, Buzz receives the bill for Kevins stay from Cedric. Peter suddenly calls out, "Kevin, you spent $967.00 on room service?!", at which point Kevin runs back to the hotel.

==Cast==
*Macaulay Culkin as Kevin McCallister, a boy who is the youngest son of the McCallister family. 
*Joe Pesci as Harry, the short leader of the Sticky Bandits, formerly called the Wet Bandits. Daniel Stern as Marv, the tall member of the Sticky Bandits, formerly called the Wet Bandits. John Heard as Peter McCallister, the father.
*Catherine OHara as Kate McCallister, Kevins mother.
*Tim Curry as Mr. Hector, a concierge at the Plaza Hotel who is suspicious of Kevin. eccentric lady who tends to pigeons in Central Park who becomes Kevins friend.
 Chris Columbus has an uncredited cameo as a man in "Duncans Toy Chest."

==Distribution==

===Marketing=== Sega Mega Mouse Trap.  

The Talkboy cassette recorder used by Kevin in the film was originally a non-working prop during shooting. It was later released due to a popular letter writing campaign by fans along with the film by Tiger Electronics (currently under Hasbro).

American Airlines had product placement in the film with the McCallisters making their trip on the airlines two Boeing 767s. The airline became part of the films release on home video.

Coca-Cola products make several appearances in the film, including in scenes when Kevin rides a limousine and when Kevins cousin Fuller wakes up at the Plaza Hotel. Notably, Pepsi products appeared in the first Home Alone film instead.

===Home media===
 
The film was first released on VHS on July 27, 1993. It was re-released in 1997 along with Home Alone on VHS. It was later released on DVD on October 5, 1999 as a basic package, with no special features other than theatrical trailers for the film and its predecessor and successor. The film was released on Blu-ray Disc in 2009 with no special features, and was released alongside Home Alone in a collection pack the following year. The film was reissued again on DVD and Blu-ray Disc in 2013.

==Reception== cartoon violence is only funny in cartoons. Most of the live-action attempts to duplicate animation have failed, because when flesh-and-blood figures hit the pavement, we can almost hear the bones crunch, and it isnt funny." 
Review aggregation website Rotten Tomatoes gives the film a score of 24% based on reviews from 25 critics. 

===Box office===
The film opened to $31.1 million from 2,222 theaters, averaging $14,008 per site.  While it started off better than Home Alone, the final box office gross was much less.  $173,585,516 was taken in the United States and a total of $358,994,850 worldwide.   

==Novelization==

Home Alone 2 was novelized by Todd Strasser and published by Scholastic in 1992 to coincide with the film. It has an ISBN of 0-590-45717-9. An audiobook version was also released read by Tim Curry.

As in the novelization of the first film the McCallisters live in Oak Park, Illinois and the crooks are named as Harry Lyme and Marv Murchens.

In the beginning of the novelization, a prologue, which ends up being Marvs nightmare in prison, he and Harry sneak away from the cops and return to Kevins house to seek revenge on Kevin McCallister. Kevin bolts into the garage with Marv and Harry in hot pursuit. Harry and Marv end up triggering extra traps that Kevin had set up in the garage. Kevin watches as Marv ends up triggering a trap where a running lawnmower falls on his head (This was a trap featured in Home Alone 3).

==Music==
John Williams returned from the first installment to score Home Alone 2. While the film featured the first films theme song "Somewhere in My Memory", it also contained its own theme entitled "Christmas Star". Two soundtrack albums of the film were released on November 20, 1992, with one featuring Williams score and the other featuring contemporary Christmas songs featured in the film. Ten years later, a 2-disc Deluxe Edition of the film score soundtrack was released.

===Original Score===
{{Infobox album  
| Name        = Home Alone 2: Lost in New York – Original Score
| Type        = film
| Artist      = John Williams
| Cover       = Home alone 2 soundtrack.jpg
| Released    = November 20, 1992
| Recorded    =
| Genre       = Classical
| Length      = 63:20
| Label       = Arista Records, 20th Century Fox Records
| Producer    = Far and Away  (1992)
| This album  = Home Alone 2  (1992) Jurassic Park  (1993)
| Misc        = {{Extra chronology Home Alone
| Type         = film
| Last album   =   (1990)
| This album   = Home Alone 2 (1992) Home Alone 3 (1997)
  }}
}} scored the first installment in the franchise. While the soundtrack is mostly a repeat of the first films material,  there are a few new prominent themes such as "Christmas Star" and "Plaza Hotel and Duncans Toy Store." Ultimately, the soundtrack fell out of print.

# Somewhere in My Memory (3:49)
# Home Alone (2:01)
# We Overslept Again (2:46) Christmas Star (3:18)
# Arrival in New York (1:41)
# Plaza Hotel and Duncans Toy Store (3:45)
# Concierge and Race to the Room (2:04)
# Star of Bethlehem (3:28)
# The Thieves Return (4:35)
# Appearance of Pigeon Lady (3:19) O Come All Ye Faithful / O Little Town of Bethlehem / Silent Night) (5:02)
# Into the Park (3:49)
# Haunted Brownstone (3:01)
# Christmas Star and Preparing the Trap (4:17)
# To the Plaza Presto (3:22)
# Reunion at Rockefeller Center (2:36)
# Kevins Booby Traps (3:41)
# Finale (3:55)
# Merry Christmas, Merry Christmas (2:51)

===Original Soundtrack Album===
{{Infobox album  
| Name        = Home Alone 2: Lost in New York – Original Soundtrack Album / Home Alone Christmas
| Type        = soundtrack
| Artist      = Various artists
| Cover       =
| Released    = November 20, 1992 (Original) September 16, 1997 (HAC)
| Recorded    =
| Genre       = Christmas music|Christmas, Pop music|pop, rock and roll, Contemporary R&B|R&B
| Length      = 39:26
| Label       = Arista Records (Original) Sony BMG (HAC)
| Producer    =
| Last album  =
| This album  =
| Next album  =
| Misc        =
}}{{Album reviews AllMusic
| rev1Score =    
}}
Home Alone 2: Lost in New York – Original Soundtrack Album is a 1992 soundtrack album that contains music from or inspired by   with an alternate tracklisting. Both versions feature tracks of John Williams score, though the tracks are of different songs between the original album and its re-release.

;Original Soundtrack Album tracklisting
# " )
# " )
# " , composed by John Williams, lyrics by Leslie Bricusse)
# "My Christmas Tree" (2:35) (Home Alone Childrens Choir, composed by Alan Menken, lyrics by Jack Feldman)
# " )
# " )
# "Merry Christmas, Merry Christmas" (2:40) (John Williams)
# " )
# " )
# " )
# " )
# " )

;Home Alone Christmas tracklisting
# "All Alone on Christmas" (Darlene Love)
# "A Holly Jolly Christmas" (Alan Jackson)
# "My Christmas Tree" (The Fox Albert Choir)
# "Somewhere in My Memory" (John Williams)
# "Silver Bells" (Atlantic Starr)
# "Sleigh Ride" (TLC)
# "Christmas All Over Again" (Tom Petty and the Heartbreakers)
# "Please Come Home for Christmas" (Southside Johnny Lyon)
# "Merry Christmas, Merry Christmas" (John Williams)
# "Carol of the Bells" (John Williams)
# "Have Yourself a Merry Little Christmas" (Mel Torme)
# "O Come All Ye Faithful" (Lisa Fischer)

===The Deluxe Edition===
{{Infobox album  
| Name        = Home Alone 2: Lost in New York – The Deluxe Edition
| Type        = soundtrack
| Artist      = John Williams
| Cover       =
| Released    = November 15, 2002
| Recorded    =
| Genre       = Classical
| Length      = 1:39:49
| Label       = Varèse Sarabande
| Producer    =
| Last album  =
| This album  =
| Next album  =
| Misc        =
}}
On the films 10th anniversary, Varèse Sarabande released a two-disc special edition soundtrack entitled Home Alone 2: Lost in New York – The Deluxe Edition. The soundtrack contains John Williams cues found on the previous releases as well as additional compositions that were left out from the final film. This release is also known for resolving a mastering error that caused the music to be inaccurately pitched.  

;Disc One
# Home Alone (Main Title) (2:07)
# This Year’s Wish (1:47)
# We Overslept Again / Holiday Flight (3:19)
# Separate Vacations*(1:58)
# Arrival in New York**(2:59)
# The Thieves Return (3:28)
# Plaza Hotel (3:04)
# Concierge (1:31)
# Distant Goodnights (Christmas Star) (Lyrics by Leslie Bricusse) (2:05)
# A Day in the City (:59)
# Duncans Toy Store (2:41)
# Turtle Doves (1:29)
# To the Plaza, Presto (3:27)
# Race to the Room / Hot Pursuit (4:08)
# Haunted Brownstone (3:02)
# Appearance of the Pigeon Lady (3:21) O Come, All Ye Faithful / O Little Town of Bethlehem / Silent Night

;Disc Two
# Christmas Star - Preparing the Trap (Lyrics by Leslie Bricusse) (4:22)
# Another Christmas in the Trenches (2:33)
# Running Through Town (1:16)
# Luring the Thieves*(4:02)
# Kevin’s Booby Traps (7:23)
# Down the Rope / Into the Park (5:06)
# Reunion at Rockefeller Center / It’s Christmas (5:21)
# Finale (2:00)
# We Wish You a Merry Christmas (Traditional) and Merry Christmas, Merry Christmas (Lyrics by Leslie Bricusse) (2:51)
# End Title (1:32)
# Holiday Flight (alternate) (2:32)
# Suite from “Angels with Filthy Souls II” (:56)
# Somewhere in My Memory (Lyrics by Leslie Bricusse) (3:57)
# Star of Bethlehem (Lyrics by Leslie Bricusse) (3:32)
# Christmas Star (Lyrics by Leslie Bricusse) (3:23)
# Merry Christmas, Merry Christmas (orchestra) (2:23)

==Sequels==
 
The sequel,   would follow in 2002 and 2012, respectively.

==See also==
* 
* 
* 
* 
* 

==References==
 

==External links==
 
* 
* 
* 
* 
* 
* 

 
 
 

 

 
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 