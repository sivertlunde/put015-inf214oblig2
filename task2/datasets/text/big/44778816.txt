MSG: The Messenger
 
 
{{Infobox film
| name= MSG: The Messenger
| image= MSG_The_Messenger_of_God_Poster.jpg
| caption = Official Poster
| director=  Gurmeet Ram Rahim Singh  Jeetu Arora
| producer = Gurmeet Ram Rahim Singh  Ravi Verma
| writer=  Gurmeet Ram Rahim Singh 
| starring=  Gurmeet Ram Rahim Singh  Daniel Kaleb Flora Saini Jayshree Soni Olexandra Semen Gaurav Gera Jay Singh Rajpoot Raju Pandit
| music          = Songs: Gurmeet Ram Rahim Singh  Background Score: Amar Mohile
| cinematography=  Gurmeet Ram Rahim Singh  Vineet Sapru Mangal 
| editing        = Gurmeet Ram Rahim Singh   Sanjay Kumar Singh
| studio         = Hakikat Entertainment Pvt. Ltd.
| distributor    = Hakikat Entertainment Pvt. Ltd.
| released       =  
| runtime = 197 minutes
| country        = India
| language       = Hindi
| budget      =   
| gross       =  Disputed    
}}

MSG: The Messenger (also known as MSG: The Messenger of God) is an Indian action film directed by Gurmeet Ram Rahim Singh and Jeetu Arora. The stunts have been performed by Singh himself. The movie contains situations involving youth-related issues like drug abuse, alcoholism, etc.  The film has seven songs and it was released worldwide on Friday 13 February 2015, in Hindi, English, Tamil, Telugu and Malayalam.     The film has received negative reviews from critics. 

==Synopsis==
Guru ji (Gurmeet Ram Rahim Singh) is a spiritual leader who has a huge follower base. He has accepted the challenge of eradicating social evils including drugs and gender-related issues that have been prevailing in the society. Those with vested interests and apathetic towards the welfare of the society are now disturbed as there’s someone who has taken control of the situation. They conspire to kill him. 

==Cast==

* Gurmeet Ram Rahim Singh as Guruji 
* Daniel Kaleb as Mike
* Flora Saini as Muskan
* Jayshree Soni as Kasam
* Olexandra Semen as Alice
* Gaurav Gera as Bhondu
* Jay Singh Rajpoot as Jet Bhai
* Himanshu Tiwari as Tiwari Don
* Raju Pandit as Khurana

==Soundtrack==
{{Infobox album 
| Name        = MSG: The Messenger
| Type        = Soundtrack
| Longtype    = to MSG The Messenger
| Artist      =  
| Cover       = 
| Border      = 
| Alt         = 
| Caption     = Soundtrack cover
| Released    =   Feature Film Soundtrack
| Recorded    = 
| Length      =  
| Label       = Sony Music India
| Producer    = Gurmeet Ram Rahim Singh
}}

The music was composed by Gurmeet Ram Rahim Singh, and a sequence of the movie takes place as a live concert. The soundtrack album consists of 7 songs, all written and sung by Singh, himself.  

==Track listing==

{| class="wikitable"
|-
! No. !! Title !! Lyrics !! Singer(s) !! Length
|-
| 1 || "Desh" || Gurmeet Ram Rahim Singh
|| Gurmeet Ram Rahim Singh || 3:35
|-
| 2 || "Daru Ko Goli Maro" || Gurmeet Ram Rahim Singh ||  Gurmeet Ram Rahim Singh || 6:34 
|-
| 3 || "Papa The Great" || Gurmeet Ram Rahim Singh || Gurmeet Ram Rahim Singh & Amarpreet Insan || 4:50 
|-
| 4 || "Never Ever" ||  Gurmeet Ram Rahim Singh || Gurmeet Ram Rahim Singh || 4:48
|-
| 5 || "Never Ever remix" ||  Gurmeet Ram Rahim Singh || Gurmeet Ram Rahim Singh || 3:39
|-
| 6 || "Rataan Bataan" || Gurmeet Ram Rahim Singh || Gurmeet Ram Rahim Singh || 4:24
|-
| 7 || "Ram Ram" || Gurmeet Ram Rahim Singh || Gurmeet Ram Rahim Singh || 4:27
|}

==Promotions and Marketing==

The trailer of the film was released on 19 December 2014 and crossed over 1 million views on YouTube within 24 hours. 

On 16 January 2015 for the MSG: The Messenger premier movie at Leisure Valley ground, a large park in Gurgaon city used for exhibitions and conventions. A team from Asia Book and India Book of Records recorded the official number of people gathered as 157,231 for the promotion. 

==Filming==
The film was shot at various locations of Maharashtra, Karnataka and Kerala. The movie casting of 1 million, three hundred-thousand real actors. The film was completed in 67 days.  The cast, had almost no training in film or acting. For the song Never Ever about 125,000 performers continued their shot for three days, and on a count of three, 70 thousand candles were lit within 45 seconds.  Singh held a three-day concert at the Shah Satnam Ji Stadium in Sirsa, Haryana, from 16 August, which is part of the movie.
Singh has also given the storyline for the movie, which is funded by the dera itself and is being directed by Jeetu Arora. A few sequences were be shot in southern parts of the country.  

==Release==
The film was denied the release certificate by the Central Board of Film Certification, India and was sent to a revising committee. The members of the Central Board of Film Certification objected to the portrayal of Singh as a God in the movie.  On 15 January 2015 
the film was cleared for screening by the Film Certification Appellate Tribunal. Singh denied that he had called himself God in the film (at that time known as MSG: Messenger of God). He further added that there was nothing objectionable in his film and that the Central Board of Film Certification had muted just two words.  Punjab and Haryana High Court also allowed the release of the film.  The film was fully cleared by the Central Board of Film Certification with a clean U Certificate and released on 13 February 2015. 

==Reception==

MSG received negative reviews from critics. Bollywood Hungama gave it a rating of 1.5/5, stating that "the film can be watched once only for Gurmeet Ram Rahim Singh Ji Insan."  The Times Of India rated it 1/5,stating that "The one star is strictly for babajis intriguing choice of outfits. A multi-coloured crochet two-piece (tight tees and knee-length pants) takes the cake." Film critic Raja Sen rated it 0/5, stating that "MSG is not a movie"  The Indian Express also rated it 0/5, stating that "the film is excruciatingly awful only for non believers."  Daily News and Analysis|DNA also rated it 0/5, stating that " three hours of torture so painful that you start laughing at yourself"  Saibal Chatterjee from NDTV also rated it 0/5,  while Rohit Vats reviewing for Hindustan Times gave it 0.5/5, saying "watch it only if your survival depended on it". 

The box office gross of the movie has been contested. Bollywood Hungama, Boxofficeindia.com, and other industry sources reported figures as low as  , while sources associated with the film reported results as high as  .      

A report by the Business Standard gave tentative opening day figures of  , citing the limited appeal to non-believers, distribution to less expensive theatres, and competition from Roy (film)|Roy as reasons for the relatively weak opening. The 2015 Cricket World Cup, which kicked off the following day, was also expected to detract from later ticket sales. 

==Protests==
Various Sikh groups have protested the release of the movie and have demanded a ban on it.   

== References ==
 
 

==External links==
*  
*  
*  

 
 
 
 
 