Deceived Slumming Party
{{Infobox film
| name           = Deceived Slumming Party
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         = Edward Dillon D.W. Griffith George Gebhardt
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 7 minutes
| country        = United States Silent English intertitles
| budget         =
}}

Deceived Slumming Party is a 1908 American comedy film directed by D. W. Griffith. Several scenes of tours being conducted in New York. At the end of each scene it becomes obvious that the events of the tours have been set up to shock and defraud the tourists.

==Cast== Edward Dillon as Guide (as Eddie Dillon)   
* D.W. Griffith as Reginald O.C. Wittington     
* George Gebhardt as Chinese    
* Charles Inslee as Chinese  
* Anthony OSullivan as Chinese  
* Mack Sennett as Policeman / Waiter
* Harry Solter as Chinese

==See also==
* D. W. Griffith filmography
* List of American films of 1908

== External links ==
 

 
 
 
 
 
 
 
 
 