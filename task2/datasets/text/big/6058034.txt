The War at Home (1979 film)
{{Infobox film
| name           = The War at Home
| image_size     =
| image	=	The War at Home FilmPoster.jpeg
| caption        =
| director       = Glenn Silber
| producer       = Barry Alexander Brown and Glenn Silber
| videographer   = Charles Francis
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = 1979
| runtime        = 100 min.
| country        = U.S.A. English
| budget         =
| preceded_by    =
| followed_by    =
}} Best Documentary Feature.   

The film focuses on student protests of government policies in the Vietnam War, clashes between students and police, and the responses of politicians and the public to the turmoil. Among the major events included is the Sterling Hall bombing. Intended to destroy the Army Math Research Center in the building, the bombing caused massive destruction to other parts of the building, resulting in the death of a physics researcher, Robert Fassnacht, who was not involved in the Army Math Research Center.

==Cultural influence== samples in Ministry on the album The Mind is a Terrible Thing to Taste.   

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 