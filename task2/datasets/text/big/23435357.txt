Do Knot Disturb
 
 
{{Infobox film
| name           = Do Knot Disturb
| image          = Do Knot Disturb.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = David Dhawan
| producer       = Vashu Bhagnani
| screenplay     = Yunus Sejawal Govinda Sushmita Sen Lara Dutta Ritesh Deshmukh
| music          = Nadeem-Shravan
| cinematography = Vijay Arora
| editing        = Nitin Rokade
| studio         = 
| distributor    = Puja Entertainment (India) Ltd. Reliance Big Pictures
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Do Knot Disturb is a Bollywood comedy film directed by David Dhawan. The film is a remake of the 2006 French film The Valet (French: La Doublure). It released on 2 October 2009, and received generally positive reviews upon release, though failed to make an impact at the box office.

== Plot ==
The film is based on Raj (Govinda (actor)|Govinda) who has a girlfriend, supermodel Dolly (Lara Dutta), and a wife, Kiran (Sushmita Sen). Raj fires Mangu (Rajpal Yadav), a cook, because of attempting to seduce the female staff. To take revenge, Mangu takes a picture of Raj with Dolly and gives it to Kiran. Kiran gets suspicious but then Raj says that Dolly is not his girlfriend. He says that there are three people in the picture, Raj, Dolly and Govardhan (Ritesh Deshmukh). Raj tells Kiran that the guy, who came into the picture as he was walking by, is Dollys boyfriend and tells Govardhan to live with Dolly for a couple of days so Kirans suspicions will go away.

Govardhan gets really troubled when Dollys ex-boyfriend Diesel (Sohail Khan) shows up and threatens Govardhan that, when he finds out who Dollys boyfriend is, Diesel will beat him up. Meanwhile, Kiran has a detective (Ranvir Shorey) follow Dolly and Govardhan. The twist in the story comes when Kiran tells Raj that she is going to Puna to meet her mom, when shes actually making a plan with the detective to catch Dolly and Raj red-handed. As soon as Kiran leaves, Raj calls and books the presidential suite at a hotel for Dolly and him, just like Kiran expected. While Dolly and Raj are enjoying themselves, Kirans detective starts his job. But he gets crushed by the window as he is trying to climb into Raj and Dollys room. When Raj and Dolly notice, they call Govardhan to help get rid of the body because they think he is dead. Just as Govardhan shows up, so does Dollys ex, Diesel. While trying to get rid of the body, Rajs ex-cook, Mangu, shows up (he is now working at the hotel).

The story takes a twist when they discover that the detective is alive. Lucky for them, he has lost him memory from the fall he suffered. Raj tells him he is John Matthews and he is married. Dolly tells him he is John Matthews and divorced. Govardhan tells him he is John Matthews, still a bachelor. Mangu tells him he is his friend and that he has a memory problem; under this pretense, he takes his watch and necklace saying that he forgot that Mangu actually gave it to him. Diesel runs into Govardhan and is about to beat him up when he tells him that Raj is her boyfriend. When Diesel runs into Raj, he tells him Govardhan is Dollys boyfriend. Govardhan finds Dolly and tells her that instead of lying about love, she should be with the one who really loves her, Diesel. Dolly agrees and hugs Govardhan telling him he is a good guy. Unfortunately, Diesel mistakes the situation and starts to beat up Govardhan. Dolly saves Govardhan by telling Diesel that Govardhan convinced her to go back to him. Dolly leaves with Diesel.

Govardhan goes back to his sick mother in the hospital. (She is the one he did this all for. He needs money for her hospital bill and agreed to go along with everything Raj said.) He agrees to marry the woman of his mothers choice, Mala (Rituparna Sengupta) the nurse taking care of her. And Raj convinces Kiran that nothing is going on and they go home as well. In the midst of all this, the detective gets his memory back and goes to Kiran with the proof of her husbands infidelity. As he gets there, Kiran and Raj are about to leave for a second honeymoon. When the detective sees the love between Kiran and Raj, he loses hope and throws the camera with the proof of Raj and Dollys affair. The camera lands on Kirans foot, and she sees the pictures. The story ends with Raj crying and living all alone.

== Cast == Govinda as Raj Saxena
* Sushmita Sen as Kiran
* Ritesh Deshmukh as Govardhan 
* Lara Dutta as Dolly
* Sohail Khan as Diesel Bhai
* Ranvir Shorey as M.P. Nunnu / John Matthews
* Rajpal Yadav as Mangu
* Manoj Pahwa as Buntu
* Himani Shivpuri as Govardhans Mom Ali Asghar as Bagga
* Satish Kaushik as Govardhans Boss
* Rituparna Sengupta as Nurse Mala

== Reception ==
The film received positive reviews from critics. Taran Adarsh rated it 3.5/5 and described it as one of David Dhawans best works yet. However, the film opened to a poor response and grossed $213,525. Overall, a nett gross of  65&nbsp;million, and also faced competition with Wake Up Sid which surpassed Do Knot Disturb completely. The film was rated a flop by Boxofficeindia. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 