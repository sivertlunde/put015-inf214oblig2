The Phantom of the Air
{{Infobox Film
| name           = The Phantom of the Air
| image_size     = 
| image	=	The Phantom of the Air FilmPoster.jpeg
| caption        =  Ray Taylor
| producer       = Henry MacRae
| writer         = Basil Dickey Ella ONeill George H. Plympton
| narrator       =  Craig Reynolds William Desmond
| music          =  John Hickson
| editing        = Alvin Todd Edward Todd
| distributor    = Universal Pictures 1933
| runtime        = 12 chapters (240 min)
| country        = United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Universal Serial movie serial Ray Taylor. Craig Reynolds William Desmond. 

==Plot== William Desmond) invents an anti-gravity device called the "Contragrav," which is sought after by a gang of smugglers led by Mort Crome (LeRoy Mason).  

Crome wants the invention but pilot Bob Raymond (Tom Tyler) comes to the inventors aid, using another of Edwards inventions, the superplane, "The Phantom." Able to control the aircraft from an underground headquarters, Bob foils Cromes plans. A last attempt to get at the inventors work leads to an explosion at his workshop that kills the criminals. Edmunds escapes and is reunited with his daughter (Gloria Shea) and Bob. 

==Chapter titles==
# The Great Air Meet
# The Secret of the Desert
# The Avenging Phantom
# The Battle in the Clouds
# Terror of the Heights
# A Wild Ride
# The Jaws of Death
# Aflame in the Sky
# The Attack
# The Runaway Plane
# In the Enemys Hands
# Safe Landing
 Source:  
==Cast==
* Tom Tyler as Bob Raymond, pilot
* Gloria Shea as Mary Edmunds, Thomas Edwards daughter and Raymonds love interest.
* LeRoy Mason as Mort Crome, head of a gang of smugglers attempting to steal Edmunds "Contragrav" device. Craig Reynolds as Blade, Raymonds sidekick William Desmond as Mr Thomas Edmunds, the inventor of the "Contragrav" and "The Phantom"
* Sidney Bracey as Munsa
* Walter Brennan as "Skid"
* Jennie Cramer as Marie
* Cecil Kellogg as Joe
* Edmund Cobb as Bart, one of Cromes henchmen
* Bud Osborne as Spike, one of Cromes henchmen
* Nelson McDowell as Scotty
* Tom London as Jim
* Ethan Laidlaw as Durkin, one of Cromes henchmen
* Al Ferguson as Al, one of Cromes henchmen

==References==
===Notes===
 
===Bibliography===
 
* Cline, William C. "Filmography". In the Nick of Time. Jefferson, North Carolina: McFarland & Company, Inc., 1984. ISBN 0-7864-0471-X.
* Harmon, Jim and Donald F. Glut. The Great Movie Serials: Their Sound and Fury. London: Routledge, 1973. ISBN 978-0-7130-0097-9.
* Weiss, Ken and Ed Goodgold. To be Continued ...: A Complete Guide to Motion Picture Serials. New York: Bonanza Books, 1973. ISBN 0-517-166259.
* Wynne, H. Hugh. The Motion Picture Stunt Pilots and Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing Co., 1987. ISBN 0-933126-85-9.
 

==External links==
*  
*  

 
{{succession box  Universal Serial Serial 
| before=Clancy of the Mounted (1933)
| years=The Phantom of the Air (1933)
| after=Gordon of Ghost City (1933)}}
 

 

 
 
 
 
 
 
 
 
 