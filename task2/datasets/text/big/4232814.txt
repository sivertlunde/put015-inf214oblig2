In Hell
{{Infobox film
| name = In Hell
| image = InHell.jpg
| image_size =
| caption = DVD cover
| director = Ringo Lam
| producer = Danny Lerner John Thompson David Varod
| writer = Eric James Virgets Jorge Alvarez Steve Latshaw Les Weldon
| starring = Jean-Claude Van Damme Lawrence Taylor Marnie Alton Malakai Davidson Billy Rieck
| music = Alexander Bubenheim
| cinematography = John B. Aronson
| editing = David M. Richardson
| studio = 777 Films Corporation Millennium Films Nu Image Columbia TriStar Home Entertainment
| released = 
| runtime = 96 mins
| country = United States
| language = English
| budget = $17 million
}}
In Hell is a 2003 American prison film directed by Ringo Lam, and starring Jean-Claude Van Damme and Lawrence Taylor.  It is the third and final collaboration between Jean-Claude Van Damme and Hong Kong film director Ringo Lam.

==Plot==
Kyle LeBlanc (Van Damme) is an American working overseas in Russia. When he hears his wife being attacked over the phone, Kyle rushes home to find that he is too late. The man who has killed his wife is found not guilty on lack of evidence (but in fact because the defense bought the judge). Kyle, therefore, takes the law into his own hands and kills the man to avenge his wifes death, and for this he is sentenced to life in prison without parole. He soon befriends inmate Billy Cooper, a 21-year-old who is subjected to constant rape and beatings by prison inmates (they are actually helped by the guards who lock him in cells with these prisoners during the night) and also Malakai, a wheelchair-bound prisoner who claims to know the ins and outs of prison life.

After getting into a brawl with an inmate who provoked him in a way similar to his wifes murderer, Kyle is put in solitary confinement. Then he is transferred to a cell with Inmate 451 (Lawrence Taylor) who has the reputation of killing inmates, and the sadistic head of the guards believes he will do the same to Kyle. However, over time they begin to trust one another.

The majority of the film focuses on organized fights the warden holds among his prisoners, and he amuses himself by betting on these fights to fill his pockets. After savagely killing another prisoner in his first fight, Kyle is continuously forced into more because the warden and guards know he is a sure thing, and slowly he begins to lose his sanity. At one point 451 even asks him "Do you even know who you are? Probably not." Meanwhile, Billy attempts multiple times to escape the prison, first by running during outside work detail, and then again by sneaking off during the Russian Independence Day celebration (the latter of which fails as he is betrayed by Malakai, who informed the guards because his need of special medicine). 451 discovers his betrayal, and in retaliation, pours lighter fluid on him and sets him on fire.

After Billy is locked in a cell with prison fighter Valya overnight, Billy is beaten to within an inch of his life after he spits in Valyas face. Billy later succumbs to his injuries, but before he dies, he whispers to Kyle "Dont let them make you become something youre not." With this advice, Kyle now knows he must fight another battle; the fight for inner peace, as it is the only way he can become the man he once was. Kyle refuses to fight in the next match, and as a result, is hung by his arms outside for all to see as a consequence his actions. However, seeing Kyles courage and his ability to stay strong during his punishment, the other inmates begin to follow suit by refusing to fight. Kyle is released soon from his restraints, and is forced into a fight with Miloc, a gargantuan prisoner kept separate from the general population who Kyle kept hearing through the walls from his time in solitary confinement. During the fight, Kyle manages to knock on a door repeatedly, making Miloc recognize him (as this was his only form of communication, and he embraces him as a friend.) The two then turn on the guards, and ignite a full-scale riot, during which Miloc is killed by gunfire.

Soon, 451 gives Kyle the evidence of all the murders that have happened in this prison for the past 20 something years that he had planned to expose to the US government. While the guards are getting the prisoners under control, 451 shows Kyle a secret passage to the prison garage for their next move. Kyle participates in one final fight, to which he wins, but the guards have indicated they will kill him afterward. When two guards take Kyle to the garage, 451 launches an attack and kills one of them while Kyle holds the other at gunpoint and pins him down to the ground. After taking the key to free himself, Kyle takes one of the guards uniforms to disguise himself and drives off in one of their cars while 451 stays behind to assassinate the warden for his misdeeds. His fate is unknown, but Kyle is able to return to the US and expose the prisons actions. Three months later, the prison was shut down.

==Cast==
*Jean-Claude Van Damme – Kyle LeBlanc
*Lawrence Taylor – 451
*Marnie Alton – Grey LeBlanc
*Alan Davidson - Malakai
*Billy Rieck – Coolhand
*Jorge Luis Abreu – Boltun
*Lloyd Battista – General Hruschov
*Michael Bailey Smith – Valya
*Robert LaSardo – Usup Carlos Gómez – Tolik
*Chris Moir – Billy Cooper
*Paulo Tocha – Viktor
*Raicho Vasilev - Andrei
*Manol Manolov - Ivan
*Valodian Vodenicharov - Dima
*Veselin Kalanovski - Sasha
*Atanas Srebrev - Misha
*Asen Blatechki - Zarik Juan Fernandez - Shubka
*Valentin Ganev - Bolt

==Production==
It was filmed in Sofia, Bulgaria in 36 days between July 18 and August 23, 2002.

==Home media== Region 2 Columbia TriStar Home Entertainment.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 