Susan Slept Here
{{Infobox film
| name           = Susan Slept Here
| image          = Susan-slept-here-poster.jpg 
| caption        = original film poster
| director       = Frank Tashlin
| producer       = Harriet Parsons
| writer         = Steve Fisher (play) Alex Gottlieb (play and screenplay)
| starring       = Dick Powell Debbie Reynolds Richard Myers
| cinematography = Nicholas Musuraca
| editing        = Harry Marker RKO Radio Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2.25 million (US) 
}}

Susan Slept Here is a 1954 American romantic comedy film starring Dick Powell (in his last film role) and Debbie Reynolds. Shot in Technicolor, the film was based on the play of the same name by Steve Fisher and Alex Gottlieb.  The films plotline was later used again by director Frank Tashlin for 1962s Bachelor Flat. 

==Plot== Hollywood screenwriter who has suffered from partial writers block since winning an Academy Award and has been unable to produce a decent script. One Christmas Eve, he receives an unexpected and very unwanted surprise present.
 Vice Squad vagrancy and hitting a sailor over the head with a beer bottle. Not wanting to keep her in jail over the holidays and aware that Mark was interested in writing a script about juvenile delinquency, the kindhearted cop decides to bend the rules (much to the disapproval of his partner). Hanlon suggests that Susan stay with Mark until her arraignment the day after Christmas.

Mark is naturally appalled, but is eventually persuaded to take the girl in. This doesnt go over too well with his long-time fiancée, Isabella Alexander (Anne Francis), the demanding daughter of a United States Senate|U.S. Senator. Isabellas jealousy grows when Susan develops a crush on Mark. Marks secretary Maude Snodgrass (Glenda Farrell), his best friend Virgil (Alvy Moore), and his lawyer Harvey Butterworth (Les Tremayne), do their best to keep the situation under control.

When Harvey lets slip that Susan will likely stay in a juvenile detention facility till she is 18, Mark impulsively takes her to Las Vegas and marries her. The marriage, he explains to his friends, will last for just long enough to convince the judge that Susan has made good. To avoid consummating the marriage, he takes Susan out dancing till she collapses with fatigue, and brings her back to Hollywood.
 Sierra Nevada mountains to work on his script with Maude. The marriage is reported in the newspapers. Enraged Isabella confronts Susan, but is hauled away by Hanlon and his partner.

Some weeks later, Isabella finds Mark in the cabin. She has calmed down, but Mark says he thinks they are not really suited to each other. Susan also arrives, determined to win Mark to a real marriage. She is encouraged and supported by Maude, who still regrets leaving her childhood love behind for an attempted acting career in Hollywood. Susan refuses to sign the annulment papers, while Mark still will not consummate the marriage.

When Susan is seen eating strawberries and pickles, Marks friends assume the worst: that she is pregnant. Susan eventually confesses to Mark that she just likes that combination. Mark has his own confession: he is in love with Susan but is worried by their age difference. Susan tells him all the reasons that they should stay married and pulls him into the bedroom.

==Cast==
* Dick Powell as Mark Christopher
* Debbie Reynolds as Susan Beauregard Landis
* Anne Francis as Isabella Alexander
* Alvy Moore as Virgil, Marks assistant
* Glenda Farrell as Maude Snodgrass
* Horace McMahon as Sergeant Monty Maizel
* Herb Vigran as Sergeant Sam Hanlon
* Les Tremayne as Harvey Butterworth, Marks lawyer
* Mara Lane as Marilyn, Marks neighbor
* Maidie Norman as Georgette, Marks maid
* Rita Johnson as Dr. Rawley, Harveys psychiatrist
* Ellen Corby as Coffee Shop Waitress
* Benny Rubin as Sylvester the janitor
* Barbara Darrow as Miss Jennings, Dr. Rawleys secretary
* Sue Carlton as Coffee Shop Cashier

==Production==
Reynolds later admitted having "a mad crush on" Dick Powell. "He taught me common courtesy and to treat my crew and colleagues with equal respect." 
==Reception==
Reynolds liked the film later stating "that little comedy made $5,500,000, pulled RKO out of the red and then Howard Hughes sold the studio". THE UNSINKABLE DEBBIE REYNOLDS RIDES THE CREST
By HOWARD THOMPSON. New York Times (1923-Current file)   09 Aug 1964: X7.  

==Award nominations== Hold My Hand" (sung by Don Cornell), and for Best Sound, Recording (John Aalberg).   

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 