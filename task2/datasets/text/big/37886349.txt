Aashiqui 2
 

 

 

{{Infobox film
| name           = Aashiqui 2
| image          = Aashiqui 2 (Poster).jpg
| alt            = poster featuring a couple hugging each other
| caption        = Theatrical release poster
| director       = Mohit Suri
| producer       = Mahesh Bhatt Gulshan Kumar  (Presenters)  Mukesh Bhatt Bhushan Kumar Krishan Kumar
| writer         = Shagufta Rafique
| starring       = Aditya Roy Kapoor Shraddha Kapoor
| music          = Original songs: Mithoon Ankit Tiwari Jeet Ganguly Background Score Raju Singh
| cinematography = Vishnu Rao
| editing        = Deven Murudeshwar
| studio         = Vishesh Films
| distributor    = T-Series|T-Series Films  M M Films  (India) 
| released       =  
| country        = India
| language       = Hindi
| runtime        = 140 minutes
| budget         =  
| gross    =  (10 Days) http://www.boxofficeindia.com/Details/art_detail/gundayworldwidebusiness#.UwW9amKSyAo    
}}
 romantic musical drama film directed by Mohit Suri. Starring Aditya Roy Kapoor and Shraddha Kapoor in the lead roles, it was produced by Bhushan Kumar, Krishan Kumar, and Mukesh Bhatt under the T-Series and Vishesh Films banners. Set in the early 2010s, Aashiqui 2 is a love story centring around the turbulent relationship between musicians Rahul and Aarohi, a relationship which is affected by Rahuls issues with alcohol abuse and temperament.
 1990 musical blockbuster Aashiqui, A Star Sunn Raha Hai" topped the charts across various platforms in India. 
 Telugu as Nee Jathaga Nenundali, starring Sachiin J Joshi and Nazia Hussain. The Telugu version is being directed by Jaya Ravindra and produced by Bandla Ganesh

== Plot ==
 alcohol addiction to perform at a stage show in Goa. After nearly completing a song, he is unexpectedly interrupted by Aryan (Salil Acharya) during his performance, Rahul fights him, stops his performance, and drives to a local bar. He meets Aarohi Keshav Shirke (Shraddha Kapoor), a Maharashtrian bar singer who idolizes Rahul. After noticing Aarohi looking at a photograph of Lata Mangeshkar in the bar, he assumes that she wants to become a singer. Impressed by her simplicity and voice, Rahul promises to transform her into a singing sensation, and asks her to never perform again in bars. Aarohi quits her job and returns to Mumbai with Rahul, who convinces record producer Saigal (Mahesh Thakur) to meet her. When Aarohi calls Rahul, he is attacked and injured by some unknown people, and is unable to receive her call. His friend and manager Vivek (Shaad Randhawa) says that news of Rahuls accident should not be leaked to the media, and instead publicises a false story that Rahul has left the country to participate in stage shows. When Aarohi attempts to contact Rahul again, Vivek ignores the calls. After two months of fruitlessly attempting to contact Rahul, a broken Aarohi is forced to sing in bars again because of her family problems.

After recovering from his injuries, Rahul vows to search for Aarohi. He learns that Aarohi is working in a bar again and that Vivek had ignored her calls without informing him. Rahul apologizes to Aarohi and sacks Vivek, and they meet with Saigal for the recording agreement. Rahul begins to train Aarohi, who signs a music contract to sing in films and becomes a successful playback singer. Her family and Rahul are happy. But one day, he goes to a bar and watches Aarohi winning the award but two men who is in the bar begin to gossip that Rahul is using her as a servant. Hearing this he relapses into alcohol addiction. Aarohi, who loves Rahul more than her career, comforts him and make love. Despite Aarohis mothers disapproval, Aarohi moves in with Rahul and things go well until Rahuls addiction worsens, causing him to become aggressive and violent.

To help Rahul fight his alcoholism, Aarohi attempts to rehabilitate Rahul, sacrificing her singing career in doing so. After Saigal reminds them about their dream of Aarohi becoming a successful singer, Rahul orders her to focus on her work. During Aarohis stage show, Rahul meets a journalist backstage, who accuses him of using Aarohi for pleasure and money. Furious, Rahul beats up the journalist and starts drinking. He ends up in the police station, and Aarohi comes to get him out. Rahul overhears Aarohi telling Saigal that she is going to sacrifice her career for him and is ready to give up her celebrity status because Rahul is more important to her. Rahul thinks that he has become a burden in her life, and that leaving her is his only option to save her. The next day he bids her farewell and commits suicide

Distraught by Rahuls death, Aarohi decides to quit her career but Vivek persuades her to stay. He reminds her that Rahul wanted her to become a successful singer and has given his life as he did not want to be a burden on her. Aarohi agrees, and resumes her career as a singer. Later, she signs her name as "Aarohi Rahul Jaykar" in a fans handbook as a tribute to Rahul and her unsung desire to marry him. As rain starts falling, she watches the couple who took her autograph sharing a romantic moment under a jacket as she and Rahul had done when he was alive.

== Cast ==

* Aditya Roy Kapoor as Rahul Jaykar
* Shraddha Kapoor as Aarohi Keshav Shirke
* Shaad Randhawa as Vivek
* Mahesh Thakur as Saigal Uncle
* Shubhangi Latkar as Aarohis Mother
* Chitrak Bandhopadyay as Salim Bhai
* Mahesh Bhatt as Rahuls Father (voice)
* Salil Acharya as Aryan
* Ankit Tiwari as the Music Composer at the studio (Special Appearance)

== Crew ==

* Directed By: Mohit Suri

* Produced By: Bhushan Kumar, Krishan Kumar, and Mukesh Bhatt

* Presented By: Gulshan Kumar and Mahesh Bhatt

* Story, Screenplay and Dialogue Written By: Shagufta Rafique

* Original Song Music : Mithoon, Ankit Tiwari and Jeet Ganguly

* Lyrics: Irshad Kamil, Sandeep Nath, Sanjay Masoom and Mithoon

* Background Score : Raju Singh

* Cinematography: Vishnu Rao

* Production Design and Art Directed By: Rajat Poddar

* Associate Producer : Sakshi Bhatt

* Action Co originator: Abbas Ali Moghul

* Choreographer:

* Sound Designed and Audio Mixed By: Parikshit Lalwani and Kunal Mehta

* Studio: Vishesh Films and T-Series

* Distributor: T-Series and M M Filmz

* Visual Effect: Prime Focus Ltd

== Production ==

=== Development ===

In September 2011, the Indian media reported that Mahesh Bhatt and Bhushan Kumar were keen to remake the 1990 musical blockbuster Aashiqui.  Kumar approached Bhatt for a possible sequel, although it was Shagufta Rafiques melodramatic romantic script which persuaded him that the film had potential as a sequel and decided to proceed with the project.  Given Aashiqui s status in Hindi cinematic history as one of the finest Indian musicals of all time, many expressed concerns towards the decision to remake the film, dubious that the producers could come up with a soundtrack on par with the quality of the 1990 film.    Bhatt stated that they completely resisted the temptation to use the soundtrack of the earlier film, and promised that Aashiqui 2 would revive the era of melodious film music, as Aashiqui had done 22 years ago.   
 Blood Money, was to direct the picture,  but the following month it was announced that Mohit Suri had replaced Mahadkar as director at the last minute. Bhatt confirmed the development, saying "Earlier we had finalised Vishal for the project. But now we have scrapped that idea and found a fresh one. we got Mohit to direct the film".  Several media outlets falsely reported that the film is a remake of the Vishesh films 1990 love triangle Awaargi. However, Mahesh Bhatt denied the rumours and said "Aashiqui 2 is not a remake of any of our films. Its an original script. A very contemporary love story dealing with mature emotions." 

=== Casting ===
 
The films producers  launched a nationwide talent hunt to discover new faces for the film, initially refusing to employ established actors. However, the actors who came to audition were not promising enough for the roles, and the idea was scrapped.    Mahesh Bhatt said, "It was a disastrous talent hunt. We discovered that people lacked the courage to audition. Those who are amateurs went for audition...and people with certain talent were like why should we risk public rejection."    When Suri saw some pictures of Aditya Roy Kapoor and met him, he found Kapoor perfect for the role and cast him to play the male lead.  In June 2012, Shraddha Kapoor was signed to play the female lead.  Bhatt said, "Yes, Shraddha Kapoor is playing the lead with the two boys Aditya Roy Kapoor and Shaad Randhawa. We found her to be very talented. All three actors have extremely challenging dramatic roles"  When asked about replacing new actors with known ones, Suri said "People said I couldnt make a film with new actors and expect an audience to come in. But I was pretty sure I wanted Aditya and Shraddha to play my protagonists. My writer Shagufta Rafique and I saw them as the protagonists. See, Aditya and Shraddha may have had unsuccessful films before. But that never took away from their talent." 

=== Filming ===

Principal photography for the film began in October 2012 with films lead cast.  The film was shot in Goa, Mumbai and Cape Town.  During the filming in South Africa, Shraddha Kapoor needed medical attention after kneeling on broken glass fragments during the scene in which she had to kneel on the floor and talk to her co-star Aditya Roy Kapoor.  Aditya Roy Kapoor also received burns to his hand during the filming of the scene in which they light some Chinese lanterns in Cape Town. 

== Soundtrack ==
 
The soundtrack of the film was composed by Mithoon, Jeet Ganguly and Ankit Tiwari. Lyrics were penned by Irshad Kamil, Mithoon, Sandeep Nath and Sanjay Maasoom. The soundtrack album consists of 11 songs. Upon its release, all the songs became very popular and topped the charts across India especially Tum Hi Ho & Sunn Raha Hai. The soundtrack was released on 4 April 2013.

== Marketing and release ==

The first teaser was released on 22 March 2013, and was well received by critics and audiences.  Unlike other films whose theatrical trailers are released first, the makers of the film chose to release the songs before the trailer.    The first song, "Tum Hi Ho", was released on 23 March 2013. The song became an instant hit with approximately 2 million views on YouTube within 10 days of release, which helped in the marketing of the film.  Various versions of the song were uploaded by amateur singers, guitarists and DJs on social networking sites. {{cite web|title=Aashiqui 2 tops the charts as Tum Hi Ho becomes love anthem of the year
| url=http://www.dailymail.co.uk/indiahome/indianews/article-2310031/Aashiqui-2-tops-charts-Tum-Hi-Ho-love-anthem-year.html|author=Shrivastava, Priyanka|work=Daily Mail|date=16 April 2013|accessdate=22 May 2013}}  It has been trending on Twitter and YouTube since its launch.   

The films preview poster showing Aditya and Shraddha under a jacket in a rain-drenched street with the streetlight casting a glow was released along with music on 8 April 2013.      At the music release event, Aditya and Shraddha recreated the scene from Aashiqui from under a jacket (much like the poster) on the stage.  The theatrical trailer was released in mid-April 2013, two weeks before the films release, and was well received by critics and audiences.  

Unlike most Bollywood films which indulge in months of promotion before the release, Aashiqui 2 had less than three weeks for promotion before its release.  A music concert where singers (who sang songs in the soundtrack album) performed to their respective songs was organised to promote the film.  The makers of the film launched the Aashiqui 2 jackets, as seen in the films poster. Statues resembling the signature image of the couple hiding under the jacket were placed inside various theatres.  Due to the romantic theme of the film, it was originally planned for a Valentines day release on 14 February 2013, but this was postponed because of production delays.   The films new release date was 10 May 2013,  
  but it was released a fortnight early on 26 April 2013 in over 2800 screens across India.   The film was not released in key markets such as UK, US, Canada, Australia and New Zealand. 

== Reception ==

The film received positive to mixed reviews from critics, who praised the performances, chemistry between the lead pair, and the music. Taran Adarsh of Bollywood Hungama rated the film 4 out of 5 stars, stating that it "brings romance back on the Hindi screen intense, pure, selfless and heart wrenching. A stirring account with brilliant moments, bravura performances, strong emotional quotient and addictive music, this ones an absolute must watch for the romantics." He praised the lead casts performances, writing that "&nbsp;...&nbsp;Aditya Roy Kapurs depiction of the intense character is outstanding&nbsp;...&nbsp;  clearly demonstrates his potency as an artiste of caliber and competence. Shraddha also gets to sink her teeth into this challenging character and the attractive youngster is simply amazing, more so towards the demanding moments in the second hour. Furthermore, the chemistry between Aditya and Shraddha is incredible."  Indiatimes gave the film a rating of 3.5 out of 5 and said, "Suri pitches the story with old-world romance, high-drama and well-crafted heart-breaking moments."  Indo-Asian News Service rated the film 3.5 out of 5 and wrote, "Director Mohit Suri traverses the angst-soaked territory with a sincere and deep understanding of the dynamics that destroy love and trust between couples in the glamorous and competitive profession", and that, "Aashiqui 2 makes us grateful for the movement of the love story away from the standard Romeo & Juliet format into the dark destructive domain of A Star Is Born."  Komal Nahta gave it 3.5 stars out of 5 stating " Aashiqui 2 is an entertaining film with hit music and the sacrificing nature of the heroine as it biggest trump cards." 

The film also received some mixed reactions from critics. Writing for Hindustan Times, Anupama Chopra rated the film 2.5 out of 5 and believed that the film didnt fulfill its potential, but said, "Its an interesting scenario and Suri and his actors set it up well. Aditya gives Rahuls angst a certain charm. He is earnest and broken. And the real triumph here is Shraddha, whose porcelain face has a haunting vulnerability. Shes very good as the woman in the throes of a grand passion who believes that love will show the way."  Resham Sengar rated the film 2.5 and questioned the logic behind the script and believed that several of the scenes either dragged on excessively or were too abrupt, which affected the quality of the entire film.  India Today also gave the film a rating of 2.5 stars out of four, and argued that the film was only a success because of its soundtrack, saying that the film "merely banks on the power of saleable music and the novelty of a fresh cast to enable brothers Bhatt, Mahesh and Mukesh,   make maximum moolah within minimum budget as they have done all along." 

== Box office ==
 blockbuster after its three-week box office run. As of 20 May, the film was the second-highest grossing Hindi film of 2013 and the highest-grossing film produced by Vishesh Films.   According to Box Office India, Aashiqui 2 is the best trending film at the box office since 3 Idiots as the fourth weeks collections were nearly   nett, which was more than every film released in the last ten years apart from 3 Idiots. The fourth week collections of the film were the third highest of all time.  The film collected   nett approx in its fifth week.  The film went on to gross approximately   in its sixth week at the domestic box-office.  Internationally, the film had a limited release meant the film was only released in UAE and Pakistan.  The film collected   worldwide in its fourth week.    During the entire theatrical run, the film earned   worldwide. 

== Awards ==

Filmfare Awards  

* Winner,  , Mithoon and Jeet Ganguly
* Winner,   (Tum Hi Ho)
* Nominated,   Sunn Raha Hai)

Screen Awards  

* Winner,   (Tum Hi Ho) Sunn Raha Hai)
* Winner,   & Shraddha Kapoor
* Nominated,  , Mithoon and Jeet Ganguly
* Nominated, Best Actress Female : Shraddha Kapoor
* Nominated, Most Popular Actor Male : Aditya Roy Kapur
* Nominated, Most Popular Actress Female : Shraddha Kapoor

== References ==
 

== External links ==
 

*  
*  

 

 
 
 
 
 
 
 
 
 
 
 