Come as You Are (film)
{{Infobox film
| name           = Come as You Are
| image          = ComeAsYouAre2011Poster.jpg
| caption        = Film poster
| director       = Geoffrey Enthoven
| producer       = Mariano Vanhoof
| executive producer = Asta Philpot
| writer         = Pierre De Clercq Mariano Vanhoof Asta Philpot
| starring       = Gilles De Schryver Robrecht Vanden Thoren Tom Audenaert
| music          = Stijn Meuris
| cinematography = Gerd Schelfhout
| editing        = Philippe Ravoet
| studio         =
| distributor    =
| released       =  
| runtime        = 108 minutes
| country        = Belgium
| language       = Dutch, French, Spanish
| budget         =
| gross          =
}}
Come as You Are, also known as Hasta la Vista, is a Belgian drama film released in 2011 featuring Gilles De Schryver, Robrecht Vanden Thoren, Tom Audenaert and Isabelle de Hertogh. The film is directed by Geoffrey Enthoven,  written by Pierre De Clercq and based upon a real-life experience. 

==Plot==
The Flemish boys Lars, Philip and Jozef are somewhere between 20 and 30 years of age and each has a physical handicap. Jozef is almost blind and needs to use a Magnifying glass|magnifier. Philip suffers from paraplegia. He can only move his head and can use one hand which gives him the strength to control his automated wheelchair. Lars has an incurable brain tumor a side effect of which is that he is restricted to a wheelchair as a result of his increasing paralysis.  The three are good friends and visit each other frequently.

Philip, who is able to control a computer keyboard with a mouthpiece, had a conversation with a man who lost his legs during his last holiday at the Dutch sea-side. That man frequently visits a luxury brothel in Spain which is specialized to host men with a handicap. Philip convinces Lars and Philip to organize a trip to Spain "as they do not want to die as a virgin". Of course, they have to convince their parents but are afraid to tell them the real reason about the trip. Thats why they deceive them and tell them that they are only going to visit some wine gardens in France and Spain. As Lars already made a well-organized plan about the trip and found a tour guide who is specialized in "holidays for people with a handicap" the parents eventually agree.

Some days before departure, Lars goes to his doctor for approval to make the trip, but it seems his brain tumor has grown. His doctor advises to cancel his trip. Philip and Jozef initially cancel as well since they do not want to abandon Lars. Lars however insists that the trip should continue, so they decide to leave sneakily. However, the tour operator cant make the trip any more as it was cancelled by the parents, and does not want to jeopardize his professional and private life by ignoring the wishes of the parents. The tour operator gets pity and decides to send Claude, who also owns a specialized bus.

They are surprised when Claude seems to be an oversized, unattractive, grouchy woman who does not understand Flemish (as she lives in Brussels). Philip and Lars dislike Claude and always use abusive Dutch language to describe "the insensible mammoth-looking woman". Neither Philip nor Lars speak French language|French, so Jozef becomes the interpreter. He is more friendly towards Claude and cant laugh with the remarks of Philip and Lars.

Claude seems not to be very sociable. She only drives the boys to their different destinations and separates herself during each break. She does want to help the boys, but is always refused. The three are convinced they do not need Claude as "a replacement for their parents" and can handle the situation. Soon, they discover this is not that easy.

Claude insists that the boys must accept her as they do need her help as a nurse. The three are surprised when they discover Claude does understand the Dutch language. They are embarrassed and ask Claude to forgive their misbehaviour, which she accepts. The following days are very entertaining and Claude decides to overnight in open sky. The next day, she drives them to a hotel. The three are led to a meeting room and are astonished when they meet their parents. They were able to locate their sons via the original tour operator who gave them the mobile number of Claude. Claude, who got a call of the parents, could not refuse the parents request to drive them to this hotel. She is just out of prison due to maltreatment of her previous husband and does not want to get into new problems. The parents want to take their children back to Belgium. Philip and Lars get very aggressive and scream they do not want to be treated like children. Lars insists that his parents let them continue the trip. Eventually, they approve of their sons continuing the trip to Spain with Claude as a guide. Once they arrive Claude surprises them by taking them to a villa instead of the foreseen bungalow, which is a present from the parents.

The boys decide that they want to see the night-life of the Spanish town. Claude brings them to the centre and allows them to take off by themselves. They try to strike up a conversation with a couple of girls and invite them to go to a restaurant together. Although the girls provide them with a recommendation, they are reluctant to join and state they dont have enough time, so the boys take off on their own. On the way to the restaurant, Lars is taken aside by a different girl which he had met before in a shop. He instructs Philip and Jozef to continue without him and takes off with the girl. Later on they are talking but Lars gets sick and gets embarrassed. When Philip and Jozef return to the bus after dinner, they find Lars already there, clearly upset. Philip starts ranting about Lars ditching them, but Jozef tells him to shut up.

The first visit at the brothel is a disaster as Lars gets ill. During their next visit, Jozef does not want to go as he fell in love with Claude. This seems to be mutual and the two have sex in the tour bus. Lars and Philip enjoy their visit in the brothel. That night, the group decides to sleep on the beach. Next morning, Philip finds a deceased Lars. Philip is picked up by his parents to travel to Belgium by aeroplane, whereas Jozef decides to stay with his new girlfriend Claude who drives him back to Belgium on her bus.

==Awards==
* "Come as You Are" was shown as opener for the Ostend Film Festival  
* The movie won the "Grand Prix des Amériques" at the international movie festival in Montréal. It also got the "Peoples Choice Award" and a prize given by the Ecumenical Jury.  
* The movie won a "Golden Spike", which is the first prize for the best film at the international movie festival in Valladolid. In same contest, it also won the "Youth jury award" 
* It was nominated in the "Alice nella città" of the internationel movie festival in Rome.
* It won the "Prix de publique Europe" at the film festival in Alpe dHuez  
* It was nominated for the Magritte Award for Best Flemish Film in Coproduction.
* The movie won the European Film Award - Peoples Choice Award for Best European Film|Peoples Choice Award for Best European Film in 2012.

==References==
 

==External links==
* 
*   at  

 
 

 
 
 
 
 
 
 