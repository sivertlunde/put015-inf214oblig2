Gambling City
{{Infobox film 
| name = Gambling City
| image = Gamblingcity.jpg
| caption = DVD Cover Art
| director = Sergio Martino
| producer = Luciano Martino
| writer = Ernesto Gastaldi
| narrator = 
| starring = Luc Merenda,  Enrico Maria Salerno
| music = Luciano Michelini
| cinematography = Giancarlo Ferrando
| editing = Eugenio Alabiso	 	 
| distributor = 
| released = 
| runtime = 101 mins
| country = Italy Italian
| budget = 
| preceded_by = 
| followed_by =
}}
 1975 Italian poliziotteschi film by Sergio Martino. It stars Luc Merenda,  Enrico Maria Salerno, Dayle Haddon and Corrado Pani. 

Gambling City is one of several European heist films produced in response to the popular success of American films in the genre like The Sting (1973).  The film features many of the genres standard conventions, including a Byronic hero who acts as a social bandit by practicing confidence tricks, the heros forbidden love with a socially repressed damsel in distress (who may or may not exhibit facets of the femme fatale) and a morally corrupt dandy as villain and the heros foil (literature)|foil. The film was co-written by Martino and Ernesto Gastaldi,  and produced by Martino’s brother, Luciano. 

==Plot==

Gambling Citys story centers on the exploits of master poker player and card sharp Luca Altieri (Merenda); Altieri begins working for underground illegal gambling kingpin "The President"  after running a scam at a high-stakes poker table in The Presidents casino; soon after entering The Presidents employ, however, Altieri enters into a violent competition with The Presidents hot-tempered, self-entitled son and heir, Corrado (Pani), for the hand of Corrados girlfriend, Maria Luisa (Haddon). 

===Opening Scenes===

Gambling City opens with Altieri entering an illegal gambling parlor in Milan and taking a seat beside several tuxedoed patrons at its high-roller table. In contrast to his slickly attired opponents Altieri is dressed in a threadbare cardigan and slacks and has  a carnation in his lapel. Noticing the newcomers obvious lack of means at a table where a single chip is worth 10,000 lira, one player asks Altieri if he has enough money to back his bets. To this Altieri responds, "Win or bust."

Altieri then proceeds to execute his scam by initially pretending to be a poker novice. He fumbles his cards and requests a re-deal despite his having four-of-a-kind queens, explaining that he doesnt trust receiving such a strong hand so early. Within minutes he has fleeced his fellow players, and while waiting to make his exit via elevator, he reveals his scheme to a security guard standing beside him. In response, the security guard tells Altieri the elevator is not an exit at all and escorts the scamp into it. They then take the elevator to the casinos basement where Altieri meets The President, Corrado and Maria Luisa. 

==Production==

 

==Releases==
The film released on Region 0 NTSC DVD by NoShame films in 2005 as part of the "Sergio Martino Collection." The DVD is currently out-of-print. 

==Reception==

Gambling City received little attention in the European or American press at its release, but experienced a revival in interest when now-defunct Italian cult-film company NoShame Films re-released it on DVD in 2005. At the time of its re-release New York Times reviewer Dave Kehr noted the "powerful, rhythmic sense of violence" the film shares with other Martino films. But Kehrs review primarily focused on the film as an early example of Martinos talents. Kehr praised the absolute lack of sentimentality displayed by a director who would go on to make over 50 feature-length films, including spaghetti westerns like A Man Called Blade (1977) and science-fiction fantasies like 2019: After the Fall of New York (1983). 

Since the films release on DVD, however, no other mainstream critics have commented on it, and what attention it has received from the blogosphere has been mostly negative. TwitchFilm.net reviewer "logboy" called it "a fairly odd experience," but qualified his disparagement with, "It has its saving graces in amount substantial enough to make it an interesting experience in many isolated regards."  And Gambling City critic Matt Hwang called the films score "something between the distorted wind-up chime of a broken Jack-in-the-box|Jack-in-the-Box and the Merry-go-Round accordion." 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 