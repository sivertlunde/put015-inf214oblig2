Varsham (2004 film)
{{Infobox film
| name           = Varsham
| image          = Varsham.jpg
| director       = Sobhan
| writer         = Paruchuri Brothers Veeru Potla M.S. Raju |
| starring       = Prabhas, Trisha Krishnan|Trisha, Tottempudi Gopichand|Gopichand, Prakash Raj |
| producer       = M.S. Raju
| music          = Devi Sri Prasad
| cinematography = S. Gopal Reddy
| distributor    = Sumanth Arts
| released       = 14 January 2004
| runtime        = 166 minutes
| country        = India
| language       = Telugu
| budget         =      
| gross          =   
}}
 Tollywood film Tamil as Mazhai starring Jayam Ravi and Shriya Saran and in Oriya as Barsa My Darling. It was dubbed in Hindi as Baarish The season of love.

==Plot==
The film was loosely based on the Ramayana. Venkat (Prabhas) and Sailaja (Trisha Krishnan|Trisha) are youngsters who first meet on a train during a rain shower. Venkat keeps bumping into Sailaja coincidentally every time it rains. This makes them both feel that it is perhaps the rain that keeps bringing them together, and they start to fall in love. Venkat is mesmerized by Sailajas beauty, charisma, and childlike behavior. Sailaja, also, is impressed by Venkat. Due to unfortunate circumstances the two are drawn apart. At the same time that the two meet, Sailaja catches the eye of antagonist Bhadranna (Tottempudi Gopichand|Gopichand) who also becomes infatuated with Sailaja. Venkat and Sailaja meet in Warangal again and believe that they are meant for each other because it rains every time they meet. Ranga Rao (Prakash Raj), Sailajas dad, is a typical black sheep with all kinds of bad habits. In the process, he is ready to marry off his daughter to Bhadranna for money. Ranga Rao plants certain ego problems in the minds of Venkat and Sailaja in order to break them up. This causes Venkat to leave Warangal for Vizag. And Sailaja prepares to become an actress (her father convinces her). Later on, Bhadranna kidnaps Sailaja. The rest of the story is about how Venkat rescues Sailaja from the bastion of Bhadranna and how he clears up the misunderstanding with Sailaja in the process.

==Allegory==
  was selected as lead heroine marking her first collaboration with Prabhas.]]
Throughout the movie, there is strong allusion to the Hindu epic Ramayana. Indeed, the story parallels the Ramayana in many ways, Venkat being an analogous Rama, Sailaja his Sita and Bhadranna being Ravana analogue. Towards the beginning of the movie, as Ranga Rao enters Bhadrannas residence for the first time, he proclaims - "లంక లాగ ఉన్నదే" - "this looks just like Lanka". Later, during a live play of the Ramayana at his house, upon seeing Rama take Sita back from Ravana, Bhadranna stops the actors from continuing the play and demands the script be changed immediately by having Ravana tie the mangalasutra around Sitas neck.
Similarly, at the end of the movie, during another live play of the Ramayana, Rama shoots a large Ravana effigy with a burning arrow, and it is this burning collapsing Ravana that ultimately kills Bhadranna, leaving us with the final allegory of Rama killing Ravana and rescuing Sita.

==Shooting spots==
Venkat (Prabhas) and Sailaja (Trisha Krishnan|Trisha) confirm their love at a Temple while its raining. This vital scene was shot at the Nandi statue of Thousand Pillar Temple in Warangal city. "Mellaga" song was shot at the same temple.

==Awards==
;Nandi Awards Best Female Playback Singer - K.S. Chitra - Nuvvostanante Best Choreographer - Prabhu Deva Best Audiographer - Madhusudan Reddy Filmfare Awards Best Film - M.S. Raju Best Actress Trisha
* Best Music Director - Devi Sri Prasad Best Female Playback Singer - K.S. Chitra Best Cinematographer - S. Gopal Reddy

;Santosham Film Awards Best Film - M.S. Raju Best Actress Trisha
* Best Young Performer - Prabhas Best Music Director - Devi Sri Prasad Best Cinematographer - S. Gopal Reddy Best Choreography - Prabhu Deva Best Screenplay - M.S. Raju Best Dialogue - Paruchuri Brothers

==Soundtrack==
The soundtrack was composed by Devi Sri Prasad. The lyrics to all the songs were written by Sirivennela Sitaramasastri.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length =
| title1 = Nuvvosthanante
| extra1 = K. S. Chithra|Chitra, Raqeeb Alam
| length1 = 5:32
| title2 = Mellagaa Sumangali
| length2 = 5:19
| title3 = Nizam Pori Sunitha Rao
| length3 = 4:12
| title4 = Langa Voni Usha
| length4 = 3:52
| title5 = Neeti Mullai Sagar
| length5 = 1:20
| title6 = Kopama
| extra6 = Karthik (singer)|Karthik, Shreya Ghoshal
| length6 = 4:50
| title7 = Joole Joole Kalpana
| length7 = 5:02
}}

==Box-office performance==
* The film celebrated a 50-day run in 200 centers 100 days in 68 centers film did 22 Crore business. 

==References==
 

==External links==
*  

 
 
{{succession box
| title=Filmfare Best Film Award (Telugu)
| years=2004
| before=Okkadu
| after=Nuvvostanante Nenoddantana}}
 
 

 
 
 
 
 
 