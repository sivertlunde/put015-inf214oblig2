A Bird in a Bonnet
{{multiple issues|
 
 
 
}}

{{Infobox Hollywood cartoon
| cartoon_name = A Bird in a Bonnet Sylvester and Tweety)
| director = Friz Freleng
| story_artist = Warren Foster Arthur Davis Virgil Ross
| voice_actor = Mel Blanc June Foray (uncredited) Daws Butler (uncredited)
| musician = John Seely John Burton, Sr.
| distributor = Warner Bros. Pictures
| release_date = September 27, 1958 (USA)
| color_process = Technicolor
| runtime = 6:30 (one reel)
| movie_language = English
}}
 Sylvester and Tweety.  Released September 27, 1958, the cartoon is directed by Friz Freleng. The voices were performed by Mel Blanc and June Foray.

==Plot==
In New York City, Granny is in a hat shop looking for a new hat.  The sales lady has her try on several hats, but Granny seems unsatisfied with each choice (a Napoleon chapeau makes her imitate Napoleons hand in coat pose and chuckle "Not tonight, Josephine!"). Meanwhile, Sylvester is chasing Tweety outside and Tweety makes his way into the hat shop.  Just as Tweety hides at a table, Sylvester runs in and tries to rummage through the hats to look for him, but the sales lady catches him in the act, and chases him out.  Just as the sales lady comes to the table where Tweety is hiding, Tweety stands real still on a hat, making the sales lady think that hes a cute little stuffed bird on a hat.  After Granny tries it on, she thinks the same thing and buys the hat.

Tweety soon realizes the joys of being said "stuffed bird" has a two-fold purpose, the second being that it is perfect refuge from Sylvester. But as usual, the puddy tat does everything to get at the bird, first following Granny out of the hat shop. The first time, Granny turns around, but sees Sylvester pretending to sleep.  The second time, Granny quickly gets wise and swats the cat with her umbrella.

Other failed attempts for Sylvester to get Tweety include:

* Perching himself atop a delivery truck. Just as he is about to grab dinner, the truck speeds away, requiring the cat to take the crosstown bus to return downtown. Sylvester then barely avoids getting hit by an oncoming car, causing his heart to beat fast and his fur to go white.
* Hiding inside an English gentlemans hat. The man walks up beside Granny and makes a snide remark, just as Sylvester is making a grab for Tweety. Granny uses her umbrella to clout the man and — unwitting — the cat.
* Following Granny into Lacys Department Store (a play on Macys) and into an elevator. The cats tail gets stuck in the elevator door, and when he finally disembarks the elevator, his tail has been stretched from the stores eight floors. Ed Norton-esque sewer worker).
* Later, using a bellows to blow Grannys hat from her head, causing it to go onto the street. Sylvester barely avoids getting hit by several cars and recovers the hat, but he is struck by a motorscooter driving from an alley.  Granny manages to retrieve the hat safely.

In the ending gag, Sylvester uses a fishing rod and reel to latch onto the hat. Granny gets into a taxi, and Sylvester is pulled away by the speeding driver. After being an unwilling car skier for several city blocks (and nearly getting hit by two trucks), Sylvester eventually realizes he needs to reel himself in ... which he does to open the taxis sunroof and grab Tweety. Just after saying his only line in the cartoon — "Now Ive got you, buster!" — the car drives into the Holland Tunnel, where the cat hits his head against the side of the entrance; the bird flies out of his hand and back safely onto the taxicabs roof. "You know, I wose more puddy tats that way!" remarks Tweety as the cartoon ends.

==Score==
"A Bird in a Bonnet" is one of six cartoons scored with stock music by John Seely from the Capitol Records Hi-Q (production music)|Hi-Q library because of a musicians strike in 1958; the others are Weasel While You Work, Hip Hip-Hurry!, Hook, Line and Stinker, Gopher Broke, and Pre-Hysterical Hare. This cartoon is the only Friz Freleng cartoon to have a Seely score; two others were directed by Chuck Jones (both starring Wile E. Coyote and Road Runner), while the remaining three were helmed by Robert McKimson.

==Succession==
 
{{succession box |
before= A Pizza Tweety Pie | Tweety and Sylvester cartoons |
years= 1958 |
after= Trick or Tweet |}}
 

==References==
 
* Friedwald, Will and Jerry Beck. "The Warner Brothers Cartoons." Scarecrow Press Inc., Metuchen, N.J., 1981. ISBN 0-8108-1396-3.

 
 
 
 