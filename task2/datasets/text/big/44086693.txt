Arangum Aniyarayum
{{Infobox film
| name           = Arangum Aniyarayum
| image          =
| caption        =
| director       = P Chandrakumar
| producer       = RS Prabhu
| writer         = Dr. Balakrishnan
| screenplay     = Dr. Balakrishnan Seema
| music          = A. T. Ummer
| cinematography = Anandakkuttan
| editing        =
| studio         = Sree Rajesh Films
| distributor    = Sree Rajesh Films
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, Seema in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Sankaradi
*Sukumaran
*KPAC Sunny Seema

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Sathyan Anthikkad. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ilam Thennalo Pularithan || Vani Jairam || Sathyan Anthikkad || 
|-
| 2 || Maanishaada || Jolly Abraham || Sathyan Anthikkad || 
|-
| 3 || Ponmukilin || K. J. Yesudas || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 

 