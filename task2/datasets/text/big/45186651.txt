A Narmada Diary
A Narmada Diary is a 1995 documentary on the struggle of those adversely impacted by the Sardar Sarovar Dam project.  It was directed by Anand Patwardhan and Simantini Dhuru and released in the year 1995. A record from about 1990-1993, of the measures adopted and hardship faced by the Narmada Bachao Andolan movement and the people inhabiting the place affected was presented.  The documentary won the "Filmfare Award for Best Documentary, 1996" and the "Grand Prize" at Earth-Vision Film Festival,  Tokyo, 1996. In spite of winning the central governments national awards, the movie was not allowed to be shown on National TV 

==See also==
*Drowned Out, a 2002 documentary film about opposition to the dam

==References==
 

 
 
 
 
 
 