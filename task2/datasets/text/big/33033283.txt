Pursued (1934 film)
{{Infobox film
| name           = Pursued 
| image          = 
| caption        = 
| director       = Louis King
| producer       = Sol Wurtzel
| writer         = 
| narrator       = 
| starring       = Rosemary Ames Victor Jory
| music          = David Buttolph Hugo Friedhofer Samuel Kaylin 
| cinematography = L. William OConnell
| editing        = 
| distributor    = Fox Film Corporation
| released       = August 24, 1934
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Pursued is a 1934 drama film produced and distributed by Fox Film Corporation. It is based on a story from the Saturday Evening Post, The Painted Lady by Larry Evans. It was previously filmed by Fox as a silent When a Man Sees Red in 1917. 

==Cast==
*Rosemary Ames - Mona
*Victor Jory - Beauregard
*Russell Hardie - David Landeen George Irving - Dr. Otto Steiner

unbilled
*Chan Jim - Bird Seller
*Nora Cecil - Tourist Welfare Worker
*James Dime - Malays
*Miss Frieda - Dancer
*Douglas Gerrard - English Sailor
*John Gough - Mate
*Virginia Hills - Percentage Girl
*Pert Kelton - Gilda
*Elsie Larson - Percentage Girl
*Margaret Mann - Tourist
*Torben Meyer - Hansen
*Lucille Miller - Percentage Girl
*Stanley Price - Servant
*Constance Purdy - tourist Welfare Worker
*Tom Ricketts - Tourist
*John Rogers - Ships Steward
*Betty Schofield - Dancer
*Allan Sears - Mate
*Harry Semels - Cafe Proprietor
*Ella Serrurier - Dancer

==References==
* 

==External links==
* 
* 

 
 
 
 
 
 
 


 