Song of the City
{{Infobox film
| name           = Song of the City
| image          = Song of the City 1937.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Errol Taggart
| producer       = Michael Fessier Lucien Hubbard
| screenplay     = Michael Fessier 
| story          = Michael Fessier 
| starring       = Margaret Lindsay Dean Jagger J. Carrol Naish Nat Pendleton Dennis Morgan Marla Shelton
| music          = William Axt Leonard Smith
| editing        = John Baxter Rogers 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Song of the City is a 1937 American musical film directed by Errol Taggart and written by Michael Fessier. The film stars Margaret Lindsay, Dean Jagger, J. Carrol Naish, Nat Pendleton, Dennis Morgan and Marla Shelton. The film was released on April 2, 1937, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Margaret Lindsay as Angelina Romandi
*Dean Jagger as Paul Herrick 
*J. Carrol Naish as Mario
*Nat Pendleton as Benvenuto Romandi
*Dennis Morgan as Tommy 
*Marla Shelton as Jane Lansing
*Inez Palange as Mrs. Mama Romandi
*Charles Judels as Mr. Pietro Papa Romandi
*Edward Norris as Guido Romandi
*Fay Helm as Marge
*Frank Puglia as Tony

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 