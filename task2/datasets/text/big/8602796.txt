Anupama (film)
 
{{Infobox film
| name =Anupama
| image =
| image size =
| caption =
| director = Hrishikesh Mukherjee
| producer = L. B. Lachman
| writer =  Rajinder Singh Bedi  Biren Tripathy (Dialogue Director)
| story      = Hrishikesh Mukherjee
| screenplay = Bimal Dutta  D.N. Mukherjee
| dialogue   = Rajinder Singh Bedi  Biren Tripathy (Dialogue Director)
| narrator =
| starring = Dharmendra Sharmila Tagore Deven Verma
| music = Hemant Kumar
| cinematography = Jaywant Pathare
| editing = Das Dhaimade
| distributor =
| released =  
| runtime = 148 minutes
| country = India Hindi
| budget =
}} 1966 Hindi film directed by Hrishikesh Mukherjee. The movie stars Dharmendra, Sharmila Tagore, Shashikala, Deven Verma and Durga Khote. The music was composed by Hemant Kumar.

The film was critically acclaimed and was nominated for several Filmfare Awards and did Above Average business at the Indian Box Office. 

==Plot==
Mohan Sharma (Tarun Bose), a successful businessman in Bombay, marries late in life, and is leading a happy married life, when, unfortunately, his wife dies during childbirth leaving behind a young daughter, Uma (Sharmila Tagore), whom he cannot bear to see, except when he is drunk! Naturally, the daughter grows up all by herself and becomes highly introvert. As time passes, Mohan Sharmas health starts failing due to overwork and alcoholism; doctors suggest change of weather to a hill-station, Mahabaleshwar. 

Meanwhile, Arun (Deven Verma), son of Mohan Sharmas friend Hari Mehta, and who is set to marry Uma, returns home after studying engineering abroad for five years, and joins them along with his friend, Ashok (Dharmendra), a writer and a teacher. Here things change when young Ashok enters Umas life, but she cannot do anything to jeopardize the already fragile relationship between her and her father.

==Cast==
* Dharmendra - Ashok
* Sharmila Tagore - Uma Sharma
* Shashikala - Anita Bakshi Annie Dulari - Sarla David - Moses
* Tarun Bose - Mohan Sharma
* Deven Verma - Arun
* Durga Khote - Ashoks Mother
* Brahm Bhardwaj - Suresh Bakshi
* Surekha Pandit - Mother of Uma Sharma

==Soundtrack==

The music for the movie was composed by Hemant Kumar and the lyrics were written by Kaifi Azmi.  The movie includes popular songs such as Kuch Dil Ne Kaha sung by Lata Mangeshkar and Ya Dil Ki Suno Duniyawalo by composer Hemant Kumar himself. 

{| class="wikitable"
|-
! Song
! Singer/s
|-
| Kuchh Dil Ne Kaha 
| Lata Mangeshkar
|-
| Dheere Dheere Machal
| Lata Mangeshkar
|-
| Bheegi Bheegi Faza
| Asha Bhosle
|-
| Kyon Mujhe Itni Khushi
| Asha Bhosle
|-
| Ya Dil Ki Suno Duniyawalo
| Hemant Kumar
|}

==Awards==
* 1967:  . 
;Nominated
* Best Film: Hrishikesh Mukherjee
* Best Director: Hrishikesh Mukherjee
* Best Story: Hrishikesh Mukherjee
* Best Supporting Actress: Shashikala  

==References==
 

==External links==
*  
*   at Rediff.com.
*  

 

 
 
 
 
 
 
 