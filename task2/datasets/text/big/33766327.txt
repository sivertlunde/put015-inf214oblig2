Every Bastard a King
 
{{Infobox film
| name           = Every Bastard a King
| image          = 
| caption        = 
| director       = Uri Zohar Topol
| writer         = Eli Tavor Uri Zohar
| starring       = Yehoram Gaon
| music          = 
| cinematography = David Gurfinkel
| editing        = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}}
 Best Foreign Language Film at the 41st Academy Awards, but was not accepted as a nominee. 

==Plot==
The story focuses on an American reporter who visits Israel with his girl friend. While there,
he becomes friendly with the Arabs and Israelis. Before long things change and he soon learns 
that war will break out. The film is set just before the 1967 Six Day War.

==Cast==
* Yehoram Gaon as Yoram
* Oded Kotler as Raphi Cohen
* Pier Angeli as Eileen William Berger as Roy Hemmings
* Ori Levy as Foreign Office Official (as Uri Levy)
* Reuven Morgan as The Photographer
* Ariela Shavid as Anat

==See also==
* List of submissions to the 41st Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 