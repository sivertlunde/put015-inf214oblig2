Mumbai Mirror (film)
 
{{Infobox film
| name           = Mumbai Mirror
| image          = Mumbai Mirror film poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Ankush Bhatt
| producer       = Raina S Joshi
| starring       = Gihana Khan Prakash Raj Vimala Raman Aditya Pancholi Mahesh Manjrekar  Prashant Narayanan Raja Bherwani
| music          = Amjad Nadeem
| writer         = Ghalib Asad Bhopal
| cinematography = Harshad Mhambre
| editing        = Faisal Mahadik Imran Mahadik
| studio         = Vedia & Entertainment Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         =   
| gross          = 
}}
Mumbai Mirror is a 2013 Indian Hindi film directed by Ankush Bhatt starring Sachiin J Joshi, Prakash Raj, Gihana Khan, Vimala Raman, Aditya Pancholi, Mahesh Manjrekar, Raja Bherwani and Prashant Narayanan. It is a crime thriller which shows conflict between powerful, corrupt dance bar owners and the Mumbai police. Mumbai Mirror released on 18 January 2013 and was an average grosser at the Box Office.

==Cast==
* Sachiin J Joshi as Abhijeet Patil
* Prakash Raj as Shetty
* Gihana Khan as Rani
* Raja Bherwani as nikhil
* Vimala Raman as Jia
* Mahesh Manjrekar as ACP Gaitonde
* Aditya Pancholi as Durrani
* Prashant Narayanan as Manish

==Production==

===Casting===
Sachiin J Joshi plays a "quirky cop".   Mallika Sherawat who agreed to play the lead opposite Sachiin J Joshi, walked out of the project.   Gihana Khan was chosen as female lead.  Prakash Raj has been signed on for the villain role. 

==Soundtrack==
The soundtrack of Mumbai Mirror is composed by Amjad Nadeem and the album contains six tracks. Musicperk.com rated the album 6.5/10 quoting "Average album. Definitely not one of the best". 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track # !! Song !! Length !! Singer(s) !! Lyrics
|-
| 1
| "Doom Pe Lakdi"
| 4:30
| Sukhwinder Singh, Sachiin J Joshi, Nadeem Khan & Preeti & Pinky
| Shabbir Ahmed
|-
| 2
| "Marjawa"
| 5:33
| Sonu Nigam
| Shabbir Ahmed
|-
| 3
|"Govinda Ala Re"
| 5:19 Wajid Ali
| Shabbir Ahmed
|-
| 4
| "Thumka"
| 4:26
| Shreya Ghoshal
| Satya Prakash
|-
| 5
| "Blunder"
| 4:03
| Ritu Pathak
| Naveen Tyagi
|-
| 6
| "Marjawa"
| 5:30
| Sayantani Das
| Shabbir Ahmed
|-
|}

==References==
 

==External links==
*  

 
 
 


 