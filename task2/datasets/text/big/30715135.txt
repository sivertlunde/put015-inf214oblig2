Sket
 
{{Infobox film
| name           = Sket
| image	         =
| caption        = Film poster
| director       = Nirpal Bhogal
| producer       = Nick Taussig  Paul Van Carter Daniel Toland Terry Stone
| writer         = Nirpal Bhogal
| starring       = Ashley Walters Lily Loveless Riann Steele Aimee Kelly Emma Hartley-Miller Adelayo Adedayo
| studio         = Gunslinger
| distributor    = Revolver Entertainment
| released       =  
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         = $1 million
| gross          = 
}}
 British urban thriller film, Rowley Way borough of Camden. The film premiered at the BFI London Film Festival as part of the "Film on the Square", and was released in cinemas Nationwide on October 28, 2011. The DVD and Blu-ray were released on March 5, 2012.

==Plot==
Sisters Kayla (Aimee Kelly) and Tanya (Kate Foster-Barnes) move from Newcastle upon Tyne to commence a new life near their estranged father after their mother has died. Kayla is reluctant to reconcile with him. Meanwhile, drug dealer/gang-boss Trey (Ashley Walters) has instructed his female companion Shaks (Riann Steele) to murder a crackhead who has fallen behind on the payments for her drugs.

On her way home from a day of shopping, two youths harass Kayla on the bus. However, she manages to escape from harm after Danielle (Emma Hartley-Miller) and her crew beats them up. After her ordeal, Kayla decides to return home, instead of meeting her sister for a meal in a cafe. As Tanya leaves the cafe, she finds Trey attacking Shaks for not confronting the crackhead, and tries to help Shaks. Instead, Trey pounces on Tanya and leaves her in the street to die. Worrying that Kayla will reveal the identity of her sisters killer to the police, Trey sends his men to take her out. Realizing that she is slowly running out of options, Kayla realizes her only hope of survival and revenge is to get in tow with Danielle and her crew, but could her new friendship cost Danielles life?

==Cast==
*Ashley Walters as Trey
*Lily Loveless as Hannah
*Riann Steele as Shaks
*Aimee Kelly as Kayla
*Emma Hartley-Miller as Danielle
*Adelayo Adedayo as Kerry
*Varada Sethu as Kiran Richie Campbell as Ruds
*Kate Foster-Barnes as Tanya
*Michael Maris as Drew
*Ashley Chin as Titch
*Leon Ajikawo as Reet
*Candis Nergaard as Scarred Woman
*David Nellist as Kaylas Dad
*Marc Sutcliffe as Sparks

==External links==
* 
* 
*  at Screen Base
* 

 
 
 
 
 
 
 
 
 
 
 
 