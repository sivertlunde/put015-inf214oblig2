D.E.B.S. (2004 film)
{{Infobox film
| name     = D.E.B.S.
| image    = DEBS_poster.JPG
| caption  = Theatrical release poster
| director = Angela Robinson
| producer = Andrea Sperling
| writer   = Angela Robinson
| starring = Sara Foster  Jordana Brewster  Meagan Good  Devon Aoki  Jill Ritchie
| music    = Steven M. Stern
| cinematography = M. David Mullen
| editing  = Angela Robinson
| studio = Samuel Goldwyn Films  Destination Films  Anonymous Content
| distributor = Screen Gems
| released =      
| runtime  = 91 minutes
| country  = United States
| language = English
| budget   = $3.5 million
| gross    = $97,446 
}}
D.E.B.S. is a 2004 American action-comedy film written and directed by Angela Robinson. It is an expansion of a short film D.E.B.S. (2003 film)|D.E.B.S. that made the festival circuit (including the Sundance Film Festival). The film is both a parody and an emulation of the Charlies Angels format. The plot revolves around a love story between one of the heroes and the villain. Their relationship is hindered because they are on opposite sides of the law and the fact that they are both female.

== Plot ==
Four coeds are about to graduate from a secret paramilitary academy known as D.E.B.S. (Discipline, Energy, Beauty, and Strength). The team consists of Max Brewer (Meagan Good), who tends to draw her gun at the slightest provocation, Janet (Jill Ritchie), who wishes for nothing but to earn her stripes as a true D.E.B., the chain-smoking and promiscuous Dominique (Devon Aoki), and Amy Bradshaw (Sara Foster), who is doubtful about being a spy, despite being an honor student at the academy. Amy has broken up with her boyfriend Bobby Matthews (Geoff Stults), a Homeland Security agent. Her personal life takes a back seat to the re-emergence of Lucy Diamond (Jordana Brewster), a criminal mastermind who, by coincidence, is the subject of Amys thesis. The D.E.B.S. are sent by the heads of D.E.B.S., Mrs. Petrie (Holland Taylor) and Mr. Phipps (Michael Clarke Duncan), to survey Lucy, expecting to witness criminal dealings. Little do they know that Lucy has simply been set up on a blind date by her chief henchman and best friend, Scud (Jimmi Simpson) to meet an ex-KGB assassin named Ninotchka Kaprova (Jessica Cauffiel). 

The date is a complete disaster, ending in a shootout where Lucy escapes in the chaos. As the D.E.B.S. follow her, they split up, with Amy eventually running into Lucy. Lucy attempts to charm Amy, who is surprised to learn Lucy is a lesbian, since none of the material she studied for her thesis reveals this, into lowering her gun and disappears the moment Amys attention wavers. The team is reunited, and they praise Amy for being the only federal agent who has ever encountered Lucy and lived to tell about it. Later, Lucy returns to speak with Amy and coerces her into going out for a night of fun, roping Janet into the events to prevent being found. They end up at a club, where Janet ends up bonding with Scud while Lucy further charms Amy. The two learn more about each other, and as Lucy senses Amys attraction, she leans in to kiss her, only to be interrupted by a shocked Janet. Lucy takes the two D.E.B.S. home. Janet is indignant, claiming that Amy is violating protocol, and is further 
troubled at learning Amy might not be straight.  Amy blackmails her into silence by telling her that she can withhold her stripes if she tells anybody. 

The next day, Miss Petrie meets with Amy and promotes her to squad leader, effectively replacing Max. Max is furious, but must follow Amy as they respond to a bank heist orchestrated by Lucy. Amy and Max continue to bicker when responding to the bank heist, while unbeknownst to them, Lucy has only committed the crime in order to see Amy again. Amy finds Lucy, they kiss, and Lucy asks Amy to come with her. The other D.E.B.S. find a note spray-painted on the wall reading "I have the girl", that leads them to assume Amy has been kidnapped, though Janet seems to know exactly what has happened. Miss Petrie organizes a nationwide manhunt to find Amy, while Janet desperately tries to keep Amy apprised of the situation by calling, though Amy never hears any of Janets phone messages. Amy tells Lucy of her dreams of running of to Barcelona , but Lucy and Amy are seen by a jealous Ninotchka, and she anonymously tips the D.E.B.S. to their location. The D.E.B.S. catch the two women having sex, causing Max storm out in disgust, followed by Dominique, Bobby and Janet. Though Lucy tries to reassure her, Amy returns, mortified, to the D.E.B.S. in attempts to explain herself. 

Miss Petrie decides to exile her, but Max steps in with an alternative to save Amys reputation and thus prevent the D.E.B.S. from looking foolish. Meanwhile, Lucy does everything she can to win Amy back, returning everything she has ever stolen, and with Scuds help, plans to crash the D.E.B.S. year-end dance, where Amy is to be made D.E.B. of the year. Lucy fights her way to the auditorium, and is just in time to hear Amy publicly give her speech about how awful it was to be Lucys captive, but when Amy and Lucy meet eyes, Amy retracts her entire speech, and admits that the days she spent with Lucy were the best days of her life, before running offstage. She and Lucy find each other in a storage room very like the one in which they first met, and they share another long-awaited kiss. Max and the squad catch up to them, but Max reconciles herself with the fact that her best friend is in love. She lets them go and helps Amy and Lucy make their escape, later awarding Janet with her stripes. Lucy and Amy ride of into the night, heads on one anothers shoulders.

== Cast ==
* Sara Foster as Amy Bradshaw
* Jordana Brewster as Lucy Diamond
* Meagan Good as Max Brewer
* Devon Aoki as Dominique
* Jill Ritchie as Janet
* Geoff Stults as Bobby Matthews
* Jimmi Simpson as Scud
* Holland Taylor as Mrs. Petrie
* Michael Clarke Duncan as Mr. Phipps
* Jessica Cauffiel as Ninotchka Kaprova
* Aimee Garcia as Maria
* Scoot McNairy as Stoner

== Production ==

=== Inception   ===
Director, writer, and editor Angela Robinson began to draw comics about the D.E.B.S. in college as a sideline to her writing (Robinson stated this in an extra on the U.S. DVD entitled "Infiltrating the D.E.B.S."). She received a $20,000 grant from Power Up to make a D.E.B.S. (2003 film)|10-minute short based on the concept, which toured a number of film festivals, including the Sundance Film Festival.

== Post film collaborations == The Finder, Fox canceled the series after the shows first season.

== Reception ==

=== Critics ===
D.E.B.S. received mixed reviews, garnering a 40% "rotten" rating at Rotten Tomatoes based on 58 reviews. 

=== Box office ===
D.E.B.S. did not receive a wide release, maxing out at a mere 45 theaters. After 21 days, it amassed only $97,446. 

=== Awards and nominations   ===
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Year !! Festival !! Award !! Category/Recipients
|-
| 2004 || Berlin International Film Festival || Reader Jury of the "Siegessäule" || Angela Robinson
|-
| 2005 || Black Movie Awards || Outstanding Achievement in Writing || Angela Robinson   (Nominated) 
|-
| 2005 || Black Movie Awards || Outstanding Performance by an Actress in a Leading Role || Meagan Good   (Nominated) 
|}

== References ==
 

== External links ==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 