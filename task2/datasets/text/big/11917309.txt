Battle at Kruger
 
 watering hole in Kruger National Park, South Africa, during a safari guided by Frank Watts. It was filmed by videographer David Budzinski and photographer Jason Schlosberg.
 National Geographic documentary on the video debuted on the National Geographic Channel on 11 May 2008. 

== Background ==
 vehicle on the opposite side of the watering hole with a digital camcorder,  the video begins with the herd of African buffalos approaching the water, unaware that lions are crouched nearby lying in wait for them. Upon seeing the lions, the buffalos flee and the lions charge and disperse the herd, picking off a buffalo calf and unintentionally knocking it into the water while attempting to make a kill. As the lions try to drag the buffalo out of the water, it is grabbed by a crocodile, who fights for it in a brief tug of war before giving up and leaving it to the lions. The lions lie down and prepare to feast, but are quickly surrounded by the massive reorganized buffalo herd, which moves in and surrounds the lions. One of the lions is tossed into the air by the alpha male buffalo and the buffalo chases it away. The remaining lions are subsequently scattered and chased away soon after the initial engagement, and the baby buffalo escapes into the herd while a few lions remain surrounded by the buffalos. The buffalos then proceed to aggressively chase the remaining lions away. 

== Expert commentary ==

Two veterinarians and animal behaviorists interviewed by Time (magazine)|Time assert that the behavior exhibited by the buffalo is not unusual. Dr. Sue McDonnell of the University of Pennsylvania (School of Veterinary Medicine)  said of the video:

  dominant male and several females and their babies.  If a youngster is threatened, both the harem males and bachelor males — which usually fight with one another — will get together to try to rescue it." 
 
 National Geographic said of the video:

 
"There is no doubt at all that the tourist who shot that scene   was unbelievably lucky. I mean, we wouldve considered ourselves lucky to have had that whole scene happen in front of us." 
 

== References ==
 

== External links ==
* 
* 
*  on National Geographic Society|NationalGeographic.com website (retrieved 12 May 2008)
* 

 
 
 
 
 
 
 
 