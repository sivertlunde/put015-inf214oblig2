A Month in the Country (film)
 
{{Infobox film
| name           = A Month in the Country
| image          = A Month in the Country poster.jpg 
| image_size     =
| caption        = Original theatrical poster, showing Colin Firth and Natasha Richardson. Pat OConnor
| producer       = Kenith Trodd
| screenplay     = Simon Gray
| based on       =  
| narrator       =
| starring       = Colin Firth Kenneth Branagh Natasha Richardson Patrick Malahide
| music          = Howard Blake
| cinematography = Kenneth MacMillan
| editing        =
| distributor    = Euston Films Channel Four Films Warner Bros.
| released       =  
| runtime        = 96 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = $443,524 (USA) 
}} Pat OConnor. novel of the same name by J. L. Carr, and stars Colin Firth, Kenneth Branagh, Natasha Richardson and Patrick Malahide. The screenplay was by Simon Gray.

Set in rural Yorkshire during the 1920s, the film follows a destitute World War I veteran employed to carry out restoration work on a Medieval mural discovered in a rural church while coming to terms with the after-effects of the war.

Shot during the summer of 1986 and featuring an original score by  , 21/02/2005 

==Plot==
Set in the early 1920s, the film follows the experiences of Tom Birkin (Colin Firth), who has been employed under a bequest to carry out restoration work on a Medieval mural discovered in a church in the small rural community of Oxgodby, Yorkshire. The escape to the idyllic countryside is cathartic for Birkin, haunted by his experiences in World War I. Birkin soon fits into the slow-paced life of the remote village, and over the course of the summer uncovering the painting begins to lose his trauma-induced stammer and tics.
 archaeologist James Moon (Kenneth Branagh), another veteran, who like Birkin has been emotionally scarred by the war. Moon is employed in the village under the same bequest, working to uncover a mysterious lost grave, but is more interested in discovering the remains of an earlier Saxon church building in the field next to the churchyard.
 Nonconformist family Jim Carter), with whom he dines on Sundays; the hospitality of the chapel congregation is contrasted against the established church, who have consigned the penniless Birkin to sleep in the church belfry. Ellerbecks children eventually persuade Birkin to preach a sermon at a nearby Methodist chapel. Birkin also forms an emotional, albeit unspoken, attachment to Alice Keach (Natasha Richardson), the young wife of the vicar. The vicar himself (Patrick Malahide) is portrayed unsympathetically as an obstruction to the work in the church, viewing the medieval painting as symptomatic of the superstition prevalent in the community.

==Cast==
A Month in the Country featured film debuts or early roles of several notable British actors. Although it was the third cinema feature film to cast Colin Firth, it was his first lead role. Similarly, it was Kenneth Branaghs first cinema film, and Natasha Richardsons second. Conversely, it was the last role of David Garth who died in May 1988.   at amitc.org, URL accessed 31 July 2008 

*Colin Firth as Tom Birkin
*Kenneth Branagh as James Moon
*Natasha Richardson as Alice Keach
*Patrick Malahide as the Reverend J.G. Keach Jim Carter as Ellerbeck
*Vicki Arundale as Kathy Ellerbeck
*Martin ONeil as Edgar Ellerbeck
*Richard Vernon as Colonel Hebron
*Tim Barker as Mossop
*David Garth as Old Birkin

==Production==

===Development=== Pat O’Connor chosen to direct. In contrast to the book, which is narrated as a recollection by Birkin as an old man, the film is set entirely in the 1920s, except for a brief moment towards the end. In initial drafts of the screenplay, Gray had included a narrator, but OConnor felt this was not the correct way to present the story: I felt that if I couldn’t do it in the present, suggesting internal pain by performance, then I wouldn’t really want to do it at all.   at amitc.org, URL accessed 8 August 2008  

Funding for the film was scarce, and it eventually fell to  Euston Films (a subsidiary of Thames Television) and Channel Four Films, who had had some success with low budget features such as My Beautiful Laundrette. 

===Filming===
 , Buckinghamshire was a major location in the film.]]
  on the North Yorkshire Moors Railway and the surrounding countryside was the setting for the Ellerbecks house.]]

To compensate for the lack of budget, a very tight shooting schedule was planned over 28 days, during which Kenneth Branagh was only available for two weeks and was performing on-stage nightly in London.  
 location filming Bray Studios in Berkshire. 

The church, which is a main location for the film, was substantially Staging (theatre)|set-dressed. Despite having several original medieval wall paintings, the largest addition was the creation of the medieval mural by artist Margot Noyes. To create the impression of an austere country church, Victorian stone flags were replaced with brick pavers for the duration of filming and the original wall paintings covered up. Plastic guttering and other modern additions were covered up or removed.   at amitc.org, URL accessed 6 August 2008  The churchyard had several gravestones added, including the large box tomb which is a focus of several scenes. {{Church Times
| author = Pat Ashworth
| title = Old way of being church
| url = http://www.churchtimes.co.uk/articles/2007/26-october/features/old-way-of-being-church
| issue = 7546
| date = 26 October 2007
| page = 20
| accessed = 7 June 2014
}} 

Several members of the local community were used as extras in the film, and local children were recruited by the director to collect butterflies to be released out-of-shot to create a "summer feeling". However there was some opposition to the disruption caused by the filming, and also problems involving unwelcome damage to a section of the interior plasterwork, which had to be restored after filming had concluded.  

===Music=== flashback montage Deutsche Messe (D. 872) "Zum Sanctus: Heilig, heilig ist der Herr". 

Howard Blake recalls: "I went to a viewing and saw that the film was very profound, with a serious anti-war theme, but a certain amount of found choral music had already been laid in by the editors...I explained that I loved the film and I thought the choral/orchestral music worked brilliantly but it was very big and rich and I felt a score would have to emerge from it and be very pure and expressive and quite small — and that I could only hear this in my head as done by strings only."   at amitc.org, URL accessed 31 July 2008 
Blake decided to compose his score to match the key of the Schubert Mass, in order for the music to continue seamlessly. However, during the recording session with his orchestra, the Sinfonia of London, he found that the Schubert piece was running slow and therefore flat, and he had to ask the players to tune flat to match his intended key.  , URL accessed 22 July 2008 

Due to the small budget of the film, Blake agreed "in lieu of a reasonable fee" to retain the copyright to his music.  The score was subsequently arranged into a suite for string orchestra, and is available on CD in a recording by the English Northern Philharmonia conducted by Paul Daniel. 

==Reception and awards==
Upon its release in 1987, the film was generally well received by critics. Rita Kempley, writing in The Washington Post suggested "Its all rather Arthurian, with its chivalric hero on his spiritual quest, the atmosphere suffused, seeming to dance with once and future truths."  Tom Hutchinson in the Mail on Sunday praised "a script whose delight is in the rounded reality of its characters". Janet Maslin, writing in The New York Times praised OConnors direction, suggesting it lent the film "a strong sense of yearning, as well as a spiritual quality more apparent in the look of the film than in its dialogue."  Desmond Ryan of The Philadelphia Inquirer wrote "Rarely has the impossibility of love been more wrenchingly presented than in the scenes of dashed hope between Firth and Richardson.   at amitc.org, URL accessed 22 July 2008 

However, Nigel Andrews of the Financial Times found it "like a pastoral parable that has been left outside in the damp too long, causing its batteries to go flat"  and following a 2008 screening, Sam Jordison of The Guardian suggested "even though this film is (unusually) faithful to the book...it is really little better than inoffensive. Somehow the magic that makes JL Carrs book so precious is missing." 

The film was the recipient of two awards: Pat OConnor won the Silver Rosa Camuna at the Bergamo Film Meeting in 1987  and Howard Blake was awarded the Anthony Asquith Award for Musical Excellence by the British Film Institute in 1988.  In addition, Colin Firth was nominated for an Evening Standard Award.  The film was screened in the Un Certain Regard section at the 1987 Cannes Film Festival.   

==DVD release==
Following its cinema release, the film was transferred to VHS in 1991 in a pan and scan edition. However, when Glyn Watkins, a poet who had been encouraged by J.L. Carr early in his career, wanted to screen the film at the launch of a poetry book in 2003 at the National Media Museum in Bradford, the museum found that all original 35mm film prints had disappeared.
 agents of rights to Region 2 restored and released on DVD. 

==References==
 

==External links==
*  
*  
* 
* 

 

 
 
 
 
 
 
 
 