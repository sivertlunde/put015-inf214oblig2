Hell Night
 
 
{{Infobox film
| name = Hell Night
| image =Hell-night-1981.jpg
| caption = Original poster
| director = Tom DeSimone
| writer = Randy Feldman Peter Barton
| music = Dan Wyman
| cinematography = Mac Ahlberg
| editing = Anthony DiMarco
| producer = Irwin Yablans Bruce Cohn Curtis
| distributor = Compass International Pictures
| released =  
| runtime = 101 min.
| awards =
| country = United States
| language = English
| budget = Unknown
}} independent horror fraternity hazing ("hell night") set in an old manor, during which a deformed maniac terrorizes and murders many of the college students. The film blends elements of slasher films and Creature Features, and has developed a large cult following.
 The Blob in 1988 and 2002s The Scorpion King, served here as an executive producer. The film was nominated for a Razzie Awards for Worst Actress for Blair. 

==Plot==
The film begins with a young teen girl screaming, only to reveal that she is a student at a frat party. While at the party, the camera cuts to Peter, who is the president of Alpha Sigma Rho. He decides that four new pledges should have an initiation. He decides that the group of youths: bookish Marti, brawn Jeff, partying Denise, and surfer Seth, should spend the night in the supposedly "haunted" estate, Garth Manor, where there was a murder twelve years prior to the present events. Peter began telling the story about the previous events that took place twelve years earlier as he leads them to the estate. The houses former owner, Raymonde Garth, who dismembered his family in the parlor strangled & killed his wife to death & kills his four deformed children: Margeret, Morris, & Suzanne, in seemingly violent ways, before finally hanging himself. However, the youngest child, Andrew Garth, dead or alive, was never found and is rumored to have seen the murders and fled the scene, hiding away in the house. Rumor has it that the deformed Andrew might still be living hidden away inside Garth Manor. 

After being escorted to the house, Seth and Denise hooked up, leaving Marti and Jeff in the parlor to talk. Jeff explains that he only came because of his rich father and lifestyle, while Marti explains that she worked in her fathers car garage over the summer and that she also did the Literature Notes for the sorority sister May so that she could have free clothing, what ever room she wanted, and a free car. Meanwhile, Peter, Scott, and May have rigged mechanical devices to scare the pledges. While May is walking back around the house, a pair of filthy hands reached out and violently dragged her into a pit  by an unseen figure and decapitates her with a spade. While Scott is up on the roof rigging another device, he hears a noise and decides to investigate, where a bare handed figure breaks his neck by twisting his head around his body. After Peter tries to pull a prank on May, he climbs up to the roof to see about Scott, whom he finds dead, hanging by a wire and flees. Peter climbs back off the roof where the killer chases him into the hedge maze, where he gets lost after his escape attempt. but he yet ran into another assailant who impales him with a scythe. The audience suddenly realizes that they are two killers.

Meanwhile, the group of pledges discovers the sound effects and scares set up around the house. Seth and Denise retreat back upstairs where they have a sensual moment together to Seths enjoyment. Seth excuses himself to the bathroom, where he admires his reflection. While Seth is in the bathroom, an unknown person emerges from the closet grabs or kills Denise before Seth returns. Seth later returns and thinking Denise is under the covers, pulls them back, only to discover Mays severed head from earlier. Seth screams in horror and pulls on his clothes while he tries to escape. Marti and Jeff runs off to investigate the room where Denise and Seth were sleeping and also see the head much to Martis horror. The trio decide that while Seth climbs the fence to seek help, Marti and Jeff should stay at Garth Manor and search for the missing Denise. 

Jeff and Marti search the house, discovering Scotts body. They sit together before Jeff decides to search the grounds, as there is a light in the hedge maze outside. Marti rejects the idea, believing it to be the deformed Andrew, but eventually gives in and stays hidden in the bedroom. Jeff eventually discovers that the light was Peters flash light and also discovers Peters body to his horror. He runs away, not seeing the gate keys that are grasped firmly in his hand. Jeff informs Marti of Peters murder and the discovery of his corpse. They assume that the real murderer is in fact Andrew Garth, but they are still unsure if this assumption is true or not. As they discuss the matter, a shape begins to form in the large rug behind Marti, who turns around and screams in terror. Jeff arms himself with a pitchfork, which he impales in the figure. The two pull back the piled up rug, only to reveal a trap door, and that the figure beneath the rug has fallen back into the hole below. Jeff decides that they should go after them and the two descends into the darkness. While exploring the tunnels the pair discover the missing Denises body, which is sitting with another pair of mummified corpses of a cobweb site with rats crawling on them at an infested dinner table.

Marti and Jeff are discovered by a lumbering and deformed man, who chases them, enraged. Jeff tries to fight back, but is knock and injured and thrown down a flight of stairs when the other killer appears on the scene. The two apparent Garth sons traps Marti and Jeff, but the two discover the same passageway Peter used earlier to pull a prank on Denise and manage to escape and stall the two Garths. The pair escape and heads back to the bedroom. Meanwhile, Seth arrives at the frat house, but everyone is either passed out or sleeping. Seth leaves the frat house and retreats to the police station, where he is scolded and the police dismiss and threaten to put him in jail for no belief in his story. Seth sneaks away, steals a gun and bullets, and climbs out a window and then jacks a car from a man and tells him hes going to Garth Manor. Seth arrives back at Garth Manor where he is attacked by the second Garth son. 

A struggle ensues and Seth manages to shoot the killer with the stolen gun. He is greeted Marti and Jeff. As they tell Seth about the discovered bodies and the killers, the same deformed arm of Andrews grabs Seth and drags him into the darkness as Marti and Jeff hears a gunshot, but the shotgun slides alone across the floor. Marti decides to make an attempt to obtain it, but Andrew lunges from the darkness and chases Jeff and Marti back upstairs and into the bedroom where they lock the door. As Jeff tries to Marti out safely, she manages to escape up onto the rooftop, but Jeff is unfortunate and Andrew throws him from the second floor of the house to his death below while Marti watches in horror. Marti climbs off the roof using the ladder while she is descending the ladder, the killer bursts through a window and Marti fights back, but loses her balance and falls off the ladder. She gets back up and runs into the hedge maze where she discovers Peters body for herself and steals the keys from his hand. Marti also manages to find the gate, where she discovers Seths stolen car but is unable to start the car, and hotwires it. Marti climbs into the car, where she backs into the right side of the gate, knocking it over. Andrews horrid face appears from the car roof as smashes the glass and attempts to strangle Marti. Marti swerves the car around, and notices the spiked tips of the knocked over gate. Marti presses on the gas pedal and impales Andrew to tail pike as he spews blood, writhes in agony and dies. Marti faints from exhaustion as the car horn fades out. 

Later Marti awakens in the morning, with the killer still impaled on the toppled gate and the crashed car. She gets out of the car in stunned silence, dazed yet still victorious and walks over to the camera as the credits begin to roll.

==Cast==
*Linda Blair as Marti Gaines
*Vincent Van Patten as Seth
*Kevin Brophy as Peter Bennett
*Jenny Neumann as May West
*Suki Goodwin as Denise Dunsmore
*Jimmy Sturtevant as Scott Peter Barton as Jeff Reed

==Production==
Filming Hell Night took only 40 days.  The majority of the movie was shot in three locations: The outside of Garth Manor was shot at a mansion in Redlands, California. (The Kimberly Crest mansion was converted from a private residence to a museum shortly after filming was completed). The hedge maze was brought in as there was no actual garden maze on the mansion property. The inside of Garth Manor was filmed in a residential home in Pasadena, California. The frat party was filmed in an apartment lobby in Los Angeles, California. The many underground tunnels filmed in the movie were actually no more than two corridors in which the director had the actors running repeatedly through from different angles. 

For the scene where Jeff is thrown down a flight of stairs and hurt his leg, there was not a lot of acting involved. In reality, actor Peter Barton had really hurt himself and most of his limping was due to being in real physical pain.

The two actors who portrayed the Garth killers are not listed anywhere in the credits, and their real names remain a mystery. However, on the DVD commentary, it is noted that they are both German nationals whom spoke little or no English, and that one of them (the middle aged bearded man) died shortly after the release of the film.
==Release==
 

==Reception==
 
 Time Out wrote "Amazing   what a competent director, cameraman and cast can do to help out a soggy plot", calling the film "tolerably watchable by comparison with the average Halloween (1978)|Halloween rip-off. 

==References==

 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 