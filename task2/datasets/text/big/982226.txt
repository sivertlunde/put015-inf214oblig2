Rover Dangerfield
 
 
{{Infobox film 
| name           = Rover Dangerfield 
| image          = Movie poster rover dangerfield.JPG
| alt            = 
| caption        = Theatrical release poster
| director       = {{Plainlist|
* James L. George
* Bob Seeley}}
| producer       = {{Plainlist|
* Willard Carroll
* Tom L. Wilhite
* Executive:
* Rodney Dangerfield}}
| screenplay     = Rodney Dangerfield
| story          = {{Plainlist|
* Rodney Dangerfield
* Harold Ramis}}
| based on       =  
| starring       = {{Plainlist|
* Rodney Dangerfield
* Susan Boyd
* Ronnie Schell Shawn Southwick
* Sal Landi
* Ned Luke
* Bert Kramer
* Robert Pine
* Dana Hill}} David Newman
| editing        = Tony Mizgalski Hyperion Animation
| distributor    = Warner Bros.
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
}} animated Musical musical comedy Hyperion Animation and released by Warner Bros., starring the voice talents of comedian Rodney Dangerfield, who also wrote and co-produced the film. It is about a street dog named Rover, who is owned by a Las Vegas showgirl. Rover gets dumped off Hoover Dam by the showgirls boyfriend. However, rather than drowning, Rover ends up on a farm.

==Plot== Las Vegas, gambling and chasing girls with his best friend Eddie. One night, he sees his owner Connies boyfriend, Rocky, in a transaction with a pair of gangsters, and accidentally disrupts it. Thinking that Rocky is an undercover cop setting them up, the gangsters flee, telling Rocky that he has blown his last chance. The next day, Connie goes on the road for two weeks, leaving Rocky to look after Rover. In revenge for ruining his deal, Rocky puts Rover in a bag, drives him to Hoover Dam, and throws him in the water. The bag is later pulled out of the water by two passing fishermen, who take Rover back to shore and place him in the back of their pickup truck. However, Rover wakes up and jumps out of the truck when the fishermen stop for gas, and begins to wander down the road on his way back to Vegas. Instead, he ends up in the countryside, and eventually runs into a farmer, Cal, and his son, Danny, who convinces his father to take the dog in. Cal agrees on one condition: at the first sign of trouble, hell be sent to an animal shelter, and if nobody claims him, the animal shelter can put him to sleep.

Rover has difficulty adjusting to life on the farm, but with the help of Daisy, the beautiful dog next door, and the other dogs on the farm, he succeeds in earning his keep. Rover spends Christmas with the family, and begins to fall in love with Daisy, who returns his affections. However, one night in an attempt to save the Christmas turkey from some wolves, Rover ends up caught holding the dead bird, looking as if he killed it. The next morning, Cal takes Rover into the woods to shoot him, but is attacked by the wolves. Rover manages to fight the wolves off, and brings the other farm dogs to get Cal home.

Rovers heroics make the papers, allowing Eddie and Connie to find out where he is. Connie travels to the farm and brings Rover back to Vegas, where Rover confronts Rocky. After Rocky accidentally confesses to Connie what he did, she breaks up with him. Angered, he tries to retaliate but Rover and his dog friends chase him into the limo of the gangsters. At first, hes relieved that they came to his rescue but questions why were they even there in the first place. While Rover happily listens, the thugs proceed to reveal that they had set him up and imply that they are going to murder him by throwing him into the Hoover Dam.

Some time later, Rover, missing Daisy, becomes depressed. Connie, realizing this, takes Rover back to the farm to stay. Rover is reunited with Daisy, who reveals to him that he is now a father, unveiling six puppies. The story ends with Rover teaching his kids how to play cards, and playfully chasing Daisy around the farmyard.

==Voice cast==
 
* Rodney Dangerfield as Rover, the films protagonist; a fun-loving dog living in Las Vegas. Despite the title, he is never addressed as "Rover Dangerfield"
* Susan Boyd as Daisy, Rovers love interest
* Ronnie Schell as Eddie, Rovers loyal best friend. Shawn Southwick as Connie, Rover and Eddies owner; a Las Vegas showgirl.
* Sal Landi as Rocky, the main antagonist; Connies boyfriend and a gangster who dislikes Rover.
* Ned Luke as Raffles
* Bert Kramer as Max
* Robert Pine as Duke
* Dana Hill as Danny
* Eddie Barth as Champ Dennis Blair as Lem Don Stewart as Clem
* Gregg Berger as Cal
* Paxton Whitehead as Count
* Christopher Collins as Gangster #1 / Wolves
* Bob Bergen as Gangster #2
 

==Production==
  Jeff Smith, best known as the creator of the self-publishing|self-published comic book series Bone (comics)|Bone, described working on key frames for the films animation to editor Gary Groth in The Comics Journal in 1994. 

==Release== Warner Archives later released the film on DVD on December 7, 2010. 

==Reception==
Alex Sandell of "Juicy Cerebellum" called it "one of the worst animated films ever, even if you are a fan of Rodney Dangerfield|Dangerfield", and Cherryl Dawson and Leigh Ann Palone TheMovieChicks.com both agreed that "this movie gets no respect and doesnt deserve any". One of the more positive reviews came from Douglas Pratt of "DVDLaser", saying that "the story is quite entertaining and provides so much of the films appeal that the artwork just wags along with it".

==See also==
* List of American films of 1991
* List of animated feature-length films

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 