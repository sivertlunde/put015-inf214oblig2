Three Loan Wolves
{{Infobox Film |
  | name           = Three Loan Wolves 
  | image          = ThreeLoanWolvesTITLE.jpg
  | caption        = 
  | director       = Jules White Felix Adler Harold Brauer Jackie Jackson Joe Palma Wally Rose 
  | cinematography = George F. Kelley 
  | editing        = Edwin H. Bryant
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 16 40"
  | country        = United States
  | language       = English
}}
Three Loan Wolves is the 93rd short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Jackie Jackson) how he came to have three fathers. The Stooges, owners of a pawn shop, owe money to the Gashouse Protection Society, a bunch of loan sharks.  When one of the mobsters comes to their shop to demand money, the Stooges deal with him in their typical Stooge fashion.  To complicate matters, a lady (Beverly Warren) leaves a baby in the shop as part of a plan to sell a phony diamond and the Stooges wind up caring for the kid.  The lady left the kid there at the suggestion of the mobster the Stooges had just thrown out of their shop.  

The Stooges have no idea how to take care of the kid.  Soon his crying gets on Moe nerves, and their attempts to stop the kid only end up with Curly giving the baby a gun as a pacifier.  Curly assures Moe the gun isnt loaded only to have it fire when he tries to show it isnt loaded. The bullet causes a hanging lamp to fall and hit Moe in the head.  The baby only stops crying when Curly makes an improvised bottle with milk.

Later, the same mobster shows up with some of his goons to get the money.  The trio manage to defeat the crooks and when they finish telling the story, the kid goes off to find his real mother.  Moe and Curly blame Larry for the entire mess and decide to punish him.

==Production notes==
The title is a parody of Columbias movie series "The Lone Wolf." 

This is the eleventh of sixteen Stooge shorts with the word "three" in the title.

The theme reverts to the syncopated, jazzy version of "Three Blind Mice" previously used on Gents Without Cents, Three Pests in a Mess, Booby Dupes and Idiots Deluxe instead of the revamped, sliding strings version used during this period.

One notable gag in this short involves one of the Stooges following instructions to the letter.  Larry is removing a very large stack of dishes from a crate, when Moe asks him for help.  Larry says he cant because hes busy.  When Moe tells him to drop what hes doing, Larry obliges by dropping the entire stack of dishes onto the floor.

==Curlys illness==
Three Loan Wolves was filmed near the end of Curly Howards career. The 42-year-old comedian had suffered a series of minor strokes several months prior to filming, and his performances had been unpredictable. By the time of Three Loan Wolves, he had lost a considerable amount of weight, and lines had creased his face. Larry Fine and Moe Howard look stocky by comparison.
 inaugural short for Columbia Pictures in 1934, Woman Haters), with nearly the entire film revolving around him. Curly also tried desperately to maintain his falsetto voice, but was clearly unable to. Several scenes are heartbreaking to watch, as the once-superstooge had been relegated to an occasional line of dialogue. 

Beverly Warren appeared at a 2003 Three Stooges convention in Fort Washington, Pennsylvania, and reported that Curlys illness was not discussed on the set. She added that filming was completed at such a rapid pace, she rarely saw Curly or Moe (as she only shared screen time with Larry). 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 