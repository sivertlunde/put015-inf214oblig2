Magic in the Water
{{Infobox film
| name           = Magic in the Water
| image          = Magicwater.jpg
| caption        = Theatrical release poster
| director       = Rick Stevenson
| producer       = William Stevenson Matthew OConnor
| screenplay     = Rick Stevenson Icel Dobell Massey
| story          = Ninian Dunnett Rick Stevenson Icel Dobell Massey
| starring       = Mark Harmon Joshua Jackson Harley Jane Kozak Sarah Wayne
| music          = David Schwartz
| cinematography = Thomas Burstyn
| editing        = Allan Lee
| studio         = Triumph Films Oxford Film Company
| distributor    = TriStar Pictures
| released       =   
| runtime        = 101 minutes
| country        = United States Canada
| awards         =
| language       = English Dutch
| budget         = 
| gross          = US$2.68 million 
}}
Magic in the Water is a 1995 family film directed by Rick Stevenson and starring Mark Harmon, Joshua Jackson and Sarah Wayne. It is about a fictional lake monster in British Columbia. The film was distributed by TriStar Pictures and produced by Triumph Films.

==Plot==
Ashley Black (Sarah Wayne) is depressed because her father Jack (Mark Harmon) spends all his time focusing on his job instead of her and her older brother Joshua (Joshua Jackson). She constantly records his radio show and listens to it. One day, her father takes them to a remote Canadian lake that was popular with tourists due to a myth about an aquatic monster named Orky. They rented a cabin next to an elderly Native American man who was confined to a wheelchair. Jack meets a local psychiatrist named Wanda (Harley Jane Kozak) who is trying to aid some local men who claim that they have been possessed by Orky. When Ashley runs away, Jack also has the same experience whilst looking for her. As a result he becomes more devoted to his children.

Ashley and Joshua find out that the reason that Orky is possessing people is to try and tell them that he is dying because a businessman is dumping toxic waste into the lake. Ashley and Joshua help the old man in the cabin next to theirs to find a totem pole in the woods. With the help of Hiro (Willie Nark-Orn), the son of some Japanese monster seekers, they expose the businessmans illegal dumping. Orky, however, still dies from the poisonous waste. The old man summons a lighting bolt which enters a hole in the cave where Orky lives. Ashley and Hiro stay on the dock overnight and leave some cookies out. When she realizes that the cookies have been eaten Ashley screams with joy which suggests that Orky is still alive.

==Cast==
*Mark Harmon as Jack Black
*Joshua Jackson as Joshua Black
*Harley Jane Kozak as Wanda
*Sarah Wayne as Ashley Black
*Willie Nark-Orn as Hiro
*Adrien Dorval as Wright Hardy
*Mark Acheson as Lefty Hardy
*Anthony Towe as Taka
*John Procaccino as Frank
*Tom Cavanagh as Simon, Patient #1 Gareth Bennett as Christian, Patient #2
*Brian T. Finney as Bug-Eyes, Patient #3

==Reception==
Magic in the Water received negative reviews from critics and holds a 21% rating on Rotten Tomatoes based on 24 reviews.

Critic Leonard Maltin wrote in his book that "All the magic must be in the water; theres certainly none on the screen.  Routine family film feels like recycled Steven Spielberg|Spielberg."   Roger Ebert criticized the films special effects, describing the creature Orky as an "ashen Barney & Friends|Barney". He also notes that Orky barely appears in the film at all. 
 cinematography and Genie Award for Best Achievement in Overall Sound|sound.

==See also== Loch Ness (1996)
*   (2005)
*   (2007)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 