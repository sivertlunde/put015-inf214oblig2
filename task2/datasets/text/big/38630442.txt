The Movie Out Here
{{Infobox film
| name           = The Movie Out Here
| image          = 
| alt            = 
| caption        = 
| director       = David Hicks
| producer       = 
| writer         = 
| starring       = Robin Nielsen, Viv Leacock, James Wallis
| music          = 
| cinematography = 
| editing        = 
| studio         = Alliance Films eOne
| released       =  
| runtime        = 90 minutes
| country        = Canada
| budget         = 
| gross          = 
}} Canadian buddy buddy comedy engagement and crowdsourcing to help promote the film.

Despite the ambitious nature of the project, and the marketing campaign resulting in a notable increase in market share for Kokanee, the film itself was met with negative reviews from critics for its over-reliance on lewd content and product placement.

== Plot ==
Adam (Robin Neilsen), a lawyer from Toronto, returns to his hometown of Fernie, British Columbia and discovers that a ski waxing business run by his friend Theo (James Wallis) is under threat by a real estate developer, prompting them to hold a party as a Fundraising|fundraiser. 

== Production ==
The   mascot killed off in an upcoming advertisement. The campaign was followed up in 2011 by a campaign featuring the Rangers ghost encouraging users to vote via Facebook in an election to determine the new Kokanee Ranger.       After Labatt requested a "big" idea for their next campaign, Grips creative director Randy Stein suggested that they produce a film, taking advantage of the continuing narrative portrayed by Kokanees past advertising campaigns.   Stein had expressed curiosity surrounding the concept of branded content—a form of entertainment content produced in collaboration with an advertiser.   He believed that the films target audience had become more accepting of branded entertainment, but still did not want to have advertising "shoved down their throats."  

Kokanee and Alliance Films officially announced The Movie Out Here in May 2012; its title alluding to Kokanees long-time slogan "The Beer Out Here".       The Movie Out Here was produced in an unconventional manner in contrast to most feature films; the film had already received financing and promotion from Labatt before its screenplay was even completed.   Aside from executive producer Jeff Sackman, the film was written, produced, and directed by Grip staff members.    The film was described as a lewd, "feel-good" comedy about a group of friends who had reunited in the city of Fernie and want to "kind of go back to their roots." The film was also designed to pay tribute to life in Western Canada, including its landscape, communities, and its bar (establishment)|bars.   
 National Hockey League lockout that shortened the 2012-13 NHL season|2012-13 season reduced sales of beer in Canada by 12% overall, requiring the company to use new strategies to promote its products.  The film was targeted towards males in their mid-20s, a key market for beer that has also struggled due to increasing competition from distilled beverages.   Alongside traditional product placement, The Movie Out Here also incorporates popular characters from Kokanees past advertising campaigns, such as the Ranger, the Glacier Girls, and the Sasquatch.   Kokanees brand manager Amy Rawlinson explained that the film should not be considered a "90-minute beer commercial", and was conceived from the start to be a "stand-alone" film. Rawlinson was more concerned about the emotional aspects of Kokanees presence in the film rather than the number of times it appears.   

==Distribution==

===Promotion=== coaster obtained tablets on-site to encourage customers to vote immediately after their order. 

===Release===
The Movie Out Here premiered at the Whistler Film Festival on November 30, 2012.     The film began a limited release at theatres in Canadas western provinces (such as British Columbia, Alberta, and Saskatchewan) on March 1, 2013. 

== Reception == Postmedia News gave The Movie Out Here a 1.5 out of 5; sharing similar criticisms, Monk described the film as being a product of a "Toronto boardroom" with a cliché plot, and doubted whether its writers were even trying to make it a "real" movie. She concluded that The Movie Out Here was not a "celebration of British Columbias culture", but a film which "pimps it out for sudsy profits using boobies, fart jokes and sexist references to slutty girlfriends and casual intercourse."   

Randy Stein believed that the response to the The Movie Out Here by critics did not matter, and his staff even predicted that "hardcore film reviewers" would either "go in wanting to hate this film", or not even review it at all. He went on to report that the film had received a positive response from its target audience via social media. Among the theatres that screened the film in Vancouver, The Movie Out Here was the #4 film on its opening weekend.  Kokanees promotional efforts also had a positive impact on its beer sales; by the end of the nine-month campaign supporting the film, Kokanee had experienced a 6% increase in market share. 

==See also ==
* Branded entertainment
* Secrets of the Mountain and The Jensen Project, television films produced by and featuring product placement by Procter & Gamble and Walmart

== References ==
 

 

 
 
 
 
 
 
 