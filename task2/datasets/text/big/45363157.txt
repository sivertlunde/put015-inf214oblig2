The Gay Defender
{{Infobox film
| name           = The Gay Defender
| image          = The Gay Defender poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Gregory La Cava
| producer       = Jesse L. Lasky Adolph Zukor
| screenplay     = Ray Harris Grover Jones Herman J. Mankiewicz George Marion Jr. Sam Mintz Kenneth Raisbeck
| starring       = Richard Dix Thelma Todd Fred Kohler Jerry Mandy Robert Brower Harry Holden Fred Esmelton
| music          =
| cinematography = Edward Cronjager
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Gregory La Cava and written by Ray Harris, Grover Jones, Herman J. Mankiewicz, George Marion Jr., Sam Mintz and Kenneth Raisbeck. The film stars Richard Dix, Thelma Todd, Fred Kohler, Jerry Mandy, Robert Brower, Harry Holden and Fred Esmelton. The film was released on December 10, 1927. by Paramount Pictures. {{cite web|url=http://www.nytimes.com/movie/review?res=9A0CE5DB1E31EE32A25755C2A9649D946695D6CF|title=Movie Review -
  Gay Defender - Blood and Roses. - NYTimes.com|work=nytimes.com|accessdate=10 February 2015}}  

==Plot==
 

== Cast ==  
*Richard Dix as Joaquin Murrieta
*Thelma Todd as Ruth Ainsworth
*Fred Kohler as Jake Hamby
*Jerry Mandy as Chombo
*Robert Brower as Ferdinand Murrieta
*Harry Holden as Padre Sebastian
*Fred Esmelton as Commissioner Ainsworth
*Frances Raymond as Aunt Emily Ernie Adams as Bart Hamby 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 