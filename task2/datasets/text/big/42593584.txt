The Last Enemy (film)
{{Infobox film
| name = The Last Enemy 
| image =
| image_size =
| caption =
| director = Umberto Barbaro
| producer = Antonio Piras 
| writer = Fritz Eckhardt   Francesco Pasinetti   Umberto Barbaro 
| narrator =
| starring = Fosco Giachetti   María Denis   Guglielmo Sinaz   Gemma Bolognesi
| music = Emilio Tufacchi 
| cinematography = Mario Albertelli 
| editing = Umberto Barbaro   Vincenzo Sorelli 
| studio = SCIA 
| distributor = 
| released = 1938
| runtime = 88 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Last Enemy (Italian:Lultima nemica) is a 1938 Italian drama film directed by Umberto Barbaro and starring Fosco Giachetti, María Denis and Guglielmo Sinaz. 

==Cast==
* Fosco Giachetti as Franco Rossi 
* María Denis as Anna Vitali 
* Guglielmo Sinaz as Commendator Vitali, Annas father 
* Gemma Bolognesi as Signora Vitali, Annas mother  Carlo Lombardi as Doctor Vitali 
* Giuliana Gianni as Elsa, woman doctor 
* Alida Valli as A friend of Anna 
* Mario Pisu as Ferdinando De Medio 
* Elena Zareschi as Isa Fleur 
* Otello Toso as Enzo 
* Livia Minelli as A nurse 
* Carlo Petrangeli as Giancarlo 
* Giorgio Capecchi as Doro 
* Oreste Fares as Prof. Mancini 
* Tatiana Pavoni as A friend of Elsa 
* Joop van Hulzen as Doctor

== References ==
 

== Bibliography ==
* Ben-Ghiat, Ruth. Fascist Modernities: Italy, 1922-1945. University of California Press, 2001. 
* Moliterno, Gino. The A to Z of Italian Cinema. Scarecrow Press, 2009.

== External links ==
*  

 
 
 
 
 
 
 
 


 