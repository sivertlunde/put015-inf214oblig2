Roningai
{{Infobox film
| name           = Roningai (Samurai Town: Story 1 and Story 2)
| image          =
| caption        =
| director       = Masahiro Makino
| producer       =
| writer         = Itaro Yamagami
| starring       = Komei Minami Toichiro Negishi Seizaburo Kawazu Tsuyako Okajima
| cinematography = Minoru Miki
| editing        =
| studio         = Makino Film Productions
| distributor    = Digital Meme (DVD)
| released       =  
| runtime        = Story 1: 15 reels (original); 8 minutes (DVD) Story 2: 19 reels (original); 72 minutes (DVD)
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}} Shozo Makino (considered the father of Japanese film). These films lent status to ensemble casts and did not rely on famous stars. The film was known for its depiction of the unique setting of the ronin town as well as for the exquisite camera work and fast-paced sword fighting scenes.

==External links==
*  
*  

 

 
 


 