Bioscope (film)
{{Infobox film
| name           = Bioscope
| image          = Bioscope_film.jpg
| caption        = Film DVD cover
| alt            =
| caption        =
| director       = K. M. Madhusudhanan NFDC
| writer         = K. M. Madhusudhanan
| starring       = Walter Wagner Nedumbaram Gopi Bharathan Najrakkal Mekha Rajan
| music          = Chandran Veyyattummal
| cinematography = M. J. Radhakrishnan
| editing        = Beena Paul
| studio         =
| distributor    =
| released       =  
| runtime        = 94 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Special Jury award at the 56th National Film Awards and also won 5 awards in the 2008 Kerala State Film Awards.         

==Plot==
Bioscope, set in the early years of the twentieth century, is a story of history entering the paths of memories and dreams. The story of villagers, made mute by colonialism and slavery, entering the garden of a new vision through a new machine, bioscope. The protagonist Diwakaran’s new journey starts with his acquisition of a bioscope. The Frenchman DuPont, who does bioscope shows on the coasts of Tamil Nadu, is the architect of his new journey. Diwakaran was stunned by early forms of cinema images. His relationship with DuPont and the bioscope start with his astonishment when he first saw moving images. It turns into a story of inseparable friendship. Divakaran purchases the machine from DuPont and intends to entertain his village folks with his films, but ultimately falls prey to superstitions and suspicions about the instrument.

==Production== bioscope shows in Kerala in 1907. The director plans to make a sequel. “Bioscope is the first part of a trilogy. The second part is titled Kannadi Kottaka (Mirror Cinema Hall) and is set in contemporary Kerala. It is about a movie house and three people who are connected to it,” says Madhusudhanan.

==Cast==
* Walter Wagner as Dupont
* Murugan as Diwakaran
* Ramgopal Bajaj as Kaimal
* Nedumbaram Gopi as Murugan Nair
* Bharathan Najrakkal as Kumaran
* T. V. Gangadharan as Inspector
* Mekha Rajan as Nalini
* Kuttiyedathi Vilasini as Ammini
* Nilamboor ayesha as Malu
* Anusha Mohan as mute girl

==Awards and Accolades==
* 56th National Film Awards Special Jury award

* 2008 Kerala State Film Awards Special Jury Award - K. M. Madhusudhanan Best Editor - Beena Paul Best Cinematographer - M. J. Radhakrishnan Best Background Music - Chandran Payyatumel Best Processing Lab - Chithranjali Studio

* NETPAC Jury Award (Indian Competition section) - Osians Cinefan Festival of Asian and Arab Cinema|OSIANs Cinefan Asian & Arab Film Festival, 2008    

* SAIFF 2008, New York - Best Cinematography    
 International Filmfestival Mannheim-Heidelberg, 2008 - Special mention of the International Jury

==References==
 

==External links==
*  

 
 
 
 
 