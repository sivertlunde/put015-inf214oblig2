Dance Madness
{{Infobox film
| name           = Dance Madness
| image          =
| image_size     =
| caption        =
| director       = Robert Z. Leonard
| writer         = Frederica Sagor
| starring       = Claire Windsor Conrad Nagel Hedda Hopper
| music          =
| cinematography = John Arnold William H. Daniels
| editing        = William LeVanway
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 mins.
| country        = United States Silent English intertitles
| budget         =
}}
 silent comedy film directed by Robert Z. Leonard based upon a script by Frederica Sagor. The film starred Claire Windsor, Conrad Nagel and Hedda Hopper.  Dance Madness is now considered lost film|lost. 

According to the credited screenwriter, Frederica Sagor, Dance Madness was "patently a rewrite" of The Guardsman, a work by Ferenc Molnár that was later directly adapted for film.  Sagor notes the screenplay was not written by her, but by Alice D.G. Miller, and she only provided script rewrites. 

==Plot==
May (Claire Windsor) is married to Roger, an alcoholic hell-raiser (Conrad Nagel). During one of their riotous parties, she tests his fidelity by impersonating a notorious masked dancer (Hedda Hopper) and trying to seduce him.

==Cast==
* Conrad Nagel - Roger Halladay
* Claire Windsor - May Russell
* Hedda Hopper
* Douglas Gilmore
* Mario Carillo
* Pauline Starke

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 


 