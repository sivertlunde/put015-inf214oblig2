Baba (2012 film)
 
 
{{Infobox film
| name           = Baba
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Akram Farid
| producer       = Walid Al Kurdi
| writer         = Karim Famy
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = New Century Production
| distributor    = New Century Production
| released       =  
| runtime        = 
| country        = Egypt
| language       = Arabic
| budget         = 
| gross          = 
}}

 Egyptian drama film written by Karim Famy and directed by Akram Farid for New Century Production.

==Plot==
Hazem (Ahmed Al Sakka) is a successful gynaecologist who falls in love with Farida (Durrah) who works as an interior designer.  When they get married, Hazem discovers his inability to father children and the two seek a medical solution through in vitro fertilization…

==Cast==
* Nicole Saba
* Salah Abdallah		
* Edward	
* Soleiman Eid		
* Ahmed el-Sakka as Hazem		
* Dorra Zarrouk as Farida (as Durrah)

==References==
 

==External links==
*   at the Internet Movie Database
*  

 
 

 