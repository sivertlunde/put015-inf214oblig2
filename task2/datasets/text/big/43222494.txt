London Has Fallen
{{Infobox film
| name           = London Has Fallen
| image          = London Has Fallen poster.jpg
| image_size     = 220px
| caption        = Teaser poster
| director       = Babak Najafi
| producer       = Gerard Butler   Mark Gill   Danny Lerner   Matt OToole   Alan Siegel   Les Weldon
| writer         = Creighton Rothenberger   Katrin Benedikt   Christian Gudegast   Chad St. John
| starring       = Gerard Butler   Aaron Eckhart   Morgan Freeman Trevor Morris 
| cinematography =
| editing        =
| studio         = Gerard Butler Alan Siegel Entertainment   Millennium Films   Nu Image
| distributor    = Focus Features
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 action Thriller thriller film directed by Babak Najafi and written by Creighton Rothenberger, Katrin Benedikt, Christian Gudegast and Chad St. John. It is a sequel to Olympus Has Fallen. The film stars Gerard Butler, Aaron Eckhart and Morgan Freeman.

Filming began on October 24, 2014, in London, a Christmas break started in November with filming resumed in February 2015. London Has Fallen is set to be released on October 2, 2015, by Focus Features.

==Plot==
The story picks up in London, where the British Prime Minister has died under mysterious circumstances. His funeral is a must-attend event for leaders of the Western world. But what starts out as the most protected event on Earth turns into a deadly plot to kill the worlds most powerful leaders and unleash a terrifying vision of the future. Only three people have any hope of stopping it: the President of the United States of America Benjamin Asher (Aaron Eckhart), his formidable Secret Service head Mike Banning (Gerard Butler), and a British MI-6 agent who rightly trusts no one. 

==Cast== Secret Service agent Mike Banning  President Benjamin Asher  Vice President Allan Trumbull   
* Angela Bassett as Lynne Jacobs, the Director of the United States Secret Service  US Army General Edward Clegg    Secretary of Defense Ruth McMillan 
* Radha Mitchell as Leah Banning, Mikes wife and a nurse 
* Jackie Earle Haley as Deputy Chief Mason 
* Sean OBryan as Ray Monroe, Deputy Director of the National Security Agency 
* Mehdi Dehbi as Sultan Mansoor, the youngest of three brothers whose life has been forever changed after a drone strike. 
* Alon Aboutboul 
* Charlotte Riley 
* Waleed Zuaiter 

==Production== The Equalizer.  On May 1, 2014, it was announced Focus Features had acquired distribution rights to the sequel and will release it on October 2, 2015.  On August 18, 2014, it was announced that Charlie Countryman director Fredrik Bond would be taking over direction from Fuqua,  but Bond left the film on September 18, just six weeks before the shooting was set to begin.  On September 28, director Babak Najafi signed on to direct the film.    On October 10, Jackie Earle Haley joined the film to play Deputy Chief Mason.    On November 12, Mehdi Dehbi joined the film to play Sultan Mansoor, the youngest of three brothers whose life has been forever changed after a drone strike.   

===Filming===
The principal photography on the film began on October 24, 2014, in London.   A four-week shooting would be taken place with actors Freeman, Eckhart, Bassett and Melissa Leo and then there would be a Christmas break.  But Gerard Butler who was filming Geostorm in October, would take part in February 2015 when filming would again start after a pause. The filming has recommenced around March 2015 with Butler and Eckhart filming scenes.    A helicopter was seen making an expert landing in the courtyard of Somerset House, which is normally used to host London Fashion Week and summer film screenings.  Butler and Angela Bassett were seen filming in Somerset House.  Butler stated in an interview that the movie was also filmed in India and is set to continue in Bulgaria.  The President of Bulgaria Rosen Plevneliev visited the set of London Has Fallen during filming at Boyana Cinema Center in Bulgaria.   Filming would last through April 2015. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 