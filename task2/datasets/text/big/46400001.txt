The Crisis is Over
{{Infobox film
| name = The Crisis is Over
| image = 
| image_size =
| caption =
| director = Robert Siodmak
| producer = Seymour Nebenzal
| writer = Jacques Constant   Frederick Kohner   Max Kolpé   Curt Siodmak 
| narrator = Marcel Carpentier Jean Lenoir    Franz Waxman 
| cinematography = Maurice Forster   Eugen Schüfftan
| editing = 
| studio =  Nero Films
| distributor = Paramount Pictures
| released = 5 October 1934
| runtime = 74 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Marcel Carpentier. Many of those who worked on the film were exiles from Nazi Germany.  It was made by Nero Films, which until recently had been based in Berlin.

==Cast==
* Albert Prejean as Marcel 
* Danielle Darrieux as Nicole  Marcel Carpentier as Bernouillin 
* Pedro Elviro as Hercule  
* Paul Velsa as Le machiniste 
* Paul Escoffier as Le manager  
* Milly Mathis as La gouvernante 
* Jeanne Marie-Laurent as La mère de Nicole  
* Régine Barry as Lola Garcin  
* Jane Loury as Mme Bernouillin  
* Suzanne Dehelly as Olga  
* René Lestelly as Alex  
* Alla Donell as Une girl  
* Wanda Barcella as Une girl  De Silva as Une girl   Sherry as Une girl  
* Véra Ossipova as Une girl  
* Adrienne Trenkel as Une girl  
* Mme. Wendler as Une girl 
* Jacques Beauvais as Le maître dhôtel  
* Raymond Blot as Un impresario  
* Suzy Delair 
* Albert Malbert as Le commissaire  
* Franck Maurice 
* Ilse Nast
* Beatrice Emanuel

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 

 
 
 
 
 
 

 