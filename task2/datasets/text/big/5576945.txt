Devil in the Flesh (1998 film)
 Le Diable au corps}}
 
{{Infobox film|
  name     = Devil in the Flesh |
  image          = Devilinfleshus.jpg |
    writer         = Kurt Anderson and Richard Brandes| Phil Morris Robert Silver Sherrie Rose Ryan Bittle|
  director       = Steve Cohen |
  producer       = Robert Baruc John Fremes |
  distributor    = |
  released   = August 21, 1998 |
  runtime        = 99 minutes |
  country = United States |
  language = English |
     budget   = |
  music          = |
}} Le Diable au corps (The Devil in the Flesh).

==Plot==
The film follows a beautiful but troubled young girl, Debbie Strand (Rose McGowan), placed in foster care with her estranged grandmother in Los Angeles after her mother and father die in a suspicious house fire. Her grandmother is an ultra-conservative fundamentalist Christian, who is convinced that she can undo years of Debbies presumed hedonism with some "correctional therapy." Shortly thereafter she begins to abuse Debbie and forces her to wear demeaning and outdated clothes. Debbie soon becomes enthralled with Peter Rinaldi (Alex McArthur), an English teacher at her new high school. However, Peter has a fiancée named Marilyn and strong scruples, so he repeatedly rejects Debbies advances. Before long, Peter finds that his life is ruined and the bodies are piling up. During the hectic climax, Debbie breaks into Marilyns home with the intention of wiping her out. Peter soon realizes Debbie is there and is quickly on his way. Meanwhile, Debbie confronts Marilyn. Marilyn runs through the kitchen, but Debbie tackles her to the ground. After a brief scuffle, Debbie is too strong as she knocks the helpless Marilyn unconscious. As she attempts to murder her, Peter rushes in and saves the day.

==Cast==
*Rose McGowan as Debbie Strand
*Alex McArthur as Peter Rinaldi
*Peg Shirley as Fiona Long Phil Morris as Detective Joe Rosales
*Robert Silver as Detective Phil Archer
*Sherrie Rose as Marilyn
*Ryan Bittle as Greg Straffer
*Wendy Robie as Principal Joyce Saunders
*Morgan DiStefano as Student

==External links==
* 
*  
*  

 
 
 
 
 
 
 


 