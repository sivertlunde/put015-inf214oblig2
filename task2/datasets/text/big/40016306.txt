Unstoppable (2013 film)
{{Infobox film
| italic title   = force
| name           = Unstoppable
| image          = File:Unstoppable 2013 cover.jpg
| director       = Darren Doane
| writer         = Kirk Cameron
| starring       = Kirk Cameron
| producer       = John Bona Marshall Foster
| studio         = Liberty University Camfam Studios
| distributor    =
| released       = September 24, 2013
| country        = United States
| language       = English
}}
Unstoppable is an 2013 American documentary film hosted by Kirk Cameron. Directed by Darren Doane. Inspired by the death of a close friend who succumbed to cancer at age 15, Cameron (who also wrote the script) has stated that his mission for the film is to answer the question of, "Where is God in the midst of tragedy and suffering?" The film is being made in partnership with Liberty University and was released for a one-night-only screening in theaters on September 24, 2013.

Links to the films official website were blocked from Facebook and the trailer was removed from YouTube. After widespread news coverage and outrage from fans, the Facebook block was lifted. Spokespeople for Facebook said that the domain name purchased for the film was previously used for spam, and the link was reinstated after a short period of time. YouTube reallowed the trailer, but did not make a public comment.

== Description ==
According to the projects official Facebook page:

 In Unstoppable, a brand-new documentary, Kirk takes you on a personal and inspiring journey to better understand the biggest doubt-raiser in faith: Why? Kirk goes back to the beginning—literally—as he investigates the origins of good and evil and how they impact our lives … and our eternities. Reminding us that there is great hope, Unstoppable creatively asks—and answers—the age-old question: Where is God in the midst of tragedy and suffering? 

The film is made in partnership with Liberty University. 

== Facebook and YouTube controversy ==
On July 18, 2013, Kirk Cameron posted a picture to Facebook asking his fans to petition Facebook to stop censoring links to the movies website. He said, "Calling all friends of Faith, Family, and Freedom! Facebook has officially blocked me and you (and everyone else) from posting any link to my new movie at UnstoppableTheMovieDOTcom."  More than 300,000 people shared the picture. The story spread to numerous media outlets such as CBS,  ABC    and Entertainment Tonight.  The link was reinstated  soon thereafter.

A spokesperson for Facebook stated that the site "was blocked for a very short period of time after being misidentified as a potential spam or malware site. We learn from rare cases such as these to make our systems even better."  Michael Kirkland, communications manager of Facebook, made the following statement to   purchased for the movie was previously being used as a spam site and it hadnt been refreshed in our system yet. We were in direct contact with Kirks team on this and reversed the block as soon as we confirmed that the address was no longer being used for spam." 

Soon after the link was unblocked on Facebook, a teaser trailer for Unstoppable was blocked on YouTube. It was replaced by a message warning that, "This video has been removed as a violation of YouTubes policy against spam, scams, and commercially deceptive content."  YouTube later removed the block due to outcries of censorship from fans and media outlets.    Representatives for YouTube did not immediately respond to a request for comment.  

== Release ==
Unstoppable was released on September 24, 2013 as a one-night live stream from Liberty University.  Church showings were scheduled to begin November 15, 2013. http://unstoppablethemovie.com/ 

== Home media ==
DVD was made available January 28, 2014. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 