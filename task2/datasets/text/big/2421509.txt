Another Woman
 
{{Infobox film
| name = Another Woman
| image = Another_woman_moviep.jpg
| image size = 
| caption = Theatrical release poster
| director = Woody Allen
| producer = Robert Greenhut
| writer = Woody Allen
| starring = Gena Rowlands Ian Holm Mia Farrow
| music = 
| cinematography = Sven Nykvist
| editing = Susan E. Morse
| distributor = Orion Pictures
| released =  
| runtime = 84 minutes
| country = United States
| language = English
| budget =
| gross = $1,562,749
}} drama stars Gena Rowlands as a philosophy professor who accidentally overhears the private analysis of a stranger but finds the womans regrets and despair awaken in her something personal.

==Synopsis==

Marion Post (Gena Rowlands) is a New York philosophy professor past the age of 50 on a leave of absence to write a new book. Due to construction work in their building, she sublets a furnished flat downtown to have peace and quiet.

Her work there is interrupted by voices from a neighboring office in the building where a therapist conducts his analysis. She quickly realizes that she is privy to the despairing sessions of another woman (Mia Farrow) who is disturbed by a growing feeling that her life is false and empty. Her words strike a chord in Marion, who begins to question herself in the same way.

She comes to realize that, like her father ( ) and his fragile wife Lynn (Frances Conroy), her best friend from high school Claire (Sandy Dennis), her first husband Sam (Philip Bosco), and her stepdaughter Laura (Martha Plimpton).

She also realizes that her marriage to her second husband, Ken (Ian Holm), is unfulfilling and that she missed her one chance at love with his best friend Larry (Gene Hackman). She finally manages to meet the woman in therapy as she contemplates a Klimt painting called "Hope". Although she wants to know more about the woman, she ends up talking more about herself, realizing that she made a mistake by having an abortion years ago and that at her age there are many things in life she will not have anymore.

By the end of the film, Marion resolves to change her life for the better.

==Cast==
*Gena Rowlands...  Marion Post
*Ian Holm... Ken
*Mia Farrow... Hope
*Blythe Danner...  Lydia
*Betty Buckley...  Kathy
*John Houseman...  Marions Father
*Sandy Dennis...  Claire
*Frances Conroy...  Lynn
*Philip Bosco...  Sam
*Martha Plimpton...  Laura
*Harris Yulin...  Paul
*Gene Hackman...  Larry Lewis
*David Ogden Stiers... Young Marions Father

==Background== Wild Strawberries, Gymnopedie No. 1 by Erik Satie (Debussys orchestral arrangement renamed as Gymnopédie No. 3), and poetry- Archaic Torso of Apollo by Rainer Maria Rilke, to serve its narrative, as do earlier and later films such as Hannah and Her Sisters, Crimes and Misdemeanors, and Husbands and Wives. It also focuses primarily on upper middle class intellectuals, as nearly all of Allens 1980s films do.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 