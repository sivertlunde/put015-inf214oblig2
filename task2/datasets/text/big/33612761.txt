Sangham (1988 film)
{{Infobox film
| name           = Sangham
| image          =
| caption        =
| director       = Joshiy
| producer       = K Rajagopal
| writer         = Dennis Joseph
| screenplay     = Dennis Joseph Parvathy Saritha Shyam
| cinematography =
| editing        = K Sankunni
| studio         = KRG Movie International
| distributor    = KRG Movie International
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, Parvathy and Saritha in lead roles. The film had musical score by Shyam (composer)|Shyam.    The film is about a spoiled brat and his adolescent friends, and the embarrassing situation that they get trapped in. Mammootty appears in the leading role of Kuttapayi, a spoiled brat of a rich family. The film was a huge hit at box office.

==Cast==
 
*Mammootty
*Thilakan Parvathy
*Saritha Seema
*Mukesh Mukesh
*PC George
*James
*Prathapachandran
*Appa Haja
*Balan K Nair
*Ganesh Kumar
*Jagadish
*VK Sreeraman
*Vinu Chakravarthy
 

==Soundtrack== Shyam and lyrics was written by Shibu Chakravarthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Innalle Punchavayal || P Jayachandran, Chorus || Shibu Chakravarthy || 
|-
| 2 || Nirasandhya || KS Chithra || Shibu Chakravarthy || 
|}

==References==
 

==External links==
*  

 
 
 

 