How to Deal

{{Infobox film
 | name = How to Deal
 | image = How to deal poster.jpg
 | caption = Theatrical release poster
 | director = Clare Kilner
 | writer = Sarah Dessen ( novels ) Neena Beber
 | starring = Mandy Moore Allison Janney Trent Ford
 | producer = Erica Huggins
 | distributor = New Line Cinema (USA) Focus Features (International)
 | budget = $16,000,000
 | gross= $14,308,132  	
 | released = 18 July 2003 (USA)
 | runtime = 101 minutes
 | country = United States
 | language = English
}}
 That Summer Someone like You.

==Plot==

Halley Marie Martin (Mandy Moore) is a 17-year-old high school student who is disillusioned with love after seeing many dysfunctional relationships around her. Her parents are now divorced and her father, Len Martin (Peter Gallagher), a radio talk show host, has a new young girlfriend that the entire family despises; mainly because shes the reason for the divorce. Her mother, Lydia (Allison Janney), is now always alone while her sister, Ashley, is so overwhelmed by her upcoming wedding with Lewis Warsher that she barely exists in the house anymore. On top of that, the shallowness of all the girls and guys at her school convinces Halley that finding true love is impossible.

When Halley walks in on her best friend Scarlett having sex with her boyfriend (Scarletts boyfriend, not Halleys), the high school soccer champ, Michael Sherwood, Halley tries to warn her of the complications that lie ahead. Ignoring her advice, Scarlett embarks on a summer fling with Michael. In the meantime, Halley must deal with Ashley and the rest of the Martins must deal with her soon-to-be in-laws, Lewis overbearing Southern parents, who still have a maid, who just happens to be African-American. In the meantime, Halley runs into classmate Macon Forrester, a slacker who never shows up for biology and is more interested in having fun than school work. He is Michael Sherwoods best friend.

Then a few weeks later, tragedy strikes. Michael dies of a heart defect on the soccer field while Halley, Scarlett and other classmates watch helplessly. This event changes the lives of Halley and Scarlett forever. While Scarlett does her best to look beautiful for Michael at his funeral, Macon gives a moving speech about his friend. Struggling with Michaels death, Scarlett, at age sixteen, soon learns that she is pregnant with his child. With Halley at her side, Scarlett reveals the pregnancy to her mother.

Halleys father, Len Martin, marries his mistress, Lorna, in a beach-side ceremony, with guests from all over the world, or "within his radio frequency", as Halley puts it.

Halley and Macon eventually start a relationship. He takes her to his and Michaels favorite hangout and once come close to having sex. Halley and her mother get into an argument and on New Years Eve, Halley sneaks out with Macon to a party. Again, they come very close to having sex but this time Halley stops it and Macon is upset. On their way home, they are both upset and distracted and hit a tree. Halley gets a broken arm, but otherwise they both walk away from it unharmed, but Halley breaks up with Macon saying that she cant wait around for him to grow up. Later things go awry when Halley finds Ashley drunk on the familys front porch. Finding a male strippers thong around her neck, Halley witnesses the break-up of her sisters engagement. She also learns that her mother sneaks out once a week to have sex with a man she met a couple of weeks previous, Steve. After another make-up, Lewis proposes to Ashley again, this time at a crowded airport: she says yes.

On the day of Ashley and Lewis wedding, Macon bursts into Lens radio studio, professing his apologies and love to Halley. He then heads to the wedding. On the way, he finds Scarlett, who insisted on walking to the wedding, going into labor on the side of the road. He puts her in the car and walks into the wedding, getting Halleys attention. The three rush to the hospital, leaving Scarletts mother behind in the rush.

While Scarlett is having her baby, Halley makes a list of all the little things she hates about Macon, the way his hair falls over his face, the way his voice gets low when it serious, the way he bites his lip when hes nervous and the way his eyebrow twitches. Halley playfully uses Macons Jedi Mind Trick and they kiss and dance briefly in the hallway. The movie ends as Halley and Macon embrace and we see that Scarlett gave birth to a baby boy.

==Main cast==
*Mandy Moore - Halley Marie Martin
*Allison Janney - Lydia Williams Martin
*Trent Ford - Macon Forrester
*Alexandra Holden - Scarlett Smith
*Dylan Baker - Steve Beckwith
*Nina Foch - Grandma Halley Williams
*Peter Gallagher - Len Martin
*Mackenzie Astin - Lewis Gibson Warsher II
*Connie Ray - Marion Smith
*Mary Garrison - Ashley Renee Martin
*Sonja Smits - Carol Warsher
*Laura Catalano - Lorna Queen
*Ray Kahnert - Donald Sherwood
*Andrew Gillies - Buck Warsher John White - Michael Sherwood
*Alison MacLeod - Sharon Sherwood
*Bill Lake - Ed
*Charlotte Sullivan - Elizabeth Gunderson
*Philip Akin - Mr. Bowden
*Claire Crawford - Seamstress
*Ennis Esmer - Ronnie
*Thomas Hauff - Minister

==Soundtrack==
===Tracklisting===
#Billy S. - Skye Sweetnam
#Do You Realize?? - The Flaming Lips
#Its On The Rocks - The Donnas
#Why Cant I? - Liz Phair
#Wild World - Beth Orton John Mayer
#Thats When I Love You - Aslyn
#Thinking About Tomorrow - Beth Orton
#Promise Ring - Tremolo
#Take The Long Road And Walk It - The Music
#Waves - Marjorie Fair
#Surrender - Echo
#Wild World - Cat Stevens

==Reception==
The films reviews were generally negative, with a score of 28% in review aggregator Rotten Tomatoes based on 93 reviews. 

===Box office===
In its opening weekend, the film grossed $5,809,960 million in 2,319 theaters in the United States and Canada, ranking #8 at the box office; it was the best debut for a teen film that week. By the end of its run, How to Deal grossed $14,195,227 domestically and $112,905 internationally, totaling $14,308,132 worldwide, making the film a monetary success. {{cite web
| url=http://boxofficemojo.com/movies/?id=howtodeal.htm
| title=How to Deal (2003)
| publisher=Box Office Mojo
| accessdate=2011-07-28
}} 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 