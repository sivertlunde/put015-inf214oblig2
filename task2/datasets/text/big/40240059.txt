Every Thing Will Be Fine
 
{{Infobox film
| name           = Every Thing Will Be Fine
| image          = 
| caption        = 
| director       = Wim Wenders
| producer       = Gian-Piero Ringel
| writer         = Bjørn Olaf Johannessen Robert Naylor Rachel McAdams
| music          = Alexandre Desplat 
| cinematography = Benoît Debie
| editing        = Toni Froschhammer
| studio         = Neue Road Movies
| distributor    = 
| released       =  
| runtime        = 118 minutes 
| country        = Germany Canada France Sweden Norway 
| language       = English
| budget         = 
| gross          = 
}}

Every Thing Will Be Fine is a German drama film directed by Wim Wenders and written by Bjørn Olaf Johannessen, and released in 3D film|3D. It is Wenders first full-length dramatic feature in seven years. The film stars James Franco, Charlotte Gainsbourg, Rachel McAdams and Marie-Josée Croze.  Every Thing Will be Fine premiered out of competition in February 2015 at the 65th Berlin International Film Festival.   

== Plot ==
The film centres around Tomas (James Franco), a writer who causes an accident while driving and spends the next 12 years examining the effect of the tragedy on his life and that of Kate (Charlotte Gainsbourg), who lost someone close. 

== Cast ==
* James Franco as Tomas  
* Charlotte Gainsbourg as Kate
* Rachel McAdams as Sara
* Marie-Josée Croze as Ann  
* Peter Stormare as Editor
* Patrick Bauchau as Dad
* Julia Sarah Stone as Mina Robert Naylor as Christopher
* Philippe Vanasse-Paquet as 12-year-old Christopher

== Production ==
Producer Gian-Piero Ringel produced the film through his company Neue Road Movies. HanWay Films has worldwide distribution rights.    Belgian Benoît Debie was the director of photography, Warner Bros. released the film in Germany and Mongrel Media released it in Canada.    One week before the films premiere at the Berlin Film Festival, French composer Alexandre Desplat recorded the score with the Gothenburg Symphony Orchestra in Sweden.  

=== Casting ===

On May 7, 2013 James Franco joined the cast of the film as lead actor,    playing Tomas, a writer who accidentally causes the death of a child.  The day before shooting it was announced that the cast would include Charlotte Gainsbourg and Marie-Josée Croze, with Gainsbourg was set to play the female lead role.   

=== Filming ===
Director Wim Wenders began shooting the film on August 13, 2013 in Montreal, Canada.   After a break shooting began again in winter 2014. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 