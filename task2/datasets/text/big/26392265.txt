Ciao (film)
{{Infobox film
| name           = Ciao
| image          = CiaoPoster1500.jpg
| caption        = Ciao movie poster
| director       = Yen Tan Jim McMahon
| writer         = Yen Tan Alessandro Calza
| starring       = Adam Neal Smith Alessandro Calza Charles W. Blaum Ethel Lung
| music          = Glen Walsh
| cinematography = Michael Victor Roy
| editing        = David Patrick Lowery
| distributor    = Regent Releasing
| released       =  
| runtime        = 87 minutes
| country        = United States Italian
| budget         = 
}} gay independent film directed and co-written by Yen Tan, starring Adam Neal Smith, Alessandro Calza, Charles W. Blaum and Ethel Lung.

==Synopsis== Dallas

Marks best friend Jeff (Adam Neal Smith) is left with the task of going through Marks stuff and informing relatives and friends of his death. While going through Marks e-mails to let people know about his passing, Jeff discovers that Mark had been corresponding with an Italian man named Andrea (Alessandro Calza), who has already planned a trip to fly to Dallas and to visit Mark for the first time without knowing he has actually died.

Jeff invites Andrea to come to Texas anyway and stay with him for two days at his place. Ciao portrays these two days where the two bereaved friends one from Dallas and the other from Italy meet and talk mostly about Mark and the impact he had on both of them in a close, personal and frank manner. Through these intimate conversations, the two men form a rapport that grows, and they are soon drawn together both by their connection with the deceased Mark, and by a growing intimacy with each other. Andrea has to leave at the end of his 2-day stay, but invites Jeff to come to Italy for a visit at some later date.

==Cast==
*Adam Neal Smith as Jeff
*Alessandro Calza as Andrea
*Charles W. Blaum as Mark
*Ethel Lung as Lauren
*John S. Boles as Marks Father
*Margaret Lake as Marks Mother
*Tiffany Vollmer as Doctor

==Production== Jim McMahon, co-produced and edited by David Patrick Lowery, and co-produced by James M. Johnston, who also served as the 1st assistant director.

==Music==
Main musical theme of the film is "Five Times a Minute" sung by Charles W. Blaum (who plays the role of the dead Mark) and Adam Neal Smith (who plays the role of Jeff). The song was written by Curtis Glenn Heath. It is shown when Andrea (played by Alessandro Calza) introduces a video he had received from Mark where he professes his love and regards for both Andrea and Jeff.

==Reception==
The film received mixed reviews. Ruthe Stein from the San Francisco Chronicle praised the acting, but like some other reviewers criticised the "snails pace" of the movies story.  AfterElton named Ciao "the best gay movie Ive seen this year"  and the Los Angeles Times called it "a revelation; a minimalist work of maximum effect".

==Awards==
The film won the Jury Prize / Best Feature Film at the Philadelphia International Gay & Lesbian Film Festival, the Queer Lion Competition at Venice Film Festival, was given honorable mention at the AFI Dallas Awards, and was part of the Official Selection for Outline Framefest Newline. 

==References==
 

==External links==
*  
* 
* 

 
 
 
 
 
 
 
 
 