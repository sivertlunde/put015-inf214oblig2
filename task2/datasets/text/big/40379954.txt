Cry Vengeance
{{Infobox film
| name           = Cry Vengeance
| image          = Cry Vengeance poster.jpg
| alt            =
| caption        = Theatrical release poster Mark Stevens
| producer       = Lindsley Parsons John H. Burrows
| screenplay     = Warren Douglas George Bricker
| story          = 
| narrator       = 
| starring       = Mark Stevens
| music          = Paul Dunlap
| cinematography = William A. Sickner
| editing        = Elmo Veron Allied Artists
| distributor    = Allied Artists
| released       = 21 November 1954
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Mark Stevens, starring himself. 

==Plot== Douglas Kennedy), whom he considers responsible.

It turns out that Morelli is hiding out in Ketchikan, Alaska. After his arrival in the isolated city, Vic finds his enemy but also the latters charming little daughter. With the help of tavern owner Peggy Harding (Martha Hyer), he discovers that Morelli did not order the bombing and that the true murderer was the hitman Roxey (Skip Homeier).

Roxey, who has followed Vic, murders Morelli, but is wounded by Vic in a shootout, then falls from atop a dam. After saying farewell to Roxey and to Morellis orphaned daughter, Vic travels back to San Francisco, but with a hint that he might return.

==Cast== Mark Stevens as Vic Barron
* Martha Hyer as Peggy Harding
* Skip Homeier as Roxey
* Joan Vohs as Lily Arnold  Douglas Kennedy as Tino Morelli
* Don Haggerty as Lt. Pat Ryan
* Cheryl Callaway as Marie Morelli
* Warren Douglas as Mike Walters
* Mort Mills as Johnny Blue-eyes
* John Doucette as Red Miller Lewis Martin as Nick Buda

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 

 