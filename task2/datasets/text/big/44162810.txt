Aashrayam
{{Infobox film
| name           = Aashrayam
| image          =
| caption        =
| director       = K Ramachandran
| producer       = John Paul (dialogues) John Paul
| starring       = Prem Nazir Sukumari Nedumudi Venu Sankaradi
| music          = MB Sreenivasan
| cinematography = Anandakkuttan
| editing        = MN Appu
| studio         = Minar Movies
| distributor    = Minar Movies
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by K Ramachandran. The film stars Prem Nazir, Sukumari, Nedumudi Venu and Sankaradi in lead roles. The film had musical score by MB Sreenivasan.   

==Cast==
*Prem Nazir
*Sukumari
*Nedumudi Venu
*Sankaradi
*Kalaranjini Baby Anju
*Kasim Seema
*Ramachandran

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaayalin Kaathil   || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Nithyanaaya manushyanu vendi || K. J. Yesudas, Chorus || Poovachal Khader || 
|-
| 3 || Pirannaalillaatha || K. J. Yesudas, S Janaki, Nedumudi Venu || Poovachal Khader || 
|-
| 4 || Thaazhikakkudavumaay || K. J. Yesudas, S Janaki || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 