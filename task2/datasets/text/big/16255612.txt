Unbelievable Adventures of Italians in Russia
{{Infobox film
| name           = Unbelievable Adventures of Italians in Russia
| image          = Unbelievable Adventures of Italians in Russia.jpg
| image_size     =
| caption        = US Theatrical Poster
| director       = Eldar Ryazanov Franco Prosperi
| producer       = Dino De Laurentiis Luigi De Laurentiis
| writer         = Emil Braginsky Franco Castellano Giuseppe Moccia Eldar Ryazanov Andrei Mironov Ninetto Davoli Antonia Santilli Alighiero Noschese Tano Cimarosa Yevgeniy Yevstigneyev Olga Aroseva
| music          = Carlo Rustichelli
| cinematography = Mikhail Bitz Gábor Pogány
| editing        = A. Stepanov
| distributor    = Mosfilm
| released       = January 31, 1974
| runtime        = 100 min.
| country        = Soviet Union Italy
| language       = Russian rubles
| gross          =
| preceded_by    =
| followed_by    =
}} Italian film about some Italians searching for treasure in Russia, and directed by Franco Prosperi and Eldar Ryazanov.

== Plot summary == male nurses, mafioso and a hobbling man. After that they all fly to Russia by the same flight (of course none of them tells the others that hes flying to Russia for the treasures). Having some adventures in the airplane because of mafioso, they all arrive to Leningrad and begin to dig under every lion statue in the city.

== Cast == Andrei Mironov — Andrey Vasiliev
* Ninetto Davoli — Giuseppe (voiced by Mikhail Kononov)
* Antonia Santilli — Olga (voiced by Natalia Gurzo) Aleksandr Belyavskiy)
* Tano Cimarosa — Rosario Agrò (voiced by Mikhail Gluzsky)
* Yevgeniy Yevstigneyev — Lame man
* Olga Aroseva — Andreys mother
* lion named King (lion)|King.

== Crew ==
*Screenplay by — Emile Braginsky, F. Kastellano, D. Pipolo, E. Ryazanov
*Directed by — Eldar Ryazanov
*Main Operators — Habor Pogani, Mikhail Bitz
*Main Artist — Michail Bogdanov
*Art Directors of Costumes — E. Fiorentini, L. Kusakova
*Composer — Carlo Rustichelli
*Soundoperator — Jury Michaylov
*Conductors — E. Hachaturian, I. Shpiller
*Director — V. Dostale
*Operators — I. Simonelli, V. Semin
*Montage — I. Brojovskaya
*Grime — O. Struncova, M. Chiglyakova
*Editor — A. Stepanov
*Music Editor — R. Lukina
*Directors of Picture — Karlen Agajanov and Karlo Bartolini

== External links ==
* 
* 

 

 
 
 
 
 
 