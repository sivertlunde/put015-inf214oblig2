Yugpurush
{{Infobox film
| name           = Yugpurush
| image          = Yugpurush(1998).jpg
| image_size     = 200px
| caption        = Movie poster
| director       = Partho Ghosh
| producer       = Vijay Mehta
| writer         = Pradeep Ghatak (screenplay),
                   Hriday Lani(dialogue)
| narrator       =
| starring       = Nana Patekar Jackie Shroff Manisha Koirala
| music          = Amar Haldipur Rajesh Roshan
| lyrics         = Majrooh Sultanpuri
| cinematography = Ramanna
| editing        = R Rajendran
| choreography   = Bhushan Lakhandri
| distributor    = Eros International
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}

Yugpurush is a 1998 Drama Bollywood film directed by Partho Ghosh and produced by Vijay Mehta. The film stars Nana Patekar, Jackie Shroff and Manisha Koirala in the title roles.   It is an adaptation of Fyodor Dostoyevsky novel Idiot.

==Cast==
*Nana Patekar-Anirudh
*Jackie Shroff-Ranjan Duara
*Manisha Koirala-Sunita
*Ashwini Bhave-Deepti
*Mohnish Bahl-Mohnish
*Yashwant Dutt-Yeshwant Dutt
*Shivaji Satam-Paresh Kumar
*Mohan Joshi-Mohan
*Sulbha Deshpande-Mrs. Mohan

==Plot==

Anirudh (Nana Patekar) is released from a facility treating patients with mental ailments. Anirudh also happens to be an exceptional artist and does portraits of new people he meets. As he struggles with adjusting with the "normal" world, he encounters Sunita (Manisha Koirala), an escort and Ranjan (Jackie Shroff), the son of a renowned politician. Ranjan, is madly in love with Sunita but Sunita does not feel the same way about him. Anirudh is the only person to see beyond Sunitas seemingly comfortable and affluent existence and knows that deep down she is extremely sad and alone. They develop a special bond of friendship despite their vastly different personalities and backgrounds. Anirudh also becomes good friends with Ranjan. Ranjan is a good-hearted but egotistical man who cannot bear even the thought of failure in any aspect of his life. Can he handle Sunitas rejection? Can he deal with the fact that Sunita feels closer to Anirudh than him? Will Sunita find happiness? And, most importantly, will Anirudh survive in this "sane" world?

==Music==

The music of the film is composed by Rajesh Roshan and Amar Parshuram Haldipur while sound mixing is done by Hitendra Ghosh and Buta Singh.The lyrics of the film is penned by Majrooh Sultanpuri  under music company T-Series.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| title1 = Bandhan Khula Panchhi Udaa
| extra1 = Ravindra Sathe
| lyrics1 = 
| length1 = 4:48
| title2 = Chale Hum Do Jan Sair Ko
| extra2 = Kumar Sanu, Ravindra Sathe
| lyrics2 = 
| length2 = 5:53
| title3 = Yeh Jeevan Path Mera
| extra3 = Ravindra Sathe
| lyrics3 = 
| length3 = 5:34
| title4 = Koi Jaise Mere Dil Ka
| extra4 = Asha Bhosle
| lyrics4 = 
| length4 = 5:11
| title5 = Kanhaiya Se Kahiyo
| extra5 = Ravindra Sathe
| lyrics5 =
| length5 = 6:17
| title6 = Hello Hello Aayee
| extra6 = Alka Yagnik
| lyrics6 = 
| length6 = 7:03
}}

==References==
 

==External links==
*  

 
 