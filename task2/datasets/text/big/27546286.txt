Escape from the Dark
 
 
{{Infobox film
| name           = Escape from the Dark
| caption        = 
| image	=	Escape from the Dark FilmPoster.jpeg
| director       = Charles Jarrott
| writer         = 
| starring       = Alastair Sim
| music          = 
| cinematography = 
| editing        =  Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 104 minutes
| country        = United Kingdom United States
| language       = English
| budget         = 
| gross          = 
}} Walt Disney Productions film, it was retitled The Littlest Horse Thieves when released to the US in March 1977. 

This was Alastair Sims final screen role.

==Plot==
Set in a mining town in Yorkshire, the film is about a company that plans to use machines instead of ponies to get coal. A group of children intervene to attempt a rescue of the ponies when they are to be killed but, after they are caught, the authorities must decide what to do about the ponies.

==Cast==
* Alastair Sim as Lord Harrogate
* Peter Barkworth as Richard Sandman
* Maurice Colbourne as Luke Armstrong
* Susan Tebbs as Violet Armstrong Andrew Harrison as Dave Sadler
* Chloe Franks as Alice Sandman

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 