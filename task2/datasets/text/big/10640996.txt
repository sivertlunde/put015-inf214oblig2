Padikkadavan (1985 film)
{{Infobox film|
| name = Padikkadavan
| image = Padikathavan 1985.jpg
| caption = Poster Rajasekhar
| Rajasekhar
| Ambika
| producer = N. Veerasami V. Ravichandran
| music = Ilaiyaraaja
| cinematography = V. Ranga
| editing = R. Vittal S. B. Mohan
| released = 11 November 1985
| studio = Sri Eswari Productions
| distributor = Sri Eswari Productions
| runtime =
| country = India Tamil
| budget =
| gross =  2.96 crore 
}}
 Ravichandran along with his father Veerasami. The film was a huge blockbuster and had a run for more than 200 days breaking the box office records in many distribution territories for Tamil films. This film is considered as one of the biggest hits for Rajnikanth during the 1980s and increased his fan base among all classes of audience.

==Plot==
Sivaji is the loving elder step-brother of Rajinikanth and Vijaybabu. After his marriage, his wife (Vadivukkarasi) sends his brothers out when he goes to Chennai. Rajni, the older of the two, toils and becomes a taxi driver to educate and treat his brother. But his brother strays and gets mixed up with some wrong-doers. He marries a girl (Ramya Krishnan) from a rich family. The girls uncle (Jaishankar), is a smuggler who sells drugs using his brothers lorries, kills him and puts the blame on Rajni. Sivaji, the presiding judge, realises that its his long-lost brother and returns as a lawyer to save him.

==Cast==
* Rajinikanth as Rajendran (Raja)
* Sivaji Ganesan
* Jaishankar
* Nagesh
* Poornam Vishwanathan
* Thengai Seenivasan
* Janakaraj Ambika
* Vadivukkarasi
* Ramya Krishnan
* Indira
* Santhi
* Suryakala
* Vijaybabu as Babu
* Rasi

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vairamuthu || 05:00
|-
| 2 || Oorai Therinchikitten || K. J. Yesudas || 04:07
|-
| 3 || Oru Koottu || Malaysia Vasudevan || 03:45
|- Vaali || 04:32
|-
| 5 || Solli Adipenadi || Malaysia Vasudevan, S. Janaki || Gangai Amaran || 04:29
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 