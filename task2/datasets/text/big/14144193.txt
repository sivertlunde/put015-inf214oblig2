Mysterious Island (serial)
{{Infobox film
| name = Mysterious Island
| image = Mysterious_Island_(serial).jpg
| image_size = 
| caption = 
| director = Spencer Gordon Bennet
| producer = Sam Katzman
| based on =  
| writer = Screenplay:  Lewis Clay Royal K. Cole George H. Plympton Richard Crane Ralph Hodges
| music = Mischa Bakaleinikoff  }} Gerard Carbonara Mario Castelnuovo-Tedesco  }}
| cinematography = William Whitley
| editing = Earl Turner
| distributor = Columbia Pictures
| released =  
| runtime = 15 chapters  }}
| country = United States
| language = English
| budget  = 
| gross = 
}}
 serial released Mercurians as an additional set of villains and it has been labelled as a space opera version of the Verne novel. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut |author2-link=Donald F. Glut  
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | origyear = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 332–336
 | chapter = 13. The Classics "You Say What Dost Thou Mean By That? and Push Him Off the Cliff"
 }}  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 11
 | chapter = 2. In Search of Ammunition
 }} 

==Plot== POW Capt. Cyrus Harding an observation balloon. In his escape, Harding is accompanied by sailor Pencroft, his nephew Bert, writer Gideon, loyal soldier Neb, and a dog. A hurricane blows the balloon off course, and the group eventually crash-lands on a cliff-bound, volcanic, uncharted (and fictitious) island, located in the South Pacific, with very unusual inhabitants. They name it "Lincoln Island" in honour of American President Abraham Lincoln.
 Ayrton (a wild man exiled on the island) and Captain Shard (a ruthless pirate). A mystery man, who possesses great scientific powers, also makes his presence known to the group of people; he is Captain Nemo, who survived the whirlpool in 20,000 Leagues Under the Sea, and unlike the character in the Disney film, was not fatally wounded by military troops from warships. On the way, our quintet of heroes must battle the elements and peoples while trying to figure out a way off the island and back to civilization.

==Cast== Richard Crane Cyrus Harding
* Marshall Reed as Jack Pencroft
* Karen Randle as Rulu of Mercury Ralph Hodges as Herbert Bert Brown
* Gene Roth as Pirate Capt. Shard
* Hugh Prosser as Gideon Spillett
* Leonard Penn as Captain Nemo Terry Frost Ayrton (the wild man)
* Rusty Wescoatt as Moley Bernard Hamilton as Neb William Fawcett as Mr. Jackson
* George Robotham as Mercurian
* Anthony Ross as Confederate lieutenant
* Sid Ross as Mercurian
* Pierre Watkin as Southern officer

==Production== Flash Gordon and masks from The Spiders Web. 

==Critical reception==
Harmon and Glut were largely positive about this serial: "Although fantastic beyond credibility, Mysterious Island actually contained more elements from the original source than most such adaptations of the sound era." 

==Chapter titles==
# Lost in Space
# Sinister Savages
# Savage Justice
# Wild Man at Large
# Trail of the Mystery Man 
# The Pirates Attack
# Menace of the Mercurians
# Between Two Fires
# Shrine of the Silver Bird
# Fighting Fury
# Desperate Chances
# Mystery of the Mine
# Jungle Deadfall
# Men from Tomorrow
# The Last of the Mysterious Island
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 252
 | chapter = Filmography
 }} 

==References==
 

==External links==
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 