Trick or Treat (1952 film)
 
{{Infobox Hollywood cartoon cartoon name=Trick or Treat
|series=Donald Duck
|image=Trick or Treat (1952).jpg image size=
|alt=
|caption=Theatrical release poster
|director=Jack Hannah
|producer=Walt Disney story artist=Ralph Wright voice actor=June Foray Clarence Nash The Mellowmen Paul J. Smith
|animator=Volus Jones Bill Justice George Kreisl Don Lusk Dan MacManus (effects) layout artist=Yale Gracey background artist =Yale Gracey Walt Disney Productions RKO Radio Pictures release date=  (USA) color process=Technicolor
|runtime=8 minutes
|country=United States
|language=English preceded by=Uncle Donalds Ants followed by=Dons Fountain of Youth
}} Walt Disney RKO Radio his nephews Witch Hazel. Paul J. Smith and performed by The Mellowmen.  

==Plot==
The film opens with the song "Trick or Treat for Halloween", the lyrics of which tell the films moral &ndash; one must be generous on Halloween or face trouble.
 Witch Hazel observes Huey, Duey, and Louie trick-or-treating. When the trio go to their uncle Donald Ducks house, Donald decides to prank the boys.  So instead of giving them candy, he intentionally puts firecrackers in their bags, then pulls a string that dumps a bucket of water on their heads. After Donald bids farewell to the boys, the discouraged nephews go and sit on the curb.

But Hazel, who was watching the drama unfold, approaches the boys and tries to encourage them. When she discovers that they believe in witches, she offers to help them get their treats from Donald after all.  At first, she tries to convince Donald herself, but he skeptically retorts, pulls and releases her stretchy nose, and pranks her as well with a bucket of water, not believing she is a real witch. Realizing that the job may be tougher than she anticipated, Hazel tells the boys she will use her magic for this situation.  In another location, a scene paying homage to Shakespeares Macbeth shows Hazel and the nephews concocting a magic potion, adding somewhat more whimsical ingredients than the Three Witches in Macbeth. After testing the potion, Hazel fills an insecticide duster (similar in appearance to a Flit gun) and returns to Donalds house.

Upon arriving back at Donalds house, Hazel sprays the potion at an assortment of objects, causing them to become animated or  , he changes his mind. Donald locks his pantry and swallows the key. Hazel then uses the potion on Donalds feet to give her control of their maneuverability, and commands them to kick out the key, causing Donald to perform a crazy dance. When the key is kicked out, Donald throws it under the pantry door. Hazel sprays Donalds feet again and orders them to smash the door down with Donald. This is initially unsuccessful, so Hazel commands him to take a longer start (bout a MILE OR TWO!), and he literally runs that far before he breaks down the pantry door and is left unconscious on the floor in defeat.

In the end, Huey, Duey, and Louie collect their treats and Hazel departs. A final shot shows an enchanted Jack-o-lantern suddenly pop onto the screen saying "Boo!" to the viewers before smiling.

==Adaptations==
 
A print adaptation by Carl Barks was published simultaneously in the Donald Duck comic book. Barks was given a storyboard of the film by Ralph Wright while production of the film was still in progress. Barks was asked to create a 32-page comic adaptation, yet Barks did not believe he had enough material. In the end he wound up making a lot of his own material, even creating new characters such as Smorgie the Bad.

When the final product was sent to the publisher, Barks segment with Smorgie was rejected, and the story was cut to 27 pages. To fill out the rest of the comic book, Barks created an additional story called "Hobblin Gobblins." The original story was later restored with the publication of the Carl Barks Library. 

Disneyland Records also produced an audio adaptation that was narrated by Ginny Tyler who also voices Witch Hazel. This version was 12 minutes long and also included a song and story from the Haunted Mansion Disneyland attraction.  

==Releases==
*1952 &ndash; Original theatrical release
*1957 &ndash; Walt Disney anthology television series|Disneyland, episode #3.15: "All About Magic" (TV) Super 8 release
*1972 &ndash; The Mouse Factory, episode #1.4: "Spooks and Magic" (TV)
*1983 &ndash; A Disney Halloween (TV special)
*1990 &ndash; " " (VHS) The Ink and Paint Club, episode #1.34: "Donalds Nephews" (TV) The Legend Gold Classics Collection (VHS and DVD) The Black Cauldron, Gold Classics Collection (VHS and DVD)
*2002 &ndash; Mickeys House of Villains (DVD)
*2008 &ndash; " " (DVD) The Black Cauldron, 25th Anniversary Edition (DVD)

==References==
 
 
 
 
 
 
 
 