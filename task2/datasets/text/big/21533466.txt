The Spessart Inn
 
{{Infobox film
| name           = Das Wirtshaus im Spessart The Spessart Inn
| image          = Das Wirtshaus im Spessart movie poster.jpg
| caption        = Film poster showing Liselotte Pulver and Carlos Thompson.
| director       = Kurt Hoffmann
| producer       =  
| writer         =       based on a novella by Wilhelm Hauff
| starring       = Liselotte Pulver
| music          = Franz Grothe
| cinematography =  
| editing        = Claus von Boro
| studio =         Georg Witt-Film GmbH
| distributor    = Constantin Film
| released       =  
| runtime        = 99 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

The Spessart Inn ( ) is a 1958 West German musical comedy film directed by Kurt Hoffmann. It starred Liselotte Pulver and Carlos Thompson.

==Plot==
In the early years of the 19th century, Felix and Peter, two journeyman|journeymen, are travelling across the Spessart hills to Würzburg. Scared of the bandits that plague these parts, especially after a brief encounter with them, the two are glad to find an inn in the middle of the forest. However, it turns out that they have wandered into a den of thieves. The owners are in league with the bandits, who this very night plan to abduct Franziska, the Comtesse von Sandau, who is travelling through the forest with a group including her fiancee, Baron Sperling. Their coach is waylaid by a trap and the bandits direct them to the nearby inn. The waitress warns the journeymen of impending danger and they pass on the warning to the nobles. During the night, the brigands arrive. To escape, the Comtesse switches places with Felix and, in a mans clothes, escapes with Peter. Felix (as Comtesse), Franziskas maid and the pastor are taken to the bandits lair. The bandits send Baron Sperling on his way to carry the demand for 20,000 Gulden to Graf Sandau, Franziskas father.

When Franziska arrives at her fathers castle, he refuses to pay the ransom for the commoner who has taken his daughters place. Instead, Graf Sandau decides to send the military against the bandits. Franziska thus rides to the bandits lair and pretends to be a highwayman herself. The bandit leader accepts her as a henchman, but makes her sleep in his hut. In the morning he discovers her true identity but keeps this information from his men.

When Graf Sandau finds out that the Comtesse has gone to the bandits he sends Baron Sperling to the inn with the ransom money. The soldiers shadow him but the plan to follow the bandits back to their lair fails. Meanwhile, at the bandits lair, their corporal finds out that the "Comtesse" they have imprisoned is a man. Franziska, on the pretense that she wants to find out the truth about the prisoner, switches back places with Felix, which saves his life but makes her a prisoner. The corporal wants to kill her and a confrontation ensues. The leader sides with the prisoners and during the fight, the parson escapes. The soldiers find the bandits lair, but the leader snatches the Comtesse and rides off.

Franziska conceals the bandit leader in her fathers castle. He reveals to her that he is in fact the son of an Italian Count from whom Graf Sandau borrowed money in the past, which he never repaid. After his fathers death he came to Germany to regain his money from Graf Sandau, but before he could do so he was taken prisoner by the bandits. They forced him to join their band and eventually he became their leader. The planned abduction of the Comtesse was intended to make Graf Sandau finally pay the money he owed.

The soldiers search the castle and the bandit leader has to flee. However, he later returns and elopes with Franziska who was about to get married to Baron Sperling—a purely financial match set up by her father. The bandit leader/count takes Franziska in lieu of the money he is owed and they drive off in a wagon.

==Cast==
* Liselotte Pulver as Comtesse Franziska von Sandau
* Carlos Thompson as Bandit Captain
* Günther Lüders as Baron Eberhard Sperling
* Rudolf Vogel as Parucchio
* Wolfgang Neuss as Knoll Wolfgang Müller as Funzel
* Ina Peters as Maid Barbara
* Kai Fischer as Bettina
* Veronika Fitz as Louise
* Herbert Hübner as Graf Sandau
* Hubert von Meyerinck as Obrist von Teckel
* Helmut Lohner as Felix
* Hans Clarin as Peter
* Paul Esser as Bandit Corporal
* Otto Storr as Parson
* Karl Hanft as Jacob
* Heini Göbel as Coachman Gottlieb
* Ernst Brasch as Servant Anton
* Vera Complojer as Landlady
* Anette Karmann as Kitchen Maid Adele
* Georg Lehn as Stadtbote
* Ralf Wolter as Bandit 

==Production==
The script was based on an 1826 novella by Wilhelm Hauff and written by Heinz Pauck and Luiselotte Enderle. Curt Hanno Gutbrod also worked on the script.  An earlier film version of Hauffs story had been released in 1923, directed by Adolf Wenter and starring Friedrich Berger and Ellen Kürty.   
 Schloss Mespelbrunn, actually located within the Spessart hills, was used as the castle of Graf Sandau. Another location was the market square of Miltenberg, a town between the Spessart and the Odenwald.

==Release==
Das Wirtshaus im Spessart premiered on 15 January 1958 at the Gloria-Palast in Berlin.   

The film was entered into the 1958 Cannes Film Festival and nominated for the Palme dOr.   

Kurt Hoffmanns Das Spukschloss im Spessart and Herrliche Zeiten im Spessart were sequels of a sort to this film.

==Reception==
The  " for "Artistically Best Film of the Year" and in 1961 the Preis der deutschen Filmkritik for "Best Film".

Liselotte Pulver received the Filmband in Silber for "Best Actress" at the Deutscher Filmpreis in 1958 for her role as Franziska. 

The Lexikon des internationalen Films calls the film "a colourful, delightfully spooky and cheerful film with a parodistic touch".   

More recently, theatres in several towns in Germany, including Mespelbrunn, Fulda or Donauwörth have staged plays/musicals that were based on the film or at least included some of the elements it had added to Hauffs original story.      
 {{cite web|url=http://www.fulda.de/aktuelles/news/einzelansicht/das-wirtshaus-im-spessart-im-schlosstheater.html
|title=Fulda.de: Das Wirtshaus im Spessart (German)|accessdate=25 May 2013}} 

==Further reading==
* Wilhelm Hauff: The Caravan. The Sheik of Alexandria and his Slaves. The Inn in the Spessart (Classic Reprint). Forgotten Books, 2010. ISBN 978-1440069499. 
* Klaus Rosenthal: Das Wirtshaus im Spessart. Ein deutsches Film-Musical (German). Schlossallee-Verlag, Mespelbrunn 1998. (self-published)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 