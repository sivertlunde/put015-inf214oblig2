Masikip sa Dibdib
{{Infobox film
| name           = Masikip sa Dibdib
| image          = Masikipdvd.jpg
| alt            = Cover shows a yellow-green background. In the foreground are the films characters. The films name is at the upper-right side.
| caption        = DVD release cover
| director       = Joyce E. Bernal
| producer       = Vic del Rosario, Jr. Rufa Mae Quinto
| screenplay     = 
| story          = 
| based on       = 
| starring       = Rufa Mae Quinto Antonio Aquitania Gina Pareno John Lapus Sunshine Dizon
| studio         = Viva Films
| distributor    = Viva Films Media Asia Films
| released       =  
| runtime        = 
| country        =  
| language       = Tagalog, Filipino
| music          = Chuckie Dreyfus
| cinematography = 	
| editing        = 
| gross          = 
| budget         = 
}} Filipino comedy film, directed by well-known romance/comedy director Joyce E. Bernal.    The film was released to Philippine theaters on April 14, 2004. It stars Rufa Mae Quinto as Boobita Rose, a kind loving lady, who faced the struggles in her life which included problems with her family.   Quinto produced the film together with Viva Films.  The film was released on video under the title Masikip sa Dibdib: Ang Tunay na Buhay ni Boobita Rose.

==Plot==
With the weight of an alcoholic née suicidal sister Brigitte (Sunshine Dizon), a womanizing addict for a brother Bogs (John Lapus), a hypochondriac for a mother Lupe (Gina Pareno), and a socially challenged lovelife Mark (Rudy Hatfield) hanging on her shoulders, Boobita Rose (Rufa Mae Quinto) breaks down by breaking into a song. However, she proves to be a tough cookie to crumble and gamely deals with her tribulations through a roller coaster ride of laugh and tears. Regine Velasquez, Ogie Alcasid, Lani Misalucha, and other big-name stars grace the musical interludes and provide added comic relief. 

==Cast==
* Rufa Mae Quinto as Boobita Rose: A hardworking, physically gifted girl and breadwinner of a dysfunctional unappreciative family.   
* Antonio Aquitania as Randy
* Gina Pareno as Lupe: Boobitas mother who is a hypochondriac and self-pitying mess. Pareno is known for easily switching from drama to comedy when the situation calls for it. 
* John Lapus as Bogs: Boobitas drug-addicted womanizer brother who gets girls pregnant. He has love scenes with no less than three Viva Hot Babes, namely Andrea del Rosario, Ella V. and Gwen Garci. Lapuz is a confirmed gay, but he said he got “turned on” with Ella V. 
* Sunshine Dizon as Brigitte: Boobitas alcoholic sister, a role Dizon said she never thought she’d play. She stated, "  So opposite of the real me that’s why it’s very challenging.” 
* Phytos Kyriacou as Boogie
* Tita Swarding as Lolo Benito: the family’s drunkard grandfather, a ‘straight’ role, contrary to Swardings real gay self. 
* Rudy Hatfield as Mark
* Jannica Pareno as Marks wife
* Kier Legaspi as Lupes boyfriend
* Bernard Bonnin as Bogart: Lupes husband who comes back to her only when he runs out of money. Otherwise, he’s with different women. Bonnin expressed “First time I’m doing this kind of comedy. I like it.” 
* Charlie Davao as Senator
* Chinggoy Alonso as Boss
* Raquel Montessa as Boss wife
* Ralion Alonso as Boss son
* Earl Ignacio as Teacher
* Lui Manansala as Principal

===Singers===
* Bituin Escalante (performed "Lupa")
* Blakdyak (performed "Dont Do That, Joey")
* Salbakuta (performed "Long Distance")
* Ogie Alcasid (performed "Huwag Ka Lang Mawawala")
* Lani Misalucha (performed "Tila")
* Regine Velasquez (performed "Saan Ako Nagkamali")
* Mark Bautista (performed "Kapag Akoy Kailangan")
* Sarah Geronimo (performed "Ibulong sa Hangin")
* Karylle (performed "Breaking My Heart")
* Kyla (performed "Maghihintay Ka Lamang")
* Anna Fegi (performed "Wala Ka Na") Nina (performed "What If")
* Ely Buendia (performed "Keeper")
* Martin Nievera (performed "You Are My Song")

===Cameo appearances===
* Andrea del Rosario
* Bobby Andrews
* Kristine Jaca
* Gwen Garci

==Soundtrack==
Songs were released by VIVA Records. 
# "Masikip sa Dibdib" by Rufa Mae Quinto
# "Perfecto" by Rufa Mae Quinto
# "Bitin na Bitin" by Rufa Mae Quinto

==References==
 

==External links==
* 

 
 
 
 