Inquilaab (2002 film)
{{Infobox film
| name           = Inquilaab
| image          = Inquilaabb.jpeg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Anup Sengupta
| producer       = S. Shubhayu
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       =  Prosenjit Chatterjee Arpita Pal Chiranjit Chakraborty Abhishek Chatterjee
| music          = S. Shubhayu
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2002
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film directed by Anup Sengupta. The film features actors Prosenjit Chatterjee and Arpita Pal in the lead roles. Music of the film has been composed by S. Shubhayu.  

== Cast ==
* Prosenjit Chatterjee
* Arpita Pal
* Chiranjit Chakraborty
* Abhishek Chatterjee
* Chinmoy Ray

== References ==
 

 
 
 


 