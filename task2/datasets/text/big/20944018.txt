Cinema 16: American Short Films
{{Infobox film
| name           = Cinema 16: American Short Films
| image          = 
| caption        =
| director       = Various
| producer       = Luke Morris   Ben Lederman (co-producer)
| editor         = 
| starring       =
| music          =
| cinematography = 
| studio         = 
| distributor    = Momac Films/ Warp Films
| released       = 
| runtime        = 209 minutes
| country        = United States
| language       = English
| budget         =
}}

  from directors such as Tim Burton, Alexander Payne, Maya Deren and George Lucas, as well as less well known names. It is the fourth in a series of DVDs released by Cinema16. 

==Contents==
The short films included are  , Carmen, Feelings, Paperboys, and Screen Test: Helmut. 

==Reception== cineaste if this DVD isnt gracing your shelves." 

An Eye for Film review says, "The standard of work is very high and this collection is a must for film buffs everywhere." 

A Digitally Obsessed review says, "A necessarily varied lot, and a good overview of American filmmaking on a small scale. The commentary tracks are particularly astute, as well." 

==References==
 

==External links==
* 
* 

 
 

 