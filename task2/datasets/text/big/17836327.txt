Tarzan's Fight for Life
{{Infobox film 
  | name = Tarzans Fight for Life
  | image = Tarzans Fight for Life poster.jpg
  | caption = Theatrical release poster
  | director = H. Bruce Humberstone		
  | producer = Sol Lesser
  | writer = Thomas Hal Phillips
  | based on =  
  | starring = Gordon Scott Eve Brent Rickie Sorensen Jil Jarmyn Cheeta Ernest Gold William E. Snyder
  | editing = Aaron Stell	 MGM
  | released =  
  | runtime = 86 min.
  | country = United States
  | language = English
  | budget = gross = $2,045,000  . 

}}
Tarzans Fight for Life (1958) is an action adventure film featuring Edgar Rice Burroughs famous jungle hero Tarzan   and starring Gordon Scott, Eve Brent, Rickie Sorensen, Jil Jarmyn, and Cheeta the chimpanzee. The movie was directed by H. Bruce Humberstone. The picture was the second Tarzan movie released in color, and the last to portray the ape man speaking broken English until Tarzan, the Ape Man (1981 film)|Tarzan, the Ape Man (1981). The filming locations were in Africa and Hollywood, California.

==Plot== James Edwards) of the Nagasu tribe, who regards their work as a threat to his own livelihood. Futa incites the tribe to waylay Annes fiance Dr. Ken Warwick (Harry Lauter), who is saved by Tarzan (Gordon Scott). Later Tarzan and his adopted son Tartu (Rickie Sorensen) enlist the doctors services on behalf of Jane (Eve Brent), suffering from appendicitis. Futa hypnotizes Moto (Nick Stewart), a native assistant of Sturdy, to murder Jane, but Tarzan thwarts the plot. Learning that the young Nagasu chief (Roy Glenn) is sick, Tarzan attempts to persuade them to let Sturdy treat them. Seizing his chance, Futa has the ape man taken captive and condemned to death. To restore his own credentials, the witch doctor then undertakes to cure the chief himself, hedging his bets by having his henchman Ramo (Woody Strode) steal medicine from Sturdy. Unfortunately, Ramo purloins a poison by mistake. Freeing himself, Tarzan intervenes and prevents the administration of the poison to the chief; Futa then swallows it himself to demonstrate that there is no harm in it — and dies. Dr. Sturdy is consequently called in, successfully curing the chief.
==Box Office==
According to MGM records the film made $720,000 in the US and Canada and $1,325,000 elsewhere, resulting in a profit of $348,000. 
==Legacy==
The film was released to coincide with the 40th anniversary of the first Tarzan book.  It was the last Tarzan film made by Sol Lesser who retired and handed over the franchise to Sy Weintraub.

Shortly after completing this film, Scott, Brent, and Sorensen would play the same roles in an attempt to launch a "Tarzan" television series. However, the extremely low-budget project failed to sell, and the three half-hour episodes were spliced into an ersatz feature, Tarzan and the Trappers, released to television in 1966.

==Notes==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 