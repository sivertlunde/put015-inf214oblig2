Veer Kunal
{{Infobox film
| name           = Veer Kunal 
| image          = Veer_Kunal_1945.jpg
| image_size     = 
| caption        = Kishore Sahu and Shobhana Samarth in Veer Kunal
| director       = Kishore Sahu
| producer       = Kishore Sahu
| writer         = 
| narrator       = 
| starring       = Kishore Sahu Shobhna Samarth Durga Khote Mubarak
| music          = Khan Mastana
| cinematography = Chandu
| editing        = Kantilal B Shukla
| distributor    =
| studio         = Ramnik Productions  
| released       = 1945
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1945 Hindi historical fiction film directed by Kishore Sahu.    Besides directing the film Sahu also produced it and wrote the story and screenplay. The cimematographer was Chandu. Produced under the Ramnik Productions banner, it had music by Khan Mastana.    The starcast consisted of Kishore Sahu, Shobhna Samarth, Durga Khote, Mubarak and Maya Banerji.   

The film based on popular historical folk myth, focuses on Emperor Ashokas son, Kunala|Kunal. Ashoka has brought in a third wife, who through her evil planning has Kunal blinded and thrown in the dungeons. The story then follows episodes in Kunals life till he gets Ashokas impartial justice. 

==Plot== durbar meeting. When he hears the entire story, his sense of fair play, his impartial justice, and remorse at his sons blinding help him mete out a strict punishment to Tishya, condemning her to be burnt.

==Cast==
* Kishore Sahu
* Shobhna Samarth as Tishyarakshita
* Durga Khote as Kuruvaki
* Mubarak as Ashok
* Maya Banerji
* Nila Nagini as Kanchanmala, wife of Kunal
* Vasantrao Pahelwan
* Moni Chatterjee
* Kanta Kumari

==Reception==
Kishore Sahu "made news" by getting Sardar Vallabhbhai Patel to inaugurate the film premiere at Novelty cinema in Bombay on 1 December 1945, according to the magazine Filmindia.    Some of the comments from other newspapers of the time were quoted in Filmindia in an advertisement praising the film. The Bombay Chronicle praised its sets and costumes. The Sunday Standard called it the "greatest picture". The Free Press Journal explained its popularity due to "high level of performances" and Sahus direction. The Times of India, while acclaiming Sahus direction also gave credit to the acting, sets, photography, and a tight "gripping" story.   

Filmindia commended Sahu for casting Shobhana Samarth as the "vamp", which was in contrast to majority of her roles where she portrayed Sita, Damayanti and Taramati in several films. Her acting was appreciated and was referred to as her "best performance". Sahu, Samarth and Durga Khotes performances were much-admired. Mubarak as Ashok was criticised for looking "stupid and unconvincing", while Nila Nagini was cited as having an "ungainly appearance". The symbolic use of eyes which attract Tishya, and her blinding Kunal, were highly praised by Patel, finding the thematic approach far superior.The film was criticised for being overly serious and "bordering on morbidity". The production values were stated to be inconsistent.    

The "expensive picture" was cited as being "more for the intellectuals than for the masses",    however, it was claimed to be a success at the box-office.   

==Kunal In Films==
* In Hindi, Veer Kunal (1945) was the third film on the subject of Ashokas son, Kunal. The earlier two were Veer Kunal (1925), a silent film directed by Manilal Joshi, and Veer Kunal (1932), a Talkie directed by M. D. Bhavnani and G. S. Devare.      
 Ashok Kumar (1941) directed by the "noted" film maker Raja Chandrasekhar. It starred the famous M. K. Thyagaraja Bhagavathar as Kunal and the film is remembered for Bhagavathars "melodious songs". It was a commercial success.   

==Soundtrack==
The film had only four songs, and all of them according to Baburao Patel, were in keeping with the mood and theme of the film, and not frivolous.    The film had music by Khan Mastana. The three lyricists were Ambikesh Kuntal, Neelkanth Tiwari and Pandit Madhur.     

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Lyricist
|- 
| 1
| Chalo Milkar Chale
| Ambikesh Kuntal
|-
| 2
| Ghir Aaye Re Patang
| Neelkanth Tiwari
|-
| 3
| Mere Nainon Ke Taare
| Neelkanth Tiwari
|-
| 4
| Veena Aisa Raag Suna De
| Pandit Madhur
|}

==References==
 

==External links==
* 

 

 
 
 
 