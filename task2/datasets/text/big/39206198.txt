The Drop (film)
{{Infobox film
| name           = The Drop
| image          = The Drop Poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Michaël R. Roskam
| producer       = Peter Chernin Dylan Clark Mike Larocca
| writer         = Dennis Lehane
| based on       =  
| starring       = Tom Hardy Noomi Rapace James Gandolfini Matthias Schoenaerts
| music          = Marco Beltrami 
| cinematography = Nicolas Karakatsanis
| editing        = Christopher Tellefsen
| studio         = Chernin Entertainment
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 106 minutes 
| country        = United States
| language       = English
| budget         = $12.6 million 
| gross          = $18.7 million   
}}
 crime drama same name. The film stars Tom Hardy, Noomi Rapace and James Gandolfini, with Matthias Schoenaerts, John Ortiz and James Frecheville in supporting roles. It was screened in the Special Presentations section of the 2014 Toronto International Film Festival, and was released theatrically on September 12, 2014.   

The films 2014 release marked the late Gandolfinis final appearance in a feature film. 

==Plot==
 
Bob Saginowski (Tom Hardy) is a bartender at a neighbourhood bar in Brooklyn called "Cousin Marvs". Marv (James Gandolfini) ceded ownership of the bar years earlier to Chechen mobsters and now operates it with Bob as a drop for illegal takings. Marv berates Bob and some patrons for commemorating the 10th anniversary of the murder of a man named Richie Whelan. Later at night, Nadia (Noomi Rapace), discovers Bob rescuing a battered pit bull pup abandoned in her garbage can. She assists Bob in caring for the dog. Bob leaves the dog in her care until he can decide whether to adopt him.

On another night, the bar is robbed by two masked gunmen. During the robbery, Bob notices that one of the gunmen is wearing a broken watch. Detective Torres (John Ortiz), who attends the same church frequented by Bob, shows up to investigate the aftermath. Marv is infuriated at Bob for informing Torres about the watch, providing the police an early lead on the culprits. The next day, Chovka, the face of the Chechen mob, shows up to question Marv and Bob about the robbery, asking the two of them whether or not they know one of the robbers impaled in the back of a van. He demands to know why the cops know about the broken watch, and warns Marv he must retrieve the money. It is later revealed that Marv orchestrated the robbery by meeting with one of the perpetrators.

Bob eventually agrees to adopt the dog and names him Rocco, all the while bonding with Nadia, who agrees to care for him whenever Bob tends to the bar. While walking Rocco in a park, a passer-by, Eric Deeds (Matthias Schoenaerts) comments to Bob that Rocco is a nice looking dog.

Meanwhile, Marvs sister Dottie (Ann Dowd) informs Marv that a collection agency has been calling to settle their fathers life support bills. She suggests that they should take him off life support, to which Marv disagrees. On his way into the bar, Marv begins to suspect he is being followed after providing directions to a stranger. Deeds appears at Bobs house and provides proof at the admission that he is the actual owner of Rocco and was responsible for beating and abandoning him. After demanding Rocco is returned, Deeds takes Bobs umbrella and leaves.

Later at the back of the bar, Bob and Marv discover a bag containing a severed arm with a broken watch together with the money. Bob disposes of the arm with an aloofness noticed by Marv. Shortly after, Bob is unnerved when Deeds appears in the bar and reveals he knows Nadia. Marv informs Bob that Deeds is a local thug who claims to have killed Richie Whelan. Bob confronts Nadia and asks her how well she knows Deeds, though Nadia is offended by the question and leaves.

Bob and Marv return the money to the Chechens, who in turn inform them that the bar will be the drop bar for the night of the Super Bowl.

Bob returns home to find Nadia waiting. Upon explaining that she and Deeds used to date, Bob accepts that Nadia was hiding the reason for her scars on her neck and they reconcile.  Marv meets the other robber and, while driving, informs him of a plan to rob the bar again during the night of the Super Bowl. However, disturbed by the death of his partner, the robber refuses to take part and exits the car. Marv runs him over and kills him.

After walking Nadia to her home at night, Bob returns to his own home and discovers his umbrella, implying that Deeds was present. Marv later meets with Deeds to recruit him to rob the bar after the Super Bowl. Bob later confronts Deeds, the latter threatens to tell the police that Bob stole and beat Rocco unless Bob pays him $10,000. He warns Bob that Rocco will likely be killed if he is returned.

Later, Marv informs Bob that he will be calling off sick on the day of the Super Bowl. Bob is suspicious Marv would miss out on the large number tips gained during a Super Bowl night, and asks him if he is planning on doing something that they cannot clean up this time. On the Super Bowl night and with Rocco in tow, Bob takes $10,000 hidden in his basement and hides it behind the bar counter together with a pistol. Elsewhere, Deeds breaks into Nadias home and forces her to go to the bar with him. Marv warns Deeds that Bob is not what he seems. Marv lines his trunk with plastic and heads to the bar, parking and waiting nearby. Various mobsters drop off money throughout the night. Eventually the bar clears except for Deeds and Nadia. Nadia warns Bob that Deeds is armed. Deeds refuses Bobs offer of $10,000 as payment for adopting Rocco and threatens to kill Nadia if he does not open the safe during a 90 second window at 2AM. While they wait, Bob narrates the story of Richie Whelan. Marv used to be a loan shark and Richie owed him a significant amount of money, but he won a casino jackpot and was able to pay Marv back. In order to cover Marvs own debts and prevent anyone knowing he was paid back, Marv had Bob murder Richie and dispose of the body, not Deeds. Bob then fatally shoots Deeds and tells a frightened Nadia that it is safe to leave.

Later in the night, Bob is sitting at the bar with Rocco while the Chechens dispose of Deeds body and collect the money from the drop. Chovka informs Bob he was aware of Marvs deceit and negotiated a deal with him that he would buy him a ticket to a safe place. Meanwhile, the stranger Marv suspected of following him earlier executes Marv in his car at point blank range. Chovka accepts Bob played no part in the attempted robbery and provides him ownership of the bar as a reward.

On another day, Detective Torres visits the bar to question Bob about the disappearance of Deeds. Torres tells Bob that Deeds was in a psychiatric ward at the time of Richie Whelans death. Torres implies that he knows Bob is responsible for the disappearance of Deeds, as well as Whelan, saying, "They never see you coming, do they, Bob?".   Later, Bob visits Nadias home with Rocco and tells her that he knows she may not want to see him again, but, if so, he needs her to tell him so he knows that is, in fact, what she wants. However, Nadia offers to take a walk with him and Rocco.

==Cast==
 
* Tom Hardy as Bob Saginowski
* Noomi Rapace as Nadia
* James Gandolfini as Cousin Marv
* Matthias Schoenaerts as Eric Deeds
* John Ortiz as Detective Torres
* Elizabeth Rodriguez as Detective Romsey
* Michael Aronov as Chovka
* Morgan Spector as Andre
* Michael Esper as Rardy
* Ross Bickell as Father Regan
* James Frecheville as Fitz
* Tobias Segal as Bri
* Patricia Squire as Millie
* Ann Dowd as Dottie
* Chris Sullivan as Jimmy
 

==Reception==
===Box office===
The Drop opened in 809 theaters in North America and grossed $4,104,552, with an average of $5,074 per theater and ranking #6 at the box office. The films widest release in the United States was 1,192 theaters and it ultimately earned $10,724,389 domestically and $7,933,992 internationally for a total of $18,658,381, above its budget of $12.6 million. 

===Critical response===
The Drop received positive reviews from critics and has a "certified fresh" score of 89% on Rotten Tomatoes based on 178 reviews with an average score of 7.1 out of 10. The critical consensus states "Theres no shortage of similarly themed crime dramas, but The Drop rises above the pack with a smartly written script and strong cast."  The film also has a score of 69 out of 100 on Metacritic based on 36 critics indicating "generally favorable reviews".  Particular praise was given to the performances of Tom Hardy and James Gandolfini.  

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 