La très très grande entreprise
{{Infobox film
| name     = La très très grande entreprise |
| caption =  |
| starring       = Roschdy Zem Marie Gillain Jean-Paul Rouve Adrien Jolivet Guilaine Londez|    
| director       = Pierre Jolivet |
| producer       = Charles Gassot and Jacques Hinstin |
| editing       = Yves Deschamps and Charlotte Theillard |
| released   =   |
| runtime    = 102 minutes |
| country    = France |
| language   = French |
| music      = Manu Katché |
}}
La très très grande entreprise (English title: The Very Very Big Company) is a French comedy film directed by Pierre Jolivet and released in France in November 2008. It is an indirect follow-up to his film Ma petite entreprise, the story of which is set nine years before that of La très très grande entreprise.

==Synopsis==
Four friends - an oyster-farmer, an accountant’s assistant, a restaurateur and a blue-collar worker - decide to take on a multi-national agro-chemical company with a turnover of €9bn and which has been polluting their region on a large scale. Having been awarded a paltry amount of compensation, they decide to seek true justice. They have however just 30 days to uncover vital new evidence ...

==Cast==
* Roschdy Zem : Zak
* Marie Gillain : Mélanie
* Jean-Paul Rouve : Denis
* Adrien Jolivet : Kevin
* Guilaine Londez : Brigitte Lamarcq
* Arlette Thomas : Mme de Marthod
* Wilfried Romoli : Romolli
* Vikash Dhorasoo : Sanjay
* Nicolas Marié : Maître Dessax
* Anne Loiret : Sophie Dantec
* Eric Prat : Boisselier
* Cyril Couton : Philippe Malzieux
* Scali Delpeyrat : Boissy DAnglas
* Ludovic Bergery : Philippe
* Serge Larivière : M. Andretti

==External links==
*  

 
 
 
 

 