Kaneez (1949 film)
{{Infobox film name = Kaneez image = Kaneez (1949 film).jpg image size =  border = yes alt = DVD cover caption = DVD cover director = Krishan Kumar producer = Krishan Kumar  writer =  screenplay = Hasrat Lucknavi story = Hasrat Lucknavi based on =  narrator =  starring = Shyam Kuldeep Kaur Ramesh Sinha Khwaja Sabir Nazir Kashmiri Urmila Devi Jillo Bai Tiwari music = Ghulam Haider Hansraj Behl O.P. Nayyar (Film score) cinematography =  editing = studio =  distributor = released =   runtime =  country =  language = Urdu/Hindi budget =  gross =
}}
 Hindi film directed and produced by Krishan Kumar,       starring Shyam, Munawar Sultana and Kuldeep Kaur in lead roles.

== Synopsis ==

Sabira is the daughter of a wealthy, Akbar, who is cheated by his manager, Hamid, and forced into a mental home. Sabira marries Hamids son, Akhtar but the marriage is destroyed by a woman called, Darling, who is after Akhtar for his money. Sabira is forced to become a servant in her own house but she recovers her place as the mistress as Darling is exposed and Akhtar realized her worth for him.

== Music ==
 Ghulam Haider and Hansraj Behl  with a large playback of Shamshad Begum, Mohammad Rafi, Surinder Kaur,     Geeta Dutt, Zeenat Begum, G.M. Durrani,    S.D. Batish and Rajkumari. The lyrics written by Hasrat Lucknavi, Sarshar Sailani, Shahir Ghaznavi and Harishchandra Akhtar. O.P. Nayyar composed the background music.
   

== Cast ==

{|class="wikitable"
|-
!Actor/Actress
!Role
|- Munawar Sultana|| Sabira (Akbars daughter)
|- Shyam (actor)|Shyam|| Akhtar (Hamids son)
|- Khwaja Sabir|| Hamid (Akbars manager)
|- Ramesh Sinha|| Akbar
|- Kuldeep Kaur|| Darling
|}

== See also ==
*Shama (1946 film)

== References ==
 

== External links ==
 

 
 
 
 
 


 