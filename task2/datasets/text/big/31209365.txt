Lava (1980 film)
{{Infobox film
| name           = Lava
| image          =
| caption        = Hariharan
| producer       = GP Balan
| writer         = S. L. Puram Sadanandan (dialogues) Hariharan
| starring       = Prem Nazir Jagathy Sreekumar Prameela Sathar
| music          = G. Devarajan
| cinematography = Melli Irani
| editing        = VP Krishnan
| studio         = GP Films
| distributor    = GP Films
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, Hariharan and produced by GP Balan. The film stars Prem Nazir, Jagathy Sreekumar, Prameela and Sathar in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir as Ramu
*Jagathy Sreekumar as Kuttappan
*Prameela as Janaki
*Sathaar as Gopi
*Bahadoor as Govindan
*Balan K Nair as Velayudhan GK Pillai as Police Officer
*Jayamalini as Dancer
*K. P. Ummer as Rajasekharan
*Krishna Kurup
*Kunjandi as Kumaran Madhavi as Seetha
*Nellikode Bhaskaran as Gopalan
*Oduvil Unnikrishnan as Panikkar  Sumithra as Sindhu
*Varalakshmi as Rajasekharans wife
*Santo Krishnan
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aashaalathayile || P Jayachandran, Chorus || Yusufali Kechery || 
|-
| 2 || Chirakulla Mohangale || P. Madhuri || Yusufali Kechery || 
|-
| 3 || Ee thaarunya || K. J. Yesudas, P Jayachandran || Yusufali Kechery || 
|-
| 4 || Maarante Kovilil || K. J. Yesudas || Yusufali Kechery || 
|-
| 5 || Vijayappoomaala || P. Madhuri, Chorus, CN Unnikrishnan || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 


 