Night Train to Terror
{{Infobox film
| name           = Night Train to Terror
| image          = Night train to terror poster 01.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatricla release poster
| film name      = 
| director       = John Carr Phillip Marshak Tom McGowan Jay Schlossberg-Cohen Gregg C. Tallas
| producer       = William F. Messerli Gene Ruggiero Jay Schlossberg-Cohen
| writer         = Philip Yordan
| screenplay     = 
| story          = 
| based on       = 
| narrator       =  Cameron Mitchell Richard Moll Marc Lawrence
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Visto International Inc.
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Night Train to Terror is an independent horror film directed by John Carr, Phillip Marshak, Tom McGowan, Jay Schlossberg-Cohen, Gregg C. Tallas, and written by Academy Award winner Phillip Yordan that was released in 1985 and has since become an infamous cult classic of grade-Z movie fare. 

==Plot==
God and Satan are on board a train and discuss the fate of three individuals. In the first story, "The Case of Harry Billings", a man is kidnapped and taken to an insane asylum where he is put under hypnosis and lures victims to be tortured and murdered as part of an organ-harvesting operation. The second story, "The Case of Gretta Connors", entails two young lovers who become involved in a sinister cult of people fascinated with death. The final story "The Case of Claire Hansen" involves an apprentice to the Devil who is out to destroy mankind and a group of immortals who are out to stop him.

==Production==
Night Train to Terror is actually pieced together from three other films:

:Cataclysm (1980)
:Death Wish Club (1983)
:Scream Your Head Off (unfinished)

Footage from this film was also later edited into Marilyn Alive and Behind Bars (1992).

In the end credits, Satan is credited as being played by "Lu Sifer" and God by "Himself".

==Release==
The film was given a limited release theatrically by Visto International Inc. in 1985.   The film received poor reviews and less than spectacular box office attraction. 

The film was given its first official Bluray and DVD release in October 2014 by Vinegar Syndrome.  Extras for the film include the full-length version of Greta (aka Death Wish Club), an interview with producer/director Jay Schlossberg-Cohen, an interview with assistant editor Wayne Schmidt, and a commentary track by The Hysteria Continues. 

The film is often compared to the likes of Plan 9 From Outer Space due to its bad dialogue, poor editing, and numerous continuity errors.

==References==
 

== External links ==
*  

 
 
 
 
 
 
 