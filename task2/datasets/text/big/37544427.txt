Hang the DJ (film)
{{Infobox film
| name           = Hang the DJ
| image          =
| caption        =
| director       = Marco La Villa Mauro La Villa
| producer       = Marco La Villa Mauro La Villa Henrique Vera Villanueva  executive producer: Andrea Kragaris  Rafael Rodriguez   associate producer: Vincenzo Scanzano, Carlos Orengo
| starring       = Roger Sanchez Junior Vasquez DJ Qbert Mix Master Mike John "Jellybean" Benitez Carl Cox Claudio Coccoluto Kool DJ Red Alert
| cinematography = Stephen Reizes Steve Beasse Bill St. John
| distributor    = Pony Canyon
| released       =  
| runtime        = 90 minutes English
}}

Hang The DJ is a 1998 music documentary film debut by twin brothers Marco and Mauro La Villa. Featuring Roger Sanchez, Junior Vasquez and DJ Qbert the film presents the cult of DJs in the era that catapulted them into superstars from the inside. Performances from around the globe are intercut with commentary from fans and interviews with the DJs themselves.

The name of the film comes from lyrics in Panic (The Smiths song)|"Panic" the 1986 song from The Smiths. 

==Production==
The film was shot over a period of two years 1996 and 1997 all over the world. Shooting locations: New York, San Francisco, Los Angeles, Las Vegas, Miami, Washington D.C., London, Montreal, Nottingham, Essen(Germany), Amsterdam, Madrid, Cannes, Paris, Liverpool and Amsterdam. 

==Release==
Hang the DJ film premiered at the 1998 Toronto International Film Festival in September 1998   and also screened at the International Documentary Film Festival Amsterdam,  the Stockholm International Film Festival,  the Melbourne International Film Festival   and the New Zealand International Film Festival   previous to its theatrical release. 

==External links==
*  

==Notes==
 

 
 
 


 