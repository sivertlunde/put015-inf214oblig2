Fighting Caballero
{{Infobox film
| name           = Fighting Caballero
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Elmer Clifton
| producer       = George M. Merrick
| writer         = {{Plainlist|
* Elmer Clifton
* George M. Merrick
}}
| screenplay     = 
| story          = 
| based on       =  
| starring       = {{Plainlist|
* Rex Lease
* Dorothy Gulliver
}}
| narrator       = 
| music          = 
| cinematography = Bert Longenecker
| editing        = Carl Himm
| studio         = Weiss Productions
| distributor    = Superior Talking Pictures
| released       =   
| runtime        = 63 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
}} Western film Mexican and investigates misdeeds at a silver mine. The films title is derived from "Caballero (disambiguation)|Caballero", the Spanish word for  "knight" or "gentleman".

==Cast==
*Rex Lease as Joaquin
*Dorothy Gulliver as Pat
*Earl Douglas as Pedro
*George Chesebro as Devil Jackson
*Robert Walker as Bull
*Wally Wales as Wildcat
*Milburn Moranti as Alkali Potts
*George Morelle as Si Jenkins
*Pinkey Barnes as Beetle
*Carl Mathews as Jose
*Barney Furey as Sheriff
*Franklyn Farnum as Bartender

== External links ==
*  

 
 
 
 

 