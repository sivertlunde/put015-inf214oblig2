School of the Holy Beast
{{Infobox film
| name           = School of the Holy Beast
| image          = School_of_the_Holy_Beast.jpg
| caption        = Poster to School of the Holy Beast
| director       = Norifumi Suzuki
| producer       = 
| eproducer      =
| aproducer      =
| writer         = Masahiro Kakefuda Norifumi Suzuki
| starring       = Yumi Takigawa Fumio Watanabe Emiko Yamauchi
| music          = Masao Yagi
| cinematography = 
| editing        =  Toei
| released       = February 16, 1974 (Japan)
| runtime        = 
| rating         =
| country        = Japan
| awards         =
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Pinky violence made by Toei Company in 1974 in film|1974.

==Plot==
A young woman (Yumi Takigawa) becomes a nun at the Sacred Heart Convent to find out what happened to her mother years earlier. She encounters a lesbian mother superior, lecherous archbishops, and uncovers many dark secrets. The convent also practices brutal discipline and encourages masochistic rituals such as self-flagellation. In one scene, two nuns are forced to strip to the waist and whip each other severely with heavy floggers. Later, Takigawa is tortured and whipped by a group of nuns armed with rose-thorns.

==Cast==
* Yumi Takigawa as Maya Takigawa
* Emiko Yamauchi as Matsuko Ishida
* Yayoi Watanabe as Hisako Kitano
* Yōko Mihara as Sadako Matsumara
* Fumio Watanabe as Priest Kakinuma

==Critical appraisal==
Praising the work of writer/director Norifumi Suzuki as well as the leading actors, critic Donald Guarisco of Allmovie says, "This Japanese shocker manages to   shocking and artistically stunning all at once." {{cite web |first=Donald|last=Guarisco|url=http://wm04.allmovie.com/cg/avg.dll?p=avg&sql=1:322647~T1|title=School of the Holy Beast : Review
|accessdate=2007-10-10|publisher=Allmovie}} 

In TokyoScope: The Japanese Cult Film Companion, Patrick Macias calls the film a "comic adaptation and a blasphemous sermon of high camp and knowing literary intelligence." He continues, "Trashy as it may sound, Suzukis film is absolutely gorgeous to gaze upon." 

==Availability==
The Cult Epics company released School of the Holy Beast on DVD region code|region-1 DVD on August 30, 2005. The extras on the DVD included the original theatrical trailer, and interviews with lead actress Yumi Takigawa and film critic Risaku Kiridoushi. 

==Notes==
 

==Sources==
*  
*  
* {{cite web |first=Donald|last=Guarisco|url=http://wm04.allmovie.com/cg/avg.dll?p=avg&sql=1:322647~T1|title=School of the Holy Beast : Review
|accessdate=2007-10-10|publisher=Allmovie}}
*  
*  
*  
*  
*  
*  

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 


 
 