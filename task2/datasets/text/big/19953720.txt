The Lions Are Loose
{{Infobox film
| name           = Les Lions sont lâchés
| image          = Les Lions sont laches poster.jpg
| image_size     = 
| caption        = 
| director       = Henri Verneuil
| producer       = 
| writer         = France Roche  Michel Audiard
| narrator       = 
| starring       = Jean-Claude Brialy  Claudia Cardinale   Danielle Darrieux   Michèle Morgan
| music          = Georges Garvarentz       Christian Matras 
| editing        = Borys Lewin    Gaumont (SNEG) Vides Cinematografica
| distributor    = Société des Etablissements L. Gaumont
| released       = 20 September 1961
| runtime        = 95 minutes
| country        = Italy / France
| language       = French
| budget         = 
| gross          = 
| website        = 
}} Christian Matras.

It tells the story of three women living in Paris.

==Principal cast==
*Jean-Claude Brialy as  Didier Marèze 
*Claudia Cardinale as  Albertine Ferran 
*Danielle Darrieux as  Marie-Laure Robert-Guichard 
*Michèle Morgan as  Cécile 
*Lino Ventura as  Le docteur André Challenberg 
*Denise Provence as  Hélène Challenberg 
*Daniel Ceccaldi as Georges Guichard 
*Martine Messager as  Florence Guichard
*Darry Cowl  as  Richard
*Jean Ozenne as Alfred Robert-Guichard
*Francis Nani as Arnaud Guichard
*François Nocher as Gilles
*Louis Arbessier as Frédéric Moine
*Bernard Musson as Gabriel, Butler
*Charles Aznavour as Himself

==External links==
*  at Alice Cinema
* 
* 

 

 
 
 
 
 


 