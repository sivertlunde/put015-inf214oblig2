Tony (2009 film)
 
{{Infobox film
| name           = Tony
| image          = TONYposter.jpg
| caption        = Theatrical release poster
| director       = Gerard Johnson
| writer         = Gerard Johnson
| producer       = Dan McCulloch
| starring       = Peter Ferdinando Lorenzo Camporese Ricky Grover Neil Maskell
| runtime        = 76 minutes  Matt Johnson
| cinematography = David Higgs
| studio         = AbbottVision
| distributor    = Revolver Entertainment
| released       =   
| country        = United Kingdom
| language       = English
| budget         =
}} social realist drama written and directed by Gerard Johnson and starring Peter Ferdinando.  The film has the alternative title of Tony: London Serial Killer.

==Plot==
Tony Benson lives alone in a flat on an Dalston council estate tower block. He obsessively collects 1980s violent action movies, and he aimlessly wanders the streets of London in an unsuccessful attempt to connect with another person. He has been unemployed and living on state benefits for over 20 years.

He is soon revealed to be a serial killer, killing men he encounters by chance, including two drug addicts whom he uncomfortably tries to engage with after inviting them to his home to smoke heroin. The encounter ends with Tony suffocating one of them with a plastic bag while locking the other in a cupboard before letting him go, believing the terrified addict wouldnt go to the police.

The rare visitors Tony does get usually complain of the rotting smell from his house, which he blames on clogged drains. Others he meets at gay bars before bringing them back to his flat and usually end with him rejecting their advances and killing them, similar in fashion to Jeffrey Dahmer. He also gets dismissed after visiting a prostitute with only £5, asking the price of a cuddle and failing in his attempt to strike up a conversation with her.

Tony disposes of the bodies of his victims by butchering their torsos and human organs in his kitchen sink before bagging them and dumping them in the River Thames and canals in London. He shares a bed with a rotting corpse, obviously one of his victims, and drags bodies which are stashed around the house out to put on the couch to sit and watch TV with him.
 paedophile and wrongly blames him for his sons disappearance. Tony becomes suspect number one because of his unusual appearance and his withdrawn nature towards other people. A police detective visits to question him on the missing boys whereabouts, putting Tony at risk of being discovered.

However the boy is soon discovered and Tony is let off the hook. The film ends with Tony casually walking around London.

==Critical response==
The film received mostly positive reviews, especially for Gerard Johnsons direction and Peter Ferdinandos portrayal of Tony. It holds an aggregate 80% rating on Rotten Tomatoes based on 15 reviews. 

==External links==
* 
* 

== References==
{{reflist|refs=

   

   
}}

 
 
 
 
 
 
 
 
 
 
 