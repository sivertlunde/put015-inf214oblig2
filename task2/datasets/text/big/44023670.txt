Thacholi Marumakan Chandu
{{Infobox film 
| name           = Thacholi Marumakan Chandu
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       = P Bhaskaran
| writer         = N. Govindankutty
| screenplay     = N. Govindankutty
| starring       = Prem Nazir Sukumari Jayabharathi Srividya
| music          = V. Dakshinamoorthy
| cinematography = U Rajagopal
| editing        = K Sankunni
| studio         = Suchithramanjari
| distributor    = Suchithramanjari
| released       =  
| country        = India Malayalam
}}
 1974 Cinema Indian Malayalam Malayalam film, directed and produced by P. Bhaskaran . The film stars Prem Nazir, Sukumari, Jayabharathi and Srividya in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
*Prem Nazir as Thacholi Othenan Kurup, Chandu (double role)
*Sukumari as Kunki
*Jayabharathi as Thazhathumadom Maathu
*Srividya as Kanni
*Adoor Bhasi as Kandacheri Chappan
*Thikkurissi Sukumaran Nair
*Sreelatha Namboothiri as Kuttimani
*T. R. Omana as Maakkam
*Bahadoor as Pokkan
*K. P. Ummer as Kandarar Menon Meena as Eppennu
*Balan K Nair as Yenali
*Kaviyoor Ponnamma  as Unnichirutha
*Philomina as Nanga
*Sankaradi as Thazhathumadom Kannoth Moothavar
*T. S. Muthaiah as Guru
*SP Pilla as Vaittil Thampan
*Muthukulam Ragavan Pilla as Kanachira Kannappan
*N Govindankutty as Kunjikelu
*Santo Krishnan

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by P. Bhaskaran and . 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Illam Nira Vallam Nira || Chorus, Kalyani Menon || P. Bhaskaran || 
|-
| 2 || Induchoodan Bhagavaante || S Janaki || P. Bhaskaran || 
|-
| 3 || Kannalmizhi Kanimalare || K. J. Yesudas, S Janaki || P. Bhaskaran || 
|-
| 4 || Kudakumala Kunnimala || Ambili, ST Sasidharan || P. Bhaskaran || 
|-
| 5 || Onnaaman Kochuthumbi || Ambili, Chorus, Sreelatha Namboothiri || P. Bhaskaran || 
|-
| 6 || Pachamalakkiliye || Chorus, Sreelatha Namboothiri || P. Bhaskaran || 
|-
| 7 || Thacholi Omana Kunjichanthu  || P Jayachandran, Chorus ||  || 
|-
| 8 || Vadakkini Thalathile || S Janaki || P. Bhaskaran || 
|-
| 9 || Vrischikappoonilaave || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 