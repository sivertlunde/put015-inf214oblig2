The Fruit Machine
 Fruit machine}}
{{Infobox film
| name     = The Fruit Machine Wonderland (USA)
| image    = TheFruitMachine.jpg
| caption  = German DVD Cover for The Fruit Machine, a/k/a Wonderland (USA)
| director = Philip Saville
| producer = Steve Morrison
| writer   = Frank Clarke
| starring = Emile Charles Tony Forsyth Robert Stephens Robbie Coltrane Bruce Payne
| music    = Hans Zimmer
| cinematography = Dick Pope
| editing  = Richard Bedford
| distributor = Cannon Films Vestron Video Pro-Fun Media
| country  = United Kingdom
| released =  
| runtime  = 103 minutes
| language = English
| budget   =
}} The Gospel of John, Metroland (1997 film)|Metroland) about two friends, gay teens running from an underworld assassin and the police. This was a film made by UK TV company Granada Productions; now known as ITV Productions.

Starring Tony Forsyth, Emile Charles, Bruce Payne and Robbie Coltrane in the role of "Annabelle".
 cult following due primarily to its straightforward portrayal of British gay youth and the rising careers of Coltrane, Payne, Saville and Hans Zimmer.

The film has had two   DVD version has still not materialised.

==Overview==
A combination of adventure, buddy film, road movie and 1980s filmmaking, the film plays out a number of social issues of the time, as seen through the eyes of two British 16-year-olds.

One of the teenagers is played by Emile Charles (younger brother of Craig Charles). He spends a lot of time at the dolphinarium at the Wonderland theme park.

Symbolism notwithstanding, variations of Bruce Paynes character Echo and his machete appeared on both versions of the films promotional materials—even though Echo was a non-speaking role.
 The Da Vinci Code, The Lion King). No "official" soundtrack was ever released, but the 20-minute The Fruit Machine Suite appears on Hans Zimmers CD HANS ZIMMER: The British Years, a sampling of his earlier film work.

Divine (Glen Milstead)|Divines songs, popular around the world and on the charts in Britain at the time, were utilized in the disco dance sequence.

The film also contained the title song, sung by Paul Lekakis produced by Stock, Aitken & Waterman.

==Synopsis==

Eddie and Michael are best friends on the brink of adulthood. They are both gay, but hold diametrically opposed outlooks on life. Eddie likes watching old movies on video with his mum. Michael likes video games and the street. They are total opposites that argue like an old married couple. Leaving behind the grim, oppressive reality of Liverpool (in the 1980s unemployment rates in Liverpool were amongst the highest in the UK), they stumble into the bizarre fantasy world of a transvestite nightclub called The Fruit Machine, run by "Annabelle". There, they witness a brutal gangland murder by Echo that transforms their quest for adventure into a run for their lives. Alone and afraid, yet hopeful, they wind up in Brighton with Vincent and Eve at Wonderland, where their path is strewn with manipulation, deceit and murder.

==Featured cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- 
| Emile Charles || Eddie
|-
| Tony Forsyth || Michael
|- 
| Robert Stephens || Vincent
|- 
| Robbie Coltrane || "Annabelle"
|- Clare Higgins || Eve
|-
| Bruce Payne || Echo
|-
| Carsten Norgaard || Dolphin Man
|-
| Kim Christie || Jean
|-
| Louis Emerick || Billy
|-
| Julie Graham || Hazel
|-
| Forbes Collins || John Schlesinger
|}

==References==
*  
*  
*  
*  

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 