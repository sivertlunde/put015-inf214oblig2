Karmayogi (1978 film)
{{Infobox film
| name           = Karmayogi
| image          = Karmayogi.jpg
| image_size     =
| caption        = 
| director       = Ram Maheshwari
| producer       = 
| writer         = Binoy Chatterjee Ela Maheshwary
| narrator       = 
| starring       =Jeetendra Raaj Kumar Rekha Mala Sinha Reena Roy Nazir Hussain
| music          = Kalyanji Anandji
| cinematography = 
| editing        = M. S. Shinde
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Karmayogi is a 1978 Bollywood action film directed by Ram Maheshwari . Telugu as Nipputo Chelagaatam in 1982 with Krishnam Raju, Sharada (actress)|Sharada, Jayasudha and Sarath Babu.

==Cast==
*Raaj Kumar ... Shanker / Mohan 
*Jeetendra ... Ajay
*Mala Sinha ... Durga
*Rekha ... Rekha 
*Reena Roy ... Kiran 
*Ajit... Keshavlal 
*Sudhir Dalvi ... Judge
*Nazir Hussain... Catholic priest Dhumal ... Kakaram


==Plot==
 
The hero - Shankar (Raaj Kumar) was earlier betrayed and swindled by many people in his life, leading to poverty and hardships. He takes the path of crime which yields instant result (money) to him. His wife, Durga (Mala Sinha) is a highly religious and pious woman who firmly believes in the teachings of Geeta. Shankar feels that he and Durga cannot live together as husband and wife. Hence he leaves her along-with his son Mohan .He leaves his village and shifts to the city of Mumbai. He teaches his son to steal and rob people.The father-son duo commit crimes in association with Keshav Lal (Ajit) and his confident - Bhiku Ghasi Ram (Dheeraj Kumar).

On the other hand, Durga faces immense hardships. When Shankar left him, she was pregnant and after leaving the house (because it has been attached by the court because of non-payment of loan taken by Shankar against that), she gives birth to her second son who courtesy the good teachings and nice Samsakaras of his mother, grows up as an idealist - Ajay (Jeetendra). Ajay is a lawyer but he runs a newspaper also, named as - Karmayogi. Keshav Lal conspires against Shankar and gets him caught by the police. Since Shankar has murdered a police officer, he gets death penalty. Later Mohan happens to meet his separated mother and younger brother but he has already chosen his end and after seeking his revenge from the conspirators, follows his father to the gallows. However in his ending moments, his mother is there to recite the couplets of Geeta to him. These couplets are not only those belonging to the concept of Karma but also the concepts of soul and rebirth as explained by Lord Krishna.

Lord Krishna says in Geeta that a man has a right over his deeds but not their results. He, therefore, should do his duty diligently without pondering over its outcomes. If the deeds are good, their outcomes will be positive. If the deeds are bad, their outcomes will, quite naturally, be negative. The fruits or the results of the deeds are never instant. They take their due course in materializing. Hence a person should be patient enough to wait for the results of his endeavours (specially when they are positive). And the best way to maintain patience is to be indifferent towards the fruits of your deeds. And one who believes in performing his duties with sincerity without thinking or getting impatient about the fruits / rewards, is called a Karmayogi.

This theory of Karma (deed / effort) and Karma-Phala (reward of the deed) may sound impractical and not useful to many modern day people who go by common logic and feel that if the reward (or its timing) is uncertain, then whats the meaning of effort. The protagonist also thinks in the same way. He is not patient to wait for the fruit of his effort. He wants his reward instantly. And this is mostly not possible only when the effort is good or lawful. However, unlawful activities may yield their materialistic rewards within no time.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aaiye Huzoor Aaiye Na"
| Asha Bhosle
|-
| 2 
| "Ek Baat Kahoon Main Sajna"
| Lata Mangeshkar
|-
| 3
| "Mohabbat Hoon Haqeeqat Hoon"
| Asha Bhosle
|-
| 4
| "Tum Nahin Yahan Hum Nahin"
| Kishore Kumar, Mohammed Rafi, Asha Bhosle
|-
| 5
| "Jaisi Karni Vaisi Bharni"
| Manna Dey
|-
| 6
| "Tere Jeevan Ka Hai Karmon Se Nata"
| Manna Dey
|}

==External links==
*  

 
 
 
 
 

 
 