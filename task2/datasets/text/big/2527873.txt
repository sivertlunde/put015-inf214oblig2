Bright Leaf
 
{{Infobox film
| name           = Bright Leaf
| image          = 1950 brightleaf.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Curtiz
| producer       = Henry Blanke
| screenplay     = Ranald MacDougall
| based on       =  
| starring       = {{Plainlist|
* Gary Cooper
* Lauren Bacall
* Patricia Neal
}}
| music          = Victor Young
| cinematography = Karl Freund
| editing        = Owen Marks
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Bright Leaf is a 1950 American drama film directed by Michael Curtiz and starring Gary Cooper, Lauren Bacall and Patricia Neal.  
 Foster Fitz-Simons. The title comes from the type of tobacco grown in North Carolina after the American Civil War. The plot is loosely based on the rivalry of tobacco tycoons Washington Duke and John Harvey McElwee, according to Bright Leaves, a 2003 documentary by Ross McElwee.

== Plot ==
In 1894, poor but arrogant Brant Royle (Gary Cooper) returns to his hometown of Kingsmont, North Carolina, to settle his recently deceased uncles estate. Years before, he had been driven out by powerful tobacco plantation owner Major Singleton (Donald Crisp) for daring to fall in love with Singletons daughter Margaret (Patricia Neal). 

When Royle stops a runaway carriage bearing Margaret, she gives him a cool reception, but his ardor remains undiminished. The only one glad to see him is Sonia Kovac (Lauren Bacall), who has always made it plain she loves him. Upon inheriting her mothers house, she prospered by turning it into a bordello.

Meanwhile, inventor John Barton (Jeff Corey) is unable to interest Major Singleton in financing the building of his revolutionary cigarette rolling machine. Singleton and all the other growers are cigar men. After Barton sees the hatred between Singleton and Royle, he approaches Royle. With only a few dollars to his name, Royle gets the money Barton needs from Sonia, making her a partner. The first man he hires is Chris Malley (Jack Carson), a medicine showman. 

Bartons invention produces cigarettes at a fraction of the cost of hand rolling, and Royles company grows by leaps and bounds. One by one, Royle destroys the plantation owners, until only Singleton is left. Finally, Royle shows the Major that he has gained control of Singletons company, having bought up all the shares that Singleton has been selling. Royle offers to give them back ... as a wedding present.

When Singleton discovers that Margaret is willing to marry Royle despite not loving him, he challenges the upstart to a duel. Royle declines, but Singleton shoots and slightly wounds the unarmed man. His public disgrace and loss of honor leads him to commit suicide.

Margaret marries Royle and they honeymoon abroad. Chris Malley, now Royles second in command, discovers that Margaret has been selling millions of dollars worth of stock that her husband gave her and that she also gave Barton, forced out of the company by Royle, the idea to leak information to the Attorney General, leading to monopoly charges. When Royle confronts Margaret, she tells him she had been scheming and planning to bring him down ever since her father killed himself. Realizing too late that he has alienated all his true friends, he apologizes to Sonia and leaves town.

== Cast ==
*Gary Cooper as Brant Royle
*Lauren Bacall as Sonia Kovac
*Patricia Neal as Margaret Jane Singleton
*Jack Carson as Chris Malley, aka "Dr Monaco"
*Donald Crisp as Major Singleton
*Gladys George as Rose, one of Sonias whores Elizabeth Patterson as Tabitha Singleton, Margarets older relation and companion
*Jeff Corey as John Barton
*Taylor Holmes as Lawyer Calhoun
*Thurston Hall as Phillips, a tobacco grower

==Notes==
 

== Legacy == Bright Leaves by filmmaker Ross McElwee, a descendant of the man whose life was reflected in both the novel and film.   

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 