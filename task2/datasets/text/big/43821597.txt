Balam (1949 film)
{{Infobox film
| name           = Balam
| image          = Balam 1949.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Homi Wadia
| producer       = Hila Wadia Productions
| writer         = JBH Wadia
| screenplay     = 
| story          = 
| based on       = 
| narrator       =  Wasti Jayant Jayant Agha Agha
| music          = Husnlal Bhagatram 
| cinematography = 
| editing        = 
| studio         = Wadia Movies
| distributor    = 
| released       = 1949
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1949 Hindi romantic action film directed by Homi Wadia.    Credited as A Hila Wadia Production  by Wadia Brothers the film starred Suraiya, Wasti (actor)|Wasti, Nigar Sultana, Jayant (actor)|Jayant, Gulnar and Agha (actor)|Agha.    The music directors were Husnlal Bhagatram.    

==Cast==
* Suraiya
* Wasti
* Nigar
* Jayant
* Agha
* Jankidas
* Suraiya Chawdhry
* Gulnar
* Anwari
* Master Ratan
* H. Prakash

==Music==
The music was composed by Husnlal Bhagatram and lyrics were by Qamar Jalalabadi.    Lata Mangeshkar sang her  first duet with Suraiya in this film with the song O Pardesi Musafir Kise Karta Hai Ishare.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-
| 1
| O Pardesi Musafir Kise Karta Hai Ishare
| Suraiya, Lata Mangeshkar
|-
| 2
| Pyar Mein Do Dil Mile Aur Door Ho
| Suraiya
|-
| 3
| Aise Mein Agar Tum Aa Jate
| Suraiya
|-
| 4
| Jo Bhoole Hain Tujhe Ae Dil
| Suraiya
|-
| 5
| Dekh Li O Duniyawale Teri Duniya
| Suraiya
|-
| 6
| Tum Hamein Bhool Gaye
| Mohammed Rafi
|-
| 7
| Aata Hai Zindagi Mein Bhala Pyar Kis Tarah
| Suraiya, Mohammed Rafi
|-
| 8
| Thukra Ke Hamein Chal Diye
| Mohammed Rafi
|}

==References==
 

==External links==
* 

 


 

 
 
 
 
 