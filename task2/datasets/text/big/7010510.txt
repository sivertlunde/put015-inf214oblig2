The Super Cops
 
{{Infobox film
| name           = The Super Cops
| image          = Supercops.jpg
| image_size     =
| caption        =
| director       = Gordon Parks
| producer       = William Belasco
| writer         =  book:  
| narrator       =
| starring       = Ron Leibman David Selby Sheila Frazier Pat Hingle Dan Frazer Joseph Sirola Arny Freeman
| music          = Jerry Fielding
| cinematography = Richard C. Kratina
| editing        = Moe Howard
| distributor    = Metro-Goldwyn-Mayer United Artists
| released       = March 20, 1974
| runtime        = 90 min.
| country        = United States English
| budget         =
| preceded_by    =
| followed_by    =
}}
The Super Cops is a   (also based on a true story).

==Plot==
Two rookie policemen, Dave Greenberg and Rob Hantz, straight out of the NYPD police academy, want to do more than traffic control. They have their own ideas about fighting crime against drug dealers, criminals and corrupt cops in Brooklyn.

== In other media ==
The Archie Comics superhero imprint Red Circle Comics published one issue of a Super Cops comic book (with stories written by Marv Channing) in July 1974 in comics|1974.

== DVD Release == Warner Archive on Twitter, and Warner responded by announcing a remastered manufactured on-demand edition of The Super Cops. The film was released on DVD via the Warner Archive Collection on September 13, 2011.

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 

 