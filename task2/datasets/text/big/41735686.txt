Broadway Hostess
{{Infobox film 
 | name = Broadway Hostess Frank McDonald
 | producer = 
 | writer = George Bricker (Screenplay) Benjamin M. Kaye (Story, Uncredited)
 | starring = Wini Shaw Genevieve Tobin Lyle Talbot
 | music = Mort Dixon (Music and lyrics) Leo F. Forbstein (Musical director) Allie Wrubel (Music and lyrics) Ray Heindorf  (Music arranger-uncredited)
 | cinematography = Arthur L. Todd (photography)
 | editing =Jack Killifer
 | distributor = Warner Bros.
 | released =  
 | runtime = 68 minutes
 | language = English
 | country = United States
 | budget = 
}}
 romantic comedy musical that 1935 Academy Best Dance Direction category.  For which Bobby Connolly was nominated for, along with the film Go Into Your Dance.  

==Plot==
It is about a small town girl on her rise to stardom. But is having problems with love. Tommy falls in love with Winnie, but he feels she is in love with her manager Lucky. Lucky claims he does not want to get married, but is in fact in love with the rich socialite Iris. While Iriss brother loses money with his gambling problem. 

==Cast==
*Wini Shaw as Winnie (credited as Winifred Shaw)
*Genevieve Tobin as Iris
*Lyle Talbot as Lucky
*Allen Jenkins as Fishcake Phil Regan as Tommy Marie Wilson as Dorothy
*Spring Byington as Mrs. Duncan-Griswald-Wembly-Smythe  Joe King as Big Joe Jarvis (Credited as Joseph King) Donald Ross as Ronnie Marvin
*Frank Dawson as Morse - Iris Butler
*Harry Seymour as Club Intime Emcee

==Uncredited cast members (Incomplete)==

*Ward Bond as Luckys Henchman Richard Powell as Third Member of Quartet in Playboy of Paree Number 
*June Travis as Mrs. Bannister
*Jack Wise as Nightclub Waiter 
*Jane Wyman as a chorus girl

==Reviews==
The 1935 The New York Times gave it a bad review saying it was very clichéd and hard to sit through. 

==References==
 

== External links ==
*  
* 
*  

 
 
 
 
 
 
 
 
 
 
 


 
 