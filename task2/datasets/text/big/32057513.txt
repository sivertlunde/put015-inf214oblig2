Exile (1994 film)
 
{{Infobox film
| name           = Exile
| image          = 
| caption        = 
| director       = Paul Cox
| producer       = Paul Cox Santhana Naidu Paul Ammitzboll
| writer         = E. L. Grant Watson Paul Cox based on = novel Priests Island by E.L. Grant Watson
| starring       = Aden Young Beth Champion Claudia Karvan
| music          = Paul Grabowsky
| cinematography = Nino Gaetano Martinetti
| editing        = Paul Cox
| studio = Illumination Films Film Victoria Australian Film Finance Corporation
| distributor    = Beyond Films Roadshow (video)
| released       =  
| runtime        = 95 minutes
| country        = Australia
| language       = English
| budget         = A$2 million "Production Survey", Cinema Papers, May 1993 p74-75 
}}
Exile is a 1994 Australian drama film directed by Paul Cox. It was entered into the 44th Berlin International Film Festival.    The film was shot entirely on location in Tasmania. 

==Plot==
In the 19th century a young man is banished to an island for stealing a few sheep.

==Cast==
* Aden Young as Peter Costello
* Beth Champion as Mary
* Claudia Karvan as Jean
* Norman Kaye as Ghost Priest David Field as Timothy Dullach
* Chris Haywood as Village Priest
* Barry Otto as Sheriff Hamilton
* Hugo Weaving as Innes
* Tony Llewellyn-Jones as Jeans Father
* Nicholas Hope as MacKenzie
* Gosia Dobrowolska as Midwife
* Tammy Kendall as Alice

==Production==
The film is based on a novel which was set in Scotland but Cox relocated it to Tasmania. Although an earlier script was written by another writer, Cox wrote the screenplay for the film over eight days while on holiday on a Greek Island. Half the budget was provided by the Film Finance Corporation. Andrew L. Urban, "Paul Cox: Exile", Cinema Papers, August 1993 p6-9  

The film was shot from 15 March to 25 April 1993 on the Freycinet Peninsula on the east coast of Tasmania.  Cox:
 Its a very religious film. Because of that, it is not very commercial, is not very successful. I think its a very good film... Exile is about the sea. Its also about society, how it always destroys the individual: that were not the end product of that society, were just there to be manipulated and used. Its about a man kicked out of society who really becomes himself. He shines, burns through all the rubbish of the mind and the body. He has to somehow survive physically as well, and he does it quite brilliantly. People even get jealous of him. They ban him and exile him.   accessed 18 November 2012  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 