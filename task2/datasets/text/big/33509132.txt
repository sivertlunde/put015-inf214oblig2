Gunfighters (film)
 
{{Infobox film
| name           = Gunfighters
| image          = Gunfighters FilmPoster.jpeg
| border         = yes
| caption        = Theatrical release poster
| director       = George Waggner
| producer       = Harry Joe Brown
| screenplay     = Alan Le May
| based on       =  
| starring       = {{Plainlist|
* Randolph Scott
* Barbara Britton
* Bruce Cabot
}}
| music          = {{Plainlist|
* Rudy Schrager
* Gerard Carbonara
}}
| cinematography = Fred Jackman Jr.
| editing        = Harvey Manger
| studio         = {{Plainlist|
* Columbia Pictures
* Producers-Actors Corporation
}}
| distributor    = Columbia Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western Cinecolor The Searchers author Alan Le May, the film is about a gunfighter who lays down his guns after being forced to shoot his best friend, and decides to become a cowhand on a ranch. The film was released in the United Kingdom as The Assassin.

==Plot==
Trying to put his life as a gunfighter behind him, Brazos Kane (Randolph Scott) goes off to join old pal Bob Tyrell at the Inskip ranch, only to see him gunned down by an unseen shooter.

Brazos takes the body to the Banner ranch, but the ruthless Banner (Griff Barnett) has him arrested for the murder by Yount (Grant Withers), a corrupt deputy. Brazos has the bullet that killed his friend and slips it to Jane Banner (Dorothy Hart), the ranchers daughter.

Inskip (Charley Grapewin) frees him before Brazos can be unjustly tried and hanged. Brazos makes the mistake of trusting Bess (Barbara Britton), sister of Jane, but she is in love with ranch foreman Bard Macky (Bruce Cabot), the man who killed Tyrell.
 John Miles), the last straw. Brazos arms himself and goes after the bad guys, wounding Yount several times to make him talk, then calling out Orcutt and Bard for a final showdown, with Janes help.

==Cast==
* Randolph Scott as Brazos Kane
* Barbara Britton as Bess Banner
* Bruce Cabot as Bard Macky
* Charley Grapewin as Rancher Inskip
* Steven Geray as Jose aka Uncle Joe
* Forrest Tucker as Ben Orcutt
* Charles Kemper as Sheriff Kiscaden
* Grant Withers as Deputy Bill Yount John Miles as Johnny ONeil
* Griff Barnett as Mr. Banner
* Dorothy Hart as Jane Banner

==Production==
Gunfighters was filmed on location at Andy Jauregui Ranch and Monogram Ranch in Newhall, California, Vasquez Rocks Natural Area Park in Agua Dulce, California, and Sedona, Arizona.   

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 