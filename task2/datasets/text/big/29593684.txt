Min Fynske Barndom
 
 Danish island of Funen. Published in 1927, it was the basis of the Erik Clausens film of the same name in 1994, translated into English as My Childhood Symphony.

==Book==

In his autobiography, the Danish composer Carl Nielsen describes his early life on the island of Funen until he moved to Copenhagen in 1884 in order to study at the Royal Danish Academy of Music|Conservatory. It has been pointed out, however, that as he did not begin writing the account until prompted by his daughter in 1922, the story he tells may have been somewhat over-romanticised, reflecting Hans Christian Andersens similarly difficult childhood, also on the island of Funen. 

The work does nevertheless provide a detailed account of the composers early years and is a primary source of information about this period of his life. It describes the hardships of his family, how his father, a painter and farm labourer, went off playing the fiddle at local dances and celebrations to earn a little more. It tells of his earliest musical memories, especially the time when his mother handed him a violin when he was in bed with the measles. We also learn of his school life: "I was not very good at bookish subjects, but not one of the worst either".  He was fortunate that Emil Petersen, a teacher at the school, "taught me later on to play the violin properly from notation", enabling him to play at dances with his father. He also tells us of jobs as a goose-herd when still quite small, a summer spent at a tile works, and an apprenticeship with a shopkeeper who went bankrupt. His musical career began when his father "had heard that there was an opening for a musician with the Sixteenth Battalion in Odense, We agreed that I should practise the trumpet intensively and register for the audition..." All went well and Nielsen was able to play in the band while taking violin lessons in Odense. 

==Film==

In 1994, Eric Clausen directed the 125-minute film, Min Fynske Barndom, which is based heavily on Nielsens autobiography. It describes how Nielsen evolved from being a gooseherd and a drummer for the village players to first a member of a regimental band and then a composer of international renown. It also tells the story of an unsuccessful romance.  The English-language version is entitled My Childhood Symphony. 

==Bibliography==
* Carl Nielsen: "Min fynske Barndom", Copenhagen: Martin, 1927, 220 pp. (original edition)
* Carl Nielsen: "Min fynske barndom", Frederiksberg: Fisker & Schou, 1995, 181 pp. (current edition)
* Carl Nielsen, Reginald Spink (translator): "My childhood", Copenhagen: Wilhelm Hansen, 1972, 152 pp. 

==References==
 

 
 
 
 
 
 