Moestika dari Djemar
Moestika dari Djemar (  (now Indonesia). Multiple modern sources also use the incorrect spelling Moestika dari Djenar,  but contemporary sources uses an "m".  

Moestika dari Djemar was directed by Jo An Djan and produced by Jo Kim Tjan for Populairs Film. The black-and-white film starred Dhalia, Rd Mochtar, Rd Kosasie, Eddy T Effendi, and 9-year-old Djoeriah (in her feature film debut). The studio had been established the preceding year and already released a single film, Garoeda Mas, the year before.   
 Japanese occupation began in 1942. 
 eastern Java. It was targeted at audiences of all ages and advertised "brilliant costumes&nbsp;– mysticism&nbsp;– adventure and romance". 

The film is likely lost film|lost. The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==References==
 

==Works cited==
 
* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
* {{cite web
  | title = Moestika dari Djenar
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-m018-41-689102_moestika-dari-djenar
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 25 July 2012
  | archiveurl = http://www.webcitation.org/69QrAH9Ie
  | archivedate = 25 July 2012
  | ref =  
  }}
*{{Cite news
 |title=(untitled)
 |language=Dutch
 |work=Bataviaasch Nieuwsblad
 |date=8 January 1942
 |url=http://kranten.kb.nl/view/article/id/ddd%3A110612831%3Ampeg21%3Ap008%3Aa0043
 |page=8
 |publisher=Kolff & Co.
 |accessdate=15 September 2013
 |ref= 
}}
*{{Cite news
 |title=(untitled)
 |language=Dutch
 |work=Soerabaijasch Handelsblad
 |date=11 February 1942
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011122037%3Ampeg21%3Ap007%3Aa0116
 |page=7
 |publisher=Kolff & Co.
 |accessdate=15 September 2013
 |ref= 
}}
 

==External links==
* 
 

 
 