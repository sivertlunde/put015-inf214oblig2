Little Sinner
{{Infobox film
| name           = Little Sinner
| image          =Little sinner TITLE.JPEG
| image size     =
| caption        =
| director       = Gus Meins
| producer       = Hal Roach
| writer         =
| narrator       =
| starring       =
| music          = Leroy Shield
| cinematography = Francis Corby
| editing        = Louis McManus
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 17 15" 
| country        = United States
| language       = English
| budget         =
}}
 short comedy film directed by Gus Meins. It was the 139th Our Gang short to be released.

==Plot==
Anxious to go fishing, Spanky skips out of Sunday school, despite the admonitions of his pals Alfalfa, Mildred, Sidney, and Marianne that "Somethings going to happen to you." Actually, everything happens to Spanky and his kid brother (Eugene "Porky" Lee) in the course of the morning. Chased out of a private estate by cantankerous caretaker, the two boys wander into a dark, mysterious woods just as an eclipse occurs and at the same time a large group of black worshippers are holding a mass baptism ceremony.  Some now insist on seeing the baptism and background singing of the Negro spiritual "(Why dont you) Come out of the wilderness" as a racist stereotype.  They miss the point that this story had a religious casting, Spanky skipping church, and going into the "wilderness".  At worst it is an internal pun on the plot.  The song is performed well and was most likely not intended to denigrate anyone.

Inevitably, the kids scare the worshippers, and vice versa, culminating in a hectic chase.   

==Note==
*Little Sinner was withdrawn from the "Little Rascals" TV package in 1971 due to its racial content. It was reinstated in 1979 with severely edited prints that exclude the eclipse and the baptism. The original version was reinstated for the 2001 to 2003 showings on AMC (TV channel)|AMC.
*This episode marks the first appearance of Eugene Gordon Lee as Porky.

==Cast==
===The Gang=== Eugene Lee as Porky
* George McFarland as Spanky
* Billie Thomas as Buckwheat
* Carl Switzer as Alfalfa
* Jerry Tucker as Jerry
* Rex Downing as Our Gang member
* Sidney Kibrick as Our Gang member
* Donald Proffitt as Our Gang member
* Jackie Banning as Mary Ann (unconfirmed)

===Additional cast===
* Ray Turner as Man at Baptism/Man losing tent Clarence Wilson - Property owner
* Barbara Goodrich as Church extra
* Joan Lott as Church extra
* Philip Marley Rock as Church extra
* John Collum as Undetermined role
* Mildred Kornman as Undetermined role
* Dickie De Nuet as Undetermined role
* The Etude Chorus as Singers at Baptism

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 