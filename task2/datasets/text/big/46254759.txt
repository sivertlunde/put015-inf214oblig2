Asha Jaoar Majhe
{{Infobox film
| name = Asha Jaoar Majhe
| image = Labour_of_Love_Poster.jpg
| caption  = International poster of the film.
| director = Aditya Vikram Sengupta
| writer = Aditya Vikram Sengupta
| starring = Ritwick Chakraborty Basabdatta Chatterjee
| producer = Jonaki Bhattacharya   Aditya Vikram Sengupta	
| distributor = For Films
| cinematography = Mahendra J. Shetty   Aditya Vikram Sengupta
| editing = Aditya Vikram Sengupta
| released =  
| runtime = 84 minutes Bengali
| country = India
}}
Asha Jaoar Majhe ( ) is a 2014 Bengali-language film by Aditya Vikram Sengupta. The film stars Ritwick Chakraborty and Basabdatta Chatterjee as an unnamed married couple, where it brings focus to the hard lives endured by ordinary working people in Calcutta.
 Best Audiography.   

==Plot==
The woman (Basabdatta Chatterjee) works in a handbag factory while the man (Ritwick Chakraborty) works the night shift at a printing press. The film follows that young married couple as they go about their day, though only actually meeting in a fantasy where they seem happy and content.

==Awards==

62nd National Film Awards (India) 
* Indira Gandhi Award for Best First Film
* Best Audiography

71st Venice International Film Festival (Venice Days)
* Best Director of a Debut Film

Marrakech International Film Festival
* Best Director 

Abu Dhabi Film Festival
* Jury Special Mention

BFI London Film Festival
* Honorable Mention

Bangalore International Film Festival
* NETPAC Award for Best Asian Film

Jaipur International Film Festival
* Best Feature Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 