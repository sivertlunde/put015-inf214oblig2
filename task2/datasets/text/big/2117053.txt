Weekend (1967 film)
{{Infobox film name            = Week-end image           = Weekendflm.jpg caption         = Film poster director        = Jean-Luc Godard producer        = writer          = Jean-Luc Godard starring        = Mireille Darc Jean Yanne music           = Antoine Duhamel cinematography  = Raoul Coutard editing         = Agnès Guillemot distributor     = Athos Films released        =   runtime         = 105 minutes country         = France language        = French budget          = $250,000 (estimated)
|}}
Weekend ( ) is a 1967 black comedy       film written and directed by Jean-Luc Godard and starring Mireille Darc and Jean Yanne, both of whom were mainstream French TV stars. Jean-Pierre Léaud, iconic comic star of numerous French New Wave films including François Truffaut|Truffauts Les Quatre Cent Coups (The Four Hundred Blows) and Godards earlier Masculin, féminin, also appears in two roles. Raoul Coutard served as cinematographer; Weekend would be his last collaboration with Godard for over a decade.

The film was nominated for the Golden Bear at the 18th Berlin International Film Festival in 1968.      

== Plot ==
Roland (Jean Yanne) and Corinne (Mireille Darc) are a bourgeois couple, although each has a secret lover and conspires to murder the other. They set out by car for Corinnes parents home in the country to secure her inheritance from her dying father, resolving to resort to murder if necessary.

The trip becomes a chaotically picaresque journey through a French countryside populated by bizarre characters and punctuated by violent car accidents. After their own car (a Facel-Vega) is destroyed in a collision, the characters wander through a series of vignettes involving class struggle and figures from literature and history, such as Louis Antoine de Saint-Just (Jean-Pierre Léaud) and Emily Brontë (Blandine Jeanson).
 hippie revolutionaries Seine and Oise Liberation Front) that support themselves through theft and cannibal|cannibalism. Roland is killed during an escape attempt; he is chopped up and cooked. 

==Cast==
 
*Mireille Darc as Corinne
*Jean Yanne as Roland
*Paul Gégauff as Pianist
*Jean-Pierre Léaud as Louis Antoine de Saint-Just|Saint-Just
*Blandine Jeanson as Emily Brontë
*Yves Afonso as Tom Thumb
*Juliet Berto as The Radical
 

== Background==
According to a letter from the Argentine writer Julio Cortázar to his translator Suzanne Jill Levine, the indirect inspiration for the movie was Cortázars short story "The Southern Thruway." Cortázar explained that while a British producer was considering filming his story, a third party presented the idea to Godard, who was unaware of its source. Because he had had no input on the making of the film, Cortázar vetoed the suggestion to translate the storys title as "Week-End" to take advantage of the tie-in. 

==References==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 
 