Annie Get Your Gun (film)
{{Infobox film
| name           = Annie Get Your Gun
| image          = Annie get your gunfilmposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = George Sidney Busby Berkeley   Charles Walters  
| producer       = Arthur Freed Roger Edens
| screenplay     = Sidney Sheldon
| based on       =  
| starring       = Betty Hutton  Howard Keel  Benay Venuta
| music          = Irving Berlin
                   formerly Jerome Kern
| cinematography = Charles Rosher
| editing        = James E. Newcom
| distributor    = Metro Goldwyn Mayer
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $3,734,000  . 
| gross          = $7,756,000 
}} musical comedy stage musical of the same name, was directed by George Sidney. Despite some production and casting problems (Judy Garland was fired from the lead role after a month of filming in which she clashed with the director and repeatedly showed up late or not at all), the film won the Academy Award for best score and received three other nominations. Star Betty Hutton was recognized with a Golden Globe nomination for Best Actress.

==Cast==
* Betty Hutton as Annie Oakley  Frank Butler
* Benay Venuta as Dolly Tate
* Louis Calhern as Col. William F. Cody (Buffalo Bill)
* J. Carrol Naish as Chief Sitting Bull Edward Arnold as Pawnee Bill
* Keenan Wynn as Charlie Davenport
* Clinton Sundberg as Foster Wilson
* Evelyn Beresford as Queen Victoria (uncredited)
* John War Eagle as Indian Brave (uncredited) 
* Chief Yowlachie as Little Horse (uncredited)

==Musical numbers==
# "Colonel Buffalo Bill" — Charlie, Dolly, Ensemble
# "Doin What Comes Naturlly" — Annie, Siblings
# "The Girl That I Marry" — Frank
# "You Cant Get a Man with a Gun" — Annie
# "Theres No Business Like Show Business" — Frank, Buffalo Bill, Charlie Davenport, Annie with ensemble
# "They Say Its Wonderful" — Annie, Frank
# "Moonshine Lullaby" — Annie, Porters, Siblings
# "Theres No Business Like Show Business (Reprise)" — Annie
# "My Defenses Are Down" — Frank, Ensemble
# "Im an Indian, Too" — Annie
# "I Got Lost in His Arms" — Annie
# "I Got the Sun in the Morning" — Annie Anything You Can Do" - Annie, Frank
 An Old Fashioned Wedding" was written for the 1966 revival). The 2000 compact disc release of the soundtrack includes all of the films numbers and, "Lets Go West Again" (a Hutton number deleted before the films release), an alternate take of Wynns "Colonel Buffalo Bill", and Garlands renditions of Annies pieces.

==Production history==
Betty Hutton played Annie with Howard Keel (making his film debut) as Frank Butler and Benay Venuta as Dolly Tate. Frank Morgan was originally cast as Buffalo Bill Cody but after shooting the films opening production number, "Colonel Buffalo Bill", he died suddenly of a heart attack. Morgan was replaced by Louis Calhern. Originally, Judy Garland had been cast in the title role, and recorded all of her songs and worked for two months under Busby Berkeleys direction, who was in charge of all the musical numbers.  In the late 30s, Judy Garland and Mickey Rooneys "Andy Hardy" musical numbers had been staged and directed by Busby Berkeley.  Berkeley was severe with Garland insisting she perform at her best effort. Garland had resented the hard driving Busby from her past experience working on the "Andy Hardy" features.  Berkeley felt Garlands attitude lacked any effort and enthusiasm.  Garland continually complained about Busby to Louis B. Mayer, trying to have him fired from the feature.  Judy Garlands revenge was to be late, or not showing up for each days filming schedule.  MGM suspended Judy Garland for her delinquency for not appearing daily for her scheduled call times. Garland claimed she was forced to leave the production (according to press releases) because of poor health and other personal problems; that would later end her career with MGM. Garlands dismissal from this film (from which some footage and recordings have survived) figures in the show-biz legend of Judy Garlands fall from grace, her alleged unreliability, and the view of her as a victim of the studio. Betty Garrett was considered but the role of Annie eventually went to Betty Hutton. Shooting resumed after five months, with George Sidney replacing Charles Walters (who in turn replaced Berkeley) as director.  

According to Betty Hutton, she was treated coldly by most of the cast and crew because she replaced Garland. During an interview with  . Additional studio recordings of Garland also exist and have been released by Rhino Records.

==Release==
Despite the production problems, the film became popular in its own right. During its initial release, MGM recorded it as earning $4,708,000 in the US and Canada and $3,048,000 overseas, resulting in a profit of $1,061,000.   

In 1973 it was withdrawn from distribution, owing to a dispute between Irving Berlin and MGM over music rights, which prevented the public from viewing this film for almost 30 years. It was not until the films 50th Anniversary in 2000 that it was finally seen again in its entirety.

One of Huttons costumes, the very first "Wild West Show" costume seen in the film for the reprise of "Theres No Business Like Show Business" is on permanent display at the Costume World Broadway Collection Museum in Pompano Beach, Florida.

==Awards and nominations== Academy Award for Best Music Scoring of a Musical Picture (WON)    Academy Award for Best Art Direction-Set Decoration, Color (Cedric Gibbons, Paul Groesse, Edwin B. Willis, Richard A. Pefferle) (nominee)
* Academy Award for Best Cinematography, Color (nominee)
* Academy Award for Best Film Editing (nominee) Golden Globe Award for Best Motion Picture Actress - Musical or Comedy - Betty Hutton (nominee)
* Photoplay Award for Most Popular Female Star - Betty Hutton (WON)
* Writers Guild of America Award for Best Written American Musical - Sidney Sheldon (WON)

==References==
 

==Further reading==
* 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 