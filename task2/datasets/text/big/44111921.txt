Abie's Irish Rose (1946 film)
{{Infobox film 
| name           = Abies Irish Rose
| image          = 
| alt            = 
| caption        = 
| director       = A. Edward Sutherland
| producer       = A. Edward Sutherland 
| screenplay     = Anne Nichols
| based on       =  
| starring       = Michael Chekhov Joanne Dru Richard Norris J. M. Kerrigan George E. Stone Vera Gordon Emory Parnell
| music          = John Scott Trotter 	
| cinematography = William C. Mellor
| editing        = William H. Ziegler 	
| studio         = Bing Crosby Productions
| distributor    = United Artists
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Abies Irish Rose is a 1946 American comedy film directed by A. Edward Sutherland and written by Anne Nichols. The film stars Michael Chekhov, Joanne Dru, Richard Norris, J. M. Kerrigan, George E. Stone, Vera Gordon and Emory Parnell. The film was released on December 27, 1946, by United Artists.  

==Plot==
 

== Cast ==
*Michael Chekhov as Solomon Levy
*Joanne Dru as Rosemary Murphy Levy
*Richard Norris as Abie Levy
*J. M. Kerrigan as Patrick Murphy
*George E. Stone as Isaac Cohen
*Vera Gordon as Mrs. Cohen
*Emory Parnell as Father John Whalen Art Baker as Rabbi Jacob Samuels
*Eric Blore as Stubbins
*Bruce Merritt as Rev. Tom Stevens
*Roy Atwell as Dick Saunders
*Eddie Parks as Gilchrist
*Vera Marshe as Mrs. Edna Gilchrist
*James Nolan as Policeman Charlie Hall as Hotel Porter
*Harry Hays Morgan as Hotel Clerk

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 