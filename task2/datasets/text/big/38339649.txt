Jug Face
{{Infobox film
| name           = Jug Face
| image          = Jug Face Movie Poster.jpg
| alt            = 
| caption        = 
| director       = Chad Crawford Kinkle
| producer       = {{plainlist|
* Andrew van den Houten
* Robert Tonino
}}
| writer         = Chad Crawford Kinkle
| starring       = {{plainlist|
* Sean Bridgers
* Lauren Ashley Carter
* Larry Fessenden
* Sean Young
* Daniel Manche
* Michael G. Crandall
}}
| music          = Sean Spillane
| cinematography = Chris Heinrich
| editing        = Zach Passero
| studio         = Modernciné
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Jug Face is a 2013 American horror film written and directed by Chad Crawford Kinkle and starring Sean Bridgers, Lauren Ashley Carter, Larry Fessenden, Sean Young and Daniel Manche. The story follows a teen (Carter), who is pregnant with her brothers child and tries to escape from a backwoods community, only to discover that she must sacrifice herself to a creature in a pit.

==Plot==
A backwoods community worships a pit with healing powers. Dawai creates jugs of faces from clay, and when a jug portrays the face of a member of the community, that person must be sacrificed to the creature that lives in the pit. Ada, who has been having sex with her brother Jessaby, finds her own face on Dawais latest jug. Scared, she hides the jug and keeps it a secret.

Ada is arranged to be "joined" to a boy from another family, Bodey. Later, Ada discovers she is pregnant. While she is with Bodeys sister, Eileen, Ada has a vision of Eileens death, and the creature later kills Eileen. Worried that they have angered the creature, the community questions Dawai, who claims to be ignorant of whether theres a missing jug, as he creates them while under a trance. The townspeople become frustrated with Dawai and demand that he search for any potentially missing jugs. Ada meets with Dawai, and she suggests that he create a new one from memory. Adas mother, Loriss, inspects her to see if she is a virgin, and Ada claims to have broken her own hymen. Loriss punishes Ada, who later informs Jessaby that she is pregnant.

The following day, the community gathers around the pit as Dawai presents his latest jug with the face of Bodey, who is sacrificed. Ada visits her sickly grandfather and sees a vision of an emaciated boy, who explains that her grandfather hid his wifes jug face the same as she did, and the community poisoned her grandfather as punishment. Jessaby tells Sustin, his father, that he is sick, so they go to the pit to heal him. Ada has a vision of Jessaby getting killed; the vision comes true. As it seems Dawai has failed again in interpreting the pits desires, he is severely punished by the community and tied beside the pit to be taken. Ada breaks Dawai free, and they run off. However, the two are caught, brought back, and punished. Assuming that it is Dawai with whom Ada had a relationship, Ada is whipped, miscarries her child, and then reveals to her parents that it was her brothers baby.

Ada has a vision of her fathers death, and the creature kills him. She reveals to everyone that she was the missing jug face and that all the deaths are her fault. Her mother ties her up beside Dawai next to the pit. The Shunned Boy and Adas grandfather appear and free her from her ropes, but she will not leave because she knows Dawai will be killed. The next morning, Adas mother finds her free of her ropes but kneeling beside the Pit. Ada apologizes and is sacrificed. The film ends as Dawai lights a candle next to his jug face of Ada.

==Cast==
* Lauren Ashley Carter as Ada
* Sean Bridgers as Dawai
* Sean Young as Loriss
* Larry Fessenden as Sustin
* Daniel Manche as Jesseby
* David Greathouse as "The Creature"
* Chip Ramsey as Store Owner

==Production== Slamdance Screenwriting Competition, and it was announced at the 2012 Slamdance Film Festival that Moderncinés Andrew van den Houten and Robert Tonino would produce the film in Nashville, Tennessee.  It was announced in January 2012 that Lauren Ashley Carter, Sean Bridgers, Larry Fessenden, and Sean Young were cast in the film.  Sean Young said that she found it difficult to play an unsympathetic character, but she enjoyed working with Carter and Fessenden. Young was deliberately made to look old, which she said was helped by the custom-made clothing. 

Potter and sculptor Jason Mahlke designed and created the face jugs for the film. 

==Release== VoD release in July by Gravitas Ventures, with a national theatrical release to follow.   Jug Face grossed $14,315 in domestic DVD sales and $7,491 in domestic Blu-ray sales, for a total of $21,806. 

==Reception==
Rotten Tomatoes, a review aggregator, reports that 78% of 18 surveyed critics gave the film a positive review; the average rating was 6.3/10.   Metacritic rated it 58/100 based on nine reviews. 

Brad Miska of Bloody Disgusting rated the film a 3.5 out of 5 and wrote, "Jug Face feels really small, in a good way, and captures the essence of this tiny group of worshippers. Its aesthetically similar to other Modernciné movies; it’s a look that gives the film quality and technical shows expertise. Although, its carried by strong performances, and a few explicate   shots of gore."  Brad McHargue, writing for Dread Central, praised Kinkles script, as well as "the stellar performances of Carter and Bridgers." McHargue called it "loaded with talent" and "filled with emotion and just enough blood to keep the gore hounds satisfied."  Ryan Larson, a writer for Shock Till You Drop, wrote that "Jug Face isnt a groundbreaking movie. But its exceptional on a number of levels and is so because of a powerful director and some great acting."  Scott Weinberg, writing for Fearnet, praised the acting and wrote, " he directors steadfast insistence on presenting a potentially outlandish horror tale as plainly and realistically as possible that elevates Jug Face beyond that of a mere curiosity."  Rob Nelson of Variety (magazine)|Variety described it as "an impressively oozing slab of indie horror that bodes well for first-time writer-director Chad Crawford Kinkle." 

While the film is mainly praised for its script, effective low-budget filmmaking and performances, the storys supernatural elements received some criticism. In a   review, Ben Umstead wrote, "While steeped in thick atmosphere and anchored by some interesting riffs on classic horror tropes the film is far more underwhelming in execution than it is dynamic and provocative -- elements that feel largely left on the page."  Nicolas Rapold of The New York Times wrote, "Some low-budget manifestations of the supernatural jazz up the frights now and again, but as the novelty of worshiping a hole in the ground fades, the film paints itself into a corner." 

==References==
 

==External links==
*  
*   on AllMovie
*   on Rotten Tomatoes
*  

 
 
 
 
 
 
 
 