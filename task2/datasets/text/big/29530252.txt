Be-Shaque
{{Infobox film 
 | name = Be-Shaque 
 | image = BeShaque.jpg
 | caption = DVD Cover Kashinath
 | producer = 
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Yogeeta Bali Amrish Puri Jalal Agha Sonia Sahni
 | music = 
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = 1981
 | runtime = 120 min.
 | language = Hindi Rs 3 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1981 Hindi Indian feature directed by Kannada Actor -Director Kashinath (actor)|Kashinath, starring Mithun Chakraborty, Yogeeta Bali, Amrish Puri and Jalal Agha

==Cast==
*Mithun Chakraborty
*Yogeeta Bali
*Jalal Agha
*Amrish Puri
*Sonia Sahni
*Suresh Chatwal 
*Asha Chandra 
*Vinod R. Mulani 
*Paintal 


Plot

Shyam Sunder lives a wealthy lifestyle in a small village in India along with his widowed step-mother, Nirmala. He has taken to alcohol, and womanizing, goes away very often without informing anyone, and frequently asks Nirmala for money. One day Lucky informs Nirmala that he has seen Shyams dead body. She summons the Police, who arrive but find the dead carcass of a bear, and eventually decide that Shyam may have gone to Goa without informing Nirmala. Subsequently a young man named Prakash, who claims he is the son of Lucknow-based Tehsildar Somshekhar, arrives there, meets with Nirmala, informs her that he is Shyams friend and business partner, and a author by profession. She permits him to live in her spacious house, but subsequently asks him to move to the guest house. Prakash meets and falls in love with Roopa, who lives a middle-class lifestyle with her widowed mother, Radha, and servant, Gopal. Soon Roopa also falls in love with him and introduces him to her mom, who approves of him. Prakash will soon find out that all is not well when he comes across a framed photograph of Roopa in the company of a young man, and yet another photograph with the same man, as well as a child in Roopas arms. Then he is told about an abandoned house that has a ghastly secret. Prakash must now find out who the man in the photograph is, whether Roopa is a mother of the child in the photograph as well as the secret of the abandoned house, and the reason why Shyam is still missing. While he sets about to do this, he does not know he is under the scrutiny of several people in this quiet village - and at least one of them is ready to kill him.

==References==
*http://www.imdb.com/title/tt1219317/

==External links==
*  

 
 
 


 