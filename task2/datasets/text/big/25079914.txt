They Dare Not Love
{{Infobox Film
| name           = They Dare Not Love
| image          = "They_Dare_Not_Love"_(1941).jpg
| image_size     = 
| caption        = 
| director       = James Whale
| producer       = Samuel Bischoff Charles Bennett Ernest Vajda
| based on       = story by James Edward Grant
| narrator       = 
| starring       = George Brent Martha Scott Paul Lukas
| music          = Jacques Belasco	(uncredited)
| cinematography = Franz Planer Al Clark
| studio         = Columbia Pictures
| distributor    = Columbia Pictures 
| released       = May 16, 1941
| runtime        = 75 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| website        = 
| amg_id         = 
}} romantic war Charles Bennett Face to Face, "Hello Out There", but it was not included in the released film.  

==Plot==
A prince flees Austria when the Nazis take over and settles in London. He encounters a beautiful Austrian émigré who makes him realize his mistake in leaving. He strikes a deal with the Nazis to return in exchange for some Austrian prisoners, but discovers that the Nazis are not to be trusted.

==Cast==	
*Prince Kurt von Rotenberg - George Brent	
*Marta Keller - Martha Scott	
*Baron von Helsing - Paul Lukas

Rest of cast listed alphabetically:	
*Louis (uncredited) - Sig Arno	
*German Secretary -  Georgia Backus (uncredited)	
*Capt. Wilhelm Ehrhardt - Edgar Barrier (uncredited)	
*Second Sailor - Don Beddoe (uncredited)	
*First Sailor - Nicholas Bela (uncredited)	
*Pierre - Leon Belasco (uncredited)	
*Baron Shafter - Roman Bohnen (uncredited)	
*Stewardess - Olga Borget (uncredited)	
*Prof. Keller - Egon Brecher (uncredited)	
*Blonde Officer - Lloyd Bridges (uncredited)	
*Michael -  Stanley Brown (uncredited)	
*Deck Steward - Jack Chefe (uncredited)	
*Sub-Lieutenant Blackler - Peter Cushing (uncredited)	
*English Father - Leslie Denison (uncredited)	
*Doorman - Paul Deno (uncredited)	
*Reporter - Eddie Fetherston (uncredited)
*Countess Marlik - Marguerita Sylva (uncredited)

==Critical reception==
The New York Times wrote, "with all the proved talent Columbia put behind the manufacture of "They Dare Not Love" it is hard to understand why the new film at Loews State Theatre (New York City)|Loews State should turn out to be the disappointment it is. Granting that James Whales direction is pedestrian, that the performances of Martha Scott, George Brent and Paul Lukas are no better, we still feel that the root of all evil in this case sprouted back in the story department presided over by Charles Bennett, Ernest Vajda and James Edward Grant. Though the plot they whipped up probably is no more fantastic than some of the things happening in the world today it does not rouse either ones imagination or emotions..."They Dare Not Love" is vapid fare."  {{cite web|url=http://www.nytimes.com/movie/review?res=9506E1DD1F3DE33BBC4E52DFB366838A659EDE|title=Movie Review -
  They Dare Not Love - At Loews State - NYTimes.com|publisher=}} 

==External links==
* 

==References==
 

 

 
 
 
 
 


 