Metal Skin
 
{{Infobox film
| name           = Metal Skin
| image          = Metal Skin (1994) poster.jpg
| caption        = 
| director       = Geoffrey Wright
| writer         = Geoffrey Wright
| producer       = Daniel Scharf
| starring       = Aden Young   Tara Morice   Nadine Garner   Ben Mendelsohn
| music          = John Clifford White
| cinematography = Ron Hagen
| editing        = Bill Murphy   Jane Usher
| distributor    = Village Roadshow
| released       =          (premiered at the Toronto Film Festival)
| runtime        = 115 minutes
| rating         = MA15+ (Australia)
| country        = Australia
| language       = English
| website        =
}}

Metal Skin is a 1994 Australian film written and directed by Geoffrey Wright, starring Aden Young, Tara Morice, Nadine Garner and Ben Mendelsohn. The film follows the lives of four adolescents in and around the Blue-collar worker|blue-collar Melbourne suburb of Altona, Victoria|Altona.

==Synopsis==
Melbourne, 1994. Joe (Aden Young) lives with his mentally-ill father (Petru Gheorghiu) in working-class Altona. A shy misfit whose great love is hotted-up cars, Joe gets a job at a supermarket where he is befriended by fellow revhead Dazey (Ben Mendelsohn), a confident womaniser at the crossroads with his girlfriend, Roslyn (Nadine Garner). Savina (Tara Morice), a devil worshipper who works with Joe, feigns interest in him to get closer to Dazey. When Joe discovers Savina’s deceit he embarks on a violent and tragic rampage.   

==Reception== 1994 Venice Film Festival, Metal Skin proved a disappointment at the Australian box office when it was released on 4 May 1995 where it grossed $883,521.   Australian critic Andrew Howe praised it as "a dark, arresting ode to suburban hopelessness"  however  Todd McCarthy writing in Variety (magazine)|Variety described the film as "so overwrought and unrelievedly grim that it comes close to playing like a parody of teenage angst movies." 

===Awards=== AFI Awards in 1995 for Best Production Design (Steven Jones–Evans) and Best Sound (Frank Lipson, David Lee, Steve Burgess, Peter Burgess, Glenn Newnham) and received nominations for Best Actor (Aden Young), Supporting Actor (Ben Mendelsohn), Supporting Actress (Nadine Garner) and Costume Design (Anna Borghesi). Aden Young and Ben Mendelsohn shared the Film Critics Circle of Australia prize for Best Actor.

==Novelisation==
The novelisation of Metal Skin was written by Jocelyn Harewood and published by Text Publishing in 1995. Harewood follows the film closely however the book explores other sides of the characters: Joe’s inner rage at his brain-damaged father and his love for what his father has been; Savina’s destructive witchcraft; Dazey’s moments of self-awareness and higher motives. It was published as an e-book in November 2012 and made available on Harewoods website. 

==See also==
*Cinema of Australia

==References==
 

==External links==
*   at the Internet Movie Database
*   at Rotten Tomatoes

 

 
 
 
 