Nickelodeon (film)
 
{{Infobox film
| name = Nickelodeon
| image = Nickelodeon film poster.jpg
| caption = Original Theatrical poster
| director = Peter Bogdanovich Frank Marshall Irwin Winkler
| writer = Peter Bogdanovich W. D. Richter
| starring = Ryan ONeal Burt Reynolds Tatum ONeal Brian Keith Stella Stevens John Ritter
| music = Richard Hazard
| cinematography = László Kovács (cinematographer)|László Kovács
| editing = William C. Carruth
| studio = Columbia Pictures EMI Films British Lion Films
| released = December 21, 1976
| runtime = 121 minutes
| country = United Kingdom United States
| language = English
| budget = $9 million
}} 1976 comedy film directed by Peter Bogdanovich, and stars Ryan ONeal, Burt Reynolds and Tatum ONeal. According to Bogdanovich, the film was based on true stories told to him by silent movie directors Alan Dwan and Raoul Walsh. It was entered into the 27th Berlin International Film Festival.   

==Plot==
Going from a lawyer to a writer, and then to a film director, is the career path on which we find Leo Harrigan (Ryan ONeal). But Leo has problems as well, such as being hopelessly smitten with his leading lady, who chooses to grab his attentions by getting herself engaged to his vulgar and ignorant leading man, Buck Greenaway (Burt Reynolds).

==Cast==
*Ryan ONeal as Leo Harrigan
*Burt Reynolds as Buck Greenway
*Tatum ONeal as Alice Forsyte
*Jane Hitchcock as Kathleen Cooke
*Brian Keith as H.H. Cobb
*Stella Stevens as Marty Reeves
*John Ritter as Franklin Frank
*Brion James as bailiff
*Sidney Armus as judge
*Joe Warfield as defence attorney
*Lorenzo Music as Mullins
*Jeffrey Byron as Steve
*Priscilla Pointer as Mabel
*Philip Bruns as Duncan Frank Marshall as Dinsdales assistant
*Harry Carey, Jr. as Dobie
*James Best as Jim
*George Gaynes as Reginald Kingsley
*M. Emmet Walsh as "Father" Logan
*Miriam Byrd-Nethery as Aunt Lula
*Les Josephson as the Nickelodeon bouncer
*Griffin ONeal as a boy on a bicycle
*Hamilton Camp as Blacker

==Production==
The film started as a script by W.D. Richter called Stardust Memories which was purchased by Irwin Winkler. Winkler took the project to David Begelman at Columbia, who pursued Bogdanovich as director. Winkler later stated:
 He made David come to his office and wait until the receptionist said, Mr. Bogdanovich will see you now. As soon as we came inside, we were very haughtily told that he thought the script was a piece of  . Id been around long enough to know that I should take that as a bad sign. I remember coming out of the meeting, saying, David, why should we make the movie with someone who hates our script? And all David said was, Hey, hes a genius.... What he filmed had nothing to do with the original script. I know it meant a lot to Peter to have all of the authentic stories about the silent period in the film, but Ricks script, authentic or not, was terrific. It was just a great drama. By the time Peter was done with it, it was authentic, but it wasnt dramatic anymore. Peter hadnt really experienced any failure yet -- we hired him before At Long Last Love had come out -- so he was easily the most arrogant person Id ever met in the business, before or since. When we shot the picture, he actually directed some of the scenes on horseback. When I asked him why he was on horseback, he said, "Because thats the way John Ford did it."  
Bogdanovich has an alternative version:
 I should have never gotten involved, I should have done it myself. Id been planning to do a big picture about the silent era, largely based on the interviews with Dwan, Walsh and McCarey. I was preparing it and I got a call from my agent and she said theyre preparing a movie called Starlight Parade, theres another director involved but they want you. I said, "Well, I dont really want to do their script, Ill have to rewrite it completely." "Theyll let you rewrite it, whatever you want." Basically I rewrote the whole damn thing and never used any of Starlight Parade. The trouble was, again, the picture had a balance between comedy and drama and it was a comedy-drama, no question about it, and I had wanted to do it in black and white. It was very important to do it in black and white and Columbia, the studio, wouldnt let me. I had a big fight about that and they cancelled the picture. Then Barry Spikings at British-Lion came in and funded some of the picture, threw in a few million dollars. It ended up being a Columbia-British Lion picture and but when it was all done it was a difficult picture.   accessed 3 June 2013  
Columbia provided $6 million, British Lion $2 million. The directors fee was $700,000 - $500,000 of which was held as a completion guarantee. Bogdanovich--Will Nickelodeon Be His Last Picture Show?: Bogdanovich--What Went Wrong? Peter Bogdanovich-- What Went Wrong?
By DAVID DENBY. New York Times (1923-Current file)   30 Jan 1977: D1 

Bogdanovich said that his original choices for the lead roles were Jeff Bridges, John Ritter, Cybill Shepherd and Orson Welles. However Columbia Pictures head David Begelman refused. "I just had a smaller picture in mind," said the director later. Both Burt Reynolds and Ryan were good in it, and Jane Hitchcock was good but she didnt have any threat about her." 

"The whole idea was to capture the era, since obviously the original films were shot in black and white," Bogdanovich says. "My cinematographer, Laszlo Kovacs, carefully lit everything to accommodate black-and-white, which is why the lighting looks so good. We used a lot of the techniques of the silent era, irising in and out of scenes. There are no opticals at all in the film. But all the studio wanted was another broad comedy like Whats Up, Doc? "   accessed 3 June 2013 

During filming, Burt Reynolds collapsed on set one day. Doctors could not figure out what was wrong with him and the film had to be postponed for two weeks while he recovered.  The film went over schedule and over budget and Bogdanovich had to forfeit his $500,000. 

Winkler says when he saw a rough cut of the final film he thought it was "atrocious... for Peter to blame the movies failure on the casting and not being in black-and-white is a really terrible excuse for a guy who simply screwed up a really terrific script." 

Bogdanovich reminisced in 2004:
 The previews were edgy and the studio wanted me to take most of the drama out, play it more comedy and turn it more into a Whats Up Doc?, which it really wasnt. So that threw it off and it got fucked up. Again, the picture came out not at all the way I wanted. I tried to recut that one and I couldnt get back to it.Theres about five minutes Id like to put back that really makes a difference, some heavy stuff where you find out that Ryan ONeal has an affair with Stella Stevens, it becomes very clear, and you see that John Ritter knows it, all that stuff. It was just much heavier and darker. So the picture got screwed up and thats why I took three years off and went away."  

==Reception== Daisy Miller and At Long Last Love (the latter of which was also an attempt to revive an older style of film making). However, as for Richter;
 After it became clear the picture was a failure, the most amazing thing happened: I got more job offers than ever before in my life. People seemed so mad at Peter that they were eager to make excuses for me and help me out. And they all wanted to hear about working with him.
 Bogdanovich--Will Nickelodeon Be His Last Picture Show?: Bogdanovich--What Went Wrong? Peter Bogdanovich-- What Went Wrong?
By DAVID DENBY. New York Times (1923-Current file)   30 Jan 1977: D1  
After making the film, Bogdanovich felt he had compromised so much he took several years off directing before returning with Saint Jack (1979).

==Alternative versions==
The 2009 DVD release includes a 125 minute "Directors Cut" in black and white.  "Theres nothing to distract you," said Bogdanovich, "Ryans blond hair and blue eyes dont distract you, and you focus on the action in an easier way. Thats why the funniest movies ever made were silent comedies -- Buster Keaton, Harold Lloyd, Charlie Chaplin. It focuses the attention in a different way, and color is distracting for that sort of thing." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 