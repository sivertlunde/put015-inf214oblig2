Matariki (film)
 
 
{{Infobox film
| name           = Matariki
| image          = Matarikifilmposter.jpg
| caption        = New Zealand theatrical release poster Michael Bennett
| producer       = Fiona Copland
| screenplay     =  
| story          = Iaheto Ah Hi
| narrator       =
| starring       =  
| music          = Don McGlashan
| cinematography = Alun Bollinger John Gilbert
| studio         = Filmwork
| distributor    = Arkles Entertainment
| released       =   
| runtime        = 92 minutes
| country        = New Zealand
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Matariki is a 2010 New Zealand drama film set in Otara, South Auckland. The film is told through five interweaving stories all set in the days leading to the rising of Matariki. The film incorporates a variety of languages including, English language|English, Māori language|Māori, Tokelauan, Samoan language|Samoan, and Cantonese. It features an ensemble cast and is the feature debut for actors Susana Tang and Jason Wu. The film was funded by the New Zealand Film Commission.

==Plot== Michael Bennett’s first feature film merge into a fascinating portrait of a community.

It is New Year’s Eve and Gunge (Edwin Wright) finds that his debt to a ruthless drug dealer sets off a chain of events that rip through his South Auckland neighbourhood. After bravely intervening in a beating, star rugby player Tama (Mark Ruka) finds his own life in jeopardy. His wife, Megan (Sara Wiseman), comes into conflict with Tama’s Māori people|Māori family over decisions concerning his care. Tama’s brother Rick (Jarod Rawiri) is so rocked by the tragedy that he is forced to confront his own secrets and his allegiance to Maori values.

Meanwhile, teenaged Aleki (Jason Wu) struggles to find his identity in a new home. Transplanted from a small island in the South Pacific to the vibrant, multicultural community of South Auckland, Aleki feels caught between his father’s traditional values and the temptations of his new culture. Nearby, Lisa (Alix Bushnell), who is nine months pregnant and devoted to her drug addicted boyfriend, begins to suspect that he may not make the best father for her baby.

The problems and fears of this remarkably disparate group intertwine to form a compelling sphere of hope and transcendence. Renowned cinematographer Alun Bollinger (a favourite of director Peter Jackson) masterfully captures the gritty shadows of a crime in-progress, the harsh neon of a hospital room and the soft intimate lamplight of a lover’s bed. Building on the success of his notable short films "Cow" and "Kerosense Creek", Bennett focuses on the triumphs and crises of these unknowingly connected individuals to reveal the evolving mosaic of a culture and a city on the cusp of transformation.

==Cast==
(Alphabetical)
*Alix Bushnell as Lisa
*Ben Baker as Alekis Dad
*Camille Keenan as ICU Nurse
*Cherie James as Aunty Rachels Daughter
*Dylan Tavita as Kid
*Edwin Wright as Gunge
*Iaheto Ah Hi as Tyrone
*Jarod Rawiri as Rick
*Jason Wu as Aleki
*Joseph Naufahu as Young Cop
*Mabel Wharekawa as Aunty Rachel
*Mark Ruka as Tama
*Michael Whalley as Jermaine
*Pua Magasiva as Sergeant Wolfgramm
*Rachel Nash as Midwife
*Sara Wiseman as Megan
*Serena Cotton as Social Worker
*Susana Tang as Spit
*Vela Manusaute as Russ
 Michael Bennett.

==Production==
 Michael Bennett saw a one-man stage play by Iaheto Ah Hi about his Tokelauan cousin named Aleki (Ah Hi subsequently went on to play lead character Tyrone in the film). Filming began in April 2009 with it almost entirely shot in Otara, South Auckland.  Many locals were used as extras in the film (including location manager Damion Nathan), especially during scenes set in the Otara markets. Post production was partially done in Peter Jacksons Park Road Post studios. 
 Misfits of Feels Like Rain".
 John Gilbert.

==Music==
{{Infobox album  
|  Name        = Matariki (Original Motion Picture Soundtrack)
|  Type        = Soundtrack
|  Artist      = Don McGlashan
|  Cover       = 
|  Released    = 15 November 2010
|  Recorded    =
|  Genre       = Soundtrack
|  Length      =
|  Label       = EMI Music New Zealand ltd
|  Producer    =
|  Reviews     =
}}

Matariki (Original Motion Picture Soundtrack) was composed by Don McGlashan. What Love Can Do was written for Matariki by McGlashan and sung by Bella Kalolo. The soundtrack was given a 4 out of 5 rating by The New Zealand Herald.

===Track listing===
# "What Love Can Do"  (Bella Kalolo & The GLCC Youth Choir)   - 3:24
# "Siva Mai"  (Te Vaka)   - 4:12
# "Ride With Us"  (Boss (musician)|Boss)   - 3:45 Pitch Black)   - 5:57
# "These Roses"  (Gin Wigmore)   - 3:11
# "Teine Ole Atunu"  (Jamoa Jam)   - 3:30 Misfits of Science)   - 3:44
# "Raining Blood"  (Concord Dawn)   - 3:00
# "E Sio Vare Vare"  (Tongareva 5 with Mr Taia)   - 1:58
# "Sneaky Sneaky Dog Friend"  (Connan and the Mockasins)   - 3:53
# "Bolero Bamboos"  (Bill Sevesi)   - 3:44
# "Cupid"  (The Brunettes)   - 3:01
# "Mum"  (Prince Tui Teka)   - 3:11
# "Sister Risk"  (The Phoenix Foundation)   - 4:50
# "Feels Like Rain"  (The StoneFeather Blues Experience)   - 4:50

==Release==
The film held its world premiere on 11 September 2010 at the Toronto International Film Festival.  It screened in the Contemporary World Cinema section. It also screened at the 2010 Hofer Filmtage in Germany in late October. The film was released in cinemas in New Zealand on 18 November 2010, and on DVD in mid-2011.

==Reception==
At the 2010 Toronto International Film Festival, Matariki received three sold out screenings, and also received several sold out screenings at the 2010 Hofer Filmtage
 The Dominion Kate Rogers awarded the film 3 and a half stars out 4 describing the "multicultural heart" as one of the films strengths. Actor Temuera Morrison who saw the film in Toronto, said he was "absolutely blown away" and described the film as a "slice of reality in South Auckland".

===Accolades=== Brussels International Independent Film Festival Awards

{| class="wikitable" style="font-size:90%"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Awards and Nominations
|- style="text-align:center;"
! style="background:#ccc;"| Award
! style="background:#ccc;"| Category
! style="background:#ccc;"| Nominee
! style="background:#ccc;"| Result
|- Aotearoa Film & Television Awards Outstanding Feature Film Debut Michael Bennett Michael Bennett
| 
|- Best Supporting Actor in a Feature Film Edwin Wright
| 
|- Best Supporting Actress in a Feature Film  Sara Wiseman
|  
|- Best Supporting Actress in a Feature Film  Alix Bushnell
| 
|- Best Original Music in a Feature Film  Don McGlashan
| 
|- Best Production Design in a Feature Film   Miro Harre
| 
|- Brussels International Brussels International Independent Film Festival Awards  Best Director Michael Bennett Michael Bennett
|  
|- Best Actor Iaheto Ah Hi
|  
|- New Zealand Script Writers Awards New Zealand Best Feature Film Script Michael Bennett Michael Bennett & Gavin Strawhan
| 
|}

==References==
 
 

==External links==
*  
*   Website
* 
*   on Twitter
*   on Facebook
*   at Flicks.co.nz
*   at NZ On Screen

 
 
 