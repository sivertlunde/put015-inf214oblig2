Hardwired (film)
 
{{Infobox film
| name           = Hardwired
| image          = DVD cover of Hardwired (film).jpg
| caption        = DVD cover
| director       = Ernie Barbarash
| producer       = Christine Haebler Kirk Shaw
| writer         = Mike Hurst
| starring       = {{plainlist|
* Cuba Gooding Jr.
* Michael Ironside
* Tatiana Maslany
* Juan Riedinger
* Lance Henriksen
* Val Kilmer
}}
| music          = Schaun Tozer
| cinematography = Stephen Jackson
| editing        = Gordon Williams
| studio         = Stage 6 Films
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $5 million  
| gross          =
}}
Hardwired is a 2009 American science fiction film directed by Ernie Barbarash, written by Mike Hurst, and starring Cuba Gooding Jr. and Val Kilmer. The film was released on direct-to-video|direct-to-DVD in the United States on November 3, 2009. 

==Plot== implant a advertisements until hackers led test subject. He agrees to help but is captured during the attempt and taken to a facility controlled by Hope Corp. Luke, with the help of Punk Red and Punk Blue, manages to fight his way to the leader of the project, Virgil Kirkhill. Virgil is killed after a short confrontation, and Luke escapes. He meets up with "Red" and "Blue" (the only remaining members of the underground group). Together they vow to continue the fight until Hope Corporation is destroyed.

==Cast==
* Cuba Gooding Jr. as Luke Gibson
* Val Kilmer as Virgil Kirkhill
* Michael Ironside as Hal
* Tatiana Maslany as Punk Red
* Eric Breker as Robert Drake
* Juan Riedinger as Punk Blue
* Chad Krowchuk as Keyboard
* Terry Chen as Carter Burke
* Hiro Kanagawa as Dr. Steckler
* Rachel Luttrell as Candace
* Donny Lucas as Bennett
* Ali Liebert as Catalina Jones
* Monica Mustelier as Veronica "Ronnie" Gibson
* Julius Chapple as Lewis
* Sunita Prasad as Nurse Price
* Lance Henriksen as Hope

== Production ==
Ironside was originally offered Kilmers role, Virgil Kirkhill, but he declined, as he wanted to stretch his acting beyond playing imposing antagonists. Ironside said that he was originally drawn to the script based on the The Matrix|Matrix-style themes. 

==Reception==
David Nusair of ReelFilm panned the film, stating, "Hardwired inevitably establishes itself as just another sloppy, run-of-the-mill direct-to-video actioner – with Cuba Gooding Jrs presence in the central role ultimately more sad than anything else."  Scifi Movie Page also negatively reviewed the movie, writing that "Hardwired is cheesy, seriously cheesy."   Steve Power of DVD Verdict wrote, "Amazingly, Hardwired isnt completely terrible." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 