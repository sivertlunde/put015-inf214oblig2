Eco-Pirate: The Story of Paul Watson
{{Infobox film
| name           = Eco-Pirate: The Story of Paul Watson
| image          = Eco-Pirate - The Story of Paul Watson poster.png
| image_size     = 200px
| border         = 
| alt            = 
| caption        = 
| director       = Trish Dolman
| producer       = Kevin Eastwood Trish Dolman
| music          = Michael Brook
| editing        = Brendan Woollard
| studio         = Screen Siren Pictures Optic Nerve Films eOne Epix Epix (USA)
| released       =  
| runtime        = 110 min
| country        = Canada
| language       = English
}}
 Hot Docs Documentary Film Festival.

==Synopsis==
The film begins as Watson and members of the Sea Shepherd Conservation Society are arriving in the Southern Ocean Whale Sanctuary on board the RV Farley Mowat, in search of illegal whaling operations. They come upon the Japanese whaling supply vessel Oriental Bluebird and warn her to leave the area, but she refuses. Playing "Ride of the Valkyries" on a loudspeaker, the Farley Mowat then approaches and broadsides the Bluebird using a welded steel blade that protrudes from the hull of the boat—a demonstration of the kind of tactics Watson and the Sea Shepherds are known for. 

The film then goes into an account of Watsons long and controversial history as an activist, going into depth about his role in co-founding Greenpeace, his subsequent disagreements with its other founders, and his separation from the organization to found his own Sea Shepherd Conservation Society. Using interviews with key figures in the environmental movement as well as news footage from the 1970s through the 2000s, the film recounts confrontations with various hunting and fishing bodies over the past 30 years which shaped Watsons methodology as an activist. 

Throughout the film Watson faces problems with the international legal system, aging boats, inexperienced crew, fundraising, and criticism from his colleagues and family.

==Appearances== Patrick Moore, Bob Hunter, Paul Spong, Allison Lance, Lani Lum Watson, Chris Aultman, David Suzuki, Farley Mowat, Peter Garrett, Andrew Darby, Joji Morishita, Pablo Salas, Loyola Hearn, Anthony Kiedis, Pete Bethune, and Terri Irwin. 

==Production==
Eco-Pirate was shot over seven years  in the Antarctic, the Galapagos, North and South America, Australia and New Zealand.  

In an interview with POV Magazine, Trish Dolman reports struggling to obtain fresh-sounding interviews from Watson since he is a consummate media-man and has told his stories so many times, particularly that of the 1975 confrontation with a Soviet whaling fleet and its prey. She says it took some "nudging" to get the vivid account he gives in the film.  
 MV Shonan Maru 2 collided with Sea Shepherds MY Ady Gil, destroying the latter. 
 Telefilm Canada Rogers Documentary Fund.

==Release== Hot Docs Documentary Film Festival in Toronto.  It opened theatrically on July 22, 2011 at the TIFF Bell Lightbox in Toronto, and Fifth Avenue Cinemas in Vancouver.

===Reviews===
Katherine Monk of the Vancouver Sun called Eco-Pirate a "thoroughly thought-provoking and emotionally poignant portrait of a Canadian outlaw" while praising director Dolman for maintaining distance from her subject: "She gets intimate material, but she’s clearly never seduced by Watson’s eco-charms to the point of being putty-headed." 

Robert Bell of exclaim.ca criticized the film by calling it "a dissection of a Canadian figurehead   never finds its flow or footing." 

However Drew Kerr of torontoscreenshots.com called the film "a well-rounded portrayal of the man", acknowledging its portrayal of Watsons "genius" and "oddball charm" while praising its use of interviews which are "highly critical" of him.  

Referring to her balanced approach with the film, Watson was quoted in a Georgia Straight interview: “It’s down the middle. Trish has tried to present the side that opposes me and the side that supports me, and I think she did a good job on it.” 

The Globe and Mail praised the film as a "refreshingly rounded picture of a modern real-life action hero." 

===Awards===
Eco-Pirate was nominated for Best Documentary at the 2011 Warsaw International Film Festival, it won the RIFF Environmental Award at the 2011 Reykjavík International Film Festival, and it won Best Documentary at the 2011   where Watson himself  .  

At the 2012 Leo Awards it was nominated for Best Direction in a Documentary Program or Series (Trish Dolman), Best Documentary (Trish Dolman and Kevin Eastwood, producers), Best Musical Score in a Documentary Program or Series (Michael Brook) and Best Picture Editing in a Documentary Program or Series (Brendan Woollard). 

== External links ==
*  
*  

==References==
 

*
*
*
*

 

 