Bataille de boules de neige
{{Infobox film
| name           = Bataille de boules de neige
| image          =
| caption        =
| director       = Louis Lumière
| producer       = Louis Lumière
| writer         =
| starring       =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = France Silent
| budget         =
| gross          =
}}
 short black-and-white silent documentary Louis Lumière.

==Plot==
The camera is centred on a pathway made through a snow covered city street. On both side of the pathway, several men and women are engaged in a snowball fight.  A cyclist comes forward upon the path towards the fight, and is hit by a couple snowballs as he approaches. He continues riding towards the snowball-armed melee and is struck successively by several nearby participants as he comes between them, losing control of his bicycle and falling to the ground. His cap is flung onto the pathway. One male participant in the engagement grabs a hold of the cyclists bicycle and lifts it off the ground, and the fallen cyclist scrambles to his feet and yanks his bicycle away from the participant. After retrieving possession of his bicycle, the cyclist gets atop and rides away from the fight in the same direction he came from. He leaves his cap behind at the scene of his fall. 

==Production== 35 mm aspect ratio of 1.33:1.

==Current status==
Given its age, the copyright on this short film has now expired. It is featured in a number of film collections including The Movies Begin - A Treasury of Early Cinema, 1894-1913. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 


 
 