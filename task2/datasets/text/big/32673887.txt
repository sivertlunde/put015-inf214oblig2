What Now, Catherine Curtis?
{{Infobox film
| name           = What Now, Catherine Curtis?
| image          =
| caption        =
| director       = Charles Walters
| producer       = Gary Morton
| writers        = Sheldon Keller Lynn Roth
| starring       = Lucille Ball Joseph Bologna Art Carney
| music          = Nelson Riddle
| editing        = John Foley
| distributor    = Lucille Ball Productions
| released       =  
| runtime        =
| country        = United States English
| budget         =
}}

What Now, Catherine Curtis? is a 1976 American TV Film, starring Lucille Ball as Catherine Curtis, a middle-aged divorcee who holds on to life after a break in 23 years of marriage. Its broken down into three parts, titled "First Night," "First Affair" and "First Love."

== Cast ==
* Lucille Ball as Catherine Curtis
* Joseph Bologna as Peter
* Art Carney as Mr. Slaney

== Public reception ==
This special was the #16 show for the week and won its timeslot with a 22.6 rating and 36% share of the audience.

== References ==
 

== External links ==
*   on the Internet Movie Database

 
 


 