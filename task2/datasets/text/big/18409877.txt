A String of Pearls (film)
 
{{Infobox film
| name           = A String of Pearls
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         = Bernardine R. Leist
| starring       = Blanche Sweet Dorothy Bernard
| music          =
| cinematography = G. W. Bitzer
| editing        =
| distributor    =
| released       =  
| runtime        = 17 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 short silent silent drama film directed by D. W. Griffith and starring Blanche Sweet.   

==Cast==
* Dorothy Bernard - The Poor Woman Charles West - The Poor Womans Brother (as Charles H. West)
* Kate Bruce - The Poor Womans Mother
* Blanche Sweet - The Second Woman
* William J. Butler - The Second Womans Father
* Adolph Lestina - The Doctor
* Mack Sennett - The Musician
* Charles Hill Mailes - The Rich Doctor
* Dell Henderson - The Millionaire
* Grace Henderson - The Millionaires Wife

==See also==
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 