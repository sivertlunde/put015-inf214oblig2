Ishq Junoon

{{Infobox film
| name           = Ishq Junoon
| image          =
| alt            =  
| caption        =
| director       = Sanjay Sharma
| producer       = Anuj Sharma
| writer         = 
| starring       = 
| music          = Sonu Nigam   Jeet Gannguli   Ankit Tiwari   Anjjan Bhattacharya
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}


Ishq Junoon is a film being produced by Anuj Sharmas Shantketan Entertainments in association with Vinr Films and directed by Sanjay Sharma.  The film is a romantic thriller. The Sharma brothers duo recently launched the film at a suburban studio where they recorded the first song, Tu Hi Mera Rab Hai. 

== Music ==
The makers have roped in four composers — Sonu Nigam, Jeet Gannguli, Ankit Tiwari and Aanjan Bhattacharya — for the films music.  

==Soundtrack==
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
| Track # || Title || Singer(s) || Lyrics || Composer(s)
|- Sonu Nigam Sonu Nigam
|- Tu Hi Ankit Tiwari||????? Ankit Tiwari 
|- Phir Tu Mohit Chauhan||????? Anjjan Bhattacharya 
|- Dance Club Anjjan Bhattacharya 
|- Jeet Gannguli
|}

==References==
{{reflist|refs=

 {{cite news
|url=http://www.bharatcinetvwriters.org/entries/general/-anuj-sharma-take-off-with-romantic-musical-thriller-ishq-junoon-passion-of-love-
|title=Anuj Sharma take off with Romantic Musical Thriller.
|work=Bharat Cine Tv Writers
|accessdate=}} 

 {{cite news
|url=http://timesofindia.indiatimes.com/entertainment/hindi/music/news/The-Sharmas-rope-in-four-composers-for-new-film/articleshow/46043488.cms
|title=The Sharmas rope in four composers for new film.
|work=Times of India
|accessdate=}} 

 {{cite news
|url=http://www.filmsofindia.com/news/4903-anuj-sharma-takes-off-with-romantic-musical-thriller-ishq-junoon-the-passion-of-love
|title=The Sharmas rope in four composers for new film.
|work=Films of India
|accessdate=}} 

 {{cite news
|url=http://indianexpress.com/article/entertainment/screen/rewriting-history-4/
|title=Rewriting history: Anjjan Bhattacharya
|work=Indian Express
|accessdate=}} 

}}

 
 
 