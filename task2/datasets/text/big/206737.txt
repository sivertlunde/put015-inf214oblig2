Deliverance (1919 film)
{{Infobox film
| name           = Deliverance
| image          =Deliverance (1919) - Keller.jpg
| image_size     =200px
| caption        =film still with Helen Keller and her brother and mother
| director       = George Foster Platt
| producer       = Helen Keller Film Corporation
| writer         = Francis Trevelyan Miller
| narrator       =
| starring       = Etna Ross Ann Mason Tula Belle Edith Lyle  Betty Schade Jenny Lind
| music          = Anselm Goetzl
| cinematography = Lawrence Fowler Arthur L. Todd
| editing        =
| distributor    = George Kleine System
| released       =  
| runtime        = 90 min
| country        = United States
| language       = Silent
}}

Deliverance is a 1919 silent film which tells the story of the life of Helen Keller and her teacher, Annie Sullivan. It stars Etna Ross, Tula Belle, Edith Lyle, Betty Schade, Sarah Lind, Ann Mason and Jenny Lind. The film also features appearances by Helen Keller, Anne Sullivan, Kate Adams Keller and Phillips Brooks Keller as themselves. The movie was directed by George Foster Platt and written by Francis Trevelyan Miller.

==Cast (in credits order)==
*Etna Ross	... 	Young Helen Keller
*Tula Belle	... 	Young Nadja
*Edith Lyle	... 	Younger Anne Sullivan (as Edythe Lyle)
*Betty Schade	... 	Mrs. Kate Adams Keller as a young woman
*Jenny Lind	... 	Martha Washington
*Sarah Lind	... 	Mammy
*Ann Mason	... 	Helen Keller as a young woman
*Helen Keller	... 	Herself
*Anne Sullivan	... 	Herself
*Kate Adams Keller	... 	Herself
*Phillips Brooks Keller	... 	Himself
*Polly Thompson	        ... 	Helens secretary
*Ardita Mellinina	... 	Nadja
*J. Parks Jones  	... 	Nadjas son
*True Boardman	... 	Helens boy friend (uncredited)
*Herbert Heyes      ...     Ulysses   at silentera.com  (uncredited)

==Preservation status==
A copy of the film is in the Library of Congress film archive. 

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 


 
 