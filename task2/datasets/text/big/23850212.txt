Triangle (2009 British film)
 
 
{{Infobox film
| name           = Triangle
| image          = Triangle (Christopher Smith).jpg
| caption        = Theatrical poster Christopher Smith
| producer       = {{plainlist|
* Jason Newmark
* Julie Baines
* Chris Brown
}}
| writer         = Christopher Smith
| starring       = {{plainlist|
* Melissa George
* Michael Dorman
* Rachael Carpani
* Henry Nixon
* Emma Lung
* Liam Hemsworth
}}
| music          = Christian Henson
| cinematography = Robert Humphreys
| editing        = Stuart Gazzard
| studio         = {{plainlist|
* UK Film Council
* Dan Films (UK)
}} Icon Film Distribution   
| released       =  
| runtime        = 99 minutes 
| country        = United Kingdom Australia
| language       = English
| budget         = $12 million   
| gross          = $1.3–1.6 million    
}} Christopher Smith, and starring Melissa George and Michael Dorman. The film was released in the United Kingdom on 16&nbsp;October 2009.     George plays a single mother who goes on a boating trip with several friends.  When they are forced to abandon their ship, they board a derelict ocean liner, where they become convinced that someone is stalking them.

== Plot == autistic son Tommy on a boat trip with her friend Greg. While Jess tidies up the house, the doorbell rings, but  no one is there.

Jess later arrives at the harbor alone and boards Gregs boat. Joining them for the trip are Victor, a runaway teen who lives with Greg; Sally and Downey, married friends of Greg; and Heather, a friend of Sallys. While on the boat out at sea, they are overwhelmed by a storm and Heather is swept out into the water when the boat capsizes. When the storm clears, the others climb onto the upturned boat.

When an ocean liner arrives, the group board it, only to find it apparently deserted, though Jess experiences a sense of déjà vu as they explore. Various strange occurrences take place on the ship, hinting that there is someone else on board, though the person remains unseen. Jess eventually witnesses the others die one by one: Victor from a wound in his head, while Greg, Sally and Downey are shot by a burlap-masked shooter. The shooter chases Jess to the ships front deck, but she fights back; she disarms and sends her assailant overboard.
 causality loop, whereby she is the unseen figure of earlier. Jess attempts to warn Victor, only to accidentally impale his head on a wall hook. She flees deeper into the ship and finds evidence that the loop has repeated many times before. Jess decides that she can change the way events play out, but another counterpart of hers from an earlier loop is still on board the ship and kills the others one by one. This older counterpart is then killed and thrown overboard by Jess newer counterpart, and the loop repeats again.

Gregs upturned boat and its survivors return, and Jess realizes the time loop restarts when everyone is killed. Desperate to stop the loop and prevent them from boarding at all, Jess sets everything from the first loop into motion, and she herself becomes the shooter. When she is disarmed, she urges her counterpart to kill everyone when they return, before falling off the ship.

Jess awakens washed ashore and returns home, only to find herself returned to earlier that morning. As she watches from outside, it is revealed that Jess abuses Tommy out of anger toward his autism. Promising to stop the abuse, Jess distracts her counterpart with the doorbell and kills her in order to take her place. Jess then leaves with Tommy in her car, only to find evidence that this sequence of events has happened many times before as well. Their car is hit by a truck and Tommy is killed, leaving Jess alone. A taxi driver approaches and Jess accepts a ride to the harbor, where she joins the others on Gregs boat, setting the events of the loop in motion again.

== Cast ==
* Melissa George as Jess
* Michael Dorman as Greg
* Rachael Carpani as Sally
* Henry Nixon as Downey
* Emma Lung as Heather
* Liam Hemsworth as Victor
* Joshua McIvor as Tommy

== Production == Christopher Smith. The UK Film Council awarded 1.6 million pounds ($2.8 million) of public money from the National Lottery fund towards the development, production and distribution of the film.   Smith was inspired by Dead of Night and Memento (film)|Memento. He wanted to make a circular film that explored déjà vu that avoided using the same elements of Jacobs Ladder (film)|Jacobs Ladder. The scenes were split between a real liner that they built off of Australia and sets. Smith fought to build the liner, as he believed it was important that they avoid shooting everything with green screens.   The film is based in part on the story of Sisyphus, a figure in Greek mythology. 

== Release ==
The film premiered in the UK at the London FrightFest Film Festival on 27 August 2009. Triangle was theatrically released on 16 October 2009, in the UK;  30 December 2009 in Belgium;  21 January 2010 in the Netherlands. 

Triangle grossed $894,985 in its native UK and $1,303,598 total worldwide.   It did not receive a theatrical release in the US.

=== Home media === Icon Home Entertainment distributed Triangle on DVD and Blu-ray Disc|Blu-ray in the UK, with a release date of 1 March 2010,  whilst First Look Studios distributed the title on both DVD and Blu-ray Disc with a release date of 2 February 2010. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that the film received positive reviews from 82% of 38 surveyed critics; the average rating was 6.6/10.  Empire (magazine)|Empire gave the film a 4/5 stars rating and called it a "satisfying mind-twister, with an unexpectedly poignant pay-off."  Variety (magazine)|Variety said that Triangle only makes some kind of sense on its own fantastic level. {{cite web|work=Variety (magazine)|Variety|title=Triangle date = Time Out London reviewer Nigel Floyd praised Melissa Georges "fearless, credible performance" that "grounds the madness in a moving emotional reality."  The Guardian critic, Philip French compared it to a "Möbius strip" in which the viewer "wonders how Smith will keep things going" and added the viewer will "leave his picture suitably shaken."  Fellow Guardian critic Peter Bradshaw wrote that Triangle is a "smart, interestingly constructed scary movie", complimenting Smith for "creating some real shivers."   Entertainment.ies Mike Sheridan was less impressed. Although he praised Georges acting, he wrote that her performance "cant shield the fact that this still an exceptionally non-scary horror, that will have you scratching your head more than jumping out of your seat," ultimately rating it 2/5 stars.   The Scotsman called it "a trickily plotted and slickly made effort that nevertheless cant quite make its premise fly in gripping enough fashion." 

== See also ==
* List of films featuring time loops

== References ==
 

== External links == Icon Entertainment International
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 