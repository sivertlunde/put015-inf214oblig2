De Flat
 
{{Infobox film
| name           = De Flat
| image          = 
| image_size     = 
| caption        = 
| director       = Ben Verbong
| producer       =  Jean van de Velde
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = April 21, 1994
| runtime        = 
| country        = Netherlands
| language       = Dutch
| budget         = 
}} 1994 Netherlands|Dutch mystery film directed by Ben Verbong.

De Flat (also known as House Call) is a R-Rated film for nudity and strong sexuality scenes.

==Plot summary==
Roos Hartman is a young doctor who lives with her son in a large apartment complex. When a fellow tenant is brutally murdered, the police and Hartmans friends suspect her mysterious neighbour, Eric Coenen. As she becomes romantically involved with Coenen, she doubts he would commit such a crime, but soon she begins to investigate the case further and discovers some startling facts relating to his involvement...

==Cast==
*Renée Soutendijk	... 	Roos Hartman
*Victor Löw	... 	Eric Coenen
*Hans Hoes	... 	Jacques Posthuma
*Jaimy Siebel	... 	Davy
*Mirjam de Rooij	... 	Lidy van Oosterom
*Leslie de Gruyter	... 	Hennie van Oosterom
*Guy Sonnen	... 	Cees den Boer
*Huib Rooymans	... 	Erwin Nijkamp
*Miguel Stigter	... 	Marcel van der Kooy
*Jacques Commandeur	... 	Charles Uffingh
*Maud Hempel	... 	Nel van Lier
*Ann Hasekamp	... 	Mrs. Veenstra
*Peter Smits	... 	Officer
*Jaap Maarleveld	... 	Carel Wijnsma

== External links ==
*  

 
 
 
 
 
 


 
 