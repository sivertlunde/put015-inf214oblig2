Itha Oru Dhikkari
{{Infobox film 
| name           = Itha Oru Dhikkari
| image          =
| caption        =
| director       = NP Suresh
| producer       = Purushan Alappuzha
| writer         = Purushan Alappuzha Alappuzha Karthikeyan (dialogues)
| screenplay     = Purushan Alappuzha
| starring       = Prem Nazir Jayabharathi Srividya Sukumaran
| music          = A. T. Ummer
| cinematography = PN Sundaram
| editing        = NP Suresh
| studio         = Sreedevi Movies
| distributor    = Sreedevi Movies
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by NP Suresh and produced by Purushan Alappuzha. The film stars Prem Nazir, Jayabharathi, Srividya and Sukumaran in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Prem Nazir
*Jayabharathi
*Srividya
*Sukumaran
*Balan K Nair
*MG Soman
*Mala Aravindan
*Sathyakala

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ariyaathe Ariyaathe Anuraaga  || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Ente Janmam Neeyeduthu || K. J. Yesudas, S Janaki || Poovachal Khader || 
|-
| 3 || Meghangal || K. J. Yesudas, S Janaki || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 