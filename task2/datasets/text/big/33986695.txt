The Tragedy of Deaf Sam-yong
 
{{Infobox film
| name           = The Tragedy of Deaf Sam-yong
| image          = 
| film name =  
 | hanja          =  의 벙어리  
 | rr             = Biryeonui beongeori Samyong
 | mr             = Piryŏn ŭi Pŏng-ŏri Samnyong}}
| caption        = 
| director       = Byeon Jang-ho
| producer       = 
| writer         = Kim Kang-yun Na Do-hyang
| starring       = Kim Hui-ra
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = South Korea
| language       = Korean
| budget         = 
}} Best Foreign Language Film at the 46th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Kim Hui-ra
* Yun Yeong-kyeong
* Shin Yeong-il
* Choi In-suk

==See also==
* List of submissions to the 46th Academy Awards for Best Foreign Language Film
* List of South Korean submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 