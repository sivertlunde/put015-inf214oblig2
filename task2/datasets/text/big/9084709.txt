Yours for the Asking
{{Infobox film
| name           = Yours for the Asking
| image          =
| image_size     =
| caption        =
| director       = Alexander Hall James Dugan (assistant)
| producer       = Lewis E. Gensler
| writer         = Eve Greene William R. Lipman (story) Philip MacDonald Harlan Ware William H. Wright (story)
| narrator       =
| starring       = George Raft Dolores Costello Ida Lupino
| music          =
| cinematography = Theodor Sparkuhl
| editing        =
| distributor    = Paramount Pictures
| released       = August 19, 1936
| runtime        =
| country        =
| language       =
| budget         =
| preceded_by    =
| followed_by    =
}}
 1936 film starring George Raft as a casino owner and Dolores Costello as the socialite he hires as hostess.  The movie also features Ida Lupino and was directed by Alexander Hall. 

==Cast==
* George Raft as Johnny Lamb
* Dolores Costello as Lucille Sutton
* Ida Lupino as Gert Malloy
* Reginald Owen as Dictionary McKinney
* James Gleason as Saratoga
* Edgar Kennedy as Bicarbonate
* Lynne Overman as Honeysuckle
* Groucho Marx as Sunbather (uncredited)
* Dennis OKeefe as Man (uncredited)
* Charles Ruggles as Sunbather (uncredited)
* Ellen Drew as Girl (uncredited)
* Thomas A. Curran (uncredited)

==Production==
Groucho Marx, Charles Ruggles and Tookie Spreckles appeared as extras when the film shot some footage on Coronado Island. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 71 

==Reception==
The film made a small profit. 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 