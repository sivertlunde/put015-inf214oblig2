Crashout
{{Infobox film
| name           = Crashout
| image          = Crashout film poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Lewis R. Foster 
| producer       = Hal E. Chester
| screenplay     = Hal E. Chester Lewis R. Foster
| narrator       =  Arthur Kennedy Luther Adler
| music          = Leith Stevens
| cinematography = Russell Metty
| editing        = Robert Swink
| studio         = 
| distributor    = The Filmakers Inc.
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Arthur Kennedy, William Talman. 

==Plot==
Convict Van Duff is the leader of a large-scale prison break.  The breakout works as the six survivors hide out in a forgotten mine working near the prison.

Once the coast is clear they then set out on a long, dangerous journey.  The convicts take by foot, car, train and truck in an attempt to get to some hidden stolen bank loot.  On the journey the doomed prisoners meet with some locals including a farm woman (Beverly Michaels), a kidnapped doctor (Percy Helton) and a young woman on a train (Gloria Talbott).

==Cast==
* William Bendix as Van Morgan Duff Arthur Kennedy as Joe Quinn
* Luther Adler as Pete Mendoza William Talman as Luther Remsen 
* Gene Evans as Maynard Monk Collins
* Marshall Thompson as Billy Lang
* Beverly Michaels as Alice Mosher

==References==
 

==External links==
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images)
*  

 

 
 
 
 
 
 
 
 


 