Tapped (film)
{{Infobox film
| name = Tapped
| image =
| director = Stephanie Soechtig,  Jason Lindsey
| producer =
| writer =
| music =
| distributor =
| studio =
| released =  
| runtime =
| country = United States
| language = English
}} ocean pollution "kept leading them to bottled water". 

==Synopsis== economically and Ecology|ecologically.  The filmmakers focused on industry giants such as PepsiCo and Nestlé Waters, visiting a town containing a Nestlé factory as well as running tests on the bottles the company uses for its products. Their results came back showing "several potentially harmful chemicals, some known carcinogens".   CT Post  The documentary also focused on the amount of bottles that are recycled, noting that "Forty percent of bottled water is really just filtered tap water, and every day we throw away 30 million single-served bottles of water. 

==Reception== Grist wrote "A few too many mid-interview cutaways to Soechtig looking concerned came off as a little journalistically self-important, but Tapped does a solid job of covering every aspect of this damaging industry and inspiring more outrage than despair."  The Los Angeles Times praised the film, stating that the film was "persuasive" and a "compact, clear-headed documentary".  NPR|NPRs Glenn McDonald praised the film but criticized some of the editing done to "ridicule industry spokespeople", stating that it was unnecessary as the films content "sells itself". 

===Response by bottling companies===
Nestlé CEO Kim Jeffery responded to several of the questions brought up by the film, stating that the bottles used for the products were safe and that one of the chemicals discovered in the tests, Bisphenol-A was "in the liners of all canned foods to prevent botulism, and in the DVDs of the documentary that people were able to purchase". 

==Awards and accolades==
* Anchorage International Film Festival 2009 - Best Documentary 
* Eugene International Film Festival 2009 - Best Documentary 

==References==
 

==External links==
*  
*  
*  


 

 

 
 
 
 