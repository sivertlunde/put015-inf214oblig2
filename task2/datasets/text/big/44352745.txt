So This Is College
{{Infobox film
| name           = So This Is College
| image          = So This Is College poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Sam Wood
| producer       = Sam Wood 
| screenplay     = Al Boasberg Delmer Daves Joseph Farnham Robert Montgomery Sally Starr Phyllis Crane
| music          =  Leonard Smith Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Robert Montgomery, Sally Starr and Phyllis Crane. The film was released on November 8, 1929, by Metro-Goldwyn-Mayer.  

==Plot==
Biff and Eddie, two college classmates at the University of Southern California, are life-long friends, fraternity brothers, and members of the football team. Although they make a vow at the beginning of their senior year that they will no longer allow their pursuit of girls to get into the way of they friendship, they soon break their vow when both fall for pretty Babs Baxter, a popular co-ed. Vying for her affections, Biff and Eddie play pranks on each other that soon causes a serious breach in their relationship. When Biff finally realizes that Eddie wants to marry Babs, he decides to step aside for the sake of their friendship, but at the seasons big football game, both realize that Babs has merely been toying with them she introduces them to her fiancé Bruce. After winning the game, Biff and Eddie decide never again to let girls come between them--until they see another pretty girl in the park.

== Cast ==	 
*Elliott Nugent as Eddie Robert Montgomery as Biff
*Cliff Edwards as Windy Sally Starr as Babs
*Phyllis Crane as Betty
*Dorothy Dehn as Jane
*Max Davidson as Moe
*Ann Brody as Momma
*Oscar Rudolph as Freshie
*Gene Stone as Stupid
*Polly Moran as Polly
*Lee Shumway as Coach

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 