Liberty Kid
{{Infobox film
| name           = Liberty Kid
| image          = 
| alt            = 
| caption        = 
| director       = Ilya Chaiken
| producer       = Larry Fessenden Roger Kass Mike King Mike S. Ryan Claude Wasserstein
| writer         = Ilya Chaiken Al Thompson Kareem Savinon Anny Mariano Rayniel Rufino
| music          = Jeff Grace
| cinematography = Eliot Rockett
| editing        = Dave Rock
| studio         = Glass Eye Pix
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} American film that is directed by Ilya Chaiken.

==Plot summary==
Two friends Derrick and Tico lost their jobs at a concession stand at the Statue of Liberty because of 9/11. In order to make money, they become drug dealers and participate in insurance scams. Derrick wants to go to college and has to support his two kids. When recruiters from the army come, Derrick decides to join the army    because he is told that hell get money for college and live rent free. When he tells his mom his decision, she says that she is afraid that hell have to go to war. The recruiter tells him that his mom is only worried because it is her job as a mom.   

==Reception==
The film got 87% on Rotten Tomatoes out of 15 reviews. 

Stephen Farber, of The Hollywood Reporter, said that even though "Liberty Kid" is a small film, much of it is deeply poignant; it enhances our compassion for all the ghosts of Sept. 11. Its cautiously optimistic conclusion also strikes a welcome note without falling into sentimentality.  Bilge Ebiri, of The Nerve, said that a lesser director would have played this story for cheap emotions. But to her eternal credit, Chaiken keeps her movie grounded in her characters, allowing Thompson and Savinons true-to-life performances to carry us through what is, on paper, an elaborate plot.  Don Willmot, of Film Critic, said that Thompson and Savinon are a terrific pair and deserve big parts in bigger films. 

==Awards==
The film won Best Film at the New York Latino Film Festival.  The film won Critics Pick from both The New York Times  and New York Magazine. 

==Film festivals==
The film played at New York Latino Film Festival,  the Woodstock Film Festival,  and the Atlanta Film Festival. 

==DVD release==
The DVD has an audio commentary by Chaiken, Thompson and Saviñon, deleted scenes, behind the scenes featurette, photo gallery, and conversations with Iraq War veterans as special features. The DVD is in 5.1 stereo sound. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 