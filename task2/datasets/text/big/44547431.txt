Blue Gold: American Jeans
 
 
{{Infobox film
| name           = Blue Gold: American Jeans
| image          = BlueGoldWiki.jpg
| alt            =
| caption        =
| director       = Christian D. Bruun
| producer       = Christian D. Bruun Mark A. Romeo Theis Jessen
| writer         = Christian D. Bruun, John H. Marks
| narrator       = Edward Burns
| music          = P.T. Walkley
| cinematography = Christian D. Bruun
| editing        = Jason Watkins
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Blue Gold: American Gold is a 2014 American documentary film directed and written by Christian D. Bruun and narrated by Edward Burns. The film is weaving together the story of how one unlikely garment ended up connecting us all. Starting in the American West the film travels around the world following jeans hunter and vintage jeans expert, Eric Schrader. An ambassador of Americana, Eric is trading in the history, myth, and intrinsic values that have made blue jeans one of the most expensive and fetishised piece of vintage clothing on the planet. From fashion history and subculture aspiration, to the lost tradition of American manufacturing, Blue Gold explores Americana in a globalized world, where cultural exchange and greater transparency demand social responsibility and inspire innovation.

==Production==
The film was made over five years, filming throughout the United States, Europe, the Middle East, Asia, and Japan. The film mixes various video formats and filming techniques. Some interviews were conducted remotely over Skype with a local crew present and one scene directly recorded via webcam.

==Featuring== Elio Fiorucci, Diane Gilman and many more.

==Original score==
The film has an original score by New York artist P.T. Walkley with additional songs by P.T.Walkley. It also features a track performed by Last Man Standing (written by Sean Pinkley).

==Release==
The documentary opened at the Berkshire International Film Festival on May 31, 2014. It has since screened at the Hollywood Film Festival and Starz Denver Film Festival. Blue Gold: American Jeans is scheduled for theatrical release in 2015.

==External links==
*  
*  
*  
*  

 
 
 
 
 