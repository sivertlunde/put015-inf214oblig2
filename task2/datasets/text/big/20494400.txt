Lord Edgware Dies (film)
{{Infobox film
| name           = Lord Edgware Dies
| image          = 
| image_size     = 
| caption        =  Henry Edwards
| producer       = Julius Hagen
| writer         = Agatha Christie (novel) H. Fowler Mear Richard Cooper John Turnbull
| music          = 
| cinematography = Sydney Blythe
| editing        = 
| studio         = Twickenham Film Studios
| distributor    = Radio Pictures
| released       = August 1934
| runtime        = 80 minutes
| country        = United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1934 Cinema British mystery Henry Edwards Richard Cooper.  The film was based on the Agatha Christie novel Lord Edgware Dies, first published in 1933.
 Black Coffee, both released in 1931. Like them, it was filmed at Twickenham Film Studios.

==Cast==
* Austin Trevor - Hercule Poirot Jane Carr - Lady Edgware Richard Cooper - Captain Hastings John Turnbull - Inspector Japp
* Michael Shepley - Captain Roland Marsh
* Leslie Perrins - Bryan Martin
* C.V. France - Lord Edgware
* Esme Percy - Duke of Merton

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 


 