Pink Tush Girl
{{Infobox film
| name = Pink Tush Girl
| image = Pink Tush Girl.jpg
| image_size = 
| caption = Theatrical poster 
| director = Kōyū Ohara   
| producer = 
| writer = Narito Kaneko (screenplay) Osamu Hashimoto (novel)
| narrator =  Ako Yūko Katagiri
| music = Daikō Nagato
| cinematography = Masaru Mori
| editing = Atsushi Nabeshima
| distributor = Nikkatsu
| released =  
| runtime = 87 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}
 Roman porno Ako and Yūko Katagiri. It is based on an award-winning novel by Osamu Hashimoto. 

==Synopsis==
An extroverted high school girl befriends an introverted female classmate, and the two begin exploring their curiosity about sex together. They begin working as prostitutes, but their friendship is tested when they both fall in love with the same man. The film ends on an upbeat note with the two girls reconciling their friendship.    Lewis, Graham R. "The Films of Koyu Ohara" in Asian Cult Cinema, #27 (2nd quarter 2000), p.26. 

==Cast==
* Kahori Takeda ( ) as Rena Sakakibara  Ako ( ) as Yuko Taguchi
* Atsushi Takahashi ( ) as Genichi Kikawada
* Yūji Nogami ( ) as Takinoue
* Akio Kuwasaki ( ) as Yamashina
* Asami Morikawa ( ) as Ryōko Matsuzaki
* Kunio Shimizu ( ) as Tōru Kinoshita
* Risato Sasaki ( )	
* Yūko Katagiri as Tokie Yamada
* Yûya Uchida ( ) as Senichi Yamada
* Ushi Tōyama ( ) as Ken Ishida
* Satoko Ōtake ( ) as High school girl
* Nobue Ichitani ( ) as Nobue Sakakibara
* Etsuko Seki ( ) as Hiroko Taguchi
* Shirô Kishibe ( ) as Mastumoto
* Osamu Hashimoto ( ) as Shop owner

==Critical appraisal==
In their Japanese Cinema Encyclopedia: The Sex Films, the Weissers note that with the Pink Tush Girl trilogy, Kōyū Ohara reversed the style of his earlier dark films such as the True Story of a Woman in Prison trilogy (1975-1977). In the Pink Tush Girl films Ohara depicts, "an erotic world from pop music and high school tribulations". Unlike most films in the Roman Porno series, the films were popular with both men and women. The Weissers attribute this partly to the on-screen relationship between the two lead performers, Kahori Takeda and Ako. 

The director noted that this upbeat, breezy take on the sex lives of two female characters was at odds with the way that women were depicted in the pink film at the time. Ohara was enthusiastic about this aspect of the project, and because of this, caused the film to go over-budget. He remembered, "I was scolded severely by the Nikkatsu management", but he felt vindicated when the films popularity caused the studio to ask him to make sequels. 

In his survey of the films of Kōyū Ohara, Graham Lewis writes that though Pink Hip Girl is less serious in tone than Oharas earlier films, it is no less well-made and thoroughly enjoyable. He notes that Oharas nickname, "King of Pink-Pop" derives largely from his work in this film and its sequels. Lewis concludes, "If Oharas other films sound too rough for you, this might be your cup of tea". 

Jasper Sharp judges Pink Tush Girl to be arguably Oharas best film. He writes that the films road movie structure, catchy theme song, and longer running length make it a more fully rounded cinematic experience than many contemporary Nikkatsu Roman Pornos. He speculates that, because of the wider popularity of this film, Nikkatsu would have been wise to pursue the lighter tone of this film rather than the dark S&M films that they concentrated on producing at the time.   

==Availability==
Pink Tush Girl was released theatrically in Japan on April 29, 1978.  It was released on DVD in Japan on DVD December 22, 2005, as part of Geneon Universal Entertainment|Geneons second wave of Nikkatsu Roman porno series.  

==References==
;Notes
 

;Bibliography
;;English
*   
*  
*  
*  

;;Japanese
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 