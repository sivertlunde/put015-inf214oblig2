Mister Ten Per Cent
{{Infobox Film
| name           = Mister Ten Per Cent
| image          = 
| image_size     = 
| caption        = 
| director       = Peter Graham Scott
| producer       = W.A. Whittaker
| writer         = Mira Avrech Charlie Drake Norman Hudis Lew Schwarz
| starring       = Charlie Drake
| music          = Ron Goodwin Gerald Gibbs Jack Harris
| studio         = Associated British Picture Corporation (ABPC)
| distributor    = Warner-Pathé
| released       =  1967
| runtime        = 84 mins
| country        = United Kingdom
| language       = English
| preceded_by = 
| followed_by = 
}} British comedy film, directed by Peter Graham Scott and starring Charlie Drake. 

==Plot==
Percy Pointer, a construction worker and amateur dramatist, writes a drama Oh My Lord and hopes to have it professionally produced. A dishonest producer agrees to back the play, hoping that it will be a disaster, so that he can claim insurance on its failure. To Percys distress, the first audience see the play as a slapstick comedy, not the drama he intended it to be. 

The play is a hit and audiences love it. But Percy is upset by the turn of events and attempts to ruin the production. It then emerges that in his ignorance of showbusiness contracts, he has signed away 10% of any revenue to so many people that he actually owes 110% of the money.

His attempts to sabotage the production lead to his being banned from the theatre. But with great resourcefulness, he manages to enter the theatre backstage and create havoc. With the audience thinking this is a part of the comedy and hugely enjoying it, Percy takes to the stage and addresses the audience, asking them why they find his drama so funny. No-one can find an answer, but they cheer him anyway. 
 A Night at the Opera (1935).
 The Producers by a year...", drawing attention to the uncanny resemblance between the plots of the two films.

==Cast==
* Charlie Drake as Percy Pointer
* Derek Nimmo as Tony
* Wanda Ventham as Kathy
* John Le Mesurier as Jocelyn Macauley Anthony Nicholls as Casey
* Noel Dyson as Mrs. Gorman
* John Hewer as Townsend Anthony Gardner as Claude Crepe
* Ronald Radd as Publicist
* John Laurie as Scotsman Colin Douglas as Policeman
* Annette Andre as Muriel
* Justine Lord as Lady Dorothea George Baker as Lord Edward
* Joyce Blair as Lady Dorothea
* Una Stubbs as Lady Dorothea
* Lynn Ashby as The Maid
* Nicole Shelby as Fiona
* Gina Warwick as Ellen Percy Herbert as Inspector Great
* Desmond Roberts as Manservant

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 