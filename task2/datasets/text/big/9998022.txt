La Mary
{{Infobox film
| name           = La Mary
| image          = La Mary poster.jpg
| image_size     = 220px
| border         = yes
| alt            =
| caption        = Theatrical release poster
| director       = Daniel Tinayre
| producer       = Ricardo Tomaszewski Guillermo Cervantes Luro
| screenplay     = José Martínez Suárez Augusto R. Giustozzi
| based on       =  
| starring       = Susana Giménez Carlos Monzón
| music          = Luis María Serra
| cinematography = Miguel Rodríguez
| editing        = Antonio Ripoll
| studio         = Globus-Baires
| distributor    = Producciones Del Plata   3C Films Group  
| released       =   
| runtime        = 107 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          = 
}}

La Mary is a 1974 Argentine Romance film|romantic-drama film directed by Daniel Tinayre starring Susana Giménez and Carlos Monzón. The screenplay is by José Martínez Suárez and Augusto R. Giustozzi, based on the 1965 short story of the same name by Emilio Perina, included in Historias apasionadas: La Mary y El Fiscal and later released as a novel with the release of the film.

The film is set in the 1940s and centers on Mary (Giménez), a young girl from a working class neighbourhood in Buenos Aires who has the reputation of being able to predict the future. She marries Cholo (Monzón), and their passionate relationship is marked by the confrontation of Marys chastity and Cholos sexual desire. Tension arises when Mary begins to predict a series of deaths.
 Triple A threatened to kill the entire cast, accusing them of "offense against morality and decency."  The film is also notable for the development of a romantic relationship between Giménez and Monzón during its production, a relationship that would be turbulent and attracted widespread media attention in the years to follow.   

It is now considered a cult film and a classic of Argentine cinema.    In 2014, for the 40 year anniversary of its release, the film was remastered and re-released, declared "of cultural interest" by the city of Buenos Aires and the subject of an exhibition at the Pablo C. Ducrós Hicken Museum of Cinema.  

== Plot ==
By 1930, Evaristo, a humble worker who lives near the Riachuelo river in Buenos Aires, is preparing to go to work with two partners. His daughter Mary calls him from her bed; the girl has a fever and this forces Evaristo back. The tram that the three workers missed falls into the river and almost all the passengers die. For this and other episodes, Mary earns the reputation that she can predict the future.

By 1940, Mary is now a very happy but very chaste young woman. One afternoon, in a bus, she sees a young man who smiles at her. When she gets off the bus, the man follows her to find out where she lives. That night Mary tells her father Evaristo that she had already chosen a husband, but does not know his name or where he lives.

Rosita, a friend of Mary, announces her sudden wedding. During the wedding party, Tito, a pharmacist friend, reveals to Mary that Rosita is marrying because she is pregnant and an abortion was too dangerous. At hearing the word "abortion," Mary feels revolted, gets dizzy, and flees the party. Mary becomes hysterical. "Theyre all whores ... its all shit," he shouts to Tito.

That summer, Mary is reunited with the man from the bus, Cholo, who works for a meat packing company with his brothers and is also a boxer. Mary invites him to her house and they quickly start a relationship. At New Years Eve, Mary meets Cholos family: Cholos mother (a widow) Mrs. America, Cholos brother Raul and Rauls wife Sofia, Cholos brother Hector and his wife Luisa, and Cholos sister Claudia and her husband Ariel. At Marys request, Cholo leaves boxing behind.

Cholo and Mary make out passionately. Cholo tries to consummate the relationship, but Mary says she will marry a virgin, and when Cholo attempts to force himself on her, she rejects him. Five days later they reconcile and soon they plan their wedding. On the day of the civil wedding Mary announces that she will remain a virgin until the church wedding, to be held two days later. Again Cholo tries to force Mary, but she rejects him very violently. After the church wedding, Cholo and Mary have a passionate wedding night in a hotel in Buenos Aires and consummate their relationship.

Sofia, sister in law of Cholo announces worriedly that she is pregnant. The previous delivery had complications, and Sofia fears what will happen if she decides to continue with the pregnancy. Mary says that “it is better to sacrifice ones life rather than to kill a child ... it is very dangerous to play with fate." Sofia decides to have an abortion, but she dies as a result of the intervention. When Mary finds out, she feels revolted and repeats: “Theyre all shit.” After the funeral, Mary declares that Sofia deserved what happened to her, but then she decides to help raise Sofia’s children and respect her memory. Cholo’s sister Claudia said Mary prophesied the death of Sophia, but Mary says she only gave Sofia some advice.

Six months after becoming a widower, Cholos brother Raul begins a relationship with a widow. Mary finds out and accuses Raul of desecrating Sofias memory. Ariel, Claudias husband and  Cholos brother-in-law, defends Rauls courtship. Mary says to Ariel: "I wonder what you would think if you were to die and Claudia were to find someone else." Claudia, who believes that Mary is a prophetess, tells her to shut up. Mary begins to move away from Cholo and asks Cholo to leave the business he has with his brothers.

Mary becomes increasingly sad and sullen. Cholo gets a day off. On the day that Ariel takes Cholos place at work, an accident occurs with the delivery truck and Ariel dies. When Mary attends the wake, Claudia tells her to leave and they stop talking to each other.

Marys "prophecies" continues to be fulfilled: After Ariel dies, Claudia finds a boyfriend and plans to remarry. Mary gets more depressed, retreats from Cholos family and begins to wear black. Only Cholos sister Luisa still visits Mary, but Mary prophecies Luisas, and her own, death. Thus Mary imagines the progressive death of all of Mrs. Americas children-in-law. Marys rejection of her in-laws affects her relationship with Cholo. Mary has nightmares, suffers depression and isolates herself from the world.

When Luisa gets sick with food poisoning, Mary finally loses her mind. He says it is fate that Luisa, and then herself, die. Luisa recovers, but Mary is asleep when Cholo returns with the good news. When Cholo is asleep, Mary gets up, strips herself naked, puts on her wedding dress, and takes a knife. Mary goes to the bedroom and kills Cholo by stabbing him in the heart.

==Cast==
 , Alberto Argibay, Dora Baret, Jorge Rivera López, Olga Zubarry, Susana Giménez and Carlos Monzón.]]
* Susana Giménez as Mary
* Carlos Monzón as Cholo
* Alberto Argibay as Raúl
* Dora Baret as Sofía
* Teresa Blasco
* Juan José Camero
* Dora Ferreiro
* María Rosa Gallo as Doña Consuelo
* Antonio Grimau as Tito
* Juana Hidalgo as Rosita
* Leonor Manso as Luisa
* Ubaldo Martínez as Don Evaristo
* Jorge Rivera López as Ariel
* Olga Zubarry as Claudia

==References==
 

==External links==
 
*  
*   at Cinenacional.com
*  

 
 
 
 
 