Next Goal Wins
 
 
{{Infobox film
| name           = Next Goal Wins
| image          = The poster for the film Next Goal Wins.jpg
| caption        = British poster for Next Goal Wins
| director       = Mike Brett Steve Jamison
| producer       = Kristian Brodie
| starring       = Thomas Rongen Jaiyah Saelua Nicky Salapu
| music          = Roger Goula
| cinematography = Mike Brett and Steve Jamison
| editing        = Julian Quantrill 
| studio         = Archers Mark Agile Films K5 International
| distributor    = Icon Productions 
| released       =  
| runtime        = 97&nbsp;minutes
| country        = United Kingdom
| language       = English Samoan
| budget         = 
| gross          = 
}} national football team of American Samoa as they try to recover from the indignity of being known as one of the weakest football teams in the world, and to qualify for the 2014 FIFA World Cup.

==Synopsis== lost 31–0 to Australia, the worst loss in international football history, and have been dogged by defeat ever since. They want to qualify for the 2014 FIFA World Cup, but continue to lose on the pitch. To help turn their luck around, the Football Federation American Samoa hire Dutch-born, America-based coach Thomas Rongen.   
 OFC World Cup Qualification.

Jaiyah Saelua, a member of the squad since 2003 is the first faafafine player to compete in a mens FIFA World Cup qualifier.    

==Production== RED Epic camera over two visits to the island of six and eight weeks respectively. 

==Release==
The film had its premiere at the Tribeca Film Festival on 19 April 2014.    It was released in the United Kingdom on 9 May. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 