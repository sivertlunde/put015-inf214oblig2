Cachorro
 
{{Infobox Film
| name = Cachorro
| image = tlareleasingbearcub.jpg
| image_size =
| caption =
| director = Miguel Albaladejo
| producer = Juan Alexander Sergio Castellote Jose Luis Garcia Arroyo
| writer = Miguel Albaladejo Salvador García Ruiz
| narrator = David Castillo Diana Cerezo Arno Chevrier Teresa Empar Ferrer
| music = Lucio Godoy
| cinematography = Alfonso Sanz
| editing = Pablo Blanco
| distributor = TLA Releasing Televisión Española Canal+ Telemadrid New York) November 19, 2004 (Los Angeles)
| runtime = 98 min.
| country = Spain Spanish
| budget =
| preceded_by =
| followed_by =
}}
 2004 Spain|Spanish gay-themed (in particular, the gay bear community) drama film written and directed by Miguel Albaladejo. It is about a bearish gay man who ends up looking after his nephew while his sister goes away to India and in turn makes him develop a fatherly bond with the boy as well as forcing him to alter his lifestyle. The Spanish word cachorro describes any young, furry animal such as a cub or puppy.

==Plot==
As a favor to his hippie sister who has gone off to India, Pedro, a gay dentist, has agreed to look after his nine-year-old nephew, Bernardo. Bernardo’s father is dead and the boy and his uncle have not had much to do with each other until now. Originally, the boy was to stay with Pedro for a few days, but six weeks have passed with no word from the boys mother.

His nephew’s presence forces Pedro to take a break from his otherwise extremely active sex life. In fact, Pedro was beginning to tire of the superficial nature of his many relationships. Even his boyfriend, Manuel, who suddenly pays Pedro a visit and who shares his penchant for leather and latex, is not really the man with whom he wants to spend the rest of his life, although they express love for each other. Then, all at once, an entirely different set of problems crops up. For one, there is Doña Teresa, Bernardo’s paternal grandmother, who one day darkens Pedro’s door and puts the emotional screws on him. Worse still, however, is the news that Bernardo’s mother has been arrested in India for drug smuggling. She may be facing a prison sentence of thirty years, so the embassy informs Pedro. Pedro is just as shocked at this news as Bernardo; nevertheless, he realizes that he is now responsible for the boy. Without further ado, he decides to rise to the challenge this represents. There suddenly seem to be so many things to organize – such as repairs to the house and finding a school for the boy – that Pedro hardly misses the life he used to lead. Gradually, however, he succeeds in rekindling his sex life – albeit in a less excessive form.

Then Doña Teresa puts in another appearance, this time demanding custody of Bernardo. But Pedro is ready to fight for the boys welfare. After illegally obtaining his medical history (revealing that Pedro is HIV positive) and hiring a private investigator who obtains pictures of Pedro on a night out in a gay club, and threatening him with both, he ends up letting her put Bernardo in a boarding school. When his grandmother goes to visit him later on, revealing Pedros HIV status (despite their previous agreement), Bernardo reveals that he already knew from his mother, and that his mother was HIV positive as well. She then reveals that he is in the hospital with pneumonia. He then tells her that he hates her and that its her fault that hes currently in the hospital, because he wasnt there to care for him.

Three years pass, during which several letters go back and forth between Bernardo, Pedro, his mother and his grandmother. We then see Bernardo and two of his friends, a boy and girl, at a funeral, and a casket being lowered, although it is not immediately revealed whose funeral this is. Shortly after, a cab pulls up with Pedro inside, out of the hospital and healthy, and he expresses lament at the passing of Bernardos grandmother. After a brief conversation, Bernardo returns to his two friends, kisses both, and parts from them, leaving the viewer to wonder whether or not hes romantically involved with one of them. Bernardo then returns to his uncle and they ride away together in the cab.

==Cast==
*José Luis García Pérez as Pedro David Castillo as Bernardo, 9 years old
*Daniel Llobregat as Bernardo, 14 years old
*Diana Cerezo as Lola
*Arno Chevrier as Manuel
*Empar Ferrer as Doña Teresa
*Elvira Lindo as Violeta
*Mario Arias as Javi

==Distribution== New York Los Angeles on November 19, 2004, earning $99,261 at the U.S. Box Office.  It was first released on DVD-video on 10 May 2005, and was released again as an unrated directors cut on 1 March 2007.

==Film Festivals== 2004 Berlin International Film Festival 2004 Tribeca Film Festival 40th Chicago International Film Festival 2004 Philadelphia International Gay & Lesbian Film Festival 2004 Miami International GL Film Festival
  2005 Tokyo International Lesbian & Gay Film Festival

==Tag line==
Parenthood is about to get a little hairier.

==References==
 

==External links==
* 
*  at Allmovie
*  

 
 
 
 
 
 
 
 
 
 
 
 