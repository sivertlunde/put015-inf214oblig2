Brushfire (film)
{{Infobox film
  | title = Brushfire!
  | image = Brushpost.jpg
  | caption = Original film poster
  | director = Jack Warner, Jr.
  | producer = Jack Warner, Jr.
  | writer = Jack Warner, Jr.  Irwin Blacker (story) John Ireland Everett Sloane Jo Morrow Carl Esmond Howard Caine Al Avalon James Hong
  | music = Irving Gertz
  | cinematography = Eddie Fitzgerald
  | editing =
  | distributor = Paramount Pictures
  | released = February 1962
  | runtime = 80 minutes
  | country = United States
  | language = English
  | budget =
  | gross =
  | awards =
    }}
 Viet Nam Laos Low brushfire conflicts.

==Plot== John Ireland) planters in an unnamed Southeast Asian nation.  They are drawn into a conflict with a group of guerrillas led by Martin (Carl Esmond) and Vlad (Howard Caine) who have abducted a young American couple Tony (Al Avalon) and Easter (Jo Morrow) Banford.    The two use their jungle fighting expertise and knowledge of the local land to rescue them and wipe out the guerrillas. Jeff states that the rescue effort was worth the high cost in lives because it kept an uprising from developing into a rebellion. 

==Cast== John Ireland as Jeff
* Everett Sloane as Chevern
* Carl Esmond as Martin
* Howard Caine as Vlad
* Al Avalon as Tony
* Jo Morrow as Easter

==Notes==
 

==External links==
* 

 
 
 
 
 
 
 
 


 