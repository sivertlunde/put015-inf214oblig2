Quo Vadis (1924 film)
{{Infobox film
| name           = Quo Vadis
| image          = Quo Vadis poster.jpg
| image_size     = 
| caption        =  film poster
| director       = Gabriellino DAnnunzio   Georg Jacoby 
| producer       = Arturo Ambrosio
| writer         = Henryk Sienkiewicz (novel)   Gabriellino DAnnunzio   Georg Jacoby
| narrator       = 
| starring       = Emil Jannings   Elena Sangro   Lillian Hall-Davis   Rina De Liguoro
| music          = 
| editing        =
| cinematography = Curt Courant   Alfredo Donelli   Giovanni Vitrotti
| studio         = Unione Cinematografica Italiana
| distributor    = Unione Cinematografica Italiana (Italy)   First National Pictures (US)
| released       = October 1924 (Austria) 15 February 1925 (USA)
| runtime        = 90 minutes 120 minutes (directors cut)
| country        = Italy
| language       = Silent   Italian intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Italian silent silent historical Quo Vadis a 1951 film.

==Plot==
In  , already hated by the Romans.

==Production== Fascist Italy epics of the previous decade.  Rudolph Valentino was invited to star in the film, but was forced to turn the offer down due to contractual reasons.  Production quickly became troubled - the film ran seriously over-budget, and additional financing had to be raised from Germany. The new backers insisted that a German director, Jacoby, be appointed to co-direct. 

==Reception== musicals and comedy film|comedies. 

==Cast==
* Emil Jannings as Nero 
* Elena Sangro as Poppea 
* Lillian Hall-Davis as Licia 
* Rina De Liguoro as Eunica 
* Andrea Habay as Petronius 
* Raimondo Van Riel as Tigellinus 
* Gildo Bocci as Vittelius 
* Gino Viotti as Chilone Chilonides 
* Alfons Fryland as Vinicius 
* Bruto Castellani as Ursus 
* Elga Brink as Domitilla 
* Arnold Kent as Roman Guard 
* Marcella Sabbatini as Girl 
* Lucia Zanussi

==References==
 

==Bibliography==
* Barton, Ruth. Hedy Lamarr: The Most Beautiful Woman in Film. University Press of Kentucky, 2010. 
* Holston, Kim R. Movie Roadshows: A History and Filmography of Reserved-Seat Limited Showings. McFarland, 2012.
* Moliterno, Gino. The A to Z of Italian Cinema. Scarecrow Press, 2009. 
* Ricci, Steven. Cinema and Fascism: Italian Film and Society, 1922–1943. University of California Press, 2008.
* Scodel, Ruth & Bettenworth, Anja. Whither Quo Vadis: Sienkiewiczs Novel in Film and Television. John Wiley & Sons, 2009.
* Williams, Michael. Film Stardom, Myth and Classicism: The Rise of Hollywoods Gods. Palgrave Macmillan, 2012.

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 