The Man Unconquerable
{{Infobox film
| name           = The Man Unconquerable
| image          = The Man Unconquerable film ad.png
| alt            = 
| caption        = 
| director       = Joseph Henabery
| producer       = 
| screenplay     = Julien Josephson Hamilton Smith  Jack Holt Sylvia Breamer Clarence Burton Anne Schaefer Jean De Briac Edwin Stevens
| music          = 
| cinematography = Faxon M. Dean 
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent Jack Holt, Sylvia Breamer, Clarence Burton, Anne Schaefer, Jean De Briac and Edwin Stevens. The film was released on July 2, 1922, by Paramount Pictures.  
 
==Plot==
 

== Cast == Jack Holt as Robert Kendall
*Sylvia Breamer as Rita Durand
*Clarence Burton as Nilsson
*Anne Schaefer as Duenna 
*Jean De Briac as Perrier
*Edwin Stevens as Michaels
*Willard Louis as Governor of Papeete 

==Filming Troubles==
From a period newspaper:
"Famous Plea Fruitless.
"Dont give up the ship."
This time this was not the appeal of the famous Perry, but Clarence Burtons instructions, and he found them difficult to carry out, especially when the ship gave him up-by sinking. Burton, together with a gang of men who took the parts of pearl divers, was in command of a pearling tug in the new Paramount picture, "The Man Unconquerable." As usual, Burton was the villain. Jack Holt,
the star of the picture, in an armed launch gave him battle. Presently the tug commenced to sink, faster than they had intended.
"Stick to it!" yelled Joseph Henabery, the director, hurrying to get the close-ups. But before he could arrive on the scene the tug had gone under. The only close-ups he secured were of Burton and his pearl divers splashing about on the surface trying to remove their clothes so that they could remain afloat. One of the deck hands on the photographers boat stood by with a boat hook to pull
them out when they showed signs of going down for the third time. In order to ret a good "shot" of the sinking boat it was necessary to drag it by chains to shallow water at high tide and repair it at low tide. For four days a crew of laborers worked at the job; then they sat disgustedly on the beach while they watched the tug towed out to sea again and resunk -this time, properly and with due regard to the fans. "   {{cite news |author= |agency= Citing public domain text published before 1923 |title=Famous Plea Fruitless. Dont give up the ship."
 |url=http://chroniclingamerica.loc.gov/lccn/sn83045462/1922-06-04/ed-1/seq-61/ |newspaper=Evening star |location=Wahsington, DC |date=June 4, 1922 |access-date=February 15, 2015 }} 

== References ==
 

== External links ==
*  
 
 
 
 
 
 
 
 
 