Dada (2000 film)
{{Infobox film
 | name = Dada
 | image = DadaMithun.jpg
 | caption = DVD Cover
 | director = T L V Prasad
 | producer = Mahendra Dhariwal
 | camera=Venkatesh
 | writer = Meeraq Mirza
 | dialogue =  Rami Reddy Dilip Tahil Ishrat Ali Jasi Thakur Raza Murad Deepak Shirke Ghulam Ali
 | lyrics = Nawab Arzoo
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  15 September 2000
 | runtime = 125 min.
 | language = Hindi Rs 2.1 Crores
 | preceded_by = 
 | followed_by = 
 }}
Dada is an action Hindi film made in 2000. A revenge drama, with Mithun in the lead role.

==Plot==

A story of a simpleton whose life changes when he witnesses a shoot-out of a Don and rescues him. The don takes him as his successor. How the negative elements of the underworld influence him forms the films finale.

==Cast==
*Mithun Chakraborty
*Swati Manvi
*Dilip Tahil
*Goswami
*Ishrat Ali
*Jasi Thakur
*Rami Reddy
*Raza Murad
*Deepak Shirke Arjun  as  Goon

==References==
 

==External links==
* 

 
 
 
 
 
 