Old Clothes
{{Infobox film
| name           = Old Clothes
| image          = Posteroldclothes.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Edward F. Cline
| producer       = Jack Coogan, Sr.
| writer         =  Robert E. Hopkins
| story          = Willard Mack
| based on       =  
| narrator       = 
| starring       = Jackie Coogan Joan Crawford Max Davidson Lillian Elliott Allan Forrest
| music          = 
| cinematography = Frank B. Good Harry Davis 
| editing        = 
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =   
| runtime        = 65 mins.
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          =
}}
 1925 American silent drama film, starring Jackie Coogan and Joan Crawford.

This was the first film in which Crawford was credited with her new name — Joan Crawford.  She had been renamed by the studio, who deemed her birth name, Lucille LeSueur, as sounding unfit for a movie star.

==Plot summary==
Tim Kelly (Jackie Coogan) and Max Ginsberg (Max Davidson) have struck it rich by investing in copper stock.   But when the stock takes a dive, they are compelled to go back into their former profession — junk dealers. They take in the destitute Mary Riley (Joan Crawford) as a boarder and she hits it off so well with them that she winds up becoming a partner in their rag & junk company.  Mary falls in love with a man named Nathan Burke (Allan Forrest), the son of wealthy parents. Nathans mother (Lillian Elliott), however, disapproves of Mary.  Eventually it is revealed that Mrs. Burke came from a poor background herself, and her long-ago sweetheart was Max. After this discovery, she gives the couple her blessings. The copper stock soars in value once again, so Kelly and Ginsberg are back in the money. 

==Cast==
*Jackie Coogan - Tim Kelly
*Max Davidson - Max Ginsberg
*Joan Crawford - Mary Kelly
*Allan Forrest - Nathan Burke
*Lillian Elliott - Mrs. Burke
*James Mason - Dapper Dan
*Stanton Heck - The adjuster
*Dynamite the Horse

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 