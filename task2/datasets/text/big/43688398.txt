Revivre
{{Infobox film
| name           = Revivre
| image          = Make-Up (2014 film).jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Im Kwon-taek
| producer       = Shim Jae-myung
| writer         = Song Yoon-hee
| based on       =   Kim Gyu-ri Kim Ho-jung
| music          = Kim Soo-chul
| cinematography = Kim Hyung-koo
| editing        = Park Sun-duck   Steve M. Choi
| studio         = Myung Films
| distributor    = Little Big Pictures
| released       =   
| runtime        = 93 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}

Revivre ( ; lit. "Cremation" or "Make-up/Cosmetics") is a 2014 South Korean drama film directed by Im Kwon-taek and starring Ahn Sung-ki.   It premiered in the Out of Competition section of the 71st Venice International Film Festival in 2014, and was released in South Korean theaters on April 9, 2015.  

The film is based on Kim Hoons short story Cremation (also translated as From Powder to Powder),  which won the Yi Sang Literary Award in 2004.   

==Plot==
Oh Sang-moo is in his mid-fifties and is a successful marketing executive at a major cosmetics company. He struggles to juggle corporate life and preparing for a new ad campaign, while tirelessly caring for his ailing wife, Jin-kyung. Jin-kyungs health has steadily and painfully deteriorated in the last four years due to brain cancer. During this difficult time, Sang-moo also becomes aware of his growing feelings for Choo Eun-joo, the much younger, alluring new addition to his marketing team. When his wife finally succumbs to her disease, Sang-moo becomes conflicted over his profound grief and newfound passion.

==Cast==
*Ahn Sung-ki as Oh Sang-moo Kim Gyu-ri as Choo Eun-joo
*Kim Ho-jung as Jin-kyung

==Awards and nominations==
{| class="wikitable"
|-
! Year !! Award !! Category !! Recipient !! Result
|- 2015
| 51st Paeksang 51st Baeksang Arts Awards
| Best Film
| Revivre
|  
|-
| Best Director
| Im Kwon-taek
|  
|-
| Best Actor
| Ahn Sung-ki
|  
|-
| Best Supporting Actress
| Kim Ho-jung
|  
|-
|}

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 