I Am Kalam
 
 
{{Infobox film
| image          = i-am-kalam-2.jpg
| caption        = Theatrical release poster
| director       = Nila Madhab Panda
| producer       = Santanu Mishra Jitendra mishra (Associate)
| Production     = Smile Foundation Sanjay Chauhan
| screenplay     = Sanjay Chauhan
| starring       = Harsh Mayar Gulshan Grover Pitobash Tripathy Beatrice Ordeix Papon Susmit Bose Shivji Dholi
| cinematography = Mohana Krishna
| editing        = Prashant Nayak
| studio         = 
| released       =  
| runtime        = 87 minutes
| country        = India
| language       = Hindi
| budget         = 30&nbsp;million
| gross          = 60&nbsp;million
}}
I Am Kalam is a 2011 Hindi film directed by Nila Madhab Panda. The plot revolves around Chhotu, a poor Rajasthani boy, who is inspired by the life of the former President of India, A. P. J. Abdul Kalam    and his strong desire to learn. The character of Chhotu has been performed by Harsh Mayar,    a Delhi slum boy. The film was screened in the market section at the 63rd Cannes Film Festival on 12 May 2010.  It has been showcased in various film festivals and has received many awards and honours.          

==Plot==
Chhotu is intelligent boy of about 12 years of age from Rajasthan. Born into poverty, he is pawned off by his mother to work at a roadside food stall under the somewhat kind owner, Bhati. The mother says repeatedly "Schools is not in our destiny". The movie is about how there is no such destiny and such supposed destinies can be changed by ones own hard work.

One day Chhotu watches President Dr. A.P.J. Abdul Kalam on television and it inspires him. Chhotu changes his name to Kalam and decides he wants to become someone who wears a tie and who is respected by others.

Chhotu/Kalam is quick to learn how to prepare tea and other things needed for the food stall, and he impresses Bhati with his abilities and creative ideas like delivering tea riding a camel , which the European and American tourists residing at the King Rudraprataps Mansion cum Hotel love and cherish. Kalam(Chhotu) while working for Bhati befriend kings son Prince Ranvijay singh(Hussan Saad) who feels lonely as he cannot befriend a commoner. Soon Kalam and Prince become true friends sharing gifts, books and skills which they command individually. The other employee, a young man going by the name of Laptan (played by Pitobash Tripathy), envies and mistreats Kalam by enforcing daily chores on him, at one time destroying all of his books. Kalam also helps Prince by writing an essay in Hindi for which the Prince receives an award. But his happiness is short lived as palace guards search Kalams room and find clothes and books, which the prince had given to him as a present secretly. Even though Kalam is accused of being a thief, he does not reveal the source of the items to protect the prince from his fathers wrath. Disheartened Kalam hitches a ride to New Delhi where he tries to meet the president but he couldnt so he leaves a letter to the president citing How he has been inspired by him and the realisation that any common child can become a president, prime minister(a successful, respectable person) by hard work and not by destiny.

Meanwhile, the prince tells his father that he gave the clothes to Kalam and that Kalam wrote the speech which earned first prize. The nominal king realises his mistake and sends the prince to find Kalam in New Delhi. Kalam is found near the India Gate and is brought back home. The king tells him that he can study in the same school as the prince and employs Kalams mother as well. Bhati offers to pay the school fees, but Kalam says he will pay for them himself. At the end Kalams dream comes true he goes to school with prince. The film ends giving a message that "The children come to school not only for information....but also for transformation".

I am Kalam is a Smile Foundation production and is the first film ever in India to be produced by a development organisation. With this film Smile Foundation is trying to reach out to people around the world drawing their attention to support the movement of educating every child in India to secure a better future. It is a fact that in a world that is already exposed to extraordinary content, plain appeals do not stand a chance any more in grabbing attention. Keeping this in mind, Smile Foundation proactively took the initiative and the challenges therein to come up with a mainstream Bollywood film; and has tried to package a very important cause in a contemporary fable like manner in an attempt to reach out to the maximum number of people towards this cause.

==Cast==
* Harsh Mayar as Chhotu/Kalam
* Hussan Saad as Prince Ranvijay.
* Gulshan Grover as Bhati the dhaba owner
* Beatrice Ordeix (a French actor) as Lucy
* Pitobash Tripathy as Laptan
* Meena Mir as Chhotus mother
* Suresh Acharya as Lakhas Assistant
* Biswajeet Bal as Sukha Singh
* Rajat Bhalla as Police Havaldar
* Garima Bharadwaj as Rani Sa
* Sanjay Chauhan as Raja Rudra Pratap Singh (as Sanjay Chouhan)
* S.D. Chouhan as Ranvijays Servant

==Release==
I Am Kalam released in India screens on 5 August 2011. A special screening was held for Dr. A.P.J. Abdul Kalam at his Delhi residence on 29 July and also to take his blessings. 

==Reception==

===Critical reception===
It is a critically acclaimed movie. The film was rated 4.40 on 5 by audiences at the Transilvania International Film Festival recently.  Critic Vianayak said "This film is an example of how a childrens film can regale with solid storytelling, at the same time creating space for undercurrent comment." TOI wrote " Its inspirational, intelligent, topical and entertaining too. More importantly, it brims over with heart and soul, leaving no one untouched with its simple message of providing an equal opportunity".  DNA appreciate movie – "At a little over 90 minutes, I Am Kalam is a gripping watch that leaves you feeling uplifted and positive. Try not to miss it.".  Rediff.com stated that I Am Kalam is a winner.  Taran Adarsh inspired movie by saying "On the whole, I am kalam is an inspiring and motivating film that deserves to be encouraged. Recommended!".

==Awards and nominations==

===Wins===
* Golden Elephant in Best Screenplay in 17th International children Film Festival, India
* Intl Film Festival of India(IFFI),Goa by Young Jury for Best Feature Film   
* National Film Award for Best Child Artist 2011 –  Harsh Mayar.  He won it along with two other children 
* Lucas Film Festival, Germany for Best Feature Film {{cite web |title=I Am Kalam wins two international awards |url=
http://www.bollywoodtrade.com/trade-news/i-am-kalam-wins-two-international-awards/index.htm |publisher=Bollywood Trade|date=14 September 2010|accessdate=19 November 2012}} 
* Barbican Childrens Film Festival, London for Audience Favorite
* Cinekid Intl Film Festival, Amsterdam for Special Mention
* Intl Jury at Ale Kino Intl Film Festival, Poland for Special Mention 
* Don Quixote Prize of the International Ciné-Club Federation (ICCF) at the Lucas Film Festival 
* Special Diploma at Minsk Intl Film Festival, Listapadzik 
* Aravindan Award for Best Debutante Director  Best Story to Sanjay Chauhan 

===Nominations===
* Cinekid Film Festival, Amsterdam for Cinekid Lion Award
* Cinekid Film Festival, Amsterdam for Cinekid Audience Award
* Buster Film Festival, Denmark for Politiken Audience Award
* Mumbai Intl Film Festival (MAMI) for Indian Frame Showcase 
* Cinekid Film Festival, Amsterdam, for Cinekid Lion Award

== References ==
 

==External links==
*  
*  
*  

===Location ===
This Film is shot in Rajasthan India .

 
 
 
 
 
 