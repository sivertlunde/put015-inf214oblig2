Girl with Green Eyes
 
 
{{Infobox film
| name           = Girl with Green Eyes
| image          = Girlwithgreeneyes.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Desmond Davis
| producer       = Oscar Lewenstein
| writer         = Edna OBrien
| narrator       =
| starring       = Peter Finch Rita Tushingham Lynn Redgrave Maire Kean Arthur OSullivan Julian Glover
| music          = John Addison
| cinematography = Manny Wynn
| editing        = Brian Smedley-Aston
| studio         = Woodfall Film Productions
| distributor    = United Artists Corporation
| released       = 10 August 1964
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| budget         = £140,000 Alexander Walker, Hollywood, England, Stein and Day, 1974 p261 
| gross          =
| preceded_by    =
| followed_by    =
}} British drama film, which Edna OBrien adapted from her own novel, The Lonely Girl. It was directed by Desmond Davis, and stars Peter Finch, Rita Tushingham, Lynn Redgrave and Julian Glover.

==Plot==
In 1960s Dublin, Kate Brady, a young rural girl, takes a room with her friend, Baba Brennan. She becomes involved with a much older man, a writer named Eugene Gaillard, who eyes her and ignores her more worldly roommate.

==Cast==
* Peter Finch as Eugene Gaillard  
* Rita Tushingham as Kate Brady  
* Lynn Redgrave as Baba Brennan  
* Marie Kean as Josie Hannigan
* Arthur OSullivan as James Brady  
* Julian Glover as Malachi Sullivan  
* T. P. McKenna as The Priest   Joe Lynch as Andy Devlin  
* Yolande Turner as Mary Maguire  
* Harry Brogan as Jack Holland   David Kelly as Ticket Collector

==References==
 

==External links==
*  

 

 

 
 
 
 
 
 
 
 
 
 

 
 