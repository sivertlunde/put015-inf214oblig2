Blood Night: The Legend of Mary Hatchet
{{Infobox film
| name           = Blood Night: The Legend of Mary Hatchet
| image          = Blood Night.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = 
| director       = Frank Sabatella
| producer       = Frank Mosca   Frank Sabatella
| writer         = Frank Sabatella
| screenplay     = Elke Blasi
| story          = 
| based on       = 
| narrator       = 
| starring       = Bill Moseley   Nate Dushku   Danielle Harris
| music          = Victor Bruno   Stephen Tubin
| cinematography = Jarin Blaschke   Christopher Walters
| editing        = Stephen Franciosa Jr.
| studio         = Chaos Squared   Harrington Talents   Sideshow Pictures
| distributor    = Aspect Film
| released       =  
| runtime        = 83 minutes
| country        = United states
| language       = English
| budget         = $3,000,000
| gross          = 
}}

Blood Night: The Legend of Mary Hatchet is a 2009 horror film written and directed by Frank Sabatella, and co-written by Elke Blasi.

== Plot ==

In 1978, Mary Mattock has her first period, and due to menstrual psychosis, murders her parents with a hatchet. Deemed mentally unfit, Mary is placed in Kings Park Psychiatric Center, where she is raped and impregnated by a guard in 1989. This incident is covered up, and after being told her child was a stillbirth, Mary goes on a rampage through the hospital. The police find Mary wandering the grounds, and open fire on her after she throws the severed head of her rapist at them, ending her killing spree. "Mary Hatchet" subsequently becomes a folklore figure, with local youngsters celebrating the Mischief Night-esque "Blood Night" in her name.

In 2008, a group of teenagers prepare for their Blood Night party by holding a séance at Marys grave, where they are told one of the stories relating to her by the cemetery caretaker, Gus, a former Kings Park employee who claims that Mary will keep coming back to kill until she finds her child, who was buried in a shallow grave near Kings Park. The teenagers proceed to party at a house, where straggling guests are killed by an unseen murderer amidst supernatural occurrences.

The remaining teenagers flee when they uncover their friends bodies, and run into Gus. After hearing the teenagers story, Gus has them accompany him to the abandoned Kings Park Psychiatric Center, where they dig up the grave of Marys baby, Gus being convinced that reuniting mother and child will appease Marys spirit. The babys casket is empty, so the group break into Kings Park in search of answers, and discover paperwork that reveals that Marys child is Alissa, a missing party guest visiting from Chicago. A series of flashbacks show that Alissa killed everyone either due to being influenced by Mary, or because she is experiencing the onset of a disorder similar to her to mothers.

Alissa appears, kills Gus, and chases the others through the asylum, picking them off until only Alex and Lanie remain. Alex and Alissa fight, and after a bloody struggle, Alex strangles Alissa to death. As Lanie and a wounded Alex search for an exit, the latter is decapitated by Marys enraged ghost, which then lunges at the screaming Lanie.

== Cast ==

* Bill Moseley as Graveyard Gus
* Danielle Harris as Alissa Giordano
* Nate Dushku as Alex
* Samantha Facchi as Mary "Hatchet" Mattock
* Anthony Marks as Chris
* Billy Magnussen as Eric
* Alissa Dean as Nichole
* Maryam Basir as Jen
* Samantha Hahn as Lanie
* Michael Wartella as Gibbz
* Patricia Raven as Young Mary Mattock
* Connor Fox as Katz
* Russel Dennis Lewis as Tim
* Rich Ceraulo as Corey
* Garett Neil Stevens as Huey
* Josh Segarra as Tyler
* Samantha Jacobs as Jessica
* Sal Rendino as Bob Mattock
* Nancy Malleo as Linda Mattock

== Home Media ==
{|class="wikitable"
|-
!DVD name
!Region 1 A
!Region 2 B
!Region 4 B
!DVD Special Features
|-
| Blood Night
| 19 April 2011
| N/A
| 2 February 2011
| 
Audio Commentary with Writer/Director Frank Sabatella 

Spilling Blood: The Making of Blood Night 

Outtakes Reel
|-
|}


== Reception ==
 Arrow in slasher in the past couple decades" it still possessed "all the right ingredients to make a good horror movie".  

Daryl Loomis of DVD Verdict had a positive response to film, finding that while it did not do anything too groundbreaking or fresh, the gory and imaginative kills coupled with the fair level of suspense, solid setup, and acceptable performances made it "one of the better slasher films Ive seen in some time".   A later review for the same website, written by Gordon Sullivan, found Blood Night to be a good (but not great) slasher, despite drawbacks like the uneven tone, and lengthy buildup to the gory mayhem.  

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 