Aranazhika Neram
{{Infobox film
| name           = Ara Nazhika Neram
| image          = 
| image size     =
| caption        =
| director       = K. S. Sethumadhavan
| producer       = M.O.Joseph
| screenplay     = Parappurath
| based on       = Ara Nazhika Neram by Parappurath
| narrator       =  Kottarakkara Prem Sathyan K. Ragini Sheela Ambika Sukumaran
| music          = G. Devarajan
| cinematography = Melli Irani
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} novel of the same name. Parappurath also played a minor role in the film. The story of the film revolves around an orthodox Christian family headed by Kunjenachan (Kottarakkara Sreedharan Nair), a ninety-year-old patriarch who lives his life by The Book. The film also features Prem Nazir, Sathyanesan Nadar|Sathyan, Ragini (actress)|Ragini, Sheela, K. P. Ummer, Adoor Bhasi and Ambika Sukumaran.    

==Cast==
* Kottarakkara Sreedharan Nair as Kunjonachan Sathyan as Mathukutty
* Prem Nazir as Rajan Ragini as Deenamma
* Sheela as Santhamma
* Ambika Sukumaran as Kuttiamma
* Adoor Bhasi as Sivarama Kurup
* K. P. Ummer as Thomas
* Sankaradi as Geevarghese Bahadur as Kunju Cherukkan
* N. Govindan Kutty as Philippose Meena as Annamma
* Jessey as Danny
* Parappurath

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma and Father Nagel.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anupame Azhake || K. J. Yesudas || Vayalar Ramavarma ||
|-
| 2 || Chippi Chippi || CO Anto, Latha Raju || Vayalar Ramavarma ||
|-
| 3 || Daivaputhranu || P Susheela || Vayalar Ramavarma ||
|-
| 4 || Samayamaam Radhathil || P. Leela, P. Madhuri || Father Nagel ||
|-
| 5 || Swarangale Sapthaswarangale || P. Leela || Vayalar Ramavarma ||
|}

==Awards==
; Kerala State Film Awards (1970) Best Director -  K. S. Sethumadhavan Best Story - Parappurath Best Actor - Kottarakkara Sreedharan Nair

==References==
 

==External links==
*  

 
 
 
 


 