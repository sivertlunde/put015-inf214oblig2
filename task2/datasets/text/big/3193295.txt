The Myth of Fingerprints
{{Infobox film
| name = The Myth of Fingerprints
| image =Poster of the movie The Myth of Fingerprints.jpg
| caption =
| director = Bart Freundlich
| producer = Bart Freundlich Tim Perell Mary Jane Skalski James Schamus
| writer = Bart Freundlich
| starring = Julianne Moore Roy Scheider Hope Davis Blythe Danner Noah Wyle Laurel Holloman Michael Vartan John Phillips
| cinematography = Stephen Kazmierski
| editing = Ken J. Sackerham Kate Williams
| studio = Good Machine
| distributor = Sony Pictures Classics
| released =  
| runtime = 93 minutes
| country = United States
| language = English
| budget = $2 million
| gross = $458,815
}}

The Myth of Fingerprints is a 1997 American film drama written and directed by Bart Freundlich. It stars Blythe Danner, Roy Scheider, Noah Wyle, and Julianne Moore (who later became Freundlichs wife). {{cite web|url=http://movies.nytimes.com/movie/review?res=9C0CEEDF1538F934A2575AC0A961958260|
date=1997-09-17|accessdate=2012-03-14|author=Stephen Holden|title=A Family as the Thanksgiving Turkey|publisher=The New York Times}} 

The film is named after the song "All Around the World or the Myth of the Fingerprints" by Paul Simon, featured on his 1986 album Graceland (album)|Graceland. The song is concerned with dispelling the "myth" that people are different the world over: "Ive seen them all, and, man, theyre all the same."

==Plot==
When a dysfunctional family gathers for Thanksgiving at their New England home, past demons reveal themselves as one son returns for the first time in three years.

==Cast==
* Julianne Moore as Mia
* Roy Scheider as Hal
* Hope Davis as Margaret
* Blythe Danner as Lena
* Noah Wyle as Warren
* Laurel Holloman as Leigh
* Michael Vartan as Jake
* Chris Bauer as Jerry

==Production==
Set in New England, the film was shot in Andover, Maine|Andover, Bethel, Maine|Bethel, and Waterville, Maine|Waterville, Maine (Colby College).

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 