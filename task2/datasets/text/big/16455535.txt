The Blue Eagle
 
{{Infobox film
| name           = The Blue Eagle
| image          = The Blue Eagle.jpg
| caption        = Theatrical poster to The Blue Eagle (1926)
| director       = John Ford (uncredited)
| producer       = John Ford
| writer         = Gerald Beaumont Malcolm Stuart Boylan Gordon Rigby George OBrien Janet Gaynor
| cinematography = George Schneiderman
| editing        =  Fox Film Corporation
| released       =  
| runtime        = 58 minutes 
| country        = United States Silent English intertitles
| budget         = 
}}

The Blue Eagle is a 1926 American action film directed by John Ford. Prints of the film exist in the Library of Congress film archive and in the UCLA Film and Television Archive, but one reel is missing.      

==Cast==
  in 1927]] George OBrien as George Darcy
* Janet Gaynor as Rose Kelly William Russell as Big Tim Ryan
* Margaret Livingston as Mrs. Mary Rohan
* Robert Edeson as Chaplain Regan, aka Father Joe Philip Ford as Limpy Darcy (as Phillip Ford) David Butler as Nick Dizzy Galvani
* Lew Short as Sergeant Kelly
* Ralph Sipperly as Slats "Dip" Mulligan
* Jerry Madden as Baby Tom
* Jack Herrick as "On Da Nose" Sailor (uncredited)
* Jack Pennick as Ships crewman (uncredited)
* Charles Sullivan as Sailor Giving George Boxing Gloves (uncredited)
* Harry Tenbrook as Bascom, a Stoker (uncredited)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 