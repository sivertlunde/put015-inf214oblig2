Maha Purusha
{{Infobox film 
| name           = Maha Purusha
| image          =  
| caption        = 
| director       = Joe Simon
| producer       = Joe Simon H. G. Raju K. S. Nataraju D. J. Nayak
| story          = Haridas Bhat Joe Simon
| writer         = Ku Nagabhushan (dialogues)
| screenplay     = Joe Simon Vishnuvardhan Gayatri Gayatri Jai Jagadish Chandrashekar
| music          = Chellapilla Satyam
| cinematography = H. G. Raju
| editing        = P. Venkateshwara Rao
| studio         = Bhagya Movies
| distributor    = Bhagya Movies
| released       =  
| runtime        = 135 min
| country        = India Kannada
}}
 1985 Cinema Indian Kannada Kannada film, directed by Joe Simon and produced by Joe Simon, H. G. Raju, K. S. Nataraju and D. J. Nayak. The film stars Vishnuvardhan (actor)|Vishnuvardhan, Gayatri (actress)|Gayatri, Jai Jagadish and Chandrashekar in lead roles. The film had musical score by Chellapilla Satyam.  

==Cast==
  Vishnuvardhan
*Gayatri Gayatri
*Jai Jagadish
*Chandrashekar
*Roopadevi
*Pramila Joshai
*Renuka
*Sulakshana in Guest Appearance
*Chethan Ramarao
*B. K. Shankar
*K. V. Manjaiah
*Gopala Krishna
*Aravind
*Kakolu Ramaiah
*Veerendra Kumar
*Nagaraj
*Idris
*Nataraj
*Vinay
*Jr Narasimharaju
*Aparichitha Aravind
*Prabhu
*Baby Hema
*Kamala
*Vimala
*Anupama
*Vijayalakshmi
*Bharathi
 

==Soundtrack==
The music was composed by Satyam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vishnuvardhan || R. N. Jayagopal || 04.23
|- Janaki || R. N. Jayagopal || 04.46
|- Janaki || R. N. Jayagopal || 05.11
|- Janaki || Chi. Udaya Shankar || 04.40
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 