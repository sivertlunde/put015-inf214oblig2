Age of the Dragons
{{Infobox film
| name           = Age of the Dragons
| image          = Age of the Dragons.jpg
| director       = Ryan Little
| producer       = Gil Aglaure Devin Carter McKay Daines Steven A. Lee Joe Pia Peter Urie
| writer         = Gil Aglaure Anne K. Black McKay Daines
| starring       = Danny Glover Vinnie Jones
| music          = J Bateman
| cinematography = 
| editing        = John Lyde
| distributor    = KOAN  Metrodome Distribution 
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = $5 million
| gross          = $1 million
}}
Age of the Dragons is a 2011 fantasy film starring Danny Glover and Vinnie Jones, directed by Ryan Little. A fantasy-themed reimagining of Herman Melvilles classic novel, Moby Dick, it was released in the United Kingdom on March 4, 2011.

==Plot==
Harpooner Ishmael (Corey Sevier) joins Ahab (Danny Glover) and his crew on the Pequod, in this instance an armored land boat that hunts for dragons. The seven-strong crew ostensibly seeks the precious "vitriol", a highly explosive liquid substance found inside the fire-breathing winged creatures, which powers the mythical realm they live in. Ishmael joins their quest, and soon learns that in fact, Ahabs mission is one of revenge on a particular great white dragon that decades ago scarred and injured him, and also killed his sister. Forced to hide from the sunlight due to the burn wounds on his body, Ahab now tries to kill all dragons, and especially the white one. Conflict arises through a romantic entanglement between Ishmael and Ahabs adopted daughter, Rachel (Sofia Pernas), which causes hostility from the jealous hothead Flask (Larry Bagby). In the white dragons lair, Ahabs secrets are revealed and Rachel must choose between following him on his dark quest or escaping to a new life with Ishmael. She chooses the latter and in a final confrontation, Ahabs spear, which was tied to his foot, becomes entangled in the White Dragons neck. The creature flies off with a screaming Ahab, until he is slammed against a rock pillar and silenced. The white dragon flies off into the distance, with Ahabs body clinging on to it.

==Cast==
* Danny Glover as Ahab
* Vinnie Jones as Stubbs
* Corey Sevier as Ishmael
* Sofia Pernas as Rachel
* Larry Bagby as Flask
* Kepa Kruse as Queequeg 
* Nico Valencia as Bingzters David Morgan as Starbuck

==Development==
The film was originally going to be called Dragon Fire.  On February 3, 2010 it was announced that Danny Glover and Vinnie Jones had joined the cast, and that filming would begin in Utah the following week. The films budget was around $5 million,  and was the first film developed by distribution company Metrodome. A video from the set was revealed on March 5, 2010.  Several of the scenes were filmed at Castle Amphitheater in Provo, Utah behind the Utah State Hospital. Other filming locations included Stone Five Studios in the Riverwoods Business Park. Glover was quoted as saying "This is a great idea ... its going to be fun."  The trailer for the film was released on October 15, 2010.

==Reception==
The film received almost universal negative reviews in the British press.   stated 　“This crude picture, shot in snow-covered Utah, where the Pequod becomes a battle engine on large wooden wheels, is unamusingly ridiculous.”  
Variety (magazine)|Variety added 　“Generic dialogue and dull incident. Shoddy CGI indicates a production budget thats fatally inadequate for the task at hand.”
 .”

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 