Cenere
{{Infobox film
| name           = Cenere
| image          = Cenere.jpg
| image_size     =
| caption        = Publicity still of Eleonora Duse and Febo Mari in Cenere
| director       = Febo Mari
| producer       = Arturo Ambrosio
| writer         = Febo Mari, Eleonora Duse
| starring       = Eleonora Duse Febo Mari
| cinematography = Eugenio Bava
| editing        =
| released       =  
| runtime        = 38 minutes
| country        = Italy
| language       =
| budget         =
| gross          =
| distributor    = The Ambrosio Film Co.
}}
 1916 silent Sardinian writer Italian theater star Eleonora Duse.    Quote: "Indeed it was, as Duse would never venture before the cameras again, leaving "Cenere" as her only film record." 

==Plot==
Rosalia Derios is an unmarried woman in a small Sardinian village whose lover abandons her before the birth of their son, whom she names Anania. Realizing that she will not be able to properly raise the child, she gives full possession of Anania to her one-time lover.  However, she entrusts the boy with a sacred amulet before he leaves.  Anania grows to adult and retains possession of the amulet, but he is haunted by his mother’s absence and tries to locate her.  He pushes aside career prospects and cancels his plans for marriage to pursue his search for Rosalia. Anania proves successful and locates his mother. But Rosalia cannot take shock of being reunited with her adult son and kills herself.    

==Production==
When Eleonora Duse was approached in 1916 to appear in a film adaptation of Cenere, she had been absent from performing on stage for seven years. Duse expressed deep respect for the source material and its potential as a film. She later explained her return to acting in this production by stating: “I have been persuaded to create the character of Rosalia Derios, from the novel by Grazia Deledda, because it seemed to me that in the sorrowful figure of the mother, all sacrifice for her son, a figure moving in an austere and solemn landscape, would assume the total and clear plastic and spiritual significance that the silent theater must force itself to realize.”    

Duse co-wrote the screenplay for Cenere with Febo Mari, her director and co-star.  The film was shot over a four-month period on locations across Sardinia. 

Duse initially had hopes for Cenere to open a new career in film acting. But upon seeing the finished film, she was disappointed in both the production and her performance. "I made the same mistake that nearly everyone has made," she said after viewing herself on screen. "But something quite different is needed. I am too old for it. Isnt it a pity?"  Duse would later write to the French singer Yvette Guilbert with the request not to see “that stupid thing, because you’ll find nothing, or almost nothing, of me in that film.” 

==References==
 

==External links==

* 

 
 
 
 
 