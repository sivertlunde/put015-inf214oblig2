Melinda (film)
{{Infobox film
| name           = Melinda
| image          = Melinda poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Hugh A. Robertson
| producer       = Pervis Atkins
| screenplay     = Lonne Elder III
| story          = Raymond Cistheri  Paul Stevens Rockne Tarkington Ross Hagen
| music          = Jerry Butler Jerry Peters Bill Butler
| editing        = Paul L. Evans 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Paul Stevens, Rockne Tarkington and Ross Hagen. The film was released on August 16, 1972, by Metro-Goldwyn-Mayer.  {{cite web|url=http://www.nytimes.com/movie/review?res=9801E2D81130E73BBC4F52DFBE668389669EDE|title=Movie Review -
  Melinda - Melinda Arrives |publisher=The New York Times|accessdate=2 December 2014}} 

==Plot==
 

== Cast ==
*Calvin Lockhart as Frankie J. Parker
*Rosalind Cash as Terry Davis
*Vonetta McGee as Melinda Paul Stevens as Mitch
*Rockne Tarkington as Tank
*Ross Hagen as Gregg Van
*Renny Roker as Dennis Smith
*Judyann Elder as Gloria
*Jim Kelly as Charles Atkins
*Jan Tice as Marcia
*Lonne Elder III as Lt. Daniels
*Edmund Cambridge as Detective
*George Fisher as Young Man
*Joe Hooker as Romes Servant
*Allen Pinson as Rome Jack Manning as Bank Man
*Gene LeBell as Hood
*Gary Pagett as Sgt. Adams
*Khalil Bezaleel as Washington
*Nina Roman as Bank Woman
*Jeannie Bell as Jean
*Earl Maynard as Karate Group
*Dori Dixon as Karate Group
*Douglas C. Lawrence as Karate Group
*Evelyne Cuffee as Karate Group
*Peaches Jones as Karate Group 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 