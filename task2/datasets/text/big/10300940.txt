Mysterious Object at Noon
{{Infobox film
| name           = Mysterious Object at Noon
| image          = Mysterious Object DVD cover.jpg
| caption        = DVD cover.
| director       = Apichatpong Weerasethakul
| producer       = Gridthiya Gaweewong Mingmongkol Sonakul 
| writer         = 
| narrator       = 
| starring       = Phurida Vichitphan Mesini Kaewratri
| music          = 
| cinematography = Prasong Klimborron Sayombhu Mukdeeprom	
| editing        = Mingmongkol Sonakul Apichatpong  Weerasethakul
| distributor    = 
| released       = October 2, 2000 (Canada)
| runtime        = 83 minutes
| country        = Thailand
| language       = Thai
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2000 Cinema Thai independent independent experimental experimental  documentary film directed by Apichatpong Weerasethakul.

==Production==
The film is unscripted and uses the exquisite corpse party game as a concept, with the film crew traveling across Thailand, interviewing people and asking each person to add their own words to a story.

The film was shot in 16mm and enlarged to 35mm for international exhibition. 

==Reception==

===Festivals and awards=== NETPAC Special Mention Prize at the Yamagata International Documentary Film Festival. The film was screened at many other film festivals, including the London Film Festival, the Singapore International Film Festival and the Hong Kong International Film Festival.

===Critical reception=== Thai cinema, Mysterious Object at Noon received little attention in the directors native country. However through film festival screenings overseas, the film gained positive notice from film critics.

"Mr. Weerasethakuls film is like a piece of chamber music slowly, deftly expanding into a full symphonic movement; to watch it is to enter a  , retrieved 2007-03-27. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 