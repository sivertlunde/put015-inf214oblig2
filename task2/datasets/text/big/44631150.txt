A Daughter of Confederacy
{{infobox film name = A Daughter of the Confederacy image = Daughter_Confederacy_Wiki.jpg caption = Gene Gauntier(left) and Jack J. Clark. director = Sidney Olcott producer = Gene Gauntier Feature Players writer = Gene Gauntier starring = Gene Gauntier Jack J. Clark distributor = Warners Features cinematography = 
| released =  
| runtime = 3000 ft
| country = United States language = Silent film (English intertitles) 
}}

A Daughter of the Confederacy is a 1913 American silent film produced by Gene Gauntier Feature Players and distributed by Warners Features. It was directed by Sidney Olcott with Gene Gauntier and Jack J. Clark in the leading roles.

==Cast==
* Gene Gauntier - Nan, the Girl Spy
* Jack J. Clark - Captain Allison

==Production notes==
* The film was shot in Jacksonville, Fla.
* It is the first film produced by Gene Gauntier Feature Players.

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 
 


 