Ochtendzwemmers
{{Infobox film
| name           = Ochtendzwemmers
| image          = Ochtendzwemmers.jpg
| image_size     = 
| caption        = 
| director       = Nicole van Kilsdonk
| producer       = 
| writer         = Mieke de Jong
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2000
| runtime        = 90 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}} Dutch film directed by Nicole van Kilsdonk.

==Plot==
Every morning is a fixed group of people in the local swimming pool. None of the regulars know each other, only by sight. Yet the police here think otherwise, who do a morning raid on a suspect and believe everyone is together in what they constitute a criminal organization. One of the main suspects is Loes, who takes her story to the detective, her story seems to resolve the whole thing but then it is suspected that she is a racist.

==Cast==
*Ricky Koole	... 	Loes
*Daniël Boissevain	... 	Bing (as Daniel Boissevain)
*Felix Burleson	... 	Ampie Sylvester
*Viggo Waas	... 	Frankie
*Adriaan Olree	... 	Te Bokkel
*Tatum Dagelet	... 	Tanja
*Frank Lammers... 	Herman
*Josh Meyer... 	Kenneth
*Dennis Rudge		
*Olga Zuiderhoek... 	Moeder Bakker
*Jetty Mathurin	... 	Desiree
*Dimme Treurniet	... 	Rechercheur
*Juda Goslinga	... 	Marco
*John Serkei	... 	Charley
*Dennis de Getrouwe	... 	Dennis

== External links ==
*  

 
 
 


 