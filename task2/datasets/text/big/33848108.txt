Crime Busters
{{Infobox film
| name = Crime Busters I due superpiedi quasi piatti
| image = Crime Busters.jpg
| caption =
| director = E.B. Clucher
| writer = E.B. Clucher
| starring = Terence Hill Bud Spencer
| music = Guido De Angelis Maurizio De Angelis
| cinematography = Claudio Cirillo
| editing =
| producer =
| distributor =
| released =  
| runtime = 110 minutes
| awards =
| country = Italy United States
| language = Italian English
| budget =
}} Italian action-comedy movie directed in 1977 by E.B. Clucher.
 Golden Screen The Exorcist and The Towering Inferno. 

==Plot==
Wilbur Walsh (Bud Spencer) and Matt Kirby (Terence Hill) are in Miami, looking for work as longshoremen, but the area is managed by shady dealers who refuse to give them a job, after which the dealers are beaten up and have three of their cars wrecked in the process. Walsh and Kirby meet up and then leave the dock, tired of looking for a job. Matt is particularly intrigued by the closed nature of Wilbur, who tries to avoid it in any way. Matt, after introducing himself, suggests that Walsh and he should work together on something he had been planning; the robbery of a supermarket. Walsh accepts, aided by Kirbys conniving ways to remove police attention, but by mistake the two end up in the police station and, to prevent being locked away, they say that they want to become police officers, which is granted. Both Matt and Wilbur complete their training, even though they differ from the other recruits for their unorthodox methods of making an arrest and overall rebellious nature, eventually being on real service. During their job, one of the two friends becomes familiar with a family of Chinese, whose uncle was killed by unknown assailants. Upon investigation, the two come to face the same ruffians that had spread to the port and denied them a job before. In fact, the criminals will be the key of "the two superpied almost flat" to get to the heart of the gang of traffickers.

== Cast ==
* Bud Spencer: Wilbur Walsh
* Terence Hill: Matt Kirby
* David Huddleston: Captain McBride
* Luciano Catenacci: Fred Clyne
* Riccardo Pizzuti: Freds henchman
* Jill Flanter: Galina Kocilova
* April Clough: Angie Crawford
* Laura Gemser: Susy Lee
* Luciano Rossi: John Philip Forsythe, alias Geronimo

==References==
 

==External links==
* 
 
 

 
 
 
 
 
 
 
 
 
 
 

 