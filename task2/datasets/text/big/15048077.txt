Komisario Palmun erehdys
{{Infobox Film
| name           = Komisario Palmun erehdys
| image          = 
| image_size     = 
| caption        = 
| director       = Matti Kassila
| producer       = Toivo Särkkä
| writer         = Matti Kassila Kaarlo Nuorvala Mika Waltari (novel)
| starring       = Joel Rinne Matti Ranin Leo Jokela Jussi Jurkka Leevi Kuuranne Elina Pohjanpää
| music          = Osmo Lindeman
| cinematography = Olavi Tuomi
| editing        = Elmer Lahti
| distributor    = Suomen Filmiteollisuus
| released       =  
| runtime        = 103 minutes
| country        = Finland Finnish
| budget         = 
| gross          = 
}}
 Finnish crime film directed by Matti Kassila for Suomen Filmiteollisuus. It is set in 1930s Helsinki and centers around Inspector Palmus investigation of the murder of rich and decadent Bruno Rygseck. It is based on Mika Waltaris 1940 novel of the same name, and was the first film adaptation of his Inspector Palmu novels.

The film was followed by three sequels, Kaasua, komisario Palmu! (1961), Tähdet kertovat, komisario Palmu (1962) and Vodkaa, komisario Palmu (1969), which were produced by a different studio, Fennada-Filmi, but directed by Kassila and featured the same core cast. It has enjoyed great popularity over the years, and in 2012, it was voted the best Finnish film of all time by Finnish film critics, journalists and bloggers in a poll organized by Yle Uutiset. 

==Plot==
The film opens with a scene of guests arriving at the crime-themed dinner party of Bruno Rygseck, the rich and decadent heir of the Rykämö Concern (business)|concern. The guests are his cousins Airi and Aimo Rykämö, Airis fiancé Erik Vaara, who works for the concern and strongly dislikes Bruno, and Irma Vanne, the daughter of vuorineuvos Vanne. In order to scare them as they arrive, Bruno has dressed up as the Grim Reaper. 
 , Helsinki, photographed in 1915. The second building from the left was used for the exterior shots of Bruno Rygsecks townhouse.]] coke was delivered in the early morning and have since been in the basement. Although Laihonen does not know Bruno or Vanne from before, he had been invited to the house by her to get back the stolen manuscript of his unpublished novel.

The atmosphere in the house becomes increasingly hostile towards the investigation as Palmu asks more detailed questions about the mornings events. He learns that the previous night, Aimo had stolen Amalias cat and brought it with him to the dinner party, where Bruno had poisoned it and invited her to view the cadaver for his amusement. Despite Palmus suspicions, the investigation is closed due to pressure from the powerful Rygsecks and because there is no evidence to prove that the death was not accidental. Laihonen asks the policemen and Vanne to join him to the luxurious Hotel Kämp for a late lunch.

At Kämp, Vanne tells the policemen more about the previous night. They had been playing a game in which each contestant has to commit a crime that the victim cannot report to the police. The winner was to be chosen by Vaara at the party. Her crime had been to steal the manuscript; Aimos to steal the cat; and Airis to have ten of Aimos promissory notes signed by Bruno. She had refused to reveal how she had gotten them, other than that it was blackmail. Bruno had then asked Vaara to come to his bedroom in order to show him his crime in private. Afterwards, Vaara had stormed out of the house in fury.

When Palmu and the detectives arrive back at the police station late in the afternoon, they are told that Alli Rygseck has been poisoned with prussic acid mixed in her absinthe. She had been back at the Rygseck house to discuss with the family members who was to inherit Bruno: she had insisted that she should get the house. Brunos case is now also re-opened, and the policemen head back to the house. Palmu interrogates Airi about the promissory notes, and she reveals that Aimo had been forging Brunos signature to pay off his gambling debts. Bruno had told her that he would contact the police about it unless she were to agree to do something, although she refuses to specify exactly what. In Brunos bedroom, Palmu finds an album of nude photographs he had taken of his female friends, with one page torn off.

That evening, the policemen meet Brunos uncle Gunnar Rygseck, the head of the Rykämö concern, at his office. He tries to bribe Palmu and claims that Bruno was suicidal and had intended the poisoned absinthe for himself before dying accidentally. Next, Palmu confronts Vaara, whose office is in the same building, about what Bruno showed him. It is revealed to have been a nude photograph of Airi, which Palmu notices is a forgery. He tells Vaara that Aimo killed Bruno and that Airi will be imprisoned as an accomplice as she has tried to protect her brother: this leads Vaara to confess Brunos murder. Palmu asks for Airi, who also works for the concern, to come to Vaaras office. She confirms that the photograph is forged, and finds the idea that her brother killed Bruno laughable. Vaara takes back his confession, and Palmu admits that he never truly believed either him or Aimo to be the murderer.

Palmu calls the Rygseck house, where Amalia is moving in, and is told by her that Veijonen has disappeared. Palmu goes to interrogate Vanne again, and she confesses to have in fact secretly stayed in Alli Rygsecks old bedroom on the night of the party. When she walked through the corridor leading to the bathroom the next morning in order to get to the back door, she thought that a stair creaked behind her, as if someone else was there as well. Palmu places her on house arrest at Laihonens apartment.

In order to get the murderer to act, Palmu sends detective Virta to tell all suspects that Vanne knows something about the murders. He is also to get Brunos photo album from the Rygseck house and to go show it to Vanne. At the house, Amalia convinces Virta to give her his gun for protection as she is scared of Veijonen. When Virta returns to the station, he learns that Veijonen has been caught and is cleared of the murders. Soon after, Palmu calls Laihonen and asks to speak to Vanne. He admits that she has gone to help Amalia search the house – he had lied to the gullible Virta earlier that she was asleep. The police rush back to the Rygseck house, and stop Amalia from shooting Vanne. In the struggle, Virta is shot on the shoulder.
 not of sound mind, she is not prosecuted but is left to the care of her brother.

==Cast==
* Joel Rinne as Inspector Frans J. Palmu
* Matti Ranin as Detective Toivo Virta
* Leo Jokela as Detective Väinö Kokki 
* Jussi Jurkka as Bruno Rygseck
* Saara Ranin as Amalia Rygseck
* Elina Salo as Airi Rykämö
* Pentti Siimes as Aimo Rykämö
* Leevi Kuuranne as Veijonen
* Elina Pohjanpää as Irma Vanne
* Matti Oravisto as Erik Vaara
* Leo Riuttu as K. V. Laihonen
* Aino Mantsas as Alli Rygseck
* Arvo Lehesmaa as Gunnar Rygseck
* Risto Mäkelä as Police chief Hagert
* Pentti Irjala as Doctor Dahlberg
* Toivo Mäkelä as Pianist at Hotel Kämp
* Irja Rannikko as Brunos cook
* Anton Soini as Coke delivery man
* Arttu Suuntala as Driver of coke truck

Uncredited
* Kaarlo Wilska as a policeman
* Oiva Luhtala as a policeman
* Arno Carlstedt as a man in Esplanadi|Café Kappeli
* Leo Lastumäki as a carpenter
* Tauno Söder as a crime scene investigator
* Leo Pentti as a crime scene investigator
* Paavo Hukkinen as a crime scene investigator
* Arvo Kuusla as a crime scene investigator
* Rose-Marie Precht as a girl in Brunos swimming pool
* Katriina Rinne as a girl in Brunos swimming pool
* Esko Salminen as a boy in Brunos swimming pool
* Eero Maijala as a boy in Brunos swimming pool
* Orma Aunio as a boy in Brunos swimming pool

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 