Teddy Bears' Picnic (film)
{{Infobox film
| name = Teddy Bears Picnic
| image = Teddybearspicnicdvd.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Harry Shearer
| producer = Marc Ambrose
| writer = Harry Shearer see below
| cinematography = Jamie Reynoso Jeffrey Ford
| studio = Century of Progress Productions
| distributor = Magnolia Pictures
| released =  
| runtime = 80 minutes
| country = United States
| language = English
| budget = $800,000 
| gross = $28,149 
}}
Teddy Bears Picnic is a 2002 American comedy film written and directed by Harry Shearer. It was released in May 2002 to limited audiences. Shearer has a small role in the film. It is also Kenneth Mars last film before his death.   

==Plot==
Teddy Bears Picnic covers an annual encampment of prominent male leaders at the Zambezi Glen, a thinly-veiled reference to the Bohemian Grove.  

The film starts out with the first ever womens day at the glen, where wives and girlfriends of Zambezi members are invited to visit the glen ahead of the annual encampment, which also serves to introduce the glen and the characters to the audience. The actual retreat itself begins after the members have returned without any women and kicks off with the "Assassination of Time", based on the real Cremation of Care at the Bohemian Grove, with a pelican replacing the latters owl. After that, the festivities begin, including an all-male chorus line in drag, which is photographed by one of the club employees who smuggles out the pictures to the news media.

This violation of the privacy of the glen causes the leaders of the membership to work on spin control, while the employee who took the pictures is emboldened by his success and the promise of a hefty reward to record footage of the glen with a camera smuggled in with the help of a local newscaster. In this time we also see what members do to enjoy themselves at the retreat, including drinking, urinating on trees while naked, and visiting nearby prostitutes.

After filming some of the activities at the glen, the cameraman is spotted by some members and flees in to the woods. From here, the members invoke their privilege and connections, with disastrous results.  The members call in the military to track down the cameraman with dogs, flares, and helicopters, which sets off a forest fire. When the road out of the glen is blocked by an overturned truck filled with drinks for the glen members, one of the characters orders his chauffeur to drive through anyway, making the blockage worse. A helicopter flying without lights at night at the behest of one of the members collides with a news helicopter covering the fire.

==Cast==
 
* John Michael Higgins as Whit Summers
* Ming-Na Wen|Ming-Na as Katy Woo
* Henry Gibson as Clifford Sloane
* David Rasche as Elliot Chevron
* Brenda Strong as Jackie Sloane Chevron
* Judith Owen as Nancy McMahon
* Robert Mandan as Stanton Vandermint
* Morgan Fairchild as Courtney Vandermint
* Michael McKean as Porterfield "Porty" Pendleton
* Alan Thicke as himself
* Harry Shearer as Joey Lavin
* Justin Kirk as Damien Pritzker
* Travis Wester as Denny OLeary
* George Wendt as General Edison "Pete" Gerberding
* Kenneth Mars as Gene Molinari
* Bob Einstein as Dom Molinari
* John OHurley as Earle Hansen
* Howard Hesseman as Ted Frye
* Fred Willard as Senator Roger Dickey
* Annabelle Gurwitch as Jennifer Gersh
* Joyce Hyser as Rita DOnofrio
* Kurtwood Smith as Secretary of Transportation William Easter
* Dick Butkus as himself Larry Miller
 

==Production==
Harry Shearer wrote, directed, and executive-directed the film.    He hired old friends and colleagues (Michael McKean, Howard Hesseman, and Fred Willard) and younger performers (Annabelle Gurwitch as a camp follower and Justin Kirk as a disgruntled part-time employee who tries to smuggle videotape of the glens strange rites to a local television station). He paid everyone "low-budget scale," Shearer said. 

The film was shot in less than three weeks, at a cost of $800,000.    Shearer funded the film; he said that the money came from the Fox Broadcasting Company, for his voiceover work on The Simpsons: "Thats why theres a Thank You to Rupert Murdoch at the end of the credits – its his money, he just doesnt know it. It just kind of flowed through me." 

The film was shot using high-definition video equipment.  Cinematographer Jaime Reynoso shot the film; post-production work was done by Visionbox Pictures. 

==Distribution==
The film was screened at the U.S. Comedy Arts Festival in February 2001,  the USA Film Festival in April 2001  and at the St. Louis International Film Festival in November 2001.  It had a limited released to theaters in March 2002. 

==Critical reception==
The film currently has a 0% rating on Rotten Tomatoes.  On Metacritic, the film carries a 32/100 rating, indicating "generally unfavorable reviews". 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 