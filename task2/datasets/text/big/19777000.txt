Hendes store aften
 
{{Infobox Film
| name           = Hendes store aften
| image          = 
| image size     = 
| caption        = 
| director       = Annelise Reenberg
| producer       = Poul Bang John Olsen Annelise Reenberg
| narrator       = 
| starring       = Helle Virkner
| music          = Sven Gyldmark
| cinematography = Kjeld Arnholtz
| editing        = Anker Sørensen
| studio         = Saga Studio
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Hendes store aften is a 1954 Danish romance film directed by Annelise Reenberg and starring Helle Virkner.

==Cast==
* Helle Virkner - Marianne Friis
* Poul Reichhardt - John Bagger
* Johannes Meyer - Vicevært Julius Jensen
* Karin Nellemose - Ejer af Chez Madame
* Angelo Bruun - Hr. Poul
* Bodil Steen - Frk. Lilly
* Olaf Ussing - Sagfører Simonsen
* Lili Heglund - Kunde
* Poul Müller - Overtjener på Nimb
* Ib Schønberg - Himself
* Wandy Tworek - Himself
* Gaby Stenberg - Yvonne
* Betty Lange - Fru Simonsen
* Edith Hermansen

==External links==
* 

 
 
 
 
 
 
 