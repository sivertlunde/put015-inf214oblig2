The Garden of Sinners: Paradox Spiral
{{Infobox film name           = The Garden of Sinners: Paradox Spiral director       = Takayuki Hirao producer       = {{Plainlist|
*Atsuhiro Iwakami
*Hikaru Kondo
}} screenplay     = Masaki Hiramatsu story          = Kinoko Nasu based on       =   starring       = {{Plainlist|
*Maaya Sakamoto
*Kenichi Suzumura
*Takako Honda
*Tetsuya Kakihara Joji Nakata Koji Yusa
}} music          = {{Plainlist|
*Yuki Kajiura
*Kalafina
}} studio         = ufotable distributor    = Aniplex released       =   runtime        = 114 minutes country        = Japan language  Japanese
}}
  is a 2008 Japanese   (2008) and followed by   (2008).

==Plot==

===Shikis arc===
In the middle of the night, Tomoe Enjo stabs his parents to death in a fit of rage, before escaping his apartment in a panic. Not long after, a homeless burglar finds their bodies, but when he returns with the police, to his surprise, he finds them being greeted by the residents in perfect health.

Sometime later, Shiki helps fend off some school bullies that were attacking Tomoe. Seemingly taken by Shikis presence, he quickly asks her to help hide him, ecstatically claiming that he is a murderer. Shiki agrees, and simply cites she “is the same as him” when he is surprised by her easy acceptance of his status.

For the next month, Tomoe stays at Shikis place, their life punctuated with Shikis nightly sojourns, and Tomoe waiting for the news report of his parents murder. During this time, Tomoe notices a strange man in a red hat and coat following Shiki. Tomoe warns her of this fact, which she idly dismisses. When they get into an argument over this, Tomoe claims he loves her, and as he lacks any worth, hed be willing to die for her. Shiki refuses the offer, and asks him to consider where he feels his real home is. Tomoe feels then that he cannot hide any longer with Shiki, and leaves.

Not long after, Tomoe is shocked to see his mother still alive. Confused, he returns to Shiki. To confirm Tomoes murder, Shiki accompanies him in returning to his residence at Ogawa apartment: an ominous, circular complex colored red, which was completed recently.

When they enter the elevator, Shiki notices they are ascending in a spiral. Reaching the 4th floor, Shiki insists on not ringing the doorbell, and simply entering Tomoes home. Inside, they are met with Tomoes abusive family, even another Tomoe, and watch the familys final moments as Tomoes mother succumbs to murder-suicide. Shiki explains that these are simply imitation puppets that are revived in the morning and are forced to relive their final day alive repeatedly. Because Tomoe did not ring the doorbell, they continued to act as if they had no visitor.

Shiki then brings Tomoe to the opposite side of the apartment, his “real home”. She explains that when elevator began operating, due to the construction of the complex, none of the residents realized the rotating elevator made them exit 180 degrees, into another set of flats. When they enter Tomoes original residence, they find the rotted corpses of the parents. This side of the apartment is used to store corpses. However, this intrusion also causes the undead puppets of the other deceased apartment residents to attack them when they exit. Shiki easily cuts them down, but afterwards she is confronted by Soren Araya. It is revealed in a short flashback that sometime during the second movie, Shiki had previously fought Soren.

Soren explains that he controls the apartment, as part of an experiment that uses the building to simulate a “miniature world that concludes in a day”. After driving all the occupants to kill one another, he has been making them repeat their deaths over the past half year, hoping for a deviation in their deaths. He also reveals that he was the one who manipulated Kirie and Fujino in the previous movies to attack Shiki, as antithesis to her, in hopes he can force her to recognize her own “Origin”. As they are meeting early, Soren decides to capture her now to reach the spiral of origin.

Shikis mystic eyes are unable to perceive lines on Sorens body, due to his great age (his Origin being “Stillness”) and mystical artifacts he grafted onto his left arm in preparation. She attacks, regardless, even able to cut through his defenses, but in the end, she is defeated by Soren, and absorbed into the building.

Tomoe, the lone witness of all this, grabs Shikis knife and escapes back to her home.

===Mikiyas arc===
Toko receives a tip from a friend on the police force rergarding a mysterious report made by a burglar that upon entering an apartment, he found a middle-aged couple murdered. However, when the police follow up and ring the doorbell of their apartment, the man who was seen dead by the burglar answers as if nothing was wrong. A particular interest is taken to Tomoe, the son of the couple, who is missing. Toko requests that Mikiya, who just returned from a month of driving school in the suburb to obtain his license, investigate the situation.

Mikiya finds the schematics of the strange apartment building, two separate semi-circular buildings that are separate apart from the lobby containing only one central elevator, and reports this to both Toko and Shiki. Apparently, the builder meant for the construction to be for a company dormitory, but instead opened it to the public. Mikiya also reports that the elevator was inoperable for the first month residents lived in the building and also manages to find background details on 30 of the 50 families residing within the building.

Mikiya is also given a katana by Shikis family to deliver to her. Toko requests that she not assemble it within the office, as its age would cause all of the magical barriers to break. Mikiya also makes arrangements to meet Shiki, but is surprised to find her apartment locked, although there was never a lock on the door in the past.

Toko and Mikiya set out and investigate the building, Toko reveals that she helped design it and there is a reason only 30 of the 50 family backgrounds could be found, the others were faked using certificates of already deceased people. Mikiya is automatically affected by the strange buildings architecture and design, which Toko explains were purposely designed in that manner to make the residents go insane. As Mikiya exits the elevator on the 4th floor, he is confused to find himself on the wrong side of the building than the blueprints noted, and is further confused when he checks on the residents of apartment 405, Tomoes family, and finds them there, although they are supposed to be living in the opposite building in apartment 410. When asked by Toko to meet him one flight up the stairs, Mikiya is surprised to find that he arrived on the 6th floor instead of the 5th, although seemingly only going up one floor of the building. Toko tells him the confusion is due to the elevator rotating 180 degrees while lifting and augmentation to the stairway, which Mikiya deduces as pistons raising the stairwell one level, therefore effectively "switching" the apartments of the residents to the other half of the building after the completion of the elevator.

Mikiya then visits Shikis apartment again but finds Tomoe there instead. He tells Tomoe that he is going to look for the now missing Shiki and questions Tomoe on his motives for helping to rescue Shiki. Mikiya brings Tomoe with him on the condition they make one stop first, despite disagreeing with Tomoes sentiments that this is something he is just doing for Shikis sake. Mikiya surprises Tomoe by bringing him to his childhood home where Tomoe regains his memories and realizes that he "has a home" and will fight for his own sake, not Shikis. During the flashback, we learn that Tomoes key was important to him because his father told him that it was a symbol of how the members of their family protected each other.

Mikiya and Tomoe proceed to the apartment complex to rescue Shiki. They separate before entering, deciding to take separate routes, and agree to never search for each other again after parting so that neither one would feel guilty if something happened to the other. After bidding farewell to Tomoe, Mikiya enters the building and is confronted by Cornelius, a Magus from Tokos past, who assumes Mikiya is Tokos apprentice. Although Mikiya manages to stab Cornelius, it does no good. Cornelius superimposes his hatred for Toko on to Mikiya, even acting as if he were Toko, while slamming his head into a wall continuously, knocking Mikiya unconscious.

===Final arc===
Toko arrives before Mikiya and Tomoe, but is killed in battle. Her head is kept alive in a jar temporarily, but Cornelius insists on killing her himself. Cornelius leaves the basement with Tokos head in tow. He meets Mikiya, who is heading in the front entrance as a distraction for Tomoe. Meanwhile, Tomoe has snuck into the underground lab, where he finds his brain in a jar and confronts Soren, whose organs are hardwired into the building—his "body" is a puppet. Tomoe heads to the tenth floor, where Shiki is, bringing the katana from earlier. Tomoe stabs Soren in the heart but Soren kills him, but he sees a blurry figure that looks like Shiki in front of Soren just before he dies.

Toko arrives again, explaining to Cornelius that she had made a puppet that was an exact copy of herself. Cornelius realizes that killing her old body was the trigger that awakened the new one. Toko kills Cornelius, and is confronted by Soren as she is tending to Mikiyas head injury. Soren asks whether Toko intends to oppose him; she replies that she had already lost, when she died. She says that the one would stop him would be Shiki. Then she guesses of Sorens real purpose on apartment complex by representing the Taiji in order to take in the Taiji (using Shiki (yin) to get into the Spiral of Origin and end the world). Soren replied yes and says he has sealed her outside of space, which incites Toko to laugh derisively. She explains that he would have been better off sealing her in concrete, as no such barrier would hold Shiki for long. As if on cue, Shiki arrives with the katana to duel with Soren and Shiki finally kills him. As Soren was dissolving slowly, he discusses with Toko his reason of ending the world is that he wanted to records the deaths of all humans and a conclusion to his deed. With that he achieves happiness for giving meaning to meaningless deaths of the human race.

We see a sequence with Shiki and Tomoe as each others mirror images as these represents two sides of the Taiji; it is not clear whether this is real or imagined. The credits roll.

After the credits, Mikiya arrives at Shikis apartment, and uses the key Tomoe had given him to get inside. Shiki remarks that it is unfair that she doesnt have a key to his apartment.   

==Cast==
 
*Maaya Sakamoto as  
*Kenichi Suzumura as  
*Takako Honda as  
*Tetsuya Kakihara as   Joji Nakata as   Koji Yusa as Cornelius Alba

==References==
 

==External links==
*   
*   
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 