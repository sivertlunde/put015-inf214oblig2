Le Doulos
 
{{Infobox film
| name =Le Doulos
| image = Ledoulos.jpg
| caption =
| director = Jean-Pierre Melville
| producer = Carlo Ponti Georges de Beauregard
| writer = Pierre Lesou novel (credited) Jean-Pierre Melville
| starring =Jean-Paul Belmondo Serge Reggiani
| cinematography = Nicolas Hayer
| music = Jacques Loussier Paul Misraki
| editing = Monique Bonnot
| distributor = Pathé Contemporary Films
| released = 1962
| runtime = 108 min
| country = France
| language = French
| budget = gross = 1,477,619 admissions (France) 
}}
Le Doulos ( ) is a 1962 French crime film directed by Jean-Pierre Melville. It was released theatrically as The Finger Man in the English-speaking world, but all video and DVD releases have used the French title. Intertitles at the beginning of the film explain that its title refers both to a kind of hat and to the slang term for a police informant|informer.

Le Doulos is based on a novel by Pierre Lesou.  While the film comes before what are considered Melville’s masterpieces of the genre, Le Samouraï (1967) and Le Cercle rouge (1970), one can unmistakably observe several of Melville’s trademark techniques in this film.

 
== Plot ==
The narrative unfolds through two characters, Maurice and Silien, and consistently switches back and forth between them, leading the audience to grasp randomly for a distinct main character or hero (despite the fact that both are criminal anti-heroes).  Through Maurice and Silien’s actions, the film explores just how deeply qualities such as friendship and loyalty run.

Le Doulos begins by introducing us to Maurice, an ex-con, just released from prison after serving a six-year sentence.  He then murders his friend Gilbert, stealing the jewels he had been hiding, that were proceeds from a recent heist.  Shortly afterwards, Maurice plans a heist of a rich man’s estate and shares his plan with Silien, who is rumored to be a police informant.  Silien is later picked up and questioned by the police.  The film unfolds from there, incorporating a number of plot twists revealed through Melville’s traditionally styled hard-boiled dialogue and picturesque visuals.

==Principal cast==
{| class="wikitable"
!Role
!Actor
|- Silien
|Jean-Paul Belmondo
|- Maurice Faugel Serge Reggiani
|-
|Rémy Philippe Nahon
|-
|Thérese Monique Hennessy
|- Superintendent Clain Jean Desailly
|- Gilbert Varnove
|René Lefèvre (actor)|René Lefèvre
|- Jean
|Phillipe March
|- Nuttheccio
|Michel Piccoli
|}


==Reception==
Le Doulos ranks at number 472 in Empire magazine|Empire magazines 2008 list of the 500 greatest movies of all time. 

==Legacy== 36 Quai des Orfèvres, Olivier Marchal uses the name Silien for his police informant. (source 36: Film Notes by Miles Fielder) 

American filmmaker Quentin Tarantino cited the screenplay for Le Doulos as being his personal favorite and being a large influence on his debut picture Reservoir Dogs 

== References ==
 

==Further reading==

* Nogueira, Rui (ed.). 1971. Melville on Melville. New York: Viking Press.
* Vincendeau, Ginette. 2003. Jean-Pierre Melville : an American in Paris. London: British Film Institute. (ISBN 0851709508 (hardbound), ISBN 0-85170-949-4 (paperback))

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 