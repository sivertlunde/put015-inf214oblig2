My Kingdom (film)
 
  British crime film directed by Don Boyd and starring Richard Harris, Lynn Redgrave and Jimi Mistry.

It premiered at the 2001 Toronto International Film Festival but failed to make an impression   The following year My Kingdom grossed $2,607 on its opening weekend in Los Angeles for an eventual domestic gross of $4,296 in its US release. 

The film, co-scripted by Boyd with Nick Davies and drawing on both their researches into the London and Liverpool criminal underworld (which in Boyds case included the Kray twins | Kray bothers), brought Boyd into conflict with its principal lead Richard Harris, who wanted to rewrite the script.   The film subsequently received mixed reviews while generally acknowledging a fine performance from Harris who was nominated for a British Independent Film Award.  

==Cast==
* Richard Harris ...  Sandeman
* Reece Noi ...  The Boy
* Lynn Redgrave ...  Mandy Tom Bell ...  Quick
* Emma Catherwood ...  Jo
* Aidan Gillen ...  Puttnam
* Louise Lombard ...  Kath
* Paul McGann ...  Dean
* Jimi Mistry ...  Jug
* Lorraine Pilkington ...  Tracy
* Colin Salmon ...  The Chair
* James Foy ...  Animal
* James McMartin ...  Mineral
* Danny Lawrence ...  Tigger
* Gerard Starkey ...  Minder
* Sasha Johnson Manning ...  Soprano
* Seamus ONeill ...  Snowy Chris Armstrong ...  Dutch farmer
* Ingi Thor Jonsson ...  Dutch farmer
* Otis Graham ...  Delroy
* David Yip ...  Merv
* Kieran OBrien ...  Photographer
* Jack Marsden ...  Billy the whizz
* Amer Nazir ...  Mutt
* Mushi Noor ...  Jeff
* Carl Learmond ...  Rudi
* Anthony Dorrs ...  Skunk
* Steve Foster ...  Toffee
* Oscar James ...  Desmond
* Sylvia Gatril ...  Brothel receptionist
*Sharon Byatt ...  Annie
*Desmond Bayliss ...  John the dog
* Kelly Murphy ...  Karen
* Leanne Burrows ...  Miss Joy

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 