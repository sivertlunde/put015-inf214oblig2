Unnidathil Ennai Koduthen
{{Infobox film
| name = Unnidathil Ennai Koduthen
| image =
| director = Vikraman
| writer = Karthik Ajith Roja Ramesh Khanna
| producer = G. Venugopal K. Muralidaran
| studio = Lakshmi Movie Makers
| cinematography = M. S. Annadurai
| music = S. A. Rajkumar
| distributor =
| released = August 15, 1998
| runtime = 165 mins
| language = Tamil
| country = India
}}
 Karthik and Roja in the lead roles, with Ajith Kumar in a guest appearance. Ramesh Khanna, Moulee and Madhan Bob played other supporting roles. The film went on to win critical acclaim and was declared a commercially successful venture at the box office.

==Plot==
Radha (Roja (actress)|Roja) is an illegitimate daughter who lives in her fathers house as a maidservant with her step mother and father. The step mothers brother, Sanjay (Ajith Kumar) comes from Canada to visit Roja and her family. He said he likes her but needs at least 3 months to decide if she has the right qualities in a wife before he agrees to marry her. One day, two thieves Selvam (Karthik (actor)|Karthik) and his friend steals a God statue from a temple and hide inside Radhas fathers house while the elders are away. Radha finds them and lock them inside the house for 3 days. After 3 days, Radha gives Selvam some meaningful advice before he leaves. Selvam seems to have a liking of Radha and later he comes to know about Radhas story about her family and Sanjay. One day, her step mother thrown her out of the house for accusing her of stealing her gold necklace and leaving a cigarette in a drawer. Selvam then finds a place for her in a hostel and makes many sacrifices in order to make her a famous singer. Finally, Radhas family accept her as a daughter and Sanjay is now ready to marry her, but who will Radha choose? 

==Cast==
  Karthik as Selvam
* Ajith Kumar (special appearance) as Sanjay Roja as Radha
* Ramesh Khanna as Selvams friend
* Moulee as Viswanathan, Radhas father
* Sathyapriya as Radhas stepmother
* Madhan Bob as Radhas uncle
* Fathima Babu as Radhas aunt Sheela
* Vaiyapuri as Delhi
* K. Natraj
* Singamuthu as police constable
* Kovai Senthil as house owner
* R. Sarathkumar in a cameo appearance
* Kushboo in a cameo appearance
* K. S. Ravikumar in a cameo appearance Deva in a cameo appearance
* S. A. Rajkumar in a cameo appearance
* Gangai Amaren in a cameo appearance
* James Vasanthan in a cameo appearance
 

==Production== Karthik starrer Vijay in Meena also revealed that she had to reject an offer to star in the leading female role due to date problems. 

Ajith Kumar later revealed that his role in the film was initially supposed to be a full length role but the character underwent changes after the film started. He continued to play his part in the film due to his admiration for co-actor, Karthik (actor)|Karthik.  The success of Unnidathil Ennai Koduthen, later led to Karthik making a guest appearance in the Ajith-starrer Anantha Poongathe. 

==Release== Third Best Best Storywriter Best Cinematographer a Special Best Actress Best Film, Best Actor Best Actress for the respective members in the film.  The film gave a fresh lease in their careers to the lead pair, while Vikramans work won him further recognition.  Furthermore Ramesh Khanna, after his new gained recognition, was able to find a financier to make his directorial debut Thodarum. Indolink.com rated the film amongst the "best of 1998", revealing that the film ran over 200 days in theatres across Tamil Nadu. 

Indolinl.com gave the film an above average review mentioning that "All the actors perform their part well. The story places the characters in situations where they behave predictably or in some cases where they are not true to themselves. Other than that minor snitch, the movie is quite entertaining.". 

==Remakes== Telugu remake Venkatesh and Soundarya in lead roles, where it enjoyed a similar reception.  It was remade in Kannada as Kanasugara with V. Ravichandran. It was also remade in Bengali and Oriya as Shakal Sandhya and Mo Duniya Tu Hi Tu respectively.

==Soundtrack==
{{Infobox album|  
  Name        = Unnidathil Ennai Koduthen
|  Type        = Soundtrack
|  Artist      = S. A. Rajkumar
|  Cover       =
|  Released    = 1998
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       = Lucky Audio
|  Producer    =
|  Reviews     =
|  Last album  =
|  This album  = Unnidathil Ennai Koduthen (1998)
|  Next album  =
}}
The soundtrack of the film was composed by S. A. Rajkumar, who also played a cameo as himself in the film alongside music directors Gangai Amaren and Deva (music director)|Deva.

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    =
| title1          = Edho Oru Paatu Hariharan
| length1         = 4:26
| title2          = Edho Oru Paatu Sujatha
| length2         = 4:26
| title3          = Kaatrukku Thudivittu
| extra3          = P. Unni Krishnan|Unnikrishnan, K. S. Chithra
| length3         = 4:42
| title4          = Malligai Poove Malligai Poove
| extra4          = Unnikrishnan, Sujatha
| length4         = 4:26
| title5          = Thottabedda Kuliru
| extra5          = Hariharan
| length5         = 4:16
| title6          = Vaanam Bhadiyen
| extra6          = Sujatha
| length6         = 4:17
}}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 