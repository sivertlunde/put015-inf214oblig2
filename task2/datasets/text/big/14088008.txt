Summer's Tail
{{Infobox film
| name           = Summers Tail
| image          = Summer-s-tail-2007-2.jpg
| image_size     = 
| caption        = Taiwan film poster
| director       = Cheng Wen-Tang
| producer       = Tsui Siuming Enno Jan Fu-Wha Cheng Wen-Tang
| narrator       =  Enno Hannah Lin Han Enno
| cinematography = Lin Cheng-Ying
| editing        = Lei Chen-Ching
| distributor    = 
| released       =  :   Taiwan:  
| runtime        = 98 minutes Taiwan
| Mandarin
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Taiwanese film directed by Cheng Wen-Tang.

==Cast== Bryant Chang Jui-Chia as Jimmy Chan
* Dean Fujioka as Akira Fuwa
* Enno Cheng|Enno as Yvette Chang
* Hannah Lin Han as Wendy Lin
* Christine Ke Huan-Ju as Xiu I-Wei
* Lu Yi-Ching as Yvettes mother
* Yao Hsiao-Min as Boss
* James Wen Sheng-Hao as Willys Father
* Li Hsiu as Yvettes grandmother
* Chen Chih-Chieh as Willy

==Synopsis==

Summer-Flaming, fervid, flowing the youth sweat; every second is rippling of the passion and react. To be duty-bound not to turn back!
 
Tail-A long time ago its said once human being all had but because of not knowing its purpose, tail is gradually vanishing.
 
But, actually, each person all has a tail.
 
In the eyes of her classmates, Yvette Chang(Enno) is a rock and roll girl, she has a cat by the name of "Summer", because of a congenital heart condition, she has left school and spends much of her time every day with Summer messing around, playing the guitar, and getting out and about. During the time when she is not in school, every day is filled with discovery, and since the specially gifted and talented classs student Jimmy Chan(Bryant Chang), fell in love with a teacher, and met the fate of being expelled from the school, now the streets have one more high school student on the roam. Yvette willingly opens her arms to embrace all of Jimmys emotions, and comfort him. But Jimmy doesnt quite understand why Yvette, who suffers from cardiac disease, is so full of ambition and warmth for living.
 
Two high school drop-outs are out riding bicycles, when between the rice paddies and the irrigation canals, they find a place free from the control of adults, an undeveloped piece of land near the tracks of the Taiwan High Speed Rail. Under the blue skies, they gather with their classmates from the same school, another gifted and talented student named Wendy(Hannah Lin) and the Japanese exchange student Akira (Dean Fujioka) who loves soccer but is always the last in academic, and together at this secret base they relax, have fun, and fool around. The destiny which has brought the four of them together serves as a powerful force of empathy and encouragement, as they enjoy fun times together, and try to save an embittered suicidal father. Against the backdrop of clear skies, reflecting the visions and secrets lodged in the hearts of four almost grown up children, whether life is normal or exciting, with success or failure, they are always able to enjoy the experience.
 
This demonstrates the ability to make oneself feel joy in living, which is a great gift from above, just like a tail, which you can wag just for fun, when you have time to spare from trying to do something. Actually, we all have a tail, which is also trying to keep us happy.
 
What is the purpose of that tail? Finding your own happiness. 

==References==
 

==External links==
*  
*  
*  

 
 
 