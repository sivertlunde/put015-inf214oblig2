Little Otik
{{Infobox film
| name           = Otesánek
| image          = Littleotik.jpg
| caption        = Film poster for Otesánek
| director       = Jan Švankmajer
| producer       = Keith Griffiths Jaromir Kallista Jan Švankmajer
| writer         = Jan Švankmajer
| starring       = Veronika Zilková Jan Hartl Kristina Adamcová
| music          = Ivo Spalj (sounds)   Carl Maria von Weber (musical score)
| cinematography = Juraj Galvánek
| editing        = Marie Zemanova
| distributor    = Zeitgeist Films
| released       = Czech Republic:   UK: October 26, 2000
| runtime        = 132 minutes
| country        = Czech Republic United Kingdom
| language       = Czech
| budget         =
}}
Little Otik ( ), also known as Greedy Guts, is a 2000 Czech film by Jan Švankmajer and Eva Švankmajerová. Based on the folktale Otesánek by Karel Jaromír Erben|K.J. Erben, the film is a comedic live action, stop motion-animated feature film set mainly in an apartment building in the Czech Republic.   

The film uses the Overture to Der Freischutz (1821) by Carl Maria von Weber as the score.

==Plot==
Karel Horák (Jan Hartl) and Božena Horáková (Veronika Žilková) are a childless couple and for medical reasons are doomed to remain so. While on vacation with their neighbors at a house in the country, Karel decides to buy the house at the suggestion of his neighbor. When he is fixing up the house, he digs up a tree stump that looks vaguely like a baby. He spends the rest of the evening cleaning it up and then presents it to his wife. She names the stump Otík and starts to treat it like a real baby. She then works out a plan to fake her pregnancy and becoming more and more impatient she speeds up the process and gives birth one month early.

Otík comes alive and has an insatiable appetite. Alžbětka (Kristina Adamcová), the neighbors daughter, has been suspicious all along, and when she reads the fairy tale about Otesánek, the truth becomes clear to her. Meanwhile little Otík has been just eating and growing. At one point he eats some of Boženas hair, and another day she returns home to find that Otík has eaten their cat. Karel and his wife are at odds with Karel pushing for killing the thing and Božena defending it as their child. The baby later consumes a postal worker (Gustav Vondráček) and then a social worker (Jitka Smutná).

The resulting deaths lead Karel to tie up and lock Otík away in the basement of their apartment building, leaving Otík to starvation|starve. Alžbětka secretly takes over as prime caretaker. She tries to keep Otík fed with normal human food, but, when her mother stops her, she is forced to drawing straws (matches in this case) to choose a person to feed to Otík. The first victim is an old man and pedophile, Mr. Žlábek (Zdeněk Kozák), and the second victim is Karel himself, who had come with a chainsaw but on seeing Otík calls him "son" and drops the chainsaw. Afterwards, Božena goes into the basement and is heard screaming. In the end, Otík disobeys Alžbětka despite repeated warnings and eats Mrs. Správcovás (Dagmar Stříbrná) cabbage patch, prompting the old woman to take charge.

===Ending===
In the fairy tale upon which the movie is based, the old woman kills Otesánek by splitting his stomach open with a Hoe (tool)|hoe; however, the film ends with her descending the stairs, Alžbětka reciting the end of the fairy tale tearfully; the audience is not allowed to witness the deed.

==Cast==
*Veronika Žilková as Božena Horáková
*Jan Hartl as Karel Horák
*Kristina Adamcová as Alžbětka
*Jaroslava Kretschmerová as Alžbětkas Mother
*Pavel Nový as Alžbětkas Father
*Dagmar Stríbrná as Pani spravcova (the caretaker)
*Zdenek Kozák as Mr. Žlábek
*Gustav Vondracek as Mládek, the postman
*Jitka Smutná as Bulanková, the social worker

==Reception==
The film is critically acclaimed with multiple awards, and it is certified fresh at the film news/reviews website rottentomatoes.com. The Czech Critics Awards named it the best feature film. It won three Czech lions for best art direction (Jan Švankmajer and Eva Švankmajerová), best film, best film poster (Eva Švankmajerová), as well as the Czech Critics Awards.  It was nominated for best actress, best costumes, best director, best screenplay, best sound, and best supporting actress. It also won two awards at the Plzn film festival. 

Little Otik was placed at 95 on Slant Magazines best films of the 2000s. 

==Notes==
 

==External links==
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 