Rebecca of Sunnybrook Farm (1917 film)
{{Infobox film
| name           = Rebecca of Sunnybrook Farm
| image          = Rebecca of Sunnybrook Farm 1917 poster.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Marshall Neilan
| producer       = 
| based on       =  
| writer         = Frances Marion
| narrator       =  Eugene OBrien
| cinematography = Walter Stradling
| editing        = 
| distributor    = Artcraft Pictures Corporation
| released       =  
| runtime        = 70 minutes
| country        = United States Silent (English English intertitles)
| budget         = 
}}

  1917 silent silent comedy-drama of the same name by Kate Douglas Wiggin. This version is notable for having been adapted by famed female screenwriter Frances Marion. The film was made by the "Mary Pickford Company" and was an acclaimed box office hit. When the play premiered on Broadway in the 1910 theater season the part of Rebecca was played by Edith Taliaferro.   

==Cast==
 
* Mary Pickford as Rebecca Randall Eugene OBrien as Adam Ladd
* Helen Jerome Eddy as Hannah Randall
* Charles Stanton Ogle as Mr. Cobb Marjorie Daw as Emma Jane Perkins
* Mayme Kelso as Jane Sawyer
* Jane Wolfe as Mrs. Randall
* Josephine Crowell as Miranda Sawyer Jack McDonald as Reverend Jonathan Smellie
* Violet Wilkey as Minnie Smellie
* F.A. Turner as Mr. Simpson
* Kate Toncray as Mrs. Simpson
* Emma Gordes as Clara Belle Simpson
* Wesley Barry as School Boy/Ringmaster (uncredited)
* Milton Berle as Bit Part (uncredited)
* Zasu Pitts as Undetermined Role (uncredited)

==Production notes==
Rebecca of Sunnybrook Farm was filmed in Pleasanton, California.

==See also==
* Rebecca of Sunnybrook Farm (play)
* Rebecca of Sunnybrook Farm (1932 film)
* Rebecca of Sunnybrook Farm (1938 film)

==References==
 

==External links==
 
*  
*  
*  
*  
*   available for free download from Internet Archive

 

 
 
 
 
 
 
 
 


 