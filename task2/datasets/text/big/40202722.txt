The Pirate Fairy
 
{{Infobox film name           = The Pirate Fairy image          = The Pirate Fairy poster.jpg caption        = Theatrical release poster director       = Peggy Holmes producer       = Jenni Magee-Cook screenplay     = Jeffrey M. Howard Kate Kondell story          = John Lasseter Peggy Holmes Bobs Gannaway Jeffrey M. Howard Lorna Cook Craig Gerber based on       =   starring       = Mae Whitman Christina Hendricks Tom Hiddleston music          = Joel McNeely cinematography =  editing        = Anna Catalano studio         = DisneyToon Studios Prana Studios distributor  Walt Disney Studios Home Entertainment released       =   runtime        = 78 minutes country        = United States language       = English budget         =  gross          = $64,942,074 
}} James Hook.   

==Plot==
 
Zarina (Christina Hendricks), a very smart and inquisitive pixie dust-keeper fairy, is amazed by the magic behind pixie dust and is determined to find out all that it is capable of. After her secretly experimenting with some blue pixie dust and causing an accident in Pixie Hollow, her supervisor Fairy Gary prohibits her from being a dust-keeper. In sorrow, Zarina takes her experiments and leaves Pixie Hollow.

One year later, Pixie Hollow celebrates the Four Seasons Festival, with performances from fairies of all the seasons. During the show, Tinker Bell (Mae Whitman), Silvermist (Lucy Liu), Iridessa (Raven-Symoné), Rosetta (Megan Hilty), Fawn (Angela Bartys), and Vidia (Pamela Adlon) spot Zarina flying around the audience and using some strange pixie dust to summon several poppies that cause everyone to fall asleep, except Tink and her friends who take cover. After Zarina is gone, they realize that she stole all of the blue pixie dust, the only way to make the yellow dust that the fairies use to fly, so they go after her to return it to its rightful place.

They follow Zarina to the coast, where it is revealed that she became the captain of a pirate crew, including the cabin boy, James Hook (Tom Hiddleston). The fairies manage to retrieve the blue dust for a moment, but Zarina gets it back after throwing at them some multicolored pixie dust that magically switches their innate fairy abilities and talents. Tinker Bell becomes a water fairy, Silvermist becomes a fast-flying fairy, Fawn becomes a light fairy, Iridessa becomes a garden fairy, Rosetta becomes an animal fairy, and Vidia becomes a tinker fairy, much to her horror and disgust. They struggle with their swapped talents as they search for Zarina and the pirate ship, in the process meeting a baby crocodile which takes a liking to Rosetta. They find the ship and sneak in, where they hear James saying how the pirates met Zarina after drifting off course, and she became the captain with the promise of making the ship fly so that they could plunder anything without getting caught. The ship arrives at Skull Rock, where the fairies discover the pirates camp and a mysterious Pixie Dust Tree, which Zarina had grown using pink-colored pixie dust of the garden talent.

The fairies attempt to retrieve the blue pixie dust but are caught when Iridessa loses control over her nature/earth-based talent and reveals their location. Tink tries to convince Zarina to return home to Pixie Hollow, but she refuses as no one appreciates her unusual talents in dust-keeping, which eventually led her to leave. With the fairies now captured, the pirates begin making pixie dust which becomes successful. Hook, curious about what its like to fly, convinces Zarina to use some on him, with the result that he begins to fly. After joyfully flying around the cave, Hook double-crosses Zarina and traps her in a lantern, revealing he is the real captain of the pirate crew and that he has merely been using her all along to get the pixie dust for his own purposes of getting to the second star to plunder around the seven seas of Neverland.

Tinker Bell and the others attempt to escape their prison without any success, until the baby crocodile Rosetta befriended arrives and releases them. The fairies attempt once more to retrieve the blue pixie dust and almost succeed, before being confronted by Hook, who threatens to throw Zarina into the sea unless they hand the blue pixie dust over. Tink willingly gives the pixie dust up, and Hook sprinkles it over the ship before throwing Zarina into the water, leaving the fairies to rescue her. As the pirates sail towards the Second Star, the fairies return and use their switched talents to defeat the pirates and turn the ship away from the Second Star. Zarina attempts to retrieve the blue pixie dust from Hook, who chases after her. Zarina manages to gain a speck of blue pixie dust which she then throws at Hook, who starts flying crazily as the two kinds of pixie dust react to each other. As the fairies fly away, Hook angrily swears revenge on them until he is attacked by the baby crocodile, leaving his crew members to laugh at him, much to his dismay. Zarina returns the blue pixie dust to Tink and her friends, before preparing to leave. However, she is offered the chance to return to Pixie Hollow and this time accepts, and helps her friends to sail the ship back to Pixie Hollow where the others are waking up with no recollection of what had happened. Zarina promises not to tamper and experiment with pixie dust again, but is convinced by Tink to show off her astoundingly profound pixie dust abilities one last time. Zarina does so, and in the process, gives Tink and her friends back their original fairy talents, allowing them to put on a beautiful performance to the audience at the Four Seasons Festival. Everyone rushes over to congratulate them, particularly Zarina whose unusually strong and rare talent of pixie dust alchemy is finally accepted and can be who she is. 

Upon an extra scene during the end credits of the film, a drifting Captain Hook comes across an unknown ship at sea. One of the crew members, Smee, comes to greet him. Taking no notice of Smees words, Hook orders to be pulled up into the ship.

== Cast ==
 
{{Multiple image direction = horizontal footer = From left to right: Mae Whitman, Christina Hendricks, Tom Hiddleston image1 = Mae Whitman March 22, 2014 (cropped).jpg width1 =   image2 = Christina Hendricks @ the Serenity Premiere.jpg width2 =   image3 = Tom Hiddleston Cannes 2013.JPG width3 =  
}}
 
* Mae Whitman as Tinker Bell, a tinker fairy and Periwinkles twin sister.
* Christina Hendricks as Zarina, an unusually skilled and inquisitive dust-keeper fairy.
* Tom Hiddleston as James "Hook," the captain of the pirate ship disguised as the cabin boy.
* Lucy Liu as Silvermist, a water fairy.
* Raven-Symone as Iridessa, a light fairy.
* Megan Hilty as Rosetta, a garden fairy.
* Pamela Adlon as Vidia, a fast-flying fairy.
* Angela Bartys as Fawn, an animal fairy.
* Jim Cummings as Oppenheimer, one of James crew members.
* Carlos Ponce as Bonito, one of James crew members.
* Jim Cummings as Port, one of James crew members.
* Mick Wingert as Starboard, one of James crew members.
* Kevin Michael Richardson as Yang, one of James crew members.
* Jeff Bennett as Mr. Smee, a crew member from an unknown ship who meets James.
* Rob Paulsen as Bobble, a wispy tinker fairy with large glasses and Clanks best friend.
* Jeff Bennett as Clank, a large tinker fairy with a booming voice.
* Grey DeLisle as MC Fairy / Gliss.
* Kari Wahlgren as Sweetpea / Sydney.
* Jeff Bennett as Fairy Gary, a large dust-keeper fairy.
* Jane Horrocks as Fairy Mary, the overseer of all tinker fairies.
* Jesse McCartney as Terence, a dust-keeper fairy and Tinker Bells close friend.
* Anjelica Huston as Queen Clarion, the queen of all Pixie Hollow and Lord Miloris love interest. Peter Pan.

==Production==
The film was originally titled Quest for the Queen.     Peggy Holmes, co-director of Secret of the Wings signed on to direct the film.  It introduced new characters, Zarina, voiced by Christina Hendricks, and James aka young Captain Hook, voiced by Tom Hiddleston.  Carlos Ponce also lent his voice to one of the characters in the film.   

Disney announced in January 2014 that former Project Runway winner and fashion designer Christian Siriano would be in charge of creating the ensemble costumes for the fairies, specifically Zarina. Siriano stated that "I loved the challenge of this project. I havent designed for an animated character before, and Im excited to take my skills into Zarinas world. Shes a unique and new character and I wanted to help make her memorable and iconic. Disney characters are everlasting and Im so happy as a young designer to help create a bit of Disney history." 

==Release==
The film was released internationally in theaters with the title Tinker Bell and the Pirate Fairy on February 13, 2014 and later dates,    with 2D and 3D screenings available.   In the United States the screenings took place exclusively at the El Capitan Theater in Hollywood, from February 28 to March 19, 2014.  It was originally scheduled for Fall 2013, before another DisneyToon Studios film, Planes (film)|Planes, took its place, delaying the film to Spring 2014.   

===Marketing===
A trailer for the film was released on the Secret of the Wings Blu-ray and DVD on October 23, 2012.  Tinker Bell series not to allude to the Disney Fairies brand in promotional material and not to display the brand logo at the beginning of the film, showing instead the DisneyToon Studios logo.  

===Home media===
The film was released by Walt Disney Studios Home Entertainment on DVD and Blu-ray in the United States on April 1, 2014,    and in the United Kingdom on June 23, 2014. 
Bonus features of the DVD include a documentary and two animated shorts. The Blu-ray contains the bonus features of the DVD and additions such as deleted scenes, sing-along songs and a making-of clip of "The Frigate That Flies" song with actor Tom Hiddleston. 

During the pre-order period of the combo pack with the DVD, Blu-ray and Digital Copy versions of the film, a limited edition set of four lithographs featuring shots from the film would be included with the order.   Pixie Hollow Bake Off and 10 other mini shorts, the story book and read-along CD of The Pirate Fairy film, a set of six wall decal sheets, and a glitter brush.  

==Reception==
Reviews for The Pirate Fairy have been generally positive. On film aggregation website Rotten Tomatoes, it has a 75% rating, with an average score of 6.1/10, based on reviews from 16 critics. 

==Soundtrack==
The film was scored by Joel McNeely,  who has also scored the previous films in the Tinker Bell (film series)|Tinker Bell series.

===Songs===
The soundtrack features an original song titled "Who I Am," performed by Natasha Bedingfield, as well as Bedingfields previously released song, "Weightless," which was initially used on the films scratch recording but so well received, director Peggy Holmes decided to make it permanent.   

Another original song, "The Frigate That Flies," with music by Gaby Alter and lyrics by Gaby Alter and Itamar Moses,  is performed in the film by the pirate crew as a musical number and its also possibly Captain James Hooks first sea shanty in his life.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 