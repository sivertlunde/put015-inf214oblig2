August (1996 film)
{{Infobox film
| name = August
| image = August_film_dvd.jpg
| image size =
| caption = DVD Cover Art
| director = Anthony Hopkins Rhian Morgan  Danial Parri Jones
| music = Anthony Hopkins
| writers = Anton Chekhov (play) Julian Mitchell (writer)
| released = 9 August 1996
| runtime = 94 minutes
| language = English
| budget =
}}
August is a 1996 drama film directed by and starring   in a small role in one of his earliest films, as Griffiths. It is an adaptation of Chekhovs Uncle Vanya, with Ieuan taking over the title role.

The film was Hopkinss first feature film with a full cast (he had previously directed the one-man-performance of Dylan Thomas: Return Journey in 1990). It would be over a decade before his next directorial effort would, Slipstream (2007 film)|Slipstream in 2007, which he also wrote and for which he also composed the score.

==Adaptation and issues== vacation home (at one point Ieuan states that he feels that he has been cheated by the Prof. Blathwaite, just as "the English have always cheated the Welsh").

==Language==
It is primarily in English, with a few lines in Welsh here or there - such as diolch yn fawr iawn ("thank you very much"), cariad (a term of endearment, meaning "love"), and iechyd da ("cheers").

==See also==
* Meibion Glyndŵr, on Welsh-English relations surrounding the English taking vacation homes in Wales.
* List of Welsh films

== External links ==
*  

 

{{navbox name = Anthony Hopkins title = Films directed by Anthony Hopkins state = autocollapse listclass = hlist
 list1 = 
* August (1996)
* Slipstream (2007 film)|Slipstream (2007)
}} 

 
 
 
 
 
 
 


 