Must Come Down
 
  
 
}}

Must Come Down is a 2012 independent feature film written and directed by Kenny Riches, starring David Fetzer and Ashly Burch (who is known for her role in the video game parody series, Hey Ash, Whatcha Playin?).  It was produced by Patrick Fugit and Dominic Fratto.  Must Come Down is Riches first feature film, and after premiering at the Cinequest Film Festival in March 2012, the film went on to play the Phoenix Film Festival, Newport Beach Film Festival, and Minneapolis–Saint Paul International Film Festival. 

In 2013 it was shown at the International Film Awards Berlin "ifab"  as part of the official selection. At the ifab lead actress Ashly Burch was nominated for the Best Actress ifab Award and cinematographer Bill Otto was nominated for the Best Cinematography ifab Award.

== Summary ==
Holly (Ashly Burch) and Ashley (David Fetzer), both in the midst of individual quarter-life crises, meet by chance when their peculiar activities intersect at the same bus stop. Ashley, in his late twenties, quit his job to travel in search of internal clarity—stopping along the way to visit his childhood home with an ulterior motive to break in for the nostalgic rush. Holly, in her early twenties, is mourning the loss of an immature and short-lived relationship with her restaurant manager who ultimately fired her, leaving her jobless and directionless as well as heartbroken. Both struggling to move on from the past, Holly and Ashley share brief, strange, and juvenile misadventures as they compare and contrast, and commiserate as grown-up children stumbling through life’s final bout of growing pains.

== Production ==
Must Come Down was shot on location in Salt Lake City, Utah in 2010 on the RED camera using anamorphic lenses.

== Soundtrack and score ==
The  original score the film was created by Andrew Shaw, who is a Salt Lake City musician.  A soundtrack including this score, and the tracks by other Salt Lake City musicians/bands (Cathy Foy, David Fetzer, Palace of Buddies, and Tolchock Trio) will be released in 2013.  

Track List:
*“Noel”
Written by Nick Foster, Tim Myers
Performed by Palace of Buddies
*“The Boy In The Well”
Written/Performed by David Fetzer
*“Hyperstimulation”
Written/Performed by Cathy Foy
*"Icarus" 
Written by Casey Dienel 
Performed by White Hinterland 
*“Factory”
Written by Oliver Lewis
Performed by Tolchock Trio
*“The Park”
Written by MJ Parker, Charlie Gokey, Alex Abnos
Performed by Secret Cities
*“Wild Things”
Written/Performed by Cathy Foy
Courtesy of Cathy Foy
*“Dinner Bells”
Performed by Wolf Parade
Written by Spencer Krug, Arlen Thompson, Dan Boeckner, Hadji Bakara

== Cast ==
* David Fetzer
* Ashly Burch
* Liberty Cordova
* Colin Fugit 
* Paul Chamberlain

==External links==
*  
*   
*  

 
 