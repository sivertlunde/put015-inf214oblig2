9 (2009 animated film)
{{Infobox film
| name           = 9
| image          = 9posterfinal.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Shane Acker
| producer       = {{Plainlist|
*Tim Burton
*Timur Bekmambetov
*Jim Lemley
*Dana Ginsburg}}
| screenplay     = Pamela Pettler
| story          = Shane Acker
| based on       =  
| starring       = {{Plainlist|
*Elijah Wood
*John C. Reilly
*Jennifer Connelly
*Christopher Plummer
*Crispin Glover
*Martin Landau
*Fred Tatasciore
}}
| music          = Deborah Lurie  
| cinematography = Kevin R. Adams
| editing        = Nick Kenway
| studio         = {{Plainlist|
*Relativity Media Starz Animation Tim Burton Productions Bazelevs Animation
}}
| distributor    = Focus Features
| released       =  
| runtime        = 79 minutes 
| country        = United States Russia
| language       = English
| budget         = $30 million
| gross          = $48,428,063 
}} science fiction voice talents 2005 short film of the same name.  The film was released on September 9, 2009, and it was distributed by Focus Features. It received mixed to positive reviews, and has since developed a cult following.

The screenplay for the film was written by Pamela Pettler, with casting by Mindy Marin, production design by Robert St. Pierre and Fred Warter, and art direction by Christophe Vacher.

==Plot==
Prior to the events of the film, a scientist is ordered by his dictator to create a machine in the apparent name of progress. The Scientist uses his own intellect to create the B.R.A.I.N., a thinking robot. However, the dictator quickly seizes it and integrates it into the Fabrication Machine, an armature that can construct an army of war machines to destroy the dictators enemies. Lacking a soul, the Fabrication Machine becomes corrupted and decides to exterminate almost all life on Earth. The Fabrication Machine reprograms the other war machines to turn against humans by using toxic gas and chemical weapons. On the verge of destruction, the Scientist uses alchemy to create nine homunculus-like rag dolls known as "Stitchpunks" using portions of his own soul via a talisman and dies upon finishing the last one.

Sometime later, the last doll, 9, awakens in the Scientists workshop. Taking the talisman with him, 9 ventures out into the devastated city and meets 2, a frail inventor who gives him a voice box and is surprised when 9 reveals the talisman. The last surviving machine, the Cat-Beast, attacks the pair and takes 2 and the talisman. 9 collapses, but awakens in Sanctuary, a cathedral that is home to the other dolls, including the dogmatic leader 1, his large bodyguard 8, the cycloptic engineer 5, and the mentally unstable oracle 6. 1 immediately labels 2 as dead, but 9, having seen the factory where the Cat-Beast has taken 2, decides to rescue him. 9 and 5 venture to the factory where they find 2. 7, the only female of the dolls, arrives and slays the Cat-Beast, but 9, drawn by curiosity, connects the talisman to the previously derelict Fabrication Machine, causing the interface to absorb 2s soul into the machine, reviving it and killing him in the process whilst 9, 5, and 7 manage to escape its factory.

7 takes 9 and 5 to the library, where the silent scholar twins, 3 and 4, show 9 the Fabrication Machines origins. 5 realizes the talismans symbols match the clairvoyant drawings of 6. 9 and 5 return to Sanctuary to investigate, but 1 confronts and chastises them. The Fabrication Machine starts constructing new machines; one of them, the bird-like Winged Beast, attacks Sanctuary, leading to a battle between the dolls and the Winged Beast. 7 joins the fight, but is injured. 5 and 6 manage to kill the Winged Beast by reactivating the propeller of a derelict plane and knocking it into the blades.

As the group retreat to the library, 6, 3, and 4 cryptically explain the talismans origins, but 1 once again chastises the group, then reveals he had sent 2 out of the church on a scouting trip to die, calling him old and weak. Meanwhile, angered at the loss of the Winged Beast, the Fabrication Machine finds 2s corpse and uses it as a hypnotic lure for another robot, the Seamstress. The Seamstress attacks the library and captures 7 and 8, but 2s body is safely recovered and given a funeral by the others. The others then run to the factory to destroy the machines. 9 goes inside alone, kills the Seamstress, and rescues 7, but is too late to save 8, who is killed by the Fabrication Machine. Afterwards, 9 and 7 escape while the others destroy the factory.

All of the dolls celebrate the destruction of the factory, but the Fabrication Machine, which survived, attacks the group and kills 5 and 6, but not before the latter convinces 9 to go to the Scientists workshop to find answers. 9 follows 6s instructions, finding a holographic recorded message from his creator, who explains that the "Stitchpunks" he created are all a part of himself, including 9, making them the only hope for humanity. He then explains that the talisman can be used against the Fabrication Machine to free the dolls souls trapped in it. 9 reunites with the other dolls and devises a plan to sacrifice himself to give the others enough time to retrieve the talisman from the Fabrication Machine. However, 1, having had a change of heart, allows himself to be killed instead, giving 9 the opportunity to remove the talisman. The Fabrication Machine tries to grab it, but 9 activates it and uses it to reabsorb the souls taken by the Fabrication Machine, killing it in the process. 9, 7, 3, and 4 free the souls of 5, 1, 6, 2, and 8 from the talisman and they fly up into the sky, causing it to rain. The final image shows that the raindrops contain small flecks of glowing bacteria, bringing life back to the world.

==Cast==

===Creations===
 
*9 (Elijah Wood) is the youngest of the group that represents the Scientists humanity, bigheartedness, thoughtfulness, and sincerity. He is very intelligent, but he can make mistakes due to his curiosity. He seeks the truth in the history of his creation, and wishes to know the meaning of life. He has a strong brotherly bond with 5.
*8 (Fred Tatasciore) is the brutish ruffian from the Scientists personality. He wields the one half of a scissor and a knife as his weapons. He is a master of his weapons. He is the largest of the group, but is the least intelligent.
*7 (Jennifer Connelly) is the fighter part of the Scientists personality and (possibly) the only female of the group. A rebel and a loner, she is willing to take many risks for the good of her people.
*6 (Crispin Glover) is the artistic portion of the Scientists personality. He sees things that the others in the group dont see. 6s fingers are made of ink pen nibs, which he uses to draw. 
*5 (John C. Reilly) is the Healer part of the Scientists personality. 5 is caring, nurturing, and the loyal, bighearted "common man" who always tries to play the peacemaker. 
*3 and 4 are twins, and the Historians of the group. They are unable to speak, instead using flickering lights in their eyes to communicate with each other and 4 can project images from his/her eyes for the other members of the group. They are very intelligent and energetic.
*2 (Martin Landau) is the creative and genius portion of the Scientists personality. He is a kind, delicate old inventor.  He is fascinated by garbage and scrap, and loves to explore the wastelands and look for parts for his inventions.
*1 (Christopher Plummer) is the cowardly, arrogant portion of the Scientists personality.  He is the oldest of the group and the leader, demanding absolute loyalty from the others and frequently clashing with 9, who refuses to follow him.

===Humans===
*The Scientist (Alan Oppenheimer) invented the nine creations in an attempt to thwart the corrupt and warlike B.R.A.I.N., hoping that they would continue the spark of life. Each of his creations contains a portion of his human soul, embodying both his qualities and flaws. 
*The Chancellor (Tom Kane) was responsible for causing the Fabrication Machine to turn against humanity.
*The Radio Announcer (Fred Tatasciore)
*The Newscaster (Helen Wilson)

===Machines===
*The Cat Beast is the first machine that 9 encounters, and the main antagonist of the original short film 9 (2005 film)|9. It was the last active machine in the world until the reactivation of the Fabrication Machine. With a gait somewhere between a lion and a monkey, it has spines on its back, a cat skull for a head, a red mechanical eye in its left socket and a light bulb in its right, which it uses to see in the dark. 

*The Fabrication Machine/B.R.A.I.N. (Binary Reactive Artificially Intelligent Neurocircuit) is the machine that built all the machines. It was designed by the Scientist as an A.I. for use by the Chancellor.

*The Winged Beast is a pterodactyl-like machine constructed by the Fabrication Machine to hunt down the creations. It has knives and scissors for a mouth, four small red eyes around its "head", a tarp or flag for its bat-like wings, and a harpoon on the end of its tail. Several human bones appear to be integrated into its structure. It can fly through a combination of its wings and an electric fan in its body. In battle, it uses the blades on its head, the claws on its wings, or its harpoon tail, which can be fired and retracted at will.

*The Seamstress is a cobra-like robot designed by the Fabrication Machine to capture the Scientists other creations; it is also its most formidable warrior. Its serpentine body bears numerous spindly metal limbs that end in a variety of claws, scissors, needles and blades. Spools of red thread are attached to its back, and 2s lifeless body is attached to its tail. Its head is a mixture of a skull and a broken doll mask with a requisite red mechanical eye hidden by the black fabric of its body, surrounded by smaller limbs that can spread the fabric to reveal its face. It flashes light through 2s eyes to hypnotize its victims, immobilizes them with its thread, and binds them in its own body to take back to the Fabrication Machine.

* Seekers are large hot air balloon-like machines with searchlights and alarms similar to air-raid sirens that scout around the factory.

* Spiderbots are small tarantula-like robots that are made by the Fabrication Machine to repopulate the humanless world. 
 tripods from The War of the Worlds. They are fast for their size, and use powerful machine guns that can penetrate concrete. They can also launch capsules that exude toxic gas. As said by 1, the gas kills all life, including bacteria. They dont seem to be able to kill bacteria higher up in the atmosphere.

==Production== short film by the same title. The screenplay was written by Pamela Pettler. Acker, a young director, was influenced by Tim Burton who worked with him on this animated feature. It was produced in part by Tim Burton, Timur Bekmambetov and Jim Lemley, and was released on September 9, 2009. Animation began in Luxembourg at Attitude Studio, but subsequently moved to Starz Animation in Toronto, Canada.  The film was released by Focus Features. Originally, TriStar Pictures was supposed to release the film with Focus Features, but was later dropped out to distribute Planet 51. 

==Music==
{{Infobox album
| Name        = 9: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Danny Elfman & Deborah Lurie
| Cover       = 9 The Original Motion Soundtrack.jpg
| Released    =    Reprise
| Last album  = Terminator Salvation (2009)
| This album  = 9 (2009)
| Next album  = Genre = Length = Recorded = 2008-2009}} The Wizard 78rpm gramophone Welcome Home" Amazon before Welcome Home".

=== Track listing ===
{{Track listing
| extra_column    = 
| total_length    =45:68 
| all_music       = Danny Elfman and Deborah Lurie with soundtrack, except "Welcome Home" (lyrics by Claudio Sanchez, music by Coheed and Cambria)
| title1          = Introduction
| length1         = 1:42
| title2          = Finding Answers
| length2         = 1:48
| title3          = Sanctuary
| length3         = 2:12
| title4          = Winged Beast
| length4         = 4:28
| title5          = Reunion/Searching for Two
| length5         = 2:12
| title6          = The Machines
| length6         = 0:58
| title7          = Out There
| length7         = 2:42
| title8          = Twins
| length8         = 1:36
| title9          = Slaying the Beast
| length9         = 1:21
| title10          = Return of the Machines
| length10         = 2:47
| title11          = Burial
| length11         = 1:24
| title12          = Reawakening
| length12         = 3:10
| title13          = The Aftermath
| length13         = 1:41
| title14          = Confrontation
| length14         = 1:53
| title15          = The Seamstress
| length15         = 2:05
| title16          = Return to the Workshop
| length16         = 1:54
| title17          = The Purpose
| length17         = 5:20
| title18          = Release
| length18         = 4:00 Welcome Home
| note19           = performed by Coheed and Cambria
| length19         = 6:15
}}

==Marketing== trailer was Welcome Home".   
 the original short film; The Winged Beast, a pterodactyl-like machine with movable blades in its mouth; the Seamstress, a hypnotic serpent; Steel Behemoths, large two-legged machines armed with a machine gun and poison gas missiles which can kill in a matter of seconds; the Fabrication Machine, a Cyclops|cyclopic, spider-like machine with many multi-jointed arms; and Seekers, aerial machines with searchlights.   Later trailers also reveal the existence of several small spider-like machines.

Part of the films marketing strategy was its release date of September 9, 2009 ("9/9/09").
 Land of the Lost. It is an extensive trailer which includes a bit of the background story behind the existence of the creations.

In April 2009, the films "Scientist" began making journal entries on a Facebook page called "9 Scientist", including essays about each of his nine creations. The "9 Scientist" Facebook page seemingly references events leading up to the release of the film.    A viral campaign promotional website for 9 was launched. It shed some light upon the background of the 9 world.  Since the work of The Scientist is ultimately responsible for the destruction of mankind, it is of some note that the actor who plays this role, Alan Oppenheimer is the cousin of scientist Robert Oppenheimer, the "Father of the Atomic Bomb". 
 rated PG-13 for "violence and scary images". NECA Toys released collectible action figures of 9 and 1.

==Video game==
Shortly before the films release, SkyZone released a mobile game adaptation entitled 9: The Mobile Game for the iPhone and iPod Touch. The plot is similar to the movies plot, with minor differences. It received mixed reviews.

==Reception==

===Critical reaction=== title = 9 (2009) url = publisher =  , it currently holds a score of 60 out of 100 indicating mixed or average reviews.  Roger Ebert gave the film 3 out of 4 stars, saying it is "beautifully animated and intriguingly unwholesome...nevertheless worth seeing".  The general sentiment by critics is that the film is "long on imaginative design but less substantial in narrative."    Variety (magazine)|Variety s Todd McCarthy says, "In the end, the pictures impact derives mostly from its design and assured execution." 

===Box office=== I Can Do Bad All By Myself with approximately $10,740,446 and $15,160,926 for its 5-day opening.  As of November 29, 2009, the film has grossed United States dollar|US$48,428,063 worldwide.   

===Awards and nominations===
* Honored with the Winsor McCay Award   (producer Tim Burton)
{| class="wikitable"
|-
|+ Awards
|-
! Award
! Category
! Recipient(s)
! Outcome
|- Annie Awards
|-
| Best Animated Effects in a Feature Production
| Alexander Feigin
| 
|-
| Best Production Design in a Feature Production
| Christophe Vacher
| 
|-
| Producers Guild of America Awards
| Producer of the Year in Animated Motion Picture
|
|  
|-
| Visual Effects Society Awards
| Outstanding Animation in an Animated Feature Motion Picture
| Ken Duncan, Jinko Gotoh, Daryl Graham, Joe Ksander
|  
|-
| Washington D.C. Area Film Critics Association
| Best Animated Film
|
|  
|- Motion Picture Sound Editors
|-
| Best Sound Editing - Sound Effects, Foley, Dialogue and ADR in an Animation Feature Film
| Shie Rozow, Pascal Garneau, Denise Thorpe, Jana Vance, Will Files, Jeremy Bowker, Luke Dunn Gielmuda, Jill Purdy (Skywalker Sound)
|  
|-
| Best Sound Editing - Sound Effects, Foley, Music, Dialogue and ADR Animation in a Feature Film
|
|  
|-
|}

==Home release== 2005 short film of the same name, cast interviews, and commentary by the filmmakers.

==Sequel==
{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 95%; background:#F0F8FF; color:black; width:28em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"I think there is definitely room. I mean, the way we end the film, there is a slight suggestion that it may be a new beginning. And I think we could continue the journey from where we left off and see how these creatures are existing in a world in which the natural environment is coming back and perhaps even threatening them in some way. Do they make the decision to not affect it, or do they try to affect it in some way? And do they still try to hold on to that humanity within them or do they recognize themselves at being machines too and go off on a different trajectory? So theres lots of idea that I think that we could play with and make another story out of." 
|-
|style="text-align: left;" |— Director Shane Acker in an interview with Joblo.com.
|}

Currently no plans for a sequel have been made, but possibilities have been mentioned via the films DVD commentary. Shane Acker has also mentioned the possibility of a sequel being made because of the lack of darker animated films, claiming that everything is G and PG rated with little to no dark elements. He has said that he will continue to make darker animated films, either doing so with a sequel to 9 or original ideas for future films.  Before the theatrical release of the film, director Shane Acker and producer Tim Burton stated they were open for a sequel, depending on how well the film was received.  Since the films release onto DVD, there have been no further mentions of a sequel, with Acker focusing on his latest project Deep. 

==See also==
 
* List of American films of 2009
* 9 (2005 film)|9 (2005 film) – The original short film on which 9 is based.
* Homunculus
* Rag doll

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 