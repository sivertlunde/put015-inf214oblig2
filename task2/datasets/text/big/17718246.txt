Blind Date (2007 film)
{{Infobox film
| name           = Blind Date
| image          = Blind Date (2008 film) poster.jpg
| caption        = Theatrical release poster
| director       = Stanley Tucci
| producer       = Bruce Weiss Gijs van de Westelaken
| writer         = Stanley Tucci David Schechter
| starring       = Stanley Tucci Patricia Clarkson
| music          =
| cinematography = Thomas Kist
| editing        = Camilla Toniolo
| distributor    = Variance Films
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
}} 1996 film Dutch director Theo van Gogh.

==Plot==
While struggling to reconnect after the death of their daughter, a married couple construct an elaborate game of pretend on a series of blind dates, hoping that they will be able to openly talk about the state of their relationship in the wake of tragedy. 

==Cast==
*Stanley Tucci as Don, the husband 
*Patricia Clarkson as Janna, the wife 
*Thijs Römer  as the waiter
*Gerdy De Decker as the tango dancer
*Georgina Verbaan as the cute woman
*Robin Holzauer as the little girl
*Sarah Hyland as the child
*Peer Mascini as the sole drinker

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 