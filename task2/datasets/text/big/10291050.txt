The Runaround (1931 film)
{{Infobox Film name = The Runaround image = Runaround1931P.jpg producer  =  director = Tommy Atkins (assistant) screenplay = Barney A. Sarecky writer = Zandah Owen starring = Mary Brian Joseph Cawthorn Marie Prevost Johnny Hines Geoffrey Kerr music = Max Steiner cinematography = Ray Rennahan editing = Anne McKnight George Marsh studio = RKO Radio Pictures distributor = RKO Radio Pictures released =   }} runtime = 82 minutes language = English
|country = United States
|}}

The Runaround (1931) is a comedy-drama film that was photographed entirely in Technicolor. It was directed by William James Craft, from a screenplay by Alfred Jackson and Barney Sarecky, based on a story by Zandah Owen.  The film stars Mary Brian, Joseph Cawthorn, Marie Prevost, Johnny Hines, and Geoffrey Kerr.  Produced and directed by RKO Radio Pictures, it premiered in New York City on August 7, 1931, and was released national on August 22.  It was the first film to be shot in a new Technicolor process which removed grain, resulting in a much improved color. The film was released in Great Britain as Waiting for the Bride.

==Plot==
Millionaire playboy Fred White is attempting to make chorus girl Evelyn his latest conquest. Evelyn, on to Freds scheming, has some scheming of her own, attempting to maneuver Fred into marriage. In a last ditch effort to get Evelyn into bed, Fred purchases a diamond bracelet, to which he has attached a key to the apartment he has leased as their potential love nest. When he shows the bracelet to his friend, Howard, the friend warns Fred that Evelyn is simply a gold-digger, only interested in getting him to marry her so that she can gain access to his money. The two make a bet.  If Fred wins by getting her to be his kept woman, Howard has to pay for the bracelet and the cost of the apartment, and if Howard wins, by rejecting Freds non-marital advances, Fred will owe Howard the same amount of money.

When his plan to establish the love nest does not work out, Fred is dismayed, but Evelyn opens the door by inviting him to dinner the following night.  She uses the dinner as a pretense to set to entrap Fred into marriage.  Part of her plan involves her friend, Lou, to pose as her father.  Not understanding that he is being entrapped, Fred realizes that he is really in love with Evelyn, and actually makes a real proposal of marriage.  The night of his bachelor party, Howard is still distrustful of Evelyns motives and gets Lou drunk, after which he reveals Evelyns plot to entrap Fred.  Fred is devastated, and agrees to a plan to embarrass Evelyn at the altar on the day of their wedding.

Meanwhile, Evelyn realizes that she is no longer after Fred simply for his money, that she has actually fallen in love with him.  She cannot bring herself to confess her underhanded scheming to Fred, and simply does not show up on the day of their wedding. Fred rushes from the church to her house, where he finds a letter she had written to him in which she confesses everything.  He convinces her to come back to the church and go through with the wedding.  They return to the church, where everything is explained to the guests, and the two of them are married. 

==Cast==
* Mary Brian as Evelyn
* Geoffrey Kerr as Fred White
* Marie Prevost as Margy
* Joseph Cawthorn as Lou
* Johnny Hines as Howard
 AFI database. )

==Production==
The musical sequences originally in the film were recycled from an aborted Technicolor revue, which was to have been titled Radio Revels of 1930. 

As a result of the quality of the color work in this film, Radio Pictures decided to produce three more pictures in the new process.  The first of these, Fanny Foley Herself (1931), was the only one to be completed and released in Technicolor. The titles of the two other features were Marcheta and Bird of Paradise. While Marcheta seems to have been abandoned, Bird of Paradise became a black-and-white production starring Dolores del Río and released in 1932. 

Mary Brian was borrowed from Paramount Pictures, to whom she was under contract, to make this picture. Noel Francis and Jack Mulhall were originally cast to be in this film, but were replaced.  Mary Astor was originally cast in the part of Mary Brian. 

==Preservation status==
Only an incomplete black-and-white copy of the cut version seems to have survived.  It was released intact in countries outside the United States (under the title "Waiting for the Bride"), where a backlash against musicals never occurred, but it is unknown whether a copy of this version still exists.

==Reception==
The film recorded a loss of $160,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p38 

==See also==
*List of early color feature films

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 