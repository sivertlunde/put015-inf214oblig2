One Bright Shining Moment: The Forgotten Summer of George McGovern
 
{{Infobox film director = Stephen Vittoria writer = Stephen Vittoria  producer = Stephen Vittoria  starring = Warren Beatty Dick Gregory Gary Hart George McGovern Ron Kovic George McGovern Gloria Steinem Gore Vidal Howard Zinn  narrator = Amy Goodman  music = Elvis Costello Donovan Bob Dylan Robbie Robertson Leon Russell  distributor = First Run Features  released = 2005  runtime = 125 min.  country = United States  language = English
}}

One Bright Shining Moment: The Forgotten Summer of George McGovern (2005 in film|2005) is a documentary film directed by Stephen Vittoria.   

==Overview== 1972 presidential Democratic U.S. Senator George McGovern.
 feminist activist and journalist Gloria Steinem, historian Howard Zinn, author Gore Vidal, former presidential candidate and McGovern 1972 campaign manager Gary Hart, satirist-activist Dick Gregory, and George McGovern himself.

The film won the Jury Prize Award for Best Documentary Feature at the 2005 Sarasota Film Festival.

==References==
 

==External links==
* 

 

 
 
 
 


 