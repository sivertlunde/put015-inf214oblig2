Thumb Wars
 
{{Infobox film
| name           = Thumb Wars
| image          =
| image_size     =
| caption        =
| director       = Steve Oedekerk
| producer       = Steve Oedekerk Tom Koranda Paul Marshal
| writer         = Steve Oedekerk
| starring       = Steve Oedekerk Ross Shafer Mark Decarlo Rob Paulsen Paul Greenberg Andrea Fears
| music          = Robert Folk
| cinematography = Mike DePrez
| editing        = Mike DePrez
| 3D/VFX Production Pipeline Engineer = Brian M. Thomas
| distributor    = Icestorm Entertainment Image Entertainment (USA, DVD) Revolver Entertainment(United Kingdom|UK)
| released       = May 18, 1999 October 2, 2008 (DVD Re-release)
| runtime        = 29 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 
Thumb Wars: The Phantom Cuticle is a   that premiered the next day. The TV version provided a trimmed down version of the film. It was also re-released in 2002 alongside Thumbtanic as the "Thumb Wars/Thumbtanic Thumb Double Feature." It was for VHS and DVD.  In 2005, the filmettes were again released together as a UMD Video on the PlayStation Portable system.

==Storyline==
The 29-minute film opens with a parody of Star Wars Once upon a time#Modern variants|"A long time ago..." phrase: "If there were thumbs in space and they got mad at each other there would be... THUMB WARS."
 the Thumb, Evil Thumbpire Sacul region Thumbellion Resistance Fighters have retreated to a hidden base. The Thumbpire is constructing a big dangerous weapon thing with enough fire power to blow stuff up. If the Thumbellion can destroy the big dangerous weapon thing, they will live and the good side of the thumb will reign. If they dont there can be no sequels. No sequels means no merchandising, no fan clubs, no freaky guys at conventions that have way too much free time and no clear desire to date girls. Victory is imperative!"

Opening scenes depict   Scooby-Doo|Scoobi-Doobi Benubi. At Oobedoobs place, they view a hologram of Princess Bunhead. She informs them that Black Helmet Man is building a "Death Star|big, dangerous weapon thing" and that she needs their help. Loke and Oobedoob look up her holographic dress. After they viewed the message, they came back to Lokes house to find that his uncle Soondead and aunt Gonnabiteit have been clipped by Black Helmet Mans troops. Loke determines to go and fight this evil menace.
 Crunchy and Hand Duet. working girl. fugitive and clear and presumed innocent patriot games Air Force One..."
 Gabba the Thumb Star which can spin planets. Once on the space station, Oobedoob and Black Helmet Man engage in a final duel, which Oobedoob is tricked into losing. Princess Bunhead appears and says, "I escaped somehow. Lets go."  The team, minus Oobedoob, flee. However, Oobedoobs voice can be heard in Lokes head saying that he is still there "in spirit."  Loke mentions that he considers this to be "kinda creepy".
 Master Puppet. Thumb Master.

The group travels to the Thumbellion base to regroup for the final attack on the Death Thumb. They learn that there is a button on the station designed to blow the entire thing up, much to the confusion of Princess Bunhead. The Thumbellion fleet launches an attack on the station. This sets the stage for the final confrontation between Loke Groundrunner and Black Helmet Man atop Lokes X-Wing|Finger-wing Fighter. It is then that Black Helmet Man reveals: "Loke, I am your... mother!" and throws his cape back to reveal a pink dress.  Hands Hand appears and knocks Black Helmet Man off into space. As he floats into space, Black Helmet Man calls out to Loke by yelling, "Just wait till your father gets home!"

When Loke proudly proclaims that he will use the Power of the Thumb to hit the destruct button, Oobedoobs voice is heard saying to use the ships targeting system, as "thats what its there for." Loke fires a missile which hits the button and the giant thumb is destroyed.

The last scene shows the cast at a Chili Cookoff, along with Master Puppet (visibly controlled by a puppeteer) and the ghost of Oobedoob. The film ends with a voiceover from Oobedoob: "The Thumb will be with you...always."

== Production ==
The production for Thumb Wars started in 1997 with Steve Oedekerk, Paul Marshal, Tom Koranda, Mark DeCarlo and Jim Jackman. In 1998, the production for Thumb Wars developed more seriously into filming. The production started filming in Los Angeles, California in the California suburbs.

==Running gags==
Throughout the movie different characters will ask Loke to touch tongues. First Oobedoob, requiring this to seal the trainings beginning. Also, Master Puppet asks so to train Loke in the ways of the Thumb. 
As a running gag between all of the Thumbs! featurettes, a one-eyed thumb appears in each one. In this case as the bartender at the cantina.

==Releases==
Thumb Wars was first released on VHS, and then again with additional features, including character bios, commentary, and a Gabba the Butt interview, on DVD.
 UMD for the PlayStation Portable, released by O Entertainment and Image Entertainment.  This disc features the two Thumbation filmettes Thumbtanic and Thumb Wars in that order.  It also includes wraparound footage of The Thumbersons.  Despite including the Cast Interviews from Thumbtanic and the Gabba the Butt interview and Character Bios from Thumb Wars, the film lacks the commentary available on the single-film releases.

==See also==
*Thumbs!
*Thumbtanic
*Franken thumb
*Blair Thumb
*Bat Thumb
* Godthumb

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 