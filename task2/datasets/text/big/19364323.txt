Population: 1
{{Infobox film
| name           = Population: 1
| image          = Population One (1986 film).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Rene Daalder
| producer       = 
| writer         = Rene Daalder
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1986
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
 1986 punk rock musical film written and directed by Rene Daalder.

The film stars Tomata du Plenty of the Screamers as a defense contractor who somehow becomes the sole survivor of a nuclear holocaust. In his solitude, he traces the history of U.S. civilization in the 20th century through musical numbers featuring himself and 1980s punk diva Sheela Edwards.  
 Vampira (Maila Nurmi), Penelope Houston, Carel Struycken, K.K. Barrett, Mike Doud and El Duce. 

Cast (in credits order) Tomata Du Plenty		(as Tomata DuPlenty),
	Dino Lee		(as Dino Lee Bird),
	Helen Heaven,		
	Sheela Edwards,	
	Nancye Ferguson,		
	Holly Small,		
	Jane Gaskill,		
	El Duce,	
	Tony Baldwin,		
	Gorilla Rose,		
	Tequila Mockingbird,		
	Anthony-James Ryan (as A. James Ryan),
	Mike Doud,		
	Steve Berlin,		 David R. Campbell		(credited as both David Campbell and Richard Campbell),		
	K.K. Barrett (as Keith KK Barrett),
	Steve Hufsteter		
	Robert Alexander (as Rob Wray),
	Beck		(as Beck Campbell),
	Gino Havens		(as Geno Havens),
	Mari-Sol García		(as Mari-Sol Garcia),
	Tommy Gear,		
	Al Hansen,		
	Maicol Sinatra,		
	Susan Ensley,		
	Jean Leider,		
	Paul Ambrose,		
	Chase Holiday		
	Penelope Houston,		
	Jeff Laing.

Population: 1 was released on DVD in the U.S. in October 2008. 

== References ==
 

== External links ==
* 
* 
* 

 
 
 
 


 