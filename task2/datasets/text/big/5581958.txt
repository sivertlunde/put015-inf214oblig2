Hideous Kinky (film)
 
 
{{Infobox film
 | name           = Hideous Kinky
 | image          = Hideous kinky poster.jpg
 | caption        = Hideous Kinky film poster
 | director       = Gillies MacKinnon
 | producer       = Ann Scott Billy MacKinnon
 | based on       =  
 | starring       = Kate Winslet Saïd Taghmaoui
 | music          = John E. Keane
 | cinematography = John de Borman
 | editing        = Pia Di Ciaula BBC The Film Consortium Greenpoint Films L Films
 | distributor    = AMLF
 | released       = 2 October 1998
 | runtime        = 98 min.
 | language       = English French Arabic
 | budget         = ₤3.2 million   
 | gross          = $1,263,279 (Domestic) 
}} novel of English mother (Kate Winslet) who moves from London to Morocco with her two young daughters. The soundtrack included music by  Canned Heat,  Richie Havens and the Incredible String Band.

==Plot summary== English life, 25-year-old Julia heads for Morocco with her daughters, six-year-old Lucy and eight-year-old Bea. Living in a low-rent Marrakech hotel, the trio survive on the sale of hand-sewn dolls and money from the girls father, a London poet who also has a child from another woman.
 conman Bilal, sexual gears are set in motion. He eventually moves in with them and serves as a surrogate father. Julias friend Eva urges Julia to study in Algiers with a revered Sufi master at a school of "the annihilation of the ego". In another sequence, European dandy Santoni invites Julia and the girls to his villa. As finances dwindle, Bilals philosophy is "God will provide", although usually it is Bilal himself who provides. Sometimes he also disappears. At one point Bea contracts a streptococcus infection while he is gone and nearly dies. Bilal returns only to disappear again, but he has a plan. They discover that three return tickets that suddenly appear have been bought by him with money he got from the sale of his uniform. In the end, Julia and the girls board a train back to London.

==Main cast==
* Kate Winslet ... Julia, the mother of Lucy and Bea
* Saïd Taghmaoui ... Bilal, con man and acrobat
* Bella Riza ... Bea, eight year old sister of Lucy
* Carrie Mullan ... Lucy, six year old sister of Bea
* Pierre Clémenti ... Santoni, the European
* Sira Stampe ... Eva, Julias friend
* Abigail Cruttenden ... Charlotte

== References ==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 