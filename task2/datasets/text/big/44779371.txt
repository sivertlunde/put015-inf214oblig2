Where the Trail Divides
{{Infobox film
| name           = Where the Trail Divides
| image          = Where the Trail Divides poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = James Neill
| producer       = 
| screenplay     = William Otis Lillibridge 
| starring       = Robert Edeson Theodore Roberts J. W. Johnston Winifred Kingston James Neill Constance Adams
| music          = 
| cinematography = Robert L. Carson 
| editing        = 
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Western silent film directed by James Neill and written by William Otis Lillibridge. The film stars Robert Edeson, Theodore Roberts, J. W. Johnston, Winifred Kingston, James Neill and Constance Adams. The film was released on October 12, 1914, by Paramount Pictures.  

==Plot==
 

== Cast == 
*Robert Edeson as How Landor
*Theodore Roberts as Colonel William Landor
*J. W. Johnston as Clayton Craig 
*Winifred Kingston as Bess Landor
*James Neill as Sam Rowland
*Constance Adams as Mrs. Rowland
*Fred Montague as Rev. John Eaton
*Antrim Short as Little How
*Mary Higby as Little Bess 

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 