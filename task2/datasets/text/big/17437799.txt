Iron Cross (film)
{{Infobox film
| name           = Iron Cross
| image          =Iron Cross (film).jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Joshua Newton
| producer       =  
| writer         = Joshua Newton
| starring       =  
| music          =  
| cinematography = Adrian Cranage, James Simon
| editing        = Joshua Newton
| distributor    = 
| released       =  
| runtime        = 130 minutes
| country        = United Kingdom
| language       = English
| budget         = $30 Million  (estimated) 
}}
Justice/Vengeance (also known as Iron Cross in the United States ) is a 2009 British thriller film. The film was written and directed by British film director Joshua Newton and was released in the USA in December 2009, although it was not released in the UK until March 2011. It stars Roy Scheider in his final film role.

When developing the script, Joshua Newton, Iron Cross s writer and director, asked himself what his father would do in the event that he discovered the man who murdered his family during the Holocaust. The character of Joseph played by Roy Scheider is loosely based on Joshua Newtons father Bruno Newton, who died during the filming from the same disease that took Roy Scheiders life nine months later, Multiple myeloma. As Scheider died before production was finished, his scenes were completed utilizing CGI techniques to stand in for the actor.

On 20 September 2010 director Joshua Newton won the Visionary Filmmaker Award at the 26th Boston Film Festival. 

==Plot== New York police officer and Holocaust survivor, travels to Nuremberg to visit his son Ronnie (Scott Cohen) years after turning his back on him for rejecting a promising career in the NYPD and marrying a local artist, Anna (Calita Rainford). No sooner does Joseph attempt to heal the rift with Ronnie then he swears that living in the apartment above, under the false name of Shrager, is the now aging SS Commander (Helmut Berger) who slaughtered his entire family during World War II. With little hope of seeing him stand trial, Joseph talks Ronnie into exacting justice - and vengeance - and together they set out to kill him.

Flashbacks reveal the teenage love of Young Joseph (Alexander Newton) for a heroic Polish girl, Kashka (Sarah Bolger) and his narrow escape from the massacre, leading to the films climax.

==Cast==
* Roy Scheider as Joseph Scott Cohen as Ronnie
* Alexander Newton as Young Joseph
* Helmut Berger as Shrager / Vogler
* Calita Rainford as Anna
* Mónica Cruz as Gaby
* Sarah Bolger as Kashka
* Enrique Arce as Guillermo
* Anna Polony as Frau Ganz

==Oscar campaign==
In the summer of 2009, after Variety (magazine)|Variety editor Tim Grey listed Iron Cross, among about 50 other films, as a potential Oscar nominee the magazines sales staff sold Joshua Newton on a $400k Oscar ad campaign. Such campaigns constitute about 80% of Varietys ad revenue. The campaign was aborted after Variety ran a pan of the film, by freelancer Robert Koehler, after the film had an unpublicized qualifying run, for a week, in Los Angeles. The review was subsequently removed from the magazines website. 
 SLAPP motion and dismissed Calibras case. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 