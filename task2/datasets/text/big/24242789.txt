Rude (film)
{{Infobox film
| name           = Rude
| image          = 
| caption        = 
| director       = Clement Virgo
| producers       = Clement Virgo Damon DOliveira Karen King
| writer         = Clement Virgo
| starring       = Maurice Dean Wint
| music          = 
| cinematography = Barry Stone
| editing        = Susan Maggi
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Canada
| language       = English
| budget         = 
}}

Rude is a 1995 Canadian crime film directed by Clement Virgo. It was screened in the Un Certain Regard section at the 1995 Cannes Film Festival.   

==Cast==
* Maurice Dean Wint - General
* Rachael Crawford - Maxine
* Clark Johnson - Reece
* Richard Chevolleau - Jordan
* Sharon Lewis - Rude (as Sharon M. Lewis)
* Melanie Nicholls-King - Jessica
* Stephen Shellen - Yankee
* Gordon Michael Woolvett - Ricky
* Dayo Ade - Mike
* Dean Marshall - Joe
* Ashley Brown - Johnny
* Andy Marshall - Addict
* Falconer Abraham - Austin
* Junior Williams - Curtis
* Andrew Moodie - Andre

==References==
 

==External links==
* 

 
 
 
 
 
 
 