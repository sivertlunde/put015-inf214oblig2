Julie (2006 film)
 
 

{{Infobox film
| name           = Julie
| image          = Julie 2006 film poster.jpg
| image_size     =
| caption        = Film poster
| director       = Poornima Mohan
| producer       = KCN Mohan
| writer         = Sethumadhavan
| narrator       =
| starring       = Ramya Dino Morea Sundeep Malani
| music          = Rajesh Ramnath
| cinematography = T. Janardhan
| editing        = Manohar
| distributor    =
| released       =  
| runtime        = 145 minutes
| country        = India Kannada
| budget         =  
}}
 Kannada film by Poornima Mohan.    The movie is a remake of the 1975 Hindi film Julie (1975 film)|Julie, which is itself a remake of the 1974 Malayalam film Chattakkari (1974 film)|Chattakari.  The movie stars Ramya as a young woman that discovers that she has gotten pregnant after a one-night stand with her childhood sweetheart.  

==Plot==
Julie (Ramya) is a young Christian woman who finds herself pregnant after giving into a night of passion with Shashi (Dino Morea), a Hindu follower and her childhood sweetheart. Faced with the choice of abortion or carrying the child to term, Julie chooses to keep the pregnancy a secret from Shashi and goes to a convent, where she will deliver the child and put it up for adoption. Afterwards Julie returns home in the hopes that Shashi will marry her now that she does not have a baby, as she thought that he would not have shown an interest in her otherwise. Despite her well-laid plans, Julie finds that things arent so simple as that. Not only does her brother want her to move to England, but she also meets Richard, a man who doesnt care that she has had a baby out of wedlock. Ultimately Julie manages to reconcile with Shashi and live happily ever after.

==Cast==
* Ramya as Julie.
* Dino Morea as Shashi.
* Sundeep Malani as Mr. Gupta, Julies boss.
* Sihi Kahi Chandru
* Ramesh Bhat
* Chitra Shenoy

==Music==
{{Infobox album
| Name = Julie
| Type = Soundtrack
| Artist = Rajesh Ramanath
| Cover =
| Border =
| Alt =
| Caption =
| Released =  
| Recorded = 2006 Feature film soundtrack
| Length = 39:34 Kannada
| Label = 
| Producer = Aishwarya
| This album=Julie (2006)
| Next album=Tirupathi
}}
Rajesh Ramanath had composed the songs for Julie and also used tunes from the 1975 film.  
{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 39:34
| title1 =  Nanna Ninna Pritiyalli
| extra1 = Kunal Ganjawala
| length1 = 3:48
| title2 = My Heart is Beating Chaitra
| length2 = 5:50
| title3 = Navanitha Chora Priyadarshini
| length3 = 4:13
| title4 = Dhava Dhava Yeh Eyali
| extra4 = Chaitra H. G.|Chaitra, Udit Narayan
| length4 = 5:07
| title5 = Ee Hadu Priyadarshini
| length5 = 4:50
}}

==Reception==
Critical and audience reception was largely negative.   

== References ==
 

 
 
 
 