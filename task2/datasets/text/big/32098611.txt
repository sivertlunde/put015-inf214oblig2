Karishma Kudrat Kaa
{{Infobox film
 | name = Karishma Kudrat Kaa
 | image = KarishmaKudrat.jpg
 | caption = Vinyl Record Cover
 | director = Sunil Hingorani
 | producer = Arjun Hingorani
 | writer = 
 | dialogue = 
 | starring = Dharmendra Mithun Chakraborty Rati Agnihotri Anita Raj Shakti Kapoor  Saeed Jaffrey
 | music = Kalyanji Anandji
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = December 06, 1985
 | runtime = 130 min.
 | language = Hindi
 | country = India Rs 2.5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1985 Hindi Indian feature directed by Sunil Hingorani, starring Dharmendra, Mithun Chakraborty, Rati Agnihotri,  Anita Raj, Shakti Kapoor  and Saeed Jaffrey

==Cast==

*Dharmendra ...  Vijay / Karan 
*Mithun Chakraborty ...  Sub-Inspector Raj 
*Rati Agnihotri ...  Radha (Vijays sister) 
*Anita Raj ...  Paro 
*Urmila Bhatt ...  Vijays mom 
*Saeed Jaffrey ...  Lala Dayaluram 
*Shakti Kapoor ...  Bhagad Singh 
*Padma Khanna ...  Mother of dead child 
*Jankidas ...  Jankidas 
*Dinesh Hingoo ...  Kammobais patron 
*Praveen Kumar ...  Zoravar 
*Lalita Kumari ...  Kammobai 
*Mehmood Jr. ...  Dayalurams assistant 
*Raj Mehra ...  Mahadev 
*Ram Mohan ...  Robert 
*Murad ...  Guru 
*Jagdish Raj ...  Inspector General of Police 
*Prem Sagar ...  Police Commissioner 
*Manik Irani ...  Rangaraj
*Shamim Rahman ...

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dil Mein Paheli"
| Anwar (singer)|Anwar, Sadhana Sargam
|-
| 2
| "Hum Dono Hain"
| Suresh Wadkar, Sadhana Sargam
|-
| 3
| "Kahin Tu Woh To Nahin"
| Udit Narayan, Sadhana Sargam
|-
| 4
| "Ek Din Milkar"
| Manhar Udhas, Sadhana Sargam
|-
| 5
| "Tere Vadon Pe"
| Sadhana Sargam
|}

==References==
*http://ibosnetwork.com/asp/filmbodetails.asp?id=Karishma+Kudrat+Ka

==External links==
*  

 
 
 
 
 