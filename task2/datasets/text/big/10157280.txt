Dear Pyongyang
{{Infobox film
| name           = Dear Pyongyang
| image          = Dear Pyongyang film poster.jpg
| caption        = South Korean theatrical poster
| director       = Yang Young-hee (South Korea)   Yang Yong-hi (Japan)
| producer       = Inaba Toshiya
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = Nakaushi Akane
| distributor    = Cheon, Inc.
| released       =  
| runtime        = 107 minutes
| country        = Japan
| language       = Japanese Korean
| budget         = 
}}
Dear Pyongyang is a documentary film by  : 梁英姬) about her own family. Shot in Osaka Japan (Yangs hometown) and Pyongyang, North Korea, the film features Korean and Japanese dialogue with subtitles. The US release also has Korean and Japanese dialogue, but with added English subtitles.       In August 2006, Yang also released a book in Japanese under the same title expanding on the themes she explored in the film. 

== Story == repatriation campaign sponsored by ethnic activist organisation and de facto North Korean embassy Chongryon; as the only daughter, Yang herself remained in Japan. However, as the economic situation in the North deteriorated, the brothers became increasingly dependent for survival on the care packages sent by their parents. The film shows Yangs visits to her brothers in Pyongyang, as well as conversations with her father about his ideological faith and his regrets over breaking up his family.   

== Film festivals ==
* Sundance Film Festival, 2006 
* Pusan International Film Festival, 2006 
* Berlin International Film Festival, 2006 

== References ==
 

== External links ==
* http://www.film.cheon.jp
*  
*  

 
 
  
 
 
 
 
 

 