Love Island (film)
 
{{Infobox film
| name           = Love Island
| image          = Loveislandpos.png
| image_size     =
| caption        = Film poster
| director       = Bud Pollard
| producer       = Harold Kussell (associate producer) Hall Shelton (producer)
| writer         = John E. Gordon (story) Daniel Kusell (screenplay)
| narrator       = André Baruch
| starring       = See below
| music          = Alfonso Corelli
| cinematography = George F. Hinners
| editing        = Bud Pollard
| distributor    = Astor Pictures Corporation
| released       = 16 July 1952
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Love Island is a 1952 American film directed by   (1935). It was the final directorial effort of Bud Pollard who had previously directed several race films and exploitation films.

== Plot summary == Naviator Lt Taber relates the account of his parachuting from his damaged plane onto the island of Tuba Tara ("Love Island" in English).  He falls in love with one of the local beauties and fights for her.

== Cast ==
*Paul Valentine as Lt. Richard Taber
*Eva Gabor as Sarna
*Malcolm Lee Beggs as Uraka, Sarnas suitor
*Frank McNellis as Aryuna, Sarnas father
*Dean Norton as Tamor, Sarnas friend
*Kathryn Chang as Klepon
*Bruno Wick as Ninga
*Richard W. Shankland as Fumanku, high priest
*Howard Blaine as Capt. Blake
*Vicki Marsden as Stewardess

== Soundtrack ==
* Across the Sea
Written by Jerry Bragin
*Love at First Sight
Written by Jerry Bragin 
Sung by Paul Valentine

== DVD release ==
The film was released on DVD on 21 March 2005.

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 