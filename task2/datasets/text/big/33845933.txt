The Spreading Dawn
{{Infobox film
| name           = The Spreading Dawn
| image          = The Spreading Dawn.jpg
| imagesize      =
| caption        =
| director       = Laurence Trimble
| producer       = Samuel Goldwyn
| writer         = Basil King (short story: The Spreading Dawn)
| starring       = Jane Cowl
| cinematography = Phil Rosen
| editing        =
| distributor    = Goldwyn Pictures
| released       =  
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}} lost with a fragment, apparently only part of reel 3, surviving at the Library of Congress.  

This film was based on a short fiction The Spreading Dawn by Basil King that first appeared in the Saturday Evening Post. It was later expanded and printed as a novel in 1927.

==Cast==
*Jane Cowl - Patricia Mercer Vanderpyl
*Orme Caldara - Anthony Vanderpyl
*Harry Spingler - Bentley Vanderpyl (*billed Harry Springer)
*Florence Billings - Mrs. Cornelia Le Roy
*Henry Stephenson - Mr. LeRoy(*billed Harry Stephenson)
*Alice Chapin - Mrs. Mercer
*Helen Blair - Young Lizzie
*Cecil Owen - Colonel Lee
*Mabel Ballin - Georgina Vanderpyl
*Edmund Lowe - Captain Lewis Nugent
*Edith McAlpin - Old Lizzie

uncredited
*Antoinette Erwin
*Lettie Ford
*Charles Hammond
*Marion Knapp

==References==
 

==External links==
*  
* 
* 

 
 
 
 
 
 
 
 


 