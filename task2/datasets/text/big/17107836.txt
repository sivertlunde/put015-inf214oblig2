Welikathara
 
{{Infobox film
| name = Welikathara
 | image = File:Gamini Fonseka in a scene from Welikathara.jpg
 | caption = Gamini Fonseka as A. S. P. Randeniya in the climax scene of the film
 | director = D. B. Nihalsinghe
 | producer = D. B. Nihalsinghe   Swarnamali Gunatunge
 | writer = Tissa Abeysekera
 | starring = Joe Abeywickrema Gamini Fonseka
 | music = S. Elvitigala
 | Cinematography = D. B. Nihalsinghe
 | editing =
 | distributor =
 | country    = Sri Lanka 1971
 | runtime = 118 minutes Sinhala
 | budget =
 | followed_by =
}}

Welikathara (Sinhala, Desert) is a 1971 dramatic film made by Sri Lankan director D. B. Nihalsinghe.It was the first film directed by D. B. Nihalsingha.

Since Weikathara was the first film in CinemaScope in Sri Lanka, it faced many obstacles, first of which was the lack of access to cinemas capable of showing that format. (Most of them had been reserved for US films). This was solved by arranging a cross circuit release, combining cinemas in the three competing circuits at that time- another first.

The film was a box office success as well as being critically appreciated, going on to be presented by a Sri Lankan Presidential Award as  one of the Ten Best Sri Lankan films of all time.

==Plot==
The film is about the clash between Goring Mudalali (Joe Abeywickrema) and ASP Randeniya (Gamini Fonseka).

The film marked turning points in the career of may of those who were involved in it.

D.B.Nihalsingha went on to direct several other award winning films, "Ridi Nimnaya", "Maldeneiye Simieon" and "Kelimadala", before he went on to pioneer color television production in Sri Lanka and South Asia.

It was loosely based on a true story and the first screenplay by Tissa Abeysekara,  who  He went on to become one of Sri Lankas  best Script writers.

(Later, Dr.) Tissa Abeysekara until then was an "additional dialogue writer" to Dr. Lester James Peris, marked his entry as a screenplay writer. He went on to become Sri Lankas foremost script writer thereafter.
Joe Abeywicrema, who until then was a comedy actor, went on to become a character actor of repute with his magnificent performance.

Gamini Fonseka, who was Sri Lankas foremost star at that time,gave a significant performance where, in total reversal of the heroic roles he portrayed, acted out a character in decline.

 

Somadasa Elvitigala,  added one more feather to his cap after the beautiful music score he created for Sath Samudura.

===Cast===
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Joe Abeywickrema || Goring Mudalali
|-
| Gamini Fonseka || ASP Randeniya
|-
| Suvineetha Weerasinghe ||  
|-
| Sweeneta Abeysekera ||  
|}

==Production==
The film was shot in Point Pedro and was South Asias and Sri Lankas first to be done in cinemascope.  It was a box office success after its first screening on October 27, 1971 and critically well received.

==Awards and nominations== Presidential Film Award in 1999
*Named 10th Best Sri Lankan Film of the first 50 years by Presidential council in 1997

==References==
 

 
 


 