The Vindicator (film)
 
 
{{Infobox film
| name           = The Vindicator
| image          = vindicator_movie_poster.jpg
| image_size     = 214x396
| caption        = VHS cover
| director       = Jean-Claude Lord
| producer       = Don Carmody John Dunning
| writer         = Edith Rey David Preston
| narrator       = Richard Cox Pam Grier Maury Chaykin
| music          = Paul Zaza
| cinematography = René Verzier
| editing        = Michaël Karen
| studio         = Michael Levy Enterprises
| distributor    = 20th Century Fox
| released       =  
| runtime        = 93 min
| country        = Canada English
| budget         =
| gross          =
}} Key Video  (a division of CBS/FOX Video), and is now out of print.  The special effects were by Stan Winston.  

As of 2012, the only known DVD release is in Brazil, where it is known as "Roboman".

==Plot==
 Richard Cox), where all kinds of high-tech research is being conducted. One of these projects is developing "rage program" software that can sense when a user is provoked and take over its brain to destroy the attacker. After Carl confronts Whyte about some suspicious funding cuts, Whyte sends his employee Massey to kill him and disguise the death as a lab accident.

ARC informs Carls pregnant wife Lauren (Teri Austin) and daughter Catherine (Catherine Disher) that Carl has died. In reality, Whyte is keeping Carls charred body in a suspension of oxygenating fluid which keeps his brain functioning. Using Carls space suit and ARCs cutting-edge prosthetics technology, they build Carl a cyborg body and dub him "Project Frankenstein". The rage program is also installed, with a remote control unit acting as a safeguard. The reanimation initially fails, so researcher Gail Vernon (Lynda Mason Green) disassembles the suit. After the remote control unit is removed, Carl suddenly revives and throws Gail onto a control panel in rage. This releases lab monkeys who attack Gail in rage, killing her.

Carl stows away on a garbage truck, which drops him into an incinerator. Thanks to his immense strength, he is able to break out and heads for his residence. On the way, some street thugs chase him into an alley, and he kills them in rage. Realizing that the rage program forbids close contact with people, he talks to his wife from outside their house through a synthesizer in their living room. Later, the thugs bodies are discovered and Whyte, fearing a police investigation of Project Frankenstein, hires the elite assassin Hunter (Pam Grier) to eliminate Carl.

The next day, Lauren visits Carls colleague and friend Burt Arthurs (Maury Chaykin) to tell him about her conversation with Carl. Burt, secretly in cahoots with Whyte, shows her security footage of the accident to convince her of Carls death. In the evening, Carls voice comes over the synthesizer again; this time, however, Whytes accomplice Kessler is monitoring them. Carl learns that Massey signed his autopsy report and death certificate, and sets out to confront him. At his home, secured by Hunters forces, Massey is snorting cocaine and evicting his girlfriend Lisa, when Carl appears to interrogate him. In doing so, Carl learns that if he can get to the suits programming, he can remove the rage program. Massey shoots Carl in panic and gets thrown out a window. Lisa bears witness, so Hunter kills her to cover up the project.
 gas line and manages to incinerate Hunters men as well as Kessler, then escape. Hunter confronts Whyte, who had not disclosed the Frankenstein enhancements, and he admits that the enhanced Carl is nearly indestructible. Meanwhile, Carl arranges a meeting with Lauren by coded message. Upset over his grotesque appearance, he exhorts her to leave him and move far away. He also asks her to summon Burts help, which she does. Unsuspecting of Burts treachery, Carl meets with him, and falls into a trap of quick-setting resin. Whyte sends the block of resin to ARC, but Carls strength prevails and he breaks out during transport.

Burt goes to the Lehman residence, where he confesses his lust for Lauren. Rebuffed, he subdues her by strangulation. Hunter later arrives and kills Catherine. Carl shows up in time to crush Burt inside his car, but Lauren gets captured and taken to ARC to lure Carl. Meanwhile, Whytes girlfriend is being interrogated by the police, hinting that Project Frankenstein will be investigated and defused.

At ARC, Carl manages to evade the security cameras for some time, so Hunter goes on the intercom and threatens to kill Lauren unless he comes to the laboratory to bargain for her. There, Hunter throws Lauren onto Carl to provoke him, but he does not go into a rage, since he reprogrammed himself in the computer room while the cameras werent monitoring him. Realizing she is no match for the cyborg, Hunter commits suicide. Carl and Lauren get to Whyte, who has turned himself into a cyborg along with the corpses of Gail and Kessler. Carl, his life-support failing, battles Whyte while Lauren dispatches the others by pulling their own life-support umbilical cables. As Whyte begins to overpower Carl, Lauren hands him an umbilical, and he drowns Whyte by filling his suit with fluid. His own fluid supply exhausted, Carl expires.

Years later, Lauren is visiting a museum with her son Carl Jr. They admire a display of Carls space suit, which has been successfully deployed on Mars.
==Cast==

*David McIlwraith as Carl
*Teri Austin as Lauren Richard Cox as Whyte
*Pam Grier as Hunter
*Maury Chaykin as Burt
==Production==
It was filmed in Montreal. 

==Critical Reaction==
Janis L. Pallister in her survey of Quebecker cinema calls it "tedious", noting a strong influence from the earlier films of David Cronenberg.   Horror Chronicles praised the special effects, while criticising a thin storyline and uneven pacing.   Film School Rejects criticised the acting and low production values. http://www.filmschoolrejects.com/features/junkfood-cinema-the-vindicator.php  HorrorMovies.ca gave it 7/10. 

==References==
 

==External links==
*  
*  

 
 
 
 


 