Rachel (film)
{{Infobox film
| name = Rachel 
| image = 
| caption = 
| director = Simone Bitton
| producer = Thierry Lenouvel  
| writer = Simone Bitton
| starring = Rachel Corrie
| cinematography = Jacques Bouquin   
| music = 
| editing = Jean-Michel Perez Catherine Poitevin
| distributor = Women Make Movies
| released = 
| runtime = 100 min.
| country = France, Belgium
| language = 
| budget = 
| gross = 
}}
Rachel is a 2009 documentary film by Simone Bitton detailing the death of Rachel Corrie. Rachel was an American peace activist, killed by an Israeli military bulldozer in 2003 during a nonviolent action against the demolition of Palenstinians homes in Rafah, Gaza.

The premiere was at Berlin Film Festival in February 2009. "Rachel is an in-depth cinematic investigation into the death of an unknown young girl, made with a rigor and scope normally reserved for first-rate historical characters. It gives voice to all the people involved in Rachel’s story, from Palestinian and foreign witnesses to Israeli military spokespersons and investigators, doctors, activists and soldiers linked to the affair. The film begins like a classic documentary, but soon develops, transcending its subject and transforming into a cinematographic meditation on youth, war, idealism and political utopia. In the beginning, there is this: she was called Rachel Corrie. She was 23. She was convinced that her American nationality would be enough to make her an effective human shield, that her simple presence would save lives, olive trees, wells and houses."  

Simone Bitton: "I suppose that I initiated the project of Rachel as an attempt to cleanse myself of this shame: I wanted to film in Gaza because I know that if filmmakers stop filming in places where they are not welcome, not only will it become easier for occupation forces everywhere to kill and destroy – but also documentary cinema itself will die – and only the TV news media spectacle of war will remain."  

The first North American public screening was at the 2009 Tribeca Film Festival.  It is said the film reports the case from "an Israeli point of view".  But a controversy arose with respect to the showing of the film at the 2009 San Francisco Jewish Film Festival, and the festivals extending an invitation to Corries mother to speak. Jewish Voice for Peace, the films sponsor, was called "anti-Semitic" by the festival managers. 

== References ==
 

==External links==
* 
* 
*   at Women Make Movies

 
 
 
 
 
 
 