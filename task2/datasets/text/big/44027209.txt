Rajayogam
{{Infobox film
| name           = Raajayogam
| image          =
| caption        = Hariharan
| producer       =
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Muthukulam Raghavan Pillai
| music          = M. S. Viswanathan
| cinematography = TN Krishnankutty Nair
| editing        = VP Krishnan
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film,  directed by Hariharan (director)|Hariharan. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Muthukulam Raghavan Pillai in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
 
*Prem Nazir
*Jayabharathi
*Adoor Bhasi
*Muthukulam Raghavan Pillai
*Bahadoor
*Junior Sheela
*K. P. Ummer Meena
*Nellikode Bhaskaran
*Vidhubala
 

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Mankombu Gopalakrishnan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Akkarappacha Thedi || K. J. Yesudas || Mankombu Gopalakrishnan ||
|-
| 2 || Ezhunilaappanthalitta || K. J. Yesudas || Mankombu Gopalakrishnan ||
|-
| 3 || Muthukkudakkeezhil || P Susheela || Mankombu Gopalakrishnan ||
|-
| 4 || Rathnaakarathinte Madiyilninnum || K. J. Yesudas || Mankombu Gopalakrishnan ||
|}

==References==
 

==External links==
*  

 
 
 


 