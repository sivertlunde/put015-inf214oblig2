Divide and Conquer (film)
{{Multiple issues|
 
 
}}

{{Infobox Film
| name           = Why We Fight: Divide and Conquer
| image          = File:Why We Fight.3.Divide And Conquer.webm
| caption        = Film
| director       = Frank Capra Anatole Litvak
| producer       = Office of War Information
| writer         = Julius J. Epstein Philip G. Epstein
| narrator       = Walter Huston
| starring       = 
| music          = 
| cinematography = Robert Flaherty
| editing        = William Hornbeck
| distributor    = War Activities Committee of the Motion Pictures Industry
| released       = 1943
| runtime        = 57 min
| country        = United States
| language       = English
| budget         = 
}}
Divide and Conquer (1943) is the third film of Frank Capras Why We Fight propaganda film series, dealing with the Nazi conquest of Western Europe in 1940.
 fall of blockading Germany world conquest.

Hitlers treachery towards the small neutral countries of Europe is exposed - to  : "Germany never had any quarrel with the Northern States and has none today" - to the  : "The Reich has put forth no claim which may in any way be regarded as a threat to Belgium".  These quotes are repeated after the conquest of each of these countries is shown.

The first targets of the Nazis in 1940 were Denmark and Norway.  Nazi interest in Norway is described in terms of Germanys desire to use Norways fjords as U-boat bases, and to use airfields in Norway for a bomber attack on the British naval base at Scapa Flow.  After Hitlers surprise invasion of Denmark is briefly mentioned, the film accuses the Nazis of using Trojan Horse ships - designed to look like merchant ships but concealing troops, tanks and artillery guns - as a way of seizing control of all of Norways ports.  The role of Norwegian traitors such as Vidkun Quisling in aiding the Nazi conquest of Norway is also emphasized.  At the end of the section on Norway, Hitler is likened to gangster John Dillinger and Nazi-occupied Norway is portrayed as the northern claw of a giant pincer movement aimed against Britain.  The conquest of France would provide the southern claw.

The films story of France begins in 1914 at the   is displayed on-screen).  The film then goes on to describe the defensive orientation of 1930s France, exemplified by the Maginot Line.  This is explained as being primarily due to the 6 million casualties which France suffered in World War I, but also due to factors including Nazi fifth column activities, political corruption and greedy vested interests.
 regime collaborating with them.]]
 British Expeditionary Force contributed an additional 10 divisions.

The important role of paratroopers in the conquest of the Netherlands is covered, as is the fact that the Germans easily defeated Belgian resistance at Fort Eben-Emael knowing the best method of attack after extensive practice on an exact copy of the fortress built in occupied Czechoslovakia.  Special attention is also paid to Nazi atrocities, such as the Rotterdam Blitz after the surrender of the city and Nazi attacks on villages and small towns (designed to choke roads with refugees and thus impede the Allied troop movements).

It is then mentioned that the Nazis attack on Belgium and the Netherlands was a feint to distract from the main attack through the Ardennes, where the Allies least expected it.  A U.S. military officer shows an animation which demonstrates the German blitzkrieg technique - tanks form the front spearhead, while infantry spill off from the sides to form solid walls, thus protecting the center of the column through which trucks pass to supply all forces involved.
 

==See also==
*List of films in the public domain in the United States
*Propaganda in the United States

==External links==
*  
*  

 
 

 
 
 
 
 
 