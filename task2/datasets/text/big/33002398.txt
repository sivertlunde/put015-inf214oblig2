Main Intequam Loonga
 
{{Infobox film
| name           = Main Intequam Loonga
| image          = 
| image_size     = 
| caption        = 
| director       = Tatineni Rama Rao
| producer       =
| writer         = D. V. Narasa Raju
| narrator       = 
| starring       =Dharmendra Reena Roy
| music          =Laxmikant Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1982
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Main Intequam Loonga  is a 1982 Bollywood sports film starring Dharmendra, Reena Roy, Amrish Puri and Nirupa Roy. Dharmendra plays the role of a boxer in the film. Footage from this film was used in the 2007 film Apne also starring Dharmendra as a retired boxer. It is a remake of the 1978 Kannada film Thayige Thakka Maga, starring Dr.Rajkumar and Padmapriya.

==Plot==
Kumar (Dharmendra) is interested in boxing but her mother Janki (Nirupa Roy) doesnt like him to be a boxer. He falls in love with his neighbour, Mala (Reena Roy) detective by profession. They were soon to marry. The story moves forward by Mala finding out that G.D. (Amrish Puri) who runs a gym where he trains boxers and only wants his trainee to win boxing championship, he does lots of illegal activities and tries to find out proof against him so that he can be jailed. The story takes turn when Janki gets a call from menal hospital that Ganga has completely recovered and can be discharged from the hospital. She shares her agony with her husband and tells him that it would be impossible to give back her son to his biological mother Ganga. Overhearing this conversation, Kumar is shattered. But when Ganga meets his son for the first time, she doesnt reveal him truth herself realising that Janki is heart patient and would die without Kumar and soon moves to Haridwar. Kumar goes to Haridwar to find her and brings her back and give her a house to stay. He doesnt reveal Janki about their association. Soon he comes to know that his biological father, Ajay (Dara Singh) was a boxing champion and was killed by G.D. Kumar decides to take revenge by winning boxing championship against those killers and he finally succeeds. But, seeing murder of her husband murderers, Ganga again loses her mental balance and dies on the spot. Kumar comes back home and break the news in front his parents that Ganga is dead but still doesnt confess that he knows the truth. The story ends with family remembering Ganga.

==External links==
*  

 
 
 
 
 
 

 