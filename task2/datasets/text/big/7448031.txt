High Times' Potluck
{{Infobox Film 
| name = High Times Potluck
| image = 
| caption = 
| director =  Alison Thompson
| producer = 
| writer = Victor Colicchio Nicholas Iacovino
| narrator = 
| starring = Frank Adonis Theo Kogan Charles Malik Whitfield Victor Colicchio Nicholas Iacovino Jason Mewes Jackie Martling Dan Lauria Tommy Chong
| music = 
| cinematography = 
| editing = 
| distributor = 
| released = 
| runtime = 90 minutes
| country = United States
| language = English
| budget = 
}}
High Times Potluck is a 2002 comedy film by High Times that revolves around a mobster in Manhattan who discovers the magic of marijuana.

This comedy follows a suitcase of high-grade marijuana from the farm where it was grown, to its eventual distribution in New York. Along the way the weed passes through the hands of an obnoxious artist (Jason Isaacs), an unfortunate small-time dealer (Jason Mewes), a bereaved Mafia gangster (Frank Adonis), the lead-singer of a punk band (Theo Kogan) and an aging TV detective (Frank Gorshin). This uneven tale of murder, deception, and romance sees a bizarre assortment of characters drawn together, as various plots and sub-plots interweave, culminating in a showdown at a "Reefer Rally".

The film won the Audience Award for "Best Feature Film" at New York International Independent Film Festival, and "Best Comedy" at the Atlantic City Film Festival.

According to Box Office Mojo, it grossed just $3,168 on its opening weekend and $4,827 total. 

 

==External links==
*  

 
 
 
 
 
 
 


 