The Perfect Couple (film)
 
{{Infobox film
| name           = The Perfect Couple
| image          = The Perfect Couple film poster.jpg 
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         = 최강 로맨스
 | hanja          = 最強　로맨스
 | rr             = Choegang Romaenseu
 | mr             = Choegang Romaensŭ}}
| director       = Kim Jung-woo
| producer       = Kim Min-gi Lee Min-ho Heo Chang
| writer         = Lee Jong-seop
| starring       = Hyun Young Lee Dong-wook
| music          = Sohn Moo-hyun
| cinematography = Yun Myeong-shik
| editing        = Ko Im-pyo
| distributor    = Showbox
| released       =  
| runtime        = 109 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = United States dollar|$7,228,471
}} 2007 South Korean film.

== Plot ==
While eating skewered tempura at a street vendor, young reporter Choi Soo-jin accidentally sticks the skewer into the side of a detective, Kang Jae-hyuk, who was chasing a suspected criminal. After this encounter, Soo-jin is told to work on a story about a detective, and the detective turns out to be Jae-hyuk. Soo-jin joins his crackdown on drug dealers, and the two start to fall in love.

== Cast ==
* Hyun Young ... Choi Soo-jin
* Lee Dong-wook ... Kang Jae-hyuk
* Lee Jeong-heon
* Jeon Soo-kyeong
* Jeong Jae-jin
* Kim Jae-man
* Kim Seung-min
* Joo Seok-tae
* Lee Myeong-jin
* Jang Hyun-sung (cameo)

== Release ==
The Perfect Couple was released in   on its opening weekend with 380,933 admissions.  It went on to receive a total of 1,299,274 admissions nationwide,  with a gross (as of 18 March 2007) of United States dollar|$7,228,471. 

== References ==
 

== External links ==
* http://thebest.showbox.co.kr/
*  
*  

 
 
 
 

 