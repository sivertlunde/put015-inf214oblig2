Half-Rate Honeymoon
{{Infobox film
| name           = Half-Rate Honeymoon
| image          = 
| image_size     = 
| caption        = 
| director       = Steve Sekely
| producer       = Ferenc Szigeti
| writer         = Gyula K. Halász, Károly Kristóf   
| narrator       = 
| starring       = Irén Ágay  Pál Jávor (actor)|Pál Jávor Gyula Kabos    György Dénes
| music          = Pál Gyöngy
| editing        = 
| cinematography = István Eiben  
| studio         = Lux Film
| distributor    = 
| released       = 27 October 1936
| runtime        = 85 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    =  Hungarian romance film directed by Steve Sekely and starring Irén Ágay, Pál Jávor (actor)|Pál Jávor and Gyula Kabos. A Hungarian couple take advantage of a cut-price holiday offer in Italy where they visit many of the major sights. A German-language version  Hochzeitsreise zu 50% was also released.

==Cast==
* Irén Ágay  - Kovács Kató
* Pál Jávor (actor)|Pál Jávor - Kerekes Pál, mérnök
* Gyula Kabos  - Pernauer Lajos
* György Dénes - Ifj. Majer úr
* Mici Erdélyi  - Lujza, Pernauer titkárnője és felesége
* Lajos Köpeczi-Boócz  - Id. Majer úr, vezérigazgató

==Bibliography==
* Cunningham, John. Hungarian Cinema: From Coffee House to Multiplex. Wallflower Press, 2004.

==External links==
* 

 

 
 
 
 
 
 