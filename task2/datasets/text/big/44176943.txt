Ivide Ee Theerathu
{{Infobox film
| name           = Ivide Ee Theerathu
| image          =
| caption        =
| director       = PG Vishwambharan
| producer       = Augustine Prakash John Paul (dialogues) John Paul Madhu Srividya Rohini K. P. Ummer
| music          = A. T. Ummer
| cinematography = K Ramachandrababu
| editing        = G Venkittaraman
| studio         = Santhosh Creations
| distributor    = Santhosh Creations
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Rohini and K. P. Ummer in lead roles. The film had musical score by A. T. Ummer.   

==Cast== Madhu
*Srividya Rohini
*K. P. Ummer
*Rahman

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Illikkombil || P Jayachandran, Chorus || Bichu Thirumala || 
|-
| 2 || Kannil Nilaavu || KG Markose || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 