Day Is Longer Than Night
{{Infobox film
| name           = Day Is Longer Than Night
| image          = 
| caption        = 
| director       = Lana Gogoberidze
| producer       = 
| writer         = Lana Gogoberidze Zaira Arsenishvili
| starring       = Daredjan Kharshiladze
| music          = 
| cinematography = Nugzar Erkomaishvili
| editing        = 
| distributor    = 
| released       = May, 1984
| runtime        = 138 minutes
| country        = Soviet Union (Georgia)
| language       = Georgian
| budget         = 
}}

Day Is Longer Than Night ( ) is a 1984 Georgian drama film directed by Lana Gogoberidze. It was entered into the 1984 Cannes Film Festival.   

==Cast==
* Daredjan Kharshiladze
* Tamari Skhirtladze
* Guram Pirtskhalava
* Irakli Khizanishvili
* Guram Palavandishvili
* Leo Pilpani
* Akaki Khidasheli
* Guranda Gabunia
* Sofiko Arsenishvili
* Nika Khazaradze
* Gigola Talakvadze
* Natia Gogochuri
* Amiran Amiranashvili

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 