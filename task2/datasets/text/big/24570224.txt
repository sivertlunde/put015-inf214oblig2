Ghar (film)
{{Infobox film
| name = Ghar
| image =Ghar78.jpg
| caption = DVD cover
| director = Manik Chatterjee
| producer = N N Sippy
| writer = Dinesh Thakur
| narrator =
| starring =Vinod Mehra Rekha
| music = R.D. Burman Gulzar (lyrics)
| cinematography = Nando Bhattacharya
| editing = Waman Bhonsle Gurudutt Shirali
| distributor =N.N. Sippy Productions Eros Entertainment (1998)
| released =9 February 1978 
| runtime = 
| country = India
| language = Hindi
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 Indian Bollywood film directed by Manik Chatterjee, released on 9 February 1978. The film about the young married couple coping with the aftermath of rape, and stars Vinod Mehra and Rekha in lead roles. 

==Plot==
Vikas Chandra (Vinod Mehra) and Aarti (Rekha) have recently married, and they move into their new apartment. One day they go out to watch a late night Hindi movie at a local cinema theatre. The movie gets over well after midnight, and as no cab is found this hour, they decide to walk home by foot. On the way, they are suddenly waylaid by four men, who assault Vikas, leaving him unconscious, and forcibly take Aarti with them. When Vikas regains his senses he finds himself in hospital with a head wound. He is informed that Aarti is in the same hospital, after being gang-raped and assaulted. This incident makes headlines in the media, and is also a subject of discussion by politicians during their election campaign. Vikas feels haunted by this incident, and does not know how to act further. Aarti, on the other hand, has been completely traumatised, and is unable to trust any male. Now the couple faces a serious crisis leading to a loveless relationship, and only a miracle can bring the old spark back into their married life.

==Cast==
* Vinod Mehra as Vikas Chandra 
* Rekha as Aarti Chandra 
* Prema Narayan as Aartis friend  Asit Sen as Chattterjee 
* Dinesh Thakur as Doctor 
* Asrani as Police Inspector
* Madan Puri as Vikas Father 

==Awards==
*Won, Filmfare Best Story Award - Dinesh Thakur
*Nominated, Filmfare Best Actress Award - Rekha

== Music ==
{{Infobox Album |
| Name       = Ghar
| Type       = Album
| Artist     = R.D. Burman
| Cover      = Gharcd.jpg
| Released   =   1978 (India) Feature film soundtrack
| Label      =  Sa Re Ga Ma
| Producer   = N.N. Sippy Tere Mere Sapne (1971)
| This album = Ghar (1978)
| Next album = Jugnu (1973)
}}

The soundtrack of the film contains 6 songs. The music is composed by R.D. Burman, with lyrics by Gulzar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Singer(s)
|-
|1.
| "Aaj Kal Paon"
| Lata Mangeshkar
|-
|2.
| "Aap Ki Aankhon Mein"
| Kishore Kumar, Lata Mangeshkar
|-
|3.
| "Botal Se Ek Baat"
| Mohammed Rafi, Asha Bhosle
|-
|4.
| "Phir Wohi Raat Hai"
| Kishore Kumar
|-
|5.
| "Tere Bina Jiya"
| Lata Mangeshkar
|-
|6.
|"Halka Sa Nasha Rehta Hai
| Lata Mangeshkar
|}

the song "phir wohi raat" is inspired from the carpenters song "sing".

==References==
 
==External links==
* 
 
 
 
 
 
 