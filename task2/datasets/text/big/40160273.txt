Cannibal (2013 film)
 
{{Infobox film
| name           = Cannibal
| image          = 
| caption        = 
| director       = Manuel Martín Cuenca
| producer       =  Alejandro Hernández Antonio de la Torre
| music          = 
| cinematography = Pau Esteve Birba
| editing        = 
| distributor    = 
| released       =  
| runtime        = 116 minutes
| country        = Spain Romania
| language       = Spanish Romanian
| budget         = 
}}

Cannibal ( ) is a 2013 Spanish-Romanian thriller film directed by Manuel Martín Cuenca. It was screened in the Special Presentation section at the 2013 Toronto International Film Festival.   

==Plot== Antonio de la Torre) is a soft-spoken tailor that hides a secret double life as a serial killer. He stalks his prey carefully and carts them off to a remote mountain cabin where he slices them up and takes a bit of their meat home with him to devour later at his convenience. Carlos otherwise lives his life uneventfully until his path crosses with the beautiful Alexandra (Olimpia Melinte), who tries to spark off a relationship with him. Shortly thereafter, Alexandra goes missing and Carlos is contacted by Alexandras twin sister Nina (also played by Melinte), who believes that Alexandra ran off after stealing money from their parents. Carlos offers to help Nina to a degree that makes her instantly suspicious, but she decides to stay with him because Alexandras brutish boyfriend is causing trouble. As the film progresses Carlos finds himself becoming drawn to Nina and eventually he offers to take her to his remote cabin. He decides that he will drug and murder her, but finds himself incapable of performing the task. Carlos tries to confess that hes murdered not only Ninas sister, but also multiple other women and was going to murder Nina as well, but she refuses to believe him. However, as they drive down the mountainside together, Nina deliberately causes a car accident in order to kill them both.

==Cast== Antonio de la Torre as Carlos
* María Alfonsa Rosso as Aurora
* Olimpia Melinte as Nina and Alexandra
* Joaquín Núñez
* Gregory Brossard as Boyfriend

==Reception==
Critical reception for Cannibal has been mixed and the movie currently holds a rating of 50% at Rotten Tomatoes, based on 10 reviews.  Variety (magazine)|Variety gave a favorable review for Cannibal, writing "Sumptuously shot in carefully composed long takes, the film firmly keeps its butchery offscreen, and given its glacial pace and lack of overt sensationalism, it definitely ranks as a niche item — and a rarefied one, at that. But sophisticated arthouse audiences might eat it up." 

In September 2014, the film won the Meliès d’Argent for the best European fantastic film at the Strasbourg European Fantastic Film Festival. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 