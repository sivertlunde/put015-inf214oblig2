Rikky and Pete
 
 
{{Infobox Film
| name           = Rikky and Pete
| director       = Nadia Tass David Parker Bill Hunter Bruno Lawrence Bruce Spence Lewis Fitz-Gerald Dorothy Alison Don Reid
| music          = Eddie Rayner   Phil Judd (songs)
| cinematography = David Parker
| runtime        = 101 minutes
| country        = Australia
| language       = English
| budget = A$4million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p327-329 
| gross = $1,071,875 (Australia)
}}
 David Parker starring Stephen Kearney and Nina Landis. 

==Plot==
Rikky Menzies (Nina Landis) and Pete Menzies (Stephen Kearney) are sister and brother.  Rikky is an out-of-work geologist, and aspiring singer (vocals by Wendy Matthews).  Pete is a misfit who dreams up weird inventions.  Pursued by police sergeant Whitstead (Bill Hunter), on whom Pete has been playing practical jokes, they flee to the mining town of Mount Isa.

== Cast ==
*Stephen Kearney as Pete Menzies
*Nina Landis as Rikky Menzies
*Tetchie Agbayani as Flossie Bill Hunter as Whitstead
*Bruno Lawrence as Sonny
*Bruce Spence as Ben
*Lewis Fitz-Gerald as Adam
*Dorothy Alison as Mrs. Menzies Don Reid as Mr. Menzies
*Peter Cummins as Delahunty
*Peter Hehir as Desk Sergeant
*Ralph Cotterill as George Pottinger
*Roderick Williams as Holy Joe
*Denis Lees as Fingers Robert Baxter as Truckyard Man

==Production==
$3 million of the budget was provided by United Artists with the rest of the money raised privately in Australia through 10BA. Shooting took place in Melbourne and Broken Hill. 

== Reception ==
=== Box Office ===
Rikky and Pete grossed $1,071,875 at the box office in Australia,  which is equivalent to $2,014,185 in 2009 dollars.

==Soundtrack==
*Score by Eddie Rayner and Brian Baker
*Songs for Noels Cowards by Phil Judd
*"Recurring Dream" by Crowded House
*"Run A Mile" by Schnell Fenster
*"Perfect World" by Blue Healers Keith Glass and the Honky Tonks

==See also==
*Cinema of Australia

==References==
 

=== Further reading ===
* Murray, Scott (editor), Australian Film, 1978-1994, Oxford, 1995. ISBN 0-19-553777-7

==External links==
* 

 

 
 
 
 
 
 
 
 
 