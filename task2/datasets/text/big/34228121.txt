Shakti (2012 film)
{{Infobox film
| name           = Shakti
| image          = 2012 Kannada film Shakti poster.JPG
| caption        = Film poster
| director       = Anil Kumar
| producer       = Ramu
| writer         = Anil Kumar
| starring       = Malashri P. Ravi Shankar Sadhu Kokila Ashish Vidyarthi
| music          = Vardhan
| cinematography = Rajesh Kata
| editing        = Lakshman (Picture House)
| studio         = Ramu Enterprises
| distributor    = 
| released       =  
| runtime        = 144 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 action Kannada film directed by Anil Kumar starring Malashri and P. Ravi Shankar. This is Ramu Enterprises 32nd film produced by Ramu. The film released across Karnataka on 6 January 2012.

==Production==
The film production went on floors in July 2011. This marks actress Malashris 8th film with her husbands home banner. Malashri plays tough cop fighting against corruption. Anil Kumar, who was introduced as a script writer makes his debut at the direction and also scripts the screenplay along with dialogues. The film is expected to release in the first week of January 2012.  One of the highlights of the film is the association of 5 stunt directors namely Thriller Manju, Ravi Varma, Ram Lakshman, Palani Raj and Mass Mada. K. Kalyan has written lyrics for the lone song composed by Vardhan in this film. The song "Bandiyu Saguthide" is sung by veteran actor Shivarajkumar. Akash Audio holds the audio recording rights. 

== Cast ==
* Malashri as Shakti
* Sayaji Shinde as Anjanappa
* P. Ravi Shankar
* Ashish Vidyarthi
* Sharath Lohitashwa as Bettadappa
* Avinash
* Sadhu Kokila
* Vinaya Prasad
* Kiran Srinivas as Vijay
* Radhika Gandhi
* Hema Chowdhary
* Ashalatha
* Pratap

==Soundtrack==
{{Infobox album
| Name        = Shakti 
| Type        = Soundtrack
| Artist      = Vardhan
| Cover       = 2012 Kannada film Shakti album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 5 January 2012
| Recorded    =  Feature film soundtrack
| Length      = 4:36
| Label       = Alpha Digitech
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Vardhan composed the films soundtrack for the album consisting of three tracks.  The track "Bandi Saaguthide" was sung by actor Shiva Rajkumar, with lyrics penned by K. Kalyan. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 4:36
| lyrics_credits = yes
| title1 = Bandi Saguthide
| lyrics1 = K. Kalyan
| extra1 = Shiva Rajkumar
| length1 = 3:15
| title2 = Naale Emba (Female Vocals)
| lyrics2 = 
| extra2 = Malashri
| length2 = 0:41
| title3 = Naale Emba (Male Vocals)
| lyrics3 = 
| extra3 = Vardhan
| length3 = 0:40
}}

==Awards==

{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 2nd South Indian International Movie Awards SIIMA Award Best Actor in a Negative Role
|P. Ravi Shankar
| 
|-
|}

==External links==
* 

==References==
 

 
 
 
 


 