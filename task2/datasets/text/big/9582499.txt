Broadway Rhythm
 
{{Infobox film
| name           = Broadway Rhythm
| image          = BroadwayRhythm.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Roy Del Ruth Jack Cummings
| writer         = Screenplay by Dorothy Kingsley and Harry Clork   story by Jack McGowan Based on the musical Very Warm for May by Jerome Kern and Oscar Hammerstein, 2nd.
| narrator       = 
| starring       = George Murphy   Ginny Simms
| music          = Musical direction and supervision Johnny Green Leonard Smith, A.S.C.
| editing        = Albert Akst
| studio         = Metro-Goldwyn-Mayer
| distributor    = 
| released       =  
| runtime        = 115 minutes United States English
| budget         = 
| gross          =
}}
 Jack Cummings Cover Girl (1944).   The film starred George Murphy- who had appeared in Broadway Melody of 1938 and Broadway Melody of 1940. Mayer then replaced Powell with Ginny Simms. Other cast members included Charles Winninger, Gloria DeHaven, Lena Horne, Nancy Walker, Eddie "Rochester" Anderson, the Ross Sisters, and Ben Blue, as well as Tommy Dorsey and his orchestra.

==Plot==
Murphy plays a successful Broadway musical comedy producer named Johnnie Demming.  He needs a star for his new show. He’s smitten with the glamorous film star, Helen Hoyt (Simms), and offers the part to her, but she turns him down because she wants to be sure she’s in a hit.  Johnnie’s father (Winninger), retired from vaudeville, wants to do his own show.  He gets his daughter, Patsy (DeHaven) and also Helen.  Johnnie feels betrayed by his father.

The film is very loosely based on the Broadway musical Very Warm for May (1939). However, all the songs from the musical except for "All the Things You Are" were left out of the film. Some of the songs from the movie are by Jerome Kern and Oscar Hammerstein II: 
* All the Things You Are 
* That Lucky Fellow
* In Other Words
* Seventeen
* All in Fun

==Additional songs==
*"Somebody Loves Me" by George Gershwin, Ira Gershwin Raye and DePaul
*"What Martin and Blane
*"Pretty Pretty Baby" Tony Jackson, Egbert Van Alstyne, Gus Kahn Gabriel Ruiz, Ricardo Lopez Mendez

==Cast==
 
 
Starring
*George Murphy as Jonnie Demming
*Ginny Simms as Helen Hoyt
 
With
*Charles Winninger as Sam Demming
*Gloria DeHaven as Patsy Demming
*Nancy Walker as Trixie Simpson
*Ben Blue as Felix Gross
*Lena Horne as Fernway de la Fer
*Eddie "Rochester" Anderson as Eddie
*Tommy Dorsey and his Orchestra
 
and
*Hazel Scott as Herself
*Kenny Bowers as Ray Kent
*The Ross Sisters as Maggie, Aggie and Elmira
*Dean Murphy as Hired Man
*Louis Mason as Farmer
*Bunny Waters as Bunnie
*Walter B. Long as Doug Kelly
 

==External links==
* 

 

 
 
 
 
 
 
 

 