Salladin the Victorious
{{Infobox film
| name           = Al Nasser Salah Ad-Din
| image          = 
| caption        =
| director       = Youssef Chahine
| producer       = Assia
| writer         = Mohamed Abdel Gawad Youssef Chahine (screenplay) Abderrahman Charkawi (screenplay) Youssef El Sebai (story) Naguib Mahfouz (novel) Ezzel Dine Zulficar
| starring       = Ahmed Mazhar
| music          = Angelo Francesco Lavagnino
| cinematography = Wadid Sirry
| editing        =
| distributor    =
| released       =  
| runtime        = 145 minutes
| country        = Egypt
| language       = Arabic
| budget         =
| gross          =
}}
Saladin the Victorious ( ,  , Hamdi Gheiss, Ahmed Luxor, Nadia Lutfi, Hussein Riad, Laila Taher and Zaki Toleimat. It was entered into the 3rd Moscow International Film Festival.   


==Background==

For some historical context, the movie depicts the events of the Third Crusade. What happens during those events is that after Saladin reclaimed Jerusalem, the European powers led by King Richard of England, Emperor Barbarossa of Germany and King Phillip Augustus of France joined together to reclaim it and return it to Christian hands. This resulted in the war between the Europeans and Saladin, which lasted for three years before a truce was made between Saladin and King Richard, allowing Saladin to keep the land while Christians could freely enter Jerusalem.

When the movie came out, it came at a time when Egypt was free of colonial rule and was released between two wars with Israel. Due to this, the Egyptian government was trying to promote its ideals, with the leader at the time, Gamal Abdel Nasser, being the representative of it. Saladin in many ways references and parallels Nasser as like the president, the movie Saladin pushes forth the ideal of a Pan Arab unity as all Arabs are united in the movie in fighting the European powers, which is no better portrayed then in the line "My dream is to see an Arab Nation under one flag, hearts united and free of hate." This is also portrayed well with Issa, whos an Christian Arab, yet chooses to fight alongside Saladin and his army. Similarly, it also has an anti-colonialism message as the European powers are trying to subjugate the Arab lands under their rule, but they resist and successfully manage to peacefully resolve the war. This is shown in showing the bronze Arabs pulling siege towers at the head of the Crusader army, the Arabs represent those who remain in oppression under imperialistic rule while the mechanical siege towers represent the war-like machines that were present in the battles Egypt fought for their independence.  

==Plot==
 king (Omar emperor under Richard the Lionheart of England.  Saladin succeeds in preventing the recapture of Jerusalem, and in the end negotiations between himself and Richard (whom Saladin admires as the only honorable infidel leader) leave the Holy Land in Muslim hands.

The movie also has a subplot involving the Christian Arab Issa (Salah Zulfikar), and the Crusader Louisa (Nadia Lutfi). At the beginning, both first meet when Issa accidentally comes upon her when shes taking a bath, and after he turns away waiting for her to get dressed before he takes her prisoner due to being a Crusader, she shoots an arrow at him and escapes. Eventually, after Issa in turns spares her life twice, Louisa chooses to give up her arms as a Crusader and becomes a nurse. This leads to the two falling in love and marrying each other, with Louisa choosing to remain in Jerusalem with him.

==Cast==
* Ahmed Mazhar as Saladin King Richard I (Richard the Lion-Heart)
* Leila Fawzi as Virginia, Princess of Kerak
* Nadia Lutfi as Louisa de Lusignan
* Salah Zulfikar as Issa Alauwam
* Tewfik El Dekn as Prince of Akka King Philip of France
* Mahmoud El-Meliguy as Conrad of Montferrat|Conrad, Marquis of Montferrat
* Ahmed Louxor as Raynald of Châtillon king Guy of Lusignan 
* Zaki Tulaimat as Duke Arthur Queen Berengaria
* Ibrahim Emara
* Mohamed Hamdi
* Mohamed Abdel Gawad

==See also==
*Saladin section of legacy in the films and Media.
*List of historical drama films
*Battle of Hattin
*Third Crusade


==References==
 


==External links==
*  
 
 
 
 
 
 
 
 
 
 
 
 
 


 