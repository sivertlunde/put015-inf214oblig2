Satiricosissimo
{{Infobox film
 | name =Satiricosissimo
 | image =Satiricosissimo.jpg
 | caption =
 | director = Mariano Laurenti
 | writer =  Roberto Gianviti   Dino Verde
 | starring =  Franco Franchi Ciccio Ingrassia Edwige Fenech
 | music =  Carlo Rustichelli
 | cinematography = Tino Santoni
 | editing =   Giuliana Attenni
 | producer =  Vittorio Martino Leo Cevenini
 | language = Italian  
 | country = Italy
 | runtime = 89 min
 | released =  
 }} 1970 Cinema Italian comedy film directed by Mariano Laurenti. It is a parody of the 1969 Federico Fellini film Fellini Satyricon.         

== Plot summary ==
Ciccio loves very much the novel Satyricon of Petronius Arbiter, although his friend Franco does not understand him. Ciccio also saw the famous Federico Fellinis film about the novel, and he goes with Franco in a country inn, near Rome. The director, seeing the success of the film by Fellini, does furnish the inn in the fashion of ancient Rome. Even the guests and the waiters are dressed to the ancient, and so do Franco and Ciccio, but they break a jar of wine and are hunted. The two fall asleep in a clearing, and wake up in the Rome of the Emperor Nero.

Franco and Ciccio risk being killed, and so theyre saved just by writer Petronius who hire them as servants. Petronius is the best adviser to Nero, who is scared because he believes that his mother Agrippina wants to kill him. Franco and Ciccio therefore must watch over the life of the emperor, but they soon discover that the killer who wants to murder Nero is not the mother.

== Cast ==

* Franco Franchi as  Franco
* Ciccio Ingrassia as  Ciccio
* Edwige Fenech as  Poppaea Sabina 
*  Giancarlo Badessi  as  Nero
* Arturo Dominici as Tigellinus
* Karin Schubert as  Acte
* Pino Ferrara as  Petronius
* Linda Sini as  Agrippina the Younger Seneca
* Gigi Reder as The Innkeeper 
* Ignazio Leone as  The Judge 
* Samson Burke as  Taurus

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 

 
 