Preethsod Thappa
{{Infobox film
| name = Preethsod Thappa
| image =
| caption = 
| director = V. Ravichandran
| writer = Krishna Vamshi
| based on = Ninne Pelladata (1996)
| producer = Rockline Venkatesh
| starring = V. Ravichandran   Shilpa Shetty   Prakash Raj   Lokesh
| music = Hamsalekha
| cinematography = G. S. V. Seetharam
| editing = P. R. Soundarraj
| studio  = Rockline Productions
| released =  
| runtime = 144 minutes Kannada
| country = India
}}
 Lakshmi and Telugu blockbuster Tabu in the lead roles.

The film released on 6 November 1998 across Karnataka cinema halls and was declared a musical blockbuster with all the songs tuned by Hamsalekha receiving exceptionally well. This was the last film with Hamsalekha - Ravichandran combination before they parted ways due to the differences in opinion. 

== Cast ==
* V. Ravichandran
* Shilpa Shetty 
* Prakash Raj 
* Lokesh Lakshmi
* Srinivasa Murthy
* Doddanna
* Vinaya Prasad Ramakrishna
* Umashree
* Ramesh Bhat
* Y. Vijaya
* Suchitra
* Tennis Krishna
* Mandeep Roy
* Adarsha

== Soundtrack ==
All the songs are composed and written by Hamsalekha. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyrics
|-
| 1 || "Bangaradinda Bannana" || K. J. Yesudas, Anuradha Sriram || Hamsalekha
|-
| 2 || "Sone Sone" || K. J. Yesudas, Anuradha Sriram || Hamsalekha
|-
| 3 || "Ondu Moda" || L. N. Shastry, Anuradha Sriram || Hamsalekha
|-
| 4 || "Dingu Dingu" || Anuradha Sriram, Ramesh Chandra || Hamsalekha
|-
| 5 || "Raja Raja" || Anuradha Sriram || Hamsalekha
|- Hamsalekha
|-
| 7 || "Choriyagide Nanna Dil" || S. P. Balasubrahmanyam, K. S. Chithra || Hamsalekha
|-
|}

==Awards==
* 1999 - Karnataka State Film Award for Best Female Playback Singer - Anuradha Sriram for "Raja Raja" song.

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 