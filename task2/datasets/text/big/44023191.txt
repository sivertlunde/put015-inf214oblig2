Kottaaram Vilkkaanundu
{{Infobox film
| name           = Kottaaram Vilkkaanundu
| image          =
| caption        =
| director       = K Suku
| producer       = Jameena
| writer         = Jagathy NK Achari
| screenplay     = Jagathy NK Achari
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Thikkurissi Sukumaran Nair
| music          = G. Devarajan
| cinematography = Melli Irani
| editing        = MS Mani
| studio         = Suvarna Films
| distributor    = Suvarna Films
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film,  directed by K Suku and produced by Jameena. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir
*Jayabharathi
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Sreelatha Namboothiri
*T. R. Omana
*Bahadoor
*K. P. Ummer Kunchan
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bhagavaan Bhagavaan || Ayiroor Sadasivan, Sreekanth || Vayalar ||
|-
| 2 || Chandrakalabham || K. J. Yesudas || Vayalar ||
|-
| 3 || Chandrakalabham || P. Madhuri || Vayalar ||
|-
| 4 || Janmadinam Janmadinam || P. Madhuri, Ayiroor Sadasivan, Chorus || Vayalar ||
|-
| 5 || Neelakkannukalo..Thottene Njan || P Jayachandran, P. Madhuri || Vayalar ||
|-
| 6 || Sukumaara Kalakal || K. J. Yesudas || Vayalar ||
|-
| 7 || Whisky Kudikkaan || P Jayachandran || Vayalar ||
|}

==References==
 

==External links==
*  

 
 
 


 