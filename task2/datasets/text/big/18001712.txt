Irupatham Noottandu
{{Infobox film
| name           = Irupatham Noottandu
| image          = Irupatham Noottandu.jpg
| caption        = Theatrical release poster
| director       = K. Madhu
| producer       = M. Mani
| writer         = S. N. Swami Ambika Jagathy Shyam   Vipin Das
| editing        = P.V. Krishnan
| studio         = Sunitha Productions
| distributor    = Aaroma Release
| released       =  
| runtime        = 140 mins
| country        = India
| language       = Malayalam
| budget         =   30 Lakhs 
| gross          =  3.75 crores    
}}

Irupatham Noottandu (English: Twentieth Century) is a 1987 Malayalam film directed by K. Madhu and featuring  Mohanlal, Suresh Gopi, Ambika (actress)|Ambika, and Jagathy Sreekumar. It became one of the biggest blockbusters in Malayalam film history. The film grossed more than Rs. 3.75 crores at the box office.  The script was written by S. N. Swamy. The film also has an unofficial sequel in 2009 Sagar Alias Jacky Reloaded.The films sequel was directed by Amal Neerad.The film had a run of about 175 days at the box office.Upon release, it shattered several box office records and became the highest grossing malayalam film which was later broken by "chithram".

== Plot ==
The story is about Sagar Alias Jacky (Mohanlal) & his accomplice Sekharankutty (Suresh Gopi). Ashwathy (Ambika (actress)|Ambika) is a reporter for a newsmagazine looking to investigate the connection between politics and crime in Kerala. She is drawn to the life of Sagar alias Jacky who runs a clandestine gold smuggling business for Sekharankutty, son of the State Chief Minister. Jacky is an enigmatic character who stops the smuggling business from growing because of ethical issues with narcotics, and spends his personal time Ashwathy publishes a sensational article connecting Jacky to the Chief Minister, and pushes the uneasy relationship between Sekharankutty and Jacky into an open confrontation. Srinath & Jagathy also play important roles. Indian Revenue Service officials have been shown in good light in this film.

==Cast==

*Mohanlal as Sagar Alias Jacky
*Suresh Gopi as Shekaran kutty (son of chief minister) Ambika as Aswathy(JOURNALIST) Urvashi
*Kaviyoor Ponnamma
*Jagathy Sreekumar Jose
*Prathapachandran
*Kollam Ajith

==Soundtrack== Shyam and lyrics was written by Chunakkara Ramankutty. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ambarappoo Veedhiyilu || K. J. Yesudas || Chunakkara Ramankutty || 
|}

== References ==
  

== External links ==
*  

 
 


 