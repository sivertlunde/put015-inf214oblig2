Spark: A Burning Man Story
{{Infobox film
| name           = Spark: A Burning Man Story
| director      = Steve Brown and Jessie Deeter
| producer       = Steve Brown and Jessie Deeter
| associate producer = Theresa Desautels
| executive producer = Alec Lorimore, Chris Weitz, David Chang, Robert Zangrillo and Konstantin Othmer
| original music = Joacheim Cooper
| music supervisor = Amine Ramer
| director of photography = John Behrens
| editing        = Andrew Gersh
| studio         = Spark Pictures
| distributor    = Filmbuff
| released       =  
| runtime        = 91 minutes
| rating         = TVMA
| country        = United States
| language       = English
}}
 Spark: A Burning Man Story is a 2013 independent documentary    film with its world premiere at the South by Southwest film festival (SXSW) in Austin, Texas.     That is about some 60,000 or so people that gather from around the world for a week at the annual Burning Man festival in Black Rock Desert of Nevada to collaborate building a temporary city that operates on a "gifting" economy where nothing is bought or sold.  Participants bring in everything; food, water, and shelter.  The week features large-scale art installations and partying, at the conclusion of the week a celebration in the ritual burning of a giant effigy, and after one week, take it all away.   

The film also focuses on the individual artists and their dreams and struggles. The Burning Man organization is made up of 50 year-round staff and over 4,000 volunteers. Burning Man operates on ten principles or ideologies written by founder Larry Harvey.

==Production Credits==
*Production: Spark Pictures	
*Original Music: Joacheim Cooper
*Music Supervisor: Amine Ramer
*Executive Producers: Alec Lorimore, Chris Weitz, David Chang, Robert Zangrillo and Konstantin Othmer
*Editor: Andrew Gersh
*Director of Photography: John Behrens
*Associate Producer: Theresa Desautels
*Produced and Directed: Steve Brown and Jessie Deeter
*Distribution: Filmbuff

==Awards==
*Official Selection, Ashland Independent Film Festival, 2013
*Official Selection, Seattle International Film Festival, 2013   

===Critics reviews===
The film has appeared in the following critics reviews:
{| class="wikitable" style="font-size: 95%;"
|-
! Critic
! Publication
! Date
|- Hilary Armstrong || Huffington Post (San Francisco) || March 14, 2013 
|- Lauren Musacchio August 12, 2013 {{cite news|url=http://www.rollingstone.com/music/videos/spark-a-burning-man-story-dives-into-desert-culture-20130812 | title=Spark: A Burning Man Story Dives Into Desert Culture |
last=Musacchio |first=Lauren|authorlink=Lauren Musacchio|newspaper=RollingStone|date=2013-08-12|accessdate=2014-09-29}} 
|- Geoff Berkshire August 13, 2013 
|- Andy Webster August 15, 2013 
|- David Lewis September 5, 2013 
|- Zel McCarthy September 19, 2013 
|}

==References==
 

==External links==
*   on the Internet Movie Database
*  
**  
*  
**  

 
 
 
 
 