Capitaine Achab
 
Capitaine Achab (in English, Captain Ahab) is a 2004 French short film directed by Philippe Ramos.  It is an interpretation of Moby-Dick by Herman Melville. In this movie, Achab falls in love with Louise, whose white skin is symbolized in his dreams by the whale Moby Dick. The film was presented at the Cannes Film Festival and Pantin in 2004. 

*Directed by Philippe Ramos
*Writing credits : Herman Melville (novel) Philippe Ramos screenplay
*Release date : December 2003 ("Festival du film court de Paris") 19 May 2004 (Cannes Film Festival) 12 June 2004 (Pantin Short Film Festival) May 2004 Arte TV.

==Cast==
*Valérie Crunchant : Louise
* Frédéric Bonpart : Achab
*Alexis Locquet (Achab as a child)
*Aristide Demonico (Achabs father)
*Aymeric Descrèpes (Starbuck)
*Mona Heftre (Achab’s Aunt)

==Awards==
*"Prix de la presse 2003" at the "Festival du film court de Paris"

==External links==
* 
* 

 

 
 
 
 

 