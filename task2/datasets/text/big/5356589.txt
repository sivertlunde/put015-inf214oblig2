Raja Ki Aayegi Baraat
 
{{Infobox film
| name = Raja Ki Aayegi Baraat
| image = rkab.jpg 
| starring = Rani Mukerji Shadaab Khan
| released =   
| country = India
| language = Hindi
}}
Raja Ki Aayegi Baraat ("My King Will Arrive") is an 1997 Indian Bollywood film written and directed by Ashok Gaikwad. 

The film marked the Hindi film debut of actress Rani Mukherjee.  It was shot in Gangtok, and coordinated with the filming of Biyer Phool, another home production of Ram Mukherjee, Ranis father, which Rani was simultaneously shooting in the area. 

== Plot ==

Mala works as a school-teacher and had a great dream that her bridegroom will arrive on a horse and her marriage will be done in a great pomp and ceremony. One day her friend confides in her about her love affair with the son of a wealthy man, who has been intimate with her, but now is marrying someone else. Mala accompanies her friend to this males wedding, disrupts it, and eventually ends up getting it cancelled. The grooms friend, Raj, is enraged and insults Mala, in response to which she slaps him. Enraged all the more, Raj seeks Mala out, and in the presence of her pupils, brutally slaps and then rapes her by getting off her clothes. Then she walks without any clothes with her pupils who hides her body with umbrella as it was raining and all the passers-by beholds her, and when her father looks her in this way he throws a piece of cloth on her to cover her body . The police get involved and Raj is arrested.
 
Rajs dad, Rai Bahadur Diwanchand, attempts to give some money to Malas guardian, Gyani Kartar Singh, who refuses to accept it. The court case proceeds; the judge, overcome by Malas evidence, finds Raj guilty, convicts him, and orders that he marry Mala in the next twenty-four hours. Raj attempts to kill Mala are in vain, and they get married without great pomp and ceremony. After the marriage, Rai Bahadur goes overseas and leaves his family, especially his elder daughter-in-law, Sharda, to deal with Mala and ensure that she is belittled, tortured, and eventually driven out of their house. Shardas younger sister tries to make Raj get trapped in her love, but she fails because Mala insults her and goes to her sister.
 
Sharda, her younger sister and a silly brother go to the airport which Raj reaches before they leave. The three make up lies that Mala told them to get out of the house. Raj gets upset, goes home and starts to beat Mala with his belt, but Kartar Singh gets there in time. Suddenly Rajs mama (uncle) shows up. He pretends to protect Mala and make Raj and Mala leave for their honeymoon, but Raj and his mama actually plan to kill Mala.
 
The uncle leaves a snake in the house when Mala is having a shower. When she gets dressed, she sees the snake on the ledge of the window. She screams and faints. Raj and his uncle think that shes dead. When Raj tries to remove the sindoor on Malas forehead the snake bites his legs. Mala gains consciousness and saves him by sucking the snakes poison. She faints again due to the poison. Raj starts to realise that shes really nice. She is saved and Raj and Mala gradually fall in love. But nobody except Rajs elder brother accepts Mala. When Raj decides to leave, his father hits Mala with a vase on her head and she is badly wounded. Raj takes her to the hospital. The doctor says she is in a critical condition. Rajs father comes to apologise and gets scared when the police comes to investigate the matter. When Mala regains consciousness, she saves her father in law by lying that she has slipped off the stairs. After this incident, she is accepted by Rajs family and Raj and Mala are remarried this time with great pomp.

== Cast ==
*Rani Mukerji as Mala
*Shadaab Khan as Raj
*Javed Khan (actor) as Malas lawyer		
*Gulshan Grover as Gyani Kartar Singh
*Maleeka R Ghai as Malas Friend 		
*Sulabha Deshpande as Kaushalya
*Yunus Pervez as Judge
*Mohnish Behl as Ramesh
*Divya Dutta as Shardas younger sister
*Saeed Jaffrey as Rai Bahadur Arjun as Inspector Khan
*Goga Kapoor ar Gyan Prakash
*G. Asrani as Shardas brother
*Raza Murad as Rajs Uncle
*Jennifer Winget (a kid in school)

== References ==
 

== External links ==
* 

 
 
 
 