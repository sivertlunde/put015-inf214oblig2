Mere Yaar Ki Shaadi Hai
{{Infobox film
| name           = Mere Yaar Ki Shaadi Hai
| image          = Mere yaar ki shaadi hai dvd.jpg
| image_size     = 
| caption        = DVD cover of Mere Yaar Ki Shaadi Hai
| director       = Sanjay Gadhvi
| producer       = Yash Chopra   Aditya Chopra
| writer         = Mayur Puri (dialogue)
| screenplay     = Aditya Chopra   Sanjay Gadhvi
| narrator       = Saurabh Shukla
| starring       = Uday Chopra Tulip Joshi Jimmy Shergill Bipasha Basu
| music          = Pritam Jeet Ganguly
| cinematography = Sunil Patel
| editing        = V.V. Karnik Ritesh Soni
| studio         = 
| distributor    = Yash Raj Films
| released       = 7 June 2002
| runtime        = 159 mins
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 2002 Indian Bollywood film directed by Sanjay Gadhvi and produced by Yash Chopra and Aditya Chopra under Yash Raj Films. The film starred Uday Chopra, Jimmy Shergill, Bipasha Basu and Tulip Joshi. The film was said to be partly inspired by My Best Friends Wedding (1997).  The director Gadhvi, however, claims that he mainly drew from earlier Hindi films and that My Best Friends Wedding only provided less than 1% of the inspiration for his film. It was declared a "semi-hit" by Box Office India.

== Summary ==

Sanjay Malhotra (Uday Chopra) lives in Mumbai with his friend Ria (Bipasha Basu). He receives a phone call from his childhood friend Anjali Sharma (Tulip Joshi) who shocks him with the news that she is getting married. Unfortunately Sanjay has loved Anjali for years. Jealous and frustrated Sanjay makes his way to Anjali with an intention to stop her marriage.

Sanjay goes to Anjalis home and soon meets her groom- Rohit Khanna (Jimmy Shergill). Sanjay then begins to scheme. He organises a bachelor party for Rohit and all the men in the family. Rohit ends up completely drunk. He picks up on the fact that Sanjay is out to stop his marriage and vows to make sure that Sanjay fails in this. Sanjay and Ria try to make Anjali jealous to make her understand she loves him. Later Ria tells Anjali that she and sanjay were never lovers. Then Anjali tells them that she loves Sanjay. Rohit is heartbroken when he comes to know of this. Rohit tells Anjalis mom that she and her daughter, Anjali always  wanted Sanjay to marry Anjali from deep within their heart. On Knowing his absence in her wedding, Anjali  leaves to meet Sanjay in her bridal dress. She asks him why did he left . Sanjay expresses his love for her saying that he cant see her marrying anyone else. Finally both express their love for love each other. Even though Rohit fails in his challenge, he is happy for their love. Sanjay and Anjali get married,Rohit and Ria sing and dance in their best friends wedding.

== Cast ==
* Uday Chopra ... Sanjay (Sanju)
* Tulip Joshi ... Anjali Sharma (Anju)
* Jimmy Shergill ... Rohit
* Bipasha Basu ... Ria
* Dina Pathak ... Dadi
* Deven Verma ... Hari Taya
* Tanaaz Irani ... Anu
* Saurabh Shukla ... Anjalis Mama
* Parikshit Sahani ... Rohits Dad Bindu ... Kapila Tai
* Shamita Shetty ... Dancer (Special appearance in song Sharara)
* Alok Nath ... Anjalis Dad
* Neena Kulkarni ... Anjalis Mom

== Casting ==

The film was first offered to Amisha Patel who was a superstar at the time, but the actress refuse to do the film because she felt there is no meaning to act with flop actors in her beginning career. And then replaced by newcomer Tulip Joshi. Upset with Amishas statement, actor Uday chopra told in an interview that he will never work with this actress and not even Yash Raj Banner will sign her.

== Songs ==
The films music is by the duo Pritam and Jeet Ganguly, with lyrics by Javed Akhtar. The movie initiated Asha Bhosles comeback.
{| class=wikitable
|-
! #
!Song
!Singer(s)
!Picturised on
!Length
|- 1
|Ek Ladki Udit Narayan & Alka Yagnik
| Uday Chopra & Tulip Joshi
|05:37
|- 2
|Humne Suna Hai Udit Narayan, Alka Yagnik, Sudesh Bhonsle & Jaspinder Narula
| Uday Chopra & Tulip Joshi
|05:01
|- 3
|Sharara  Sonu Nigam & Asha Bhosle
| Jimmy Shergill & Shamita Shetty
|04:56
|- 4
|Jaage Jaage  Sonu Nigam, Alka Yagnik & Udit Narayan
| Uday Chopra, Tulip Joshi & Jimmy Shergill 
|05:38
|- 5
|Hum Dono Jaise Kaun Yahan Krishnakumar Kunnath|KK & Sunidhi Chauhan
| Uday Chopra, Jimmy Shergill, Tulip Joshi & Bipasha Basu
|05:41
|- 6
|Mere Yaar Ki Shaadi Hai  Udit Narayan, Sonu Nigam & Alka Yagnik
| Uday Chopra, Jimmy Shergill & Tulip Joshi
|05:41
|-
|}

==References==
 

== External links ==
*  

 

 
 
 
 
 
 