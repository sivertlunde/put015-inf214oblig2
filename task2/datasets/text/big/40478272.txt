The Steagle
{{Infobox film
|image=The Steagle film.jpg
|director=Paul Sylbert
|writer=Irvin Faust (novel) Paul Sylbert (screenplay) 
|producer=
|starring=Richard Benjamin Chill Wills Cloris Leachman Thomas Stanford
|studio=AVCO Embassy Pictures
|distributor=
|runtime=87 minutes
|released=1971
|language=English
|country=United States
}}

The Steagle is a 1971 American film starring Richard Benjamin and directed by Paul Sylbert. 

==Plot==
During the Cuban missile crisis, a professor decides to live out all of his dreams, travelling across the country and taking on a different persona in each city.

==Cast==
* Richard Benjamin	as Harold Weiss, B.A., M.A., Ph.D.
* Chill Wills	as Tall-Guy McCoy
* Cloris Leachman	as Rita Weiss
* Jean Allison	as Florence Maguire
* Suzanne Charney	as Marcy (as Suzanne Charny) George Mann as Dean Briggs
* Ivor Francis	as Reverend
* Susan Tyrrell	as Louise
* Jack Bernardi	as Marty Panesh
* Susan Kussman	as Joan Peter Hobbs as Dr. Payne
* Joseph Bernard	as Max Levine
* Frank Christi	as Mr. Forbes
* Diane Ladd	as Mrs. Forbes
* Harold Reynolds	as Dr. Plymile  
* Warren Munson	as Man on Train

==References==
 

==External links==
* 

 
 
 
 
 
 


 