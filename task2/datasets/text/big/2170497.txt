Raja Harishchandra
{{Infobox film
| name           = Raja Harischandra
| image          = Raja Harishchandra (1913).webm
| image_size     =
| caption        =
| director       = Dadasaheb Phalke
| producer       = Dadasaheb Phalke for Phalkes Films
| writer         = Dadasaheb Phalke
| story          = Ranchhodbai Udayram
| narrator       =
| starring       = D. D. Dabke P. G. Sane
| cinematography = Trymbak B. Telang
| editing        =
| distributor    =
| released       =  
| runtime        = 40 minutes
| country        = India
| language       = Silent film
| budget         =
}}
 1913 Indian Indian silent Indian icon English and Marathi and it is therefore often regarded as the first Marathi film as well.

The film marked a historic benchmark in the film industry in India. Only one print of the film was made and shown at the Coronation Cinematograph. It was a commercial success and paved the way for more such films.   

==Plot==
The film opens with a scene of a tableaux patterned on the painting by Raja Ravi Varma, of Raja Harishchandra and his wife with their son. The film revolves around the noble and righteous king, Harishchandra, who first sacrifices his kingdom, followed by his wife and eventually his children to honour his promise to the sage Vishwamitra. Though, in the end, pleased by his high morals, the Gods restore his former glory, and further bestow him with divine blessings.

==Cast==
Dattatraya Damodar Dabke, a Marathi stage actor played the lead role of Raja Harischandra. The female lead role of Taramati, Harischandras wife was also played by a male actor called  Anna Salunke  and Phalkes son Bhalachandra D. Phalke was the child artist who donned the role of Rohtash, son of Harischandra.  Sage Vishwamitras role was played by G.V. Sane. The story was an adaptation from the Hindu mythology and was scripted by Ranchhodbai Udayram and Dada Saheb Phalke. Other artists in the film were:
*Dattatreya Kshirsagar
*Dattatreya Telang
*Ganpat G. Shinde
*Vishnu Hari Aundhkar
*Nath T. Telang

==Production==
 .   The film was screened at the Coronation Cinema in 1913 before invited audience of the representatives of the press and  guests. The film received wide acclaim and was a commercial success. Phalke followed it up by making films such as Satyavan Savitri, Satyawadi Raja Harish Chandra(1917), Lanka Dahan(1917), Sri Krisna Janma(1918) and Kaliya Mardan(1919). 

Phalke was greatly influenced by the style of painter Raja Ravi Verma in the making of his films. Just as Verma brought Hindu mythology on canvas, Phalke brought it in motion pictures.   He would make many films based on mythological stories from Ramayana, Mahabharata and others.
 nautch girls, to agree to act in film. He hence had to cast a delicate-looking man to play the role of Queen Taramati, Harishchandras wife. Phalke discovered Salunke, who used to work in a restaurant as a cook, for this role. Salunke would later play the role of both Rama and Sita in Phalkes 1917 film Lanka Dahan and become the most popular actor and actress of his time.    Dadasahebs wife cooked food alone, without any help, for the whole cast and crew, which were more than 500 people. She also washed the clothes and costumes, helped in the posters and production of the film, and co-operated with the cast, satisfying them with food and water. 

Harishchandrachi Factory is a 2009 film based on the making of Raja Harischandra. The title is based on the fact that, when the film was made, working in films was taboo, so Dada Saheb advised his artists to tell others that they were working in the factory of one Harishchandra. 

==Classification as first Indian film== last = first = date = url = title = Tornes Pundlik came first, but missed honour|work=The Times Of India
}}  

==Release==
The film premiered on 21 April 1913 at the Olympia Theatre, Grant Road for a selective audience that included famous personalities of Bombay (Mumbai) and editors of many newspapers.  It was first shown in public on 3 May 1913    at Bombays Coronation Cinema, Girgaon, where crowds thronged the roads outside the hall, as it marked the beginning of the Indian film industry. The film was so successful that Dada Saheb had to make more prints to show the film in rural areas as well. The film was a grand success and soon established Phalke as a producer and paved the way for the Indian film industry.

 , Mumbai]]

==Extant Prints== by the same name.  

==Gallery==
  King Harishchandra, in a scene from Raja Harishchandra, 1913. A scene from the film Raja Harishchandra, 1913.
 

==See also==
*Marathi Cinema
*Alam Ara - The first Indian sound film
*List of incomplete or partially lost films
*List of Hindu mythological films

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 
 