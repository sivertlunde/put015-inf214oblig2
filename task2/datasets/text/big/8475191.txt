Boogeyman 2
{{multiple issues|
 
 
}}

{{Infobox film
| name = Boogeyman 2
| image = Boogeyman2.jpg
| caption = DVD release cover
| director = Jeff Betancourt Michael Williams
| writer = Brian Sieve Matt Cohen David Gallagher Mae Whitman Renée OConnor Tobin Bell
| music = Joseph LoDuca
| cinematography = Nelson Cragg
| editing = Jeff Betancourt
| distributor = Columbia Pictures
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget = 
| gross = 
}}
Boogeyman 2 is a 2007 direct-to-video horror film directed by Jeff Betancourt and starring Danielle Savre. It is the sequel to the 2005 film Boogeyman (film)|Boogeyman. Despite being a direct-to-DVD release, the film received a very rare theatrical run in some countries including Russia, Italy, Mexico and Venezuela. A sequel, Boogeyman 3, was released on January 20, 2009.

==Plot== Matt Cohen).Her scotophobic Mark germaphobic Paul masochistic Alison agoraphobic Darren (Michael Graziadei) who is scared of commitment and relationships, and Nicky (Chrissy Griffith), a bulimic girl who fears extreme weight gain.

Laura struggles in facing her fear of the Boogeyman, the entity that Laura believes killed her parents. Then, the therapy group begins to get murdered, one by one. All of their deaths relate to their fears. Mark falls down the elevator shaft, trying to escape from the darkness when the lights go out, and is torn in half. The next is Paul, who accidentally eats a cockroach after finding them in his bag of chips. The killer, a mysterious man wearing a Boogeyman mask and cloak, then hands him a bottle of cleaning solution, which Paul drinks and burns a hole in his throat. Laura begins to suspect these were not accidents. The lights go out at the hospital and all thats left is Laura, Alison, Darren, Nicky, Dr. Ryan and Gloria, the receptionist. Gloria (Lesli Margherita) goes to the basement to check the lights. Alison is killed when the Boogeyman ties her to the bed and places maggots on her arms, which crawl into Alisons skin using the cuts, forcing her to cut them out, killing herself in the process.

Dr. Ryan goes to the basement to check on Gloria and is electrocuted when the Boogeyman throws an electrical cord into a puddle of water she is standing in. Laura finds a file on her brother and ones on other patients with Bogyphobia (what Laura has). She finds out that all Bogyphobia patients including Tim Jensen (character from the first film) have committed suicide after being treated by Dr. Allen. Darren and Nicky have sexual intercourse in the locker room and Laura finds Alison. She gets Darren and Nicky, but when they get to where Alison was, the blood and the maggots have been cleaned up. Darren and Nicky think shes crazy. Darren and Nicky go to Darrens room, where they have an argument. Darren thinks they shouldnt have a relationship. Darren forces Nicky out of the room and is killed when the Boogeyman painfully cuts him open and takes out his heart.

Laura goes to the basement when she hears Nicky scream and finds her on a table. Nicky has hoses attached to her, filling her with bile until she explodes. Laura runs away in shock, and the Boogeyman attacks her. He chases her through the hospital, where she finds Gloria dead and Dr. Ryan barely alive and mumbling, in some sort of trance. Laura begins a cat and mouse game with the Boogeyman, who chases her through the underground storage area, finally running into Dr. Allen who thinks Laura committed the killings. He tries to sedate her, but the Boogeyman stabs Dr. Allen and shoves two needles in his eyes. The Boogeyman is revealed to be Henry, Lauras brother. Henry was treated by Dr. Allen. Dr. Allen abused Henry, locking him in a closet.

Henry was beginning to doubt the Boogeymans existence, until the Boogeyman himself appeared in the closet and entered into him. Whist possessing Henry, the Boogeyman decided to take revenge on those who didnt believe in him, and this way he would make himself known to the world. Henry then chases Laura. She hides in a room, but Henry finds her and she decapitates him with garden shears. The police arrive and discover that under the Boogeyman mask was Dr Ryan. Between killing Dr. Allen and Laura running and hiding, he put a delusional Dr. Ryan in the Boogeyman mask and Laura decapitated Dr. Ryan. Knowing Henry is running free she has a breakdown and is dragged away by the police.

==Cast==
*Danielle Savre as Laura Porter Matt Cohen as Henry Porter
*Tobin Bell as Dr. Mitchell Allen
*Renée OConnor as Dr. Jessica Ryan
*Chrissy Griffith as Nicky 
*Michael Graziadei as Darren
*Mae Whitman as Alison
*Johnny Simmons as Paul
*David Gallagher as Mark
*Lesli Margherita as Gloria
*Tom Lenk as Katy Perry
*Sammi Hanratty as Young Laura Porter
*Jarrod Bailey as Young Henry Porter
*Lucas Fleischer as Mr. Harry Porter
*Suzanne Jamieson as Mrs. Porter
*Christopher John Fields as Detective
* Mocking Bird as Crazy Dave

==Reception==
Filmed on the budget of $4,500,000, the film has made $2,333,673 in Domestic DVD revenue and $562,064 in other States since release. Despite a financial deficit, the film had been able to make a third installment to the Boogeyman film series.

===Critical response===
The film received mixed reviews from audiences, but was declared an improvement over the first installment.

==References==

 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 