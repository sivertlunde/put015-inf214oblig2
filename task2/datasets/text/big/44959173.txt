School for Love
{{Infobox film
| name           = School for Love
| image          = 
| caption        = 
| director       = Marc Allégret
| producer       = Régie du Film Del Duca Films
| writer         = Marc Allégret Roger Vadim based on = novel by Vicki Baum
| starring       = Jean Marais Brigitte Bardot
| music          = Jean Wiener
| cinematography = 
| editing        = 
| distributor    = 
| released       = 26 April 1955 (France); 28 January (West Germany)
| runtime        = 96 minutes
| country        = France
| awards         = 
| language       = French
| budget         =  gross = 919,416 admissions (France) 
}}
 French Drama drama film from 1955, directed by Marc Allégret, written by Marc Allégret, starring Brigitte Bardot and Jean Marais. The scenario was based on a novel of Vicki Baum.  

The film was known under the title "Joy of Living" or "School for Love" (USA), "Sweet Sixteen" (UK), "Reif auf jungen Blüten" (West Germany). 
== Cast ==
* Jean Marais : Éric Walter
* Brigitte Bardot : Sophie
* Isabelle Pia : Élis (Élisa) Yves Robert : Clément
* Denise Noël : Marie Koukowska-Walter
* Mischa Auer (crédité Misha Auer) : Berger
* Lila Kedrova : la mère de Sophie
* Edmond Beauchamp : le père d’Élis
* Yvette Etiévant : la mère d’Élis
* Georges Reich : Dick
* Anne Colette : Marion
* Odile Rodin : Erika
==Production==
It was filmed in locations:  Franstudio, Saint-Maurice, Val-de-Marne|Saint-Maurice, Val-de-Marne. 

== References ==
 

== External links ==
*  
*   at the Films de France
 
 

 
 
 
 
 
 
 
 


 