Arena (2011 film)
{{Infobox film
| name           = Arena
| alt            =  
| image	=	Arena FilmPoster.jpeg
| caption        = 
| director       = Jonah Loop
| producer       = Justin Bursch Mike Callaghan Reuben Liber
| writer         = Robert Martinez Michael Hultquist
| starring       = Samuel L. Jackson Kellan Lutz
| music          = 
| cinematography = Nelson Cragg
| editing        = Harvey Rosenstock
| studio         = 
| distributor    = Stage 6 Films
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $10 million
| gross          = 
}}
Arena is a 2011 action film starring Samuel L. Jackson and Kellan Lutz. Filming took place in Baton Rouge, Louisiana.

==Plot==
The Deathgames is a popular, controversial, and illegal web-show featuring a modern day gladiator arena where combatants fight to the death for the entertainment of online viewers, including a group of college students, and a group of Chinese office workers. Government authorities have been searching to shut down the operation, but to no avail. David Lord (Kellan Lutz), a fireman and paramedic gets into a terrible car accident with his pregnant wife, Lori (Nina Dobrev), who does not survive the crash. Grief-stricken, David considers suicide, but decides against it. Later he drowns his sorrows at a local bar, while a mysterious woman, Milla (Katia Winter), watches him from afar. After witnessing him easily subdue the bars bouncer, Milla seduces David before incapacitating him and allowing him to be kidnapped.

Milla, it turns out, is a recruiter for The Deathgames, which is run by the confident Logan (Samuel L. Jackson) and his two sexy assistants Kaneko (Irene Choi) and Kawaii (Lauren Shiohama). Kaden (Johnny Messner), the executioner is doubtful of the fighting ability of a mere "doctor." David is locked in a small cell and befriends a fellow fighter in the cell next to his, Taiga (Daniel Dae Kim), who tells him that he was coerced into participating due to a threat on his wifes life. David reluctantly wins his first fight, and Milla becomes more invested in his success (as she gets paid when he wins). Logan tells David that he will set him free if he wins ten fights in a row.

In order to persuade David to fight again, Kaden reveals that Taigas wife is in their hands and will be killed if he fails to win his next fight. After David wins, it is revealed that his opponent was Taiga, who was given the same task for his wife. Remorsefully, David kills Taiga, but not before incurring serious wounds of his own. It is then, that David officially accepts Logans offer, on one the condition: the tenth and final opponent must be Kaden himself.

Milla begins to feel bad for David after seeing how badly he is hurt and personally attends to his wounds. She becomes closer to him as he fights his next few fights and has him moved to a more comfortable room with more space and a bed. She brings him food and women. After each fight she personally addresses his wounds. Eventually she realizes her attraction to him and Milla and David have sex.

For the penultimate fight, Kaden arranges for the release of an international serial killer named Brutus Jackson, which leads Logan to believe that Kaden is afraid of David. Before the fight, David tells Milla to contact his brother Sam, who he has not seen since the car accident. She does so, but Sam does not seem to want to hear from David. David defeats Brutus in brutal fight, and Logan is distressed. Logan has a doctor create a serum to inject into David, which will slow him down and make him an easy victim for Kaden in their fight.

Large crowds watch online as Kaden initially pummels David, but much to Logans surprise, David begins to gain the heavy advantage. As they fight, it is revealed through flash backs that "David Lord" is the assumed named of a secret government agent who was specifically sent to infiltrate The Deathgames. The phone call to "Sam" was in order to alert his superior, Agent McCarty (James Remar) about his location via phone tracing. It is also revealed that Milla secretly switched Logans serum with a simple saline solution prior to the fight. A group of soldiers invade the facility and arrest Milla, Kaneko, and Kawaii. Filled with rage after defeating Kaden, David hunts down Logan and corners him in a stockroom. As David is about to strike the deadly blow, soldiers arrive, snapping David out of his murderous fury. Logan escapes.

Agent McCarty consoles David as he deals with the fact that he had to kill so many people and assures him that Logan will be caught. Milla apologizes to David as she is escorted away, and McCarty assures David that she is in good hands.

==Cast==
*Kellan Lutz as David Lord
*Samuel L. Jackson as Logan
*Katia Winter as Milla
*Nina Dobrev as Lori
*Derek Mears as Brutus Jackson
*Daniel Dae Kim as Taiga
*James Remar as Agent McCarty Johnny Messner as Kaden

==Development==
The film had a budget of around $10 million. Filming began in May 2010 in Louisiana. The script was written by Michael Hultquist, Robert Martinez, and Tony Giglio.

According to Heat Vision, Visual effects veteran Jonah Loop made his directorial debut with this indie film.

==Reception==
Arena has received mostly negative reviews, with an aggregate 25% on Rotten Tomatoes.  Criticism focused mostly on the hammy script and shallow plot. The film has also been accused of racial typecasting, as the "Japanese" office workers watching the tournament are actually speaking Chinese.

==References==
 
 
* http://www.empireonline.com/news/feed.asp?NID=27604
* http://www.horror-movies.ca/horror_18212.html
* http://www.heatvisionblog.com/2010/04/samuel-l-jackson-kellan-lutz-eyeing-deathsport-exclusive.html
* http://www.movieweb.com/news/NE65E766TQ1C96
* http://screencrave.com/2010-04-16/samuel-l-jackson-and-kellan-lutz-join-deathgames/
* http://www.reuters.com/article/idUSTRE63K0TB20100421
* http://www.movieweb.com/news/NEmvKsmvViIcqm

==External links==
*  

 
 
 
 
 
 