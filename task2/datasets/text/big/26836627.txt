Vamps (film)
 
 
{{Infobox film
| name           = Vamps
| image          = Vamps (2012 film).jpg
| image_size     = 215px
| alt            = Red lips, sharp vampire teeth
| caption        = Theatrical release poster
| director       = Amy Heckerling
| producer       = {{Plain list | 
* Lauren Versel
* Molly Hassell
* Stuart Cornfeld
* Maria Teresa Arida
* Adam Brightman
}}
| writer         = Amy Heckerling
| starring       = {{Plain list | 
* Alicia Silverstone
* Krysten Ritter
* Sigourney Weaver
* Dan Stevens Richard Lewis
* Wallace Shawn
* Justin Kirk
* Kristen Johnston
* Malcolm McDowell
}}
| music          = David Kitay
| cinematography = Tim Suhrstedt
| editing        = Debra Chiate
| studio         = Red Hour Films Lucky Monkey Pictures
| distributor    = Anchor Bay Films 
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| gross          = $3,361 
}}
Vamps is a 2012 American comedy horror film that reunites Clueless (film)|Clueless director Amy Heckerling with actors Alicia Silverstone and Wallace Shawn and was released on November 2, 2012. 

==Plot==
Stacy (Krysten Ritter) and Goody (Alicia Silverstone), are two socialite vampires living the good life in New York City. Goody was turned in 1841 by the vampire queen Ciccerus (Sigourney Weaver). She struggled with her life as a vampire until Stacy was turned by Ciccerus sometime during the early 1990s. Goody was able to teach Stacy how to use her new abilities, like sustaining themselves on rat blood, but keeps her actual age a secret because she is afraid of being viewed as old.
 Richard Lewis), whom she has not seen since the 1960s. They re-connect under the pretense that she is Goodys daughter, but Danny eventually learns the truth when he sees her bite into another man to prevent a stroke. When he asks why she left him, Goody explains that even though she loved him, she did not want to stand in his way of finding someone he can actually build a life with.

Meanwhile, Stacy begins a relationship with a young college student named Joey (Dan Stevens). It is soon revealed that Joey is the son of the infamous vampire slayer Abraham Van Helsing|Dr. Van Helsing (Wallace Shawn) who is in town to find and kill Ciccerus. After spending the night at Joeys place, he sees  Stacy crawling down the side of his apartment building in order to get home before the sun rises. Despite his initial shock, Joey accepts that Stacy is a vampire and the two resume dating. When Stacy begins to experience strange vomiting, Goody discovers that Stacy is actually pregnant, but that the baby wont survive unless she becomes human again. 

Goody tells Stacy that they must become human again in order for her to stay pregnant, and the two decide that while they have had a lot of fun as vampires, "being young is getting kinda old". Goody then goes to Dr. Van Helsing to get his help in killing Ciccerus. When asked why she wanted to become a vampire, Goody explained that her husband and sisters had been killed during a cholera epidemic, and that she had to stay alive in order to protect her two children. Van Helsing then has his assistant look up Goodys living descendants; most of them being doctors, teachers, and politicians. The girls team up with Joey and Dr. Van Helsing to track down Ciccerus and after a struggle, they end up killing Ciccerus in order for Stacy to keep her baby and have a future with Joey. Stacy looks relatively the same despite being  forty years old, but Goody rapidly ages into an old woman. Revealing her actual age, Goody accompanies Joey and Stacy to Times Square where Goody reminisces about her life. As the sun rises, she disintegrates into ash.

A few years later, Stacy and Joey show up at Dr. Van Helsings house to pick up their young daughter, whom they have named after Goody. As Van Helsing plays with his granddaughter, he notices that the little girl sports a set of vampire fangs.

==Cast==
* Alicia Silverstone as Goody
* Krysten Ritter as Stacy
* Sigourney Weaver as Ciccerus
* Dan Stevens as Joey Van Helsing Richard Lewis as Danny
* Wallace Shawn as Abraham Van Helsing|Dr. Van Helsing
* Justin Kirk as Vadim
* Kristen Johnston as Mrs. Van Helsing
* Malcolm McDowell as Vlad Tepish
* Marilu Henner as Angela
* Zak Orth as Renfield
* Larry Wilmore as Professor Quincy
* Meredith Scott Lynn as Rita
* Brian Backer as the Dentist
* Taylor Negron as the Pizza Guy
* Amir Arison as Derek
* Todd Barry as Ivan
* Gael García Bernal as Diego Bardem
* Joel Michaely as Peter - Nerdy Sales Guy

==Production==

===Casting=== Time Stands Still.  Krysten Ritter was Heckerlings first choice for the role of Stacy.  Michelle Pfeiffer was initially offered the role of Ciccerus but had to turn it down due to scheduling commitments. {{cite news|url=
http://www.tgdaily.com/games-and-entertainment-features/62734-on-clueless-vampires|title=On clueless vampires|publisher=TG Daily|last=Konow|first=David|date=April 13, 2012|accessdate=April 14, 2012}} 

===Filming===
Principal photography lasted 37 days in Detroit, Michigan|Detroit, while using New York for exterior shots.    Red Hour Films produced the film, alongside Lucky Monkey Pictures. 

== Release == limited theatrical release began on November 2, 2012,  followed by a Blu-ray and DVD release.

== Reception ==
Review aggregation website Rotten Tomatoes gives the film a score of 50% based on reviews from 16 critics. 

==See also==
*Vampire film

== References ==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 