Allegro Non Troppo
 
{{Infobox film
| name        = Allegro Non Troppo
| image       = Allegro_non_Troppo_LOW.jpeg
| caption     = Theatrical release poster
| writer      = Bruno Bozzetto Guido Manuli
| starring    = Maurizio Micheli Maurizio Nichetti Néstor Garay Maurialuisa Giovannini
| director    = Bruno Bozzetto
| producer    = Bruno Bozzetto
| studio      = Bruno Bozzetto Film Ministero del Turismo e dello Spettacolo (uncredited)
| distributor = Roxy International (1977) (Italy) Specialty Films (1977) (USA) (subtitled)
| released    =  
| runtime     = 85 minutes
| country     = Italy
| language    = Italian
| music       =
}}
 animated film directed by Bruno Bozzetto. Featuring six pieces of classical music, the film is a parody of The Walt Disney Company|Disneys Fantasia (1940 film)|Fantasia, two of its episodes being derived from the earlier film.    The classical pieces are set to color animation, ranging from comedy to deep tragedy. At the beginning, in between the animation, and at the end are black and white live-action sequences, displaying the fictional animator, orchestra, conductor and filmmaker, with many humorous scenes about the fictional production of the film.  Some of these sections mix animation and live action.

The film has been released in two versions, the first includes live action sequences in between the classical pieces. The second version of the film omits these, replacing them with animated plasticine letters spelling out the title of the next piece of music.

==Title==
In music, an instruction of "allegro ma non troppo" means to play "fast, but not overly so". Without the "ma", it means Not So Fast!, an interjection meaning "slow down" or "think before you act". The common meaning of "allegro" in Italian is "joyful". The title reveals therefore a dual meaning of "allegro", and in addition to meaning "Not So Fast!" can also be read as "joyful, but not too much".

== Program ==
*Claude Debussy|Debussys Prélude à laprès-midi dun faune, an elderly satyr repeatedly attempts to cosmetically recapture his youth and virility, all in vain.   With each failure, the satyr gets smaller and smaller, until he roams across a vast countryside which turns out to be a womans body.
 Slavonic Dance No. 7, Op. 46, begins in a large community of cave-dwellers. A solitary cave man wants to break away from the group and builds himself a new home. From this point on, the rest of the community copies everything that he does.  His attempts to break away from them leads to his planning a bizarre act of mass vengeance with unintended and humorous consequences.

*Maurice Ravel|Ravels Boléro, primordial sugar water at the bottom of a Coca-Cola bottle left behind by space travelers attains life, and progresses through fanciful representations of the stages of evolution and history until skyscrapers destroy all that has come before. This segment parallels The Rite of Spring segment from Fantasia. Its opening moment was used as the image for the film poster.
 Valse triste, a cat wanders in the ruins of a large house. The cat remembers the life that used to fill the house when it was occupied.  Eventually all of these images fade away,  as does the cat, just before the ruins are demolished.

*Antonio Vivaldi|Vivaldis Concerto in C major for 2 Oboes, 2 Clarinets, Strings and Continuo RV 559, a female bee attempts to eat a small meal, but is continually interrupted by two lovers sitting down and then becoming romantically involved in the grass.  Finally the bee decides enough is enough and the male lover gets it in the end.

*Igor Stravinsky|Stravinskys The Firebird (specifically The Princesses Khorovod and The Infernal Dance of King Katschey) begins with a lump of clay molded by a monotheistic symbol of the omniscient pyramid, first making a few unsuccessful creatures with overly awkward limbs, then finally the Adam and Eve as portrayed in Book of Genesis|Genesis. Adam and Eve then transform into cel animation and, as in Genesis, the serpent comes up to them, offering the fruits of knowledge in the form of an Tree of the knowledge of good and evil|apple. After they refuse it the serpent swallows the apple himself. Falling asleep, he is immediately plunged into a nightmare in a hellish environment where he is first tormented by fiery demons and then plagued by things that are supposed to corrupt humankind (sex, alcohol, money, material objects, drugs, violence);  he also grows arms and legs and is magicked into a suit and fedora. When he wakes up, he is still wearing the suit and hat but after telling Adam and Eve his dream in a fast-motion and incomprehensible manner, he sheds the suit (losing his arms and legs but keeping the hat) and spits up the still-whole apple.

*In an epilogue sequence (which features an assortment of short, unidentified orchestral clips instead of a single piece, though Slavonic Dance No. 7 can be very briefly heard again towards the end) the films host asks an animated Igor (fictional character)|Igor-type monster (identified as "Franceschini") to retrieve a finale for the movie from a basement storeroom. Franceschini rejects several of these, but delightedly approves of one which depicts a ridiculously escalating war, ending with the earth exploding. The serpent from the Firebird Suite pops out and bites him on the nose, and the words "HAPPY END" drop on them.

===Live action sequences===
The uncut film also contains comic live action sequences that parody the Deems Taylor introductions from Fantasia. "The Presenter" (Maurizio Micheli) introduces "The Orchestra Master" (Néstor Garay), an orchestra made up of little old ladies, and "The Animator" (Maurizio Nichetti).  A pretty young cleaning woman (Marialuisa Giovannini) also appears in each segment, although shes barely acknowledged by any of the characters except the Animator (who seems to take an increasing fancy to her as the movie progresses). Each sequence portrays action (like the tossing of a Coca Cola bottle) that leads directly into the next animated portion of the film. After the "Ravels Bolero" chapter, a gorilla (inspired by the animated character in the Bolero) also appears a few times, first chasing then dancing with The Animator, then later beating up the Orchestra Master who has attacked the Animator.

==Cast==
* Maurizio Micheli as The Presenter
* Maurizio Nichetti as The Orchestra Master
* Néstor Garay as The Animator
* Maurialuisa Giovannini as The Cleaning Girl
* Osvaldo Salvi as Man in Gorilla costume

== Notes ==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 