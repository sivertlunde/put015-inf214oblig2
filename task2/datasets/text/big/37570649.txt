Magic Temple
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Magic Temple
| image          = Magictempleposter.jpg
| director       = Peque Gallaga Lore Reyes
| producer       = Malou Santos Charo Santos-Concio Trina Dayrit
| story          = Peque Gallaga Lore Reyes Erik Matti
| screenplay     = Peque Gallaga Lore Reyes
| starring       = Jason Salcedo Junell Hernando Marc Solis Anna Larrucea Jun Urbano Jackie Lou Blanco Gina Pareño Aljon Jimenez Koko Trinidad Christopher Peralta Carlo Aquino
| music          = Archie Castillo
| cinematography = Joe Tutanes
| editing        = Danny Gloria
| studio    = Star Cinema
| released       =  
| runtime        = 
| country        = Philippines Tagalog
| budget         = 
| gross          = 
}}
 Filipino family film|family-fantasy film|fantasy-adventure film directed by the acclaimed director Peque Gallaga and released by Star Cinema. It was written by Peque Gallaga, Lore Reyes and Erik Matti. It is notable for winning all of the 7 nominations including the Best Film at the 1996 Metro Manila Film Festival.

==Plot==
The magical world of "Samadhi" is threatened by the evil forces of Ravenal and Sifu sends out three teenage boys to journey on the Magic Temple. Jubal, Sambag and Omar, each with their own unique power battles the threat to the world of "Samadhi" and along the way faces many extraordinary things.
The boys are sure to face a huge battle ahead of them but with the help of magical creatures they met along their journey, evil is eliminated and they learn the true value of camaraderie and believing in them selves to face any problem.

==Cast and characters==
*Jason Salcedo (Gino Zipagang) as Jubal
*Junell Hernando as Sambag
*Marc Solis as Omar
*Anna Larrucea as Yasmin
*Jun Urbano as Sifu
*Jackie Lou Blanco as Ravenal
*Gina Pareño as Telang Bayawak
*Aljon Jimenez as Rexor
*Cholo Escaño as Sisig
*Koko Trinidad as Grand Master
*Sydney Sacdalan as Shaolin Child
*Chubi del Rosario as Gamay
*Mae-Ann Adonis as Rexors mother
*Tess Dumpit as Jubals mother
*Kristopher Peralta as young Rexor
*Jay Rodas as Krystala

==Trivia==
*The names of the three main characters has national symbolism in it. Jubal is an Igorot, a tribe from Luzon; Sambag is a Visayan word for Tamarind from the Visayas and Omar is a name found among the Moslem tribes of Mindanao. The names, therefore, accounts for the Philippines major islands.

==See also==
*Magic Temple (film series)

==Recognitions==

===Awards and nominations===
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 1996
| rowspan="13" align="left"| Metro Manila Film Festival   . IMDB. Retrieved 2014-04-09.  Best Picture
| align="center"| Magic Temple
|  
|- Best Director
| align="center"| Peque Gallaga and Lore Reyes
|  
|- Best Screenplay
| align="center"| Peque Gallaga, Lore Reyes and Erik Matti
|  
|- Best Original Story
| align="center"| Peque Gallaga, Lore Reyes and Erik Matti
|  
|- Best Production Design
| align="center"| Rodell Cruz 
|  
|- Best Cinematography
| align="center"| Joe Tutanes 
|  
|- Best Editing
| align="center"| Danilo Gloria
|  
|- Best Musical Score Archie Castillo
|  
|- Best Original Theme Song
| align="center"| Archie Castillo
|  
|- Best Visual Effects
| align="center"| Benny Batoctoy
|  
|- Best Make-up Artist
| align="center"| Siony Tolentino
|  
|- Best Sound Recording
| align="center"| Michael Idioma and Ronald de Asis
|  
|- Best Float
| align="center"| Magic Temple
|  
|-
| rowspan="5" align="center"| 1997
| rowspan="3" align="left"| FAMAS Awards
| align="left"| Best Sound Michael Idioma and Ronald de Asis
|  
|-
| align="left"| Best Child Actress Anna Larrucea
|  
|-
| align="left"| Best Supporting Actress Gina Pareño
|  
|-
| rowspan="2" align="left"| Gawad Urian Awards
| align="left"| Best Cinematography Joe Tutanes
|  
|-
| align="left"| Best Production Design Rodell Cruz
|  
|}

===Special awards===
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 1996
| rowspan="1" align="left"| Gatpuno Antonio J. Villegas Cultural Awards  Best Picture
| align="center"| Magic Temple
|  
|}

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 