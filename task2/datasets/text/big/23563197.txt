Saawan... The Love Season
{{Infobox film
| name           = Sawaan... The Love Season सावन  
| image          = Saawan... The Love Season. 2006 film.jpg
| image_size     =
| alt            =
| caption        =
| director       = Saawan Kumar Tak
| producer       = Saawan Kumar Tak
| writer         = Saawan Kumar Tak
| narrator       =
| starring       = Saloni Aswani Kapil Jhaveri Salman Khan
| music          = Aadesh Shrivastav
| cinematography = Rajendra Prasad K.
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 121 min.
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} Hindi Social Saawan Kumar, starring Saloni Aswani, Kapil Jhaveri, Ranjeet, Prem Chopra and Salman Khan amongst others in pivotal roles. The title of the film was named after the director Saawan Kumar.

==Plot==

Saawan... The Love Season tells the story of a couple, Raj (Kapil Jhaveri) and Kajal (Saloni Aswani). They get married, and when they come back from their Honeymoon in Patiala, Kajal finds a man who predicts the future (Salman Khan). Whether its the death of a person at 9 p.m. or a major accident on the Mumbai-Pune Expressway, the man knows it all. Hes a modern-day Nostradamus. When Kajal asks him about her future, he tells her shed die two days later. Obviously, Kajal is shattered, she tells Raj who swears if anything happens to her, he would kill the Nostradamus. On the fateful day, Kajal is mistakenly shot by the cops outside a shopping mall and dies. Raj blames the Nostradamus for it, breaks into his house, bashes him black and blue and Nostradamus dies. When Nostradamus dies, Kajal, who is already pronounced dead, wakes up. Shes alive now. She falls in love with Raj all over again, and with the help of Rajs friend Funsukh (Johny Lever) and his admirer, they get the couple re-married and they live happily ever after.

==Cast==
* Saloni Aswani as Kajal Kapoor
* Kapil Jhaveri as Raj
* Prem Chopra as Fakkirchand Kapoor
* Ranjeet as Rajs dad
* Johnny Lever as Funsukh
* Bobby Darling as Funsukhs admirer
* Kiran Rathod
* Salman Khan as Nostradamus (cameo)

==Soundtrack==
* Punjabi Ankhonwali - Shaan (singer)|Shaan, Sunidhi Chauhan
* Tu Mila De -
music composer  
Sonu Nigam
* Ready for Love -  Vasundhara Das Shaan
* Jo Maangi Khuda se - Kunal Ganjawala
* Mere Dil Ko Dil Ki Dhadkan Ko - Shaan (singer)|Shaan,  Shreya Ghoshal  

==References==
 
==External links==
*  

 
 
 