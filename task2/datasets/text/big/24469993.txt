Ladies of Leisure
{{Infobox film
| name           = Ladies of Leisure
| image          = LadiesOfLeisurePoster.JPG
| image_size     = 225px
| alt            = 
| caption        = original newspaper advertisement
| director       = Frank Capra
| producer       = {{Plainlist|
* Frank Capra  
* Harry Cohn
}}
| writer         = 
| screenplay     = Jo Swerling
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Barbara Stanwyck
* Ralph Graves
}}
| music          = Mischa Bakaleinikoff   Joseph Walker
| editing        = Maurice Wright
| studio         = Columbia Pictures
| distributor    = Columbia Pictures Woolf & Freedman Film Service (UK)
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} romantic drama film directed by Frank Capra and starring Barbara Stanwyck and Ralph Graves. The film is based on the 1924 play Ladies of the Evening by Milton Herbert Gropper,  and has a screenplay by Jo Swerling. 

The film is about an artist from a wealthy family who hires a "party girl" as his model. As they get to know each other, the girl begins to regret her past, and the two fall in love, but they must face his familys strong opposition to their union. Ladies of Leisure received generally positive reviews, and Stanwycks performance was praised by critics.    The success of the film made Stanwyck a star.    

==Plot==
Aspiring artist Jerry Strong (Ralph Graves), the son of a wealthy railroad tycoon, sneaks out of a party he allowed his friend Bill Standish (Lowell Sherman) to hold at his New York penthouse apartment and studio. While out driving in the country, Jerry meets self-described "party girl" Kay Arnold (Barbara Stanwyck), who is escaping from another party aboard a yacht, and gives her a ride back to the city. He sees something in her and offers her a job as his model for a painting titled "Hope". In their first session, Jerry wipes off her makeup to try to bring out her true nature. Perpetual partier and drunkard Standish thinks Kay looks fine just the way she is and invites her on a cruise to Havana. She declines his offer.

As they get to know each other better, Kay falls in love with Jerry and comes to rue her tawdry past. This is reflected in her face, and she finally achieves a pose Jerry finds inspiring. He paints so late into the night that he offers to let her sleep on his couch.

The next morning, Jerrys father John (George Fawcett) shows up and demands he dismiss Kay and marry his longtime fiancée Claire Collins (Juliette Compton). John has found out all about Kays checkered background; she does not deny the facts. When Jerry refuses, John cuts off all relations with his stubborn son. Kay decides to quit anyway for Jerrys benefit. This forces him to declare he loves her. She suggests running off to Arizona.

Jerrys mother (Nance ONeill) comes to see Kay. Though Kay convinces her that she genuinely loves Jerry, Mrs. Strong still begs her to give him up for his own good. Kay tearfully agrees and makes plans to go to Havana with Bill Standish. Her roommate and good friend Dot Lamar (Marie Prevost) races to tell Jerry, but by the time she reaches him, the ship has sailed. Despondent, Kay tries to commit suicide by leaping into the water. When she awakens in the hospital, Jerry is waiting at her bedside.

==Cast==
*Barbara Stanwyck as Kay Arnold
*Ralph Graves as Jerry Strong
*Lowell Sherman as Bill Standish
*Marie Prevost as Dot Lamar
*Nance ONeil as Mrs Strong
*George Fawcett as John Strong
*Juliette Compton as Claire Collins
*Johnnie Walker as Charlie

==Production== Broadway in late 1924, and after Capra completed a first draft screenplay he invited Jo Swerling &ndash; a playwright from New York who had been brought to Hollywood by Cohn    &ndash; to work on the script. Swerling initially declined because he thought it was a "putrid piece of Gorgonzola (cheese)|gorgonzola",  "inane, vacuous, pompous, unreal, unbelievable – and incredibly dull",    but decided to work on it nonetheless. "I went to my hotel, locked myself in my room and for five days pounded out a rewrite story of the plot Id heard, interrupting the writing only long enough for black coffee, sandwiches and brief snatches of sleep", the screenwriter later recalled. "I was simply writing a newspaper yarn with a longer deadline than usual." McBride 1992, pp. 212–220. 
 Frank Fay, The Noose at Warner Bros., and Capra was so impressed by it he urged Cohn to sign her immediately. 

When filming began, Capra quickly learned Stanwyck was unlike any actress he previously had directed. In his autobiography The Name Above the Title, he recalled:
 
 Joseph Walker on four silent films. The director was impressed not only with Walkers artistic vision, but his various camera-related inventions as well. McBride 1992, p. 189.  He not only ground his own lenses, but he used a different one for each of the actresses he photographed.  Many of the elements typical of Capra films – the backlighting of actresses, the transformation of minimal sets into dreamlike images, the delicate night scenes and erotic rain scenes – were suggested to Capra by Walker. McBride 1992, p. 215.  The two collaborated on twenty projects between 1928 and 1946. McBride 1992, p. 190. 

Ladies of Leisure was filmed from December 1929 to January 1930, at Columbias studios and on location at    Both of Capras silent and sound versions are held by the Library of Congress.  The film was remade in 1937 by Columbia as Women of Glamour,  starring Virginia Bruce and directed by Gordon Wiles. 

==Critical reception==
Following the films premiere on April 5, 1930, Photoplay magazine reported that halfway through the showing, the audience "choked up" and that "something was happening ... a real, beautiful, thrilling wonder had been born."  The Photoplay article continued:
 

In his review for The New York Times, Mordaunt Hall praised the film for "its amusing dialogue, the restrained performances of nearly all the players and a general lightness of handling that commends the direction of Frank Capra."    Hall concluded, "The picture is sufficiently variegated in drama and more amusing moments to be attractive film fare." 

The review in Variety (magazine)|Variety was less positive, noting that Stanwyck "delivers the only really sympathetic wallop of the footage" and "saves the particular picture with her ability to convince in heavy emotional scenes." 

TV Guide rated the film 2½ out of four stars and noted, "Capra kept everyone under tight rein and any tendency to emote was admirably stifled under his firm direction." 

==Awards and honors==
When the film was released, Capra was convinced that it would garner Academy Award nominations for himself and Stanwyck, and possibly even for best film.  When it failed to get a single nomination, both he and Columbia studio head Harry Cohn were outraged, and sent angry letters to the Academy of Motion Picture Arts and Sciences.  As a result, Capra became a member of the Academy but, wanting more, campaigned to be one of the institutions board of governors, and was elected to a three-year term. 

==References==
Explanatory notes
 

Citations
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 