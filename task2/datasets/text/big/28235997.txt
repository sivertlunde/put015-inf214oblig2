Call a Messenger
{{Infobox film
| name           = Call a Messenger
| image          = 
| image_size     =
| director       = Arthur Lubin
| producer       = Ken Goldsmith (associate producer)
| writer         = Arthur T. Horman (screenplay) Michael Kraike (story) Sally Sandlin (story)
| starring       = The Dead End Kids Little Tough Guys
| music          = Hans J. Salter
| cinematography = Elwood Bredell Charles Maynard
| distributor    = Universal Studios
| released       = November 4, 1939
| runtime        = 65 min
| country        = United States
| language       = English
| budget         =
}}
Call a Messenger is a 1939 Universal Studios film that starred Billy Halop and Huntz Hall of the Dead End Kids and several of the Little Tough Guys.

==Plot==
Jimmy Hogan and his gang are caught robbing a post office. Jimmy is given a choice to either go to reform school or work as a messenger boy for the post office as punishment. Jimmy decides to be a messenger boy, and soon drags his pals into the job. The kids eventually enjoy their jobs, especially when their new boss, Frances ONeill, turns out to be quite attractive.

After becoming friends with fellow messenger boy Bob Prichard, Jimmy decides to hook Bob up with his sister, Marge. He feels that Bob is a much better match for Marge then a local gangster who has been spending too much time with her. Pretty soon, Jimmys brother Ed returns home from prison. At first, Jimmy is glad to have his brother back home, but pretty soon, he and Ed get mixed up with some gangsters who plan on robbing the post office.

==Chronology==
*In terms of chronological order, this was released after the Dead End Kids film, The Angels Wash Their Faces.

==Production==
Dead End Kids Billy Halop and Huntz Hall returned to Universal beginning with this film. Universal decided to pair the Dead End Kids with the Little Tough Guys. Pretty soon, most of the other Dead End Kids would also sign on. In this film, the billing appears as: Billy Halop and Huntz Hall of the Dead End Kids and the Little Tough Guys.

Most of the Little Tough Guys returned for this film, reprising the roles they had in their previous films. The only regular Little Tough Guy who did not return for this film was Monk (Charles Duncan). Ironically, at one point in this film, Jimmys sister makes a reference to Monk, even though he is absent.

==Cast==

===The Dead End Kids===
*Billy Halop - Jimmy Hogan
*Huntz Hall - Ratholemew Pig Smith

===The Little Tough Guys===
*Hally Chester - B. Murph Murphy
*Billy Benedict - P. Trouble Kearns
*David Gorcey - J. Yap Crawley
*Harris Berger - R. Sailor Walters

===Additional Cast=== Robert Armstrong - Kirk Graham
*Mary Carlisle - Marge Hogan
*Anne Nagel - Frances ONeill
*Victor Jory - Ed Hogan
*Buster Crabbe - Chuck Walsh
*El Brendel - Baldy Jimmy Butler - Bob Prichard
*George Offerman Jr. - Big Lip

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 