Paid in Error
{{Infobox film
| name           = Paid in Error
| image          = 
| image_size     = 
| caption        = 
| director       = Maclean Rogers
| producer       = A. George Smith
| writer         = H. F. Maltby Basil Mason
| story          = John Chancellor
| based on       =
| narrator       = 
| starring       = George Carney Lillian Christine Tom Helmore
| music          = 
| cinematography = 
| editing        = 
| studio         = George Smith Enterprises
| distributor    = 
| released       =  
| runtime        = 68 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Paid in Error is a 1938 British comedy film directed by Maclean Rogers and featuring George Carney, Lillian Christine and Tom Helmore. A man is mistakenly given a large sum of money at the bank. 

==Cast==
* George Carney as Will Baker
* Lillian Christine as Joan Atherton
* Tom Helmore as Jimmy Randle
* Marjorie Taylor as Penny Victor
* Googie Withers as Jean Mason
* Molly Hamley-Clifford as Mrs. Jenkins
* Jonathan Field as Jonathan Green
* Aubrey Mallalieu as George

==References==
 

== External links ==
*  
*   at BFI

 
 
 
 
 
 


 