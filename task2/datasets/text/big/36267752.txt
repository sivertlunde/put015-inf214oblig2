A Gentleman of Leisure (1915 film)
 
{{infobox film
| name           = A Gentleman of Leisure
| image          =
| imagesize      =
| caption        =
| director       = George Melford
| producer       = Jesse Lasky
| based on       =  
| starring       = Wallace Eddinger
| music          =
| cinematography = Walter Stradling
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = United States
| language       = Silent English intertitles
}}
A Gentleman of Leisure is a surviving  1915 silent film comedy produced by Jesse Lasky and distributed by Paramount Pictures. It stars stage veteran Wallace Eddinger. The film is based on a novel A Gentleman of Leisure by P. G. Wodehouse and 1911 Broadway play adapted by Wodehouse and John Stapleton. A young actor named Douglas Fairbanks was a cast member in the play several years before beginning a film career. This film survives in the Library of Congress.   

==Cast==
*Wallace Eddinger - Robert Edgar Willoughby Pitt
*Sydney Deane - Sir Thomas Pitt
*Gertrude Kellar - Lady Julia Blunt Tom Forman - Sir Spencer Dreever
*Carol Holloway - Molly Creedon
*Fred Montague - Big Phil Creedon (as Frederick Montague)
*William Elmer - Spike Mullins (as Billy Elmer)
*Frederick Vroom - Macklin, Pitts Friend
*Francis Tyler - Willett, Pitts Friend
*Monroe Salisbury - Stutten, Pitts Friend
*Mr Machin - Fuller, Pitts Friend
*Florence Dagmar - Kate
*Lawrence Peyton - Ole Larsen (as Larry Peyton)
*Robert Dunbar - Jeweler
*Lucien Littlefield - Clerk

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 