The Motel (film)
{{Infobox Film
  | name = The Motel 
  | image = TheMotelposter.jpg
  | caption = Out of Place in the Middle of Nowhere.
  | producer =  Michael Kang Michael Kang
  | starring = Jeffrey Chyau Sung Kang Jade Wu Samantha Futerman
  | music = Nathan Larson
  | distributor = PalmPictures
  | released = January 24, 2005 (Sundance)
  | runtime = 76 minutes English Guangzhou Cantonese
  | budget = $250,000
  }}
 Michael Kang.  The film won the Humanitas Prize in the Sundance Film Festival category, and was nominated for an Independent Spirit Award for Best First Feature.

It is based on the novel Waylaid by Ed Lin.

==Plot==
Thirteen-year-old Ernest Chins life is devoted to working at his familys hourly-rate motel, where a steady stream of prostitutes, johns, and various other shady characters come and go.  Abandoned by his father, he lives with his mother, grandfather, and younger sister Katie.  The film is a loosely assembled series of vignettes examining the difficulty of   man named Sam Kim (Sung Kang), who is caught in a downward spiral after estrangement from his wife.

==Reception==
The Motel was met with critical acclaim and particular praise went to Michael Kangs directing. The film scored a rating of 87% on the review-aggregate website Rotten Tomatoes based on 31 reviews.   

Bob Longino of Atlanta Journal-Constitution liked the film and wrote, "There is honesty and integrity in the filmmaking and the performances, which make The Motel among the best character studies of the year." 

Elizabeth Weitzman from New York Daily News said that the Motel is "Unlike so many indie films, Michael Kangs gently empathetic debut embraces eccentricity without drowning in its own hip irony." 

V.A. Musetto of the New York Post exclaimed in his review "Kang makes an impressive feature directorial debut with The Motel. But the person to keep an eye on is Jeffrey Chyau, a student at the Bronx High School of Science, who is a delight in the lead role." 

Stephen Holden of the New York Times wrote in his positive review that "Michael Kangs small, perfectly observed portrait of a Chinese-American boy captures the glum desperation of inhabiting the biological limbo of early adolescence." 

Joey Leydon at Variety (magazine)|Variety magazine said of the film, "Indie coming-of-age dramedy about a precocious Chinese-American youth whose family operates a sleazy roadside motel signals arrival of a singularly promising filmmaker." and ended with comparing Kangs directing to acclaimed Indie directors Francois Truffaut and Frank Whaley saying "Writer-director Michael Kang covers familiar territory mined memorably by auteurs ranging from Francois Truffaut to Frank Whaley." 

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 