Dead Dudes in the House
{{Infobox film
| name = Dead Dudes in the House
| image = Deaddudesinthehouse.jpg
| caption = Original VHS Cover. None of the actors featured on the cover are in the actual movie.
| director = James Riffel
| producer = Mark Bladis Melissa Lawes James Riffel
| writer = James Riffel
| starring = {{plainlist|1=
* James Griffith
* John Dayton Cerna
* Sarah Newhouse
* Douglas Gibson
* Victor Verhaeghe
* Mark Zobian
}}
| music =
| cinematography =
| editing = Valerie Schwartz
| distributor = Troma Entertainment
| released = 1991
| runtime = 95 minutes
| language = English
| budget =
| preceded_by =
| followed_by =
}}
Dead Dudes in the House, originally titled The Dead Come Home and sometimes released as The House on Tombstone Hill,   is a 1991 independent horror film written and directed by James Riffel and distributed by Troma Entertainment.

For reasons unknown, Troma advertises the characters in Dead Dudes as "hip-hop teens", as evidenced by the actors on the green VHS cover (who do not appear in the movie), despite the fact that there is no reference to hip-hop of any kind throughout the entire movie.  

According to Troma president Lloyd Kaufmans book All I Need to Know about Filmmaking I Learned from the Toxic Avenger, Troma fans consider Dead Dudes in the House to be one of the companys unsung masterpieces.

==Plot==
The film follows a group of teenagers who travel up to a dilapidated house for some fixing up. Unbeknownst to them, the house is occupied by a murderous old woman and her sultry daughter, who proceed to pick the teens off one by one.

==Cast==
* James Griffith
* J. D. Cerna (billed as John Dayton Cerna)
* Sarah Newhouse
* Douglas Gibson
* Victor Verhaeghe
* Mark Zobian

==Release==
The VHS and DVD of the film are both available on Amazon and eBay. Createspace has made the film available on a DVD-R via Amazon. No official release is planned as of 2010.

==References==
 

==External links==
* 
*  – at the Troma Entertainment movie database

 
 
 
 

 