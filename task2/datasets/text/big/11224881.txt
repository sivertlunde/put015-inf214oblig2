A Midsummer Night's Dream (1909 film)
{{Infobox film
| name           = A Midsummers Night Dream
| image          = A Midsummer Nights Dream (1909 film).webm
| caption        = Charles Kent J. Stuart Blackton (co-director)
| producer       = J. Stuart Blackton
| writer         = Eugene Mullin (scenario) William Shakespeare (play)
| narrator       =
| starring       = Walter Ackerman Charles Chapman Dolores Costello Helene Costello
| music          =
| cinematography =
| editing        =
| distributor    = Vitagraph Studios
| released       =  
| runtime        = 1 reel
| country        = United States
| language       = Silent film
| budget         =
| gross          =
}} Charles Kent classic play by William Shakespeare.

==Plot== Lysander (Maurice Demetrius (Walter Helena (Julia Puck (Gladys Hulette) away with a magic herb, which, dabbed on the eyes of a sleeping person, shall make the "victim" fall in love with the first person to appear after awakening. Soon, Lysander and Demetrius are smitten with the wrong girls and Titania has fallen in love with Nick Bottom|Bottom, the egotistical leader of the tradesmen, whom Puck has turned into an ass. When Penelope discovers all this mischief, she lifts the spell and the wedding of the duke and Hippolyta can proceed.

==Cast== Demetrius
*Charles Chapman
*Dolores Costello
*Helene Costello Lysander
*Rose Tapley as Hermia Helena
*Florence Turner as Queen of Fairies Puck
*Elita Proctor Otis as Hippolyta

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 