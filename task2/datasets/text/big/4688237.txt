Once Upon a Time, Cinema
 
{{Infobox film
| name           = Once Upon a Time, Cinema
| caption        =
| image          = Once Upon a Time, Cinema FilmPoster.jpeg
| director       = Mohsen Makhmalbaf
| producer       = Mohammad Mehdi Dadgo
| writer         = Mohsen Makhmalbaf
| starring       = Ezzatolah Entezami Mehdi Hashemi
| music          =
| cinematography =
| editing        = Mohsen Makhmalbaf
| distributor    = Facets Multimedia Distribution
| released       =  
| runtime        = 90 minutes
| country        = Iran
| language       = Persian
| budget         =
}}
Once Upon a Time, Cinema (  i.e. Naser al-Din Shah, Movie Star) is a 1992 Iranian comedy fantasy film written and directed by Mohsen Makhmalbaf. 

The film includes clips from old Iranian films from the silent age onwards. Made in black-and-white (with a brief colour sequence), it parodies many of the conventions of silent slapstick comedy and early fantasy films.

==Plot== Mehdi Hashemi), who is looking for someone called Atieh (Future). As he calls out to her, he is magically transported back in time from the early twentieth century to the reign of Naser al-Din Shah in 19th century Iran. Captured by the Shahs guards, he shows films from the (future) history of Iranian cinema to the Shah (Ezzatolah Entezami). The Shah is entranced and eagerly shows his family the apparently magical medium. 
 his harem slide, Golnar rejects him, and attempts to escape, leading to a slapstick chase-scene.

Meanwhile the cinematographer is showing films to the Shahs wives. The Shah himself wants everyone to see Lor Girl. There is a big open-air showing. Other films are also shown, notably the vigilante revenge drama Qeysar (film)|Qeysar (1969). The vigilante hero comes out of the film. The Shah talks to him about using him against his enemies, and confesses that his one true love in the harem was killed by his other jealous wives. Now he only dreams of Golnar. Meanwhile, disturbed by the disruptive power of cinema, the Shahs advisors meet to discuss how to censor this dangerous medium.
 The Cow (1969), a film about a man believes he is a cow. He then instructs him to pull a plough to help a poor family. The cinematographer is arrested and condemned for insulting the Shah. As he is about to be executed, the full power of cinema is unleashed, blowing the court away. The film moves into colour, ending in a series of clips of people embracing.

==Accolades and interpretations==
Naser-ed-din Shah won two Special Jury Prizes from the Istanbul International Film Festival and the Karlovy Vary International Film Festival. 
 The Actor, the first dealt with cinema and the people, the second with cinema and the artist. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 