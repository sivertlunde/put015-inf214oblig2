Everly (film)
 
{{Infobox film
| name           = Everly
| image          = Everly poster.jpg
| alt            = 
| caption        = Theatrical release poster Joe Lynch
| producer       = {{Plainlist|
* Rob Paris
* Andrew Pfeffer
* Adam Ripp
* Luke Rivett}}
| screenplay     = Yale Hannon
| story          = {{Plainlist|
* Yale Hannon
* Joe Lynch}}
| starring       = Salma Hayek
| music          = Bear McCreary
| cinematography = Steve Gainer
| editing        = Evan Schiff
| studio         = {{Plainlist|
* Crime Scene Pictures
* Vega, Baby!
* Anonymous Content}}
| distributor    = {{Plainlist|
* The Weinstein Company|RADiUS-TWC
* Dimension Films}}
| released       =   
| runtime        = 92 minutes  
| country        = United States
| language       = {{Plainlist|
* English
* Spanish
* Japanese}}
| budget         = 
| gross          = 
}} action Thriller thriller film Joe Lynch and written by Yale Hannon based on a story by Lynch and Hannon. The film stars Salma Hayek as the title character with Akie Kotabe, Jennifer Blanc, Masashi Fujimoto, Togo Igawa, Gabriella Wright, Caroline Chikezie, Laura Cepeda, and Hiroyuki Watanabe in supporting roles.
 limited theatrical release on February 27, 2015 by Dimension Films.

==Plot==
Everly (Salma Hayek), a prostitute working for the brutal criminal overlord Taiko (Hiroyuki Watanabe), is being attacked in her apartment by Taikos enforcers after he discovers that she has been working with the police to bring down his organization. However, though Taiko expected his men to kill her easily after torturing her, Everly retrieves a gun she had hidden and manages to kill all of her attackers. Taiko then begins a sadistic game of toying with Everly, dispatching numerous hired killers and offering the other prostitutes in the building (which he presumably owns) a bounty if they manage to kill her. Meanwhile, Everly attempts to contact her mother Edith (Laura Cepeda) and young daughter Maisey (Aisha Ayamah) to save them from other henchmen of Taiko and get them out of the situation alive.

As the film progresses, it is revealed that Everly was a prisoner in the plush apartment after having been kidnapped by Taiko four years earlier and forced into prostitution. She has had no contact with Edith or Maisey during that time, and they are confused and angry at the deadly situation they are suddenly thrust into. As Everly fights off the numerous attacks, the foes become more outlandish; originally they are simply armed thugs, but eventually they appear in costumes and with themed groups for extravagant tortures. 

Eventually, Edith is killed by a sniper hired by Taiko from the building across from the apartment. Everly is finally subdued, but manages to kill Taiko after his soldiers have left. Afterwards, Everly reconciles with Maisey, who was under the protection of one of Everlys neighbors, who was eventually killed. The film closes with Everly seemingly succumbing to her wounds, but with Maisey still alive and potentially safe with the death of Taiko. However, immediately before the credits, there is audio of a beeping heart-monitor and gasp of breath to indicate that Everly might not have died after all.

==Cast==
* Salma Hayek as Everly	
* Akie Kotabe as Dead Man
* Laura Cepeda as Edith		
* Jennifer Blanc as Dena
* Togo Igawa as The Sadist
* Gabriella Wright as Anna
* Caroline Chikezie as Zelda
* Hiroyuki Watanabe as Taiko
* Jelena Gavrilović as Elyse
* Masashi Fujimoto as The Masochist
* Dragana Atlija as Lizzy
* Aisha Ayamah as Maisey

==Production==
On February 1, 2012, Kate Hudson joined the cast.  On May 10, 2013, Salma Hayek joined the cast to replace Hudson in the lead role. 

==Release==
Everly premiered at Fantastic Fest on September 20, 2014.   The film was released on iTunes on January 23, 2015,  and was released theatrically on February 27, 2015 by Dimension Films. 

===Critical reception===
Everly received generally negative reviews from critics. The  , which assigns a normalized rating out of 100 based on reviews from critics, the film has a score of 35 based on 16 reviews, indicating "generally unfavorable reviews." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
  
 
 
 
 
 
 
 
 
 
 
 