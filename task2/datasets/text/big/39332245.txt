My Black Mini Dress
{{Infobox film
| name           = My Black Mini Dress 
| image          = My Black Mini Dress-poster.jpg
| caption        = Promotional poster for My Black Mini Dress 
| director       = Heo In-moo
| producer       = Jang Suk-bin   Jung-hyun   Kim Woo-sang   Kim Jung-bok   Ahn Soo-yeon 
| screenplay     = Heo In-moo
| based on       =  
| starring       = Yoon Eun-hye   Park Han-byul   Cha Ye-ryun   Yoo In-na 
| music          =  
| cinematography = Yoon Hong-sik  
| editing        = Ham Sung-won
| studio         = Tori Pictures
| distributor    = CJ Entertainment
| released       =  
| runtime        = 107 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
| film name      = {{Film name
 | hangul         = 마이 블랙 미니드레스 
 | hanja          = 
 | rr             = Mai Beulraek Minideureseu 
 | mr             = }}
}}

My Black Mini Dress ( ; also known as Little Black Dress) is a 2011 South Korean film starring Yoon Eun-hye, Park Han-byul, Cha Ye-ryun and Yoo In-na.  Based on the 2009 chick lit novel of the same title by Kim Min-seo, the film revolves around the dreams, failures, and friendship of four 24-year-old women still looking for a direction in life.  

==Plot==
Yoo-min (Yoon Eun-hye), Hye-ji (Park Han-byul), Soo-jin (Cha Ye-ryun) and Min-hee (Yoo In-na) were the best of friends in college. While majoring in theater and film at an elite university, they were united by their passion for Seouls clubbing scene and luxury shopping, but life isnt so easy now that theyre in the real world. Clueless about what to do with her life, Yoo-min takes up a job as an assistant to a famous TV scriptwriter in order to buy an expensive black mini dress (or "little black dress"), but her work turns out to be babysitting her bosss twin boys. Rich girl Min-hee plans to study abroad but shes not going anywhere until she improves her English. Socialite Hye-ji shoots to fame after appearing in a Levi Strauss & Co.|Levis ad, but her newfound stardom creates a rift with her friends. Aspiring actress Soo-jin is at her wits end after failing so many auditions, and things only get worse when her father goes bankrupt. 

==Cast==
*Yoon Eun-hye as Yoo-min  
*Park Han-byul as Hye-ji 
*Cha Ye-ryun as Soo-jin 
*Yoo In-na as Min-hee 
*Lee Yong-woo as Seok-won
*Jeon Soo-kyung as writer
*Shin Dongho as Yoo Seung-won
*Gil Eun-hye as assistant writer 
*Lee Mi-do as pre-college girl
*Moon Soo-jong as Yoo-mins father
*Ko Gyu-pil as Yoo-shin
*Ahn Chi-yong as Min-hees father
*Won Jong-rye as Min-hees mother
*Kim Choon-gi as Young-mis father
*Choi Min-geum as Young-mis mother
*Choi Yoon-young as Young-mi
*Paul Stafford as English teacher
*Lee Chun-hee as Soo-hwan (cameo)
*Ko Chang-seok as director (cameo)
*Shin Seung-hwan as assistant director (cameo) Kim Kwang-kyu as pre-college girls father (cameo)
*Moon Hee-kyung as Yoo-mins mother (cameo)
*Baek Soo-ryun as lottery ticket grandmother (cameo)

==References==
 

==External links==
*    
*   at Naver  
*  
*  
*  

 
 
 
 