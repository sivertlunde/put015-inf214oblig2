Anarkali (1955 film)
 
{{Infobox film 
  | name = Anarkali
  | image =
  | caption =
  | director = Vedantam Raghavaiah
  | producer = Anjali Devi
  | writer =
  | starring =Akkineni Nageswara Rao Anjali Devi Adinarayana Rao P.
  | cinematography = Kamal Ghosh
  | editing =
  | distributor =
  | released = 1955
  | runtime =
  | language =  Telugu
  | country = India
  | budget =
  | preceded_by =
  | followed_by =
    }} Telugu romance film based on the love story between Prince Salim and Anarkali, courtesan in Moghul emperor, Akbar.

==Cast==
*Anjali Devi as Anarkali Salim
*S. V. Ranga Rao as Akbar
*Kannamba		
*Chittor V. Nagaiah		
*Surabhi Balasaraswati		
*Peketi Sivaram

==Crew==
*Direction : Vedantam Raghavaiah	 	
*Producer : Anjali Devi	 Adinarayana Rao P.	
*Lyrics : Samudrala Raghavacharya 	
*Cinematography : Kamal Ghosh	 	
*Art Direction : A.K. Sekhar and Vaali	 	
*Playback Singers : Jikki, Ghantasala Venkateswara Rao and P. Susheela

==Songs==
*"Anda Chandalu Gani" - P. Susheela
*"Jeevitame Saphalamu" - Jikki Ghantasala & Jikki
*"Nanu Kanugonuma" - Jikki 
*"Rajasekhara Neepai Moju Teeraledura" - Ghantasala & Jikki
*"Ninugaana Sambarana" - Jikki
*"Prema Janga" - Jikki
*"Anandame" - Jikki
*"Anarkali" - Ghantasala
*"Ravoyi Sakha" - Jikki

==References==
 

==External links==
*  

 
 
 
 
 

 