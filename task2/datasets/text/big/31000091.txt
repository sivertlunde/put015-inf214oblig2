This Is the Life (1944 film)
{{Infobox film
| name           = This Is the Life
| image          = 
| image size     = 
| caption        = 
| director       = Felix E. Feist
| producer       = Bernard W. Burton
| writer         = Wanda Tuchock
| based on       =  
| starring       = Donald OConnor Susanna Foster Peggy Ryan
| music          = 
| cinematography = Hal Mohr
| editing        = 
| distributor    = Universal Pictures
| released       =  
| runtime        = 87 min.
| country        = United States
| language       = English
}}
 American film Army to serve in World War II.

== Cast ==

* Donald OConnor as Jimmy Plum
* Susanna Foster as Angela Rutherford
* Peggy Ryan as Sally McGuire
* Louise Allbritton as Harriet West Jarrett
* Patric Knowles as Maj. Hilary Jarret
* Dorothy Peterson as Aunt Betsy
* Jonathan Hale as Dr. Plum
* Eddie Quillan as Gus
* Frank Jenks as Eddie
* Frank Puglia as Music Teacher
* Maurice Marsac as Leon
* Virginia Brissac as Mrs. Tiggett
* unbilled players include Mantan Moreland

== External links ==

*  

 
 
 
 
 
 
 
 

 