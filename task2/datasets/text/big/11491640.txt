Flick (film)
 
 
{{Infobox film
| name = Flick
| image = Flick cover.jpg
| caption = 
| director = David Howard
| producer = Rik Hall
| writer = David Howard Liz Smith Hayley Angel Wardle Julia Foster
| music = Richard Hawley
| cinematography = Chris Seager
| editing = Madoc Roberts
| distributor = 
| released =  
| runtime = 96 minutes
| country = United Kingdom
| language = English
}} campy British horror film written and directed by David Howard, and starring Hugh OConor and Faye Dunaway. It had its theatrical release in 2008, and the DVD of the film was released in the United Kingdom on 19 October 2009. The film was shot in and around Cardiff, Pontypool, Newbridge, Caerphilly, Briton Ferry  Wales. 

==Story==
Memphis cop Lieutenant McKenzie is called in to investigate a series of strange deaths and weird sightings following the resurrection of a murder victim, a local boy, Johnny Flick Taylor (Hugh OConor) from the 1950s who is brought back to life in modern times and tries to find his teenage sweetheart named Sally who is now aged 62 and also to seek revenge for his death.

==Cast==
Faye Dunaway as Lieutenant Annie McKenzie
 
Julia Foster as Sally Andrews
 
John Woodvine as Dr. Nickel
 
Michelle Ryan as Sandra
 
Sara Harris Davies as Diane
 
Hugh OConor as Johnny
 
Hayley Angel Wardle as Young Sally 
 
Mark Benton as Sergeant Miller
  Liz Smith as Johnnys Mother
 
Rhys Parry Jones as Dockside official
 
Richard Hawley as Bobby Blade
 
Kerrie Hayes as Young Sue

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 