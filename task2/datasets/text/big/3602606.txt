Modern Inventions
 
{{Infobox Hollywood cartoon cartoon name=Modern Inventions
|series=Donald Duck
|image=Modern Inventions.png
|caption="Your hat, sir" Jack King
|producer=Walt Disney story artist=Carl Barks voice actor=Clarence Nash Billy Bletcher Cliff Edwards
|musician=Oliver Wallace Paul Allen Jonny Cannon  layout artist= background artist= Walt Disney Productions
|distributor=United Artists release date=  (USA) color process=Technicolor
|runtime=8 minutes 30 seconds
|country=United States
|language=English preceded by=Mickeys Amateurs followed by=Hawaiian Holiday
}} 1937 American animated short Walt Disney Jack King, his first project at the Disney studio, and features original music by Oliver Wallace. The voice cast includes Clarence Nash as Donald, Billy Bletcher as the Robot Butler, and Cliff Edwards as the Robot Barber. 

Modern Inventions pokes fun at modern conveniences. The scene of Donald in the barbers chair was submitted by Carl Barks as his first story contribution at Disney. 

==Plot== quarter on a line to get in (this allows him to keep his money and get in as well). Once inside, he is confronted with the "Robot Butler", a robot who takes hats ("Your hat, sir.") After Donalds hat is taken away from him, Donald uses a magic trick to produce another hat (similar to the way he produces flutes in The Band Concert). He says, "So!" and continues on his way. He first encounters a robotic hitch-hiker, which activates when he makes driving noises. However, when he laughs at it, it punches him in the face. Next he goes to the wrapping machine, which says "Do not touch" but Donald ignores the sign and hops on. When he pulls a lever, the machine proceeds to grab him in two robotic arms, put transparent wrapping paper around him, and put him in ribbons, like a package. He manages to break out by vigorously shaking, and continues exploring. 

All the time, Donald has been losing hat after hat to the Robot Butler, making Donald angrier and angrier. Eventually, the Butler chases him through the museum to an automated baby carriage, which Donald hides inside. Donald is given a babys hat to wear as is rocked as the song "Rock-a-bye Baby" is played. Donald then begins acting like a baby, sucking his feet, playing with toys offered to him and getting ticked under the chin, and having his feet counted "This Little Piggy went to market." Donald then begins whining about not getting his milk. The machine gets out a bottle of milk but it hits him in the face instead of going into his mouth, making Donald agitated. The machine begins torturing him with toys and more milk in the face, turning him over and pinning a diaper on his bottom and powdering it. 

The Robot Butler is again attracted by Donalds laughing and yanks the baby hat off his head. Donald produces one last hat and goes to the one exhibit hes not yet seen: a self-operating hair makeup chair. Using his "cheat" coin, Donald pays to get his hair done. However, instead of giving him a haircut, it flips him over, removes his hat, and given his bottom a cut, wrapping his rear end in a towel, cutting off his tail feathers, cleaning his bill, coating his face with black oil, sifting through his bottom feathers, applying a wet towel to it, slapping his blackened face with a cloth, combing his bottom with a comb, making a gap through it, smoothing it out and finally, giving him a pig tail design. The Robot Butler appears and removes Donalds last hat, which causes Donald to enter an explosive rage.          

==Releases ==
*29 May 1937 &ndash; original release (theatrical)
*1985 &ndash; "Cartoon Classics: The Continuing Adventures of Chip n Dale Featuring Donald Duck" (VHS)
*c. 1992 &ndash; Mickeys Mouse Tracks, Episode 7 (TV)
*c. 1992 &ndash; Donalds Quack Attack, Episode 30 (TV) Ink & Paint Club", Episode 26 "Classic Donald" (TV) 
*18 May 2004 &ndash; " " (DVD)

==References==
 

==External links==
* 
* 
 
 
 
 
 
 
 