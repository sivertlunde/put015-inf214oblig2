Supporting Characters
 
 
 
{{Infobox film
| name = Supporting Characters
| image =Supporting Characters movie.jpg
| alt = 
| caption = Promotional Poster Daniel Schechter
| producer = Let It Play Pipeline Entertainment (II) Renart Films
| writer = Daniel Schechter Tarik Lowe
| starring = Alex Karpovsky Tarik Lowe Melonie Diaz Arielle Kebbel Sophia Takal
| music = Jordan Galland
| cinematography = Richard P. Ulivella
| editing = Daniel Schechter
| studio = 
| distributor = 
| released =  
| runtime = 
| country = United States
| language = English
| budget = 
| gross = $4,917   
}}
 Daniel Schechter. It was written by Schechter and Tarik Lowe.
It had its world premiere at the Tribeca Film Festival on April 20, 2012. 
The film follows two New York film editors trying to balance their personal lives while reworking a film.

==Production==

===Film development===
Dan Schechter: "When I started, I just wanted to make a film, a really good film, for under $50,000, and that was the main goal (and we ended up incredibly close to that number). The editing thing was an arbitrary choice at first. This film was about two best buds and their respective relationships, and we just needed to give them some career to do together so they could bitch at work to one another. So I chose editing because I had some good experiences I could draw from, and perhaps we could get away with not being a "film about making films” because, really, that storyline is about specifically being an editor and what that day job is like." 

Schechter and Lowe based the semi-autobiographical film on incidents from their personal lives, crafting a simultaneously offbeat and naturalistic New York story. Schechters own experience as an editor is evident in the script, which blends intimate relationships with humorous details of life in an editing room.

Schechter had a number of his indie movie associates perform in this movie, such as Melonie Diaz. Lena Dunham and Alex Karpovsky, both of Girls (TV series)|Girls fame also starred.

==Reception==

===Critical response===
The film opened to positive reviews. Rotten Tomatoes gives a score of 86% based on seven reviews. 

Jason Bailey of DVD Talk had the following evaluation to make: Lowe and director Daniel Schechters script is fast and witty--the Nick/Darryl duets (which make up a good chunk of the picture) have a sharp, conversational style and tempo, as well as the sense of a long-time friendship, articulated by ball-busting and one-upsmanship--and it got an insiders knowledge of the business, what with the metaphorical act of partioning drives and labeling bins, or the escalating flirtations at an ADR session. (Between this and the hot foley recording scene in Nobody Walks, its a big year for sexy sound nerds.)" 

Vanessa Martinez, a critic with Indie Wire discusses the powerful rapport of the main characters of the film: "you will enjoy witnessing this duo’s connection, and even co-dependence to a degree, as they find a way to remain loyal to each other through lifes highs and lows aside from their very different personalities, thanks to naturalistic and engaging performances. Also, the twists and turns of the film’s plot prevent the film from becoming predictable and ordinary, much like life itself." 

Brandon Harris a reporter with Filmmaker (magazine) said:"
Daniel Schechter’s Supporting Characters surprised me. I was prepared to hate it. Excited to, even. It charmed the pants right off me. It might as well be TV, the way it’s shot (of course that means a lot less than it did ten years ago since TV is art now), and it couldn’t be more built to play Tribeca, but it’s genuinely written with feeling, humor and friendship that feels true and complicated." 
 cameo in Characters)."—Jason Guerrasio
of Fandango. 

==Soundtrack==
  
Again drawing on his friends in the Indie movie business, the soundtrack is by friend Indie director and musician Jordan Galland  In exchange for doing the soundtrack for
this movie, Schecter did the trailer for Alter Egos, a Jordan Galland movie. 

Coincidentally, Galland provided a song to Lena Dunhams movie Tiny Furniture for free.

; Track listing
 
(Available on iTunes Store) 
* The Fight
* She was my First Love, man
* Times Square
* Baxters Theme
* This is my Girlfriend
* Cant Say Why
* Cab Ride Home
* A Friend is a Friend
* New York Romance
* We have one life
* 10 years

==References==
 

 

 
 
 
 