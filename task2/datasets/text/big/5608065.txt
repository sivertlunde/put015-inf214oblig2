Rubin and Ed
 

{{Infobox Film
| name = Rubin and Ed
| image = RubinandEdVHS.jpg
| director = Trent Harris Paul Webster
| writer = Trent Harris
| starring = Crispin Glover Howard Hesseman Karen Black Michael Greene Anna Louise Daniels
| music = Fred Myrow
| editing = Brent A. Schoenfeld
| studio = Working Title Films
| distributor = IRS Media (1992) (USA) 1991
| runtime = 82 minutes
| language = English
| gross    = $15,675 
}}
 American Independent independent Comedy buddy film written and directed by Trent Harris and released in 1991 in film|1991. It is about an eccentric, unsociable young man who is forced by his mother to make some friends before shell return his stereo to him. He is joined on a trip through a desert by a pyramid scheme salesman, to assist in finding a location to bury a frozen cat.

Crispin Glover appeared on Late Night with David Letterman in 1987 dressed and in character as Rubin Farr. This caused much confusion to David Letterman as he, after almost being kicked in the face by Glover, walked off his own set while still on the air.

==Production==
  Salt Lake City, Hanksville, Utah|Hanksville, and Goblin Valley State Park.

==Cast==
{| class="wikitable" width="40%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Howard Hesseman || Ed Tuttle
|-
| Crispin Glover || Rubin Farr
|-
| Karen Black || Rula
|-
| Michael Greene || Mr. Busta
|-
| Brittney Lewis || Poster Girl
|-
| Anna Louise Daniels || Rubins Mom
|-
| Ray Gordon || Barking Man
|-
| Dorene Nielsen || Eds Mom
|-
| Frank Magner || Bob
|-
| Aaron Tranberg || Desk Sergeant
|-
| Patrick Michael Collins || Lacky
|-
| Jonathan Chapin || Brat
|-
| Jane Mendenhall || Woman By Pool
|- James Nielsen || Eds Dad
|-
| Diane St. Cyr || Bonnie
|-
| Michael Scott || Jimbo
|}

== References == 

 

==External links==
* 
* 
*  

 
 
 
 
 
 

 