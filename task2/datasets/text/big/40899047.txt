My Father and the Man in Black
My Ring of Fire.

==Production==
The film employs historically accurate flashbacks. Starting with how Holiff met Johnny Cash when he hired him to sign autographs at his "Sols Square Boy" drive-in in London, Ontario  Hollif went on to sign Cash to a number of other music gigs and Cash hired him to be his manager, with a contract written on the back of a paper napkin.

It is narrated by Jonathan Holiff, interlaced with archival audio by Johnny Cash and Saul Holiff.

The structure of the film interlocks the relationship of Cash and Saul, and often pans to the relationship of Saul with Jonathan. His son often resented his fathers time on the road. There is an emphasis of the rift between Cash and Saul, caused by Cashs status as a Born again Christian and Saul as an Atheist, and how their relationship spiraled out of control. Variety.com comments on the unique structure of this film: 
 "Documentaries as expressions of filial trauma usually fail to generate audience empathy. But with its posthumous, anguished, first-person confessional revolving around the larger-than-life Man in Black this one partly transcends its inherent self-indulgence."  

==Reception and awards==
My Father and the Man in Black has had mixed reviews from critics in the United States. The film has a "Fresh" rating (63 percent) on critic review aggregator site  .com, the movie is described as "too damn interesting to be maudlin."  A review in The Village Voice stated  "heart and feeling is soaked through it like the sweat in Cashs guitar strap." 

The film has been nominated and has won a number of awards on  the film festival circuit.  
* Lewiston Auburn Film Festival: Best Documentary  2012 
* Buffalo Niagara Film Festival : Best Documentary 2013 
* Tiburon International Film Fedtival: Orson Welles award {{cite web|url=http://johnny-and-saul.com/uploads/websites/211/wysiwyg/MAN_IN_BLACK_-_Press_Notes.pdf|format=PDF|title=MY FATHER AND
THE MAN IN BLACK|publisher=Johnny-and-saul.com|accessdate=25 December 2014}} 
* Edinburgh documentary Film Festival: Best feature 

==Reviews==
The film garnered mostly positive reviews in other countries, particularly in the United Kingdom. The Financial Times  called it "a non-fiction Walk the Line with script input by Eugene O’Neill."  The Guardian said "finally, a fresh angle on the Cash mythology."   A reviewer with The Daily Telegraph commented on the frequent dramatic confrontations between Cash and Holiff revealed in authentic audio of phone exchanges between the two.  A reviewer from the UK website "Film Forward" states that the movie is a type of answer song to the movie "Walk the Line  and it covers a number of themes not mentioned in Walk the Line, like Cashs conversion to Christian fundamentalism at the peak of his career, the racism Cash faced by the KKK when they believed Cashs first wife was African American, also the Antisemitism Holiff faced both growing up and in the early days of country music.

==Historical accuracy==
The documentary is considered among the most historically accurate films ever made about Johnny Cashs career in the 1960s and early 1970s — and the often stormy relationship with his manager between 1958 and 1977. This is due to the fact that the film is driven by contemporaneous audio diaries and telephone calls, and hundreds of letters, between the two men. A number of the movie props are genuine articles given to Holiff by Cash. 

==Soundtrack==
* Lee Harvey Osmond
* Michael Timmins of The Cowboy Junkies

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 