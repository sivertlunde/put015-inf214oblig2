Brahma Puthrudu
{{Infobox film
| name           = Brahma Putrudu
| image          =
| caption        =
| director       = Dasari Narayana Rao
| producer       = D. Ramanaidu
| writer         = V.C. Guhanathan  (narrative|story)  Dasari Narayana Rao  (dialogues) 
| screenplay     = Dasari Narayana Rao Venkatesh  Rajani Mohan Babu Jayasudha
| music          = Chakravarthy
| cinematography = P.S. Prakash
| editing        = K.A. Martand
| studio         = Suresh Productions
| distributor    =
| released       =  
| runtime        = 144 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Tollywood film Venkatesh and Rajani played the lead roles and music composed by K. Chakravarthy|Chakravarthy.  The film was remake of Tamil film  Michael Raj. The film was Super Hit at the box office.  Venkatesh won Filmfare Award for Best Actor - Telugu.

==Plot==
Sreedevi ( ) and Shakthi both love each other. Will Shakthi succeed in finding Sreedevi’s father? Will he be able to meet his mother?

==Cast==
  Venkatesh as Shakthi
* Rajani as Ammadu
* Mohan Babu as Ranjeeth
* Jayasudha as Kiranmayi
* Nutan Prasad as Guru Murthy
* Srividya as Janaki Baby Shalini as Sridevi  Allu Ramalingaiyah as Tatarao
* Suthivelu as S.I.Simham
* Nagesh as Constable Engeenayalu
* Mada as Tailor
* Ramana Reddy as Shakthis assistant
* Potti Veeraiah as Shakthis assistant
* Gundu Hanumantha Rao as Constable
* Jaya Prakash Reddy as S.P.
* Prabha as Rekha
* Radhakumari as Rekhas Mother
* Jayamalini as Constable Geeta
* Y.Vijaya as Sundari
 

==Soundtrack==
{{Infobox album
| Name        = Brahma Puthrudu
| Tagline     =
| Type        = film Chakravarthy
| Cover       =
| Released    = 1988
| Recorded    =
| Genre       = Soundtrack
| Length      = 18:51
| Label       = Annapurna Music Chakravarthy
| Reviews     =
| Last album  = Raktha Tilakam   (1988)
| This album  = Brahma Puthrudu   (1988)
| Next album  = Manchi Donga   (1988)
}}
Music composed by K. Chakravarthy|Chakravarthy. Lyrics written by Dasari Narayana Rao. Music released on ANNAPURNA Music Company.
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|"Nee Yabba"
|S. P. Balasubrahmanyam|S. P. Balu, S. Janaki
|3:36
|- 2
|"Ayyababoi"
|S. P. Balu,S. Janaki
|3:30
|- 3
|"Sannajaji Chettu Kindha"
|S. P. Balu, P. Susheela
|3:58
|- 4
|"Ammayi Mukku"
|S. P. Balu, S. P. Sailaja
|3:57
|- 5
|"Amma Thodu"
|S. P. Balu
|3:50
|}

== References ==
 

==External links==
*  

 
 
 
 
 
 
 

 