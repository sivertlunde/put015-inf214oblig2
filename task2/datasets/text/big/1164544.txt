Identity (film)
{{Infobox film name = Identity image = Identity poster.jpg caption = Theatrical release poster director = James Mangold producer = Cathy Konrad writer = Michael Cooney starring = John Cusack Ray Liotta Amanda Peet Alfred Molina Clea DuVall Rebecca De Mornay music = Alan Silvestri cinematography = Phedon Papamichael Jr. editing = David Brenner studio = Konrad Pictures distributor = Columbia Pictures released =   runtime = 90 minutes country = United States language = English  budget = $28 million    gross = $90,259,536 
}}
Identity is a 2003 American psychological thriller film directed by James Mangold from a screenplay written by Michael Cooney. The film stars John Cusack, Ray Liotta, Amanda Peet, Alfred Molina, Clea DuVall and Rebecca De Mornay.
 1987 and Ten Little Indians (1989 film)|1989, the plot draws from the structure the novel first popularized in which 10 strangers arrive at an isolated location which becomes temporarily cut off from the rest of the world, and are mysteriously killed off one by one. The first several scenes also use a reverse chronology structure.

==Plot==
A convict introduced as "Malcolm Rivers"—who was abandoned as a child at a motel by his prostitute mother—awaits execution for several vicious murders that took place at an apartment building. Malcolms psychiatrist, Dr. Mallick, has discovered his journal that may explain why he committed the murders. With this late evidence brought forth, a new hearing takes place.
 torrential rainstorm at a remote Nevada motel, run by Larry Washington. The group consists of an ex-cop, now limousine driver, Ed Dakota; Caroline Suzanne, an actress popular in the 1980s; Officer Rhodes, who is transporting serial killer Robert Maine; Paris Nevada, a prostitute; newlyweds Lou and Ginny Isiana; and the York family, George and Alice, and mute 9-year-old son Timmy. The Yorks are in crisis because Alice has been struck by Eds car. With both ends of the road completely flooded, the group prepares to spend the night. However, they quickly find there is an unknown murderer present, killing off each of the guests. Caroline is the first to be killed. Ed, finding her severed head in a clothes dryer, thinks Maine killed her. When they check the convict, they discover he has escaped.

All the others become worried, and Ginny flees in terror to her room. Her husband Lou chases after her but is also murdered.  Maine runs through the hills, only to be dumbfounded when he finds himself back at the motel.  He enters the diner, where Ed and Rhodes jump and beat him into unconsciousness, putting Larry on guard duty. However, Maine is later found dead. Paris discovers a dead body in Larrys freezer, which is revealed to be the real hotel manager. Larry attempts to escape in his truck, claiming he did not kill anybody; he accidentally runs over George, killing him. 

Each body is accompanied by a numbered room key, the order of which suggests a countdown. The survivors tie Larry up, and as he tells them his story the others start to believe he really did not kill anyone. Subsequently, Alice is discovered to have died from her injuries. Ginny and Timmy die when their car blows up, but their bodies are nowhere to be found. The remaining four discover that all the bodies have disappeared and that all ten share the same birthday; Ed realizes that all ten names are linked to US states (Caroline being the Carolinas, Lou Isiana being Louisiana, etc.). Paris discovers that Rhodes is actually a convict as well; he killed the corrections officer transporting him and Maine cross state and assumed the cops identity. Rhodes attempts to kill Paris, but she is saved by Larry, who hits Rhodes with a fire extinguisher, only to be shot and killed by him.

Back at the hearing, the contents of Malcolms journal are revealed, indicating Malcolm suffers from an extreme case of dissociative identity disorder, harboring ten distinct personalities. Mallick is able to bring forth one of Malcolms personalities: Ed - all of the events happening at the motel are concurrently occurring inside Malcolms mind, and each one of Malcolms personalities is represented by one person at the motel. Mallick explains to "Ed" that the events at the motel are a result of treatment Malcolm is receiving: the killings at the motel are Malcolms mental attempts to eliminate his nine excess personalities. Mallick further gives "Ed" the mission of making sure that the hostile personality (i.e., the one responsible for Malcolms committing the crimes for which he is being tried) is eliminated to prevent Malcolm from being executed.

Back in the motel setting, Ed believes Rhodes is the murderer, and the two shoot each other to death, leaving only Paris alive. When Mallick demonstrates that the homicidal personality is dead, the Judge decides to place Malcolm in a mental institution under Mallicks care. 
 orange grove, she discovers the room 1 motel key, and finds Timmy behind her. Timmy, the true homicidal personality, had orchestrated all the deaths at the motel, and made it appear that he had been killed as well; he finishes his task by killing Paris. Now driven only by Timmy, Malcolm strangles Mallick and then attacks the orderlies and the van driver, forcing the van off the side of the road.

==Cast==
*John Cusack as Edward "Ed" Dakota, a limousine driver and a former Los Angeles police officer.
*Ray Liotta as Samuel Rhodes, a convict posing as a police officer.
*Amanda Peet as Paris Nevada, a Las Vegas prostitute.
*Clea DuVall as Ginny Isiana, a superstitious newlywed.
*William Lee Scott as Lou Isiana, Ginnys husband.
*Rebecca De Mornay as Caroline Suzanne, an 80s Hollywood TV actress chauffeured by Ed. John Hawkes as Larry Washington, the motel owner.
*Leila Kenzle as Alice York, a wife and mother who is injured in a car accident.
*John C. McGinley as George York, Alices husband and Timmys stepfather.
*Bret Loehr as Timothy "Timmy" York, Alices son and Georges stepson.
*Jake Busey as Robert Maine, Rhodes partner-in-crime.
*Pruitt Taylor Vince as Vincent Taylor Malcolm Rivers (listed as "Malcolm Rivers" in films credits), a convicted serial killer.
*Alfred Molina as Dr. Mallick, a psychologist.
*Carmen Argenziano as Marty, the defense lawyer.
*Marshall Bell as Gary, the district attorney.
*Matt Letscher as Greg, the assistant district attorney.
*Holmes Osborne as Judge Taylor
*Frederick Coffin as Detective Varole
*Stuart M. Besser as Larry, the motel owner.

==Production== Los Angeles County, while the majority was shot on a sound stage at Sony Pictures Studios in Culver City.

Angelo Badalamenti was originally signed to score the film, but his music was replaced with a new score by Alan Silvestri (Silvestri had previously replaced Badalamenti on 1991s Shattered (1991 film)|Shattered).

==Reception==
===Critical response===
Identity has a rating of 62% on Rotten Tomatoes out of 169 reviews.  It scored 64 out of 100 on Metacritic, indicating a "generally favorable reviews". 

Film critic Roger Ebert wrote upon the films release, "Altogether, there are 10 guests. One by one, they die. Agatha Christie fans will assume that one of them is the murderer—or maybe its the clerk... I think it is possible that some audience members, employing   Law of Economy of Characters, might be able to arrive at the solution slightly before the movie does." 

Mick LaSalle of SFGate reported, "At first, Identity seems like nothing more than a pleasing and blatant homage (i.e. rip-off) to the Agatha Christie-style thriller where marooned guests realize that a murderer is in their midst ... weve seen it before. Yet make no mistake. Identity is more than an entertaining thriller. Its a highly original one." 
 Donald Kaufman pause. But theres nothing self-parodic about Identity—the viewer must not only swallow the nullifying third-act bombshell but actually re-engage with the movie on its new, extremely dubious terms." 

Brian Mckay of eFilmCritic.com wrote, "This films cardinal sin was not that it had an engrossing but extremely far-fetched setup to a lackluster resolution—-a resolution that probably sounded good during the initial script pitch, but which nobody realized was going to be such a misfire until the production was already at the point of no return. No, what Identity is guilty of most is bad timing - it simply gives away too much, too soon. At about the halfway mark (if not much sooner), the films big "twist" will finally dawn on you (and if it doesnt, theyll end up coming right out and saying it five minutes later anyway). And once it does, you will no longer care what happens afterward." 

===Box office performance===
Identity opened on April 25, 2003 in the United States and Canada in 2,733 theaters.  The film ranked at #1 on its opening weekend, accumulating $16,225,263, with a per theater average of $5,936.    The films five-day gross was $18,677,884.   

The film dropped down to #3 on its second weekend, behind newly released X2 (film)|X2 and The Lizzie McGuire Movie, accumulating $9,423,662 in a 41.9% drop from its first weekend, and per theater average of $3,448.    By its third weekend it dropped down to #4 and made $6,477,585, $2,474 per theater average.   

Identity went on to gross $52.1 million in the United States and Canada and $38.1 million overseas. In total, the film has grossed over $90 million worldwide, making it a moderate box office success. 

===Accolades===
The film was nominated for  , respectively.

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 