Karthik Calling Karthik
 
{{Infobox film
| name           = Karthik Calling Karthik
| image          = Karthik Calling Karthik pic.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Vijay Lalwani
| producer       = Farhan Akhtar Ritesh Sidhwani
| screenplay     = Chandrasekaran Vijay
| story          = 
| starring       = Farhan Akhtar Deepika Padukone Ram Kapoor Shefali Shah Vipin Sharma Elangovan Karthikeyan Mohamed Ashiq
| music          = Shankar-Ehsaan-Loy
| cinematography = Sanu Varghese
| editing        = Vignesh Muniyandi
| studio         = Excel Entertainment
| distributor    = Reliance Big Pictures
| released       =  
| runtime        = 135 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} The New Twilight Zone.

==Plot==
Karthik (Farhan Akhtar) is an introvert who lacks confidence and feels trapped in his average job at a construction company. He is continuously troubled by an incident from his childhood: His older brother, Kumar, used to torture him, but whenever he complained to his parents, they did not believe him. One day, Kumar took Karthik to a well and tried to throw him in it, but Karthik escaped. Kumar accidentally fell inside the well and died. Karthik has thought himself responsible for his brothers death ever since.

Shonali Mukherjee (Deepika Padukone) is a co-worker at Karthiks company, whom Karthik  secretly loved  though she remains unaware of his existence, much less his feelings. After being derided by his boss Mr Kamath (Ram Kapoor) yet again; Karthik figures his life can’t get any better and decides to commit suicide. Just as he is about to, a stranger with the same, exact voice as his, calls and says that he is also Karthik, convincing him that he has the ability to change his life. These phone calls become Karthiks life guide. His chats take place every morning at 5:00&nbsp;a.m. and the caller provides advice on Karthiks problems, guiding him to become a successful man, win Shonalis heart, and bring color to his otherwise dreary life.

However, when Karthik tells Shonali and his psychiatrist about the phone calls, despite being warned not to, the mysterious caller gets angry and tells Karthik that if he could bring him up, he could also throw him down. As per his word, things start going downhill. Karthiks boss throws him out and Shonali leaves him. Karthik decides that if he goes somewhere he does not know, then the caller wouldnt know where he is either and stop calling him. Karthik travels to an unknown place, takes shelter in a small hotel, and asks the receptionist to remove the telephone and room number plate.

After a few months, Karthik is comfortably living in  . He has an alter-ego that is more assertive and advises him on how to live life. All this time, the strange caller was Karthik himself. He has been dealing with this condition from a young age, when he created a fake brother named Kumar. Karthiks phone has the capacity to record messages and act as a playback feature at a certain time. Karthik would wake up in the middle of the night, leave himself messages as his alter ego, and return to sleep, where he would awake once again at 5am to take his own calls.

Eventually, Karthik Narayan becomes so disturbed that he attempts to commit suicide again. Shonali, realizing the truth, arrives at the right time to save him. They reconcile and she stands by him, helping him with his condition. After a few months, Karthik is in the process of dealing with his disorder and lives a happy and rehabilitated life with Shonali by his side.

==Cast==

* Farhan Akhtar as Karthik Narayan
* Deepika Padukone as Shonali Mukherjee
* Ram Kapoor as Mr. Kamath
* Shefali Chhaya as Mrs. Kapadia
* Vivan Bhatena as Ashish
* Vipin Sharma as Karthiks Landlord
* Yatin Karyekar
* Siddhartha Gupta as Young Karthik
* Swapnel Desai as Young Kumar
* Kanchan Pagare 	as Peon at ACL Office
* Haansaa as Secretary at ACL Office
* Abhay Joshi as Phone salesman Mumbai
* Brijendra Kala as Telephone exchange clerk

==Production==
In an effort to prepare for his role as loner introvert Karthik, Farhan Akhtar isolated himself at home and school and turned off his phone.  He also learned to solve the Rubiks Cube, an activity which his character completes in only one try. 

==Release==

===Critical reception===
On the review-aggregation website ReviewGang, the film scored 5.5/10 based on 11 reviews.  Film critic Rajeev Masand gave the film 2 out of 5 possible stars. Nikhat Kazmi of Times of India rated it 3.5 out of 5, calling it "immensely watchable, purely for the class act by Farhan Akhtar in the title  role".  Shweta Parande of IBNLive.com gave a rating of 3.5 out of 5, applauding the cast and crew for their performances and their efforts on building up the story as a team.  IndiaGlitz gave a 3 out of 5, concluding "Go but with an open mind." 

===Box Office===
Karthik Calling Karthik was a declared a below average, netting   at the end of its run.  The movie grossed   and collected a distributor share of  . {{cite web|title=
Karthik Calling Karthik Below Average Teen Patti Dull|url=http://www.boxofficeindia.com/boxdetail.php?page=shownews&articleid=1554&nCat=box_office_report}} 

==Soundtrack==
{{Infobox album  
| Name        = Karthik Calling Karthik
| Type        = Soundtrack
| Artist      = Shankar-Ehsaan-Loy
| Cover       = KCKalbumcover.jpg
| Released    = 26 February 2010
| Recorded    = Feature film soundtrack
| Length      =
| Label       = T-Series
| Producer    = Farhan Akhtar, Ritesh Sidhwani
| Last album  = My Name Is Khan   (2010)
| This album  = Karthik Calling Karthik   (2010)
| Next album  = Hum Tum Aur Ghost   (2010)
}}
{{Album reviews
| rev1 = Planet Bollywood
| rev1Score =    
| rev2 = Glamsham
| rev2Score =    
| rev3 = Starboxoffice
| rev3Score =    
| rev4 = Yahoo! Movies
| rev4Score =    
| rev5 = Music Aloud
| rev5Score =    
}}
The soundtrack of the film is composed by Shankar-Ehsaan-Loy with lyrics penned by Javed Akhtar. The album was released on 20 February 2010. 
 Teen Patti. 

Before the album got released, an online copy was leaked onto the Internet. Farhan Akhtar confirmed this and requested not to download them illegally. 

===Track listing===
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track # !! Song !! Singer(s)!! Duration
|-
| 1
| "Hey Ya!"
| Clinton Cerejo, Shankar Mahadevan, Loy Mendonsa
| 4:17
|-
| 2
|"Uff Teri Adaa"
| Shankar Mahadevan & Alyssa Mendonsa
| 5:06
|-
| 3
| "Jaane Ye Kya Hua" KK
| 4:10
|-
| 4
| "Kaisi Hai Yeh Udaasi"
| Kailash Kher & Sukanya Purayastha
| 6:07
|-
| 5
| "Karthik Calling Karthik"
| Suraj Jagan, Shankar Mahadevan, Caralisa Monteiro, Malika Singh
| 3:11
|-
| 6
| "Karthik 2.0"
| MIDIval Punditz, Karsh Kale
| 4:15
|-
| 7
| "Karthik Calling Karthik (Theme Remix)"
| Suraj Jagan, Shankar Mahadevan, Caralisa Monteiro, Malika Singh (Remixed by MIDIval Punditz & Karsh Kale)
| 3:11
|-
| 8
|"Hey Ya! (Remix)"
| Clinton Cerejo, Shankar Mahadevan, Loy Mendonsa (Remixed by Digital Boyz)
| 5:17
|-
| 9
|"Uff Teri Adaa (Remix)"
| Alyssa Mendonsa, Shankar Mahadevan (Remixed by Udyan Sagar of Nucleya)
| 4:06
|}

==Awards and nominations== 2011 Zee Cine Awards
Nominated  Best Music - Shankar Ehsaan Loy
 2011 IIFA Awards
Nominated  Best Male Playback Singer - Shankar Mahadevan
 2011 Star Screen Awards
Nominated  Best Music - Shankar Ehsaan Loy

==References==
 

==External links==
*  
*  

 

 
 
 
 