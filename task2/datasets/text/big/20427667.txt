Berlin, Appointment for the Spies
{{Infobox film
| name           = Berlino - Appuntamento per le spie
| image          = Berlino - Appuntamento per le spie.jpg
| image_size     = AIP film poster by Reynold Brown
| director       = Vittorio Sala
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =Riz Ortolani
| cinematography =
| editor       =
| distributor    =
| released       =  1965
| runtime        = 85 minutes
| country        = Italy Italian
| budget         =
}} Italian Eurospy film directed by Vittorio Sala and starring Dana Andrews. The film is also known as Bang Youre Dead.

It was retitled Spy in Your Eye for American International Pictures American release where it was double billed with Secret Agent Fireball.

==Cast==
*Brett Halsey as Bert Morris
*Pier Angeli as Paula Krauss
*Gastone Moschin as Boris
*Tania Béryl  as  Madeleine
*Dana Andrews  as  Col. Lancaster George Wang  as   Ming
*Alessandro Sperli  as   Karalis
*Marco Guglielmi as   Kurt
*Renato Baldini  as   Belkeir
*Mario Valdemarin  as  Willie
*Luciana Angiolillo as  Miss Hopkins
*Luciano Pigozzi  as  Leonida
*Tino Bianchi  as Dr. Van Dongen
*Massimo Righi  as Lavies
*Franco Beltramme as  Serghey

==External links==
*  

 
 
 
 
 
 
 
 
 

 
 