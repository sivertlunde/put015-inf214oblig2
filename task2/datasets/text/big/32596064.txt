Moses (film)
{{Infobox film
| name           = Moses
| image          =
| caption        = Roger Young
| producer       = Antena 3 Televisión,  Beta Film
| writer         = Lionel Chetwynd
| starring       = Ben Kingsley Frank Langella
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 20 December 1995 (Italy)  7 April 1996 (US)
| runtime        = 180 minutes
| country        = US, Czech, UK, France, Germany, Italy, Spain
| awards         =
| language       = English
| budget         =
}}
 drama TV Roger Young, written by Lionel Chetwynd and starred Ben Kingsley and Frank Langella.  It was filmed in Morocco. The film aired in the United States on the TNT Network

== Cast ==
* Ben Kingsley – Moses
* Frank Langella – Merneptah
* Christopher Lee – Ramesses II
* Anton Lesser – Eliav Jethro
* Anna Galiena – Ptira
* David Suchet – Aaron
* Geraldine McEwan – Miriam Anthony Higgins - Korach
* Enrico Lo Verso – Joshua
* Maurice Roëves - Zerack
* Sonia Braga – Sephora (uncredited)
* Vincent Riotta - Midan

== See also ==
* Moses the Lawgiver

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 

 

 