Climates (film)
{{Infobox Film  name        = Climates  image       = Climates.jpg caption     = Poster director    = Nuri Bilge Ceylan writer      = Nuri Bilge Ceylan starring    = Ebru Ceylan Nuri Bilge Ceylan Nazan Kırılmış producer    =  distributor = Pyramide released    = 2006 country     = Turkey language    = Turkish
}}
Climates ( ) is a 2006 Turkish drama film directed by Nuri Bilge Ceylan. The film charts the deteriorating relationship between a professional Istanbul couple, İsa and Bahar, played by Ceylan and his wife Ebru Ceylan. It was Ceylans first film shot on High-definition video.

==Plot==
The film starts on a summer holiday in Kaş, where the couple are barely talking. İsa is taking pictures of ancient monuments for a perpetually unfinished thesis for the university class he teaches; Bahar watches. At the beach she falls asleep and dreams that he is smothering her in sand. After rehearsing his speech while Bahar is swimming, Isa tells her that he wants to break up. While riding back to the city on a motorbike she suddenly covers his eyes with her hands, which causes the bike to crash, although both of them avoid serious injury. The couple go their separate ways and Bahar tells him not to call.

As fall follows summer, back in Istanbul, İsa makes contact again with a woman, Serap, with whom he cheated on Bahar before and who is in a relationship with an acquaintance of his. Winter arrives and İsa dreams of a holiday in the sun, but instead flies to Ağrı Province|Ağrı, the snowy eastern province of Turkey, where Bahar is working as an art director filming a TV series on location. He tries to win her back, but she rejects his advances. Later, she comes to his hotel room and stays overnight, but in the morning he flies off alone.

==Cast==
* Ebru Ceylan as Bahar
* Nuri Bilge Ceylan as İsa
* Nazan Kırılmış as Serap
* Mehmet Eryılmaz as Mehmet
* Arif Aşçı as Arif
* Can Özbatur as Güven

==Technique==
The film features long takes of head shots, and poetic landscapes. The dialogue has long silences during which casual sounds are highlighted, such as the sound of a woman drawing on a cigarette.

==Reception== Michael Phillips chose the film as the best film of the year and as the third best film of the decade 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 