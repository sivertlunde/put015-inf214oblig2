Whom the Gods Destroy
 
{{Infobox film
| name           = Whom the Gods Destroy
| image          = 
| caption        = 
| director       = Walter Lang
| producer       = 
| writer         = Albert Payson Terhune Sidney Buchman
| starring       = Walter Connolly
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
}}

Whom the Gods Destroy is a 1934 American drama film directed by Walter Lang.   

==Cast==
* Walter Connolly as John Forrester aka Eric Jann aka Peter Korotoff Robert Young as Jack Forrester
* Doris Kenyon as Margaret Forrester
* Macon Jones as Jack Forrester - Age 14
* Scotty Beckett as Jack Forrester, Age 4 (as Scott Beckett)
* Rollo Lloyd as Henry Braverman
* Maidel Turner as Henrietta Crosland
* Henry Kolker as Carlo - the Puppeteer
* George Humbert as Niccoli
* Hugh Huntley as Jamison - Ships Officer
* Hobart Bosworth as Alec Klein
* Gilbert Emery as Professor Weaver
* Akim Tamiroff as Peter Korotoff

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 