Kill Me Again
{{Infobox film
| name           = Kill Me Again
| image          = KillMeAgain.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = John Dahl 
| producer       = Steve Golin Sigurjón Sighvatsson
| writer         = John Dahl Rick Dahl
| starring       = Val Kilmer Joanne Whalley Michael Madsen
| music          = William Olvis
| cinematography = Jacques Steyn
| editing        = Eric L. Beason Frank E. Jimenez Jonathan P. Shaw PolyGram Movies Propaganda Films
| distributor    = Metro-Goldwyn-Mayer 
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $4 million 
| gross          = $283,694 
}}
Kill Me Again is a 1989 American neo-noir thriller film directed by John Dahl, and starring Val Kilmer, Joanne Whalley and Michael Madsen.

== Plot == abusive boyfriend Vince (Madsen). She hires Jack Andrews (Kilmer), a second class private investigator, to arrange her "death". She wants to restart her life with a new identity and the money she got from helping Vince commit robberies. Due to Jacks financial problems, he joins Fay after her fake death. Unfortunately, Vince finds out that Fay is still alive. The hunt for Jack, Fay and the money begins.

== Cast ==
* Val Kilmer as Jack Andrews
* Joanne Whalley as Fay Forrester
* Michael Madsen as Vince Miller
* Jon Gries as Alan Swayzie
* Pat Mulligan as Sammy

== Reception ==
=== Critical response ===
The film drew a mixed reception. Variety (magazine)|Variety gave it a mostly positive review, stating: "The tale of a down-and-out detective and a seamy femme fatale is a thoroughly professional little entertainment.    Time Out gave it a mostly negative review, complaining: "Derived from assorted Hitchcocks and noir classics, the tortuous storyline of writer-director Dahls determinedly sordid thriller has its moments," but was critical of the three lead actors and concludes: "Setting its study of betrayal and deceit in and around the gambling towns of the Nevada desert, the film sporadically achieves a truly seedy atmosphere, but there are too many symbols, too many loose ends, and too many vaguely sensationalist scenes.   

=== Box office ===
The film was a failure at the box office,    but it later achieved some success on home video. 

== See also ==
* List of American films of 1989

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 

 