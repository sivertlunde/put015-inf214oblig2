Kangaroo (2007 film)
 
{{Infobox film
| name           = Kangaroo
| image          = Kangaroo (2008 film).jpg
| alt            = 
| caption        = 
| director       = Raj Babu
| producer       = Cicily Biju Kaipparedan
| writer         = Anil Raj   J. Pallassery
| starring       = Prithviraj Sukumaran  Kavya Madhavan  Jayasurya  Jagathi Sreekumar
| music          = Alex Paul   Saji Ram
| cinematography = Saloo George
| editing        =  V. Sajan  
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Kangaroo is a 2007 Malayalam film made in Cinema of India|India, by Raj Babu starring Prithviraj Sukumaran and Kavya Madhavan. This film was well noted for having a lot of touch of Knanaya background.

==Plot==
Josekutty (Prithviraj Sukumaran) is a hard-working auto rickshaw driver from a good family. Josekutty falls in love with Jansi, whose sister Nancy is an unwed mother of a boy. Josekutty is asked to see Nancy for an arranged marriage, and he mistakenly thinks that Jansi is Nancy, and he agrees to the marriage. Josekutty agrees to marry her. Nancy is found dead on the day before the marriage. Josekutty raises her child as his son. He tries to find out who fathered Nancys son. He learns that Monachan (Jayasurya), who was the cousin of Nancy, was in love with her, and Monachan had killed her boyfriend, who was the father of her child. He also murdered Nancy by mistake. Josekutty and Jansi make up, and decide to take care of the orphaned child as their own.

==Cast== Prithviraj  ... Joseph Abraham alias Josekutty  Kavya ...  Jancy 
*Jayasurya ...  Monichan 
*Lalu Alex ...  Stephen 
*Harisree Asokan ...  Pappikunju 
*Jagathi Sreekumar ...  Mathew Abraham alias Matthukutty
*Salim Kumar ...  Current Kunjachan  Kavery ...  Nancy 
*Suraj Venjaramood ...  Babychan 
*Indrans ...  Chellappan 
*Bindu Panicker ...  Annakutty 
*Kalaranjini ...  Cicily 
*Sukumari ...  Josekuttys Mother 
*K. P. A. C. Lalitha ...  Jansis Mother 
*Sreejith Ravi ...  Syringe Vasu 
*Santhakumari ...  Janamma 
*T. P. Madhavan ...  Paul K. Mani

==External links==
*  
 
 
 
 

 
 