McFarland, USA
{{Infobox film
| name = McFarland, USA
| image = McFarland, USA poster.jpg
| border = yes
| caption = Theatrical release poster
| alt = 
| director = Niki Caro
| producer =  
| screenplay = Christopher Cleveland Bettina Gilois Grant Thompson
| story = Christopher Cleveland Bettina Gilois
| starring =   
| music = Antônio Pinto (composer)|Antônio Pinto
| cinematography = Adam Arkapaw
| editing = David Coulson Mayhem Pictures Walt Disney Studios Motion Pictures
| released =  
| runtime = 129 minutes 
| country = United States
| language = English
| budget = $17 million 
| gross = $43.4 million 
}}
 sports drama cross country McFarland High Jim White, the schools coach, who leads the team to win a state championship.    The film was released on February 20, 2015, received positive reviews from critics, and grossed over $43 million.

==Plot==
 
 Jim White is chastising his players for an underwhelming half. Jim gets into a verbal altercation with one of the players, resulting at Jim throwing a cleat at him. Following this incident, Jim is forced to move with his wife, Cheryl, and two daughters, Julie and Jamie. They settle down in McFarland, California, a predominantly Hispanic neighborhood. On their first night there, the family goes to a small restaurant and leaves to find a large gathering of Latinos in cars surrounding the place. One man makes suggestive motions toward Julie, prompting the family to leave faster.

White starts his new job as the life science/PE teacher and assistant football coach at school. During Jims PE class, three boys, the Diaz brothers Danny, Damacio, and David, are forced by their mother to leave. During the schools football game,one of the players, Johnny Sameniego, gets injured on the field. The head coach wants him to get back out there, but White orders Johnny to sit out, which angers the head coach.

The coach complains to Principal Camillo about Jims actions during the game and demands that he step down. During the next days PE class, Jim sits on the bleachers and watches the boys run around the track with Jamie joining him. She comments that the boys, particularly Victor and Johnny (who got cut from the football team) move very fast. Jim goes to Camillo to convince him to let him start a cross country team to compete in the upcoming state championships. Jim also enlists Johnnys help to recruit six more runners for the team, starting with Victor. Johnny asks the Diaz brothers, as well as two other students - Jose Cardenas and Thomas Valles. Thomas has gotten in trouble at school for being involved in altercations with other boys that mocked his younger sister for getting pregnant, so Jim negotiates for him to join the team to avoid suspension. Although the boys are initially reluctant, they agree to join the team and run with Jim across the town, with Danny lagging behind as he is the fattest of the team.

After some training, the team goes to their first meet in Palo Alto as boys from the other schools taunt the McFarland team. The race commences, and the McFarland team lands in the final spot. Knowing the boys are disappointed, Jim takes full responsibility for the loss to not let them get discouraged.

Having been late for his daughters birthday, Jim goes for a drive and sees Thomas sitting on a bridge over the road. He approaches him and notices a bruise on his eye, which Thomas says was his own fault because he tried to stop his father from hurting his hands so that he could keep working. Jim talks him down and tells him that he has good things ahead of him, encouraging him to continue doing something worthwhile.

The Diaz brothers are pulled out of the team by their father, who wants them to keep working the fields. Jim goes to the brothers home, and the next morning goes out to work on the fields with them. The team goes up for another race to qualify for the championships. The team competes, and manage to just break into the top four teams, enough to qualify. Meanwhile, a coach from Palo Alto gives Jim a card, saying he is interested in an open full-time position.

The people in town learn that Julie just turned 15 and decide to help organize a quinceañera for her. They bring Cheryl, Julie, and Jamie to the salon while everyone sets up at home.  Thomas gives her a bracelet that his grandmother made. Later, Javi and Lupe offer to take Julie out for her own "parade", with Thomas accompanying them. Not long after, their car is confronted by a group of punks that bother Javi. The authorities contact Jim, forcing him and Cheryl to drive to the scene and find Julie on the ground, with scrapes on her legs and shaking tearfully. Jim is angry over this, despite being assured that Julie was protected.

Jim meets with the Palo Alto district for the coaching position. The boys and his family learn about this, and are upset with him. He tells Cheryl that he could bring them the better life that he promised them. Cheryl argues that they have settled in just fine here and it would be wrong for Jim to abandon the team and the friends they have made. The day arrives for the state championships. The whole community goes out to support the team. The race commences, shortly after Jim notices that Jose is moving too fast and worries that he will overexert himself. Thomas runs faster to pick up the slack as Jose starts to slow down. Thomas ends up crossing the finish line first, while the others come in closely. Jim worries when he doesnt see Jose, but to both his and Thomass surprise, it is Danny that runs faster and picks up the slack.

The judges tally their scores together and McFarland comes in first, and the team and town celebrates while Jim turns down the job offer from Palo Alto to remain in McFarland.

==Cast== Jim White 
*Maria Bello as Cheryl White 
*Morgan Saylor as Julie White 
*Carlos Pratts as Thomas Valles 
*Johnny Ortiz as Jose Cardenas 
*Hector Duran as Johnny Sameniego 
*Sergio Avelar as Victor Puentes 
*Michael Aguero as Damacio Diaz 
*Rafael Martinez as David Diaz 
*Ramiro Rodriguez as Danny Diaz 
*Diana-Maria Riva as Señora Diaz
*Vanessa Martinez as Maria Marsol 
*Martha Higareda as Lupe   
*Valente Rodriguez as Principal Camillo  Chris Ellis as Coach Jenks 
*Eloy Casados as Dale Padilla

==Production==
William Broyles Jr. was hired to write the screenplay for the film, which was in development since 2004.  Negotiations for Kevin Costner to star were finalized in July 2013.  Principal photography took place in Camarillo, California. 

==Release==
The film was previously slated for a November 21, 2014 release, under the title McFarland,  but was pushed back to February 20, 2015 and given a new title.   The film was released in Canada under its original title, McFarland.

==Reception==
=== Box office ===
As of April 19, 2015, McFarland, USA has grossed $42.9 million, against a budget of $17 million. 

The film opened in North America on February 20, 2015 and earned $11 million in its opening weekend, finishing 4th at the box office.   

===Critical reception=== rating average of 6.8/10. The sites critical consensus reads, "Disneys inspirational sports drama formula might be old hat, but McFarland, USA proves it still works — especially with a talented director and eminently likable star in the mix."  On Metacritic, which assigns a normalized rating, the film has a score of 60 out of 100, based on 32 critics, indicating "mixed or average reviews".  In CinemaScore polls conducted during the opening weekend, cinema audiences gave the film an average grade of A on an A+ to F scale. 

Stephen Farber of The Hollywood Reporter gave a positive review, writing that "While the beats of the story are often stock, the picture benefits from sensitive direction by New Zealander Niki Caro and from a most appealing performance by Kevin Costner."  James Rocchi of The Wrap wrote "A feel-good movie that earns all those good feelings, McFarland USA might be running on a predetermined track, but the heart it shows along the journey is what makes it a winner."  A.O. Scott of The New York Times described the film favorably as "a slick and safe Disney version of a fascinating and complicated reality", and that "Mr. Costner, with his knack for grumpy understatement, manages both to dominate the film and to deflect attention from himself." 

==Soundtrack==
{{Infobox album  Name        = McFarland, USA (Original Motion Picture Soundtrack) Type        = Soundtrack Artist      = Various artists Cover       =  Released    = February 17, 2015 Recorded    = Genre       = {{flat list|
*Latin pop
}} Length      = 46:31 Producer    =  Label  Walt Disney Misc        = 
}}

{{Track listing
|collapsed=no extra_column   = Artist(s) title1         = Juntos (Together) extra1         = Juanes length1        = 3:18 title2         = The Real McFarlands extra2         = Antônio Pinto (composer)|Antônio Pinto length2        = 2:39 title3  Me and Baby Brother extra3  War
|length3        = 3:26 title4         = Lets Hit It Again extra4         = Antônio Pinto length4        = 1:54 title5         = Lords Prayer extra5         = Antônio Pinto length5        = 3:48 title6  Watermelon Man extra6         = Mongo Santamaria length6        = 2:26 title7         = Barbie Bike extra7         = Antônio Pinto length7        = 1:45 title8  Flash Light extra8  Parliament
|length8        = 4:29 title9         = Convoy to State extra9         = Antônio Pinto length9        = 2:05 title10        = Whittier Blvd. extra10        = Thee Midniters length10       = 2:28 title11        = Beach extra11        = Antônio Pinto length11       = 2:26 title12        = This Aint Golf extra12        = Antônio Pinto length12       = 2:10 title13        = Thats Not Danny Diaz extra13        = Antônio Pinto length13       = 7:52 title14        = McFarland Theme extra14        = Antônio Pinto length14       = 2:43 title15        = América extra15        = Los Tigres del Norte length15       = 3:02 total_length   = 46:31
}}

==See also==

*White savior narrative in film

==References==
 

==External links==
* 
* 
*   at History vs. Hollywood

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 