Fear in the Night (1947 film)
 
{{Infobox film
| name           = Fear in the Night
| image          = Fear in the Night 1947 poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Maxwell Shane
| producer       = William H. Pine William C. Thomas
| screenplay     = Maxwell Shane
| based on       =   Paul Kelly DeForest Kelley Ann Doran Kay Scott
| music          = Rudy Schrager
| cinematography = Jack Greenhalgh
| editing        = Howard A. Smith
| studio         = Pine-Thomas Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 72 minutes
| country        = United States 
| language       = English
| budget         =$128,000 Hedda Hopper: LOOKING AT HOLLYWOOD
Los Angeles Times (1923-Current File)   13 Dec 1946: A3.  
| gross          =
}} Paul Kelly Kevin McCarthy. 

==Plot==
Bank teller Vince Grayson (DeForest Kelley) dreams that he stabs a man in an octagonal room of mirrors and locks the body in a closet.  When he wakes up, he discovers marks on his throat, a strange key and a button in his pocket, and blood on his cuff. Cliff Herlihy (Paul Kelly), his police officer brother-in-law, tries to convince him it was just a dream.

A few days later, while trying to find cover from the rain, the pair finds themselves taking shelter in the strange house from Vinces dream. They discover that the police found two bodies in the house, one in the mirrored room and one run over in the driveway. Mrs. Belknap, who was run over by a car, gave the police a description matching Vince before she died.

At first Vince is hopeful that he is innocent because he does not know how to drive, but he recognizes the victims from his dream. Overcome with remorse, he attempts suicide, but is rescued by Cliff. The detective uncovers clues that point to an evil hypnotist (Robert Emmett Keane) manipulating Vince. They realize that the hypnotist is actually Mr. Belknap in disguise and try to trap him by pretending that Vince wants hush money.

Belknap puts Vince under hypnosis and tries to get him to drown himself. Cliff rescues him from the lake and Mr. Belknap is killed in a car accident as he is trying to evade the police. It is implied that Vince will be acquitted of all charges since he killed the man in the mirrored room in self-defense.

==Cast== Paul Kelly as Cliff Herlihy
* DeForest Kelley as Vince Grayson
* Ann Doran as Lil Herlihy
* Kay Scott as Betty Winters
* Charles Victor as Captain Warner
* Robert Emmett Keane as Lewis Belknap, aka Harry Byrd
* Jeff York as Deputy Torrence

==Reception==

===Critical response===
When the film was released the film critic for The New York Times panned the film, writing, "Fear in the Night, a minor shocker which opened at the Rialto yesterday, is just about as ridiculous as any that comes in this line ... It is not only silly but rather dull. DeForest Kelley is dopey as the fall guy and Paul Kelly is brisk as his detective friend." 

More recently, film critic Dennis Schwartz was more positive and liked the film, writing, "An excellent low-budget psychological thriller directed and written by Maxwell Shane that is based on the story "Nightmare" by Cornell Woolrich. Cinematographer Greenhalghs shadowy black and white photography gives it a film noir look ... The taut pulp story, dreamy atmospheric settings and brooding mood throughout, all serve the film well. The crisp acting was just right. DeForest Kelley, in his debut performance, does a fine job as the innocent victim." 

==See also==
* List of films in the public domain

==References==
 

==External links==
 
*  
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images)
*   analysis by author Thomas C. Renzi at Film Noir of the Week

===Streaming audio===
*   on Suspense (radio drama)|Suspense: March 13, 1948. Radio drama of story on which the movie was based.

 
 
 
 
 
 
 
 
 
 
 