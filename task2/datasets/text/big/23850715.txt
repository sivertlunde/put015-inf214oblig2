Shirley (film)
{{Infobox film
| name           = Shirley
| image          =
| caption        =
| director       = A.V. Bramble
| producer       = 
| writer         = Charlotte Brontë (novel)   Arthur Q. Walton
| starring       = Carlotta Breese Clive Brook Elizabeth Irving   Mabel Terry-Lewis
| music          = 
| cinematography = 
| editing        = 
| studio         = Ideal Film Company
| distributor    = Ideal Film Company
| released       = 1922 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British silent silent drama film directed by A.V. Bramble and starring Carlotta Breese, Clive Brook, Elizabeth Irving and Mabel Terry-Lewis.  It is based on the 1849 novel Shirley (novel)|Shirley by Charlotte Brontë.

==Cast==
* Carlotta Breese as Shirley
* Clive Brook as Robert Moore
* Elizabeth Irving as Caroline Helston
* Mabel Terry-Lewis as Mrs. Prior
* Harvey Braban as Nunnally
* Joe Nightingale
* David Miller

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 