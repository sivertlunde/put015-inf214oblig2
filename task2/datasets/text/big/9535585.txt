Real Men (film)
 
{{Infobox film|
  name           = Real Men|
  image          = DVD cover of the movie Real Men.jpg|
  caption        = DVD cover for Real Men |
  producer       = Martin Bregman |
  director       = Dennis Feldman |
  writer         = Dennis Feldman| Isa Andersen Gail Barle Mark Herrier Dyanne Thorne |
  music          = Miles Goodman |
  cinematography = John A. Alonzo| Malcolm Campbell Glenn Farr |
  distributor    = United Artists |
  released       = September 25, 1987 |
  runtime        = 85 min. |
  country        = United States | English |
  budget         = |
  gross          = $873,903 |
}}
Real Men is a 1987   agent Nick Pirandello (Belushi) and weak and ineffectual insurance agent Bob Wilson (Ritter).

==Plot==
The film opens with Agent Pillbox (played by John Ritter) walking through a forest, when he is shot and killed by an unseen assassin. Pillbox had been engaged on a dry run for a meeting that was to take place with a group of aliens seeking to help humans eliminate a chemical that will end all life on Earth, which scientists accidentally dumped in the ocean.

The fee for this lifesaving miracle? The aliens want a glass of water. However, the Russians, along with a rogue element in the FBI, would like to get to the aliens first because they have also offered something called the "Big Gun" - a gun so big that it could destroy a planet.

Since the aliens have dealt with Pillbox, he is the only one they trust. Tough guy government agent Nick Pirandello is recruited to escort a meek office worker named Bob Wilson (also played by Ritter) whom FBI computers have found as a lookalike for the deceased Agent Pillbox. However, Wilson is no agent, having been easily pushed around by a group of bullies who lived down the street, and by a milkman who was trying to seduce his wife.

Pirandello is unconventional, and likes to do things his way. He meets Wilson at Wilsons home, with Russian agents close on his tail. Their first meeting is to say the least awkward, with Wilson thinking hes an intruder and trying (poorly, yet comically) to attack him, culminating in a shoot-out with the Russians that devastates Wilsons house.

With Wilson now reluctantly in tow, they travel across the country via the "long scenic route" in order to meet with the Aliens somewhere near Washington, D.C. Despite being told the truth, Wilson repeatedly tries to escape (believing the agent crazy and a kidnapper) until Pirandello shows him a piece of alien technology that was gifted to him.

Now a believer, Wilson is willing to do the job, but is still just a weakling compared to Agent Pirandello, that is, until a chance meeting with corrupt FBI agents dressed as clowns. After being lied to, and told hes a sleeper "Super Agent", he charges into battle. Though hes knocked out with one punch by the clowns, Pirandello defeats them, but makes the waking/groggy Wilson believe he did it, though he admits the sleeper agent story was a lie. It proved effective though as Wilson gains a new macho attitude that hed never had.

As Wilson grows stronger, Pirandello grows weaker, because he fell in love with a woman (whom he later found was a dominatrix) he met in a bar in Pittsburgh. After Pirandello abandons the mission, Wilson is left on his own. During a final shootout staged in the woods between rogue FBI element and Wilson, Pirandello finally comes to his senses and rejoins the mission. Wilson completes his quest and they receive the "good package" to save humanity.

Wilson returns to his home to find it completely rebuilt. Using his new-found machismo, he deals with the bullies and the amorous milkman, bringing the final curtain to the film.

==External links ==
 
*  
*  

 
 
 
 
 
 
 
 
 