1 Day
 
{{Infobox film
| name = 1 Day
| image = 1 Day film.jpg
| caption =
| director = Penny Woolcock {{cite news|title= 1 Day the movie - an interview with Penny Woolcock
|work= BBC|date=|url=http://www.bbc.co.uk/birmingham/content/articles/2009/10/29/1day_interview_with_penny_woolcock_feature.shtml
|accessdate=2010-03-02}} 
| producer =
| writer = Penny Woolcock
| executive producer =
| starring = Dylan Duffus Duncan Tobias Yohance Watson Ohran Whyte
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 102 minutes
| country = United Kingdom
| language = English
| budget = £2,000,000 (estimated)  
| followed_by =
}}
1 Day is a 2009 British crime film about gangs and their communities in inner city Birmingham. {{cite news|title= Birmingham gangster movie 1 Day grossed under £44k nationwide on opening weekend |work= Birmingham Mail|date=|url=http://www.birminghammail.net/news/birmingham-news/2009/11/17/birmingham-gangster-movie-1-day-grossed-under-44k-nationwide-on-opening-weekend-97319-25181383/
|accessdate=2010-03-02}}   The story follows Flash as he attempts to get £100,000 to his boss Angel in less than 24 hours or face certain death.  The film is street-cast and features no professional actors. 

==Plot==
Flash (Dylan Duffus) receives a phone call from Angel (Yohance Watson) announcing that hes being released early from prison and wants the £500,000 hes left Flash for safekeeping. Flash is £100,000 short of the full amount and is pushed for time. Flash is forced to strike a deal with Evil (Duncan Tobias) who more than lives up to his name. The movie follows Flashs race against time as he is pursued by a rival gang called The Zampa Boys as Flash is part of OSC (Old Street Crew). He is also pressured by his three irate baby mothers and his grandmother.

==Cast==
*Dylan Duffus	 as	Flash
*Duncan Tobias	 as	Evil
*Yohance Watson	 as Angel
*Ohran Whyte	 as	Pest
*Chris Wilson	 as	Prison Officer
*Malik MD7.           as.  El Presidente

==Reception==
The film has received a mixed reception amongst film critics. {{cite news|title= 1 Day |work= The Times |date=2009-11-06 |url=http://entertainment.timesonline.co.uk/tol/arts_and_entertainment/film/film_reviews/article6904423.ece
|accessdate=2010-03-02 | location=London | first=Kevin | last=Maher}}     

In Birmingham it was withdrawn from the Odeon Cinemas chain, on the advice of the West Midlands Police. The West Midlands Police say they did not give such a statement.  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 