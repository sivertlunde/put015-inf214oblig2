A Millionaire's First Love
 
{{Infobox film
| name           = A Millionaires First Love
| image          = A Millionaires First Love.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| hanja          =  의 첫사랑
| rr             = Baekmanjangjaui cheot-sarang
| mr             = Paengmanchangjaŭi ch‘ŏtsarang}} Kim Tae-kyun
| producer       = Lee Joo-ick
| writer         = Kim Eun-sook  
| starring       = Hyun Bin Lee Yeon-hee
| music          = Kim Tae-seong
| cinematography = Choi Chan-min
| editing        = Ko Im-pyo
| distributor    = Lotte Entertainment
| released       =  
| runtime        = 116 minutes
| country        = South Korea
| language       = Korean
| budget         =
| gross          =
}} Kim Tae-kyun, starring Hyun Bin and Lee Yeon-hee.   The theme of the film is "Nothing is more important than the true love of your heart."  It was released in South Korean cinemas on February 9, 2006, and recorded 639,596 admissions during its run.   

==Plot== Gangwon Province and graduate. Until then all access to his penthouse, cottage and credit cards are denied. Should he fail to graduate or drops out then he loses everything. If he wishes to give up, he will only receive 0.1% of his over-all inheritance. With this in mind, he heads out to the countryside, to a small town in which daily life is far removed from what he is used to.

Shortly after settling into his new home, he meets 18-year-old Choi Eun-hwan (Lee Yeon-hee), who happened to run into him back in Seoul. They don’t exactly hit it off; he’s far too stubborn and cool for school. Conversely, she sees hope for him and sets out to make him see it for himself. They eventually draw closer, thanks to a set of coincidences and school projects. Jae-kyung learns of Eun-hwans terminal disease and does what he can to make her happy. He even tells his lawyer that he would give up his inheritance so he could buy an orphanage for her.

One day during the rehearsal of a play at school, Jae-kyung notices that the play is resembles the incident that had happened in his life. He recalls a childhood memory of being at the orphanage at Gangwon, where he had met Eun-hwan and had made a promise to return in ten days. He finds Eun-hwan and tells her that he did not remember her or his promise because his parents had died in a car accident on that same day and his memories were thus erased. He tells her he loves her and they kiss. 

The day of the play comes and Jae-kyung asks his classmate to trade places as a favour; him as the Colonel rather than the maid. He goes to Eun-hwans side and she asks him if he could put on the socks he bought her earlier. While they are dancing, she feels a sudden pain and goes backstage much to Jae-kyungs worry. However, she returns. Eun-hwans mother has also come to see the show.

Later, they celebrate the success of the play. The couple sit on a swing, Eun-hwan tells Jae-kyung "I love you" and that shes sleepy. Before falling asleep, she tells Jae-kyung to wake her up after 3 minutes. Soon after, the first snow comes.

On their graduation day, Eun-hwan receives the Award of Friendship but does not attend the ceremony as she never woke up. A bouquet of white flowers sits in her seat instead, next to Jae-kyungs chair. He stays for a while and walks outside carrying the flowers. His grandfathers lawyer comes, congratulates him and tells him that the inheritance is now his.

Later, Jae-kyung builds a house with numerous windows in memory of Eun-hwan. He states that after 50 first snows hit its windows, they will meet again.

==Soundtrack==
# Prologue
# Insa (Farewell) - Jaejoong from TVXQ
# 거친 나의 나날들 
# Gray Noise - Yeongene
# 기억을 따라가는 재경 (Memories Follow Jae-kyung)
# 털양말 (Feather Socks)
# 피리부는 소녀 (Flute Girl)
# Dialogue 1
# 우리 은환이 좀 살려주세요 (Please Let Eun-hwan Live)
# Dialogue 2
# Kiss
# 이제 너 많이 힘들어 지겠다 (It Will Be Really Hard for You Now)
# Dialogue 3
# 들판을 거닐며 
# Insa (Inst.)
# Dialogue 4
# Insa - Lee Yeon-hee
# Dialogue 5
# 첫눈 (First Snow)
# Insa - TVXQ
# Dialogue 6

==International release==
Distribution rights to Japan were purchased by Digital Adventure for  .  

==Remake== Nepali film Telugu film Pilla Zamindar (2011). Both unofficial remakes were hits at the box office in their respective countries.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 