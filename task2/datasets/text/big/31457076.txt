The Test (1935 film)
{{Infobox film
| name           = The Test
| image          =
| image_size     =
| caption        =
| director       = Bernard B. Ray
| producer       = Bernard B. Ray (producer) Harry S. Webb (associate producer)
| writer         = James Oliver Curwood (story) L.V. Jefferson (continuity) L.V. Jefferson (dialogue)
| narrator       =
| starring       = See below
| music          =
| cinematography = J. Henry Kruse Abe Scholtz
| editing        = Frederick Bain
| distributor    = Reliable Pictures
| released       = 1935
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Test is a 1935 American adventure film directed by Bernard B. Ray and produced by Ray and Harry S. Webb for Reliable Pictures. It features as its hero the dog Rin Tin Tin Jr.

== Cast ==
*Grant Withers as Brule Conway
*Grace Ford as Beth McVey
*Monte Blue as Pepite La Joie
*Lafe McKee as Dad McVey
*Artie Ortego as Henchman Black Wolf
*Jimmy Aubrey as Henchman Donovan
*Nanette the Dog as Nanette
*Rin Tin Tin Jr. as Rinnie

==Plot==
Fur trapper Brule Conway sets Rin Tin Tin Jr. to guard his furs. The henchmen of a rival trapper use a female dog to lure Rinty away from his post, and then proceed to steal the stash of furs. Rinty sets out to capture the thieves and return his masters furs.

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 
 


 