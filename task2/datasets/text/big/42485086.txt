Hold That Pose
 
{{Infobox Hollywood cartoon cartoon name=Hold That Pose
|series=Goofy
|director=Jack Kinney
|producer=Walt Disney story artist=Dick Kinney Milt Schaffer voice actor=Pinto Jimmy MacDonald Paul Smith
|animator=John Sibley Ed Aardal Hugh Fraser  Jack Boyd layout artist=Al Zinnen background artist=Ed Levitt Walt Disney Productions
|distributor=RKO Radio Pictures release date=  (USA) color process=Technicolor
|runtime=6:49 minutes
|country=United States
|language=English 
}}
 Walt Disney Productions and released RKO Radio Pictures. The films plot centers on Goofy trying to get a job as a wildlife photographer but ends up causing trouble in a grizzly bears pen at a zoo. This is also Humphrey the Bears debut appearance.    

==Plot==
A narrator explains that when you get bored or constantly have unsatisfying days, you need a hobby - in   and cut film. When Goofy gets the film all loaded in, the narrator says that he should pick a task suck as wildlife watching. Goofy goes to the grizzly bear habitat at a zoo to photograph Humphrey the Bear. Slight gags show Goofy to be an inept photographer, waking Humphrey up, putting a cape over Humphreys head, taking pictures right in his face, and creating a small volcano that blows up on Humphrey. The last straw comes when Goofy takes a picture of Humphreys dinner with his family. The angered bear chases Goofy out of the zoo, across town, through a stage and back to Goofys apartment via a taxi (with Humphrey as the driver). The bear chases Goofy up the stairs, and finally into Goofys apartment and closet where Goofy is mauled. However, Humphrey calms down after Goofy shows him his pictures, and when Humphrey goes on vacation he takes them with himself to show them to tourists.

==See also==
*Rugged Bear Grin and Bear It
*Bearly Asleep 
*Beezy Bear
*Hooked Bear
*In the Bag

==References==
 

==External links==
* 


 
 
 
 
 

 