Cop and a Half
 
{{Infobox film
| name           = Cop and a Half
| image          = Cophalf.jpg
| caption        = Theatrical Release Poster
| director       = Henry Winkler
| producer       = Paul Maslansky
| writer         = Arne Olsen Ralph Wilcox Rocky Giordani Bill Butler
| editing        = Daniel P. Hanley Carroll Timothy OMeara Roger Tweten Imagine Films Entertainment
| distributor    = Universal Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $40,689,691 music = Alan Silvestri}}
Cop and a Half is a 1993 American crime film|criminal-comedy film directed by Henry Winkler, and starring Burt Reynolds, Norman D. Golden II and Ray Sharkey in his final role. It was originally planned to be a sequel of the 1990 hit Arnold Schwarzenegger comedy, Kindergarten Cop.

==Plot==
Devon Butler (Golden) is an eight-year-old boy who lives in Tampa and dreams of being a police officer|cop. He watches police TV shows, knows police procedures and plays cops and robbers with his friend Ray. One day, while snooping around in a warehouse, he witnesses a murder. He goes to the police, who want the information, but he refuses to give it unless they make him a cop. They then team him with veteran cop (and child hater) Nick McKenna (Reynolds), and they team up in a comic series of events to find the killer. They eventually come to a mutual understanding in order to bring the killer to justice.

==Cast==
*Burt Reynolds as Det. Nick McKenna 
*Norman D. Golden II as Devon Butler
*Ruby Dee as Rachel
*Holland Taylor as Captain Rubio
*Ray Sharkey as Vinnie Fountain
*Sammy Hernandez as Raymond Sanchez
*Frank Sivero as Chu
*Rocky Giordani as Quintero
*Marc Macaulay as Waldo
*Tom McCleister as Rudy Ralph Wilcox as Det. Matt McPhail
*Tom Kouchalakos as Det. Jenkins

==Soundtrack==
Joey Lawrences "Nothin My Love Cant Fix" is used as the end title song.

==Reception==
The film received generally negative reviews from critics and audiences. It currently holds a 17% "rotten" rating at the movie review aggregator site Rotten Tomatoes, where only two reviews out of the twelve polled are positive.  

Film critic and historian Leonard Maltin wrote, "A hemorrhoid-and-a-half to whoever sits through this abjectly painful comedy, which does for Burt Reynolds career what Stop! Or My Mom Will Shoot did for Sylvester Stallones." Critic Gene Siskel also excoriated the film, seeing it as indicative of "artistic bankruptcy" on Burt Reynolds part, and singled out Norman D. Golden IIs performance as "awkward". Siskel later called it the worst movie of 1993. Siskel speculated that NBC thought little of the film when they aired it in its broadcast-network debut, pointing out that they scheduled it opposite the 1997 Super Bowl.  However, Roger Ebert gave the film a positive review, giving it a thumbs up. He also gave it 3 stars out of a possible 4 saying "There isnt much thats original in "Cop and a Half," but theres a lot thats entertaining, and theres a winning performance by a young man with a big name, Norman D. Golden II, who plays little Devon Butler, a kid who dreams of someday wearing the shield."

===Box office===
The movie debuted at No.1.  In its second week it dropped to number 3. 

==Awards==

{| class="wikitable"
|-
! Awards
! Category
! Subject
! Result
|- Stinkers Bad Movie Awards  Worst Picture
|
| 
|- Worst Actor Burt Reynolds
| 
|- Worst Actor Norman D. Golden II
| 
|- Golden Raspberry Award Golden Raspberry Worst Actor Burt Reynolds
| 
|- Golden Raspberry Worst New Star Norman D. Golden II
| 
|- Young Artist Award Best Actor Under Ten in a Motion Picture
| 
|}

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 