Chatrapathy (2004 film)
{{Infobox film
| name           = Chatrapathy
| image          = 
| caption        = 
| director       = Srimahesh
| producer       = Babu Raja
| starring       = Sarathkumar Nikita Thukral
| music          = S. A. Rajkumar
| cinematography = Karthik Raja
| editing        = 
| distributor    =
| studio         = JJ Films
| released       =   
| runtime        =
| country        = India
| language       = Tamil
| budget         =
}}
Chatrapathy is a Tamil language film directed by Srimahesh. The film features R. Sarathkumar and Nikita Thukral in lead roles whilst Vadivelu, Mahadevan and Rajesh play supporting roles. The film was released in February 2004 and later re-released in Hindi under the dubbed title of The Great Chatrapathy. 

==Plot==
Saravanan is a Bus Driver in a college. He also takes care of an Orphanage which is home to numerous children and old-aged people. Indhu, a student of the college tries to Woo Saravanan while Rowdys and other Anti-Social elements in the city are found dead one after the other. Chakravarthy, the politico who loses his right-hand man vows revenge and realizes that, Saravanan the person who killed his henchmen is indeed an Army Major whom he and his men had eliminated years ago in a scuffle over his sister Saranyas land.

How Saravanan fends off the evil elements and unites with Indhu forms the rest of the storyline.

==Cast== Sarathkumar as Saravanan
*Nikita Thukral as Indhu Mahadevan as Chakravarthy Adithya Menon Rajesh
*Vadivelu
*Ilavarasu  Jasper
*Lambert Saranya as Saravanans Sister 
*Sabitha Anand Rambha in a special appearance

==References==
 

 
 
 
 
 

 