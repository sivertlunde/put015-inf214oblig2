Strange Bargain
{{Infobox film
| name           = Strange Bargain
| image          = Strange bargain poster small.jpg
| image_size     = 150px
| alt            =
| caption        = Theatrical release poster
| director       = Will Price
| producer       = Sid Rogell
| screenplay     = Lillie Hayward
| story          = J.H. Wallis
| narrator       =
| starring       = Martha Scott Jeffrey Lynn Harry Morgan
| music          = Friedrich Hollaender
| cinematography = Harry J. Wild
| editing        = Frederic Knudtson
| studio         = RKO Pictures
| distributor    = 
| released       =    |ref2= }}
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Strange Bargain is a 1949 American crime film noir directed by Will Price, and starring Martha Scott, Jeffrey Lynn and Harry Morgan. 

==Plot==
A bookkeeper in need of money agrees against his own better judgement to help a wealthy man carry out an elaborate suicide plan.

==Cast==
* Martha Scott as Georgia Wilson
* Jeffrey Lynn as Sam Wilson
* Harry Morgan as Lt. Richard Webb
* Katherine Emery as Edna Jarvis
* Richard Gaines as Malcolm Jarvis
* Henry ONeill as Timothy Hearne
* Walter Sande as Sgt. Cord
* Michael Chapin as Roddy Wilson
* Arlene Gray as Hilda Wilson
* Raymond Roe as Sydney Jarvis
* Robert Bray as Det. McTay

==Reception==
===Critical response===
A.H. Weiler, the film critic for the New York Times penned a fairly positive review. He wrote, "As a modest entry from Hollywood, Strange Bargain, which began a stand yesterday as the associate attraction to the Palaces vaudeville bill, is surprisingly diverting fare. Obviously not intended to set a cinematic landmark, it is, nevertheless, a melodrama that presents an extraordinary situation fairly suspensefully and, for the most part, through intelligent dialogue and direction. And, while it follows a familiar outline as a crime and punishment adventure, it does so neatly and with competent characterizations." 

Film critic Dennis Schwartz called the film "  well-conceived mystery B-film, but strictly second feature material."  He also added in his review, an interesting anecdote regarding a re-make of the film on television, "Strange Bargain showed up in a 1989 (sic) episode of TVs Murder, She Wrote. By removing the original happy ending, the TV installment allowed Angela Lansbury to solve the mystery of the boss murder--and to exonerate the long-imprisoned bookkeeper, played again by Jeffrey Lynn. Also appearing on this Murder She Wrote were Lynns Strange Bargain costars Martha Scott and Henry Morgan. 

In actuality, the episode aired in April 1987, during the shows third season.  And, it was originally entitled "The Days Dwindle Down."

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 