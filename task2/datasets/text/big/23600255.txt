Hitler (1996 film)
 
 
{{Infobox film
| name           = Hitler
| image          = Hitler Malayalam.jpg Siddique
| Lal Auseppachan Azeez Siddique
| Mukesh Shobana Shobhana Saikumar Saikumar Jagadish Vani Viswanath
| music          = S. P. Venkatesh (music) Gireesh Puthenchery (lyrics)
| cinematography = Anandakuttan
| editing        = T. R. Shekhar K. R. Gaurishankar
| studio         = Lal Creations Lal Release
| released       =  
| runtime        = 153 minutes
| country        = India
| language       = Malayalam
| gross =
}}

Hitler (  film written and directed by Siddique (director)|Siddique, co-produced by Lal (actor)|Lal, and starring Mammootty in the lead role and Mukesh (actor)|Mukesh, Shobana Chandrakumar|Shobhana, Saikumar (Malayalam actor)|Saikumar, Jagadish, and Vani Viswanath in supporting roles.

== Plot ==
The movie speaks about the life of Madhavankutty (Mammootty), locally known as "Hitler" due to his tough character, domineering personality and his uncontrolled rage at the youngsters of the area, for stalking his five young sisters. Even though he loves his family more than anything in this world, he fails to express his love. He had been looking after the family since the death of their mother. Their father Pillechan (Innocent (actor)|Innocent) is married again with two daughters and his children from the first marriage do not even speak with him (Madhavankuttys uncle Gangadhara Menon is responsible for the separation between the children and Pillechan, which is revealed in a later part of the story). Madhavankuttys marriage was fixed with his cousin Gauri (Shobana Chandrakumar|Shobhana) and his sister Ammu’s (Vani Viswanath) marriage with Gauris brother Balachandran (Mukesh (actor)|Mukesh). Once in Madhavankuttys absence, Balachandran sneaked in to his house to be with Ammu, but get caught which results in hostility between the two families and Madhavankutty opposes any of the decided marriages to happen between the families. Balachandran on the other hand wants to make them happen.

Things change when the eldest sister Seetha goes through an unfortunate event, where her unmarried professor (M. G. Soman) under influence, rapes her. Coming back to senses, the professor confesses his act to Madhavankutty. Devastated on hearing this and learning how this incident can affect the lives of his younger sisters, he is forced to go with the solution that the professor puts forward. He gets Seetha married to the professor. His sister Ammu unaware of the facts behind the marriage get furious with him, while him being inherently stoic behave as a rude dominant to hide the facts. Balachandran finds this as an opportunity to get Ammu, who otherwise wouldn’t do anything against her brother’s wishes. Balachandran and Ammu get married and settle in the neighborhood.

Due to the new developments, Madhavankuttys uncle Gangadhara Menon (Kozhikode Narayanan Nair) tries to befriend their rival family – Kompara. They on the other hand were waiting for an opportunity to take revenge, pretend to offer help, while their real intention was to increase the hatred between Madhavankutty and his uncle as they see Madhavankutty as a threat and can never do anything unless they remain enemies. They also suggest a marriage alliance with Gauri, which Gangadhara Menon approves. Gauri, still in love with Madhavankutty, acts pregnant saying Madhavankutty as the one responsible for it to prevent the marriage. This agitates Gangadhara Menon and under the advice of Nandakumar (from Kompara family), he decides to destroy the lives of Madhavankutty’s sisters, the way he destroyed Gauris life. Madhavankuttys father learns about the plot and foils them, but it causes the girls and him to end up in the police station. He fails to convince his son about the truth and they become angrier at their father for his behavior. Nandakumar (Saikumar (Malayalam actor)|Saikumar) sees the opportunity and they murder Pillechan. Madhavankutty, eventhough angry at his father’s deeds, does not desert his half sisters and brings them home, much to the dismay of his sisters who have an argument with him and leaves to Ammus home to stay. Balachandran realizes the seriousness of the situation and asks the girls to go back to their brother. He also say that whatever he did, was for Madhavankutty to stop sacrificing his life for his sisters and to get him married to his sister who loves him for life. Balachandran goes to find Madhavankutty to apologize, but gets attacked by a gang of thugs under the command of a person, seemingly Madhavankutty, while it actually was Krishnanunni the eldest of Kompara family who framed him. Balachandran survives and thinks Madhavankutty did it, which causes everyone, even Gauri to turn against him. Gangadhara Menon along with the ones from Kompara family goes to finish Madhavankutty once and for all, but learns that he himself was the target and Balachandran was not attacked by Madhavankutty. They try to murder Menon thinking after all that has happened Madhavankutty will not dare rescue him, but to their surprise he shows up to save Menon. Wounded Menon at the hospital tells to the rest of the family about his nephews innocence. Madhavankutty survives the attack and succeeds in getting rid of the Kompara family from their lives. Losing hope at everything he believes, he decides to leave all behind and go someplace. He does not listen to the apologies or requests of the sisters to stay. He asks Balachandran to take care of the sisters and to find a better person for Gauri as he will never be a good husband for her. He begins to leave while a youngster shows up staring at his sisters leading him to chase the one, indicating that he cant stop being himself and can never leave. Satelite rights -Surya Tv and Amrita Tv

==Cast==
* Mammootty as Mamangalath Madhavankutty/Hitler Madhavankutty Mukesh as Mamangalath Balachandran Shobana as Gauri
* Ilavarasi as Seetha
* Vani Viswanath as Ammu
* Suchithra as Gayathri Chippy as Thulasy
* Seetha as Ambily
* Seena Antony as Sandhya
* Manju Thomas as Sindhu Innocent as Madhavan Kuttys father
* Jagadish as Hrudayabhanu
* Kozhikode Narayanan Nair as Gangadhara Menon and Gouris father
* K. P. A. C. Lalitha as Gouris mother Zainuddin as Sathyapalan
* Adoor Bhavani as Sathyapalans mother
* Cochin Haneefa as Jabbar
* Idavela Babu as Chandru
* Kalabhavan Rahman as Sulaiman
* V. K. Sreeraman Saikumar
* Mohanraj
* Kanakalatha

== Soundtrack ==
The films soundtrack contains 7 songs, all composed by S. P. Venkatesh and Lyrics by Gireesh Puthenchery.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Kithachethum Kaatte"
| K. S. Chitra, M. G. Sreekumar
|-
| 2
| "Maarivil Poonkuyile"
| Arundhathi
|-
| 3
| "Nee Urangiyo"
| K. J. Yesudas
|-
| 4
| "Neeyurangiyo Nilave (F)"
| K. S. Chitra
|-
| 5
| "Sundarimaare Ketti" Chorus
|-
| 6
| "Vaarthinkale"
| K. S. Chitra
|-
| 7
| "Vaarthinkale"
| K. J. Yesudas
|}

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
|-
| 1997
| Hitler (1997 film)|Hitler Telugu cinema|Telugu Rajendra Prasad, Rambha (actress)|Rambha, Prakash Raj Dasari Narayana Rao Muthyala Subbayya
|-
| 2000
|Krodh (film)|Krodh Hindi cinema|Hindi
| Sunil Shetty, Rambha (actress)|Rambha, Apoorva Agnihotri, Sakshi Shivanand
| Ashok Honda
|-
| 2003
|Military (film)|Military Tamil cinema|Tamil
|Sathyaraj, Rambha (actress)|Rambha, Livingston (actor)|Livingston, Vijayalakshmi (Kannada actress)|Vijayalakshmi, Manivannan
| G. Sai Suresh
|-
| 2005
|Varsha (2005 film)|Varsha Kannada cinema|Kannada Vishnuvardhan (actor)|Vishnuvardhan, Ramesh Aravind, Manya (actress)|Manya, Komal, Doddanna
| S. Narayan
|-
|}

==External links==
*  

 

 
 
 
 
 
 