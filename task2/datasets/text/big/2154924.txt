Pornstar Pets
{{Infobox film
| name        = Pornstar Pets
| image       = Pornstar-Pets-poster.jpg
| caption     = DVD cover for Pornstar Pets
| director    = Margie Schnibbe
| writer      =
| starring    = Ron Jeremy, Rebecca Lord
| producer    =
| distributor = Brink Film
| released    =  
| runtime     = 52 min.
| country     = United States
| language    = English
| budget      =
}}
Pornstar Pets is a 2005 documentary film directed by Margie Schnibbe.

The 52 minute film includes 25 pornographic actors and their sometimes exotic pets in a vein similar to Celebrity Pets.

== Content ==
Schnibbe says that her "intention in making Pornstar Pets was to demystify the life of the adult performer and to reveal the humanity of those often viewed as outsiders by society. By showing porn stars interacting with their family pets, we get a glimpse of the kinder, gentler side of the adult industry and the people who work in it."

=== Actors ===
The featured actors include: Airforce Amy and her cats Mufasa, Nala and Kiera
*Brittany Andrews and her cats Ahab and Skoschi
*Violetta Blue and her rat Lithium her cat Blue
*Kim Chambers and Scott Styles and their Sugar Gliders Queer and Little Girl
*Jessica Drake and her Malamute Sky
*Ron Jeremy and his turtle Cherry and rabbit Tyrannosaurus Rex Chihuahua Tito
*La Tia Lopez and her Pit Bull Platinum Boxers Beavis, Leon and Lolita
*Nick Manning and Shay Sights and their dog Tim, cats Willie and Albert and fish Gill
*Mr. Marcus and his Pekeapoo Princess
*Bill Margold and his cats Pogo and Samson
*Sharon Mitchell and her macaw Boo
*Melissa Monet and her dog Bonnie
*Kitten Natividad and her Pit Bull China and cats Negro, Tina and Lovee
*Dave Pounder and his cat Anthrax
*Lena Ramon and her menagerie of 37 animals
*Anastasia Sands and her Persian Cat Banjo
*Jeremy Steele and his turtle Socrates and three Chinchillas
*Evan Stone and his dog Sadie
*Sunset Thomas and her Border Collie Domino and horses
*Taylor Wane and her Yorkshire Terrier Buddie and mix-breed dog Boo-Boo
*Teri Weigel and her snakes, birds and Shelties

The film is distributed by  .  

== References ==
 

==External links==
* 
* 

 
 
 


 