Saw VI
 
{{Infobox film
| name           = Saw VI
| image          = Saw VI Poster.jpg
| alt            = Gears and machinery form the shape of a VI. The title of the film is seen near the bottom of the poster.
| caption        = Theatrical release poster
| director       = Kevin Greutert
| producer       = Mark Burg Oren Koules
| writer     = Patrick Melton Marcus Dunstan
| starring       = Tobin Bell Costas Mandylor Betsy Russell Mark Rolston Peter Outerbridge Shawnee Smith 
| music          = Charlie Clouser
| cinematography = David Armstrong
| editing        = Andrew Coutts
| studio         = Twisted Pictures
| distributor    = Lionsgate Films
| released       =  
| runtime        = 90 minutes  
| country        = Canada United States
| language       = English
| budget         = $11 million 
| gross          = $68.2 million 
}}
Saw VI is a 2009 Canadian-American horror film directed by Kevin Greutert from a screenplay written by Patrick Melton and Marcus Dunstan. It is the sixth installment in the seven–part Saw (franchise)|Saw franchise and stars Tobin Bell, Costas Mandylor, Betsy Russell, Mark Rolston, Peter Outerbridge, and Shawnee Smith. It was produced by Mark Burg and Oren Koules of Twisted Pictures and distributed by Lionsgate.
 Jigsaw Killer William Easton, Peter Strahm, now suspected of being Jigsaws last accomplice, and Mark Hoffman is drawn into motion to protect his secret identity.

Greutert, who served as editor for all the previous Saw films, made his directorial debut with Saw VI. Melton and Dunstan, the writers for both  . Filming took place in Toronto from March to May 2009 with a budget of $11 million.
 Reviews were mixed, with some criticizing the acting and others praising Greuterts directing.

==Plot== Dan Erickson, Peter Strahms Lindsey Perez is still alive; Erickson had her survival kept secret to protect her.
 Pamela Jenkins, Jill Tuck John Kramers will. A flashback shows that John previously brought Amanda Young to Jill, who had declared her a lost cause, as proof that his methods worked. Later, as she delivers a package left in the box to the hospital, it is shown that Jill met with John at the plant hours before his death, where he gave her the boxs key and his promise that she would have a way out.
 Emily and List of Saw characters#Carousel Room Victims|Shelby.
 Brent and Seth Baxter was discovered, but the voice did not match Jigsaws. Erickson and Perez bring Hoffman to the site where a technician is unscrambling the voice, and Erickson tells Hoffman that abnormalities found in Strahms fingerprints revealed to the agents that Strahm was dead. Hoffmans voice is unscrambled by the technician, at which point he kills everyone in the room and plants fingerprints using Strahms severed hand, before setting the room on fire to destroy the evidence.
 Lynn Denlon, using the knowledge that Amanda had an unintentional role in Jills miscarriage. The letter was found by Pamela at the plant and given to Jill, who uses it to ambush Hoffman with an electric shock from behind. At the same time, William reaches the end of his path and finds himself between the cages, where it is revealed that Pamela is Williams sister while Tara and Brent are Harolds surviving family. A videotape of John informs Tara that she can either kill William or free him using a marked switch. When Tara is unable to do so, Brent shifts the switch down and a platform of needles swings down and pierces Williams body, pumping him with hydrofluoric acid, and killing him. After restraining Hoffman, Jill locks a new reverse bear trap to his head, and shows him the sixth envelope, which contained his photograph. She leaves him with 45 seconds and no key, but Hoffman manages to remove the device as it activates, and screams in agony as his right cheek is torn open, leaving his face disfigured.

==Cast==
 
  John Kramer Detective Mark Hoffman Jill Tuck
* Shawnee Smith as Amanda Young William Easton Agent Dan Erickson Agent Lindsey Perez Pamela Jenkins Harold Abbott Tara Abbott Brent Abbott Debbie
* Dave
* Josh
* Shelby
* Emily
* Gena
* James Gilbert Aaron
* Allen
* Addy
* Hank
* Simone
* Eddie
* James Van Patten as List of Saw characters#Trevor|Dr. Heffner Cecil Adams Timothy Young
 

==Production==

===Development and writing=== editor for the film.  Saw VI marked David Armstrongs last time to serve as cinematographer of the series.    Mark Burg and Oren Koules again served as producers, with James Wan and Leigh Whannell, creators of the series, executive producing. Charlie Clouser was brought back to compose the score.    Patrick Melton and Marcus Dunstan, writers of Saw IV and Saw V, returned to write the sixth installment.     Melton said that the film had good pacing and a resolution for the series.  Greutert commented that Saw VI would have some finality to it, something he always wanted to see in the series.  During the early planning stage for the script it was suggested that Mandylors character, Detective Hoffman, should take on the American Mafia|mafia, but the idea was quickly dismissed as not "feeling Saw enough".      Greutert said in a Demon FM interview that Lionsgate told him a week before filming, that Saw VI would be post-converted into 3D film|3D. Greutert was upset by this, since the film he envisioned was a 2D film, aesthetically. The plans were later abandoned due to time restraints. 

===Casting===
  Lionsgate made a public statement ensuring her a leading role in the film but did not elaborate further on her character. 

It was confirmed on March 24, 2009 that  s character List of Saw characters#Dr. Gordon|Dr. Gordon back but Elwes was not available. He was later cast in Saw 3D, though the storyline is very different than the one Greutert had for him in Saw VI. 

===Filming and trap designs===
  medication to prevent motion sickness. ]]
With a budget of $11 million, Saw VI began principal photography on March 30, 2009 and wrapped on May 13, 2009.      The film was shot at Torontos Cinespace Film Studios.   Greutert said that the victims in traps would be more one-on-one with the trap and would be more personal to them. This was compared to Saw IV and Saw V, which most of the traps were set in big rooms and involved several people at one time. 

Armstrong told   that the "carousel room" was, to date, the "longest trap scene ever". He admitted that originally they had ten actors riding the carousel, but it was ultimately scaled down to six, to "tie in to the   title".  Post-production services were provided by Deluxe Entertainment Services Group Inc.|Deluxe. 

==Soundtrack==
 
{{Infobox album  
| Name = Saw VI: Original Motion Picture Soundtrack
| Type = Soundtrack
| Artist = Various Artists
| Cover = SawVIsoundtrack.jpg
| Released = October 20, 2009
| Recorded = Heavy metal, metalcore
| Length = 75:38 Trustkill
| Producer =
| Last album = Saw V Original Motion Picture Soundtrack  (2008)
| This album = Saw VI: Original Motion Picture Soundtrack  (2009) Saw 3D Original Motion Picture Soundtrack (2010)
}} heavy metal music, something that had been missing since Saw IV. He said in his review that "Its a fitting marriage, as hard rock and heavy metal are the sonic suitors to horror and torture porn films and video games". He particularly liked the songs by Hatebreed ("In Ashes They Shall Reap"), Converge ("Dark Horse"), My My Misfire ("The Sinatra"), and Kittie ("Cut Throat"), calling the songs the "most ferocious moments this time around".  |title=Saw VI (Soundtrack) Review|last=Christopher Monger|first=James|work=Allmusic|publisher=All Media Guide|accessdate= }} 

;Track listing
{{tracklist
| collapsed       = yes
| headline = 6 Chances
| writing_credits = yes
| extra_column = Artist total_length = 23:37
 In Ashes They Shall Reap
| extra1 = Hatebreed
| writer1 = Chris Beattie, Matt Byrne, Jamey Jasta, Sean Martin
| length1 = 3:20
 The Last Goodbye
| extra2 = Lacuna Coil
| writer2 = Don Gilmore, Lacuna Coil
| length2 = 4:15
 Reckless Abandon Jason Wood
| extra3 = It Dies Today
| length3 = 3:56

| title4 = Your Soul Is Mine
| writer4= Mushroomhead
| extra4 = Mushroomhead
| length4 = 4:50
 Warpath
| Mark Hunter, Jim LaMarca, Chris Spicuzza
| extra5 = Chimaira
| length5 = 4:18
 Code of the Road
| writer6 = John Calabrese, Dan Cornelius, Danko Jones
| extra6 = Danko Jones
| length6 = 2:58
}}

{{tracklist
| collapsed       = yes
| headline = 6 Lessons
| writing_credits = yes
| extra_column = Artist total_length = 19:19
 Genocide 
| note7 = Saw VI Remix
| writer7= Christopher Garza, Mark Heylmun, Alexandro Lopez, Mitchell Lucker
| extra7 = Suicide Silence
| length7 = 3:01
 Ghost in the Mirror
| writer8 = Kellen McGregor, Memphis May Fire, Matt Mullins
| extra8 = Memphis May Fire
| length8 = 3:53

| title9 = The Countdown Begins
| writer9 = William Bean, Brian Kemsley, Ryan OConnor, Tim OConnor, Erik Perkins Outbreak
| length9 = 1:48
 Still I Rise
| note10 = Saw VI Remix
| writer10 = Jonathan Donais, Brian Fair, Paul Romanko
| extra10 = Shadows Fall
| length10 = 3:27
 Dead Again
| writer11= Peter Steele
| extra11 = Type O Negative
| length11 = 4:16
 Dark Horse
| writer12 = Kurt Ballou, Jacob Bannon, Ben Koller, Nate Newman Converge
| length12 = 2:55
}}

{{tracklist
| collapsed       = yes
| headline = 6 Choices
| writing_credits = yes
| extra_column = Artist total_length = 20:58
 Cut Throat
| writer13 = Mercedes Lander, Morgan Lander, Tara McLeod, Ivy Vujic
| extra13 = Kittie
| length13 = 2:56

| title14 = Never Known
| writer14 = Bon Harris, Douglas McCarthy
| extra14 = Nitzer Ebb
| length14 = 4:04 Roman Holiday
| writer15 = Jordan Buckley, Keith Buckley, Josh Newton, Mike Novak, Andy Williams
| extra15 = Every Time I Die
| length15 = 2:51

| title16 = The Sinatra
| writer16 = My My Misfire
| extra16 = My My Misfire
| length16 = 3:20

| title17 = Lethal Injection The Flood
| extra17 = The Flood
| length17 = 3:48

| title18 = More Than a Sin James Grant
| extra18 = James Brothers
| length18 = 3:59
}}

{{tracklist
| collapsed       = yes
| headline = Bonus Digital Tracks
| extra_column = Artist total_length = 11:29
 We Own the Night
| extra19 = The 69 Eyes
| length19 = 3:57

| title20 = Watch Us Burn
| extra20 = Ventana
| length20 = 4:00
 Forgive & Forget
| extra21 = Miss May I
| length21 = 3:32
}}

==Release== R rating restricted screenings adult theaterss Buena Vista, the films foreign distributor, appealed the decision.  After producers cut several of the "most violent scenes" to obtain a "not under 18" rating, it will be released in Spain on October 8, 2010. 

===Home media=== commentary tracks, the first film with the original bonus features from the initial release.  The film is now sold by itself without the original Saw. According to The Numbers.com, which only counts DVD sales in the United States, Saw VI placed number three its first week on the DVD sales chart, selling 220,107 units ($2,766,088) in the United States.  In comparison, Saw V sold 515,095 units ($11,326,939) its first week.  In the first three weeks Saw VI sold 443,710 units for $7,587,396. 

==Reception==

===Box office===
Saw VI opened in 3,036 theaters on 4,000 screens  and earned $6.9 million on its opening day, in second place behind Paranormal Activity which grossed $7.5 million that day during its second weekend of wide release.   It grossed $14.1 million its opening weekend,  which is the lowest of all the Saw films.    It remained at number two behind Paranormal Activity which was playing on only 64% as many screens as Saw VI, but made 67% more money.  

On Halloween weekend, it moved down to number six and made $5.2 million, a 63% decrease in ticket sales from the previous weekend.  By its third weekend it declined in sales by 61% and was removed from 945 theaters. It fell into 11th place with $2 million.  By its fourth weekend, ticket sales declined by 78% and the film was pulled from 1,314 theaters; it made $449,512.  On its fifth and final weekend it made $91,875, an 80% decrease, and it was pulled from an additional 599 theaters. It was being shown in 178 theaters by the end of its run.  The film closed out of theaters on  , after only 35 days.   
 United Kingdom Russia with Spain on United States and Cinema of Canada|Canada, and $40.5 million in other markets, for a worldwide total of $68.2 million; making it the lowest-grossing film of the series.  

{| class="wikitable" width=99% border="1"
!rowspan="2"  align="center" | Release date (United States)  
!rowspan="2"   align="center" | Budget (estimated) 
!colspan="3"  align="center" | Box office revenue 
|-
!align="center"   | United States/Canada
!align="center"   | Other markets
!align="center"   | Worldwide
|- October 23, 2009
|align="center" | $11,000,000
|$27,693,292
|$40,540,337
|$68,233,629
|-
|}

===Critical response  ===
Saw VI was not screened in advance for critics.     The film received mixed reviews from film critics, and garnered the second best critical reception out of the series, after Saw (2004 film)|Saw (2004). Review aggregation website Rotten Tomatoes reports that 37% of 70 critics have given the film a positive review, with a rating average of 4.3 out of 10,  and the critical consensus "It wont earn the franchise many new fans, but Saw VI is a surprising step up for what has become an intricately grisly annual tradition."  Metacritic, which assigns a normalized score out of 100 to reviews from film critics, gives the film a rating score of 30, based on 12 reviews.  CinemaScore polls reported that the average grade moviegoers gave the film was a "C" on an A+ to F scale. 
 s character was killed in Saw III, his Jigsaw persona sustains a crucial part in every sequel. ]]
 IGN Movies rated Saw VI three out of five and wrote that "while Saw VI certainly offers a redemption for the series and the promise of a coming power struggle for Jigsaws legacy, Saw VII will no doubt mark the time to either shake things up or watch this franchise get the ax". 

Frank Scheck of   gave the film a negative review. He was displeased that the film offered nothing new saying, "The first three Saw movies had some intriguing ideas and an unusual way of presenting them, but the three most recent films have barely bothered to come up with anything fresh."   Kim Newman of Empire (magazine)|Empire gave the film three out of five, stating "Saw VI gets back to Saw basics in gripping, gruesome manner." 

  gave the film three and a half out of five, writing, "Director Kevin Greutert hasnt helmed a lot of films in the past, but he did edit all of the previous Saws. As it turns out, his mastered craft lends well to directing. He spins a taut, tight, concise web of terror and surprise. The best entry in the series since Saw II."  Brad Miska of Bloody Disgusting gave the film seven out of ten and wrote "Saw VI is faithful to the franchise and the twist/finale are 100% satisfying. Saw fans will walk out of the theater with their fists in the air with the feeling that theyve reclaimed their beloved franchise."  Marc Savlov of Austin Chronicle gave the film one and half out of five stars, saying "Enshrouding the whole gooey mess in the already blood-spattered surgical garb of the ongoing health care debate is a crafty move on the screenwriters part, but once you get past that pseudo-ironic touch, this Saw is no more or less disturbing than any other in the series". 

==See also==
 
* 2009 in film

==References==
 
*  

==External links==
 
*  
*  
*  
*  
*   at Facebook

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 