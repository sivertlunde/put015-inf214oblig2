Big Mommas: Like Father, Like Son
{{Infobox film
| name           = Big Mommas: Like Father, Like Son
| image          = Big Mommas Poster.jpg
| alt            = An FBI agent disguised as a fat woman. His stepson holding a dress on a clothes hanger looking surprised.
| caption        = Theatrical release poster
| director       = John Whitesell
| producer       = David T. Friendly Michael Green
| screenplay     = Matthew Fogel
| story          = Matthew Fogel Don Rhymer
| based on       =  
| starring       = {{Plainlist| 
* Martin Lawrence
* Brandon T. Jackson
}} David Newman
| cinematography = Anthony B. Richmond
| editing        = Priscilla Nedd-Friendly
| studio         = {{Plainlist| 
* Regency Enterprises New Regency Friendly Films
* Runteldat Entertainment
* The Collective
}}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $32 million 
| gross          = $83,915,414 
}} FBI agent Malcolm Turner. Jascha Washington declined to reprise his role as Trent Pierce from the original film, and Brandon T. Jackson replaced him for his role.  The film was released on February 18, 2011 by 20th Century Fox.

==Plot== FBI agent Malcolm Turner (Martin Lawrence) is elated to learn that his stepson, Trent Pierce (Brandon T. Jackson), has been accepted to attend Duke University in Durham, North Carolina. However, Trent is uninterested and instead wants Malcolm to sign a recording contract for him since he is underage. When Malcolm refuses, Trents best friends encourage him to ambush Malcolm on the job in order to obtain the signature. Malcolm, in an attempt to capture Russian gang member Chirkoff (Tony Curran), uses an informant named Canetti to deliver a flash drive to the gang, while Trent attempts to ambush Malcolm on the job. Canetti (Max Casella) reveals that the flash drive is empty and a duplicate is hidden with a friend at the Georgia Girls School for the Arts.

During the exchange, Canettis cover is blown and he is killed, which Trent witnesses. Malcolm eventually rescues Trent and they escape, but since Trents car was left at the scene Malcolm knows the gang members will be able to track them down so Malcolm and Trent are forced to hide undercover. Malcolm once again becomes Sherrys grandmother, Big Momma, and also disguises Trent as an obese girl named "Charmaine", Big Mommas great-niece. Big Momma takes a job as a house mother at the Georgia Girls School for the Arts, while Charmaine is enrolled as a student. Surrounded by attractive young women, Trent nearly blows his cover, but manages to befriend a girl named Haley Robinson (Jessica Lucas). The headmistress announces that a historic music box has been stolen from the library, and Malcolm deduces that this music box contains the flash drive. While scoping out the library, Big Momma encounters security guard Kurtis Kool, who attempts to woo her while giving a tour. Seeing a picture of Kurtis with Canetti, Malcolm realizes that he is the friend, and tries to find out more about the music box.

Meanwhile, the gang members approach Trents best friends, posing as record producers, and encourage them to notify them of Trents whereabouts. Charmaine sets up a date between Haley and himself, though she doubts she will be interested in the seemingly egotistical "Prodi-G," Trents hip-hop alias. Trent reverts to his true self and the date goes well, but an encounter with Trents best friends causes the gang members to tail them.  Trent helps Haley perfect her musical performance for the upcoming "Showcase" event, turning it into a duet, and the two exchange a kiss at the end of the date, while Haley encourages Trent to pursue college. Before the gang members can capture Trent, he changes into his Charmaine disguise, throwing them off. Learning of an exchange between two students and Kurtis Kool, Big Momma attempts to flirt with Kurtis in order to apprehend him for stealing the music box, but the secret exchange ends up being the stolen gamecock from the Ignatius Boys School.

During this encounter, Malcolm reveals his true identity to Kurtis. After gaining the favor of several students by offering sage advice and comfort to them, Big Momma finally learns the music box was actually stolen by Haley as a hazing to become a full pledged member of the "Divas", a group of top artists in the school.  As she is about to perform her duet, Malcolm forces Trent to retain his Charmaine disguise, and he attempts to perform the duet with Haley as Charmaine, only to break disguise and ruin the performance.  As Haley storms off, the gang members arrive and a chase ensues. Trent accidentally draws their attention as he attempts to pursue Haley and explain himself. Just as he recovers the flash drive, the gang members catch up and hold him at gunpoint. Big Momma interferes and provides an escape, but all three are caught again and Malcolms disguise is revealed. Just as Chirkoff is about to kill them, Kurtis arrives with a taser and saves the day.

Trent and Haley reconcile, and Malcolm signs Trents record contract, only to have him tear it up and reveal his new plan to attend college. As the film ends, Malcolm and Trent make an agreement to keep the whole ordeal a secret from Sherry.

During the end credits, a music video is shown for Trents song, "Lyrical Miracle". It features Malcolm as Big Momma, Kurtis Kool, the girls and the teachers from the school, as well as the gang members.

==Cast==

* Martin Lawrence - Malcolm Turner/Big Momma
* Brandon T. Jackson - Trent Pierce/Charmaine Pierce
* Jessica Lucas - Haley Robinson
* Michelle Ang - Mia
* Portia Doubleday - Jasmine
* Tony Curran - Chirkoff
* Faizon Love - Kurtis Kool (uncredited)
* Ana Ortiz - Gail Fletcher
* Max Casella - Anthony Canetti

== Production ==
New Regency Productions spent $32 million to make the film, less than previous films in the series. They were able to reduce costs because Lawrence agreed to take a pay cut and thanks to tax incentives in Georgia (U.S. state)|Georgia.          Principal photography began in April 2010.   

==Reception==

=== Critical response    ===
Big Mommas: Like Father, Like Son was critically panned. Rotten Tomatoes gives the film a score of 5% based on 57 reviews, with an average rating of 2.6 out of 10.  Metacritic gives the film a "generally unfavorable" rating of 22% based on reviews from 14 critics. 

Mike Hale of The New York Times notes strong similarities to Some Like It Hot and describes Faizon Loves performance as the only honestly funny thing in the whole film. 

===Awards and nominations===
  
{| class="wikitable"
|-
!Award
!Category
!Subject
!Result
|- Golden Raspberry Award Golden Raspberry Worst Actress Martin Lawrence
| 
|- Golden Raspberry Worst Supporting Actress Brandon T. Jackson
| 
|- Golden Raspberry Worst Supporting Actor Ken Jeong
| 
|-
|}

=== Box office ===
The film was released in North America on February 18, 2011, ranking 5 that weekend, with a gross of $16,300,803 from 2,821 theaters.  As of 26 May 2011, Big Mommas has grossed  $37,915,414 in the US, and $44,770,652 elsewhere, for a worldwide total of $82,686,066. 

==Soundtrack  == One Chance & T-Pain. The song starts off with a verse by Jackson and then a verse from T-Pain then another verse by Jackson but this verse is rapped by his alterego, Charmaine from the movie. Another song called "Lyrical Miracle" by Brandon T. Jackson as he goes by the name of Trents rapper named Prodi-G and also features Martin Lawrence as his alter-ego, Big Momma, with other characters from the movie.

=== Songs featured in the film ===
* "Lyrical Miracle" Brandon T. Jackson ft. Alana D. and Marc John Jeffries (as Big Momma and Kurtis Kool)
* "Lyrical Miracle" (solo) Brandon T. Jackson
* "Lyrical Miracle" (end credits version) Brandon T. Jackson ft. Martin Lawrence (as Big Momma), Faizon Love as (Kurtis Kool), and Alana D.
* "Aint Nobody" Brandon T. Jackson ft. Jessica Lucas
* "Baby You Know" Brandon T. Jackson, Jessica Lucas ft. Alana D. One Chance ft. T-Pain

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 