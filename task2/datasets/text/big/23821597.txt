Cat Chaser
{{Infobox film
| name = Cat Chaser
| image = CatChaserDVD.jpg
| caption = DVD cover
| director = Abel Ferrara
| producer = Peter S. Davis William N. Panzer
| writer = Elmore Leonard James Borelli
| starring = Peter Weller Kelly McGillis
| narrator = Reni Santoni
| music = Chick Corea
| cinematography = Anthony B. Richmond
| editing = Anthony Redman
| distributor = Vestron Pictures
| released =  
| runtime = 90 min.
| country = United States English
| budget =
| gross =
}}
Cat Chaser is a 1989 film directed by Abel Ferrara and starring Peter Weller and Kelly McGillis, based on the novel of the same name by Elmore Leonard. It was adapted from the novel by Leonard and James Borelli.

Filming was not a happy experience for McGillis, who didnt make another major film afterwards for almost a decade. She said in 2001: "It was the most hateful experience of my life, and I said, if this is what acting is going to be, I will not do it. On the last day of shooting, I said to Abel, Are you done with me? He said, Yeah. I walked in my trailer and shaved my head. I said, Screw you, I never want to act again." Hasted, Nick (2001) " ", The Independent, 29 March 2001  Davis, Steven Paul (2001) The A-Z of Cult Films and Film-makers, Batsford, ISBN 978-0-7134-8704-6 

The film was released on VHS tape in the United States in 1991 by Vestron Video and the UK in 1994 by 4 Front and for the first time on DVD in 2003 by Lions Gate/Artisan, and issued in the UK in 2004 by Arrow Films. The Lions Gate DVD featured Weller and McGillis on the cover with the text "Passion. Greed. Murder. Tonight They Pay," with the story marketed as an erotic thriller.

==Plot== Dominican Republic intervention who now runs a small beachfront motel in Miami.

While searching for a Dominican woman named Luci Palma who saved his life in 1965 (and gave him the nickname "Cat Chaser"), he begins a relationship with Mary DeBoya, the wealthy, unhappy wife of a former Dominican general who continues to use sadistic methods to get what he wants.

Moran gets involved in a plot by his fellow military veteran Nolen Tyner and a former New York policeman, Jiggs Scully, to rip off the general. Moran must elude a number of double-crosses as he and Mary attempt to gain her freedom plus $2 million of the generals money.

The film was shot in Santo Domingo and Florida.

==Cast==
* Peter Weller as George Moran
* Kelly McGillis as Mary DeBoya
* Charles Durning as Jiggs Scully
* Frederic Forrest as Nolen Tyner
* Tomás Milián as Andres DeBoya
* Maria M. Ruperto as Luci Palma
* Juan Fernández de Alarcon as Rafi

==Critical evaluation==
The film received mixed reviews.  , 1 January 1989  Entertainment Weekly called the film "baroquely sleazy" and said that it failed to make sense. Burr, Ty (1996) " ", Entertainment Weekly, 31 May 1996  The Roanoke Times described the film: "Despite some serious flaws, Cat Chaser is one of the better screen adaptations of an Elmore Leonard novel".  Weller was criticized for his "stiff performance" by Mick Martin and Marsha Porter in The Video Movie Guide 1995. 

==Awards and nominations==
Cat Chaser was nominated for the Best Film award at Mystfest in 1989.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 