Kokowääh
{{Infobox film
| name           = Kokowääh
| image          = Kokowääh.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Til Schweiger
| producer       = Béla Jarzyk   Til Schweiger   Thomas Zickler
| writer         = Béla Jarzyk   Til Schweiger
| screenplay     = 
| story          = 
| based on       = 
| starring       = Til Schweiger   Emma Tiger Schweiger   Jasmin Gerat   Samuel Finzi
| music          = Dirk Reichdardt   Mirko Schaffer   Martin Todsharow
| cinematography = Christoph Wahl
| editing        = Constantin von Seld
| studio         = Barefoot Films
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 126 minutes
| country        = Germany
| language       = German
| budget         = €5,650,000  (estimated) 
| gross          = 
}} onomatopoetic depiction of the French pronunciation of coq au vin.    A sequel, Kokowääh 2, was released on February 7, 2013 with Schweiger having returned as director, co-writer and producer.

==Plot==
Kokowääh is set in Berlin and Potsdam, Brandenburg, Germany.    The plot concerns the travails of Henry (Til Schweiger), an established author of fiction, who must deal with the emergence of his 8-year-old natural daughter Magdalena (Emma Tiger Schweiger), the previously unknown product of a one-night-stand|one-night indiscretion in Stockholm, Sweden|Stockholm. {{cite news 
|first=Hannah |last=Pilarczyk |title=Til-Schweiger-Satire: Die Vorgeschichte von "Kokowääh" |work= ), with whom he is working on the adaptation. Little Magdalena, still in the state of shock, loves her foster father Tristan (Samuel Finzi) more than the biological one. Throughout the film, Henry and Magdalena build a close relationship, which he eventually describes in his script "Kokowääh" (referring to the French meal "Coq au vin").

== Cast ==
* Til Schweiger as Henry
* Emma Tiger Schweiger as Magdalena
* Jasmin Gerat as Katharina
* Samuel Finzi as Tristan
* Numan Acar as the worker
* Meret Becker as Charlotte
* Anne-Sophie Briest as the mother in the supermarket
* Anna Julia Kapfelsperger as Bine
* Friederike Kempter as the agent
* Torsten Künstler as the courier
* Miranda Leonhardt as Maria
* Misel Maticevic as Rob Kaufmann
* Genoveva Mayer as the woman in the bar
* Sönke Möhring as the policeman
* Jessica Richter as Esther
* Luna Schweiger as the daughter in the supermarket
* Katharina Thalbach as the patient
* Sanny van Heteren as Christiane
* Johann von Bülow as the firefighter
* Richard von Groeling as Rashid
* Jahmar Walker as the "Chap"
* Ulrich Wickert as the newscaster
* Birthe Wolter as the receptionist
* Fahri Ogün Yardım as the pizza guy

== Production ==
Kokowääh was filmed in Berlin and Potsdam, Brandenburg, Germany,  from 21 July to 13 September 2010. The budget was estimated to be €5,650,000.    The director and lead actor Til Schweiger and Béla Jarzyk, who also produced the film, wrote the script in a Turkish hotel in Berlin.   

== Critical reception == Golden Screen Award, which is given only to films that have been watched by more than 3 million viewers.    It was the most successful film in Germany in the first half of 2011.    The film itself received generally good to mixed reviews. Andreas Scheiner of the Die Zeit found the film "light and entertaining", though he added it "lacked depth".    Dieter Oßwald of the Programmkino.de praised the film as a "strong–point daddy–comedy".    Andrea Butz of the WDR2, however, criticized the film for "one–dimensional leaps and drawn characters".    Jan Füchtjohann of the Süddeutschen Zeitung also criticized Kokowääh, writing it showed "over long distances like a commercial for yogurt".   

== Other media ==

=== Music === German Singles Gold certification.    The music video for "Stay" has two versions, the regular one and the one that features parts from Kokowääh.

=== Home media ===
Kokowääh was released on both DVD and Blu-ray on 19 August 2011    on the iTunes    and the Amazon.com.   

== Release dates ==
{| class="sortable wikitable" 
|-
! Country
! Release date
! Title
! Notes
|-
|   Germany
| 3 February 2011
| Kokowääh
| 
|-
|   Austria
| 3 February 2011
| Kokowääh
| 
|-
|   Switzerland
| 3 February 2011
| Kokowääh
| 
|-
|   Luxembourg
| 5 February 2011
| Kokowääh
| 
|-
|   Kazakhstan
| 8 September 2011
| Kokowääh
| 
|-
|   Russia
| 8 September 2011
| Соблазнитель
| 
|-
|   Belarus
| 15 September 2011
| Kokowääh
|
|-
|   Taiwan 20 January 2012
| 紅酒燉香雞
| 
|-
|   Hungary
| 28 July 2012
| Kislány a küszöbön
| TV premiere
|-
|   Italy
| 1 September 2013
| Kokowääh
| TV premiere
|}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 