Winner (film)
 
{{Infobox film
| name           = Winner
| image          = "Winner,_2003".jpg
| image_size     =
| caption        =
| director       = Sundar C
| producer       = Suthan S. Ramachandran
| writer         = Boopathy Pandian (dialogue)
| screenplay     = Sundar C
| story          = Sundar C
| art director   = Nagu. R. K Prashanth Kiran Kiran Vadivelu Vijayakumar M. N. Nambiar Riyaz Khan
| music          = Yuvan Shankar Raja lyrics          = P. Vijayan   Viveka choreography     = Chinni Prakash   Brindha   Prabhu Shinuvas
| cinematography = Prasad Murella
| editing        = P. Sai Suresh
| distributor    =
| studio         = Mother India Movies International
| released       = 27 September 2003
| runtime        =
| country        = India Tamil
| budget         =
| preceded_by    =
| followed_by    =
}}
 Indian Tamil Tamil language Prashanth and Kiran in lead roles, while Vadivelu, Vijayakumar (actor)|Vijayakumar, M. N. Nambiar and Riyaz Khan among others play other supporting roles. The film, which has music scored by Yuvan Shankar Raja and camera work handled by Prasad Murella, released on 27 September 2003 and became a commercial success at the box office.Dubbed in hindi as Winner- The Real Hero.

==Plot==
Sakthi (Prashanth (actor)|Prashanth) studying in a college in the city & he gets into quarrels every other day. Not able to withstand the threats from hooligans, Sakthi’s mother (Jayamurali) & father (Vijayakumar (actor)|Vijayakumar) packs him up to their village where Sakthi’s grandfather Velayutham (M. N. Nambiar) & grandmother Sivagami (M.N.Rajam) are cultivating Paddy. In that village an innocent servant Kaipulla (Vadivelu) constantly challenged by Sakthi’s relative Kattadurai (Riyaz Khan). Sakthi visits the grandparents & decides to stay there. A marriage of their relative is planned & the whole family from all over Tamil Nadu assembles. Neelaveni (Kiran Rathod|Kiran) who accompanies her relatives falls in love with Sakthi. Kattadurai is supposed to marry Neelaveni. Many event follows. Kattadurai often clash with Sakthi & gets beaten up. Amidst the wedding preparation a group of former enemies (Rajkapoor, Anuradha) along with Neelaveni’s father (Prathap Singh) kidnap Neelaveni. Sakthi goes after them. After a huge tussle whether Sakthi brings back Neelaveni, Whether they unite or not is the rest of the story.

==Cast==
  Prashanth as Sakthi Kiran as Neelaveni
* Vadivelu as Kaipulla
* M. N. Nambiar as Velayutham Vijayakumar
* M. N. Rajam as Sivagami
* Riyaz Khan as Kattadurai Rajkapoor
* Prathap Singh 
* Vimalraj
* Santhana Bharathi
* Nirosha Anuradha
* Junior Balaiya
* Crane Manohar
* Vichu
* Bonda Mani
* Jayamurali
 

==Production==
Prashanth signed on to star in the film, being produced by R. Bhooma Ramachandran in late 2001, with Aarthi Aggarwal signed on the play the films heroine.  It was reported that she had impressed actress Kushboo with her performances in Telugu films and Kushboo thus receommended her to her film maker husband, Sundar C.  However she was later replaced by Kiran Rathod and filming for project began as early as January 2002. The film was primarily shot in Chennai and Pollachi, while scenes showcasing a wedding at Kirans house was shot at Vasan House, Chennai.   Prashanth was briefly hospitalised after injuring himself filming an action scene for the film.  

Despite being completed by the end of 2002, the release of the film was held up after Sundar C decided to prioritise his work on Kamal Haasans Anbe Sivam.

==Release==
The film opened to positive reviews in September 2003.   A critic noted "It is a typical Sundar C. brand entertainer with comedy, stunts, romance and sentiment thrown in to make the product racy", concluding it was set to be a "box office winner".  Another critic also noted "Vadivelu’s comedy is the highlight of the film. He has given a consistent performance in the film". 

==Soundtrack==
{{Infobox album|  
| Name = Winner
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover =
| Released = 17 March 2003
| Recorded = 2002 Feature film soundtrack
| Length =
| Label = New Music Classic Audio
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Pop Carn (2002)
| This album  = Winner (2003)
| Next album  = Kaadhal Kondein (2003)
}}

After Unakkaga Ellam Unakkaga (1998) and Rishi (2000), Sundar C. and Yuvan Shankar Raja worked together for the third time for the music composition of this film. The soundtrack, released on 17 March 2003, features 6 tracks with lyrics penned by Pa. Vijay and Viveka.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration 
|- 1 || "Eye Um Eye Um" || Devan || 4:23
|- 2 || "Endhan Uyir Thozhiyae" || Udit Narayan || 4:38
|- 3 || "Mathapoo" || Tippu (singer)|Tippu, Premji Amaran, Sri Ranjani || 3:59
|- 4 || "Mudhal Murai" || Srinivas (singer)|Srinivas, Mahalakshmi Iyer || 5:10
|- 5 || "Engirundhai" || Harish Raghavendra || 4:06 
|- 6 || "Kozhi Kokkarra" || Udit Narayan, Prashanthini || 4:25 
|}

==References==
 

 

 
 
 
 
 
 
 