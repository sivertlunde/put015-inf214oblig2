The General Line
 
{{Infobox film
| name           = The General Line
| image          = The General Line.jpg
| image_size     =
| caption        = Film poster
| director       = Grigori Aleksandrov Sergei M. Eisenstein
| producer       = 
| writer         = Grigori Aleksandrov Sergei M. Eisenstein
| narrator       = 
| starring       = Marfa Lapkina M. Ivanin Konstantin Vasilyev Vasili Buzenkov
| music          = 
| cinematography = Eduard Tisse
| editing        = 
| studio         = 
| distributor    = 
| released       = 1929
| runtime        = 121 minutes
| country        = Soviet Union
| language       = Silent film
| budget         = 
| gross          = 
}}
The General Line aka Old and New ( , Transliteration|translit.&nbsp;Staroye i novoye) is a 1929 Soviet film directed by Sergei Eisenstein.

The General Line was begun in 1927 as a celebration of the collectivization of agriculture, as championed by old-line Bolshevik  , in honour of the 10th anniversary of the Revolution. By the time he was able to return to this film, the Partys attitudes had changed and Trotsky had fallen from grace. As a result, the film was hastily re-edited and sent out in 1929 under a new title, The Old and the New. In later years, archivists restored The General Line to an approximation of Eisensteins original concept. Much of the directors montage-like imagery—such as using simple props to trace the progress from the agrarian customs of the 19th-century to the more mechanized procedures of the 20th—was common to both versions of the film.

== External links ==
*  
*  

 

 
 
 
 
 
 
 

 
 