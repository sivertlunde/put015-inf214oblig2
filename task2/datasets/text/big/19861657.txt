Get Him to the Greek
{{Infobox film
| name           = Get Him to the Greek
| image          = Get Him to the Greek.jpg
| caption        = Theatrical release poster
| director       = Nicholas Stoller
| producer       = Judd Apatow Joshua Blake Nicholas Stoller David Bushell Rodney Rothman
| writer         = Nicholas Stoller 
| screenplay     = 
| story          = 
| based on       = Characters by Jason Segel Diddy Colm Meaney
| music          = Lyle Workman
| cinematography = Robert Yeoman
| editing        = William Kerr Michael Sale
| studio         = Relativity Media Spyglass Entertainment Apatow Productions Universal Pictures
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $40 million 
| gross          = $91.3 million 
}} rock comedy film written, produced and directed by Nicholas Stoller and starring Russell Brand and Jonah Hill. The film was released on June 4, 2010. Get Him to the Greek is a spin-off sequel of Stollers 2008 film Forgetting Sarah Marshall, reuniting director Stoller with stars Hill and Brand. Brand reprises his role as character Aldous Snow from Forgetting Sarah Marshall, while Hill plays an entirely new character. The film also stars Elisabeth Moss, Rose Byrne, Colm Meaney and Sean Combs.

==Plot== Greek Theatre in Los Angeles on the tenth anniversary of his legendary performance there in 1999.

Sergio sends Aaron to London to escort Aldous to the performance. Before he leaves, Daphne informs Aaron that she has received a job offer in Seattle and that they are moving there, which leads to an argument resulting in an apparent break-up. After retrieving Aldous from his apartment, Aaron learns that Aldous had not been expecting him and that he had thought the concert was not for another two months. They then bar-hop across the city and Aaron futilely tries to get Aldous to catch one of many missed flights. Daphne calls Aaron to apologize for the fight, only to learn that Aaron believes that they are broken up. Throughout his partying, Aarons cellphone accidentally calls Daphne periodically, informing her of Aarons activities. Aaron and Aldous travel to New York City for Aldouss appearance on Today (NBC program)|Today. To keep Aldous sober for his performance, Aaron imbibes all of Aldouss whiskey and marijuana. Minutes before the performance, Aldous realizes he is unable to remember the lyrics to his most recent and unpopular song, "African Child", and replaces it with an older hit, "The Clap", to cheers and excitement from the audience.
 smuggle heroin depressed and Las Vegas rapes him. Neapolitan of drugs". Aaron panics and starts to have a bad trip, believing he is having a heart attack. Jonathan makes the trip worse by agreeing that Aaron is having a heart attack while Aldous attempts to calm Aaron down, primarily by the comforting sensation of stroking the furry walls of the hotel suite. Aldous fights with his father; Sergio (who is also high) jumps in the fight, and inadvertently sets the lounge on fire.

Aldous attempts to help Aaron by giving him an adrenaline shot and they run out of the hotel, chased by Sergio, who is hit by a car but comes out unharmed. Aldous and Aaron flee to Los Angeles, where Aaron convinces Aldous to visit Jackie Q. She has been sleeping with Metallicas drummer, Lars Ulrich (playing himself) and confesses that Naples is not actually his biological son, but instead is a photographers son. This makes him even more miserable. Meanwhile, Aaron goes to his home to apologize to Daphne. They are interrupted when Aldous arrives at their house and proposes that he, Aaron, and Daphne engage in a threesome; Daphne (who is mad at Aaron) agrees and Aaron hesitantly goes along. During the threesome, Aaron angrily decides to kiss Aldous, breaking it up. Daphne and Aaron both immediately regret the threesome, and Aaron angrily tells Aldous to go, criticizing Aldouss overall mental state. Instead of preparing for his show, Aldous goes to the rooftop of the Standard Hotel in downtown Los Angeles, and calls Aaron, threatening to jump. Aaron rushes to the hotel, and attempts to coax Aldous down from the roof.

Instead, Aldous jumps into a rooftop pool several floors down, accidentally breaking his arm. Aldous tells Aaron that he is lonely, sad and embarrassed, but Aaron reminds Aldous that thousands of fans love him and are waiting just to see him. Aldous decides to perform at the Greek Theatre despite his injury, even though Aaron pleads for him to go to the hospital. Upon their arrival, Sergio offers Aaron drugs to give to Aldous so he will not cancel the concert. Aaron, tired of the abuse Sergio has given him, refuses and quits his job on the spot, much to Sergios dismay. Aaron walks stage-side with Aldous, trying to convince him to go to the hospital instead. However, Aaron sees how happy Aldous is while performing and heads home to reconcile with Daphne. Months later in Seattle (where Aaron and Daphne have moved), Aldous, sober once again, has returned to fame with a single "Furry Walls" produced by Aaron (now his official producer) based on events from their night in Las Vegas, performing on the VH1 Storytellers program.

==Cast==
 
* Jonah Hill as Aaron Green, a record company employee
* Russell Brand as Aldous Snow, a free-spirited rock star with a faltering career who first appears in Forgetting Sarah Marshall
* Elisabeth Moss as Daphne Binks, Aarons girlfriend
* Rose Byrne as Jackie Q, a scandalous pop star, Aldouss on and off girlfriend and Napless mother
* Sean Combs as Sergio Roma, a record company owner and Aarons boss who assigns him to manage Aldous
* Colm Meaney as Jonathan Snow, Aldouss father
* Kali Hawk as Kali Coleman, one of Aarons co-workers
* Aziz Ansari as Matty Briggs, one of Aarons co-workers
* Nick Kroll as Kevin McLean, one of Aarons co-workers
* Ellie Kemper as one of Aarons co-workers
* Jake Johnson as Jazz Man, one of Aarons co-workers
* Karl Theobald as Aldouss assistant
* Carla Gallo as Destiny, a groupie
* T. J. Miller as Brian, a hotel clerk
* Lino Facioli as Naples, Jackie Q and Aldouss son, later revealed to have been fathered by a photographer Today Show production assistant Sarah Marshall, an actress and Aldouss former girlfriend
* Dinah Stabb as Lena Snow, Aldouss mother
* Lenny J. Widegren as the Infant Sorrow guitarist

===Cameo guest stars===
* Tom Felton
* Christina Aguilera Pink
* Kurt Loder (as Kurt F. Loder)
* Meredith Vieira
* Mario Lopez
* Pharrell Williams
* Paul Krugman
* Lars Ulrich
* Rick Schroder
* Zöe Salmon
* Katy Perry (deleted scenes)
* Dee Snider (uncredited)
* Holly Weber (uncredited)
* Billy Bush Rachel Roberts

Russell Brands friends Karl Theobald, Greg "Mr Gee" Sekweyama and Jamie Sives also appear in the film.

==Production==

===Development=== Greek Theatre."  In July 2008, Brand mentioned that he would be reprising his Aldous Snow role from Forgetting Sarah Marshall, in a new film from Apatow in which the character was back on drugs.     
In an interview with CHUD.com, Apatow would later reveal that Get Him to the Greek was indeed a Spin-off (media)|spin-off of Forgetting Sarah Marshall with Brand again playing a no-longer-sober Aldous Snow while in a different interview Nicholas Stoller said that Jonah Hill will play a different character named Aaron Green, a young music executive.      

===Filming===
 , London.]] Paramount studio lot preparing to present the 2008 MTV Video Music Awards, he approached Christina Aguilera, Pink (singer)|Pink, and Katy Perry about filming cameos for Get Him to the Greek.  
 Las Vegas, Los Angeles and London.  While filming in Trafalgar Square, Brand was pushed into a fountain by a passerby. {{Cite news
| url = http://www.dailymail.co.uk/tvshowbiz/article-1205180/Russell-Brand-pushed-fountain-passer-filming-new-film-London.html
| title = Thats not in the script: Russell Brand pushed into fountain by passer-by while filming movie
| newspaper = The Daily Mail
| first=Niall
| last=Firth
| date=August 8, 2009
}}
 

==Release==

===Critical reception===
 
The film has received generally positive reviews. Review aggregation website  , the film has a score of 65 out of 100, based on 39 critics, indicating "generally favorable reviews". {{cite web|url= http://www.metacritic.com/film/titles/gethimtothegreek|title= Get Him to the Greek (2010): Reviews|publisher= Amazon.com
| work = CBS|accessdate= 2010-05-05}} 

Roger Ebert of the Chicago Sun-Times gave the film three out of four stars by saying that "under the cover of slapstick, cheap laughs, raunchy humor, Gross out|gross-out physical comedy and sheer exploitation, Get Him to the Greek also is fundamentally a sound movie." 

===Box office===
In its opening weekend, the film debuted at #2 behind Shrek Forever After with a gross of $17,570,955.  The movie fell to fourth the following week with a weekend gross of $10,100,000.
In the UK, Get Him to the Greek opened at #1 grossing £1,569,556 in its first week before dropping to #2 the following week with takings of £1,048,898. It has collected $60,974,475 in the United States and Canada as well as $30,287,004 overseas bringing its worldwide total to $91,261,479. {{cite web
|url= http://www.boxofficemojo.com/movies/?id=gethimtothegreek.htm
|title=Get Him to the Greek Box Office Mojo
| publisher = Amazon.com
| work=boxofficemojo.com
| accessdate=2010-09-14 
}}
 

===Home media===
A 2-disc and 1-disc unrated and theatrical version of the film was released on DVD and Blu-ray Disc on September 28, 2010.

==Music==

===Soundtrack===
{{Infobox album  
| Name        = Get Him to the Greek: Soundtrack
| Type        = Soundtrack
| Artist      = Infant Sorrow
| Cover       = 
| Released    = June 1, 2010
| Recorded    =
| Genre       = Soundtrack
| Length      =
| Label       = Universal Republic
| Producer    = 
| Last album  =
| This album  = Get Him to the Greek: Soundtrack (2010)
| Next album  =
}}
{{Album ratings
| rev1      = allmusic
| rev1Score =    Pitchfork
| rev2Score = 5.5/10 
}}
Kim Garner, the senior vice president of marketing and artist development at Universal Republic Records, said that Brand and Universal Pictures "felt very strongly about doing something like this as opposed to a traditional soundtrack," and that they "wanted to release it like we would an actual rock bands album."   

{{Track listing
| headline       = Track listing
| total_length   = 46:10
| lyrics_credits = yes
| music_credits  = no
| extra_column   = Artist
| title1         = Just Say Yes
| length1        = 2:18
| lyrics1        = Jarvis Cocker, Jason Buckle, Tim McCall, Ross Orton
| extra1         = Infant Sorrow
| title2         = Gang of Lust
| length2        = 2:03
| lyrics2        = Carl Barat
| extra2         = Infant Sorrow
| title3         = Furry Walls
| length3        = 3:07
| lyrics3        = Dan Bern, Mike Viola, Judd Apatow
| extra3         = Infant Sorrow
| title4         = Going Up
| length4        = 4:06
| lyrics4        = Jason Segel, Lyle Workman
| extra4         = Infant Sorrow
| title5         = Bangers, Beans and Mash
| length5        = 3:32
| lyrics5        = Jason Segel, Lyle Workman
| extra5         = Infant Sorrow
| title6         = The Clap
| length6        = 2:44
| lyrics6        = Dan Bern, Mike Viola
| extra6         = Infant Sorrow
| title7         = I Am Jesus
| length7        = 2:39
| lyrics7        = Jason Segel, Lyle Workman
| extra7         = Infant Sorrow
| title8         = Riding Daphne
| length8        = 3:28
| lyrics8        = Jason Segel, Lyle Workman
| extra8         = Infant Sorrow
| title9         = F.O.H.
| length9        = 3:52
| lyrics9        = Jarvis Cocker, Chilly Gonzales
| extra9         = Infant Sorrow
| title10        = Yeah Yeah Oi Oi
| length10       = 2:52
| lyrics10       = Dan Bern, Mitch Marine
| extra10        = Infant Sorrow
| title11        = African Child (Trapped in Me)
| length11       = 3:06
| lyrics11       = Mike Viola
| extra11        = Infant Sorrow
| title12        = Little Bird
| length12       = 3:24
| lyrics12       = Mike Viola
| extra12        = Infant Sorrow
| title13        = Searching for a Father
| length13       = 3:43
| lyrics13       = Jason Segel, Lyle Workman
| extra13        = Infant Sorrow
| title14        = Supertight
| length14       = 2:37
| lyrics14       = Jason Segel, Lyle Workman, Russell Brand
| extra14        = Jackie Q featuring Aldous Snow
| title15        = Ring Round
| length15       = 2:25
| lyrics15       = Greg Kurstin, Inara George
| extra15        = Jackie Q
}}
{{Track listing
| headline       = Deluxe edition bonus tracks
| lyrics_credits = yes
| music_credits  = no
| extra_column   = Artist
| title16        = Jackie Q
| length16       = 3:41
| lyrics16       = Carl Barat
| extra16        = Infant Sorrow
| title17        = Pound Me in the Buttox
| length17       = 3:31
| lyrics17       = Paul Clarke, Matthew Dick
| extra17        = Jackie Q featuring Aldous Snow
| title18        = Chocolate Daddy
| length18       = 2:54
| lyrics18       = Lyle Workman, Rodney Rothman
| extra18        = Chocolate Daddy
| title19        = Fuck Your Shit Up
| length19       = 2:28
| lyrics19       = Lyle Workman
| extra19        = Jumbo Shrimp
}}
The following songs were featured in the film, but not included in the soundtrack: And Ghosted Pouts (Take the Veil Cerpin Taxt)" by The Mars Volta London Calling" by The Clash
# "Anarchy in the U.K." by The Sex Pistols
# "20th Century Boy" by T. Rex (band)|T.Rex
# "Rocks Off" by The Rolling Stones
# "Another Girl, Another Planet" by The Only Ones
# "Strict Machine" by Goldfrapp
# "Ghosts N Stuff" by Deadmau5 featuring Rob Swire
# "Personality Crisis" by The New York Dolls
# "Girls on the Dance Floor" by Far East Movement Heureux Tous Les Deux" by Franck Alamo 
# "Come on Eileen" by Dexys Midnight Runners
# "Cretin Hop" by The Ramones
# "Stop Drop and Roll" by Foxboro Hot Tubs Touch My Body" by Mariah Carey Mika
# "Fuck Me Im Famous" by DJ Dougal and Gammer What Planet Luciana
# "Inside of You" by Infant Sorrow (which was originally featured in Forgetting Sarah Marshall).
# "Licky feat. Princess Superstar (Herve Remix)" by Larry Tee

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 