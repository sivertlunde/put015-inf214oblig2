To Grab the Ring
{{Infobox Film
| name           = To Grab the Ring
| image          = 
| image size     = 
| caption        = 
| director       = Nikolai van der Heyde
| producer       = John Rosinga
| writer         = Nikolai van der Heyde Jan Blokker George Moorse
| narrator       = 
| starring       = Ben Carruthers
| music          = 
| cinematography = Gérard Vandenberg
| editing        = 
| distributor    = 
| released       = 14 March 1968
| runtime        = 98 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
| preceded by    = 
| followed by    = 
}}

To Grab the Ring is a 1968 Dutch film directed by Nikolai van der Heyde. It was entered into the 18th Berlin International Film Festival.   

==Cast==
* Ben Carruthers - Alfred Lowell, Acteur
* Françoise Brion - Hélène
* Liesbeth List - Sandra van Dijk
* Al Mancini - David Knight
* Vladek Sheybal - Mijnheer Smith
* Edina Ronay - Vriendin
* Dunja Rajter
* John Van de Rest
* Jan Vreeken - Gangster
* Joop Admiraal
* Cox Habbema
* Melvin Clay - Anush
* Ko Koedijk
* Simon van Collem

==See also==
*List of Dutch films

==References==
 

==External links==
* 

 
 
 
 
 
 