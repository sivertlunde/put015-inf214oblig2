Oil Sands Karaoke
{{Infobox film
| name           = Oil Sands Karaoke
| image          = "Oil_Sands_Karaoke"_official_movie_poster.jpg
| caption        = Poster
| director       = Charles Wilkinson
| producer       = Tina Schliessler Charles Wilkinson Kevin Eastwood Murray Battle
| writer         =
| narrator       =
| music          = Various
| cinematography = Charles Wilkinson
| editing        = Tina Schliessler  Charles Wilkinson
| studio         = Shore Films Inc.
| distributor    = Indiecan Entertainment  Knowledge Network
| released       =  
| runtime        = 82 minutes
| country        = Canada
| language       = English
| website        = http://osk.knowledge.ca/
| website_title  = Official site
}}
 Charles Wilkinson. The film follows five people working in or around the infamous oil sands of Northern Alberta as they compete in a karaoke contest held at local watering hole Baileys Pub. The film was produced by Wilkinson and Tina Schliessler, and executive produced by Kevin Eastwood and Knowledge Networks Murray Battle.

== Summary ==

Albertas oil sands project has been called the single most destructive industrial project in human history.  Oil Sands Karaoke focuses on the people who live and work around the oil sands project, whose voices are rarely heard in the debates about the economic value and environmental cost of the project.

As the five featured workers progress through the stages of the karaoke contest, singing pop favourites from Creedence Clearwater Revival to Britney Spears, they explain through interviews how and why they came to work in the oil industry, and how it has impacted their lives. Some are dismissive of the opinions of "environmentalists". Others say they are aware of the destructive impact that the oil industry has on the environment, but without the lucrative salaries available from the oil companies, their debts and financial obligations would be too much to bear. All feel that singing helps relieve the stress of long hours working heavy equipment, or the loneliness of living in a remote industrial town.

Director Wilkinson believes the vicious tone of the debate over the oil sands has precluded any meaningful discussion. "Theres so much shouting that we dont talk," he says. "...there are human beings involved. We need to talk about this stuff, but we should maybe try to be a bit more polite with each other."   

== Featured characters ==

Dan Debrabandre works as a haul truck driver. The mining truck that he drives, a Caterpillar 797, is one of the biggest motorized vehicles ever invented and can hold up to 500 tons of bitumen-laced dirt. Prior to working in the oil sands, Dan pursued a career as a country singer, but family and financial obligations made him give up on his recording dreams. Dan says he is grateful for all the financial and emotional support his friends and family gave him while he worked on his music, and part of the reason he works a lucrative oil sands job is to pay them back.

Brandy Willier is another haul truck operator from idyllic High Prairie, Alberta. When Brandy was young, she lived on a first nations reserve with her parents,  developing a love of singing from listening to her father play guitar. However, Brandys father died when she was only seven which began a long period of struggle and instability for her and her mother. She credits her truck driving job with giving her life stability, although she finds Fort McMurray to be a lonely place.
 Purple Rain". dissociative post-traumatic stress disorder, which he suffered due to violence and abuse in his childhood and teens. Massey believes he was the first gay man to come out in blue-collar Fort McMurray, and his company now sponsors multiple charitable programs, including a drag show, to combat bullying and homophobia. 

Chad Ellis works in the Suncor plant as a scaffolder. A talented entertainer, he dreams of becoming a full-time singer but realizes that he has to make a secure living somehow. As a child, Chads religious family prompted him to sing in their church choir. When he was older, he recorded in a basement studio, and eventually began performing opening acts for singers such as Tone Loc. He stopped performing and moved to Fort McMurray after a violent conflict with a partners ex-boyfriend. He  now sings in the local karaoke scene for fun.

Jason Sauchuk is a soft-spoken fan of videogames who trained as a computer programmer, but joined his father in the heavy equipment industry after school. While working with his father, he incurred debt  supporting a girlfriend with children. His partner turned out to be unfaithful and they separated, and company layoffs cost him his previous job. Now he works as a haul truck driver, a job lucrative enough to help him dig himself out of debt.

==Release==

Oil Sands Karaoke had its world premiere on April 26, 2013 at Hot Docs Canadian International Documentary Festival in Toronto.  The Western Canadian premiere took place at The Vancouver International Film Festival on October 4, 2013.
 

The film also screened at the Calgary International Film Festival,  Yorkton Film Festival, Fort Lauderdale International Film Festival, and Available Light Film Festival and had a limited theatrical run in Fort McMurray,  Vancouver,  Toronto, Ottawa, Winnipeg, and Regina.

Oil Sands Karaoke had its television broadcast premiere on May 6, 2014 on Knowledge Network.  The film had its Ontario broadcast premiere on January 21, 2015 on TVOntario. 

== Critical reception ==

The film was received favourably by several notable documentary critics:

Marsha Lederman of the Globe and Mail wrote: "Poignant and beautifully shot, the film takes pains not to judge as it paints a dignified portraits of the boom town and the workers".   
Katherine Monk wrote in the Vancouver Sun that it has “All the suspense of The Voice, all the emotional conflict of American Idol and all the beauty of The Road.” 
Greg Klymkiw named it one of his Hot Docs Hot Picks for 2013, and called it "a thing of genuine beauty...quite unlike any documentary about the environment that youll ever see." 
Nicholas Gergesha of Point of View Magazine wrote "...filmmakers Charles Wilkinson and Tina Schliessler generate a meaningful and multifaceted discussion about the environment, the economy and the often-ignored human element in a destructive industry." 
The Huffington Posts Sarah Kurchak lauds Wilkinson as "a filmmaker dedicated to pushing past all of the misinformation and rhetoric surrounding environmental issues and getting to the heart of the matter..." 

==Awards==

At the 2014 Yorkton Film Festival, the film received two Golden Sheaf Awards: Best Documentary (Science/Nature/Technology), and Best Director (Non-Fiction). 

== References ==

 

== External links ==

*  
*  
* Official Site:   at Knowledge Network
*  

 
 
 
 
 
 
 
 
 
 