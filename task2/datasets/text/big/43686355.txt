Vacation (2015 film)
{{Infobox film
| name           = Vacation
| image          = 
| alt            =
| caption        =  Jonathan Goldstein David Dobkin Chris Bender
| writer         = John Francis Daley   Jonathan Goldstein
| based on       =  
| starring       = Ed Helms   Christina Applegate   Leslie Mann Chris Hemsworth   Charlie Day Beverly DAngelo Chevy Chase Keegan-Michael Key Regina Hall Skyler Gisondo Steele Stebbins Elizabeth Gillies
| music          = Mark Mothersbaugh 
| cinematography = Barry Peterson
| editing        =  
| studio         = New Line Cinema Big Kid Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
 adventure comedy Jonathan Goldstein David Dobkin. National Lampoons Vacation series. The film stars Ed Helms, Christina Applegate, Chris Hemsworth, and Charlie Day. The film is scheduled to be released by Warner Bros. on July 31, 2015.

== Synopsis ==
Following in the footsteps of his father Clark (Chevy Chase) and hoping for some much-needed family bonding, a grown-up Rusty Griswold (Ed Helms) surprises his wife, Debbie (Christina Applegate), and their two sons, James (Skyler Gisondo) and Kevin (Steele Stebbins), with a cross-country trip back to Americas favorite family fun park: Walley World, as it will be closing forever. 

== Cast ==
* Ed Helms as Russell "Rusty" Griswold
* Christina Applegate as Debbie Griswold, Rustys wife
* Leslie Mann as Audrey Griswold-Crandall
* Chris Hemsworth as Stone Crandall, an up-and-coming anchorman and Audreys husband
* Charlie Day as a river-rafting guide
* Beverly DAngelo as Ellen Griswold
* Chevy Chase as Clark Griswold
* Keegan-Michael Key  and Regina Hall  as the heads of an idyllic family that is friends with the Griswolds
* Skyler Gisondo as James Griswold, Rusty and Debbies older son
* Steele Stebbins as Kevin Griswold, Rusty and Debbies younger son
* Elizabeth Gillies  as Heather
* Tim Heidecker 
* Nick Kroll 
* Kaitlin Olson 
* Michael Peña 

==Production== David Dobkin Jonathan Goldstein, the story was to focus on Rusty Griswold as he took his own family to Wally World before the theme park permanently closes. 

In July 2012, it was announced that   and Charlie Day will also star.  Skyler Gisondo and Steele Stebbins will star as Rusty Griswolds sons along with Helms and Christina Applegate.  On September 15, Leslie Mann joined the film to play the role of Rustys sister Audrey Griswold.    On September 29, Keegan-Michael Key and Regina Hall were set to play family friends of the Griswolds in the film.    On October 10, director Francis Daley revealed in an interview that he might do a cameo with Samm Levine and Martin Starr, which would be a reunion of hit comedy show Freaks and Geeks, though it was not confirmed.  On November 12, four actors joined to play Four Corners cops which include Tim Heidecker, Nick Kroll, Kaitlin Olson and Michael Peña.   

===Filming===
Filming began on September 16, 2014, in Atlanta, Georgia (U.S. state)|Georgia.   On September 16, scenes were filmed on location in Olympic Flame Restaurant.    Scenes were shot around Piedmont and 6th Aves from October 6–8, which included the Shellmont Inn.   On October 22, 2014, scenes were filmed at the U.S. National Whitewater Center in Charlotte, North Carolina. 

== Release ==
On October 6, 2014, Warner Bros. moved the films release date from November 13 to October 9, 2015, and officially titled as Vacation.  On March 30, 2015, Warner Bros. moved the films release date from October 9 to July 31, 2015. 

== References ==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 