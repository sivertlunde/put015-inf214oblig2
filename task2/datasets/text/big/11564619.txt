The Brass Bottle (1964 film)
{{Infobox Film
| name           = The Brass Bottle
| image          = Brassbottleposter.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Harry Keller Robert Arthur
| writer         = Thomas Anstey Guthrie|F. Anstey (novel) Oscar Brodney
| narrator       = 
| starring       = Tony Randall Burl Ives Barbara Eden
| music          = Bernard Green
| cinematography = Clifford Stine
| editing        = Milton Carruth Ted J. Kent Universal Pictures
| released       = May 20, 1964
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

The Brass Bottle is a 1964 American fantasy film about a modern man who accidentally acquires the friendship of a long-out-of-circulation jinn|djinn. 

The film starred Tony Randall, Burl Ives and Barbara Eden. Edens role was instrumental in getting her cast as the star of the TV series I Dream of Jeannie, even though she did not play a djinn in this film.

==Plot==
Architect Harold Ventimore (Tony Randall) buys a large antique container that turns out to imprison a djinn named Fakrash (Burl Ives), whom Harold inadvertently sets free. Fakrash is effusively grateful for his release, and persistently tries to do favors for Harold to show his gratitude. However he has been in the brass bottle for a long time, and Fakrash’s unfamiliarity with the modern world causes all sorts of problems when he tries to please his rescuer. Harold ends up in a great deal of trouble, including with his girlfriend, Sylvia Kenton (Barbara Eden).

== Jinn and genie ==
Though the word jinn is commonly translated into English as "genie", author Thomas Anstey Guthrie|F. Anstey points out the distinction in the novel The Brass Bottle (originally published in 1900) which provides the basis of the film.

==Cast==
*Tony Randall as Harold Ventimore
*Burl Ives as Fakrash
*Barbara Eden as Sylvia Kenton Kamala Devi as Tezra, a female djinn
*Edward Andrews as Professor Kenton
*Lulu Porter as a belly dancer
*Richard Erdman as Seymour Jenks
*Kathie Browne as Hazel Jenks
*Ann Doran as Martha Kenton
*Philip Ober as William Beevor
*Parley Baer as Samuel Wackerbath
*Howard Smith as Senator Grindle

==Reception==
The New York Times critic A. H. Weiler dismissed it as "one of the duller fantasies dreamed up by Hollywoods necromancers." 

== Home media ==
The Brass Bottle was released on DVD for Region 1 (U.S. and Canada only) as part of the Universal Vault Series in January 2010. 

==Other versions==
Two prior versions of Ansteys novel were filmed. Both were silent and bore the same name. They were released in 1914 and The Brass Bottle (1923 film)|1923.
 Tamil by Javar Sitaraman as Pattanathil Bhootham or Ghost in the City in 1964.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 
 