Hindustan the Mother
{{Infobox film
| name           = Hindustan the Mother 
| image          = 
| writer         =G. Hari Babu Duriki Mohana Rao Naresh Bhanupriya Master Sai Subhakar Jeeba
| director       =G. Haribabu
| producer       =G. Haribabu
| cinematography =Hari Anumolu
| distributor    =
| editing        = Murali Ramaiah
| music          =M. M. Keeravani
| country        = India
| released       = 2000 Telugu
| budget         = 
}} Telugu political drama film in the backdrop of terrorism. The film was directed by G. Hari Babu. Upon release, the film has received many positive reviews, and was premiered in the Indian Panorama section at the International Film Festival of India, and at the BFI London Film Festival. 
  
The film was also premiered at the 2001 Mumbai Film Festival.
 
The film has also garnered two state Nandi Awards.
 

==Awards==
;Nandi Awards
*Sarojini Devi Award for a Film on National Integration (2000) - G. Haribabu
*Nandi Award for Best Child Actor (2000) - Master Subhakar. 

==References==
 

 
 
 
 
 
 
 

 