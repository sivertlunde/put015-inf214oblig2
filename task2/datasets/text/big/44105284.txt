Little Iodine (film)
{{Infobox film 
| name           = Little Iodine
| image          = Little Iodine poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Reginald Le Borg Buddy Rogers
| screenplay     = Richard H. Landau
| based on       =  
| starring       = Ann Marlowe Marc Cramer Eve Whitney Irene Ryan Hobart Cavanaugh
| music          = Alexander Steinert 
| cinematography = Robert Pittack 	
| editing        = Lynn Harrison 
| studio         = Comet Productions
| distributor    = United Artists
| released       =  
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Little Iodine is a 1946 American comedy film directed by Reginald Le Borg and written by Richard H. Landau. The film is based on the comic strip Little Iodine by Jimmy Hatlo. The film stars Jo Ann Marlowe, Marc Cramer, Eve Whitney, Irene Ryan and Hobart Cavanaugh. The film was released on October 20, 1946, by United Artists.  

==Plot==
Little Iodine (Marlowe) stays true to her comic strip nature in this film, where she does her best to break up the marriage of her parents (Cavanaugh and Ryan), ruin a romance between Janis and Marc (Whitney and Cramer), and cost her father his job. Unlike her comic-based character, however, Iodine has a change of heart and sets out to right the wrongs.

== Cast ==
*Jo Ann Marlowe as Little Iodine
*Marc Cramer as Marc Andrews
*Eve Whitney as Janis Payne
*Irene Ryan as Mrs. Tremble
*Hobart Cavanaugh as Mr. Tremble
*Lanny Rees as Horace
*Leon Belasco as Simkins
*Emory Parnell as Mr. Bigdome
*Sarah Selby as Mrs. Bigdome
*Jean Tartriquin as Grandma Jones

==See also==
*List of lost films

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 