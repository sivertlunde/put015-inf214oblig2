The First Men in the Moon (1919 film)
{{Infobox film
| name = The First Men in the Moon
| image =
| caption = Bruce Gordon J. L. V. Leigh
| producer =
| writer = R. Byron Webber  H.G. Wells (novel) Bruce Gordon Heather Thatcher Lionel dAragon
| cinematography =
| editing =
| distributor = Gaumont British (UK)
| released =  
| runtime =
| language = English intertitles
| country = United Kingdom
| budget =
}} Bruce Gordon and J. L. V. Leigh. The film is based on H. G. Wells science fiction novel The First Men in the Moon (1901). There have since been many other adaptations of the original story on the big screen, radio and video.

As of August 2010, the film is not held in the  , ISBN 978-1926837-31-4- see   (retrieved May 5 2014).  

==Plot==
The synopsis from The Bioscope trade paper of 5 June 1919 reads as follows:
 
In the company of Rupert Bedford, a grasping speculator, Samson Cavor, an elderly inventor-scientist, ascends to the Moon in a sphere coated with Cavorite, a substance which has the property of neutralizing the law of gravity.  After strange adventures with the Selenites (the inhabitants of the Moon), Bedford villainously deserts the professor and returns to Earth alone in order to make a fortune for himself out of Cavorite. By means of wireless telegraphy, however, Hogben, a young engineer in love with Cavors niece, Susan, succeeds in getting in touch with the stranded inventor, who denounces Bedford and states that he has been amicably received by the Grand Lunar, overlord of the Selenites.  Susan thereupon indignantly rejects the proposals of Bedford, who has represented it as Cavors last wish that she should marry him, and, instead, accepts Hogben as her husband. 
 

==Notability==
Robert Godwin credits the film as "the first movie to ever be based entirely on a famous science fiction novel." 

==Cast== Bruce Gordon as Hogben 
* Heather Thatcher as Susan
* Hector Abbas as Sampson Cavor
* Lionel dAragon as Rupert Bedford
* Cecil Morton York as Grand Lunar

==See also==
*List of lost films

==References==
 

==External links==
* , with extensive notes
* 

 
 

 

 
 
 
 
 
 
 
 
 
 