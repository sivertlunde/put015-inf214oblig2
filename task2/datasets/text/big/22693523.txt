Marine Boy (film)
{{Infobox film
| name           = Marine Boy
| image          = Marine Boy film poster.jpg
| caption        = Theatrical poster
| director       = Yoon Jong-seok
| producer       = Won Dong-yeon Kim Ho-seong Lee Sang-yong
| writer         = Yoon Jong-seok
| starring       = Kim Kang-woo Cho Jae-hyun Park Si-yeon
| music          = Jeong Jae-il
| cinematography = Kim Ji-yong
| editing        = Steve M. Choe  Kim Chang-ju
| studio         = REALies pictures
| distributor    = CJ Entertainment
| released       =  
| runtime        = 118 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =  
}} 2009 South mule by a gangster boss, smuggling drugs by sea. Yoon regarded the films title as a unique point, saying, "There is something beguiling about its duality, how these criminals are called something that romantically calls to mind the popular cartoon Marine Boy or the star swimmer Park Tae-Hwan". 

== Plot == mule who smuggles drugs across the open waters.

Knowing the danger he faces, Cheon-soo tries to make a run for it, but he is arrested at the airport by police detective Kim Gae-ko. Kim is intent on capturing Kang, and faced with no other choice, Cheon-soo agrees to work as a spy on his behalf. His situation becomes further complicated when he falls for Yu-ri, a jazz singer under Kangs charge and the daughter of Kangs best friend. But Yu-ri suspects Kang of killing her father, and she and Cheon-soo plot to take the drug money for themselves.

== Cast ==
* Kim Kang-woo ... Cheon-soo
* Cho Jae-hyun ... Company president Kang
* Park Si-yeon ... Yu-ri
* Lee Won-jong ... Detective Kim Gae-ko
* Eom Hyo-seob ... Section chief Jo
* Oh Dae-hwan ... Coach Lee
* Choi Soo-rin ... Jazz bar owner
* Yoo Ji-yeon ... House madame
* Park Byeong-eun ... Ji-ho
* Im Hyeong-gook ... Choi Do-wook
* Jo Jae-yoon ... Department head Im
* Kim Joon-bae ... Company president Hwang
* Oh Kwang-rok ... Doctor Park
* Choi Jung-woo ... Gambling man
* Hakuryu ... Suzuki

== Production ==
Marine Boy was the debut feature film by writer and director Yoon Jong-seok. Yoon said that he first began thinking about the story after watching a "very moving  . Kim went on to film dangerous water sequences without using a stunt double, earning praise from his fellow cast members and the production crew. Yang Sung-jin. " ". The Korea Herald, 6 January 2009. Retrieved on 6 May 2009.  Lee Hyo-won. " ". The Korea Times, 6 January 2009. Retrieved on 6 May 2009.  For her role as a jazz singer, Park Si-yeon received vocal training to improve her performance. " ". HanCinema, 22 May 2008. Retrieved on 6 May 2009. 
 Cebu Island in the Philippines.  The films production went through much trial and error due to the crews lack of experience of filming at sea, and both crew members and actors suffered severe seasickness during filming.  From the initial draft of the script in 2004 to the end of production, Marine Boy took more than four years to complete. 

== Release == topped the The Scam and Handphone (film)|Handphone—during the same month, placing the three films in competition with each other.  Distribution rights for the film in Turkey were sold at the 59th Berlin International Film Festival,  and it was one of three Korean films to be screened at the 12th Spring Showcase of the Hawaii International Film Festival. 

== Critical response ==
Lee Hyo-won of The Korea Times was critical of Marine Boy for feeling "too Hollywood at times, with expensive cars crashing and featuring straight-to-bed romances", and noted that the films middle section felt slow after a "fast paced and highly engrossing" beginning. However, Lee praised Cho Jae-hyun for portraying Kang "with ferocious naturalness and surprising tenderness", and credited supporting cast members Lee Won-jong and Oh Kwang-rok for giving the film substance. 

== References ==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 