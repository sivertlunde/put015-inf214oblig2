Psycho Cop
 
{{Infobox film
| name           = Psycho Cop
| image          = PsychoCop.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = VHS cover
| director       = Wallace Potts
| producer       = Jessica Rains   Cassian Elwes
| writer         = Wallace Potts
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Jeff Qualle   Linda West   Cindy Guyer   Dan Campbell   Robert R. Shafer   Palmer Lee Todd   Greg Joujon-Roche
| music          = Jeff Walker   Alex Parker   Keyth Pisani
| cinematography = Mark Walton
| editing        = Ian McVey
| studio         = Smoking Gun
| distributor    = Southgate Entertainment
| released       =  
| runtime        = 87 minutes
| country        =  
| language       = English
| budget         = 
| gross          =
}}
 1989 horror film starring Robert R. Shafer as Police Officer Joe Vickers. The film is very similar to Maniac Cop, though the titular character is not a mutilated monster, unlike Maniac Cop. Also like the Maniac Cop series this film was criticized by the Police Interest Group for what they considered an unrealistic portrayal of police. The film spawned a sequel, Psycho Cop 2, in 1993. The film is currently unavailable on DVD in Region 1.

== Cast ==

* Robert R. Shafer - Greg Henley/Ted Warnicky/Officer Joe Vickers
* Jeff Qualle - Doug
* Palmer Lee Todd - Laura
* Dan Campbell - Eric
* Cindy Guyer - Julie
* Linda West - Sarah
* Greg Joujon-Roche - Zack
* Bruce Melena - Officer Bradley (Cop #1)
* Glenn Steelman - Officer Chris (Cop #2)
* Julie Araskog - Dead Woman
* Denise Hartman - Barbara and Cop #3
* David L. Zeisler - Greg and Cop #4

== External links ==

*  

 
 
 
 
 
 
 

 