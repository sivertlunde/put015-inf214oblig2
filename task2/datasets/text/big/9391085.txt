High School Musical 3: Senior Year
 
{{Infobox film
| name = High School Musical 3: Senior Year
| image = HSM 3 Poster.JPG
| image_size = 215px
| alt = The six main cast members do their signature jump, this time in prom outfits and graduation gowns
| caption = Theatrical release poster
| director = Kenny Ortega
| producer = Bill Borden Barry Rosenbush
| writer = Peter Barsocchini
| based on =  
| starring = Zac Efron Vanessa Hudgens Ashley Tisdale Lucas Grabeel Corbin Bleu Monique Coleman David Lawrence
| cinematography = Daniel Aranyò
| editing = Lauren Flaum
| studio = Walt Disney Pictures Borden and Rosenbush Entertainment  Walt Disney Studios Motion Pictures
| released = October 24, 2009
| runtime = 111 minutes   120 minutes (DVD Special Edition)
| country = United States
| language = English
| budget = $11 million    
| gross = $252.9 million 
}} teen Romantic romantic comedy musical film and the third installment in the High School Musical (film series)|High School Musical series. Produced and released on October 24, 2009 by Walt Disney Pictures, it was the only film in the series to be released theatrically. Kenny Ortega returned as director and choreographer, as did all six primary actors.

The sequel follows the main six high school seniors: Troy, Gabriella, Sharpay, Ryan, Chad, and Taylor as they are faced with the challenging prospect of being separated after graduating from high school. Joined by the rest of their East High Wildcat classmates, they stage an elaborate spring musical reflecting their experiences, hopes, and fears about the future.

The film received positive reviews, relatively better than the first installment of the series, and, in its first three days of release, High School Musical 3: Senior Year grossed $50 million in North America and an additional $40 million overseas, setting a new record for the largest opening weekend for a musical film.

==Plot==
 Now or Right Here, Right Now").
 exchange student I Want It All").
 the first A Night to Remember"). The next day, Ryan walks in on Kelsi composing in the music room and they start to sing ("Just Wanna Be with You") which leads to Ryan asking Kelsi to prom. While Troy and Chad reminisce about their past ("The Boys Are Back"), Sharpay and Tiara discover that Gabriella has a chance to go to college early to Stanford and Sharpay convinces Troy that he is the only thing keeping Gabriella from her dream. Troy talks to Gabriella about this and after sharing an awkward goodnight, Gabriella sings ("Walk Away") and leaves for college the next day.
 Albuquerque for prom or graduation. However, on the day of the prom, Troy visits Gabriella at Stanford University and convinces her to return and they kiss during the lunch break (Can I Have This Dance? (Reprise)"). Back at East High, Sharpay is prepared for the last musical at East High and Troys fellow basketball player Jimmie receives a text from Troy to tell him to cover for him onstage because he is going to be late. The Juilliard representatives are there, and watch as the show seems to go well.

Kelsi and Ryan start out the show followed by a couple other numbers; Jimmie then performs with Sharpay and embarrasses her, although the audience applauds the performance. Troy and Gabriella appear during the second half of the show and sing their duet together. Tiara then betrays Sharpay and tells her how she is going to take over next year in the drama department. Sharpay finally learns how it feels to be manipulated and humiliated, but nevertheless does not wish to go down. While Tiara performs, Sharpay immediately crashes her performance and upstages her ("Senior Year Spring Musical").

At the end of the musical, Ms. Darbus reveals that both Kelsi and Ryan have won the Juilliard scholarship and tells about everyones future plans, in which Troy reveals hes chosen Berkeley so he can be close to Gabriella where he can play basketball and perform in theater ("Were All in This Together ("Graduation Mix").

At the graduation ceremony, Troy gives the class speech after being selected by Ms. Darbus. Throwing their caps in the air, the graduates form a giant Wildcat before breaking out into song and dance (High School Musical). As the film ends, the graduates run across the field, the curtain falls, and they appear on the East High stage. The main cast then perform the High School Musical song, before jumping and bowing when the curtain falls.

==Cast==
:See List of characters from High School Musical (film series)|High School Musical characters
Main Characters:
* Zac Efron as Troy Bolton, the basketball team captain and boyfriend of Gabriella.
* Vanessa Hudgens as Gabriella Montez, girlfriend of Troy.
* Ashley Tisdale as Sharpay Evans, a glamorous rich teen.
* Lucas Grabeel as Ryan Evans, Sharpays choreographer brother.
* Corbin Bleu as Chad Danforth, Troys right-hand man.
* Monique Coleman as Taylor McKessie, Chads love interest.

Side Characters:
* Olesya Rulin as Kelsi Nielsen, Ryans love interest.
* Chris Warren, Jr. as Zeke Baylor, Sharpays love interest.
* Ryne Sanborn as Jason Cross, Marthas love interest.
* Kaycee Stroh as Martha Cox, Jasons love interest.
* Alyson Reed as Ms. Darbus
* Bart Johnson as Coach Jack Bolton
* Jemma McKenzie-Brown as Tiara Gold
* Manly "Little Pickles" Ortega as Boi Evans
* Matt Prokop as Jimmy "Rocket Man" Zara
* Justin Martin as Donnie Dion Leslie Wing Pomeroy as Lucille Bolton
* Socorro Herrera as Mrs. Montez
* Robert Curtis Brown as Vance Evans
* Jessica Tuck as Darby Evans
* David Reivers as Charlie Danforth
* Yolanda Wood as Mrs. Danforth
* Joey Miyashima as Principal Matsui

==Musical numbers==
 

{| class="wikitable" style="text-align:center;"
|-
! style="width:195px;"| Song || style="width:285px;"| Lead Singers || style="width:250px;"| Scene || style="width:310px;"| Notes
|- Now or Never" || Troy, Gabriella, Chad, Zeke, Jason, Martha, and Cheerleaders || East High gym || The championship game 
|- Right Here, Right Now" || Troy and Gabriella || Troys Treehouse || After-party at Bolton residence 
|- I Want It All" || Sharpay and Ryan || East High cafeteria || Sharpay and Ryans fantasy sequence
|-
| "Can I Have This Dance?" (Verse 1 + Bridge) || Troy and Gabriella || Rooftop garden of East High || Gabriella teaches Troy the waltz 
|- A Night to Remember" || Troy, Chad, Gabriella, Jason, Zeke, Taylor, Martha, Kelsi, Sharpay, Ryan || East High Auditorium || Rehearsal for musical number 
|-
| "Just Wanna Be with You" || Ryan and Kelsi; Troy and Gabriella || East High Auditorium/East High Music Room || A declaration of friendship that survives against all odds 
|-
| "The Boys Are Back" || Troy and Chad || Rileys Auto Salvage Junkyard || Troy and Chad reliving childhood memories/finding out what they want to be in life
|-
| "Right Here, Right Now" (Reprise) || Troy and Gabriella || Gabriellas house/Troys treehouse|| Emotional reprise of "Right Here, Right Now"; Troy and Gabriella worrying about how far away theyll be from each other (Only on extended edition)
|-
| "Walk Away" || Gabriella || Gabriellas house || Gabriellas move to Stanford University
|-
| "Scream" || Troy || Throughout East High || Troys confusion about his future after Gabriella leaves
|-
| "Can I Have This Dance?" (Verse 2) || Troy and Gabriella || Stanford University/East High Auditorium || Troy convinces Gabriella to come back for the musical and graduation
|-
| "Senior Year Spring Musical" || Kelsi, Ryan, Chad, Sharpay, Jimmie, Troy, Gabriella, & Tiara || East High Auditorium (The Spring Musical performance) || Featuring: Last Chance with Kelsi & Ryan, Now or Never (Reprise) with Chad and the Jocks, I Want it All (Reprise) with Ryan and the Dancers, Just Wanna Be with You (Reprise) with Sharpay, Jimmy, Troy, Gabriella, and the Ensemble, and A Night to Remember (Reprise) with Tiara, Sharpay, and Male Chorus
|-
| "Were All in This Together (Graduation Mix)" || Troy, Gabriella, Sharpay, Ryan, Chad, Taylor (on soundtrack); Choir (in film) || East High auditorium/graduation ceremony || A slower, graduation-tuned reprise from the first installment
|-
| "High School Musical" || Troy, Gabriella, Sharpay, Ryan, Chad and Taylor|| Graduation Ceremony on the East High Football Field || The trilogy finale
|}

==Production== East High School – the GOED board Friday approved a maximum $2 million incentive for the production, the largest ever given to entice a filmmaker to Utah." 

Principal photography began on May 3, 2008; the 41 days scheduled for shooting was a longer period than for the first two films. 

Stan Carrizosa, the winner of ABCs summer reality show,   appears in a music video "Just Getting Started" that is shown over the end credits of the theatrical release of the film.  The shows other 11 finalists were featured in the music video as well.

===Development=== People Magazine Writers Guild Screen Actors Guild strike shut down Hollywood for several months. Efron declined to comment for the article, and although contract negotiations still are ongoing, sources say Efron is being offered a salary closer to $3 million, not $5 million, for the follow-up, which focuses on senior year at East High. Whatever the price, hes still perceived as a steal." 

Ortega stated that pre-production would most likely start in January 2008. Filming began May 3, 2008, at East High School in Salt Lake City, Utah. He stated that the script had been submitted before the writers strike started and that they were developing music. He added that filming will happen in Salt Lake City, Utah (as the first two films), hinted that the plot will be something of the nature of the Wildcats final year in High School and stated, "it looks like weve rounded up the cast."  

Before filming began, the HSM3 board and cast held a press conference at East High School announcing the start of filming. The film would be released in theaters (in the United States) on October 24, 2008, though the film was to open in several countries including the UK at least one week earlier. The film had a $11 million budget and a 40-day shooting period. The film was said at the time to be the final installment with the current cast. The London premiere was the biggest London premiere of all time. 

===Vanessa Hudgens photo controversy===
Despite early speculation that Vanessa Hudgens would be dropped from the film due to her nude photo scandal, The Walt Disney Company denied the reports, saying, "Vanessa has apologized for what was obviously a lapse in judgment. We hope shes learned a valuable lesson."   

==Release==

===Sing-along version===
On November 7, 2008, High School Musical 3: Senior Year: The Sing-Along Edition with lyrics highlighted on the screen was released in selected theaters.
The sing along version was released on May 21, 2011. 

===Critical reception===
The film met with mixed to positive reviews. As of December 2010, High School Musical 3: Senior Year has a fresh rating of 65% on  , which assigns a weighted average score out of 100 to reviews from mainstream critics, gave the film a score of 57 based on 26 reviews, which is interpreted as "Mixed or average reviews" by Metacritic. 
 The Telegraph praised the changes brought about by the higher budget of a theatrical release: "High School Musical 3 uses its bigger budget to inject colour, scale, and visual depth. The opening basketball game alone is dizzying as the camera swoops high and wide, before a winning point makes the crowd erupt". 
 Frankenstein monster of an entertainment, featuring major components that were already trotted out the first two times." 

Peter Johnson of The Guardian describes the film as so bland that it "makes cellophane taste like chicken jalfrezi", and says that "the sheer squeaky-cleanness of everything is creepy, and when the characters are called upon to dance, they do so with robotic efficiency, and sing in that decaffeinated high vibrato, like 21st-century Hollywood castrati." 

 , yet hes also achingly sincere. His fast-break alertness makes him the most empathetic of teen idols; hes like a David Cassidy who knows how to act, and who can swoon without getting too moist about it. Apart from Efron, the breakout star is Ashley Tisdale, whose Sharpay makes narcissism a goofy, bedazzled pleasure." 

MovieGuide has also favorably reviewed the film, strongly recommending it for the family as "fun, clean and full of energy" and describing it as "thin on plot" yet nevertheless "a phenomenon." 

The BBC film critic Mark Kermode loved the film and said it was in his top 5 films for the year, and named Tisdale the "Best Supporting Actress" of 2008.

The Fort Worth Star-Telegram stated that the latest installment was "critic-proof" and "everything fans could hope for and more." They go on to say that "the kids finally look like true performers rather than Disney Channel mainstays desperately trying to remain relevant, and they deserve the lucrative careers that lie ahead" and gave the film a rating of four out of five stars. The film was also well received in the UK.  Hudgens was recognized as Favorite Movie Actress at Nickelodeons Kids Choice Awards, Efron was voted Best Male Performance at the 2009 MTV awards and Choice Actor: Music/Dance at the 2009 Teen Choice Awards and Tisdale was voted Breakthrough Performance Female at the 2009 MTV Movie awards and Best Supporting Actress at the 2009 UK Kermode Awards.

===International release===
{| class="wikitable"
! style="width:195px;"| Country || style="width:285px;"| Title || style="width:250px;"| Theatrical release || style="width:310px;"| TV release
|-
|  
| rowspan="2" | High School Musical 3: Senior Year October 24, 2008
| style="text-align:center;"| April 4, 2010
|-
|  
|-
|  
| High School Musical 3: Ano da Formatura October 24, 2008
| style="text-align:center;"| March 13, 2011 
|-
|  
| rowspan=5|High School Musical 3: La Graduación October 24, 2008
|-
|  
| style="text-align:center;"| October 21, 2008
|-
|   October 28, 2008
|-
|   October 30, 2008
|-
|                   October 31, 2008
|-
|  
| High School Musical 3 : Nos années lycée October 22, 2008 October 31, 2011
|-
|    
| High School Musical 3: Senior Year October 24, 2008 December 4, 2009
|-
|  
| High School Musical 3: Yıldızlar Takımı November 14, 2008 July 23, 2011
|-
|  
| High School Musical 3: Senior Year October 23, 2008
|-
|  
| High School Musical 3: Fin de Curso October 24, 2008 November 16, 2012
|-
|  
| High School Musical 3: Senior Year October 31, 2008
| 
|-
|  
|  October 29, 2008
|
|-
|  
| October 22, 2008
|
|-
| 
| rowspan=5|High School Musical 3: Senior Year October 29, 2008 September 19, 2012
|-
|  October 24, 2008
|-
|  January 22, 2009
|-
|  November 20, 2008
|-
|       
| style="text-align:center;"|N/A
|}

===Box office=== Les Miserables Mamma Mia!, for the biggest opening ever for a movie musical.  The film also opened at #1 overseas, with an international opening of $40,000,000. High School Musical 3: Senior Year grossed $90,559,416 in North America and $162,349,761 in other territories leading to a worldwide total of $252,909,177, which was above even Disneys expectations. 

==Awards==
{| class="wikitable"
|-
! Year
! Award
! Category
! Result
|-
| rowspan="3" align="center" | 2008
| rowspan="2" align="center" | CMA Wild and Young Awards
| Best Actor International   (Zac Efron) 
|  
|-
| Most Popular Celebrity International   (Zac Efron) 
|  
|-
| align="center" | Phoenix Film Critics Society Awards
| Best Live Action Family Film
|  
|-
| rowspan="21" align="center" | 2009
| align="center" | ASCAP Film and Television Music Awards Adam Watts, Theodore Thomas, Theron Thomas and Timothy Thomas) 
|  
|- BAFTA Awards 
| BAFTA Kids Vote – Feature Film 
|  
|- Golden Reel Award
| Best Sound Editing – Music in a Musical Feature Film   (Tanya Noel Hill and Charles Martin Inouye) 
|  
|-
| rowspan="5" align="center" | MTV Movie Awards
| Best Male Performance   (Zac Efron) 
|  
|-
| Breakthrough Performance Female   (Ashley Tisdale) 
|  
|-
| Best Movie
|  
|-
| Breakthrough Performance Female   (Vanessa Hudgens) 
|  
|-
| Best Kiss   (Vanessa Hudgens & Zac Efron) 
|  
|-
| align="center" | National Association of Latino Independent Producers
| Outstanding Achievement Award  (Kenny Ortega) 
|  
|-
| rowspan="2" align="center" | Nickelodeon Kids Choice Awards
| Favorite Movie
|  
|-
| Favorite Actress   (Vanessa Hudgens) 
|  
|-
| align="center" | Nickelodeon Australian Kids Choice Awards
| Fave Movie Star   (Zac Efron) 
|  
|-
| align="center" | UK Kermode Awards
| Best Supporting Actress   (Ashley Tisdale) 
|  
|-
| rowspan="7" align="center" | Teen Choice Awards
| Choice Movie Actor: Music/Dance   (Zac Efron) 
|  
|-
| Choice Movie: Music/Dance
|  
|-
| Choice Movie Actress: Music/Dance   (Vanessa Hudgens) 
|  
|-
| Choice Movie Actress: Music/Dance   (Ashley Tisdale) 
|  
|-
| Choice Movie Liplock   (Zac Efron and Vanessa Hudgens) 
|  
|-
| Choice Movie Actor: Music/Dance   (Corbin Bleu) 
|  
|-
| Choice Music: Soundtrack  ( ) 
|  
|-
| align="center" | Young Artist Award
| Jackie Coogan Award - Contribution to Youth  (Kenny Ortega) 
|  
|}

==Home media==
High School Musical 3: Senior Year was released in Region 1 DVD and Blu-ray Disc|Blu-ray on February 17, 2009,  in Region 2 DVD on February 16, 2009  and in Region 3 DVD on February 24, 2009.  The DVD was released in single- and two-disc editions. 

In Region 2, the single-disc edition DVD featured most of the two-disc edition bonus features such as bloopers, deleted scenes, extended version of the film, sing-along and cast goodbyes.  In Region 3, only the single-disc edition DVD was released with all of the two-disc bonus features as well the extended edition of the film. In the Philippines, it was released on February 25, 2009. The Region 4 DVD was released on April 8, 2009. As of November 1, 2009, the DVD has sold over 23 million copies and generated over $200 million in sales revenue.   

==Broadcasting== Disney Channel US brought 4 million viewers. 

===International release=== M6

Disney Channel India was the first to telecast High School Musical 3 officially. It was originally telecasted on 18 October as a part of Club HSM, but did not gain too much popularity. It was again aired on January 1 as a part Fully Funny First Day, and again on 14 February as a part of Pyaar Ke after effects.

==Possible sequel and Reunion film==
In 2009, Gary Marsh, president of Disney Channel confirmed that there will be fourth installment in the series saying, "Im excited to be part of a franchise that, thematically, encourages kids to believe in themselves. Like the first three movies, my goal is to keep the art form of the traditional musical alive, yet bring a contemporary emotional response to the story and its characters through song and dance."

It was also confirmed that the main stars such as Zac Efron, Vanessa Hudgens, Ashley Tisdale or any of the original Wildcats gang would not be returning as the lead roles but an entirely different actors will be cast for the film. Reports said that the fourth film would have the title High School Musical 4: East Meets West and would be directed by Jeffrey Hornaday. It was also reported that the plot would follow around a love triangle set against the cross-town school rivalry between the East High Wildcats and West High Knights. The film should begin to proceed in late 2014 and premiere in July 2015. There will be an epilogue 10 years later of where the Wildcats are today. In an interview with Showbiz Spy in March 2012, Efron stated that he would be open to filming a fourth film, saying, "I cant say for sure if it will happen but deep down in my heart, I would jump at the chance." He then went on to say that he was "pretty sure   would feel the exact same way." In March 2014 Efron once again discussed the possibility of a reunion film.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  , DVD Talk

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 