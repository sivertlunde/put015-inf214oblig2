On My Own (film)
{{Infobox film
| name           = On My Own
| image          = 
| image size     =
| caption        = 
| director       = Antonio Tibaldi Rosa Colosimo Films
| writer         = Antonio Tibaldi Gill Dennis John Frizzell
| based on       = 
| narrator       =
| starring       = Judy Davis Matthew Ferguson
| music          = Franco Piersanti
| cinematography = Vic Sarin
| editing        = Edward McQueen-Mason
| studio         = Alliance Communications Corporation 
| distributor    = Alliance Films
| released       = 1991, 1992, 1993 (Italy, Spain, Australia)
| runtime        = 97 minutes
| country        = Australia Canada English
| budget         = C$3,885,000 "Production Surveys", Cinema Papers, May 1991 p73 
| gross = A$60,000 (Australia) 
| preceded by    =
| followed by    =
}} boarding school, whose father lives in Hong Kong, and whose mother (played by Davis) is from England. The plot revolves around Simon coming to terms with the revelation that his mother suffers from schizophrenia.
==Production== Ontario boarding school” and the scenes showing the school were filmed at two real-life boarding schools in Ontario, Ridley College (with its distinctive bell tower and entrance gates, called the Marriott Gates) and Upper Canada College (including the interior of the Upper School and the interior of Wedd’s boarding house).

Post-production for the film was done in Australia by the South Australian Film Corporation. 

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 