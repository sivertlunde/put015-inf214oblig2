Putting Pants on Philip
{{Infobox film
| name = Putting Pants on Philip
| image = L&H_Putting_Pants_on_Philip_1927.jpg
| caption = Theatrical poster
| director = Clyde Bruckman
| producer = Hal Roach
| writer = H.M. Walker
| starring = Stan Laurel Oliver Hardy
| cinematography = George Stevens
| editing = Richard C. Currier
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 19 minutes
| country = United States
| language = Silent film  English intertitles
| budget =
}}
 silent short film starring American comedy double act Laurel and Hardy. Made in 1927, its the first film to bill them as an official team. The duo appeared in a total of 107 films between 1921 and 1950.

==Plot==
Piedmont Mumblethunder (Hardy), seeing Philip for the first time, tells a friend that he pities whoever has to collect this character, only to be upset when he turns out to be that person.  Hardy is embarrassed at the effeminacy of his kilt-wearing Scottish nephew Philip (Laurel), and is further upset by repeated incidents with the kilt. Women faint.

In one chase scene, a dog, riled up by the action, chases after Hardy. This was not scripted, but added to the hilarity.

==Production== The Second Hundred Years, directed by Fred Guiol and supervised by Leo McCarey, who suggested that the performers be teamed permanently.

The film was partially shot at the historic Culver Hotel.   

==Cast==
*Stan Laurel - Philip
*Oliver Hardy - Piedmont J. Mumblethunder
*Charles A. Bachman - Officer
*Ed Brandenberg   - Bus conductor Harvey Clark - Tailor
*Dorothy Coburn - Girl chased by Philip
*Sam Lufkin - Ships doctor Don Bailey 
*Chet Brandenburg Alfred Fisher Jack Hill
*Eric Mack Bob OConnor
*Retta Palmer
*Lee Phelps
*Heath Parks

==Notes==
 

==References==
*William K. Everson|Everson, William K. The Complete Films of Laurel and Hardy. New York: Citadel, 2000, 1967. ISBN 0-8065-0146-4. (First book-length examination of the individual films)

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 


 
 