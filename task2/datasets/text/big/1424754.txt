Cinderella Man
 
 
{{Infobox film
| name           = Cinderella Man
| image          = Cinderella Man poster.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Ron Howard
| producer       = Brian Grazer Ron Howard Penny Marshall
| writer         = Cliff Hollingsworth Akiva Goldsman
| story           = Cliff Hollingsworth
| starring       = Russell Crowe Renée Zellweger Paul Giamatti Craig Bierko Paddy Considine Bruce McGill Ron Canada
| music          = Thomas Newman
| cinematography = Salvatore Totino Mike Hill Dan Hanley Parkway Productions
| distributor    = Universal Pictures  (North America)  Buena Vista Pictures  (International) 
| released       =  
| runtime        = 144 minutes
| country        = United States   Canada
| language       = English
| budget         = $88&nbsp;million
| gross          = $108.5 million 
}}
 boxing champion James J. Braddock and inspired by his life story. The film was produced by Howard, Penny Marshall, and Brian Grazer. Damon Runyon is credited for giving Braddock this nickname. Russell Crowe, Renée Zellweger and Paul Giamatti star.

==Plot== boxer from New Jersey, formerly a light heavyweight contender, who is forced to give up boxing after breaking his hand in the ring. This is both a relief and a burden to his wife, Mae; she cannot bring herself to watch the violence of his chosen profession, yet she knows they will have no good income without his boxing.
 Joe Gould, offers him a chance to fill in for just one night and earn cash. The fight is against the number-two contender in the world, Corn Griffin.

Braddock stuns the boxing experts and fans with a third-round knockout of his formidable opponent. He believes that while his right hand was broken, he became more proficient with his left hand, improving his in-ring ability. Despite Maes objections, Braddock takes up Goulds offer to return to the ring. Mae resents this attempt by Gould to profit from her husbands dangerous livelihood, until she discovers that Gould and his wife also have been devastated by hard times.
 Max Baer a possibility, Braddock continues to win. Out of a sense of pride, he uses a portion of his prize money to pay back money to the government given to him while unemployed. When his rags to riches story gets out, the sportswriter Damon Runyon dubs him "The Cinderella Man", and before long Braddock comes to represent the hopes and aspirations of the American public struggling with the Depression.

A title fight against Baer comes his way. Braddock is a 10-to-1 underdog (competition)|underdog. Mae is terrified because Baer, the champ, is a vicious man who reportedly has killed at least two men in the ring. He is so destructive that the fights promoter, James Johnston, forces both Braddock and Gould to watch a film of Baer in action, just so he can maintain later that he warned them what Braddock was up against.

Braddock demonstrates no fear. The arrogant Baer attempts to intimidate him, even taunting Mae in public that her man might not survive. When he says this, she becomes so angry that she throws a drink at him. She is unable to attend the fight at the Madison Square Garden Bowl or even to listen to it on the radio.

On June 13, 1935, in one of the greatest upsets in boxing history, Braddock defeats the seemingly invincible Baer to become the heavyweight champion of the world.

An epilogue reveals that Braddock later worked on the building of the Verrazano Bridge, owning and operating heavy machinery on the docks where he worked during the Depression, and that he and Mae used his boxing income to buy a house, where they spent the rest of their lives.

==Cast==
* Russell Crowe as James J. Braddock
* Renée Zellweger as Mae Braddock Joe Gould
* Bruce McGill as James Johnston Max Baer
* Paddy Considine as Mike Wilson
* David Huband as Ford Bond
* Connor Price as Jay Braddock
* Ariel Waller as Rosemarie "Rosy" Braddock
* Patrick Louis as Howard Braddock
* Rosemarie DeWitt as Sara Wilson
* Linda Kash as Mrs. Gould
* Nicholas Campbell as Sporty Lewis
* Gene Pyrz as Jake
* Chuck Shamata as Father Roddick
* Ron Canada as Joe Jeanette
* Alicia Johnston as Alice
* Troy Amos-Ross as John Henry Lewis
* Mark Simmons as Art Lasky
* Art Binkowski as Corn Griffin
* David Litzinger as Abe Feldman
* Matthew G. Taylor as Primo Carnera
* Rance Howard as Announcer Al Fazin
* Robert Norman Smith as reporter

==Production== Madison Square Garden, complete with fake store fronts and period stop lights. A stretch of Queen Street East between Broadview and Carlaw was also made up to appear to be from the 1930s and dozens of period cars were parked along the road. Maple Leaf Gardens was used for all the fight scenes, and many scenes were filmed in the Distillery District. Filming also took place in Hamilton, Ontario at the harbour for the dock workers scene.     The main apartment was shot north of St. Clair Avenue on Lauder Avenue on the east side.  An awning was put up for a dress shop, later turned into a real coffee shop.

The Toronto Transit Commissions historic Peter Witt streetcar and two more cars from the nearby Halton County Radial Railway were used for the filming, travelling on Torontos existing streetcar tracks.

==Release== Miracle on 34th Street.

==Reaction==
===Critical reception and box office===
The film received positive reviews from most critics. Rotten Tomatoes judged it "Certified Fresh" with a score of 80%, based on reviews from 207 critics. 
Metacritic gives the film a score of 69% based on reviews from 40 critics.  It received an A+ rating from CinemaScore. 

Despite critical acclaim, it was a box office disappointment during its first several weeks. During its North American theatrical run, the film (which cost $88&nbsp;million) earned only $61,649,911.   

===Depiction of Max Baer=== Max Baer is portrayed as a brutal person who behaves inappropriately outside the ring and viciously inside it (to the point of killing two opponents). In the film, Baer is shown making a pass at Braddocks wife in a nightclub, and warning Braddock that he might kill him in the ring. There is no evidence that this incident happened. Baers relatives and boxing historians have criticized this depiction of him, arguing that Baer killed one man in the ring, Frankie Campbell, not two -- the film contends he also caused the slightly delayed death of Ernie Schaaf, a claim never proven -- and was considered by many to be a gentleman. This is supported by historical evidence which shows that Baers demeanor, both within and outside the ring, was much less brutal than the film portrayed.

On the other hand, the portrayal of Max Baers style of boxing in the film is very close to what happened in the actual bout with Braddock. 

The author of the book on which the film was based has asserted that Baer was kind, intelligent, charismatic, good humored and clowning, loved and respected. He fought to win, brutally when he had to. Prize fighting by its nature is brutal and violent. He pointed out the emotional pain that Baer endured the rest of his life following Campbells death, and the fact that Baer gave purses from his bouts to Campbells family to help provide Campbells children with an education. 

The films depiction of Baer is similar to that in the press in the 1930s, and often used by promoters to attract interest in his fights.  The real Max Baer (who was also an actor) did star as a negatively depicted, hostile boxer in the film The Harder They Fall.

Max Baer was actually a Jewish activist who wore a large Star of David on his boxing shorts in fights. That star makes it easy to distinguish Baer from Braddock in the black-and-white film footage of their bout.

==Awards and nominations==
Academy Award
* Best Supporting Actor (Paul Giamatti) (Nominated) Mike Hill) (Nominated) Lance Anderson) (Nominated)

BAFTA Award
* Best Supporting Actor (Paul Giamatti) (Nominated)
* Best Original Screenplay (Cliff Hollingsworth, Akiva Goldsman)

Critics Choice Movie Award
* Best Actor (Russell Crowe) (Nominated)
* Best Supporting Actor (Paul Giamatti) (Won)
* Best Director (Ron Howard) (Nominated)
* Best Film (Nominated)

Golden Globe Award
* Best Actor – Motion Picture Drama (Russell Crowe) (Nominated)
* Best Supporting Actor – Motion Picture (Paul Giamatti) (Nominated)

Screen Actors Guild Award
* Outstanding Actor – Motion Picture (Russell Crowe) (Nominated)
* Outstanding Supporting Actor – Motion Picture (Paul Giamatti) (Won)

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 