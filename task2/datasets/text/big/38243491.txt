Fatima (1938 film)
{{Infobox film
| name           = Fatima
| image          = Fatima ad.png
| image_size     = 
| border         = 
| alt            = 
| caption        = Newspaper ad
| director       = {{plain list| Joshua Wong Othniel Wong
}}
| producer       = Tan Khoen Yauw
| writer         = Saeroen
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       ={{plain list|
*Roekiah
*Rd Mochtar
*ET Effendi
}}
| music          = Lief Java
| cinematography = {{plain list|
*Joshua Wong
*Othniel Wong
}}
| editing        = 
| studio         = Tans Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies
| language       = Indonesian
| budget         = 
| gross          = 
}} Othniel and Joshua Wong. Written by Saeroen, it starred Roekiah, Rd Mochtar, and ET Effendi and followed two lovers who are disturbed by a rich youth. The film followed the same formula as the earlier hit Terang Boelan (Full Moon; 1937), and saw commercial success domestically. It is one of three films which Misbach Yusa Biran credits with reviving the domestic film industry, which had been faltering.

==Plot==
Fatima (Roekiah) is in love with Idris (Rd Mochtar), the son of a poor fisherman on the island of Motaro. One day, the rich youth Ali (ET Effendi) comes to the island and tries to steal Fatimas heart. She, however, is unwilling to receive him and gives his gifts to Idris, so that the latter can sell them. Ultimately it is revealed that Ali is the leader of a gang, and the police trace him through a stolen ring he had given Fatima. 

==Production==
The success of Albert Balinks Terang Boelan in 1937, released in a stagnant domestic film industry, led the Tan brothers (Khoen Yauw and Khoen Hian) to reestablish their production house Tans Film.  For the companys first production, the Tans called the Wong brothers, Othniel and Joshua, to direct and handle daily activities with the company.  The Wongs, who had served as cinematographers for Terang Boelan but found themselves unemployed after the studio closed its feature film division, accepted. They also handled cinematography on Fatima, shooting in black-and-white. 

The Wongs brought with them much of cast of Terang Boelan, as well as some crew members. The previous films writer, Saeroen, penned the story for Fatima, closely following the same formula of romance, good music, and beautiful scenery as his earlier film. The main cast members, including Roekiah, Rd Mochtar, and ET Effendi, as well as actors in minor roles like Kartolo (Roekiahs husband), also joined.  They were given higher wages than when they had worked on Terang Boelan. Roekiah, for example, earned a monthly fee of 150&nbsp;Netherlands Indies gulden|gulden, with another 50 gulden for Kartolo; this was twice as much as she had earned previously. 
 Portuguese influences). The film contained several songs, with vocals by Roekiah, Mochtar, Kartolo, Louis Koch, and Annie Landouw. 

==Reception and legacy==
Fatima was released on 24 April 1938, with marketing handled by the Tan brothers.  It was a commercial success, reportedly earning 200,000&nbsp;gulden on a 7,000&nbsp;gulden investment.  Most of the audience was native, although it reportedly interested Dutch viewers as well. A review in the daily Bataviaasch Nieuwsblad wrote that, although the film could not be judged by European standards, the performance by Roekiah was enjoyable and that the domestic cinema seemed "on the right track".  

The Tans enjoyed the Fatima  success, but the Wongs were displeased with their percentage of its profits, which they considered too low.  Nonetheless, they continued to work with the Tans until the 1940,  and, in 1948, the Tans and Wongs established another production house.  Mocthar and Roekiah, meanwhile, remained the main stars of Tans until Mochtar left the company in 1940 over a wage dispute. 
 domestic films Japanese occupation in 1942. 

Fatima is likely a lost film. The American visual anthropologist Karl G. Heider writes that all Indonesian films made before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==Footnotes==
 
==References==
 

==Works cited==
 
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
*{{cite web
  | title = Fatima
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-f006-38-574433_fatima
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 24 July 2012
  | archiveurl = http://www.webcitation.org/69OPnL4MG
  | archivedate = 24 July 2012
  | ref =  
  }}
*{{cite news
 |title=Filmaankondiging Cinema: Fatima
 |trans_title=Film Announcement: Fatima
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011070854%3Ampeg21%3Ap003%3Aa0076
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |page=3
 |date=25 April 1938
 |publisher=Kolff & Co.
 |accessdate=22 January 2013
 |ref= 
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
*{{cite book
 |url=http://books.google.ca/books?id=sl92GYNaKJIC
 |title=A to Z about Indonesian Film
 |language=Indonesian
 |isbn=978-979-752-367-1
 |last=Imanjaya
 |first=Ekky
 |ref=harv
 |publisher=Mizan
 |location=Bandung
 |year=2006
}}
*{{cite web
 |title=Othniel Wong
 |url=http://filmindonesia.or.id/movie/name/nmp4b99bd523f3ef_Othniel-Wong
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=29 August 2012
 |archiveurl=http://www.webcitation.org/6AHIOJqsm
 |archivedate=29 August 2012
 |ref= 
}}
 

==External links==
* 

 
 
 