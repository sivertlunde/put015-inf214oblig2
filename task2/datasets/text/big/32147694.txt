Reception (film)
 
Reception is a 2011 short film directed by Dane McCusker and written by Jasper Marlow. It revolves around a pregnant hotel worker played by ex-Home and Away star Jessica Tovey.   Show Me Shorts, New Zealand Film Festival Line up 

== Plot ==
A pregnant hotel worker (Jessica Tovey) receives a late night phone call from a disgruntled guest. He says there are strange noises coming from the room next to his. Upon investigation the pregnant girl finds the room empty except for lots of baby paraphernalia. The door shuts locking her in and she is forced to confront her deepest fears. 

== Release ==
Reception was screened as part of the 2011 Dungog Film Festival.  It screened in the Walk on the Dark Side section of short films at the Show Me Shorts Film Festival in New Zealand, and won the "Best Horror/Thriller" award at the 10th International Student Film Festival Hollywood   International Student Film Festival Hollywood official site 

== References ==
 

== External links ==
*   at IMDB|imdb.com
*  
*  
 

 
 
 
 

 