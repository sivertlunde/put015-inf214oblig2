Professor Toto
 

{{Infobox television film
| bgcolour =
| name = Professor Toto and Sophia
| image = 
| caption =
| format = Animation
| runtime =
| creator =
| director = Francois Thibaut
| producer = The Language Workshop For Children
| writer = Francois Thibaut
| starring = Lani Minella Philippe Perriard Olivia Caillot   Elizabeth Acosta-Crommett Tony Allotta Samantha Catalano Tony Zhang Ziaoyan Su Dominik Sachsenheimer  
| music = Dominik Sachsenheimer  
| country =   United States German
| network =
| released = 1994
| first_aired =
| last_aired =
| num_episodes =
| preceded_by =
| followed_by =
}}
Professor Toto and Sophia is an animated film produced by Francois Thibaut and The Language Workshop for Children for purposes of second language acquisition for children and has been translated into French, Spanish, Italian, Mandarin Chinese, and German versions. The film is intended to educate children in language pronunciation, vocabulary, and grammar. The Professor Toto animation utilizes the "Thibaut Technique" teaching methodology. The Professor Toto animation has a 60 minute running time, and the separate vocabulary review segment has a 20 minute running time.

==Plot==
The animation begins as avuncular and humorous Professor Toto invites his class to watch a cartoon about a day in the life of a boy named Eric. Eric describes what he is doing, "I comb my hair," "I put on my socks," "I put on my shoes," "I drink hot chocolate," "Dad drinks coffee," and "Mom drinks tea," in school, lunchtime, park, snack time, dinner, and bedtime environments. Then the professor introduces his class to more foreign language words and images periodically prompting viewers to repeat and asking review questions such as "what is the cat wearing?" The animation introduces animals, clothing, colors, body parts, foods, action verbs, adjectives, prepositions, places, directions, shapes, sports, musical instruments, time, months, and seasons.

==Characters==
*Professor Toto – a jocular, older language teacher played by Lani Minella (English Version), Philippe Perriard (French Version), David Crommett (Spanish Version), Tony Allotta (Italian Version), Tony Zhang (Chinese Version), and Dominik Sachsenheimer (German Version)

*Eric – an eight-year-old boy played by Lani Minella (English Version), Olivia Caillot (French Version), Elizabeth Acosta-Crommett (Spanish Version), Samantha Catalano-Wells (Italian Version), Ziaoyan Su (Chinese Version), and Katrin Biemann (German Version)

*Sophia – an exemplary student played by Lani Minella (English Version), Olivia Caillot (French Version), Elizabeth Acosta-Crommett (Spanish Version), Samantha Catalano-Wells (Italian Version), Ziaoyan Su (Chinese Version), and Katrin Biemann (German Version)

*Various Animals - Lani Minella (English Version), Philippe Perriard and Olivia Caillot (French Version), David Crommett and Elizabeth Acosta-Crommett (Spanish Version), Tony Allotta and Samantha Catalano-Wells (Italian Version), Tony Zhang and Ziaoyan Su (Chinese Version), Dominik Sachsenheimer and Katrin Biemann (German Version).

==Credits==
*Written by: The Language Workshop for Children
*Music by: Jérôme Rossi, Dominik Sachsenheimer
*Art Direction: Mirtad Gazazian, Jack Williams, Bill Stout
*Animation and Design: Mirtad Gazazian, Jack Williams, Bill Stout
*Graphics: Francesca Labrini, Paul Kaugust
*Additional Artwork: Anne Marie Raboud, Robert Dress
*Clean-Up Colors: Mirtad Gazazian, Jack Williams, Bill Stout
*Storyboards: Anne Marie Raboud, Majid Chibane
*Sound Editing and Mixing: Mirtad Gazazian, Alex Melik-Barkhudarov
*Sound Engineering: Jérôme Rossi, Rebeca Vallejo, Hortencio Gomes,
*Assistants: Paula Bloom, Nikolin Eyrich, Veronica Noguera Sicard, Michelle Novotny
*Directed by: Francois Thibaut
*Produced by: The Language Workshop for Children

==External links==
*  
*  ( ) educational program
*  ( ) language education series
*  Parents Choice Award
*  I-Parenting Media Award
*  T.H.E. Educational Administrators Journal
*  Tech Trends Article
*  Grandparents.com review
*  Babble.com Article
*  EBSCO Library Tracking System
*  World Cat Library Inventory System
*Gangwer, Kristin. (2007-04-03).  . Babble.com.

 
 
 