The Day He Arrives
{{Infobox film
| name           = The Day He Arrives
| image          = The Day He Arrives.jpg
| alt            = 
| caption        = 
| film name      = {{Film name
| hangul         =  
| hanja          =  
| rr             = Bukchon Banghyang
| mr             = Pukchon Panghyang}}
| director       = Hong Sang-soo
| producer       = Kim Kyoung-hee
| writer         = Hong Sang-soo Kim Bo-kyung
| music          = Jeong Yong-jin
| cinematography = Kim Hyung-koo
| editing        = Hahm Sung-won
| studio         = Jeonwonsa Films
| distributor    = Jeonwonsa Films JoseE Films
| released       =  
| runtime        = 79 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =    
}} 64th Cannes Film Festival.    The film received 45,223 admissions on its domestic release.

== Plot == Bukchon (North Village;  ), Jongno District. When the friend does not answer his calls, Seong-jun wanders around Bukchon and runs into an actress he used to know. The two talk for a while, but soon part. He makes his way down to Insa-dong and drinks makgeolli (rice wine) by himself. Some film students at another table ask him to join them—Seong-jun used to be a film director. He soon gets drunk and heads for his ex-girlfriends house.

Whether it is the next day or some other day, Seong-jun is still wandering around Bukchon. He runs into the actress again. They talk and soon part. He eventually meets his friend and they head to a bar called Novel with a female professor his friend knows. The owner of the bar has a striking resemblance to Seong-juns ex-girlfriend. He plays the piano for her.

Whether it is the next day or some other day, Seong-jun goes to the Jeongdok Public Library with his friend and mentions that it was the first place he chased after a woman. Later, they have drinks with a former actor who had been doing business in Vietnam. The same female professor joins them and the four go to the bar called Soseol (lit. "Novel"). Seong-jun gets drunk and ends up kissing the owner of the pub.

Seong-jun may have spent a few days in Seoul with his friend, or it may still be his first day there. He may have learned something from the encounter with his ex-girlfriend, or may have to meet the woman that resembles her again, for the first time. As life presents itself in no more than today’s worth of time, Seong-jun also has no other choice than to face his "today".

== Cast ==
* Yoo Jun-sang as Seong-jun, a professor of film studies
* Kim Sang-joong as Young-ho, a film critic and friend of Seong-jun
* Song Seon-mi as Bo-ram, a professor of film studies Kim Bo-kyung as Kyung-jin (Seong-juns ex-girlfriend) / Ye-jeon (bar owner)
* Kim Eui-seong as Joong-won, an ex-actor
* Park Soo-min as an actress 
* Go Hyun-jung as a cinema fan
* Gi Ju-bong as a producer
* Baek Jong-hak as a director 
* Baik Hyun-jhin as a composer 
* Ahn Jae-hong as student 1 
* Bae Yoo-ram as student 2 
* Jeong Ji-hyeong as student 3

==See also==
*List of black-and-white films produced since 1970

== References ==
 

== External links ==
*    
*   at The Cinema Guild
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 