Dobara
:see Dubara Palace for the palace in Cairo
{{Infobox film
| name           =Dobara
| image          =Dobara.jpg
| image_size     =250px
| caption        =
| director       =Shashi Ranjan
| producer       =
| writer         =
| narrator       =
| starring       =Jackie Shroff, Mahima Chaudhry and Raveena Tandon
| music          =Anu Malik 
| cinematography =
| editor       =
| distributor    =
| released       = 24 September 2004
| runtime        =
| country        = India Hindi
| budget         =
}} 2004 Indian Hindi film directed by Shashi Ranjan on his debut.    It stars Jackie Shroff, Mahima Chaudhry and Raveena Tandon.

==Plot==
Ranbir and Anjali Saigal, a happily married couple, find themselves in trouble when visited by an escapee from a mental asylum, Ria Deshmukh.

==Cast==
*Jackie Shroff... 	Ranbir Sehgal
*Raveena Tandon... 	Ria
*Mahima Chaudhry ... 	Dr. Anjali Sehgal
*Gulshan Grover ... 	Truck driver
*Soni Razdan ... 	Mrs. Devika Mehta
*Vrajesh Hirjee 		
*Seema Biswas 		
*Moammar Rana 	 		

==Reception==
The film received a mixed reception by critics. Taran Adarsh praised the performances of the main actors and the music by Anu Malik such as Humnasheen and Mujhse Kyon Roothe Ho? but said "the sluggish pace of its narrative and classy treatment act as barricades. And the pre-climax portions, involving the child and the conclusion to the triangle, take the graph of the film downhill."
 Rediff.com criticised the script and remarked that "there were too many holes in it" and questioned the choice of film for a debut by the director. 

==References==
 

==External links==
* 

 
 
 

 