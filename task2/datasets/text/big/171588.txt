Sweet and Lowdown
:For the 1944 Benny Goodman film, see Sweet and Low-Down. For the Dave Van Ronk album, see Sweet & Lowdown.
{{Infobox film
| name           = Sweet and Lowdown
| image          = Sweet_lowdown_moviep.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Woody Allen
| producer       = Jean Doumanian
| writer         = Woody Allen
| starring       = Sean Penn Samantha Morton Anthony LaPaglia Uma Thurman
| cinematography = Zhao Fei
| editing        = Alisa Lepselter
| studio         = Sweetland Films
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $4,197,015
}}
Sweet and Lowdown is a 1999 American comedy-drama film written and directed by Woody Allen. The film tells the story, set in the 1930s, of a fictional jazz guitarist named Emmet Ray (played by Sean Penn) who regards himself as the second greatest guitarist in the world (after jazz icon Django Reinhardt) who falls in love with a mute woman (Samantha Morton). The film also stars Uma Thurman and Anthony LaPaglia. 
 La Strada, Best Actor Best Supporting Actress respectively. Like several of Allens other films (e.g., Zelig), Sweet and Lowdown is occasionally interrupted by interviews with critics and biographers like Allen, Nat Hentoff, and Douglas McGrath, who comment on the films plot as if the characters were real-life people.

==Plot summary==
Emmet Ray (Sean Penn) is a jazz guitarist who achieved some acclaim in the 1930s with a handful of recordings for RCA Victor, but who faded from public view under mysterious circumstances. Though a talented musician, Rays personal life is a shambles. He is a spendthrift, womanizer and pimp who believes that falling in love will ruin his musical career. Due to his heavy drinking, hes often late or even absent for performances with his quintet. After music, his favorite hobby is shooting rats at garbage dumps. Ray idolizes famed guitarist Django Reinhardt, and is said to have fainted in his presence and to have fled a nightclub performance with severe stage fright upon hearing a false rumor that Reinhardt was in the audience. 

On a double date with his drummer, Ray meets Hattie, a shy, mute laundress. After overcoming some initial frustration due to the difficulties of communication, Ray and Hattie form an affectionate and close relationship. She accompanies him on a cross-country trip to Hollywood, where he plays in a short film; Hattie is spotted by a director and enjoys a brief screen career. However Ray is convinced that a musician of his stature should never settle down with one woman. On a whim, Ray marries socialite Blanche Williams (Uma Thurman). However, Blanche sees Ray mainly as a colorful example of lower-class life and a source of inspiration for her literary writings. She reports that Ray is tormented by nightmares and shouts out Hatties name in his sleep.

When Blanche cheats with mobster Al Torrio (Anthony LaPaglia), Ray leaves her and locates Hattie. He assumes that she will take him back, but discovers that she is happily married and raising a family. Afterwards, on a date with a new woman, a despondent Ray plays a melody that Hattie adored and then smashes his guitar and forlornly repeats the phrase "I made a mistake!" He angrily smashes his guitar to pieces as his date leaves him. Woody Allen and the rest of the documentary experts remarked that Rays final composes were legendary, finally reaching the quality of Reinharts.

==Cast==
* Sean Penn as Emmet Ray
* Samantha Morton as Hattie
* Anthony LaPaglia as Al Torrio
* Uma Thurman as Blanche
* James Urbaniak as Henry John Waters as Mr. Haynes
* Gretchen Mol as Ellie
* Denis OHare as Jake
* Molly Price as Ann
* Brian Markinson as Bill Shields
* Tony Darrow as Ben
* Daniel Okrent as A.J. Pickman
* Brad Garrett as Joe Bedloe
* Woody Allen as Himself
* Ben Duncan as Himself
* Nat Hentoff as Himself
* Douglas McGrath as Himself

==Production==
Hot off his 1969 directing debut   instead. In 1995, he dismissed The Jazz Baby as having been "probably too ambitious." 

In 1998, Allen returned to the project, rewriting the script and dubbing it Sweet and Lowdown. In the role of Emmet Ray, a jazz guitarist whom Allen had originally planned to play himself, the director cast Sean Penn; Allen also considered Johnny Depp, but the actor was busy at the time. Bjorkman, Stig, ed. Woody Allen on Woody Allen: Revised Edition. London: Faber and Faber, 1995, 2004.  p. 347-56.  In regard to working with Sean Penn, who had a reputation for being difficult to work with, Allen later said, "I had no problem with him whatsoever... He gave it his all and took direction and made contributions himself... a tremendous actor." 

In an October 26, 2009, appearance on Howard Sterns radio program, Rosie ODonnell claimed that Allen offered her the role of Hattie, despite the fact that she had been vocal in her disgust over Allens relationship with Soon-Yi Previn. When Stern asked if she were at all tempted to take the role despite her personal feelings, she replied that she was "not for one minute tempted." The role went to Samantha Morton.
 Best Actor Best Actress Oscar nomination, Mortons performance was met with critical acclaim, with Salon.com critic Stephanie Zacharek saying that she "quietly explodes  ... Her performance is like nothing Ive seen in recent years." 

Sweet and Lowdown was filmed entirely in  , January 2, 2002. Accessed June 6, 2007.  but set in the Chicago area and California. 

The film was the first of Allens that was edited by Alisa Lepselter, who has edited all of Allens films since. Lepselter succeeded Susan E. Morse, who edited Allens films for the previous twenty years.

It was the first of three films where Allen collaborated with Chinese cinematographer Zhao Fei.  Allen had first noticed Zhao with his award winning work on Raise the Red Lantern, some years earlier.

==Music==
The music for the film was arranged and conducted by Dick Hyman. All of the guitar solos are played by guitarist Howard Alden. Alden also coached Sean Penn on playing the guitar for his role in the film.
 crescent moon cable breaks while Sean Penn is riding it. Pizzarelli did all other rhythm tracks.

==Reception==
Sweet and Lowdown received generally positive reviews; it currently holds a 78% fresh rating on Rotten Tomatoes, with the consensus "Critics praise Woody Allens Sweet and Lowdown for its charming, light-hearted comedy and quality acting."  The film carries a 70 on Metacritic, indicating generally favorable reviews. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 