Khadak
 German film visions and out-of-body experiences. When a disease kills some of the livestock his family relies on, it is taken as an omen of misfortune. Soon after, Bagi and many others are forcibly relocated and made to work at an open pit mine mining coal. Eventually Bagi meets a woman, named Zolzaya, who participates in raids against the freight trains which transport coal away from the facility. As Bagi fights to overcome his epilepsy, together with Zolzaya he leads a group of young people who try to disrupt the mining operations and enliven the despondent populace to return to their nomadic lifestyle.

==Cast==
*Batzul Khayankhyarvaa as Bagi
*Tsetsegee Byamba as Zolzaya
*Banzar Damchaa as Grandfather
*Tserendarizav Dashnyam as Shamaness	
*Dugarsuren Dagvadorj as Mother
*Uuriintuya Enkhtaivan as Naraa

==Music==
Some of the music in this film is composed and performed by the Mongolian band Altan Urag. {{Cite web
| url = http://movies.nytimes.com/person/493011/Altan-Urag
| title = Altan Urag Filmography
| publisher = New York Times
| accessdate = 2015-02-23
}} 

==Awards==
The film won the Lion Of The Future - “Luigi De Laurentiis” Award for a Debut Film at the 63rd Venice International Film Festival.

==References==
 

==External links==
*   Internet Movie Database

 
 