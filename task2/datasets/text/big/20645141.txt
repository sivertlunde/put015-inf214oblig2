List of Bengali films of 1997
  Tollywood (Bengali language film industry) based in Kolkata in the year 1997. 
{{cite web
|url=http://www.gomolo.in/ConditionalSearch.aspx?Yearcode=1990-1999&Langcode=Bengali&Categories=M&GotoYear=1997&GotoPage=2
|title=gomolo.in: Yearwise search result
|publisher=www.gomolo.in
|accessdate=2008-12-11
|last=
|first=
}}
 

==Highest grossing==
 

===Top ten movies===
 

====Critically acclaimed films====
 

==1997==

{| class="wikitable sortable" border="0"
|-
! Title
! Director
! Cast
! Genre
! Notes/Music
|-
|- Manoj Sen||Tapas Paul, Satabdi Roy||||
|- Asim Samanta||Ashok Kumar, Raakhee||||
|- Swapan Saha||Prasenjit Chatterjee, Rituparna Sengupta||||
|- Haranath Chakraborty||Ranjit Mullick, Prasenjit||||
|- Rituporno Ghosh||Indrani Haldar, Rituparna Sengupta||||
|- Malay Bhattacharyay||Rabi Ghosh, Dhritiman Chatterjee||||
|- Budhhadeb Dasgupta||Indrani Halder, Biplab Chatterjee||||
|}

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 
 
 

 
 
 
 