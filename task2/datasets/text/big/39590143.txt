Madame de La Pommeraye's Intrigues
{{Infobox film
| name           = Madame de La Pommerayes Intrigues
| image          = 
| image_size     = 
| caption        = 
| director       = Fritz Wendhausen 
| producer       = Erich Pommer
| writer         = Denis Diderot (story)   Paul Beyer   Fritz Wendhausen
| narrator       = 
| starring       = Olga Gsowskaja   Margarete Schlegel   Grete Berger   Alfred Abel
| music          = 
| editing        =
| cinematography = Carl Hoffmann
| studio         = Russo Film
| distributor    = Decla-Bioscop
| released       = 20 January 1922
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent film directed by Fritz Wendhausen and starring Olga Gsowskaja, Margarete Schlegel and Grete Berger. The film was produced by Russo Film, a short-lived company backed by Decla-Bioscop which aimed to adapt literary works for the screen.  It was based on a story by Denis Diderot. It premiered at the Tauentzienpalast on 20 January 1922. 

==Cast==
* Olga Gsowskaja as Madame Pommeraye 
* Margarete Schlegel as Jeanette dAisnon 
* Grete Berger as Madame dAisnon - Her Mother 
* Alfred Abel    Paul Hartmann

==References==
 

==Bibliography==
* Hardt, Ursula. From Caligari to California: Erich Pommers life in the International Film Wars. Berghahn Books, 1996.

==External links==
* 

 
 
 
 
 


 