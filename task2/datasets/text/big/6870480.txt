Sugar and Spice (2006 film)
 
{{Infobox film
| name           = Sugar & spice: Fûmi zekka
| image          = 
| caption        = 
| director       = Isamu Nakae 
| producer       = Toru Ota
| writer         = Eimi Yamada (novel "Fûmi zekka") Fumie Mizuhashi (screenplay)
| starring       = Yûya Yagira Erika Sawajiri Mari Natsuki Yo Oizumi Bolin Chen Lyla performed Oasis
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 125 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
Sugar and Spice is a Japanese movie cast in 2006, starring Yuya Yagira and Erika Sawajiri.

==Plot==
Recently graduated from high school, 17-year-old Shiro (Yagira Yuya) decides to put off college and work at a gas station instead.  Shy and introspective, Shiro understands he is at a turning point of his life, but is unsure of what lies ahead.  Though his parents disapprove of his decision, he has the support of his flower child grandmother (Natsuki Mari) who declares that a gas station is a romantic place for lifes drifters.  Surely enough, soon a new co-worker, college student Noriko (Erika Sawajiri), drifts into Shiros life.  He falls headfirst into a bittersweet first love that ushers him into the world of adulthood.

==Cast==
* Yūya Yagira - Shiro
* Erika Sawajiri - Noriko
* Mari Natsuki - Grandma
* Chen Bolin - Mike
* Yo Oizumi - Gas Station Guy

==Production==
Released on 16 September 2006 under the sub-title What little girls are made of.

==External links==
*  

 
 


 