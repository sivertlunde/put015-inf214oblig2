Live! At Shepherd’s Bush, London
{{Infobox film
| name        = Live at Shepherds Bush, London
| image       = Europe Live at Shepherds Bush, London DVD.jpg
| director    =
| producer    =
| writer      = Europe
| Europe
| distributor = Nordisk Film
| released    =  
| runtime     = 150 min  (DVD)  180 min  (Blu-ray) 
| language    = English
| imdb_id     =
| music       =
| awards      =
| budget      =
}}
 Swedish hard rock band Europe (band)|Europe. The main feature is a concert filmed at the O2 Shepherds Bush Empire in London, England on 19 February 2011. It was released on DVD and Blu-ray Disc|Blu-ray on 15 June 2011.

Both the DVD and Blu-ray editions includes an extra CD that includes the same concert, except for three songs due to time constraints.

==Track listing== Last Look at Eden"
# "The Beast" Rock the Night"
# "Scream of Anger"
# "No Stone Unturned"
# "Carrie (song)|Carrie"
# "The Getaway Plan"
# Guitar Feature: "The Loner" (tribute to Gary Moore)
# "Seventh Sign"
# "New Love in Town"
# "Love is Not the Enemy" More Than Meets the Eye"
# Drum Feature: William Tell Overture
# "Always the Pretenders"
# "Start from the Dark"
# "Superstitious (song)|Superstitious"
# "Doghouse" The Final Countdown"

==Bonus features==
===DVD & Blu-Ray===
*Live photo gallery
*Live footage from Stockholm ice stadium "Hovet" 28 December 2009:
*#"Prelude/Last Look at Eden"
*#"Love is Not the Enemy"
*#"Superstitious"
*#"Gonna Get Ready"
*#"Scream of Anger"
*#"No Stone Unturned"
*#"Carrie" (on Blu-ray only)
*#"Start From The Dark" (on Blu-ray only)
*#"New Love in Town"
*#"Let The Good Times Rock" (on Blu-ray only)
*#"Cherokee" (on Blu-ray only)
*#"The Beast"
*#"Seven Doors Hotel" (on Blu-ray only)
*Live footage from Gröna Lund in Stockholm 17 September 2010:
*#"Last Look at Eden"
*#Guitar Feature
*#"Seventh Sign"
*#"Start from the Dark"
*Live photo gallery
*Documentary: interviews and rehearsals (on Blu-ray only)
*Music videos:
**"Last Look at Eden"
**"New Love in Town"

===Extra live CD===
# "Prelude"
# "Last Look at Eden"
# "The Beast"
# "Rock the Night"
# "Scream of Anger"
# "No Stone Unturned"
# "Carrie"
# "The Getaway Plan"
# "Seventh Sign"
# "New Love in Town"
# "Love is Not the Enemy"
# "More Than Meets the Eye"
# "Always the Pretenders"
# "Start from the Dark"
# "Superstitious"
# "The Final Countdown"

==Personnel==
*Joey Tempest – lead vocals, rhythm & acoustic guitars
*John Norum – lead & rhythm guitars, backing vocals
*John Levén – bass guitar
*Mic Michaeli – keyboards, backing vocals
*Ian Haugland – drums, backing vocals

 

 
 
 
 
 