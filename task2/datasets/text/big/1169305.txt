Okinawa Rendez-vous
 
 
{{Infobox film
| name           = Okinawa Rendezvous
| image          = Okinawa Rendezvous VCD.jpg
| image_size     =
| caption        = VCD cover of Okinawa Rendezvous
| director       = Gordon Chan
| producer       = Gordon Chan
| writer         = Gordon Chan Chan Hing-Kai
| narrator       = Tony Leung
| music          =
| cinematography = Cheng Siu-Keung
| editing        = Chan Ki-Hop
| distributor    = China Star Entertainment Group
| released       = 28 July 2000
| runtime        =
| country        = Hong Kong
| language       = Cantonese
| budget         =
}} 2000 Cinema Hong Kong film produced and directed by Gordon Chan, and starring Leslie Cheung, Faye Wong, Tony Leung Ka-fai and Gigi Lai.

While not one of Hong Kongs more significant films of the year, it was claimed by its director that it was filmed in less than 2 months without a script. It was shot entirely in the southern Japanese island of Okinawa and the film features many beautiful views of the island.
 Big Heat (大热). Also, there were a few possible sub-themes like Faye Wongs New Tenant (新房客) which is as well available in her 2000 album Fable (album)|Fable (寓言).

==Plot==
Jimmy Tong (Leslie Cheung) is an expert blackmailer and thief who specialises in white-collar crimes. With his side-kick (Vincent Kok), Jimmy steals a personal diary belonging to a Yakuza leader Ken Sato (Masaya Kato) intending to use its details as a platform for blackmailing and to extort money. Sato agreed to the uneasy deal and made preparations to pay Jimmy his exorbitant demands only for Satos girlfriend Jenny (Faye Wong) to betray him and make off with the money to Okinawa.

Elsewhere, Dat Lo (Tony Leung) was vacationing with his girlfriend (Gigi Lai) and another jilted girl (Stephanie Che) in Okinawa (intending to use the vacation to dump his own girlfriend), but stumbles into Jimmy whom he had little problems recognising as an international crook. From here onwards, Dat set aside his irrelevant plans to dump his companions and sought to devise a plan to entrap and to subsequently arrest Jimmy. Dat tried to convince Jimmy as an accomplice to a new bank heist of which Jimmy needed little persuasion. However, Jenny comes into the frame and before long, both Jimmy and Dat fell in love with her.

==Cast and roles==
* Jimmy Tong - Leslie Cheung
* Jenny - Faye Wong
* Dat Lo - Tony Leung Ka-fai
* Sidekick Kuk Bo - Vincent Kok
* Ken Sato - Masaya Kato
* Cookie - Stephanie Che
* Bar/Cafe owner - Asuka Higuchi
* Sandy - Gigi Lai

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 