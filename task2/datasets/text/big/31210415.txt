Tiger by the Tail
 
{{Infobox film
| name           = Tiger by the Tail
| image          = "Tiger by the Tail" (1955).jpg
| image_size     =
| caption        = Belgian poster
| director       = John Gilling
| producer       = Robert S. Baker Monty Berman
| screenplay     = John Gilling Willis Goldbeck
| based on       =  
| narrator       = Donald Stewart
| music          = Stanley Black Eric Cross
| editing        = Jack Slade
| studio         = Tempean Films
| distributor    = Eros Films
| released       = 1955
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
}} British crime Donald Stewart.  It is an adaptation of the novel Never Come Back by John Mair. Larry Parks, a memorable Al Jolson in The Jolson Story, had fallen foul of Americas House Un-American Activities Committee, and had his first film role for four years starring in this British low budgeter.   

==Plot==
An American journalist works to expose a criminal gang in London. But his investigation of their counterfeiting activities leads to his kidnapping by the gang.

==Main cast==
* Larry Parks - John Desmond
* Constance Smith - Jane Claymore Donald Stewart - Macauly
* Cyril Chamberlain - Foster
* Ronan OCasey - Nick
* Lisa Daniely - Anna Ray
* Alexander Gauge - Fitzgerald
* Ronald Leigh-Hunt - Doctor Scott
* June Rodney - Young Psychiatric Nurse
* Joan Heal - Annabella
* Thora Hird - Mary
* Doris Hare - Nurse Brady
* Marie Bryant - Melodie
* Russell Westwood - Sam
* John H. Watson - Truscott
* Robert Moore - Clarke
* Hal Osmond - Charlie
* Frank Forsyth - Sergeant Gross
* Alastair Hunter - Hotel Clerk
* Margot Bryant - Cleaning Woman (uncredited)

==Critical reception==
*Radio Times described the film as "a tatty little tale."  
*Sky Movies called it a "thoroughly routine British `B thriller...Familiar situations and backdrops give a competent cast&nbsp;... little chance to elevate their material above the ordinary. Director John Gilling, who also co-scripted, ensures the thriller is competent in all departments if no more."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 