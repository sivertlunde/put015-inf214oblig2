Psychomania
 
{{Infobox film
| name           = Psychomania
| image          = Psychomania Poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Don Sharp
| producer       = Andrew Donally
| writer         = Arnaud dUsseau Julian Zimet
| starring       = George Sanders Nicky Henson Mary Larkin Ann Michelle Roy Holder John Cameron
| cinematography = Ted Moore Richard Best
| distributor    = 
| released       = 1973
| runtime        = 95 min.
| country        = United Kingdom English
}}

Psychomania (1973 in film|1973) is a British horror film|horror-cult film starring Nicky Henson, Beryl Reid, George Sanders (in his final film) and Robert Hardy.

Frequently revisited by nostalgic modern audiences for scenes filmed in the (now demolished and rebuilt) Hepworth Way shopping centre and Wellington Close housing block in Walton-on-Thames, Surrey,  it is also more obscurely known as Death Wheelers Are... Psycho Maniacs in Australia and The Death Wheelers in the United States.

==Plot== Alex of A Clockwork Orange fame. Like Alex, he has an unusual pet (a toad) and a distinctive catch phrase, "hello, little green friend."

Tom is the leader of a violent teen gang, who ride motorcycles, dabble in black magic and call themselves "The Living Dead". In a similar vein, his mother and her sinister butler get their kicks out of holding séances in their home. With her help (and following in his fathers footsteps), Tom makes a pact with the devil to return from the dead. One by one, he and his fellow bikers commit suicide with the goal of returning as one of the "undead". They failed, but the ones who do return gather together at a secret place called "The Seven Witches" (a circle of standing stones), after which they continue to terrorize the locals.

==Soundtrack== John Cameron was released on LP and CD in 2003 by Trunk Records.  

Two of Camerons pieces from the score—"Witch Hunt (Title Theme from the Film Psychomania)" and "Living Dead (Theme from the Film Psychomania)"— were released in 1973 as a 7" single on the Jam label, using the artist name "Frog." This Frog record was reissued in 2011 by Spoke Records as a limited edition vinyl 7". 

==Cast==
* George Sanders as Shadwell 
* Nicky Henson as Tom Latham
* Mary Larkin as Abby Holman
* Ann Michelle as Jane
* Roy Holder as Bertram 
* Denis Gilmore as Hatchet 
* Miles Greenwood as Chopped Meat 
* Peter Whitting as Gash 
* Rocky Taylor as Hinky 
* Robert Hardy as Chief Inspector Hesseltine 
* Patrick Holt as Sergeant 
* Alan Bennion as Constable 
* John Levene as Constable 
* Beryl Reid as Mrs. Latham  Roy Evans as Motorist 
* Bill Pertwee as Publican  Denis Carey as Coroners Assistant 
* June Brown as Mrs. Pettibone 
* Martin Boddey as Coroner 
* Heather Wright as Girl with Parcels  Larry Taylor as Lorry driver (uncredited)

== References ==
 

==External links==
*  
*  
*  
* http://www.britishhorrorfilms.co.uk/psychomania.shtml
* http://www.spokerecords.co.uk/releases_spk1202_frog.html
* http://psychomania.bondle.co.uk

 

 
 
 
 
 
 
 
 