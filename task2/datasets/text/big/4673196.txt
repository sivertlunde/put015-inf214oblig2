Barbie in the Nutcracker
{{Infobox film
| name           = Barbie in the Nutcracker (2001 film)
| image          = BarbieNutcracker.jpg
| image_size     =
| caption        = DVD cover
| director       = Owen Hurley
| producer       = Jesyca Durchin and Jennifer Twiner McCaron
| writer         = Linda Engelsiepen Hilary Hinkle
| screenplay     = Rob Hudnut
| based on       =   The Nutcracker by Pyotr Ilyich Tchaikovsky  
| narrator       =
| starring       = Kelly Sheridan Tim Curry Kirby Morrow Chantal Strand
| music          = Pyotr Ilyich Tchaikovsky
| editing        = Anne Hoerber Mattel Entertainment Universal Studios Home Entertainment (re-release)
| released       =  
| runtime        = 78 minutes
| country        = United States Canada
| language       = English
| budget         = 
}} Barbie film computer animated Barbie films, all of which feature the voice of Kelly Sheridan as the Barbie. The story is loosely adapted from E. T. A. Hoffmanns story The Nutcracker and the Mouse King and music based from Pyotr Ilyich Tchaikovsky|Tchaikovskys ballet The Nutcracker. The film includes a substantial amount of motion capture, including dances performed by members of the New York City Ballet and choreography by Peter Martins.

==Plot==

The frame story is of Barbies young friend, Kelly, having trouble performing a ballet move and her fears of going onstage. Barbie tells her a story of The Nutcracker to cheer her up.

A girl named Clara lives with Drosselmeyer, her stern grandfather, and Tommy, her younger brother. On Christmas Eve, they receive a surprise visit from their Aunt Elizabeth, who comes with gifts for her niece and nephew. Clara receives a Nutcracker that Aunt Elizabeth tells her contains the heart of a prince. Tommy tries to grab the doll from Clara, but in the scuffle, the Nutcrackers arm is broken.

Clara fixes the Nutcracker and falls asleep near the Christmas tree. She awakens to see her Nutcracker suddenly alive and fighting the Mouse King and his mouse army. When Clara tries to help, the Mouse King shrinks her down to his size, though he is still unable to defeat them and temporarily retreats.

The Nutcracker explains that he needs to find the Sugarplum Princess who is the only person who can stop the Mouse King from taking over his world. The wise owl of the grandfather clock advises Clara to follow the Nutcracker, since the Sugarplum Princess is the only one who can make Clara her original size again. The owl also gives Clara a locket that has the power to send Clara home after they find the Sugarplum Princess.

The two journey into the Land of Parthenia in the Gingerbread Village, where the children tells them that the rightful heir to the throne, Prince Eric, has gone missing. Clara realizes that her Nutcracker is the missing prince and he reveals that his previous careless attitude lead the king to pronouncing the Mouse as acting king until Eric accepted his responsibilities. The Mouse decided he wanted to be king for good and put a spell on Eric, turning him into a Nutcracker. Eric hopes to redeem himself by finding the Sugarplum Princess and make things right again.

Clara and the Nutcracker are joined by Major Mint and Captain Candy. They cross the Sea of Storms with the purple horse named Marzipan, and arrive at the Sugarplum Princess Island. Clara is separated from the group, who are all captured by the Mouse Kings bats, and she ventures alone to the Mouse Kings palace to free her friends. After being rescued, the Nutcracker fights a final battle with the Mouse King, during which the Mouse King is hit with his own spell resulting in his scepter to disintegrate and shrunk into the size of a real mouse. He flees into the sewers.

The Nutcracker was injured in the battle and Clara kisses him, whereupon he is restored to his true form as Prince Eric. Clara, because she was able to break the spell, is revealed as the Sugarplum Princess. Eric is crowned king and the couple, who have fallen in love, dance as the citizens of the Land of Parthenia celebrate their victory. At this time the shrunken Mouse King makes one more attempt to defeat Clara, stealing her heart-shaped locket and opening it. The Mouse King is knocked to the ground by a snowball and apparently dies, but Clara disappears, having been magically returned home.

Clara wakes up in the living room where she fell asleep. The Nutcracker is missing, and she runs to her grandfather, who dismisses the story as her imagination. Just then, Aunt Elizabeth returns with a young man – it is Eric. Revealing him to be the son of a friend, she insists that he stay for dinner and drags Grandfather Drosselmeyer and Tommy away. Eric gives her back the locket and asks her to dance. She answers, "I couldnt say no to the King", and the couple waltz together. A snow-globe shows the Prince – now a king – and the Sugarplum Princess dancing happily in the Palace courtyard.

The story goes back to Kelly and Barbie. Kelly realizes the importance of not giving up and makes one more attempt at the ballet move she just cant do. Kelly and Barbie dance to the music and Kelly finally gets her steps right.

==New York City Ballet dancers==
* Charles Askegard
* Maria Kowroski
* Benjamin Millepied
* Nora Y. Mullman
* Abi Stafford
* Yue Nhice Fraile

==Voice cast==
* Kelly Sheridan as Barbie/Clara
* Kirby Morrow as The Nutcracker/Prince Eric
* Tim Curry as The Mouse King
* Peter Kelamis as Pimm
* Christopher Gaze as Major Mint
* Ian James Corlett as Captain Candy
* Chantal Strand as Kelly
* Kathleen Barr as Aunt Elizabeth Drosselmayer/Owl
* French Tickner as grandfather Drosselmayer
* Alex Doduk as Tommy
* Britt McKillip as Peppermint girl
* Danny McKinnon as Gingerbread boy
* Shona Gailbraith as Fairies

==Theatrical release==
On October 23, 2001 the movie had a limited release in cinemas for a week.

=== DVD/VHS Release ===

The DVD and VHS was released on October 23, 2001. The new DVD version of the movie was released on October 5, 2010.

==Television showings==
The film was edited down to one hour for its television showings. 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 