The Man with the Iron Fists 2
{{Infobox film
| name           = The Man with the Iron Fists 2
| image          = The Man with the Iron Fists 2.jpg
| alt            = 
| caption        = DVD cover
| director       = Roel Reiné
| producer       = Marc Abraham Ogden Gavanski  RZA John Jarrell 
| story          = RZA
| starring       = RZA Sahajak Boonthanakit Pim Bubear Ocean Hou Grace Huang Andrew Lin Khiri Steven Lowenstein Dustin Nguyen
| music          = Howard Drossin
| cinematography = Roel Reiné
| editing        = Radu Ion Charles Norris 
| studio         = Arcade Pictures A Strike Entertainment
| distributor    = Universal Pictures Home Entertainment
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 RZA and John Jarrell. It is the sequel to the 2012 film The Man with the Iron Fists. The film stars RZA, Sahajak Boonthanakit, Pim Bubear, Ocean Hou, Grace Huang, Andrew Lin and Khiri Steven Lowenstein. The film was released on DVD and Blu-ray on April 14, 2015. 

== Plot ==
In Nineteenth century China, sometime after the first Film The Blacksmith Thaddeus (RZA) leaving Jungle Village is wounded and travels down river to Si Fu Village. He is found by a local miner Li Kung (Dustin Nguyen) and his Family where they offer the Blacksmith refuge. While he is healing, Thaddeus finds himself in the middle of a conflict between the towns Miners and The Beetle Clan. Led by the evil Master Ho (Carl Ng), who has taken control over Si Fu Village from the towns Mayor (Cary-Hiroyuki Tagawa). With Thaddeus at his side, Kung reawakens his past and transforms into a deadly warrior once again to save his Family and Village in this martial arts epic.

== Cast == RZA as The Blacksmith / Thaddeus Henry Smith / The Man with the Iron Fists
*Cary-Hiroyuki Tagawa as The Mayor
*Carl Ng as Master Ho
*Dustin Nguyen as Li Kung
*Eugenia Yuan as Ah Ni
*Pim Bubear as Innocence
*Ocean Hou as Shou
*Grace Huang as Gemini Female
*Andrew Lin as Gemini Male
*Khiri Steven Lowenstein as Bing		
*Charlie Ruedpokanon as Li Guang
*Sahajak Boonthanakit as Bolo
*Simon Yin as Cha Pao

==Production== RZA announced the sequel, saying: "I just finished writing it. I just wrote the new screenplay and gave it to a new director. I think it’s going to go. Some of the same characters will be back." 

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 