Le Juge Fayard dit Le Shériff
{{Infobox film
| name     = Le Juge Fayard dit Le Shériff
| image    =
| director = Yves Boisset
| producer = Lise Fayolle Yves Gasser Yves Peyrot 
| writer = Yves Boisset   Claude Veillot
| starring = Patrick Dewaere   Aurore Clément
| cinematography = Jacques Loiseleux
| music = Philippe Sarde
| editing = Albert Jurgenson Laurence Leininger 		
| country = France
| language = French
| runtime = 112 minutes
| released =  
}}
Le Juge Fayard dit Le Shériff is a 1977 French crime film written and directed by Yves Boisset. The film was inspired by the death of François Renaud.

== Cast ==
* Patrick Dewaere as le juge Fayard 
* Aurore Clément as Michèle Louvier 
* Philippe Léotard as linspecteur Marec 
* Michel Auclair as Simon Pradal, le Docteur 
* Jean Bouise as le procureur général Arnould 
* Daniel Ivernel as Marcheron 
* Jean-Marc Bory as Monsieur Paul, alias Lucien Degueldre
* Marcel Bozzuffi as Joanno

== External links ==
* 

 
 
 
 
 
 
 


 
 