Pietà (film)
{{Infobox film name           = Pietà  image          = Pieta poster.jpg caption        = Theatrical release poster film name      = {{Film name hangul         =   rr             = Pieta mr             = P‘iet‘a}} director       = Kim Ki-duk producer       = Kim Soon-mo   Kim Ki-duk writer         = Kim Ki-duk starring       = Lee Jung-jin   Jo Min-su music          = Park In-young cinematography = Cho Young-jik editing        = Kim Ki-duk distributor  NEW   Drafthouse Films released       =   runtime        = 104 minutes country        = South Korea  language       = Korean budget         =  gross          =   
}}
Pietà ( ) is a 2012 South Korean film. The 18th feature written and directed by Kim Ki-duk, it depicts the mysterious relationship between a brutal man who works for loan sharks and a middle-aged woman who claims that she is his mother, mixing Christian symbolism and highly sexual content.     
 Cannes and Berlin International Film Festival|Berlin.        
 Italian Pietà (piety/pity), signifying depictions of the Virgin Mary cradling the corpse of Jesus.

==Plot==
Kang-do is a heartless man who has no living family members and whose job is to threaten debtors to repay his clients, the loan sharks who demand a 10x return on a one month loan. To recover the interest, the debtors would sign an insurance for handicap, and Kang-do would injure the debtors brutally to file the claim. One day he receives a visit from a strange, middle-aged woman claiming she is his long-lost mother. Over the following weeks, the woman stubbornly follows him and he continues to do his job. But he is slowly moved and changed by the motherly love expressed from this woman. 

==Cast==
*Lee Jung-jin ... Lee Kang-do
*Jo Min-su ... Jang Mi-sun 
*Kang Eun-jin ... Myeong-ja, Hun-cheols wife
*Woo Gi-hong ... Hun-cheol
*Cho Jae-ryong ... Tae-seung 
*Lee Myeong-ja ... Mother of man who committed suicide using drugs 
*Heo Jun-seok ... Man who committed suicide Kwon Se-in ... Man with guitar 
*Song Mun-su ... Man who committed suicide by falling
*Kim Beom-jun ... Myeongdong man
*Son Jong-hak ... Loan shark boss 
*Jin Yong-ok ... Wheelchair man
*Kim Seo-hyeon ... Old woman 
*Yu Ha-bok ... Container man 
*Seo Jae-gyeong ... Kid
*Kim Jae-rok ... Monk 
*Lee Won-jang ... Sang-gu, committed suicide by hanging
*Kim Sun-mo ... Jong-dos neighbour
*Kang Seung-hyeon ... neighbouring shop owner 
*Hwang Sun-hui ... old woman

==Release==
Pietà premiered in competition at the Venice Film Festival on September 4, 2012.        It received theatrical release in South Korea on September 6, 2012.     
 Drafthouse Films is doing a theatrical release in North America. 
 Foreign Language Film submission to the 85th Academy Awards, but it did not make the final shortlist.      

==Sexual violence==
The films depiction of the violence and sexuality between Kang-do and the woman who claims to be his long-lost mother have provoked intense reactions and is debated by critics.   Some of the most controversial scenes in the film includes when Kang-do feeds the woman a piece of his own flesh from his thigh,    and a scene when he shoves his hand into the womans private area, molesting her, and asking her "I came out of here? Can I go back in?".  There is another subsequent scene when she gives Kang-do a handjob. 

==Reception== Michael Mann, who presided over the jury, said the film stood out because it "seduced you viscerally."   

Deborah Young of The Hollywood Reporter described it as "an intense and, for the first hour, sickeningly violent film that unexpectedly segues into a moving psychological study."    Young gave high praises to the films acting performances, however states "it’s not an exaggeration to say there’s not a single pleasant moment in the film’s first half" and "Viewers will keep their eyes closed" for the majority of the film.  Young further praised the visual style of the film with "Kim gives scenes a dark, hand-held look in which the frame edge disappears into black shadows. It’s not a particularly attractive style but does reflect the ugliness of its subject." 

Leslie Felperin of Variety (magazine)|Variety describes it as the directors "most commercial pic in years" though it nonetheless features the directors usual trademarks of "brutal violence, rape, animal slaughter and the ingestion of disgusting objects."    Felperin further states the film is a "blend of cruelty, wit and moral complexity." 

Dan Fainaru of Screen International states "Starting with a grisly suicide and ending with a burial, this isn’t an easy or pleasant film to watch." 

Oliver Lyttelton of IndieWire praised the two lead actors performances and their on-screen chemistry as mother and son: "there’s a real tenderness to the two performances, particularly that of Lee, who reverts from a strong-and-silent brute to easing into the childhood that he never got to live. And the disturbing, vaguely Oedipal relationship at the core is a fascinating one..."    However Lyttelton gave the film a C+, and criticizes "Its a shame then, that in the second half of the film, the interestingly twisted mother-son relationship shifts gears and becomes something closer to the kind of revenge movie that Korean cinema has become known for. Its not quite a full-on genre exercise, but it’s probably the closest to such a thing that Kims ever made, and while he has his own twists to provide, its still a disappointingly conventional turn for the film to take." 

==Awards== 69th Venice Film Festival
*Golden Lion

2012 32nd Korean Association of Film Critics Awards 
*Best Film
*Best Director - Kim Ki-duk
*Best Actress - Jo Min-su
*FIPRESCI Korean Branch

2012 49th Grand Bell Awards 
*Best Actress - Jo Min-su
*Special Jury Prize - Kim Ki-duk

2012 Korean Popular Culture and Art Awards  
*Order of Cultural Merit (Korea)#Eun-gwan Medal .28Silver Crown.29|Eun-gwan Order of Cultural Merit - Kim Ki-duk Okgwan Order of Cultural Merit - Jo Min-su Okgwan Order of Cultural Merit - Lee Jung-jin

2012 6th Asia Pacific Screen Awards 
*Screen International Jury Grand Prize - Jo Min-su
 33rd Blue Dragon Film Awards     
*Best Film

2012 2nd Shin Young-kyun Arts and Culture Foundations Beautiful Artist Awards 
*Grand Prize - Kim Ki-duk

2012 Korean Art Critics Conference  
*Best Artist Award - Kim Ki-duk

2012 Women in Film Korea Awards  
*Best Technical Award - Park In-young (music director)
 2012 Satellite Awards 
*Satellite Award for Best Foreign Language Film

2012 9th Dubai International Film Festival  
*Best Director (Muhr AsiaAfrica) - Kim Ki-duk

2012 Korea Film Actors Association  
*Lifetime Achievement Award - Kim Ki-duk
*Achievement Award - Jo Min-su
*Achievement Award - Lee Jung-jin

2013 4th KOFRA Film Awards (Korea Film Reporters Association)   
*Best Film of 2012
*Best Actress - Jo Min-su

2013 23rd Fantasporto Directors Week  
*Best Film
*Best Actress - Jo Min-su
 7th Asian Film Awards        
*Peoples Choice for Favorite Actress - Jo Min-su

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of South Korean submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 