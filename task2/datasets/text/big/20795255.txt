Holiday on the Buses
 
 
 
{{Infobox Film
| name           = Holiday on the Buses
| image          = "Holiday_on_the_Buses"_(1973).jpg
| image_size     = 
| caption        = 
| director       = Bryan Izzard Ronald Chesney Ronald Wolfe
| writer         = Ronald Chesney Ronald Wolfe Bob Grant Stephen Lewis Adam Rhodes
| music          = Denis King
| cinematography = Brian Probyn
| editing        = James Needs
| distributor    = EMI Films#MGM-EMI|MGM-EMI
| released       =  
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
}} On the Ronald Chesney Hammer Films.

==Plot==
 Bob Grant) Stephen Lewis) are sacked. Stan and Jack get a job as bus crew at a Pontins holiday camp in Prestatyn, North Wales, only to find that Blakey has also got a job at the camp, as security inspector. Stan invites the family to stay (using his staff discount) whilst he proceeds to chat up the guests and staff.  Tight fisted Arthur refuses to pay the train fare, instead relying on his ever present motorcycle and sidecar to take the Butlers to the camp.  They meet Stan on the road, who accidentally forces them into a kerb causing all the luggage to land in a river and all the familys clothes are ruined.  

Meanwhile, Stan and Jack embark on their usual misadventures whilst trying to evade Blakeys watchful eye, who is also trying to teach an old-time dancing class with the camps guests in his spare time.  Stans attempts to snare the affections of Mavis, a female guest are repeatedly thwarted by her overbearing mother, and then his own family obligations to babysit Little Arthur, who sprays the inside of the chalet with ink.  The Butlers then have to try and redecorate the chalet without Blakey finding out.  Stans final attempt at seducing Mavis fails when the group goes on a boat cruise in a stormy sea, where Stan succumbs to seasickness, whilst Jack, as ever manages to steal Stans love.

Arthur manages to patch up the damage to the chalet, but the new paint is ruined a second time by Olive losing her glasses and putting handprints all over the walls.   Arthur uses petrol from his motorcycle to clean the paint brushes, but neglects to tell anyone olive poured it down the toilet, when Stans carelessly discarded cigarette causes the toilet to explode.  He and Jack set up a diversion to keep Blakey busy whilst they steal a toilet from the camps stores and repair the damage.

Despite these mishaps, the Butler family’s holiday ends with reasonable success, and a new batch of guests arrive at the camp.  Blakey gets fired by the manager for his romantic misadventures with the camp nurse, whilst Stan and Jack set their sights on two more female guests.  They borrow the bus for a evening trip to the beach, but the bus sinks in the sand and is submerged when the tide comes in.  They are sacked and end up on the dole again; where Stan finds to his horror that Blakey is now a clerk at the labour exchange.  Blakey initially gloats over Stan’s predicament, then offers him a job driving a wrecking ball – as it is appropriate for someone who is "always smashing things up".

==Cast==
* Reg Varney - Stan Butler
* Doris Hare - Mum
* Michael Robbins - Arthur Rudge
* Anna Karen - Olive Rudge Stephen Lewis - Inspector Blakey Blake Bob Grant - Jack Harper
* Wilfrid Brambell - Bert Thomsson Kate Williams - Nurse
* Arthur Mullard - Wally Briggs
* Queenie Watts - Lily Briggs
* Henry McGee - Holiday Camp Manager
* Adam Rhodes - Little Arthur
* Michael Sheard - Depot manager
* Hal Dyer - Mrs. Coombs
* Franco De Rosa - Luigi
* Gigi Gatti - Maria
* Eunice Black - Mrs Hudson
* Maureen Sweeney - Mavis
* Sandra Bryant - Sandra
* Carolae Donoghue - Doreen
* Tara Lynn - Joyce Alex Munro - Patient

==Production notes==
It was filmed on location at The Pontins Holiday Camp, Prestatyn, North Wales. Interiors were completed at Elstree Studios#Elstree Studios, Borehamwood|EMI-MGM Elstree Studios, Borehamwood.  The film was also Michael Robbins and Reg Varneys final appearances in the On The Buses franchise; having already quit the TV series for its final season which aired the same year.

The open top bus was XFM 229, a Bristol LD new to Crosville Motor Services.

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 