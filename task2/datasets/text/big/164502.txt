The Adventures of Baron Munchausen
 
 
 
{{Infobox film
| name = The Adventures of Baron Munchausen
| image = Adventures of baron munchausen.jpg
| image_size = 
| alt = 
| caption = Theatrical release poster
| director = Terry Gilliam
| producer = Thomas Schühly Ray Cooper Jake Eberts David Tomblin Stratton Leopold
| writer = Terry Gilliam Charles McKeown John Neville Sarah Polley Eric Idle Jonathan Pryce Oliver Reed Uma Thurman
| music = Michael Kamen
| cinematography = Giuseppe Rotunno
| editing = Peter Hollywood
| studio = Allied Filmmakers
| distributor = Columbia Pictures
| released =  
| runtime = 126 minutes  
| country = United Kingdom
| language = English
| budget = $46.63 million 
| gross = $8,083,123 US 
}} adventure Fantasy fantasy comedy John Neville, Sarah Polley, Eric Idle, Jonathan Pryce, Oliver Reed, and Uma Thurman.

Based on the tall tales about the 18th-century German nobleman Baron Munchausen and his wartime exploits against the Ottoman Empire, the film was critically well-received but was a failure at the box office.

==Plot== Sting in a cameo) who had just accomplished a near-superhuman feat of bravery, claiming that his bravery is demoralizing to other soldiers.

Not far into the play, an elderly man claiming to be the real Baron interrupts the show, protesting its many inaccuracies. Over the complaints of the audience, the theatre company and Jackson, the "real" Baron gains the houses attention and narrates through flashback an account of one of his adventures, of a life-or-death wager with the Grand Turk, where the younger Barons life is saved only by his amazing luck plus the assistance of his remarkable associates: Berthold, the worlds fastest runner; Adolphus, a rifleman with superhuman eyesight; Gustavus, who possesses extraordinary hearing, and sufficient lung power to knock down an army by exhaling; and the fantastically strong Albrecht.

When gunfire disrupts the elderly Barons story, Jackson cancels the acting troupes contract because of the Baron. The Baron wanders backstage, where the Angel of Death tries to take his life, but Sally Salt, the young daughter of the theater companys leader, saves him and persuades him to remain living. Sally races to the wall yelling for the Turkish army to go away, and the Baron accidentally fires himself through the sky using a mortar and returns riding a cannonball, narrowly escaping the Angel of Death once again. Insisting that he alone can save the city, the Baron escapes over the citys walls in a hot air balloon constructed of womens underwear, accompanied by Sally as a stowaway.

The balloon expedition proceeds to the Moon, where the Baron, who has grown younger, finds his old associate Berthold, but angers the King of the Moon, a giant with separate minds in his head and body, who resents the Baron for his romantic past with the Queen of the Moon. The death of the Kings body, and a bungled escape from the Moon brings the trio back to (and beneath) the Earth, where the Roman God Vulcan hosts his guests with courtesy and Albrecht is found. The Baron and Vulcan (mythology)|Vulcans wife, the Goddess Venus (mythology)|Venus, attempt a romantic interlude by waltzing in the air, but this cuts short the hospitality and Vulcan expels the foursome from his kingdom into the South Seas.

Swallowed by an enormous sea creature from the play earlier, the travelers locate Gustavus, Adolphus, and the Barons trusty horse Bucephalus. The Baron (who again appears elderly after being "expelled from a state of bliss") struggles with the conflicting goals of heroism and a peaceful death, encountering the Angel of Death for the third time. Finally they escape by blowing "a modicum of snuff" out into the sea creatures cavernous interior, causing it to sneeze the heroes out through its whale-like blowhole. The Baron, young once again, sails to where the Turkish army is located but the Barons associates are too elderly and tired to fight the Turk as in the old days.

The Baron lectures them firmly but to no avail, and he storms off intending to surrender to the Turk and to Jackson. His cohorts rally to save the Baron, and through a series of fantastic acts they rout the Turkish army and liberate the city. During the citys celebratory parade, the Baron is shot dead by Jackson and the Angel of Death appears a final time to take the Barons life. An emotional public funeral takes place, but the denouement reveals that this is merely the final scene of yet another story the Baron is telling to the same theater-goers who were attending the theater in the beginning of the film. The Baron calls the foregoing "only one of the many occasions on which I met my death" and closes his tale by saying "everyone who had a talent for it lived happily ever after."

An ambiguous finale reveals that the city has indeed been saved, even though the events of the battle apparently occurred in a story rather than the films reality. The Baron rides off on Bucephalus. As the Baron and Bucephalus are bathed in the light of the sun parting the clouds, they apparently disappear, and the credits roll over a triumphant blast of music.

==Cast==
  John Neville as Baron Munchausen
* Sarah Polley as Sally Salt
* Eric Idle as Berthold / Desmond
* Jonathan Pryce as The Right Ordinary Horatio Jackson Vulcan
* Venus / Rose Bill Paterson as Henry Salt
* Charles McKeown as Rupert / Adolphus
* Winston Dennis as Bill / Albrecht Jack Purvis as Jeremy / Gustavus
* Ray D. Tutto (Robin Williams) as The King of the Moon  . Q: Why is Robin credited in some projects as Ray D. Tutto, Marty Fromage and Sudy Nim? A: These are all in-jokes related to Robins cameo appearances. In The Adventures of Baron Munchausen, Robins King of the Moon character proclaims himself "re di tutto" ("king of everything" in Italian).   
* Valentina Cortese as Queen Ariadne / Violet
* Peter Jeffrey as Sultan Mahmud I
* Alison Steadman as Daisy
* Ray Cooper as Functionary
* Don Henderson as Commander Sting (Cameo appearance|cameo) as Heroic officer
* Terry Gilliam (uncredited) as Irritating singer 
 

==Production==
===Background===
 
  (or Baron Munchausens Narrative of his Marvellous Travels). The tales were further embellished and translated back to German by Gottfried August Bürger in 1786.  These tales were frequently extended and translated throughout the 19th century, further fictionalized in the 1901 American novel Mr. Munchausen.
 The Adventures of Baron Munchausen (1943, Josef von Báky with script by Erich Kästner), The Fabulous Baron Munchausen (1961, Karel Zeman), and The Very Same Munchhausen (1979) directed by Mark Zakharov, who depicted Munchausen as a tragic character, struggling against the conformity and hypocrisy of the world around him.

===Budget===
The film was over budget; what was originally $23.5 million,  grew to a reported $46.63 million.   Gilliam, acknowledging he had gone over budget, said its final costs had been nowhere near $40 million. 

In The Madness and Misadventures of Munchausen (included on the bonus DVD of the 20th Anniversary Edition of Munchausen), producer Thomas Schühly said that as part of a deal with 20th Century Fox before it ended up with Columbia, a budget plan had been set up of $35 million, "and its strange, the   final cost was 35  .   We always had a budget of 34 or 35 million, the problem was when I started to discuss it with Columbia, Columbia would not go beyond 25.   Everybody knew from the very beginning that this cutting out was just a fake.   The problem was that David Puttnam got fired, and all these deals were oral deals.   Columbias new CEO, Dawn Steel, said Whatever David Puttnam   said before doesnt interest me".

Regarding the new regimes apparent animosity towards all of Puttnams projects and Munchausen, Gilliam added in the same documentary, "I was trying very hard to convince Dawn Steel that this was not a David Puttnam movie, it was a Terry Gilliam movie." Similarly Kent Houston, head of Peerless Camera doing the films special effects said in Madness and Misadventures that they were promised a bonus if they would finish the effects in time, but when they approached the person again when they were done, he was met with the reply, "Im not gonna pay you, because I dont want to seem to be doing anything that could benefit Terry Gilliam."

===Experience===
Munchausen is the third entry in Gilliams "Trilogy of Imagination", preceded by Time Bandits (1981) and Brazil (1985 film)|Brazil (1985).  All are about the "craziness of our awkwardly ordered society and the desire to escape it through whatever means possible." Matthews, Jack (1996). "Dreaming Brazil" (essay accompanying The Criterion Collection DVD release).  All three films focus on these struggles and attempts to escape them through imagination: Time Bandits, through the eyes of a child, Brazil, through the eyes of a man in his thirties, and Munchausen, through the eyes of an elderly man. 

When the production finally came to a successful closure, several of the actors commented on the rushed tightness of the whole project. Said Eric Idle, "Up until Munchausen, Id always been very smart about Terry Gilliam films. You dont ever be in them. Go and see them by all means - but to be in them, fucking madness!!!" http://www.smart.co.uk/dreams/bmfact.htm 

Sarah Polley, who was nine years old at the time of filming, described it as a traumatic experience. " t definitely left me with a few scars ... It was just so dangerous. There were so many explosions going off so close to me, which is traumatic for a kid whether its dangerous or not. Being in freezing cold water for long periods of time and working endless hours. It was physically grueling and unsafe." 

Production designer Ferretti afterwards compared Gilliam to his former director, saying, "Terry is very similar to Fellini in spirit. Fellini is a wilder liar, but thats the only difference! Terry isnt a director so much as a film author. He is open to every single idea and opportunity to make the end result work. Often the best ideas have come out of something not working properly and coming up with a new concept as a result. He is very elastic and thats one quality in a director that I admire the most." 

==Release==
;United States
When The Adventures of Baron Munchausen was finally completed, David Puttnam, who had obtained the films US distribution rights for Columbia Pictures, had been replaced as CEO of Columbia; coupled with Gilliams prior quarrels with major studios over Brazil (1985 film)|Brazil, the film saw only very limited distribution in the US, earning $8 million in US box office.     

In Madness and Misadventures, Robin Williams commented on the low number of release prints that Columbia produced, saying "  regime was leaving, the new one was going through this, and they said, This was their movies, now lets do our movies! It was a bit like the new lion that comes in and kills all the cubs from the previous man."

In a 2000 interview with IGN, Gilliam said about the contemporary press perception of the film being a financial disaster how "It seemed actually appropriate that Munchausen – the greatest liar in the world – should be a victim of some of the greatest liars in the world." He compared the films budget problems to the even more serious problems of Were No Angels (1989 film)|Were No Angels that commonly go unmentioned, and he went on to declare its difficulties as a mixture of "trade press" still being upset about Gilliams battle with Universal over Brazil, nepotism, and an intrigue on behalf of Ray Stark successfully trying to have Puttnam removed from Columbia, coupled with the fact the studio was being sold at the time:

 The negative stories about the shoot that were turning up in the Hollywood press were coming, we found out later, from a source at Film Finances – which was the completion bond company on the film. Their lawyer was a guy named Steve Ransohoff, whose father was Martin Ransohoff – who was Ray Starks friend and partner.   I thought it was quite extraordinary, because the stories were doing two things – they were making me and the whole project look like it was completely out of control and all my fault, and that Film Finance, the completion guarantors, were the only thing holding it together – the people trying to bring control to it... the fact was, they were absolutely useless. 

 The ultimate fact was that when the film was ultimately released, there were only 117 prints made for America – so it was never really released. 117 prints! ...an art film gets 400. We were ultimately the victim of Columbia Tri-Star being sold to Sony, because at that time all they were doing was trying to get the books looking as good as possible. We werent the only film that suffered, but we were the most visible one. And what happened – to complete the story in a neat and tidy way – was that they were not spending any money on advertising to promote any of the movies started by the previous regime – by Putnams regime. They were burying films left right and center by spending no money on them – and the books looked really good at the end of that. 
 Last Emperor. We actually opened well in the big cities – we opened really well. A friend who had bought the video rights said he had never seen anything so weird – Columbia was spending their whole time looking at exit polls to prove the film would not work in the suburbs, and so it would be pointless to make any more prints. He said, "Ive never seen anything like this." There it was. Then it becomes this kind of legend – which it deserves to be... even if its the wrong legend.    

;Europe Neue Constantin Film were able to give it a more appropriate release, and subsequently on home video, actually in 1999 becoming the very first feature DVD issued by Columbia. 

==Critical reception==
The Adventures of Baron Munchausen has 92% positive reviews on review aggregator Rotten Tomatoes,  and on Metacritic has a score of 69 out of 100 (out of 15 reviews on display, 11 give it a favorable 70-100% rating). 

Regarding the obvious gap between the movies troubled production and its eventual triumph of aesthetic cinematic form on the screen, Jeff Swindoll wrote in his 2008 DVD Review of Munchausen for Monsters and Critics: "For the absolute hell that the production of the film turned out to be, you really dont see any of that tension on the screen.   the film is a fantastic, whimsical treat.   Baron Munchausen is full of whimsy, fantasy, bright colors, and fabulous characters. None is as fantastic as the Baron himself as played, with a twinkle in his eye, by the grand John Neville." 

Roger Ebert gave the film 3 out of 4 stars and found that it was "told with a cheerfulness and a light touch that never betray the time and money it took to create them", appreciating "the sly wit and satire that sneaks in here and there from director Terry Gilliam and his collaborators, who were mostly forged in the mill of Monty Python". While considering the films special effects as "astonishing", Ebert also contended "the movie is slow to get off the ground" and "sometimes the movie fails on the basic level of making itself clear. Were not always sure who is who, how they are related, or why we should care". But "allowing for the unsuccessful passages there is a lot here to treasure", and Ebert concluded overall, "this is a vast and commodious work", "the wit and the spectacle of Baron Munchausen are considerable achievements". Additionally, Ebert considered John Nevilles title role performance as appearing "sensible and matter-of-fact, as anyone would if they had spent a lifetime growing accustomed to the incredible". 

The Washington Post called the film a "wondrous feat of imagination", though "except for Williams, the actors are never more than a detail in Gilliams compositions."   

Richard Corliss wrote: 
 Everything about Munchausen deserves exclamation points, and not just to clear the air of the odor of corporate flop sweat. So here it is! A lavish fairy tale for bright children of all ages! Proof that eccentric films can survive in todays off-the-rack Hollywood! The most inventive fantasy since, well, Brazil! You may not believe it, ladies and gentlemen, but its all true. 
 Richard Conway, who did the special effects, and Peerless Camera Company Ltd., responsible for the optical effects. Without them, Baron Munchausen would have looked about as big and as interesting as a 25-cent postage stamp." 

==Accolades==
The film was nominated for four British Academy Film Awards, winning three:
* Best Costume Design
* Best Make Up Artist
* Best Production Design
and losing Best Special Effects to Back to the Future Part II.

In 1990, the film was nominated for four Academy Awards|Oscars:    Best Art Direction (Dante Ferretti, Francesca Lo Schiavo) (lost to Batman (1989 film)|Batman) Best Costume Henry V) Best Visual Effects (lost to The Abyss) Best Makeup (lost to Driving Miss Daisy)

In 1991, the film was further nominated for four Saturn Awards: Total Recall)
* Best Fantasy Film (lost to Ghost (1990 film)|Ghost) Dick Tracy)
* Best Special Effects (lost to Back to the Future Part II)
 Silver Ribbon in three categories:
* Best Cinematography
* Best Costume Design
* Best Production Design

It was nominated in 1990 for a Hugo Award for Best Dramatic Presentation (losing to Indiana Jones and the Last Crusade); actress Sarah Polley was nominated for two Young Artist Awards in the categories Best Musical or Fantasy and Best Young Actress.

==Home media== commentary track by Gilliam and deleted scenes. The first DVD edition of the film, issued on 27 April 1999, did not include any of these or any other extras.

A 20th anniversary edition was released on DVD and Blu-ray Disc|Blu-ray on 8 April 2008. It includes a new commentary with Gilliam and co-writer/actor McKeown, a three-part documentary on the making of the film, storyboard sequences, and deleted scenes. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 