Under a Texas Moon
{{Infobox film name = Under a Texas Moon image = Under_A_Texas_Moon_1930_Poster.jpg caption = Film poster producer = director = Michael Curtiz  writer = Gordon Rigby Stewart Edward White   starring = Frank Fay Raquel Torres Myrna Loy Noah Beery music = Ray Perkins cinematography = William Rees (Technicolor) editing = Ralph Dawson distributor = Warner Bros. released =   runtime = 82 minutes language = English country = United States
}}
 1930 musical western film photographed entirely in Technicolor. It was based on the novel Two-Gun Man (from 1929) which was written by Stewart Edward White. It was the second all-color all-talking feature to be filmed entirely outdoors as well as being the second western in color and the first all-talking all-color western. The film features one theme song by the title of "Under A Texas Moon."

==Synopsis==
Frank Fay, as a Mexican named Don Carlos, rides into a small Texas border settlement on the Fourth of July in the early 1880s. He is accompanied by his two inseparable companions, played by Georgie Stone and George Cooper. The day is being celebrated in the style of a  Spanish fiesta. Fay challenges a rough Texan, played by Noah Beery, to a duel, only to find himself invited to undertake the dangerous task of capturing a cattle rustler who has been stealing cattle from the Lazy Y Ranch. He accepts the task on the promise of receiving seven thousand dollars in gold if he can return both the thief and the stolen cattle within ten days. During the next nine days, Fay spends his time making love to every pretty girl he meets, serenading many of theme by singing the theme song to the film while playing his guitar, while his two companions join in the harmonizing. He lies to them all, telling each girl exactly what she wishes to hear. Throughout all this time he does nothing towards earning his reward. On the tenth day, he captures the cattle rustler and turns up the cattle to everyones surprise by using a simple method that no one had thought of. He rides back to Mexico with his latest conquest in his arms.

==Cast==
(in credits order) Frank Fay as Don Carlos
*Raquel Torres as Raquella
*Myrna Loy as Lolita Romero
*Armida Vendrell as Dolores
*Noah Beery as Jed Parker
*Georgie Stone as Pedro George Cooper as Philipe
*Charles Sellon as José Romero Jack Curtis as Buck Johnson
*Sam Appel as Pancho Gonzalez
*Tully Marshall as Gus Aldrich
*Mona Maris as Lolita Roberto
*Francisco Marán as Antonio

==Preservation==
The film survives as a single nitrate Technicolor print, copied by the UCLA Film and Television Archive.

==Response==
New York Latinos led by Gonzalo González protested the film, characterizing it as "anti-Mexican" mainly because Frank Fay portrayed Mexicans as being liars and womanizers. Police brutalized the picketers, killing González. The murder sparked a Pan-Latino protest, in which the Latino civil rights activist Luisa Moreno participated. She later told Bert Corona that the experience "motivated her to work on behalf of unifying the Spanish-speaking communities." {{cite book
  | last = García
  | first = Mario T.
  | authorlink = 
  | coauthors = 
  | title = Memories of Chicano History: The Life and Narrative of Bert Corona
  | publisher = University of California Press
  | year = 1994
  | location = Berkeley, California
  | pages = 
  | url = http://ark.cdlib.org/ark:/13030/ft3p30052t/
  | doi = 
  | isbn = 0-520-20152-3 }} 

==See also==
*List of early color feature films

==References==
;Notes
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 