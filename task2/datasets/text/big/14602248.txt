The Mars Canon
{{Infobox Film
|  name           = The Mars Canon
|  image          = 
|  caption        = 
|  director       = Shiori Kazama
|  editing        = 
|  cinematography = 
|  music          = Masaya Abe
|  producer       = 
|  writer         = Tomoko Ogawa Syotaro Oikawa
|  starring       = 
|  distributor    = Argo Pictures
|  released       = September 28, 2002 (Japan)
|  runtime        = 121 min.
|  country        = Japan 
|  language       = Japanese 
|  budget         =
}} 2002 Japanese film directed by Shiori Kazama. Themes of the film include adultery and homosexuality, as Mars connotes both fighting and sexual intercourse.

==Synopsis==
The film explores the relationship problems of two couples, Kohei and Kinuko, and Manabe and Hijiri, and the solutions they try to devise as a way out. Kohei and Kinuko, despite their age differences, seem like a happy pair, but there is an insurmountable distance between them. Kohei is married to another woman, and Kinuko, though she knows he will never divorce, cant bring herself to break off the relationship and start anew. Manabe and Hijiri, meanwhile, start off happily enough, but eventually their passion begins to wane as Manabe starts looking to other women for sex. Hijiri, feeling rejected, moves into an apartment next door to Kinuko, where she plots to break up the mismatched couple to her own advantage.

==Cast==
*Makiko Kuno as Kinuko Takeuchi
*Mami Nakamura as Hijiri Tokita
*Fumiyo Kohinata as Kohei Deguchi
*Kiyohiko Shibukawa as Tatsuya Manabe
*Eri Hayasaka as Arimi Deguchi
*Haruku Shinozak as Fumiyo Komatsu
*Ryuichi Hiroki

==Awards==
The film won the Asian Film Award at the 2001 Tokyo International Film Festival held October 27-November 4, 2001. 

==Release==
The film was presented at the Berlin International Film Festival on February 12, 2002. It was screened at the 24th PIA Film Festival on July 1, 2002  and was released theatrically in Japan on September 28, 2002. 

==References==
 

==External links==
*  
*  
*   
* 

 
 
 
 
 


 
 