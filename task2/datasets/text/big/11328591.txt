Daemon (film)
 
 
{{Infobox Film
| name           = Daemon
| image          = 
| image_size     = 
| caption        = 
| director       = Colin Finbow
| producer       = Childrens Film Unit Colin Finbox
| writer         = Colin Finbow
| narrator       = 
| starring       = Susannah York  Bert Parnaby  Arnaud Morell
| music          = David Hewson
| cinematography = Titus Bicknell
| editing        = 
| distributor    = 
| released       = 1985
| runtime        = 
| country        = UK
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Daemon is a 1985 British horror film about a young man who is possessed by a demon. It was written and directed by Colin Finbow.  It starred Susannah York, Bert Parnaby and Arnaud Morell. It is a ghost story set in suburbia. Eleven-year-old Nick movies into a large old house with is sisters Jennie and Clare, left in the care of Helga the Swedish au pair while their parents are away in America. Nick is unhappy at his new school, where he is befriended by a boy called Sam and intimidated by scripture teacher Mr Crabb, who is interested in the occult and demonology. Nick hears voices in the house and receives messages on his computer screen (a new touch in 1986); he also suffers inexplicable blisters on his feet and grazes on his elbows and knees. When he dreams of burning and wakes up in a bed full of ashes, Nick tells a psychiatrist (Susannah York in a Mum-like name-value cameo) that he feels he is possessed by a demon. Three of his schoolmates agree and, when Crabb dies in a freak accident, resolve to drive a stake through his heart. However, the cause of the haunting turns out to be Tom, a child chimney sweep from 1839 who was burned to death in the houses chimney. After a climactic fire, Toms skeleton is discovered in the old fireplace and the ghost is laid to rest.

The symptoms that Nick and his friend interpret as those of demonic possession the blisters, grazes and fiery nightmares actually turn out to be down to his psychic sympathy with Tom, who was burned alive while crawling up a Victorian chimney. This revelation of human cruelty and injustice lurking within supernatural manifestations allows for a finale that is satisfyingly apocalyptic on a small scale.

==External links==
* 

 
 
 
 
 
 


 
 