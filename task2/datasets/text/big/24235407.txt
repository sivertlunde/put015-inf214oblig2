Munchie Strikes Back
{{Infobox film
| name           = Munchie Strikes Back
| image          = Munchie strikes back.jpg
| image_size     = 225px
| alt            = 
| caption        = 
| director       = Jim Wynorski Mike Elliott  
| writer         = Jim Wynorski R.J. Robertson
| narrator       = 
| starring       = Lesley-Anne Down Andrew Stevens Howard Hesseman
| music          = Chuck Cirino
| cinematography = Don E. FauntLeRoy
| editing        =  Roger Corman Production Concorde Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 comedy feature film directed by Jim Wynorski and written by Wynorski and R.J. Robertson, as a sequel to Munchie, with Howard Hesseman providing Munchies voice.    

==Synopsis== last adventure, Munchie must appear before a celestial court presided over by Kronus (Angus Scrimm).  The tribunal proceeds to blame Munchie for a number of historical calamities, including the sinking of Atlantis, the crash of the LZ 129 Hindenburg|Hindenburg, the catacylism of Vesuvius, the meltdown of Chernobyl.  As punishment, he is sentenced to help single mom Linda McClelland (Lesley-Anne Down).  Lindas son Chris (Trenton Knight) is the only one who can see Munchie, and Munchie and he become best buddies.  Chris is the pitcher of a Little League team, and is being bullied by  Brett Carlisle (Corey Mendelsohn), who is his rival in baseball as well as for the attentions of the girl-next-door Jennifer (Natanya Ross).  While out of town on a business trip, Lindas boss makes improper advances to her. When she refuses his advances, he fires her. This creates financial hardship on the family.  Munchie intervenes in their lives, making it so Chris has a perfect game and impresses Jennifer, and Linda finds a bag of money to prevent eviction.  Munchie appears back before the tribunal, who assign his next punishment... to help Bill Clinton.

==Partial cast==
*Lesley-Anne Down as Linda McClelland 
*Andrew Stevens as Shelby Carlisle 
*Howard Hesseman as Munchie (voice) 
*Angus Scrimm as Kronas 
*John Byner as Coach Elkins 
*Steve Franken as Professor Graves 
*Natanya Ross as Jennifer 
*Toni Naples as Newscaster 
*Trenton Knight as Chris McClelland 
*Ace Mask as Mr. Poyndexter 
*Cory Mendelsohn as Bret 
*Antonia Dorian as Cleopatra 
*Jamie McEnnan as Gage Dobson  

==Critical response==
Reviewers generally found the movie a poor sequel to Munchie. TV Guide offered that Munchie Strikes Back "...turns an annoying, no-budget childrens movie into an annoying, no-budget childrens series," and in considering the films attempts at humour, wrote the film "is the sort of ill-considered comedy in which a Little League team can be named the Hillside Stranglers, and big laughs are expected from the sight of a woman kicking a dog."    X-Entertainment was only slightly less harsh, offering in their review "...the series saw its last hurrah in the movie were reviewing today, 1992s Munchie Strikes Back. Assuming that at least some of you have seen Munchies, throw away everything you think you know about the lore. The last two films were entirely different and strictly for kids. Its for this reason that Im having trouble calling this pile of shit a pile of shit—putting myself in the mindset of a seven-year-old, its not too terrible. Cant really recommend it as fodder for your bad movie nights (stick with the original for those), but if youve ever wanted 80 minutes worth of Howard Heeseman voicing the poor mans ALF, Munchie Strikes Back caters to your unique and pitiful whim."     John Stanley gave the film one and a half stars, denouncing it as a watered "down kiddie fantasy from the 
Roger Corman factory..." 

==References==
 

==External links==
* 
* 

 
 