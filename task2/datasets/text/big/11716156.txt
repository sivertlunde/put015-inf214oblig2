Flatland (2007 film)
 
 
{{Infobox film
| name = Flatland
| image = Flatland-Poster.jpg
| image_size =
| caption = Promotional film poster
| director = Ladd Ehlinger Jr.
| producer = F.X. Vitolo
| based on =   by Edwin A. Abbott
| writer = Tom Whalen
| narrator =  Chris Carter Megan Colleen Ladd Ehlinger Jr. Mark Slater
| cinematography = 
| editing = Ladd Ehlinger Jr
| studio = 
| distributor = 
| released = 
| runtime = 95 minutes
| country = United States English
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}} Mark Slater.

==Plot==
  1000 words --> attorney at law, struggles to instruct his grandson, A Hexagon, in the art of sight recognition. The lesson is interrupted by A Squares brother B, a clerk to President Circle, warning A to stay home during a meeting at the Senate of the Great Southern Republic of Flatland.

The Senate session has been called to discuss the increasing hostilities between the government and the Chromatist movement, led by Senator Chromatistes, an irregular dodecagon. The movement seeks legalization of the right of Flatlanders to color their sides as they see fit. Traditionally taboo, laws against it had been relaxed; this emboldened the Chromatists to demand legalization. The Great Southern Republic distinguishes itself from its enemy, the Northern Kingdom, by its stances on Chromatism and Irregulars along with a democratic government. Relaxing the laws has already been perceived as weakness by the Northern Kingdom who are massing on the borders.

Against his brother’s warning, A Square meets his new client, the first female charged as a Chromatist;  on his way home he is caught in the melee leaving the Senate. President Circle’s soldiers killed Senator Chromatistes and his supporters, sparking a riot across the city. A Square just gets home safely, then barricades his family against the chaos for the night.

Once asleep, A Square dreams of visiting a one-dimensional world, Lineland, and attempts to convince the realms ignorant king of a second dimension, but fails to make him see outside of his eternally straight line. A Square awakens to learn that the deadly riots originated in the Senate meeting that B Square was attending. A visit to B Square’s home confirms that he is missing. A Square scours the city, now under martial law, seeking B Square. At the marketplace, he meets a merchant selling a fascinating "glow point", and buys one for A Hexagon, who has been sulking since being disciplined for coloring one of his sides purple.

The family prepares for another anxious night, only to be terrified by the sudden appearance of A Sphere, CEO of Messiah, Inc. A Sphere declares that A Square is his apostle of the Three Dimensions and privately begins to explain things to him. A Square refuses to entertain the notion of three dimensions, resorting to violence with A Sphere. A Sphere decides the only way to convince A Square of the three dimensions is to show him.

Suddenly and painfully, A Square is plucked out of his dimension and into Spaceland. After the initial shock, the reality of 3 dimensions becomes clear to A Square, along with the possibility of locating his brother from his newfound vantage point. Having business himself at the Great Hall, A Sphere brings A Square to look for his brother there. On their arrival, A Sphere expounds upon three dimensions to President Circle and the Priests who anticipated this event. After rejecting A Sphere’s message and attempting to kill him, the Flatland leaders execute all who have witnessed the event, except B Square, who is imprisoned for life on pain of death in exchange for his silence while his brother watches from high above Flatland.

Realizing that time in Spaceland is short, at least for A Square, A Sphere brings him to Messiah, Inc. to finish his education on the gospel of Three Dimensions. Enthralled by the complex world of Spaceland, A Square posits further about 4 dimensions and so forth to A Sphere who dismisses it as nonsense. Meanwhile, A Square’s intrusion into Spaceland has become a national emergency, which prompts the Spaceland Senate to call to him to appear for a hearing, to explain this breach of protocol of bringing a Flatlander into their midst. They claim it will be viewed as an act of, and provocation for, war by their enemies, the X-Axis.

During the hearing, A Square also learns that the X-Axis considers the Great Senate as weak because they have allowed the continued existence of his own world, Flatland, which they view as an abomination. As the debate rages, an ailing A Square tries to explain his theory of multiple dimensions to an unsympathetic crowd. Air-raid sirens wail as A Square collapses from the overwhelming effects of gravity on his two-dimensional body and chaos ensues.

A Sphere manages to send his dying apostle back on his way to Flatland via a mailing tube before bombs destroy both Messiah, Inc. and A Sphere himself, but A Square’s journey back is halted by A Spheres hovercraft, who attempts to crush A Square to avenge A Spheres death. But, an X-Axis ship crashes into and destroys the hovercraft, and A Square ends up plunging in freefall toward Flatland and through its surface into unknown regions below, where he experiences a revelation on dimensionality and infinity.

A Square finds himself in his own bed on the eve of the year 3000 and his family informs him that the government has issued orders for the arrest of anyone proclaiming the gospel of three dimensions. Undeterred, A Square hurries to see his brother in Flatland jail to discuss their new shared knowledge of the third dimension. B Square, afraid of execution, denies the experience and in a panic assaults his brother who falls temporarily into an unconscious state where he encounters A HyperSphere along with the Monarch of Pointland who curiously resembles a “glow point.” As the Monarch drones on in his monologue of “being the all in all, the one in the one,” A HyperSphere informs his former apostle that time is short and A Square must proclaim the gospel of the three dimensions to his fellow Flatlanders although they, like the Monarch, will probably remain trapped within their own perspectives.

Returning to himself, A Square escapes Flatland prison and outruns the guards. Arriving home, A Square informs his wife that they are going to defect to the Northern Kingdom where he might be able to spread the gospel of three dimensions to a more open minded populace. The soldiers arrive and A Square escapes with the help of Frau A Square’s “war cry” that temporarily stuns them. Before he can reach the border, A Square is cornered by the soldiers whose attempt to dismantle and segment him is thwarted by the Northern Kingdom army’s attack. In the fracas, things suddenly begin to disappear as though sucked down through the fabric of Flatland until only A Square remains. He too begins to disappear until there is only his eye, then a point of light, a glowing point of light, which welcomes him into another dimension.

==Cast==
{| class="wikitable"
!Actor
!Character
!Dimensional shape
!Role
|- Simon G. Hammond A Sphere sphere
|The CEO of Messiah, Inc. and a resident of Spaceland.
|- Greg Trent President Circle circle
|The president of the Southern Republic of Flatland.
|- Chris Carter Chris Carter The King of Lineland line (geometry)|line The ruler of Lineland and the longest line in Lineland (which is only "Six Inches of Space").
|- Linda Meigs Mathilde & Millicent pyramid (geometry)|pyramids A Spheres secretaries. 
|- Jon Shoemaker Soldier Y triangle
|A lethal triangle soldier in the Southern Republic Army.
|- Ashley Blackwell A Line line
|An accused Chromatism line.
|- Michael Karle The Pentagon Doctor pentagon
|The chief doctor at the Hospital of Reconfiguration.
|- Jeff Sanders|Dr. Jeff Sanders Cube Carlton cube
|One of A Spheres many workers.
|- Oscar Gutierrez Oscar Gutierrez The Crazy Old Trapezoid trapezoid
|An elderly, demented trapezoid.
|- Ladd Ehlinger Jr. A Square square
|The main character; a defense attorney and the chosen apostle to preach the Gospel of the Third Dimension.
|- Karen Ehlinger Frau A Square line
|A Square’s wife.
|- Megan Ehlinger A Hexagon hexagon
|A Square’s youngest son.
|- Robert Ehlinger B Square square
|President Circle’s personal assistant and A Squares brother.
|- Catherine Ehlinger Frau B Square line
|B Square’s wife.
|-
|}

===Other voices===
{{columns-list|3|
*Denise Carter
*Juliana Carter
*Mark Carter
*Jacqueline Clift
*Colin Duckworth
*Corin Ehlinger
*Perrin Ehlinger
*Raven Hood
*Chris Kolb
*Lauren Meigs
*Matthew Meigs
*Lacon Mitchell
*Jorge J. Raub
*Sean C. Spurlock
*Bill Wells
}}

==Production==
Ehlinger recorded dialogue for the film throughout 2005 in his home studio.  Using off-the-shelf 3D animation software packages such as Lightwave 3D, Adobe After Effects and others, Ehlinger animated and edited the film  by himself over the course of two years,  starting in 2005.  . flatlandthefilm.com. Ladd Ehlinger, Jr.  He sent completed animations to composer Mark Slater for scoring, who sent back audio files. 

==Reception==
Most reviews were positive with specific criticisms, with one quite negative exception. At SciFi.com, Paul Di Filippo gave the film a rating of "A", stating that it "entertains, enlightens and educates," and that "Ehlinger manages to retain the Victorian satire on pomposity and cultural blindness while updating it to modern conditions. And ...   conceptual breakthrough is brilliantly handled."  Di Filippos single criticism was that the superimposed text exposition got "a bit heavy-handed."   
Film Threat reviewer Phil Hall referred to the films "bold originality and vibrant intelligence", stating, "Sequences throb with raw power, provoking visceral emotional responses". Hall flatly called it "a work of genius," and gave it five stars (of five).     
Dennis Schwartz (Ozus World) gave Flatland an "A", referring to it as "smart, without being cheeky", in taking the likely unfilmable source material, and creating a "spirited avant-garde" film.    He summarized:  

Scott Green at Aint it Cool News called it "captivating", "an enjoyable mental amusement park ride", and "something amazingly different and intriguing to watch."  But he noted, "the complexity of the world being explored does not coherently coalesce", and that the film attacks divisive topics "with an undisciplined flurry of jabs." Green was intrigued by the films "glib omniscient" title cards, writing that their presence "almost makes for a character in and of itself."  

Aylish Wood, reviewing in Science Fiction Film and Television, described the intertitles as fine for children, but "annoying" for adults, and found the math exposition to be "painless" but "a touch too long." She found that too many plot threads were not tied up, and an "overabundance of possible meanings", for example a "slew of referencing" around the character of Senator Chromatistes, which "disperses our understanding". Wood noted flaws in pacing and the intertitles, but found the expression of emotion via color and music to be "effective". Ultimately the film was "a mixed feast" with frustrating "cluttered logic."  

Carl Schroeder wrote in The Global Intelligencer that the film is one of "two of the best movie versions ever made" of the story.  He states that the film "preserves the biting social satire of the original story with ideas and abstract violence (bleeding polygons) not appropriate for little kids (teens will be fine)."  He continues, the "film touches on current events, including allusions to the Iraq war and anti-gay prejudice, to conclude apocalyptically (the book just ends with the protagonist in prison). Most adults will want to see both Flatland versions, sooner or later."   
 Dan Schneider of Blogcritics gave an overall negative review. He criticized the departures by the film from Abbotts book, such as the character of the king instead being a president (who wears a crown), and the divergence into satire when A Sphere visits A Square, where the sphere is a CEO, instead of Abbotts "mystical guide".  Schneider points out that where A Squares experience was originally religious, the film makes it a "wow moment used to lead into some cheap gags", and states that "the story dissolves." He found the films satire "predictable", and described it as "best when sticking to the books original points."  Schneider faults writer Tom Whalens script for changing Abbotts story "too much", the music by Mark Slater as "sometimes apt", but at other times "a mess", the intertitle cards as "annoying", the ending for being "muddled" and trying "too hard for the relevance of 2001: A Space Odyssey",  and the DVD itself for lacking a commentary track.  

In Mathematics in Popular Culture, Lila Marz Harper described the film as "more radical" than Flatland: The Movie, showing more biological detail, and even dreams. She remarked that the film "vividly mimics" Abbotts description of the physical remolding of childrens bodies to conform to societal norms, and "adheres closely" to the discrimination against women in Abbotts story. She noted that some variations from the book were confusing: one of A Squares sons is a hexagon, unlike the all-pentagon siblings in Abbotts story.   

==References==
 

==External links==
*  
*  
*   at Rotten Tomatoes

===Articles===
*  
*  

===Reviews===
*  
*  
* , a review of both   and Flatland the Film by Ryan Somma
*"Two reviews in one" of   and Flatland the Film by "Bob Stout" at Amazon.com: filed under Johnson’s & Davis’s  , and under Ehlinger’s  

 

 
 
 
 
 
 
 
 