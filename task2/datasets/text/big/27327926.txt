Tender Son: The Frankenstein Project
{{Infobox film
| name           = Tender Son: The Frankenstein Project
| image          = Tender Son.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Kornél Mundruczó
| producer       = Viktória Petrányi
| writer         = Kornél Mundruczó
| starring       = Rudolf Frecska
| music          = Philipp E. Kümpel Andreas Moisa
| cinematography = Mátyás Erdély
| editing        = David Jancsó
| studio         = Proton Cinema
| distributor    =
| released       =  
| runtime        = 105 minutes
| country        = Hungary
| language       = Hungarian
| budget         = € 1.6 million
| gross          =
}}
Tender Son: The Frankenstein Project ( ) is a 2010 Hungarian film written and directed by Kornél Mundruczó,  developed from his own theatrical play and loosely based on Mary Shelleys Frankenstein. The film was screened in the main competition at the 2010 Cannes Film Festival,  where it was poorly received by critics.

== Cast ==
* Rudolf Frecska as boy
* Kitty Csíkos as girl
* Kornél Mundruczó as director
* Lili Monori as mother
* Miklós Székely B. as father

== Production == HUF (€540,000) in support from the Motion Picture Public Foundation of Hungary and 145,000 Euro from the Mitteldeutsche Medienförderung in Germany. The total budget was €1.6 million.  

== Reception ==
 , Boyd van Hoeij was disappointed with how the filmmakers had bypassed the original novels mythological allusions: "Mundruczo and regular co-scripter Yvette Biro (Delta, Johanna (film)|Johanna) have completely neutered Shelleys clever notion of a hero incompatible with his surroundings by replacing the monster with a flesh-and-blood human with no backstory, turning him into a supposed equal rather than a misunderstood outcast. Without a clear understanding of his psychology or past (How was he treated in the orphanage? How does he feel about his parents absence for most of his life?), his random killing spree seems simply incomprehensible and vile." 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 