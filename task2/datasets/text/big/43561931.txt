Diary ng Panget (film)
{{Infobox film
| image          = DiaryngPanget.jpg
| caption        = Theatrical release poster
| name           = Diary ng Panget 
| director       = Andoy Ranay Denny R.   
| producer       =  
| music          = Teresa Barrozo
| cinematography = Pao Orendain
| story          = 
| screenplay     = Mel Mendoza-Del Rosario Denny R.     (novel)
| editing        = Tara Illenberger
| starring       =  
| studio         = 
| distributor    = Viva Films
| released       =  
| country        = Philippines
| language       =  
| runtime        = 110 minutes 
| budget         = 
| gross          = PHP 120,932,910      
 
}} romantic comedy novel of Denny R. James Reid, Yassi Pressman, and Andre Paras. It was distributed by Viva Films and released on April 2, 2014, in theaters nationwide.   

== Cast ==
=== Main Cast ===
*Nadine Lustre  as Reah "Eya" Rodriguez James Reid as Cross Sandford
*Yassi Pressman as Lorraine "Lory" Keet
*Andre Paras as Chad Jimenez

=== Supporting Cast ===
*Gabby Concepcion as Mr. Sandford
*Mitch Valdez as Auntie
*Candy Pangilinan as Mayordoma
*Aj Muhlach as Ian
*Arkin Del Rosario as Seven
*Coraleen Waddell as Riri
*Janna Roque as Femme
*Carissa Quintas as Steph

== Production ==
=== Music ===
Due to the success of the movie, the soundtrack for the film was released March 26, 2014 by Viva Records with a co-production of Flipmusic records.  The physical copy of soundtrack is available in leading music stores and digitally via iTunes. 

{{Infobox album  
| Name       = Diary ng Panget (Original Movie Soundtrack) 
| Type       = Soundtrack
| Artist     = Various artists
| Cover      = DiaryNgPangetOST.jpg
| Alt        =
| Released   = March 26, 2014
| Recorded   = 2014 Pop
| Length     = 34:59 Viva Records
| Producer   = 
| Last album =
| This album =
| Next album =
}}{{Track listing
| title1          = No Erase 
| length1         = 3:39 James Reid and Nadine Lustre Rocketeer
| length2         = 4:09 
| extra2          = Reid, Lustre
| title3          = Paligoy-ligoy
| length3         = 3:18
| extra3          = Lustre
| title4          = Natataranta
| length4         = 3:09
| extra4          = Reid
| title5          = Di Ko Alam
| length5         = 3:36
| extra5          = Yassi Pressman and Andre Paras
| title6          = Dyosa
| length6         = 3:37
| extra6          = Yumi Lacsamana
| title7          = Kakaibabe
| length7         = 3:31
| extra7          = Donnalyn Bartolome
| title8          = Labing Isang Numero
| length8         = 3:09
| extra8          = Thyro Alfaro
| title9          = Halika Na
| length9         = 3:54
| extra9          = Shehyee feat. Ann B. Mateo
| extra_column    = Artist
| title10         = Dinggin
| length10        = 4:17
| extra10         = Sugar High total_length     = 34:59
}}

== Reception ==
=== Critical response ===
Diary ng Panget received mix to negative reviews from critics.

Zig Marasigan of Rappler states that Diary ng Panget provides very little insight on the modern day troubles of a young adult. "It succeeds in bringing together a particularly charming cast with relative newcomer Nadine Lustre leading the charge. Director Andoy Ranay does well to draw an entertaining amount of chemistry between his 4 actors, but unfortunately fails to keep the story, as well as its characters, from breaking out."  

Philbert Ortiz-Dy of Click the City gave a negative review in the film, stating that " he simplicity of the story is further undermined by the confusing direction. The film seems to struggle with staging a lot of scenes, the punchlines lost in the odd rhythms of the filmmaking." He also stated that " he direction really fails the cast of young actors, who are acting their hearts out   Diary ng Panget is squandered by the subpar direction. The looseness of the narrative might have even been an asset if the direction was more inclined to make bolder or more well thought-out choices about how it presented the story." 

=== Box office === top ten highest grossing Filipino films of 2014.

== See also ==
*List of Filipino films and TV series based on Wattpad stories

== References ==
 

== External links ==
* 
*  at Box Office Mojo
* 

 
 
 
 
 
 
 
 
 
 