Murderball (film)
{{Infobox film
| name            = Murderball
| image           = Murderball.jpg
| image_size      = 215px
| alt             = 
| caption         = Theatrical release poster
| director        = Henry Alex Rubin Dana Adam Shapiro
| producer        = Jeffrey V. Mandel Dana Adam Shapiro Mark Zupan Bob Lujano Joe Soares Brent Poppen
| music           = Jamie Saft
| cinematography  = Henry Alex Rubin
| editing         = Conor ONeill Geoffrey Richman Participant Productions A&E (TV channel)|A&E IndieFilms
| distributor     = ThinkFilm
| released        =  
| runtime         = 88 minutes
| country         = United States
| language        = English
| budget          = 
| gross           = $1,750,211
}} Canadian and 2004 Paralympic Best Documentary MTV film released through ThinkFilm as well as Participant Media.

==UN screening== Mark Zupan, winced when describing how embarrassed he was to have his mother hear his remarks on the sex lives of persons with quadriplegia.

==Production== Sony PD150 was used to shoot some of the early interviews. The crew rigged a Sennheiser shotgun microphone to use as a boom, and relied heavily on Lavaliere wireless microphones as well. Available lighting was used almost exclusively. Additional light was provided using an inexpensive china ball. In one example of on-the-spot lighting, a flashlight was diffused using only a napkin. 

==Reception==
Murderball garnered almost universally positive reviews; it currently holds a 98% fresh rating on Rotten Tomatoes, with the consensus "An entertaining and gripping documentary that shows being confined to a wheelchair doesnt mean the fun has to end."  This film is also #1 on the Rotten Tomatoes countdown of the top sports movies.  Murderball also received positive reviews from Hollywood.com  and Roger Ebert, who said "This is one of those rare docs, like Hoop Dreams, where life provides a better ending than the filmmakers could have hoped for." 

==Awards==
{| class="wikitable"
|-
!Award !! Category !! Recipient !! Result
|-
| Sundance Film Festival Audience Award
| Best Documentary Feature
| 
|  
|-
| 78th Academy Awards Best Documentary Feature
| 
|  
|-
| Full Frame Documentary Film Festival Audience Award
| Best Feature
| 
|  
|- Indianapolis International Film Festival Audience Award
| Best Feature Film
| 
|  
|-
| Best Non-Fiction Film
| 
|  
|}

==References==
 

==External links==
*  
*  
*  
*  
*   from 2005, at MTV.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 