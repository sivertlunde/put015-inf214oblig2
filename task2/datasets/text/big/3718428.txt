Left Behind: The Movie
 
 
{{Infobox film
| name           = Left Behind: The Movie
| image          = Left Behind DVD cover.jpg
| caption        = DVD cover art
| director       = Vic Sarin Ralph Winter Reverend Tim LaHaye Jerry B. Jenkins Alan B. McElroy Paul Lalonde Joe Goodman
| narrator       = Brad Johnson Clarence Gilyard, Colin Fox
| music          = James Covell
| cinematography = George Tirl
| editing        = Michael Pacek
| studio         = Namesake Entertainment
| distributor    = Cloud Ten Pictures
| released       =  
| runtime        = 96 minutes
| country        = Canada United States
| language       = English
| budget         = $4 million
| gross          = $4,224,065
}}

Left Behind is a Christian-based film released in 2000 and starring   and  .

==Plot==
GNN television journalist Cameron "Buck" Williams reports from Israel about a new technology with which food will grow almost anywhere. He interviews Israeli scientist Chaim Rosenzweig, and praises him for creating a miracle. Suddenly, Arab Mikoyan MiG-29 and Russian fighter jets fly overhead in a surprise air raid. A missile hits near Buck and Chaim as they retreat to a military bunker. The sun disappears even though it is still mid-day. Israel Defense Forces|Israels defenses are unable to counterattack, but the attacking jets start spontaneously exploding and crashing down. Buck runs outside with the news camera and records the drama as some GNN executives and reporters watch back in Chicago. The entire attacking force is destroyed.

The story shifts to pilot Rayford Steele, who has been asked to fly from New York to London at short notice, causing him to miss his son Raymies birthday party. Despite his wifes and his daughters protests, he agrees and leaves his family behind. Rayfords daughter, Chloe Steele, is leaving for her college exams. Buck, having decided to go to London for an investigation of the attack, boards Rayfords plane.

On the flight, a flight attendant, Hattie Durham, who is having an affair with Rayford, reveals shes taking a job at the UN and this is her last flight. Later during the flight, some passengers awaken to realize that several of their fellow passengers are missing. Panic sets in, and Buck helps Hattie try to keep the passengers calm. Upon returning to the cockpit, they discover that people (later revealed to be Christians) are mysteriously disappearing worldwide and some planes are down from missing flight crews. He is forced to turn the plane back and land in Chicago. Shortly after landing, Buck locates Rayford and asks him to fly him to New York City. Rayford refuses, saying that he has to be with his family, but says he will find Buck a private pilot, and they both drive to Rayfords home.

Meanwhile, Chloe is driving home from her college exams when she encounters a large traffic accident. She goes to check on a crashed semi, whose driver vanished. People are reporting abandoned cars and children missing from their seats. While Chloe is inspecting the carnage, her car is stolen by a hurt man and she is stranded on the wrecked highway. She eventually starts walking down the highway. Rayford discovers that his wife and son are missing. He and Buck are forced to stay in the house because of a military-enforced curfew. Rayford starts to read his wifes Bible.
 Bruce Barnes. Bruce has also been left behind because he never truly believed in God. A believer at last, he begs for forgiveness and asks God for a second chance to help people. Rayford enters the church and kneels next to Bruce, telling him that God already has used him. They then watch a videotape left by another Reverend Billings dealing with the Rapture, in which all true believers are taken to Heaven, while the rest are left behind to endure the Tribulation&mdash;seven years of war and suffering.

When Buck gets to New York City, he finds that his friend Dirk Burton has been killed. While he is there, he takes a computer disc and is almost shot by a sniper. Buck decodes the computer disc and finds out that someone is trying to bankrupt the UN in order to control the worlds food supply. Rayford confronts Hattie, telling her that their "affair" was wrong, and that he wants her forgiveness, and she leaves in a huff. Rayford tells Chloe about God and she says she believes. Meanwhile, Buck flies back to Chicago to meet with an old friend, CIA agent Alan Thompkins. After the meeting, Alan is killed in a car bombing, which Buck narrowly escapes. He goes to Rayfords house, because they are the only ones Buck knows in Chicago. Taking the wounded Buck to New Hope Church (as a makeshift hospital), Rayford and Bruce show Buck the tape that Reverend Billings made. Buck, however, does not fully believe the claims, and he goes to warn Chaim about the plot against the UN. Rayford and Chloe attempt to stop him, because he doesnt have God on his side. Buck ignores the Steeles advice and goes to the UN anyway.

At the UN, Buck sees the plans for the new Israeli temple, and realizes that everything Rayford, Chloe and Bruce told him was true. Before the meeting, Buck finally accepts God and asks Him to show him The Way. God shows him that UN Secretary-General Nicolae Carpathia is the Antichrist when he reveals his plan for world domination, of which his plan to rebuild the temple of Israel is a logical first step. Carpathia shoots Jonathan Stonagal and Joshua Todd-Cothran, who were behind the plot to bankrupt the UN, and then brainwashes the new "kings and queens" (the 10 UN delegates) into thinking that Stonagal shot Cothran and himself. Everyone, even the press, believes Carpathia, except Buck, who leaves and returns to the church, where he resolves to fight Carpathia with the help of his friends. Narrating, Buck says the "seven years of peace" declared by Nicolae will be the seven worst years mankind has ever seen, and that faith is all they need.

==Cast==
  Buck Williams Brad Johnson Captain Rayford Steele
* Gordon Currie as Nicolae Carpathia
* Chelsea Noble as Hattie Durham Bruce Barnes
* Janaya Stephens as Chloe Steele Colin Fox as Chaim Rosenzweig
* Daniel Pilon	as Jonathan Stonagal
* Tony De Santis as Joshua Todd-Cothran
* Jack Langedijk as Dirk Burton
* Krista Bridges as Ivy Gold
* Thomas Hauff	as Steve Plank
* Neil Crone as Ken Ritz
* T.D. Jakes as Pastor Vernon Billings (appearing on a video to be played post-Rapture)
* Rebecca St. James as Bucks Assistant GNN Reporter
* John Hagee as an unnamed passenger on Captain Steeles flight, where many passengers (including Hagee) disappear after the Rapture
 

==Production==
Cloud Ten Pictures licensed the rights to the first two Left Behind novels from Namesake Entertainment, which had licensed them from the books authors. Filming commenced in early May 2000 and continued for a total of 31 days.

An Ontario quarry was used for the scenes of Israel.  Bowmanville Zoos Mike Hackenberger commented, "Camels sell the look.... As a prop, camels are great. You can move em around, you can stick em there, and you see a camel on sand, you know its desert. . . They might not fit through the eye of the needle, but without them, this movie would have been a disaster. There should be at least one camel in every movie." 

Before Janaya Stephens took the role of Chloe Steele, it had been given to Lacey Chabert, who dropped out due to scheduling conflicts.

Some of the extras who played the saved were noted Christian ministers, most notably Jack Van Impe and John Hagee (who are featured on the airplane shortly before the mass disappearance; Hagee was also very instrumental in the movies promotion) and T.D. Jakes (who appears in the video that the group watches, telling them what to expect in the post-Rapture world).  Notable Christian musicians were also used as extras; Bob Carlisle and Rebecca St. James appeared as news anchors, and the Christian group Jake appeared as police guards towards the end of the movie.

==Soundtrack releases==

Two CDs of music from the film have been released.

On October 3, 2000, Reunion Records released Left Behind: The Movie Soundtrack, featuring a collection of songs from and inspired by the movie. The CD includes these fifteen tracks:

# Never Been Unloved (Bruce’s Song) – Michael W. Smith Joy Williams
# Sky Falls Down (Israel Is Attacked) – Third Day Plus One Avalon
# Can´t Wait For You To Return – Fred Hammond
# Midnight Cry (Closing Theme) – Various Artists LaRue
# Jake
# Come Quickly Lord – Rebecca St. James
# After All(Rayford’s Song) – Bob Carlisle
# Live For The Lord (Irene’s Song) – Kathy Troccoli
# All The Way To Heaven – V*Enna
# No Fear (Panic In The City) – Clay Crosse SHINE

On February 6, 2001, Reunion released the second CD, titled Left Behind: The Original Motion Picture Score. The CD featured the orchestral score composed by James Covell, and performed by the London Symphony Orchestra and the Lake Avenue Choir. The CD includes these seventeen tracks:

 
# Prologue
# Left Behind Main Title
# Surprise Attack
# Rayfords Conversion
# Dirks in Trouble
# Rebuild the Temple
# Rapture
# Rayford Comes Home
# Loss of a Friend
# Bucks Mission
# Chloes Choice
# One Left, the Other Taken
# Goodbyes
# I Dont Want to Lose You
# Prayers for Buck
# Seven Years
# The Final Chapter
 

==Reception and release==
The film received all around critical backlash and negative reviews. It received a 16% positive rating among reviewers on the Rotten Tomatoes website.   The Washington Posts Desson Howe described it as "a blundering cringefest, thanks to unintentionally laughable dialogue, hackneyed writing and uninspired direction. The more this movie tries, the worse it gets. Its sincerity ends up becoming a bulging bulls-eye for rotten-tomato throwers." 

The film was released  on DVD first. According to the filmmakers, this was to build interest in the film. The first DVD, released on October 31, 2000, featured coupons for the upcoming theatrical release, allowing those going to see it to get in for the price of a matinee ticket. The pre-theatrical release DVD sold fairly well, despite negative reviews.

The film was released theatrically on February 2, 2001. Minor changes from the DVD version were made. The visual effects of the attack on Israel at the beginning of the film were updated, looking more realistic. Over the end credits, the music video for the song "Midnight Cry" was played, replacing the theme song by Bryan Duncan and Shine.

The film opened 17th in the nation over the February 2–4 weekend, making $2,158,780. The film went on to gross a total of $4,224,065, barely surpassing its budget. 

==Legal dispute==
Owing to dissatisfaction with the quality of this movie and its sequels, LaHaye filed suit against Namesake Entertainment and Cloud Ten Pictures in July 1999, claiming breach of contract.   On July 3, 2008, Tim LaHaye and Cloud Ten settled legal disputes on the film adaptations of the book series.  Part of the agreement grants LaHaye an opportunity to remake the series.  He asserts: 
 
My dream has always been to enter the movie theater with a first-class, high-quality movie that is grippingly interesting, but also is true to the biblical storyline -- and that was diluted in the first attempt. But Lord willing, we are going to see this thing made into the movie that it should be, and that all the world sees it before the real Rapture comes.  
 rebooted with 2014 film, which has also been poorly received.

==References==

===Notes===
*All quotes from people affiliated with Left Behind: The Movie are from the "Making of Left Behind" featurette.
 

===Bibliography===
*Forbes, Bruce David and Jeanne Halgren Kilde (eds.), Rapture, Revelation, and the End Times: Exploring the Left Behind Series. New York: Palgrave Macmillan, 2004. ISBN 1-4039-6525-0
*Frykholm, Amy David. Rapture Culture: Left Behind in Evangelical America. Oxford University Press, 2004.  ISBN 0-19-515983-7
*Reed, David A., LEFT BEHIND Answered Verse by Verse. Morrisville, NC: Lulu.com, 2008. ISBN 1-4357-0873-3
*Rossing, Barbara R., The Rapture Exposed: The Message of Hope in the Book of Revelation, New York: Basic Books, 2004. ISBN 0-8133-4314-3
*Shuck, Glenn W.. Marks Of The Beast: The Left Behind Novels And The Struggle For Evangelical Identity. New York University Press, 2004. ISBN 0-8147-4005-7
*Gribben, Crawford, Rapture Fiction and the Evangelical Crisis. Evangelical Press, 2006. ISBN 0-85234-610-7.
*Snow Flesher, LeAnn, "Left Behind? The Facts Behind the Fiction". Valley Forge, Judson Press, 2006. ISBN 0-8170-1490-X

==External links==
* 
* 

 

 
 
 
 
 
 
 
 