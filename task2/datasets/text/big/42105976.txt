Gaijin: Roads to Freedom
{{Infobox film
| name           = Gaijin: Roads to Freedom
| image          = Gaijin Roads to Freedom Poster.jpg
| caption        =
| director       = Tizuka Yamasaki
| producer       = 
| writer         = Tizuka Yamasaki  Jorge Durán
| starring       = Kyoko Tsukamoto Antônio Fagundes Jiro Kawarazaki Gianfrancesco Guarnieri Alvaro Freire
| music          = John Neschling
| cinematography = Edgar Moura
| editing        = Vera Freire   Lael Rodrigues
| studio         = Ponto Filmes
| distributor    = Embrafilme
| released       =  
| runtime        = 112 minutes
| country        = Brazil
| language       = Portuguese Japanese
| budget         = 
| gross          =
}}
 debut film of director Tizuka Yamasaki.  

The film is based on real events in the history of  , was released on September 2, 2005.

==Plot==
Japan, 1908. Motivated by poverty in the country and few job prospects, many Japanese have emigrated in search of opportunities. As the emigration company only accepted family groups who had at least a couple, Yamada (Jiro Kawarazaki) and Kobayashi (Keniti Kaneko) who were brothers, see as solution that Yamada would marry Titoe (Kyoko Tsukamoto), who was only 16 years old. Yamada and Titoe had just met and, along with a cousin, they depart to Brazil. After 52 days of travel they finally arrive in Brazil where they will work in Santa Rosa farm, in São Paulo, where the coffee expansion was intense. But they stumble upon a foreman who handles hostile settlers, demanding to work to exhaustion. In addition they are stolen by the owners of the farm, only being treated with respect by other settlers and by Tonho (Antônio Fagundes), the counter of the farm. 

==Cast==
*Kyoko Tsukamoto as Titoe
*Antônio Fagundes as Tonho
*Jiro Kawarazaki as Yamada
*Keniti Kaneko as Kobayashi
*Gianfrancesco Guarnieri as Enrico
*Álvaro Freire as Chico Santos
*Louise Cardoso as Angelina
*José Dumont as Ceará
*Yuriko Oguri as Mrs. Nakano
*Clarisse Abujamra as Felícia
*Carlos Augusto Strazzer as Dr. Heitor
*Dorothy Leirner as Grazziela
*Maiku Kozonoi as Keniti Nakano

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 