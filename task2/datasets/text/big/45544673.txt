The Man Who Wouldn't Die (1942 film)
{{Infobox film
| name           = The Man Who Wouldnt Die
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Herbert I. Leeds
| producer       = Sol M. Wurtzel
| writer         = Brett Halliday (character)
| screenplay     = Arnaud dUsseau
| based on       =   
| narrator       = 
| starring       = Lloyd Nolan Marjorie Weaver Helene Reynolds
| music          = David Raksin
| cinematography = Joseph MacDonald Fred Allen
| studio         = 
| distributor    = Twentieth Century Fox Film Corporation
| released       =  
| runtime        = 65 min.
| country        =   English
| budget         = 
| gross          = 
}}
 1942 Mystery Mystery directed by Herbert I. Leeds, starring Lloyd Nolan and Marjorie Weaver. This movie is the 5th of a series of seven of the Michael Shayne movies produced by Twentieth Century Fox between 1940-1942.

==Plot summary==
 

==Cast==
 
* Lloyd Nolan as Michael Shayne
* Marjorie Weaver as Catherine Wolff
* Helene Reynolds as Anna Wolff
* Henry Wilcoxon as Dr. Haggard
* Richard Derr as Roger Blake
* Paul Harvey as Dudley Wolff
* Billy Bevan as Phillips - the Butler
* Olin Howland as Chief of Police Jonathan Meek (as Olin Howlin)
* Robert Emmett Keane as Alfred Dunning
* LeRoy Mason as Zorah Bey
* Jeff Corey as Coroner Tim Larsen Francis Ford as Caretaker
 

==References==
 	

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 