Nijangal Nilaikkindrana
{{Infobox film
| name = Nijangal Nilaikkindrana
| image = Nijangalnilaikkindrana.jpg
| caption = LP Vinyl Records Cover
| director = Govindan
| writer =
| starring = 
| producer = Vijay for Vijayaraji Pictures
| music = Shankar Ganesh
| editor =
| released = 
| runtime = Tamil
| budget =
}}
 Indian feature film, starring newcomers in lead roles.  

==Soundtrack==
{{Infobox album Name     = Nijangal Nilaikkindrana Type     = film Cover    = Released =  Music    = Shankar Ganesh Genre  Feature film soundtrack Length   = 20:24 Label    = Polydor
}}

The music composed by Shankar Ganesh.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics 
|-
| 1 || Paarvaiyo Unnidam || N.V.Haridas & Vani Jayaram|| Muthulingam
|-
| 2 || Achangal Vetkangal || Malaysia Vasudevan & Chorus || Muthulingam
|-
| 3 || Muthangale Aayirame || K. J. Yesudas || Muthulingam
|-
| 4 || Thulluthu Thulluthu|| K.P.Brahmanandan S. P. Sailaja || Muthulingam
|}

==Cast==

*Newcomers

==References==
 

== External links ==
*  

 
 
 
 

 