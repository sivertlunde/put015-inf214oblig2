Infection (2004 film)
{{Infobox film
| name           = Infection
| image          = Infectionposter.jpg
| caption        = Japanese film poster
| director       = Masayuki Ochiai
| producer       = Takashige Ichise
| story          = Ryoichi Kimizuka
| screenplay     = Masayuki Ochiai
| starring       = Kōichi Satō Masanobu Takashima
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
  is a 2004 Japanese horror film directed by Masayuki Ochiai, of a run-down hospital where a doctors mistake unwittingly creates dark consequences for all. It was released as part of the six-volume J-Horror Theater series.

==Plot==
 
At a hospital, Dr. Akiba refuses to admit a patient with a strange black rash and is alerted to a serious crisis in Room 3 where a burn victim has died, having apparently been given the wrong drug. Akiba, another doctor, and four nurses decide to cover up the cause of death and move the body to an unused room.

Meanwhile, Akiba discovers that the patient he refused to admit has been left in the hallway. Doctor Akai decides to study his symptoms: though he is technically still alive, his body mass is liquefying into green goo. Not knowing how much Akai knows about the events in Room 3, Akiba and the others reluctantly agree to help with the examination but when they return to the patients room, they find he has vanished and the head nurse has been knocked unconscious. 

It becomes obvious that they are all at risk of infection when the head nurse goes mad and begins to bleed green goo from her ears and eyes. The doctors put her on a bed and cover her with plastic in an attempt to limit the infections spread. 

Soon after, the mean nurse finds the young nurse (who had previously visited the room where the head nurse was kept) practising drawing blood on herself. Their conversation goes awry when the young nurse lets out a manic laugh and plunges two needles into her body, splattering the mean nurse with green goo. 

While speaking with a patient he killed earlier, Uozumi is overcome with guilt and starts crying. Akiba walks in but finds Uozumi alone. His eyes turn white and green goo starts seeping from him. Akiba panics and turns to find the mean nurse, now infected and covered in green goo, smiling and hanging upside down from the ceiling. After finding the misuse nurse giving her own blood to the dead burn patient and running into the infected head nurse, Akiba flees the room.

Akiba confronts Akai and accuses him of creating and spreading the virus. Akai denies this and explains that the infection is actually spread mentally, infecting the subconscious of its victims. He then urges Akiba to remember what really happened in Room 3 earlier in the night.

Dr. Nakazono walks in to find Akiba talking to himself, forcing him to realise that hes been speaking with his reflection. He looks around and sees the corpses of the mean nurse and the inexperienced nurse, dead and covered in blood with no green goo in sight, and realizes that the last few hours have been a hallucination. Nakazono calls the police as Akiba recalls the events in Room 3 and realizes that "Akai" was actually the burn patient. The same series of events is shown again but with Akiba as the burn patient and Akai as Akiba, giving the order for the wrong drug, and massacring the entire staff. 

The hospital is evacuated the next morning - all of the staff save Nakazono are dead and the police is searching for Akiba who has disappeared. When Nakazono leaves the hospital, she sees all the red lights change to green and vice versa; panicking, she accidentally cuts her hand and green blood pours out.

The movie ends with a shot of a locker in the room where the burn victim was kept. Someone inside the locker is calling for help as green goo starts pouring out of it. The top of the locker opens and Akibas goo covered hand reaches out before falling to the floor.

== Cast ==
*Kōichi Satō as Dr. Akiba
*Michiko Hada as Dr. Nakazono
*Yōko Maki (actress)|Yōko Maki as the Mean nurse
*Shiro Sano as Dr. Kiyoshi Akai
*Tae Kimura as the Misuse nurse
*Masanobu Takashima as Dr. Uozumi
*Kaho Minami as  Head nurse
*Mari Hoshino as  Rookie nurse
*Moro Morooka as Dr. Kishida
*Isao Yatsu as a hospital patient

== Release ==
The film was released as part of producer Takashige Ichises J-Horror Theater series which includes Premonition (2004 film)|Premonition, Reincarnation (film)|Reincarnation, and Retribution (2006 film)|Retribution. 

==References==
 

== External links ==
 
*  
*  
*  
 
 

 
 
 
 
 
 
 