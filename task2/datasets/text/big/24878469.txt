See You in the Morning (film)
{{Infobox film
| name           = See You in the Morning
| image          = See You in the Morning.jpg
| caption        = Original movie poster
| director       = Alan J. Pakula
| producer       = Alan J. Pakula Susan Solt
| writer         = Alan J. Pakula
| starring       = Jeff Bridges Alice Krige Farrah Fawcett Drew Barrymore Lukas Haas David Dukes Frances Sternhagen George Hearn Theodore Bikel Linda Lavin Heather Lilly Macaulay Culkin Tom Aldredge William Le Massena Kate Wilkinson
| music          = Michael Small
| cinematography = Donald McAlpine
| editing        = Evan A. Lottman
| studio         = Lorimar Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 119 minutes
| country        = United States English
| budget         =
| gross          = $4,795,009 (USA)
}} 1989 romantic comedy film written and directed by Alan J. Pakula, and starring Jeff Bridges, Alice Krige and Farrah Fawcett. It features music by Nat "King" Cole and Cherri Red. The original music score was composed by Michael Small.

==Plot==
While Beth Goodwin (Alice Krige) is a happily married mother of two children, living with a pianist (David Dukes), psychiatrist Larry Livingstone (Jeff Bridges) is living in New York with wife Jo (Farrah Fawcett) and their two young children. Three years later, Beth’s husband Peter suddenly becomes paralyzed in his hands and commits suicide, leaving Beth and the children heartbroken. Larry is abandoned by his wife and two children, who have moved to London since the split.

Larry is introduced to Beth at a party of Martin (George Hearn) and Sidney (Linda Lavin), and an immediate attraction ensues. Larry is distracted, though, by the attendance of Jo, who has shown up with her new flame, actor Jack (Mark La Mura). At the end of the night, he brings home Beth, and is welcomed by her children Cathy (Drew Barrymore) and Petey (Lukas Haas).

Even though Beth’s mother Neenie (Frances Sternhagen) criticizes him for having given up on his marriage with Jo too fast, Larry continues to see Beth. He introduces his children Robin (Heather Lilly) and Billy (Macaulay Culkin) to the Goodwins, but they do not blend well together. Robin blames her father for replacing Jo with Beth, and a planned wedding is postponed due to the continuing friction. Sidney, Beth’s best friend, advises her to give Larry another chance, explaining how love is not always perfect by giving example of how Martin once committed adultery. Simultaneously, Larry dresses up as Cupid and convinces her to marry him.

Beth quickly starts to worry about their future, fearing that Larry will one day abandon her the way Peter did. Larry proposes they move to another apartment, explaining that their current residence will always be Cathy and Petey’s father’s place. Beth’s children are opposed to a move. While Beth is overseas for work, daughter Cathy starts to rebel and is arrested for shoplifting at Bloomingdale’s. Larry continues to feel vulnerable to Jo, whose mother is revealed to be dying, prompting Jo to leave the children with Larry. 

Since the death of Neenie, Larrys focus is on comforting his ex-wife and kids. Jo admits that she still loves him, leaving Larry uncertain what to do. He assures Beth that he did not commit adultery, despite feeling connected to Jo.  Cathy and Petey mistake Larrys and Beth’s night of making love for a violent struggle. Their fear that something bad has happened, making the children realize how much they actually appreciate Larry. The whole family agrees to move to a new house.

==Cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Jeff Bridges || Larry Livingstone
|-
| Alice Krige || Beth Goodwin
|-
| Farrah Fawcett|| Jo Livingstone
|-
| Drew Barrymore || Cathy Goodwin
|-
| Lukas Haas || Petey Goodwin
|-
| David Dukes || Peter Goodwin
|-
| Frances Sternhagen || Neenie
|-
| George Hearn || Martin
|-
| Theodore Bikel || Bronie
|-
| Linda Lavin || Aunt Sidney
|-
| Heather Lilly || Robin Livingstone
|-
| Macaulay Culkin || Billy Livingstone
|-
| Tom Aldredge || Beths Father
|-
| William Le Massena || Larrys Father
|- Dorothy Dean || Larrys Mother
|-
| Alixe Gordin || Beths Mother
|-
| Kate Wilkinson || Aunt Matilda
|- Christopher Curry || Larrys Brother
|-
| Betsy Aidem || Larrys Sister-in-Law
|-
| Robert Levine || Judge
|-
|}

==Critical reception==
Vincent Canby of the New York Times criticized the films script, but praised the cast:
 
Roger Ebert of the Chicago Sun-Times gave it two and a half stars out of four and also felt that the script was not great but that the actors did well:
 

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 