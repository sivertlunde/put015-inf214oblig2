Touch and Go (1986 film)
{{Infobox film
| name           = Touch and Go
| image_size     =
| image	=	Touch and Go FilmPoster.jpeg
| caption        =
| director       = Robert Mandel
| producer       = Stephen J. Friedman
| writer         = Alan Ormsby Bob Sand Harry Colomby
| narrator       = Slash (not credited)
| music          = Georges Delerue
| cinematography = Richard H. Kline
| editing        = Walt Mulconery
| distributor    = TriStar Pictures
| released       =  
| runtime        = 101 min
| country        = United States
| language       = English
| budget         =
| gross          = $1,254,040
| website        =
}}

Touch And Go is a 1986 comedic drama film directed by Robert Mandel, starring Michael Keaton, María Conchita Alonso and Ajay Naidu.

==Plot==
Bobby Barbato (Keaton) is a pro athlete in ice hockey. As always, he expects cheering crowds and beautiful women coming after him. But one day, a gang of youths begin to mug him but he manages to fend them off and then catches the youngest member of the gang, Louis DeLeon (Naidu). He then gives Louis a ride home and meets his mother, Denise (Alonso). After a fight, Bobby moves on with his life but then he and Denise begin to fall for each other and Louis eventually becomes close friends with the hockey player.

==Cast==
* Michael Keaton as Bobby Barbato
* María Conchita Alonso as Denise
* Ajay Naidu as Louis DeLeon
* Cynthia Cypert as 2nd Girl in Bar

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 