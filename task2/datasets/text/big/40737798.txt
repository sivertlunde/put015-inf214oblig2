Steel and Lace
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Steel and Lace
| image          =
| caption        =
| director       = Ernest D. Farino
| producer       = David DeCoteau John Schouweiler Charles W. Fries (executive producer)
| writer         = Joseph Dougherty Dave Edison
| starring       = Clare Wren Bruce Davison Stacy Haiduk David Naughton Michael Cerveris
| music          = John Massari
| cinematography = Thomas M. Callaway
| editing        = Christopher Roth
| distributor    = Fries Entertainment
| released       =  
| runtime        = 90 minutes
| country        = United States English
| budget         =
| gross          =
}}
Steel and Lace is a 1991 science fiction action film directed by Ernest D. Farino.

== Plot == robot scientist to represent his sister at the criminal trial. However Daniel has gotten several of his friends to provide an alibi for the night of the rape, and he is found not-guilty.
Gaily is horrified by the verdict and leaps from a tall building, despite her brothers pleas.

Albert uses his knowledge of robotics to resurrect his sister as a cyborg, which he then sends after Daniel and his business partners; the five men who provided false witness in court.

== Cast ==
* Clare Wren as Gaily Morton
* Bruce Davison as Albert Morton
* Stacy Haiduk as Alison
* David Naughton as Detective Dunn
* Michael Cerveris as Daniel Emerson
* Scott Burkholder as Tobby
* Paul Lieber as Oscar
* Brian Backer as Norman
* John J. York as Craig
* Nick Tate as Duncan David L. Lander as Schumann
* John DeMita as Agent Spoon Cindy Brooks as Girl in Ford Thunderbird|T-Bird William Prince as Old Man

==Reception==
Review aggregator site Rotten Tomatoes reports that 18% of critics gave the film a positive review based on 2 reviews.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 


 
 