Tamagotchi: Happiest Story in the Universe!
{{Infobox film
| name           = Tamagotchi: Happiest Story in the Universe!
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Jōji Shimura
| producer       =
| writer         = Aya Matsui
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Toho  (Japan Theatrical) 
| released       =     
| runtime        = 89 min (DVD version)
| country        = Japan
| language       = Japanese, English
}}
  is a 2008 Japanese Animated Film produced by OLM, Inc.|OLMs Team Kamei division, based on the Tamagotchi digital pet franchise jointly created by Bandai and WiZ. It is directed by Jōji Shimura and written by Aya Matsui, released into Japanese theaters on December 20, 2008, and on DVD on June 26, 2009. It was later released in France on February 17, 2010.
 Anime sequel of the same name.

==Story==
 
At Tamagotchi School, the Tamagotchi children wait for the flying library to arrive. While talking about the kind of books they would like to read, another young student adds a rude comment to Mametchis dream. Chamametchi recognizes him as Kikitchi, a new student in her class. Mametchi then explains to Kikitchi that the flying library is a flying ship that contains magical books that children are magically transported into and made the main character when they read them. Just then, Kikitchis sensitive hearing picks up the sound of the ships horn. Although no one else can hear it, the ship appears after a minute. The Otogitchi Siblings, who take care of the library, bring the ship down, and let down a flight of stairs, which the Tamagotchi children eagerly climb up.

Down in the library, the children eagerly grab various books and open them, which causes various objects to pop out. The Tamagotchi children then jump into different books, causing them to transport into the story. The title theme then plays as the Tamagotchi go on their various adventures inside their stories. Chamametchi flies on a magic carpet to a castle in the sky; Mametchi relaxes on a machine in the ocean, which he then goes into and flies away into space in; Memetchi goes to an elegant party in a castle and meets a prince, which she and Makiko fight over; Kuchipatchi eats endlessly in a land made entirely out of candy; and various other Tamagotchi children dance and fly in other lands of music and letters. The last person to be seen is Kikitchi, who sits on a leaf alone looking at the sky. Next to him, his scarf has been taken off, showing he has no interest in the story he is in.

After the children exit their stories and are starting to leave the library, Kikitchi hears a small voice calling out for help. However, due to the fact that he can hear smaller sounds than others, no one else can hear it, and tell him he must be hearing things. As Kuchipatchi, Mametchi, Memetchi, and Chamametchi tell about what happened while they were in their stories, Makiko and Memetchi get into an argument over the prince that had been in their story. As they are walking, Kikitchi makes them fall by throwing banana peels in front of them. As he laughs, Memetchi asks him if he enjoyed his story. He responds that he didnt like it at all.

Meanwhile, Mametchi has an idea for an invention, and he rushes home. After several failed attempts, he finally gets his experiment right, and he invites his friends over to watch him complete it. But after several events, the experiment goes wrong, and a small creature emerges. Mametchi explains that he did not expect his experiment to become and actual living thing, but that the creature can absorb the feelings of others and use it to make other people happy. Mametchi decides to call his new creature "Hapihapitchi".

While the birth of Hapihapitchi is being celebrated, Kikitchi eats a large meal on his own. He is full after one bite, and turns on his TV. He sees an advertisement for Celebria, a newly completed super-shopping mall. Kikitchis parents then appear on TV, explaining that they built Celebria as a place where all Tamagotchis could go to have fun. Kikitchi sadly turns off the TV, and watches fireworks from outside his window.

Hapihapitchi begins to use her power to make people happy. She attempts to use her power on Kikitchi, but Kikitchi runs away, yelling that forcing people to be happy is "nonsense". Over time, Mametchi begins to notice that Hapihapitchi is actually making people unhappy; they now depend on Hapihapitchi to make them happy when they are upset. Mametchi then tells Hapihapitchi not to use her powers on anyone until he can figure out what to do.

At school, the flying library comes again. Kikitchi, who is hiding in a corner, hears the small voice  calling out for help again, and begins to search for the source. Hapihapitchi also senses the unhappiness that is connected with the voice, and she begins to search for the source, too. As Kikitchi, Mametchi, Memetchi, Kuchipatchi, Chamametchi, and Hapihapitchi run into each other, a book opens up at their feet, entitled "The Worlds Happiest Story". They enter the book, where they notice that, unlike the other magical books in the library, they dont become the main character. The story tells of a man named Happy who lived with his dog named Lucky. Because his name is "Happy", he is determined to become the happiest man in the world, so he sets off on a journey, leaving behind his home and his dog. He becomes rich and a hero, but he still isnt the happiest man in the world. The characters find Happy, but he states that he doesnt want their help. As the Tamagotchis follow Happy through the various pages of the story, they notice that the story loops; the end simply brings them back to the beginning. Kikitchi notices that Happy acts similar to how he acted, and he begins to feel bad. However, before the Tamagotchis can help Happy, the school bell rings, and they have to leave the library. Unknown to anyone else, Kikitchi leaves the Library, the book with him.

Back at his house, Kikitchi remembers an event that occurred when he was younger. A group of younger Tamagotchis were talking about a festival that was to occur the next day, but Kikitchi hears a tree say that it will rain tomorrow. When he tries to tell the others, the children call him a liar and a horrible person. Unable to wait, Kikitchi enters the book he has taken home. As he does, however, a tornado of paper engulfs Celebria, and causes the Tamagotchi caught in the tornado to become two-dimensional. Mametchis family sees the news report on TV, and Chamametchi and Mametchi instantly think of Kikitchi. The family then heads to Mame Laboratories to discuss the problem with other scientists.

At the laboratory, one of the scientists explains that the last page in Happys story had been torn out, so that he is unable to complete his story. Over time, the book had become unstable, and Kikitchi entering the story caused it to become unstable enough to affect the real world. The only way to save the world was to have Otogitchi, the captain of the flying library, write a new ending with a magic crayon. Meanwhile, Papamametchi reluctantly allows Mametchi and his friends to enter the story to save Kikitchi.

Mametchi and his friends enter the book on the flying library ship. In the book, Kikitchi is talking with Happy, who is beginning to believe he will never be happy. The flying ship finally finds them, where Otogitchi explains to Happy that they have to write a new ending to the story. They attempt to write several different endings, but they all disappear, meaning that they arent correct. When he begins to become discouraged, the other Tamagotchi begin to give him ideas of what makes them happy. Kikitchis idea, which is when someone says "hello" to him during class, has a large impact on Happy, and he begins to get an idea. Unfortunately, the world inside the story begins to fall apart, and the magic crayon is accidentally dropped. Since Happy states that he cant leave the book with the other Tamagotchi, Hapihapitchi asks Mametchi if she can try to make Happy happy. Happy is surrounded with memories, and he finally decides how he wants his story to end. Kikitchi risked his life to get the magic crayon back, and he asks Happy what he wants the ending to be. Happy says that he wants to go back to his home as it was before he left. Otogitchis sister draws a door, and Happy opens it; suddenly, the book stops disintegrating, and returns to normal. Kikitchi realizes that Lucky was the one calling out to him, and is happy that Happy is now happy.

Suddenly, Hapihapitchi collapses in a field. Mametchi explains that without any happiness, Hapihapitchi would die. Hapihapitchi gets up and weakly sprinkles her last happiness over Kikitchi, stating that she promised she would make him happy. Kikitchi begins to cry, and Mametchi tells everyone that Hapihapitchi needs happiness to absorb, but no one can be happy while Hapihapitchi is about to die. Happy approaches Hapihapitchi, picks her up, and tells her that he is the happiest man in the world. Suddenly, Hapihapitchis health is restored, and she springs back to life and Kikitchi stops crying but is still sad. The Tamagotchi wave goodbye to Happy and Lucky, and leave the book on the flying ship.

Back in the real world, the citizens of Tamatown celebrate the return of the heroes. Kikitchis parents comfort him, by promising to make more time for him from now on. Happily, Kikitchi introduces his parents to his new friends.

The end credits show Tamatown being cleaned up after the paper tornado, and the Tamagotchi children continuing to enjoy the magical story books. A picture inside the wall of Mametchis home also shows a brief picture of Tanpopo, the main heroine from the first Tamagotchi film.

==Cast==
{|class="wikitable"
|- Character
!width="33%"|Japanese Version
|-
!Mametchi Rie Kugimiya
|-
!Chamametchi
|Yūko Gibu
|-
!Memetchi
|Ryōka Yuzuki
|-
!Kuchipatchi Asami Yaguchi
|-
!Kikitchi Junko Takeuchi
|-
!Hapihapitchi Satomi Kōrogi
|-
!Happy Daisuke Sakaguchi
|}

==External links==
* 

 

 
 