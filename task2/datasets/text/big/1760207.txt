The Passion of Anna
 
{{Infobox film name            = The Passion of Anna image           = The Passion of Anna poster.jpg caption         = Film poster director        = Ingmar Bergman producer        = Lars-Owe Carlberg writer          = Ingmar Bergman starring        = {{Plainlist|
*Max von Sydow
*Liv Ullmann
*Bibi Andersson }} music           = cinematography  = Sven Nykvist editing         = Siv Lundgren distributor     = United Artists released        = 10 November 1969 runtime         = 101 minutes country         = Sweden language        = Swedish budget          =
}}
The Passion of Anna is a 1969 Swedish drama film written and directed by Ingmar Bergman. Its original Swedish title is En passion, which means "A passion". Bergman was awarded Best Director at the 1971 National Society of Film Critics Awards for the film.

==Plot==
The audience is introduced to Andreas Winkelman, a man living alone and emotionally desolate after the recent demise of his marriage. He meets Anna, who is grieving the recent deaths of her husband and son.  She uses a cane as a result of the car crash that killed them. While Anna uses Andreas phone, he listens to her conversation, after which she departs visibly distraught. Anna has left her handbag behind and Andreas searches it, finding and reading a letter from her husband that will later prove she is deceptive.

Andreas is friends with a married couple, Eva and Elis (mutual friends of Anna) who are also in the midst of psychological turmoil.  Elis is an amateur photographer who organizes his work based on emotion. Eva feels Elis has grown tired of her and has problems sleeping. One night while Elis is away, Eva visits Andreas, as she is bored and lonely. They listen to music and drink wine, which makes them drowsy, and finally Eva sleeps for several hours. When she wakes up, they have sex. Afterward, she explains that during her only pregnancy years ago, she went to the hospital to treat her insomnia. The medicine they gave her helped her condition but killed the child. She conveys that it allowed her and Elis to share a moment of emotional affinity.

Andreas visits Elis whom he promised could photograph him. Elis leaves the room for a moment and Eva enters. In their conversation, Eva reveals that Anna has moved in with Andreas, and though she is not displeased (as she likes both of them), she warns him to be wary of Anna. Elis enters  the room; when Eva asks him why he looks angry, he says he only gets angry at human trifles (alluding to the affair).
Their relationship is not passionate but Andreas and Anna start off relatively content.  Anna appears zealous in her faith and steadfast in her search for truth, but gradually her delusions surface-reinforced by what Andreas read in the letter. For his part, Andreas is unable to overcome his feelings of deep humiliation about himself and remains disconnected, further dooming the relationship with Anna, as he prefers solitude and freedom to companionship.  Throughout the film, an unknown person among the island community commits acts of animal cruelty, hanging a dog and violently killing cattle.  A friend of Andreas is wrongly accused of these crimes, leading the community to threaten and beat him, catalyzing his suicide. Within a few days of the friends death, Anna and Andreas have a physical fight during which they reveal their strong distaste for each other. Afterwards Anna lays in bed while Andreas follows two firetrucks that passed his home. They were headed to a large barn fire. When Andreas arrives, he is told that the unknown man who is the true culprit of the animal cruelty covered a barn full of animals in gasoline and lit it on fire, locking the animals in. It is obvious to the community that Andreas friend was unjustly abused and committed suicide because of flimsy human suspicion, therefore, chances for healing are lost.

Anna shows up at the fire in her car. Andreas gets in. As they drive down the road beside the sea, Andreas explains that he desires his solitude again and that their parting will not be difficult as neither one truly loved the other. He also reveals that he knows the truth about her husband. Anna begins to speed the car while he talks. He asks if she is going to kill him like she killed her husband and they fight over the wheel, eventually he stops the car in the flat ground beside the road. He tells her she is out of her mind. Anna drives away while Andreas paces back and forth on the side of the road.

== Analysis ==
Bergman uses his deconstructionist devices, cutting occasionally to his actors being interviewed about their characters, scratched lenses, and uncorrected muted colour, and inserts others footage into reels.  There is a black and white filmed dream sequence (that alludes to his earlier film Shame (1968 film)|Shame) to contrast with Nykvists muted colour photography.

==Cast (in credits order)==
*Max von Sydow – Andreas Winkelman
*Liv Ullmann – Anna Fromm
*Bibi Andersson – Eva Vergerus
*Erland Josephson – Elis Vergerus
*Erik Hell – Johan Andersson
*Sigge Fürst – Verner
*Britta Brunius – Woman in dream
*Lars-Owe Carlberg – Police officer
*Malin Ek – Woman in dream
*Barbro Hiort af Ornäs – Woman in dream
*Svea Holst – Verners wife
*Marianne Karlbeck – Woman in dream
*Annika Kronberg – Katarina
*Brita Öberg – Woman in dream
*Brian Wikström – Police officer
*Ingmar Bergman – Narrator (voice) (uncredited) 
*Hjördis Petterson – Johans sister (uncredited)

== External links ==
* 
* 

 

 
 
 
 
 
 