Happily Ever After (2009 film)
 
{{Infobox film
| name = Happily Ever After
| image = Happily Ever After poster.jpg
| caption =
| producer = Yap Kai Lyn
| director = Azrael Chung   Ivy Kong
| writer = Philip Lui
| starring = Ken Hung   Michelle Wai   Carlos Chan
| editing = Cheung Ka-Fai
| cinematography = Edmond Fung
| studio = Emperor Motion Pictures
| distributor =
| released =  
| runtime = 92 minutes
| country = Hong Kong
| language = Cantonese
| budget =
}}
Happily Ever After ( ) is an 2009 Hong Kong drama-romance film directed by Azrael Chung and Ivy Kong.

==Plot==
Nan Au-Yeung (Michelle Wai) and Sze Tso-chi (Ken Hung) share the same birthday, go to the same school, love photography, and are just as competitive. But they did not know of each other’s existence until they “crossed swords” at a debate tournament. And they both felt as if the fairytale prince and princess finally found each other. Later in a birthday party, Nan thought Chi played a trick on her, leaving a slap on his face. Four years later, Nan Au-Yeung encounters his ghost and learns that he is already dead...

==Cast==
* Ken Hung - Sze Tso-chi
* Michelle Wai - Nan Au-Yeung
* Carlos Chan - Chun Man
* Benz Hui
* Jacky Leung
* Gladys Fung
* A. Lin

==Critical reception==
Perry Lam of Muse (Hong Kong magazine)|Muse Magazine gave the film a mixed review, writing that its a decent commercial entertainment designed to satisfy our residual, vulgar longing for the intensely romantic. 

==References==
 

==External links==
*  
*   at the Hong Kong Movie Database

 
 
 
 
 


 
 