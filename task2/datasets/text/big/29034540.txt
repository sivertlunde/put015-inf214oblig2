My Week with Marilyn
 
 
{{Infobox film
| name = My Week with Marilyn
| image = My Week with Marilyn Poster.jpg
| caption = Theatrical release poster Simon Curtis
| producer = David Parfitt Harvey Weinstein
| screenplay = Adrian Hodges
| based on =   Michelle Williams Kenneth Branagh Eddie Redmayne Emma Watson Judi Dench
| cinematography = Ben Smithard
| editing = Adam Recht
| music = Conrad Pope Alexandre Desplat
| studio = The Weinstein Company BBC Films LipSync Productions Trademark Films
| distributor = Entertainment Film Distributors The Weinstein Company
| released =  
| runtime = 101 minutes   
| country = United Kingdom
| language = English
| budget = Pound sterling|£6.4 million (United States dollar|$10 million)   
| gross =$34,556,085 
}} Simon Curtis Michelle Williams, Colin Clark, it depicts the making of the 1957 film The Prince and the Showgirl, which starred Marilyn Monroe (Williams) and Laurence Olivier (Branagh). The film focuses on the week in which Monroe spent time being escorted around London by Clark (Redmayne), after her husband, Arthur Miller (Dougray Scott), had left the country.

Principal photography began on 4 October 2010 at Pinewood Studios. Filming took place at Saltwood Castle, White Waltham Airfield and on locations in and around London. Curtis also used the same studio in which Monroe shot The Prince and the Showgirl in 1956. My Week with Marilyn had its world premiere at the New York Film Festival on 9 October 2011 and was shown at the Mill Valley Film Festival two days later. The film was released on 23 November 2011 in the United States and 25 November in the United Kingdom.
 Golden Globe for Best Actress in a Musical or Comedy Motion Picture. She also earned Best Actress nominations from the Academy Awards and British Academy Film Awards.

==Plot==
  Colin Clark Michelle Williams). Colins first task is to find a suitable place for Marilyn and her husband, Arthur Miller (Dougray Scott), to stay at while they are in England. The press find out about the house, but Colin reveals he secured a second house just in case, impressing Olivier and Marilyns publicist, Arthur P. Jacobs (Toby Jones).

The paparazzi find out about Marilyns arrival at Heathrow and they gather around the plane when it lands. Marilyn brings her husband, her business partner, Milton H. Greene (Dominic Cooper), and her acting coach Paula Strasberg (Zoë Wanamaker) with her. She initially appears to be uncomfortable around the many photographers, but relaxes at the press conference. Olivier becomes frustrated when Marilyn is late to the read-through. She insists Paula sits with her and when she has trouble with her lines, Paula reads them for her. The crew and the other actors, including Sybil Thorndike (Judi Dench), are in awe of Marilyn. Colin meets Lucy (Emma Watson), a wardrobe assistant to whom he is attracted, and they go on a date. Marilyn starts arriving later to the set and often forgets her lines, angering Olivier. However, Sybil praises Marilyn and defends her when Olivier tries to get her to apologise for holding the shoot up.
 a new play that appears to poke fun at her. Arthur later returns to the United States. Vivien comes to the set and watches some of Marilyns scenes. She breaks down, saying Marilyn lights up the screen and if only Olivier could see himself when he watches her. Olivier tries unsuccessfully to reassure his wife. Marilyn does not show up to the set following Arthurs departure and she asks Colin to come to Parkside and they talk. The crew becomes captivated by Marilyn when she dances for a scene and Milton pulls Colin aside to tell him Marilyn breaks hearts and that she will break his too. Lucy also notices Colins growing infatuation with Marilyn and breaks up with him.
 Philip Jackson), Marilyns bodyguard. Colin is called to Parkside one night as Marilyn has locked herself in her room. Colin enters her room and Marilyn invites him to lie next to her on the bed. The following night, Marilyn wakes up in pain and claims she is having a miscarriage. A doctor tends to her and Marilyn tells Colin that she wants to forget everything. She later returns to the set to complete the film. Olivier praises Marilyn, but reveals she has killed his desire to direct again. Lucy asks Colin if Marilyn broke his heart and he replies that she did, to which she replies that he needed it. Marilyn comes to a local pub, where Colins is staying, and thanks him for helping her. She kisses him goodbye and Roger drives her to the airport.

==Cast== Michelle Williams - Marilyn Monroe Sir Laurence Olivier Colin Clark
* Emma Watson - Lucy
* Dougray Scott - Arthur Miller
* Dominic Cooper - Milton H. Greene
* Julia Ormond - Vivien Leigh Dame Sybil Thorndike
* Derek Jacobi - Sir Owen Morshead
* Zoë Wanamaker - Paula Strasberg Philip Jackson - Roger Smith

==Production==

===Development===
  wrote the screenplay.]] Simon Curtis approached producer David Parfitt about making a film based on them.       Parfitt said everyone liked the idea, but because Monroe is so familiar and iconic to people, they wondered what was left to say.  Adrian Hodges, who wrote the screenplay, told David Gritten of The Daily Telegraph "If youd said to me one day Id write a film about her, Id have been amazed, because I wouldnt have known where to start."  Gritten reported the saving grace for Hodges was that Clarks books were written about Monroe at a specific time. 
 Blue Valentine.  The film is produced by Trademark Films and is also financed by LipSync Productions.    My Week with Marilyn is Curtis debut feature film.   

===Casting===
  as Marilyn Monroe and Dougray Scott as Arthur Miller on set in Mayfair, London.]]
In August 2009, Marcell Minaya of Digital Spy reported Scarlett Johansson had become the frontrunner to play Monroe in the film.    Kate Hudson, Amy Adams and Michelle Williams were also named as potential candidates for Monroe.  Three months later, it was revealed that Williams had been in talks for the role.    In November 2011, Curtis revealed he had only sought Williams to play Monroe.  She was the only actress that producers met with during casting.  Williams committed to My Week with Marilyn two years before shooting started.  The actress told Adam Green of Vogue (magazine)|Vogue that the notion of playing Monroe was daunting, but as she finished reading the script, she knew she wanted the role.    The actress then spent six months reading biographies, diaries, letters, poems and notes about and from Monroe. She also looked at photographs, watched her films and listened to recordings.  Williams had to gain weight for the role and she worked with a choreographer to help perfect Monroes walk. 
 Harry Potter actress Emma Watson had been cast in the small role of wardrobe assistant, Lucy.    Watson was scheduled to spend only a few days on set shooting her scenes to prevent her studies at Brown University from being interrupted.  Kenneth Branagh began talks with producers for the role of Laurence Olivier in July 2010 after Ralph Fiennes had to pull out to direct his adaptation of Coriolanus (film)|Coriolanus.   Branagh was later cast in the role. 
 Philip Jackson plays Monroes private detective and Judi Dench plays Sybil Thorndike.     Zoë Wanamaker is Paula Strasberg, the actresss acting consultant and Richard Clifford was cast as Richard Wattis, the actor who played a courtier in The Prince and the Showgirl.  The film also stars Toby Jones, Geraldine Somerville, Simon Russell Beale and Michael Kitchen.   It was announced on 8 October 2010, that casting on the film had been completed. 

===Filming===
Principal photography on My Week with Marilyn commenced on 19 September 2010.    Dench filmed her scenes during that month as she had to go to India to begin work on The Best Exotic Marigold Hotel.  Shooting took place at the Pinewood Studios from 4 October 2010.  Three days later, White Waltham Airfield was turned into a 1950s London Heathrow Airport to recreate the moment when Monroe arrived in Britain to begin production on The Prince and the Showgirl.    Curtis used the studio in which Monroe shot The Prince and the Showgirl in 1956 to film scenes for My Week with Marilyn.    Williams was given the same dressing room Monroe had used at the time of her shoot.  Filming took place on locations in and around London.  One such location included Parkside House in the village of Englefield Green, where Monroe and Miller lived during their stay in England.     The films production designer, Donal Woods, toured the house with Curtis prior to filming and noticed the exterior looked much as it did when Monroe posed for some publicity shots there fifty years ago. 

British Cinematographer reported the production had filmed scenes at Saltwood Castle, near Folkestone, where Clark grew up as a young boy.    The film was also shot at Eton College, which Clark attended, and outside Windsor Castle for a few hours during one Saturday morning.  Cinematographer Ben Smithard said the creative and visual references in My Week with Marilyn did not come from other films, but from stills of American photographer and painter, Saul Leiter.  Smithard told British Cinematographer that a significant amount of time was spent in pre-production. He said "On an historic film like this, you need to do as much prep as you can get. Its like a history lesson, and you can learn about a point in time."  The cinematographer framed My Week with Marilyn in the standard anamorphic format as it is "very good for personal stories" and suited the film. He added that it is easy to frame two actors, but the format is not so good for architectural features.  The shoot for My Week with Marilyn lasted seven weeks and was completed in November 2010.     Post production ran from 28 November 2010 until 31 August 2011. 

===Costumes and make-up===
 
The costume designer for My Week with Marilyn was Jill Taylor.    Taylor, who previously worked on Sliding Doors and Match Point, created the costumes for the film in six weeks and she dressed the entire cast.    She sourced many of the items from vintage shops, auction houses and markets.  Speaking to Estella Shardlow of Vintage Seekers, Taylor said "I trawled through loads and loads of antiques fairs and vintage shops to see if we could find original vintage pieces that would suffice for the film. We were pretty successful but we also had to reproduce a lot from original photographs – for example, we had to do the scene where she lands in this country, which is well-documented on newsreel."    Taylor told Shardlow it was difficult to find fabrics that looked as "lush" as they did in the fifties and she had some challenging costumes to make, including a dress from The Prince and the Showgirl for Denchs character, Sybil Thorndike.  The designer also said she had to make some adaptations to a suit worn by Ormond, as she is a completely different body shape to her character, Vivien Leigh.  During research for Watsons character, Lucy, Taylor found an original cast and crew photograph from The Prince and the Showgirl.  One of the girls in the picture was wearing a tartan dress, so Taylor went out and found Watson an original tartan dress to wear.  She said she and her team had fun with Watsons character, as she is a young and fashion-conscious girl. The designer said "Given the American influence to England in the 50s, her style is quite Sandra Dee and girlie." 

Taylor told Sarah Smith of InStyle that she worked from many photographs of Monroe, particularly ones taken on her honeymoon with Miller.  Taylor said she drew upon a certain picture of the actress wearing a mans shirt and a pencil skirt and she made the outfit for the film.  Taylor added "There was also one scene when   is in a car and shes got a black chiffon headscarf and there was a coat I did for her that was actually in the Sothebys catalogue. We reproduced that coat, which was like an oatmeal silk coat with a black velvet collar, and we made it into a jacket for Michelle, rather than a coat."  Taylor also worked with Williams during the design process and she explained the actress would bring picture references for her.  Taylor would do sketches for Williams as they talked and the designer said "it was a collaboration about what she thought she would like to wear and what I thought."  The designer told Smith she was very pleased with how successful the white dress she had made for Williams during The Prince and the Showgirl scenes turned out. Taylor used a fitting photograph of Monroe with The Prince and the Showgirl costume designer to help her make the garment.  She explained that the dress was quite intricate to make and there were no doubles, so Williams had to wear the same dress for eleven days.  Taylor worried that something would happen to the dress and was relieved when the shoot was over.  When asked if Williams had a favourite outfit, Taylor said the actress particularly enjoyed wearing a black dress and the skirt and shirt combination. 

The hair and make-up designer for the film was Jenny Shircore.    She told Joe Nazzaro of Make-Up Artist Magazine that the biggest challenge for her was transforming Williams into Monroe. Shircore said Williams features are quite different from Monroes, but she did not want to use prosthetics to shape her face as the emotion Williams was conveying in her performance had to come through the make-up.  Shircore explained "There are times in the film when shes actually wearing very little make-up but we still kept tiny aspects of Marilyn, such as the eyebrows, the shading and the shape of the lips, so we would keep three or four major points that helped us towards Marilyn. Some of it was quite difficult, because Michelles eyes are completely different. Marilyn had very distinctive eyelids, so we had to try and form that shape on Michelles eyes by the use of light and shade."  The make-up artist said most of the scenes in the film see the actors from The Prince and the Showgirl in their film make-up, so Shircore had to copy the original scene for My Week with Marilyn and get it right.  She also revealed a few minor prosthetics were used on some cast members to help recreate the characters. Shircore told Nazzaro "Im not going to give them away, but they are things that make a difference and theyre all beautifully conceived, produced and worn by the actors." 

===Music=== Lang Lang Heat Wave". Autumn Leaves" and "Memories Are Made of This".  The soundtrack was released digitally on 1 November 2011. 

The film also includes original music from The Prince and the Showgirl composed by Richard Addinsell

==Release==
The first trailer for the film was introduced by Harvey Weinstein during the 2011 Cannes Film Festival.  It was officially released on 6 October 2011.  My Week with Marilyn had its world premiere on 9 October 2011 at the 49th New York Film Festival.  The film was shown at the Mill Valley Film Festival two days later and it was then added to the lineups of the Hamptons International Film Festival and the 26th Annual Fort Lauderdale International Film Festival.   
 AFI Fest.   My Week with Marilyn was shown out of competition at the Rome Film Festival and it closed the Dubai International Film Festival on 13 December.   It was then shown at the History of Capri#20th and 21st centuries|Capri, Hollywood International Film Festival in January 2012.   

My Week with Marilyn was released on 25 November in the United Kingdom.  The film was originally scheduled to be released on 4 November in the United States, but shortly after its premiere at the New York Film Festival, The Weinstein Company moved the release date to 23 November.     The film opened in a limited release in 73 markets and 244 theaters.  On 17 February 2012, Kristina Bustos of Digital Spy reported My Week with Marilyn would expand into 600 more theatres across the United States on 24 February. 

===Home media===
My Week with Marilyn was released on DVD and Blu-ray Disc|Blu-ray on 13 March 2012 in the United States and on 16 March in the United Kingdom.     It was released in Australia on 21 June 2012.  The film is distributed by The Weinstein Company and Anchor Bay Entertainment.  Extras include a directors commentary and a featurette called "The Untold Story of an American Icon".  My Week with Marilyn entered the UK DVD Top 40 at number six and the Blu-ray Top 40 at number nine.   On its first week of release in the US, the film entered at number six on the DVD Sales Chart, selling an estimated 172,748 DVDs making $2,589,493. 

==Reception==

===Box office===
My Week with Marilyn earned £749,819 upon its opening weekend in the United Kingdom.  The film opened to 397 cinemas and landed at number three in the UK box office top ten.  The following week the film earned £483,239 and slipped three places in the box office chart.  In its third week, My Week with Marilyn earned £192,834 and fell to number seven.  In the first five days of its opening in limited release, My Week with Marilyn grossed $2.06 million in the United States.    Ray Suber of Box Office Mojo reported the film played at 123 locations on 23 and 24 November, before expanding to 244 cinemas for the Thanksgiving three-day weekend, where it placed in the Top 12 with $1.75 million.  Amy Kaufman of the Los Angeles Times said 71% of people who saw My Week with Marilyn during its opening few days in the US were over the age of 35.  In January 2012, six weeks after it was released, My Week with Marilyn broke the $10 million mark in cinemas. 

===Critical reception=== rating average of 7.1 out of 10.    According to the sites summary of the critical consensus, "Michelle Williams shines in My Week with Marilyn, capturing the magnetism and vulnerability of Marilyn Monroe."  Metacritic, which assigns a score of 1–100 to individual film reviews, gives the film an average rating of 65 based on 38 reviews.  David Rooney from The Hollywood Reporter praised Williams performance and said she nailed Monroes vocal style.  Rooney also praised Redmayne as Clark, saying his scenes with Williams were captivating.  However, Rooney went on to say "Fault lies with both Hodges’ workmanlike script and Curtis failure to excavate much psychological depth."  He added My Week with Marilyn is starchy and short on perspective, making it "superficial showbiz pageantry."  Ronnie Scheib of Variety (magazine)|Variety said My Week with Marilyn "flits uneasily between arch drawing-room comedy and foreshadowed tragedy" and is too stagily directed by Curtis, who lines up the characters with "no attention to spatial logic or rhythmic flow."    Scheib added the film coasts on Williams performance, while the story feels like it has been ripped from film fan magazines of the time.  Rex Reed of The New York Observer called My Week with Marilyn "pure perfection." 
 Blue Valentine -- her moves and voice as Marilyn evoke the subjects understated, magnetic performances."    Weinreich went on to praise the rest of the cast, including Redmayne, Branagh and Dench, saying they are "especially good."  A writer for indieWire said My Week with Marilyn is like a "superficial Lifetime made for TV-movie."    The writer went on to say the film is not terrible, but there is "very little meat on the bone."  They added the film has a terrific cast who do their best with an average script.  Robbie Collin, writing for The Daily Telegraph gave the film three out of five stars and said "Michelle Williams makes a mesmeric Monroe in My Week With Marilyn, but the film falls disappointingly short on boop-boop-be-doo." 
 David Denby also praised Williams performance as Monroe, saying "In My Week with Marilyn, Williams makes the star come alive. She has Monroes walk, the easy, swivelling neck, the face that responds to everything like a flower swaying in the breeze. Most important, she has the sexual sweetness and the hurt, lost look that shifts, in a flash, into resistance and tears."    The critic called the film "charming and touching" and said it is expertly made.  Writing for Time (magazine)|Time, Mary Pols called My Week with Marilyn "nothing more than a lively confection."    Pols went on to say "Williams locates a central truth, the contradictory allure of this utterly impossible woman — mercurial, vain, foolish, but also intelligent in some very primal way and achingly vulnerable."  Upon giving the film three and a half out of four stars, critic Roger Ebert said "What matters is the performance by Michelle Williams. She evokes so many Marilyns, public and private, real and make-believe. We didnt know Monroe, but we believe she must have been something like this. Were probably looking at one of this years Oscar nominees." 

Manohla Dargis of The New York Times thought Branagh was miscast as Olivier, but she said he made up for that with "his crisp, at times clipped, enunciation and a physical performance that gives Olivier enough vitality so that when, early in, the character sweeps into his production office with his wife, Vivien Leigh (Julia Ormond, a wan placeholder for the original), he dazzles Clark and jolts this slow-stirring movie awake."    Of Williams, the film critic said she "tries her best, and sometimes thats almost enough."  Dargis said the main problem is with Hodges script, which "offers a catalog of Monroe stereotypes."  Stella Papamichael of Digital Spy gave the film four out of five stars and she praised many of the casts performances. Papamichael added "While you wont learn anything new about Marilyn Monroe, you can revel in the silky feel of nostalgia."  Empire (magazine)|Empire magazines Angie Errigo gave My Week with Marilyn three out of five stars and she said "At moments hilarious and others touching, its a sweet, slight affair, more pretty pageant than pithy biographical drama. Expect awards nominations to stack up for Williams and Branagh."  The Wall Street Journals film critic, Joe Morgenstern was negative about the film saying, "When bad movies happen to good people, the first place to look for an explanation is the basic idea. That certainly applies to "My Week with Marilyn," a dubious idea done in by Adrian Hodgess shallow script and Simon Curtiss clumsy direction."  Nishi Tiwari of Rediff.com said Williams is "a fascinating watch", but there is nothing about Monroe in the film that we did not know already. 

===Accolades===
  Take This Best Female Best Actress Best Supporting Best Actress Washington D.C. Best Supporting Actor.  On 11 December, Williams won the Boston Society of Film Critics Award for Best Actress.  She also won the Best Actress award from the Detroit Film Critics Society, while Branagh garnered a nomination for Best Supporting Actor. 
 Best Actress Best Actress Best Supporting Actor. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 