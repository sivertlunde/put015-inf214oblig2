Buried Country
 
Buried Country is a highly regarded documentary film, book    and soundtrack.   It tells the story of   and was originally published by Pluto Press in 2000, {{cite book
| author= Clinton Walker
| title=    Buried Country: The Story of Aboriginal Country Music
| publisher=Pluto Press
| location= Sydney
| year=     2000
| pages=    323
| ISBN=     1-86403-152-2
}}  and in 2015 was re-published in a new updated edition by Verse Chorus Press. Film Australia has produced a study guide. {{Cite book
| publisher = Film Australia
| isbn = 1-86403-152-2
| pages = 73
| last = Kirkbright
| first = Christopher J.
| title = Study Guide: Buried Country: the story of Aboriginal country music
| accessdate =11 October 2008
|year=2000
| url = http://www.curriculumsupport.education.nsw.gov.au/secondary/hsie/aboriginal6/assets/pdf/buriedcountry.pdf
}} 

The soundtrack Buried Country: Original Film Soundtrack (Larrikin Records) is a now-deleted double CD produced by Clinton Walker and showcasing music by the Aboriginal artists featured in the film and book. The documentary was directed by Andy Nehl and written by Andy Nehl and Clinton Walker and shot by Warwick Thornton.

==Featured artists==
*1940s-50s
**Jimmy Little Georgia Lee
**Vic Sabrino
**Herbie Laughton
*1960s-70s
**Dougie Young
**Black Allan Barker
**Lionel Rose
**Vic Simms
**Bobby McLeod
**Harry and Wilga Williams George and Ken Assang
*1970s-80s Gus Williams
**Auriel Andrew Bob Randall
**Isaac Yamma
*1980s-90s
**Roger Knox
**Kevin Gunn
**Kev Carmody
**Troy Cassar-Daley
**Archie Roach
**Ruby Hunter Tiddas
**Warumpi Band

==References==
 

==External links==
*  Collection Listing for Buried Country: The Story of Aboriginal Country Music by Clinton Walker
*  Film Listing
*  
* 

 
 
 
 
 
 
 
 
 
 


 
 
 