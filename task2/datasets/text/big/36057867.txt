Ayul Kaithi
{{Infobox film
| name           = Ayul Kaithi
| image          = 
| caption        = 
| director       = K. Subash|K. Subhash
| producer       = A. N. Ramasamy
| writer         = K. Subhaash
| starring       =  
| music          = Sankar Ganesh
| cinematography = Y. N. Murali
| editing        = Krishnamoorthy Siva
| studio         = Ram Balaji Movies
| distributor    = Ram Balaji Movies
| released       =  
| country        = India
| runtime        = 130 minutes
| language       = Tamil
}}
 1991 Tamil Tamil drama Prabhu and Revathi in lead roles. The film, produced by A. N. Ramasamy, had musical score by Sankar Ganesh, and was released on 29 June 1991.  

==Plot==

Chandrasekhar (Prabhu (actor)|Prabhu), a prisoner condemned to the life imprisonment, escapes from the jail to probably kill his ex-girlfriend Nithiya (Revathi). Sudharshan (Livingston (actor)|Livingston), a police officer, tries to catch him.

==Cast==
 Prabhu as Chandrasekhar alias Sekhar
*Revathi as Nithiya Livingston as Sudharshan
*Goundamani
*Chinni Jayanth as Chandrasekhars friend
*Jaiganesh as Jeevanandham
*Usilaimani

== Soundtrack ==

{{Infobox album |  
  Name        = Ayul Kaithi |
  Type        = soundtrack |
  Artist      = Sankar Ganesh |
  Cover       = |
  Released    = 1991 |
  Recorded    = 1991 | Feature film soundtrack |
  Length      = 16:22 |
  Label       = |
  Producer    = Sankar Ganesh |  
  Reviews     = |
  Last album  = |
  This album  = |
  Next album  = |
}}

The film score and the soundtrack were composed by film composer Sankar Ganesh. The soundtrack, released in 1991, features 4 tracks.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Lyrics !! Duration 
|-  1 || Vaali (poet)|Vaali || 1:28
|- 2 || Paaduthu Paaduthu (duet) || S. P. Balasubrahmanyam, K. S. Chithra || 4:10
|- 3 || Oru Raakkkozhi || S. P. Balasubrahmanyam, K. S. Chithra || 4:52
|- 4 || Kathazha Kadu || Malaysia Vasudevan, Swarnalatha || 5:52
|}

==References==
 

 
 
 
 
 


 