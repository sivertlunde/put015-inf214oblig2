I.O.U.S.A.
{{Infobox film
| name           = I.O.U.S.A.
| image          = Iousaposter.jpg
| caption        = Theatrical release poster
| director       = Patrick Creadon
| producer       = Christine OMalley Sarah Gibson
| writer         = Patrick Creadon Christine OMalley Addison Wiggin David Walker
| music          = Peter Golub
| cinematography = Patrick Creadon
| editing        = Doug Blush
| graphic design = Brian Oakes
| distributor    = Roadside Attractions
| released       = August 22, 2008
| runtime        = 85 min.
| country        = United States
| language       = English
| budget         = $500,000
| gross          = $825,037
}}
  American documentary national debt. David Walker, the former U.S. United States Comptroller-General|Comptroller-General, as they travel around the United States on a tour to let communities know of the potential dangers of the national debt. The tour was carried out through the Concord Coalition, and was known as the "Fiscal Wake-Up Tour." 
 Pete Peterson, David Walker, William Niskanen, and Bill Novelli following the screening. The film was broadcast on CNN on January 10, 2009. 

==Plot==
The film follows Bixby and Walker who describe systematically four serious deficits shaping the U.S. economy: budget, savings, the balance of payments, and leadership. As of the early 2008 release of the film they had created a national debt of over $9.6 trillion, $30,000 for each American.   
* The budget deficit section highlights the 53 trillion dollars in unfunded benefits (medicare, medicaid and social security) that will come due and can only be paid by tripling taxes or cutting all government spending except for that to those programs.
* The savings deficit is created by individuals living beyond their means and accumulating personal debt instead of savings.
* The balance of payments problem is the trade deficit caused by the U.S. importing more than it exports, especially from China, draining money and goods from its economy. China has the greatest trade surplus in the world while the USA has the largest trade deficit in the world.
* The leadership deficit is the lack of civic or political leaders willing to make it clear Americans must cut government spending, pay more taxes, save more of their personal income and use less imported materials.

==Cast==
The cast includes David M. Walker,
Warren Buffett,
Douglas Durst,
Alan Greenspan,
Yoni Gruskin,
Kay Harms,
Chrissy Hovde, Paul ONeill,
Diane Rehm,
Robert Rubin,
Scott Spradling,
Mike Tully.
It also includes several members of Congress, including Senators 
Kent Conrad and Judd Gregg,  George Miller, and Ron Paul.   

Archival footage included in the documentary features Humphrey Bogart, Bing Crosby, 
Stephen Colbert,
Sue Herrera,
Steve Kroft,
Chris Parnell,
Peter G. Peterson,
Donald Rumsfeld,
Tim Russert,
Brian Williams,
as well as footage of ten former U.S. presidents: 
Franklin Delano Roosevelt,
Dwight D. Eisenhower,
John F. Kennedy,
Richard Nixon,
Gerald Ford, 
Jimmy Carter, 
Ronald Reagan, George Bush, 
Bill Clinton, and
George W. Bush. 

==Post-festival change==
In the cut of I.O.U.S.A. screened at the Sundance Film Festival in January 2008, the original designers from Agora Financial had audiotape of Nixon conspiring with his advisers to blame the decision to close the “gold window” on “speculators. ” After they sold the film to the Peterson Foundation, that story beat was edited out. The final cut of the film released in Aug. 22, 2008 blames rampant inflation in the 1970s on Arthur Burns, then chair of the Federal Reserve. 

In February 2008, Walker announced that he would be resigning from his post as Comptroller General to become the president and CEO of the newly established The Peter G. Peterson Foundation, a position from which he could more freely draw attention to the serious issues the U.S. is facing. 

==Reception==
I.O.U.S.A. received mostly positive reviews from critics. Rotten Tomatoes reported a score of 86% among critics, and a Certified Fresh rating with a consensus of "A potent and lithely constructed documentary about Americas financial crisis, I.O.U.S.A grabs you with figures but holds you with irreverent wit."  It received a score of 70 from Metacritic and a label of "Generally favorable reviews". 

In a January 2008 review after the films Sundance premiere, Justin Chang wrote: 
{{cite news| date= January 23, 2008
| title= I.O.U.S.A. (documentary)
| url= http://www.variety.com/review/VE1117935899
| author=Justin Chang
| accessdate=2011-11-02
| work=Variety}} 
:"With the same eye for snazzy visual aids and casual human eccentricity that informed his delightful crossword-puzzle docu Wordplay (film)|Wordplay, helmer Patrick Creadon tackles a markedly grimmer story in I.O.U.S.A., an alternately amusing and alarming primer on Americas off-the-charts fiscal irresponsibility. Meant to raise awareness of the skyrocketing national debt and the disaster it spells for future generations, this highly informative docu reps a heady mix of charts, graphs and talking heads, but its superb packaging and timely subject matter should give it a shot at theatrical exposure before it cashes in on homevid and broadcast slots."

In an August 2008 review focused mostly on the films subject matter, Roger Ebert began with the following:   
:"A letter to our grandchildren, Raven, Emil and Taylor: I see you growing up into such beautiful people, and I wish all good things to you as you make the leap into adulthood. But I have just seen a documentary titled I.O.U.S.A. that snapped into sharp focus why your lives may not be as pleasant as ours have been."
According to Ebert: 
:"I dont really believe this review will inspire enormous numbers of people to go see the film. But if they do, theyll find it accomplishes an amazing thing. It explains the national debt, the foreign trade deficit, the decrease in personal savings, how the prime interest rate works, and the weakness of our leaders. No, not only George W. Bush, but politicians of both parties, who know if they vote against tax cuts, they will be lambasted by their opponents and could lose their jobs."
Later that year, I.O.U.S.A. made Eberts list of Top Five Documentary Films of 2008. 

==Companion book== David Walker, Paul ONeill, Bill Bonner. I.O.U.S.A. was also loosely inspired by Bill Bonner and Addison Wiggins book, Empire of Debt. 

==See also==
* United States public debt
* United States federal budget
* Treasury bonds Social Security
* Social Security debate (United States)
* Medicare (United States)
* Gross domestic product
* Late-2000s financial crisis
* Emergency Economic Stabilization Act of 2008
* Troubled Asset Relief Program
* DISCLOSE Act
* Wall Street reform
*  
* Debtocracy Inside Job
* Lets Make Money
* Generation Zero
* Too Big to Fail (film)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 