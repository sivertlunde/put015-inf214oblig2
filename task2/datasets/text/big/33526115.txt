Eloïse's Lover
{{Infobox film
| name           = Eloïses Lover
| image          = File:Eloïse Spanish Poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Spanish poster
| film name      = Eloïse
| director       = Jesús Garay (director)|Jesús Garay
| producer       = José Antonio Pérez Giner
| writer         = Cristina Moncunill
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Diana Gómez Ariadna Cabrol Laura Conejero Bernat Saumell
| music          = Carles Cases
| cinematography = Carles Gusi
| editing        = Anastasi Rinos
| studio         = Els Quatre Gats Audiovisuals S.L.
| distributor    = 
| released       =   
| runtime        = 91 minutes
| country        = Spain
| language       = Catalan
| budget         = 
| gross          = 
}}Eloïses Lover (original Catalan title: Eloïse) is a 2009 Catalan language|Catalan-language Spanish film directed by Jesús Garay (director)|Jesús Garay.

== Plot ==
The drama depicts a young woman, Asia, falling in love with a young artist and lesbian, Eloise, and discovering her sexuality. Scenes of Asia hospitalized and in a coma are interspersed through the film. As the story unfolds we discover their love story and the tragic accident leading up to her hospitalization.

== Cast ==
*Diana Gómez as	Àsia
*Ariadna Cabrol as Eloïse
*Laura Conejero as La Mare
*Bernat Saumell as Nathaniel
*Carolina Montoya as Erika
*Miranda Makaroff as Norah

== External links ==
* 

 
 
 
 
 


 
 