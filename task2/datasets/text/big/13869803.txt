Vijayendra Varma
{{Infobox film
| name           = Vijayendra Varma
| image          = Vijayendra Varma.jpg
| image_size     =
| caption        = 
| writer         = M. Ratnam  
| story          = K. V. Vijayendra Prasad 
| screenplay     = K. V. Vijayendra Prasad  M. Ratnam Sai Vidyardhi
| producer       = Konda Krishnam Raju
| director       = Swarna Subba Rao Laya  Sangeeta  Ankita Koti
| cinematography = V. S. R. Swamy   P. V. V. Jagan Mohana Rao
| editing        = Kotagiri Venkateswara Rao
| studio         = Aditya Productions
| released       =  
| runtime        = 168 minutes
| country        =   India
| language       = Telugu
| budget         =
| gross          = 
}}

Vijayendra Varma is a 2004 Telugu cinema|Telugu, Patriotic film produced by Konda Krishnam Raju on Aditya Productions banner, directed by Swarna Subba Rao. Starring Nandamuri Balakrishna, Laya (actress)|Laya, Sangeeta, Ankita in the lead roles and music composed by Saluri Koteswara Rao|Koti.  The film recorded as flop at box office.  

==Plot==
An unnamed man (Bala Krishna) lives with his wife (Laya), daughter and in-laws. He does not have a name nor does he know who he is. He cannot remember anything that has happened 7 years before. However, he realizes that he possesses special combat skills whenever he comes across evil elements. , she reveals that he was found in river in a mutilated state and he was taken care of by her. As the man goes to Hyderabad searching for his identity, a few incidents lead to the answer. He discovers that he is none other than the most respected and committed Indian Army Officer, Vijayendra Varma. The rest of the story is about how he retraces his past and saves the nation from Pakistani Jihadis.

==Cast==
{{columns-list|3|
* Nandamuri Balakrishna as Vijayendra Varma Laya
* Ankitha
* Sangeetha
* Mukesh Rishi 
* Ashish Vidyarthi 
* Ahuti Prasad
* Chalapathi Rao
* Mannava Balayya|M. Balayya Bhupendra Singh
* Satya Prakash 
* Surya 
* Mohan Raj
* Shob Raj 
* Brahmanandam   Sudhakar
* M. S. Narayana   Venu Madhav
* Giri Babu 
* Raghu Babu 
* Narra Venkateswara Rao 
* Lakshmipathi Manorama
* Ralyalakshmi 
* Delhi Rajeswari
* Banda Jyothy 
* Shoba Rani 
* Ooma Chowdary 
* Master Tanush 
* Baby Kavya  
}}

==Soundtrack==
{{Infobox album
| Name        = Vijayendra Varma
| Tagline     = 
| Type        = film Koti
| Cover       = 
| Released    = 2004
| Recorded    = 
| Genre       = Soundtrack
| Length      = 28:12
| Label       = Supreme Music Koti
| Reviews     =
| Last album  = Naalo Unna Prema   (2004)  
| This album  = Vijayendra Varma   (2004)
| Next album  = Gowri (film)|Gowri   (2004)
}}

Music composed by Saluri Koteswara Rao|Koti. Music released on Supreme Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 28:12
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Siggu Paparo 
| lyrics1 = Suddala Ashok Teja Chitra 
| length1 = 4:43

| title2  = Oh Manmadha  Sirivennela Sitarama Sastry
| extra2  = Udit Narayan, Shreya Ghoshal
| length2 = 5:06

| title3  = Maisamma Maisamma
| lyrics3 = Veturi Sundararama Murthy
| extra3  = Udit Narayan,Chitra
| length3 = 4:46

| title4  = Guntadu Guntadu  
| lyrics4 = Suddala Ashok Teja Kousalya
| length4 = 4:20

| title5  = Mandapetalo Chandrabose 
| extra5  = Shankar Mahadevan,Chitra
| length5 = 4:38
}}

==Others== Hyderabad

==References==
 
 

 

 
 
 
 


 