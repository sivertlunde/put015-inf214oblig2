Breaker! Breaker!
{{Infobox film
| name           = Breaker! Breaker!
| image          = Breaker breaker.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Don Hulette
| producer       = Samuel Schulman Bernard Tabakin Don Hulette John Burrows
| writer         = Terry Chambers
| narrator       = George Murdock Terry OConnor Michael Augenstein
| music          = Don Hulette   Terry Chambers   Denny Brooks
| cinematography = Mario DiLeo
| editing        = Steven Zaillian	
| studio         = Paragon Films Inc. Worldwide Productions AIP
| released       =  
| runtime        = 95 min.  (approx.) 
| country        = United States English
| budget         =
| gross          =
}}

Breaker! Breaker! is a 1977 action film starring Chuck Norris. 

==Plot==
J.D. (Chuck Norris), a trucker from California, returns from the road to learn that an old friend was assaulted and paralyzed by Sergeant Strode (Don Gentry), a policeman in Texas City, California. He makes inquiries into Texas City and learns that its policemen (Strode and Deputy Boles  ) have a history of "trapping" truckers for a corrupt judge running various rackets in the so called "City".

When his younger brother Billy (Michael Augenstein) begins working as a trucker, J.D. warns him to stay away from Texas City. But Billy is easily fooled by an officer (Strode) on a CB radio, who pretends hes a fellow trucker.  

After Billy disappears, J.D. sets out in search of him. He goes to Texas City and barges in on a city council meeting, wherein Trimmings stooges boast of their booties. He befriends a waitress, a single mother, working at a diner which overcharges outsiders. After getting into a fight with the owner of the local wrecking yard and accidentally killing him, J.D. is arrested and sentenced to death by Judge Trimmings.

J.D.s girlfriend tells his fellow truckers whats happened via CB radio. They come to rescue J.D. and Billy and tear the town down.

==Cast==
* Chuck Norris as John David "J.D." Dawes George Murdock as Judge Joshua Trimmings Terry OConnor as Arlene Trimmings
* Don Gentry as Sergeant Strode
* John Di Fusco as Arney
* Ron Cedillos as Deputy Boles
* Michael Augenstein as William "Billy" Dawes
* Dan Vandegrift as Wilfred
* Douglas Stevenson as Drake
* Paul Kawecki as Wade 
* Larry Feder as George
* Jack Nance as Burton
* David Bezar as Tony Trimmings
* Ron Holmstrom as trucker voices
* The Great John L. as Kaminski

==Pop culture notes==

The film was referenced on the May 24, 2007 episode of Late Night with Conan OBrien, when OBrien used a Breaker! Breaker! lever to showcase random scenes from Walker, Texas Ranger. 

The film was a subject of good natured ridicule in a March 21, 2013 video-on-demand release by Rifftrax. 

==References==
 

 
 
 
 
 
 
 