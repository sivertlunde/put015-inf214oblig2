Classe Tous Risques
{{Infobox Film   
| name           = Classe Tous Risques	
| image          = Classe tous risquesPoster.jpg
| caption        = 
| director       = Claude Sautet 
| producer       = Robert Amon Jean Darvey
| writer         = Claude Sautet Pasqual Jardin
| based on       = Classe tous risques by José Giovanni
| starring       = Lino Ventura Sandra Milo Jean-Paul Belmondo 
| music          = Georges Delerue 
| cinematography = Ghislain Cloquet 
| editing        = Albert Jurgenson 
| distributor    = Cinédis
| released       =  
| runtime        = 104 minutes
| country        = France
| language       = French
| budget         =  gross = 1,726,839 admissions (France)
}} Classe Tous Risques ("Consider All Risks") is a 1960 French-Italian gangster film directed by Claude Sautet and starring Lino Ventura, Jean-Paul Belmondo and Sandra Milo. It was first released in the United States as The Big Risk. It tells the story of Abel Davos (Ventura), a French mobster who tries to make his way from Italy through Marseille to Paris, hunted by the French police|police, and Eric Stark (Belmondo), who turns out to be the only person willing to help Davos. The film is an adaptation of the novel with the same title by José Giovanni.

Now widely considered a masterpiece, at the time of its release it was somewhat overshadowed by the French New Wave. It did however influence the French cinema, especially Jean-Pierre Melvilles work.

==Plot==
The French gangster Abel Davos is on the run from the police. He has been trialed in absentia and sentenced to death. He flees to Italy together with his wife Thérèse and their two children. He performs a successful holdup in Milano with his accomplice Raymond, but chased by the police the two decide to return to France. Entering France on a seemingly desert beach, the small group is surprised by two customs officers and end up in a gunfight, where Thérèse and Raymond are killed. Left alone with the children, Abel calls his friends Riton and Fargier in Paris to come and pick him up in Nice. Unable to travel to Nice, they send a man named Éric Stark, who works as an ambulance driver. Éric lets Abel hide in his attic and the two men become friends.

==Production== French Gestapo Collaboration on 13 March 1952.

The film was a produced by Frances Filmsonor, Mondex Films and Les Films Odéon in collaboration with Italys Zebra Film. Principal photography took place from 7 October to 8 December 1959. Locations were used in Nice, Paris and Milano.   

==Release==
The film was released in France on 23 March 1960.  It recorded 1,726,839 admissions in its home country.  The film was banned in Finland during the sixties. 

==Reception==
Kenneth Turran in a review of 2006 for the  , which made its straightforward use of genre look a bit old-fashioned. ... It is worth seeking out, not only because Classe Tous Risques represents a missing piece of film history - a link between the great postwar policiers and the brooding 1960s gangster dramas of Jean-Pierre Melville - but because it is a tough and touching exploration of honor and friendship among thieves." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 
 