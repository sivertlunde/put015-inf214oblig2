The Pied Piper (1933 film)
 
{{Infobox Hollywood cartoon
| cartoon name      = The Pied Piper Silly Symphony
| image             = 
| image size        =
| alt               =
| caption           =
| director          = Wilfred Jackson
| producer          = Walt Disney
| story artist      =
| narrator          =
| voice actor       = 
| musician          = 
| animator          =
| layout artist     =
| background artist = Walt Disney Productions
| distributor       = United Artists
| release date      =   (USA)
| color process     = Technicolor
| runtime           = 7 minutes, 30 seconds
| country           = United States
| language          = English
| preceded by       = Lullaby Land
| followed by       = The Night Before Christmas
}}
 Walt Disney Productions, directed by Wilfred Jackson, and released on September 16, 1933, as a part of the Silly Symphonies series.

==Plot==
In the city of Hamelin, there was a rodent problem as it kept spreading and eating all the food in sight. The mayor thought it was becoming a big nuisance until the Pied Piper showed up. The mayor offered to pay him a bag of gold for his services, or at least thats what the Pied Piper thought. He used his pipe to hypnotize the mice to follow him out of Hamelin. Then, he made cheese with his pipe, tempting the mice to come in, and once all of the mice were in the holes of the cheese, he disappeared all of them and the cheese. When he came back, he got cheated and was only given one gold coin. The reason was because the mayor said that he was only blowing a pipe. Then, seeing how all the children were made to work hard and never have fun, the Pied Piper got revenge and got all of the children inculding a crippled boy in Hamelin to go with him. The mayor and the adults thought he was bluffing, but it was true. Then he opened part of a mountain which led to a joyland for children with a fun playground and some candy as the Pied Piper and the children and crippled boy lived happily ever after.

==Home Video Release==
#  (VHS)1985
#   (DVD) 2005
#   (DVD) 2006
#   (DVD) 2009

== External links ==
#  

 
 

 
 
 
 
 
 
 
 
 
 
 

 