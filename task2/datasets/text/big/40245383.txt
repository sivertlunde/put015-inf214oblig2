Dehraadun Diary
{{Infobox film
| name           = Dehraadun Diary
| image          = Dehraadun Diary poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Milind Ukey
| producer       = Anita Nandwani & Yatin Nandwani
| writer         = 
| starring       = Adhyayan Suman Ragini Nandwani Rati Agnihotri Rohit Bakshi Vishal Bhonsle Ashwini Kaleskar
| music          = Ripul Sharma
| cinematography = 
| editing        =
| studio         = Swami Samartha Creations & Ashwagandha Entertainments
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Dehraadun Diary is a Hindi thriller film released in January 2013.  This was the debut film for Ragini Nandwani    who made the jump from TV to film industry. The film is based on a real life murder. 

==Cast==
The cast includes  

*Adhyayan Suman
*Ragini Nandwani
*Rati Agnihotri
*Rohit Bakshi
*Vishal Bhonsle
*Ashwini Kalsekar

==Plot==
The story revolves around the daughter of a politician who is in love with the son of a retired IAS officer but her lover is killed by her brother due to their status difference. The struggles of the family who lost their son and that of the lover who has to fight against her own family for justice in a corrupt system forms the rest of the movie. 

==Reception==
The film did not do well in the box office, Times of India rated the movie 1.5 stars.

==References==
 

==External links==
*  

 
 
 


 