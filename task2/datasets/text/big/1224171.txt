The General Died at Dawn
{{Infobox film
| name           = The General Died at Dawn
| image          = Generaldiedatdawn.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Lewis Milestone
| producer       = William LeBaron
| writer         = Charles G. Booth Clifford Odets
| starring       = Gary Cooper   Madeleine Carroll   Akim Tamiroff
| music          = Werner Janssen
| cinematography = Victor Milner
| editing        = Eda Warren
| distributor    = Paramount Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
}}
The General Died at Dawn is a 1936 film that tells the story of a mercenary who meets a beautiful girl while trying to keep arms from getting to a vicious warlord in war-torn China.  The movie was written by Charles G. Booth and Clifford Odets, and directed by Lewis Milestone.
 Dudley Digges.  Director Milestone has a cameo role.
 Best Actor Best Cinematography Best Music, Score. There are several scenes in the film that show startling originality at the time. At one point, the camera focuses on a white door knob, and then dissolves to a white billiard ball to connect disparate scenes. In another scene, two characters have a conversation in which they speculate about the fates of other characters in the drama. The answers to their questions appear in screen segments in the corners of the screen, marking an unusual use of split screen to join narrative.
 Morris Abraham "Two-Gun" Cohen. During the early 1930s, Cohen ran guns for various warlords in mainland China.

This is reported to be the first film to use foam latex appliances.  Makeup artist Charles Gemora applied sponge rubber eyelids for one of the actors.

John Howard Reid called it one of the fifty finest films Hollywood ever made. 

==Cast==
*Gary Cooper as OHara 
*Madeleine Carroll as Judy Perrie
*Akim Tamiroff as General Yang Dudley Digges as Mr. Wu
*Porter Hall as Peter Perrie / Peter Martin
*William Frawley as Brighton
*J.M. Kerrigan as Leach
*Philip Ahn as Oxford
*Lee Tung Foo as Mr. Chen
*Leonid Kinskey as Stewart (shipping line clerk)
*Val Durand as Wong
*Willie Fung as Bartender
*Hans Fuerberg as Yangs Military Advisor
* John OHara as Newspaper Reporter

==References==
 
 Leon Schlesinger Productions. In it, a major tells tall tales about his hunting adventures to a boy who resembles Freddie Bartholomew. The character of the major may have been influenced by Colonel Heeza Liar.

A third-season episode of the TV show, M*A*S*H (TV series)|M*A*S*H, was entitled "The General Flipped at Dawn" (broadcast 9/10/74). In the episode, Harry Morgan appears as Major General Bartford Hamilton Steele, a batty General who is convinced that the 4077th needs to move closer to the front lines, to be near the action. (Morgan formally joined the cast of M*A*S*H in Season Four as the much-saner Colonel Sherman T. Potter.)

==External links==
*  at the Internet Movie Database
*  
*  on Escape (radio program)|Escape: April 16, 1949  
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 