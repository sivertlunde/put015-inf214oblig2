Raintree County (film)
{{Infobox film 
| name           = Raintree County
| image          = Raintree_movieposter.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Edward Dmytryk David Lewis
| screenplay         = Millard Kaufman
| based on       =  
| starring       = Montgomery Clift Elizabeth Taylor Eva Marie Saint Lee Marvin Rod Taylor
| music          = Johnny Green Robert Surtees John D. Dunning
| distributor    = Metro-Goldwyn-Mayer (MGM)
| released       =  
| country        = United States
| runtime        = 182 minutes
| language       = English
| budget         = $5,474,000  . 
| gross          = $9,080,000 
}}

Raintree County is a 1957 American Technicolor melodramatic film set during the American Civil War, directed by Edward Dmytryk.   It stars Montgomery Clift, Elizabeth Taylor, Eva Marie Saint, and Lee Marvin.
 same name by Ross Lockridge, Jr.

==Plot==
In 1859, idealist John Wickliff Shawnessey (Montgomery Clift), a resident of Raintree County, Indiana, is distracted from his high school sweetheart Nell Gaither (Eva Marie Saint) by Susanna Drake (Elizabeth Taylor), a rich New Orleans girl. He has a brief and passionate affair with Susanna while she is visiting. Following her return to the South, she comes back to Indiana to tell Shawnessey she is pregnant with his child. John marries her out of honor and duty, leaving Nell heartbroken.

They move south to live with Susannas family. He learns that Susannas mother went insane and died in a suspicious fire, along with Susannas father and a female slave implied as being the fathers concubine. Susanna suspects that the slave may have been her biological mother. Gradually Susanna appears to be suffering from mental illness. She tells John that she faked pregnancy to trick him into marriage.
 Civil War. John works as a teacher and they have a child, Jimmy, born at the outbreak of the war. In the wars third year, Susanna develops severe paranoia and delusions. She flees Indiana with Jimmy and seeks refuge among her extended family in Georgia.

Shawnessey is determined to find her and recover his son. He enlists in the Union Army in hopes of encountering his wife and child. After fighting in terrible battles, he finds Jimmy at an old plantation and learns that Susanna has been placed in an insane asylum. He is wounded while carrying Jimmy back to Northern lines, and is discharged from the Union Army. Johnny searches for Susanna, finding her kept in terrible conditions at the asylum. He brings her back with him to Raintree County.

After the end of the war and President Abraham Lincolns assassination, Shawnessey considers his future. Nell urges him to run for political office. Recognizing that Nell and John still love each other, Susanna sacrifices herself and deludedly enters the nearby swamp in the middle of the night to find the legendary raintree. Four-year-old Jimmy follows her. The search party eventually finds her body. John and Nell find Jimmy asleep and carry him out of the swamp, failing to see the tall raintree glowing in the sunlight.

==Cast==
* Montgomery Clift as John Wickliff Shawnessey
* Elizabeth Taylor as Susanna Drake
* Eva Marie Saint as Nell Gaither
* Nigel Patrick as Prof. Jerusalem Webster Stiles
* Lee Marvin as Orville Flash Perkins
* Rod Taylor as Garwood B. Jones
* Agnes Moorehead as Ellen Shawnessy
* Walter Abel as T.D. Shawnessy 
* Jarma Lewis as Barbara Drake 
* Tom Drake as Bobby Drake  Rhys Williams as Ezra Gray 
* Russell Collins as Niles Foster 
* DeForest Kelley as Southern officer

==Production==
The movie was a "passion project" for Dore Schary, then head of production at MGM. Rod Taylor actively campaigned for his role in the film.  Raintree County was shot at various locations, including Dunleith and Elms Court antebellum mansions, Frankfort and settings in and near Danville, Kentucky|Danville.

During filming, Montgomery Clift was injured and nearly killed in a serious automobile accident: when feeling tired and suffering from a hangover, he drove into a telephone pole and wrecked his car. He broke his nose, cut his lip, and  fractured his jaw in three places, which had to be wired back together by surgery.    Although he spent weeks in surgery and recovery, he returned to finish the film. Damage to his face was apparent in several scenes filmed after the accident; the left side of his face was partially paralyzed.  His performance in the scenes shot after the accident was also markedly different from those shot before; he began to drink heavily and take a concoction of drugs which made his face more haggard and gave his eyes a furtive look, and affected his attitude and posture.  Edward Dmytryk later confessed in his autobiography Its a Hell of a Life but Not a Bad Living that he found  "a hundred containers" of every kind of drug and "a beautiful leather case fitted with needles and syringes" in Clifts hotel room and once found him so drunk that his cigarette had burned itself out between his fingers.  During filming in Danville, Clifts behavior grew increasingly erratic and bizarre, ordering his steak "blue-rare" (nearly uncooked) and adding masses of butter and pepper and eating it with his fingers, and running naked through the town, which resulted in a policeman being stationed outside his hotel room door to prevent him leaving during the night. 

Elizabeth Taylor also had problems during the production with her period clothing, and on one occasion she collapsed from hyperventilation and was treated with Clifts bottle of Demerol and a syringe, delivered by the doctor. She took over a week to also recover from tachycardia following the incident.  On set she was often late for filming and preoccupied with her romance with Mike Todd, who hired a commercial airliner to personally deliver some expensive presents to her in Danville. 
 Ben Hur. Around the World in 80 Days.  Due to the major delays in filming due to the issues with Clift and Taylor and the extravagant sets, the film was the most expensive US-based film in MGMs history, and Dmytryk never directed for MGM again. Though a success at the box office, it did not recoup its cost. Stephen Vagg, Rod Taylor: An Aussie in Hollywood, Bear Manor Media, 2010 p54  In the credits for the film, the authors name is misspelled as Ross Rockridge Jr.

==Reception==
According to MGM records, the film earned $5,830,000 in North America and $3,250,000 internationally. But because of its high cost, the movie recorded a loss of $484,000.  
 Gone with Time Out called the film an "elephantine bore". 

==Awards and nominations== Best Art Best Costume Best Music, Scoring.   

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 