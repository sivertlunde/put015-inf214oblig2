Sköna Helena
{{Infobox film
| name           = Sköna Helena
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Gustaf Edgren
| producer       = 
| screenplay     = 
| based on       =  Max Hansen Eva Dahlbeck
| music          = Jacques Offenbach
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1951
| runtime        = 
| country        = Sweden
| language       = 
| budget         = 
| gross          = 
}}
Sköna Helena (Beautiful Helen) is a Swedish musical film of 1951 directed by Gustaf Edgren, and based loosely on the story and music of the opéra bouffe La belle Hélène.  It was director Gustaf Edgrens last film and stars Max Hansen and Eva Dahlbeck.

==Background==
Much of the music originates in Jacques Offenbachs opéra bouffe La Belle Hélène (words by Henri Meilhac and Ludovic Halévy) which premiered in Paris in 1864, since when it has been part of the operetta repertory.
 Max Hansen Royal Stockholm Opera for full houses over 150 performances opposite Hjördis Schymberg as Helena.  In 1952 Hansen would record a set of selections from the operetta with Elisabeth Söderström, Arne Andersson and Hasse Funck, conducted by Sune Waldimir on the Musica label (SK 19854-5).
 Stensunds dockyard, while other sets were constructed in and around Sandrews studios at Gärdet in Stockholm.

The film had its premiere on 26 December 1951 at the Royal cinema in Stockholm as well as several other Swedish towns and cities.   accessed 15 January 2014. 
 Smiles of Royal Opera, the Oscarsteatern and the Södra Teatern in Stockholm.

The cast also boasted the prolific film actor Stig Järrel, Elisaveta von Gersdorff Oxenstierna (daughter of an exiled Russian noble), and Per Grundén who went on to an international career (including the Vienna State Opera and Vienna Volksoper) followed by TV and film roles.

Bertil Bokstedt, who later held senior positions at the Royal Opera in Stockholm and accompanied international opera singers such as Jussi Björling and Birgit Nilsson, was the conductor. 

==Synopsis==
Sandwiched between the East and the West, is the neutral, small, carefree kingdom of Arcadia. Its two powerful neighbours vie for world domination. The eastern neighbours ruler Trojanus has recently taken several minor states under his "protection". Prince Paris is shackled and put aboard a Trojan ship as a galley-slave. Trojanus believes that the Arcadian province Thermopylae, with its mountain passes would make a useful route to the west and begins an intrigue to usurp the area peacefully. He has sought the support of Arcadias royalists, who form a treacherous fifth column.

Meanwhile, Arcadia is unaware of all these plans. Under an assumed name King Menelaus amuses himself playing ping-pong with the delightful fashion model Läspia. Unbeknown to the King the plotting Calchas has made a deal with Trojanus ambassador, Hector, for a marriage for Menelaus. The chosen consort is the Trojan beauty queen Helen. When the king is told of his imminent entry into matrimony, he is dismayed; the prospect of a foreign wife by his side and having to abandon Läspia does not appeal to him.

Läspia discovers that her wooer is the king himself, and starts a counter-plot to out-manoeuvre the future queen. When Helena arrives, she takes her place in the chamber of honour. Läspia is supported by the United States of Rome who want to prevent Trojanus getting power over the Arcadian king through marriage. To this end, Secret Service man Hercules is despatched to her aid. Hercules frees Paris, and a meeting is arranged between Paris and Helen. The result is just as Läspia and Hercules had hoped: Helena falls in love with the beautiful singing prince and rejects the king. However, Menelaus has inadvertently consumed a love potion meant for Paris, and is consumed by a violent desire for Helena.

Despite Helenas chilly responses to the love-sick Menelaus, Hector urges her to marry the king. The Trojan ambassador is still hoping for a lease on Thermopylae, and the prospect of a wedding night with beauty queen leads Menelaus on when the marriage contract is drawn up. On the wedding night Läspia enters the bedroom and manages to lock Paris in there. When Menelaus enters, he realizes that his new wife is unfaithful, by counting the number of feet sticking out from the end of the bed. Läspias scheme has succeeded. 

Helena flees to Paris and returns the contract. Menelaus wakes finally to the danger threatening his country and sends the royalist partys fifth column to the East and ten thousand troops to Thermopylae to defend the province. Helena is forced to cancel her flight to put her seal on a royal divorce. Menelaus may now join Läspia and make her the new queen of Arcadia.

== Cast ==
*Eva Dahlbeck - Helen of Troy|Helena, a Trojan princess Max Hansen - Menelaus, King of Arkadia Paris
*Åke Söderblom - Hercules, a Roman Secret Service agent
*Elisaveta von Gersdorff Oxenstierna/Elisaveta - Läspia, a fashion guru
*Stig Järrel - Hector, the Trojan ambassador
*Carl-Gunnar Wingård - Calchas, Prime Minister of Arkadia
*Åke Claesson - Marcellus, Roman ambassador
*John Botvid - Caro, footman of Menelaus
*Sigge Fürst - Waiter in the House of Delights
*Arne Wirén - Achilles, the Arkadian commander
*Keve Hjelm - Lager Myrten, a fifth columnist
*Olav Riégo - Leader of battle
*Viveka Linder - Eris, a fairy
*Ullacarin Rydén – the Goddess Minerva
<!-- 
==Musik i filmen==
*Gudinnorna/balettmusik, kompositör Jacques Offenbach 
*Ljuva frihet (rysk folkvisa), kompositör Offenbach, textbearbetningar Gustaf Edgren och Rune Waldekranz
*Från himmelens höjd 
*Hovmästar-kupletten 
*Ja, ni är så skön 
*Jag är sköna Helena 
*Jag är sköna Helenas man
*Kärlek måste vi ha 
*Paris entrésång
*Ping Pong-kupletten
*Slavinnornas kör
*Säg Venus this list from Swedish Wikipedia needs verifying somehow -->

==Critical assessment==
Following Max Hansens major success in Offenbachs La Belle Hélène in Stockholm, Anders Sandrew laid out a substantial sum to get the operetta star into his studio. However, the way that the plot became a political farce – depicting emerging East-West relations – was not to the taste of Stockholm newspaper critics. On of them (Gunnar Oldin) went so far as to claim that the film had become “a mixture of Offenbach and the Soviet defector Victor Kravchenko (defector)|Kravchenko”. Eva Dahlbecks role was compared to the lead in the 1939 film Ninotchka, and others complained about the excessive number of "...gags, anachronisms and whimsy.   accessed 15 January 2014. 

Communist MP and editor of the New Day paper, Gustav Johansson claimed that "old Offenbach has been mobilized in Americas cold war against the peace movement". The female critic Lill in the Svenska Dagbladet was happier with the films mixture of operetta action and political satire, with its many witty and elegant moments and a seamless flow of song numbers. 

==References==
 

 
 
 
 
 
 
 