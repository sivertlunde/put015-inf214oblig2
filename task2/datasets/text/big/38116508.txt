Singh vs Kaur
 
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Singh vs Kaur
| image          =Singh vs Kaur.jpg
| alt            = 
| caption        =
| director       = Navaniat Singh
| producer       = D. Ramanaidu
| writer         = Dheeraj Rattan
| screenplay     = Dheeraj Rattan
| story          = Dheeraj Rattan
| starring       = Gippy Grewal Surveen Chawla Binnu Dhillon B N Sharma Japji Khaira Avtar Gill Sean Bindra  Taranvir Dhanoa
| music          = Jatinder Shah
| cinematography = Jitan Harmeet Singh
| editing        = Manish More
| studio         = Suresh Productions Sippy Grewal Productions Gurfateh Films 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Punjabi
| budget         =
| gross          = 
}}                                                                                                                                                        Singh vs Kaur is a Punjabi romantic comedy starring Gippy Grewal, Surveen Chawla, Japji Khaira and Binnu Dhillon in lead roles.Singh vs Kaur was released on 15 February 2013.   
 	
Gippy Grewal will be seen in a turbaned avatar for the very first time in his entire film career.
The film is also the first Punjabi movie that has been produced by a multi-lingual producer, D. Ramanaidu.
Dheeraj Rattan copied the script of this film from Bollywood Movie Singh is Kinng. A remake of this movie in Bengali named Romeo vs Juliet starring Ankush Hazra and Mahiya Mahi.

==Plot==
Nihal Singh (Gippy Grewal), living in the village of Nalun tattebha is considered a burden on the head of villagers and they all decide to throw him out of the village on the advice of Sarpanch (Avtar Gill). Nihals mother is a widow who lost her husband when Nihal was only a few years old. Meanwhile, Nihals best friend Taari (Binnu Dhillon), who is addicted to watching girls with his associates on the Internet and print their photos out, plans an idea to clear up the whole situation. Taari says that to solve the situation he should marry Sarpanchs niece Sapna, an overweight girl, which he initially goes through with before regretting his actions. A comic situation arises when Sapna tells her Uncle (Sarpanch) that she loves Nihal and will die if she doesnt marry him. Sarpanch, with his niece, presents the proposal to his mother. Meanwhile, Nihal says that he doesnt love her and was just joking. Taari again uses his head and prints out the photo of a girl named Jasneet (Surveen Chawla), but Nihal doesnt understand what to do to cancel the marriage proposal. He eventually decides to present a photo of Jasneet, unknown to him at first sight, saying that he loves her. The villagers are unaware of the situation; Nihals Chachi, who is very jealous of him, asks Nihal if he is actually in love, then asks where she lives. Nihal says that she lives in Canada. Sarpanch tells Nihal to bring the girl in one month to prove his declaration true otherwise he will have no option to marry him to his overweight niece.
Nihal feels helpless, but with Taaris help he travels to Canada to find the girl. Taari and Geeta arranges with his uncles house Chacha Dhind (B N Sharma) and his son (Sean Bindra). Finally, he finds Jasneet, who it is revealed has been attacked several times. Jasneet tells Nihal to become her bodyguard. Nihal cant think of a reason to refuse and so accepts. He defends her from attempts on her life, fighting with her rivals. Eventually, he and Jasneet become close friends, but Nihal reveals that she is engaged to her boyfriend (Rohit Khurana) who lives in America. Nihal feels sad after hearing this but soon thinks of an idea: he asks Jasneet to stay with him in Punjab. It is known that the attacks will continue until she turns 21 years old, so Jasneet, having no idea how to prevent the attacks, goes with Nihal to Punjab to prevent any further incidents.
Taari and geeta kept on showing the photos to the whole village. Jasneet is unaware that she has been lied to by Nihal, but Nihals mother feels proud after hearing that her son is married. Jasneet soon learns and leaves out of hurt, but then thinks of Nihals mother and doesnt want to break her heart, so she returns. Upon her return, she and Nihal begin to fall in love with each other. Another comic situation arises when Nihal takes Jasneet home to Nabha and unintentionally calls his brother a Saala (brother-in-law) out of frustration. His girlfriends brother, mischievous in nature, tells the whole family that his sister is married although the other villagers are unaware of this ever happening. Nihal curses himself for using the word again in his life to someone. Meantime, Jasneets NRI (Non-resident Indian and person of Indian origin|non-resident Indian) boyfriend comes to take her away, but Jasneet tells him that she now loves Nihal, shocking him. In the meantime, Nihal learns that Jasneets NRI boyfriend had a previous relationship with Nihals friend before breaking up with her. Nihal calls Jasneet to tell her that her NRI friend (Rohit Khurana) is surely a fraud and blackmailer, warning her that he is responsible for all the previous attacks on her after gaining access to her property. Jasneet is shocked and stops the car while travelling with her boyfriend. He then shows his true colors, following her with his men and confessing that he wanted her to stay alive and did not want to kill her; his intention was for Jasneet to turn 21 so that he could marry her and blame her uncle. Nihal soon arrives to save Jasneet, getting rid of her boyfriend and his associates. Jasneet and Nihal then live happily after all the difficulties.

==Cast==

* Gippy Grewal as Nihal Singh
* Surveen Chawla as Jasneet Kaur
* Binnu Dhillon as Taari B N Sharma as Dhindsa
* Avtar Gill
* Tej Hundal as Angrej Singh (cameo)
* Sean Bindra as Dhindsas Son
* Sukhpreet Singh Kalkat as Rupinader Singh
* Japji Khaira as Simrat

==Reception==
Rick of Punjabiportal.com praised the movie but pointed out certain technical errors as well. He praised Gippy Grewal and Surveen Chawla for their spontaneous acting but pointed out the abrupt editing and too much attention given to style in action sequences.   

The film has received mostly positive reviews. According to Ballewood.in, "Singh vs Kaur is a slick action comedy that would set new benchmarks for the Punjabi film industry. A full-blown masala entertainer that establishes Gippy Grewal as the first real action hero of Punjab." Gippy Grewals performance has been especially appreciated. "It is an entertainer where you have songs, dance, entertainment, power packed dialogues and full power South Indian style action."

According to box office reports, Singh vs Kaur has scored very well in Punjab after also having a strong start.
It can be considered another blockbuster in Gippys career.    It had the third highest opening week collections for a Punjabi film at the time of release in Punjab before Gippys next film lucky di unlucky story came and beat it and took first place. The opening week collections for this film were Rs 113.5&nbsp;million. 

==Remake==
The film has been remade in Bengali named Romeo vs Juliet. It was an Indo-Bangladesh joint project. 
Rama Naidu was quite impressed with Gippy Grewal, Surveen Chawla and Binnu Dhillon starrer Punjabi movie Singh vs Kaur to be remade in Telugu also. He has selected his grand son Naga Chaitanya as hero for this movie. There is chance to Tamanna to play female lead role. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 