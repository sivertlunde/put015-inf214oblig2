Chick (1936 film)
{{Infobox film
| name =  Chick
| image =
| image_size =
| caption =
| director = Michael Hankinson
| producer = Jack Raymond
| writer = Edgar Wallace (novel)   Daniel Wheddon   D.B. Wyndham-Lewis   Gerard Fairlie   Cyril Gardner   Irving Leroy
| starring = Sydney Howard   Betty Ann Davies   Fred Conyngham   Cecil Humphreys
| music = Percival Mackey
| cinematography = Francis Carver
| editing = John E. Morris
| studio = British & Dominions Film Corporation
| distributor = United Artists 
| released = September 1936
| runtime = 72 minutes
| country = United Kingdom English 
| budget =
| gross =
}} comedy crime novel of 1928 silent film. The film was made at Elstree Studios.  The hall porter at an Oxbridge College inherits an Earldom and enjoys a series of adventures.

==Cast==
* Sydney Howard as Chick Beane 
* Betty Ann Davies as Peggy 
* Fred Conyngham as  Sir Anthony Monsard  
* Cecil Humphreys as Sturgis  
* Mae Bacon as Gert  
* Wallace Geoffrey as Latimer  
* Aubrey Mather as The Dean  
* Arthur Chesney as Lord Frensham 
* Edmund DAlby as Rennie  
* Robert Nainby as Mr. Beane  
* Merle Tottenham as Maid  
* Aubrey Fitzgerald as Banks  
* Fred Rains as Warden  
* Eric Micklewood as Undergraduate  
* Joe Monkhouse as Mason   Richard Morris as Clerk 
* Aubrey Pollock as Lawyer

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 