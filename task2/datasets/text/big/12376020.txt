Superdad
{{Infobox Film
| name = Superdad
| image =Poster of the movie Superdad.jpg
| director = Vincent McEveety Bill Anderson
| writer = Joseph L. McEveety Harlan Ware Joe Flynn Kathleen Cody Buddy Baker Andrew Jackson
| editing = Ray de Leuw Walt Disney Productions Buena Vista Distribution
| released = December 14, 1973
| runtime = 96 minutes
| country = United States
| language = English
}}
 Walt Disney Joe Flynn, Kathleen Cody. The film marks the on-screen debut of Bruno Kirby.
 Vietnam era, but it was a critical and commercial failure.

==Plot== Kathleen Cody) from her childhood friends, whom he believes have no ambition.  He especially disapproves of her boyfriend, Bart (Kurt Russell).  Initially he makes a few attempts to bridge the generation gap, but he fails.  Late in the summer, Wendy receives a letter informing her that shes won a full scholarship to her parents alma mater, Huttington College.  Unbeknownst to her, the letter is fake; her father has paid the first years tuition himself, and had a friend at the college send the letter to her.  He did this so Wendy would not attend City College with Bart and her other friends.

Charlie later visits Wendy at Huttington, and discovers that the college has changed considerably since he attended there.  Wendy later discovers his plot, and joins the campus counterculture as a way of getting even.  She inadvertently becomes engaged to a hippie artist named Klutch.  Charlie attempts to intervene on her behalf, and ends up in a fistfight with Klutch.  Fortunately, Wendys boyfriend Bart comes to the rescue.  At this point, Charlie learns that Bart had turned down a scholarship to Huttingdon so he could be near Wendy.  The movie ends with Wendys marriage to Bart.

==Cast==
* Bob Crane as Charlie McCready
* Barbara Rush as Sue McCready
* Kurt Russell as Bart Joe Flynn as Cyrus Hershberger Kathleen Cody as Wendy McCready
* Joby Baker as Klutch
* Dick Van Patten as Ira Kushaw
* Bruno Kirby as Stanley
* Judith Lowry as Mother Barlow
* Ivor Francis as Dr. Skinner on TV
* Jonathan Daly as Rev. Griffith
* Naomi Stevens as Mrs. Levin
* Nicholas Hammond as Roger Rhinehurst Jack Manning as Justice of the Peace
* Jim Wakefield as House Manager

==Home viewing== The Wonderful World of Disney.  To date it has not been widely released on DVD (made available exclusively to members of the Disney Movie Club in that format), but it did have a limited release on VHS and BETA. It was also recently released on iTunes.

==In other media==
Superdad was featured in the biographical film Auto Focus, with Bob Crane (Greg Kinnear) seeing his role as the leading man in this Disney film as a way to revive his career following the retirement of his hit series Hogans Heroes. Footage of the film is shown where Crane is on water skis (Kinnear in a re-shoot of that scene), along with a voice-over of how Superdad sat on the shelves for a year before flopping at the box office.

==External links==
*  
*  
*  

 

 
 
 
 
 
 