Pal Joey (film)
{{Infobox film
| name           = Pal Joey
| image          = Poster - Pal Joey 01.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical poster
| director       = George Sidney
| producer       = Fred Kohlmar
| writer         = 
| screenplay     = Dorothy Kingsley
| story          = 
| based on       =  
| narrator       = 
| starring       = Rita Hayworth Frank Sinatra Kim Novak
| music          = Richard Rodgers Lorenz Hart Morris Stoloff (supervision) Nelson Riddle George Duning (arrangements) Arthur Morton (orchestrations)
| cinematography = Harold Lipstein
| editing        = Viola Lawrence Jerome Thoms
| studio         = 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 109 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = $4.7 million (US) 
}}
 musical play Hermes Pan. Nelson Riddle handled the musical arrangements for the Rodgers and Hart standards "The Lady is a Tramp", "I Didnt Know What Time It Was," "I Could Write a Book" and "Theres A Small Hotel."

The film is considered by many critics to be the definitive Sinatra vehicle.  Sinatra won the Golden Globe Award for Best Actor - Motion Picture Musical or Comedy for his role as the wise-cracking, hard-bitten Joey Evans. Along with its strong box office success, Pal Joey also earned four Academy Award nominations and one Golden Globe nomination. 
  Cover Girl in 1944, but her tenure was soon to end, in 1959 with Gary Cooper in They Came to Cordura.

==Plot summary==
 )]]
The setting is San Francisco; Joey Evans (Frank Sinatra) is a second-rate singer, a heel known for his womanizing ways (calling women "mice"), but charming and funny. When Joey meets Linda English (Kim Novak), a naive chorus girl, he has stirrings of real feelings. However, that does not stop him from romancing a former flame and ex-stripper (Joey says, "She used to be Vera...with the Vanishing Veils"), now society matron Vera Prentice-Simpson (Rita Hayworth), a wealthy, willful, and lonely widow, in order to convince her to finance his dream, "Chez Joey", a night club of his own. 

Soon Joey is involved with Vera, each using the other for his/her own somewhat selfish purposes. But Joeys feelings for Linda are growing. Ultimately, Vera jealously demands that Joey fire Linda. When Joey refuses ("Nobody owns Joey but Joey"), Vera closes down "Chez Joey". Linda visits Vera and agrees to quit in an attempt to keep the club open. Vera then agrees to open the club, and even offers to marry Joey, but Joey rejects Vera. As Joey is leaving for Sacramento, Linda runs after him, offering to go wherever he is headed. After half-hearted refusals, Joey gives in and they walk away together, united.

==Cast==
* Rita Hayworth as Vera Prentice-Simpson
* Frank Sinatra as "Pal" Joey Evans 
* Kim Novak as Linda "The Mouse" English
* Barbara Nichols as Gladys 
* Bobby Sherwood as Ned Galvin
* Judy Dan as Hat Check Girl (uncredited)
* Hank Henry as Joe Muggins

==Notable changes== 
The happy ending of the film contrasts with that of the stage musical, where Joey is left alone at the end.

The transformation of Joey into a "nice guy" departed from the stage musical, where Joeys character was notable for being the anti-hero. 
 
The film varies from the stage musical in several other key points: the setting was changed from   drummer to mime the bumps and grinds, the extra playing the drums is disconcertingly switched with a professional musician in a jump cut).

==Song list==
Of the original 14 Rodgers and Hart songs, eight remained, but with two as instrumental background, and four songs were added from other shows. 

#Pal Joey: Main Title  
#"That Terrific Rainbow" - chorus girls and Linda English  Too Many Girls) - Joey Evans
#"Do It the Hard Way" - orchestra and chorus girls 
#"Great Big Town" - Joey Evans and chorus girls 
#"Theres a Small Hotel" (introduced in the 1936 musical On Your Toes) - Joey Evans
#"Zip" - Vera Simpson
#"I Could Write a Book" - Joey Evans and Linda English 
#"The Lady Is a Tramp" (introduced in the 1937 musical Babes in Arms) - Joey Evans
#"Bewitched, Bothered and Bewildered" - Vera Simpson 
#"Plant You Now, Dig You Later" - orchestra 
#"My Funny Valentine" (introduced in the 1937 musical Babes in Arms) - Linda English
#"You Mustnt Kick It Around" - orchestra 
#Strip Number - "I Could Write a Book" -Linda English
#Dream Sequence and Finale: "What Do I Care for a Dame"/"Bewitched, Bothered and Bewildered"/"I Could Write a Book" - Joey Evans

==Soundtrack==
The recordings on the soundtrack album featuring Sinatra only are not the same songs that appeared in the film. "The Lady Is a Tramp" is an outtake from Sinatras 1957 album A Swingin Affair!, while the others were recorded in mono only at Capitol Studios. The Sinatra songs as they appear in the film as well as those performed by Rita Hayworth and Kim Novak (both were dubbed), Jo Ann Greer (Hayworth) and Trudi Erwin (Novak) were recorded at Columbia Pictures studios in true stereo.

===Chart positions===
{| class="wikitable sortable"
|-
!Chart
!Year
!Peak position
|- UK Albums Chart  1958
|align="left"|1
|-
|}

 
 
 
 
 

==Critical reception and box office==
Opening to positive reviews on October 25, 1957, Pal Joey was an instant success with critics and the general public alike. The Variety (magazine)|Variety review summarized: "Pal Joey is a strong, funny entertainment. Dorothy Kingsleys screenplay, from John OHaras book, is skillful rewriting, with colorful characters and solid story built around the Richard Rodgers and Lorenz Hart songs. Total of 14 tunes are intertwined with the plot, 10 of them being reprised from the original. Others by the same team of cleffers are I Didnt Know What Time It Was, The Lady Is a Tramp, Theres a Small Hotel and Funny Valentine." 

The New York Times commented, "This is largely Mr. Sinatras show...he projects a distinctly bouncy likeable personality into an unusual role. And his rendition of the top tunes, notably "The Lady Is a Tramp" and "Small Hotel," gives added lustre to these indestructible standards." {{cite web|url=http://movies.nytimes.com/movie/review?res=990CE0DD1030E03ABC4051DFB667838C649EDE&scp=3&sq=%22Pal%20Joey%22&st=cse|title=Movie Review -
  Pal Joey - Screen: Pal Joey Back on Broadway; Sinatra Is Starred in Film of Hit Show - NYTimes.com|publisher=|accessdate=13 September 2014}} 

With box office receipts of $4.7 million, Pal Joey was ranked by Variety as one of the ten highest earning films of 1957.

==Awards and nominations==
:Wins in bold
Academy Awards      

* Best Art Direction/Set Decoration: Walter Holscher, William Kiernan and Louis Diage
* Best Costume Design: Jean Louis
* Best Film Editing: Viola Lawrence and Jerome Thoms
* Best Sound, Recording: Columbia Studio Sound Department, John P. Livadary, Sound Director

Golden Globes

* Best Film, Musical or Comedy
* Best Actor, Musical or Comedy: Frank Sinatra

Writers Guild of America

* Best Written American Musical

==Background==
Gene Kelly had starred as Joey Evans on Broadway in Pal Joey. When Columbia Pictures head Harry Cohn bought the rights for the motion picture, he wanted Gene Kelly, who was contracted to MGM. Cohn saw Rita Hayworth as Linda English. Billy Wilder expressed interest in Pal Joey as a picture for Marlon Brando (Joey), Mae West (Vera) and Rita Hayworth (Linda), Columbia Pictures passed. Pal Joey would be the last film with Rita Hayworth at Columbia Studios after a long career. Columbia began to give the star treatment to Kim Novak, after her success in Picnic (1955 film)|Picnic and Pal Joey.

==References==
 

==Further reading==
*  

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 