The Father of My Children
 
{{Infobox film
| name           = The Father of My Children
| image          = The Father of My Children.jpg
| caption        = Film poster
| director       = Mia Hansen-Løve Philippe Martin David Thion
| writer         = Mia Hansen-Løve
| starring       = Michaël Abiteboul
| music          = 
| cinematography = Pascal Auffray
| editing        = Marion Monnier
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = France
| language       = French
| budget         = 
}}
The Father of My Children ( ) is a 2009 French drama film directed by Mia Hansen-Løve. It won the Jury Special Prize in the Un Certain Regard section at the 2009 Cannes Film Festival.    It is based in part on the life of the late Humbert Balsan. 

==Cast==
* Michaël Abiteboul
* Chiara Caselli
* Alice de Lencquesaing
* Louis-Do de Lencquesaing
* Manelle Driss
* Sandrine Dumas
* Eric Elmosnino
* Dominique Frot
* Alice Gautier
* Valerie Lang
* Harald Leander as Swedish assistant
* Igor Hansen Love
* Antoine Mathieu
* Patrick Mimoun
* Elsa Pharaon
* Olivia Ross

==References==
 

==External links==
;Official
*    
*    
;Database
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 