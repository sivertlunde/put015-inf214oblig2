Cape No. 7
{{Infobox film
| name           = Cape No. 7 海角七號
| image          = Cape No.7.jpg
| caption        = 
| producer       = Jimmy Huang Wei Te-Sheng
| director       = Wei Te-Sheng
| writer         = Wei Te-Sheng
| starring       = Van Fan Chie Tanaka Kousuke Atari Rachel Liang Johnny C.J. Lin Ming Hsiung Ying Wei-Min Ma Nien-hsien Joanne Yang Bjanav Zenror Ju-Lung Ma Hsiao-Lan Pei Chang Kuei Shino Lin Pei-Chen Lee Chin-Yen Chang
| music          = Chi-Yi Lo Fred Lu
| cinematography = Ting-chang Chin
| editing        =  Buena Vista Astro Entertainment Sdn Bhd (Malaysia)
| released       =  
| runtime        = 129 minutes
| country        = Taiwan Japanese
| budget         = NT$ 50 million  (US$1.5 million)
| gross          = Domestic (Taiwan)   7.62 million (US$1 million)
}} romance comedic Taiwanese and Mandarin Chinese with significant lines in Japanese language|Japanese. Before its commercial release, the film was world premiered on June 20, 2008 at the 2008 Taipei Film Festival as the opening film.  The film later won 3 awards in this festival.

Prior to this film, the two leading actors Van Fan and Chie Tanaka only had minor acting experience while some of the supporting roles were filled by non-actors. Even without a strong promotional campaign, the film had become so popular in Taiwan that on November 1, 2008 it became the second highest grossing film in the countrys cinematic history, behind James Camerons Titanic (1997 film)|Titanic.     The film has grossed over US$13,804,531 since its release. 
 81st Academy Awards for Best Foreign Language Film, although it did not secure a nomination. 

==Plot== Japanese era Hengchun falls in love with a local girl with the Japanese name Kojima Tomoko (Liang). After the Surrender of Japan, the teacher is forced to return home as Taiwan was placed under the administrative control of the Republic of China. On his trip home, he pens seven love letters to express his regret for leaving Kojima Tomoko, who originally planned to elope with him to Japan.

More than 60 years later, Aga (Fan), a struggling young rock band singer leaves Taipei to return to his hometown of Hengchun. There, his step father (Ma), the Town Council Representative, arranges a position for him as a postman, replacing the aging Old Mao (C. Lin), on leave after a motorcycle accident broke his leg. One day, Aga comes across an undeliverable piece of mail that was supposed to be returned to the sender in Japan; the daughter of the now deceased Japanese teacher had decided to mail the unsent love letters to Taiwan after discovering them. Aga unlawfully keeps and opens the package to discover its contents, but the old Japanese-style address Cape No. 7, Hengchun, Pingtung|Kōshun District, Takao Prefecture can no longer be found.

Meantime a local resort hotel in nearby Kenting National Park is organizing a beach concert featuring Japanese pop singer Kousuke Atari, but Agas step father makes use of his official position to insist that the opening band be composed of locals. Tomoko (Tanaka), an over-the-hill Mandarin-speaking Japanese fashion model dispatched to Hengchun, is assigned the difficult task of managing this hastily assembled band, led by Aga along with six other locals of rather particular backgrounds. After a frustrating trial period, Aga and Tomoko unexpectedly begin a relationship. With some assistance from hotel maid Mingchu (S. Lin), who is revealed to be Kojima Tomokos granddaughter, Tomoko helps Aga find the rightful recipient of the seven love letters. Tomoko then tells Aga that she plans on returning to Japan after the concert because of a job offer. After returning the seven love letters, a heartbroken but determined Aga returns to the beach resort and performs a highly successful concert with his local band alongside Kousuke Atari while Kojima Tomoko reads the letters.

==Cast==
{| class="wikitable" style="text-align: left"
|-
!width="120"|Actor
!width="240"|Role
!width="500"|Note
|-
| Van Fan
| Aga (阿嘉)
| The bands lead singer. Hengchun native who recently returned to the town after failing to find success in Taipei.
|-
| Chie Tanaka
| Tomoko (友子)
| Japanese model and agent assigned to organize the local band.
|-
| Kousuke Atari
| Japanese teacher in flashback scenes/himself in the present.
| He is an actual pop star in real life
|- 
| Rachel Liang ( )
| Kojima Tomoko (小島友子) in flashback scenes.
| Was left behind by her lover.
|- 
| Johnny C.J. Lin ( )
| Old Mao (茂伯) bassist and tambourineist in the band, close to 80 years old. Proud of having his talents referred to by the yueqin as a "national treasure" and desperate to perform in any capacity possible. Capable of speaking Japanese.
|- 
| Ming Hsiung ( )
| Rauma (勞馬) Rukai tribe origin; guitarist in the band. Abandoned by his wife. Left the Taiwan police special forces for his wife before she abandoned him.
|- 
| Ying Wei-Min ( )
| Frog (水蛙)
| Mechanic in a local motorcycle shop; drummer of the band. Infatuated with his boss wife.
|- 
| Ma Nien-hsien ( )
| Malasun (馬拉桑) Hakka origin Checheng Township; bassist in the band.
|- 
| Joanne Yang ( )
| Dada (大大)
| 10-year-old girl who plays piano in local church; keyboardist in the band.
|- 
| Bjanav Zenror ( )
| Olalan (歐拉朗)
| Raumas father; policeman; former bassist in the band.
|- 
| Ju-Lung Ma ( )
| Hong Kuo-jung (洪國榮),Town Council Representative
| Agas step father; insists on allowing local talent to perform in the concert.
|- 
| Hsiao-Lan Pei ( )
| Agas mother.
| 
|- 
| Chang Kuei ( )
| Hotel Manager
| Under pressure from Agas stepfather, was forced to allow the band formed by locals to perform in the concert.
|- 
| Shino Lin ( )
| Lin Mingchu (林明珠)
| Hotel maid; capable of speaking Japanese. Kojima Tomokos granddaughter and Dadas mother.
|- 
| Pei-Chen Lee ( )
| Motorcycle shop owners wife triplet sons. Frogs employers wife and infatuation.
|- 
| Chin-Yen Chang ( )
| Meiling (美玲)
| Hotel receptionist. Originally had disputes but later developed brief relationship with Malasun. 
|- 
| Yukihiko Kageyama ( )
| Narrator of the teachers love letters
| Voice only
|}

==Filming== epic film Seediq Bale, which had problems securing financial interest.   

By the end of 2006 Wei had finished Cape No. 7s script. He was subsequently awarded NT$ 5 million for winning the "Domestic Film Fund" from Taiwans  , Hakka people|Hakka, Mainland Chinese|Mainlanders, Taiwanese aborigines, and international tourists; the weather variation between the tropical southern Taiwan and the snow-laden Japan. Wei believed that such a setting would provide the ideal backdrop for the "harmony in diversity" theme of Cape No. 7. 
 Checheng and Manjhou Townships, Fangliao Township Jiadong Township of Pingtung County, Kaohsiung International Airport, Ximending and Wenshan District in Taipei.   The final scene presenting the Japanese teacher (Kousuke Atari) and other Japanese people leaving Taiwan by ship was filmed in an abandoned brewery in Taichung with more than 500 Extra (actor)|extras.  Before filming, Chie Tanaka had been staying in Taiwan for 15 months to study Chinese so she did not have major problems dealing with the Chinese lines in the film. Kousuke Atari played himself and as the 1940s Japanese teacher in this film.

As the production went over budget, Wei had problems securing additional capital; he subsequently refinanced his home and forced his family NT$ 30 million (approx. $900,000 USD) into debt before release. Cast members Pei-Chen Lee and Chin-Yen Chang revealed that during filming Wei could barely afford the film rolls and lodging for the crew.   Wei later said this films zealous reception should help him manage his debts. 
 Buena Vista International, which made 50 copies of the film and distributed them nationally in August. 
{{cite journal
 |author=Teng Sue-feng
 |others=tr. by Jonathan Barnard
 |title=Taiwans Film Industry after Cape No. 7
 |journal=Taiwan Panorama
 |date=February 2009
 |url=http://www.taiwan-panorama.com/en/show_issue.php?id=200929802030e.txt&distype=text
 |quote=He observed that the films that hit the screens before the end of summer vacation in 2007 were all "safe bets" at the box office.
}}
 

The films 133 minute theatrical version was edited from the first cut of 160 minutes. Aga and Tomokos love scene was shortened, and some of their dialog before the love scene was cut as well.  The final version was shortened in Mainland China after the government film agency snipped out over half an hour of the film for political purposes. Nevertheless, rampant piracy in Mainland China saw the unedited version mass distributed via street stalls and the internet.

The films marketing is combined with extensive merchandise, including original soundtrack CD, books, novelization, accessories, tours and musical concerts. The film contributed tourism for Hengchun, and especially boosted Hotel Chateau. A televised concert was held with actors performing some numbers in the movie.

==Reception==

===Taiwan===
Although the film only attracted moderate box-office success during its first weeks of release, it was helped by strong word of mouth buzz, mainly in blogs and on the Professional Technology Temple (PTT) Bulletin Board System, eventually leading to its record-breaking box-office performance.  
 The Dark Knight. 

{| class="wikitable" style="text-align: left"
|- Date
!width="200"|Accumulated box-office in Taipei (NT$) Milestone
|- Aug 29, 2008
|5.66 million
|
|- Sep 11, 2008
|28.18 million surpassed Secret (2007 film)|Secret to become the top-selling Taiwan-made film in this islands cinematic history 
|- Sep 18, 2008
|51.99 million
|
|- Sep 27, 2008 109 million surpassed The The Dark Knights 109-million accumulated Taipei sales record 
|- Oct 4, 2008 151 million surpassed Lust, Caution (film)|Lust, Cautions 136-million accumulated Taipei sales record 
|- Oct 6, 2008 164 million surpassed Police Story 3s 156-million accumulated Taipei sales record to become the top-selling mandarin-language film in Taiwan history  
|- Oct 14, 2008 201 million surpassed  s 200-million accumulated Taipei sales record to become the 4th top-selling film in Taiwan history  
|- Oct 19, 2008 215 million surpassed  s 213-million accumulated Taipei sales record to become the 3rd top-selling film in Taiwan history 
|- Nov 1, 2008
|225.7 million surpassed Jurassic Park (film)|Jurassic Parks 225.2-million accumulated Taipei sales record to become the 2nd top-selling film in Taiwan history, only behind James Camerons Titanic (1997 film)|Titanics 387.8-million 
|- Dec 12, 2008 232 million Final Taipei box-office (530 million total for the entire country) 
|}
Critics attribute the films box office success to its honest depiction of the rural southern Taiwan;  the strong emotional resonance among older viewers;  the humorous tone, optimistic characters, and musical performances.  

===International===
The film gathered widespread attention at the 2008  , both subsidiaries of Malaysian pay-TV group  .   The film made its United States premiere on October 10, 2008 in Honolulu at the Hawaii International Film Festival, where it won the Halekulani Golden Orchid Award for Best Narrative.  Its premiere in the Continental United States was on December 12, 2008 in Los Angeles. 

===Profit Sharing===
Teng Sue-feng used the film to discuss how film profit is shared in Taiwan. Teng estimates the revenue from the movie to be NTD 520 million, and the production cost to be NTD 50 million. After the deducting the production costs, 60% of the profit goes to movie theaters, 10% to the distributor. The director gets 10%, which is about NT$140 million.   

==Soundtrack==
Cape No. 7s soundtrack was released on October 24, 2008 by the local Forward Music Corporation, Van Fans then agency.

# "The First Letter: "Tomoko, Are You Still Waiting for Me?" (Yukihiko Kageyama)
# "Dont Wanna" (Van Fan)
# "The Second Letter: Destiny Is this Eras Sin" (Yukihiko Kageyama)
# "Love You to Death" (Joanne Yang, etc.)
# "The Third Letter: Tomoko, I Fell in Love with You at that Moment" (Yukihiko Kageyama)
# "Where to Go" (Bjanav Zenror)
# "The Fourth Letter: Why the Sea Breeze Always Brings Weep" (Yukihiko Kageyama)
# "To Daughter" (Patricia Ho)
# "The Fifth Letter: Tomoko, I Really Miss You: Ah! Rainbow!" (Yukihiko Kageyama)
# "As Happy as Can Be" (live version from the film) (Van Fan, Johnny C.J. Lin, Ming Hsiung, Ying Wei-Min, Ma Nien-hsien, Joanne Yang)
# "The Sixth Letter: I Write My Shame into the Last Letter" (Yukihiko Kageyama)
# "South of Border" (Van Fan)
# "Heidenröslein" (Van Fan, Kousuke Atari, Chie Tanaka)
# "The Seventh and Last Letter" (Van Fan)
# "1945" (orchestral version)

For an unspecified reason this soundtrack does not include the films ending theme, "Beautiful Scenery" sung by Rachel Liang. This omission gave some fans reason to boycott this soundtrack. 

==DVD/BD release==
On November 14, 2008, Cape No. 7s distributors announced that the films official DVD/BD release date will be December 19, 2008. On November 30, 2008 the number of copies ordered in advance had reached 23,000, far surpassing the runner-up record holder in Taiwans cinematic history, The Lion Kings 12,000 reservations in 1995. 

==Awards==

===Win===
{| class="wikitable" style="text-align: left"
|-
!Film Festival
!Awards/Recipients
|-
| 2008 Taipei Film Festival
| Best Cinematography (Ting-chang Chin), Taipei Million Grand Award, Best Audience Award 
|-
| 2008 Asian Marine Film Festival
| Grand Prize 
|-
| 2008 Hawaii International Film Festival
| Halekulani Golden Orchid Award for Best Narrative 
|-
| 2008 Kuala Lumpur International Film Festival
| Best Cinematography (Ting-chang Chin)   
|-
| 2008 Golden Horse Award
| Best Supporting Actor (Ju-Lung Ma), Best Original Film Score (Fred Lu, Lo Chi-Yi), Best Original Film Song (Matthew Yen, Tseng Chih-Hao, Van Fan), The Audience Choice Award, The Outstanding Taiwanese Film of the Year, The Outstanding Taiwanese Filmmaker of the Year (Wei Te-Sheng)   
|-
| 3rd Asian Film Awards
| The Edward Yang New Talent Award(Wei Te-sheng) 
|-
| 9th Chinese Film Media Award
| The Juries Special Award 
|-
| 2009 Shanghai Film Critics Awards
| Film of Merit 
|}

===Nominated===
{| class="wikitable" style="text-align: left"
|-
!Film Festival
!Awards/Nominees
|-
| 2008 Kuala Lumpur International Film Festival
| Best Director (Wei Te-Sheng), Best Asian Film 
|-
| 2008 Golden Horse Award
| Best Feature Film, Best Director(Wei Te-Sheng), Best New Performer(Chie Tanaka, Johnny C.J. Lin), Best Cinematography(Ting-chang Chin), Best Sound Effect(Tu Duu-Chi) 
|-
| 2008 Festival International du film asiatique de Vesoul
| Asian Feature Film
|-
| 28th Hong Kong Film Awards
| Best Asian Film
|}

==Controversies==
*The United Daily News published an editorial criticizing the film as "marred by colonial thoughts during the Japanese era" on September 25, 2008. 

*At the 2008 Pusan Festival, a deputy director of Taiwans Government Information Office was barred from attending the closing ceremony due to political pressure from the Peoples Republic of China. Information about the film was also pulled from the festivals website. 
 National Police Agency, was asked about his thoughts after seeing Cape No. 7. He responded that in two scenes he felt the film degraded the image of Taiwans police force, and behavior such as Rauma beating Aga certainly would not be allowed in real Taiwan society. 

*During his visit to Taiwan in November 2008,   slang, foul words, and scenes such as Old Mao speaking Japanese were cut. 

==References==
 

==External links==
*  
*  

 
 
|-
! colspan="3" style="background: #DAA520;" | Golden Horse Award
|-
{{succession box
| title = The Outstanding Taiwanese Film of the Year
| years = 2008
| before=  Secret (2007 film)|Secret 
| after = TBD
}}
 

 
 
 
 
 
 
 
 