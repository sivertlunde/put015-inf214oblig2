Street Fighter (1994 film)
 
 
 
{{Infobox film
| name           = Street Fighter
| image          = StreetFighterMoviePoster.jpg
| image_size     = 215px
| alt            = The background is filled with a big screen showing the face of a man wearing a blue beret. In front of the screen is a man, dressed in red and wearing a cape, standing on a platform, with his arms raised up.
| caption        = Theatrical release poster
| director       = Steven E. de Souza
| producer       = {{Plain list |
*Edward R. Pressman
*Kenzo Tsujimoto
*Akio Sakai
}}
| writer         = Steven E. de Souza
| based on       =  
| starring       = {{Plain list |
*Jean-Claude Van Damme
*Raúl Juliá
*Ming-Na|Ming-Na Wen
*Damian Chapa
*Kylie Minogue
*Wes Studi
}}
| music          = Graeme Revell
| cinematography = William A. Fraker
| editing        = {{Plain list |
*Edward M. Abroms
*Donn Aron
*Dov Hoenig
*Anthony Redman
*Robert F. Shugrue
}}
| studio         = Capcom Universal Pictures   Columbia Pictures  
| released       =  
| runtime        = 102&nbsp;minutes
| country        = United States   Australia
| language       = English 
| budget         = $35&nbsp;million 
| gross          = $99,423,521   
}}
Street Fighter is a 1994 American  , produced by Capcom, and stars Jean-Claude Van Damme, and Raúl Juliá, along with supporting performances by Byron Mann, Damian Chapa, Kylie Minogue, Ming-Na Wen and Wes Studi.

The film altered the plot of the original game and motives of the Street Fighter characters. It also significantly lightened the tone of the adaptation, inserting several comical interludes (for instance, one particular fight scene between E. Honda and Zangief pays homage to the old Godzilla films).
 General M. Bison was widely praised and garnered him a nomination for Best Supporting Actor at the Saturn Awards. Julia, who at the time was suffering from stomach cancer (as evidenced by his pale and gaunt facial complexion throughout the movie), took the role at the request of his two children. This was Julias final posthumous theatrical performance, and he died two months before the films release. The film is dedicated to his memory.

Two video game tie-ins based on the film were released which used digitized footage of the actors performing fight moves, similar to the presentations in the Mortal Kombat series of games.

==Plot== Shadaloo City, William F. Carlos "Charlie" severely disfigured by the procedure, Dhalsim secretly alters his cerebral programming to maintain Charlies humanity.
 Ryu Hoshi Viktor Sagat Vega (Jay Balrog (Grand L. Bush), all of whom desire vengeance on Bison and Sagat, stumble across the plan, and, over Guiles objections, attempt to assassinate Bison and Sagat at a party. In order to maintain Bisons trust, Ryu and Ken stop the assassination and reveal Chun Li, Honda, and Balrog to Bison.

Upon returning to his base, Bison inducts Ryu, Ken and Vega into his organization and orders Honda and Balrog imprisoned and Chun-Li taken to his quarters. Ryu and Ken break Balrog and Honda out of confinement and rush to confront Bison, who is fighting Chun-Li, but Bison manages to escape and unleash a sleeping gas, sedating them all. Meanwhile, Guile plans his assault on Bisons base. He is briefly impeded by the Deputy Secretary of the A.N. (Simon Callow), who informs Guile that the decision has been made to pay Bison the ransom, but Guile takes no notice and proceeds with the mission on his own. At the base, Dhalsim is found by a security guard, and a fight ensues in which Charlie is released and he kills the guard to protect Dhalsim. Guile arrives shortly thereafter and sneaks into the lab, where Charlie begins strangling him. Charlie stops when he recognizes Guile. Horrified at what his friend has become, Guile prepares to kill Charlie, to end his suffering, but Dhalsim stops him. Because the ransom was never paid, Bison prepares to kill the hostages by unleashing Charlie on them, but Guile emerges and a gunfight ensues with Guile only barely holding his own until the remaining A.N. forces arrive. After ordering Ryu, Ken, Chun-Li, Honda and Balrog to find and rescue the hostages, Guile engages Bison in a one-on-one duel. As Guile and Bison fight, Ryu and Ken are confronted by Sagat and Vega, and only barely manage to defeat them. Bisons computer expert, Dee Jay (Miguel A. Nunez Jr.) steals Bisons trunk of money and escapes, joined by Sagat. Bisons bodyguard, Zangief (Andrew Bryniarski), engages Honda in a vicious fight until learning from Dee Jay that Bison was the true enemy, and decides to join sides with Ryu and Ken.

Guile eventually gains the upper hand against Bison and kicks him into a bank of hard drives, delivering him a fatal electric shock, but a revival system restores Bison and he reveals that his suit is powered by electromagnetism which enables him to fly and fire electricity from his knuckles. Bison takes control of the fight and eventually moves to deal the death blow, but Guile counters with a well-timed roundhouse kick that sends Bison crashing into his gigantic monitor wall, but also causes the bases energy field to overload. The hostages are rescued, but Guile stays behind to convince Dhalsim and Charlie to return with him, though they refuse, Charlie unwilling to return to society in his condition and Dhalsim wishes to atone for his part in mutating Charlie in the first place. Guile escapes just as the base explodes and reunites with his comrades, while Sagat and Dee Jay realize the trunk was full of useless "Bison Dollars" that Bison intended to use once he had taken over the world. As the base collapses, the fighters strike their winning poses from Street Fighter II: The World Warrior.

In a post-credits scene, Bisons computer is reactivated from solar power and the revival system restores Bison again. His fist smashes through the rubble and a computer screen is shown selecting "World Domination: Replay".

==Cast== Colonel Guile Bison
*Ming-Na|Ming-Na Wen as Chun-Li Ken
*Kylie Minogue as Cammy
*Simon Callow as A.N. Official Ryu
*Roshan Seth as Dhalsim
*Andrew Bryniarski as Zangief Balrog
*Robert Carlos Blanka
*Miguel A. Núñez, Jr. as Dee Jay
*Gregg Rainwater as T. Hawk
*Kenya Sawada as Captain Sawada Vega
*Peter Peter Tuiasosopo Honda
*Wes Sagat

==Production==
Because Capcom was co-financier of the film, every aspect of the production required their approval. Among other points, they mandated a December 1994 release date, which required the cast and crew to maintain an aggressive filming schedule.    Capcom had long envisioned Jean-Claude Van Damme as Guile and asked him to be cast. After Van Damme was cast as Guile and Raúl Juliá as Bison, most of the casting budget had been spent.  (Van Dammes fee alone took nearly 8 million dollars of the films 35 million dollar budget. ) This meant that the majority of other parts had to go to little-known or unknown actors.    Kylie Minogue was cast as Cammy as a result of the Australian Actors Guild wanting Steven E. de Souza to hire an Australian actor. By the time he received the request the only part not cast was that of Cammy. De Souza first learned of Minogue from her cover photo on a "Worlds 30 Most Beautiful People" edition of Who (magazine)|Who magazine.    

The casts physical training was handled by Hollywood trainer and world karate champion Benny Urquidez.  Charlie Picerni was hired as the stunt cordinator; he took the job with the condition that he would need ample time to train the cast. De Souza agreed, however plans were switched once it was learned the Raúl Juliá was suffering from cancer.  Initially plans were to shoot Juliás less intensive scenes first while the rest of the cast would train with Picerni, however upon seeing Juliá, de Souza realized that they could not show him in his current weakened state and was forced to switch the filming around. This led to an environment where the cast would be trained only right before their scenes—sometimes only hours ahead. 
 James Bond and a war film. In addition, he indicated that he also did not want to shoehorn in elements from the games, citing the previous years poorly received Super Mario Bros. (film)|Super Mario Bros. film as an example. De Souza said that he avoided the supernatural elements and powers from the games but would hint at their use for a sequel. 
 Gold Coast during the spring and summer months of 1994 with most of the interiors and exteriors filmed on soundstages in Brisbane. Some exterior scenes were filmed in Bangkok, Thailand which were used as the backdrop for the fictitious Shadaloo City.  The Bangkok scenes were filmed first, in Spring 1994, with filming in Australia beginning that Summer. 

The MPAA gave the first submitted cut of the film an R classification which was unacceptably high for Capcom,  who had stated from the start that it should be a PG-13 film.  After various cuts were made a G rating—according to de Souza—was given which was bumped up to PG-13 with the addition of an expletive in post production.   

==Music==
===Soundtrack===
 
 Hammer was still released as a single, which charted #57 in the UK.

===Score===
Graeme Revell composed the films score, an hour of which was released by Varèse Sarabande.  Revell ignored previously existing music from the franchise. The music differs from Revells more popular style,  most notably with the absence of pervasive electronic elements, and is entirely orchestral. The campy style of the film is reflected in the scores parody cues. The music during the scene where Ryu faces Vega in the cage fight quotes Georges Bizets Habanera from the opera Carmen (opera)|Carmen, and a theme heard throughout the score, particularly in the track "Colonel Guile Addresses the Troops", is reminiscent of Bruce Broughtons main theme for Tombstone (film)|Tombstone. 

==Reception==
===Box office===
The film earned $3,124,775 on its opening day.  It grossed $9,508,030 on its opening weekend, ranking at #3 behind Dumb and Dumber and The Santa Clause at the box office.   On its second weekend it grossed $7,178,360 and dropped down to #7.  The film grossed $33,423,521 at the domestic box office and $66,000,000 at the international box office, making a total of $99,423,521 worldwide. 

===Critical response===
 
 
Street Fighter received negative reviews from critics. Rotten Tomatoes gives it a rating of 12% on based on reviews from 25 critics. The sites consensus states: "Though it offers mild entertainment through campy one-liners and the overacting of the late Raúl Juliá, Street Fighter s nonstop action sequences are not enough to make up for a predictable, uneven storyline."   

Leonard Maltin gave the film his lowest rating, writing that "even Jean-Claude Van Damme fans couldnt rationalize this bomb."  Richard Harrington of The Washington Post said the film was "notable only for being the last film made by Raúl Juliá, an actor far too skilled for the demands of the evil warlord, Gen. M. Bison, but far too professional to give anything less than his best."    Critic Stephen Holden of The New York Times referred to the film as "a dreary, overstuffed hodgepodge of poorly edited martial arts sequences and often unintelligible dialogue".   

The film has found a small but lasting cult following which sees most of its negative points as comically surreal. 

=== Awards ===
 
In 2009, Time (magazine)|Time listed the film on their list of top ten worst video games movies.  GameTrailers ranked the film as the eighth worst video game film of all time. 
The film also received two nominations at the Saturn Awards: Best Science Fiction Film and Best Supporting Actor (a posthumous nomination for Raúl Juliá). 

==Related media== CoroCoro Comics Special.

Two video games based on the film were produced. The first was a  , produced by American developer  , released for the PlayStation and Sega Saturn. Despite sharing the same title, neither game is a port of the other, although they both used the same digitized footage of the film s cast posing as the characters in each game. Capcom also announced that an "enhanced port" was being created for the Sega 32X by their newly formed USA research and development department.  This version was never released.
 Street Fighter animated series, a follow-up to this film which combined story aspects of the film with those in the games.

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 