No Subtitles Necessary: Laszlo & Vilmos
{{Infobox film
| name           = No Subtitles Necessary: Laszlo & Vilmos
| image          = No Subtitles Necessary Poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = James Chressanthis
| producer       = James Chressanthis Tony Frere Zachary Kranzler Kian Soleimanpour
| writer         = James Chressanthis
| starring       =
| music          = Jeffrey Kite
| cinematography = Anka Malatynska
| editing        = Elisa Bonora
| studio         = NC Motion Pictures PBS
| released       =  
| runtime        = 104 minutes
| country        = United states
| language       = English
| budget         =
| gross          =
}}
No Subtitles Necessary: Laszlo & Vilmos is a 2008 American documentary film written and directed by James Chressanthis. 

==Synopsis== 1956 Soviet invasion.  Coincidentally, they also photographed many of the tumultuous events during the invasion.

Michael Goldman, of the Los Angeles Times, described their introduction to America:

:"After a harrowing journey secreting the footage out of the country so it can be seen by the rest of the world, they end up in Los Angeles, where they toil anonymously in B-level biker films, wandering into Roger Cormans orbit. Soon after, both men flash to prominence filming several classic movies, playing important roles in the New Hollywood movement of the late 60s and 70s." 

The documentary chronicles their careers with numerous clips and testimonials from Peter Bogdanovich, Peter Fonda, Dennis Hopper, Sandra Bullock, Tatum ONeal, Sharon Stone and Barbra Streisand and others.

==Interviews==
 
 
* Karen Black
* Peter Bogdanovich
* Sandra Bullock
* Graeme Clifford
* Allen Daviau
* Richard Donner
* Peter Fonda
* Dennis Hopper
* László Kovács (cinematographer)|László Kovács
* Ellen Kuras
* Rachel Miner
* Bob Rafelson
 
* Tatum ONeal
* Owen Roizman
* Mark Rydell
* Ray Dennis Steckler
* Sharon Stone
* Vittorio Storaro
* Barbra Streisand
* Jon Voight
* Haskell Wexler
* John Williams
* Irwin Winkler
* Vilmos Zsigmond
 

==Background==
One of the motivations for making the documentary was " he one-two punch of Robert Altman|Altmans death in 2006 and the onset of health problems for Mr. Kovacs ultimately spurred Mr. Chressanthis and his co-producers to bring the two men together again — this time in front of a camera — and have them tell their tale."  Director James Chressanthis studied under both Kovacs and Zsigmond. 

Some critics noted that the film is more than about Zsigmond and Kovacs work.  Michael Goldman said, "As much as No Subtitles Necessary documents the professional successes of these cinematographers, its mainly about their profound friendship and devotion to each other. Near the end of the documentary, Audrey Kovács illustrates that point, insisting that while she lost her husband in 2007, she isnt in fact his only widow. Frankly, I think that László had one widow, and its Vilmos, she sighs. I think they were as close as two men could ever be." 

==Reception==

===Critical response===
Rob Nelson, film critic for Variety (magazine)|Variety magazine liked the documentary and said, "Still inadequately renowned for having literally supplied the pictures, gritty and pretty, of the 1970s New Hollywood, Hungary-born lensers Vilmos Zsigmond and the late Laszlo Kovacs are warmly illuminated in No Subtitles Necessary. Spanning the pairs half-century-long friendship and individual work on strikingly shot films of the past four decades, pic deftly combines personal, political and cinematic histories through anecdote-laden interviews and eye-popping clips. Docu would naturally light up any serious fest worldwide, while its humanistic ode to the lensers love of their families, their collaborators and each other gives it a shot at even wider exposure." 

In his review of the film/DVD, critic Kevin Jagernauth, discussed the Hollywood contacts and film shoots that helped the cinematographers careers in the early days.  He wrote of Vilmos, "For Zsigmond, it would be Robert Altmans anti-western McCabe & Mrs. Miller that would bring him fame. For anyone who has seen the film, the entire mood is set by the cinematographers gauzy lensing of the interiors, and his uncomprosing camerwork with the exteriors. Robert McLachlan (Final Destination 3, Cursed (2005 film)|Cursed) recalls being on the set, and being amazed that what he saw with his own eyes and on the big screen couldnt be more different, inspiring him to follow his own path to becoming a cinematographer." 

==See also==
* Visions of Light (1992)
* Cinematographer Style (2006)

==References==
 

==External links==
*  
*  
*  
*  
*   at PBSs Independent Lens PBS YouTube Channel

 
 
 
 
 
 
 