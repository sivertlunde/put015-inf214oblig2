Madame Behave
{{infobox film
| name           = Madame Behave
| image          = 
| imagesize      =
| caption        =
| director       = Scott Sidney
| producer       = Al Christie
| writer         = Jean Arlette (play) F. McGrew Willis (scenario) Ann Pennington
| cinematography = Gus Peterson Alec Phillips
| editing        =
| distributor    = Producers Distributing Corporation (PDC)
| released       = November 21, 1925 (premiere) December 6, 1925 (nationwide)
| runtime        = 6 reels (5,417 feet)
| country        = United States
| language       = Silent film (English intertitles)
}} silent film comedy starring cross-dressing actor Julian Eltinge. The film is based on a play by Jean Arlette and was produced by Al Christie with distribution through Producers Distributing Corporation (or PDC). This film is a survivor with an incomplete print residing with the Library of Congress.  

==Cast==
*Julian Eltinge - Jack Mitchell/"Madame Behave" Ann Pennington - Gwen Townley
*Lionel Belmore - Seth Corwin
*David James - Dick Corwin Tom Wilson - Creosote Jack Duffy - M.T. House
*Stanhope Wheatcroft - Percy Fairweather
*Evelyn Francisco - Laura Barnes
*Tiny Sandford - Policeman

==See also==
*List of partially lost films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 