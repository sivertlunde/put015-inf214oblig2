Freak Orlando
{{Infobox film
| name           = Freak Orlando
| image          = Freak Orlando poster.jpg
| caption        = Film poster
| director       = Ulrike Ottinger
| producer       = Renée Gundelach Sibylle Hubatschek-Rahn
| writer         = Ulrike Ottinger
| narrator       = 
| starring       = Magdalena Montezuma
| music          = 
| cinematography = Ulrike Ottinger
| editing        = Dörte Völz-Mammarella
| distributor    = 
| released       =  
| runtime        = 126 minutes
| country        = West Germany 
| language       = German
| budget         = 
}}

Freak Orlando is a 1981 West German comedy film directed by Ulrike Ottinger and starring Magdalena Montezuma.    

==Cast==
* Magdalena Montezuma – Orlando, als Pilger, Orlando Zyldopa, Orlando Orlanda, Orlando Capricho...
*Hans Langerschlanger - Muntzy Pimplips 
* Delphine Seyrig	... 	Helena Müller, als Lebensbaumgöttin, Kaufhausonsängerin, Mutter der Wundergeburt....
* Albert Heins – Herbert Zeus
* Claudio Pantoja – Zwei Tänzer
* Hiro Uchiyama – Zwei Tänzer (as Hiro Uschiyama) Galli – Chronistin
* Eddie Constantine – Säulenheiliger
* Else Nabu – Heilige Wilgeforte
* Maria Bucholt – Kleine Menschen
* Paul Glauer – Kleine Menschen
* Alfred Raupach – Kleine Menschen
* Luzig Raupach – Kleine Menschen
* Monika Ullemeyer – Kleine Menschen (as Monika Ullemayer)
* Dirk Zalm – Kleine Menschen
* Luc Alexander – Zwolf Lederboys

==References==
 

==External links==
* 

 
 
 
 
 
 
 