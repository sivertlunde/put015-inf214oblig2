Bag of Bones (film)
{{Infobox television film name = Bag of Bones image = Bag of Bones Title Card.jpg image_size =  caption = Title card  genre = Mystery
 distributor =  director = Mick Garris producer = Michael Mahoney screenplay = Matt Venne based on =   narrator =  starring = Pierce Brosnan Annabeth Gish Melissa George Anika Noni Rose Caitlin Carmichael music = Nicholas Pike cinematography =  editing = Andrew Cohen Patrick McMahon studio =  budget =  country = United States language = English network = A&E Network released =   runtime =  num_episodes = 2 or 1 website = http://www.aetv.com/bag-of-bones/}}
 Channel 5 on 29 December 2012, it was however shown as a single three-hour film. 

==Plot==
Best-selling novelist Mike Noonan and his wife, Jo, are unable to conceive children; Mike conceals that he has a low sperm count. Jo is killed by a bus while crossing a street. As she dies in his arms, Mike notices she bought a pregnancy test, and assumes that she may have been having an affair. Overcome by grief from her death, Mike develops a case of writers block. He suffers a series of nightmares about his wife and their summer home on Dark Score Lake in Maine. On advice from his brother, Sid, Mike takes a trip to the summer house.

Once there, he meets a young widow named Mattie Devore and her 6-year-old daughter, Kyra. Befriending them, he earns the ire of Matties estranged father-in-law Max Devore. Max has been trying to get custody of Kyra since Mattie shot his son Lance (Lance was trying to drown the child). Despite the turmoil, Mike begins to write again, but visions and nightmares lead him to believe he isnt alone. He finds that his wifes spirit is with him. He also detects the spirit of a 1930s singer named Sara Tidwell who plays records of her music. Sara also appears in dreams that Mike has of her last day alive in 1939. During his stay at the lake, Mike learns about "Dark Score Crazy," an apparent form of madness that caused several men in the town to murder their daughters by drowning them.

Max gives Mike an ultimatum: Max will stop the custody battle for Kyra if Mike agrees not to interfere with their lives. Mike agrees. Shortly afterward, Max has his assistant help him in committing suicide. At the funeral, Mike meets Edgar White, an old friend of his grandfather. Edgar confesses to Mike that Edgar, Mikes grandfather, and two other men assisted Max with the rape and murder of Sara Tidwell and the drowning of her daughter, Keisha. With her dying breath, Sara cursed the boys descendants to drown their own daughters in retribution for drowning Keisha. The boys buried Sara with her daughter in the woods.

Mike tells Mattie the story. Mattie hypothesizes that if Jo was pregnant, she may have returned to Dark Score to confirm the curse on Mikes bloodline. Mattie is suddenly shot and killed in a manner that Mike saw in a dream, but before she dies she begs him to take care of Kyra. Mike is chased back to his home by Matties killers, but escapes when they are killed by a falling sign during a storm. Mike tries to console Kyra and puts her to bed. Maxs spirit angrily compels Mike to drown Kyra in fulfillment of the curse, but Matties spirit struggles to thwart him. Mike takes refuge in his office, where he finds clues left by Jo in the story he had been writing, directing him to douse Sara and Keishas remains with lye to end the curse.

A tree that had been transformed into the figure of a woman marks the burial site. Mike returns to this tree to recover the remains, but the haunted tree attempts to deter him. Jo appears and distracts Sara, allowing Mike time to use the lye to dissolve the bones. Saras spirit is appeased and the tree returns to normal. Mike says a final goodbye to Jo. He returns home to find Kyra in the bathtub, and he is attacked by Maxs assistant who tries to kill them both. Mike kills her, and Matties spirit appears before them. Mattie tells Kyra that she is safe and that Mike will take care of her. The next morning, Mike announces that he intends to adopt Kyra as his own daughter.

== Cast ==
*Pierce Brosnan as Mike Noonan
*Melissa George as Mattie
*Annabeth Gish as Jo Noonan
*Anika Noni Rose as Sara Tidwell
*William Schallert as Max Devore
*Caitlin Carmichael as Kyra Devore
* David Sheftell as Max Devore (young)
* Gary Levert as George Footman
* Jason Priestley as Marty
* Matt Frewer as Sid Noonan

== Background ==
The project started when Mark Sennet of Sennet Entertainment and Stewart Mackinnon of Headline Pictures secured the rights to the novel and contracted Mick Garris to direct the film, Matt Venne to write the screenplay and attached Pierce Brosnan in the leading role. Filming started August 2011 in Nova Scotia. Singer Kelly Rowland was originally cast as the role of Sara Tidwell but was replaced by Anika Noni Rose. 

==Critical and popular reception==
 s Blair Marnell gave the miniseries a 6 out of 10 rating, commenting: "The greatest weakness of Bag of Bones is that it seems to be too intent upon maintaining the pace of Kings novel to the detriment of the onscreen story." 

Part One was watched by 3.37 million viewers and had a 1.1 with the 18-49 age range, Part Two dropped slightly to 2.99 million with a 0.9 rating.      

==Connection to Kings other works==

*At a book signing, one of Mikes fans describes himself as his "number one fan"; Jo teases him to "have fun with Annie Wilkes". Annie Wilkes is an obsessed fan in Kings novel, Misery (novel)|Misery. The Stand, plays Mike Noonans brother.
*Mikes agent Marty mentions, "Theres even talk of a newly-discovered Bachman book". "Richard Bachman" is a pen name used by Stephen King for some of his books.
*Mike is looking for clues in a crossword puzzle and comes up with the phrase "Booya moon". This is a fictional world in Kings novel Liseys Story.
*Mike briefly has a conversation with Ralph Roberts, who is busy solving a crossword puzzle, and notes that the old man has the look of an insomniac. Ralph Roberts is the main character of Stephen Kings Insomnia (novel)|Insomnia. Both novels occur in the town of Derry, Maine.
*Mike mentions Bill Denbrough, a character from It (novel)|It.
*Mike mentions Alan Pangborn and Polly to the Sheriff who are from Needful Things

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 