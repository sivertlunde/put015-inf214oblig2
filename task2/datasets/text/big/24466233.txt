Khastegi
{{Infobox Film
| name           = Khastegi
| image          = 
| image_size     =
| caption        = Sample poster
| director       = Bahman Motamedian
| producer       = Bahman Motamedian Esmaeil Mirzaei Ghomi
| writer         = Bahman Motamedian
| music          = Iman Vaziri
| cinematography = Homayoon Paivar
| editing        = Bahman Motamedian Behzad Mosleh
| distributor    = Celluloid Dreams
| runtime        = 76 minutes
| country        = Iran
| language       = Persian
}}
Khastegi (Persian: خستگی; also known as Tedium and Sex My Life) is a 2008 Persian independent film written and directed by Bahman Motamedian and produced in Iran. It was shown at the 65th Venice International Film Festival in 2008.

==Plot==
Khastegi (aka Tedium) tells the story of seven Iranian transsexuals living in Tehran. 

==Screenings==
* 24th BFI London Lesbian and Gay Film Festival (British Film Institute|BFI) (March 17–31, 2010/ London, England)  
* Cinema Digital Seoul Film Festival (CinDi) (August 19–25, 2009 / Seoul, South Korea)  
* Chelsea Art Museum (July 15- August 19, 2009 / New York, USA)  
* 19th Toronto LGBT Film Festival (May 14–24, 2009 / Toronto, Canada)  
* 24th Torino GBLT Film Festival (23–3 April 2009 / Torino, Italy)  BEST DOCUMENTARY PRIZE to Khastegi (Sex My Life) by Bahman Motamedian (Iran, 2008) at 24th Turin GBLT Film Festival   
* Prague international Film Festival (Febiofest) (26 March-3 April 2009 / Prague, Czech Republic) 
* Mexico City International Contemporary Film Festival (FICCO) (17 Feb. - 1 Mar. 2009 / Mexico, Mexico City) 
* Asia Pacific Festival of 1st Films (4–10 December 2008 / Singapore)  
* Three Continents Festival, Nantes (22 November - 2 December 2008 / Nantes, French) 
* Sao Paulo Film Festival (17–30 October 2008 / São Paulo, Brazil) 
*  Venice Film Festival (August 27- September 6, 2008 / Venice, Italy)           

==Awards and nominations==
*Awards the first prize	(24th Torino GBLT Film Festival 23–3 April 2009 / Torino, Italy)  
*Special citation (Best producer) (Asia Pacific Festival of 1st Films/ 4–10 December 2008/ Singapore)  
*Brian Prize (65th Venice International Film Festival / August 27 to September 6, 2008)  
*Queer Lion Award 	(65th Venice International Film Festival / August 27 to September 6, 2008)  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 