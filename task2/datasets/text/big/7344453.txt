Falafel (film)
{{Infobox film
| image          =Falafelthemovie.jpg
| name           =Falafel
| writer         =Michel Kammoun
| starring       =Elie Mitri   Gabrielle Bou Rached   Said Serhan   Issam Bou Khaled   Michel El Hourany   Hiam Abou Chedid   Fadi Abi Samra   Rafic Ali Ahmad   Roger Assaf
| director       =Michel Kammoun 
| released       =2006
| runtime        =83 minutes
| country        =Lebanon
| music          =Toufic Farroukh
| cinematography =Muriel Aboulrouss
| language       =Arabic
| producer       =Roy films (Lebanon)   Ciné-Sud Promotion
|
}}
 Lebanese film written and directed by Michel Kammoun. It world premiered on September 16, 2006 at the Ayam Beirut Festival. 
 Falafel is Kammouns first feature film.

==Synopsis==
A summer evening in Beirut. The life of Toufic, a young Lebanese man, and his nightly strolls. Between his family, friends and love affairs, he tries to seize every day of his life, through pleasures and entertainment. For him, every second is the most important. Soon he discovers that having a normal life, in this country, is a luxury. 15 years after the war had ended, a volcano is lying dormant on every street corner, like a time-ticking bomb that is ready explode… This night will be pivotal in the life of the young man. It was produced by Elle Kensington.

==Cast==
* Elie Mitri (Toufic)
* Gabrielle Bou Rached (Yasmin)
* Said Serhan
* Issam Bou Khaled (Abboudi)
* Michel El Hourany (Nino)
* Aouni Kawas (Jo)
* Hiam Abou Chedid
* Rafic Ali Ahmad
* Roger Assaf
* Fadi Abi Samra
* Maryann Capasso

==Awards==
*Golden Bayard for Best Film, Festival International du Film Francophone de Namur, Belgique 2006   
*Golden Bayard for Best Music, Festival International du Film Francophone de Namur, Belgique 2006 
*Silver Muhr for Best Film, Dubai International Film Festival, UAE 2007 
*Audience Award - LILLE International Film Festival, France 2007.
*Best First Film Award - ROTTERDAM Arab Film Festival, Nederland 2007.
*ART AWARD (Best First Film Award) - ALEXANDRIA International Film Festival, Egypt 2007.
*Bronze Dagger for Best Film, Muscat International Film Festival, Oman 2008
*Special Jury Prize, International First Film Festival Annonay, France 2008.
*Palmera de Bronce, Mostra de Valencia "Cinema del Mediterrani", Spain 2007 

==External links==
* 
* 
*  
* 

==Reviews==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

==References==
 

 
 
 
 
 