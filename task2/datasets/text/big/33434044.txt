Let Us Be Gay
{{Infobox film
| name           = Let Us Be Gay
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Robert Z. Leonard
| producer       = Robert Z. Leonard Irving Thalberg
| writer         = Frances Marion Lucille Newmark 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Norma Shearer
| music          = 
| cinematography = Norbert Brodine
| editing        = Basil Wrangell
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =   
| runtime        = 79 mins.
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Let Us Be Gay is a 1930 American comedy-drama film produced and distributed by MGM. It was directed by Robert Z. Leonard and stars Norma Shearer. It is based on a 1929 Broadway play by Rachel Crothers.  

==Cast==
*Norma Shearer - Mrs. Katherine Brown
*Marie Dressler - Mrs. Bouccy Bouccicault
*Rod La Rocque - Bob Brown
*Gilbert Emery - Towney Townley
*Hedda Hopper - Madge Livingston
*Raymond Hackett - Bruce Keane
*Sally Eilers - Diane
*Tyrell Davis - Wallace Granger
*Wilfred Noy - Whitman, the Butler
*William H. OBrien - Struthers, a Servant
*Sybil Grove - Perkins, a Maid
*Elinor Flynn  (Uncredited) Mary Gordon - Mrs. McIntyre (Uncredited)
*Helene Millard - Helen Hibberts(Uncredited) Dickie Moore - Young Bobby (Uncredited)

==DVD release==
The film was released on DVD through the Warner Archive Collection.

==References==
 

==External links==
*  
*  
* 
* 

 

 
 
 
 
 
 
 
 
 


 