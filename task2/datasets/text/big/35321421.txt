Magic Flute Diaries
{{Infobox television film
| bgcolour =
| name = Magic Flute Diaries
| image = Magic Flute Diaries DVD cover.jpg
| caption = DVD cover
| runtime = 104 minutes Kevin Sullivan
| producer = Trudy Grant Kevin Sullivan Ray Sager
| writer = Mozart (original opera)
| starring = Rutger Hauer Warren Christie Mireille Asselin
| music = Peter Breiner Mozart (original opera)
| country = Canada
| language = English
| released =  
}}
Magic Flute Diaries is a film inspired by Mozarts classic opera, The Magic Flute. It does not use the plot of the opera. The film was released in 2008 by Sullivan Entertainment. Magic Flute Diaries won the award for Best Family Film in the 2008 Staten Island Film Festival. 

==Synopsis==
Tom (Warren Christie), a young classical singer, reluctantly accepts the lead role in a production of The Magic Flute during Mozart’s 250th birthday celebration in Salzburg at the urging of his girlfriend Sandy (Kelly Campbell). 
As rehearsals unfold, Tom is captivated by the magical power of Mozart’s final opera. He is completely overcome with amazement at the musical genius that surrounds him. In an effort to visualize Mozart’s fantasy, Tom imagines himself in the opera’s story and drifts in and out of reality as if in a dream. Gradually he becomes completely enraptured by the intoxicating musical atmosphere swirling around him.

Tom’s rapture is heightened further when he meets his mysterious co-star, Masha (Mireille Asselin), an unknown Russian soprano of astonishing talent. This extraordinary young singer is kept isolated from both the company and the press by her manager, Professor Nagel (Rutger Hauer). Tom becomes infatuated with Masha causing his relationship with Sandy to fall apart. Tom finds himself seduced as much by Mozart’s music as by his bewitching co-star. Tom’s concern for the girl, and his obsession to find out more about her mysterious past, becomes a quest that parallels the operatic fable actually being played out onstage.

Tom’s strong feelings for Masha have left him unable to distinguish between reality and fantasy. Like his character in the opera, Tom wonders if he is being tested for a higher purpose but he knows he must play out his role in Masha’s story until the final curtain.

==Production==

===Origin=== Kevin Sullivan, The Woman in White, which used three-dimensional backdrops in its London production. Though Sullivan claims not to be an opera buff, he had always had a love for Mozart’s The Magic Flute. 

===Filming===
Due to the impossibility (budget and time constraints) of filming on-location in Salzburg, Sullivan and a team travelled to Austria, Germany and Hungary, taking digital photographs of various churches, streetscapes, grand interiors and scenic backdrops. Tilley, Steve. "The Land of Movie Magic", The Toronto Sun, 23 June 2006.  Filmed in Toronto, shooting was completed in front of a green screen, allowing computer animators to remove the background and splice in digital photographs and images in postproduction. Terauds, John, "There’s Magic in the air", The Toronto Star, 24 June 2006.  Thom Best served as DOP, with special effects by Tony Willis. Davidson, Sean, "Sullivan feels the Magic", Playback, 10 July 2006.  Advanced mixers with live links to the live cameras allowed for the technicians to adjust background images in real time as the cameras rolled, giving a rough idea of what the finished product would look like. 

===Opera Atelier===
The Toronto opera company, Opera Atelier, partnered with Sullivan Entertainment for the production, providing costumes, crew and cast.  Opera Atelier co-bosses Marshall Pynkoski and Jeannette Zingg were choreographers for dance numbers in the film.  Pynkoski also played the role of the opera director in the film. 

==Cast==
* Rutger Hauer as Dr. Richard Nagel
* Warren Christie as Tom (Tamino)
*Mireille Asselin as Masha (Pamina)
*Kelly Campbell as Sandra (Papagena)
*Olivier Laquerre as Papageno
*Curtis Sullivan as Sarastro
*Erin Windle as The Queen of the Night
* Daniel Kash as Monostatos

==References==
 

==External links==
*  
*  

 
 

 
 
 
 