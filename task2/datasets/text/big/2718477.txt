A Tale of Two Cities (1911 film)
{{Infobox film
| name           = A Tale of Two Cities
| image          = Ataleoftwocities-1911-newspaperad.jpg
| image_size     =
| caption        = Newspaper advertisement.
| director       = William J. Humphrey
| producer       = J. Stuart Blackton
| studio         = Vitagraph Studios
| writer         = Charles Dickens (novel) Eugene Mullin (scenario)
| narrator       =
| starring       =
| cinematography =
| editing        =
| distributor    = General Film Company
| released       =  
| runtime        = 30 minutes
| country        = United States Silent (English intertitles)
| budget         =
}}
 novel by Charles Dickens.

According to film historian Anthony Slide, this was a three-reel film, totalling 30 minutes, released in weekly one-reel segments. Talmadge played the small role of Mimi the Seamstress, who accompanies Sidney Carton to the guillotine, although in this version he ascends the scaffold before her, and her death is not actually depicted.     The film debut of Lydia Yeamans Titus.

==Cast==
*Maurice Costello...Sydney Carton
*Florence Turner...Lucie Manette
*John Bunny Woman on the way to the guillotine 
*William J. Humphrey...The Duke DEvremonde
*Florence Foley
*Kenneth Casey...Dukes Son
*Ralph Ince
*James W. Morrison...Peasant Brother
*Julia Swayne Gordon Charles Kent...Dr. Manette
*Tefft Johnson
*Leo Delaney...Darnay
*William Shea
*Mabel Normand
*Earle Williams
*Edith Storey
*Lillian Walker...Peasant Sister Helen Gardner
*Dorothy Kelly
*Edwin R. Phillips
*Eleanor Radinoff
*Anita Stewart
*Lydia Yeamans Titus

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 