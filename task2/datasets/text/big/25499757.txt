Pramani
{{Infobox film
| name           = Pramani
| image          = Pramani.jpg
| alt            =  
| caption        = Theatrical poster
| director       = B. Unnikrishnan
| producer       = B.C.Joshy
| writer         = B.Unnikrishnan
| starring       =  
| music          = M. Jayachandran
| cinematography = Shamdat Manoj
| studio         = 
| distributor    = Play House
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =  
| gross          =  
}} Sneha and Janardhanan and Lakshmi play Prabhu in Tamil as Bramma.

==Plot==

Viswanatha Paniker (Mammootty) is a corrupted Panchayat president of Thazhe Keezhppadam grama panchayath in Kuttanad for 25 years. He earns a lot of wealth through his corruption and by cornering his village people to hand over their lands to him. But he never acquires in his name, instead he gives them to his relatives. He has also been nicknamed America for his tricky ways to play the big brother and thereby getting control over the issues where he gets involved. Panickers dealings are done through his cousin brother and confidant, Somasekharan (Siddique (actor)|Siddique). On the other hand Castro Vareeth (Janardhanan (actor)|Janardhanan) is totally different man from Viswanathasa Paniker. He wants his village people to have a peaceful life.

The members of the Panchayat who work under Viswanatha Paniker are also ruthless and they are afraid of the arrival of the new Panchayat Secretary Janaki (Sneha (actress)|Sneha) as they claim that she is very strict. On arrival, she finds out that there are so many illegal transactions and threatens that she will write to the higher authority. But, Panicker never listens to her and continue their illegal works. Somasekharan meets Bobby (Fahadh Faasil) who is on his mission to instruct the people about the activities of Panicker and bring them to light. Soma threatens Bobby but he continues his work. Panicker later learns that Bobby is the grandson of Rosy Teacher (Lakshmi (actress)|Lakshmi) and son of his old friend Varkichan Joseph(Prabhu Ganesan|Prabhu) and Panicker never comes across his way.

Panicker illegally wants to sign an agreement for Cyber Park Project which needs more land for the construction of the building. Janaki is against it claiming that it is an agricultural land. But Panicker-Soma wants the agreement to be signed and Soma house-arrests Janaki along with her ailing mother (K. P. A. C. Lalitha) while Panicker signs the agreement with a secretary for that day in place of Janaki and he controls the other members of Panchayat with the bribe. Later Rosy teacher advises him and asks him to cancel the agreement. She also recites that Panicker has got this job only because of the death of her son, Varkichan and she always wanted Panicker to be honest like her son.

Panicker, on the advice of Rosy, plans to cancel the agreement but Soma and the other family members stands against him. They asks him to leave the house if he wants to cancel the agreement. Panicker leaves the house with the Orphan girl, whom he claims that, he saw her in the train on the day of Varkichans death. Panicker starts living in the Panchayat office and Janaki feels sad for him and starts supporting him on knowing that he had changed. Panicker and Janaki goes to meet the higher authority in Alapuzha to cancel the agreement but the authority says that he had been receiving anonymous letters from the village that there is an illegal relationship between Janaki and Panicker. Janaki insults the authority by saying that he had time to read these letters but never had time to do his job. 
 Trivandrum for work. Next day, Bobbys things are seen near the river and Soma claims that Panicker has killed Bobby as he came to about Panickers true color. Rosy believes it and she slaps Panicker. Panicker recites the story of her son Varkichan that he had lurked from others for the past 25 years. He says Varkichan had a relationship with a lady after his marriage and fathered a girl and Varkichan had taken him to his concubines funeral. By accident, Varkichan falls from the train during their return journey. He also says that the girl whom he claimed to be an orphan whom he saw in train, is Varkichans daughter. Panicker swears that he will bring Bobby back.

Panicker slams Soma and finds Bobby. He apologizes to the village and also to Rosy teacher and swears that he will go in a right way.

==Cast==
* Mammootty as Viswanatha Paniker Sneha as Janaki
* Fahadh Faasil as Bobby
* Nazriya Nazim as Sindhu Lakshmi as Rosy Teacher Janardhanan as Castro Vareeth Siddique as Somasekharan Prabhu as Varkichan Joseph
* K. P. A. C. Lalitha as Janakis Mother
* Anoop Menon as District Collector
* Suraj Venjaramoodu as Maoist Murugan Baiju
* P. Sreekumar
* Lakshmipriya as Somasekharans sister
* T Parvathy

== Production ==

===Casting=== Sneha was Thuruppu Gulan Sneha plays Prabhu was roped in to play a Cameo role.

===Filming=== Kochi and Alappuzha. 

== Reception ==
The film got mixed reviews from the critics and from several film reviewing sites. Paresh C Palicha of Rediff.com rated the film 2 in a scale of 5 and said, "In the final analysis, Pramaani delivers only on one promise, that being a star vehicle. In every other aspect it fails."  Sify.coms reviewer said, "In all fairness, Pramani is watchable as a passé entertainer, if you are not looking forward to some nice experiments. Its an old-fashioned tale, being told in the formulaic or the clichéd style." 

==References==
 

== External links ==
*  

 
 
 