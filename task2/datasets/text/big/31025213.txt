Las locuras de Tin-Tan
{{Infobox film
| name           =Las locuras de Tin-Tan
| image          = 
| image size     =
| caption        =
| director       =  Gilberto Martínez Solares
| producer       =Fernando de Fuentes
| writer         =  Carlos León (story), Gilberto Martínez Solares
| narrator       =
| starring       =  Germán Valdés, Carmelita González, Marcelo Chávez
| music          = Gonzalo Curiel
| cinematography = Jorge Stahl Jr.
| editing        = José W. Bustos
| distributor    = 
| released       =  9 January 1952 (Mexico)
| runtime        = 87 min
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1952 Mexico|Mexican film. It was produced by 
Fernando de Fuentes.

==Cast==
* 	Germán Valdés	 ...	Tin-Tan (as German Valdes Tin-Tan)
* 	Carmelita González	 ...	Lolita
* 	Marcelo Chávez	 ...	Marcelo (as Marcelo)
* 	Evangelina Elizondo		
* 	Tito Novaro		
* 	Wolf Ruvinskis		
* 	Eva Calvo		
* 	Joaquín García Vargas		 (as Borolas)
* 	Florencio Castelló		
* 	Armando Sáenz		
* 	Francisco Reiguera		
* 	Nicolás Rodríguez (actor)|Nicolás Rodríguez		
* 	Quinteto Allegro
		
==External links==
*  

 
 
 
 


 