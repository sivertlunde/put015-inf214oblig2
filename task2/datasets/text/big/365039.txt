Six Days Seven Nights
{{Infobox film
| name           = Six Days Seven Nights
| image          = Six_days_seven_nights.jpg
| caption        = Theatrical release poster
| director       = Ivan Reitman
| producer       = Ivan Reitman Roger Birnbaum
| writer         = Michael Browning
| starring       = Harrison Ford Anne Heche  Taj Mahal Michael Chapman
| editing        = Wendy Greene Bricmont Sheldon Kahn
| studio         = Touchstone Pictures Caravan Pictures Northern Lights Entertainment Roger Birnbaum Productions Buena Vista Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $70 million
| gross          = $164,839,294 
}}

Six Days Seven Nights is a 1998 adventure film|adventure-comedy film, directed by Ivan Reitman and starring Harrison Ford and Anne Heche. The screenplay was written by Michael Browning. It was filmed on location in Kauai, and released on June 12, 1998.

== Plot == New York South Pacific. The final leg of their journey to Makatea is in a small dilapidated aircraft, piloted by middle-aged American Quinn Harris. They are accompanied by Quinns girlfriend and co-pilot Angelica.

On their first night on the island, a drunk Quinn makes a move on Robin, which she rejects as Frank appears. Later that night, Frank proposes to her and she happily accepts.

The next morning Robin is called away to Tahiti to supervise a fashion event. She hires Quinn to fly her there, but a thunderstorm forces them to crash-land on a deserted island. Initially believing they are on an island with a peninsula to the north, they climb a mountain to disable a beacon Quinn believes to be there. They discover they are in fact on a different island with no beacon. Fighting for survival on the island, they inadvertently become witnesses to South Asian pirates, who discover and chase them.

Meanwhile, after getting drunk and thinking that Robin is dead, Frank sleeps with Angelica after she seduces him.

After evading the pirates, Robin and Quinn are caught, but narrowly escape by jumping into the ocean from a cliff. They camp next to the wreckage of a World War II Japanese plane. Salvaging parts from it, they succeed in getting Quinns plane airworthy again, and fly back to Makatea just in time for their funerals, as well as escaping the pirates, whose boat is destroyed. Frank is very happy to see Robin alive, but is disgusted at himself for sleeping with Angelica and not being able to tell Robin about it. Robin goes to the hospital where Quinn is recovering and tells him her feelings, but he rejects her.

Robin then decides to go back to New York with Frank, but at the airport in Tahiti she finds that she is unable to go. Frank finally tells her he slept with Angelica and she tells him about her feelings for Quinn. They decide they are not in love and she gives him back the engagement ring.

Quinn has a change of heart and rushes to the airport, but is too late to stop the plane. He then encounters Robin getting off an airplane, having stopped the flight. He walks up to her and they embrace and kiss each other.

== Cast ==
* Harrison Ford as Quinn Harris
* Anne Heche as Robin Monroe
* David Schwimmer as Frank Martin
* Jacqueline Obradors as Angelica
* Temuera Morrison as Jager
* Allison Janney as Marjorie
* Douglas Weston as Philippe Sinclair
* Cliff Curtis as Kip
* Danny Trejo as Pierce
* Ben Bodé as Tom Marlowe
* Derek Basco as Ricky
* Amy Sedaris as Robins secretary

== Production == de Havilland Huey Helicopter suspending the unmanned aircraft with a 200 foot cable with the engine running. 

Harrison Ford is a certificated pilot and did his own flying in the movie, after fulfilling the insurance companys training requirements. 

== Reception ==

=== Critical response ===
The film received generally mixed to negative reviews. The film holds 36% positive reviews at review aggregation website Rotten Tomatoes, based on 39 reviews.  
It holds a score of 51% on Metacritic based on reviews from 23 critics. 

=== Box office ===
The films revenue exceeded its $70 million production budget in the United States earning $74,329,966, and with strong international sales totaling $90,509,328, Six Days Seven Nights ended its theatrical run with a worldwide total of $164,839,294.   

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 