Close My Eyes (film)
{{Infobox film
| name           = Close My Eyes
| image          = CloseMyEyes.jpg
| caption        = Promotional movie poster for the film
| director       = Stephen Poliakoff
| producer       = Therese Pickard
| writer         = Stephen Poliakoff
| starring       = Alan Rickman Clive Owen Saskia Reeves Michael Gibbs
| cinematography = Witold Stok
| released       = 1991 
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
}} Michael Gibbs FilmFour International by Therese Pickard.

The film won the Evening Standard British Film Awards|Evening Standard film award for best picture in 1991.

==Synopsis==
The film opens in 1985. Town planning student Richard Gillespie (Clive Owen) walks through a scene of tower-blocks en route to visit his older sister Natalie (Saskia Reeves), whom he is not very close to since they each grew up with a different parent after their parents divorced.  Natalie has recently bought a fairly noisy apartment with her lover only to have him leave her within two months. She is also unhappy in her job as a buyer. On Richards insistence, they go for a late-night walk and Natalie seems to cheer up, but on waking in the middle of the night, due to loud music from the neighboring apartment, Richard finds Natalie still awake and still extremely depressed. Richards attempts to lighten the mood are ended when Natalie pulls him into a somewhat passionate embrace, although she immediately apologizes and makes the excuse she just wanted someone to hug.  Richard does not seem to have a reaction.

The film checks in on the two in 1987 and 1990. During this time, Richard becomes an extremely successful town planner and something of a lothario while Natalies career stagnates in a succession of low-level administrative jobs she hates. 

The next time they meet is 1991. Richard, committed to the environment and sustainable town planning, is interviewed for a job with the magazine Urban Alert, formed to put pressure on the planners of Docklands to consider the needs of the local residents. The editor, Colin (Karl Johnson), is somewhat skeptical of Richards intentions but takes him on anyway to work with his deputy, Jessica (Lesley Sharp). Meanwhile, Natalie applies for a job as secretary to a very powerful and rich stock analyst named Sinclair Bryant (Alan Rickman). She does not get the job but later marries Sinclair.
 Petersham and is surprised by her changed, more "posh" mannerisms.  He is introduced to Sinclair and the two get along extremely well as they are both highly intelligent and seem to have similar interests in literature (Marcel Proust) and a similar outlook on life.  

However, Natalie is not fully happy with her life because she feels insignificant compared to Sinclair. Shortly after Richards visit to her home, she visits his apartment. While attempting to prevent their attraction for each other from spilling over, they end up naked on the floor. Natalie tries to make Richard stop her from making love. He stops her and wanders to the other side of the room. Eventually, they succumb to their desires and have sex.

Afterwards, they try to avoid each other; this proves difficult as Sinclair invites Richard round to a social gathering and the two, still attracted to each other, agree to meet again to sort things through.  Meanwhile, at work, which Richard has proven to be good at, Colin has developed AIDS and informs the office. After this revelation, Richard and Colin find it hard to interact with each other because they are both pretending nothing is wrong.

Natalie and Richard meet again in Sinclairs parents apartment and have sex, although afterwards, Natalie is quite firm that their relationship must end because, after all, they are brother and sister. In the course of the weekend, Sinclair infers Natalie is having an affair because she was not in Nuneaton where she claimed to be (he tries to call her when the dishwasher floods the kitchen), but she still claims on her return that this was where she was. 

At Richards job, Colin is dying but Richard takes him out of hospital one last time to confront an unpleasant city planner with facts they intend to publish in their magazine. The two of them grill him and Colin eats half a sandwich, offering the other half to Richard (who takes a bite without issue) and then offering it to the city planner who is terrified to even touch it because of Colins illness. 

Things then go from bad to worse as Richard becomes obsessed with Natalie and when he turns up unexpectedly at their house he finds only Sinclair, who promptly whisks him off on a boat trip he had planned for himself and grills him on the subject of Natalies affair. Sinclair does not yet suspect Richard but will not believe that Richard doesnt know who it is, but respects his decision not to reveal the person.

On hearing that (because of the recession) Sinclair and Natalie plan to move to America, Richard loses his self-control completely, harasses Natalie in public at a hotel and then attempts suicide by taking sleeping pills, unsuccessfully, as Natalie arrives unexpectedly at his apartment. Richard is invited to their going-away party on the condition that he behave.  Richard attends with Jessica (now the editor, as Colin has died) but abandons her quickly after arriving to go on a hunt for Natalie. When he finds her, he announces he wants to kill her and  pulls her off the site and into the countryside. The two fight in the middle of a road, with Richard accusing Natalie of using him, but he then pulls her to safety when they are both nearly run over by a delivery van. Natalie then apologizes for using him and says that she and Sinclair arent leaving the country after all but wanted to have the party anyway. They return dishevelled and the guests quickly leave. Sinclair appears and, though he doesnt wish to hear their confession, he makes it quite clear that he knows what has happened, and that he forgives them. 

At the end of the film the three of them walk, together, into an autumn sunset by the river.

==Themes==

The film is largely a grand-scale re-working of Poliakoffs earlier stage play Hitting Town in that the main plot remains one of brother/sister incest, although the film also covers the chaos (as the film sees it) that was the initial stages of the London Docklands development, the late 1980s recession and attitudes towards AIDS. A parallel thread running through the movie is the rapacious replacement of the classical by the modern, represented visually by old and new buildings.

==Cast==

Alan Rickman as Sinclair, a very wealthy, intellectual, and slightly eccentric stock broker/financial analyst who marries Natalie after meeting her when she applies for a job working for him.

Clive Owen as Richard, Natalies brother and (when first encountered in 1985) a student town planner, who becomes very successful in this career, but in 1991 gives it up to work for Urban Alert - a magazine dedicated to highlighting the inadequacies and injustices of the London Docklands development. 

Saskia Reeves as Natalie, Richards beautiful elder sister. She has worked through a series of low-level administrative jobs she hated before meeting Sinclair. While married to him she becomes a partner in a recruitment agency. She and Richard were never close as children because they each lived with a different parent when their parents split up. 

Karl Johnson as Colin, Richards boss and the editor of Urban Alert. 

Lesley Sharp as Jessica, Richards co-worker whom he tries (but fails) to seduce.  

Kate Garside as Paula, a girl who works in the restaurant that Urban Alert staff usually order food from, and whom Richard has a brief relationship with. 

  of Urban Alert.

==Locations==

The film was shot mainly in London and, specifically, London Docklands with Sinclair and Natalies house being in Marlow.  The grand party that is the stage for the films climax was shot at Polesden Lacey in Great Bookham|Bookham, Surrey. The final scenes along the river are at Henley on Thames, Oxfordshire.

==DVD/video releases==
 R Certificate in the US.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 