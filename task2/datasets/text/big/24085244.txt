The End of the World (1992 film)
{{Infobox film
| name           = The End of the World
| image          = 
| caption        = 
| director       = João Mário Grilo
| producer       = Paulo Branco
| writer         = João Mário Grilo Paulo Filipe
| starring       = José Viana
| music          = 
| cinematography = Antoine Héberlé
| editing        = Carla Bogalheiro Christian Dior
| distributor    = 
| released       =  
| runtime        = 64 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
}}

The End of the World ( ) is a 1992 Portuguese drama film directed by João Mário Grilo. It was screened in the Un Certain Regard section at the 1993 Cannes Film Festival.   

==Cast==
* José Viana - Augusto Henriques
* Adelaide João - Conceição das Neves
* Zita Duarte - Violante
* Santos Manuel - Guard 1
* Heitor Lourenço - Guard 2
* João Lagarto - João
* Carlos Daniel - Carlos
* Alexandra Lencastre - Maria do Carmo
* Henrique Viana - Laureano
* Mário Jacques - Prosecutor
* Rui Mendes - Judge
* Maria João Luís - Alda

==References==
 

==External links==
* 

 
 
 
 
 
 
 