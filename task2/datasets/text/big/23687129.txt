Beck – Flickan i jordkällaren
{{Infobox film
| name           = Beck – Flickan i jordkällaren
| image          =Beck 18 - Flickan i jordkällaren.jpg
| image_size     =
| alt            = 
| caption        = Swedish DVD-cover
| director       = Harald Hamrell
| producer       = Lars Blomgren Börje Hansson Hans Peter Lundh Tomas Michaelsson
| writer         = Cecilia Börjlind Rolf Börjlind
| narrator       =
| starring       = Peter Haber Mikael Persbrandt Måns Nathanaelson Stina Rautelin
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 2006
| runtime        =
| country        = Sweden Swedish
| budget         =
| gross          =
}}
Beck – Flickan i jordkällaren is a 2006 Swedish film about the Swedish police detective Martin Beck. It was directed by Harald Hamrell.

== Cast ==
*Peter Haber as Martin Beck
*Mikael Persbrandt as Gunvald Larsson
*Måns Nathanaelson as Oskar Bergman
*Stina Rautelin as Lena Klingström
*Ing-Marie Carlsson as Bodil Lettermark
*Marie Göranzon as Margareta Oberg
*Ingvar Hirdwall as Martin Becks neighbour
*Peter Hüttner as Oljelund
*Dieter Pfaff as Sperling
*Elisabet Carlsson as Lillemor Bernér
*Lars Amble as Jan Ekman
*Marika Lindström as Ingrid Ekman
*Tomas Köhler as Nils Ekman
*Lena B. Eriksson as Ylva Johansson
*Viveca Jedholm as Karin Norgren
*Lydia Flores Garcia as Lucia Barber
*Staffan Kihlbom as Uno Mateg
*Hendrik Törling as Bernt Johansson
*Jenny Hamrell as Esmeralda
*Björn Gustavsson as "Killen som finner liket"

== References ==
* 

== External links ==
* 

 

 
 
 
 
 


 
 