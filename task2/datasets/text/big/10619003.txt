Do Anjaane
{{Infobox film
| name           = Do Anjaane
| image          = Do Anjaane.jpg
| image_size     = 
| caption        = 
| director       = Dulal Guha
| producer       = Tito
| writer         = Shafiq Ansari Nabendu Ghosh Nihar Ranjan Gupta 
| narrator       = 
| starring       = Amitabh Bachchan Rekha Prem Chopra
| music          = Kalyanji Anandji
| cinematography = M Rajaram
| editing        = Bimal Roy
| distributor    = 
| released       = January 01, 1976
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
 Vishnuvardhan and Nalini (actress)|Nalini.

==Synopsis==

Raj (Amitabh Bachchan) is found wounded on railway tracks and when he awakens he has no memory of who he is and doesnt remember anything about his life. Six years later he is living with a wealthy couple and is now named Naresh Dutt. 

One day he begins to regain his memory when he watches a film and realises that the actress in the film is related to him somehow. It is then that he discovers the actress in the film is none other than his wife Rekha Roy (Rekha). He finds out that she is now a very successful film actress changing her name to Sunita Devi and her manager is none other than Ranjeet Malik (Prem Chopra) who was once his best friend. 

In flashbacks he remembers that his real name is Raj Singh and Ranjit was the one who attempted to kill him six years earlier by throwing him off the train he was travelling on with Rekha. Raj also discovers that his young son who is now 10 years old has been sent to a boarding school and sets out a plan to regain custody of his son. First however he plans on taking revenge against Ranjit. Raj comes up with a plan to re-enter Rekha and Ranjits lives by disguising himself as a film producer. He meets with Rekha and Ranjit and offers Rekha the opportunity to act in his new film titled Do Anjaane. Rekha and Ranjit grow suspicious of Naresh Dutt as they start to realise that he bears a striking resemblance to Rekhas supposedly deceased husband Raj Singh and the storyline of his film eerily mirrors Rekhas past life. Will she discover the true identity of Naresh Dutt? and what revenge has Raj/Naresh got planned for Ranjit?

==Cast==
*Amitabh Bachchan .... Raj Singh
*Rekha .... Rekha Roy
*Prem Chopra ..... Ranjit
*Utpal Dutt .... Mr Sanyal
*Pradeep Kumar ... Amit Dutt
*Lalita Pawar .... Rajs Neighbour
*Anoop Kumar .... Rajs co-worker
*Mithun Chakraborty .... Ghanti (Amits Neighbour)

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aai Karke Singaar"
| Lata Mangeshkar
|-
| 2
| "Kahin Door Mujhe Jana Hai"
| Lata Mangeshkar
|-
| 3
| "Luk Chhip Luk Chhip Jao Na (Male)"
| Kishore Kumar
|-
| 4
| "Luk Chhip Luk Chhip Jao Na (Duet)"
| Kishore Kumar, Shivangi Kolhapure 
|}
==Awards==
*Prem Chopra won the Filmfare Best Supporting Actor Award, the only win for the film.
==References==
 

== External links ==
*  

 
 
 
 

 