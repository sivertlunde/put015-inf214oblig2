Do Lachhian
{{Infobox film name = Do Lachhian image =  image_size =  border =  alt =  caption =  director = Jugal Kishore producer = Mulk Raj Bhakhri writer =  screenplay =  story =  based on =   narrator =  starring = Sunder Satish Khairati music = Hansraj Behl cinematography =  editing =  studio = Mohan Studio distributor = released =   runtime = 105 minutes country = India language = Punjabi
|budget =  gross =
}}

Do Lachhian ( ) is a 1960 (sometimes written 1959) Punjabi film    directed by Jugal Kishore,       starring Gopal Sehgal, Daljeet, Indra Billi and Krishna Kumari in lead roles.

== Synopsis ==

Do Lachhian is a romantic drama. Two girls, Lachhi (waddhi Lachhi) and Chhoti Lachhi, the daughters of Dharmu and Karmu, respectively, are in love with Labbhu and Sohna, respectively. Labbhu and his friend Natthu are alcoholic thieves and have a little crime record. A moneylender, Bhainge Shah, also wants to marry Chhoti Lachhi, and for this purpose he manages a fight between Dharmu and Karmu and so between Lachhi and Chhoti Lachhi with the help of Labbhu and Natthu. Bhainge Shah, taking advantage of the fight, manages a marriage with Chhoti Lachhi but at the time his servant, Jagga, an admirer of Lachhis beauty, reveals the truth to Lachhi and she, apologising for their misunderstanding, saves the marriage reaching in time. Labbhu also realises his mistake and apologise. So the lovers meet.

== Music ==

The music is composed by Hansraj Behl     and lyrics written by Varma Malik   with playback by Mohammad Rafi, Shamshad Begum and Lata Mangeshkar. Some of the hit numbers include haye ni mera baalam, hai barha zalim and teri kanak di rakhi by Shamshad Begum.

;Tracklist

*Ik Pind Do Lachhian
*Haye Ni Mera Baalam, Hai Bra Zaalim
*Bhaven Bol te Bhaven Na Bol
*O Majjh Gaan Walia
*Gora Rang Na Ho Jave Kaala (Bolian style on Visakhi Mela)
*Teri Kanak Di Rakhi

== Cast ==

{|class="wikitable"
|-
!Actor/Actress
!Role
|- Gopal Sehgal||Sohna
|- Indira Billi||
|- Krishna Kumari||
|-
|Daljeet||
|- Sunder (actor)|Sunder||Natthu
|}

== See also ==
*Lachhi (1949 film)|Lachhi
*Bhangra (film)|Bhangra
*Satluj De Kandhe
*Kankan De Ohle
*Kaude Shah

== References ==
 

 
 
 