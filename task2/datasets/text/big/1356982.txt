Sabrina (1995 film)
 
{{Infobox film
| name           = Sabrina
| image          = Sabrina movie.jpg
| image_size     = 215px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Sydney Pollack
| producer       = Sydney Pollack Scott Rudin
| writer         = Barbara Benedek David Rayfiel
| starring       = Harrison Ford Julia Ormond Greg Kinnear
| music          = John Williams
| cinematography = Giuseppe Rotunno
| editing        = Fredric Steinkamp
| distributor    = Paramount Pictures
| released       =  
| runtime        = 127 minutes
| country        = Germany United States
| language       = English French
| budget         = $50 million 
| gross          = $53,458,319
}} romantic comedy-drama film adapted by Barbara Benedek and David Rayfiel. It is a remake of the 1954 film Sabrina (1954 film)|Sabrina co-written and directed by Billy Wilder that starred Humphrey Bogart, Audrey Hepburn, and William Holden, which in turn was based upon a play titled Sabrina Fair.
 John Wood, French actress Fanny Ardant.

==Plot==
Sabrina Fairchild is the young daughter of the Larrabee familys chauffeur, Thomas, and has been in love with David Larrabee all her life. David is a playboy, constantly falling in love, yet he has never noticed Sabrina, much to her dismay.

Sabrina travels to Paris for a fashion internship at Vogue (magazine)|Vogue (rather than a culinary course as in the original film) and returns as an attractive, sophisticated woman. David, after initially not recognizing her, is quickly drawn to her despite being newly engaged to Elizabeth Tyson, a doctor.

Davids workaholic older brother Linus fears that Davids imminent wedding to the very suitable Elizabeth might be endangered. If the wedding were to be canceled, so would a lucrative merger with the brides family business, Tyson Electronics, run by her father Patrick. This could cost the Larrabee Corporation, run by Linus and his mother Maude, in the neighborhood of a billion dollars.

Linus tries to redirect Sabrinas affections to himself and it works. Sabrina falls in love with him, even though she quotes others as calling Linus "the worlds only living heart donor" and someone who "thinks that morals are paintings on walls and scruples are money in Russia."

In the process, Linus also falls in love with her.  Unwilling to admit his feelings, Linus confesses his scheme to Sabrina at the last minute and sends her back to Paris. Before she gets on the plane to Paris, her father informs her that during the years of driving the father of David and Linus, he listened. When Mr. Larrabee sold, he sold and when Mr. Larrabee bought, he bought. Sabrina jokingly says "So you are telling me that you have a million dollars?" Her father says no, he has two and a quarter million dollars and that her mother would want her to have it.

Meanwhile, Linus realizes his true feelings for Sabrina, and is induced to follow her to Paris by chiding from his mother and a newly aware David, who steps into his shoes at the Larrabee Corporation. Linus arrives in Paris and reunites with Sabrina, revealing his love to her and kissing her.

==Cast==
* Harrison Ford as Linus Larrabee
* Julia Ormond as Sabrina Fairchild
* Greg Kinnear as David Larrabee
* Angie Dickinson as Ingrid Tyson
* Richard Crenna as Patrick Tyson
* Nancy Marchand as Maude Larrabee
* Lauren Holly as Elizabeth Tyson John Wood as Thomas Fairchild
* Dana Ivey as Mack
* Fanny Ardant as Irene
* Valérie Lemercier as Martine
* Paul Giamatti as Scott
* Elizabeth Franz as Joanna 
* Miriam Colon as Rosa 
* Patrick Bruel as Louis 
* Becky Ann Baker as Linda

==Production==
The music was composed by John Williams and includes a song performed by Sting (musician)|Sting; both were nominated for Academy Awards.

The location used to portray the Larrabee familys mansion was the Salutation estate, which is located on West Island in Glen Cove, New York.  This home was built around 1929 for Junius Spencer Morgan III, who was a director of the Morgan Guaranty Trust Company.  His father was J. P. Morgan, Jr., who was a banker and the son of J. P. Morgan, the renowned financier. The property is no longer owned by the Morgan family, but it is still in private hands and used as a residence.  The movie made extensive use of this mansions interiors during the filming.  

==Critical reception==
The film was a domestic Box office bomb with a result was US$53 million, primarily because it suffered from inevitable comparisons to the Sabrina (1954 film) with its trio of stars, Audrey Hepburn, Humphrey Bogart, and William Holden.  However, critics gave the film mostly mixed-to-positive reviews, with a fresh Rotten Tomatoes score of 65% based on 46 reviews.   

===Awards and nominations=== Best Original Score, Academy Award for Best Original Song ("Moonlight (1995 song)|Moonlight")
* Golden Globe 1996: Received three nominations, "Best Film - Comedy/Musical", "Best Actor -Comedy/Musical-Harrison Ford", "Best Original Song" ("Moonlight")
* Grammy 1997: Received a nomination for Best song composed for Film or TV series ("Moonlight")
* CFCA 1996: Most Promising Actor (Greg Kinnear)

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 