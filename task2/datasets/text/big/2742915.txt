Texas Chainsaw Massacre: The Next Generation
{{Infobox film
| name           = Texas Chainsaw Massacre: The Next Generation
| image          = Texas chainsaw massacre the next generation.jpg
| image_size     = 
| caption        = Original 1994 poster
| director       = Kim Henkel
| producer       = Robert Kuhn Kim Henkel  
| writer         = Kim Henkel
| narrator       =  Robert Jacks Tonie Perensky
| music          = Wayne Bell Robert Jacks
| cinematography = Levie Isaacks
| editing        = Sandra Adair
| studio         = Genre Pictures Return Productions Ultra Muchos Productions
| distributor    = New Line Cinema
| released       =  
| runtime        = 94 minutes  (original version)  87 minutes  (re-release) 
| country        = United States
| language       = English
| budget         = $600,000
| gross          = $185,898 
}}
 independent Cinema American horror film written and directed by Kim Henkel, and starring Renée Zellweger and Matthew McConaughey. The plot follows four teenagers who encounter Leatherface and his maniacal family in backwoods Texas on the night of their prom. The film is a loose remake of and quasi-sequel to the original The Texas Chain Saw Massacre (1974), which Henkel had co-written with Tobe Hooper. It has only loose connections to the previous two sequel films, which are mentioned in the films opening prologue as "two minor, yet apparently related incidents" which happened after the events of the original film.

It was released at several film festivals under the title The Return of the Texas Chainsaw Massacre in 1994 before being shelved for three years. The film was re-cut and released under the title Texas Chainsaw Massacre: The Next Generation in late summer 1997, after Zellweger and McConaughey had both become major Hollywood stars.

==Plot==
The film begins with four teenagers at their senior prom: Jenny, Heather, Barry, and Sean. Heather begins to look for her boyfriend, Barry, who is making out with another girl in the darkness. Heather discovers them and attempts to drive away in Barrys car, alongside Jenny and Sean, who are in the backseat. Barry eventually gains access into the car, where Heather scolds him angrily. Heather does not pay attention to the road and ends up wrecking with another driver, who passes out in the ensuing confusion.

The four teens decide that Sean should look after the unconscious driver, while Jenny, Heather, and Barry go look for help. While Sean looks after the driver, Heather, Barry and Jenny discover a rural real estate office occupied by Darla, a trashy insurance agent, who calls up her tow trucker boyfriend, Vilmer, to help out at the scene of the wreck. Meanwhile, Heather and Barry leave with Jenny, who loses them. 
 
Vilmer, a backwoods man with a robot leg, eventually arrives at the scene of the crash, where he snaps the unconscious drivers neck and chases Sean in his pickup, eventually running him over. Meanwhile, Heather and Barry come across a dilapidated farmhouse, where Barry looks for a way inside to use the bathroom while Heather waits on the porch. Barry is discovered by W.E., Vilmers brother. While Heather waits on the porch, Leatherface appears, playing with her hair and then chasing her. After Leatherface catches her, he stuffs her inside a meat locker. While Barry is using the restroom, he discovers human remains in the bathroom, and Leatherface bludgeons him to death with a sledge hammer. After killing Barry, Leatherface removes Heather from the meat locker, hanging her on a meathook in midair.

Jenny arrives back at the wreckage, only to find her boyfriend and the driver absent. She meets Vilmer, who shows her the bodies of Sean and the Driver. Vilmer then chases her in his truck, only for her to escape into the woods. After hiding in thicket, Jenny is attacked by Leatherface, wielding a chainsaw; he chases her through the woods, and back to the Sawyer house, where Jenny locks the door, but Leatherface begins to break it down by sawing through the wood panels. Jenny then discovers the remains of a Texas Ranger after taking refuge upstairs. She takes the Rangers gun, attempts to shoot at Leatherface, but discovers that it has no bullets. Leatherface chases Jenny back upstairs, where she jumps out a window, and onto the roof. Jenny tries to use a telephone cable to escape, but Leatherface cuts the cable and Jenny crashes through the roof of the Sawyers greenhouse. Jenny retreats back into the forest and arrives back at Darlas office, begging for help.

Walter shows up and it turns out that Darla and W.E. are in cahoots. Walter beats Jenny with an electric cattle prod, stuffs her in a gunny sack and then into Darlas trunk. W.E. leaves Darla to go pick up some pizzas she ordered earlier with Jenny in the trunk of her car. After getting the pizzas, she returns home with Jenny.

The family terrorizes Jenny, who escapes into Darlas car, but is subdued by Vilmer, who jumps on the hood of the car. Jenny wrecks and is dragged back inside the house where she is treated by Darla, who gets her ready for dinner time. Jenny falls unconscious, but awakens in the early morning hours at a dinner table, clean and redressed in a skimpy pageant gown, with a cross dressed Leatherface, the familys apparent Grandpa (possibly an elderly Drayton Sawyer), and a family of stuffed corpses. Jenny screams in terror and soon the family is joined by a mysterious Agent Rothman, who tells the family that they arent doing their "jobs" correctly. Rothman makes an idle threat to Vilmer, and then takes his leave. After Vilmer crushes Heathers skull in anger, Jenny tries to escape, but is held down by Vilmer as Leatherface revves his chainsaw, preparing to behead Jenny.

As Vilmer holds Jenny down, and Leatherface makes pathetic attempts to decapitate her, she manages to snatch Vilmers remote to his cybernetic leg and uses it to dislocate his knee as an advantage to escape, which she succeeds in doing. Jenny escapes to the main road, with Leatherface in hot pursuit, where she is helped by an elderly couple, but the couples RV is soon turned over by Vilmer and Leatherface. Jenny climbs out of the wreckage, barely harmed, and Leatherface and Vilmer pursue her on foot. An apparent "Order of the Illuminati" airplane operated by one of Rothmans colleagues, swoops over head and the blade grazes Vilmers skull, ultimately killing him. Jenny watches as Vilmer finally dies while Leatherface screams in horror and frustration, grieving the loss of his elder brother.

A black limousine appears, and Jenny enters it only to discover Rothman inside. Rothman tells Jenny that her experience was supposed to be spiritual, but that it went awry and that Vilmer had to be stopped. She is dropped off at a hospital, where she sees a mysterious blonde woman being wheeled down a corridor as police question her.

Meanwhile, Leatherface swings his chainsaw in frustration, where the screen cuts to black and the credits roll on.

==Cast==
{{columns-list|2|
* Renée Zellweger as Jenny 
* Matthew McConaughey as Vilmer Slaughter  Robert Jacks as Leatherface 
* Tonie Perensky as Darla Slaughter 
* Joe Stevens as Walter Slaughter 
* Lisa Marie Newmyer as Heather
* Tyler Shea Cone as Barry 
* John Harrison as Sean
* James Gale as Rothman
* Vince Brock as "Im Not Hurt"
* Chris Kilgore as Rothmans Chauffeur 
* Susan Loughran as Jennys Mother
* David Laurence as Jennys Stepfather
* Grayson Victor Schirmacher as Grandfather
* Jeanette Wiggins as Woman Eating Chocolates 
* John Dugan as Cop at Hospital*
* Paul A. Partain as Hospital Orderly*
* Marilyn Burns as Patient on Gurney/Sally Hardesty*
* Debra Marshall as Cop in Buds Pizza* cameo 
}}

==Production==
In developing the film, Robert Kuhn stated:  I wanted to go back to the original, and   did, too. We agreed on that right off. And the first major thing was getting him to write the script. I raised the money to get it written, and for us to start trying to put this thing together. Then we went out to the American Film Market in LA and talked to a bunch of people about financing. At that point Id raised some money, but not nearly enough to make the film, and we looked at the possibilities of making a deal with a distributor. But I knew there wasnt any hope of us making one we could live with. There never is. Kim would say, Hey, so-and-so is interested, and it might be a deal we can live with. So wed talk to em and Id ask three or four hard questions, and Id just kind of look over at Kim and hed say Yeah. Then Id go back and start trying to raise some more money. I just started going to everybody I knew and I got it in bits and pieces, wherever I could.  
 Bastrop    in 1994 on a budget of $600,000.    The majority of the cast and crew were locals from Austin, aside from David Gale, a stage actor from Houston. 

==Release==
The film had a very rough and complicated release history, including re-editing and re-issue into cinemas (thus the film has different versions and alternate titles). The process occasioned disputes between the filmmakers and distributors at Columbia Pictures.
 South by Southwest Film and Media Conference in 1995,  and later that year the film saw its first home video release in the form of a LaserDisc released in Japan.  Prior to this, during the films post-production stage, Columbia Pictures reportedly signed to distribute the film theatrically (along with its home video|home-video release) in October 1995, and agreed to spend no less than $500,000 on prints and advertising.   20 October 1997 (article retrieved from AC FilmVault 10 July 2009)   Cinepix Film Properties back in 1993.

  terrorizes  

In a 1997 interview with The Austin Chronicle, Robert Kuhn stated that:

{{cquote|Well, we definitely feel that Columbia/TriStar has not done what they agreed to do in terms of trying to market this film in the best possible fashion. They have not tried to exploit this film to monetarily benefit us as they should have. Theyve just low-keyed it. They dont want to be guilty of exploiting Matthew because of their relationship with CAA, which is the strongest single force in Hollywood these days. You get on the wrong side of them, youre in trouble. So I understand their problem, but at the same time, they should have either given the film back to us or they should have done the best release they could have done. And they havent done that. 
}}

Eventually, the film reached the big screen in a   on VHS in September 1998, and did not receive a DVD release until July 13, 1999. The original Columbia/Tristar DVD release has since been reissued with alternative cover art.

==Critical reception==
Texas Chainsaw Massacre: The Next Generation received mostly negative reviews.  Mike Clark of USA Today called it "The kind of cinematic endeavor where you suspect both cast and crew were obligated to bring their own beer,"  while Owen Gleiberman wrote in Entertainment Weekly that the film "recapitulates the absurdist tabloid-redneck comedy of the great, original Chainsaw without a hint of its primal terror."  Janet Maslin of The New York Times said: "It was way back in 1995 that this schlocky horror farce, then known as "Return of the Texas Chainsaw Massacre," first appeared with the unknown actors Matthew McConaughey and Renee Zellweger in starring roles. But even in a film whose principal props include litter, old pizza slices and a black plastic trash bag, its clear that these two were going places." 

The film did receive some positive reviews, however: John Anderson of the Los Angeles Times referred to the film as "  giddy mix of gruesome horror and campy humor," while Joe Leydon of Variety (magazine)|Variety said the film "manages the difficult feat of being genuinely scary and sharply self-satirical all at once... it is adept at keeping its audience in a constant state of jumpiness." He also lauded Zellwegers performance, calling her "the most formidable scream queen since Jamie Lee Curtis went legit."  The Austin Chronicle also gave the film a positive review, stating: "Writer-director Kim Henkel penned the original Chainsaw and this effort shows that he still has a felicitous grasp of the things that cause us to shudder in dread." 

The movie was nominated for a Stinkers Bad Movie Awards for "The Sequel Nobody Was Clamoring For". 

==Alternate versions==
The original, unedited cut of the film features a few differences from the re-issued 1997 cut of the film, including:

* a subplot that involves Jennys stepfather abusing her in the opening scene
* more dialogue between Heather and Barry in the car
* a longer conversation between Jenny and Darla in the bath-room

The original cut also featured different musical effects, a handful of different transitional shots, as well as a few scenes tinted different colors. 

The most widely available cut of the film, titled Texas Chainsaw Massacre: The Next Generation, does not contain this footage. The Canadian DVD release of the film through Lions Gate Entertainment remains the only known home-video release   that includes all of the cut footage from the original version of the movie. The version titled The Return of the Texas Chainsaw Massacre runs for 95 minutes, while the version titled Texas Chainsaw Massacre: The Next Generation runs for only 86 minutes; a nine-minute difference.

The versions of the film available differ from country to country, but Herald Videogram released the original Return cut on Laserdisc in Japan.

==Soundtrack==
 
The films sound-track featured many local Texan bands, and never got a full CD release. However, star Robert Jacks, a friend of  , featuring Debbie Harry on the cover with a portrait of Jacks as Leatherface, featured in his three costumes, on the wall behind her.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 