Nishabd
{{Infobox film
| name           = Nishabd
| image          = Nishabd.jpg
| caption        = Movie poster for Nishabd
| writer         =
| starring       = Amitabh Bachchan Jiah Khan Aftab Shivdasani Revathi Rukhsar
| director       = Ram Gopal Verma
| producer       = Ram Gopal Verma
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| language       = Hindi
| country        = India
| music          = Vishal Bhardwaj
| budget         =
}} American Beauty.

== Synopsis ==
The film opens with young Ritu (Shradha Arya) bringing her friend Jia (Jiah Khan) to spend the holidays with her at her home in Kerala. Ritus parents Vijay (Amitabh Bachchan) and Amrita (Revathi) have a beautiful home surrounded by picture-perfect surroundings. Vijay is a photographer and Amrita is a homemaker.

Jias mother lives alone in Australia (Jias parents are divorced), and she studies in India. She is a free-spirited teenager with no attachments and no worries. She professes some affinity to Vijay, but it doesnt go much further than that. But Vijays world is turned upside down when he takes pictures of Jia watering herself down with the garden hose. Something innocent grows into something bigger, and something bigger grows into something beyond control.

Jias world collides with Vijays causing three casualties. Ritu witnesses an intimate moment between Vijay and Jia and tries to get Jia out of the house without revealing the truth to her mother but fails. Around this time, Amritas brother Shridhar (Nassar) visits them. Through a sequence of events, he uncovers the disturbing truth about Jia and Vijay. Shridhar questions Vijay and realises that things are serious between Jia and him.Vijay confesses his love for Jia to Amrita, leaving her shattered.

==Cast==
* Amitabh Bachchan as Vijay
* Jiah Khan as Jia
* Revathi as  Amritha
* Nassar as Shridhar
* Aftab Shivdasani as Rishi

== Production  ==
The film was shot in 20 days.  The movie was shot at Munnar in Kerala.  The claim that it was inspired by Vladimir Nabokovs Lolita was denied by Amitabh Bachchan in the CNN IBN TV show Unspoken relationships - Nishabd special (3 March 2007).

==Reception==

The film has received mixed reviews from critics. Praise has been given to the performances of the lead actors, however the plot and script have been subject to criticism.

==Awards and nominations==
Khan was nominated for the Filmfare Award for Best Female Debut in 2007 for her performance in Nishabd

==See also==
*Joggers Park
*Cheeni Kum American Beauty 
*Anokha Rishta

== References ==
 

== External links ==
*  
*   at  
*  

 

 
 
 
 