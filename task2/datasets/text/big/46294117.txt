The Gift (2015 film)
{{Infobox film
| name           = The Gift
| image          = The Gift 2015 Film Poster1.png
| alt            = 
| caption        = 
| director       = Joel Edgerton
| producers      = Jason Blum   Joel Edgerton   Rebecca Yeldham
| writer         = Joel Edgerton
| starring       = Jason Bateman   Rebecca Hall   Joel Edgerton
| music          = 
| cinematography = Eduard Grau
| editing        = Matt Chesse   Luke Doolan
| production companies = Blue-Tongue Films   Blumhouse Productions
| distributor    = STX Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

The Gift is an upcoming American thriller film written and directed by Joel Edgerton. The film stars Jason Bateman, Rebecca Hall and Edgerton. The film is set to be released domestically by STX Entertainment on July 31, 2015.

== Plot ==
Simon and Robyn are a young married couple whose life is going just as planned until a chance encounter with an acquaintance of Simon’s from high school sends their world into a tailspin. Simon doesn’t recognize Gordo at first, but after a seemingly coincidental series of encounters proves troubling, a terrible secret from their past is uncovered. As Robyn learns the truth about what happened between Simon and Gordo, she begins questioning how well she really knows her husband.   

== Cast ==
* Jason Bateman as Simon 
* Rebecca Hall as Robyn 
* Joel Edgerton as Gordo 

== Production == The Square (2008) and Felony (2013 film)|Felony (2013).   

Principal photography on the film began on January 19, 2015,  and ended on February 23, 2015. 

==Trailer==
The first trailer for The Gift was released on April 1, 2015.  It premiered on Twitters live video streaming app Periscope (app)|Periscope, making STX Entertainment the first advertiser to work with Twitter on a campaign incorporating Periscope. 

== Release ==
STX Entertainment set The Gift for domestic release on July 31, 2015. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 