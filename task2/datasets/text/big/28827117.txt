Dim Sum Funeral
{{Infobox film
| name           = Dim Sum Funeral
| image size     =
| image	=	Dim Sum Funeral FilmPoster.jpeg
| alt            =
| caption        =
| director       = Anna Chi
| producer       = Clark Peterson
| writer         = Donald Martin
| screenplay     =
| story          =
| narrator       =
| starring       = Kelly Hu Bai Ling Russell Wong Steph Song Lisa Lu Julia Nickson
| music          = Scott Starrett
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =   AFI Fest
| runtime        = 95 minutes
| country        = Mandarin
}}
Dim Sum Funeral is a 2008 comedy/drama film. It was directed by Anna Chi  and stars Kelly Hu, Bai Ling, Russell Wong, Steph Song and Talia Shire.

== Plot ==
After their mother dies, four Chinese-American siblings return to their Seattle home.  As their mother wanted a traditional seven-day funeral, the estranged siblings stay the week and deal with their issues. 

== Cast ==
;Xiao family
*Lisa Lu as Mrs. Xiao, the matriarch
*Julia Nickson as Elizabeth ("Liz"), a daughter of Mrs. Xiao
*Russell Wong as Alexander ("Alex"), the son of Mrs. Xiao
*Françoise Yip as Victoria, a daughter of Mrs. Xiao
*Steph Song as Meimei, a daughter of Mrs. Xiao

;Others
*Talia Shire as Viola, a Jewish friend of Mrs. Xiao
*Bai Ling as Dede, Meimeis girlfriend
*Curtis Lum as Bruce Lee, a Buddhist monk and prospective sperm donor to Meimei and Dede
*Adrian Hough as Michael, Lizs husband
*Isaah Brown as Jason, Victorias son
*Tseng Chang as Zhou Lin, a friend of Mrs. Xiao, an eminent Chinese pianist, and Meimeis father
*Kelly Hu as Cindy, Alexs wife and a former Miss Taiwan
*Valerie Tian as Emily, Alexs daughter

==Reception==
Dim Sum Funeral was widely panned by critics.  Fionnuala Halligan of Screen Daily noted the films "uneven tone", but she commended the set design and decoration by James Willcock. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 