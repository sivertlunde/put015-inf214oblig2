The Reluctant Saint
{{Infobox film
| name = The Reluctant Saint
| image = Reluctantsaint.jpg
| caption = VHS cover for the film
| writer = John Fante Joseph Petracca
| starring = Maximilian Schell Ricardo Montalban Lea Padovani Akim Tamiroff Harold Goldblatt Arnoldo Foa Mark Damon Luciana Paluzzi Carlo Croccolo Giulio Bosetti Elisa Cegani
| director = Edward Dmytryk
| producer = Dmytryk-Weiler Production
| music = Nino Rota
| distributor = Davis/Royal Films International
| released = 1962
| runtime = 105 min.
| language = English
| budget =
}}
 mystic who is honored as a saint by the Catholic Church. It stars Maximilian Schell as Joseph, as well as Ricardo Montalban, Lea Padovani, Akim Tamiroff, and Harold Goldblatt. The movie was written by John Fante and Joseph Petracca and directed by Edward Dmytryk.
 levitation and intense ecstasies. This movie follows Joseph (Maximilian Schell) from his final days living at home with his mother (Lea Padovani). Because of his slow wits, she has kept him in school despite his being a grown man, older than the other students. He is seen bearing patiently and good-heartedly the ridicule of his fellow villagers, and enduring failed attempts at work as a laborer. At the insistence of his mother (who saw no viable alternatives), we see him joining a Franciscan friary through the influence of his uncle (Harold Goldblatt), an authority in the religious order. But trouble follows Joseph wherever he goes, including the friary, because of his slow wits. Eventually his good heart is noticed by a visiting bishop (Catholic Church)|bishop, (Akim Tamiroff), who orders that he be trained to be a Catholic priest|priest.

Despite Josephs incapacity for the necessary scholarly studies, and preference for just managing the sheep and other animals in the friarys stable, Joseph does become a priest. Although he learns little from the tutoring by the friars, Joseph passes the necessary examinations for the priesthood through a series of unlikely or possibly miraculous events. Soon after, when Joseph is seen levitating at the time of his ecstatic prayers to the Virgin Mary, one of the superiors in the community, (Ricardo Montalban), claims that Joseph suffers from demonic possession. Joseph is therefore exorcism|exorcised, but his levitations continue, persuading everyone--including his former critic--of the divine origins of his levitations.

Most of the key events in the movie are based on historical events or reports about the life of St. Joseph of Cupertino.

==External links==
* 
*Hollywood.com listing: http://www.hollywood.com/movies/detail/id/254326
 
 
 
 
 
 
 
 
 
 
 
 

 
 