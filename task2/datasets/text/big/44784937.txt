Annavra Makkalu
{{Infobox film|
| name = Annavra Makkalu
| image = 
| caption =
| director = Phani Ramachandra
| writer = Phani Ramachandra
| starring = Shivrajkumar  Maheshwari   Reman Singh
| producer = Y. R. Jayaraj
| music = Rajesh Ramanath
| cinematography = B. S. Basavaraj   S. V. Srikanth
| editing = S. Manohar
| studio = Shivashakti Productions
| released =  
| runtime = 151 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada action action drama film directed by Phani Ramachandra and produced by Y. R. Jayaraj. The film features Shivarajkumar, in triple roles along with Maheswari, Reman Singh  and Suneha, all making their debuts, in the lead roles.  

The films score and soundtrack was scored by Rajesh Ramanath and the stunts were choreographed by Thriller Manju.

== Cast ==
* Shivarajkumar 
* Maheshwari
* Reman Singh
* Suneha
* Charan Raj
* Srinath
* Jai Jagadish
* M.N Lakshmi Devi
* Shankar Ashwath
* Pramila Joshai
* Padma Vasanthi
* Sundar Raj
* Honnavalli Krishna
* Master Anand

== Soundtrack ==
The soundtrack of the film was composed by Rajesh Ramanath. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Baaro Baaro Nanna Shiva
| extra1 = Manjula Gururaj
| lyrics1 = 
| length1 = 
| title2 = Hogabeda Hudugi Nanna Bittu
| extra2 = Rajesh Krishnan & Chandrika Gururaj
| lyrics2 = 
| length2 = 
| title3 = Oh Jaana Neene Nanna
| extra3 = S. P. Balasubrahmanyam, Swarnalatha
| lyrics3 = 
| length3 = 
| title4 = Aadabeku Raja 
| extra4 = Rajesh Krishnan , Swarnalatha
| lyrics4 = 
| length4 = 
| title5 = Annavra Makkalu Naavu
| extra5 = S. P. Balasubrahmanyam, Rajesh Krishnan 
| lyrics5 = 
| length5 = 
}}

== References ==
 

== External links ==
*  

 
 
 
 
 


 

 