Saturday's Millions
{{Infobox film
| name           = Saturdays Millions
| caption        = 
| image	         = 
| director       = Edward Sedgwick
| producer       = 
| writer         = Dale Van Every Robert Young Grant Mitchell William Kent
| music          = 
| cinematography = 
| editing        = 
| studio         = Universal Pictures
| distributor    = 
| released       = 1 October 1933
| runtime        = 75 mins
| country        = United States
| language       = English
| budget         = 
}}
Saturdays Millions is a 1933 American film. Alan Ladd has an uncredited role.

==Plot==
Jim Fowler is Western Universitys football hero and is constantly besieged by reporters. Jims father Ezra comes to visit him and becomes reacquainted with an old Western football chum, Mr. Chandler, who happens to be the father of Jims girlfriend Joan. 

Jim keeps his roommate, Andy, busy by sending him to collect money on their laundry concessions business, even though Andy is desperately trying to meet his girlfriend Thelma, who has just come for a visit. When the coach tells Chandler and Fowler that Jim is nervous and erratic, Chandler invites Jim to spend the night before the big game at his home. 

After-dinner conversation reveals that Jim sees football as merely a business, and feels devalued by his popularity because he thinks people are only interested in him because of football, not for who he is. Joan is disillusioned that Jim treats football as a racket, and the fathers are disappointed because they sincerely love the game.

==External links==
*  at IMDB
*  at TCMDB

 
 
 
 
 
 

 