Terror (1977 film)
{{Infobox film
| name           = Terror
| image          = 
| caption        = 
| director       = Gert Fredholm
| producer       = Erik Crone Jørgen Lademann Jørgen Schmidt Nicolaisen Hans Hansen Torben Nielsen
| starring       = Bo Løvetand
| music          = 
| cinematography = Morten Arnfred
| editing        = Anders Refn
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Terror is a 1977 Danish crime film directed by Gert Fredholm and starring Bo Løvetand.

==Cast==
* Bo Løvetand - Boy Johnny Olsen - Trigger
* Ole Meyer - Schatz
* Jess Ingerslev - Benny
* Poul Reichhardt - Kriminalassistent Ancher
* Ole Ernst - Kriminalassistent Brask
* Ulf Pilgaard - Kriminalassistent Rieger
* Holger Juul Hansen - Kriminalkommisær Runge
* Knirke - Kate
* Ingolf David - Streichner
* Henning Palner - Direktør Ejlersen
* Lykke Nielsen - Direktør Ejlersens sekretær Jette
* Beatrice Palner - Schatz mor
* Aksel Erhardtsen - Schatz far
* Inger Stender - Schatz fars sekretær
* Lise Thomsen - S-togspassager
* Lone Helmer - Cafeteriadame
* Ib Tardini - Cafeteriamedhjælper
* Ulla Jessen - Bennys fars veninde
* Ulla Koppel - Dame i pølsebod
* Bent Warburg
* Mogens Hermansen
* Lillian Tillegren
* Arne Westermann

==External links==
* 

 
 
 
 
 


 
 