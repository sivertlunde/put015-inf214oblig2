Judwaa
 
 
{{Infobox film
| name           = Judwaa
| image          = Judwaa1.jpg
| caption        = DVD Cover
| director       = David Dhawan
| producer       = Sajid Nadiadwala
| story          = E. V. V. Satyanarayana
| sponsor        = Dr Rohit Munjal Rambha
| music          = Anu Malik
| cinematography = W. B. Rao
| editing        =
| studio         =
| distributor    = Nadiadwala Grandsons Entertainment
| released       =  
| runtime        = 138 mins
| country        = India
| language       = Hindi
| budget         = 8 crore 
| gross          = 30 crores  
}} Indian Bollywood Telugu movie Hello Brother, which is based on Jackie Chans movie Twin Dragons.

==Plot==
Mrs. Malhotra (Reema Lagoo) is the mother of twins, who were separated at birth, and she only has one son. Mrs. Malhotra goes into shock and then is restricted to a wheelchair. She resides with her husband and son in the US. Quiet and mature Prem, (Salman Khan) one of the twins, and his dad (Dalip Tahil) return to India together with Mrs. Malhotra to get Prem married. Prems future fiance is the outgoing and precocious Mala (Karishma Kapoor), who is the daughter of Mr. Sharma (Kader Khan). There Prem meets with his twin brother very outgoing and precocious Raja (also Salman Khan), who looks like him, but is exactly the opposite in nature. A series of comic scenes result as a result of the twins getting together. Prem loves quiet and mature Roopa (Rambha (actress)|Rambha) and Raja loves Mala (Karishma Kapoor), and both the girls are confused by the change they perceive in their respective lovers. To make matters worse, Tiger Ratanlal (Mukesh Rishi) has sworn vengeance against the Malhotra family for what they did to his dad (Deepak Shirke).

==Cast==
* Salman Khan as Raja / Prem Malhotra
* Karisma Kapoor as Mala
* Rambha   as Roopa Bindu as Roopas mom
* Kader Khan as Sharma (Malas dad)
* Shakti Kapoor as Rangeela
* Anupam Kher as Inspector / Havaldar Vidyarthi
* Satish Shah as Havaldar / Inspector
* Mukesh Rishi as Ratanlal Tiger
* Dalip Tahil as S.P Malhotra,Prem and Rajas Father
* Reema Lagoo as Prems mom
* Tiku Talsania as Sharmas Brother-in-law
* Dinesh Hingoo as a Jeweller (Guest Appearance)
* Ram Sethi as Waiter in Hotel (Guest Appearance)

==Box office==
According to Box Office India, it had an excellent opening in 1997, grossed approx 30 crores and declared a  Big hit  by Box Office India. 

== Music ==
{{Infobox album|
| Name = Judwaa
| Type = Soundtrack
| Artist = Anu Malik
| Cover =
| Released =   1997 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  Venus Records
| Producer = Anu Malik
| Reviews =
| Last album = Namak (1996 film)|Namak (1996)
| This album = Judwaa (1997)
| Next album = Auzaar (1997)
}}

The soundtrack of the film contains 6 songs. The music is composed by Anu Malik, with lyrics authored by Dev Kohli.
{| class="wikitable"
|-
! # !! Title !! Singer(s) !! Length
|-
| 1
| "Duniya Mein Aaye"
| Kumar Sanu, Kavita Krishnamurthy
| 06:02
|-
| 2
| "East Aur West India Is The Best"
| Anu Malik
| 07:04
|-
| 3
| "Oonchi Hai Building" Poornima
| 05:10
|-
| 4
| "Tan Tana Tan Tan Tan Tara"
| Abhijeet Bhattacharya|Abhijeet, Poornima
| 06:39
|-
| 5
| "Tera Aana Tera Jaana"
| Kumar Sanu, Kavita Krishnamurthy
| 04:53
|-
| 6
| "Tu Mere Dil Mein Basja"
| Kumar Sanu, Poornima
| 04:46
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 