Aadyakiranangal
{{Infobox film
| name = Aadyakiranangal
| image =
| caption =
| director = P. Bhaskaran
| producer = P Bhaskaran V Abdulla
| writer = Parappurathu Thoppil Bhasi (dialogues)
| screenplay = Thoppil Bhasi Sathyan Madhu Madhu Ambika Ambika K. R. Vijaya
| music = K. Raghavan
| cinematography = U Rajagopal
| editing = R Venkattaraman
| studio = Chithra Sagar
| distributor = Chithra Sagar
| released =   
| country = India Malayalam
}} 1964 Cinema Indian Malayalam Malayalam film, Ambika and K. R. Vijaya in lead roles. The film had musical score by K. Raghavan.   The film won National Film Award for Best Feature Film in Malayalam.  

==Cast==
   Sathyan as Kunjukutty  Madhu as Pappachan  Ambika as Gracy 
*K. R. Vijaya as Marykutty 
*Adoor Bhasi as Krishnan Ashan 
*Jose Prakash as Damodaran 
*P. J. Antony as Kariyachan 
*T. R. Omana as Pennukunju 
*Gopi
*Thoppil Bhasi as Pulayan 
*Adoor Pankajam as Kunjeli 
*Bahadoor as Velu 
*Elizabeth as Eliyamma 
*Haji Abdul Rahman as Appukuttan 
*Kambissery Karunakaran as Kuttichan 
*Kedamangalam Ali as Kuriachan 
*Kunjandi as Paappi 
*Kuthiravattam Pappu 
*Master Ajith P Bhaskaran as Joyimon 
*Philomina as Annamma 
*S. P. Pillai as Avaran 
*Sunny as Sunny 
*Vasu Pradeep as Thevan 
 

==Soundtrack==
The music was composed by K. Raghavan and lyrics was written by P. Bhaskaran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aanachaal Naattilulla || Adoor Bhasi, Chorus, Kuthiravattam Pappu || P. Bhaskaran || 01:14
|- 
| 2 || Bhaarathamennaal || P Susheela, Chorus || P. Bhaskaran || 03:06
|- 
| 3 || Car Lorreel Keri || Adoor Bhasi || P. Bhaskaran || 00:14
|- 
| 4 || Kallupaalathil Kariyaachan || Adoor Bhasi || P. Bhaskaran || 00:41
|- 
| 5 || Kalyaanamothiram || P. Leela || P. Bhaskaran || 03:25
|- 
| 6 || Kannoor Dharmadam || Adoor Bhasi, Chorus || P. Bhaskaran || 00:50
|- 
| 7 || Kizhakkudikkile || AP Komala || P. Bhaskaran || 03:04
|- 
| 8 || Malamoottil Ninnoru || K. J. Yesudas || P. Bhaskaran || 02:54
|- 
| 9 || Manjulabhaashini Baale || Adoor Bhasi || P. Bhaskaran || 00:54
|- 
| 10 || Oonjaale Ponnoonjaale || P. Leela || P. Bhaskaran || 03:20
|- 
| 11 || Pathivaayi Pournami || P Susheela || P. Bhaskaran || 04:21
|- 
| 12 || Shanka Vittu Varunnallo || Adoor Bhasi || P. Bhaskaran || 00:48
|}

==References==
 

==External links==
*  

 

 
 
 


 