Culloden (film)
 
{{Infobox film
| name           = Culloden
| image          = Culloden title.jpg
| image_size     =
| caption        = The title card
| director       = Peter Watkins
| producer       =
| writer         = Peter Watkins
| narrator       =
| starring       =
| music          =
| cinematography = Dick Bush
| editing        = Michael Bradsell
| distributor    =
| released       = 15 December 1964 (UK)
| runtime        = 69 min.
| country        = United Kingdom
| language       = English, Scottish Gaelic
| budget         =
}}
 TV war reporting. The film was based on John Prebbles study of the battle.   

==Production==
Culloden was Watkinss first full-length film. It was also his first use of his docudrama style in which actors portray historical characters being interviewed by filmmakers on the scene as though it was happening in front of news cameras. The film was produced on a low budget, with only a handful of extras and a single cannon. Watkins made use of carefully planned camera angles to give the appearance of an army.   

Watkins also "wanted to break through the conventional use of professional actors in historical melodramas, with the comfortable avoidance of reality that these provide, and to use amateurs—ordinary people—in a reconstruction of their own history." He accordingly used an all-amateur cast from London and the Scottish Lowlands for the Hannoverian forces, and people from Inverness for the Jacobite army. This later became a central technique of Watkinss filmmaking.

According to an estimate by the cinematographer for the film, Dick Bush, about 85% of all camerawork in Culloden was hand-held.   This newsreel-style shooting gave an already gritty reality a sense of present action.  Culloden looked like a documentary of an event which occurred before the camera was invented. From this the film illustrates the recognizable documentary style of cinéma vérité. 

==Reception==
Culloden won in 1965 both a Society of Film and Television Arts (BAFTA) TV Award for Specialised Programmes   and the British Screenwriters Award of Merit. In a list of the 100 Greatest British Television Programmes drawn up by the British Film Institute in 2000, voted for by industry professionals, Culloden was placed 64th.  Writing for Eye for Film, Amber Wilkinson praised Culloden, commenting that "the mastery of   direction is obvious from first to last". 

==Production crew==
*Production design - Anne Davey, Colin MacLeod, Brendon Woods
*Makeup artist - Ann Brodie
*Sound department - John Gatland, Lou Hanks
*Production unit -  Rodney Barnes, Valerie Booth, Roger Higham, Jennifer Howie, Michael Powell	
*Historical advisor - John Prebble
*Production unit - Geraldine Proudfoot, Geoff Sanders
*Battle coordinator - Derek Ware

==See also==
*Drama documentary
*The Highlanders (Doctor Who)

==References==
;Notes
 

;Further reading
* Culloden, John Prebble, Atheneum 1962
* Peter Watkins commentary on his first BBC documentary:  
*   from Peter Watkins website
* British Film Institute, Culloden, "Background and Context",  
* Culloden entry in the BFI TV-100, 2000 list,  

== External links ==
*  

 

 
 
 
 
 
 