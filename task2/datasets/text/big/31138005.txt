The Rising Generation
{{Infobox film
| name           = The Rising Generation
| image          =
| caption        =
| director       = George Dewhurst   Harley Knoles 
| producer       = 
| writer         = Wyn Weaver (play)   Laura Leycester (play)   Dion Titheradge
| starring       = Alice Joyce  Jameson Thomas   Robin Irvine   William Freshman
| music          = 
| cinematography = 
| editing        = George Dewhurst
| studio         = Westminster Films
| distributor    = Williams and Pritchard Films
| released       = September 1928
| runtime        = 7,000 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}}
  British silent silent comedy film directed by Harley Knoles and George Dewhurst and starring Alice Joyce, Jameson Thomas and Robin Irvine.  It was based on a play by Laura Leycester. A couple masquerade as servants.

==Cast==
* Alice Joyce - Mrs. Kent
* Jameson Thomas - Major Kent
* Robin Irvine - George Breese
* William Freshman - Robert Kent Joan Barry - Peggy Kent
* Betty Nuthall - School Friend
* Gerald Ames - John Parmoor
* Gerald Rawlinson - Augustus
* Pamela Deane - Friend
* Eric Findon - Friend
* Eugenie Prescott - Maid
* Clare Greet - Cook
* Nervo and Knox - Themselves

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 
 