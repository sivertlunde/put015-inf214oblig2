Bedre enn sitt rykte
{{Infobox film
| name           = Bedre enn sitt rykte
| image          = 
| image size     =
| caption        = 
| director       = Edith Carlmar
| producer       = 
| writer         = Otto Carlmar   Eva Seeberg
| narrator       =
| starring       = Vigdis Røising   Kari Nordseth   Magne Bleness   Atle Merton
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 12 September 1955 (Norway)
| runtime        = 87 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama film directed by Edith Carlmar, starring Vigdis Røising, Kari Nordseth, Magne Bleness and Atle Merton. The story is based on a novel by Eva Seeberg.

Dag (Bleness) is finishing school, and needs a top grade to get into medical school. He takes private lessons with the French teacher (Nordseth), and a romantic relation develops between the two. Karin (Røising) is in love with Dag, and waits for the relationship to end, while Roald (Merton) is in love with Karin.

==External links==
*  
*   at Filmweb.no (Norwegian)

 
 
 
 