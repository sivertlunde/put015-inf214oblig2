Animal Farm (1999 film)
{{Infobox television film
| name           = Animal Farm
| image          = tt0204824.jpeg
| caption        = Theatrical poster
| director       = John Stephenson Greg Smith  Robert Halmi
| writer         = Alan Janes Martyn Burke (teleplay) George Orwell  (novel)
| narrator       =
| starring       = Kelsey Grammer Ian Holm Julia Louis-Dreyfus Patrick Stewart Julia Ormond Paul Scofield Pete Postlethwaite Peter Ustinov
| music          = Richard Harvey
| cinematography = Mike Brewster Colin Green Hallmark Films
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $23 million
| preceded by    =?
| followed by    =?
}} Hallmark Films. novel of the same name. The film tells the story of farm animals successfully revolting against their human owner, only to slide into a more brutal tyranny among themselves.

==Plot==
 
As a storm washes away the ruins of Manor Farm, Jessie, a female Border Collie, Benjamin the donkey, Muriel the goat, and other farm animals make their return to the farm after years of hiding in the wilderness. Jessie reflects on the events that led them to their current situation.  The film flashes back years earlier.
 Old Major, the oldest pig at Manor Farm, holds a meeting with the animals in the barn.  Old Major tells the animals that mankind is their enemy, for they serve and provide for mankind without reward. Old Major teaches the animals the song, “Beasts of the World,” which proclaims that animals must overthrow man in order to be free and equal. The meeting is interrupted when Jones stumbles outside the barn and accidentally fires his gun, killing Old Major. When Jones neglects to feed the animals, Boxer (Animal Farm)|Boxer, the work horse, leads the animals to the food shed, and the pigs lead a revolution against Jones, his wife, and the farm workers, forcing them off the property.

Under the rule of animals, Manor Farm is renamed Animal Farm by Snowball (Animal Farm)|Snowball, a pig who has learned to read and write. Snowball paints on the barn doors what he calls the seven commandments of Animalism, which, in accordance with Old Majors views, forbid animals from behaving like humans or killing other animals. The final commandment reads, "All animals are equal." Snowball teaches the animals to chant, "Four legs good, two legs bad" and also reveals the “Hoof and Horn,” a flag that represents Animal Farm. Napoleon, another pig, declares that the farmhouse is to be preserved as a museum, and the three pigs (Snowball, Napoleon, and Squealer (Animal Farm)|Squealer) oversee the farms operation. Napoleon also takes Jessies puppies from her, claiming that it is best for them to receive an education from him.  Snowball, when questioned by the farm animals, confesses that he and the other pigs have taken the farms milk and apples for themselves. Squealer explains that the pigs well-being takes priority because they are the brains of the farm. Jessie is the only one unconvinced.

Having learned that Jones has lost control of his farm, Frederick leads an invasion into Animal Farm with other local farm workers. Snowball has planned for such an invasion and leads the animals to victory, causing the humans to retreat. In his defeat, Frederick considers working with the animals instead.

Snowball proposes that the animals build a windmill to improve their operations, but Napoleon opposes the plan. When the animals show support for Snowball, Napoleon calls Jessies puppies, now grown dogs trained as his private army, to chase Snowball out of Animal Farm and leaving his fate unknown. Napoleon declares Snowball a "traitor and a criminal," and Squealer claims that the windmill was Napoleons plan all along (leaving the animals unaware that Napoleon and Squealer are evil and they are the real traitors). Napoleon declares the pigs will now "decide all aspects of the farm." When Frederick begins to trade with the pigs, Boxer remembers Old Major mentioning that animals were not to engage in trade. Napoleon explains that "Animal Farm cannot exist in isolation." Napoleon has the skull of Old Major placed in front of the barn to oversee the farms progress, and has a statue of himself erected nearby. Jessie confesses to the other animals that she witnessed the pigs living in the house and sleeping in the beds. Squealer explains that no commandment has been broken. He had, in fact, altered the commandment, ‘No animal shall sleep in a bed’ to, ‘No animal shall sleep in a bed with sheets.’

Jones and his wife sabotage Animal Farm by blowing up the almost-complete windmill with dynamite. Napoleon frames Snowball for the sabotage, and the windmills reconstruction begins under strict supervision by Napoleons army, the Animal Guard. Squealer announces that "Beasts of the World" no longer has meaning since Animal Farms establishment, and he replaces it with a new song called "Glorious Leader, Napoleon." The pigs consume more food, leaving the other animals with little to eat. Napoleon declares that Snowball is causing the food shortage and that the hens will have to surrender their eggs to the market. When the hens oppose, Napoleon declares that the hens are all criminals and that no food will be given to them. The pigs produce propaganda films using Jones filming equipment.  While celebrating Napoleon as a leader, the films show the deaths of animals that have broken Napoleons rules.  It is revealed that the commandment, "No animal shall kill any other animal" has been changed to, ‘No animal shall kill any other animal without cause."  The commandment, "No animal shall drink alcohol," is also changed to, "No animal shall drink alcohol to excess," after the pigs begin to buy whiskey from Frederick.

After Boxer collapses from being overworked, Squealer informs Jessie that Napoleon will be sending Boxer to the hospital. Benjamin notices that the van that comes for Boxer is marked with the words "Horse Slaughterer," but Boxer is taken to his death before the other animals can intervene. As Jessie and Benjamin plan to flee from Animal Farm, Napoleon is paid for selling Boxer to the glue factory, and Squealers latest propaganda film assures the animals that the van was from the hospital, but had been previously been the property of a horse slaughterer. Frederick and his wife dine with the pigs in the farmhouse, where Napoleon announces that the farms name will revert to Manor Farm. Watching through a warped glass window, Jessie sees the faces of Frederick and Napoleon distorted in such a way that she cant tell the difference between them. Muriel and Benjamin notice that the commandment "All animals are equal" has been extended to include, "but some animals are more equal than others." As Jessie, Benjamin, and Muriel escape the farm with a small group of animals, a propaganda film displays a performance of a new song dedicated to Napoleon. The performance features Napoleon wearing clothes and standing on two legs, as the Animal Guard chants, "Four legs good, two legs better!" The performance ends with a speech from Napoleon, who announces plans to make weapons and build walls to preserve the farm. He declares the revolution over and announces, "All animals are now free!"

The film returns to the present, after Napoleons dictatorship has collapsed and the farm has fallen in ruins. Jessie finds several of her puppies in the abandoned ruins of the farm who all recognize their mother and she knew that Napoleon is dead (what happened to Squealer and the other animals is unknown). The farm is bought by a new family (though what became of Mr. Jones and his wife is also unknown), and Jessie vows that the animals will "not allow them to make the same mistakes." She says that they will work together to rebuild the farm now that they are finally free.

==Cast==
{| class="wikitable"
|- Cast
!align="center"|Character Notes
|- Julia Ormond Jessie
|align="center"|(voice)
|- Patrick Stewart Napoleon (Animal Napoleon
|align="center"|(voice)
|- Ian Holm Squealer (Animal Squealer
|align="center"|(voice)
|- Julia Louis-Dreyfus Mollie
|align="center"|(voice)
|- Kelsey Grammer Snowball (Animal Snowball
|align="center"|(voice)
|- Pete Postlethwaite Jones (Animal Farmer Jones/voice Benjamin
|align="center"|
|- Paul Scofield Boxer (Animal Boxer
|align="center"|(voice)
|- Peter Ustinov Old Major
|align="center"|(voice)
|- Alan Stanford Frederick (Animal Farmer Frederick
|align="center"|
|-
|}

==Production==
Filming began on August 25, 1998 and ending on November 6. Because of the extensive CGI work and other post-production requirements, the film was not delivered to TNT and Hallmark Entertainment until June 1999.

Fourteen animals were built to represent the animals of Animal Farm at Jim Hensons Creature Shop in London: four pigs (Old Major, Snowball, Napoleon, and Squealer), two horses (Boxer and Mollie), a sheepdog (Jessie), a donkey (Benjamin), a raven (Moses), a goat (Muriel), a sheep, a rat, a chicken, and a duck. 

Ten dogs were cast into the film from Fircroft Kennels. Their Border Collie, Spice, played the role of Jessie.

In early screenplays done by Martyn Burke for this film, Jessie was set to be a male character, rather than a female.

==Reception==
The film received mixed reviews. It currently holds a 40% rating on Rotten Tomatoes. 
It was criticized for its loose adaptation of the book, its simplicity and lack of subtlety, and for being too dark and political for children while being too familiar and simplistic for adults.

==References==
 

==External links==
 
 
*  

 

 
 
 
 
 
 
 
 
 