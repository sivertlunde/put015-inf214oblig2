Tenebrae (film)
{{Infobox film
| name           = Tenebrae
| image          = Tenebrae.jpg
| alt            = 
| caption        = Original Italian theatrical release film poster
| director       = Dario Argento
| producer       = Claudio Argento Salvatore Argento
| writer         = Dario Argento John Saxon Daria Nicolodi
| music          =  Claudio Simonetti Fabio Pignatelli Massimo Morante
| cinematography = Luciano Tovoli
| editing        = Franco Fraticelli
| studio         = Sigma Cinematografica Roma
| distributor    = Titanus
| released       = 28 October 1982
| runtime        = 110 minutes  (original cut)  101 minutes  (directors cut)  91 minutes  (edited cut) 
| country        = Italy
| language       = English Italian
| budget         = Unknown
| gross          = Unknown
}}
 horror Thriller thriller film John Saxon and Daria Nicolodi. After having experimented with two exercises in pure supernatural horror, 1977s Suspiria and 1980s Inferno (1980 film)|Inferno, Tenebrae represented Argentos return to the giallo subgenre, which he had helped popularize in the 1970s. The story concerns an American writer promoting his latest Crime fiction|murder-mystery novel in Rome, only to get embroiled in the search for a serial killer who has apparently been inspired to kill by the novel.

The film was released in Italy and throughout most of Europe without experiencing any reported censorship problems, but was classified, prosecuted and banned as a "video nasty" in the United Kingdom. Its theatrical distribution in the United States was delayed until 1984, when it was released in a heavily censored version under the title Unsane. In its cut form, Tenebrae received a mostly negative critical reception, but the original, fully restored version later became widely available for reappraisal. It has been described by Maitland McDonagh as "the finest film that Argento has ever made",  although most critics tend to disagree.

==Plot==
Peter Neal (Franciosa) is an American writer of violent horror novels whose books are tremendously popular in Europe.  In Italy to promote his latest work, entitled Tenebrae, he is accompanied by his literary agent Bullmer (Saxon) and his adoring assistant Anne (Nicolodi). He is unaware that he is also being followed by his embittered ex-wife Jane (Veronica Lario). Immediately prior to Neals arrival in Rome, a beautiful young shoplifter (Ania Pieroni) is brutally slashed with a straight razor by an unseen assailant. The murderer sends Neal a letter informing him that his books have inspired him to go on a killing spree. Neal is soon contacted by the police, who put Detective Giermani (Giuliano Gemma) in charge of the investigation, along with the detective’s female partner Inspector Altieri (Carola Stagnaro).  

More killings occur. Tilde (Mirella DAngelo), a beautiful lesbian journalist, is murdered at her home along with her lover. Later, Maria (Lara Wendel), the young daughter of Neals landlord, is bloodily hacked to death with a splitting axe after stumbling into the assailants lair. Neal notices that TV interviewer Christiano Berti (John Steiner) appears to have an unusually intense interest in the novelists work. At night, Neal and his second assistant Gianni (Christian Borromeo) watch Bertis house for suspicious activity. Gianni decides to separate from Neal in order to get a better view. Alone, Gianni watches in horror as an axe-carrying assailant brutally hacks Berti to death. But he is unable to see the murderers face. Gianni finds Neal unconscious on the lawn, having been knocked out from behind.

Giermanis investigation reveals that Berti was unhealthily obsessed with Neals novels, and now that he is dead it is believed that the killings will cease. However, Bullmer, who is having an affair with Jane, is stabbed to death while waiting for his lover in a public square. Gianni is haunted by the thought that he had seen, but did not recognize, something important at Bertis house during the night of the interviewers murder. He returns to the house and suddenly remembers what was so important – he had heard Berti confessing to his attacker, "I killed them all, I killed them all!"  Before Gianni can share this important detail with anyone, he is attacked from the back seat of his car and strangled to death.
 pistol when a figure with the same axe leaps through her window, hacking off one of her arms. She spews blood over the kitchen walls before falling to the floor, the killer continuing to hack at her until she is dead, revealing Neal to be the murderer. Upon learning the details of Bertis sadistic murder spree, Neal had suddenly been overwhelmed by a forgotten memory involving Neals murder of a girl who had sexually humiliated him when he was a youth in Rhode Island. The memory now constantly torments him and has inflamed his previously repressed lust for blood, having Neal completely insane.

When Inspector Altieri arrives at the house a few minutes after Janes death, Neal kills her too. Later, Giermani and Anne arrive at the house in the pouring rain, and when Neal sees that he cannot escape, he slits his throat in front of them. Finding the telephone out of order, Giermani and Anne go outside to report the incident from his car radio. Giermani returns to the house and is suddenly murdered by Neal, who had faked his own death. Neal waits inside for Anne to return, but when she opens the door, she accidentally knocks over a metal sculpture that impales and kills Neal. The horror-stricken Anne stands in the rain and screams over and over again.

==Cast==
*Anthony Franciosa as Peter Neal John Saxon as Bullmer
*Daria Nicolodi as Anne
*Christian Borromeo as Gianni
*Lara Wendel as Maria Alboretto
*Giuliano Gemma as Detective Germani
*Veronica Lario as Jane McKerrow
*Mirella DAngelo as Tilde
*Mirella Banti as Marion 
*John Steiner as Christiano Berti
*Ania Pieroni as Elsa Manni 
*Eva Robins as Girl on Beach (credited as Eva Robins)
*Carola Stagnaro as Detective Altieri

==Influences== The Girl Who Knew Too Much (1963) as the original giallo.   Biographer James Gracey refers to the film as a "reflexive commentary on his earlier work."  By the time he made Tenebrae, Argento had become the acknowledged master of the subgenre, to the point where he felt confident enough to be openly Self-reference|self-referential to his own past, referencing the "reckless driving humor" from The Cat o Nine Tails (1971) and the hero from The Bird with the Crystal Plumage (1970).    
 Beyond a Reasonable Doubt (1956) ("in which a man convicted of murder on false evidence...is in fact guilty of the murder") and Roy William Neills Black Angel (1946) ("in which a man who tries to clear a murder suspect does so at the cost of learning that he himself is the killer") both use such a similar plot twist to Tenebrae that Argento may very well have used them as partial models for his story. 

==Production==
 ]]
Dario Argento has claimed that Tenebrae was influenced by a disturbing incident he had in 1980 with an obsessed fan. The fan telephoned him repeatedly, day after day, until finally confessing that he wanted to kill the director.  Although ultimately no violence of any kind came of the threat, Argento has said he found the experience understandably terrifying and was inspired to write Tenebrae as a result of his fears.  

Although tenebrae/tenebre is a Latin/Italian word meaning "darkness" or "shadows", Argento ordered cinematographer Luciano Tovoli to film the movie with as much bright light as possible, to create a futuristic look.  The lighting and camerawork used in Andrzej Żuławskis Possession (1981 film)|Possession (1981) was an influence on the look of the film.  Shot on location in Rome, with Michele Soavi also assisting Argento in the directing,  much of Tenebrae takes place during daytime, or in harshly overlit interiors. Except for the finale and some night scenes, the entire movie is shot with clear, cold light permeating the surroundings. Argentos stated rationale for this approach was an attempt to imitate what he saw as the "realistic manner of lighting" used in television police shows. The director explained that he was adopting "...a modern style of photography, deliberately breaking with the legacy of German Expressionism. Todays light is the light of neon, headlights, and omnipresent flashes...Caring about shadows seemed ridiculous to me and, more than that, reassuring."    Film scholar Richard Dyer highlights several intelligent devices uses by Argento in the editing of the film, noting that interpolated sequences are sometimes punctuated by "shots of pills and the sound of running water."  Steffen Hantke believes that the shock cuts in the latter part of the film are among cinemas "most brutal and stylized", and exhibit a degree of abstract expressionism.  Film scholar Leon Hunt argues that the devices and themes utilized by Argento in the making of Tenebrae make it as much an example of art cinema as anything else. 
 Louma crane to film a several minutes-long tracking shot that acted as an introduction to the sequence. Due to its extreme length, the tracking shots potential for complications ended up being the most difficult and complex part of the production to complete.  Gracey describes the shot as an example of "aerial gymnastics", scaling the victims house in "one seamless take, navigating walls, roofs, and peering in through windows, in a set piece that effortlessly exposes the penetrability of a seemingly secure home".  Patrick McAllister, writing as Ironwolfe on Gerry Carpenters Scifilm website described it as   camera used was new to the industry at the time, and was bulky and not as easy to use as it is now. The 2.5 minute sequence took three days to shoot.| }}

Christopher Walken had reportedly been offered the role of Peter Neal, which eventually went to John Saxon.  Although an Italian production, most of the cast members spoke their dialogue in English to increase the films potential for successful exportation to the United States. For domestic audiences, the film was dubbed into Italian. In the English-language version, Franciosa, Gemma, Saxon and Steiner all provided their own voices, while Nicolodis voice was reportedly dubbed by actress Theresa Russell. 

===Title===
Some European publicity materials for the film, including posters and lobby card sets, advertised the film as Tenebre, and the 1999 Anchor Bay DVD release uses that same title. However, on the print itself, during the opening credits, the title is clearly Tenebrae. In addition, the title of Neals latest book in the film is shown in closeup as being Tenebrae. In a lengthy interview with Argento conducted by Martin Coxhead that appeared in two issues of Fangoria in 1983 and 1984, the title was always referred to as "Tenebrae".       DVD Talk reviewer Justin Felix noted in his review of the DVD release that, "in its package art, Anchor Bay refers to this movie as Tenebre - although the movie itself is titled Tenebrae."   

==Themes== themes in Tenebrae. In interviews conducted during the film’s production, the usually somewhat reticent Argento offered his own views as to the thematic content of the film. As biographer Maitland McDonagh noted in Broken Mirrors/Broken Minds: The Dark Dreams of Dario Argento, "...Argento has never been more articulate and/or analytical than he was on the subject of Tenebrae."  Film scholar William Hope identifies that the film is devoid of classical narrative progression, and writes that the characters "lack a narrative function or purpose, existing only to be killed in a spectacular fashion, their death hardly moving the narrative on at all. Traditional cause and effect are seemingly forgotten or actively ignored". 

==="Aberrant" sexuality===
According to Argento biographer James Gracey, a number of critics have drawn comparisons with the character of Peter Neal and the director himself, and have speculated that the character is an alter-ego.  As in many of Argentos films, which tend to eroticize the murder of beautiful women,  gender, sexuality and power are major issues in the foreground of the film.  The novel in the film itself is described as being "about human perversion and its effects on society".  Male and female sexual deviancy are a central theme, with the victims being what Flanagan refers to as "filthy, slimy perverts". The first victim is a sexually promiscuous shoplifter, and his next two are the lesbian reporter and her bisexual lover. He kills the comparatively "normal" Maria only because she inadvertently discovers his twisted compulsion. McDonagh notes that Tenebrae expands on the themes of sexuality and transvestitism in his earlier films such as The Bird with the Crystal Plumage, The Cat o Nine Tails, Four Flies on Grey Velvet (1972), and Deep Red, but believes that Tenebrae s "overall sensuality sets it apart from Argentos other gialli."  She says that the films sexual content and abundant nudity make it "the first of Argentos films to have an overtly erotic aspect," and further notes that "Tenebrae is fraught with free-floating anxiety that is specifically sexual in nature."  Gracey notes that in several scenes the victims gaze directly into the camera, which demonstrates Argentos "preoccupation with voyeurism and spectacle". 

   as a sadistic temptress about to be stabbed to death; this murder acts as the catalyst for the protagonists actions throughout the film]]
 flashbacks are Freudian shiny fetishistic imagery of these flashbacks, combined with the sadistic details of the murder sequences in the main narrative, "set the parameters of Tenebrae s fetishistic and fetishicized visual vocabulary, couched in terms both ritualistic and orgiastically out of control...Peter Neal indulges in sins of the flesh and Tenebrae revels in them, inviting the spectator to join in; in fact, it dares the viewer not to do so." 

===Vision impairment===
Paul Flanagan has observed that Argentos protagonists in his giallo films almost always suffer from vision impairment of some kind.    
It is these characters’ chronic inability to find the missing pieces of a puzzle (the puzzle being the solution of a murder or series of murders) that generally provides much of the films’ narrative thrust.  Most obviously is the blind Franco Arno (Karl Malden) in The Cat o Nine Tails, who must use his heightened aural sense in combination with visual clues supplied to him by his niece to solve a mystery.  In The Bird with the Crystal Plumage (1970), Sam Dalmas (Tony Musante) witnesses a murder attempt but admits to the police that something seems to be "missing"; as the films surprise ending makes clear, he didnt "miss" anything at all, he simply misinterpreted what happened in front of his eyes. In Deep Red (1975), Marcus (David Hemmings) has a similar problem in both seeing and not seeing the murderer at the scene of the crime, and doesnt realize his mistake until its almost too late. This recurring theme, according to Douglas E. Winter, creates "a world of danger and deception, where seeing is not believing..." 

Flanagan observes that in Tenebrae, Argento offers two separate characters who suffer from impaired vision. Gianni (Christian Borromeo) is an eyewitness to an axe-murder, but the trauma of seeing the killing causes him to disregard a vital clue. Returning to the scene of the crime, he suddenly remembers everything, and is promptly murdered before being able to tell a soul. Homicide detective Giermani reveals that he is a big fan of the novels of Agatha Christie, Mickey Spillane, Rex Stout, and Ed McBain, but admits that he has never been able to guess the identity of the killer in any of the books. He is similarly unable to solve the real mystery until the last corpses are piled at his feet – he cannot see Peter Neal for what he really is. 

===Dark doubles===
In his review of Tenebrae, Kevin Lyon observes, "The plot revolves around the audacious and quite unexpected transference of guilt from the maniacal killer (about whom we learn very little, itself unusual for Argento) to the eminently likeable hero, surely the films boldest stroke."  While also noticing this device as being "striking", McDonagh notes that this guilt transmission/transfer occurs between two dark doubles, two seriously warped individuals.  She suggests that "Neal and Berti...act as mirrors to one another, each twisting the reflection into a warped parody of the other."  Bertis obsession with Neals fiction compels him to commit murder in homage to the writer, while Neal seems to think that his own violent acts are simply part of some kind of "elaborate fiction." When the bloody Neal is confronted by Giermani immediately after having killed numerous people, Neal screams at him, "It was like a book...a book!"

McDonagh notes that Argento also emphasizes a similar doubling between Neal and Giermani.  "Giermani...is made to reflect Neal even as Neal appropriates his role as investigator...the detective/writer and the writer/detective each belittles his other half, as though by being demeaned this inverted reflection could be made to go away." McDonagh also observes that, in what is arguably the films most potent shock, Neal at one point really does make Giermani "go away", virtually replacing him on screen "in a shot that is as schematically logical as it is logically outrageous." 

===An imaginary city===
In an interview that appeared in Cinefantastique, Argento noted that the film was intended as near-science fiction, taking place "about five or more years in the future...Tenebrae occurs in a world inhabited by fewer people with the result that the remainder are wealthier and less crowded. Something has happened to make it that way but no one remembers, or wants to remember...It isnt exactly my Blade Runner, of course, but nevertheless a step into the world of tomorrow. If you watch the film with this perspective in mind, it will become very apparent."    Despite Argentos claim, Maitland McDonagh observed that this vaguely science-fictional concept "isnt apparent at all" and, in fact, no critics noted the underlying futuristic theme in their reviews of the theatrical release of the film. 

While rejecting this thematic concern as unrealized by Argento, McDonagh noticed that the result of the directors experiment is a strange "architectural landscape" that becomes the "key element in differentiating Tenebrae from Argentos earlier gialli." Argentos use of unusual architectural space and so-called visual "hyper-realism" results in an enormously fake looking environment. Seizing on the directors additional comment, "...I dreamed an imaginary city in which the most amazing things happen", she notes that the films "fictive space couldnt be less real", with its "vast unpopulated boulevards, piazzas that look like nothing more than suburban American malls, hard-edged Bauhaus apartment buildings, anonymous clubs and parking garages." 

==Soundtrack==
 
  Goblin to bass normal and Fretless guitar|fretless), and Massimo Morante (electric and acoustic guitar) – reunited at Argentos request to work on Tenebrae.  The resulting Synthesizer|synth-driven score was credited to "Simonetti-Pignatelli-Morante". 

While not as well regarded as Goblins earlier scores for Deep Red, Suspiria, or Dawn of the Dead (1978), Tim Lucas felt the soundtrack is "...so fused to the fabric of the picture that Tenebrae might be termed...a giallo musicale; that is, a giallo in which the soundtrack transcends mere accompaniment to occupy the same plane as the action and characters." Lucas, Tim. Video Watchdog magazine, issue #108, pp. 71–72.  Review of Tenebrae DVD  Writers David Kerekes and David Slater were also favorable to the score;  writing that the film "bristles with arresting imagery and a cracking musical score from ex-members of Goblin". 

The Tenebrae soundtrack album has been enduringly popular enough to have had multiple reissues in numerous countries since its original release in 1982 on the Italian Cinevox label.  That version consisted of only eight tracks. In 1997, Cinevox issued a greatly expanded version on Compact Disc|CD, including eleven bonus tracks, with a running time of over an hour. In 2004, the expanded CD was released in the U.S. on the Armadillo Music label.  In 2012 it was released again on Vinyl by AMS Records (Italy).

==Release and reception==
===Original viewing and controversy===
  poster campaign replaced the slashed neck with a red ribbon.]]
Tenebrae had a wide theatrical release throughout Italy and Europe, something the director very much needed after having suffered major distribution problems with his previous film, Inferno. In the United States, however, the film fared far less well. It remained unseen until 1984, when Bedford Entertainment briefly released a heavily edited version under the title Unsane.  It was approximately ten minutes shorter than the European release version and was missing nearly all of the films violence, which effectively rendered the numerous horror sequences incomprehensible. In addition, certain scenes that established the characters and their relationships were excised, making the films narrative difficult to follow. Predictably, this version of Tenebrae received nearly unanimously negative reviews.     
 Video Nasties" that were successfully prosecuted and banned from sale in UK video stores under the Video Recordings Act 1984. This ban lasted until 1999, when Tenebrae was legally rereleased on videotape, with an additional one second of footage removed from the film (this version was also missing the previously censored five seconds). In 2003, the BBFC reclassified the film and passed it without any cuts.     

The film has since been released basically uncut minus approximately 20 seconds of extraneous material  on DVD in the US, allowing the film to be properly evaluated for the first time.

===Later reception===
  said that "Tenebre is a riveting defense of auteur theory, ripe with self-reflexive discourse and various moral conflicts. Its both a riveting horror film and an architects worst nightmare."  Keith Phipps, of The Onions A.V. Club, noted "...Argento makes some points about the intersection of art, reality, and personality, but the directors stunning trademark setpieces, presented here in a fully restored version, provide the real reason to watch."   Almar Haflidason, in a review for BBC.co.uk, opined, "Sadistically beautiful and viciously exciting, welcome to true terror with Dario Argentos shockingly relentless Tenebrae."  Tim Lucas in Video Watchdog said, "Though it is in some ways as artificial and deliberate as a De Palma thriller, Tenebrae contains more likeable characters, believable relationships, and more emphasis on the erotic than can be found in any other Argento film." Lucas, Tim. Video Watchdog magazine, issue #49, pgs. 68-72.  Review of Tenebrae Laserdisc  Gordon Sullivan of DVD Verdict wrote, "Tenebre is a straight-up giallo in the old-school tradition. It may have been filmed in 1982, but it comes straight out of the 70s tradition. Weve got all the usual suspects, including a writer for a main character, lots of killer-cam point of view, some crazily over the top kills, and approximately seventy-two twists before all is revealed...  For fans of Argentos earlier giallo, this is a must-see." 
 Time Out thought that the film was "unpleasant even by contemporary horror standards".  John Kenneth Muir, author of Horror Films of the 1980s, considers the film to be far inferior to Suspiria, but acknowledges that it was so "unremittingly gory" that it justified its US title of "Unsane".  John Wiley Martin, although evaluating the film as a "technically mesmeric" one, felt that thematically it was a "disappointingly retrograde step" for Argento.  Christopher Null of Filmcritic.com referred to it as a "gory but not particularly effective Argento horror flick", while Dennis Schwartz dismissed it as trash.  Gary Johnson, editor of Images, complained that "Not much of Tenebre makes much sense. The plot becomes little more than an excuse for Argento to stage the murder sequences. And these are some of the bloodiest murders of Argentos career."  In 2004, Tim Lucas reevaluated the film and found that some of his earlier enthusiasm had dimmed considerably, noting that, "Tenebre is beginning to suffer from the cheap 16 mm-like softness of Luciano Tovolis cinematography, its sometimes over-storyboarded violence (the first two murders in particular  look stilted), the many bewildering lapses in logic...and the overdone performances of many of its female actors..." Lucas, Tim. Video Watchdog Magazine, issue #108, pgs. 71–72. Review of Tenebrae DVD 

==Legacy== The Untouchables (1987).  In Raising Cain, De Palmas "surprise reveal" of John Lithgow standing behind a victim is often discussed as being an unacknowledged "steal" from Tenebrae.   Robert Zemeckiss What Lies Beneath (2000) also contains a very similar moment, although Zemeckis has denied having any familiarity at all with Italian thrillers. 

==References==
 

===Sources===
 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
*  
* 
* 
*  
* 
* 
* 
 

==External links==
*  

    

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 