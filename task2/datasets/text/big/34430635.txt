Ex – Amici come prima!
{{Infobox film
| name           = Ex - Amici come prima!
| image          = ExAmiciComePrima2011Poster.jpg
| caption        = Film poster
| director       = Carlo Vanzina
| producer       = Federica Lucisano Fulvio Lucisano
| screenplay     =Enrico Vanzina and Carlo Vanzina
| based on       = 
| starring       = Alessandro Gassman 
| music          = Giuliano Taviani Carmelo Travia
| cinematography = Carlo Tafani
| editing        = Raimondo Crociani
| studio         = Italian International Film Rai Cinema
| distributor    = RAI
| released       =  
| runtime        = 98 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          =
}}Ex – Amici come prima! is an Italian comedy film.  It was directed by Carlo Vanzina and its ensemble cast is led by Alessandro Gassman and Anna Foglietta.  
It was produced by Italian International Film and distributed by 01 Distribution, a subsidiary of RAI.

==Cast==

*Alessandro Gassman: Max
*Vincenzo Salemme: Antonio
*Ricky Memphis: Fabio
*Enrico Brignano: Marco
*Teresa Mannino: Floriana 
*Gabriella Pession: Valentina
*Tosca DAquino: Nunzia
*Natasha Stefanenko: Olga
*Paolo Ruffini: Paolo  
*Anna Foglietta: Sandra
*Veronika Logan: Miss Marangoni
*Elena Barolo: Esther 
*Rosabell Laurenti Sellers: Barbara 

==External links==
*  
 

 
 


 