Venkalam
{{Infobox film
| name           = Venkalam
| image          = Venkalam.jpg
| caption        = VCD cover
| director       = Bharathan
| producer       = V. V. Babu
| writer         = A. K. Lohithadas
| starring       =  
| music          = {{Plainlist|
* Songs:
* Raveendran
* BGM: Johnson
}}
| cinematography = Ramachandra Babu
| editing        = B. Lenin V. T. Vijayan
| studio         = Srishti Arts
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Urvashi in the lead roles.

== Cast == Murali	 ...	Gopalan
* Manoj K. Jayan	 ...	Unnikrishnan Urvashi	 ...	Thankamani
* K. P. A. C. Lalitha	 ...	Kunjipennu Sonia	 ...	Sulochana
* Kuthiravattam Pappu
* Mala Aravindan	 ...	Ayyappan
* Ragini	 ...	Karthyayani
* Philomina	 ...	Grand Mother
* Priyanka	 ...	Nadathara Kanakam
* Nedumudi Venu	 ...	Chathukutti Innocent	 ...	Kandappan
* Seena Antony	 ...	Indira
* M.S Tripunithura	 ...	Damodaran Nampoothiri
* Salu Kuttanadu

== Songs ==
The songs of the movie were written by P. Bhaskaran and were composed by Raveendran. The male version of the song "Pathu veluppinu" was included in the music cassettes and CDs although it was not part of the movie. This song gave its singer Biju Narayanan a place in the Malayalam music industry.
{| class="wikitable"
|-
! Song!! Singer
|-
| Aarattu Kadavinkal ... || K. J. Yesudas, K. S. Chithra
|-
| Othiri othiri ..... || K. J. Yesudas, Lathika
|-
| Pathu veluppinu .... ||K. S. Chithra
|-
| Seeveli mudangi ...... || K. J. Yesudas
|}

== External links ==
*  

 
 
 

 