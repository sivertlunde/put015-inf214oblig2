The Back of Beyond
 
 
{{Infobox film
| name           = The Back of Beyond
| image          =
| image_size     =
| caption        =
| director       = John Heyer
| producer       = John Heyer Douglas Stewart Tom Kruse William Butler Jack the Dogger Old Joe the Rainmaker the Oldfields of Ettadina Bejah Malcolm Arkaringa the people of the Birdsville Track Kevin Brennan
| music          = Sydney John Kay
| cinematography = Ross Wood
| editing        = John Heyer
| distributor    = Shell Film Unit
| released       = 1954
| runtime        = 66 minutes
| country        = Australia
| language       = English
| budget         = Australian pound|£12,000 (estimated)
| gross          =
| preceded_by    =
| followed_by    =
}}

The Back of Beyond (1954) is a feature-length award-winning Australian documentary film produced and directed by John Heyer for the Shell Film Unit. In terms of breadth of distribution, awards garnered, and critical response, it is Heyers most successful film. It is also, arguably, Australias most successful documentary: in 2006 it was included in a book titled 100 Greatest Films of Australian Cinema, with Bill Caske writing that it is "perhaps our   national cinemas most well known best kept secret". 
 Tom Kruse, along the remote Birdsville Track from Marree, in South Australia, to Birdsville, in southwest Queensland. In 1957, Heyer wrote that this film, when viewed with Francis Birtles earlier In the Track of Burke and Wills (1916), "clearly suggest  that the true image of Australia is, and always has been, the image of Man against Nature". 
 Tom Kruse to public notice, and resulted in his being appointed a Member of the Order of the British Empire (MBE) on 1 January 1955. 

==Synopsis==

In simple terms, the film follows a "typical" journey made by Tom Kruse, from Marree to Birdsville, some 325 miles away, showing the various people he met along the Track and the sorts of obstacles he faced. In fact, sometimes described as a docudrama, the film was closely scripted: it comprises a number of re-enactments and a lost children story, rather than chronicling an actual trip.
 Bejah Dervish, the Afghan camel driver who "fought the desert by compass and by Koran"; William Henry Butler, Kruses record-playing companion; Jack the Dogger who kills wild dingoes; and old Joe the Aboriginal rainmaker. Australian Screen curator, Lauren Williams, suggests that the film "can be read like a collection of travelling vignettes along the Birdsville Track, embracing the experiences of these people and the isolated ‘never-never’ land they occupy".   

The sequences in the film are, as described by Cunningham:   

#Titles, Introduction
#Marree
#Travelling, The Night Bog
#Etadinna
#Cooper Crossing
#Kopperamanna Mission
#Travelling Vignettes
#Lost Children
#Windstorm, Birdsville

==Production==

The film took 3 years to make: one year of thinking and planning, one year of production, and one year to edit and finish it. The film was scripted in advance, though changes were made during filming and production. Of the three years, only six weeks were spent shooting on location. 

Heyer prepared the shooting script after undertaking a research trip with Tom Kruse, and location shooting began in late 1952. The film was edited by Heyer in Sydney at Mervyn Murphys Supreme Sound studio. Shirley, G and Adams, B (1983) Australian cinema: the first eighty years, Sydney, Currency Press 

Conditions for the location shoot were harsh – with both the terrain and the weather creating difficulties for the crew. Sand, in particular, created havoc with the equipment. Audio-tapes of the soundtrack recorded on location could not be used due to sand damage, and the whole film had to be revoiced in post-production. Lauren Williams writes that "While it was common to post-sync dialogue and sound effects in documentaries at this time, Kruse and other participants in the film expected to hear their own voices up on screen and some of them were reportedly shocked to hear another persons accent coming out of their own mouths". 

==Themes==

Lauren Williams, writes that "the film reconfirms settler anxieties about the outback as a place of isolation, brutal indifference, danger and timelessness" but at the same time presents "the characters in the landscape as survivors, people who endure, battlers with hearts of gold". 

==Style==

John Heyer and Ross Wood, his cinematographer, had both worked for the Commonwealth Film Unit prior to joining Shell. Lauren Williams argues that "Woods accomplished visual style and Heyers grasp of film language combine in   to create some of the most iconic images of the Australian outback filmed in this period". 
 Harry Watt. Douglas Stewart, on the script later in the production phase. In 1955, Stewart published a book of poems titled The Birdsville Track drawing from his work on the films script.

While the film is highly praised and granted classic status, some critics question specific aspects, most commonly the Lost Children sequence. Some argue that that it breaks the narrative flow, while others insist that it works well.

==Release and distribution==

The film, released only in   in June 1954 and it was shown widely in Australia, including throughout the outback.  Its Canberra premiere was held at the Albert Hall on 8 July. Amongst the audience of 300 were many diplomats, including those from the United States of America, Japan, China and Ceylon. 

In the first year of its release in Australia, due largely to Shells extensive distribution and exhibition network, it was seen by over 750,000 people.  It was also televised extensively overseas, and represented Australia at several film festivals. 

==Reception==
 ABC radio broadcast on 1 May 1954 said, prophetically, that "I believe this film will become a classic. It is poetic, imaginative and yet tough at the same time. There is humour in it, unforced and natural ... John Heyers The Back of Beyond is a landmark in Australian documentary. It will cause a sensation in Britain". 

Other reviews of the time include: 
 The Listener, 3 June 1954)
*"A small audience sitting in a private cinema off the Strand today saw what must rank as one of the most remarkable documentaries ever made." (from The Manchester Guardian, 19 February 1954)
*"... a landscape where man is always solitary, always on the defensive against Nature; once more, the Shell film-makers revive faith in documentary." (from Dilys Powell in Sunday Times, 21 February 1954)
*"... is bound to rank as an Australian masterpiece ... the message of the film is by no means one of unrelieved horror and pessimism. There is much hopefulness in the unaffected courage and the humour it finds among the people who live along the Birdsville Track." (from The Sydney Morning Herald, 25 March 1954)
*"... is significant on account of the perfect blending of sound, words and images ... The images of the other films shown at the Festival are as static as picture postcards and certainly dont possess the suggestive powers of John Heyers film." (from Uomini E Film, Venice, Volume 4–5 August 1954) Charles Chauvel in Walkabout, 1959)

In addition to its being regularly discussed in academic circles and frequent retrospective screenings, evidence of its ongoing longevity as a significant film include:

*the publication in 1968, by the British Film Institute, of Eric Elses study guide 
*the release of a 50th anniversary DVD collection in 2004
*the 50th anniversary screening in remote Marree in 2004 that drew a large audience 
*its listing in 2006 in Hockings 100 Greatest Films of Australian Cinema

==Awards==

*1954 Venice Biennale: Grand Prix Assoluto
*1954 Edinburgh International Film Festival: Diploma
*1955 Cape Town Film Festival: Diploma
*1956 Montevideo Film Festival: 1st Prize
*1956 Johannesburg Film Festival: Diploma
*1956 Trento Film Festival: Diploma

==Related films==

*The Outback Mailman (1986)
*The Postman (1996)
*Back to the Back of Beyond (1997, Robert Francis)
*Last Mail from Birdsville: The Story of Tom Kruse (1999)

==Notes==
 

==References==
*John Burgan|Burgan, John “The Back of Beyond”, Encyclopedia of the Documentary Film, ed. Ian Aitken, Routledge, New York, 2006, pp. 71–2
*Hocking, Scott (ed.) (2006) 100 greatest films of Australian cinema, Scribal Publishing
*Lansell, Russell and Beilby, Peter (ed) (1982) The documentary film in Australia North Melbourne, Cinema Papers
* 

==External links==
 
* 
* 
*  at Oz Movies
*  at Australian Screen Online
 
 
 
 
 
 
 