Poisson d'avril (film)
{{Infobox film
| name           = Poisson davril
| image          = 
| caption        = 
| director       = Gilles Grangier
| producer       = Victory, Intermondia Films
| writer         = Michel Audiard Gérard Carlier
| starring       = Zappy Max Louis de Funès Annie Cordy
| music          = Étienne Lorin
| cinematography = 
| editing        = 
| distributor    = Victory Films
| released       = 28 July 1954 (France)
| runtime        = 102 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| gross = $18,772,000 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1954, directed by Gilles Grangier, written by Michel Audiard, starring Zappy Max and Louis de Funès. 

== Cast ==
* Bourvil : Emile Dupuy, auto mechanic
* Louis de Funès : the guard fishes
* Annie Cordy : Charlotte, wife of Emile
* Denise Grey : Clémentine Prévost, wife of Gaston
* Jacqueline Noëlle : Annette Coindet, a cousin
* Pierre Dux : Gaston Prévost, the lover of Annette
* Louis Bugette : le patron du garage
* Maurice Biraud : the seller of fishing equipment
* Gérard Sabatier : small Jacky Dupuy
* Paul Faivre : Louis, the bistro: "Pépère"
* Charles Lemontier : Mr Gauthier

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 


 