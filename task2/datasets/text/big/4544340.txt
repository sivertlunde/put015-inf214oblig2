Fool for Love (film)
{{Infobox film
| name           = Fool for Love
| image          = Fool for love.jpg
| writer         = Sam Shepard
| starring = {{Plainlist|
* Sam Shepard
* Kim Basinger
* Randy Quaid
* Harry Dean Stanton
}}
| director       = Robert Altman
| music          = George Burt
| cinematography = Pierre Mignot
| editing        = Stephen P. Dunn Luce Grunenwaldt
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $2 million Andrew Yule, Hollywood a Go-Go: The True Story of the Cannon Film Empire, Sphere Books, 1987 p189 
| gross          = $900,000 
| distributor    = Cannon Group
}}
 original play and the adaptations screenplay, alongside Kim Basinger, Harry Dean Stanton, Randy Quaid and Martha Crawford. It was entered into the 1986 Cannes Film Festival.    It was filmed in Las Vegas,NM. The house used for several scenes was 1001 7th st. 

==Plot==
May (Kim Basinger) is hiding out at an old motel in the Southwest. An old flame and childhood friend, Eddie (Sam Shepard) shows up. He threatens to metaphorically and at times, literally, drag her back into the life she had fled from. The film focuses on the couples fluctuating past and present relationships, and the dark secrets hidden within, including one from an old man who lives near the motel (Harry Dean Stanton).

==Cast==
* Sam Shepard - Eddie
* Kim Basinger - May
* Harry Dean Stanton - Old Man
* Randy Quaid - Martin
* Martha Crawford - Mays Mother
* Louise Egolf - Eddies Mother
* Sura Cox - Teenage May
* Jonathan Skinner - Teenage Eddie
* April Russell - Young May
* Deborah McNaughton - The Countess
* Lon Hill - Mr. Valdes

==Soundtrack==
Sandy Rogers wrote the soundtrack songs including the title country pop ballad ("Fool for Love"), which later would also appear in the film Reservoir Dogs and on its soundtrack album release. 

==Reception==
The film received average reviews,   though was praised by several high profile critics, like Roger Ebert, who said, With "Fool for Love," he (Altman) has succeeded on two levels that seem opposed to each other. He has made a melodrama, almost a soap opera, in which the characters achieve a kind of nobility. 

==See also==
*Fool for Love (play) - the original 1983 play, often considered part of a quintet with Shephards other works.

==References==
 

==External links==
*  
*   at Rotten Tomatoes

 
 

 
 
 
 
 
 
 
 
 


 