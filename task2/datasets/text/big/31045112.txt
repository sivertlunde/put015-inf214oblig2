Adikkurippu
{{Infobox film
| name = Adikkurippu
| image = Adikkurippu.jpg
| image size =
| alt =
| caption = DVD Cover
| director = K. Madhu
| producer =
| story = Jose Kurian
| screenplay = S. N. Swamy
| narrator = Urvashi
| Shyam
| cinematography =
| editing =
| studio = Suriya Films
| distributor =
| released =   
| runtime =
| country =
| language =
| budget =
| gross =
}}

Adikkurippu is a 1989 Malayalam language|Malayalam-language legal thriller film written by S. N. Swamy and directed by K. Madhu.   It stars Mammootty in the lead role, who plays the role of an advocate.

==Plot==
The story revolves around the castaway Basheer (Jagathy Sreekumar), lost his memory partially. He is unable to recollect the past about him. The captain of the ship (Lalu Alex) who found him would like to handover Basheer to the relatives post anchoring in Kochi. But someone tries to kill Basheer and planning to deport him. Adv. Bhaskara Pillai (Mammootty)comes to his rescue and finds the people behind the ploy.

==Cast==
* Mammootty as Adv. Bhaskara Pillai Urvashi as Geetha Janardanan as William
* Jagathy Sreekumar as Basheer Vijayaraghavan as Mohemmedali
* Sreenath as Raju
* Jose Prakash as Menon Lizy as Veena/Bhaskara Pillais Sister
* Sukumaran as Chief Minister
* Lalu Alex as Captain John Samuel
* K. P. A. C. Sunny as Karthikeyan
*Meenakumari as Bhaskara Pillais Mother
* Babu Namboothiri as Public Prosecutor APP
* Jagadish as Bapputty
* Kollam Thulasi as Collector
* Prathapachandran as Venkata Swamy Azeez as Judge
* Kothuku Nanappan ... Aasan Santhakumari ... Basheers Mother Vijayan as HomeMinister Keshavan
*Nandhu as Dentists Assistant
*M. S. Thrippunithura as Customs Officer
*Nassar Latheef as Sunny
*Suma Jayaram as Basheers sister

==References==
 

==External links==
*  
*   at the Malayalam Movie Database

 
 


 