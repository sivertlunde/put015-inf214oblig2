The Guy from Harlem
 

{{Infobox film
| name           = The Guy from Harlem
| image          =Poster of the movie The Guy from Harlem.jpg
| image_size     =
| caption        =
| director       = Rene Martinez Jr.
| producer       =
| writer         = Gardenia Martinez (screenplay)
| narrator       =
| starring       = 
| music          = Cecil Graham
| cinematography = Rafael Remy
| editing        =
| distributor    =
| released       =
| runtime        = 86 minutes (USA)
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Guy from Harlem is a 1977 American Blaxploitation film directed by Rene Martinez Jr (Who also directed the controversially titled "The Six Thousand Dollar Nigger").

The film is also known as The Good Guy from Harlem. 

==Plot==

Al Connors (Loye Hawkins) is a private detective operating in Miami, Florida.  Upon arrival at his office hes told by his secretary that an old CIA contact and friend had been trying to get a hold of him in Als absence from the office.

The CIA contact, David McLeod, arrives shortly afterwards in order to enlist Als services to protect the wife of an African head of state in town for diplomatic business.   McLeod suspects a security leak and feels the wife is under better protection out of CIA hands.  Connors accepts the job and suggests posing as a newlywed couple at a local hotel.

Connors meets the wife, Mrs. Ashanti, at the hotel and informs her of the ruse to which shes initially reluctant to participate in until Connors assures her its for appearances only and doesnt have any inappropriate intentions towards her.  Ashanti complains of a sore back, prompting Connors to call for a masseuse to come to their room.

Later, Connors observes the masseuse at the window gesturing to a man on the sidewalk, raising his suspicions of her and prompting him to watch her perform her work even at the protest of Wanda and the masseuse.  (He turns out to be correct in his suspicions, as in a moment when he is out of the room the masseuse prepares a syringe.)  The masseuse leaves without incident and Connors orders some well-done New York strip steaks and J&B Scotch from room service.

Room service arrives and without provocation Connors knocks out the room service deliverer, claiming he can "smell a New York Strip from a mile away" and then reveals a gun hidden under the cover of one of the room service trays.  Connors holds the gun to the deliverers (a man dressed as a woman) head and makes him confess who sent him.  Soon two more men enter the room; thugs working for a local crime kingpin "Big Daddy", who Connor quickly dispatches using his fighting and martial arts skills.  Connors deems the hotel to be unsafe and leaves with Ashanti to a safer location.

The safer location turns out to be the apartment of Connors current girlfriend, who is initially reluctant to leave but soon does so without objection.  Ashanti continues to complain about her sore back and Connors offers to help her with his own massage knowledge eventually leading to he and Ashanti consummating their relationship on the bed of apartments tenant. Presumably the next day, Connors safely delivers Ashanti back into the hands of the CIA and her husband, the head of state of an African nation.

On return to his office Connors expresses his intentions to have a "goof-off day" to his secretary but the intentions are soon dashed with the entrance of another potential client, this one being Harry De Bauld and small entourage of body guards/thugs. De Bauld is the leader of a local criminal organization and recently his daughter, Wanda, has been kidnapped by a rival gang, ran by a man known only as "Big Daddy."  A money/hostage exchange is set to occur and De Bauld loudly wishes Connors to exploit that exchange in order to recover his daughter, to carry it out Connors is given a large sum of cash for his services as well as a large amount of cocaine, his end of the hostage exchange.

Connors meets with one of Big Daddys men in a local gym to set up the details of the exchange, but Connors uses that as a ruse to follow the man to the hide-out where they are holding Wanda.  After taking out several of the men guarding the location as well as the man running the compound Connors is able to recover Wanda and he takes her with him to a safe hiding place.

The hiding place, once again, turns out to be his girlfriends home whom he has to eject, this time at her great annoyance.  Wanda and Connors talk some, Wanda revealing shes not happy with her father whose criminal activities had recently grown more intense with the involvement of drug trafficking. Wanda and Connors consummate their relationship on the bed of the apartments tenant.

The following day Connors delivers Wanda back into the hands of her father in his office but refuses to return the drugs trying to convince Du Bauld to drop that aspect of his organization in order to repair the relationship with his daughter.  Connors also wishes to meet with Big Daddy in order to put a stop to his activities once and for all.

Connors and Big Daddy meet later to discuss their differences. Big Daddy tries to enlist Connors into his organization but Connors refuses and the two fight. Connors is able to quickly defeat and kill Big Daddy and then leaves in the arms of Wanda (who had arrived with her brothers to observe the fight and potentially aid Connors.)

==Cast==
*Loye Hawkins as Al Connors Cathy Davis as Wanda De Bauld
*Patricia Fulton as Mrs. Ashanti
*Wanda Starr as Sue
*Steve Gallon as Harry De Bauld
*Laster Wilson as Larry De Bauld
*Vaughn Harris as David McLeod, CIA
*Richie Vallon as Jim
*Michael Murrell as Paul Benson
*Angela Schon as Jo Ann
*Douglas Ferraro as Man #1
*Fernando Yi as Man #2
*Colleen Martinez as Masseuse
*Hyatt Hodgdon as Mac
*Mickeal Taylor as Special Man
*Raff Prieto as Waitress
*Lafon Hockaday as Health Spa Owner
*Patrick Hackett as Health Spa Extra
*Jesus Correa as Health Spa Extra
*Douglas Brinson as Bellboy

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 