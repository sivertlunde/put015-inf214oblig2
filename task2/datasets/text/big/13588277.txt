Psychopath (1973 film)
 
{{Infobox film
| name           = The Psychopath
| image          = 
| image_size     = 
| caption        = 
| director       = Larry G. Brown
| producer       = Larry G. Brown
| writer         = Larry G. Brown Walter Dallenbach
| starring       = 
| music          = Country Al Ross
| cinematography = Jack Beckett
| editing        = Dennis Jakob John Williams
| studio         = 
| distributor    = 
| released       =
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Ashton as one of its co-stars.

== Plot ==
Mr. Rabbey, the host of a childrens television show, is every boys and girls favorite. But unknown to everyone, when he hears stories from the kids who watch his show about the abuse they suffer at the hands of their parents, he starts visiting the parents and murdering them. Eventually, the police begin to suspect him of the murders.

== Cast ==
* Tom Basham as Mr. Rabbey
* Gene Carlson as Burt Mitchell
* Gretchen Kanne as Carolyn
* David Carlile as Perry Forbes
* Barbara Grover as Judy Cirlin
* Lance Larsen as Harold Cirlin
* Jeff Rice as Richard
* Peter Renaday as Lt. Hayes (as Pete Renoudet)
* Jackson Bostwick as Sgt. Graham John Ashton as Sgt. Matthews (as John D. Ashton)
* Mary Rings as Mother in Park
* Margaret Avery as Nurse
* Sam Jarvis as Coroner
* Brenda Venus as Joanie
* Carol Ann Daniels as Mrs. DSicca Bruce Kimball as Mr. DSicca

== Trivia ==
The tagline of the film is "Hes Judge... Jury... Executioner".
 Captain Marvel from 1974 to 1975 in the series Shazam! (TV series)|Shazam!

==External links==
*  
*  
*    by TCM Underground 


 
 
 
 
 
 

 