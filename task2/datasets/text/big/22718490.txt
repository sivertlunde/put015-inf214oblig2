Ojōsan shachō
{{Infobox Film
| name           = Ojōsan shachō
| image          = Ojosan shacho poster.jpg
| caption        = Japanese movie poster
| director       = Yuzo Kawashima Kō Nakahira (assistant director) Chiyoo Umeda (art director)
| producer       = Shochiku
| writer         = Yoshiro Tomita (writer) Ruiju Yanagisawa (writer)
| starring       = 
| music          = Tadashi Manjome
| cinematography = 
| editing        = 
| distributor    =  1953 
| runtime        = 93 min.
| country        = Japan
| awards         =  Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| 
}} Japanese film directed by Yuzo Kawashima.

It is musical film.

== Cast ==
* Hibari Misora as Madoka Ohara
* Ichirō Arishima as Tetsutaro Kaitani
* Takiko Egawa as musical actress
* Kodayu Ichikawa as Juzaburo Ohara
* Kokinji Katsura as Sanpachi Sakurakawa
* Shoichi Kofujita as Mr. O
* Yōko Kosono as Kikuko Morikawa
* Tatsuo Nagai as Mito
* Shinyo Nara as a tutor Sugiura
* Shirō Osaka as Keigo Namiki
* Keiji Sada as Goro Akiyama
* Takeshi Sakamoto as Ippachi Sakurakawa
* Mutsuko Sakura as Osugi Morikawa
* Ichirō Shimizu as Senzo Akakura
* Akira Takaya as Gamaroku
* Norikazu Takeda as Matsuzo Morikawa
* Jun Tatara as Yasuda
* Yumeji Tsukioka as Yumiko Kaitani

== References ==
 

== External links ==
* http://www.imdb.com/title/tt0408052/

 
 
 
 
 
 
 


 
 