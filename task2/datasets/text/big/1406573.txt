Seducing Doctor Lewis
 
 
{{Infobox film
| name           = La grande séduction
| image          =
| image_size     = 
| border         = 
| alt            = 
| caption        =
| film name      = 
| director       = Jean-François Pouliot
| producer       = Roger Frappier Luc Vandal Ken Scott
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Raymond Bouchard David Boutin Benoit Brière Lucie Laurier|
| music          = Francois-Pierre Lue   Maxime Barzel   Paul-Étienne Côté
| cinematography = Douglas Koch
| editing        = Dominique Fortin
| studio         = 
| distributor    = Dogwoof Pictures (UK) Wellspring Media (USA)
| released       =  
| runtime        = 108 minutes
| country        = Canada
| language       = French
| budget         = 
| gross          = $12,587,032   
}}

Seducing Doctor Lewis (   . It won the Audience Award at 2004 Sundance Film Festival. Starring in the movie are Raymond Bouchard, Benoit Brière, David Boutin and Lucie Laurier.

==Plot==
The tiny fishing village Ste-Marie-la-Mauderne on the north coast of Quebec is in decline. Every resident collects welfare. To lure a company into building a plastic container factory nearby, they need to double their population of 120, a resident doctor, and a $50,000 bribe for the company owner.

Montreal plastic surgeon Dr Christopher Lewis (David Boutin) gets pulled over for speeding by an officer originally from Ste-Marie-la-Mauderne. The officer will not arrest him for drug possession, if Dr Lewis will visit Ste-Marie-la-Mauderne for one month. In a deleted scene, Dr Lewis sells cocaine to his patients. 

Mayor Germain Lesage (Raymond Bouchard) hatches a plan. The entire village will convince Dr Lewis to stay. They tap his phone, and pretend to share his likes: fishing, cricket, fusion jazz, all the same foods. A local banker leaves small amounts of money for Dr Lewis to find, and attempts to secure a loan for the bribe. Dr Lewis likes the beautiful post office worker Ève Beauchemin (Lucie Laurier), but Ève knows he has a girlfriend, Brigitte, in Montreal.

The ruse works, but they cannot secure a loan. Local banker Henri Giroux (Benoit Brière) fronts the money from his personal savings, after a bank executive calls him an Automated teller machine|ATM. When the company owner arrives, everyone continues their elaborate trick, and convinces him to build the a factory there. The owner is ready to sign, but insists that they must have a doctor. 

When Dr Lewis learns that his Montreal girlfriend Brigitte has been having an affair with his best friend for three years, he proclaims that he will stay because everyone in the village is genuine. Germain feels bad for lying, confesses all, and explains that a doctor in residence would save the village. Dr Lewis decides to stay, the factory is built, Ste-Marie-la-Mauderne is saved, and everyone gains renewed pride.

==Production== Harrington Harbour, producers felt the island looked too pretty to fill the role of a fishing village experiencing hard times, so they worsened its appearance in the movie.

==Awards==
*Winner - Sundance Film Festival: World Cinema Audience Award  

==Remakes==
  remake was Mary Walsh, Cathy Jones, and Gordon Pinsent as town folks.     The storys setting was moved to Newfoundland.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 