I Want Your Money
{{Infobox film
| name           = I Want Your Money
| image          = I want your money poster.jpg
| alt            = Caricature of United States President Barack Obama wearing an Uncle Sam hat and blue jacket point his right-hand index finger at the viewer above the title of movie.
| caption        = Theatrical release poster Ray Griggs
| producer       = Michael Kim Binder Ray Griggs Doug Stebleton
| writer         = Randall Norman Desoto Ray Griggs
| music          = Don L. Harper
| cinematography = Matthew Mayotte
| editing        = Jessica Graeme Lowry RG Entertainment
| distributor    = Freestyle Releasing
| released       =  
| runtime        = 92 minutes   
| country        = United States
| language       = English
| budget         = $400,000 
| gross          = $433,000 
}} Ray Griggs. It contrasts Barack Obama to Ronald Reagan.

==Premise== conservative viewpoint, the differences between "Reaganomics" and "Obamanomics" as American economic and governmental policies, as well as their respective impact on life in the United States of America, as summarized from films official web site:   

 Two versions of the American dream now stand in sharp contrast. One views the money you earned as yours and best allocated by you. It champions the traditional American dream, which has played out millions of times through generations of Americans, of improving one’s lot in life and the entrepreneurial spirit of daring to dream and to build big. The other believes that the federal government, using taxpayers’ money, should play a major role in leveling out the nation’s wealth to guarantee outcomes to all, regardless of effort. How America chooses between these two views of the role of government, at this crucial juncture, will have everything to do with the future we and our children and our children’s children will enjoy.  

The film uses computer animation, film clips, archival footage, dramatizations, music, graphics, and on-camera interviews with well-known public figures and experts "to tell the story in the plainest terms of the choice between the Obama and the Reagan views of the role of the federal government in our society." 

==Interviews==
The following individuals were interviewed for I Want Your Money: 
{| width="100%">
|-
! colspan=6| 
|-
| width="15%" valign="top"|
* Gary Bauer
* Kenneth Blackwell
* Andrew Breitbart Chris Edwards
| width="15%" valign="top"|
* Lee Edwards
* Steve Forbes
*  
* Newt Gingrich
| width="15%" valign="top"|
* Mike Huckabee
* Allen Icet
* Tom McClintock
* Thad McCotter
| width="15%" valign="top"|
* David M. McIntosh Edwin Meese lll Stephen Moore
* Kate Obenshain
| width="15%" valign="top"|
* Star Parker
* Michael Reagan
* Lila Rose
* George Runner
| width="15%" valign="top"|
* Rob Schaaf
* John Stossel
*  
* Pete Wilson
|}

==Release==
The film had a limited release on October 15, 2010 in 537 theater screens, with an opening weekend box office of $249,428 USD in North American rentals.  

==Reception==
I Want Your Money holds a critical rating score of   at Rotten Tomatoes.

==See also==
* Economic policy of Barack Obama
* Reaganomics

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 