Tum Mere Ho
{{Infobox film
| name =Tum Mere Ho
| image =
| director = Tahir Hussain
| producer =
| writer =
| starring =Aamir Khan Juhi Chawla
| cinematography =
| editing =
| studio =
| distributor =
| released =1990
| runtime =
| country = India
| language = Hindi
}}

Tum Mere Ho is a Romantic Bollywood movie released in 1990 starring Aamir Khan and Juhi Chawla  and directed by Tahir Hussain.  

==Plot summary==
Shiva (Aamir khan) is blessed with magical snake-charming powers. When he meets Paro (Juhi Chawla) from a nearby village, he falls head-over-heels in love with her. She is also attracted to him. But Paros father, Choudhry Charanjit Singh, is not pleased with this match, and he hires men to subdue and kill Shiva, all in vain. Then Shiva attempts to rescue Paro from a shapeshifting snake, and this earns him the wrath of his mate - another shapeshifting snake, who will not rest until Shiva is dead.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Maine Daba Li Thi"
| Anuradha Paudwal
|- 
| 2
| "Sheesha Chahe Toot Bhi Jaaye"
| Udit Narayan
|-
| 3
| "Jatan Chahe Jo Karle"
| Udit Narayan, Sadhana Sargam
|- 
| 4
| "Tum Mere Ho"
| Udit Narayan, Anupama
|- 
| 5
| "Tum Mere Ho 2"
| Udit Narayan
|}

==References==
 

 
 