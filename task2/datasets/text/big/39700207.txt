National Lampoon's Bag Boy
{{Infobox film
| name           = Bag Boy
| image          = National-lampoons-bag-boy.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD Cover
| film name      = 
| director       = Mort Nathan
| producer       = 
| writer         = Hans Rodionoff
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Paul Campbell Larry Miller Robert Hoffman Andrew Gross
| cinematography = Hubert Taczanowski
| editing        = 
| studio         = 
| distributor    = Virgil Films
| released       =  
| runtime        = 94 min.
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}
 Paul Campbell and Marika Dominczyk. The plot involves a teenager who enters the competitive world of grocery store bagging. 

==Cast==
* Dennis Farina as Marty Engstrom Paul Campbell as Phil Piedmonstein
* Marika Dominczyk as Bambi Strasinsky
* Josh Dean as Freddy Robert Hoffman as Clyde Windmill Wynorski
* Nick Lashaway as Ace
* Wesley Jonathan as Alonzo Ford
* Bruce Altman as Norman
* Lisa Darr as Laurie Larry Miller as Pike
* Richard Kind as Dave Weiner
* Rob Moran as Ralph Riley
* Jeanette Puhich as Beehive Hair Woman
* Erin Hiatt as Scantily Clad Girl
* Carlos Lacamara as Julio
* Carly Craig as The girl in the museum Michael OConnell as Cart Wrangler
* Brooke Shields as Mrs. Hart

==References==
 

==External links==
 

 

 
 


 