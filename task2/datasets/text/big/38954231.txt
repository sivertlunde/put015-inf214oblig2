College Lovers
{{Infobox film
| name           = College Lovers
| image          = College_Lovers_1930_Poster.jpg
| caption        = Theatrical poster
| director       = John G. Adolfi
| producer       =
| writer         = Douglas Z. Doty
| story          = Earl Baldwin
| starring       = Jack Whiting Marian Nixon Frank McHugh Guinn Big Boy Williams
| music          = Alois Reiser
| cinematography = Frank Kesson
| editing        = Fredrick Y. Smith
| distributor    =  
| released       =  
| runtime        = 61 minutes
| country        = United States English
| budget         =
}}
College Lovers  (1930) is an talkie|all-talking Pre-Code Hollywood|pre-code comedy film produced and released by First National Pictures, a subsidiary of Warner Bros., and directed by John G. Adolfi. The movie stars Jack Whiting, Marian Nixon, Frank McHugh and Guinn Big Boy Williams. The film was based on the story by Earl Baldwin.

==Cast==
*Jack Whiting as Frank Taylor  
*Marian Nixon as Madge Hutton  
*Frank McHugh as Speed Haskins  
*Guinn Big Boy Williams as Tiny Courtley
*Russell Hopton as Eddie Smith 
*Wade Boteler as Coach Donovan 
*Phyllis Crane as Josephine Crane
*Richard Tucker as Gene Hutton
*Charles Judels as Spectator
*Pauline Wagner as Frank McHughs girl friend

==Synopsis==
Guinn Big Boy Williams, a star football player, decides to leave Sanford college after he has found that his girlfriend has eloped with another man. He is drive to the train station by Russell Hopton, his best friend, and also a football player for the same college. Jack Whiting, who plays the part of the student manager of the Sanford college athletic association as well as part of the president of the student body, knows that the college needs Williams to win the important game against Colton college. 

Whiting conspires with his girlfriend, played by Marian Nixon, to stop Williams from leaving. He also makes use of Frank McHugh, who plays the part of Whitings assistant in the film. Nixon fakes a suicide on a bridge when she notices Hopton and Williams approaching. They quickly run to help  her and both of them fall in love with her, without realizing that she really love Whiting. Williams and Hopton soon become suspicious of each other and constantly spy on each other, leaving Nixon to spend her time with Whiting. Just before the big game, Hopton and Williams have an argument and show no interest in the upcoming game. Whiting suggests that Nixon write each of them an identical love note, telling the recipient that she loves him alone. 

When Williams and Hopton receive these notes, they end their quarrelling, each thinking that Nixon prefers them to the other. Half way through the game, one of them discovers the others note and they begin  accusing each other of stealing their notes. Their fighting causes them to be benched. Colton ties the score and promises to be the winner, which so scares Hopton and Williams that they shake hands and go back into the game. When the winning touchdown for Sanford is a matter of inches away from the goal line, the two backs waste the last minute of the game trying to decide which of them will have the honor of making the final touchdown and the game ends in a tie.

==Production==
The film was planned as a full-scale musical comedy. The majority of the musical numbers of this film, however, were cut out before general release in the United States because the public had grown tired of musicals by late 1930. Although music was mentioned when the film was first released, ads and reviews soon mentioned that, even though Jack Whiting was a musical comedy star, there was no singing in the picture. These cuts accounts for the very short length of the film. The film was marketed as a straight comedy film. The complete musical film was released intact in countries outside the United States where a backlash against musicals never occurred. It is unknown whether a copy of this full version still exists.

==Songs==
Although some modern sources mention the songs "One Minute of Heaven" and "Up and At Em" as being performed in this film, they were actually written for the 1929 musical comedy The Forward Pass. Since the film is now lost, and the music was cut from circulating prints in the United States, it is not certain what the songs that were written for this picture were.

==Preservation==
No film elements are known to survive. The soundtrack, which was recorded on Vitaphone disks, may survive in private hands.

==See also==
*List of lost films

==External links==
* 

 
 
 
 
 
 
 
 