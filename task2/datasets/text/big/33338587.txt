Skinheads (film)
{{Infobox film
| name           = Skinheads
| image size     = 
| image	=	Skinheads FilmPoster.jpeg
| border         = 
| alt            = 
| caption        = 
| director       = Greydon Clark
| producer       = Greydon Clark
| writer         = Greydon Clark David Reskin
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Barbara Bain Chuck Connors Frank Noon
| music          = Dan Slider
| cinematography = Nicholas Josef von Sternberg
| editing        = Travis Clark
| studio         = 
| distributor    = Greydon Clark Productions
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Skinheads is a 1989 United States thriller film, directed, written and produced by Greydon Clark.

==Plot==
A group of white power skinhead|neo-Nazi skinheads violently rob a Jewish grocery. After they flee the scene of the crime, they stop at a roadside diner where they encounter some traveling students. The neo-Nazis terrorize the students and the owner of the diner, leaving two survivors who escape to the nearby Colorado mountains. The neo-Nazis give chase but are met with opposition when a World War II veteran living in the woods comes to the aid of the students.

==Cast==
*Barbara Bain as Martha
*Jason Culp as Jeff
*Elizabeth Sagal as Amy
*Chuck Connors as Mr. Huston
*Frank Noon as Walt

==External links==
*  
*  

 

 
 
 
 


 