48 Shades
 
 
{{Infobox film
| name           = 48 Shades
| image          = 48shadesposter.jpg
| caption        = Theatrical release poster
| director       = Daniel Lapaine
| producer       = Rob Marsala
| executive producer = Fiona Crago
  Line producer = Pam Collis
writer         = Nick Earls (novel) Daniel Lapaine Richard Wilson Emma Lung Robin McLeavy Victoria Thaine
| music          = Justin Hunter
| cinematography = Tony Luu
| editing        = Nicola Scarrott Frans Vandenburg
| distributor    = Buena Vista International
| released       =  
| runtime        = 96 minutes
| country        = Australia
| language       = English
|}} comedy by Richard Wilson, Emma Lung, Robin McLeavy, and Victoria Thaine.

It was filmed in Brisbane, Australia. School scenes from the film were filmed in the real-life Brisbane Boys College. The book on which the film is based has also been adapted into a play for La Boite Theatre Company.

==Plot==
A few months ago Dan had to make a choice. Go to Geneva with his parents for a year, board at school or move into a house with his uni student bass-playing aunt, Jacq, and her friend, Naomi. He picked Jacqs place.

Now hes doing his last year at school and trying not to spin out. Trying to be cool. Trying to pick up a few skills for surviving in the adult world. Problem is, he falls for Naomi, and things become much, much more confusing.

As Dan fumbles through the process of forming a relationship with someone of the opposite sex, he also learns about making pesto, interpreting the fish tank scene from the film Romeo + Juliet, why almost all birds are one of the 48 shades of brown, and why his best course of action is just to be himself.

==Cast== Richard Wilson as Dan
*Emma Lung as Naomi
*Robin McLeavy as Jacq
*Nicholas Donaldson as Chris Burns
*Victoria Thaine as Imogen
*Eleanor Logan as Lisa Paul Bishop as Mr. Wilkes
*Michael Booth as Phil

===Box office===
48 Shades took $193,230 at the box office in Australia. 

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 


 