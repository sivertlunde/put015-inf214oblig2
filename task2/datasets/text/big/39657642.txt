Wood Love
{{Infobox film
| name           = Wood Love
| image          = 
| image_size     = 
| caption        =  Hans Neumann 
| producer       = 
| writer         = William Shakespeare (play)   Klabund    Hans Behrendt    Hans Neumann
| narrator       = 
| starring       = Werner Krauss   Valeska Gert   Alexander Granach   Hans Albers
| music          = Hans May
| editing        =
| cinematography = Reimar Kuntze   Guido Seeber
| studio         = Neumann-Filmproduktion  UFA
| released       = 10 March 1925
| runtime        = 80 minutes
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}}
 German silent silent comedy Hans Neumann and starring Werner Krauss, Valeska Gert and Alexander Granach. It was an adaptation of William Shakespeares A Midsummer Nights Dream. 

Ernö Metzner worked as the films art director.

==Cast==
* Werner Krauss as Bottom 
* Valeska Gert as Puck 
* Alexander Granach as Waldschrat 
* Hans Albers as Demetrius 
* Charlotte Ander as Hermia  Theodor Becker as Theseus 
* Hans Behrendt   
* Wilhelm Bendow as Flaut 
* Paul Biensfeldt    Walter Brandt as Schnock 
* Tamara Geva as Oberon 
* Ernst Gronau as Squenz 
* Armand Guerra as Wenzel  Paul Günther as Egeus 
* Martin Jacob as Schlucker 
* Adolf Klein   
* Lori Leux as Titania 
* André Mattoni as Lysander 
* Fritz Rasp as Schnauz 
* Rose Veldtkirch   
* Barbara von Annenkoff as Helena 
* Ruth Weyher as Hippolyta 
* Bruno Ziener as Milon

==References==
 

==Bibliography==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.

==External links==
* 

 

 
 
 
 
 
 
 


 
 