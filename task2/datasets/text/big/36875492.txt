List of lesbian, gay, bisexual or transgender-related films of 1998
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1998. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==Films==
{| class="wikitable sortable"
! width="20%" | Title
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|-
|2by4||   ||  || Drama||
|-
|54 (film)|54||   ||  || Drama||
|-
|2 Seconds (2 secondes) ||   ||  || Comedy, Drama, Romance ||
|-
| ||   ||  || Comedy, drama||
|-
|Bedrooms and Hallways||   ||   || Romantic, comedy||
|-
|Beefcake (film)|Beefcake||   ||      || Drama||
|-
|Billys Hollywood Screen Kiss||   ||  || Romantic, comedy||
|-
|Bishonen (film)|Bishonen|| Yonfan ||   || Romantic, drama||
|-
|Bombay Boys||   ||   || Comedy drama||
|-
| ||   ||  || Documentary||
|-
|Caresses (Carícies) ||   ||  || Drama||
|-
|C-l-o-s-e-r||   ||       || Short ||
|-
|Dear Jesse||   ||  || Documentary||
|-
|Dont Tell Anyone||   ||     || Comedy, drama||
|- Edge of Seventeen||   ||  || Romantic, comedy, drama||
|-
|Finding North||   ||  || Romantic, comedy, drama||
|-
|Forever Fever||  ||   || Comedy, drama || 
|-
|From the Edge of the City (Apo tin akri tis polis) ||   ||   || Crime, drama||
|- Show Me Love (Fucking Åmål) ||   ||     || Romantic, drama||
|- Get Real||   ||   || Romantic, drama||
|-
|Gia||   ||  || Drama||
|- Gods and Monsters||   ||    || Drama||
|-
|Goodbye Emma Jo||   ||  || Short ||
|-
|Hard (film)|Hard||   ||  || Crime, drama||
|- Head On||   ||   || Drama||
|-
|High Art||   ||    || Romantic, drama||
|- Like It Is||   ||   || Drama||
|-
| ||   ||       || Drama||
|-
| ||   ||  || Romantic, comedy, drama||
|-
| ||   ||  || Romantic, comedy, drama||
|- Out of the Past||   ||   || Documentary||
|-
| ||  , Randy Barbato ||  || Documentary||
|-
|Scènes de lit||   ||  || Short||
|-
|Sex/Life in L.A.||   ||     || Documentary, adult ||
|-
|Sitcom (film)|Sitcom||   ||  || Drama, thriller, comedy||
|-
|Some Prefer Cake||   ||  || Comedy, drama, romance ||
|- Soft Hearts (Pusong Mamon) ||   ||  || Comedy||
|-
|Stiff Upper Lips||   ||     || Comedy||
|-
|The Thin Pink Line||     ||   || Comedy||
|-
|Those Who Love Me Can Take the Train (Ceux qui maiment prendront le train) ||   ||  || Drama||
|-
|Two Girls and a Baby||   ||   || Short, comedy||
|-
|Velvet Goldmine||   ||    || Drama||
|- Bob Smith
|-
|When Love Comes||   ||   || Romantic, drama||
|-
|Wild Things||   ||  || Thriller, crime, erotic||
|-
| ||   ||   || Romantic, comedy, fantasy, horror||
|- Philip Brooks ||     || Documentary||
|-
|Your Friends & Neighbors||   ||  || Comedy drama||
|}

==References==
 

 

 
 
 