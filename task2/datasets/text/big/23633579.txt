The Vyborg Side
{{Infobox film
| name           = The Vyborg Side
| image          = 
| image_size     = 
| caption        = Film poster
| director       = Grigori Kozintsev Leonid Trauberg
| producer       = 
| writer         = Grigori Kozintsev Leonid Trauberg Lev Slavin
| narrator       = 
| starring       = Boris Chirkov
| music          = 
| cinematography = Andrei Moskvin Georgi Filatov 	
| editing        = 
| distributor    = 
| studio         = Lenfilm
| released       =  
| runtime        = 3277 meters (121 minutes)
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

The Vyborg Side ( ) is a 1939 Soviet film directed by Grigori Kozintsev and Leonid Trauberg, the final part of trilogy about the life of a young factory worker, Maxim. The film was also released in the United States under the title New Horizons.

==Plot==
Following the Russian Revolution, Maksim is appointed state commissar in charge of the national bank. With great efforts, he learns the complexies of the banking trade and begins to fight off sabotaging underlings. Dymba, now a violent enemy of the Republic, tries to rob a wine store but is arrested with Maksims help. Maksim also exposes a conspiracy of a group of tsarist officers who prepare an attempt against Lenin. He then joins the Red Army in its fight against the German occupation. 

==Cast==
* Boris Chirkov - Maksim
* Valentina Kibardina - Natasha
* Mikhail Zharov - Platon Vassilievich Dymba
* Natalya Uzhviy - Yevdokia Ivanovna Kozlova
* Yuri Tolubeyev - Yegor Bugai
* Anatoli Kuznetsov - Workers Deputy Turayev
* Boris Zhukovsky - Defense Attorney
* Aleksandr Chistyakov - Mishchenko
* Nikolai Kryuchkov - soldier	
* Vasili Merkuryev - student
* Mikheil Gelovani - Stalin
* Leonid Lyubashevsky - Sverdlov
* Maksim Shtraukh - Lenin
* Ivan Nazarov - Lapshin
* Dmitri Dudnikov - Ropshin

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 

 