Move Over, Darling
{{Infobox film
| name           = Move Over, Darling
| image          = Move Over Darling - Poster.jpg
| image_size     = 225px
| alt            = 
| caption        = 1963 Theatrical poster  Michael Gordon
| producer       = Martin Melcher Aaron Rosenberg Bella Spewack Sam Spewack Leo McCarey Hal Kanter Jack Sher
| narrator       = 
| starring       = Doris Day James Garner Polly Bergen Thelma Ritter Don Knotts Chuck Connors  Edgar Buchanan
| music          = Lionel Newman
| cinematography = Daniel L. Fapp Robert L. Simpson
| studio         = 20th Century Fox
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $3,350,000 
| gross          = $12,705,882   The Numbers. Retrieved September 5, 2013. 
| preceded_by    = 
| followed_by    = 
}}
 Michael Gordon.  The picture was a remake of a 1940 screwball comedy film, My Favorite Wife, with Irene Dunne, Cary Grant and Gail Patrick. In between these movies, a version entitled Somethings Got to Give began shooting in 1962, directed by George Cukor and starring Marilyn Monroe and Dean Martin, but was never finished.

The film was chosen as the 1964 Royal Film Performance and had its UK premiere on 24 February 1964 at the Odeon Leicester Square in the presence of H.R.H. Prince Philip, Duke of Edinburgh.

==Plot==
Ellen Wagstaff Arden (Doris Day), a mother of two young girls named Jenny and Didi, was believed to be lost at sea following an airplane accident. Her husband, Nick Arden (James Garner), was one of the survivors.
 declared legally so he can marry Bianca (Polly Bergen), all on the same day. However, Ellen is alive; she is rescued and returns home that particular day. At first crestfallen, she is relieved to discover from her mother-in-law Grace (Thelma Ritter) that her (ex-) husbands honeymoon has not started yet.

When Nick is confronted by Ellen, he eventually clears things up with Bianca, but he then learns that the entire time Ellen was stranded on the island she was there with another man, the handsome, athletic Stephen Burkett (Chuck Connors) - and that they called each other "Adam" and "Eve."

Nicks mother has him arrested for bigamy and all parties appear before the same judge that married Nick and Bianca earlier that day. Bianca and Ellen request divorces before the judge sends them all away. Bianca leaves Nick, while Ellen storms out, still married to Nick, declared alive again. Ellen returns to Nicks house unsure if her children will recognize her. Her children welcome her home, and so does Nick.

==Cast==
*Doris Day as Ellen Wagstaff Arden
*James Garner as Nick Arden
*Polly Bergen as Bianca Steele Arden
*Thelma Ritter as Grace Arden
*Fred Clark as Mr. Codd
*Don Knotts as Shoe Clerk
*Chuck Connors as Adam
*Edgar Buchanan as Judge Bryson
*John Astin as Clyde Prokey
*Pat Harrington, Jr. as District Attorney
*Max Showalter as Hotel Desk Clerk

==Production notes== Bella & Sam Spewack (The 1940 film is referenced by Ellen while she is giving Bianca a massage). The story is a comedic update of the 1864 poem "Enoch Arden" by Alfred, Lord Tennyson, hence the lead characters last name. This was the seventh film version based on the Lord Tennyson poem.
 The Great Escape  and the director was George Cukor. Marilyn Monroe was fired early in its production cycle for often not showing up for shooting, ultimately appearing in only about 30 minutes of usable film.  At first, they tried to continue with Lee Remick in Monroes place, but Martin balked at working with anyone else. Monroe was re-hired but died before she could resume filming, and that version was never completed. Unable to complete the movie, and having already sunk a considerable amount of money into the production and sets, 20th Century Fox went ahead with the project, albeit with a new title, new director Michael Gordon, and a new cast (with the exception of Thelma Ritter, who was also cast as Grace Arden in the Cukor version).

James Garner accidentally broke Days rib during the massage scene when he pulls her off of Bergen. He wasnt aware of what had happened until the next day, when he felt the bandage while putting his arms around Day.
 Beverly Hills Holmby Hills. neoclassical house Italianate structure.

The producers scheduled the scene with Doris Day riding through a car wash for the last day of shooting because they were concerned that the chemicals in the detergents might affect her complexion. When the scene went off without a hitch, they admitted their ploy to Day, then used the story in promotional materials for the film.
 one of theatrical rentals. 

==Soundtrack music==
*"Move Over, Darling" – Music and lyrics by Joe Lubin, Hal Kanter and Terry Melcher (Days son) arranged by Jack Nitzsche. Sung by Doris Day and chorus (featuring ace West Coast session singers the Blossoms, featuring Darlene Love, Fanita James, and Jean King). during the opening credits and played as background music at the end. Reached #8 in the British singles chart in 1964 for Day and in 1983 for Tracey Ullman. 

*"Bridal Chorus (Here Comes the Bride)" from Lohengrin (1850) – Written by Richard Wagner. Played when Nick and Bianca arrive at their honeymoon hotel

*"Beautiful Dreamer" – Music and lyrics by Stephen Foster. Played as background music during the memorial service for Ellen

*"Twinkle Lullaby" – Music and lyrics by Joe Lubin. Sung by Ellen (Doris Day) to her children.

==References==
 

==External links==
* 
*  at Archive of American Television
*  

 
 

 
 
 
 
 
 
 
 
 
 