Ore Rektham
{{Infobox film 
| name           = Ore Rektham
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       =
| writer         =
| screenplay     = Ambika
| music          = Rajan Nagendra
| cinematography =
| editing        =
| studio         =
| distributor    = 
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Ambika in lead roles. The film had musical score by Rajan Nagendra.   

==Cast==
*Ambareesh Ambika

==Soundtrack==
The music was composed by Rajan Nagendra and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Aanandam || Krishnachandran, Lathika || Sreekumaran Thampi || 
|-
| 2 || Poovilalinja || Jolly Abraham, Lathika || Sreekumaran Thampi || 
|-
| 3 || Ravi Kandathellaam || Krishnachandran || Sreekumaran Thampi || 
|-
| 4 || Thenkaattu Veeshi || Jolly Abraham || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 