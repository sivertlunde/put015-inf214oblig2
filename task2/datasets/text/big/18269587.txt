Bad Day on the Block
 
{{Infobox film
| name           = Bad Day on the Block
| image          = Under_pressure.jpg
| image size     =
| caption        =
| director       = Craig R. Baxley
| producer       = Chris Chesser Alan Beattie
| writer         = Betsy Giffen Nowrasteh
| narrator       = David Andrews Noah Fleiss John Ratzenberger
| music          = Gary Chang
| cinematography = David Connell
| editing        = Sonny Baskin
| distributor    = Largo Entertainment
| released       =
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 directed by Craig R. Baxley. It stars Charlie Sheen and Mare Winningham. Although intended to be released in theaters, it was ultimately distributed direct-to-video. However, it was released to the theatres in some countries under the name Under Pressure; it initially premiered in Turkey. The film is sometimes also known as The Fireman.

==Plot== David Andrews) and their two children (Noah Fleiss and Chelsea Russo) are to blame for them leaving by manipulating his wife. Lyle began to hurt their children. Lyle also terrorizes Reese and Catherine Braverton. He does not excuse any accidents made by the Bravertons two children, Zach and Chelsea. Lyle continues thinking that the Bravertons are his enemies. In flashback (continuity)|flashbacks, it shows his violent tendencies, like playing Russian roulette with his wife and he being angry on his sons birthday (stemming from his own strict disciplinary upbringing). In another flashback, it is shown that Lyle left the mother of the baby to die in the fire because he considered her negligent.

Lyle still loves his wife and wants to ruin the Bravertons life, because of their happiness. One day, the Bravertons refrigerator malfunctions, and they call a repairman who gets the wrong address. The repairman, Ron, arrives at Lyles house, who makes racist remarks (as Ron reminds him of a slum lord, whose negligence of a gas leak led to many fatalities). A slanging match ensues a fight, which ends with Rons death; his mouth is superglued shut to silence his screams. During this time, the Bravertons report Lyle to the police for threatening them. Officers Al Calavito and Sandy Tierra respond to the call. Because of Lyles decorated status, Calavito and Tierra doubt the Bravertons. When they go to see Lyle, Lyle tells them that the Bravertons are lying and that Reese beats Catherine. After Lyle breaks into the Bravertons house again, Calavito and Tierra search Lyles house.  Tierra discovers the repairmans body while Lyle attacks and murders Calavito, by slamming a firemans axe into his torso.  Sandy overhears the struggle and goes to help Calavito, but she is also killed by having her neck broken (while Lyle rants disciplinary scripture). Later, Reese goes over to talk to Lyle, who savagely beats him.  After tying him up, Lyle throws Reese through the Bravertons kitchen window.

Lyle begins to play Russian roulette with the Bravertons, explaining how his late father taught him its history (along with numerous other disciplinary methods by which he abides) prior to his luck in roulette expiring. Zack stabs Lyle with a pocket knife, which only makes Lyle angrier. Catherine tries to talk to Lyle, pretending to be his ex-wife. While he is confused, Reese attacks Lyle from behind. After a brief fight, Lyle (distraught that he is being blamed for his actions) is shot by Catherine (branding him a coward). As he dies, Lyle stares at the engraving his wife inscribed on his watch (a sign of happier times). The film ends with the Bravertons spending some time as a family, trying not to notice a billboard displaying Lyle representing L.A.s fire department.

==Cast==
*Charlie Sheen - Lyle Wilder
*Mare Winningham - Catherine Braverton David Andrews - Reese Braverton
*Noah Fleiss - Zach Braverton
*Dawnn Lewis - Sandy Tierra
*Chelsea Russo -  Chelsea Braverton
*John Ratzenberger - Al Calavito
*Keone Young - Ron the Repairman
*David Hewlett - Andrew
*Cody Jones - D.J.
*Karen Brigman - Marge Wilder
*Rory Flynn - Kenny Wilder
*Michelle Moffat - Angel
*David Sutcliffe - Helicopter Observer
*Errol G. Farquharson - 1st Gangbanger
*Robert Thomas - 2nd Gangbanger
*Roy T. Anderson - 3rd Gangbanger
*Reggie Jackson - Himself

==External links==
* 

 
 
 
 
 
 
 