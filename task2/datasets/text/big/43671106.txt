Baalu Belagithu
{{Infobox film
| name           = Baalu Belagithu
| image          = 
| caption        = 
| director       = Siddalingaiah
| producer       = K S Prasad   B V Srinivas   A S Bhakthavathsalam
| writer         = Siddalingaiah
| screenplay     = Siddalingaiah
| story          = 
| based on       =  
| narrator       =  Rajkumar  Jayanthi  Bharathi
| music          = Vijaya Bhaskar
| cinematography = R. Chittibabu
| editing        = P. Bhaktavatsalam
| studio         = 
| distributor    = Chitrashree International
| released       = 1970 
| runtime        = 145 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Rajkumar in Jayanthi and Bharathi in the lead roles.  The film was released under Chitrashree International banner and produced by K S Prasad, B V Srinivas and A S Bhakthavathsalam.

== Cast == Rajkumar as Shankar / Papanna (dual roles) Jayanthi as Lalitha Bharathi as Lakshmi
* T. B. Nagappa
* Dwarakish 
* Prabhakar
* Rama
* Shanthamma
* Jyothi Lakshmi in an item song

== Soundtrack ==
The music of the film was composed by Vijaya Bhaskar and lyrics for the soundtrack written by Chi. Udaya Shankar and Vijaya Narasimha. 

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Cheluvada Muddada"
| P. B. Sreenivas, P. Susheela
|-
| 2
| "Kamalada Hoovinda"
| P. B. Sreenivas
|-
| 3
| "Neethivantha Baalale Beku"
| P. B. Sreenivas
|-
| 4
| "Hennu Aadidaaga"
| L. R. Eswari
|-
|}

==See also==
* Kannada films of 1970

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 

 