Gray Matters
 
{{Infobox film
| name           = Gray Matters
| image          = Gray matters movie poster.jpg
| caption        = Theatrical poster
| alt            = 
| director       = Sue Kramer
| producer       = Jill Footlick John J. Hermansen Sue Kramer
| writer         = Sue Kramer Heather Graham Tom Cavanagh Bridget Moynahan Molly Shannon Alan Cumming Sissy Spacek Rachel Shelley
| music          = Andrew Hollander
| cinematography = John S. Bartley
| editing        = Wendey Stanzler
| distributor    = Yari Film Group Freestyle Releasing 
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| gross          =  $59,000 
}} Heather Graham, Tom Cavanagh and Bridget Moynahan. It premiered on October 21, 2006 at the Hamptons International Film Festival and had a United States limited theatrical release on February 23, 2007.

==Plot==
Gray Baldwin (Heather Graham), a family oriented and quiet bachelorette, lives a close-knit life with her brother, Sam (Tom Cavanagh). Their lifestyle is generally co-dependent, involving them living together, going to dance classes together, etc. Gray and Sams relationship has never been thought of as strange, but once a dinner party guest mistakes the siblings as a couple, Gray and Sam decide to venture outside of one another. While discussing with each other what they can do about their single lifestyles, and how they can "hook each other up" with a significant other, their conversation continues to the park, where Gray spots a possible girlfriend for Sam, Charlie Kelsey (Bridget Moynahan). Gray introduces herself, then Charlie to Sam, and they instantly bond, agreeing to see each other again.

To Grays surprise, Charlie and Sam become engaged the morning after their meeting. Charlie and Sam are so madly in love with one another, they plan to go to Las Vegas the following morning and elope there, and invite Gray along. Gray is a little hesitant, but agrees when Charlie says they can have a "mini bachelorette party". Gray agrees and the three make their way to Vegas. While in Vegas, Gray takes Charlie out for a hen night, and after many drinks, the two share a drunken but passionate kiss. The next morning, Charlie doesnt remember anything, but Gray hasnt slept the whole night, due to the bond she felt with Charlie. The situation makes Gray finally realize that not only is she attracted to women, but is falling in love with her sister-in-law. 

The events in Vegas force a journey of self-discovery, testing the relationship between two very close siblings, and to finding happiness in lonely Manhattan. When Gray eventually comes out to Sam, he tells her hes known all along, since they were young kids.

Later, Sam accidentally discloses Grays orientation to her entire office, which, along with Sams encouragement, gives her impetus to move on to further Self-actualization|self-actualizing.

==Cast== Heather Graham as Gray
* Tom Cavanagh as Sam
* Bridget Moynahan as Charlie
* Molly Shannon as Carrie
* Alan Cumming as Gordy
* Sissy Spacek as Sydney
* Rachel Shelley as Julia Barlett
* Alejandro Abellan as Juan
* Don Ackerman as Conrad Spring
* Warren Christie as Trevor Brown
* Casey Dubois as Boy in park
* Samantha Ferris as Elaine
* Gloria Gaynor as Herself
* Emily Anne Graham as Young Gray

===Casting===
Both Alan Cumming and Rachel Shelley are significant actors to the LGBT community. Cumming is openly bisexual and an LGBT rights activist, and Shelley has become familiar to queer audiences in her role of Helena Peabody in The L Word, on which Cumming has guest-starred.

==Production==
* The film was shot on location in New York City in only twenty-one days.
* The screenplay was loosely based on the life of Carolyn Kramer, the directors sister.


==Reception==

The Movie turned to be an overall flop earning $59,619 (USA) screening through 2007.

== References == 
 

==External links==
*  
*  

 
 
 
 
 
 
 