Francis in the Haunted House
{{Infobox film
| name           = Francis in the Haunted House
| director       = Charles Lamont Robert Arthur
| writer         = Herbert H. Margolis William Raynor
| starring       = Mickey Rooney Virginia Welles Frank Skinner Herman Stein
| cinematography = George Robinson
| editing        = Milton Carruth
| distributor    = Universal-International
| released       =  
| runtime        = 80 min.
| country        = United States
| language       = English
| budget         =
| gross          = $1.2 million (US)  
}} Robert Arthur and directed by Charles Lamont. 

This is the seventh and final film in Universal-Internationals Francis the Talking Mule series, notably without series director Arthur Lubin, star Donald OConnor, or Francis voice actor Chill Wills.
 
==Plot==
Francis witnesses a murder and then befriends bumbling reporter David Prescott (Mickey Rooney), who may be next in line. With Francis help and guidance, Prescott uncovers a mystery involving murder, an inheritance, and a spooky old mansion on the edge of town.

==Cast==
*Mickey Rooney as David Prescott
*Paul Frees as The Voice of Francis
*Virginia Welles as Lorna MacLeod
*James Flavin as Police Chief Martin
*Paul Cavanagh as Neil Frazer
*Mary Ellen Kay as Lorna Ann
*David Janssen as Police Lieutenant Hopkins
*Ralph Dumke as Mayor Hargrove
*Richard Gaines as D.A. Reynolds Richard Deacon as Jason
*Dick Winslow as Sergeant Arnold
*Timothy Carey as Hugo

==Production==
This seventh and final entry in the   and voice actor Paul Frees, who did a close approximation of Wills voice as Francis.  

Mickey Rooney replaced Donald OConnor as a new but similar character, David Prescott. According to his autobiography, Rooney was originally considered for a United Artists Francis feature film before Universal acquired the rights.  

Rooneys casting was announced in January 1956. FRANCIS SERIES TO BE CONTINUED: Mickey Rooney Will Star in the Seventh Installment in Place of OConnor Columbia Seeks Josh Logan New Version of Jane Eyre
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   06 Jan 1956: 18.   

Charles Lamont was announced as the films director some weeks later. Lamont to Direct Francis
New York Times (1923-Current file)   21 Jan 1956: 18.   

Chill Wills wanted more money than Universal were willing to play, so the studio auditioned various voice actor replacements, including Mel Blanc, MMURRAY LAUDS ROAD TOURS ROLE: Actor, Once Skeptical, Sees Personal Appearances as Great Aid to Hollywood Armand Deutsch Returning
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   01 Feb 1956: 27.   before settling on Paul Frees. BROOKLYN WRITER GETS POT OF GOLD: Morton Thaw to Do Screen Play of His Video Script, Honest in the Rain Mexican Unit Organized Of Local Origin
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   14 Feb 1956: 24.  

The film made no attempt at explaining why Francis left original sidekick Peter Stirling. In the script Francis says he decided to befriend reporter Prescott because "I once lived on a farm owned by Prescotts uncle and wanted to protect his nephew out of respect for the deceased." With the original elements missing, the film, a standard tale of fake ghosts and gangsters, was poorly received; it was widely reviewed as the weakest entry in the series.

==Video releases==
The original film, Francis (1950), was released in 1978 as one of the first-ever titles in the new   (1951).

The first two Francis films were released again in 2004 by Universal Pictures on Region 1 and Region 4 DVD, along with the next two in the series, as The Adventures of Francis the Talking Mule Vol. 1. Several years later, Universal released all 7 Francis films as a set on three Region 1 and Region 4 DVDs, Francis The Talking Mule: The Complete Collection.

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 