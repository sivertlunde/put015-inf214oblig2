She Shall Have Murder
{{Infobox film
| name           = She Shall Have Murder
| image          = "She_Shall_Have_Murder"_(1950).jpg
| image_size     =
| alt            =
| caption        = British 1-sheet poster
| director       = Daniel Birt
| producer       = Guido Coen Derrick De Marney
| writer         = Allan MacKinnon
| based on       = a novel by Delano Ames
| narrator       =
| starring       = Rosamund John  Derrick De Marney 
| music          = Eric Spear
| cinematography =  Robert Navarro
| editing        =  Stefan Osiecki
| studio         = Derrick De Marney Productions (as Concanen) British Lion Films  (in association with)
| distributor    = Independent Film Distributors
| released       = December 1950	
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = 
| preceded_by    =
| followed_by    =
}} British drama film directed by Daniel Birt and starring Rosamund John, Derrick De Marney and Felix Aylmer.  A law office clerk who aspires to be a crime writer, turns into a detective when someone at her work is murdered.

==Cast==
* Rosamund John ...  Jane Hamish
* Derrick De Marney ...  Dagobert Brown
* Mary Jerrold ...  Mrs. Robjohn
* Felix Aylmer ...  Mr. Playfair
* Joyce Heron ...  Rosemary Proctor Jack Allen ...  Maj. Stewart
* Henryetta Edwards ...  Sarah Swinburne
* Harry Fowler ...  Albert Oates John Bentley ...  Douglas Robjohn
* Beatrice Varley ...  Mrs. Hawthorne
* June Elvin ...  Barbara Jennings
* Jack McNaughton ...  Barman
* Olaf Pooley ...  Mr. White
* Dennis Val Norton ...  Pub Landlord
* Francis de Wolff ...  Police Inspector
* Jonathan Field ...  Darts Player
* Jimmy Rhodes ...  Racing Man
* Tony Hilton ...  Steward
* Frances Leak ...  Shooting Gallery Attendant
* Wanda Rands ...  Change Girl
* Duncan Lamont ...  Police Sergeant

==References==
 

==External links==
* 

 
 
 
 
 

 