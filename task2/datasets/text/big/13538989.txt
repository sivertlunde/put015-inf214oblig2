A Blade in the Dark
{{Infobox film
| name = A Blade in the Dark
| image = A Blade in the Dark poster.jpg
| alt =
| caption = Italian theatrical release poster
| director = Lamberto Bava
| producer = Lamberto Bava Mino Loy  (uncredited)  Luciano Martino  (uncredited) 
| writer = Elisa Briganti Dardano Sacchetti
| starring = Andrea Occhipinti Anny Papa Fabiola Toledo Michele Soavi Valeria Cavalli
| music = Guido De Angelis Maurizio De Angelis
| cinematography = Gianlorenzo Battaglia
| editing = Lamberto Bava
| studio = National Cinematografica Nuova Dania Cinematografica
| released = 6 August 1983
| runtime = 110 minutes
| country = Italy
| language = English
}}

A Blade in the Dark (Italian: La casa con la scala nel buio) is a 1983 Italian giallo film directed by Lamberto Bava.

== Plot ==
Bruno, a composer, becomes involved in a series of murders of tenants who live in a villa. In a horror film Bruno is scoring a young child, taunted by cruel bullies, descends into a dark cellar after a bouncing tennis ball. The kids hear a scream and the ball bounces up to them, leaving bloody tracks on the wall. Sandra, Brunos director, explains that her inspiration was the childhood of Linda, the villas previous tenant.

== Cast ==
* Andrea Occhipinti as Bruno
* Michele Soavi as Tony Rendina/Linda
* Lara Naszinsky/Lamberti as Julia
* Fabiola Toledo as Angela
* Anny Papa as Sandra
* Stanko Molnar as Giovanni
* Valeria Cavalli as Katia

== Release ==
The film was released three times on DVD in the United States. The first time in 2001 by Anchor Bay Entertainment.  The company subsequently re-released it in 2003 on a double feature DVD with Lamberto Bavas Macabre (1980 film)|Macabre.  Both these versions are out of print. The third DVD release came from Blue Underground in 2007. 

== Critical reception == AllMovie gave the film a mixed review, writing, "Lamberto Bava eschews complex mystery in favor of elaborate stalk-and-slash sequences, with only partial success". 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 