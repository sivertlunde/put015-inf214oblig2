Jiang hu: The Triad Zone
 
  Jiang Hu.
{{Infobox film
| name           = Jiang hu: The Triad Zone
| image          = Jiang-hu-the-triad-zone.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Dante Lam
| producer       = 
| writer         = Chan Hing-kai Amy Chi
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Anthony Wong Eason Chan Law Lan
| music          = Tommy Wai
| cinematography = Cheung Man-po
| editing        = Chan Ki-hop
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$ 1,626,690 
| preceded by    = 
| followed by    = 
}} Hong Kong film directed by Dante Lam.

==Cast and roles==
* Tony Leung Ka-fai - Jimmy Yam
* Sandra Ng - Sophie Yam
* Anthony Wong Chau-sang - Master Kwan Wan Cheung
* Eason Chan - Chan Chin-Wah
* Jo Kuk
* Roy Cheung - Ho Kwun Yue
* Lee Siu-kei - Kei
* Chan Fai-hung (credited as Chan Fai Hung)
* Law Lan - Keis wife
* Lee San-san - Jo Jo Cheung
* Samuel Pang - Tiger
* Eric Tsang	
* Lee Lik-Chi	
* Richard Ng	
* Ann Hui	
* Maria Chung		
* Lam Chi-Sing - Jojos Brother
* Carl Ng - Carl
* Ng Doi-yung - Big Mouth
* Siu Leung - Luk See
* Chapman To

==Notes==
 

==External links==
*  
*  
*  

 

 
 
 
 