Forever Friends (film)
{{Infobox film
| name           = Forever Friends
| image          = ForeverFriends1996filmhkposter.jpg traditional = 四個不平凡的少年, 號角響起 simplified = 四个不平凡的少年, 号角响起 jyutping = Sei goh bat ping faan dik siu nin}}
| border         = 
| alt            = 
| caption        = Forever Friends HK DVD cover
| director       = Chu Yin-ping
| producer       = Hui Pooi-Yung
| writer         = Wu Nien-Jen
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Nicky Wu Takeshi Kaneshiro Jimmy Lin Alec Su
| music          = 
| cinematography = Chan Shu-Wing
| editing        = Chen Bo-Wen, Mui Tung-Lit
| studio         = Chang Hong Channel Film & Video Ltd. Co. (HK)
| distributor    = Upland Films Corporation Limited Mei Ah Entertainment
| released       =   
| runtime        = 83 minutes
| country        = Taiwan Mandarin Cantonese 
| budget         = 
| gross          = HK $149,270.00 
}}
 
 Taiwanese   war film|war-comedy film directed by Chu Yin-ping, starring Taiwans popular "four little heavenly kings" (Nicky Wu, Takeshi Kaneshiro, Jimmy Lin and Alec Su. The original Taiwanese version of the film in Mandarin Chinese language was released in 1995 under the title "號角響起"  (pinyin: Hao jiao xiang qi) which literally translate to "sounded horns".  With the raising popularity of the "four little heavenly kings" in Hong Kong, the film was dubbed in Cantonese and renamed to a more comical title which literally translate to "four not normal youngsters" in order to cater more to the Hong Kong audiences. 

== Synopsis ==
In the mid 1990s with rumor of Mainland China declaring war on Taiwan, both sides tense up afraid of war breaking out. Taiwan in order to show their military power against Mainland gives all of their troops a strict intense war training in order to prepare for war.

Captain Li Wei-Han (Nicky Wu) is the newly appointed captain of a "Special Squad" unit which consist of misfits and rejects from other army units. He is young for his ranking which leads to others mistaking him as a private at first. He has high expectations for his unit and tells him he will lead them to succeed. On the first official day of training he is embarrassed by his unit in front of his superiors when his unit doesnt even arrive at the finish point of training but all of them falls asleep in the middle of the training field instead. This leads to him being strict and stern with them which makes his while unit hate him, but at the end his entire squad realize his a caring leader that has always looked out for them.

Private Luo Zhi-Jiang (Alec Su), whose specialty is only studying and retaking college entrance exams is a clumsy clots. He has taken his college exam three times without passing, without being able to attend any universities he is forced to serve his army term.  Previously the army DJ in his former unit, he accidentally played the wrong song on the loud speaker and was told to pack his belongings and sent to "Special Squad" unit. He is weak and cowardly which makes it hard for him to complete most of his training tasks. Every night he writes in a journal of the hardship he has endured while serving in the army and counting the days until his mandatory army term is over.

Private Li Ta-Wei (Takeshi Kaneshiro), is a former triad member with a huge tattoo on his left shoulder, because of his past and physical appearance he is stereotype with triad comments from the captain. He previously servered a two year prison sentence for gang fighting and killing someone, and regrets his triad past because his triad boss never once visited him while he in jail. He thinks due to his triad past no one will ever want to become close to him and that he will never be respected as a human being. He has a hot temper and often gets into fights with troops from other units.

Private Zeng Zhi-Xiang (Jimmy Lin), is a self-appointed fortune teller master and wizard who can put a curse on someone. A master at courting girls, he has two wives back at home. He was transferred to the "Special Squad" unit because while trying to get on the good side of his previous captain he gave his captain a few lucky numbers to gamble with which didnt work and caused his previous captain to lose a big sum of money. His previous captain put him through intense training as punishment for giving him phony lucky numbers, while yelling at Zhi-Xiang, the captain suffered an epilepsy seizure. No one believed the captain had an epilepsy seizure and claimed that Zhi-Xiang had put an hex on the captain for punishing him.

Due to Zhi-Jiang clumsiness during training, captain Li is constantly given a stern scolding of his inadequate to properly train an army unit. Soon the "Special Squad" is label as the "Cheerleading Squad" of the army by captain Lis superiors and constantly made fun of by the other army squads. During a visit by the army corporal,  Zhi-Jiang is asked by the corporal on his daily life as an army soldier. He tells the corporal frankly about how he feels and is punished by the sergeant for not knowing how to speak words, while the a private from another squads tells the corporal about how much he is enjoying army life. The corporal ask the other private to extend his army term since he is enjoying it so much. Not wanting to serve an extended army term the private admits to lying to the corporal. The corporal is furious and tells the entire army squads that they should be truthful in their words.

During a mock war training exercise privates Luo, Li, Zeng and Wei are assigned to distribute water and retrieve water for the troops on their team that is participating in the mock war. While driving around the training grounds they encounter the rival army team, not wanting to be capture they fight till their deaths until the captain for the rival team tells his troops to move on as they dont want to waste time dealing with four lowly privates on the other team. The four use the opportunity of not being captured to go into civilian town and visit Zhi-Xiangs wives and Ta-Weis mother. On their way back to the army base they encounter all the superiors from the rival team at a breakfast stand, since the superiors from the rival team were in the middle of eating and not prepared for combat the four ends up capturing all the higher superiors from the rival units and ends up winning the mock war for their squad teams.

== Cast ==
*Nicky Wu as Captain Li Wei-Han
*Takeshi Kaneshiro as Private Li Ta-Wei
*Jimmy Lin as Private Zeng Zhi-Xiang
*Alec Su as Private Luo Zhi-Jiang
*Wu Ming as Ta-Wei mother
*Cheung Laap-Wai as Private Wei Yu-nan 
*Wang De-Zhi as Army Sergeant
*Cheung Ching-Yung as Private Li Wei Hon 

== Facts ==
  compulsory military service.
*Jimmy Lin was the more popular of the "four little heavenly kings" and had an established fan base in Hong Kong in the mid 90s. After the filming of the film Lin was required to serve his compulsory military service which saw his popularity in both Taiwan and Hong Kong diminish when the film was released in Hong Kong. 
*After the finish of filming, Nicky Wu and Alec Su from the popular Taiwanese boyband Xiao Hu Dui officially disbanded and embarked on solo careers. 
*The film was released in Japan with Takeshi Kaneshiro billed as the main star due to his growing popularity in the country.

== Production credits ==
*Production Manager: Lee Ging-Suen, Chan Gei-Yuen
*Assistant Director: Chung Bing-Wong, Chui Gwok-Yin
*Art Director: Lu Chun
*Script Supervisor: Kelly Mang Cheung-Lee
*Lighting: Choi Sam-Kat
*Costume Designer: Lok Sai-Gwong
*Makeup: Yau Yee-Chu
*Props: Lam Sing-Kwok
*Presenter: Ng Dui

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 