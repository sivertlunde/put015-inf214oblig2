Playing by Heart
{{Infobox film 
  | name = Playing by Heart
  | image = Playing_by_heart.jpg
  | caption = 
  | director = Willard Carroll 
  | producer = Willard Carroll Meg Liberman
  | writer = Willard Carroll
  | starring =Gillian Anderson Ellen Burstyn Sean Connery Anthony Edwards Angelina Jolie Jay Mohr Ryan Phillippe Dennis Quaid Gena Rowlands Jon Stewart Madeleine Stowe John Barry
  | cinematography =Vilmos Zsigmond
  | editing =Pietro Scalia
  | studio = Hyperion Pictures
  | distributor = Miramax Films
  | released =  
  | country = United States
  | language = English
  | runtime = 121 minutes
  | budget = $14,000,000
  | gross = $3,970,078
    }}
Playing by Heart is a 1998 American comedy-drama film, which tells the story of several seemingly unconnected characters. It was entered into the 49th Berlin International Film Festival.   

==Plot==
Among the characters are a mature couple about to renew their vows (Sean Connery and Gena Rowlands); a woman (Gillian Anderson) who accepts a date offer from a stranger (Jon Stewart); a gay man dying of AIDS (Jay Mohr) and his mother (Ellen Burstyn) who has struggled to accept him; two clubbers who meet in a nightclub (Ryan Phillippe and Angelina Jolie); a couple having an affair (Anthony Edwards and Madeleine Stowe) and a man (Dennis Quaid) who tells his tragic life story to a woman he meets in a bar (Patricia Clarkson), but seems to have a strange connection to another mysterious woman. As the film continues and the stories evolve, the connections between the characters become evident. Kellie Waymire, Nastassja Kinski, Alec Mapa, Amanda Peet and Michael Emerson also have roles in the film.

==Cast==
*Gillian Anderson as Meredith
*Ellen Burstyn as Mildred
*Sean Connery as Paul
*Anthony Edwards as Roger
*Angelina Jolie as Joan
*Jay Mohr as Mark
*Ryan Phillippe as Keenan
*Dennis Quaid as Hugh
*Gena Rowlands as Hannah
*Jon Stewart as Trent
*Madeleine Stowe as Gracie

==Reception==
Playing by Heart received mixed to positive reviews from critics and currently holds a 60% "fresh" rating on Rotten Tomatoes based on 53 reviews. The consensus summarizes: "Its overly talky, but Playing by Heart benefits from witty insights into modern relationships and strong performances from an esteemed cast."

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 


 