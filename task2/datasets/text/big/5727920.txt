We Own the Night (film)
 
{{Infobox film
| name           = We Own the Night
| image          = We Own The Night poster.jpg
| caption        = Theatrical release poster James Gray Nick Wechsler
| writer         = James Gray
| starring       = Joaquin Phoenix Mark Wahlberg Eva Mendes Robert Duvall
| music          = Wojciech Kilar
| cinematography = Joaquín Baca-Asay
| editing        = John Axelrad
| studio         = 2929 Productions
| distributor    = Columbia Pictures Universal Pictures (international)
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = $21 million 
| gross          = $54,926,886
}} James Gray and starring Joaquin Phoenix, Mark Wahlberg, Eva Mendes and Robert Duvall. It is the third film directed by Gray, and the second to feature Phoenix and Wahlberg together, the first being The Yards.

The title comes from the motto of the New York City Police Department|NYPDs Street Crimes Unit, which disbanded in 2002.

The film premiered May 25, at the 2007 Cannes Film Festival.    It was released October 12, 2007 in the United States and Canada. It was released in the United Kingdom on December 14, 2007 and in Australia on February 28, 2008.

==Plot==
In 1988 in Brooklyn|Brooklyn, New York, Bobby Green (Joaquin Phoenix) is the manager of the successful El Caribe nightclub in Brighton Beach that is frequented by Russian black market gangster and drug lord Vadim Nezhinski (Alex Veadov), and owned by Marat Buzhayev, Vadims uncle and Bobbys boss.

Bobby has distanced himself from his father, NYPD Deputy Chief Burt Grusinsky (Robert Duvall), and his brother, Captain Joseph Grusinsky (Mark Wahlberg). He uses the maiden name of his mother, Carol Green, as his last name, preferring to remain on the sidelines and enjoy a hedonistic life with his girlfriend Amada Juarez (Eva Mendes) and best friend Louis "Jumbo" Falsetti (Danny Hoch).

When Joseph leads a police raid on El Caribe in the hopes of arresting Vadim, Bobby refuses to cooperate. The incident strains Bobbys relationship with his father and brother even more, to the point that Bobby and Joseph come to blows.
 listening device hidden in a cigarette lighter, but when the device is discovered, he narrowly escapes being murdered, and the police raid the operation and arrest Vadim.

Bobby and Amada are placed in protective police custody, and their relationship begins to deteriorate. Months later, Vadim escapes custody while being transported to a hospital. The police prepare to move Bobby and Amada to a new location. During a blinding thunderstorm, the police convoy is intercepted by Vadims men, and during a chaotic car chase, Burt is fatally shot. Bobby passes out in the rain when he sees his fathers body.

The police take Bobby and Amada back to a hotel near Kennedy Airport. Bobby wakes up a few hours later and finds Joseph in the hotel room. After Joseph tells him that their father died, the grief-stricken Bobby asks how "they" found them. At the subsequent funeral, a colleague of Josephs, Captain Jack Shapiro, gives him Burts Korean War medal. Bobby is told that a Russian shipment of cocaine is arriving sometime in the coming week.

To avenge his father, Bobby decides to officially join the police force without the consent of Amada, who leaves him. After he is sworn into the NYPD, Bobby, now in uniform, learns the true involvement of Jumbo, his friend, and Marat, Vadims uncle. He and Joseph organize a final sting operation, set for April 4, 1989. During the raid, Joseph is emotionally incapacitated by the memory of his shooting and cannot continue. Vadim flees into the reed beds, and the police toss in flares to smoke him out. As the beds are engulfed in flame and smoke, Bobby runs in to find Vadim himself, ignoring the other officers pleas that he wait. Bobby shoots Vadim in the chest, mortally wounding him.

Nearly a year after the raid on El Caribe, Bobby graduates from the NYPD Police Academy to become a full-time police officer. Before the ceremony, Joseph reveals to Bobby that he has decided to switch to a job in the administration sector, since the shooting led him to realize that he needs to spend more time with his children. As the chaplain announces that Bobby is to give the valedictory speech, Bobby thinks he sees Amada in the audience, but it turns out to be an illusion. Bobby and Joseph express their brotherly love.

==Cast==
* Joaquin Phoenix as Robert "Bobby" Green/Grusinsky
* Mark Wahlberg as Captain Joseph "Joe" Grusinsky
* Eva Mendes as Amada Juarez
* Robert Duvall as Deputy Chief Albert "Burt" Grusinsky
* Alex Veadov as Vadim Nezhinski
* Dominic Colon as Freddie
* Danny Hoch as Louis "Jumbo" Falsetti
* Oleg Taktarov as Pavel Lubyarski
* Moni Moshonov as Marat Buzhayev
* Antoni Corone as Lieutenant Michael Solo
* Craig Walker as Russell De Keifer
* Tony Musante as Captain Jack Shapiro
* Yelena Solovey as Kalina Buzhayev Coati Mundi as Himself

* Former New York City Mayor Ed Koch, who was Mayor during the time frame in which the film was set,  makes a cameo appearance as himself.

==Reception==
The film received mixed reviews from critics. As of May 2009 on the review aggregator Rotten Tomatoes, 55% of critics gave the film positive reviews, based on 133 reviews.  On Metacritic, the film had an average score of 59 out of 100, based on 33 reviews. 

In its opening weekend in the United States and Canada, the film grossed $10.8 million in 2,362 theaters, ranking #3 at the box office.  The film grossed a total of $54.5 million worldwide &mdash; $28.5 million in the United States and Canada and $26.0 million in other territories. 

The film was a commercial success in the United States, as Sony Pictures only paid $11 million for the rights to distribute this film.  Sony Pictures released this film through its Columbia Pictures division.

The film has been a hit in the United States DVD market, as it has brought in more than $22 million in DVD sales  and more than $32 million in DVD rentals. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 