Blue Money (film)
 
{{Infobox film
| name           = Blue Money
| image          = Blue Money (Movie Poster, 1972).jpg
| image_size     = 220px
| alt            = 
| caption        = Original release poster
| director       = Alain Patrick Robert C. Chinn
| writer         = Alain Patrick
| story          = Nick Boretz Bob Chinn
| starring       = Alain Patrick Barbara Caron Inga Maria Jeff Gall
| music          = Jay Torrey
| cinematography = Michael Stringer
| editing        = Eugene Roth
| studio         = Nessa Productions
| distributor    = Crown International Pictures
| released       =  
| runtime        = 77 minutes   93 minutes (extended cut)
| country        = United States
| language       = English
| budget         = 
| gross          = $8,188 (First Weekend) http://www.tcm.com/tcmdb/title/739621/Blue-Money/notes.html 
}} soft core porn film written and directed by Alain Patrick as Alain-Patrick Chappuis and based upon a story by Nick Boretz. The film is distributed by Crown International Pictures.

==Plot==
Yearning to make real films in early 1970s Hollywood, California, 25-year-old French-Canadian Jim DeSalle gets caught up in the adult film industry, trying to support himself and his wife Lisa (Barbara Caron), with their Baby. They seem to have the perfect life, but it all falls apart.
 Vice Squad for being involved in making and distributing pornographic films. Everything starts to unravel for Jim:  The police begin a series of raids, his "sleazy distributors" wont pay him, he has a fling with an actress, his wife takes their child and leaves him, and his the creditors are seeking to seize his assets and evict him from his beach house. 

It seemed like he had everything at the beginning of the film, it now looks as if hes going to be left with nothing. 

It may be too late when Jim and Lisa finally realize that all they really want to do is to get on their boat, with Baby, and sail to the middle of the ocean, to get as far away from it all as they can. Just as Jim reveals to the Vice Cop.

(The uncredited role of the Vice Cop was done as a favor to director Alain Patrick by his friend Gary Kent).

==Cast==
* Alain Patrick as Jim
* Barbara Mills as Lisa
* Inga Maria as Ingrid
* Jeff Gall as Mike
* Oliver Auuey as The Fatman
* Steve Rourson as Freddie
* Maria Arnold, Susanne Fields, Eve Orlon, Sandy Dempsey, Susan Wescott, and Leslie Otis as Models
* Vanessa Chappuis as Baby
* John Parker as Benny
* Alex Elliot as Larry Bob Chinn as Narrator

==Production== Bob Chinn) describing black-and-white stills and video footage (in the fictional film stated as coming from the police investigation surveillance films). 

Some of those involved in the project also worked on a number of other films, such as the two leads, Alain Patrick, who worked on over 26 other film and television projects, such as Time Tunnel, Harry O, and Ironside (TV series)|Ironside; and, Barbara Caron (Barbara Mills), who performed in The Stewardesses and Executives Wives, among her 38 other film projects.  

As many of those who worked on the film have also worked in the industry the film portrays, the movie itself is a quasi-documentary.

===Release=== VOD with different edits and run times. It is also in a number of DVD collections of similarly themed Low-budget film|low-budget films distributed by Crown International Pictures, such as Sextette, The Sister-in-Law and The Virgin Queen of St. Francis High, by Mill Creek Entertainment and others, as well as being shown on television.

===Soundtrack===
Song, "Walk in the Sun", lyrics by Clarence Morley, music and song by Jay Torrey 

==Reception== Bob Chinn, cinematographer R. Michael Stringer, and performers Barbara Mills, Maria Arnold, Sandy Dempsey, Eve Orlon, and Suzanne Fields. In a "very well made film with a good story and some great performances" which "paints a fairly unhappy picture of the adult film industry",  as a "small scale version of Boogie Nights", from some of those involved, at the time. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 