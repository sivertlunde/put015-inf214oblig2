Next Door (1994 film)
{{Infobox film
| name           = Next Door
| image          = Nextdoorvhs.gif
| caption        = 1995 VHS cover
| director       = Tony Bill
| producer       = Jay Benson, Barney Cohen
| writer         = Barney Cohen
| starring       = James Woods Randy Quaid Kate Capshaw Lucinda Jenney
| music          = Van Dyne Parks
| cinematography = Thomas Del Ruth
| editing        = Axel Hubert
| studio         = Showtime Networks Nederlander Television and Film Productions, Inc. Tudor Entertainment, Inc. TriStar Television
| released       =  
| runtime        = 95 min.
| country        = United States English
}} Showtime network and was made available on VHS January 17, 1995. As of January 2009, the film has yet to find a DVD release.

==Plot==
Matt (Woods), a sophisticated college professor and Karen (Capshaw), his schoolteacher wife, have inconsiderate neighbours (a loutish beer-swilling butcher and his wife, played by Quaid and Jenney) whose lawn sprinkler drowns their flowers. A feud erupts and as a series of tit-for-tat actions escalate, they also start to get crueler and more destructive.

==Cast==
*James Woods as Matt Coler
*Randy Quaid as Lenny Benedetti
*Kate Capshaw as Karen Coler
*Lucinda Jenney as Marci Benedetti
* Miles Fuelner as Bucky

==References==
*  

==External links==
*  at Facebook
* 
 
 
 
 
 
 