The Grinning Face
{{Infobox film
| name           = The Grinning Face
| image          = 
| image_size     = 
| caption        = 
| director       = Julius Herska 
| producer       = 
| writer         = Victor Hugo (novel)   Louis Nerz
| narrator       = 
| starring       = Franz Höbling   Nora Gregor   Lucienne Delacroix   Anna Kallina
| music          = 
| editing        =
| cinematography = Eduard Hoesch
| studio         = Olympic-Film  
| distributor    = 
| released       = 18 March 1921
| runtime        = 87 minutes
| country        = Austria
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| 
}} Austrian silent silent horror film directed by Julius Herska and starring Franz Höbling, Nora Gregor and Lucienne Delacroix. It is an adaptation of the 1868 novel The Man Who Laughs by Victor Hugo. 

==Cast==
* Franz Höbling as Gwynplaine 
* Nora Gregor as Herzogin Josiane 
* Lucienne Delacroix as Dea  Anne of England 
* Eugen Jensen as Barkilphedro 
* Armin Seydelmann as Lord Bolinbroke 
* Franz Weißmüller as Ursus 
* Jimmy Court as Lord David Dirry-Moir  Josef Moser as King James II of England 
* Robert Balajthy as Lord Linäus Claincharlie 
* Susanne Osten as Lady Dirry-Moir 
* Arped Kramer as Dr. Gerardus 
* Fritz Strassny as Dr. Hardqusnonne

==References==
 

==Bibliography==
* Prince, Stephen. The Horror Film. Rutgers University Press, 2004.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 