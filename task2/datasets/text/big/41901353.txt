Paper Planes (film)
 
{{Infobox film
| name           = Paper Planes
| image          = paperplanesposter.png
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical film poster
| film name      = 
| director       = Robert Connolly
| writer         = Robert Connolly   Steve Worland
| producer       = Robert Connolly   Liz Kearney   Maggie Miles
| starring       = Sam Worthington   Julian Dennison   David Wenham   Ed Oxenbould
| music          = Nigel Westlake
| cinematography = Tristan Milani
| editing        = Nick Meyers
| Sales        = Arclight Films
| distributor    = Roadshow
| released       =  
| runtime        =96 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          =A$9.45 million 
}}

Paper Planes is an Australian family drama film directed by Robert Connolly which he co-wrote with Steve Worland. The film stars Sam Worthington, David Wenham, Julian Dennison and Ed Oxenbould.

The film tells a story about a young boy who lives in Western Australia,  Dylan, who dreams of competing in the World Paper Plane Championships in Japan.

It opened in Australian cinemas on 15 January 2015 on 253 screens. It has grossed A$9.45 million at the Australian box office as of 24th of March, 2015.

The story is loosely based on an episode of Australian Story called Fly With Me. 

== Synopsis == RAAF fighter-pilot Terry Norris), a kite-hawk (Milvus migrans) he feeds on his way to school and later a Japanese paper plane champion Kimi (Ena Imai).

== Cast ==
* Sam Worthington as Jack  
* Ed Oxenbould as Dylan  
* David Wenham as Patrick  
* Deborah Mailman as Maureen
* Ena Imai as Kimi
* Nicholas Bakopoulos-Cooke as Jason Terry Norris as Grandpa Peter Rowsthorn as Mr. Hickenlooper
* Julian Dennison as Kevin

== Production ==
On 9 November 2013 it was announced that filming had begun in Perth, Western Australia and Tokyo with Robert Connolly directing.    Sam Worthington, David Wenham, Julian Dennison and Ed Oxenbould are starring in the film. 

== Reception == The Sapphires, which was released in August 2012. 
 The Bank, Three Dollars and Balibo (film)|Balibo, delivers an adorable family film that is uplifting, warm, winning and, most of all, funny."

Fiona Williams of sbs.com.au says "theres a lot to like in Paper Planes  ideas about ingenuity and resilience, and that may bode well for getting bums off the beach and onto seats in the films late summer school holiday release period."

== Book ==

Steve Worland, who co-wrote Paper Planes, novelised the screenplay into a best-selling book for young readers. It was published on 2nd January 2015 through Puffin, a division of Penguin Random House. It includes directions on how to fold a paper plane and photographs from the movie.

== References ==
 

== External links ==
*  
*   at  

 
 
 
 
 
 