Moments of Fiction
{{multiple issues|
 
 
 
}}

Moments of fiction is a 2011 multilingual Emarati film presented by M M Productions. Produced by Naim Zaboura and Neda Ahmed.  Directed and written by Mohammed Mamdouh, Screen played by Mohammed Mamdouh and Nicholas Karavatos. Set in The United Arab Emirates; Fujairah, Dubai, and Sharjah
.

==Plot==
A movie that tells the story of a 19 year-old young boy by the name of Mohammed Salaheldin, who began his Film School at his University in Dubai, with two of his oldest friends; Kane and Mike. Consecutively, he is followed and taunted by his past and is faced to share it with a psychiatrist. Further into the movie, his past is revealed and the audience is shown what had happened to Mohammed before his joining of the University. The main focus of the Film is on Mohammeds past relationship with a lover that is attempting to resolve her dissimilarity with him. While searching for material at the University library one day, he encounters a bare, mysterious, and impulsive girl who works there. Her refusal to share her name and her attitude towards life leaves Mohammed intrigued. Eventually, not only is she his muse but his best friend, and potentially his lover. Trusting the unusual nature of the library girl, he exposes his obscurest memories to her, helping Mohammed mend his passionate nature and initiating him on a refreshing journey of self-discovery.

==Cast==
* Rik Aby as Dr.Fields
* Victoria Borasio as Lauren
* Jasmine Henricson as Lesley
* Mohammed Mamdouh as Mohammed
* Duncan Murray as Kane
* Filip Ranebo as Mike 0ne
* Reena Sadek as Library Girl
* Anthony Tassa as Professor Virgil

==Crew members==

;Directed by
* Mohammed Mamdouh - director

;Produced by
* Neda Ahmed - line producer
* Mohammed Al Shaibani - associate producer
* Abeer Ali - co-producer
* Nabeel Butt - co-producer
* Mohammed Mamdouh - executive producer
* Naim Zaboura - producer

;Cinematography by
Vincent Van der Plas   (director of photography)

;Film Editing by
* Neda Ahmed - edited by
* Mohammed Mamdouh - co-editor

;Second Unit Director or Assistant Director
* Abeer Ali - second assistant director
* Nabeel Butt - first assistant director

;Art Department
* Mohammed Al Shaibani - storyboard artist

;Special Effects by
* Piotr Szablowski - special effects

;Camera and Electrical Department
* Nabeel Butt - additional camera operator
* Nabeel Butt - camera operator: second unit
* Nabeel Butt - first assistant camera
* Mohammed Mamdouh - camera operator
* Mohammed Mamdouh - camera operator: second unit
* Vincent Van der Plas - camera operator

;Animation Department
* Mohammad Ahmed Fikree - animation director

==External links==
* Moments of fiction at IMDb http://www.imdb.com/title/tt1841747/fullcredits#cast
* M-M Productions official website http://mm-films.webs.com/momentsoffiction11.htm
* Moments of Fiction official Facebook Page https://www.facebook.com/MomentsOfFiction?sk=info
* Moments of Fiction Official trailer http://www.youtube.com/watch?v=bNaoirJfbGY
* Directors website http://www.mohammedmamdouh.com

==See also==
For other movies created or partly filmed in Dubai, see one of the following:
*City of Life The Fallen
*100 Miles
*Dam 999
* 

 

 
 
 