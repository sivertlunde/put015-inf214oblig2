A Talent for Loving (film)
{{Infobox film
| name           = A Talent for Loving
| image          =
| image_size     =
| caption        =
| director       = Richard Quine
| producer       = Walter Shenson
| writer         = Richard Condon
| narrator       =
| starring       = Richard Widmark
| music          =
| cinematography =
| editing        =
| distributor    = Paramount Pictures
| released       = 1969
| runtime        = 110 min.
| country        = United Kingdom   United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}} comedy Western Western film directed by Richard Quine, and based on the 1961 parodic Western novel A Talent for Loving, or The Great Cowboy Race by Richard Condon, who also wrote the screenplay. The film stars Richard Widmark and Cesar Romero.
The home video version of the film (Simitar Entertainment) is re-titled "Gun Crazy" and has been edited to 95 minutes. 

==Plot==
A gambler named Patten wins the deed to a Mexico ranch, but finds a curse has been placed on the place. Don Jose must marry off his beautiful, nymphomaniacal daughter to permanently free her of the curse.

==Cast==
*Richard Widmark as Major Patten
*Cesar Romero as Don Jose
*Caroline Munro as Evalina
*Chaim Topol as Molina
*Fran Jeffries as Maria
*Derek Nimmo as Moodie
*Max Showalter as Franklin
*Joe Melia as Tortillaw
*John Bluthal as Martinelli
*Libby Morris as Jacaranda
*Judd Hamilton as Jim
==NOTE==
In December 1965, Walter Shenson offered "A Talent for Loving" to Brian Epstein as a film vehicle for The Beatles. Unfortunately, they rejected it unanimously.
==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 