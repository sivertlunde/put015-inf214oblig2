Bhagyada Bagilu
{{Infobox film 
| name           = Bhagyada Bagilu
| image          =  
| caption        = 
| director       = K. S. L. Swamy (Ravi)
| producer       = B H Jayanna
| writer         = Dore
| screenplay     = Ravi Rajkumar Balakrishna Balakrishna Dwarakish K. S. Ashwath in Guest Appearance
| music          = Vijaya Bhaskar
| cinematography = K Janakiram
| editing        = Bal G Yadav
| studio         = S J K Productions
| distributor    = S J K Productions
| released       =  
| country        = India Kannada
}}
 1968 Cinema Indian Kannada Kannada film, directed by K. S. L. Swamy (Ravi) and produced by B H Jayanna. The film stars Rajkumar (actor)|Rajkumar, Balakrishna (Kannada actor)|Balakrishna, Dwarakish and K. S. Ashwath in Guest Appearance in lead roles. The film had musical score by Vijaya Bhaskar. 

==Cast==
  Rajkumar
*Balakrishna Balakrishna
*Dwarakish
*K. S. Ashwath in Guest Appearance
*Prasad in Guest Appearance
*B. Vijayalakshmi
*B. V. Radha
*B. Jaya (actress)|B. Jaya
*B. Jayashree
*Papamma
*Mysore Srinivas
*Srirangamurthy
*Shastry
*Venkatesh
*Narayan
*Raju
*Venkataramaiah
 

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Naane Rajakumara || PB. Srinivas || Chi. Udaya Shankar || 03.07
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 


 