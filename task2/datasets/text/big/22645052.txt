Little Tony (film)
 
{{Infobox Film
| name           = Kleine Teun
| image          = Little Tony.jpg
| image_size     =
| caption        = 
| director       = Alex van Warmerdam
| producer       = 
| writer         = Alex van Warmerdam
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = April 30, 1998
| runtime        = 95 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}} 1998 cinema Dutch comedy film drama directed by Alex van Warmerdam. It was screened in the Un Certain Regard section at the 1998 Cannes Film Festival.   

==Cast==
*Alex van Warmerdam	... 	Brand
*Annet Malherbe	... 	Keet
*Ariane Schluter	... 	Lena
*Sebastiaan te Wierik	... 	Kleine Teun
*Aat Ceelen	... 	Butcher
*Beppe Costa	... 	Verger
*Joeri Keyzers	... 	Baby
*Rick Keyzers	... 	Baby
*Maike Meijer	... 	Young Mother
*Thomas Rap	... 	Man at strawheap
*Hanneke Riemer	... 	Friend of Lena
*Dolf Sauter	... 	Priest
*Tomas te Wierk	... 	Teun
*Houk van Warmerdam	... 	Acolyte
*Marc van Warmerdam	... 	Neighbour
*Mees van Warmerdam	... 	Acolyte

==References==
 

== External links ==
*  

 

 
 
 
 
 


 
 