Buongiorno papà
{{Infobox film
| name     = Buongiorno papà
| image    = Buongiorno papà.jpg
| director = Edoardo Leo
| producer = 
| writer =  
| starring =Raoul Bova 
| cinematography = Arnaldo Catinari
| music =    	Gianluca Misiti
| editing =  
| distributor =  	 Italian 
| released =  
}} Nastri dargento, for best comedy film and for best actor (a nominee tied between Raoul Bova and Marco Giallini). {{cite news|last=Anna Finos|title=
Nastri dargento, testa a testa fra Sorrentino e Tornatore |url=http://trovacinema.repubblica.it/news/dettaglio/nastri-dargento-testa-a-testa-fra-sorrentino-e-tornatore/431533 |accessdate=5 March 2014|newspaper=La Repubblica|date=May 31, 2013}} 

== Cast ==
*Raoul Bova: Andrea Manfredini
*Marco Giallini: Enzo Brighi
*Edoardo Leo: Paolo
*Nicole Grimaudo: Lorenza Metrano
*Rosabell Laurenti Sellers: Layla Brighi
*Paola Tiziana Cruciani: Adele Stramaccioni
*Mattia Sbragia: Roberto Manfredini
*Antonino Bruschetta: Adriano

== References ==  
 

== External links ==
* 
 
 
 
  
   

 
 