Copperhead (2013 film)
 
{{Infobox film
| name           = Copperhead
| image          = Copperhead 2013 film.jpg
| alt            =
| caption        = film poster Ron Maxwell 
| producer       = Ron Maxwell
| story          = Harold Frederic
| screenplay     = Bill Kauffman
| starring       = {{Plainlist|
* Billy Campbell
* Angus Macfadyen
* Peter Fonda
* Augustus Prew
* Lucy Boynton
* Casey Thomas Brown
* François Arnaud (actor)|François Arnaud
* Josh Cruddas
* Genevieve Steele
}}
| music          = Laurent Eyquem
| cinematography = Kees Van Oostrum
| editing        =
| studio         = Swordspoint Productions Brainstorm Media
| distributor    = Film Collective
| released       =  
| runtime        = 118 minutes
| country        = Canada United States
| language       = English
| budget         =
| gross          = $171,740   
}}
 Ron Maxwell . It stars Billy Campbell. Shot at Kings Landing Historical Settlement in New Brunswick, Canada and set in upstate New York, it was released June 28, 2013.  The film is based on a 19th-century novel, The Copperhead, by Harold Frederic. The title refers to Northern opponents of the American Civil War, known as Copperhead (politics)|Copperheads.

==Plot==
In a rural community upstate New York in 1862, farmer Abner Beech is a Northern antiwar Democrat. While his neighbors take up the Union cause in the ongoing American Civil War, Beech believes that coercion in resisting the secession of the southern states is unconstitutional, and gradually becomes more and more harassed for his views, derisively called a "Copperhead". His son, Thomas Jefferson Beech, enlists in the Union Army. Beech also arouses the ire of militant abolitionist Jee Hagadorn, whose daughter Esther (Lucy Boynton) loves Jeff.

==Cast==
* Billy Campbell as Abner Beech
* Angus Macfadyen as Jee Hagadorn
* Peter Fonda as Avery
* Augustus Prew as Ni Hagadorn
* Lucy Boynton as Esther Hagadorn
* Casey Thomas Brown as Jeff Beech
* François Arnaud (actor)|François Arnaud as Warner Pitts
* Josh Cruddas as Jimmy
* Genevieve Steele as MRye Beech

==Production==
The film had the working title Copperhead: The War at Home.  Filmed at Kings Landing Historical Settlement in New Brunswick, Canada,.       "Copperhead" was produced by Ron Maxwell and co-produced by John Houston.

==Reception==
"Well researched, intellectually admirable, and beautifully photographed . . . If every war has more than one side, this story of one man who dares to stand against the tide of history has a contemporary relevance that remains uncontested."  

===Box office===
Copperhead opened in limited theatrical release June 28, 2013, playing in 59 theaters.  and was simultaneously released in video-on-demand platforms including amazon instant video where it is remains available.

==Relation to directors previous works== Gods and The Last Full Measure. 

==See also==
*  
*  

==References==
 

==External links==
*  
*  

 

 
 
 
 
 