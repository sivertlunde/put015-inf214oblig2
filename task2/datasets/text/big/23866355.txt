Mella Thirandhathu Kadhavu
{{Infobox film
| name = Mella Thirandhathu Kadhavu
| image = Mella Thirandhathu Kadhavu.jpg 
| director =  R. Sundarrajan_(director)|R. Sundarrajan
| writer = R. Sundarrajan Mohan Radha Radha Amala Amala Visu Charlie
| producer = M. Saravanan (film producer)|M. Saravanan M. Balasubramaniam
| music =  M. S. Viswanathan Ilayaraja
| cinematography = Raja Rajan
| editing = R. Bhaskaran
| studio = AVM Productions
| distributor = AVM Productions
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}}
  Amala and Senthil .This movie is a box office hit. 

The films score and soundtrack are composed by Ilaiyaraaja and M. S. Viswanathan. Later they worked together in 4 more films, Senthamizh Paattu, Senthamizh Selvan, En Iniya Ponnilave and Vishwa Thulasi. Entire BG Score including the songs BG Music for all the songs was done by Ilaiyaraaja while 5 Songs were Composed by M. S. Viswanathan.

==Plot==
Tulasi (Radha) is a village girl. One day she gets a letter from her cousin Subramani(Mohan). She gets excited as they were close friends in childhood but Mohan left with his family to city. Subramani visits the village for his college thesis about folk songs. Tulasi is in love with Subramani but she is confused whether Subramani also loves her. Tulasis father passes away due to heart attack. When Subramanis mother arranges a marriage for Subramani with Tulsi he refuses. Because of this, her mother died in frustration. Then Tulasi comes to her Uncles house. Later a flashback comes in which Subramani is in love with Noorjahan (Amala). Since Noorjahan used to come to the music college with veil, Subramani has not seen her face. On a day when they plan to meet, Noorjahan dies after she steps into quick sand. Finally whether Tulasi marries her beloved cousin Subramani is the story.

==Cast== Mohan as Subramani Radha as Tulasi
*Visu as Tulasis father
*Senthil as visus auction assistant Amala as Noorjahan
*M. N. Nambiar as Hindi teacher and Noorjahans father Charlie as Subramanis friend
*Gundu Kalyanam  as Subramanis friend

==Music==
All the songs are composed by M. S. Viswanathan except Sakkara Kattikku Kuzhaloodum Kannanukku, which were composed by Ilayaraja. The song "Vaa Vennila" is inspired from one of the old composition of M. S. Viswanathan from the movie Chandirani.
{{tracklist	
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 
| title1       = Vaa Vennila
| extra1       = S. P. Balasubrahmanyam, S. Janaki Vaali
| length1      = 4.39
| title2       = Thedum Kan Paarvai
| extra2       = S. P. Balasubrahmanyam, S. Janaki Vaali
| length2      = 4.43
| title3       = Dil Dil Dil Manadhil
| extra3       = S. P. Balasubrahmanyam, P. Susheela Vaali
| length3      = 4.41
| title4       = Kuzhaloodum Kannanukku
| extra4       = K. S. Chithra Vaali
| length4      = 4.38
| title5       = Ooru Sanam
| extra5       = S. Janaki
| lyrics5      = Gangai Amaran
| length5      = 4.38
| title6       = Sakkara Kattikku
| extra6       = K. S. Chithra, S. P. Sailaja, B. S. Sasirekha
| lyrics6      = Gangai Amaran
| length6      = 5.18
| title7       = Vaa vennila
| extra7       = S. Janaki Vaali
| length7      = 2.8
}}

==References==
 

==External links==
 
 
 

 
 
 
 
 
 