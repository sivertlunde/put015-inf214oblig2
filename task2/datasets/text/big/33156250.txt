Home Sick
{{Infobox film
| name           = Home Sick
| image          = HomeSick2007.jpg
| image size     =
| caption        = 
| director       = Adam Wingard
| producer       = E. L. Katz Peter Katz
| writer         = E. L. Katz
| based on       = 
| starring       = 
| music          = Zombi
| cinematography = Andor Becsi Michael Bear Praytor
| editing        = 
| studio         = Population 1280 Films
| distributor    = Synapse Films
| released       =  
| runtime        = 89 minutes
| country        = United States English
| budget         = 
}}
Home Sick is a 2007 horror film written by E. L. Katz and directed by Adam Wingard.

The film had its world premiere at the 2007 Fantasia Festival.   

== Plot ==
A strange man crashes a party and asks each person to name who they hate.  With some prodding, they all answer the stranger.  When the named people die in horrible ways, the party-goers suspect that the stranger is a spree killer.

== Cast ==
 
* Will Akers as Robert
* Brandon Carroll as Devin
* Patrick Engel as Mathew
* Shaina Fewell as Alexis
* Jeff Dylan Graham as Ben
* L.C. Holt as Anthony
* E. L. Katz as Cashier
* Matt Lero as Tim
* Bill Moseley as Mr. Suitcase
* Forrest Pitts as Mark
* Lindley Praytor as Claire (as Lindley Evans)
* Tiffany Shepis as Candice
* Jonathan Thornton as Boss
* Tom Towles as Uncle Johnny
* X-Zanthia as Call Girl #1
 

== Production ==
The film took almost six years to be released in its final cut. 

== Release ==
Home Sick directors cut debuted on 14 July 2007 at the Fantasia Festival  and opened in other film festivals on the dates given below.

{| class="wikitable sortable"
|- style=background:#ccccff
! Region
! Release date
! Festival
|- Canada
| style="background: #f7f8ff; white-space: nowrap;" |  Fantasia Festival 
|- United States
| style="background: #f7f8ff; white-space: nowrap;" |  Cinema Wasteland Convention
|}

=== Home Release ===
Home Sick was released on DVD in the United States on August 26, 2008. 

== Reception ==
Bloody Disgusting rated it 3/5 stars and wrote that the film is "absolutely worth the wait. It’s not the best low budget horror film to ever come along but it definitely hits a home run with the special effects work".   Adam Tyner of DVD Talk rated it 3/5 stars and called it "sick, twisted, and really offbeat."   Paul Pritchard of DVD Verdict criticized the film for its focus on gore over story. 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 

 