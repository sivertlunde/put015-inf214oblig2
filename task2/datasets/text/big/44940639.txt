Jandamarra's War
 
{{Infobox film
| name           = Jandamarras War
| image          = 
| caption        = 
| director       = Mitch Torres
| producer       = Andrew Ogilvie Andrea Quesnelle Eileen Torres
| writer         = Mitch Torres
| starring       =
| music          = 
| cinematography = Allan Collins Jim Frater Rusty Geller
| editing        = Lawrie Silvestrin
| distributor    = 
| genre          = Documentary
| released       =  
| runtime        = 55 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}}
 Aboriginal Australian warrior of the Bunuba people from Western Australia.

==Synopsis== European colonists were frequently killing Aboriginal Australians. As a teenager, he left the cattle station with his Uncle Ellemarra to be initiated in Bunuba Law, but when they were caught spearing sheep, both were sent to prison. After he left prison, he was expelled from Bunuba society for sleeping with other mens women and soon after he became friends with a policeman named Richardson. Later he killed Richardson, marking the beginning of his 3-year war against the Europeans.

In 1894, Jandamarra led a rebellion against invading European pastoralists in order to defend Bunuba land and culture.
 Kimberley area for fear of him. His life ended when he was shot dead by Mungo Micki, an Aboriginal tracker.       

==Cast==
*Darcy Anderson as Fred Edgar
*David Beurteaux as Lukin
*Emmanuel J. Brown as Lilimarra
*Gabriel Brown as Baby Jandamarra
*Tamara Cherel as Mayannie
*Grant Currie as Drewry
*Ernie Dingo as Narrator (voice)
*Peter Docker as Richardson
*Bevan Green as Young Jandamarra
*Andy Hallen as Forester
*Keithan Holloway as Jandamarra
*Stanley Jangary Snr. as Ellemarra
*Kaylene Marr as Jinny
*Jack Mccale as Micki
*Blythe McGuckin as Blythe
*Craig Snell as Gibbs
*Kurt Wheatley as Constable Pilmer
*Brendon Williams as Captain
*Bruce Williams as Jim Crowe
*Matt Wood as Nicholson {{cite web|url=http://www.imdb.com/title/tt1865414/fullcredits?ref_=tt_ov_st_sm |title=
Jandamarras War (2011 TV Movie) - Full Cast & Crew |publisher=IMDb |accessdate=4 January 2015 }}  

==Production==
===Script===
Director and Scriptwriter Mitch Torres wanted the film to portray the story of Jandamarra as accurately as possible, noting that:
 

===Filming===
Principal filming took place over ten days in June 2010 and most of the films scenes were shot on Bunuba land in locations close to where the historical events being reenacted actually occurred. 

==Reviews==
Jim Schembri wrote in The Age that "historical documentaries are at their best when they illuminate a little known narrative rather than merely recite a famous one. Hopefully, it will only be a matter of time before some wily filmmakers seize on the potential to develop Jandamarras story into a full-blown feature film". 

==Awards==
{| class="wikitable"  
|-
! Ceremony
! Category
! Result
|-
| AACTA Awards
| Best Documentary Under One Hour
|     
|-
| AACTA Awards
| Best Cinematography in a Documentary
|   
|-
| AACTA Awards
| Best Sound in a Documentary
|   
|}

==See also==
*Jandamarra

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 