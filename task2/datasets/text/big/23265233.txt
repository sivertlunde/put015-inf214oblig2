Independence Day (1983 film)
{{infobox film name = Independence Day image = Independence day 1983 poster.jpg caption = writer = Alice Hoffman starring = Kathleen Quinlan David Keith Dianne Wiest Cliff DeYoung director = Robert Mandel producer = Robert Singer (producer) Daniel H. Blatt (producer) written = Alice Hoffman runtime = 110 min. cinematography = Charles Rosher Jr. music = Charles Bernstein editing = Tina Hirsch Dennis Virkler released =   studio = Warner Bros. Pictures distributor = Warner Bros. Pictures country = United States language = English budget = gross = $151,462 (USA)
}}

Independence Day is a 1983 film directed by Robert Mandel from a script by the novelist Alice Hoffman. It was designed by Stewart Campbell and shot by Charles Rosher. It stars Kathleen Quinlan, David Keith, Cliff DeYoung, Frances Sternhagen and Dianne Wiest. 
 Alice Adams and the young Margaret Sullavan in The Shop Around the Corner, but the Quinlan character "has the talent driving her on past all that."  Wiest plays a battered wife.  
 State of the Art: "Kathleen Quinlan plays the part of the woman artist with a cool, wire-taut intensity, Robert Mandel keeps the whole cast interacting quietly and satisfyingly, Wiest has hold of an original character and plays her to the scary hilt." 

==Plot==
In a small Texas town where she works as a waitress in her familys diner, Mary Ann Taylors true love is photography. She would like to get beyond these limits, but when Jack Parker returns to town, he lets her know that hes been to the big city and happiness there is as elusive as anyplace else.

Mary Ann and Jack fall in love. Their bliss is interrupted, however, by the discovery that Jacks meek sister is being physically abused by her husband. Jack tries to intervene, but in the end, Nancy decides she needs to take drastic action herself.

==Cast==
* Kathleen Quinlan as Mary Ann Taylor
* David Keith as Jack Parker
* Frances Sternhagen as Carla Taylor
* Cliff DeYoung as Les Morgan
* Dianne Wiest as Nancy Morgan
* Josef Sommer as Sam Taylor
* Bert Remsen as Red
* Richard Farnsworth as Evan
* Brooke Alderson as Shelly
* Noble Willingham as Andy Parker

==References==
 

==External links==
* 

 

 
 
 
 
 
 

 