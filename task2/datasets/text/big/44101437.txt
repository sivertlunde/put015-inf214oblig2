Onningu Vannengil
{{Infobox film 
| name           = Onningu Vannengil
| image          = Onningufilmposter.png
| caption        = 
| director       = Joshiy
| producer       = Sajan
| writer         = AR Mukesh Kaloor Dennis (dialogues)
| screenplay     = Kaloor Dennis Shankar Jagathy Sreekumar Thilakan James Shyam
| cinematography = Anandakkuttan
| editing        = K Sankunni
| studio         = Saj Productions
| distributor    = Saj Productions
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Shankar in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Mammootty Shankar
*Nadia Moidu
*Jagathy Sreekumar
*Thilakan
*James
*Lalithasree
*Lalu Alex
*Paravoor Bharathan

==Soundtrack== Shyam and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anuje Ninakkaay || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Dum Dum Dum Swaramelam || KS Chithra, Sharreth || Poovachal Khader || 
|-
| 3 || Kaalangal Maarunnu || KS Chithra || Poovachal Khader || 
|-
| 4 || Kaalangal Maarunnu || K. J. Yesudas || Poovachal Khader || 
|-
| 5 || Maarikko Maarikko || KS Chithra, Vani Jairam || Poovachal Khader || 
|-
| 6 || Mangalam Manjulam || K. J. Yesudas, KS Chithra || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 