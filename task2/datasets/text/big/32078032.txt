Hawaiian Vacation
{{Infobox film
| name           = Hawaiian Vacation
| image          = Hawaiian Vacation poster.jpg
| alt            = 
| caption        = 
| director       = Gary Rydstrom
| producer       = Galyn Susman
| story          = Erik Benson Christian Roman
| screenplay     = Erik Benson Jason Katz Gary Rydstrom
| starring       = Tom Hanks Tim Allen Joan Cusack Don Rickles Estelle Harris Wallace Shawn John Ratzenberger Blake Clark Jeff Pidgeon Jodi Benson Michael Keaton Timothy Dalton Jeff Garlin Kristen Schaal Bonnie Hunt Bud Luckey
| music          = Mark Mothersbaugh
| editing        = Axel Geddes
| studio         = Pixar Animation Studios Walt Disney Pictures Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 5 minutes
| country        = United States
| language       = English
}}
 computer animated Toy Story films and takes place after the events of Toy Story 3. It was first screened in theaters ahead of Pixars feature film Cars 2,          and was included on the films media release. 

==Plot==
Months after Toy Story 3, it is winter break for Bonnie, who is going on vacation to Hawaii with her family. The toys are excited to have a week of relaxation, but Barbie and Ken reveal themselves to have stowed away in Bonnies backpack, hoping to join her in Hawaii. Bonnie leaves them in her room, however, much to Kens horror and disappointment when he realizes they are not going to Hawaii. Barbie reveals to Woody that Ken planned to have their first kiss on a beach at sunset (based on a travel brochure), inspiring Woody, Buzz, and the rest of Bonnies toys to go all out and recreate Hawaii for the two. After various adventures in "Hawaii", Ken and Barbie share their first kiss in the snow at sunrise, recreating the scene from the brochure. However, the two step off the edge of the porch without realizing it and end up buried in snow; a post-credits scene shows the other toys trying to free them from a block of ice in which they are now frozen.

==Voice cast==
  Barbie
* Ken
* Woody
* Tim Allen as Buzz Lightyear Jessie
* Don Rickles as List of Toy Story characters#Mr. Potato Head|Mr. Potato Head
* Estelle Harris as List of Toy Story characters#Mrs. Potato Head|Mrs. Potato Head Rex
* Hamm
* Slinky Dog Aliens
* Emily Hahn Bonnie
* Lori Alan as Bonnies Mom
* Timothy Dalton as List of Toy Story characters#Mr. Pricklepants|Mr. Pricklepants Buttercup
* Trixie
* Dolly
* Chuckles
* Zoe Levin as List of Toy Story characters#Peas-in-a-Pod|Peas-in-a-Pod Spanish Buzz
* Angus MacLane as Captain Zip
* Axel Geddes as Rexing Ball

==Production==
The film was first announced by Lee Unkrich who said, "We have announced were going to do a short film in front of Cars 2 that uses the Toy Story characters. Were going to keep them alive; theyre not going away forever."   The short films title and plot were later revealed on February 17, 2011. 

==Reception==
Charlie McCollum of Mercury News called it a "delightful snippit of life" that is "crisp, funny and sweet". He wrote that "in terms of storytelling and wit, it pretty much eclipses Cars 2," the feature film it preceded. 

==Home media==
As of July 2012, Hawaiian Vacation is available as a digital download on Amazon Instant Video  and iTunes Store.  The short was released on November 13, 2012, on the DVD and Blu-ray Disc of Pixar Short Films Collection Volume 2.    The short film will be released on Blu-ray and DVD on August 19, 2014, with Toy Story of Terror! and two other Toy Story Toons. 

==References==
 

==External links==
*  
*  
*  

 
{{succession box Day & Night Pixar Animation short films
 | years  = 2011 Small Fry}}
 

 
 
 
 
 

 
 
 
 
 
 
 