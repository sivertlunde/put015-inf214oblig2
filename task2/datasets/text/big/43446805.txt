Pattanathu Rajakkal
{{Infobox film
| name           = Pattanathu Rajakkal
| image          =
| image_size     =
| caption        =
| director       = S. A. Chandrasekhar
| producer       = P. S. V. Hariharan
| writer         = Shoba Chandrasekhar
| starring       = Vijayakanth Silk Smitha Jaishankar S. S. Chandran
| music          = Sankar Ganesh
| cinematography = D. D. Prasad
| editing        = P. R. Gowthamraj
| art direction  = P. L. Shanmugam
| released       =  
| country        = India Tamil
}}
 1982 Cinema Indian Tamil Tamil film, directed by S.A.Chandrasekar, starring Vijayakanth and Silk Smitha in lead roles. The film had musical score by Sankar Ganesh and was released on 1981. 

==Cast==
*Vijayakanth
*Silk Smitha 
*Jaishankar
*S. S. Chandran

==Soundtrack==
The music was composed by Shankar Ganesh.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kaathal Enum || Malaysia Vasudevan || Pulamaipithan || 5:11 
|-  Vaali || 2:58 
|}

==References==
 

==External links==
* 

 
 
 
 
 


 