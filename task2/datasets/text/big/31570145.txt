Henpecked
{{Infobox Hollywood cartoon
| cartoon_name = Henpecked
| series = Oswald the Lucky Rabbit
| image = Henpecked Oswald.jpg
| caption = The bear tells Oswald to not make any noise. Bill Nolan Bill Nolan
| animator = Bill Nolan Ray Abrams Manuel Moreno Clyde Geronimi Pinto Colvig
| voice actor = Pinto Colvig Walter Lantz
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release date =  
| color process = Black and white
| runtime = 6:34
| language = English
| preceded by = Snappy Salesman
| followed by = The Singing Sap
}}
Henpecked is a 1930 animated short produced by Walter Lantz that features Oswald the Lucky Rabbit.

==Plot== swing song. Just then, a disturbed bear from another room comes in and confronts him. The bear sternly tells Oswald to quit playing and ensure silence. Oswald hesitantly agrees to the demands. Momentarilly a frog from a glass bowl jumps out and lands on some piano keys before leaping out of the scene. In this, the bear picks up the piano and tosses it out the window. Before the bruin leaves, Oswald was given a reminder.

Hours later, Oswald receives a phone call. Speaking to him was a stray kitten who is a friend of his. The kitten was looking to come visit Oswald as well as bringing in a pack of other stray kittens. Oswald rejected the offer because of the deal he made with his grumpy neighbor. Nevertheless, the stray kittens marched towards the apartment.
 clothes iron in the latters trousers. Oswald tries to intervene but to no avail. The bear twitches in pain and frantically runs around before sitting in a bucket filled with water.

Minutes afterward, one of the stray kittens plays a trombone. As that tiny cat performs, the slide of his instrument started striking the ceiling, and its impact was felt by the bear who was bathing straight upstairs. Eventually, the floor of the bathroom crumbles and collapses. The bruin plummets to the floor below and figured this was the last straw.

Learning that they are in hot water, Oswald and the stray kittens locked themselves in an apartment room. Immediately, the bear came up with a successful method of sucking them under the door using a vacuum cleaner. The bear removes the vacuum bag and dumps it outside where it opened automatically somehow. Although they were excluded, the stray kittens had a good time and Oswald couldnt believe it.

==Cast==
* Pinto Colvig as Oswald the Lucky Rabbit and the Bear
* Walter Lantz as the stray kittens

==Publicity flaws==
The second edition of Jeff Lenburgs Encyclopedia of Animated Cartoons mistakenly mentions Hen Fruit as the working title for this short. Hen Fruit is in fact, a 1929 Oswald cartoon. As a result, references to Henpecked were removed in the third edition. {{cite web
|url=http://lantz.goldenagecartoons.com/1930.html
|title=The Walter Lantz Cartune Encyclopedia: 1930
|accessdate=2011-04-24
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==See also==
* Oswald the Lucky Rabbit filmography

==References==
 

==External links==
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 
 