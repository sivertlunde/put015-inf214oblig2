Fiery Fireman
{{Infobox Hollywood cartoon|
| cartoon_name = Fiery Fireman
| series = Oswald the Lucky Rabbit
| image =
| caption =
| director = Friz Freleng Rudolph Ising
| story_artist =
| animator =
| voice_actor =
| musician =
| producer = George Winkler
| distributor = Universal Pictures
| release_date = October 15, 1928
| color_process = Black and white
| runtime = 5:46 English
| preceded_by = Panicky Pancakes
| followed_by = Rocks and Socks
}}

Fiery Fireman is a silent animated short produced by George Winkler, and stars Oswald the Lucky Rabbit. It is among the few Oswald shorts from the Winkler period known to be existing.

==Plot summary==
Oswald is a fire fighter who is seen resting in bed inside the fire department. Also lying in bed beside him is his colleague, a horse.

One day, a condo building goes ablaze, and calls for help from the scene are audible miles away. Oswald and his horse are at first reluctant to leaving their bed but still manage to rush toward the site on time.

In their first rescue mission, Oswald scales a building to the floor where stranded mice are waiting. Oswald provides them a long rope which they used to slide downward. Next, Oswald and his companion moved to another building to rescue a hippo. Inside their targeted room, they find the hippo unconscious and try to carry her out the window. As Oswald starts down the ladder the hippo awakens. The massive weight of the large animal causes the ladder to collapse and the two occupants to plummet down to and through the sidewalk, leaving a hole. Oswald comes up through a basement door, carrying the hippo single-handedly.

==See also==
* Oswald the Lucky Rabbit filmography

==External links==
*  

 

 
 
 
 
 
 
 
 


 