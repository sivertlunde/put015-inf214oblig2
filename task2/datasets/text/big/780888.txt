Fourteen Hours
 
{{Infobox film
| name           = Fourteen Hours
| image          = Fourteen hours 1951 poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Henry Hathaway
| producer       = Sol C. Siegel
| screenplay     = John Paxton
| based on       =   Paul Douglas Robert Keith Alfred Newman
| cinematography = Joe MacDonald
| editing        = Dorothy Spencer
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} New York police officer trying to stop a despondent man from jumping to his death from the fifteenth floor of a hotel.
 Paul Douglas Robert Keith, Debra Paget and Howard Da Silva. It was the screen debut of Grace Kelly and Jeffrey Hunter, who appeared in small roles. 

The screenplay was written by John Paxton, based on an article by Joel Sayre in The New Yorker. Sayres article described the 1938 incident upon which the film was based.

==Plot== Paul Douglas), a policeman on traffic duty in the street below, tries to talk him off the ledge to no avail. He is ordered back to traffic patrol by police emergency services deputy chief Moksar (Howard Da Silva). But he is ordered to return when the man on the ledge will not speak to psychiatrists summoned to the scene.  Robert Keith) in Fourteen Hours.]]
Coached by a psychiatrist (Martin Gabel), Dunnigan tries to relate to the man on the ledge as one human to another. 
 Robert Keith), whom he despises, arrives. The divorced father and mother clash over old family issues, and the conflict is played out in front of the police. Dunnigan seeks to reconcile Robert with his father, whom Cosick has been brought up to hate by his mother. Dunnigan forces Mrs. Cosick to reveal the identity of a "Virginia" mentioned by Robert, and she turns out to be his estranged fiancee. 

While this is happening, a crowd is gathering below. Cab drivers are wagering on when he will jump. A young stock room clerk named Danny (Jeffrey Hunter) is wooing a fellow office worker, Ruth (Debra Paget), whom he meets by chance on the street. A woman (Grace Kelly) is seen at a nearby law office, where she is about to sign the final papers for her divorce. Amid legal formalities, she watches the drama unfold. Moved by the tragic events, she decides to reconcile with her husband. 

After a while, Dunnigan convinces Cosick everyone will leave the hotel room so that he can rest. As Cosick steps in, a crazy evangelist sneaks into the room and Cosick goes back to the ledge. This damages his trust in Dunnigan, as does an effort by police to drop down from the roof and grab him. As night falls, Virginia (Barbara Bel Geddes) is brought to the room, and she pleads with Robert to come off the ledge, to no avail. All the while, the police, under the command of Moksar, are working to grab Robert and put a net below him. 

Dunnigan seems to make a connection with Cosick when he talks about the good things in life, and he promises to take Cosick fishing for "floppers" (flounder) on Sheepshead Bay. Cosick is about to come inside when a boy on the street accidentally turns on a spotlight that blinds Robert, and he falls from the ledge. But he manages to grab a net that the police had stealthily put below him, and he is hauled into the hotel. Dunnigan is greeted by his wife and son, and Danny and Ruth walk the street hand in hand.   

==Cast==
  trying to talk Richard Basehart off the ledge]] Paul Douglas as Charlie Dunnigan
* Richard Basehart as Robert Cosick
* Barbara Bel Geddes as Virginia Foster
* Debra Paget as Ruth
* Agnes Moorehead as Christine Hill Cosick Robert Keith as Paul E. Cosick
* Howard Da Silva as Deputy Chief Moskar
* Jeffrey Hunter as Danny Klempner
* Martin Gabel as Dr. Strauss
* Grace Kelly as Mrs. Louise Ann Fuller
* Frank Faylen as Walter, room service waiter
* Jeff Corey as Police Sgt. Farley
* James Millican as Police Sgt. Boyle
* Donald Randolph as Dr. Benson

==Cast notes== Robert Keith, in Fourteen Hours]] La Strada.   

Baseharts wife, costume designer Stephanie Klein, was diagnosed with a brain tumor during filming of Fourteen Hours in May and June 1950, and died following brain surgery during production of the film that July. 

Grace Kelly made her film debut in Fourteen Hours, beating out Anne Bancroft for the role.  Kelly was noticed during a visit to the set by Gary Cooper, who subsequently starred with her in High Noon. Cooper was charmed by Kelly and said that she was "different from all these sexballs weve been seeing so much of." However, her performance in Fourteen Hours was not noticed by critics, and did not lead to her receiving other film acting roles. She returned to television and stage work after her performance in the film.   

A nonprofessional performer named Richard Lacovara doubled for Basehart in long shots on the ledge, which had been enlarged to minimize risk of falling. Lacovara was protected by a canvas life belt hidden under his costume, connected to a lifeline, Even with the double, Basehart still had to endure over 300 hours of standing on the ledge with little movement during the fifty days of shooting in New York, even though he had a sprained ankle and his legs were ravaged by poison oak contracted on the grounds of his Coldwater Canyon home. 

Barbara Bel Geddes, who played Baseharts love interest, did not appear in another film until Vertigo (film)|Vertigo, seven years later in 1958.
 West Side Leif Erickson John Randolph.   

==Production notes==

===Factual basis===
  on the ledge of the Gotham Hotel in 1938, as his sister pleads with him to come in.]]
Although the onscreen credits contain a statement saying that the film and characters depicted were "entirely fictional," the movie was based on the suicide of John William Warde, a twenty-six-year-old man who jumped from the seventeenth floor of the Gotham Hotel in New York City on July 26, 1938, after eleven hours on a ledge. The character of Charlie Dunnigan was based on Charles V. Glasco, a New York City policeman who tried to convince Warde to come off the ledge.      

Glasco pretended to be a bellhop at the hotel, and tried to persuade Warde that he would be fired if he did not come off the ledge.  Warde, who had made previous suicide attempts, also heard pleas from his sister.  But the efforts to persuade him were to no avail, and he eventually jumped. Police had tried to rig a net below him, but the net could not be extended sufficiently from the hotel to block his fall. During his eleven hours on the ledge, traffic was stopped for blocks around the hotel, which was located on 55th Street and Fifth Avenue in Manhattan, and thousands watched the drama unfold. 

===Pre-production and filming===
Writer  .   

Sayres story was originally purchased as a vehicle for Richard Widmark, who was to play the man on the ledge, with Robert Wagner to play the role of Danny, but was replaced by Jeffrey Hunter. 

Twentieth Century Fox changed the title from The Man on the Ledge to Fourteen Hours at the request of Wardes mother, so that the picture would not be as closely identified with her son. Studio chief Darryl F. Zanuck considered changing the setting of the movie to another city for the same reason. But it was ultimately filmed in New York. 
 Kiss of Death (1947) and Call Northside 777 (1948), was assigned to the project. The film was made in just six weeks with a modest budget.  The New York exteriors were filmed at the Guaranty Trust Company building, located at 128 Broadway in lower Manhattan.  The building has since been demolished.

Hathaway avoided stasis by cutting between the film ledge and the reaction of the crowd below, and by adroit use of camera angles. It is considered to be his finest film.   

===Post-production===
The film originally ended with Robert falling to his death. Both endings were shot, and Hathaway preferred the realistic ending that showed Robert falling to the ground, as occurred in the Warde incident. However, on the same day the film was previewed, the daughter of Fox president Spyros Skouras jumped to her death. Skouras wanted the film shelved, but instead released Fourteen Hours six months later with the ending that showed Robert surviving his fall. 

==Reception==

===Critical response===
The New Yorker singled out Baseharts performance for praise, saying that he "succeeds in conveying the notion that he is indeed sorely beset." 

The New York Times film critic, Bosley Crowther, praised the "gripping suspense, absorbing drama and stinging social comment in this film." Crowther said: "Fitly directed by Henry Hathaway in a crisp journalistic style and played to the hilt down to its bit parts, it makes a show of accelerating power." Crowther praised Baseharts "startling and poignant" performance, and said that Douglas "takes the honors as the good-natured cop who finds all his modest resources of intelligence and patience taxed by this queer case." He also praised Da Silva, Moorehead, and the other supporting players for bringing "personality and credibility to this superior American film."   
 Ace in the Hole, Time Out observed that "the emphasis is as much on the reaction of bystanders as on the plight of the would-be suicide."   

===Accolades=== Best Art Direction (Lyle R. Wheeler, Leland Fuller, Thomas Little, Fred J. Rode).   

Fourteen Hours was listed as among the top ten motion pictures of 1951 by the National Board of Review of Motion Pictures. For his performance in the movie, Basehart won the 1951 award for best actor by the board.    The film also was nominated for the BAFTA award for best film from any source. Hathaway was nominated for the Golden Lion Award at the Venice Film Festival, and Paxton was nominated for a Writers Guild of America award for his screenplay.   

==Legacy==
Despite good reviews and a strong push by the studio to publicize the movie, with Paul Douglas appearing on the cover of Life (magazine)|Life magazine, Fourteen Hours faded into obscurity. When the film was shown in revival at a Los Angeles Theater in 2003, only one print survived. However, the title was included in Twentieth Century Foxs “Fox Film Noir” DVD series in 2006. 

Writing in Dark City: The Lost World of Film Noir, author Eddie Muller wrote: "Its a tense depiction of one mans personal despair, amid the teeming concrete indifference of the modern city."   
 Cameron Mitchell, as an episode of The 20th Century Fox Hour.

In 2012, another remake was released as Man on a Ledge, starring Sam Worthington and Elizabeth Banks in the lead roles.

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images)
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 