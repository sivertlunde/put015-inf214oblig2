He's Way More Famous Than You
 
{{Infobox film
| name           = Hes Way More Famous Than You 
| image          = Hes Way More Famous Than You.jpg
| caption        = 
| director       = Michael Urie
| producer       = {{Plainlist |
*Christopher Sepulveda 
*Michael Anderson
*Geoffrey Soffer 
}}
| screenplay     = {{Plainlist |
* Halley Feiffer
* Ryan Spahn
}}
| story          = 
| based on       = 
| starring       = {{Plainlist |
* Jesse Eisenberg
* Halley Feiffer
* Ben Stiller
* Mamie Gummer
* Vanessa L. Williams
* Michael Urie
}}
| music          = Jeff Beal
| production design = Dara Wishingrad  
| cinematography = Austin F. Schmidt
| editing        = Jim Mol
| studio         = {{Plainlist |
* Logolite Entertainment
* Ur-Mee Productions
}}
| distributor    = Warner Bros.
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Vanessa Williams, Tracee Chimo, Austin Pendleton, and Michael Chernus. It premiered at the 2013 Slamdance Film Festival, where it was acquired by Gravitas Ventures and for theatrical distribution.
 Michael Anderson of Logolite Entertainment, Ur-Mee Productions and Geoff Soffer. It was released pre-theatrically on iTunes, Amazon Instant Video and Video on Demand on April 8, 2013, through Gravitas Ventures and Warner Bros., and was released theatrically in the United States and Canada on May 10, 2013. The DVD was released on September 24, 2013 by Kino Lorber and Archstone Distribution.

==Plot==
When once-up-and-coming indie film starlet Halley Feiffer loses her boyfriend, her agent and her career in one fell swoop she finally realizes that something has got to change...she has to become WAY MORE FAMOUS! Armed with a stolen script and two pitchers of sangria, Halley enlists the help of her brother Ryan to make a movie, starring herself (of course), and any A-list celebrity she can land. Halley will stop at nothing in this balls to the wall Hollywood comedy...even if it means hurting the only people who truly care about her.

==Festivals==
*2013 Slamdance Film Festival (January 20) (World Premiere)
*2013 Friars Club Comedy Film Festival (April 1) (Opening Night Film)
*2013 Cleveland International Film Festival (April 6)
*2013 Phoenix Film Festival (April 7) 
*2013 Dallas International Film Festival (April 10)
*2013 Newport Beach Film Festival (April 26)

==References==
 

==External links==
*  

 
 