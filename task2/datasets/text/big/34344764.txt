Seema Tapakai
{{Infobox film
| name           = Seema Tapakai
| image          = Seema Tapakai poster.jpg
| caption        = Movie Poster
| director       = G.Nageswara Reddy
| producer       = Dr. Vijaya Prasad Malla
| story          = G. Nageswara Reddy
| screenplay     = G. Nageswara Reddy
| writer         = Marudhuri Raja  (dialogues) 
| starring       = Allari Naresh Poorna Sayaji Shinde Jaya Prakash Reddy
| music          = Vandemataram Srinivas
| cinematography = Adusumilli Vijay Kumar
| editing        = Kotagiri Venkateswara Rao
| studio         = Wellfare Creations
| distributor    = 
| released       =  
| runtime        = 128 min 
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}}
 Telugu comedy film written and directed by G.Nageswara Reddy, produced by Dr. Vijaya Prasad Malla under Wellfare Creations banner and starring Allari Naresh and Poorna in lead roles. The film soundtrack was composed by Vandemataram Srinivas and Cinematography was handled by Adusumilli Vijay Kumar. Dialogues of the movie were written by popular writer Marudhuri Raja. Screenplay was handled also by G Nageswara Reddy.Legendary actor Nandamuri Balakrishna clapped at opening ceremony of the movie .  This film marks the debut of actress Poorna in Telugu Cinema. Seema Tapakai was released on 13 May 2011. 

==Plot==
Sri Krishna (Allari Naresh) is the son of a big businessman and multimillionaire, GK (Sayaji Shinde). His father is so rich that he hates even the smell of the poor, and despises anyone who talks about giving money as alms. Sri Krishnas family too, doesnt mind the opulence they have. Coming from this family, he falls in love with a Satya (Poorna), who thinks the rich become richer by exploiting the poor. In order to make Satya fall for him, Krishna starts acting as a poor chap, with a poor family. He buys his way out to show Satya that he helps the poor, and unintentionally becomes their savior of sorts. When Satya accepts his love, he recruits his family to help him convince her completely. So the rich father becomes a banana seller, rich mother becomes a bajji seller etc. However, it turns out that Satya is the daughter of Nagineedu (Nagineedu) a factionist from Kurnool who has a rivalry with another faction leader Venkatappa(Jaya Prakash Reddy). She urges her father to share his riches with his followers, an idea which his father doesnt take. But on knowing that she wants to get married to a poor boy, he doesnt mind it. In order to get them married the Krishna asks her father to act as a Head Master. Both families in their disguises respectively meet and arrange the marriage of Krishna and Satya. Faction leader Venkatappa attacks the family of GK to kill Nagineedu. Knowing the truth about Nagineedu, GK is angered and cancels the marriage. Krishna uses his brain to try and help resolve issues between Venkatappa and Nagineedu and also convince his father GK for the marriage. At the End, the two factionist understand the value of life and they stop fighting. Both GK and Nagineedu once again agrees upon Krishnas marriage.

== Cast ==
* Allari Naresh ... Sri Krishna
* Poorna ... Satya
* Sayaji Shinde ... GK (Gurajada Krishnamurthy)
* Sudha ... Rajyalakshmi (GKs Wife)
* L.B. Sriram ... Aravind
* Brahmanandam ... Melimbangaram
* M. S. Narayana ... GKs PA
* Nagineedu ... Nagineedu
* Jaya Prakash Reddy ... Venkatappa Jeeva
* Rao Ramesh ... Narsimha
* Vennela Kishore ... Kishore
* Uttej
* Ravi Prakash ... Manoj (GKs Son)
* Tirupati Prakash ... Servant
* Surekha Vani ... GKs Daughter
* Geetha Singh ... Maid

== Crew ==
* Banner: Wellfare Creations
* Music: Sri Vasanth
* Dialogues: Marudhuri Raja
* Cinematography: Adusumilli Vijay Kumar
* Fights: Ram-Laxman
* Editing: Kotagiri Venkateswara Rao
* Art: K.V. Subbarao
* Audiographer: E. Radha Krishna
* Lyrics: Bheems, Veturi Sundararama Murthy|Veturi, Bhaskarabhatla, Chilakarekka Ganesh
* Choreography: Brinda, Krishna Reddy, Prem Rakshith
* Story: G Nageswara Reddy
* Screenplay: G Nageswara Reddy
* Direction: G Nageswara Reddy
* Producer: Dr. Vijaya Prasad Malla
* Theatrical release date: May 13, 2011

==Production and Casting==
This movie was produced by Dr. Vijaya Prasad Malla who is also a MLA in Andhra Pradesh legislative assembly. The director of this movie G Nageswara Reddy had previously worked with Allari Naresh in the movie Seema Sastri as director. He collaborated for the second time with Allari Naresh for this movie. Both Naresh and Nageswara Reddy are known for their comedy in movies. As expected this movie was a comedy entertainer. The movie was delayed due to the Telugu movie strike of 2011. This was Allari Nareshs second release in the year after Aha Na Pellanta. Poorna who acted in several Tamil and Malayalam movies in the past was cast for the role of Satya, daughter of a factionist. This is Poornas first Telugu movie. Being a comedy movie, it features most of the Telugu comedians like L.B. Sriram,  Brahmanandam, M. S. Narayana, Sayaji Shinde, Rao ramesh, Jaya Prakash Reddy, Jeeva (Telugu actor)|Jeeva, Uttej, Vennela Kishore and Geeta Singh. Famous dialogue Writer Marudhuri raja assisted Nageswara Reddy with the dialogues in the movie. Shooting of the movie began in 2010 and was completed in April 2011 and the movie was released in May 2011. Shooting was predominantly done in Hyderabad.

==Release and Reception==
The movie was released in India and Overseas on 13 May 2011.   The movie was received with positive reviews. Suresh Kavirayani of Times of India gave a good 3 of 5 rating for the movie and said the movie was fun to watch with whole family.  Esskay from 123 Telugu gave a 3.5 of 5 rating for the movie and commented that the movie was the best entertainer for the season.  Sunita Chowdary from Cinegoer gave a very positive review of the movie and said that it was a fun film for all that summer.  The movie fared very well at the box-office and was declared a hit in the year 2011.

==Soundtrack==
{{Infobox album
| Name        = Seema Tapakai
| Type        = soundtrack
| Artist      = Vandemataram Srinivas
| Cover       =
| Released    =  
| Recorded    = 
| Genre       = Film soundtrack
| Length      = 
| Label       = Madhura Audio
| Producer    = Vandemataram Srinivas
}} Veturi and music by Bappi Lahiri was remixed by Vandemataram Srinivas for this movie. The song was sung by Javed Ali and Sravana Bhargavi.

{{Track listing
| extra_column    = Artist(s)
| lyrics_credits  = yes
| title1 = Dheere Dheere Dille | lyrics1 = Bheems  | extra1 = N. C. Karunya|Karunya, Geetha Madhuri | length1 = 4:25 Veturi | extra2 = Javed Ali, Sravana Bhargavi | length2 = 4:40
| title3 = I Love U Baby | lyrics3 = Bhaskarabhatla Ravikumar | extra3 = Viswa, Sravana Bhargavi | length3 = 4:15
| title4 = Dheere Dheere Dille | lyrics4 = Bheems | extra4 = N. C. Karunya|Karunya, Geetha Madhuri | length4 = 4:25
| title5 = Kandi Chenu | lyrics5 = Chilakarekka Ganesh | extra5 = Vedala Hemachandra|Hemachandra, Singer Usha|Usha, Noyal | length5 = 4:01
}}

==Remake== Hindi as Ajab Gazabb Love. Directed by Sanjay Gadhvi, the film stars Jackky Bhagnani and Nidhi Subbaiah in lead roles. 

==References==
 

== External links ==
*  

 
 
 