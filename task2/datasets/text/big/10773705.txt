Zehreela Insaan
{{Infobox Film
| name           =Zehreela Insaan 
| image          =Zehreela-Insaan.jpg 
| image_size     = 
| caption        = 
| director       = Puttanna Kanagal|S.R. Puttana Kanagal
| producer       = Virendra Sinha
| writer         = 
| genre          = Action
| starring       = Rishi Kapoor Moushumi Chatterjee Neetu Singh Ambareesh
| music          = R. D. Burman
| cinematography = S.N.Dubey
| editing        = 
| distributor    = 
| released       = 1974
| runtime        = 150 minutes
| country        =   India Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Zehreela Insaan is a 1974 Hindi film directed by Puttanna Kanagal and produced by Virendra Sinha. The film stars Rishi Kapoor, Moushumi Chatterjee, Neetu Singh, Pran (actor)|Pran, Nirupa Roy, Dara Singh, Iftekhar, Madan Puri and Paintal (comedian)|Paintal.

It is a remake of a 1972 Kannada movie Naagarahaavu.

Although the Hindi version did not do well in the box office and is forgotten, one song O Hansini from the film, sung by Kishore Kumar, still stands out. Both the Kannada and Hindi versions were based on a popular novel written by legendary Kannada writer T. R. Subba Rao (TaRaSu). The Hindi version was shot extensively in Chitradurga Fort in South Karnataka|Karnataka. Both the Kannada and Hindi versions directed by Puttanna Kanagal and the music was composed by R.D. Burman.

==Cast==
* Rishi Kapoor as Arjun Singh
* Moushumi Chatterjee as Aarti
* Neetu Singh as Margaret Pran as Masterji
* Ambareesh as (as Jaleel)
* Dara Singh as Pahelwan Manorama as Mary Dulari as Arjuns Mother
* Ratnamala as Mrs. Shyam Lal (as Ratanmala)
* Madan Puri as John (as Madanpuri)
* Iftekhar as Principal Vishamber Nath
* Raj Mehra as Arjuns Father (as Rajmehra)
* Sajjan as Shyam Lal Asit Sen as Murari Lal (as Ashit Sen)
* Paintal as Ranjeet
* Ghanshyam Rohera as Arjuns Classmate (as Ghan Shyam)
* Maruti Rao as Budhram (as Maruti)
* Jagdish Raj as Bidre (as Jagdishraj) Vijay Kumar as
* Nirupa Roy as Shobha
* Yogesh Chhabra as Tukaram Phillips




==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Lyricist !! Duration
|-
| 1
| "O Hansini"
| Kishore Kumar 
| Majrooh Sultanpuri
|  
|-
| 2
| "Dum Tumhari Dum"
| Kishore Kumar
| Majrooh Sultanpuri
|  
|-
| 3
| "Saanp Se Badhke Zehreela Insaan" Shailendra Singh 
| Majrooh Sultanpuri
|  
|-
| 4
| "Mere Dil Se Ye Nain Mile" Shailendra Singh, Asha Bhosle
| Majrooh Sultanpuri
|  
|-
| 5
| "Suno Kahani"
| Lata Mangeshkar
| Majrooh Sultanpuri
|  
|}





== External links ==
*  
The film based on Kannada Novel by TaRaSu (T R Subba Rao)
* http://www.mp3songsdk.com/Zehreela-Insaan_1974.html
* http://ww.raaga.com/channels/hindi/moviedetail.asp?mid=h000264

 
 
 
 
 

 