Submarine X-1
 
 
{{Infobox film
| name = Submarine X-1
| image = Subx1pos.jpg
| caption = Original film poster
| producer = John C. Champion William Graham
| screenplay = Donald S. Sanford    Guy Elmes
| story = John C. Champion Edmund North
| starring = James Caan David Sumner Norman Bowler Brian Grellis
| music = Ron Goodwin
| cinematography = Paul Beeson
| editing = John S. Smith
| studio = Mirisch Films
| distributor = United Artists
| released =  
| runtime = 90 minutes
| country = United Kingdom
| language = English
}} Operation Source German battleship Tirpitz in 1943.  In the film James Caan stars as Lt. Commander Richard Bolton, a Canada|Canadian, who must lead a group of midget submarines in an attack on a German battleship.

==Plot== Norwegian fiord.

The film progresses with Commander Bolton training the crews of the three submarines.  He must overcome tensions with some of his former crew members, while keeping their activities hidden from outsiders and German airplanes.  The crews successfully fend off an attack by German commandos, who discover their base.  Bolton is forced to make hasty preparations for his attack before their submarine base can be destroyed.

Two of the submarines are lost while attempting to cut through submarine nets at the entrance to the fiord.  One submarine crew is captured and taken to the German battleship for interrogation.  The surviving submarine penetrates the submarine nets in the fiord and places explosives under the German battleship.  The submarine then manages to escape as the battleship explodes.  The film concludes as the battleship sinks.

==See also== Above Us the Waves is a similar film about the true-life submarine attacks on the Tirpitz. Operation Source details the use of X class submarines against German battleships in World War II. Norwegian fiord.

==References==
 

== External links ==
*  
*  
*   at Yahoo! Movies

 

 
 
 
 
 
 
 
 
 