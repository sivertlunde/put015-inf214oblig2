Silent Bloodnight
{{Infobox film
| name           = Silent Bloodnight
| image          = SilentBloodnight.jpg
| alt            = 
| caption        = DVD cover
| director       = Stefan Peczelt   Elmar Weihsmann
| producer       = Stefan Peczelt   Elmar Weihsmann
| writer         = Stefan Peczelt   Elmar Weihsmann
| starring       = Mike Vega   Monica Baci   Andy Freund   Julia Melcher   Vanessa Vee   Markus Schöttl   Christina Conti   Christine Dune   Robert Cleaner   Alexander E. Fennon
| music          = Peter Zirbs   Harald Salaun
| cinematography = Reinhard Kofler   Volkmar Geiblinger
| editing        = Stefan Peczelt
| studio         = Tigerline Productions
| distributor    = Brain Damage Films
| released       =  
| runtime        = 84 minutes
| country        = Austria
| language       = English
| budget         = €350,000
| gross          = 
}}
Silent Bloodnight is a 2006 horror film written, directed and produced by Stefan Peczelt and Elmar Weihsmann.

== Plot ==
 skinny dipping. Nina follows Jacob and Jamie into the woods, observes them having sex, and is dragged off by Tony and Sammy. When Jacob and Jamie finish, Jamie goes off alone, and is chased by a man who has murdered Tony and Sammy. The man drowns Jamie in the lake, while Jacob (who was looking for Nina) watches in shock. Jacob goes to the police station, where he tells the chief that he received a call from his father, Willy, who he thinks has committed suicide.

While doing a feature for the news, reporter Sabrina Meyers (the chiefs daughter, who glimpsed a bloodied Jamie the night she died) finds Jamies necklace in the lake, theorizes Jacob (who says Nina was found and placed in a sanitarium, while Jamie went back home) murdered her, and asks around about him. Meanwhile, Sabrinas boyfriend Matt and three others (Joe, Cindy and Dana) are getting a summer camp ready. Sabrina uncovers more circumstantial evidence that something happened at the lake, but no one believes her stories, though her investigation draws the ire of the killer (who murders Sabrinas cameraman Andy and his girlfriend Jennifer) and his accomplice, a figure dressed in white that attacks Sabrina, but is scared off by her father.

People take Sabrina seriously when Jamies body is discovered (only to be stolen moments later) and a waitress Jamie had sought help from the night she died is found dead from bee stings (something arranged by the killer). The chief questions Jacob, and finds holes in his story, the main one being Ninas whereabouts. When her father finishes, Sabrina interviews Jacob herself, and goes home, where her father has stationed an officer to guard her. Matt stops by, and while he and Sabrina argue over her visiting Jacob, the white figure knifes the officer and Matt, and abducts Sabrina. The chief realizes something is wrong when he calls Sabrinas cellphone, and a dying Matt answers.

Sabrina regains consciousness and sneaks away from the white figure, revealed to be Jacob. Jacob and the main killer, his father Willy, are committing murders because Nina was raped and accidentally killed by Tony and Sammy. The two are wiping out witnesses to the duos crime, and their own homicides. Jacob and Willy realize Sabrina has escaped, and follow her to the summer camp, where they murder Dana and Joe. Sabrina and Cindy are forced into the back of Sammys stolen van by Jacob, who tries to poison them with exhaust fumes. As Jacob is driving, he is forced off the road and fatally shot by the chief, who revives Sabrina and Cindy. The film ends with Willys whereabouts unknown, and Sabrina giving an interview to the media.

== Cast ==

* Vanessa Vee as Sabrina Meyers
* Mike Vega as Mr. Meyers
* Alexander E. Fennon as William "Willy" Wilson
* Robert Cleaner as Jacob Wilson
* Christine Dune as Cindy
* Christina Conti as Jamie
* Andy Freund as Joe
* Julia Melchor as Dana
* Markus Schöttl as Matt
* Andrea Stotter as Nina Wilson
* Dirk Smith as Jhon the Cop
* Sabine Kranzelbinder as Jennifer
* Nikolaus Barton as Andy
* Monica Baci as Waitress
* Martin Thamer as Sammy Compton
* Daniel Eiper as Tony
* Tobi Schwarzbauer as Ice Cream Man
* GI Gerhard Falgenhauer as CSI Police Officer 1
* Monika Bergner as CSI Police Officer 2
* Tobias Schwarzbauer as CSI Photographer
* Stefan Peczelt as Cameraman
* Elmar Weihsmann as Lightman

== Reception ==

Slasherpool gave Silent Bloodnight one star out of five, and dubbed it "a homemade piece of shit" with a thin story, incompetent acting and lighting, terrible continuity, and cheesy music.  Vegan Voorhees (which awarded one and a half stars out of five) wrote that the film looked blurred and of poor quality, and suffered from incomprehensibility and laughable dialogue. 

Silent Bloodnight was deemed competently put together, but unoriginal, unintentionally comical and "inadvertently insane" by Horrorview, which bestowed upon it a score of one out of five.  Terror Titans called the film "a stupefying mess" but an entertaining one that did have enough good qualities (such as the bloodletting and camerawork) to warrant a score of five out of ten. 

== References ==

 

== External links ==

*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 