Gazal (1964 film)
{{Infobox film
| name           = Gazal
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Ved-Madan
| producer       = Ved-Madan
| writer         = 
| screenplay     = Agha Jani Kashmiri Majnu Lucknowi  (dialogue) 
| story          = 
| based on       =  
| starring       = {{plainlist|* Meena Kumari
* Sunil Dutt Rehman
* Prithviraj Kapoor}}
| narrator       = 
| music          = Madan Mohan (music director)| Madan Mohan Sahir Ludhianvi  (lyrics) 
| cinematography = Jagdish Chadha
| editing        = Prabhakar Gokhale
| studio         =     Natraj Productions  
| distributor    = 
| released       =  1964   
| runtime        = 129 minutes
| country        = India
| language       = Urdu/Hindi
| budget         = 
| gross          =  
}}
Gazal is a Urdu-Hindi romance musical film directed by Ved-Madan, starring Meena Kumari and Sunil Dutt. The muslim social film about the right of young generation to marriage of their choice. It had music by Madan Mohan (music director)| Madan Mohan with lyrics by Sahir Ludhianvi, featuring notable filmi-ghazals like "Rang Aur Noor Ki Baraat" performed by Mohammed Rafi and " Naghma O Sher Ki Saugaat" performed by Lata Mangeshkar.  

==Plot==
Set in Agra, the film is the story of Ejaz (Sunil Dutt), who is editor of Inquilab. When listens Naaz Ara Begum (Meena Kumari) sing, he falls deeply in love with her. However soon he loses her job. Though Ejaz and Naaz manage to meet secretly, with help of Naazs sister Kausar. This relationship has many people going against, first Akhtar Nawab (Rehman (actor)|Rehman), Naazs paternal cousin, who wishes to marry her and Nawab Bakar Ali Khan (Prithviraj Kapoor) her father.

==Cast==
* Meena Kumari as  Naaz Ara Begum
* Sunil Dutt as Ejaz Rehman as Akhtar Nawab
* Rajendra Nath as Dr. Nawab Mehmood Ali Khan
* Nazima as Kausar Ara Begum
* Raj Mehra as Babuji
* Minoo Mumtaz as Courtesan (in song "Yun Bhi Hai")
* Mridula Rani as Mrs. Bakar Ali Khan
* Pratima Devi as Ejazs Mother
* Mohsin Abdullah as  Ejazs Dad
* Prithviraj Kapoor as Nawab Bakar Ali Khan

Ghazals==Music==
{{Track listing
| extra_column = Singer(s)
|all_lyrics=Sahir Ludhianvi
|all_music=Madan Mohan (music director)| Madan Mohan   

| lyrics_credits  = 
| title1 = Rang Aur Noor Ki Baraat  | extra1 = Mohammed Rafi | lyrics1 = | length1 = 06:15
| title2 = Mujhe Ye Phool Na De  | extra2 = Mohammed Rafi Suman Kalyanpur | lyrics2 = | length2 = 03:17
| title3 = Naghma O Sher Ki Saugaat | extra3 = Lata Mangeshkar | lyrics3 = | length3 = 03:14
| title4 = Dil Khush Hai Aaj Unse | extra4 = Mohammed Rafi | lyrics4 = | length4 = 03:18
| title5 = Unse Nazrein Mili  | extra5 = Mohammed Rafi Minu Purshottam | lyrics5 = | length5 = 03:14
| title6 = Ishq Ki Garmi E Jazbaat | extra6 = Mohammed Rafi | lyrics6 = | length6 = 03:13
| title7 = Ada Katil Nazar Barkebala | extra7 = Lata MangeshkarAsha Bhosle | lyrics7 = | length7 = 03:18
| title8 = Mere Mahboob Kahin Aur Milakar Mujhse | extra8 = Mohammed Rafi | lyrics8 = | length8 = 03:18
}}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 