Sleepover (film)
 
{{Infobox film
| name = Sleepover
| image = Sleepover.jpg
| caption = Theatrical release poster
| director = Joe Nussbaum Bob Cooper Charles Weinstock
| writer = Elisa Bell
| starring = Alexa Vega Mika Boorem Jane Lynch Sam Huntington Sara Paxton Brie Larson Steve Carell Jeff Garlin
| music = Deborah Lurie
| cinematography = James L. Carter
| editing = Craig P. Herring
| studio = Landscape Entertainment Weinstock Productions
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 89 minutes
| country = United States
| language = English
| budget = $10 million
| gross = $10,148,953
}}
 2004 American teen film directed by Joe Nussbaum and starring Alexa Vega, Mika Boorem, Jane Lynch, Sam Huntington, Sara Paxton, Brie Larson, Steve Carell and Jeff Garlin.

==Plot==
On the last day of 8th grade, Julie Corky (Alexa Vega) has a slumber party. One of the girls she has invited, Staci Blake (Sara Paxton), declines, saying she is going to the high school dance with her boyfriend Todd (Thad Luckinbill). Julie is worried about how she will fit in during high school and her unrequited crush on Steve, a fellow highschooler. She is concerned she will not be popular enough to sit at The Fountain, the cool lunch spot. These worries are increased due to the fact that her best friend Hannah (Mika Boorem), is moving to Vancouver. At school, Julie, Hannah and their friend Farrah (Scout Taylor-Compton) witness Stacis friend Liz (Brie Larson) bullying Yancy (Kallie Flynn Childress), a girl their age who is somewhat overweight. Julie feels sorry for Yancy, and as a result invites her to her sleepover.

While leaving school Julie, Hannah, Farrah and Yancy are bombarded by Russell "SpongeBob" Hayes (Evan Peters) and his posse of skateboard pals Lance (Hunter Parrish) and Miles (Shane Hunter). SpongeBob tries to show Julie a picture of him in a coma, but she shows no interest. That night, Todd tells Staci they are skipping the dance much to her dismay. Instead, Todd drives Staci to a deserted location and makes continuous passes at her which she refuses, ultimately getting her dumped and left by Todd who drives off without her. After being dumped by Todd, Staci goes to Julies house and challenges the girls to a scavenger hunt. The winning team gets to sit at The Fountain and the losing team has to sit by the dumpsters. Julie is reluctant to accept, but Hannah does so despite Julies unease saying how she could be sitting right next to Steve at The Fountain.

Liz e-mails the scavenger hunt list to Julie which includes dressing a window mannequin at Old Navy in their own clothes, have a guy from DatesSafe.com buy them a drink at the Cosmo Club, stealing a decal off of a PatrolTech Security car and, to Julies horror, borrowing a pair of boxer shorts from Steve Phillips (Sean Faris). With her mom (Jane Lynch) out with a friend, and her dad (Jeff Garlin) preoccupied with fixing their houses water filter, Julie and the girls sneak out. Ren (Sam Huntington), Julies older college-dropout brother agrees to cover for them in exchange for money, which he needs. Immediately following the girls departure, SpongeBob, Lance and Miles skateboard to the house and climb up through an upstairs window that turns out to be Rens "former" room. Declaring it to be the "wrong room", Ren points them to Julies room.

Aided by Ren, the boys manage to avoid detection from Mr. Corky. They then notice the scavenger hunt list on Julies computer and print off their own sheet, deciding to join in on the hunt themselves. With the help of Yancys dads Nissan Hypermini, the girls make it to Old Navy. But Staci and Lizs team have already been there to dress their mannequins and lock the window display. The girls are forced to dress male mannequins and are caught in the act by Officer Sherman (Steve Carell), but escape by locking him in the display window with one of the mannequin arms. They then arrive at the Cosmo Club, but are not let in by the guard, no thanks to SpongeBob who suddenly shows up. Julie and Hannah sneak in by hiding in an empty drum case while Yancy and Farrah wait outside. A guy walks over and, seeing Yancy sitting alone, offers to sneak her in.

She declines before asking why he is talking to her, since usually guys only ask her to hold the door. He introduces himself as Peter (Ryan Slattery), and says he would never ask her to hold the door for him, making her smile as he then leaves. With Julie disguised as "June", her profile from the dating website, she suddenly discovers that her blind date is actually her English teacher Mr. Corrado (Johnny Sneed). He doesnt recognize her at first. But he eventually does he upon hearing her laugh and is shocked. When Julie explains the entire situation, he expresses understanding and admits that he went to the same high school as Julie will, but never got to sit by The Fountain. He then buys her a ginger ale as Hannah takes a picture of it for the hunt list and they leave. But as they do, they spot Julies mother dancing on one of the tables.
 Douglas Smith), which ignites curiosity in Steve as to who she is. Julie barely makes it home just in time to make the call. . After arriving at Steves house, the Hypermini needs charging. Hannah says they will find a plug while Julie goes and gets the boxers. Cautiously, Julie enters the house and is seen by Staci and Liz, who break into Steves car to get his shorts from an overnight bag. Humorously, Liz calls PatrolTech Security claiming to see a "suspicious person". Inside, Steve is looking through old yearbooks trying to find pictures of Julie, but puts them down to get ready for the dance.

Julie hides in the shower as Steve comes into the bathroom, turning the shower on and, unknowingly, undressing in front of Julie. Outside, the Hypermini crashes into Officer Shermans patrol car, causing Steve and Gregg to look out the window and giving Julie the opportunity to flee, grabbing the boxers as she goes. Julie is stopped by Officer Sherman, but the girls manage to drive by and give her the chance to quickly jump in and to take off. Julie takes the decal off the PatrolTech cruiser as she races for the car and everyone makes a dash for the high school parking lot. Having stopped for drinks, Staci and Liz barely make it to the parking lot in time. Staci suggests a tie breaker, which Julie and Hannah reluctantly accept. The first team to get the king or queens crown at the dance will win. Inside the dance, SpongeBob asks Farrah if they won. She tells them they just have to get the crown first.

SpongeBob pencils in "get crown" to his list. Right before the dancing contest, Todd is spotted kissing a girl, who claims to be his girlfriend for the past six months. Staci confronts them, which turns into a cat fight which she loses. Belittled, SpongeBob sticks up for Staci, much to her surprise. To everyones surprise Staci enters the dance contest with SpongeBob since he had defended her. During the contest, SpongeBobs list falls out of his pocket, which is eventually found and picked up by Steve. Gregg then points Julie out to him on the dance floor. Staci and SpongeBob win the dance and get their picture taken. SpongeBob excitedly shows Staci his coma picture, but states that the photo of them winning is way cooler. Farrah accidentally lets it slip to Yancy that Staci was originally the third guest, not her. She is sad and feels betrayed at first, but bounces back quickly after Peter turns up and dedicates a song to her and they share a dance.

Steve is named King and chooses a surprised Julie as his dance partner. During the dance, Steve takes his crown off and places it on Julies telling her "you win", gaining victory for Julie and her friends. Outside, Steve reveals how he knew about the scavenger hunt due to the list he found inside the dance. They are about to kiss when Julie gets a call from Ren urging them to hurry home because their mom called and is coming back. Julie apologizes and leaves a disappointed Steve in front of The Fountain as she and her friends race home, resulting Julie losing her crown in the process. When Officer Sherman passes on a bike, he runs over it and flips over with his bike. He gives up looking for Julie and calls his mother to come take him home. The girls make it home just in time to pretend being asleep as Mr. and Mrs. Corky come in to check on them.

It is revealed that Mr. Corky knows about his wifes clubbing, but doesnt mind it at all since everyone needs a night out every now and again. The next morning at breakfast, Julies mom confronts her asking "exactly" what they did last night, showing Julie the scarf she had dropped in the Cosmo club. Surprisingly, Mrs. Corky is not mad but confesses it is difficult to believe how fast Julie is growing up. After her friends leave, Julie goes back to her room where she notices her dented crown outside her window. As she reaches for it, Steve comes into view and places the crown on her head again. They then kiss passionately. The film ends with a scene of Staci and Liz, now in high school, eating their lunch by the school dumpsters among the trash and the social rejects.

==Cast==
* Alexa Vega as Julie Corky
* Mika Boorem as Hannah Carlson
* Jane Lynch as Gabby Corky
* Sara Paxton as Staci Blake
* Sam Huntington as Ren Corky
* Brie Larson as Liz Daniels
* Kallie Flynn Childress as Yancy Williams
* Scout Taylor-Compton as Farrah James
* Sean Faris as Steve Phillips
* Steve Carell as Officer Sherman Shiner
* Jeff Garlin as Mr. Corky
* Evan Peters as Russell "SpongeBob" Hayes
* Hunter Parrish as Lance Gregory
* Alice Greczyn as Linda
* Thad Luckinbill as Todd
* Katija Pevec as Molly
* Johnny Sneed as Mr. Corrado
* Eileen April Boylan as Jenna Allen
* Ryan Slattery as Peter
* Summer Glau as Ticket Girl
* Max Van Ville as Skater Dude
* Shane Hunter as Scooter Douglas Smith as Gregg

==Reception==
The film opened at #10 in the box office with $4,171,226. The film would later make $9,436,390 in the United States and $712,563 internationally, resulting in a $10,148,953 gross worldwide, on a $10 million budget. 

==Soundtrack==
# "Imaginary Superstar- Skye Sweetnam
# "Freeze Frame" - Jump5
# "I Want Everything" - Hope 7 No Secrets
# "Stuck" - Allister
# "Havin Fun" - Planet Melvin
# "Remember" - Gabriel Mann
# "We Close Our Eyes" - Allister
# "Hole in the Head" - Sugababes
# "Next Big Me" - Verbalicious
# "Heaven Is a Place on Earth" - Becky Baeling
# "Wannabe (song)|Wannabe" - Spice Girls

==Novelization== Scholastic Inc. American fantasy and science fiction author Suzanne Weyn.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 