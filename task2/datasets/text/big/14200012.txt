Horror Hospital
 
 
{{Infobox film| name = Horror Hospital
 | director = Antony Balch
 | image	=	Horror Hospital FilmPoster.jpeg Richard Gordon
 | writer = Antony Balch Alan Watson
 | starring = Robin Askwith Michael Gough 1973 (United Kingdom|UK)
 | runtime = 85 min. English
  }}

Horror Hospital is a 1973 British horror-comedy film starring Robin Askwith, Michael Gough, Dennis Price and Skip Martin.  It was the penultimate film directed by Antony Balch. The film was originally released on DVD in the US by Elite Entertainment on 2 November 1999. A new remastered DVD with a new commentary from producer Richard Gordon was released by Dark Sky Films on 15 June 2010.

==Plot==
When attempts to break into the pop business leaves him with nothing but a bloody nose, songwriter Jason Jones (Robin Askwith) decides to take a break with "Hairy Holidays", an outfit run by shifty, gay travel agent Pollock (Dennis Price). After failing to chat Jason up, Pollock sends him to pseudo-health farm Brittlehurst Manor. On the train journey there Jason meets Judy (Vanessa Shaw) who is also on the way to the same destination to meet her long lost unt. Both are unaware that the health farm (i.e. "Horror Hospital") is a front for Dr. Storm (Michael Gough) and his lobotomy experiments that turn wayward hippies into his mindless zombie slaves. The wheelchair-bound doctor surrounds himself with an entourage that includes Judys aunt, erstwhile brothel madam Olga (Ellen Pollock), dwarf Frederick (Skip Martin) and numerous zombie biker thugs. Dr. Storm also has a Rolls Royce fitted with a giant blade that decapitates escapees and interfering parties. Abraham (Kurt Christian) arrives at the Horror Hospital "looking for his chick" and is promptly whacked around the head by the motorcycle zombies. Frederick, fed up at literally being Storms whipping boy helps the kids escape—paving the way for the 70s youth to put the final spanner in the works to Storms scheme.

==Cast==
* Michael Gough	 -	Dr. Christian Storm
* Robin Askwith	- 	Jason Jones
* Vanessa Shaw	- 	Judy Peters
* Ellen Pollock	- 	Aunt Harris
* Dennis Price	- 	Mr. Pollack
* Skip Martin - 	Frederick
* Kurt Christian	- 	Abraham
* Barbara Wendy	- 	Millie
* Kenneth Benda	- 	Carter
* Martin Grace	- 	Bike Boy
* Colin Skeaping	- 	Bike Boy
* George Herbert	- 	Laboratory Assistant
* Susan Murphy            -       Lobotomy Victim Number 1.
* James Boris IV        - 	"Mystic" Band Member (as James IV Boris)
* Allan Hudson   -  	"Mystic" Band Member (as Allan "The River" Hudson)
* Simon Lust	- 	"Mystic" Band Member 
* Alan Watson	-  Transvestite in Club (uncredited)
* Antony Balch	- 	Bearded Man in Club/Bike Boy (uncredited)
* Ray Corbett	- 	Hunting Man (uncredited) Richard Gordon	- 	Man in Club (uncredited)

==Production==
After the success of his feature film debut Secrets of Sex (1969), an anthology sex film that flirted with horror themes, Balch envisioned his second film as an out and out horror film and one with a continuous narrative.
Location filming was undertaken in and around Knebworth House near Stevenage, Hertfordshire.

===Writing===
The script was written by Balch and his friend Alan Watson during the 1972 Cannes Film Festival, although the films title was thought up before the plot. Among Watson’s ideas for the Horror Hospital script was the lethal Rolls Royce, with its giant blade that decapitated people as it drove by.

===Filming===
The film was shot on a four-week schedule beginning on 16 October 1972. Shooting was done at Merton Park (mainly the pop group scene), Battersea Town Hall (which provided the interiors of Brittlehurst Manor) and Knebworth House.

The film’s last night party on 11 November was compromised when Phoebe Shaw served cake that was laced with drugs. In his autobiography Askwith wrote "I don’t know what she put in the cake but I ended up with a twenty stone electrician Roy, sitting on my lap telling me he thought he was in love with me." Only producer Richard Gordon managed to avoid eating the cake.

===Casting===
Robin Askwith’s role was specially written for him after he appeared in Gordon’s previous 1972 production Tower of Evil. Balch asked Michael Gough to base his performance on Bela Lugosi, screening him a 16mm print of The Devil Bat, in which Lugosi plays a mad, perfume manufacturer.
 David Copperfield and brief roles as a boutique assistant in Say Hello to Yesterday (1970) and a police cadet in Ooh… You Are Awful (1972). 

Veteran character actor, Dennis Price, and Dwarfism|dwarf actor Skip Martin, who ran a tobacconist in between acting assignments, also appeared in the film. As well as Kurt Christian whose full title was Baron Kurt Christian Von Siengenberg, and who left the country not long after the film was released. His ambition at the time, according to Films and Filming magazine, was to "play a role that does not involve killing somebody".
Nicky Henson was originally considered for Christian’s role.

==Soundtrack==
Horror Hospital also contains a pop music number "Mark of Death", composed by Jason DeHavilland and performed by the group Mystic (James IV Boris, Alan "The River" Hudson, Simon Lust).

Norwich (England) hip-hop group Stonasaurus recorded a concept album about the film in 2003.

==Release==
The DVD release is set for 15 June 2010 by MPI Media Group. 

== References ==
 

==External links==
* 

 

 
 
 
 
 