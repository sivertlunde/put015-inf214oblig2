Glenn Tilbrook: One for the Road
{{multiple issues|
 
 
}}

Glenn Tilbrook: One for the Road is a 2004 documentary directed by Amy Pickard which follows new wave group Squeeze (band)|Squeeze. 
 title =Id film him brushing his teeth|publisher=the Guardian|date=24 January 2006|author=
Caroline Sullivan}}  - shows Tilbrook attempting to mount a month-long US tour using a mobile home instead of a tour bus and hotels. The film centres on Tilbrooks apparent good humour in the face of a series of calamities - including vehicle breakdown - and his unusual stagecraft. At one stage he takes his entire audience into a car park and in another sequence performs in a fans apartment.

The film features performance excerts from a number of Squeeze and Tilbrook songs including: Up the Goodbye Girl", "Some Fantastic Place" and "By The Light of the Cash Machine".

The film premiered in 2004 at Londons Raindance festival and has since been released on DVD. 

==References==
 

 
 
 


 