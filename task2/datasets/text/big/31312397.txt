Manifesto (film)
Manifesto American comedy comedy drama film directed by Dusan Makavejev and starring Camilla Søeberg, Alfred Molina and Simon Callow.  It is based on the novel Pour une nuit damour by Émile Zola. In the 1920s an autocratic central European monarch plans a visit to a small town where revolutionaries plan to assassinate him.

==Cast==
* Camilla Søeberg - Svetlana Vargas
* Alfred Molina - Avanti
* Simon Callow - Police Chief Hunt
* Eric Stoltz - Christopher
* Lindsay Duncan - Lily Sachor
* Rade Serbedzija - Emile
* Svetozar Cvetkovic - Rudi Kugelhopf
* Chris Haywood - Wango
* Patrick Godfrey - Doctor Lombrosow
* Linda Marlowe - Stella Vargas
* Ronald Lacey - Conductor
* Tanja Boskovic - Olympia
* Gabrielle Anwar - Tina
* Enver Petrovci - The King
* Zeljko Duvnjak - Martin

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 