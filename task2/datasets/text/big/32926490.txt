Altiplano (film)
{{Infobox film
| name           = Altiplano
| image          = Altiplano_film.jpg
| alt            =  
| caption        = DVD release poster
| director       = Peter Brosens, Jessica Woodworth
| producer       = Heino Deckert  Ma.Ja.De Fiction
| writer         = Peter Brosens, Jessica Woodworth
| starring       = Magaly Solier Jasmin Tabatabai Olivier Gourmet
| music          = 
| cinematography = Francisco Gózon
| editing        = Nico Leunen
| studio         = 
| distributor    = Imagine Film Distribution (Benelux) Farbfilm Verleih (Germany) Cineworx (Switzerland) First Run Features (USA, Canada)
| released       =   
| runtime        = 109 minutes
| country        = Peru  Belgium Persian
| budget         = 
| gross          = 
}}
Altiplano is a film by Peter Brosens and Jessica Woodworth starring Magaly Solier, Jasmin Tabatabai and Olivier Gourmet. It takes places on three continents in five different languages. It tells the stories of two women in mourning and how their destinies merge.

==Plot summary== mercury that causes many people of the nearby village Turubamba to succumb to illness. Max and his fellow physicians suspect toxins to be the reason for the affection. They decide to collect more data in Turubamba.

Meanwhile Saturnina, a young woman from the village loses her fiancé to the contamination. Upon the physicians arrival Saturninas mother angrily rejects the doctors request to examine the body. The villagers turn their rage on the doctors and stone Max to death. Saturnina leads an unsuccessful demonstration against the mines truck drivers. After its dissolution Saturnina commits suicide by drinking quicksilver and films her death on the camera Max had dropped when he was killed.

Grace sets out on a journey to the place of Maxs death. Saturninas mother welcomes her and offers hospitality. Grace watches the video. In the end she partakes in Saturninas funeral and finally ends her mourning over her husband.

==Production==
Shooting took place on locations in Belgium and Peru for 43 days between June and October 2008. Due to extreme weather conditions at 5.000 m height in the Andes in Peru the crew and cast had access to a medical team 24 hours a day.

Although the film is fictional it is inspired by true events that took place in 2000 in the Peruvian village Choropampa District|Choropampa. Furthermore some characters like Saturnina and Max are also based on reports and anecdotes of local villagers and foreign doctors. 
 
 

==Cast==
* Magaly Solier as Saturnina
* Jasmin Tabatabai as Grace
* Olivier Gourmet as Max
* Behi Djanati Ataï as Sami
* Edgar Condori as Nilo/Omar
* Sonia Loaiza as mother
* Edgar Quispe as Ignacio
* Norma Martinez as female doctor
* Rodolfo Rodríguez as Raúl

==Reception and awards==
The film has gained mainly positive response.   It has also won a number of independent film awards:

* Bangkok International Film Festival 2009:  Special Golden Kinnaree Award for Environmental Awareness 
* Festival Film Europeen de Virton 2009: Prize of the City Virton
* Lucania Film Festival 2010: Best Feature Film  
* Festival de Cinema de Avanca 2010: Best Feature Film  
* Festival de Cinema de Avanca 2010: Best Actress for Magaly Solier
* International Film Festival Tofifest 2010: Special Jury Award  
The film was nominated for two Magritte Awards in the category of Best Co-Production and Best Costume Design for Christophe Pidre and Florence Scholtes in 2011. 

==References==
 

==External links==
*  
*  
*  
*   (German)
*  

,

 
 
 
 