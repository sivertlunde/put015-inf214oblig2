Paisa Yeh Paisa
{{Infobox film
| name           = Paisa Yeh Paisa
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sohanlal Kanwar	 
| producer       = Ashok Khanna
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Jackie Shroff Meenakshi Seshadri
| music          = Usha Khanna
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Sohanlal Kanwar and produced by Ashok Khanna. It stars Jackie Shroff and Meenakshi Seshadri in pivotal roles.

==Cast==
* Jackie Shroff...Shyam
* Meenakshi Seshadri...Sapna
* Deven Verma...Sukhiram
* Nutan...	Laxmi
* Om Shivpuri...Seth Chunilal
* Satyendra Kapoor...Professor
* Amrish Puri...Jugal
* Gulshan Grover...Guddu

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jis Dil Mein Pyar Na Ho"
| Kishore Kumar
|-
| 2
| "Sajna O Sajna"
| Anuradha Paudwal
|-
| 3
| "Mera Malik Mera Dil Hai"
| Lata Mangeshkar
|-
| 4
| "Jor Jor Se Bajao Jara Band"
| Mohammed Aziz, Anuradha Paudwal
|}
==External links==
* 

 
 
 

 