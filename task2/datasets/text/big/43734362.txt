The Light of Western Stars (1925 film)
{{Infobox film
| name          = The Light of Western Stars
| image          =
| image size      =
| director       = William K. Howard
| producer       = Adolph Zukor Jesse Lasky
| writer         = Zane Grey(novel)  George C. Hull(adaptation) Lucien Hubbard(adaptation)
| cinematography = Lucien Andriot
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 90 minutes; 7 reels
| country        = United States
| language       = Silent film..(English intertitles)

}} Jack Holt, Noah Beery. The film was based on a Zane Grey novel and had been filmed before in 1918. 

==Cast== Jack Holt - Gene Stewart
*Billie Dove - Madeline Hammond Noah Beery - Brand
*Alma Bennett - Bonita William Scott - Al Hammond George Nichols - Billy Stillwell
*Mark Hamilton - Monty Price
*Bob Perry - Nelse (*as Robert Perry)
*Eugene Pallette - Stub (*as Gene Pallette)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 