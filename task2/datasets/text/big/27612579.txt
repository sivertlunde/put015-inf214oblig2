One for the Money (film)
 
{{Infobox film
| name           = One for the Money
| image          = One for the Money Poster.jpg
| image_size     = 220px
| director       = Julie Anne Robinson
| producer       = Sidney Kimmel Wendy Finerman Tom Rosenberg Gary Lucchesi
| screenplay     = Liz Brixius Stacy Sherman Karen Ray Karen McCullah Lutz
| based on       =  
| starring       = Katherine Heigl Jason OMara Daniel Sunjata John Leguizamo Sherri Shepherd Debbie Reynolds
| music          = Deborah Lurie
| cinematography = Jim Whitaker
| editing        = Lisa Zeno Churgin Sidney Kimmel Entertainment Lionsgate
| released       =  
| runtime        = 91 minutes  
| country        = United States
| language       = English
| budget         = $40 million 
| gross          = $36,893,721   
}} crime comedy 1994 novel Kirsten Smith. It stars Katherine Heigl, Jason OMara, Debbie Reynolds, Daniel Sunjata and Sherri Shepherd.

==Plot==
Stephanie Plum, out of work and out of cash, turns in desperation to her disreputable cousin Vinnie, of Vinnies Bail Bonds, for work. Despite having no equipment, training or particular skill she becomes a bail enforcement agent, chasing after Vinnies highest stakes bail jumper: Joe Morelli, a former vice cop who is wanted for murder, who also happened to seduce and dump Stephanie back in high school after taking her virginity.

In the midst of the chase, Stephanie has to deal with her meddling family, a problematic tendency of witnesses who die when she gets too close, and lessons in bounty hunting from the mysterious Ranger. When she finally catches up to Morelli, she realizes that the case against him doesnt add up and that the old flame from their school days may just be rekindling.

==Cast==
* Katherine Heigl as Stephanie Plum
* Jason OMara as Joseph "Joe" Morelli
* Daniel Sunjata as Ricardo "Ranger" Carlos Manoso
* John Leguizamo as Jimmy Alpha
* Sherri Shepherd as Lula
* Debbie Reynolds as Grandma Mazur
* Patrick Fischler as Vinnie Plum
* Ana Reeder as Connie Rossoli
* Gavin-Keith Umeh as Benito Ramirez
* Ryan Michelle Bathe as Jackie
* Nate Mooney as Eddie Gazarra
* Debra Monk as Mrs. Plum
* Louis Mustillo as Mr. Plum
* Annie Parisse as Mary Lou
* Fisher Stevens as Morty Beyers
* Danny Mastrogiorgio as Lenny
* Leonardo Nam as John Cho
* Adam Paul as Bernie Kuntz

==Production== Lionsgate announced Sidney Kimmel The Last Song) would direct. 
 metropolitan Pittsburgh Ambridge in Beaver County, Central Northside UPMC facility in the inner suburb of Braddock, Pennsylvania|Braddock, doubled for the books setting of Trenton, New Jersey, neighborhoods and government buildings. Establishing shot of bridge overlooking Trenton, New Jersey was filmed in Kittanning, Pennsylvania. 

==Release==
The film was released on January 27, 2012.  

==Reception==
The film was not initially screened for critics and has been widely panned; it currently holds a 2% rating on Rotten Tomatoes based on 53 reviews with the consensus: "Dull and unfunny, One for the Money wastes Katherine Heigls talents on a stunningly generic comic thriller." 

Despite the poor reception, author Janet Evanovich was delighted with how the film turned out and did some joint interviews with Heigl to promote the film. Evanovich stated that she would now envision Heigl as Stephanie when writing the character. 

===Box office===
The film debuted at #3 behind   with $11.5 million on its opening weekend.  One for the Money grossed $26,414,527 domestically and $10,479,194 globally to a total of $36,893,721 worldwide, below its $40 million budget. 

===Awards===
Heigl was nominated for a  .  

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 