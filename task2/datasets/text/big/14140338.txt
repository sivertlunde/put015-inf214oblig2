Resurrected (film)
{{Infobox film
| name           = Resurrected
| image          = Resurrected1989.jpg
| caption        = DVD cover art
| director       = Paul Greengrass
| producer       = Adrian Hughes Tara Prem
| writer         = Martin Allen Tom Bell Rita Tushingham David Thewlis Rudi Davies
| music          = John E. Keane
| cinematography = Ivan Strasburg
| editing        = Dan Rae
| distributor    = Castle Pictures Ltd. Hobo Film Enterprises
| released       = February 1989
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 1989 drama directorial debut, British Armed soldier Philip Williams,  who is presumed dead and left behind in the Falklands but is accused of desertion when he reappears seven weeks after the Falklands War ends.

Resurrected premiered at the 39th Berlin International Film Festival in February 1989.   

== Cast ==
  Tom Bell as Mr. Deakin
* Rita Tushingham as Mrs. Deakin
* David Thewlis as Kevin Deakin
* Rudi Davies as Julie
* Michael Pollitt as Gregory Deakin
* Christopher Fulford as Slaven
* Ewan Stewart as Corporal Byker
* David Lonsdale as Hibbert Peter Gunn as Bonner
* William Hoyland as Captain Sinclair
* Mark Wing-Davey as Major Dunbar
* Gary Mavers as Johnny Fodden
* Kenny Ireland as Denzil Clausen
* Philomena McDonagh as Ileen Clausen
* Lorraine Ashbourne as Reeva
* Michelle Ohare as Nurse 1
* Steve Coogan as Youth 2
 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 