Kunjali Marakkar (1967 film)
{{Infobox film 
| name           = Kunjali Marakkar
| image          =
| caption        =
| director       = SS Rajan
| producer       = TK Pareekutty
| writer         = K Padmanabhan Nair
| screenplay     = K Padmanabhan Nair
| starring       = Prem Nazir Sukumari PK Saraswathi PK Sathyapal
| music          = BA Chidambaranath
| cinematography = 
| editing        = G Venkittaraman
| studio         = Chandrathara
| distributor    = Chandrathara
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam historical film, directed by SS Rajan and produced by TK Pareekutty. The film stars Prem Nazir, Sukumari, PK Saraswathi and PK Sathyapal in lead roles. The film had musical score by BA Chidambaranath.    The film won the National Film Award for Best Feature Film in Malayalam. 

==Cast==
 
*Prem Nazir as Antonio/Narayana Nair
*Sukumari
*PK Saraswathi
*PK Sathyapal
*P.J. Antony as Zamorin’s nephew
*Jyothilakshmi
*Kottarakkara Sreedharan Nair as Kunjali Marakkar, Chief Naval Officer of Samoothiri
*Kunjava
*Kuthiravattam Pappu
*Kuttyedathi Vilasini
*Premji as Zamorin of Calicut
*Santha Devi
 

==Soundtrack==
The music was composed by BA Chidambaranath and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aattinakkare || P Jayachandran, AK Sukumaran, AP Komala, B Vasantha, KP Chandramohan || P. Bhaskaran || 
|-
| 2 || Muttathu Pookkana || P. Leela || P. Bhaskaran || 
|-
| 3 || Neeyillaathaarundabhayam || S Janaki || P. Bhaskaran || 
|-
| 4 || Ololam Kaavilulla || S Janaki || P. Bhaskaran || 
|-
| 5 || Oru Mullappoomaalayumaay || P Jayachandran, Prema || P. Bhaskaran || 
|-
| 6 || Udikkunna Sooryane || K. J. Yesudas, P Jayachandran, AK Sukumaran || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 

 
 
 
 
 

 