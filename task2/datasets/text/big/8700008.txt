A Sunday in Hell
{{Infobox Film
| name           = A Sunday in Hell
| image          = 
| caption        = 
| director       = Jørgen Leth
| producer       = Christian Clausen
| writer         = Jørgen Leth
| narrator       = David Saunders
| starring       = 
| music          = Gunner Møller Pedersen
| cinematography = 
| editing        = Lars Brydesen
| distributor    = 
| released       = 1976
| runtime        = 111 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}
A Sunday in Hell (original title: En Forårsdag i Helvede) is a 1976 Danish documentary film directed by Jørgen Leth. The film is a chronology of the 1976 Paris–Roubaix bicycle race from the perspective of participants, organizers and spectators.

Paris–Roubaix is the most famous and usually the most dramatic of the spring classics. Much of the latter portion is over narrow, cobbled tracks that choke with dust on dry days and become slick and muddy in rain. For the riders its a challenge to keep going without puncturing or crashing.

The film captures not just the events of the 1976 edition but the atmosphere of a professional race. It begins by introducing the contenders: Eddy Merckx, Roger De Vlaeminck (the previous years winner), Freddy Maertens, and Francesco Moser, each with their supporting riders (the Cycling domestique|domestiques), who are charged with helping their team leader win. The film gives views of the team directors, protesters (the race is halted for a while), spectators, mechanics and riders. As the cobbled section is entered the selection begins. Riders puncture, crash, make the wrong move - the race plays out. By the finish in the velodrome in Roubaix only a few are in with a chance. The winner is a surprise, but that is part of the appeal. Post-race the exhausted riders, mired in dirt, give interviews in the velodromes showers. They look like men who have been to hell and back.

 "You can see every bead of sweat on the cyclists and every smashed-up ankle. It really makes you never want to get on a bike again. But it is an amazing film." - Nick Fraser, BBC commissioning editor  
 "Arguably the best film ever made about professional cycling" - Peter Cowie, International Film Guide  

==References==
 

==External links==
* 
*  
*  (Storyville (television series)|Storyville)
* 
* 

 
 
 
 
 
 
 
 


 
 