The Squall
 
{{Infobox film
| name           = The Squall
| image          = File:Film_Poster_for_The_Squall.jpg
| caption        = Theatrical release poster
| director       = Alexander Korda
| producer       = Ray Rockett Jean Bart (play)   Bradley King   Paul Perez Richard Tucker   Alice Joyce   Loretta Young  Carroll Nye
| music          = Leo F. Forbstein
| editing        = Edward Schroeder
| cinematography =  John F. Seitz
| studio         = First National Pictures
| distributor    = Warner Bros. Pictures 
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} American drama Richard Tucker, Jean Bart. The film has been released to DVD on the Warner Archive Collection label.

==Cast== Richard Tucker – Josef Lajos
* Alice Joyce – Maria Lajos
* Loretta Young – Irma
* Carroll Nye – Paul Lajos
* ZaSu Pitts – Lena
* Harry Cording – Peter
* George Hackathorne – Niki
* Marcia Harris – Aunt Anna
* Knute Erickson – Uncle Dani
* Myrna Loy – Nubi
* Nicholas Soussanin – El Moro

==Plot==
In Hungary, a gypsy girl caught in a storm takes shelter in a farmhouse. She entertains all the male members of the household, leading to jealous anger. The spell she has put on the house is only lifted when her husband arrives and takes her away.   

==Production== Night Watch Silent to Burbank studios as the only sound stage there was used by Warner Brothers during the day. 

==Preservation==
Print held by the Library of Congress. 

==References==
 

==Bibliography==
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 