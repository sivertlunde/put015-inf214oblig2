One on One (1977 film)
{{Infobox film
| name = One on One
| image = one on one1977.jpg
| image_size = 215px
| alt = 
| caption = Promotional poster
| director = Lamont Johnson Martin Hornstein
| writer = Robby Benson Jerry Segal
| starring = Robby Benson Annette OToole Charles Fox
| cinematography = Donald M. Morgan
| editing = Robbe Roberts
| distributor = Warner Bros.
| released =  
| runtime = 98 minutes
| country = United States
| language = English
}}
One on One is a  1977 drama film starring Robby Benson and Annette OToole. It was written by Benson and Jerry Segal (Robbys father), and shot on location in 1976 at Colorado State University. The film features songs from Seals and Crofts and was directed by Lamont Johnson.

==Plot==
Henry Steele (Robby Benson), a naive high school basketball star, wins a college scholarship. Talented but with a tendency to show off, Henry must overcome the pressures of bullying from his team members and a confrontation with a mean-spirited coach (G.D. Spradlin). 

On the academic side, Henry must deal with his lack of reading skills. He is assigned a tutor to help him through the semester, the beautiful Janet Hays (Annette OToole); her academic and emotional support help Henry get through freshman year.

==Cast==
* Robby Benson - Henry Steele
* Annette OToole - Janet Hays
* G.D. Spradlin - Coach Moreland Smith
* Lamont Johnson - Barry Brunz
* Melanie Griffith - The Hitchhiker
* Hector Morales - Gonzales
* Gail Strickland - B.J. Rudolph

==Reception==
The film currently has an 83% Fresh rating on Rotten Tomatoes. 

==Soundtrack==
 
Seals and Crofts would perform the bulk of the songs on the films soundtrack, including "My Fair Share", which peaked at #28 on the Hot 100 on 19 November 1977.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 

 