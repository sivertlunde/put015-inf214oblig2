Get Out Your Handkerchiefs
{{Infobox film
| name           = Get Out Your Handkerchiefs
| image          = Préparez vos mouchoirs.jpg
| caption        = French film poster
| director       = Bertrand Blier
| producer       = Paul Claudon Georges Dancigers Alexandre Mnouchkine
| writer         = Bertrand Blier
| starring       = Carole Laure Gérard Depardieu Patrick Dewaere Michel Serrault Riton Liebman
| music          = Georges Delerue
| cinematography = Jean Penzer
| editing        = Claudine Merlin
| distributor    = New Line Cinema
| released       =  
| runtime        = 105 minutes
| language       = French
| country        = France
}}
Get Out Your Handkerchiefs ( ) is a 1978 French  , September 23, 1993, URL accessed February 25, 2013.  directed by Bertrand Blier. The film won the Academy Award for Best Foreign Language Film at the 51st Academy Awards.   

==Plot==
Raoul (Gérard Depardieu) and his wife Solange (Carole Laure) are eating in a restaurant when Raoul expresses concern with Solanges apparent depression, as she eats little, suffers migraines and insomnia and also sometimes faints. He finds another man in the room, Stéphane (Patrick Dewaere), to be her lover and hopefully enliven her again. Stéphane is puzzled by Raouls plan but gives in to his desperate appeals for help. The two men take turns sleeping with Solange, and both try to impregnate her without success, believing a lack of a child to be the source of her depression.

Raoul, Solange and Stéphane work at a boys camp in the summer, where they meet a 13-year-old math prodigy named Christian Belœil (Riton), who is bullied by the other boys. Solange becomes protective of Christian and one night lets him sleep in her bed. She awakes to find Christian exploring her body and scolds them.  They make up and have sex, despite a drastic age difference. Afterwards, Solange becomes dependent on the boy, to the point where she, Raoul and Stéphane kidnap him from his boarding school. Christian eventually impregnates her, and the film ends with Raoul and Stéphane walking away after serving six months in prison.

==Production== Going Places New York New York, October 16, 1978, p. 120. 

==Reception==
The film had a total of 1,321,087 admissions in France. http://www.jpbox-office.com/fichfilm.php?id=7799 
 New York Time Out called it "an erratic, often hilarious movie."  In his 2002 Movie & Video Guide, Leonard Maltin gives the film three and a half stars and calls it "disarming" and "highly unconventional." Leonard Maltin, ed., Leonard Maltins 2002 Movie & Video Guide. A Signet Book, 2001, p. 514.  Arion Berger writes that "to experience Get Out Your Handkerchiefs is to watch a master at the peak of his powers."  An Epinions critic wrote "Get Out Your Handkerchiefs is good for some laughs while flaunting somewhat outrageous disregard for standard sexual mores." 

The film won the Academy Award for Best Foreign Language Film  and was named the best film of 1978 by the  , February 12, 1979, vol. 11, no. 6. 

Not all reviews were favourable, as People (magazine)|People wrote the humour could be "downright incomprehensible" and "so airy it floats right off the screen." 

Variety (magazine)|Variety wrote that "a rather bizarre mixture of gritty comedy, satire and delving into female status makes this a literary film. There is a lot of talk, sometimes good, but often edgy and too often pointless in lieu of a more robust visual dynamism and life." 

==Cast==
* Gérard Depardieu as Raoul
* Carole Laure as Solange
* Patrick Dewaere as Stéphane
* Michel Serrault as the neighbour
* Eléonore Hirt as Madame Belœil
* Jean Rougerie as Monsieur Belœil
* Sylvie Joly as the passer-by
* Riton (Liebman) as Christian Belœil
* Liliane Rovère as Marthe the barmaid ("Bernadette")
* Michel Beaune as the doctor in the street
* Roger Riffard as the doctor at the port
* André Thorent as the teacher
* André Lacombe as the councillor
* (Alain) David Gabison as the man
* Gilberte Géniat as the usherette
* Jean Perin as a worker
* Bertrand de Hautefort as an officer

==See also==
* List of submissions to the 51st Academy Awards for Best Foreign Language Film
* List of French submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 