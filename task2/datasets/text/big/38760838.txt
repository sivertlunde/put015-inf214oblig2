Ateşten Gömlek
{{Infobox film name           = Ateşten Gömlek image          =  caption        =  director       = Muhsin Ertuğrul producer       =   writer         =   starring       =   music          =   cinematography = Cezmi Ar editing        =  distributor    =  released       =   runtime        =  country        = Turkey language       = Turkish budget         =  gross          = 
}}
 Turkish drama film, co-produced by Kemal Seden and Şakir Seden, written and directed by Muhsin Ertuğrul based on a memoir of the same title (The Turkish Ordeal, published first in English) by Halide Edip Adıvar. The movie marks a milestone in the cinema of Turkey as  for the first time ever Turkish Muslim actresses, namely Bedia Muvahhit and Neyyire Neyir, featured in a movie.  It is in general about some events during the Turkish War of Independence (1919-1922).    Its remake with the same title was released in 1950, directed by Vedat Örfi Bengü. 

The movie was premiered on April 23, 1923, on the third anniversary of Grand National Assembly of Turkeys foundation at Palas Sinema in Beyoğlu, Istanbul. It was shown to audience in two separate ticketed screens.  

==Plot== Greek troops occupation of Sultanahmet Square. occupation of Turkish National enters Izmir as the first Turkish soldier. In order to draw Ayşes attention, Peyami intends also to be the first soldier in Izmir. Peyami is killed in action soon after. Ayşe, hearing the bad news, runs to the front, but she is also killed by enemy shrapnel shell.  

==First Turkish Muslim actresses==
The novel Ateşten Gömlek written by Halide Edip Adıvar (1884-1964), a womens rights activist, who actually participated in the Turkish War of Independence, serial in the newspaper İkdam between June 6 and August 11, 1922 before it was published in book form.   Since it became of great interest, its filming came into question.

In the Ottoman Empire, acting of Muslim women in movies was not allowed for reasons of religion. In all the movies, the woman roles were played by the Christian or Jewish women of minorities in Turkey. Adıvar stipulated that she would only permit her novel be filmed when the lead role is featured by a Turkish Muslim woman. 

Muhsin Ertuğrul (1892-1979) asked his friend Ahmet Refet Muvahhit whether his newly married wife Bedia Muvahhit (1897-1994) would be eligible for the lead role. She accepted to feature as Ayşe. For the supporting female role of Kezban in the movie, a newspaper advertisement was published. Only one woman, Münire Eyüp (1902-1943), applied. She played in the movie under the pseudonym Neyyire Neyir. She later married to Muhsin Ertuğrul. 

==Cast==
{{columns-list|2|
* Muhsin Ertuğrul as İhsan
* Emin Beliğ Belli as Peyami
* Bedia Muvahhit as Ayşe
* Neyyire Neyir as Kezban
* Refik Kemal Arduman
* Hakkı Necip Ağrıman 
* Vasfi Rıza Zobu
* Oğuz Yemen
* Behzat Haki
* Sait Köknar
* Behzat Butak
* Kınar Hanım
}}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 