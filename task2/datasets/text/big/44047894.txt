Valarthumrugangal
{{Infobox film 
| name           = Valarthumrugangal
| image          =
| caption        = Hariharan
| producer       = 
| writer         = MT Vasudevan Nair
| screenplay     = MT Vasudevan Nair
| starring       = Ratheesh Sukumaran Madhavi Thikkurissi Sukumaran Nair
| music          = MB Sreenivasan
| cinematography = Melli Irani
| editing        = G Venkittaraman
| studio         = Priyadarsini Movies
| distributor    = Priyadarsini Movies
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by Hariharan (director)|Hariharan. The film stars Ratheesh, Sukumaran, Madhavi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by MB Sreenivasan.    

==Cast==
 
*Ratheesh
*Sukumaran as Bhaskaran
*Madhavi as Janu
*Thikkurissi Sukumaran Nair
*VT Aravindakshamenon
*Vijayavani
*Balan K Nair Chithra
*G. GK Pillai
*K. P. Ummer
*Nagesh
*Nanditha Bose
*Oduvil Unnikrishnan
*P. K. Abraham
*Prathima
*Santo Krishnan
 

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by MT Vasudevan Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaakkaalan Kaliyachan || K. J. Yesudas || MT Vasudevan Nair || 
|-
| 2 || Karmathin Paathakal Veedhikal || K. J. Yesudas, Chorus || MT Vasudevan Nair || 
|-
| 3 || Oru Murikkannaadiyilonnu Nokki || S Janaki || MT Vasudevan Nair || 
|-
| 4 || Shubha Raathri || K. J. Yesudas || MT Vasudevan Nair || 
|}

==References==
{{reflist|refs=
   

}}

==External links==
*  

 
 
 

 