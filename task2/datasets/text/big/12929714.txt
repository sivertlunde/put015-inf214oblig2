Jupiter's Darling (film)
 
{{Infobox film
| name           = Jupiters Darling 
| image          = Jupiters-darling-1955.jpg
| image size     = 
| alt            = 
| caption        = Film poster
| director       = George Sidney George Wells
| writer         = Dorothy Kingsley
| starring       = Esther Williams Howard Keel Marge Champion Gower Champion George Sanders Richard Haydn David Rose
| cinematography = Charles Rosher Paul Vogel
| editing        = Ralph E. Winters
| studio         = Metro-Goldwyn-Mayer 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $3,337,000  . 
| gross          = $2,520,000 
}}
 musical romance MGM in Carthaginian military commander and George Sanders as Fabius Maximus, Amytiss fiance. In the film, Amytis helps Hannibal swim the Tiber River to take a closer look at Romes fortifications.

The film features many historical characters, including Roman generals Fabius Maximus and Scipio Africanus who appears briefly, in addition to Hannibal. Carthaginians Mago Barca and Maharbal also appear.

Jupiters Darling was based on Robert E. Sherwoods anti-war comedy play The Road to Rome (1927).   

The film was the last of three films Williams and Keel made together, the other two being Pagan Love Song (1950) and Texas Carnival (1951).

==Cast==
*Esther Williams as Amytis
*Howard Keel as Hannibal
*Marge Champion as Meta
*Gower Champion as Varius
*George Sanders as Fabius Maximus
*Richard Haydn as Horatio
*William Demarest as Mago
*Norma Varden as Fabia
*Douglass Dumbrille as Scipio
*Henry Corden as Carthalo
*Michael Ansara as Maharbal
*Martha Wentworth as Widow Titus
*John Olszewski as Principal Swimming Statue
*  Esther Williams singing voice dubbed by Jo Ann Greer

==Production==
  as Amytis in one of the films underwater sequences]] Chuck Walters Easy to Pal Joey.

During shooting, Williams broke her left eardrum, which had already been broken in five other films. She was fitted with a prosthesis from latex that covered her nose and ears that prevented water from rushing in. As a result, she could barely hear, taste or smell while wearing it. 

In one of the films scenes, Amytis, while fleeing from Hannibal and his soldiers, rides a horse over the edges of a cliff on the Tiber River. Williams refused to do the scene, and when the studio refused to cut it, the director called in a platform diver that Williams knew, Al Lewin. The stunt took place one time; the studio got its shot, and Lewin broke his back. 

==Release==
The films world premiere was held in Milwaukee, Wisconsin.  The cast, including a 350-pound baby elephant named Jupiters Darling, embarked on a tour of nine U.S. cities. 

Box office reception was poor - according to MGM records it made $1,493,000 in the US and Canada and $1,027,000 elsewhere resutling in a loss of $2,232,000.  Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 464 

===Critical reception===
A 1955 New York Times review of the film claimed that "Esther Williams must be getting bored with water. She goes swimming only three times in M-G-Ms "Jupiters Darling," which came yesterday to the Music Hall, and two of these times are forced upon her. She dunks only once for fun. And that, we might note, is the most attractive and buoyant thing in the film. It comes when Miss Williams, cast rashly as the fiancée of Emperor Fabius Maximus of Rome, peels off her stola and tunic after a long hot day in town and goes swimming in the pool of her villa, which is fancier than any pool in Hollywood." It also stated that "Miss Williams had better get back in that water and start blowing bubbles again." 

==Notes==
 

==See also==
*List of films set in ancient Rome

==References==
*{{cite book
|last=Williams 
|first=Esther 
|year=1999 
|title=The Million Dollar Mermaid: An Autobiography 
|edition=1st 
|url=http://books.google.com/books?id=qItZAAAAMAAJ&q=the+million+dollar+mermaid&dq=the+million+dollar+mermaid&cd=1 
|accessdate=2009-12-11 
|isbn=978-0-15-601135-8
|id=ISBN 0-15-601135-2 }}

==Further reading==
* 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 