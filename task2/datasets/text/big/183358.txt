SLC Punk!
{{Infobox film
| name = SLC Punk!
| image = SLC Punk.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = James Merendino
| producer = Sam Maydew Peter Ward Executive: Jan de Bont Michael Peyser Andrea Kreuzhage
| writer = James Merendino
| narrator = Matthew Lillard Christopher McDonald Devon Sawa see below
| cinematography = Greg Littlewood
| editing = Esther P. Russell
| studio = Beyond Films Blue Tulip Productions Straight Edge
| distributor = Sony Pictures Classics
| released =  
| runtime = 98 minutes  
| country = United States
| language = English
| budget = 
| gross = $299,569
}}
SLC Punk! is a 1998 American comedy-drama film written and directed by James Merendino. The film is about the young punk rock fan Steven "Stevo" Levy, a college graduate living in Salt Lake City. The character is portrayed as a stereotype of an anarchist punk in the mid-1980s. Many events and characters in the movie are allegedly based on real life, although they may have been exaggerated.
 Stephen Egerton, Descendents and All (band)|ALL.  SLC Punk was chosen as the opening-night feature at the 1999 Sundance Film Festival. 

Merendino created the film based on his experience growing up in Salt Lake City. Although the film is not autobiographical, Merendino has said that many characters were based on people he knew. 

==Plot== afraid of needles and actually believes that any drug (with the notable exception of alcohol and cigarettes) is inherently dangerous.

Stevo and Bob go from party to party while living in a dilapidated apartment. They spend much of their time fighting with members of other subcultures, particularly rednecks. Stevo has a casual relationship with a girl named Sandy, while Heroin Bob is in love with Trish, the manager of a head shop, however he is reluctant to ask her to become his girlfriend.

The two of them are shaped by their experiences with their parents. Stevos parents, now divorced, are former hippies who are proud of their youthful endeavors; however, Stevo is revolted by what he perceives as their "selling out" by becoming affluent Reagan Republicans, which they lamely try to justify. Stevos grades are excellent, and when his father—a lawyer with a Porsche and a penchant for younger women—sends an application to Harvard Law School and Stevo is accepted, he nevertheless rejects it because of his beliefs. By contrast, Bobs father is a paranoid, drunken wreck who mistakes his son and his friend for Central Intelligence Agency operatives, and chases them away with a shotgun when they visit him on his birthday.
 panhandling on the streets with some obvious mental issues.

While Stevo understands that his relationship with Sandy is casual, hes still enraged when he discovers her having sex with another man, and savagely beats him, later loathing himself because his action contradicts his own belief in anarchism. His social circle begins to drift away, as Mike leaves Salt Lake City to attend the University of Notre Dame. Stevo falls in love with a young rich girl named Brandy (Summer Phoenix), who points out that his anarchistic clothing and attitude are more of a fashion choice than an actual political philosophy. Rather than being offended, Stevo takes the criticism thoughtfully and they passionately kiss.

At the same party, Heroin Bob complains of a headache (induced by Spandau Ballets "She Loved Like Diamond" playing on a stereo), and is given Percodan, which he consumes after being told the pills are simply "vitamins" that will help his headache. The accidental drug overdose kills him in his sleep, seemingly justifying the aversion to chemicals he previously espoused in a diatribe delivered to Stevo. When Stevo discovers that his best friend is dead, he breaks down completely. At the funeral, he appears with a shaved head and changed clothing, and decides that hes done with his punk lifestyle. He decides to go to Harvard, and suggests in the narration that he marries Brandy and she will be the mother of his children. He notes in his closing narration that his youthful self would probably kick his future selfs ass, wryly describing himself as ultimately just another poseur.

===The "Tribes"===
The film features several cliques presented as "tribes." The film focuses primarily on the punk tribe, but includes several others as well: Nazis and Redneck (stereotype)|rednecks.
* Mod revival|Mods: Mods wear suits and ties, and they ride scooters. Theyre generally the rivals of the punks, but the character John the Mod acts as a diplomat who freely moves between the tribes. In the beginning of the movie, the mods are trying to buy acid from Sean.
* Rednecks: Rednecks are rural Utah folk who wear trucker caps and flannel, and drive around in big trucks. Punks hate them for their conservative values.
*  s who wear pseudo-military fatigues and Nazi armbands. Punks and mods are shown to be predatory towards the Nazis. Heavy metal Guys: They have long hair and flannel. Not much else is known about them, except that Stevo explains that they are predatory toward the New Wavers.
*  s and are said to be the least threatening of the tribes. They are described as being "the new hippies." Every Tribe is predatory to the New Wavers.
* The Teddy Boys: Though it is not mentioned in the film itself, Eddy belongs to the Teddy Boy scene.

==Cast==
* Matthew Lillard as Steven "Stevo" Levy
** Christopher Ogden as young Stevo
* Michael A. Goorjian as "Heroin" Bob
** Francis Capra as young Bob
* Jason Segel as Mike
* Annabeth Gish as Trish
* Jennifer Lien as Sandy Christopher McDonald as Mr. Levy
* Devon Sawa as Sean the Beggar
* Adam Pascal as Eddie
* Til Schweiger as Mark Jimmy Duval as John the Mod
* Summer Phoenix as Brandy

==Production notes==
The film was shot in an aggressive, highly kinetic style, with sweeping crane shots, fast dolly moves, and jump cuts.

Most of the film was shot on location in Salt Lake City, with a scene taking place in Evanston, Wyoming. Numerous scenes took place in locally well-known areas: West High School near downtown Salt Lake City.
* The scene wherein Heroin Bob chastises Stevo for using acid takes place at Presidents Circle at the University of Utah. Mall in Holladay, Utah.  Seans "womens clothing" job interview takes place inside a Cottonwood Mall storefront. acid at Memory Grove Park, a World War I memorial park.
* Many exterior street scenes occur just north of the Frank E. Moss Federal Courthouse in the downtown area.
* The ECP concert was shot at the SLC Indian Center.
* The scenes depicting Heroin Bobs funeral were shot inside and outside The Cathedral of the Madeleine. The cathedral is located just east of downtown Salt Lake City.
* The apartment where Stevo and Heroin Bob live was the Big D Construction building, across from Pioneer Park.
* The store where they bought the "Wyoming Beer" is Porters Fireworks and Liquor on the outskirts of Evanston.

== In other media ==

=== Comics ===
Writer-director James Merendino produced a comic book adaptation of the film in 1999, illustrated by Dean Haspiel and published by Straight Edge Productions/Lulu Publishing.

==Soundtrack==
{{Infobox album
| Name = SLC Punk!
| Type = soundtrack
| Artist = Various
| Released = March 16, 1999
| Genre = Punk rock
| Label = Hollywood Records
| Cover =
}}
 I Never Promised You a Rose Garden" - The Suicide Machines (originally performed by Lynn Anderson)
# "Sex and Violence" - The Exploited Fear
# "1969" - The Stooges
# "Too Hot" - The Specials
# "Cretin Hop" - Ramones Blondie
# "Gangsters" - The Specials Generation X Rock N Roll" - The Velvet Underground
# "Gasoline Rain" - Moondogg Mirror in the Bathroom" - Fifi (originally performed by The English Beat)
# "Amoeba" - The Adolescents
# "Kill the Poor" - Dead Kennedys
# "Look Back and Laugh" - Minor Threat

Eight Bucks Experiment, the band portraying fictional English band ECP, were featured on a European release of the soundtrack.  The three songs they recorded live for the punk concert scene were sent back to the band after filming. They self-released the songs on the One Of These Days EP through their Blue Moon Recordings label website. 

==Sequel==
 
In April 2013, director James Merendino announced that a sequel to SLC Punk! titled Punks Dead will begin filming later in the year and will be released in 2014 with most of the original cast reprising their roles.  The film was successfully funded by an Indiegogo campaign launched on October 27, 2013, and completed on January 15, 2014. Merendino said of the sequel, “I made SLC Punk! when I was a kid, and accordingly, the story is naive, and, as just a coming of age story, not finished. The characters are facing big questions, 18 years later, as outsiders, punk rockers… What relevance do they have in a world where all statements have already been made? In the years since I made SLC Punk!, it has found a rather large and supportive following who have been very kind to me. So in making a sequel, I feel I owe it to those people to really do it right."  

==References==
 

==External links==
 
*  
*  
*  
*  }

 
 
 
 
 
 
 
 
 
 
 
 
 
 