Rushmore (film)
{{Infobox film
| name           = Rushmore
| image          = Rushmoreposter.png
| caption        = Theatrical release poster
| director       = Wes Anderson
| producer       = Barry Mendel Paul Schiff
| writer         = Wes Anderson Owen Wilson Brian Cox Mason Gamble Sara Tanaka Stephen McCole Connie Nielsen Luke Wilson
| music          = Mark Mothersbaugh
| cinematography = Robert Yeoman
| editing        = David Moritz
| studio         = Touchstone Pictures American Empirical Pictures Buena Vista Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $20 million   
| gross          = $17,195,500  
}}
Rushmore is a 1998 comedy-drama film directed by Wes Anderson about an eccentric teenager named Max Fischer (Jason Schwartzman in his first film), his friendship with rich industrialist Herman Blume (Bill Murray), and their mutual love for elementary school teacher Rosemary Cross (Olivia Williams). The film was co-written by Anderson and Owen Wilson. The soundtrack was scored by regular Anderson collaborator Mark Mothersbaugh and features several songs by bands associated with the British Invasion of the 1960s.
 independent cinema. Rushmore also won Best Director and Best Supporting Male awards at the 1999 Independent Spirit Awards while Murray earned a Golden Globe nomination for Best Performance by an Actor in a Supporting Role in a Motion Picture.
 Empire also named Rushmore the 175th greatest film of all time in 2008. Four years after, Slant Magazine ranked the film #22 on its list of the 100 Best Films of the 1990s.  According to Shortlist (magazine)|ShortList, it is one of the 30 coolest films ever. 

==Plot==
Max Fischer, a mature and eccentric 15-year-old, is both Rushmores most extracurricular and least scholarly student. Herman Blume is a cynical industrialist who comes to admire Max. Rosemary Cross is a widowed first grade teacher who becomes the object of both Maxs and Hermans affection.

Maxs life revolves around Rushmore Academy, a private school in Houston, where he is a scholarship student. He spends nearly all of his time on elaborate extracurricular activities, caring little how it affects his grades. He also feuds with the schools headmaster, Dr. Guggenheim.

Blume finds his operation of a multimillion dollar company to be unsatisfying. He is frustrated that his marriage is failing and the two sons hes putting through Rushmore are boorish and unrepentant brats spoiled by their mother. He and Max become close friends; Max admires Hermans success while Herman is impressed by Maxs cocksure attitude.

Rosemary arrives at the academy as a new teacher after the death of her husband (and former Rushmore student), and Max quickly develops an infatuation. He makes many attempts at courting her. While she initially tolerates Max, Rosemary becomes increasingly alarmed at his obvious obsession with her. Along the way Blume attempts to convince Max that Rosemary is not worth the trouble, only to fall for Rosemary himself. They begin dating without Maxs knowledge.

After Max attempts to break ground on an aquarium without the schools approval, he is expelled from Rushmore. He is then forced to enroll in his first public school, Grover Cleveland High. Attempts to engage in outside activities at his new school have mixed results. A fellow student, Margaret Yang, tries to engage Max, but he pays little attention to her. Rosemary and Blume attempt to support him in his new school.

Eventually, Maxs friend Dirk discovers the relationship between Rosemary and Blume and informs Max as payback for a rumor Max started about his mother. Max and Blume go from being friends to mortal enemies, and they engage in back-and-forth acts of revenge. Max informs Blumes wife of her husbands affair, thus ending their marriage. Max then puts bees in Blumes hotel room, then Blume destroys Maxs bicycle with his car. Max cuts the brake lines on Blumes car, for which he is arrested.

Max eventually gives up and explains to Blume that revenge no longer matters because even if he (Max) wins, Rosemary still would love Blume. Max becomes depressed and stops attending school. He cuts himself off from the world and works as an apprentice at his fathers barber shop.

One day, Dirk stops by the shop to apologize to Max and bring him a Christmas present. Dirk suggests Max see his old headmaster in the hospital, knowing Blume will be there. Max and Blume meet and are cordial, and Max finds out that Rosemary broke up with Blume. He also manages to bring Dr. Guggenheim out of his coma. Max begins to apply himself in school again. He also develops a friendship with Margaret Yang, whom he casts in one of his plays.

Max takes his final shot at Rosemary by pretending to be injured in car accident, soliciting her affection. When she discovers that Maxs injuries are fake, he is rebuffed again. Max makes it his new mission to win Rosemary back for Blume. His first attempt is unsuccessful, but then he invites both Herman and Rosemary to the performance of a play he wrote, making sure they will be sitting together. In the end, she and Blume appear to reconcile. Max and Margaret Yang also become a couple.

Max and Rosemary look at each other enigmatically as they share a dance at the plays wrap party.

==Cast==
 
* Jason Schwartzman as Max Fischer
* Bill Murray as Herman Blume
* Olivia Williams as Rosemary Cross
* Seymour Cassel as Bert Fischer Brian Cox as Dr. Nelson Guggenheim
* Mason Gamble as Dirk Calloway
* Sara Tanaka as Margaret Yang
* Connie Nielsen as Mrs. Calloway
* Luke Wilson as Dr. Peter Flynn
* Stephen McCole as Magnus Buchan
* Kumar Pallana as Mr. Littlejeans

==Production== Walt Disney Studios. He offered them a $10 million budget. 

===Casting===
Anderson and Wilson wrote the role of Mr. Blume with Bill Murray in mind, but doubted they could get the script to him.    Murrays agent was a fan of Andersons first film, Bottle Rocket, and urged the actor to read the script for Rushmore. Murray liked it so much that he agreed to work for Screen Actors Guild#Standardized pay and work conditions|scale,    which Anderson estimated to be around $9,000.    The actor was drawn to Anderson and Wilsons "precise" writing and felt that a lot of the film was about "the struggle to retain civility and kindness in the face of extraordinary pain. And Ive felt a lot of that in my life".    Anderson created detailed storyboards for each scene but was open to Murrays knack for improvisation. 
 Britain for the role of Max Fischer before finding Jason Schwartzman.  In October 1997, approximately a month before principal photography was to begin, a casting director for the film met the 17-year-old actor at a party thanks to his cousin and filmmaker Sofia Coppola.    He came to his audition wearing a prep-school blazer and a Rushmore patch he had made himself.  Anderson almost did not make the film when he could not find an actor to play Max but felt that Schwartzman "could retain audience loyalty despite doing all the crummy things Max had to do".  Anderson originally pictured Max, physically, as Mick Jagger at age 15,  to be played by an actor like Noah Taylor in the Australian film Flirting (film)|Flirting - "a pale, skinny kid".    When Anderson met Schwartzman, he reminded Anderson much more of Dustin Hoffman and decided to go that way with the character.  Anderson and the actor spent weeks together talking about the character, working on hand gestures and body language. 

===Principal photography===
Filming began in November 1997.  On the first day of principal photography, Anderson delivered his directions to Murray in a whisper so that he would not be embarrassed if the actor shot him down. However, the actor publicly deferred to Anderson, hauled equipment, and when Disney denied the director a $75,000 shot of Max and Mr. Blume riding in a helicopter, Murray gave Anderson a blank check to cover the cost, although ultimately the scene was never shot. 
 Lamar High School in Houston was used to depict Grover Cleveland High School, the public school. In real life, the two schools are directly next to each other.  Richard Connelly of the Houston Press said that the Lamar building "was ghettod up to look like a dilapidated inner-city school."  Many scenes were also filmed at North Shore High School. The films widescreen, slightly theatrical look was influenced by Roman Polanskis Chinatown (1974 film)|Chinatown.  Anderson also cites The Graduate and Harold and Maude as cinematic influences on Rushmore.   

==Soundtrack==
  Ooh La La", its an essential soundtrack".   

==Release==
Rushmore had its world premiere at the 25th Telluride Film Festival in 1998 where it was one of the few studio films to be screened and be well received by critics and audiences.    The film was also screened at the 1998 New York Film Festival and the Toronto Film Festival where it was a hit with critics.       The film opened in New York City and Los Angeles for one week in December in order to be eligible for the Academy Awards. 

===Box office=== USD $43,666, selling out 18 of 31 showings.  The film opened in wide release in 764 theaters on February 19, 1999, grossing $2.8 million in its first weekend. It went on to make $17.1 million, below its $20 million budget. 

===Critical response===
The film was well received by critics, earning mostly positive reviews. It has an 89% "certified fresh" rating on Rotten Tomatoes based on 102 reviews with an average rating of 8.1 out of 10 with the consensus "This cult favorite is a quirky coming of age story, with fine, off-kilter performances from Jason Schwartzman and Bill Murray."  The film also has a score of 86 on Metacritic based on 31 reviews. 

In his review for the Daily News, film critic   gave the film three out of four stars and wrote that Bill Murray was "at his off-kilter best".    Todd McCarthy, in his review for  , Richard Schickel praised Rushmore as "an often deft, frequently droll little movie turns into an increasingly desperate juggling act, first trying to keep too many dark and weighty emotional objects aloft, then trying to bring them back to hand in a graceful and satisfying way".   

In her review for New York Times, Janet Maslin wrote, "Its a particular treat for its skewed, hilarious memories of a cutthroat boyhood".    In his review for The Independent, Anthony Quinn said of Schwartzman that "he perfectly captures the poignancy of a character who understands his failings but hasnt yet the emotional resources to conquer them".    In her review for the Washington Post, Rita Kempley praised Schwartzmans performance for winning "sympathy and a great deal of affection for Max, never mind that he could grow into Sidney Blumenthal".    Entertainment Weekly gave Rushmore an "A" rating and wrote, "Anderson concentrates on beautifully disciplined filmmaking, employing 1960s British Invasion hits . . . to further define Maxs adolescent dislocation".    Jonathan Rosenbaum, in his review for the Chicago Reader, wrote, "To their credit, Anderson and Wilson share none of the class snobbery that subtly infuses much of J.D. Salinger|Salingers work ... But like Salinger they harbor a protective gallantry toward their characters that becomes the films greatest strength and its greatest weakness".   

A lifelong fan of film critic Pauline Kael, Anderson arranged a private screening of Rushmore for the retired writer. Afterwards, she told him, "I genuinely dont know what to make of this movie".    It was a nerve-wracking experience for Anderson but Kael did like the film and told others to see it.  Anderson and Jason Schwartzman traveled from Los Angeles to New York City and back on a touring bus to promote the film.  The tour started on January 21, 1999 and went through 11 cities in the United States. 

In   — and then some. Thanks to stellar performances and the directors original comic vision, its easily one of the years finest films. ... Rushmore is somewhat indebted to Harold and Maude and similar comedies of that era, but the complexity of Max and the audacity of the films set pieces place it in a league of its own." 

===Awards and nominations===
Rushmore was nominated for two Independent Spirit Awards - Wes Anderson for Best Director and Bill Murray for Best Supporting Actor with the actor winning.       Murray was also nominated in the Best Supporting Actor category for the Golden Globes. 
 Best Supporting Best Supporting Actor of the year as did the New York Film Critics.     Film critic David Ansen ranked Rushmore the 10th best film of 1998.    Spin (magazine)|Spin hailed the film as "the best comedy of the year". 

Rushmore is number 34 on Bravo (US TV channel)|Bravos "100 Funniest Movies". The film was also ranked #20 on Entertainment Weekly magazines "The Cult 25: The Essential Left-Field Movie Hits Since 83" list    and ranked it #10 on their Top 25 Modern Romances list.   

Rushmore was one of the 400 nominated films for the American Film Institute list AFIs 100 Years...100 Movies (10th Anniversary Edition) 

==Home media==
Buena Vista Home Entertainment released an edition of the film on June 29, 1999 with no supplemental material. This was followed by a special edition on January 18, 2000 by The Criterion Collection with remastered picture and sound along with various bonus features, including an audio commentary by Wes Anderson, Owen Wilson and Jason Schwartzman, a behind-the-scenes documentary by Eric Chase Anderson, Anderson and Murray being interviewed on The Charlie Rose Show, and theatrical "adaptations" of Armageddon (1998 film)|Armageddon, The Truman Show and Out of Sight, staged specially for the 1999 MTV Movie Awards by the Max Fischer Players.

A Criterion Collection Blu-ray was released on November 22, 2011.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 