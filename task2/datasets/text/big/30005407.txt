Carver (film)
{{Infobox film
| name           = Carver
| image          =
| image_size     = 
| caption        = US film poster
| director       = Franklin Guerrero Jr.
| producer       = Franklin Guerrero Jr. Eric Williford
| writer         = Franklin Guerrero Jr.
| starring       = Natasha Malinsky Erik Fones  Matt Carmody Neil Kubath David G. Holland Jonathan Rockett Kristyn Green
| music          = Paul E. Jessen Matt Baxter
| cinematography = Ryan Bedell
| editing        = Shochun Ampekennerali Allumination Filmworks
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Carver is the second horror film directed by Franklin Guerrero Jr., after The 8th Plague. Made in 2008, it stars Matt Carmody, Neil Kubath, and Erik Fones.  

==Plot==
A group of young campers take a detour through the mountain town of Halcyon Ridge, and stop at a bar where the owner asks them for help getting supplies from their storehouse in the woods. When they get there, they discover bizarre snuff films which, unbeknownst to them, are real. They soon become the targets of two homicidal brothers with an insatiable bloodlust.   

== References ==
 

== External links ==
*  
*  


 
 
 
 
 


 