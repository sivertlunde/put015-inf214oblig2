Tesis
 
{{Infobox film
| name           = Tesis
| image          = Tesis1996.jpg
| caption        = DVD cover
| director       = Alejandro Amenábar
| producer       = Emiliano Otegui José Luis Cuerda
| writer         = Alejandro Amenábar, Mateo Gil Eduardo Noriega Ana Torrent
| music          = Alejandro Amenábar
| cinematography = Hans Burman
| editing        = María Elena Sáinz de Rozas
| distributor    =
| released       = 1995
| runtime        = 125 min, (Mexico: 118 min)
| country        = Spain Spanish
| budget         = Spanish peseta|Pts116,000,000
| gross          =
}}
 1996 Cinema Spanish film. Eduardo Noriega.

== Plot ==
Ángela is writing a thesis on audiovisual violence with the help of her thesis director. After finding a movie on the topic and watching it, he turns up dead.

Ángela will let her classmate Chema take part on her personal investigation. Both of them decide to take the movie home and watch it. The drama will start when they discover it is a  Snuff film|snuff-movie in which a girl is tortured and killed. The doors of a new audiovisual world open up in front of them and along with this new opportunity, arrives the risk of becoming the main character of the next snuff-movie.

==Production notes== Pts116 million - equal to about Euro|€696,000.

==External links==
* 
*  
*   

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 

 