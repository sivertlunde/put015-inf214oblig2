Joe Butterfly
{{Infobox film
| name = Joe Butterfly
| other name =
| image =
| director = Jesse Hibbs
| producer = Aaron Rosenberg
| writer = Sy Gomberg Jack Sher Marion Hargrove
| based on = play by Evan Wylie and Jack Ruge
| starring = Audie Murphy Burgess Meredith George Nader
| music = Joseph Gershenson (supervision)
| cinematography = Irving Glassberg
| editing = Milton Carruth
| distributor = Universal Studios#Universal-International|Universal-International
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| gross = $1.3 million (US rentals) 
}} Teahouse of the August Moon, released seven months earlier. 

==Plot== con artist Joe Butterfly. Butterfly shows them the high life letting them live in a mansion complete with beautiful girls.

==Cast==
 
 
* Audie Murphy as Private John Woodley
* George Nader as Sergeant Ed Kennedy
* Keenan Wynn as Henry Hathaway
* Keiko Shima as Chieko
* Fred Clark as Colonel E. E. Fuller
* John Agar as Sergeant Dick Mason
* Charles McGraw as Sergeant Jim McNulty
* Shinpei Shimazaki as a little boy
* Reiko Higa as False Tokyo Rose
 
* Tatsuo Saito as father
* Chizu Shimazaki as mother
* Herbert Anderson as Major Ferguson
* Eddie Firestone as Sergeant Oscar Hulick
* Frank Chase as Chief Yeoman Saul Bernheim Harold Goodwin as Colonel Hopper
* Willard Willingham as a soldier
* Burgess Meredith as Joe Butterfly
 

==Production==
The movie was shot partly in Hong Kong and Japan. According to co-writer Sy Gomberg, Audie Murphy was extremely uncomfortable playing comedy. However, the movie was an enormous hit in Japan, in part because of their admiration of Murphy, and partly because of its sympathetic depiction of the Japanese. Don Graham, No Name on the Bullet: The Biography of Audie Murphy, Penguin, 1989 p 266-267  Following the film, Murphy brought home a 14-year-old Japanese girl who stayed with the Murphys and helped raise their children while she attended school in America.  
 

==References==
 

==External links==
* 
*  at TCMDB

 

 
 
 


 