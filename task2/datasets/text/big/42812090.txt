Flim Flam Films
{{Infobox Hollywood cartoon|
| cartoon_name = Flim Flam Films
| series = Felix the Cat
| image = 
| caption =  Pat Sullivan
| story_artist = 
| animator = Otto Messmer
| voice_actor = 
| musician = 
| producer = Jacques Kopfstein
| studio = Pat Sullivan Studios
| distributor = Bijou Films, Inc. Educational Pictures Corporation
| release_date = September 18, 1927
| color_process = B&W
| runtime = 7 min English
| preceded_by = Felix in Wise Guise
| followed_by = Felix Switches Witches
}}

Flim Flam Films is a silent animated short subject featuring Felix the Cat.

==Plot==
Felix tries to put three kittens to bed as part of their nap time. He then goes to the living room to chat with his iceweasel buddy. It appears the little cats are not sleepy as they cry in boredom. While Felix wonders what he should do, his friend offers a suggestion of taking the kittens to a cinema.

Felix and the kittens are off to a cinema in town. He tries to purchase tickets at a booth but it appears cats are not permitted in the theater as the seller shoos them away. They then disguise themselves as a man, and manage to buy a ticket without the seller realizing. Their disguise, however, does not keep them covered for long when a guard at the cinema entrance spots some bizarreness on top. Feeling completely exposed, they split up and flee. The four cats are still desperate in wanting watch as they sneak through a tiny opening in the cinemas walls. Finally inside, they get to see a film, and the one starring in it turns out to be Felix himself, much to the kittens enjoyment. But when the Felix character in the film gets attacked by a lion, the angry kittens rush forth and assault the screen, therefore causing a commotion from the audience.

On a field just outside of town, the kittens are weeping about never getting to watch something, and Felix is still wondering what he should do next. In no time Felix comes up with an idea, and tells the little cats that they should create their own film.

Hours later, Felix and the kittens return to the field, bringing with them a crank-operated movie camera. Using the device, Felix takes videos of a ballet dancer, a marching band, a rising hot air balloon, and a female swimmer in the lake whom Felix romances with. The kittens also take some shots too.

They then return home that night to watch the film they made. The characters in the film, however, appear upside down but Felix is able to make quick adjustments. It appears the female swimmer whom Felix dated is the iceweasels girlfriend, much to the annoyance of that guy whos in attendance. The iceweasel mercilessly batters Felix. The kittens are too frightened to intervene, knowing the iceweasel is more powerful than the lion in the cinema film. Felix is left covered in a lot bandages, an image he would display again in Whys and Other Whys.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 


 