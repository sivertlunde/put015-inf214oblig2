North Star (1996 film)
{{Infobox film
 | name           = North Star
 | image          = NorthStar1996.jpg
 | image_size     = 
 | border         = 
 | alt            = 
 | caption        = 
 | director       = Nils Gaup
 | producer       = Christopher Lambert
 | writer         = {{Plainlist|
*Sergio Donati
*Lorenzo Donati
*Paul Ohl
}}
 | based on       =  
 | starring       = {{Plainlist|
* James Caan
* Christopher Lambert
* Catherine McCormack
* Burt Young
* Morten Faldaas
* Reidar Sørensen
* Nicholas Hope
* Frank Salsedo
* Sverre Anker Ousdal
* Frank Krog
}}
 | music          = {{Plainlist|
* Bruce Rowland John Scott
}}
 | cinematography = Bruno de Keyzer	
 | editing        = {{Plainlist|
* Michael A. Hoey
* Kant Pan
}}
 | studio         = 
 | distributor    = {{Plainlist|
* Warner Brothers
* Regency Enterprises
}}
 | released       =  }}
 | runtime        = 90 minutes
 | country        = 
 | language       = English
 | budget         = 
 | gross          = 
}}
 1996 Action film|action-Western film starring James Caan, Christopher Lambert and Catherine McCormack. Directed by Nils Gaup, it was written by Sergio Donati and loosely based on Henry Wilson Allens 1956 Western novel The North Star. Lambert executive produced the film.

==Plot summary==
 American land baron, owns the majority of  Nome and placer claims. He uses power, influence and 
nationalism to make local politics and law enforcement work in his favor. Unbeknownst to those outside of McLennons gang, the original owners of the land were all killed on McLennons orders.

The gangs latest target, Hudson Santeek (Lambert) narrowly escapes the attempt on his life but not before witnessing McLennons right-hand thug, Reno
(Burt Young) murder Santeeks adoptive father - an elderly Athabaskan tribal chief - in cold blood. Santeek, who is half white and half American Indian, had "purchased" his surrogate 
familys land in a bid to keep miners away.
 Irish immigrant trophy wife, intervenes at gunpoint;
McLennon disarms Santeek and the two engage in a fight. After Santeek floors McLennon, he kidnaps Sarah, steals a dogsled and rides off into the posse and treks out into the tundra to kill Santeek and rescue his wife.

On the course of the journey, Santeek talks to his captive about her husbands murderous property deed scams, but she does not immediately believe him and 
re-avows her loyalty to her husband. However, when McLennon and his men locate Santeeks cabin hideout and shoot at it indiscriminately, Sarah pleas for her husband not 
to kill her abductor. McLennon notices that she is wearing a shirt made of animal skin that Santeek had put her earlier to keep her warm when she was cold. He erroneously assumes that she betrayed him and had an affair with Santeek. Calling her a "whore," he disowns her and orders his men to 
continue firing at Santeeks cabin.

Santeek escapes while his pet wolf fatally lunges at the throat of one of the posse gunmen. Santeeks wolf then attacks the gangs sled dogs, but Smiley shoots and kills the animal. With no clear objective and unfamiliarity with the freezing, unpredictable terrain, members 
of McLennons posse begin to turn on one another, but McLennon kills the burnouts in his gang and sets out to finish the job he started.

Meanwhile, back in Nome, Col. Henry Johnson (Jacques Francois) and the U.S. Marshals begin their own investigation of McLennons land seizures, noticing that all of the original trustees are 
mysteriously deceased and that Santeek (the most recent Disseisor|disseisee) was witnessed in town alive. On the basis of this discrepancy, the federals declare Martial Law on Nome and accompany
the towns top lawman, Sheriff Lamont (Nicholas Hope) to bring McLennon and Santeek back for questioning. The enforcers locate McLennon and his gang,
but McLennon refuses to obey their orders and the posse shoot Lamont and the other officials to death.

McLennon and the surviving members of his gang eventually track and corner Santeek in a glacier cave. While Santeek scuffles with Reno, McLennon tips over a wood totem and Reno and Santeek slide down and fall through the sheath of a frozen lake. Reno drowns during the struggle, while Santeek, with his naturalist survival skills, makes it out of the icy water alive.
 town speech, reaffirming his mining policies to the chagrin of immigrant prospectors and the delight of his cronies. However, Johnson and the U.S. Marshals interrupt the huddle and arrest McLennon for the murder of sheriff Lamont and the other constables. Sarah, McLennons disillusioned wife, had witnessed the massacre and ultimately informed on him.

Soon after, Santeek returns to town to personally settle scores with the incarcerated McLennon, but finds the former had escaped from his cell and slit the throats of the jailguards in the process. McLennon breaks into his loft apartment (above the saloon which he had owned) and attempts to kill Sarah for turning on him, but Santeek appears behind McLennon and plunges a dagger into his back.

==Cast==
*James Caan as Sean McLennon
*Christopher Lambert as Hudson Saanteek
*Catherine McCormack as Sarah
*Burt Young as Reno
*Morten Faldaas as Smiley
*Jacques François as Col. Henry Johnson
*Mary M. Walker as Haina
*Renny Hoalona Loren as Cheelik
*Frank Salsedo as Nakki
*Reidar Sørensen as Bjorn Svenson
*Hilde Grythe as Hannah Svenson
*Sverre Anker Ousdal as Lindberg
*Nicholas Hope as Sheriff Lamont
*Frank Krog as Big Tim
*Norman Charles as Tonga

==Production==
Although set in the U.S. state of Alaska, the movie was shot entirely in Norway in Maridalen, Møsvatn, and Orre bach. 

==Reception==
The film currently holds a 4.7 on imdb.com. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 