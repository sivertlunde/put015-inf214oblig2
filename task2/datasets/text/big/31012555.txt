Hankyū Densha
 
{{Infobox film
| name           = Hankyu Densha
| image          = Hankyu Densha-p1.jpg
| alt            = 
| caption        = movie poster
| director       = Yoshishige Miyake
| producer       =  Yoshikazu Okada
| based on       = "Hankyu Densha" by Hiro Arikawa
| starring       = Miki Nakatani
| music          = 
| cinematography = 
| editing        = 
| studio         = Cocoon Co., Ltd.
| distributor    = Toho
| released       =    
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
}}
  is a 2011 Japanese film. It is based on the novel Hankyū Densha by Hiro Arikawa    and directed by Yoshishige Miyake. It was released in cinemas in the Kansai region of Japan on April 23, 2011, and then in other regions of the country from April 29.

The stage for the story is the commuter railway Hankyū Imazu Line in Hyōgo Prefecture, Japan.   

==Cast==
* Miki Nakatani as Shoko Takase, an office lady in her 30s. She was engaged to her boyfriend when she entered the company, but he was stolen away by a younger co-worker.   
* Erika Toda as Misa Morioka, a college student with a good-for-nothing boyfriend. 
* Kaho Minami as Yasue Ito
* Tetsuji Tamayama as Ryuta Toyama
* Nobuko Miyamoto as Tokie Hagiwara, a traditional old woman and the grandmother of Ami.  The young Tokie was played by Mei Kurokawa.
* Kasumi Arimura as Etsuko Kadota
* Mana Ashida as Ami Hagiwara, Tokies suffering granddaughter.    Ashida was selected for this role because she is from Nishinomiya, Hyogo, and is able to speak naturally in the Kansai dialect. 
* Mikahoa Mina as a housewife who is part of a circle of women who have a rich husband. 
* Ryo Katsuji as Keiichi Kosaka
* Yū Koyanagi as Katsuya
* Suzuka Morita as Etsukos friend		
* Mitsuki Tanimura as Miho Gondawara
* Saki Aibu as Mayumi
* Tsutomu Takahashi	as Kengo

=="Hankyu Densha" production companies==
*Kansai Telecasting Corporation
*Dentsu
*Gentosha Hankyu Corporation
*Pony Canyon
*Yomiuri Shimbun
*Yomiuri Telecasting Corporation

==Sponsors==
*Tasaki & Co., Ltd.
*Hitachi, Ltd.
*KDDI
*Kwansei Gakuin University
*Hankyu Realty Co., Ltd.
*Taisho Pharmaceutical Co., Ltd.
*Kobeya Baking Group
*PURELA
*Hankyu Hanshin Daiichi Hotel Group

==Film Festival==
This film was featured in the 3rd Okinawa International Movie Festival in the "Peace" category.   

==References==
 

==External links==
*  
*  

 
 
 
 

 

 