Soorappa
{{Infobox film name           = Soorappa image          = image_size     = caption        = director       = B. Naganna producer       = I D Kamalakar M B Babu M S Suresh Babu R Jagadish writer  Bharathi
|narrator       = starring  Vishnuvardhan Shruti Shruti Charan Raj music          = Hamsalekha cinematography = Ramesh Babu editing        = Suresh Urs studio         = Super Hit Films released       =   runtime        = 158 minutes country        = India language  Kannada
|budget         =
}}
 Kannada drama Vishnuvardhan in Shruti and Anu Prabhakar in the prominent roles.  The film had a musical score and soundtrack composed and written by Hamsalekha.
 Tamil blockbuster Bharathi and starred Mammootty and Devayani (actress)|Devayani. 

The film released to generally positive reviews from critics and was one of the biggest hits of the year 2000. 

== Cast == Vishnuvardhan as Soorappa Shruti 
* Charan Raj
* Anu Prabhakar
* Chi Guru Dutt
* Bank Janardhan
* M.N Lakshmi Devi
* Ramesh Bhat
* Tennis Krishna
* Mandeep Roy
* Krishne Gowda

== Soundtrack ==
The music of the film was composed and written by Hamsalekha.

{{Infobox album  
| Name        = Soorappa
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Magnasound
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Ee Mannige Naa Chiraruni
| lyrics1 	= Hamsalekha
| extra1        = S. P. Balasubrahmanyam
| length1       = 
| title2        = Sooryanobbane Chandranobbane
| lyrics2 	= Hamsalekha
| extra2        = S. P. Balasubrahmanyam
| length2       = 
| title3        = Yaaru Kaanada Sapthasagara
| lyrics3       = Hamsalekha
| extra3 	= Rajesh Krishnan, K. S. Chithra
| length3       = 
| title4        = Mangala Ragada
| extra4        = Rajesh Krishnan, K. S. Chithra
| lyrics4 	= Hamsalekha
| length4       = 
| title5        = Badavan Mane Oota Ruchiyammi
| extra5        = Rajesh Krishnan, K. S. Chithra
| lyrics5       = Hamsalekha
| length5       = 
}}

== References ==
 

== External source ==
*  
*  

 
 
 
 
 
 
 


 