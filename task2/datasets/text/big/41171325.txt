Madron (film)
{{Infobox film
| name           = Madron
| image          = Madron.jpg
| caption        = 
| director       = Jerry Hopper
| producer       = 
| writer         = 
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film directed by Jerry Hopper   and filmed in Israel; the first feature shot in Israel that was set in a non-Israeli location.  
was nominated for and Academy Award for Best Original Song and a Golden Globe Award for Best Original Song in 1971 for the song "Till Love Touches Your Life" by Riz Ortolani (music) and Arthur Hamilton. It was a return to feature films and the final film of director Jerry Hopper.

==Plot==
A nun, the only survivor of an Indian massacre of a wagon train, is taken in by a cantankerous old gunfighter.



==Cast==
* Richard Boone as Madron
* Leslie Caron as Sister Mary
* Gabi Amrani as Angel
* Paul L. Smith as Gabe Price
* Aharon Ipalé as Singer

==References==
 

==External links==
*  

 

 
 
 


 