Fugitive Pieces (film)
{{Infobox film
| name           = Fugitive Pieces
| image          = Fugitive pieces.jpg
| image_size     =
| caption        = Fugitive Pieces Poster
| director       = Jeremy Podeswa
| producer       =  
| writer         =  
| narrator       =
| starring       = {{plainlist|
*Stephen Dillane
*Rade Šerbedžija
*Rosamund Pike
*Ayelet Zurer
*Robbie Kay
*Ed Stoppard
*Rachelle Lefèvre
}}
| music          = Nikos Kypourgos
| cinematography = Gregory Middleton
| editing        = Wiebke von Carolsfeld
| distributor    =  
| released       =  
| runtime        = 104 min.
| country        =  
| language       = {{plainlist| English
*Greek Greek
*Yiddish Yiddish
*German German
}}
| budget         = $Canadian dollar|CAD9.5 million
| gross          =
| preceded_by    =
| followed_by    =
}}
 novel of the same name written by Anne Michaels. The film tells the story of Jakob Beer, who is orphaned in Poland during World War II and is saved by a Greek archeologist. The film premièred 6 September 2007 as the opening film of that years Toronto Film Festival.

==Cast==
*Stephen Dillane as Jakob Beer
*Rade Šerbedžija as Athos
*Rosamund Pike as Alex
*Ayelet Zurer as Michaela
*Robbie Kay as Young Jakob
*Ed Stoppard as Ben
*Rachelle Lefèvre as Naomi
*Nina Dobrev as Bella
*Themis Bazaka as Mrs. Serenou
*Danae Skiadi as Allegra
*Diego Matamoros as Jozef
*Sarah Orenstein as Sara
*Larissa Laskin as Irena
*Daniel Kash as Maurice
*Devon Bostick as Teenage Ben

==Production== Hamilton and Toronto) in 2006 at a cost of $Canadian dollar|CAD9.5 million.   

Matthew Davies was the production designer for the film. Peter Emmink was in charge of art direction. The costumes were designed by Anne Dixon. Set decoration was provided by Erica Milo and Nikos Triandafilopoulos. Visual effects were provided by Mr. X Inc. John Rowley was the music supervisor. Diane Pitblado was the dialect coach. 

==Release==
Fugitive Pieces premièred on 6 September 2007 as the opening film of that years Toronto Film Festival.  It was later shown at the Vancouver International Film Festival, the Warsaw International Film Festival, the Rome Film Festival, the International Thessaloniki Film Festival in Greece (where it was shown under the title Syntrimmia psyhis), the Santa Barbara International Film Festival and the Newport Beach Film Festival. 

It opened in limited release in the United States on 2 May 2008, grossed $102,212 in 30 theatres its opening weekend,  and earned a total US gross of $634,379.   

==Critical reception==
 , the review aggregator Rotten Tomatoes reported that 68 percent of critics gave the film positive reviews, based on 41 reviews &mdash; with the consensus that the film is "a moving holocaust tale aided by solid performances. Though the retelling is a bit too subtle, the moving story and solid performances lift Fugitive Pieces beyond standard holocaust tales".  Metacritic reported the film had an average score of 60 out of 100, based on 19 reviews &mdash; indicating mixed or average reviews. 

==Awards and nominations==
{| class="wikitable" Year
!width="100"|Nominated/Won
!width="100"|Award/category
!width="100"|Festival/organization Role
|-
| 2007 || Won  || Best Actor  || Rome Film Festival || Rade Šerbedžija as Athos
|- Best Supporting Actor in a Motion Picture || Satellite Award || Rade Šerbedžija as Athos
|-
| 2008 || Won    || Best Film || Sydney Film Festival || &mdash;
|-
| 2008 || Won || Audience Award (Narrative Feature) || Sarasota Film Festival || &mdash;
|-
| 2008 || Won  || Jury Award || Newport Beach Film Festival || &mdash;
|-
|}

The film won the jury award of the Newport Beach Film Festival in the categories Best Cinematographer (Gregory Middleton), Best Director and Best Screenplay (Jeremy Podeswa) and Best Film.

==References==
 

==External links==
* 
*   
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 