Doraemon: Nobita and the Kingdom of Clouds
{{Infobox film
| name           = Nobita and the Kingdom of Clouds
| image          = Nobita and the Kingdom of Clouds.jpg
| image_size     =
| caption        =
| director       = Tsutomu Shibayama
| producer       =
| writer         =
| narrator       =
| starring       = Nobuyo Ōyama Noriko Ohara
| music          = Shunsuke Kikuchi
| cinematography = Hideko Takahashi
| editing        = Kazuo Inoue Yuko Watase
| studio         = Asatsu
| distributor    = Toho Company
| released       =   
| runtime        = 98 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          = ¥1.68 billion  ($17.9 million)
}}
  is a feature-length Doraemon film which premiered on March 7, 1992.

== Plot ==
A boy, his father, and his grandfather are stranded in an island after their boat is crushed by a storm. The boys grandfather sees a spaceship ordering him to leave as there will be a storm that will flood the island. That night, a storm does indeed come and floods the island.

While studying about types of cloud, Nobita asks if there is Heaven in one of them, causing him to be ridiculed by everyone in the school. He resorts to find Heaven on his own when Doraemon initially rejects his pleas, though Doraemon later insists that they should build their own artificial "Heaven". Using Doraemons gadgets, they solidify a large area of clouds, build an environment on it, and create several cloud-shaped robots for help. They initially keep secret of it by using an invisible dome to disguise the cloud, but later tell it to Gian, Suneo, and Shizuka when they have to invest more money to build a huge castle and create buildings. With shares from Suneo (who invested the most), they manage to transform the area into a large kingdom with various facilities of their interest. Meanwhile, various nature areas in the world, such as the Amazon rainforest are reported to have been stolen by a cloud. As the news reach Nobita and his friends, the city cloud hits a high mountain. Doraemon and Nobita head down to check and find the boy seen in the beginning of the film, unconscious while riding a Crypthodon. They save the boy, but are puzzled by his and the Crypthodons appearance. The next day, Nobita is crowned the king and wears a special crown which causes everyone in the city cloud to bow down to him, which he uses to stop Gian and Suneo from fighting, but later learns that the boy had disappeared. They head down to try to find the Crypthodon, but winds up arriving at a mysterious land inside a cloud.

They meet Paruparu, a "sky human" who explains they are in the Kingdom of Clouds, a kingdom located in various clouds in the sky, hidden from the terrestrial humans and inhabited by humans who resorted to live in the sky after they had been disturbed by how other humans caused much destruction to Earth. The kingdom is the one responsible for the natural disappearance in Earth, explaining that they did that to prevent their extinction. Nobita and his friends are offered shelter, but escapes, mistaking the sky humans for locking them up. They are separated, with Gian, Suneo, and Shizuka taken back by the sky humans, while Nobita and a broken Doraemon are forced to wander the clouds. Gian, Suneo, and Shizuka explore the kingdom with Paruparu, eventually learning that the kingdom have been planning to release a great flood on Earth, dubbed as the "Noah plan" to clean up all of its "mess", but not before they evacuate all living things from it. Despite going against the idea, Gian, Suneo, and Shizuka lose favor during a court to determine the execution of the plan. Meanwhile, Nobita and Doraemon meet with their friend the lilliputian Hoi and his family, who had been taken to the kingdom after Donjara Villages destruction, as well as the disappeared boy, revealed to be named Tagaro. As Tagaro tells that it is impossible to leave the kingdom after wearing a special ring (which Nobita does not wear), Hoi uses his magical scarf on a swallow to transport Nobita and Doraemon to Nobitas kingdom. They go through the Anywhere Door to leave, but accidentally switch the time which causes them to arrive at the Earth several days later, when the Kingdom of Clouds have launched their flood. Doraemon fixes himself when his head hits a pole and goes back to the city cloud with Nobita.

To prevent the flood, Doraemon reveals a cannon which can destroy any cloud, though he only wants to threaten the sky humans, not necessarily defeat them. Meanwhile, a group of animal hunters escape from the Kingdom of Clouds (after being captured earlier) and resort to Nobita and Doraemons help. They steal Nobitas crown and fire a shot which destroys a section of the Kingdom of Clouds. Gian, Suneo, Shizuka, and Paruparu arrive to stop them, but they are thrown to jail and being reunited with Nobita and Doraemon. Feeling responsible for the destruction of the kingdom, Doraemon breaks free and destroys Nobitas city cloud gas using his iron head, destroying everything in the cloud, though Nobita, his friends, and the hunters are saved by the sky humans. Doraemon is gravely injured, while Paruparu speaks her objection for the Noah plan. Nobita and his friends are further supported by many of their friends who had been helped by them in the past, including Hoi and his family, Moa and Dodo, and the now-adult humanoid plant Kibo, who revives Doraemon with his green laser. With increased favor, the sky humans decide to abandon their Noah plan, and Nobita and his friends are transported back to Earth, bidding farewell on the sky humans.

== Cast ==
* Nobuyo Ōyama - Doraemon
* Noriko Ohara - Nobita Nobi
* Michiko Nomura - Shizuka Minamoto
* Kaneta Kimotsuki - Suneo Honekawa
* Kazuya Tatekabe - Jian

==Television broadcast==
The movie aired in India in Disney Channel India on 25 January 2014 and Hungama TV on 10 March 2014 on as Doraemon The Movie Nobita In Jannat No.1. 

==References==
 

== External links ==
*    
*  

 
 

 
 
 
 
 
 
 