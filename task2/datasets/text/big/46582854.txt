Grain (film)
{{Infobox film
| name           = Grain
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = Semih Kaplanoğlu
| producer       = Semih Kaplanoğlu, Johannes Rexin, Fredrik Zander, Sophie DuLac, Michel Zana
| writer         = Semih Kaplanoğlu
| screenplay     = 
| story          = 
| starring       =  
| music          = 
| cinematography = Giles Nuttgens
| editing        = 
| studio         = Heimatfilm, Kaplan Film, Arte France Cinéma
| distributor    = Piffl Medien
| released       =  
| runtime        = 
| country        =
| language       = English
| budget         = 
| gross          = 
}}

Grain is an upcoming 2015 German/Turkish/French science fiction film written and directed by Semih Kaplanoğlu.

==Plot==

Grain is set in the near future when human survival is threatened by war and famine. Scientists and geneticists must combine forces to create a sustainable food source.    

==Production==
The film is produced by Kaplanoğlus company, Kaplan Films; Heimatfilm (Germany) and Arte France Cinéma (France).   Part of the film was shot in Detroit, Michigan. 

==Cast==
*Jean-Marc Barr
*Ermin Bravo Grigory Dobrygin
*Lubna Azabal

==References==
 

==External links==
* 

 

 
 
 
 
 