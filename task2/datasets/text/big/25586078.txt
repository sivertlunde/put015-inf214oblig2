The Last Flight (2009 film)
{{Infobox film
| name           = The Last Flight
| image          = Le dernier vol.JPG
| image size     = 
| alt            = 
| caption        = French poster for the film
| director       = Karim Dridi
| producer       = Jean Cottin  Sidonie Dumas
| writer         = Karim Dridi Pascal Arnold Sylvain Estibal (novel)
| narrator       = 
| starring       = Marion Cotillard Guillaume Canet
| music          = Le Trio Joubran
| cinematography = Antoine Monod
| editing        = 
| studio         = Gaumont Film Company
| distributor    = 
| released       =     
| runtime        = 86 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 Bill Lancaster.

==Synopsis== French Camel Corps who joins in the hunt for Lancaster. As the two endure hardships in the desert, they begin to develop feelings for each other.

==Cast==
*Marion Cotillard as Marie Vallières de Beaumont
*Guillaume Canet as Antoine Chauvet
*Frédéric Epaud as Louis
*Michaël Vander-Meiren as Vasseur
*Guillaume Marquet as Capatain Vincent Brosseau
*Saïdou Abatcha as Saïddou

==Production== romantic epic.    After he narrated the story to Marion Cotillard, she agreed to play Marie, a character loosely based on Lancasters real-life lover, Chubbie Miller.
 Moroccan village Tuareg culture, Guillaume Canet learned Tuareg languages|Tamasheq, the Tuareg language. 

==Reception==

===Critical response=== The English The Sheltering Sky.   

===Box office===
The Last Flight opened in France in the fifth position behind Avatar (2009 film)|Avatar, Arthur and the Invisibles, Nicolas Vaniers Loup, and Frédéric Berthes RTT. By 27 December it had earned only $2.8 million. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 