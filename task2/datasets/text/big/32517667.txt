Dost (1944 film)
{{Infobox film
| name           = Dost
| image          =
| caption        =
| director       = Shaukat Hussain Rizvi
| producer       =  Navin Pictures
| writer         =
| starring       = Kanhaiyalal, Maya Banerjee, Agha (actor)|Agha, Motilal (actor)|Motilal, Noor Jehan, Shaukat Hussain Rizvi Sajjad Hussain Shams Lakhnavi (lyrics)
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}} 1944 Bollywood Sajjad Hussain.

==Cast==
*Kanhaiyalal
*Maya Banerjee Agha
*Himalaya Dasani
*Husn Banu
*Mirza Musharraf Motilal
*Noor Jehan
*Ram Pyari
*Shaukat Hussain Rizvi
*Sherali
*Varmala Kumthekar

==Soundtrack==

Dost was the first film of Sajjad Hussain as an independent music director, and its soundtrack featured hit songs. {{cite book
 | title = Hindi Film Song: Music Beyond Boundaries
 | author = Ashok Ranade
 | publisher = Bibliophile South Asia
 | year = 2006
 | isbn = 978-81-85002-64-4
}} 

{{Track listing
| extra_column    = Singer(s)

| all_lyrics      = Shams Lakhnavi Sajjad Hussain

| title1          = Ab Kaun Hai Mera
| extra1          = Noor Jehan
| length1         = 1:45

| title2          = Alam Par Alam Sitam Par Sitam
| extra2          = Noor Jehan
| length2         = 3:10

| title3          = Badanaam Muhabbat Kaun Kare
| extra3          = Noor Jehan
| length3         = 3:05

| title4          = Koi Prem Ka Deke Sandesa
| extra4          = Noor Jehan
| length4         = 3:35

| title5          = More Sajana Aake Daras Dikha
| extra5          = Noor Jehan
| length5         = 3:20
}}

==References==
 

==External links==
* 

 
 
 

 