Philadelphia (film)
{{Infobox film
| name           = Philadelphia
| image          = Philadelphia_imp.jpg
| caption        = Theatrical release poster
| director       = Jonathan Demme
| producer       = Jonathan Demme Edward Saxon
| writer         = Ron Nyswaner
| starring       = Tom Hanks Denzel Washington
| music          = Howard Shore
| cinematography = Tak Fujimoto Craig McKay
| studio         = Clinica Estetico
| distributor    = TriStar Pictures
| released       =    
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         = $26 million
| gross          = $206.7 million 
}}
Philadelphia is a 1993 American drama film and one of the first mainstream Hollywood films to acknowledge HIV/AIDS, homosexuality, and homophobia. It was written by Ron Nyswaner, directed by Jonathan Demme and stars Tom Hanks and Denzel Washington.

Hanks won the Academy Award for Best Actor for his role as Andrew Beckett in the film, while the song "Streets of Philadelphia" by Bruce Springsteen won the Academy Award for Best Original Song. Nyswaner was also nominated for the Academy Award for Best Original Screenplay, but lost to Jane Campion for The Piano.

==Plot==
The movie tells the story of Andrew Beckett (Tom Hanks), a Senior Associate at the largest corporate law firm in Philadelphia.  Beckett hides his homosexuality and his status as an AIDS patient from the other members of the law firm.  On the day Beckett is assigned the firms newest and most important case, a partner in the firm notices a lesion on Becketts forehead.  Although Beckett attributes the lesion to a racquetball injury, it is actually due to Kaposis Sarcoma, a form of cancer marked by multiple tumors on the lymph nodes and skin.

Shortly thereafter, Beckett stays home from work for several days to try to find a way to hide his lesions.  While at home, he finishes the paperwork for the case he has been assigned and then brings it to his office, leaving instructions for his assistants to file the paperwork the following day, which marks the end of the statute of limitations for the case.  Later that morning he receives a call asking for the paperwork, as the paper copy cannot be found and there are no copies on the computers hard drive.  The paperwork is finally discovered in an alternate location and is filed with the court at the last possible moment.  The following day Beckett is fired by the firms partners.

Beckett believes that someone deliberately hid his paperwork to give the firm an excuse to fire him, and that the firing is actually as a result of his diagnosis with AIDS.  He asks several attorneys to take his case, including personal injury lawyer Joe Miller (Denzel Washington).  Miller has mysophobia and knows little about Becketts disease.  After declining to take the case, Miller immediately visits his doctor to find out if he could have contracted the disease.  The doctor explains the methods of AIDS infection.

Unable to find a lawyer willing to represent him, Beckett is compelled to act as his own attorney.  While researching a case at a law library, Miller sees Beckett at a nearby table.  After a librarian announces that he has found a book on AIDS discrimination for Beckett, others in the library begin to first stare and then move away, and the librarian suggests Beckett retire to a private room.  Disgusted by the other peoples behavior, Miller approaches Beckett, reviews the material Beckett has gathered, and takes the case.

As the case goes before the court, the partners of the firm take the stand, each claiming that Beckett was incompetent and that he had deliberately tried to hide his condition.  The defense repeatedly suggests that Beckett had invited his illness through his homosexual acts and was therefore not a victim.  In the course of testimony, it is revealed that the partner who had noticed Becketts lesion had previously worked with a woman who had contracted AIDS after a blood transfusion and so should have recognized the lesion as relating to AIDS.  According to that partner, the woman was an innocent victim, unlike Beckett, and further testified that he did not recognize Becketts lesions.  To prove that the lesions would have been visible, Miller asks Beckett to unbutton his shirt while on the witness stand, revealing that his lesions were indeed visible and recognizable as such.

Beckett eventually collapses during the trial.  During his hospitalization, the jury votes in his favor, awarding him back pay, damages for pain and suffering, and punitive damages.  Miller visits the visibly failing Beckett in the hospital after the verdict and overcomes his fear  enough to touch Becketts face.  After Becketts family leaves the room, he tells his partner, Miguel (Antonio Banderas) that he is ready to die. At the Miller home, Joe and his wife are awakened by a phone call from Miguel, presumably to tell them that Beckett has died. The movie ends with a reception at Becketts home following the funeral, where many mourners, including Miller, view home movies of Beckett as a healthy child.

==Cast==
* Tom Hanks as Andrew Beckett
* Denzel Washington as Joe Miller
* Jason Robards as Charles Wheeler
* Antonio Banderas as Miguel Álvarez
* Joanne Woodward as Sarah Beckett
* Robert W. Castle as Bud Beckett   
* Mary Steenburgen as Belinda Conine
* Ann Dowd as Jill Beckett
* Lisa Summerour as Lisa Miller Charles Napier as Judge Lucas Garnett
* Roberta Maxwell as Judge Tate
* Buzz Kilman as Crutches
* Karen Finley as Dr. Gillman
* Robert Ridgely as Walter Kenton
* Bradley Whitford as Jamey Collins
* Ron Vawter as Bob Seidman
* Anna Deavere Smith as Anthea Burton
* Tracey Walter as Librarian
* Julius Erving as himself
* Ed Rendell as himself
* Chandra Wilson as Chandra David Drake as Bruno
* Roger Corman as Mr. Laird

==Inspiration==
The events in the film are similar to the events in the lives of attorneys Geoffrey Bowers and Clarence B. Cain.

Bowers was an attorney who in 1987 sued the law firm Baker & McKenzie for wrongful dismissal in one of the first AIDS discrimination cases. Cain was an attorney for Hyatt Legal Services who was fired after his employer found out he had AIDS. He sued Hyatt in 1990 and won just before his death. 

===Controversy===

Bowers family sued the writers and producers. A year after Bowerss death, producer Scott Rudin interviewed the Bowers family and their lawyers and, according to the family, promised compensation for the use of Bowerss story as a basis for the film. Family members asserted that 54 scenes in the movie are so similar to events in Bowerss life that some of them could only have come from their interviews. However, the defense said that Rudin abandoned the project after hiring a writer and did not share any information the family had provided.     The lawsuit was settled after five days of testimony. Although terms of the agreement were not released, the defendants did admit that "the film was inspired in part" by Bowerss story.   

Jonathan Demme has stated that he was moved to direct the film after a friend of his, the illustrator Juan Suárez Botas, was diagnosed with AIDS.

==Release== And the Band Played On) and signaled a shift in Hollywood films toward more realistic depictions of gays and lesbians. According to a Tom Hanks interview for the 1995 documentary The Celluloid Closet, scenes showing more affection between him and Banderas were cut, including one with him and Banderas in bed together. The DVD edition, produced by Automat Pictures, includes this scene. 

==Reception==

===Box office===
Philadelphia was originally released on December 22, 1993, in a limited opening of only 4 theaters, and had a weekend gross of $143,433 with an average of $35,858 per theater. The film expanded its release on January 14, 1994 to 1,245 theaters and opened at #1, grossing $13,817,010 over the 4-day Martin Luther King, Jr. weekend, averaging $11,098 per theater. The film stayed at #1 the following weekend, earning another $8,830,605.

In its 14th weekend, the weekend after the Oscars, the film expanded to 888 theaters, and saw its gross increase by 70 percent, making $1,941,168 and jumping from #15 the previous weekend (when it made $1,141,408 from 673 theaters), to returning to the top 10 ranking at #8 that weekend.

Philadelphia eventually grossed $77,446,440 in North America and $129,232,000 overseas for a total of $206,678,440 worldwide against a budget of only $26 million, making it a huge box office success, and becoming the 12th highest grossing film in the US of 1993.  , Box Office Mojo 

===Critical response===
Philadelphia earned mostly positive reviews from critics, with Hanks and Washington receiving wide praise for their performances, and garnering a 78% approval rating at online movie critic site Rotten Tomatoes, based on 47 reviews, with an average rating of 6.6/10.  In a contemporary review for the Chicago Sun-Times, Roger Ebert gave the film three-and-a-half out of four stars and said that it is "quite a good film, on its own terms. And for moviegoers with an antipathy to AIDS but an enthusiasm for stars like Tom Hanks and Denzel Washington, it may help to broaden understanding of the disease. Its a ground-breaker like Guess Whos Coming to Dinner (1967), the first major film about an interracial romance; it uses the chemistry of popular stars in a reliable genre to sidestep what looks like controversy." 

Christopher Matthews from Seattle Post-Intelligencer wrote “Jonathan Demme’s long-awaited Philadelphia is so expertly acted, well-meaning and gutsy that you find yourself constantly pulling for it to be the definitive AIDS movie.”     James Berardinelli from ReelViews wrote “The story is timely and powerful, and the performances of Hanks and Washington assure that the characters will not immediately vanish into obscurity.”   Rita Kempley from the Washington Post wrote “It’s less like a film by Demme than the best of Frank Capra. It is not just canny, corny and blatantly patriotic, but compassionate, compelling and emotionally devastating.”  

===Accolades===

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Academy Awards Academy Award Best Actor Tom Hanks
| 
|- Academy Award Best Makeup Carl Fullerton and Alan DAngerio
| 
|- Academy Award Best Original Song Neil Young  ("Philadelphia") 
| 
|- Bruce Springsteen  ("Streets of Philadelphia") 
| 
|- Academy Award Best Original Screenplay  Ron Nyswaner
| 
|- American Society ASCAP Awards Top Box Office Films Howard Shore
| 
|- Most Performed Songs from Motion Pictures Bruce Springsteen  ("Streets of Philadelphia") 
| 
|- British Academy BAFTA Award BAFTA Award Best Original Screenplay Ron Nyswaner
| 
|- Berlin International Film Festival Golden Berlin Bear Jonathan Demme
| 
|- Silver Berlin Bear for Best Actor    Tom Hanks
| 
|- Casting Society of America Artios for Best Casting for Feature Film, Drama Howard Feuer
| 
|- Chicago Film Critics Association Award Best Director Jonathan Demme
| 
|- Best Actor Tom Hanks
| 
|-
|Dallas–Fort Worth Film Critics Association Award Best Actor
| 
|- GLAAD Media Award GLAAD Media Outstanding Film – Wide Release Jonathan Demme
| 
|- Edward Saxon
| 
|- Golden Globe Awards Golden Globe Best Actor – Motion Picture Drama Tom Hanks
| 
|- Golden Globe Best Screenplay Ron Nyswaner
| 
|- Golden Globe Best Original Song Bruce Springsteen  ("Streets of Philadelphia") 
| 
|- Golden Screen, Germany TriStar Pictures
| 
|- Grammy Award Best Song Written Specifically for a Motion Picture or for Television Bruce Springsteen  ("Streets of Philadelphia") 
| 
|- MTV Movie Awards MTV Movie Best Movie Jonathan Demme
| 
|- Edward Saxon
| 
|- MTV Movie Best Male Performance Tom Hanks
| 
|- MTV Movie Best On-Screen Duo
| 
|- Denzel Washington
| 
|- MTV Movie Best Song from a Movie Bruce Springsteen  ("Streets of Philadelphia") 
| 
|- National Board of Review Award Top Ten Films
| 
|- Writers Guild of America Award Best Original Screenplay Ron Nyswaner
| 
|-
|}
 Top 100 Heroes and Villains.

The film was ranked #20 on AFIs 100 Years...100 Cheers.

==Soundtrack==
A soundtrack album was released in 1993 containing the main music featured in the film,  mastered by Ted Jensen.

===Track listing===
{{Track listing
| extra_column    = Artist(s)
| total_length    = 
| writing_credits = 
| title1          = Streets of Philadelphia
| extra1          = Bruce Springsteen
| writer1         = 
| length1         = 3:56
| title2          = Lovetown
| extra2          = Peter Gabriel
| writer2         = 
| length2         = 5:29
| title3          = Its in Your Eyes
| extra3          = Pauletta Washington
| writer3         = 
| length3         = 3:46
| title4          = Ibo Lele (Dreams Come True) RAM
| writer4         = 
| length4         = 4:15
| title5          = Please Send Me Someone to Love Sade
| writer5         = 
| length5         = 3:44
| title6          = Have You Ever Seen the Rain?
| extra6          = Spin Doctors
| writer6         = 
| length6         = 2:41
| title7          = I Dont Wanna Talk About It
| extra7          = Indigo Girls
| writer7         = 
| length7         = 3:41
| title8          = La mamma morta
| note8           = From the Opera Andrea Chénier
| extra8          = Maria Callas
| writer8         = 
| length8         = 4:53
| title9          = Philadelphia
| extra9          = Neil Young
| writer9         = 
| length9         = 4:06
| title10          = Precedent
| extra10          = Howard Shore
| writer10         = 
| length10         = 4:03
}}

The album was re-released in 2008 in France only as a joint CD and DVD pack with the film itself, however the track listing remained the same. The catalogue number is 88697 322052 under both the Sony BMG Music Entertainment and Sony Classical labels with identical catalogue numbers.  The director purposefully asked Bruce Springsteen to make the main song for this film in an effort to try to get more people who don’t know much about AIDS to be more comfortable with viewing the film, and to raise awareness overall. 

==See also==
 
* List of American films of 1993
* Cultural depictions of Philadelphia
* Andrea Chénier, opera by Umberto Giordano

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 