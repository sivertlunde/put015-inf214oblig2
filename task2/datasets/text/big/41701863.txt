Parade en sept nuits
{{Infobox film
| name           = Parade en sept nuits
| image          =
| caption        =
| director       = Marc Allégret
| producer       = 
| writer         = Marc Allégret Marcel Achard 
| based on = 
| starring       = Louis Jourdan Jules Berry Victor Boucher André Lefaur Micheline Presle Raimu
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = France
| language       = French
| budget         = 
}}
Parade en sept nuits is a 1941 French film. 
==Plot==
In a dog pound, one of the dog tells stories about his former life, including adventures in a circus.
==Production==
Production commenced in 1940 at Studio Pathe-Cinea in Paris, but was interrupted by the war. It resumed almost a year later in the town of Nice at Studios de la Victorine. 

==References==
 
==External links==
*  at IMDB
 
 

 