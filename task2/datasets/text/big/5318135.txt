My Friend Irma Goes West
{{Infobox Film |
 name = My Friend Irma Goes West|
 image = myfriendirmawest.jpg|
 director = Hal Walker|
 writer = Cy Howard Parke Levy| John Lund Marie Wilson Diana Lynn Dean Martin Jerry Lewis|
 producer = Hal B. Wallis|
 distributor = Paramount Pictures|
 released =  |
 runtime = 90 minutes|
 language = English| 
 gross = $2.4 million (US rentals)  10,530 admissions (France)   at Box Office Story |
}} My Friend My Friend Irma (1949) was released May 31, 1950 by Paramount Pictures.

==Plot== John Lund) Marie Wilson), Jane (Diana Lynn) and Seymour (Jerry Lewis), all head to Hollywood.

The trip ends suddenly when the producer is discovered to be an escaped lunatic. Al tries to set things straight by taking the gang to Las Vegas to work at a casino, but things arent as they seem. Irma causes havoc by wrecking a rigged roulette wheel, and she gets kidnapped and held for ransom until Al can raise $50,000.

Meanwhile, Seymour, dressed as an Indian brave, locates Irma and rescues her. The publicity received during the entire incident brings another movie offer... this time for Irma and Seymour.

==Production==
My Friend Irma Goes West was filmed from January 31 through March 18, 1950.  Although At War with the Army was made before this film it was not released until December 1950, making My Friend Irma Goes West the second Martin and Lewis film released.

==Cast== John Lund	 ... 	Al Marie Wilson... 	Irma Peterson
*Diana Lynn	... 	Jane Stacy
*Dean Martin	... 	Steve Laird
*Jerry Lewis	... 	Seymour
*Corinne Calvet	... 	Yvonne Yvonne
*Lloyd Corrigan	... 	Sharpie Corrigan
*Gregg Palmer ... Ambulance attendant
*Don Porter	 ... 	Mr. Brent

==DVD releases==
My Friend Irma Goes West has been released twice on DVD. It was originally released on a two-film collection with My Friend Irma on October 25, 2005. A year later it was included on an eight-film DVD set, the Dean Martin and Jerry Lewis Collection: Volume One, released October 31, 2006.

==References==
 

== External links ==
* 

 
 

 
 
 
 