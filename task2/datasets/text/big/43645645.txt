L'ospite
{{Infobox film
| name           = Lospite
| image          = The Guest. film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Liliana Cavani
| producer       = 
| writer         = Liliana Cavani
| starring       = Lucia Bosè   Glauco Mauri 
| music          = Gioacchino Rossini
| cinematography = Giulio Albonico
| editing        = Andreina Casini
| studio         = Lotar Film  RAI 
| distributor    = Sacis
| released       =  
| runtime        = 93 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
Lospite ( ) is a 1972 Italian drama film directed by Liliana Cavani and starring Lucia Bosè. The plot follows a woman who once released from a mental hospital, tries in vain to fit into society. Made with a very low budget, it was shown out of competition at the Venice Film Festival. Marrone, The Gaze and the Labyrinth, p. 254 
==Plot==
A writer, obtains permission to visit a psychiatric hospital in order to prepare for his next novel. He finds several deficiencies in both the medical and social treatment of the patients. His criticisms are not well received by the doctors. the writer meets Anna, a woman who healed from her mental issues,has been entrusted to the care of her brother. Annas reintegration into her family and society have proved difficult.
==Cast==
* Lucia Bosè as Anna
* Glauco Mauri as Piero
*Peter Gonzales as Luciano
*Alvaro Piccardi as Anna’s brother
*Giancarlo Caio as the doctor
*Gian Piero Frondini
*Alfio Galardi
*Maddalena Gillia
*Maria Luisa Salmaso
*Lorenzo Piani

==Notes==
 

== References ==
* Marrone, Gaetana. The Gaze and the Labyrinth: the Cinema of Liliana Cavani. Princenton Paperbacks, 2000. ISBN 0-691-00873-6

==External links==
*  Detailed information on this film.
* 

 

 
 
 
 
 
 
 
 