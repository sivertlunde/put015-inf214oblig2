Demolition Man (film)
 
 
 
{{Infobox film
| name           = Demolition Man
| image          = Demolition man.jpg
| caption        = Theatrical release poster
| director       = Marco Brambilla
| producer       = {{plainlist|
* Joel Silver 
* Michael Levy
* Howard Kazanjian
}}
| screenplay     = {{plainlist| Daniel Waters
* Robert Reneau
* Peter M. Lenkov
}}
| story          = {{plainlist|
* Peter M. Lenkov
* Robert Reneau
}}
| starring       = {{plainlist|
* Sylvester Stallone
* Wesley Snipes
* Sandra Bullock
* Nigel Hawthorne
}} 
| music          = Elliot Goldenthal Alex Thomson
| editing        = Stuart Baird
| studio         = Silver Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $159.1 million 
}}
Demolition Man is a 1993 American science fiction action film directed by Marco Brambilla in his directorial debut. The film stars Sylvester Stallone and Wesley Snipes. The film was released in the United States on October 8, 1993. 
 crime lord Cryogenically frozen in 1996, they are restored to life in the year 2032 to find mainstream society changed and all crime seemingly eliminated.
 dystopian novel, Brave New World.   

==Plot== LAPD Sgt. John Spartan leads a Special Operations unit on an unauthorized mission to rescue hostages taken by the psychopathic career criminal Simon Phoenix and his henchmen. After a thermal scan reveals no sign of the hostages, Spartan enters Phoenixs stronghold, and engages Phoenixs men and captures Phoenix himself, who before his arrest has detonated several barrels of C-4 (explosive)|C4, destroying the building. The hostages bodies are found in the rubble, Phoenix "pleads his regard", and Spartan is charged with their deaths. Both men are frozen in the "California Cryo-Penitentiary" and exposed to subconscious rehabilitation techniques.
 Santa Barbara pacifist guidance and control of an envangelistic Dr. Raymond Cocteau. Weapons and vices are outlawed, human behavior is regulated, citizens carry implanted transceivers, and in the resulting absence of any violent crime, the San Angeles Police (SAPD) has lost any ability to handle violent behavior of any kind.

Phoenix is awakened for a parole hearing, kills the warden, armed guards, and several peace officers, demonstrating superhuman abilities and martial arts skills. Veteran officer Zachary Lamb suggests that Spartan be revived and reinstated to the force to help them capture Phoenix. Lieutenant Lenina Huxley is assigned to assist Spartan in his transition, despite the reluctance of Chief George Earle, who takes an immediate dislike to him.

The revived Spartan has trouble adapting to life in the future. Most of Huxleys fellow officers perceive Spartan as thuggish and uncivilized, he finds the culture, bans and the peaceful society repulsing (constantly getting fines over excessive swearing), and is at odds with Earle, who finds him to be a barbaric, heretic "caveman".  In the meantime, the white-robed Dr. Cocteau has recruited Phoenix to kill Edgar Friendly, the ragtag leader of the "Scraps"—resistance fighters living in the ruins beneath San Angeles, whom Cocteau sees as the threat to the narcotized society he has created.

The first Spartan-Phoenix confrontation is at the "Museum of Antiquities" weapon exhibit, where Phoenix goes to arm himself, encountering Spartan, who had deduced this strategy. Phoenix evades Spartan and encounters Dr. Cocteau, whom he tries to shoot, but he is programmed against that ability. Cocteau reminds him of why he was revived: to kill Edgar Friendly. In a subsequent encounter, Dr. Cocteau adds Spartan to his hit list for Phoenix, and agrees to give him the territory of Santa Monica upon completion. Spartan and Huxley learn of this and that Dr. Cocteau is "an evil Fred Rogers|Mr. Rogers" rather than San Angeless saintly god-king. He had programmed Phoenix to make him a more capable, dangerous maniac, and to use him as an assassin to eliminate Friendly. While Spartan, Huxley and young officer Alfredo Garcia enter the underground city to warn Friendly, Phoenix confronts Cocteau and demands that he release a list of other prisoners to assist him.

At Friendlys base, Phoenix and an irredeemable supplement of recruits attempt to kill both Spartan and Friendly, whom Spartan and Huxley have joined underground. They escape in Friendlys vintage Oldsmobile 4-4-2, and pursue Phoenix, who stole a police car. In communication during the car chase, Phoenix reveals that the hostages Spartan tried to rescue in their 1990s encounter were dead before the building exploded: Spartan was innocent of any crime and was terminated (frozen) for nothing. Phoenix escapes. Friendly, recruiting Garcia, leads the Scraps from the underground to join the police against Phoenix and his gang.

Phoenix orders the gang to kill Cocteau, which his programming prevents him from doing directly. Spartan and Huxley arrive at Cocteaus headquarters to capture Phoenix and his accomplices. Phoenix escapes to the prison to revive (defrost) and recruit even more dangerous convicts. After knocking out Huxley to protect her, Spartan enters the prison to confront Phoenix. Spartan uses a cryotube to freeze Phoenix solid, destroys Phoenix by knocking his head off, and escapes as the cryomachinery overloads, destroying the prison. With Cocteau dead and the prison destroyed, the police and the Scraps find themselves at odds over how to begin the framework for their new society. Spartan suggests that they find a way to compromise between order and personal freedom, then kisses Huxley and departs with her.

==Cast==
 
* Sylvester Stallone as Sergeant John Spartan
* Wesley Snipes as Simon Phoenix
* Sandra Bullock as Lieutenant Lenina Huxley 
* Nigel Hawthorne as Doctor Raymond Cocteau 
* Benjamin Bratt as Officer Alfredo Garcia 
* Denis Leary as Edgar Friendly
* Rob Schneider as Erwin (uncredited) 
* Jack Black as Wasteland Scrap 
* Bill Cobbs as Zachary Lamb (old) 
* Bob Gunton as Chief George Earle 
* Paul Perri as Squad Leader
* Pat Skipper as Helicopter Pilot
* Glenn Shadix as Associate Bob
* Trent Walker as Boggle Guard  Troy Evans as Tough Cop 
* Grand L. Bush as Zachary Lamb (young)
* Steve Kahan as Captain Healy
* Andre Gregory as Warden William Smithers
* Jesse Ventura as Adam, Cryocon Ally
* Brandy Ledford as "wrong number" video phone girl
 

Bullock replaced original actress Lori Petty in the role of Lenina Huxley after a few days filming.  Her characters name is a reference to Aldous Huxley, the author of Brave New World, and Lenina Crowne, a character in Brave New World. 

Originally Jean-Claude Van Damme and Steven Seagal were offered lead roles in the film.  The role of Simon Phoenix was also offered to Jackie Chan. 

==Production==

===Regional differences===
One of the films focal points is Taco Bell being the sole surviving restaurant chain in the world. Because Taco Bell is not widely available outside the U.S., the European version substitutes it with Pizza Hut, with lines re-dubbed and logos changed during post-production. 

===Plagiarism controversy=== Hungarian science the fall of the Iron Curtain, and that he knows the person he claims to be responsible for illegally selling his idea to the filmmakers. 

===Soundtrack===
  title theme Sting during Demolition Man.

Elliot Goldenthal composed the score for the film. It was his second big Hollywood project after the Alien 3 (soundtrack)|Alien³ score.

== Release ==
The film debuted at No. 1 at the box office.     Demolition Man grossed $58,055,768 by the end of its box office run in North America and $159,055,768 worldwide.   

Warner Bros. released it on VHS in March 1994,  on DVD in October 1997,  and on Blu-ray in August 2011. 

==Reception==
The film received mixed reviews. Rotten Tomatoes gives the film a score of 64% rating on based on 36 reviews.  The film scored a 34/100 on Metacritic, based on 9 reviews. 

 
   

On Siskel & Ebert, Gene Siskel gave the film a "thumbs down", criticizing its violence, but he praised its "funny offbeat script." Roger Ebert praised the movie: "Unlike so many other movies of its genre, it really does have a satiric angle to it." 

==Adaptations==

===Literature===
A four-part limited-series comic adaptation was published by DC Comics starting in November, 1993. A novelization, written by Robert Tine, was also published in October, 1993.

===Games=== Demolition Man 3DO version is a multi-genre game that incorporates Full Motion Video scenes, with both Sylvester Stallone and Wesley Snipes reprising their roles as their characters in scenes that were filmed exclusively for the game.
 Williams released Demolition Man based on the movie. It is designed by Dennis Nordman. The game features sound clips from the movie, as well as original speech by Stallone and Snipes. This game was part of WMS SuperPin series (The Twilight Zone#Pinball|Twilight Zone, Indiana Jones#Pinball|Indiana Jones, etc.).

==See also==
* List of films featuring surveillance

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 