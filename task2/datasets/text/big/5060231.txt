The Ugly Dachshund
{{Infobox film
| name           = The Ugly Dachshund
| image          = The Ugly Dachshund poster.jpg
| caption        = Theatrical release poster
| director       = Norman Tokar
| producer       = Walt Disney Winston Hibler
| writer         = Albert Aley Gladys Bronwyn Stern  
| based on       =  
| narrator       =  Dean Jones Suzanne Pleshette Charlie Ruggles
| music          = George Bruns Edward Colman Robert Stafford Walt Disney Productions Buena Vista Distribution
| released       =   (premiere)  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $6.2 million (est. US/ Canada rentals) 
| preceded_by    = 
| followed_by    = 
}}
 Walt Disney Dean Jones and Suzanne Pleshette in a story about a Great Dane who believes hes a dachshund. Based on a 1938 novel by Gladys Bronwyn Stern, the film was written by Albert Aley and directed by Norman Tokar. The Ugly Dachshund was one of several light-hearted comedies produced by the Disney Studios during the 1960s.

==Plot==
Fran Garrison ( ) mentions that his female Great Dane, Duchess, has also just given birth but unfortunately does not have enough milk for all of her puppies and the runt of the group has been cast aside by her - as most dogs do under these circumstances. Doc Pruitt convinces Mark to bring the Great Dane puppy home so that Danke may wet nurse him until he has weaned. When he arrives home and Fran notices that there is another puppy, she is surprised but does not suspect that the puppy is from another litter and reminds Mark that he should thank Danke for giving him a boy like he always wanted. Before he knows it and after keeping his secret from Fran, the puppies have grown drastically in size. Of course, the Great Dane puppy whom he has named Brutus is significantly larger than the other four Dachshunds. As the dog grows up with Frans Dachshund puppies, he believes he is one of them and picks up mannerisms like hunching close to the ground to walk. The Dachshunds are mischievous creatures and lead poor unsuspecting Brutus through a series of comic misadventures with Officer Carmody (now Sergeant Carmody) being chased up a tree, Marks studio being splattered with paint, and a garden party being turned topsy-turvy. Fran wants Mark to remove Brutus from the house once-and-for-all but when Brutus saves her favorite puppy, Chloe from the garbage truck, she changes her mind. Mark and Fran enter their dogs in a dog show with Brutus meeting others of his breed. He notices a female Harlequin Great Dane and stands at attention. He goes on to win two blue ribbons. Brutus finally finds out what its like to be a Great Dane.

==Cast== Dean Jones as Mark Garrison
* Suzanne Pleshette as Fran Garrison Charlie Ruggles as Doctor Pruitt
* Kelly Thordsen as Officer Carmody
* Parley Baer as Mel Chadwick
* Robert Kino as Mr. Toyama Mako as Kenji Charles Lane as the Judge
* Gil Lamb as the Milkman
* Dick Wessel as Eddie the Garbageman

==Reception==

=== Critical reception ===
The film has received generally mixed reviews. 

==References==
 

== External links ==
*   
*  
*  

 

 
 
 
 
 
 
 
 
 