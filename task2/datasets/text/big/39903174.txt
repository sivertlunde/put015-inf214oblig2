Cravos de Abril
Cravos Portuguese historic Ricardo Costa. Covering the events between April 24 and May 1, it portrays the Carnation Revolution, which took place in Portugal in April 1974 and put an end to Oliveira Salazar’s dictatorship.

== Synopsis ==
Introduction: the state of the country, the Portuguese Colonial War, the fascist dictatorship.

The first hours of April 24 at Praça do Comércio in Lisbon. Besieging and besieged troops. The unfolding of events during the square occupation. A threatening frigate sails up and down the Tagus river. The rebelled armed forces move towards Largo do Carmo.
 Rua Augusta National Republican Guard is attacked by rebels and capitulates. There a crowd of enthusiastic citizens join together inciting soldiers to fight.
 Estado Novo, Caxias fortress Peniche next day, the prison of which will be the new home for the captured agents, the «pides».

The declaration of the revolutionary intents broadcast by the Movimento das Forças Armadas (MFA) illustrated by Siné. Mário Soares and Álvaro Cunhal arrive in Lisbon after their long exiles. The crowd at the «first May 1», the International Workers Day rally in Lisbon, 1974, is carried away by their speech.

Soldiers are enthusiastically given red carnations by passers on the Lisbon streets. The wall paintings in honor of the revolution. The future in question. (Producer´s citation)

== Credits == Ricardo Costa
* Producer: Ricardo Costa
* Photography: Ricardo Costa
* Format: 16 mm color and b/w RTP (16mm, b/w)
* Photos by Eduardo Gageiro
* Illustrations by Siné
* Sound: radio archives
* Editing: Maria Beatriz
* Image lab: Ulyssea Filme RTP
* Running time: 28
* Premiere: Mai 1 1976 (RTP)
* DVD release: Lusomundo

== History == Ricardo Costa is awoken with a phone call from his friend Ilidio Ribeiro, his partner in publishing activities, announcing that a revolutionary coup was just taking place and that he would soon pick him up with his car. Costa owns a Paillard-Bolex 16mm movie camera, running with a manual wound spring motor. He has two color Kodak|Eastman   rolls stored in the frig. Their publishing house, MONDAR editores, has in press a book with drawings by the French humorist Maurice Sinet (Siné), entitled CIA.  The publishers are watched by the police, the PIDE/DGS, since their books disturb good order and subvert the rules of the fascist regime which has been governing Portugal for about fifty years.
 National Republican Guard (GNR) broadcasts orders to their agents to fight against their enemy. Understanding where the confrontation would take place, they move to the right place immediately, the square Praça do Comércio, where the rebels had gathered their tanks.

Ricardo Costa starts shooting. Just a few curious persons moved on the square among soldiers at that time, besides some professional photographers, such as Eduardo Gageiro. The situation evolves. More and more citizens arrive to support the coup. Followed by an increasing crowd, the tanks move to the Largo do Carmo, to the headquarters of the GNR, where Marcelo Caetano, António de Oliveira Salazar|Salazar’s successor, had taken refuge. Ricardo makes short shots to spare film, but captures the essential events occurred until dawn. Cut in four 30 meters rolls, 120 meters of film have been impressed.
 ARD had been in the meantime contacted and wants those pictures. The Lisbon airport is closed, but the film is sent with the first plane flying to Germany, where it is broadcast. Siné takes the first airplane flying from Paris to Lisbon and arrives just before the celebrations of May 1. Engaged in the revolution in his own way, he makes drawings from what he sees and comments to the Portuguese press.    

The second 120 meters reel is used to film Mário Soares’s and Álvaro Cunhal’s arrivals, the Workers’ Day rally in Lisbon and other occasional events just before Mai 1.  
 PREC (The revolutionary period in Portugal, April 1974 through April 1976).

The film April Carnations will just be finished when the revolutionary period comes to an end. It will permiere at the RTP in Mai 1 1976.

==References==
 

== See also ==

===See also===
* Carnation Revolution
* Direct cinema
* Political cinema

=== External links ===
Special screenings :
*     at   magazine, number 8, April 21, 1999) (fr) Peniche fortress, April 2004  (celebrating the revolution 30 years later) (pt)
*   Cravos de Abril, April 3, 2005, at  , Résidence André Gouveia, Paris (pt)
* Université Rutgers-Newark, New Jersey, USA, April 25, 2006
* Cinemateca Portuguesa (Portuguese Film Archive) – screening restored film, 2008
* UCAD Cheikh Anta DIOP University, Dakar, Senegal, April 25, 2008
*     at the Salle du Portugal de la CPLP (Community of Portuguese Language Countries, 2008
*   Cinemateca Portuguesa (Portuguese Film Archive) April 30, 2013 ( )
----
Infos : 
*  
*  , DVD release: Lusomundo RTP 
*     at Cinema Português

 
 
 
 
 