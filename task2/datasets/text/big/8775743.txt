The Alphabet Killer
{{Infobox film
| name = The Alphabet Killer
| image = Alphabet killer mp.jpg
| image_size =
| caption = Theatrical release poster
| director = Rob Schmidt
| producer = Tom Malloy Isen Robbins Aimee Schoof Russell Terlecki Daniel Sollinger
| writer = Tom Malloy
| starring = Eliza Dushku Cary Elwes Michael Ironside Bill Moseley Carl Lumbly Meltem Cumbul Tom Noonan  and Timothy Hutton
| music = Eric Perlmutter
| cinematography = Joe DeSalvo
| editing = Frank Reynolds
| distributor = Anchor Bay Entertainment
| released = November 7, 2008
| runtime = 100 minutes
| country = United States
| language = English
| budget =
| gross = $33,975 (INT) http://www.boxofficemojo.com/movies/?id=alphabetkiller.htm 
| preceded_by =
| followed_by =
}}

The Alphabet Killer is a 2008 Thriller (genre)|thriller-horror film, loosely based on the Alphabet murders that took place in Rochester, New York between 1971 and 1973.  Eliza Dushku stars as the main character, alongside Cary Elwes, Michael Ironside, Bill Moseley and Timothy Hutton. The film is directed by Rob Schmidt, director of Wrong Turn also starring Dushku and written by Tom Malloy, who also acted in a supporting role.

==Plot==
Megan Paige (Eliza Dushku) is an investigator for the Rochester Police Department investigating the murder of a young girl named Carla Castillo. Her body was found in the nearby village of Churchville, New York with white cat hair on it. Against opposition of her colleagues and partner/boyfriend Kenneth Shine (Cary Elwes), Megan insists that the murder is a work of a serial killer. Despite Megan’s considerable efforts she fails to catch the killer. Stress and obsession of the investigation causes Megan to hallucinate the victims image. She ultimately has a nervous breakdown after being kicked off the case and tries to commit suicide.
 Webster with some white cat hair on it, Megan successfully lobbies to rejoin the investigation. Partnered with Steven Harper (Tom Malloy), they try to find links between the girls. Then another girl, Melissa Maestro, is killed in Macedon, New York|Macedon. They find a number of commonalities between Wendy and Melissa, but fail to connect these to the first victim. The Webster Police Department, which has jurisdiction over the latest murder but are uncooperative, receive a call from 19-year-old Elizabeth Eckers who is being held hostage in a house. Megan is convinced the suspect is not the Alphabet Killer and breaks procedure to preempt a police raid. Megan defuses the situation, but an officer shoots the suspect through a window and kills him. Webster police declare that the Alphabet Killer is dead and announce the discovery of white cat hair in the house. Megan spirals into another nervous breakdown.

Certain that the Webster police planted the evidence in order to justify killing an innocent, Megan continues the investigation on her own. Megan discovers that all three girls attended Saint Michaels Church (Rochester, New York)|St. Michaels Church in Rochester. Still suffering from hallucinations of the victims, Megan visits the church and tries to question the pastor, but suffers another breakdown and is hospitalized. Megan flees from the hospital and takes refuge in Ledges home. There, she finds out that he used to work as the math teacher for the St. Michaels Church which finally reveals that he is the killer. Before she can act, he leaps from his wheelchair - having only pretended to be disabled - and attacks her. Ledge knocks her unconscious and drives to a remote spot near the Genesee River to kill her. Before Ledge can inject her with a sedative and dump her in the river, Megan breaks free and shoots him with his own gun. Ledge falls into the river just past a large waterfall - though its unclear if he is dead. Unsure whether Ledge is dead and confused by her surroundings, Megan is driven by the intense situation to another, longer breakdown.

Megan is hospitalized and kept under intensive psychiatric care. The final scenes of the film show Megan heavily sedated and strapped to a bed in a psychiatric ward.  There is no one else in the room, but in her state, she envisions the spirits of the victims waiting for her to return and seek justice for them.
 communion and exchanging glances with a potential victim (Gabby Servati).  It is unclear if these scenes of Ledge are actually occurring or are part of Megans psychosis.

A title card announces: "In 2006, police exhumed a firemans body and posthumously cleared him as a suspect. To date, the Alphabet Killer has not been found."

==Cast==
* Eliza Dushku as Megan Paige
* Cary Elwes as Kenneth Shine
* Timothy Hutton as Richard Ledge
* Tom Malloy as Steven Harper
* Michael Ironside as Nathan Norcoss
* Carl Lumbly as Dr. Ellis Parks
* Bill Moseley as Carl Tanner
* Tom Noonan as Captain Gullikson
* Melissa Leo as Kathy Walsh
* Martin Donovan as Jim Walsh
* Andrew Fiscella as Len Schafer

==Production==
  A Beautiful Mind and Zodiac (film)|Zodiac.   
 climactic scene was shot near the High Falls of Genesee River. 

==Reception==

===Release===
The film was screened at multiple film festivals, including European Film Market and Screamfest Horror Film Festival. The film had a limited theatrical release in the United States on Friday, November 7, 2008 when it was released in 2 theaters, only in New York.  As of December 14, 2008 the films domestic earnings are $29,784 while it grossed $4,191 in the foreign markets for a worldwide total of $33,975. 

===Critical response===
The film received negative reviews from critics, earning a 14% "Rotten" rating on Rotten Tomatoes.  Gary Goldstein of the Los Angeles Times commended the actors performances but thought the end was very unsatisfactory.  LA Weeklys Luke Thompson said the plot was quite predictable, but said that the presence of multiple supporting characters keeps viewers guessing, which made the film very interesting.  Jeannette Catsoulis of The New York Times praised Dushkus skills and Schmidts interest in "facts than in frights".  Andy Klein of Los Angeles CityBeat bemoaned the "preposterousness of the mysterys solution". 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 