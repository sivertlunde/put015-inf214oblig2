Die zärtlichen Verwandten
{{Infobox film
| name           =  The Tender Relatives
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Oswald
| producer       = Richard Oswald  
| writer         = Fritz Friedmann-Frederich   Ernst Neubach 
| narrator       = 
| starring       = Harald Paulsen   Charlotte Ander   Felix Bressart   Ralph Arthur Roberts
| music          = Felix Gunther   Willy Rosen
| editing        = 
| cinematography = Friedl Behn-Grund
| studio         = Richard-Oswald-Produktion
| distributor    = Atlas Film
| released       = 29 August 1930
| runtime        = 97 minutes
| country        = Germany
| language       = German 
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German comedy film directed by Richard Oswald and starring Harald Paulsen, Charlotte Ander and Felix Bressart.  The films art direction was overseen by Franz Schroedter.

==Cast==
* Harald Paulsen as Herr Linsemann 
* Charlotte Ander as Frau Linsemann 
* Felix Bressart as Uncle Emil 
* Ralph Arthur Roberts as Uncle Adolf 
* Paul Henckels as Herr Weber 
* Emmy Wyda as Frau Weber 
* Gustl Gstettenbaur as Webers son
* Harry Nestor as Cousin Wilhelm 
* Lotte Lorring as Wilhelms wife
* Hans Hermann Schaufuß as Herr Stempel 
* Adele Sandrock as Frau Stempel 
* Wilhelm Bendow as Uhl, a musician
* Camilla von Hollay as a child-minder
* Kurt Lilien as the factotum 
* Willy Rosen as a singer

==References==
 

==Bibliography==
* Kasten, Jürgen & Loacker, Armin. Richard Oswald: Kino zwischen Spektakel, Aufklärung und Unterhaltung. Verlag Filmarchiv Austria, 2005.

==External links==
* 

 

 
 
 
 
 
 
 


 