Maging Sino Ka Man (film)
{{Infobox film
| name             = Maging Sino Ka Man
| image            = 
| caption          = 
| director         = Eddie Rodriguez
| producer         = Vic Del Rosario Jr.
| writer           = Emmanuel H. Borlaza   Eddie Rodriguez
| based on         = 
| starring         = Sharon Cuneta   Robin Padilla
| cinematography   = Sergio Lobo
| editing          = Ike Jarlego Jr.
| music            = 
| studio           = Viva Films
| distributor      = Viva Films
| released         =  
| runtime          = 
| country          = Philippines
| language         = Filipino, English
| budget           = 
| gross            = 
}}

Maging Sino Ka Man is 1991 Filipino film starring Robin Padilla and Sharon Cuneta.

==Plot==
Famous singer, Monique (Sharon Cuenta) escapes her home after her mother is in disbelief that her stepfather tried to assault her. She goes into hiding by altering her looks and assumes the name, Digna. She goes living in a shelter for women and works various odd jobs. With her past coming back to haunt her again, Moniques fate is altered with a chance encounter with Carding (Robin Padilla), a small, petty thief who looks after six orphaned children.

==Cast==
*Sharon Cuneta as Monique / Digna
*Robin Padilla as Karding
*Edu Manzano as Gilbert
*Vina Morales as Loleng
*Rosemarie Gil as Beatrice
*Suzanne Gonzales as Cleo
*Dennis Padilla as Libag
*Ali Sotto as Tala
*Rez Cortez as Alex
*Carmi Martin as Hika
*Dick Israel as Pando
*Malou de Guzman as Letty
*Christopher Roxas as Tambok

==External links==
*  

 
 
 


 