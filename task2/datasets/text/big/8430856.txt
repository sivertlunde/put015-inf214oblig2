Fakin' da Funk
{{Infobox film|
  name     = Fakin Da Funk |
  image          =|
  image_size     =|
  caption  = Fakin Da Funk |
  writer         = Tim Chey |
  starring       = Pam Grier  Duane Martin Margaret Cho Dante Basco John Witherspoon Tone Loc Tichina Arnold Tatyana Ali with Nell Carter and Ernie Hudson |
  director       = Tim Chey |
  executive producers   = Tom Coleman, Harry Yoo, BJ Ahn |
  distributor       = USA Networks|
  released   = July 20, 1997 |
  runtime        = 89 min. |
  language = English |
  music          = Charlie Gross |
  awards         = |
}} Chinese son adopted by black parents who relocates to South Central Los Angeles. A second story involves Mai-Ling, an exchange student played by Margaret Cho, who by another twist, gets sent to the wrong hood. The film was written and directed by Tim Chey.

==Cast==
*Ernie Hudson as Joe Lee
*Pam Grier as Annabelle Lee
*Margaret Cho as May-Ling
*Dante Basco as Julian Lee
*Rashaan Nall as Perry Lee
*Duane Martin as Brandon John Witherspoon as Bill
*Tone Loc as Frog
*Bo Jackson as Reverend Cecil
*Tichina Arnold as Tracy Chris Spencer as Charlie
*Kelly Hu as Kwee-Me
*Rudy Ray Moore as Larry
*Reynaldo Rey as Earnest
*Nell Carter as Claire
*Martin Chow as Sushi Chef Marta Cunningham as Tracy
*Ron Yuan as Chinese Walter
*Dwanye Hackett as Willie
*Tatyana Ali as Karyn

==Reception==
Variety (magazine)|Variety called the film "an energetic, highly likable comedy". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 