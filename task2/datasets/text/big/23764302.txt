The Cannibals (1988 film)
{{Infobox film
| name           = The Cannibals
| image          = Os Canibais.jpg 
| caption        = 
| director       = Manoel de Oliveira
| producer       = Paulo Branco Paulo De Sousa
| writer         = Álvaro Carvalhal Manoel de Oliveira João Paes
| starring       = Luís Miguel Cintra
| music          = 
| cinematography = Mário Barroso
| editing        = Manoel de Oliveira
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
}}

The Cannibals ( ) is a 1988 Portuguese drama film directed by Manoel de Oliveira. It was entered into the 1988 Cannes Film Festival.   

==Cast==
* Luís Miguel Cintra - Viscount dAveleda
* Leonor Silveira - Margarida
* Diogo Dória - Don João
* Oliveira Lopes
* Pedro T. da Silva - Niccolo
* Joel Costa
* Rogério Samora - Peralta
* Rogério Vieira
* António Loja Neves
* Luís Madureira
* Teresa Côrte-Real
* José Manuel Mendes
* Cândido Ferreira
* Glória de Matos - (as Glória Matos)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 