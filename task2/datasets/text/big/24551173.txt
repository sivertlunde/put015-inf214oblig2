Invisible Wings
{{Infobox film
| name           = Invisible Wings
| image          = Invisible Wings Poster.jpg
| caption        = Film poster for Invisible Wings for the Chinese release
| director       = Feng Zhenzhi
| writer         = Feng Zhenzhi Zhao Huili Gu Tianyang
| starring       = Lei Qingyao
| cinematography = Jiang Xiujia Hai Tao
| distributor    = Joyous Media
| released       =  
| runtime        = 93 minutes
| country        = China
| language       = Mandarin
}} amputee Lei Qingyao.  Lei plays Zhi Hua in a sobering tale based on her own life.

==Plot==
The film features the determination of a teenage girl, Zhi Hua (Lei Qingyao), who loses both her arms due to an electric shock while recovering a kite from the electric wires. Distraught, the Zhi Hua attempts to end her life but eventually finds a new will to live. She goes on to train her feet to do everything, such as writing, cooking, changing clothes and even making kites. She aspires to become a doctor but is denied admission and this makes her even more determined. Zhi Hua begins to train to become a swimming champion, beating all Olympic-level participants. Zhihua eventually goes on to become a national champion in swimming.

==Reception== Huabiao award for her performance  and Best Newcomer Hundred Flowers Awards.

==References==
 

==External links==
*  
*   from distributor Joyous Media

 
 
 
 
 
 

 