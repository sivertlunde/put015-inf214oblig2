Road to Perdition
 
{{Infobox film
| name           = Road to Perdition
| image          = Road to Perdition Film Poster.jpg
| caption        = Theatrical release poster
| director       = Sam Mendes
| producer       = {{Plain list|
* Richard D. Zanuck
* Dean Zanuck
* Sam Mendes
}}
| screenplay     = David Self
| based on       =  
| starring       = {{Plain list|
* Tom Hanks
* Paul Newman
* Jude Law
* Jennifer Jason Leigh
* Stanley Tucci
* Daniel Craig
 
}}
| music          = Thomas Newman
| cinematography = Conrad L. Hall
| editing        = Jill Bilcock
| studio         = The Zanuck Company
| distributor    = {{Plain list|
* DreamWorks Pictures  
* 20th Century Fox  
}}
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = $80 million
| gross          = $181 million   
}} graphic novel of the same name by Max Allan Collins. The film stars Tom Hanks, Paul Newman (in his final live-action film role), Jude Law, and Daniel Craig. The plot takes place in 1931, during the Great Depression, following a mob enforcer and his son as they seek vengeance against a mobster who murdered the rest of their family.
 American Beauty, pursued a story that had minimal dialogue and conveyed emotion in the imagery. Cinematographer Conrad Hall took advantage of the environment to create symbolism for the film, for which he won several awards, including a posthumous Academy Award for Best Cinematography. The film explores several themes, including the consequence of violence and father-son relationships.

The film was released on July 12, 2002, and eventually grossed over $180 million worldwide.  The cinematography, setting, and the lead performances by Hanks and Newman were well received by critics. A home media release debuted on February 25, 2003.

==Plot==
Michael Sullivan Sr. (Hanks) is an enforcer for Irish mob boss John Rooney (Newman) in Rock Island, Illinois, during the Great Depression. Rooney raised the orphan Sullivan and loves him more than his own biological son, the unstable Connor (Craig). Connor snaps and kills disgruntled associate Finn McGovern (Ciaran Hinds) when meeting him with Sullivan, resulting in Sullivan gunning down McGoverns men. Sullivans twelve-year-old son Michael Sullivan, Jr, had hidden in his fathers car and witnesses the event. Despite Sullivan swearing his son to secrecy and Rooney pressuring Connor to apologize for the reckless action, Connor murders Sullivans wife Annie and younger son Peter, mistaking him for Sullivan, Jr. He then sends Sullivan to an ambush at a speakeasy but Sullivan realizes and escapes to Chicago with his son to deliver a proposition to Al Capone: in exchange for Sullivans work, Capone would give up the location of Connor, who has gone into hiding.

Capones Underboss Frank Nitti (Tucci) rejects Sullivans proposals, before informing Rooney of the meeting. Rooney reluctantly allows Nitti to dispatch assassin Harlen Maguire (Law), who is also a crime scene photographer, to kill Sullivan. Maguire tracks him and his son to a roadside diner, but fails to kill Sullivan. Realizing Maguires intentions, Sullivan punctures Maguires car tire before escaping.
 laundered money, hoping to trade it for Connor while teaching Michael to drive their getaway car. Sullivan is impeded when the mob withdraws its money, so he visits Capones accountant Alexander Rance (Baker) at his hotel. The encounter is a set-up, with Rance stalling Sullivan until Maguire enters with a shotgun. In the ensuing crossfire, Rance is killed by Maguires shotgun, Maguire is injured by flying glass shards, and Sullivan escapes with the ledgers. As he flees, Maguire shoots Sullivan in the shoulder.
 embezzling from his father for years, using the names of dead men. As the Sullivans depart, they give the couple much of the stolen money. Sullivan confronts Rooney with the information while they attend Catholic Mass|Mass. Although Rooney already knew about the embezzlement and that Connor was likely to die by Sullivans hand or the mob, he refuses to give up his son. He encourages Sullivan to leave with his son, while mourning his departure.

Later one night, cloaked by darkness and rain, Sullivan dispatches Rooneys entire entourage with his Thompson submachine gun and walks directly up to Rooney. As Rooney mutters that he is glad his killer is Sullivan, an emotionally reluctant Sullivan pulls the trigger. Seeing no further reason to protect Connor, Nitti reveals his location to Sullivan, after making the latter promise to end the feud. Sullivan goes to the hotel where Connor is hiding and kills him.

Sullivan drives his son to stay at his Aunt Saras beach house in Perdition, a town on the shore of Lake Michigan. However, he is ambushed and shot by a disfigured Maguire. As Maguire prepares to photograph the dying Sullivan, Michael Jr. appears and points a gun at Maguire, but cannot muster the will to fire. Sullivan pulls out his gun and kills Maguire, before dying in his sons arms. Mourning his fathers death, Michael Jr. returns to the elderly farm couple to live with them. While growing up, Michael Jr. reflects that his fathers only fear was that his son would become like him. When asked if Sullivan was a good or bad man, he replies "he was my father".

==Cast==
* Tom Hanks as Michael Sullivan, Sr., a top-notch hitman who works for John Rooney. Hanks was sent a copy of the graphic novel Road to Perdition by Steven Spielberg while he was filming Cast Away (2000). Initially too busy to make sense of the story, he later received David Selfs adapted screenplay, to which he became attached. Hanks, a father to four children, described Michael Sullivans role, "I just got this guy. If youre a man, and youve got offspring ... emotionally, its devastating."  getaway driver, Hoechlin was trained by a driving instructor. 
* Paul Newman as John Rooney, a crime boss who treats Sullivan as a surrogate son. Newman was unanimously the first choice for the role.  The actor prepared by requesting Frank McCourt, the Irish-American author of Angelas Ashes, to record a tape of his voice. 
* Jude Law as Harlen Maguire, a crime scene photographer who moonlights as an assassin. Self, who created this character (who did not exist in the graphic novel), explained, "He gets so jaded from exposure to this world, he steps over the line from being the storyteller to being the story maker."    To capture the "seedy countenance" of the character, Law was given a sallow skin tone that reflected the wear from working in a darkroom. Laws teeth also received a lower gumline and had a rotted look.  He was also given a weak, thinning hairline. Maguire carries a Speed Graphic camera that serves as dual symbolism for his acts of murder. Maguires apartment also displays a collection of photographs of dead bodies, some of them actual police stills from the 1930s. 
* Daniel Craig as Connor Rooney, the unstable, violent son of John Rooney. He’s deeply jealous of; and despises, the intrusion of the outsider (Sullivan), into an exclusive club (The Sons of John Rooney), which both John Rooney and Michael Sullivan together wrongly believe, can be joined as a result of a long acquaintanceship, that grows into friendship.
* Stanley Tucci as Frank Nitti, the underboss for the Al Capone Outfit. Tucci had previously avoided roles in gangster films, believing that Hollywood stereotyped all Italian-Americans as gangsters. However, attracted by the prospect of working with Mendes and his crew, the actor accepted the role of Nitti, a real-life Mob boss from Chicago. 
* Jennifer Jason Leigh as Annie Sullivan, the wife of Michael Sullivan, Sr.
* Liam Aiken as Peter Sullivan, the doomed younger son of Michael Sullivan, Sr.
* Dylan Baker as Alexander Rance, an accountant who holds the ledgers for the Rooney crime syndicate.
* Ciarán Hinds as Finn McGovern, the man whose murder by Connor Rooney is witnessed by Sullivans son.
* Anthony LaPaglia as Al Capone, the notorious crime boss. This character was filmed for a single scene, which was omitted from the final cut,  and can be found in the DVDs deleted scenes.  Mendes believed that Capone was more menacing as an unseen presence. Actor Alfred Molina was approached to portray Capone, but Molina was forced to turn the role down due to scheduling conflicts with Frida (2002). 

== Production == Road to Rules of Engagement (2000). The Zanucks agreed on the storys prospect and sent it to director-producer Steven Spielberg. Shortly afterward, Spielberg set up the project at his studio DreamWorks, though he did not pursue direction of the film due to his full slate.   
 American Beauty A Beautiful The Shipping News,    and The Lookout. DreamWorks sent Mendes Road to Perdition as a prospect, and Mendes was attracted to the story, considering it "narratively very simple, but thematically very complex".  One theme that he saw in the story was of the parents world that is inaccessible to their children. Mendes considered the storys theme to be about how children deal with violence, and whether exposure to violence would render children violent themselves. Mendes described the script as having "no moral absolutes", a factor that appealed to the director.   

=== Writing ===
Spielberg first contacted screenwriter David Self to adapt the story into a feature film.  Self wrote an initial draft that remained close to the source material and retained most of its dialogue. The screenplay was then rewritten by uncredited writers, distancing the script from the graphic novel and leaving the core elements of the story.  Some of the harsher aspects of the story were toned down as the script became more streamlined; for example, in some early drafts of the screenplay, Sullivan became an alcoholic, but this element was ultimately absent from the final version. 

The story itself is deeply informed by the Lone Wolf and Cub manga series. Novelist Max Allan Collins acknowledged the influence of Lone Wolf and Cub on his graphic novel Road to Perdition in an interview to the BBC, declaring that "Road To Perdition is an unabashed homage to Lone Wolf And Cub". 
 John Looney and his son Connor were changed to Rooney, and the surname of Tom Hanks character and his family was streamlined from the original OSullivan to simply Sullivan. One significant addition to the script was the creation of Maguire to provide a persistent element of pursuit to the Sullivans departure from the old world. 

Hanks and cinematographer Conrad Hall requested Mendes to limit violence in the film to meaningful acts, rather than gratuitous carnage. Hanks character, Michael Sullivan, is known as "The Angel of Death" in the graphic novel and invokes fear in those around him, but his infamy is downplayed in the film.  Mendes, who described the graphic novel as "much more pulpy", sought to reduce the graphic novels background to its essence, seeking the "nonverbal simplicity" of films like Once Upon a Time in America (1984), Pat Garrett and Billy the Kid (1973),  and films by Akira Kurosawa that lack dialogue.  Duplicate language in characters confrontations in Road to Perdition was trimmed to the absolute minimum.    Mendes described Road to Perdition as a "poetic, elegiac story, in which the pictures tell the story".  An example of one such unspoken scene in the film was the piano duet between Rooney and Michael Sr., intended to convey their relationship without words.  In the final 20 minutes of Road to Perdition, the script was written to have only six lines of dialogue. 

The author of the Perdition graphic novel, Max Allan Collins, originally wanted to write the adapted screenplay for the feature film, but was not given the opportunity.  He chose to stay out of the scripting process out of respect for the different style of writing for a different medium, though he served as a consultant in the process. Collins praised the addition of Maguire and considered the minimalist use of dialogue to be appropriate.    The author also applauded the films version of Rooney as "more overtly a father figure" to Sullivan. 

Collins opposed the profanity in the script, as the vulgar language did not fit his vision of the 1930s.  He also contested the path of Sullivans son in the film. In the graphic novel, the son kills once, and in the film, he does not kill anyone. Collins also disagreed with the narration technique of the film. In the novel, the son narrates the story as an adult, becoming a priest, while in the film, he narrates while still a young boy. 

===Filming=== Pullman as well as the Chicago suburb of Geneva, Illinois|Geneva, Illinois. The Armory, the states largest location mainstay which houses the Illinois State National Guard, was provided to the studio by the Illinois State Film Commission. Sets were built inside the Armory, including interiors of the Sullivan familys home and the Rooney mansion. The availability of an inside location provided the crew complete control over the lighting environment, which was established with the rigging of scaffoldings.   
  Super 35 format. 

The director filmed exterior scenes in Illinois in the winter and the spring of 2001, using real weather conditions such as snow, rain, and mud for the scenes. Mendes considered the usage of bleak weather conditions and the intended coldness of Gassners exterior locations to define the characters emotional states. Pullman became a key location to reflect this theme, having several settings, including the towns historic Florence Hotel, easily redressed by the crew for the film.  Filming concluded in June 2001. 

===Cinematography===
 ]] To establish the lighting of scenes in Road to Perdition, Mendes drew from the paintings of Edward Hopper as a source of inspiration, particularly Hoppers New York Movie (1939). Mendes and cinematographer Conrad Hall sought to convey similar atmospheric lighting for the films scenes, applying a "less is more" mantra.  Hall also shot wide open scenes that retained one point in the depth of field sharply focused. Hall considered the technique to provide an emotional dimension to the scenes. The cinematographer also used unconventional techniques and materials to create unique lighting effects. One of Halls methods was to use black silk in daylight exterior scenes to filter the light enough to create an in-shade look. 

Hall purposely distanced the camera from Hanks character, Michael Sullivan, Sr., at the beginning of the film to establish the perspective of Sullivans son, who is unaware of his fathers true nature.  Hanks character was filmed as partially obscured and seen through doorways, and his entrances and exits took place in shadows. A wide lens was used to maintain a distance from the character. 

Shots in the film were drawn directly from panels in the graphic novel, illustrated by Richard Piers Rayner. An instance of the direct influence is the scene in which Michael, Jr. looks up at the Chicago skyline from the vehicle, with the skyline reflected in the vehicles glass. 

A seamless 40-second driving scene, in which Michael Sullivan and his son travel into Chicago from the countryside, was aided by visual effects. The live-action part of the scene was filmed at LaSalle Street, and due to the lack of scenery for part of the drive down LaSalle Street, the background of Balbo Drive was included with the use of visual effects. 

==Themes==

===Consequences of violence===
 
The films title, Road to Perdition, is both Michael Sullivan and his sons destination town and a euphemism for Hell, a road that Sullivan desires to prevent his son from traveling. Sullivan, who chooses his violent path early on in life, considers himself irredeemable and seeks to save his son from a similar destiny|fate. Said Mendes, "  is in a battle for the soul of his son. Can a man who has led a bad life achieve redemption through his child?" "Taking the Road". Production Notes. Road to Perdition (2002). Retrieved 2007-06-06.  Hanks described Sullivan as a man who achieved a comfortable status through violent means, whose likely repercussions he ignored. Sullivan is a good father and husband, but also has a job that requires him to  be a violent killer. The film explores this paradoxical dichotomy. When Sullivan is faced with the consequences, Hanks says, "At the moment were dropped into the story, it is literally the last day of that false perspective."  . Production Notes. Road to Perdition (2002). Retrieved 2007-06-06.  To keep Sullivan from justifying his violent actions in the film, Mendes omitted scenes in the final cut that had Sullivan explaining his background to his son. 

In the film, most of the numerous acts of violence are committed off-screen. The violent acts were also designed to be quick, reflecting the actual speed of violence in the real world. The focus was not on the direct victims of the perpetuated violence, but the impact of violence on the perpetrators or witnesses to the act. 

===Fathers and sons===
The film also explores father-son relationships, not only between Michael Sullivan and his son, but between Sullivan and his boss, John Rooney, and between Rooney and Connor. Sullivan simultaneously idolizes and fears Rooney, and Sullivans son feels the same about his own father. Rooneys son, Connor, has none of Sullivans redeeming qualities, and Rooney is conflicted about whom to protect: his biological son or his surrogate son. Connor is jealous of his fathers relationship with Sullivan, which fuels his actions, ultimately causing a domino effect that drives the film. 

Because Sullivan shields his background from his son, his attempt to preserve the father-son relationship is actually harmful. Tragedy brings Sullivan and his son together.    Sullivan escapes from the old world with his son, and the boy finds an opportunity to strengthen the relationship with his father. Tyler Hoechlin, who portrayed Michael, Jr., explained, "His dad starts to realize that Michael is all he has now and how much hes been missing. I think the journey is of a father and son getting to know each other, and also finding out who they themselves are." 

===Water=== motif in the film. It was developed after researching the wake scene at the beginning of the film informed the director that corpses were kept on ice in the 1920s to keep bodies from decomposing. The notion was interwoven into the film, which linked the presence of water with death.  . Production Notes. Road to Perdition (2002). Retrieved 2007-06-06.  Mendes reflected on the theme, "The linking of water with death... speaks of the mutability of water and links it to the uncontrollability of fate. These are things that humans cant control." 

==Release==
When filming concluded in June 2001,  the studio intended a United States release for the following Christmas. But by September 2001, Mendes requested more time. It was rescheduled for release on July 12, 2002, an unconventional move that placed the drama among the action-oriented summer films. 

===Box office===
Road to Perdition opened in 1,798 theaters over its opening weekend, competing against several other new releases including  , and  . The film grossing $22,079,481, placing second to Men in Black II, which was in its second week of release.    It eventually grossed $104,454,762 in the United States and $76,546,716 in other territories for a worldwide total of $181,001,478. 

===Critical response  ===
The film received  positive reviews and acclaim from critics, with the lead performances of Hanks and Newman being praised. Reviewer James Berardinelli, on his own ReelViews web site, praised Road to Perdition for its atmosphere and visuals, but he considered an emotional attachment to be lacking except for Sullivans son. 
Roger Ebert of the Chicago Sun-Times praised Halls cinematography and the thematic use of water. He, too, felt an emotional detachment from the characters, saying, "I knew I admired it, but I didnt know if I liked it... It is cold and holds us outside." 

Eleanor Ringel Gillespie of The Atlanta Journal-Constitution enjoyed the films cinematography and Depression-era setting, as well as the performances of Hanks and Newman. Gillespie expressed the wish that the film lasted a little longer to explore its emotional core further.  Eric Harrison of the Houston Chronicle considered Road to Perdition "the most brilliant work in this   genre" since the uncut Once Upon a Time in America (1984). Harrison considered Selfs script "so finely honed that the story can change directions in a heartbeat." 

Kirk Honeycutt of  s "evocative" score. 

  described the film as "grim yet soppy." He added: "The action is stilted and the tabloid energy embalmed."  Stephen Hunter of The Washington Post thought that the script lost its path when Sullivan and his son fled their old life. 

Review aggregation website Rotten Tomatoes gives the film a score of 81% based on reviews from 210 critics, with an average score of 7.5/10.  Metacritic, which assigns a weighted average score out of 100 from reviews by mainstream critics, gave a film rating of 72/100 based on 36&nbsp;reviews. 

===Accolades  === John Pritchett), and Best Sound Editing (Scott Hecker). The sole award went, posthumously, to Hall for Cinematography. 
 BAFTA Awards Outstanding Achievement in Cinematography in Theatrical Releases.  In April 2006, Empire (magazine)|Empire recognized Road to Perdition as number six in its list of the top 20 comic book films. 

===Home media===
Max Allan Collins, who authored the graphic novel, was hired to write the novelization for the film adaptation. Collins initially turned in a draft that contained 90,000 words, but the licensing at DreamWorks required the author to use only the dialogue from the film and no additional dialogue. Collins reluctantly edited the novelization down to 50,000 words and later said he regretted taking on the task. 
 DTS soundtrack. Instead, the DVD included a Dolby Digital 5.1 soundtrack.  A special edition DVD containing both DTS and Dolby Digital 5.1 soundtracks was also released, excluding the "Making of" documentary to fit both soundtracks. 

Road to Perdition was released on Blu-ray Disc on August 3, 2010, featuring a widescreen transfer, a DTS-HD Master Audio 5.1 soundtrack, and all of the features from the DVD release. 

==References==
 

==Further reading==
* 

==External links==
* 
* 
* 
* 
*  at American Cinematographer
 

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 