Vi arme syndere
 
{{Infobox film
| name           = Vi arme syndere
| image          = Vi arme syndere.jpg
| caption        =
| director       = Erik Balling Ole Palsbo
| producer       = John Hilbert
| writer         = Fleming Lynge Sigfrid Siwertz
| starring       = Ib Schønberg
| music          =
| cinematography = Verner Jensen
| editing        = John Hilbard
| distributor    = Nordisk Film
| released       =  
| runtime        = 105 minutes
| country        = Denmark
| language       = Danish
| budget         =
}}

Vi arme syndere ( ) is a 1952 Danish comedy film directed by Erik Balling and Ole Palsbo.

==Cast==
* Ib Schønberg - Hannibal Svane
* Johannes Meyer - Gulbæk
* Ellen Gottschalch - Baronessen
* Astrid Villaume - Ulla Blom
* Bendt Rothe - Maler Astrup
* Gunnar Lauring - Kurt Lönning
* Freddy Koch - Baron Willy von Ehrenburg
* Knud Heglund - Truelsen
* Minna Jørgensen - Fru. Riise
* Berit Erbe - Tove
* Lise Ringheim - Jenny
* Per Buckhøj
* Einar Juhl
* Thorkil Lauritzen Henry Nielsen
* Karl Stegger
* Keld Markuslund
* Alma Olander Dam Willumsen (as Olander Dam Willumsen)
* Dirch Passer

==External links==
* 

 
 
 
 
 
 
 


 
 