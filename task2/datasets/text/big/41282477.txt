The Voorman Problem
{{Infobox film
| name = The Voorman Problem
| director = Mark Gill
| producer = Baldwin Li   Lee Thomas
| based on =  
| writer = Mark Gill Baldwin Li
| starring = Martin Freeman Tom Hollander
| released =  
| runtime = 13 minutes
| country = United Kingdom
| language = English
}} Mark Gill, David Mitchell. BAFTA Award.  The film stars Martin Freeman and Tom Hollander. 

==Plot==

Dr. Williams is hired by Governor Bentley to work as a prison psychiatrist after "The War in the East" has produced a doctor shortage. Williams is informed about the Voorman problem; a prisoner named Voorman is convinced that he is a god and has convinced the rest of the prisoners who spend all day chanting in worship. It is unclear what Voormans crime is due to a computer malfunction.

Williams interviews a straitjacketed Voorman in a locked room in the prison. Voorman calmly explains that he is a god, and that he created the world exactly nine days ago. When the Doctor objects, Voorman suggests a test of his powers. He will eliminate Belgium as proof he is a god.
 Walloon Lagoon."

Back at the prison, Williams is baffled by the lack of any evidence of Belgium, but refuses to believe in Voormans divinity. Voorman expresses his exhaustion with being a god and suggests the two switch places. In a flash the two have switched places, Voorman dressed as the well dressed doctor, and Williams disheveled and in a straitjacket. Williams calls for the guards and Voorman starts to leave stating that it is no use. As he leaves, Voorman advises Williams to "keep an eye on North Korea."

Voorman leaves the room as the sounds of the prisoners chanting get louder.

==Cast==
*Martin Freeman as Dr. Williams
*Tom Hollander as Voorman
*Elisabeth Gray as Mrs. Williams
*Simon Griffiths as Governor Bentley

==References==
 

==External links==
*  
 
 
 

 
 
 
 

 