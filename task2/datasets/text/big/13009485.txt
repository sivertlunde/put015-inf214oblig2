Anecdote (film)
{{Infobox film name = Anecdote image =   image_size = caption =  director = Yefim Abramov Nizami Musayev producer =  writer = Rovshan Aghayev Yfim Abramov Nizami Musayev narrator =  starring = Rasim Balayev Jeyhun Mirzayev Yashar Nuri music =  cinematography =  editing = Ramiz Fataliyev Nusret Kesemenli distributor =  studio = Azerbaijanfilm released = 1989 runtime = 136.22 min country = Soviet Union language = Russian
|budget =  gross = 
}} Azerbaijani film Soviet management system in Azerbaijan SSR at the end of 1980s and about the decadence and corruption of the Soviet bureaucracy.

<!-- ==Plot==
 -->

==Reception==
* 

==Cast==
* Rasim Balayev - Rahimov
* Jeyhun Mirzayev - Mammadov
* Yashar Nuri - Mammad
* Mukhtar Maniyev - Kerimov
* Khuraman Hajıyeva - Khalilova
* Yelena Kostina - Alya
* Alexander Sharovski - Prosecutor
* Loghman Kerimov - The Deaf
* Latifa Aliyeva - Mother
* Lala Baghirova - Rimma
* Azhdar Hamidov - Ahmed
* Rafig Aliyev - Komsomol leader
* Arif Kerimov - Murtuzov
* Nizami Musayev - Dunyaminov
* Sadig Huseynov - Retired Colonel
* Tarlan Babayev - Dancer
* Ogtay Suleymanov - Factor worker
* Sahib Guluzade - Head of the Society
* Alla Sannikova - A dwarf lady
* Igor Sannikov - A dwarf man
* Alim Aslanov - Colonel Nazarov
* Dadash Kazimov - Elevator Man
* Faig Babayev - Factory Director

==See also==
*Azerbaijani films of the 1980s

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 
 