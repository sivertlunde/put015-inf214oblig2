La diosa de Tahití
{{Infobox film
| name           = La diosa de Tahití (Los chacales de la Isla Verde)
| image          = 
| caption        =
| director       = Juan Orol
| producer       = Juan Orol
| writer         = Julián Cisneros Icaro Cisneros
| starring       = Rosa Carmina Arturo Martínez Marco de Carlo
| music          = Antonio Rosado
| cinematography = Rosalío Solano
| editing        = 
| distributor    = España Sono Films
| released       = February 11, 1953 (México) 
| runtime        = 99 min 
| country        = Mexico
| language       = Spanish
| budget         =
| gross          =
}}
La diosa de Tahití (The Goddess of Tahiti) also known as Los chacales de la Isla Verde (The Jackals of Isla Verde), is a Mexican drama film directed by Juan Orol. It was released in 1953 and starring Rosa Carmina and Arturo Martínez.

==Plot==
Paula (Rosa Carmina), the biggest star of a cabaret located on some exotic island of the South Seas, harbor a fugitive who starts a dangerous adventure that compromises her safety, in a bitter struggle between smugglers and military.

==Cast==
* Rosa Carmina ... Paula
* Arturo Martínez ... Silvestre
* Marco de Carlo ... Alfredo
* Gilberto González ... Pancho

==Reviews==
Delirious romantic melodrama directed by Juan Orol, which include the pleasant musical numbers danced by the rumbera Rosa Carmina and the recreation exotic environments in which the filmmaker puts his history. 

==References==
 

==External links==
*  

 
 
 
 
 
 