Between Yesterday and Tomorrow (film)
{{Infobox film
| name           = Between Yesterday and Tomorrow 
| image          = 
| image_size     = 
| caption        = 
| director       = Harald Braun 
| producer       = Walter Bolz
| writer         = Harald Braun   Jacob Geis   Herbert Witt
| narrator       = 
| starring       = Hildegard Knef   Winnie Markus   Sybille Schmitz   Willy Birgel
| music          = Werner Eisbrenner
| editing        = Adolf Schlyssleder
| cinematography = Günther Anders (cinematographer)|Günther Anders
| studio         = Bavaria Film
| distributor    = Schorcht Filmverleih 
| released       = 11 December 1947
| runtime        = 107 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 German drama film directed by Harald Braun and starring Hildegard Knef, Winnie Markus and Sybille Schmitz. In Post-war Germany a group of former guests return to a luxurious Munich hotel where they are haunted by memories of their past interaction with Nelly Dreifuss, a Jewish woman who had died during the Nazi era. It was part of both the cycle of rubble films and subgenre of Hotel films. As with many other German rubble films, it examines issues of collective guilt and future rebuilding.

==Cast==
* Hildegard Knef as Das Mädchen Kat 
* Winnie Markus as Annette Rodenwald 
* Sybille Schmitz as Nelly Dreifuss 
* Willy Birgel as Alexander Corty 
* Viktor de Kowa as Michael Rott 
* Viktor Staal as Rolf Ebeling 
* Carsta Löck as Frau Gertie 
* Adolf Gondrell as Dr. Weber 
* Walter Kiaulehn as Intendant Kesser 
* Erich Ponto as Professor von Walther 
* Erhard Siedel as Herr Hummel 
* Otto Wernicke as Ministerialdirektor Trunk

==Bibliography==
* Shandley, Robert R. Rubble Films: German Cinema in the Shadow of the Third Reich. Temple University Press, 2001.

==External links==
* 

 
 
 
 
 
 
 


 