She Loves Me Not (1918 film)
 
{{Infobox film
| name           = She Loves Me Not
| image          = 
| caption        = 
| director       = 
| producer       = Hal Roach
| writer         = 
| starring       = Harold Lloyd
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States Silent English intertitles
| budget         = 
}} short comedy film featuring Harold Lloyd. A print of the film survive at the film archive of the British Film Institute.   

==Cast==
* Harold Lloyd 
* Snub Pollard 
* Bebe Daniels 
* Sammy Brooks
* Billy Fay
* William Gillespie
* Estelle Harrison
* Lew Harvey
* Bud Jamison
* Margaret Joslin
* Dee Lampton
* Oscar Larson
* Marie Mosquini
* James Parrott
* Dorothea Wolbert
* Noah Young

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 