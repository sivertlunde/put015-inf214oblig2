Seven Thunders (film)
{{Infobox Film
| name           = Seven Thunders
| image_size     = 
| image	=	Seven Thunders FilmPoster.jpeg
| caption        = 
| director       = Hugo Fregonese
| producer       = Daniel M. Angel
| writer         = Rupert Croft-Cooke (novel) John Baines
| narrator       =  Tony Wright Anna Gaylor
| music          = 
| cinematography = 
| editing        = 
| studio         = Rank Organisation
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Second World War film about two British escaped prisoners of war.

==Plot== Tony Wright), escape from separate prison camps and are paired together by the underground. They are taken to Marseille by a fishing boat captain to wait for the right opportunity to try for England. While they wait, local woman Lise (Anna Gaylor) falls in love with Dave. He is attracted to her, but is engaged.
 Vichy official Bourdin (George Coulouris) become his latest victims.

Given an ultimatum by his displeased superiors to do something about the French Resistance, the German military commander of the city decides to evacuate and demolish the crime-infested Old Quarter, where Dave and Jim are hiding, with only two hours warning. Dave wants to wait for the fishing captain, but Jim chooses to try Dr. Martout. When the captain does show up, Dave goes to fetch Jim, barely saving him from becoming Martouts 100th victim. Martout escapes in his car, but in his haste, crashes and is killed. With buildings blowing up right and left, the pair make their way, with Lises help, to the boat and freedom. Aboard the boat, Dave and Lise embrace.

==Cast==
*Stephen Boyd as Dave
*James Robertson Justice as Dr. Martout
*Kathleen Harrison as Mme. Abou, a helpful Englishwoman married to a Frenchman Tony Wright as Jim
*Anna Gaylor as Lise
*Eugene Deckers as Emile Blanchard
*Rosalie Crutchley as Therese Blanchard, Emiles wife. She tries to commit suicide when her young daughter is killed by mistake.
*Katherine Kath as Mme. Parfait
*James Kenney as Eric Triebel, a young German corporal who accidentally shoots the Blanchards child 
*Anton Diffring as Colonel Trautman Martin Miller as Schlip
*George Coulouris as Bourdin
*Carl Duering as Major Grautner
*Edric Connor as Abou

==External links==
* 

 
 
 
 
 
 

 