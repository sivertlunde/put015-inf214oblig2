The Hoosier Schoolmaster (1935 film)
{{Infobox film
| name           = The Hoosier Schoolmaster
| image          =
| image_size     =
| caption        =
| director       = Lewis D. Collins
| producer       = Paul Malvern
| writer         = Edward Eggleston (novel) Charles Logue (writer)
| narrator       =
| starring       =
| music          =
| cinematography = Harry Neumann
| editing        = Carl Pierson
| distributor    =
| released       = 15 May 1935
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Hoosier Schoolmaster is a 1935 American film directed by Lewis D. Collins.

==Cast== Norman Foster as Ralph Hartsook
*Charlotte Henry as Hannah
*Dorothy Libaire as Martha Means
*Tommy Bupp as Shocky
*Otis Harlan as Squire Hawkins
*Fred Kohler Jr. as Bud Larkin
*William V. Mong as Jake Means Russell Simpson as Doc Small
*Joseph E. Bernard as Randall
*Wallace Reid Jr. as Hank
*George Gabby Hayes as Pearson
*Sarah Padden as Sarah

==External links==
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 