Native Land
{{Infobox film
| name           = Native Land
| image          =
| image size     =
| caption        =
| director       = Leo Hurwitz Paul Strand
| producer       = Leo Hurwitz
| writer         = Leo Hurwitz Ben Maddow
| starring       = Paul Robeson (Narrator/Vocalist) Fred Johnson
| music          = Marc Blitzstein
| cinematography = Paul Strand
| editing        = Lionel Berman Leo Hurwitz Bob Stebbins	
| distributor    =
| released       = 11 May 1942
| runtime        = 79 minutes
| country        = United States
| language       = English
}} 1942 documentary film   directed by Leo Hurwitz and Paul Strand. 

A combination of a documentary format and staged reenactments, the film depicted the struggle of trade unions against union-busting corporations, their spies and contractors. It was based on the 1938 report of the La Follette Committees  investigation of the repression of labor organizing.

Famous African-American singer, actor and activist Paul Robeson participated as an off-screen narrator and vocalist.

==Cast==
* Paul Robeson as Narrator and vocalist (voice)
* Fred Johnson as Fred Hill, a farmer
* Mary George as Hills wife
* John Rennick as Hills son
* Amelia Romano as Window scrubber
* Houseley Stevenson as White sharecropper
* Louis Grant as Black sharecropper
* James Hanney as Mack, Union president
* Howard Da Silva as Jim, an informer Art Smith as Harry Carlyle
* John Marley as Thug with crowbar

==Restoration and re-release==
A restored version of the film was released in 2011. The film was restored by the UCLA Film & Television Archive, funded by the Packard Humanities Institute.    

The new print was made “from the original 35mm nitrate picture negative, a 35mm safety duplicate negative, and a 35mm safety up-and-down track negative.” 

The restoration premiered at the UCLA Festival of Preservation on March 26, 2011  and was screened at other North American cities in 2011 including  Vancouver. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 