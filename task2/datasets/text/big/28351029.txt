Brahmasthram (2010 film)
{{Infobox film
| name           = Brahmasthram
| image          = 
| image_size     = 
| caption        = 
| director       = V. Somanath
| producer       = V. Somanath Ambili Nedumkunnam
| writer         = 
| narrator       = 
| starring       = Saiju Kurup Maidhili
| music          = Vijay Krishna
| cinematography = R.H.Ashok 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Brahmastram is a 2010 Malayalam film directed by V.Somanath starring Saiju Kurup and Maidhili.

== Plot ==
Brahmasthram tells the story of a boatman, whose only daughter Sindhu is the victim of ragging and rape by some rich recalcitrant students in medical college. No one supports Sindhu as the culprits are from very rich families. Due to pressure from Sindhu and her father, Superintendent of Police Indrajith is appointed to investigate the case. How he wins the case in court forms the rest of the film. 

== Cast ==
* Saiju Kurup
* Maidhili
* Jagathy Sreekumar
* Rajan P. Dev Vijayaraghavan
* Suraj Venjaramood
* Dhanya Madhavan
* Kanakalatha Abu Salim

== Soundtrack ==
{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| title1 = Sandhyayude | extra1 = K. J. Yesudas | lyrics1 = Vayalar Sarath Chandra Varma
| title2 = Priyasakhi | extra2 = M. Jayachandran, Sujatha Mohan | lyrics2 = K S Hariharan
| title3 = Kalivakku | extra3 = Vidhu Prathap | lyrics3 = K S Hariharan
| title4 = Pennalle | extra4 = Jyothis Krishna, Chorus | lyrics4 = Anil Vadanamkurissi
| title5 = Sandhyayude | extra5 = Swetha | lyrics5 = Vayalar Sarath Chandra Varma
}}

== References ==
 

==External links==
* http://www.nowrunning.com/movie/5105/malayalam/brahmasthram/index.htm
* http://popcorn.oneindia.in/title/1522/brahmastram.html
* http://www.mallumovies.org/movie/brahmasthram

 
 
 


 