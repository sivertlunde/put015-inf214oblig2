The Honor of the Mounted
{{Infobox film
| name           = The Honor of the Mounted
| image          = 
| caption        = 
| director       = Allan Dwan
| producer       = 
| writer         = Arthur Rosson Pauline Bush Lon Chaney
| music          = 
| cinematography = 
| editing        =  Universal Film Manufacturing Company
| released       =  
| runtime        = 30 minutes
| country        = United States 
| language       = Silent English intertitles
| budget         = 
}}
 silent Short short drama Pauline Bush, Lon Chaney.    The film is now considered lost film|lost. 

==Cast==
* Murdock MacQuarrie as Mountie Pauline Bush as Marie Laquox Lon Chaney as Jacques Laquox
* James Neill as Post Commandant
* Gertrude Short

==Production notes== Mount Lowe, California over a two day period. Director Allan Dwan also shot Bloodhounds of the North (which also starred Murdock MacQuarrie, Pauline Bush, and Lon Chaney) at the same time.  During filming, Lon Chaney and Arthur Rosson got lost in a canyon and were not located until the end of the day. The cast and crew were also stranded in their cabins for five days due to heavy rains. Dwan had the cast rehearse for an upcoming film, Richelieu (film)|Richelieu, in an effort to save time. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 