Yuvaraja (film)
{{Infobox film name           = Yuvaraja image          = image_size     = caption        = director       = Puri Jagannath producer       = R. Srinivas writer         = Arun Prasad P.A. narrator       = starring       = Shivarajkumar Lisa Ray Bhavna Pani music          = Ramana Gogula cinematography = K. Datthu editing        = S. Manohar studio         = R S Productions released       =   runtime        = 147 minutes country        = India language       = Kannada budget         =
}} Kannada action sports film directed by Puri Jagannath and produced by R. Srinivas. The film stars Shivarajkumar, Bhavna Pani and Lisa Ray in the lead roles.  The film is a remake of Telugu film Thammudu (film)|Thammudu (1999) directed by Arun Prasad P.A. and starring Pawan Kalyan, Preeti Jhangiani and Aditi Gowitrikar.

The film was released on 2 November 2001 across Karnataka cinema halls and got mixed response from critics and audience. The film completed a successful 50 days run at multiple theaters.

==Cast==
* Shivarajkumar 
* Lisa Ray 
* Bhavna Pani 
* Kumar Govind
* Srinath
* Avinash Sharan
* Ramesh Bhat
* Balaraj
* Bullet Prakash
* Dharma
* Kashi
* Umesh
* Mandeep Roy

==Soundtrack==
The music of the film was composed by Ramana Gogula and lyrics written by K. Kalyan. 

{{Infobox album  
| Name        = Yuvaraja
| Type        = Soundtrack
| Artist      = Ramana Gogula
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Akash Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Missamma Kissamma
| lyrics1  	= K. Kalyan
| extra1        = Ramana Gogula, Nanditha
| length1       = 
| title2        = Bangalooru Students
| lyrics2 	 = K. Kalyan
| extra2        = Ramana Gogula
| length2       = 
| title3        = Monalisa Monalisa
| lyrics3       = K. Kalyan
| extra3  	= Ramana Gogula, Nanditha
| length3       = 
| title4        = Naajooku Naari
| extra4        = Ramana Gogula
| lyrics4 	= K. Kalyan
| length4       = 
| title5        = Chandana Siri
| extra5        = S. P. Balasubrahmanyam
| lyrics5       = K. Kalyan
| length5       = 
| title6        = Look At My Face
| extra6        = Ramana Gogula
| lyrics6 	= K. Kalyan
| length6       = 
}}

==References==
 

==External source==
*  
*  

 

 
 
 
 
 
 
 
 
 


 

 