Love (2011 film)
{{Infobox film
| name           = Love
| image          = Angels & Airwaves - Love film poster.jpg
| border         = yes
| alt            = Love 2011 poster
| caption        = Theatrical release poster
| director       = William Eubank
| producer       =  
| writer         = William Eubank
| starring       = Gunner Wright
| music          = Angels & Airwaves
| cinematography = William Eubank
| editing        =  
| studio         =  
| distributor    = National CineMedia  
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = $500,000 
| gross          = $1,495,102
}} science fiction FanTasia 2011, Love Live event.   
 psychological Social effects of human connection fragility of dying Earth-Apocalyptic apocalyptic Doomsday doomsday scenario) Pale Blue memories and stories as humanitys legacy.   

==Synopsis== Union soldier, Captain Lee Briggs (Bradley Horne), is dispatched on a mission to investigate a mysterious object reported to Union forces. He leaves to venture on the mission.
 CAPCOM and Polaroid pictures of former ISS crew members left aboard the ship. 
 unpressurised module of the space station to perform repairs and discovers the 1864 journal of Briggs. Miller reads Briggs account of the war and becomes enthralled by the mysterious object he is searching for, not realizing he will soon become more familiar with the very same object, and not by accident.

In 2045, six years after losing contact with CAPCOM and with a failing oxygen system inside the ISS, Miller puts on a space suit and goes for a Extravehicular activity|spacewalk, deciding that it would be easier for him to detach his tether and slowly drift towards Earth and to burn in the atmosphere than slowly suffocate to death on board the ISS. He finds, however, that he is unable to go through with his suicide.

Miller is seen still aboard the ISS, presumably much later: his hair has grown extremely long, and he is extensively tattooed. The cramped quarters of the space station have become a rats nest symbolic of his diminished sanity. He then seems to be contacted from outside the ISS, and to receive instructions to dock and transfer over. He does so, and seems to arrive in a giant uninhabited structure of distinctly human making. It is unclear whether this is reality or imagined by Miller, who is now insane.
 mainframe where he finds a book titled "A Love Story As Told by You". Inside this book, he finds pictures of Captain Lee Briggs with his discovery, a gigantic cube-like alien object that may have helped advance Human society. In the index of the book Miller finds a reference to himself and types it into the computer prompt. He then finds himself inside a generic hotel room, where a disembodied voice says:

 

During the speech we see the same cube-like object in space in the year 2045. The viewer is left to assume that this object has obtained Lee Miller and is speaking directly to him. The film ends with the voice of a computer speaking of human connections and love.

==Production==
 
File:Eubank-Love-2011-Figur-ISS-Set-Ext-1.jpg|The ISS set built in a driveway, seen here protected from the rain by plastic tarps.
File:Eubank-Love-2011-Figur-ISS-3.jpg|The interior of the space station set. Gunner Wright in the film Love.
 
<!---
File:Eubank-Love-2011-Figur-ISS-4-Wright.jpg|Eubank directs Wright inside the ISS.
---> pizza bags, velcro, Building insulation|insulation, Christmas lights, and other salvaged material as components to the ISS set.    According to Tom DeLonge, the production was going to rent the space station from another movie but instead opted to construct it from salvaged materials for budget reasons.   

Early teasers were released in 2007 and 2009. On January 10, 2011, the films final trailer was released on Apple Trailers. The release of this trailer saw coverage on several industry websites.  Based on the style choices seen in the films trailer, reviewers have mentioned similarities to  , Moon (film)|Moon, and Solaris (2002 film)|Solaris.   

==Release==

===Festival circuit===
  world premiere Santa Barbara as one of eleven films chosen as "Best of the Fest".

The 2011 Seattle International Film Festival featured Love in both their Sci-Fi and Beyond Pathway and their New American Cinema program. The film played on May 21 at the Pacific Place Theatre and May 22 at the SIFF Cinema. The film played a third time, June 11, at the Egyptian Theatre.
 Fantasia International Film Festival held in Montreal, Quebec. Its FanTasia screening on July 18 in Hall Theatre, as part of the festivals Camera Lucida Section, marked the films international premiere. The film also screened in Athens, Lund, London, Nantes, South Korea, Spain, Israel, and elsewhere.

{| class="wikitable"
|- Date
! Festival
! Location
! Awards
! Link
|-align="center"
| align="center"| Feb 2–5, Feb 11
| align="left"| Santa Barbara International Film Festival
| Santa Barbara, California  
| Top 11 "Best of the Fest" Selection
|  
|-align="center"
| align="center"| May 21–22, Jun 11
| align="left"| Seattle International Film Festival
| Seattle, Washington  
| 
|  
|-align="center"
| align="center"| Jul 18, Jul 25
| align="left"| Fantasia Festival
| Montreal, Quebec  
|   
"for the resourcefulness and unwavering determination by a director to realize his unique vision"
|  
|-align="center" Love Live Nationwide Screening  
|-align="center"
| align="center"| Sep 16 Athens International Film Festival Attica  
|  
|  
|-align="center"
| align="center"| Sep 19 Lund International Fantastic Film Festival
| Lund, Scania|Skåne  
|
|  
|-align="center"
| align="center"| Sep 28
| align="left"| Fantastic Fest
| Austin, Texas  
| 
|  
|-align="center"
| align="center"| Oct 9 London Int. Festival of Science Fiction Film
| London Borough of Camden|London, England  
| Closing Night Film
|  
|-align="center"
| align="center"| Oct 9, Oct 11
| align="left"| Sitges Film Festival
| Sitges, Catalonia  
| 
|  
|-align="center"
| align="center"| Oct 1, Oct 15
| align="left"| Gwacheon International SF Festival
| Gwacheon, Gyeonggi-do  
| 
|  
|-align="center"
| align="center"| Oct 17, Oct 20 Icon TLV Central  
| 
|  
|-align="center"
| align="center"| Oct 23 Toronto After Dark
| Toronto, Ontario  
|    
|  
|-align="center"
| align="center"| Nov 11 Les Utopiales
| Nantes, Pays de la Loire  
| 
|  
|-align="center"
|-align="center"
| align="center"| Nov 12, Nov 18
| align="left"| Indonesia Fantastic Film Festival
| Jakarta, Bandung  
| 
|  
|-align="center"
| align="center"| Nov 16–18
| align="left"| American Film Festival|AFF Lower Silesia  
| 
|  
|}

===Limited release===
 
Love was shown nationwide   on August 10, 2011. 

===DVD===
Angels & Airwaves released a box set containing the film Love, the soundtrack to the film, Love Part I, and the bands fourth studio album Love Part II on November 8, 2011.

==Reception==
At the Santa Barbara International Film Festival, the film was originally slotted three showings but two additional showings in the Arlington Theatre were added after some original showings sold-out.

Dennis Harvey, for Los Angeles-based magazine Variety (magazine)|Variety, wrote "  spiritual abstruseness and the scripts myriad other ambiguities might infuriate in a film less ingeniously designed on more tangible fronts. But Love delights with the detail of its primary set as well as in accomplished effects, consistently interesting yet subservient soundtrack textures (the sole original song is reserved for the   and a brisk editorial pace…"   

Dustin Hucks, for Aint It Cool News, wrote "Love can at times get very broad with scenes, dialogue, and flow… if you’re keen on clarity and the linear, Love is going to leave you frustrated. For others, however&ndash;the challenge of understanding what is what may lead to a desire for repeat viewings, which for me – is a lot of fun… This is a film that’s clearly not for everyone – but has a lot to offer the Inception and Moon (film)|Moon crowds."

Hucks continued to say Love was one of the most visually exciting low-budget films hed seen in some time and concluded with an overall endorsement: "Love is well worth seeking out in theaters – but don’t miss it on DVD if you don’t get the opportunity to view it in theaters."   

==See also== Apollo 13, a 1995 film dramatizing the Apollo 13 incident
* Gravity (film)|Gravity, 2013 3D science-fiction space drama film
* Moon (film)|Moon, a 2009 British science fiction drama film 
* List of films featuring space stations

==References==
 

==External links==
 
*  
*  wall|name=Love}}
*  
*   at the iTunes Preview
*    

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 