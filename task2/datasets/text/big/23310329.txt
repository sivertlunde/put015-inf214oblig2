Undercover (1943 film)
{{Infobox film
| name           = Undercover (1943 film)
| image size     = 
| image	=	Undercover FilmPoster.jpeg
| alt            = 
| caption        = 2010 DVD release cover
| director       = Sergei Nolbandov
| producer       = Sir Michael Balcon S. C. Balcon
| writer         = 
| screenplay     = John Dighton Monja Danischewsky Sergei Nolbandov (uncredited) Milosh Sekulich (uncredited) 
| story          = George Slocombe Milosh Sekulich (uncredited) Sergei Nolbandov (uncredited)
| narrator       =  Michael Wilding Stephen Murray Tom Walls Stanley Baker Godfrey Tearle
| music          = Frederic Austin
| cinematography = Wilkie Cooper
| editing        = Eileen Bolan
| studio         = Ealing Studios
| distributor    = 
| released       =  
| runtime        = 80 min 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Stephen Murray Michael Wilding as Constantine, and Stanley Baker as Petar. The movie was re-released in the United States in 1944 by Columbia Pictures under the title Underground Guerrillas. The Ealing movie was similar to the 20th Century Fox wartime film Chetniks! The Fighting Guerrillas (1943) made in the U.S. The plot revolves around a resistance movement that emerges in Yugoslavia after the German invasion in 1941. The guerrillas are able to blow up trains, engage in sabotage, and to battle German troops. In the final scene, the guerrillas are shown going into the Serbian mountains to continue their resistance struggle until the German forces are driven out of the country. The movie was released on DVD on January 25, 2010 by Optimum Home Entertainment in the UK.

==Cast==
 
* John Clements --- Milosh Petrovitch
* Mary Morris --- Anna Petrovitch Stephen Murray --- Dr. Stephan Petrovitch
* Tom Walls --- Kossan Petrovitch
* Rachel Thomas --- Maria Petrovitch Michael Wilding --- Constantine
* Stanley Baker --- Petar
* Godfrey Tearle --- General von Staengel
* Robert Harris --- Colonel von Brock
* Niall MacGinnis --- Dr. Jordan
* Ivor Barnard --- Stationmaster
* Terwyn Jones --- Danilo
* Finlay Currie --- Priest Ben Williams --- Dragutin

==Plot==

The film is based on the Yugoslav resistance movement under the command of General Draza Mihailovich. But politics overtook the situation because Mihailovich and the Royalists were about to be abandoned and betrayed by the British government - as parts of the Chetnik movement co-operated with the Nazis - in favor of the Communist and Stalinist leader Josip Broz Tito at the time. Speaking in the British Parliament on February 22, 1944, the then Prime Minister, Winston Churchill, said: “General Mihailovic, I much regret to say, drifted gradually into position where his commanders made accommodations with Italian and German troops…”

The screenplay, by John Dighton and Monja Danischewsky, was accordingly amended and the movie re-edited. It was a black and white war movie, 80 minutes in length, that focused on the Petrovitch family in Belgrade, Serbia. One brother, Captain Milosh Petrovitch, a Yugoslav military officer, played by John Clements, emerges as a Serbian guerrilla who forms an anti-Nazi resistance movement in the mountains of Serbia. The other brother, Dr. Stephan Petrovitch, played by Stephen Murray, is a physician in the Belgrade Municipal Hospital who acts as a quisling or collaborator to obtain information for the guerrillas. German General von Staengel, played by Godfrey Tearle, does not suspect that Stephan is an undercover agent for the Serbian guerrillas. 

Using information obtained by Stephan, the guerrillas are able to ambush a German train and to free Yugoslav POWs, wound General Staengel, and to blow up a strategic railway tunnel in the mountains. In retaliation, German troops under Colonel von Brock, played by Robert Harris, execute six Serbian schoolchildren in retaliation and as a lesson. Anna Petrovitch, Miloshs wife, is taken prisoner by German forces and interrogated. She escapes and rejoins Milosh in the mountains.

Stephan manages to plant explosives on a train which he sets to go off in a mountain tunnel. His father Kossan, played by Tom Walls, is captured by German troops and placed on the train to deter an attack. Stephan and Kossan are both killed when the explosives go off and destroy the train and the tunnel. In retaliation, Staengel orders that "one hundred Yugoslavs for every German" will be killed and orders retaliatory strikes against the Serbian guerrillas.

The climax is a pitched battle between the Germans and the guerrillas. The Serbian guerrillas defeat the German troops and retreat into the mountains to continue the guerrilla war against Axis occupation forces.
 

==Production team==
* Director: Sergei Nolbandov
* Producer: Sir Michael Balcon
* Associate Producer: S.C. Balcon
* Script: John Dighton, Monja Danischewsky, Sergei Nolbandov (uncredited), and Milosh Sekulich (uncredited). Based on a story by George Slocombe, Milosh Sekulich (uncredited), and Sergei Nolbandov (uncredited)
* Cinematography: Wilkie Cooper
* Art Direction: Duncan Sutherland
* Editing: Eileen Boland
* Supervising Editor: Sidney Cole
* Special Effects: Roy Kellino
* Technical Advisors: Milosh Sekulich, W.E. Hart
* Music: Frederic Austin

== Sources ==
* Barr, Charles. Ealing Studios: A Movie Book. Berkeley, CA: University of California Press, 1999.
* Barr, Charles. (1974). "Projecting Britain and the British Character: Ealing Studios, Part II." Screen, 15(2), pages 129-163.
* Milosh Sekulich Papers at the School of Slavonic and East European Studies (SSEES) Library, University College, London, UK. Web link: http://www.ssees.ac.uk/archives/sek/sek3.htm
* "How Jugoslavia Fights Back." Pictorial Post, October 10, 1942.
* "Mihailovich: Yugoslavias Unconquered." Time magazine cover, May 25, 1942.
* Dick, Bernard F. The Star-Spangled Screen: The American World War II Film. Lexington, KY: University Press of Kentucky, 2006. p.&nbsp;164.
* "Liberty for the Chetniks." Master Comics, Captain Marvel Jr., #14, February 24, 1942. Quality Comics.
* Orwell, George. "As I Please." Tribune, January 12, 1945.
* Sava, George. The Chetniks. London, UK: Faber and Faber, Ltd., 1942.
* Heydenau, Friedrich. The Wrath of the Eagles: A Novel of the Chetniks. Translated by June Barrows Massey. NY: E.P. Dutton & Co., Inc., 1943.
* "The Chetniks Of Yugoslavia." The War Illustrated, Volume 6, #146, January 22, 1943.
*Marsh, Fred T. Book Review: "The Chetniks: Sergeant Nikola: A Novel of the Chetnik Brigades by Istvan Tamas," The New York Times, Sunday, December 13, 1942.
* Creasey, John. The Valley of Fear. London: John Long, 1943. Doctor Palfrey joins the Chetniks. Reprinted under the title The Perilous Country, 1949, 1966, 1967, and 1973 in the U.S. by Walker and Co.
* St. John, Robert. "Balkan Supermen. Wrath of the Eagles: A Novel of the Chetniks. By Frederick Heydenau." Translated by Barrows Mussey. 318 pp. New York: E.P. Dutton& Co. $2.50. New York Times, Book Review, Sunday, June 27, 1943, page BR6.
* Tabori, Paul. The Ragged Guard, A Tale of 1941. London: Hodder & Stoughton Limited, 1942.
* Tamas, Istvan. Sergeant Nikola; A Novel of the Chetnik Brigades. NY: L.B. Fischer Publishing Corporation, 1942.
* Undercover on the citwf database: http://www.citwf.com/film365195.htm
* "The Eagle of Yugoslavia." Time magazine, Monday, May 25, 1942. Web link: http://www.time.com/time/magazine/article/0,9171,766569,00.html
* Sindbaek, Tea. (2009). "The Fall and Rise of a National Hero: Interpretations of Draza Mihailovic and the Chetniks in Yugoslavia and Serbia Since 1945." Journal of Contemporary European Studies, 17, 1, 47-59.
* Lees, Michael. The Rape of Serbia: The British Role in Titos Grab for Power, 1943-1944. San Diego, CA: Harcourt Brace Jovanovich, 1990.
* Ove, Torsten. "93-year-olds WWII feats are hidden no longer." Pittsburgh-Post Gazette, Sunday, November 23, 2008.
* Martin, David. The Web of Disinformation: Churchills Yugoslav Blunder. San Diego and New York: Harcourt, Brace, Jovanovich, 1990.
* Martin, David. Ally Betrayed: The Uncensored Story of Tito and Mihailovich. NY: Prentice-Hall, 1946.
* Martin, David. Patriot Or Traitor: The Case of General Mihailovich. Introductory essay by David Martin. Hoover Institution, 1978.
* Seaman, Mark. Special Operations Executive: A New Instrument of War. London, UK: Routledge, 2006.
* Deroc, Milan. British Special Operations Explored: Yugoslavia in Turmoil, 1941-1943, and the British Response. Boulder, CO: East European Monographs/New York: Columbia University Press, 1988.
* Howarth, Patrick. Undercover: The Men and Women of the Special Operations Executive. London: Routledge, 1980.
* Hamrick, S.J. Deceiving the Deceivers. Yale University Press, 2004.
* Tosevic, Dimitri J. Third Year of Guerilla. Toronto, Canada: The Periscope Publishing Company, 1943.
* Pribichevich, Stoyan. World Without End: The Saga of Southeastern Europe. New York: Reynal & Hitchcock, 1939.
* Yovitchitch, Lena. Within Closed Frontiers: A Woman in Wartime Yugoslavia. London and Edinburgh: Chambers, 1956.
* Williams, Heather. Parachutes, Patriots and Partisans: The Special Operations Executive and Yugoslavia, 1941-1945. Madison, WI: The University of Wisconsin Press, 2003.
* Trew, Simon C. Britain, Mihailovic and the Chetniks, 1941-42. St. Martins Press, 1997.
* Petrovitch, Svetislav. Free Yugoslavia Calling. Introduction by the Hon. Fiorello H. LaGuardia. NY: The Greystone Press, 1941.
* Pavelic, Boris, and Bojana Oprjan-Ilic. "USA Nevertheless Decorates Chetnik Leader Draza Mihailovic." Novi List, Rijeka, Croatia, May 10, 2005.
* Roberts, Walter R. Tito, Mihailovic and the Allies, 1941-1945. New Brunswick, NJ: Rutgers University Press, 1973.
* Kurapovna, Marcia. Shadows on the Mountain: The Allies, the Resistance, and the Rivalries that Doomed World War II Yugoslavia. Hoboken, NJ: John Wiley and Sons, 2009.

==External links==
* 

 
 
 
 
 
 
 
 
 

 