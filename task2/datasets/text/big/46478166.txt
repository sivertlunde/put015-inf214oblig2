Hate Crimes in the Heartland
{{Infobox film
| name = Hate Crimes in the Heartland
| image = Hate Crimes in the Heartland Poster Final.jpg
| caption = Theatrical Poster
| director = Rachel V. Lyon
| producer = Rachel V. Lyon
| writer = Rachel V. Lyon Bavand Karim
| cinematography = Bavand Karim
| music = Jon Brennan
| editing = Jeremy Freedberg Steve Taylor 
| studio = Lioness Media Arts
| distributor = Alexander Street Press
| released =  
| runtime = 52 minutes
| country = United States
| language = English
}}
Hate Crimes in the Heartland is a 2014 American  , and the 2012 "Good Friday Murders."   

==Background==
Filmmakers Rachel Lyon and  ,"    which was presented at the Northern Kentucky Law Review Symposium in 2012.    Initial funding for the film was raised through a Kickstarter campaign.    Lyon and Karim traveled to Tulsa and Chicago to conduct interviews,   and footage of the race riot survivors was acquired from the film Before They Die! 

Production was completed in December 2013.

==Synopsis==
The film examines the underlying racial tensions in Tulsa, Oklahoma, through two central events, the 1921 race riots and the 2012 "Good Friday Murders." Through interviews with a variety of scholars and public figures, the documentary explores the roots of American racial animosity, presenting Tulsa as a microcosm of the American social, cultural, and racial landscape, and scrutinizing the role of the media in controlling information, and influencing public response and the justice system.

==Interviews==
People interviewed in Hate Crimes in the Heartland include:

*Rev. Jesse Jackson, Civil Rights Activist and Founder, Rainbow/PUSH
*Charles Ogletree, Jesse Climenko Professor of Law at Harvard Law School
*Andrea Lyon, Dean and Professor of Law at Valparaiso University Law School
*Dewey F. Bartlett, Jr., Mayor of Tulsa
*Tim Harris, Tulsa District Attorney
*Chuck Jordan, Chief of Police, Tulsa Police Department

==Screenings==
In January 2014, Lioness Media Arts announced a partnership with The Ford Foundation, Amnesty International, and the Charles Hamilton Houston Institute for Race and Justice at Harvard University for a series of national screenings and discussion forums.    

Hate Crimes in the Heartland premiered nationally in seven cities during a Black History Month tour in February 2014, with screenings held in Tulsa,  Oklahoma City,  Cincinnati,  and on the campuses of Florida Atlantic University  and DePaul.  The film has also been featured at the National Underground Railroad Freedom Center,   the Cleveland Museum of Art,   the University of Oklahoma,  Harvard University,  and the New School. 

==Response==
Following the films release, JustFilms Director Cara Mertes of the Ford Foundation stated, “Hate Crimes in the Heartland revisits powerful moments in American history — past and present — that remind us of the profound racial tensions that have shaped the country’s trajectory.”   

Amnesty International praised the film’s message of reconciliation. “Dr. Martin Luther King, Jr. asserted that civil rights are human rights, and that is the message that we take away from this film,” said Steven W. Hawkins, Executive Director of Amnesty International USA. “Given the historical racial tensions of the last century and the ongoing racial violence of today, we must work to build a culture that values and believes in human rights if we hope to overcome these problems."   

David Harris, Managing Director of the Charles Hamilton Houston Institute for Race and Justice, emphasized the film’s potential for public education. "Through the personal accounts of survivors, witnesses, journalists, and lawmakers, Hate Crimes in the Heartland enriches the public understanding of the underlying tension in Americas heartland, exposing injustices that occurred and giving a voice to those whose perspectives would otherwise remain unheard,” Harris said.   

According to Harvard Professor Charles Ogletree, “Heartland gives an honest accounting of the 1921 Tulsa race riot, revealing Tulsa’s story in the context of racial disparity happening in every city in America today as well."   

==Awards==
Hate Crimes in the Heartland received the Paul Robeson Award for Best Feature Documentary at the 2014 Newark Black Film Festival. 

==Release==
The documentary is available through Alexander Street Press.  

==See also==
*Tulsa Race Riot
*Before They Die! (2010), documentary film directed by Reggie Turner. 
*"Reconstructing the Dreamland: The Tulsa Race Riot of 1921: Race, Reparations, and Reconciliation" (2002), book by Alfred Brophy. 

==References==
 

==External links==
* 
* 

 
 
 
 
 