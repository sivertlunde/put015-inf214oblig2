Nayak (2001 film)
 
 
{{Infobox film
| name           = Nayak
| image          = Nayak poster.jpg
| caption        = Film poster
| director       = S. Shankar
| producer       = A.M. Rathnam
| writer         = S. Shankar Anurag Kashyap (dialogues)
| starring       = Anil Kapoor Rani Mukerji Amrish Puri Paresh Rawal Johnny Lever Saurabh Shukla
| music          = A. R. Rahman
| cinematography = K. V. Anand
| editing        = B. Lenin V. T. Vijayan
| studio         = Sri Surya Movies
| released       =  
| runtime        = 187 minutes
| country        = India Hindi
| budget         =
}} Tamil film Mudhalvan (1999). The films score and soundtrack, composed by A. R. Rahman, were reused from Mudhalvan with Hindi lyrics. It was critically acclaimed and has since developed a mass cult following among audience. The film was a box-office failure, primarily because of poor marketing. It was one of the most expensive films to be completely shot in India. At the same time this film was remade in Bangladesh named "Minister" directed by Kazi Hayat starring Manna (actor), Moushumi, Dildar and Kazi Hayat 

==Synopsis==
Riots and violence cause deaths and destruction in Mumbai city but the Chief Minister Balraj Chauhan (Amrish Puri) because of some political motive, does not take any preventive step. TV reporter Shivaji Rao (Anil Kapoor) in a telly interview of Chauhan publicly brings to light the CMs disregard towards the elimination of the recent violence and his total failure as a CM working for the growth and development of the state. Chauhan, just to stick up for his political image, says to Rao snapping his fingers, "Sirf ek din... sirf ek din ke liye CM bannke dikhaao", (Become the CM just for a day), something that later became a popular and theoretical political concept in India where edification in political and social arena is a crying need. Rao accepts Chauhans challenge and emerges as an efficient CM wiping out various socio-economic problems of the state and dismissing the corrupt government employees. Raos beloved is played by Rani Mukherjee.
==Plot==
The plot focuses on the protagonist, Shivaji Rao (Anil Kapoor). He happens to be an ambitious TV reporter, working for "Q TV" along with his friend Topi (Johnny Lever). While on the job, Rao records a conversation in which the Chief Minister Balraj Chauhan (Amrish Puri), takes an indifferent stand during riots triggered by a fight between some college students and bus drivers, so as not to lose his voter base. Due to police inaction, there is loss of life and damage to property. To explain his actions, Chauhan later agrees to a live interview with Shivaji, during the course of which Rao raises these issues and airs the Chief Ministers stand he had caught on tape.

In response to Shivajis allegations about the mismanagement by his government, Chauhan redirects the question by instead talking about how difficult his job is due to red tape and bureaucracy, and challenges Shivaji to be the CM for a day and experience those problems himself. Shivaji reluctantly accepts the challenge to prove Chuhan wrong. Educated and vigilant, Shivaji takes care of issues that affect the populace everyday, giving the slum their rightful apartments, getting jobs for the unemployed, suspending inefficient and corrupt government officers. He is assisted by the secretary, Bansal (Paresh Rawal) who serves as a side kick. As the last act of the day, Rao gets Chauhan arrested as he is the root cause of all the corruption. Later, Chauhan bails out, and passes an ordinance to nullify all orders passed by Shivaji when he was the CM. Taking Shivajis success as an insult, Chauhan sends hitmen to get him killed and destroys his house on false grounds.

Meanwhile Shivaji falls in love with Manjari (Rani Mukerji), a naive and carefree villager. When Rao asks her father (Shivaji Satam) her hand in marriage, he refuses on grounds that Rao is not employed by the government. After his tenure as a one-day CM he starts preparing for the Indian Civil Service Examination to win Manjaris hand in marriage. However, Bansal shows up and tells him that Shivajis popularity has rocketed sky-high and people want him to become the next CM of the state. He is reluctant at first, but when Chauhans henchmen vandalise Q TV premises to intimidate him, and the people show their support by thronging to his place in huge numbers, he agrees to take part in the elections.

In the ensuing state elections, he wins by a vast majority. But Manjari’s father, angered by Shivajis decision, refuses to let his daughter marry him. Here, Chauhan’s political allies desert him causing his defeat. On becoming the chief minister, Shivaji brings about lots of improvements and quickly becomes an idol in the people’s eyes. However his growing popularity is threatened continuously by Chauhan who uses his henchmen to kill him or at least tarnish is image as a public hero. But Shivaji promptly answers by digging out all accusations against Chauhan and his allies. This, however, causes a reunion of Chauhan and his allies. After a failed attempt on Shivajis life by hiring an assassin, a bomb is detonated at his home killing his parents.

In the final attempt Chauhan orders Pandurung to destroy law and order and cause bomb explosions in various parts of the city. But a priest is able to overhear a few men planning to detonate bombs. He immediately notifies Shivajis office via the "Complaint Box" department. Pandurung is arrested and under tactical inquiry by Shivaji and his secretary, he discloses the location of the 4 bombs. A Bomb Squad is able to defuse 3 bombs, but the 4th one explodes before they could reach to it. Chauhan, however, uses this success against Shivaji by blaming the young CM for the bomb. Seeing no way out, Shivaji summons Chauhan to the secretariat and creates a situation such that it would seem as if Chauhan was there to shoot Shivaji but failed. Shivaji takes up a gun, but pointing it to his arm shoots himself, and then hands over the gun to Chauhan. Now getting to know that Shivaji had set a trap, an enraged Chauhan attempts to shoot Shivaji, but his shot misses. The security guards then shoot down Chauhan killing him. Shivaji, secretly tells Bansal the truth and says that "finally they have turned me into a politician too" but Bansal believes that Chauhan deserved death saying "he instituted politics for a long time for corruption and evil; you did it only once for good" . Manjaris father also comes to realise that Shivaji is in fact a great man who sees duty before everything else and allows Manjari to marry him.

In the end, the city develops under the governance of Shivaji Rao and his colleagues. The complaint box, where people were supposed to fill in their complaints and information about various threats anonymously, is shown to be empty, signifying that the reasons to be afraid have been taken care of.

==Cast==
* Anil Kapoor as Shivaji Rao
* Rani Mukerji as Manjari
* Amrish Puri as Chief Minister Balraj Chauhan
* Paresh Rawal as Bansal
* Pooja Batra as Laila
* Saurabh Shukla as Pandurang
* Neena Kulkarni as Shivaji Raos mother
* Shivaji Satam as Manjaris Father
* Johnny Lever as Topi Kitty as Shivaji Raos father
* Omakuchi Narasimhan as Gameshow contestant who answers "Machar"
* Sushmita Sen in Special Appearance in song "Shakalaka Baby"

==Crew==
* Director – S. Shankar
* Producer – A.M. Rathnam
* Dialogues – Anurag Kashyap
* Story & Screenplay – S. Shankar
* Music – A.R. Rahman
* Cinematography – K.V. Anand
* Editor – B. Lenin – V. T. Vijayan
* Art – Thotta Tharani
* Lyrics – Anand Bakshi Ahmed Khan
* Stunts – Kanal Kannan
* Co-Director – Vijayakumar Raichurs
* Associate Directors – B. Chintu Mohapatra, Raj Acharya
* Assistant Directors – Muthu Vadugu, G. Anandha Narayanan, K.R.Mathivanan, K. Balachery
* Visual Effects – Pentamedia Graphics (P) Ltd., Cytura (Florida, USA)
* Costumes – Manish Malhotra, Nalini Sriram, Shefali (Sushmita Sen), Rohit Bal (Saiyaan Song)
* Hair Designer - Chandra, Bhavna Shah (Rani Mukherjee)
* Make-up – K.M.Sarathkumar, Deepak Chauhan (Anil Kapoor), Rajesh (Rani Mukherjee), Govinda Mehta (Amrish Puri)
* Sound Design – H. Sridhar
* Sound Effects – C. Sethu, Balu
* Songs Recording – H. Sridhar, A. Sivakumar
* Processing – Prasad Colour Lab
* Banner – Sri Surya Movies

==Trivia== Arjun and he had interestingly retained the characters name in the Hindi version.  Originally the first and original choice of the role of Manjari was Manisha Koirala was going to reprise her earlier role from Mudhalvan but she was busy with huge and multi-starrer films like Grahan and Lajja (film)|Lajja therefore she refused. Later on the role played by Rani Mukerji went to Karisma Kapoor, Tabu (actress)|Tabu, Raveena Tandon and Preity Zinta but all of them had date troubles and therefore Mukerji came on board.

==Box office==
Nayak was declared a "Below Average" by Box Office India and IBOS Network due to its high budget and distribution price. The film grossed only   in India.  In the UK, Nayak grossed £83,496 during its theatrical run. 

==Soundtrack==
{{Infobox album  
| Name        = Nayak: The Real Hero
| Type        = soundtrack
| Artist      = A. R. Rahman
| Cover       = 
| Released    = 4 July 2001
| Recorded    = Panchathan Record Inn
| Genre       = Film soundtrack|
| Length      = 38:32
| Label       = T-Series
| Producer    = A. R. Rahman
| Reviews     = 
| Last album  = One 2 Ka 4  (2001)
| This album  = "Nayak: The Real Hero"  (2001)
| Next album  = Love You Hamesha  (2001)
|}}
The soundtrack for the film was composed by A.R.Rahman. He reused all the songs except "Mudhalvane" song, from the original film Mudhalvan. A new track "Saiyyan" was added. The track "Shakalaka Baby" was reused in the musical Bombay Dreams also. The lyrics for the pretuned songs were written by veteran poet Anand Bakshi and this was one of his last films before he died in 2002.

The audio rights were bought by T-Series for  6 crore and released on 4 July 2001. Comparing with the Tamil version which was a massive success, the album did only an average business. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Singer(s)
|-
| 1
| "Chalo Chale Mitwa"
| Udit Narayan, Kavita Krishnamurthy, Swarnalatha
|-
| 2
| "Saiyyan"
| Sunidhi Chauhan, Hans Raj Hans
|-
| 3
| "Chalo Chale Purva"
| Udit Narayan, Kavita Krishnamurthy
|-
| 4
| "Ruki Sukhi Roti"
| Shankar Mahadevan, Alka Yagnik
|-
| 5
| "Shakalaka Baby"
| Vasundhara Das, Praveen Mani, Shiraz Uppal
|-
| 6
| "Tu Accha Lagta Hain"
| Hariharan (singer)|Hariharan, Kavita Krishnamurthy
|-
| 7
| "Chidiya Tu Hoti Toh" Harini
|}

==In popular culture==
Current Chief Minister of Delhi, Arvind Kejriwal is often compared to the protagonist of Nayak,   due to the formers similarities to the crusade against corruption and defeating the incumbent government coming as a rookie in politics.  

==Future==
In September 2013, Anil Kapoor revealed that he was signed to reprise his role in Nayak Returns. He also added that the film would not be a sequel to Nayak, and would have a "completely fresh subject".  Production on the film will begin in early 2015. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 