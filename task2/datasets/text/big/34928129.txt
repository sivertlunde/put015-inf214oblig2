The Players (film)
 
{{Infobox film
| name           = The Players
| image          = 
| caption        = 
| director       = Emmanuelle Bercot Fred Cavayé Alexandre Courtès Jean Dujardin  Michel Hazanavicius Jan Kounen Eric Lartigau Gilles Lellouche
| producer       = Jean Dujardin    Marc Dujardin Éric Hannezo Guillaume Lacroix
| writer         = Nicolas Bedos Philippe Caverivière Jean Dujardin  Stéphane Joly Gilles Lellouche
| starring       = Jean Dujardin Gilles Lellouche
| music          = Pino DAngiò Evgueni Galperine
| cinematography = Guillaume Schiffman
| editing        = Anny Danché
| distributor    = Mars Films
| released       =  
| runtime        = 109 minutes
| country        = France
| language       = French
| budget         = $12 million 
| gross          = $24,610,854 
}} omnibus comedy film starring Jean Dujardin and Gilles Lellouche, with each of them also directing and writing a segment.

== Plot ==
Series of vignettes about the theme of male infidelity and its adulterous variants. 

== Controversy ==
The posters for the film on which Jean Dujardin and Gilles Lellouche can be seen in suggestive positions (like gripping a pair of naked female legs) attracted accusations of sexism.   The French professional authority of regulation for advertising received four official complaints accusing the posters of sexism. Following this controversy, the posters were taken down, and the distributor excused himself.  Jean Dujardin refers to I mostri by Dino Risi.

== Cast ==
*Jean Dujardin as Fred / Olivier / François / Laurent / James 
*Gilles Lellouche as Greg / Nicolas / Bernard / Antoine / Eric
* Lionel Abelanski as the director
* Guillaume Canet as Thibault
* Éric de Montalier as the emergency doctor
* Charles Gérard as Richard
* Dolly Golden as the mistress
* Sandrine Kiberlain as Marie-Christine
* Katia Lewkowicz as Maximes mother
* Alexandra Lamy as Lisa
* Éric Massot as the waiter
* Mathilda May as Ariane
* Géraldine Nakache as Stéphanie
* Isabelle Nanty as Christine
* Maëva Pasquali as Nathalie
* Manu Payet as Simon
* Clara Ponsot: Inès
* Stéphane Roquet as the chain-smoking colleague
* Hélène Seuzaret as Isabelle, Érics wife
* Anthony Sonigo as Benjamin 
* Anne Suarez as Julie
* Bastien Bouillon as Valentin

==References==
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 


 
 