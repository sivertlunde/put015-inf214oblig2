1,000 Dollars a Minute
{{Infobox film
| name           = 1,000 Dollars a Minute
| image          = 1,000DollarsAMinute1935Poster.jpg
| caption        = Film Poster
| director       = Aubrey Scotto
| producer       = Nat Levine
| writer         = Everett Freeman Claire Church Jack Natteford
| screenplay     = 
| story          = 
| based on       =   Roger Pryor
| music          = 
| cinematography = Jack A. Marta
| editing        = Ray Curtiss
| studio         = 
| distributor    = Republic Pictures (I)
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Sound Recording.   

==Plot==
A broken and pennyless newspaperman takes part in an experiment where two crazy millionaires are offering a prize of $10,000 to anyone that can spend $1,000 a minute, every minute, for 12 hours straight.

==Cast== Roger Pryor as Wally Jones
* Leila Hyams as Dorothy Summers
* Edward Brophy as Benny Dolan
* Sterling Holloway as Pete
* Edgar Kennedy as Police Officer McCarthy
* Purnell Pratt as Charlie, the Editor
* Herman Bing as Vanderbrocken
* Arthur Hoyt as Jewel clerk William Austin as Salesman
* Franklin Pangborn as Reville George Hayes as New Deal Watson (as George Hayes)
* Morgan Wallace as Big Jim Bradley

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 