The Lawless Rider
{{Infobox film
| name           = The Lawless Rider
| image          =
| caption        =
| director       = Yakima Canutt Alex Gordon
| writer         = Johnny Carpenter
| based on       =
| starring       = Johnny Carpenter, Texas Rose Bascom
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = United Artists
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = $57,000 
| gross          = 
}}

The Lawless Rider is a 1954 American western black and white film directed by Yakima Canutt, starring Johnny Carpenter, Texas Rose Bascom and Frankie Darro, and marketed by United Artists. 
==Plot==
The Bascom Ranch, owned by Texas Rose Bascom, is targeted by an outlaw gang with the intent to steal cattle off the ranch. Outlaw Freno Frost runs the rustling gang.  One of the gang members is Jim Bascom, Texas Rose Bascoms wayward brother.  When she discovers that her brother is running with the outlaws, she seeks help from the law.  Sheriff Brown is unable or unwilling to help, so Texas Rose asks her boyfriend, U.S. Marshall Johnny Carpenter, to come to her aid.  Johnny Carpenter shows up in town, in disguise, and impersonates the gunslinger Rod Tatum in order to infiltrate the outlaw gang.  Texas Rose Bascom performs her fancy trick roping act for the townfolk, but ruffians interrupt the event. Confusion follows when the real Rod Tatum and the impersonator meet on the street.

==Cast==
*Dale Barrell as The Kid
*Earl W. Bascom as an outlaw
*Texas Rose Bascom as Texas Rose Bascom
*Weldon Bascom as Sheriff Brown
*Shirley Belger as Sis
*Bob Burns as a rancher
*Hank Caldwell as leader of the Saddle Kings Band
*Roy Canada as Andy
*Tap Canutt as Young Marshall
*Frank "Red" Carpenter as Big Red
*Johnny Carpenter as Johnny Carpenter and Rod Tatum
*Fred Carson as Snake Eyes
*Bill Chaney as Bill
*Bill Coontz as Red Rock
*Frankie Darro as Jim Bascom
*Johnny Dew as Carson
*Douglass Dumbrille as Marshall Brady
*Kenne Duncan as Freno Frost
*Leonard P. Geer as Jack
*Ray Morgan as Scar
*Noel Neill as Nancy James
*Bud Osborne as Tulsa
*Blackie Pickerel as Raven
*Lou Roberson as Black Jack Ketchum
*Frank Robbins as Larrabie
*Lennie Smith as Lennie
*Ted Smith as Ted
*Tommy Thomas as a blacksmith

==Sound track==
* Thinking of You, sung by Texas Rose Bascom

==Stunts==
* Weldon Bascom
* Tap Canutt
* Yakima Canutt, director
* Frank "Red" Carpenter
* Fred Carson
* Bill Coontz

==Other credits and crew members==
* Rudy DeSaxe, music
* William C. Thompson, director of cinematography
* Carlo Lodato, film editing
* Micky Meyers, costume design
* Bud Sweeney, makeup artist
* Willard Kirkham, assistant director
* Glen Glenn, sound
* Fred Grossi, still photography
* John C. Fuller, editor
* George N. Brown, transportation
* Winifred Gibson, script supervisor
* Sam Tilden, public relations

==Producers==
*Alex Gordon, executive producer
*Johnny Carpenter, producer
*Weldon Bascom, associate producer

==Production history== Alex Gordon who would go on to make several movies with Arkoff. Gordons friend Ed Wood worked on the movie. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p13  Tom Weaver, Earth Vs. the Sci-fi Filmmakers: 20 Interviews McFarland, 2005 p 115 

==References==
 

==External links==
*  at IMDB

 
 
 
 
 
 