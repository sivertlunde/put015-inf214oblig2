Poovellam Un Vasam
{{Infobox film
| name           = Poovellam Un Vaasam
| image          =
| director       = Ezhil
| writer         = Ezhil
| starring       = Ajith Kumar Jyothika
| producer       = Viswanathan Ravichandran Aascar Film Pvt. Ltd Vidyasagar
| cinematography = Arthur A. Wilson
| editing        = A. Sreekar Prasad
| distributor    =
| released       = 17 August 2001
| runtime        =
| country        = India Tamil
}} Vivek also appear in supporting roles while Yukta Mookhey appeared in a solo item number. It was released in August 2001.  The film received to positive reviews from critics and declare box office hit.

==Plot==
Two families stay in identical bungalows, adjacent to each other, tracing their friendship to over forty years. Their respective progenies Chinna and Chella (Ajith and Jyotika) are childhood buddies, their easy camaraderie and strong bonding carried over to their adult years. The two study in the same college, same class, zoom together on the motorbike, their togetherness at home and college, as accepted a fact as it was in films like Piriyadha Varam Vendum, etc. Somewhere along the way, the duo fall in love, throwing some soft glances at each other, when the other is not looking. Their college mate, Karna (Yugendran, son of Malaysia Vasudevan), makes devious plans to separate the duo and make Chella his. He doesnt seem to have any running feud with Chinna, is quite friendly with the duo, nor does he seem to be particularly in love with Chella. Chinna laps up all that Karna tells him about Chella loving him, and is heartbroken and goes out of station on a business trip.

Chella, persuaded by both families to marry an NRI, boldly informs them that she loved Chinna, and would marry him. The two families are of course happy. Chinna returns home and learns of the preparation for his engagement ceremony. Feeling that since Chella loved Karna, it must be family pressure that must have forced her to agree, Chinna backs out. Now it is Chellas turn to be indignant about Chinnas mistrust of her love, and after giving him a piece of her mind, she backs out, with Chinna pleading for acceptance. After a period of separation, during which all ties between the two families are severed, Chellas family soon come to realise that they cannot live without their dearest friends (and lover). The two families then reunite.

==Cast==
 
* Ajith Kumar as Chinna
* Jyothika as Chella
* Sivakumar
* Ragu
* Nagesh
* V.S. Raghavan Vivek
* Yugendran as Karna
* Kovai Sarala
* Sathyapriya
* Sona Heiden as Anitha
* Ilavarasu
* Madhan Bob
* Vaiyapuri
* Sukumari
* Yukta Mookhey in a guest appearance
 

==Production==
Ajith Kumar signed the film in early 2001 after he pulled out of Praveenkanths commercial film Star (2001 film)|Star.  The producers had initially approached Simran (actress)|Simran, Aishwarya Rai and then Preity Zinta to appear in the lead roles, with their refusals leading to the casting of Jyothika.  Miss World 2001, Yukta Mookhey was roped in to appear in an item number.  Ajith gained mass image before the filming so producer Ravichandran expressed his doubts to Ezhil about carrying on with such a family oriented film with an action hero like Ajith in hand. But Ezhil was confident about Ajith’s capabilities in pulling off varying roles. 

Twin bungalows were erected in Prasad Studios for the film by art director Prabhakaran as the director could not locate a twin bungalow in the same compound. One house was built to feature antiques and the other with modern artifacts and computers.    During the making of the film, Ajith was admitted to a Chennai city hospital for a surgery of his backbone causing a months delay in the film. 

==Release==
The film opened to positive reviews with The Hindu critic labelling it as "reasonably interesting", with Ajiths performance being praised as "Ajith is natural and neat" with claims that "he has to work harder on his soliloquies and sad expressions". 
 special prize Best Costume Best Art Best Comedian Vidyasagar for Best Music Director.Also it won Cinema Express Award for Best Actress for Jyothika.

==Soundtrack==

{{Infobox Album |  
| Name        = Anjaneya
| Type        = soundtrack Vidyasagar
| Cover       = 
| Released    = 2001
| Recorded    =  Feature film soundtrack |
| Length      = 
| Label       = Star Music Vidyasagar
| Reviews     = 
}}

The soundtrack was composed by Vidyasagar_(Composer)|Vidyasagar.

{|class="wikitable" width="70%"
! Song Title !! Singers || Lyricist
|-
| "Kaadhal Vandhadhum" || K.J. Yesudas, Sadhana Sargam || Vairamuthu
|-
| "Pudhu Malar Thottu" || Sriram Parthasarathy|| Vairamuthu
|-
| "Thirumana Malargal" || Swarnalatha|| Vairamuthu
|-
| "Thalaattum Kaatre" || Shankar Mahadevan|| Vairamuthu
|-
| "Chella Namm Veetuku" || Sujatha Mohan, Malaysia Vasudevan, Harish Raghavendra|| Vairamuthu
|-
| "Yukthamukhi" || Devan Ekambaram|Devan, Clinton Cerejo|| Vairamuthu
|-
|}

==References==
 

 

 
 
 
 