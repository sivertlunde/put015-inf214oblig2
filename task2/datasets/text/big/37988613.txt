The Bamboo Saucer
{{Infobox film
| name           = The Bamboo Saucer
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Frank Telford
| producer       = Charles E. Burns, Jerry Fairbanks
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Dan Duryea, John Ericson
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   1968
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Communist China. 

This was the final film of actor Dan Duryea. An alternate title for this film was Collision Course.

==Plot==
An unidentified flying object is reported to have landed in a small Chinese village. Teams from the United States and Soviet Union race to get to it first.

==Cast==
* Dan Duryea as Hank
* John Ericson as Norwood
* Lois Nettleton as Anna Bernard Fox as Ephram
* James Hong as Archibald
* Nan Leslie as Dorothy Vetry (her last acting role)

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 