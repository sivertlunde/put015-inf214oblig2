Chicago (1927 film)
{{Infobox film
| name = Chicago
| director = Frank Urson
| image	= File:Chicago lobby card.jpg
| caption=Lobby card
| image size = 
| producer = Cecil B. DeMille
| writer = Maurine Dallas Watkins (play) Lenore J. Coffee
| based on =  
| starring = Phyllis Haver Julia Faye Victor Varconi May Robson
| cinematography = J. Peverell Marley
| editing = Anne Bauchens
| distributor = Pathé|Pathé Exchange
| released =  
| runtime = 118 minutes
| country = United States
| language = Silent film English titles
| budget = $1,500
}}

Chicago is a 1927 comedy-drama silent film produced by Cecil B. DeMille and directed by Frank Urson.

==Plot==
The plot of the film is drawn from the play Chicago (play)|Chicago by Maurine Dallas Watkins which was in turn based on the true story of Beulah Annan, fictionalized as Roxie Hart (Phyllis Haver), and her spectacular murder of her boyfriend.

The silent film adds considerably to the material in Watkins play, some additions based on the original murder, and some to Hollywood considerations. The murder, which occurs in a very brief vignette before the play begins, is fleshed out considerably. Also, Roxies husband Amos Hart (played by Victor Varconi) has a much more sympathetic and active role in the film than he does either in the play or in the subsequent musical. The ending is more cruel to Roxie, in keeping with Hollywood values of not allowing criminals to profit too much from their crimes (although she does get away with murder).

==Cast==
* Phyllis Haver as Roxie Hart
* Julia Faye as Velma Kelly
* Victor Varconi as Amos Hart
* May Robson as Matron
* Eugene Pallette as Rodney Casely
* Robert Edeson as William Flynn

==Preservation status==
The film was long difficult to see, but a recent print was made available from the UCLA Film and Television Archive, enabling the film to play at festivals and historic theaters around the country. This has greatly improved the reputation of the film. 

A print of the film also survives at Gosfilmofond Russian State Archives.

==See also==
* Roxie Hart (film)
* Chicago (musical)
* Chicago (2002 film)

==References==
 

==Further reading==
* Zsófia Anna Tóth. “ . Ph.D. dissertation, University of Szeged, 2010.
*  " ," Americana: Ejournal of American Studies in Hungary, 4 (1), Spring 2008.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 


 