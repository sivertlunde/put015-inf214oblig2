Modern Girls
 
{{ Infobox film |
| name = Modern Girls
| image = Modern-Girls-Poster.jpg
| caption = Theatrical release poster
| director = Jerry Kramer
| producer = Thomas Coleman (executive producer) Gary Goetzman (producer) June Petersen (associate producer) Anita Rosenberg (associate producer) Michael Rosenblatt (executive producer)
| writer = Laurie Craig Anita Rosenberg
| starring = Virginia Madsen Cynthia Gibb Daphne Zuniga Clayton Rohner
| cinematography = Karen Grossman
| music = Eddie Arkin Iva Davies Jay Levy
| editing = Mitchell Sinoway
| studio = 
| released = November 7, 1986
| awards =
| budget =
| gross = $604,849 (USA) (sub-total)
| runtime = 84 min.
| distributor = Ascot Video Atlantic Entertainment Group
| language = English
| country = United States
}}
 1986 comedy film.    The film is directed by Jerry Kramer and stars Virginia Madsen, Daphne Zuniga and Cynthia Gibb. 

== Plot Summary ==
Three girls, Margo, Kelly, and Cece are roommates living in Los Angeles working menial jobs by day, and by night they enjoy the vibrant and decadent night life of 1980s Los Angeles. 

Margo (Zuniga) has a boring job in telemarketing, Cece (Gibb) gets fired from her job at a department store, while Kelly (Madsen) works in a pet store and is very good at selling pets, mainly thanks to her looks.

Its Friday night and the girls are getting ready for a night out on the town, however Margo and Cece soon discover that Kelly has taken Margos car to go meet a DJ shes infatuated with.

Luckily, Kellys date for the night, Clifford (Rohner, in a dual role), one of her many infatuated customers, arrives to pick her up, so Margo and Cece hitch a ride with him to go to the club where the DJ is working. Cliffie (as Cece calls him) reluctantly tags along a roller-coaster ride with them and learns how the girls usually spend their nights out.

Meeting rock star Bruno X (Rohner), surviving a police raid, getting dumped by DJ Brad, taking a trip on Ecstasy (drug)|Ecstasy, escaping from crazed fans and a run-in with a sexual criminal are some of the situations the group gets themselves into.

==Main cast==
* Cynthia Gibb ... Cece
* Virginia Madsen ... Kelly
* Daphne Zuniga ... Margo
* Clayton Rohner ... Clifford/Bruno X
* Chris Nash ... Ray
* Martin Ferrero ... Music video director
* Stephen Shellen ... Brad
* Rick Overton ... Marsats Troy Evans ... Club owner
* John Dye ... Mark
* Mark Holton ... Boss 
* Stuart Charno ... Nerdy Guy
* Cameron Thor ... D.J. # 1
* Ron Campbell ... D.J. # 2
* Pamela Springsteen ... Tanya
* Mike Muscat ... Mechanic

==Original Soundtrack==
* Depeche Mode - But Not Tonight (Black Celebration 1986, Singles 1986-1998 (DVD2 on Europe Version). Lyrics by Martin Lee Gore. 
* Modern Girls (soundtrack)

== DVD Release ==
On April 2, 2012, Modern Girls was brought to DVD, as part of the MGM Limited Edition Collection series.

==See also==
* List of American films of 1986

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 