A House, A Home
{{Infobox film
| name = A House, A Home
| image = Theatrical poster for the 2012 film A House, A Home.jpg
| alt =
| caption = Theatrical release poster
| director = Daniel Fickle Mark Smith James Strayer Courtney Eck
| screenplay = Mark Smith Daniel Fickle
| based on =  
| starring = Meredith Adelaide Calvin Morie McCarthy 
| music = Alialujah Choir
| cinematography = Reijean Heringlake
| editing = Daniel Fickle Joe Forsythe
| studio = Two Penguins Productions Four Winters
| distributor =
| released =  
| runtime = 7 minutes    
| country = United States
| language = English
| budget =
| gross =
}} Portland Cello Adam Shearer Adam Selzer, Mark Smith, and written by Daniel Fickle and Mark Smith. Starring Meredith Adelaide and Calvin Morie McCarthy the film begins at the last lines of the song "A House, A Home": "You die knowing hell bury you / Next to your love in the ground..." and tells a story of how a love, a death and another death are reconciled in a subterranean world.         |archivedate=25 November 2012}}          
 Mill Valley  film festivals, A House, A Home was nominated for numerous awards ultimately winning thirty-two accolades including Best Short Film at the 32nd New Jersey Film Festival  and Best of Festival at the 55th Rochester International Film Festival.   A House, A Home was selected as an Official Honoree in The 17th Annual Webby Awards in the Music category.   

== Synopsis ==
Under the care of  James C. Hawthorne|Dr. James C. Hawthorne, fictional characters Partrick Brennan (1896-1914) and Sophia Mendenhall (1898-1921) share their temporal lives attracted to each other but are unable to a foster a relationship because of the confines of Dr. Hawthornes mental institution. Patrick becomes convinced that Dr. Hawthorne is romantically involved with Sophia, a false reality that leaves him distraught and prompts him to take his life.    

18-year-old Patrick (Calvin Morie McCarthy) lives an after-life in a small room continuously sketching the same object and exploring a tunnel system behind one of the walls.  Seven years have passed since he was buried by his guardian Dr. Hawthorne. The year is 1921, and the arrival of a neighbor is a redemptive blessing for Patrick.    

Patrick peers through a keyhole and sees Sophia (Meredith Adelaide), now five years his senior. Discovering a passageway he crawls through a narrow tunnel and arrives at her door. Their reunion is awkward for Patrick. Their roles have been reversed, her life experiences eclipse his. The inhibitions of adolescence are in Sophias past, but being far from the world they once inhabited the relative aspects of experience no longer matter.     

==Origin==
 Al James, Jesse Emerson, Matt Sheehy, Richie Young Adam Shearer was asked to contributed to the 15-track compilation.      
 Oregon State Norfolk & Western and M. Ward to collaborate on the arrangement. After completing A House, A Home, Shearer and Selzer were inspired to continue writing and recording. They invited Alia Farah to join them and the band Alialujah Choir was formed.   
 Mark Smith Weinland to South by Southwest|SXSW. On that trip Shearer shared the masters from Alialujah Choirs recording sessions.  
Smith became enamored with the song A House, A Home.  On his return to Portland, Oregon|Portland, he decided to create a video that would begin at the last lines of the song: You die knowing hell bury you / Next to your love in the ground...  Smith contacted director Daniel Fickle and asked him to collaborate on the film. 

==Pre-production==

To create an underground environment a series of sets were constructed from June 2011 to December 2011. The sets were constructed for the camera. Every wall and the ceilings of Sophias and Patricks rooms were removable. The floors were affixed with castor wheels allowing the rooms to rotate 360 degrees.   

==Filming==
 Zeiss Compact Primes. 

===Awards===

{| class="wikitable"
|-
! year
! Film Festival
! Country
! Category
! Result
|-
| 2012 United Kingdom Film Festival
|  
| Best Short 
|     
|-
| 2012 Poppy Jasper International Short Film Festival
|  
| Best Drama
|     
|-
| 2012 Short Sharp Festival
|  
| Best Score
|     
|-
| 2012 Philadelphia Film & Animation Festival
|  
| Best Music Video
|     
|-
| 2012 Flatland Film Festival
|  
| Audience Choice Award
|     
|-
| 2012 Nevada Film Festival
|  
| Platinum Reel Award
|     
|-
| 2012 Interrobang Film Festival
|  
| Best Free Form Film
|     
|-
| 2012 International Film Festival Antigua Barbuda
|  
| Best Music Video
|     
|-
| 2012 Columbia Gorge International Film Festival
|  
| Best Music Video
|     
|-
| 2012 Lucerne International Film Festival
|  
| Platinum Reel Award
|     
|-
| 2012 Silicon Valley Film Festival
|  
| Best Music Video
|     
|-
| 2012 International Film Festival of Cinematic Arts
|  
| Best Editing
|      
|-
| 2012 Oregon Film Awards
|  
| Best Short Film
|     
|-
| 2012
| Accolade Competition
|  
| Art Direction
|       
|-
| 2012 Rumschpringe International Short Film Festival
|  
| Best Music Video
|      
|-
| 2012 One Cloudfest
|  
| Best Music Video
|      
|-
| 2012 One Cloudfest
|  
| Best Cinematography
|   
|-
| 2012 One Cloudfest
|  
| OCF FAV
|      
|-
| 2013 Macon Film Festival
|  
| Best Music Video
|     
|-
| 2013
| Canada International Film Festival
|  
| Rising Star Award
|   
|-
| 2013 Knickerbocker Film Festival
|  
| Best Story
|      
|-
| 2013
| Geneva Film Festival
|  
| Best Narrative Short
|     
|-
| 2013 Charleston International Film Festival
|  
| Best Film
|       
|-
| 2013
| Honolulu Film Awards
|  
| Aloha Accolade
|      
|-
| 2013
| New Jersey Film Festival
|  
| Best Short Film
|     
|-
| 2013 First Glance Film Festival Hollywood
|  
| Best Director
|     
|-
| 2013 First Glance Film Festival Hollywood
|  
| Audience Favorite
|   
|-
| 2013 First Glance Film Festival Philadelphia
|  
| Best Music Video
|      
|-
| 2013 Portland Music Video Festival
|  
| Silver Reel Award
|     
|-
| 2013 Indie Gathering
|  
| Best Music Video
|      
|-
| 2013
| Rochester International Film Festival
|  
| Shoestring Trophy
|       
|-
| 2013
| Rochester International Film Festival
|  
| Best of Festival
|     
|-
|}

===Nominations and Official Selections===
{| class="wikitable collapsible collapsed" style="min-width:40em"
! colspan=3 | Nominations and Official Selections
|-
! Year
! Film Festival
! Country
|-
| 2012
| Raindance Film Festival 
|  
|-
| 2012
| Woodstock Film Festival   
|  
|-
| 2012
| New Orleans Film Festival   
|  
|-
| 2012
| San Diego Film Festival   
|  
|-
| 2012 VisionFest   
|  
|-
| 2012
| Cornwall Film Festival   
|  
|-
| 2012 Park City Music Film Festival   
|  
|-
| 2012 Budapest Short Film Festival    
|  
|-
| 2012 Take Two Film Festival   
|  
|-
| 2012 New York City International Film Festival   
|  
|-
| 2012 Artfest Film Festival   
|  
|-
| 2012
| Tucson Film & Music Festival   
|  
|-
| 2012
| Zero Film Festival   
|  
|-
| 2012
| Sacramento Film and Music Festival   
|  
|-
| 2012 United Film Festival   
|  
|-
| 2012 Great Lakes International Film Festival   
|  
|-
| 2012
| Cincinnati Film Festival    
|  
|-
| 2013
| Beloit International Film Festival   
|  
|-
| 2013 Trail Dance Film Festival     
|  
|-
| 2013 Sedona International Film Festival    
|  
|-
| 2013
| San Antonio Film Festival    
|  
|-
| 2013 White Sands International Film Festival       
|  
|-
| 2013
| List of short film festivals|Action/Cut International Short Film Festival    
|  
|-
| 2013
| Action On Film International Film Festival    
|  
|-
| 2013 Balinale International Film Festival    
| 
|-
| 2013 Maverick Movie Awards      
| 
|-
| 2013
| Fantastic Fest    
| 
|-
| 2013
| Mill Valley Film Festival     
| 
|-
| 2013 Aesthetica Short Film Festival   
| 
|-
| 2013
| Pop Montreal    
| 
|-
| 2013
| Columbus International Film & Video Festival   
| 
|-
| 2013
| Bahamas International Film Festival   
| 
|-
| 2014
| Sonoma International Film Festival   
| 
|-
|}

==Additional appearances==
 MTV Canada    
*NME    
*Current TV  |archivedate=25 November 2012}} 
*First Post 
*IndieWire 
*VH1   
*Country Music Television   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 