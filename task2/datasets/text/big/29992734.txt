Kutsal Damacana: Dracoola
{{Infobox film
| name           = Kutsal Damacana: Dracoola
| image          = KutsalDamacanaDracoolaTheatricalPoster.jpg
| alt            =
| caption        = Theatrical Poster
| director       = Korhan Bozkurt
| producer       =  
| writer         = Ahmet Yılmaz
| starring       =   
| music          = 
| cinematography = Ferhan Akgün
| editing        = Engin Öztürk
| studio         =   
| distributor    = Tiglon
| released       =  
| runtime        =
| country        = Turkey
| language       = Turkish
| budget         = US$2,500,000
| gross          = 
}}

Kutsal Damacana: Dracoola is a 2011 Turkish   (2010).

==Production==
The film was shot on location in Istanbul, Turkey.   

==Synopsis==
Sebahattin grew up eating birdseed after being abandoned in the yard of a mosque when he was a baby boy. A self-taught young man, Sebahattin starts working as a servant for a well-to-do family, where he develops an unrequited love towards the family’s beautiful daughter, Demet. However, his platonic happiness will not be long-lived when the servants’ wing of the mansion is visited one night by the legendary bloodsucker Count Dracoola.

==Release==

===Premiere===
The film was premiered at a special gala showing on   at İstinye Park AFM Theater in Istanbul, where, due to a controversial regulations from the Tobacco Products and Alcoholic Beverages Market (TAPDK), organizers had to cancel plans to serve alcohol.   

=== General release ===
The film opened on nationwide general release in 221 screens across Turkey on   at number 2 in the national box office with a first weekend gross of US$520,818.   

==References==
 

==External links==
*  

 
 
 
 
 
 