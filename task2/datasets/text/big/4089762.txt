Vaastav: The Reality
{{Infobox film
| name           = Vaastav: The Reality (haqiqat in urdu)
| image          = Vaastav The Reality.jpg
| caption        = DVD cover
| director       = Mahesh Manjrekar
| producer       = Deepak Nikalje
| writer         = Imtiyaz Husain  (Dialogue) 
| screenplay     = Mahesh Manjrekar
| story          = Mahesh Manjrekar
| starring       = Sanjay Dutt Namrata Shirodkar Mohnish Behl  Shivaji Satam Reema Lagoo Paresh Rawal Sanjay Narvekar
| music          = Jatin-Lalit
| narrator       = Reema Lagoo
| cinematography = Vijay Aroraa
| editing        = V.N. Mayekar
| distributor    = Adishakti Films
| released       =  
| runtime        = 
| country        = India Hindi
| gross          =   17,00,00,000   
}}
 drama written and directed by Mahesh Manjrekar and starring Sanjay Dutt and Namrata Shirodkar. It also features Sanjay Narvekar, Mohnish Behl, Paresh Rawal, Reema Lagoo and Shivaji Satam in supporting roles.
 Mumbai underworld. The film is said to be loosely based on the life of Mumbai underworld gangster Chota Rajan. 

The film was well received by both critics and audiences and is often regarded by many as one of Sanjay Dutts best performances.  The film was extremely successful both in India and overseas.  It was nominated for and won many awards. Over the years it has become a Cult film. 

The film was followed by the 2002 sequel Hathyar (2002 film)|Hathyar.

==Plot==
Vaastav opens with a family performing the annual rites of a dead person on the beach. When the young son of the deceased asks his grandmother all about the deceased, she begins to narrate the story.
 middle man in the Mumbai underworld. Raghu and Dedh Footiya now end up in the Mumbai underworld.

Vitthal Kaanya (Ashish Vidyarthi), a rival gang lord, offers Raghunath (Raghu) and Dedh Footiya protection and later hires them both as hitmen. Raghunath becomes a respected hit man, with Dedh Footiya as his accomplice. With Raghunath in his gang, Vitthal Kaanya hits a peak in the Mumbai underworld. Later Raghunath is approached by the home minister Babban Rao (Mohan Joshi) and who asks Raghunath to work for him and uses Raghunath for his needs. Raghu agrees, much against the wishes of  Assistant Inspector Kishore Kadam (Deepak Tijori), a good friend of Raghu, who continues to help him by advising him and providing inside information. Vitthal Kaanya is soon killed by rival gangsters.

While Babban Rao relies on Raghunath, there are some others who despise Raghunath and are waiting in the sidelines to see when he makes an error. Raghunath does so, and Babban Rao is soon under serious pressure from the public and government. He issues a shoot-to-kill warrant for Raghunath. Dedh Footiya is killed in an encounter. Then Raghu comes to know from Kishore that the police have been ordered to kill him in an "encounter". Raghu is now on the run, both from the police and Babban Raos men. Raghunath knows now that he must protect his wife, parents, and family, as they too are in danger. He realizes that there is no escape from this harsh reality. He arranges to meet Babban Rao with the help of Suleman Bhai (Paresh Rawal) and kills Babban Rao as he would spoil others lives like his in the future. In the process, Suleiman Bhai is also killed.

Unable to save himself from the police, Raghu comes back to his home and tells his mother to save him. He apparently has become crazy and starts hallucinating. His mother takes him away to safety. He tells her to take his gun and kill him, so she remembers how Raghu had once taught her how to use a gun, pulls the trigger and kills him.

As the film ends, we see the family fulfilling the annual rites of Raghu on the Mumbai beach, as the film had begun, with Raghus mother explaining all that happened to her young son.

==Cast==
* Sanjay Dutt as Raghunath "Raghu" Namdev Shivalkhar
* Namrata Shirodkar as Sonia
* Sanjay Narvekar as Dedh Foootya
* Mohnish Bahl as Vijaykanth Namdev Shivalkhar, Raghus brother
* Shivaji Satam as Namdev, Raghus father
* Reema Lagoo as Shanta, Raghus mother
* Deepak Tijori as sub-inspector Kishore Kadam (Kisha)
* Paresh Rawal as Sulemaan Bhai (Mandavali Badshah) 
* Mohan Joshi as Home Minister Babban Rao
* Ashish Vidyarthi as Vitthal Kaanya 
* Himani Shivpuri as Laxmi Akka, Bordello Madam
* Kashmira Shah as dancer in the song Jawani Se
* Mahesh Manjrekar as himself in a song
* Ekta Bahl as Puja, Vijays Wife
* Jack Gaud as Fracture Bandya
* Ganesh Yadav as Chhota Fracture
* Kishore Nandlaskar
* Bharat Jadhav
* Makarand Anaspure
* Anand Abhyankar
* Achyut Potdar
* Usha Nadkarni as Dedh footiyas Mother

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Meri Duniya Hai" 	
| Sonu Nigam, Kavita Krishnamurthy
| 04:40
|-
| 2
| "Meri Duniya Hai(Male)"
| Sonu Nigam
| 04:38
|-
| 3
| "Tere Pyar Ne (Male)"
| Kumar Sanu
| 04:36
|-
| 4
| "Tere Pyar Ne (Female)"
| Kavita Krishnamurthy
| 04:37
|-
| 5
| "Jawani Se Ab Jung"
| Preetha Mazhumdar
| 04:44
|-
| 6
| "Apni To Nikal Padi"
| Kumar Sanu, Atul Kale
| 04:22
|-
| 7
| "Har Taraf Hai Yeh Shor"
| Vinod Rathod, Atul Kale
| 05:41
|-
| 8
| "Aarti"
| Ravindra Sathe
| 03:14
|-
| 9
| "Vaastav Theme"
| Ravindra Sathe
| 01:14
|-
| 10
| "Apanee Maa Hai Duniya"
| Shankar Mahadevan
|  05:33
|}

==Awards==

===2000 Filmfare Awards=== Best Actor - Sanjay Dutt 	 Best Supporting Actor (Nominated) - Sanjay Narvekar 	 Best Supporting Actress (Nominated) - Reema Lagoo

===2000 Awards of the International Indian Film Academy===
* Award for Artistic Excellence - Sanjay Dutt (Best Actor)
* Award for Technical Excellence - V. N. Mayekar (Editing)

===2000 Screen Weekly Awards===
* Best Actor - Sanjay Dutt

==Sequel==
In 2002, a sequel was made named Hathyar (2002 film)|Hathyar. It was a critical and commercial success.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 