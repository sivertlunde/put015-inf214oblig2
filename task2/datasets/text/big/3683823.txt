The World of Henry Orient
{{Infobox film
| name           = The World of Henry Orient
| image          = WORLDOFH-00AA1-poster_hires.jpg
| caption        = theatrical poster
| director       = George Roy Hill
| producer       = Jerome Hellman
| screenplay     = Nora Johnson Nunnally Johnson
| based on       = The World of Henry Orient&nbsp;by  
| starring       = Peter Sellers Paula Prentiss Merrie Spaeth Tippy Walker Tom Bosley
| music          = Elmer Bernstein
| cinematography = Boris Kaufman Arthur J. Ornitz
| editing        = Stuart Gilmore
| distributor    = United Artists
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         =
| gross          = est. $2,100,000 (US/ Canada) 
}}
The World of Henry Orient  is a 1964 American comedy film based on the novel of the same name by Nora Johnson, who co-wrote the screenplay with her father, Nunnally Johnson. It was directed by George Roy Hill and stars Peter Sellers, Paula Prentiss, Angela Lansbury, Tippy Walker, Merrie Spaeth, Phyllis Thaxter, Bibi Osterwald and Tom Bosley.

Filming started in June 1963, wrapped that October, and the film premiered at Radio City Music Hall on March 19, 1964. In 1965 it was nominated for the Golden Globe Award in the category "Best Motion Picture, Musical or Comedy" and for a Writers Guild of America Award for "Best Written American Comedy."

==Plot==
Concert pianist Henry Orient (Peter Sellers) pursues an affair with a married woman, Stella Dunnworthy (Paula Prentiss), while two teenage private-school girls, Valerie Boyd (Tippy Walker) and Marian Gilbert (Merrie Spaeth), stalk him and write their fantasies about him in a diary.

Orients paranoia leads him to believe that the two girls, who seem to pop up everywhere he goes, are spies sent by his would-be mistresss husband. When Vals mother, Isabel Boyd (Angela Lansbury), finds their diary, she suspects that Henry has acted inappropriately with her daughter.  She contacts Orient and they end up having an affair. Val finds out about it, as does her dad (Tom Bosley). There is an unhappy ending for Vals parents marriage, but Val and her dad start to develop a much closer relationship.

==Cast==
*Peter Sellers as Henry Orient
*Tippy Walker as Valerie "Val" Campbell Boyd
*Merrie Spaeth as Marian "Gil" Gilbert
*Angela Lansbury as Isabel Boyd
*Tom Bosley as Frank Boyd
*Paula Prentiss as Stella Dunnworthy
*Phyllis Thaxter as Mrs. Avis Gilbert
*Bibi Osterwald as Erica "Boothy" Booth
*John Fiedler as Sidney Al Lewis as Store Owner
*Peter Duchin as Joe Daniels Fred Stewart as Doctor
*Philippa Bevans as Emma Hambler
*Jane Buchanan as Lillian Kafritz

==Production==
The pianists unusual surname, "Orient", came about because Nora Johnson based the character on Oscar Levant, a real-life concert pianist, raconteur, and film actor. Since the word "levant" means Orient in French (literally the direction from which the sun rises), the name is a play on words.  In the film, several allusions to the pianists unusual name occur when his two teenage fans put on Chinese conical hats, address their idol as "Oriental Henry," kowtow to an Asian-style altar, and adopt vaguely Japanese-sounding names for themselves.

==Reception==
The World of Henry Orient was the official U.S. entry at the 1964 Cannes Film Festival.      

The film was well-received by critics and has an 88% rating at Rotten Tomatoes. In his review for The New York Times, Bosley Crowther wrote that it was "one of the most joyous and comforting movies about teenagers that weve had in a long time".   

It was voted one of the Years Ten Best Films by the National Board of Review in 1964.   

==Musical adaptation== Palace Theatre on October 23, 1967.  It starred Don Ameche as Henry Orient, Neva Small as Marian Gilbert, Robin Wilson as Valerie Boyd, Milo Bouton as Mr Boyd, Carol Bruce as Mrs. Boyd and Louise Lasser as Stella. Pia Zadora also appeared in the role of a student. The show ran for 80 performances and closed on December 31, 1967, receiving less than stellar reviews.  William Goldman, in his study of the 1967-68 theater year "The Season", claimed that the musical was of high quality but was old fashioned, and "had the misfortune" to open just a week after all the critics "were overcome by Hair (musical)|Hair," which had a modern sound.
 Tony for "Best Choreography." 

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 