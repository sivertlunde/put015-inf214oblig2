Taking Liberties (film)
 
 
{{Infobox film
| name           = Taking Liberties
| image          = Taking_liberties_poster.jpg
| alt            = 
| caption        = Film Poster Chris Atkins
| producer       = {{Plainlist|
* Kurt Engfehr
* Christina Slater
}}
| narrator       = {{Plainlist|
* Ashley Jensen
* David Morrissey
}}
| starring       = 
| music          = Vince Watts
| cinematography = 
| editing        = Nick Fenton
| studio         = 
| distributor    = {{Plainlist|
* Revolver Entertainment
* S2S Productions
}}
| released       =   
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Taking Liberties (also known as Taking Liberties Since 1997) is a documentary film about the erosion of civil liberties in the United Kingdom and increase of surveillance under the government of Tony Blair. It was released in the UK on 8 June 2007. 
 Chris Atkins, said on 1 May that he wanted to expose "the Orwellian state" that now threatened Britain as a result of Mr Blairs policies.   There is also an accompanying book. 

==Synopsis==
The film starts with three coaches of peace protestors on the way to the RAF Base in Fairford, Gloucestershire on 22 March 2003 held for two hours by 100 riot police and escorted back to London denied the right to protest. An animated sequence profiles the key events of WWII; Reichstag fire, Kristalnacht, the invasion of Poland, allied victory and the subsequent European convention on Human rights designed to ensure it never happened again. Archive footage of Tony Blair defends New Labour laws which undo this convention and which the 7 July 2005 London bombings survivor Rachel North rallies against.

===Freedom of Speech===

Tony Benn introduces the story of the expulsion of Walter Wolfgang from the Labour Party Conference for protesting a speech by Jack Straw. Toby Rhodes of Splash Clothing relates the story of a 20 year old student stopped by police for wearing one of his company’s "Bollocks to Blair" T-shirts.

===Right to Protest===

Maya Evans and Milan Rai of the Justice Not Vengeance discuss their arrest for an illegal memorial service outside Downing Street.  Photos, title cards and archive footage tell the story of protestor Brian Haw. Peace Campaigners Sylvia & Helen talk about their arrest at the Menwith Hill US Military Base in Yorkshire. Baptist Minister Malcolm Carroll and the Rickford family discuss the Climate Camp protest at East Midlands Airport. Finally the collusion between police and EDO MBM to break anti-war protests by the Brighton & Hove Citizens Weapons’ Inspectors is revealed.

===Right to Privacy===

Director Chris Atkins visits Counter Terror World 2006 at Kensington Olympia, London to discuss surveillance technology with the delegates. An animated sequence introduces the Panopticon and compares it to the surveillance society before going to profile the history of ID Cards from Harry Willcock’s 1950 protest to their use in the Rwanda genocide and by the Stasi, the East German secret police. Photos, title cards and archive footage tell the story of Blair and Bush’s rush to war on Iraq and the resultant 7/7 attacks which the film maintains ID cards could not have prevented.

===Innocent Until Proven Guilty===

John Tulloch rallies against the proposed 90-days detention extension which the Sun used his image to promote. Archive footage of Metropolitan Police Commissioner Sir Ian Blair defends the 2 June 2006 Forest Gate raid on the home of Abdul Koyair and Mohammed Abdul Kahar. An animated sequence details Clause 39 of the Magna Carta, which establishes Habeas Corpus and lead to the banning of slavery, but following wartime suspension and the internment laws in Northern Ireland is, according to interviewees, has been undermined by the Blair government.

===No Detention Without Charge===

Retired Headmistresses Jennifer and Des visit the confines of Algerian asylum seeker Mouloud Sihali who is under indefinite house arrest without charge due to his involvement in the alleged Wood Green ricin plot used as an example by Colin Powell in his UN testimony despite no ricin ever being found. Former jury members testify to the lack of evidence in the trial which saw him acquitted only to be later re-arrested and threatened with deportation. David Bermingham of the Natwest 3 reveals the dangers of the extradition treaty with the US signed by David Blunkett.

===Ban on Torture===

Former Guantanamo detainee Moazzam Begg and the family of Libyan exile Omar Deghayes reveal prisoner abuses including the force feeding of hunger strikers in the "torture chair". The shackle-shuffle protest outside the Hiatt factory in Birmingham protests the manufacture of shackles for Guantanamo. A mock advert for RendAir introduces the illegal nature of extraordinary rendition flights to regimes which use torture with the alleged complicity of the British government denied by Jack Straw and Tony Blair. "The Road to Guantanamo" star Riz Ahmed discusses his own detainment.

An epilogue contends that neither Gordon Brown nor any of his generations of successors are likely to return the rights which Tony Blair have removed without a fight like that put up by the Fairford coach campaigner who won their case at the House of Lords.

==Participants==
*Rachel North – Writer & 7/7 Survivor
*Tony Benn – Former Labour MP & Cabinet Minister
*Walter Wolfgang
*Toby Rhodes – Splash Clothing Henry Porter – Novelist & Observer Columnist
*Maya Evans – Justice Not Vengeance Co-ordinator
*Milan Rai – Justice Not Vengeance Co-ordinator
*Mark Thomas – Comedian & Activist
*Sylvia & Helen – Peace Campaigners
*Malcolm Carroll – Baptist Minister
*Ellen & Rose Rickford – Students
*Frances – Ellen & Rose’s Mum
*Brendan – Ellen & Rose’s Dad
*Richard – Peace Campaigner
*John & Linda Catt
*Stephanie Harrison – Barrister
*Chris – Peace Campaigner
*Timothy Lawson-Cruttenden – Solicitor for EDO MBM
*Lydia D’Agostino - Solicitor
*Chief Superintendent Barry Norman – Founder of Forward Intelligence Team (F.I.T.)
*Shami Chakrabarti – Director of Liberty (Human Rights Organisation)
*Boris Johnson – Conservative MP
*Kenneth Clarke – Conservative MP
*Phil Booth – NO2ID National Co-ordinator Ross Anderson – Cambridge University
*Clare Short – Former Labour Cabinet Minister
*Michael Mansfield QC
*John Tulloch
*Jennifer & Des – Retired Headmistresses
*Mouloud Sihali
*David Bermingham – Natwest 3
*Clive Stafford Smith – Lawyer for Guantanamo Detainees
*Moazzam Begg – Former Guantanamo Detainee Kate Allen – Director, Amnesty Internartional
*Philippe Sands QC – Author of "Lawless World" David Nicholl – Human Rights Campaigner
*Amani Deghayes – Omar Deghayes|Omar’s Sister
*Zohra Zewawi – Omar’s Mother
*Baroness Sarah Ludford – Liberal Democrat MEP
*Stephen Grey – Author of "Ghost Plane"
*Michael Scheuer – Former Chief of CIA Bin Laden Unit
*Riz Ahmed
*Jane Laporte – Fairford Coach Campaigner

==Reception==
The film received generally positive reviews, gaining 4/5 from The Guardian,  The Times,  and the BBC.  Director Chris Atkins was also nominated for the Carl Foreman award for most promising newcomer. 

==See also==
* Fahrenheit 9/11

==References==
 

==External links==
* 
*  
* 
*  at Counter Culture
*  The Scotsman, 1 May 2007
*  The Telegraph, 2 May 2007
*   Reuters, Wednesday, 2 May 2007  
*  4rfv Ireland, 10 May 2007
*  RINF Alternative News, 19 June 2007
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 