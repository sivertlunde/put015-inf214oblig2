Extraterrestrial (2011 film)
{{Multiple issues|
 
 
}}

 

{{Infobox film
| name           = Extraterrestrial
| image          = Extraterrestrel - Poster.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Nacho Vigalondo
| producer       = {{Plainlist|
* Nacho Vigalondo
* Nahikari Ipiña
}}
| writer         = Nacho Vigalondo
| starring       = {{Plainlist|
* Michelle Jenner
* Carlos Areces
* Julián Villagrán
* Raúl Cimas
* Miguel Noguera
}}
| music          = Jorge Magaz
| cinematography = Jon D. Domínguez
| editing        = 
| studio         =  {{Plainlist|
* Arsénico Producciones
* Sayaka Producciones Audiovisuales}}
| distributor    =  Vértigo Films
| released       =  
| runtime        = 
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}}
Extraterrestrial ( ) is a 2011 Spanish science-fiction romantic comedy.  Directed by Nacho Vigalondo (in his second feature), it stars Michelle Jenner, Julian Villagran and Carlos Areces.

It was filmed in   in Sitges, Spain, and the Fantastic Fest in Austin, Texas.

==Synopsis==
Julio (Julian Villagran) wakes up one morning in an apartment, unable to remember what happened the night before.  He barely has a chance to speak to the girl he spent the previous evening with, Julia (Michelle Jenner), when it becomes clear there is an alien spacecraft hanging over the city.

Before long they discover the next door neighbor, Angel; and Julias boyfriend, Carlos are both alive.

==Cast==
*Michelle Jenner as Julia
*Carlos Areces as Ángel
*Julián Villagrán as Julio
*Raúl Cimas as Carlos
*Miguel Noguera

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 