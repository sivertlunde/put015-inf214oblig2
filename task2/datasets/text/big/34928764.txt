The Price of a Party
{{infobox film
| name           = The Price of a Party
| image          =
| imagesize      =
| caption        =
| director       = Charles Giblyn
| producer       = Howard Estabrook
| writer         = William Briggs MacHarg (screen story) Charles F. Roebuck (scenario) Harrison Ford Arthur Edmund Carewe Mary Astor
| music          =
| cinematography = John F. Seitz
| editing        =
| distributor    = Associated Exhibitors
| released       = November 23, 1924
| runtime        = 6 reels
| country        = United States Silent English intertitles
}}
The Price of a Party is a 1924 silent melodrama produced by Howard Estabrook with a release through Associated Exhibitors. The film is based on a story by William Briggs MacHarg with the films scenario written by Charles F. Roebuck. Charles Giblyn directed with Hope Hampton starring.

==Cast==
*Hope Hampton - Grace Barrows Harrison Ford - Robert Casson
*Arthur Edmund Carewe - Kenneth Bellwood
*Mary Astor - Alice Barrows
*Dagmar Godowsky - Evelyn Dolores
*Fred Hadley - Stephen Durrell
*Edna Richmond - Evelyns Maid
*Donald Lashey - Hall Boy
*Florence Richardson - Jazz Queen
*Daniel Pennell
*J. Moy Bennett

==Preservation status==
The film is now considered a lost film.  

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 