Satyawadi Raja Harishchandra
 
{{Infobox film name           = Satyawadi Raja Harishchandra image          = Harishchandra.jpg image_size     = caption        = A poster of the film released in the newspaper director       = Rustomji Dhotiwala producer       = Elphinstone Bioscope writer         = Nityabodha Bidyaratna starring       = Hormusji Tantra, Savaria, Gaharjan, Behramshaw cinematography = Jyotish Sarkar distributor    = Madan Theatre released       =   runtime        = 120 minutes country        = British Raj language  Bengali inter-titles
}} 1917 Silent silent Black-and-white|black Indian cinema, Solar Dynasty, who donated his entire kingdom and sold himself and his family to keep the promise given to the sage Vishvamitra in the dream.  It is also the first feature film made in Calcutta. The intertitles used in the film were in Bengali language as the film was a silent film.  The film was released on 24 March 1917 at New Tent Maidan, Calcutta.  

==Plot==
 

The Hindu sage Vishwamitra approaches king Harishchandra and informs him of a promise made by the king during the sages dream to donate his entire kingdom. Being virtuous, Harishchandra immediately donates his entire kingdom to the sage and walks away with his wife Taramati and son Rohitashwa. As the entire world came to the control of the sage, after Harishchandra donated his kingdom, the king had to go to Varanasi, a holy town dedicated to Lord Shiva which was the only place outside the influence of the sage. As a part of donation, the sage claims an additional amount as "Dakshina" (honorarium) to be paid to complete the act of donation. As Harishchandra does not have anything left for himself, he sells his wife and son to a Brahmin family to pay for the Dakshina. However, the money collected was not sufficient enough for sage and then Harishchandra sells himself to the guard at the cremation ground.

While working as a servant for a Brahmin family, Harishchandras son gets bitten by a snake while plucking the flowers for his masters prayer, he then dies. Taramati takes his body to the cremation grounds where Harishchandra is working. She does not have sufficient money to pay to perform the rites and Harishchandra does not recognize his wife and son. He advises Taramati to sell off her Mangalsutra, a symbolism of marriage in India, to pay the amount for cremation. Having been granted the boon that only her husband can see her mangalsutra, Taramati recognizes Harishchandra and makes him aware of the happenings. Dutiful Harishchandra requests Taramati to pay the amount to finish the cremation and declines to accept mangalsutra as amount. Taramati then offers her only possession, a saree - her lone dress, a part of which was used to cover the dead body of her son.
 heaven in Hindu deities along with the sage Vishwamitra manifests themselves and praises Harishchandra for his perseverance and steadfastness. They bring Harishchandras son back to life. They also offers the king and his wife, instant places in heaven. Harishchandra refuses it stating that he is still bound to his master, the guard at the cremation ground. The sage Vishwamitra then reveals that the guard is Yama (the god of death in Hinduism) and Yama allowes Harishchandra to accept the offer from Vishwamitra.

Being Kshatriya (the ruling and military elite of the Vedic-Hindu social system) Harishchandra still declines the offer saying that he cannot leave behind his subjects and requests heaven for all of them. The gods declines his offer to which Harishchandra suggests to pass on all his good virtues to his people so that they can rightfully accompany him to heaven. Pleased with Harishchandra, gods accepts his offer and offers heavenly abode to the king, the queen and all their subjects.

==Production==
 Madan Theaters Limited, was mainly involved in exhibition, distribution and production of Indian films during the silent era of film industry.  Madan Theaters Limited eventually became Indias largest film production-distribution-exhibition company and was also a noted importer of American films after World War I. 
 Italian artists reels having length of 7000 feet and was a 35 mm film. Pt. Nityabodha Bidyaratna wrote the screen play. The film was produced by J. F. Madans Elphinstone Bioscope and was distributed by his another company, Madan Theaters Limited.  

==See also==
* List of Hindu mythological films

==References==
 

===Notes===
    The Irving refers to British stage actor Henry Irving. 

===Bibliography===
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
 

==External links==
* 

 
 
 
 
 
 
 