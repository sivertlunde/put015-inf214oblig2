Do You Believe? (film)
 
{{Infobox film
| name     = Do You Believe?
| image    = Do You Believe? film.jpg
| alt      = 
| caption  = 
| director = Jonathan M. Gunn 
| producer = {{Plainlist|
* Harold Cronk
* Michael Scott
* David A.R. White
* Russell Wolfe}}
| writer = {{Plainlist|
* Chuck Konzelman
* Cary Solomon}}
| starring = {{Plainlist|
* Mira Sorvino
* Sean Astin Alexa PenaVega
* Ted McGinley
* Andrea Logan White
* Cybill Shepherd
* Lee Majors
* Madison Pettis
* Brian Bosworth}}
| music   = Will Musser
| cinematography = Brian Shanley
| editing = Vance Null
| production companies = {{Plainlist|
* 10 West Studios
* Believe Entertainment
* Pure Flix Entertainment}}
| distributor = Freestyle Releasing
| released =  
| runtime  = 115 minutes 
| country  = United States
| language = English
| budget   = 
| gross    = $12.7 million 
}} Christian drama Alexa PenaVega, Sean Astin, Madison Pettis, Cybill Shepherd, and Brian Bosworth.  It was released on March 20, 2015. 

==Plot==
The film begins with an opening quote from  . A pastor (McGinley) is shaken by a street preacher regarding his faith, which causes a chain of events for twelve different people, moving towards one cataclysmic event. Some will survive while others will not, but many lives will be changed. An older couple (Shepherd and Majors) have lost their only child. The aforementioned pastor and his wife (Melchior) were unable to conceive a child. A mother (Sorvino) and young daughter (Makenzie Moss) find themselves homeless, a young nurse (Dominguez) and her husband (Matthews) are caught in a court case that may ruin their lives.  A young mother (Pettis) is about to give birth.  A war veteran (Soria) returns home with post-traumatic stress disorder|PTSD. A suicidal young woman (PenaVega) seeks to see her father and feel loved again. A street gang is caught up in the mix when one of the members (Shwayze) unexpectedly ends up in church while running and hiding from the police. The core message for all involved in the drama is a simple question – "Do You Believe?"

==Cast==
 
* Mira Sorvino as Samantha
* Sean Astin as Dr. Farell Alexa PenaVega as Lacy
* Ted McGinley as Matthew
* Andrea Logan White as Andrea 
* Cybill Shepherd as Teri 
* Lee Majors as J.D. 
* Madison Pettis as Maggie
* Brian Bosworth as Joe
* Joseph Julian Soria as Carlos
* Tracy Melchior as Grace
* Valerie Domínguez as Elena
* Senyo Amoaku as Kriminal
* Liam Matthews as Bobby
* Shane Carson as Detective
* Delpaneaux Wills as 40 Ounce
* Makenzie Moss as Lily
* Shwayze as Pretty Boy
* Arthur Cartwright as Little B
 

==Release==

===Box office===
Do You Believe? opened theatrically in 1,320 venues on March 20, 2015 and earned $3,591,282 in its first weekend, ranking number six in the domestic box office, behind  ,  .   

==Reception==

===Critical response=== rating average of 4.2/10.  On Metacritic, which assigns a normalized rating, the film has a score of 22 out of 100, based on 6 critics, indicating "generally unfavorable reviews". 

Film reviewer for The Dove Foundation, Edwin L. Carpenter starts his review with, "...the best faith-based film I have ever seen!"    Writing for the The Times-Picayune, Mike Scott describes the film as, "It is not mainstream entertainment; it is mainstream Sunday school – which is fine if this is what you want to see at the movie theater."  Scott Foundas, Chief Film Critic at Variety (magazine)|Variety magazine, pans the film with "But when all its threads are finally pulled into place, Do You Believe? proves about as spiritually enlightening as a Kmart throw rug."     Huffington Posts Jackie Cooper gave the film 7/10.  Micheal Foust, writing for The Christian Post nominates the film as the new best evangelistic film ever, he goes on to reason, "The majority of movie critics will likely give it poor reviews, partially because it is more overtly evangelistic than any successful faith-based theatrical movie in recent history. But Im guessing those who see the film will like it."    Writing for RogerEbert.com, Peter Sobczynski reports, "Subtle as a sledgehammer to the toes and only slightly more entertaining, Do You Believe? will no doubt play well with viewers already predisposed towards liking it because it has been designed to reconfirm their already deeply-felt beliefs rather than doing anything that might cause them to think about or challenge those beliefs in any meaningful way."   

===Audience response=== Rotten Tomatoes Audience Score is currently 4.3/5 with 87% reporting they liked the film   

==References==
{{reflist| refs =
   

   

   

   

}}

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 