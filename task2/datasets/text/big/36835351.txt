Windmills of the Gods (miniseries)
{{Infobox film
| name = Windmills of the Gods
| image = Windmills of the Gods (TV miniseries).jpg
| caption =
| director = Lee Philips
| writer =
| starring = Jaclyn Smith
| music = Perry Botkin, Jr.
| cinematography = Gayne Rescher
| editing =
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country = United States
| language = English
| budget =
}} 1988 American novel with the same name written by Sidney Sheldon.   It was broadcast on February 7 through February 9, 1988 by CBS.  It was the fifth miniseries based on a Sheldons book, and the third adaptation starred by Jaclyn Smith. 

== Cast ==
* Jaclyn Smith	as 		Mary Ashley
* Robert Wagner	as 	Mike Slade
* Franco Nero	as  	"President of Romania" Alex Ionescu
* Christopher Cazenove	as 	Dr. Louis Desforges
* Michael Moriarty	as  	Ellison
* Ian McKellen	as  	Chairman
* Ruby Dee	as 	Dorothy, Mary Ashleys Secretary 
* Susan Tyrrell	as 	Neusa
* Jeffrey DeMunn	as 	Rogers 
* David Ackroyd	as 	Dr. Edward Ashley 
* Stephanie Faracy	as  	Florence Schiffer
* Ari Meyers	as 	Beth Ashley
* Richard K. Olsen	as  	Ben Cohn
* Betsy Palmer	as  	Mrs. Hart Brisbane 
* John Pleshette	as  	Eddie
* John Standing	as  	Sir George
* J.T. Walsh	as 	Colonel Bill McKinney
* Jean-Pierre Aumont
* Lisa Pelikan	 	 Nicholas Ball

==References==
 

==External links==
* 

 
 
 

 
 
 
 
 


 