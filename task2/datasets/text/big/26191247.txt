Killing Bono
{{Infobox film
| name           = Killing Bono
| image          = Killing Bono.jpg
| alt            = 
| caption        = 
| director       = Nick Hamm
| producer       = Ian Flooks Mark Huffam Piers Tempest
| screenplay     = Dick Clement Ian La Frenais Simon Maxwell Ben Bond (additional material)
| story          = Neil McCormick   Ben Barnes Martin McCann
| music          = 
| cinematography = Kieran McGuigan
| editing        = Billy Sneddon
| studio         = Northern Ireland Screen
| distributor    = Paramount Pictures (UK) Arc Entertainment (US)
| released       =  
| runtime        = 114 minutes
| country        = United Kingdom Ireland
| language       = English
| budget         = 
| gross          = 
}}
Killing Bono is a 2011 British-Irish comedy film directed by Nick Hamm, based on Neil McCormicks 2003 memoir Killing Bono: I Was Bonos Doppelgänger. 
 Ben Barnes Martin McCann as Irish singer Bono.  It marked Pete Postlethwaites final film role.

The film was shot in Northern Ireland  and was funded by Northern Ireland Screen   and was released by Paramount Pictures (the distributor of U2s film Rattle and Hum) in the United Kingdom on 1 April 2011. Sony Music Entertainment released the movies soundtrack worldwide.  The European premiere was held in the Savoy Cinema in Dublin. 

==Cast== Ben Barnes as Neil
*Robert Sheehan as Ivan 
*Pete Postlethwaite as Karl
*Krysten Ritter as Gloria
*David Fennelly as Frankie ( Frank Kearns)
*Charlie Cox as Unknown Role
*James Lonergan as Kevin
*Ralph Brown as Leo
*Justine Waddell as Danielle 
*Luke Treadaway as Nick Martin McCann as Bono
*Peter Serafinowicz as Hammond

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 