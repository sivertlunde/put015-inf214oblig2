The Honourable Wally Norman
{{Infobox film
| name           = The Honourable Wally Norman
| image          =
| caption        =
| director       = Ted Emery
| producer       = Barbara Gibbs Rick Kalowski Emile Sherman Jonathan Shteinman
| writer         = Andrew Jones Rick Kalowski Kevin Harrington Shaun Micallef Greig Pickhaver
| music          = Paul Robert Burton Jim Conway Arne Hanna
| cinematography = David Foreman
| editing        = Stephen Evans
| distributor    = Becker Entertainment
| released       = 13 November 2003
| runtime        = 88 minutes
| country        = Australia
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} 
 Kevin Harrington, Shaun Micallef, and Greig Pickhaver.
 AFI awards.

==Plot==
The story begins with a corrupt Member of Parliament (Shaun Micallef|Micallef) shutting down a country towns main source of employment in the local Meat packing industry|meatworks. This leaves Wally Norman (Kevin Harrington (actor)|Harrington) out of a job, until drunk politician Willy Norman accidentally writes the wrong name on the parliamentary nomination form.
 fear of public speaking.

Meanwhile, Myles is attracted to Wallys daughter, and a wombats career skyrockets.

Much of the films humour comes from wordplay, such as naming the town Givens Head, and the foreman of the meatworks being named George Foreman|George. Shaun Micallef said he had to insist that his own moniker was modified from F. Ken Oath to F. Ken Oats to soften one of the films less subtle attempts at punning.   

==Cast== Kevin Harrington as Wally Norman
* Alan Cassell as Willy Norman
* Shaun Micallef as Ken Oats Rosalind Hammond as Dolly Norman
* Nathaniel Davison as Myles Greenstreet
* Greig Pickhaver as The Chairman
* Octavia Barron-Martin as Laurie Norman
* Tom Budge as Normie Norman
* Bryan Dawe as Richard Nicholls
* Melissa Madden-Gray as Rebecca-Jane Thompson
* Paul Kelman as Gary
* Reg Evans as Barry
*Paul Makin as Alan Unwin Bookmaker

==Filming== Mount Barker Nairne for the town in the movie. The town of Lobethal provided the hotel, Mount Barker provided the meatworks and the St Francis de Sales College, and Nairne hosted the main street for the Gravity racer|billy-cart race shown near the beginning of the movie.

Mike Rann, at that time Premier of South Australia, had a cameo appearance at 1:12:10 in the film. He played the man who was thrown out of his seat by The Chairman. 

==Critical reception==
The   said "The Honourable Wally Norman seriously lacks bite; its a determinably retro affair, amiable and mildly amusing, but pretty bland."     Urban Cinefile dismissed the film as "a shallow and contrived affair with the occasional shadow of humour passing over it."   

==Box office==
The Honourable Wally Norman grossed $181,395 at the box office in Australia. 

==See also==
* Cinema of Australia
* South Australian Film Corporation

==References==
 

==External links==
*  
*  
* 

 
 
 
 
 
 