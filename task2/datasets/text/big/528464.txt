Ed Wood (film)
{{Infobox film
| name           = Ed Wood
| image          = Ed Wood film poster.jpg
| caption        = Theatrical release poster
| director       = Tim Burton
| producer       = Tim Burton Denise Di Novi Scott Alexander Larry Karaszewski
| based on       = Nightmare of Ecstasy  by Rudolph Grey Lisa Marie Jeffrey Jones Bill Murray
| music          = Howard Shore
| cinematography = Stefan Czapsky
| editing        = Chris Lebenzon
| studio         = Touchstone Pictures Burton/Di Novi Productions Buena Vista Pictures
| released       =  
| runtime        = 127 minutes
| country        = United States
| language       = English
| budget         = $18 million 
| gross          = $5.9 million 
}}
 biopic directed Lisa Marie, and Bill Murray are among the supporting cast.
 Problem Child its sequel, Alexander and Karaszewski struck a deal with Burton and Denise Di Novi to produce the Ed Wood biopic, and Michael Lehmann as director. Due to scheduling conflicts with Airheads, Lehmann had to vacate the directors position, which was taken over by Burton.
 Best Makeup Rick Baker (who designed Landaus prosthetic makeup), Ve Neill and Yolanda Toussieng.

==Plot== Variety magazine George Weiss Mike Starr) life story, Ed is inspired to meet Weiss in person. Weiss explains that Variety s announcement was a news leak, and it is impossible to purchase Jorgensens rights. The producer decides to "fictionalize" the film, titled I Changed My Sex!.  Ed tries to convince Weiss that he is perfect to direct I Changed My Sex! because he is a transvestite, but is unsuccessful since Weiss wants a director with experience.  One day, Ed meets his longtime idol Bela Lugosi (Martin Landau), after spotting him trying out a casket. Ed drives Bela home and the two become friends. Later, Wood persuades Weiss to let him direct I Changed My Sex! by convincing him that having a star in the film would sell tickets, and they could sign Bela for a low price.
 financing his next film independently. Ed is unsuccessful in finding money for Bride of the Atom, but is introduced to the psychic The Amazing Criswell (Jeffery Jones) who gives him advice on how to sell himself better.
 Loretta King wrap party because of his circle of friends and transvestism. Also, Bela, who is revealed to be highly depressed and a morphine addict, attempts to conduct a double suicide with Ed after the government cuts off his unemployment, but is talked out of it. Bela checks himself into drug rehabilitation|rehab, and Ed meets Kathy OHara (Patricia Arquette), who is visiting her father at the same hospital. Ed takes her on a date and reveals to her his transvestism, which she accepts.

Ed begins to shoot a film with Bela outside his home. When Ed and company attend the premiere for Bride of the Monster, an angry mob chases them out of the theater. Sometime later, Bela passes away, leaving Ed without a star. Ed convinces a church leader named Reynolds that funding Eds script for Grave Robbers from Outer Space would result in a box office success, and generate enough money for Reynolds dream project (the Twelve Apostles films). Dr. Tom Mason (Ned Bellamy), Kathys chiropractic|chiropractor, is chosen to be Belas stand-in for resembling Lugosi (with half his face covered). However, Ed and the Baptists begin having conflicts over the title and content of the script which they want to have changed to Plan 9 from Outer Space along with Eds B movie directing style, his casting decisions and his transvestism. This causes a distressed Ed to leave the set and immediately take a taxi to the nearest bar (establishment)|bar, where he encounters his idol Orson Welles (Vincent DOnofrio). Welles tells Ed that "visions are worth fighting for", and filming for Plan 9 finishes with Ed taking action against his producers. The film ends with the premiere of Plan 9 with Ed believing it will be the film he "will be remembered for", and Ed and Kathy taking off to Las Vegas, Nevada to get married.

==Cast==
*  , who had shown him Plan 9 from Outer Space and Glen or Glenda.  To get a handle on how to portray Wood, Depp studied the performance of Jack Haley as the Tin Man in The Wizard of Oz, and the acting of Mickey Rooney, Ronald Reagan and Casey Kasem.     He watched several Reagan speeches because the actor felt he had a kind of blind optimism that was perfect for Ed Wood. Depp also borrowed some of Kasems cadence and "that utterly confident, breezy salesman quality in his voice".  Rick Baker created the prosthetic makeup designs. Baker did not use extensive make-up applications, only enough to resemble Lugosi and allow Landau to use his face to act and express emotion.    For research, Landau watched 25 of Lugosis films and seven interviews between the years of 1931 and 1956.  Landau did not want to deliver an over-the-top performance. "Lugosi was theatrical, but I never wanted the audience to feel I was an actor chewing the scenery...I felt it had to be Lugosis theatricality, not mine." 
*  .
* Patricia Arquette as Kathy OHara: Eds girlfriend after his relationship with Dolores. Kathy does not have a problem with Eds transvestism, and is eventually married to Ed. Their marriage lasts until Eds death in 1978. She never remarried. Arquette met her real-life counterpart during filming. The actress found her to be "very graceful and very nice".   
*  . She is dismissive of Ed at first, but decides to join the cast of Plan 9 from Outer Space, on the condition that she have no lines.
*   TV entertainer. Criswell helps Ed with usual production duties, finding investors and acting in Eds films.
*  s and frequent actors. Paul is hired to find the Lugosi stand-in for Plan 9 from Outer Space, while Conrad accidentally has a brief dispute with Lugosi during Glen or Glenda.
*   friend. Bunny is assigned to find transvestites to appear in Glen or Glenda, as well as portraying the "The Ruler" in Plan 9 from Outer Space. Bunny also has an unsuccessful sex reassignment therapy attempt.
*   hired by Wood to be in two of his films, Bride of the Monster and Plan 9. Loretta King: King replaces Dolores in the movie "Bride of the Monster" after Wood mistakes her for an heiress able to front the money for the production costs.
*   who is chosen to be Lugosis stand-in for Plan 9.
*   producer known for his work on exploitation films. Weiss hires Ed to direct Glen or Glenda.
*   late in the film. Maurice LaMarche did Welles uncredited voice.
The film also includes cameos from actors who worked with Wood on Plan 9 from Outer Space, Gregory Walcott and Conrad Brooks.

==Production== biopic of Problem Child pitched the idea to Heathers director Michael Lehmann, with whom they attended USC film school.  The basis for their treatment came from Rudolph Greys Nightmare of Ecstasy,  a full-length biography, which draws on interviews from Woods family and colleagues.    Lehmann presented their treatment to his producer on Heathers, Denise Di Novi. Di Novi had previously worked with Tim Burton on Edward Scissorhands, Batman Returns and The Nightmare Before Christmas, and a deal was struck with Lehmann as director and Burton and Di Novi producing. 
 Mary Reilly for Columbia Pictures with Winona Ryder in the title role. 

However, Burton dropped out of Mary Reilly over Columbias decision to fast track the film and their interest with Julia Roberts in the title role instead of Ryder. This prompted Burton to becoming interested in directing Ed Wood himself, on the understanding it could be done quickly.  Lehmann said, "Tim wanted to do this movie immediately and direct, but I was already committed to Airheads."    Lehmann was given executive producer credit. Alexander and Karaszewski delivered a 147-page screenplay in six weeks. Burton read the first draft and immediately agreed to direct the film as it stood, without any changes or rewrites. Salisbury, Burton, pp. 128-130  Ed Wood gave Burton the opportunity to make a film that was more character-driven as opposed to style-driven. He said in an interview, "On a picture like this I find you dont need to storyboard. Youre working mainly with actors, and theres no effects going on, so its best to be more spontaneous."   
 optioning the Walt Disney Studios, who had previously produced The Nightmare Before Christmas. Similar to Nightmare, Disney released Ed Wood under their Touchstone Pictures banner. With a budget of $18 million, Disney did not feel the film was that much of a risk, and granted Burton total creative autonomy. Burton also refused a salary, and was not paid for his work on Ed Wood. Principal photography began in August 1993,  and lasted 72 days.  Despite his previous six-film relationship with Danny Elfman, Burton chose Howard Shore to write the film score. Under the pressure of finishing the score for Batman Returns, the relationship between the two strained {{cite web | url = 
http://www.smh.com.au/entertainment/movies/danny-elfman-presents-his-tim-burton-movie-scores-at-adelaide-festival-20141016-116qbg.html | title = Danny Elfman presents his Tim Burton movie scores at Adelaide Festival | work = Sydney Morning Herald | accessdate =October 16, 2014}}  and Burton admitted he and Elfman experienced "creative differences" during The Nightmare Before Christmas Salisbury, Burton, pp.137-144  

The movie was filmed at various locations in and around the Los Angeles area. 

==Historical accuracy==
When describing the films accuracy, Burton explained, "its not like a completely hardcore realistic biopic. In doing a biopic you cant help but get inside the persons spirit a little bit, so for me, some of the film is trying to be through Ed a little bit. So its got an overly optimistic quality to it."  Burton acknowledged that he probably portrayed Wood and his crew in an exaggeratedly sympathetic way, stating he did not want to ridicule people who had already been ridiculed for a good deal of their life. Burton decided not to depict the darker side of Woods life because his letters never alluded to this aspect and remained upbeat. To this end, Burton wanted to make the film through Woods eyes.  He said in an interview, "Ive never seen anything like them, the kind of bad poetry and redundancy– saying in, like, five sentences what it would take most normal people one   Yet still there is a sincerity to them that is very unusual, and I always found that somewhat touching; it gives them a surreal, weirdly heartfelt feeling."   

The scenes of Bela Lugosi used for Plan 9 from Outer Space were not filmed outside his own house, as the film depicts. They were, in fact, filmed outside Tor Johnsons house.  George Weiss about making I Changed My Sex!, Weiss mentions Chained Girls, implying that Chained Girls was made before Glen or Glenda, when in fact Chained Girls was made afterwards.

Burton biographer Ken Hanke criticized the depiction of Dolores Fuller. "The real Fuller is a lively, savvy, humorous woman," Hanke said, "while Parkers performance presents her as a kind of sitcom moron for the first part of the film and a rather judgmental and wholly unpleasant character in her later scenes."  During her years with Wood, Fuller had regular TV jobs on Queen for a Day and The Dinah Shore Show, which are not mentioned. Fuller criticized Parkers portrayal and Burtons direction, but still gave Ed Wood a positive review. "Despite the dramatic liberties, I think Tim Burton is fabulous. I wished they could have made it a deeper love story, because we really loved each other. We strove to find investors together, I worked so hard to support Ed and I." 

==Release==
Ed Wood had its premiere at the 32nd New York Film Festival at the Lincoln Center.  The film was then shown shortly after at the 21st Telluride Film Festival  and later at the 1995 Cannes Film Festival, where it was in competition for the Palme dOr.    

=== Home media ===
The DVD edition of Ed Wood initially had difficulty reaching store shelves in North America due to unspecified legal issues. The initial release had a featurette on transvestites — not relating to the film or its actors in any way — which was removed from subsequent releases. An initial street date of August 13, 2002 was announced  only to be postponed.  A new date of February 3, 2003 was set,  only for it to be recalled again without explanation, although some copies quickly found their way to collectors venues such as eBay. The DVD was finally released on October 19, 2004. 

== Reception ==

===Box office===
Ed Wood had its limited release on September 30, 1994. When the film went into wide release on October 7, 1994 in 623 theaters, Ed Wood grossed $1,903,768 in its opening weekend.    The film went on to gross $5,887,457 domestically,   much less than the production budget of $18 million. 

===Critical response===
Ed Wood received positive reviews from critics and has a rating of 92% on Rotten tomatoes based on 60 reviews with an average score of 8 out of 10. The consensus states "Tim Burton and Johnny Depp team up to fete the life and work of cult hero Ed Wood, with typically strange and wonderful results."  The film also has a score of 70 out of 100 on Metacritic based on 19 reviews.  

 s, in which a great title, a has-been star and a lurid ad campaign were enough to get bookings for some of the oddest films ever made." {{cite news | author = Roger Ebert | title = Ed Wood | work = Chicago Sun-Times | url = 
http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19941007/REVIEWS/410070301/1023 | date = 1994-10-07 | accessdate = 2008-11-28}}   Ebert and Gene Siskel gave the film Two Thumbs Up on Siskel and Ebert, with Siskel calling it "a tribute to creative passion" and "one of the years very best".

Peter Travers of Rolling Stone presented Burtons decision with positive feedback on not making a direct satire or parody of Woods life. "Ed Wood is Burtons most personal and provocative movie to date," he wrote. "Outrageously disjointed and just as outrageously entertaining, the picture stands as a successful outsiders tribute to a failed kindred spirit." 

Janet Maslin, writing in The New York Times, thought Johnny Depp "proved" himself as an established "certified great actor". "Depp captures all the can-do optimism that kept Ed Wood going, thanks to an extremely funny ability to look at the silver lining of any cloud."  Todd McCarthy from Variety (magazine)|Variety called Ed Wood "a fanciful, sweet-tempered biopic about the man often described as the worst film director of all time. Always engaging to watch and often dazzling in its imagination and technique, picture is also a bit distended, and lacking in weight at its center. The result is beguiling rather than thrilling."  
 Capraesque hero with little to be optimistic about, since he was also a classic American loser. Thats a fine start, but the film then marches in staid chronological order." Corliss continued, "One wonders why this Burton film is so dishwatery, why it lacks the cartoon zest and outsider ache of Beetlejuice, Edward Scissorhands or Batman Returns." 

===Accolades=== Best Actor Best Supporting Rick Baker Best Supporting Best Screenplay Grand Prix of the Belgian Syndicate of Cinema Critics. 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 