Focus (2001 film)
{{Infobox film
| name           = Focus
| image          = Focus poster.jpg
| image_size     =
| caption        =
| director       = Neal Slavin Robert A. Miller, Michael R. Bloomberg, and Neal Slavin
| writer         = Arthur Miller (novel), 
| screenplay     = Kendrew Lascelles 
| narrator       =
| starring       = {{Plain list | 
* William H. Macy
* Laura Dern
* David Paymer
* Meat Loaf
}}
| music          =
| cinematography = Juan Ruiz Anchía Tariq Anwar  David B. Cohn
| distributor    = Paramount Classics
| released       = 9 September 2001
| runtime        = 106 minutes
| country        = United States English
| budget         =
| gross          =
| website        =
}}
 1945 novel by playwright Arthur Miller.

==Plot==
In the waning months of World War II, a man is mistakenly identified as a Jew by his antisemitic Brooklyn neighbors. Suddenly the victims of religious and racial persecution, he finds himself aligned with a local Jewish immigrant in a struggle for dignity and survival.

==Cast==
* William H. Macy as Lawrence Larry Newman
* Laura Dern as Gertrude Gert Hart
* David Paymer as Mr. Finkelstein
* Meat Loaf as Fred (as Meat Loaf Aday)
* Kay Hawtrey as Mrs. Newman
* Michael Copeman as Carlson
* Kenneth Welsh as Father Crighton
* Joseph Ziegler as Mr. Garage
* Arlene Meadows as Mrs. Dewitt
* Peter Oldring as Willy Doyle
* Robert McCarrol as Meeting Hall Man (as Robert Mccarrol)
* Shaun Austin-Olsen as Sullivan
* Kevin Jubinville as Mr. Cole Stevens
* B.J. McQueen as Mel
* Conrad Bergschneider as Toughs Leader

==Reception==
===Critical response===
Rotten Tomatoes gave the film a rating of 56% based on 81 reviews, with the sites consensus "Though full of good intentions, Focus somehow feels dated, and pounds away its points with a heavy hand." 

==References==
 
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 

 