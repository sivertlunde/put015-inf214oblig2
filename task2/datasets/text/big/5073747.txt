The Oklahoma Kid
{{Infobox film
| name = The Oklahoma Kid
| image = Theoklahomakid.png
| director = Lloyd Bacon
| producer = Samuel Bischoff, Hal B. Wallis
| writer = Wally Kline  Edward E. Paramore Rosemary Lane
| music = Max Steiner  Stephen Foster
| cinematography = James Wong Howe
| editing = Owen Marks
| distributor = Warner Bros. Pictures
| released =  
| runtime = 85 min English
| budget =
}}  1939 western film starring James Cagney and Humphrey Bogart.  The movie was directed for Warner Bros. by Lloyd Bacon.  Cagney plays an adventurous gunslinger in a broad-brimmed cowboy hat while Bogart portrays his black-clad and viciously villainous nemesis.  The film is often remembered for Cagneys character rubbing the thumb and forefinger of his hand together and exulting, "Feel that air!"
 Rosemary Lane, Donald Crisp, and Ward Bond. Rosemary Lanes sister Priscilla Lane also starred with Cagney and Bogart in The Roaring Twenties that same year.

==Cast==
*James Cagney as Jim Kincaid (The Oklahoma Kid)
*Humphrey Bogart as Whip McCord  Rosemary Lane as Jane Hardwick 
*Donald Crisp as Judge Hardwick 
*Harvey Stephens as Ned Kincaid 
*Hugh Sothern as John Kincaid  Charles Middleton as Alec Martin 
*Edward Pawley as Ace Doolin 
*Ward Bond as Wes Handley 
*Lew Harvey as Ed Curley 
*Trevor Bardette as Indian Joe Pasco 
*John Miljan as Ringo (the lawyer)
*Stuart Holmes as Grover Cleveland (uncredited)

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 


 