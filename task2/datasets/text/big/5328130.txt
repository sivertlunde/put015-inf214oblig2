Nýtt líf
 
{{Infobox film|
  name     = Nýtt Líf |
  image          = Nýtt_Líf_(DVD).jpg |
  director       = Þráinn Bertelsson |
  producer       = Jón Hermannsson |
  writer         = Þráinn Bertelsson |
  starring       = Eggert Þorleifsson Karl Ágúst Úlfsson  |
  distributor    = Nýtt Líf ehf. |
  released   = 4 September 1983  , More Icelandic Films, Icelandic Film Centre  | min | Icelandic |
  music          = Various |
  budget         = ??? |
}}
 Westman Islands and stars Eggert Þorleifsson and Karl Ágúst Úlfsson, among others. The music features several musicians including the band Tappi Tíkarrass (of which Björk was a member), which contributed the songs "Sperglar" and "Kukl" (a.k.a. "Seiður").

==Synopsis==
The film is the first of three about the misadventures of two friends, Þór and Danni.   Two friends working at a restaurant get fired from their jobs and after seeing an advertisement for the fishing industry of the Westman Islands, decide to go there and make some money. The islands become a perfect setting for many funny and strange situations, especially after word gets around that the two are spies from the Ministry of Fisheries. 

==Full cast==
Karl Ágúst Úlfsson: Daníel Ólafsson. 
Eggert Þorleifsson: Þór Magnússon. 
Runólfur Dagbjartsson: Víglundur, work manager. 
Eiríkur Sigurgeirsson: Axel, best employee. 
Sveinn Tómasson: Ási, the shipper. 
Guðrún Kolbeinsdóttir: María Víglundsdóttir. 
Elva Ósk Ólafsdóttir: Miss Snæfells and Hnappadalssýsla. 
Ingveldur Gyða Kristinsdóttir: Silja, Axels’ girlfriend. 
Magnús S. Magnússon: Sigurður mayonese. 
Frímann Lúðvíksson: Júlli, the house keeper. 
Hlynur Ólafsson: Alli, guest at the ball at Alþýðuhúsið. 
Guðlaug Bjarnadóttir: Magga, Þór’s ex-wife. 
Sigurður Hallmarsson: middle-aged shop manager. 
Sólrún Yngvadóttir: Woman on balcony. 
Unnur Guðjónsdóttir 
Sigurgeir Scheving 
Einar Hallgrímsson 
Kolbrún Halfdánardóttir 
Hörður Haraldsson 
Sveinbjörn Friðjónsson 
Baldur Sæmundsson 
Aðalheiður Sveinsdóttir 
Elva Kolbeinsdóttir 
Auður Björgvinsdóttir 
María G. Hallgrímsdóttir 
Elín Magnúsdóttir 
Harpa Kolbeinsdóttir 
Jóhannes Ágúst Stefánsson 
Elías Bjarnhéðinsson 
Páll Scheving  Graham Smith 
Jónas Þórir Jónasson 
Tappi Tíkarrass (Björk|Björk Guðmundsdóttir - vocals. Jakob Smári Magnússon - bass. Eyjólfur Jóhannsson - guitar. Guðmundur Þór Gunnarsson).

==Credits==
Direction: Þráinn Bertelsson. 
Production: Jón Hermannsson. 
Writer: Þráinn Bertelsson. 
Cinematography: Ari Kristinsson. 
Film edition: Ari Kristinsson.
Set decoration: Magnús S. Magnússon. 
Costume: Marta Bjarnadóttir and Jón Karl Helgason. 
Make-up department: Jón Karl Helgason. 
Assistant director: Sigurgeir Scheving.  Brian Pilkington. 
Sound: Jón Hermannsson. 
Sound recording assistant: Jóhannes Jónsson. 
Sound re-recording mixer: Aad Wirtz. 
Assistant camera: Jón Karl Helgason and Þorsteinn Þorsteinsson. 
Music: Ágúst Pétursson, Ási í Bæ, Guðmundur Rúnar Lúðvíksson, Oddgeir Kristjánsson, Sigfús Halldórsson, Svavar Benediktsson, Tappi Tíkarrass (Björk Guðmundsdóttir - vocals. Jakob Smári Magnússon - bass. Eyjólfur Jóhannsson - guitar. Guðmundur Þór Gunnarsson). 
Music advisor: Megas.

==References==
 

==External links==
* 
*  

 
 
 
 

 