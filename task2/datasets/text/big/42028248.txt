Rojavai Killathe
 

{{Infobox film
| name           = Rojavai Killathe
| image          = 
| image_size     =
| caption        = 
| director       = Suresh Krissna
| producer       = K. Sukumar
| writer         = M. S. Madhu  (dialogues) 
| screenplay     = Suresh Krissna
| story          = Ananthu
| starring       =   Deva
| cinematography = P. S. Prakash
| editing        = Gowtham Raju
| distributor    =
| studio         = Sukumar Art Combines
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1993 Tamil Tamil crime Arjun and Deva and was released on 3 December 1993.  

==Plot==

Alexander (Arjun Sarja|Arjun) is a henchman who works for the smuggler Peter (Tiger Prabhakar) and Alexander is considered by Peter as his faithful henchman. Peter often confronts his enemy Ayyanar (Radha Ravi). Ayyanar forces the innocent Anu (Kushboo) to dance in stage and he ill-treats her. Peter orders Alexander to kill the Ayyanars weak point Anu. Alexander cannot kill her and finally shoots on her shoulder. Wounded, Anu is admitted to hospital. Then, Alexander decides to save her from Peter and Ayyanar.

==Cast==
 Arjun as Alexander (Duraipandi)
*Kushboo as Anu
*Radha Ravi as Ayyanar
*Tiger Prabhakar as Peter
*Goundamani as David Senthil as Thandavarayan
*Vadivelu
*Sarath Babu as Sathasivam, Alexanders brother
*Venniradai Moorthy
*Arunkumar
*Oru Viral Krishna Rao
*Ra. Sankaran as Anus father Renuka as Sathasivams wife
*Sharmili as Davids wife
*Suryakala
*LIC Narasimman as a doctor
*Nalini Kanth
*Master Robert as Naina

==Soundtrack==

{{Infobox Album |  
| Name        = Rojavai Killathe
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1993
| Recorded    = 1993 Feature film soundtrack |
| Length      = 23:33
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1993, features 5 tracks with lyrics written by Vairamuthu.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Arthamulla Pattu || S. P. Balasubrahmanyam, K. S. Chithra, Baby Sujatha || 5:02
|- 2 || Moodiko Moodiko || S. P. Balasubrahmanyam, Swarnalatha || 4:57
|- 3 || Nee Oru Pakkam || S. P. Balasubrahmanyam, K. S. Chithra || 4:33
|- 4 || Onnachi Rendachi || Swarnalatha || 3:57
|- 5 || Yamuna Nathi Karaiyil || K. S. Chithra || 5:04
|}

==References==
 

==External links==

 

 
 
 
 
 
 


 