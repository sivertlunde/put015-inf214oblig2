The Wanted 18
 
{{Infobox film
 | name           =
 | image          = The Wanted 18 Poster.jpg
 | alt            = The Wanted 18 Official Poster
 | caption        = The Wanted 18 Official Poster
 | film name      = The Wanted 18
 | director       = {{Plainlist|
* Amer Shomali Paul Cowan
 }}
 | producer       = {{Plainlist|
* Saed Andoni
* Ina Fichman
* Dominique Barneaud
* Nathalie Cloutier
  }}
 | based on       = true story
 | music          = Benoit Charest
 | cinematography = Daniel Villeneuve, Germán Gutierrez
 | distributor    = NFB, Kino Lorber
 | released       =  
 | runtime        = 75 minutes
 | country        = Palestine, Canada, France
 | language       = Arabic, English, Hebrew
}}
 Paul Cowan and Palestinian visual artist and director Amer Shomali.   

==Background==
In the 1980s, as part of a Palestinian boycott of Israeli taxation and commodities, residents of Beit Sahour decided to form a collective and stop purchasing milk from Israeli companies, in a quest for greater self-sufficiency. They purchased cows from a sympathetic kibbutznik and set about teaching themselves how to care for the animals and milk them—even sending a member to the United States to learn dairy farming. The farm was a success, with strong local demand for “Intifada milk.” However, the herd was declared a “threat to the national security of the state of Israel” and Israel sought to impound the cows, forcing Palestinians to devise ways to keep them hidden.          

== Animated sequences and visual style ==
 
The film is framed as being told from the point of view of the cows—Rikva, Ruth, Lola and Goldie— who appear in humorous Claymation animated sequences.   The filmmakers intended The Wanted 18 to have a comic book feel, even shooting live-action interviews at an angle to replicate the look of comic book panels. 

==Production==
The idea for the film began in Shomali’s boyhood, spent largely at a Syrian refugee camp where his main escape had been reading comic books, one of which dealt with the story of the  Beit Sahour cows. Montreal-based producer Ina Fichman first heard of the story when a group of producers and broadcasters at a documentary-pitch event Ramallah. Shomalis original intention had been to make a short animated film on the story. However, Fichman believed it had the makings of a feature documentary and approached veteran Montreal-based documentary filmmaker Paul Cowan. The project took nearly five years to complete—a lengthy process due to the time involved in creating the animation as well as the fact that Shomali and his Canadian collaborators lived thousands of miles apart. 

Interviewed in the film are Jalal Oumsieh, a schoolteacher who had purchased the 18 cows, geology professor Jad Ishad, pharmacist Elias Rishmawi and butcher Virginia Saad. The film also interviews two members of the Israeli government: Shaltiel Lavie, then-military governor of the region, and Ehud Zrahiya, his Arab affairs adviser. 

The film score is composed by Benoît Charest. The Wanted 18 is a co-production of Intuitive Pictures, the National Film Board of Canada, Bellota Films and Dar Films Productions. The producers are Fichman and Nathalie Cloutier.  The film  received funding from the Beirut-based Arab Fund for Arts and Culture (AFAC).  and from SANAD, Abu Dhabi film festival.

==Release==
The film had its world premiere at the 2014 Toronto International Film Festival and was screened to a capacity crowd in Ramallah.  Other festival screenings as of the fall of 2014 include the Abu Dhabi Film Festival, where the film received the award for Best Documentary from the Arab World,    the Rencontres internationales du documentaire de Montréal,  as well as the 2014 Carthage Film Festival, where the film received its Golden tanit for best documentary film.

==References==
 

==External links==
* 
*  at the Toronto International Film Festival

 
 
 
 
 
 
 
 
 
 
 
 
 
 