Birth Story: Ina May Gaskin and the Farm Midwives
{{Infobox film
| name           = Birth Story: Ina May Gaskin and the Farm Midwives
| image          = 
| caption        = 
| director       = Sara Lamm Mary Wigmore
| producer       = Sara Lamm Mary Wigmore Kate Roughan Zachary Mortensen
| writer         = 
| starring       = Ina May Gaskin
| music          = Robin Pecknold
| cinematography = 
| editing        = 
| distributor    = 
| studio         = Ghost Robot/Reckon So Productions
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Birth Story: Ina May Gaskin and the Farm Midwives is a 2012 documentary film about Ina May Gaskin directed by Sara Lamm and Mary Wigmore.

==Plot==
The documentary shows Gaskin and others practicing home birth at a commune. Today as nearly one third of all US babies are born via C-section; the struggle is to protect their midwifery knowledge and to promote respectful, safe maternity practices. 

From the backs of school buses, these birth activists rescue American midwifery from extinction and change the way a generation approaches pregnancy. With unprecedented access to the midwives archival video collection, as well as modern footage of life at the alternative community where they live, this documentary shows childbirth the way most people have never seen it.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 

 