Last Days (2005 film)
 
{{Infobox film
| name           = Last Days
| image          = Lastdaysposter.jpg
| image_size     = 220px
| alt            = 
| caption        = Theatrical release poster
| director       = Gus Van Sant
| producer       = Gus Van Sant Dany Wolf
| writer         = Gus Van Sant
| starring       = Michael Pitt Lukas Haas Asia Argento Scott Patrick Green
| music          = Rodrigo Lopresti Michael Pitt
| cinematography = Harris Savides
| editing        = Gus Van Sant
| studio         = HBO Films Picturehouse Films
| released       =  
| runtime        = 96 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $2,456,454 
}} foreign and documentary films. The film received mixed-to-negative reviews. It is meant to be based on Kurt Cobain, but contradicts the factual evidence of Cobains final days.

==Plot==
Introspective artist Blake is buckling under the weight of fame, professional obligations and a mounting feeling of isolation. Dwarfed by towering trees, Blake slowly makes his way through dense woods. He scrambles down an embankment to a fresh spring and undresses for a short swim. The next morning he returns to his house, an elegant, if neglected, stone mansion. Many people are looking for Blake—his friends, his managers and record label, even a private detective—but he does not want to be found. In the haze of his final hours, Blake will spend most of his time by himself. He avoids the people who are living in his house, who approach him only when they want something, be it money or help with a song. He hides from one concerned friend and turns away another. He visits politely with a stranger from the Yellow Pages sales department, and he ducks into an underground rock club. He wanders through the woods and later plays a new song, one last rock and roll blowout. Finally, he stumbles into the greenhouse next to his mansion. Soon after his friends leave, while looking back into the greenhouse one of the friends looks in from the car to notice a man in an orange jumpsuit. The next morning he is found dead by a worker. A spectre of the musician, naked, slowly climbs out of his dead body and up the ladder possibly to the "nirvana" waiting above.

==Cast==
 

* Michael Pitt as Blake
* Lukas Haas as Luke
* Asia Argento as Asia
* Scott Patrick Green as Scott
* Nicole Vicius as Nicole
* Ricky Jay as Detective
* Ryan Orion as Donovan
* Director and Van Sant colleague Harmony Korine
* Rodrigo Lopresti as a member of the Band in Club
* Kim Gordon as Record executive
* Adam and Andy Friberg as Elders Friberg 
* Thadeus A. Thomas as Yellow Pages salesman

==Relation to other Van Sant films==
  as Guy In Club]] Death Trilogy", Paranoid Park, a later film by Van Sant, the same technique is used.

==Production==
===Background===
Van Sant has stated he had contemplated the project for nearly a decade.  At one time, he wanted to do a  . "

===Music===
Last Days features two original compositions by Michael Pitt, an acoustic song entitled "Death to Birth", and an electric jam called "That Day". Another piece, "Untitled", is by Lukas Haas.  .

===Filming===
The film was shot in the Hudson Valley region of New York, which, due in part to cinematographer Harris Savides specialized treatment of the film stock, suggests the Pacific Northwest, where Van Sant is from. 

==Reception==
Last Days received mixed reviews, including The Village Voice and The New York Times. It currently holds a 57% approval rating on the website Rotten Tomatoes.

===Awards===
The film was entered in the 2005 Cannes Film Festival where it won the Technical Grand Prize.    It was nominated for an Independent Spirit Award for Best Cinematography, but failed to win any awards at the Independent Spirit Awards|festival.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 