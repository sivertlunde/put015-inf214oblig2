Pigskin Parade
{{Infobox film
| name = Pigskin Parade
| image = Pigskinparademp.jpg
| caption = Promotional movie poster for the film David Butler
| producer = Bogart Rogers Darryl F. Zanuck
| writer = William M. Conselman Mary Kelly Nat Perrin Arthur Sheekman Harry Tugend Jack Yellen
| starring = Stuart Erwin Patsy Kelly Jack Haley Betty Grable Judy Garland
| music = David Buttolph
| cinematography = Arthur C. Miller
| editing = Irene Morra
| released =  
| runtime = 93 minutes
| country = United States
| language = English
}} David Butler.
 Tony Martin and, in her feature film debut, 14-year-old Judy Garland. 20th Century Fox distributed this film.

==Plot== Yale inadvertently invites the small Texas State University to come to Connecticut and play against its football team for a benefit game. Coincidentally, TSU has just hired a new coach, Slug Winters (Jack Haley), who arrives at the college with his wife Bessie (Patsy Kelly) just in time to hear the announcement that the team is to play Yale.

The coach digs in to whip the team into shape, with Bessies help, she knowing more about football than Slug does. But just before the big game, Bessie causes an accident and the teams quarterback Biff Bentley breaks his leg. All seems hopeless until Slug and Bessie stumble across an Arkansas hillbilly named Amos Dodd, played by Stuart Erwin, who throws a football like no one theyve ever seen. They find him tossing melons with his sister, Sairy (Judy Garland).

The only problem remaining is to figure a way to get the college to enroll the hillbilly so that he can take the place of the injured quarterback. Amos also falls for attractive student Sally Saxon (Arline Judge), bringing out jealousy in her rich suitor Mortimer Higgens.

Texas State travels to the game at Yale, which is played in a blizzard. Yale is leading 7-6 in the final minutes when Slug accidentally knocks himself unconscious on the sideline. Bessie takes over and sends in a play, which hillbilly Amos runs barefoot for the winning touchdown. 

==University in film== Texas State University did not receive that name until 2003. The logo on the football players uniforms is the logo of Tarleton State University  in Stephenville Texas. In the 20s, 30s, 40s and into the 50s Tarleton played Texas A&M and Oklahoma university as shown in the movie.

==Cast==
* Stuart Erwin as Amos
* Jack Haley as Slug
* Patsy Kelly as Bessie
* Arline Judge as Sally
* Grady Sutton as Mortimer
* Fred Kohler, Jr. as Biff Tony Martin as Tommy
* Betty Grable as Laura
* Judy Garland as Sairy

==Award nominations==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- bgcolor="#B0C4DE" align="center" Year
! Award
! Result
! Category
! Recipient
|-  Best Actor in a Supporting Role || Stuart Erwin 
|}

==References==
 

==External links==
*   
*  
*  
*  
*  at New York Times
 

 
 
 
 
 
 
 
 
 
 