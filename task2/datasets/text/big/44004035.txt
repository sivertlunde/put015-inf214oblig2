Pengal
{{Infobox film 
| name           = Pengal
| image          = Pengal.jpg
| image_size     =
| caption        =
| director       = AK Sahadevan
| producer       =
| writer         = MK Sahadevan Varghese Tholathu (dialogues)
| screenplay     = MK Sahadevan Sathyan T. S. Muthaiah Kottayam Chellappan MS Namboothiri
| music          = KV Job George
| cinematography = PK Madhavan Nair
| editing        = KD George
| studio         = Renowned Films
| distributor    = Renowned Films
| released       =  
| country        = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, directed by AK Sahadevan . The film stars Sathyan (actor)|Sathyan, T. S. Muthaiah, Kottayam Chellappan and MS Namboothiri in lead roles. The film had musical score by KV Job and George.   

==Cast== Sathyan
*T. S. Muthaiah
*Kottayam Chellappan
*MS Namboothiri
*Madhumathi
*S. P. Pillai

==Soundtrack==
The music was composed by KV Job and George and lyrics was written by TP Sukumaran, Santhakumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaarmukilolivarnna || P. Leela || TP Sukumaran, Santhakumar || 
|-
| 2 || Kannupotthikkalikkunna || K. J. Yesudas, LR Eeswari || TP Sukumaran, Santhakumar || 
|-
| 3 || Raararo Raariraro || S Janaki || TP Sukumaran, Santhakumar || 
|-
| 4 || Thedukayaanellaarum || CO Anto || TP Sukumaran, Santhakumar || 
|-
| 5 || Vyaamoham || K. J. Yesudas || TP Sukumaran, Santhakumar || 
|}

==References==
 

==External links==
*  

 
 
 

 