Makaramanju
{{Infobox film
| name           = Makaramanju
| image          = Makaramanju.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Lenin Rajendran
| producer       = Lenin Rajendran B. Rakesh   
| writer         = Lenin Rajendran Bala Chithra Iyer Saiju Kurup
| music          = Ramesh Narayan
| cinematography = Madhu Ambat
| editing        = Mahesh Narayanan
| studio         = 
| distributor    = Sree Gokulam Films
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}
Makaramanju ( ) is a 2011 Malayalam language romantic drama film written and directed by Lenin Rajendran. The film is about celebrated painter Raja Ravi Varmas life at a certain stage in his life. It also narrates the story of epic character Pururavas. Cinematographer Santhosh Sivan and Karthika Nair appears in the lead roles.

"Urvashi Pururavas" is the painting that has been woven into the narrative of the film. In a story-within-a-story format, Lenin Rajendran intertwines the story of the artist with that of the mythological Urvashi and her beloved King Pururavas. The film received mixed reviews upon release. It was one of the five Malayalam films to be screened at the International Film Festival of India.  It won the FIPRESCI Prize for Best Malayalam Film at the International Film Festival of Kerala.  It has been nominated for the Indian National Film Awards for 2010. 

==Plot==
The film tells the story of Raja Ravi Varma at a certain stage in his life. He is in the process of painting a masterpiece. The theme of his painting is Pururavas, the legendary king who fell in love with the heavenly nymph Urvashi, who later agrees to become his wife on certain conditions, but disappears without a trace when she discovers that the conditions were violated. Pururavas wanders all around to find her and ultimately does get united with his lover. Ravi Varma, during his work, finds himself attracted to his model Sugandha Bai and this relationship begin to acquire certain shades of the legend of Urvashi and Pururavas. Together they are thrown into a torrent of love and passion from which they find it difficult to scape.

==Awards==
International Film Festival of Kerala 2010 
* FIPRESCI Prize for Best Malayalam Film 
Kerala State Film Awards  2010
* Kerala State Film Award for Second Best Film
Kerala Film Critics Association Awards 2010
*  Best Debut Actress - Karthika Nair
Vanitha Film Award 2010
*  Best Debut Actress - Karthika Nair
1st South Indian International Movie Awards 2010
*  Best Debut Actress - Karthika Nair
*  Best Cinematographer - Madhu Ambat

==Cast==
* Santhosh Sivan as Raja Ravi Varma and Pururavas. Cinematographer Santhosh Sivan is on his acting debut through this film.  He dons dual roles in the film, and is pairing up with three different heroines.  Coincidentally, Santhosh Sivan had made use of themes from Ravi Varmas paintings - Damayanti and the swan, Lady in thought and Girl carrying milk tray in his directorial venture Anandabhadram.  
* Karthika Nair as Suganda Baai and Urvashi. Karthika dons her second full length role through this film,  as well as makes her Malayalam debut.  The film is considered to be a breakthrough in the actresss career and there have been reports that Academy Award-winning British director Danny Boyle was impressed with Karthikas  performance in the film and has signed her in his next project. 
* Lakshmi Sharma as Bhageerathi, Ravi Varmas wife.  . Nowrunning.com. Retrieved 17 February 2011. 
* Mallika Kapoor as Vasundhara, wife of Pururavas.
* Jagathy Sreekumar as Govardhandas Makhanji, Ravi Varmas business partner.  Raja Raja Varma, Ravi Varmas younger brother. 
* Chithra Iyer as Rukku Baai, Sugandas mother. 
* Nithya Menon as a model. Shamna Kassim as a model.
* Dinesh Panicker as Divan
* Chitra Iyer as Sugandha

==Voice dubbing==
*Actor Biju Menon has dubbed the voice for Santhosh Sivan.
*Bhagyalakshmi - Bhageerathy-(Lekshmi Sharma)
*Praveena- vasundhara (Malika Kapoor)
*Vimmy Mariam George - Urvashi/ Sugandhitha Bhai (Karthika Nair)



==Production==
Lenin Rajendran was the first to make a film on Swati Tirunal, the royal composer from Kerala. And through Makaramanju, the filmmaker pays homage to Raja Ravi Varma. The director says it is the artists special place in Indian art that had drawn him to his life. "A few years ago, I had done a play on Ravi Varma for Kerala Peoples Arts Club|KPAC. During my reading and research on him, I was fascinated to know more about the man and the circumstances in which those works were created. Here was a prince who had paid a price for pursuing his passion for the arts. He faced hostility and criticism but that did not put an end to his affair with brush and paint. Cases were filed against him but he fought against the orthodoxy and won the right to express himself. I felt there was space for a movie," he explains. Instead of a biopic on the artist, the director wanted to focus on the artist at work, and chose a single work to unfold the life of the artist and the period he worked and lived in. "Although his style was European, his themes were from Indian mythology. In each of his works on the Indian Puranas, he chose a point in the story that was filled with drama and emotion. I was stuck by the sensitivity and acumen of the man who used his brush to depict those moments. That is why I felt I could understand him better if I concentrated on a work and then narrate the story of the artists creative process," says the director. 

The principal production began in October 2009. The films set has been put up in an old bungalow known as Church bungalow on the compound of a Lutheran church, near Pallichal in Thiruvananthapuram district. The film was also shot in Kochi and Mumbai.   

==Music==
{{Infobox album|  
| Name = Makaramanju
| Type = Soundtrack
| Artist = Ramesh Narayan
| Cover = Makaramanjuaudio.jpg
| Released = 2010
| Recorded =  Feature film soundtrack
| Label = M. C. Audios & Videos
| Last album = 
| This album = 
| Next album = 
}}
The films music has been composed by Ramesh Narayan. There are eight tracks in the soundtrack, with lyrics by Kavalam Narayana Panicker, K. Jayakumar and Chandran Nair.  

{{tracklist
| headline     = 
| extra_column = Artist(s)
| writing_credits = yes
| total_length =
| title1       = Manjil Melle
| extra1       = K. J. Yesudas
| length1      = 
| writer1      = Chandran Nair
| title2       = Aahko Chahiye Hariharan & Sujatha
| length2      = 
| writer2      = K. Jayakumar
| title3       = Mele Mele
| extra3       = Ramesh Narayan & Anuradha Sriram
| length3      = 
| writer3      = Kavalam Narayana Panicker
| title4       = Then Thennale
| extra4       = Srinivas (singer)|Srinivas, Sunita Menon
| length4      = 
| writer4      = Kavalam Narayana Panicker
| title5       = Kaanuvan Vaiki
| extra5       = Hariharan & Sujatha
| length5      = 
| writer5      = K. Jayakumar
| title6       = Mele Mele
| extra6       = Ramesh Narayan
| length6      = 
| writer6      = Kavalam Narayana Panicker
| title7       = Mosobathiya Manjari
| length7      = 
| writer7      = Traditional
| title8       = Saalabhanjike
| extra8       = Ramesh Narayan
| length8      = 
| writer8      = Chandran Nair
}}

==Reviews==
The film received mixed reviews from various critics. A review by Nowrunning.com writes: "Makaramanju loses grip on its material half way through, and remains an elusive piece that doesnt tug at your heart strings. Which is why, you walk out of the hall mumbling to yourself that perhaps a bit more clever scripting could have made all the difference."  Rediff.coms reviewer said that the film "is watchable but you have to brush up your knowledge of mythology and history to fully appreciate it." 

== Accolades ==

{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 1st South Indian International Movie Awards SIIMA Award Best Cinematographer Madhu Ambat
| 
|-
|}

==References==
 

==External links==
*  
*  
*   at the Malayalam Movie Database

 
 
 
 
 
 
 