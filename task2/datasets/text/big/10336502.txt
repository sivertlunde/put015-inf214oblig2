Bon Voyage! (1962 film)
{{Infobox film
| name           = Bon Voyage!
| image          = Bon Voyage - 1963 - Poster.png
| caption        = 1962 Theatrical Poster
| director       = James Neilson Ron Miller Bill Walsh James A. Herne Charles Kenyon based on = novel by Joseph & Merrjane Hayes
| starring       = Fred MacMurray Jane Wyman Deborah Walley Tommy Kirk Kevin Corcoran
| music          = 
| cinematography = William E. Snyder
| editing        = Cotton Warburton Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 130 minutes
| country        = United States
| language       = English
| budget         = $3 million DISNEY & FAMILY ROLL ON THE HIGH SEAS
By EUGENE ARCHER. New York Times (1923-Current file)   20 Aug 1961: X5. 
| gross          = $5.5 million  (rentals)  
}} James Neilson comic book  and an adaptation appeared in the comic strip Walt Disneys Treasury of Classic Tales. It stars Fred MacMurray, Jane Wyman, Deborah Walley, Tommy Kirk and Kevin Corcoran as the Willard family on a European holiday. The family crossed the Atlantic Ocean on   which survives today, stripped and moored at Pier 82 in Philadelphia, Pennsylvania.

The character actor James Millhollin appears in the film as the ships librarian. 

==Production== The Desperate Hours and Bon Voyage was his second book; he and his wife wrote it after taking a trip across the Atlantic. Writing Demon, Deep in a Flood Story, Then Along Came Diane to Upset His Plans
Hansen, Harry. Chicago Daily Tribune (1923-1963)   02 Oct 1955: c9. 

Film rights were bought by Universal before the book had even been published for $125,000 and it was announced the film would be produced by Ross Hunter and written by the Hayes. Bon Voyage Announced as Major Buy; Holiday in Monaco Wald Film,
Schallert, Edwin. Los Angeles Times (1923-Current File)   09 Oct 1956: C11.  Esther Williams was originally announced as star. Looking at Hollywood: Esther Williams Gets Role of Mother of 5 in Next Film
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   18 Jan 1957: a7.  Then James Cagney was going to play the lead. CAGNEY TO HEAD BON VOYAGE CAST: Actor Signs for Third Film at Universal--Columbia Plans Wackiest Ship Ladd Firm Buys Story
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   15 July 1957: 15  Filming dates were pushed back then Bing Crosby was linked to the project. STRANGE VICORY BROUGHT BY LLESSER: Story by Rose Franken and Husband to Be Filmed -- Kovacs Signs Contract
By THOMAS M. PRYORSpecial to The New York Times.. New York Times (1923-Current file)   22 Oct 1958: 40. 

In early 1960, it was announced Disney had optioned the novel. Disney said it was likely Ken Annakin would direct with Karl Malden, James MacArthur and Janet Munro to star. BY WAY OF REPORT: Disney Plans Voyage -- Other Movie Items
By A.H. WEILER. New York Times (1923-Current file)   10 Jan 1960: X7.  Later Robert Stevenson was announced as director. VIEW FROM A LOCAL VANTAGE POINT: On the Harvey, Disney Production Schedule -- Freedom Subject
By A.H. WEILER. New York Times (1923-Current file)   05 Mar 1961: X7. 

"Its far out for us," said Disney, "but still Disney. Im really a gag man and missed the kind of pictures Frank Capra and Harold Lloyd used to make. Since nobody else wanted to do them, I decided to make them myself." Looking at Hollywood: Disney Out of Step? Not at Box Office
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   20 Apr 1962: b14. 

Eventually Fred MacMurray, Jane Wyman and Tommy Kirk firmed as the three leads. However casting the daughter proved more difficult. "You must build a picture," said Walt Disney. "You dont write it all - only part of it. And its the light and comic picture thats toughest of all to build." A Legendary Tale Spinner Looks Ahead--British TV Adjusts a Balance: Disney Boosting Live-Action Films
By John C. Waugh. The Christian Science Monitor (1908-Current file)   14 Mar 1961: 6. 
 West Side Story. Looking at Hollywood: Hedda Has High Praise for Guns of Navarone
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   19 June 1961: b2.  Deborah Walley was cast on the basis of her performance in Gidget Goes Hawaiian. Gidgets Deborah Signed by Disney: Oboler Adding Two to Five; World by Night Fascinating
Scheuer, Philip K. Los Angeles Times (1923-Current File)   10 Aug 1961: B13. 

Filming began on 15 August 1961. Looking at Hollywood: Disney Film of Sea Chase to Start Soon
HEDDA HOPPERS STAFF. Chicago Daily Tribune (1923-1963)   29 Apr 1961: 15.  It took place partly on location on a genuine ocean cruiser travelling across the Atlantic and in France. Walt Disney accompanied the film on location. 

Tommy Kirk did not get along with Jane Wyman:
 I thought Jane Wyman was a hard, cold woman and I got to hate her by the time I was through with Bon Voyage. Of course, she didnt like me either, so I guess it came natural. I think she had some suspicion that I was gay and all I can say is that, if she didnt like me for that, she doesnt like a lot of people.  
The title song was written by Disney staff songwriters, Robert B. Sherman and Richard M. Sherman.

==Reception==
Bon Voyage! was the tenth most popular film of 1962, grossing $11,000,000.

==Awards==
The film was nominated for two Academy Awards.    Costume Design Bill Thomas) Sound (Robert O. Cook)

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 