The Sheriff Was a Lady
 
{{Infobox film
| name = The Sheriff Was a Lady 
| director = Sobey Martin
| writer = Gustav Kampendok
| starring = Freddy Quinn Rik Battaglia Beba Loncar Carlo Croccolo Mamie Van Doren 
| runtime = 101 minutes 
| country = Germany 
| released = August 28, 1964 (West Germany)  German (original) English (US version) 
}} German musical western film, starring Freddy Quinn. Notable German co-stars, include, Rik Battaglia, Beba Loncar, and Carlo Croccolo. Playing a small role of Olivia is, Mamie Van Doren, a 1950s Hollywood sex goddess. Today the film is known as a "cult classic" by the few people who have been fortunate to see it.

==Cast==
*Freddy Quinn - Black Bill/John Burns/Freddy 
*Rik Battaglia - Steve Burns 
*Beba Loncar - Deputy Sheriff Anita 
*Carlo Croccolo - Sheriff Mickey Stanton 
*Tude Herr - Joana Stanton 
*Josef Albrecht - Ted
*Otto Waldis - Old Joe 
*Ulrich Hüls - Buck 
*Klaus Dahlen - Harry 
*Mariona - Saloon Singer 
*Bruno W. Pantel - Barman 
*Milivoge Popovic-Mavid - Perkins Henchman 
*Mamie Van Doren - Olivia

 
 
 
 
 
 
 
 


 
 