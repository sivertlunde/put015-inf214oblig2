Surf's Up (film)
{{Infobox film
| name           = Surfs Up 
| image          = Surfs upmp.jpg
| caption        = Theatrical release poster
| director       = Ash Brannon Chris Buck Chris Jenkins
| screenplay     = Don Rhymer Ash Brannon Chris Buck Chris Jenkins Chris Jenkins Christian Darren
| starring       = Shia LaBeouf Jeff Bridges Zooey Deschanel Jon Heder Mario Cantone James Woods Diedrich Bader
| music          = Mychael Danna
| editing        = Ivan Bilancio
| studio         = Sony Pictures Animation Sony Pictures Imageworks
| distributor    = Columbia Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $100 million 
| gross          = $149,044,513 
}} family comedy film directed by Ash Brannon and Chris Buck. It features the voices of Shia LaBeouf, Jeff Bridges, Zooey Deschanel, James Woods and Jon Heder among others.

In production since 2002 at Sony Pictures Animation, it was the studios second theatrical feature. The film premiered in the United States on June 8, 2007, and was distributed by Columbia Pictures.
 North Shore. Real-life surfers Kelly Slater and Rob Machado have vignettes as their penguin surfer counterparts. To obtain the desired hand-held documentary feel, the films animation team motion-captured a physical camera operators moves.

==Plot==
 
A documentary crew follows the events of Cody Maverick (Shia LaBeouf), a 17-year-old rockhopper penguin who has wanted to be a professional surfer ever since a visit from surf legend Zeke "Big Z" Topanga several years ago. When a talent scout shorebird named Mikey (Mario Cantone) arrives to find entrants for the "Big Z Memorial" surfing contest, Cody jumps at the chance despite lackluster support from his family. En route to the contest, Cody befriends another entrant, Chicken Joe (Jon Heder).

The entrants arrive at Pen Gu Island, the site of the contest, where Cody meets—and immediately falls in love with—Lani (Zooey Deschanel), an eighteen-year-old female gentoo penguin who is a lifeguard. He also meets Tank "The Shredder" Evans (Diedrich Bader), an egotistical penguin who has won the Big Z Memorial nine times since it was first held after Zs disappearance during a previous match 10 years ago.

Cody sees Tank disrespecting Big Zs memorial shrine and then Tank attacks Chicken Joe. Cody immediately challenges Tank to a surfing duel, which Tank easily wins while Cody nearly drowns. Lani rescues Cody and takes him to her uncle, the "Geek" (Jeff Bridges), to help Cody recover from his injuries. Cody wakes up and panics when he cant find the souvenir necklace he got as a kid from Big Z. Geek downplays the necklace, but decides to return it when he later finds it in his hut.
 koa log and offers to teach him to make a perfect surfboard. They attempt to take the log back to Geeks house, only to lose control of it and end up on a beach away from the contest. When Cody gets to the beach, they discover a shack full of old trophies and surfboards, which are actually Zs old belongings. He spots Geek watching all those things, and he realizes that Geek is actually Z, the same surfer he had idolized all those years, and asks Z to teach him to surf. Reluctantly, Z agrees, but says that Cody has to make his own board first.

The attempt doesnt go well, however, as an impatient Cody doesnt listen to Zs advice about using long, smooth strokes and makes a board that shatters as soon as he tries to enter the water. Frustrated, he storms off, running into Lani, who eventually persuades him to return. That night, a calmer, more patient Cody works on a new board, finishing it by morning, falling asleep.

Z compliments Cody on his board, but when Cody is eager to start training, Z instead has him do seemingly menial tasks unrelated to surfing. Cody loses his patience with Z only playing with him, but when Z is asleep, he places him on the board and pushes him into the water. Z asks him if he had fun, and when Cody tells "yes", Z takes him to the water. Z teaches Cody, telling him to surf the waves with long, smooth strokes, just like making the board. Lani comes in and joins them. Cody then asks Z if hell come watch the contest, but Z refuses, saying he faked his disappearance because he realized he couldnt compete with Tank, and that he was tired. Upset that Z just gave up, Cody leaves, meets up with Joe, and gets back to the contest just as it begins.

Tank easily makes the finals, as do Cody and Joe, and Joe turning out to be a natural born surfer. In the semi-finals, Tank battles with Cody, with Tank playing with him, but he falls off the board and loses. Cody and Joe make it to the finals. During the finals, however, Tank bursts in and tries to cause Joe to wipe out. Cody intervenes at the last minute, sending him and Tank out of bounds on the rocks. Tank wipes out and is rescued by Lani while Z, who had been secretly watching Codys performance, helps Cody get back to the beach.

Z and Cody find out that Joe won by default since Tank and Cody were disqualified. Cody no longer cares about winning, however, having decided hed rather just have fun instead. Z reveals himself to the audience and invites all the spectators to surf. Cody finishes his interview and joins the rest of his friends (including Z and Joe) in the water.

==Cast==
*Shia LaBeouf as Cody Maverick - A seventeen-year-old northern rockhopper penguin who is determined to win the tenth annual Big Z Memorial Surf Off.
*Jeff Bridges as Ezekiel Big Z Topanga  a.k.a. "Geek" - Codys idol, a professional surfer who is presumed dead.
*Zooey Deschanel as Lani Aliikai - A gentoo penguin  lifeguard and Big Zs niece. She quickly becomes Codys love interest.
*Jon Heder as Chicken Joe - A rooster from Sheboygan, Wisconsin who also participates in the Big Z Memorial Surf Off and befriends Cody. 
*Mario Cantone as Mikey Abromowitz - A talent scout shorebird who is sent out to find wannabe surfers. Don King.
*Diedrich Bader as Tank "The Shredder" Evans, a surfer who has won the Big Z Memorial Surf Off nine times.
*Dana Belben as Edna Maverick - Codys mother.
*Brian Posehn as Glen Maverick - Codys older brother.
*Kelly Slater as Himself, professional surfer and sportscaster.
*Rob Machado as Himself, professional surfer and sportscaster.
*Ash Brannon as Himself, the filmmaker of the movie.
*Chris Buck as Himself, the other filmmaker of the movie.
*Sal Masekela as SPEN Announcer
*Reed Buck as Arnold, a young gentoo penguin who wants to become a surfer.

==Music==

===Soundtrack===
{{Infobox album  
| Name        = Surfs Up: Music from the Motion Picture
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = Surfs Up Soundtrack.jpg
| Released    = June 5, 2007
| Recorded    =
| Genre       = Film soundtrack
| Length      = 50:17
| Label       = Columbia Records
| Producer    =
| Reviews     =
}}

Surfs Up: Music from the Motion Picture was released on June 5, 2007. The following 14 songs are on the Sonys official film soundtrack. 

{{Track listing
| writing_credits = yes
| total_length    = 50:17
| title1          = Reggae Got Soul 311
| length1         = 3:09 Drive
| Incubus
| length2         = 3:52
| title3          = Stand Tall
| writer3         = The Dirty Heads
| length3         = 3:11
| title4          = Lose Myself
| writer4         = Lauryn Hill
| length4         = 4:35
| title5          = Just Say Yes
| writer5         = Ken Andrews
| length5         = 3:40
| title6          = Forrowest
| writer6         = Forro in the Dark
| length6         = 4:44
| title7          = Pocket Full of Stars
| writer7         = Nine Black Alps
| length7         = 3:32
| title8          = Into Yesterday
| writer8         = Sugar Ray
| length8         = 4:11 Big Wave
| writer9         = Pearl Jam
| length9         = 2:57 Wipe Out
| writer10        = The Queers
| length10        = 1:41
| title11         = Run Home (Instrumental) Priestess
| length11        = 3:37 What I Like About You
| writer12        = The Romantics
| length12        = 2:57
| title13         = You Get What You Give
| writer13        = New Radicals
| length13        = 4:57 Hawaiian War Chant (Ta-Hu-Wa-Hu-Wai)
| writer14        = Bob Wills & His Texas Playboys
| length14        = 3:14
}}

According to the films end credits, the version of Wipe Out (song)|"Wipe Out" heard in the film is actually performed by the punk band The Queers. The official soundtrack includes this version under the pseudonym "Big Nose", presumably for marketing purposes. {{cite video
| title=Surf’s Up
| medium=DVD
| publisher=Sony Pictures Animation/Columbia Pictures
| date=2007}}  {{cite web
| title=Surfs Up (2007)
| publisher=The Soundtrack Info Project
| url=http://www.soundtrackinfo.com/title/surfsup.asp
| accessdate=2010-12-30
}}  Two songs by Green Day, "Welcome to Paradise" and "Holiday (Green Day song)|Holiday" in an instrumental version, are used for background music in the film. {{cite web
| title=Surfs Up (2007)
| publisher=The Soundtrack Info Project
| url=http://www.soundtrackinfo.com/title/surfsup.asp
| accessdate=2009-12-30
}}  However, neither song appears on the official soundtrack album. In the primary teaser trailer, the song "Get on Top" by the Red Hot Chili Peppers can be heard in the background. "Welcome to Paradise" was also used in the second trailer promoting Surfs Up, as well as "Three Little Birds" by Sean Paul featuring Ziggy Marley.

The DVD and Blu-ray behind-the-scenes featurette entitled "Making Waves" features the song "The Water", performed by Venice (band)|Venice. This also does not appear on the "Surfs Up" soundtrack, but can be found on Venices "Garage Demos Part 2: Fast Stuff" CD.

===Score===
{{Infobox album  
| Name        = Surfs Up: Original Ocean Picture Score
| Type        = Film score
| Longtype    = 
| Artist      = Mychael Danna
| Cover       = Surfs Up Score.jpg
| Released    = January 29, 2008
| Recorded    =
| Genre       = Film score
| Length      = 28:15
| Label       = Sony
| Producer    =
| Reviews     =
}}

Surfs Up: Original Ocean Picture Score was composed for the film by Mychael Danna and it was released on a limited edition (of a thousand units) 23 track CD. 

{{Track listing
| all_writing     = Mychael Danna
| total_length    = 28:15
| title1          = Legends
| length1         = 2:28
| title2          = Sports Network Presents
| length2         = 1:05
| title3          = Youre In
| length3         = 2:33
| title4          = Big Zs Shrine
| length4         = 0:43
| title5          = Taking on Tank
| length5         = 1:20
| title6          = The Geek
| length6         = 1:00
| title7          = Stuck With This Guy
| length7         = 0:34
| title8          = I Dont Have a Way
| length8         = 0:58
| title9          = Log Roll
| length9         = 0:44
| title10         = The Board Shack
| length10        = 1:52
| title11         = Cody Struggles
| length11        = 0:59
| title12         = Lani and Cody
| length12        = 1:11
| title13         = Waterfall
| length13        = 1:04
| title14         = In the Bogus
| length14        = 0:54
| title15         = Training
| length15        = 1:32
| title16         = The Has Been
| length16        = 0:51
| title17         = The Big Z Memorial
| length17        = 0:45
| title18         = First Round
| length18        = 1:02
| title19         = Shredding
| length19        = 0:55
| title20         = Winning Is...
| length20        = 1:05
| title21         = Boneyards
| length21        = 2:34
| title22         = Losers
| length22        = 0:46
| title23         = Pointed the Way Back
| length23        = 1:20
}}

==Release==

===Marketing===
*To promote the films release, surfboard wax bearing the films logo and a rendering of Cody was given out at the 2006 San Diego Comic-Con
 plush characters of Cody, Lani, Chicken Joe, and Geek replace some of the toys in the US lineup.
 lenticular motion surf board shaped pieces.

*Build A Bear stores sold plush toys of the films protagonist, Cody Maverick. Patrons could "build" their own Cody as well as dress him up in an array of surf-themed clothing and other accessories in Build A Bear workshops. They could also purchase Cody and accessories at the official site.

*Several plushes from Nanco intended for claw machines can also be found and purchased in varying sizes (from 6&nbsp;inches to 11&nbsp;inches to 17&nbsp;inches). Characters available: Cody, Lani, Tank, Geek, Big Z, Chicken Joe, and Reggie Belefonte. A rare set of larger (Geek measures 15&nbsp;inches) and higher quality plushes had been made available at the premieres after party. Won either through available games, or given away with gift sets for attendees. Cody, Lani, Arnold and Geek were created and so far no plans for the dolls to be sold commercially have appeared.

*A series of childrens 100 piece puzzles are available as well depicting various characters and setups.

*Five inch waterproof plush tub toys released by Jakks Pacific in minimal quantities to most major retailers. Five characters were produced, including Cody, Tank, Big Z, Arnold, & Chicken Joe. Each character has an attached washcloth/surfboard, but sport questionable likenesses (and in the case of Chicken Joe, entirely off model, bearing more of a resemblance to Mikey).

*Along with the release of the film, a companion Surfs Up (video game)|Surfs Up video game was released for all the current video gaming systems on the market. All versions of the game are the same with mild graphical differences, with only the Nintendo DS version changing the overall format.

===Home media===
Surfs Up was released on high-definition   and The ChubbChubbs Save Xmas, which premiered with this release.   

==Reception==

===Critical response===
Surfs Up has received generally positive reviews from critics, and the film is Certified Fresh on  , gave the film a score of 64 out of 100, based on 26 reviews. 

Some reviews noted that in spite of it coming so soon after many films featuring penguins (  critic Nancy Churnin agreed: "Sorry, cynics, Surfs Up is a charmer. And if the birds look somewhat familiar, they have something fresh to say about friendship and what winning is all about."  While   of New York Post had a different opinion: "Maybe the next penguin flick will do more justice to the subject." 
 Endless Summer surf documentaries."  On the other hand, Bill Muller, The Arizona Republics critic, disliked almost everything: "From the nondescript voices to routine animation to an over-written story, this movie spends much of its time gasping for air." 

===Box office===
The film opened at number four and grossed $5,804,772 at the box office on its opening day in North America. It grossed $17,640,249 in its opening weekend. The film eventually was a failure at the box office as it grossed $58,867,694 in North America, and $90,176,819 in the other territories, making the total worldwide gross $149,044,513.   

===Accolades===
Surfs Up was nominated for the Academy Award for Best Animated Feature at the 80th Academy Awards, but lost to Ratatouille (film)|Ratatouille. 

The films was also nominated for 10 Annie Awards at the 35th Annie Awards, and won two awards: Animated Effects, and Animation Production Artist:  
* Best Animated Feature
* Animated Effects: Deborah Carlson (won)
* Animation Production Artist: John Clark (won)
* Character Animation in a Feature Production: Dave Hardin
* Character Animation in a Feature Production: Alan Hawkins
* Character Design in an Animated Feature Production: Sylvain Deboissy
* Directing in an Animated Feature Production: Ash Brannon & Chris Buck
* Production Design in an Animated Feature Production: Marcelo Vignali
* Storyboarding in an Animated Feature Production: Denise Koyama
* Writing in an Animated Feature Production: Don Rhymer and Ash Brannon & Chris Buck

==Cancelled sequel==
Since the release of the original, Mario Cantone, Jon Heder and Shia LaBeouf have mentioned the possibility of a sequel several times. No release dates, cast, filming crew or details about a possible script have been brought up since, due to the box office disappointment of the first film.  

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*   - articles about the film production, mostly technical

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 