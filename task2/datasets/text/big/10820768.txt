Suspicious River
{{Infobox film
| name           = Suspicious River
| image          = Suspiciousriver_poster.jpg
| image size     =
| caption        = Suspicious River DVD
| director       = Lynne Stopkewich
| producer       = Hamish McAlpine Erik Stensrud
| writer         = Laura Kasischke Lynne Stopkewich
| narrator       =
| starring       = Molly Parker Callum Keith Rennie Mary Kate Welsh Joel Bissonnette Deanna Milligan Sarah-Jane Redmond Byron Lucas Don S. Davis
| music          = Don MacDonald
| cinematography = Gregory Middleton
| editing        = Allan Lee
| distributor    =
| released       =  )
| runtime        = 92 min
| country        = Canada English
| budget         =
| website        =
| amg_id         =
}}
 Canadian dramatic film, released in 2000. The film was directed by Lynne Stopkewich, based on a novel by Laura Kasischke.

==Cast==
* Leila Murray- Molly Parker
* Gary Jensen- Callum Keith Rennie
* Rick Schmidt- Joel Bissonnette
* Uncle Andy- Byron Lucas
* Jack (Leilas Father)- Norman Armour
* Bonnie (Leilas Mother)- Sarah-Jane Redmond
* Millie- Deanna Milligan

==Plot summary==
Leila Murray works as a receptionist in a motel. She begins to offer sexual services to customers in exchange for money. As she explores her sexuality, she discovers within herself an attraction towards semi-rough sex. One of her customers, Gary Jensen, beats her during their first encounter. When he returns to apologize, she accepts and begins to see him again. When one of Leilas other customers gets too rough, Leila flees to Garys room to seek his consolation and protection. His gentle treatment of her encourages her growing trust in him.

Leilas only confidant, outside of Gary, is a little girl that plays around the inn. The girl lives in an unhappy home, where her parents fight constantly. One day, the girls Uncle Andy takes the child and her mother Bonnie on a roadtrip. In the midst of this trip, the little girl discovers that her mother and her Uncle Andy are having an affair.

Meanwhile, Leila has grown very close to Gary, who begins to represent an escape from her passionless marriage and dead-end job. She stops charging him for sex. Despite momentary misgivings about his intentions, his passion convinces her to turn away from her life. She steals the money from the inn, and gives it to Gary, along with her savings acquired through her sexual services of the inns clients. Together, they drive away in his car to his cabin in the woods. Once there, she is informed by one of Garys friends that he is a pimp, and that his intention is to sell her services to his friends. Trapped, with no money or means of transportation, she is raped and beaten by a group of men invited by Gary. In the midst of her trauma, her mind turns back to the day in her childhood when her Uncle Andy had taken Leilas mother Bonnie out on a roadtrip and killed Bonnie in a fit of jealousy, right in front of the child Leila. It is thus revealed that Leila is the little girl to whom she had been talking throughout the film. The little girls broken home life holds the key to Leilas grown-up sexuality.

After Leila is raped, one of the men takes pity on her and helps her to escape Garys cabin. Gary and the men form a search party and comb the woods to find her. The little girl, Leilas child self, appears to Leila to inform her that Gary intends to kill her and that she must fight for her survival. When Gary finds Leila, he is distracted by the distant screaming of the little girl that he cant see, and Leila takes the opportunity to run off and flee to safety in the arms of her friend and co-worker Millie, who had been searching the woods for her, as well. Thus Leila turns her back on Gary and acknowledges the previously-suppressed memories of her past.

==Awards and nominations==
Leo Award Nominations:
*Best Director of Feature Length Drama (Lynne Stopkewich)
*Best Musical Score of Feature Length Drama (Don MacDonald)
*Best Overall Sound of Best Feature Length Drama
*Best Performance of Feature Length Drama - Female (Molly Parker)
*Best Picture Editing of Feature Length Drama (Allan Lee)
*Best Screenwriter of Feature Length Drama (Lynne Stopkewich)
Leo Award Wins:
*Best Cinematography of Feature Length Drama (Gregory Middleton)
*Best Feature Length Drama
*Best Performance of Feature Length Drama - Male (Callum Keith Rennie)

==External links==
*  
*  

 

 

 
 
 
 