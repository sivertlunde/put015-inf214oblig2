Thirudan
{{Infobox film
| name           = Thirudan
| image          =
| image_size     =
| caption        =
| director       = A. C. Tirulokchandar
| producer       = K. Balaji
| writer         =
| screenplay     =
| starring       = Sivaji Ganesan K. R. Vijaya K. Balaji Vijayalalitha
| music          = M. S. Viswanathan
| cinematography =
| editing        =
| studio         = Sujatha Cine Arts
| distributor    = Sujatha Cine Arts
| released       =  
| country        = India Tamil
}}
 1969 Cinema Indian Tamil Tamil film,  directed by A. C. Tirulokchandar. The film stars Sivaji Ganesan, K. R. Vijaya, K. Balaji and Vijayalalitha in lead roles. The film had musical score by M. S. Viswanathan.    The film was a remake of Telugu film. The film was remade in Hindi as Himmat in 1970.

==Cast==
*Sivaji Ganesan
*K. R. Vijaya
*K. Balaji
*Vijayalalitha
*Nagesh
*Major Sundarrajan

==Soundtrack==
The music was composed by M. S. Viswanathan.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Susheela || Kannadasan || 03.59 
|- 
| 2 || Kotai Mathilmele || T. M. Soundararajan, L. R. Eswari || Kannadasan || 03.18 
|- 
| 3 || Ninaiththapadi Kidaiththadadi || L. R. Eswari || Kannadasan || 02.57 
|- 
| 4 || Pazhaniyappan Pazhani || T. M. Soundararajan || Kannadasan || 03.42 
|}

==References==
 

==External links==
*  

 

 

 
 
 
 
 
 


 