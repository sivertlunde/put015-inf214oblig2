The Secret of the Grain
{{Infobox film
| name = The Secret of the Grain
| image = La_Graine_et_le_Mulet.jpg
| caption = Theatrical release poster
| director = Abdellatif Kechiche
| producer = Claude Berri
| writer = Abdellatif Kechiche Ghalia Lacroix
| starring = Habib Boufares Hafsia Herzi  Farida Benkhetache  Abdelhamid Aktouche  Bouraouïa Marzouk  Sabrina Ouazani  Olivier Loustau
| cinematography = Lubomir Bakchev
| editing = Camille Toubkis  Ghalia Lacroix
| distributor = Pathé Distribution
| released =  
| runtime = 151 minutes
| country = France  Tunisia
| language = French
| budget = 
| gross =
}}
The Secret of the Grain ( , also released internationally as Couscous) is a 2007 France|Franco-Tunisian drama film directed by Abdellatif Kechiche. The film stars Habib Boufares as an aging immigrant from the Maghreb whose ambition to establish a successful restaurant as an inheritance for his large and disparate family meets sceptical opposition from the French bureaucracy.

The French title of the film refers to a "grain of couscous" and to Flathead mullet|mullet, a type of small fish, both popular in Tunisian cuisine. The two ingredients constitute both the staple of his extended familys diet and the menu on which he plans to establish his restaurant.

==Plot==
Slimane Beiji (Habib Boufares) is the divorced head of a Franco-Arabic family.  As he is being forced out of his job at the local shipyard, he interacts in a series of extended vignettes with various members of his extended family including his ex-wife, his sons and daughters, their husbands and wives, and his grandchildren. Determined to leave a legacy for his beloved family, and encouraged by his long-term partners daughter, Rym, (Hafsia Herzi) he pursues his dream of converting a dilapidated boat into a family restaurant that will specialise in his ex-wifes fish couscous, a meal that she prepares for the entire family every Sunday.

With Ryms enthusiastic help, Slimane applies for the relevant licences and loans, but soon finds himself knee-deep in bureaucratic red tape. Undaunted, Slimane enlists his sons to help with the renovation and arranges an opening gala of a large dinner party, to which he invites the many bureaucrats on whose decision the fate of the project rests, in an attempt to demonstrate the viability of the enterprise. On the night in question attendance is high, although notable by her absence is his long-term partner, Latifa. Insulted and threatened by the involvement of Slimanes ex-wife in the project, the restaurants cook and creator of the pivotal fish couscous, Latifa refuses to leave her hotel. Rym pleads with her to attend, encouraging her to use the opportunity to flaunt her comparative youth and beauty in front of Slimanes ex-wife, and eventually she concedes.

Having prepared the couscous, fish, vegetables and sauce in large metal cauldrons, Slimanes ex-wife sends the food to the boat with her sons and leaves her apartment to find a poor man so that she can donate a plate of food that she habitually reserves for the less fortunate. At the restaurant, the sons unload the metal cauldrons and the women serve wine and appetisers to the waiting guests. The guests, talking amongst themselves, begin to speak both positively and anxiously about the likely success of the restaurant, now concerned that it may draw custom away from the other restaurants in the area. Slimanes son Majid, whilst surveying the crowd, notices a bureaucrats wife with whom he has had several illicit sexual liaisons and decides to leave quietly. He instructs his brother to tell the rest of the family that he has gone to help a friend who has broken down on the highway. The women start to heat up the food when they notice the absence of the couscous. Panic mounts when they discover that it is definitively absent and Majid, who has taken the car that still holds the metal cauldron in the trunk, is not answering his phone. Knowing that it will take at least another hour to cook a new batch, they continue to frantically call Majid and attempt to call Souad, who is out searching for an unfortunate in need of plate of couscous. Slimane takes his motorbike out to Souads apartment building to find her.

In the dining room the guests, despite the attempts of the women to placate them with assurances and date liqueur, become extremely restless. Their comments turn nasty and they turn on the waitressing girls. When he cannot find his ex-wife, Slimane leaves the apartment building to find his motorbike stolen. The culprits, three young boys, sit atop the stolen bike on the other side of the river, mocking him. He runs after them, but every time they stop to mock him they move on before he can catch up to them.

Tensions in the dining room reach a peak and Rym decides to step in. She whispers to the musicians, and suddenly the music stops and the lights go out. When they come back on, Rym stands before the assembled diners in a red belly-dancing outfit. The musicians play for her and she entrances the guests with a fervent performance, charged with youthful, sexual energy. Latifa uses the opportunity to slip off the boat and return to the hotel to start a new pot of couscous.

As Rym dances and sweats, her mother walks up the gangplank with a new cauldron of couscous, whilst Slimane continues to chase the youths around the apartment building. Finally he collapses to his knees, slumped on the asphalt.

==Cast==
* Habib Boufares as Slimane Beiji, a sixty-year-old shipyard worker and immigrant from the Maghreb. Divorced from his first wife, Souad, he lives in the run-down Hotel de lOrient owned by Latifa and her daughter Rym.
* Hafsia Herzi as Rym, the precocious twenty-year-old daughter of hotelier Latifa and a second-generation immigrant. She regards Slimane as her father and works to assist him in realising his ambition of starting a couscous restaurant.
* Hatika Karaoui as Latifa, proprietor of the Hotel de lOrient and Slimanes lover.
* Bouraouïa Marzouk as Souad, Slimanes first wife of roughly equal age and matriarch of their family. She lives in an apartment building on the floor below her son Hamid and regularly cooks a large Sunday meal of couscous for her extended family.
* Farida Benkhetache as Karima, Slimanes daughter from his first marriage to Souad. She works in a tuna cannery and has two young children of her own by her husband.
* Abdelhamid Aktouche as Hamid
* Alice Houri as Julia, a young Russian immigrant unhappily married to Majid. She feels isolated from and unsupported by his large family, left alone all day and often all night to care for their young son with only her brother for comfort and assistance. Nonetheless she attempts to participate in the endeavour to establish the couscous restaurant.
* Cyril Favre as Sergei, Julias supportive and diplomatic brother who gently attempts to reconcile Slimane and his family to his sisters suffering and Majids neglect.
* Sami Zitouni as Majid, Slimane and Souads eldest son. Married to Russian immigrant Julia with an infant son, he is a notorious philanderer; whilst he neglects his own young family, he still participates fully in the life of his extended family.
* Sabrina Ouazani as Olfa, another of Slimanes daughters by Souad and the youngest sibling, charged with caring for the younger children in the extended family.
* Mohamed Benabdeslem as Riadh, Slimanes youngest son by Souad. An adolescent with a nascent moustache, he assists in renovating the restaurant-boat under his fathers supervision and maintains a romantic infatuation with Rym.
* Henri Cohen as M. Dorner, the Deputy Mayor of the port town responsible for authorising Slimanes docking permissions for his restaurant-boat. A powerful man in the town, he is slated as a potential candidate for Mayor.
* Violaine de Carné as Mme. Dorner, the wife of the Deputy Mayor and casual affair of Slimanes son, Majid.

==Critical reception==
The film appeared on several critics top ten lists of the best films of 2008. A. O. Scott of The New York Times named it the 3rd best film of 2008,  Andrew OHehir of Salon.com|Salon named it the 6th best film of 2008,  and Scott Foundas of LA Weekly named it the 7th best film of 2008 (along with A Christmas Tale).     

==Awards and nominations==
César Award, 2008:
* Best French Film
* Best Director: Abdellatif Kechiche
* Best Original Screenplay: Abdellatif Kechiche
* Most Promising Actress: Hafsia Herzi

Antalya Golden Orange Film Festival, 2007
* Best Director (Eurasia Film Festival): Abdellatif Kechiche

Venice Film Festival, 2007:
* Special Jury Prize (ex-æquo / tie, with Im Not There)
* Marcello Mastroianni Prize (for actor or actress in a début role): Hafsia Herzi
* SIGNIS Award - Honorable Mention: Abdellatif Kechiche
* Nominated: Golden Lion

Louis Delluc Prize, 2007

==Notes==
 

==External links==
*    
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 