Monster (2003 film)
 
{{Infobox film
| name = Monster
| image = Monster movie.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Patty Jenkins
| producer = Charlize Theron Mark Damon Clark Peterson Donald Kushner Brad Wyman
| writer = Patty Jenkins
| starring = Charlize Theron Christina Ricci Bruce Dern Lee Tergesen  BT
| Steven Bernstein
| editing = Arthur Coburn Jane Kurson
| studio = DEJ Productions Media 8 Entertainment
| distributor = Newmarket Films
| released =   
| runtime = 109 minutes
| country = United States Germany
| language = English
| budget = $8 million
| gross = $60,378,584
}} crime drama prostitute who executed in Florida in 2002 for killing six men (she was not tried for a seventh murder) in the late 1980s and early 1990s. Wuornos was played by Charlize Theron, and her fictionalized lover, Selby Wall (based on Wuornos real-life companion Tyria Moore), was played by Christina Ricci. Patty Jenkins wrote and directed the film.
 Golden Globe Outstanding Lead Actress.

==Plot==
After moving from Michigan to Florida, Aileen Wuornos, a Prostitution|prostitute, meets Selby Wall in a gay bar. After initial hostility and declaring that she is not gay, Aileen talks to Selby over beers. Selby takes to Aileen almost immediately, as she likes that she is very protective of her. Selby invites her to spend the night with her. They return to the house where Selby is staying (temporarily exiled by her parents following the accusation from another girl that Selby tried to kiss her). They later agree to meet at a roller skating rink, and they kiss for the first time. Aileen and Selby fall in love, but they have nowhere to go, so Selby goes back to her Aunts home. 

After being raped and brutalized by a client, Vincent Corey, Aileen kills him in self-defense and decides to quit prostitution. She confesses her action to Selby, while Selby has been angry with her for not supporting the two of them. Aileen tries to find legitimate work; but because of her lack of qualifications and criminal history, prospective employers reject her and are occasionally openly hostile. Desperate for money, she returns to prostitution. She robs and kills her johns, each killed in a more brutal way than the last, as she is convinced that they are all trying to rape her. She spares one man out of pity when he admits he has never had sex with a prostitute but eventually kills another man who, instead of exploiting her, offers help. Aileen uses the money she steals from her victims to indulge herself and Shelby

 sentenced to death. On October 9, 2002, Aileen is executed by lethal injection.

==Cast==
* Charlize Theron as Aileen Wuornos
* Christina Ricci as Selby Wall (based on Tyria Moore)
* Bruce Dern as Thomas
* Lee Tergesen as Vincent Corey (based on Richard Mallory)
* Annie Corley as Donna
* Pruitt Taylor Vince as Gene / Stuttering "John"
* Marco St. John as Evan / Undercover "John"
* Marc Macaulay as Will / Daddy "John" Scott Wilson as Horton / Last "John"
* Kane Hodder as Undercover cop
* Brett Rice as Charles

==Reception== mentally ill  woman – Wuornos had antisocial personality disorder and borderline personality disorder.  For the role, Theron gained   and wore prosthetic teeth.  Critics called her performance, and her makeup, a "transformation".    Film critic Roger Ebert named it best film of the year, and wrote "What Charlize Theron achieves in Patty Jenkins Monster isnt a performance but an embodiment...   is one of the greatest performances in the history of the cinema."   
 SAG Award for her performance.

In 2009, Roger Ebert named it the third best film of the decade.  Review website Rotten Tomatoes reports that 82% of critics gave the film a positive review, with a "Certified Fresh" and an average score of 7.2/10.

==Soundtrack==
{{Infobox album
| Name        = Monster
| Type        = Soundtrack BT
| Cover       = Monster OST.jpg
| Released    = January 30, 2004
| Recorded    = 
| Genre       = 
| Length      = 
| Label       = dts Entertainment
| Producer    = 
}} BT released a soundtrack to the film.  Included with the release is a DVD featuring all fifteen original cues, and an additional nine cues that would not fit on the CD, as well as an interview with BT and Patty Jenkins, and remix files for "Ferris Wheel".

All songs written by BT.
# "Childhood Montage"
# "Girls Kiss"
# "The Bus Stop"
# "Turning Tricks"
# "First Kill"
# "Job Hunt"
# "Bad Cop"
# "Call Me Daddy Killing"
# "I Dont Like It Rough"
# "Ferris Wheel (Love Theme)"
# "Ditch the Car"
# "Madman Speech"
# "Cop Killing"
# "News on TV"
# "Courtroom"

==See also==
* List of lesbian, gay, bisexual, or transgender-related films by storyline

==References==
 

==External links==
 

*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 