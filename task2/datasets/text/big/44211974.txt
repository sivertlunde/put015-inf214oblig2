Melodies of America
{{Infobox film
| name = Melodies of America
| image = 
| image_size =
| caption =
| director = Eduardo Morera
| producer = 
| writer =  Francisco Chiarello   Ariel Cortazzo   Conrado de Koller
| narrator =
| starring = José Mojica   Silvana Roth   June Marlowe   José Ramírez (actor)|José Ramírez
| music = Agustín Lara   Rodolfo Sciammarella
| cinematography = Mario Pagés
| editing = 
| studio = San Miguel Films  
| distributor = 
| released = 1941
| runtime = 94 minutes
| country = Argentina Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Melodies of America (Spanish:Melodías de América) is a 1941 Argentinian musical comedy film directed by Eduardo Morera and starring José Mojica, Silvana Roth and June Marlowe.   It was intended as an Argentine response to the Latin-American themed films produced by Hollywood as part of the Good Neighbor policy.

==Synopsis== panamerican theme. They eventually manage to raise the finances for the film and hires a famous Mexican singer to star in it. On the boat down he meets and falls in love with a poor woman from Buenos Aires. Meanwhile an American actress who has failed in Hollywood attempts to get a role in the production.

==Cast==
* José Mojica
* Silvana Roth
* June Marlowe
* José Ramirez (actor)|José Ramirez
* Armando Bó
* Carmen Brown
* Pedro Quartucci
* María Santos
* Bola de Nieve 
* Ana María González
* Nelly Omar
* Juan Carlos Altavista
* Rafael Carret

== References ==
 

== Bibliography ==
* Melgosa, Adrián Pérez. Cinema and Inter-American Relations: Tracking Transnational Affect. Routledge, 2012. 
  
== External links ==
* 

 
 
 
 
 
 
 
 
 
 


 