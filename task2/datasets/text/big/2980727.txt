They Made Me a Killer
{{Infobox film
| name = They Made Me a Killer
| image = They Made Me a Killer poster.jpg
| caption = Theatrical release poster
| director = William C. Thomas
| producer = William H. Pine William C. Thomas
| writer = Owen Franes (story) Daniel Mainwaring (as Geoffrey Homes) Winston Miller Kae Salkow Robert Lowery Barbara Britton Alexander Laszlo
| cinematography = Fred Jackman Jr.
| editing = Henry Adams
| studio = Pine-Thomas Productions| distributor = Paramount Pictures
| released =  
| runtime = 64 min
| country = United States
| language = English
| budget =
}}
They Made Me a Killer is a 1946 Film noir directed by William C. Thomas, and written by Daniel Mainwaring, Winston Miller and Kae Salkow, based on story by Owen Franes.  It stars Barbara Britton and Robert Lowery. The film was made by Pine-Thomas Productions|Pine-Thomas, the B-movie unit of Paramount Pictures.

== Plot ==

Tom Durling quits his job and drives across country after his brother is killed in an accident. He gives an attractive girl a ride and hes forced at gun point to be the driver in a bank robbery. During the crime, Steve Reynolds, another innocent man is involved and killed in the escape. After a high-speed chase, the car crashes and Durling is knocked unconscious while the bandits get away. The police arrest Durling but refuse to believe that he wasnt one of the robbers.

Durling escapes the police then later teams with Reynolds sister in an attempt to prove his innocence.  The trail leads to a small roadside diner where the two end up finding the gang hiding out in the buildings basement.  They go undercover, her as a waitress and Durling joins the gang.  In the end, they trick the criminals into confessing their crimes. Durlings reputation is saved, and the criminals, led by a Ma Barker-type mom, get shot up.

== Cast ==
 Robert Lowery as Tom Durling
*Barbara Britton as June Reynolds Lola Lane as Betty Ford
*Frank Albertson as Al Wilson, Glen Grove patrolman
*Elisabeth Risdon as Ma Conley
*Byron Barr as Steve Reynolds Paul Harvey as District Attorney Booth

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 


 