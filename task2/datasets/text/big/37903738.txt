Hwayi: A Monster Boy
{{Infobox film
| name           = Hwayi: A Monster Boy
| image          = File:Hwa-yi_poster.jpg
| director       = Jang Joon-hwan
| producer       = Lee Jun-dong   Lee Dong-ha   Lee Dae-hee
| writer         = Park Ju-seok
| starring       = Yeo Jin-goo   Kim Yoon-seok
| music          = Mowg
| cinematography = Kim Ji-young
| editing        = Kim Sang-beom   Kim Jae-beom
| distributor    = Showbox  
| released       =  
| runtime        = 126 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}}
Hwayi: A Monster Boy ( ) is a 2013 South Korean film about a 16-year-old boy of the same name (played by Yeo Jin-goo ) who is raised by five criminal fathers to become the perfect assassin. It takes pulling the trigger to discover his true identity after he realizes the mystery surrounding his past and his fate.   It was the highly anticipated second feature film by director Jang Joon-hwan, a decade after his 2003 cult favorite sci-fi comedy/thriller Save the Green Planet!.    

==Plot==
In the middle of a forest, a notorious five-member crime organization lives together on a deserted farm. One day, following a plan
that didnt go as expected, an infant whom they had kidnapped in a blackmailing scheme is left with them. 14 years later,
this boy, who is now named Hwa-yi, lives as the son of his five criminal fathers: Seok-tae, the cold but charismatic leader, Ki-tae, a stammering driving expert, Jin-seong, an ideal planner, Beom-soo, a guns expert, and Dong-beom a cold martial artist. Hwa-yi has been brought up in a unique way, learning skills from his five fathers instead of going to school. Though he has adapted to this life, sometimes he longs for the ordinary life of other boys, which seems impossible for him.

The gang plans another crime, which will see Hwa-yi participate for the first time. During a fight, Hwa-yi fires a gun to kill
someone. At first, his fingers tremble and the gun shakes but after one shot, he keeps pulling the trigger as though
possessed. He finds a picture of a child on the man he shot and soon Hwa-yi starts to uncover a huge secret and his
tragic destiny. His life is turned upside down when he learns that the man he killed was his real father. From that moment on, Hwa-yi vows revenge on his gangster fathers, using the devious skills he picked up from a life in crime. 

==Cast==
*Yeo Jin-goo - Hwa-yi
*Kim Yoon-seok - Seok-tae
*Cho Jin-woong - Ki-tae
*Jang Hyun-sung - Jin-seong
*Kim Sung-kyun - Dong-beom
*Park Hae-joon - Beom-soo
*Park Yong-woo - Detective Chang-ho
*Lee Geung-young - Im Hyung-taek
*Yoo Yeon-seok - Park Ji-won
*Moon Sung-keun - CEO Jin Nam Ji-hyun - Yoo-kyung
*Seo Young-hwa - Im Hyung-taeks wife
*Im Ji-eun - Young-joo
*Kim Young-min - Detective Jung-min
*Woo Jung-gook - blind masseur
*Lee Joon-hyuk - police officer who sees Hwa-yi entering the house
*Han Sung-yong - police officer checking for drunk driving

==Box office==
Released on October 9, 2013, known as Hangul Day, a national holiday in South Korea, Hwayi: A Monster Boy opened strong at the box office, reaching 1.2 million admissions over its first five days.   At the end of its run, it had sold a total of 2,394,418 tickets, with a gross of  .

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
|- 2013
| style="text-align:center;"| 21st Korean Culture and Entertainment Awards
| Best New Actor (Film)
| Yeo Jin-goo
|  
|-
| style="text-align:center;"| 33rd Korean Association of Film Critics Awards
| Best New Actor 
| Yeo Jin-goo
|  
|- 34th Blue Dragon Film Awards
| Best Supporting Actor
| Cho Jin-woong
|  
|-
| Best New Actor 
| Yeo Jin-goo
|  
|-
| Best New Actress Nam Ji-hyun
|  
|-
| Best Screenplay
| Park Ju-seok
|  
|-
| Best Music
| Mowg
|  
|- 2014
| style="text-align:center;"| 5th KOFRA   Film Awards
| Best New Actor 
| Yeo Jin-goo
|  
|- 50th Baeksang Arts Awards
| Best New Actor 
| Yeo Jin-goo
|  
|-
| style="text-align:center;"| 14th Directors Cut Awards
| Best New Actor 
| Yeo Jin-goo
|  
|-
| style="text-align:center;"| 23rd Buil Film Awards
| Best New Actor 
| Yeo Jin-goo
|  
|-
| style="text-align:center;"| 51st Grand Bell Awards
| Best New Actor 
| Yeo Jin-goo
|  
|}

==Remake==
Lee Jun-dong, head of Hwayis production company Now Film, will co-produce an English-language remake with U.S.-based Spanish producer Frida Torresblanco (Pans Labyrinth). Lee said, "We have decided to take part in the production as a collaborator in order to ensure the quality of the remake, rather than simply handing over the storyline." 

==References==
 

==External links==
*   
* 
* 
* 

 
 
 