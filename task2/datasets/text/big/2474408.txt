Mom and Dad
 
 
{{Infobox film
| name           = Mom and Dad
| image          = MomAndDad.jpg
| image_size     =
| caption        = Film poster, circa 1947, listing sex-segregated showings
| director       = William Beaudine
| producer       = Kroger Babb J. S. Jossey 
| writer         = Screenplay:   Mildred Horn
| starring       = Hardie Albright Lois Austin George Eldredge June Carlson Jimmy Clark Bob Lowell 
| music          = Edward J. Kay 
| cinematography = Marcel LePicard
| editing        = Dick Currier
| distributor    = Hygienic Productions Hallmark Productions Modern Film Distributors
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $63,000
}}
Mom and Dad (known as  The Family Story in the United Kingdom)  is a feature-length 1945 film directed by William Beaudine, and largely produced by the exploitation film maker and presenter Kroger Babb. Mom and Dad is considered the most successful film within its genre  of "sex hygiene" films. Although it faced numerous legal challenges and was condemned by the National Legion of Decency,  it went on to become one of the highest-grossing films of the 1940s.
 United States National Film Registry, in recognition of its numerous achievements.

==Production==
Despite the commercially successful run of Babbs debut film, Dust to Dust&mdash;a reworked version of the 1938 film Child Bride&mdash;his production company Cox and Underwood disbanded, forcing him to form his own unit, Hygienic Productions.   Having attended a meeting in Burkburnett, Texas, that discussed the alleged impregnations of young women by G.I. (military)|G.I.s from nearby Sheppard Air Force Base, Babb was inspired to shoot a film based on the subject.   His future wife Mildred Horn drafted a screenplay which later evolved into Mom and Dad. Babb located 20 investors willing to fund the movie, and hired William Beaudine as director. Briggs. 

Production of the film cost Babb and his investors a total of $63,000. Library of Congress press release   The movie was shot in five separate studios over six days in 1944, Pressbook.  and was spread across various Monogram Pictures lots; co-producer J. S. Jossey was a Monogram stockholder. On January 3, 1945, Mom and Dad premiered at the Warner Bros. theatre in Oklahoma City, Oklahoma. 
 feature length status.  Eric Schaefer notes that the "primary purpose" of the plot of Mom and Dad  was to "serve as the vehicle onto which the spectacle of the clinical reels can be grafted", such as the live birth scene.  The marketing materials suggest the latter reason also, and many posters for the film promised that "You   actually SEE the birth of a baby!"   The dialogue is carefully worded, and uses period euphemisms rather than explicit terms that may have been controversial at the time.  In particular, at no time does the film specifically mention sexual intercourse or pregnancy. 

==Plot== hygiene books" from her mother Sarah Blake (Lois Austin); however, the mother refuses because the girl is not yet married.  The girl later learns from her father Dan Blake (George Eldredge) that the pilot has died in a crash. She tears up a letter she had been writing to him, and lowers her head as the film fades into intermission.

The film resumes at the point when the girl discovers that her clothes no longer fit, sending her into a state of despair.  She takes advice from her teacher, Carl Blackburn (Hardie Albright), who had previously been fired for teaching sex education. Blackburn blames her mother for the problem, and accuses her of "neglect  the sacred duty of telling their children the real truth."  Only then is the girl able to confront her mother.   
 stillborn and other times put up for adoption.  

==Cast==
* Hardie Albright – Carl Blackburn, the teacher.
* Lois Austin – Sarah Blake, the mother.
* George Eldredge – Dan Blake, the father.
* June Carlson – Joan Blake, the teen-age girl.
* Jimmy Clark – Joans brother.
* Bob Lowell – Jack Griffin, the pilot.
* Jane Isbell – Mary Lou, Joans friend.
* Jimmy Zaner – Allen Curtis, Joans hometown boyfriend. Robert Filmer – Superintendent McMann.
* Willa Pearl Curtis – Junella, the Blake familys African-American maid.
* Virginia Van – Virginia, Daves girlfriend.
* Forrest Taylor – Dr. Ashley, the obstetrician.
* Jack Roper – The coach.

The official credits also acknowledge The Four Liphams as well as the California State Champion dancers of the jitterbug. 

==Marketing and presentation== Washington Post article covering Babbs  career, the film critic Kenneth Turan wrote that Mom and Dad did not "flourish because of its birth footage&nbsp;or because of its puerile plot, which Babb himself disparages .&nbsp;.&nbsp;.   success flowed, rather, from Babbs extraordinary promotional abilities."   The film was exhibited across the United States, and over 300 prints were produced.  In the weeks preceding the screening, local presenters sought to attract the attention of the towns inhabitants by distributing letters to local newspapers and church leaflets protesting against the films moral basis. This strategy often utilized fabricated letters supposedly written by the mayor of a nearby city, who wished to register concern about local young women in his area who had seen the film and were awakened enough to discuss problems similar to ones of their own. 
 promotional and marketing materials.  Babbs marketing strategy centered on overwhelming small towns with advertisements and letters, in an attempt to create a controversial atmosphere. In keeping with his motto of "You gotta tell em to sell em,"  the film became so ubiquitous that Time (magazine)|Time wrote that its presentation "left only the livestock unaware of the chance to learn the facts of life." 
 Olympic gold medalist Jesse Owens was hired to make appearances instead of an actor playing Forbes.  The "Elliot Forbes" actors were usually people local to the production company, sometimes out-of-work performers. Mondor, personal letter.   Along with "Forbes", presentations were often held with "nurses" in attendance, ostensibly in the event that someone fainted due to the content of the film;  such "nurses" were often hired locally. 

Modern Film Distributors later distributed the film, and sold over forty-five thousand copies of the books Man and Boy and Woman and Girl following Forbess lecture. The text was written by Babbs wife, McDougal.  and was filled with both biological and sexual education materials relevant to the films subject matter; generating extra profit items for their distributors. The sales of these books netted an estimated $31,000 for the distribution company,  while Babb estimated the total sales for all distributions at 40 million copies. 

Babb insisted that the program be followed closely; a contractual agreement with theaters required that each presentation follow a similar approach. Because the Forbes lecture formed part of the viewing, extra   pricing, set specific times for the segregated viewings, and prohibited the screening  of the film on Sundays. 

==Reception==
Mom and Dad is the third highest grossing film of the 1940s in dollar value,  and returned close to $63 for each dollar invested by its backers.  The Los Angeles Times estimates that the film grossed between $40 million and $100 million, Turan, 22,  and it has been cited as the most successful sex hygiene film ever released. It remains the most profitable pre-1960 exploitation film; ranking among the top ten grossing films of both the 1940s and 1950s, even when scaled against those years mainstream releases. 
 motion picture censorship system.  It has been claimed that nearly 428 lawsuits were laid against both Babb and Mom and Dad during the films run.   Babb often used the supposed educational value of his films as an offer of defense, and recommended such tactic to theater owners in his pressbooks.  One successful challenge was in New York City, where Mom and Dad remained censored until 1956, when the Appellate Division of the New York State Supreme Court overturned the ruling of the censorship board, deciding that human birth did not qualify as "indecent". 

According to Modern Film Distributors, as of the end of 1956, the film has been dubbed into a dozen languages and attended by an estimated worldwide attendance figure of over 175 million people, at over 650,000 performances.   Card Mondor purchased the rights to exhibit the film in New Zealand and Australia during the mid-1960s, almost twenty years after the films debut.   In the late 1970s, a story on Babb by the Press-Enterprise estimated that the film had been dubbed into 18 languages. 
  Street Corner Universal produced a similar film, The Story of Bob and Sally, but was unable to screen it due to the production code, and eventually sold the rights.   The volume of imitations led to the formation of Modern Film Distributors, a group of exploitation filmmakers, in an effort to minimize booking conflicts. 
 R rating.   The movie was such a success that it is still shown decades later around the world.   In 2005, a version was added to the National Film Registry. 

==References==

===Notes===
 

===Bibliography===
* Felicia Feaster and Bret Wood, Forbidden Fruit: The Golden Age of Exploitation Film. (Baltimore, Maryland: Midnight Marquee Press, 1999. ISBN 1-887664-24-6)
* David F. Friedman, A Youth in Babylon: Confessions of a Trash-Film King. (Buffalo, New York: Prometheus Books, 1990. ISBN 978-1-57392-236-4)
* Letter to Michael Zengel from Card Mondor, February 5, 1994. Available from the Academy of Motion Picture Arts and Sciences archives.
* Library of Congress:  . URL accessed August 27, 2006.
* Wendy L. Marshall, William Beaudine: From Silents to Television. (Scarecrow Press, 2005; ISBN 0-8108-5218-7) The Press-Enterprise, (Riverside, California), unknown date.
* Pressbook (Wilmington, Ohio: Hallmark Productions, circa 1959.)
* Eric Schaefer, Bold! Daring! Shocking! True!: A History of Exploitation Films, 1919–1959. (Durham, N.C.: Duke University Press, 1999. ISBN 0-8223-2374-5)
* Kenneth Turan, "Kroger Babb: Superhuckster." Los Angeles Times via The Washington Post, November 11, 1977.
* Variety, Kroger Babb obituary, January 30, 1980.
* " ." Reason, November 2003.

==External links==
*  
*  

 

 
 
 
 
 
 
 