Pudhiavan
{{Infobox film
| name           = Pudhiavan
| image          = 
| caption        = 
| director       = Amirjan
| producer       = Kavithalayaa Productions
| writer         = Anandu
| screenplay     = Anandu Murali Anitha Raveendran N. Viswanathan
| music          = V. S. Narasimhan
| cinematography = B. Ragunatha Reddy
| editing        = S. S. Nazeer
| studio         = Kavithalayaa Productions
| distributor    = Kavithalayaa Productions
| released       = 1984
| country        = India
| runtime        = 147 min
| language       = Tamil
}}
 1984 Cinema Indian Tamil Tamil film Raveendran and N. Viswanathan in lead roles. The film, had musical score by V. S. Narasimhan. 

==Cast==
  Murali as Manohar
*Anitha Raveendran
*N. Viswanathan
*Delhi Ganesh
*Mohanapriya
*Vaani
*Indira Devi
*Sulochana
*Ramani
*Sundara Moorthy
*Veera Raghavan
*Sudhan
*Babu
*Kumaresh
*Suresh
*Mahendravarman
*S. Kumar Rajeev in Friendly Appearance
*Gopi in Friendly Appearance
*T. S. Babumohan in Friendly Appearance
*Charle in Friendly Appearance
*Arundathi in Friendly Appearance
*Agalya in Friendly Appearance
 

==Soundtrack== 
The music was composed by VS. Narasimhan.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Naano Kann Paarthen || K. J. Yesudas, Kalyan || Vairamuthu || 04.26 
|- 
| 2 || Then Mazhaiyile || S. P. Balasubrahmanyam || Vairamuthu || 03.42 
|}

==References==
 
 

 
 
 
 


 