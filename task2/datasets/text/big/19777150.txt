På tro og love
 
{{Infobox film
| name           = På tro og love
| image          = På_tro_og_love_1955_Torben_Anton_Svendsen_poster_Aage_Lundvald.jpg
| caption        = Theatrical poster by Aage Lundvald
| director       = Torben Anton Svendsen
| producer       = Erik Balling
| writer         = Erik Balling Knud Poulsen
| starring       = Poul Reichhardt
| music          = 
| cinematography = Verner Jensen Jørgen Skov
| editing        = Carsten Dahl
| distributor    = Nordisk Film
| released       =  
| runtime        = 90 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

På tro og love is a 1955 Danish family film directed by Torben Anton Svendsen and starring Poul Reichhardt.

==Cast==
* Poul Reichhardt - Hans
* Astrid Villaume - Grete
* Helge Kjærulff-Schmidt - Georg
* Gunnar Lauring - Chefen
* Helle Virkner - Vera Gimmer
* Lis Løwert - Solveig
* Ove Sprogøe - Henry
* Sigrid Horne-Rasmussen - Fru Gimmer
* Louis Miehe-Renard - Frederiksen
* Einar Juhl - Revisor
* Carl Johan Hviid - Portieren
* Birgit Sadolin - Telefonpige
* Else Kornerup - Kontordame
* Mogens Lind - Speaker
* Edith Hermansen - Klinikassistent

==External links==
* 

 

 
 
 
 
 
 