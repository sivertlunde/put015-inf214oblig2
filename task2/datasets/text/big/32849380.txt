2-Headed Shark Attack
{{Infobox film
| name           = 2-Headed Shark Attack
| image          = 2HEADEDSHARKATTACKDVD.jpg
| caption        = DVD cover
| director       = Christopher Ray
| producer       = David Michael Latt
| writer         =  
| starring       =  
| music          = Chris Ridenhour
| cinematography = Stuart Brereton
| editing        = Rob Pallatina
| distributor    = The Asylum
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $1 million
| gross          = 
| preceded by    = 
}}
2-Headed Shark Attack is a horror film by The Asylum, released on January 31, 2012 in the United States. The film stars Carmen Electra, Charlie OConnell and Brooke Hogan.        The film premiered September 8, 2012, on Syfy.

==Plot==
A group of friends wakeboarding are attacked and devoured by a two-headed Great White Shark. Meanwhile, a Semester at Sea ship, called the Sea King, led by Professor Franklin Babish and his wife, Anne hits a dead shark, which becomes lodged in the boat’s propeller and damages the ships Hull (watercraft)|hull, causing the boat to take on water. Soon after, the two-headed Great White shark attacks the boat and breaks the radio antenna, preventing ship co-captain Laura from summoning help. The group then notices a deserted atoll nearby, so Laura drives the Sea King closer, before Professor Babish takes the students, consisting of Kate, Paul, Liza, Michelle, Cole, Lyndsey, Ethan, Jamie, Ryan, Kirsten, Alex, Kristen, Kirk, Haley, Jeff, Alison, Mike, and Dana, over to the atoll on a dinghy, while Anne remains on the Sea King with Laura and the ships crew, Han and Dikilla.

As Professor Babish and the students explore the atoll, Laura enters the water to repair the ships hull, but is quickly ripped apart by the two-headed Great White shark. Meanwhile, the group searches the atoll for scrap metal to help repair the boat, where Kate tells Kirsten she is afraid of water. Haley and Alison decide to go swimming topless with Kirk, only to be attacked and eaten by the two-headed shark. The rest of the group meet up and find two small speedboats before an apparent earthquake hits, causing Professor Babish to fall and badly cut his leg. Jeff and Mike take Professor Babish back to the Sea King on the dinghy and Anne tends to her husband while Jeff and Mike head back to the atoll, and discover Lauras severed hand in the water before the two-headed shark bites off Jeffs right arm. They attempt to swim back to the Sea King, but both are ultimately killed.

Kate and Paul manage to fix the two boats they found, while Cole finds a gasoline tank to fuel them. He then jumps in one of the boats with Ryan, Jamie and Alex and drives away, prompting Kate, Paul, and Dana to follow in the other. The two boats race each other, unaware of Professor Babishs and Annes warnings of the two-headed Great White shark. The two-headed shark attacks Coles boat, causing Ryan to fall into the water, where he is devoured, notifying the other students of the shark. Paul realizes the two-headed Great White shark will continue to chase Coles boat as it has a bigger engine. Cole also realizes this and jumps off the boat, leaving Jamie and Alex to be eaten. When they all reach shore, Kate furiously confronts Cole for letting Jamie and Alex die and a few minutes later, Anne, Professor Babish, and the crew arrive at the island, leaving the Sea King.

Shortly afterward, another earthquake hits and the group begins to suspect that that the supposed earthquakes are actually the atoll collapsing in on itself from the underground. Now that it is more urgent to escape, the group creates a plan in which they hook up a generator to metal poles and place them in the water to distract the shark while Kate and Cole travel to the Sea King and repair the hull. The plan works until the two-headed Great White shark attacks the poles, knocking Han and Dikilla into the water where they are eaten, before the two-headed shark swims to the Sea King. Kate fixes the Sea King, only for Cole to drive away without her, forcing her to swim back to the atoll. The two-headed shark attacks the Sea King, causing it to sink and send out a distress signal, and eats Cole, whose cell phones ringing attracted its attention, while he attempts to escape on a lifeboat. As the group panics, the atoll begins to rapidly sink, prompting everyone to flee for their lives, but Kristen and Dana become separated from the group and are devoured when they go onto a dock. At the same time, Professor Babish and Anne spot a small tsunami coming and are eaten as well after they realize that they cannot escape. The tsunami hits and the atoll sinks, leaving the survivors with very little land. They take shelter in the still standing chapel, but the two-headed shark breaks in and devours Lyndsey, Michelle, Liza, and Ethan.

Kate, Paul, and Kirsten escape the chapel and discover the gasoline tank and lure the two-headed shark towards it. Kate attempts to stab the two-headed shark, but Kirsten takes the tank, attracts its attention, and attempts to blow it up with her lighter as it begins chewing her up, but only one of its heads get blown off. Kate and Paul find one of the boats, and turn it on and jump off, before taking shelter on part of the remaining atoll. The shark attacks the boat, which is moving without a pilot, and bites the motor, causing it to explode, finally killing it (similar to the shark death in Jaws (film)|Jaws). A helicopter then comes to save Kate and Paul the only survivors of the entire group of 23 people.

==Cast==
 
* Carmen Electra as Anne Babish
* Charlie OConnell as Professor Babish
* Brooke Hogan as Kate
* Christina Bach as Dana
* David Gallegos as Paul
* Geoff Ward as Cole
* Mercedes Young as Liza
* Shannan Stewart as Lyndsey
* Tihirah Taliaferro as Michelle
* Michael Dicarluccio as Ethan
* Lauren Vera as Jamie
* Marckenson Charles as Ryan
* Ashley Bissing as Kristen
* Corinne Nobili as Kirsten
* Benjamin James as Alex
* Chase Conner as Kirk
* Anna Jackson as Haley
* Amber English as Alison
* Collin Carmouze as Jeff
* Casey King Leslie as Mike
* Morgan Thompson as Laura
* Gerald Webb as Han
* Anthony E. Valentin as Dikilla
 

==Production== Monster Man episode "Seeing Double." The original design of the shark had one shark head stacked on top the other, but creature designer Cleve Hall pushed to change the design to side-by-side heads.

==Soundtrack==
===Tracklist===
"More"
*Music by Sy and Vero
*Produced by Sadaharu Yagi
*Performed by Neon Line

"Swallow Whole"
*Music by Sy and Vero
*Produced by Sadaharu Yagi
*Performed by Neon Line

"Its Killing Me To Live"
*Produced, Music and Lyrics by Matthew Arner

"Jungle Jam"
*Written and Performed by Yoshi Miyamoto

"Transmission"
*Performed by Closer to Venus
*Written and Produced by Eduardo Olguin and Arturo Gutierrez

==Sequel==
Christopher Rey filmed 2015 with The Asylum the sequel 3-Headed Shark Attack, with Jaason Simmons, Danny Trejo and wrestler Rob Van Dam in the leading roles. 

==References==
 

==External links==
*   at The Asylum
*  
*    
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 