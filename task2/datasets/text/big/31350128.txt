Akash Chhoa Bhalobasa
{{Infobox Film
| name           = Akash Chhoa Bhalobasa
| image          = Akash Chhoa Bhalobasa.jpg
| image_size     = 200px
| caption        = VCD Cover
| director       = S A Haque Olike
| producer       = Vision
| writer         = S A Haque Olike  Amzad Hossain Riaz Purnima Purnima Razzak Sharmily Ahmed Prabir Mitra Afzal Sharif
| music          = Habib Wahid S I Tutul BFDC
| cinematography = Alamgir Khoshru
| editing        = Touhid Hossain Chowdhury
| distributor    = Vision
| released       = 24 October 2008 
| runtime        = 137 Min.
| country        = Bangladesh Bengali
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
| imdb_id        =
}}
Akash Chhoa Bhalobasa ( ) is a Bangladeshi Bengali language film.  The film released on 24 October 2008 in all over Bangladesh.

==Cast==
* Riaz (Actor)|Riaz as Akash
* Purnima (Actress)|Purnima as Chhoa
* Razzak as Mir Amzad Ali
* Sharmily Ahmed as Chhoa’s Grand Mother
* Prabir Mitra as Chowdhury
* Afzal Sharif as Altu
* Nasrin as Special Appreance (Song-Dance)
* Jamilur Rahman as Chhoas Servant
* Rehana Jolly as Chowdhury’s Wife
* Diti as Minu Special Appreance (Akash’s Mother)

==Music==
{{Infobox album
| Name = Akash Chhoa Bhalobasa
| Type =Soundtrack
| Artist = Habib Wahid and S I Tutul
| Cover = 
| Background = Gainsboro |
| Released = 2008 (Bangladesh)
| Recorded = 2007
| Genre = Soundtrack/Filmi
| Length =
| Label =
| Producer =Agniveena
| Reviews =
| Last album = 
| This album = Akash Chhoa Bhalobasa (2008)
| Next album = 
|}}

===Soundtrack===

{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Title !! Singers !! Performers !! Notes
|- 1
|Prithibir Joto Sukh Habib Wahid and Nancy Riaz (Actor)|Riaz Purnima
|
|- 2
|Hridoye Likhechhi Tomari Nam S I Tutul and Samina Chowdhury Riaz (Actor)|Riaz Purnima
|Title Song
|- 3
|Haway Haway Dolna Dole Habib Wahid and Nancy Riaz (Actor)|Riaz Purnima
|- 4
|Tumi Swapno Tumi Swargo Subir Nandi and S I Tutul Purnima (Actress)|Purnima, Razzak and Sharmily Ahmed
|
|- 5
|Najor Na Lage Jeno S I Tutul Riaz (Actor)|Riaz Purnima
|
|- 6
|Bohupath Khuje Nadi Sagore Haray S I Tutul Riaz (Actor)|Riaz Purnima
|
|- 7
|Kancha Kancha Basher Bera Kanak Chapa and S I Tutul Riaz (Actor)|Riaz, Purnima (Actress)|Purnima, Nasrin and Masum Babul
|
|-
 

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 