What Richard Did
{{Infobox film
| name           = What Richard Did
| image          = What-Richard-Did-DVD-cover.jpg
| size           = 200
| caption        = DVD cover
| alt            = 
| director       = Lenny Abrahamson
| producer       = Ed Guiney
| writer         = Malcolm Campbell 
| starring       = 
| music          = 
| cinematography = David Grennan
| editing        = Nathan Nugent
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Ireland
| language       = English
| budget         = 
| gross          = $488,327 
}}

What Richard Did is a 2012 Irish film directed by Lenny Abrahamson and written by Malcolm Campbell. The film is loosely based on Kevin Powers Bad Day in Blackrock, a fictionalised story based on the assault on Brian Murphy outside the Burlington Hotel in Dublin in 2000. It won the best Irish film of the year award at the 10th Irish Film & Television Awards  and was the most commercially successful Irish film of 2012. 

It has screened at the 2012 Toronto International Film Festival and the BFI London Film Festival and was selected to screen at the Tribeca Film Festival in New York in April 2013. 

==Plot==
Richard Karlsen is the golden-boy athlete, and undisputed alpha-male of his privileged set of Dublin south side teenagers. Everything is perfect for Richard until one drunken summer night he destroys it all, by viciously assaulting a romantic rival. The boy’s subsequent death from his injuries shatters the lives of Richard and the people closest to him.

  

==Cast==
* Jack Reynor as Richard Karlsen
* Róisín Murphy as Lara
* Lars Mikkelsen as Peter Karlsen
* Lorraine Pilkington as Katherine Karlsen
* Sam Keeley as Conor Harris
* Michelle Doherty as Liv

==Reception==
 

===Critical response===
Peter Bradshaw writing in the The Guardian called the film slow-burning and disturbing. "Abrahamson shows that whatever the failings and weaknesses of the young, it is their elders who insist on wriggling away from blame. What Richard Did is an engrossing and intelligent drama that throbs in the mind for hours after the final credits". 

===Awards===
  
In February 2013, the film picked up five awards at the 10th Irish Film & Television Awards including the award for Best Film. Jack Reynor won for Actor Lead Film whilst Lenny Abrahamson and Malcolm Campbell picked up awards for Best Director and Script with Nathan Nugent winning for Editing Film.    In May 2013, It won the Golden Tulip (best movie) at The Istanbul International Film Festival in Turkey. 

==Home Media==
The film was released on DVD in Ireland on 8 February 2013. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 