Point Blank (2010 film)
{{Infobox film
| name           = Point Blank
| image          = PointBlank2010Poster.jpg
| caption        = French poster
| director       = Fred Cavayé
| producer       = Cyril Colbeau-Justin Jean-Baptiste Dupont
| writer         = Fred Cavayé Guillaume Lemans
| screenplay     = 
| story          = 
| based on       =  
| starring       = Gilles Lellouche Roschdy Zem Gérard Lanvin Elena Anaya
| music          = Klaus Badelt
| cinematography = Alain Duplantier
| editing        = Benjamin Weill Gaumont
| distributor    = Gaumont (France) Magnolia Pictures (US)
| released       =  
| runtime        = 84 minutes
| country        = France
| language       = French
| budget         = €10.8 million 
| gross          = $9,230,157 
}}
 thriller film directed by Fred Cavayé and starring Gilles Lellouche, Roschdy Zem, Gérard Lanvin, and Elena Anaya. 

==Plot== sonogram appointment where she is told that she must stay on her back for six weeks to avoid life threatening complications. Meanwhile, Hugo Sartet, a safecracker and hired thief, is running from two hitmen. During the chase, he is hit by a motorcycle and rushed to the hospital where Samuel works. Shortly after, Samuel sees a man leave Hugos bedside and arrives just in time to reinsert Hugos respirator which had been unplugged. This prompts the police to set up a protection detail under the command of Captain Catherine Fabre. While discussing the assassination attempt, another captain, Patrick Werner, arranges to have jurisdiction of the case transferred to him, saying that he has been after Sartet for years.

When Samuel returns home that day, he and Nadia are attacked and she is abducted.  Samuel gets a phone call saying that he must help Hugo Sartet escape from the hospital if he wants to see his wife again. At the hospital, the cop guarding Hugos bed refuses to leave but Samuel is able to subdue him with a defibrillator. Samuel then wakes up Hugo with a shot of adrenaline and guides him outside while holding the cops gun. When Nadias captors call again, they are revealed to be Hugos brother Luc working with a man named Marconi. The brothers arrange to set up the hostage trade at a nearby railway station but when Samuel and Hugo get there, Hugo notices the two men who were trying to kill him earlier. Knowing that they are dirty cops, Hugo persuades Samuel to turn around and flee to his apartment. While Hugo is showering, Samuel decides that he does not trust them to deliver his wife safely a second time and calls the police. Captain Fabre says that she is closest to the address Samuel gave and calls for backup. However, a group of police led by Captain Werner arrives first.

When Fabre bursts in moments later, Werner reacts to this unforeseen circumstance by shooting Fabre, revealing himself to be part of the conspiracy. Werner leaves and tells his men to finish off the others but Hugo gains the upper hand and coerces a confession out of a cop named Vogel. Vogel says that Werner enlisted Hugo with the help of Marconi to crack the safe as part of a plan to extort money from Francis Meyer, a millionaire who was recently murdered. Vogel also says that Werner forced Meyer to change his will, shot him in the head and kept a video of it as an insurance policy which is stored on a USB drive in his office. When the other police arrive, Samuel and Hugo must escape since they have been framed as cop killers. They flee to the warehouse where Nadia was being held but discover that Marconi has sold them out. Hugos brother is dead and Nadia has been taken by the dirty cops until they can eliminate all witnesses. Hugo tracks down Marconi and kills him. He and Samuel then come up with a plan that will enable them to break into the police station.

Coordinated by Hugos underground contacts, a series of robberies breaks out, forcing the police station into a frenzy. Samuel and Hugo arrive at the police station where Nadia is being held by a dirty cop named Moreau. Using the chaos to disguise themselves as police, Hugo goes to Werners office and Samuel begins searching for his wife. Not at the police station, Werner discovers Marconis body and tells Moreau to kill Nadia by pushing her out of the bathroom window, making it look like a suicide. Samuel arrives to find Nadia fighting for her life, and together, they are able to overpower Moreau and begin making their escape.

Hugo attempts to break into Werners safe but runs out of time to crack it before Werner enters his office and immediately opens his safe to ensure the USB drive is safely there. Taking the upper hand, Hugo receives the drive by holding Werner at gunpoint. Before Hugo knocks the wind out of him, Werner taunts Hugo about how his brother died. Nadia begins to hemorrhage, finally showing symptoms that the doctor warned her about. As Samuel tries to stop the bleeding, he is recognized as a wanted man and taken away in handcuffs. He is passed by Hugo, still blending into the crowd, who slips a phone into Samuels pocket with the USB drive attached. When he is a safe distance away, Hugo calls the phone, prompting Officer Susini to seize the USB drive as evidence against Samuel. Werner comes to but Susini is already watching the tape when he arrives and he is too late to cover it up. Still in custody, Samuel is allowed to go the hospital to visit his wife, who has given birth to a healthy baby.

Eight years later, the Pierret family is celebrating Christmas and Samuel hears a news report about Patrick Werner who is halfway through his prison sentence. The report mentions that Werner was released for one day, only to be found dead, hanging by a noose. Samuel stares, knowing that Hugo Sartet was responsible.

==Cast==
* Gilles Lellouche as Samuel Pierret
* Roschdy Zem as Hugo Sartet
* Gérard Lanvin as Captain Patrick Werner
* Elena Anaya as Nadia Pierret
* Mireille Perrier as Captain Catherine Fabre
* Claire Perot as Officer Anaïs Susini
* Moussa Maaskri as Officer Vogel
* Pierre Benoist as Officer Mercier
* Valérie Dashwood as Officer Moreau
* Virgile Bramly as Officer Mansart
* Nicky Naude as Officer Richert
* Adel Bencherif as Luc Sartet
* Brice Fournier as Marconi
* Jacques Colliard as Francis Meyer

==Remake== The Target starred Ryu Seung-ryong and Lee Jin-wook in the leading roles, and was directed by Yoon Hong-seung. 

Ravi K. Chandran will direct a Bollywood remake, set to begin production in 2015. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 