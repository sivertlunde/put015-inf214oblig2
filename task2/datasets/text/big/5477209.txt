Saps at Sea
{{Infobox film
  | name = Saps at Sea
  | image = L&H Saps at Sea 1940.jpg
  | caption = Theatrical poster  Gordon Douglas
  | producer = Hal Roach Felix Adler Gil Pratt Charles Rogers Charlie Hall
  | music = Marvin Hatley   Leroy Shield
  | cinematography = Art Lloyd William Ziegler
  | distributor = United Artists
  | released =  
  | runtime = 57 24"
  | language = English
  | country = United States
  | budget = 
}} Gordon Douglas, distributed by United Artists, and Laurel and Hardys last film produced by Hal Roach Studio.

== Plot ==
Stan and Ollie work in a horn factory, where Hardy is already under stress from all the incessant noise. One worker (  they can while never actually going out to sea. When Stans trombone teacher (Eddie Conrad) arrives and Ollie, returning from a fight with the janitor (Ben Turpin) who messed up the plumbing, hears the music, goes berserk and throws the teacher out, he knows he should take that advice. Phoning the hotel manager to complain why that teacher was allowed in, Hardy is accidentally knocked out the window and into the street.

Stan and Ollie rent an unseaworthy boat called Prickly Heat that is supposed to stay moored to the Dock (maritime)|dock. Later that night an escaped murderer named Nick Grainger (Richard Cramer) stows away on the boat to avoid being caught by the police. The goat which they have brought to provide milk (but which is evidently just a billy goat, since it has big long horns, and is referred to as "he") chews away at the docking line, and the boat drifts out to sea. The next day Nick confronts Stan and Ollie with a gun (which he affectionately names "Nick Jr") and tells them to make him breakfast. They have no food on board, so they decide to prepare Nick a "synthetic" breakfast made up of string, soap and whatever else they can find. Nick spies on them and realizes what they are up to, and forces them to eat the fake food. Stan becomes inspired and starts to play his trombone and Ollie starts to go crazy and overcomes the criminal. In fact, a few times Laurel pauses to catch his breath and its starts to smoke out of the trombone and Hardy has to call to him to resume, to maintain his animal rage until he finally knocks Nick out cold.

When the police eventually arrive in another boat to take Nick into custody, Laurel demonstrates to them how he got Hardy powered up --- by playing the mangled trombone. The result: Hardy again flies into a blind horn-induced rage and mindlessly assaults one of the cops, the boys get arrested, also, and they are thrown into jail in the same cell that Nick is in.  The audience is left to imagine what assorted horrors await the boys when the vengeful Nick regains consciousness.

== Notes == Charlie Hall, James Finlayson and Ben Turpin. Mary Gordon who played Mrs. Hudson opposite Basil Rathbones Sherlock Holmes. HMS Prince of Wales during the voyage to Newfoundland, where Franklin Delano Roosevelt and Winston Churchill met to establish the Atlantic Charter. It was a favorite film of Prime Minister Winston Churchill, who called it "A gay but inconsequent entertainment". 
* The title is a spoof of the 1937 film, Souls at Sea, starring Gary Cooper and George Raft.

==Cast==
* Stan Laurel - Stan
* Oliver Hardy - Ollie
* Richard Cramer - Nick Granger James Finlayson - Dr. Finlayson
* Ben Turpin - Cross-eyed plumber
* Eddie Conrad - Professor OBrien Charlie Hall - Desk clerk
* Harry Bernard - Harbor patrol captain
* Patsy OByrne - Mother
* Francesca Santoro - Little girl
* Harry Hayden - Mr. Sharp Robert McKenzie - Captain McKenzie
* Patsy Moran - Switchboard operator
* Gene Morgan - First policeman Mary Gordon - Mrs. ORiley
* Eddie Borden - Berserk employee

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 