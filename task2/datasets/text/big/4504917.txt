I Am a Sex Addict
{{Infobox Film
| name           = I Am a Sex Addict
| image          = Sex_Addict_poster.jpg
| image_size     = 225px
| caption        = theatrical poster
| director       = Caveh Zahedi
| producer       = Thomas Logoreci Greg Watkins Caveh Zahedi
| writer         = Caveh Zahedi
| starring       = Caveh Zahedi Rebecca Lord
| music          = Hilary Soldati
| cinematography = Greg Watkins
| editing        = Thomas Logoreci Caveh Zahedi
| distributor    = IFC Films
| released       = October 14, 2005
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $120,790
| preceded_by    = 
| followed_by    = 
}}
 autobiographical comedy comedy by American independent independent director documentary style, the film chronicles Zahedis own sex addiction and its impact on his life, relationships, and film making. His addiction was manifested by visiting prostitutes, and being open about this with his successive partners.
 Showtime in Sundance Channel in the United States.

Though not a sequel; his 2001 film In the Bathtub of the World documents his life from where I Am a Sex Addict ends.

==Cast==
  
*Caveh Zahedi as Caveh
*Rebecca Lord as Caroline
*Emily Morse as Christa
*Amanda Henderson as Devin
*Greg Watkins as Greg
*Olia Natasha as French prostitute
*Corinna Chan as Asian prostitute
*Stephanie Carwin as Italian prostitute  
 
*Katarina Fabic as L.A. night prostitute
*Alexandra Guerineaud as Prostitute in Paris
*Mara Luthane as Newscaster
*Bruna Raynaud as Prostitute in Paris
*Anastasia Vega as Oscar presenter
*Jasmine Raff as German brothel prostitute
*Vicca as German brothel prostitute
 

== External links ==
 
*  
*  

 

 
 
 
 
 
 

 