Naan Rajavaga Pogiren
{{Infobox film
| name           = Naan Rajavaga Pogiren
| image          = Naan Rajavaga Pogiren.jpg
| alt            =  
| caption        = Film poster
| director       = Prithvi Rajkumar
| producer       = V Chandran
| writer         = Prithvi Rajkumar Vetrimaaran (dialogues) Nakul Chandini Chandini Avani Modi
| music          = G. V. Prakash Kumar
| cinematography = Velraj
| editing        = Kishore Te.
| studio         = Udhayam VLS Cine Media
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         =
| gross          =
}}
Naan Rajavaga Pogiren (English:I am going to become a King) is a 2013 Tamil action film co-written and directed by Prithvi Rajkumar, assistant of Vetrimaaran and writer of Sathyaraj starrer Lootty and 6.2.  The film features Nakul (actor)|Nakul, Chandni and newcomer Avani Modi in the lead roles. The film opened average reviews and was an average grosser at the box office. 

==Cast== Nakul as Raja / Jeeva Chandini as Valli
* Avani Modi as Reema
* Nishanth as Wahab
* A. Venkatesh (director)|A. Venkatesh as Isakkimuthu Annachi
* Gaurav Narayanan as Police Ravi
* Manivannan as Kamaraj
* Bobby Simha as Shankar Subramaniam Kasthuri as Professor Bharathi Suresh as Rajas Father
* Seetha as Rajas Mother
* Vanitha Vijayakumar as Dr. Diana Chetan as Dr, Dianas Husband
* Delhi Ganesh as Kamarajs Assistant
* Mayilsamy
* G. V. Kumar as Reemas father
* Vasu Vikram as Rajas Uncle Aarthi as Karate Durga
* Boys Rajan as Vallis Father

Special appearances
* Zarine Khan
* Vetrimaaran
* G. V. Prakash Kumar
* Velraj

==Production==
The shoot of the film began on December 12, 2011 at Mathivanan Home in Chinnalapatti.  Based on a request from Prithvi Rajkumar, Vetrimaaran directed the particular scene featuring Nakul and also wrote the dialogues for the film.  

==Soundtrack==
{{Infobox album|  
| Name = Naan Rajavaga Pogiren
| Type = Soundtrack
| Artist = G. V. Prakash Kumar
| Cover  =
| Released =   Feature film soundtrack
| Length =  Tamil
| Label = Gemini Audio
| Producer = G. V. Prakash Kumar
| Reviews =
| Last album =Paradesi (2012 film)|Paradesi (2012)
| This album =Naan Rajavaga Pogiren (2012)
| Next album =Ongole Githa (2012)
}}
The soundtrack, composed by G. V. Prakash Kumar, was released on December 14, 2012 at Sathyam Cinemas by Ameer Sultan and Dhanush. Apart from the cast and crew, Vasanthabalan, Gaurav, Sivakarthikeyan, Jithan Ramesh, Vanitha Vijayakumar, Sriman, Dhananjayan were among those present.  Musicperk.com rated the album 5.5/10 quoting "GV misses the bus". 

{{Track listing
| extra_column = Performer(s)
| lyrics_credits = yes
| title1 = College Paadam | extra1 = Rahul Nambiar, Megha (singer)|Megha, Santhosh | lyrics1 = Annamalai | title2 = Yaarivano | extra2 = G. V. Prakash Kumar, Saindhavi | lyrics2 = Madhan Karky | title3 = Kalaaipom | extra3 = Silambarasan, Maya, Satish | lyrics3 = Prithvi Rajkumar | title4 = Raja Raja | extra4 = Shalmali Kholgade | lyrics4 = Madhan Karky | title5 = Malgove | extra5 = Sonu Kakkar | lyrics5 = Annamalai | length =
}}

==Critical reception==
Hindu wrote:"Not-bad fare for masala movie fans".  Behindwoods wrote:"Naan Rajavaga Pogiren is an ambitious effort, by a promising new comer, but with a less gripping screenplay".  Sify stated  On the whole, it is well-worn, formulaic fare that might appeal to viewers who find comfort in the familiar. If you have time go for it, as it is not entirely unwatchable. 

== References ==
 

== External links ==
 

 

 
 
 
 
 