Blood Creek
 
{{Infobox film
| name           = Blood Creek
| image          = Blood Creek.jpg
| caption        = Theatrical release poster
| director       = Joel Schumacher
| producer       = Paul Brooks Tom Lassally Robyn Meisinger
| writer         = Dave Kajganich
| narrator       = 
| starring       = Dominic Purcell Henry Cavill Michael Fassbender
| music          = David Buckley
| cinematography = Darko Suvak Mark Stevens
| studio         = Gold Circle Films
| distributor    = Lionsgate
| released       =   }}
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
| website        = 
}}
Blood Creek, previously known as Creek and Town Creek, is a horror film directed by Joel Schumacher, starring Michael Fassbender as the main antagonist and written by Dave Kajganich.  The film had a limited theatrical release on September 18, 2009.  The film also stars Dominic Purcell and Henry Cavill as brothers on a mission of revenge who become trapped in a harrowing occult experiment dating back to the Third Reich. 

==Plot==
 
In 1936, a German professor, Richard Wirth, is hosted by the Wollners, a family of German emigrants in West Virginia.  The Wollners believe him to be a visiting scholar, but Wirth turns out to be a Nazi occultist who seeks a Viking runestone buried on their property.  When Wirth reveals he wants to use it for evil, he is interrupted by the family, who trap him in their basement and bind him through a ritual that requires frequent human sacrifices.  Linked to Wirth, the family survive through the decades, operating as both captors and servants to Wirth, who they keep weakened.

In 2007, 25-year-old Evan Marshall is surprised when his older brother Victor suddenly appears after having disappeared during a camping trip in rural West Virginia.  Victor explains that he has escaped his captors, and they quickly prepare to return for vengeance.  The brothers head to the farm and confront the Wollners. They, in turn, warn the siblings about Wirth. They do not listen until Wirth gets out of the cellar and begins his terror. Wirth reveals that the reason Victor was able to escape was because Wirth knew that Victor would come back to the farm for revenge and would eventually free him from the Wollners, so he let Victor go on purpose. The brothers manage to poison and decapitate Wirth, but as a result the Wollners rapidly turn old and die. Before the youngest dies, she tells Evan that SS leader Heinrich Himmler has sent eight more Nazi agents to different farms. Evan finds a map that was under the farm and discovers that others like Wirth are at other farms. While Victor returns home to his family, Evan heads out to the other farms to stop the Nazis.

==Cast==
*Dominic Purcell – Victor Marshall 
*Henry Cavill – Evan Marshall 
*Michael Fassbender – Richard Wirth Emma Booth – Liese
*Rainer Winkelvoss – Otto
*Shea Whigham – Luke
*Wentworth Miller – German soldier (Uncredited)

==Production==
 
 
The production was filmed on locations in Romania (Bucharest and MediaPro Studios).

==Reception==
 
Blood Creek received poor reviews from critics.  Rotten Tomatoes, a review aggregator, reports that 33% of six surveyed critics gave the film a positive review; the average rating was 4/10.   Bloody Disgusting rated the film 3/5 stars and called it "an enjoyable, marginally original, and fast-paced tale, with a franchise-ready villain and some truly entertaining setpieces."   Justin Julian of DreadCentral rated it 2/5 stars, and, while he praised the villain, stated that the film needed "two or three rewrites ... to tighten the script." 

==Release==
The DVD was released on January 19, 2010 and features audio commentary from Joel Schumacher. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 