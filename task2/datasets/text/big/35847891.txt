White Nights (1916 film)
{{Infobox film
| name           = White Nights
| image          = 
| image_size     = 
| caption        = 
| director       = Alexander Korda
| producer       = Jenő Janovics
| writer         =  Victorien Sardou (play)   Alexander Korda    Jenő Janovics
| narrator       = 
| starring       = Lili Berky Kálmán Körmendy  György Kürthy
| music          = 
| editing        = 
| cinematography = 
| studio         = Corvin Film
| distributor    = 
| released       = 1916
| runtime        = 
| country        = Hungary Silent  Hungarian intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian silent silent drama film directed by Alexander Korda and starring Lili Berky, Kálmán Körmendy and György Kürthy. It was based on the play Fédora (play)|Fédora by Victorien Sardou and is sometimes known by the alternative title Fédora. It was Kordas first film for the Corvin Film studio. It was a major success and was one of the first Hungarian films to be exported to other countries. 

==Cast==
* Lili Berky
* Kálmán Körmendy 
* György Kürthy
* Andor Szakács
* Rezső Harsányi
* Valeria Berlányi
* Aranka Laczkó
* József Berky

==References==
 

==Bibliography==
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.

==External links==
* 

 

 

 
 
 
 
 
 
 
 