Babbitt (1934 film)
{{Infobox film
| name           = Babbitt
| image          = 
| image_size     = 
| caption        = 
| director       = William Keighley
| producer       = Samuel Bischoff (uncredited)
| writer         = Mary C. McCall, Jr. Tom Reed (adaptation) Niven Busch (adaptation) Ben Markson (additional dialogue)
| based on       =  
| starring       = Aline MacMahon Guy Kibbee Claire Dodd
| music          = 
| cinematography = Arthur L. Todd
| editing        = Jack Killifer
| studio         = First National Pictures
| distributor    = First National Pictures
| released       =  
| runtime        = 74-75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Babbitt is a 1934 film adaptation of the novel of the same name by Sinclair Lewis directed by William Keighley and starring Aline MacMahon, Guy Kibbee and Claire Dodd. A staid small-town businessman gets ensnared in shady dealings.

==Cast==
*Aline MacMahon as Myra Babbitt
*Guy Kibbee as George F. Babbitt
*Claire Dodd as Tanis Judique
*Maxine Doyle as Verona Babbitt
*Glen Boles as Ted Babbitt
*Minor Watson as Paul F. Reisling
*Minna Gombell as Zilla Reisling Alan Hale as Charlie McKelvey
*Berton Churchill as Judge Virgil Thompson
*Russell Hicks as Commissioner Lyle Gurnee
*Nan Grey as Eunice Littlefield (as Nan Gray) Walter Walker as Luke Ethorne
*Arthur Aylesworth as Zeke
*Addison Richards as District Attorney
*Harry Tyler as Martin Gunch
*Arthur Hoyt as Willis Ivans
*Mary Treen as Miss McGoun
*Hattie McDaniel as Rosalie (uncredited)

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 