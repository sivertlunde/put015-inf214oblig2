The Big Show-Off
{{Infobox film
| name           = The Big Show-Off
| image          =
| image_size     =
| caption        =
| director       = Howard Bretherton
| producer       = Claude Spence (associate producer) Sydney M. Williams (producer)
| writer         = Lászl&#xF3; Vadnay (screenplay) Richard Weil (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Jack Greenhalgh
| editing        =
| distributor    = Republic Pictures
| released       = 22 January 1945
| runtime        = 79 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Arthur Lake before his return to the Blondie (1938 film)#Blondie film series|Blondie film series and Dale Evans, most frequently seen with her husband Roy Rogers.

==Plot==
Nightclub pianist Sandy Elliott is madly in love with nightclub singer June Mayfield, who ignores his existence, preferring the obnoxious Wally Porter, the nightclub emcee.  Sandy follows June to discover to his disgust that she is a big fan of professional wrestling.  Sandys friend Joe the night club owner decides to make the shy Sandy attractive to June by paying a thug to disrupt Junes singing, then being thrown out by Sandy.  Joe adds fuel to Junes new, smoldering love for Sandy by making her promise not to tell a secret: that Sandy is really the masked wrestler known as "The Devil".

==Cast== Arthur Lake as Sandy Elliott, Shy Pianist
*Dale Evans as June Mayfield, Night Club Singer
*Lionel Stander as Joe Bagley, Club Owner
*George Meeker as Wally Porter, Club MC Paul Hurst as The Devil, Masked Wrestling Champion
*Marjorie Manners as Mitzi
*Sammy Stein as Boris the Bulgar, Wrestler
*Louis Adlon as Muckenfuss
*Dan Tobey as Wrestling Announcer Douglas Wood as Dr. Dinwiddle
*Emmett Lynn as Hobo
*Sherry Cameron as Dancer, Cameron and Kirby Team
*Klayton Kirby as Dancer, Cameron and Kirby Team
*Anson Weeks as Orchestra Leader

==Soundtrack==
* Dale Evans with the Anson Weeks Orchestra - "Theres Only One You" (Written by Dale Evans)
* Dale Evans with the Anson Weeks Orchestra - "Cleo From Rio" (Written by Dave Oppenheim and Roy Ingraham)
* Sherry Cameron and Klayton Kirby with the Anson Weeks Orchestra - "Memories of Old Vienna"
* "Hoops My Dear" (Written by Dave Oppenheim)

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 