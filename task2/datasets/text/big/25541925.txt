Abandoned (2010 film)
 
 
{{Infobox film
| name           = Abandoned
| image          = Abandoned poster.jpg
| alt = 
| caption        = Official theatrical poster
| director       = Michael Feifer
| producer       = Barry Barnholtz Michael Feifer Jeffrey Schenck Diane Healey Wood Dickinson Peter Sullivan Jeffrey Schenck (story)
| screenplay     = 
| story          = 
| based on       =  
| starring       = Brittany Murphy Dean Cain Mimi Rogers
| music          = Andres Boulton
| cinematography = Denis Maloney, A.S.C.
| editing        = Bryan Roberts
| studio         = Renegade Pictures ARO Entertainment Barnholtz Entertainment Feifer Worldwide
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Abandoned is a thriller film directed by Michael Feifer and starring Brittany Murphy, Dean Cain, Mimi Rogers and Jay Pickett. 

==Synopsis==
The film opens with Mary Walsh (Murphy) who is a banker delivering her boyfriend, Kevin Peterson (Cain), to a hospital for a routine outpatient surgery. A nurse tells her the surgery will be exactly one hour. When she returns to take Kevin home, she discovers that he has mysteriously disappeared. An administrator (Rogers) can find no record of Kevin, and when Mary contacts the police, Detective Franklin arrives and initiates a search for Kevin but finds no evidence that Kevin was ever in the facility.

Increasingly frantic, Mary is taken to staff psychiatrist Dr. Bensley (Peter Bogdanovich), who pronounces her unstable. Now she must not only find her missing boyfriend, but prove her sanity as well.

Mary is then approached by an anonymous older man claiming to know of Kevin’s whereabouts. He demands a ransom of $10 million and informs Mary she has one hour to comply or her boyfriends life will be at risk. Forced with no other alternative than to embezzle the money to secure Kevins release, Mary steals the money from her bank. However, when she transfers the funds as directed she comes face to face with Kevin and discovers the terrible truth.

Kevin is part of the gang who "kidnapped" him and she has been ensnared in an elaborate scheme aimed at stealing $10m from her bank. With the money gone and Mary as the only witness the gang decide they need to remove Mary.

Mary successfully escapes from one of the gang members (Hallow) but in so doing she kills him in self-defense. When Hallows phone rings she picks it up and hears the others waiting on confirmation that she has been killed. Kevin realizes that Mary is still alive and orders the others to return and finish off Mary. The gang attempt to run Mary down in their van, but she somehow manages to escape through a doorway prompting two of the gang to chase her while Amanda stays behind. Mary manages to kill one of them (Cooper) and continues to evade the other.

Detective Franklin, chasing a lead, uncovers the plot and races back to the hospital. When he arrives he manages to apprehend one of the criminals. He also steps in to save Marys life by shooting an armed Kevin, whereupon the film ends.

==Cast==
* Brittany Murphy as Mary Walsh
* Dean Cain as Kevin Peterson
* Mimi Rogers as Victoria Markham
* Peter Bogdanovich as Dr. Bensley
* Jay Pickett as Detective Franklin
* Tim Thomerson as Cooper
* America Young as Amanda
* Tara Subkoff as Nurse Anna
* Wood Dickinson as Man in Suit

==Production==
Abandoned was shot in June 2009 and was Murphys last filmed project before her death on December 20, 2009. 

==Release==
Anchor Bay Entertainment acquired distribution rights in North America and released the film direct-to-video on August 24, 2010. 

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 