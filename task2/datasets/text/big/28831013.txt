Glasblåsarns barn
{{Infobox film
| name           = Glasblåsarns barn
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Anders Grönros
| producer       = Anders Birkeland Hans Lönnerheden
| screenplay     = Anders Grönros
| story          = Maria Gripe
| narrator       = 
| starring       = 
| music          = 
| cinematography = Philip Øgaard
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Sweden, Norway, Finland
| language       = Swedish
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Glasblåsarns barn is a 1998 Swedish film directed by Anders Grönros. It is based on the novel with the same name by Maria Gripe.

==Cast==
*Stellan Skarsgård – Albert
*Pernilla August – Sofia
*Thommy Berggren – the Emperor
*Elin Klinga – the Empress
*Lena Granhagen – Flaxa Mildväder
*Oliver P. Peldius – Klas
*Jasmine Heikura – Klara
*Ann-Cathrin Palme – Nana
*Måns Westfelt – the coachman
*Ewa Fröling – Nana (voice)
*Johan Ulveson –  Kloke (voice)
*Helge Jordal – the glass seller
*Margreth Weivers – the doll seller
*Martin Lange – a butler
*Peter Nystedt – a butler

==External links==
* 
* 

 
 
 
 


 