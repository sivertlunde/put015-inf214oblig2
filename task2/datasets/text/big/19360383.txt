A Tough Winter
{{Infobox film
| name = A Tough Winter
| image =Tough winter TITLE.JPEG
| image size =
| caption =
| director = Robert F. McGowan
| producer = Robert F. McGowan Hal Roach
| writer = Robert A. McGowan H. M. Walker
| narrator =
| starring = Allen Hoskins Mary Ann Jackson Bobby Hutchins Jackie Cooper Norman Chaney Pete the Pup
| music = Ray Henderson Marvin Hatley
| cinematography = Art Lloyd
| editing =Richard C. Currier MGM
| released =  
| runtime = 20 22"
| country = United States
| language = English
| budget =
}}
 short comedy film directed by Robert F. McGowan.    It was the 99th (11th talking) Our Gang short that was released.

==Plot==
More a vehicle for comedian Stepin Fetchit, the gang go to Wheezers and Mary Anns house to have a taffy pull. But they get the ingredients mixed up and make a huge mess. They attempt to clean up and ask Stepin to help, but he crosses wires, plumbing and gas lines. As a result, light bulbs pop, water sprays from gas heaters, a phone acts as a vacuum cleaner and music plays from the icebox.

==Production notes==
The first Pete the Pup was poisoned by an unknown assailant after this film. Because of perceived racism toward African Americans, A Tough Winter was eliminated from the syndicated Little Rascals television package in 1971.

==Cast==
===The Gang===
* Norman Chaney as Chubby
* Jackie Cooper as Jackie
* Allen Hoskins as Farina
* Bobby Hutchins as Wheezer
* Mary Ann Jackson as Mary Ann
* Beverly Parrish as Girl with Mary Ann
* Warner Weidler as Our Gang member
* Wolfgang Weidler as Our Gang member
* Pete the Pup as Himself
* Dinah the Mule as Herself
* Tommy Atkins as Toddler (unconfirmed)

===Additional cast===
* Stepin Fetchit as Stepin
* Lyle Tayo as Miss Radio

==See also==
* Our Gang filmography

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 