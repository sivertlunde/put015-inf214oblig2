Concealed 360
{{multiple issues|
 
 
}}

{{Infobox film
| title      = Concealed 360
| image      = Concealed the 360 Degree Film.jpg
| director   = Martin Wood
| writer     = Scott Fowler
| editing    = Martin Wood Scott Fowler
| music      = Martin Wood
| runtime    = 25 minutes
| released   =  
| country    = United States
| language   = English
}}
Concealed 360 is an interactive short film which uses 360-degree audio and visual exploration to explore how one’s personal perspective affects their interpretation of narrative.  Filmed with a 360-degree camera-rig consisting of six 1080p cameras, the film enables users to pan 360-degrees, while taking in the action of each scene as it unfolds in order to solve the murder mystery. 

==Plot==
Dillon Wagner, an amateur drug trafficker, emerges from a coma and tries to figure out how he ended up in a hospital. In addition to his memory loss, he finds out that his closest friend and drug connection, Joey Valeri, was murdered. Not knowing why, or by whom, Wagner desperately tries to piece together the last few days leading up to the murder.

==Cast==
* Chris Morocco as Dillon Wagner
* Neal Channa as Joey Valeri
* Omero Vaquera as Raul
* Erika Robertson as Lara
* Dolla Will as Dolla
* Julian Vinatieri as Lance Martin
* Brandon Matthews as Stack
* Roger Wood as The Neighbor

==Score==
{{tracklist
| extra_column    = Artist
| title1          = The Fog
| extra1          = Original Score (Produced By: TY THA ARTIST)
| length1         = 1:49
| title2          = House Call
| extra2          = Original Score (Produced By: TY THA ARTIST)
| length2         = 5:18
| title3          = Thirst
| extra3          = Original Score (Produced By: TY THA ARTIST)
| length3         = 1:34
| title4          = Meet The Boss
| extra4          = Original Score (Produced By: TY THA ARTIST)
| length4         = 1:19
| title5          = 191
| extra5          = Original Score (Produced By: TY THA ARTIST)
| length5         = 2:06
| title6          = Big Money
| extra6          = Original Score (Produced By: TY THA ARTIST)
| length6         = 3:15
| title7          = All Over The News
| extra7          = Original Score (Produced By: TY THA ARTIST)
| length7         = 4:29
| title8          = The Job
| extra8          = Original Score (Produced By: TY THA ARTIST)
| length8         = 0:43
| title9          = Last Call
| extra9          = Original Score (Produced By: TY THA ARTIST)
| length9         = 1:23
| title10         = One For Me
| extra10         = Lil Ro feat. Monique Martinez (Produced By: TY THA ARTIST)
| length10        = 5:00
| title11         = Sac 2 Tha Bay
| extra11         = TY THA ARTIST  feat. 3rd, Young Saint, J.Gib, D.Willz (Produced By: TY THA ARTIST)
| length11        = 4:37
| title12         = Hate On Me
| extra12         = Lil Ro feat. Young Dru, J-Minix (Produced By: TY THA ARTIST)
| length12        = 3:21
}}

==Screenings==
Pre-screenings of Concealed 360 were conducted on May 19 and May 20 at the Maker Faire 2012 in San Mateo.  The reception was over 65,000 people in attendance as the user interactivity user tested. The final interactive screening was held on June 13 at CSU Eastbay. 

==References==
 

 
 
 
 