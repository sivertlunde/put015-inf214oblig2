Harvest Time
 
{{Infobox film
| name           = Harvest Time
| image          = 
| caption        = 
| director       = Marina Razbezhkina
| producer       = Natalia Zheltukhina
| writer         = Marina Razbezhkina
| starring       = Lyudmila Motornaya
| music          = 
| cinematography = Irina Uralskaya
| editing        = 
| distributor    = 
| released       =  
| runtime        = 67 minutes
| country        = Russia
| language       = Russian
| budget         = 
}}

Harvest Time ( , Transliteration|translit.&nbsp;Vremya zhatvy) is a 2004 Russian drama film directed by Marina Razbezhkina. It was entered into the 26th Moscow International Film Festival.   

==Cast==
* Lyudmila Motornaya as Antonina
* Vyacheslav Batrakov as Gennadiy
* Dmitri Yakovlev as Vanya (as Dima Yakovlev) Dmitri Yermakov as Kolya (as Dima Yermakov)
* Sergei Starostin as Narrator (voice)
* Vika Vasilyeva (voice)
* Sainkho Namtchylak (voice)
* Dmitri Derduga as Vanya (voice) (as Dima Derduga)
* Mikhail Izotov as Kolya (voice) (as Misha Izotov)
* Svetlana Efremova as Singer

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 