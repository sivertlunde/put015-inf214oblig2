Million Dollar Crocodile
{{Infobox film
| name           = Million Dollar Crocodile
| image          = Million-dollar-crocodile.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Lin Lisheng
| producer       = Li Rui
| writer         = Lin Lisheng Ma Hua Ma Yu
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Guo Tao Lam Suet Shi Zhaoqi Xiong Xinxin
| music          = Dong Dongdong
| cinematography = Li Xi
| editing        = Zhou Xinxia Wei Nan
| studio         = 
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = China
| language       = 
| budget         = US$4.7 million   
| gross          = 
}}

Million Dollar Crocodile is a 2012 Chinese monster movie directed by Lin Lisheng. The film stars Barbie Hsu,  Guo Tao, Lam Suet and is about a group of people seeking a crocodile that has swallowed a million yuan. 

The film was released on June 8, 2012 and is Chinas first monster movie. 

==Release==
Million Dollar Crocodile was shown as a "work-in-progress" print at Marché du Film at the Cannes Film Festival in May 2012.    Australian sales firm Odins Eye Entertainment picked up the rights to Million Dollar Crocodile.  The film was released in China on June 8, 2012.  Million Dollar Crocodile will be the opening film at the 36th Montreal World Film Festival. 

==Notes==
 

 
 
 


 