True Love (1989 film)
{{Infobox Film
| name           = True Love
| image          = TrueLoveFilm.jpg
| caption        = DVD cover
| director       = Nancy Savoca Richard Guay Shelley Houis Luke Wienecke Richard Guay
| starring       = Annabella Sciorra Ron Eldard
| music          = 
| cinematography = Lisa Rinzler
| editing        = John Tintori
| distributor    = United Artists
| released       = September 15, 1989
| runtime        = 104 minutes
| country        = United States English Italian Italian
| budget         = $750,000 Gerry Molyneaux, "John Sayles, Renaissance Books, 2000 p 183 
| gross          = $1,354,268 (USA) (sub-total)
}}
True Love is a 1989 American comedy film directed by Nancy Savoca.  An unflinching look at the realities of love and marriage which offers no "happily ever after" ending, it won the Grand Jury Prize at the 1989 Sundance Film Festival.

==Cast==

*Annabella Sciorra as Donna
*Ron Eldard as Michael
*Aida Turturro as Grace
*Roger Rignack as Dom
*Star Jasper as JC
*Michael James Wolfe as Brian
*Kelly Cinnante as Yvonne
*Rick Shapiro as Kevin
*Vincent Pastore as Angelo (Donnas father)

==References==
 

==External links==
* 
*  

 
 
{{succession box
| title= 
| years=1989
| before=Heat and Sunlight
| after=Chameleon Street}}
 

 

 
 
 
 
 
 
 
 
 
 
 
 


 