Dil Bole Hadippa!
 
 
{{Infobox film
| name = Dil Bole Hadippa!
| image = Dilbolehadippa!.jpg
| alt =  
| caption = Theatrical release poster Anurag Singh
| producer = Aditya Chopra
| screenplay = Aparajita
| story = Aparajita
| starring = Shahid Kapoor Rani Mukerji
| music = Pritam
| cinematography = Sudeep Chatterjee
| editing = Ritesh Soni
| studio =
| distributor = Yash Raj Films
| released =  
| runtime = 148 minutes
| country = India
| language = Hindi
}} Anurag Singh and produced by Aditya Chopra under the Yash Raj Films banner. It stars Shahid Kapoor and Rani Mukerji in pivotal roles in the story about a young woman who pretends to be a man to join an all-male cricket team. It also has Anupam Kher, Dalip Tahil, Rakhi Sawant and Sherlyn Chopra in supporting roles.    The film released on 18 September 2009,  and received negative reviews upon release, and was a box office disappointment. It is a remake of the 2006 Hollywood film Shes the Man.

==Plot==
Veera (Rani Mukerji) is a young woman who lives in a small village but dreams of playing cricket in the big league, being extremely talented in the game.  Veera works in a nautanki and dances with the star performer, Shanno (Rakhi Sawant), an arrogant and conceited woman. 

Rohan (Shahid Kapoor) is an accomplished captain of a county cricket team in England. His father, Vicky, and mother, Yamini, are separated. Rohan lives with his mother in England and his father lives in India. Vicky has been captaining the Indian cricket team against the Pakistani cricket team in a tournament called the Aman Cup. Every year, for 8 years, India has lost all matches. To win, Vicky pretends to have a heart attack and asks Rohan to come to India. When Rohan reaches India, he agrees to captain the team for his father, determined to make the team win.
 disguises herself as a man named Veer and is accepted into the team. One day when a glass of juice is thrown in Veeras face, her hair falls, but she hides it and runs to the mens changing room. Rohan looks for "Veer", but finds Veera in the changing room instead. Veera quickly pretends to be Veers sister to cover the disguise. Rohan argues with her but later asks "Veer" to bring him to Veera to apologise. Rohan falls in love with Veera as herself (he does not recognise her as Veer). Rohan and Veera go on a date and Veera falls in love with Rohan as well.

The big day of the match arrives, when India is set to play Pakistan in Lahores Gaddafi Stadium. Rohans mother arrives at the match and she and Rohans father are reunited. In the match, Veer gets the other player out and everyone hugs her in joy. In the excitement, one of Veers brown contact lenses falls out onto Rohans finger and he realises that Veer is Veera. He argues with her about her deception and goes back to play the match without "Veer". Upset, Veer takes off her disguise, changing back to herself. When Rohans team starts losing, Vicky tells Rohan that it isnt important to win or lose any more; he is happy that at least through the match, he and his mother came. But Rohan understands how badly his father wants to win this match, so he gets Veera to come back and play the match.

Veera puts her disguise back on and plays well, but in the closing stages, she is tripped by a Pakistan fielder. Rohan rushes to her aid and the medical unit says that Veeras arm is fractured, but she determinedly continues playing. She shows her talent by switching sides and batting with her left hand. They win the match and Rohan asks "Veer" to show his true identity. Everyone is shocked that Veer is actually a woman and accuses her of being a cheat. Veera then gives a heart-touching speech about women and their talents that become useless because of mens dominance. Everybody realises that talented women should be allowed to play with men in cricket teams and gives her a round of significant applause. Veera sees Rohans love for her and the two are reunited.

==Cast==
* Shahid Kapoor as Rohan Singh
* Rani Mukerji as Veera Kaur/Veer Pratap Singh
* Anupam Kher as Vikram Singh (Vicky)
* Dalip Tahil as Liyaqat Ali Khan (Lucky)
* Rakhi Sawant as Shanno Amritsari
* Sherlyn Chopra as Soniya Saluja
* Vallabh Vyas as Home Minister Parimal Chaturvedi
* Vrajesh Hirjee as Chamkila
* Poonam Dhillon as Yamini Singh (Vikrams Wife)
* Shonali Nagrani as herself

==Production==
 
Filming began on 17 July 2008. Shahid Kapoors portions were shot in February 2009, since he was shooting for Kaminey from June to December 2008. Urmila Matondkar was initially offered to play a cameo role in the film but she eventually opted out of the project. Sanjay Leela Bhansali had already bought rights for the title Hadippa. When Yash Raj Films requested him to give them rights for the title, Bhansali refused. Later, it was announced that the movie title would be Dil Bole Hadippa.

==Box office==
Dil Bole Hadippas domestic net collections were   11.50 crore for its opening weekend.  It went on to collect a net of   23.7 crore, and was declared "Flop" by Box office India. http://www.boxofficeindia.com/showProd.php?itemCat=288&catName=MjAwOQ 

==Soundtrack==
{{Infobox album
| Name = Dil Bole Hadippa
| Type = soundtrack
| Artist = Pritam
| Cover = Dil-bole-hadippa-new-poster-fly-or-flop-14074194404a79f31608cdc1.88565044.jpg
| Released = 12 August 2009
| Recorded =
| Genre = Film soundtrack
| Length = 30:17 YRF Music
| Producer = Pritam
}}
The soundtrack of Dil Bole Hadippa had been composed by Pritam with lyrics provided by Jaideep Sahni. The melody composition for the track "Ishq Hi Hai Rab" was composed by Mukhtar Sahota, a United Kingdom-based Music Director.

{{Track listing
| extra_column = Singers
| title1 = Gym Shim
| extra1 = Joshilay
| length1 = 03:17
| title2 = Bhangra Bistar
| extra2 = Alisha Chinoy, Sunidhi Chauhan, Hard Kaur
| length2 = 04:45
| title3 = Ishq Hi Hai Rab
| extra3 = Sonu Nigam, Shreya Ghoshal, Mukhtar Sahota
| length3 = 05:25
| title4 = Discowale Khisko
| extra4 = Krishnakumar Kunnath|KK, Sunidhi Chauhan, Rana Mazumder
| length4 = 04:26
| title5 = Hadippa
| extra5 = Mika Singh
| length5 = 04:08
| title6 = Discowale Khisko
| note6 = Remix
| extra6 = Master Saleem
| length6 = 04:14
| title7 = Hadippa
| note7 = Remix
| extra7 = Mika Singh, Sunidhi Chauhan
| length7 = 04:17
}}

==Awards==
;V. Shantaram Awards
* Best Heroine – Rani Mukerji 

;Anandalok Purashkar
* Best Actress (Critics Choice) – Rani Mukerji 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 