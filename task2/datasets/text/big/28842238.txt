The Lincoln Lawyer (film)
{{Infobox film
| name = The Lincoln Lawyer
| image = The Lincoln Lawyer Poster.jpg
| alt = 
| caption = Promotional poster
| director = Brad Furman
| producer = Sidney Kimmel Tom Rosenberg Gary Lucchesi Richard Wright Scott Steindorff
| based on =  
| screenplay = John Romano
| starring = Matthew McConaughey Marisa Tomei Ryan Phillippe Josh Lucas John Leguizamo Michael Peña Bob Gunton Frances Fisher Bryan Cranston William H. Macy
| music = Cliff Martinez
| cinematography = Lukas Ettlin
| editing = Jeff McEvoy Lionsgate Lakeshore SKE Entertainment Stone Village Pictures
| distributor = Lionsgate
| released =  
| runtime = 118 minutes
| country = United States
| language = English
| budget = $40 million   
| gross = $85.5 million 
}} novel of the same name by Michael Connelly, starring Matthew McConaughey, Ryan Phillippe, William H. Macy, Bryan Cranston and Marisa Tomei. The film is directed by Brad Furman, with a screenplay written by John Romano.

The story is adapted from the first of several novels featuring lawyer Mickey Haller, who works out of a chauffeur-driven Lincoln Town Car rather than an office. Haller is hired by a wealthy Los Angeles businesswoman to defend her son, who is accused of assault. Details of the crime bring up uncomfortable parallels with a former case, and Haller discovers the two cases are intertwined.

==Plot== biker club led by Eddie Vogel (Trace Adkins). 

A high-profile case comes his way and Haller is hired to represent wealthy Louis Roulet (Ryan Phillippe), a Beverly Hills playboy and son of real estate mogul Mary Windsor (Frances Fisher). Roulet is accused of brutally beating prostitute Regina Campo (Margarita Levieva). Roulet insists he is the innocent victim of a frame-up. Haller and his investigator, Frank Levin (William H. Macy) analyze photos and evidence and find it similar to one of Hallers past cases that resulted in a life-sentence for his client, Jesus Martinez (Michael Peña), for murdering a woman, despite his repeatedly proclaiming his innocence.

Hallers ex-wife, prosecutor Maggie McPherson (Marisa Tomei), has never appreciated Haller representing guilty clients, though they remain close. Haller wonders if he should have done more for Martinez rather than persuading him to plead guilty to avoid the death penalty. Haller visits the prison, where Martinez becomes agitated when Haller shows him Roulets photo. Haller now believes Roulet is the killer in the Martinez case, but, bound by Attorney–client privilege|attorney–client confidentiality rules, is unable to reveal what he knows.

Roulet breaks into Hallers house and nonchalantly admits to committing the murder for which Martinez was convicted. He makes veiled threats toward Hallers ex-wife and their daughter. Levin is found shot to death after leaving Haller a voicemail message claiming that he found Martinezs "ticket out of jail." Levin was shot with a .22 caliber pistol, and Haller discovers that his late fathers .22 Colt Woodsman is missing from its box.

Detective Lankford (Bryan Cranston), who dislikes Haller, discovers the guns registration and suspects Hallers involvement in Levins murder. Haller is certain that Roulet stole the weapon when he broke into Hallers home.

Obliged to do his best for his client, guilty or not, Haller ruthlessly cross-examines Campo and discredits her in the jurys eyes. Haller then sets up a known prison informant with information on the previous murder. When the informant testifies, Haller discredits him, and the states attorney (Josh Lucas) can only move to dismiss all charges. Roulet is set free, to his mothers delight, but the police arrest him immediately for the previous murder, based upon testimony Haller coaxed from the informant.

Haller acquires a Smith & Wesson pistol from his driver, Earl, for protection. Roulet is released due to lack of evidence and intends to kill Hallers ex-wife and daughter. Haller is waiting at Maggies house when Roulet arrives. He mockingly tells Haller that he cannot guard his family all the time. The bikers Haller previously represented suddenly arrive and brutally beat Roulet.

Maggie discovers that Levin had found a parking ticket that was issued to Roulet near the previous murder victims house. It is strong evidence against Roulet in his pending murder trial and will support Martinezs innocence. Upon arriving home, Haller discovers Roulets mother, Mary Windsor, waiting inside. She shoots him with the Colt Woodsman, confessing that she murdered Levin to protect her son. Haller shoots and kills her.

When Haller is released from the hospital, he learns that Martinez has been released, and that the District Attorney will seek the death penalty against Roulet. As Haller rides off to his next case, he is pulled over by Vogel and the biker gang, whose next case he takes pro bono due to their previous help.

==Cast==
 
* Matthew McConaughey as Mickey Haller
* Marisa Tomei as Margaret McPherson
* Ryan Phillippe as Louis Ross Roulet
* Josh Lucas as Ted Minton
* John Leguizamo as Val Valenzuela
* Michael Peña as Jesus Martinez
* Bob Gunton as Cecil Dobbs
* Frances Fisher as Mary Windsor
* Bryan Cranston as Detective Lankford
* William H. Macy as Frank Levin
* Trace Adkins as Eddie Vogel
* Laurence Mason as Earl
* Margarita Levieva as Regina "Reggie" Campo
* Pell James as Lorna Taylor
* Shea Whigham as Dwayne Jeffrey "DJ" Corliss
* Katherine Moennig as Gloria
* Michael Paré as Detective Kurlen
* Michaela Conlin as Detective Heidi Sobel
* Mackenzie Aladjem as Hayley Haller
 

==Reception==
After watching a rough cut of the film on November 12, 2010, Michael Connelly, author of the book The Lincoln Lawyer, said: 
 
 average score of 63, based on 30 reviews, which indicates "generally favorable reviews."  Roger Ebert of the Chicago Sun-Times gave the film 3 stars out of a possible 4, saying, "The plotting seems like half-realized stabs in various directions made familiar by other crime stories. But for what it is, The Lincoln Lawyer is workmanlike, engagingly acted and entertaining."

==Home media==
The film was released on Blu-ray and DVD on July 12, 2011. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 