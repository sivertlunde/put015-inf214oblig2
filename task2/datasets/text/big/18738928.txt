Moggina Manasu
{{Infobox film
| name = Moggina Manasu
| image = MogginaManasu.jpeg
| caption = Film poster Shashank
| writer =
| producer = E. Krishnappa
| screenplay = Shashank Yash Skanda Manoj Harsha
| music = Mano Murthy Shashank
| cinematography = K. S. Chandrasekhar
| editing = Suresh Urs
| released = 18 July 2008
| studio = E. K. Entertainers
| runtime = 149 minutes
| language = Kannada
| country = India
}}
 Shashank and produced by E. Krishnappa under the banner E. K. Entertainers. It stars Radhika Pandit and Shuba Punja in lead roles. The supporting cast features Yash (actor)|Yash, Skanda, Manoj, Sangeetha Shetty, Manasi and Harsha. 

The film was a critical and a commercial success at the box-office having received appreciation from critics and audience alike. The film also completed a 100-day run at a theatre in Bangalore. 

==Plot==
Moggina Manasu focuses on the confused state of mind of teenage girls and their relationships with boys. It also touches on teenage pregnancy and ragging in colleges, camaraderie with seniors, and teenagers disrespect towards their parents and studies. The film tells the stories of four teenage girls and the trials and tribulations they face during their college time.
Chanchala (Radhika Pandit) goes to the college. It was her first day, and she was ragged by her seniors Akka and Didi as Chanchala calls them. Another village girl Renuka Devi (Shubha Poonja) often called as Renu, also comes to the college. She wants to be a Doctor and treat her village people. Later akka, Didi, Renu and Chanchala becomes best friends. Akka and Didi change Renu and Chanchu completely. Renu and Chanchu feels that they need a boyfriend. Chanchu loves her English teacher but later comes to know she should not. Renu gets a boyfriend Akash and chanchu is also persuaded by a person to love her. Later they love Each other. but that man tries to avoid her in her activities. He refers her as public Property which angers her and she leaves him. Later enters Rahul, a singer (Yash) who loves her more than his life. Even she starts loving him. Later Renu gets Pregnant because of Akash. He tells her to abort the child. She doesnt want to. She tells this to Chanchu and Didi who scolds her. Didi tells that all men are cheaters and she lost her sister as her sister was cheated by a man. Chanchu listens to all this and Talks about this with Rahul.
Next day Renu is found Hanging. Renus death changes Chanchus mind. She breaks up with Rahul. After many years she was going back to Mysore where she studied . In the train she remembers all this. She meets Akka whose life was settled. Akka had run away with her boyfriend and marries which had angered her father and her mother died of this. But now her father, her husband and her daughter. Chanchu is Happy to see them together. She goes to the college where she studied with Akka as Old students association function had been held. There she meets Didi who had also married an led a settled life. They remember Renu. Later Chanchu remembers Rahul. Just then she hears Rahuls voice from Auditorium. She hurries there. Later there is a song program by him in memory of her. She thinks of going to him but thinks that he may have forgotten her and married another. But after the song Rahul tells that he had lost his love but now he is getting it back. He tells them that he is going to propose the girl he loves in front of them. He proposes Chanchala in front of everyone. She is astonished and happy. Yashs parents and Chanchus parents also agrees to their marriage as they loved each other even after so many years. They get married.
        
==Cast==
* Radhika Pandit as Chanchala
* Shubha Poonja as Renuka Devi
* Sangeetha Shetty as Sanjana
* Manasi as Deeksha Yash
* Skanda
* Manoj
* Harsha Jai Jagadish
* Avinash

==Soundtrack==
{{Infobox album
| Name        = Moggina Manasu
| Type        = Soundtrack
| Artist      = Mano Murthy
| Cover       = Moggina Manasu audio cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 12 July 2008
| Recorded    =  Feature film soundtrack
| Length      = 47:56 Kannada
| Label       = Anand Audio
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Mano Murthy composed the music for the film and the soundtracks. The album consists of eleven soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 47:56
| lyrics_credits = yes
| title1 = Gari Gari Shashank
| extra1 = Chetan Sosca
| length1 = 4:19
| title2 = Geleya Beku
| lyrics2 = Jayant Kaikini
| extra2 = Chaitra H. G., Priya Himesh
| length2 = 5:01
| title3 = I Love You
| lyrics3 = Shashank
| extra3 = Sonu Nigam
| length3 = 4:37
| title4 = I Love You (Bit)
| lyrics4 = Shashank
| extra4 = Sonu Nigam
| length4 = 1:01
| title5 = Male Baruvahaagide
| lyrics5 = Jayant Kaikini
| extra5 = Shreya Ghoshal
| length5 = 4:43
| title6 = Moggina Manasali
| lyrics6 = Shashank
| extra6 = Shreya Ghoshal
| length6 = 5:09
| title7 = Moggina Manasali (Pathos)
| lyrics7 = Shashank
| extra7 = Vijay Prakash
| length7 = 5:04
| title8 = O Nanna Manave
| lyrics8 = V. Nagendra Prasad
| extra8 = Sonu Nigam
| length8 = 5:16
| title9 = Om Namha Om
| lyrics9 = Sini
| extra9 = Kumar Sanu
| length9 = 4:26
| title10 = Teenage Teenage
| lyrics10 = Shashank
| extra10 = Chaitra H. G., Inchara
| length10 = 3:12
| title11 = Yakhing Hadtharo
| lyrics11 = Shashank
| extra11 = Akanksha Badami
| length11 = 5:08
}}

==Critical reception==
Upon its theatrical release, Moggina Manasu received generally positive response from critics. Sify.com gave the film a rating of three out of five and praised the roles of music, cinematography and the acting departments in the film writing, "The spectacular cinematography and music for this film is good. Each and every frame is taken with utmost care and all the songs are tolerable and soothing to the ears. A new promising heroine Radhika Pandit is the surprise element who is extraordinary in her first film." and further added writing, "Shubha Punja has also done a neat job and the other two girls Sangeetha Shetty and Manasi also fit the bill perfectly. Among the boys, it is Yash who stands out, though the performances of Skanda, Harsha and Manoj is good."  R. G. Vijayasarathy of Rediff.com|Rediff too gave the film a 3/5 rating and wrote, "Great performances from the new talents, superior technical work by cameraman Chandrashekhar and good editing by Suresh Urs, enhance the film." However, he called for "more attention should have been paid to the screenplay." 

== Awards == Filmfare awards Best Film. 
 Best Film
*  
*  
*  
*  

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 