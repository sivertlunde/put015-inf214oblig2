The Harms Case
{{Infobox film
| name           = The Harms Case
| image          = Slučaj Harms.jpg
| image size     = 
| caption        = 
| director       = Slobodan D. Pesic
| producer       = Slobodan Stojicic-Lesi
| writer         = Aleksandar Ciric Brana Crncevic Jovan Markovic Slobodan D. Pesic
| starring       = Mladen Andrejevic
| music          = 
| cinematography = Milos Spasojevic
| editing        = Neva Paskulovic-Habic
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Yugoslavia
| language       = Serbo-Croatian
| budget         = 
}}

The Harms Case ( ) is a 1987 Yugoslavian drama film directed by Slobodan D. Pesic. It was screened in the Un Certain Regard section at the 1988 Cannes Film Festival.     

==Cast==
* Mladen Andrejevic - Zabolocki (as Mladen Andrejević)
* Abdula Azinovic - Pijanac (II)
* Miroslav Bukovcic - Oficir koji puca
* Branko Cvejic - Marija Vasilijevna
* Ivana Despotovic - Ida Markovna (II)
* Ljubomir Didic - Arhivar
* Ljubomir Draskic - Svestenik
* Rahela Ferari - Stara dama
* Erol Kadic - Pijanac (I)
* Milutin Karadzic - Invalid instruktor
* Vojislav Kostic - Majstor
* Aleksandar Kothaj - Crnoberzijanac
* Violeta Kroker - Ida Markovna (I)
* Predrag Lakovic - Harmonikas (as Predrag Laković) Danil Harms
* Damjana Luthar - Andjeo
* Dubravka Markovic - Oficir andjeo
* Ivana Markovic - zena Fedje Davidovica
* Zoran Miljkovic - Inspektor KGB
* Zeljko Nikolic - Fedja Davidovic
* Bogoljub Petrovic - KGB ... agent
* Ljubo Skiljevic - KGB ... agent (II)
* Dijana Sporcic - Zena policajac
* Milica Tomic - Irina Mazer (as Milica Tomić)
* Milivoje Tomic - Kino operator
* Eugen Verber - Pisar
* Olivera Viktorovic - Zena u kafani
* Francisko Zegarac - Danil Harms ... dete
* Branislav Zeremski - Malogradjanin
* Stevo Žigon - Professor

==References==
 

==External links==
* 

 
 
 
 
 
 
 