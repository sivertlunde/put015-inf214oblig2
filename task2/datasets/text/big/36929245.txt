Before Midnight (film)
 
{{Infobox film
| name           = Before Midnight
| image          = Before Midnight poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Richard Linklater
| producer       = Richard Linklater Christos V. Konstantakopoulos Sara Woodhatch
| writer         = Richard Linklater Ethan Hawke Julie Delpy
| based on       =  
| starring       = Ethan Hawke Julie Delpy
| music          = Graham Reynolds
| cinematography = Christos Voudouris
| editing        = Sandra Adair
| studio         = Castle Rock Entertainment Venture Forth
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 109 minutes  
| country        = United States
| language       = English Greek French
| budget         = $3 million   
| gross          = $20,737,030   
}} romantic drama film, the third in a trilogy featuring two characters, following Before Sunrise (1995) and Before Sunset (2004). It was directed by Richard Linklater and stars Ethan Hawke and Julie Delpy. Co-written by Linklater, Hawke and Delpy, the film picks up the story nine years after the events of Before Sunset; Jesse (Hawke) and Céline (Delpy) spend a summer vacation together in Greece.
 limited opening wide in Best Adapted Screenplay. Some elements were drawn from the screenplay for the first film.

==Plot==
Nine years have passed since Before Sunset. Jesse and Céline have become a couple and parents to twin girls, conceived when they were together in Paris during a brief visit. Jesse struggles to maintain his relationship with his teenage son, Hank, who lives in Chicago with Jesses ex-wife. After spending the summer with Jesse and Céline on the Greek Peloponnese peninsula, Hank is being dropped off at the airport to fly home. Jesse has continued to find success as a novelist, while Céline is at a career crossroads, considering a job with the French government.

The couple discuss their concerns about Hank, and then about Célines choices for her career. Over dinner they talk more about love and life. Friends staying with them pay for a hotel room so they can have a night alone.  While walking to the hotel, the couple reminisce about coming together. After reaching the hotel, they have a fierce argument, expressing fears about their present and future together.

Céline leaves their room and sits alone in the hotels outdoor restaurant. Jesse joins her and suggests how things might change from that night. She says their fantasies will never match the imperfect reality. Jesse proclaims his love, saying he does not know what else she could want. Céline resumes Jesses joke and the two seem to reconcile.

==Cast==
* Ethan Hawke as Jesse
* Julie Delpy as Céline
* Seamus Davey-Fitzpatrick as Hank
* Jennifer Prior as Ella
* Charlotte Prior as Nina
* Xenia Kalogeropoulou as Natalia
* Walter Lassally as Patrick
* Ariane Labed as Anna
* Yiannis Papadopoulos as Achilleas
* Athina Rachel Tsangari as Ariadni
* Panos Koronis as Stefanos

==Production==
Richard Linklater, Ethan Hawke, and Julie Delpy had all discussed doing a sequel to Before Sunset (or the third in a trilogy). In November 2011, Hawke said that he, Delpy and Linklater 
 "have been talking a lot in the last six months. All three of us have been having similar feelings, that were kind of ready to revisit those characters. Theres nine years between the first two movies and, if we made the film next summer, it would be nine years again, so we started thinking that would be a good thing to do. So were going to try and write it this year."   

In June 2012, Hawke confirmed that the sequel to Before Sunset would be filmed that summer.  Soon after, Delpy denied filming would take place that year.  But by August 2012, numerous reports emerged from Messenia, Greece, that the film was being shot there.  

The completion of filming the movie, titled Before Midnight, was announced on September 5, 2012.  Linklater said that, after ten weeks of writing and rehearsing, the film was made in fifteen days for less than $3 million.  He intended to take it to a film festival in early 2013. 

==Release== premiered on January 20, 2013, at the 2013 Sundance Film Festival.  It had its international premiere out of competition at the 63rd Berlin International Film Festival.   

The film opened to general audiences on May 24, 2013, at five theaters in New York, Los Angeles, and Austin, Texas.    It was released wide in 897 theaters on June 14, 2013.   

===Box office===
The film grossed $8,110,621 domestically and $12,626,409 overseas, for a worldwide gross of $20,737,030. 

===Critical reception===
Like the first two films of the trilogy, Before Midnight received widespread critical acclaim.   gives the film a score of 94 based on reviews from 41 critics, indicating "universal acclaim". It was listed as the third-best film of the year after 12 Years a Slave and Gravity.  It was the second-best reviewed film of 2013 according to Rotten Tomatoes, after Alfonso Cuaróns Gravity (film)|Gravity. 

According to Total Films Philip Kemp, 
 "As with its two predecessors — and with the films of French New Wave director Éric Rohmer, presiding deity of this kind of cinema—Midnights essentially a film about people talking. But when the talks this good, this absorbing and revealing and witty and true, whos going to complain?" Kemp described it as a "more-than-worthy, expectation-exceeding chapter in one of modern cinemas finest love stories. As honest, convincing, funny, intimate and natural as its predecessors."  

Perry Seibert of AllMovie also praised the film, writing: "The screenwriting trio fill the movie with long, discursive conversations (there are only two scenes in the first 20 minutes) that feel utterly improvised when they are performed, but are far too deftly structured to be anything other than the work of consummate artists."  Eric Kohn, from Indiewire, gave the film rave reviews, adding it to his list of Top 10 Films of 2013. He wrote that "With Before Midnight, Richard Linklater has completed one of the finest movie trilogies of all time." 

==Accolades==
{| class="wikitable plainrowheaders sortable" style="width:95%;"
|- style="background:#ccc; text-align:center;"
! scope="col"| Award / Film Festival
! scope="col"| Date
! scope="col"| Category
! scope="col"| Recipient(s) and nominee(s)
! scope="col"| Result
|-
|-style="border-top:2px solid gray;" Academy Awards  86th Academy March 2, 2014 Academy Award Best Writing (Adapted Screenplay) Richard Linklater, Julie Delpy, Ethan Hawke
| 
|-
|-style="border-top:2px solid gray;" AARP The AARP Annual Movies for Grownups Awards 
| January 6, 2014
| Best Screenwriter
| Richard Linklater (with Julie Delpy and Ethan Hawke)
|  
|-
|-style="border-top:2px solid gray;" Austin Film Austin Film Critics Association Awards     December 17, 2013 Best Film
|Before Midnight
| 
|- Best Austin Film Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Bodil Awards  67th Bodil February 1, 2014 Best US Feature Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Boston Online Film Critics Association  2013 Boston December 8, 2013 Ten Best Films of the Year
|Before Midnight
| 
|- Best Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Chicago Film Chicago Film Critics Association Awards   December 16, 2013
|  Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;"
! scope="row" rowspan=2|Critics Choice Awards   19th Critics January 16, 2014 Broadcast Film Best Adapted Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|- Louis XIII Genius Award Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Denver Film Critics Society   2013 Denver January 13, 2014 Best Adapted Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Detroit Film Critics Society  December 13, 2013 Detroit Film Best Film
|Before Midnight
| 
|- Best Actress Julie Delpy
| 
|- Best Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Golden Globe Awards    71st Golden January 12, 2014 Golden Globe Best Actress – Motion Picture Musical or Comedy Julie Delpy
| 
|-
|-style="border-top:2px solid gray;" Gotham Awards  Gotham Independent December 2, 2013 Best Film
|Before Midnight
| 
|-
|-style="border-top:2px solid gray;" Hollywood Film Festival  17th Hollywood October 17, 2013
|Screenwriter(s) of the Year Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Houston Film Houston Film Critics Society Awards  December 15, 2013 Best Picture
|Before Midnight
| 
|- Best Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Indiana Film Critics Association  December 16, 2013 Best Film
|Before Midnight
| 
|- Best Adapted Screenplay
| Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Independent Spirit Awards   29th Independent March 1, 2014 Independent Spirit Best Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|- Independent Spirit Best Female Lead Julie Delpy
| 
|-
|-style="border-top:2px solid gray;" Los Angeles Film Critics Association  Los Angeles December 8, 2013 Los Angeles Best Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" National Society of Film Critics  January 4, 2014 Best Actress Julie Delpy
| 
|- Best Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" New York Film Critics Online  December 8, 2013 Best Picture
|Before Midnight
| 
|- Best Films of 2013
|Before Midnight
| 
|-
|-style="border-top:2px solid gray;" Online Film Critics Society Awards  Online Film December 16, 2013 Online Film Best Film
|Before Midnight
| 
|- Online Film Best Actress Julie Delpy
| 
|- Online Film Best Adapted Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" San Diego Film Critics Society  December 11, 2013 Best Adapted Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" San Francisco Film Critics Circle  December 13, 2013 Best Adapted Screenplay
| Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Satellite Awards     18th Satellite February 23, 2014 Satellite Award Best Adapted Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;"
! scope="row" rowspan=2|St. Louis Gateway Film Critics Association Awards   December 16, 2013 Best Adapted Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|- Best Art-House or Festival Film
|Before Midnight
| 
|-
|-style="border-top:2px solid gray;" Toronto Film Critics Association   December 16, 2013 Toronto Film Best Adapted Screenplay
| Julie Delpy, Ethan Hawke and Richard Linklater
| 
|- Toronto Film Best Actress Julie Delpy
| 
|-
|-style="border-top:2px solid gray;" Washington D.C. Area Film Critics Association Awards  Washington D.C. December 9, 2013 Best Adapted Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|-
|-style="border-top:2px solid gray;" Women Film Critics Circle  December 16, 2013 Screenwriting Award Julie Delpy
| 
|- Best Equality of Sexes
|Before Midnight
| 
|- Best Screen Couple Julie Delpy and Ethan Hawke
| 
|-
|-style="border-top:2px solid gray;" Writers Guild of America Award  Writers Guild February 1, 2014 Writers Guild of America Award for Best Adapted Screenplay Julie Delpy, Ethan Hawke and Richard Linklater
| 
|}
    Each date is linked to the article about the awards held that year.

===Top ten lists===
According to Metacritic, the film appeared on the following critics top 10 lists of 2013. 
 
* 1st – James Berardinelli, ReelViews
* 1st – The A.V. Club
* 1st – Chris Nashawaty, Entertainment Weekly
* 1st – Stephen Holden, The New York Times
* 1st – Justin Chang, Variety (magazine)|Variety   
* 2nd – A.A. Dowd, The A.V. Club   
* 2nd – Nick Schager, The A.V. Club 
* 2nd – Total Films 50 Best Movies of 2013   
* 2nd – E! 
* 3rd – Lisa Schwarzbaum, BBC 
* 3rd – 3 News 
* 3rd – Owen Gleiberman, Entertainment Weekly
* 3rd – Eric Kohn, Indiewire
* 3rd – Film Comments 50 Best Films of 2013   
* 4th – Christopher Rosen & Mike Ryan, The Huffington Post Rolling Stone
* 4th – Kristopher Tapleys, HitFix 
* 5th – Robert Gifford, The Diamondback   
* 6th – Sam Adams, The A.V. Club 
* 6th – Marlow Stern, The Daily Beast   
* 6th - Lukas Krycek, Comedian/Film Critic  
* 7th – Stephanie Zacharek, The Village Voice   
* 7th – Digital Spy 
* 8th – Empire (film magazine)|Empire
* 8th – Scott MacDonald, The A.V. Club 
* 8th – Chris Vognar, The Dallas Morning News   
* 11th – Ben Kenigsberg, The A.V. Club 
* 12th – Glenn Kennys 30 Top Films of 2013   
* 14th – Ignatiy Vishnevetsky, The A.V. Club 
* In alphabetical order – Manohla Dargis, The New York Times
* In alphabetical order – Dana Stevens, Slate (magazine)|Slate
* Best films of 2013 – Peter Bradshaw, The Guardian   
* Best movies of the year – David Denby, The New Yorker 
* Best movies of 2013 – The Week 
 
The A.V. Club film critics named "The fight" scene the Scene of the Year. 

==References==
 

==External links==
 
*  
*  , NPR, 19 May 2013
*   at Sony Pictures Classics
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 