Short Cut to Nirvana: Kumbh Mela
{{Infobox film
| name           = Short Cut to Nirvana: Kumbh Mela
| image          =
| caption        =  Nick Day Nick Day
| starring       =  Jasper Johal Justin Davis Swami Krishnanad
| music          = Bob Muller
| Production Company    = Mela Films LLC
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English Hindi
}} 2004 feature Nick Day and Maurizio Benazzo about the 2001 Maha Kumbh Mela at Allahabad. The documentary premiered in the USA on May 11, 2004.  The film won several awards on the festival circuit and played in theaters across the US and Europe.

==Kumbh Mela: Background==

The   (Prayag), Haridwar, Ujjain and Nasik. The 2001 event at Allahabad was also known as the Maha (great) Kumbh Mela, occurring only once every 12 Kumbh Melas, or every 144 years. The Kumbh Mela is one of the largest human gatherings in history, with an estimated 70 million people attending the 2001 event    

==Plot==
Short Cut to Nirvana Looks at the Kumbh Mela from the point of view of four visitors from the US and Canada, each on their first visit to the event, although one, Los Angeles-based photographer Jasper Johal, is himself an Indian returning for the first time in 23 years. Two of the visitors, Dyan Summers and Justin Davis, befriend a likable young Hindu monk Swami Krishnanand, who acts as their guide and translator, and appears in much of the film. The structure of the film is episodic with an underlying theme that builds, with visits to various elaborate camps set up by gurus and yogis, some of whom have unusual and extreme practices, such as keeping on arm raised for many years or sitting on a throne of nails over a flaming pit. The filmmakers interview several of these spiritual teachers, and these interviews are often interconnected with visual and musical interludes that illustrate the diverse activities taking place at the event. These include ritual bathing, dancing and theatre, head shaving, fire rituals, and other devotional activities. The 14th Dalai Lama, Tenzin Gyatso, also appears in the film as an official guest of Hindu leaders seeking to harmonize relations between Hinduism and Buddhism. The film’s closing scene is the highlight of the Kumbh Mela, known as Mauni Amavasya, the new moon bathing day, which is considered the most auspicious time to take a holy dip at the sangam – the confluence of the Yamuna, Ganges and mythical Saraswati rivers. An estimated 25 million pilgrims enter the sangam during this 24-hour period, which is believed to cleanse their karma for many lifetimes.

==Release and reception==

Short Cut to Nirvana premiered in the US in May, 2004 and was on release for over one year. The film also received a theatrical release in Germany, with the premiere in Hamburg in April, 2006. The film was released by Zeitgeist Films in October 2005.

The film was generally well received by the critics and scored 75% fresh by online movie review aggregator Rotten Tomatoes’ Top Reviewers.  

The Los Angeles Timess Kevin Crust wrote "If the film offers any lesson, it is that nirvana is not easily attainable, so there really are no shortcuts." Carla Meyer of the San Francisco Chronicle wrote "A snapshot of the festival, one that radiates good cheer and offers moments of true, godly goodness". {{cite web |url=http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2004/12/10/DDGULA8QL61.DTL |title=
A gathering of gurus and true believers |author=Carla Meyer |date=2012-10-04 |work=sfgate.com |publisher=San Francisco Chronicle |accessdate=2012-05-05}}  

"Short Cut to Nirvana is a beautifully crafted documentary that details the organized chaos and curious piety surrounding the Kumbh Mela, an Indian religious festival held on 12-year intervals for the past two millennia".  

The New York Timess A.O. Scott wrote "Given the events size and complexity, it is perhaps inevitable that this documentary feels haphazard and superficial, more tourists photo album than analysis. Still, the glimpses it offers are never less than fascinating".  

==See also==
* Kumbh Mela

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 