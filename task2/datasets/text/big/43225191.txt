Junction (film)
{{Infobox film
 | name           = Junction
 | image          = 
 | caption        = Theatrical release poster
 | director       = Tony Glazer
 | producer       = Steven Saxton  Summer Crockett Moore   Tony Glazer  Roy McDonald  Pat Patterson
| writer         = Tony Glazer
 | starring       = David Zayas Michael OKeefe Anthony Rapp Anthony Ruivivar Neal Bledsoe  Summer Crockett Moore  Tom Pelphrey  Harris Doran Sharon Maguire 
 | music          = Austin Wintory
 | cinematography = Adrian Correia
 | editing        = Phyllis Housen
 | distributor    = Grand Entertainment Group
 | released       =  
 | runtime        = 91 minutes
 | country        = United States
 | language       = English
}} thriller film written and directed by Tony Glazer. The film stars Neal Bledsoe, Summer Crockett Moore, Tom Pelphrey and Harris Doran with David Zayas, Michael OKeefe and Anthony Ruivivar in supporting roles.

Desperate to score drugs, four strung out meth-addicts - Donald (Bledsoe), Kari (Moore), David (Pelphrey) and Spot (Doran) – break into an upscale suburban home looking for a flat screen television that was requested by their dealer (Ruivivar). During the burglary, Spot and Donald discover a dark secret about the homeowners, which pits the four meth-addicts against each other and the police (Zayas and O’Keefe).

The film was released in the United States on July 14, 2012.

==Plot==
Four strung out meth-addicts - Donald (Bledsoe), Kari (Moore), David (Pelphrey) and Spot (Doran) – are desperately craving for their next fix of meth. Short on cash, the group discusses a variety of options including sending Kari to exploit their dealer’s fondness for her. After some debate, the group decides to sends David, their leader and designated driver, to meet with their longtime dealer Tai (Ruivivar). Fortunately, Tai needs a television as a birthday gift for his mother. Desperate, David agrees to take up Tai on his offer of drugs in exchange for a new flat screen television for his mom.

Searching for a flat screen television to rob, the group drives to the upscale neighborhood of Verterra Hill where they expect to readily find one in the well furnished townhouses.  The group spots and breaks into a seemingly empty house. Quickly, the group finds the sought flat screen television and puts it in their car’s back seat. While ransacking for more valuables, Donald discovers a stash of child pornography DVDs as well as photos of a couple and their young daughter, whom are presumed to be the house residents.  
Donald, a victim of pedophilia, goes mad upon the discovery and has a fight with Spot over it. Donald tries to persuade the group to turn over the stash to the authorities. When the home owner - Connor (Anthony Rapp) – unexpectedly returns, Donald viciously attacks him and ties him up. Entering soon after, Connor’s wife – Jennifer (Maguire) and daughter Mia (Kotch) are also bound and gagged.  When the group is distracted by Donald torturing Connor, Mia sneaks away to make a 911 call.

What started as a simple house burglary turns into a hostage situation with the arrival of the police, who are led by hostage negotiator Lt. Tarelli (Zayas) and his colleague - Walters (O’Keefe). Meanwhile, Tai calls Donald seeking an update on his flat screen TV errand. Upon learning about the hostage situation, Tai tells Donald to not call him until he has escaped from the authorities. After hanging up, Tai disposes of his burner cell phone in a nearby lake. Internal tensions within both groups fuel to the chaotic situation:  Lt. Tarelli’s past role in a failed hostage rescue and Donald’s insistence that the parents be exposed as pedophiles. During the hostage negotiations, Donald demands that a television crew be sent to the house to interview him. Donald plans to expose the parents as pedophiles. Taking advantage of this, Lt. Tarelli instead sends police officers posing as a television film crew to mount a hostage rescue.

During the ensuing hostage rescue chaos, Kari is killed (knife to throat) and Connor is shot by the SWAT team. Meanwhile, David and Spot are captured and arrested. After hostage rescue, Jennifer and Mia are debriefed and sent home. At the command center/precinct house, Walters commends Lt. Tarelli on the successful hostage rescue. Upon returning home, Jennifer listens to her phone’s answering machine messages including one left by her late husband Connor. In the message, Connor is angry at discovering the stash of child pornography. He threatens to turn over the evidence to the police and reveal that Jennifer is pedophile. After the message ends, Jennifer deletes the recording and resumes cleaning the house. As the movie ends, and before the credits roll, Tai is shown breaking into a police impound lot to recover “his” flat screen TV that was left in the four meth addicts’ car.

==Cast==
 
* David Zayas as Lt. Tarelli
* Michael O’Keefe as Walters
* Anthony Rapp as Connor
* Anthony Ruivivar as Tai
* Neal Bledsoe as Donald
* Summer Crockett Moore as Kari
* Tom Pelphrey as David
* Harris Doran as Spot
* Sharon Maguire as Jennifer
* Danielle Kotch as Mia
* Joanna Bayless as Mrs. Pendelson
* Peter Aguero as Thug
* Laurence Blum as Patrol Officer Laurant
* Matt De Matt as Ross
* Bryan Deehring as Martin Bateman
* Pat Patterson as Patrice
* Derek Roche as Thomas
* Yvonne Jung as Reporter Wood
* Kaipo Schwab as Reporter
* Maria Arce as SWAT Team Member #2
* James Adames as Police Officer
 

==Production==
Principal photography took place in November and December 2010 in Fort Lee, New Jersey.

==Reception==

===Critical Response===
The film was acclaimed by critics.

Jeannette Catsoulis, in The New York Times, praised the movie as "a cleverly constructed hostage thriller cum morality play".  Frank Scheck, The Hollywood Reporter, called Junction "A quietly effective thriller with a few clever narrative tricks up its sleeve."   Scheck complimented Director Tony Glazer by stating Junction "overcomes its low-budget limitations with clever variations on its familiar genre as well as a taut execution". Catsoulis applauded Tony Glazers directing "Executing a tonal shift that might challenge a more seasoned director, Mr. Glazer provides carefully choreographed action and violence that’s both credible and divorced from sadism".

Critics universally highlighted the casts solid performances. Catsoulis said the cast delivered "solid acting (there isn’t a weak performance in the bunch)". Echoing this, Scheck commented that "strong performances from a cast that includes such familiar faces" played major role in Junction.

==Release==
Junction was released on July 14, 2012

===Awards and nominations===
;Awards
* 2012 Fort Lauderdale International Film Festival – Spirit of the Independent Award For Feature Film
* 2012 Long Island International Film Expo – Best First Feature
* 2012 Long Island International Film Expo – Best Screenplay for Tony Glazer
* 2012 Long Island International Film Expo – Best Actor for Neal Bledsoe
* 2012 Long Island International Film Expo – Best Supporting Actor for Harris Doran
* 2012 Long Island International Film Expo – Best Supporting Actress for Summer Crockett Moore
* 2012 Long Island International Film Expo – Best Art Direction
* 2013 SoHo International Film Festival – Best Feature Film - Jury Prize
* 2013 ReelHeART International Film Festival – Best Ensemble Cast

;Nominations
* 2012 Austin Film Festival – Best Narrative Feature 
* 2012 Central Florida Film Festival – Best Feature Film 
* 2014 Prism Awards – Feature Film - Substance Use

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 