Paanch Fauladi
{{Infobox film
| name           = Paanch Fauladi
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Mohan Bhakri	
| producer       = Neelu Bhakri Inderjeet Walia	
| writer         = 
| screenplay     = Ranbir Pushp	
| story          = 
| based on       =  
| narrator       =  Javed Khan Dara Singh
| music          = Uttam Singh|Uttam-Jagdish
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Mohan Bhakri. It stars Raj Babbar, Javed Khan, Dara Singh and Amjad Khan in pivotal roles.

==Cast==
* Raj Babbar as Raja (Fauladi #2) Javed Khan as Fauladi #4
* Amjad Khan as Delaware Khan (Fauladi #5)
* Salma Agha as Julie
* Anita Raj as Annu
* Ram Mohan as Zamindar Shamsher Singh
* Bob Christo as Seth (Englishman)
* Huma Khan as Jarawar Singhs mistress
* Seema Deo as Parvati

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jara Chal Sambhal Ke Aanchal Ko"
| Suresh Wadkar, Lata Mangeshkar
|-
| 2
| "Koi Kahe Jani Mujhe"
| Suresh Wadkar, Lata Mangeshkar
|-
| 3
| "Jis Ladki Se Tumko"
| Suresh Wadkar, Lata Mangeshkar
|-
| 4
| "Aaj Behna Ki Shaadi Hai"
| Suresh Wadkar, Lata Mangeshkar
|-
| 5
| "Aaj Behna Ki Shaadi Hai (Sad)"
| Suresh Wadkar, Lata Mangeshkar
|}
==External links==
* 

 
 
 

 