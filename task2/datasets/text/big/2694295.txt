Duelist (film)
{{Infobox film
| name           = Duelist
| image          = Duelist film poster.jpg
| caption        = Original Korean poster
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Hyeongsa
 | mr             = Hyǒngsa}}
| director       = Lee Myung-se
| producer       = Lee Myung-se Yoo Jeong-hee
| based on       =  
| writer         = Lee Myung-se Lee Hae-kyeong
| starring       = Ha Ji-won Kang Dong-won
| music          = Jo Seong-woo
| editing        = Ko Im-pyo
| cinematography = Hwang Ki-seok
| studio         = Production M
| distributor    = Korea Pictures
| released       =  
| runtime        = 111 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}}
Duelist ( ; lit. "Detective") is a 2005 South Korean martial arts film directed by Lee Myung-se.       

== Plot ==
The movie opens with a fish tale narrated by a low-class metalsmith in a tavern in Joseon-era south-western Korea. The scene then cuts to a street circus, in which an elegant masked swordmaster (Kang Dong-won) fascinates his market-place audience. Undercover detective Ahn (Ahn Sung-ki), and his protégé Namsoon (Ha Ji-won) are tracking down suspected money-counterfeiter gang, when the masked swordmaster ends his show by killing a government official who carries the kingdoms currency metal cast.

The swordmaster escapes when a cart crashes and disgorges a mountain of counterfeit coins, causing public commotion. The distraction is a success, but Namsoon chased and dueled with the escaping swordmaster/duelist, proving herself a master of martial arts specializing in a pair of long knives. She succeeded in cutting a quarter of his mask and glanced at his revealed eye before he escaped. She and her team are left with bodies of the counterfeiting gang, seemingly massacred in an instant by an unknown duelist.

It is then revealed that counterfeit coins are spreading wildly among the populace, causing hyperinflation that threatens the monarchy. The police forces are determined to find and arrest the counterfeiters, believed to include an insider in the government. From a secret connection, the detectives obtain a picture of the duelist, noted for his "Sad Eyes", which Namsoon recognizes from her previous encounter. Their suspicion points to Song, the powerful defense minister. This suspicion is confirmed when Namsoon spots the duelist entering Songs manor.

Namsoon and Ahn failed to chase the duelist. However, as Namsoon is walking alone in the night, Sad Eyes appeared from the shadows, asking if she is following him "because you like me?" Namsoon attacks, entering a duel that starts to become affectionate. Eventually, he vanishes after making a small cut on her clothing, revealing her cleavage. The resolute Namsoon persuaded her team to infiltrate minister Songs palace. This plan is implemented, resulting with only Namsoon succeeding in her thinly-disguised appearance as a lady of pleasure. Coincidentally, she ends up having to serve the duelist in private, which she handles very tactlessly.

Song called the duelist, interrupting Namsoons session with her. Song turns out to be a superb swordsman, the only person who can outperform the duelist. Their conversation reveals that Song plans to overthrow the reigning young king and replace him with his future son-in-law, and that the duelist has been involved in this plan since childhood. Song reminds Sad Eyes that the duelist can kill him anytime he distrusts him, hinting that the duelist might be the planned royal successor. Song says that he always loved the duelist like a son.

After assaulting Sad Eyes in public, Namsoon is dismissed from the case by her director, only to be secretly assigned to find the final evidence to prove Songs involvement. Overhearing Namsoons objectives, the duelist gave the needed evidence in a secret meeting with her, thereby sacrificing his destiny. Namsoon realizes her feelings for Sad Eyes. The father-figure Ahn recognizes this, advising her to forgo her feelings since he is a criminal while she is with the police. The movie draws towards a climax as scores of armed police surround Songs manor and attack the duelist and Song, as well as the other corrupt officials. In their last conversation, the minister tried to speak the nameless duelists true name, but the police separate them and detective Ahn kills the minister.

After the battle, Namsoon anxiously searches for the duelists fate, only to be told by Ahn that "he is dead". Devastated, she returns to the lanes where she had encountered the duelist, haunted by his images. While she is walking back, the duelist appears once more in front of her. Namsoon feels that finally she can express her feelings, and in a denouement the couple dances in a picturesque combat under the moonlight. They were fighting "like they were making love", but then "disappeared suddenly", as narrated by the metalsmith in the tavern from the first scene.

The final scene shows Namsoon and Sad Eyes spotting each other from a distance in the marketplace.

== Cast ==
* Ha Ji-won as Detective Namsoon  
* Kang Dong-won as Sad Eyes
* Ahn Sung-ki as Detective Ahn
* Song Young-chang as Minister Song Pil-joon
* Yoon Joo-sang as Bong-chool
* Do Yong-goo
* Shim Cheol-jong
* Bae Joong-sik as Detective Bae
* Hwang Eui-doo as Detective Hwang
* Jang In-bo as Detective Jang

== Characterization ==
 
Although set in a historical era, the movie uses a very modern style of storytelling. Although the film is rich in historic details, it contains many intentional historical inaccuracies as well as modern cultural elements.

The movie is nevertheless impressionistic, with extensive use of slow motion, freeze frames, multiple images, and varying perspectives. The film is shot in brilliant, saturated hues, with strong reds perhaps predominating. Black, white, blue and a smaller amount of yellow are all present in concentrated form. The scenes are cut, often abruptly, and multiple shots are often used in the same screen.

The movie uses mainly modern Western music with Korean elements. The opening scene uses a theme from the generic circus music Entrance of the Gladiators. The romantic leitmotif is played with piano, accordion, and violin during the middle scenes and electric guitar in the denouement. A version of Dies Irae is used. Even the undercover scene use a modern tango, drummings, and psychedelic rock typical of action/detective movies in modern urban setting. 

Although the movie is about a duelist, its sword-fight scenes are highly stylized and choreographed more as dance than conflict.  Little blood is shown in spite of the obvious killings in some scenes.  There are long stretches in the movie where there is no talking—only action, suspense, dance, or humor. Sometimes the characters lines of thoughts are spoken while they are not speaking. Much of the movie involves the duelist and the female protagonist in stylized combat, evocative of violent mating rituals of some animals. At times, they come within inches of a kiss, but it seems their lips never touch. 

The story also shows some element of polarity. Both sides have a father-figure and a protégé, all of which are masters of martial arts. Detective Ahn and Namsoon continuously exhibit silly, uncultivated behaviours, while minister Song and duelist Sad Eyes are sophisticated aristocrats. Namsoons interactions with Ahn are comical and crude, while Sad Eyes and Song speak in a very refined language, often communicating without speaking. The lovers also show contrast, with Namsoon being boisterous and overacting, while Sad Eyes is very somber and silent.

While the movie is a love story involving an outlaw and a woman on the side of the police, humor is continually brought in. Namsoons encounters with Sad Eyes proved her to be just about his match in combat, but not in matters of love. The duelist is always cool and calm, whereas her love for him, abetted by her raucous character, makes her a mouse that his cat-like character and moves can toy with. Thus, even the polices plan for her to go undercover in the ministers palace go humorously awry, as she literally tumbles for him.

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  
*   at Koreanfilm.org

 

 
 
 
 
 
 
 
 
 
 
 
 