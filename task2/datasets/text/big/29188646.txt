Girls' Night
{{Infobox film
| name     = Girls Night
| image    = Girls Night.jpg
| caption  = Film poster
| director = Nick Hurran
| producer = Bill Boyes
| writer   = Kay Mellor
| starring = Julie Walters Brenda Blethyn
| cinematography = David Odd   
| editing  = John Richards
| distributor = Granada Productions Showtime Entertainment
| released =  
| runtime  = 102 minutes
| country  = United Kingdom
| language = English
| budget   = $4.5 million
}} Las Vegas, Nevada after an unexpected jackpot win on the Bingo (Commonwealth)|bingo.   

Premiered to a mixed response by critics at the 1998 Sundance Film Festival, who noted it a "rather formulaic tearjerker   two powerhouse Brit actresses",  Hurran won a Silver Spire at the San Francisco International Film Festival  and received a Golden Bear nomination at the 48th Berlin International Film Festival for his work.   

==Story==
Dawn and Jackie, two workers in an electronics factory, have been best friends since childhood and share practically everything. So it is no surprise to anybody when Dawn shares her big bingo win with Jackie. Eventually Dawn discovers the results of her tests and knows for sure that she has terminal cancer. Dawn has always dreamed of going to Las Vegas so, on impulse, Jackie buys the tickets and off they go on one last great adventure together. In Las Vegas they meet the enigmatic Cody who is attracted to Jackie but immediately understands the situation and helps the two friends to enjoy their dream holiday in Las Vegas by taking them on a trip into the desert. The two friends return home for Dawn to make her peace with her friends and family before her ultimate demise. Dawns death, even though expected, devastates Jackie leaving her friendless and alone, or does it? 

==Cast==
*Julie Walters — Jackie Simpson
*Brenda Blethyn — Dawn Wilkinson
*Kris Kristofferson — Cody Philip Jackson — Dave Simpson
*George Costigan — Steve Wilkinson Anthony Lewis — Mathew Wilkinson
*Maxine Peake — Sharon
*James Gaddas — Paul
*Judith Barker — Helen
*Sue Cleaver — Rita
*Meera Syal — Carmen
*Sophie Stanton — Jane
*Fine Time Fontayne — Ken
*Brent Huff — Bobby Joe
*Nigel Whitmey — Tyrone
*Kathryn Hunt — nurse

==References==
 

==External links==
* 

 

 
 
 
 
 
 