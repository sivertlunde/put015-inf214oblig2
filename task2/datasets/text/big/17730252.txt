Swapnakoodu
 
{{Infobox film name           = Swapnakkoodu image          = Swapnakoodu.jpg caption        = Movie poster writer         = Iqbal_Kuttippuram|Dr. Iqbal Kuttippuram Kamal starring  Prithviraj  Bhavana
|director Kamal
|producer       = P. Rajan banner         = Vaisakha Movies cinematography = P. Sukumar music          = Mohan Sithara editing        = K. Rajagopal distributor    = Lal Release & PJ Entertainments UK runtime        = 160 min released       = 15 September 2003 budget         = country        = India language       = Malayalam
}} Malayalam film Bhavana in the lead roles. The films score and soundtrack were composed by Mohan Sithara. The  film was commercially successful and the songs in the film were shot in Austria.

==Plot==
There are three hotel management students: Deepu (Kunchacko Boban), a calm and cool guy who is never able to express his true feelings; Kunjoonju (Prithviraj Sukumaran), a swaggering Casanova who makes a pass at every girl he meets; and Krishnaamoorthy (Jayasurya), a playful, jolly fellow who flips over every second girl who crosses his path.

They start living in a house owned by Kamalas (Meera Jasmine) family, which includes her mother, Sophia (Kalaranjini) and sister Padma (Bhavana Menon). Deepu and Kunjoonju both fall in love with Kamala but do not tell each other. Kamala has feelings for Kunjoonju . Later, Kunjoonju gets to know that Deepu likes her and tells Kamala that he was not serious about her and he is breaking up with her as he is bored of it. Kamalas mother is no more at that time and in some time Padma also dies in a freak accident. The bachelors studies are over and they have to return to Kerala. They are upset that they have to leave Kamala alone. Kunjoonju asks Deepu to take her with him, but Deepu is adamant that he will not. Finally, Kunjoonju says that he is going to take her as he cannot leave her alone. At that point Deepu admits that he was aware of their relation and he was waiting for Kunjoonju to disclose that. They go back and take Kamala with them.

==Cast==
*Kunchacko Boban.... Deepu
*Prithviraj Sukumaran.... Alex Chandy a.k.a. Kunjoonju
*Jayasurya.... Krishnamoorthy
*Meera Jasmine.... Kamala
*Bhavana Menon|Bhavana.... Padma
*Vijeesh.... Abbas
*Cochin Haneefa ... Philippose
*Kalaranjini ... Sophia Manya ..... Kurjeet (Cameo appearance) Laila ..... Cameo appearance

==Soundtrack== composed by Mohan Sithara, with lyrics by Kaithapram.
 Jyotsna  Sujatha
* "Malarkili" - Madhu Balakrishnan, Pradeep Babu 
* "Ishtamallada" - Afsal (singer)|Afsal, Chitra Iyer
* "Marakam Ellam" - Vidhu Prathap
* "Maya Sandhye" - Yesudas,Jyotsna

==Box office==
The movie was a superhit at the box office. 

==References==
*  
*  

==External links==
 

 

 
 
 