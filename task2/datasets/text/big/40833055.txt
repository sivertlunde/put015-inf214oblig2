Plastic (2011 film)
 
{{Infobox film
| name           = Plastic
| director       = Jose Carlos Gomez
| writer         = Jose Carlos Gomez
| producer       = Laura Locascio
| starring       = North Roberts Colleen Boag Matthew Prochazka Christian Gray
| music          = DC McAuliffe
| editing        = Jose Carlos Gomez
| cinematography = Jose Carlos Gomez
| distributor    = R Squared Films
| released       = 2011      
| budget = $50,000 
| runtime        = 80 minutes
| country        = United States
| language       = English
}}
Plastic is a 2011 horror crime film.  It was written and directed by Jose Carlos Gomez and stars North Roberts, Colleen Boag, and Matthew Prochazka. The film was mostly shot in Villa Park, Illinois. Plastic has won several awards, and is now available on DVD and Video on Demand.

==Plot==
The story centers on serial killer Albert Mullin, and his final killing spree. 

==Production==
Filmed mostly in Villa Park, Plastic was shot in fourteen days. Other locations included Chicago, Geneva, Illinois, and Elgin, Illinois. 

==Festivals and Awards==
-Indie Horror Film Festival, 2012; Best Supporting Actor (Christian Gray), and Directors Choice Award (Jose Carlos Gomez).

-Park City Film Music Festival, 2012; Official Selection.

-Fright Night Film Festival, 2012; Official Selection.

-Columbia Gorge International Film Festival, 2012; Official Selection.

-The Prairie State Film Festival, 2012; Best Feature, and Directors Choice Award. 

-Action on Film International Festival, 2012; Official Selection.

-The Chicago Horror Film Festival, 2012; Seven nominations. Won Best Actor (North Roberts).

-Twin Rivers Media Festival, 2013; Official Selection.

==Reception==
Plastic achieved positive reviews from critics. Blood Sucking Geeks review noted, "Plastic creates tension buildup while involving the viewer in the individual characters, and then brings it to a smart, solid close." 

== References ==
 

==External links==
* 

 
 
 


 