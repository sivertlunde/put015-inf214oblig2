Nevada City (1941 film)
{{Infobox film
| name           = Nevada City
| image_size     =
| image	=	Nevada City FilmPoster.jpeg
| caption        =
| director       = Joseph Kane
| producer       = Joseph Kane (associate producer)
| writer         = James R. Webb (original screenplay)
| narrator       =
| starring       = Roy Rogers
| music          = William Nobles
| editing        = Lester Orlebeck
| distributor    =
| released       = 20 June 1941
| runtime        = 58 minutes (original release) 54 minutes (edited version)
| country        = United States English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1941 United American film directed by Joseph Kane starring Roy Rogers.

== Cast ==
*Roy Rogers as Jeff Connors
*George "Gabby" Hayes as "Gabby" Chapman
*Sally Payne as Jo Morrison
*George Cleveland as Hank Liddell
*Billy Lee as Chick Morrison
*Joseph Crehan as Mark Benton
*Fred Kohler Jr. as Jim Trevor / Black Bart
*Pierre Watkin as Amos Norton Jack Ingram as Sheriff Pat Daley

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 