Ponvayal
{{Infobox film|
| name = Ponvayal பொன் வயல்
| image = Pon Vayal.jpg
| caption  = Promotional Poster
| director = A. T. Krishnaswami
| writer =  A. T. Krishnaswami
| starring =  
| producer = A. T. Krishnaswami
| music = Thurayur Rajgopal Sarma R. Rajagopal
| distributor =
| cinematography = 
| editing = 
| released =  1954 
| runtime =  
| country = India Tamil
| budget =
}}

Ponvayal ( ;  ) was a 1954 Tamil film produced and directed by A. T. Krishnaswamy, who also wrote the screenplay and dialogues. It stars T. R. Ramachandran, Anjali Devi and R. S. Manohar in the lead roles, while other actors like K. A. Thangavelu and V. K. Ramasamy (actor)|V. K. Ramasamy play supporting roles. The film is currently lost film|lost. http://www.thehindu.com/arts/cinema/article2801100.ece 

==Plot==
Sengodan (T. R. Ramachandran) owns 10 acres of land which prospered through his sheer hard work — it came to be called "Ponvayal" (Golden Farm). There were strong rumours that an incredible fortune in gold lay buried in the land.

Sengodan is in love with his cousin Semba (Anjali Devi). Esraj (K. A. Thangavelu), a counterfeiter, plans to get at the fortune. He seeks the help of a graduate Bangaru (R. S. Manohar) and his lover (Mynavathi). Without his knowledge, a policeman (K. Sarangkapani) and an actress "Savaal" Kannamma work against him and the smart couple succeed in spoiling the evil designs of the counterfeiter. Meanwhile, Sengodan is arrested on a charge of attempting to murder Bangaru. However, the smart detective helped by his girlfriend finds out the truth and all is well that ends well.

==Cast==
* T. R. Ramachandran as Sengodan
* Anjali Devi as Semba
* R. S. Manohar as Bangaru
* Mynavathi as Bangarus lover
* K. Sarangkapani as the policeman
* K. A. Thangavelu as Esraj

==Production==
  Kalvanin Kadhali, Salem District where a hillock resembled a deer at rest, and hence the name of the place.

Noted writer-director-producer A. T. Krishnaswami wrote the screenplay and dialogue based on Kalkis story and directed the film. He co-produced it with comedian Ramachandran and others. However, Krishnaswami did not get the recognition he richly deserved for his skills. 

==Soundtrack==
The music of the film was composed by noted Carnatic musician Thuraiyur Rajagopal Sarma and R. Rajagopal, under the supervision of another "forgotten" theatre personality N. P. Abdul Khader. The lyrics were penned by Yogi Suddhanantha Bharathiyar and Sundara Vathiyar who were popular during that period. 

{{tracklist
| headline        = Track listing 
| extra_column    = Singer(s)
| total_length    = 
| lyrics_credits  = 
| title1          = Azhiyaatha Veettil
| lyrics1         = 
| extra1          = Jikki 
| title2            = Enna Vinodham
| lyrics2         = 
| extra2          = Unknown
| title3            = Namma Kaliyaanam
| lyrics3         = 
| extra3          = Unknown
| title4            = Siruppudhan Varugudhaiya
| lyrics4         = 
| extra4          = Seerkazhi Govindarajan 
}}

==Reception== print of the film is known to exist today, making it a lost film. 

==References==
 

 
 
 
 