Birth (film)
{{Infobox film
| name           = Birth
| image          = Birth movie.jpg
| caption        = Theatrical release poster
| director       = Jonathan Glazer
| producer       = Lizie Gower Nick Morris Jean-Louis Piel
| writer         = Jean-Claude Carrière Milo Addica Jonathan Glazer
| starring       = Nicole Kidman Cameron Bright Danny Huston Lauren Bacall
| music          = Alexandre Desplat
| cinematography = Harris Savides
| editing        = Sam Sneade Claus Wehlisch
| distributor    = New Line Cinema
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = US$20 million
| gross          = US$23,925,492
| preceded by    =
| followed by    =
}}
Birth is a 2004 American drama film directed by Jonathan Glazer, starring Nicole Kidman, Lauren Bacall, Danny Huston and Cameron Bright.
 reincarnated as a 10-year-old boy (also named Sean). Annas initial skepticism is swayed by the childs intimate knowledge of the former married couples life. Despite critical praise for various components of the film, including Kidmans acting and Glazers direction, Birth received mixed reviews. 

Distributed by New Line Cinema, the films worldwide box office earnings total was US$23,925,492. 

==Plot==
Sean and Anna (Nicole Kidman) are a married couple living in New York City.  While scenes of Central Park are shown onscreen, Sean is heard lecturing to an unseen audience, explaining that he does not believe in reincarnation. After the lecture he goes jogging, collapses, and dies.  Ten years later, Anna has accepted a marriage proposal from her boyfriend, Joseph (Danny Huston).

When Clifford (Peter Stormare), Seans brother, arrives at Annas engagement party, his wife Clara (Anne Heche) excuses herself, saying she forgot to wrap Annas gift.  Instead, she buys a replacement after hurriedly burying the gift while a young boy (Cameron Bright) secretly looks on.

At a party for Annas mother (Lauren Bacall), the boy who followed Clara claims to be Annas deceased husband, Sean, and warns her not to marry Joseph.  At first Anna dismisses the boys claim. When Anna receives a letter from him the next day warning her not to marry Joseph, she realizes the boy truly believes he is her reincarnated husband.

That night Anna and Joseph discuss the letter. Since the building watchman seems to know the boy (and that the boy is also named Sean), Joseph calls to get more information.  When Sean answers the phone, Joseph rushes downstairs to confront him.  He takes him to Seans father (Ted Levine), and the three of them order Sean to leave Anna alone. Sean refuses to recant his story, and Anna watches Sean collapse in his fathers arms.

Sean leaves a message on Annas answering machine, which her mother overhears.  That day at lunch, Annas mother mentions that Sean wants to meet Anna in the park, and that she will know where.  Anna hurries to Central Park and finds Sean waiting in the spot where her husband died.  He offers to submit to questioning.

Annas brother-in-law Bob (Arliss Howard), a doctor, talks to Sean, recording his responses on tape.  He answers all the questions, even giving intimate details of Anna and Seans sex life.  Sean is brought to Annas by his mother (Cara Seymour), and he is able to identify parts of the apartment.  Everyone except Anna remains doubtful. Annas family becomes worried, particularly her sister Laura (Alison Elliott), who treats Sean with contempt.

When Anna misses an appointment with her fiance to spend time with Sean, Joseph begins feeling worried not merely about the boy, but about Annas odd behavior.  His jealousy is made plain when he physically attacks Sean. When Sean runs out, Anna follows him and kisses him on the lips.

Anna seems convinced by the boys story and asks Clara and Clifford to meet him. Clara encounters Sean at the door and asks him to visit her later. When he visits, he brings a backpack full of Annas love letters to Sean.  These were Claras spiteful engagement gift, which the boy secretly unearthed and read the night of the party.  We learn that Clara was Seans lover before his death, and that he gave the letters to her unopened as proof of his love.  Clara was jealous that Sean would not leave Anna, but abandoned her plan to give Anna the letters.  When Clara points out that if he were really a reincarnation of Sean he would have come to her first, Sean runs out, confused.

When Anna finds Sean, she suggests they run away and marry when he is of legal age.  He tells Anna that since he loves her he must not be the reincarnated Sean.

Anna apologizes to Joseph, and they are married at the beach.  Sean wrote a long letter apologizing to Anna, wondering why he had the delusion of being her husband.  Anna wades into the sea in anguish after the ceremony.  Joseph gradually pulls her back onto the sand and whispers into her ear.

==Cast==
*Nicole Kidman as Anna
*Cameron Bright as Young Sean
*Danny Huston as Joseph
*Lauren Bacall as Eleanor
*Alison Elliott as Laura
*Arliss Howard as Bob
*Michael Desautels as Sean
*Anne Heche as Clara
*Peter Stormare as Clifford
*Ted Levine as Mr. Conte
*Cara Seymour as Mrs. Conte
*Libby Skala as Bridesmaid

==Production==
Director Jonathan Glazer was interested in making a film about "the idea of eternal love" and a "mystery of the heart".    While writing the script, he was not interested in making a ghost story or a "paranormal piece".    He envisioned a   to discuss the idea with French screenwriter Jean-Claude Carrière at his producers recommendation. Carrière ended up helping Glazer with the story and acted as a script consultant.  The director spent eight months going back and forth to Paris every weekend turning one paragraph into three acts. The script went through 21 drafts as Glazer and co-screenwriter Milo Addica worked on the story.  With only a few weeks before principal photography was to begin, the two writers decided to refocus the entire film. Originally, the script was about the boy and they changed it to be about the woman instead. 

Actress Nicole Kidman read the screenplay and wanted to do the film when she found out that Glazer was directing as she loved his previous film, Sexy Beast.    She approached the director about doing the film. At first, he resisted because he felt that "her celebrity is so everywhere that I thought it could only hurt the delicate nature of this character".  However, he met with Kidman and realized that "she was ready to inhabit the role".  The more he talked to Kidman about her character, he would rewrite the script on weekends, tailoring it specifically for her.  To show Anna in mourning both externally and internally, Glazer gave her short hair, spare wardrobe and short, clipped speech.  The director explained Annas appearance as "somebody who had sort of let all glamor go and sexuality go".    Kidman said that Glazer instructed her to do small, personal reactions. She found the character to be all-consuming so that she could not separate herself from the role. To research for the role, Kidman spoke to two friends who had lost their fathers and they talked about how it still affected them years after. 

Addica and Glazer often wrote scenes the day before they were shot, giving them to the actors on the actual day they were shooting. 

==Reception==
Birth debuted at the 2004 Venice Film Festival where its first press screening was greeted with widely reported booing    and catcalls.  Glazer responded, "People are a bit polarised by it, which is healthy". 

The film was not well received among critics, garnering generally mixed to negative reviews. Birth has a 39% rating at Rotten Tomatoes and a 50 metascore at Metacritic. In his review for Newsweek, David Ansen wrote, "the script is hooey. Birth is ridiculous, and oddly unforgettable".    Michael OSullivan, in his review for the Washington Post, wrote, "What Im not so fond of is the cop-out ultimately taken by the filmmakers, who cant seem to follow through on their promisingly metaphysical premise (let alone the theme of obsessive love), electing instead to eliminate all ambiguity".    In his review for the New York Daily News, Jack Mathews called the film, "corny, plodding, implausible and - on occasion - seriously creepy".   
 David Thomson included the film in his list of 10 lost works of genius. 

===Controversy===
The film generated controversy due to a scene wherein Kidman shares a bath with Bright, both apparently naked.  In fact, Bright was never naked and the two were never even in the same room during the filming of the bath scene apart from one camera shot, and when this shot happened both wore special clothes that were not visible to the camera.  Glazer insists that the scene is not erotic or exploitative. "I can imagine that, before people see it, they might think it was salacious. But I knew it was never going to be that." 

At a press conference at the Venice Film Festival, Kidman addressed the controversy of her character kissing a boy: "It wasnt that I wanted to make a film where I kiss a 10-year-old boy. I wanted to make a film where you understand love."    Further controversy occurred at the festival when a journalist described Kidman as a "screen legend", to which her co-star, Lauren Bacall replied, "She is a beginner".  Kidman downplayed Bacalls remarks and said, "I certainly dont feel like a big star in Hollywood". 

Complaints of the films "cop-out" ending are questioned by Roger Ebert in his review, who notes: "There seem to be two possible explanations for what finally happens, but neither one is consistent with all of the facts."  That the young Sean knows precisely where the adult Sean died, for example, cannot be explained by his having memorized the love letters.

==Box office==
Birth was ranked 12th on its opening weekend, garnering United States dollar|USD$1,705,577 from 550 theaters. The worldwide box office earnings total was US$23,925,492, with US$5,095,038 in the United States (US) and US$18,830,454 in markets outside of the US.   

==See also==
* Sexy Beast (2000) Under the Skin (2013)

==References==
 

==External links==
*  
*  
*  
*  
*  
*   by Dave Calhoun for Time Out (26 Oct 2004)
*   by Daniel Robert Epstein for Suicide Girls
*  
*  
*  
*   - Village Voice
*  

 
 
 
 
 
 
 
 
 
 
 