The Big House (2000 film)
 
 
{{Infobox Film
| name = The Big House
| image =
| image_size =
| caption =
| director = Rachel Ward
| producer =
| writer = Rachel Ward
| narrator = Tony Martin Gary Sweet Paul Kelly
| cinematography = Toby Oliver
| editing = Margaret Sixel
| distributor =
| released = 18 October 2001 (USA)
| runtime = 24 minutes
| country = Australia
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
}} short drama Tony Martin and Gary Sweet. It was shown at the 2001 Reel Affirmations International Gay and Lesbian Film Festival.

==Plot==
Williams (Tony Martin) is a long term prisoner when Sonny (Kick Gurry) comes and is assigned to be Williams’ cell mate.

Williams decides to take Sonny under his wing, but his help and protection is conditional that Sonny occasionally has non-penetrative sex with him – masturbation or oral sex. Sonny is repulsed by this.

Sonny notices a photo of a boy above Williams’ bunk. Williams says that the boy in the photo is his son, and is probably a grown man by now. Williams has had no contact with his son for many years.

In the shower block Sonny is confronted with the prospect of male rape by another long term prisoner Jacko (Gary Sweet). Sonny then accepts Williams’ protection. 

Sonny does his time and then thanks Williams for looking out for him during his prison stay. 

Williams is assigned a new cell mate, and it appears unbeknownst to him that it is Anthony, his own son… with the prospect that Williams will like-wise ‘protect’ him during his prison stay.

==Cast== Tony Martin as Williams
* Gary Sweet as Jacko
* Kick Gurry as Sonny
* Paul Pantano as Anthony
* Shay Lawrence as Prisoner

==Awards==
In 2001 the film won the Australian Film Institute award for Best Short Fiction Film and was nominated for Best Screenplay. In 2002 it won the Film Critics Circle of Australia award for Best Short. {{cite web
 | title =Awards for The Big House
 | publisher =Internet Movie Database
 | url =http://www.imdb.com/title/tt0290465/awards
 | accessdate =5 November 2007 }}
 

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 