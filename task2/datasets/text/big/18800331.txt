Islands in the Stream (film)
{{Infobox film
| name           = Islands in the Stream
| image          = Islands in the stream poster small.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Franklin J. Schaffner
| producer       = Peter Bart Max Palevsky
| screenplay     = Denne Bart Petitclerc
| based on       =  
| starring       = George C. Scott David Hemmings Gilbert Roland
| music          = Jerry Goldsmith
| cinematography = Fred J. Koenekamp	
| editing        = Robert Swink
| distributor    = Paramount Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
  novel of the same name. The film was directed by Franklin J. Schaffner and starred George C. Scott, Hart Bochner, Claire Bloom, Gilbert Roland, and David Hemmings. Variety Film Reviews|Variety film review; March 9, 1977, page 16.  

==Plot==
Artist Thomas Hudson is an American who has left the civilized world for a simple life in the Caribbean. Schaffner tells the tale in four parts:
 Queen Mothers Anniversary.

*The Boys - Weeks after the celebrations for the Queen Mother, Tom is reunited with his three sons. It is a bittersweet reunion, because he left them and his wife Audrey four years before. Later they go on a challenging fishing trek to catch a Marlin. The segment ends as the boys return to the United States, where oldest son Tom joins the Royal Air Force in time for the Battle of Britain. Their father writes and tells them in a monolog how much he misses them.

*The Woman - Toms wife Audrey is introduced. Hoping she can give him companionship and love, Audrey returns to Tom to try to find what feelings may still exist between them. Tom finds he still loves his wife, but her real motive is revealed as the segment ends: she is there to tell him that young Tom is dead. This spoils her attempt at a reconciliation.

*The Journey - Tom attempts to help refugees escape the  " Eddy, that the refugees may not survive the voyage, and this trip may be suicide for all concerned if they face the Cuban Coast Guard. (In the novels climax, Tom battles a U-boat off the coast of Bimini.)

==Cast==
* George C. Scott as Thomas Hudson
* David Hemmings as Eddy
* Gilbert Roland as Captain Ralph
* Susan Tyrrell as Lil Richard Evans as Willy
* Claire Bloom as Audrey
* Julius Harris as Joseph
* Hart Bochner as Tom
* Brad Savage as Andrew
* Michael-James Wixted as David
* Hildy Brooks as Helga Ziegner

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 