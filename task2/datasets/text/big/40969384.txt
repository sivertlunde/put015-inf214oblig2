Pallet on the Floor
 
 
{{Infobox film
 | name = Pallet on the Floor
 | image = 
 | caption = 
 | director = Lynton Butler Larry Parr
 | writer = Ronald Hugh Morrieson (novel) Martyn Sanderson (screen) & Lynton Butler, Robert Rising
 | starring = Basil Spence Marshall Napier John Bach Bruno Lawrence Terence Cooper   Peter Rowley   Ian Watkin 
 | music = Bruno Lawrence (musical director & composer)   Jonathan Crayford & Barry Johnstone (composer-arrangers)
 | cinematography = Kevin Hayward
 | editing = 
 | distributor =
 | released = 1986 
 | runtime = 88 min. English
 | budget =
| gross = 
 }}
Pallet on the Floor is a 1986 New Zealand made comedy and horror film, based on the 1976 novel (his last novel) by Ronald Hugh Morrieson. The film was shot in 1983 at Patea, in a closed-down "meatworks" (abattoir), but was not released until 1986. Bruno Lawrence wrote the jazz score, and has a small cameo role playing Morrieson as a bass player at a wedding.

Though starring notable New Zealand actors and musicians, the focus of the plot was shifted to the British remittance man in the hope that Peter O’Toole would take the role. While billed as comedy, the film depicts 1960s racism and class divisions, and maintains Morrieson’s trademark preoccupations .... of sex, death, mateship, voyeurism, violence, booze and mayhem in bleak small town New Zealand. 

==Plot==

Life was hard enough for Sam Jamieson without Jack Voot’s lechery and Miriam Breen’s jealousy. Then life at Kurikino erupted into a sensation of murder and blackmail, turning his life into a nightmare from which the efforts of Tinny Entwistle, Gigglejuice Saunders and the Remittance Man could not save him. But Spud McGhee had an idea .... . Sam never gets round to working on his cottage, he goes to the Brian Boru the only hotel in Kurikino, run by Amos Blennerhasset. Wife Sue is pregnant. Sam works at the big slaughter-house across the bridge at the foot of the hill known as the Works. 

==Cast==
* Peter McCauley ... Sam Jamieson
* Jillian OBrien ... Sue Jamieson
* Bruce Spence ... Basil Beaumont-Foster (the Remittance Man) 
* Shirley Gruar ... Miriam Breen 
* Alastair Douglas ... Stan Breem 
* Tony Barry ... Larkman (the cop) 
* Jeremy Stephens ... Spud McGhee
* Michael Wilson ... Shorty Larsen
* Terence Cooper ... Brendon OKeefe 
* John Bach ... Jack Voot 
* Marshall Napier ... Joe Voot 
* Curly del Monte ... Mason Voot 
* Peter Rowley ... Henderson
* Ian Watkin ... Amos
* Sonny Waru ... Mohi Te Kiri 
* John Rist ... Gigglejuice Saunders 
* Bruno Lawrence ... Ronald Hugh Morrieson

==External links==
*  
*  
*  

==References==
 

 
 
 
 
 
 
 
 
 
 


 
 