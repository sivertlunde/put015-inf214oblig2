Appu (2002 film)
{{Infobox film
| name           = Appu
| image          = 2002 Kannada film Appu poster.jpg
| caption        = Film poster
| director       = Puri Jagannath
| producer       = Parvathamma Rajkumar
| writer         = Puri Jagannath
| screenplay     = Puri Jagannath
| starring       = Puneeth Rajkumar Rakshita Avinash
| narrator       = 
| music          = Gurukiran
| cinematography = K. Datthu
| editing        = S. Manohar
| studio         = Poornima Enterprises
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Kannada film Rajkumar family. It marked the screen debut Puneeth and Rakshita in lead roles. 
 Telugu as Tamil as Dum (2003 Tamil film)|Dum and in Bengali as Priya Amar Priya in 2008.

==Cast==
* Puneeth Rajkumar
* Rakshita
* Avinash
* Srinivasa Murthy Sumithra
* Srinivasa Murthy Ashok
* Satyajith
* Prithviraj
* Vinayak Joshi
* Keerthi
* Honnavalli Krishna
* Bullet Prakash

==Production==
After the success of Yuvaraja (film)|Yuvaraja, Puri Jagannadh was approached by Rajkumar family to introduce their third son Puneet Rajkumar to make his onscreen debut as lead actor. Puri gladly accepted the opportunity.  Rakshitha, daughter of cameraman BC Gowrishankar made her acting debut with this film, she went on to play the same character in its Telugu and Tamil remakes. 

==Soundtrack==
{{Infobox album
| Name        = Appu
| Type        = Soundtrack
| Artist      = Gurukiran
| Cover       = 2002 Kannada film Appu album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 2002
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Akash Audio
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Gurukiran composed the films background score and music for its soundtrack, with the lyrics written by Upendra, Sriranga and Hamsalekha. The soundtrack album consists of six tracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Taliban Alla Alla
| lyrics1 = Upendra
| extra1 = Puneeth Rajkumar
| length1 = 
| title2 = Baare Baare Kalyana
| lyrics2 = Sriranga Chithra
| length2 = 
| title3 = Panavidu Panavidu
| lyrics3 = Hamsalekha Rajkumar
| length3 = 
| title4 = Ellinda Aarabhavo
| lyrics4 = Sriranga
| extra4 = Udit Narayan, Chithra
| length4 = 
| title5 = Jolly Go Jolly Go
| lyrics5 = Hamsalekha
| extra5 = Shankar Mahadevan
| length5 = 
| title6 = Aa Devara Haadidu
| lyrics6 = Hamsalekha
| extra6 = Rajkumar 
| length6 = 
}}

==References==
 

 

 
 
 
 
 
 


 