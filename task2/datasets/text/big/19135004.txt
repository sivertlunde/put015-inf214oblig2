Professor Kranz tedesco di Germania
 
{{Infobox film
| name           = Professor Kranz tedesco di Germania
| image          = Professor Kranz tedesco di Germania.jpg
| caption        = Film poster
| director       = Luciano Salce
| producer       = Fausto Saraceni
| writer         = Leonardo Benvenuti Augusto Caminito
| starring       = Paolo Villaggio
| music          = Piero Piccioni
| cinematography = Danilo Desideri
| editing        = Antonio Siciliano
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}
Professor Kranz tedesco di Germania is a 1978 Italian comedy film directed by Luciano Salce and starring Paolo Villaggio.
   
Despite the title the character played by Villaggio bears little resemblance from the one he played on Italian television shows under the same moniker. Instead of a sadistic/overbearing personality Kranz is here shown with a bumbling and meek demeanor more similar to that of the other loser characters in Villaggios repertoire (Fantozzi and Fracchia).

==Plot==
The story, recounting the traversies of a German scientist/stage magician eking out a miserly life in Rio de Janeiros favelas until getting involved in an improbable kidnapping/ransom plot, is paper-thin and little more than an excuse to show "exotic" locales.

==Cast==
* Paolo Villaggio as Kranz
* José Wilker as Leleco
* Vittoria Chamas as Dosdores Maria Rosa as Raimunda
* Walter DÁvila
* Geneson Alexandre De Souza
* Joachim Soares
* Berta Loran
* Gina Teixeira
* Fernando José
* Adolfo Celi as Carcamano

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 