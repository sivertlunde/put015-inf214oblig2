Naach Uthe Sansaar
{{Infobox film
| name           = Naach Uthe Sansaar
| image          = 
| image_size     = 
| caption        = 
| director       = Yakub Hasan Rizvi
| producer       = 
| writer         =
| narrator       = 
| starring       =
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1976
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1976 Bollywood romance film directed by Yakub Hasan Rizvi. 

==Cast==
*Chandrima Bhaduri ...  Mrs. Mehto 
*Simi Garewal ...  Somu 
*Rajan Haksar ...  Catholic Priest 
*Aruna Irani ...  Nital 
*Shashi Kapoor ...  Karmu 
*Roopesh Kumar ...  Johnny 
*Hema Malini ...  Nanki Mehto 
*Leela Mishra ...  Karmus mom 
*Rajendra Nath ...  Trithu 
*Bhushan Tiwari ...  Vaidraj Mehto 
*Ramayan Tiwari ...  Jaggu Mehto (as Tiwari) 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tere Sang Jeena Tere Sang Marna"
| Mohammed Rafi, Lata Mangeshkar
|-
| 2
| "Logva Kahe Mujhe"
| Mohammed Rafi
|-
| 3
| "O Diljaniya"
| Mohammed Rafi
|-
| 4
| "Sukhi Dharti Dhool Udaye"
| Mohammed Rafi
|-
| 5
| "Naach Uthe Sansaar"
| Mohammed Rafi, Lata Mangeshkar
|-
| 6
| "Aaja Re Aaja"
| Mohammed Rafi
|}
==External links==
*  

 
 
 
 
 
 