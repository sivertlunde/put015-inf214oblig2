Pax Americana and the Weaponization of Space
 
{{Infobox film
| name = Pax Americana and the Weaponization of Space|
| image = Pax_americana.jpg
| director = Denis Delestrac
| writer = Denis Delestrac, Harold Crooks
| narrator = Mikela Jay
| producer = Lucie Tremblay, Jeremy Edwardes, Brice Garnier, ARTE France
| editing = Alam Raja
| music = Amon Tobin
| cinematography = Jean-Pierre Saint-Louis
| sound = Philippe Scultety
| sound design = Benoît Dame
| sound mix = Jean-Pierre Bissonnette
| distributor = Films Transit International
| released =  
| runtime = 85 minutes
| country = Canada - France
| language = English - French
}}
Pax Americana and the Weaponization of Space is a documentary film by Denis Delestrac with a music score by Amon Tobin. The film deals with the issue of space weapons and their politics, featuring interviews with several key United States military personal, academics such as Noam Chomsky and others, including Martin Sheen. The film won the Best Documentary award at the 2009 Whistler Film Festival and has been selected in a number of international film festivals.  

==Synopsis==
The prospect of Earth being ruled from space is no longer science-fiction. The dream of the original Dr. Strangelove, Wernher von Braun (from Nazi rocket-scientist to NASA director) has survived every US administration since World War II and is coming to life. Today the technology exists to weaponize space, a massive American industry thrives, and nations are maneuvering for advantage.

==Production==
Among the challenges faced by the production team was the difficulty to get the authorization to bring a camera inside the Air Force Space Command bases and other US military and government locations. After four months of investigation, the US Department of Defenses public relations office decided to grant Denis Delestrac and his crew the access to the locations they had requested. It is the first time most of the Air Force Space Command bases have been filmed by a civilian crew.

==Locations==
* Air University at Maxwell Air Force Base, Montgomery, Alabama
* Air Force Space Commands 50th Space Wing, Schriever Air Force Base, Colorado Springs, Colorado
* Kirtland Air Force Base, Albuquerque, New Mexico
* Starfire Optical Range, Albuquerque, New Mexico
* The Space Foundation, National Space Symposium, Colorado Springs, Colorado
* The White House, Washington DC
* US Air Force Academy, Colorado Springs, Colorado
* US Air Force Research Laboratory, Albuquerque, New Mexico
* US Department of Defense, Arlington, Virginia
* United States House of Representatives, Washington DC
* White Sands Missile Range, Albuquerque, New Mexico

==Interviews==
 
The film features interviews with prominent weaponization of space advocates, space policy analysts, politicians, diplomats, military officials and peace activists. The following individuals were interviewed for the film:
 
  Deputy Chief of Staff of the Air Force for Air and Space Operations
* Doctor Helen Caldicott, Australian physician and anti-nuclear advocate
* General Kevin P. Chilton, Combatant Commander|Commander, United States Strategic Command
* Professor Noam Chomsky, MIT professor, linguist and political activist
* Joseph Cirincione, president, the Ploughshares Fund
* Richard DalBello, vice president, Intelsat
* Everett Dolman, military space strategist
* Alain Dupas, European space expert
* Craig Eisendrath, former diplomat, US State Department
* Frank Gaffney, Center for Security Policy
* Bruce Gagnon, Global Network against Weapons and Nuclear Power in Space
* Bill Gertz, The Washington Times
* Karl Grossman, author of "Weapons in Space"
* Theresa Hitchens, United Nations Institute for Disarmament Research

 
* Gregory Kulacki, Union of Concerned Scientists
* Senator Jon Kyl
* Professor Howard E. McCurdy, American University
* Mike Moore, author of "Space Cop"
* Roald Sagdeev, former director of the Russian Space Research Institute
* Martin Sheen, actor and activist
* Isabelle Sourbès-Verger, French National Centre for Scientific Research
* Baker Spring, The Heritage Foundation
* Steven Staples, The Rideau Institute
* Robert J. Stevens, CEO, Lockheed Martin Corporation
* Cadet Colton Tuttle, US Air Force Academy
* Tim Weiner, The New York Times
* Loring Wirbel, Citizens for Peace in Space
* Litai Xue, Stanford University

 

==Versions==
 

===Theatrical version===
The 85-minute theatrical version was screened at international film festivals since November 2009 and  released in theaters across Canada in June 2010.

===ARTE version===
A 76-minute version, titled "Pax Americana ou la conquête militaire de lespace", was created for French-German broadcaster and co-producer ARTE. It was broadcast in March 2010 and was followed by a debate on the weaponization of space including French TV host Annie-Claude Elkaïm with Samuel Black (Henry L. Stimson Center, Washington DC), Otfried Nassauer (Berlin Information Center for Transatlantic Security, Berlin) and Xavier Pasco (Fondation pour la Recherche Stratégique, Paris)

===52 minutes version===
A 52-minute version was edited for the French-Canadian public broadcaster Radio Canada (dubbed and narrated in French) and for the international TV market (in English).

===CBC version===
This 43-minute version, titled "Masters of Space", premiered on CBC-TV and repeated on CBC News Network in April 2010 as part of the scientific and environmental program "The Nature of Things", presented and narrated by acclaimed Canadian environmental activist David Suzuki.

==Festivals and awards==
* Best Documentary Award, Whistler Film Festival 2009 (Canada)
* Official Selection, International Documentary Festival of Amsterdam « IDFA » 2009 (Netherlands)
* Official Selection, Festival international de programmes audiovisuels « FIPA » 2010 (Biarritz, France)
* Official Selection, Thessaloniki Documentary Festival 2010 (Greece)
* Official Selection, Victoria Film Festival, 2010 (Canada)
* Official Selection, Planete Doc Review Film Festival, 2010 (Varsaw, Poland)
* Official Selection, « DOXA » Documentary Festival, 2010 (Vancouver, Canada)
* Official Selection, London International Documentary Festival, 2010 (London, UK)
* Official Selection, Sci-Fi London, 2010 (London, UK)
* Official Selection, Madrid International Documentary Festival, 2010 (Madrid, Spain)
* Official Selection, Encounters International Documentary Festival, 2010 (Cape Town, South Africa)
* Opening Film, Oxdox International Documentary Festival, 2010 (Oxford, UK) DocAviv International Documentary Festival, 2010 (Tel Aviv, Israel)

==Soundtrack==
The original soundtrack was composed by electronic artist and DJ Amon Tobin who created the Pax Americana Theme and other exclusive tracks for the movie. Additionally, Pax Americana features a number of artists signed by the London based label Ninja Tune like Coldcut, Blockhead (music)|Blockhead, Bonobo and Neotropic as well as Montréal based DJ Champion.

==References==
 

==External links==
 
*   official website
*  
*   trailer and movie clips on YouTube
*  
*   international agent
*  

 
 
 
 
 
 