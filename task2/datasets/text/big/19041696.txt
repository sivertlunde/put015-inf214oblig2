Ordinary Heroes (film)
 
 
{{Infobox film
| name           = Ordinary Heroes 千言萬語 / Qian yan wan yu
| image          = 
| border         = 
| alt            = 
| caption        = 
| director       = Ann Hui
| producer       = 
| writer         = Kin Chung Chan
| screenplay     = 
| story          = 
| based on       =   Anthony Wong
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}
Ordinary Heroes ( ) is a 1999 Cantonese|Cantonese-language film directed by Ann Hui. It was co-produced by Hong Kong and Peoples Republic of China|China.  It concerns social reform activists in Hong Kong.

The films Chinese title refers to a popular song by Taiwanese singer Teresa Teng.

==Cast==
* Lee Kang-sheng - Tung
* Anthony Wong Chau-Sang - Peter Kam
* Tse Kwan-Ho - Yau
* Loletta Lee - Sow (credited as Rachel Lee)
* Paw Hee Ching - Tungs mother

==Awards== Golden Horse Best Foreign Language Film submission at the 72nd Academy Awards, but did not manage to receive a nomination.  It was also entered into the 49th Berlin International Film Festival.   

==References==
 

==External links==
* 
* 
*  

 
 
 

 
 
 
 
 
 
 
 
 