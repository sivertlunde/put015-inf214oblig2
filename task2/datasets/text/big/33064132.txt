The Incident (2011 film)
{{Infobox film
| name           = The Incident
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        =  Alexandre Courtes
| producer       = Wassim Béji Emilie Châtel Doug Davison Roy Lee
| writer         =  S. Craig Zahler  
additional dialogue and material Jérôme Fansten
| based on       =  
| narrator       = 
| starring       = Rupert Evans Dave Legeno Anna Skellern
| music          = Christophe Chassol
| cinematography = Laurent Tangy
| editing        = Baxter
| studio         = Artémis Productions Marquis Productions Vertigo Entertainment Wy Productions
| distributor    = SND Groupe M6 
| released       =  
| runtime        = 85 minutes
| country        = France   
| language       = english
| budget         = 
| gross          = 
}}
 Alexandre Courtes and written by S. Craig Zahler. 

== Plot ==
In 1989, George (Rupert Evans), Max (Kenny Doughty) and Ricky (Joseph Kennedy) are in a band, but it is clear that the group is starting to fall apart. Ricky is unreliable, Max is immature and George is becoming distant as he builds a relationship with his girlfriend Lynn. In order to make ends meet, the three also work in the cafeteria of an asylum for the criminally insane along with a guy named William (Marcus Garvey). George is the head chef and puts effort into preparing high quality food and being courteous to the inmates. The inmates are generally docile due to the pills they take, but one inmate in particular named Harry Green (Richard Brake) creeps George out.

One morning after a late-night rock show, an exhausted George has to come in early to work in a torrential downpour. Many bad omens occur - with the chicken meat still attached to the heads, George cutting himself, and later, he loses his temper at both his bandmates and Harry Green who stares menacingly at him. George also thinks that he sees Harry Green convince a fellow inmate to spit out his pills. Max tells George to take a nap while he prepares the dinner meal. A few hours later, however, an explosion is heard and Max wakes George up to tell him that the power has gone out in the building. Because the exterior doors are electronically locked, everyone is now trapped inside the building.
 
Many of the inmates have already been brought into the dining room for dinner service and the head guard enlists George and William to help escort them back to their cells. Unknown to those in the kitchen, several other inmates have seized the opportunity to overtake and kill the prison guards and nurses on the other side of the building. George and William succeed in getting the inmates to their cells, but they see that the inmates are growing restless and the locks on all the doors are not working. When they return to the kitchen, they find the head guard has disappeared and Ricky and Max have barricaded themselves in the freezer.

Suddenly, several inmates walk into the dining area and begin trying to break into the kitchen. Max, William and George manage to sneak out of the kitchen after the inmates break in, but Ricky refuses to come with them and remains barricaded in the freezer. George sees Harry Green smiling at all of the mayhem, appearing to be the ringleader of the riots. The threesome run around the asylum, finding the head guard decapitated. They eventually make their way to the head guards office and use the landline phone to call the police, but are told that the police will be unable to respond for at least an hour.

Eventually, the prisoners capture Ricky and fatally impale him in front of George, Max, and William. Urged on by Harry Green, the inmates storm the head guard office, although the cooks manage to kill all of the ones who attack (Maxs nose is bitten off in the struggle). The three flee back to the kitchen, but eventually George is knocked out and Max is captured. When George awakens, he is naked and tied to a table. George is forced to watch as inmates (again led by Harry Green) burn Max alive on the stovetop. One inmate also begins mutilating George, though eventually George convinces him to leave him alone.

George is finally confronted by Harry Green, who begins to gleefully eat his own fingers.  George screams at Harry Green to end the madness to no avail. George hears police sirens and soon, William leads the police into the kitchen. Harry Green has disappeared. As the medics begin wheeling George out of the asylum, he looks over to see the corpse of Harry Green (with all his fingers intact), seemingly having been killed at the very beginning of the riot. Apparently, George has hallucinated Harry Greens presence throughout the night.

The film jumps ahead to focus on George in a bed while Lynn cries at his side. George has a vacant expression, having seemingly been driven mad by his experience. The camera zooms into Georges mind where he is still working in the cafeteria with his band mates, and the inmates are still on the loose.

== Cast ==
* Rupert Evans as George
* Dave Legeno as J.B.
* Anna Skellern as Lynn
* Richard Brake as Harry
* Kenny Doughty as Max
* Eric Godon as The Pill Inmate
* Joseph Kennedy as Ricky
* Darren Kent as Pete
* Marcus Garvey as William
* Martin Swabey as Underwear Inmate
* Pascal Biondolillo as James Harper
* Ian Lyons as Police Lieutenant
* Sandro Mastronardi as Ambulance Man
* Claude Stark as Tim
* Nina Newman as Jennifer

== Release ==
The film was released in the USA on 4 May 2012 as Asylum Blackout. 

==References==
 

==External links==
*  

 
 
 
 
   
   
 
 
 