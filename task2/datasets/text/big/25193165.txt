No, No, Nanette (1940 film)
{{Infobox film
| name           = No, No, Nanette
| image          = 
| image_size     = 
| caption        = 
| director       = Herbert Wilcox
| producer       = Merrill G. White (associate producer) Herbert Wilcox (producer)
| writer         = Frank Mandel (musical) Otto A. Harbach (musical) Vincent Youmans (musical) Emil Nyitray (musical) Ken Englund (screenplay)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Russell Metty
| editing        = Elmo Williams
| distributor    = RKO Radio Pictures
| released       = 13 December 1940
| runtime        = 96 minutes 126 minutes (Ontario, Canada)
| country        = USA
| language       = English
| budget = $570,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56 
| gross = $940,000 
}}
 the musical of the same name.

== Cast ==
*Anna Neagle as Nanette Richard Carlson as Tom Gillespie
*Victor Mature as William Trainor
*Roland Young as Mr. "Happy" Jimmy Smith
*Helen Broderick as Mrs. Susan Smith
*Zasu Pitts as Pauline Hastings
*Eve Arden as Kitty
*Billy Gilbert as Styles Tamara as Sonya
*Stuart Robertson as Stillwater Jr. / Stillwater Sr.
*Dorothea Kent as Betty
*Aubrey Mather as Remington, the butler Mary Gordon as Gertrude, the cook Russell Hicks as "Hutch" Hutchinson

== Soundtrack ==
* Anna Neagle - "No No Nanette" (Written by Vincent Youmans and Irving Caesar)
* Anna Neagle and Roland Young - "I Want To Be Happy" (Written by Vincent Youmans and Irving Caesar)
* Tamara - "I Want To Be Happy"
* Eve Arden - "I Want To Be Happy"
* Sung by Anna Neagle and Richard Carlson - "I Want To Be Happy"
* Anna Neagle and Richard Carlson - "Tea For Two" (Written by Vincent Youmans and Irving Caesar)
* "Ochi Chornya" (traditional)

==Reception==
Although the film was popular its cost meant it made a small loss of $2,000. 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 

 
 

 