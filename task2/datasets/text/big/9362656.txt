Passing Glory
{{Infobox film
| name           = Passing Glory
| image          = Passing Glory Poster Small.jpg
| alt            = 
| caption        = TNT Film Poster
| film name      =   Steve James
| producer       = Quincy Jones Magic Johnson Harold Sylvester
| writer         =  
| screenplay     = Harold Sylvester
| story          = 
| based on       =  
| starring       = Andre Braugher Rip Torn Sean Squire
| narrator       =  
| music          = Stephen James Taylor
| cinematography = 
| editing        = 
| studio         =  
| distributor    =  
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}} Steve James. documentary Hoop Dreams, also directed by Steve James.  Music composed by Stephen James Taylor.

==Plot==
An angry black priest (Andre Braugher) in 1960s New Orleans goes against the wishes of his parish leader (Rip Torn) as he pushes a basketball game between his unbeaten all-black team and an undefeated all-white prep school team. Jesuit High. It focuses on the struggles that Father Joseph Verette had in trying to pull the game off and trying to earn respect for his team.

Hired as a history teacher, Father Perry will not let the athletes in his classes be given the special treatment that theyve been used to. "I teach history," he informs the headmaster when asked to take over the suddenly vacant position of basketball coach. "I believe sports are overemphasized." Moreover, coming from the North, he cant understand why star black athletes dont go to the best white colleges, as they should. "Down here, should and is is a long ways apart," the dad of the teams star informs him.
The film includes many tangible examples of the racism then present. The blacks have to go to a separate "coloreds only" line at fast food outlets, and ordering a meal in the wrong place can and does get you thrown in jail.

==Cast==
* Andre Braugher as Father Joseph Perry
* Rip Torn as Father Robert Grant
* Sean Squire as Travis Porter
* Ruby Dee as Mommit Porter
* Bill Nunn as Howard Porter
* Daniel Hugh Kelly as Mike Malone Sr.
* Tony Colitti as Chick Viola
* Shawn Wright as Jerry Singer

==References==
 

==External links==
*  
*   article in Baltimore Sun, by John Rivera, February 20, 1999 (about film based on facts and use of "artistic license")

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 