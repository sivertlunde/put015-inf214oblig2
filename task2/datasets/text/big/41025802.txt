Mundasupatti
 
{{Infobox film
| name           = Mundaasupatti
| image          = 
| alt            = 
| caption        = Film Poster
| director       = Ram
| producer       = C. V. Kumar
| story          = Ram
| screenplay     = Ram Vishnu Nandita Kaali Venkat
| music          = Sean Roldan
| cinematography = P. V. Sankar
| editing        = Leo John Paul
| distributor    = 
| studio         =   Fox Star Studios
| released       = 13 June 2014
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Vishnu and Nandita. Produced by Thirukumaran Entertainments C. V. Kumar in association with Fox Star Studios, it features music by Sean Roldan, editing by Leo John Paul and camerawork by P. V. Sankar.    Based on a short film Ram himself shot in 2011 for the TV show, Naalaya Iyakunar, the film is set in the 80s in a fictional village called Mundasupatti, where people believe bad luck will befall them if they are photographed. The story illustrates what happens when two photographers visit the village.    Mundasupatti released on 13 June 2014 to positive critical response.

==Cast== Vishnu as Gopi
*Nandita as Kalaivaani
*Kaali Venkat as Azhagumani
* Ramdoss as Muniskanth
* Anandaraj as Zamindar Ekambaram
* Kovai Uma

==Production==
Ram Kumar had shot about 10 short films for the TV show, Naalaya Iyakunar. When Kadhalil Sodhappuvadhu Yeppadi (a film developed from a short film) released, he said he was inspired to make his own film. He took a year off to work on the script and to select his actors. He called it a challenge to convert the script for a short film into a full-length one and stated that he had added new characters for the film version. 

Vishnu Vishal was cast for the lead role, with the director stating that he wanted "someone who is funny, but very subtly so".  Vishnu, who had till then only played characters that were emotional,    for the first time attempted a full-length comedy role, who felt it was "very refreshing"    and added that "for the first time, I could relate to my character".  Nandita was signed for the lead female role,  and Ram said that he wanted "someone who is dusky" for the role of Kalaivaani, adding that he was impressed by her performance in Attakathi.  Kaali Venkat who had been part of the original short film played the assistant to Vishnus character.   Ramdoss, seen in small roles previously, was given the role of an aspiring actor, a "fantastic role", according to him, that, he said, gave him "a lot of scope for acting". 

The filming started on 28 August 2013  and was completed by October 2013.  The film was shot in 57 days  in Sathyamangalam, which has houses that are over a 100 years old. 

==Soundtrack==
{{Infobox album
| Name     = Mundasupatti
| Type     = Soundtrack
| Artist   = Sean Roldan
| Released = 28 April 2014
| Cover    =  Feature film soundtrack
| Length   = 22:25
| Label    = Think Music
| Producer = Sean Roldan
| Last album = Sathuranga Vettai (2014)
| This album = Mundasupatti (2014)
| Next album = 
}}
The films soundtrack was composed by independent musician Sean Roldan. It was the first film project he had signed on, although two of his soundtrack albums got released before the release of Mundasupatti.  Sean Roldan had composed seven tracks for the film, besides singing three of them. The album to Mundasupatti was launched along with the films trailer on 28 April 2013 at the Suryan FM radio station. 

The songs as well as the score received very positive feedback by critics. Behindwoods gave it 3.25 out of 5 stars and called it "one of the most enterprising albums of this year".  Musicaloud.com gave it a score of 8.5 out of 10 and wrote, "There seems to be no stopping Sean Roldan".  Rediff wrote, "Sean Roldans rustic songs and background score are definitely the highlights of the film, especially the folk styled Rasa Maharasa and the beautiful melody Idhu Enna".    Deccan Chronicle wrote, "The   standout of the film was Sean Roldans background and music. His catchy but affectionate songs fit very well with the clayey tones of the film – which is to say that they mould and reshape the landscape in lieu with the plot".   

{{Track listing extra_column = Singer(s) music_credits = no lyrics_credits = no all_lyrics = Muthamil (except where noted)

|title1= Uchiyila Udhichavane
|extra1= Vivek Narayan
|length1= 2:09
|title2= Kadhal Kanave
|extra2= Pradeep Kumar, Kalyani Nair
|length2= 4:08
|title3= Rasa Magarasa (Duet Version)
|extra3= Rita, Anthony Dasan, Sean Roldan
|length3= 3:06
|title4= Ambala Singam
|extra4= Haricharan, Sean Roldan
|notes4= GKB
|length4= 3:58
|title5= Idhu Enna
|extra5= Haricharan, Kalyani Nair
|length5= 2:59
|title6= Rasa Magarasa (Solo Version)
|extra6= Sean Roldan
|length6= 3:06
|title7= Killadi Oruthan
|extra7= Anthony Dasan
|length7= 2:59
}}

==Release==
The satellite rights of the film were sold to STAR Vijay.  To promote the film the makers had created an interactive application on the website, www.mymundasupattistyle.com that allowed visitors to create their own avatars akin to characters from Mundasupatti.  The idea was well received and had gone viral on the social networking sites. 

A special screening of Mundasupatti was arranged for distributors and exhibitors couple of prior to the release. Subsequently, the film opened across nearly 207 screens in Tamil Nadu, which was considered "very high for a Vishnu film".  The film released on 13 June 2014 alongside five other Tamil releases, but according to Sify.com, it got "the best of screens and prime shows in multiplexes". 

===Critical reception===
The film received generally positive feedback from critics. The Times of India gave 3.5 stars out of 5 and wrote, "Director Ram Kumar, who is the latest promising find from the TV show Naalaya Iyakkunar, certainly knows how to end a joke with a punchline. The film is a tad overlong ... and some of the comedy feels repetitive but Ram Kumar manages to keep things playful and entertaining at all times ... that we look past some of the indulgences".    Sify.com wrote, "Mundasupatti is a rip-roaring rocker! It is a full length laugh riot which is fresh and entertaining. The wacky story and treatment of debutant director Ram Kumar, makes it tickling and an absolute fun ride".    Deccan Chronicle gave 3.5 out of 5 and wrote, "They say that a tragedy today is bound to be a comedy tomorrow – an unfortunate incident, a set of beliefs to explain that incident, and the culture of fear and superstition that this broods are all probable misnomers that we all will look back at and have a mighty laugh. Take it a step further by adding situational comedy to an initial tragedy and what we have is a film that succeeds in many levels with the possible muse of timing and pacing".  Silverscreen.in wrote, "Mundasupatti is set in the 80s. Fascinatingly so. It is perhaps a bizarrely comical version of a Stephen Leacock essay, but fashionably vintage and very Indian".    The New Indian Express wrote, "the debutant director has lived up to the expectations, establishing his credentials with his very first effort. The plot is refreshing, the screenplay engaging, the dialogue sparkling with wit and humour. The director moves his narration deftly and with confidence. Mundasupatti with no big names to boast of, yet again proves it. Its a film not to be missed". {{cite web|url=http://www.newindianexpress.com/entertainment/reviews/No-Big-Names-to-Boast-of-But-a-Must-watch-Comic-Entertainer/2014/06/15/article2281264.ece|title=No Big Names to Boast of, But a Must-watch Comic Entertainer –
The New Indian Express|publisher=newindianexpress.com|accessdate=20 July 2014}} 

Baradwaj Rangan wrote, "I wish more had been done with the central conceit. This is not a lazy film by any stretch. But a comedy should be light on its feet. Linger too much, and it can begin to seem out of focus".    Behindwoods gave it 3 stars out of 5 and wrote, "Mundasupatti could have used some trimming as it gets slightly tedious at times but the feeling while coming out, is one of satisfaction. This debut offering from director Ram Kumar has turned out to be a worthy product from the CV Kumar – Fox Star Studios camp" and concluded that the film is "A feel-good comedy entertainer".    Indiaglitz gave 3 out of 5 and wrote, "though being predictable, this one glues you to the seat for an enjoyable two and half hours. Revolving entirely around the sentiments attached to camera, with a young and vibrant crew, the film is a thorough entertainer".    Rediff.com gave it 2.5 out of 5 stars and called the film "an entertaining rom-com" and "a decent attempt by debutant D Ram Kumar", but criticized that "the narration is extremely slow paced and the film too long, taking away some of the fun".    OneIndia gave 2.5 out of 5 and concluded, "Mundasupatti is a fun-filled drama. Entertains at first watch".    The Hindustan Times gave 2.5 out of 5 and concluded, "Mundasupatti has some delightful moments of freshness to offer".    Rajasekar S, from Cinemalead called Mundasupatti as a long and tiring ride, rating it at a half baked 2.5 out of 5.   

===Box office===
Mundasupatti collected   31&nbsp;million in its opening weekend in Tamil Nadu, which was the highest ever opening for a C. V. Kumar film.      

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 