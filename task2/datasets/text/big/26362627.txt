Love and Anger (film)
 
{{Infobox film name = Amore e rabbia -Love and Anger- caption = A poster bearing the films English title: Love and Anger image = Amore e rabbia FilmPoster.jpeg director = Marco Bellocchio  (segment "Discutiamo, discutiamo")  Bernardo Bertolucci  (segment "Agonia")  Jean-Luc Godard  (segment "Lamore")  Carlo Lizzani  (segment "Lindifferenza")  Pier Paolo Pasolini  (segment "La sequenza del fiore di carta")  Elda Tattoli  (segment "Discutiamo, discutiamo")")  producer = writer = Pier Paolo Pasolini Mauro Bolognini Marco Bellocchio Bernardo Bertolucci Carlo Lizzani Jean-Luc Godard starring = Marco Bellocchio Ninetto Davoli Julian Beck Nino Castelnuovo music = Giovanni Fusco distributor = cinematography = Alain Levent Sandro Marconi editing = Nino Baragli Franco Fraticelli released =   runtime = 102 minutes country = Italy France language = Italian, French, English, German budget =
}}
Amore e rabbia (Love and Anger) is a 1969 anthology film that includes five films directed by five Italian directors and one French director. It premiered at the 19th Berlin International Film Festival in 1969.   

==Plot==
The film is composed of episodes that deal with some of the themes present in Jesus parables and anecdotes of the canonical gospels. These issues, however, are reproduced in the present from their directors.

==Segments==
===Indifference===
A man is suffering from road, badly injured. Passers do not deign to look at him, and continue walking on their way. The episode is taken from Jesus parable of the Good Samaritan.

===Agony===
A bishop is ill and about to die. Before he dies, the man has a vision of God, who tells him that his life has been misspent. The bishop realizes that he spent his life not properly respecting the gospel, but now it is too late.

===The sequence of the paper flower===
A beautiful smiling guys walking on the streets of a city, bringing with him a large poppy paper. The boy is the goodness and innocence of youth, which is soon cut short by human wickedness. Indeed, while the merry boy is walking, the episode shows the evil done by man during the Second World War. At the end of the story, the boy is struck by lightning from the sky and dies, guilty of having been in his life a happy person and a good neighbor.

===Love===
A  woman and a manre arguing with each other. They represent democracy and the peoples revolution that can not get along, although their ideas are similar.

===We tell, tell===
A group of young guys occupies a university. Young people are fighters student revolution of the Sixties, and now that they have in hand the building, the guys begin to argue among themselves, bringing new ideas and changes. However, they do nothing but talk nonsense, not changing anything in society.

==Cast==
Discutiamo, discutiamo directed by Marco Bellocchio and Elda Tattoli
*Marco Bellocchio as Lecturer

Agonia directed by Bernardo Bertolucci
*Julian Beck as Dying Man Jim Anderson
*Judith Malina
*Giulio Cesare Castello as Priest
*Adriano Aprà as Clerk
*Fernaldo Di Giammatteo Petra Vogt
*Romano Costa as Clerk
*Milena Vukotic as Nurse

LAmore directed by Jean-Luc Godard
*Christine Guého
*Nino Castelnuovo
*Catherine Jourdan
*Paolo Pozzesi

Lindifferenza directed by Carlo Lizzani Tom Baker

La sequenza del fiore di carta directed by Pier Paolo Pasolini
*Ninetto Davoli as Riccetto
*Rochelle Barbini as The little girl
*Aldo Puglisi as Dio

==References==
 

== External links ==
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 

 
 