Bob the Butler
{{Infobox film
| name           = Bob the Butler
| image          =
| image size     =
| caption        = Bob the Butler movie poster
| director       = Gary Sinyor
| producer       = Esther Randall Gavin Wilding
| writer         = Jane Walker Wood Steven Manners Steven Manners Gary Sinyor
| narrator       =
| starring       = Tom Green Brooke Shields Genevieve Buechner Benjamin B. Smith Rob LaBelle Valerie Tian Simon Callow Iris Graham
| music          = David A. Hughes
| cinematography = Jason Lehel
| editing        = Richard Overall First Independent Pictures Vivendi Entertainment
| released       =  
| runtime        = 90 min
| country        = United States English
| budget         = $5,000,000
| gross          = $7,000,000 (DVD Sales)
}}

Bob the Butler is a 2005 family comedy film starring Tom Green. 

==Plot==
Bob, a man who cant hold a job, discovers an ad in the Yellow Pages for a butler school. Anne Jamieson (Brooke Shields), a single mother and neat freak, hires Bob as her butler.

==Cast==
*Tom Green - Bob Tree
*Brooke Shields - Anne Jamieson
*Genevieve Buechner - Tess Jamieson
*Benjamin Smith - Bates Jamieson
*Nicole Potvin - Morgan
*Rob LaBelle - Jacques
*Valerie Tian - Sophie
*Simon Callow - Mr. Butler
*Iris Graham - Mama Clara

==Production==
Bob the Butler was originally planned to be released in theaters October 2005 with a presumed PG-13 rating, but instead was aired on the Disney Channel on August 28, 2005, edited down to a PG rating. The film is now available on DVD. Langley City, Aldergrove and  Abbotsford, British Columbia|Abbotsford, British Columbia. Shortreed Elementary School was used as the school in the film.

==Music==
Tom Green performed the credits music "My Name is Bob", which was a track produced by Mike Simpson of The Dust Brothers. The background music for that track ended up being used for Greens 2005 single Teachers Suck from his rap album Prepare For Impact.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 