Darfur Now
{{Infobox Film
| name           = Darfur Now
| image          = Darfur now poster.jpg
| caption        = Darfur Now promotional poster
| director       = Ted Braun
| producer       = Cathy Schulman Don Cheadle Mark Jonathan Harris
| writer         = Ted Braun
| starring       = Don Cheadle Luis Moreno-Ocampo Adam Sterling Sheikh Ahmad Mohammed Abakar Hejewa Adam Pablo Recalde
| music          = Graeme Revell
| cinematography = Kirsten Johnson
| editing        = Edgar Burcksen Leonard Feinstein	
| distributor    = Warner Independent Pictures Participant Productions
| released       = November 2, 2007
| runtime        = 98 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} genocide in crisis in Darfur.

Darfur Now premiered at the 2007 Toronto International Film Festival.  The film was released in the United States and Canada on November 2, 2007.

==Summary==
Darfur Now follows the story of six individuals, who are tied together by the same cause: the crisis in Darfur. These individuals include Don Cheadle, an Oscar-nominated actor using his celebrity status to draw attention to the issue, as well as Adam Sterling, a 24-year-old waiter and activist urging Governor Schwarzenegger to sign a bill to keep California funds from investing in companies with interests in Sudan, and  ; Ahmed Mohammed Abakar, a displaced builder and farmer who now serves as leader and head sheikh of a camp of 47,000 other displaced Darfurians; and Pablo Recalde, leader of the World Food Program in West Darfur.

==See also==
*Eyes and Ears Of God – Video surveillance of Sudan, a 2012 documentary film
*Nuba Conversations, a 2000 documentary film
*Sri Lankas Killing Fields, a 2011 documentary film.

==References==
 

==External links==
*   at MySpace
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 


 