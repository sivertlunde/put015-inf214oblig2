Así es el tango
{{Infobox film
| name           = Así es el tango
| image          = Asíeseltango.jpg
| caption        = Screenshot
| director       = Eduardo Morera
| producer       = Adolfo Z. Wilson
| writer         = Florencio Chiarello (play) Eduardo Morera
| starring       = Tita Merello Tito Lusiardo
| music          = Juan Carlos Cobián
| cinematography = 
| editing        = 
| distributor    = Cinematográfica Terra
| released       = 1937
| runtime        = 105 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| followed_by    = 
}}
 Argentine romantic musical directed and written by Eduardo Morera, based on a play by Florencio Chiarello. Starring Tita Merello and Tito Lusiardo. The film is an Argentine tango film a hugely popular genre of the period and Argentine culture.

==Cast==
*Olinda Bozán
*Tito Lusiardo
*Luisa Vehil
*Tita Merello
*Fernando Ochóa
*José Ramírez (actor)|José Ramírez
*Eduardo Armani
*Olga Mom
*Lely Morel
*Carlos Enríquez

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 