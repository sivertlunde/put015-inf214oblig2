C Me Dance
{{Infobox film
| name           = C Me Dance
| image          = C Me Dance.jpg
| image_size     = 
| caption        = 
| director       = Greg Robbins
| producer       = 
| writer         = Greg Robbins
| starring       = Greg Robbins Christina DeMarco Laura Romeo Peter Kent
| music          = Stephen Tammearu
| cinematography = Robert J. Sommer
| editing        = David Kosor
| studio         = Uplifting Entertainment
| distributor    = Freestyle Releasing
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $500,000 
| gross          = 
}}
C Me Dance is a 2009 Christian film, written and directed by Greg Robbins. It was produced by Uplifting Entertainment, distributed by Freestyle Releasing, and was released on April 3, 2009.     It has been endorsed by the Leukemia & Lymphoma Society and The Dove Foundation.  It has had songs written by Lincoln Brewster, Eowyn, Stephanie Fraschetti and Terri Shamar. 

== Plot == blood disease. Sheri rebels against her father and God, causing her father to seek to keep his daughter close to God, to soften her heart, and to live out her dream before dying, all while he anguishes over losing his daughter so young. While praying to God to ask for strength and clear direction for her remaining days, God blesses Sheri so she is able to bring people to Christ. 

== Filming == Carlynton High School, the United Methodist Church of Carnegie, at Cefalos Restaurant and the Carnegie Performing Arts Center. 

== Release ==
On April 3, 2009, C Me Dance released in at least 150 theaters  in twelve U.S. states: Alabama, Arkansas, Florida, Georgia, Louisiana, Mississippi, North Carolina, Ohio, Oregon, Pennsylvania, South Carolina and Tennessee.   

=== Reception ===
The films reviews were mostly negative. Galen Holley of the Northeast Mississippi Daily Journal, said "C Me Dance is a darker, riskier movie that tends more toward mysticism than Christian social commentary," but did also say "  creatively ambitious and bold."  Luke Thompson of the L.A. Weekly review wrote that the film "plays like a fake Christian movie Troy McClure might end up starring in on an episode of The Simpsons, though it’s apparently for real".  Gary Goldstein of the Los Angeles Times was also negative towards the film, saying "its fine to know your audience and cater to its entertainment needs, but even the most devout viewer subjected to Robbins ham-fisted film might think, OK, now tell me something I dont already know.".  The film received a 0% from review aggregator Rotten Tomatoes based on five reviews.  The Huffington Post included the film in its article "The 9 Worst Movies Ever Made."  

== References ==
 

== External links ==
*  
*  
*  
*   at ChristianCinema.com
*   at The Dove Foundation
*  

 
 
 
 
 