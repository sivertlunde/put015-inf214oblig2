The Greatest Battle
{{Infobox film
 | name = The Greatest Battle
 | image =  The Greatest Battle.jpg
  | director = Umberto Lenzi
 | writer =
 | starring =  Henry Fonda John Huston Stacy Keach Samantha Eggar
 | music =    Franco Micalizzi
 | cinematography = 	Federico Zanni
 | editing =
 | producer = Dimension Pictures
 | released = 	7 February 1978
 | runtime =
 | awards =
 | country =
 | language =
 | budget =
 }} Italian title Italian Macaroni Macaroni war movie.

== Cast ==
* Helmut Berger: Lt. Kurt Zimmer
* Samantha Eggar: Annelise Hackermann
* Giuliano Gemma: Captain Malcolm Scott
* Henry Fonda: General Foster
* John Huston: Professor OHara
* Stacy Keach: Lt. Manfred Roland Ray Lovelock: John Foster
* Aldo Massasso: Jean, the reporter
* Guy Doleman: General Whitmore
* Venantino Venantini: Michael
* Ida Galli: Miss Scott
* Edwige Fenech: Danielle
* Andrea Bosic: Mimis Parnat
* Mirko Ellis: Captain Hans
* Orson Welles: Narrator (Voice)

==Reception==
Despite the great cast the movie generally received poor reviews. For Mick Martin and Marsha Porter it shows "lots of phony battle scenes, bad acting, and a poor script".   Leonard Maltin writes: "Amateurish muddle about WW2 combines tired vignettes with well-known stars, dubbed sequences with others, and newsreel footage narrated by Orson Welles. A waste of everybodys time". 

==References==
 

==External links==
* 

 

 
 
 
 
 


 