Ira & Abby
{{Infobox Film
| name           = Ira & Abby
| image          = Iraandabby.jpg
| image_size     =
| caption        = Robert Cary
| producer       = Brad Zions
| writer         = Jennifer Westfeldt
| narrator       =
| starring       = Chris Messina Jennifer Westfeldt Fred Willard Frances Conroy Jason Alexander Robert Klein Judith Light
| music          = Marcelo Zarvos
| cinematography = Harlan Bosmajian
| editing        = Phillip J. Bartell
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Robert Cary and released in the United States by Magnolia Pictures. The poignant love story stars Chris Messina and Jennifer Westfeldt (who also wrote the screenplay) in the title roles, and co-stars Fred Willard, Frances Conroy, Jason Alexander, Robert Klein and Judith Light.

==Summary==
The film follows the story of Ira Black, a 33-year-old psychology Ph.D. candidate with therapist parents.  Black has been in an on-and-off relationship with Leah for the past nine years and is, as he confides to his therapist, unsatisfied with their relationship on many levels.  It is implied that they two have the same conversation every time Black comes to the office.  His therapist then informs him that their 12 years of doctor-patient relationship must come to a close as therapy clearly isnt helping him.  The therapist encourages Black to be spontaneous, finish his dissertation, and do things he wouldnt normally do.

Ira goes to his favorite cafe and struggles (as usual) to order.  After flip-flopping several times, he finally orders.  While hes eating his meal, he looks across the street to see a gym and remembers that Leah found him overweight.  After his meal, he goes to the gym and makes an appointment for a tour with the irritating receptionist and waits 45 minutes before Abby arrives to show him around.

Initially irritated with the long wait, Ira is soon struck by Abbys ability to be involved in other peoples lives.  She seems to know everyone at the gym and be a trusted source of advice and a good listening ear.  She gives Ira a terrible tour, but they somehow hit it off and spend the next six hours talking in the unused yoga room.

Through their conversation, Ira discovers that Abby lives with her parents who are musicians.  She is a great believer in peoples goodness and always wants to help others.  At the end of their conversation, she proposes marriage to Ira, who is initially shocked.  After a bit though, he agrees and the two consummate their engagement in her office.

At the end of the day, both Ira and Abby return home to tell their parents of their engagement.  Iras parents are upset that its not Leah, and Abbys are extremely excited and begin to plan their daughters wedding right away.  Later that night, Ira and Abby talk about their future and make an agreement to have sex every day.

The two marry on the patio of Abbys familys brownstone in a small ceremony and spend the evening chatting inside.  When they attempt to return to Iras apartment to enjoy their wedding night, the car they hired breaks down.  Desperate to enjoy each other, the two take the subway from a bad neighborhood.  They are mugged at gunpoint on the way, and Abby steps up to face the mugger and offer him the money he needs.  Ira is scared at first, and then proud of his new wife.

Over the next weeks, Ira and Abby adjust to being married and enjoy shopping for their apartment.  They attend the movies and awkwardly run into Leah, which brings Iras worries about Abby to the front of his mind.  The two begin marriage counseling.  At the same time, Iras mother and Abbys father begin to have an affair after Iras mother begins a voice-over career.  When the families take a holiday picture (Abbys familys tradition), Ira learns that Abby was married twice before.  Angry that she didnt tell him, Ira asks for and gets an annulment. After returning to Dr. Friedman (his old therapist) and realizing how much he loves Abby, Ira proposes again, and the two remarry.

After the second wedding, with the two ex-husbands in attendance, things seem to get better.  One day, Abby meets Leah at the gym, and Leah confides in her that she misses her ex.  Not realizing that she is talking about Ira, Abby offers her advice &mdash; that she should get in contact with him for closure.  Not long after that, Abby goes to dinner with one of her ex-husbands.  Ira is extremely worried, so he takes Leah up on her offer for drinks.  The two go to her apartment and kiss.  Guilt-racked Ira returns home to find Abby sobbing on their couch, afraid that he was cheating and emotionally rattled from her dinner.  The next day, Leah and Abby meet for lunch and discuss their evenings.  When Ira walks in to meet Abby, the two women realize that he is the man they are both talking about.

Following this latest escapade, Abby pulls all of the therapists that she, her parents, Ira, and Iras parents have ever used into one room for a giant session.  After a while, Ira and Abby realize that they can put their differences aside and love each other.  The film ends with the two of them divorcing and vowing to love each other.

==Reviews==
*   - The New York Times
*   - The Boston Globe
*  
*  

==External links==
*  
*  
*  
*   at Apple.com

 
 
 
 
 
 