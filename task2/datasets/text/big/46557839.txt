Rocky (2008 film)
{{Infobox film
| name = Rocky
| image = 
| caption =
| director = S. K. Nagendra Urs
| writer = Vijay Chendur Yash  Bianca Desai   Santhosh 
| producer = K L Thimmappa Raju   Mekala Narayanaswamy
| music = Venkat - Narayan
| cinematography = S. R. Sudhakar
| editing = S. K. Nagendra Urs
| studio = T N Films
| released =  
| runtime = 152 minutes
| language = Kannada
| country = India
| budget =
}}
 romantic drama Yash and newcomer Bianca Desai in the lead roles along with Jai Jagadish, Ramesh Bhat and  Santhosh in other pivotal roles. 

The film featured original score and soundtrack composed by Venkat-Narayan. The film met with negative response with critics noting the screenplay and story to be following the lines of Mungaaru Male.  

== Cast == Yash as Rocky
* Bianca Desai as Usha
* Santhosh as Vishwas
* Jai Jagadish
* Ramesh Bhat
* Padmaja Rao
* Karibasavaiah
* Girija Lokesh
* M.N Lakshmi Devi
* Mithra


== Soundtrack ==
The music was composed by Venkat-Narayan and the audio was sold on Skanda Audio label. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Snehada Chiguru Priyadarshini
| lyrics1 = Panchajanya
| length1 = 
| title2 = Olla Ollare Olla
| extra2 = Kunal Ganjawala
| lyrics2 = M. S. Santhosh Kumar
| length2 = 
| title3 = 20-20 Balle Balle
| extra3 = Devi Sri Prasad, Panchajanya
| lyrics3 = Santhosh
| length3 = 
| title4 = Kareyale Ninna Hariharan
| lyrics4 = Jayant Kaikini
| length4 = 
| title5 = 1 2 Ring Ring Karthik
| Kaviraj
| length5 = 
| title6 = Manase Manase
| extra6 = Sonu Nigam
| lyrics6 = Harish Shringa
| length6 = 
| title7 = Kareyale Ninna
| extra7 = Narayan
| lyrics7 = Jayant Kaikini
| length7 =
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 

 