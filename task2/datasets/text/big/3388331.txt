Jana Aranya
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Jana Aranya (The Middleman)
| image          = Jana Aranya, 1976 film, poster.jpg
| director       = Satyajit Ray
| producer       = Indus Films (Subir Guha)
| writer         = Satyajit Ray (screenplay) Mani shankar Mukherjee (novel)
| starring       = Pradip Mukherjee Satya Banerjee Dipankar Dey Lily Chakravarti Gautam Chakravarti Aparna Sen Sudesna Das Utpal Dutt Rabi Ghosh
| cinematography = Soumendu Roy
| editing        = Dulal Dutta
| music          = Satyajit Ray
| distributor    =
| released       = February 20, 1976
| runtime        = 131 mins
| country        = India Bengali
| budget         =
}}
 1976 Bengali Bengali film directed by Satyajit Ray, based on the novel of the same name by Mani Shankar Mukherjee. It is the last among Rays Calcutta trilogy series, the previous two being, Pratidwandi (The Adversary, 1970) and Seemabaddha (Company Limited, 1971).

==Plot==

The film portrays the hopelessness of the middle-class, educated, urban youth in modern India.

After making numerous unsuccessful attempts to find a job Somnath (Pradip Mukherjee), the central protagonist, finally decides to start his own business as a middle-man&mdash;i.e. someone who supplies an order in exchange for a fixed commission.

In spite of his ambition, he soon however finds himself involved in petty dealings, which appear unsatisfactory to him. His friend Sukumar, having gone through similar ordeals but finally being unable to land a job, becomes a taxi-driver.

One day, Somnath finds that in order to land a big order, he must appease a client by supplying him with a prostitute. Despite his tremendous hesitation and after trying several brothels, Somnath finds a girl for the purpose. However, she turns out to be his friend Sukumars younger sister. Embarrassed and at a complete loss, Somnath offers her money and requests her to leave, but the girl refuses. Her purpose is to earn money, not beg, she tells him. Somnath delivers her to his client and lands the contract but suffers internally.

==Cast==
* Pradip Mukherjee - Somnath
* Kalyan Sen      - Mr. Bakshi
* Satya Bandyopadhyay - Somnaths Father
* Dipankar Dey	- Bhombol
* Arati Bhattacharya - Mrs. Ganguli
* Gautam Chakraborty - Sukumar
* Lily Chakravarty - Kamala
* Bimal Chatterjee - Adok
* Kalyan Chatterjee - Somnaths friend
* Bimal Deb - Jagabandhu
* Santosh Dutta	- Hiralal
* Utpal Dutt - Bishuda
* Rabi Ghosh - Natabar Mittir
* Soven Lahiri	- Goenka
* Padmadevi - Mrs. Biswas
* Aparna Sen - Somnaths ex-girlfriend
* Sudeshna Das - Kauna / Juthika

==Awards and nominations==
The film has been nominated for and won the following awards since its release:

==External links==
* 
* 

 

 
 
 
 
 
 
 