Cake (2014 film)
 
 
{{Infobox film
| name = Cake
| image = Cake poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Daniel Barnz
| producer = Ben Barnz Jennifer Aniston Kristin Hahn Courtney Solomon Mark Canton
| writer = Patrick Tobin
| starring = Jennifer Aniston Adriana Barraza Mamie Gummer Felicity Huffman William H. Macy Chris Messina Lucy Punch Britt Robertson Anna Kendrick Sam Worthington
| music = Christophe Beck
| cinematography = Rachel Morrison
| editing = Kristina Boden
| studio = After Dark Films Echo Films
| distributor = Cinelou Releasing, Freestyle Releasing    United States; wide}}
| runtime = 102 minutes  
| country = United States
| language = English
| budget = $7–10 million 
| gross = $2,683,301  
}}
Cake is a 2014 American drama film written by Patrick Tobin, directed by Daniel Barnz, and starring Jennifer Aniston, Adriana Barraza, Felicity Huffman, William H. Macy, Anna Kendrick, and Sam Worthington. It debuted in the Special Presentations section of the 2014 Toronto International Film Festival.    "Claire" is an acerbic but pleasant woman who has become embittered with her life after surviving a car accident, which killed her child and left her in chronic pain.  She becomes fascinated with Nina, a woman in her chronic pain support group, who recently committed suicide, by visiting her home and befriending her husband and son.  Together, Claire and Nina’s husband deal with their losses.

Anistons performance has earned nominations for the Screen Actors Guild Award and Golden Globe Award.

==Plot==
 
The movie begins with a group of women in a chronic pain support group who want closure after the suicide of one of their members, Nina (Anna Kendrick). Disgusted with Nina, Claire (Jennifer Aniston) responds with an insensitive detailing of the suicide that leaves the group repulsed, and she is asked to find another support group. 

Claire arrives at home and finds herself almost out of painkillers. After a restless sleep caused by the severe pain, she gets her house-maid Silvana (Adriana Barraza) to drive her to the doctor, where she is given a small prescription for Percocet and Oxycodone. At home, she tells Silvana to go home early and then has sex with the pool man, Arturo. Silvana goes home to her family where her daughter tells her to quit her job since Claire is a demanding boss and doesnt pay her; Silvana defends Claire.

In her bed, Claire tosses in pain. She hears a knock coming from the backyard. When she goes to observe, it is revealed to be the deceased Nina, lounging on an inflatable raft. They talk and Nina taunts her, asking why she doesn’t kill herself considering she is an atheist and doesnt believe in Heaven or Hell. Nina then holds Claire under the water. Claire wakes up from the nightmare. The next day, Claire is in aquatic therapy and being very difficult towards her therapist, Bonnie (Mamie Gummer). Bonnie suggests Claire finds someone else to work with. Later, when everyone is gone, Claire tries to kill herself, using weights to hold herself underneath the water, but she resurfaces.

Claire greets Annette (Felicity Huffman) at the support group office and she threatens to sue the group for discriminating against her unless Annette provides her with Nina’s address. Claire shows up at Nina’s house and meets her husband, Roy (Sam Worthington). She tells him that she used to live in the house and he gives her a tour, including his son’s room. As she’s leaving, Roy tells her that Annette had given him a heads up and he knows she’s from the support group.

Having run out of medicine, Claire asks Silvana to take her to Tijuana. When they arrive, they visit a pharmacy and purchase painkillers and a religious statue in which to smuggle the pills. When they get to the border checkpoint, the patrolman is suspicious of Claire for lying back in her seat (because of her pain) and they are pulled over to have their car searched. Claire calls her husband, who works for the government, for help. 

The next morning, Claire is waiting for Roy at his home. They go to a cemetery where he hangs a wind chime over Nina’s grave. Later they go to a bar and talk about his reaction to Nina’s death. He says in a support group, he had to speak to a partner as if she was Nina and he had told her, ‘thank you for ruining my life and my son’s life’ and that he hates her and hopes she burns in Hell.

Back at her place, Claire asks Roy to sleep with her, not in a sexual way but so neither has to be alone. When she wakes up in the morning he is gone. She takes a cab to Roy’s house and tells him she was worried he was going to kill himself, but he knows she just didnt want to be alone. Claire raids Nina’s medicine cabinet, which still has unused prescriptions in it. The next morning, a doorbell awakens Roy and Claire, he forgot his mother was dropping his son back home. The father and son stop by Nina’s grave and admire the new hanging chimes while Claire waits in the car, taking more of Nina’s medication.

The three of them go to a restaurant and Claire spies Nina in the back. But after confronting her, the vision quickly disappears and frustrated, she walks out of the restaurant. Roy shows up at Claire’s house, returning the purse she left behind. Even though Claire is asleep Silvana invites him in. When Claire wakes up, he shares the suicide note Nina left for him. It simply says “FORGIVE ME.” He tells Claire that Silvana told him about her accident and the death of her little boy. When Claire later talks to Silvana, she learns that Roy and his son have been invited for lunch in a few days.

At her aquatic therapy class, Claire apologizes to Bonnie for coming off as uncooperative and explains that she’s just in a lot of pain. Bonnie encourages her to make an attempt to get better. Claire then goes to the center where the support group is held and apologizes to Annette for blackmailing her, giving her a large bottle of vodka as a goodwill gesture.

Roy and his son come over to Claire’s house for the tamale lunch and the boy wants to go swimming, but hasnt brought his swimsuit. Claire suggests borrowing one of her son’s old bathing suits and goes into his room for the first time since his death. Silvana finds her struggling to open a taped-up box and ushers her out of the room. In the backyard, Roy tells Claire he’s left his support group and is going to go back to work next week. 

During lunch, they are interrupted when Leonard (William H. Macy), the man who caused the car accident that killed her son and caused all her pain, shows up at the front door and refuses to leave. Claire breaks down and tells him to get off her property before she calls the police. He tells her he can’t live with himself but she is unforgiving and attacks him. Roy and Silvana pull Claire off of the man.

Claire takes numerous pills. She starts hallucinating and having flashbacks of her accident. She overdoses, and ends up at the hospital, awakening to Silvana at her bedside, praying with rosary beads. As she goes through withdrawals and experiences hallucinations, Claire sees Nina entering the hospital with a cake. Nina reminds Claire that in the support group, they were asked what their dream would be if they didnt have chronic pain. Nina had replied that she’d want to make a birthday cake for her son from scratch. The dream ends with Nina throwing the cake out the window and then jumping herself. Claire wakes up screaming, pulling out her IV and asking for no more drugs.

After being released from the hospital, Claire asks Silvana to take her to Riverside. While watching a movie at the drive-in, Claire is in pain without her drugs and says she needs to get out of the car. She sneaks through a hole in the fence and sees Nina sitting on train tracks. Claire joins her, lying down with her head on a rail. As they both lay down on the tracks, Nina tells Claire that her last thoughts are important and that theyre all she gets to take with her. Claire acknowledges that she was a good mother, and Nina disappears. 

Silvana finds Claire on the tracks and, after they are back in the parking lot, she rants in Spanish about how frustrated she is with Claire. Silvana is interrupted when Claire points out their car has been stolen. Claire and Silvana take a cab to a motel and spend the night. The next morning a young runaway girl, Becky (Britt Robertson), tries to break into their rental car but runs into Claire, who was lying back in the passenger seat; they give her a ride. Claire then offers Becky $100 to make her a yellow cake with fudge frosting. When they get back to her place, she sets out to bake. There is a note on the kitchen counter from Claire’s husband, Jason (Chris Messina), that says “He belongs here.” She goes into the living room and sees that the picture she had taken down is hanging again. It is of Claire and her son in a happy moment. She cries but leaves the picture hanging, even calling her husband to thank him for the gift.

Silvana wakes Claire up after she falls asleep, informing her that the runaway girl has stolen her purse. Claire is not angry, saying "Shes just a kid." On the counter is the homemade cake. Claire delivers the cake to Roy’s house, along with a shark kite for his son’s birthday. Claire is driven to her son’s gravesite by Silvana. She puts wind chimes over his grave, just like Roy did at Nina’s. Back in the car, Claire has a sudden urge to try to sit upright. She changes her mind at first but as the film concludes, she pulls the lever and the chair returns to sitting position. Claire exhales deeply, content.

==Cast==
 
* Jennifer Aniston as Claire Bennett
* Adriana Barraza as Silvana
* Anna Kendrick as Nina Collins
* Sam Worthington as Roy Collins
* Mamie Gummer as Bonnie
* Felicity Huffman as Annette
* William H. Macy as Leonard
* Chris Messina as Jason Bennett
* Lucy Punch as Nurse Gayle
* Britt Robertson as Becky
* Paula Cale as Carol
* Ashley Crow as Stephanie
* Manuel Garcia-Rulfo as Arturo
* Camille Guaty as Tina
* Allen Maldonado as Buddy
* Camille Mana as Nurse Salazar
* Julio Oscar Mechoso as Dr. Mata
* Evan OToole as Casey Collins
* Pepe Serna as Nuncio
* Misty Upham as Liz
* Rose Abdoo as Innocencia
* Alma Martinez as Irma
 

==Production==
On February 10, 2014, it was announced that Jennifer Aniston would play the lead in Cake.    Daniel Barnz, the director, said "Of the zillions of Jennifer Aniston fans, I might be the biggest one of all. Ive especially loved her more dramatic performances, and I can’t wait to watch her tackle a role that has such a brilliantly funny voice and so much raw pain (hats off to writer Patrick Tobin). I’m honored to be collaborating with Ben, Kristin and Courtney, and it’s exciting that Cake will be the first film under the Cinelou banner. It feels like we’re all taking a leap of faith together, and that’s pretty thrilling.".  On March 15, Mexican actress Adriana Barraza is also announced in the cast of the drama.  The rest of the cast was revealed on April 1. 

Principal photography, which took place in Los Angeles, began April 3, 2014  and ended May 6. 

==Release== select theatres general release on January 23, 2015 by Freestyle Releasing.  

===Home Media===
The film was released on DVD & Blu-Ray April 21, 2015.

==Reception==
Cake received mixed reviews from critics. On Rotten Tomatoes, the film holds a rating of 49%,  based on 104 reviews, with an average rating of 5.8/10. The sites critical consensus reads, "Cake finds Jennifer Aniston making the most of an overdue opportunity to test her dramatic chops, but it lacks sufficient depth or warmth to recommend for all but her most ardent fans."  On Metacritic, the film has a score of 49 out of 100, based on 34 critics, indicating "mixed or average reviews". 
 Toronto premiere, Deadline described Clayton Davis Oscar scene. There is no massive crying fit. Its a complete performance from beginning to end and she deserves the appropriate accolades for it."   Of Anistons performance, David Nusair of Reel Film Reviews wrote "...the actress steps into the shoes of her thoroughly damaged character to an often revelatory extent."  Sheri Linden of the Los Angeles Times also spoke positively of Anistons performance, writing "Aniston lends the role an impressively agonized physicality and brings ace timing to the screenplays welcome gallows humor." 

===Accolades===
 {| class="wikitable sortable"
|+ Awards
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| 2014 People Magazine Awards People Magazine Movie Performance of the Year - Actress Jennifer Aniston
| 
|- 2015
|Capri-Hollywood Film Festival Best Actress
| 
|- Santa Barbara International Film Festival The Montecito Award
| 
|-
|Critics Choice Movie Awards Best Actress
| 
|- 72nd Golden Golden Globe Awards Golden Globe Best Actress - Motion Picture Drama
| 
|- 21st Screen Screen Actors Guild Awards Screen Actors Outstanding Performance by a Female Actor in a Leading Role
| 
|- Casting Society of America
|Low-Budget Drama Mary Vernieu, Lindsay Graham
| 
|-
|}

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 