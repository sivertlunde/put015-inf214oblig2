Cal (2013 film)
{{Infobox film
| name           = Cal
| image          = CalFilm2013.jpg
| image_size     = 215px
| alt            = 
| caption        = DVD cover
| director       = Christian Martin
| producer       = Christian Martin Bernie Hodges
| writer         = Christian Martin
| starring       = Wayne Virgo Tom Payne Emily Corcoran Lucy Russell
| music          = Cliff Airey Rob Dunstone
| cinematography = Jack ODowd
| editing        = Jack ODowd

| studio         =   Bonne idée Productions
| distributor    = TLA Releasing
| released       =  
| runtime        = 89 minutes
| country        = Bristol, United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

 Cal  is a 2013 British-released drama film starring Wayne Virgo, Tom Payne, Emily Corcoran, Lucy Russell.
The film was written and produced by Christian Martin and Bernie Hodges and directed by Christian Martin.   

==Plot==
This film is a sequel to Shank (2009 film)|Shank were troubled teen Cal who was once involved in the gang life who now left and fled his home town Bristol to start living a new life. After Cals mother falls ill in the hospital he returns to Britain. Cal finds that as many other places he has visited in Europe his hometown has suffered from an economic collapse as well. 

==DVD Release==
The Cal DVD came out on 9 September 2013 in the UK. 

==Filming Locations==
* An industrial Estate near Pennywell Road, Easton, Bristol
* A now demolished housing estate, Southmead, Bristol
* Trenchard Street Car Park, Trenchard Street, Bristol
* Skull graffiti building - Ninetree Hill, Cotham, Bristol
* Laundrette - Cotham Road South, Cotham, Bristol
* Blackberry Hill Hospital, Fishponds, Bristol
* Montpelier Park, Montpelier, Bristol
* Brandon Hill, Centre, Bristol
* St Pauls area, Bristol

==References==
 

 
 
 


 