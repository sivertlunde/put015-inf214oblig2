Four of the Apocalypse
{{Infobox film
| name           = Four of the Apocalypse
| image          = Four of the Apocalypse.jpg
| caption        = Original Italian theatrical poster
| director       = Lucio Fulci
| producer       = Edmondo Amati  (uncredited) 
| writer         = Ennio Di Concini
| story          = Bret Harte  (as Francis Bret Harte) 
| narrator       = Edward Mannix Harry Baird 
| music          = Franco Bixio Fabio Frizzi Vince Tempera
| cinematography = Sergio Salvati
| editing        = Ornella Micheli
| distributor    = Anchor Bay Entertainment
| released       = 12 August 1975 
| runtime        = 104 min.
| country        = Italy
| language       = Italian
}}
 western writer Bret Harte, "The Luck of Roaring Camp" and "The Outcasts of Poker Flat". It was filmed in Spain, Italy and Austria.

== Plot ==

The sole survivors from a vigilante attack on a wild west town, gambler Stubby Preston, pregnant prostitute Bunny, and alcoholics Clem and Bud, who see ghosts, strike out for the next town. On the trail, the four draw the attention of Chaco, a bandit gunman. At first, Chaco is cautiously accepted into the travelling party, but then he poisons the group with hallucinogens and leaves them to die. The survivors then find a mining town, where Bunny goes into childbirth|labor. After tragedy befalls Bunny, Stubby seeks his revenge against Chaco.

== Cast ==

* Fabio Testi as Stubby Preston
* Lynne Frederick as Emanuelle Bunny ONeill
* Michael J. Pollard as Clem Harry Baird as Butt/Buck/Bud Wilson
* Tomas Milian as Chaco Donal OBrien as Sheriff of Salt Flat
* Adolfo Lastretti as Reverend Sullivan
* Bruno Corazzari as Lemmy
* Giorgio Trestini as doctor
* Charles Borromel as Montana
* Salvatore Puntillo as recovering man
* Lone Ferk as Mormon girl
* Lorenzo Robledo as tortured sheriff
* Edward Mannix as narrator
* Claudio Ruffini as gunman
* Goffredo Unger as gambler
* Alfonso Rojas

== Soundtrack ==

The soundtrack was written by Fabio Bixio, Fabio Frizzi and Vince Tempera.   

# "Movin On" (3:39) (Sung by Greennfield Cook and Benjamin Franklin Group)
# "Deaths Song" (2:35)
# "Serene Moments" (2:59)
# "Bunny (Lets Stay Together)" (3:54) (Sung by Greennfield Cook and Benjamin Franklin Group)
# "Farewell to the Friends" (2:12)
# "On the Traces of Chaco" (1:26)
# "Slow Violence" (2:11)
# "Was It All in the Vain" (2:33) (Sung by Greennfield Cook and Benjamin Franklin Group)
# "The Swindler of the Saloon" (2:54)
# "Let Us Pray" (0:56) (Sung by Greennfield Cook and Benjamin Franklin Group)
# "Chaco" (1:51)
# "Stubby (Youre Down and Out)" (3:12) (Sung by Greennfield Cook and Benjamin Franklin Group)
# "Bunny Love Song" (1:21)
# "Reawaken from the Trip" (1:32)
# "I Quattro Dell Apocalise (Outtakes Suite)" (21:47)

== Critical reception ==

AllRovi|Allmovies review of the film was positive, calling it "what could very well be the Italian splatter-masters most personal, poignant, and compelling film – not to mention one of the most original spaghetti Westerns ever filmed." 

== Differences from short stories ==

Testis character, Stubby, is the "John Oakhurst" character from "Poker Flats". Lynne Fredericks "Bunny" is named "Duchess" in the original story. The outlaw Chaco was not in the book, and the action in the film is considerably more violent than in the stories. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 