Blood of Dracula's Castle
{{Infobox film
| name           = Blood of Draculas Castle
| image          = Bloodofdraculacastle.jpg
| caption        =
| director       = Al Adamson, Jean Hewitt
| producer       = Martin B. Cohen, Samuel M. Sherman, Rex Carlton, Al Adamson
| writer         = Rex Carlton
| starring       = John Carradine Alex DArcy Paula Raymond Robert Dix Ray Young Vicki Volante John Cardos Kent Osborne Gene OShane Barbara Bishop
| music          =
| cinematography = László Kovács (cinematographer)|László Kovács
| editing        = Peter Perry Jr.
| distributor    = Crown International Pictures  (theatrical, USA) 
| released       =  
| runtime        = 84 min
| country        = United States English
| budget         =
}}
 horror cult cult B-movie directed by Al Adamson and released by exploitation film specialists Crown International Pictures.

==Plot==
Count Dracula (Alexander DArcy) and his vampire wife (Paula Raymond), hiding behind the pseudonyms of Count and Countess Townsend, lure girls to their castle in the Arizona desert to be drained of blood by their butler George (John Carradine), who then mixes real bloody marys for the couple. Then the real owners of the castle show up, along with Johnny, who is a serial killer or a werewolf depending on which version you watch. The owners refuse to sell, so Dracula wants to force them to sell.  In a final confrontation, the vampires are forced to stand in the sunlight and dissolve.

The role of Countess Townsend was originally intended for Jayne Mansfield, but she died in a car accident before shooting began.  A sequel, to be called Draculas Coffin, was planned but never materialized.

Ostensibly located in Arizona, the film was actually shot at  ) 

==Production==
Jayne Mansfield was originally set to star in the film as Countess Townsend. A proposed sequel titled Draculas Coffin was announced but never filmed. 


==Cast==

* John Carradine: George, the butler 
* Paula Raymond: Countess Townsend 
* Alexander DArcy: Count Dracula, alias Count Charles Townsend 
* Robert Dix: Johnny 
* Gene Otis Shayne: Glen Cannon (as Gene OShane) 
* Jennifer Bishop: Liz Arden (as Barbara Bishop) 
* Vicki Volante: Ann, motorist-victim 
* Ray Young: Mango 
* John Bud Cardos:(as John Cardos) 
* Ken Osborne		 
* Joyce King: Body in Water

==References==
 

==External links==
*  
*  
 
 

 
 
 
 
 
 
 
 
 

 