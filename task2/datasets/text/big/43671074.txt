Roman Tales (film)
{{Infobox film
 | name =Roman Tales
 | image =  Roman Tales (film).jpg
 | caption =
 | director = Gianni Franciolini
 | writer = Sergio Amidei  Age & Scarpelli  Alberto Moravia  Francesco Rosi
 | story = Alberto Moravia (tales) 
 | starring =  Maurizio Arena Franco Fabrizi Totò Vittorio De Sica Silvana Pampanini
 | music =  Mario Nascimbene
 | cinematography = Mario Montuori
 | editing =  Adriana Novelli
 | producer = Niccolò Theodoli
 | distributor = Daria Cinematografica
 | released =1955
 | runtime = 95 min
 | awards =
 | country =Italy
 | language =   Italian
 | budget =
 }} 1955 Cinema Italian comedy David di Donatello Awards, for best director and best producer.   

== Plot ==
In Rome, during the Fifties, three boys attempt to commit a robbery. Theyre Mario, Alvaro and Otello, which make use of the aid of a trickster, Professor Semprini, who claims to be a great intellectual. But the man is the garbage boy of the lawyer Mazzoni Baralla, upright man, who goes on the trail of the three boys as soon as they attempt the shot. Indeed Alvaro, Mario and Otello are arrested, after being deceived by Semprini, who pretends from them a payment for the design of the plan. In fact the three first attempt to pass off counterfeit notes, then pretend guards vice squads team in Villa Borghese. After the arrest and exoneration, the three decide to return to their old and simple jobs.

== Cast ==

* Totò: Professor Semprini
* Vittorio De Sica: Lawyer Mazzoni Baralla
* Silvana Pampanini: Maria
* Franco Fabrizi: Alvaro
* Antonio Cifariello: Otello
* Giancarlo Costa: Spartaco
* Maurizio Arena: Mario
* Sergio Raimondi: Valerio Zerboni
* Nando Bruno: Amilcare
* Mario Riva: The Waiter
* Mario Carotenuto: The "Commendatore"
* Eloisa Cianni: Iris
* Giovanna Ralli: Marcella
* Maria Pia Casilio: Annita
* Anita Durante:  Alvaros Mother
* Turi Pandolfini: Client of the Barber 
* Aldo Giuffrè: Lawyer

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 