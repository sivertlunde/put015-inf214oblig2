The Day the Earth Caught Fire
 
 
{{Infobox film
| name           = The Day the Earth Caught Fire 175px
| caption        = Theatrical release poster
| director       = Val Guest
| producer       = Val Guest  Frank Sherwin Green
| writer         = Wolf Mankowitz  Val Guest
| starring       = Janet Munro Leo McKern Edward Judd
| music          = Stanley Black Monty Norman
| cinematography = Harry Waxman
| editing        = Bill Lenny studio = Val Guest Productions
| distributor    = British Lion Films (UK) Universal International Pictures (USA)
| released       =  
| runtime        = 98 minutes budget = £200,000 Tom Weaver, "Val Guest", Double Feature Creature Attack: A Monster Merger of Two More Volumes of Classic Interviews McFarland, 2003 p 114 
| country        = United Kingdom
}}
The Day the Earth Caught Fire is a British science fiction disaster film starring Edward Judd, Leo McKern and Janet Munro. {{Cite book
 | author=Dubeck, Leroy W.; Moshier, Suzanne E.; Boss, Judith E. | title=Fantastic voyages: learning science through science fiction films
 | edition=2nd | publisher=Springer | year=2004
 | isbn=0-387-00440-8 | page=254 }}  It was directed by Val Guest and released in 1961 in film|1961, and is one of the classic apocalyptic films of its era.    The film opened at the Odeon Marble Arch in London on 23 November 1961.
 Daily Express Building in Fleet Street, London.

==Plot==
 
A lone man walks through the sweltering streets of a deserted London. The film then goes back several months. Peter Stenning (Edward Judd|Judd) was an up-and-coming journalist with the Daily Express but a messy divorce has thrown his life into disarray. His Editor (Arthur Christiansen|Christiansen) has begun giving him lousy assignments. He begins drinking too much. (One of his lines is, "Alcoholics of the press, unite!"    ) Stennings only friend, Bill Maguire (Leo McKern|McKern), is a veteran Fleet Street reporter who offers him encouragement and occasionally covers for him by writing his copy.
 meteorological events British Met Office to get data mean temperatures. While there he meets Jeanie (Janet Munro|Munro), a young telephonist. They "meet cute", trading insults; later, they fall in love.
 tilt of the Earth  has been changed by 11 degrees, altering the climatic zones and changing the pole and the equator. The increasing heat has caused water to evaporate and mists to cover Britain. Eventually it is shown how the whole orbit of the Earth has been pushed towards the sun.

The government imposes a state of emergency, and starts rationing supplies. People start evacuating the cities. Scientists conclude that the only way to bring the Earth back into a safer orbit is to detonate a series of nuclear bombs in western Siberia. Stenning, Maguire and Jeanie gather at a bar to await the outcome. As the countdown reaches zero the bombs are detonated; 30 seconds later the shock wave travels round the world, causing dust to fall from the bars ceiling. Two versions of the newspapers front page have been prepared: one reads "World Saved", the other "World Doomed". Stenning, because he broke the story, dictates the day’s editorial, still without any indication of whether the nuclear blasts have been successful or not. In the meantime, a few remaining printers wait by the presses for the word of which headline will be used.

The film concludes ambiguously with the sound of pealing church bells. The audience is left to decide whether this heralds a new beginning for mankind or its doom.

==Cast==
 
 
* Edward Judd as Peter Stenning
* Leo McKern as Bill Maguire
* Janet Munro as Jeannie Craig
* Michael Goodliffe as Jacko, Night editor
* Bernard Braden as News editor
* Reginald Beckwith as Harry
* Gene Anderson as May
* Renée Asherson as Angela
* Arthur Christiansen as Jeff Jefferson the Editor
* Austin Trevor as Sir John Kelly
* Edward Underdown as Sanderson
* Ian Ellis as Michael Stenning
  John Barron as 1st Sub-Editor (uncredited)
* Timothy Bateson as printer (uncredited)
* Peter Blythe as Copy Desk (uncredited)
* Peter Butterworth as 2nd Sub-Editor (uncredited)
* Norman Chappell as hotel receptionist (uncredited)
* Geoffrey Chater as Pat Holroyd (uncredited)
* Pamela Green as nurse (uncredited)
* Carmel McSharry as woman lost in the fog (uncredited) George Merritt as Smudge (uncredited) Charles Morgan as Foreign Editor (uncredited)
* Marianne Stone as Miss Evans, Jeffs Secretary (uncredited)
 

===Notes===
Arthur Christiansen, a former editor of the Daily Express, played himself as the editor of the newspaper. Three years before Zulu (1964 film)|Zulu, a then-unknown Michael Caine played an uncredited police officer diverting traffic. 

==Production== British Lion to finance it by putting up his profits from Expresso Bongo as collateral. All the finance was British. 
 anamorphic lenses using the French Dyaliscope process.

Critic Doug Cummings said, about the look of the film, "Guest also manages some visual flair. The film was shot in anamorphic widescreen, and the extended frame is always perfectly balanced with groups of people, city vistas, or detailed settings, whether bustling newsrooms, congested streets, or humid apartments. Although the films special effects arent particularly noteworthy, matte paintings and the incorporation of real London locations work to good atmospheric advantage (heavy rains buffet the windows; thick, unexpected fog wafts through the city; a raging hurricane crashes into the British coast). Guest also cleverly incorporates stock footage to depict floods and meteorological disasters worldwide. The visual style of the film is straightforward and classical, but each scene is rendered with a great degree of realism and sense of place."   

Reviewer Paul A. Green wrote, "Guest and his editor Bill Lenny worked with archive footage. Theres a quick shot of a fire-engine from The Quatermass Experiment - but otherwise you cant see the joins." 
 Anchor Bay The War of the Worlds, which ends with the joyous ringing of church bells after the emergency (and a nuclear explosion). But Guest maintained that his intention was to always have an ambiguous ending.

Monty Norman, who was credited with writing "Beatnik Music" in a couple of scenes, would become well known one year later when his "James Bond theme" was used in the title sequence of Dr. No (film)|Dr. No.

==Certification==
The film was rated "X" (over 16s only admitted) by the British Board of Film Censors on its initial release. The current BBFC DVD/Blu-ray certificate is "12".

===Locations=== HM Treasury Building in Westminster and on Palace Pier, Brighton.

== Themes ==
Essayist Paul A. Green discusses many of the themes in the film in his review: 
 Gutenberg technology, Murdoch introduced computerised newsrooms, smashed the print unions and moved operations to Docklands, eventually dragging the rest of Fleet Street with him."

*Nuclear weapons testing - "Then the premise of the film - that nuclear tests alter the earths orbit, disrupt the climate and send the planet spiralling towards the sun - makes a deeper impact... Global destruction through nuclear war is becoming an existential reality... Nuclear holocaust anxieties in the movies were not new, of course. But these fears were usually externalised as monster mutation narratives..."

*Escapism - "Everyones keeping busy except boozy Stenning, who clearly resents being tasked to write a lightweight piece about sun-spots, when he used to be the papers hotshot columnist with serious ambitions as a writer. Hed rather be in Harrys Bar, a cosy all-day drinking club modelled on Fleet Streets El Vinos."

*Social class - "Stennings discontent is not explicitly political, in any specific ideological sense... But theres the same restlessness about the restrictions of class. Stenning voices a distrust of traditional upper-crust Anglo-Saxon attitudes that parallels the increasingly awkward questions the narrative raises about the inertia of the British Establishment, as well as the mood of a Britain on the edge of social change. "You ought to see the way theyre bringing him up, Bill. Itll be the right prep school next. And then the right boarding school. And by the time they finish with him, hell be a right bowler-hatted, whos-for-tennis, toffee-nosed gent, but he wont be MY son...."
 Gender politics - "This encounter with Jeanie signals the beginning of Stennings slow transformation. It also exemplifies the transformation of gender politics in UK bureaucracy since 1961. Today a bright woman like Jeannie would probably be running the whole department rather than servicing a duplicating machine, which is where Stenning discovers her. "Im not women!" she informs Stenning, when he makes one of his bar-room generalisations."
 End of Dunkirk spirit in the face of a new threat. Theres a hint, voiced by Maguire earlier, that "weve gone soft" and that under these new and even more extreme circumstances, social cohesion might unravel and give way to hysteria."

==Reception==

===Critical response===
The film holds an 86% "Certified Fresh" rating on the review aggregate website Rotten Tomatoes.  Critic Doug Cummings called it "an unusually literate and thematically nuanced genre film," adding, "The disaster genre is not generally known for its insights into characters or its clever dialogue, but The Day the Earth Caught Fire is an admirable exception. Its attention to the inner and outer lives of its protagonists makes its physical doom an externalized metaphor for Stennings personal life, off-kilter and spinning out of control, both fates equally weighted between hope and despair." 

Reviewer Dennis Schwartz wrote, "An intelligent low-budget sci-fi doomsday pic that gives us an authentic Fleet Street look at an old-fashioned newspaper office back in the day and a suspenseful scenario of the world tinkering on destruction as seen through the eyes of the newspaper. Val Guest ... efficiently directs by making good use of the atmospheric effects such as the extreme heat and mist on Londoners, which gives this fascinating story an eerie feel. Guest and Wolf Mankowitz write a taut screenplay, with an observant look at the London scene." 

Paul Green, cited above, wrote in a 2005 commentary, "London is on the cusp of the sixties, where protest and youth cultures are breaking through, but social and sexual mores are still semi-formalised and girls work in typing pools... In a contemporary context of global warming, asymmetric warfare, nuclear proliferation and dwindling resources, the films underlying optimism seems touching." 
 The film’s Bill McGuire, emeritus professor of geophysical and climate hazards at University College London. The latter, real McGuire, is also a science writer. His latest book, Waking the Giant, provides a factual, scientific case for how global warming can even trigger earthquakes, tsunamis and volcanoes. Who needs science fiction with reality like this? {{cite news 
|title=  A doomed Earth of science fiction may well become a reality
|author=   Andrew Simms	
|url=  http://www.theguardian.com/environment/blog/2014/sep/01/doomed-earth-science-fiction-climate-reality
|format= blog
 |newspaper=  The Guardian
|date= 1 September 2014
|accessdate=  5 September  2014
}} 
|}}

===Awards===
Val Guest and Wolf Mankowitz received the 1962 BAFTA for Best Film Screenplay for The Day the Earth Caught Fire. 

==See also==
* List of apocalyptic films
* List of nuclear holocaust fiction

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 