In Old Monterey
{{Infobox film
| name           = In Old Monterey
| image          = In_Old_Monterey_Poster.jpg
| caption        = Theatrical release poster
| director       = Joseph Kane
| producer       = Armand Schaefer (associate)
| screenplay     = {{Plainlist|
* Gerald Geraghty
* Dorrell McGowan
* Stuart E. McGowan
}}
| story          = {{Plainlist|
* Gerald Geraghty
* George Sherman
}}
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* June Storey
}}
| music          = Raoul Kraushaar (musical director) Ernest Miller
| editing        = Edward Mann
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 54 minutes
| country        = United States
| language       = English
}} Western film directed by Joseph Kane and starring Gene Autry, Smiley Burnette, and June Storey. Based on a story by Gerald Geraghty and George Sherman, the film is about an army sergent and former rancher who runs into opposition from local ranchers when the United States Army sends him to purchase their ranch land needed for a strategic air base.   

==Plot==
The United States Army sends Sergeant Gene Autry (Gene Autry), a rancher in civilian life, to negotiate the purchase of ranch land out West needed to build a strategic air base. Local ranchers, led by Gabby Whittaker (George "Gabby" Hayes), refuse to sell their land to the government.

After some investigation, Gene suspects that the ranchers refusal to sell their land may be influenced by borax mine magnate Stevenson (Jonathan Hale), who wants more money than the government is offering for his Atlas Borax Mine. To prevent ranchers from selling their land to the army, Stevenson and his men embark upon a plan of sabotage that makes it appear that the army fliers are employing ruthless tactics to force the ranchers into selling.

Influenced by Stevensons covert agitation, the ranchers are driven to armed resistance when Gabbys nephew Jimmy (Billy Lee) is killed in an explosion caused by Stevenson. As the ranchers prepare to barricade the town, Gene rides to the rescue with proof that Stevenson was responsible for the explosion and the sabotage. Angered by the treachery, the ranchers, led by Gene in an armored truck, ride to the Atlas Borax Mine and bring Stevenson and his gang to justice.

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse
* June Storey as Jill Whittaker
* George "Gabby" Hayes as Gabby Whittaker
* The Hoosier Hot Shots as Soldier Musicians
* Sarie and Sallie as Themselves
* The Ranch Boys as Singing Cowhands
* Stuart Hamblen as Bugler
* Billy Lee as Jimmy Whittaker
* Jonathan Hale as Stevenson, Owner of the Atlas Borax Mine
* Robert Warwick as Major Clifford V. Forbes
* William Hall as Gilman, Mine Foreman
* Eddie Conrad as Club Marine Owner Champion as Genes Horse (uncredited)   

==Production==
===Stuntwork=== Tom Steele
* Bill Yrigoyen (stunt double for Gene Autry) 

===Filming locations===
* Alabama Hills, Lone Pine, California, USA   

===Soundtrack===
* "It Happened in Monterey" (Mabel Wayne, Billy Rose) by Gene Autry during the opening credits and at the end
* "Born in the Saddle" (Gene Autry, Johnny Marvin) by Gene Autry My Buddy" (Walter Donaldson, Gus Kahn) by Gene Autry and The Hoosier Hotshots
* "It Looks Like Rain" by Smiley Burnette and The Hoosier Hotshots
* "Tumbling Tumbleweeds" (Bob Nolan) by Gene Autry and The Ranch Boys
* "Little Pardner" (Gene Autry, Johnny Marvin, Fred Rose) by Gene Autry
* "Virginia Blues" (Fred Meinken, Ernie Erdman) by The Hoosier Hotshots
* "Columbia, the Gem of the Ocean" (David T. Shaw) by Gene Autry, George Gabby Hayes and Townsmen
* "The Vacant Chair (We Shall Meet But We Shall Miss Him)" (George Frederick Root, Henry S. Washburn) by Gene Autry and June Storey in church, with organ accompaniment
* "Colors" by a bugler when the flag is lowered
* "Taps" (Daniel Butterfield) by a bugler for lights out
* "Youre in the Army Now (song)|Youre in the Army Now" (Traditional) variations played often in the score   

==Memorable quotes==
* Sergeant Gene Autry: We need places where our men can train and our equipment can be tested over and over again until every flaw can be found and corrected. And I dont believe any of you are such poor Americans that you wont be proud to do your part.

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 