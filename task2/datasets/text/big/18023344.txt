The Rose Tattoo (film)
{{Infobox film
| name           = The Rose Tattoo
| image          = The Rose Tattoo (1955 film poster).jpg
| caption        = Original film poster
| director       = Daniel Mann
| producer       = Hal B. Wallis
| writer         =  
| starring       = {{unbulleted list 
| Anna Magnani 
| Burt Lancaster 
}}
| music          = Alex North
| cinematography = James Wong Howe
| editing        = Warren Low
| distributor    = Paramount Pictures
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English, Italian
| budget         = 
| gross = $4.2 million (US) 
}} 1955 film play of Italian Anna Broadway in 1951, but she rejected the offer because of her difficulty with the English language at the time. By the time of this film adaptation, she was ready. 

Anna Magnani won the Academy Award for Best Actress for her performance, and it also won Best Art Best Cinematography Best Picture and Best Supporting actress for Pavan.

== Plot ==
Serafina Delle Rose (Anna Magnani) proudly praises her husband Rosario (David Newell) to her female neighbors in a shopping market, before revealing that she is pregnant with their second child. Returning home she finds Rosario asleep in bed and whispers to him that she is with child. Emerging from his room she finds a young woman named Estelle (Virginia Grey) at the door who wants her to make a shirt for her lover from some expensive silk material. It transpires that Rosario is her lover as, when Serafina is out of the room, she steals a photograph of him from Serafinas sideboard before departing.

Later that night Rosario is out working (he is a van driver who hauls bananas) and Serafina is busy working on the shirt for her customer. The women of the neighborhood discover that Rosario has been killed in a fiery road accident whilst trying to speed away from the police; he was smuggling something illegal under the bananas. When Serafina discovers her beloved husbands demise she collapses, and later the local doctor informs her daughter Rosa (Marisa Pavan) and the women of the neighborhood that Serafina has miscarried.

Three years later Serafina is a recluse, having withdrawn from the world. She has allowed her appearance to deteriorate. It is the day of the high school graduation and the women of the neighborhood are impatiently banging on Serafinas front door for their daughters graduation dresses, which she has sewn. Rosa is also begging for her graduation dress from her mother, but Serafina has locked them—along with the rest of Rosas clothes—away. Rosas teacher turns up in the neighborhood demanding to know what is keeping them. She goes to the Delle Rose household and eventually makes Serafina see sense and she hands over the key.

After much thought Serafina decides to attend her daughters graduation. During this time two women arrive asking if Serafina can quickly mend their bandanas for a festival to which they are going. Serafina reluctantly does so, but is appalled by their talk of men and reprimands them. One of the women, Bessie (Jo Van Fleet), takes offense and informs Serafina that her late husband was having an affair. Serafina does not go to the graduation ceremony after this revelation, but instead sits alone in the dark until Rosa comes home. She is infuriated when Rosa introduces her to her new sailor boyfriend Jack Hunter (Ben Cooper) and Serafina mistrusts his intensions. After her interrogation, he confides that he is a virgin himself.  She forces him to vow before a statue of the Virgin Mary that he will respect Rosas innocence. Then Rosa and Jack are picked up by a car of friends for a party.

Serafina is still stinging from the revelation the her husband was unfaithful.  On a mission to root out the truth, she heads to the church (where a bazaar is being held) to ask the priest if her husband had confessed to an affair with another woman.  He refuses to answer and she attacks him.  A truck driver named Alvaro (Burt Lancaster) pulls her off the priest, but she tears Alvaros shirtfront in her attack.  Serafina then collapses; she is thrown out of the church, and is mocked by her female neighbors.  Alvaro drives the dazed Serafina home in his banana truck. She offers to repair his torn shirt. At her home Alvaro acts the clown, and flatters Serafina with compliments, brashly confiding his life story to her.  Some wine is consumed and she begins to talk of her late husband, who she still idolizes.  She mentions that he used to have a rose tattooed on his chest. Serafina loans Alvaro the rose silk shirt that she had sewn the night of her husbands death until she is able to repair the shirt she tore.  They agree to meet later that night.  Alvaro returns, having impulsively gotten a rose tattooed on his chest. Serafina is disgusted and goes to throw him out, stating that he is dishonoring Rosarios memory. He awkwardly blunders at her; she attempts to throw him out. Reconsidering, Serafina demands that Alvaro  drive her to a club her husband used to attend. Once there she meets Estelle who asked her to make a silk shirt for her lover and guesses correctly that she was having an affair with him. Estelle confesses that she was having an affair with Rosario before proceeding to show Serafina the rose tattooed on her chest as a symbol of her love for Rosario. Returning home, Serafina smashes the urn containing Rosarios ashes.  She invites Alvaro to return in the night.

Alvaro turns up hours later at the Delle Rose household intoxicated and, mortified by her actions, Serafina leaves him in a drunken stupor and retires to bed. That night Rosa returns home and falls asleep on the sofa. As Alvaro awakes from his drunken stupor he sees Serafinas beautiful daughter lying there asleep and moves over to kiss her. As he does Rosa wakes up and screams, before confronting her mother as to why there is a strange man in the house.

Serafina gets rid of him, but the following morning she finds him on top of a boat mast outside her house begging for her forgiveness. Serafina and Rosa are extremely embarrassed and Serafina refuses to leave the house in order to make him come down, much to Rosas frustration. At that moment Jack arrives and asks Serafina if he can marry Rosa. Serafina is stunned, but seeing that this is what Rosa wants she gives them her consent and they leave to be married. Serafina then calls Alvaro down from the telephone pole before declaring in front of her neighbors that they have to pick up from where they left off the night before. As they enter the house, Serafina puts on the radio whilst Alvaro pours them a drink and they laugh together.

== Cast ==
* Anna Magnani – Serafina Delle Rose
* Burt Lancaster – Alvaro Mangiacavallo
* Marisa Pavan – Rosa Delle Rose
* Ben Cooper – Seaman Jack Hunter
* Virginia Grey – Estelle Hohengarten
* Jo Van Fleet – Bessie
* Sandro Giglio – Father De Leo
* Mimi Aguglia – Assunta
* Florence Sundstrom – Flora

==Production and release==
Much of the film was shot on location in Key West, Florida, although the setting is not specifically mentioned in the film.  The house featured in the film stands to this day and is known, appropriately, as the "Rose Tattoo House".

The premiere of the movie was held at the Hotel Astor, New York on December 2, 1955, with the attendance of Arthur Miller, Marlon Brando, Marilyn Monroe and Jayne Mansfield among other celebrities.

== Awards and nominations ==
; Awards 
* Academy Award for Best Actress – Anna Magnani
* Academy Award for Best Art Direction – Hal Pereira, Tambi Larsen, Sam Comer, Arthur Krams
* Academy Award for Best Cinematography – James Wong Howe

; Nominations
* Academy Award for Best Picture – Hal B. Wallis
* Academy Award for Best Supporting Actress – Marisa Pavan
* Academy Award for Costume Design – Edith Head
* Academy Award for Film Editing – Warren Low
* Academy Award for Original Music Score – Alex North

== References ==
{{Reflist
| refs =

 
{{cite book
| last = Osborne
| first = Robert
| authorlink = Robert Osborne
| year = 1994
| title = 65 Years of the Oscar: The Official History of the Academy Awards
| publisher = Abbeville Press
| location = London
| isbn = 1-55859-715-8
| page = 137
}}
 

 
{{cite web
| title = The Rose Tattoo
| publisher = New York Times
| url = http://movies.nytimes.com/movie/42127/The-Rose-Tattoo/awards
| accessdate = 2008-12-22
}}
 

}}

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 