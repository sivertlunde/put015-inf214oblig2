A Day on Mars
 
{{Infobox film
| name           = A Day on Mars
| image          = 
| image_size     = 
| caption        = 
| director       = Heinz Schall 
| producer       = Arzén von Cserépy
| writer         = Mathilde Wieder
| narrator       = 
| starring       = Lilly Flohr   Hermann Picha   Gerhard Ritterband   Hans Behrendt
| music          = 
| editing        =
| cinematography = Curt Courant
| studio         = Cserépy-Film 
| distributor    = 
| released       = 20 February 1921
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent science fiction film|sci-fi comedy film directed by Heinz Schall and starring Lilly Flohr, Hermann Picha and Gerhard Ritterband. It premiered in Berlin on 20 February 1921. 

==Cast==
* Lilly Flohr as Filmstar / Mars-Prinzessin 
* Hermann Picha as Astronom Himmelswurm 
* Gerhard Ritterband as Famulus
* Hans Behrendt   
* Henri Peters-Arnolds

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 


 
 