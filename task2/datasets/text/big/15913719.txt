Fantômas (1920 serial)
 
{{Infobox film
| name           = Fantômas
| image          = Fantomas 1921 ad.jpg
| caption        = Advert for the film
| director       = Edward Sedgwick William Fox
| writer         = Marcel Allain George Eshenfelder Edward Sedgwick Pierre Souvestre
| starring       = Edward Roseman Edna Murphy
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 20 episodes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 
  crime film serial directed by Edward Sedgwick. The film is considered to be lost film|lost.   

==Cast==
* Edward Roseman as Fantômas
* Edna Murphy as Ruth Harrington
* Johnnie Walker as Jack Meredith
* Lionel Adams as Prof.James D. Harrington
* John Willard as Detective Fred Dixon
* Eve Balfour as The Woman in Black
* Rena Parker as The Countess
* Irving Brooks as The Duke
* Ben Walker as The Butler
* Henry Armetta as The Wop
* Rita Rogan

==See also==
* List of American films of 1920
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 

 
 