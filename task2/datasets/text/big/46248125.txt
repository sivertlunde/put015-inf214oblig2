Mister Rock and Roll (film)
{{Infobox film
| name           = Mister Rock and Roll
| image          = Mister Rock and Roll poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Charles S. Dubin
| producer       = Ralph B. Serpe 
| screenplay     = James Blumgarten 
| starring       = Alan Freed Teddy Randazzo Lois OBrien Rocky Graziano Jay Barney Al Fisher Lou Marks
| music          =
| cinematography = Maurice Hartzband 	
| editing        = Angelo Ross 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Mister Rock and Roll is a 1957 American musical film directed by Charles S. Dubin and written by James Blumgarten. The film stars Alan Freed, Teddy Randazzo, Lois OBrien, Rocky Graziano, Jay Barney, Al Fisher and Lou Marks.   The film was released on October 16, 1957, by Paramount Pictures.

==Plot==
 

== Cast ==
*Alan Freed as himself
*Teddy Randazzo as himself
*Lois OBrien as Carole Hendricks
*Rocky Graziano as himself
*Jay Barney as Joe Prentiss
*Al Fisher as Al
*Lou Marks as Lou
*Leo Wirtz as Earl George
*Ralph Stantley as Station Representative
*Lionel Hampton as himself
*Ferlin Husky as himself
*Frankie Lymon as himself
*Little Richard as himself
*Brook Benton as himself
*Chuck Berry as himself
*Clyde McPhatter as himself
*LaVern Baker as herself
*Shaye Cogan as herself

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 