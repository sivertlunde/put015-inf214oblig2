The Sum of Us (film)
 
 
{{Infobox film
| name           = The Sum of Us Kevin Dowling
| image	=	The Sum of Us FilmPoster.jpeg David Stevens Jack Thompson Russell Crowe John Polson Deborah Kennedy
| music          = Brad Fiedel
| cinematography = Russell Carpenter
| editing        = Frans Vandenburg
| distributor    = The Samuel Goldwyn Company Australian Film Commission
| released       =   (Australia)   (UK & US)
| runtime        = 100 min.
| country        = Australia English
| gross          = $3,827,456
}}
 
 Kevin Dowling Jack Thompson. The screen adaptation mimics the plays device of breaking the fourth wall with direct to camera conversational asides by both Harry and Jeff.

==Plot==
Widower Harry Mitchell (Thompson) lives with his gay son Jeff (Crowe), with both men struggling in their searches for true love. Harry is completely comfortable with his sons sexuality, and is almost over-eager in his support for his sons search for a boyfriend. Harry meets an attractive but judgemental divorcee through a dating service, and this leads to some conflict between the two main characters. However, when Harry suffers a stroke and loses the power of speech, the story takes a darker turn, becoming a meditation on the enduring strength of love, both familial and romantic, in the face of adversity.

==Box Office==
The Sum of Us grossed $3,827,456 at the box office in Australia. 

==Awards==
Stevens screenplay won awards from the Australian Film Institute and the Montréal World Film Festival, and the movie was named Best Film at the Cleveland International Film Festival.

==See also==
 
*Cinema of Australia

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 