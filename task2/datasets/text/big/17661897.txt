Koizora
 
{{Infobox animanga/Header
| name            = Koizora
| image           = Koizora.png
| caption         = 
| ja_kanji        = 恋空―切ナイ恋物語
| ja_romaji       = Koizora: Setsunai Koi Monogatari
| genre           = Romance novel|Romance, Tragedy
}}
{{Infobox animanga/Print
| type            = novel series
| author          = Mika   
| publisher       = Starts Publications
| publisher_en    = 
| demographic     =
| magazine        = 
| imprint         =
| published       = 2005 (online) October 2006 (print)
}}
{{Infobox animanga/Print
| type            = manga
| title           = 
| author          = Ibuki Haneda
| publisher       = Futabasha
| publisher_en    = 
| demographic     = Seinen
| magazine        = Comic Mahou no iland
| first           = June 21, 2007
| last            = February 21, 2009
| volumes         = 8
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = 
| director        = Natsuki Imai
| producer        = Kazuya Kamana Masayuki Morigawa Jun Nasuda
| writer          = Mutsuki Watanabe
| music           = Shin Kouno
| studio          = Toho
| licensor        = 
| released        = November 3, 2007
| runtime         = 129 minutes
}}
{{Infobox animanga/Video
| type            = drama
| title           = 
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          = 
| licensor        =  TBS
| network_en      = 
| first           = August 2, 2008
| last            = September 13, 2008
| episodes        = 6
| episode_list    =
}}
 
 coming of cell phone website "Mahō no iLand",  where chapters would be released exclusively for mobile reading, Koizora received a hard print publication from Starts Publications in October 2006, with the story being separated into two volumes.   

Koizora is claimed to be a biographical account of Mika, or at least, based on first-hand accounts.  It boomed in popularity and became a mass cultural phenomenon,  spawning a theatrical film,       a television drama adaptation,  and a manga adaptation.

== Plot == drunkenly phones her, but his friend Hiro confiscates the phone and converses with her instead. Although Mika does not know who Hiro is, she feels at ease at the sound of his voice and the two befriend each other.

Mika and Hiro agree to meet each other when school starts, and to Mikas shock, Hiro turns out to be the delinquent boy she is afraid of, who proves his identity as the caller with a photo of the sky on his cellular phone. However, as she understands how gentle he is, they begin to fall in love with each other and face a multitude of challenges threatening their relationship, such as Hiros ex-girlfriend Saki, who is still in love with Hiro. Although Hiro assures Mika that he broke up with her, Saki holds a vendetta against Mika and hires a group of men to rape Mika. The horrific encounter ends with Hiro and his older sister punishing Saki and the rapists, but not long after, someone writes a provocative message on all the school chalkboards, resulting in Mika being harassed over the phone. Despite these events, Hiro vows to protect Mika, and she begins to compare him to the sky. Soon after, Mika becomes pregnant after she and Hiro make out in the library. Hiro is thrilled and gains his parents approval to raise the child, although her parents disapprove. Despite this, they are determined to raise the baby, until Mika has a miscarriage due to Saki pushing her downstairs. Mika is told that she may not be able to become pregnant again. Despairing, Mika and Hiro build a grave for their daughter, and promise to visit annually on the day of the babys death.

During their second year in high school, Hiro discovers that he has cancer and makes the painful decision to break up with Mika to keep his condition from causing her pain, but secretly continues to follow her whereabouts through his high school friend, Nozumu. Although hurt, Mika gradually forgets about Hiro and meets a college student named Yu, who becomes sympathetic to her situation. The two date, and Yu even prevents Mikas parents from divorcing. On the first anniversary of the babys death, Mika finds Hiro visiting the grave. On the second anniversary, she finds Nozomu instead. He reveals Hiros condition to Mika and she makes a difficult decision to leave Yū to go to his bedside. Upset that he pretended he didnt love her only to ensure she would have a happy future, she insists that only he can make her happy. Having Mika back, Hiro is determined to fight his disease and begins to improve as Mika takes an academic leave from college to visit him in the hospital. During a routine checkup, Hiros condition takes a turn for the worse and he dies before Mika gets a chance to say good bye to him.

Mika is distraught by Hiros death and attempts suicide by jumping off a bridge, but is stopped when two white doves fly towards the sky. She drops Hiros diary which was given to her after his death and discovers a letter he wrote to her before he died. She learns that he had anticipated his death and was happy with the time he had spent with her. She decides to carry on with life, not just for herself, but for Hiros sake as well. A month after Hiros death, Mika discovers that she is pregnant by Hiro with another baby girl, and feels a sense of closure knowing that Hiro has left a new life behind for her. She is later on seen placing a pair of blue mittens for Hiro next to the smaller, pink ones that were meant for her baby and praying for both their happiness.

==Reception==
Approximately 20 million people subscribed to Koizora.  The novel later received a hard print publication in October 2006 by Starts Publications and was published into two volumes. The books sold more than two million copies and became a mass cultural phenomenon in Japan.  Joanna Mauermann of Reading Worldwide attributes the popularity of Koizora to the sensation of realism in that "they are modelled on the readers own situation" and " he strong presence of the characters, presented in dialogues and (inward) monologues, address the reader directly, and the emotional state of the main character is experienced immediately."  

In addition to the empathy readers, the anonymity of the author attributes to the appeal of the Koizora. Much like the anonymous Densha Otoko, readers are more likely to tolerate exaggerated writing, first-hand accounts or not, due to how Mikas anonymity makes Koizora seem more "real" and "personal."  

Despite its popularity, Koizora has been attacked for having sexual and violent material available to young readers. In a review called "A crime of the media", a grade school teacher criticized the novel for supporting the misconception in young girls that rape leads to love. She suggested that the story had induced prepubescent girls to fantasize about rape.   

==Adaptations==
===Manga===
Koizora was adapted into a manga series drawn by Ibuki Haneda and overseen by Mika herself. The manga was published by Futabasha Publishers under the "Comic Mahou no iRando" label and lasted for eight volumes, the first volume releasing on June 21, 2007  and the last on February 21, 2009. 

===Film===
 

Koizora: Setsunai Koi Monogatari was loosely adapted into a theatrical film under the mononymous title Koizora. It was directed by Natsuki Imai   and starred Yui Aragaki as Mika and Haruma Miura as Hiro.  The film adaptation was released on November 3, 2007, with a gross revenue of 3.9 billion yen and 3.14 million people attending the theater.    It debuted at #3 on the box office.    The DVD was released on April 25, 2008.   

===Drama===
Koizora was also adapted into a TV drama series, which aired from August 2, 2008 to September 13, 2008, with a total of six episodes. From March to June 2008, auditions were held to choose the actor and actress for the two starring roles.  Erena Mizusawa was chosen to play Mika, and Koji Seto as Hiro; there were a total of more than a thousand applicants.    TBS broadcast the serial drama in prime-time, and the first audience rating was 5.6%.

== References ==
 

== External links ==
*   
*   

 
 
 

 
 
 
 
 
 
 
 
 
 