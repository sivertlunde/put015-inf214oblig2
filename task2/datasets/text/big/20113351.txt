The Secret Land
{{Infobox film
| name           = The Secret Land
| caption        = Film poster
| image	         = The Secret Land FilmPoster.jpeg
| director       = 
| producer       = Orville O. Dull
| writer         = Harvey S. Haislip William C. Park Robert Montgomery Robert Taylor Van Heflin
| starring       = 
| music          = 
| cinematography = 
| editing        = Fredrick Y. Smith MGM
| released       =  
| runtime        = 71 minutes
| country        = United States 
| language       = English
| budget         = $216,000  .  gross = $576,000 
}}

The Secret Land is a 1948 American documentary film about an American expedition code-named "Operation High Jump" to explore Antarctica.    It won the Academy Award for Academy Award for Best Documentary Feature.      

==Cast== Robert Montgomery - Narrator (voice) (as Comdr. Robert Montgomery, U.S.N.R.) Robert Taylor - Narrator (voice) (as Lt. Robert Taylor U.S.N.R.)
* Van Heflin - Narrator (voice) (as Lt. Van Heflin, A.A.F., Ret.)
* James Forrestal - Himself (as James V. Forrestal)
* Chester W. Nimitz - Himself
* Rear Admiral Richard E. Byrd - Himself (as Admiral Byrd)
* Richard Cruzen - Himself (as Admiral Cruzen)
* Robert S. Quackenbush - Himself (as Captain Quackenbush)
* George J. Dufek - Himself (as Captain George Dufek)
* Paul A. Siple - Himself (as Dr. Siple) Charles W. Thomas - Himself (as Captain Thomas)
* Richard E. Byrd Jr. - Himself
* Vernon D. Boyd - Himself (as Captain Boyd)
* Charles A. Bond - Himself (as Captain Bond)
* David E. Bunger - Himself (as Commander David E. Bunger)
* John E. Clark - Himself (as Captain Clark)
* John D. Howell - Himself (as Commander Howell)
* William Kearns - Himself (as Lieutenant j.g. Bill Kearns)
* Ralph LeBlanc - Himself (as Lieutenant j.g. Frenchie LeBlanc)
* Henry H. Caldwell - Himself (as Captain Caldwell)
* Owen McCarty - Himself
* William Warr - Himself
* James H. Robbins - Himself (as Shorty Rogers)

==Reception==
The film earned $395,000 in the US and Canada and $181,000 elsewhere, resulting in a profit of $10,000. 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 


 