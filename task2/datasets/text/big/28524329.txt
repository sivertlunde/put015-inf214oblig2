Mortel Transfert
{{infobox film
| name = Mortel Transfert
| image = MortelTransfert.jpg
| director = Jean-Jacques Beineix
| producer = Reinhard Kloos
| writer = Jean-Jacques Beineix
| based on =  
| starring = Jean-Hugues Anglade   Hélène de Fougerolles
| music = Reinhardt Wagner
| cinematography = Benoît Delhomme
| editing = Yves Deschamps Kako Kelber
| released =  
| runtime = 122 minutes
| country = France Germany
| language = French
}}
Mortel Transfert is a Franco-German thriller, directed by Jean-Jacques Beineix, adapted from the novel of the same name by Jean-Pierre Gattégno.  The music was provided by the composer of Roselyne et les lions, Reinhardt Wagner. Some of the paintings in the film were produced by Pierre Peyrolle, who had designed the "Try another world" poster in La Lune dans le caniveau. Mortel transfert went into production in April 2000 and was released in France on 10 January 2001. 

==Synopsis==
Michel Durand is a psychoanalyst who is in the habit of dropping off to sleep while listening to his patients. At the end of one session Madame Kubler does not move. What has happened? Who killed the wife of Max Kubler, the crooked property developer? Why are Michel Durands arms hurting so much? Can you murder someone while asleep? Michel Durand needs to do something in any case, because Police Commissioner Chapireau is investigating and Max Kubler is going crazy looking for his wife. 

==Critical responses== After Hours. The critic from Libération applauded the black humour which  he felt was reminiscent of Alfred Hitchcocks The Trouble with Harry and wondered if Beineix could have exploited the comedy element more.  Pascal Mérigeau in Le Nouvel observateur suggested the film showed Beineixs profound self-disgust.

==Cast==
* Jean-Hugues Anglade as Michel Durand
* Hélène de Fougerolles as Olga Kubler
* Miki Manojlovic as Erostrate
* Valentina Sauca as Hélène Maier Robert Hirsch as Armand Slibovic
* Yves Rénier as Max Kubler
* Catherine Mouchet as The professor of maths
* Denis Podalydès as Commissioner Chapireau

==References==
 

==External links==
*  

 
 
 
 
 


 