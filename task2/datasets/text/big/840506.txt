Sebastian (1995 film)
{{Infobox film
| name           = Sebastian
| image          = 
| image_size     = 
| caption        = 
| director       = Svend Wam
| producer       = Lars Kolvig Hansi Mandoki Petter Vennerød
| writer         = Per Knutsen (novel) Hansi Mandoki
| narrator       = 
| starring       = Hampus Björck Nicolai Cleve Broch Ewa Fröling Helge Jordal Rebecka Hemse Emil Lindroth
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| preceded_by    = 
| followed_by    = Swedish film based on the novel by Per Knutsen that chronicles the coming out experience of a teenage boy. It is directed by Svend Wam.

==Plot==
16-year-old Sebastian (Hampus Björck) has nice parents and a great circle of friends, is doing well in school, has good looks, and leads a happy life—or at least that is what everyone thinks. Secretly, he has been brooding for some time over the fact that he is in love with his best friend, the rather hunky Ulf (Nicolai Cleve Broch).

One evening, after some playful frolicking around with Ulf, Sebastian plants a kiss on Ulfs lips, not exactly to the latters delight. Being sexually rejected in this way throws Sebastian into an even deeper depression about his sexuality. He refuses to discuss these issues with his parents and only after a long talk with a female friend of his, who is also in love with Ulf and guesses correctly what Sebastians problem could be, he opens up to his family and friends, only to find that it is not all that problematic to them.

Even the coming out to Ulf, which he anticipated to be very painful, turns out to be not so bad and actually takes a comical turn when Ulf admits to having had a homosexual experience himself some time ago. In the end, basically nothing has changed, except that Sebastian no longer has to waste energy guarding his secret and can instead concentrate on being happy, having fun with his friends, and perhaps finding a real boyfriend someday.

==Reactions==
In critical reviews, Sebastian is frequently dismissed as escapist gay youth fare that does not address any deeper issues. However, as the movie points out correctly, the most difficult part of the coming out process is fighting ones inner uncertainty and self-doubt. And even if (as in the case of Sebastian) a coming out is comparatively eventless, the inner resolve required to take that step stays the same.

==External links==
* 

 
 
 
 
 


 
 