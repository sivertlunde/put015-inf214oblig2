Bharya Oru Manthri
{{Infobox film
| name           = Bharya Oru Manthri
| image          =
| caption        =
| director       = Raju Mahendra
| producer       =
| writer         =
| screenplay     = Menaka Sukumaran Anuradha Balan K Nair
| music          = Kannur Rajan
| cinematography =
| editing        =
| studio         = Vinayaka Arts
| distributor    = Vinayaka Arts
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, Anuradha and Balan K Nair in lead roles. The film had musical score by Kannur Rajan.   

==Cast== Menaka
*Sukumaran Anuradha
*Balan K Nair

==Soundtrack==
The music was composed by Kannur Rajan and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Karalinteyullil || P Jayachandran, Chorus || Bichu Thirumala || 
|-
| 2 || Sukhamulla kulirthennal || K. J. Yesudas || Bichu Thirumala || 
|-
| 3 || Vaathsyayanante Raathrikal || S Janaki || Bichu Thirumala || 
|-
| 4 || Veene ninne meettaan || K. J. Yesudas, Vani Jairam || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 