The Secret of St. Ives
The historical adventure film directed by Phil Rosen and starring Richard Ney, Vanessa Brown and Henry Daniell. It is adapted from the 1897 novel St. Ives (novel)|St. Ives by Robert Louis Stevenson. During the Napoleonic Wars a French officer is captured and held as a prisoner in Britain. He manages to escape with the help of a local woman.

==Main cast==
* Richard Ney as Anatole de Keroual 
* Vanessa Brown as Floria Gilchrist 
* Henry Daniell as Major Edward Chevenish 
* Edgar Barrier as Sgt. Carnac 
* Aubrey Mather as Daniel Romaine 
* Luis Van Rooten sa Clausel 
* John Dehner as Couguelat  Paul Marion as Amiot  Douglas Walton as Allan St. Ives 
* Jean Del Val as Count St. Ives  Phyllis Morris as Annie Gilchrist
* Everett Glass as Priest 
* John Goldsworthy as General Ordney 
* Gerald Hamer as Hudson, the Footman Gordon Richards as Prosecuting Officer

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 