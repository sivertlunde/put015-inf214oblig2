D2: The Mighty Ducks
{{Infobox film
| name = D2: The Mighty Ducks
| image = D two the mighty ducks.jpg
| caption = Theatrical release poster
| director = Sam Weisman
| producer = Jon Avnet Jordan Kerner Steven Brill Michael Tucker Jan Rubes Kathryn Erbe
| music = J.A.C. Redford
| cinematography = Mark Irwin
| editing = John F. Link Eric Sears
| studio = Walt Disney Pictures Avnet–Kerner Productions Buena Vista Pictures
| released =  
| runtime = 106 minutes
| language = English Icelandic
| budget =
| gross = $45.6 million
}} sports comedy film directed by Sam Weisman. It is a sequel to the 1992 film The Mighty Ducks and produced by Walt Disney Pictures and Avnet–Kerner Productions. In the United Kingdom and Australia, the film was titled The Mighty Ducks (the first having been titled Champions and subsequently, on home releases, as The Mighty Ducks Are the Champions).

==Plot==
Former peewee ice hockey coach Gordon Bombay is a star in the minor leagues and is expected to make it to the National Hockey League soon. However, after a career-ending knee injury, he returns to the Blukeville district of Minneapolis. Bombay is then offered a chance to coach a team representing the United States in the Junior Goodwill Games. Team USA consists of many of the old Ducks, in addition to five new players with special talents.
 enforcer skills, becoming known as the "Bash Brothers". Backup goaltender Julie asks Bombay for a chance to play, but he tells her to wait, as current goalie Greg Goldberg is on a hot streak.

Reality sets in when the team suffers an embarrassing 12-1 defeat at the hands of Team Iceland, coached by ex-NHL player Wolf "The Dentist" Stansson, who is known for his tough reputation. Team USA plays badly, with Julie and Portman ejected from the game. Star center Adam Banks manages to score a goal but gets slashed in the wrist moments later. Frustrated, Bombay drives his players even harder, but they begin to suffer, completely exhausted. His practice sessions become brutal and long.  Realizing the children are too tired to complete their school work or even stay awake in class, the teams tutor, Michelle McKay, intervenes.  She cancels the practice and confronts Bombay.  Now better rested, the players come across a street hockey team who teaches them how to play like "the real Team USA".
 team captain.

At first, Iceland appears to be out to dominate Team USA again, but they manage to score one goal. Unfortunately, the Ducks take penalties: Ken picks a fight with an Iceland player ("stick, gloves, shirt") after scoring the teams first goal, the Bash brothers celebrate this by fighting with the entire Iceland bench and Dwayne lassoes an opposing player, about to check Connie. Bombay is annoyed because "this isnt a hockey game, its a circus."

After a motivational locker room speech from Bombay and new Duck jerseys from Jan, the team emerges rejuvenated. The Ducks manage to tie the game with a score from Connie, Banks, Luis, and finally when Russ outsmarts Team Iceland by disguising himself as Goldberg, so as to prevent himself from being covered and pulling off a successful "knucklepuck". The game is forced to go to a five-shot Overtime (ice hockey)|shootout. With a 4-3 score in favor of the Ducks, Gunnar Stahl (the tournaments leading scorer) is Team Icelands final shooter. Bombay knows Gunnar favors shooting the glove side after a triple deke (ice hockey)|deke, and replaces Goldberg with Julie, who has a faster glove. Gunnar advances on Julie and fires a hard slapshot. Although Julie falls to the ice, she slowly turns to look at her glove while the entire stadium (and presumably the home audience of millions) waits in breathless anticipation. She then opens her glove and drops the puck, signifying the game-winning save. With this, the Ducks triumph over Iceland to win the tournament. Despite Wolfs disappointment, he congratulates Bombay and Gunnar, being the gentleman he is congratulates Charlie stating "Good work, Captain Duck".

The film concludes with the team returning to Minnesota on a plane and sitting around a campfire singing Queen (band)|Queens "We Are the Champions" as the credits roll.

==Cast==
In credits order: 
* Emilio Estevez as Gordon Bombay
* Kathryn Erbe as Michelle McKay Michael Tucker as Mr. Tibbles
* Jan Rubes as Jan
* Carsten Norgaard as Wolf "The Dentist" Stansson
* Maria Ellingsen as Maria
* Joshua Jackson as Charlie Conway, #96
* Elden Henson as Fulton Reed, #44
* Shaun Weiss as Greg Goldberg, #33 Matt Doherty as Les Averman, #4
* Brandon Adams as Jesse Hall, #9
* Garette Ratliff Henson as Guy Germaine, #00
* Marguerite Moreau as Connie Moreau, #18
* Vincent Larusso as Adam Banks, #99
* Colombe Jacobsen as Julie Gaffney, #6
* Aaron Lohr as Dean Portman, #21
* Ty ONeal as Dwayne Robertson, #7
* Kenan Thompson as Russ Tyler, #56
* Mike Vitar as Luis Mendoza, #22
*   as Ken Wu, #16
* Scott Whyte as Gunnar Stahl, #9

===Cameo appearances===
There are several cameo appearances in D2: The Mighty Ducks from famous athletes.
* Kristi Yamaguchi - Champion Olympic figure skater
* Greg Louganis - Champion Olympic diver
* Kareem Abdul-Jabbar - Basketball player
* Wayne Gretzky, Chris Chelios, Luc Robitaille, Cam Neely, Paul Kariya - Professional (NHL) Ice Hockey players Bob Miller - announcer

===Departures===
Mighty Duck players that were in the first film but not this one:
* Tammy Duncan (Jane Plank, her figure skating skills were replaced with those of Ken Wu)
* Tommy Duncan (Danny Tamberelli)
* Terry Hall (Jussie Smollett, despite the continuation of the characters brother, Jesse) Aaron Schwartz)
* Peter Mark (J.D. Daniels)

==Reaction==

===Critical reception===
The film received negative reviews. It has received a 21% rating on Rotten Tomatoes (actually making it the most well received entry of the series on the site). 
 Desson Howe of The Washington Post wrote: "D2: The Mighty Ducks reaches an extraordinary low &ndash; even for a Disney sequel. This unctuous barrage of flag-waving, message-mongering, counterfeit morality, which contains the stalest kiddie-team heroics in recent memory, makes the original, innocuous Ducks look like one of the Great Works." {{cite news 
  | last = Howe
  | first = Desson
  | coauthors = 
  | title = ‘D2: The Mighty Ducks’ 
  | work = The Washington Post
  | pages = 
  | publisher = 
  | date = 1994-03-25
  | url = http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/d2themightyduckspghowe_a0b044.htm
  | accessdate = 2009-02-05}} 

===Box office===
In its opening weekend, the film grossed $10,356,748 domestically.   It was a financial success, with a final domestic box office total of $45,610,410.

==Soundtrack== Queen - "We Will Rock You" You Aint Seen Nothin Yet" (Bachman-Turner Overdrive Cover) Rock and Roll"
# Martha Wash - "Mr. Big Stuff" David Newman - "Mighty Ducks Suite" Tag Team - "Whoomp! (There It Is)"
# The Troggs - "Wild Thing"
# Gear Daddies - "Zamboni (song)|Zamboni"
# Queen - "We Are the Champions"
# John Bisaha - "Rock the Pond"

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 