The Pursuit of Happyness
 
 
{{Infobox film
| name           = The Pursuit of Happyness
| image          = poster-pursuithappyness.jpg
| caption        = Theatrical release poster
| director       = Gabriele Muccino
| producer       = Will Smith 
| writer         = Steven Conrad
| starring       =   Andrea Guerra
| cinematography = Phedon Papamichael
| editing        = Hughes Winborne
| studio         =  
| distributor    = Columbia Pictures
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = $55 million
| gross          = $307.1 million
}} biographical drama film based on Chris Gardners nearly one-year struggle with homelessness. Directed by Gabriele Muccino, the film features Will Smith as Gardner, a homeless salesman. Smiths son Jaden Smith co-stars, making his film debut as Gardners son, Christopher Jr. Will Smith and Jaden Smith later appeared together in the film After Earth.

The screenplay by Steven Conrad is based on the best-selling memoir written by Gardner with Quincy Troupe. The film was released on December 15, 2006, by Columbia Pictures. For his performance, Smith was nominated for an Academy Award and a Golden Globe for Best Actor.

The unusual spelling of the films title comes from a mural that Gardner sees on the wall outside the daycare facility his son attends. He complains to the owner of the daycare that "happiness" is incorrectly spelled as "happyness" and needs to be changed.

==Plot==
In 1981, San Francisco salesman Chris Gardner (Will Smith) invests his entire life savings in portable bone-density scanners, which he demonstrates to doctors and pitches as a handy quantum leap over standard X-rays. The scanners play a vital role in Chris life. While he is able to sell most of them, the time lag between the sales and his growing financial demands enrage his already bitter and alienated wife Linda (Thandie Newton), who works as a hotel maid. The lack of a stable financial state increasingly erodes their marriage, in spite of them caring for their five-year-old son, Christopher (Jaden Smith).
 BART station. Gardner boards a train but loses one of his scanners in the process. His new relationship with Jay earns him the chance to become an intern stockbroker. The day before the interview, Gardner grudgingly agrees to paint his apartment so as to postpone moving out due to his difficulty in paying the rent. While painting, Gardner is greeted by the police at his doorstep, who takes him to the station, stating he has to pay for his numerous parking tickets he has accumulated. As part of the sanction, Gardner is ordered to spend the night in jail, complicating his schedule for the interview the next morning. He manages to arrive at Dean Witters office on time, albeit still in his shabby clothes. Despite his appearance, he impresses the interviewers, and lands an internship. He will be amongst 20 interns competing for a paid position as a broker.

Gardners unpaid internship does not please Linda, who eventually leaves for New York. After Gardner bluntly says she is incapable of being a single mother, she agrees that Christopher will remain with his father. Gardner is further set back when his bank account is garnished by the IRS for unpaid income taxes, and he and his young son are evicted. He ends up with less than twenty-two dollars, resulting in them being homeless, and are forced at one point to stay in a restroom at a BART station. Other days, he and Christopher spend nights at a homeless shelter, in BART, or, if he manages to procure cash, at a hotel. Later, Gardner finds the bone scanner that he lost in the station and, after repairing it, sells it to a physician, thus completing all his sales of his scanners.

Disadvantaged by his limited work hours, and knowing that maximizing his client contacts and profits is the only way to earn the broker position, Gardner develops a number of ways to make phone sales calls more efficiently, including reaching out to potential high value customers, defying protocol. One sympathetic prospect who is a top-level pension fund manager even takes him and his son to a San Francisco 49ers game. Regardless of his challenges, he never reveals his lowly circumstances to his colleagues, even going so far as to lend one of his bosses five dollars for cab fare, a sum that he cannot afford. Concluding his internship, Gardner is called into a meeting with his managers. One of them notes he is wearing a new shirt. Gardner explains it is his last day and thought to dress for the occasion. The manager smiles and says he should wear it again tomorrow, letting him know he has won the coveted full-time position. Fighting back tears, Gardner shakes hands with them, then rushes to his sons daycare to embrace Christopher. They walk down the street, joking with each other and are passed by a man in a business suit (the real Chris Gardner in a cameo appearance). The epilogue reveals that Gardner went on to form his own multi-million dollar brokerage firm.

==Cast==
* Will Smith as Chris Gardner
* Jaden Smith as Christopher Gardner Jr.
* Thandie Newton as Linda Gardner Brian Howe as Jay Twistle
* Dan Castellaneta as Alan Frakesh
* James Karen as Martin Frohm
* Kurt Fuller as Walter Ribbon
* Takayo Fischer as Mrs. Chu

==Production==
 s struggle with homelessness]]

===Development===
Chris Gardner realized his story had Hollywood potential after an overwhelming national response to an interview he did with 20/20 (US television series)|20/20 in January 2002. {{cite news
 | first = Bill
 | last  = Zwecker
 | title = There’s a Way—and Maybe a Will—for Gardner Story
 | work  = Chicago Sun-Times
 | page  = Pg. 36
 | date  = 2003-07-17}}  He published his autobiography on May 23, 2006, and later became an associate producer for the film. The movie took some liberties with Gardners true life story. Certain details and events that actually took place over the span of several years were compressed into a relatively short time and although eight-year-old Jaden portrayed Chris as a five-year-old, Gardners son was just a toddler at the time.

===Casting=== Muhammad Ali, he can play you!" {{cite news
 |author=Indo-Asian News Service
 | title =Christopher Gardner unimpressed jihyg with Shakti
 | work =Newswire
 | pages =1000089 words
 | publisher =HT Media Ltd.
 | date =2006-12-14}} 

===Filming===
Gardner makes a cameo appearance in the film, walking past Will and Jaden in the final scene. Gardner and Will acknowledge each other; Will then looks back at Gardner walking away as his son proceeds to tell him knock-knock jokes.

===Music=== Andrea Guerra on January 9, 2007.

{{Track listing
| collapsed       = No
| total_length    = 40:00
| title1          = Opening
| length1         = 3:09
| title2          = Being Stupid
| length2         = 1:39
| title3          = Running
| length3         = 1:30
| title4          = Trouble at Home
| length4         = 1:30
| title5          = Rubiks Cube Taxi
| length5         = 1:53
| title6          = Park Chase
| length6         = 2:29
| title7          = Linda Leaves
| length7         = 4:02
| title8          = Night at Police Station
| length8         = 1:36
| title9          = Possibly
| length9         = 1:45
| title10         = Wheres My Shoe
| length10        = 4:20
| title11         = To the Game/Touchdown
| length11        = 1:37
| title12         = Locked Out
| length12        = 2:20
| title13         = Dinosaurs
| length13        = 2:40
| title14         = Homeless
| length14        = 1:55
| title15         = Happyness
| length15        = 3:50
| title16         = Welcome Chris
| length16        = 3:45
}}
 Higher Ground" and "Jesus Children of America", both sung by Stevie Wonder, and Glide Ensemble.

===Deviation from actual events===
Although generally faithful to the series of events, many subtle details deviate from the actual event. 

Examples include:
* The age of Chriss son: In the film he is five years old. In reality he was two years old. 
* The arrest of Chris: In the film he was arrested for unpaid parking tickets. In reality he was visited by police on charges of domestic abuse and later found to have unpaid parking tickets.
* The income of Chris: In the film he is shown selling bone density scanners. In reality, he sold medical supplies; he never sold a bone density scanner.
* Chriss son, though portrayed as being from his relationship with his estranged wife, was in reality the result of an affair between Chris and Jackie Medina, a dental student, during their marriage. 

==Release==

===Box office===
The film debuted first at the North American box office, earning $27 million during its opening weekend and beating out heavily promoted films such as Eragon (film)|Eragon and Charlottes Web (2006 film)|Charlottes Web. It was Smiths sixth consecutive #1 opening and one of Smiths consecutive $100 million blockbusters. The film grossed $162,586,036 domestically in the US and Canada. In the hope Gardners story would inspire the down-trodden citizens of Chattanooga, Tennessee to achieve financial independence and to take greater responsibility for the welfare of their families, the mayor of Chattanooga organized a viewing of the film for the citys homeless. {{cite news
 |author=The Associated Press State & Local Wire
 | title =News briefs from around Tennessee
 | pages =788 words
 | publisher =AP Newswire
 | date =2006-12-15}}  Gardner himself felt that it was imperative to share his story for the sake of its widespread social issues. "When I talk about alcoholism in the household, domestic violence, child abuse, illiteracy, and all of those issues—those are universal issues; those are not just confined to ZIP codes," he said.   

===Home media=== Region 1 DVD sales accounted for an additional $89,923,088 in revenue, slightly less than half of what was earned in its first week of release.  About 5,570,577 units have been sold, bringing in $90,582,602 in revenue. 

==Reception==

===Critical response===
The film was received generally positively by critics, with Smith receiving widespread acclaim for his performance. Film review site Rotten Tomatoes calculated a 67% overall approval based on 171 reviews. The sites critical consensus reads, "Will Smiths heartfelt performance elevates The Pursuit of Happyness above mere melodrama." 

In the San Francisco Chronicle, Mick LaSalle observed, "The great surprise of the picture is that its not corny . . . The beauty of the film is its honesty. In its outlines, its nothing like the usual success story depicted on-screen, in which, after a reasonable interval of disappointment, success arrives wrapped in a ribbon and a bow. Instead, this success story follows the pattern most common in life&nbsp;— it chronicles a series of soul-sickening failures and defeats, missed opportunities, sure things that didnt quite happen, all of which are accompanied by a concomitant accretion of barely perceptible victories that gradually amount to something. In other words, it all feels real." 
 realist Drag drag . craw . . . Its the same old bootstraps story, an American dream artfully told, skillfully sold. To that calculated end, the film making is seamless, unadorned, transparent, the better to serve Mr. Smiths warm expressiveness . . . How you respond to this mans moving story may depend on whether you find Mr. Smiths and his sons performances so overwhelmingly winning that you buy the idea that poverty is a function of bad luck and bad choices, and success the result of heroic toil and dreams." 

Peter Travers of Rolling Stone awarded the film three out of a possible four stars and commented, "Smith is on the march toward Oscar . . .   role needs gravity, smarts, charm, humor and a soul thats not synthetic. Smith brings it. Hes the real deal." 

In Variety (magazine)|Variety, Brian Lowry said the film "is more inspirational than creatively inspired—imbued with the kind of uplifting, afterschool-special qualities that can trigger a major toothache . . . Smiths heartfelt performance is easy to admire. But the movies painfully earnest tone should skew its appeal to the portion of the audience that, admittedly, has catapulted many cloying TV movies into hits . . . In the final accounting,   winds up being a little like the determined salesman Mr. Gardner himself: easy to root for, certainly, but not that much fun to spend time with." 

Kevin Crust of the Los Angeles Times stated, "Dramatically it lacks the layering of a Kramer vs. Kramer, which it superficially resembles . . . Though the subject matter is serious, the film itself is rather slight, and it relies on the actor to give it any energy. Even in a more modest register, Smith is a very appealing leading man, and he makes Gardners plight compelling . . . The Pursuit of Happyness  is an unexceptional film with exceptional performances . . . There are worse ways to spend the holidays, and, at the least, it will likely make you appreciate your own circumstances." 

In the St. Petersburg Times, Steve Persall graded the film B- and added, "  is the obligatory feel-good drama of the holiday season and takes that responsibility a bit too seriously . . . the film lays so many obstacles and solutions before its resilient hero that the volume of sentimentality and coincidence makes it feel suspect . . . Neither Conrads script nor Muccinos redundant direction shows   lifted the real-life Chris above better educated and more experienced candidates, but it comes through in the earnest performances of the two Smiths. Father Will seldom comes across this mature on screen; at the finale, he achieves a measure of Oscar-worthy emotion. Little Jaden is a chip off the old block, uncommonly at ease before the cameras. Their real-life bond is an inestimable asset to the on-screen characters relationship, although Conrad never really tests it with any conflict." 

National Review Online has named the film #7 in its list of The Best Conservative Movies. Linda Chavez of the Center for Equal Opportunity wrote, "this film provides the perfect antidote to Wall Street and other Hollywood diatribes depicting the world of finance as filled with nothing but greed." 

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Academy Award Academy Award Best Actor Will Smith
| 
|- BET Awards|BET Award BET Award Best Actor
| 
|- Black Reel Black Reel Award Black Reel Best Film
| 
|- Black Reel Best Actor Will Smith
| 
|- Black Reel Best Breakthrough Performance Jaden Smith
| 
|- Broadcast Film Critics Association Award Broadcast Film Best Actor Will Smith
| 
|- Broadcast Film Best Young Performer Jaden Smith
| 
|- Capri Award Movie of the Year
| 
|- Chicago Film Critics Association Award Chicago Film Best Actor Will Smith
| 
|- David di Donatello Award David di Best Foreign Film
| 
|- Golden Globe Award Golden Globe Best Actor - Motion Picture Drama Will Smith
| 
|- MTV Movie MTV Movie Award MTV Movie Best Male Performance
| 
|- MTV Movie Best Breakthrough Performance Jaden Smith
| 
|- NAACP Image Award NAACP Image Outstanding Motion Picture
| 
|- NAACP Image Outstanding Actor in a Motion Picture Will Smith
| 
|- NAACP Image Outstanding Supporting Actor in a Motion Picture Jaden Smith
| 
|- NAACP Image Outstanding Supporting Actress in a Motion Picture Thandie Newton
| 
|- Nastro dArgento Nastro dArgento Best Score Andrea Guerra Andrea Guerra
| 
|- Phoenix Film Critics Society Award Best Young Actor Jaden Smith
| 
|- Screen Actors Guild Award Screen Actors Outstanding Performance by a Male Actor in a Leading Role Will Smith
| 
|- Teen Choice Teen Choice Award Teen Choice Choice Movie - Drama
| 
|-
|rowspan=2|Choice: Chemistry Will Smith
| 
|- Jaden Smith
| 
|-
|Choice: Breakout Male
| 
|-
|}

==See also==
 
* List of American films of 2006

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 