White Elephant (2012 film)
 
{{Infobox film
| name           = White Elephant
| image          = Elefante Blanco (2012 film).jpg
| caption        = Film poster
| director       = Pablo Trapero
| producer       = Pablo Trapero
| writer         = Pablo Trapero Alejandro Fadel Martín Mauregui Santiago Mitre
| starring       = Ricardo Darín Jérémie Renier Martina Gusman
| music          = 
| studio         = Patagonik Film Group
| cinematography = Guillermo Nieto
| editing        =  BVI  
| released       =  
| runtime        = 120 minutes
| country        = Argentina Spain France
| language       = Spanish
| budget         = 
}}

White Elephant ( ) is a 2012 Argentine drama film directed by Pablo Trapero. The film competed in the Un Certain Regard section at the 2012 Cannes Film Festival.      

==Plot==
Two priests, the old veteran Father Julián and his new younger Belgian colleague, Father Nicolás, and the social worker Luciana, work in a slum area of Buenos Aires, Argentina known as Ciudad Oculta. Together they fight to resolve the issues of the neighborhood’s society. Their work will have them face the clerical hierarchy, the organized crime and the repression, risking their lives defending their commitment and loyalty towards the people of the neighbourhood.   

The films title comes from the gigantic abandoned hospital that dominates the area, described by Peter Bradshaw in The Guardian as, "a deserted wreck and cathedral of poverty known as the white elephant where the homeless camp and drug-dealers ply their trade."   Linked 2014-01-13 

==Main cast==
* Ricardo Darín as Julián
* Jérémie Renier as Nicolas
* Martina Gusman as Luciana

==Release==
After its premiere at the 2012 Cannes Film Festival on 21 May 2012, the film went on general release in Argentina three days later. It saw a very limited release (just 1 screen) in the United States on 29 March 2013,  and a more general release in the United Kingdom on 26 April 2013. 

The views of the British critics were mixed. Peter Bradshaw in   was more enthusiastic, calling it a "potent drama about the lawless slums of Buenos Aires – it feels like The Mission with all exoticism firmly excised." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 