A Madea Christmas (film)
 
{{Infobox film
| name           = A Madea Christmas
| image          = A Madea Christmas.jpg 
| alt            =  
| caption        = Theatrical film poster
| director       = Tyler Perry
| producer       = Tyler Perry Ozzie Areu Matt Moore
| writer         = Tyler Perry
| starring       = Tyler Perry Anna Maria Horsford Larry the Cable Guy Chad Michael Murray Tika Sumpter Eric Lively Alicia Witt Lisa Whelchel Kathy Najimy Sweet Brown JR Lemon
| music          = Christopher Young
| cinematography = Alexander Gruszynski
| editing        = Maysie Hoy
| studio         = Tyler Perry Studios Lionsgate
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $25 million
| gross          = $53.4 million 
}} comedy film play of the same name. This is the seventeenth film by Perry, and the seventh in the Madea franchise. It was released in theaters on December 13, 2013.

==Plot==
In Atlanta, GA|Atlanta, Georgia Mabel "Madea" Simmons (Tyler Perry) has been talked into getting a job at a local store as customer service by her great-niece Eileen (Anna Maria Horsford). Madea quickly gets fed up with the customers and starts speaking rudely to them, causing herself to get fired on her first day. Madea then asks for her paycheck, but her boss tells her they will mail it to her. Infuriated, Madea steals a dress and some money from the cash register, then exits shortly after.

Meanwhile, Eileens daughter, Lacey ( ) fund the school through his business. Lacey also wants one of her students, Bailey (Noah Urrea) to sing in the Christmas Jubilee. Stuck with many issues to deal with, she tells Eileen that she will not be able to come home for Christmas.

Eileen, determined to see her daughter for Christmas, decides to pay Lacey a surprise visit. She brings Madea, who is hesitant at first, with her, and has Oliver, who is on his way with a contract for the sponsorship, as their ride. The trio then head to Alabama, where Lacey lives.

The trio eventually make it to Alabama. This makes things awkward, since Lacey has not yet told Eileen that she is married to Connor (Eric Lively) because she is afraid of what Eileen will think of her marrying a white man. Eileen despises White people because her husband left her for a white woman when Lacey was 2 years old; she has lied about the story and said a white woman killed her husband.

Connors parents, Buddy (Larry the Cable Guy), and Kim (Kathy Najimy), have come to visit him for Christmas, but they must not mention that he and Lacey are married. Meanwhile, Eileen decides to get a Christmas tree and cuts down a tree with a yellow ribbon wrapped around it in the backyard, not knowing that Kim planted the tree in memory of her deceased father. The family soon realizes that Eileen cut down Kims  tree, upsetting them.

That night, Eileen walks in on Buddy and Kim, seeing Buddy with a sheet over his head, convincing her that Buddy is in the KKK (Ku Klux Klan).) Now scared, she bars the door to the room she and Madea share, resulting in Madea telling her to remove it, because she has to go to the bathroom several times at night. Eileen simply gives Madea a cup to use instead, and Madea refuses.

The Next Day, Lacey goes to the meeting with the school principal, her ex, and the town mayor to go over the contract for Olivers representatives sponsorship of the jubilee. While she is out Madea looks after Laceys class. When Madeas back is turned, the class bully steals her purse. Madea realizes this and tells the students a modern day version of the story of the first Christmas, in order to keep calm and to hopefully have the person who stole her purse return it to her. Though this fails, she tries to then tell them the story of Easter, but she loses her temper half way through the story and demands her purse back. Meanwhile, with the contract glazed over and signed, the town and school now have the money they need. Lacey says good-bye to Oliver, and thanks him for his help, only to have Oliver try to pull a move on her. Now angry with Oliver and telling him that she is married, Lacey then prepares to return to her class, but to her and the principals shock and dismay, Madea has tied the girl that stole her purse to the cross decoration in the classroom.

==Cast== Mabel "Madea" Simmons
* Larry the Cable Guy as Buddy Williams
* Kathy Najimy as Kim Williams
* Chad Michael Murray as Tanner McCoy
* Anna Maria Horsford as Eileen Murphy
* Tika Sumpter as Lacey Williams (née Murphy)
* Eric Lively as Conner Williams
* JR Lemon as Oliver
* Alicia Witt as Amber
* Lisa Whelchel as Nancy Porter
* Noah Urrea as Bailey McCoy
* Ashlee Heath as Audry
* Caroline Kennedy as Lucy Sweet Brown as herself 
* Antoine Dodson as YouTube guy Jonathan Chase as Alfred
* Vickie Eng as Customer #6 Daniel Mann as Austin

==Filming==
Filming began in the beginning of January to late-March. Filming took place in the towns of McDonough, Georgia and Atlanta, Georgia, and at Tyler Perry Studios. In keeping with his support of local businesses, numerous props were sourced locally. For example, the 6&nbsp;ft tall nutcrackers and large gift boxes came from Eclectia.net on Howell Mill Road.

==Soundtrack== Meet the Browns. It featured Smokey Robinson, Mariah Carey, Brian McKnight, James Brown, Kem (singer)|KEM, Boyz II Men, Kelly Rowland, Jeremih, Stevie Wonder, Jackson 5, Pearl Bailey, Ashanti (singer)|Ashanti, Mprynt & Kevin Ross.

==Reception==
A Madea Christmas was panned by critics. On  , the film has a score of 28 out of 100, based on 20 critics, indicating "generally unfavorable reviews" 

==Home media==
A Madea Christmas was released on Blu-ray and DVD on November 25, 2014.  The DVD contains a first look at the opening of the upcoming animated feature "Madeas Tough Love".

==Awards and nominations==

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Golden Raspberry Award Golden Raspberry Worst Picture Ozzie Areu
| 
|- Matt Moore
| 
|- Tyler Perry
| 
|- Golden Raspberry Worst Director
| 
|- Golden Raspberry Worst Screenplay
| 
|- Golden Raspberry Worst Actress
| 
|- Golden Raspberry Worst Screen Combo
| 
|- That worn-out wig and dress
| 
|- Larry the Cable Guy
| 
|- Golden Raspberry Worst Supporting Actor
| 
|-
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 