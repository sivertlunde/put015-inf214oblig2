Yesterday (1959 film)
{{Infobox film
| name           = Yesterday
| image          = 
| caption        = 
| director       = Márton Keleti
| producer       = 
| writer         = Imre Dobozi
| starring       = Zoltán Makláry
| music          = 
| cinematography = Barnabás Hegyi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Yesterday ( ) is a 1959 Hungarian drama film directed by Márton Keleti. It was entered into the 1st Moscow International Film Festival.   

==Cast==
* Zoltán Makláry as Csendes Imre
* Ferenc Ladányi as Szabó alezredes
* Sándor Pécsi Antal Páger as Mácsay, volt foldbirtokos
* László Ungváry as Man in Mackintosh (as László Ungvári)
* János Görbe as Pandúr
* Tibor Bitskey
* Gyula Szabó as Szusza-Kis (as ifj. Szabó Gyula)
* Béla Barsi
* László Bánhidi (as Bánhidy László)
* Hilda Gobbi
* László Kozák

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 