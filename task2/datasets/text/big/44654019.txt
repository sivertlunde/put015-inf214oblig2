Down Through the Ages of the Desert
{{infobox film name = Down Through the Ages of the Desert image = Down_Age_Wiki.jpg caption = Advertising published in Motion Picture Story Magazine, September 1912 director = Sidney Olcott producer = Kalem Company writer = Gene Gauntier starring = Jack J. Clark Gene Gauntier distributor = General Film Company cinematography = George K. Hollister
| released =  
| runtime = 1000 ft
| country = United States language = Silent film (English intertitles) 
}}

Down Through the Ages of the Desert is a 1912 American silent film produced by Kalem Company and distributed by General Film Company. It was directed by Sidney Olcott with Gene Gauntier and Jack J. Clark in the leading roles.

==Cast==
* Gene Gauntier  
* Jack J. Clark  

==Production notes==
The film was shot in Luxor, Egypt.

==External links==
*    website dedicated to Sidney Olcott


 
 
 
 
 
 

 