Aasman Mahal
{{Infobox film
| name           = Aasman Mahal
| image          = Aasman_Mahal_(1965).jpg
| image_size     = 
| caption        = 
| director       = K. A. Abbas
| producer       = K. A. Abbas
| writer         = K. A. Abbas
| narrator       =  David
| music          = J. P. Kaushik
| cinematography = Ramchandra
| editing        = 
| distributor    =
| studio         = Naya Sansar
| released       =  
| runtime        = 172 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1965 Hindi social family drama film directed by K. A. Abbas.     Anwar Hussain. 

The story revolves around a Nawab, who having lost his money, still continues to live in the old traditional style, maintaining a grandeur facade. His son opposes the unrealistic life-style.

==Plot==
An elderly impoverished Nawab lives in his ancestral Haveli (Mansion). A business man wants to buy it, in order to convert it into a hotel. Though financially in a desperate state, the Nawab refuses to sell his property and clings on to his old-fashioned ideals of nobility. His son, however, is more in tune with the changing times and is in love with the daughter of the house help. 

==Cast==
* Prithviraj Kapoor
* Dilip Roy
* Surekha 
* Nana Palsikar
* Mridula Rani David Abraham Anwar Hussain
* Madhukar
* Irshad Panjatan Rashid Khan

==Production==
Cited in the Limca Book of Records to be one of the first films to be "shot on location without sets", thereby not making use of "studio" sets.    The film shooting took place in its entirety in Hyderabad. Prithviraj Kapoors acting won him a special "Honor at the Karlovy Awards".    

==Soundtrack==
One of the notable songs by composer J. P. Kaushik from the film, and described as an "introspective" and "philosophical" number, was "Main Aahein Bhar Nahin Sakta".    The lyricists were Ali Sardar Jafri and Majaz Lakhnawi, and the singers were Vijaya Majumdar, Mahendra Kapoor, Geeta Dutt and Madhukar.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|-
| 1
| "Tune Samjha Hi Nahi Kya Hai"
| Vijaya Mazumdar
| Ali Sardar Jafri
|-
| 2
| "Main Aahe Bhar Nahi Sakta"
| Mahendra Kapoor, Vijaya Majumdar
| Majaz Lakhnawi
|-
| 3
| "Khubsurat Hai Teri Tarah Shikayat Teri"
| Mahendra Kapoor
| Ali Sardar Jafri
|-
| 4
| "Ae Raat Zara Aahista Guzar"
| Geeta Dutt, Madhukar
| Ali Sardar Jafri
|-
| 5
| "Kaun Achcha Hai Yahan Kaun Kharab" 
|
|
|-
| 6
| "Mai Sharabi Hoon" 
| Mahendra Kapoor,Vijaya Majumdar
| Ali Sardar Jafri
|}

==References==
 

==External links==
* 

 

 
 
 
 
 