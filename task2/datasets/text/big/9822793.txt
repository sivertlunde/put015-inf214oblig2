Death Wish (film)
 
{{Infobox film
| name = Death Wish
| image = death wish movie poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Michael Winner Bobby Roberts
| screenplay = Wendell Mayes
| based on =   William Redfield Steven Keats
| music = Herbie Hancock
| cinematography = Arthur J. Ornitz
| editing = Bernard Gribble
| distributor = Paramount Pictures    Columbia Pictures  
| released =  
| runtime = 94 minutes  
| country = United States
| language = English
| budget = $3 million
| gross = $22,000,000  
}} Death Wish by Brian Garfield. The film was directed by Michael Winner and stars Charles Bronson as a man who becomes a vigilante after his wife is murdered and his daughter is sexually assaulted during a home invasion. It was the first of a franchise, the Death Wish (film series).

At the time of release, the film was attacked by many film critics due to its support of vigilantism and advocating unlimited punishment of criminals.  The novel denounced vigilantism, whereas the film embraced the notion. Nevertheless, the film was a commercial success and resonated with the public in the United States, which was facing increasing crime rates during the 1970s.    Since then, the film has been considered a Cult Film and has generated a strong following among fans of vigilante films, who regard it as one of the first films to introduce the "pedestrian" vigilante.

==Plot==
Paul Kersey is an architect living in Manhattan with his wife Joanna and daughter Carol Toby.  One day, Joanna and Carol are followed home from the store by a group of thugs who break into the apartment, beat Joanna and sexually assault Carol.  Paul rushes to the hospital to discover that his wife has died of her injuries.
 Tucson airport, and presents Paul with a gift, which he places into Pauls checked luggage.

Back in Manhattan, Paul learns from his son in law that his daughter is severely depressed from the trauma of the assault.  Paul visits his daughter, who is now catatonic, at the hospital.  Heartbroken, Paul and his son in law have his daughter committed to a mental hospital on Long Island for intensive treatment.  Later upon his return to Manhattan, he opens his suitcase and discovers that Ames gift is a nickel-plated .32 Colt Police Positive revolver. He loads the revolver, places it into his overcoat, and takes a late night stroll. Paul encounters a mugger, an ex-convict named Thomas Leroy Marston, who attempts to rob him at gunpoint with a .38 Smith & Wesson Model 36 revolver. Paul then shoots Marston with his own revolver, killing him.

Shocked that he just killed a human being, Paul runs home and is violently ill. But motivated by his desire for revenge, he continues to walk city streets at night looking for violent  criminals. The following night, Paul again deliberately walks through the city at night in order to invite an attack. He guns down three more men, who are robbing a defenseless old man in a vacant alley. A few nights later, two muggers see Paul on a subway. They attempt to rob him at knife-point but Paul shoots them both with the revolver.

The next scene has Paul then sitting in a sleazy Times Square coffee shop surrounded by prostitutes and assorted street people. He pays his bill to the cashier purposely revealing a wallet full of cash. He leaves followed by two thugs who have taken the bait. Yet again a robbery attempt is made. Paul shoots one but the other manages to stab him in his shoulder. As a wounded Paul stumbles off, the one who stabbed him gets away mortally wounded, dying at a hospital.

NYPD Lt. Frank Ochoa investigates the vigilante killings. His department narrows a list to men who have had a family member recently killed by muggers and who are war veterans. The public, meanwhile, is happy that somebody is doing something about crime. Ochoa soon suspects Paul. He is about to make an arrest when the District Attorney intervenes and tells Ochoa to "let him loose" in another city instead.  The D.A. and the Police Commissioner do not want the fact to get out that street crime in New York City has dropped dramatically since Paul became a vigilante.  Ochoa doesnt like the idea, but relents.  Ochoa says that he will try to "scare him off."

Paul shoots two more muggers before being wounded by a third mugger with a M1911A1 pistol at a warehouse. His gun is discovered by a young patrolman, Jackson Reilly, who hands it to Ochoa, who tells him to forget that he ever saw it and additionally tells the press that the wounded Paul is just another mugging victim. Hospitalized, Paul is ordered by Ochoa to leave New York, permanently.  Ochoa tells Paul to have his company transfer him to another city and Ochoa will dump Pauls Colt revolver in the river.  As Ochoa walks out of Pauls hospital room, Paul replies, "By sundown?".
 hoodlums harassing a young woman. He excuses himself and helps the woman. The hoodlums make obscene gestures, but Paul points his right hand like a gun and smiles, suggesting that his vigilantism will continue.

==Cast==
 
* Charles Bronson as Paul Kersey
* Hope Lange as Joanna Kersey
* Vincent Gardenia as NYPD Lt. Frank Ochoa William Redfield as Samuel "Sam" Kreutzer
* Steven Keats as Jack Toby
* Stuart Margolin as Ames Jainchill Stephen Elliott as Police Commissioner
* Kathleen Tolan as Carol Toby
* Jack Wallace as Hank
* Christopher Guest as Jackson Reilly
* Jeff Goldblum as Freak #1
* Olympia Dukakis as Cop at precinct

;Uncredited
* Paul Dooley as Cop at hospital
* Eric Laneuville as Subway mugger
* Lawrence Hilton-Jacobs as Park mugger
* Sonia Manzano as Grocery clerk
* Tom Hayden as E.R. doctor
* Denzel Washington as Alley mugger #1
 

Character actor Robert Miano had a minor role as a mugger in the film. Lawrence Hilton-Jacobs, who would later co-star on the highly successful TV show Welcome Back, Kotter, had an uncredited role as one of the Central Park muggers near the end of the film. Denzel Washington made his screen debut as an uncredited alley mugger. Actress Helen Martin, who had a minor role, subsequently appeared in the television sitcoms Good Times and 227 (TV series)|227. Sonia Manzano, Maria from Sesame Street, has an uncredited role as a supermarket checkout clerk. Christopher Guest makes one of his earliest film appearances as a young police officer who finds Kerseys gun.

==Production== 1972 novel Bobby Roberts. He was offered the chance to write a screenplay adapting one of the two novels, and chose Relentless. He simply considered it the easier of the two to turn into a film. Talbot (2006), p. 1-31  

Wendell Mayes was then hired to write the screenplay. He preserved the basic structure of the novel and much of the philosophical dialogue. It was his idea to turn police detective Frank Ochoa into a major character of the film. Talbot (2006), p. 1-31  His early drafts for the screenplay had different endings than the final one. In one, he followed an idea from Garfield. The vigilante confronts the three thugs who attacked his family and ends up dead at their hands. Ochoa discovers the dead mans weapon and considers following in his footsteps. Talbot (2006), p. 1-31  In another, the vigilante is wounded and rushed to a hospital. His fate is left ambiguous. Meanwhile, Ochoa has found the weapon and struggles with the decision to use it. His decision left unclear. Talbot (2006), p. 1-31 
 The Mechanic (1972), Scorpio (film)|Scorpio (1973), and The Stone Killer (1973). Talbot (2006), p. 1-31 

The film was rejected by other studios because of its controversial subject matter, and the perceived difficulty of casting someone in the vigilante role. Winner attempted to recruit Bronson, but there were two problems for the actor. First, his agent Paul Kohner considered that the film carried a dangerous message. Second, at this point the screenplay followed the original novel in describing the vigilante as a meek accountant—hardly a suitable role for Bronson. Talbot (2006), p. 1-31 
 Bobby Roberts to liquidate their rights. The original producers were replaced by Italian film mogul Dino De Laurentiis.  De Laurentiis convinced Charles Bluhdorn to bring the project to Paramount Pictures. Paramount purchased the distribution rights of the film in the United States market, while Columbia Pictures licensed the distribution rights for international markets. De Laurentiis raised the 3-million-dollar budget of the film by pre-selling the distribution rights. 

With funding secured, screenwriter Gerald Wilson was hired to revise the script. His first task was changing the identity of the vigilante to make the role more suitable for Bronson. "Paul Benjamin" was renamed to "Paul Kersey". His job was changed from accountant to architect. His background changed from a World War II veteran to a Korean War veteran. The reason for him not seeing combat duty changed from serving as an army accountant to being a conscientious objector. Talbot (2006), p. 1-31  Several vignettes from Mayes script were deemed unnecessary and were therefore deleted. Talbot (2006), p. 1-31 
 Western film Wild West, taking place in Tucson, Arizona. The final script had the vigilante making the occasional reference to Westerns. While confronting an armed mugger, he asks him to draw. When Ochoa asks him to leave town, he asks if he has until sundown to do so. Talbot (2006), p. 1-31  The killing in the subway station was supposed to remain off-screen in Mayes script, but Winner himself decided to turn this into an actual, brutal scene. Talbot (2006), p. 1-31 
 Loews Theater of New York City. Talbot (2006), p. 1-31 
==Soundtrack==
  The Spook Head Hunters. She gave me Head Hunters, which was staggering. And I said, Dino, never mind a cheap English band, well have Herbie Hancock. Which we did."

Hancocks theme for the film was quoted in "Judge, Jury and Executioner," a 2013 single by Atoms for Peace (band). The title of the song is apparently a reference to a phrase on the theatrical release poster for Death Wish, shown above, and the bassline of "Judge, Jury and Executioner" strongly resembles that of the movies main theme.

==Critical reception==
Death Wish received mixed to extremely negative reviews upon its release, due to its support of vigilantism, but it had an impact on U.S. audiences and began widespread debate over how to deal with rampant crime. The films graphic violence, particularly the brutal rape scene of Kerseys daughter as well as the explicit portrayal of Bronsons premeditated slayings, was considered exploitative, but realistic in the context of an urban U.S. atmosphere of rising crime rates.  

Many critics were displeased with the film, considering it an "immoral threat to society" and an encouragement of antisocial behavior. Vincent Canby of the New York Times was one of the most outspoken writers, condemning Death Wish in two extensive articles.   
 Death Sentence, which was published a year after the films release. In later years, the film would be liked for its disturbing, serious view of one mans violent war on crime. Bronson defended the film: he felt it was intended to be a commentary on violence and was meant to attack violence, not romanticize it. And even many critics began to like the original film more than the sequels, which were more exploitative and contrived. 

The film holds a 67% rating based on 24 reviews with an average rating of 5.8/10 on the review aggregate website Rotten Tomatoes. 

* AFIs 100 Years... 100 Heroes and Villains:
** Paul Kersey – Nominated Hero
* AFIs 100 Years... 100 Thrills - Nominated

==Impact and influence== The Great Escape. Bronson became an American film icon who experienced great popularity over the next twenty years.
* In the series later years, the Death Wish franchise became a subject of parody for its high level of violence and the advancing age of Bronson (an episode of The Simpsons named "A Star Is Burns" showed a fictional advertisement for Death Wish 9, consisting of a bed-ridden Bronson saying "I wish I was dead"). However, the Death Wish franchise remained lucrative and drew support from fans of exploitation cinema. The series continues to have widespread following on home video and is occasionally broadcast on various television stations within the US and in Europe. The One That Got Away", Roger dresses similar to the leading protagonist when he plans revenge on an alternate personality of himself. He tells an alternate character that Death Wish was a movie, when his reference via outfit is misconstrued.
* Bronson is mentioned in the 1997 Notorious B.I.G classic "Kick in the Door". In the song, Biggie proclaims: "Sold more powder than Johnson and Johnson/Tote steel like Bronson, vigilante/You wanna get on son, you need to ask me."
* In the 1990 Spider-Man comic arc "The Death of Jean DeWolff", Bronson makes a small appearance in one panel, holding a newspaper with the headline "vigilante".
* A clip from the film of Goldblums character yelling "Goddamn rich cunt!" is often played on The Opie and Anthony Show.
* In 2011 Takis Magazine profiled a white nationalist author and blogger who adopted the pseudonym "Paul Kersey" after the Bronson character. 
* Curtis Sliwa described Bernhard Goetz as "Charles Bronson in Death Wish."  

==Home media==
The film was first released on VHS and LaserDisc in 1980. It was later released on DVD in 2001 and 2006. Currently, the VHS, laserdisc, and DVDs are out of print. A 40th Anniversary Edition was released on Blu-ray Disc|Blu-ray in 2014. 

==Remake==
In late January 2012, The Hollywood Reporter stated that a remake was confirmed and would be written and directed by Joe Carnahan. 

==Sources==
*  

==References==
 
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 