Djantoeng Hati
{{Infobox film
| name      = Djantoeng Hati
| image     = Poster djantoeng hati.jpg
| image size   = 125
| border     = 
| alt      = 
| caption    = Poster
| director    =Njoo Cheong Seng
| producer    ={{plainlist|
*Fred Young
*SI Liem
}}
| writer     = Njoo Cheong Seng
| starring    = {{plainlist|
*Rr Anggraini
*Ariati
*Chatir Harro
}}
| music     = R Koesbini
| cinematography = The Teng Chun
| editing    = 
| studio     = Majestic Pictures
| distributor  = 
| released    =  
| runtime    = 
| country    = Dutch East Indies Malay
| budget     = 
| gross     = 
}}
Djantoeng Hati (Heart and Soul) is a 1941 film from the Dutch East Indies directed by Fred Young. A tragedy warning against modernity,  it starred A Sarosa, Rr Anggraini, and Ariati

==Plot==
Two students&nbsp;– the traditional Karina (Rr Anggraini) and metropolitan Roesdjana (Ariati)&nbsp;– are in competition for Karinas husband Sobari (Chatir Harro). Eventually Karina wins out.  

==Production== Fred Young and SI Liem of Majestic Film.  It was the companys first production.  It starred A Sarosa, Rr Anggraini, Soerip, and Ariati;  Njoos wife Fifi Young, who had previously acted in all his films, was unable to act owing to health reasons.   Most of its stars were of noble (ningrat) descent, an attempt to draw middle-class audiences,  while the story focused on students to draw educated viewers. 

The black-and-white film was shot by The Teng Chun, one of Fred Youngs friends from when he studied in the United States; The also allowed Majestic to use his studios in Batavia (modern-day Jakarta).  Thes brother Teng Liong served as sound director. The film featured seven kroncong songs by R Koesbini which were sung by the main cast. 

==Release and reception== Japanese occupation the following year. 

Djantoeng Hati is likely a lost film. The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==References==
Footnotes
 

Bibliography
 
* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
* {{cite web
  | title = Djantoeng Hati
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-d013-41-163161_djantoeng-hati
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate =25 July 2012
  | archiveurl = http://www.webcitation.org/69QsKAeFn
  | archivedate = 25 July 2012
  | ref =  
  }}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
 
 

 
 
 