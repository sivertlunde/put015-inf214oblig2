South American George
{{Infobox film
| name = South American George 
| image = "South_American_George"_(1941).jpg
| image_size =
| caption = Swedish theatrical poster
| director = Marcel Varnel
| producers = Ben Henry Marcel Varnel  
| writer = Leslie Arliss   Norman Lee   Austin Melford (Additional dialogue) 
| narrator =
| starring = George Formby   Linden Travers   Enid Stamp-Taylor   Felix Aylmer
| music = Harry Bidgood
| cinematography = Arthur Crabtree
| editing = Edward B. Jarvis
| studio =  Columbia British Productions 
| distributor = Columbia Pictures Corporation 
| released = 27 December 1941 (UK)
| runtime = 92 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website = 1941 Cinema directed by George Formby Herbert Lomas. produced by Columbia (British) Productions.

==Synopsis==
A press agent hurries to bring in a substitute after a South American opera star flops. A lookalike takes over from the tenor, but chaos ensues when the bogus singer finds himself hunted by paid assassins.  

==Cast==
*George Butters/Gilli Vanetti ...George Formby
*Carole Dean ...Linden Travers
*Frances Martinique ...	Enid Stamp-Taylor
*Enrico Richardo ...Jacques Brown
*Mr Appleby ...	Felix Aylmer
*Swifty ...	Ronald Shiner
*Slappy ...	Alf Goddard
*George White ...Gus McNaughton
*Mrs Durrant ...Mavis Villiers Herbert Lomas Cameron Hall
*Mr Durrant ...	Eric Clavering
*Mrs Butters ...Beatrice Varley

==Critical reception==
TV Guide wrote, "Formbys comic talents give the unlikely story a few fun moments, though the film is for the most part a hit-and-miss effort."  

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 
 