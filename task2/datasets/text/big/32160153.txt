Doctor Love (film)
{{Infobox film
| name           = Doctor Love
| image          = Dr-Love-Malayalam.jpg
| caption        = Film poster
| alt            =
| director       = K. Biju
| producer       = Joy Thomas Sakthikulangara
| writer         = K. Biju
| narrator       = Asif Ali Bhavana Ananya Ananya Bhagath Manuel
| music          = Vinu Thomas Gopi Sunder (background score)
| cinematography = Shaji
| editing        =
| studio         = Jithin Arts
| distributor    = Maxlab Cinemas and Entertainments
| released       =  
| runtime        = 157 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Doctor Love or Dr. Love is a Malayalam romantic comedy film directed by K. Biju. The film was produced by Joy Thomas Sakthikulangara, and stars Kunchacko Boban, Bhavana Menon|Bhavana, Ananya (actress)|Ananya, Manikkuttan, Rejith Menon, Salim Kumar, and others. The music of the film was arranged by Vinu Thomas. Maxlab Studios and Entertainment distributed the film. It was released on September 9, 2011.

==Plot==
Dr. Love is the story of Vinayachandran (Kunchacko Boban), a romantic novel writer who helps people with love issues. He goes to a college to settle a love matter and becomes a hero. He is given the title Doctor Love - Romance Consultant by all the students in the campus. He finds out that Ebin  (Bhavana (actress)|Bhavana) is his childhood best friend. They parted by lifes ways. Problems faced by Vinayachandran on campus form the rest of the story.

==Partial cast==
* Kunchacko Boban as Vinayachandran Bhavana as Ebin (Campus Devil) Ananya as Gauri Shari
* Bhagath Manuel as Sudhi
* Rejith Menon as Martin
* Hemanth Menon as Roy
* Manikuttan as Vengidi
* Shraavan 
* Aju Varghese 
* Jikku as susheelan
* Salim Kumar As shreya
* Bindu Panicker As ajithakumari teacher
* Vidhya Unni As Manju
* Baby Ester as Ebin Jr

==Soundtrack==
{{Infobox album
| Name        = Doctor Love
| Type        = soundtrack
| Artist      = Vinu Thomas
| Cover       = 
| Released    = 
| Recorded    = 
| Genre       = Film soundtrack
| Length      = 
| Label       = 
| Producer    = Vinu Thomas 
| Last album  = 
| This album  = Doctor Love (2011)
| Next album  = 
}}

This album was composed by Vinu Thomas in 2011. The background score was composed by Gopi Sunder. Lyrics are by Vayalar Sarath Chandra Varma.

{| class="wikitable"
|-
! Song !! Singer!! Length !! Picturization
|-
| Ormakal Verodum || Karthik ||  || 
|-
| Nannavoola || Ranjith (singer)|Ranjith, Benny Dayal, Vinu Thomas, Anju Joseph, Kids Chorus ||  || 
|-
| Ninnedonikkula || Riya Raju ||  || 
|-
| Aakasham Doore || Najim Arshad & Vivekanandan ||  ||
|-
| Paalappoo || Najim Arshad ||  ||
|-
| Neraane || Ranjith (singer)|Ranjith, Franco, Balu Thankachan, Vipin Xavier, Vineeth Sreenivasan, CJ Kuttappan, Namitha ||  ||
|-
| Kai Onnadichen || Vineeth Sreenivasan, Rimy Tomy, CJ Kuttappan ||  ||
|}

==Critical reception==
Rediff.com gave the film a rating of 2 out of 5 and said "Dr Love is yet another campus film".  Sify also rated the movie at 2/5 and said "Dr.Love tries to present all those masala once again, but it looks amateurish to the core. Watch it at your own risk please".  Indiaglitz  said "All in all, Dr.Love has some interesting moments and the ensemble star cast pitches in real performances too. On the whole, the movie could be an ideal popcorn flick targeted to strike a chord with the youth and those who relish candy floss and madcap entertainers. If you are not looking for wisdom and rationale in a light-hearted entertainer, we are sure you will savour this campus carnival" and gave 2.75 stars out of 5. 

==References==
 

==External links==
*  
*  


 
 
 
 
 