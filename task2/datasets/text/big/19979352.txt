Mann Vasanai
{{Infobox film
| name = Mann Vasanai
| image = Mann_Vasanai_dvd.jpg
| caption = Official DVD Box Cover
| director = P. Bharathiraja
| producer = Chithra Lakshmanan
| writer = Panju Arunachalam
| screenplay = P. Bharathiraja
| story = P. Kalaimani
| narrator = Pandiyan Revathi Ramanathan Senathipathy Vijayan Vinu Chakravarthy Anitha Badan Kanthimathi Sathya Srilatha Y.vijaya
| music = Ilaiyaraaja   
| cinematography = B. Kannan
| editing = V. Rajagopal
| studio = Gayathri films
| distributor = Gayathri films
| released = 29 July 1983
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}}

Mann Vasanai is a 1983 Indian Tamil language film starring Revathi, Pandiyan, Vinu Chakravarthy, Kantimathi, and Y. Vijaya. It was remade as Mangammagari Manavadu in 1984 directed by Kodi Ramakrishna in Telugu language.  It is also highly successful.

==Plot==
Movie produced by Chithra Lakshmanan under the banner of Gayathri Films with a huge success.

Love, village feud and circumstances lead a young man to abandon his village and lover and head to the armed forces. Upon his return, his lover realizes that things are never going to be the same again.

==Cast== Pandiyan
*Revathi Ramanathan
*Senathipathy
*Suryakanth Vijayan
*Vinu Chakravarthy
*Anitha Badan
*Kanthimathi
*Sathya
*Srilatha
*Y.vijaya

==Production==
Pandiyan was selling bangles in Madurai, India when he was spotted by director Bharathiraja who offered him the lead role in the film. Asha was spotted by notable Tamil director Bharathiraja, who was searching for a new heroine for his next film, and went on to play the heroine of the film. Her name Asha kutty was changed to a screen name Revathi.  

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="6" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-  style="background:#ccc; text-align:center;" Duration  (mm:ss) 
|-
|Adi Kukkamma Malaysia Vasudevan, B. S. Sasirekha Panju Arunchalam
|4:23
|-
|Anantha Thenn Malaysia Vasudevan, S. Janaki
|4:28
|-
|Arisi Kuthum Malaysia Vasudevan, S. P. Sailaja
|M. G. Vallabhan
|4:34
|-
|Indha Bhoomi Ilaiyaraaja
|rowspan=2|Gangai Amaran
|2:30
|-
|Mookarai Mokkamma Malaysia Vasudevan, B. S. Sasirekha
|4:24
|-
|Poththi Vachcha (Duet)
|S. P. Balasubrahmanyam, S. Janaki Vairamuthu
|4:29
|-
|PothiVacha (Lady)
|S. Janaki Vairamuthu
|4:29
|-
|Pothi Vacha (Sad)
|S. Janaki Vairamuthu
|4:33
|-
|Vangadi Vangadi Malaysia Vasudevan
|
|4:35
|}

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 


 