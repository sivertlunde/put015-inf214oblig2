Dick Tracy Meets Gruesome
{{Infobox film
|name=Dick Tracy Meets Gruesome
|image=Dick Tracy Meets Gruesome FilmPoster.jpeg
|caption=Theatrical Poster John Rawlins
|producer=Herman Schlom
|writer=Characters:   Robert E. Kent
|starring=Boris Karloff Ralph Byrd
|music=Paul Sawtell
|cinematography=Frank Redman
|editing=Elmo Williams
|studio=RKO Radio Pictures
|distributor=RKO Radio Pictures
|released= 
|runtime=65 minutes
|country=United States
|language=English
|budget=
|gross=
}} thriller film starring Ralph Byrd, Anne Gwynne, and Boris Karloff. The film is the fourth and final installment of the Dick Tracy#Early feature films|Dick Tracy film series released by RKO Radio Pictures. Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 322-323 

==Plot==
Just out of jail, Gruesome goes to the Hangmans Knot saloon, where his old crime crony Melody is now playing piano. Gruesome takes him to a plastics manufacturer, where X-Ray and a mysterious mastermind are in possession of a secret formula and hatching a sinister plot.

Ignoring a warning not to touch anything, Gruesome sniffs a nerve gas that paralyzes him. He appears to be dead and is taken to the city morgue.

Dick Tracy is at headquarters speaking with college professor Dr. Tomic, a scientist who suspects someone has been following him. At the morgue, Tracys sidekick Pat has his back turned when Gruesome wakes up and knocks him out. Pat describes him to Tracy as looking a lot like the actor Boris Karloff.

At a bank where Tess Trueheart happens to be, Gruesome and Melody use the nerve gas to incapacitate the customers and security guard. They rob the place of more than $100,000 and shoot a cop on the sidewalk before Tracy and his men arrive. Gruesome demands half of the loot from X-Ray .... or else.

Tracy tries to learn the secret of the formula from Dr. Tomics top assistant, Professor Learned, before going after Gruesome and his gang. It all ends in a shootout, with Gruesome shot by Tracy, and then back at headquarters, where Tracy ends up frozen by nerve gas just as hes about to kiss Tess.

==Cast==
* Ralph Byrd as Dick Tracy - The tough, square-jawed detective
* Anne Gwynne as Tess Truehart - Tracys girlfriend, who witnesses the bank robbery
* Boris Karloff as Gruesome - A corpse-like gangster.
* Skelton Knaggs as Rudolph X-Ray - Gruesomes spectacled henchman
* Edward Ashley as Dr. Lee Thal -  an interested party.
* June Clayworth as Dr. I.M. Learned - Prof. Tomics assistant
* Lyle Latell as Pat Patton - Tracys bumbling sidekick
* Tony Barrett as Melody Fiske - A greedy, piano-playing thug Jim Nolan as Dan Sterne - A nosy newspaper reporter
* Joseph Crehan as Chief Brandon - Tracys reliable boss
* Milton Parsons as Dr. A. Tomic - a State U. physicist

==Legacy==
*In 2007, Dick Tracy Meets Gruesome was shown on the horror hosted television series Cinema Insomnia.    Apprehensive Films later released the Cinema Insomnia episode onto DVD.    Mike Curtis and artist Joe Staton introduced a version of the Gruesome character into the Dick Tracy comic strip.   They introduced a character inspired by Melody (Mel. O. Dee) on October 27th of that year.  
*American band Poet Named Revolver, from Nashville, TN, released an album called "Meets Gruesome". Recorded in 2007, first released in 2008, and reissued in 2014 by No Kings Record Cadre, with visual references to the film. 

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 