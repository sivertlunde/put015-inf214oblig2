Manmadhan (film)
 
{{Infobox film
| name           = Manmadhan
| image          = Manmadhan.jpg caption         = Film poster
| director       = A. J. Murugan

| writer         = Silambarasan Balakumaran (dialogues)
| screenplay     = Silambarasan
| story          = Silambarasan Santhanam Atul Kulkarni Yana Gupta Brinda Parekh
| producer       = S. K. Krishnakanth
| music          = Yuvan Shankar Raja
| cinematography = S.Murthy   R. D. Rajasekhar Anthony
| studio         = Indian Theatre Production
| distributor    =
| released       = 12 November 2004
| runtime        = 165 min
| country        = India
| language       = Tamil
}}
 Santhanam playing supporting roles and guest appearances by Mandira Bedi as a psychologist, Brinda Parekh as playgirl and Yana Gupta as a club dancer. The films score and soundtrack are composed by Yuvan Shankar Raja. The film released on 12 November 2004 and was later dubbed in Telugu as Manmadha.The film was declared Blockbuster at the box office at the end of a 275 days theatrical run.

==Plot==
 
A violently depressed Madhan (Silambarasan Rajendar|Silambarasan) arrives at a hospital seeking psychiatric counselling. This proves to be a ruse, however, and he craftily seduces the psychiatrist (Mandira Bedi). Subsequently, he kills her.

Madhan Kumar is shown to be an accounts manager who also studies music. A shy college student Mythili (Jyothika) has a nightmare of Madhan (whom she has never met before) raping her,  and fears him when she meets him in real life. Intrigued by her behaviour, he approaches and befriends her. She soon  realizes that he is by all accounts a good person, and starts to love him. However, every night Madhan stalks various women who are morally corrupt, seduces and kills them. Every time he embraces a woman with the intent of seducing her, his nose begins to bleed (a characteristic trait). Assistant Commissioner Deva (Atul Kulkarni) is assigned to bring the serial killer to justice.

Mythili eventually discovers proof that Madhan is the killer and rapist and hands him over to the police. Held in police custody, Madhan reveals that the killer is actually his younger brother Madhan Raj (also Silambarasan). Madhanrajs motives are explained in a flashback:

Madhan Raj, a simple village youth arrives in Coimbatore to attend college, sharing a room with an irreverent student Bobby (Santhanam (actor)|Santhanam). He is approached by his classmate Vaishnavi (Sindhu Tolani) and soon starts to love her unconditionally. His love is fueled by his innocence and ethical values. The relationship progresses as far as a marriage proposal; however, a squabble starts as Madhanraj suspects an affair between Vaishnavi and her classmate Seenu. He arrives at Vaishnavis house to ask her forgiveness. By chance, Madhanraj looks into her bedroom window and finds her sleeping with Seenu. He also overhears her saying that she never loved him, and merely accepted his proposal to divert his suspicion of her affair.

Heartbroken and revolted, Madhanraj confronts her, saying he knows the truth. When she denies it, he lashes out and accidentally kills her. Initially he grieves over his act, but realizes that she deserved it for cheating, and intentionally kills Seenu as well. He arrives at his brother Madhan Kumars house and reveals his crime.

Madhan Kumar ends his flashback, and is released. Mythili apologizes for believing him to be a killer, and admits that she loves him deeply. However, the actual events that spurred the birth of Manmadhan are revealed (only to the audience):

After killing Vaishnavi and Seenu, Madhanraj was, in fact, remorseful and depressed over the events that led to this. He confesses to his brother and soon afterwards, locks himself in his room and prepares to commit suicide. Madhan Kumar watches horrified and helpless, as his younger brother hangs himself and loses his life. Madhan Kumar enters the room and embraces his brothers corpse, and suffers a nosebleed. He swears to avenge his brothers death by killing unfaithful women everywhere; thus it was Madhan Kumar himself who became . It is hinted that his exploits will continue.

==Cast==

{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|- Silambarasan || Madhan Kumar/ Madhanraj "Mottai Madhan"/ Manmadhan
|-
| Jyothika || Mythili
|-
| Goundamani || "Puncture" Pandian
|-
| Atul Kulkarni || Assistant Commissioner Deva
|-
| Sindhu Tolani || Vaishnavi
|- Santhanam || Bobby
|-
| Mandira Bedi || Psychiatrist (Guest appearance)
|-
| Yana Gupta || Item number
|-
| Mayuri || Malathi
|}

==Other Crew==
* Dialogue: Balakumaran
* Art direction: Rajeevan
* Stunts: Kanal Kannan
* Executive production: K. K. Ravi & S. Murugan

==Box Office==
The film received largely positive reviews from critics and grand welcome from general audience collected   15+ crore at the box office and declared Blockbuster at the box office.

==Critical Reception==
The film received largely positive reviews from critics mainly about its plot and Blockbuster at the box office.

==Soundtrack==
{{Infobox album
|  Name        = Manmadhan
|  Type        = soundtrack
|  Artist      = Yuvan Shankar Raja
|  Cover       = 
|  Background  = Gainsboro
|  Released    = 1 July 2004
|  Recorded    = 2004
|  Genre       = Soundtrack
|  Length      = 33:19 22:11 (2nd release)
|  Label       = Bayshore
|  Producer    = Yuvan Shankar Raja
|  Reviews     = 
|  Last album  = 7G Rainbow Colony (2004)
|  This album  = Manmadhan (2004)
|  Next album  = Bose (film)|Bose (2004)
}}
The soundtrack, composed by Yuvan Shankar Raja, released on 1 July 2004 and features 6 songs. The lyrics were penned by Vaali (poet)|Vaali, Snehan, Pa. Vijay and Na. Muthukumar. The singer include the films lead actor Silambarasan Rajendar|Silambarasan, Anushka Manchanda from the girl-pop group Viva (band)|Viva! and rapper Blaaze, who all sung for the first time under Yuvan Shankar Rajas direction. After the release of the film, several bonus tracks, that featured in the film, but not in the soundtrack, were released again as a soundtrack, which includes pieces of the film score and the earlier released songs. All the tracks were composed by Yuvan Shankar Raja.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration !! Lyricist!! Notes
|- 1 || "Thathai Thathai" ||   ||
|- 2 || "Manmadhane Nee" || Sadhana Sargam || 4:34 || Snehan ||
|- 3 || "Oh Mahire" ||   || Not featured in the film
|- 4 || "Vanamenna" ||   ||
|- 5 || "En Aasai Mythiliye" ||   || Remixed from the Tamil film Mythili Ennai Kadhali, composed by Vijaya T. Rajendar
|- 6 || "Kadhal Valarthen" ||   ||
|}

{{tracklist
| headline     = Bonus tracks (Second release)
| extra_column = Singer(s)
| all_music    = Yuvan Shankar Raja
| total_length = 22:11
| title7       = Sedi Sedi Onnu
| extra7       = Silambarasan
| length7      = 1:34
| title8       = Sedi Sedi Onnu (Music)
| extra8       = Instrumental
| length8      = 0:35
| title9       = Kannale
| extra9       = Yuvan Shankar Raja
| length9      = 1:12
| title10      = Sedi Sedi Onnu 2
| extra10      = Silambarasan
| length10     = 0:49
| title11      = Fight Theme
| extra11      = Instrumental
| length11     = 0:47
| title12      = Pesamalae Mugam
| extra12      = Yuvan Shankar Raja
| length12     = 1:44
| title13      = Sedi Sedi Onnu 3
| extra13      = Silambarasan
| length13     = 1:19
| title14      = Thathai Thathai 2
| extra14      = Silambarasan, Clinton Cerejo, Blaaze, Vasundhara Das
| length14     = 5:52
| title15      = Manmadhan Theme 1
| extra15      = Instrumental
| length15     = 2:35
| title16      = Manmadhan Theme 2
| extra16      = Instrumental
| length16     = 1:34
| title17      = Manmadhan Theme 3
| extra17      = Instrumental
| length17     = 0:59
| title18      = Manmadhan Theme 4
| extra18      = Instrumental
| length18     = 1:02
| title19      = Manmadhan Theme 5
| extra19      = Instrumental
| length19     = 1:24
| title20      = Manmadhan Theme 6
| extra20      = Instrumental
| length20     = 0:45
}}

==External links==
*  

==References==
 

 
 
 
 
 
 
 
 
 