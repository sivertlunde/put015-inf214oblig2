V2: Dead Angel
{{Infobox film
| name           = V2: Dead Angel
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Aleksi Mäkelä
| producer       = Markus Selin
| screenplay     = Marko Leino
| based on       =  
| starring       = Juha Veijonen Hannu-Pekka Björkman Jussi Lampi Johanna Kokko Lotta Lindroos
| music          = Lauri Porra
| cinematography = Pini Hellstedt
| editing        = Dan Peled
| studio         = Solar Films Buena Vista
| released       =  
| runtime        = 92 minutes
| country        = Finland
| language       = Finnish
| budget         = €1.5 million 
| gross          = €1,541,266 
}} Finnish crime film directed by Aleksi Mäkelä. It is based on the novel Jäätynyt enkeli by Reijo Mäki. In the film private detective Jussi Vares investigates the homicide of a young woman that occurred in the city of Pori.  It is the second film in the Vares (film series)|Vares series and the last film to feature Juha Veijonen as Vares.

== Cast ==
* Juha Veijonen as Jussi Vares
* Hannu-Pekka Björkman as Jakke Tienvieri
* Jussi Lampi as Veikko Hopea
* Johanna Kokko as Mirjami Sinervo
* Lotta Lindroos as Lila Haapala
* Kari-Pekka Toivonen as Taisto Pusenius
* Pekka Huotari as Harry Jalkanen
* Kari Väänänen as Usko Saastamoinen
* Seppo Pääkkönen as Nils Hellman
* Matti Onnismaa as Matti Urjala
* Vesa Vierikko as Aarno Kaitainen
* Tommi Korpela as Tom Marjola
* Jasper Pääkkönen as Dante Hell
* Kari Hietalahti as Kullervo Visuri
 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 