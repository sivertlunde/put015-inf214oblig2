One Perfect Day
 
{{Infobox film
| name           = One Perfect Day
| image          = One Perfect Day.jpg Paul Currie
| producer       = Paul Currie, Phil Gregory
| writer         = Paul Currie, Chip Richards Nathan Phillips, Dawn Klingberg Josh G. David Hobson Paul van Dyk
| cinematography = Gary Ravenscroft
| editing        = Amelia Ford Gary Woodyard Roadshow Entertainment (Australia)
| released       =  
| runtime        = 106 minutes
| country        = Australia
| language       = English
| budget         =
}} 
One Perfect Day is an Australian film released in 2004.

==Plot== French painter Henri Matisse.
 ambient noises classical music and displays this by bringing a homeless woman living in the Underground on stage for a concert.

A sympathetic professor decides that he is the type of innovative artist needed to revive a dying opera artform. Having shocked operas establishment, he returns home to Melbourne on the death of his younger sister Emma, who suffers a fatal overdose after experimenting with drugs at a rave dance party. He discovers a CD of her own mixes and decides to enter the genre of electronic music to follow the path she was pursuing in the hopes of discovering more about his sister and how she became involved in this dance world.
Emmas death acts as a catalyst that drives Tommy and his girlfriend Alysse apart. In despair, Alysse falls prey to a sleazy entrepreneur named Hector Lee who owns a club called Trance-Zen-Dance and who is also a drug dealer. Hector Lee has a young assistant called Trig who is a VJ, and is always getting new footage and talent.

==Cast==
*Dan Spielman .... Tommy Matisse
*Leeanna Walsman .... Alysse
*Kerry Armstrong .... Carolyn
*Abbie Cornish .... Emma
*Rory Williamson .... Noah Nathan Phillips .... Trig
*Syd Brisbane .... Hamish
*Frank Gallacher .... Malcolm
*Alex Menglet .... May
*Andrew Howard .... Hector Lee
*Nathan Wentworth .... Stevie
Jay Smeaton  ......  Party dancer 2

==Crew== Paul Currie director & writer
*Chip Richards... writer
 David Hobson ... Composer Original music score
*Paul van Dyk... Additional Music Score

==Soundtrack==
The soundtrack was released on 15 February 2004 by Universal Music, and debuted at number forty-six on the Australian album charts in the week beginning 23 February 2004 and manage to reach to number twenty-two. The title track, sung by Lydia Denker, debuted at number thirty-five on the Australian single charts. There are two versions of the soundtrack:

;One-disc version track listing
# "Blow Wind Blow" - Rairbirds  
# "Break It" (Down James Brown) - The Offcuts
# "Krazy Krush" - Ms. Dynamite Blur
# "What Is the Problem" - Graffiti
# "Dont Let the Man" - Fatboy Slim
# "Are You Ready for Love" (Radioslave mix) - Elton John One Perfect Day" - Lydia Denker
# "No One Knows" - Queens of the Stone Age Underworld
# "Late at Night" - Futureshock
# "All of Me" - Groove Armada
# "Schoolgirl" - Sandrine
# "Feathers and Down" - The Cardigans
# "Hectors Demise" - Dust Brothers
# "Ordinary World" (Josh G. Abrahams remix) - Mandy Kane
# "Pictures of You" (Paul Mac remix) - after The Cures 1989 album Disintegration (The Cure album)|Disintegration
# "One and the Same" (One Perfect Day Remix) - after Rob Dougans 2002 album Furious Angels

===Two-disc version track listing===
;Disc 1:
# "Blow Wind Blow" - Rairbirds  
# "Break It" (Down James Brown) (Josh G. Abrahams Remix) - The Offcuts
# "Krazy Krush" - Ms. Dynamite
# "What Is the Problem?" &ndash; Graffiti
# "Dont Let the Man Get You Down" - Fatboy Slim
# "Are You Ready for Love" (radio slave remix) - Elton John
# "One Perfect Day" - Lydia Denker
# "Pictures of You" (Paul Mac remix) - after The Cures 1989 album Disintegration
# "Two Months Off" - Underworld
# "Late at Night" - Futureshock
# "All of Me" - Groove Armada
# "Schoolgirl" &ndash; Sandrine
# "Feathers and Down" - The Cardigans
# "Hector’s Demise" - The Dust Brothers
# "No One Knows" (Unkle Remix) - Queens of the Stone Age
# "Ordinary World" (Josh Abrahams remix) - Mandy Kane
# "One and the Same" (One Perfect Day remix) - after Rob Dougans 2002 album Furious Angels
# "Design Music" - Sven Väth

;Disc 2:
# "The Man with the Red Face" - Laurent Garnier
# "Horsepower" - C.J. Bolland
# "Graffiti Part Two" - Stereo MC’s
# "No Transmission" - Lhb
# "To Know" - NuBreed Lamb
# "Just Show Me" - Grandadbob Euroboy
# "Sly-ed" - Man With No Name
# "Drop Some Drums" - (Love) Tattoo Orbital Featuring Lisa Gerrard
# "One Perfect Day" (epic dance mix) - Ali Mcgregor David Hobson and Lisa Gerrard
# "Final Moments" - David Hobson
# "One Perfect Day" (orchestral mix) - David Hobson & Orchestra
# Ride - Helmut

==Box Office==
One Perfect Day grossed $1,152,011 at the box office in Australia,. 

==See also==
*Cinema of Australia

==References==
 

==External links==
*  
*  
*  
* 

 
 
 
 
 
 
 
 
 