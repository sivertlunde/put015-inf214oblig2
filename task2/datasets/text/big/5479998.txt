Murders in the Rue Morgue (1932 film)
{{Infobox film
  | name = Murders in the Rue Morgue
  | image = Murdersintheruemorgueposter.jpg
  | caption = 
  | director = Robert Florey
  | producer = Carl Laemmle, Jr. Tom Reed (screenplay) Dale Van Every (screenplay) Robert Florey (adaptation) John Huston (add. dialogue) Ethel M. Kelly (uncredited) Leon Ames Bert Roach Brandon Hurst Arlene Francis
  | music =
  | cinematography = Karl W. Freund
  | editing = Miton Carruth
  | distributor = Universal Pictures
  | released = February 21, 1932
  | runtime = 60 mins
  | language = English
  | country = United States
  | budget = $186,090 Michael Brunas, John Brunas & Tom Weaver, Universal Horrors: The Studios Classic Films, 1931-46, McFarland, 1990 p32 
  | preceded_by = 
  | followed_by = 
}} 1932 horror Universal to cut its running time from 80 minutes to 61 minutes. 

This film was produced as a compensatory package for Lugosi and Florey, after both were dropped from Frankenstein (1931 film)|Frankenstein (1931 in film|1931). Lugosi had originally been cast as Dr. Frankenstein, and the film was to be directed by Florey, who had been developing the coveted project. Lugosi was subsequently demoted to play the mute Frankensteins monster|monster, however, which he claimed to have turned down. For unclear reasons, Florey was replaced as director by James Whale.

Box office results for Murders in the Rue Morgue were disappointing, and Lugosis original Universal contract for Dracula was not extended. Today, however, the film is generally well-regarded by critics and is considered a cult classic. 

==Plot==
The film is set in 1845 in Paris. A mad scientist, Dr. Mirakle (Bela Lugosi), abducts young virgin women and injects them with ape blood, in order to create a mate for his talking sideshow ape Erik (Charles Gemora, the gorilla performer).
 Leon Ames—credited as Leon Waycoff—in the role of Poes standard detective icon, C. Auguste Dupin), his fiancee Camille LEspanaye (Sidney Fox, in the role of an original character in the short story), and their friends Paul (Bert Roach) and his girl Mignette (silent film actress Edna Marion, in her last film role) visit carnival sideshows, including Mirakles sideshow where he exhibits Erik. Both master and servant are enchanted by Camille, whom Mirakle plans to become Eriks mate. He invites her to come and take a closer look at Erik, who grabs Camilles bonnet. Dupin tries to get it back, when Erik tries to strangle him. Mirakle backs him off and offers Camille to replace the bonnet. But Camille is reluctant and suspicious to give the doctor her address, so, when they leave, Mirakle orders his servant Janos (Noble Johnson) to follow her.

 
One of Mirakles victims, a prostitute, is found dead in a river (a homage to another Dupin-Poe tale, "The Mystery of Marie Roget"), and is fished out and taken to the police station. Dupin wants to examine the girls blood, but the morgue keeper (DArcy Corrigan) wont allow. A bribe convinces him to draw some of the blood himself and deliver it to Dupin the next day. Dupin discovers in the blood a foreign substance, also found in the blood of other victims.
 German Franz Odenheimer (Herman Bing) and a Dane (Torben Meyer). All of them state that they had heard Camille screaming and also someone else talking in a strange language (The German thinks it was Italian, the Italian thinks it was Danish and the Dane thinks it was German). Camilles mother is found dead, stuffed in the chimney (the fate of Camille herself in the original story) and her hand clutching ape fur. Dupin points out from the fur that Erik himself may be involved.

The police, along with Dupin, run to Mirakles hideout. Before they arrive, Erik turns against his master and strangles him. He grabs Camille when the police arrive and they chase him. The police shoot Janos in the back when he tries to keep them at bay. Erik, pursued, is cornered on the roof of a small dockside house. He confronts Dupin, who shoots the animal dead and eventually saves his fiancee from the peril.

A sequel, The Mystery of Marie Roget (film), was made by Universal in 1942.

==Cast==
 
*Bela Lugosi as Dr Mirakle
*Sidney Fox as Mademoiselle Camille LEspanaye Leon Ames as Pierre Dupin
*Bert Roach as Paul
*Betty Ross Clarke as Mademoiselle LEspanaye
*Brandon Hurst as Prefect of Police
*DArcy Corrigan as Morgue Keeper
*Noble Johnson as Janos The Black One
*Arlene Francis as Streetwalker

==See also==
* List of American films of 1932

==References==

 
 
==External links==
 
*   at Rotten Tomatoes
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 