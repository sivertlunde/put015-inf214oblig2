Preah Vesandor
 
{{Infobox film| name           = Preah Vesandor
| image          = 
| director       = Fai Som Ang
| producer       = Som Yvette
| writer         =
| starring       = Tep Rundaro Ampor Tevi Pisith Pilika Oum Sovanny Prum Sovuthy
| music          = 
| cinematography = 
| editing        = 
| studio         = Sománg Rathanak
| distributor    = 
| released       = 1993
| runtime        = 
| country        = Cambodia
| language       = Khmer
| budget         = 
| gross          = 
| followed_by    = 
}}
 1993 musical musical movie starring Tep Rundaro, Pisith Pilika, Oum Sovanny, Ampor Tevi and other stars of the time.

==Soundtrack==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Notes
|-
| Kout Jet Nung Met Tida
| Ros Serey Sothear
| Picturized on Neary Roth Kunthea
|-
| Tomnuonh Neang Metrei
| Him Sivorn 
| Picturized on Pisith Pilika
|-
| Preah Vesandor
| Bun Janton 
| Picturized on Tep Rundaro
|-
| Pka Reek Bang Sluk
| Sinn Sisamouth, Ros Serey Sothear, Pan Ron
| Picturized on Tep Rundaro
|-
| Srey Na Doch Knhom
| Him Sivorn
| Picturized on Oum Sovanny
|-
| My Love For You
| Prum Sovuthy and Un Sophal
| Starring Yuos Bovannak and Ampor Tevi
|-
| Haum Krolean
| Prum Sovuthy and Him Sivorn
| Starring Neay Kuy and Jum Reksmey
|-
| Veal Srey Sronos
| Sinn Sisamouth 
| Starring Hong Poly and Hok Leakenna
|-
| Veal Srey Sronos
| Pan Ron 
| Starring Hong Poly and Hok Leakenna
|-
| Baek Kaóm
| Ros Serey Sothear 
| Picturized on Oum Sovanny
|-
|}

==Cast==
*Tep Rundaro
*Prum Sovuthy
*Oum Sovanny
*Pisith Pilika
*Yuthara Chany
*Hong Poly Maktura
*Ampor Tevi
*Neay Kuy
*Hok Leakenna
*Keo Koliyan
*Neary Roth Kunthea
*Jum Reksmey
*Yuos Bovannak

==References==

 
 
 


 