Postmen in the Mountains
{{Infobox film
| name           = Postmen in the Mountains
| image          = Postmen in the Mountains.jpg
| film name = {{Film name| jianti         = 那山那人那狗
 | fanti          = 那山那人那狗
 | pinyin         = Nàshān nàrén nàgǒu}}
| director       = Huo Jianqi
| producer       = Kang Jianmin
| writer         = Si Wu
| based on       =   Zhao Lei
| music          = Wang Xiaofeng Liu Ye
| distributor    = 
| country        = China
| released       =  
| runtime        = 93 minutes
| language       = Mandarin
}} 1999 Chinese film directed by Huo Jianqi. It is based on the short story of the same name by Peng Jianming (彭见明).
 Liu Ye), but accompanies him on the first tour. Together, they deliver mail on a 230 Li (length)|li (about 115&nbsp;km) long walking route, into the rural heart of China and in the process the son learns from the mails recipients more about the father he hardly knew.

It was filmed on location in Suining County and Dao County, in southwestern and southern Hunan. A portion of the film takes place in a village of the Dong people, including an evening festival featuring a lusheng dance.

==Plot== Liu Ye) begins his first journey as a postman at the mountainous rural areas of the aforesaid regions.  His father (Ten Rujun), a veteran postman forced to retire due to a bad knee, decides to accompany him together with the familys faithful dog, Buddy.
 pop songs played on the sons transistor radio (including Michael Learns to Rocks "Thats Why You Go Away", which is an anachronism given that the film is set in the early 1980s).

==Reception==
Postmen was well received both abroad and at home in China where it won both Best Film and Best Actor (for Ten Rujun) at the Golden Rooster Awards in 1999.

===Awards and nominations=== 1999
**Best Actor – Ten Rujun
**Best Film 2002
**Best Foreign Film (nominated) 2002
**Best Foreign Language Film 2002
**Peoples Choice Award 	
**Grand Prix des Amériques (nominated)

==External links==
* 
* 
* 

 
 
{{succession box Golden Rooster for Best Picture
| years=1999
| before=Live in Peace The Road Home tied with Fatal Decision & Roaring Across the Horizon}}
 
 

 
 
 
 
 
 
 
 
 