Early Bloomer
{{Infobox Film
| name           = Early Bloomer
| image          = Early Bloomer poster.jpg
| caption        = Poster for Early Bloomer
| director       = Kevin Johnson
| producer       = Sande Scoredos
| writer         = 
| starring       = 
| music          = Mark Mancina
| editing        = Guy T. Wiedmann
| studio         = Sony Pictures Imageworks
| distributor    = Columbia Pictures
| released       =  
| runtime        = 3 min 42 sec
| country        = United States English
| budget         =
| preceded_by    = 
}}
 2003 computer-animated short by Sony Pictures Imageworks.  It was written by Guy T. Wiedmann and directed by Kevin Johnson. It was studios second short film after The ChubbChubbs!. 

==Plot==
The 3-minute film is following a tadpole who grows legs before the other tadpoles and is teased for it until the others unexpectedly grow legs too.

==Release==
Early Bloomer was theatrically released on May 9, 2003 along with Daddy Day Care. On December 2, 2003, the short was also released as special feature on the Daddy Day Care DVD.  The short is also available on iTunes. 

==References==
 

==External links==
*  
*  
* 

 

 
 
 


 