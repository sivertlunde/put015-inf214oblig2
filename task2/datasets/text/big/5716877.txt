The Chamber (film)
{{Infobox film
| name           = The Chamber
| image          = the chamber poster.jpg
| caption        = The Chamber movie poster
| director       = James Foley John Davis Brian Grazer Ron Howard
| writer         =   Screenplay: William Goldman Phil Alden Robinson (as "Chris Reese")
| starring       = Chris ODonnell Gene Hackman Faye Dunaway Lela Rochon Robert Prosky Raymond J. Barry Shannon Griffith (uncredited) David Marshall Grant
| music          = Carter Burwell Ian Baker Mark Warner
| studio         = Imagine Entertainment Davis Entertainment Universal Pictures
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         = $50 million
| gross          = $22,540,359
}}
 thriller film novel of the same name. The film was directed by James Foley and stars Gene Hackman and Chris ODonnell.

==Plot==
 , the setting of the film]] death sentence for the murder of two Jewish children 30 years ago. Only 28 days before Cayhall is to be executed, Adam meets his grandfather for the first time in the Mississippi State Penitentiary which has held him since his conviction in 1980. The meeting is predictably tense when the educated, young Mr. "Hall" confronts his venom-spewing elder, Mr. "Cayhall" about the murders. The next day, headlines run proclaiming Adam the grandson who has come to the state to save his grandfather, the infamous Ku Klux Klan bomber.

While the old mans life lies in the balance, Adams motivation in fighting this battle becomes clear as the story unfolds. He fights not only for his grandfather but also perhaps for himself. He has come to heal the wounds of his own fathers suicide, to mitigate the secret shame he has always felt for having this man as a grandfather and to bring closure (psychology)|closure, one way or another, to the suffering the old man seems to have brought to everyone he has ever known. At the end, there is some redemption for the condemned man when he expresses remorse and realizes his wasted life of hate. It is revealed that there were others involved in the bombing, but he is the only one who is actually executed.

==Cast==
* Chris ODonnell as Adam Hall 
* Gene Hackman as Sam Cayhall 
* Faye Dunaway as Lee Cayhall Bowen 
* Robert Prosky as E. Garner Goodman 
* Raymond J. Barry as Rollie Wedge/Donnie Cayhall 
* Bo Jackson as Sgt. Clyde Packer 
* Lela Rochon as Nora Stark 
* David Marshall Grant as Gov. David McAllister 
* Nicholas Pryor as Judge Flynn F. Slattery 
* Harve Presnell as Atty. Gen. Roxburgh
* Millie Perkins as Ruth Kramer

==Production history==
Ron Howard was originally set to direct the film, but left the project to direct Ransom (1996 film)|Ransom (1996). He stayed on as a producer on the film.  Brad Pitt was committed to playing Adam Hall, but left the project when Howard left to direct Ransom. 

William Goldman, who wrote the early drafts, described the project as a "total wipeout disaster... a terrible experience" and never saw the finished movie. {{cite book
  | last = Goldman
  | first = William
  | authorlink = William Goldman
  | title = Which Lie Did I Tell?: More Adventures in the Screen Trade
  | publisher = Vintage
  | year = 2001
  | origyear = 2000
  | isbn = 0-375-40349-3
  | page = 125
}} 

==Filming locations==
Scenes were filmed in the actual gas chamber at Parchman Penitentiary.  Other locations were filmed in Chicago, Jackson, Mississippi, Indianola, Mississippi, Greenwood, Mississippi, Parchman, Mississippi, and Los Angeles.

==Reception==
Critical reaction to The Chamber has been negative, with the film earning a rating of 12% on Rotten Tomatoes. 

  also gave the film two stars out of four, saying: "Plot-wise, The Chamber is full of seeming irrelevancies. The movie should have been streamlined better; theres no need to try to include virtually every character from the book.   The Chamber   is mechanical and artificial, and tells you what to think." 
 Razzie Award Worst Supporting Actress, but did not win the award.

The score by Carter Burwell was well received. 

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 