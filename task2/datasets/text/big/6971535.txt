Laura (1979 film)
{{Infobox film
| name = Laura: Shadows of a Summer
| image = Laura1979.jpg
| image_size =
| caption = DVD cover David Hamilton
| producer = Serge Laski   Malcolm J. Thomson David Hamilton  Joe Morhaim   André Szots
| narrator =
| starring = Maud Adams Dawn Dunlap
| music = Patrick Juvet
| cinematography = Bernard Daillencourt
| editing = Joële Van Effenterre
| distributor = 21st Century Film Corporation
| released =  
| runtime = 95 minutes
| country = France
| language = French
| budget =
| gross =
| preceded_by =
| followed_by =
}} David Hamilton. Hamilton Himself made a cameo appearance in the film. It stars a then sixteen-year-old Dawn Dunlap as the title character.

==Plot==
James Mitchell plays Paul Wyler, a successful sculptor best known for sculpting nude young girls. When he encounters an old flame, Sarah (Maud Adams), he is so smitten by her beautiful daughter Laura that he asks Sarah if she will pose for him. Sarah, married but still jealous of the fact that her former lover is more attracted to her daughter, tells Paul that Laura is not interested, even though she is actually quite enthusiastic about doing it. As an alternative to live posing, Sarah takes photos of Laura posing nude and gives them to Paul so that he can sculpt Laura. After a fire at an art exhibit, though, Paul goes blind and cannot complete the sculpture. In the end, Laura, when going to say goodbye to Paul, allows him to finish his work by feeling her body and sculpting by feeling. This leads to a sexual encounter between the two, and the next morning Lauras mother comes to take her away.

==Production== David Hamiltons photography and his other films.

==Cast==
* Maud Adams as Sarah Moore
* Dawn Dunlap as Laura Moore
* James Mitchell as Paul Thomas Wyler
* Pierre Londiche as Richard Moore
* Thierry Redler as Costa
* Maureen Kerwin as Marline Royer
* Gunilla Astrom as Diane
* Katia Kofet as Claudie
* Louise Vincent as Madame Flory
* Luciano as Timothez Sega
* Bill Millie as the choreographer

==Companion book==
In 1979, Hamilton released a portfolio tie-in with the film, entitled Shadows of a Summer.

==External links==
*  
*  

==References==
 

 
 
 
 
 
 
 


 
 