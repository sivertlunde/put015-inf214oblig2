Gold (2013 film)
 
{{Infobox film
| name           = Gold
| image          = 
| caption        = 
| director       = Thomas Arslan
| producer       = Florian Koerner von Gustorf Michael Weber
| writer         = Thomas Arslan
| starring       = Nina Hoss Uwe Bohm
| music          =  Dylan Carlson
| cinematography = Patrick Orth
| editing        = Bettina Böhler
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = Germany
| language       = German English
| budget         = 
}}
 Western film directed by Thomas Arslan. The film premiered in competition at the 63rd Berlin International Film Festival.      

==Plot== German and Austrian-Hungarian origin explore a sparsely populated part of Canada in search for gold.

==Cast==
* Nina Hoss as Emily Meyer
* Uwe Bohm as Gustav Müller
* Kindall Charters as First Indian
* Rosa Enskat as Maria Dietz
* Peter Kurth as Wilhelm Laser
* Marko Mandic as Carl Böhmer
* Wolfgang Packhäuser as Otto Dietz
* Lars Rudolph as Joseph Rossmann

==Reception== homage to "late-era Western". She also appreciated the cinematography for portraying the landscapes as  "ruggedly majestic".    
 plot offered "little in the way of entertainment or palpable suspense". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 
 