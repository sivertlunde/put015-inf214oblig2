The Root of All Evil (1947 film)
 
{{Infobox film
| name           = The Root of All Evil
| image          = Rootofallevil1947.jpg
| caption        = VHS cover Brock Williams
| producer       = Harold Huth
| writer         = Brock Williams J. S. Fletcher (novel)
| starring       = Phyllis Calvert Michael Rennie
| cinematography = Stephen Dade
| music          = Bretton Byrd
| studio         = Gainsborough Pictures
| released       =  
| runtime        = 110 minutes
| country        = United Kingdom
| language       = English
}}
 Brock Williams for Gainsborough Pictures and starring Phyllis Calvert and Michael Rennie.  The film was the first directorial assignment for Williams, who was better known as a screenwriter, and also produced the screenplay based on a novel by J. S. Fletcher.

==Plot== John McCallum), but takes him for granted and feels that he lacks the spark or ambition to match her determination to make something of herself.  Instead she pursues Albert Grice (Hubert Gregg), son of a wealthy grocery store owner, and believes they have an understanding.  She is horrified when Albert goes on holiday, and returns newly married to another woman.

Seeing a possible payday as compensation for her disappointment, Jeckie sues Albert for breach of promise and emotional distress, and after she plays up her status as jilted victim in court she is awarded a considerable sum in damages.  Seeing the chance for revenge, she uses her windfall to set up her own grocery store, directly opposite that of the Grice emporium.  By undercutting on prices and offering customer perks, she soon succeeds in poaching nearly all of their business and starts to accumulate a tidy sum in profits.  Her ambition however stretches beyond a grocery store and its relatively modest financial potential.  She is intrigued to meet a handsome stranger Charles Mortimer (Rennie), who tells her that there are large deposits of oil on the edge of town and he is looking for a financial backer to help him exploit them.

Jeckie agrees to throw her lot in with Charles to get their hands on the land under which the oil can be drilled.  It belongs to an elderly man Scholes (Moore Marriott), who is of the opinion that it is a stony, barren and useless plot, and is happy to sell for what seems on the surface a generous price.  The oil operation quickly proves to have huge financial potential, and soon becomes a sizeable industry raking in vast profits.  Now a wealthy woman, Jeckie buys the grandest house in the area and lives a life of luxury.  She has fallen in love with Charles, but when she learns that he has misled her and is in fact married, she orders him to leave and says he will get no more share of the profits.

Meanwhile Scholes resentment at being swindled had been simmering in the background, and finally explodes when he decides to set fire to the refinery to exact his revenge.  The whole operation is destroyed in a spectacular blaze.  Faced with losing everything, Jeckie finally starts to analyse her own ruthlessness and avarice.  She realises that she has made many enemies and has few real friends.  But the faithful Joe has never criticised or judged her, and she finally sees that he was the man for her all along.

==Cast==
* Phyllis Calvert as Jeckie Farnish
* Michael Rennie as Charles Mortimer John McCallum as Joe Bartle
* Brefni ORorke as Farnish
* Moore Marriott as Scholes
* Hazel Court as Rushie Farnish
* Hubert Gregg as Albert Grice Arthur Young as George Grice
* Reginald Purdell as Perkins
* Stewart Rome as Sir George
* George Carney as Bowser George Merritt as Landlord
* Ellis Irving as Auctioneer

==References==
 
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 