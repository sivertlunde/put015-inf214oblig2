Teenage Cave Man
 
{{Infobox film
| name           = Teenage Cave Man
| image          = Teenage caveman.JPG
| image_size     = 200px
| caption        = film poster by Reynold Brown
| director       = Roger Corman
| producer       = Roger Corman
| writer         = R. Wright Campbell
| narrator       =
| starring       = Robert Vaughn Darah Marshall
| music          = Albert Glasser
| cinematography = Floyd Crosby
| editing        = Irene Morra AIP (1958, original) Lions Gate (2006 DVD)
| released       =  
| runtime        = 65 min.
| country        = United States English
| budget         = $70,000 
}}
Teenage Cave Man is a 1958    Lead actor Robert Vaughn has stated in an interview that he considered it to be the worst film ever made.  The film was later featured on the television series Mystery Science Theater 3000.

== Plot ==
 stones the giant to death. In a surprising and interesting denouement via voice-over by the giant after his death, the truth is revealed: the hideous figure is actually the last survivor of an ancient nuclear holocaust. Surviving due to his radiation suit, he wandered across the land as humanity slowly rebuilt itself, his terrible appearance causing everyone to fear and shun him. The final message of the movie is this: would humanity repeat its mistake?

==Production==
The film was originally known as Land of Prehistoric Women. MOVIELAND EVENTS: Machine Gun Kelly New Crime Thriller
Los Angeles Times (1923-Current File)   19 Dec 1957: B15.  

==DVD==
Teenage Cave Man was released to DVD by Lions Gate on April 18th 2006, as part of a two-disc set, with Viking Women and the Sea Serpent as the first disc.

==See also==

* Survival film, about the film genre, with a list of related films

== Notes ==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 