Knight Without Armour
{{Infobox Film   
| name           = Knight Without Armour 
| image          = Knight Without Armour.jpg
| caption        = U.S. film poster as reproduced on bookcover
| director       = Jacques Feyder 
| producer       = Alexander Korda  James Hilton (novel)   Arthur Wimperis (additional dialogue)
| starring       = Marlene Dietrich   Robert Donat
| music          = Miklós Rózsa  Pyotr Ilyich Tchaikovsky 
| cinematography = Harry Stradling Sr.
| editing        = Francis D. Lyon 
| studio         = London Film Productions
| distributor    = United Artists
| released       = 1 June 1937
| runtime        = 107 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} historical drama novel of James Hilton. The music score was by Miklós Rózsa, his first for a motion picture, utilising additional music by Pyotr Ilyich Tchaikovsky|Tchaikovsky.

The film stars Marlene Dietrich as Alexandra Adraxine and Robert Donat as A.J. Fothergill. Filmed on a budget of near $1 million, Knight Without Armour became an expensive box office failure, making roughly $750,000 worldwide.

==Plot== Herbert Lomas), the father of Alexandra (Marlene Dietrich). When the attempt fails, the would-be assassin is shot, but manages to reach Peters apartment, where he dies. For his inadvertent involvement, Peter is sent to Siberia.

  White Army. Their relief is short-lived; the Red Army defeats the White the next day, and Alexandra is taken captive once more. Peter steals a commission as a commissar of prisons from a drunk and uses the document to free her. The two, now deeply in love, flee into the forest. Later, they catch a train.

At a railway station, the countess is identified by one Communist official, but Commissar Poushkoff (John Clements), an overly sensitive young man, is entranced by Alexandras beauty. Insisting that her identity be verified, he  arranges to take her and Fothergill to Samara, Russia|Samara. Along the way, they become good friends, but Poushkoff grows overwrought after drinking too much brandy with dinner aboard the train. He then allows the couple to escape at a stop, committing suicide to provide a diversion.

The lovers board a barge travelling down the Volga River. Alexandra becomes seriously ill. When Peter goes for a doctor, he is arrested by the Whites for not having papers. Meanwhile, a Red Cross doctor finds Alexandra and takes her for treatment. About to be executed, Peter makes a break for it and catches the Red Cross train transporting Alexandra out of Russia.

==Cast==
*Marlene Dietrich as Alexandra Adraxine, née Vladinoff
*Robert Donat as A.J. Fothergill / "Peter Ouranoff"
*Irene Vanbrugh as Duchess Herbert Lomas as General Gregor Vladinoff
*Austin Trevor as Colonel Adraxine, Alexandras husband
*Basil Gill as Axelstein
*David Tree as Maronin
*John Clements as Poushkoff
*Frederick Culley as Stanfield
*Laurence Hanray as Colonel Forester
*Dorice Fordred as the Maid
*Franklin Kelsey as Tomsky
*Laurence Baskcomb as Commissar
*Hay Petrie as Station Master
*Miles Malleson as Drunken Red Commissar

According to Robert Osborne of Turner Classic Movies, Donat suffered a severe, week-long bout of his chronic asthma during production, causing Alexander Korda to consider replacing him. Dietrich persuaded him to wait until Donat had recovered.

==Further reading==
* 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 