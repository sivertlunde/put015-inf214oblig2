Harvard Beats Yale 29-29
{{Infobox film
| name           = Harvard Beats Yale 29-29
| image          = 
| alt            = 
| caption        = 
| director       = Kevin Rafferty
| producer       = Kevin Rafferty
| writer         =  Brian Dowling
| music          = 
| cinematography = Kevin Rafferty
| editing        = Kevin Rafferty
| studio         = Kevin Rafferty Productions Kino International 
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 1968 meeting their storied rivalry. The game has been called "the most famous football game in Ivy League history".                

==Story== Harvard and Yale were Brian Dowling, Yale was heavily favored to win and they quickly led the game 22–0.    With two minutes remaining on the clock they still led 29–13.   As the last seconds ticked down, Harvard, coached by John Yovicsin, tied the game, scoring  16 points in the final 42 seconds.    The Harvard Crimson declared victory with a famous headline, "Harvard Beats Yale 29-29,"   providing the title for Rafferty’s documentary.     

==Production== WHDH telecast, Don Gillis doing the sports commentator|play-by-play.  The film was set to celebrate the 40th anniversary of the 1968 game between Yale and Harvard.   

The documentary includes game footage with contemporary interviews with the men who played that day, as well as contextual commentary about the Vietnam War, the sexual revolution, Garry Trudeaus Yale cartoons, and various players relationships with George W. Bush (Yale), Al Gore (Harvard), and Meryl Streep (Vassar College|Vassar).

==Cast== Guard
*Brian Brian Dowling as himself – Yale Quarterback Don Gillis as himself – Sportscaster George Bass as himself – Yale Tackle 
*Frank Champi as himself – Harvard Quarterback
*Gus Crim as himself - Harvard Fullback End 
*Rick Frisbie as himself – Harvard Cornerback  Halfback (Captain Team Captain) 
*Kyle Gee as himself – Yale Tackle  Safety  Halfback (archive footage) 
*Ray Hornblower as himself – Harvard Halfback
*Ron Kell as himself – Yale Defensive Back  Guard  Fullback 
*Ted Livingston as himself – Yale Tackle Fred Morris Center 
*Ted Skowronski as himself – Harvard Center 
*Bruce Weinstein as himself – Yale End
*Mike Bouscaren as himself – Yale Linebacker
*Robert Dowd as himself- Harvard Tackle

==Reception== Brian Dowling becoming the character "B.D." in the Doonesbury comic strip (Garry Trudeau attended Yale), and player Bob Levin remembering dating a Vassar undergraduate named Meryl Streep." 

Underscoring that the film had appeal to more than just sports fans, Bruce Elder of All Movie Guide began his review with "it is only fair of this writer to point out that he cares not one whit about, and has not a scintilla of interest in football. Having said that, we can also say, without equivocation, that Kevin Raffertys Harvard Beats Yale 29-29 is a dazzling, engrossing, must-see piece of film all about...football. Except that its also about a lot more." 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 