Gyarakal
{{Infobox film
| name           = Gyarakal 
| image          = Gyarakal movie.jpeg
| image_size     = 8 kbps
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Haranath Chakraborty
| producer       = Pijush Saha 
Rajib Bhadra
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Prosenjit Chatterjee Rachana Banerjee Jisshu Sengupta Ranjit Mallik Kanchan Mullick
| music          = Babul Bose
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2004
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film directed by Haranath Chakraborty and produced by  Pijush Saha,Rajib Bhadra. The film features actors Prosenjit Chatterjee and Rachana Banerjee in the lead roles. Music of the film has been composed by Babul Bose    

== Plot ==
This is a comedy. A most popular business man named Balohari Majumdar(Ranjit Mallik) wants his daughter to became a great singer. One day Baloharis daughter Disha(Soumili Biswas) and her song teacher sang a song. Disha and her music teacher Tablu(Jisshu Sengupta) loved each other. Balohari does not approve, Tablu runs away and loses his job. Then Balolari became angry and he told everyone he needed a married music teacher with his wife. In the meantime a local singer named Pradip(Prosenjit Chatterjee) needed a job. One day he went to a club (three-not-three) function. At the function he met Sikha(Rachana Banerjee). Sikha needed 30,000 rupees immediately. So they are became a fake husband-wife and came to Baloharis home. Baloharis family liked them. Then they stayed at Baloharis home. One day Tablu watched Pradip(Prosenjit Chatterjee) and Disha leave a market. He was very angry. In the mean time Sikha fell in love with Pradip(Prosenjit Chatterjee). Pradip(Prosenjit Chatterjee) came to Baloharis factory and caught Baloharis cashier who was stealing things from Boloharis factory. Then Disha sang a song in a competition and won. Sikha tried to stop Pradip to leave Baloharis home. Sikha informed her father to send her back. Disha was missing. They also thought Pradip had kidnapped his daughter. Balohari came to know that Pradip-Sikha were not a married couple. He became very angry. Pradip wanted Tablu and Disha to get married. Finally, Balohari accepted Tablu, and Disha and Tablu were happily married.

== Cast ==
* Prosenjit Chatterjee as Pradip
* Rachana Banerjee as Sikha
* Jisshu Sengupta as Tablu
* Soumili Biswas as Disha
* Ranjit Mallik as Balohari Majumdar
* Anamika Saha
* Kanchan Mullick
* Nimu Bhowmik Rajesh Sharma
* Nirmal Kumar

== References ==
 

==External links==
*  
 
 

 
 
 
 


 