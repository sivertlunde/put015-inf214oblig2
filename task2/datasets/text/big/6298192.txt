Song of Russia
{{Infobox film
| name           = Song of Russia
| image          = Poster of the movie Song of Russia.jpg
| image_size     = 225px
| caption        = Theatrical Film Poster
| director       = Gregory Ratoff László Benedek (uncredited)
| producer       = Joe Pasternak Pandro S. Berman Richard Collins (screenplay)
| narrator       = Robert Taylor Susan Peters Robert Benchley
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $1,828,000  .  Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 350 
| gross          = $3,729,000 
}}
 Robert Taylor, Susan Peters and Robert Benchley. 

==Plot==
American conductor John Meredith (Robert Taylor) and his manager, Hank Higgins (Robert Benchley), go to   concerning this film, including synopsis 

==Cast== Robert Taylor as John Meredith
*Susan Peters as Nadya Stepanova
*John Hodiak as Boris Bulganov
*Robert Benchley as Hank Higgins
*Felix Bressart as Petrov
*Michael Chekhov as Ivan Stepanov
*Darryl Hickman as Peter Bulganov
*Jacqueline White as Anna Bulganov

==Perception as pro-Soviet propaganda==
The positive portrayal of the Soviet Union in the film is clearly linked to the wartime alliance of the Soviet Union and the U.S.  
 The North Star. This assertion was supported by the Russian-born pro-capitalist and anti-Communist writer Ayn Rand, who was specifically asked by a HUAC investigator to see the film and provide an expert opinion on it.  Ayn Rand, in her 1947 testimony before the HUAC, cited Song of Russia as an example of Communist propaganda in the Hollywood motion picture-industry, depicting an idealized Soviet Union with freedom and comfort that never existed in the real Soviet Union.  

Robert Taylor himself protested, after the fact, that he had had to make the film under duress, as he was under contract to MGM. This is the rationale he used to explain why he was a friendly witness during the HUAC hearings in the 1950s.

==Reception==
Despite the criticism it received in later years, historians claiming it is nowadays more remembered for its contents rather than its quality, Song of Russia was initially received positively. The New York Times called some scenes "a fine bit of cinematic art".      Furthermore, the reviewer praised the cast, writing:
:"Taylor makes a very good impression as a young American caught in Russia by love and war. And Susan Peters is extraordinarily winning as a mentally solemn but emotionally bonny Russian girl. Robert Benchley throws some straws of cryptic humor into the wind as the Americans manager, and Michael Chekhov, Vladimir Sokoloff and Michael Dalmatov are superb as genial Russian characters." 

Big Spring Daily Herald called Taylor and Peters "the most dynamic new romantic team since Clark Gable was paired with Lana Turner". 

===Box office===
The movie was also popular, earned $1,845,000 in the US and Canada and $1,884,000 elsewhere resulting in a profit of $782,000. 

==References==
 

==External links==
*  
*  

 
 

 
 
 
   
 
 
 
 
 