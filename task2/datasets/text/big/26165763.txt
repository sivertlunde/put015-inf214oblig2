Two Left Feet (film)
{{Infobox film
| name           = Two Left Feet
| image          = "Two_Left_Feet"_(1963).jpg
| image_size     = 
| caption        = 
| director       = Roy Ward Baker
| producer       = Roy Ward Baker Leslie Gilliat John Hopkins
| based on       = the novel In My Solitude by David Stuart Leslie
| narrator       = 
| starring       = Michael Crawford Philip Green
| cinematography = Wilkie Cooper Harry Gillam
| editing        = Michael Hark John Pomeroy
| studio         = Roy Ward Baker Productions
| distributor    = British Lion Film Corporation  (UK) 
| released       = March 1963
| runtime        = 93 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Two Left Feet is a 1963 Comedy-drama film (British Lion) directed by Roy Ward Baker, starring Nyree Dawn Porter, Michael Crawford, David Hemmings and Julia Foster.

==Plot==

Based on David Stuart Leslies novel, Two Left Feet is a story about Alan Crabbe (Michael Crawford) a callow youth desperate for a date with any girl who can offer him the experience he lacks. Every time Alan tries a manful stride into the jungle of sex, his two left feet turn the attempt into a trip-and-stumble. Then he meets Eileen (Nyree Dawn Porter), the new waitress at the corner cafe, and some sparks begin to fly.

==Behind the scenes==
 X Certificate 15 Rating when released on video in 1994).
 Double bill, after a delay of two years being left on the shelf. 

The film was based on the 1960 novel In My Solitude by David Stuart Leslie. 

Tommy Bruce sang "Two Left Feet" to the opening credits of the film.  It also featured the song "Where Were You" by Susan Maughan.

==References==
 

==External links==
* 

 

DVD Release 1.9.14 Network

 
 
 
 
 
 


 