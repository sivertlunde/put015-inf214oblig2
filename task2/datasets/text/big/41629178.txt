Gas House Kids
{{Infobox film
| name =  Gas House Kids 
| image =
| image_size =
| caption =
| director = Sam Newfield
| producer = Sigmund Neufeld
| writer = Elsie Bricker   George Bricker   Raymond L. Schrock
| narrator = Robert Lowery   Billy Halop   Teala Loring   Carl Switzer
| music = Leo Erdody 
| cinematography = Jack Greenhalgh
| editing = Holbrook N. Todd 
| studio = Producers Releasing Corporation
| distributor = Producers Releasing Corporation 
| released =October 9, 1946
| runtime = 71 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Robert Lowery, Billy Halop and Teala Loring. It was followed by two sequels Gas House Kids Go West and Gas House Kids in Hollywood, both released in 1947. A group of unruly New York City children battle a criminal gang. Along with several other series made at the time, it was inspired by the Dead End Kids. 

==Cast== Robert Lowery as Eddie OBrien  
* Billy Halop as Tony Albertini  
* Teala Loring as Colleen Flanagan 
* Carl Switzer as Sammy Levine   David Reed as Pat Flanagan  
* Rex Downing as Mickey Papopalous
* Rocco Lanzo as Gus Schmidt 
* Hope Landin as Mrs. OBrien 
* Ralph Dunn as Detective OHara 
* Paul Bryar as Shadow Sarecki   Charles C. Wilson as Inspector Shannon

==References==
 

==Bibliography==
* The American Film Institute Catalog of Motion Pictures Produced in the United States: Feature Films, 1941 - 1950: Feature Films. University of California Press, 1999.

==External links==
* 

 
 
 
 
 
 
 
 


 