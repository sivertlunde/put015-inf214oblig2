Sirayil Pootha Chinna Malar
{{Infobox film
| name           = Sirayil Pootha Chinna Malar
| image          = 
| image_size     =
| caption        =
| director       = Amirtham
| producer       = M. Gopi
| writer         = Arulmozhi (dialogues)
| screenplay     = Amirtham
| story          = Amirtham Shanthipriya Rajesh Rajesh
| music          = Ilaiyaraaja
| cinematography = S. Maruthi Rao Amirtham
| editing        = P. Venkateswara Rao
| studio         = Sri Thirumala Art Productions
| distributor    = Sri Thirumala Art Productions
| released       =  
| runtime        = 130 minutes
| country        = India Tamil
}}
 1990 Cinema Indian Tamil Tamil film, Shanthipriya and Rajesh in lead roles. The film had musical score by Ilaiyaraaja.  

==Cast==
 
*Vijayakanth
*Bhanupriya Shanthipriya
*Rajesh Rajesh
*S. S. Chandran
*Jayabharathi Tara
*Kokila Thyagu
*Charle
*Sivaraman
*S. A. Kannan
*R. Shankaran
*Azhagu
*Manoj
*Abhinaya
*Vani
*Latha
*Ushapriya
*Premi
 

==Soundtrack==
The music was composed by Ilaiyaraaja.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Janaki || Vaali || 04.40 
|-  Chithra || Vaali || 05.07 
|-  Chithra || Pirai Sudan || 05.03 
|- 
| 4 || Thaana Pazhutha || Kovai Soundararajan || Gangai Amaran || 04.27 
|-  Janaki || Pulamaipithan || 04.56 
|-  Chithra || Gangai Amaran || 04.12 
|}

==References==
 

==External links==

 
 
 
 
 


 