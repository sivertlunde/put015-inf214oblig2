It (1927 film)
{{Infobox film
| name           = "It"
| image          = It1927clarabow.jpg
| image_size     =
| director       = Clarence G. Badger
| producer       = Clarence G. Badger
| writer         = Elinor Glyn (story and adaptation) George Marion, Jr. (titles)
| screenplay     = Hope Loring Louis D. Lighton
| based on       =   William Austin
| cinematography = H. Kinley Martin
| editing        = E. Lloyd Sheldon
| distributor    = Paramount Pictures
| released       =  
| runtime        = 72 min.
| country        = United States Silent English English intertitles
}} silent romantic comedy film which tells the story of a shop girl who sets her sights on the handsome and wealthy boss of the department store where she works. It is based on a novella written by Elinor Glyn and originally serialized in Cosmopolitan (magazine)|Cosmopolitan magazine.

Because of this film, actress Clara Bow became a major star of the highest magnitude, and a result, became known as the "It girl". 

The film had its world premiere in Los Angeles on January 14, 1927, followed by a New York showing on February 5, 1927. "It" was released to the general public on February 19, 1927. 
 lost for many years, but a nitrate-copy was found in Prague in the 1960s.  In 2001, "It" was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot== William Austin) notices Betty, though, and she uses him to get closer to Cyrus. 
 roller coasters hot dogs, and has a wonderful time. At the end of the evening, he tries to kiss her. Betty Lou slaps his face, and hurries out of his car and into her flat. She then peeks out her window at him as he is leaving.
 welfare workers. Monty arrives at just the wrong moment, forcing Betty to continue the charade with him. He tells Cyrus. Although he is in love with her, Cyrus offers her an "arrangement" that includes everything but marriage. Betty Lou, shocked and humiliated, refuses, quits her job, and resolves to forget Cyrus. When she learns from Monty about Cyruss misunderstanding, she fumes, and vows to teach her former beau a lesson.

When Cyrus hosts a yachting excursion, Betty Lou makes Monty take her along, masquerading as "Miss Van Cortland". Cyrus at first wants to remove her from the ship, but he cannot long resist Betty Lous it factor; he eventually corners her and proposes marriage, but she gets him back, by telling him that shed "rather marry his office boy", which accomplishes her goal, but breaks her heart. He then learns the truth about the baby, and leaves Monty at the helm of the yacht to go find her. Monty crashes the yacht into a fishing boat, tossing both Betty Lou and Adela into the water. Betty Lou saves Adela, punching her in the face when she panics and threatens to drown them both. At the end of the film, she and Cyrus reconcile on the anchor of the yacht, with the first two letters of the ships name, Itola, between them. Monty and Adela are upset at losing their friends, but it is implied they pursue a relationship with each other as the film ends.

== The concept of "It" ==
The invention of the concept It is generally attributed to Elinor Glyn, but already in 1904, R. Kipling, in the short story "Mrs. Bathurst" introduced It.   
 

In February 1927 Cosmopolitan (magazine)|Cosmopolitan published a two-part serial story in which Glyn defined It.

 

== Production ==

Paramount Pictures paid Glyn $50,000 for the concept, gave her a small part in the film as herself, and gave her a "story and adaptation" credit. 

Hope Loring, Louis D. Lighton and George Marion Jr. (titles) wrote the screenplay and Carl Sandburg noted that Glyns magazine story was "not at all like the film, not like it in any respect."  In the original version of the story, the character with the magnetic personality was a male. Paramount producers suggested the character be female. Also the original female character, Ava Cleveland, was upper class whereas Betty Lou is working class. Nevertheless, Glyn was fully involved in the film adaptation and was very flexible about the transition. Weedon et al. (2014) Elinor Glyn as Novelist, Moviemaker, Glamour Icon and Businesswoman , Ashgate Publishing, Ltd., London and Burlington, VT ISBN 978-1-4724-2182-1 

 
This is one of the first examples of a "concept film", as well as one of the earlier examples of product placement. The concept of "It" is referred to throughout the film, including the scene where Glyn appears as herself and defines "It" for Mr. Waltham. Cosmopolitan magazine is featured prominently in a scene where the character Monty reads Glyns story and introduces it to the audience.

Stage actress Dorothy Tree had her first film role in a small, uncredited part. A young Gary Cooper was cast in a minor role as a newspaper reporter.

==Reception==
"It" was a hit with audiences all over the United States, breaking box office records. Critics praised the film, especially its star, as "a joy to behold".  "It" turned Clara Bow from an up & coming movie actress into the biggest movie star of the 1920s, and in the process, became a film legend as a result of "It". The term "The It girl" has since entered the cultural lexicon.

==See also==
*The House That Shadows Built (1931 promotional film by Paramount)

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 