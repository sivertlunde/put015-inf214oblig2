The Katnips of 1940
{{Infobox Hollywood cartoon|
| cartoon_name = The Katnips of 1940
| series = Krazy Kat
| image = 
| caption = 
| director = Manny Gould Ben Harrison
| story_artist = Harry Love
| animator = Allen Rose Preston Blair
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| studio = 
| distributor = Columbia Pictures 
| release_date = October 12, 1934
| color_process = Black and white
| runtime = 5:43 English
| preceded_by = The Trapeze Artist
| followed_by = Krazys Waterloo
}}

The Katnips of 1940 is a short animated film distributed by Columbia Pictures, and stars Krazy Kat. For some reason, the year 1940 is used when the film is released six years earlier.

==Plot==
Krazy is a dance instructor who is teaching moves to a quartet of clowns wearing leotards and high-heeled pumps. When the clowns are having trouble following his instructions, Krazy puts ropes on their legs to show them how to move. 

Momentarily, a Swedish girl with a blond spiral hair comes out of a dance school only a few yards away before entering Krazys studio. The Swedish girl comes to Krazy, and shows him her dance skills. Krazy, however, isnt interested and therefore turns her down as he is expecting a famous soprano to arrive. The Swedish girl then shows her singing skills by letting out a high tone that jolts Krazy off his feet. But Krazy still turns her down. The Swedish girl then demonstrates her acting skills as she goes into a movable balcony, and recites some romantic poems. To keep her away, Krazy raises and sets the balcony to the peak. The Swedish girl screams in horror upon seeing no way down.

A fancy car arrives just outside the studio minutes later. Exiting the vehicle is the soprano whom Krazy was waiting for. The soprano comes to Krazy, and sings a few notes for demonstration. The Swedish girl, who is still up on the movable balcony, applauds the sopranos test act. The Swedish girl is also leaning forward too much before falling off the platform and onto the soprano. Upon getting smashed by the Swedish girl, the sopranos voice is ruined. The soprano leaves the scene embarrassed. Krazy has no choice but to pick the Swedish girl for the show.

Later that night, the scene shows the outside of the theater with a banner of the event called "The Katnips of 1940." The name of the soprano on the banner is also shown being replaced by that of the Swedish girl. The event is started by a group of singing can can dancers. Next it is the turn of the Swedish girl to take center stage. The Swedish girl, wearing a fedora, is at first too hesitant to step into the limelight but thankfully Krazy literally gives her a push. The Swedish girl sings, dances, and plays castanets on the stage with complete fluency. Krazy, also wearing a fedora, joins her on stage seconds afterward. With a flawless performance, both of them receive applause. After the lights move away from them and back, Krazy and the Swedish girl are seen dressed like Uncle Sam and the Statue of Liberty respectively.

==External links==
*  at the Big Cartoon Database
* 
 

 
 
 
 
 
 
 
 
 
 

 