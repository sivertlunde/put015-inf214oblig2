Shikari (1963 film)
{{Infobox film
| name           = Shikari 
| image          = 
| caption        =
| director       = Mohammed Hussain
| producer       = F C Mehra  - Eagle Films
| writer         = Surinder Mehra and Vrajendra Gaur (screenplay) Ajit Ragini Helen Madan Puri  K N Singh  Tun Tun   Kamal Mehra
| music          = G.S. Kohli 
| lyrics         = Qamar Jalalabadi 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1963
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = }}

Shikari (English: The Hunter) is a hit Hindi movie directed by Mohammed Hussain and was released in 1963. The actors are  Ajit Khan|Ajit, Ragini, Helen (actress)|Helen, Madan Puri, K N Singh, Tun Tun,  Kamal Mehra, and others. The music of the film was the biggest asset of this film, and the songs O, tumko piya, dil diya, mangi hain duaayen, ye rangeen mehfil, agar main poochhoon, had catchy tunes to become popular on the music-chart of the year.

== External links ==
*  

 
 
 


 