Pokiri
 
 
 
{{Infobox film
| name           = Pokiri
| image          = Pokiri movie poster.jpg
| caption        = Film poster
| director       = Puri Jagannadh
| producer       = Puri Jagannadh   Manjula Ghattamaneni
| writer         = Puri Jagannadh
| starring       = Mahesh Babu Ileana DCruz Prakash Raj
| music          = Mani Sharma
| cinematography = Shyam K. Naidu
| editing        = Marthand K. Venkatesh
| distributor    = Vaishno Academy
| released       =  
| runtime        = 164 minutes
| country        = India
| language       = Telugu
| budget         =     
| gross          =   
}}

Pokiri (translation : Rogue) is a 2006 Telugu film|Telugu, gangster-thriller film written and directed by Puri Jagannadh starring Mahesh Babu and Ileana DCruz in the lead roles and Prakash Raj, Sayaji Shinde and Ashish Vidyarthi in supporting roles. The film is a joint production between Puri Jagannadhs Vaishno Academy and Manjula Ghattamanenis Indira Productions.
 Tamil as Hindi as Kannada as Porki (2010) and also dubbed into Hindi as Tapoori Wanted.
 7th IIFA Film Festival held in Dubai.    The film ultimately claimed to be one of the highest grossing Telugu film of its time after its theatrical run.  The film has won four Nandi Awards, two Filmfare Awards South for the year 2006 and one Santosham Film Awards for 2007. The film recorded as Industry Hit at the box-office and was one of the last films in the industry to achieve this verdict.

== Plot ==
The city of Hyderabad is rife with the nefarious activities of land mafia. There are two rival gangs, one under Ali Bhai (Prakash Raj) who resides in Dubai and controls the operation through his siblings Ajju and Mona, and another operated by local goon Narayan. Both threaten builders and land owners into giving them protection money or property, through force, extortion or murder. Sayed (Sayaji Shinde) arrives as the new Commissioner of Hyderabad and starts cracking down on crime.

Pandu (Mahesh Babu), a thug, is abducted by Mallesh, a member of Alis gang and his henchmen. Pandu has taken a contract from Narayan to beat up Mallesh, which he does. Ajju invites Pandu to join Ali Bhais gang. Pandu declines stating that he does not work for any gang, but is ready to do anything given enough money. Pandu falls in love with Shruthi (Ileana DCruz) when he visits his cousins (Ajay (actor)|Ajay) aerobics class, but she mistakes him for a thug. Shruthi, a college student, lives with her widowed mother and younger brother. Bramhi (Brahmanandam) lives above Shruthis house, and frequently tries to convince Shruthi to marry him. Sub inspector Pashupathy (Ashish Vidyarthi), is a corrupt officer in the colony where Pandu and Shruthi live who often works with the land mafia. He is also on the payroll of Ali Bhai. He falls for Shruthi and decides to make her his mistress, even after multiple rejections by Shruthi.

Pandus first assignment with Alis gang is to kill a member of Narayans band. However, police show up at the spot where Pandu and the other gangsters are waiting. Pandu engages the cops long enough for the others to finish the task and flee. He also helps Shruthi escape from Pashupathy. She is impressed by his kindness and friendship soon blossoms between the two, leading to the development of unspoken romantic feelings for each other. When Shruthi tries to expresses her feelings they are attacked by members of Narayans gang, whom Pandu finishes off. Shruthi is shocked to learn that Pandu is a gangster with no qualms about killing people.

Pashupathy arranges for some thugs to pretend to rape Shruthi, so that no decent family will want to take her as their daughter-in-law, with no other option Shruthi and her mother will accept his demands. Pandu learns of this and assaults Pashupathy incognito, warning him that if he is found responsible for this incident then things will turn ugly for him. After much ado and mental anguish, Shruthi accepts Pandu.

Ali Bhai comes to Hyderabad and kills Narayan. He also meets Pandu to discuss the killing of a minister by blowing up a balloon. Pandu disagrees with Ali Bhais method as it would involve killing innocents. In the middle of their argument police raid the club and arrest Ali Bhai. His gang members retaliate by creating a lewd video of the police commissioners daughter and releasing it to media. They also kidnap her, forcing the police to release Ali Bhai. Ali Bhai drugs the commissioners daughter, causing her to reveal that her father placed a mole in Ali Bhais gang. They find that Pandus friends father Suryanarayana was a police inspector and the inspectors son, Krishna, is now a part of Pandus gang. Ali Bhai kills Krishna in front of Suryanarayana. However, it is revealed that the person who died was actually Ajay. Krishna Manohar, son of Suryanarayana has gone undercover to finish off the underworld mafia gangs. Ali Bhai kills Suryanarayana, hoping that Krishna Manohar would come to see his fathers body. When the real Krishna actually turns up, Ali Bhai is shocked to see it is none other than Pandu, whom he had told his whole plan to.

Krishna uses Pasupathy to call Ali Bhai to find out his location, Binny Mills. He goes there with Pasupathy and starts to dispatch Ali Bhais men one by one. In a final confrontation, Ali Bhai is reminded of the commissioners warning about "the one cop who could make you wet your pants". Krishna kills Ali Bhai by slitting his throat. Finally Krishna kills Pasupathy when he tries to backstab him, citing his most frequent catchphrase : "Once I commit to something, even I cant convince myself".

== Cast ==
  was selected as lead heroine marking her first collaboration with Mahesh Babu.]]
* Mahesh Babu as Krishna Manohar IPS / Pandu
* Ileana DCruz as Shruthi
* Prakash Raj as Ali Bhai
* Isaiah as Guru 
* Satya Prakash as Narayana 
* Nassar as Inspector Suryanarayana
* Ashish Vidyarthi as Sub Inspector Pasupathi
* Sayaji Shinde as Police Commissioner Sayed 
* Brahmanandam as Brahmi – Software Engineer, Shruthis neighbour
* Subbaraju as Nayar Ali as Beggar Association leader Ajay as Ajay
* Jyothi Rana as Mona
* Master Bharath as Shruthis brother
* Mumaith Khan in Item number

== Release ==
The film was released on 28 April 2006.   

=== Critical reception ===
Pokiri released to huge critical acclaim.

Idlebrain gave a 3.5/5 rating and mentioned "Mahesh Babus performance in Pokiri itself is worth your ticket and the rest is bonus!".  Sify wrote "Pokiri is made to showcase Mahesh Babus heroism and it works to a large extent. He is fantastic and the superbly choreographed action scenes in a local train by fight master Vijayan and the shoot-out in the ruins are fantastic. The highlight is Mahesh Babu who looks cool, hip and is in complete control. Ileana is there for glamour and she is promising". 

=== Box office ===
The film was released on 397 screens, including 354 in Andhra Pradesh, 20 in Karnataka, 3 in Orissa, 3 in Tamil Nadu, 1 in Mumbai and 16 Overseas.  The film was later released in 30 theatres in Orissa. It debuted in Chennai with a 98.5% opening in a single screen at Jayaprada theatres.  The film has completed 50 days in 300 centres.  The film has completed 100 days in 200 centres, of them 163 were direct centres including 17 centres in Hyderabad alone.  The film has completed 175 days in a record 63 centres.  The film has completed 200 days in 15 centres.    The film has completed 365 days in Bhagiradha theatre, Kurnool.  The film has completed 500 days in Bhagiradha theatre, Kurnool.    The final worldwide gross of the film is  520&nbsp;million and most of it was collected from Andhra Pradesh.   

== Awards and nominations ==
{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- Nandi Awards|Nandi Awards 2006  Nandi Award Best Popular Feature Film Puri Jagannadh, Manjula Ghattamaneni
| 
|- Nandi Award Best Editor Marthand K. Venkatesh
| 
|- Nandi Award Best Fight Master FEFSI Vijayan|Vijayan
| 
|- Nandi Award Best Male Dubbing Artist
|P. Ravi Shankar|P. Ravi Sankar
| 
|- Filmfare Awards 54th Filmfare Awards South 2006    Filmfare Award Best Director – Telugu Puri Jagannadh
| 
|- Filmfare Award Best Actor – Telugu Mahesh Babu
| 
|- Santosham Film Santosham Film Awards 2007  Santosham Best Actor Award Mahesh Babu
| 
|}

== Soundtrack ==
{{Infobox album|  
| Name     = Pokiri
| Type     = Soundtrack
| Artist   = Mani Sharma
| Released =  
| Recorded = 2006 Feature film soundtrack
| Length   =  
| Label    = Aditya Music
| Producer = Mani Sharma
| Last album = Veerabhadra (film)|Veerabhadra (2006)
| This album = Pokiri (2006)
| Next album = Ashok (film)|Ashok (2006)
}}

The film has six tracks composed by Mani Sharma. The music was launched on 30 March 2006 and was released in stores on 6 April 2006. Lyrics for the three songs were penned by Bhaskarabhatla Ravikumar and another two songs were written by Kandikonda and remaining one song was written by Viswa.

{{tracklist
| headline = Tracklist
| extra_column   = Singer(s)
| total_length   = 28:01
| lyrics_credits = yes
| title1  = Deva Devuda
| lyrics1 = Bhaskarabhatla Ravikumar
| extra1  = Naveen
| length1 = 4:32
| title2  = Dole Dole
| lyrics2 = Viswa
| extra2  = Ranjith (singer)|Ranjith, Suchitra
| length2 = 4:43
| title3  = Gala Gala
| lyrics3 = Kandikonda
| extra3  = Nihal
| length3 = 4:35
| title4  = Ippatikinka
| lyrics4 = Bhaskarabhatla Ravikumar
| extra4  = Muralidhar, Suchitra
| length4 = 4:38
| title5  = Jagadame
| lyrics5 = Kandikonda
| extra5  = Kunal Ganjawala
| length5 = 4:31
| title6  = Choododhantunna
| lyrics6 = Bhaskarabhatla Ravikumar
| extra6  = Karthik (singer)|Karthik, Mahalakshmi Iyer
| length6 = 5:02
}}

== Remakes == Vijay and Asin and directed by Prabhu Deva.  Two years later, it was remade into Hindi, also directed by Prabhu Deva, as Wanted (2009 film)|Wanted,  starring Salman Khan and Ayesha Takia and produced by Boney Kapoor though it was influenced more by the Tamil version. Wanted turned out to be a huge success, becoming not only one of the biggest hits of 2009, but also one of the biggest Bollywood hits of all time. All of the remakes have featured Prakash Raj as the villain Ali Bhai (Telugu, Tamil) and Ghani Bhai (Hindi). Pokiri has also been remade in Kannada as Porkhi and starring Darshan (actor)|Darshan, Pranitha, Devaraj, Ashish Vidyarthi while being directed by M. D. Sridhar and produced by Bachche Gowda.  It has also been remade in Bengali as Rajotto starring Shakib Khan.

{| class="wikitable" style="width:50%;"
|- style="background:#ccc; text-align:center;"
| Pokiri (Telugu language|Telugu) (2006) || Pokkiri (Tamil language|Tamil) (2007) || Wanted (2009 film)|Wanted (Hindi language|Hindi) (2009) || Porki (Kannada language|Kannada) (2010)|| Rajotto (Bengali language|Bengali) (2014)
|- Vijay || Shakib Khan
|- Boby
|- Ilish Kobra
|-
| Nassar || Nassar || Vinod Khanna || Avinash|| Pribir Mitro
|- Napoleon || Sanko Panja
|- Rothun
|- Arefin Iqbal
|}

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 