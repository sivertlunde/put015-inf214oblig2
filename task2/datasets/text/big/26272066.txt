Olivier, Olivier
{{Infobox film
| name           = Olivier, Olivier
| image          = Olivier, Olivier film poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Agnieszka Holland
| producer       = 
| writer         = 
| screenplay     = Agnieszka Holland
| story          = 
| based on       =  
| narrator       = 
| starring       = François Cluzet  Brigitte Roüan  Gregoire Colin  Frédéric Quiring  Marina Golovine
| music          = Zbigniew Preisner
| cinematography = Bernard Zitzermann
| editing        = 
| studio         = Oliane Productions   Films A2   Canal+
| distributor    = 
| released       =  3 March 3, 1993
| runtime        = 110 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Olivier, Olivier is a 1992 drama film directed by Agnieszka Holland. It entered the competition at the 49th Venice International Film Festival  and won an award at the 1992 Valladolid International Film Festival. 

The plot involves a nine-year-old boy who disappears. Six years later he reappears in Paris, yet there are doubts about his real identity.

==Cast==
*Grégoire Colin as Olivier
*Frédéric Quiring as Marcel
*Faye Gatteau as Nadine petite
*Emmanuel Morozof as Olivier petit
*Brigitte Roüan as Elisabeth Duval
*François Cluzet as Serge Duval

==References==
 

==External links==
* 
*New York Times synopsis: http://movies.nytimes.com/movie/36183/Olivier-Olivier/overview

 

 
 
 
 
 


 