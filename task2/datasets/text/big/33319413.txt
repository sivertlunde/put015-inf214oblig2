The Walk (2001 film)
 
{{Infobox film
| name = The Walk
| image = 
| caption = 
| producer = Peter Spirer Gayle Ann Kelley Charles X Block
| director = Peter Spirer
| writer =
| editing = Peter Spirer Iain Kennedy Michelle Nunez
| cinematography = Peter Spirer
| co-producer =
| line producer =
| narrator =
| starring =
| graphics =
| colorist =
| music = Benedikt Bryndern
| distributor = Rugged Entertainment
| released = 
| runtime = 55 minutes
| country = United States
| awards = English
}}
The Walk is a City Block Films and Aslan Pictures production in cooperation with the Indigenous peoples of California.  It was an official selection of the Sundance Film Festival  and was distributed by Rugged Entertainment.

==Plot==
The Walk tells the story of three hundred Native people that attempted to walk 768 miles, from the Pala Reservation near San Diego to Sacramento, in an effort to bring unification to the tribal nations of California.
 Native American population in the United States with 278 tribal nations, more than any other state in the country. The history of California Indians is brutal and devastating, but few Americans, including Californians, know anything about these tribes. The Walk tells the moving story of the traditional walk of several California tribal communities and their supporters.  Led by Native spiritual leader, Robert John, and beginning at the Pala reservation in Southern California, the group makes the 768-mile journey to Sacramento, the states political seat. There they hope to bring light to issues threatening Native American sovereignty, preservation of culture and language, and intertribal unity.

== References ==
 
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 