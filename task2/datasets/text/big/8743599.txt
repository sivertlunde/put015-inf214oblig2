Baazi (1995 film)
 
 

{{Infobox film
| name           = Baazi
| image          = Baazi95.jpg
| caption        =
| director       = Ashutosh Gowariker
| producer       = Salim Akhtar
| story          = Ashutosh Gowariker
| starring       = Aamir Khan Mamta Kulkarni
| music          = Anu Malik
                         Lyrics - Majrooh Sultanpuri  Anvar Sagar
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 180 minutes
| country        = India
| language       = Hindi
| budget         =  
| gross          = (Domestic gross) 
}} The Hard Way was copied in this movie.

==Plot==
The story opens with a group of people travelling in a bus. The bus is stopped on its way and a bunch of hooligans board the bus and create a nuisance when a man tricks them into getting off the bus. Later the bus stops for a tea break and the hooligans arrive at the same time when suddenly a passing convoy is attacked and the assassins try to kill the man inside one of the cars. The man using his swiftness foils the attack and manages to nab and arrest one of the attackers while killing another.  The rest manage to escape the scene.

This man is Inspector Amar Damjee (Aamir Khan) and the person in the car turns out to be Chief Minister Vishwasrao Chowdhury (Raza Murad) who was impressed with Amar and delegates to him the task of locating the people behind a multi-crore rupee international scandal to Amar. Amar promises to fulfill the job to the best of his ability.

It is shown that Chaubey (an assistant to the CM) was the one who committed the fraud and is trying to ensure that he is not caught and hence he had asked the assassins to attack the CM. Amar tries to break the back of crime and this starts irritating Chaubey who senses how close Amar is getting.  Chaubey then frames Amar for the murder of the daughter of the Police Commissioner Mazumdar(Kulbhushan Kharbanda).

Amar, after a long fight with the head assassin Raghu, escapes prison along with several others.  He then plots a scheme to go undercover as a woman to find the man behind all of this.  He finds Chaubey and recognizes him as the man who killed both his parents.  Eventually, Chaubeys assassins take everyone hostage in a 12-story tower in attempt to attack the CM once again, but Amar slowly but surely rids of them all.  As Chaubey tries to escape the scene via a helicopter on the terrace, Amar prevents him and eventually knocks him into a satellite dish, electrocuting Chaubey to death.  Amar is congratulated by the CM and the Police Commissioner.

==Cast==
* Aamir Khan – Inspector Amar Damjee
* Mamta Kulkarni – Sanjana Roy, Journalist
* Paresh Rawal – Deputy CM Chaturvedi alias Chaubey/Javed
* Avtar Gill – Deshpandey, Sub Inspector
* Kulbhushan Kharbanda – Commissioner Mazumdar
* Raza Murad – Chief Minister Vishwasrao Chowdhury
* Satish Shah – Editor Roy
* Mukesh Rishi – Raghu
* Jaya Mathur – Anjalia
* Asrani – Hirandani 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dheere Dheere Aap Mere"
| Udit Narayan, Sadhana Sargam
|-
| 2
| "Jaane Mujhe Kya Hua"
| Udit Narayan, Sadhana Sargam
|-
| 3
| "Na Jaane Kya Ho Gaya"
| Udit Narayan, Sadhana Sargam
|-
| 4
| "Dole Dole Dil Dole"
| Kavita Krishnamurthy
|-
| 5
| "Dhadkata Hai Mera Dil"
| Udit Narayan, Kavita Krishnamurthy
|-
| 6
| "Maine Kaha Mohataram"
| Udit Narayan, Sadhana Sargam
|}

==Box office==
The film grossed    92.5 million at Indian box-office and was declared " Hit" by Box Office India. 

==References==
 

==External links==
*  

 

 
 
 
 
 