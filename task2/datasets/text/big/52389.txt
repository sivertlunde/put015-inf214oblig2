Independence Day (1996 film)
 
{{Infobox film
| name           = Independence Day
| image          = Independence day movieposter.jpg
| caption        = Theatrical release poster
| director       = Roland Emmerich
| producer       = Dean Devlin
| writer         = Dean Devlin Roland Emmerich
| starring       = Will Smith Bill Pullman Jeff Goldblum Mary McDonnell Judd Hirsch Margaret Colin Randy Quaid Robert Loggia James Rebhorn Harvey Fierstein Vivica A. Fox
| music          = David Arnold
| cinematography = Karl Walter Lindenlaub David Brenner
| studio         = Centropolis Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 145 Minutes/153 minutes (extended)
| country        = United States
| language       = English
| budget         = $75 million    
| gross          = $817.4 million 
}}
 Nevada desert alien attack human population, Independence Day holiday in the United States. The screenplay was written by Emmerich and producer Dean Devlin.

While promoting Stargate (film)|Stargate in Europe, Emmerich came up with the idea for the film when fielding a question about his own belief in the existence of alien life. He and Devlin decided to incorporate a large-scale attack when noticing that aliens in most invasion films travel long distances in outer space only to remain hidden when reaching Earth. Principal photography for the film began in July 1995 in New York City, and the film was officially completed on June 20, 1996.
 43rd highest-grossing Best Sound Mixing.

A sequel to Independence Day is scheduled to be released on June 24, 2016.

==Plot==
 
 alien mothership President Thomas J. Whitmore (Bill Pullman) about the attack. Whitmore orders large-scale evacuations of the targeted cities, but the aliens attack with advanced directed-energy weapons before these can be carried out. Whitmore, portions of his staff, and the Levinsons narrowly escape aboard Air Force One as Washington, D.C. is destroyed.
 Black Knights, force field. Vice President Roswell in 1947.

When scientist Dr. Brackish Okun (Brent Spiner) attempts to autopsy the alien, it regains consciousness and attempts to escape. When questioned by Whitmore, the alien attempts a psychic attack against him, but is killed by Whitemores security detail. Whitmore then mentions that while he was being attacked, he saw the aliens thoughts; what its species were planning to do. They were like locusts; their entire species travel from planet to planet, destroying all life and harvesting the natural resources.  Whitmore orders a nuclear attack on the destroyers, but the first attempt in Houston fails to penetrate the force field of the destroyer and the remaining strikes are aborted.

On July 4, Levinson devises a plan to use the repaired attacker to introduce a computer virus and plant a nuclear missile on board the mothership, theorizing that this will disrupt the force fields of the destroyers. Hiller volunteers to pilot the attacker, with Levinson accompanying him. With not enough military pilots to man all available aircraft, volunteers including Whitmore and Casse are enlisted for the counterstrike.

With the successful implantation of the virus, Whitmore leads the attack against an alien destroyer approaching Area 51. Although the force field is deactivated and the fighters are able to inflict damage, their supply of missiles quickly becomes exhausted. As the destroyer prepares to fire on the base, Casse has one missile left, but it jams, and he decides to sacrifice his own life. He flies his aircraft into the alien weapon with a kamikaze attack, destroying the craft. The Americans inform resistance forces around the world about how to destroy the other craft, while the nuclear device destroys the alien mothership as Hiller and Levinson escape. They return unharmed and reunite with their families. The whole world then celebrates its heroes victory as well as its true Independence Day.

==Cast==
  Six Degrees of Separation.  Persian Gulf War fighter pilot. To prepare for the role, Pullman read Bob Woodwards The Commanders and watched the documentary film The War Room. 
* Jeff Goldblum as David Levinson, an Massachusetts Institute of Technology|MIT-educated computer expert, chess enthusiast, and Environmentalism|environmentalist, working as a satellite technician in New York City. First Lady Marilyn Whitmore, President Whitmores wife.
* Judd Hirsch as Julius Levinson, Davids father. The character was based on one of producer Dean Devlins uncles. DVD commentary 
* Robert Loggia as General William Grey, a U.S. Marine Corps general who is the Chairman of the Joint Chiefs of Staff. Loggia modeled the character after generals of World War II, particularly George S. Patton.  crop duster abducted by the aliens ten years prior to the films events.
* Margaret Colin as Constance Spano, White House Communications Director and Davids former wife.
* Vivica A. Fox as Jasmine Dubrow, a single mother, Stevens girlfriend (later wife), and exotic dancer.
* James Rebhorn as Albert Nimzicki, the United States Secretary of Defense|U.S. Secretary of Defense and former director of the Central Intelligence Agency|CIA. Rebhorn described the character as being much like Oliver North.  The characters eventual firing lampoons Joe Nimziki,    Metro-Goldwyn-Mayer|MGMs head of advertising who reportedly accounted for unpleasant experiences for Devlin and Emmerich when studio executives forced recuts of Stargate (film)|Stargate.   
* Harvey Fierstein as Marty Gilbert, Davids boss.
* Adam Baldwin as Major Mitchell, a United States Air Force|U.S. Air Force officer who is Area 51s commanding officer.
* Brent Spiner as Dr. Brackish Okun, the unkempt and highly excitable scientist in charge of research at Area 51. Devlin, who is open to the idea of bringing Dr. Okun back in the event of a sequel, later implied the character is merely in a coma when he appears to have been killed by an alien.  The characters appearance and verbal style are based upon those of visual effects supervisor Jeffrey A. Okun, with whom Emmerich had worked on Stargate (film)|Stargate. 
* James Duval as Miguel Casse, Russells eldest son.
* Lisa Jakub as Alicia Casse, Russells teenage daughter.
* Giuseppe Andrews as Troy Casse, Russells younger son.
* Ross Bagley as Dylan Dubrow, Jasmine Dubrows son.
* Mae Whitman as Patricia Whitmore, President Whitmores daughter.
* Bill Smitrovich as Captain Watson.
* Kiersten Warren as Tiffany, Jasmines exotic dancer friend.
* Harry Connick, Jr. as Captain Jimmy Wilder, Steves best friend and fellow pilot. Connick took over the part for Matthew Perry, originally cast in the role.   digitallyobsessed.com. Retrieved July 8, 2008. 
* Frank Welker as Alien Vocal Effects

==Production==
 , "Black Knights"]]
The idea for the film came when Emmerich and Devlin were in Europe promoting their film Stargate (film)|Stargate. A reporter asked Emmerich why he made a film with content like Stargate if he did not believe in aliens. Emmerich stated he was still fascinated by the idea of an alien arrival, and further explained his response by asking the reporter to imagine what it would be like to wake up one morning and discover 15-mile-wide spaceships were hovering over the worlds largest cities. Emmerich then turned to Devlin and said, "I think I have an idea for our next film."  Aberly and Engel 1996, p. 8.    Entertainment Weekly. Retrieved July 8, 2008. 

Emmerich and Devlin decided to expand on the idea by incorporating a large-scale attack, with Devlin saying he was bothered by the fact that "for the most part, in alien invasion movies, they come down to Earth and theyre hidden in some back field ... r they arrive in little spores and inject themselves into the back of someones head." Aberly and Engel 1996, p. 93.  Emmerich agreed by asking Devlin if arriving from across the galaxy, "would you hide on a farm or would you make a big entrance?"  The two wrote the script during a month-long vacation in Mexico,  and just one day after they sent it out for consideration, 20th Century Fox chairman Peter Chernin greenlit the screenplay.  Pre-production began just three days later in February 1995.   The United States Armed Forces|U.S. military originally intended to provide personnel, vehicles, and costumes for the film; however, they backed out when the producers refused to remove the scripts Area 51 references. 

A then-record 3,000-plus special effects shots would ultimately be required for the film.  The shoot utilized on-set, in-camera special effects more often than Digimation|computer-generated effects in an effort to save money and get more authentic pyrotechnic results.  Many of these shots were accomplished at Hughes Aircraft in Culver City, California, where the films art department, motion control photography teams, pyrotechnics team, and model shop were headquartered. The productions model-making department built more than twice as many miniatures for the production than had ever been built for any film before by creating miniatures for buildings, city streets, aircraft, landmarks, and monuments. Aberly and Engel 1996, p. 72.  The crew also built miniatures for several of the spaceships featured in the film, including a 30-foot (9.1 m) destroyer model  and a version of the mother ship spanning  .  City streets were recreated, then tilted upright beneath a high-speed camera mounted on a scaffolding filming downwards. An explosion would be ignited below the model, and flames would rise towards the camera, engulfing the tilted model and creating the rolling "wall of destruction" look seen in the film.  A model of the White House was also created, covering   by  , and was used in forced-perspective shots before being destroyed in a similar fashion for its own destruction scene. Aberly and Engel 1996, p. 82.  The detonation took a week to plan  and required 40 explosive charges. 

  canyon, and the footage was used as pilot point-of-view shots. ]]

The films aliens were designed by production designer Patrick Tatopoulos. The actual aliens of the film are diminutive and based on a design Tatopoulos drew when tasked by Emmerich to create an alien that was "both familiar and completely original".  These creatures wear "bio-mechanical" suits that are based on another design Tatopoulos pitched to Emmerich. These suits were   tall, equipped with 25 tentacles, and purposely designed to show it could not sustain a person inside so it would not appear to be a "man in a suit". Aberly and Engel 1996, p. 91. 
 Kaiser Steel El Toro and Area 51 exteriors.  It was here where Pullman filmed his pre-battle speech. Immediately before filming the scene, Devlin and Pullman decided to add "Today, we celebrate our Independence Day!" to the end of the speech. At the time, the production was nicknamed "ID4" because Warner Bros. owned the rights to the title Independence Day, and Devlin had hoped if Fox executives noticed the addition in dailies, the impact of the new dialogue would help them win the rights to the title.  The right to use the title was eventually won two weeks later. 
 composited images were not added to the final print because production designers decided the blue panels gave the sets a "clinical look".  The attacker hangar set contained an attacker mock-up   wide  that took four months to build.  The White House interior sets used had already been built for The American President and had previously been used for Nixon (film)|Nixon.  Principal photography completed on November 3, 1995. 

The film originally depicted Russell Casse being rejected as a volunteer for the July 4 aerial counteroffensive because of his alcoholism. He then uses a stolen missile tied to his red biplane to carry out his suicide mission. According to Dean Devlin, test audiences responded well to the scenes irony and comedic value.  However, the scene was re-shot to include Russells acceptance as a volunteer, his crash course in modern fighter aircraft, and him flying an F/A-18 instead of the biplane. Devlin preferred the alteration because the viewer now witnesses Russell ultimately making the decision to sacrifice his life,  and seeing the biplane keeping pace and flying amongst F/A-18s was "just not believable".  The film was officially completed on June 20, 1996. 

===Music===
 
The score was composed by David Arnold and has received two official CD releases. RCA released a 50-minute album at the time of the films release. Then in 2010, La-La Land Records released a limited edition 2-CD set that comprised the complete score plus 12 alternate cues. 

==Release== Super Bowl air time to kick off the advertising campaign for potential blockbusters.   boxofficemojo.com. Retrieved July 8, 2008.    

Foxs Licensing and Merchandising division also entered into co-promotional deals with   entered a merchandising deal with the films producers to create a line of tie-in toys.    In exchange for product placement, Fox also entered into co-promotional deals with Molson Coors Brewing Company and Coca-Cola. 
 teaser trailers and marketing campaigns, acknowledging them as "truly brilliant".   atthemovies.tv. Retrieved July 8, 2008. 

 

The film had its official premiere held at Los Angeles now-defunct Mann Plaza Theater on June 25, 1996.    It was then screened privately at the White House for President Bill Clinton and his family    before receiving a nationwide release in the United States on July 2, 1996, a day earlier than its previously scheduled opening.   
 lenticular cover.  Often accessible on these versions is a special edition of the film, which features nine minutes of additional footage not seen in the original theatrical release.  Independence Day became available on Blu-ray Disc|Blu-ray discs in the United Kingdom on December 24, 2007,  and in North America on March 11, 2008.  The Blu-ray edition does not include the deleted scenes.

===Censorship=== militant group Hezbollah called for Muslims to boycott the film, describing it as "propaganda for the so-called genius of the Jews and their concern for humanity." In response, Jewish actor Jeff Goldblum said, "I think Hezbollah has missed the point: the film is not about American Jews saving the world; its about teamwork among people of different religions and nationalities to defeat a common enemy."  

==Reception==

===Box office===
 .]] 43rd highest worldwide gross of all-time for a film. Hoping to capitalize in the wake of the films success, several studios released more large-scale disaster films,  and the already rising interest in science fiction-related media was further increased by the films popularity. 

A month after the films release, jewelry designers and marketing consultants reported an increased interest in dolphin-themed jewelry, since the character of Jasmine in the film wears dolphin earrings and is presented with a wedding ring featuring a gold dolphin.   

===Critical response=== Jurassic Park. 

 .  Lisa Schwarzbaum of Entertainment Weekly gave it a B+ for living up to its massive hype, adding "charm is the foremost of this epics contemporary characteristics. The script is witty, knowing, cool."  Eight years later, Entertainment Weekly would rate the film as one of the best disaster films of all-time.  Kenneth Turan of the Los Angeles Times felt that the film did an "excellent job conveying the boggling immensity of   extraterrestrial vehicles   and panic in the streets" and the scenes of the alien attack were "disturbing, unsettling and completely convincing". 

However, the films nationalistic overtones were widely criticized by reviewers outside the U.S. Movie Review UK described the film as "A mish-mash of elements from a wide variety of alien invasion movies and gung-ho American   gave the film a Star (classification)|five-star rating in the magazines original review of the film. 
 their on-air review of the film.  

American Film Institute lists
* AFIs 100 Years...100 Thrills – Nominated 
* AFIs 10 Top 10 – Nominated Science Fiction Film 

===Accolades===
{| class="wikitable"
|- style="background:#b0c4de; text-align:center;"
! Award
! Subject
! Nominee
! Result
|- Cinema Audio CAS Awards   IMDb. Retrieved September 4, 2012.  Best Sound Mixing Chris Carpenter, Bob Beemer, Bill W. Benton and Jeff Wexler
| 
|- Academy Awards 
| 
|- Academy Award Best Visual Effects Volker Engel, Douglas Smith, Clay Pinney and Joe Viskocil
| 
|- Saturn Awards  Saturn Award Best Special Effects
| 
|- Saturn Award Best Science Fiction Film
|
| 
|- Saturn Award Best Director Roland Emmerich
| 
|- Saturn Award Best Writer Roland Emmerich and Dean Devlin
| 
|- Saturn Award Best Costumes Joseph A. Porro
| 
|- Saturn Award Best Supporting Actor Brent Spiner
| 
|- Saturn Award Best Supporting Actress Vivica A. Fox
| 
|- Saturn Award Best Young Actor James Duval
| 
|- Saturn Award Best Music David Arnold
| 
|- Saturn Award Best Actor Jeff Goldblum
| 
|- Will Smith
| 
|- Nickelodeon Kids Choice Awards|Kids Choice Awards  Favorite Movie Actor
| 
|- Favorite Movie
|
| 
|- Hugo Awards  Hugo Award Best Dramatic Presentation
|
| 
|- Young Artist Awards  Best Young Actor – Age 10 or Under Ross Bagley
| 
|-
|rowspan=1|Peoples Choice Awards  Favorite Dramatic Motion Picture
|
| 
|- MTV Movie Awards  MTV Movie Best Action Sequence Aliens blow up cities
| 
|- MTV Movie Best Movie
|
| 
|- Best Male Performance Will Smith
| 
|- Best Breakthrough Performance Vivica A. Fox
| 
|- Best Kiss Will Smith and Vivica A. Fox
| 
|- Grammy Awards  Best Instrumental Composition Written for a Motion Picture or for Television David Arnold
| 
|- Satellite Awards  Outstanding Visual Effects Volker Engel, Douglas Smith, Clay Pinney and Joe Viskocil
| 
|- Outstanding Film Editing David Brenner David Brenner
| 
|- Mainichi Film Awards  Best Foreign Language Film
|
| 
|- Japan Academy Japanese Academy Awards 
|
| 
|- Amanda Awards 
|
| 
|- Blockbuster Entertainment Awards  Favorite Actor – Sci-Fi Will Smith
| 
|- Universe Readers Choice Awards  Best Actor
| 
|- Best Supporting Actress Vivica A. Fox
| 
|- Best Science Fiction Film
|
| 
|- Best Special Effects Volker Engel, Douglas Smith, Clay Pinney and Joe Viskocil
| 
|- Best Director Roland Emmerich
| 
|- Best Score David Arnold
| 
|- Best Cinematography Karl Walter Lindenlaub
| 
|- Best Writing Roland Emmerich and Dean Devlin
| 
|- Golden Raspberry Awards  Worst Written Film Grossing Over $100 Million
| 
|-
|}
== In other media ==
===Books===
Author Stephen Molstad wrote a tie-in novel to help promote the film shortly before its release. The novel goes into further detail on the characters, situations, and overall concept not explored in the film. The novel presents the films finale as originally scripted, with the character played by Randy Quaid stealing a missile and roping it to his crop duster biplane.

Following the films success, a prequel novel entitled Independence Day: Silent Zone was written by Molstad in February 1998.  The novel is set in the late 1960s and early 1970s, and details the early career of Dr. Brackish Okun. 

Molstad wrote a third novel, Independence Day: War in the Desert in July 1999. Set in Saudi Arabia on July 3, it centers around Captain Cummins and Colonel Thompson, the two Royal Air Force officers seen receiving the Morse code message in the film.
 Marvel comic book was also written based on the first two novelizations.

===Radio===
On August 4, 1996,  ; the first 20 minutes were set as being live. 

===Computer games=== mobile version was released in 2005. A computer game entitled ID4 Online was released in 2000. 

===Toys===
Trendmasters released a toy line for the film in 1996.  Each action figure, vehicle or playset came with a 3 1⁄2" floppy disk that contained an interactive computer game. 

==Sequel==
  
The possibility of a sequel had long been discussed,    and Devlin once stated the worlds reaction to the September 11 attacks influenced him to strongly consider making a sequel to the film.       Devlin began writing an outline for a script with Emmerich,    but in May 2004, Emmerich said he and Devlin had attempted to "figure out a way how to continue the story", but that this ultimately did not work, and the pair abandoned the idea.  In October 2009, Emmerich said he once again had plans for a sequel,    and has since considered the idea of making two sequels to form a trilogy.  On June 24, 2011, Devlin confirmed that he and Emmerich have found an idea for the sequels and have written a treatment for it, with both Emmerich and Devlin having the desire for Will Smith to return for the sequels.  In October 2011, however, discussions for Smith returning were halted, due to Foxs refusal to provide the $50 million salary demanded by Smith for the two sequels. Emmerich, however, made assurances that the films would be shot Back to back film production|back-to-back, regardless of Smiths involvement.  In July 2012, Devlin reiterated that the Independence Day sequel is still in development, and the script currently takes place in 2012, 16 years after the original films events. 

In March 2013, Emmerich stated that the titles of the new films would be ID Forever Part I and ID Forever Part II.  The films will take place twenty years after the original, when reinforcements of the original alien race arrive at Earth after finally receiving a distress call. Bill Pullman has confirmed his participation, though Will Smith has not. The new films will focus on the next generation of heroes, including the stepson of Smiths character in the original film. In May 2013, Roland Emmerich and Dean Devlin mentioned that wormholes would be used as a plot device in ID Forever and added that they would like Jeff Goldblum to reprise his role from the original.  The film was originally going to be released on July 3, 2015.  In June 2013, Emmerich confirmed to the New York Daily News that Will Smith is not returning to the sequel because "he’s too expensive".  Later in June, it was officially confirmed that both Goldblum and Pullman would return in the sequel, and that a gay character would be prominently featured.     On September 26, 2013, actor Michael B. Jordan was said to have been considered for a role in the film.    On November 12, 2013, it was announced that the first sequel had been rescheduled for a July 1, 2016 release.  On May 29, 2014, it was announced that the script for the first sequel written by Emmerich and Devlin, would be rewritten by Carter Blanchard.  On October 14, 2014, Fox moved up the release date to June 24, 2016.  On November 26, 2014, the sequel was given an official green light by 20th Century Fox with a release date of June 24, 2016, noting that this will be a stand-alone sequel that will not split into two parts as originally planned, with filming beginning in May 2015 and casting being done after the studio locks down Emmerich as the director on the film.  On December 4, 2014, Devlin confirmed that Emmerich would indeed be directing the sequel.  On January 27, 2015, The Wrap reports that Liam Hemsworth is being eyed for a role in the sequel.  In March 2015, Jessie Usher was cast as Steves son. Emmerich announced on Twitter that Hemsworth joined the cast along with Charlotte Gainsbourg while Jeff Goldblum and Vivica A. Fox were returning as David Levinson and Jasmine Dubrow in the film.       In an interview with The Hollywood Reporter, Emmerich said that film sequel will cost about $150-$160 million.  On March 24, 2015 it was revealed that Emmerich is in talks with Travis Tope for an undisclosed role. 

It was later announced that Brent Spiner would return for the sequel and Joey King will be a new addition. On April 17, 2015, it was announced that Bill Pullman and Judd Hirsch had officially signed on to return.  On April 27, 2015, Emmerich announced on his Twitter page that Maika Monroe was cast in the sequel. 

==See also==
* List of films featuring extraterrestrials

==References==
 
* Aberly, Rachel and Volker Engel. The Making of Independence Day. New York: HarperPaperbacks, 1996. ISBN 0-06-105359-7.

==External links==
 
*  
*  
*  
*  
*  
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 