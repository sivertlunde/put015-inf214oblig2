Hard to Get (1929 film)
{{Infobox film
| name =  Hard to Get 
| image =
| image_size =
| caption =
| director = William Beaudine
| producer = Ray Rockett Richard Weil
| narrator = James Finlayson   Louise Fazenda 
| music = Alois Reiser 
| cinematography = John F. Seitz 
| editing = Stuart Heisler 
| studio = First National Pictures 
| distributor = Warner Brothers
| released = August 4, 1929 
| runtime = 80 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} James Finlayson. A dress shop employee falls in love with a millionaire.

A lost film per IMDb. 

==Cast==
*  Dorothy Mackaill as Bobby Martin 
* Charles Delaney as Jerry Dillon  James Finlayson as Pa Martin 
* Louise Fazenda as Ma Martin 
* Jack Oakie as Marty Martin 
* Edmund Burns as Dexter Courtland 
* Clarissa Selwynne as Mrs. Cortland 
* Margaret Beaudine as Martin daughter  

==References==
 

==Bibliography==
* Marshall, Wendy L. William Beaudine: From Silents to Television. Scarecrow Press, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 

 