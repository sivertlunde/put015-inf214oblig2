Guddu
{{Infobox Film
| name           = Guddu गुड्डु  
| image          = Guddu.jpg
| image_size     = 
| caption        = DVD cover
| writer         = Abrar Alvi 
| starring       = Shah Rukh Khan Manisha Koirala Deepti Naval
| director       = Prem Lalwani
| producer       = Prem Lalwani
| distributor    = 
| released       =  
| runtime        = 
| language       = Hindi
| music          = Naushad
| budget         = 
}}

Guddu ( ,  }}) is a 1995 Bollywood romance film written by Abrar Alvi and directed by Prem Lalwani. The movie stars Shah Rukh Khan, Manisha Koirala, Deepti Naval, Mukesh Khanna and Mehmood Ali|Mehmood. Its music was composed by Naushad.

== Plot ==

Guddu Bahadur (Shah Rukh Khan) is the only child in the Bahadur family, consisting of Advocate Vikram (Mukesh Khanna) and his wife, Kavita (Deepti Naval). While Kavita is a devout and a religious Hindu, Vikram is an atheist. Guddu and Salina Gupta (Manisha Koirala) are in love. One day while driving, they meet with an accident and Salina loses her sight. Guddu blames himself and so do Salinas parents. Guddu is asked to keep away from Salina. Salina herself knows that the accident was not Guddus fault, but would not like to meet Guddu and be a burden on him. Guddu overhears a conversation between their family doctor and his dad and he comes to know that he has brain tumor. He has only a few more months to live. He wants to donate his eyes to the hospital so Salina can benefit from them, but his dad will not even consider this. Things take a turn for the worse, when Guddu decides to go to court to assert his right to donate his eyes, his dad ends in the hospital and so does Guddu. Kavita couldnt bear the trauma of her husbands illness and her sons disease, thus she decides to devote herself to god for five days without drinking a sip of water. Her prayers get answered and her husbands life is saved and her sons operation is successful. She dies in the feet of god while praying and her eyes get donated to Salina. Salina and Guddu get married and are blessed with twins and they live happily ever after.

== Cast ==
* Shahrukh Khan ... Guddu Bahadur
* Manisha Koirala ... Salina Gupta
* Deepti Naval ... Kavita Bahadur
* Mukesh Khanna ... Advocate Vikram Bahadur
* Ashok Saraf ... Baliya
* Shylendar ... Suraj

==Music==
{{Infobox album   
| Name = Guddu
| Type = soundtrack 
| Artist = Naushad 
| Cover = 
| Released = 1995
| Recorded =  Feature film soundtrack 
| Length = 
| Label =  
| Producer = 
| Music Director = Naushad 
| Reviews = 
| Last album = Aawaz De Kahan Hai (1990)
| This album = Guddu (1995)
| Next album =   (2005)
}}
The soundtrack for the movie was composed by Naushad and lyrics penned by Majrooh Sultanpuri. The soundtrack consists of 7 songs, featuring vocals by Lata Mangeshkar, Devaki Pandit, Kumar Sanu, Mohammed Aziz, Sadhana Sargam, Majid Shola and Suresh Wadkar.

#Thandi Mein Pasina - Devaki Pandit, Kumar Sanu
#Gulashan-Gulashan Kali-Kali - Suresh Wadkar, Devaki Pandit
#Dil Hai Pyare - Kumar Sanu, Sadhana Sargam
#Dil Kahe Har Dum - Suresh Wadkar, Mohammed Aziz, Majid Shola
#Hum Dono Panchhi - Devaki Pandit, Kumar Sanu
#Mere To Radheshyam - Lata Mangeshkar
#Pyar Mera Zindagi - Kumar Sanu, Devaki Pandit

==External links==
*  

 
 
 
 