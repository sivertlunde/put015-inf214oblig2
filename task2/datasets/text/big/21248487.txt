Donsol (film)
{{Infobox Film
| name           = Donsol
| image          = donsol-movie.jpg
| image_size     =
| caption        = Movie Poster 
| director       = Adolfo Alix, Jr.
| producer       = Bicycle Pictures
| writer         = Adolf Alix 
| starring       = Sid Lucero Angel Aquino Cherie Gil Bembol Roco Jacklyn Jose
| music          = Jesse Lucas
| cinematography = Eli Balce
| editing        = Tara Illenberger
| distributor    = Bicycle Pictures Star Cinema
| released       = 2006
| runtime        = 145 minutes 
| location       = Donsol, Sorsogon
| language       = Filipino, Bicolano
}}

Donsol is an indie film written and directed by Adolfo Alix, Jr. about a summer love made in the island of Donsol. The movie produced by Bicycle Pictures stars Sid Lucero and Angel Aquino.

==Synopsis==
Donsol is a movie about a whaleshark guide named Daniel who falls in love with the beautiful but mysterious tourist Teresa. The two individuals are nursing their own heartaches but find themselves hopelessly drawn to each other; Daniel was left behind by his girlfriend for a rich man, Teresa a widowed breast cancer patient.

The film was shot entirely in Donsol, Sorsogon and there are underwater shots of the star of the movie the butanding (whaleshark). It tackled major issues like illegal fishing that poses threats to the butanding. Fidel (Daniels father) is an illegal fisherman and was caught by the police coast guard. This created friction between the relationship of Daniel and the kapitana of the town who provided jobs for them as a Butanding Interaction Officer or BIO.

The film was the Philippines official submission to the OSCARS Foreign Language Film category in 2007.

==Cast==
* Sid Lucero &ndash; Daniel
* Angel Aquino &ndash; Teresa
* Cherie Gil &ndash; Mars
* Jacklyn Jose &ndash; Ligaya
* Bembol Roco &ndash; Fidel
* Kenneth Ocampo &ndash; Nog
* Mark Gil &ndash; Dustin
* Aaron Junatas &ndash; Daniels younger brother

==Awards==

{|class=wikitable border="1" style="border-collapse:collapse;background:#f9f9f9;border: 1px #aaa solid;"
!Year
!Result
!Award
!Category/Recipient
|- Goldent Screen Award || Best Cinematography (Eli Balce) and Best Breakthrough Performance by an Actor (Sid Lucero)
|-
|  2007 || nominated || Gawad Urian Award || Best Actor (Sid Lucero)
|-
|  2007 || nominated || FAMAS Award || Best Art Direction (Gessan Enriquez), Best Child Actor (Aaron Junatas), and Best Musical Score (Jesse Lucas)
|-
|  2007 || won || FAMAS Award || Best Cinematography (Eli Balce)
|-
|  2006 || won || Spirit of the Independent Award || Feature Category
|-
|  2006 || won || Balanghai Trophy || Best Cinematography and Best Performance of an Actress (Angel Aquino)
|-
|}

==See also==
* Donsol, Sorsogon

==References==
*  
*  

== External links ==
*  

 
 
 

 