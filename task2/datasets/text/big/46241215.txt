Dhire Bohe Meghna
 

{{Infobox film
| name           = Dhire Bohe Meghna
| image          =   
| alt            = theatrical release poster
| caption        = theatrical release poster Alamgir Kabir
| producer       = Bangladesh Films International
| writer         = 
| screenplay     = Alamgir Kabir
| story          = Alamgir Kabir
| based on       =  
| starring       = {{Plain list |
* Bobita
* Hasu Banerjee
* Ajmal Huda Golam Mustafa
* Bulbul Ahmed Anwar Hossain
* Khalil Ullah Khan
}}
| narrator       = 
| music          = Sataya Saha
| cinematography = {{Plain list |
* Arun Roy
* M A Mobin
* Komal Nayek
}}
| editing        = Debobrato Sen Gupta
| studio         = 
| distributor    = Star Film Distribution
| released       = 1973 
| runtime        = 109&nbsp;Minutes
| country        = Bangladesh Bengali
| budget         =  
| gross          =  
}}
 Bengali war Alamgir Kabir and it was his first Feature Film.   

== Background ==
Initially Zahir Raihan was the original planner of the movie  Dhire Bohe Meghna .

==Plot==

 

== Cast ==
* Bobita
* Hasu Banerjee
* Ajmal Huda Golam Mustafa
* Bulbul Ahmed Anwar Hossain
* Khalil Ullah Khan

== Music == Hemanta Mukherjee and Sandhya Mukhopadhyay gave voice for the two songs.

{{Track listing
| extra_column    = Singer

| title1          = Kato je dhire bohe Megna (কতো যে ধীরে বহে মেঘনা) Hemanta Mukherjee, Sandhya Mukhopadhyay
| length1         = 3:46

}}

== Reception ==
In 2002, the British Film Institutes list of South Asian films,  Dhire Bohe Meghna  was ranked in No.8 of the top 10 ranked movie.   

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 