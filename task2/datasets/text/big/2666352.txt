Garçon stupide
{{Infobox film 
 | name = Garçon stupide
 | image = Garçon stupide film.jpg
 | caption = 
 | director = Lionel Baier 
 | writer = Lionel Baier, Laurent Guido
 | starring = Pierre Chatagny, Natacha Koutchoumov 
 | producer = Robert Boner 
 | distributor = Picture This!
 | released = 10 March 2004 ( Switzerland ) 
 | runtime = 94 minutes
 | country = Switzerland French
 | budget = 
  | }}
 2004 film directed by Lionel Baier.

==Main cast==
*Pierre Chatagny &ndash; Loïc
*Natacha Koutchoumov &ndash; Marie
*Rui Pedro Alves &ndash; Rui
*Lionel Baier &ndash; Lionel

This French language film tells the story of 20 year old Loic, who works by day in a chocolate factory, and by night cruises the internet for sex with older men. His life is a series of pointless anonymous sexual encounters until he meets one man who appears to be interested in him for himself, and not just his body.

Loics journey to self-awareness is told through a series of episodic events, such as the suicide of his best friend, his growing infatuation with a local teams soccer star, a car accident and subsequent hospitalisation which uncomfortably reunites him with his parents. At the end of the film he realizes that he can be his own person and he recites a list of things he will never do in order to fit in and belong.

==External links==
* 
* 

 
 
 
 
 
 
 
 


 
 