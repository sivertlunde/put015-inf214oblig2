Nausherwan-E-Adil
{{Infobox film
| name           = Nausherwan-E-Adil 
| image          = Nausherwan-E-Adil.jpg 
| image_size     = 
| caption        = 
| director       = Sohrab Modi
| producer       = Minerva Movietone
| writer         = Shams Lucknowi
| narrator       = 
| starring       = Sohrab Modi Naseem Banu Mala Sinha Raaj Kumar
| music          = C. Ramchandra Parwaiz Shamshi (lyrics)
| cinematography = Lateef Bhandare
| editing        = P. Bhalchandra
| distributor    =
| studio         = Minerva Movietone
| released       = 1957
| runtime        = 137 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1957 costume action drama Hindi/Urdu film directed by Sohrab Modi.  Produced by Minerva Movietone, it had music composed by C. Ramchandra with lyrics by Parwaiz Shamshi.  The story, screenplay and dialogue writer was Shams Lucknowi. The cinematographer was Lateef Bhandare. The cast included Sohrab Modi, Naseem Banu, Mala Sinha, Raaj Kumar, Bipin Gupta and Shammi (actress)|Shammi. 
 Pukar (1939).   

==Plot==
Sultan-e-Iran (Emperor of Iran) is a Just ruler and known as such, Nausherwane-e-Adil (Nausherwan The Just) (Sohrab Modi). His laws are equal for everyone and follows them in his dispensation of Justice. However when questioned about a certain ruling by a Christian priest David (Bipin Gupta), he realises that the laws he is following have come down through ages without being written down. He decides to rectify this and gets his Wazir (Niranjan Sharma) to start work on it. Nausherwan now decides to go incognito into his country to see for himself if his people are contented and happy. When he returns he sets about bringing reformation in the laws with the help of his judiciary. He puts forth two laws; anyone deceiving a girl will be walled up, and anyone betraying the state will be put to death.

Nausherwan has a wife (Naseem Banu) and two sons, Naushahzad (Raaj Kumar) and Hormuz. He now declares his older son Naushahzad as heir to the throne. Naushahzad saves a young girl when he fishes her out of the water with the help of his friend Altaf (Agha (actor)|Agha). They take her to the priest David where it’s discovered that she’s his long-separated daughter, Marcia (Mala Sinha). Naushazad says he’s a Christian named Joseph. Soon Marcia and David fall in love. Joseph now declares to his mother (whos also a Christian) that hes a Christian, but she asks him to keep it hidden from his father as only an Iranian (Zorastrian) can become the ruler. Complications arise and Nausherwan is tested when David and Marcia come to him demanding justice as they feel Naushhzad, as Joseph, has deceived them. When Nausherwan pronounces judgement as set down by his laws, Naushahzad revolts and is fought off by the Commander (Murad (actor)|Murad) who wounds him critically. Marcia kills herself with a dagger. Nausherwan now renounces his kingdom and establishes his younger son Hormuz as the new ruler.

==Cast==
* Sohrab Modi as Sultan-e-Iran Nausherwan-e-Adil
* Mala Sinha as Marcia
* Raaj Kumar as Shehzada Naushazad / Joseph
* Naseem Banu as Malika-E-Iran
* Bipin Gupta as Hakim David Murad as Ram Wazir Agha as Altaf Shammi as Altaf’s wife
* Niranjan Sharma as Wazir-e-Ala
* Amirbano
* Sheelabano
* Hira Sawant as dancer

==Soundtrack==
The film had C. Ramchandras music direction and the lyricist was the poetess Parvaiz Shamsi. The playback singers were Mohammed Rafi, Lata Mangeshkar, Asha Bhosle, Zohrabai Ambalewali and Chandbala.  The film had some popular songs including the ghazal written Parvaiz Shamsi, "Yeh Hasrat Thi Ki Is Duniya Mein Bas Do Kaam Kar Jaate" sung by Mohammed Rafi for Raaj Kumar.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Yeh Hasrat Thi Ke Is Duniya Mein Bas Do Kaam Kar Jaate 
| Mohammed Rafi
|-
| 2
| Taron Ki Zuban Par Hai Muhabbat Ki Kahani 
| Lata Mangeshkar, Mohammed Rafi
|-
| 3
| Bhool Jayein Sare Gham, Doob Jaayein Pyaar Mein 
| Lata Mangeshkar, Mohammed Rafi
|-
| 4
| Yeh Nazakat Yeh Aalam Shabab Ka 
| Asha Bhosle
|-
| 5
| Bhini Bhini Hai Mithi Mithi Hai 
| Lata Mangeshkar
|-
| 6
| Ham Aah Bhi Bharte Hain, 
| Lata Mangeshkar
|-
| 7
| Raat Dhalee, Jaan Chalee 
| Lata Mangeshkar
|-
| 8
| Kar Khushamad Toh Har Ek 
| C. Ramchandra
|-
| 9
| Mere Dard-e-Jigar Ki Har Dhadkan 
| Zohrabai Ambalewali, Asha Bhosle, Chandbala
|}

==Trivia==
Raaj Kumar (credited in the film as Raj Kumar) was unable to make his mark following this film, even though he was cast as a hero opposite Mala Sinha.   

==References==
 

==External links==
* 
*  Muvyz, Inc.

 

 
 
 
 