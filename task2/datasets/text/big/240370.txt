Resurrection (1980 film)
 
{{Infobox film
| name           = Resurrection
| image          = Resurrection_poster.jpg
| image_size     =
| caption        = Promotional poster for Resurrection
| director       = Daniel Petrie
| producer       = Renee Missel and Howard Rosenman
| writer         = Lewis John Carlino
| narrator       =
| starring       = Ellen Burstyn Sam Shepard Richard Farnsworth Roberts Blossom Clifford David Pamela Payton-Wright Jeffrey DeMunn Eva Le Gallienne
| music          = Maurice Jarre
| cinematography = Mario Tosi
| editing        = Rita Roland
| distributor    = Universal Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
}}
Resurrection is a 1980 film which tells the story of a woman who survives the car accident which kills her husband, but discovers that she has the power to heal other people. She becomes an unwitting celebrity, the hope of those in desperate need of healing, and a lightning rod for religious beliefs and skeptics. The film stars Ellen Burstyn, Sam Shepard, Richard Farnsworth, Roberts Blossom and Eva Le Gallienne.

The movie was written by Lewis John Carlino and directed by Daniel Petrie.
 Best Actress Best Actress in a Supporting Role (Eva Le Gallienne).

In 2010, this film was released on DVD as part of the Universal Vault Series of DVD-on-Demand titles.

A novelization was written by George Gipe.

==Cast==
 
* Ellen Burstyn as Edna Mae (Harper) McCauley
* Jeffrey DeMunn as Joseph "Joe" McCauley, Ednas husband 
* Roberts Blossom as John Harper, Ednas father
* Eva Le Gallienne as Pearl, Ednas grandmother 
* Lois Smith as Kathy, Ednas cousin 
* Sam Shepard as Cal Carpenter, Ednas lover Richard Hamilton as Reverend Earl Carpenter, Cals father
* Richard Farnsworth as Esco Brown, owner of "Last Chance Gas" 
* Sylvia Walden as Louise, a woman Edna heals in Los Angeles  Madeline Thornton-Sherwood as Ruth, a townswoman Edna heals 
 

==External links==
* 
* 1980 film:  
* 1999 TV film:  

 
 

 
 
 
 
 
 
 
 
 
 

 