Joe Kidd
{{Infobox film
| name           = Joe Kidd
| image          = Joe kidd.jpg
| caption        = Promotional movie poster for the film
| director       = John Sturges Robert Daley
| writer         = Elmore Leonard John Saxon
| music          = Lalo Schifrin
| cinematography = Bruce Surtees
| editing        = Ferris Webster
| distributor    = Universal Pictures
| released       =   
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $6 million Glenn Lovell, Escape Artist: The Life and Films of John Sturges, University of Wisconsin Press, 2008 p277 
}} western film starring Clint Eastwood and Robert Duvall, written by Elmore Leonard and directed by John Sturges.

The film is about an ex-bounty hunter hired by a wealthy landowner named Frank Harlan to track down Mexican revolutionary leader Luis Chama, who is fighting for land reform. It forms part of the Revisionist Western genre.

==Plot== John Saxon) has organized a peasant revolt against the local landowners, who are throwing the poor off land that rightfully belongs to them.

When a posse — financed by wealthy landowner Frank Harlan (Robert Duvall) — is formed to capture Chama, Kidd is invited to join but prefers to remain neutral. Harlan persists and Kidd finally relents when he learns that Chamas band has raided his own ranch and attacked one of the workers.

The posse rides into a village and forces the villagers into the church at gunpoint. They threaten to kill five Mexican hostages unless Chama surrenders. Harlan throws Kidd into the church to prevent him from helping Helen, a female captive who is also Chamas lady love (unbeknownst to Harlan), and the other Mexican hostages.

Kidd manages a daring escape and saves the hostages, determined to find Chama himself and see that justice is done. But when he does capture Chama and delivers him to Sheriff Mitchell (Gregory Walcott) Harlan is already waiting for them in town. 

To get to the jailhouse, Kidd drives a steam engine through the town saloon. A gunfight then ensues between Kidd and Harlans men. Kidd manages to kill Harlan in the court house by hiding in the judges chair. Chama then surrenders to Mitchell but not before Kidd punches the sheriff for not standing up to Harlan and his murderous plan. Kidd then collects his things and leaves town with Helen.

==Cast==
*Clint Eastwood as Joe Kidd 
*Robert Duvall as Frank Harlan  John Saxon as Luis Chama 
*Don Stroud as Lamarr Simms 
*Stella Garcia as Helen Sanchez 
*James Wainwright as Olin Mingo 
*Paul Koslo as Roy Gannon 
*Gregory Walcott as Sinola County Sheriff Bob Mitchell 
*Dick Van Patten as Hotel manager 
*Lynne Marta as Elma 
*John Carter as Judge 
*Pepe Hern as Priest 
*Joaquín Martínez as Manolo 
*Ron Soble as Ramon 
*Pepe Callahan as Naco
*Clint Ritchie as Deputy Calvin

==Production==
  John Saxon. Robert Duvall was cast as Frank Harlan, a ruthless land owner who hires Eastwoods character, a former frontier guide named Joe Kidd, to track down the culprits and scare them away. Don Stroud, who Eastwood had starred alongside in Coogans Bluff (film)|Coogans Bluff, was cast as another sour villain who encounters Joe Kidd. 
 June Lake, east of the Yosemite National Park.  The actors were initially uncertain with the strength of the three main characters in the film and how the hero Joe Kidd would come across. McGilligan (1999), p.217  According to writer Leonard, the initial slow development between the three was probably because the cast were so initially awestruck by having Sturges direct that they surrendered authority to him.  
Eastwood was far from being in perfect health during the film and suffered symptoms that suggested a bronchial infection in addition to having several panic attacks, falsely reported in the media as his having an allergy to horses. McGilligan (1999), p. 219  During production, the script for the finale was altered when producer Bob Daley jokingly said that a train should crash through the barroom in the climax, and he was taken seriously by cast and crew, who thought it was a great idea. 

==Reception==
Joe Kidd was released in the United States in July 1972, and it eventually grossed $5.8 million.  The film received a mixed reception from critics. Roger Greenspun of The New York Times, in his review of the film wrote, "I think it is a very good performance in context. Like so many Western heroes, Joe Kidd figures even in his own time as an anachronism — powerful through his instincts mainly, and through the ability of everybody else, whether in rage or gratitude, to recognize in him a quality that must be called virtue. The great value of Clint Eastwood in such a position is that he guards his virtue very cannily, and in the society of "Joe Kidd," where the men still manage to tip their hats to the ladies, but just barely, all the Eastwood effects and mannerisms suggest a carefully preserved authenticity."  The New York Post praised the actors performances while criticizing the film, calling the actors "diamonds set in dung". Hughes, p.27 

==References==
 

==Bibliography==
* 
* 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 