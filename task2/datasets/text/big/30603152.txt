The Case of Itaewon Homicide
{{Infobox film name           = The Case of Itaewon Homicide  image          = The Case of Itaewon Homicide.jpg border
| film name = {{Film name hangul         =    hanja          =   rr             = Itaewon Salinsageon  mr             = It‘aewŏn Sarinsakŏn}} director       = Hong Ki-sun producer       = David Cho Lee Jeong-hee Jeong Seong-hun Sin Beom-su writer         = Lee Maeyu-gu starring       = Jang Keun-suk Jung Jin-young music          = Park Ji-woong cinematography = Oh Cheng-ok editing        = Kang Sung-hoon distributor    =  released       =   runtime        = 100 minutes country        = South Korea language       = Korean budget         =  gross          = $3,337,582 
}}
The Case of Itaewon Homicide ( ; Transliteration|translit.&nbsp;Itaewon Salinsageon) is a 2009 South Korean film, based on the true story of the Itaewon murder case, which shocked Korea when college student Jo Jong-Pil was found dead at an Itaewon Burger King in 1997. Two teenagers, Arthur Patterson, the son of an American service member and Korean mother, and Edward Lee, became suspects but were eventually freed due lack of evidence and the case was never solved. It stars Jang Keun-suk as Arthur Patterson and Jung Jin-young as his lawyer. Song Joong-ki played the victim. The film had 531,068 admissions in South Korea nationwide. 

==Pending extradition of Arthur Patterson==
Public prosecutors in South Korea have reopened the case, after finding DNA evidence proving Patterson the murderer and testimony from Edward Lee, the witness. Patterson was arrested by US authorities in May 2011, and is currently undergoing court hearing in California for extradition to Korea for trial and conviction.

The murder was investigated in 1997 by CID Sleuths J. Choi, D. Zeliff, T. Barnes and B. Crow.  

==Cast==
* Jung Jin-young as Prosecutor Park
* Jang Geun-suk as Robert J. Pearson (Arthur Patterson)
* Shin Seung-hwan as Alex "AJ" Jung (Edward Lee)
* Oh Gwang-rok as Attorney Kim Byeon
* Ko Chang-seok as Alexs father
* Song Joong-ki as Jo Jong-Pil (murder victim)
* Kim Jung-ki as Judge
* Choi Il-hwa as Jong-Pils father
* Kim Min-kyung as Jong-Pils mother
* Song Young-chang
* Jo Seung-eun
* Park Jin-young as Attorney Jang
* Jin Kyung as Prosecutor Parks wife

== References ==
 

==External links==
*   at HanCinema
*  
*  
 
 
 
 
 
 
 
 

 