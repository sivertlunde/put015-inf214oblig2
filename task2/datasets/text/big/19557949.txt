Panic in Paradise (film)
{{Infobox film
| name           = Panic in Paradise
| image          = 
| caption        = 
| director       = Hagen Hasselbalch
| producer       = Hagen Hasselbalch
| writer         = Hagen Hasselbalch Tørk Haxthausen Børge Høst
| starring       = Alf Kjellin
| music          = 
| cinematography = Hagen Hasselbalch Ole Roos
| editing        = Mogens Green Hagen Hasselbalch
| distributor    = 
| released       = 18 October 1960
| runtime        = 82 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Panic in Paradise ( ) is a 1960 Danish comedy film directed by Hagen Hasselbalch and starring Alf Kjellin.

==Cast==
* Alf Kjellin - Frederik
* Katarina Hellberg
* Mogens Brandt
* Dirch Passer - Greven
* William Brüel
* Helge Kjærulff-Schmidt - Gas & Vandmesteren
* Kirsten Olsen
* Vivi Ancher
* Olaf Ussing
* Paul Møller
* Ego Brønnum-Jacobsen
* Erik Fiehn
* Johannes Allen
* Tex Ilden
* Finn Methling
* Peter Kitter
* Henrik Antonsen
* Erik Persson - (voice)

==External links==
* 

 
 
 
 
 


 
 