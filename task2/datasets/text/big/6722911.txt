Big Mama (film)
{{Infobox Film
| name           = Big Mama
| image          = 
| caption        = 
| director       = Tracy Seretean
| executive      = Mitchell Block and Sheila Nevins
| producer       = Tracy Seretean
| writer         = 
| music          = Bobby McFerrin Rob Mounsey
| cinematography = Tamara Goldsworthy
| editing        = 
| distributor    = California Newsreel
| released       = 2001
| runtime        = 35 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
| mpaa_rating    = 
| tv_rating      = 
}}
 Best Documentary Short Subject.   

==Plot==
The film follows 18 months in the life of Viola Dees (89 years old) as she tries to persuade Los Angeles authorities that she can care for her grandson, 9-year-old Walter. Born to a drug addicted mother, Walter was in foster care until Dees managed to get him released into her care at the age of four. He was a very disturbed child, traumatized by the death of his father and the disappearance of his mother, while still appearing bright and sweetly loving to his grandmother.

The film focuses on the continuous battle against age discrimination faced by Dees and many like her. While contending with her own declining health, and a bureaucratic and legal system that continually threatens to force them apart, Dees fights the misconception that age supersedes ones ability to love and care for a child.

The film continues to follow the family when life deals them several blows. Dees suffers a heart attack, provoking hostile and disturbed behavior from Walter who burns their house down when he sets a magazine ablaze in his room. When Walter is admitted to a psychiatric hospital, the doctors determine that Dees is no longer able to handle her grandson, and will not release him to her until she agrees to place him in long-term residential care. After a challenging search, Walter is accepted at an appropriate facility and thrives during his year there. However, when treatment is completed, social workers determine that Dees is too frail to care for him, and Walter is placed in a foster home. Walters aunts and uncles are unable to take him in, possibly because they feel unqualified to deal with his often threatening and troubled behavior.

==Awards and nominations== Best Documentary Short Subject
* 2001 Winner, Heartland Film Festival, Crystal Heart Award
* 2001 Winner, San Francisco International Film Festival, Golden Gate Award
* 2001 Winner, Washington D.C Oscar Winning Movie

==References==
 

==External links==
*   - including acceptance speech
*  
*  
*   Oscar Winning

 

 
 
 
 
 
 
 
 
 

 