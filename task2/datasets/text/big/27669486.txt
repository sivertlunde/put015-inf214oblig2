The Legend of Frenchie King
{{Infobox film
| name           = The Legend of Frenchie King
| image          = Les Petroleuses poster.jpg
| caption        = French theatrical release poster
| director       = Christian-Jaque
| producer       = Francis Cosne Raymond Erger
| writer         = Marie-Ange Aniès Daniel Boulanger Clément Bywood Eduardo Manzanos Brochero Jean Nemours
| starring       = Brigitte Bardot Claudia Cardinale
| music          = Christian Gaubert
| cinematography = Henri Persin
| editing        = Nicole Gauduchon
| distributor    = gross = 2,234,479 admissions (France)  released        = 16 December 1971
| runtime        = 94 minutes
| country        = France
| language       = French
|}} western comedy film directed by Christian-Jaque and starring Brigitte Bardot and Claudia Cardinale.

==Cast==
* Brigitte Bardot as Louise a.k.a. Frenchie King
* Claudia Cardinale as Marie Sarrazin
* Michael J. Pollard as Sheriff
* Patty Shepard as Petite Pluie
* Emma Cohen as Virginie
* Térèsa Cimpera as Caroline Oscar Davis as Mathieu
* Georges Beller as Marc
* Patrick Préjan as Luc
* Rocardo Salvino as Jean
* Valéry Inkijnoff as Spitting Bull
* Micheline Presle as Aunt Amelie
* Denise Provence as Mlle. Letellier
* Leroy Hayns as Marquis
* Jacques Jouanneau as M. Letellier
* Raoul Delfossé as Le Cornac
* France Dougnac as Elisabeth 

==Plot==
In Bougival Junction, New Mexico in 1880 the Francophone town is led by Marie Sarrazin. A new family arrives, calling themselves the Millers, but in fact they are the daughters of the hanged outlaw Frenchie King and his eldest daughter Louise seeks to keep her fathers name alive by donning mens clothing and continuing his criminal ways. Louise and Maria fight but when they are both jailed they team up to take revenge on the towns men. 

==Reception==
The film received generally negative reviews. Bardots performance in particular was criticised by Jean Loup Passek, who noted how uncomfortable she seemed in the films outdoors action setting.  Writing in Variety (magazine)|Variety Gene Moskowitz dismissed the film as "predictable, naive and gauche" whilst Tom Milne called it "drearily unfunny". 

==See also==
*List of Spaghetti Western films

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 