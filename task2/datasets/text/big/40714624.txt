Strugglers: The Reality Behind
{{Infobox film
| name           = Strugglers: The Reality Behind
| image          = Strugglers 2013 Poster.jpg
| image_size     = 300px
| border         = 
| alt            = 
| caption        = Theatrical Release Poster
| film name      = 
| director       = Anil Bajoria Balai Biswas
| producer       =  Jayanta Kumar Gupta, Uday Shankar Verma, Deepak More
| writer         =  Anil Bajoria
| screenplay     = Balai Biswas
| story          = Anil Bajoria
| based on       =  
| narrator       =  See below
| music          = Silajit Majumder
| cinematography = Indranil Sarkar
| editing        =  Ribhu Bhowmick
| studio         = VMG Motion Pictures Private Limited
| distributor    =  
| released       =  
| runtime        = 135 minutes
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}
 Bengali Suspense Thriller Film directed by Anil Bajoria and Balai Biswas and produced under the banner of VMG Motion Pictures Private Limited   

== Cast ==
* Meghna Halder  
* Ratan Lakhmani
* Deep Maity
* Kamalika Chanda
* Faiz Khan
* Koushik Chakra
* Rupa Bhattacharya
* Shantilal Mukherjee
* Silajit Majumder
* Rupam Islam
* Anaya Bhattacharya
* Dhee Majumder
* Juhi Sengupta

== Soundtrack ==
{{Infobox album  
| Name       = Strugglers: The Reality Behind
| Type       = Soundtrack
| Artist     = Silajit Majumder
| Cover      = 
| Alt        = 
| Released   = 2013
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Universal Music India
| Producer   = 
| Last album = Chupkatha (2012)
| This album = Strugglers: The Reality Behind (2013)
| Next album = 
}}

Silajit Majumder composed the film score for Strugglers: The Reality Behind.   

=== Track listing ===
{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Singer(s)
| total_length    = 21:49
| writing_credits = no
| lyrics_credits  = no
| music_credits   = no
| title1          = Abbulish
| extra1          = Silajit Majumder, Ananya Bhattacharya, Dhee Majumder
| length1         = 4:21
| title2          = Andhokar
| extra2          = Rupam Islam
| length2         = 3:31
| title3          = Dhumki
| extra3          = Ananya Bhattacharya
| length3         = 4:11
| title4          = Rupkatha (Acoustic)
| extra4          = Anupam Roy
| length4         = 2:58
| title5          = Rupkatha (Unplugged)
| extra5          = Ananya Bhattacharya
| length5         = 3:18
| title6          = Andhokar (Mix Version)
| extra6          = Silajit Majumder
| length6         = 3:30
}}

== References ==
 