Sports Day (film)
{{Infobox film
| name           = Sports Day (alternative title, The Colonels Cup)
| image          =
| image_size     =
| caption        =
| director       = Francis Searle
| producer       = Mary Field and Bruce Woolf for GBI - Gaumont British Instruction, School programmes and educational films 
| writer         = Francis Searle  
| starring       = Peter Jeffrey Roy Russell Jean Simmons
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
}}

Sports Day is a 1945 short film directed by Francis Searle for Gaumont British Instruction.   It featured an early appearance of Jean Simmons.
 
==Plot==
A schoolboy almost misses the school sports day when he is wrongly punished for cruelty to a dog.

==Cast==

*Peggy    -    Jean Simmons
*Tom      -    Peter Jeffrey
*Col.House   -   Roy Russell
*Headmaster -    Ernest Borrow
*Bill     -    David Anthony

==Notes==
 MoI -  we made films like Citizens Advice Bureau, and  Sam Pepys joins the Navy, Hospital Nurse. " In the interview  he describes  how he came to work with Jean Simmons.
 Merton Park with Jean Simmons, who I cast in a short picture. Mary Field and Bruce Woolf were the bosses at GBI. (Gaumont British Instruction). Mary Field did childrens films and she had a film she offered to me (Sports Day, 1945). Aida Foster, who was a big agent for juveniles, set up an audition and Jean, aged about fourteen, came on as bright as a button; she had learned the part and that was it. I didn’t bother looking at any of the others.  

==References==
 

 


 