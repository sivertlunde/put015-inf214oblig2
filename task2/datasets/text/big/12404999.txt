The Lonely Trail
{{Infobox film
| name           = The Lonely Trail
| image          = Poster of The Lonely Trail.jpg
| caption        = Theatrical poster
| director       = Joseph Kane
| producer       = Nat Levine
| writer         = {{plainlist|
*Bernard McConville
*Jack Natteford
}}
| starring       = {{plainlist|
*John Wayne
*Ann Rutherford
}}
| cinematography = William Nobles
| editing        = Robert Jahns]
| released       =  
| runtime        = 56 minutes
| country        = United States
| language       = English
}}
 Western film starring John Wayne and Ann Rutherford.

==Plot==
Though he fought for the North in the Civil War, John is asked by the Governor of Texas to get rid of some troublesome carpetbaggers. He enlists the help of Holden before learning that Holden too is plundering the local folk.

==Cast==
* John Wayne as Captain John Ashley
* Ann Rutherford as Virginia Terry
* Cy Kendall as Adjutant General Benedict Holden
* Bob Kortman as Captain Hays
* Fred "Snowflake" Toones as Snowflake
* Sam Flint as Governor of Texas Dennis Moore as Dick Terry
* Jim Toney as Trooper Jed Calicutt
* Etta McDaniel as Mammy
* Yakima Canutt as Trooper Bull Horrell
* Lloyd Ingraham as Tucker (bookkeeper)
* James A. Marcus as Mayor Bob Burns as Rancher Jeff Pruitt
* Rodney Hildebrand as Cavalry captain
* Eugene Jackson as Harmonica player/dancer

==See also==
* John Wayne filmography

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 