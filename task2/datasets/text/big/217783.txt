The Sterile Cuckoo
 
{{Infobox film
| name           = The Sterile Cuckoo
| image          = File:Film_Poster_for_The_Sterile_Cuckoo.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Alan J. Pakula
| producer       = David Lange Alan J. Pakula
| writer         = Screenplay: Alvin Sargent
| based on       =  
| starring       = Liza Minnelli Wendell Burton Tim McIntire
| music          = Fred Karlin
| cinematography = Milton R. Krasner
| editing        = Sam OSteen
| studio         = Boardwalk Productions
| distributor    = Paramount Pictures 
| released       =  
| runtime        = 107 min.
| country        = United States
| language       = English
| gross          = $13,982,357   
}}
The Sterile Cuckoo (1969 in film|1969), released in the UK as Pookie, is a  theatrical release feature film released by Paramount Pictures that tells the story of an eccentric young couple whose relationship deepens despite their differences and inadequacies, and stars Liza Minnelli, Wendell Burton, and Tim McIntire.    
 1965 novel John Nichols, and directed by Alan J. Pakula, in his directing debut. 
 Hamilton College in Clinton, Oneida County, New York|Clinton, New York. Some of it was filmed in Sylvan Beach, New York, including the Sylvan Beach Union Chapel.
 Best Actress Best Music, Come Saturday Morning").

==Plot==
Mary Ann "Pookie" Adams (Minnelli) is an oddball, quirky teenager who meets the quiet, reserved Jerry Payne (Burton) while waiting for a bus heading to their colleges, which are near each other, where they have enrolled as freshmen. Jerry immediately sees that Pookie is different, even strange. She lies to a nun on the bus so the nun will switch seats with her.

Jerry is beginning to settle into college life with his roommate (McIntire) when the aggressive Pookie shows up one Saturday morning out of the blue. They spend much time together over the weekend, and before long are seeing each other regularly. 

Jerry falls in love with Pookie, but soon their different personality types pull them apart. After having sex, Pookie tells Jerry she might be pregnant. After the pregnancy scare is over, Jerry wants to spend spring break alone to catch up on his studies. Pookie pleads to stay with him, and he relents.

A week alone with the needy and at times unstable Pookie makes Jerry realize more that they need time apart. Discovering later that she has left college, Jerry finds her in the same boarding house where she had stayed on the first day she came to visit. He puts her on a bus for home, and the young lovers part ways for good.

==Cast==
*Liza Minnelli as Mary Ann Pookie Adams
*Wendell Burton as Jerry Payne
*Tim McIntire as Charlie Schumacher

==Reception== review aggregate 13th highest grossing film of 1969.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 