The Seine Meets Paris
 
{{Infobox Film
| name           = The Seine Meets Paris
| image          = 
| caption        = 
| director       = Joris Ivens
| producer       = 
| writer         = Jacques Prévert
| narrator       = Serge Reggiani
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1957
| runtime        = 30 minutes
| country        = France
| language       = French
| budget         = 
}}
The Seine Meets Paris ( ) is a 1957 French short documentary film directed by Joris Ivens from a screenplay by Jacques Prévert. Told from the perspective of a boat trip through the city, it features scenes of daily life along the river. The film won the short film Palme dOr at the 1958 Cannes Film Festival.

==External links==
* 
* 
*  (French)

 

 
 
 
 
 
 
 
 
 
 
 


 
 