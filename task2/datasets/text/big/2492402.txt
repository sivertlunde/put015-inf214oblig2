Waisa Bhi Hota Hai Part II
{{Infobox film|
  name           = Waisa Bhi Hota Hai Part II |
  image          =  Waisa_Bhi_Hota_Hai.jpg|
  caption        = DVD cover of Waisa Bhi Hota Hai Part II |
  writer         = Shashanka Ghosh  Javed Ahmed  Junaid Memon |
  starring       = Arshad Warsi Prashant Narayanan Sandhya Mridul Anant Jog|
  director       = Shashanka Ghosh |
  producer       = Rahul Misra Sameer Gupta |
  distributor    = Impact Films |
  released       = 14 November 2003 | Hindi |
  music          = Vishal-Shekhar Shibani Kashyap Saibal Basu Abhinav Dhar|
  cinematography = Andre Menezes|
  editing        = Neerav Ghosh|
  runtime        = 138 minutes |
  country        = India|
  awards         = |
  budget         =
}}

Waisa Bhi Hota Hai Part II ( ,  ) is a 2003 Indian movie starring Arshad Warsi. Shashanka Ghosh, the creative driver behind the launch of MTV and Channel V in India, marks his debut as a director with this film.  Running in many parallel threads, it is in equal parts a comedy, satire, crime, and a Hindi masala film. The film is most famous for its songs "Allah ke Bande" performed by Kailash Kher and "Sajna Aa Bhi Jaa" performed by Shibani Kashyap.

== Plot ==

Puneet Sayal ( . Till the said dream materialises however, hes living in Bombay with his girlfriend Agni (Sandhya Mridul).

Things go asunder one day when he finds out his brothers been shot dead &mdash; a brother hes hardly acknowledged in the past. Agni finds out and, following an argument, throws him out of the house. Puneet goes on a drinking spree that leads him to a park bench where he witnesses someone being shot. He doesnt know it yet, but this is the end of his life as he knows it.

He saves the injured man &mdash; a gangster called Vishnu (Prashant Narayanan) &mdash; and the act lands him squarely in the middle of Bombays famed gang wars.

The war between ganglords Ganpat (Anant Jog) and Gangu (Pratima Kazmi) is a second thread in the film. Ganpat is the dominant kingpin, and Gangu is the perpetual second-in-place whos never given up her dreams of displacing Ganpat as top don.

== Cast ==

* Arshad Warsi as Puneet Sayal
* Prashant Narayanan as Vishnu
* Sandhya Mridul as Inspector Agni Sinha
* Anant Jog as Ganpat
* Pratima Kazmi as Gangu Tai
* Suchitra Pillai as Shalu Manini De as Sumi
* Skand Mishra as Chandu

== Reception ==
The film did poorly at the box office in most parts of India. However, a small audience in urban India did find it interesting and have compared it with the likes of other cult classics.  Arshad Warsi and Prashant Narayanan were highly appreciated for their performances.  Singer Kailash Kher, however, won the most plaudits for his brilliant voice in the chart-topping song "Allah ke Bande".

== Trivia ==
*  Waisa Bhi Hota Hai Part II is not a sequel to any other film. Part I and Part III are in the same movie. Part I runs for about 9 minutes before Part II starts, while Part III runs for less than a minute before the end credits roll.
* In the credits, the filmmakers acknowledges the Coen Brothers, Quentin Tarantino, Ram Gopal Varma, Takeshi Kitano, Mahesh Manjrekar and Ramesh Sippy as inspiration.
* Singers Kailash Kher, Shibani Kashyap and Rabbi Shergill make a cameo in the film and sing their songs on-screen.  Maria Goretti make special appearances.
* The barista trio in the film comprises Channel V VJs Ranvir Shorey and Shruti Seth, and John Owen.

== Quotes ==
*(Translated from Hindi): "My wife is in the police, my friends a shooter... how did I become a writer?"

== Awards == Best Male Playback Singer award at the 2004 Star Screen Awards. The film was nominated for Best Performance in a Negative Role (Pratima Kazmi), Best Dialogues, Best Editing, Best Lyrics and Best Screenplay at the Star Screen Awards but failed to win any.

== Soundtrack ==
{{Infobox album|
  Name        = Waisa Bhi Hota Hai Part II |
  Type        = soundtrack |
  Artist      = Vishal-Shekhar, Shibani Kashyap, Saibal Basu, Abhinav Dhar |
  Background  = gainsboro |
  Released    = October 2003|
  Recorded    = Unknown |
  Genre       = Film soundtrack |
  Length      = |
  Label       = Zee Records |
  Producer    = |
  Reviews     = |
}}

The soundtrack features seven songs by composers including Vishal-Shekhar, Shibani Kashyap, Saibal Basu and Abhinav Dhar and written by Sadaquat Hussain, Vishal Dadlani, Abhinav Dhar and Virag. The soundtrack listing is given below. 

# "Aao Aa Jao Aa Bhi Jao"  (Sunidhi Chauhan)  - 4:48
# "Allah Ke Bande"  (Kailash Kher)  - 4:06
# "Laundiya Ke Pallu Mein"  (Rabbi Shergill)  - 4:51
# "My Name Is Gurdeep"  (Bali Brahmbhatt)  - 3:53
# "Saajna Aa Bhi Ja"  (Shibani Kashyap)  - 4:26
# "Tum Bas Tum"  (Shibani Kashyap)  - 3:57
# "Prem Dunk"  (Shibani Kashyap)  -  3:40

==See also==
* Indian Mafia
* Cult Movies

==Notes==
 

== External links ==
*  
*  
*  
 

 
 
 