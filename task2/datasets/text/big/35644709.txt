Camille Rewinds
 
{{Infobox film
| name           = Camille Rewinds
| image          = Camille redouble (2012 film).jpg
| caption        = Film poster
| director       = Noémie Lvovsky
| producer       = Philippe Carcassonne Jean-Louis Livi
| writer         = Noémie Lvovsky Florence Seyvos Pierre-Olivier Mattei  Maud Ameline
| starring       = Noémie Lvovsky Yolande Moreau
| music          = Gaetan Roussel   Joseph Dahan
| cinematography = Jean-Marc Fabre
| editing        = Annette Dutertre   Michel Klochendler Gaumont
| released       =  
| runtime        = 120 minutes
| country        = France
| language       = French
| budget         = $6.7 million   
| gross          = $7 million
}}

Camille Rewinds ( )  is a 2012 French drama film directed by Noémie Lvovsky. The film was screened in the Directors Fortnight section at the 2012 Cannes Film Festival   where it won the Prix SACD.  Moreau received a Magritte Award for Best Supporting Actress for her role. 

==Plot==
On her way to a party Camille consults a quirky clockmaker because she needs to have her watch fixed. The watch has sentimental value for her since she has received it as a birthday present when she became 16 years old. Next morning Camille realises it is the year 1985 and she is again a teenager.

==Cast==
* Noémie Lvovsky as Camille Vaillant
* Jean-Pierre Léaud as the clockmaker
* Yolande Moreau as Camilles mother
* Denis Podalydès as Alphonse
* Samir Guesmi as Éric
* Michel Vuillermoz as Camilles father
* Vincent Lacoste as Vincent
* Judith Chemla as Josepha
* India Hair as Alice
* Julia Faure as Louise
* Micha Lescot as the director
* Anne Alvaro as the adoption teacher
* Mathieu Amalric as the French teacher
* Riad Sattouf as the director

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 