Confessions from a Holiday Camp
{{Infobox film
| name           = Confessions from a Holiday Camp
| image          = Confessions from a holiday camp.jpg
| caption        = Promotional poster 
| director       = Norman Cohen Greg Smith Michael Klinger Christopher Wood Sheila White
| music          = Ed Welch
| cinematography = Ken Hodges
| editing        = Geoffrey Foot Columbia Pictures Corporation
| released       =  
| runtime        = 88 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
Confessions from a Holiday Camp is a 1977 British comedy film. It is the last film in the series which began with Confessions of a Window Cleaner. The film was released in North America in 1978 under the title Confessions of a Summer Camp Counsellor. 

==Plot== prison officer, takes over the camp and is determined to install discipline into the staff. He is on the verge of dismissing Timmy and Sidney; however, Sidneys suggestion of organising a beauty contest changes his mind.

Producer Michael Klinger wasnt happy with the script, noting a number of problems that he felt detracted from the quality that set the series apart from its imitators. 

==Soundtrack==
The title track to the film was called Give Me England and was performed by scrumpy and western band, The Wurzels arranged and conducted by Ed Welch and produced by Bob Barratt.  It was released on 45 by EMI records Ltd (EMI 2677).
They released an album of the same name in 1977.

==Further proposed films==
Although Holiday Camp would turn out to be the last film in the series, a fifth and a sixth film, Confessions of a Plumbers Mate and Confessions of a Private Soldier, had been planned in 1977. Filming was set to begin on Plumbers Mate at the end of February 1978. Robin Askwith even expressed a desire to direct Private Soldier, but neither film materialised. In November 1977 the studio canceled plans for future films.       Columbia Pictures president David Begelman, who had been very supportive of the British film industry and who had Green-light|green-lit the first Confessions film, had been implicated in a cheque-forging scandal and either quit or was fired. His successor had no interest in financing low-budget, profitable British films. 
 Michael Klinger rejected a script based on Confessions from a Haunted House . Plans to shoot a made-for-video Confessions film in the 1980s also came to nothing,  as did a proposed 1992 film, "Confessions of a Squaddie", which was proposed with action due to take place in post-Gulf War Kuwait.

==Cast==
Cast overview, first billed only:
 
*Robin Askwith ....  Timmy Lea 
*Antony Booth ....  Sidney Noggett 
*Bill Maynard ....  Mr. Lea 
*Doris Hare ....  Mrs. Lea  Sheila White ....  Rosie Noggett 
*Linda Hayden ....  Brigitte 
*Lance Percival ....  Lionel 
*John Junkin ....  Mr. Whitemonk 
*Liz Fraser ....  Mrs. Antonia Whitemonk 
*Colin Crompton ....  Roughage 
*Nicola Blackman ....  Blackbird 
*Nicholas Bond-Owen ....  Kevin (as Nicholas Owen) 
*Caroline Ellis ....  Gladys
*Sue Upton .... Renee
*Penny Meredith ...  Married Woman 
*Mike Savage ....  Kevins Dad 
*Janet Edis ....  Kevins Mum 
* Deborah Brayshaw ...  Go Cart Girl 
* Kim Hardy ...  Announcer 
* David Auker ...  Alberto Smarmi 
* John Bryant ...  Young Man 
* Charlie Stewart ...  Piper  Carrie Jones ...  Bikini Girl 
* Julia Bond ...  Bikini Girl 
* Betty Hare ...  Mourner 
* Winifred Braemar ...  Mourner 
* Margo Field ...  Mrs. Dimwiddy 
* Marianne Stone ...  Waitress 
* Leonard Woodrow ...  Chaplain 
* Lauri Lupino Lane ...  Mayor 
* Ingrid Bower ...  Holiday Maker 
* Robert Booth ...  Holiday Maker 
* Michael Segal ...  Holiday Maker 
* Matt Kilroy ...  Chauffeur
 

==References==
 

==Bibliography==
*  

==External links==
*  
*   BFI Film & TV Database

 

 
 
 
 
 
 
 
 
 
 