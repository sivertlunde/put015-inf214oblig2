Step Down to Terror
{{Infobox film
| name            = Step Down to Terror
| image           = Step Down to Terror poster.jpg
| caption         = Theatrical release poster
| director        = Harry Keller
| producer        = Joseph Gershenson
| writer          = Mel Dinelli Czenzi Ormonde Sy Gomberg
| based on = story Uncle Charlie by Gordon McDonnell
| starring        = Colleen Miller   Charles Drake   Rod Taylor  Josephine Hutchinson
| music           = 
| cinematography  = Russell Metty
| editing         = 
| studio = Universal-International
| distributor     = Universal-International
| released        = 1958
| runtime         = 
| country         = United States
| language        = English
| budget          = 
| gross           = 
}}
Step Down to Terror (also known as The Silent Stranger) is a 1958 American film directed by Harry Keller. It is a remake of the 1943 Alfred Hitchcock film Shadow of a Doubt. 

==Plot==
Johnny Williams (Drake), a psychotic serial killer who returns to his hometown to visit his mother (Hutchinson) and widowed sister-in-law Helen (Miller), both of whom are ignorant of his criminal past. Johnny hopes to settle down and start life anew, but Helen, her suspicions aroused by visiting detective Mike Randall (Taylor), discovers the truth about her beloved brother-in-law. Failing to talk Helen out of turning him in, Johnny methodically plots her murder. When all his plans fail he drags Helen into his car and drives off with her. Knowing Johnny is going to kill her, Helen grabs the keys, and he is forced to swerve to avoid hitting a boy riding his bike, and is killed in the resulting accident. At the memorial service, as Johnny is lauded as a model citizen, only Helen and Mike know the real truth.

==Cast==
*Colleen Miller as Helen Walters
*Charles Drake as Johnny Williams Walters
*Rod Taylor as Mike Randall
*Josephine Hutchinson as Mrs. Sarah Walters
*Jocelyn Brando as Lily Kirby
*Alan Dexter as Roy the photographer
*Ricky Kelman as Doug Walters

==Production==
At one stage Ross Hunter was going to produce and Donna Reed was going to star. Kennedy, Field Peyton Favorites; Big Global Group to Honor Disney
Schallert, Edwin. Los Angeles Times (1923-Current File)   03 May 1957: 25.  
==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 