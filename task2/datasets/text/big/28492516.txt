Cyclops (1982 film)
{{Infobox film
| name           = Cyclops
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Antun Vrdoljak
| producer       = 
| screenplay     = Antun Vrdoljak
| based on       = Cyclops by  Ranko Marinković
| narrator       = 
| starring       = Frano Lasić
| music          = Miljenko Prohaska
| cinematography = Tomislav Pinter
| editing        = Damir German
| studio         = 
| distributor    = 
| released       = 1982
| runtime        = 
| country        = Yugoslavia
| language       = Croatian
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Croatian film directed by Antun Vrdoljak, based on the 1965 novel of the same title by Ranko Marinković. 

==Cast==
*Frano Lasić as Melkior Tresić
*Ljuba Tadić as Maestro
*Rade Šerbedžija as Ugo
*Mira Furlan as Enka
*Marija Baxa as Vivijana
*Mustafa Nadarević as Don Fernando
*Relja Bašić
*Boris Dvornik
*Ivo Gregurević

==References==
 

==External links==
* 
*  at Filmski-Programi.hr  

 
 
 
 
 
 


 