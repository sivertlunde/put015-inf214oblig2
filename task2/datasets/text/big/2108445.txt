Porky's II: The Next Day
{{Infobox film
| name           = Porkys II: The Next Day 
| image          = PorkysII.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Bob Clark 
| producer       = Don Carmody Bob Clark 
| writer         = Roger Swaybill Alan Ormsby Bob Clark
| starring = {{plainlist|
* Dan Monahan
* Wyatt Knight
* Mark Herrier Roger Wilson Cyril OReilly
* Tony Ganios
* Kaki Hunter
* Scott Colomby
* Nancy Parsons Joseph Running Fox
* Eric Christmas
* Bill Wiley Edward Winter
* Ilse Earl
* Cisse Cameron
* Art Hindle
}} 
| music          = Carl Zittrer 
| cinematography = Reginald H. Morris 
| editing        = Stan Cole  Astral Films 
| distributor    = 20th Century Fox 
| released       =  
| runtime        = 98 minutes
| country        = United States Canada
| language       = English 
| budget         = 
| gross          = $33,759,266 (North America) 
}}

Porkys II: The Next Day is the 1983 sequel to the 1982 film Porkys. The film is co-written and directed by Bob Clark. Unlike the previous film, Porky himself does not appear.

==Plot== Romeo opposite a white girl Juliet (Wendy).  Reverend Flavel, who has previously exposed his bigoted nature, welcomes the Klans support of his movement.  The Angel Beach gang then begins plotting their retaliation and revenge against Reverend Flavel, Commissioner Gebhardt, the rest of the county commissioners, and the Klan.

The teens discover that the county commissioners, who are publicly riding the political wave of decency and morality, enjoy secretly watching pornographic movies in the basement of the courthouse.  So, the Angel Beach gang takes a tape recorder with a microphone to the basement of the courthouse while the county commissioners are watching pornographic movies, aim the microphone at the ventilator window, and record the politicos loud and crude commentary on the events in the porno films, including their irreverent mocking of Reverend Flavel (And the fact that Reverend Flavel provided some of the pornography).

After his success in shutting down the Angel Beach High Shakespeare Festival, Reverend Flavel convenes an outdoor revival meeting at Angel Beach High to commemorate his success, and the county commissioners attend and stand on stage with Reverend Flavel.  In retaliation for an attack and attempt by the Klan members to shave the head of the groups Seminole friend, the gang lures the Klan members (who are on their way to Reverend Flavels revival meeting) into the school gym where the gang along with a gym-ful of Seminole Indians "persuade" the Klan members to submit to having their heads shaved.  The Angel Beach gang and the Seminole Indians then make the Klan members strip naked and march over to Reverend Flavels revival meeting where they force the hapless and humiliated Klan members into the revival meeting.  The sight of naked men shocks the crowd, and in his typical opportunistic fashion, Reverend Flavel decries the naked Klan members as being "The Spawn of Satan."  During Reverend Flavels prognostications, the gang commandeers the public address system and plays the recording of the county commissioners raucous commentary on the pornographic movie that they watched in the basement of the courthouse only days before.  A comment on the recording about Reverend Flavel having donated the pornographic film that the county commissioners were watching ends up being Reverend Flavels undoing as he and the county commissioners stand before the crowd completely discredited, disgraced, and exposed as selfish hypocrites.

Meanwhile, Wendy accepts an out-of-town dinner date in Miami with Commissioner Gebhardt, who intends to seduce her. However, rather than showing up in her normal modest clothing, Wendy shows up at the formal restaurant wearing a deliberately showy, vulgar outfit with her breasts artificially padded with a container to an enormous size. She speaks in an extremely loud voice, constantly blurting out Gebhardts name and status as a county commissioner of Seward County who will shortly be standing for re-election and alleging that she and Gebhardt became involved while she was a Brownie in Gebhardt wifes girl scout troop and that she is now pregnant with his child.  With the help of liquefied corn-and-pea soup concealed in the container that padded her bust, Wendy pretends to throw up in one of the restaurants decorative fountains to the horror of the maître d and the disgust of the restaurants upscale patrons.  Pee-Wee snaps a picture of the disgraced Commissioner Gebhardt before he, Wendy, and Steve depart in a blaze of glory back to the rally at Angel Beach High to join the triumphant celebration of the defeat of the persons responsible for the cancellation of their Shakespeare Festival, which is promptly reinstated.

==Cast==
* Dan Monahan as Edward "Pee Wee" Morris
* Wyatt Knight as Tommy Turner
* Mark Herrier as Billy McCarty
* Tony Ganios as Anthony "Meat" Tuperello
* Scott Colomby as Brian Schwartz Cyril OReilly as Tim Cavanaugh
* Kaki Hunter as Wendy Williams
* Ilse Earl as Mrs. Morris
* Eric Christmas as Mr. Carter
* Bill Wiley as Reverend Bubba Flavel
* Nancy Parsons as Ms. Beulah Balbricker
* Joseph Runningfox as John Henry Roger Wilson as Mickey Jarvis
* Art Hindle as Officer Ted Jarvis
* Bill Hindman as Coach Goodenough
* Mal Jones as Mayor Abernathy
* Richard Liberty as Commissioner Couch
* Fred Buch as Commissioner Hurley Edward Winter as Commissioner Bob Gebhardt
* Will Knickerbocker as Klan #1

==Production==
Greynolds Park in North Miami, Florida stood in for the Everglades and a plant nursery on Old Cutler Road was turned into a cemetery for the graveyard scenes. 

==Reception==

===Box office===
The films gross receipts were considerably lower than the first Porkys film. While Porkys grossed $105 million in the North American market, Porkys II: The Next Day only took in $33,759,266.  

===Critical response===
   At the Movies. They both hated the film, they called it "totally beneath contempt, there is nothing this movie wont stoop to to get a joke. Its sick, anti-semitic, dishonest and corrupt from one end to the other", and they were particularly offended by the comedic and lighthearted treatment of the Ku Klux Klan in this film. 
The movie was nominated for a Stinkers Bad Movie Awards for Worst Picture. 

==References==
{{Reflist|refs=
    
    
}}

==External links==
 
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 