Dark Shadows (film)
 
{{Infobox film
| name           = Dark Shadows
| alt            =
| image          = Dark Shadows 2012 Poster.jpg
| caption        = Theatrical release poster
| director       = Tim Burton
| producer       = {{plainlist|
* Richard D. Zanuck
* Graham King
* Johnny Depp
* Christi Dembrowski David Kennedy}}
| screenplay     = Seth Grahame-Smith
| story          = {{plainlist|
* John August
* Seth Grahame-Smith}}
| based on       =  
| starring       = {{plainlist|
* Johnny Depp
* Michelle Pfeiffer
* Helena Bonham Carter
* Eva Green
* Jackie Earle Haley
* Jonny Lee Miller
* Chloë Grace Moretz
* Bella Heathcote}}
| music          = Danny Elfman
| cinematography = Bruno Delbonnel
| editing        = Chris Lebenzon
| studio         = {{plainlist|
* Village Roadshow Pictures
* Infinitum Nihil GK Films
* The Zanuck Company}}
| distributor    = {{plainlist| Warner Bros. Pictures
* Roadshow Entertainment  }}
| released       =  
| runtime        = 113 minutes   
| country        = United States
| language       = English
| budget         = $150 million 
| gross          = $245.5 million 
}}
 horror comedy gothic television Angelique Bouchard, witch who was responsible for transforming Collins into a vampire). Michelle Pfeiffer stars as Collins cousin, Elizabeth Collins Stoddard, the reclusive matriarch of the Collins family.   The film had a limited release on May 10, 2012,  and was officially released the following day in the United States. 

The film performed disappointingly at the US box office, but was well-received elsewhere. Critics praised its visual style and consistent humor, but felt it lacked a focused or substantial plot and developed characters.  The film was produced by Richard D. Zanuck, who died two months after its release. It featured the final appearance of original series actor Jonathan Frid, who died shortly before its release.

==Plot==
In 1760; the Collins family moves from Liverpool, England to Maine and establishes a fishing town Collinsport, building their estate Collinwood. Sixteen years later, the son Barnabas Collins falls in love with a young woman Josette Du Pres, scorning a maid named Angelique Brouchard, who had fallen in love with him. Turning to black magic she curses the Collins family; first killing Barnabas parents in an assumed accident, then by enchanting Josette to leap to her death off a cliff. In a fit of grief, Barnabas attempts to leap to his own death, but that fails as well, as Angelique has cursed him the immortal life of a vampire. When he still rejects her advances, she turns the town against him and he is buried alive in a coffin.
 hypnotizing the groundskeeper, Willie and introducing himself to the family. Elizabeth believes him to be a conman, until he reveals hidden riches within the house, where Elizabeth thought the family to be destitute and she realizes who and what he is. She asks to keep it a secret, passing Barnabas off as a distant cousin, but eventually Julia discovers his identity through hypnosis.

Barnabas is instantly smitten with Victoria when he meets her, as she resembles Josette and seeks out Carolyns advice on courting her. She suggests they hold a ball; with musician Alice Cooper as the headliner. Angelique; now an immortal witch herself visits Barnabas and threatens him if he goes into competition against her, nevertheless, he reopens the Collins family business and uses hypnosis to steal several of her fishermen. She tries to buy him off, but after a particularly athletic moment of lust, he rejects her offer. At the ball, Barnabas catches Roger stealing from guests and having sex with the help in the coat room while David stands guard, Angelique makes a grand appearance in order to seduce Barnabas, but when she catches him kissing Victoria she becomes embittered. After catching him trying to break into the secret room, Barnabas gives Roger a choice; to stay and be an exemplary father to David, or leave with enough money to live his life alone. Roger chooses the latter, to Davids heartbreak. His identity is then revealed to the rest of the family and Victoria when he saves David from a falling disco ball. After discovering Julia; who had suggested blood transfusions to turn human again, using his blood to instead make herself immortal, he kills her by draining her of her blood and Willie helps him dump her body into the bay.

Angelique summons Barnabas again to her office, but when he rejects her offer again, she traps him in another coffin, this time burying him in a mausoleum, shortly after he is found and freed by David. Meanwhile Angelique destroys the Collins factory and implicates Barnabas in several murders, turning the townsfolk against the Collins family. They go to the mansion and meet with the family and Barnabas who then reveals her true nature to the public. Carolyn reveals she is a werewolf thanks to Angelique, and Angeliques assault on the family causes a massive fire to break out. With the help of his mom, David manages to deliver a blow to Angelique that leaves her hanging from the chandelier, which then falls to the ground, mortally wounding her. She literally offers her heart to Barnabas again, but he refuses it and it turns to dust. Davids mother tells him that Victoria is headed toward the cliffs; a final curse by Angelique. Barnabas narrowly misses her as she falls and he leaps after her, biting her so she can survive the fall as a vampire. She awakens, and the two kiss passionately on the rocky shore. David asks Elizabeth what the Collins family will do now, and Elizabeth says they will "rebuild and carry on."

Meanwhile, in the bay. The assumed dead Julia suddenly opens her eyes to her new immortal life as well.

==Cast==
* Johnny Depp as Barnabas Collins, an 18th-century vampire who awakens to the 20th century.  Justin Tracy appears briefly in the film as a young Barnabas. While an empathetic character aware of his sinister nature, Barnabas retains a vicious streak, never forgiving and relentless in the kill. His only loyalties to his family aside, he is a well mannered man consistently trapped in the mindset of an 18th-century Englishman.
* Michelle Pfeiffer as Elizabeth Collins Stoddard, the family matriarch.  Stern and strict, but loyal and devoted to her family, Elizabeth cares for every member of the household and tends to help them through their personal trials. To "endure" as they always have, she isnt afraid to stand up for herself.
* Helena Bonham Carter as Julia Hoffman|Dr. Julia Hoffman, the familys live-in psychiatrist, hired to deal with David and his belief in ghosts.  Somewhat vain and losing to her aging, she takes it upon herself to receive transfusions from Barnabas (under the guise of trying to cure him). She is often drunk or taking pills. Barnabas woos her into having a crush on him, often taking advantage of his naive nature without regarding the possible consequence of betraying him. Angelique Bouchard, a vengeful witch who plots a vendetta against Barnabas and his family.  She wears a constantly false smile that resembles a feature like a porcelain doll and like glass, her face becomes cracked when damaged, revealing a possibly hollow interior. She had spent two centuries using her immortal nature to ruin the Collins family name and give herself more power, posing as five generations of the "Bouchard Women".
* Jackie Earle Haley as Willie Loomis, the manors caretaker.  He becomes Barnabas servant through hypnosis; he retains his own mindset, but follows his masters will instantly.
*  " brother.  Larcenous and greedy, Roger often takes advantage of what he can through theft, trickery or bribery. When Barnabas catches him snooping around for the secret vault, he offers Roger a choice, either stay at Collinwood and be an exemplary father to his son David, or leave with the money to live on. The selfish man chooses the latter, hurting David at his departure.
* Bella Heathcote as Victoria Winters, Davids governess and Barnabas love interest.  Heathcote also plays the role of Josette du Pres. Victoria and Maggie Evans roles in the original series were combined in the film, with Maggie choosing to adopt the name of Victoria after seeing a poster for winter sports in Victoria, British Columbia while on the train to Collinsport. As a child, Victoria was sent to a mental hospital by her parents for possessing the ability to see and speak to ghosts, only to escape and find refuge with the Collins family. She is from New York and has a very kind nature. She is mutually attracted to Barnabas, but recoils after discovering his true nature.
* Chloë Grace Moretz as Carolyn Stoddard, Elizabeths rebellious teenage daughter.  She offers Barnabas advice on love, music, and insight into the current era. She wants to run away to New York when she turns 16, but her mother constantly forbids it. She also turns out to be a werewolf, as she was bitten by a werewolf sent by Angelique when she was a baby. David Collins, Rogers "precocious" 10-year-old son, who can see his mothers ghost, who was killed by Angelique at sea. 
* Ray Shirley as Mrs. Johnson, the manors elderly maid. 
* Christopher Lee as Silas Clarney, a "king of the fishermen who spends a lot of time in the local pub, The Blue Whale."    
* Alice Cooper as himself. 
* Ivan Kaye as Joshua Collins, the father of Barnabas Collins. 
* Susanna Cappellaro as Naomi Collins, the mother of Barnabas Collins.  William Hope as Sheriff Bill of Collinsport
* Hannah Murray as Hippie Chick
* Guy Flanagan as Bearded Hippie San Diego Comic-Con 2011, it was also confirmed that four actors from the original series appear in the film. In June 2011, Jonathan Frid, Lara Parker, David Selby and Kathryn Leigh Scott all spent three days at Pinewood Studios to film cameo appearances. They all appeared as party guests during a ball held at Collinswood Manor.    Frid died in April 2012, making this his final film appearance.

==Production==
In July 2007, Warner Bros. acquired film rights for the gothic soap opera Dark Shadows from the estate of its creator Dan Curtis. Johnny Depp had a childhood obsession with Dark Shadows, calling it a "dream" to portray Barnabas Collins, and ended up persuading Burton to direct.  The projects development was delayed by the 2007–2008 Writers Guild of America strike. After the strike was resolved, Tim Burton was attached to direct the film.   By 2009, screenwriter John August was writing a screenplay for Dark Shadows.  In 2010, author and screenwriter Seth Grahame-Smith replaced August in writing the screenplay.  August did, however, receive story credit with Smith for his contribution to the film. Filming began in May 2011. It was filmed entirely in England, at both Pinewood Studios and on location.    Depp attempted to emulate the "rigidity" and "elegance" of Jonathan Frids original Barnabas Collins, but also drew inspiration from Max Schrecks performance in Nosferatu.   
 Harry Potter and the Half-Blood Prince&nbsp;— worked on the project.

==Music==

===Score===
{{Infobox album  
| Name        = Dark Shadows: Original Score
| Type        = Film
| Artist      = Danny Elfman
| Cover       =
| Released    = May 8, 2012
| Recorded    = 2011–2012
| Genre       = Orchestral
| Length      = 52:45
| Label       = WaterTower Music
| Producer    =
| Chronology  = Dark Shadows music
| Last album  =
| This album  = Dark Shadows: Original Score (2012)
| Next album  = Dark Shadows: Original Motion Picture Soundtrack (2012)
}}

The film was scored by long-time Burton collaborator Danny Elfman. An album featuring 21 tracks of compositions from the film by Elfman was released on May 8, 2012. 

====Track listing====
{{Track listing
| collapsed =
| headline = Dark Shadows: Original Score
| title1 = Dark Shadows Prologue
| note1 = Uncut
| length1 = 7:52
| title2 = Resurrection
| length2 = 2:54
| title3 = Vicki  Enters Collinwood
| length3 = 1:21
| title4 = Deadly Handshake
| length4 = 2:14
| title5 = Shadows (Reprise)
| length5 = 1:08
| title6 = Is It Her?
| length6 = 0:43
| title7 = Barnabas Comes Home
| length7 = 4:18
| title8 = Vickis Nightmare
| length8 = 1:26
| title9 = Hypno Music
| length9 = 0:47
| title10 = Killing Dr. Hoffman
| length10 = 1:14
| title11 = Dumping the Body
| length11 = 0:58
| title12 = Roger Departs
| length12 = 2:33
| title13 = Burn Baby Burn / In-Tombed
| length13 = 2:49
| title14 = Lava Lamp
| length14 = 2:17
| title15 = The Angry Mob
| length15 = 4:40
| title16 = House of Blood
| length16 = 3:38
| title17 = Final Confrontation
| length17 = 2:20
| title18 = Widows  Hill (Finale)
| length18 = 3:47
| title19 = The End?
| note19 = Uncut
| length19 = 2:42
| title20 = More the End?
| length20 = 1:55
| title21 = We Will End You!
| length21 = 1:09
}}

===Soundtrack===
{{Infobox album  
| Name        = Dark Shadows: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various artists
| Cover       =
| Released    = May 8, 2012
| Recorded    = 1966–2012
| Genre       = Progressive rock, psychedelic rock, hard rock, Pop music|pop, Rhythm and blues|R&B, orchestral
| Length      = 44:43 Sony Music
| Producer    = Various, Tim Burton
| Chronology  = Dark Shadows music
| Last album  = Dark Shadows: Original Score (2012)
| This album  = Dark Shadows: Original Motion Picture Soundtrack (2012)
| Next album  =
| Misc        =
}}
 rock and pop songs, Top of Season of Get It No More Ballad of Go All The Joker" by the Steve Miller Band) was released on May 8 as a Music download|download,  and on various dates as a Compact Disc|CD, including on May 22 as an import in the United States,  and on May 25, 2012 in Australia.  Songs not featured on the soundtrack that are in the film include "Superfly (song)|Superfly" by Curtis Mayfield, and "Crocodile Rock" by Elton John.

====Track listing====
: Included next to each track is the year of the songs original release, excluding the score pieces. 
{{Track listing
| collapsed =
| headline = Dark Shadows: Original Motion Picture Soundtrack
| extra_column = Artist
| title1 = Nights in White Satin
| note1 = 1967
| extra1 = The Moody Blues
| length1 = 4:26
| title2 = Dark Shadows – Prologue
| extra2 = Danny Elfman
| length2 = 3:56
| title3 = Im Sick of You
| note3 = 1972/1973
| extra3 = Iggy Pop
| length3 = 6:52 Season of the Witch
| note4 = 1966
| extra4 = Donovan
| length4 = 4:56 Top of the World
| note5 = 1972
| extra5 = The Carpenters
| length5 = 3:01
| title6 = Youre the First, the Last, My Everything
| note6 = 1974
| extra6 = Barry White
| length6 = 4:35 Bang a Gong (Get It On) 
| note7 = 1971
| extra7 = T. Rex (band)|T. Rex
| length7 = 4:26 No More Mr. Nice Guy
| note8 = 1972/1973
| extra8 = Alice Cooper
| length8 = 3:08
| title9 = Ballad of Dwight Fry
| note9 = 1971
| extra9 = Alice Cooper
| length9 = 6:36
| title10 = The End?
| extra10 = Danny Elfman
| length10 = 2:30
| title11 = The Joker
| note11 = original song from 1973
| extra11 = Johnny Depp
| length11 = 0:17
}}

==Reception==

===Box office=== The Avengers.      However, the film was popular overseas. The film came second to The Avengers in most countries in regard to opening box office takings. 

===Critical response===
Dark Shadows has received mixed  reviews from film critics, with a "rotten" percentage of 37% and an average rating of 5.3/10 on the review aggregator website   gives the film a score of 55% based on 42 reviews. 
 comedy or Drama film|drama)  pointing to Grahame-Smiths script, and that some jokes fell flat.    Some further claimed that Tim Burton and Johnny Depps collaborations have become tired,      and that Depp overacted in the film.    Many of the same, and other reviewers, however, noted its visual style was impressive.   

Positive reviewers, on the other hand, opined that the film did successfully translate the mood of the soap opera,  also acclaiming the actors—most notably Depp as Barnabas, who several said was the stand-out character due to his humorous culture shock,  as well as Pfeiffer   —and their characters; and further, that the films 70s culture pastiche worked to its advantage. 
 lugubrious source material and a sporadic tremor of violence, surprisingly effervescent," and opined in a mostly positive review that Burtons "gift for deviant beauty and laughter has its own liberating power."   

  suggested by its title or in playful, go-for-broke Camp (style)|camp."   

  among the women here, but theyre all bewitching."    Peter Bradshaw, in the British newspaper The Guardian, weighed the film in a mixed write-up, giving it three stars out of five, and pointing out his feeling that "the Gothy, jokey darkness of Burtons style is now beginning to look very familiar; he has built his brand to perfection in the film marketplace, and it is smarter and more distinctive than a lot of what is on offer at the multiplex, but there are no surprises. There are shadows, but they conceal nothing."   

===Accolades===
{| class="wikitable" rowspan=5; style="text-align: center; background:#ffffff;"
!Award!!Category!!Recipient!!Result!!Ref.
|- Young Artist 34th Young Best Performance Gulliver McGrath|| ||   
|- 2013 Kids Favorite Movie Johnny Depp|| ||
|}

==Home media==
 
Dark Shadows was released on both Blu-ray and DVD in the United States on October 2, 2012, the date confirmed by the official Dark Shadows Facebook page, and the official Dark Shadows web site.  The film was released on both formats several days earlier in Australia; in stores on September 24, and online on September 26, 2012.  The film was released on October 15, 2012 in the UK.
 UltraViolet digital copy of the film.

==See also==
*Vampire film
There have been two other feature films based on the soap opera Dark Shadows:
* House of Dark Shadows (1970)
* Night of Dark Shadows (1971)

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 