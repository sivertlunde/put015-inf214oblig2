Thazhvaram
{{Infobox film
| name = Thazhvaram
| image = Thazhvaram.jpg
| image_size =
| caption = Thazhvaram DVD cover
| director = Bharathan
| producer = V. B. K. Menon
| writer = M. T. Vasudevan Nair
| narrator = Anju Sankaradi Balan K. Nair Johnson Songs: Bharathan Venu
| editing = B. Lenin V. T. Vijayan
| studio = Anughraha Cini Arts
| distributor = Anughraha Cini Arts Release (Kerala)
| released =  
| runtime = 130 minutes
| country = India
| language = Malayalam
| budget =
| gross =
}} Malayalam Thriller (genre)|revenge-thriller film directed by Bharathan and written by M. T. Vasudevan Nair, starring Mohanlal, Salim Ghouse,  Anju (actress)|Anju, Sumalatha,  and Sankaradi in the lead roles. It tells the tale of Balan (Mohanlal) whos on the lookout for revenge against Raju (Salim Ghouse) for the murder of Balans wife Raji (Anju).
 cult status in Kerala since its release. Considered as the best thriller of all time in Malayalam cinema. Mohanlals acting skills and Bharathans direction are at the peak in this film. {{cite news|title=Screenplays for ever
|url=http://www.thehindu.com/entertainment/screenplays-for-ever/article6449094.ece|agency=The Hindu|date=September 26, 2014}}  

==Plot==
Balan (Mohanlal) is in search of Raju (Salim Ghouse) and the search takes him to a hillside. Balan reaches a house, where he is welcomed  whole-heartedly by both Nanu (Sankaradi), the house owner, and Kochutty (Sumalatha), his daughter. Balan realizes that Raju stays with them as Raghavan. Balan comes to know that Nanu has helped Raju to start farming and that Nanu intends to get his daughter married to Raju. Balan decides to wait for Raghavan, alias Raju. Raju, on his arrival, smells his enemy.

The film progresses with the two having to pretend before Nanu and Kochutty that they are good friends. The film cuts to flashback, where Raju alias Raghavan is shown as a friend of Balan once. He, in greed for money, one day killed Balans wife and ran away with Balans hard earned money. Balan is now back in search of Raju to avenge for the death of his wife. 

Raju attacks Balan in one of the numerous encounters the duo has and almost kills Balan. Balan survives the attack to save Nanu and his daughter from Raghavan.

The film is special for the mood it creates of the friction between the Balan and Raju. The dressing code matches what the director often symbolizes to a vulture.

==Cast==
*Mohanlal as Balan
*Salim Ghouse as Raju (credited as Saleem)
*Sumalatha as Kochootti Anju as Raji
*Sankaradi as Nanu

==Soundtrack==
The soundtrack of this film had only one song composed by Bharathan himself to the lines written by Kaithapram and rendered by K. J. Yesudas.
{| class="wikitable"
|-
! # !! Song Title !! Singer !! Note(s)
|-
| 1 || "Kannetha Doore Marutheeram"  || K. J. Yesudas || Ragam: Sudha Saveri
|}

==Response==
 Venu is considered as the highlight of the film. Thazhvaram had just a handful of characters and the plot was so well developed that it was a visual treat for the views. The fighting in the climax amidst violent vultures is considered as one of the best climax scenes in Malayalam cinema.

==Trivia==
*The songs were composed by Bharathan himself.
*Even though, was a big hit when released, now, over the years, this film is considered as a cult classic in Malayalam.
*The performance of Mohanlal is considered as one of his best in his acting career by the critics.
*This is Bharathans second film with M. T. Vasudevan Nair. The first was Vaishali (film)|Vaishali in 1988.
*This is the second collaboration of mohanlal and bharathan after kattathe killikudu.
* Bharathan and Gayathri Ashokan did the Poster Designs

==External links==
*  

==References==
 

 
 
 
 
 
 
 
 
 
 
 