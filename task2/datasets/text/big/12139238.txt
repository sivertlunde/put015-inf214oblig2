Tooth and Nail (film)
{{Infobox film
| name           = Tooth and Nail
| image          = Tooth and nail.jpg
| caption        = Promotional film poster
| director       = Mark Young
| producer       = Jonathan Sachar Patrick Durham
| writer         = Mark Young Michael Kelly Robert Carradine Vinnie Jones Michael Madsen
| music          = Elia Cmiral
| cinematography = Gregg Easterbrook
| editing        = Mark Young
| distributor    = After Dark Films  (theatrical) 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Tooth and Nail is a 2007 horror film written, directed and edited by Mark Young, about a group of people in a post-apocalyptic world who must fight to survive against a band of vicious cannibals.

== Plot ==
Tooth and Nail is a post-apocalyptic movie where mankind has depleted all fossil fuel reserves and civilization has collapsed. A group of survivors called Foragers take cover in an abandoned hospital where the group attempt to re-build society. After saving a young girl from being killed and eaten by a group of vicious cannibals called Rovers, the Foragers find themselves on the run from the cannibals, who stalk the survivors and brutally kill them off one-by-one as the Foragers begin to fight back, causing a chaotic battle of blood and mayhem.

==Cast==
*Rachel Miner as Neon
*Nicole DuPort as Dakota
*Rider Strong as Ford Michael Kelly as Viper
*Robert Carradine as Darwin
*Michael Madsen as Jackal
*Vinnie Jones as Mongrel
*Alexandra Barreto as Torino
*Emily Catherine Young as Nova
*Beverly Hynds as Victoria
*Patrick Durham as Shepherd
*Jonathan Sachar as Wolf
*Garrett Ching as Pug
*Kevin E. Scott as Max

==Release==
This film was released in theatres as part of After Dark Films 8 Films to Die For|Horrorfest, which ran November 9–18, 2007.

==External links==
*  
*  
*  
*   at Bloody-Disgusting.com

 

 
 
 
 
 
 


 