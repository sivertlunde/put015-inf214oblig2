Inch by Inch (film)
 
{{Infobox film
|name=Inch by Inch
|image=Inch by Inch DVD cover.jpg
|caption=DVD cover
|director=Matt Sterling
|producer=Matt Sterling
|writer=Matt Sterling
|cinematography=Nick Eliot Doug Williams
|editing=Paul James
|released=1985  (United States) 
|country=United States
|studio=Huge Video
|runtime=64 minutes
|starring=Jeff Quinn Doug Jensen Tom Brock
|language=English
|distributor=Falcon Studios  (DVD) 
}}
 gay pornographic film,  directed, written, and produced by Matt Sterling,   photographed by Nick Eliot and Doug Williams, edited by Paul James,  and starring Jeff Quinn, Doug Jensen, and Tom Brock.   The runtime is 64 minutes,  and DVD copies have been distributed by the Falcon Studios.  

== Plot ==

Tony Stefano finds Steve Henson and Mike Raymond breaking into his apartment. After he deals with them, Tony then goes to his bed and then fantasizes a homosexuality|male-to-male threesome scene. Meanwhile, Mark Miller, Christopher Lance, Bill Joseph, and Kevin Luken are engaging an orgy on the roof of the apartment building. In the next scenario, Mike Raymond voyeurs at Tom Brock by the window and they then engage each other. In the last scenario, Jim Pulver and Jeff Quinn hook each other up in an empty subway car.  The film ends with Tom Brock entering the subway and the following text: "The non-stop excitement continues....   This bracketed phrase actually appears in the film.  in the next Matt Sterling film, coming February 1986". 

;Scenes 
* Scene 1: Tony Stefano, Steve Henson, and Mike Raymond
* Scene 2: Mark Miller, Christopher Lance, Bill Joseph, and Kevin Luken
* Scene 3: Steve Wright, Doug Jensen, and Toby Matson; Tony Stefano, fantasizing
* Scene 4: Mike Raymond and Tom Brock
* Scene 5: Jim Pulver and Jeff Quinn

== Reception ==

Keeneye Reeves rated this film three out of four stars, called it a "hot n sleazy pre-condom fun thats worth seeing twice", and praised the sex scenes, yet Reeves criticized the runtime as "short" and ending as "hokey".  " ." Video Reviews and Comments May 2006. Web. 10 April 2012. 

The reviewer from Rad Video rated this movie four out of five stars and praised its sex scenes and male bodies but found its runtime too short. " ." Alane Video. Web. 10 April 2012.  Mark Adnum from Outrate called this film "a masterpiece of gay porn packed with great sex and hot guys".  Both reviewers found its ending a tease to an upcoming sequel that never happened.  

In the book The Culture of Queers (2002), Richard Dyon called the subway scene a blend of "  were "too legible and too appropriate to be true."  Nevertheless, he found sex performances of two men sexy, titillating, well shot and edited,  and well-done signals of "abandonment and sexual hunger" as part of realism of anonymous sex. 

Director Matt Sterling picked two scenes of this film as his favorites for the compilation of Matt Sterlings accomplishments, Best of All: the window voyeurism scene of Tom Brock and Mike Raymond, and the subway scene of Jim Pulver and Jeff Quinn. 

In 1986, Inch by Inch won two Gay Producers Association Awards for the Best Video and Best Newcomer (Nick Eliot)  and one Adam Film World award for the Gay Movie of the Year. 

== Notes ==
;Footnotes
 

;Inline references
 
 " ." TLA Video. Web. 10 April 2012. 
 " ." NYMmedia.com. Web. 10 April 2012. 
 " ." NYMmedia.com. Web. 10 April 2012. 
 " ." Male-erotika.com. Web. 10 April 2012. 
 " ." Gay Erotic Video Index, 2010. Web. 10 April 2012. 
 Dyer 2002, p. 188 
 Adnum, Mark. "Takes A Mile (Inch by Inch review)." Outrate.com 18 April 2009. Web. 15 April 2012    .  . 
 Dyer 2002, pp. 188&ndash;191. 
 Dyer 2002, pp. 189. 
 

=== References ===
* Dyer, Richard. The Culture of Queers. London: Routledge—Taylor & Francis, 2002. Hardcover: ISBN 0-415-22375-X. Paperback: ISBN 0-415-22376-8.

== Further reading ==
* Escoffier, Jeffrey. Bigger Than Life: The History of Gay Porn Cinema from Beefcake to Hardcore. Philadelphia, PA: Running Press Book Publishers, 2009. ISBN 978-0-7867-2010-1.  .

== External links ==
*  

 
 
 