Prem Kahani (1937 film)
{{Infobox film
| name           = Prem Kahani
| image          = Prem Kahani (1937 Hindi film).png
| caption        = Song book cover
| alt            = Song book cover with title in three languages showing the key crew of the film
| director       = Franz Osten
| producer       = Himanshu Rai
| writer         = Niranjan Pal
| starring       = Ashok Kumar   N.M. Joshi   Mayadevi   Vimala Devi   Madhurika Devi Saraswati Devi
| cinematography = Joseph Wirsching
| editing        = Dattaram Pai
| studio         = Bombay Talkies
| released       = 
| runtime        = 87.20 minutes
| country        = British Raj
| language       = Hindi
| budget         =
| gross          =
}} 1937 Bollywood|Hindi film, directed by Franz Osten and starring Ashok Kumar, N.M. Joshi, Mayadevi, Vimala Devi, Madhurika Devi and others. 

==Production==
Bombay Talkies produced two films in 1937, Jeevan Prabhat and Prem Kahani. This is also Bombay Talkies eighth film since the studio was founded in 1934.
 Saraswati Devi, one of the few female composers of Hindi cinema, composed music for this film.
 Niranjan Pals original title for the English-language story and screenplay was Touchstone or Marriage Market, indicating a shorthand version of the films theme. The verse that Pal uses as a mood reference for one of the songs in the film is from Arthur Ryder’s 1912 translation of Kalidasa’s Kumarasambhava or The Birth of the War-God. 

==References==
 

==External links==
*  
*  

 

 
 
 


 