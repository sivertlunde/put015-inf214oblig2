Neds (film)
 
 
 
{{Infobox film
 | name = Neds
 | image = Neds2010Poster.jpg
 | caption = Film poster
 | director = Peter Mullan
 | Written by = Peter Mullan 
 | produced by = Alain de la Mata, Lucinda Van Rie (UK), Conchita Airoldi (ITL), Fidelite (Fr) Marcus Nash Linda Cuthbert John Joe Hay Sean Higgins Craig Armstrong
 | cinematography = Roman Osin
 | editing = Colin Monie
 | released =  
 | runtime = 124 minutes Film4 Scottish Wild Bunch
 | distributor = E1 Entertainment (UK) New Video Group
 | country = United Kingdom France Italy Glaswegian Scots
 | gross = £632,204}}
 ned culture and the consequences of it on his teenage years.

==Plot==
  Hardridge says he will beat John up when he moves to secondary school. John tells his older brother Benny, who is a gang leader of a young team called "Young Cardonald|Car-D" and Benny beats Canta up. Once John joins secondary school everything goes fine, apart from him not being in the top class. He is then told by the headmaster that if he proves to be different from his brother (who assaulted two teachers during the previous year before being expelled) then he will move up a class by Christmas time, which he does. Since John lacks a social life and friends, his teacher advises him to attend a summer camp for children with disabilities, where he meets a middle-class boy called Julian. One day he accidentally breaks one of Julians fathers records and is forbidden to see Julian again.

While walking home, a group of youths, calling themselves "Young Car-D", threaten to mug him until they realize who his brother is, at which point they leave him alone and even ask him to join them, as well as giving him vodka and cigarettes, which he accepts. This is the start of his downward spiral. Once school starts, it has become clear that John has changed his ways for the worse – graffitiing the desks, being impolite to teachers and going for cigarettes in the toilets at breaktime. At one point he gets caught up in a fight between two gangs and obtains a knife which had been kicked under the door of the toilet cubicle in which he is hiding during the fight. Subsequently, during a weekend he gets back at Julians family for rejecting him by throwing a bag full of fireworks through their window while they are having a meal.

He then goes to a social club, where members of the "Car-D" gang force two boys from a rival gang called "the Krew" out. They come back later and throw a bicycle through the function room window, urging the "Car-D" to give chase up to the edge of their territory, where they escape via a walkway. John gives chase and ends up running right into the rest of the "Krew" and their leader who then chase him. He pushes his way into an Irish womans home to escape. She shelters him temporarily. It transpires that the woman is the mother of the boy who threw the bicycle.

Between 1972 and 1974 John becomes more deeply involved in gang feuds. He slits a boys throat in a gang fight and hides the blade. Returning home he finds that the police are there and after hiding from them, on their departure John learns that his brother has been arrested as a suspect for the stabbing. The next day John finds that his brothers bail has been set at £15 by the Sheriff court. He robs a bus driver at knife point to raise the money but fails to pay the sum in time.

His violent lifestyle continues and while out for a walk with Claire, one of the girls from the gang, he spots a now isolated and weak Canta. He confronts him about the bullying incident years before and the encounter ends with him knocking Canta to the flor and dropping a stone slab on him, causing him brain damage.

John becomes increasingly confrontational, throwing a glass bottle at a passing police officer as he hangs out with "Young Car-D" in a park. The gang ostracize him for attracting police attention. At home his father is drunkenly abusive to his mother so John later beats him round the head with a frying pan. His mother tells John to get out of the house and he is then forced to take refuge in the machine room of the lifts in a block of high rise flats. He survives by stealing doorstep deliveries of milk and bread to the flats.

One day John finds the gate to the boiler room locked and goes for a walk to a statue of Jesus Christ, who John urges to come down jokingly, he then has a vision that he does so but the figment of Jesus ends up beating him for his choice of life. The next morning, his sober dad finds him and tells him to go home.

Johns father asks him to put him out of his misery and end his life. John goes to his room and prepares two knives which he tightly straps to both hands. He goes downstairs where his father is urinating in a bottle. He asks John to wait until he is asleep. John wanders the streets, stripped to the waist and in a self-destructive mood. He comes across the rival gang and viciously attacks one of them, injuring him badly. John is then pursued by the other gang members and is badly beaten in turn. Members of "Young car-D" then spot him and fight off his attackers only to have John turn on them after getting to his feet. Injured, he returns home to the room where his father is asleep. Unable to go through with killing his father, he collapses on top of him and falls asleep . 
 
He chooses to change his ways and returns to school, attending a remedial class, wanting no further involvement in the gang. The class goes on a field trip to a safari park and their minibus breaks down. He thinks the teachers have abandoned him with Canta and he decides to leave the van, walking hand-in-hand with Canta through a pack of lions; the lions leave them alone as they walk off into the distance.

==Cast==
*Conor McCarron – John McGill
*Gregg Forrest – Young John
*Marianna Palka – Aunt Beth
*Joe Szula – Benny
*Mhairi Anderson – Elizabeth
*Gary Milligan – Canta
*John Joe Hay – Fergie
*Christopher Wallace – Wee T
*Richard Mack – Gerr
*Paul Smith – Key Man
*Khai Nugent – Tam 
*Ryan Walker – Sparra
*Lee Fanning – Minty
*Ross Greig – Fifey
*Greg McCreadie – Tora
*Scott Ingram – Casper
*Cameron Fulton – Crystal
*Craig Kerr – Rebel
*Martin Bell – Julian
*Kat Murphy – Claire
*Stefanie Szula – Linda
*Annie Watson – Agnes
*Zoë Halliday – Mandy
*Sara MacCallum – Shelagh
*John Forrest – McCluskey Boy
*Louis McLaughlin – Robert
*David OBrien – Bernard
*Ross Weston – Danny
*Claire Gordon – Louise Marcus Nash – Patrick
*Victoria Rose – Carole
*Chelsey Hanratty – Eileen
*Linda Cuthbert – Mrs. Matherson
*Peter Mullan – Mr. McGill

==Reception==
The film has received mostly positive reviews from critics.     

Neds won the Golden Shell (Best Film) at the San Sebastian Film Festival in September 2010. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 