Little Caesar (film)
{{Infobox film
| name           = Little Caesar
| image          = LittleCaesarP.jpg
| caption        = Theatrical release poster
| director       = Mervyn LeRoy
| producer       = Uncredited: Hal B. Wallis Darryl F. Zanuck
| based on       =  
| writer         =   Darryl F. Zanuck
| starring       = Edward G. Robinson Douglas Fairbanks, Jr. Glenda Farrell
| music          = Erno Rapee
| cinematography = Tony Gaudio
| editing        = Ray Curtiss
| studio         = First National
| distributor    = Warner Bros.
| released       =  
| runtime        = 79 minutes
| country        = United States English
}}

Little Caesar is a 1931 Warner Bros. Pre-Code crime films|Pre-Code crime film that tells the story of a hoodlum who ascends the ranks of organized crime until he reaches its upper echelons. Directed by Mervyn LeRoy and starring Edward G. Robinson and Douglas Fairbanks, Jr., the story was adapted by Francis Edward Faragoh, Robert N. Lee, Robert Lord and Darryl F. Zanuck (uncredited) from the novel of the same name by William R. Burnett. Little Caesar was Robinsons breakthrough role and immediately made him a major film star.

==Plot== Chicago to Stanley Fields), while Joe wants to be a dancer. Olga (Glenda Farrell) becomes his dance partner and girlfriend.

Joe tries to drift away from the gang and its activities, but Rico makes him participate in the robbery of the nightclub where he works. Despite orders from underworld overlord "Big Boy" (Sidney Blackmer) to all his men to avoid bloodshed, Rico guns down crusading crime commissioner Alvin McClure during the robbery, with Joe as an aghast witness.

Rico accuses Sam of becoming soft and seizes control of his organization. Rival boss "Little Arnie" Storch (Maurice Black) tries to have Rico killed, but Rico is only grazed. He and his gunmen pay Little Arnie a visit, after which Arnie hastily departs for Detroit. The Big Boy eventually gives Rico control of all of Chicagos Northside.

Rico becomes concerned that Joe knows too much about him. He warns Joe that he must forget about Olga and join him in a life of crime. Rico threatens to kill both Joe and Olga unless he accedes, but Joe refuses to give in. Olga calls Police Sergeant Flaherty and tells him Joe is ready to talk, just before Rico and his henchman Otero (George E. Stone) come calling. Rico finds, to his surprise, that he is unable to take his friends life. When Otero tries to do the job himself, Rico wrestles the gun away from him, though not before Joe is wounded. Hearing the shot, Flaherty and another cop give chase and kill Otero. With information provided by Olga, Flaherty proceeds to crush Ricos organization.

Desperate and alone, Rico "retreats to the gutter from which he sprang." While hiding in a flophouse, he becomes enraged when he learns that Flaherty has called him a coward in the newspaper. He foolishly telephones the cop to announce he is coming for him. The call is traced, and he is gunned down by Flaherty behind a billboard - an advertisement featuring dancers Joe and Olga - and dying, utters his final words, "Mother of mercy, is this the end of Rico?"

==Cast==
 ]]
*Edward G. Robinson as Caesar Enrico "Rico" Bandello / "Little Caesar"
*Douglas Fairbanks Jr. as Joe Massara
*Glenda Farrell as Olga Stassoff
*William Collier Jr. as Tony Passa
*Sidney Blackmer as "Big Boy"
*Ralph Ince as Pete Montana Stanley Fields as Sam Vettori
*Maurice Black as "Little Arnie" Lorch
*George E. Stone as Otero
*Armand Kaliz as De Voss
*Thomas Jackson as Sergeant Flaherty
*Nick Bela as Ritz Colonna

==Award and honors==
*  ‒ Nominated
*  in 2000
*American Film Institute:
**AFIs 100 Years...100 Movies: Nominated in both 1998 and 2007
**AFIs 100 Years...100 Thrills: Nominated
**AFIs 100 Years...100 Heroes and Villains: Rico listed as the #38 best villain
**AFIs 100 Years...100 Movie Quotes: Ricos final words, "Mother of mercy, is this the end of Rico?", listed as the #73 best quote
**AFIs 10 Top 10: Listed #9 in the gangster film genre 

==Legacy== Never a The Little Giant. 

The box office triumph of Little Caesar also spawned the production of several successful gangster films, many of which were also made by Warner Brothers. 

===Possible homosexual subtext===
 
One interpretation of the films title character is that he may be a repressed or  : St. Martins Press, 2002. ISBN 0-312-28311-3   with the evidence thereof cited as including Oteros fawning admiration of Rico, Ricos great affinity for Joe, and Ricos complete lack of interest in romantic relationships with women, as well as his utter contempt for Joes interest in women.   When the film was released, Burnett apparently drew this same conclusion about the screen version of the character. Having written Rico as explicitly heterosexual in his novel, Burnett wrote a letter of complaint to the films producers about the conversion of the character to gay in the screen adaptation. 

==See also==
* 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 