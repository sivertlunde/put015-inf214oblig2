Chicago Massacre: Richard Speck
{{Infobox film
| name = Chicago Massacre: Richard Speck
| image =
| caption =
| director = Michael Feifer
| producer = Michael Feifer
| writer = Michael Feifer
| starring = Corin Nemec Andrew Divoff Tony Todd Debbie Rochon
| music = Andres Boulton
| cinematography = Matt Steinauer
| editing =
| distributor =
| released =  
| runtime = 92 minutes
| country = United States
| language = English
| budget = 
| gross = 
}}
Chicago Massacre: Richard Speck is a 2007 horror film directed by Michael Feifer. The film is based on the true story of 1960s mass murderer Richard Speck.

==Cast==
* Corin Nemec as Richard Speck
* Andrew Divoff as Jack Whitaker
* Tony Todd as Captain Dunning
* Debbie Rochon as Candy
* Joanne Chew as Sondra Azano
* Cherish Lee as Sharon
* Kelsey McCann as Annette
* Caia Coley as Nurse Boyd
* Daniel Bonjour as Detective Harper
* Cameo Cara Martine as Barbara Billing
* Mitchell Welch as Left Eye
* Samm Enman as Union Woker
* Deborah Flora as Tracy Hendricks

==References==
 

==External links==
*  
*  

 
 
 
 
 

 