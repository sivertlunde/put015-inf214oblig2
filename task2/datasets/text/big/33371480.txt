The Days of Being Dumb
 
 
 
{{Infobox film
| name           = The Days of Being Dumb
| image          = DaysofBeingDumb.jpg
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 亞飛與亞基
| simplified     = 亚飞与亚基
| pinyin         = Yǎ Fēi Yú Yǎ Jī
| jyutping       = Ngaa3 Fei1 Jyu2 Ngaa3 Gei1}}
| director       = Blackie Ko
| producer       = Peter Chan Joe Ma James Yuen Cheung Chi Sing Tony Leung Jacky Cheung Eric Tsang Kent Tong Anita Yuen
| music          = Richard Lo
| cinematography = Jingle Ma Andrew Lau Tony Miu
| editing        = Chan Kei Hop
| studio         = United Filmmakers Organisation Movie Impact
| distributor    = Newport Entertainment
| released       =  
| runtime        = 92 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$9,883,635
}} 1991 film Days of Being Wild which also featured Leung and Cheung.

==Plot==
Fred and Keith were childhood friends who dreamed of being triad members. As their became adults, they have achieved the dream and joined many gangs. However, after all the boss dies, the two were considered as jinxes by people and no gangs want to take them in. The two then depend on themselves to do business and got a prostitute from Singapore, Jane. Jane, however, believed she was supposed to be model and the two of them did not have courage to put her to work. Jane is also later found out to be a lesbian. Later, famous triad leader Kwan comes and takes Fred and Keith in to prove to people that he is invulnerable to their jinx. Under Kwan, these two manage to become triad heroes. However, later thing go wrong and Kwan is turned against the two and they must strike back to protect themselves.

==Cast== Tony Leung as Fred Tung
*Jacky Cheung as Keith
*Eric Tsang as Ball
*Kent Tong as Kwan
*Anita Yuen as Jane
*Jamie Luk as Slim
*Yeung Wan King as hoodlum tenant
*Chu Tau as hoodlum tenant
*Cheng Tai Kim as hoodlum tenant
*Tania Wong as Lisa
*Chan Chi Fai as Piggy
*Wong Hung as Brother Nine
*Glen Chin as General Lee
*Tam Wai as Iron
*Lam Chung as gangster (uncredited cameo)
*Billy Ching as Gold Teeth Shing
*Chun Kwai Po as Teeths gangster
*Cheung Ying Wa as car jockey
*Wan Kung Wai as car jockey
*Chan Leung Shun as car jockey
*Ben Luk as Mr. Lau
*Leung Kei Hei as Keiths dad
*Wan Seung Yin as Keiths 3nd  aunt
*Lee Chi Hang as young Keith
*William Chu as young Fred
*Simon Cheung as policeman in theatre
*Yeung Kei Sing as policeman in theatre
*Garry Chan as Big Mouth Cheong
*Sam Dang as Kwans thug
*Ng Piu Chuen as Kwans thug
*Mike Lau as Kwans thug
*Chang Yuk Chuen as Kwans thug
*Au Chiu Hang as Kwans thug
*Ng Yuk Sau as Kwans thug
*Wong Ying Kit as Little Kit
*Fung Yuen Chi as Lau Pei
*Tsui Kai Wah as long hair passerby
*Szeto Bobby as rascal
*So Chung as rascal
*Cheung Sam Po as rascal
*Or Wai Man as female TV reporter
*Sam Kin Sang as policeman
*To Kwan Pang as hawker
*Tse Chi Wah as watermelon hawker
*Wong Yun Ching as fat boy student
*Four Tse as funeral attendee
*Leung Kai Chi as Peis gangster
*Ho Chi Moon as Kwans gangster in theatre
*Kent Chow

==Box office==
This film grossed HK$9,883,635 during its theatrical run from 6 August to 26 August 1992 in Hong Kong.

==Award==
*12th Hong Kong Film Awards
**Won: Best New Performer (Anita Yuen)

==External links==
*  at LoveHKFilm.com
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 