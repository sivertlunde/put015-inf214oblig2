Speed Demon (1932 film)
 
{{Infobox film
| name           = Speed Demon
| image          = 
| caption        = 
| director       = D. Ross Lederman
| producer       = 
| writer         = Charles R. Condon
| starring       = William Collier Jr.
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
}}

Speed Demon is a 1932 American drama film directed by D. Ross Lederman.   

==Cast==
* William Collier Jr. as Speed Morrow
* Joan Marsh as Jean Torrance
* Wheeler Oakman as Pete Stenner Robert Ellis as Langard
* George Ernest as Catfish Jones
* Frank Sheridan as Captain Torrance
* Wade Boteler as Runyan
* Edward LeSaint as Judge (as Edward J. LeSaint)
* Fuzzy Knight as Lefty
* Ethan Laidlaw as Red
* Harry Tenbrook as Bull

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 