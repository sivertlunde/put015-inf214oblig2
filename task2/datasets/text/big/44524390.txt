Tell No Tales (film)
{{Infobox film
| name           = Tell No Tales
| image          = Tell No Tales poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Leslie Fenton
| producer       = Edward Chodorov
| screenplay     = Lionel Houser 
| story          = Pauline London Alfred Taylor 
| starring       = Melvyn Douglas Louise Platt Gene Lockhart Douglass Dumbrille Florence George Halliwell Hobbes
| music          = William Axt
| cinematography = Joseph Ruttenberg
| editing        = W. Donn Hayes 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Tell No Tales is a 1939 American crime film directed by Leslie Fenton and written by Lionel Houser. The film stars Melvyn Douglas, Louise Platt, Gene Lockhart, Douglass Dumbrille, Florence George and Halliwell Hobbes. The film was released on May 12, 1939, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Melvyn Douglas as Michael Cassidy
*Louise Platt as Ellen Frazier
*Gene Lockhart as Arno
*Douglass Dumbrille as Matt Cooper 
*Florence George as Lorna Travers
*Halliwell Hobbes as Dr. Lovelake
*Zeffie Tilbury as Miss Mary
*Harlan Briggs as Davie Bryant
*Sara Haden as Miss Brendon
*Hobart Cavanaugh as Charlie Daggett 
*Oscar OShea as Sam ONeil
*Theresa Harris as Ruby
*Jean Fenwick as Mrs. Lovelake
*Esther Dale as Mrs. Haskins
*Joseph Crehan as Chalmers
*Tom Collins as Phil Arno
*Clayton Moore as Wilson

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 