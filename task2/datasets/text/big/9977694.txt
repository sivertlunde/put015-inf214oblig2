Screwballs II
{{Infobox film
|  name           = Loose Screws
|  image          = Loose Screws.jpg
|  writer         = Michael Cory, Linda Shayne, Jim Wynorski
|  starring       = Bryan Genesse, Lance Van Der Kolk
|  director       = Rafal Zielinski Robin Miller
|  editing        = Stephan Fanfara
|  producer       = Maurice Edward Smith
|  studio         = Crazy Wheels Film Corporation
|  distributor    = Concorde Pictures Lightning Video Buena Vista Home Entertainment Severin Films
|  released       = 1985
|  music          = Fred Mollin
|  runtime        = 92 minutes English
}} 1985 comedy film.    It is a sequel to Screwballs. 

==Cast and crew==
*Bryan Genesse	 ...	Brad Lovett
*Lance Van Der Kolk	 ...	Steve Hardman
*Alan Deveau	 ...	Hugh G. Rection
*Jason Warren	 ...	Marvin Eatmore
*Annie McAuley	 ...	Nikki Nystroke
*Karen Wood	 ...	Gail Poulet
*Liz Green	 ...	Tracey Gratehead Mike MacDonald	 ...	Mr. Arsenault
*Cynthia Belliveau	 ...	Mona Lott (as Cyd Belliveau)
*Deborah Lobban	 ...	Hilda Von Blow
*Carolyn Tweedle	 ...	Female Teacher
*Stephanie Sulik	 ...	Claudia Arsenault
*Terrea Smith	 ...	Wendy the Waitress (as Terrea Oster)
*Wayne Fleming	 ...	Pigpen M.C.
*Lisa Maggiore       ...    Student

==Synopsis==
Brad Lovett, Steve Hardman, Hugh G. Rection, and Marvin Eatmore are four get-nowhere boys who are forced into summer school, ending up at Cockswell Academy under the supervision of Principal Arsenault.  The boys play a game where they earn points for every girl with which they score. On misadventures of their own, they decide to go for the ultimate 100-point score, Mona Lott, the new French teacher. But when theyre unable to get a shot at her, they end up in the unforgiving clutches of the principal. After all is lost, they take one final chance during the schools anniversary celebration.

==Soundtrack==
The Soundtrack was handled by Fred Mollin.
Soundtrack includes songs by The Nu Kats.
#Changing  (Demi Moore - Freddy Moore)  - The Nu Kats
#Circular Impressions - (Denis Keldie - L. Stevenson) - The Extras
#Summer Fun - (Bill King) - Bill King Quartet
#Jump For Joy - (Tim Ryan - Jonathan Goldsmith - Kerry Crawford) - Tim Ryan
#Dance The Screw - (Errol Francis - Susan Francis) - Errol Francis and the Francis Factor
#Dance Tonight - (Errol Francis) - Errol Francis and the Francis Factor
#Do The Screw - (Fred Mollin) - Meyer and Kronke
#School Break - (Errol Francis - Susan Francis - Mark Francis) - Errol Francis and the Francis Factor
#Screw It - (Denis Keldie - L. Stevenson) - Denis Keldie

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 



 