Basilicata Coast to Coast
{{Infobox film
| name     = Basilicata Coast to Coast
| image    = Basilicata Coast to Coast.jpg
| border   = yes
| director = Rocco Papaleo
| producer = 
| starring = Rocco Papaleo Alessandro Gassman Paolo Briguglia Max Gazzè
| cinematography = 
| music = 
| country = Italy
| language = Italian
| runtime = 105 minutes
| released =  
}}
Basilicata Coast to Coast is a 2010 Italian comedy film directed by Rocco Papaleo.

== Plot ==
Nicola Palmieri (Rocco Papaleo) is a high school math teacher with a passion for music. He is the frontman of a local music band he formed with a group of friends from Maratea. Guitarist Salvatore Chiarelli (Paolo Briguglia) is a medical student who has somehow forgotten to graduate and fall in love; double bass player Franco Cardillo (Max Gazzè) is a fisherman at whom love has taken away word and purpose in life; drummer Rocco Santamaria (Alessandro Gassman), Salvatores cousin, is a TV personality whose popularity is declining and hasnt been able to find a showbiz job in the last two years.

During the Summer, the quartet decides to sign up for the national theater-song festival in Scanzano Jonico, renaming themselves "Le Pale Eoliche" (the windmill blades). To get from Maratea to Scanzano Jonico, they need to cross Basilicata from its coast on the Tyrrhenian Sea to the one on the Ionian Sea. Nothing peculiar about this, with a little over 100 km to drive through in about one hour. Thats when Nicola suggests leaving 10 days earlier and on foot, trying to find a purpose in life which theyve lost.

They set themselves on to a picaresque trip, followed by a local church TV crew along with bored local journalist Tropea Limongi (Giovanna Mezzogiorno), daughter of a renowned local politician. During their long trek, the group walks through little-known backroads, with only a small wagon pulled by a white horse to transport supplies, instruments and two tents. They also rehearse the songs they will be performing at the festival, with impromptu concerts in the small villages they walk by. 

This journey will prove therapeutic for everybody: Salvatore finds the willpower to complete medical school; Rocco finds a normal job; Franco starts talking again, uncovering a deep feeling towards Tropea in the mean time; Nicola finally completes one of his projects, showing his wife that he can be an assertive man.      

== Cast ==
* Rocco Papaleo - Nicola Palmieri
* Alessandro Gassman - Rocco Santamaria
* Paolo Briguglia - Salvatore Chiarelli
* Max Gazzè - Franco Cardillo
* Michela Andreozzi - Lucia
* Giovanna Mezzogiorno - Tropea Limongi
* Claudia Potenza - Maria Teresa
* Gaetano Amato - Onorevole Limongi
* Antonio Gerardi - Carmine Crocco
* Augusto Fornari - Press Agent
* Antonio Andrisani - Prete

== External links ==
* 

 
 
 
 
 

 