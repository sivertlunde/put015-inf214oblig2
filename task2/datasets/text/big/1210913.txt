Pootie Tang
 
{{Infobox film
| name = Pootie Tang
| image = Pootie Tang_poster.JPG
| alt = 
| caption = Theatrical release poster
| director = Louis C.K.
| producer = {{Plain list|
* Caldecot Chubb
* David Gale
* Ali LeRoi
* Chris Rock}}
| writer = Louis C.K.
| starring = {{Plain list|
* Lance Crouther
* J. B. Smoove
* Jennifer Coolidge
* Wanda Sykes
* Robert Vaughn
* Chris Rock}}
| music = {{Plain list| QD3
* Prince Paul  }}
| cinematography = Willy Kurant
| editing = Doug Abel
| studio = {{Plain list|
* MTV Films Chris Rock Productions}}
| distributor = Paramount Pictures
| released =  
| runtime = 81 minutes  
| country = United States
| language = English
| budget = $7 million   
| gross = $3,313,583 
}} comedy sketch that first appeared on The Chris Rock Show,  the character Pootie Tang is a satire of the stereotyped characters who appeared in old blaxploitation films. The title characters speech, which vaguely resembles pidgin, is mostly unintelligible to the audience, but the other characters in the film have no problem understanding him.

==Plot==
Pootie Tang, born in "a   who is "too cool for words", even as a young child. His life is marked by the deaths of his mother "Momma Dee", and shortly thereafter his father "Daddy Tang", who dies after being mauled by a gorilla during his shift at the steel mill (the third time someone had suffered that particular fate). Just before Daddy Tangs death, Pootie inherits his fathers belt and is told that (as long as he has right on his side) he can "whoop anyones ass with just that belt."
 CEO of multi-industrial conglomerate LecterCorp, learns of Pootie Tangs positive influence on society — and his negative influence on LecterCorps bottom line. After his henchmen and a vile villain named Dirty Dee are sent away by Pooties friends, Lecter encourages his right-hand lady, Ireenie, to seduce Pootie Tang into signing an agreement with LecterCorp that would stop Pootie Tangs influence on Americas children.

Pootie Tang falls for Ireenies tricks and subsequently falls apart. His status as pop culture icon is destroyed, and he engages on a quest to "find  self". This journey is encouraged by Biggie Shortie, who promises to wait for Pootie to return to her and to the rest of society. Pootie moves to a farm where the local sheriff decides Pootie and his daughter should start dating. After his single corn stalk dies, he has a vision of Daddy Tang and Momma Dee. Daddy Tang reveals that there is nothing special about Pooties belt; instead, Pootie must fight evil with the goodness that is inside him. After dealing with Dirty Dee and his henchman Froggy (as well as getting his belt back), Pootie realizes he must move back to the city and fight crime once again.

Pootie Tang returns to the city just as Dick Lecter is unveiling the first of his new restaurant chain, Pooties Bad Time Burgers. At a small news conference, Pootie confronts Lecter only to discover that Lecter has amassed dozens of "Pootie-alikes" who will spread the message of LecterCorp around the nation. Pootie Tang, with the help of Biggie Shortie, defeats all of these henchmen and Lecter himself. Good triumphs over evil once again, and Biggie Shortie finally gets her man: she and Pootie Tang plan to get married now that Pootie is back. Elsewhere, Dick Lecter leaves corporate life and becomes an actor, Ireenie leaves him and becomes a counselor helping at-risk teenage prostitutes, and Dirty Dee is still dirty.

===Pootie Tang-isms===
Although Pootie Tang speaks in a completely unintelligible African American Vernacular English|jive, everyone he meets seems to understand him. Here are some examples of his vocabulary. See wikiquote for a more complete list.

{| class="wikitable"
|-
| "Bammies." || many interpretations and usages, yet usually refers to normal acquaintances
|-
| "Sa Da Tay." || generally positive interpretations
|-
| "Wa Da Tah." || many interpretations and usages, yet usually is a confirmation statement like "thats for sure!"
|-
| "Capatown." || many interpretations and usages, yet in many instances means "Calm down now" in a friendly manner
|-
| "Dont bang the dillies!" || Dont
|-
| "The Tipi Tais." || generally accepted as "the kids"
|-
| "Dirty Dee, youre a baddy daddy lamatai tabby chai!" || a threat, an insult, or both
|-
| "Whats the Dabble Dee?" || generally accepted as "Whats the matter?"
|-
| "May I dane on the cherries, Mama T?" || "Mother, would it be acceptable for me to have some more peas?"
|-
|-
| "Main Damie." || generally accepted as "Best Friend"
|-
| "Well Bob, Im a pone tony." || explains his achievement in many diverse fields
|-
| "Im going to sine your pitty on the runny kine!" || a warning to his enemies of impending punishment; also used to smooth-talk the ladies
|-
| "Ma Dilly." || seems to refer to a female "Damie"; he refers to Biggie Shorty as his Dilly
|-
| "Nay-no" || "no," as in "I gotta say the Nay-no, my brotha"
|-
| "Cole me down on the panny sty" || Pooties way of indicating complicity and friendship in his listener 
|-
|-
| "Cole me on the panny sty." || Close to the above in sound, but gravely different in meaning. Cole me on the panny sty is insultingly nonsensical, as shown by the reaction of Bob Costas 
|-
| "You aint come one, but many tine tanies" || Pooties way of saying "You didnt just bring yourself, you brought your whole crew to back you up", implying cowardice towards his listener
|-
|}

==Cast==
 
* Lance Crouther as Pootie Tang
* J. B. Smoove as Trucky
* Jennifer Coolidge as Ireenie
* Wanda Sykes as Biggie Shorty
* Robert Vaughn as Dick Lecter
* Chris Rock as JB/Radio DJ/Daddy Tang
* Reg E. Cathey as Dirty Dee
* J.D. Williams as Froggy
* Mario Joyner as Lacey
* Dave Attell as Frank
* Laura Kightlinger as Laura Knight
* Rick Shapiro as Shakey
* Missy Elliott as Diva
* David Cross as Dennis, a Pootie-alike
* Cole Hawkins as Little Pootie
* Andy Richter as Record executive
* Kristen Bell as Record executives daughter
* Keesha Sharp as Party girl
* Todd Barry as Greasy
* Bob Costas as himself
* Fabian Celebre as Sleuthy
* Anthony Iozzo as Slobby
 

==Production==
Originally a Paramount Classics film titled Pootie Tang in Sine Your Pitty on the Runny Kine, the budget was increased and transferred to the main Paramount Pictures division. WTF with Marc Maron - Louis C.K. part 1  Louis C.K.|C.K. has stated that he was all but fired from the film during the editing phase. According to him, Ali LeRoi was hired to extensively re-edit the film.  Openly agreeing with Roger Eberts dismissive criticism that the movie should not have even been released, C.K. has said that the finished product, though containing parts he enjoyed, was far from his own vision. 

==Reception==
Critical reception was generally negative, with Rotten Tomatoes only gauging 29% positive reviews.  Roger Ebert gave it a half-star rating, criticizing it for excessive use of vulgar language and demeaning portrayal of women, describing it as a "train wreck" and finishing his review by bluntly stating "This film is not in a releasable condition".  Nathan Rabin at The Onion A.V. Club said Pootie Tang "borders on audience abuse" and "confuse  idiocy for absurdity and randomness for wit".  However, a few years later, fellow A.V. Club writer Scott Tobias revisited the film and included it in his New Cult Canon series, noting that "Pootie Tang repelled mainstream critics and audiences, but it holds an exalted status among alt-comedians and fans of subversive anti-humor|anti-comedy in general". 

 :
 

==Soundtrack==
 
A soundtrack containing hip hop and R&B music was released on June 16, 2001 by Hollywood Records. It peaked at #51 on the Top R&B/Hip-Hop Albums and #22 on the Top Soundtracks.

==In popular culture== Blockbuster instead of a copy of Pootie Tang. At the end of the horror film spoof, the aliens claim they watched the video tape that spurred their visits to Earth because they thought it was Pootie Tang. In Scary Movie 4, many characters argue about this film.
* In the TV show The Bernie Mac Show, Chris Rock (who appears in Pootie Tang) attends a poker game at Bernie Macs house. A player remarks that he really enjoyed Pootie Tang, to which Rock replies that "even   momma didnt see Pootie Tang." Also in the episode "The Talk" Wandas friend tells Bernie that she was in the video store and she saw Bernie on the cover of Pootie Tang. Bernie denies that he is who she saw, but she insists she is correct. Fat Tony has the lyric "...and I make the coochies stang like Im Pootie Tang" in his 2010 song "Luv It Mayne", also remixed by Das Racist.
* In Kanye Wests song, "School Spirit", off his album The College Dropout, he says "Thats how dude became/the young Pootie Tang, Tippy Tow", imitating Pootie Tangs style of speech.
* In Big Seans album Finally Famous, in his song "Dance (Ass)", he says "Drop that ass, make it boomerang/ Take my belt off, bitch Im Pootie Tang."
* In Snoop Doggs song "Bo$$ Playa" from his album Paid tha Cost to Be da Boss, he says "Holla at em Doggy Dogg, let him do his thing/Ok, Sa-da-tay, like my nigga Pootie Tang."
* On the Comedy Central show Tosh.0, Rock appeared as a "Guest Host", eventually becoming so frustrated with the regular viral videos that he says he would rather "Shoot Pootie Tang 2" than do the show permanently.
* Conan OBrien said many times on his show that Pootie Tang is "the greatest name of a film ever". 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 