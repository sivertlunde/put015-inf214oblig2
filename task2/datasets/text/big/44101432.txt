Ayanum
{{Infobox film 
| name           = Ayanum
| image          =
| caption        = Harikumar
| producer       = Sivan Kunnampilly John Paul (dialogues) Harikumar
| Madhu Srividya Innocent
| music          = MB Sreenivasan
| cinematography = Vipin Mohan
| editing        = G Murali
| studio         = Thushara Films
| distributor    = Thushara Films
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Harikumar and Innocent in lead roles. The film had musical score by MB Sreenivasan.   

==Cast==
  Madhu
*Srividya
*Mammootty Innocent
*Shobhana
*Thilakan
*Aloor Elsy
*K. P. A. C. Azeez
*Idavela Babu
*Jagannatha Varma
*Sreenath
*TM Abraham
*Thodupuzha Vasanthi
 

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by Mullanezhi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Prakaasha varshangalkkakale || K. J. Yesudas, KS Chithra || Mullanezhi || 
|-
| 2 || Swargasthanaaya Punya || S Janaki, Unni Menon, Latha Chitra || Mullanezhi || 
|}

==References==
 

==External links==
*  

 
 
 

 