Siin me oleme!
{{Infobox Film
| name           = Siin me oleme!
| image          = Siin me oleme (1978).jpg
| image_size     = 
| caption        = DVD Cover of digitally remastered version.
| director       = Sulev Nõmmik
| producer       = Eesti Telefilm
| writer         = Enn Vetemaa Sulev Nõmmik
| narrator       = 
| starring       = 
| music          = Ülo Vinter
| cinematography = 
| editing        = 
| distributor    = Eesti Telefilm
| released       = 1 January 1979
| runtime        = 67 min
| country        = USSR, Estonian SSR Estonian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 motifs from Estonian for Summer tourists).

==Influence==
Together with Viimne reliikvia, Noor pensionär and Mehed ei nuta, Siin me oleme! is one of the most memorable Estonian movies from the Soviet occupation era.   Even decades later, Smuuls catchphrases popularised by the movie, such as Were from Tallinn, well pay! ( ) are widely recognised and recycled by Estonian people. 

==Cast==
*Lia Laats as Kohviveski
*Ervin Abel as John
*Renate Karter as Lõke
*Karl Kalkun as Ärni
*Eva Meil as Ärnis wife
*Kadri Jäätma as Liina
*Sulev Nõmmik as Aadu
*Väino Puura as Mart
*Lauri Nebel as Timmu

==External links== Estonian Public Broadcasting Archives  
* 

 
 
 
 
 
 