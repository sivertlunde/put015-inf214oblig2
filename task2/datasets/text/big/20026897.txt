The Extraordinary Adventures of Mr. West in the Land of the Bolsheviks
{{Infobox film
| name           = The Extraordinary Adventures of Mr. West in the Land of the Bolsheviks
| image          = Mr West.jpg
| image_size     = 
| caption        = Film poster
| director       = Lev Kuleshov
| producer       = 
| writer         = Nikolai Aseyev Vsevolod Pudovkin
| narrator       = 
| starring       = Porfiri Podobed Boris Barnet Aleksandra Khokhlova Vsevolod Pudovkin
| music          = 1980s re-release: Benedict Mason 
| cinematography = Aleksandr Levitsky
| editing        = Aleksandr Levitsky
| distributor    = 
| released       = April 27, 1924
| runtime        = 94 min.
| country        = Soviet Union Russian and English intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1924 film Soviet director Lev Kuleshov. It is notable as the first Soviet film that explicitly challenges American stereotypes about Soviet Russia.   

==Plot== American ignorance of the Soviet Union. 
 American magazines. He takes along his cowboy friend Jeddie played by Boris Barnet for protection and as a companion.

However, on arriving in the USSR his briefcase is stolen, he gets separated from Jeddie and he falls into the hands of a group of thieves, including a run-down Countess (played by Aleksandra Khokhlova), who masquerade as counter-revolutionaries. The thieves play on Wests fears and engineer his abduction by crooks dressed up as caricature Bolshevik "barbarians." The thieves then "rescue" West from the clutches of these fictional Bolsheviks, extorting thousands of dollars from him along the way. 

In the end, it is the real Bolshevik police who rescue West, rather than his friend Jeddie (who meanwhile has hooked up with an American girl living in Moscow). West then takes a sightseeing tour of Moscow, where he sees that the Soviet government did not destroy all cultural landmarks, such as Moscow University and the Bolshoi Theater, as the thieves suggested. The film culminates in Mr.West watching a military parade with the policeman and concluding that the American view of the Soviet Union is wrong. He telegraphs his wife instructing her to hang a portrait of Lenin in his study.

==Credited Cast==
* Porfiri Podobed - Mr. West
* Boris Barnet - Jeddy - the cowboy
* Aleksandra Khokhlova - The Countess
* Vsevolod Pudovkin - Zhban

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 