Where Is Freedom?
{{Infobox film
 | name = Where Is Freedom? (Dovè la libertà?)
 | image = Where Is Freedom?.jpg
 | caption =
 | director = Roberto Rossellini
 | writer =  Vitaliano Brancati  Ennio Flaiano  Antonio Pietrangeli Vincenzo Talarico
 | starring = Totò Renzo Rossellini
 | cinematography =Aldo Tonti Tonino Delli Colli
 | editing =
 | producer =Carlo Ponti Dino De Laurentiis
 | distributor =
 | released = 1952
 | country = Italy
 | language = Italian
 | runtime = 84 min 
 }}
Where Is Freedom? ( ) is a 1954 Italian comedy-drama film directed by Roberto Rossellini.    
 
The film had a troubled production because, after shooting some scenes, Rossellini lost interest in the film and abandoned the set. The work was completed after about a year, mainly from Mario Monicelli, with some scenes also shot by Lucio Fulci and Federico Fellini. Despite that, Rossellini is the sole credited director of the film. Paolo Mereghetti. Dizionario dei Film 1996. Baldini & Castoldi, 1995. ISBN 88-859-8799-0. 

== Plot ==
Difficulties and troubles of an ex-convict. Embittered and disillusioned by life, he will soon plan his return to prison.

== Cast ==
*Totò: Salvatore Lo Jacono 
*Vera Molnar: Agnesina 
*Nita Dover:  maratoneta di danza 
*Franca Faldini: Maria 
*Leopoldo Trieste: Abramo Piperno 
*Antonio Nicotra: maresciallo 
*Salvo Libassi:  maresciallo #2 
*Giacomo Rondinella:  carcerato 
*Ugo DAlessio:  giudice
*Mario Castellani: pubblico ministero 
*Vincenzo Talarico: avvocato difensore

==References==
 

==External links==
* 
   
 

 
 
 
 
 
 
 


 
 