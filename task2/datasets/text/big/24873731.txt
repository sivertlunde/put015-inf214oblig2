Singam Puli
{{multiple issues|
 
 
}}

{{Infobox film
| name = Singam Puli
| image = Singam Puli.jpg
| caption = 
| director = Sai Ramani
| producer = S. Parthiban S. Srinivasan
| writer = Sai Ramani Divya Spandana Santhanam
| music = Mani Sharma
| cinematography = Balasubramaniem
| editing = Venky
| studio = 
| distributor = 
| released =  
| runtime = 160 minutes
| country = India
| language = Tamil
| budget = 
}}
 masala film Divya Spandana Santhanam plays a pivotal role. The film was released on 4 March 2011.  Film was dubbed into Telugu as Simham Puliand in hindi as "Do ka tarka" . 

==Plot== Kuyili and Ponvannan), a daughter and twin sons—Shiva (Jiiva), who works in a fish-market and Ashok Kumar (Jiiva, again), who is a lawyer. 
Shiva is a typical hero who attacks baddies with his bare fists; a rugged man wholl brook no injustice. Devout Ashoks goodness is a facade that hides his evil nature. Both have romantic interests; Shweta (Divya Spandana) is Shivas long-time sweetheart. Ashoks life is one long, lustful journey as he charms every woman he meets into his bed.
This is where the tale picks up. Shiva can do nothing right as far as his parents are concerned; they trust the Ashok and always misunderstand Shiva’s righteous anger and his every attempt to show up his Machiavellian twin fails. Ashok, on the other hand, is pally with the local goons and uses his brains to assist them in their nefarious activities. Matters come to a head when Ashok lures a girl called Gayatri (Honey Rose) with false pretences, and eventually causing her to suicide.
Shiva lodges a complaint on Ashok but this fails as he couldn’t prove that whether it was him or Ashok who did the crime due to their faces. Shiva comes to know that a security man who saw Ashok pushing Gayatri from the roof. He tells the security man to meet him in the evening. But he is killed by Ashok as the security man saw him in the evening and thought him as Shiva. Shiva comes to know this thus begins a cat-and-mouse game between the siblings.
This causes anger to the younger one and he tries to kill his elder brother. He employs a gang to kill his elder brother. Eventually the elder brother knows this plot and confronts him. During the fight the younger brother get killed by his own man. The elder brother walks with his father who for the first time accepts him.

==Cast==
* Jiiva as Shiva and Ashok Kumar
* Divya Spandana as Shwetha
* Honey Rose as Gayatri Santhanam as Bujji Babu
* Ponvannan as Shiva and Ashok Kumars Father Pandu as Shwethas Father Kuyili as Shiva and Ashok Kumars Mother
* Meera Krishnan as Shwethas Mother

==Soundtrack==
All songs scored by Mani Sharma.
{{Track listing
| extra_column = Performer(s)
| title1 = Figaru | extra1 = Naveen
| title2 = Poove Poove | extra2 = Rahul Nambiar
| title3 = Varrale | extra3 = Ranjith (singer)|Ranjith, Saindhavi
| title4 = Kangalal | extra4 = Karthik (singer)|Karthik, Rita
| title5 = Ullasame | extra5 = Ranjith, Janani
| title6 = Naadilla | extra6 = Mukesh
}}

==Critical reception==
Rohit Ramachandran in nowrunning.com gave the film 2/5 stars stating that "Singam Puli is a mildly entertaining film that is worth checking out if youre looking for popcorn escapism". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 