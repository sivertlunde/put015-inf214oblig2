The Showdown (1940 film)
{{Infobox film
| name           = The Showdown
| image          = The Showdown (1940 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Howard Bretherton
| producer       = Harry Sherman 
| screenplay     = Donald Kusel Harold Daniel Kusel
| story          = Jack Jungmeyer William Boyd Russell Hayden Britt Wood Morris Ankrum Jan Clayton Wright Kramer Donald Kirke
| music          = John Leipold
| cinematography = Russell Harlan
| editing        = Carroll Lewis 
| studio         = Harry Sherman Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film William Boyd, Russell Hayden, Britt Wood, Morris Ankrum, Jan Clayton, Wright Kramer and Donald Kirke. The film was released on March 8, 1940, by Paramount Pictures.  

==Plot==
 

== Cast ==		 William Boyd as Hopalong Cassidy
*Russell Hayden as Lucky Jenkins
*Britt Wood as Speedy McGinnis
*Morris Ankrum as Baron Rendor
*Jan Clayton as Sue Willard 
*Wright Kramer as Colonel Rufe White
*Donald Kirke as Harry Cole
*Roy Barcroft as Bowman Eddie Dean as The Marshal
*Kermit Maynard as Henchman Johnson
*Walter Shumway as Henchman Snell
*The Kings Men as Singing Cowhands 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 