Stable Companions
{{Infobox film
| name           = Stable Companions
| image          =
| caption        = Albert Ward 
| producer       = G.B. Samuelson
| writer         = Walter Summers Robert English   Arthur Pusey
| music          = 
| cinematography = 
| editing        = 
| studio         = G.B. Samuelson Productions
| distributor    = Jury Films 
| released       = June 1922
| runtime        = 5,960 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent sports Albert Ward Robert English. It is set in the world of horse racing.

==Cast==
* Clive Brook as James Pilkington 
* Lillian Hall-Davis    Robert English as Sir Horace Pilkington 
* Arthur Pusey    Fred Mason  
* James Wigham Thomas Walters

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

==External links==
* 
 
 
 
 
 
 
 
 

 
 