Summer of the Seventeenth Doll (film)
 
{{Infobox film name           = Summer of the Seventeenth Doll image          = "Summer_of_the_Seventeenth_Doll_(1959).jpg caption        =  producer  Leslie Norman director       = Leslie Norman  writer         = John Dighton the play by Ray Lawler starring       = Ernest Borgnine Anne Baxter Angela Lansbury John Mills music          = Benjamin Frankel cinematography = Paul Beeson editing        = Gordon Hales
| studio = Hecht Hill Lancaster (Australia) distributor    =  United Artists released       = 2 December 1959 (Australia) 16 December 1961 (USA) runtime        = 94 min. country        = Australia United Kingdom United States language       = English  budget         = 
}} Leslie Norman and is based on the Ray Lawler play Summer of the Seventeenth Doll. In the USA the film was released under the title Season of Passion.

==Plot==
Queensland sugarcane cutters Roo and Barney spend the off season in Sydney each year, seeing their girlfriends. For sixteen years Roo has spent the summer with barmaid Olive, bringing her a kewpie doll, while Barney romances Nancy. In the seventeenth year, Barney arrives to find that Nancy has married; however Olive has arranged a replacement, manicurist Pearl. Roo has had a bad season, losing his place as head of the cane cutting team to a younger man, Dowd.

Barney tries to smooth things over between Roo and Dowd, who falls for Bubba, a girl who has grown up with the cane cutters. Barney leaves to work with Dowd until Roo proposes to Olive, who declines. With this Roo and Barney leave together to find somewhere new to go.

==Cast==
*Ernest Borgnine as Roo
*Anne Baxter as Olive
*John Mills as Barney
*Angela Lansbury as Pearl
*Vincent Ball as Dowd
*Ethel Gabriel as Emma
*Janette Craig as Bubba
*Deryck Barnes as Bluey
*Alan García as Dino

==Play==
 
Summer of the Seventeenth Doll is a pioneering Australian play written by Ray Lawler and first performed at the Union Theatre in Melbourne, Australia on 28 November 1955. The play is almost unanimously considered by scholars of literature to be the most historically significant in Australian theatre history, openly and authentically portraying distinctly Australian life and characters. It was one of the first truly naturalistic "Australian" theatre productions.

==Film adaptation== The Devils Disciple for the company. Dighton:
 I intend to stick to the play as closely as possible. The two barmaids and the old woman are good characters, but a little more colour is needed in the development of the relationship between the two cane-cutters. In its construction Lawlers play runs downhill all the way. This, I feel, was a weakness. I intend to give the film version what I regard as a necessary build-up to a dramatic peak in the middle.     Leslie Norman Eureka Stockade The Shiralee in Australia) directed and Ernest Borgnine played the lead. 

The film was retitled Season of Passion for the American market.  This decision was severely lamented by some fans of the play, whose complaints were rooted in three essential criticisms:
*The "Americanization" of the text, namely the casting of American actor Borgnine, who played his character (Roo) with an American accent. Others have thought the film was a recruiting film for migrants with the Englishman John Mills as Barney and Alan Garcia as Dino, an Italian friend and fellow cane cutter who does not feature in the play. The female leads are played by Anne Baxter and Angela Lansbury, though the film features many Australian actors. Bondi Beach and Luna Park Sydney rather than the confines of the then working-class Melbourne suburb of Carlton, Victoria|Carlton.
*The drastic changes to key plot points, namely the alternate, "happy" ending that the 1959 film adaptation entailed. This alternate ending was considered by some to be representative of a dire misunderstanding of the play and its message, and by others an attempt to make the film an international success at the box office and critical acclaim similar to the kitchen sink realism of Marty. The producers also added a comedy sequence where a young girl attempted to trick Roo in a tent at Luna Park.

Shooting began in December 1958, taking place at Pagewood Studios and Artransa Studios. There were some location scenes at Luna Park and Bondi Beach. For one scene, Sydney residents on the shore were asked to leave their lights burning to provide a romantic backdrop to the action. Filming wound up in February. 

==References==
 
* 

==External links==
*  at Oz Movies
 

 
 
 
 
 
 
 
 