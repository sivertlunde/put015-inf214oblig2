Static (film)
{{Infobox Film
| name           = Static
| image          = Static DVD cover.jpg
| image_size     = 
| caption        = DVD cover
| director       = Mark Romanek
| producer       = Amy Ness
| writer         = Keith Gordon Mark Romanek
| starring       = Keith Gordon Amanda Plummer Bob Gunton
| cinematography = Jeff Jur
| editing        = Emily Paine
| studio         = NFI Productions
| released       = September 10, 1986
| runtime        = 93 minutes
| country        = United States
| language       = English
}}

Static is an American Comedy-drama|comedy-drama film directed by Mark Romanek. The film stars Keith Gordon, Amanda Plummer, and Bob Gunton, and was released in 1986 by NFI Productions. It was shot in Page, Arizona and Lake Powell, Nevada.
 directorial debut, before he went on to direct numerous music videos for much his career. However, Romanek has since disowned the film, claiming that his subsequent film One Hour Photo (2002), as his feature film directorial debut.  

== Plot ==
A quirky, out-of-of place worker (Keith Gordon) at a crucifix factory in the Bible Belt invents a device he claims can show pictures of Heaven. Discouraged and confused by the inability of those around him to see anything but a screenful of static, he charismatically hijacks a bus of friendly elderly people in order to get media attention for his invention.

==Cast==
* Keith Gordon.....Ernie Blick
* Amanda Plummer.....Julia Purcell
* Bob Gunton.....Frank
* Janice Abbott.....Sonya
* Reathel Bean.....Fred Savins
* Kitty Mei-Mei Chen.....Li
* Barton Heyman.....Sherriff William Orling
* Jane Hoffman.....Emily Southwick
* Lily Knight.....Patty
* Joel Krehbeil.....Deputy Tom Terrence
* Eugene Lee.....Dale
* Jack Murakami.....North
* Mike Murakami.....South
* Uma Ridenhour.....Sarah
*

==Award nominations==
{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;" Year
! Award
! Result
! Category
! Recipient
|- 
| 1986 || Sundance Film Festival || Nominated || Grand Jury Prize (Dramatic) || 
Mark Romanek 
|}

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 