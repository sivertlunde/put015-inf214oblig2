Sheila Levine Is Dead and Living in New York
{{Infobox film
| name           = Sheila Levine Is Dead and Living in New York
| image          = Sheila Levine poster.jpg
| image_size     =
| alt            =
| caption        = Movie Poster
| director       = Sidney J. Furie
| producer       = Harry Korshak
| screenplay    = Kenny Solms
| based on   =     
| narrator       =
| starring       = Roy Scheider Jeannie Berlin Rebecca Dianna Smith
| music          =
| cinematography = Donald M. Morgan
| editing        = Argyle Nelson Jr.
| studio         =
| distributor    = Paramount Pictures
| released       = 16 May 1975 (USA)
| runtime        =  113 min.
| country        = United States English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Sheila Levine Is Dead and Living in New York is a 1975 film directed by Sidney J. Furie. The film was written by Kenny Solms, based on the novel by Gail Parent. The film was shot on location in New York City. 

==Tagline==
"Sheila Levine is every single girl who has had to attend her younger sisters wedding."

==Plot==
Painfully shy Sheila Levine moves from Pennsylvania to New York City where she shares an apartment with an extroverted, socially active roommate. When Sheila goes to a nightclub with her roommate, she meets Sam, a doctor who is looking for a one-night stand. She is very attracted to Sam, but he is interested in her vivacious roommate. Sheila goes back to Pennsylvania for a while and then returns to New York City with the intent of winning Sam over, but finds that he and her roommate are engaged.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Jeannie Berlin || Sheila Levine
|-
| Roy Scheider || Sam Stoneman
|-
| Rebecca Dianna Smith || Kate
|-
| Janet Brandt || Bernice
|-
| Sid Melton || Mannie
|- Charles Woolf || Wally
|-
| Leda Rogers || Agatha
|-
| Jack Bernardi || Uncle Herm
|}

==Critical reception==
Vincent Canby of The New York Times did not care for the film although he liked the novel on which the film was based: Something disastrous happened to the heroine of Gail Parents funny novel, Sheila Levine Is Dead and Living in New York, on her way to the silver screen... This Sheila is so aggressively naive and dumb, when it suits the purposes of the comedy, that its quite impossible to believe that even her family could stand her, to say nothing of the Mr. Right with whom the film provides her. 
}}
Despite an advertising campaign featuring a poster with the quote "Jeannie Berlin triumphs!" from one film critic, the movie is given a BOMB rating in Leonard Maltins annual guide to movies ratings book, panned with the sentence, "Dead is right."

==Soundtrack==
*"Love Me or Love People" - Composed by Michel LeGrand

==References==
 
*List of American films of 1975

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 