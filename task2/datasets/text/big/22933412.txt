Grown Ups (film)
 Grownup}}
{{Infobox film
| name = Grown Ups 
| image = grownupsmovie.jpg
| director = Dennis Dugan
| producer = Jack Giarraputo Adam Sandler Fred Wolf Kevin James Chris Rock David Spade Rob Schneider
| music = Rupert Gregson-Williams
| cinematography = Theo van de Sande
| editing = Tom Costain
| studio = Happy Madison Productions Relativity Media
| distributor = Columbia Pictures
| released =  
| runtime = 102 minutes  
| country = United States
| language = English
| budget = $80 million 
| gross = $271.4 million   Box Office Mojo 
}}
 Kevin James, Chris Rock, David Spade, and Rob Schneider. The film tells a story of five childhood friends who won their junior high school basketball championship in 1978. They reunite three decades later to mourn the death of their coach. Meeting at a lakeside cottage they rented when they were young, the friends also re-connect with each other, their spouses, and their children.

Grown Ups was produced by Sandlers production company Happy Madison Productions and was distributed by Columbia Pictures.  Sandler, Rock, Schneider, and Spade all joined the cast of Saturday Night Live in the Saturday Night Live (season 16)|1990&ndash;1991 season; supporting cast including Colin Quinn, Maya Rudolph, Tim Meadows, and Norm Macdonald have also been SNL cast members.

== Plot == Kevin James) claims he is now a co-owner of a lawn furniture company, is married to Sally (Maria Bello) and has two children, Donna and Bean (Ada-Nicole Sanger and Morgan Gingerich). Much to Erics chagrin, Bean continues to nurse on Sally. Kurt McKenzie (Chris Rock) is a stay-at-home father who is married to Deanne (Maya Rudolph), the primary breadwinner of the family, and has two children, Andre and Charlotte (Nadji Jeter and China Anne McClain). Deanne is pregnant with another child and her mother (Ebony Jo-Ann) also lives with the family. Rob Hilliard (Rob Schneider) has been divorced three times and has daughters Jasmine, Amber, and Bridget (Madison Riley, Jamie Chung, and Ashley Loren ) from those marriages. His current wife, Gloria (Joyce Van Patten), is 30 years older than him. Marcus Higgins (David Spade) is a slacker and lothario. All five friends regularly harass with each other in comedic fashion throughout the film: Lenny for being rich; Eric for being overweight; Kurt for being skinny and not being more useful; Rob for his way of saying "Maize!" and for having a much older wife; and Marcus for being sexually irresponsible.
 4th of July weekend for his friends to stay at. However, Lenny cannot stay for the whole weekend because Roxanne has a fashion show in Milan. While at the lake house, Lenny is annoyed in which their kids would rather play video games than spend time outdoors, so that he and the other friends are forcing their kids to play outside during their stay instead. Eric has a small incident on a rope swing and injures a bird. At a local restaurant, Lenny talks to his old nemesis, Dickie Bailey (Colin Quinn), who is still upset at Lenny because he allegedly had his foot out of bounds when he made the long-ago game winning shot. Dickie challenges him and his friends to a rematch, but Lenny declines after noting that Dickie is decidedly out of shape. The next day, the five friends spread Buzzers ashes into the woods. Rob becomes depressed during this event, lamenting his failed marriages, and later says his three daughters from the past marriages are coming. After having to deal with Jasmine, Rob goes and hangs out with his friends, who were fishing. After making some jokes on Rob, the others elect to cheer him up with a game of arrow roulette. Rob remains in the circle the longest, making him the winner, but the arrow hits directly through Robs foot. When Gloria tries to use a maize-covered poultice, Rob snaps at Gloria. Lenny gets the kids interested in talking on cup-phones and Roxanne accidentally tells Becky that she is the "Tooth Fairy". Happy that they are enjoying the same kind of fun he had as a child, Lenny installs an extensive cup-phone network in the house.

Roxanne ultimately figures out that it is more important to stay at the lake house than going to Milan. The five friends decide to go to a water park, where Bean learns to drink milk out of a carton and Marcus flirts constantly with Jasmine and Amber, having bought them skimpy bikinis. Rob kicks a ride attendant down a water slide, who insults Bridget for being less attractive than her sisters. Eric ignores Donnas warning about a chemical in the kiddie pool that turns urine blue, and chaos results when he and his friends urinate in the pool. The spouses try to attract a hunk, but he is laughed off due to his high-pitched voice. Later, Lenny and the group go to the zip line and meet Dickie again, this time with his own group of friends, including Wiley (Steve Buscemi), who ends up in the hospital after sliding down the zip line by his feet. Lenny teaches his son how to shoot a perfect shot during basketball. Afterwards, the friends end the night by sharing their dance with their spouses. The next day, Roxanne picks up Lennys phone and confronts him on lying about canceling their flight trip before she agreed on staying for the family instead of going to Milan. Eventually, everyone starts to tell the truth about their feelings and lives. On their final day at the lake house, Lenny and his buddies are challenged once again to a basketball rematch by Dickie. At the game-deciding shot, Lenny purposely misses in order to allow Dickie and his team to win. Before the end of the film, Marcus plays another game of arrow roulette, this time with an entire crowd of people, and everyone takes off with Wiley accidentally getting his foot impaled by the arrow.

== Cast ==
* Adam Sandler as Lenny Feder
** Michael Cavaleri as Young Lenny Kevin James as Eric Lamonsoff
** Andrew Bayard as Young Eric
* Chris Rock as Kurt McKenzie
** Jameel McGill as Young Kurt
* David Spade as Marcus "Higgy" Higgins
** Kyle Brooks as Young Marcus
* Rob Schneider as Rob Hilliard
** Joshua Matz as Young Rob
* Salma Hayek as Roxanne Feder
* Maria Bello as Sally Lamonsoff
* Maya Rudolph as Deanne McKenzie
* Joyce Van Patten as Gloria Noonan
* Blake Clark as Coach Robert "Buzzer" Ferdinando
* Victoria Cyr, Niece of Coach Buzzer 
* Di Quon as Rita
* Steve Buscemi as Wiley
* Colin Quinn as Dickie Bailey
* Hunter Silva as Young Dickie
* Lisa M.(Seitz) Francis as Baileys wife
* Tim Meadows as Malcolm
* Ebony Jo-Ann as Deannes Mother
* Madison Riley as Jasmine Hilliard
* Jamie Chung as Amber Hilliard
* Ashley Loren as Bridget Hilliard
* Jake Goldberg as Greg Feder
* Cameron Boyce as Keith Feder
* Alexys Nicole Sanchez as Becky Feder
* Ada-Nicole Sanger as Donna Lamonsoff
* Frank and Morgan Gingerich as Bean Lamonsoff
* Nadji Jeter as Andre McKenzie
* China Anne McClain as Charlotte McKenzie
* Dan Patrick as Norby the Ride Guy
* Tim Herlihy as Pastor
* Norm Macdonald as Geezer
* Jonathan Loughran as Robideaux
* Connor Panzner as Young Robideaux
* Dennis Dugan as Basketball Referee

== Production ==
Filming commenced in Southborough, Massachusetts on May 18, 2009, but most of the filming was done in Essex, Massachusetts, Water Wizz at Wareham, Massachusetts, as well as some scenes shot in Marblehead, Massachusetts on the exclusive Marblehead Neck. 

== Music ==

The score to Grown Ups was composed by Rupert Gregson-Williams, who recorded his score with a 55-piece ensemble of the Hollywood Studio Symphony at the Newman Scoring Stage at 20th Century Fox.   

== Release ==

=== Box office ===
The film was a box office success grossing $162,001,186 in the United States and $109,429,003 in other countries, with a worldwide gross of $271,430,189.  Happy with the gross, Adam Sandler showed his appreciation by buying brand-new Maserati sports cars for his four co-stars.       The film won at the 2011 MTV Movie Awards for the "Best Line from a Movie" category, which it won for the line "I want to get chocolate wasted!", delivered by Becky, played by Alexys Nycole Sanchez.

=== Critical reception ===
  
The film was heavily panned by critics, citing mostly the acting performances and lack of substantial plot. Review aggregation website Rotten Tomatoes give the film a score of 10% based on 164 reviews, with an average score of 3.4/10, and the sites consensus is: "Grown Ups cast of comedy vets is amiable, but theyre let down by poor direction and the scattershot, lowbrow humor of a stunted script."  Metacritic awarded the film an average score of 30 out of 100 based on 32 reviews. 

=== Awards ===
  
 .

=== Home media ===
Grown Ups was released on DVD and Blu-ray Disc on November 9, 2010.

== Sequels ==
  Kevin James, Chris Rock, David Spade, Salma Hayek, Maya Rudolph, Maria Bello and Steve Buscemi reprised their roles, except Rob Schneider. New cast includes Andy Samberg, Taylor Lautner and Patrick Schwarzenegger. The sequel follows Lenny Feder as he relocates his family back to the small town where he and his friends grew up.  Much like its predecessor, Grown Ups 2 was heavily panned    and was also a box office hit.   

In an interview, David Spade stated possibility for a Grown Ups 3.

== References ==
 

== External links ==
 

*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 