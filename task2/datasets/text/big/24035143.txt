Romance on the Run
 
{{Infobox film
| name           = Romance on the Run
| image          =File:Romance on the Run 1938.jpg
| image_size     =
| caption        =Film poster
| director       = Gus Meins
| production company = Republic Pictures Corporation
| Associate Producer     = Herman Schlom Eric Taylor (story) Jack Townley (writer)
| narrator       =
| starring       = See below
| art director  = John Victor Mackay
| musical director  = Alberto Colombo
| song by = Jack Lawrence (musician) and Peter Tinturin Ernest Miller
 supervising editor = Murray Seldeen
| editing        = Ernest J. Nims
| costumes   = Irene Saltern
| distributor    = Republic Pictures Corporation
| Copyright = Republic Pictures Corporation
| released       =  
| runtime        = 53 minutes (edited version) 68 minutes (edited version)
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Romance on the Run is a 1938 American film directed by Gus Meins.

== Plot Summary ==

A very expensive necklace, also known as "Czarinas Tears, are robbed from Phelpss jewelry shop, The next morning Phelps (Granville Bates) orders insurance company manager J. W. Ridgeway (Andrew Tombes) to retrieve it. Mr. Ridgeway is convinced that the insurance policy of Mr. Phelps has not been renewed as he doesnt think Phelps deserves it. But his young secretary  Miss Harrison (Patricia Ellis) had renewed it automatically about a month before.
So Mr. Ridgeway has to solve the problem. Lieutenant Eckhard (William Demarest) enters the scene and demands that Mr. Ridgeway leaves the case entirely to the police. But Mr. Ridgeway has his private investigator Mr. Drake (Donald Woods (actor)). 
Drake meets singer Lily Lamont (Grace Bradley) in a night club and make her speak to Mr. Cooper (Craig Reynolds (actor)), who acts as if he didnt knew her. But in fact as Drake leaves them, they speak as good acquainted people.
Meantime Drake goes into the apartment of Charlie Cooper to search for the necklace. Cooper discovers him, and Drake manages to find the necklace even though Cooper tries to disguise him. Enthusiastic he returns to Mr. Ridgeway, collects his fee (10.000 $) makes Lieutenant be even more sour about him and leaves very happy. Soon after Mr. Phelps arrives expecting to find his Zarina necklace. But unfortunately its only an imitation. Mr. Ridgeway has to leave for the train station and Miss Harris reacts very fast and follows him as she gets to know, that the case is not solved.
A chase first at the train station, then in the train, finally at a family in the middle of nowhere leads them - Drake and Miss Higgins - to find the true necklace, and with it between the two a romantic story has evolved. They manage to return in time into the office of Mr. Ridgeway, pretending that nothing happened, while the true necklace is going to be handed over to Mr. Phelps. The Police Lieutnant is the idiot of the wit and has to hand over the phony necklace, while Dale shows with her eyes where the true one is lying. Her love, Barry Drake can hand over the true "Tsarina Tears" Necklace, Whitey is proud of his master, and Dale and Barry have a lunch engagement at the wedding license office.

== Cast ==
*Donald Woods (actor) as Mr. Barry Drake
*Patricia Ellis as Miss Dale Harrison
*Grace Bradley as Lily Lamont
*Edward Brophy as Whitey Whitehouse
*William Demarest as Lieutenant Eckhard
*Craig Reynolds (actor) as Charlie Cooper
*Andrew Tombes as J.W. Ridgeway
*Granville Bates as Mr. Phelps
*Bert Roach as Happy Drunk
*Georgia Simmons as Ma Hatfield
*Leon Weaver as Pappy Hatfield
*Edwin Maxwell as Mondoon
*Jean Joyce as Dolly
 

== References ==
 

== External links ==
* 
* 


 
 
 
 
 
 
 
 