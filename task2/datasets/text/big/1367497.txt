Running on Empty (1988 film)
{{Infobox film 
| name           = Running on Empty 
| image          = Running on Empty movie poster.jpg
| caption        = Theatrical release poster
| director       = Sidney Lumet 
| producer       = Griffin Dunne  Amy Robinson 
| writer         = Naomi Foner
| narrator       = 
| starring       = River Phoenix Judd Hirsch Christine Lahti Martha Plimpton 
| music          = Tony Mottola 
| cinematography = Gerry Fisher
| editing        = Andrew Mondshein
| distributor    = Warner Bros. 
| released       =  
| runtime        = 111 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = $2,835,116
}} counterculture couple on the run from the FBI, and how one of their sons starts to break out of this fugitive lifestyle.
 Academy Award best supporting actor for his role in the film; Naomi Foner was nominated for Best Original Screenplay. Phoenix was nominated for Best Performance by an Actor in a Supporting Role at the Golden Globes; Lahti was nominated for Best Performance by an Actress. The film was nominated for Best Director and Best Motion Picture (Drama), and it won a Golden Globe for Best Screenplay. Plimpton was nominated for a Young Artist Award for Best Young Actress in a Motion Picture.

The film marked the second time Phoenix and Plimpton would play one anothers romantic interest, having co-starred in the film The Mosquito Coast two years earlier.

==Plot==
The story revolves around parents Annie and Arthur Pope (Lahti and Hirsch) who in the 1970s were responsible for the anti-war protest bombing of a napalm laboratory. The incident accidentally blinded and paralyzed a janitor who wasnt supposed to be there. Theyve been on the run ever since, relying on an underground network of supporters who help them financially. At the time of the incident, their son Danny (Phoenix) was two years old. As the film begins, he is in his late teens, and the family (along with younger son Harry) are again relocating and assuming new identities.

As the film progresses, Dannys overwhelming talent as a pianist catches the attention of his music teacher at school. The teacher begins to pry into Dannys personal life, particularly questioning why records from his previous school are unobtainable. While he pushes Danny to audition for Juilliard, Danny also falls in love with Lorna (Plimpton), the teachers teenage daughter.

As the pressure to have his own life and realize his own dreams intensifies, Danny reveals his family secret to Lorna. Meanwhile, Annie finds out about Dannys audition, and begins to come to terms with the fact that she must let her son go and find his own way. This does not sit well with Arthur, even as Annie risks their safety to contact her estranged father and arrange a home and life for Danny if they should decide to leave him behind.

In the end, when Arthur hears on the radio that one of their underground colleagues has been shot and killed running from the authorities, he realizes that it is better for his son to pursue his dreams than to continue living a dangerous life on the run for which he bears no responsibility. The family leaves Danny behind and heads off for their next identity in a new town.

==Cast==
* Christine Lahti as Annie Pope
* Judd Hirsch as Arthur Pope
* River Phoenix as Danny Pope
* Martha Plimpton as Lorna Phillips
* Ed Crowley as Mr. Phillips
* Jonas Abry as Harry Pope
* L. M. Kit Carson as Gus Winant
* Steven Hill as Donald Patterson
* Augusta Dabney as Abigail Patterson

==Real-life inspirations== John Simon states that the characters bombing of a napalm research facility was inspired by the Sterling Hall bombing of 1970. 

==Reaction==
Running on Empty was released on September 9, 1988 in 22 theaters where it grossed $215,157 on its opening weekend. It went on to make $2.8 million in North America.   

Film critic Roger Ebert gave the film four out of four stars and called it "one of the best films of the year."    In her review for the New York Times, Janet Maslin wrote, "The courtship between Danny and Lorna is staged especially disarmingly, with Mr. Phoenix and Miss Plimpton conveying a sweet, serious and believably gradual attraction."    Newsweek magazines David Ansen wrote, "A curious mix of soap opera and social history, Lumets film shouldnt work, yet its fusion of oddly matched parts proves emotionally overpowering. You have to be pretty tough to resist it."     However, Hal Hinson, in his review for the Washington Post wrote, "Running on Empty doesnt make much sense for the title of the movie ... but it does work as a description of the director. Sidney Lumet may be the laziest major director working today."   

The film has an 85% rating on Rotten Tomatoes.

== References ==
 

==External links==
* 
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 