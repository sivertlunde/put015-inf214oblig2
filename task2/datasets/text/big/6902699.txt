Pennies from Heaven (1981 film)
{{Infobox film
| name           = Pennies from Heaven
| image          = Pennies from Heaven.jpg
| caption        = Theatrical release poster
| director       = Herbert Ross
| producer       = Rick McCallum Herbert Ross Nora Kaye
| writer         = Dennis Potter
| starring       = Steve Martin Bernadette Peters Christopher Walken Jessica Harper Vernel Bagneris
| music          = Ralph Burns Con Conrad Marvin Hamlisch Billy May
| cinematography = Gordon Willis
| editing        = Richard Marks
| Studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = United States dollar|$22,000,000 
}}
 1978 BBC Depression era On Golden Pond. The film starred Steve Martin, Bernadette Peters, Christopher Walken and Jessica Harper. The director was Herbert Ross and the choreographer was Danny Daniels. Variety Film Reviews|Variety film review; December 9, 1981. 

==Plot==
 
 
In 1934, Chicago sheet-music salesman Arthur Parker (Martin) is having a hard time, both in his business and at home with his wife Joan (Harper). His business is failing and Joan is not amorous enough for Arthur and refuses to give him the money she inherited from her father to start his own business.

Arthurs dream is to live in a world that is like the songs he tries to sell. He is refused a bank loan, although he fantasizes that he gets it to a song and dance routine of "Yes! Yes!". In his travels, Arthur meets a shy, beautiful but plain school teacher, Eileen (Peters). Arthur expresses his instant attraction by lip-synching to the song "Did You Ever See a Dream Walking?", as Eileen, converted to a brighter version of herself, dances. He convinces her that he loves her and they embark on a short affair, but Arthur leaves her and returns to Joan, who, desperate to keep him, agrees to give him the money he wanted. Arthur denies his affair, though Joan is sure he is lying, singing "Its a Sin to Tell a Lie".

Eileen is optimistic about her affair with Arthur, imagining leading her class in "Love Is Good for Anything That Ails You", but she becomes pregnant and is fired. With nowhere to go, she is then taken in by stylish pimp Tom (Walken). Eileen is attracted to Toms "badness" (the same way she was attracted to Arthur), and fantasizes him singing "Lets Misbehave" to her. The attraction ends there, however, as it is quite clear that Tom means business, and arranges for her to have an abortion.

When Arthur meets Eileen again—as "Lulu"—she is now a prostitute and has adopted an aggressive manner. They resume their romance, and Eileen leaves Tom and her sordid life. Impulsively, Arthur convinces her to run away together. Having failed to sell his business, Arthur and Eileen break into the store one night and trash it, smashing its phonograph records (except for "Pennies From Heaven"). To supplement their income, Eileen keeps prostituting in spite of Arthurs objections.
 Pennies from The Glory of Love", with Arthur saying, "Weve worked too hard not to have a happy ending."

==Cast==
*Arthur Parker — Steve Martin
*Eileen (a.k.a. "Lulu") — Bernadette Peters
*Tom — Christopher Walken
*Joan Parker — Jessica Harper
*Accordion man — Vernel Bagneris
*Mr. Warner — John McMartin
*Detective — John Karlen
*Counterman — Duke Stroud

==Production==
This was Steve Martins first dramatic role in a film. Martin had watched the original miniseries and considered it "the greatest thing   ever seen".    He trained for six months learning to tap dance. Christopher Walken trained as a dancer as a young man and he was able to use his dancing skills in the film.

According to a 1990 article in The Times, MGM had Dennis Potter rewrite the script 13 times and required him to buy back his copyright from the BBC, for which he paid BBC "something over $100,000". In addition, MGM prohibited broadcast of the BBCs original production for ten years. Around 1989, at the prompting of Alan Yentob, the controller of BBC2, producer Kenith Trodd was able to buy back the rights from MGM for "a very inconsiderable sum." In February 1990, the BBC rebroadcast the original Pennies from Heaven serial for the first time since 1978.

In the same Times article, Trodd stated that Bob Hoskins and Cheryl Campbell, the stars of the original series, "were terribly upset that they werent considered for the film. I think they still blame Dennis and me in some way, but there was no way to argue the point with MGM."

The style of the movie balances the drab despair of the Depression era and the characters sad lives with brightly colored dream-fantasy lavish musical sequences. The characters break into song and dance to express their emotions. For example, Eileen turns into a silver-gowned torch singer in her school-room, with her students lip-synching and dancing ("Love Is Good for Anything That Ails You"). Tom seduces Eileen with a tap dance/striptease routine on top of a bar ("Lets Misbehave"). Arthur and Eileen go to a movie (Follow the Fleet) and wind up dancing in formal wear, first with, then in, a Fred Astaire-Ginger Rogers musical number from the film, "Lets Face the Music and Dance". All the songs are lip-synched except Martin singing/speaking the title song at the end, but Arthur, Tom, and Eileen dance.

Four paintings are recreated as  , and New York Movie and Nighthawks by Edward Hopper. Three of the four were painted after 1934, when the movie takes place, and all depict scenes in New York City rather than the Chicago setting of the movie.

==Response==
The film grossed slightly more than $9 million at the box-office against a budget of $22 million. 
  as "Eileen" "brought a cocky attitude and a sexy exuberance to the musical numbers." ]]
When asked in  , and fans were confused to see Martin in a serious role. "You just cant do a movie like Pennies from Heaven after you have done The Jerk", Martin said in a BBC interview.

The film received good reviews, and currently has an 84% on Rotten Tomatoes with the consensus, A complicated little musical, Pennies from Heaven is a dazzling, tragic spectacle.  The film was given a rapturous review by   as Best Motion Picture Actress in a Comedy or Musical for her role as Eileen Everson, a schoolteacher turned prostitute.    A review of the DVD reissue asserted, "Peters brought a cocky attitude and a sexy exuberance to the musical numbers." Hatch, George.  . DVDverdict.com, accessed July 22, 2011 

Fred Astaire, who was powerless to prevent the reuse of his old footage, detested the film: "I have never spent two more miserable hours in my life. Every scene was cheap and vulgar. They dont realize that the thirties were a very innocent age, and that   should have been set in the eighties – it was just froth; it makes you cry its so distasteful."   

==Awards and nominations==

;Academy Awards   
*Best Costume Design – Bob Mackie (nominated) Richard Tyler and Al Overton, Jr. (nominated)
*Best Writing, Screenplay Based on Material from Another Medium –Dennis Potter (nominated)
;Boston Society of Film Critics Awards
*Best Cinematography – Gordon Willis (WON)
;Golden Globes
*Best Motion Picture Actress, Comedy/Musical – Bernadette Peters (WON)
*Best Motion Picture, Comedy/Musical (nominated)
*Best Motion Picture Actor, Comedy/Musical – Steve Martin (nominated)
;National Society of Film Critics Awards, USA
*Best Cinematography – Gordon Willis (WON)

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 