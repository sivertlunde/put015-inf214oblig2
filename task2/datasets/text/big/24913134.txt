4.3.2.1
 4, 3, 2, 1}}
 
 
{{Infobox film
| name = 4.3.2.1
| image = 4321 film poster.jpg
| caption = Film poster
| director = Noel Clarke Mark Davis
| producer = Damon Bryant Dean OToole Noel Clarke
| writer = Noel Clarke Ashley Thomas Noel Clarke Andrew Harwood Mills
| music = Adam Lewis
| cinematography = Franco Pezzino
| editing = Mark Davis Mark Everson
| distributor = Universal Studios
| released =  
| runtime = 117 minutes
| country = United Kingdom
| language = English
| budget =
| gross = $1,163,967    
| certificate = Rated 15
}} Ashley Thomas, Andrew Harwood Mills and Clarke.    It was released on 2 June 2010.

==Plot== Ashley Thomas). Unbeknownst to Dillon, Shannon has a crush on him. As the police turn up, Dillon and Smoothy run off and Dillon accidentally drops a stolen diamond into Cassandras bag. The four girls then walk out and go their separate ways home.

Firstly, the story focuses upon Shannon: she walks into her home just as her mother is leaving her father. She loses her temper and runs away, going to Jos home, who has to rush to work, telling her she does not have time to talk. Later on, Jo calls Shannon over to the supermarket where she works, but tells her to leave as soon as she arrives. When she refuses, Dillon kisses Jo, upsetting Shannon, so she grabs a Pringles tube from the shop and runs away.

After getting drunk at a bar, she goes to tunnel where she graffitis. Sitting on a bench, she is attacked by a gang and then taken in by Kelly (Michelle Ryan), who saved her. Shannon realises that Kelly seems to be looking for the Pringles. This is later confirmed when she finds out that she is searching for "15 diamonds". One is already in Cassandras bag, whilst the rest are in the Pringles can which had fallen out of Shannons bag in the tunnel. She escapes by knocking Kelly out with the bathroom door. She finds the diamonds by going back to the bench where she was attacked. Her story ends with her holding the diamonds above a bridge, suggesting she is about to jump and commit suicide. Jo, Cassandra and Kerrys arrive and appear to threaten Shannon into handing the diamonds over.

Secondly, the story moves onto Cassandra, who visits New York to meet up with a man called "Brett" she has met on the internet and audition for a prestigious piano school. She has sex with Brett and in the morning finds all her possessions gone except for her handbag, which contains a diamond. She also finds a letter that Shannon has been looking for, which was written by her mother explaining why she left.

She goes to "Bretts" house to find that "Brett" is a stalker who had hacked into Cassandras computer, taking videos of her. When the fake Brett comes, she knocks him out but he later escapes and chases her down the street where she is rescued by a black woman and her family, who do not take kindly to Bretts racist comments. Cassandra forces her way into an audition with Jago Larofsky (Mandy Patinkin) and wins a place at his school. She then leaves to go home to London.

Thirdly the story of Kerrys is explored. Kerrys and her girlfriend Jas (Susannah Fielding) break into Cassandras flat and stay there. Kerrys brother Manuel (Gregg Chillin), upon discovering them inside, locks them in the panic room and throws a party. When the two girls escape from the panic room, they lock Manuel in there and run away.

Finally, Jo, who works at a 24-hour supermarket with Angelo (Jacob Anderson). She finds that her new manager Tee (Noel Clarke) is in town. Tee has been working with Dillon and Smoothy to deliver the diamonds, but one is missing. Tee asks Dillon and Smoothy to come over to the supermarket. Dillon and Smoothy come to get the money, when they find that Tee has betrayed them, keeping the money for himself and they hold up the store in retaliation.

When Shannon arrives, Jo tries to make her leave and Dillon kisses Jo, which, unseen to Shannon, is at gunpoint. Shannon leaves, stealing a tube of Pringles, and Dillon and Smoothy escape, leaving Tee with the 14 diamonds. Tee, however has hidden them in a tube of Pringles, the same tube that Shannon stole. When Tee is about to be shot by Kelly, Jo rescues him, but when he tries to run, Angelo attacks him. Tee is then arrested.

Cassandra returns with the last diamond and the letter. She meets Jo and Kerrys and they go to find Shannon. They talk her down and give her the letter, comforting her. They put the fourteen diamonds together, give them into the police and fly to New York. Unbeknownst to them, Kelly is also on the plane.

==Cast==
 
 
* Emma Roberts as Joanne
* Tamsin Egerton as Cassandra
* Ophelia Lovibond as Shannon
* Shanika Warren-Markland as Kerrys
* Adam Deacon as Dillon
* Susannah Fielding as Jas
* Jacob Anderson as Angelo
* Freddie Stroma as Cool Brett
* Linzey Cocker as Gwen Ben Drew as Terry Ashley Thomas as Smoothy Alan McKenna as Mr. Jones
* Camille Coduri as Mrs. Phillips
* Sean Pertwee as Mr. Richards
 
* Davie Fairbanks as Fraser
* Nicholas Briggs as Barry
* Kate Magowan as Mrs. Richards
* Helen McCrory as Mrs. Jones
* Ben Miller as Mr Philips
* Alexander Siddig as Robert
* Michelle Ryan as Kelly Eve as Latisha
* Mandy Patinkin as Jago Larofsky
* Kevin Smith as Big Larry
* Noel Clarke as Tee
* Andrew Harwood Mills as Phil
* Gregg Chillin as Manuel
 

==Critical reception==
The film has received mixed reviews from critics. On film review website Rotten Tomatoes, the film has garnered nine fresh reviews and sixteen negative reviews, giving the film an overall rotten 36% rating from critics.   
 Time Out gave the film a positive review, saying that "These girls are brilliantly un-victimy and always come out fighting. If only they weren’t incessantly paraded about in their underwear for the viewing pleasure of men".

However, Peter Bradshaw from British newspaper The Guardian and Wendy Idle from The Times both gave the film a negative review. Bradshaw said the film is "all over the place", also deeming that the acting is on the "torpid side", and Idle believed the film "might just claim back a small corner of the multiplex audience from the relentless onslaught of cynical Hollywood garbage" and described the film as "mostly" bad.

==Soundtrack==
{{Infobox album 
| Name        = 4.3.2.1
| Type        = Soundtrack
| Artist      = Various
| Cover       = 4.3.2.1soundtrack.jpg
| Released    =  
| Recorded    = 2009–2010
| Genre       = Film soundtrack
| Length      = 63:38
| Label       = Sony Music Entertainment
| Producer    = Noel Clarke
}}
 Keep Moving"- Adam Deacon & Bashy featuring Paloma Faith
# "No Bullshit" – Bodyrox
# "When Im Alone" – Lissie
# "Ya Get Me" (Movie Snippet) – Adam Deacon
# "On This Ting" – Adam Deacon
# "A Different Light" – Kerry Leatham
# "Bend Over" (Movie Snippet) – Kevin Smith & Tamsin Egerton Wiley and Incredubwoy
# "I Wanna Party" – Mz Bratt
# "Dont Look Back" – The Union Exchange Eliza Doolittle
# "Do You Fancy Me?" (Bluff) – Kerry Leatham Henriette Bond
# "Drunk Girls" – Stefan Abingdon
# "Paradox" – WKB featuring Myles Sanko
# "DanceFloor" – Davinche
# "This Year" – Mz Bratt featuring Griminal
# "Forever" – Ashley Walters
# "Shes A Gangsta" – Bashy featuring Zalon
# "You Took My Shopping" (Movie Snippet) – Tamsin Egerton
# "Typical Actor" – Adam Deacon
# "Pretty Young Things" – Bodyrox
# "My Size Kid" – Adam Deacon
# "Strangely Sexy Though" (Movie Snippet) – Emma Roberts

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 