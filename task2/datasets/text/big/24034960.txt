Musik ombord
 
{{Infobox Film
| name           = Musik ombord
| image          = 
| image_size     = 
| caption        = 
| director       = Sven Lindberg
| producer       = 
| writer         = Edvard Matz Kerstin Matz Lars Widding
| narrator       = 
| starring       = Alice Babs
| music          = 
| cinematography = Hilding Bladh
| editing        = 
| distributor    = 
| released       = 23 October 1958
| runtime        = 96 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
Musik ombord is a 1958 Swedish film directed by Sven Lindberg and starring Alice Babs.

== Cast ==
  
*Alice Babs
*Sven Lindberg
*Lena Nyman
*Mikael Bolin
*Svend Asmussen
*Ulrik Neumann
 
*Lena Granhagen
*Tage Severin
*Douglas H&#xE5;ge Paul Kuhn
*Ove Tjernberg
*Rolf Johansson
 
*Monica Ekberg
*Erik Strandell
*Eivor Landström
*Torsten Winge
*Birgitta Tegelberg
*Siv Ericks
 
*John Norrman
*Synnøve Strigen
*Kotti Chave
*Ragnar Sörman
*Sten Ardenstam
*Göthe Grefbo
 
*Curt Löwgren
*Birger Sahlberg
*Bibi Carlo
*Lars Kühler
*Jan Tiselius
*Hilding Bladh
 

== External links ==
* 

 
 
 
 
 


 
 