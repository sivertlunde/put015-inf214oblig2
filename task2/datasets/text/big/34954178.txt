Le Retour d'un aventurier
{{Infobox film
| name           = Le Retour d’un aventurier 
| image          = 
| caption        = 
| director       = Moustapha Alassane
| producer       = Moustapha Alassane 
| writer         = 
| starring       = Zalika Souley Djingarey Maïga Moussa Harouna Ibrahim Yacouba Abdou Nani
| distributor    = 
| released       = 1966
| runtime        = 34
| country        = Niger
| language       = 
| budget         = 
| gross          = 
| screenplay     = Moustapha Alassane 
| cinematography = Moustapha Alassane 
| sound          = Moussa Hamidou 
| editing        = Philippe Luzuy 
| music          = Nelos Amelonion 
}}

Le Retour d’un aventurier  is a 1966 film. In this pop art African-style Western, Moustapha Alassane lays into African mimicry.

== Synopsis ==
A man returns home to his village with Western cowboy duds and forms a gang with his old buddies. Getting into their role, the black cowboys spread panic throughout the village with brawls and robberies.

== References ==
 
*  

 
 
 


 