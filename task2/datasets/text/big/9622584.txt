Blonde and Blonder
 
{{Infobox film
| name           = Blonde and Blonder
| image          = Blonde and blonder.jpg
| caption        = Theatrical release poster
| director       = Dean Hamilton
| producer       = Dean Hamilton Jeffrey Barmash Pamela Anderson Dean Borstein
| writer         = Rolfe Kanefsky
| starring       = Pamela Anderson Denise Richards Emmanuelle Vaugier Meghan Ory Peter Allen
| cinematography =
| editing        = Stan Cole
| distributor    = Rigel Entertainment First Look Studios
| released       =  
| runtime        = 94 minutes
| country        = Canada
| language       = English
| budget         = $8,000,000
| gross          = $779,273
}}
Blonde and Blonder is a 2008 Canadian comedy film starring Pamela Anderson, Denise Richards and Emmanuelle Vaugier.  The film was directed by Dean Hamilton, and was released on January 18, 2008.

The films name is a reference to Dumb and Dumber.

==Plot==
Two dumb blondes, Dee Twiddle (Pamela Anderson) and Dawn St. Dom (Denise Richards), meet each other at their first lesson in flight school. Dee is a big-breasted professional dancer with a pet turtle, Virgil, who has a gas problem. Dawn is a former secretary who has tried to be a dancer. After meeting with each other in flight school, they take off in their plane without the instructor. When they notice that its their first lesson, the blondes panic and crash into a golf course. The blondes survive the crash with no injuries. The blondes eventually become friends and they notice that they have been neighbors for nearly a year.
 John Farley) were supposed to monitor what was happening, after seeing Dee and Dawn run from the club, they mistake the blondes for being the infamous assassins Cat and Kit.

Believing that Dee and Dawn are assassins, Leo and Swan offer them $250,000 to "take out" Hang Wong (Byron Mann), the head of the Triads in Niagara Falls, Ontario. Dee and Dawn agree, but they think that they have to take Wong on a date. Federal agents discover the plan to kill Wong and follow the blonde duo also thinking they are Cat and Kit. The Godfather sends Leo and Swan to follow the girls to make sure Wong gets whacked. Once the blonde duo arrive in Niagara Falls, they settle at a casino hotel and resort, which Mr. Wong owns. Cat and Kit also arrive there and find out that Dee and Dawn are pretending to be them and have been the hired assassins. They plan to get revenge because Dee and Dawn stole their reputation for whacking Rimoli and are pretending to be them to kill Wong.

While Dawn is winning money at the casino, Dee meets Mr. Wong during his meeting with Leo and Swan.  Dee tells Wong that Leo and Swan hired her to show him a good time. Federal agents spot Dee with Wong in the casino, and chase them. Wong decides to kidnap Dee and takes her to his yacht.  Meanwhile, they are chased by the federal agents, Dawn (Denise Richards|Richards), Cat and Kit. The federal agents and police take everyone into custody.

Back at the casino, the police arrest Kit and Cat, and also Wong for kidnapping Dee. Dawn finally finds her dream guy and with the millions of dollars Dawn won at the casino, Dee and Dawn establish Dee and Dawns Famous Turtle Sanctuary in the countryside where they only have one turtle but promise to have more soon.

==Cast==
* Pamela Anderson as Dee Twiddle
* Denise Richards as Dawn St. Dom
* Emmanuelle Vaugier as Cat
* Meghan Ory as Kit, Cats Assistant
* Kevin Farley as Leo, a Gangster John Farley as Swan, a Gangster
* Joey Aresco as Agent Campbell
* Garry Chalk as Agent Gardenia
* Byron Mann as Mr. Wong
* Jay Brazeau as Louie Ramoli
* Woody Jeffreys as Ken, Dawns Dreamguy John Farley as Leutant Porter
* John Tench as Evangelos, Ramolies Partner
* Alex Bruhanski as Gangsterboss Godfather
* Phoenix Ly as One Eye, one of Wangs Killers
* Kyle Labine as the fresh Bellboy

==Reaction==

===Box office performance===
Worldwide:  	 $779,273    

===Critic response===
The movie was a huge flop, getting horrible reviews from both critics and viewers. It brought in a very low amount compared to its 8 million dollar budget.

Critics treated the DVDs additional behind-the-scenes featurette, "The Making of Blonde and Blonder", with a fair amount of respect:  "This featurette ...is actually somewhat entertaining", "it at least appears that it was fun making this movie", "the film looks like it was a hoot to make",  "the occasional interview goof makes it interesting."

==Notes==
 

==External links==
*  
*  
*  
*  
*   at Yahoo! Movies
*  

 
 
 
 
 