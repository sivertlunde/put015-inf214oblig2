Vilambaram
{{Infobox film
| name = Vilambaram
| image =
| image_size =
| caption =
| director = Balachandra Menon
| producer = K. G. Rajagopal
| writer = Balachandra Menon
| screenplay = Balachandra Menon Ashokan Balachandra Menon
| music = S. P. Venkatesh
| cinematography = Anandakkuttan
| editing = Hariharaputhran
| studio = GR International
| distributor = GR International
| released =  
| country = India Malayalam
}}
  1987 Cinema Indian Malayalam Malayalam film, Ashokan and Balachandra Menon in lead roles. The film had musical score by S. P. Venkatesh.   
 
==Cast==
  
*Sukumari as Mary 
*Thilakan as James  Ashokan as Basheer 
*Balachandra Menon as Advocate PK Namboothiri 
*Sankaradi as Judge  Baiju as Hotel Boy  Ambika as Sheela 
*Jalaja as Razeena 
*MG Soman as Balagopalan 
*Saritha as Balagopalans Wife  Shari as Valsala 
 

==Soundtrack==
The music was composed by S. P. Venkatesh. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Enthaanandam || G. Venugopal, Janakidevi || P Bhaskaran || 
|- 
| 2 || Thaarakale Ambiliye || Janakidevi, Sindhu Premkumar, Kala || P Bhaskaran || 
|}
 
==References==
 

==External links==
*  

 
 
 


 