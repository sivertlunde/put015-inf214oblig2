Asterix and the Big Fight (film)
{{Infobox film name = Astérix et le coup du menhir image = caption = writer = Adolf Kabatek and Yannik Voight, adapted from René Goscinny and Albert Uderzo starring = Roger Carel (French) / Jürgen von der Lippe (German) / Bill Oddie (English) narrator =  director = Philippe Grimond producer = Yannick Piel music = Michel Colombier distributor = released =   runtime = 81 minutes|
| country = France, Germany
| language = French / German / English awards = budget =
}}
 1989 animated comic book series. The movie has a different plot from the book of the same name. It combines plot elements from Asterix and the Big Fight and Asterix and the Soothsayer. Although there is plenty of fighting — as usual for an Asterix story — the actual fight that the story is named for is not part of the movies plot. The novelization was titled "Operation Getafix" (the German translation of the film was Operation Hinkelstein, a hinkelstein being a menhir).

==Plot summary== Romans plan Getafix to keep him from making the magic potion. In an effort to rescue him, Obelix accidentally puts Getafix out of action with a menhir, the impact of which causes amnesia and insanity.

While the Gauls come to grips with this, a major storm sweeps over the village and a soothsayer named Prolix seeks shelter with them. He quickly deceives the more credulous villagers about the authenticity of his abilities and after the storm passes he sets up in the forest nearby.

Asterix and Vitalstatistix desperately attempt to have Getafix brew the potion, resulting in explosions and the occasional flying cauldron — alerting the Romans that something is up. They send a camouflaged, and very reluctant, spy to investigate. He is quickly captured and used as a guinea pig for Getafix less explosive concoctions. However one of these makes him lighter than air causing him to float away, and he reports their problem.

The Romans send a patrol to investigate, and come back with the Soothsayer instead, however the villagers think the sceptical Asterix has driven him off. The centurion is quickly convinced that Prolix is genuine (much to his dismay since Gaulish soothsayers are to be arrested) and decides to use him to chase away the villagers. Soon Prolix appears at the gates and foretells doom if the village is not abandoned — all but Asterix, Obelix and a still crazed and potion brewing Getafix leave, going to a nearby island.

Meanwhile Getfix brews a truly noxious potion whose vapours engulf the village, driving off the Romans who had quickly moved in, but also convincing them that the soothsayer had real abilities since the smell matched his prediction of pestilence. However when Getafix accidentally tastes some he is cured (despite Obelix attempt to treat his condition with a second tap from a menhir).

Getafix brews the magic potion and convinces the villagers to test the soothsayers reliability by attacking the Roman camp. The soothsayer is "menhired", the centurion demoted to the ranks, and the village goes back to normal.

==Cast==
{| class="wikitable"
|-
! Character
! France
! United Kingdom
! United States
|-
! Asterix
| Roger Carel
| Bill Oddie
| Henry Winkler
|-
! Obelix
| Pierre Tornade
| Bernard Bresslaw
| Rosey Grier
|-
! Prolix
| Julien Guiomar
| Ron Moody Hal Smith
|-
! Bonemine/Impedimenta
| Marie-Anne Chazel
| Sheila Hancock
| Lucille Bliss
|-
! Panoramix/Getafix/Vitamix
| Henri Labussière
| Peter Hawkins
| Eddie Bracken
|-
! Centurion
| Roger Lumont
| Brian Blessed
| Ed Gilbert
|-
! Assurancetourix/Cacofonix
| Edgar Givry and Jean-Jacques Cramier (singing)
| Tim Brooke-Taylor
|
|-
! Abraracourcix/Vitalstatistix/Bombastix
| Henri Poirer
| Douglas Blackwell
| Allan Melvin
|-
! Ardeco
| Patrick Préjean
| Andrew Sachs
|
|}

==Release notes== dub of the Asterix and the Big Fight featured the voices of British actors Bill Oddie, Bernard Bresslaw, Peter Hawkins, Brian Blessed, Tim Brooke-Taylor, Andrew Sachs, and Ron Moody, amongst others. For the English DVD Box Set release, rather than using the British dub, an American dub was included, featuring the voices of Henry Winkler as Asterix, Rosey Grier as Obelix and Lucille Bliss as Impedimenta. This dub had been intended for a U.S. release which ultimately never materialized, and it was shelved for over a decade until it appeared on DVD. The American dub is considered inferior by fans of the original due to it changing parts of the plot as well as character names (Getafix is changed to "Vitamix", Vitalstatistix to "Bombastix", Cacofonix to "Franksinatrix," and Unhygienix to "Fishstix"), and general dumbing-down for an audience assumed to be unfamiliar with the characters.   The American dub assigns stereotypical Italian comedy accents to the Roman characters, features a narrator ( Tony Jay ) explaining the plot to the audience, and makes changes to the terminology of the original story, substituting "wizard" for druid, "fortuneteller" for soothsayer, "rockets" for menhirs, and "vitamin potion" for the magic potion.

==Historical inaccuracies==
The optio wears a standard legionarys helmet in the film. In his rank, his helmet would actually have had plumes of horse hair or feathers on either side of his helmet that could be accompanied by a helmet crest. He would also carry the hastile, a special staff roughly his own size.

==References ==
#  {{cite web url = http://www.comedix.de/forum/viewtopic.php?t=1829 title = Fan Opinions.
}}
#  {{cite web url = http://www.sprites-inc.co.uk/forum/showthread.php?p=33241#post33241 title = Fan Opinions.
}}

==External links==
* 
* 
 

 
 
 
 
 
 
 
 