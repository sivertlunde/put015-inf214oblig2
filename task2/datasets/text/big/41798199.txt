Nailbiter
{{Infobox film
| name           = Nailbiter
| image          = File:Nailbiter2013filmposter.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Patrick Rea
| producer       = 
| writer         = Patrick Rea, Kendal Sinn
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Emily Boresow, Erin McGrane, Meg Saricks
| music          = Julian Bickford
| cinematography = Hanuman Brown-Eagle
| editing        = Josh Robison
| studio         = Ministry Machine Productions, SenoReality Pictures
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $300,000    
| gross          = 
}}
Nailbiter is a 2013 horror film directed by Patrick Rea. The movie was first released onto DVD in Japan on January 25, 2013 and received its American debut on April 5, 2013 at the Phoenix Film Festival. It stars Erin McGrane as a mother that defend herself and her children against a dangerous foe. Rea intends to film a sequel to the film, which is tentatively titled Nailbiter 2. 

==Plot==
 
Janet (Erin McGrane) and her children are on the way to the airport to pick up their father (Aaron Laue) when they are forced to take shelter from a tornado that is ravaging the area. They manage to find a storm shelter outside of a seemingly abandoned house, only for the group to become trapped by a fallen tree that prevents them from leaving the storm shelter. Things are made worse when they discover that theyre not alone in the house or storm shelter and the group is attacked by a monstrous creature.

==Cast==
*Emily Boresow as Alice Maguire
*Meg Saricks as Jennifer Maguire
*Erin McGrane as Janet Maguire
*Joicie Appell as Mrs. Shurman Michelle Davidson as Dina Ian Dempsey as Sean
*Ben Jeffrey as Deputy Carr
*Aaron Laue as Lt. Maguire
*Allen Lowman as Tom
*Zane Martin as Little monster Mark Ridgway as Sheriff
*John D. Barnes as Townsperson
*Jason Coffman as Creature Tom Conroy as Bartender
*Anita Cordell as Traveler at airport

==Production==
Rea first announced plans for Nailbiter in 2008 but did not begin production until 2009,  as he had to first raise funding for the film.  Filming began in 2009 in Kansas City, Missouri,    but shooting stopped 2/3 of the way into the film as Rea ran out of funding.  The cast and crew resumed filming in December 2010 and shot an additional airport scene in May 2011, after which point they moved fully into post production.     While working on the films script, Rea chose to make much of the cast female as he "felt like that would be more endearing as well as more compelling."  Before officially beginning filming, Rea had his four main actors "go out and act like a family for awhile" in order to build a connection between the actors.  Other than financial difficulties, the only significant issue with the filming for Nailbiter experienced was the weather, as the "days   wanted to look stormy were sunny and the days   wanted to look sunny looked stormy". 

==Reception==
 
Critical reception for Nailbiter has been mixed to positive.  A reviewer for Star Pulse commented that "As far as sheer creep factor, "Nailbiter" delivers handsomely" but criticized the DVDs lack of a "making of" documentary.  The Oklahoma Gazette praised the films setting, as they felt that this was "intriguing" and also commented favorably on the choice of making Erin McGranes character an alcoholic.  Aint It Cool News also cited McGranes characters alcoholism as a highlight, as they saw this as one of several small touches that they enjoyed.  Dread Central remarked that the film was "not a perfect flick", as they felt that McGrane took too long to settle into her character but overall saw the movie as "ingenious, creepy and a delightful modern take on an old-school sense of storytelling". 

===Awards===
*Best Director at Fright Night Film Fest (2013, won)
*Best Horror Feature at Shriekfest (2013, won) 

==References==
 

==External links==
*  
*  

 
 
 