Return of Sergeant Lapins
{{Infobox film
| name = Return of Sergeant Lapins
| image = Serzanta_Lapina_atgriesanas.jpg
| caption = 
| director = Gatis Šmits
| producer =  
| writer =   
| starring =  
| music = Kārlis Lācis
| cinematography = Soopum Sohn
| editing = Oskars Morozs
| studio = TANKA Solid Entertainment
| distributor = TANKA
| released =  
| runtime = 80 minutes
| country  = Latvia Sweden
| language = Latvian LVL 426,000
  }}
Return of Sergeant Lapins (  or  ) is a 2010 comedy-drama film directed by Latvian director Gatis Šmits.
 The New Theatre of Riga in 2007. 

Before being released on November 26, the film was screened at the 15th Busan International Film Festival, which took place in October 2010.

== Plot ==
The film is set in Riga, where Sergeant Krists Lapiņš returns from an international mission and moves into a remote flat in the citys Āgenskalns district. However, although expecting to live a peaceful life, Krists finds himself involved in several adventures.

== Cast ==
*Andris Keišs as Krists Lapiņš
*Guna Zariņa as Alise Lagzdiņa
*Gatis Gāga as Didzis Budļevskis
*Vilis Daudziņš as Ervīns Meijers
*Kaspars Znotiņš as Dainis Geidmanis
*Baiba Broka as Maira Mežsarga
*Linda Šingireja as Ināra Meijere
*Igors Ziemelis as Mazais

== References ==
 

== External links ==
*    
*  

 
 
 
 
 
 


 
 