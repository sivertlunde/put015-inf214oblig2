The Story of a Small Town
 
 
 
  
The Story of a Small Town (Chinese: 小城故事; pinyin: xiǎochéng gùshì) is a 1978 Taiwanese film, produced by Hsiao Chung Productions ( ). The film revolves around a family living in a small town. Kenny Bee plays the male family member who is released from prison and falls in love with a mute woman (Lin Feng-jiao).

The film was remade as a TV series, featuring Yang Kuei-mei ( ) and Yin Xiaotian ( ). 

==Cast and crew==
;Cast
* Kenny Bee
* Lin Feng-jiao, credited as Joan Lin
* Ge Hsiang-ting ( )
* Lee Lieh ( )
* Chen Jun-jieh ( ), credited as Ou Di ( )

;Crew
* Li Hsing ( ), director and supervisor
* Liu Teng-shan ( ), story
* Chang Yung-hsiang ( ), screenplay
* Chen Kun-hou ( ), cinematography
* Tsai Cheng-pin ( ), art director
* Chih Hsueh-fu ( ), director of lighting
* Weng Ching-hsi ( ), composer, credited as Tony Wong in this film and Tang Ni ( ) in other materials

==Reception==
In the 16th Golden Horse Film Festival and Awards (1979), this film won the Best Picture, Best Original Screenplay, Best Actress (Lin Feng-jiao), and Best Child Star (Weng Ching-hsi). 

==Music==
Xiaocheng gushi ( : xiǎochéng gùshì), translated as either Small Town Story or Story of a Small Town, is a 1979 album by Teresa Teng. Tracks 1, 6, 7, and 8 are theme songs from the film. With the exception of track 7, the names of these tracks are different for each release.

The album was released by Kolin Records ( ) in Taiwan and by Polydor Records in Hong Kong and overseas. Polydor re-recorded the theme songs for its own edition and released this album as Taiwans Love Songs 6: Small Town Story ( ).

Weng Ching-hsi ( ) is credited as Tang Ni ( ) on this album.

===Side A===
#Kolin: "Moment of Silence" (  momo de yi ke) — 2:13 Polydor: "You Are in My Heart" (  ni zai wo xinli) — 2:54
#: Music by Weng Ching-hsi, lyrics by Chuang Nu ( )
#: Sub-theme of the film; Polydor re-recording has additional, slightly different lyrics
# "xinshi" ( ), "On Your Mind" or "On My Mind", music by Weng Ching-hsi, lyrics by Suen Yi ( ) — 3:28
# "Poetry" (  shiyi), written by Liu Chia-chang — 3:00
# "A piece of a fallen leaf" (  yipian luoye), music by Takashi Miki ( ), lyrics by Chuang Nu — 3:27
# "The September Story" (  jiuyue de gushi), music by Inaba Akira ( ), lyrics by Suen Hei ( ) — 3:42
# Kolin: "Precious Small Town" (  xiaocheng duo keai) — 2:11 Polydor: "Spring in a Small Town" (  chunfeng man xiaocheng) — 2:22
#: Music by Weng Ching-hsi, lyrics by Chuang Nu
#: Sub-theme of The Story of a Small Town

===Side B=== Small Town Story" (  / Xiaocheng gushi) — 2:19 (Kolin) / 2:35 (Polydor)
#: Music by Weng Ching-hsi, lyrics by Chuang Nu
#: Main theme of The Story of a Small Town
# Kolin: "Quiet Lane" (  jingjing de xiaolu) — 3:15 Polydor: "Lane" (  / Xiaolu) — 3:04
#: Music by Weng Ching-hsi, lyrics by Chuang Nu
#: Sub-theme of The Story of a Small Town
# "Me and You" (  wo he ni), music by Minoru Endo ( ), lyrics by Lin Huang-kuen ( ) — 3:46
#: Mandarin version of "kitaguni no haru" ( ), first sung in 1977 by Masao Sen ( )
# "Free and at Leisure" (  xiaoyaozizai), music by Minoru Endo, lyrics by Lin Huang-kuen — 3:28
#: Mandarin version of  , first sung in 1973 by Tetsuya Watari
# "Place of First Loves" (  chulian de difang), music by Liu Chia-chang; lyrics by Suen Hei — 2:27
#: Originally sung by Chiang Lei ( ) 
# "Thinking of You" (  xiangqi ni), music by Kimiaki Inomata ( ), lyrics by Lin Huang-kuen — 3:28

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 