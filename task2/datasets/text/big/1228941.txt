Manhattan Murder Mystery
{{Infobox film
| name           = Manhattan Murder Mystery
| image          = Manhattanmurderposter.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Woody Allen
| producer       = Robert Greenhut
| writer         = Woody Allen Marshall Brickman
| narrator       = 
| starring       = Woody Allen Diane Keaton Anjelica Huston Alan Alda
| music          = 
| cinematography = Carlo Di Palma
| editing        = Susan E. Morse
| distributor    = TriStar Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States English
| budget         = United States dollar|$13.5 million (est.)
| gross          = $11,285,588 (USA)
}}
Manhattan Murder Mystery (1993) is a comedy murder mystery film, directed by and starring Woody Allen and written by Woody Allen and Marshall Brickman.

==Plot==
 
Larry Lipton (Woody Allen) and his wife Carol (Diane Keaton) meet their older neighbors Paul (Jerry Adler) and Lilian (Lynn Cohen) House in the elevator in a pleasant encounter. But the next night, Lilian is found to have died of a heart attack. The Liptons are surprised by the death because Lilian seemed so healthy.

A couple of days later, Carol notes Pauls cheerfulness and thinks his behavior is suspicious. Inventing a social excuse to visit him, she finds a funerary urn hidden in a cupboard, seeming to refute Pauls claim that Lilian had been buried. Carol becomes more suspicious after hearing him leave his apartment at 1:00&nbsp;a.m. She thinks that he left to dispose of the urn, but Larry tells Carol shes "inventing a mystery."

Carol manages to sneak into Pauls apartment while he is away. The urn is missing, and she finds two tickets to Paris and a reservation for a Paris hotel with a woman named Helen Moss. From Pauls apartment, she calls Ted (Alan Alda), a friend of Larry and Carols who is equally interested in the "mystery". Paul returns unexpectedly. While hiding under the bed, Carol overhears a conversation between Paul and a woman whom she supposes is Helen Moss. 

Later, Ted finds out where Helen Moss lives. They follow her to a theater that Paul owns, and find out that Helen Moss (Melanie Norris) is a young actress. While eavesdropping on Paul and Helen, they hear the couple talking about money. 

Subsequently, Carol spots the supposedly dead Lilian House on board a passing bus. Larry suggests that Lilian must have a twin. Ted investigates and finds that Lilian has a sister and a brother, but no twin. Larry and Carol then trace this "Lilian" to a hotel where she is registered under a fake name. On the pretense of delivering a personal gift, they enter her hotel room and find her dead on the bedroom floor. They call the police, but no trace of any dead body is found. 

The Liptons return later to search the room for clues. While leaving, they get trapped in the lift and  accidentally stumble across Lilians body inside the emergency exit panel. Upon exiting to the street, they spot Mr. House putting the body in the trunk of his car. The Liptons follow his car out of the city to a junk yard, where the man dumps the body in a pile of scrap metal, and it is dropped into a melting furnace with the metal. Paul is aware that the Liptons had been following him. But Larry and Carol are helpless as the evidence of the murder is now destroyed.

Meeting at a restaurant to discuss the mystery, Larry, Carol, Ted, and Marcia Fox (Anjelica Huston), a client and friend of Larrys, Marcia says that Paul has committed the perfect murder and advises Carol and Larry on ways to bring him to justice. They plan to trick Helen into auditioning for a fake production so that they can record her voice to create a phone call to Paul announcing that Carol and Larry had recovered Lilians body and demand that Paul give them $200,000 or kill them. Marcia believes that Paul will try to kill them, and the police can then catch him in the attempt.

Paul decides to kidnap Carol and calls Larry to ask for a meet up where he will release Carol in return for Lilians body. At the meet up, Paul and Larry get into a scuffle, but Larry manages to break away to search for Carol in the theater. Paul stalks Larry with a pistol but becomes disoriented by an array of mirrors and glass behind the theater screen reflecting the movie being played (Orson Welless The Lady from Shanghai). At a critical moment, Pauls loyal assistant, an older paramour whom Paul earlier brushed aside in favor of Helen, appears at the scene and shoots him in an exchange of gunfire. Larry rescues Carol and they call the police.

Later, Marcia explains to Ted what happened. The dead body in the apartment was Lilians rich sister, who bore a passing resemblance to Lilian but was not her twin. The sister had suffered a heart attack while visiting them, and the Houses decided to take advantage of the situation. Paul would claim that his wife had died, while Lilian would assume the identity of her sister (a recluse living at the hotel) in order to change her sisters will naming Lilian and Paul as sole beneficiaries. But Paul then double-crossed and killed Lilian, too, so he could run off with Helen.

==Cast==
* Woody Allen as Larry Lipton
* Diane Keaton as Carol Lipton
* Jerry Adler as Paul House
* Lynn Cohen as Lillian House
* Alan Alda as Ted
* Melanie Norris as Helen Moss
* Marge Redmond as Mrs. Dalton
* Anjelica Huston as Marcia Fox
* Joy Behar as Marilyn
* Zach Braff as Nick Lipton

==Production== first time novelist" with whom Allens character became romantically involved (Huston was 41 during production). 

In the fall of 1992, Allen called Diane Keaton and asked her to fill in for Farrow, and she immediately accepted.  When asked if he had re-written the script to fit Keatons talents, Allen said, "No, I couldnt do that. In a regular script I would have done that upon hiring Diane Keaton. But I couldnt   because its a murder mystery, and its very tightly plotted, so its very hard to make big changes... I had written   more to what Mia likes to do. Mia likes to do funny things, but shes not as broad a comedian as Diane is. So Diane made this part funnier than I wrote it."   

Making the film was a form of escape for Allen because the "past year was so exhausting that I wanted to just indulge myself in something I could relax and enjoy".  He also found it very therapeutic working with Keaton again. After getting over her initial panic in her first scene with Alan Alda, Keaton and Allen slipped back into their old rhythm.  After she had trouble with that scene, Allen decided to re-shoot it. In the meantime, she worked with her acting coach and did other scenes that went well.    According to Allen, Keaton changed the dynamic of the film because he "always look(s) sober and normal compared to Keaton. I turn into the straight man". Huston said that the set was "oddly free of anxiety, introspection and pain", and this was due to Keatons presence. 

The film was shot in the fall of 1992 on the streets of Greenwich Village, the Upper East Side and the Upper West Side. Allen had cinematographer Carlo Di Palma rely on hand-held cameras, "swiveling restlessly from one room to another, or zooming in abruptly for a close look " in a style one reviewer called "meaningless affectation." 
 2nd and 3rd Avenue New York one group another group. Allen staged a climactic shoot-out in a roomful of mirrors that, according to Allen, referenced a similar shoot-out in Orson Welles film, The Lady from Shanghai.   

This was Allens second and final film with TriStar Pictures, and it was speculated in the press that this deal was not extended because of the filmmakers personal problems, or that his films were not very profitable. Allen, however, denied these allegations in interviews at the time.  Zach Braff made his feature film debut in a one-scene role as the son of Allen and Keatons characters; Braff later said, "When I look at that scene now, all I can see is the terror in my eyes". 

==Reception==
Manhattan Murder Mystery opened on August 18, 1993 in 268 theaters and made USD $2 million in its opening weekend. It went on to gross $11.3 million in North America, below its estimated $13.5 million budget.     Its £1,920,825 in box office made it the List of 1994 box office number-one films in the United Kingdom|number-one film in the United Kingdom for the weekend ending January 23, 1994.

In his review for   gave the film four out of four stars, and advised fans to forget Allens tabloid woes because "theres a better reason why Allen fans should give it a shot. Its very, very funny, and theres no mystery about that".    Janet Maslin called it a "dated detective story" but also wrote, "it achieves a gentle, nostalgic grace and a hint of un-self-conscious wisdom".     Desson Howe, in The Washington Post, complained that there was "little new" in this film. Allen and Keaton are essentially playing Alvy Singer and Annie Hall gone middle-aged".   

The film holds a 92% rating on Rotten Tomatoes, with 24 positive out of 26 reviews. 

===Nominations===
* 
* , Anjelica Huston
* 

==References==
 

==External links==
* 
* 
* 
* 
*  at Turner Classic Movies

 

 
 
 
 
 
 
 
 
 
 
 
 
 