Tony Hawk in Boom Boom Sabotage
{{Infobox Film
| name = Tony Hawk in Boom Boom Sabotage
| image = BoomBoomSabotage.jpg
| caption = DVD cover
| director = Johnny Darrell
| producer = Sharan Wood Nicole Makila (associate)
| writer = Johnny Darrell Ben Burden Smith Sib Ventress
| narrator = Michael Donovan Michael Dobson Aidan Drummond Brian Drummond Mackenzie Gray Tony Hawk
| music = Brian Carson Johnny Oilsin
| cinematography =
| editing = Jordan Atkinson Sylvain Blais Logan McPherson
| distributor =
| released = 2006-9-12 (United States|USA) 
2007-3-12 (Canada)
| runtime = 70 min
| country = USA, Canada English
| budget =
| preceded_by =
| followed_by =
}}
Tony Hawk in Boom Boom Sabotage is a computer animation|computer-animated feature film|feature-length film starring Tony Hawk produced by Mainframe Entertainment. FUNimation Entertainment distributed the movie in USA and Japan, while Alliance Atlantis distributed it in Canada.
In Mainframes early productions, the movie was titled Tony Hawk in Boom Boom Goes the Circus, which uses cel-shading characters. Some of the early designs can still be seen in Mainframes web site.

==Broadcast information==
It has been seen on Cartoon Network and released on DVD.

YTV premiered the movie on 2007-3-12.

==Plot==
  kidnapped by kids may be his only hope.

==Main cast==
* Noel Callahan: Sage (voice) Michael Dobson: Larry Grimley/Worker/Homey Clown (voice)
* Michael Donovan: TV Narrator/Commercial VO (voice)
* Aidan Drummond: Jesse (voice)
* Brian Drummond: Hamshank/Chopper Chuck/DJ/Mimic (voice)
* Mackenzie Gray: Marshall/Boris/Stilt Walker/Floor Worker (voice)
* Tony Hawk: Himself (voice)
* Scott Hyland: Frank/Carnie (voice)
* David A. Kaye: Kud (voice)
* Colin Murdock: John Dullard/Carnie (voice)
* Brenna OBrien: Jesse
* Nicole Oliver: Reporter/Buzzie Bee
* David Orth: Todd, Carnie
* Carter West: Switch Mitch
* Chiara Zanni: Kit

==Awards==
The movie won Best Production - Feature Length and Best Original Musical Score categories in the 1st Annual Canadian Awards for the Electronic & Animated Arts in 2006. 

==See also==
* List of animated feature films
* List of computer-animated films

==References==
 

==External links==
*  

 
 
 
 


 