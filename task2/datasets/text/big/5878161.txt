Favela Rising
{{Infobox film
| name           = Favela Rising
| image          = FavelaRising.jpg
| caption        = Theatrical release poster
| director       = Matt Mochary Jeff Zimbalist
| producer       = Ravi Anne
| writer         = 
| starring       = 
| music          = Force Theory Michael Furjanic Neill Sanford Livingston
| cinematography = Kelly Mark Green Matt Mochary Jeff Zimbalist
| editing        = Jeff Zimbalist
| distributor    = HBO/Cinemax Documentary THINKFilm VOY Pictures
| released       =  
| runtime        = 80 minutes
| country        = Brazil United States
| language       = Portuguese
| budget         = 
| gross          = 
}} directors Jeff Zimbalist and Matt Mochary. It was produced by Sidetrack Films and VOY Pictures. It debuted at the Tribeca Film Festival on April 24, 2005 where it won the award for Best New Documentary Filmmaker for Zimbalist and Mochary. The films look at life in Brazils slums won it further awards such as Best Documentary Film from the New York Latino Film Festival and Best Feature Documentary from Big Sky Documentary Film Festival.  The film has won over twenty-five international festival awards and was short-listed for an Oscar.

The film focuses on the work of Anderson Sá, a former drug trafficker who establishes the grassroots movement AfroReggae.  The group, Grupo Cultural AfroReggae (AfroReggae Cultural Group), was initially intended to draw in adolescents interested in a number of musical genres.  These genres include, but are not limited to Soul music|soul, reggae, Rap music|rap, and hip-hop. Early on, Grupo Cultural AfroReggae offered a different type of education to the youth it attracted.  This education included workshops focusing on dance, recycling, football, percussion, and more. The group aims at using music and education to better the lives of youth and prevent further growth of gangs.  Grupo Cultural AfroReggae believes that through education, there is a greater likelihood that adolescents will not get caught up in drugs and gang violence. 

==References==
 

==External links==
*  
*  
*  
*  , coordinator for international partnerships of Afro Reggae

 
 
 
 
 
 

 