El Monasterio de los Buitres
{{Infobox Film
| name           = El Monasterio de los Buitres
| image          = 
| caption        = 
| director       = Francisco del Villar
| producer       = 
| writer         = Vicente Leñero
| narrator       = 
| starring       = Enrique Lizalde Enrique Álvarez Félix  Irma Serrano
| music          = 
| cinematography = Gabriel Figueroa
| editing        = 
| distributor    = Estudios Churubusco Azteca S.A 1973
| runtime        = 102 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Mexican motion picture categorized as religious Drama film|drama. It was filmed in 1973. 

== Synopsis  == Father Prior using psychoanalysis. During the process, the young men manifested indecision, sexual problems (as incest or homosexuality) and lack of faith. 

== Cast ==
* Enrique Lizalde as Father Prior
* Enrique Álvarez Félix as Emilio
* Irma Serrano as Amalia
* Augusto Benedico as Pablo
* Enrique Rocha as Andres
* Héctor Bonilla as Marcos
* Otto Sirgo as Juan Eduardo Noriega as Don Manuel
* David Estuardo as Antonio
* Jose Chávez as Camilo
* Carlos Cámara
* Margarita de la Fuente as Carmen

== External links ==
*  

== Notes ==
 

 
 
 
 
 
 

 
 