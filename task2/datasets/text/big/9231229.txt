Klondike Annie
 
{{Infobox film
| name           = Klondike Annie
| image          = Klondike Annie.jpg
| image_size     =
| caption        =
| director       = Raoul Walsh
| producer       = William LeBaron
| writer         = Mae West Frank Mitchell Dazey
| starring       = Mae West Victor McLaglen
| music          = Victor Young
| cinematography = George T. Clemens
| editing        = Stuart Heisler
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
}} 1936 black-and-white comedy drama film starring Mae West and Victor McLaglen.  The film was co-written by West from her play "Frisco Kate", which she wrote in 1921.  The film was directed by Raoul Walsh.
Mae West portrays a kept woman by the name of Rose Carlton, "The Frisco Doll".  She accidentally murders her keeper, Chan Lo, while he is trying to kill her and escapes on a steamer to Nome, Alaska. The Frisco Doll is now wanted for murder.  En route she is given space in a cabin with a missionary woman, Sister Annie Alden.  The woman is on her way to rescue a financially troubled mission in Nome.  Sister Annie dies en route. The Frisco Doll, fearing apprehension by the Sheriff, assumes the identity of Sister Annie and dresses her up as a prostitute in a scene later deleted by the censors.

The Frisco Doll decides to keep Sister Annies promise of rescuing the mission and raises the money through a soul-shaking sermon and song.
Klondike Annie/Rose Carlton/The Frisco doll knows she needs to turn herself in and prove her innocence because it was self-defense.  Steaming back to San Francisco she falls in love with the Captain of the boat, Bull Brackett.  "Bull, ya aint no oil paintin, but ya are a fascinatin monster".   

This film caused a rift between West and William Randolph Hearst, who decided never to print Wests name in any of his newspapers. The reason given was the racy material of the film and Wests sexual persona in a religious setting. This may seem hypocritical due to his extramarital affair with actress Marion Davies. West was quoted as saying "I may have invited censorship into Hollywood, but I also saved the industry and Paramount."

The songs were composed by Gene Austin, who also appeared in the film.

Production began on September 16, 1935 and concluded in December of that year. Klondike Annie was released February 21, 1936 at a production cost of $1,000,000. As usual with Wests films, scenes were deleted to make this film presentable in most markets.  Eight minutes of the film were deleted.  The footage is presumably lost forever.  In this lost footage is the scene in which The Frisco Doll stabs Chan Lo when he was going to stab her instead.  The other lost scene is when The Frisco Doll switched identities with Sister Annie and dressed Sister Annie up as a prostitute.  The veiled connection of Sister Annie and The Salvation Army made this scene inappropriate to the censors  but its deletion made the final print of the film appear choppy.

The State of Georgia went so far as to ban this film outright. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 