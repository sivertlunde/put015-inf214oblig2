Nuit 1
 
{{Infobox film
| name           = Nuit #1
| image          = Nuit 1.jpg
| alt            = 
| caption        = 
| director       = Anne Émond
| producer       = Nancy Grant Sylvain Corbeil (associate producer)
| writer         = Anne Émond
| starring       = Catherine de Léan Dimitri Storoge
| music          = 
| cinematography = Mathieu Laverdière
| editing        = Mathieu Bouchard-Malo Metafilms
| distributor    = K Films Amerique
| released       =  
| runtime        = 91 minutes
| country        = Canada
| language       = French
| budget         = 
| gross          = 
}}
Nuit #1 ( ) is a Canadian drama film directed by Anne Émond. 

== Plot ==
Clara (Catherine de Léan) and Nikolai (Dimitri Storoge) meet at a sweat-soaked rave and end their night at his apartment. The first part of the film is an erotic and candid portrait of their lovemaking, When Clara tries to sneak out without saying goodbye, this typical hookup takes an unexpected turn. 

== Awards/nominations==
* 2011 Vancouver International Film Festival Best Canadian Feature Film - Won (Anne Émond)
* 2012 Genie Award for Best Performance by an Actress in a Leading Role - Nominee (Catherine de Léan)
* 2012 Genie Award for Best Original Screenplay - Nominee (Anne Émond)   
* 2012 Claude Jutra Award for best film by a first-time director - Won 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 