Anazapta
{{Infobox film
| name           = Anazapta
| image          = Anazapta dvd cover.jpg
| image size     = 
| caption        = DVD cover
| border         = 
| alt            = 
| director       = Alberto Sciamma
| producer       = Michael Cowan 
| screenplay     = Harriet Sand  Alberto Sciamma
| based on       = 
| starring       = Jason Flemyng   Lena Headey   Christopher Fairbank   Ian McNeice   Jeff Nuttall Dan Jones
| cinematography = Alastair Meux 
| editing        = George Akers
| studio         = Beyond Films   Enterprise Films   Great British Films   Spice Factory 
| distributor    = 
| released       =  
| runtime        = 105 min  (DVD cut) 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Anazapta is a 2002 British mystery thriller film directed by Alberto Sciamma and starring Jason Flemyng, Lena Headey, Christopher Fairbank, Ian McNeice, Jeff Nuttall. In the US the movie was released as Black Plague.  

==Plot==
The film is set in England in 1348. There goes on Hundred Years War between England and France. A detachment of soldiers returns to their village, but that does not bring happiness to its owner, the beautiful English noblewoman Lady Matilda (Lena Headey). Her husband, Sir Walter de Mellerbi, remains hostage in France.

Meanwhile, the soldiers brought with them a prisoner, who can be exchanged for Sir Walter and for a solid ransom to save her estate from bankruptcy. After some time, however, the inhabitants of the manor begin to die, one by one, a mysterious and painful death. Lady Matilda starts suspecting that the prisoner, whom she cares for, is not the one he claims to be.

==Cast==
*Lena Headey ... as Lady Matilda Mellerby 
*David La Haye ... as Jacques de Saint Amant 
*Jason Flemyng ... as Nicholas 
*Christopher Fairbank ... as Steward  Anthony ODonnell ... as Randall 
*Jeff Nuttall ... as Priest 
*Ralph Riach ... as Physician
*Hayley Carmichael ... as Agnes 
*Ian McNeice ... as Bishop 
*Jon Finch ... as Sir Walter de Mellerby

==References==
 

==External links==
* 
* 
* 

 
 
 
 

 
 