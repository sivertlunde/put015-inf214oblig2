Ecstasy (film)
 
{{Infobox film
| name           = Ecstasy
| image          = Ecstasy 1933 Lobby Card.jpg
| border         = 
| alt            = 
| caption        = Original lobby card
| director       = Gustav Machatý
| producer       = {{Plainlist|
* Moriz Grunhut
* Gustav Machatý
}}
| writer         = Jacques A. Koerpel
| screenplay     = {{Plainlist|
* Frantisek Horký
* Gustav Machatý
}}
| story          = Robert Horky
| starring       = {{Plainlist|
* Hedy Lamarr
* Aribert Mog
* Zvonimir Rogoz
}}
| music          = Giuseppe Becce
| cinematography = {{Plainlist|
* Hans Androschin
* Jan Stallich
}}
| editing        = Art Jones
| studio         = Elektafilm
| distributor    = Albert Deane
| released       =  
| runtime        = 82 minutes
| country        = Czechoslovakia
| language       = {{Plainlist|
* German
* Czech
* French
}}
| budget         = 
| gross          = 
}} romantic drama marries a wealthy but much older man. After abandoning her brief passionless marriage, she meets a young virile engineer who becomes her lover. Ecstasy was filmed in three language versions—German, Czech, and French. Barton 2010, p. 30. 

Ecstasy was highly controversial in its time because of scenes in which Lamarr swims in the nude and runs through the countryside naked. It is also perhaps the first non-pornographic movie to portray sexual intercourse and female orgasm, although never showing more than the actors faces. The film was wrongly celebrated as the first motion picture to include a nude scene, rather than the first to show sexual intercourse, for which it has a better claim. Robertson, Patrick (2001). Film Facts. New York: Billboard Books, p. 66. 

==Plot==
Emil (Zvonimir Rogoz), a precise, orderly older man, carries his happy new bride Eva (Hedy Lamarr) over the threshold of their home. (He has great difficulty opening the lock on the front door, trying key after key.) She is greatly disappointed on her wedding night; he does not even come to bed. After living in the unconsummated marriage for a while, she cannot bear it any longer and runs back to her father (Leopold Kramer), a horse breeder. A divorce is issued.

One day, she goes horse riding. She goes skinny dipping, leaving her clothes on the horse, only to have it wander off, attracted by another locked in a corral. She chases after it all over the countryside. The horse is finally caught by Adam (Aribert Mog), the virile young foreman or engineer of a road construction gang. Seeing this, she hides in the bushes, where he finds her. At first, she is ashamed of her nudity, but then she glares at him in defiance. He gives her back her clothes. When she tries to leave, she hurts her foot. At first, she resists his efforts to help, then accedes.

That night, she cannot stop thinking about him. Finally, she goes to his isolated residence. After some hesitation, they embrace and spend the night together. Her pearl necklace is removed and she forgets to take it with her the next morning.

When she returns home, she finds an unwelcome visitor, her ex-husband, who has been waiting for her all night. He tries to reconcile with her, but she tells him that it is too late. He leaves.

By chance, while driving away, he encounters his rival. Adam guides him through the construction and asks for a ride into town. On the way, he shows the necklace, which Emil recognizes. Emil considers driving into an approaching train at a crossing, but thinks better of it. 

That night, he sits alone in a hotel room, while a fly tries futilely to get out through a closed window and several others are shown trapped in flypaper. Downstairs, Adam and Eva are dancing when Emil shoots himself. Adam does not know of the connection between Emil and Eva, and she does not tell him.

The young couple had planned to take the train to Berlin. While waiting at the station, Adam falls asleep and a distraught Eva leaves on a different train without him. A sad Adam returns to his work. Eva is shown in Adams daydream happily holding a baby.

==Cast==
* Hedy Lamarr as Eva Hermann (as Hedy Kiesler)
* Aribert Mog as Adam
* Zvonimir Rogoz as Emile
* Leopold Kramer as Evas father
* Emil Jerman as Evas husband (Czech voice)
* Jirina Steimarova as Typist
* Bedrich Vrbský as Evas father (Czech voice) 
* Jirina Stepnicková as Eva (Czech voice) 
* Antonín Kubový as Vagrant
* Karel Macha-Kuca as Lawyer
* Eduard Slégl as Vagrant
* Pierre Nay as Adam (French voice) 
* André Nox as Evas father (French voice)
* Jan Sviták as Dancer on the terrace

==Production==
Ecstasy was filmed in the summer of 1932, with a German language script that contained only five pages. Shearer 2010, p. 27.  The outdoor scenes were filmed in and around Prague, Czechoslovakia. The indoor scenes were filmed in the Atelier Schönbrunn studios in Vienna, Austria.

==Release==
The world premiere of the film took place on 20 January 1933 in Prague, Czechoslovakia. In Austria, the film was released on 14 February, but due to censorship problems, German cinemas did not show it until 8 January 1935.
 condemned by the Legion in 1933, one of the first foreign films condemned by the Legion. 
 Hays Code seal of approval which would allow it a wide American release. Joseph Breen called the picture "highly—even dangerously—indecent" in an inter-office memo to Will H. Hays,    and told the producers:
 
 state censor boards such as New York approved the film but most others either only allowed it with restrictions, demanded substantial cuts, or in the case of Pennsylvania, banned it altogether. 

==References ==
;Citations
 
;Bibliography
 
*  
*  
*  
*  
*  
 

==Further reading ==
*Gardner, Gerald. The Censorship Papers: Movie Censorship Letters from the Hays Office, 1934 to 1968. Dodd Mead 1988 ISBN 0-396-08903-8

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 