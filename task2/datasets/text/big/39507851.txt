Alexander and the Terrible, Horrible, No Good, Very Bad Day (film)
 
{{Infobox film
| name = Alexander and the Terrible, Horrible, No Good, Very Bad Day
| image = Alexander and the Terrible, Horrible, No Good, Very Bad Day poster.jpg
| border = yes
| alt =
| caption = Theatrical release poster
| director = Miguel Arteta
| producer = Shawn Levy Dan Levine Lisa Henson
| screenplay = Rob Lieber 
| based on =  
| starring = Steve Carell Jennifer Garner Ed Oxenbould Dylan Minnette Kerris Dorsey
| music = Christophe Beck
| cinematography = Terry Stacey Pamela Martin
| studio = Walt Disney Pictures 21 Laps Entertainment The Jim Henson Company Walt Disney Studios Motion Pictures
| released =  
| runtime = 81 minutes  
| country = United States
| language = English
| budget = $28 million 
| gross = $100.7 million   
}}

Alexander and the Terrible, Horrible, No Good, Very Bad Day is a 2014 American comedy film directed by Miguel Arteta from a screenplay written by Rob Lieber. The film stars Steve Carell, Jennifer Garner, and Ed Oxenbould, and is based on Judith Viorst and Ray Cruzs 1972 Alexander and the Terrible, Horrible, No Good, Very Bad Day|childrens book of the same name. 

Co-produced by Shawn Levy and Lisa Henson for Walt Disney Pictures through their respective production companies, 21 Laps Entertainment and The Jim Henson Company, the film was released in North America on October 10, 2014.  The film received mixed to positive reviews from critics and was a box office success, grossing $100 million worldwide against a $28 million budget.

==Plot==
The film follows the exploits of Alexander Cooper (Ed Oxenbould), an ordinary 11-year-old boy, and his "terrible, horrible, no good, very bad day." He is left out by his family; his older brother, Anthony (Dylan Minnette), his older sister, Emily (Kerris Dorsey), his mother, Kelly (Jennifer Garner), his father, Ben (Steve Carell), and his baby brother, Trevor (Elise/Zoey Vargas).
 gum in Peter Pan, Kelly is working for a publication company that is publishing a new childrens book, and Ben, who has been unemployed for several months, has landed a job interview as a game designer for a video game company.

That same morning, Alexander attends school where he experiences another series of misfortunes, as well as finding out that his friends, including his crush, Becky Gibson (Sidney Fullmer), and best friend, Paul (Mekai Curtis), will all be attending the birthday party of Phillip Parker (Lincoln Melcher) instead of his, due to Philips expensive party entertainment and popularity. He tries to tell his family how miserably his day has gone, but none of them even listen to what he says. That evening, Anthony upsets Celia during a phone call while yelling at Alexander saying he is an "idiot brother", and Emily rehearses her stage lines in Kellys Volvo while leaving its light on. Alexander makes himself a makeshift birthday sundae and wishes his family could experience the disappointments he does every day.

The next morning, Alexander wakes up to find his family in disarray and chaos erupts; his parents have overslept, Emily has a cold, and Anthony has found out that he and Celia broke up. The battery in Kellys car is dead, because Emily left its light on all evening long while rehearsing, therefore Ben has to take Trevor with him to the interview after dropping Kelly off at work. At school, Paul gives Alexander good news: Philip has cancelled his birthday party due to illness, and calls his father, asking him to plan a party for him. Kelly is informed of an embarrassing typo in the book they are publicizing, and needs to stop Dick Van Dyke from reading it at public reading later. Ben takes Trevor along to the office interview and meets Greg (Donald Glover) who seems impressed at his credentials, although they decide to hold another meeting after Trevor ingests a highlighter. Meanwhile at school, Anthony finds out that Celia has come back with him. Anthony is so happy about what happened, that he jumps up and accidentally slaps a banner which is held up by two trophy display cases between him, causing them to fall and smash, leading to his teacher taking him to the principals office and suspending him.
 family minivan, and fail his driving exam. Alexander confesses that the familys day was jinxed because of his ill wishes the night before, and apologizes, but Ben says "Apology not accepted" because they can still have some good in it. Afterwards, they all go to Emilys play, which is inadvertently sabotaged by her impaired behavior due to her getting excessively drunk after overdosing on the cough syrup. Afterwards, the game design firm calls Ben and asks him to meet them at an authentic Japanese hibachi restaurant called Nagamaki for another meeting. The family, joined by Celia, goes to the restaurant in their badly damaged minivan, where Ben accidentally sets his shirt on fire at the grill, embarrassing him to the employers. The whole family consoles him, admitting that they will overcome whatever else the day has in store for them. Alexander states, "You just gotta have the bad days so you can love the good days even more", and Anthony suddenly decides not to go to the prom with Celia, stating that his family is more important and they also break up completely. 
 Australian petting zoo for Alexanders birthday party. They then decide to pitch in to try and salvage the day by helping host it. Ben and Kelly receive good news: he got hired for the game design job, and she has been informed that the celebrity reading went viral and has created publicity for the book. Reunited, Ben then brings out the cake, with Alexander wishing for more days like the one they shared together.

==Cast==
* Ed Oxenbould as Alexander Cooper 
* Steve Carell as Ben Cooper, Alexanders father   
* Jennifer Garner as Kelly Cooper, Alexanders mother 
* Dylan Minnette as Anthony Cooper, Alexanders older brother   
* Kerris Dorsey as Emily Cooper, Alexanders older sister 
* Bella Thorne as Celia Rodriguez, Anthonys girlfriend 
* Elise & Zoey Vargas as Trevor Cooper, Alexanders baby brother
* Sidney Fullmer as Becky Gibson, Alexanders crush
* Megan Mullally as Nina 
* Toni Trucks as Steph
* Donald Glover as Greg   
* Joel Johnstone as Logan 
* Jennifer Coolidge as Ms. Suggs, Anthonys driving instructor 
* Samantha Logan as Heather 
* Dick Van Dyke as Himself Thunder From Down Under as Themselves
* Mekai Curtis as Paul, Alexanders friend
* Lincoln Melcher as Phillip Parker
* Mary Mouser as Audrey Gibson
* Reese Hartwig as Elliott Gibson
* Martha Hackett as Mrs. Gibson
* Burn Gorman as Mr. Brand

==Production== 21 Laps and Lisa Henson from The Jim Henson Company. Steve Carell joined in April 2012, to star as Ben, Alexanders father.  In October 2012, the project was picked up by Walt Disney Pictures,  after Fox was reportedly "uncomfortable with the budget."  By February 2013, Cholodenko had left the project,  and a month later, it was reported that Miguel Arteta was in talks with Disney to replace Cholodenko. 

In April 2013, Jennifer Garner was in talks to play Alexanders mother.  In June 2013, Disney set the release date for October 10, 2014, and confirmed that Carell and Garner were cast as Alexanders parents.  The same month, Disney announced the casting of Ed Oxenbould as Alexander.    In July 2013, Bella Thorne was cast in the film as Alexanders older brothers girlfriend.    Joel Johnstone,    Megan Mullally and Jennifer Coolidge joined the cast a month later.   
 Pasadena and Arcadia, California|Arcadia, the San Fernando Valley, and Melody Ranch in Newhall, Santa Clarita, California|Newhall.  Filming lasted through October 2013. 

==Release==
The film officially premiered at the El Capitan Theatre in Los Angeles on October 6, 2014. 

===Home media===
Alexander and the Terrible, Horrible, No Good, Very Bad Day was released by Walt Disney Studios Home Entertainment on DVD and Blu-ray on February 10, 2015. 

==Reception==
=== Box office === Gone Girl and Dracula Untold.    In its second weekend, the film dropped to number four, grossing an additional $11.4 million.  In its third weekend, the film dropped to number seven, grossing $7.1 million.  In its fourth weekend, the film dropped to number eight, grossing $6.5 million. 

=== Critical response ===
The film has received mixed to positive reviews from critics. The review aggregator website Rotten Tomatoes gave the film a rating of 62%, based on 105 reviews, with an average rating of 5.8/10. The websites consensus reads, "Affably pleasant without ever trying to be anything more, Alexander and the Terrible, Horrible, No Good, Very Bad Day is a fine—albeit forgettable—family diversion."  Metacritic, which uses a weighted average, assigned a score of 54 out of 100, based on 28 critics, indicating "mixed or average reviews". 

  gave the film two and a half stars out of four, saying "The movie is so over-the-top that it makes little narrative sense, but its often successful in its naked pursuit of belly laughs."  Katie Rife of The A.V. Club gave the film a B, saying "Alexander is a watchable, affable, pretty good, well-done kids movie buoyed by a humorous script and talented cast."  Bill Goodykoontz of The Arizona Republic gave the film three out of five stars, saying "It turns out the film is not terrible or horrible or very bad. No good? Not that, either." 

Claudia Puig of USA Today gave the film two and a half stars out of four, saying "It may have the years longest title, but Alexanders movie is not terrible, horrible, or even half bad. In fact, Alexander and the Terrible, Horrible, No Good, Very Bad Day is a pleasant, entertaining way to spend just under 90 minutes, particularly if accompanied by children."  Sandie Angulo Chen of The Washington Post gave the film three out of four stars, saying "Even the bathroom humor is forgivable when the end result is a crowd-pleasing comedy and a surprisingly entertaining treat for the whole family."  Rafer Guzman of Newsday gave the film two and a half stars out of four, saying "Alexander and the Terrible, Horrible, No Good, Very Bad Day, a Disney film, stretches the book thinner than pizza dough and feels about as nutritious. Still, its intentions are good and so is its cast, particularly Ed Oxenbould, a bright-eyed, expressive 13-year-old making his screen debut as Alexander Cooper."  Bruce Demara of the Toronto Star gave the film three out of four stars, saying "Director Miguel Arteta, whose previous work is a mixed bag of television and film, gets almost everything right here, including bringing together a solid cast."  A.O. Scott of The New York Times gave the film a negative review, saying "Alexander and the Terrible, Horrible, No Good, Very Bad Day is the latest example of a wonderful children’s book turned into a mediocre movie. This kind of thing happens so frequently — exceptions like Where the Wild Things Are and, arguably, Shrek prove the rule upheld by every recent big-screen Dr. Seuss adaptation — that you could almost believe that there is malice involved." 

Betsy Sharkey of the Los Angeles Times called the film "Not so terribly horrible. Not so terribly terrific either."  Tom Russo of The Boston Globe gave the film a positive review, saying "What the filmmakers come up with is a modestly likable mix of zany and gently warmhearted, even if they overdo both elements at times."  David Hiltbrand of The Philadelphia Inquirer gave the film two out of four stars, saying "Its a film where you start chuckling as soon as someone says something like, "I just want everything to be perfect tonight."  Adam Graham of The Detroit News gave the film a B-, saying "Alexander wont change your day, but its not terrible, horrible, no good or very bad, either."  Calvin Wilson of the St. Louis Post-Dispatch gave the film a three out of four stars, saying "Arteta keeps the action speeding along while eliciting spot-on performances. Carell is at his discombobulated best, and Garner anchors the proceedings with aplomb."  Lindsey Bahr gave the film a B, saying "Alexander is pleasantly devoid of the vulgarity and too-current pop culture references that are the default mode for many contemporary live-action kids pics, and its earnest celebration of family gives the movie a comforting throwback vibe."  Bruce Ingram of the Chicago Sun-Times gave the film two and half stars out of four, saying "Disney’s bland comedy Alexander and the Terrible, Horrible, No Good, Very Bad Day might have been a little more entertaining if it had been a little more, terrible, horrible, no good and so forth."  Joe Neumaier of the New York Daily News two out of five stars, saying "Just another loud, boy-centric comedy aimed at ’tweens. The movie turns a slight children’s book — in this case, Judith Viorst’s 1972 fave, from which it takes mainly the title — into a charmless mishmash." 

==Soundtrack==
{{Infobox album  
| Name        = Alexander and the Terrible, Horrible, No Good, Very Bad Day (Music from the Motion Picture)
| Type        = EP
| Artist      = Various artists
| Cover       = 
| Released    = October 7, 2014
| Recorded    =  Pop
| Length      =  Walt Disney
| Producer    = 
| Reviews     = 
}} The Vamps, Kerris and Justine Dorsey, The Narwhals, Charles William and IDK & The Whatevs. 

{{Track listing
| collapsed       = no
| headline        =
| extra_column = Artist
| title1 = Hurricane The Vamps
| length1 = 3:17
| title2 = Best Worst Day Ever
| extra2 = Kerris Dorsey and Justine Dorsey
| length2 = 3:35
| title3 = We Are the Ones (Own the World)
| extra3 = Charles William
| length3 = 3:08
| title4 = Surf Surf Dont Drown The Narwhals
| length4 = 3:27
| title5 = Perfect World
| extra5 = IDK and The Whatevs
| length5 = 3:06
| title6 = Suite from Alexander and the Terrible, Horrible, No Good, Very Bad Day
| extra6 = Christophe Beck
| length6 = 3:20
}}

==References==
 

==External links==
*   at Disney.com
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 