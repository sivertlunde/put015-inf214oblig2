The Fairies' Hallowe'en
 
{{Infobox film
| name           = The Fairies Halloween
| image          = The Fairies Halloween 1910.jpg
| caption        = A surviving film still
| director       =
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = Motion Picture Distributing and Sales Company
| released       =  
| runtime        =
| country        = United States English inter-titles
}}
 silent short short comedy produced by the Thanhouser Company. The plot focuses on Marie, played by Marie Eline, as a young girl who plays with a jack-o-lantern crafted by her father. When she falls asleep, the girl dreams that she is invited by the Fairy Queen to their Halloween party. Her doll and pumpkin are given life and she enjoys the party. As she awakes, she mourns that it was all a dream, but was content for the experience. The film was advertised as a trick film and received mixed reception by reviewers. It was released on October 28 1910, but records show that the film was still being shown in 1913. The film is presumed lost film|lost.

== Plot == queen of the fairies came to her and invited her to a Halloween party that her loyal subjects were giving in their woodland retreat. And so that Marie would be perfectly happy, not feel embarrassed among strangers, the queen asked Dollie and Pumpkin to be her guests also. And, as a further mark of favor, she waved her hand, and Dollie and Pumpkin could run around and talk as well as Marie could. It certainly made it more enjoyable for the little girl. A fairy Halloween party is one of the finest things going in the amusement line, and only very nice and very good little girls are invited to them. As Marie filled these requirements, she had a perfectly glorious time, but was inclined to weep when she woke up later in her own little bed, and found that her toys had lost their miraculous gift of life. But it consoled her somewhat to have them, for they served as reminders of her wonderful evening. And perhaps the fairies will ask her to attend another party later. If they do, she has decided that Pumpkin and Dollie must have their shares of the good times as before." 

== Production == George Middleton, Grace Moore, John W. Noble, Anna Rosemond, Mrs. George Walters.  The film was advertised as being a trick film, employing the use of clever camera techniques to create special effects. 

== Release and reception ==
The single reel comedy, approximately 1,000 feet long was released on October 28, 1910.    The film had a wide national release, with theaters showing the film in Indiana,  North Carolina,  Arizona,  Maryland,  Kansas,  and Pennsylvania.  One advertisements shows the film still being promoted in 1913 and a possible showing in 1917.   The film was also shown in Vancouver, Canada by the Province Theatre. 

  was critical of the production, stating "This sounds as if it ought to be very clever, but aside from the work of the little heroine and the dance of the fairies it is entirely uninspired. It needs a drastic application of imagination. The broad idea is excellent, but the details of the management indicate impoverished resources. When the aid of fairies is invoked it opens up all sorts of avenues of fanciful treatment, but this film doesnt get away from stolid realities. For example, nobody could imagine anything more earthly than the outside of the house from which the little girl escaped with the fairy queen and her comrades. That should have been treated fancifully. The events pictured dont amount to anything; there is a sameness about them, nothing surprising happens, and not very much that is pretty. Worst of all is the poetry(?), which is interpolated to explain the narrative; that is beyond criticism. The Thanhouser Company has missed good opportunities in this film."  The strange inclusion of the question mark after poetry is not a note by Bowers, for it was actually written into the review itself in the edition. Though no other review mentions poetry.

== References ==
 

 
 
 
 
 
 
 
 
 