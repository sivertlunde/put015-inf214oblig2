Stolen (2009 documentary film)
 
{{Infobox film
| name           = Stolen
| image          = Stolen-poster.jpg
| caption        = Promotional poster
| director       = Violeta Ayala Dan Fallshaw
| producer       = Tom Zubrycki Violeta Ayala Dan Fallshaw Deborah Dickson
| writer         = Violeta Ayala and Dan Fallshaw
| starring       =
| cinematography = Dan Fallshaw and Violeta Ayala
| editing        = Dan Fallshaw
| distributor    =
| released       =  
| runtime        = 78 minutes
| country        = Australia
| language       = Spanish language|Spanish, Hassaniya, English
| budget         =
| gross          =
}}
Stolen is a 2009 Australian documentary film that uncovers slavery in the Sahrawi refugee camps controlled by the Polisario Front located in Algeria and in the disputed territory of Western Sahara controlled by Morocco, written and directed by Violeta Ayala and Dan Fallshaw. It had its world premiere at the 2009 Sydney Film Festival,  where a controversy started after one of the participants in the documentary, Fetim, a black Sahrawi, was flown to Australia by the Polisario Liberation Front to say she wasnt a slave. The POLISARIO, avowing that it doesn’t condone slavery and needing to safeguard its image on the world stage to support its independence fight, began an international campaign against the film. It put out its own video denouncing Stolen, in which several people who Ayala and Fallshaw interviewed say they were coerced or paid by the Australian duo.  On May the 2nd 2007, while filming in the refugee camps Ayala and Fallshaw were detained by the Polisario Front and Minurso and the Australian ministry of foreign affairs negotiated their release. "The Polisario Front officials criticised the interest the two journalists took in black members of the Sahrawi population, Reporters Without Borders has learned. Ayala told the press freedom organisation that she saw cases of enslavement. “The fact that they are fighting for their independence does not mean that Polisario’s leaders can allow themselves to commit such human rights violations,” she said. “It is our duty as journalists to denounce such practices. We originally went there to work on the problem of separated families. But during our stay, we witnessed scenes of slavery.” 

In 2008 Human Rights Watch published a   confirming that vestiges of slavery still affects the black minority in the Polisario refugee camps and in Western Sahara, the report included a   signed by the POLISARIO’s Ministry of Religious and Cultural Affairs.
 Singapore IFF, Cleveland IFF, Norwegian Short Film Festival, Frontline Club Liberation Season, and Amnesty International Film Festival.
 WGBH to carry out their own investigation and present the "Stolen" two-hour special". 

Stolen premiered nationwide on February 26 on   with the directors, followed by a panel discussion whether slavery exists in Western Sahara with Eric Goldstein (Deputy Director, Middle East and North Africa Division Human Rights Watch), Madeline Campbell (Professor of Urban Studies, Worcester State University) and   (Mauritanian Anti-Slavery Activist). 

==Overview==
Is it true my white grandmother beat you as a child? asks 15-year-old Leil. Her mother, Fetim, looks at her hard, still chewing her lunch. They sit at a table, a TV behind them, as well as a doorway opening onto a bright white daylight. Leil continues, “Violeta already knows,” as the camera cuts to filmmaker Violeta Ayala, seated across from them. Her face turns cloudy as she listens: “You’ll be in trouble, by saying we were beaten,” cautions Fetim. Again, the camera shows her instructing her daughter, “It’s always been that slaves are beaten from a young age.”
 refugee camp in the Algerian desert as a child some 30 years ago, leaving behind her mother Embarka and her siblings. With some 27,000 on the waiting list, the fact that Fetim and Embarka have been selected seems miraculous.
 
And yet: this story is now reframed, as Ayala and Fallshaw learn that their subjects are not only refugees, but also slaves. The filmmakers can’t begin to imagine the complications that follow this discovery.
 
Their questions elicit astonishing and also cryptic stories, as Fetim and Leil, as well as Fetim’s cousin Matala, sort through what is safe to tell “the foreigners.” 
 
As the film reports, the Polisario Liberation Front, a nationalist organization backed by Algeria, has been fighting with Morocco over Western Sahara for the last 34 years. Neither the Moroccan government nor the Polisario want stories about slavery documented. And yet, with at least 2 million black people living in slavery in North Africa, STOLEN insists, telling such stories is only a first step to bringing out slavery’s end.

As a result of the filmmaking process whereby the Polisario and the Moroccan government attempt to confiscate and steal the tapes, STOLEN has a fragmented structure that pieces together conversations. The fragments, however, allow the black Sahrawi’s stories to be heard, be it aloud or as a whisper.

Indeed, Fetim’s initial revelation that she has a “white mother,” Deido, surprises Ayala, who wonders how they ended “up together, with so much racism in the past?”. Deido explains, sort of. “Saharan people are not all the same...Some of them buy black people and own them, others free them, but keep them as their family. We don’t talk about this anymore.”
  
A friend of Leil, Tizlam, is also outspoken concerning what it means to be a slave. “You’re just scratching the surface,” she says, her face at once poised and fierce in dim shadows. “They come and take the children and the parents can’t say anything, they have no rights.” Her grandmother concurs. “There is no law for us,” Tizlam says, “What we want is for this not to exist. It should be erased, it should be from the past, not the present or the future.” Ayala and Fallshaw describe their growing concern, not only for their own safety but also for “all the people who trusted us with their stories.”
 
As the Polisario catches wind of the new topic for Ayala and Fallshaw’s film, they begin to worry about their safety and those in the film. Their worries are well founded, as they are   by the Polisario. The filmmakers bury their tapes in the desert—an apt and awful metaphor for the experiences they’ve heard about—and then escape to Paris, where they pursue the story, hoping to recover their material and make public what they’ve witnessed. A phone call with Leil reveals, however, that their own ambitions and hopes don’t matter much: Leil cries, “Trying to do good, you did bad. Now the police are all over us.” As Ayala ponders this notion in voiceover, that “without intending to, we got Leil and Fetim in a lot of trouble,” the film structure makes clear the problem: she’s in a hotel room, at a distance. None of us can know what Leil and Fetim are experiencing—off camera. 
 
The film traces how Ayala and Fallshaw come to know the ongoing complexities of slavery. As it is denied by most North African regimes (in Mauritania, Mali, and Senegal, as well as Algeria and Morocco) and described here by Ursula Aboubacar, the Deputy Director of the United Nations High Commission for Refugees, as “a cultural issue that is existing.” That is, as Aboubacar puts it, the UN can only “combat” the practice by bringing it to the attention of local police forces, the Polisario included. Ayala is horrified by the lack of power wielded by the UN, or anyone else, it seems. Indeed, as Tizlam has said, “There is no law for us.”

==Controversy==

It can be noted that this film has generated significant controversy. One of the characters in the documentary, Fetim a refugee who live in the camps, travelled from North Africa to the premiere of the documentary at the Sydney Film Festival in Australia to denounce her portrayal in the film and to say publicly that slavery doesnt exist in the refugee camps. Her trip to Australia was paid for by the Polisario Front, the organisation who controls the refugee camps.  The Polisario Front have strongly opposed the film and deny the existence of slavery in the camps they administer.

Some Polisario supporters have claimed the translations from the non-written dialect Hassaniya Arabic to English are inaccurate. The film has been broadcast on the US public broadcaster, PBS. While disputes over the accuracy of the Hassaniya to English translations continue, much of the discussion about slavery in the film is conducted in Spanish.

In a letter from Mr. Antonio Guterres, UN High Commissioner for Refugees, to Mr. Abdelaziz, the President of the SADR (Polisario), dated June 22, 2009, Mr. Guterres expressed "regret that in the film of Violeta Ayala and Dan Fallshaw, the comments of an official of the HCR have been presented out of their context."  He went on to state: "In the complete interview, of about 90 minutes long .  .  .    reiterated strongly that if certain residual practices of slavery could still prevail in the subregion of West Africa, she had no knowledge of such practices in the refugee camps of Tindouf."  He further commented that " he HCR has established for a long time a presence in the refugee camps of Tindouf.  It does not have any information that practices similar to slavery have taken place in the camps.  In fact, no occurrence of this practice has been brought to the attention of the HCR .  .  .  ." This statement is contradicted in the   given to the filmmakers by Mrs Aboubacar, Deputy Director at the UNHCR for the Middle East and North Africa, who clearly states the UN is aware of the practice of slavery in the refugee camps.
 Tindouf refugee camps.

In the panel discussion that followed the airing of the film Stolen by PBS World as part of the AfroPoP series, Eric Goldstein, the Deputy Director of Human Rights Watch in charge of the Middle East/North Africa desk noted that when he had brought to the attention of officials in the camp an incident in which a dark skinned woman had been denied the ability to marry, they immediately took steps to remedy the situation. He also noted that this practice was the only vestige of slavery that he had found in his investigation of the camps. Bakary Tandia, a human rights activist in the movement against slavery, originally from Mauritania, commented in the post film discussion that Stolen depicts slavery in the POLISARIO camps as it is practiced in Mauritania.
 Mohamed Kamal Fadel. Gonzalez revisited the camps and re-interviewed many of the participant, some of whom had been jailed by the Polisario for speaking to the filmmakers about slavery.  The stories they told Mr. Gonzalez differ significantly from the accounts given to the filmmakers of Stolen.

There is also a critique of Stolen published at http://awsa.org.au/wp-content/uploads/2010/07/critique-v3-pdf.pdf. The Australian West Sahara Association (AWSA) who published the critic has been a long time supporter of the Saharawi refugee camps and the Polisario Front.

== Awards ==
* Best Feature Documentary Prize at the 2010 Pan African Film and Arts Festival, Los Angeles, USA 
* Best Editing and Special Mention at its NZ premiere at the Documentary Edge Festival 2010 
* Best International Feature - Rincon International FF, Puerto Rico 2010 
* Silver Olive at the XV International TV Festival Bar 2010 in Montenegro 
* Best Film at the 2010 Festival Internacional de Cine de Cuenca, Ecuador 
* Grand Prix of The Art of Document at the 2010 Multimedia Festival the Art of the Document, Warsaw, Poland 
* Best Documentary at the 2010 Africa International Film Festival, Port Harcourt, Nigeria 
* Golden Oosikar for Best Documentary at the 2010 Anchorage international Film Festival, Alaska, USA 
* Honorable Mention at the 2010 Ojai Film Festival, Ojai, USA 
* Bronze Audience Award at the 2010 Amnesty International Film Festival, Vancouver, Canada 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 