Day of the Evil Gun
 
{{Infobox film
| name           = Day of the Evil Gun
| image_size     = 
| image	=	Day of the Evil Gun FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Jerry Thorpe
| producer       = Jerry Thorpe
| writer         = Charles Marquis Warren Arthur Kennedy
| music          = Jeff Alexander
| cinematography = W. Wallace Kelley Alex Beaton
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} western starring Arthur Kennedy, and Dean Jagger. It was directed by Jerry Thorpe.

==Plot== Arthur Kennedy) to find his captured family. Warfield is a former gunman trying to forget his violent past. Forbes, a decent and humane rancher, is also in love with Warfields wife and feels guilty that he let her be captured without trying to help. An Indian trader (Dean Jagger) aids them in their quest. Along the way they are encounter Mexican bandits, Apache warriors, renegade soldiers, and a town blighted by cholera. While Warfield in his cynicism immediately recognizes malevolent motives, each encounter results in further stripping Forbes of his humanity and making him accustomed to killing. The search eventually leads them to the Apache camp where they rescue the woman and children. Safely back home, Forbes challenges Warfield (who is now unarmed, trading his gun to a storekeeper for clothing for his family) to a duel, but he refuses. Forbes shoots Warfield in the back, wounding him.  Before he can finish him off, he is shot and killed by the storekeeper using Warfields gun.

==Cast==
* Glenn Ford as Lorne Warfield Arthur Kennedy as Owen Forbes
* Dean Jagger as Jimmy Noble John Anderson as Captain Jefferson Addis
* Paul Fix as Sheriff Kelso
* Nico Minardos as Jose Luis Gomez de la Tierra y Cordoba DeLeon
* Harry Dean Stanton as Sergeant Parker
* Pilar Pellicer as Lydia Yearby
* Parley Baer as Willford
* Royal Dano as Dr. Eli Prather
* Ross Elliott as Reverend Yearby
* Barbara Babcock as Angie Warfield
* James Griffith as Storekeeper - Hazenville

==External links==
*  
*  

 
 
 

 