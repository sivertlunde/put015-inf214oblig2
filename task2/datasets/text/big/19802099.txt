Dronningens vagtmester
{{Infobox film
| name           = Dronningens vagtmester
| image          = 
| caption        = 
| director       = Johan Jacobsen
| producer       = Annelise Hovmand Johan Jacobsen
| writer         = Carit Etlar Annelise Hovmand
| starring       = Poul Reichhardt
| music          = 
| cinematography = Marko Yaacobi
| editing        = Erik Aaes
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Dronningens vagtmester is a 1963 Danish drama film based on the novel of the same name by Carit Etlar. It was directed by Johan Jacobsen and starring Poul Reichhardt. It followed the 1961 film Gøngehøvdingen (film)|Gøngehøvdingen.

==Cast==
* Poul Reichhardt - Ib
* Jens Østerholm - Svend Gønge
* Birgitte Federspiel - Kulsoen
* Gunnar Lauring - Kaptajn Esner
* Ghita Nørby - Inger
* Vivi Bach - Adelsdatteren (as Vivi Bak)
* Karin Nellemose - Adelsfruen
* Ove Sprogøe - Tam
* Pauline Schumann - Dronning Sophie Amalie
* Poul Finn Poulsen - Palle
* Henrik Wiehe - Junker Rud
* Bent Vejlby - Jens Jerntrøje
* Svend Johansen - Svensk fenrik William Rosenberg - Dronningens 1. rytter
* Benny Juhlin - Olav, svensk dragon
* Jørgen Kiil - Rosenkrantz
* Willy Rathnov - En gønge
* Karen Berg - Mette Gyde
* Niels Dybeck - Sveriges fægtekonge
* Ole Wegener 
* Per Wiking - Gønge
* William Kisum
* Erik Kühnau - Gønge
* Carl Nielsen
* Kurt Erik Nielsen
* Bjørn Spiro - Svensk soldat
* Alex Suhr

==External links==
* 

 
 
 
 
 
 
 