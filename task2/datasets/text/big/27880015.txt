Habla, mudita
{{Infobox film
| name           = Habla, mudita
| image          = HablaMudita1973Poster.jpg
| caption        = Spanish poster
| director       = Manuel Gutiérrez Aragón
| producer       = 
| writer         = Manuel Gutiérrez Aragón José Luis García Sánchez
| screenplay     = 
| story          = 
| based on       =  
| starring       = José Luis López Vázquez
| music          = 
| cinematography = Luis Cuadrado
| editing        = Pablo González del Amo
| studio         = 
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}}
 Best Foreign Language Film at the 46th Academy Awards, but was not accepted as a nominee. 

==Cast==
* José Luis López Vázquez - Rodrigo
* Kiti Manver - La Muda
* Francisco Algora - El Mudo
* Hanna Haxmann
* Francisco Guijar
* Susan Taff
* Marisa Porcel
* Rosa de Alba
* Edy Lage
* María de la Riva
* Pedrín Fernández
* Lucy Tiller
* Manuel Guitián
* Carmen Liaño
* Vicente Roca

==See also==
* List of submissions to the 46th Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 