Tailspin Tommy in the Great Air Mystery
{{Infobox film
| name = Tailspin Tommy in the Great Air Mystery
| image = Tailspin Tommy in the Great Air Mystery.jpg
| image size =
| caption = Ray Taylor
| producer = Henry MacRae
| writer = Raymond Cannon (screenplay) Ella ONeill (screenplay) Basil Dickey Robert Hershon George H. Plympton Hal Forrest (comic strip)
| narrator =
| starring = Clark Williams Jean Rogers Noah Beery, Jr.
| music = David Klatzkin Clifford Vaughan Oliver Wallace Richard Fryer John Hickson William A. Sickner
| editing = Albert Akst Saul A. Goodkind Alvin Todd Edward Todd
| distributor = Universal Pictures
| released =  
| runtime = 15 chapters (300 min)
| country = United States
| language = English
| budget =
}} Universal Serial movie serial based on the Tailspin Tommy comic strip by Hal Forrest and starring Clark Williams, Jean Rogers and Noah Beery, Jr.. The picture was the 96th of the 137 serials released by the studio (the 28th of which to be made with Sound film|sound). 

==Plot==
"Tailspin" Tommy (Clark Williams) and his fellow pilots prevent a group of corrupt businessmen, Manuel Casmetto (Herbert Heywood) and Horace Raymore (Matthew Betz ) from stealing Nazil Islands oil reserves. They are aided in their efforts by news reporter Milt Howe (Pat J. OBrien). When Tommy is triumphant, he also finds he has a movie contract.

==Chapter titles==
# Wreck of the Dirigible
# The Roaring Fire God
# Hurled from the Skies
# A Bolt from the Blue
# The Torrent
# Flying Death
# The Crash in the Clouds
# Wings of Disaster
# Crossed and Double Crossed
# The Dungeon of Doom
# Desperate Changes
# The Last Stand
 Source:  

==Cast==
* Clark Williams as "Tailspin" Tommy Tompkins
* Jean Rogers as Betty Lou Barnes, one of "Tailspin" Tommys companions
* Delphine Drew as Inez Casmetto
* Noah Beery, Jr. as Skeeter Milligan, one of "Tailspin" Tommys companions
* Bryant Washburn as Ned Curtis, Bettys uncle and a heroic oil tycoon
* Pat J. OBrien as Milt Howe, a masked mystery plot known as The Eagle and El Condor
* Herbert Heywood as Manuel Casmetto, villainous half-brother of Don Alvarado Casmetto
* Matthew Betz as Horace Raymore, villainous oil tycoon
* Paul Ellis as Enrico Garcia, Don Casmetto secretary and Manuel Casmettos henchman
* Harry Worth as Don Alvarado Casmetto, Nazil Islands ruler
* Manuel López as Gomez
* Charles A. Browne as Paul Smith
* James P. Burtis as Bill McGuire, a reporter Frank Mayo as the Dirigible Captain
* Frank Clarke as a flyer, billed as "Air Ace Frank Clark"

==Production== Fairchild cabin monoplane.  A mock-up of a dirigible included an interior section. 

===Stunts===
* Frank Clarke, stunt pilot
* George DeNormand

==Critical reception== Tailspin Tommy serial.  

==References==
===Notes===
 
===Bibliography===
 
* Cline, William C. "Filmography". In the Nick of Time. Jefferson, North Carolina: McFarland & Company, Inc., 1984. ISBN 0-7864-0471-X.
* Farmer, James H. Celluloid Wings: The Impact of Movies on Aviation. Blue Ridge Summit, Pennsylvania: Tab Books Inc., 1984. ISBN 978-0-83062-374-7.
* Harmon, Jim and Donald F. Glut. The Great Movie Serials: Their Sound and Fury. London: Routledge, 1973. ISBN 978-0-7130-0097-9.
* Weiss, Ken and Ed Goodgold. To be Continued ...: A Complete Guide to Motion Picture Serials. New York: Bonanza Books, 1973. ISBN 0-517-166259.
* Wynne, H. Hugh. The Motion Picture Stunt Pilots and Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing Co., 1987. ISBN 0-933126-85-9.
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 