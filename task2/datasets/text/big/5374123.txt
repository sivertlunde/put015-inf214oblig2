Aksar
{{Infobox film
| name           = Aksar
| image          = Aksaar.jpg
| image size     = 
| caption        = Film poster
| director       = Anant Mahadevan
| producer       = Narendra Bajaj Shyam Bajaj Sujit Suri
| narrator       =
| starring       = Emraan Hashmi Udita Goswami Dino Morea
| music          = Himesh Reshammiya
| cinematography = K. Raj Kumar
| editing        = Sanjeev Dutta
| distributor    = Siddhi Vinayak Creations, Venus Worldwide Entertainment
| released       = 3 February 2006
| runtime        = 132 minutes
| country        = India Hindi
| budget         =
| preceded by    =
| followed by    =
}}

Aksar ( ,   thriller film directed by Anant Mahadevan and produced by Narendra and Shyam Bajaj under the banner of Siddhi Vinayak Creations. The film stars Emraan Hashmi, Udita Goswami and Dino Moreain the lead roles. It features the song "Jhalak Dikhlaja" sung by Himesh Reshammiya, also done a remix video for the promotion. The film was a box office success. 

==Plot==
Ricky (Emraan Hashmi) is a leading fashion photographer, who carries his heart on his sleeve. Hes an absolute womanizer. The film begins with Ricky getting a call from Sheena (Udita Goswami), who asks him to meet her in a gym. Once there, the two have a showdown since Ricky had used and dumped Sheenas friend Nisha (Tara Sharma). A heartbroken Nisha had even contemplated attempting suicide.

Three years later, Ricky is about to hold an exhibition of his creations when a millionaire walks in and buys the entire lot even before the exhibition has begun. The millionaire, Rajveer aka Raj (Dino Morea), has a pre-condition: Ricky should make Rajs wife Sheena fall in love with him (Ricky). Ricky is perplexed, for he fails to understand why a husband would hire someone to have an affair with his wife. But Raj explains that he wants Sheena to divorce him and this would be possible only if she fell in love with another man.

Ricky flies to London, where Raj and Sheena live in a splendid mansion, and starts playing his cards. After some setbacks Sheena and Ricky begin a relationship. The plan seems to be working perfect. Raj catches Ricky and Sheena in bed, but Sheena is unfazed and does not consider this to be an issue. she also refuses to divorce Raj and tells him she is intent on continuing with her relationship with Ricky:  Raj is stunned; he feels his game plan has gone kaput.

Realizing that Sheena wouldnt divorce him, Raj turns to Ricky, tells him to pack his bags and return to India. But now Ricky does a somersault. Hes enjoying using a rich woman and staying in the lap of luxury. Raj is stunned again. Its a clear case of double crossing. Nisha attends a party in Rajs mansion. After the party is over, Nisha is crying outside claiming that she was raped by Ricky. The next morning, Sheena confronts and kills Ricky with a sword, cutting his face and by digging the sword through his chest.

The cop investigates the murder and Sheena is the prime suspect. At the time when she was being arrested, Raj stands up and declares that he is the killer and he is taken to jail. When Sheena meets him in jail, he transfers all his property to her name. When the cop comes home with a stress ball which Raj had given to him, he was playing with it. Suddenly something falls and he discovers a camera which recorded the murder scene. Sheena is taken to jail and she transfers all her wealth to Rajs name. This masterstroke was fully planned by Raj. A few years ago, all cameras were removed from the base camp but Raj deliberately left this one.

At the end, Raj is seen sitting in the car with Nisha, his love. He gives her the property papers as a token of his love.
The cop stares at Raj and Nisha in car then Raj throws his "Tension Ball" at him and says,"Aisa to aksar hota hai" (This happens often).

==Cast==
* Dino Morea as Rajveer Singh (Raj)
* Emraan Hashmi as Ricky Sharma
* Udita Goswami as Sheena Roy
* Tara Sharma as Nisha
* Suresh Menon as Benz
* Rajat Bedi as Steve Bakshi

==Soundtrack==
{{Infobox album| 
| Name        = Aksar
| Type        = soundtrack
| Artist      = Himesh Reshammiya
| Cover       =
| Released    = 
| Recorded    = 
| Producer =  Feature film soundtrack
| Length      = 
| Label       = T-Series
}}

The films soundtrack is composed by Himesh Reshammiya with lyrics penned by Sameer (lyricist)|Sameer. The song "Jhalak Dikhlaja" was released in three versions and went onto the chartbusters.

===Tracklist===
The soundtrack contains five original songs and three remixes.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="FFFF99" align="center"
! Track # !! Song !! Singer(s) !! Duration
|-
| 1 || "Jhalak Dikhlaja" || Himesh Reshammiya || 5:14
|-
| 2 || "Lagi Lagi" || Himesh Reshammiya & Sunidhi Chauhan || 5:33
|-
| 3 || "Loot Jayenge" || Kunal Ganjawala & Jayesh Gandhi || 6:02
|-
| 4 || "Mohabbat Ke" || Himesh Reshammiya & Tulsi Kumar || 5:01
|- KK & Sunidhi Chauhan || 4:56
|-
| 6 || "Jhalak Dikhlaja (Remix)" || Himesh Reshammiya || 5:01
|- KK & Sunidhi Chauhan || 4:16
|-
| 8 || "Theme Of Aksar" ||  || 2:34
|}

==External links==
*  

 
 
 
 