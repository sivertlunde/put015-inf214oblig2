The Adventures of Bullwhip Griffin
{{Infobox film
| name           = The Adventures of Bullwhip Griffin
| image          = The Adventures of Bullwhip Griffin.jpg
| border         = 
| alt            = 
| caption        =  James Neilson
| producer       = 
| writer         = 
| screenplay     = Lowell S. Hawley
| story          = 
| based on       =  
| narrator       = 
| starring       = Roddy McDowall Suzanne Pleshette Karl Malden
| music          = Richard M. Sherman and Robert B. Sherman (songs) George Bruns (score) Edward Colman
| editing        = Marsh Hendry Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 108 mins.
| country        = United States
| language       = English
| budget         = 
| gross          =$1,900,000 (US/ Canada) 
}}
 Western comedy James Neilson. The film is based on the novel By the Great Horn Spoon! by Sid Fleischman, and stars Roddy McDowall, Suzanne Pleshette and Karl Malden.  The songs were written by Richard M. Sherman and Robert B. Sherman.

==Cast==
* Roddy McDowall as Bullwhip Griffin
* Suzanne Pleshette as Arabella Flagg Bryan Russell as Jack Flagg
* Karl Malden as Judge Higgins
* Harry Guardino as Sam Trimble
* Richard Haydn as Quentin Bartlett
* Mike Mazurki as Mountain Ox
* Hermione Baddeley as Miss Irene Chesney
* Alan Carney as Joe Turner
* Parley Baer as Chief Executioner
* Arthur Hunnicutt as Referee
* Dub Taylor as Timekeeper

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 