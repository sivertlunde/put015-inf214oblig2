The Orchid Gardener
{{Infobox film
| name           = The Orchid Gardener
| director       = Lars von Trier
| producer       = Filmgruppe 16, Lars von Trier
| writer         = Lars von Trier
| starring       = Lars von Trier, Inger Hvidtfeldt
| released       = 6 May 1977
| runtime        = 37 minutes
| country        = Denmark
| language       = Danish
}}

The Orchid Gardener is a 1977 experimental film with a mysterious and symbolic plot directed by Lars von Trier.

==Plot==
A young, mentally ill man, a visual artist in crisis Victor Marse (Lars von Trier) meets two nurses (Eliza and her girlfriend) during his stay in a sanatorium. Victor lives with Eliza and her son. He imagines another woman when he is roaming at a coast. He pretends committing a suicide but Eliza does not react to it. Every moment, he stays in front of a blank canvas and thinks. Meanwhile he dresses into Nazi clothes or into women dresses, then he leaves to go to the cinema, and abuses and probably kills a small girl. His masochistic affair with Eliza lasts; he is close to shooting her with a gun but instead she takes out a whip. Victor goes along the streets then he lies naked in front of the canvas on which he has left his bloody fingerprints. After this he drives a funeral car to his work - he is employed in a garden where orchids are grown. Eliza is now the past and in the end, Victor might be dead as someone drives a cross into the ground.

==References==
*Schepelern P., Lars von Trier a jeho filmy, Orpheus, 2004

 

 
 
 
 
 

 