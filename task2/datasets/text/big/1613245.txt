Kadhalan
 
 
{{Infobox film
| name           = Kadhalan
| image          = Kadhalan.jpg Shankar
| producer       = K. T. Kunjumon
| writer         = Balakumaran  (Dialogue) 
| screenplay     = Shankar
| story          = Dalton trumbo, Shankar
| starring       = Prabhu Deva Nagma Vadivelu Raghuvaran Girish Karnad
| music          = A. R. Rahman Jeeva
| editing        = B. Lenin V. T. Vijayan
| studio         = K. T. Kunjumon|A. R. S. Film International
| distributor    = A. R. S. Film International
| runtime        = 170 minutes
| country        = India
| language       = Tamil
| released       =  
| budget         = 
}}
 Tamil  written and soundtrack were Telugu as National Film CGI (in The Mask.

== Plot ==
Prabhu, the chairman of Chennais Government Arts College, is asked by his Principal to invite the Governor of Tamil Nadu, Kakarlal Sathyanarayana Moorthy, to be the chief guest on the annual day of the college. When he goes to invite Kakarlal, he bumps into his daughter, Shruthi, instantly falling in love with her. To propose his love for her, he even joins in the same dance class as her. She too subsequently gets to know more about him and falls in love with him. Since there is considerable fear of terrorism in the state, Kakarlal does not permit his daughter to travel anywhere without bodyguards. On one occasion, she manages to give them the slip and runs away with Prabhu at a dance festival. Shortly after, there is the threat of a bomb being planted. Shruthi is immediately brought home and Prabhu is arrested. Shruthi is forbidden to see Prabhu. After being beaten severely, Prabhu is released from police custody and taken back home, tended by his police father Kathiresan. He then goes to meet Shruthi unaware of the changed circumstances. This is where he finds evidence that will lead him to a hideout of assassin Mallikarjuna who is the mastermind of all the bomb blasts. Prabhu realizes that Mallikarjuna has been working for Kakarlal to kill innocent civilians with bombs. Kakarlal pays Mallikarjuna to put a bomb in Government General Hospital. Kakarlal betrays Mallikarjuna by killing him with a (bomb radio). Unfortunately, Mallikarjuna survives but hes wounded. Mallikarjuna changes the time, so the bomb can explode earlier. When Kakarlal shows up, he realizes that Mallikarjunas still alive but Mallikarjuna disappears again. Prabhu and other people take the patients out of the Government General Hospital. Kakarlal, Kakarlals wife, and Shruthi get stuck in an elevator. Prabhu finds the bomb and manages to get out of the hospital as soon as possible, so he can throw the bomb off the bridge into the water. The bomb explodes in the water. After Shruthi gets out of the elevator, a wounded Mallikarjuna breaks from the top and electrocutes Kakalal to death. Mallikarjuna also dies because of blood loss. It is weird for this film to end very sadly when evil Kakalal dies but his family doesnt know the truth. The film ends with the reunion of Prabhu and Shruthi. Its presumed that Prabhu and Shruthi got married.

==Cast==
* Prabhu Deva as Prabhu
* Nagma as Shruthi
* Vadivelu as Vasanth (Kaliaperumal)
* Raghuvaran as Mallikarjuna
* S. P. Balasubrahmanyam as Kathiresan Manorama as Shruthis grandma
* Girish Karnad as Kakarla Sathyanarayana Moorthy
* Allu Rama Lingaiah as Shruthis Grandpa
* Ajay Rathnam as Security officer
* Dhamu as Prabhus friend Shankar in a cameo appearance (Kadhalikum Pennin song)
* Raju Sundaram in a cameo appearance (Kadhalikum Pennin song)
* Dr. Shreeram Lagoo as the Governor

== Release == Telugu as National Film Best Male Best Audiography, Best Editing Best Special Effects.

== Music ==
 
{{Infobox album
| Name        = Kadhalan
| Type        = Soundtrack
| Artist      = A. R. Rahman
| Cover       = Album_kadhalan_cover.jpg
| Background  = Gainsboro
| Released    = 1994
| Recorded    = Panchathan Record Inn
| Genre       = Soundtrack
| Length      = 34:50 Pyramid Lahari Music Sa Re Ga Ma
| Producer    =  A. R. Rahman
|
}}

The background music score and the soundtrack were composed by  , Vairamuthu and S. Shankar who penned the Pettai Rap number. While the entire soundtrack was well received commercially and critically by reviewers, one song Mukkabla became recognised on a national level, becoming one of the most recognisable Tamil songs in history. The song was plagiarised freely by Bollywoods tunesmiths and nearly a dozen versions of the song were churned out, a feat that earned Mukkabla and Rahman a place in the Limca Book of Records.  New styles were experimented with, as in the song "Pettai Rap", a Madras bashai song which was written in a rapping|rap-like style, interspersing Tamil with English words. The synthesiser and the keyboard also feature while drawing from Tamil folk music.

The song "Urvasi" inspired the 2014 song "Birthday (will.i.am song)|Birthday" by American rapper will.i.am.  

{{tracklist
| extra_column    = Artist(s)
| title1          = Ennavale Adi Ennavale
| extra1          = P. Unnikrishnan
| length1         = 5:11
| title2          = Mukkabla
| extra2          = Mano (singer)|Mano, Swarnalatha
| length2         = 5:23
| title3          = Erani Kuradhani
| extra3          = S. P. Balasubramanyam, S. Janaki
| length3         = 5:08
| title4          = Kadhalikum Pennin
| extra4          = S. P. Balasubramanyam, Udit Narayan, Pallavi
| length4         = 4:48
| title5          = Urvasi Urvasi
| extra5          = A. R. Rahman, Suresh Peters, Shahul Hameed
| length5         = 5:39
| title6          = Pettai Rap
| extra6          = Suresh Peters, Theni Kunjarammal, Shahul Hameed
| length6         = 4:23
| title7          = Kollayile Thennai
| extra7          = P. Jayachandran
| length7         = 1:45
| title8          = Kaatru Kuthirayile Sujatha
| length8         = 1:31
| title9          = Indiraiyo Ival Sundariyo
| extra9          = Sunandha, Kalyani Menon, Minmini
| length9         = 1:02
}}

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 