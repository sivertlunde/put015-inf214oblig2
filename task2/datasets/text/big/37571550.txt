A Short History of Decay (film)
 
{{Infobox film
| name           = A Short History of Decay
| director       = Michael Maren
| producer       = Alfred Sapse
| writer         = Michael Maren
| starring       = Bryan Greenberg, Linda Lavin, Harris Yulin, Emmanuelle Chriqui, Kathleen Rose Perkins, Benjamin King
| music          = Julia Kent
| cinematography = Nancy Schreiber
| editing        = T.J. Snell
| released       =  
| country        = United States
| language       = English
}}
 Benjamin King and Kathleen Rose Perkins. Though its title is taken from the work of philosophy by Emil Cioran, it is not an adaptation of the book.

The film was shot in October and November 2012 in Wilmington, North Carolina,  Wrightsville Beach, North Carolina   and New York City. It premiered at the Hamptons International Film Festival on October 12, 2013 and it opened theatrically at the Village East Cinema on May 16, 2014.

==Plot==
It is a comedy from an original script by Michael Maren, about a failed Brooklyn writer, Nathan Fisher, played by Bryan Greenberg, who visits his ailing parents in Florida.  His mother (Lavin) has Alzheimers and his father (Yulin) has recently had a stroke.

==Cast==
* Bryan Greenberg as Nathan Fisher
* Linda Lavin as Sandy Fisher
* Harris Yulin as Bob Fisher
* Emmanuelle Chriqui as Erika Bryce
* Kathleen Rose Perkins as Shelly Benjamin King as Jack Fisher
* Rebecca Dayan as Alex
* Joanna Manning as Molly

==Coffeehouse Scene== Philip Gourevich, Kurt Andersen, Gary Shteyngart, Darin Strauss, and Nick Flynn.

== References ==
 

==External links==
*  
*  
*  
*   Edelstein on A Short History of Decay: The Little That Happens Is More Than Enough in This Beautiful Tale : New York Magazine]

 
 
 
 
 
 
 
 
 


 