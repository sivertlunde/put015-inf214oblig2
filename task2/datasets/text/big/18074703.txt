Riders of the Dawn
{{Infobox Film
| name           = Riders of the Dawn
| image          = Los Jinetes del alba 1990.jpg
| caption        = 
| director       = Vicente Aranda 
| producer       = José Luis Tafur RTVE
| writer         =  Vicente Aranda  Joaquin Jorda
| starring       = Victoria Abril, Jorge Sanz, Maribel Verdú
 | music          = José Nieto (composer)|José Nieto
| cinematography = Juan Amorós 
| editor         =  Teresa Font
| distributor    = RTVE
| released       =    1990
| runtime        =  250 min (five episodes)
| language       = Spanish
| budget         = 
|}}
 Spanish film, written and directed by Vicente Aranda, an adaptation of a novel by Jesús Fernández Santos. It stars Victoria Abril and Jorge Sanz. Made as five-episode television miniseries, it premiered at the 1990 Cannes Film Festival as two-part feature film.

==Synopsis == Asturian revolution of 1934 and the Spanish Civil War.

==Cast==
*Victoria Abril – Marian
*Jorge Sanz – Martin
* Maribel Verdú – Raquel
* Graciela Borges – Dona Amalia
* Fernando Guillén – Don Erasmo
* Gloria Muñoz – Adamina
* Antonio Iranzo – El Santero
* Joan Miralles – Ventura
* Nacho Martínez – Raquel’s father
* Clauda Gravy – Raquel’s mother
* Carlos Tristancho – Quincelibras
* Lola Baldrich – Aida

==External links==
* 
*  

 

 
 
 
 
 

 