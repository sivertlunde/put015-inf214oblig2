Gregory Go Boom
{{Infobox film
| name           = Gregory Go Boom
| image          = GGB poster.jpg
| alt            = 
| caption        = Film poster
| director       = Janicza Bravo
| producer       = Janicza Bravo Deborah J. Chesebro Brett Gelman	
| writer         = Janicza Bravo
| screenplay     = 
| starring       = Michael Cera Brett Gelman Sarah Burns
| music          = Heather Christian
| cinematography = Christian Sprenger
| editing        = Janicza Bravo Cine Bravo	
| studio         = Jash CYRK Production
| distributor    = 
| released       =  
| runtime        = 18 minutes
| country        = United States
| language       = English
}} comedy Drama drama short film, written and directed by Janicza Bravo.    The film premiered at Jash during YouTube Comedy Week on May 23, 2013.  

The film later screened at 2014 Sundance Film Festival on January 17, 2014.  It won the Jury Award at the festival.      

==Plot==
Gregory, a paraplegic man tries dating for the first time and discovered that life is very different than he had imagined.

==Cast==
*Michael Cera as Gregory
*Brett Gelman as Tom
*Sarah Burns as Rose
*Anna Rose Hopkins as Summer / Cheyenne
*Stephanie Allynne as Willie
*Mireya Lucio as Crystal
*Nick Ortega as Carlos
*Holly Kaplan as Waitress

==Reception==
Gregory Go Boom received mostly positive reviews from critics. Liana Maeby of HitFix praised Cera and the film by saying that ""Gregory" is a good little short thats worth watching simply to see Michael Cera play a character that looks like Napoleon Dynamite crossed with Yertle the Turtle."  Derek Deskins of Lonely Reviewer, praised the film by saying that "“Gregory Go Boom” is an interestingly dark and ominous short that will make you view Michael Cera completely differently." 

==Accolades==
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="2"| 2014 Sundance Film Festival
| Short Film Grand Jury Prize
| Janicza Bravo 
|  
|-
| Short Film Jury Award: U.S. Fiction
| Janicza Bravo 
|    
|}
 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 