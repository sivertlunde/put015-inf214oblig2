Civic Duty
 
{{Infobox film
| name           = Civic Duty
| image          = Civic duty.jpg
| caption        = Theatrical release poster
| director       = Jeff Renfroe
| producer       = Andrew Lanter
| writer         = Andrew Joiner
| starring       = Peter Krause Khaled Abol Naga Richard Schiff Kari Matchett Ian Tracey
| music          = Terry Huud Eli Krantzberg
| cinematography = Dylan Macleod
| editing        = Erik Hammarberg Jeff Renfroe
| distributor    = Freestyle Releasing
| released       =  
| runtime        = 98 minutes
| country        = Canada United Kingdom United States
| language       = English
| budget         = 
| gross          = 
}}
Civic Duty is a 2006 thriller film directed by Jeff Renfroe and starring Peter Krause, Khaled Abol Naga, Kari Matchett, and Richard Schiff. 

==Plot==
The film is about an American accountant bombarded with cable news and the medias obsession with terrorist plots in the post 9/11 world, who receives a jolt when an unattached Islamic graduate student moves in next door.

==Cast==
* Peter Krause as Terry Allen
* Kari Matchett as Marla Allen
* Richard Schiff as FBI Agent Tom Hilary
* Khaled Abol Naga as Gabe Hassan
* Ian Tracey as Lieutenant Randall Lloyd
* Vanesa Tomasino as Bank Teller
* Laurie Murdoch as Loan Officer
* Michael Roberds as Post Office Clerk
* Agam Darshi as Nurse
* Mark Brandon as Night Anchor Bret Anderson
* Brenda Crichlow as Day Anchor Tricia Wise
* Val Cole as Morning Anchor Susan Harwood
* Mark Docherty as News Reporter Chad Winslow
* Michael St. John Smith as Legal Analyst
* P. Lynn Johnson as Governor Bradley

==Critical reception==
Richard Nilsen, film critic for The Arizona Republic, liked the film at times, but gave the film a mixed review.  He wrote, "It does build up considerable suspense and tension; Renfroe has learned well from Alfred Hitchcock|Hitchcock. And the final confrontation builds to a harrowing pitch. But that cant rescue the film from a silly, melodramatic finish." 

==References==
 

==External links==
*   At the Movies)
*  

 
 
 
 
 
 

 