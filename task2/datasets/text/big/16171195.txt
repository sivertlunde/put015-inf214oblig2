Think Fast, Mr. Moto
 
{{Infobox film
| name           = Think Fast, Mr. Moto
| caption        =
| image	         = Think Fast, Mr. Moto FilmPoster.jpeg Norman Foster
| producer       = Sol M. Wurtzel
| based on       =  
| screenplay     = Norman Foster Philip MacDonald Thomas Beck Virginia Field Sig Ruman
| music          = R.H. Bassett Samuel Kaylin Harry Jackson
| editing        = Alex Troffey
| distributor    = Twentieth Century Fox Film Corporation
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
}}
 Thomas Beck and Sig Ruman. Mr. Moto works to stop a secret smuggling operation.

==Plot== freighter headed for Shanghai. Also on the freighter is Bob Hitchings Jr., son of the owner of the freighter. Before leaving, Hitchings Sr. gives his son a confidential letter for the head of the Shanghai branch of the company. Hitchings and Moto become friends (Moto notices the letter), and Moto helps Hitchings cure a hangover. Hitchings complains to Moto that he has not met any beautiful women on board. After a stop in Honolulu, a beautiful woman named Gloria Danton boards the ship, and she and Hitchings fall in love. But Gloria is a spy for Nicolas Marloff, who runs a smuggling operation out of Shanghai. She periodically sends him notes and leaves without saying goodbye to Hitchings. Moto finds a steward looking for Hitchings’s letter, and confronts him, knowing he was the person who killed the man in the wicker basket, as he wears the tattoo. Moto throws the man overboard and takes the letter.

At Shanghai, Hitchings meets with Joseph B. Wilkie and gives him the letter, but later learns that it is a blank sheet of paper. He calls his father, who tells him the letter said to watch out for smugglers. Hitchings is adamant on finding Gloria, and he learns from an unknown person that she is at the “international club”. Both he and Wilkie go there, as well as Moto and his date, Lela Liu. Hitchings finds Gloria performing at the club and goes to her dressing room. However, the club owner Marloff, discovers them together, and, knowing that Hitchings knows too much, locks them both up. Moto tells Lela to call the police, and seeks out Marloff. Posing as a fellow smuggler, he tricks Marloff into leading him to Gloria and Hitchings. Lela is shot while contacting the police, but manages to tell them where she is. Wilkie finds Marloff, and demands that Gloria and Hitchings be released. Marloff finds out that Moto is not a smuggler, then Moto apprehends him. Moto tells Wilkie to get Marloff’s gun, the gun explodes as Wilkie tries to grab it, killing Marloff. Police storm the building, and Moto tells them the Wilkie headed the smuggling operation. Wilkie replaced the letter and shot Lela. Moto gave Wilkie the opportunity to kill Marloff, who knew he was in on the plot, and he did. Wilkie is arrested, and things go back to normal.

==Cast==
* Peter Lorre as Mr. Moto Thomas Beck as Bob Hitchings Jr.
* Virginia Field as Gloria Danton
* Murray Kinnell as Joseph B. Wilkie
* Sig Ruman as Nicolas Marloff George Cooper as Muggs Blake John Rogers as Carson the steward
* Lotus Long as Lela Liu
* J. Carrol Naish as Adram
* George Hassell as Robert Hitchings Sr. (uncredited)
* William Law  as Shanghai Police Chief (uncredited)
* Bert Roach as The Ships Bartender (uncredited)

==Production==
Think Fast, Mr Moto was the third novel in the Moto series. Film rights to the novel were bought in July 1936. NEWS OF THE SCREEN:  Anthony Adverse Due at Strand Aug. 26 New Film On Russian Gypsies at Cameo Tonight.
New York Times (1923-Current file)   28 July 1936: 23.    Lorre was cast in the lead soon after; the actor had just signed with the studio and made two movies, Crack Up and Nancy Steel is Missing. HOLLYWOOD PICKET LINE: The Industry Watches the C. I. O.--Satisfaction--R. Halliburton Flynn
By DOUGLAS W. CHURCHILL. New York Times (1923-Current file)   April 18, 1937: 169.  He accepted the role because it gave him a rare chance to play a hero. 

Twentieth Century Fox had three other film series at the time and intended Mr Moto to be the fourth. 

Filming started 11 February, 1937. 

==Series==
Peter Lorre played the character of Mr. Motto in the eight Twentieth Century Fox Film Corporation film productions;

* Mr. Moto Takes a Vacation (1939)
* Mr. Moto in Danger Island (1939)
* Mr. Motos Last Warning (1939)
* Mysterious Mr. Moto (1938)
* Mr. Moto Takes a Chance (1938)
* Mr. Motos Gamble (1938) Thank You, Mr. Moto (1937)
* Think Fast, Mr. Moto (1937)

==Reception==
The film was well received and Fox announced plans to make five more Moto movies. The studio said they did not want to make the mistake of the Jeeves films with Arthur Treacher and vowed to provide good production values "to make them first string entertainment." 

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 