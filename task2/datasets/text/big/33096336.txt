Jeff, Who Lives at Home
{{Infobox film
| name           = Jeff, Who Lives at Home
| alt            =  
| image	         = Jeff Who Lives at Home FilmPoster.jpeg
| border         = yes
| caption        = 
| director       = Jay Duplass Mark Duplass
| producer       = Lianne Halfon Russell Smith Jason Reitman
| writer         = Jay Duplass Mark Duplass
| starring       = Jason Segel Ed Helms Judy Greer Susan Sarandon Michael Andrews
| cinematography = Jas Shelton
| editing        = Jay Deuby Indian Paintbrush   Mr. Mudd
| distributor    = Paramount Vantage
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $7.5 million  
| gross          =  $4,704,757
}} Jay and Mark Duplass and co-starring Judy Greer and Susan Sarandon.    The film premiered on September 14, 2011 at the 2011 Toronto International Film Festival and then saw a limited release in USA and Canada on March 16, 2012,  after having been pushed back from the original date of March 2.    The film received positive reviews from critics, but was a box office failure.

==Plot== stoner living Baton Rouge, Louisiana. He looks for his destiny in seemingly random occurrences. He finds inspiration in the feature film Signs (film)|Signs, which reinforces his belief in this outlook. One day, he answers the telephone; its a wrong number, from somebody asking for "Kevin," and Jeff contemplates the meaning of this, deciding its a sign.

Receiving a call from his irritated mother asking him to buy wood glue to fix a door shutter or find a new place to live, Jeff boards a bus, where he sees a kid wearing a sports jersey bearing the name Kevin.  He follows Kevin (Ross) to a basketball court, where he joins a pick-up game and the two bond.  Jeff agrees to smoke weed with Kevin, but discovers he has been tricked when he is beaten and mugged.

He happens upon a Hooters restaurant where he crosses paths with his older brother Pat (Helms), a successful yuppie struggling with a failing marriage. Pats wife Linda (Greer) is spotted at a gas station across the street with another man. Jeff and Pat spend several hours following them, first to a restaurant and later to a hotel, with Pats new Porsche being ticketed, crashed and eventually towed away at various points in the journey. The brothers also visit their fathers gravesite and fight over their conflicting life philosophies.

Jeff sees a truck reading "Kevin Kandy" and runs off to hitch a ride, only to end up at the same hotel where Pat has found Linda in a room with another man. Jeff offers to break down the door. The man is a co-worker of hers named Steve (Zissis). Linda quickly ushers Steve out and then confronts Pat about his role in their problems.  Frustrated, she leaves, saying she will move in with her mother. Jeff and Pat reconcile. Jeff explains how he is struggling to find his destiny in life, while Pat admits he wants to fall in love with Linda again and for her to do the same with him.  Jeff encourages his brother to tell her that, and they hail a taxi to pursue her.

Interspersed within the main story is the story of Sharon, who is at work, frustrated with her unfulfilled life and dissatisfaction with her sons. The doldrum is interrupted when a paper airplane with a beautiful drawing of a flower lands in her cubicle, followed by an anonymous co-worker claiming in an IM message to be a secret admirer. Sharon spends the day trying to deduce the identity of her admirer. She confides her frustrations to colleague and friend Carol (Chong), revealing that she has not dated since her husbands death. Carol encourages her to warm up to the attention she is receiving. Sharon is surprised and confused when the admirer turns out to be Carol herself, and though neither believe themselves to be attracted to their own gender, Carol appeals to Sharons desire to become close with someone who truly understands her.  At that moment, a fire alarm goes off and ceiling sprinklers activate; this is an enlightening moment for Sharon who sets off with Carol on a spur of the moment trip to New Orleans.

Jeff, Pat, Linda, Sharon, and Carol all converge on a bridge, where they are stuck in standstill traffic.  Pat exits the taxi and runs through the traffic to tell Linda how he feels, passing Carols car; Sharon sees her son and runs after him, followed by Carol.  As Jeff muses to the cab driver about seeking out his destiny only to find it isnt very exciting, he observes a helicopter flying overhead, jumps out of the taxi and also runs through the traffic, passing Pat, who was sharing his feelings with Linda when they were interrupted by the arrival of Sharon and Carol.  Jeff continues onward to discover that the cause of the traffic is an accident in which one car plummeted over the side of the bridge.  He dives into the water and rescues two children and their father; when Jeff then fails to resurface Pat dives in and rescues him.  The group reconciles after the ordeal, and the audience sees Sharon celebrating her birthday and Pat and Linda apparently faring better in their marriage.  Jeff sees a news report about his heroics and learns that the father of the kids he rescued was also named Kevin; now with a sense of purpose, he grabs some wood glue and fixes the door shutter.

==Cast==
* Jason Segel as Jeff Thompkins
* Ed Helms as Pat Thompkins
* Susan Sarandon as Sharon Thompkins
* Judy Greer as Linda Thompkins
* Rae Dawn Chong as Carol
* Steve Zissis as Steve
* Evan Ross as Kevin

==Box office==
The film grossed $840,000 in its opening weekend.    

Jeff, Who Lives at Home grossed $4,269,426 in North America and  $435,331 elsewhere, for a worldwide total of $4,704,757. 

==Critical reception==
The film received generally positive reviews. At Rotten Tomatoes, the film holds a positive "fresh" rating of 78%, based on 127 reviews and an average rating of 6.7/10, with the critical consensus saying, "Sweet, funny, and flawed, Jeff, Who Lives at Home finds the Duplass brothers moving into the mainstream with their signature quirky charm intact".    It also has a score of 60 on Metacritic based on 33 reviews, indicating "mixed or average reviews".   

Roger Ebert of the Chicago Sun-Times described Jeff, Who Lives at Home as "a whimsical comedy   on the warmth of Segel and Sarandon, the discontent of Helms and Greer, and still more warmth that enters at midpoint with Carol (Rae Dawn Chong), Sarandons co-worker at the office." He concluded that "its not a Feel Good Movie, more of a Feel Sorta Good Movie."    Peter Travers of Rolling Stone writes that the film is "funny, touching, and vital" praising the Duplass brothers by saying that "their films hit you where you live." 

==Home media==
Jeff, Who Lives at Home was released on DVD and Blu-ray on June 19, 2012. 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 