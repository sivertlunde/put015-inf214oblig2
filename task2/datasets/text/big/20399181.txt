Good Night, Elmer
 
{{Infobox Hollywood cartoon
|cartoon_name=Good Night, Elmer
|image=
|caption=Title card
|series=Merrie Melodies (Elmer Fudd)
|director=Chuck Jones
|story_artist=Rich Hogan
|animator=Philip Monroe
|layout_artist=
|background_artist=
|voice_actor=Mel Blanc
|musician=Carl W. Stalling Warner Brothers Pictures
|distributor=
|release_date=October 26, 1940
|color_process=
|runtime=
|preceded_by=Holiday Highlights
|followed_by=The Sour Puss
}}
 
Good Night, Elmer is a Merrie Melodies cartoon short released by Warner Bros. on October 26, 1940, directed by Chuck Jones, animated by Philip Monroe and written by Rich Hogan. The cartoon depicts the ill-fated attempts of Elmer Fudd, in a rare leading role, to extinguish a candle by his bedside so that he can retire for the night, with the flame always surging again in spite of Elmers best efforts. Elmer finally succeeds, but only at the expense of wrecking his bedroom in the process, and no sooner than he lies down, the sun comes up, precipitating a nervous breakdown in Fudd.

==See also==
* Looney Tunes and Merrie Melodies filmography (1940–1949)

==External links==
* 
*  at the Big Cartoon DataBase

 
 
 
 
 
 


 