The Tiger (1978 film)
Tigar is a 1978 drama film about retired boxing champion Sorga, nicknamed "Tigar". It is in the Serbo-Croatian language, and was made in Yugoslavia.

Broke, his wife leaves him for a wealthier guy. Šorga meets a juvenile thief, Čok, and decides to become his father figure. In the process of changing Čok, Šorga changes himself. The movie was written by Gordan Mihić, directed by Milan Jelić. Tigar is in Eastmancolor, mono sound mix, and runs 103 minutes.

==Cast==
*Ljubiša Samardžić as Šorga Tigar
*Slavko Štimac as Čok
*Milivoje Tomić
*Snežana Nikšić as Tamara Velimir Bata Živojinović as Direktor
*Pavle Vuisić as Šorgin trener
*Rahela Ferari as Gospođa Hadžirakić
*Radmila Savićević
*Jelica Sretenović as Student pedagogije
*Milan Bosiljčić
*Ljubomir Ćipranić
*Vera Čukić as Barbarela
*Ivan Đurđević
*Ljerka Draženović
*Dragomir Felba
*Bogdan Jakus
*Dusan Janićijević as Trener
*Vojislav Mićović
*Predrag Milinković as Kelner
*Nada Osmokrović
*Bozidar Pavicević-Longa
*Branko Petković
*Olga Poznatov
*Marinko Šebez
*Rastko Tadić
*Janez Vrhovec as Inspektor
*Gizela Vuković
*Vladan Živković

==Crew==
* Original Music by: Vojislav Kostic
* Cinematography by: Predrag Popovic
* Film Editing by: Ljiljana-Lana Vukobratovic
* Production Design by: Miodrag Hadzic 
* Costume Design: Mira Cohadzic
* Makeup: Radmila Ivatovic

==See also==
*1978 in film

==External links==
* 

 

 
 
 
 

 