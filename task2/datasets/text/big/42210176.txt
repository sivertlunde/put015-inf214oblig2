The Mozart Brothers
 
{{Infobox film
| name           = The Mozart Brothers
| image          = 
| caption        = 
| director       = Suzanne Osten
| producer       = Göran Lindström
| writer         = Etienne Glaser Niklas Rådström Suzanne Osten
| starring       = Etienne Glaser
| music          =  
| cinematography = Hans Welin
| editing        = 
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
 Best Director at the 22nd Guldbagge Awards.   

==Cast==
* Etienne Glaser as Walter
* Philip Zandén as Flemming
* Henry Bronett as Fritz
* Loa Falkman as Eskil / Don Juan
* Agneta Ekmanner as Marian / Donna Elvira
* Lena T. Hansson as Ia / Donna Anna
* Helge Skoog as Olof / Don Ottavio
* Grith Fjeldmose as Therés / Zerlina
* Rune Zetterström as Lennart / Leporello
* Niklas Ek as Georg / Donna Annas Father
* Krister St. Hill as Ulf / Mazzetto

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 