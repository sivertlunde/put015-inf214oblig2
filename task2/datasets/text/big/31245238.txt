On Again-Off Again
{{Infobox film
| name           = On Again-Off Again
| image          = On Again Off Again lobby card.jpg
| caption        = Lobby card
| producer       = Lee S. Marcus Samuel J. Briskin (executive producer) Edward Cline
| writer         = Nat Perrin (screenplay) Benny Rubin (screenplay)
| starring       = Bert Wheeler Robert Woolsey Marjorie Lord Patricia Wilder Esther Muir
| music          = Dave Dreyer Roy Webb
| cinematography = Jack MacKenzie
| editing        = John Lockert RKO Radio Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
}} musical comedy film, released by RKO Radio Pictures starring the comedy team Wheeler & Woolsey.

==Plot==
William Hobbs and Claude Horton are the owners of the drug manufacturing company "Horton and Hobbs Pink Pills". Although the two couldnt have possibly started the business without each other, they continuously bicker over everything. Eventually, the duo talk their lawyer, George Dilwig, into coming up with a way to split the team up. Annoyed by Horton and Hobbs constantly bothering him, Dilwig sarcastically suggests the two get into a wrestling match. The winner gains full ownership of the company, while the loser becomes the winners butler for one year.

==Production==
* Based on the 1914 play "A Pair of Sixes" by Edward Peple.
* Robert Woolsey was suffering from kidney disease throughout production of this film, and was in constant pain.

==Cast==
*Bert Wheeler as William Hobbs
*Robert Woolsey as Claude Horton
*Marjorie Lord as Florence Cole
*Patricia Wilder as Gertie Green
*Esther Muir as Nettie Horton Paul Harvey as Mr. Applegate Russell Hicks as George Dilwig
*George Meeker as Tony
*Maxine Jennings as Miss Meeker
*Kitty McHugh as Miss Parker
*Hal K. Dawson as Sanford
*Alec Harford as Slip Grogan
*Pat Flaherty as Mr. Green

== External links ==
*  

 

 
 
 
 
 


 