Easy Money (1983 film)
{{Infobox_Film
| name = Easy Money
| image = Easy Money (1983) film poster.jpg
| caption = Theatrical release poster
| director   = James Signorelli Dennis Blair
| starring = {{Plainlist|
* Rodney Dangerfield
* Joe Pesci
* Geraldine Fitzgerald Candy Azzara
* Jennifer Jason Leigh
}}
| producer   = Estelle Endler John Nicolella
| cinematography = Fred Schuler
| editing = Ronald Roose
| music = Laurence Rosenthal 
| distributor = Orion Pictures
| released = 19 August 1983
| runtime = 95 minutes English
| country = United States
| gross = $29,309,766
}} Dennis Blair. The original music score was composed by Laurence Rosenthal.  The theme song "Easy Money" is performed by Billy Joel and featured on his album An Innocent Man.

==Plot==
Montgomery "Monty" Capuletti (Rodney Dangerfield) is a hard-living, heavy-drinking, pot-smoking, gambling family man who makes his living as a baby photographer. He loves his wife Rose (Candice Azzara) but has a very tense relationship with his wealthy, snobbish mother-in-law Mrs. Monahan (Geraldine Fitzgerald) who runs a successful department store chain and hates the way Monty acts and lives (The movie lightly touches on ethnic rivalry---Dangerfields character is a stereotype of Italians often invoked by the Irish, while his wifes family represent the more puritanical, staid lace curtain Irish stereotypes). 

The irresponsible Monty cannot even pick up a wedding cake for his engaged daughter Allison (Jennifer Jason Leigh) without fouling up. He and his best friend Nicholas Nicky Cerone (Joe Pesci) are smoking marijuana while driving and an accident destroys the cake.

After Montys mother-in-law dies unexpectedly, his family is in for an inheritance. Attorney Scrappleton (Tom Ewell) reveals that Mrs. Monahan left a stipulation in her will that if Monty is able to curb his vices for a year by going on a diet and giving up drugs and gambling, he will receive $10 million. If not, the family gets nothing. Allisons wedding to the peculiar Julio (Taylor Negron) goes off without a hitch, at least until the wedding night. The guys Monty would often drink and gamble with are also interested in whether or not Monty can really give up everything they did together, and are betting whether or not he will make it. Monty and Nicky go to the mother-in-laws department store and find awkward fashions and catering to a clientele which clearly do not include the likes of Nicky and Monty. Nicky then argues that it may not be worth it to Monty if this is the kind of atmosphere he will be exposed to, but Monty points out he must tough it out in order to provide for his wife and daughters, not just him. Meanwhile, the scheming right-hand man to Mrs. Monahan named Clive Barlow (Jeffrey Jones) does his best to undermine Montys resolve so the money and department store can instead be left to him.
 faked her own death simply to persuade her slovenly son-in-law to straighten up. Ultimately though, she gives the money to Monty on the basis he upheld her stipulation. Now rich, Monty and his family live in a mansion. However, Monty hints the "clean living" clause has just expired as he kisses his mother-in-law good night and fakes heading to bed. Monty then proceeds to a hideaway to join Nicky and his friends for pizza, poker and beer.

==Cast==
* Rodney Dangerfield as Montgomery "Monty" Capuletti
* Joe Pesci as Nicholas "Nicky" Cerone
* Geraldine Fitzgerald as Mrs. Monahan
* Jennifer Jason Leigh as Allison Capuletti
* Jeffrey Jones as Clive Barlow
* Taylor Negron as Julio
* Candice Azzara as Rose Capuletti
* Tom Noonan as Paddy
* Tom Ewell as Scrappleton
* Lili Haydn as Belinda Capuletti
* Kimberly McArthur as Ginger Jones
* Jeff Altman as Bill Jones

==See also==
* List of American films of 1983

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 