The Deluge (film)
 
{{Infobox film
| name = The Deluge
| image = Potop plakat.jpg
| director = Jerzy Hoffman
| writer = Jerzy Hoffman Adam Kersten Wojciech Zukrowski
| based on =  
| starring = Daniel Olbrychski Małgorzata Braunek Tadeusz Łomnicki Krzysztof Kowalewski
| music = Kazimierz Serocki Jerzy Wójcik
| editing = Zenon Piórecki
| released =  
| runtime = 315 minutes
| country = Poland, USSR
| language = Polish
}}
 novel of the same name by Henryk Sienkiewicz. It was nominated for the Academy Award for Best Foreign Language Film at the 47th Academy Awards, but lost to Amarcord.    The film is the third most popular in the history of Polish cinema, with more than 27.6 million tickets sold in its native country by 1987.  Further 30.5 million were sold in the Soviet Union. 

==Plot== Swedish invasion The Deluge, which was eventually thwarted by the Polish-Lithuanian forces. However, a quarter of the Polish-Lithuanian population died through war and plague, and the countrys economy was devastated. 

==Versions==
The original film was digitally restored and shown on Polish TV in December, 2013.  For the films 40th anniversary, a new cut named Potop Redivivus will be released in the Fall of 2014.  It is two hours shorter than the original, to make it more accessible to present day moviegoers. 

==Cast==
* Daniel Olbrychski as Andrzej Kmicic
* Małgorzata Braunek as Oleńka Billewiczówna
* Tadeusz Łomnicki as Michał Wołodyjowski
* Kazimierz Wichniarz as Jan Onufry Zagłoba Janusz Radziwiłł
* Leszek Teleszyński as Bogusław Radziwiłł
* Ryszard Filipski as Soroka
* Wiesława Mazurkiewicz as Aunt Kulwiecówna
* Franciszek Pieczka as Kiemlicz
* Bruno OYa as Józwa Butrym
* Bogusz Bilewski as Kulwiec-Hippocentaurus
* Andrzej Kozak as Rekuć Leliwa
* Stanisław Łopatowski as Ranicki
* Stanisław Michalski as Jaromir Kokosiński
* Krzysztof Kowalewski as Roch Kowalski
* Stanisław Jasiukiewicz as Augustyn Kordecki
* Wiesław Gołas as Stefan Czarniecki
* Piotr Pawłowski as Jan II Kazimierz
* Leon Niemczyk as Charles X Gustav of Sweden
* Arkadiusz Bazak as Kuklinowski
* Ferdynand Matysik as Zamoyski

==See also== With Fire and Sword Colonel Wolodyjowski
* List of longest films by running time
* List of submissions to the 47th Academy Awards for Best Foreign Language Film
* List of Polish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 