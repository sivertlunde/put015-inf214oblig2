Sundown: The Vampire in Retreat
 
{{Infobox film
| name            = Sundown: The Vampire in Retreat
| image           = Sundown cover.jpg
| caption         = Theatrical release poster
| director        = Anthony Hickox
| producer        = Dan Ireland Jack Lorenz Jefferson Richard
| writer          = John Burgess Anthony Hickox
| starring        = David Carradine Morgan Brittany Bruce Campbell Jim Metzler Maxwell Caulfield Deborah Foreman M. Emmet Walsh Richard Stone
| editing         = Christopher Cibelli
| distributor     = Vestron Pictures
| runtime         = 104 minutes
| released        =  
| country         = United States
| language        = English
| budget          = $2.8 million
}} Western Weird Western|horror/comedy directed by Anthony Hickox and starring David Carradine, Bruce Campbell and Morgan Brittany. It was written by Hickox and John Burgess.
 Cannes release cult following. 

== Synopsis ==
Under the leadership of their ancient and powerful leader Jozek Mardulak, a colony of vampires seek a peaceful life in the desolate desert town of Purgatory. Key to the transition is the towns artificial-bloodmaking facility and it is just not working. Mardulak summons the human designer of the plant, who brings his wife and two young daughters along for what he thinks will be a pleasant desert vacation. Soon, he and his family are caught up in a civil war as another vampire elder, who abhors the idea of vampires being anything other than predators, organizes a revolution, and a descendant of the Van Helsing family arrives intent on destroying all vampires.

== Cast ==
* David Carradine as Jozek Mardulak
* Bruce Campbell  as Robert Van Helsing
* Morgan Brittany as Sarah Harrison
* Jim Metzler as David Harrison
* Maxwell Caulfield as Shane
* Deborah Foreman as Sandy White
* M. Emmet Walsh as Mort Bisby John Ireland as Ethan Jefferson
* Dana Ashbrook as Jack
* John Hancock as Quinton Canada
* Marion Eaton as Anna Trotsberg
* Dabbs Greer as Otto Trotsberg
* Bert Remsen as Milt Bisby
* Sunshine Parker as Merle Bisby
* Helena Carroll as Madge
* Elizabeth Gracen as Alice
* Christopher Bradley as Chaz
* Kathy MacQuarrie Martin as Burgundy
* Jack Eiseman as Nigel
* George Buck Flower as Bailey
* Erin Gourlay as Juliet Harrison
* Vanessa Pierson as Gwendolyn Harrison

==See also==
* Vampire film

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 