The Red Flute
 
{{Infobox film
| name           = The Red Flute
| image          = 
| caption        = 
| director       = Yermek Shinarbayev
| producer       =  Habibur Rahman Khan
| writer         = Anatoli Kim
| starring       = Aleksandr Pan Oleg Li Valentin Te Lubove Germanova
| music          = Vladislav Shute 
| cinematography = Sergei Kosmanev
| editing        = Polina Shtain
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

The Red Flute ( , literally "Revenge") is a 1989 Soviet drama film directed by Yermek Shinarbayev. It was screened in the Un Certain Regard section at the 1991 Cannes Film Festival.    The film was restored in 2010 by the World Cinema Foundation at Cineteca di Bologna /L’Immagine Ritrovata Laboratory and released as Revenge.   

==Cast==
* Aleksandr Pan
* Valentina Te
* Kasym Zhakibayev
* Lyubov Germanova
* Oleg Li
* Juozas Budraitis
* Zinaida Em
* Maksim Munzuk
* Erik Zholzhaksynov
* Nikolai Tacheyev
* Li Kham Dek
* Olga Yenzak
* Tsoi Ke Suk
* Yana Kan
* Oleg Fomin
* Galina Pak
* Gennadi Lyui
* Baiten Omarov

==References==
 

==External links==
* 

 
 
 
 
 
 
 