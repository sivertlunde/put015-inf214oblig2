Saath Saath (film)
{{Infobox film
| name           = Saath Saath 
| image          = 
| caption        =
| director       = Raman Kumar
| producer       = Dilip Dhawan
| writer         = Raman Kumar
| starring       = Rakesh Bedi Farooq Shaikh Deepti Naval Neena Gupta
| music          = Kuldeep Singh
| cinematography = Sunil Sharma
| editing        = David Dhawan
| Sound         = Jagmohan Anand
| distributor    = Mr Talwar
| Chief Assistant Director = Balwant Dullat
| released       = 4 March 1982
| runtime        = 145 minutes
| country        = India
| Shooting locations= Bombay 
| language       = Hindi
| Budget         = 12 Lakh Rupees
| preceded_by    =
| followed_by    =
| awards         = none
| Gross          = }}
Saath Saath is 1982 Hindi language Movie directed by Raman Kumar and starring Rakesh Bedi, Farooq Shaikh, Neena Gupta, Deepti Naval. Movie has beautiful Gazals from Jagjit Singh and Chitra Singh.

==Plot==
The story is about a simple man with high moral values. Avinash (Farooq Sheikh) is a poor guy with high self-respect. His policy is to earn while you learn. He excels in his studies and wishes for an idealistic society to live in. He wants to change the world with truth and simplicity.

==Cast==
* Rakesh Bedi  as  Rakesh
* Farooq Shaikh  as  Avinash Verma
* Neena Gupta  as  Neena
* Avtar Gill  as  Avtar
* A. K. Hangal  as  Professor Chaudhary
* Iftekhar  as  Mr. Gupta Javed Khan  as  Javed
* Deepti Naval  as  Gitanjali Gupta Geeta
* Yunus Parvez  as  Jagat Murari
* Satish Shah  as  Satish Shah
* Gita Siddharth  as  Mrs Gupta (Voice)
* Anjan Srivastav  as  Dr. B M Acharya
* Helena  as  Helena
* Kiran Vairale  as  Kiran
* Sudha Chopra  as  Professor

==Soundtrack==
{{Track listing
| headline        = Songs
| extra_column    = Playback
| all_lyrics      = Javed Akhtar
| all_music       = Kuldeep Singh

| title1          = Yu zindagi ki raah mein
| extra1          = Chitra Singh

| title2          = Pyaar Mujh se Jo Kiya Tumne Toh Kya Payogi
| extra2          = Jagjit Singh

| title3          = Tum ko dekha to yeh khayaal aaya Jagjit Singh, Chitra Singh

| title4          = Yeh bata de mujhe zindagi
| extra4          = Jagjit Singh, Chitra Singh

| title5          = Ye tera ghar ye mera ghar
| extra5          = Jagjit Singh, Chitra Singh
}}

== External links ==
*  

 
 
 

 