Sheer Madness
 
{{Infobox film
| name           = Sheer Madness
| image	         = Sheer Madness FilmPoster.jpeg
| caption        = Film poster
| director       = Margarethe von Trotta
| producer       = Eberhard Junkersdorf Margaret Ménégoz
| writer         = Margarethe von Trotta
| starring       = Hanna Schygulla Angela Winkler
| music          = 
| cinematography = Michael Ballhaus
| editing        = Dagmar Hirtz
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Germany
| language       = German
| budget         = 
}}

Sheer Madness ( , and also released as Friends and Husbands) is a 1983 German arthouse drama film directed by Margarethe von Trotta.    It was entered into the 33rd Berlin International Film Festival.   

==Cast==
* Hanna Schygulla as Olga
* Angela Winkler as Ruth
* Peter Striebeck as Franz
* Christine Fersen as Erika
* Franz Buchrieser as Dieter
* Wladimir Yordanoff as Alexaj
* Agnes Fink as Ruths mother
* Felix Moeller as Christof
* Jochen Striebeck as Bruno
* Therese Affolter as Renate
* Werner Eichhorn as Schlesinger
* Karl Striebeck as Brunos father

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 