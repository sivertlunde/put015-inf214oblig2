In the City of Sylvia
{{Infobox film
| name           = In the City of Sylvia
| image          = City of Sylvia Poster.jpg
| caption        = 
| director       = José Luis Guerín 
| producer       = Luis Miñarro Gaëlle Jones
| writer         = José Luis Guerín 
| narrator       = 
| starring       = Pilar López de Ayala Xavier Lafitte
| music          = 
| cinematography = Natasha Braier
| editing        = Núria Esquerra
| distributor    = Axiom Films  
| released       =  
| runtime        = 84 minutes
| country        = Spain
| language       = French Spanish
| budget         = 
| gross          = 
}}
In the City of Sylvia ( ) is a   in search of Sylvia, a woman he asked for directions in a bar six years before.

==Critical reception==
The film appeared on some critics top ten lists of the best films of 2008. V.A. Musetto of the New York Post named it the best film of 2008,  J. Hoberman of The Village Voice named it the 8th best film of 2008.,      and Sam C. Mac of In Review Online   named it the 2nd best film of 2008.

==Awards and nominations==
En la Ciudad de Sylvia was nominated for the 2007 Golden Lion at the Venice Film Festival, although the award was eventually presented to Ang Lee for Lust, Caution. En la Ciudad de Sylvia won the 2008 Australian Film Critics Association award for the best unreleased film in Australia at the time of the awards. Guerín won the best director award of the Premios ACE 2009 for this film. 

==References==
 

==External links==
*  , website of the production company including details of the film.
*  
*  
*  

 
 
 
 
 