The Telephone Operator (1932 film)
{{Infobox film
| name = The Telephone Operator 
| image =
| image_size =
| caption =
| director = Nunzio Malasomma
| producer = 
| writer = Friedrich Dammann (novel)   Herbert Rosenfeld (novel)   Aldo Vergano   Raffaello Matarazzo   Ernst Wolff
| narrator =
| starring = Isa Pola   Mimi Aylmer   Luigi Cimara   Sergio Tofano
| music = Otto Stransky 
| cinematography = Anchise Brizzi
| editing = Nunzio Malasomma
| studio = Società Italiana Cines 
| distributor = Cinès-Pittaluga
| released = 1932
| runtime = 80 minutes
| country = Italy Italian
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Telephone Operator (Italian:La telefonista) is a 1932 Italian comedy film directed by Nunzio Malasomma and starring Isa Pola, Mimi Aylmer and Luigi Cimara. It was a remake of the German film Wrong Number, Miss, released the same year. 

==Cast==
* Isa Pola as La telefonista 
* Mimi Aylmer as La mannequin 
* Luigi Cimara as Il direttore del telefoni 
* Sergio Tofano as Bàttigo - the tenor 
* Camillo Pilotto as La cameraman 
* Giovanni Grasso as Gedeone 
* Marcella Rovena as Bàttigos wife

== References ==
 

== Bibliography ==
* Reich, Jacqueline & Garofalo, Piero. Re-viewing Fascism: Italian Cinema, 1922-1943. Indiana University Press, 2002.
* Mancini, Elaine. Struggles of the Italian film industry during fascism, 1930-1935. UMI Research Press, 1985.

== External links ==
* 

 
 
 
 
 
 
 
 
 


 