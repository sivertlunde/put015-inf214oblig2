It's Nice to Have a Mouse Around the House
{{multiple issues|
 
 
 
}}

{{Infobox Hollywood cartoon
| cartoon_name = Its Nice to Have a Mouse Around the House
| series = Looney Tunes
| image =
| caption =
| director = Friz Freleng Hawley Pratt John Dunn Don Williams Bob Matz Norm McCabe
| layout_artist = Dick Ung
| background_artist = Tom OLoughlin
| musician = Bill Lava
| voice_actor = Mel Blanc Ge Ge Pearson
| producer = David H. DePatie Friz Freleng
| studio = DePatie-Freleng Enterprises
| distributor = Warner Bros. Pictures
| release_date = January 16, 1965
| color_process = Technicolor
| runtime = 6
| movie_language = English
}}
 Sylvester and Granny (Looney Tunes)|Granny. Released January 16, 1965, the cartoon is directed by Friz Freleng and Hawley Pratt. The voices were performed by Mel Blanc and Ge Ge Pearson.

==Characters== later in 1965.

==Plot==
Speedy Gonzales invades Grannys home and drives Sylvester to a nervous breakdown. Concerned about the welfare of her cat, Granny calls on the Jet Age Pest Control to remove the rodent. Daffy Duck is assigned the job.

When conventional traps fail, the determined Daffy decides to use a series of contraptions to capture Speedy. However, Speedy is always one step ahead of the duck, and Daffy winds up getting the worst of his machinery.

The final attempt sees Daffy try to program a robot with a card featuring Speedys picture, but Speedy grabs a Daffy Duck comic book and fools the robot. The robot is seen chasing Daffy out of the house as Speedy watches and remarks to the audience: "Its pretty nice having a mouse around the house, no?"

==Succession==
 
{{succession box |
before= The Iceman Ducketh | Daffy Duck cartoons |
years= 1965 | Moby Duck|}}
 

==References==
* Friedwald, Will and Jerry Beck. "The Warner Brothers Cartoons." Scarecrow Press Inc., Metuchen, N.J., 1981. ISBN 0-8108-1396-3.

==External links==
*  

 
 
 
 


 