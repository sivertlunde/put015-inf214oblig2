Suburban Melody
{{Infobox film
| name = Suburban Melody
| image = Carlos Gardel - Melodia de Arrabal - 1933.jpg
| image_size =
| caption = 
| director = Louis J. Gasnier
| producer = 
| writer =   Alfredo Le Pera 
| narrator =
| starring = Carlos Gardel   Trini Ramos   Blanca Vischer   Vicente Padula
| music = Marcel Lattès   Raoul Moretti   Horacio Pettorossi   Modesto Romero Martinez   José Sentís   Alberto Castellanos
| cinematography = Harry Stradling Sr. 
| editing =     
| studio = Paramount Pictures
| distributor = Paramount Pictures
| released = 5 April 1933    
| runtime = 94 minutes
| country = United States Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Suburban Melody (Spanish:Melodía de arrabal) is a 1933 American musical film directed by Louis J. Gasnier and starring Imperio Argentina, Carlos Gardel and Vicente Padula.  The film was made at the Joinville Studios in Paris by Paramount Pictures, who produced a large number of films in different languages at the studios. The film was made in Spanish language|Spanish, primarily for release in Spanish-speaking countries. Carlos Gardel appeared in a string of such productions during the 1930s. The film was extremely popular in Argentina, the native country of its three stars, where it was one of the highest-grossing releases. 

==Synopsis==
After she hears his voice, a music teacher encourages a gambler to pursue a career as a professional singer. He enjoys success, but his former criminal connections threaten to wreck his progress.

==Cast==
* Imperio Argentina as Alina 
* Carlos Gardel as Roberto Ramírez  
* Vicente Padula as Gutiérrez  
* Jaime Devesa as Rancales 
* Helena DAlgy as Marga   Felipe Sassone as Empresario  
* Manuel París as Maldonado 
* Manuel Argüelles as Julian  
* Josita Hernán as Estudiante de música

== References ==
 

== Bibliography ==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer, 2008.
* Finkielman, Jorge. The Film Industry in Argentina: An Illustrated Cultural History. McFarland, 2003.
* Nataša Durovicová, Kathleen E. Newman. World Cinemas, Transnational Perspectives. Routledge, 2010.

== External links ==
* 

 
 
 
 
 
 
 

 