Gable and Lombard
{{Infobox film
| name           = Gable and Lombard
| image          = GableAndLombard.JPG
| image size     = 
| alt            = 
| caption        = Theatrical film poster
| director       = Sidney J. Furie
| producer       = Harry Korshak
| writer         = Barry Sandler
| starring       = James Brolin Jill Clayburgh
| music          = Michel Legrand
| cinematography = Jordan Cronenweth
| editing        = Argyle Nelson, Jr.	 Universal Pictures
| distributor    = 
| released       =   (New York City, New York)
| runtime        = 131 minutes
| country        = United States English
| budget         = 
| gross          = 
}} American biographical film directed by Sidney J. Furie. The screenplay by Barry Sandler is based on the romance and consequent marriage of screen stars Clark Gable and Carole Lombard. The original music score was composed by Michel Legrand.

==Synopsis== evening wear screwball comedienne Lombard arrives in an ambulance that wrecks his car. They argue. He threatens to spank her. She punches him on the jaw. The two clearly dislike each other, and intensely so, but as fate conspires to bring them together again and again, they begin to admire each other and fall in love.
 MGM executive happily ever defense bonds during World War II.

==Principal cast==
*James Brolin ..... Clark Gable
*Jill Clayburgh ..... Carole Lombard
*Allen Garfield ..... Louis B. Mayer
*Red Buttons ..... Ivan Cooper
*Joanne Linville ..... Ria Gable
*Melanie Mayron ..... Dixie
*Morgan Brittany ..... Vivien Leigh

==Principal production credits== Producer ..... Harry Korshak Original Music ..... Michel Legrand
*Cinematography ..... Jordan Cronenweth Production Design ..... Edward C. Carfagno
*Costume Design ..... Edith Head

==Critical reception==
In his review in The New York Times, Vincent Canby called the film  

Roger Ebert of the Chicago Sun-Times described the film as a "mushy, old-fashioned extravaganza" and added,  

Variety (magazine)|Variety called it  

Time Out London says,  

TV Guide awarded it one out of a possible four stars, calling it  

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 