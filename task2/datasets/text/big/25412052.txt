Joy House (film)
{{Infobox film
| name           = Joy House
| image          = Joy House movieposter.jpg
| director       = René Clément 
| producer       = Jacques Bar
| writer         = René Clément
| starring       = Jane Fonda Alain Delon Lola Albright
| music          = Lalo Schifrin
| cinematography = Henri Decaë
| editing        = Fedora Zincone
| distributor    = Metro-Goldwyn-Mayer
| released       = June 12, 1964
| runtime        = 97 minutes
| country        = France
| language       = English/French gross = 1,414,966 admissions (France) 
}}

Joy House (French title: Les Félins) is a 1964 black-and-white mystery/thriller film starring Jane Fonda, Alain Delon, and Lola Albright. It is based on the 1954 novel by Day Keene.

It was Clements second film for MGM. Rene Clement Hired by M-G-M To Direct Love Cage in France
By EUGENE ARCHER. New York Times (1923-Current file)   26 Apr 1963: 29.  
==Plot==
In Monte Carlo, Marc, a handsome cardsharp, escapes American gangsters who have been ordered to kill him by the boss of a New York gang because he had an affair with the bosss wife. Marc hides in a mission for the poor where Barbara, a wealthy widow, finds him and hires him as her chauffeur. At Barbaras chateau, Melinda, Barbaras niece, becomes attracted to him. Marc discovers that Barbara is hiding her lover, Vincent, in the secret rooms and passageways of the chateau. She and Vincent (a bank robber sought by the police for murdering Barbaras husband) plan to murder Marc so that Vincent may use his passport in escaping to South America. Marc and Barbara begin an affair but are discovered by Vincent, who then kills Barbara but is himself killed by the American gangsters, who mistake him for Marc. Marc and Melinda plan to dispose of the two bodies, but when Melinda learns that Marc is planning to leave without her, she tricks the police into believing Marc guilty and forces him to hide in the chateaus secret rooms. He is her prisoner, just as Vincent had been her aunts.

==Cast==
* Jane Fonda as Melinda
* Alain Delon as Marc
* Lola Albright as Barbara
* Sorrell Booke as Harry
* Carl Studer as Loftus
* André Oumansky as Vincent
* Arthur Howard as Father Nielson
==Production==
It was Jane Fondas first movie in France. She later recalled that the director made it without a script:
 I didnt speak very good French then, and I never understood much of what was going on. The only people who really dug that movie, for some reason, were junkies. They used to come up to me and give me a big wink. But Im awfully glad I did it because it got me into France and I met (  Roger Vadim|Vadim. Heres What Happened to Baby Jane
By GERALD JONAS. New York Times (1923-Current file)   22 Jan 1967: 91.   

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 

 