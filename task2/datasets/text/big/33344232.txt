The Chicago 8
{{Infobox film
| name           = The Chicago 8
| image          = The Chicago 8 Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Pinchas Perry
| producer       = Pinchas Perry Al Bravo Kate Connor Alain Jakubowicz James Mathers
| writer         = Pinchas Perry
| starring       = Philip Baker Hall Gary Cole Steven Culp Mayim Bialik Danny Masterson
| music          = Peter Bateman Shay Raviv
| cinematography = James Mathers
| editing        = Richard Halsey Colleen Halsey  
| studio         =  
| distributor    =  
| released       =  
| runtime        =  
| country        = United States
| language       = English
}}
The Chicago 8 is a 2010 American drama film written and directed by Pinchas Perry and starring Philip Baker Hall, Gary Cole, Steven Culp and Mayim Bialik. The film is based on  actual court transcripts from the Chicago Seven trial.

==Plot== Democratic National Daley used police force to brutally quell the rally, and as a result, rioting broke out in the streets. One person was killed, hundreds were injured and thousands were arrested. One year later, newly elected President Richard Nixon gave the green light to prosecute eight men who were accused of conspiracy to incite a riot. The FBI brought them to Chicago, where they stood trial. They were known thereafter as The Chicago 8. 

==Cast== Judge Julius J. Hoffman William Moses "Bill" Kunstler Thomas Aquinas "Tom" Foran Nancy Sarah Kurshan Robert George "Bobby" Seale Richard Schultz Abbot Howard "Abbie" Hoffman
* Danny Masterson as Jerry Rubin Anita Hoffman (née Kushner)
* Wade Williams as Allen Ginsberg

Incidentally, Philip Baker Hall, Gary Cole, and Steven Culp played recurring characters on the television show The West Wing.

==Production==
The Chicago 8 commenced filming in September 2009 in   in August 2010 and has since screened at the Museum of Tolerance Film Festival, at the 11th annual Beverly Hills Film Festival and at the Santa Cruz Film Festival.  The film has received the top prize of Best Feature Film 2010 at the Peachtree International Film Festival and Best Feature Film 2011 at the Beverly Hills Film Festival prior to its theatrical release. 

Perry, Al Bravo, Shirly Brener, Kate Connor, Alain Jakubowicz, and James Mathers all serve as producers. The film is edited by Colleen Halsey and Richard Halsey with James Mathers serving as cinematographer. http://vimeo.com/43550574 

==Release== premiered at Atlanta in 11th annual Beverly Hills Film Festival in April 2011 and at the Santa Cruz Film Festival in May 2011. 

==Accolades==
Prior to release, The Chicago 8 has been nominated for a number of awards from its Film Festival releases. The film has received two significant awards. The Chicago 8 was awarded the top prize of Best Feature Film 2010 at the Peachtree International Film Festival in 2010. In 2011 the film attained the leading award of Best Feature Film 2011 at the Beverly Hills Film Festival prior to its theatrical release. 

==See also==
* Chicago 10 (film)|Chicago 10 (film)

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 