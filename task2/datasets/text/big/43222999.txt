The Seventeenth Kind
The Seventeenth Kind  is a film version of a short story by Michael Marshall Smith.
A sci-fi motion picture adaptation comedy, by director Andy Collier

Starring: Tony Curran, Sylvester McCoy, Brian Blessed, Lucy Pinder, Ralph Brown, Miriam Margolyes.

Scheduled release date, Winter 2014-2015. 

==Critical reception==
Reviews for the film have been positive. Dave Ollerton of The London Film Review called the film "a very funny, highly entertaining and excellently crafted thirty minute film that doesn’t drag once" and "sterling stuff".    Paul Simpson of Sci-Fi Bulletin said that the film was "a highly entertaining sharp tale".   

==References==
 

==External links==
* 

 
 