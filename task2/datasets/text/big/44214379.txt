Bangkok Assassins
{{Infobox film
| name           = Bangkok Assassins
| image          = 
| image_size     = 
| caption        = 
| director       = Yuthlert Sippapak
| producer       = 
| genre          = Action
| story          =  
| based on       =  Mario Maurer
*Arak Amornsupasiri
}}
| music          = 	 	
| cinematography = Tiwa Moeithaisong	
| editing        = Tawat Siripong
| studio         =  Grindstone Entertainment Group (USA)
*Lionsgate Home Entertainment (USA)
}}
| released       =  
| country        = Thailand Thai
| Sound          = Dolby Digital
| Runtime        = 104 min 
}}

Bangkok Assassins or Bangkok Kung Fu is a 2011 Thai action movie starring Mario Maurer and Arak Amronsupasiri. This movie was released on September 1, 2011 and distributed by Grindstone Entertainment Group and Lionsgate Home Entertainment in United States.

==Plot==
Five kids fled from a mafia who abused them as beggars. They ended up meeting the great kung fu master who adopted them and taught them how to fight. Now, the kids are grownups and become the magnificent five kung fu fighters who have only one mission: to avenge the mafia who destroyed their lives. 

==Cast==
*Mario Maurer as Naa
*Arak Amornsupasiri as Phong
*Vitsawa Thaiyanon as Kaa
*Artikitt Prinkprom as Chi
*Jarinya Sirimongkolsakul as Kor-ya

==External links==
*  

==References==
 

 
 


 
 