Talaash: The Answer Lies Within
 
 
 

{{Infobox film
| name             = Talaash: The Answer Lies Within
| image            = Talaash poster.jpg
| caption          = Theatrical release poster
| director         = Reema Kagti
| producer         = Ritesh Sidhwani Aamir Khan Farhan Akhtar
| screenplay       = Reema Kagti Zoya Akhtar
| writer           = Farhan Akhtar  (Dialogue)   Anurag Kashyap   (Additional Dialogue) 
| story            = Reema Kagti Zoya Akhtar
| starring         = Aamir Khan Rani Mukerji Kareena Kapoor Khan
| music            = Ram Sampath
| cinematography   = K. U. Mohanan
| costume designer = Rushi Sharma
| editing          = Anand Subaya
| studio           = Excel Entertainment Aamir Khan Productions
| distributor      = Reliance Entertainment
| released         =  
| runtime          = 139 minutes   
| country          = India
| language         = Hindi
| budget           =  
| gross            =   (worldwide) 
}}
 mystery thriller UA certificate Censor Board of India.    The movie released on 30 November 2012.    Box Office India declared Talaash a "Semi Hit".  It eventually grossed   worldwide.   

==Plot==
Late at night on a deserted road, well-known actor Armaan Kapoor (Vivan Bhatena) loses control of his car for no evident reason, apparently trying to avoid something, and drives into the sea. Inspector Surjan Shekhawat (Aamir Khan) with his assistant Devrath Kulkarni (Rajkummar Rao) begins an investigation into Armaans death. Surjans personal life is in turmoil since his 8-year-old son, Karan, drowned recently during a family outing. His wife, Roshni (Rani Mukerji), is still grieving and Surjan has developed a guilt complex.

Surjan learns that on the night of Armaans accident, Armaan was travelling with 2 million rupees in cash, which is missing from the car. Armaan had given the money to a blackmailer, a pimp named Shashi (Subrat Dutta). Three years back, Shashi had performed a cover-up for Armaan and his friend Sanjay Kejriwal (Suhas Ahuja). Once he learns of Armaans death, Shashi takes off with the cash, leaving behind a mistress, Mallika (Aditi Vasudev). Shashis friend, Tehmur (Nawazuddin Siddiqui), steals Shashis discarded sim card. Shashi begins blackmailing Sanjay. In panic, Sanjay employs some thugs to murder Shashi. Tehmur begins to use Shashis old sim card to blackmail Sanjay with Shashis murder.
 medium (Shernaz Patel) who is in touch with Karans soul. One night, Surjan is propositioned by an Call girl|escort, Rosie (Kareena Kapoor). He declines Rosies advances but asks her if she can become an informer. Through Rosie, Surjan finds out about Shashis connection with the case. The police find Shashis body, along with the blackmail money and a DVD. The DVD contains embarrassing CCTV footage of Armaan and his friends leaving a hotel with a girl. Convinced of Armaans link with prostitutes, Surjan focuses his search on the slums near the Hotel Lido. To speak unobserved, Rosie takes Surjan to a quiet place by the riverside and tells him that three years ago, she was picked up by Armaan and his friends, Sanjay and Nikhil (Prashant Prakash). However, she doesnt reveal anything more. Surjan tries to approach Nikhil for more information but discovers that Nikhil has been brain-dead after the incident three years ago. After a heated argument with Roshni, Surjan confides in Rosie, who comforts him by taking him to the Lido.

Tehmur is killed by the two thugs in Sanjays employ. One of the thugs is captured by the police and reveals that Sanjay has been behind the murders of both Shashi and Tehmur. Surjan arrests Sanjay, who confesses that three years ago, Sanjay, Armaan and Nikhil had picked up an escort to celebrate Nikhils birthday. While driving, Nikhil began making out with the girl in the backseat. The car door opened by accident, throwing both Nikhil and the girl from the moving car to the road. Both were badly injured. Armaan and Sanjay rushed Nikhil to the hospital but fearing scandal, left the girl unattended on the road. Sanjay had called Shashi to ask him to take care of her. However, Shashi informed them that the girl had died and that he had footage of the three leaving with her. Since then, Shashi had continued to extort money from them by blackmail.

Surjan re-watches the footage and realizes that the girl is Rosie. As he drives Sanjay to the police station, they approach the same road where Armaan had his car accident. Both Surjan and Sanjay see Rosie standing ahead. In a re-enactment of Armaans accident, Sanjay swerves the car into the river. With Sanjay already dead, Surjan struggles to get himself out of the car. Rosie appears underwater and saves him. At the police station, Surjans superior advises him against reporting his incredible story. Surjan agrees to write in the official report that Armaan Kapoors death was an accident. He returns to the riverside place where he used to meet Rosie and uncovers her remains where Shashi had hidden her body. Remembering that he had been told that "the dead come to those who are troubled," he gives her a proper funeral.

Now more accepting of the supernatural, Surjan finds a letter from Karan, penned by the medium. Through the letter, Karan tells Surjan not to blame himself for his death and that he wants both his parents to be happy. Surjan cries and Roshni consoles him. The two are reconciled.

==Cast==

* Aamir Khan as Inspector Surjan Singh Shekhawat
* Rani Mukerji as Roshni Shekhawat
* Kareena Kapoor Khan as Rosie / Simran
*Shadab Shaikh as Raju 
* Nawazuddin Siddiqui as Tehmur
* Shernaz Patel as Frenny
* Rajkummar Rao as Devrath Kulkarni
* Sheeba Chaddha as Nirmala
* Vivan Bhatena as Armaan Kapoor in flashback
* Pariva Pranati as Soniya Kapoor
* Aditi Vasudev as Mallika
* Subrat Dutta as Shashi
* Gulfam Khan as Madam ASP

==Production==

===Development===
Following the release of her first film, Honeymoon Travels Pvt. Ltd. (2007), Kagti was approached and offered to direct many films by several production houses.    However, unable to find any films that interested her, she began working on a new script with her friend Zoya Akhtar in early 2010.  Speaking about it, she said, "Zoya and I thought of the story and co-wrote it. We do a lot of writing together. We are also best friends so it is fun and there is a huge comfort factor."  Initially, Saif Ali Khan, Salman Khan and Shahrukh Khan were reportedly approached to play the male lead but opted out due to differing reasons.    Upon finalising the story, she approached Aamir Khan with the script in November 2010 and he agreed to produce and star in it.  Since the start of pre-production, the project has faced several speculations regarding its title.    In November 2011, Khan confirmed that the film was titled Talaash: The Answer Lies Within. 

In an interview with The Indian Express, Kagti revealed that a few months before casting Khan in the film, she had approached him with another project.  Later, Rani Mukerji and Kareena Kapoor were signed on to star opposite Khan.  Prior to the start of principal photography, the actors attended acting workshops and signed a non-disclosure agreement.    To prepare for a key scene in the film, Khan took swimming lessons. 

===Filming===
The principal photography for the film started in March 2011 with Khan and Mukerji in Mumbai.  For a scene involving Khan and Kapoor, it was reported that both the actors would shoot in a red-light district.  However, Kagti dismissed it and explained that it wouldnt be safe to do so. The scene was later shot at the Leopold Cafe on Colaba Causeway.  The films second phase continued with the entire cast in Pondicherry and was completed by the end of August.  The final phase was expected to commence the following month in Khopoli with an underwater shot. 

Aamir, who never knew how to swim went under rigorous training for this underwater sequence. He was trained for three months under a specialist trainer and went well prepared for the shoot.  Due to visibility problems, it was cancelled and later filmed at an undisclosed water studio in London.  The filming was completed by November 2011 after some of the remaining shots were executed at the Bandstand Promenade. 

==Soundtrack==
The soundtrack of Talaash: The Answer Lies Within, released by T-Series on 19 October 2012, featured music composed by Ram Sampath with lyrics penned by Javed Akhtar. 

{{Infobox album |  
  Name         = Talaash: The Answer Lies Within
|  Type        = Soundtrack
|  Artist      = Ram Sampath
|  Cover       = 
|  Released    =  19 October 2012 (India)
|  Recorded    = 2010–2012 OmGrown Studios, Mumbai Sterling Sound, New York Feature film soundtrack
|  Length      = 26:22
|  Label       =   T-Series
|  Producer    = Ritesh Sidhwani Farhan Akhtar Aamir Khan
|  Reviews     = Delhi Belly  (2011)
|  This album  = Talaash: The Answer Lies Within (2012)
|  Next album  = Fukrey (2013)
}}

{{track listing
| headline = Tracklist
| extra_column = Singer(s)
| total_length =
| title1 = Muskaanein Jhooti Hai
| extra1 = Suman Sridhar
| length1 = 3:20
| title2 = Jee Le Zaraa
| extra2 = Vishal Dadlani
| length2 = 3:37
| title3 = Jiya Lage Na
| extra3 =  Sona Mohapatra, Ravindra Upadhyay
| length3 = 4:37
| title4 = Hona Hai Kya
| extra4 = Ram Sampath, Rupesh
| length4 = 4:43
| title5 = Laakh Duniya Kahe
| extra5 = Ram Sampath
| length5 = 6:04
| title6 = Jee Le Zaraa (Remix)
| extra6 = Vishal Dadlani
| length6 = 3:59
}}

===Reception===

The soundtrack met with extremely positive reviews.
Indiaglitz quoted, "Once again Ram Sampath has an unmatched compilation of tracks here. Along with Javed Akhtars fantastic lyrics they have created a masterpiece here."  Bollyspice reviewed the album stating, "The soundtrack to Talaash is like a box of chocolates: it contains a little good and a little not so good in it. Yet it is still an album that is worth listening!".  Rediff reviewed, "Except a steady undercurrent of anxiety, the soundtrack of Talaash doesnt have a fixed theme or ambiance. Every song belongs to a unique genre and fosters the enigma of Kagtis suspense drama. And thats what makes Sampaths efforts truly appreciable."  Planet Bollywood said, "A film made by Aamir Khan Productions doesn’t need an item song or song for the masses to make it successful. Instead the music is made solely for the film in mind. If the listener can appreciate it before the release of the film then that is a bonus but that’s not essential."  Sify quoted, "Overall, the soundtrack is not out-of-the-box, but the overdose of electronic music doesnt bother either. Thus, its an album which may not top the charts, but some songs could definitely make their way into many peoples personal playlists."  Gomolo stated, "Ram Sampath is continuously giving very fresh and stunning music for Aamir. The album is full of fresh, scrumptious and susceptible tracks."  At Musicperk the album was summarised as, "Well,the answer lies within!", the tagline of the film itself. 

==Marketing==
Talaash: The Answer Lies Within tied up with Windows 8 for its marketing.  Aamir Khan promoted the film on the crime detective series C.I.D. (Indian TV series)|C.I.D.. The episodes were aired on 23 and 24 November 2012.   Aamir Khan visited Lucknow to promote Talaash: The Answer Lies Within.  Aamir Khan also appeared in a TV special show Nayi Soch Ki Talaash Aamir Ke Saath to promote Talaash.  Aamir Khan also visited Jalandhar for promotion purposes.  The premiere of the movie Talaash was held at PVR Cinema in Mumbai on 29 November 2012. 

==Release==
 
Originally scheduled to release in October 2011, the film was postponed to its tentative release date of 1 June 2012.    Media reports claimed that the reason behind the delay was due to the similarities with  , co-producer   starrer. (3 Idiots was released in 415 screens overseas.  ) The estimate for the number of release screens in India is around 2,500. 

In February 2012, Reliance Entertainment acquired the distribution rights of the film for    whilst its satellite rights were sold for  . 

===Controversy===
After taking offense at Aamir Khans TV show Satyameva Jayate (SMJ) for "projecting men in a bad light", several mens rights activists are now taking up cudgels against his film.  Virag Dhulia, the head of Gender Studies at a mens rights community center, said "We are not against Talaash, but Aamir. We want to send out the message that he cant portray a negative image of men."  

==Critical reception==

===India===
Talaash received mostly positive reviews from critics all-over India.      gave it 3.5 out 5 stars while commenting "Talaash successfully whets the appetite of all the Aamir Khan fans. As for the story, you can always trust Zoya Akhtar to give her best. Talaash is not a flawless film, but it is a fascinating tale that compels one to look beyond that which is evident. You can’t miss this one."  Saibal Chatterjee Of NDTV rated the film 3.5 out of 5 stars,noting"By no means is Talaash the end of your search for the perfect whodunit. But there is so much going for this compelling, slow-burning, well-acted tale set in the dark, grimy underbelly of Mumbai that you can barely take your eye off the screen." 

Meena Iyer of The Times of India award the film 3.5 out of 5, adding "You may not like Talaash, if mystery and intrigue set at a languid pace is not what you look out for in your matinee outing."  Rajeev Masand of CNN-IBN gave it 3.5 out of 5 while remarking "Its a very watchable film but not an unforgettable one."  Aseem Chhabra for Rediff.com gave the film 3.5 out of 5,reviewing "In Talaash, Kagti weaves a complex web, and she colours it with the mood and the atmosphere she and her collaborators create. Talaash has the feel of a noir film.It is an intelligent film but falters towards the end."  Anupama Chopra of Hindustan Times gave it 3 out of 5,stating "For once, I also feel the need to explain my rating: I wanted to give four stars to the first half and two stars to the second half, so it averages out to three. See Talaash and do post comments. Im curious to see how many were as furious as me." 

India Today gave 3.5 out 5 to Talaash and stated "Talaash creates its chills primarily tapping into the dark side of the mind, which makes it an unusual Bollywood film. Any resemblance to Joseph Payne Brennan and Donald M. Grants 1979 novella, Act of Providence, is purely coincidental."  Raja Sen for Rediff has given 2.5 out of 5 stars while adding that "Talaash is well made and strongly acted but is not as enjoyable to watch as it deserved to be."     Shubra Gupta of The Indian Express rated the film 2.5 out of 5,remarking "Talaash starts out as a smart, well-written noir-ish thriller, and then slips between the tracks. Pity about the second half."  Deccan Chronicle gave 2 out of 5 and stated "See Talaash only if you believe that Aamir Khan can do no wrong. Otherwise you’ll be left with a sinking feeling."  The Hindu stated that "It’s not like we haven’t seen anything like this before. If Kahaani used a key scene from a film called Taking Lives as a final twist, Talaash uses one of the most abused endings of this genre."  Firstpost stated that Talaash is a brave effort. Mainstream Bollywood movies try to keep it linear and uncomplicated. Directors avoid dealing with multiple strong subplots as might lead to a clutter. 

===Overseas===
Talaash received mostly positive reviews overseas.    Rubina Khan of   gave 4 out of 5 stars and stated Talaash is the perfect marriage of cinematography, cast, dialogue and direction, despite all the evident hallmarks of Khans own brand of brilliance, harking back to a bygone genre, the conclusion to the story might be deemed a tad too far-fetched for a modern audience.  Open Magazine gave the film 1 out of 5 stars and said the film was plagiarised from The Sixth Sense and acted by Aamir Khan. 

==Box office==

===India===
Talaash: The Answer Lies Within had a good opening at the multiplexes, where the collections were in the average of 65–70%.       The opening in the single screens were low, at an average of 40%.     The movie collected   nett in its opening day.    The movie showed considerable growth in its second day, collecting   nett.    The film had a good collection of   in its first weekend.    The film further collected   on Monday.    Talaash netted   in its first week.    The film netted   in second week to make a total of around 810&nbsp;million in two weeks.  The film further netted   in its third week.    According to Box Office India,Talaash has grossed 895.0&nbsp;million nett in three weeks.    Talaash added 7.5&nbsp;million nett in week four taking its total to 902.5&nbsp;million nett.    The films final total was approximately   nett. 

===Overseas===
Talaash had done excellent business overseas. Talaash had a $4.7 million opening at the overseas box office.    In its second weekend, the film grossed $7.25 million and was emerged a "Super Hit".    Talaash further netted US$8 million plus in 17 days in overseas.     Talaash final overseas gross was US$8.5 million at the end of its overseas run and was fourth highest overseas grossing Bollywood film of 2012.    

==Awards==

Rani Mukherji won the Best Actress in a supporting Role award at the South African Indian Film Awards in 2013. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 