Queens Logic
 
{{Infobox Film
| name = Queens Logic
| image = Queenslogicposter.jpg
| caption = 
| director = Steve Rash Russell Smith
| writer = Tony Spiridakis Joseph W. Savino
| starring = Kevin Bacon Jamie Lee Curtis Linda Fiorentino John Malkovich Joe Mantegna Ken Olin Tony Spiridakis Tom Waits Chloe Webb
| music = Joe Jackson
| cinematography = Amir M. Mokri Patrick Kennedy
| distributor = Seven Arts Pictures
| released = February 1, 1991
| runtime = 113 minutes
| country = United States
| language = English
| budget = $12 million
| gross = $612,781
}}
 1991 comedy comedy from Seven Arts Pictures starring Kevin Bacon, Linda Fiorentino, Joe Mantegna, Jamie Lee Curtis, John Malkovich, Ken Olin, Chloe Webb and Tom Waits. It was directed by Steve Rash.

==Plot==
 
When childhood friends Al, Dennis and Eliot get together for Rays wedding, which may or may not happen, they end up on a roller-coaster ride through reality. During one tumultuous, crazy weekend, they face adulthood and each other with new found maturity and discover what Queens Logic is all about. This comedy takes a look at friendship, loyalty, and love.

==Cast==
*Kevin Bacon as Dennis
*Jamie Lee Curtis as Grace
*Linda Fiorentino as Carla
*John Malkovich as Eliot
*Joe Mantegna as Al
*Ken Olin as Ray
*Tony Spiridakis as Vinny
*Tom Waits as Monte
*Chloe Webb as Patricia
*Michael Zelniker as Marty
*Kelly Bishop as Maria
*Terry Kinney as Jeremy

==Reception==

The movie gained mixed reviews.   

===Box Office===

The movie was not successful on limited release. 

==Production and DVD release== Pioneer label, the second time in 2002 under the Platinum Disc label, and the third time that same year by Artisan Home Entertainment. The DVD contains very little, just the film and its theatrical trailer. 

Queens Logic was filmed in the summer of 1989, but didnt get released until February 1991. Although released theatrically in the US, Queens Logic was released direct-to-video in the UK.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 

 