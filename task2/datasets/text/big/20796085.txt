Our Mother's House
{{Infobox film
|name=Our Mothers House
|image=Our Mothers House FilmPoster.jpeg
|director=Jack Clayton
|producer=Jack Clayton
|starring=Dirk Bogarde
|writer=Jeremy Brooks and Haya Harareet, based on the novel by Julian Gloag 
|music=Georges Delerue studio = Filmways Pictures MGM
|released=July 1967
|runtime=104 minutes
|country=United Kingdom
|awards= English
|budget=
|preceded_by=
|followed_by=
}} British drama film, directed by Jack Clayton. It nominally stars Dirk Bogarde (who only appears in the films second half) and principally features a cast of seven juvenile actors, including Pamela Franklin, Phoebe Nicholls and Mark Lester, with popular British actress Yootha Joyce in a supporting role. The screenplay was written by Jeremy Brooks and Haya Harareet, based on the 1963 novel of the same name by Julian Gloag.  
==Plot==
The seven Hook children, whose ages range from five to fourteen, live in a dilapidated Victorian house in suburban London. The older children help to care for their invalid single mother, whose chronic illness has led to her to convert to fundamentalist religion and refuse all medical help. When their mother dies suddenly, the children realise that they may be split up and sent to orphanages, so they decide to conceal their mothers death and carry on with their daily routine as if she were still alive. They secretly bury their mother in the back yard at night, and convert the garden shed into a shrine to her, where they periodically hold seances to communicate with her spirit.

The eldest child, Diana, takes charge. The children make excuses for their mothers absence to their neighbours and teachers, claiming that the doctor has sent her to the seaside for her health, and they  dismiss their abrasive housekeeper, Mrs Quayle. The children realise they can support themselves after Elsa discovers that younger brother Jiminee can convincingly forge their mothers signature, enabling them to cash the trust fund cheques that arrive for her each month, and they also discover that their mother has left over £400 in her savings account. Some of the children suggest contacting their estranged father, but the idea is rejected by Elsa, who has been indoctrinated with her mothers bitter contempt for her shiftless ex-husband. For the next six months, the children carry on an outwardly normal life, although conflict arises when Gerty innocently takes a ride on a strangers motorbike. 

Horrified by Gertys contact with an outsider, Diana consults their mothers spirit. The siblings denounce Gerty as a harlot and they punish her by taking away the precious comb their mother had given her, and cutting off her long hair. Soon after, Gerty falls ill, and although Diana follows their mothers practice and refuses to get  a doctor, Gerty eventually recovers.

When oldest brother Hubert discovers that Elsa knows their fathers contact address, he suggests that they get in touch with him, hoping he will help them. Elsa dismisses the idea and throws the address away, but when she leaves the room Hubert recovers it. The childrens secret world begins to change when Jiminee brings home Louis, a friend from his school, and allows him to hide there. Soon their teacher arrives, demanding to search the house and retrieve the missing boy, but the situation is defused by the unexpected arrival of their father, Charlie. He immediately moves in, and Hubert admits that he had secretly written to Charlie, asking him to come. The family adjusts to the new domestic situation, with Charlie taking them on outings, and even buying a new car. Most of the children (especially Diana) come to trust and love him, although Elsa remains deeply suspicious of him.

The childrens idyllic world begins to crumble after Charlie has a chance encounter on a bus with Mrs Quayle. She soon reappears at the house, demanding to know what has happened to Mrs Hook, but she is placated by Charlie. She inveigles her way back into the home, and she and Charlie soon begin a relationship. As time passes Charlie reverts to form, spending freely, drinking heavily, and entertaining loose women in the house. Learning of Jiminees ability to forge their mothers signature, Charlie convinces him to sign documents without his siblings knowledge, and he further alienates the children when he dismantles their garden shrine. Matters come to a head when an estate agent and a couple let themselves in to inspect the house. Although Diana still refuses to see the truth, Elsa and Gerty correctly deduce that Charlie intends to sell the house, and after searching his room they discover that he has squandered virtually all of their mothers savings.
 illegitimate offspring of her many adulterous liaisons, and that none of them are his own. He further reveals that he now controls the property, having used Jiminee to unwittingly forge their mothers signature and sign over the title deed for himself. When he callously declares that he despises the children, and that he intends to sell the house and put them all into care, Diana snaps and kills him with a fire iron.

The children briefly debate whether or not to bury Charlie in the garden and carry on as before, but they finally accept the gravity of their situation. As the film ends, the children leave the house for the last time and walk off into the dark to turn themselves in to the authorities.

==Cast==
*Dirk Bogarde as Charlie Hook
*Margaret Brooks as Elsa
*Pamela Franklin as Diana
*Louis Sheldon Williams as Hubert
*John Gugolka as Dunstan
*Mark Lester as Jiminee Sarah Nicholls as Gerty-
*Gustav Henry as Willy
*Parnum Wallace as Louis
*Yootha Joyce as Mrs. Quayle
*Claire Davidson as Miss Bailey Anthony Nicholls as Mr. Halbert
*Annette Carell as Mother
*Gerald Sim as Bank Clerk
*Edina Ronay as Doreen

==Production and casting==

Julian Gloags novel was brought to Claytons attention by his close friend, Canadian novelist Mordecai Richler, and according to biographer Neil Sinyard, Clayton found it "instantly fascinating". A 1966 letter from Gloag to Clayton suggests that 20th Century Fox was initially interested in producing a film adaptation and that Eleanor Perry had written a script, but it was ultimately acquired for Clayton by MGM and Filmways executive Martin Ransohoff. Clayton commissioned Jeremy Brooks (then literary manager of the Royal Shakespeare Company) to write the script, but he found Brooks adaptation too long and too close to its source, so he brought in his wife, the noted Israeli-born actress and writer Haya Harareet, who "tightened the structure and particularly changed the ending to make it more thematically consistent and psychologically consistent than in the novel". 

Richard Burton was the first choice to play Charlie, but it was thought that his fee would push the budget too high, so Bogarde was eventually cast in the role instead.  The juvenile cast was led by Pamela Franklin, who had previously worked with Clayton (as Flora, one of the possessed siblings) in The Innocents. Yootha Joyce had also worked with Clayton before, in an acclaimed cameo role in Claytons The Pumpkin Eater (1964).
 The Innocents, Something Wicked This Way Comes" (1983).

In his 1975 memoir, Dirk Bogarde recounted his experience with the production in glowing terms:

:"On my first morning in the gloomy house in Croydon I was in a bit of a funk. Eight pairs of eyes, ranging from five to fourteen, gazed at me solemnly. Not a smile, no welcoming grin even. In the little caravan in the scrubby front garden which I had been given to change in there was a jam jar stuffed with privet and some wilting Michaelmas daisies. Under it was a note. Lets hope youre as good as youre cracked up to be. Youd better be. Sincerely, The Children.

:"I loved every second of the film, which was one of the happiest I have ever made." 

Our Mothers House was also the second of five collaborations between Clayton and noted French composer Georges Delerue, who had previously written the score for Claytons The Pumpkin Eater (1964). 16 years after making Our Mothers House, Clayton and Delerue reunited for Something Wicked This Way Comes (1983), although that production was troubled, with the studio (Disney) forcing Clayton to replace Delerues original music (which was considered too dark) with a new score by James Horner. Delerue went on to compose the music for Claytons last two projects, The Lonely Passion of Judith Hearne (1987), and the TV movie Memento Mori (1992).

==Critical reception==
Reviewing the film on its U.S. release in November 1967, Roger Ebert  gave it a 3-1/2 star rating. He described the film as "one of the most suspenseful of recent years ... It isnt phony "who goes there?" suspense but suspense based on real personalities trapped in an impossible situation". Ebert also particularly praised the performances of the juvenile cast:

:"The use of children in movies is a hazardous business, if only because the little monsters walk away with the whole enterprise: plot, atmosphere, everything ... Occasionally, however, a director succeeds in beating the little prodigies into submission and wringing restrained performances from them, and when this happens, the result can be memorable. Peter Brooks did it with his castaways in "Lord of the Flies"; Lee Thompson did it with Hayley Mills in "Tiger Bay"; and now Jack Clayton has done it superbly in Our Mothers House ... (he) ... directs with firm restraint. In the April 2014 edition of the BFIs Sight & Sound, Pasquale Iannones article entitled "Age of Innocence" examines the history of children in films, and on page 36 under the sub heading "The Child Left Alone" she writes :-

Having talked about the tension, the push and pull between the worlds of adult and child, what happens when the child is left alone, when there are no rules left to break?  In Jack Claytons "Our Mothers House" (1967), a family of seven children decide not to report their mothers death from illness for fear of being sent to an orphanage.  Instead, they bury their mother in the garden of their large family home and continue life as normal.  Clayton  charts the full gamut of childhood emotion - carefree and playful one minute, unforgivingly cruel the next - in what remains one of the most sorely underappreciated portraits of the vicissitudes of childhood.

Bogarde turns in a competent performance not quite up to his best, but it really isnt his picture anyway. It belongs to the kids, and they are very real kids." 

==Awards==
* Dirk Bogarde earned one of his six BAFTA Best Actor nominations for his performance in the film, 
* The film was nominated for the Golden Lion for Best Picture at the 1967 Venice Film Festival.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 