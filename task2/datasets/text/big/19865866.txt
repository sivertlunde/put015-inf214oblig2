Lovely Angel
 
{{Infobox animanga/Header
| name            = Lovely Angel
| image           =  
| caption         = Cover of the first film, showing both the manga and live-action versions of the main protagonist.
| ja_kanji        = ラブリー·エンジェル
| ja_romaji       = laburī enjieru
| genre           = Erotic fiction|Erotic, comedy
}}
{{Infobox animanga/Print
| type            = manga
| author          = Go Nagai
| publisher       = Kobunsha
| publisher_other = ebookjapan
| demographic     = Seinen manga|Seinen
| magazine        = Weekly Hoseki
| first           = January 4, 1996
| last            = April 10, 1997
| volumes         = 5
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = Lovely Angel: Homon Soap Degozaimasu
| director        = Akira Shimizu
| producer        = Toshihiro Osato   Naoyuki Nakano
| writer          = Keishi Nagi
| music           = 
| studio          = Nikkatsu
| released        = September 26, 1997
| runtime         = 64 minutes
}}
{{Infobox animanga/Video
| type            = live film
| title           = Lovely Angel 2: Taiketsu! Homon Soap Jo vs Shuccho SM Jo!!
| director        = Akira Shimizu
| producer        = Toshihiro Osato   Naoyuki Nakano
| writer          = Keishi Nagi
| music           = 
| studio          = Nikkatsu
| released        = November 28, 1997
| runtime         = 64 minutes
}}
 
 erotic comedy manga series created by Go Nagai in 1996. The manga originally ran in the magazine Weekly Hoseki from January 4, 1996 to April 10, 1997, published by Kobunsha. {{cite web
| url         = http://www.ebookjapan.jp/shop/nagaigo/works_05.asp
| title       = 1991-2000 works list
| accessdate  = June 13, 2009
| author      = 
| date        = 
| format      = 
| work        = Nagai Go Special Corner
| publisher   = eBOOK Initiative Japan Co. Ltd.
| location    = Japan
| pages       = 
| language    = Japanese
| doi         = 
| quote       = 
}}    {{cite web
| url         = http://www.mazingerz.com/H/ANGEL.html
| title       = Lovely Angel
| accessdate  = June 13, 2009
| author      = 
| date        = 
| work        = 
| publisher   = The world of Go Nagai
| location    = Japan
| pages       = 
| language    = Japanese
| doi         = 
| quote       = 
}}  It was later compiled in five tankōbon. {{cite web
| url         = http://www.ebookjapan.jp/shop/nagaigo/books_06.asp
| title       = 1996-2000 book list
| accessdate  = June 13, 2009
| author      = 
| date        = 
| format      = 
| work        = Nagai Go Special Corner
| publisher   = eBOOK Initiative Japan Co. Ltd.
| location    = Japan
| pages       = 
| language    = Japanese
| doi         = 
| quote       = 
}}    {{cite web
| url         = http://www.mazingerz.com/H/ANGEL.html
| title       = Go Nagai tankōbon chronology
| accessdate  = June 13, 2009
| author      = 
| date        = 
| work        = 
| publisher   = The world of Go Nagai
| location    = Japan
| pages       = 
| language    = Japanese
| doi         = 
| quote       = 
}}  Two direct-to-video films were created based on the manga.

==Summary==
The manga follows the adventures of Angel, an actual angel with the mission of helping people with her special abilities. She travels with her special suitcase in search of people who are suffering a problem: youngs who cant deal with real people, men who have a real lolita complex, people that have lost their will to live, criminals that may be saved, sportsmen who have lost their touch, monks that have perverted their teachings, women who have been mistreated, etc. But those who have acted with a true evil intent may receive the visit of Black Angel, the dark counterpart of Angel who delivers divine punishment just as Angel brings true happiness.

==Manga volumes==
*Kobunsha (Kobunsha Comics)  
{| class="wikitable"
| Date || Vol. || ISBN || ISBN
|-
| October 10, 1996 || 1 ||   ||   {{cite web
| url         = http://www.amazon.co.jp/dp/4334803393
| title       = Lovely Angel #1 product description
| accessdate  = June 13, 2009
| date        = 
| publisher   = Amazon.com, Inc.
| location    = Japan
| language    = Japanese
}} 
|-
| October 10, 1996 || 2 ||   ||   {{cite web
| url         = http://www.amazon.co.jp/dp/4334803407
| title       = Lovely Angel #2 product description
| accessdate  = June 13, 2009
| date        = 
| publisher   = Amazon.com, Inc.
| location    = Japan
| language    = Japanese
}} 
|-
| February 20, 1997 || 3 ||   ||   {{cite web
| url         = http://www.amazon.co.jp/dp/4334803652
| title       = Lovely Angel #3 product description
| accessdate  = June 13, 2009
| date        = 
| publisher   = Amazon.com, Inc.
| location    = Japan
| language    = Japanese
}} 
|-
| February 20, 1997 || 4 ||   ||   {{cite web
| url         = http://www.amazon.co.jp/dp/4334803660
| title       = Lovely Angel #4 product description
| accessdate  = June 13, 2009
| date        = 
| publisher   = Amazon.com, Inc.
| location    = Japan
| language    = Japanese
}} 
|-
| August 10, 1997 || 5 ||   ||   {{cite web
| url         = http://www.amazon.co.jp/dp/4334803881
| title       = Lovely Angel #5 product description
| accessdate  = June 13, 2009
| date        = 
| publisher   = Amazon.com, Inc.
| location    = Japan
| language    = Japanese
}} 
|}

Additionally, all five volumes were also published in ebook format by ebookjapan. {{cite web
| url         = http://www.ebookjapan.jp/shop/title.asp?titleid=8890
| title       = Lovely Angel
| accessdate  = June 13, 2009
| author      = 
| date        = 
| format      = 
| work        = 
| publisher   = eBOOK Initiative Japan Co. Ltd.
| location    = Japan
| pages       = 
| language    = Japanese
| doi         = 
| quote       = 
}} 

==Live action films==
The manga has two live actions adaptations. The first one,   was released in   by Nikkatsu. {{cite web
| url         = http://www.allcinema.net/prog/show_c.php?num_c=238411
| title       = Lovely Angel: Homon Soap Degozaimasu
| accessdate  = June 13, 2009
| author      = 
| date        = 
| format      = 
| work        = 
| publisher   = allcinema
| location    = Japan
| pages       = 
| language    = Japanese
| doi         = 
| quote       = 
}}  The sequel,   was released in   by Nikkatsu as well. {{cite web
| url         = http://www.allcinema.net/prog/show_c.php?num_c=238412
| title       = Lovely Angel 2: Taiketsu! Homon Soap Jo vs Shuccho SM Jo!!
| accessdate  = June 13, 2009
| author      = 
| date        = 
| format      = 
| work        = 
| publisher   = allcinema
| location    = Japan
| pages       = 
| language    = Japanese
| doi         = 
| quote       = 
}}  Both films were directed by Akira Shimizu.

===Staff of Lovely Angel: Homon Soap Degozaimasu===
*Director: Akira Shimizu
*Producer: Toshihiro Osato, Naoyuki Nakano
*Planning: Toru Yoshida, Senji Tsubomi
*Script: Keishi Nagi
*Original work: Go Nagai
*Photography: Yoichi Ogawa
*Cast: Tomomi Kuribayashi (Angel), Yuji Sawayama (Fuzoku Writer), Moe Ishikawa, Kei Mizutani, Eisaku Shindo
Source(s)  {{cite web
| url         = http://www.mazingerz.com/ANIME/OVA.html
| title       = Original Video Ka Sakuhin List
| accessdate  = June 12, 2009
| author      = 
| date        = 
| work        = 
| publisher   = The World of Go Nagai
| location    = Japan
| pages       = 
| language    = Japanese
| doi         = 
| quote       = 
}}  {{cite web
| url         = http://www.jmdb.ne.jp/1997/ov082350.htm
| title       = Lovely Angel: Homon Soap Degozaimasu
| accessdate  = June 13, 2009
| author      = 
| date        = 
| work        = 
| publisher   = Japanese Movie Database
| location    = Japan
| pages       = 
| language    = Japanese
| doi         = 
| quote       = 
}} 

===Staff of Lovely Angel 2: Taiketsu! Homon Soap Jo vs Shuccho SM Jo!!===
*Director: Akira Shimizu
*Producer: Toshihiro Osato, Naoyuki Nakano
*Planning: Toru Yoshida, Senji Tsubomi
*Script: Keishi Nagi
*Original work: Go Nagai
*Photography: Yoichi Ogawa
*Cast: Tomomi Kuribayashi (Angel), Kei Mizutani (Black Angel), Yuji Sawayama (Fuzoku Writer), Atsushi Narasaka, Moe Ishikawa
Source(s)   {{cite web
| url         = http://www.jmdb.ne.jp/1997/ov084400.htm
| title       = Lovely Angel 2: Taiketsu! Homon Soap Jo vs Shuccho SM Jo!!
| accessdate  = June 13, 2009
| author      = 
| date        = 
| work        = 
| publisher   = Japanese Movie Database
| location    = Japan
| pages       = 
| language    = Japanese
| doi         = 
| quote       = 
}} 

==References==
 

==External links==
*    at the World of Go Nagai webpage
*    at ebookjapan
*    at d/visual
*    at Science Fiction and Fantasy Writers of Japan website
*    at allcinema
*    at the Japanese Movie Database
*    at allcinema
*    at the Japanese Movie Database

 
 
 
 
 
 
 
 
 