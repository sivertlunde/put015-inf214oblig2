One Sunday Afternoon (1948 film)
 
{{Infobox film
| name           = One Sunday Afternoon
| image          = Janis-paige-trailer.jpg
| image_size     = 
| caption        = Janis Paige in the trailer for One Sunday Afternoon
| director       = Raoul Walsh
| writer         = Robert L. Richards
| based on       =  
| narrator       = 
| starring       = Dennis Morgan Janis Paige
| music          = 
| cinematography = Wilfred M. Cline Sidney Hickox
| editing        = Christian Nyby
| studio         = Warner Brothers
| distributor    = 
| released       = January 1, 1949
| runtime        = 90 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
One Sunday Afternoon is a 1948 musical film directed by Raoul Walsh, and starring Dennis Morgan and Janis Paige.  
 1933 adaptation starred Gary Cooper. The second was The Strawberry Blonde (1941) starring James Cagney, Olivia DeHavilland and Rita Hayworth, and also directed by Walsh. While the plot of the third adaptation is the same as the others, it does have a significant number of changes.

== Cast ==
* Dennis Morgan as Timothy L. Biff Grimes 
* Janis Paige as Virginia Brush
* Don DeFore as Hugo Barnstead
* Dorothy Malone as Amy Lind (singing voice was dubbed by Marion Morgan)
* Ben Blue as Nick
* Alan Hale, Jr. as Marty

== Production ==
This film is a musical remake of the The Strawberry Blonde (1941), with some updates like an automobile for the first date instead of a horse and carriage. The tunes include "In My Merry Oldsmobile". Dennis Morgan stars in the leading role James Cagney had played in the earlier version, with Don DeFore in the role of the pseudo friend previously played by Jack Carson.

== References ==
 

== External links ==
*  

 

 
 