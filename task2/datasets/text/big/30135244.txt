Paper Marriage
 
 
{{Infobox film
| name           = Paper Marriage
| image          = 
| caption        =
| film name = {{Film name| traditional    = 過埠新娘
| simplified     = 过埠新娘
| pinyin         = Guò Bù Xīn Niáng
| jyutping       = Gwo3 Fau6 San1 Neong2}}
| director       = Alfred Cheung
| producer       = Choi Wing Cheong
| writer         = Alfred Cheung Keith Wong
| starring       = Sammo Hung Maggie Cheung Joyce Godenzi
| music          = Joseph Koo Sherman Chow
| cinematography = Jimmy Leung
| editing        = Peter Cheung Golden Harvest
| released       =  
| runtime        = 87 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = HK $13,405,571
| gross          = 
}} 1988 Cinema Hong Kong film directed by Alfred Cheung and starring Sammo Hung and  Maggie Cheung.

==Plot==
Set in Canada, a down-on-his-luck Chinese boxer named Bo Chin (Sammo Hung) accepts promise of payment to marry a Hong Kong woman named Jade Lee (Maggie Cheung) so she can get Canadian citizenship. They realize too late that they have been set up in a complicated plan to cheat them out of the womans money. Their adventures begin when Bo is forced back into the ring and Maggie tries her hand at mud-wrestling!

==Cast==
* Sammo Hung - Bo Chin
* Maggie Cheung - Jade Lee
* Alfred Cheung - Peter
* Meg Lam - Home Immigration Control officer
* Joyce Godenzi - Bos ex-wife
* Billy Chow - Thai Boxer (doubled by Chin Kar-Lok)
* Dick Wei - White Suit kicker
* Phillip Ko - Crazy Eyes
* Chin Kar-Lok - White Suits Thug
* Tony Morelli - White Suits thug
* Rainbow Ching - Immigration Officer Chin
* Hsiao Hou - White Suits thug in clown disguise
* Lee Chi Kit - Debt Colletor
* Rocky Li

==External links==
*  
*   at Hong Kong Cinemagic
*  

 
 
 
 
 
 
 
 


 
 