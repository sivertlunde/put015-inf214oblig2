The Householder
{{Infobox film
| name = The Householder
| image = The Householder, 1963 English film.jpg
| image_size =
| caption = DVD cover art James Ivory
| producer = Ismail Merchant
| screenplay = Ruth Prawer Jhabvala James Ivory based on= 
| starring = Shashi Kapoor Leela Naidu Durga Khote
| music = Ustad Ali Akbar Khan
| cinematography = Subrata Mitra
| editing = Raja Ram Khetle
| distributor = Royal Films International
| released =  
| runtime = 101 minutes
| language = English/Hindi
| country = India
| budget =
| colour = Black & white
}}
 James Ivory, James Ivory. novel of the same name by Jhabvala. 
 James Ivory, The Europeans The Bostonians A Room Howards End Peter Cameron‘s The City of Your Final Destination (2009).

==Synopsis==
Prem Sagar (Shashi Kapoor),  a teacher at a private college in Delhi, is married to Indu (Leela Naidu) in an arranged marriage recently and is still learning ropes of relationships, when the arrival of Prems mother (Durga Khote) spells doom to their budding relationship. Indu, unable to handle her interference in the marriage, leaves Prem to return to her family. Prem searches for answers from a variety of people, including a Swami (Pahari Sanyal), who reveals the secret of a successful marriage, as a result, he finally gains the maturity to love his wife. 

==Cast==
* Shashi Kapoor - Prem Sagar
* Leela Naidu– Indu
* Durga Khote  - Prems mother
* Achla Sachdev  - Mrs. Saigal
* Harindranath Chattopadhyay - Mr. Chadda
* Pro Sen  - Sohanlal
* Romesh Thapar  - Mr. Khanna (The Principal)
* Indu Lele  - Mrs. Khanna
* Pinchoo Kapoor  - Mr. Saigal
*    - Raj
*  Shama Beg - Mrs. Raj
* Patsy Dance - Kitty
* Walter King - Professor
* Ernest Castaldo - Ernest
* Pahari Sanyal - Swami

==Production==

Ivory had shot documentary, The Delhi Way was editing it in New York, when he met anthropologist Gitel Steed, who was developing a project based on her screenplay, Devgar about a village in Gujarat. Ismail Merchant was producing the film and had started getting together the finances for the film. Sidney Meyers was the director, while Ivory agreed to shoot the film, whose cast included Shashi Kapoor, Durga Khote and Leela Naidu. When the film fell through due to lack of complete financing, Merchant suggested the idea of The Householder, and same cast was used. The film cost $125,000, with some of the money Ivory had borrowed from his father. It was made in two versions, Hindi and English, the latter was picked by Columbia Pictures.    

Shooting for the film started in 1961 and was completed in 1963.    The film was shot entirely on location in   exerted an important influence both on Ivory and Merchant, as well as on this film. In an uncredited assist, he supervised the films music production and re-cut the film for Merchant and Ivory. He also lent his cameraman, Subrata Mitra, as the director of photography, and as a result the film is infused with the fluid, restrained lyricism that characterizes Rays work.  

==Crew==
* Music: Ustad Ali Akbar Khan
* Incidental Music
** Jyotirendera Moitra
** Vanraj Bhatia
* Costume Design - Bettina Gill
* Production - Bhanu Ghosh
* Hindi dialogue - R.G. Anand

==Reception==
A Channel 4 review called it, “a low-key but rewarding character piece”, “an artful social satire and also a quietly affecting love story”,  while The New York Times was rather dismissive. 

Mike Clark, of USA Today, called it "...A charming comedy of marital discord...", gave it, 3  out of 4 stars. 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 