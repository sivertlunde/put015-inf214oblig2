Common Ground (1916 film)
{{infobox film
| name           = Common Ground
| image          = Common Ground 1917 newspaperad.jpg
| imagesize      =
| caption        = Newspaper advertisement.
| director       = William C. deMille
| producer       = Jesse Lasky
| writer         = Marion Fairfax (story, scenario)
| starring       = Marie Doro Thomas Meighan
| music          =
| cinematography = Charles Rosher
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = United States
| language       = Silent film (English intertitles)
}}
Common Ground is a 1916 silent film drama produced by Jesse Lasky, directed by William C. deMille and distributed by Paramount Pictures. It is an original story for the screen and stars Thomas Meighan and Marie Doro. 

Print held by British Film Institute. 

==Cast==
*Marie Doro - The Kid
*Thomas Meighan - Judge David Evans
*Theodore Roberts - James Mordant
*Mary Mersch - Doris Mordant
*Horace B. Carpenter - Burke
*Florence Smythe - Mrs. Dupont
*Mrs. Lewis McCord - Housekeeper
*Dr. Keller - Jones

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 


 