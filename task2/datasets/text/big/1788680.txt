Payday (1972 film)
{{Infobox film
| name           = Payday
| image          = File:Film_Poster_for_Payday.jpg
| caption        = Theatrical release poster
| director       = Daryl Duke
| producer       = Don Carpenter Martin Fink
| writer         = Don Carpenter
| starring       = Rip Torn Ahna Capri Michael C. Gwynne
| music          = Ed Bogas Tommy McKinney Shel Silverstein Ian Tyson Sylvia Tyson
| cinematography = Richard C. Glouner
| editing        = Richard Halsey
| studio         = Fantasy Films Fantasy Records Pumice Finance Company
| distributor    = Cinerama Releasing Corporation (1972, original) Warner Bros. (2008, DVD)
| runtime        = 123 min.
| released       =  
| country        = United States English
| budget         = $760,000
| gross          =
}}

Payday is a film released in 1973 written by Don Carpenter and directed by Daryl Duke.  It stars Rip Torn as a country music singer.  Other members of the cast include Ahna Capri, Elayne Heilveil, and Michael C. Gwynne.  It was filmed in and around Selma, Alabama|Selma, Alabama.

==Plot==
Maury Dann (Rip Torn) is a successful country-western singer who travels around the Southern states in a Cadillac and gets himself into all sorts of adventures.

The film opens with Dann performing in a small club with his band, entourage, and nagging girlfriend, Mayleen, in tow. He meets a young girl named Sandy backstage and seduces her in the back of his car while her boyfriend and boss, Mr. Bridgeway, is looking for her.

The band returns to a nearby motel, bringing along Rosamond, a young lady from the show. The next day, Maury visits his invalid mother and, along with a couple of the guys from the band, goes on a hunting trip. He gets into a fistfight with Bob Tally over Maurys dog Snapper, who is not being properly taken care of by Maurys sick mother. After the fight, Maury reluctantly gives Snapper away to Bob, but fires him from the band before returning to the motel.

During the trip, Maury seduces Rosamond in the back of the Cadillac, much to Mayleens dismay. Later, while in the ladies room, Mayleen warns Rosamond to stay away from Maury. The band stops by a local radio station to help promote Maurys new album, Payday, and to bribe a DJ with a gift of Wild Turkey liquor to keep playing more of Maurys records on-air.

During a heated exchange, Maury kicks Mayleen out of the car and leaves her stranded by the side of the road. Maury take a detour and goes to visit his ex-wife, Galen, in order to celebrate his sons birthday. They get into an argument as well and Maury leaves without seeing his son.

At a restaurant, Maury and company are confronted by a drunken Bridgeway, who claims that Maury had raped Sandy the day before. Maury and Bridgeway step outside to discuss the matter. Bridgeway pulls out a knife and tries to attack Maury, who quickly turns the knife around and stabs Bridgeway to death. In order to avoid a scandal and missing tour dates, Maurys tour manager McGinty arranges for his chauffeur, Chicago, to take the blame for Bridgeways death.

With Chicago gone, Maury hires a young fan named Ted, an aspiring country singer, to be his new driver. Rosamond, feeling distraught over Bridgeways death, tells Ted that she wants to go home but doesnt have any money to go back. The local police and district attorney come to Maurys motel to bring him in for questioning over Bridgeways death. He makes a dash for the Cadillac. With Ted in the back seat and Maury driving the car, Maury suffers a heart attack and dies behind the wheel.

The film ends with Ted, who survives the crash but is badly injured, running out of a wooded area looking for help.

==Cast==
* Rip Torn as Maury Dann
* Ahna Capri as Mayleen Travis
* Elayne Heilveil as Rosamond McClintock
* Michael C. Gwynne as Clarence McGinty Jeff Morris as Bob Tally
* Cliff Emmich as Chicago
* Henry O. Arnold as Ted Blankenship
* Bobby Smith as Lyman Pitt
* Dallas Smith as Henry Tutweiler
* Richard Hoffman as Foggy Bottom Yonce
* Walter Bamberg as Bridgeway
* Eleanor Fell as Galen Dann
* Clara Dunn as Mama Dann
* Linda Spatz as Sandy
* Earle Trigg as Bob Dickey
* Winton McNair as Officer Ratliff
* Mike Edwards as Restaurant Manager
* Gene Cody as Motel Manager
* Frazier Moss as Jesse Weatherwax
* Bill Littleton as Process Server
* Phillip Wende as Billy Lee Blackstone
* Ed Neeley as Abe
* Sonny Shroyer as Dabney
* Cliff Hillerby as Club Manager

==Reception==
The film was named one of The Best 1000 Movies Ever Made by The New York Times. 

==DVD==
Payday was released by Warner Bros. on DVD January 8, 2008.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 