Don't Bet on Blondes
{{Infobox film
| name           = Dont Bet on Blondes
| image          = Dont Bet on Blondes.jpg
| caption        =
| director       = Robert Florey
| producer       = Samuel Bischoff
| writer         =  Isabel Dawn Boyce De Gaw
| starring       = Warren William Guy Kibee 
| music          = William Rees Thomas Richards
| distributor    = Warner Brothers
| released       = 13 July 1935 
| runtime        = 59 mins.
| country        = United States
| language       = English 
| budget         =
}}

Dont Bet on Blondes is a 1935 American romantic comedy film.
 Captain Blood later that year.

==Plot summary==
Small-time racketeer "Odds" Owen and his associates go into the semi-legitimate insurance business, but his first big client is a southern-colonel neer-do-well who wants to insure his daughter against getting married. Owens muscle successfully chases off her various suitors, thereby avoiding the risk of having to pay out against the policy, but Owen himself falls in love with her, placing him in a dilemma, caught between the heart and the wallet.

==Cast==
* Warren William as Oscar Owen  
* Claire Dodd as Marilyn Youngblood  
* Guy Kibbee as Colonel Youngblood  
* William Gargan as Numbers  
* Vince Barnett as Chuck aka Brains  
* Hobart Cavanaugh as Philbert O. Slemp  
* Clay Clement as T. Everett Markham  
* Errol Flynn as David Van Dusen  
* Spencer Charters as Doc   Walter Byron as Dwight Boardman  
* Eddie Shubert as Steve  
* Jack Norton as J. Mortimer Mousy Slade  
* Mary Treen as Owens secretary  
* Maude Eburne as Little Ellen Purdy  
* Herman Bing as Professor Friedrich Wilhelm Gruber

==Production==
The film was originally known as Not on Your Life and was always intended as a vehicle for Warren William.   Dolores del Río was originally announced as female co star.  She was eventually replaced by Claire Dodd.  Walter Byron replaced George Meeker.  Filming completed 13 May 1935. 

Errol Flynn made the movie shortly after his marriage to Lili Damita. 

==Reception==
The Los Angeles Times called it a "sparkling comedy" in which Warren William "again proves himself a delightful and suave comedian." 

==References==
 

== External links ==

*   
 

 
 
 
 
 