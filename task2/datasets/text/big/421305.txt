The Blue Lamp
 
 
{{Infobox film
 | name = The Blue Lamp
 | image = The_Blue_Lamp_UK_quad_poster.jpg
 | caption = UK original quad format film poster
 | director = Basil Dearden
 | producer = Michael Balcon
 | screenplay = T.E.B. Clarke Jack Warner Jimmy Hanley Dirk Bogarde Robert Flemyng
 | music = Ernest Irving Jack Parnell  
 | cinematography = Gordon Dines
 | editing    = Peter Tanner
 | studio  = Ealing Studios
 | distributor = General Film Distributors
 | budget =
 | released =  
 | runtime = 85 minutes 
 | country = United Kingdom
 | language = English
 | }}
 police drama, Jack Warner as veteran PC Dixon, Jimmy Hanley as newcomer PC Mitchell, and a very young Dirk Bogarde as hardened criminal Tom Riley. The title refers to the blue lamps that traditionally hung outside British police stations (and often still do). The film was to be the inspiration for the 1955–1976 TV series Dixon of Dock Green, where Jack Warner continued to play PC Dixon until he was 80 years old (even though Dixons murder is the central plot of the original film).

The screenplay was written by ex-policeman T. E. B. Clarke. The film is an early example of the "social realism" films that emerged later in the 1950s and 1960s, but it follows a simple moral structure in which the police are the honest guardians of a decent society, battling the disorganised crime of a few unruly youths.

==Plot== PC George Ealing "ordinary" White City greyhound track in West London. To Andy Mitchell falls the honour of arresting Riley.

==Main cast==
  Jack Warner as PC George Dixon
* Jimmy Hanley as PC Andy Mitchell
* Dirk Bogarde as Tom Riley
* Robert Flemyng as Sgt. Roberts
* Bernard Lee as Insp. Cherry
* Peggy Evans as Diana Lewis
* Patric Doonan as Spud
* Bruce Seton as PC Campbell Meredith Edwards as PC Hughes
* Clive Morton as Sgt. Brooks
* William Mervyn as Chief Inspector Hammond
* Frederick Piper as Alf Lewis
* Dora Bryan as Maisie
* Gladys Henson as Mrs. Dixon
* Tessie OShea as herself
 

==Production== Westbourne Park. George Dixon is named after producer Michael Balcons former school in Birmingham.

==Locations used==
 
The original blue lamp was transferred to the new Paddington Green Police Station. It is still outside the front of the station and was restored in the early 21st century. Most of the locations around the police station are unrecognisable now due to building of the Marylebone flyover. The police station at 325 Harrow Road, not far from the site of the Coliseum Cinema (324–326 Harrow Road), which is also shown in the film, has a reproduction blue lamp at its entrance.
 flyover would need the site, although that turned out not to be the case. It is now the site of Paddington Green Police Station. The scene involving a robbery on a jewellers shop was filmed at the nearby branch of national chain, F. Hinds (then at 290 Edgware Road). This was also knocked down when the flyover was built.

The scenes of the cinema robbery were filmed at the Coliseum Cinema on Harrow Road, next to the Grand Union Canal bridge. The cinema was probably built in 1922, was closed in 1956 and later demolished.  The site is now occupied by an office of Paddington Churches Housing Association.

Some of the streets used, or seen, in the film include:  .  Most of the chase is a logical following of Rileys car apart from when the car goes from Hythe Road NW10 into Sterne Street – Hythe Road in 1949 was a dead end.

==Reception==
The Blue Lamp premiered on 20 January 1950 at the   found the depiction of the police work very plausible and realistic, and praised the performances of Dirk Bogarde and Peggy Evans, but found Jack Warners and Jimmy Hanleys two policemen portrayed in a too traditional way: "There is an indefinable feel of the theatrical backcloth behind their words and actions&nbsp;... The sense that the policemen they are acting are not policemen as they really are, but policemen as an indulgent tradition has chosen to think they are, will not be banished." 

In the context of a campaign against perceived "middle-class complacency" in British film-making, Sight & Sound editor Gavin Lambert (writing under a pseudonym) attacked the films "specious brand of mediocrity" and suggested the film was "boring and parochial", views which caused outrage  and were not reflected by the record public patronage.

The film had the highest audiences in Britain for a British film that year. 

===Awards=== Venice Film Festival. 

==Legacy==
Several of the characters and actors were carried over into the TV series Dixon of Dock Green, including the resurrected Dixon, still played by Warner. The series ran on BBC One for twenty-one years from 1955 to 1976, with Warner being over eighty by the time of its conclusion.
 Arthur Elliss satirical BBC transported forwards in time into an episode of The Filth, a gritty contemporary police television series, replacing their modern day counterparts. 
 Kevin ONeill has one panel suggesting George Dixon died in August 1898, the time-period given for the first two graphic novels, as well as The War of the Worlds. However, no explanation of this claim is given.
 Ashes to Ashes concluded with a short clip of George Dixon, referring to the similarity to Dixons death in The Blue Lamp and subsequent resurrection for the television series and the underlying plot of the show. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 