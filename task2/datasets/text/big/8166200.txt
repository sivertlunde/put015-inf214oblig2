Sing a Song of Six Pants
{{Infobox Film
| name           = Sing a Song of Six Pants
| image          = SingSong6PantsOneSheet47.JPG
| director       = Jules White
| producer       = Jules White Felix Adler| Harold Brauer Virginia Hunter Joe Palma Cy Schindell
| cinematography = Henry Freulich
| editing        = Edwin H. Bryant
| distributor    = Columbia Pictures
| released       =  
| runtime        = 16 54"
| country        = United States
| language       = English
}}

Sing a Song of Six Pants is the 102nd short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

== Plot == Harold Brauer), they think that catching him might end their financial woes. Hargan conveniently ducks into their shop as the officer enters and leaves a suit with a safe combination in its pocket. After his girlfriend (Virginia Hunter) fails to retrieve the combination, Hargan returns with his henchmen, and a wild mêlée follows. The Stooges miss out on the reward but wind up with the crooks bankroll to pay off their debts.

==Production notes==
 ) in Sing a Song of Six Pants]]
The title is a takeoff on "Sing a Song of Sixpence," the classic English nursery rhyme. The name of the tailor shop is "Pip Boys," a parody of the auto service chain Pep Boys originally opened in Philadelphia in 1921. Solomon, Jon. (2002) The Complete Three Stooges: The Official Filmography and Three Stooges Companion, p. 302-303; Comedy III Productions, Inc., ISBN 0-9711868-0-4 

Sing a Song of Six Pants was remade in 1953 as Rip, Sew and Stitch, using ample recycled footage from the original.  It is one of four Stooge shorts that fell into the public domain after the copyright lapsed in the 1960s (the other three being Malice in the Palace, Brideless Groom, and Disorder in the Court). As such, these four shorts frequently appear on inexpensive VHS or DVD compilations.

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 