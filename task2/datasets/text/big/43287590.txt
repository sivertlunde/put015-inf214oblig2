Scampolo (1932 film)
{{Infobox film
| name = Scampolo 
| image =
| image_size =
| caption =
| director = Hans Steinhoff
| producer = Lothar Stark
| writer =  Dario Niccodemi (play)   Max Kolpé   Felix Salten   Billy Wilder
| narrator =
| starring = Dolly Haas   Karl Ludwig Diehl   Oskar Sima   Paul Hörbiger
| music = Artur Guttmann   Franz Waxman 
| cinematography = Hans Androschin   Curt Courant 
| editing =  Ella Ensink
| studio = Lothar Stark-Film  
| distributor = Bayerische Film
| released =   26 October 1932
| runtime = 86 minutes
| country = Germany German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Scampolo is a 1932 German comedy film directed by Hans Steinhoff and starring Dolly Haas, Karl Ludwig Diehl and Oskar Sima.  The film is an adaptation of the Italian play Scampolo (play)|Scampolo by Dario Niccodemi, which has been turned into numerous films.

==Cast==
* Dolly Haas as Scampolo 
* Karl Ludwig Diehl as Maximilian 
* Oskar Sima as Philippe 
* Paul Hörbiger as Gabriel  
* Hedwig Bleibtreu as Frau Schmid

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 
 
 
 
 
 
 
 


 