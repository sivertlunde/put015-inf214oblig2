Arirang 3
{{Infobox film name            = Arirang 3 image           = Arirang 3 1936.jpg caption         = Arirang 3
| film name      = {{Film name hangul          =   3  hanja           = 아리랑 3  rr              = Arirang sampyeon mr              = Arirang samp‘yŏn}} director        = Na Woon-gyu producer        = Cha Sang-eun writer          = Na Woon-gyu starring        = Na Woon-gyu Hyeon Bang-ran Jun Taek-yi Yun Bong-choon music           = cinematography  = Lee Shin-woong editing         = Yang Joo-nam distributor     = Han Yang Film Co. released        =   runtime         =  language        = Korean budget          = 15,000 won
}}
Arirang 3 (Hangul|아리랑 3편, Arirang sam-pyeon) is a 1936 Korean film directed by and starring Na Woon-gyu. The second sequel of Nas ground-breaking 1926 film, Arirang (1926 film)|Arirang, this was the only entry in the series that was not silent. It premiered at the DanSungSa Theater in downtown Seoul.

==Plot==
This third and last installment in the story of the mentally ill student, Choi Yeong-jin, begins with him being released from prison. He attempts to live a peaceful existence until he witnesses the rape of his sister, at which point his mental problems return.

==See also==
* Korea under Japanese rule
* List of Korean language films
* Cinema of Korea
* List of Korea-related topics

== References ==
*  
*    

==External links==
*  

 
 
 
 


 