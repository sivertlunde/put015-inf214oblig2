Vedam (film)
 
 

{{Infobox film
| name           = Vedam
| image          = Vedam_Official_Film_Poster.jpg
| alt            =  
| caption        = Official film poster Krish
| Yarlagadda Shobhu
| writer         = Krish
| starring       = Allu Arjun Manoj Manchu Anushka Shetty Deeksha Seth Manoj Bajpai Saranya Ponvannan Lekha Washington Nagayya
| music          = M. M. Keeravani
| cinematography = Gnanasekhar V. S.
| editing        = Shravan
| studio         = Arka Media Works
| distributor    = Sri Venkateswara Creations
| released       =  
| runtime        = 135 minutes
| country        = India
| language       = Telugu
| budget         =  (production cost) http://www.vedamthefilm.com 
| gross          =   
}} Filmfare Awards Tamil as Vaanam (2011), with Anushka and Saranya reprising their roles. It was also dubbed in Hindi as "Antim Faisla".

==Plot==
Vedam revolves around 5 principal characters.

Vivek Chakravarthy (Manoj Manchu) is an upcoming rock star in Bengaluru who hails from a family of decorated army officers. His mother wants him to follow the same path but he isnt interested. He is about to give his first live concert on New Years Eve with his band-mates in Hyderabad. However, Vivek is late to catch the flight and so the band is forced to travel by road. After sometime, one of their tyres get deflated. Not having a spare, the other friends go to get the tyre inflated leaving Vivek and Lasya (Lekha Washington) alone. Meanwhile, they are attacked by a Hindu racist group. However, they manage to get out of the mess with the help of a Sikh truck driver (who incidentally gets involved in an earlier altercation with Vivek). The band successfully reaches Hyderabad. However they suffer a minor accident there which involves a mother in Childbirth|labour. So Vivek and Lasya take her to a hospital nearby.

Saroja (Anushka Shetty|Anushka) is a prostitute in Amalapuram working for Rattamma. She has a best friend Karpooram who is a eunuch. Saroja doesnt like working for Rattamma and plans to run away from there. Her dreams come true when she receives a call from Hyderabad from one of her agents. The very next day, which happens to be the New Years Eve, she leaves for Hyderabad with Karpooram, without Rattammas knowledge; but not before putting the local sub-inspector Bullabbai to sleep using pills. However, Saroja is arrested in Hyderabad by constables for betraying Bullabbai. Karpooram frees her from the inspector and they are both taken by their agent to a client. The client turns out to be yet another broker, who bought Saroja and Karpooram from Rattamma. Both successfully escape from the broker after a scuffle in which Karpooram gets stabbed. So, Saroja takes her to the same hospital that Vivek and Lasya take the pregnant lady to.

Ramulu (Nagayya) is a debt-ridden weaver from Sircilla. He lives with his widowed daughter-in-law, Padma (Saranya Ponvannan) and grandson. Ramulu is desperate to get his grandson educated, being illiterate himself. However he owes money to the local landlord, who takes his grandson away saying that he would be sent to the construction business in the city for 10 years to recover the money. He gives Ramulu & Padma 3 days to clear the debt. Not knowing what to do, Padma agrees to sell her kidney, which is illegal (Ramulu had previously sold his kidney for the same purpose). They come to Hyderabad and her kidney is sold successfully in the hospital where the previous four character are. The original kidney is sold for  1,00,000 of which Ramulu gets only  37,000.

Raheemuddin Qureshi (Manoj Bajpai) is a Muslim hailing from Old Basti in Hyderabad. In the past, his wife suffered a miscarriage due to a scuffle between him and a few Ganesh devotees during a religious procession. Unable to bear the loss, he decides to leave the country. However during his farewell party, a police officer (who previously insulted Qureshi during the scuffle) comes to his house and arrests his nephews, alleging that they have links with terrorists. When Qureshi obstructs him, he arrests him too. During interrogation, one of his nephews accepts his terrorist background and says that something tragic would happen on New Years Eve. Qureshi is shocked. When the police transports him to remand, Qureshi panics and tries to escape. However, his escape bid is unsuccessful and he gets shot in the leg. He is then taken to the same hospital as the rest of the characters are in and kept in the same ward as another terrorist suspect.

Cable Raju (Allu Arjun) is a lower middle class guy hailing from Jubilee Hills slum living with his maternal grandmother. He loves Pooja (Deeksha Seth), who is from the upper class of the society. Pooja, however, doesnt know of Rajus financial background and thinks of him to be rich. Raju has to get 2 passes for a lavish New Years Eve party which cost almost  40,000. He tries to obtain that money by various means, but is unsuccessful. He even tries to snatch chains from womens necks, but is apprehended by the police when his motorbike hits a police van. He is taken to the police station where he meets Saroja. However, Dappu Subani (who initiated him into the chain snatching business) comes to his rescue and gets him freed. His grandmother, vexed at his behaviour, gives him 3,000 rupees and asks him to do whatever he wants. In a local cafeteria, he happens to see Ramulu with his money bundle and decides to steal it and thus follows him to the hospital. He successfully manages to steal the bundle from Ramulu and Padma. However, as he is about to pay for the passes, he realizes his mistake, adds his own 3,000 rupees to the bundle and returns it to Ramulu at the hospital.

Meanwhile, a group of terrorists come to the ward housing Qureshi and free their leader. They leave Qureshi unharmed on account of him being a Muslim and start the terrorist attack. Raju and Vivek try to save as many people as possible. They even kill two terrorists in the process. Qureshi saves the life of the policeman who arrested him. In the final fight sequence, both Raju and Vivek valiantly give up their lives, trying to protect the people in the hospital.

The conclusion of the movie shows Saroja deciding to change her profession; Vivek being recognized as a national hero; Raju was dead being respected by all in his community, with Pooja coming to see him along with her mother; Qureshi being freed by the thankful policeman and Ramulu successfully repaying the money and getting his grandson back.

The movie stands on an old Hindi saying, a true man is the one who dies for another.

==Cast==
* Allu Arjun as Cable Raju
* Manoj Manchu as Vivek Chakravarthy
* Anushka Shetty as Saroja
* Manoj Bajpai as Rahimuddin Qureshi
* Saranya Ponvannan as Padma
* Nagayya as Ramulu
* Deeksha Seth as Pooja
* Lekha Washington as Lasya
* Siya Gautham as Zara
* Brahmanandam
* Posani Krishna Murali
* Raghu Babu as Dappu Subani
* Rakesh Varre  
* Ravi Prakash as ACP Shiva Ram
* Satyam Rajesh as Ganapati Prudhviraj as Bullebbayi
* Kallu Chidambaram
* Nikki as Karpooram

==Production==
Director Krish had a visual of an old man walking along with a child in his dreams. However, he felt it as too arty to make a film and also the story was not complete. One day while travelling from Guntur to Hyderabad, India|Hyderabad, he narrated the story of Nagayya (initially he named it as Jalayya) to cinematographer Gnana Sekhar, who also worked with him in Gamyam. The second story initially he conceptualised was about a film crew and later changed it to music band. The title of Vedam struck him when he was penning the story at Sirivennelas home. Krish wanted to do the character of Karpooram, but decided against it after his mother warned him. Later, when discussing the story with Anushka Shetty, she suggested her personal make-up person Nikki for the role and Krish liked him. Anushka reduced her remuneration by 30% as the film was a being a low budget producction. Rahimuddin Qureshi (character played by Manoj Bajpai) is the name of Krishs best friend in intermediate. Nagayya was spotted by Krish. The climax of the film was an inspiration from 2008 Mumbai attacks|26/11 Mumbai blasts in Taj Hotel. After getting a basic story line he decided to produce Vedam on his banner First Frame entertainment, before producers Shobu Yarlagadda and Prasad Devineni decided to finance it. The five principal characters were based on the five natural elements-Water, Air, Earth, Fire and Sky. It notably became the first film Allu Arjun and Manoj Manchu starred in that did not involve their respective fathers Allu Aravind and Mohan Babu. 

==Promotion== CBFC for video release.

==Critical reception==
Vedam opened to utmost positive reviews. The Times of India gave a review stating "After watching contrived plots and stereotype protagonists, this path-breaking movie arrives as a breath of fresh air. Undeniably, its another inspiring film from the maker of Gamyam."  Rediff gave a review stating "Vedam is a must-watch for its story, screenplay, direction and technicalities. It offers a hope to the audience interested in good and sensible cinema that such a film can be made in Telugu. Vedam is a beautiful confluence of writing and technical aspects. It has set a benchmark in Telugu cinema."  idlebrain.com gave a review stating "Vedam is a good film. Please watch it and encourage good/meaningful cinema. This film will be liked by classes and A center audiences. If masses and B/C centers patronages Vedam, it will not only do good to the film, but to the entire Telugu film industry."  Esskay of 123telugu.com gave a review stating "If films like Vedam arent encouraged, we will be left with films that will be made with only money and without a soul! Watch Vedam and ask your friends to watch it, for the sake of good cinema in Telugu. The noise of clapping by the audience after the film finished is the loudest Ive heard. I plead and rest my case!" 

==Box office==
Vedam collected  (gross) until the end of its run 
Vedam Pre-release revenues went up to  , making it a Table Profit film even before its release. Allu Arjun took an acting fee of   as remuneration apart from acquiring distribution rights with a few important areas across the state.

==Awards==
;Nandi Awards
*Nandi Award for Best Feature Film – 2010.
 2011 Filmfare Awards South Best Film – Sobhu Yarlagadda & Devineni Prasad Best Director – Radhakrishna Jagarlamudi Best Actor – Allu Arjun Best Actress – Anushka Shetty

;Nominations Best Music Director -  M. M. Keeravani Best Female Sunitha - "Egiripothe" Best Lyricist - Sirivennela Sitaramasastri - "Now Or Never"
; 2011 CineMAA Awards Best Actress (Jury) – Anushka Shetty

==Music==
{{Infobox album  
| Name        = Vedam
| Type        = Soundtrack
| Released    =  
| Artist      = Various Artists
| Cover       = Vedam_Soundtrack_album_cover.jpg
| Caption     = Album Cover Telugu 
| Label       = Vel Records
| Producer    =
| Length      =
| Lyrics      = Sirivennela Seetarama Sastry
| Last album  =
| This album  =
| Next album  =
}}

The films music was composed by M. M. Keeravani while the lyrics were written by Sirivennela Seetarama Sastry, Sahiti, E. S. Murthy and M. M. Keeravani    The audio was released in the presence of notable Telugu film personalities  on 3 May 2010.
 Sunitha & M. M. Keeravani, became a major hit in Hyderabad and the state of Andhra Pradesh in the months following the release of the audio and film. Tracks 9 to 12 of the soundtrack were only made available on the official Digital release  and not the physical CD.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track # !! Song !! Artist(s)
|-
| 1 || "Now Or Never" || Benny Dayal, Geeta Madhuri & Deepu
|- Sunitha & M. M. Keeravani
|-
| 3 || "Rupai" || M. M. Keeravani
|-
| 4 || "Prapancham Naaventa Vasthunte" || Anuj Gurwara, Achu & Chaitra
|-
| 5 || "Malli Puttani"|| M. M. Keeravani
|-
| 6 || "Emi Tellisi Nannu Mohisthivi" || Padma, Poornima, Manju & Sudha Krishnan
|- Karthik
|-
| 8 || "Ee Chikati Cherani" || M. M. Keeravani
|-
| 9 || "Alalai Kammani Kalali"|| ES Murthy
|-
| 10 || "Nuvumundhani Nemundhani"|| K Kaala Bhairava
|-
| 11 || "Caterpillar"|| M. M. Keeravani
|-
| 12 || "Oneness" || M. M. Keeravani
|}

==Trivia==
In 2011, it was known that Allu Arjun and Anushka Shetty had been chosen as the lead pair for Selvaraghavans project. But it did not happen.

==References==
 

==External links==
*  

 
 
{{succession box
| title=Filmfare Best Film Award (Telugu)
| years=2010
| before=Magadheera
| after=Dookudu}}
 

 

 
 
 
 
 
 
 