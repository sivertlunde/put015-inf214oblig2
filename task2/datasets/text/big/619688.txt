Thriller – A Cruel Picture
{{Infobox film
| name = Thriller &ndash; A Cruel Picture
| image = Theycallheroneeye.jpg
| image_size = 215px
| alt =
| caption = Theatrical release poster
| director = Bo Arne Vibenius (as Alex Fridolinski)
| producer = Bo Arne Vibenius
| writer = Bo Arne Vibenius Heinz Hopf
| music = Ralph Lundsten
| cinematography = Andrew Bellis
| editing = Brian Wikström
| distributor = BAV Film   American International Pictures  
| released = May 1973    June 5, 1974   October 30, 1974  
| runtime = 107 minutes 82 minutes   104 minutes  
| country = Sweden
| language = Swedish
| budget =
}} Swedish exploitation rape and mute young woman who is being forced into heroin addiction, for which she has to work as a prostitute, and her revenge on the men responsible.

== Plot == Heinz Hopf), who makes her a heroin addict, and then becomes her pimp. To hide the fact that she was kidnapped, the Pimp writes hateful letters to the parents who become so distraught they commit suicide. At one point, she is stabbed in the eye for refusing a client. Madeleine then begins saving money to purchase a car to escape and take lessons in driving, shooting, and martial arts in order to take revenge.

== Cast ==
* Christina Lindberg as Madeleine / Frigga Heinz Hopf as Tony
* Despina Tomazani as the lesbian girl
* Per-Axel Arosenius as Madeleines Father
* Solveig Andersson as Sally
* Björn Kristiansson as The Addict
* Marie-Louise Mannervall as Woman in Village
* Hildur Lindberg as Woman in Village
* Marshall McDonagh as Karate Teacher
* Pamela Pethö-Galantai as Madeleine as a child
* Hans-Eric Stenborg as Sex Buyer
* Stig Ström as Sex Buyer
* Gunnel Wadner as Madeleines Mother
* Bo Arne Vibenius as Food Vendor

== Production ==
Director   sequences were edited into the film to profit off of the trend of pornography in Denmark and Sweden, which was being liberalized at the time. 
 saline solution during the drug scenes. Daniel Ekeroth: SWEDISH SENSATIONSFILMS: A Clandestine History of Sex, Thrillers, and Kicker Cinema,  (Bazillion Points, 2011) ISBN 978-0-9796163-6-5. 

== Release ==
 
The original running time was 107 minutes. After being banned by the Swedish film censorship board, it was cut down to 104 minutes and then 86 minutes, but still banned. It was finally released after being cut down to 82 minutes. In the United States it had also been cut to 82 minutes.

=== Home media ===
In 2004 and 2005, Synapse Films released two versions of Thriller on DVD.

Thriller: They Call Her One-Eye (Vengeance Edition): This edition, released on August 30, 2005, contains all of the action scenes that were cut from the original theatrical cut. This version is more accessible; however, it contains scanty bonus material (a theatrical trailer) and an exclusive essay in the liner notes. Widescreen, spoken languages Swedish and English language|English, with available English subtitles. Running time is 104 minutes.

Thriller &ndash; A Cruel Picture &ndash; (Limited Edition): This edition of Thriller, released on September 28, 2004, contains the film in its entirety, as well as all of the violence that was cut from the original version. Bonus Features include two trailers; THRILLER: A CRUEL LAB MISTAKE, which documents a lab accident that cost the director two days worth of footage; a photo gallery of Christina Lindberg posing nude during the shooting of the movie; and a look at the entire film in 40 seconds. Widescreen, Spoken languages English and Swedish, with available English subtitles. Running time is 107 minutes.

=== Controversy ===
Vibenius directed the film under the pseudonym Alex Fridolinski. In the marketing materials the two names share credit for story. In the scene where Lindbergs character is stabbed in the eye the director used an actual cadaver, to much controversy.  
 legal documentation to show that they had acquired the assets to the company that held Thriller as one of its properties, and thus owned the film for the remainder of the original contracted period.

The hardcore sex scenes were of a couple who went by the names Romeo and Juliette. Theyd travel in a van performing live sex acts. 

== Legacy == The Gardener from 1912.  It has received a cult following and was one of the inspirations behind Quentin Tarantinos Kill Bill, specifically for the character of Elle Driver (Daryl Hannah). 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 