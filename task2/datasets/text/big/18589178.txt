Prince Valiant (1954 film)
{{Infobox film
| name           = Prince Valiant
| image_size     =
| image	         = Prince Valiant FilmPoster.jpeg
| caption        =
| director       = Henry Hathaway
| producer       = Robert L. Jacks
| writer         = Hal Foster (comic strip) Dudley Nichols
| starring       = James Mason Janet Leigh Robert Wagner
| narrator       = Michael Rennie
| music          = Franz Waxman
| cinematography = Lucien Ballard Robert L. Simpson
| distributor    = Twentieth Century Fox
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $2,970,000 
| gross          = $2.6 million (US rentals) 
}}
 comic strip of the same name by Hal Foster. A young man seeks to join the Knights of the Round Table in order to restore his father to his own kingship, and uncovers a plot against King Arthur.

==Plot summary== Norse god pantheon. Aguar has since come under the protection of King Arthur (Brian Aherne), and once Valiant has grown to a man, he is sent to Camelot to undergo training as a knight under the tutelage of Aguars family friend, the noble knight of the Round Table, Sir Gawain (Sterling Hayden).

During his wanderings, Valiant witnesses a clandestine meeting between a group of Sligons Vikings and a black-clad knight. He is discovered, but with slyness and improvisation manages to elude his pursuers. During his flight, Valiant runs into Gawain, and after becoming convinced that Valiant is indeed the son of Aguar and hearing the princes story of the mysterious Black Knight, who is not unknown to the knights of Camelot, Gawain takes Valiant to his king. Valiant is accepted by the king as a prospective knight, but first he has, like any other knight apparent, to undergo the rigors of Squire#Knight in training|squirehood. One of the royal knights, Sir Brack (James Mason), takes an extraordinary interest in Valiant and offers to train him, but Valiant is instead assigned to Gawain.
 Barry Jones) and is taken in by his daughters, Aleta (Janet Leigh) and Ilene (Debra Paget). Upon recovery, Aleta and Valiant fall in love, but King Luke disapproves of Valiants Viking origin and so their relationship must remain a secret for the time being. From Aleta, Valiant also learns that her younger sister Ilene has a crush on Sir Gawain.

Valiant returns to Camelot and discovers to his shock that Gawain, who had grown worried over his squire, had tried to find him and run into an ambush by the Black Knight as well, likewise having escaped within an inch of his life. Noting that Sir Brack had temporarily disappeared around the same time, Valiant becomes suspicious, but on the advice of Gawain, suppresses his suspicion of Brack. Some time later, Aleta and Ilene come to Camelot to attend a tournament held in their honor; as an added prize, the winner of this joust will win Aletas hand. Valiant dons the armor of Gawain, who is too seriously wounded to participate, in order to win Aleta, but he fails and is unmasked. But then another contender appears and wins the bout before falling off his horse - and this knight turns out to be Sir Gawain himself. Awakening on his sickbed, Gawain beholds Aleta and falls head over heels in love with her, and out of respect for his patron, Valiant does not dare tell him the truth.

For his act of presumption, Valiant is punished to being confined to his quarters and attending to his master. But then a mysterious messenger comes to the castle to see Sir Brack, and the same night King Aguars seal is thrown through the window of Sir Gawains chambers and lands at Valiants feet. Realizing that his parents are in trouble, Valiant immediately leaves Camelot, leaving a bewildered Aleta behind. But as he prepares to return to his home, he is ambushed and captured by Sligons Vikings and the Black Knight, who reveals himself as Sir Brack. Brack has made a pact with Sligon: For delivering King Aguars family, Sligon will assist Brack in conquering Camelot and assuming rulership over Britain.

Shortly, Aleta, who is unwilling to let Valiant run off, arrives at the scene and is captured herself, and the two are brought to Thule, where Sligon prepares their execution and that of Valiants parents. However, a group of Christian Vikings led by Aguar and Valiants old friend Boltar (Victor McLaglen) prepare to stage a revolution, and Boltar infiltrates the castle. Valiant manages to escape his cell and team up with Boltar, who intends to assassinate Sligon while Valiant is to give an attack signal to their cohorts once Sligon has fallen. But Valiant is discovered before Boltar manages to kill Sligon, and during his struggle with a guard a false signal is given which makes the Christian Vikings attack too early. Just as things seem bleak, Valiant manages to set fire to several parts of the castle, throwing the defenders into confusion, and eventually slays Sligon in single combat.
 judicial duel to the death, and despite Gawains protests and his offer to fight in Valiants stead, the young prince accepts the challenge and succeeds in killing the traitor with his fathers sword after a long and hard fight. Following its resolution, Valiant offers Aleta back to his master, but Gawain stays his hand; during the long period of worry over their loved ones, the older knight has finally come to learn the truth, and he and Ilene have fallen in love with each other. In the end, Valiant is finally made a fully privileged Knight of the Round Table.

==Cast==
* James Mason as Sir Brack
* Janet Leigh as Princess Aleta
* Robert Wagner as Prince Valiant
* Debra Paget as Ilene
* Sterling Hayden as Sir Gawain
* Victor McLaglen as Boltar
* Donald Crisp as King Aguar
* Brian Aherne as King Arthur Barry Jones as King Luke
* Mary Philips as Queen
* Howard Wendell as Morgan Todd
* Tom Conway as Sir Kay
* Primo Carnera as Sligon
* Don Megowan  ...  Sir Lancelot    Richard Webb  ... Sir Galahad

==References==
 

==External links==
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 