The Ghost Writer (film)
{{Infobox film
| name            = The Ghost Writer
| image           = Ghostwriterlarge.jpg
| caption         = US film poster
| director        = Roman Polanski
| producer        = Roman Polanski Robert Benmussa Alain Sarde Robert Harris
| based on        =  
| starring        = Ewan McGregor Pierce Brosnan Kim Cattrall Olivia Williams Tom Wilkinson Timothy Hutton Jon Bernthal Tim Preece Robert Pugh David Rintoul Eli Wallach
| music           = Alexandre Desplat
| cinematography  = Paweł Edelman
| editing         = Hervé de Luze
| distributor     = Summit Entertainment  (United States)  Optimum Releasing  (United Kingdom) 
| released        =  
| runtime         = 128 minutes
| country         = United Kingdom France Germany
| language        = English
| budget          = $45 million   
| gross           = $60,222,298 
}} Robert Harris The Ghost, with the screenplay written by Polanski and Harris. It stars Ewan McGregor, Pierce Brosnan, Kim Cattrall and Olivia Williams.   Retrieved 2012-01-30 

The film won numerous cinematic awards including Best Director for Polanski at the 60th Berlin International Film Festival and also at the 23rd European Film Awards in 2010. 

==Plot== memoirs of Prime Minister manuscript outside, emphasizing that it is a security risk. Rhinehart is allowing the new ghost four weeks to get the story straight.
 Foreign Secretary illegal seizure of suspected terrorists and handing them over for torture by the Central Intelligence Agency|CIA, a possible war crime. Lang faces prosecution by the International Criminal Court unless he stays in the U.S. (or one of the few other countries that does not recognise the courts jurisdiction). As reporters and protesters swarm the island, the writer is moved into McAras old room at Rhineharts house, where personal belongings have not been cleared out yet. While Lang is away in Washington, D.C.|Washington, the writer finds an envelope containing photographs of Langs university days that suggest McAra might have stumbled across clues to a dark secret. Among the material is a handwritten phone number, which he rings and is answered by Rycart.

During a bicycle ride around the island, the writer encounters an old man (Eli Wallach) who tells him that the current couldnt have taken McAras body from the ferry where he disappeared to the beach where it was discovered. He also reveals that a neighbour saw flashlights on the beach the night the body was discovered, but later fell downstairs and went into a coma. The writer is later intercepted by Ruth and her security guard, who take him back to the estate. There, while drinking a bottle of Rhinehart Winery wine, Ruth admits that Lang has never been very political, and until recently had always taken her advice. When the writer tells her the old mans story, she suddenly rushes out into the rainy night to "clear her head." Upon returning, she confides in the writer that Lang and McAra had argued the night before he died. She and the writer end up sleeping together.

The next morning, the writer decides he is getting too intimate with his subject and to move back to the hotel, taking the car that McAra used on his last journey. Unable to cancel the pre-programmed directions on the cars Satellite navigation|sat-nav, he suddenly decides to follow them. He arrives at Belmont, Massachusetts|Belmont, home of Professor Paul Emmett (Tom Wilkinson). Emmett denies anything more than a cursory acquaintance with Lang, despite the writer showing him two photographs of the pair, as well as another one on the wall of Emmetts study. When the writer tells Emmett that the sat-nav proves McAra visited him on the night he died, Emmett denies any knowledge and becomes evasive. The writer leaves Emmetts estate, and is forced to elude a car pursuing him. The writer boards the ferry back to Marthas Vineyard, but when he sees the pursuit car drive aboard, with two men looking for him, he flees the boat at the last moment and checks into a small motel by the ferry dock.

Not knowing to whom to turn, the writer again dials Rycarts cell phone, asking for help. While waiting for Rycart to pick him up at the motel, the writer does research on Emmett and links his think tank to a military contractor. He also finds leads that connect Emmett to the Central Intelligence Agency|CIA. When Rycart arrives, he reveals that McAra gave him documents linking Lang to torture flights, and that McAra claimed he had found something new which he wrote about in the "beginning" of the manuscript. The men cannot, however, find anything in the early pages. The writer and Rycart further discuss Emmetts relationship with Lang, with Rycart recounting how Langs decisions uniformly benefited U.S. interests when he was Prime Minister. When the writer is summoned to accompany Lang on his return flight by private jet, he confronts Lang and accuses him of being a CIA agent recruited by Emmett. Lang derides his suggestions.
 assassinated by Fulbright scholar at Harvard. The writer realises that the clues were hidden in the original manuscript in the opening words of each chapter, and discovers the message, "Langs wife Ruth was recruited as a CIA agent by Professor Paul Emmett of Harvard University." He concludes that Ruth shaped Langs every political decision to benefit the U.S. under direction from the CIA.

The writer passes a note to Ruth telling of his discovery. She unfolds the note, and is devastated. When she sees the writer raising a glass, she is kept from following him by Emmett and other assistants. As the writer leaves the party he attempts to take a taxi, without success; then, as he crosses the street off-camera, a car accelerates in his direction, and sound effects and flying papers indicate that he has been hit.

==Cast==
* Ewan McGregor as The Ghost, an unnamed Ghostwriter|ghostwriter. British Prime Minister.
* Olivia Williams as Ruth Lang, Langs wife.
* Kim Cattrall as Amelia Bly, Langs personal assistant
* Timothy Hutton as Sidney Kroll, Langs American Lawyer|lawyer.
* Tom Wilkinson as Paul Emmett, a professor at Harvard Law School.
* Jon Bernthal as Rick Ricardelli, the ghostwriters Literary agent|agent.
* James Belushi as John Maddox,  Rhinehart executive. foreign secretary.
* Tim Preece as Roy, managing director of Rhinehearts London business. War in Afghanistan.
* Eli Wallach as The Old Man at Marthas Vineyard.

===Non-fictional allusions=== British Prime special relationship with the United States. The author of the book on which the film is based, has said he was inspired at least in part by anger at Mr Blairs policies, and calls for him to face war crimes trials. 

Robert Pugh, who portrayed the British Foreign Secretary, Richard Rycart, and Mo Asumang, who played the US Secretary of State, both physically resemble their real-life counterparts, Robin Cook and Condoleezza Rice. Like the fictional Rycart, Cook had foreign policy differences with the British Prime Minister. The old man living on Marthas Vineyard is an allegorical representation of Robert McNamara. {{cite news|url=http://www.guardian.co.uk/film/2010/apr/18/the-ghost-roman-polanski-review
|title=The Ghost—Roman Polanskis Immaculately Crafted Adaptation of Robert Harriss Bestseller Is a Chilling and Sinister Study of Power|work=The Observer|author=Philip French|French, Philip |date=April 18, 2010|accessdate=March 5, 2011
|quote=Oddly, as co-adaptors, Polanski and Harris have played down a character carefully signalled in the book. In the film, the 94-year-old Eli Wallach plays an elderly Vineyard resident who gives the ghost writer some vital information concerning the cove where the previous writers corpse washed up. In the novel, he is clearly identified as the former secretary of state Robert McNamara by his rimless glasses and hairstyle, his statement about war crimes ("We could all have been charged with those. Maybe we should have been.") and a reference to a real event in 1972: "Hell, a guy tried to throw me off that damn ferry when I was still at the World Bank." This explains Harriss curious, ludic choice of the name McAra for the original ghost in the novel.|location=London}} 

==Production==
Polanski had originally teamed with Robert Harris for a film of Harriss novel Pompeii (novel)|Pompeii,  but the project was cancelled because of the looming actors strike that autumn.  
 The Ghost. They co-wrote a script and in November 2007, just after the books release, Polanski announced filming for autumn 2008.  In June 2008, Nicolas Cage, Pierce Brosnan, Tilda Swinton, and Kim Cattrall were announced as the stars.  Production was then postponed by a number of months, with Ewan McGregor and Olivia Williams replacing Cage and Swinton as a result.
 Strausberg Airport near Berlin stood in for the Vineyard airport.  A few brief exterior shots for driving scenes were shot by a second unit in Massachusetts, without Polanski or the actors.
 arrested by Swiss police in September 2009 at the request of the U.S. and held for extradition on a 1978 arrest warrant. Due to Polanskis arrest, post-production was briefly put on hold, but he resumed and completed work from house arrest at his Swiss villa. He was unable to participate in the films world premiere at the Berlinale festival on February 12, 2010. 

==Release==
The film premièred at the 60th Berlin International Film Festival on 12 February 2010,  and was widely released throughout much of Europe during the following four weeks. It went on general release in the US on 19 March 2010 and in the UK on 16 April 2010. 

==Reception==
The film has received generally positive reviews from critics. Review aggregator Rotten Tomatoes reported that 83% of critics gave positive reviews based on a sample of 198 reviews with an average rating of 7.4/10.    Its consensus notes that, "While it may lack the revelatory punch of Polanskis finest films, Ghost Writer benefits from stylish direction, a tense screenplay, and a strong central performance from Ewan McGregor."  Another review aggregator, Metacritic, gave the film an average rating of 77% based on 35 reviews.  For Andrew Sarris the film "constitutes a miracle of artistic and psychological resilience."  Roger Ebert gave the film four stars and declared: "This movie is the work of a man who knows how to direct a thriller." 

Journalist/blogger William Bradley has dubbed it "one of the best films Ive seen in recent years" in a review for The Huffington Post that dealt with the films artistic and political dimensions.  The Guardian said: "Roman Polanskis deft take on Robert Harriss political thriller is the directors most purely enjoyable film for years."    

Writing for LAS Magazine, Theon Weber gave the film a 6.8/10 rating and called it "a thriller with topical ambitions; it takes place in a jittery, bomb-fearing Britain and America, often in airports or official buildings, where the weary rituals of security screenings refuse to let the characters or the audience relax." 

However,  ); the grant is not required to be repaid.  The Bourne The Bourne Aeon Flux, The Pianist, The Constant Gardener, Unknown (2011 film)|Unknown, Inglourious Basterds, Anonymous (film)|Anonymous, etc.

==Awards==
 
The movie has won numerous awards, particularly for Roman Polanski as director, Ewan McGregor in the lead role, and Olivia Williams as Adam Langs wife.

==See also==
*2010 in film
*British films of 2010
*List of films set in London
*List of films set in New England
*List of thriller films
*Roman de Gare (France, 2006), by Claude Lelouch (who appears as Hervé Picard), also about a ghost writer.

==References==
 

==External links==
 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 