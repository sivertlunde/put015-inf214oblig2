Aata Bombata
{{Infobox film
| name           = Aata Bombata
| image          = Kannada film Aata Bombata poster.jpg
| caption        = Film poster
| director       = B. C. Gowrishankar
| producer       = G. Manmohan A. Keshava A. Narasimha D. K. Keshava Prasad
| screenplay     = Veerappa Maralavadi B. C. Gowrishankar
| story          = Veerappa Maralavadi
| starring       = Shankar Nag Srilatha
| narrator       = 
| music          = Hamsalekha
| cinematography = B. C. Gowrishankar
| editing        = P. Bhaktavatsalam
| studio         = Padmavathi Art Productions
| distributor    = 
| released       =  
| runtime        = 121 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Kannada cinema.      

==Cast==
* Shankar Nag
* Srilatha
* Sudheer Ramakrishna
* Mysore Lokesh
* Ramesh Bhat
* Thriveni
* Honnavalli Krishna
* Tennis Krishna
* Thoogudeepa Srinivas in a cameo appearance
* G. K. Govinda Rao in a cameo appearance

==Soundtrack==
Hamsalekha composed the music for the soundtracks in the film. The album has four tracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Bhale Bhale Aata Bombata
| lyrics1 = 
| extra1 = S. P. Balasubrahmanyam
| length1 = 
| title2 = Kaalidasanajne Aayithu
| lyrics2 = 
| extra2 = S. P. Balasubrahmanyam, S. Janaki
| length2 = 
| title3 = Ittaru Ittaru Guri Ittaru
| lyrics3 = 
| extra3 = S. P. Balasubrahmanyam, Manjula Gururaj
| length3 = 
| title4 = Nodi Seereya Maaye
| lyrics4 = 
| extra4 = Manjula Gururaj
| length4 = 
}}

==References==
 

 
 
 
 


 