Nadodikkattu
{{Infobox film
| name           = Nadodikkattu
| image          = Nadodikkattu.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD cover 
| film name      = 
| director       = Sathyan Anthikkad
| producer       = Casino Productions
| writer         =  Sreenivasan 
| story          = Siddique-Lal
| based on       = 
| narrator       =  Sreenivasan Thilakan Innocent Shobana Captain Raju Mamukkoya Sankaradi  Shyam Yusuf Ali Kechery (lyrics)  
| cinematography = Vipin Mohan
| editing        = K. Narayanan
| studio         = 
| distributor    = Century Films
| released       =  
| runtime        = 158 minutes
| country        = India
| language       = Malayalam
| budget         = Rs 17 lakh http://articles.timesofindia.indiatimes.com/2012-02-02/kochi/31016603_1_malayalis-greener-pastures-vijayan 
| gross          = 
}}
 Malayalam satirical Sreenivasan based on a story by the Siddique-Lal duo. The story revolves around two impecunious young Indian men, Ramdas (Mohanlal) and Vijayan (Sreenivasan (actor)|Sreenivasan) who not being able to find any job in Kerala, plan to immigrate to Dubai to make their fortunes, but get deceived and they end up in the neighbouring state of Tamil Nadu. Nadodikattu drew upon relevant social factors affecting Kerala of the 1980s such as widespread unemployment and poverty. 
 Soman as themselves. The film represents popular Malayalam cinema at its best and established the Mohanlal-Sreenivasan combination as one of its most memorable and bankable cinematic pairs. The story, screenplay and characters have achieved cult status. 
 Seema and Mammootty. It gave way to two sequels, Pattanapravesham (1988) and Akkare Akkare Akkare (1990) known for their excess of mimicry and low-brow slapstick. 
 Katha Nayagan (1988) which starred Pandiarajan and S. V. Sekhar in the lead roles and also in Kannada as Tenali Rama with Ramesh Aravind and Jaggesh.

==Plot==

Nadodikattu opens by introducing the two protagonists, Ramdas/Dasan (Mohanlal) and Vijayan (Sreenivasan (actor)|Sreenivasan), who are employed as ill-paid peons. A commerce graduate, and proud of that, Dasan frequently vents his frustration in having to serve lesser qualified superiors and having to settle with lesser than what he feels he deserves in life. Vijayan, his less educated, comic sidekick, is his roommate and sole friend. Their relationship is one full of tension, with Dasan adopting a superior stance on the account of his education and good looks, which fiercely rattles Vijayan. Several comic situations later in the film borrow from this aspect of their characters. Dasan frequently bosses over Vijayan to do all the household chores.

Dasan and Vijayan, hopeful of getting a better job, as soon as the new managing director joins their company, wait for him to take charge. However, one day, on their way to work, they get involved in an unnecessary altercation with an unknown person. Dasan, in order to grab attention from a lady, abuses the person and pushes him into a puddle of rainwater. Vijayan punctures his car tires. But when they reach the workplace, they find out that the person that they had picked the fight with is their new managing director. Both vanish from the workplace and surface on the next day in disguises; Dasan wearing dark sunglasses and Vijayan wearing a long beard. Both try to convince their supervisor that they were seriously ill the previous day, but they are made to report to the new managing director. They manage to fool him initially and start work but the managing director soon finds out the truth from old office photographs and both get thrown out from work.

Being jobless, their landlord (Sankaradi) convinces them to take a bank loan and buy two cows. Dasan and Vijayan, upbeat about the new business, start dreaming of a bright future. However, they soon find out that the cows yield very little milk and the business fails. The bank contacts them for defaulting on the loan repayments. To escape, Vijayan sells the cows and arranges for them to go to Dubai (in United Arab Emirates) with whatever money they get. Gafoor (Mamukkoya) is an agent who promises to take them to the Gulf. Gafoor explains them that his uru (boat) going to California will be diverted past Dubai sea shore for them. Gafoor also instructs them to wear Arabic clothing thawb to fool the authorities. Dasan and Vijayan once again start dreaming about the bright and luxurious life ahead.

On reaching the sea shore, both change into the Arabic attire. Two strangers (Johny and Ajith Kollam) follow Dasan and Vijayan and forcefully exchange their suitcases. Confused, Dasan and Vijayan start exploring the city. They are surprised to see no Arabs in the city and soon find signboards in Tamil. They realize that they have been cheated by Gafoor who has off loaded them in Chennai, Tamil Nadu. Dasan and Vijayan check the suitcase and find drugs inside. They contact local police and hand over the suitcase.

Meanwhile, the strangers who are actually gang members of Ananthan Nambiar (Thilakan), smuggler and underworld don, find out that they were mistaken. Ananthan Nambiar believes that those in Arabic dress were Crime Investigation Department officers in disguise.

Dasan and Vijayan with the help of Dasan’s friend Balan (Innocent(actor)|Innocent) get jobs at Ananthan Nambiar’s office. Dasan meets his colleague Radha (Shobana) who turns out to be their neighbour as well. But soon Dasan and Vijayan gets fired by Ananthan Nambiar thinking that they are undercover officers. On the road again, Vijayan tries to get a chance in movies. He visits noted film director I. V. Sasi’s home and meets Sasi’s wife, actress Seema (actress)|Seema.

Dasan starts a small vegetable business with Radha’s help. A minor romance plot develops between Dasan and Radha. Meanwhile Ananthan Nambiar hires contract killer Pavanayi (Captain Raju) to finish off Dasan and Vijayan. But Pavanayi dies while trying to kill Dasan and Vijayan. This irks Ananthan Nambiar even more and makes him all the more paranoid about these two "officers".

As the story proceeds to climax, Dasan and Vijayan are called to a rundown factory by one of their other enemies, Kovai Venkatesan (Janardhanan (actor)|Janardhanan), a well-connected businessman with political aspirations for an ostensible meeting, where he plans to liquidate them both. Dasan and Vijayan find themselves surrounded by Ananthan Nambiar’s gang as well, the latter also having selected the location to do a drug deal. However, the both gangs mistake each other as enemies and a mixed fight breaks out. Dasan and Vijayan manage to lock all others inside the building as police arrive and arrest everyone.

The movie concludes as Dasan and Vijayan gets selected into the Tamil Nadu police as Crime Investigation Department officers and they along with Radha drive away in a jeep.

==Cast==
*Mohanlal as Ramdas (or Dasan), an unemployed Indian youth in the 1980s. He dreams of migrating to the United Arab Emirates. Along with Vijayan, Dasan is widely regarded as one of the iconic characters in the history of Malayalam films.  It was Sathyan Anthikkad who discovered the hilarious combination of Mohanlal and Srinivasan and cast them as the now iconic Dasan and Vijayan .  Srinivasan as Vijayan, another unemployed Indian youth. Vijayan, both cloying and spiteful, was the foil to the haughty Dasan who, for all his bravado, couldnt do without a companion. 
*Shobana as Radha, a Malayali living in Chennai, Tamil Nadu
*Thilakan as Ananthan Nambiar, a Malayali drug smuggler based in Chennai Innocent as driver Balagopalan, Malayali driver in Chennai. Janardhanan as Kovai Venkatesan, a Malayali politician in Chennai suburbs
*Captain Raju as Pavanayi (P. V. Narayanan), a hitman hired by Nambiar to kill the Crime Investigation Department officers
*Mamukkoya as Gafoor, an illegal human trafficking agent operating in the Arabian sea
*C.I. Paul as a Malayali inspector in Tamil Nadu police
*Meenakumari as Radhas mother Santhakumari as Ramdas mother
*T. P. Madhavan as Managing Director  
*Sankaradi as Panicker
* Kundara Johny as Varghese, Nambiars henchman
*Ajith Kollam as Nambiars henchman
*Bobby Kottarakkara as a broker Seema in a guest role as herself
*I.V. Sasi in a guest role as himself
*M.G. Soman in a guest role as himself

==Production== Seema and Mammootty, on a shoe-string budget of Rs 17 lakh. 
 Siddique and Lal who Sreenivasan who later developed them into a screen play. It was later revealed that Sathyan Anthikkad asked Sreenivasan to give a different treatment to the climax, thus they were able to come up with an agreeable climax for the script than that to the first version. Later they had creative differences during the same climax shooting and even stopped talking to each other. However, being best of friends, they re-united during dubbing of the film. 

==Reception==
The film received excellent reviews.

The film was a huge box office grosser doing well not only in Kerala but other neighbouring states and the Persian Gulf as well. The story and screenplay is considered as one of the classics of mainstream Malayalam films, portraying a cross-section of Keralas social issues at the time, primarily the struggle of unemployed Malayalee youths and the means they pursue to find employment, such as seeking job opportunities in the Persian Gulf region, the scramble for formal educational qualifications (reflected through the character Ramdasas boasting about his graduate degree in commerce with First Class), etc., through amusing comic dialogues. These dialogues are still remembered fondly by Malayalees today. This movie has been considered as the best example of the Mohanlal-Sreenivasan on-screen combination, spawning two sequels with the same duo, Pattanapravesham and Akkare Akkare Akkare, as well as a spin-off film called "((Mr. Pavanayi 99.99)).

The movie is considered as one of the most popular Malayalam movies of all time. Even after more than 25 years, the movie draws huge audience whenever it is telecast on television channels. "The secret of the duo still being alive in the Malayalis consciousness may be because its a story of two unemployed youngsters told using a humorous narrative. Unemployment was one of the biggest issues and migration to greener pastures was typical of Malayali experience. This has continued to the current decade. So, the audience has no problem relating to Dasan and Vijayan," says Sathyan Anthikkad, who directed the movie. According to Anthikkad, it was the convergence of a script-writer, director and actors at the subtlest level that resulted in its success. http://www.thehindu.com/features/cinema/siddique-speaking/article4605926.ece 

"Comedies were not much in demand here before films such as Nadodikattu happened. Lal and I were instrumental in popularising comedies to a great extent." says Siddique. 

== Sound Track == Shyam and the lyrics were penned by Yusuf Ali Kechery. 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! # !! Song !! Singer(s) !! Duration
|-
| 1 || "Karakana" || K. J. Yesudas || 4:25 
|-
| 2|| "Vaisakha Sandhye" || K. J. Yesudas || 4:09
|-
| 2|| "Vaisakha Sandhye" || K. S. Chitra || 4:09
|}


== References ==
 

==External links==
* 
 

 
 
 
 
 
 
 