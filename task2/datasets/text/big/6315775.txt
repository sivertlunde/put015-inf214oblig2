Die Nibelungen
 
 
 
{{Infobox film
| name           = Die Nibelungen
| image          = Nibelung.jpg
| caption        = Original 1924 film poster
| director       = Fritz Lang
| producer       = Erich Pommer
| screenplay     = Fritz Lang Thea von Harbou
| based on       = Nibelungenlied
| narrator       =
| starring       = Paul Richter  Margarete Schön  Hanna Ralph  Bernhard Goetzke Theodor Loos  Rudolf Klein-Rogge  Rudolf Rittner  Hans Adalbert Schlettow  Georg August Koch  Georg John  Gertrud Arnold  Hans Carl Müller  Erwin Biswanger  Fritz Alberti  Annie Röttgen 
| music          = Gottfried Huppertz
| cinematography = Carl Hoffmann Günther Rittau Walter Ruttmann
| editing        =
| studio         = Decla-Bioscop UFA
| released       =  
| runtime        = 143 min. (1st part) 145 min. (2nd part)
| country        = Weimar Republic German intertitles
| budget         =
}}
 silent fantasy films created by Austrian director Fritz Lang in 1924: Die Nibelungen: Siegfried and Die Nibelungen: Kriemhilds Revenge. 
 epic poem Century Theatre in New York City in the short-lived Phonofilm sound-on-film process. Kriemhilds Revenge was released in the U.S. in 1928.

==Plot summary==
===Die Nibelungen: Siegfried===

 The title character Sigurd|Siegfried, son of King Siegmund of Xanten, masters the art of forging a sword at the shop of Mime. Mime sends Siegfried home, but while preparing to leave, Siegfried hears the tales of the kingdom of Burgundy, the kings who rule there, as well as of Kriemhild, the princess of Burgundy. Siegfried announces he wants to win her hand in marriage, much to the amusement of the smiths. By way of physical violence, Siegfried demands to be told the way. Mime, who is envious of Siegfrieds skill as a swordsmith, claims there is a shortcut through the Wood of Woden; in reality, this route will lead Siegfried away from Burgundy and expose him to attack from the magical creatures inhabiting the wood. During his journey, Siegfried discovers a dragon, and deviates from his path to slay it. He touches its hot, yellow blood and understands the language of the birds, one of which tells him to bathe in the dragons blood in order to become invincible to attack — except for one spot on his shoulder blade, which is missed after being covered by a falling lime (linden) leaf.

Soon after, the powerful Siegfried trespasses on the land of the Nibelungs and is attacked by Alberich, King of the dwarf (mythology)|Dwarves, who has turned himself invisible. Siegfried defeats Alberich, who offers Siegfried a net of invisibility and transformation. Siegfried is not persuaded to spare Alberichs life, whereupon Alberich offers to make Siegfried "the richest king on earth!"  . While Siegfried is mesmerized by the treasure and the sword Balmung, Alberich tries to defeat him, but dies in the attempt. Dying, Alberich curses all inheritors of the treasure and he and his dwarves turn to stone.
 Hagen of King Gunther, to win the hand of Brunhild, the Queen of Iceland. The men travel to Brunhilds kingdom, where Siegfried feigns vassalage to Gunther so that he can avoid Brunhilds challenge and instead use the nets power of invisibility to help Gunther beat the powerful Queen in a threefold battle of strength. The men return to Burgundy where Gunther marries Brunhild and Siegfried weds Kriemhild. 

Brunhild is not, however, completely defeated. She suspects deceit and refuses to consummate the marriage. Hagen again convinces Siegfried to help. Siegfried transforms himself into Gunther and battles Brunhild and removes her arm-ring during battle, after which she submits to his will. Siegfried leaves the real Gunther to consummate the marriage.

Kriemhild discovers Brunhilds armlet and asks Siegfried about it. Siegfried discloses the truth to Kriemhild about his role in Brunhilds defeat. When the Nibelungen treasure that Siegfried acquired from Alberich arrives at the court of Burgundy as Kriemhilds wedding gift, Brunhild becomes more suspicious about Siegfrieds feigned vassalage to Gunther. Brunhild dons the Queen Mothers jewellery and proceeds to the cathedral to enter as the first person, as is her right as Queen of Burgundy. Kriemhild tries to take Brunhilds right of way and an argument erupts between the two Queens. Kriemhild betrays her husbands and brothers secret to Brunhild, who then confronts Gunther.

Brunhild demands that Siegfried must be killed, which she justifies by claiming that Siegfried stole her maidenhood   when he struggled with her on her wedding night. Hagen von Tronje and King Gunther conspire to murder Siegfried during a hunt in the Odenwald. Hagen deceives Kriemhild into divulging Siegfrieds weak spot by sewing a cross on the spot in Siegfrieds tunic. 

After the hunt, Hagen challenges Siegfried to a race to a nearby spring. When Siegfried is on his knees drinking, Hagen pierces him from behind with a spear. 

In an evil twist of bitter revenge, Brunhild confesses that she lied about Siegfried stealing her maidenhood in order to avenge Gunthers deceit of her. 

Kriemhild demands her family avenge her husbands death at the hands of Hagen, but her family is complicit in the murder, and so they protect Hagen. Kriemhild swears revenge against Hagen while Brunhild commits suicide at the foot of Siegfrieds corpse, which has been laid in state in the cathedral.

 

===Die Nibelungen: Kriemhilds Rache (Kriemhilds Revenge)===

Kriemhild tries to win over the people of Burgundy to help her exact revenge against Hagen, to whom her brothers have sworn allegiance. Kriemhild bribes the people with money and treasure from the Nibelungen hoard. Margrave Rüdiger of Bechlarn arrives unannounced to woo Kriemhild on behalf of his King, King Attila|Etzel, who resides in the land of the Huns. Kriemhild initially declines, but ultimately she recognises the opportunity for revenge in her marriage with Etzel and in Rüdigers allegiance to her. She forces Rüdiger to swear allegiance to her on his sword. At that very moment, news arrives that Hagen has stolen her wedding gift, the Nibelungen hoard, which Hagen has, unbeknownst to all, sunk into the Rhine river.

Kriemhild travels to Etzels lands and accepts his hand. As a gift to Kriemhild for bearing him a son, Ortlieb, Etzel grants her a wish. Kriemhild requests Etzel to invite her family to celebrate the Midsummer Solstice with them in the Hun kingdom. In the meantime, Kriemhild bribes Etzels Hun warriors with money and treasure to avenge her and attack Hagen.

When the Burgundians arrive, the Huns launch several unsuccessful attempts. Instead, the Huns  launch an attack on the Burgundian soldiers during their feast in the subterranean caves where the Huns reside. The Burgundian Knight Dankwart manages to escape the melee and warns the Burgundian kings who are feasting with Etzel and Kriemhild in Etzels palace. Upon hearing of the treacherous attack, Hagen of Tronje murders Etzels son, and battle breaks out. Dietrich of Bern manages to negotiate an exit from the hall for Etzels royal entourage, which leaves the Burgundian guests imprisoned in Etzels palace. 

The remaining 45 minutes of the film consists of multiple battles in which the Huns launch multiple attacks on the Burgundians. Kriemhild offers her family freedom if they surrender Hagen to her. They decline. Ultimately Kriemhild calls upon Rüdiger to fulfill his oath of allegiance by attacking Hagen. Rüdiger refuses, but is forced to by Etzel. At the beginning of the battle, Rüdiger is killed by Volker of Alzey after Rüdiger of Bechlarn himself smote his nearly son-in-law Giselher of Burgundy with his sword. Gerenot carries his dead brother outside the hall to show his sister what she has done with her vindictiveness. Kriemhild grieves for Giselher and begs Gerenot for a last time to surrender Hagen of Tronje to her, but he refuses again and so Gerenot is stinged by a few Huns. In a final act of desperation, Kriemhild commands the palace be set alight. 

As the flames smoulder, only Gunther and Hagen survive. Dietrich of Bern fetches the two remaining men from the palace and delivers them to Kriemhild, who demands Hagen to reveal the hiding place of the Nibelungen hoard. When Hagen states that he has sworn not to reveal the hiding place as long as one of his kings is still alive, Kriemhild commands Gunthers beheading. When Hagen reveals that no one now knows the location of the treasure apart from him and God, and that God will never tell more than he does. Kriemhild grabs Siegfrieds sword from Hagen and cuts him down. Infuriated by Kriemhilds act of murder, Sword Master Hildebrandt stabs Kriemhild from behind.

Etzels final words are that Kriemhild should be taken back home to her dead husband, Siegfried, because she never belonged to any other man  .

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-  King Siegfried of Xanten
|-  Kriemhild of Burgund
|-  Queen Brunhild of Isenland
|- 
| Hans Adalbert Schlettow || Hagen of Tronje
|- 
| Bernhard Goetzke || Volker of Alzey
|-  King Gunther of Burgund
|- 
| Rudolf Klein-Rogge || King Etzel
|- 
| Rudolf Rittner || Margrave Rüdiger of Bechlarn
|- 
| Georg August Koch || Hildebrandt
|- Alberich the dwarf / Blaodel (Etzels brother)
|-
| Gertrud Arnold || Queen Ute of Burgund
|-
| Hans Carl Müller || Gerenot of Burgund
|-
| Erwin Biswanger || Giselher of Burgund
|-
| Fritz Alberti || Dietrich of Bern
|-
| Annie Röttgen || Dietlind of Bechlarn
|}

 
 

== References ==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 