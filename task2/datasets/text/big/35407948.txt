The Silver Cliff
{{Infobox film
| name           = The Silver Cliff
| image          = The Silver Cliff Poster.jpg
| caption        = Film poster
| director       = Karim Aïnouz
| producer       = Rodrigo Teixeira
| writer         = Beatriz Bracher
| starring       = Alessandra Negrini Alice Borges Camila Amado Carla Ribas Gabi Pereira Milton Gonçalves Otto Jr. 
| music          = 
| cinematography = Mauro Pinheiro Jr.
| editing        = 
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}}

The Silver Cliff ( ) is a 2011 Brazilian drama film directed by Karim Aïnouz.     Alessandra Negrini won the Best Actress prize on the Havana Film Festival for her role.

==Plot==
Violeta (Alessandra Negrini) is a dentist married and with one son, having a normal working day. While listening to a message left on the phone she panics. The message was written by her husband, Djalma (Otto Jr.), who said he was leaving her and going to Porto Alegre. He asks Violeta to not go after him, but she does not follow the advice and tries to travel as soon as possible, to the capital of Rio Grande do Sul. 

==Cast==
* Alessandra Negrini as Violeta
* Camila Amado as Norma
* Luisa Arraes
* Milton Gonçalves
* Sérgio Guizé Thiago Martins as Nassir
* Otto Jr. as Djalma
* Carla Ribas

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 