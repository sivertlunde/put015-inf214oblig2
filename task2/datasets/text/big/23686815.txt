Beck – Okänd avsändare
{{Infobox film
| name           = Beck – Okänd avsändare
| image          =Beck 13 - Okänd avsändare.jpg
| image_size     =
| alt            = 
| caption        = Swedish DVD-cover
| director       = Harald Hamrell
| producer       = Lars Blomgren Börje Hansson
| writer         = Cilla Börjlind Rolf Börjlind
| narrator       =
| starring       = Peter Haber Mikael Persbrandt Malin Birgerson
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 2002
| runtime        = 100 min
| country        = Sweden Swedish
| budget         =
| gross          =
}}
Beck – Okänd avsändare is a 2002 film about the Swedish police detective Martin Beck directed by Harald Hamrell.

== Cast ==
*Peter Haber as Martin Beck
*Mikael Persbrandt as Gunvald Larsson
*Malin Birgerson as Alice Levander
*Marie Göranzon as Margareta Oberg
*Rebecka Hemse as Inger
*Mårten Klingberg as Nick 
*Peter Hüttner as Oljelund
*Gustaf Hammarsten as Klas Duvander 
*Ingvar Hirdwall as Valdemar, Martin Becks neighbour
*Michael Flessas as Jurij Rostoff
*Annika Hallin as Mamman

== References ==
*{{cite web | title=Beck - okänd avsändare (2002) |
url=http://www.sfi.se/sv/svensk-film/Filmdatabasen/?type=MOVIE&itemid=49060
| publisher=Swedish Film Institute | accessdate=2009-07-14}}

== External links ==
* 

 

 
 
 
 
 


 
 