Reservoir Dogs
 
 

{{Infobox film
| name             = Reservoir Dogs
| image            = Reservoir dogs ver1.jpg
| caption          = Theatrical release poster.
| director         = Quentin Tarantino
| producer         = Lawrence Bender
| writer           = Quentin Tarantino
| starring         = {{plainlist|
* Harvey Keitel
* Tim Roth
* Chris Penn
* Steve Buscemi
* Lawrence Tierney
* Michael Madsen
}}
| cinematography   = Andrzej Sekuła
| editing          = Sally Menke Live Entertainment
| distributor      = Miramax Films
| released         =  
| runtime          = 99 minutes
| country          = United States
| language         = English
| budget           = $1.2 million   
| gross            = $2.8 million 
}}
 debut of pop culture nonlinear storyline.
 cult hit.  It was named "Greatest Independent Film of all Time" by Empire (film magazine)|Empire magazine. Reservoir Dogs was generally well received, and the cast was praised by many critics. Although it was not given much promotion upon release, the film became a modest success in the United States after grossing $2,832,029, recouping its $1.2 million budget. The film was more successful in the United Kingdom, grossing nearly Pound sterling|£6.5 million, and it achieved higher popularity after the success of Tarantinos next directorial effort, Pulp Fiction. A soundtrack titled Reservoir Dogs: The Original Motion Picture Soundtrack was released featuring songs used in the film, which are mostly from the 1970s.

== Plot ==
Eight men eat breakfast at a Los Angeles diner before their planned diamond heist, most of them unknown to each other. Six of them use aliases: Mr. Blonde, Mr. Blue, Mr. Brown, Mr. Orange, Mr. Pink, and Mr. White. With them are mob boss Joe Cabot, the organizer of the heist, and his son/underboss, "Nice Guy" Eddie Cabot. The initial scene contains no clues as to their actual identities or intended group purpose; the dialogue is entirely conversational and seemingly irrelevant to any exposition. During the course of the film, the major characters are sporadically introduced by subtitles (e.g., "Mr. White") and their character backstories are explored in subsequent, non-linear fashion. They leave the diner and the scene shifts to the opening titles.

The following scene is of a speeding car, in which Mr. White comforts Mr. Orange, who has been shot in the abdomen and is bleeding profusely, revealing they just performed the heist. They soon reach an abandoned warehouse. Mr. Pink eventually arrives and angrily suggests that the job was a setup based on the rapid police response. Mr. White tells Mr. Pink that Mr. Brown was killed while escaping, Mr. Blue is presumed dead and talk about Mr. Blonde, who murdered several civilians after the alarm was triggered. Mr. White is angered that Joe, an old friend of his, employed such a "psychopath" and agrees about a possible setup while Mr. Pink reveals that he escaped with the diamonds and hid them in a secure location. They argue over whether to take the now unconscious Mr. Orange to a hospital. Mr. White also reveals that he has told Mr. Orange his name and hometown, which was breaking the rules.

Mr. Blonde, having watched them from a distance, steps forward and ends the dispute regarding Mr. Orange. Mr. White berates him for his deadly rampage, but Mr. Blonde dismisses the criticism. He tells the others to wait because Eddie is on his way there. Mr. Blonde has also taken a police officer, Marvin Nash, hostage and the three men beat Nash in an attempt to find out if there is an informant. Eddie then arrives and orders Mr. Pink and Mr. White to assist him in retrieving the stolen diamonds and dispose of the hijacked vehicles, while Mr. Blonde stays with Nash and the unconscious Mr. Orange.

Alone with Mr. Blonde, Nash denies any knowledge of a setup, but Mr. Blonde is uninterested and tortures Nash for his own amusement, slashing Nashs face with a straight razor and severing his right ear. He then douses Nash with gasoline, but before he can ignite it, Mr. Orange springs to life and shoots and kills Mr. Blonde. Mr. Orange then reveals to Nash that he is an undercover cop, reassuring Nash that, upon Joes arrival, a large police force is in position to raid the warehouse.

Eddie, Mr. Pink, and Mr. White return to the warehouse to find Mr. Blonde dead. Mr. Orange claims that Mr. Blonde was going to kill all of them and take the diamonds for himself. After impulsively pulling out his gun and killing Nash, Eddie rejects Mr. Orange’s claims, saying that Mr. Blonde was a close personal friend who had always remained loyal to his father. Joe then arrives and reveals that Mr. Blue is dead along with Mr. Brown. He confidently accuses Mr. Orange of being an informant, forcing Mr. White to defend his friend.

Joe, about to execute Mr. Orange, is stopped when Mr. White points his gun at him, and Eddie then takes aim on Mr. White, creating a Mexican standoff. Joe eventually shoots Mr. Orange, wounding him again; Mr. White shoots and kills Joe in response; Eddie shoots Mr. White, wounding him, who turns and shoots Eddie, killing him, as he falls.

Mr. Pink, who was hiding during the melee, takes the diamonds and flees, though the police are heard arriving and several shouts are heard as well as gunshots, implying Mr. Pink was shot by the police as he escaped. Badly wounded, Mr. White crawls to Mr. Orange and cradles him in his arms. However, Mr. Orange reveals to Mr. White that he is in fact an undercover cop. Devastated, Mr. White points his gun at Mr. Oranges head. The police storm the warehouse, demanding that Mr. White drop his gun. In response, Mr White presumably shoots Mr. Orange and then is gunned down by the police as the film ends.

==Cast==
 " by the George Baker Selection.]]
*Harvey Keitel as Mr. White (Lawrence Dimmick) 
*Tim Roth as Mr. Orange (Det. Freddie Newandyke)
*Steve Buscemi as Mr. Pink 
*Michael Madsen as Mr. Blonde (Vic Vega)
*Chris Penn as "Nice Guy" Eddie Cabot
*Lawrence Tierney as Joe Cabot
*Edward Bunker as Mr. Blue
*Quentin Tarantino as Mr. Brown
*Kirk Baltz as Marvin Nash Randy Brooks as Holdaway

==Production==
Quentin Tarantino had been working at Video Archives, a video store in Manhattan Beach, California, and originally planned to shoot the film with his friends on a budget of $30,000 in a 16 mm film|16&nbsp;mm black-and-white format with producer Lawrence Bender playing a police officer chasing Mr. Pink.  When actor Harvey Keitel became involved and agreed to act in the film and co-produce,  he was cast as Mr. White. With Keitels assistance, the filmmakers were able to raise $1.5 million to make the film. 
 The Taking City on Fire. 
 Glengarry Glen Ross in which the mentioned robbery is never shown on camera.  Tarantino has compared this to the work of a novelist, and has said that he wanted the film to be about something that is not seen and that he wanted it to "play with a real-time clock as opposed to a movie clock ticking". 

The title for the film came from a patron at the Video Archives. While working there, Tarantino would often recommend little-known titles to customers, and when he suggested Au revoir les enfants, the patron misheard it as "reservoir dogs". 

==Reception==

===Box office=== Cannes and Toronto International Film Festival|Toronto,  Reservoir Dogs opened in the United States in 19 theaters with a first week total of $147,839.    It was expanded to 61 theaters and totaled $2,832,029 at the domestic box office.  The film achieved box office success in the United Kingdom, where it was banned from home video release until 1995.    During the period of unavailability on home video, the film was re-released in UK cinemas in June 1994. 

===Critical reaction===
 

Reservoir Dogs has come to be seen as an important and highly-influential milestone of independent filmmaking.  It has inspired many other independent films and is considered key in the development of independent cinema.  Review aggregation website Rotten Tomatoes retrospectively gives the film a 92% based on reviews from 60 critics,  and Metacritic carries an average rating of 78/100, based on 23 reviews.  Empire (film magazine)|Empire magazine named it the "Greatest Independent Film" ever made. 
 New York Daily News compared the effect of Reservoir Dogs to that of the 1895 film LArrivée dun Train en Gare de la Ciotat, whereby audiences putatively observed a moving train approaching the camera and scrambled. Bernard claimed that Reservoir Dogs had a similar effect and people were not ready for it.    Vincent Canby of The New York Times enjoyed the cast and the usage of non-linear storytelling. He similarly complimented Tarantinos directing and liked the fact that he did not often use close-ups in the film.  Kenneth Turan of the Los Angeles Times also enjoyed the film and the acting, particularly that of Buscemi, Tierney and Madsen, and said "Tarantinos palpable enthusiasm, his unapologetic passion for what hes created, reinvigorates this venerable plot and, mayhem aside, makes it involving for longer than you might suspect."  Critic James Berardinelli was of a similar opinion; he complimented both the cast and Tarantinos dialogue writing abilities.  Hal Hinson of The Washington Post was also enthusiastic about the cast, complimenting the film on its "deadpan sense of humor". 

Roger Ebert was less enthusiastic; he felt that the script could have been better and said that the film "feels like its going to be terrific", but Tarantinos script does not have much curiosity about the characters. He also stated that "  has an idea, and trusts the idea to drive the plot." Ebert gave the film two and a half stars out of four also claiming that he enjoyed it, and that it was a very good film from a talented director, like other critics, he enjoyed the cast, but stated "I liked what I saw, but I wanted more." 
 special makeup Rick Baker.    Baker later told Tarantino to take the walkout as a "compliment" and explained that he found the violence unnerving because of its heightened sense of realism.  Tarantino commented about it at the time: "It happens at every single screening. For some people the violence, or the rudeness of the language, is a mountain they cant climb. Thats OK. Its not their cup of tea. But I am affecting them. I wanted that scene to be disturbing."     

===Critical analysis=== caper noir films and points out the irony in its ending scenes.  Mark Irwin also made the connection between Reservoir Dogs and classic American noir.    Caroline Jewers called Reservoir Dogs a "feudal epic" and paralleled the color pseudonyms to color names of medieval knights.   
 Glengarry Glen The Killing.  After this film, Tarantino himself was also compared to Martin Scorsese, Sam Peckinpah, John Singleton, Gus Van Sant, and Abel Ferrara. 

A frequently cited comparison has been to Tarantinos second and more successful film Pulp Fiction,      especially since the majority of audiences saw Reservoir Dogs after the success of Pulp Fiction. Comparisons have been made regarding the black humor in both the films, the theme of accidents,  and more concretely, the style of dialogue and narrative style that Tarantino incorporates into both films.     Specifically the relationship between whites and blacks plays a big part in the films though underplayed in Reservoir Dogs. Stanley Crouch of The New York Times compared the way the white criminals speak of black people in Reservoir Dogs to the way they are spoken of in Scorseses Mean Streets and Goodfellas. Crouch observed the way black people are looked down upon in Reservoir Dogs, but also the way that the criminals accuse each other of "verbally imitating" black men and the characters apparent sexual attraction to black actress Pam Grier. 
 Common as both Mr. Brown and Officer Nash (the torture victim of Mr. Blonde), and Patton Oswalt as Holdaway (the mentor cop who was originally played by a black actor in the film)—critic Elvis Mitchell noted that this was taking the source material back to its roots since the characters "all sound like black dudes." 

===Accolades===
 
The film was screened out of competition at the 1992 Cannes Film Festival.    Grand Prix of the Belgian Syndicate of Cinema Critics. 

American Film Institute Lists
* AFIs 100 Years...100 Thrills - Nominated 
* AFIs 100 Years...100 Heroes and Villains:
** “Mr. Blonde” (Vic Vega) - Nominated Villain 
* AFIs 100 Years...100 Songs:
** Stuck in the Middle with You - Nominated 
* AFIs 100 Years...100 Movie Quotes:
** "Are you gonna bark all day, little doggie, or are you gonna bite?" - Nominated 
* AFIs 10 Top 10 - Nominated Gangster Film 

Reservoir Dogs ranks at   97 in Empire (film magazine)|Empire magazines list of the 500 Greatest Films of All Time. 

==Home media==
In the United Kingdom, the release of VHS rental video was delayed until 1995 due to the British Board of Film Classification initially refusing the film a home video certificate (UK releases are required to be certified separately for theatrical release and for viewing at home).  The latter is a requirement by law due to the Video Recordings Act 1984.  Following the UK VHS release approval, Polygram released a "Mr Blonde Deluxe Edition",  which included an interview with Tarantino and several memorabilia associated with the character Mr. Blonde, such as sunglasses and a chrome toothpick holder.

Region 1 DVDs of Reservoir Dogs have been released multiple times. The first release was a single two-sided disc from LIVE Entertainment, released in June 1997 and featuring both pan-and-scan and letterbox versions of the film.  Five years later, Artisan Entertainment (who changed their name from LIVE Entertainment in the interim) released a two-disc 10th anniversary edition featuring multiple covers color-coded to match the nicknames of five of the characters (Pink, White, Orange, Blonde and Brown) and a disc of bonus features such as interviews with the cast and crew.   

For the films 15th anniversary,   transfer and a new supplement, but not all of the extra features from the 10th Anniversary edition.    In particular, interviews with the cast and crew were removed, and a new 48-minute-long feature called "Tributes and Dedications" was included.  The packaging for the 15th anniversary edition is fancier: the discs are enclosed in a large matchbook, and the matchbook is in a thin aluminum case made to resemble a gas can.

==Soundtrack==
{{Infobox album
| Name        = Reservoir Dogs
| Cover       = 
| Type        = Soundtrack
| Artist      = Various Artists
| Released    = October 13, 1992
| Length      = 30:50 Rock
| MCA
| Producer    =
| Chronology  = Quentin Tarantino film soundtracks
| Last album  =
| This album  = Reservoir Dogs (1992) Pulp Fiction (1994)
}}
{{Album ratings AllMusic
| rev1Score =    
}}
The Reservoir Dogs: Original Motion Picture Soundtrack was the first soundtrack for a Quentin Tarantino film and set the structure his later soundtracks would follow.  This includes the extensive use of snippets of dialogue from the film. The soundtrack has selections of songs from the 1960s to 80s. Only the group Bedlam recorded original songs for the film. The radio station "K-Billys Super Sounds of the Seventies" played a prominent role in the film.  The DJ for the radio was chosen to be Steven Wright, a comedian known for his deadpan delivery of jokes.   

An unusual feature of the soundtrack was the choice of songs; Tarantino has said that he feels the music to be a counterpoint to the on-screen violence and action.    He also stated that he wished for the film to have a 1950s feel while using 70s music.  A prominent instance of this is the torture scene to the tune of "Stuck in the Middle with You". 
 Steppenwolf song. "Harvest Moon" is written by Jay Joyce.
 Fool for Love. 

 
;Track listing
# "And Now Little Green Bag..." - dialogue extract performed by Steven Wright (0:15) The George Baker Selection (3:15)
# "Rock Flock of Five" - dialogue extract performed by Steven Wright (0:11)
# "Hooked on a Feeling" - Blue Swede (2:53)
# "Bohemiath" - dialogue extract performed by Steven Wright (0:34) I Gotcha" - Joe Tex (2:27) Magic Carpet Ride" - Bedlam (5:10)
# "Madonna Speech" - dialogue extract performed by Quentin Tarantino, Edward Bunker, Lawrence Tierney, Steve Buscemi, and Harvey Keitel (0:59)
# "Fool for Love" by Sandy Rogers (3:25)
# "Super Sounds" - dialogue extract performed by Steven Wright (0:19)
# "Stuck in the Middle with You" - Stealers Wheel (3:23)
# "Harvest Moon" - Bedlam (2:38)
# "Lets Get a Taco" - dialogue extract performed by Harvey Keitel and Tim Roth (1:02)
# "Keep on Truckin" - dialogue extract performed by Steven Wright (0:16)
# "Coconut (song)|Coconut" - Harry Nilsson (3:50)
# "Home of Rock" - dialogue extract performed by Steven Wright (0:05)

==Video game==
 
A video game based on the film was released in 2006 for Personal computer|PC, Xbox (console)|Xbox, and PlayStation 2. However, the game does not feature the likeness of any of the actors with the exception of Michael Madsen. GameSpot called it "an out and out failure".  It caused controversy for its amount of violence and was banned in Australia     and New Zealand. 

== Remake ==

Kaante, a Bollywood film released in 2002, is a remake of Reservoir Dogs. The films central plot is based on Tarantinos film, and also borrows plot points from The Usual Suspects and Heat (1995 film)| Heat. Tarantino has been quoted as saying that Kaante is his favorite among the many rip-offs of his film. 

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 