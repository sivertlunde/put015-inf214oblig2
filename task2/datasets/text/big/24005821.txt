Land Without Music
{{Infobox film
| name           = Land Without Music
| image          =
| image_size     =
| caption        =
| director       = Walter Forde
| producer       = Max Schach
| writer         = Armin Robinson (story) & Fritz Koselka (story) Rudolph Bernauer (adaptation) Ernest Betts (additional dialogue) and Eric Maschwitz (additional dialogue) Marion Dix (writer) L. du Garde Peach (writer)
| narrator       =
| starring       = Jimmy Durante Oscar Straus Van Phillips 
| cinematography = John W. Boyle
| editing        = Lynn Harrison
| studio         = Capitol Film Corporation
| distributor    = General Film Distributors
| released       = 8 October 1936
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Land Without Music is a 1936 British comedy film directed by Walter Forde and starring Richard Tauber, Diana Napier and Jimmy Durante. It was made at Denham Studios.  The film was one of a number of operetta films made in Britain during the decade.

The film is also known by the alternative title Forbidden Music in the United States.

== Plot summary ==
After discovering that her state is penniless because its citizens spend their time making music instead of money, a European Grand Duchess bans music in her domains.  A New York journalist conspires with rogues to stage a concert.

== Cast ==
*Richard Tauber as Mario Carlini
*Diana Napier as Princess-Regent Maria Renata
*Jimmy Durante as Jonah J. Whistler
*June Clyde as Sadie Whistler
*Derrick De Marney as Rudolpho Strozzi
*George Hayes as Police Colonel Strozzi
*Esme Percy as Austrian Ambassador
*John Hepworth as Pedro
*Edward Rigby as The Maestro
*George Carney as Prison Warder
*Ivan Wilmot as Chief Bandit
*Robert Nainby as Minister for War
*Joe Monkhouse as Finance Minister
*Quentin McPhearson as Customs Officer
*Evelyn Ankers as a Lady of the Court (uncredited; "Whos Carlini?" sequence)

== Soundtrack == Oscar Straus. The main songs are Simple Little Melody, Smile for Me, Heaven in a Song and You must have Music, all of which were recorded by Tauber for Parlophone.

==References==
 

==Bibliography==
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

== External links ==
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 