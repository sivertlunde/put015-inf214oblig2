Lonely Hearts (2006 film)
{{Infobox Film
| name = Lonely Hearts
| image = LonelyHearts2006MoviePoster.jpg
| director = Todd Robinson
| producer = Holly Wiersma Boaz Davidson
| writer = Todd Robinson
| starring = John Travolta James Gandolfini Jared Leto Salma Hayek Laura Dern Scott Caan Alice Krige
| music = Mychael Danna Peter Levy
| editing = Kathryn Himoff
| studio = Emmett/Furla/Oasis Films|Emmett/Furla Films
| distributor = Roadside Attractions
Samuel Goldwyn Films
                 Millennium Films
| released = 21 October 2006 (Cinema of Taiwan|Taiwan) 13 April 2007 (Cinema of the United States|USA)
| runtime = 108 minutes
| country = United States
| language = English
| budget =
| gross   = $2,519,154
}}
 Todd Robinson. Lonely Hearts Killers" of the 1940s, Martha Beck and Raymond Fernandez. The story of Beck and Fernandez was also the subject of the 1970 film The Honeymoon Killers, directed by Leonard Kastle and the 1996 film Deep Crimson, directed by Arturo Ripstein.

==Plot== con man personal ads. Upon meeting Martha Beck (Salma Hayek), he and Beck decide to join forces. The two begin travelling the country, eventually murdering at least 12 women who responded to their ads. Detective Robinson (John Travolta) and Detective Hildebrandt (James Gandolfini) are the homicide detectives who bring the couple to justice. Fernandez and Beck are eventually found and arrested and executed on the electric chair.

==Other== Todd Robinson is the grandson of Elmer C. Robinson, the character played by Travolta.

==Cast==
*John Travolta as Elmer C. Robinson
*James Gandolfini as Detective Charles Hildebrandt Raymond Martinez Fernandez Martha Jule Beck
*Dagmara Dominczyk as Delphine Downing
*Bailee Madison as Rainelle Downing
*Laura Dern as Rene
*Scott Caan as Detective Reilly

==Development and production== Riverside district Fernandina and Amelia Island.   Several scenes were shot in the Davenport Hotel in Spokane, Washington. Shots of the famous fireplace and Peacock Lounge can be seen in the background.
 Orlando and Ocala, Florida, and the state of Louisiana for the film. The film crew consisted of 218 local technicians, actors, and actresses, and 833 extras who generated a $3.28 million influx to the region. 

==Release and reception==
  as Raymond Fernandez and Salma Hayek as Martha Beck.]] Clickstar on-demand shortly after its domestic release.

The film earned $188,565 in the United States and Canada, and $2,330,589 in the rest of the world, for a combined gross of $2,519,154.  Critical reaction was mixed, and the film has a 49% rating on Rotten Tomatoes. 

===DVD release=== Region One Sony on July 31, 2007, although it was available for rental in several countries since late 2006. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 