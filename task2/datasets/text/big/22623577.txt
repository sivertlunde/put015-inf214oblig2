The Stripper (film)
{{Infobox film
| name           = The Stripper
| image          = The Stripper film poster.jpg
| image_size     =
| caption        =
| director       = Franklin J. Schaffner
| producer       = Jerry Wald Curtis Harrington (associate producer) 
| writer         = William Inge (play) Meade Roberts
| narrator       =
| starring       = Joanne Woodward Richard Beymer Claire Trevor
| music          = Jerry Goldsmith
| cinematography = Ellsworth Fredericks
| editing        = Robert L. Simpson
| studio         =
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $2,175,000 
| gross          = $1,500,000 (US/ Canada) 
}}
The Stripper (1963) is a drama film about a struggling, aging actress turned stripper and the people she knows, played by Joanne Woodward.  It is based on the play A Loss of Roses by William Inge.
 Planet of The Boys from Brazil. 

William Travilla was nominated for the Academy Award for Best Costume Design, Black-and-White.

The film was first designed to be a vehicle for two Fox contract stars, Marilyn Monroe and Pat Boone, with Boone turning down the film on moral grounds. p.8  Verswijver, Leo
 Pat Boone Interview in Movies were Always Magical: Interviews with 19 Actors, Directors, and Producers from the Hollywood of the 1930s through the 1950 McFarland, 2003  

==Plot==
She dreamed of a career in the movies, but Lila Green has found little success. She joins a group of traveling entertainers and is abandoned near her Kansas hometown by manager and boyfriend Ricky Powers.

Old friend Helen Baird takes her into her home, where Helens young son Kenny becomes infatuated with Lila. Somewhat delusional, she at first sees a future for their relationship, until coming to her senses.

Ricky returns and offers Lila a job doing a striptease. In need of money, she accepts. Kenny witnesses her show and finally realizes she is not the dream girl he loved.

==Cast==
*Joanne Woodward as Lila Green
*Richard Beymer as Kenny Baird
*Claire Trevor as Helen Baird
*Carol Lynley as Miriam Caswell
*Robert Webber as Ricky Powers
*Louis Nye as Ronnie Cavendish
*Gypsy Rose Lee as Madame Olga

==References==
 

==External links==
* 
* 
* 

 

 
 

 
 
 
 
 
 
 
 
 
 
 