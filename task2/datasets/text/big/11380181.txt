Sawdust and Tinsel
 
{{Infobox film
| name           = Sawdust and Tinsel
| image          = Sawdusttinsel.jpg
| caption        = Film poster
| director       = Ingmar Bergman
| producer       = Rune Waldekranz
| writer         = Ingmar Bergman (uncredited)
| starring       = Åke Grönberg Harriet Andersson Hasse Ekman
| music          = 
| cinematography = Hilding Bladh Sven Nykvist
| editing        = Carl-Olov Skeppstedt
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}

Sawdust and Tinsel ( ) is a 1953 Swedish drama film directed by Ingmar Bergman.

==Plot==
An aging circus ringmaster visits his estranged wife to see his young sons. Meanwhile, his jealous young lover has an affair with an actor.

==Cast==
* Åke Grönberg as Albert Johansson
* Harriet Andersson as Anne
* Hasse Ekman as Frans
* Anders Ek as Frost
* Gudrun Brost as Alma
* Annika Tretow as Agda
* Erik Strandmark as Jens
* Gunnar Björnstrand as Mr. Sjuberg
* Curt Löwgren as Blom

==Reception==
It has an 100% approval rating from 9 reviews listed at Rotten Tomatoes, with an average rating of 8/10.  In 2012, it was voted one of the 25 best Swedish films of all times. 

The film was shown as part of an Ingmar Bergman Retrospective at the 61st Berlin International Film Festival in 2011.   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 