In the Year 2889
 
 
{{Infobox film
| name           = In the Year 2889
| image          = In the Year 2889 Video cover.jpg
| image_size     =
| alt            = 
| caption        = Video cover
| director       = Larry Buchanan
| producer       = Larry Buchanan Edwin Tobolowsky
| screenplay     = 
| story          = 
| writer         = Harold Hoffman Lou Rusoff
| starring       = Paul Petersen Quinn OHara Charla Doherty Neil Fletcher Hugh Feagin
| music          = 
| cinematography = Robert C. Jessup
| editing        = Larry Buchanan
| distributor    = American International Pictures|AIP-TV
| released       =  
| running time        = 80 minutes
| country        = United States English
| budget         = 
| gross          = 
}} American International had commissioned low-budget cult film auteur Larry Buchanan to direct and produce the film as a colour remake of Roger Cormans Day the World Ended (1955).   
 short story of the same title by Jules Verne and his son, Michael Verne,  {{cite journal|title=In the Year 2889 |work=The Forum| volume= VI|number= 6| date=February 1889|pages=662–677|authors=Verne, Jules & Verne, Michael, & Metcalf, Lorettus S. (Editor)|url=http://www.julesverne.ca/vernebooks/jvbk2889.html
}}  (which, unlike the film, is set in the year 1997). A nuclear war has wiped out most of Earths citizens, and the movie follows a group of survivors who take refuge from rising radiation levels, a ruthless tough guy and cannibalistic mutants in a Dallas mansion. 

==Plot==
In a post nuclear Earth, survivors are hold up in a valley and have to protect themselves from mutant human beings, and each other in some cases.

==Cast==
* Paul Petersen as Steve
* Quinn OHara as Jada
* Charla Doherty as Joanna Ramsey
* Neil Fletcher as Captain John Ramsey
* Hugh Feagin as Mickey Brown
* Max W. Anderson as Granger
* Bill Thurman as Tim Henderson
* Byron Lord as Mutant

==Production==
The AIP gave the films director Larry Buchaman the script of the 1955 Roger Corman science-fiction film Day the World Ended to use for the film. This resulted in a almost line-for-line remake. 

This was Larry Buchamans fifth Azalea Production film and he completed it on May 1967.
The film was made after the success of an earlier film Master of the World (1961). Since this was a remake the makers needed a new title and since the AIP already registered this title for a forgotten project, it was used in this film. 

==Release==
It was released in 1967 as a TV movie.
All promotional materials, including the TV Guide, post the title as "Year 2889," but the on screen credits are indeed "In the Year 2889." 

==Reception==
The film holds an extremely low score of 2.7/10 from 604 users on the Internet Movie Database.

==Home Media Release==
 

==See also==
* List of American films of 1967
* List of films in the public domain

==References==
 

==External links==
*  
*  
*  
*   synopsis at B-Movie Central

 

 
 
 
 
 
 
 
 
 


 