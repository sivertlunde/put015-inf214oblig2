The Female Bunch
{{Infobox film
| name = The Female Bunch
| image = Femalebunch.jpg
| caption = Theatrical release poster
| director = Al Adamson
| producer = Raphael Nussbaum Mardi Rustam
| writer = Jale Lockwood Brent Nimrod Raphael Nussbaum
| starring = Russ Tamblyn Jennifer Bishop Lon Chaney Jr. Alesha Lee Geoffrey Land Regina Carrol
| music = Jaime Mendoza
| cinematography = Paul Glickman
| editing = Serge Goncharoff Brent Nimrod 
| distributor = Troma Entertainment (DVD)
| released =  
| runtime = 86 minutes
| language = English
| budget =
}}
The Female Bunch is a 1969 western film. It was directed by Al Adamson and starred Russ Tamblyn and Lon Chaney Jr. (in his final acting role in a feature film). The plot centered on a group of female criminals who cause trouble around the Mexican border. The Female Bunch was shot in the summer of 1969, at Spahn Ranch during the time that it was occupied by the Manson Family.

The film was distributed on DVD by Troma Entertainment

==Plot==
 
After a string of bad times with men, Sandy tries to kill herself. Co-waitress Libby saves her and takes her to meet some female friends of hers who live on a ranch in the desert. Grace, the leader of the gang, puts Sandy through her initiation and they get on with the real job of running drugs across the Mexican border, hassling poor farmers, taking any man they please, and generally raising a little hell. Soon Sandy becomes unsure if this is the life for her, but it may be too late to get out. 

==Cast==
* Russ Tamblyn as Bill
* Jennifer Bishop as Grace
* Lon Chaney Jr. Monti
* Regina Carrol as Libby

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 

 