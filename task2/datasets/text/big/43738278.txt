Hum Hain Lajawab (1984 film)
 

{{Infobox film
| name           = Hum Hain Lajawaab 
| image          = 
| caption        =
| director       = Mohan Segal
| producer       = Jawaharlal Bafna
| writer         = 
| starring       = Kumar Gaurav Padmini Kolhapure Monty Nath Shakti Kapoor Shakti Kapoor
| music          = Rahul Dev Burman  Anand Bakshi  lyrics 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = }} Hindi language film directed by Mohan Segal and starring Kumar Gaurav, Padmini Kolhapure.

==Cast==
* Kumar Gaurav...Thakur Pawan Kumar Singh 
* Padmini Kolhapure...Dilruba 
* Monty Nath...Dilawar Singh (as Monty)
* Shakti Kapoor...Advocate B.K. Shrivastava
* Huma Khan...Jyoti (Amar Kumars wife)
* Kalpana Iyer...Alif-Laila
* Kamal Kapoor...Thakur Karan Kumar Singh
* Yunus Parvez...Thamani Seth
* Kiran Vairale...Meenakshi Meena
* Prem Kalra...(as Prem Kalara)
* Rakesh Bedi...Marorimal Karodimal Thakkatram Makhanwala M.G.M.
* Ravindra Kapoor...John Pascal (as Ravinder Kapoor)
* Vikas Anand...Diwan
* Jagdish Raj...Khan Saab
* Shivraj...Ram Kishan
* Mohan Choti...Basti Manager

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dilbar Dil Jani"
| Asha Bhosle, R. D. Burman
|-
| 2
| "Aaya Shabab Aaya"
| Lata Mangeshkar
|-
| 3
| "Main Dilruba Hoon"
| Kavita Krishnamurthy
|-
| 4
| "Main Dariya Hoon"
| Kishore Kumar
|-
| 5
| "Koi Pardesi Aaya Pardes Mein"
| Anwar
|-
| 6
| "Duniya Badal Gai Hai"
| Anwar, Lata Mangeshkar
|-
| 7
| "Dankeki Chot Se Elan Kar Diya"
| Kishore Kumar, Meena
|}

==References==

 

== External links ==
*  

 
 
 
 
 


 