Black Gold (2011 Nigerian film)
{{Infobox film
| name           = Black Gold
| caption        = 
| image          = Black Gold FilmPoster.jpeg
| director       = Jeta Amata
| producer       = Wilson Ebyie Suzanne DeLaurentiis
| writer         = Jeta Amata
| starring       = Billy Zane Tom Sizemore Hakeem Kae-Kazim Vivica A. Fox Eric Roberts Sarah Wayne Callies Michael Madsen Mbong Amata
| music          = Joel Goffin
| cinematography = James M. Costello
| editing        = Lindsey Kent
| studio         = Rock City Entertainment
| released       =  
| runtime        = 110 minutes
| country        = Nigeria
| language       = English
}}
Black Gold is a 2011 drama film co-produced and directed by Jeta Amata. One local Niger Delta communitys struggle against their own government and a multi-national oil corporation who has plundered their land and destroyed the environment. The film was reissued in 2012 with the title Black November, with 60% of the scenes reshot and additional scenes included to make the film "more current". 

== Plot ==
They hope to tell the story from the perspective of people who have lived through it. The people who have seen their land and rivers polluted by oil, the people that are struggling.

== Cast ==
* Billy Zane
* Tom Sizemore as Detective Brandano
* Hakeem Kae-Kazim as Dede
* Vivica A. Fox as Jackie
* Eric Roberts
* Sarah Wayne Callies as Kate Summers
* Michael Madsen
* Mickey Rourke as Craig Hudson
* Mbong Amata as Ebiere (as Mbong Odungide)
* Shanna Malcolm as Mourner/Protester

== References ==
 

==External links==
*  

 
 
 
 
 
 

 