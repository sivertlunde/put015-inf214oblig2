Strähl
{{Infobox film name = Strähl image =  Strähl.jpg year = 2004  runtime = 83 language = Albanian
|director = Manuel Flurin Hendry writer = David Keller, Michael Sauter producer = Susann Rüdinger music = Michael Sauter  cinematography = Filip Zumbrunn
}}
 2004 Switzerland|Swiss film directed by Manuel Flurin Hendry. The plot takes place in Zürich, at the notorious Langstrasse.

== Plot ==

Lonesome and medicament addict narcotics detective Herbert Strähl (Roeland Wiesnekker), who hardly can handle his own life, wants to clean up the Langstrasse area. Strähl often watches Berisha (Adem Kicai), the head of a drug trafficking ring, leaving his limousine and entering a dubious building, but he isn’t able to arrest him, due to the lacking of an evidence (Berisha never touches the drugs by himself).

Strähl, who hides his depression and craving for love behind rage, clamor and stimulant drugs, has arguments with his chief Brunner (Max Rüdlinger) and with his co-workers, e.g. with Ruedi Lautenschlager (Mike Müller), who is rather dull and often visits brothels. When Beat (Raphael Clamer), a former member of the Seepolizei (Police unit on lake), joins the team, Strähl thinks, the new blood was too calm and not tough enough.

During a house search, a junkie named René Wehrli (Manuel Löwensberg) falls out of the window. Strähl gets suspended from police service. Moreover, René and his girlfiriend Carol “Caro” Hertig (Johanna Bantzer) blackmail Strähl for smart money.  The drug addict couple buys a lot of articles in a kiosk, and Strähl has to pay for them (René also fills out a “Toto”-ticket (sports betting) there, and Strähl positively influences him doing it). Despite that, René doesn’t want to take back his inculpatory statement – René demands a huge amount of heroin from Strähl.

Strähl catches the dealer Beko (Nderim Hajrullahu), who is related to Berisha’s drug mafia, and presses him to cooperate. Beko shows a lot of hidden drugs to Strähl. Strähl uses a part of the drug depots heroine to satisfy René. René, Caro and Strähl drive to the police station, where René wants to deny his statements to Strähl, while the others wait in the car. René stays in for a long time, and Strähl gets curious about what happened in the building. He goes in, too – and finds René dead in the restrooms.

Strähl and Caro smoke drugs on a foil at Strähl’s. Suddenly, the police enter the flat and find the heroin. The delinquents are escorted away in a car, but Strähl manages to open Caro’s door to let her escape (Her plastic-handcuffs can be cut off later).
 CHF winning “Toto”-ticket. Caro, on the run, now wants to encash the ticket as fast as possible. But the kiosk-woman isn’t allowed to pay out such a high amount of cash (the ticket has to be sent in). So Caro asks Beko, if he knew someone, who could exchange the ticket against cash within an hour. The mafia agrees (They offer 80’000 Swiss franc|CHF) and the parties meet at a small park. The situation escalates and Caro throws a handgrenade (that René once took from a dead junkie). The grenade doesn’t go off.

Finally Strähl succeeds in arresting Berisha, thanks to a hint by new blood Beat – hiding a serious amount of confiscated drugs in Berisha’s trunk.

==Cast==
*Roeland Wiesnekker: Herbert Strähl
*Johanna Bantzer: Carol Hertig
*Nderim Hajrullahu: Beko Mussafr
*Mike Müller: Ruedi Lautenschlager

== External links ==
*  
*  

 
 
 
 