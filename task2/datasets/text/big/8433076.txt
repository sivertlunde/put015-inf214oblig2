Karmic Mahjong
{{Infobox film
| name           = Karmic Mahjong
| image          = KarmicMahjong.jpg
| image_size     = 
| caption        = 
| director       = Wang Guangli
| producer       = Cory Vietor Zhu Lei
| writer         = Wang Guangli Huang Jinguang
| narrator       = 
| starring       = Francis Ng Cherrie Ying John Thompson
| cinematography = Lu Yuqing
| editing        =  Adam Kerby Ching-Song Liao	 	
| distributor    = 
| released       = China: April 20, 2006 Hong Kong: May 25, 2006
| runtime        = 98 min
| country        = China Mandarin Sichuanese Sichuanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 2006 Cinema Chinese comedy film directed by Wang Guangli. It stars Francis Ng as a mechanic from Chengdu plagued by bad luck and Cherrie Ying as a young woman who appears to share the same affliction. The film also features cameos by the prominent Chinese directors Wang Xiaoshuai and Jia Zhangke. 
 Hong Kong tradition of action-comedies. 
 
==Cast==
* Francis Ng - Wu Yu-Chuan, a down-on-his luck mechanic living in Sichuan. 
* Cherrie Ying - Jia, a mother who seems to share Wus bad luck. 
* Na Wai - Yin, a mobster and Wus childhood friend. Paul Chun - Qin, Yins mobster boss.
* Liu Yiwei - Master Liu as a blind fortune teller. Liang Jing - Lai, Wus wife. 
* Jia Zhangke - a mobster.
* Wang Xiaoshuai - a cop.

== Notes ==
 

==External links==
* 
* 
*  at the Chinese Movie Database

 
 
 
 
 
 
 

 
 