El Fin
 
{{Infobox film
| name           = El Fin
| image          =
| alt            = 
| caption        = Movie poster
| director       = Miguel Gómez
| producer       = Dennis Gómez Andrea Truque Miguel Gómez
| writer         = Miguel Gómez Antonio Chamu Charlie VanVleet
| starring       = Pablo Masís Kurt Dyer Álvaro Marenco Valeska Vinocour
| music          = Federico Dörries
| cinematography = 
| editing        = Miguel Gómez
| studio         = 
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Costa Rica
| language       = Spanish
| budget         = $80,000
| gross          = 
}}
El Fin is a 2012 film written and directed by Costa Rican film director Miguel Gómez, and co-produced with his brother Dennis Gómez. Pablo Masís and Kurt Dyer play the two leading characters. 
 world is at its end, so they take on a journey to enjoy their last day on Earth.

El Fin is the third film by Gómez, his two earlier films being El Cielo Rojo (2008) and El Sanatorio (2010). The latter won a Golden Skull at the Morbid Film Fest  in Mexico and received very positive reviews. El Fin will be out on theaters by February 24, 2012. 

==Plot==
Depressed by the recent death of his parents and losing his job, Nico; an ordinary 26 year old, decides to take refuge in his house for six months. Until that feared day when the media announces the end of the world is soon coming, caused by a gigantic meteorite crash, Nico is convinced by his close friend Carlos to come with him to Nosara, a beautiful beach on the Pacific coast, the only place where they were ever happy. So this is where their journey begins, where they will find a meaning to their lives, along with other interesting characters on the way.

==Cast==
* Pablo Masís as Nico
* Kurt Dyer as Carlos
* Álvaro Marenco as Marcos
* Valeska Vinocour as Andrea

==Production== Heredia and the complicated destroyed car scene was filmed in San Diego, Tres Ríos. The movie was originally coming out October 29, 2011, but it was postponed in order to resolve technical issues and improve the logistic matters of production.

==Mórbido Film Fest==
The film was presented on October 2011 as a "work in progress" at the Morbid Film Fest of Fantasy and Horror in Mexico, along with other films such as The Woman (United States), Penumbra (Argentina) and Emergo (Spain), which won first place for the peoples choice category leaving El Fin on second place. The film did receive positive reviews from several websites which attended the Festival, such as Twitch Film. 

==References==
 

 
 