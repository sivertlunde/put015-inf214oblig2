Baanaadi
{{Infobox film
| name           = Baanaadi
| image          = Kannada film Baanaadi poster.jpg
| caption        = Film poster
| director       = Nagaraj Kote
| producer       = Dhruti M. Nagaraju
| writer         = Nagaraj Kote
| screenplay     = Nagaraj Kote
| based on       =  
| starring       = Praful Vishwakarma H. G. Dattatreya Rajesh Nataranga
| music          = Karthik Sharma
 cinematography = Sabha Kumar

| editing        = Mohan Kamakshi
| studio         = Dhruti Cinema
| distributor    = 
| released       =  
| runtime        = 146 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Kannada cinema.   

== Cast ==
 
* Master Praful Vishwakarma as Kishore
* H. G. Dattatreya
* Rajesh Nataranga as Avinash
* Kumari Dhruthi as Pallavi
* Abhinaya as Kusuma
* Sringeri Ramanna as Thathayya
* Jayashree Raj as Girija
* Venkatachala as Muthyappa
* T. S. Nagabharana as Ashwath Kumar
* Mimicry Gopi as Duduma 
* Master Yashwanth Kote as Hanumya
* Master Madhusudan
* Bank Janardhan
* Ramesh Pandith
* Mugu Suresh
* Baraguru Ramachandrappa
* Manasi Sudheer
* Mohan Juneja
 

==Production==
Usiru, a novel written by Nagaraj Kote in the 1990s, deals with the upbringing of children in the current era. Deciding to direct a film based on the novel, Kote launched the film in April 2014, having signed Praful Vishwakarma, Rajesh Nataranga and H. G. Dattatreya to play characters of three generations; a young boy, his father and grandfather.  The role of Prafuls mother was played by Anubhava, who was pregnant during the filming stages.  Filming completed in July 2014.   

==Soundtrack==
{{Infobox album
| Name        = Baanaadi
| Type        = Soundtrack
| Artist      = Karthik Sharma
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 26 July 2014
| Recorded    = 2014 Feature film soundtrack
| Length      = 
| Label       = Lahari Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Karthik Sharma composed the background score for the film and music for five soundtracks in the film. The lyrics were written by Nagaraj Kote and M. N. Vyasa Rao. The track "Henda Hendthi" was taken from one of G. P. Rajarathnams works, to which the music was composed by Raju Ananthaswamy.  Another track "Yaaru Baruvaru" was taken from the works of Purandara Dasa, a Carnatic music composer who lived in the 16th century.  The album consists of six soundtracks.  It was released on July 26, 2014, in Bangalore. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| music_credits = yes
| title1 = Banadi Banadi
| lyrics1 = Nagaraj Kote
| music1 = Karthik Sharma
| extra1 = Chorus
| length1 = 
| title2 = Comingo Comingo	
| lyrics2 = Nagaraj Kote
| music2 = Karthik Sharma
| extra2 = Chorus
| length2 = 
| title3 = Bhoomi Ninna Thaayi
| lyrics3 = M. N. Vyasa Rao
| music3 = Karthik Sharma Chithra
| length3 = 
| title4 = Henda Hendthi
| lyrics4 = G. P. Rajarathnam
| music4 = Raju Ananthaswamy
| extra4 = Raju Ananthaswamy
| length4 = 
| title5 = Ellinda Bandyappa
| lyrics5 = M. N. Vyasa Rao
| music5 = Karthik Sharma
| extra5 = Venkatachala
| length5 = 
| title6 = Yaaru Baruvaru
| lyrics6 = Purandara Dasa
| music6 = Karthik Sharma
| extra6 = Ravindra Soragavi
| length6 = 
}}

== Critical reception ==
Upon theatrical release, the film received positive reviews from critics. B. S. Srivani of Deccan Herald felt that the film was successful in "conveying the message quite effectively". She concluded wroting praises of the acting performances and the music in the film.  G. S. Kumar of The Times of India reviewed the film and wrote, "Director Nagaraja Kote has chosen a topic with a social message and made best use of Hagalu Vesha. Their performance blends well with the story." He concluded giving special mention to the Dattatreyas performance and the films cinematography. 

==References==
 

==External links==
*  

 
 
 
 
 