Paradise Lost: The Child Murders at Robin Hood Hills
{{Infobox film
| name           = Paradise Lost:  Child Murders at Robin Hood Hills
| image          = Paradise Lost Dvd.jpg
| caption        = DVD cover
| director       = Joe Berlinger and Bruce Sinofsky
| producer       = 
| writer         = 
| starring       = Jessie Misskelley, Damien Echols and Jason Baldwin (the West Memphis Three)
| music          = Metallica
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Home Box Office (HBO)
| released       =  
| runtime        = 150 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  $272,750 
}}
Paradise Lost: The Child Murders at Robin Hood Hills is a 1996   and   .

==Description==
The film documents the events from the arrests of Misskelley, Echols and Baldwin for the murders of Christopher Byers, Michael Moore and Stevie Branch. Their naked and hogtied bodies were discovered in a ditch in a wooded area of West Memphis, Arkansas, known as "Robin Hood Hills."

Filmmakers Joe Berlinger and Bruce Sinofsky interview numerous people connected with the case, including the parents of the victims, the parents of the accused, members of the West Memphis Police Department (WMPD) and all the defendants involved in the trial. Berlinger and Sinofsky are not filmed themselves, and the dialogue is provided by the interviewee, rather than using a "Q & A" format.

The film starts with an introduction to the case, before moving on to the arrests of the three teenagers. Much of the community, including the detectives and the victims parents believe the murders were committed by the teenagers as part of a Satanic ritual. The community is shown to be politically conservative and strongly Evangelical Christian. Because Misskelley had provided police with a confession, his trial is separated from that of Damien and Jason, and is covered in the first half of the film.

==Trials coverage==
The first trial to be covered in the film is that of Misskelley, a trial which was severed from those of Echols and Baldwin since it was Jessie who submitted a confession. Emphasis is placed on the fact that there is a strong possibility that the confession was coerced. Interviews are conducted with Jessies family and friends, Misskelley himself and his attorney Dan Stidham. Misskelley is sentenced to life in prison.

Part two of film documents the trials of Echols and Baldwin. Like the coverage of Misskelleys trial, there are interviews with both defendants, their attorneys and their families. The families of the victims also share their views.

During the course of the filming, John Mark Byers, the stepfather of one of the victims (Christopher Byers), gives the filmmakers a knife which has blood in the hinge.  The filmmakers turn the knife over to police, who examine it; the DNA is similar to that of himself and the boy but the evidence is nonetheless inconclusive since the DNA evidence produced was fragmented and can not provide concrete links.

==Production== Some Kind of Monster about Metallica. 

==Reception== Siskel and Ebert praising the movie. {{cite news | url=http://www.rogerebert.com/reviews/paradise-lost-the-child-murders-at-robin-hood-hills-1996 | title=Paradise Lost: The Child Murders At Robin Hood Hills 
 |publisher=  (2000), which suggests that further evidence was missed or suppressed and attempts to prove Echols innocence. Followed by   (2011).

==Aftermath==
Following a successful decision in 2010 by the Arkansas Supreme Court regarding recently uncovered DNA evidence, the West Memphis Three reached a deal with prosecutors. On August 19, 2011, they entered Alford pleas, which allow them to assert their innocence and were sentenced to time served, effectively freeing them.

==See also==
 
*West of Memphis - a 2012 documentary also covering the West Memphis Three
*Devils Knot (film)|Devils Knot - a 2013 feature film dramatization of the case, directed by Atom Egoyan.

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 