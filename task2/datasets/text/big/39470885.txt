Happy Ghost III
 
 
{{Infobox film
| name           = Happy Ghost III
| image          = Happy Ghost III.jpg
| border         = yes
| alt            = 
| caption        = Hong Kong film poster for Happy Ghost III
| film name = {{Film name traditional   =開心鬼撞鬼 simplified    =开心鬼撞鬼 pinyin        = kāi, xīn, guǐ, zhuàng-chuáng, guǐ jyutping      = hoi1, sam1, gwai2, cong4-zong6, gwai2}}
| director       = Johnny To Raymond Wong   
| writer         = Raymond Wong 
| starring       = {{plainlist|*Raymond Wong
*Maggie Cheung
*Fennie Yuen
*Charine Chan}}
| music          = Joseph Koo 
| cinematography = Bob Thompson 
| editing        = David Wu  Cinema City 
| distributor    = Golden Princess 
| released       =  
| runtime        = 96 minutes 
| country        = Hong Kong 
| language       = Cantonese 
| budget         = 
| gross          = HK$ 15,339,277
}} Raymond Wong Bak Ming) takes his pregnant wife to the wrong hospital. Pan Han is given one month to find a new body to assume her reincarnation in, and decides in the meantime to harass Sam Kwai. Kwai eventually summons the Happy Ghost to help him out.

==Production==
Happy Ghost III was the first film director Johnnie To had worked for Cinema City and his first film since The Enigmatic Case (1980).  To had previously been working in television after the box office failure of The Enigmatic Case.  To found the film easy to approach as he did not have to write the script and was told he was not allowed to change it by Cinema City rules.  Tsui Hark appears in the film as the Godfather and also provides the film with the special effects. 

==Release==
Happy Ghost III was a hit for Cinema City and grossed a total of HK$ 15,339,277 and was the 11th-highest grossing film of the year in Hong Kong.  The film grossed less than its two prequels Happy Ghost and Happy Ghost II, which earned a total of HK$ 17.4 and HK$16.6 respectively. OBrien, 2003. pg. 42  The film was followed by Happy Ghost 4, which was directed by Clifton Ko. 

==Notes==
 
===References===
* {{Cite book
 | last= OBrien
 | first= Daniel 
 | title= Spooky Encounters: A Gwailos Guide to Hong Kong Horror
 |publisher= Headpress
 |year= 2003
 |isbn= 1900486318
 |url=http://books.google.ca/books?id=8XehfPFuFY8C
 |accessdate=23 May 2013
}}
* {{Cite book
 | last= Morton
 | first= Lisa
 | title= The Cinema of Tsui Hark McFarland
 |year= 2009
 |isbn= 0-7864-4460-6
 |url=http://books.google.ca/books?id=MiulRecfarcC&printsec=frontcover
 |accessdate=22 January 2011
}}
* {{Cite book
 | last= Teo
 | first= Stephen
 | title= Director in Action: Johnnie To and the Hong Kong Action Films
 |publisher= Hong Kong University Press
 |year= 2007
 |isbn= 9622098401
 |url=http://books.google.ca/books?id=sePeA0F9J9QC
 |accessdate=22 January 2011
}}

==See also==
*Johnnie To filmography
*List of Hong Kong films of 1986

==External links==
* 
*  

 
 

 
 
 
 
 
 