Vaazhkai (film)
{{Infobox film
| name           = Vaazhkai
| image          =
| image_size     =
| caption        =
| director       = C. V. Rajendran
| producer       = Chitra Lakshmanan
| writer         = Mohan Kumar
| screenplay     = Panchu Arunachalam Ambika Jaishankar M. N. Nambiar
| music          = Ilayaraja
| cinematography = T. S. Vinayagam
| editing        = Chandran - Muthu
| studio         = 
| distributor    = Gayathri Films
| released       =  
| country        = India Tamil
}}
 1984 Cinema Indian Tamil Tamil film,  directed by  C. V. Rajendran and produced by Chitra Lakshmanan. The film stars Sivaji Ganesan, Ambika (actress)|Ambika, Jaishankar and M. N. Nambiar in lead roles. The film had musical score by Ilayaraja.     The film was a remake of Hindi film Avtaar.

==Cast==
*Sivaji Ganesan Ambika
*Jaishankar
*M. N. Nambiar
*Y. G. Mahendra
*Thengai Srinivasan Manorama
*V. K. Ramasamy (actor)|V. K. Ramasamy
*Silk Smitha
*Y. Vijaya

==Soundtrack==
The music was composed by Ilaiyaraaja.  The song "Mella Mella" is one of the famous songs of Ilayaraja and the instrumental theme from this song remains one of the popular theme music in Tamil films.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ennarumai Selvangal || S. P. Balasubrahmanyam, S. P. Sailaja, Deepan Chakravarthy || Pulamaipithan || 04:21
|-
| 2 || Kaalam Maaralaam || S. P. Balasubrahmanyam, Vani Jayaram || Vairamuthu || 04:22
|-
| 3 || Kattikollava || Jayachandran, Malaysia Vasudevan, S. P. Sailaja || Vairamuthu || 04:17
|-
| 4 || Manamey Nee || Ilaiyaraaja || Muthulingam || 04:14
|- Susheela || MG. Vallabhan || 04:02
|-
| 6 || Yaavum Neeyappaa || Malaysia Vasudevan || Panchu Arunachalam || 04:22
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 