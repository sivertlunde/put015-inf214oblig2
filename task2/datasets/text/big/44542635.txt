Gandharvagiri
{{Infobox film 
| name           = Gandharva Giri
| image          =  
| caption        = 
| director       = N. S. Dhananjaya (Datthu)
| producer       = N. S. Mukund H. S. Chandru Smt B. A. Anasuya Smt Veena Nagaraj
| story          = Smt Saisuthe (Based on Novel)
| writer         = M. Narendra Babu (dialogues)
| screenplay     = N. S. Dhananjaya (Datthu) Vishnuvardhan Aarathi J. V. Somayajulu Anupama
| music          = Upendra Kumar
| cinematography = S. Ramachandra
| editing        = V. P. Krishna
| studio         = Nataraj Pictures
| distributor    = Nataraj Pictures
| released       =  
| runtime        = 129 min
| country        = India Kannada
}}
 1983 Cinema Indian Kannada Kannada film, directed by N. S. Dhananjaya (Datthu) and produced by N. S. Mukund, H. S. Chandru, Smt B. A. Anasuya and Smt Veena Nagaraj. The film stars Vishnuvardhan (actor)|Vishnuvardhan, Aarathi, J. V. Somayajulu and Anupama in lead roles. The film had musical score by Upendra Kumar.  

==Cast==
  Vishnuvardhan
*Aarathi
*J. V. Somayajulu
*Anupama
*Manu
*Sundar Krishna Urs
*Musuri Krishnamurthy
*Rajanand Leelavathi
*Sathyabhama Aroor
*Sundaramma
*Padma Archana
*Bhanumathi
* M. S. Umesh
*Master Jayasimha
*Master Goutham
*Master Ravi Siddarth
*Prakash Kunabev
*T. K. Babu
*Sathyapriya in Guest Appearance
*Mukund in Guest Appearance
*Vijay in Guest Appearance
*M. Narendra Babu in Guest Appearance
*Siddalinge Gowda in Guest Appearance
 

==Soundtrack==
The music was composed by Upendra Kumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || O Nalle || S. P. Balasubrahmanyam, S. P. Sailaja || MN. Vyasarao || 04.40
|-
| 2 || Ammanu Nudida || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.24
|-
| 3 || Gandharvagiriyali || S. Janaki|Janaki, S. P. Sailaja, S. P. Balasubrahmanyam || Vijaya Narasimha || 04.14
|- Janaki || Vijaya Narasimha || 04.07
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 