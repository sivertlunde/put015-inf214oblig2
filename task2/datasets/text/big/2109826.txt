American Desi
{{Infobox film
| name           = American Desi
| image          = American Desi DVD.jpg
| caption        = American Desi DVD Cover
| director       = Piyush Dinker Pandya
| producer       = Deep Katdare   Cyrus E. Koewing   Gitesh Pandya   Piyush Dinker Pandya
| eproducer      =
| aproducer      =
| writer         = Piyush Dinker Pandya Anil Kumar   Sunita Param   Aladdin Ullah   Eric Axen   Sanjit De Silva   Sunil Malhotra   Ami Shukla   Krishen Mehta   Smita Patel   Bina Sharif   Tirlok Malik   Ravi Khanna
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 2001
| runtime        = 100 min
| country        = United States
| awards         = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} South Asian American actors.  Also featured is an a cappella version of Nazia Hassans Urdu classic "Aap Jaisa Koi", sung by Penn Masala and originally from the movie Qurbani (1980 film)|Qurbani.  The term Desi in the title refers to the peoples and cultures of the Indian subcontinent.            

==Plot==
An Indian American|American-raised Indian, Krishnagopal Reddy (or "Kris" as he prefers to be called, to distance himself from his heritage) finds out to his dismay that his University of Pennsylvania roommates are all Indian.  He does not associate with the Indian culture that his parents and family have pushed upon him and prefers to be as American as possible. His roommates include Ajay (Kal Penn), an African-American idolizing desi; Jagjit, who loves art but studies engineering to please his father; and Salim, who is very traditional and conservative, feeling that Indian-American girls are too Westernized to make good wives.

Kris meets Nina, a girl he immediately falls for, and is surprised to find out that not only is she Indian, but she is also quite involved with Indian culture. The movie revolves around him making mistakes and trying his best to win Nina over, from joining the Indian Students Association to be near her, to learning how to perform a Dandiya Raas. Thus, Kris eventually begins to enjoy the company of his roommates, all of whom put together their knowledge and skill to help Kris impress Nina through various ways involving the Indian culture, which he eventually comes to love as well.   

==Cast==
*Deep Katdare as Krishnagopal "Kris" Reddy
*Purva Bedi as Nina Shah
*Ronobir Lahiri as Jagjit Singh
*Rizwan Manji as Salim Ali Khan
*Kal Penn as Ajay Pandya Anil Kumar as Rakesh Patel
*Sunita Param as Farah Saeed
*Aladdin Ullah as Gautam Rao
*Eric Axen as Eric Berger
*Sanjit De Silva as Chandu
*Sunil Malhotra as Hemant
*Ami Shukla as Priya
*Krishen Mehta as Mr. Reddy (Kriss father)
*Smita Patel as Mrs. Reddy (Kriss mother)
*Bina Sharif as Mrs. Saeed
*Tirlok Malik as Mr. Saeed
*Ravi Khanna as Mr. Singh

==References==
 

==External links==
*  - Official site
*  

 
 
 
 
 
 
 
 