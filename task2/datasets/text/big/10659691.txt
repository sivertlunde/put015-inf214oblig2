A Difficult Life
{{Infobox film
| name           = A Difficult Life (Una vita difficile)
| image          = Una vita difficile.jpg
| imdb_id        = 
| writer         = Rodolfo Sonego
| starring       = Alberto Sordi Lea Massari Franco Fabrizi Claudio Gora
| director       = Dino Risi
| producer       = Dino De Laurentiis
| music          = Carlo Savina
| distributor    =
| released       = 19 December 1961
| runtime        = 118 min
| language       = Italian
| budget         =
}}

A Difficult Life (Italian: Una vita difficile) is a Commedia allitaliana or Italian-style comedy film directed by Dino Risi in 1961 in film|1961.

==Plot==
Silvio (Alberto Sordi) is an Italian partisan, he and his companions belong to Italian resistance movement fighting against the fascists and the Nazi Germany|Nazis. They are at Lake Como. He is helped by Elena (Lea Massari). He spends three months hiding in Elenas grandfathers mill. They fall in love. Silvio is an idealist, probably communist, journalist writer. He goes back to war. 

The film spans the story of Italy from 1944 to 1960, from the times of poverty in World War II to the birth of the Italian Republic, the elections and Silvios ideals of the Italian Communist Party, his stay in jail, his disappointments, his crises with Elena, his attempts to get his works published and to sell his film scripts in Cinecittà to Alessandro Blasetti, Silvana Mangano and Vittorio Gassman (they appear as themselves).

==Cast==

* Alberto Sordi: Silvio Magnozzi
* Lea Massari: Elena Pavinato
* Franco Fabrizi: Franco Simonini
* Lina Volonghi: Amalia Pavinato 
* Claudio Gora: Commendator Bracci
* Antonio Centa: Carlo
* Loredana Nusciak: Giovanna
* Daniele Vargas: Marquis Capperoni
* Franco Scandurra: President of the examination board
* Mino Doro: Ragana

== Cult scenes ==
Some scenes are famous in the history of   shoot.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 
 