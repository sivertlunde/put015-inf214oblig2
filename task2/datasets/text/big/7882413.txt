Aurore (film)
{{Infobox film name = Aurore image = Aurore.jpg caption = Aurore movie poster director = Luc Dionne producer = Denise Robert  Daniel Louis writer =  Luc Dionne starring = Marianne Fortier   Serge Postigo   Remy Girard   Stéphanie Lapointe   Yves Jacques   Helene Bourgeois-Leclerc   Michel Forget   Sarah-Jeanne Labrosse |
 | cinematography = Louis de Ernsted
 | music = Michel Cusson
 | editing = Isabelle Dedieu
 | distributor = Alliance Atlantis Vivafilm
 | released = July 8, 2005  (Quebec)  
 | runtime = 115 minutes
 | country = Canada French
 | budget = $7 million CDN  
 }}

Aurore is a   (Little Aurore: The Child Martyr).

==Synopsis==
Aurore Gagnon, born in 1909 to Arzelie Caron and Telesphore Gagnon, is the second child of the couple. During the first nine years of her life, Aurore enjoys a happy life.

Then, during the fall of 1917, Aurores mother develops tuberculosis. She is brought to hospital for several months and doctors conclude she will never recover. Plans are made to give custody of Aurore and her sister Marie-Jeanne to Telesphore and his late wifes cousin, Marie-Anne (often simply referred to as Telesphores cousin in the movie), with whom he had fallen in love. During a visit to Marie-Anne before she goes into hospital, Aurores mother finds one of Marie-Annes children locked inside a wooden structure. This alerts her that Marie-Anne might not treat Telesphores children well. At the same time, Aurore sees her father kissing Marie-Anne; the girl criticizes her father for not caring enough about his ill wife.

Aurores mother dies in 1918, and Telesphore and Marie-Anne marry immediately after the funeral. This is the start of an unhappier, more difficult life for Aurore, as her stepmother and father begin at once to mistreat her. When two of Marie-Annes children die, some villagers blame her for their deaths.
 2x4 with nails, and burning her with a burning steel hook. Her father also abuses her by slapping her and striking her with an axe handle. She is also locked in an isolated room with little food. After the attack with the nails, Oreus, the peace judge of the town, forces her parents to send Aurore to hospital and suspects that she has been beaten. When Exilda Lemay (played by Francine Ruel), discovers Aurore with severe wounds all over her body, she immediately alerts Oreus about the situation. Oreus goes to Quebec to discuss the issue with a crown attorney.

When the peace judge and several others, including doctors, arrive at Aurores home, it is too late: she collapsed on the stairs and was attacked again by Marie-Anne with a 2x4. Doctors are unable to save Aurore, who passes away from blood poisoning.
 life in death by hanging for second degree murder. She is then given a life sentence, but health issues force her to leave the jail, and she later dies from breast and brain cancer. Father Leduc, whom Oreus blames for his lack of action on the case, kills himself with explosives.

==Cast==
===Main cast===
* Aurore Gagnon (10 years old) by Marianne Fortier
* Télesphore Gagnon by Serge Postigo
* Marie-Anne Houde by Hélène Bourgeois-Leclerc
* Marie-Anne Caron by Stéphanie Lapointe
* Oreus Mailhot by Rémy Girard
* Bishop Leduc by Yves Jacques
* Marie-Jeanne Gagnon by Sarah-Jeanne Labrosse

===Secondary Cast===
* Aurore Gagnon (6 years old) by Alice Morel-Michaud
* Arzélie Caron by Monique Spazini
* Nérée Caron by Michel Forget
* Exilda Lemay by Francine Ruel
* Adjutor Gagnon by Michel Barrette
* Alphonse Chandonnet by Gaston Lepage
* Arcadius Lemay by Luc Senay

==Awards==
The movie was nominated for seven awards, including five Genie Awards and two Jutra Awards, including Best Adapted Screenplay and Best Actress for Hélène Bourgeois-Leclerc. 

==See also==
*Aurore Gagnon

==Footnotes==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 