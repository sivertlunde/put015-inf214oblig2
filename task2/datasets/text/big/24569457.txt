August 32nd on Earth
 
{{Infobox film
| name           = August 32nd on Earth
| image          = August 32nd on Earth.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Denis Villeneuve
| producer       = Roger Frappier
| writer         = Denis Villeneuve
| starring       = Pascale Bussières Alexis Martin
| music          =
| cinematography = André Turpin
| editing        = Sophie Leblond
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = Canada
| language       = French
| budget         =
}}

August 32nd on Earth ( , and also known as 32nd Day of August on Earth) is a 1998 Canadian drama film directed by Denis Villeneuve. It was screened in the Un Certain Regard section at the 1998 Cannes Film Festival.   

==Cast==
* Paule Baillargeon – Doctor in hospital
* Emmanuel Bilodeau – Philippes best friend
* Pascale Bussières – Simone Prévost
* R. Craig Costin – Hired car clerk
* Joanne Côté – Monica
* Frédéric Desager – Stéphane
* Estelle Esse – Leather shop clerk
* Lee C. Fobert – Car driver – Salt Lake City
* Venelina Ghiaourov – Nurse
* Richard S. Hamilton – Taxi driver
* Marc Jeanty – Janvier
* Alexis Martin – Philippe
* Evelyne Rompré – Juliette
* Ivan Smith – Doctor
* Serge Thériault – Car driver
* Jim Levesque – Customer in café

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 