Challenging Impossibility
{{Infobox film
| name           = Challenging Impossibility
| image          = ChallengingImpossibility_small_flyer.jpg
| caption        =
| director       = Natabara Rollosson and Sanjay Rawal
| producer       = Sanjay Rawal, Mridanga Spencer and Natabara Rollosson
| music          = Parichayaka Hammerl
| editing        = Sanjay Rawal
| cinematography = Sanjay Rawal
| released       =  
| runtime        = 28 minutes
| country        = United States
| language       = English
}} weightlifting and performing feats of strength using the power of meditation. His lifts were featured on newscasts worldwide, inspiring people to transcend their personal limitations and to abandon their concepts of the restrictions of physical age. Directed by Natabara Rollosson and Sanjay Rawal. The film was an Official Selection of the 2011 Tribeca Film Festival and premiered April 22, 2011.

==Synopsis== York College.

Starting out with basic dumbbells (40&nbsp;lbs) in 1985, Sri Chinmoy quickly progressed to lift cars, planes and elephants in an effort to show how meditation can develop inner peace that can translate to tangible outer power and strength. Along Sri Chinmoy|Chinmoy’s journey of “self-transcendence” he honored and inspired individuals who have uplifted humanity by literally lifting them on a platform above head in an award program titled "Lifting Up the World with a Oneness-Heart". From its creating in 1987, Sri Chinmoy lifted over 7000 people, including Nelson Mandela,  Desmond Tutu   Muhammad Ali, Sting (musician)|Sting, Eddie Murphy, Susan Sarandon, Roberta Flack, Yoko Ono, Jeff Goldblum, and Richard Gere.   
 Chinmoy performed his own strenuous feats to spread his message of world harmony and inner peace,    to inspire people to transcend their personal limitations and to abandon their concepts of the restrictions of physical age. Appearances in the film also include Mikhail Gorbachev, Pope John Paul II, Mother Teresa, and Princess Diana.

==Premiere==
The film premiered at the 2011 Tribeca Film Festival in New York on April 22, 2011 in a new bracket of inspiring short documentaries in which "positive thinking prevails".  At the premier, Carl Lewis described how Sri Chinmoy encouraged him to transcend his own personal limitations. "Sri Chinmoy inspired me and hes followed my career, actually hes one of the few people that had been to all four of my Olympic Games...when you were in his presence it was always positive, you never heard anything negative, he was always inspiring you to be better than yourself and think about other people."  Also in attendance for the premier were Frank Zane, Hugo Girard, Nadine Tremblay, and Wayne DeMilia, who all appear in the film.

==Exhibit==
A pop-up exhibition of Sri Chinmoy|Chinmoy’s weightlifting equipment that appeared in the film. The exhibit coincided with the premier of the film in Tribeca from April 15 to May 2, 2011. 

On display were numerous custom-made machines, such as a standing calf raise machine which Sri Chinmoy used to set a record of lifting 2,400 pounds, "the equivalent of lifting an elevator packed full of people".  Among the machines was also the apparatus Sri Chinmoy used to lift people in his "Lifting Up the World with a Oneness-Heart" program, which he used to honor individuals of inspiration by lifting them overhead on a platform.

==References==
 

== External links ==
*   , vimeo.com
*  
*  
*  
*  

 
 
 
 
 