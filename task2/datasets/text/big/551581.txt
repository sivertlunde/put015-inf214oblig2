Born Free
 
 
 
{{Infobox film
| name           = Born Free
| image          = Born-Free-Poster.jpg
| image size     =
| caption        = Theatrical release poster James Hill Sam Jaffe Paul Radin
| screenplay     = Lester Cole
| based on       =  
| starring       = Virginia McKenna Bill Travers John Barry
| cinematography = Kenneth Talbot
| editing        = Don Deacon
| studio         = Shepperton Studios
| distributor    = Columbia Pictures
| released       =   (UK) (Royal Film Performance)   (US)
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| gross          = $3.6 million (est. US/ Canada rentals) 
}}
 British drama Joy and blacklisted Hollywood James Hill Sam Jaffe John Barry, won numerous awards.



==Plot== Rotterdam Zoo, while Elsa the Lioness (the smallest of the litter) remains with Joy. When Elsa is held responsible for stampeding a herd of elephants through a village, John Kendall, Adamsons boss gives the couple three months to either rehabilitate Elsa to the wild, or send her to a zoo. Joy opposes sending Elsa to a zoo, and spends much time attempting to re-introduce Elsa to the life of a wild lion in a distant reserve. At last, Joy succeeds, and with mixed feelings and a breaking heart, she returns her friend to the wild. The Adamsons then depart for their home in England; a year later, they return to Kenya for a week, hoping to find Elsa. They do, and happily discover she hasnt forgotten them, and is the mother of three cubs.

==Cast==
* Virginia McKenna as Joy Adamson
* Bill Travers as George Adamson
* Geoffrey Keen as John Kendall
* Peter Lukoye as Nuru
* Surya Patel as the Doctor
* Geoffrey Best as Watson, a big game hunter
* Bill Godden as Sam

The film also credits lions and lionesses Boy, Girl, Henrietta, Mara the Lioness|Mara, Ugas, and "the Cubs".

==Production==
George Adamson served as chief technical advisor on the film and discusses his involvement in his first autobiography, Bwana Game (UK title, 1968), known in the US as A Lifetime with Lions. 

The making of the film was a life-changing experience for actors Virginia McKenna and her husband Bill Travers, who became animal rights activists and were instrumental in creating the Born Free Foundation.

One of the lions in the film was played by a former mascot of the Scots Guards, who had to leave him behind when they left Kenya.  Haile Selassie Game Department of Uganda.

==Reception==
The film was one of the most popular movies at the British box office in 1966. 

===Critical response===
Born Free received critical acclaim. Review aggregator Rotten Tomatoes reports that 92% of 12 film critics have given the film a positive review, with a rating average of 7 out of 10. 
 title song is still haunting the elevators and supermarkets, but this is really a nicely restrained childrens tearjerker that doesnt overdo the anthropomorphism, despite extreme provocation." 

===Accolades===
*  
*   (lyrics) for Born Free (Matt Monro song)|"Born Free"
* Golden Globe Award for Best Motion Picture - Drama
*  
*  
*   Grammy Award for Best Original Score Written for a Motion Picture: John Barry

==Sequels and spinoffs==
The book Born Free (1960) was followed by two other books, Living Free (1961) and Forever Free (1963). In 1972, a film sequel entitled Living Free was released. While deriving its name from the second book, the film was based on the third book in the series. It starred Susan Hampshire and Nigel Davenport as Joy and George Adamson.

A documentary follow-up to Born Free, entitled The Lions are Free, was released in 1969. The film follows Born Free-actor Bill Travers as he journeys to a remote area in Kenya to visit George Adamson, and several of Adamsons lion friends.

In 1974, a thirteen-episode American television series was broadcast by   and Chris Noth. Joy and George Adamson do not appear as the main characters in the story.

To Walk with Lions (1999) depicts the last years of George Adamsons life, as seen through the eyes of his assistant, Tony Fitzjohn. George is portrayed by Richard Harris, and Honor Blackman makes a brief appearance as Joy.

The one-hour   stations in January 2011. It includes a collection of archival footage and an exploration into the lives of Joy and George Adamson during the years following release of the film. 

==In popular culture==
Season 1 episode 15 of The Carol Burnett Show featured a comedy skit that parodied Born Free. It aired 1 January 1968 and starred Carol Burnett, Harvey Korman, and Tim Conway.
 Far Away Places", of the AMC period drama Mad Men has the character of Peggy Olson skip work to catch a matinee of Born Free.

In the 1975 movie Shampoo (film)|Shampoo, the Jack Warden character Lester sings a snippet of the song to himself.

In the opening scene of the 2005 animated film Madagascar (2005 film)|Madagascar, Marty the Zebra has a dream sequence of running through the African savanna with the song in the background.

In the 2012 video game  , the song from the movie is played numerous times throughout the game.

The main theme from Born Free recurs throughout the later series of the popular American drama series Dexter (TV series)|Dexter.

==See also==
* Christian the lion

==References==
 

==External links==
*  
*   African Wildlife Preservation Trust
*   for the Born Free Foundation
*  
*  
*  
*  
*  
*   ( )

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 