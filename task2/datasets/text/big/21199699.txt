The Inhabited Island
{{Infobox film
| name           = The Inhabited Island
| image          = The Inhabited Island Poster.jpg
| caption        = 
| director       = Fyodor Bondarchuk
| producer       = Alexander Rodnyansky  Sergei Melkumov Strugatsky brothers (novel)
| narrator       = Gennady Vengerov Vasiliy Stepanov Aleksei Serebryakov  Fyodor Bondarchuk
| music          = Yuriy Poteenko (original music)  Pavel Doreuli (sound effects)
| cinematography = Maksim Osadchy
| editing        = Igor Litoninsky STS
| released       =    runtime        = 115 minutes (part I)  100 minutes (part II) country        = Russia language  Russian
|budget         = $36,6 million (both parts) gross          = followed_by    = 
}}
 English as Prisoners of Power; a part of Noon Universe series. It is a dystopian story set on post-apocalyptic planet Saraksh, ruled by totalitarian regime that brainwashes its citizens. Maxim Kammerer, a space explorer from Earth, crash lands on Saraksh and becomes involved in the planets everboiling politics. 

The film was met with mixed reviews. It was praised for visual style, social commentary and being faithful to the original novel, but also criticized for its direction and casting. The Inhabited island was made with one of the largest budgets for Russian cinema at the time (it remains the most expensive Russian science fiction film ever), and despite huge box office earnings and DVD sales the movie barely repaid its own budget.

==Plot summary==

=== Part I ===
 crash lands Robinson on an uninhabited island" (hence the title). Maxim encounters local military police, who dont believe Maxim is alien and send him to mental hospital in the capital. Guy Gaal (Fyodorov), a corporal of the Guards, is assigned to escort Maxim.

Maxim learns that the name of the planet is Saraksh, and the country where he landed is called Fatherland, because its ruled by a militaristic dictatorship of the Unknown Fathers. Strider (Serebryakov), a powerful figure in the government, wants to see the newcomer; he sends one of his men, Funk, after Maxim, but as they drive the street, Funk suffers from what seems to be a surge of epilepsy, and Maxim is left alone. He encounters Rada Gaal (Snigir), a sister of Guy, falls in love with her and stays with the Gaals. Guy talks Maxim into becoming a privateer ("candidate") in the Guards. They are forced to participate in brutal raids against the people called "degenerates" by state propaganda. After Maxim refuses to execute a group of captured "degenerates", Guards commander Rittmeister Chachu shoots him and leaves him to his apparent death.

However, Maxim survives and joins a group of rebel "degenerates". During his time with the rebels, Maxim learns the true nature of the regime and the purpose of towers erected across the country. Contrary to state propaganda, the towers function as mind control devices, sending a special kind of radiation that ensures the citizens loyalty to the regime. Twice in a day, the majority of people experience a patriotic frenzy, while minority, the "degenerates", are able to resist brainwashing, but they suffer from intense pain and convulsions. Unknown Fathers themselves are known to be "degenerates" (this is highlighted by Funks "epilepsy" earlier). Maxim realises that rebels are hiding the truth just as the government does, in hope that they might use the towers themselves, had they come to power.

After fighting alongside the "degenerates" in the attack on a tower, Maxim is again captured by the military and sentenced to a prison labor camp. There, inmates are tasked with cleaning the wilderness of automated defense systems that were left abandoned by decades of war. On a mission, Maxim manages to capture a mechanized tank. He drives the tank away from detention zone across the minefield, picks up Guy, and heads south to the neighbouring nations.

=== Part II ===

The second part is named Skhvatka (Skirmish); it starts with a brief description of the first part plot.

Maxim reveals the true purpose of the towers to Guy, who does not believe at first but is convinced when his enthusiasm fades as they get farther from the towers. They arrive in a city ruined by atomic bombing and inhabited by mutants. Maxim tries to convince mutants to start a rebellion against Unknown Fathers, but, being too weak, they refuse. Maxim then decides to seek help from another powerful country on Saraksh, the Islands Empire. Mutants give Maxim a bomber airship to fly for the Empire.

As Guy and Maxim travel the airship, Guy is again hit by the towers radiation. He starts histerically screaming of his loyalty to Maxim and hugging him. To stop it, Maxim collides the airship into the Tower and destroys it. After crash-landing at the shore, Maxim and Guy discover an abandoned Imperial White Submarine. There, they find evidence that Islands Empire is even worse mass-murdering fascist regime which wouldnt help their goals.

Meanwhile, the Unknown Fathers are planning an invasion of the neighbouring country of Khonti. As soon as Maxim and Guy return to Fatherland, they are captured and sent to war along with fellow prisoners, where Guy is killed in a battle. Funk, following Striders orders, finds Maxim and takes him out of the war back to the capital. Fatherland is defeated, and the State prosecutor, who was an active supporter of the war, realizes that the Unknown Fathers will execute him for failure. He contacts Maxim and reveals the location of the Tower Control Center, urging him to use it for a coup détat. Maxim sneaks in, but instead of following the prosecutors plan, he blasts the Center with explosives.

As he exits the Center, hes confronted by angry Strider. He berates and beats Maxim and reveals himself as undercover spy from Earth. Towers turned out to be Earth technology he used to stop nuclear civil war in the Fatherland, but Maxim, a well-intentioned amateur, ruined the plan that Strider prepared for decades. Strider tells Maxim to fly home to Earth. Maxim refuses and offers his help on one condition: Strider should never use the brainwashing towers anymore. After a fight and a heated argument, Strider reluctantly agrees. The final sequence shows rebels overthrowing the Unknown Fathers.

==Cast== Vasiliy Stepanov as Maxim Kammerer (voiced by Maksim Matveev)
* Yuliya Snigir as Rada Gaal
* Pyotr Fyodorov as Guy Gaal Aleksei Serebryakov as Strider
* Fyodor Bondarchuk as State prosecutor aka Smart
* Sergey Garmash as Zef
* Gosha Kutsenko as Veper
* Andrei Merzlikin as Fank
* Mikhail Yevlanov as Rittmeister Chachu
* Anna Mikhalkova as Ordi Tader
* Sergei Barkovsky as Nolle Renadu
* Aleksey Gorbunov as Shurin
* Maksim Sukhanov as Dad
* Yuriy Tsurilo as the general
* Aleksandr Feklistov as Dever
* Kirill Pirogov as Svyokor
* Yevgeni Sidikhin as Teste
* Sergei Mazayev as Voldyr
* Leonid Gromov as Gramenau
* Vasili Savinov as Lesnik
* Vyacheslav Razbegayev as Krysolov
* Dimash Akhimov as Behemoth
* Ignat Akrachkov as prosecutors referent
* Anton Eskin as Oreshnik

==Production==
 Russian STS Channel, with special effects by EyeScream Studio, Main Road Post and Quantum Creation FX.

The production was filmed in Crimea, Ukraine, with photography lasting for 10 months (February 14 – December 7, 2007). The film had a total budget of about $36.6 million, including $10 million for the films promotion and $1 million of State donations. 

== Reception ==
The Inhabited Island was met with mixed reviews. According to Russian review aggregator Kritikanstvo.ru, both parts hold an approximate approval rating around 6 of 10, based on 20 reviews.   Many critics praised the film for being surprisingly faithful to the novel. Some praised the films visual effects and action, while others criticized the work of director and editor. Mir Fantastiki review called the film a "chaotic mash of visual, dramatical and musical components, each of which separately has quite high quality".  "The movie scatters into a mosaic of spectacular images, that dont comprise the consistent image of the world", agrees Total DVD 

The film was a relative commercial failure. Despite grossing around $30 million from two parts summarily   and ranking among the top-grossing films of Russias modern cinema, it didnt repay its budget of $36 mln. The movie was released for home video very shortly after it exited theatres. According to Bondarchuk, this helped the film to become profitable eventually. 

Boris Strugatsky, the only one of the books co-authors who lived to see the film, said the film is a "directors success", noting its faithfulness to the novel. He praised the work of Serebryakov (Strider), Bondarchuk (Prosecutor General) and Garmash (Zef), but expressed disappointment over the work of Kutsenko (Vepr), Mikhalkova (Ordi Tader) and Merzlikin (Funk). He noted Stepanov as one of the biggest success of the film, saying that "Maxim Kammerer is exactly as we imagined him".         

== Release ==
Due to the length of the story, the film was split in two parts: the first premiered on December 18, 2008 and the second on April 23, 2009. Bulldog Film Distribution released the film in February 2015, on February 9 as Video on Demand and on 16 February 2015 on DVD in the UK. 

==References==
 

==External links==
* 
* 
* 
* 
* 
* , Charles Ganske

 

 
 
 
 
 
 
 
 
 