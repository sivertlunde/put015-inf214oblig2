Twin Peaks: Fire Walk with Me
 
{{Infobox film
| name           = Twin Peaks: Fire Walk with Me
| image          = FWWM_US_poster.jpg
| alt            = In the center of the poster is half of a golden heart-shaped necklace with a picture of a blonde woman (Laura Palmer) inside it. The necklace is on fire. In the background is red curtains and black-and-white zig-zag flooring.
| caption        = Theatrical release poster
| director       = David Lynch
| producer       = {{Plainlist|
* Francis Bouygues
* Gregg Fienberg}}
| screenplay     = {{Plainlist|
* David Lynch
* Robert Engels}}
| based on       =  
| starring       = {{Plainlist|
* Sheryl Lee
* Moira Kelly
* David Bowie
* Chris Isaak
* Harry Dean Stanton
* Ray Wise
* Kyle MacLachlan}}
| music          = Angelo Badalamenti
| cinematography = Ron Garcia
| editing        = Mary Sweeney CIBY Pictures
| distributor    = {{Plainlist|
* AMLF  
* New Line Cinema  }}
| released       =  
| runtime        = 134 minutes  
| country        = {{Plainlist|
* France
* United States}}
| language       = English
| budget         = $10 million
| gross          = $4.2 million 
}}
Twin Peaks: Fire Walk with Me is a 1992 French-American psychological horror film,  directed by David Lynch and written by Lynch and Robert Engels.
 Washington town series finale. Thus, while the film is often considered a prequel, it also has features more typical of a sequel.
 Special Agent Dale Cooper in the TV series, was reluctant to return out of fear of getting typecast, so his presence in the film is smaller than originally planned.
 a commercial hit in Japan.

==Plot==
  Gordon Cole (David Lynch) calls FBI Agent Chester Desmond (Chris Isaak) about the mysterious murder of Teresa Banks in the town of Deer Meadow. Cole introduces Chester to his new partner, Sam Stanley (Kiefer Sutherland), and they receive clues from Lil the dancer. During the drive to Deer Meadow, Chet explains to Sam most of the clues Lil provided, but he does not explain the blue rose pinned to Lils red dress. After difficulty with the local sheriffs department, Desmond and Stanley finally view Teresas body at the morgue. They realize that her ring is missing and the letter "T" has been placed under her fingernail. Desmond and Stanley question the town residents about Teresa, but come up with few clues. Stanley leaves Deer Meadow, while Desmond remains behind. Desmond discovers Teresas missing ring under a trailer, but as soon as he touches it, he disappears.

The following day at FBI headquarters in Philadelphia, long-lost Agent Phillip Jeffries (David Bowie) re-appears. He tells Cole about a meeting he witnessed in a dream involving the Man from Another Place, Bob (Twin Peaks)|BOB, Mrs. Chalfont, and her grandson. Suddenly, Jeffries begins to scream and disappears. Desmond is reported missing and Agent Dale Cooper (Kyle MacLachlan) is sent to Deer Meadow to investigate his disappearance. The clues to Teresa Banks murder lead to a dead end but Cooper is certain her killer will strike again.
 James Hurley James Marshall) before classes start. After school, Laura tells Donna about the differences between James and Lauras actual boyfriend, the arrogant and ill-tempered jock Bobby Briggs (Dana Ashbrook).
 agoraphobic Harold Harold Smith (Lenny Von Dohlen) that BOB took the pages. Harold tries to tell Laura that BOB is not real but Laura insists that BOB is real and wants to be her. Laura tells Harold that BOB will kill her if she does not allow him to possess her. As Harold tries to calm her down, Lauras face changes color and she utters the words, "Fire walk with me." She quickly returns to normal and visibly shaken, Laura gives   Harold her diary to keep. She leaves, stating she does not know when, or if, she will return.
 Leland (Ray Wise), emerge from the house. Laura begins to think her father is being possessed by BOB. Later that evening, the Palmer family is eating dinner. Leland sees Lauras half-heart necklace and questions her about her "lovers." Looking at her finger, Leland insinuates there is dirt underneath the nail. Later, Leland goes into his daughters bedroom and tells Laura he loves her. Laura wonders if he could truly be BOB.
 Heather Graham) next to her in bed, covered in blood. Annie tells Laura to write in her diary that "the good Dale" is trapped in the Black Lodge and cant leave. Laura sees the ring in her hand and is frightened. Laura hears her mother cry and tries to go comfort her, but finds herself inside the painting. From within the painting, Laura sees herself sleeping peacefully in bed.

Laura awakens in the morning with the ring gone from her hand. Disturbed, she removes the painting from her wall. Meanwhile, Bobby, Leo, and Jacques Renault discuss drug scores. Bobby first phones Leo asking to score more drugs, but Leo hangs up on him. Bobby then calls Jacques at the Roadhouse, who agrees to send someone to meet with him. That same evening, Laura prepares to go to the Bang Bang Bar. Donna wants to accompany her but Laura refuses to let her come along. As Laura is about to enter the bar, she encounters the Log Lady. Inside the bar, Jacques introduces Laura to two men. The group is about to leave for the Pink Room to have sex, but Donna shows up. The men are impressed with her "audition kiss" and they allow her to come along.
 Mike (Al Strobel), the one-armed man, shouts at Leland that "the thread will be torn" and shows Laura Teresas ring in an attempt to warn her that Leland is BOB, but Leland and Laura become frightened and drive away.

Leland pulls into a gas station parking lot to gather his wits, and recalls his affair with Teresa. He had asked Teresa to set up a foursome and invite some of her friends, but fled when he discovered Laura was among them. Teresa realized who he was and plotted to blackmail him and he killed her to prevent his secrets from being revealed. Laura realizes that Mikes ring was the same one from her dream.

The next night, Laura and Bobby take cocaine in the woods, and wait for Jacques drug messenger, a sheriffs deputy from Deer Meadow. The messenger delivers the drugs and takes out a gun, but Bobby shoots him first. He hastily tries to bury the messenger as Laura laughs maniacally, high on drugs. In the morning, James worries about Lauras drug use but to no avail. At night, BOB comes through Lauras window and begins raping her. She realizes that BOB is Leland, and warns Leland away from her. Upset, Laura uses more cocaine and has trouble concentrating at school. Bobby attempts to initiate sex with her, but Laura refuses. Bobby realizes Laura was only using him for his access to cocaine. He hands her his stash, breaking off their relationship. The angel in Lauras painting disappears.

In the woods, Laura tells James that "his Laura" is gone. She tells him that she loves him and runs into the woods where she meets Ronette, Jacques, and Leo. They hold an orgy in Jacques cabin as Leland watches from outside. Leland attacks Jacques outside, and Leo flees in panic. Leland takes Laura and Ronette, both bound, to the train car. Meanwhile, Mike realizes that BOB/Leland is about to kill again and chases after him. As Leland is tying Laura up a second time, she asks if he is going to kill her. He does not answer but places a mirror in front of her. She screams after seeing her reflection turn into BOB. BOB tells Laura that he wants her. Meanwhile, Ronette prays for rescue but breaks down because she is "dirty." Suddenly, an angel appears in the train car. The angel unties Laura and Ronettes ropes. Ronette opens the door to let Mike in, but Leland knocks her unconscious and kicks her out of the train car. Mike manages to throw in the ring. Laura puts it on, which prevents BOB from possessing her. Enraged, BOB stabs Laura to death.

BOB/Leland places Lauras body in the lake. As her corpse drifts away, BOB/Leland enters the Black Lodge, where he encounters Mike and the Man from Another Place, who is seated at Mikes left side as the aforementioned "arm". They tell BOB they want all their garmonbozia ("pain and sorrow"). BOB heals Lelands wound. Lauras body washes up on the lakeshore, where it is found by the Sheriffs department the following morning. Lauras spirit later sits in the Black Lodge and notices Agent Cooper at her side. He places a comforting hand on her shoulder. Laura looks deeply saddened until her angel appears, and she begins to cry and then laugh.

==Cast==
 
* Sheryl Lee as Laura Palmer
* Moira Kelly as Donna Hayward
* David Bowie as Phillip Jeffries
* Chris Isaak as Special Agent Chester Desmond
* Harry Dean Stanton as Carl Rodd
* Ray Wise as Leland Palmer
* Kyle MacLachlan as Special Agent Dale Cooper Shelly Johnson
* Dana Ashbrook as Bobby Briggs
* Phoebe Augustine as Ronette Pulaski
* Frank Silva as Killer BOB Leo Johnson
* Miguel Ferrer as Albert Rosenfield
* Pamela Gidley as Teresa Banks
* Heather Graham as Annie Blackburn
* Frances Bay as Mrs. Tremond
* Peggy Lipton as Norma Jennings Gordon Cole James Marshall James Hurley
* Jürgen Prochnow as Woodsman
* Kiefer Sutherland as Agent Sam Stanley Harold Smith Sarah Palmer Mike Nelson Philip Michael Gerard
* Gary Bullock as Sheriff Cable
 

==Production==
Twin Peaks had been canceled only a month when David Lynch announced he would be making a film with French company CIBY-2000 financing what would be the first film of a three-picture deal.    However, on July 11, 1991, Ken Scherer, CEO of Lynch/Frost productions, announced that the film was not going to be made because series star Kyle MacLachlan did not want to reprise his role of Special Agent Dale Cooper. A month later, MacLachlan had changed his mind and the film was back on.
 Of Mice and Men that prevented her from committing to the film.    Fenns character was cut from the script, Boyle was recast with Moira Kelly, and Beymers scenes were not filmed. In a September 2007 interview, Beymer claimed that he did not appear in any scenes shot for the film, although his character, Benjamin Horne, appeared in the script.   

MacLachlans reluctance was also caused by a decline of quality in the second season of the show. He said "David and Mark   were only around for the first season... I think we all felt a little abandoned. So I was fairly resentful when the film, Fire Walk with Me, came around."  Although he agreed to be in the film, MacLachlan wanted a smaller role, forcing Lynch and co-writer Robert Engels to re-write the screenplay so that the Teresa Banks murder was investigated by Agent Chester Desmond and not by Cooper as originally planned. MacLachlan ended up working only five days on the movie.

Another missing figure from Twin Peaks was co-creator Mark Frost. The relationship between Lynch and Frost had become strained during the second season and after the series ended. Frost went on to direct his own film, Storyville (film)|Storyville (1992), and was unable to collaborate with Lynch on Fire Walk with Me.   

  tour. I was there for only a few days."   
 Washington and Los Angeles, California. When shooting went over schedule in Seattle, Washington|Seattle, Washington, Lauras death in the train car had to be shot in Los Angeles on soundstage during the last day of shooting, October 31.   

==Themes==
Lynch wanted to make a Twin Peaks film because, as he claimed in an interview, "I couldnt get myself to leave the world of Twin Peaks. I was in love with the character of Laura Palmer and her contradictions: radiant on the surface but dying inside. I wanted to see her live, move and talk. I was in love with that world and I hadnt finished with it. But making the movie wasnt just to hold onto it; it seemed that there was more stuff that could be done",   and that he was "not yet finished with the material".   

Actress Sheryl Lee, who played Laura Palmer, also echoed these sentiments. "I never got to be Laura alive, just in flashbacks; it allowed me to come full circle with the character."  According to Lynch, the movie is about "the loneliness, shame, guilt, confusion and devastation of the victim of incest. It also dealt with the torment of the father – the war within him." 

==Release==
Twin Peaks: Fire Walk with Me received a reaction quite the contrary to the television series. The film was entered into the 1992 Cannes Film Festival,    where it was greeted with booing from the audience and met with almost unanimous negative reviews. According to Roger Ebert from The Chicago Sun-Times, the film was met with two extremes, one side being overall positive, while the other side being the exact opposite.  Filmmaker Quentin Tarantino, who was also in attendance, confessed in a 1992 interview, "After I saw Twin Peaks: Fire Walk with Me at Cannes, David Lynch had disappeared so far up his own ass that I have no desire to see another David Lynch movie until I hear something different. And you know, I loved him. I loved him." 

Even the CIBY-2000 party at Cannes did not go well. According to Lynch, Francis Bouygues (then head of CIBY) was not well liked in France and this only added to the films demise at the festival.  After the Cannes showing, Lynch said "It was a little bit of a sadness,   Youd like to have everybody there, but their characters didnt have a bearing on the life of her  ". 

U.S. distributor New Line Cinema released the film in America on August 28, 1992. It grossed a total of United States dollar|USD$1.8 million in 691 theaters in its opening weekend and went on to gross a total of $4.2 million in North America.   
 musical score, which won a Spirit Award, a Saturn Award and a Brit Award. 

===Critical reception=== normalized rating average score of 28% based on 16 reviews. 

The film holds a 61% rating on review aggregator website Rotten Tomatoes, with 37 of 61 critics giving the film a positive review. The site wrote of the critics consensus: "For better or worse, Twin Peaks: Fire Walk with Me is every bit as strange and twisted as youd expect from David Lynch". 

Most negative reviews came from American film critics. Among the negative reviews, Janet Maslin from The New York Times wrote, "Mr. Lynchs taste for brain-dead grotesque has lost its novelty".  Fellow Times film critic Vincent Canby concurred, "Its not the worst movie ever made; it just seems to be".    In his review for Variety (magazine)|Variety magazine, Todd McCarthy said, "Laura Palmer, after all the talk, is not a very interesting or compelling character and long before the climax has become a tiresome teenager".    USA Today gave the film one-and-a-half stars out of four, calling it, "a morbidly joyless affair".    Rolling Stone magazines Peter Travers wrote, "though the movie ups the TV ante on nudity, language and violence, Lynchs control falters. But if inspiration is lacking, talent is not. Count Lynch down but never out".    In her review for The Washington Post, Rita Kempley described the film as a "perversely moving, profoundly self-indulgent prequel".   

Most positive reviews came from British film critics. Among the positive reviews,   hailed the film as Lynchs "masterpiece".    Slant Magazine gave the film a four out of four stars,  listing it in their 100 Essential Films list. 

In the book Lynch on Lynch, Chris Rodley described the film as "brilliant but excoriating", writing that "by the time Lynch unveiled Twin Peaks: Fire Walk With Me in 1992, critical reaction had become hostile, and only now is the movie enjoying a degree of cautious but sympathetic critical re-evaluation. It is, undoubtedly, one of Lynchs cruellest, bleakest neighbourhood visions, and even managed to displease die-hard fans of the series.   In exposing the very heart of the TV series, Lynch was forced to accept that he was unlikely to return to the town of Twin Peaks again." 

==Home media==
Lynch originally shot over five hours of footage that was subsequently cut down to two hours and fourteen minutes. The footage nearly appeared on New Lines Special Edition DVD in 2002, but was nixed over budgetary and running-time concerns. 

Most of the deleted scenes feature additional characters from the television series who ultimately did not appear in the finished film.  Lynch has said that "I had a limit on the running time of the picture. We shot many scenes that—for a regular feature—were too tangential to keep the main story progressing properly. We thought it might be good sometime to do a longer version with these other things in, because a lot of the characters that are missing in the finished movie had been filmed. Theyre part of the picture, theyre just not necessary for the main story."  According to Lynch, had the movie included these scenes, it "wouldnt have been quite so dark. To me it obeyed the laws of Twin Peaks. But a little bit of the goofiness had to be removed." 

In 2007, DVDrama.com reported that MK2 was in final negotiations with Lynch about a new two-disc special edition that would include seventeen deleted scenes hand-picked by the director himself. It had been tentatively scheduled for release on October 17, 2007, but MK2 subsequently opted instead to re-release a bare-bones edition of Fire Walk with Me, citing a new version including the deleted scenes has been put on hold indefinitely. In November 2008, Lynch said the following regarding the deleted scenes:

 Twin Peaks: Fire Walk with Me is owned by a company called MK2 in France. And I spoke to them a couple of months ago.   Ive spoke to them several times about this.   I think it will happen, but maybe the financial crisis is   affecting that in some way. Im not sure whats going on. Im pretty sure theres seventeen scenes in that at least but its been a while since weve looked into that.  

Paramount Pictures, which has DVD distribution rights to the TV series, acquired the rights in Germany and most of the world excluding the US, UK, France and Canada. Paramount released their DVD in 2007. The DVD was a port straight from the MK2 French edition.

Fire Walk with Me was released on Blu-ray in France on November 3, 2010 by MK2. 

The film was released on DVD and Blu-ray in Australia by Madman Entertainment on February 8, 2012, marking the 20th anniversary of the films theatrical release. 

The film was also released on Blu-ray on June 4, 2012 in the UK by Universal UK Home Video, although it has been reported that the release suffers from defects in the audio track.  The film has been released on Blu-ray in North America on July 29, 2014, as part of the Twin Peaks complete mystery Blu-ray collection, and contains over 90 minutes of deleted and extended scenes from the film. 

===Legacy and sequel===
According to cinematographer Ron Garcia, the film was popular in Japan, in particular with women, as Martha Nochimson wrote in her book on Lynchs work, "he surmises that the enthusiasm of the Japanese women comes from a gratification of seeing in Laura some acknowledgment of their suffering in a repressive society."    Released under the title, Twin Peaks: The Last Seven Days of Laura Palmer, it was greeted with long lines of moviegoers at theaters.   

In retrospect, Lynch has said, "I feel bad that Fire Walk with Me did no business and that a lot of people hate the film. But I really like the film. But it had a lot of baggage with it. Its as free and as experimental as it could be within the dictates it had to follow." 

Mary Sweeney, the films editor, said, "They so badly wanted it to be like the TV show, and it wasnt. It was a David Lynch feature. And people were very angry about it. They felt betrayed."  For her part, Lee is very proud of the film, saying, "I have had many people, victims of incest, approach me since the film was released, so glad that it had been made because it helped them to release a lot." 
 two more films that would have continued and then concluded the series narrative. But in a 2001 interview, he said that the Twin Peaks franchise is "dead as a doornail."   
 The Killing, have noted similarities and borrowed elements from Fire Walk with Me and Twin Peaks, and compared and contrasted Sud and Lynchs works.        

==Soundtrack==
 
{{Infobox album  
| Name = Twin Peaks: Fire Walk with Me
| Type = soundtrack
| Artist = Angelo Badalamenti
| Released =   ambient
| Length =   Warner Bros.
| Producer = Angelo Badalamenti, David Lynch
}} original soundtrack.

In addition to his instrumental compositions, Fire Walk with Me s soundtrack features vocal accompanient to Badalamentis songs by jazz vocalist Jimmy Scott and dream pop singer Julee Cruise. Badalamenti performs vocals on "A Real Indication" and "The Black Dog Runs at Night", two songs by the Thought Gang, a musical project between Badalamenti and David Lynch. Lynch wrote the lyrics for several of the soundtracks songs, including "Sycamore Trees", "Questions in a World of Blue", "A Real Indiction" and "The Black Dog Runs at Night", and was the soundtracks producer alongside Badalamenti. 
 Best Music Independent Spirit Awards.    In March 2011, British music publication NME placed Twin Peaks: Fire Walk with Me s soundtrack at number 1 on their list of the 50 Best Film soundtracks Ever, describing it as "combining plangent beauty with a kind of clanking evil jazz, this is one of those endlessly evocative soundtracks that takes up residence in your subconscious and never leaves." 

{{Track listing
| collapsed = yes
| headline = Track listing
| total_length = 57:04
| music_credits = yes
| lyrics_credits = yes 
| title1 = Theme from Twin Peaks: Fire Walk with Me
| music1 = Angelo Badalamenti
| length1 = 6:40
| title2 = The Pine Float
| music2 = Badalamenti
| length2 = 3:58
| title3 = Sycamore Trees
| note3 = vocals by Jimmy Scott
| music3 = Badalamenti
| lyrics3 = David Lynch
| length3 = 3:52
| title4 = Dont Do Anything (I Wouldnt Do)
| music4 = Badalamenti
| length4 = 7:17
| title5 = A Real Indication
| note5 = by Thought Gang, vocals by Badalamenti
| music5 = Badalamenti
| lyrics5 = Lynch
| length5 = 5:31
| title6 = Questions in a World of Blue
| music6 = Badalamenti
| note6 = vocals by Julee Cruise
| lyrics6 = Lynch
| length6 = 4:50
| title7 = The Pink Room
| music7 = Badalamenti
| length7 = 4:02
| title8 = The Black Dog Runs at Night
| note8 = by Thought Gang, vocals by Badalamenti
| music8 = Badalamenti
| lyrics8 = Lynch
| length8 = 1:45
| title9 = Best Friends
| music9 = Badalamenti
| length9 = 2:12
| title10 = Moving Through Time
| music10 = Badalamenti
| length10 = 6:41 Falling
| music11 = Badalamenti
| length11 = 5:27
| title12 = The Voice of Love
| music12 = Badalamenti
| length12 = 3:55
}}

==Awards and nominations==
{| class="wikitable plainrowheaders" style="text-align:left"
|-
! scope="col"| 
! scope="col"| Category — Recipient(s)
|- Independent Spirit Awards
| Best Original Score — Angelo Badalamenti 
|-
! scope="row"| Saturn Awards Best Music — Angelo Badalamenti 
|-
! scope="col"| 
! scope="col"| Category — Nominee(s)
|- Cannes Film Festival
| Palme dOr — David Lynch 
|- Independent Spirit Awards Best Female Lead — Sheryl Lee 
|-
! scope="row"| Saturn Awards Best Writing — David Lynch and Robert Engels 
|}

==References==
 
*  

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 