The Queen of the Landstrasse
{{Infobox film
| name = The Queen of the Landstrasse 
| image =
| image_size =
| caption =
| director = Géza von Cziffra 
| producer = 
| writer = Géza von Cziffra 
| narrator =
| starring = Angelika Hauff   Rudolf Prack   Hermann Erhardt
| music = Hanns Elin   Anton Profes 
| cinematography = Hans Schneeberger    
| editing =  
| studio = Löwen-Filmproduktion 
| distributor = Prisma
| released =  8 October 1948  
| runtime = 88 minutes
| country = Austria   German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Queen of the Landstrasse (German: Konigin der Landstrasse) is a 1948 Austrian romance film directed by Géza von Cziffra  and starring Angelika Hauff, Rudolf Prack and Hermann Erhardt.  The films sets were designed by art director Fritz Jüptner-Jonstorff. The title alludes to the Landstraße district of Vienna.

==Cast==
*  Angelika Hauff as Flora Giebel genannt "Lulu"  
* Rudolf Prack as Michael von Dornberg  
* Hermann Erhardt as Johannes Giebel  
* Ditta Donnah as Nellie Giebel  
* Albin Skoda as Alfredo  
* Karl Skraup as Niko  
* Dagny Servaes as Regina von Dornberg, Michaels Tante 
* Karl Gunther 
* Petra Trautmann as Gaby  

== References ==
 

== Bibliography ==
* Robert Dassanowsky. Austrian Cinema: A History. McFarland, 2005.

== External links ==
*  

 

 
 
 
 
 
 
 

 