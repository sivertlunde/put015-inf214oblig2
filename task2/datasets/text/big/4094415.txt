The Lonely Guy
{{Infobox film
| name           = The Lonely Guy
| image          = The_Lonely_Guy.jpg
| director       = Arthur Hiller
| producer       = Arthur Hiller
| writer         = Ed. Weinberger (screenplay) Stan Daniels (screenplay) Neil Simon (adaptation) Bruce Jay Friedman (book)
| starring = {{Plainlist|
* Steve Martin
* Charles Grodin
* Judith Ivey
* Steve Lawrence
* Robyn Douglass
}}
| music          = Jerry Goldsmith
| cinematography = Victor J. Kemper
| editing        = Raja Gosnell William H. Reynolds
| studio         = Aspen Film Society
| released       =  
| distributor    = Universal Pictures
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $5,718,573
}} 1984 romantic comedy film, directed by Arthur Hiller and starring Steve Martin. The screenplay is credited to Ed. Weinberger and Stan Daniels (of Taxi (TV series)|Taxi) as well as Neil Simon (for "adaptation"), and is based on the 1978 book The Lonely Guys Book of Life by Bruce Jay Friedman.

Martin portrays a greeting card writer who goes through a period of bad luck with women. In his despair, he writes a book titled A Guide for the Lonely Guy, which changes his life.

The film also stars Charles Grodin, Judith Ivey, and Steve Lawrence and features cameo appearances from Merv Griffin, Dr. Joyce Brothers, and Loni Anderson. The theme song, "Love Comes Without Warning," was performed by the band America (band)|America.

==Synopsis==
When shy Larry Hubbard (Steve Martin), a greeting card writer, finds his girlfriend Danielle (Robyn Douglass) in bed with another man, he is forced to begin a new life as a "lonely guy." Larry befriends fellow "lonely guy" Warren (Charles Grodin), who considers committing suicide.

After going through a period of terrible luck with women, Larry meets Iris (Judith Ivey), who has dated "lonely guys" before. She gives Larry her number but he loses it due to a few mishaps.

When Warren decides to jump off the Manhattan Bridge, Larry goes to intercept him. Upon seeing Iris on the subway, Larry uses spray paint to tell her to meet him at the bridge and they convince Warren from jumping off, thus leading to their first date. Iris explains that she has been married six times, most of them "lonely guys" who have left her, often having a problem (e.g., gambling). Despite falling in love with Larry, Iris is unsure about going further, so she breaks it off.

At the pit of his despair, Larry writes a book titled A Guide for the Lonely Guy, which is rampantly successful and catapults him into an entirely different experience of life. He becomes rich and famous and even his relationship with Iris can begin on a new basis. Unfortunately, Iriss insecurities return, saying that Larry is now too good for her. She leaves him twice, once after they try to make love and again when they bump into each other on a cruise, where she falls in love with another friend of Larry, Jack (Steve Lawrence).

Jack and Iris get married despite Larry running through the city, over the Queensboro Bridge, asking help from a traffic cop and accidentally breaking up a wedding at another church. In a reversal of fortune, its Larry and not Warren who wants to jump off the bridge. Warren reassures Larry that he will find someone just like he did. Wishing that a twist of fate would bring the woman he loves back to him, Iris falls into his arms from the bridge. They then meet Warrens new girlfriend, who turns out to be Dr. Joyce Brothers. The film ends with Larry stating that he couldnt believe how well things ended and the four go on a date.

==Cast==
*Steve Martin as Larry Hubbard
*Charles Grodin as Warren Evans
*Judith Ivey as Iris
*Steve Lawrence as Jack Fenwick
*Robyn Douglass as Danielle
*Merv Griffin as Himself
*Joyce Brothers as Herself
*Julie K. Payne as rental agent Roger Robinson as greeting card supervisor
*Nicholas Mele as maître d’
*Loni Anderson as Herself (uncredited)

==Reception==
 
The Lonely Guy received mixed reviews from critics.  It holds a 48% rating on Rotten Tomatoes.

==See also==
* List of American films of 1984

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 