Action of the Tiger
{{Infobox film
| name           = Action of the Tiger
| image          = Actionofthetiger.jpg
| caption        =  Robert Carson Peter Myers (adaptation) James Wellard (novel)
| starring       = Van Johnson Martine Carol Herbert Lom Sean Connery
| producer       = Kenneth Harper George Willoughby (associate producer) Co-executive producer: Joseph Blau Johnny Meyer
| music   = Humphrey Searle
| cinematography = Desmond Dickinson
| editing = Frank Clarke Terence Young
| studio         = Claridge Productions Van Johnson Enterprises
| distributor    = Metro-Goldwyn-Mayer United States United Kingdom
| released       = August 20, 1957 (USA)
| runtime        = 93 minutes
| language       = English budget = $863,000  .  gross = $1,640,000 
}}
 Terence Young, and starring Van Johnson and Martine Carol.

The plot is about the rescue of a political prisoner held in Socialist Albania|Albania. Carson, played by Van Johnson, is an American contraband runner approached by Tracy, a French woman who wants him to help rescue her brother. Action of the Tiger is also of interest for Sean Connery fans, as the Scots actor would reunite with director Terence Young for the film Dr. No (film)|Dr. No five years later.
 Henry V. 

==Cast==
*Van Johnson as Carson  
*Martine Carol as Tracy  
*Herbert Lom as Trifon  
*Gustavo Rojo as Henri  
*José Nieto (actor)|José Nieto as Kol Stenho  
*Helen Haye as The Countess  
*Anna Gerber as Mara 
*Anthony Dawson as Security Officer   
*Sean Connery as Mike 
*Yvonne Romain as Katina 
*Norman MacOwan as Trifons father  
*Helen Goss  as Farmers Wife  
*Richard Williams as Abdyll

==Reception==
According to MGM records, the film earned $465,000 in the US and Canada and $1,175,000 elsewhere, resulting in a net profit of $25,000. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 