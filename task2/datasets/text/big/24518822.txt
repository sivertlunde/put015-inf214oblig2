Cowboy Counsellor
{{Infobox film
| name           = Cowboy Counsellor
| director       = George Melford
| producer       = M.H. Hoffman Jr.
| writer         = Jack Natteford
| starring       = Hoot Gibson
| music          =
| cinematography = {{plainlist| Tom Galligan
*Harry Neumann
}}
| editing        = Mildred Johnston
| distributor    = Allied Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
}}
 Western film courtroom drama. One reviewer deemed it "the best of Gibsons films for Monogram Pictures|Allied."   

==Plot== con artist, Jack Rutherford) robs a stagecoach, and plants some of the stolen money at the ranch of Luke Avery (Fred Gilman), Averys sister beautiful sister Ruth (Sheila Bromley) ropes an instantly smitten Alton into being Averys defense attorney. As part of his strategy to defend Avery, Alton plans to pull off another stagecoach robbery.

== Cast ==
*Hoot Gibson as Dan Alton
*Sheila Bromley as Ruth Avery Jack Rutherford as Bill Clary
*Skeeter Bill Robbins as Deputy Lafe Walters
*Al Bridge as Sheriff Matt Farraday
*Fred Gilman as Luke Avery Bobby Nelson as Bobby Avery William Humphrey as Judge Kendell
*Gordon De Main as Replaced by Lorch
*Merrill McCormick as Bearded Prisoner Sam Allen as Hotel Clerk

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 


 