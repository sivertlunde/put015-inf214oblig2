Sign o' the Times (film)
{{Infobox film
| name           = Sign o the Times
| image          = Poster of the movie Sign o the Times.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster Prince
| producer       = Robert Cavallo Stephen Fargnoli Joseph Ruffalo
| writer         = Prince
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Prince Sheena Easton Sheila E.
| music          = Prince
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Cineplex Odeon Films
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 album of the same name and sales increased accordingly. However the U.S. remained resistant to his latest album, and sales began to drop; it was at this point that Prince decided to film a live concert promoting the new material, for eventual distribution to theaters in America. 
 The Revolution keyboardist Dr. Fink, the film sees the group perform live on stage (although "U Got the Look" is represented by its promotional music video).

==Background==
 
 Sportpaleis in Antwerp, Belgium on June 29. However, the footage from these concerts was deemed unsatisfactory, partly due to the film being grainy and unusable, but also because some of the sound had been recorded on different channels and was virtually inaudible.  Consequently, the live performance was reshot at Princes Paisley Park Studios (where the between-song segues were also filmed).  According to saxophonist Eric Leeds, around 80% of the final film was drawn from the Paisley Park reshoot. 
 Sign o Purple Rain", Girls & Boys" and "Lets Go Crazy") but these songs were eventually dropped.

==Music==
 

===Set list=== Sign o the Times"
# "Play in the Sunshine"
# "Little Red Corvette"/"Housequake"
# "Slow Love"
# "I Could Never Take the Place of Your Man"
# "Hot Thing"
# "Nows the Time" (Charlie Parker cover by the band excluding Prince)
# Drum solo by Sheila E.
# "U Got the Look"
# "If I Was Your Girlfriend"
# "Forever in My Life"/"It"
# "Its Gonna Be a Beautiful Night"
# "The Cross"

==Release and reception== SKY Magazine suggested that it "was the greatest concert movie ever made". The film was released in early 2005 on DVD in Canada by Alliance Atlantis, although the cut songs still failed to appear with the rest of the footage.
 coda for Crystal Ball LP set The Black Album.

The Blu-ray Disc version was released in Australia by Via Vision Entertainment and Madman Entertainment on May 9, 2012. The film has yet to be released on DVD and/or Blu-ray in the United States.

==Notes==
 

== External links ==
*  

 

 
 
 
 
 
 
 