Puppet on a Chain (film)
{{Infobox film
| name           = Puppet on a Chain
| image          = Puppet on a Chain poster.jpg
| caption        = Film poster by Arnaldo Putzu
| alt            =  Paul Wheeler (additional material) Don Sharp (additional material)
| starring       = Sven-Bertil Taube, Barbara Parkins, Alexander Knox
| director       = Geoffrey Reeve Don Sharp (boat sequence)
| producer       = Kurt Unger
| distributor    = 
| released       = August 2, 1971 (London) 
| runtime        = 98 minutes (theatrical release)
| music          = Piero Piccioni
| cinematography = Jack Hildyard
| editing        = Bill Lenny
| studio         = Shepperton Studios
| language       = English 
| budget         = 
| gross          = 
}}
 British thriller film directed by Geoffrey Reeve and starring Sven-Bertil Taube, Barbara Parkins and Alexander Knox.  It is based on the novel Puppet on a Chain by Alastair MacLean.

== History == Live and Let Die and followed up two years later in Amsterdamned, would also feature a long canal boat chase.  

==Plot introduction==
When three hippie drug-dealers are murdered by "the assassin" (Peter Hutchins) in Los Angeles, the U.S. government sends special agent Paul Sherman (Sven-Bertil Taube) to track down the European source of heroin that is causing the drug war.

Sherman was born in Holland, but it is clear that Amsterdams chief of police, Colonel De Graaf (Alexander Knox) is unhappy with having the Americans interfere in Dutch affairs. However, Sherman’s direct contact, Inspector Van Gelder (Patrick Allen) is more cooperative, since his niece, Trudi (Penny Casdagli) suffered brain damage after a heroin overdose.

When Sherman makes contact with a deep-cover agent from Washington named Maggie (Barbara Parkins), he almost immediately has a brutal encounter with "the assassin," indicating that the drug dealers have someone on the inside. 

==Cast==
 
* Sven-Bertil Taube as Paul Sherman 
* Barbara Parkins as Maggie 
* Alexander Knox as Col. De Graaf 
* Patrick Allen as Insp. Van Gelder 
* Vladek Sheybal as Meegeren 
* Ania Marson as Astrid Lemay 
* Stewart F. Lane as George Lemay 
* Drewe Henley as Jimmy Duclos 
* Michael Mellinger as Hotel Manager
* Penny Casdagli as Trudi 
* Peter Hutchins as The Assassin 
* Henni Orri as Herta 
* Mark Malicz as  Morgenstern 
 

==Production==
 
The famous boat chase sequence was directed by Don Sharp who was specifically hired to do it. Once he completed that sequence he was hired by the producers to reshoot additional sequences. Sharp:
 The chappie who directed originally   has gone on to produce some nice movies, and before this he had a good career in shooting commercials. And he’s a talented man. But he didn’t have a story sense then, as a director, and he and his camera operator, each set-up, you know, a sequence that looked like part of a television commercial and wasn’t there for the drama of it, or just to let the audience know what was happening. And therefore I had to take parts out of, for example, a nightclub sequence. Seventy-five per cent of it was fine; only when it came to the dialogue bits between them did I have to go in and reshoot it, because it just didn’t make sense – to shoot a couple of really good, important dialogue lines to do with the plot in a shot between the legs of a dancer . . . That wasn’t exactly it but I mean that sort of thing, you know. It was done for a visual effect.   accessed 25 March 2013  

==Reception==
Sharp said the film went on to make "a mint of money" and claimed in 2007 he was still getting royalties from it being shown on television. 

==References==
 

==External links==
* 
* 
* 
 
 

 
 
 
 
 
 
 
 
 