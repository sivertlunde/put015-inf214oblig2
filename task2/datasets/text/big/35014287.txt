Not Only Mrs. Raut
{{Infobox film
| name           = Not Only Mrs. Raut
| image          = Not Only Mrs Raut.jpg
| alt            =  
| caption        = DVD cover
| director       = Gajendra Ahire
| producer       = Aditi Deshpande
| writer         = 
| based on       =  
| starring       = Aditi Deshpande Tushar Dalvi
| music          = Kaushal Inamdar
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}} Marathi social drama film directed by Gajendra Ahire. Aditi Deshpande, who plays the lead role of Mrs. Raut, has produced the film.
 National Award for Best Film in Marathi (Rajat Kamal) at the 51st National Film Awards for "its treatment of two womens struggle against male exploitation and domination".   

==Plot==
Mrs. Vidya Raut murders her boss Karkhanis near Juhu Beach and surrenders herself to the police. She also submits the bloody dagger, weapon of murder. Swati Dandavate, a budding scholar advocate decides to fight her case. Her initial meetings with Mrs. Raut go waste as she keeps confessing her crime and is ready for the punishment. Convinced that something is hidden, Swati decides to find it out. Swatis husband Aditya, who is also an advocate, is now public prosecutor in this case. His elder brother and he try to convince Swati to drop the case which she would obviously lose. But standing against her family, Swati decides to dig out truth and defend Mrs. Raut.

==Cast==
* Aditi Deshpande as Mrs. Vidya Raut
* Madhura Velankar as Advocate Swati Dandavate
* Tushar Dalvi as Public Prosecutor Aditya Vishnu Dandavate
* Mohan Joshi as Advocate Dada Vishnu Dandavate
* Vandana Gupte
* Vikram Gokhale as Judge
* Kaushal Inamdar as Witness / Hotel Boy
* Ketaki Karadekar as Sneha
* Ravindra Mankani as Raghuvir Karkhanis
* Milind Shinde as Advocate S.M. Garud

==Awards== Silver Lotus Best Film in Marathi. 

Aditi Deshpande won the Screen Best Actress - Marathi award for her performance of Mrs. Raut. She won this award joinly with Sonali Bendre, who played the role of Queen Sheelavati in Anahat (film)|Anahat (2003). 

== References ==
 

== External links ==
*  
*  


 
 

 

 
 
 
 