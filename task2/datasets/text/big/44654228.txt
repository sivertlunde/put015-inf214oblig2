English Only, Please
{{Infobox film
| name           = English Only, Please
| director       = Dan Villegas
| producer       = Atty. Joji Alonso
| writer         = Antoinette Jadaone Dan Villegas
| starring       = Jennylyn Mercado Derek Ramsay Kean Cipriano Isabel Oli Cai Cortez
| cinematography = Dan Villegas
| editing        = Marya Ignacio
| distributor    = Quantum Films (Philippines)
                   Above the Line Events (US & Canada)
| country        = Philippines
| awards         = 40th Metro Manila Film Festival Second Best Picture
| released       =  
                    
| runtime        = 105 minutes
| country        = Philippines Tagalog
| gross          = 
}} 40th Metro Manila Film Festival.

== Plot ==
Julian Parker ( ) is a top-notch Filipino-English tutor. She is strict and feisty and takes pride in teaching English and/or Filipino to more than 142 Americans, Fil-Ams, Koreans since 2006. Taking the perfect light romantic-comedic tone of popular Asian movies, with feisty leading ladies who are funny, charming and lovable English Only, Please is a simple love story about how love knows no bounds..

== Cast ==
*Jennylyn Mercado as Tere Madlansacay, a Filipino tutor from the Philippines
*Derek Ramsay as Julian Parker, a Filipino-American from the States
*Isabel Oli as Megan Montañer, Julians ex-girlfriend
*Tom Rodriguez as Ernest, Megans fiance
*Kean Cipriano as Rico, Teres "gold-digging, on-and-off" boyfriend
*Isabel Frial as Kay-Kay, Mallowss witty daughter/Teres goddaughter
*Cai Cortez as Mallows, Teres best friend
*Jerald Napoles as "the manliligaw on the street"
*Lynn Ynchausti-Cruz as Teres mother 

== Awards ==
*40th Metro Manila Film Festival 2014
**Best Picture: 2nd Place
**Best Actor: Derek Ramsay
**Best Actress: Jennylyn Mercado
**Best Director: Dan Villegas
**Best Story: Antoinette Jadaone and Dan Villegas
**Best Screenplay: Antoinette Jadaone and Anj Pessumal
**Best Editor: Marya Ignacio

== Production ==
 

== Release ==
English Only Please was released locally in the Philippines at the beginning of the 40th Metro Manila Film Festival on Christmas Day, December 25, 2014 into January 2015. Above the Line Events is the distributor of the motion picture in the United States and Canada, which premiered on February 20, 2015, showing until February 26, 2015 in US theaters and until March 5, 2015 in Canadian theaters.

== References ==
 

==External links==
* 

 
 
 


 
 