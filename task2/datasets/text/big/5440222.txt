Beau Geste (1926 film)
{{Infobox film
| name           = Beau Geste
| image          = BeauGeste26.jpg
| caption        =
| director       = Herbert Brenon Ray Lissner (assistant)
| producer       = Jesse L. Lasky Adolph Zukor William LeBaron 
| based on       =   John Russell Paul Schofield Neil Hamilton Ralph Forbes	
| music          = Hugo Riesenfeld Hans Spialek
| cinematography = J. Roy Hunt
| editing        = Julian Johnson
| distributor    = Paramount Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         = $900,000 Philip Liebfriend, Films of the French Foreign Legion, Bear Manor Media 2011  gross = $1.5 million   accessed 19 April 2014 
|}}
Beau Geste (1926) is a silent film, based on the novel Beau Geste by P. C. Wren. This version starred Ronald Colman as the title character.   

==Synopsis==
The plot concerns a valuable gem, which one of the Geste brothers, Beau, is thought to have stolen from his adoptive family.

==Cast==
*Ronald Colman as Michael Beau Geste Neil Hamilton as Digby Geste
*Ralph Forbes as John Geste
*Alice Joyce as Lady Patricia Brandon
*Mary Brian	as Isabel
*Noah Beery	as Sergeant Lejaune
*Norman Trevor as Major de Beaujolais
*William Powell as Boldini
*George Regas as Maris Bernard Siegel as Schwartz
*Victor McLaglen as Hank
*Donald Stuart as Buddy
*Paul McAllister as St. Andre
*Redmond Finlay as Cordere
*Ram Singh as Prince Ram Singh

==References==
 

==See also==
*The House That Shadows Built (1931) promotional film released by Paramount which includes excerpts of this film
==External links==
* 
*  
*  
* 

 
 

 
 
 
 
 
 
 
 
 

 
 