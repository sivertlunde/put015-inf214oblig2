Country Strong
 
{{Infobox film
| name           = Country Strong 
| image          = Country Strong Poster.jpg
| caption        = Theatrical release poster
| director       = Shana Feste
| producer       = Tobey Maguire Jenno Topping
| writer         = Shana Feste
| starring       = Gwyneth Paltrow Tim McGraw Garrett Hedlund Leighton Meester
| music          = Michael Brook John Bailey
| editing        = Connor ONeill
| studio         = Maguire Pictures
| distributor    = Screen Gems
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $20,529,194 
}} Friday Night Lights in 2004.

==Plot==
The film begins with Beau (Garrett Hedlund|Hedlund), singing with Kelly (Gwyneth Paltrow|Paltrow), a recovering alcoholic going through rehab. Beau is clearly smitten by her, and it is later revealed that the two have been having an affair, even though Kelly is married to James (Tim McGraw|McGraw). Kelly is checked out of rehab a month early by James, who wants her to go on a three-city tour to restore her image. She agrees on the condition that Beau becomes her opening act, but James has already made plans to see Chiles Stanton (Leighton Meester|Meester), a beauty queen with potential to become a rising singer, perform that night in hopes that she will be Kellys opener instead.

On the night of Chiles performance, it is revealed that Beau has been acquainted with her before and doesnt want her on the tour, dismissing her as a "Country Barbie". Chiles performance nearly falls apart as she gets stage fright and cant sing; however, Beau steps in and begins to sing "Friends in Low Places". The two sing the song together, and it gives Chiles the courage to continue on her own. James is impressed by Beaus performance, and offers the opening act to both Beau and Chiles. He suggests to Beau that he and Chiles could make quite the duo, but Beau disagrees. Nevertheless, Beau agrees to go on the tour because he cares so much about Kelly.
 Country Strong" but breaks down on stage. She attempts to try another song, "A Fighter", but breaks down again and is led off stage by her husband, ending the show. They tell the media at a press conference that they had to cancel the show due to food poisoning and head off to the next show.

Beau ends his relationship with Kelly, and begins to spend more time with Chiles, whose fame has been increasing as the tour progresses. Both Chiles and Beau form a bond and Beau lets go of his earlier hostilities towards her. She even finishes the chorus to his song "Give in to Me".

Later, Beau confronts James about Kellys worsening condition. James insists that Kelly may get better, and says that he cant let her go out with a failure. Beau disagrees and subtly admits their affair. Enraged, James punches him in the jaw and leaves the room.

Before the next show in Austin, Texas|Austin, Kelly gets drunk and is unable to go on stage. Beau and Chiles still do their opening act, and it is then that they perform "Give in to Me" for the first time together. JJ, Kellys agent, nearly cancels the tour but is dissuaded by Kellys offer of sex. Beau witnesses this and becomes frustrated with Kelly. That same night, Chiles and Beau have sex, and Beau confesses to Chiles that he likes her.

In Dallas, Beau gives Chiles a pair of star-shaped earrings she had said she always wanted and asks her to move with him to California. Chiles immediately says yes, but changes her mind and asks him for time to consider the offer.
 overdosing on prescription medication.  She leaves a letter to Beau, echoing his earlier words that "Love and fame cant live in the same place." In the letter, she gives him the advice to choose "love". Beau takes her advice, and after Kellys funeral, he moves to California.

Later on, Beau is seen singing in a bar in California, and he sees Chiles walk in, wearing the earrings he gave her. The film ends with the two singing the song they wrote together, "Give in to Me".

==Cast==
* Gwyneth Paltrow as Kelly Canter
* Tim McGraw as James Canter
* Leighton Meester as Chiles Stanton
* Garrett Hedlund as Beau Hutton
* Marshall Chapman as Winnie
* Jim Lauderdale as Kellys Bandmate
* Jeremy Childs as J.J.

==Production==
With a budget of $15 million, pre-production work on the project began in November 2009.  Feste was working as a nanny for Tobey Maguire, caring for his daughter, when she wrote the screenplay of Country Strong. She showed him her "work-in-progress" at his request and he agreed to co-produce the film.  She had real country stars in mind when she wrote the script.   

Garrett Hedlund took guitar lessons because he could not play the guitar nor sing before the film.    He explained: "When I read the script and started playing guitar I knew I wanted to get there. I mean I wasnt going to have a hand double come in and be playing the chords, having to have two different set ups just because I couldnt get it down."    Gwyneth Paltrow also took guitar lessons.  Paltrow stated she had trouble understanding her characters alcohol abuse and sought advice in playing the character from Robert Downey Jr, who has suffered from addiction in the past.   In interviews, Feste has said that American pop singer Britney Spears inspired Paltrows character in the film. 
 Union Station Mount Olivet Andrew Jackson Hall.    Filming also took place in Liberty, Tennessee.  The film was shot in 2010 from early January to the first week of March. 

==Release==
On December 14, 2010, the official screening took place at the Academy of Motion Picture Arts and Sciences in Beverly Hills, California. It was attended by the films stars and included Tim McGraw, Gwyneth Paltrow, Leighton Meester, Garrett Hedlund, and many other stars of Hollywood and country music. 

In the United States, the film received a wide release on January 7, 2011.   

==Reception==

===Critical response===
Country Strong has received mixed to negative reviews. Review aggregate Rotten Tomatoes reports that 22% of critics have given the film a positive review based on 129 reviews, with an average score of 4.4/10. However, Gwyneth Paltrow was praised for her role. The critical consensus is: "The cast gives it their all, and Paltrow handles her songs with aplomb, but Country Strong s cliched, disjointed screenplay hits too many bum notes." 
Another review aggregate, Metacritic, gave the film a 45/100, indicating mixed or average reviews.

===Box office===
The film opened to a wide release on January 7, 2011, taking in $7.5 million and coming in sixth place that weekend.  It eventually grossed just $20.5 million, beating its $15 million budget.

===Home Media===
The film was released on DVD/Blu-ray Disc|Blu-ray on April 12, 2011. The special features include the soundtrack, deleted scenes, music videos, extended performances, the original ending, as well as featurettes about the cast, the songwriters, and the costumes.

===Accolades===
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Date of ceremony
! Category
! Recipient(s)
! Result
|- Academy Awards 
| February 27, 2011 Best Original Song Tom Douglas, Hillary Lindsey and Troy Verges
|  
|- Golden Globe Awards 
| January 16, 2011 Best Original Song
| "Coming Home"
|  
|- Satellite Awards 
| December 19, 2010 Best Original Song Country Strong" Jennifer Hanson and Gwyneth Paltrow
|  
|-
| Las Vegas Film Critics Society Awards 2010 
| December 16, 2010 Best Song
| "Country Strong"
|  
|- Denver Film Critics Society Awards 2010 
| January 21, 2011 Best Original Song
| "Me and Tennessee"
|  
|- Golden Reel Awards  
| February 20, 2011
| Best Sound Editing: Music in a Musical Feature Film
| 
|  
|-
| Teen Choice Awards 
| August 7, 2011
| Actress in a Drama
| Leighton Meester
|  
|}

==Music==
  The films RCA Nashville. The soundtrack debuted at #16 on the Billboard Top Country Albums chart upon its release on October 26, 2010.  Following the films release in January 2011, the soundtrack rose to a new peak of #2 on that chart, as well as #6 on the all-genre Billboard 200|Billboard 200 albums chart.
 We Belong Together" from Toy Story 3, respectively.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 