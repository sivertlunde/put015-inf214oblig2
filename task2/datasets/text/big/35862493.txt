Her Great Match
{{infobox film
| name           = Her Great Match
| image          =
| imagesize      =
| caption        =
| director       =  
| producer       = Popular Plays and Players
| writer         = Clyde Fitch(play)
| starring       = Gail Kane Vernon Steele
| music          =
| cinematography = John W. Boyle
| editing        =
| distributor    = Metro Pictures
| released       = August 30, 1915
| runtime        = 5 reels
| country        = United States
| language       = Silent(English intertitles)
}}
Her Great Match is a silent 1915 drama film starring Gail Kane and based on the Broadway play by Clyde Fitch. In the 1905 play the star was stage beauty Maxine Elliott. This film was directed by Frenchman René Plaissetty and released through the Metro Pictures studios then just newly formed. This movie survives and is preserved at George Eastman House.   

==Cast==
*Gail Kane - Jo Sheldon
*Vernon Steele - Prince Adolph (*as Vernon Steel)
*Ned Burton - Mr. Bote
*Clarissa Selwynne - Mrs. Sheldon
*Lawrence Grattan - Mr. Sheldon Julia Hurley - The Duchess Louise

==References==
 

==External links==
* 
* 

 
 
 
 
 


 