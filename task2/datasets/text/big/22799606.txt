Don't Look Back (2009 film)
{{Infobox film
| name           = Dont Look Back
| image          = Dont Look Back film.jpg
| caption        = Theatrical release poster
| director       = Marina de Van
| producer       = Patrick Sobelman
| writer         = Jacques Akchoti Marina de Van
| starring       = Sophie Marceau Monica Bellucci
| music          = Luc Rollinger
| cinematography = Dominique Colin
| editing        = Mike Fromentin
| distributor    = Wild Bunch
| released       =  
| runtime        = 111 minutes
| country        = France
| language       = French
| budget         = €13,000,000 
| gross          = $2,564,074
}}

Dont Look Back ( ) is a 2009 French thriller film directed by Marina de Van and starring Sophie Marceau and Monica Bellucci.    Written by Jacques Akchoti and Marina de Van, the film is about a wife and mother of two children who suddenly notices changes to the way the family home is arranged and feels that her body is transforming without anyone around her noticing it. While others believe her perceptions are due to fatigue and stress, she is sure that something more profound is happening, and her search to understand these mysterious perceptions prompts her to track down a woman in Italy who holds the key to the mystery.   

==Plot==
Jeanne, a married woman with two young children, starts to notice small changes taking place in the arrangement of objects in her familys home—furniture, pictures, rooms—as well as in her physical appearance. She seems to be the only one noticing these changes, but still she is absolutely certain that her perceptions are a result of something profound, and are not caused by stress or fatigue, as everyone else seems to think. Feeling increasingly out of sorts, ever more distant, and even psychologically cut off from her husband and children, who are baffled by her behavior, Jeanne goes to her mother in the hope that she, at least, will provide for her some clarification or otherwise soothing solution. 

At her mothers home, Jeanne comes across a photograph of what she believes, or has been led to believe, is herself as a young girl, her mother, and another woman. What she sees in that photograph moves her to travel to Italy in order to track down the woman in the photograph. In Italy, Jeanne finds the woman, and in a way, discovers herself as well. She begins to solve the mystery behind her changes, and comes to learn the truth about herself.

==Cast==
* Sophie Marceau as Jeanne
* Monica Bellucci as Jeanne/Rosa Maria
* Andrea Di Stefano as Teo 1/Gianni
* Thierry Neuvic as Teo 2
* Brigitte Catillon as Nadia 1/Valérie
* Sylvie Granotier as Nadia 2
* Augusto Zucchi as Fabrizio
* Giovanni Franzoni as Enrico
* Vittoria Meneganti as Enfant brune 11 ans
* Francesca Melucci as Enfant blonde 9 ans
* Adrien de Van as Le psychiatre
* Serena dAmato as Donatella
* Myriam Muller as Nania 2 jeune
* Thomas De Araujo as Jérémie 8 ans
* Lucas Preux as Jérémie 2
* Thais Fischer as Léa 6 ans
* Chloé Béziat as Léa 2
* Daniel Plier as Michael
* Didier Flamand as Robert
* Lionel Kopp as Le boucher 1
* Frédéric Frenay as Le boucher 2
* Colette Kieffer as Sonia
* Luciano Massimo Liccardi as Guillaume
* Mounir Noum as Benjamin
* Andrea Coppola as Invité Gordale
* Olivier Demarest as Jeune homme édition
* Valentin Langlois as Hommes sécurité
* Laurent Hassid as Hommes sécurité
* Pascal Bonitzer as Joueurs cercle
* Iliana Lalic as Joueurs cercle
* Riahard Benichou as Joueurs cercle
* Raoul Schlechter as Serveur restaurant   

==Production==
;Filming locations
* Lecce, Apulia, Italy 
* Luxembourg 
* Paris, France   

==Release==
Dont Look Back was screened out of competition at the 2009 Cannes Film Festival.   

==References==
 

==External links==
*  
*  
*   at uniFrance

 
 
 
 
 
 
 
 
 
 
 