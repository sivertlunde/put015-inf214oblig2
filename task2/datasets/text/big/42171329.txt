Goa Dalli CID 999
{{Infobox film|
| name = Goa dalli CID 999
| image = 
| caption =
| director = Dorai Bhagawan
| writer = Dorai Bhagawan Rajkumar  Lakshmi  Narasimharaju
| producer = Dorai Bhagawan
| music = G. K. Venkatesh
| cinematography = B. Dorairaj
| editing = Venkatram
| studio = Anupam Movies
| released = 1968
| runtime = 144 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada crime detective - Bhagwan duo. Rajkumar in Lakshmi made her debut in Kannada cinema with this film. Sri Lankan based actress Sabitha Perera also made her Indian debut with this film. 
 Bond style of movies in the combination of the director duo with lead actor Rajkumar. The film had musical score by G. K. Venkatesh with lyrics by R. N. Jayagopal.

== Cast == Rajkumar  Lakshmi 
* Sabitha Perera Narasimharaju
* Raghavendra Rao 
* Shakti Prasad
* Jyothi Lakshmi

== Soundtrack ==
The music of the film was composed by G. K. Venkatesh with lyrics by R. N. Jayagopal.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| music_credits = no
| collapsed = no
| title1 = Love in Goa
| extra1 = S. Janaki
| music1 = 
| length1 = 
| title2 = Minchidu Ee Hennu
| extra2 = Renuka
| music2 = 
| length2 = 
| title3 = Balige Baa
| extra3 = L. R. Eswari
| music3 =
| length3 = 
| title4 = Kangale Heliri
| extra4 = L. R. Eswari
| music4 =
| length4 = 
}}

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 


 

 