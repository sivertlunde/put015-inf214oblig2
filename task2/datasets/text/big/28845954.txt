The Devil (1972 film)


{{Infobox film

| name           = Diabeł

| image          = Film_poster_for_Diabeł,_1972_motion_picture_directed_by_Andrzej_Żuławski_.jpg
| image size     =
| border         =
| alt            =
| caption        = 1987 theatrical release poster

| color          = White
| bgcolor        = Blue
| director       = Andrzej Żuławski
| producer       = 
| writer         = Andrzej Żuławski
| narrator       = 
| starring       = Malgorzata Braunek, Leszek Teleszynski, Michal Grudzinski
| music          = Andrzej Korzynski
| cinematography = Andrzej Jaroszewicz, Maciej Kijowski
| editing        = Krzysztof Osiecki
| studio         = Zespól Filmowy "X"
| distributor    = 
| released       = 1972
| runtime        = 119 minutes
| country        = Poland
| language       = Polish
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}  Polish historical film written and directed by Andrzej Żuławski. It was released in 1972. It was banned in Poland. 

==Plot==
During the Prussian armys invasion to Poland in 1793, a young Polish nobleman Jakub is saved from the imprisonment by a stranger who wants in return to obtain a list of Jakubs fellow conspirators. Following his mysterious saviour across the country, Jakub sees the overall chaos and moral corruption including his fathers death and his girlfriends betrayal. Being apparently demented by what he has seen, he commits a number of seemingly motiveless and gory killings.

==References==
 

==External links==
*  

 
 
 
 
 
 

 
 