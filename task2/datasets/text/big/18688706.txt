Un mandarino per Teo
{{Infobox film
| name           = Un mandarino per Teo
| image          = Mandarino-per-teo-un-movie-poster-1960-1020353637.jpg
| caption        = Film poster
| director       = Mario Mattoli
| producer       =
| writer         = Pietro Garinei Sandro Giovannini
| starring       = Walter Chiari
| music          =Gorni Kramer
| cinematography = Gianni Di Venanzo
| editing        = Roberto Cinquini
| distributor    =
| released       =  
| runtime        = 94 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

Un mandarino per Teo is a 1960 Italian comedy film directed by Mario Mattoli and starring Walter Chiari.

==Cast==
* Walter Chiari - Teo Tosci
* Sandra Mondaini - Rosanella Ferrante
* Ave Ninchi - Zia Gaspara
* Riccardo Billi - Ignazio Fumoni
* Carlo Delle Piane - Lo stagnaro
* Annie Gorassini - Angelo biondo
* Chim Kem - Il cinese
* Dante Bisio - Regista Fracassoni
* Anne Marie Delos - Nina Chevrolet
* Corrado Olmi - Il signore in bianco
* Salvo Libassi - Aiuto regista
* Enrico Salvatore - Aiuto regista (as Salvatore Enrico)
* Alberto Bonucci - Il notaio

==External links==
* 

 

 
 
 
 
 
 


 
 