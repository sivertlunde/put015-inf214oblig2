The Girl Downstairs
{{Infobox film
| name           = The Girl Downstairs
| image          = 
| image_size     =
| caption        = 
| director       = Norman Taurog
| producer       = 
| writer         = 
| narrator       =
| starring       = Franciska Gaal   Franchot Tone   Walter Connolly
| music          = 
| cinematography = 
| editing        = 
| studio         = Metro-Goldwyn-Mayer 
| distributor    = 
| released       = 1938
| runtime        = 
| country        = United States English
| budget         = 
| gross          = 
}}
The Girl Downstairs is a 1938 American romantic comedy film directed by Norman Taurog and starring Franciska Gaal, Franchot Tone and Walter Connolly. It is a remake of the 1936 Austrian film Catherine the Last directed by Henry Koster, which had been a major hit for Gaal.

==Main cast==
* Franciska Gaal as Katerina Linz  
* Franchot Tone as Paul Wagner  
* Walter Connolly as Mr. Brown  
* Reginald Gardiner as Willie  
* Rita Johnson as Rosalind Brown  
* Reginald Owen as Charlie Grump  
* Franklin Pangborn as Adolf Pumpfel  
* Robert Coote as Karl, Pauls Butler  
* Barnett Parker as Hugo, the Butler 
* James B. Carson as Rudolph  
* Billy Gilbert as Garage Proprietor  

==External links==
* 

 

 
 
 
 
 
 
 
 

 