Sacrifice (2010 film)
{{Infobox film
| name = Sacrifice
| image = Sacrifice film.jpg
| alt =
| caption =
| film name = {{Film name| traditional = 趙氏孤兒
| simplified = 赵氏孤儿
| pinyin = Zhào Shì Gū Ér}}
| director = Chen Kaige Chen Hong
| writer = Chen Kaige Zhao Ningyu
| starring = Ge You Wang Xueqi Fan Bingbing Vincent Zhao Huang Xiaoming
| music = Ma Shangyou
| cinematography =
| editing = Derek Hui
| studio = 21 Century Shengkai Film, Shanghai Film Group, Stellar Megamedia, TIK Films
| distributor = Stellar Megamedia
| released =  
| runtime = 124 minutes
| country = China
| language = Mandarin
| budget = US$10 million
| gross = US$29,093,560  
}}
Sacrifice is a 2010 Chinese historical drama film directed by Chen Kaige and based on the Yuan Dynasty play The Orphan of Zhao.  The film was distributed in the United States by Samuel Goldwyn Flms. 

==Plot== chancellor of Jin, and his son, General Zhao Shuo, have a rift with General Tuan Gu. Tuan Gu secretly murders the young Duke of Jin with poison and pushes the blame to the Zhao family, using that as an excuse to massacre the entire Zhao clan, numbering about 300 people. The sole survivor is Zhao Shuos baby son, Zhao Wu, who is born to the Dukes elder sister, Lady Zhuang. Lady Zhuang pleads with Tuan Gus subordinate, Han Jue, to spare her child and commits suicide after entrusting Zhao Wu to the physician Cheng Ying, telling Cheng to bring the child to Gongsun Chujiu, a friend of the Zhao family. When Tuan Gu learns that the orphan had escaped, he slashes his sword at Han Jues face in anger, disfiguring Han. Subsequently, Tuan Gu issues an order to seal the gates and to gather all newborn babies in the city. The ploy was to identify the Zhao orphan since Tuan Gu was counting on the person hiding the baby to not hand over the baby to him, hence singling out the one baby left in the city to be the Zhao infant.

Cheng Ying brings the baby Zhao Wu home, but his wife gives the child to the searching soldiers, claiming that Zhao Wu is her son. Cheng Ying then asks Gongsun Chujiu to bring his wife and his real son (disguised as Zhao Wu) out of the city, while he stays behind to try and retrieve Zhao Wu from Tuan Gu. Tuan Gu finds Cheng Ying suspicious and Cheng lies that the Zhao orphan has been taken out of the city by Gongsun Chujiu. Tuan Gu then leads his soldiers to Gongsun Chujius house and finds Cheng Yings wife and son. Mistaking Cheng Yings son as the Zhao orphan, Tuan Gu kills the infant, Gongsun Chujiu and Chengs wife.

Cheng Ying is devastated by the loss of his family and he swears vengeance on Tuan Gu. He pretends to pledge allegiance to Tuan Gu and brings Zhao Wu along with him and raises the child in Tuans house. Tuan Gu mistakenly believes that Zhao Wu is Cheng Yings son and adopts Zhao Wu as his godson. Zhao Wu gradually grows up under Tuan Gus tutelage and develops a close bond with Tuan. Cheng Ying also forges a close friendship with the disfigured Han Jue and they secretly plot to help Zhao Wu avenge his family.

One day, Cheng Ying breaks the truth to Zhao Wu, telling him that Tuan Gu, who has been a fatherly figure to him for the past 15 years, is actually the murderer of his family. Zhao Wu is unable to accept the truth initially but his attitude changes as he hears from Han Jue about how Cheng Ying sacrificed his own son to save him. Zhao Wu and Cheng Ying finally confront Tuan Gu, but Tuan is aware of Zhaos true identity, because, over the years, he has noticed that the boy resembles Zhao Shuo. Zhao Wu engages Tuan Gu in a sword fight but is no match for the latter. At the critical moment, Cheng Ying sacrifices himself to give Zhao Wu an opportunity to stab and kill Tuan Gu. The film ends with a dying Cheng Ying having a vision of his reunion with his family.

==Cast==
* Ge You as Cheng Ying (程婴), a physician.
* Wang Xueqi as Tuan Gu (屠岸贾), a Jin general.
* Zhang Fengyi as Gongsun Chujiu (公孙杵臼), a Jin official and a friend of the Zhao family.
* Vincent Zhao as Zhao Shuo (赵朔), a Jin general and Zhao Duns son.
* Fan Bingbing as Lady Zhuang (庄姬), Zhao Shuos wife and Duke Lings elder sister.
* Huang Xiaoming as Han Jue (韩厥), Tuan Gus subordinate.
* Bao Guoan as Zhao Dun (赵盾), the Jin chancellor.
* Peng Bo as Duke Ling of Jin (晋灵公), the young ruler of Jin.
* Li Dongxue as Timi Ming (提弥明), Zhao Duns bodyguard.
* Zhao Wenhao as Zhao Wu (赵武), the Zhao orphan. Wang Han played the younger Zhao Wu.
* Hai Qing as Cheng Yings wife

==References==
 

==External links==
*  
*     on Sina.com

 

 
 
 
 
 
 