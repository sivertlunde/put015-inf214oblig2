The People of Angkor
 
{{Infobox Film
| name           = The People of Angkor
| image          = 
| caption        = 
| director       = Rithy Panh
| producer       = 
| writer         = Rithy Panh
| narrator       = 
| starring       = 
| music          = Marc Marder
| cinematography = Prum Mesa
| editing        = Isabelle Roudy Marie-Christine Rougerie	 	
| distributor    = 
| released       = 2003
| runtime        = 90 minutes
| country        = Cambodia/France
| language       = Khmer
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2003 Cinema Cambodian documentary film directed by Rithy Panh.  It was exhibited at the Yamagata International Documentary Film Festival in 2005 in film|2005.

The film follows a young Cambodian boy around the temples of Angkor Wat as older men tell him about the legends depicted on the walls, and tourists tour the site.

 

 
 
 
 
 
 
 
 


 
 