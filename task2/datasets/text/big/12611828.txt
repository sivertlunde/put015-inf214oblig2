The Wagons Roll at Night
 

{{Infobox film
| name           = The Wagons Roll at Night
| image          = The Wagons Roll at Night - 1941 - poster.png
| image_size     =
| caption        = 1941 Movie Poster
| director       = Ray Enright
| producer       = Hal B. Wallis Harlan Thompson Francis Wallace (story)
| narrator       =
| starring       = Humphrey Bogart Sylvia Sidney Eddie Albert
| music          =
| cinematography =
| editing        =
| distributor    = Warner Bros. Pictures, Inc.
| released       =  
| runtime        = 84 minutes
| country        = United States English
| budget         =
| gross          =
}} Kid Galahad, also featuring Bogart (though as a villain). 

==Plot==
A circus manager turns a young farm boy into a star lion tamer.

==Cast==
*Humphrey Bogart as Nick Coster
*Sylvia Sidney as Flo Lorraine
*Eddie Albert as Matt Varney
*Joan Leslie as Mary Coster
*Sig Ruman as Hoffman the Great
*Cliff Clark	 ... Doc
*Charley Foy	 ... Snapper
*Frank Wilcox	 ... Tex
*John Ridgely	 ... Arch
*Clara Blandick	 ...	Mrs. Williams
*Aldrich Bowker	 ...	Mr. Williams
*Garry Owen	 ...	Gus
*Jack Mower	 ...	Bundy Frank Mayo	 ...	Wally

==References==
 

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 