Beur sur la ville
{{Infobox film
| name           = Beur sur la ville
| image          = 
| caption        = 
| director       = Djamel Bensalah
| producer       = Djamel Bensalah
| writer         = Djamel Bensalah
| based on       = 
| starring       = Booder Issa Doumbia Steve Tran Sandrine Kiberlain Josiane Balasko Gérard Jugnot
| music          = Rachid Taha
| cinematography = Pascal Gennesseaux	
| editing        = Jean-François Elie
| distributor    = Paramount Pictures
| studio         = Miroir Magique!
| released       =  
| runtime        = 99 minutes
| country        = France
| language       = French
| budget         = $9,7 million
| gross          = $6,674,865 
}}
Beur sur la ville is a 2011 French comedy directed by Djamel Bensalah. 

==Plot==
At 25, while Khalid Belkacem had wasted his BEPC, its traffic, its BAFA, and even his BCG. He did not expect to become the first "positive discriminated" of the police.

==Cast==
 
* Booder as Khalid Belkacem
* Issa Doumbia as Koulibali
* Steve Tran as Tong
* Sandrine Kiberlain as Diane
* Josiane Balasko as Mamie Nova
* Gérard Jugnot as Gassier
* Éva Darlan as Mme Gassier
* Roland Giraud as The Prefect Flaubert
* François-Xavier Demaison as Picolini
* Biyouna as Khalids mother
* Mohamed Benyamna as Khalids father
* Eric Berger as Picolinis secretary
* Sid Ahmed Agoumi as The Chibani
* Pierre Ménès as Pierrot
* Julie De Bona as Alice Gassier
* Khalid Maadour as José Da Silva
* Sacha Bourdo as Trouduk
* Chloé Coulloud as Priscilla
* Yves Rénier as Captain Jancovic
* Paul Belmondo as Lieutenant Liotey
* Julien Courbey as Lieutenant Juju
* David Saracino as Lieutenant Fabiani
* Rodolphe Brazy as Lieutenant Patrick
* Samy Seghir as The trainee policeman
* Valérie Lemercier as The stadium announcer
* Jean-Claude Van Damme as The colonel
* Jacques Boudet as The minister
* Lionel Abelanski as The forensic
* Marilou Berry as The girl with the teeth broken
* Frédérique Bel as The blond
* Mokobé as The paramedic
* Ramzy Bedia as Paki
* Frédéric Beigbeder as Driver Mercedes 600
* Serra Yılmaz as The nurse
* Popeck as The gypsy
* Karim Belkhadra as Darty
* Kamel Le Magicien as The baker
* Lou Charmelle as The girl
* Pape Diouf as A journalist
 

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 