Aazhi Alayaazhi
{{Infobox film
| name           = Aazhi Alayaazhi
| image          =
| caption        =
| director       = Mani Swamy
| producer       =
| writer         = Kakkanadan
| screenplay     = Kakkanadan
| starring       = Kaviyoor Ponnamma Anupama Sukumaran K. P. Ummer
| music          = G. Devarajan
| cinematography = Melli Irani
| editing        = G Venkittaraman
| studio         = Kalabindu Filims
| distributor    = Kalabindu Filims
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film,  directed Mani Swamy. The film stars Kaviyoor Ponnamma, Anupama, Sukumaran and K. P. Ummer in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Kaviyoor Ponnamma
*Anupama
*Sukumaran
*K. P. Ummer
*Kuthiravattam Pappu

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Pollunna theeyaanu sathyam || K. J. Yesudas || P. Bhaskaran ||
|-
| 2 || Poonilaavil || P. Madhuri || P. Bhaskaran ||
|}

==References==
 

==External links==
*  

 
 
 


 