Undrafted (film)
{{Infobox film
| name           = Undrafted
| image          = Undrafted film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Joseph Mazzello
| producer       = Joseph Mazzello Eric Fischer Brianna Johnson   
| writer         = Joseph Mazzello
| starring       = Joseph Mazzello Aaron Tveit Tyler Hoechlin Chace Crawford Philip Winchester
| music          = 
| cinematography = 
| editing        = 
| studio         = Dead Fish Films Parlay Pictures
| distributor    =
| released       =  
| runtime        = 90 minutes 
| country        = United States
| language       = 
| budget         = 
| gross          = 
}}
 sports comedy-drama film the directorial debut of Joseph Mazzello who also wrote, co-produce and starred  the film. It also stars Aaron Tveit, Tyler Hoechlin, Chace Crawford and Philip Winchester, and is based on the true story of Mazzellos brother who missed out on the Major League Baseball draft.

==Plot==
A collegiate baseball star was skipped over in the Major League Baseball draft. An intramural baseball game with his misfit teammates then becomes incredibly important to him as he tries to come to grips with his dashed dreams.

==Cast==
*Joseph Mazzello as Pat Murray
*Aaron Tveit as John "Maz" Mazetti
*Tyler Hoechlin as Dells
*Chace Crawford as Arthur Barone
*Philip Winchester as Fotch
*Jim Belushi as Jim
*Manny Montana as Zapata Matt Bush as David
*Michael Fishman as Antonelli
*Duke Davis Roberts as Ty Dellamonica
*Matt Barr as Anthony
*Billy Gardell as Umpire Haze
*Ryan Pinkston as Jonathan Garvey
*Toby Hemingway as Palacco
*Jay Hayden as Vinnie
*Michael Consiglio as Dave Schwartz

==Production==
===Pre-production===
Mazzello wrote the screenplay for the film, which is based on his brother, who was a successful college baseball player and was scouted to play in the majors. When he wasn’t drafted, a intramural baseball game with his teammates became the most important game of his life. Of it he said "I watched for 15 years as my brother worked tirelessly to make his dream come true only to see it never happen and it was heartbreaking   During that time I saw the love he and his teammates had for each other and for the game that is the heart of our story."  

===Filming===
Principal photography took place in Dunsmore Park, La Crescenta-Montrose, California  from September 2013 to October 8, 2013. 

==Release==
On June 2, 2014, Mazzello announced via Twitter that he had "finally finished editing my film".  It is expected to be released sometime in 2015.

===Marketing===
The first official poster was released on Mazzellos Twitter on February 17, 2015. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 