The Bay Boy
{{Infobox film
| name           = The Bay Boy
| image          = The Bay Boy.jpg
| image_size     =
| caption        = 
| director       = Daniel Petrie
| producer       = 
| writer         = Daniel Petrie
| narrator       = 
| starring       = Kiefer Sutherland Liv Ullmann Peter Donat
| music          = 
| cinematography = 
| editing        = 
| distributor    = Cineplex Odeon Films|Pan-Canadian (Canada) Orion Classics (USA)
| released       =        
| runtime        = 107 minutes
| country        = Canada France English
| budget         = 
| gross          = $162,364 (US) 
| preceded_by    = 
| followed_by    = 
}}
The Bay Boy is a 1984 Canadian film. It is a semi-autobiographical film based on director Daniel Petries experiences of growing up in Glace Bay, a mining town on Cape Breton Island, during the Great Depression. It features the screen debut of Kiefer Sutherland as the films central character.

==Plot==
Donald Campbell (Kiefer Sutherland) is a sensitive teenage boy coming of age in a dark and uncertain time for both his community and life. His mother (Liv Ullman) wants him to continue his education after high school and become a priest, but Donald is more interested in girls than prayerbooks.  After an unsuccessful attempt by a visiting priest to molest him, followed by his first sexual experience with a local girl, Donald politely informs his mother (without revealing why) that he is not going to be a priest.

Meanwhile, when he is not in school, Donald spends his time helping his father (Peter Donat) dig a Bootleg Pit; helps care for his older brother, Joe, who was the brightest boy in his grade until he got sick and was left disabled; and pursues Saxon Coldwell (Leah Pinsent), one of police Sergeant Coldwells two daughters.  Sergeant Coldwells wife died a few months earlier.

Donald lives a hard-working but fairly happy life, until the night he witnesses Sergeant Coldwell shoot and kill the Jewish couple who are his landlord and landlady.  The chief of police is a relative, so Donald feels comfortable admitting he saw the killing but he says he did not see who did it, because he is afraid of Sergeant Coldwell - especially after Sergeant Coldwell lets Donald know that he is aware that Donald did see who committed the murder (because he could not have seen the shooting without also seeing who did it).  When the Sergeant comes home and finds Donald (innocently) visiting with his daughter Dianna, he snaps mentally and tries to kill the boy - with the result his secret is revealed and he is arrested.

The film also depicts the daily lives of the eccentric locals and tight-knit families.

==Production==
The movie was filmed entirely on location in Cape Breton, and primarily in Glace Bay.  Many of the extras are performed by local residents.

==Awards== Genie Award for Best Canadian Film.

==Cast==
*Liv Ullmann as Mrs. Campbell 
*Kiefer Sutherland as Donald Campbell 
*Peter Donat as Mr. Campbell 
*Alan Scarfe ... Sergeant Tom Coldwell 
*Mathieu Carrière ... Father Chaisson 
*Chris Wiggins ... Chief Charlie McInnes 
*Thomas Peacocke ... Father McKinnon 
*Isabelle Mejias ... Mary McNeil 
*Jane McKinnon ... Dianna Coldwell 
*Leah Pinsent ... Saxon Coldwell  Peter Spence ... Joe Campbell
*Joe Warbis ... Terry the Transvestite 
*Josephine Chaplin ... Marie Chaisson 
*Pauline Lafont ... Janine Chaisson 
*Roy McMullin ... Paul Ratchford 
*Kathy McGuire ... Sister Roberta 
*Robbie Gallivan ... Frank Carrey  Bob Rose ... Danny McIsaac 
*Robert Taylor ... Paddy ONeil 
*Darren Arsenault ... Malcolm Broderick  David Ferry ... Walt Roach 
*Bette MacDonald ... Nurse 
*Fannie Shore ... Mrs. Silver 
*Sander Zilbert ... Mr. Silver 
*Tom Rack ... Sol Silver 
*Joe MacPherson ... Rory McInnes  Kevin McKenzie ... Mr. Rankin 
*Iris Currie ... Mrs. Carrey 
*Francis MacNeil ... Terry OShea 
*Michael Egyes ... Basil Broderick 
*Mary McKinnon ... Aunt Coldwell 
*Stéphane Audran ... Blanche
*Rowena Oliver ... Sissy

==References==
 

==External links==
*   
*  
* The   newsletter  .  Issue 7 (Spring 2006) features an article on The Bay Boy and "losing" films.

 
 

 
 
 
 
 
 
 
 
 
 