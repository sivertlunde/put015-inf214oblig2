Ernesto (film)
{{Infobox film
| name           = Ernesto
| image_size     = 
| image	=	Ernesto FilmPoster.jpeg
| caption        = 
| director       = Salvatore Samperi
| producer       =
| writer         = Umberto Saba (original book) Barbara Alberti Amadeo Paganini
| narrator       = 
| starring       = Martin Halm Virna Lisi
| music          = Carmelo Bernaola
| cinematography = Camillo Bazzoni
| editing        = 
| distributor    = 
| released       = 1979
| runtime        = 98 minutes
| country        = Italy Spain Germany
| language       = Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1979 film directed by Salvatore Samperi and starring Martin Halm and Virna Lisi.

==Plot== stableboy (Michele Placido) who infatuates him so both end up in an intense sexual relationship. This ends as, by chance, Ernesto has a sexual intercourse with a prostitute. Ernesto renounces then, to take lessons of violin instead. There, he meets the 15 year old Emilio, by whom Ernesto gets acquainted with his twin sister Rachel (Lara Wendel). Ernesto and Rachel are married.

==Cast==
* Virna Lisi - Ernestos Mother
* Concha Velasco - Aunt Regina (as Conchita Velasco)
* Michele Placido - The Man
* Turi Ferro - Carlo Wilder
* Martin Halm - Ernesto
* Lara Wendel - Ilio / Rachele (US: Emilio / Rachel)
* Enrique San Francisco
* Enrique Vivó
* Francisco Marsó - Uncle Giovanni
* Renato Salvatori - Cesco

==Awards==
The film was entered into the 29th Berlin International Film Festival, where Michele Placido won the Silver Bear for Best Actor.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 