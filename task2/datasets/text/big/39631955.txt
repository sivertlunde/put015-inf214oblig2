Arjun – Kalimpong E Sitaharan
{{Infobox film
| name           = Arjun – Kalimpong E Sitaharan
| image          = Arjun — Kalimpong E Sitaharan poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Prem Prakash Modi
| producer       = Mukul Sarkar
| writer         = 
| screenplay     = Abhijit Sarkar
| story          = Samaresh Majumdar
| based on       =  
| narrator       = 
| starring       = Sabyasachi Chakraborty Om Raya Chowdhury Dipankar De Biswajit Chakraborty Manoj Mitra
| music          = Nilotpal Munshi
| cinematography = Rana Dasgupta
| editing        = 
| studio         = 
| distributor    = T.Sarkar Productions
| released       =  
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film action and thriller marks the debut of young Bengali actor Om in the role of detective Arjun. Based on Sitaharan Rahasya and Khunkharapi by Samaresh Majumdar, the film was released on May 3, 2013.    

==Plot== Bengali writer Samaresh Majumdar for young children.  He wrote many stories in the Arjun series, namely Arjun Ebaar Kolkata-e, Arjun Beriye Elo, Arjun@bipbip.com and many more. This film is the first of the Arjun series based on Sitaharan Rahasya and Khunkharapi.    In the film, Arjun (Om) is seen solving a case with the help of his mentor and guide, Amol Shome (Sabyasachi Chakraborty) in Kalimpong. He is portrayed as a Gen X sleuth who doesnt own a mobile phone but can be seen riding a Motorcycle|bike.   

Sita is a Adolescence|teenager, who is on the verge of adulthood. She was born and brought up in the U.S. and has been brought to Kalimpong as a preventive step of her father, who wants to keep her away from her boyfriend, whom he thinks to be a bad influence for her. She is looked after by her grandmother Neelam. Her father is afraid of an attack from goons who think that the masters plan of a monastery is hidden somewhere in his house. But later on, matters really get serious and deep because everyone seems to be involved in a kind of conspiracy and treachery.   

==Cast==
 
* Om as Arjun
* Sabyasachi Chakraborty as Amal Shome
* Raya Chowdhury as Sita
* Dipankar De as Mr. Sen    
* Churni Ganguly as Neelam
* Biswajit Chakraborty as Major 
* Manoj Mitra as Bistubabu 
* David Chen 

==Filming==
The film was shot in several locations of West Bengal and Sikkim, which included magnificent spots in Kalimpong, misty mountains and dense jungles. 

==Critical Reception==
{| class="wikitable infobox plain" style="float:right; width:23em; font-size:80%; text-align:center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review scores
|-
! Source
! Rating
|-
| Gomolo
|  
|-
| Timescity 
|  
|-
| In.com
|  
|-
| Tollywood Dhamaka
|  
|-
| Nowrunning.com
|  
|-
|}

Arjun&nbsp;– Kalimpong E Sitaharan got mixed reviews from critics and reviewers. Gomolo rated it 2.65 out of 5 stars.    Critics of Timescity rated it 3 out of 5 stars and said that the film failed to make a mark and even commented on the weak dialogues and loud background score.    Along with Timescity, even the critics of In.com, who rated the movie 2.5 out of 5 stars, said that the film ended in such a way that highlighted the possibility of a sequel.    Tollywood Dhamaka rated the movie 2 out of 5 stars and said that the movie is quite confusing.    Nowrunning.com rated it 2.8 out of 5 stars and critic Anurima Das said that Om needs to be more careful about his role as Arjun for the sequel, though the debut was good.     "  The character of Arjun was even compared to other older and mature sleuths such as Feluda, Byomkesh Bakshi and Kakababu.   

==References==
 

 
 
 
 
 