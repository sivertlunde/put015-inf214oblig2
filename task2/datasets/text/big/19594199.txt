I, a Lover
{{Infobox film
| name           = I, a Lover
| image          = 
| caption        = 
| director       = Börje Nyberg
| producer       = Peer Guldbrandsen
| writer         = Peer Guldbrandsen
| starring       = Jørgen Ryg
| music          = 
| cinematography = Jan Lindeström
| editing        = Edith Nisted Nielsen
| distributor    = 
| released       = 11 April 1966
| runtime        = 91 minutes
| country        = Sweden Denmark 
| language       = Danish
| budget         = 
}}

I, a Lover ( ,  ) is a 1966 Swedish-Danish comedy film directed by Börje Nyberg and starring Jørgen Ryg. 

==Cast==
* Jørgen Ryg - Peter Isløv
* Jessie Flaws - Elizabeth Schmidt
* Axel Strøbye - Ole Schmidt
* Kerstin Wartel - Sigrid Schmidt
* Marie Ahrle - Beatrice Isløv (as Marie Nylander)
* Ebbe Langberg - Isac Andersen
* Jeanne Darville - Ulla Pauce
* Paul Hagen - Torbjørn Pauce
* Jytte Breuning - Hilda
* Dirch Passer - Mortensen
* Birgitta Fjeldhede - 
* Sigrid Horne-Rasmussen - Kollega til Peter
* Birger Jensen - Mand i biograf
* Benny Juhlin - 
* Tove Maës - Patient hos Ulla Pauce
* Lise Thomsen - Patient hos Ulla Pauce

==External links==
* 

 
 
 
 
 
 
 
 
 