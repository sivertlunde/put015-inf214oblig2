Kept Husbands
 
{{Infobox film
| name           = Kept Husbands
| image          = KeptHusbandsPoster.jpg
| image_size     =
| caption        = Theatrical Poster
| director       = Lloyd Bacon   
| producer       = William LeBaron Louis Sarecky (associate) 
| writer         = Louis Sarecky (writer) Forrest Halsey (adaptation) Alfred Jackson (adaptation)
| starring       = Dorothy Mackaill Joel McCrea
| music          = Max Steiner 
| cinematography = Jack MacKenzie 
| editing        = George Marsh Ann McKnight 
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =   }}
| runtime        = 76 minutes   
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 class struggles Depression era when this film was made.  The film also points out the stereotypical gender roles which were prevalent at that time.

== Plot summary ==
 
Arthur Parker (Robert McWade) is a wealthy steel magnate who is relating the story to his snobbish wife and spoiled daughter of one of his plant supervisors who fearlessly rushed in and saved the lives of two of his fellow co-workers.  When his wife, Henrietta (Florence Roberts), asks if he rewarded the young man, Parker shows his astonishment by saying that the hero had refused the thousand dollars he had offered.  When the daughter, Dot (Dorothy Mackaill), remarks that she would like to meet a man like that, the father tells her not to worry, she will, for he is coming to dinner that very evening.  Henrietta is aghast at having to socialize with someone not of their class, but Parker, who is a better judge of character, assures her that all will be well.

During dinner, Dot is smitten with the young man, Dick Brunton (Joel McCrea).  So smitten she makes a bet with her father that she can get him to marry her within four weeks, by December 20.  The father takes that bet, and lo and behold she wins Dick’s heart and gets him to accept her proposal of marriage by the deadline, despite his fears of their different social circumstances.

After the wedding, Parker sends the newlyweds on an expensive honeymoon to Europe, after which they return to their lavish home, also supplied by Parker.  Parker also promotes Dick, but within six months, his new lifestyle threatens to emasculate Dick, who loses interest in his career and finds himself dominated by Dots vapid, social whirl of bridge games, cocktail parties and passive acceptance of life as a "kept husband".  This does not sit well with the proud husband, and when Parker offers him a chance to prove himself with a new position in St. Louis, he jumps at the chance. When told of the opportunity however, Dot is less than enthusiastic, not wanting to leave her friends and social circle.  She refuses to agree to accompany Dick.

Dick decides to go to St. Louis, with or without Dot, making her incredibly upset.  Not knowing what to do, he goes to ask advice from his mother (Mary Carr), who tells him that he needs to reconcile with Dot before he leaves for St. Louis.  Meanwhile, Dot has agreed to meet with a former beau, Charles Bates (Bryant Washburn), who attempts to seduce her.  When she returns to their house the following morning, Dick questions her regarding her whereabouts.  She lies to him, and he knows it, since he had seen her with Washburn the prior evening.  Furious, he storms out, saying their marriage is over, and intending to resign from Parker’s company.

Realizing her love for him, Dot eventually finds Dick at the rail station, about to leave for St. Louis.  He has decided to take Parker’s position after all.  The husband and wife reconcile, with Dot agreeing to live within the means that Dick’s salary can provide.

== Cast ==
 
*Dorothy Mackaill as Dorothea "Dot" Parker Brunton
*Joel McCrea as Richard "Dick" Brunton
*Ned Sparks as Hughie Hanready
*Mary Carr as Mrs. Brunton
*Clara Kimball Young as Mrs. Henrietta Post
*Robert McWade as Arthur Parker
*Bryant Washburn as Charlie Bates
*Florence Roberts as Mrs. Henrietta Parker
*Freeman Wood as Mr. Post
*Lita Chevret as Gwen
 AFI database) 

== Soundtrack == The Wedding March", written by Felix Mendelssohn-Bartholdy  Three Little Words", written by Bert Kalmar and Harry Ruby - whistled by Joel McCrea 

== Notes == public domain in the USA due to the copyright claimants failure to renew the copyright registration in the 28th year after publication. 

The tag line for the film was "Every Inch a Man - Bought Body and Soul by His Wife".   

This film marked the debut in sound films of Clara Kimball Young, who had been a major star during the silent film era.  She came back after a six-year hiatus from making films. 

==References==
 

== External links ==
 
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 