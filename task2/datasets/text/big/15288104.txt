Embryo (1976 film)
{{Infobox film
| name           = Embryo
| image_size     =
| image	         = Embryo FilmPoster.jpeg
| caption        =
| director       = Ralph Nelson
| producer       = Anita Doohan Arnold H. Orgolini	
| writer         = Anita Doohan Jack W. Thomas	
| narrator       =
| starring       = Rock Hudson Barbara Carrera Diane Ladd Roddy McDowall
| music          = Gil Melle
| cinematography = Fred J. Koenekamp
| editing        = John A. Martinelli
| distributor    = Cine Artists Pictures (USA)
| released       =  
| runtime        = 104 min.
| country        = United States English
| budget         =
}}

Embryo is a 1976 science fiction / horror film which was directed by Ralph Nelson, and which starred Rock Hudson alongside Barbara Carrera. 

==Plot==
Dr. Paul Holliston (Hudson) is a geneticist who has been living alone in his rambling clinic, which he operates out of his home, after the death of his wife in a car crash in which he was the driver. This leads to his feeling constant pangs of guilt from his sister-in-law Martha Douglas (Diane Ladd), who has become his assistant.

What finally motivates Holliston to resume his medical work is another automobile accident where he is the driver; his car accidentally strikes and kills a dog one dark and stormy night. In the process of attempting, vainly, to save the dogs life, he does manage to save one of her unborn puppies, utilizing a new serum which speeds up the growth and intelligence of the animal. But the dog develops a savage side to its personality in addition to increased intelligence; when confronted with an annoying, smaller dog, the larger one kills the smaller one and conceals its body.

When Holliston manages to see what his discovery can offer the world, he applies the same technique to an unborn human. This unborn human to whom he applies the technique develops into a beautiful young woman (Carrera) who emerges, fully grown, from the incubator in two weeks. Holliston names her "Victoria" because he considers her a victory.

Victoria Spencer, as Holliston introduces her, is highly intelligent, and she becomes Hollistons protégé--the lessons culminating one night in sex.

Unfortunately for Victoria, she displays a dark side. When the serum begins to have an adverse effect on her, she commences to age rapidly, and discovers that, owing to a flaw in Hollistons research that he had mistakenly believed he had corrected, she requires serum from unborn fetuses to stay alive.

The film ends with the dying Victoria announcing that she is going to have a baby, causing Holliston to scream "Noooooooo" in horror before he apparently has a "nervous breakdown."

==Cast==
Information drawn from   at the Internet Movie Database|IMDb.
* Rock Hudson...Dr. Paul Holliston 
* Barbara Carrera...Victoria Spencer 
* Diane Ladd...Martha Douglas 
* Roddy McDowall...Frank Riley 
* Anne Schedeen...Helen Holliston 
* John Elerick...Gordon Holliston 
* Vincent Baggetta...Collier 
* Jack Colvin...Dr. Jim Winston 
* Joyce Brothers
* Dick Winslow...John Forbes 
* Ken Washington...Dr. Brink 
* Lina Raymond...Janet Novak 
* Sherri Zak...a nurse

==Most recent known copyright information==
To the extent of knowledge available as of the middle of May of 2014, Embryo was in the public domain, as Cine Artists Pictures, owners of its copyright, had, to the extent of available knowledge, gone out of business before being able to renew it.

==See also==
* List of films in the public domain

==References==
 
* 

==External links==
 
*  
*  
*  

 

 
 
 
 
 

 
 