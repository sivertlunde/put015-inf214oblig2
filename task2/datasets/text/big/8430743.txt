Aquanoids
{{Infobox Film
| name           = Aquanoids
| caption        = 
| image          = Aquanoids FilmPoster.jpeg
| director       = Reinhart Peschke
| producer       = {{plainlist|
* Albert Gordon
* Mark J. Gordon
}}
| writer         = {{plainlist|
* Mark J. Gordon
* Eric Spudic
}}
| starring       = {{plainlist|
* Laura Nativo
* Rhoda Jordan
* Edwin Craig
* Ike Gingrich
}}
| music          = Jason Peri
| cinematography = Reinhart Peschke
| editing        = Arturo Tejada
| distributor    = Wildcat Entertainment
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
}}
Aquanoids is a 2003 horror film directed by Reinhart "Ray" Peschke.

==Plot==
In the year 1987, 17 people were killed by humanoid creatures called Aquanoids.  These sea creatures seem to appear randomly and in the year 2003 return to Babylon Bay.  The heroine of the story, Vanessa, sees the creatures and warns the town of the imminent danger.  Vanessas mother was one of the individuals killed in the first wave of attacks sixteen years ago.  But the towns mayor, in an effort to acquire a major land deal for a new mall, tries to disguise the attacks as boating accidents.  
 powered scooter.  When the mayors daughter is killed by an Aquanoid, he covers up the incident and executes the medical examiner.  He even has to abort the Aquanoid baby she is carrying. The film ends with the Aquanoids being killed.

==Cast==
* Laura Nativo as Vanessa DuMont
* Rhoda Jordan as Christina
* Laurence Hobbs as Jackson
* Edwin Craig as Frank Walsh
* Ike Gingrich as Clint Jefferson
* Suzan Spann as Courtney McClure
* Robert Kimmel as Stanze
* Christopher Irwin as Bruce

==Reception==
Jon Condit of  ] rated it 1.5/5 stars and wrote, "In the end, Aquanoids only has two things going for it. The first is the obvious enthusiasm of the cast. The second is a gorgeous young starlet in skimpy outfits. Neither one makes it worth going out your way to see, although the second one does come pretty close." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 