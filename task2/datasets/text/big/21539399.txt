Final Impact (film)
{{Infobox film
| name           = Final Impact
| image          = Final_Impact.jpg
| image_size     =
| caption        =
| director       = Joseph Merhi
| producer       = George H. Shamieh 
| writer         = Stephen Smoke
| narrator       =
| starring       = Mimi Lesseos, Lorenzo Lamas
| music          =
| cinematography =
| editing        =
| distributor    = PM Home Video - Domestic Video Distributor
| released       = 1991
| runtime        =
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
Final Impact is a 1991 martial arts film starring Jeff Langton, Mimi Lesseos, Lorenzo Lamas and Michael Worth.  It was directed by Joseph Merhi. The film was shot in 18 days.

==Plot summary==
Nick Taylor (Lorenzo Lamas) is the kickboxing champion of the world but his reign is cut short by Jake Gerard (Jeff Langton). Gerard is similar to a Clubber Lang type character. Taylor gives up and hes completely destroyed by Gerards brutal victory over him. He retires and remains that way until he comes across a promising newcomer, Danny Davis (Michael Worth). Taylor trains Davis to avenge his title and Davis defeats every fighter in his path. In the end Jake Gerard and Nick Taylor square off in a battle royal on top of a roof lit by neon signs.

==Cast==
*Lorenzo Lamas as Nick Taylor
*Jeff Langton as Jake Gerard
*Mimi Lesseos
*Kathleen Kinmont as Maggie
*Michael Worth  as Danny Davis
*Frank Rivera as Stevie (as Frank Reeves)
*Mike Toney as Joe
*Mimi Lesseos as Roxy
*Kathrin Lautner as Girl in Bar
*Chuck Hull as Ring Announcer
*James M. Williams as Commentator
*Michelle Grassnick as Oil Wrestler
*Erika Nann as Oil Wrestler
*Antoinette Steen as Referee
*Kevin McCaulley as Referee
*Glenn T. Rabago as Cornor Man
*Joe Spinuzzi as Pit Boss
*Tessy Tischler as Dealer
*Joe Hoak as Roulette Dealer
*Diana Phipps as Casher at Club
*Robert Beal as Waiter
*Earle A. Armstrong as Fighter
*Roman Neri as Fighter
*Gary Daniels as (Jimmy) Nicks Club Fighter
*Lance Harris as Fighter
*Ian Jacklin as Fighter
*Azhakd Fariborz Fighter
*Art Camacho as Fighter
*Anthony Gamboa as Fighter
*Darrell Powell as Fighter
*Jon Kinikama as Fighter
*Ken Doyle as Fighter
*Fred Vanden Akker as Fighter
*Miguel Hierro as Fighter
*Joseph L. Salomone as Fighter

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 