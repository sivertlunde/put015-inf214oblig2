Un uomo a metà
{{Infobox film
 | name           = Un uomo a metà
 | image          = Un uomo a metà.jpg
 | caption        =
 | director       = Vittorio De Seta
 | producer       = Vittorio De Seta
 | writer         = Vittorio De Seta Fabio Carpi Vera Gherarducci
 | starring       = Jacques Perrin
 | music          = Ennio Morricone
 | cinematography = Dario Di Palma  
 | editing        = Fernanda Papa
 | distributor    =
 | released       =  
 | runtime        = 93 minutes
 | country        = Italy
 | language       = Italian
 | budget         =
 }} 1966 Cinema Italian drama film  directed by Vittorio De Seta.   The film entered the competition at the 1966 Venice Film Festival, in which Jacques Perrin was awarded with the Volpi Cup for Best Actor. 

==Plot==
While locked up in a mental health clinic, Michele looks back on his life. This includes reflecting on his controlling mother and selfish brother, who influenced Micheles neurosis that makes it difficult for him to establish relationships with women.

== Cast ==
* Jacques Perrin as Michele
* Lea Padovani as Madre di Michele
* Gianni Garko as Fratello di Michele
* Ilaria Occhini as Elena
* Rosemary Dexter as Marina
* Pier Paolo Capponi as Ugo
* Francesca De Seta as Simonetta
* Kitty Swan as Girl at park 
* Ivan Rassimov

==Music==
{{Infobox album |  
| Name        = Un uomo a metà
| Type        = Soundtrack
| Artist      = Ennio Morricone
| Cover       =
| Released    = 1966  (original album) 
| Recorded    =
| Genre       = Soundtrack
| Length      = 23:08
| Label       = RCA Italiana
| Producer    = The Hills Run Red (1966)
| This album  = Un uomo a metà (1966)
| Next album  = How I Learned to Love Women (1966)
}}
All music by Ennio Morricone.

# "Requiem Per Un Destino" – 23:08  (Chorus and orchestra) 

==References==
 

==External links==
* 

 
 
 


 