The Accused (1949 film)
{{Infobox film
| name           = The Accused
| image          = AccusedPoster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = William Dieterle
| producer       = Hal B. Wallis
| screenplay     = Ketti Frings
| based on       =   
| narrator       =  Sam Jaffe
| music          = Victor Young
| cinematography = Milton R. Krasner
| editing        = Warren Low
| distributor    = Paramount Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Sam Jaffe, among others. 

==Plot==
Wilma Tuttle (Young) is college professor who arouses the sexual interest of her student Bill Perry (Douglas Dick). When Perry tries to rape Tuttle, she beats him to death with a tire iron. She covers up her crime by making it seem as though Perry was killed while diving into the sea from a precipitous cliff. As she follows the police investigation of Perrys death, Wilma realizes that shell never be able to escape her conscience, especially when she falls in love with Warren Ford (Cummings), the dead boys guardian.

==Cast==
* Loretta Young as Dr. Wilma Tuttle
* Robert Cummings as Warren Ford
* Wendell Corey as Lt. Ted Dorgan Sam Jaffe as Dr. Romley
* Douglas Dick as Bill Perry
* Suzanne Dalbert as Susan Duval
* Sara Allgood as Mrs. Conner Mickey Knox as Jack Hunter
* George Spaulding as Dean Rhodes
* Francis Pierlot as Dr. Vinson
* Ann Doran as Miss Rice, Nurse
* Carole Mathews as Waitress Billy Mauch as Harry Brice

==Reception==

===Critical response===
The New York Times gave the film a positive review, and wrote, "Murder is a common and salable screen commodity...The Accused, ...is a super-duper psychological job, well spiced with terminology which sounds impressive, if not always crystal clear in meaning, and the performers go about their business with an earnestness which commands attention. Under William Dieterles assured direction, the story flows smoothly and methodically builds up suspense to a punchy climax which leaves it to the audience to determine whether the defendant should be punished or go free." 

The staff at Variety (magazine)|Variety magazine gave the film a good review.  They wrote, "The Accused exploits fear and emotional violence into a high grade melodrama...Director William Dieterle, with a solid story foundation and an ace cast upon which to build, marches the melodrama along with a touch that keeps punching continually at audience emotions...Loretta Youngs portrayal of the distraught professor plays strongly for sympathy. Its an intelligent delineation, gifting the role with life. She gets under the skin in bringing out the mental processes of an intelligent woman who knows she has done wrong but believes that her trail is so covered that murder will never out." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 