Argentinísima
{{Infobox film
| name           = Argentinísima
| image          = Argentinísimaposter.jpg
| image size     =
| caption        = Promotional Poster
| director       = Fernando Ayala  Héctor Olivera
| producer       =
| writer         = Félix Luna  Marcelo Simón	
| narrator       =
| starring       = Santiago Ayal   Domingo Cura
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 114 minutes
| country        = Argentina
| language       = Spanish
| budget         =
}} Argentine musical musical documentary film directed by Fernando Ayala and Héctor Olivera written by Félix Luna. The film premiered on 8 July 1972 in Buenos Aires.
 Argentine folklore, many of which are accompanied by dancing. Several sequences were filmed in scenic locations throughout the country.

Argentinísima II was released the following year in 1973.

==Cast==
*Santiago Ayala
*Jorge Cafrune
*Los Chalchaleros
*Tránsito Cocomarola
*Jovita Díaz
*Eduardo Falú
*Ramona Galarza
*Horacio Guarany
*Nélida Lobato
*Julio Marbíz
*Ástor Piazzolla
*Los Quilla-Huasi
*Ariel Ramírez
*Mercedes Sosa
*Norma Viola
*Atahualpa Yupanqui
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 


 
 