Wild Life (film)
 
{{Infobox film
| name           = Wild Life
| image          = Wild Life (2011 film) poster.jpg
| caption        = Amanda Forbis Wendy Tilby
| producer       = Marcy Page Bonnie Thompson David Verrall  (Executive producer|exec.)  David Christensen  (exec.) 
| writer         = Amanda Forbis Wendy Tilby
| narrator       = Adam Blackwood Luba Goy Anthony Bekenn Keith Dinicol Colin Fox Nonnie Griffin Ben Carlson Amy Rutherford
| music          = Judith Gruber-Stitzer
| cinematography =
| editing        =
| studio         = National Film Board of Canada
| distributor    =
| released       =  
| runtime        = 13 minutes 30 seconds
| country        = Canada
| language       = English
| budget         =
| gross          =
}} animated short Best Animated Best Animated Short Subject at the 39th Annie Awards  as well as a Genie Award for Best Animated Short at the 32nd Genie Awards.   

Based in Calgary, Tilby and Forbis were previously nominated for an Academy Award for Best Animated Short Film for their 1999 NFB film When the Day Breaks, with Tilby also nominated individually for her 1991 NFB short Strings (1991 film)|Strings.   

Wild Life was produced by Marcy Page and Bonnie Thompson.   

==Plot==
The film follows the story of a dapper young remittance man sent to Alberta from England in 1909, with disastrous results.  

==Production==
In addition to writing and directing the film, Forbis and Tilby drew and painted every animation frame in guache, and wrote the lyrics for the films final song. They were only able to work on Wild Life part-time, due to commercial obligations, and the film is reported to have taken them from six to over seven years, from concept to completion.    

==Awards== Best Animated Best Animated Short Subject at the 39th Annie Awards as well as a Genie Award for Best Animated Short at the 32nd Genie Awards. The short won three honors at the 38th Annual Alberta Film and Television Awards in Calgary, including the Rosie for Best Short. Forbis and Tilby also won the Rosie for Best Animator(s) or Motion Graphic Artist(s), while the award for Best Overall Sound — Drama went to Wild Life creative crew David J. Taylor and Pat Butler.   

== See also ==
* Dimanche (film)|Dimanche

== References ==
 

== External links ==
*   at the National Film Board of Canadas website where the film can currently be watched in its entirety.
*  
*  
*  
* , a video interview with Forbis and Tilby at NFB website

 
 
 
 
 
 
 
 
 
 
 