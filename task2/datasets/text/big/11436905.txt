Infrared Sightings
{{Infobox film
| name        = Infrared Sightings
| image       = GDInfraredSightingsVHS.jpg
| caption     = Infrared Sightings video tape cover
| director    = Len DellAmico Larry Lachman
| producer    = Len DellAmico Larry Lachman
| music       = Grateful Dead
| distributor = Trigon Productions
| released    =  
| runtime     = 18 minutes
| country     = United States
| language    = English
}}
Infrared Sightings is a video by the Grateful Dead, consisting of computer animation and other imagery set to music from their album Infrared Roses. It was released on VHS video tape and on laserdisc in 1992, and is 18 minutes long.
 improvisational music recorded live at a number of different Dead concerts.

The visuals for Infrared Sightings combine computer generated images, many of them abstract art|abstract, with found footage that has been altered or edited in various ways. The video is therefore somewhat reminiscent of the light shows that were projected on large screens at many Grateful Dead concerts.

==Track listing==
* intro – FreeQuency Beach
* part 1 – Underwatermelons (music: "Riverside Rhapsody")
* part 2 – Synchronations (music: "Post-Modern Highrise Table Top Stomp")
* part 3 – Yes I Kandinsky (music: "Infrared Roses")

==Credits==

===Musicians=== guitar
* percussion
* Bill Kreutzmann – drums, percussion bass
* keyboards
* Bob Weir – guitar

====With====
* Bob Bralove
* Willie Green III

===Production===
* Len DellAmico, Larry Lachman – directors, producers
* Fritz Perlberg – executive producer
* Bob Bralove – music producer
* Maury Rosenfeld — spatio-temporal opto-orchestrator
* Fred Raimondi – visual effects and creative entity
* Sam Hamann – electronic paintmeister
* David Tristram – electropaint graphics programmer/artist
* Steve Burr, "Mad" Johnny Modell – intro sound design
* Quency – special animated appearance

==See also==
*  

==References==
 
*  
*   at the Grateful Dead Family Database
*   at LaserDisc Database
* Scott, John W; Dolgushkin, Mike; Nixon, Stu. DeadBase XI: The Complete Guide to Grateful Dead Song Lists, 1999, DeadBase, ISBN 1-877657-22-0, p.&nbsp;122
 

==External links==
*  

 

 
 
 