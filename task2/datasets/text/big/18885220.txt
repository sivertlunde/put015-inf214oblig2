The Birthday Present
 
The Birthday Present is a 1957 British drama film directed by Pat Jackson. It stars Tony Britton, a top toy salesmen returning from a business trip to Germany with a watch hidden inside a toy, intended as a birthday present for his wife Sylvia Syms. He is caught by customs, arrested, and sentenced to three months in jail - in the process losing his job. The second half of the film involves him rebuilding his life.

It also featured Thorley Walters and Ian Bannen in small roles.

==Cast==
* Tony Britton as Simon Scott 
* Sylvia Syms as Jean Scott 
* Jack Watling as Bill Thompson 
* Walter Fitzgerald as Sir John Dell 
* Geoffrey Keen as Colonel Wilson 
* Howard Marion-Crawford as George Bates  John Welsh as Chief customs officer
* Lockwood West as Mr. Barraclough  
* Harry Fowler as Charlie 
* Frederick Piper as Careers Officer  
* Cyril Luckham as Magistrate  
* Thorley Walters as Photographer  
* Ernest Clark as Barrister

==External links==
*  

 
 
 
 
 
 


 