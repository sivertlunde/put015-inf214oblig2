List of Telugu films of 1997
 
  Tollywood (Telugu Hyderabad in the year 1997 in film|1997.

==1997==
{| class="wikitable"
|-
! Title !! Director !! Cast !! Music director !! Sources
|-
|Aahwanam|| S. V. Krishna Reddy || Meka Srikanth|Srikanth, Ramya Krishna, Heera Rajagopal || S. V. Krishna Reddy ||
|-
|Aaro Pranam || K. Veery || Vineeth, Soundarya, S. P. Balasubramaniam, Lakshmi || K. Veeru ||
|- Koti ||
|-
|Anaganaga Oka Roju || Ram Gopal Varma || J. D. Chakravarthy, Urmila Matondkar, Raghuvaran, Brahmanandam || Sri ||
|- Sujatha || M. M. Keeravani ||
|- Deva ||  
|- Rajendra Prasad Ramya Krishna || ||
|-
|Chinnabbai|| K. Viswanath || Daggubati Venkatesh, Ramya Krishna, Ravali, Srividya || Ilayaraja ||
|- Koti ||
|- Koti ||
|-
|Egire Paavurama || S. V. Krishna Reddy || Meka Srikanth|Srikanth, Laila Mehdin, J. D. Chakravarthy, Suhasini Maniratnam || S. V. Krishna Reddy ||
|- Pawan Kalyan, Koti ||
|- Rajendra Prasad, Rambha ||Saluri Koti ||
|-
|Iruvar|Iddaru || Mani Ratnam || Mohan Lal, Aishwarya Rai, Prakash Raj, Tabu (actress)|Tabu, Revathi Menon, Gouthami Tadimalla, Nasser, Delhi Ganesh, Madhoo || A. R. Rahman ||  
|- Srikanth || Koti ||
|- Suresh Krishna Vijayakumar || Deva ||
|- Merupu Kalalu || Rajeev Menon || Arvind Swamy, Kajol, Prabhu Deva, Nasser || A. R. Rahman ||  
|- Koti ||
|-
|Omkaram || Upendra || Rajasekhar (actor)|Rajasekhar, Prema (actress)|Prema, Bhagyashree || Hamsalekha ||
|- Rami Reddy || Vandemataram Srinivas ||
|- Saluri Koteswara Koti ||
|-
|Pelli (film)|Pelli || Kodi Ramakrishna || Vadde Naveen, Maheshwari, Sujatha (actress)|Sujatha, Pruthviraj || S. A. Rajkumar ||
|- Saluri Koteswara Koti ||
|-
|Preminchukundam Raa || Jayanth C. Paranjee || Daggubati Venkatesh, Anjala Zaveri || Mahesh Mahadevan||
|- Abbas || Koti ||
|-
|Priyam (1996 film)|Priyam || N Pandiyan || Arun Vijay, Raasi, Vadivelu, Prakash Raj || Vidhyasagar ||  
|-
|Rakshakudu || Pravin Gandhi || Nagarjuna Akkineni, Sushmita Sen, S. P. Balasubramaniam, Girish Karnad, Raghuvaran || A. R. Rahman ||  
|-
|Subhakankshalu || Bhimaneni Srinivasa Rao || Jagapathi Babu, Mantra (actress)|Mantra, Raasi || S. A. Rajkumar, Saluri Koteswara Rao ||
|-
|Suswagatam || Bhimaneni Srinivasa Rao || Pawan Kalyan, Devayani, Raghuvaran || S. A. Rajkumar ||
|-
|Veedevadandi Babu || E. V. V. Satyanarayana || Mohan Babu, Shilpa Shetty, Brahmanandam || Sirpy ||
|-
|Ugadi ||  S. V. Krishna Reddy || S. V. Krishna Reddy, Laila Mehdin || S. V. Krishna Reddy||
|-
|}

==References==
 
 
 
 

 
 
 