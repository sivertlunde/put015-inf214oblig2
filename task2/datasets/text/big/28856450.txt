Flight from Folly
{{Infobox film
  | name           = Flight from Folly
  | image          = 
  | image_size     = 
  | caption        = 
  | director       = Herbert Mason
  | producer       = Herbert Mason
  | writer         = Lesley Storm Katherine Strueby Edmund Goulding Hugh Sinclair
  | music          = Benjamin Frankel Eric Spear Edmundo Ros
  | cinematography = Otto Heller
  | editing        =  Warner Brothers-First First National Productions
  | released       =  
  | runtime        = 94 minutes
  | country        = United Kingdom
  | language       = English
  }}
 Hugh Sinclair, with music from Edmundo Ros and his famous Rumba Band.  Kirkwood had appeared in minor roles in four films between 1938 and 1940 before focussing her career on the West End stage, where she had become a major star during the war years.  Flight from Folly was designed to give Kirkwood her first starring screen role, with the hope of breaking her out as a big-name film attraction.

==Plot==
When his muse and girlfriend Nina (Tamara Desni) takes off with a continental lothario, composer and playwright Clinton Clay (Sinclair) is devastated and turns to drink for solace.  His doctor (Sydney Howard) tries, with the help of Clintons butler Neville (A. E. Matthews), to get him to pull himself together but all attempts fail as Clintons behaviour becomes ever more unbalanced and every nurse they engage is sent on her way by him in quick order. 

Showgirl Sue Brown (Kirkwood) is currently out of work, hears of Clintons problems and poses as a nurse.  She is taken on to be his keeper, and manages to placate him to the extent that he does not dismiss her.  When Clinton decides to travel to Majorca in pursuit of Nina, Sue is included in the party along with Neville and Clintons sculptor sister Millicent (Jean Gillie).  Harriet (Marian Spencer), a devious widow with designs on Clinton, follows them to Majorca.  

Once on the island, Clinton tracks Nina down and asks her to star in a tryout of a new musical he has written.  She agrees, and Clinton makes arrangements to stage the musical there.  On opening night however, the jealous Harriet locks Nina in her dressing room and disappears with the key.  Sue offers to take Ninas place on stage, and proves to be a huge success with the audience.  Clinton realises that he has fallen in love with her and is instantly cured of his malaise, happy now to let Nina go with her playboy lover.

==Cast==
* Patricia Kirkwood as Sue Brown Hugh Sinclair as Clinton Gray
* Tamara Desni as Nina
* Sydney Howard as Dr. Wylie
* Jean Gillie as Millicent
* A. E. Matthews as Neville
* Charles Goldner as Ramon
* Marian Spencer as Harriet
* Leslie Bradley as Bomber
* Edmundo Ros as Himself

==Reception and later history== Manchester Guardian called the film "unworthy of   limited but genuine talent" which "promises better work in better films".     The Daily Mirror however found the film a "neatly made and tuneful comedy" with praise for Kirkwoods "vivacious personality and talent".   The films set designs, costuming and make-up provoked criticism from a number of reviewers, with the Daily Mail commenting "the dressing of the picture... explores new regions of banal ugliness", while also remarking facetiously that Kirkwoods attempts at dancing the rumba "evoked Harringay rather more than Havana". 
 British Film 75 Most Wanted" list of missing British feature films. 

==See also==
*List of lost films

==References==
 

==Bibliography==

* David Quinlan (film critic)|Quinlan, David. (1984). British Sound Films: The Studio Years 1928-1959. BT Batsford Ltd

==External links==
*  

 

 
 
 
 
 
 
 
 