La Garce
{{Infobox film
| name           = La Garce
| image          = 
| caption        = 
| director       = Christine Pascal
| producer       = Alain Sarde
| writer         = André Marc Delocque-Fourcaud Pierre Fabre Christine Pascal
| starring       = Isabelle Huppert
| music          = 
| cinematography = Raoul Coutard
| editing        = 
| distributor    = 
| released       = 5 September 1984
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
}}

La Garce is a 1984 French thriller film directed by Christine Pascal and starring Isabelle Huppert.   

==Cast==
* Isabelle Huppert - Aline Kaminker / Édith Weber Richard Berry - Lucien Sabatier
* Vittorio Mezzogiorno - Max Halimi
* Jean Benguigui - Rony
* Jean-Claude Leguay - Brunet
* Jean-Pierre Moulin - Cordet
* Clément Harari - Samuel Weber
* Daniel Jégou - Dujarric
* Jenny Clève - Madame Beffroit
* Jean-Pierre Bagot - Monsieur Beffroit
* Madeleine Marie - Gouvernante de Weber
* Bérangère Bonvoisin - La femme de Lucien
* Mado Maurin - Madame Pasquet
* Philippe Fretun - Didier
* Brigitte Guillermet - Secrétaire de Brunet
* Stéphanie Seilhean - Edwige
* Michèle Moretti - La fille rousse
* Vicky Messica - Contre-maître Nando
* Julie Malbequi - Petite fille

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 