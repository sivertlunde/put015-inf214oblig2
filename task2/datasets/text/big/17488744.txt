The Mark of Cain (2000 film)
 
{{Infobox film
  | name =The Mark of Cain
  | image =
  | caption =
  | director = Alix Lambert
  | producer =  Go East Productions
  | writer =
  | starring  =
  | cinematography =
  | music =
  | editing =
  | distributor =
  | released = 2000
  | runtime = 73 min (French version)
  | country = Russia
  | language = Russian
  | budget =
    }}

The Mark of Cain is a 2000 documentary film on Russian criminal tattoos directed by Alix Lambert.

==Content== White Swan, The Code of Thieves” (Vor v zakone).

The prisoners of the Stalinist Gulag, or "Zone," as it is called, developed a complex social structure (documented as early as the 1920s) that incorporated highly symbolic tattooing as a mark of rank.  The existence of these inmates at prisons and forced labor camps was treated by the state as a deeply kept secret. In the 1990s, Russias prison population exploded, with overcrowding among the worst in the world. Some estimates suggest that in the last generation over thirty million of Russias inmates have had tattoos even though the process is illegal inside Russian prisons.

The Mark of Cain examines every aspect of the tattooing, from the actual creation of the tattoo ink, interviews with the tattooers and soberly looks at the double-edged sword of prison tattoos. In many ways, they were needed to survive brutal Russian prisons, but mark the prisoner for life, which complicates any readmission to “normal” society they may have.   Tattoos expressly identify what the convict has been convicted of, how many prisons he’s been in and what kind of criminal he is. Tattoos, essentially, tell you everything you need to know about that person without ever asking. Each tattoo represents a variety of things; cupolas on churches represent the number of convictions a convict has, epaulets tattooed on shoulders represent the rank of the individual in the crime world and so on and so forth.
 Drug resistant tuberculosis runs rampant through the prison populations and prisoners are served three meals a day of watery slop. There are allegations of brutality by the guards. As these men deal with pestilence, violence and grossly substandard living conditions, the prison guards and administration put on a talent show.

The film served as source material for David Cronenberg’s 2007 dramatic movie, Eastern Promises. He commented,
"This is a very courageous documentary on the tattooing subculture in Russian prisons. I dont know how it ever got made, but its beautiful, scary, and heartbreaking."

==Distribution==
The Mark of Cain was released in North America in 2008 by Microcinema International.

==Awards and nominations==
2001
*Créteil International Womens Film Festival, "AFJ Documentary Award - Special Mention" (Won, Alix Lambert)
2002
*Independent Spirit Awards, "Truer Than Fiction Award" (Nominated, Alix Lambert)

==External links==
* 
* Go East Film  
* Microcinema International  
* http://www.youtube.com/watch?v=s9JDJdaMs-Y

==See also==
*Thieves by Law

 
 
 
 
 
 