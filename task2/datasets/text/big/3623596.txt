The Grand Substitution
 
 
 
The Grand Substitution is a 1965 Chinese movie of the Huangmei Opera genre. it is based on The Orphan of Zhao.

==Synopsis== patriotic physician put to death. The Prince Consort, a Chao scion, was thus ordered to commit suicide by the sword. Physician Cheng knew that Princess Chuang Chi was pregnant, promised to bring the baby away when it was born. He managed to sneak the baby out of the palace when the time came but the evil Tu, who had been waiting for the birth, initiated a hunt for the child. He ordered the next generation of the populance put to the sword unless the child was brought to him.

Cheng was caught in a dilemma for it meant his son who was born at the same time, was in peril. He also had to uphold his promise. With grief, he decided to substitute the last surviving member of the Chao family with his own son. Kung Sun proposed a plan to ensure the safety of Cheng and his wife along with the Chao orphan. Thus, another two innocent lives were added to Tus bloodlist. A pleased Tu then put Cheng under his protection and adopted his baby son as his fosterling.
 birthday party thrown by Cheng and was cornered by Wus soldiers. Chao Wu and his mother took the opportunity to exact their revenge.

==12th Asian Film Festival==
* Best Film
* The Most Versatile Talent - Ivy Ling Po

==Huangmei Opera Vocals==
* Ivy Ling Po - Chao Wu
* Liu Yun - Princess Chuang Chi
* Kiang Hung - Physician Cheng

==Cast==
* Princess Chuang Chi - Li Li-hua
* Chao Wu - Ivy Ling Po
* Physician Cheng Ying - Yen Chun
* Madam Cheng - Chen Yen-yen
* Kung Sun - Yang Chih-ching
* Prime Minister Chao - Ching Miao
* Minister Tu - Li Ying

 
 
 
 
 
 
 

 