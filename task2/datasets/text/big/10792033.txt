Thambikku Entha Ooru
{{Infobox Film |		
| name = 	Thambikku Entha Ooru	
| image = 	Thambikku Entha Ooru dvd.jpg	
| caption = 	Official DVD cover	 Rajasekhar	
| producer = 	Meena Panju Arunachalam	
| writer = 	Panchu Arunachalam
| narrator =		
| starring = 	 
| music = 	Ilaiyaraaja
| cinematography = V. Ranga
| editing = R. Vittal
| studio = P. A. Art Productions
| distributor = P. A. Art Productions		
| released  =	20 April 1984
| runtime = 	165 min	
| country =	 India
| language = 	Tamil	
| budget = 		
| gross = 		
| preceded_by = 		
| followed_by =		
}}		
		
Thambikku Entha Ooru ( ;  ) is a 1984 Indian Tamil language film directed by Rajasekhar (director)|Rajasekhar, starring Rajinikanth, Madhavi (actress)|Madhavi, Sulakshana (actress)|Sulakshana, Sathyaraj and Senthamarai.    The music was composed by Ilaiyaraaja.   

It was remade in Kannada as Anjada Gandu with V Ravichandran.
		
==Plot==		
The film begins by showing Rajni as a lavish spendthrift, with a cavalier attitude towards life. Born to a rich father played VS Raghavan, Rajni has a very rampant and violent behaviour. Worried about Rajnis behaviour, his father decides to send him to his friend Gangadharans village, Sendhamarai  to work for him with a condition that Rajni will not reveal that he is son of gangadharan. Rajni develops a good relation with gangadharan and his family. During the course of his time, he develops a liking for the arrogant rich girl Madhavi (actress)|Madhavi. Madhavi assuming Rajni to be a poor villager humiliates him all the time. In a cat fight Rajni kisses Madhavi. From this point Madhavi develops a liking for Rajni and eventually they fall in love. In a triangular story line, Gangadharans daughter played by Sulokshana, also falls in love with Rajni, however Rajni advices and also reveals that he is in love with Madhavi. A dejected Sulokshana agrees to marry a man selected by her father, played by Sathyaraj. Madhavis father played by Vinu Chakravarthi coming to know about her love for Rajni, decides to get her married by force to Nizhalgal Ravi. Rajni rescues her and takes refuge in Satyarajs house. However Sathyaraj turns her over back to the Nizhalgal Ravi who is villainous. In a final fight sequence Rajni rescues Madhavi from Nizhalgal Ravi and returns her to her father. Rajni then leaves the village after learning hard work, discipline and many other good virtues from Gangadharan. The final scene shows Madhavi and her father at Rajnis father house discussing marriage for Madhavi. Madhavi trying to refuse suddenly realizes that she is actually in Rajnis house and to everyones surprise Rajni clad in a full suit walks down the stairs with the credits starts rolling.

==Cast==
* Rajinikanth Madhavi
* Sulakshana
* Senthamarai
* V. S. Raghavan
* Vinu Chakravarthy Srikanth
* Nizhalgal Ravi
* Sathyaraj Janagaraj
* Master Vimal
* Ennatha Kannaiya
* Vani
* Kovai Sarala

==Soundtrack==
Soundtrack is composed by Ilayaraja. The song "En Vaazhvile" was reused from Hindi song "Hey Zindagi" which Ilayaraja composed for Sadma, hindi remake of Moondram Pirai. The song "Kadhalin Deepam" is based on Charukesi raga and remains one of the famous songs from the film.   Ilayaraja was supposed to sing this song but unable to do due to hernia operation, Raja then recorded in a tape recorder by whistling and sent it to SPB.  The song "Aasaikliye" is based on Arabhi Raga. 
		
{{Infobox album  		
| Name =	Thambikku Entha Ooru	
| Type =	soundtrack	
| Longtype =		
| Artist =	Ilaiyaraaja	
| Cover =		
| Cover size =		
| Caption =		
| Released =	 	
| Recorded =		
| Genre =		
| Length =       Tamil	
| Label =		
| Producer =		
| Reviews =		
| Compiler =		
| Misc =		
}}		

{{tracklist	
| collapsed =	
| headline =	
| extra_column =	Playback Singers
| total_length =	
	
| all_writing =	
| all_lyrics =	Panchu Arunachalam
| all_music =	Ilaiyaraaja 
	
| writing_credits =	
| lyrics_credits =	
| music_credits =	
	
| title1 =	Aasaik Kiliye
| note1 =	
| writer1 =	
| lyrics1 =	
| music1 =	
| extra1 =	Malaysia Vasudevan
| length1 =	
	
| title2 =	En Vaazhvile Varum Anbe Vaa
| note2 =	
| writer2 =	
| lyrics2 =	
| music2 =	
| extra2 =	S. P. Balasubramaniam
| length2 =	
	
| title3 =	Kaathalin Deepam Onru I
| note3 =	
| writer3 =	
| lyrics3 =	
| music3 =	
| extra3 =	S. P. Balasubramaniam
| length3 =	
	
| title4 =	Kaathalin Deepam Onru II
| note4 =	
| writer4 =	
| lyrics4 =	
| music4 =	
| extra4 =	S. Janaki
| length4 =	
	
| title5 =	Kalyaana Mela Saththam
| note5 =	
| writer5 =	
| lyrics5 =	
| music5 =	
| extra5 =	S. Janaki
| length5 =	
	
}}

==References==		
 

==External links==
*  		
*  

 
 
 
 
 
 
 