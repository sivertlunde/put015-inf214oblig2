Pehchaan (1993 film)
{{Infobox film
| name                 = Pechchaan
| image                = PehchaanShetty.png
| caption              = CD Cover
| director             = Deepak Shivdasani
| producer             = A.K.Abdul Jeevat A.T	
| writer               = Robin Bhatt Javed Siddiqi Sujit Sen 
| narrator             =
| starring             = Sunil Shetty Saif Ali Khan Shilpa Shirodkar Madhoo
| music                = Anand Milind
| cinematography       = Thomas A. Xavier	
| editing              = A. Muthu
| distributor          = A J Combines
| released             = 8 October 1993
| runtime              = 138 mins
| country              = India
| language             = Hindi
| budget               =
| gross                =
}}
 Indian Bollywood film directed by Deepak Shivdasani and produced by A.K. Abdul. It stars Sunil Shetty, Saif Ali Khan, Shilpa Shirodkar and Madhoo in pivotal roles. 

== Plot ==
Judge Jagdish Verma (Raza Murad) has an accused by the name of Yogi Shankar (Kiran Kumar) in his courtroom. Judge Verma was once Yogis defence lawyer, but now must pass a sentence against him. Yogi pleads his innocence, but the Judge finds him guilty, and Yogi is sentenced to a long term (7 years) in jail. Yogi swears to avenge this injustice and humiliation. Judge Vermas family consists of Urmila (Beena Banerjee), two sons, Kunal and Karan (Sunil Shetty and Saif Ali Khan respectively), and a daughter Tina (Madhoo). Yogi serves out his term and is released from jail, seeking revenge against Juge Verma and his family. He abducts young Tina and plans to use her to control the judge and his judgments. The family is instructed not to contact the police, for the safety of Tina, and to keep this abduction a secret. Tina grows up in the company of Yogi and his men, without knowing her biological family. Tina is then used to ensnare the family into more deceit and entrapment under the direction of Yogi, leaving the Verma family no choice but to go along with Yogis plan.

==Cast==
* Sunil Shetty...Kunal Verma
* Saif Ali Khan...Karan Verma
* Shilpa Shirodkar...Seema
* Madhoo...Tina
* Kiran Kumar...Yogi Shankar
* Siddharth Ray...Robert
* Raza Murad...Judge Jagdish Verma
* Avtar Gill...Veeru
* Beena Banerjee...Urmilla Verma
* Lalit Tiwari

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tu Mere Dil Mein Rehti Hai" Abhijeet Bhattacharya|Abhijeet
|-
| 2
| "Ankhon Mein Kya"
| Abhijeet, Kavita Krishnamurthy
|-
| 3
| "Sanam O Sanam"
| Abhijeet, Kavita Krishnamurthy
|-
| 4
| "Nazuk Nazuk Nazuk"
| Udit Narayan, Alka Yagnik
|-
| 5
| "Dheere Dheere Nazar Ladne De" Poornima
|-
| 6
| "Log Aate Hai"
| Poornima
|}

==References==
 

== External links ==
*  

 
 
 


 