Live Forever: The Rise and Fall of Brit Pop
{{Infobox film
| name           = Live Forever
| image          = Live Forever poster.png
| caption        = UK theatrical poster by Damien Hirst
| director       = John Dower
| producer       = John Battsek
| writer         = John Dower
| starring       = Noel Gallagher Liam Gallagher Damon Albarn Jarvis Cocker
| editing        = Jake Martin BBC
| released       = 21 March 2003
| runtime        = 82 min.
| country        = UK
}} 2003 documentary John Dower.  The documentary is a study of popular culture in the United Kingdom during the mid to late 1990s.  The focus of the piece is British popular music (Britpop), which underwent a resurgence during the mid-1990s and then seemingly retreated with similar haste towards the end of that decade.
 Noel & Blur and 3D from Massive Attack, Louise Wener from Sleeper (band)|Sleeper, fashion designer Ozwald Boateng and modern artist Damien Hirst.

== Live Forever Compilation Album ==
The compilation album Live Forever – The Best Of Britpop was issued on the Virgin TV label in conjunction with the documentarys theatrical release. It features songs from the film and other notable artists of the Britpop era.

=== Tracklisting ===

==== CD 1 ==== Oasis
# Common People" Pulp
# Blur
# "Alright (Supergrass song)|Alright" – Supergrass Ash
# Waking Up" – Elastica Mulder and Catatonia
# Cast
# "The Changingman" – Paul Weller Stupid Girl" Garbage
# Everything Must Go" – Manic Street Preachers
# "The Riverboat Song" – Ocean Colour Scene Sleeper
# "Tattva (song)|Tattva" – Kula Shaker Come Back Embrace
# Wide Open Space" – Mansun
# "6 Underground" – Sneaker Pimps Space
# "Youre Gorgeous" – Babybird
# "Angels (Robbie Williams song)|Angels" – Robbie Williams

==== CD 2 ====
# "Protection (Massive Attack song)|Protection" – Massive Attack
# "Street Spirit (Fade Out)" – Radiohead
# "Stars" – Dubstar
# "The More You Ignore Me, The Closer I Get" – Morrissey Suede
# The Life of Riley" – The Lightning Seeds Sleeper
# "King of the Kerb" – Echobelly Getting Better" – Shed Seven Ready To Go" – Republica
# "Setting Sun"  – The Chemical Brothers Placebo
# "Breathe (The Prodigy song)|Breathe" – The Prodigy
# "Weak (Skunk Anansie song)|Weak" – Skunk Anansie Underworld
# "Loaded (Primal Scream song)|Loaded" – Primal Scream
# "Step On" – Happy Mondays The Charlatans Oasis

== External links ==
*  
*  , BBC News, 3 March 2003.

 
 
 
 
 
 