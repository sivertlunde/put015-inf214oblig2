Kayal (film)
{{Infobox film
| name           = Kayal
| image          = 
| alt            =  
| caption        = Tsunami doesnt break love...
| director       = Prabhu Solomon
| producer       = Madhan James
| writer         = Prabhu Solomon
| starring       = Chandran Anandhi Vincent
| music          = D. Imman
| cinematography = Vetrivel Mahendran
| editing        = Samuel
| studio         = God Pictures
| distributor    = Escape Artists Motion Pictures
| released       =  
| runtime        = 135 minutes
| country        = India
| language       = Tamil
| budget         =   
| gross          =  
}} Tamil  romance film directed by Prabhu Solomon and produced by Madhan of Escape Artists Motion Pictures. The film features newcomers Chandran, Anandhi and Vincent in the lead roles, while D. Imman composed the films music. The film, set against the backdrop of the 2004 Indian Ocean earthquake and tsunami,   released on 25 December 2014.<!--. 
-->
==Cast==
* Chandran as Aaron
* Anandhi as Kayalvizhi
* Meghna Vincent as Meghna
* Vincent as Socrates
* Gemini Rajeshwari Aarthi
* Divya Prabha as Divya
* Meghana Vincent as Meghna
* Yaar Kannan as Prabhakar
* Bharathi Kannan as Mani
* Jacob
* Devaraj as Deva
* Florent Pereira
* Vetrivel Raja
* Balasubramanian
* Mime Gopi
* Emey Prabhu as Ramkumar (guest appearance)

==Production== Rakshita was rechristened as Anandhi for the film, and was selected after auditioning twice before impressing Solomon.  The shoot of the film commenced in Ponneri in September 2013 and the team announced that scenes would be shot all across India including scenes at Kanyakumari  and Ladakh.  Scenes were shot for several days underwater, with the team often doing up to ten hours a day in knee-length depths of water.  The film completed shoot after 85 days in May 2014, with the director announcing that post-production would be extensive as a result of impending VFX works.   After filming finished, Prabhu Solomon revealed that the films climax would show the 2004 Indian Ocean earthquake and tsunami and noted that the film would be dedicated to victims families. 

During the music release of the film in November 2014, Prabhu Solomon revealed more details about the production of the film. He noted that the climax was shot first to ensure graphic works depicting the tsunami could have as much time spent on it as possible. He went on to add that it was his costliest production till date and the film was made at a cost of  15 crore, with the special effects, notably the use of 7.1 Atmos mix for the climax, being particularly expensive.    

==Release==
===Critical reception===
The film opened on December 25, 2014 to critical acclaim and won positive reviews. Rediff gave 3 stars out of 5, calling Kayal "a sweet and endearing tale of love with characters that touch your heart, haunting locales, beautiful music and emotions that are simple and true. A thoughtfully-written script packed with honest emotions, plenty of humour, potent dialogues and excellent performances make Kayal worth a watch".  Sify called the film a "feel-good love story" and went on to add that it has "some terrific visuals, great music and stunning climax. Prabhu Solomon has delivered an irresistible love story between an innocent girl and a free spirited young man set against the backdrop of Tsunami". 

The Hindus Baradwaj Rangan wrote, "The central emotion..., the great love between Aaron and Kayal, is too wispy to warrant all this drama, which is constantly underlined by a score that just won’t stop. We’re meant to feel their pain, their pining, but all we feel is the film straining to be an epic".  The Times of India gave the film 3 stars out of 5 and wrote, "The first half of Kayal has some of the better aspects you find in a Prabu Solomon film...However, the directors bad habits start creeping in midway into the film and things start getting implausible and heavy-handed", going on to state that "the use of the 2004 Indian Ocean tsunami, which is the backdrop the director has chosen to tell this story, feels exploitative as the tragedy that followed hardly registers on screen".  Indiaglitz.com, while giving it also 3 out of 5, wrote, "the movie got loads of positives and it is watchable for sure, but it also makes the viewer think, how long will Prabhu Solomon takes the same route to achieve success?" 

Cinemalead rated a 3.5 out of 5 and reviewed," Kayal is a technically well-made film, which was equally backed up by some awesome performances. A hat trick for Prabhu Solomon." 

==Soundtrack==
{{Infobox album 
| Name       = Kayal
| Type       = Soundtrack
| Artist     = D. Imman
| Cover      = 
| Border     = yes
| Alt        =
| Caption    = Front CD Cover
| Released   =  
| Recorded   = 2014
| Genre      = Film soundtrack
| Length     = Tamil
| Label      = Sony Music
| Producer   = D. Imman
| Last album =Jeeva (2014 film)|Jeeva (2014)
| This album =Kayal (2014)
| Next album =Vellaikaara Durai (2014)
}}
The films music was composed by D. Imman, who collaborated with Prabhu Solomon following previous successful albums in Mynaa (2010) and Kumki (film)|Kumki (2012). The audio was launched on 13 November 2014 at Sathyam Cinemas with the principal cast and crew in attendance, alongside special guests Arya (actor)|Arya, Sivakarthikeyan, Amala Paul and Anjali (actress born 1986)|Anjali.  The album opened to positive reviews from critics, with Behindwoods.com stating that the "Imman - Prabhu combo strikes gold yet again", while acknowledging that "expectations were high". 

{{track listing
| extra_column    = Singer(s)
| total_length    = 26:55
| all_lyrics      = Yugabharathi
| title1          = Paravayaa Parakkurom
| extra1          = Haricharan
| length1         = 3:49
| title2          = Yengirindhu Vandhayo
| extra2          = Shreya Ghoshal
| length2         = 4:08
| title3          = Koodave Varamaadhiri
| extra3          = Alphons Joseph
| length3         = 2:07
| title4          = Yen Aala Paakkaporaen
| extra4          = Ranjith (singer)|K. G. Ranjith, Shreya Ghoshal
| length4         = 4:21
| title5          = Unna Ippo Paakkanum
| extra5          = Haricharan, Vandana Srinivasan
| length5         = 4:23
| title6          = Deeyaalo Deeyalo
| extra6          = Oranthanadu Gopu
| length6         = 3:32
| title7          = Yenga Pulla Irukka
| extra7          = Balram
| length7         = 4:38
}}

==References==
 

== External links ==
*  
 

 
 
 
 