Madame Bovary (1949 film)
 

{{Infobox film 
| name           = Madame Bovary
| image          = madamebovarymovieposter.jpg
| caption        = Theatrical release poster
| director       = Vincente Minnelli	
| producer       = Pandro S. Berman		
| screenplay     = Robert Ardrey
| based on       = Madame Bovary by Gustave Flaubert
| narrator       = James Mason
| starring       = Jennifer Jones James Mason Van Heflin Louis Jourdan
| music          = Miklós Rózsa
| cinematography = Robert H. Planck
| editing        = Ferris Webster
| distributor    = Metro-Goldwyn-Mayer  
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English 
| budget = $2,076,000  .  gross = $2,016,000 
}}
 novel of the same name by Gustave Flaubert. It stars Jennifer Jones, James Mason, Van Heflin, Louis Jourdan, Alf Kjellin (billed as Christopher Kent), Gene Lockhart, Frank Allenby and Gladys Cooper.

It was directed by Vincente Minnelli and produced by Pandro S. Berman, from a screenplay by Robert Ardrey based on the Flaubert novel. The music score was by Miklós Rózsa, the cinematography by Robert H. Planck and the art direction by Cedric Gibbons and Jack Martin Smith.

The film was a project of the Metro-Goldwyn-Mayer studios and Lana Turner was set to star, but when pregnancy forced her to withdraw, Jones stepped into the title role. Production began in December 1948 and the film premiered the following summer.

The story of the adulterous wife who destroys the lives of many presented censorship issues with the Motion Picture Production Code. A plot device which structured the story around author Flauberts obscenity trial was developed to placate the censors. The highlight of the film is an elaborately choreographed ball sequence set to composer Miklós Rózsas lush film score.
 Best Art Direction-Set Decoration in 1950 for Cedric Gibbons, Jack Martin Smith, Edwin B. Willis and Richard Pefferle.   

==Plot==
The film opens with Gustave Flaubert (James Mason) in court to defend his novel, which has been accused of being a "disgrace to France and an insult to womanhood". In order to keep it from being banned, Flaubert tells the story of Madame Bovary from his perspective.

We are introduced to Emma (Jennifer Jones) when she is twenty years old. She lives with her father (Eduard Franz) on a small farm in the country. She is a lonely girl, and books are her only real companions. She fantasizes about love, and convinces herself that she will one day fall madly in love and have a perfect life. One day, Emmas father injures his leg. When the doctor, Charles Bovary (Van Heflin) comes, Emma falls in love with him instantly. Soon enough, Emma and Charles are married. The couple moves into a small house in the town of Yonville in Normandy. Emma is disappointed, but vows to make the house into her dream home. She insists upon decorating the house lavishly, and although Charles makes only a modest salary, he provides Emma with her every wish. Although her home is just how Emma always dreamed, she is still disappointed at her lack of social status, and feels trapped by her less-than-perfect marriage. She insists to Charles that she wants to have a baby boy, so that he will grow up and be able to live his life the way he wants and not be confined to the rank of the husband as women were. Emma is met with even more disappointment when she later gives birth to a baby girl, Berthe. Emma develops an intolerance towards Berthe and is uninvolved in raising her, depending heavily upon the nanny, Félicité (Ellen Corby). Unhappy with her life, Emma embarks upon an affair with Leon Dupuis (Alf Kjellin).

When Charles is invited to a ball at the home of the wealthy Marquis DAndervilliers (Paul Cavanagh), Emma is overjoyed. Charles does not want to go, thinking they wont fit in with the aristocratic crowd, but Emma finally convinces him to attend. Emma is very popular at the ball, wearing an extraordinary gown that is the envy of the party. She is finally living her dream, in the company of high society and popular with many men. Charles, on the other hand, feels very out of place and drinks far too much champagne. Finally, Charles drunkenly tries to dance with Emma. Embarrassed, Emma makes Charles take her home. Afterwards, Emma continues her affair with Leon. However, Leons mother (Gladys Cooper) soon makes Leon move to Paris to attend law school.

Aristocrat Rodolphe Boulanger (Louis Jourdan), whom Emma had met at the ball, moves to Yonville and tries to have an affair with Emma. At first Emma resists, as she is determined to save her marriage. To do this, she asks Charles to perform a revolutionary surgical procedure, correcting the leg of Hyppolyte (Harry Morgan), a crippled man from the village. Performing the procedure successfully would bring Charles immense fame and fortune; however, Charles knows he is not a skilled enough doctor to do it. He bluntly refuses at first, but after he realizes how much Emma wants him to do it, he eventually consents. He prepares for the surgery, and the entire village gathers around his house to cheer him on. However, at the last minute, Charles decides he cant do it. Emma then gives up all hope of happiness with Charles, and plunges into an affair with Rodolphe. She begins to mold his house into the home of her dreams, much like she did with Charles at the beginning of her marriage; however, Rodolphe finds this as nothing but an intrusion of privacy. The two of them plan to elope to Italy. She prepares to leave; however, Rodolphe goes to Italy alone, leaving Emma alone and heartbroken. Emma locks herself in the attic and tries to jump out the window, but Charles stops her just in time. She becomes very sick and stays in her bed for several months.

When Emma recovers, she and Charles travel to Rouen to see the opera. While they are there, they see Leon, who has returned from Paris. Emma stays the night in Rouen, while Charles returns to Yonville. Leon says he is a lawyer now, and has much more money than he did before. However, Emma rejects Leons attempts to renew their affair. Emma returns to Yonville to find out that Charles is out of town to attend his fathers funeral. While he is gone, the conniving town moneylender Lheureux (Frank Allenby) visits Emma to collect the Bovarys huge amount of debts. Lheureux attempts to have Emma repay him with sexual favors; however, she refuses, and tries to borrow money from other people. She first asks Leon for the money. However, Leon admits that he is actually only a clerk at the law firm he works for, and has no money to lend her. Emma then asks Rodolphe, who has returned to Yonville. At first, Emma tries flirting with him; then, she breaks down and begs him for money. Rodolphe refuses to lend it to her. Charles returns home to find an announcement that his property would be sold to repay the debts. Emma breaks into the village apothecary and swallows arsenic. She returns home, and although Charles attempts to save her, Emma dies.

The film then returns to the courtroom, where it ends with Flaubert being acquitted of all charges.

==Cast==
first billed  
 
* Jennifer Jones as Emma Bovary
* James Mason as Gustave Flaubert
* Van Heflin as Charles Bovary
* Louis Jourdan as Rodolphe Boulanger
* Alf Kjellin (billed as Christopher Kent) as Leon Dupuis 
* Gene Lockhart as  J. Homais
* Frank Allenby as Lheureux
* Gladys Cooper as Madame Dupuis John Abbott as Mayor Tuvache
 
* Harry Morgan as Hyppolyte
* George Zucco as Dubocage
* Ellen Corby as Félicité
* Eduard Franz as Rouault
* Henri Letondal as Guillaumin
* Esther Somers as Madame Lefrançois
* Paul Cavanaugh as Marquis DAndervilliers 
* Larry Simms as Justin
* Vernon Steele as Priest
 

==Reception==
According to MGM records the film earned $1,132,000 in the US and Canada and $884,000 overseas resulting in a loss of $910,000. 

==See also==
*1949 in film

==References==
 
*Epstein, Edward J. (1995).  Portrait of Jennifer. New York, Simon & Schuster.  ISBN 0-671-74056-3.
*Minnelli, Vincente (1990). I Remember It Well. New York, Samuel French ISBN 0-573-60607-2.

==External links==
 
*  
*  
*  
*  at Louisejourdan.net
*   
 
 

 
 
 
 
 
 
 
 