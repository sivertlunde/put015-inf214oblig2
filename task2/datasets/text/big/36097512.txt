Sanctimony (film)
{{Infobox film
| name         = Sanctimony
| image        = Sanctimony Film 2000 DVD.png
| caption      = American DVD cover of "Sanctimony"
| genre        = Crime, Horror, Thriller
| runtime      = 83&nbsp;minutes
| director     = Uwe Boll Paul Colichman Shawn Williamson Jeffrey Schenck Mark R. Harris Stephen P. Jarchow James Shavick
| writer       = Uwe Boll
| starring     = Casper Van Dien Michael Paré Eric Roberts
| editing      = David M. Richardson
| music        = Uwe Spies
| studio       = Regent Entertainment 1st Boll Kino Beteiligungs GmbH and Co. KG
| distributor  = Regent Entertainment Pioneer
| budget       = 
| country      = United States
| language     = English
| network      = 
| released     =  
}}
Sanctimony is a crime/horror/thriller film starring Casper Van Dien, Michael Paré and Eric Roberts. It was written and directed by Uwe Boll. The film was released in late 2000. http://www.imdb.com/title/tt0234652/ 

==Background== Jennifer Rubin Michael Rasmussen as Dr. Fricke, Tanja Reichert as Eve, David Millbern as Peter and Birgit Stein as Sandra. 

The film was filmed in Vancouver, British Columbia, Canada. 

Today, the film remains available in America on DVD through the 2001 Geneon release.  In the UK it remains available on the 2001 Metrodome DVD release.  A French import also exists from Elephant Films.  It was released on VHS in Russia via Pyramid Home Video in 2000, whilst VCL Communications released the film in Germany. 

The films two taglines read "How do you stop the killing" and "There is a serial killer in your neighbourhood..." 

==Plot==
In an anonymous American town, Tom Gerrick is a Wall Street Whiz Kid. Hes also a serial killer. Six victims have had their eyes cut out, six their ears cut off and three their tongues removed. By the time he gets to his fourth tongue victim, Gerrick is getting sick of his success. Detectives Jim Renart and Dorothy Smith are under pressure from their superior to capture the killer, and they finally get a break when Gerrick offers himself for questioning. He calls the police himself, claiming to have found the body of the sixteen-year-old girl on the street. He wants to get caught, but first he wants to play with his captors for a while. His initial sensitivity to the crime is soon replaced with a psycho-smirk and a high priced lawyer. By the time the police find out he is the killer, he is on a shooting spree, first on live television during an interviews on "the secret of success", then at his ex-fiancés non-wedding party. Actually catching Gerrick proves to be a difficult matter, especially with the feds set to take over the case in mere hours.    

==Cast==
* Casper Van Dien as Tom Gerrick
* Michael Paré as Jim Renart
* Eric Roberts as the Lieutenant Jennifer Rubin as Dorothy Smith
* Catherine Oxenberg as Susan Renart Michael Rasmussen as Dr. Fricke
* Tanja Reichert as Eve
* David Millbern as Peter
* Birgit Stein as Sandra
* Marnie Alton as Tina
* Crystal Lowe as Virginia Adam Harrington as Hank Richard Leacock as Policeman #1
* David Kaye as Toms Attorney

==Reception==
Allmovie gave a rating of two out of five stars.  Emanuel Levy also gave a two out of five star rating, whilst Michael Szymanski of the International Press Academy gave the film zero stars out of five. 

Digital Retribution gave the film three out of stars, writing "On the surface Sanctimony appears to be a blatant American Psycho rip-off, although thankfully the final product manages to add enough subtle touches to separate itself from the novel by Easton Ellis and its subsequent film adaption. Director Uwe Boll has a great visual style, giving the dark Seattle streets a menacing look, while the cinematography of Mathias Neumann is quite impressive for what is obviously a low budget feature. The musical score from Uwe Spies was also quite impressive, adding some haunting touches to the slick visuals." 

Reviewer M.J. Simpson awarded the film a B+ grade and wrote "It would be very easy (and lazy) to see this as a lower-budget spin on American Psycho, which came out around the same time, but the only similarity is that the killer is a yuppie, and we know his identity pretty much from the start. Sanctimony is a gripping detective thriller - a minor gem, exciting, scary and highly recommended." 

Variety magazine gave a mixed review, stating "Sanctimony" is an iron-jawed, unashamedly generic serial-killer thriller let down only by dialogue thats as awkward as the pics title. Classily lensed in widescreen and tighter than a rattlers tail, this should be a steady rental earner (and possible cult movie) in mature markets -- where the cast of U.S. video regulars will strike chords among buffs -- with some theatrical business elsewhere. Plot springs some surprises both in its outlandishness and in not respecting casting conventions. Otherwise, its purely formulaic, referencing any number of bigger-budgeted predecessors in look and content. Roberts, surprisingly good, and Van Dien handle the dialogue best, and Catherine Oxenburg, as Renarts wife, worst. Rubin, looking harder-faced than in her earlier career, makes a good partner to Pares macho cop. Finale is weak, but otherwise Mathias Neumanns saturated, narrow-depth-of-field lensing is always atmospheric." 

Movie-Vault.com gave the film three out of ten, writing "What an awful movie. Oh My Gawd! Are you a "Friends" fan? Then you could picture Chandler saying "Oh My Gawd" with that unique tone of voice he uses, and therefore you know the only three words that came to my mind after watching this film. The plot is simply a disaster. Eric Roberts plays the Detectives boss, but that doesnt help at all. The acting in this film sucks and all the characters are worse than boring. There are also some holes in the script, some questions are left unanswered and some scenes seem completely unnecessary. The story isnt even original, since it reminded me of other movies of this kind. And talk about endings, oh boy.. wait till you see this one! I would only recommend it to my worst enemies." 

Richard Propes of The Independent Critic gave an unfavorable review, stating "Sanctimony passes, barely, for one simple reason. In spots, I chuckled. Were these intended chuckles from Boll? I have no doubt they were not. Yet, they were chuckles and gave me a few, very brief moments of light in the darkness that is this film. Sanctimony is typical Uwe Boll...uninspired, repetitive and wholly unoriginal." 

BBC gave the film three out of five stars and wrote "Another serial killer hits the streets in this offbeat and slick little flick. Made with a bit more style than most, it looks very good in a fair DVD release. Heavily stylised visuals run through this movie, and they work well in a top-notch picture transfer. Various camera tricks and variable filming speeds are used, and the effect remains sharp and clear." 

==References==
{{Reflist|refs=
   
   
   
   
<!--
   
   
-->
}}

==External links==
*  

 

 
 
 
 
 
 
 