Ivanhoe (1952 film)
{{Infobox film 
| name = Ivanhoe
| image          = Ivanhoe (1952 movie poster).jpg
| image_size     = 225px
| caption        = Original film poster
| director       = Richard Thorpe	
| producer       = Pandro S. Berman
| screenplay     = Æneas MacKenzie Noel Langley Marguerite Roberts
| based on       =    starring  Robert Taylor Elizabeth Taylor   Joan Fontaine   George Sanders Emlyn Williams Felix Aylmer Finlay Currie
| music          = Miklós Rózsa
| cinematography = Freddie Young Frank Clarke MGM 
| released       =   
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $3,842,000  . 
| gross          = $10,878,000 
}}

  in Ivanhoe]] Robert Taylor, Elizabeth Taylor, Joan Fontaine, George Sanders, Emlyn Williams, Finlay Currie and Felix Aylmer. The screenplay is by Æneas MacKenzie, Marguerite Roberts and Noel Langley, based on the historical novel Ivanhoe by Sir Walter Scott. 
 Knights of the Round Table (1953) and The Adventures of Quentin Durward (1955). All three were made at MGMs British Studios at Elstree, near London.
 blacklisted by House on Un-American Activities Committee, and MGM received permission from the Screen Writers Guild to remove her credit from the film.

==Plot== Richard the Robert Taylor), marks of Prince John ruling in his absence.
 Norman king and orders his son to leave. Wamba (Emlyn Williams), Cedric’s court jester, begs to go with Ivanhoe and is made his squire.
 Robert Douglas), and their entourage. That night, two of the Normans try to rob Isaac, but are foiled by Ivanhoe. Not feeling safe, Isaac decides to return to his home in Sheffield; Ivanhoe offers to escort him there.

When they reach Isaac’s home, Ivanhoe secures his help raising the ransom in return for better treatment for the Jews once Richard returns. Rebecca (Elizabeth Taylor), Isaac’s daughter, visits Ivanhoe secretly in the night to reward him for rescuing her father; she gives him jewels to purchase arms and a horse for an important upcoming Jousting|joust. She falls in love with him, despite the great social gulf between them, in that Jews are not allowed to marry Gentiles.

Nearly everyone of note is at the tournament, including Prince John. Norman knights loyal to him defeat all comers. Just when it seems that they are victorious, a mysterious new Saxon knight appears, arrayed all in black, with white trim,   his face hidden behind his visor. He does not give his name, but challenges all five Norman champions. He easily defeats the first three, Malvoisin, Ralph, and Front de Boeuf (Francis de Wolff), one after the other, and also wins the fourth bout against de Bracy, but is seriously wounded in the shoulder. He is soon identified by many as Ivanhoe. When Ivanhoe salutes Rebecca after his first victory, Bois-Guilbert is immediately smitten by her beauty. In the last joust against Bois-Guilbert, the weakened Ivanhoe falls from his horse. He is carried off, to be tended to by Rebecca.

Fearing Prince John’s wrath, the Saxons depart; Ivanhoe is taken to the woods under the protection of Robin Hood (Harold Warrender). The rest make for the city of York, but are captured and taken to the castle of Front de Boeuf. When Ivanhoe hears the news, he gives himself up, in exchange for his father’s freedom. However, the Normans go back on their word and keep them both. Robin Hood’s men then storm the castle, freeing most of the captives. In the fighting, de Boeuf drives Wamba to his death in a burning part of the castle and is slain in turn by Ivanhoe. Bois-Guilbert alone escapes, by using Rebecca as a shield, while de Bracy is defeated and captured by Ivanhoe after attempting to do the same with Rowena.

Meanwhile, the enormous ransom is finally collected, but the Jews face a cruel choice: free either Richard or Rebecca, for Prince John has set the price of her life at 100,000 marks, the Jews’ contribution. Isaac chooses Richard. Ivanhoe entrusts the ransom delivery to Cedric, but promises Isaac that he will rescue Rebecca. 
 burned at wager of battle,” which cannot be denied. Prince John chooses the conflicted Bois-Guilbert as his champion. The Norman makes a last desperate plea to Rebecca: in return for her love, he is willing to forfeit the duel, though he would be forever disgraced as a knight. She refuses, saying “We are all in God’s hands, sir knight.”
 mace and chain. For most of the battle, Bois-Guilbert has the upper edge, but in the end Ivanhoe prevails, mortally wounding Bois-Guilbert. As he lies dying, Bois-Guilbert reaffirms to Rebecca that he is the one who loves her, not Ivanhoe. Rebecca accepts that Ivanhoe’s heart has always belonged to Rowena, and Richard and his knights (with Cedric as an escort) return to reclaim his throne from his usurping brother.

==Cast==
  Robert Taylor as Sir Wilfred of Ivanhoe
* Elizabeth Taylor as Rebecca
* Joan Fontaine as Rowena
* George Sanders as Sir Brian De Bois-Guilbert
* Emlyn Williams as Wamba Robert Douglas as Sir Hugh De Bracy
* Finlay Currie as Cedric
* Felix Aylmer as Isaac
* Francis de Wolff as Front De Boeuf (also as Francis DeWolff) Richard
* Basil Sydney as Waldemar Fitzurse
* Harold Warrender as Locksley - Robin Hood
* Patrick Holt as Philip DeMalvoisin
* Roderick Lovell as Ralph DeVipont Sebastian Cabot as Clerk of Copmanhurst
* John Ruddock as Hundebert Michael Brennan as Baldwin
* Megs Jenkins as Servant to Isaac
* Valentine Dyall as Norman Guard
* Lionel Harris as Roger of Bermondsley
* Carl Jaffe as Austrian Monk John
* Sir Jack Churchill as Archer on the Walls of Warwick Castle Martin Benson (uncredited)
 

==Production== House on John Sanford, the Fifth Amendment and refused to answer questions about whether they had been members of the American Communist Party. Consequently, they were both Hollywood blacklist|blacklisted,    and MGM received permission from the Screen Writers Guild to remove Roberts credit from the film. It would take nine years before she was allowed to work in Hollywood again. 

Scenes were filmed at Elstree Studios, London and on location at Doune Castle, Scotland. 

==Reception==
===Box Office===
Ivanhoe was released in the summer of 1952. In its opening 39 days, the film took $1,310,590 at the box office, setting a new record for an MGM film. According to the studio records, it made $5,810,000 in the US and Canada and $5,086,000 elsewhere, resulting in a profit of $2,762,000.  It was MGMs biggest earner for 1952 and one of the top four money-makers of the year. It was also the fourth most popular film in England in 1952. 

===Critical Reception=== Best Picture, Best Cinematography, Best Music, Scoring. In addition, Richard Thorpe was nominated by the Directors Guild of America, USA for Outstanding Directorial Achievement in Motion Pictures. There were also two Golden Globe Award nominations: Best Film Promoting International Understanding and Best Motion Picture Score, for Miklós Rózsa.

==Differences from Scotts novel==
The film omits the characters Aethelstane, Lucas Beaumanoir, and Gurth, while the Crusaders play no role. Ivanhoes early injuries are modest and he plays a very active role throughout the film. Unlike the novel, King Richard is not involved until the final scene, when he and his knights ride in (costumed as Crusaders).  In the novel, Rebecca is tried and nearly executed by the Templars, not Prince John.

In the film, Wamba the Jester (in whom the books characters of Wamba the Jester and Gurth the Swineherd are combined), dies in the course of freeing Cedric and the other captives from the castle of Front de Boeuf. Wamba does not die in Walter Scotts original novel.

The visual spectacle is given more attention than the dialogue and underlying story, with a notable jousting scene as well as a well choreographed castle siege sequence, though the main points of the plot are covered.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 