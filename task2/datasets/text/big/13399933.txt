The Wanderer (1913 film)
 
{{Infobox film
| name           = The Wanderer
| image          = File:The Wanderer poster.jpg
| caption        = Film poster
| director       = D. W. Griffith
| producer       = 
| writer         = D. W. Griffith
| starring       = Henry B. Walthall
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 17 minutes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama film directed by  D. W. Griffith.

==Cast==
* Henry B. Walthall - The Wanderer
* Charles Hill Mailes - The Father
* Christy Cabanne - The Brother
* Kate Bruce - The Old Woman
* Lionel Barrymore - The Male Lover
* Claire McDowell - The Female Lover
* Kate Toncray - The Other Mother
* Frank Opperman - The Other Father
* Mae Marsh - The Other Parents Daughter, as an Adult John T. Dillon - The Crafty Merchant Walter Miller - The Other Man Charles West - The Friar Harry Carey - A Soldier (unconfirmed)
* Adolph Lestina - A Customer Joseph McDermott - A Soldier
* Marshall Neilan

==See also==
* Harry Carey filmography
* D. W. Griffith filmography

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 