The Forbidden Team
{{Infobox film
| name           = The Forbidden Team Danish: Det forbudte landshold
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 40 minutes
| director       = Rasmus Dinesen, Arnold Krøjgaard
| producer       =  Karim Stoumann
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Jens Espensen, Michael Nybrandt, 14th Dalai Lama
| music          = 
| cinematography = 
| editing        = 
| studio         =  TV2
| released       =   Danmark India Danish
| budget         = 
| gross          =
}} Danish documentary film directed by Rasmus Dinesen and Arnold Krøjgaard. The leading cast consists of association football trainers Jens Espensen and Michael Nybrandt, the Tibet national football team and at the conclusion the Greenland national football team. Star-part is being played by the 14th Dalai Lama.

The film was rewarded as the Best Feature Film on the Krasnogorski International Filmfestival in Moscow, Special Mention on the FID in Marseilles and the Audience Award on the International Sport Movies & TV Festival in Milan. The final match against Greenland was broadcast live by the radio station Free Tibet.

==Plot== Tibetans in exile this was the first international match after many decades since the former national team had broke up.
 Dharamsala in northern India and ends with the last whistle of the match in Copenhagen, Denmark. The match was won by Greenland with 4-1.

The Tibetan Buddhist background of the players is notable in the game mentality of the players who dont show any aggression. This invokes scenes of the trainer. He believes in discipline, hard working and a strong winners mentality.

== References and external links ==
*  on the official website
*  

 
 
 
 
 
 
 
 
 
 


 
 