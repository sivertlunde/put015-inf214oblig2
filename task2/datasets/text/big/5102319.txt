Vacancy (film)
{{Infobox film name = Vacancy image = Vacancypost07.jpg caption = Theatrical release poster director = Nimród Antal producer = Hal Lieberman writer = Mark L. Smith starring = Kate Beckinsale Luke Wilson Frank Whaley Ethan Embry music = Paul Haslinger cinematography = Andrzej Sekuła editing = Armen Minasian studio = Hal Lieberman Company distributor = Screen Gems released =   runtime = 85 minutes country =   language = English
|budget = $19 million gross = $35,300,645
}}
Vacancy is a 2007 American horror film directed by Nimród Antal and starring Kate Beckinsale and Luke Wilson. It was distributed by Screen Gems and was released on April 20, 2007.

Early in the films development, it was thought that Sarah Jessica Parker was going to be in the film. A September 2006 article in The Hollywood Reporter announced that Kate Beckinsale had signed on to replace Parker. 

==Plot==
A couple, David (Luke Wilson) and Amy Fox (Kate Beckinsale) take a wrong turn on a remote mountain road. When their car breaks down, they realize they are in a cell phone dead zone, with no reception. They then decide to check into a completely isolated motel in a rural area with a strangely off-putting manager called Mason (Frank Whaley).

While in their room, a bored David takes a peek at the video tapes left on top of the television. As he watches the tapes, he realizes they are snuff films, taken in the very room in which they are staying. The couple are alarmed by this as well as loud, insistent banging coming from the door to the adjacent room, and try to run away. Men dressed in blue and wearing masks stop them, and they return to their room. David makes a run to the motels payphone booth to no avail, escaping it just before a car crashes into it. The couple decides to lock themselves in the room when they notice a truck pull in. They try to warn the truck driver only to find out that he is actually there to buy a box of tapes of the snuff films.

David and Amy escape into a tunnel that they discover in the bathroom of their room. They follow the tunnel and end up inside the managers lair, where they find video monitors taping the entire motel. Amy tries to make a call to 911 but is interrupted before she can give the operator any useful information. The couple sneak back into the tunnel, and two of the masked people follow them in shortly after, but the couple evade them making their way into the auto garage across the lot from the motel. Meanwhile, the police unit responding to Amys call arrives and David and Amy run to him as he checks the rooms. They all get inside the police car but it has been sabotaged and the killers murder the policeman while he looks under the hood.

The couple run into one of the other motel rooms. David hides Amy in a space inside the ceiling, while he ventures out. The killers catch and stab David and he ends up collapsing in the doorway. In the morning, Amy comes down from the ceiling and finds where the killers hid their car. As Amy drives away, a killer breaks into the car from the sun roof, and in her effort to fend him off while driving, Amy crashes the car into the motel, killing her attacker as well as another one of the masked men, revealed to be the gas station attendant that earlier "helped" the couple with their car troubles, who is crushed against the front of the car. She runs into the reception area where she finds a revolver. In an effort to reach the revolver, an apoplectic Mason strangles her with a telephone cord. As they fight, Mason beats her severely. But in his effort to get a good final shot with his hand-held digital video camera, Mason throws Amy within reach of a revolver she had dropped, and she shoots Mason three times. Amy immediately runs to David to find that he is still barely alive. She searches Masons body carefully for the telephone cord he had pulled out of the wall to use to strangle her, calls 911 again and returns to comfort David while they wait for the police.

==Cast==
*Kate Beckinsale as Amy Fox
*Luke Wilson as David Fox
*Frank Whaley as Mason
*Ethan Embry as the Mechanic
*Scott G. Anderson as the Killer

==Reception==
Vacancy opened at #4 in its first week at the box office grossing $7.6 million at 2,551 locations. In its second week, the film had a 45.9% drop-off, falling to a #8 position. The film has grossed a total of $28.4 million worldwide. The film received mixed reviews, with Rotten Tomatoes giving the film an average rating of 55%. "Vacancys restraint with gore is commendable", said one critic.

==Home video== UMD for the PlayStation Portable|PSP. Many versions shipped to Australia featured Sony DVD "anti-piracy" technology, which led to them being unreadable on most DVD players, including Sony DVD players.  The DVD featured a commentary by Nimrod Antal, Kate Beckinsale and Luke Wilson, all of whom claimed they thought the film was a great addition to the horror genre and for not using gore for scares but using psychological horror.

==Advertising and promotion==
The advertising strategy for the film made use of the Internet as well as a toll-free phone number. In addition to the TV spots and trailers shown in theaters and on television, the toll free number is made to sound as if one is actually calling the motel in which the film is set. In the background, screaming can be heard accompanying the voice of the proprietor, who informs callers about "slashing" prices and the "killer" deals that the motel has—that is, if it is not vacant. The toll free phone number for the ad is 1-888-9-VACANCY (1-888-9-8222629). The voice of the proprietor is Frank Whaleys.

As of September 16, 2014, the toll-free phone number for the ad is still valid. Though instead of it starting off as it goes right to Mason, it says "Hello, thanks for calling. Please press 1 to make reservations at the Pine-Wood Motel, Press 2 to buy tickets for Vacancy off of Fandango (ticket service)|Fandango, Press 3 for an operator." Since Vacancy is long gone from the theaters, pressing 2 disconnects you. Pressing 1 plays the normal intro, pressing 2 takes you to the front desk, pressing 0 plays a scratchy recording of a call to the operator. When disconnected on the recording, it too disconnects you from the call.

In addition, the phone call also refers the caller to the films website, which is also set up to be the site of the actual motel. After calling, one is given a "promotional code" (8889) which can be entered at the website,  which then shows previews of the snuff films that the motel proprietor and various people created, taken from the set. It was assisted and directed by Julie Tsaruhas.

==Prequel==
 
  was written by Mark L. Smith, and directed by Eric Bross. The film stars Agnes Bruckner and Trevor Wright. The prequel focuses on how the motels employees started their tortures.

==See also==
*List of films featuring surveillance

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 