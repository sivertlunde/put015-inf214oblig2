Bandhan (1969 film)
 
{{Infobox film
| name           = Bandhan 
| image          = Bandhan(1969).jpg
| image size     =
| caption        =
| director       = Narendra Bedi
| producer       = G.P. Sippy
| writer         = 	
| dialogue       =  Mumtaz Jeevan Kanhaiyalal Rajendra Nath Aruna Irani D.K. Sapru Anju Mahendru Ratnamala Achala Sachdev Roopesh Kumar Kamal Kapoor
| music          = Kalyanji Anandji
| cinematography = K. Vaikunth
| editing        = M. S. Shinde	 	
| lyrics         = Anjaan  Indeevar
| released       = 1969
| runtime        = 190 min
| country        = India Hindi
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Hindi film Mumtaz in the lead roles. The directorial debut film,    is the second straight movie with Rajesh and Mumtaz together in the lead roles. This film had a gross of 2,80,00,000 at the box office in 1969.  Khannas songs were performed by playback singer Mahendra Kapoor.

==Plot==
Jeevanlal is an Indian man who makes money by stealing. His wife Sita doesnt like her husband doing any criminal activity.Both have a son, Dharamchand and a daughter named Meena. One day Jeevanlal steals from the Tehsildars home, returns to his home and hides the stolen property with his son.The police arrive at his house and start enquiring the young Dharm about his father. Dharm honestly gives away the stolen properties with him to the policed officers. Jeevan gets arrested and he lands in prison. He begins to start hating his son for putting him into trouble.Years go by and now Dharm has grown up man and he loves his mother a lot. He also has a girl friend named Gauri Malikram, daughter of the village money lender. Gauri was a wild tom boy who after falling in love with Dharm becomes a sensible girl and wishes to get married to him. Dharm now has taken the occupation of farmer and he does well for himself and his family. Even now Jeevanlal still harbours hatred towards his own son. Jeevanlal even visits brothel and gives away the money kept by Dharm for his sisters wedding to a dancer Dharm, an animal lover does not like the cows being sold after they are old and useless. He respects cows and worships them. But Jeevanlal decided one day to sell the cow,which is opposed by Dharmchand. Jeevanlal strikes his son with a stick and to defend himself Dharam picks up another stick. Sita goes and stops the fight and scolds Dharm for disrespecting his father. Sita asks him to get lost and on hearing this Dharm leaves the village and comes for work to Bombay. He gets to work with a building contractor and from whatever he earns send his savings to his mother. After few months, Sita receives a letter that Dharm is coming back to home.Sita feels escalted and on the day he is supposed to reach the village, she decides to receive him in the village bus stand. But on reaching the bus stand she learns from bystanders that Dharm has already left for the house. When Sita reaches home she finds Dharm with an axe and her husband dead. Sita falls in a delimma as she knows Jeevanlal and Dharm never got along with each other but she never expected that Dharm would kill Jeevanlal. Sita still does not believe that Dharm would have killed his father. Dharm gets arrested and his lawyer is Ravi Sharma. It is then that the truth will come out what really transpired between father and son that compelled one to kill the other.

==Reception==
It received five of five stars in the Bollywood guide Collections  and grossed 2,80,00,000 at box office in 1969.

==Cast==
*Rajesh Khanna as Dharmchand "Dharma" Mumtaz as Gauri Malikram
*Jeevan as Jeevanlal
*Achala Sachdev as Sita
*Rajendra Nath as Ram Vaidraj Kanhaiyalal as Malikram
*Aruna Irani as Chameli
*D.K. Sapru as Public Prosecutor Kaul
*Ratnamala as Jamuna Malikram
*Sanjeev Kumar as Ravi Sharma
*Anju Mahendru as Sonia Sharma

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-  style="background:#ccc; text-align:center;"
! # !! Title !! Singer(s)
|-
| 1
| "Aa Jao Aa Bhi Jao"
| Mahendra Kapoor
|-
| 2
| "Aayo Re Sawan Aayo Re"
| Mahendra Kapoor
|-
| 3
| "Are Jana Hai To Jao"
| Mahehndra Kapoor, Asha Bhosle
|-
| 4
| "Bina Badra Ke Bijuriya Kaise Chamke" Mukesh
|}

==References==
 

==External links==
* 

 
 
 
 
 
 
 