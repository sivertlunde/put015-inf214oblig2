My Mother, the Mermaid
{{Infobox film 
| name         = My Mother, the Mermaid 
| image        = My Mother the Mermaid movie poster.jpg
| caption      = Theatrical poster
| film name    = {{Film name
| hangul       =  
| hanja        =  
| rr           = Ineogongju
| mr           = Inŏkongju}}
| director     = Park Heung-shik 
| producer     = Lee Joon-dong
| writer       = Park Heung-shik Song Hye-jin 
| starring     = Jeon Do-yeon Park Hae-il Go Doo-shim 
| editing      = Kim Yang-il
| music        = Jo Seong-woo 
| cinematography = Choi Young-taek
| distributor  = CJ Entertainment
| released     =  
| runtime      = 110 minutes
| country      = South Korea
| language     = Korean
| budget       = 
}} 2004 South Korean film about a young woman who quarrels with her mother but is somehow transported back in time and sees her parents courtship. In her youth, her mother was a haenyeo, a traditional free-diving|freediver.     

==Synopsis==
Na-young (Jeon Do-yeon) is an office worker who lives with her seemingly emotionally non-existent father Jin-kook (Kim Bong-geun), and loud cynical mother Yeon-soon (Go Doo-shim). As time passes she is becoming more and more like her mother Yeon-soon. 

One day her father suddenly disappears and she skips her international airplane flight to find him. By entering her fathers hometown she is somehow transported back in time to when her parents relationship was just beginning. She meets her mother, now a poor young woman(Jeon Do-yeon) working hard as a haenyo to send her younger brother to school to get the education she never received. Her father is a charming man (Park Hae-il) who delivers the mail, to be more precise about his work he is postmaster who delivers mails all over the town where her mother used to live; he befriends and teaches Yeon-soon how to read and write.

Na-young is taken in by the young Yeon-soon and, as they are now roughly the same age, the two become very close. Na-young is able to experience the trials, heartbreaks, and celebrations of Yeon-soon before she herself is suddenly transported back into the present time.

==Awards and nominations==
2004 Busan Film Critics Awards 
* Best Supporting Actress - Go Doo-shim

2004 Blue Dragon Film Awards
* Nomination - Best Film
* Nomination - Best Actress - Jeon Do-yeon
* Nomination - Best Director - Park Heung-shik 
* Nomination - Best Supporting Actress - Go Doo-shim
* Nomination - Best Cinematography - Choi Young-taek

2004 Korean Film Awards
* Best Actress - Jeon Do-yeon
* Best Supporting Actress - Go Doo-shim
* Nomination - Best Actor - Park Hae-il
* Nomination - Best Screenplay  - Park Heung-shik and Song Hye-jin
* Nomination - Best Cinematography - Choi Young-taek
* Nomination - Best Art Direction - Jo Geun-hyun
* Nomination - Best Music - Jo Seong-woo

2004 Directors Cut Awards
* Best Actress - Jeon Do-yeon

2005 Baeksang Arts Awards
* Best Director - Park Heung-shik
* Nomination - Best Actress - Jeon Do-yeon

2005 Chunsa Film Art Awards
* Best Actress - Jeon Do-yeon

2005 Grand Bell Awards
* Nomination - Best Actress - Jeon Do-yeon
* Nomination - Best Director - Park Heung-shik
* Nomination - Best Supporting Actress - Go Doo-shim
* Nomination - Best Screenplay  - Park Heung-shik and Song Hye-jin

==References==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 
 