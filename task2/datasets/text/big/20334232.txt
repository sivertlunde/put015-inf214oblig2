The Christmas Miracle of Jonathan Toomey
The 2007 British film scripted and directed by Bill Clark. It was adapted from a 1995 book of the same name, written by Susan Wojciechowski and illustrated by P. J. Lynch, from which the film was film adaptation|adapted.

==Film== Jack Montgomery and Jenny OHara.  One budget estimate is £8 million.  . Retrieved 2012-12-01.  It played at eight film festivals including the Gloria Film Festival at Salt Lake City where it was named "Best Film – 2007". It was released on DVD in the US later that year, in the UK and the Netherlands 2008, and in Germany 2011. 

==Book== Library Association, nativity figurines for a widow and her son, and thereby resolves long-held grief for his own wife and child. <!--
   that retrospective citation by CILIP does not mention the illustrations!
 -->

Walkers American division Candlewick Press published a U.S. edition within the calendar year (Library of Congress Classification PZ7.W8183 Ch 1995; ISBN 978-1-56402-320-9). 
  (first U.S. edition). Library of Congress Catalog Record. Retrieved 2012-09-03. 

==See also==

 

==References==
{{reflist |refs=
 
 . Living Archive: Celebrating the Carnegie and Greenaway Winners. CILIP. Retrieved 2012-07-15. 
}}

== External links ==
*  
*   —immediately, first edition  

 

 
 
 
 
 
 
 


 
 