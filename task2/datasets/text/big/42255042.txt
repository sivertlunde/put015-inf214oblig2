Jamindaru
{{Infobox film
| name = Jamindaru
| image =
| caption = 
| director =S. Narayan
| writer = S. Narayan
| producer = K. Manju Vishnuvardhan  Prema  Raasi  Anu Prabhakar
| music = M. M. Keeravani   Sax Raju (background score)
| cinematography = Mathew
| editing = P. R. Soundar Raj
| studio  = Lakshmishree Combines
| released =  April 3, 2002
| runtime = 160 min
| language = Kannada  
| country = India
}}
 Vishnuvardhan in Prema and Raasi in the lead roles. The film is directed and written by S. Narayan and produced by K. Manju under Lakshmishree Combines. The film, upon release, met with positive reviews and declared a blockbuster.  The music was scored by M. M. Keeravani and Sax Raju.

==Cast== Vishnuvardhan as Bettappa and Biligiri Raasi
*Prema Prema
*Anu Prabhakar
*Srinivasa Murthy
*Doddanna
*Mukhyamantri Chandru
*Hema Chowdhary
*Ashalatha
*Shivaram
*Shobharaj
*Sundar Raj

==Release==
The film was released on April 3, 2002 across Karnataka state cinema halls. The film was met with positive response at the box office.  Producer K. Manju claimed that Jamindarru was his costliest movie at the time of release. 

==Soundtrack==
All the songs are composed and scored by M. M. Keeravani. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyricist
|-
| 1 || "Bettappa Bettappa" || S. P. Balasubramanyam || S. Narayan
|-
| 2 || "Huduga Huduga" || Mano (singer)|Mano, K. S. Chithra, M. M. Keeravani || S. Narayan
|-
| 3 || "Kande Na Kande" || K. S. Chithra || S. Narayan
|-
| 4 || "Ganga Ganga" || Rajesh Krishnan, Manjula Gururaj || S. Narayan
|-
| 5 || "Bettadantha Manasu" || M. M. Keeravani || S. Narayan
|-
| 6 || "Hetthavalu Yaramma" || M. M. Keeravani || S. Narayan
|-
| 7 || "Veena" || K. S. Chithra || S. Narayan
|-
|}

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 


 