Bloodsuckers from Outer Space
{{Infobox film
| name           = Bloodsuckers from Outer Space
| image          =
| alt            =
| caption        =
| director       = Glen Coburn
| producer       = Rick Garlington
| writer         = Glen Coburn
| starring       = {{plainlist|
* Thom Meyes
* Dennis Letts
* Laura Ellis
* Robert Bradeen
* Glen Coburn
* Kris Nicolau
* Pat Paulsen
}}
| music          = Rick Garlington
| cinematography = Chad D. Smith
| editing        = Karen D. Latham
| studio         = One Of Those Productions
| distributor    = Lorimar Television#Home video|Karl-Lorimar Home Video
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Bloodsuckers from Outer Space is a 1984 American horror spoof written and directed by Glen Coburn.  It stars Thom Meyers, Dennis Letts, Laura Ellis, Robert Bradeen, Glen Coburn, Kris Nicolau, and Pat Paulsen as Texas residents who must battle a mist that turns people into zombies.

== Synopsis ==
Texas farmers turn into zombies when they become infected by an energy field from outer space.  The residents must escape before an overeager general can convince the President to drop a nuclear bomb on the rural town.

== Cast ==
* Thom Meyes as Jeff Rhodes
* Dennis Letts as General Sanders
* Laura Ellis as Julie
* Robert Bradeen as Uncle Joe
* Glen Coburn as Ralph Rhodes
* Kris Nicolau as Jeri Jett
* Pat Paulsen as the President

== Release ==
Bloodsuckers from Outer Space premiered at Joe Bob Briggs Drive-In Movie Festival in October 1984.     Paulsen attended the premiere and later said that he was embarrassed by the quality.   Lorimar Television#Home video|Karl-Lorimar Home Video released it on home video in 1986,  and Media Blasters released it on DVD on December 30, 2008. 

== Reception ==
Travis Box of the Dallas Observer cited it as one of the best low budget film made in Texas.   Mike Phalin of Dread Central rated it 5/5 stars and wrote, "Bloodsuckers From Outer Space could be one of the kings of low budget B-Movies."   Academic Peter Dendle wrote in The Zombie Encyclopedia that it "is a lot like the following years Return of the Living Dead, except that it isnt funny or exciting." 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 