Tekken: Blood Vengeance
{{Infobox film
| name           = Tekken: Blood Vengeance
| image          = Tekken Blood Vengeance poster.jpg
| alt            =
| caption        =
| director       = Yōichi Mōri
| producer       = Yoshinari Mizushima
| writer         = Dai Satō
| starring       = Hitoshi Sakimoto & Basiscape  
| cinematography =
| editing        =
| studio         = Digital Frontier Bandai Namco Games Namco Pictures Asmik Ace Entertainment
| distributor    = Bandai Entertainment
| released       =    
| runtime        = 92 minutes   
| country        = Japan
| language       = Japanese, English
| budget         =
| gross          =
}}
  is a 2011 Japanese  .

==Plot== Anna Williams Shin Kamiya, and Anna dispatches a Chinese student, Ling Xiaoyu to act as a spy, while Jin sends humanoid robot Alisa Bosconovitch for a similar purpose.

During their investigation, Xiaoyu and Alisa form a friendship, unaware that the other is working for their enemy, although they are forced to turn against one another when Shin is captured by an unknown assailant. It is here that Alisa is revealed to be a cyborg - although Alisa believes she possesses human qualities after she spares Xiaoyus life. After coming to terms with each other, Xiaoyu is abandoned by Anna and G Corporation, and the two girls flee from their previous organizations, taking refuge in their teacher, Lee Chaolans mansion.

Xiaoyu and Alisa eventually discover genetic experiments had been done on Shin and his classmates, and believe that the Mishima family is seeking Shin, the sole survivor, and M gene subject, for his immortality. The pair discover that this had in fact, been an elaborate plan engineered by Heihachi Mishima, who used Shin to lure Kazuya and Jin and get the Devil Gene. After Heihachi disposes of Shin, he, Kazuya and Jin engaged in a triple threat brawl. During the fight, Kazuya and Jin become their devil forms. Ultimately, Jin is the victor, utilizing his devil powers. Heihachi then unleashes the ancient spirits of the Mokujins and a final fist burst by Alisa leaves Mokujin Heihachi open. Jin finishes him off, by an eye blast which slices it in half. Jin then leaves, telling Xiaoyu that he awaits a future challenge.

The film ends with Alisa and Xiaoyu back at their schools festival with the pair planning to enter the next King of Iron Fist Tournament.

==Cast==
 

{| class="wikitable"
|-
! Character
! Japanese voice actor
! English voice actor 
! First Tekken Game 
|- Ling Xiaoyu Maaya Sakamoto Carrie Keranen
|align=center|Tekken 3
|- Alisa Bosconovitch Yuki Matsuoka Cristina Valenzuela|Cristina Vee
|align=center|Tekken 6
|- List of Shin Kamiya Mamoru Miyano David Vincent David Vincent First Appearance
|- Kazuya Mishima|Kazuya Mishima / Devil Kazuya Masanori Shinohara Kyle Hebert
|align=center|Tekken (video game)|Tekken
|- Jin Kazama|Jin Kazama / Devil Jin Isshin Chiba Darren Daniels
|align=center|Tekken 3 / Tekken 5
|- Nina Williams Atsuko Tanaka Atsuko Tanaka Mary Elizabeth Charlotte Bell
|align=center|Tekken (video game)|Tekken
|- Anna Williams Anna Williams Akeno Watanabe Tara Platt
|align=center|Tekken (video game)|Tekken
|- Kuma (Tekken)#Panda|Panda Taketora
|align=center|Taketora
|align=center|Tekken 3
|- Lee Chaolan
|align=center|Ryōtarō Okiayu Kaiji Tang
|align=center|Tekken (video game)|Tekken
|- Ganryu (Tekken)|Ganryu Hidenari Ugaki Paul St. George C. Cole
|align=center|Tekken (video game)|Tekken
|- Heihachi Mishima
|align=center| Unshō Ishizuka Jamieson Price|Taylor Henry
|align=center|Tekken (video game)|Tekken
|- Mokujin
|align=center|Keiko Nemoto Charlotte Bell
|align=center|Tekken 3
|}

==Production==
The film was initially announced at Namcos "LevelUp" event in Dubai on May 5, 2011. A trailer premiered at the event and also was posted online shortly after. The film, inspired by the popular  . The release arrived in late summer, also with availability in 3D. In a group interview at the LevelUp event in Dubai, attended by Christian Donlan for Eurogamer, Tekken project leader Katsuhiro Harada revealed Blood Vengeance has actually been in development since January 2010.

Harada has been very clear that this movie is not related to the Tekken (2010 film)|live-action film and is doing whatever he can to ensure this project is distanced as much as possible from that film. "That doesnt have anything to do with it this time," Harada insisted. "Were not trying to rewrite those wrongs. Fans are always asking us for a 3D movie. This is our response to them... We want to make a movie that everyone can enjoy, though. Not just Tekken fans." Namcos Tekken team has wanted to do a 3D movie after the creation of the CG opening for Tekken 6s scenario campaign.

The film is voiced in English and Japanese with subtitles for other languages. Each character in the game tends to speak their native language. Namco has "tried as much as possible" to use the same voice actors from the games for the film. Writer Dai Satō, who had previously worked on Cowboy Bebop, is a fan of the Tekken series, and was given "quite a bit of creative freedom".

On July 23, 2011, Namco Bandai previewed Tekken: Blood Vengeance at San Diego Comic-Con International|Comic-Con with writer Satō, and voice actresses Carrie Keranen and Cristina Vee as guests.   

==Release==
The film was released in both Blu-ray and DVD formats on November 22, 2011 in North America, and December 1, 2011 in Japan. The Blu-ray version was also released as a   for the Nintendo 3DS.    It is the first film to be released for the 3DS. 

== Reception ==
Tekken: Blood Vengeance has received mixed reviews with many comparing it to   and the 2010 live-action film.  ".  Joystiqs Eric Kaoili called it "a pleasant enough film, much better than the travesty that was Tekken: The Motion Picture". 

The Japanese Committee of the International 3D Society honored Tekken: Blood Vengeance along with Always Sanchōme no Yūhi 64, Kaibutsu-kun and Sadako 3D at the second annual International 3D Awards Lumiere Japan, where all four films won the Movie Award.  The film also won the International Jury Prize (Japan) at the International 3D Societys fourth annual 3D Creative Arts Awards. 

== References ==
 

==External links==
*  (web archive)
* 
* 

 

 
 
 
 
 
 
 
 