The House of the Spaniard
{{Infobox film
| name = The House of the Spaniard
| image =
| image_size =
| caption =
| director = Reginald Denham
| producer = Hugh Perceval
| writer = Arthur Behrend (novel)   Basil Mason
| narrator =
| starring = Peter Haddon   Brigitte Horney   Jean Galland   Allan Jeayes
| music = Allan Gray 
| cinematography = Franz Weihmayr
| editing = 
| studio = Phoenix Films Associated British
| released = November 1936
| runtime = 71 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} comedy thriller film directed by Reginald Denham and starring Allan Jeayes, Peter Haddon and Brigitte Horney.  It is set in Lancashire and Spain, during the ongoing Spanish Civil War. It was shot at Ealing Studios in west London, England, and on location in Lancashire and Spain. Art direction was by Holmes Paul. It was based on a novel by Arthur Behrend.

==Synopsis==
An unemployed and dull-witted young man David Grey stays with his friend in Liverpool while he looks for work. Due to a mistake he is hired by a Spanish-owned shipping company whose boss Pedro de Guzman lives in a lone, mysterious house in the marshes outside Liverpool. Greys curiosity is aroused by the unexplained death of a man who he had seen close to the house. His investigations lead to him being kidnapped, taken to Spain and embroiled in the Spanish Civil War. He is eventually able to escape thanks to the help of de Guzmans daughter Margarita.

==Cast==
* Brigitte Horney - Margarita de Guzman 
* Peter Haddon - David Grey 
* Jean Galland - Ignacio 
* Allan Jeayes - Don Pedro de Guzman 
* Gyles Isham - John Gilchrist 
* Hay Petrie - Orlando 
* Ivor Barnard - Mott 
* Minnie Rayner - Mrs. Blossom 
* Gibson Gowland - 1st Captain  David Horne - 2nd Captain 
* Ernest Jay
* Charles Lloyd-Pack - Man in train  Fred ODonovan - McNail 
* Abraham Sofaer - Vidal

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The British of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 