Aaryankavu Kollasangam
{{Infobox film 
| name           = Aaryankavu Kollasangam
| image          =
| caption        =
| director       = R Velappan Nair
| producer       = R Velappan Nair
| writer         = R Velappan Nair Kedamangalam Sadanandan (dialogues)
| screenplay     = Khadeeja Polachira Ramachandran Santo Krishnan
| music          = BA Chidambaranath
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1969 Cinema Indian Malayalam Malayalam film, directed and produced by R Velappan Nair . The film stars Keralasree Sunny, Khadeeja (actress)|Khadeeja, Polachira Ramachandran and Santo Krishnan in lead roles. The film had musical score by BA Chidambaranath.   

==Cast==
*Keralasree Sunny Khadeeja
*Polachira Ramachandran
*Santo Krishnan
*Madhavan Nair

==Soundtrack==
The music was composed by BA Chidambaranath and lyrics was written by Kedamangalam Sadanandan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Alayuvathenthinu || CO Anto || Kedamangalam Sadanandan || 
|-
| 2 || Punchiri Thooki || P. Leela || Kedamangalam Sadanandan || 
|}

==References==
 

==External links==
*  

 
 
 

 