Moms' Night Out
{{Infobox film
| name           = Moms Night Out
| image          = Moms Night Out poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Andrew Erwin   Jon Erwin
| producer       = 
| writer         = Jon Erwin   Andrea Gyertson Nasfell
| starring       = 
| music          = Marc Fantini   Steffan Fantini
| cinematography = Kristopher Kimlin
| editing        = Andrew Erwin   Jonathan Olive
| studio         = Affirm Films Provident Films
| distributor    = TriStar Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $5 million   
| gross          = $10,496,858 
}}
Moms Night Out is an American Christian film|Christian-based comedy film directed by Andrew Erwin and Jon Erwin, and written by Jon Erwin and Andrea Gyertson Nasfell. The film stars Sarah Drew, Sean Astin, Patricia Heaton and Trace Adkins. The film was released on May 9, 2014 at 1,044 theaters. 

==Plot summary==
 
Feeling overworked by her children, mother of three Allyson decides to go on a moms night out with her friends, Izzy and Sondra. The three expect a night to unwind while Allysons husband, Sean, takes care of all the kids. After their reservation is canceled things go from bad to worse when Allysons sister-in-law, Bridget, realizes that her infant son, Phoenix, is missing. The four travel across the city looking for the child and ultimately learning to quit blaming others for their own problems. While all this is going on, their husbands attempt to care for the children with disastrous results.

==Cast==
* Sarah Drew as Allyson 
* Sean Astin as Sean 
* Patricia Heaton as Sondra 
* Trace Adkins as Bones  David Hunt as Cabbie
* Andrea Logan White as Izzy
* Robert Amaya as Marco
* Abbie Cobb as Bridget
* Harry Shum, Jr. as Joey   
* Alex Kendrick as Pastor Ray 
* Anjelah Johnson as restaurant hostess
* Kevin Downes as Kevin Manwell Reyes as Desk Guy
* Sammi Hanratty as Zoe
* Jason Burkey as DJ

==Production==
Filming started and wrapped in June 2013 in Birmingham, Alabama.    The shooting eventually wrapped up on June 24th, the Erwin brothers announced.   

==Reception==
 
Moms Night Out was panned by critics. On Rotten Tomatoes, the film has an 18% rating by critics based on 38 reviews; the general consensus reads "Cheap-looking, unfunny, and kind of sexist to boot, Moms Night Out is a disappointment from start to finish."  On Metacritic, the film has a rating of 25 out of 100, based on 20 critics, indicating "generally unfavorable reviews." 

The film grossed $4.2 million in its opening weekend, finishing in 7th place. As of July 13, 2014 the film has grossed $10.5 million.  

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 