Bobby Deerfield
{{Infobox film
| name           = Bobby Deerfield
| image          = Bobby deerfield.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Sydney Pollack
| producer       = Sydney Pollack
| screenplay     = Alvin Sargent
| based on       =  
| starring       = {{Plainlist|
* Al Pacino
* Marthe Keller
* Anny Duperey
}}
| music          = Dave Grusin
| cinematography = Henri Decaë
| editing        = Fredric Steinkamp
| studio         = {{Plainlist|
* Columbia Pictures
* Warner Bros.
}}
| distributor    = {{Plainlist|
* Columbia Pictures
* Warner Bros.
}}
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $9,300,000   
}} romantic drama film directed by Sidney Pollack and starring Al Pacino and Marthe Keller. Loosely based on the 1961 novel Heaven Has No Favorites by Erich Maria Remarque, the film is about a famous American race car driver on the European circuit who falls in love with an enigmatic Swiss woman who is terminally ill.    For his performance in the film, Al Pacino was nominated for a Golden Globe for Best Motion Picture Actor.   

==Plot==
Formula One auto racer Bobby Deerfield is a calculating, control-obsessed loner who has become used to winning the checkered flag on the track. But after he witnesses a fiery crash that kills a teammate and seriously wounds a competitor, Deerfield becomes unsettled by the spectre of death.

During a visit to the survivor, Deerfields world is further set askew when he meets Lillian Morelli (Marthe Keller), a quirky, impulsive woman racing against time.

==Cast==
* Al Pacino as Bobby
* Marthe Keller as Lillian
* Anny Duperey as Lydia
* Walter McGinn as The Brother
* Romolo Valli as Uncle Luigi
* Stephan Meldegg as Karl Holtzmann Jaime Sánchez as Delvecchio
* Norm Nielsen as The Magician
* Mickey Knox as Tourist
* Dorothy James as Tourist
* Guido Alberti as Priest in the Garden
* Monique Lejeune as Catherine Modave
* Steve Gadler as Bertrand Modave
* Van Doude as The Flutist
* Aurora Maris as Woman in the Gas Station
* Gérard Hernandez as Carlos Del Montanaro
* Maurice Vallier as Priest
* Antonino Faa Di Bruno as Vincenzo
* André Valardy as Autograph Hound
* Féodor Atkine as Tommy (as Fédor Atkine)
* Patrick Floersheim as Mario
* Bernie Pollack as Head Mechanic
* Al Silvani as Mechanic
* Isabelle de Blonay as Nurse
* Franco Ressel as Man with Dog
* Dominique Briand as Reporter   

==Reception==
===Critical response=== Grand Prix Le Mans, were disappointed that the story did not play out on the race track; however, the action footage was filmed by racing cinematographers over the course of the 1976 Formula One season and features actual drivers, including Carlos Pace, Tom Pryce, James Hunt, Patrick Depailler and Mario Andretti. Vincent Canby of The New York Times said that it "may turn out to be the years most cynical movie made by people who know better, including Sydney Pollack, the director, and Alvin Sargent, who wrote the screenplay."
 Time Out stated that the film is a "classic example of a Hollywood director being struck down by a lethal art attack as soon as he sets foot in Europe."

Bobby Deerfield was released on DVD for the first time on March 11, 2008. The soundtrack, recorded on the benighted Casablanca Records label, has been unavailable for years.

===Box office===
Bobby Deerfield grossed $9,300,000 in the United States.   

===Awards and nominations===
* 1978 Golden Globe Award Nomination for Best Motion Picture Actor, Drama (Al Pacino) 

==Differences from the novel== Swiss town of Leukerbad makes no mention of the on-location filming that took place there. 

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 