8mm 2
 
{{Infobox film
| name = 8MM 2
| image = 8mm2 (DVD case).jpg
| caption = DVD cover
| director = J. S. Cardone
| producer = Scott Einbinder Carol Kottenbrook
| screenplay = Robert Sullivan
| story = Dylan Tarason
| starring = Johnathon Schaech Lori Heuring Bruce Davison Julie Benz Tim Jones
| cinematography = Darko Suvak
| editing = Elena Maganini
| distributor = Sony Pictures Home Entertainment
| released = 2005
| runtime = 106 minutes
| country = United States Hungary
| language = English
| budget =
}}

8mm 2 is a 2005 direct-to-video It stars Johnathon Schaech and Lori Heuring. The film was distributed by Sony Pictures Home Entertainment.

When the film was in production, it was titled The Velvet Side of Hell.  When Sony picked up the rights to distribute it, it was re-titled 8mm 2. Although this title suggests the film to be a sequel to the 1999 Nicolas Cage film 8mm (film)|8mm, the film has no connective elements relating it to the first 8mm film. 

The film was shot entirely in Budapest, and the credits contain many Hungarian names. 

==Plot==
An engaged couple, Tish and David (Heuring and Schaech), take a short vacation to Budapest. David is counsel to Tishs father, an ambassador who does not approve of their relationship, although this does not affect the couples relationship. While in Budapest, the two indulge in many sexual activities, eventually engaging in a threesome with a local model named Risa, in a hotel in Alhambra. The sexual tryst becomes a personal issue when they receive a letter containing pictures of the menage a trois. Realizing this escapade could ruin their careers, as well as the career of Tishs father, they venture deep into Budapest to find Risa. On their journey, they encounter their blackmailer, the housekeeper of the hotel, who told them about Risa.

He demands a sum of $200,000 for the pictures he took, and takes Tish hostage before running off, though he eventually lets her go. The blackmailer encounters David and the men begin firing at each other, but the blackmailer escapes. Going into his apartment, they find the man shot dead. A police officer, detective Kovač, later contacts them and seems to suspect them of murdering the man. Later on they find Risa dead in her apartment and presume the entire ordeal is over. Just as they begin to celebrate, another mysterious man calls them and demands $1,000,000 in exchange for the pictures, which they agree to pay through Tishs contacts and with the help of Kovač. They arrange to meet at an old warehouse, but they do not see the man when they get there.

David tells Tish to stay in the car while he goes looking for the man inside, handing her a gun. After he disappears into the warehouse, Kovač (who had been following them) appears at Tishs side of the car and knocks on the window. She informs him that the man said there should be no police, but Kovač tells her that if she wants to see David leave the building alive, she must let him go. After a while Tish hears gunfire and runs into the building, shooting at an SUV which is exiting the warehouse. She then finds Kovač dead inside. Later the mysterious man calls her again and says he has David, and demands a sum of $5,000,000 for his safe return, otherwise he will kill him. She agrees and pays the money.

The man informs her that David is tied up and dying in the basement of a store in Budapest. When Tish locates it, she discovers David tied up in the basement, with limited oxygen being fed to him via a mask. She frees him, and they thereafter continue their lives together, without any further interference from the man. On Christmas, Tish and her siblings go shopping in the city, and plan on going for a drink with David, however he calls Tish and informs her he will be working late. Tish decides she will continue shopping and meet David at home. When she boards a subway train in the evening, she notices in the train-car on the rails next to hers, the porn-store owner David knows, a Russian pimp she had tried to work for, Risa, Kovač, and the hotels housekeeper. They are smiling, as David enters the train-car, and it is made clear to Tish that they plotted everything to get their hands on the $6,000,000 she had given them throughout the events. As the train pulls away, he notices that she has seen him and the whole gang.

== Cast ==

* Johnathon Schaech as David Huxley
* Lori Heuring as Tish Harrington
* Bruce Davison as Ambassador Harrington
* Julie Benz as Lynn
* Valentine Pelka as Gorman Bellec
* Zita Görög as Risa
* Robert Cavanah as Richard
* Jane How as Mrs. Harrington
* Alex Scarlis as Perry
* Barna Illyes as Detective Kovač
* Lili Bordán as Dóra

==References==
 

== External links ==
* 

 
 
 
 
 
 
 