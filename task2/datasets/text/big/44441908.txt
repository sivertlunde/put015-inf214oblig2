Shadow of Doubt (1935 film)
{{Infobox film
| name           = Shadow of Doubt
| image          = 
| alt            = 
| caption        = 
| director       = George B. Seitz
| producer       = Lucien Hubbard
| screenplay     = Wells Root 
| story          = Arthur Somers Roche 
| starring       = Ricardo Cortez Virginia Bruce Constance Collier Isabel Jewell Arthur Byron Betty Furness
| music          = R.H. Bassett 
| cinematography = Charles G. Clarke
| editing        = Basil Wrangell 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Shadow of Doubt is a 1935 American mystery film directed by George B. Seitz and written by Wells Root. The film stars Ricardo Cortez, Virginia Bruce, Constance Collier, Isabel Jewell, Arthur Byron and Betty Furness. The film was released on February 15, 1935, by Metro-Goldwyn-Mayer. {{cite web|url=http://www.nytimes.com/movie/review?res=9B06E0D9113FE53ABC4952DFB566838E629EDE|title=Movie Review -
  Shadow of Doubt - At the Mayfair. - NYTimes.com|publisher=|accessdate=19 November 2014}}  

==Plot==
 

== Cast ==
*Ricardo Cortez as Sim
*Virginia Bruce as Trenna
*Constance Collier as Aunt Melissa
*Isabel Jewell as Inez
*Arthur Byron as Bellwood
*Betty Furness as Lisa
*Regis Toomey as Reed Ryan
*Ivan Simpson as Morse
*Bradley Page as Haworth
*Edward Brophy as Wilcox
*Samuel S. Hinds as Mr. Granby
*Richard Tucker as Mark Torrey Bernard Siegel as Ehrhardt Paul Hurst as Lt. Sackville 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 