Haunters
{{Infobox film
| name              = Haunters
| film name         = {{Film name
| hangul            = 초능력자
| rr                = Choneungryeokja
| mr                = Ch‘onŭngnyŏkcha}}
| image             = Haunters2010Poster.jpg
| caption           = South Korean poster
| director          = Kim Min-seok
| screenplay        = Kim Min-seok
| producer          = Lee Yu-jin
| cinematography    = Hong Kyung-pyo
| starring          = Kang Dong-won Go Soo
| music             = Lee Jae-jin
| editor            = Kim Sang-beom  Kim Jae-beom
| studio            = Zip Cinema
| distributor       = Next Entertainment World
| released          =  
| runtime           = 114 minutes
| country           = South Korea
| language          = Korean
| gross             =   
}} 2010 South Korean film written and directed by Kim Min-seok. It depicts the struggle between a psychic named Cho-in (Kang Dong-won) who can control people with his mind and a man named Kyu-nam (Go Soo) who is immune to the psychics supernatural power.  

== Plot ==
Kyu-nam works at a run-down pawnshop. When Cho-in walks into the shop one day and tries to use his powers to rob the store, he soon discovers that his special talent doesnt work on Kyu-nam. Upset by the fact that there is a person whom he cannot control, Cho-in chases Kyu-nam, accidentally killing someone in the process. This begins a confrontation that will push both men to the brink. 

==Cast==
*   leg on his left. He lives his life robbing others by using his mind control abilities on people to do the job for him. Upon learning that he cant control Kyu-nam because of his similar supernatural abilities, Cho-in pursues him in frustration to the point of putting everyones lives at risk.  
* Go Soo as Im Kyu-nam: A worker at a pawn shop who was fired from a previous job. He displays similar supernatural abilities to Cho-in, resulting in being immune to his mind control abilities. Realizing what at stake, Kyu-nam must protect his friends from Cho-in.  
*   daughter who is studying to become a flight attendant. She is Kyu-nams love interest and he has to protect her from Cho-in.  
* Yoon Da-kyeong as Hyo-sook: Cho-ins mother who attempted to keep his eyes blindfolded so no one can learn about his mind controlling abilities and they can believe hes blind. After leaving her abusive ex-husband(and Cho-ins father), Hyo-sook promised to take good care of him as long as he never removes his blindfold. Upon learning he had went against her wishes in removing his blindfold that resulted in his fathers death, Hyo-sook attempted to murder Cho-in in despair, but he escaped and she suffers a mental breakdown.
* Choi Deok-moon as Abby
* Abu Dod as Bubba: One of Kyu-nams close friends who tries to help him catch Cho-in. He is hanged to death alongside Ali.
* Enes Kaya as Ali: Another of Kyu-nams close friends that also tries to help him catch Cho-in. Like Bubba, he is also hanged to death.
* Byun Hee-bong as Jung-sik: The owner of a pawn shop and Yeong-sooks father. A kindly man who hires Kyu-nam and is accidentally killed by Cho-in during his robbery attempt.
* Yang Kyeong-mo as young Cho-in

==Box office==
Prior to its release, Haunters recorded the highest advance ticket sales for a Korean film in 2010 at 83.47 percent according to Max Movie; it was the first time in four years, after   Movies, 56.25 percent on Ticket Link, 84.2 percent at Cinus, 53 percent at Lotte Cinema and 44.2 percent at CJ CGV|CGV. 

The film opened number one at the South Korean box office collecting 685,670 admissions on its opening November 12–14 weekend.  The movie took the #1 spot for ticket sales for 2 weeks straight,  before it fell down to #3 spot during the 3rd week since opening. The film sold 2,017,485 tickets during the three-week span, eventually selling a total of 2,152,577 tickets nationwide. 

At the Asian Film Market held during the 2010 Busan International Film Festival, distribution rights were sold to several countries, including Thailand, Taiwan, Malaysia, Germany, Belgium, the Netherlands and Luxembourg. 

==Soundtrack==
{{Infobox album Name        = Haunters Original Soundtrack Type        = Soundtrack Longtype    =  Artist      = Lee Jae-jin Cover       =  Released    =   Recorded    = 2010 Genre       = Soundtrack Length      =  Label       =  Producer    = 
}}
# "Year 1991"
# "Encounter"
# "Haunters"
# "Solitude"
# "Brothers"
# "Montage"
# "Looking for Us"
# "Turn Off"
# "Revenge"
# "Stop"
# "Hero"
# "Go, Super Damas"
# "My Samantha"
# "The Beginning (Title) – Space cowboy"

==Remake==
A Japanese language remake titled Monsterz directed by Hideo Nakata was released in 2014. It stars Tatsuya Fujiwara (in Kangs role) and Takayuki Yamada (in Gos role).  

== References ==
 

== External links ==
*   
*  at Naver  
*   
* 
* 
* 

 
 
 
 
 
 

 