Guns of the Timberland
{{Infobox film
| name           = Guns of the Timberland
| image          = Guns of the Timberlands.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Robert D. Webb
| producer       = Aaron Spelling Alan Ladd
| writer         = Louis LAmour (novel) Screenplay Joseph Petracca
| narrator       =
| starring       = Alan Ladd Jeanne Crain Gilbert Roland Frankie Avalon
| music          = David Buttolph
| cinematography = John F. Seitz
| editing        = Jaguar Productions
| distributor    = Warner Brothers
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Guns of the Timberland is a 1960 feature film starring Alan Ladd and Jeanne Crain.  It was made by Ladds Jaguar Productions and released through Warner Bros..

==Plot==
Logger Jim Hadley (Alan Ladd) and his lumberjack crew are looking for new forest to cut. They locate a prime prospect outside the town of Deep Wells. The residents of Deep Wells led by Laura Riley (Jeanne Crain) are opposed to the felling of the trees, believing that losing them would cause mudslides during the heavy rains. Conflict between the towns residents and the loggers is inevitable.

==Production==
The novel was optioned by Jaguar in 1957. MOVIELAND EVENTS: Guns of Timberland on Active Schedule
Los Angeles Times (1923-Current File)   03 Apr 1957: B8.  
 Plumas County.   The scenes involving the steam engine and railroad cars were shot on the Western Pacific Railroad right-of-way.  The scene where the steam engine goes over the tall "bridge" was shot using the Clio trestle.

Alana Ladd, Alan Ladds daughter, is featured as the love interest for Frankie Avalons character.

In the film Frankie Avalon sings the song "Gee Whiz Whillikins Golly Gee".

== References ==

===Notes===
 

===Bibliography===
* 
* 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 