Trainwreck (film)
{{Infobox film
| name           = Trainwreck
| image          = Trainwreck poster.jpg
| alt            = 
| caption        = Teaser poster
| director       = Judd Apatow
| producer       = {{Plain list |
*Judd Apatow
*Barry Mendel
}}
| writer       = Amy Schumer
| starring       = {{Plain list |
*Amy Schumer
*Tilda Swinton
*Bill Hader
*John Cena
*Brie Larson
*Colin Quinn
}}
| music          = Jon Brion
| cinematography = Jody Lee Lipes 
| editing        = Paul Zucker 
| studio         = Apatow Productions
| distributor    = Universal Pictures
| released       =   
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Jim Norton Norman Lloyd.

Principal photography began on May 19, 2014 in New York City. The film is scheduled to be released on July 17, 2015, by Universal Pictures.

== Cast ==
 
*Amy Schumer    as Amy
*Tilda Swinton  as Dianna
*Bill Hader  as Dr. Aaron Conners
*Brie Larson  as Kim
*Colin Quinn  as Gordon
*Barkhad Abdi 
*Mike Birbiglia  
*Jon Glaser  as Schultz
*Vanessa Bayer  as Nikki
*John Cena  as Steven
*Ezra Miller  as Donald
*LeBron James    as himself
*Method Man  Norman Lloyd  as Norman Jim Norton  as himself
*Daniel Radcliffe 
*Marisa Tomei 
*Randall Park 
*Keith Robinson
*Dave Attell
*Robert Kelly
*Dan Soder
*Jim Florentine
*Nikki Glaser http://www.comingsoon.net/movies/features/434173-from-the-set-of-amy-schumer-and-judd-apatows-trainwreck 
*Claudia ODoherty 
*Bridget Everett 
*Pete Davidson as Dr. Conners patient
 

== Production ==
On August 26, 2013, Universal Pictures optioned an untitled script written by Amy Schumer that she would also star in.  On November 27, 2013, it was announced that Judd Apatow would direct the film.  On January 8, 2014, it was announced that the film would be released on July 24, 2015.  On January 30, 2014, Bill Hader joined the cast of the film.  On February 18, 2014, Brie Larson joined the cast of the film.  On March 28, 2014, Colin Quinn, Barkhad Abdi, Mike Birbiglia, Jon Glaser, Vanessa Bayer, John Cena, Ezra Miller and Tilda Swinton were cast in the film.  On May 7, 2014, Method Man and LeBron James joined the cast of the film.  On June 30, Daniel Radcliffe was spotted filming some scenes for the film which confirmed his casting.    On July 1, Marisa Tomei was confirmed to star in the film.    In an interview with The New York Times, Schumer revealed that after dismissing the original script almost entirely, the story was rewritten with an amplified and comedic version of Schumers own past as its basis. 

=== Filming ===
Principal photography began on May 19, 2014, in New York City.  On June 2, the crew began filming in the area of Manhattan and Long Island.  On June 24, Schumer and Larson were spotted during the filming of some emotional scenes in the Central Park.  On June 30, actor Daniel Radcliffe was spotted in Bryant Park filming some scenes while walking dogs.  Principal photography ended on August 1, 2014. 

=== Music ===
In December 2014, it was announced that Jon Brion would compose the music for the film. 

== Marketing and promotion ==
A first look still from the film was released on July 18, 2014 by The New York Times. 

==Release==
A rough cut of the film premiered at South by Southwest on March 15, 2015. 

==Reception== rating average of 8.8/10.  On Metacritic, which assigns a normalized rating, the film has a score of 87 out of 100, based on 4 critics, indicating "universal acclaim". 

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 