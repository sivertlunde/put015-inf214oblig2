Swathi Thirunal (film)
{{Infobox film
| name           = Swathi Thirunal
| caption        = Anant Nag as Swathi Thirunal Rama Varma
| director       = Lenin Rajendran
| screenplay     = Lenin Rajendran
| narrator       = Murali Ambika Ambika
| cinematography = Madhu Ambat
| editing        = Ravi
| music          = M. B. Sreenivasan
| distributor    = Seven Arts
| studio         = Chithranjali Studios
| released       =  
| runtime        = 133 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
 Malayalam biographical Murali in other important roles. The film is based on the life of Swathi Thirunal Rama Varma, the Maharaja of the state of Travancore.

==Plot==
The story begins with Gowri Lakshmi Bayi entrusting the four-month-old Swathi Thirunal Rama Varma to the care of the English East India Company whose representative was Col. Munro. Then it cuts to when Swathi Thirunal is sixteen and takes over the reins of Travancore from his aunt Gowri Parvati Bayi. Swathi Thirunals biography is then shown against the backdrop of the music that he himself has composed. The movie ends with his death in 1846.

==Cast==
* Anant Nag as Swathi Thirunal Rama Varma (voice dubbed by Venu Nagavally) 
* Srividya as Gowri Parvati Bayi
* Nedumudi Venu as Iraiyamman Tampi Murali as Shadkala Govinda Marar Ambika as Narayani Panam Pillai Amma Kochamma
* Babu Namboothiri as Diwanji Subbaravu  Ranjini as Sugandhavalli Jagannathan as Vadivelu Innocent as Krishna Ravu
* Kaithapram Damodaran Namboothiri as singer
* Ravi Vallathol as singer
* Kanakalatha as Narayanis Thozhi
* Mehboob Sait ( guest role ) Kuyili as dancer
* Sujatha Thiruvananthapuram

==Soundtrack==
{{Infobox album
| Name       = Swathi Thirunal
| Type       = soundtrack
| Artist     = M. B. Sreenivasan
| Cover      = Swathi Thirunal Film.jpg
| Alt        = 
| Released   = 1987
| Recorded   = 
| Genre      = 
| Length     =  
| Label      = Tharangini Music
| Producer   = K. J. Yesudas
| Last album = 
| This album = 
| Next album = 
}} Swathi Thirunal, Tyagaraja and Iraiyamman Tampi. The soundtrack features vocals of several renowned singers including M. Balamuralikrishna, who received a State award for his work.

{| class="wikitable"
|-
! !! Song Title !! Singer(s) !! Raga
|-
| 1 || Pannakendra Sayana || K. J. Yesudas, M. Balamuralikrishna, Neyyattinkara Vasudevan || Raagamaalika (Sankarabharanam, Bhairavi (Carnatic)|Bhairavi, Bhupalam)
|-
| 2 || Aralsara Parithapam || K. J. Yesudas, B. Arundhathi || Surutti
|-
| 3 || Mokshamu Galadha || M. Balamuralikrishna || Saramathi
|- Vasantha
|- Karnataka Kapi
|-
| 6 || Parvathi Nayaka || K. J. Yesudas, K. S. Chithra || Bowli
|-
| 7 || Sarasamukha Sarasija || K. J. Yesudas || Madhyamavati
|- Dhanashree
|- Kanada
|-
| 10 || Devanuke Pathi ||  S. P. Balasubrahmanyam || Darbari Kanada
|-
| 11 || Krupaya Palaya || K. J. Yesudas || Charukesi
|-
| 12 || Jamuna Kinaare ||  M. Balamuralikrishna || Mishra Pilu
|-
| 13 || Kosalendra ||  Neyyattinkara Vasudevan || Madhyamavati
|-
| 14 || Chaliye Kunjanamo || K. S. Chithra || Vrindavani sarang
|-
|}

Additional tracks from the movie

{| class="wikitable"
|-
! !! Song Title !! Singer(s) !! Raga
|-
| 1 || Bhaja Bhaja Manasa || M. Balamuralikrishna || Sindhu Bhairavi
|- Shree
|-
| 3 || Mamava Sada Varade ||  S. Janaki || Nāttai Kurinji
|-
| 4 || Omanathinkal Kidavo || S. Janaki || Kurinji
|-
| 5 || Omanathinkal Kidavo || B. Arundhathi || Kurinji
|-
| 6 || Praananaathan || B. Arundhathi || Kambhoji
|-
| 7 || Sa Ni Dha Sa (Swarangal) || Chorus  ||
|-
| 8 || Anjaneya ||  K. J. Yesudas || Saveri
|- Venmani Haridas ||
|-
|}

==Awards==
Kerala State Film Awards: Special Jury Award - Lenin Rajendran Best Play Back Singer  - M. Balamuralikrishna Best Art Director - P. Krishnamoorthy

==References==
 

== External links ==
*  

 
 
 
 
 