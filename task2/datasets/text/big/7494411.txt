Sketches of Frank Gehry
{{Infobox film
| name           = Sketches of Frank Gehry
| image          = Sketches of frank gehry.jpg
| caption        = Promotional movie poster for the film
| director       = Sydney Pollack
| producer       = Ultan Guilfoyle
| writer         =
| narrator       =
| starring       = Frank Gehry, Dennis Hopper, Philip Johnson, Edward Ruscha
| music          = Sorman & Nystrom
| cinematography = George Tiffin
| editing        = Karen Schmeer
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
}} 2006 American documentary film directed by Sydney Pollack and produced by Ultan Guilfoyle, about the life and work of the Canadian-American architect Frank Gehry. The film was screened out of competition at the 2006 Cannes Film Festival.    Pollack and Gehry had been friends and mutual admirers for years.  The film features footage of various Gehry-designed buildings, including a hockey arena for the Mighty Ducks of Anaheim, the Guggenheim Museum Bilbao, and the Walt Disney Concert Hall. The film includes interviews with other noted figures, including the following:

* Charles Arnoldi
* Barry Diller
* Michael Eisner Hal Foster
* Bob Geldof
* Dennis Hopper
* Charles Jencks
* Philip Johnson
* Thomas Krens (former director of the Solomon R. Guggenheim Museum)
* Herbert Muschamp
* Michael Ovitz
* Robert Rauschenberg
* Edward Ruscha
* Esa-Pekka Salonen
* Julian Schnabel
* Dr Milton Wexler (Gehrys therapist)

The film also discusses work on Gehrys own residence, which was one of the first works that brought him to notoriety.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 