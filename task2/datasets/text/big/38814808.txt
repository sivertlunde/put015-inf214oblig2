Vinyl (2012 film)
{{Infobox film
| name           = Vinyl
| image          = 
| alt            = 
| caption        = 
| director       = Sara Sugarman
| producer       = John H. Williams, Clay Reed & Sara Sugarman
| writer         = Sara Sugarman & Jim Cooper Keith Allen Jamie Blackley Mike Peters
| cinematography = Benji Bakshi
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Mike Peters 45 RPM" under the name of a fictitious band "The Poppy Fields". 
 Keith Allen, Perry Benson, Jamie Blackley and Julia Ford. 

Vinyl has a soundtrack written and performed by The Alarm with Mike Peters, Phil Daniels and Keith Allen all making contributions.

Filmed mostly on location in Rhyl, it features many local attractions and features. Despite being a USA production the cast is totally British with many of the actors having connections to North Wales, particularly Rhyl. The cast also includes many past members of The Rhyl T.I.C. (Theatre in the Community) which at the time of filming provided many of the younger cast including members of the fake band, the auditionees, security guards, music business employees and of course the fans. The local community of Rhyl also provided location venues in which the crew could film such as The Rhyl Pavilion, Robin Hood Caravan Park, Glan Clwyd Hospital and The Bistro night club. This allowed the film to stay close to the original true story and have the feel of an authentic biog picture.

==Synopsis== music charts. single racing Keith Allen and Johnny Jones, it is not long before the truth is exposed. Suddenly, the media focus is back on Johnny and his fight for equality in the music business. This leads to a comedic finale and a heartwarming ending.

==Cinematic release==
*USA Limited release April 2012
*UK National release 15 March 2013

==Awards==
* Winner of The Valley Film Festival "10 Degrees Hotter" Award for Best Feature

==References==
 

==External links==
*  
*  

 
 