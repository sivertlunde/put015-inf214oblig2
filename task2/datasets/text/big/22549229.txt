The Ugly Duckling (1931 film)
 
 
{{Infobox Hollywood cartoon
| cartoon name      =The Ugly Duckling
| series            = Silly Symphonies
| image             =
| image size        = 
| alt               =
| caption           =
| director          = Wilfred Jackson
| producer          = Walt Disney
| story artist      =
| narrator          =
| voice actor       = Florence Gill
| musician          =  Dick Lundy David Hand  
| layout artist     = Charles Philippi
| background artist =
| studio            = Walt Disney Productions
| distributor       = Columbia Pictures
| release date      = December 16, 1931
| color process     = Black and white
| runtime           = 7 min
| country           =
| language          = English The Fox Hunt
| followed by       = The Bird Store
}}
 color version released in 1939.

== Summary ==
Although the short film is loosely based on the Hans Christian Andersen fairy tale "The Ugly Duckling", the only real similarities are one bird getting confused for another and his unique abilities enabling him to become something special. In this version, a duckling has gotten mixed in among the farmyard chickens. Despite his best attempts to fit in with his chick siblings, things dont work out. However, when the hens chicks are threatened by a waterfall, due to them being dropped off in a river by a cyclone, the little duckling saves them and is lauded as a hero.

About eight years later, in 1939, the film would be remade and would follow the original Andersen story much more faithfully. This gives The Ugly Duckling the unique distinction of being the only Silly Symphony to be made twice.

== See also ==
* The Ugly Duckling (1939 film)|The Ugly Duckling (1939 film)

== External links ==
*   at  
*  

 

 

 
 
 
 
 
 
 
 
 

 