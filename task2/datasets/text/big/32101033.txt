Perras
{{Infobox film
| name           = Perras
| image          = 
| caption        = 
| director       = Guillermo Ríos
| producer       = Tita Sánchez
| writer         = 
| screenplay     = Guillermo Ríos
| story          = Guillermo Ríos
| based on       =  
| starring       = Claudia Zepeda Karen de la Hoya Scarlet Dergal Steph Bumelcrownd
| music          = Daniel Hidalgo Valdés Tomás Barreiro Guijosa Pablo Chemor Nieto Mellow Man Ace & Jerónimo Alenka Ríos
| cinematography = 
| editing        = Elise Du-Rant Guillermo Ríos Tita Sánchez Juan Bernardo Sánchez Mejía
| studio         = Pelearán Diez Rounds Films Fondo de Inversión y Estímulos al Cine
| distributor    = Artecinema
| released       =  
| runtime        = 96 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}}
 Thriller Drama film directed by Guillermo Ríos. Its plot revolves around 10 schoolgirls who are all suspects of something terrible that happened at school, focusing in the memories and feelings of each girl.

The film premiered in theaters in Mexico City on March 4, 2011 and had a limited distribution through the whole country.

==Plot==
10 schoolgirls have to remain in their classroom and wait for the policemen to arrive and interrogate them, since they all are suspects of something terrible that had just happened at school. As they wait, the memories and feelings of each girl are explored.

==Cast==
* Claudia Zepeda as María del Mar Solís, a.k.a. "La Matada" (The Nerd). 15 years old, Aries. She loves to dance, she dreams of being a model and love is a main topic in her life. At first, she disliked being pointed out as a "goody girl" due to her sweet and fragile appearance, but then she realized she could use it to her own advantage.  She lives with her mother, who is a radio host.  Shes a good friend of Sofía, whom she frequently visits at her home.
 orhopedic device on her leg. Shes often bullied by the other girls, especially by Sofía, and her only true friend seems to be Frida. When she was little (played by Gema Nicte-Ha) she would spend a lot of time with her grandfather, who promised to organize her Quinceañera party.

* Scarlet Dergal as Sofía Ibar, a.k.a. "La Manchada" (The Mean Girl). 16 years old, Scorpio. A spoiled and mean girl who gets anything she wants. Shes been expelled from many schools. She lives with her father, who is a judge and spends the weekends with her mother. She has no aspirations in life, so she picks on the other girls to distract herself. Deep down, she has a sweet nature.

* Steph Bumelcrownd as Iris Hernández, a.k.a. "La Ñoña" (The Fussy Girl). 14 years old, Taurus. A ditzy and fresa girl who only wants to be pretty and dreams to be like Sofía. She loves to read all sorts of magazines. Her father is a doctor. She often carries with her a tiny pink purse where she keeps everything she needs. She considers herself to be Sofías best friend.

* Andrea Pedrero as Diana Fragoso, a.k.a. "La Ciega" (The Blind Girl). 15 years old, Pisces. A softspoken girl who was blinded at a young age after accidentally spilling antifreeze on her eyes. She has clairvoyance, which she tries to keep secret. She lives with her grandmother. Her mother is an alcoholic and her father lives with his other family.

* Eva Luna Marenco as Frida Gómez, a.k.a. "La Amiga" (The Friend). 15 years old, Capricorn. She feels somewhat ashamed of her body development, which she hides by wearing baggy clothes. Her father is a corrupt policeman, so her family is well off. She loves to read and seems to be Toras only true friend. 20 years later, she had turned into a beautiful woman (played by Galilea Montijo) who has a son.

* Natalia Zurita as Patricia Gaytán, a.k.a. "La Zorra" (The Whore). 17 years old, Virgo. An extroverted and easygoing girl. Her family used to be wealthy and she attended to a private school, but after pictures of her having sex with a senator were spread, she was expelled. She has a weakness for expensive cars and dreams to move to New York to become a dancer.

* Denis Montes as Alejandra Suárez, a.k.a. "La Valemadres" (The I dont give a shit Girl). 16 years old, Sagittarius. A rude, but friendly rebellious girl. She loves to watch soap-operas and has a big Star Wars collection. Shes bisexual. She lives with her family at her grandmothers apartment. Her family often mistreats her grandmother. Her father often travels around the world and brings souvenirs to his family.

* Kariam Castro as Andrea Romero, a.k.a. "La Rara" (The Weird Girl). 14 years old, Libra. A lonely and eccentric girl. She has no real motivation in life, although she likes to dance. She likes to get obsessed with boys, until they become her boyfriends, then she loses any interest in them. Her mother is an alcoholic and she lives with her father and his family. She hates her new family and prefers to spend time with her mother, even though they get into terrible arguments.

* Alenka Ríos as Ana Ceci Flores, a.k.a. "La Mustia" (The Gloomy Girl). 17 years old, Gemini. A troubled girl. She has a black sense of humor. Her parents are teachers who think shes a troublemaker, but actually the troublemaker is her sister, who Ana Ceci admires. She often hangs out with Sofía and Iris.

==Stage Play==
Previously, around 2008, there was a stage play of Perras written by Guillermo Ríos with the following cast:
* Karen De La Hoya – Tora “La Gorda”
* Kariam Castro – Andrea “La Rara”
* Natalia Zurita – Patricia “La Zorra”
* Eva Luna Marenco – Frida “La Amiga”
* Andrea Pedrero – Diana “La Ciega”
* Denis Montes – Alejandra “La Valemadres”
* Claudia Zepeda – María del Mar “La Matada”
* Scarlet Dergal – Sofía “La Manchada”
* Alenka Ríos – Ana Ceci “La Mustia”
* Steph Boumelcroud – Iris “La Ñoña”
* Galilea Montijo – Frida (veinte años después) “La Más Perra”

==Homevideo Releases==
The DVD and Blu-ray releases have been announced for sometime in September 2011.

==Trivia==
* In one scene, Andrea sings the theme song of Quinceañera (telenovela)|Quinceañera and is later joined by the other girls. Marimar is the one from TV!"
* Alenka Ríos is the only actress who appeared in both the stage play and movie, although she played different characters.
* The main cast of the movie created Facebook profiles for their respective characters where they explain some aspects of their characters that werent explored in the movie.

==References==
(1) http://www.perraslapelicula.com/
(2) http://www.imdb.com/title/tt1857824/
(3) http://www.todopuebla.com/peliculas/perras

==External links==
*   (Spanish)
*  

 