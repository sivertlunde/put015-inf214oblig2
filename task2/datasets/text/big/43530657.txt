Mendelsohn's Incessant Visions
 
 
{{Infobox film name = Mendelsohns Incessant Visions image =   director = Duki Dror released =   runtime = 71 minutes country = Israel language = English
}}

Mendelsohns Incessant Visions is a 2011 documentary film directed by Duki Dror. The film takes as its focus the German-Jewish architect Erich Mendelsohn, exposing the complexities and intricacies of Mendelsohns life and work through a series of letters with Louise, the young cellist who would eventually become his wife. Drors film is at once an examination of Mendelsohns seminal architectural work as well as an exploration of the German-Jewish experience during and after World War II seen through Mendelsohns journey from Germany to England, British Mandate Palestine and the US.

==Synopsis==
The film follows the trajectory of Mendelsohns career, bringing to life the stories (and, in interviews with other architects and experts, the impact) of his many influential buildings such as the Einstein Tower observatory in Potsdam and the Universum (the modern day Schaubühne building), believed to be the first modern cinema in the world. Though Mendelsohn enjoyed the status of one of Germanys most important and successful architects, the outbreak of World War II led him to flee Germany.  Utilizing over 1,200 personal letters penned between Mendelsohn and the 16 year-old cellist Louise who would later become his wife, the film brings to life both the historical context in which Mendelsohn lived and worked as well as the architects personal struggles and eccentricities.   The letters between Mendelsohn and Louise also serve to bring to life other famous characters of Mendelsohns era, including the German poet and playwright Ernst Toller whose affair with Louise provides the film with additional drama. 

==References==
 
 


 