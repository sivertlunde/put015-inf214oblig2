Alien Souls
{{infobox film
| title          = Alien Souls
| image          =
| caption        =
| director       = Frank Reicher
| producer       = Jesse Lasky Margaret Turnbull(story) Hector Turnbull(story)
| starring       = Sessue Hayakawa Tsuru Aoki
| music          =
| cinematography = Walter Stradling
| editing        =
| distributor    = Paramount Pictures
| released       = May 11, 1916
| runtime        = 5 reels
| country        = USA
| language       = Silent film(English intertitles)
}}
Alien Souls is a lost 1916 silent film feature directed by Frank Reicher and starring Sessue Hayakawa, his real-life wife Tsuru Aoki and Earle Foxe.  

==Cast==
*Sessue Hayakawa - Sakata
*Tsuru Aoki - Yuri Chan
*Earle Foxe - Aleck Lindsay
*Grace Benham - Mrs. Conway
*J. Parks Jones - Jack Holloway
*Violet Malone - Gertrude Van Ness
*Dorothy Abril - Geraldine Smythe

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 