Agninakshathram (1977 film)
{{Infobox film
| name           = Agninakshathram
| image          =
| caption        =
| director       = A. Vincent
| producer       = MO Joseph
| writer         = CL Jose Thoppil Bhasi (dialogues)
| screenplay     = Lakshmi Mohan Sharma Adoor Bhasi Kottayam Santha
| music          = G. Devarajan
| cinematography = S Navkanth
| editing        = MS Mani
| studio         = Manjilas
| distributor    = Manjilas
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film,  directed by A. Vincent and produced by MO Joseph. The film stars Lakshmi (actress)|Lakshmi, Mohan Sharma, Adoor Bhasi and Kottayam Santha in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  Lakshmi
*Mohan Sharma
*Adoor Bhasi
*Kottayam Santha
*Sam
*Sankaradi
*James
*Baby Madhavi
*Bahadoor
*Bindulatha
*George Thachil
*Janardanan
*KPAC Sunny
*MG Soman
*Mallika Sukumaran
*Master Thankappan
*N. Govindankutty
*Nanditha Bose
*Oduvil Unnikrishnan
*P. K. Abraham
*Paravoor Bharathan
*Paul Manjila
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sasikala Menon.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chentheekkanal Chinnum || P. Leela, P. Madhuri, Latha Raju || Sasikala Menon ||
|-
| 2 || Kanmanippaithale || P. Madhuri || Sasikala Menon ||
|-
| 3 || Navadambathimaare || K. J. Yesudas, Chorus || Sasikala Menon ||
|-
| 4 || Nithyasahaaya Maathave || P Susheela || Sasikala Menon ||
|-
| 5 || Swarnameghathukilin || K. J. Yesudas, P. Madhuri || Sasikala Menon ||
|}

==References==
 

==External links==
*  

 
 
 


 