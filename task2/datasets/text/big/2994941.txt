Friend (2001 film)
{{Infobox film
| name           = Friend
| image          = Friend (2001 film) poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Chingu
 | mr             = Chingu}}
| director       = Kwak Kyung-taek
| producer       = Seok Myeong-hong   An Chang-guk   Hyeon Gyeong-rim   Jo Won-jang
| writer         = Kwak Kyung-taek Kim Bo-kyung
| music          = Choi Man-sik   Choi Sun-sik   Im Ju-hui   Choe Seung-yeon   Oh Hye-won    
| cinematography = Hwang Gi-seok
| editing        = Park Gok-ji
| distributor    = Korea Pictures
| released       =  
| runtime        = 118 minutes
| country        = South Korea
| language       = Korean
| budget         = 
}}
 ,  
Friend ( ) is a 2001 South Korean film written and directed by Kwak Kyung-taek. Upon its release, it became the List of highest-grossing films in South Korea|highest-grossing South Korean movie of all time. Its record was surpassed in 2003 by Silmido (film)|Silmido.
 Busan dialect. cult interest. 

==Plot==
The film follows the lives of four childhood friends: Joon-seok, the leader of the group and whose father is a powerful mob boss; Dong-su, whose father is an undertaker; class clown Jung-ho; and Sang-taek, who was an exemplary student. As children they play together, sell sexually explicit pictures cut from a magazine, and wonder if a South Korean Olympic swimmer could outrace a sea turtle. Jung-ho also showed the rest of them a VCR that his mother had, and this film he found on it. It turns out to be a pornographic movie, and the boys are entranced while arguing over what a "menstruation" was, with them believing that it was the word that adults used to describe the "vagina".
	
Fast forward to high school, where they are reunited after separating during middle school. They become smitten with the lead singer of a band of girls their age. Joon-seok invites the band to a party at his house, where each boy pairs off with one girl (though not after some tension over how to do so). Sang-taek receives his first kiss from the lead singer, Jin-sook.
	
Back in school, Joon-seok and Dong-su get in trouble after a confrontation with a teacher; After their friends convince them to go apologize, they are only off with a light suspension. During an outing to the movies, where Sang-taek catches the eye of another school kid whom he had picked a fight with earlier, Joon-seok and Dong-su fend off a whole rival school while Jung-ho protects Sang-taek. Dong-su, after the fight, returns with a steel rod and smashes the schools glass cases with its awards and trophies, dropping out of school and warning anyone who bumps into him on the street will "pay". Sang-taek is suspended from the affair. After graduation, Sang-taek and Jung-ho go to college but the others do not.

A few years later Sang-taek and Jung-ho return to find Joon-seok married to Jin-sook, the singer from the band. He is suffering severe withdrawal symptoms and is abusive towards his wife as a result of being addicted to Methamphetamine|philopon; he also appears depressed that his father is dying. Later he recovers from his addiction, divorces his wife, and mourns his father. He assumes his fathers role as a crime lord, working under Hyung-doo. Dong-su also becomes a mobster with a rival organization, led by Sang-gon (who is shown earlier to be Hyung-doos driver and therefore a former member of Joon-seoks fathers organization - he has since become independently powerful, and based on a conversation he has in a restaurant, it can be deduced that he did so by hurting his old organization), and becomes similarly powerful.

Joon-seok, Sang-taek, and Jung-ho remain close, drinking, singing karaoke, and eating galbi together during reunions. After Dong-su causes Joon-seoks boss to be imprisoned, a rogue assassination attempt, headed by Doruko, is led against Dong-su without Joon-seoks knowledge. The attack fails, and in retaliation, Dong-su mounts an attack on Joon-seoks fishing facilities, during which many of Joon-seoks men, including Doruko, are killed.

After the attack, Joon-seok visits Dong-su at his headquarters. Joon-seok talks to Dong-su as if nothing has happened, and asks if he would like to accompany him to see Sang-taek off, since he was heading to the United States. Dong-su, however, does not want to, and Joon-seok becomes serious. Joon-seok, knowing that Dong-su is being targeted for assassination by Sang-gon and his own gang for the (apparently unsanctioned) attack on Joon-seoks gang, asks Dong-su as a friend to leave for Hawaii for a few years until things cooled down. Dong-su, however, refuses and tells Joon-seok to leave to Hawaii instead. Joon-seok leaves to say farewell to Sang-taek.  After mulling over the conversation Dong-su decides after all to see Sang-taek off at the airport. However, as he is about to leave, Dong-su is betrayed by his gang and stabbed to death by gang members.

A few years later, Sang-taek returns to South Korea upon finishing his study abroad. Jung-ho explains how during Sang-taeks absence Joon-seoks gang sent him into hiding abroad. After two years, he was unable to stand the hiding any longer and had a mental breakdown. He was caught after attempting to kill himself in a foreign bar. Joon-seok goes to trial for Dong-sus murder and, even after Jung-ho bribes the jurors, Joon-seok pleads guilty to ordering Dong-sus death.

After the trial, Sang-taek visits Joon-seok in prison, and the two talk like old friends who havent seen each other. When the time comes for Joon-seok to leave to go back to his jail cell, Sang-taek asks why he pleaded guilty in court. Joon-seok simply replies, "Embarrassment. Me and Dong-su are mobsters. Mobsters shouldnt be embarrassed" - his inability to protect one of his childhood friends is clearly a source of deep personal embarrassment. Sang-taek and Joon-seok then part ways, with Sang-taek promising to visit every month even though they both know this will be the last time they see each other. Joon-seok will be executed before the next visit. The film ends with Joon-seok walking to his execution place, in a hallway with a white bright light in the end, with Joon-seok reflecting on the past when they were all children, wondering who would win in a race, the South Korean Olympic swimmer or a sea turtle, when they were all good friends.  The narrator ends with the reflection that back then, Joon-seok had actually been on Dong-sus side of that argument (that the sea turtle would win).

==Cast==
*Yu Oh-seong as Joon-seok
*Jang Dong-gun as Dong-su
*Seo Tae-hwa as Sang-taek
*Jung Woon-taek as Jung-ho Kim Bo-kyung as Jin-sook
*Gi Ju-bong as "mustache" Lee Jae-yong as "blade scar"
*Joo Hyun as Joon-seoks father
*Kim Joon-beom as young Sang-taek
*Kang Shin-il as Sang-taeks father
*Kwon Nam-hee as Sang-taeks mother
*Park Nam-hee as Joon-seoks mother
*Tiger JK as Crime boss Kim

==Awards== Torino International Festival of Young Cinema, it won the Holden Award for best script, and was nominated, but missed out on, the Prize of the City of Torino award for best film. 
 Manaki Brothers International Cinematographers Film Festival, and Jung Woon-taek won Best New Actor at the Baeksang Arts Awards. 

==Remake==
  television remake MBC in 2009.

==Sequel==
 
Yu Oh-seong reprised his role in the 2013 sequel, which takes place seventeen years after the events of this film. In it, Joon-seok meets the grown-up son of Dong-su (Kim Woo-bin), interspersed with scenes of Joon-seoks own father (Joo Jin-mo) in 1963.

==References==
 

==External links==
*  
*  

 
 
 
  
 
  
 
 

 
 
 
 
 
 
 
 
 
 
 