The Legend of Sleepy Hollow (1980 film)
{{Infobox film
| name = The Legend of Sleepy Hollow
| image =
| caption = cover
| format = TV Movie
| genre = Comedy Fantasy Horror
| director = Henning Schellerup
| producer = James L. Conway Charles Sellier (executive producer) Stan Siegel (associate producer)
| writer = Malvin Wald Jack Jacobs Thomas C. Chapman
| based on = "The Legend of Sleepy Hollow" by Washington Irving
| starring = Jeff Goldblum Meg Foster Dick Butkus Paul Sand Laura Campbell James Griffith Michael Ruud Karin Isaacson H.E.D. Redford Tiger Thompson John Sylvester White Michael Witt Marneen Fields
| music = Bob Summers
| cinematography = Paul Hipp
| editing = Michael Spence
| studio = Schick Sunn Classics Sunn Classic Pictures
| distributor = NBC Lucerne Media Starmaker Video VCI Home Video
| released = October 31, 1980
| runtime = 104 minutes
| country = United States English
}}

The Legend of Sleepy Hollow was a 1980 television movie on NBC filmed in Utah. It starred Jeff Goldblum as Ichabod Crane, Meg Foster as Katrina, and Dick Butkus as Brom Bones. The film is also known as La leggenda di Sleepy Hollow in Italy. It was directed by Henning Schellerup. Executive producer Charles Sellier was nominated for an Emmy Award for his work on the movie.

The film was not closely adapted to the original story, depicting Crane as a skeptic regarding ghosts and the supernatural, although it foreshadowed Tim Burtons similar 1999 treatment.

== Cast ==
*Jeff Goldblum as Ichabod Crane
*Paul Sand as Frederic Dutcher
*Meg Foster as Katrina Van Tassel
*Laura Campbell as Thelma Dumkey
*Dick Butkus as Brom Bones
*James Griffith as Squire Van Tassel
*Michael Ruud as Winthrop Palmer
*H.E.D. Redford as Karl
*Tiger Thompson as Ted Dumkey
*John Sylvester White as Fritz Vanderhoof
*Michael Witt as Jan Van Tassel
*Marneen Fields as Singer #1 (uncredited)

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 