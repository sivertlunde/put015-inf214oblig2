Spadikam
{{Infobox film
| name           = Spadikam
| image          = Spadikam.jpg
| image_size     = 150
| caption        = DVD movie poster Bhadran
| producer       = R. Mohan
| screenplay     = Bhadran Dr. Rajendra Babu  (dialogue) 
| story          = Bhadran
| narrator       = Urvashi Spadikam Chippy
| music          = S. P. Venkatesh P. Bhaskaran  (lyrics) 
| cinematography = J. Williams S. Kumar (cinematographer)|S. Kumar
| editing        = M. S. Mani
| Art Direction  = Valsan
| distributor    = Manorajyam Release 
| studio         = Shogun Films Ltd.
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =  8 cr
}}
 Malayalam Action drama film aka Aadu Thoma. The films name "Spadikam" means prism, which is a metaphor for the human nature. Just like how white light splits into many colors by a prism, every human transforms themselves into a different character when faced with various experiences in life. The film also stars Thilakan, Urvashi (actress)|Urvashi, Spadikam George, KPAC Lalitha, Rajan P. Dev, Silk Smitha, Nedumudi Venu, and Chippy (actress)|Chippy.The film was the highest grossing film of the year with a collection of 8 cr,and was the longest running movie of the year with a run of 225 days at the box office.
 Filmfare Award narcissistic father Chacko Master (Thilakan), upon failing to meet the latters high expectations.  It is regarded as one of the best Malayalam commercial films, and Aadu Thoma is one of the most celebrated characters in Malayalam cinema, the film and its characters hold a cult status in Keralite pop culture. 

==Plot==
Chacko Master (Thilakan) is a high-school headmaster who is a presidents medal winner in Mathematics. He is a very strict teacher and emphasizes memorization and discipline at the expense of analysis and creativity in school. He punishes his students ruthlessly and gives no special treatment to his son Thomas, who is also a student in his class. Chacko always assumes that his son is behind every mischief in class, and punishes him until the actual culprit gives himself in.

Unlike his father, Thomas isnt good in math (he cannot memorize the binomial theorem) but he is very creative and productive in designing mechanical and electronic gadgetry like a mechanical school bell which his father destroys later. For instance, he impresses his mother and sister by developing an AM radio receiver inside a soap-case at a time when phonographs were more prevalent in his village. When his father finds out about the receiver he throws it into flames and destroys it; he forces his son to focus on his studies instead of being an inventor. Thomas works hard, and he does well in his language paper, but his dad asks his language teacher(Ravunni Master-Nedumudi Venu) to grade his paper with no leniency and fail him; his father believes that only low grades will provoke Thomas to study harder with a competitive spirit. His language teacher does as asked, but regrets it later on! When Thomas finds out that even his most beloved language teacher was biased against him he becomes outraged. He runs away from home.

Fourteen years later, he returns to his village. Thomas, by now, had changed into a totally different person. He was no longer interested in clockwork and gadgetry; he owned a stone quarry and had become a well known rowdy wanted by the authorities as a prime suspect in many unsolved crimes. His father still hated him and never accepted him as family.
Toma and Ravunni masters daughter Thulasi Urvashi also his childhood friend grow closer. But as the film progresses, both father and son regret what they had done to each other. In an interesting turn of events the father and the son reunite in a very emotional way, but only to find the evil forces chasing Thoma.The movie ends with Chako master dies in hospital and Thomas chacko taken to custody

==Cast==
* Mohanlal ...  Aadu Thoma/Thomachan (Thomas Chacko)
* Thilakan ...  Chacko Master (Aadu Thomas father)
* Spadikam George ...  Kuttikkadan (Police Officer) Urvashi ...  Thulasi (Thomas love interest) Chippy ...  Jansy Chacko (Thomas younger sister)
* K.P.A.C. Lalitha ...  Mary/Ponnamma (Thomas mother)
* Rajan P. Dev ...  Manimala Vakkachan (Thomas Uncle)
* V. K. Sreeraman ...  Pookoya
* Silk Smitha ...  Laila
* Nedumudi Venu ...  Ravunni Master (Thulasis father)
* Ashokan ...  Jerry
* Karamana Janardanan Nair ...  Fr. Ottaplakkan
* N F Varghese ...  Pachu Pillai (Police Constable)
* Maniyanpilla Raju ...  Kunju Mohammad (Thomas friend) Bahadur ...  Kurup
* Kundara Johny ...  Maniyan
* Bheeman Raghu ...  Somasekharan Pillai (Police Officer)
* Roopesh Peethambaran ... Young Aadu Thoma (Thomas Chacko)
* Indrans
* Kanakalatha
* Paravoor Bharathan ...  Joseph
* Kanakalatha ...  Kuttikkadans Wife
* Sankaradi ...  Judge
* N. L. Balakrishnan

==Crew==
*Directed by: Bhadran
*Written by: Dr.C.G.Rajendra Babu
*Produced by: R. Mohan
*Music: S.P. Venkitesh
*Lyrics: P. Bhaskaran
*Cinematography: J. Williams
*Editing: M.S. Mani
*Costume Design: Indrans
*Assistant Director: Gandikuttan
*Stunts: B. Thyagarajan 
*Stills: N.L. Balakrishnan
*Art Directors: Valsan and Mannar Rajan
*Singers: M. G. Sreekumar, K.S. Chitra, Mohanlal

==Shooting Location==

The film was entirely shot in the town of Changanassery and nearby places. Most of the locations still look exactly as depicted in the film and some of the place names used in the story are also real.

==Soundtrack==
The songs were written by renowned lyricist P. Bhaskaran and composed by S. P. Venkatesh.

{| class="wikitable"
|-
! !! Song Title !! Singer(s)
|-
| 1 || Ormakal || M. G. Sreekumar
|-
| 2 || Ezhimala Poonchola || K. S. Chithra, Mohanlal
|-
| 3 || Ormakal || K. S. Chithra
|-
| 4 || Parumala Cheruvile || K. S. Chithra
|}

==Remakes==
The film was remade in Telugu as Vajram (1995 film)|Vajram with Akkineni Nagarjuna|Nagarjuna, in Tamil as Veerappu with Sundar C. and in Kannada as Mr. Theertha with Sudeep.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
! Box Office
|- Telugu ||Akkineni Nagarjuna ||S. V. Krishna Reddy ||
|- Tamil ||Sundar Badri ||
|- Kannada ||Sudeep Sadhu Kokila ||
|-
|}

==Awards==
;Kerala State Film Awards Best Actor - Mohanlal

;Filmfare Awards South Best Actor - Mohanlal

== References ==
 

== External links ==
*  

 
 
 
 