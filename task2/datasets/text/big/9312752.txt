Shadow Run (film)
 
{{Infobox Film  |
  name     = Shadow Run |
    producer         = Geoffrey Reeve Richard Morris-Adams |
  director       = Geoffrey Reeve |
  writer         = Desmond Lowden |
  starring       = Michael Caine James Fox Christopher Cazenove | David Whitaker Aidan Burch |
  editing         = Terry Warwick |
  distributor    = Boulevard Entertainment |
  released   = Unknown, 1998 |
  runtime        = 94 min. | English |
}}

Shadow Run is a 1998 crime film set in the south of England, United Kingdom|UK. It is based on the novel by Desmond Lowden. It was not released in the cinema, and eventually appeared on home video in 2001.

==Plot summary==
Haskell (Michael Caine) is a hardened gangster assigned to a job by his boss, Landon-Higgins (James Fox), to steal sheets of watermarked paper to print fake money.

==Cast==
*Michael Caine as Haskell
*James Fox as Landon-Higgins
*Matthew Pochin as Joffrey
*Rae Baker as Julie
*Kenneth Colley as Larcombe
*Christopher Cazenove as Melchior
*Rupert Frazer as Maunder
*Leslie Grantham as Liney Tim Healy as Daltrey
*Emma Reeve as Victoria
*Katherine Reeve as Zee
*Angela Douglas as Bridget
*Maurice Thorogood as Walter
*Richard Tuck as Morton John Bray as Hallam
*Khan Bonfils as Baz

==External links==
* 

 
 
 
 
 


 