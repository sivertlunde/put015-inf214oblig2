The Angel with the Trumpet (1950 film)
{{Infobox film
| name           = The Angel with the Trumpet
| image          = 
| caption        = 
| director       =  Anthony Bushell
| producer       = Karl Hartl
| writer         = Franz Tassie Karl Hartl
| starring       = Eileen Herlie
| cinematography = 
| music          = 
| studio = London Films
| distribution   = British Lion Films
| released       = 20 March 1950
| runtime        = 
| country        = United Kingdom English
| gross = £86,265 
}} a 1948 Austrian film of the same title.

==Production==
To reduce costs, this British film reused much of the earlier Austrian film, especially for distance shots and for scenes with minor characters who were dubbed. In this way, Maria Schell and Oskar Werner launched their international careers in this film. 

It was the first film Bushell directed. 

==Plot==

The head of a Viennese piano manufacturing firm marries the daughter of a Jewish academic. She has loved the Habsburg Crown Prince who can not marry her. 

==Partial cast==
* Eileen Herlie - Henrietta Stein 
* Basil Sydney - Francis Alt 
* Norman Wooland - Prince Rudolf 
* Maria Schell - Anna Linden 
* Olga Edwardes - Monica Alt 
* Andrew Cruickshank - Otto Alt 
* John Justin - Paul Alt 
* Oskar Werner - Herman Alt 
* Anthony Bushell - Baron Hugo Traun
* Anton Edthofer - Emperor Franz Joseph
* Jane Henderson - Gretel Paskiewicz 
* Wilfrid Hyde-White - Simmerl
* John Van Eyssen - Albert Drauffer

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 