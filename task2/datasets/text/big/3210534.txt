Saw (2003 film)
{{Infobox film name            = Saw 0.5 image           = Jigsawpuppet.JPG caption  Billy the Puppet delivers instructions to David director        = James Wan producer        = Darren McFarlane writer          = James Wan Leigh Whannell starring        = Leigh Whannell Paul Moder Katrina Mathers Dean Francis music           = Charlie Clouser cinematography  = Martin Smith editing         = James Wan Neil Monteith distributor     = Lions Gate Home Entertainment released        =   runtime         = 9:30 country         = Australia language        = English budget          = Australian dollar|A$5,000 (US$3,000) 
}}

Saw 0.5 is a nine-and-a-half-minute Australian short subject horror film released in 2003. It was directed by James Wan and written by Wan and Leigh Whannell, the latter also starring in it. It was originally used to pitch their script for a full-length feature film Saw (2004 film)|Saw to various studios and actors. The full-length film was eventually made in 2004.  The short film later became a scene in Saw, with Shawnee Smith as Amanda Young wearing the Reverse Bear Trap device instead of David. The original short can be viewed on the second disc of Saw: Uncut Edition.   

==Plot== David (Leigh Whannell|Whannell), is in an interrogation room talking to an unnamed, unsympathetic police officer (Paul Moder). David is in handcuffs, and he has blood on his face and shirt. He is smoking a cigarette. He tells the officer that after he finished his work as an orderly at the hospital, he was knocked unconscious and taken to a large room.
 frightening puppet that tells him that the device on his head is a "reverse animal trapping#Foothold traps or Leghold traps|beartrap", which is hooked into his jaws and will pry his face open with great force if he does not unlock it in time. The puppet tells David that the only key to unlock the device is in the stomach of his dead cellmate (Dean Francis).

David is able to break free of his bonds, but by doing so he sets off a timer on the back of the device. Across the room, he finds the body that the puppet mentioned, but also finds that the man is actually alive but under paralysis. David panics and stabs his cellmate before starting to look for the key. He slices into the mans stomach and after finding the key, David unlocks the device and throws it to the ground, just as it snaps open.

David begins screaming and weeping in horror. At the entrance to the room, the puppet from the video appears on a tricycle. He congratulates David on surviving, and tells him that he has proven he is no longer ungrateful for being alive.
 CGI replica of the bathroom used later in the full Saw film is shown, although with some differences. A small peephole is shown with an eye visible behind it, which was a concept reused in another trap in Saw. 

==Cast==
*Leigh Whannell as David
*Paul Moder as Policeman
*Katrina Mathers as Nurse
*Dean Francis as Paralyzed Victim

==Production and legacy==
Prior to 2003, colleagues James Wan and Leigh Whannell had begun writing a script based for a horror film, citing inspiration from their dreams and fears. Upon completing the script, Leigh and James had wanted to select an excerpt from their script, later to be known as Saw, and film it to pitch their film to studios. With the help of Charlie Clouser, who had composed the score for the film, and a few stand-in actors, Leigh and James shot the film with relatively no budget. Leigh had decided to star in the film as well.   

During filming, it was said that the trap used on Whannell was revealed to be fully functional and very rusted, making it very dangerous. The version of the trap used in the full film was made safer and had artificial rust. Wan later revealed that the film was shot in just over eight days. Upon completing the film, the pair traveled to Lions Gate studios to screen the film. Lions Gate immediately accepted the deal and got approved for a 1.2 million USD budget to make their script into a full feature film. Instead of being paid in advance, Wan and Whannell agreed to take a percentage of the profits from the films release, which turned out to be much more than initially expected. 

This allowed for the creation of Saw, a film that was intended to be a Direct-to-DVD release that was later released into theaters worldwide after positive reaction at the Sundance Film Festival in January 2004. The film retained the scene from the short, which changed actors but kept the key parts of the scene the same. Leigh Whannell would also return to star in the full-length film, and in Saw III. 

After the release of the full-length Saw, the film was met with overwhelming success in the box office both domestically and internationally. The film ended up grossing 55 million dollars in America, and 48 million dollars in other countries, totaling over US$103 million worldwide. This was over 100 million dollars profit, over 80 times the production budget.    This green-lit the sequel Saw II, and later the rest of the Saw (franchise)|Saw franchise based on the yearly success of the previous installment. Since its inception, Saw has become the highest grossing horror franchise of all time worldwide in unadjusted dollars. In the United States only, Saw is the second highest grossing horror franchise, behind only Friday the 13th (franchise) by a margin of $10 million.    

==Soundtrack==
*"Hello Zepp", a tune that has become staple in other Saw (franchise)|Saw films, is played when credits are shown. It is the first appearance of this piece. 

*The same sound that is heard in the introduction of the title can also be heard in the song "Screaming Slave" by Nine Inch Nails. The composer of the soundtrack for the future Saw films, and for this film, Charlie Clouser, was a member of the band during live performances. However, he was not a part of the band during the period the song was recorded.

==References==
 

==External links==
 
*  
*   at  

 
 

 

 
 
 
 
 
 
 
 