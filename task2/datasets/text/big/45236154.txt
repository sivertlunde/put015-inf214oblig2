The Hunting Ground
 Hunting Ground}}
{{Infobox film
| name           = The Hunting Ground
| image          = File:The Hunting Ground POSTER.jpg|thumbnail|right|
| caption        = Theatrical release poster
| alt            =
| director       = Kirby Dick
| producer       = Amy Ziering
| writer         = Kirby Dick
| music          = Miriam Cutler
| cinematography = Aaron Kopp Thaddeus Wadleigh
| editing        = Douglas Blush Derek Boonstra Kim Roberts
| distributor    = The Weinstein Company|RADiUS-TWC
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $300,360   
}} rape on college campuses in the United States directed by Kirby Dick and produced by Amy Ziering. It premiered at the 2015 Sundance Film Festival. The film was released on 27 February 2015,    and was subsequently broadcast on CNN.    The documentary focuses on Andrea Pino and Annie E. Clark, two former University of North Carolina students who were raped on campus and led a campaign to file a Title IX complaint against University of North Carolina at Chapel Hill|UNC. Lady Gaga recorded "Till It Happens to You" for the film.   

==Background== sexual assault in the US military, inspired them to make a documentary about the subject of sexual assault at American colleges. 

==Content== college administrators who ignore them or make them jump through hoops because they are more concerned about keeping rape statistics low. 
 Harvard and the University of North Carolina,  but it also reports about fraternities such as Sigma Alpha Epsilon. 

A section of the film is focused on Jameis Winston (a quarterback for Florida State University) and the multiple accusations of sexual assault against him. His alleged victim Erica Kinsman publicly speaks about the incident at length for the first time. 

===Participants===
Sen. Kirsten Gillibrand (D-NY), makes a brief appearance in the film. 

==Soundtrack==
The film features two tracks by  "  and an original song written by Diane Warren and produced by Nile Rodgers  titled "Till It Happens to You". According to producer Amy Ziering, the filmmakers sought to "have someone of major importance contribute a song to the film, because we thought that that would help the cause and help publicity." Executive producer Paul Blavin met with music supervisor Bonnie Greenberg, who brought the project to Diane Warren, who brought the song to Lady Gaga. Two versions appear in the documentary: a piano version near the beginning during an interview, and another for the end credits. 

==Reception== Senator Barbara Boxer responded by saying "The power on that status quo side, youre going to see it in response to this film. Believe me, there will be fallout." 

==References==
 

==External links==
* 
* 
*  
* 
*  , from an episode of Real Time with Bill Maher
*  , a critical review by Emily Yoffe (27 February 2015) at Slate (magazine)|Slates DoubleX

 

 
 
 
 
 
 
 
 
 
 
 