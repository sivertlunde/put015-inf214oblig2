Five Cartridges
{{Infobox film
| name           = Fünf Patronenhülsen
| image          = Five cartridges.jpg
| image size     = 220px
| caption        = A 1960 poster of the film.
| director       = Frank Beyer
| producer       = Willi Teichmann
| writer         = Walter Gorrish
| narrator       =
| starring       = Erwin Geschonneck 
| music          = Joachim Werzlau
| cinematography = Günter Marczinkowsky
| editing        = Evelyn Carow
| studio         = DEFA
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = East Germany German
| budget         =
| gross          =
}}

Five Cartridges ( ) is a 1960 East German film directed by Frank Beyer and starring Erwin Geschonneck, Armin Mueller-Stahl and Manfred Krug.

==Plot==
During the Spanish Civil War, a battalion of the International Brigades is cut off without water or ammunition. The commander, Major Bolaños, requests his commissar, the German Heinrich Witting, to select five volunteers who will remain in the trenches and hold off the enemy, while the battalion retreats across the Ebro River. Witting chooses the Frenchman Pierre, the German Willi, the Pole Oleg, the Spaniard José and the Bulgarian Dimitri. In addition, the Soviet radio operator Vasia stays behind to handle communications. 
 Nationalists for several hours and then breaks out. Vasia disappears; while searching for him, Witting is spotted by the enemy and shot. Before he dies, he rips a piece of paper into five parts which he encapsulates in spent cartridges. He gives a cartridge to each of his five men and orders them to carry it back to the battalion, claiming it is an important message. It would be decipherable only if all the pieces would reach their destination. 

After finding Vasia, the volunteers make their way across the Sierra in the hot summer, and run out of water. All the wells in the area are guarded by the Nationalists. The groups members become desperate with thirst, and their attempts to get water are frustrated. They begin to quarrel among themselves. Vasia, mad with thirst, wanders into a village in search of water and is caught by the Guardia Civil. Before being executed, the others rescue him. Vasia then volunteers to hold off the pursuing Falangists; pretending to surrender, he approaches the enemy soldiers with a grenade in his hand and dies when it explodes. Afterwards, Pierre leaves his cover to try and drink from a well, only to be shot dead. 

The others, almost too dehydrated to move, cross the Ebro and rejoin the battalion. When the cartridges are unsealed, they learn that the message was: "Stay together, so you will survive".

==Cast==
*Fritz Diez as Major Bolaños
*Erwin Geschonneck as Heinrich Witting
*Ulrich Thein as Vasia
*Armin Mueller-Stahl as Pierre Gireau
*Manfred Krug as Oleg Zalevski
*Hans Finohr as Pedro
*Edwin Marian as José Martinez
*Ernst-Georg Schwill as Willi Seifert
*Günter Naumann as Dimitri Pandorov
*Johannes Maus as Karl
*Jochen Diestelmann as Jerri
*Harald Jopt as Sanchez
*Dom de Beern as Otto
*Fritz-Ernst Fechner as Jirka
*Hans-Hartmut Krüger as legionnaire 
*Fred Ludwig as sergeant
*Hans-Ulrich Lauffer as nationalist officer

==Production==
Director Frank Beyer told an interviewer that he was drawn to making Five Cartridges due to it having "three dimensions: a political subject, a thrilling story and a dialog-poor, mostly picturesque narrative." The film was one of the first to employ the technique of a storyboard, enabling Beyer to "plan all the necessary arrangements for a scene before its shooting began".  
 Ernst Busch. 

==Reception==    
 
Five Cartridges won Frank Beyer great acclaim.  Beyer, composer Joachim Werzlau, set designer Alfred Hirschmeier and cinematographer Günter Marczinkowsky were all awarded the Heinrich Greif Prize on 13 May 1961.  

Paul Cooke and Marc Silberman viewed the film as a classical anti-Fascist work, noting that it was rather a political statement than a war film.  Stefan Deines wrote that it was not surprising that Pierre - whose country, France, was the only one among those represented at Five Cartridges in which the communist party opted not to try and take power - was the one who left the group, and subsequently killed. They also noted other communist political influences on the plot, like when José chalks a message for the nationalist soldiers in which he asks them why they are fighting against their working-class brothers. In another scene, a Falanga officer tells his colleague, "If you could tell me how can the communists endure without water while we cannot, you will be promoted to General."     
 John Ford. 

==References==
 
 
==External links==
*  
*   at    

 

 
 
 
 
 
 
 
 