Chess Fever
{{Infobox film
| name           = Chess Fever
| image          = Chess Fever (1925).webm thumbtime=2
| image_size     = 230px
| caption        = film poster
| director       = Vsevolod Pudovkin Nikolai Shpikovsky
| producer       = 
| writer         = Nikolai Shpikovsky
| narrator       = 
| starring       = 
| music          = 
| cinematography = Anatoli Golovnya
| editing        = 
| distributor    = 
| studio         = Gorky Film Studio|Mezhrabpom-Russ
| released       = 21 December 1925
| runtime        = 18 minutes (400 meters)
| country        = Soviet Union Russian intertitles
| budget         = 
}}
 Soviet silent comedy film directed by Vsevolod Pudovkin and Nikolai Shpikovsky. Chess Fever is a comedy about the Moscow 1925 chess tournament, made by Pudovkin during the pause in the filming of Mechanics of the Brain.  The film combines acted parts with actual footage from the tournament.

==Plot==
The heros (Vladimir Fogel) preoccupation with chess leads to him missing his own wedding ceremony, but the marital peace is restored with the help of the World Chess Champion, José Raúl Capablanca.

==Cast==
* José Raúl Capablanca – the World Champion
* Vladimir Fogel – the boy
* Anna Zemtsova – the girl
* Natalya Glan
* Zakhar Darevsky Frank Marshall – himself (cameo)
* Richard Réti – himself (cameo)
* Frederick Yates – himself (cameo)
* Ernst Grünfeld – himself (cameo)
* Mikhail Zharov – house painter
* Anatoli Ktorov – tram passenger
* Yakov Protazanov – chemist
* Yuli Raizman – chemists assistant
* Ivan Koval-Samborsky – policeman		
* Konstantin Eggert
* Fyodor Otsep – game spectator (uncredited)
* Sergei Komarov – grandfather (uncredited)

==See also==
*The Three Million Trial

==Footnotes==
 

==References==
* .

==External links==
* 
*  
*  
*  

 

__NOTOC__

 
 
 
 
 
 
 
 


 