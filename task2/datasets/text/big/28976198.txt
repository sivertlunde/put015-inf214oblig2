Welcome to Kodaikanal
{{Infobox Film
| name           = Welcome to Kodaikanal
| image          = Welcome to Kodaikanal.jpg
| image_size     = 
| caption        = 
| director       = Anil Babu 
| producer       = Hameed
| writer         = Asha Mathew (story), Kaloor Dennis (screenplay)
| narrator       =  Saikumar
| music          = Rajamani
| cinematography = Ravi K. Chandran
| editing        = P. C. Mohanan
| distributor    = Pratheeksha Pictures
| released       = 1992
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Siddique and Saikumar in the lead roles. The movie was produced by Hameed under the banner of Gemi Movies and was distributed by Pratheeksha Pictures.

==Cast==
* Jagadish as James Kutty Siddique as Vinayachandran Saikumar as Biju Zainuddin as Hussain
* Mala Aravindan as Eradi
* Bobby Kottarakkara as Kunjachan
* KPAC Sunny as Police Officer Anusha  as Maya
* Sukumari as Elizabeth Samuel
* Kanakalatha
* Shweta Menon as Kavitha

==Crew==
* Cinematography: Ravi K. Chandran
* Editing: P. C. Mohanan
* Art: Boban Koodaram
* Makeup: C. V. Sudev
* Costumes: B. R. Nagaraj
* Choreography: Kumar
* Advertisement: Colonia
* Processing: Gemini Colour Lab
* PRO: Abraham Lincoln
* Production Controller: Alvin Antony
* Production executive: Sebastian
* Office: Shafi Kozhikode
* Outdoor: Jubilee Cini Unit
* Production Manager: James Antony
* Executive Producer: Faisal

==Songs==
Rajamani gave music to the lines written by Bichu Thirumala.

;Songs list
# Pathayoram: M. G. Sreekumar, Minmini
# Manjukuttikal: M. G. Sreekumar
# Swayam Marannuvo: M. G. Sreekumar, R. Usha
# Manjukuttikal: K. S. Chithra
# Swayam Marannuvo ( 

==Notes==
 

==External links==
* 
 

 
 
 


 