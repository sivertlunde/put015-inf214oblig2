T.G.I.S.
{{Infobox television
  | show_name = T.G.I.S. (Thank God Its Sabado)
  | image =  
  | caption = 1997 logo
  | genre = Teen drama
  | runtime = 1 hour and 30 minutes VIVA Television
  | developer = Kit Villanueva-Langit
  | director = Mark Reyes Dominic Zapata(Associate Director)
  | writer = 
  | executive_producer = Veronique del Rosario-Corpuz
  | producer = Leny C. Parto
  | creative_director = 
  | cinematography = Monino Duque Jay Linao
  | starring = Angelu de Leon Bobby Andrews Raven Villanueva Michael Flores Red Sternberg Mitzi Borromeo Onemig Bondoc Rica Peralejo Ciara Sotto Dingdong Dantes Antoinette Taus Sunshine Dizon Polo Ravales Kim delos Santos Dino Guevarra
  | theme_music_composer = 
  | composer = Paul Westerberg
  | opentheme = "Dyslexic Heart" by Paul Westerberg "Walking On Sunshine" by Katrina and the Waves
  | endtheme = 
  | country = Philippines
  | location = Manila, Philippines English
  | network = GMA Network
  | picture_format = 
  | camera = 
  | audio_format = 
  | first_aired =    
  | last_aired =     
  | num_episodes = n/a
  | related = Teen Gen
  | website = 
  }}
 Filipino youth-oriented VIVA Television. The title is derived from the popular expression "Thank Goodness Its Friday|TGIF."

==Premise==
The show revolves around a clique of friends living in a fictional village presumably located somewhere in Quezon City. They deal with the various episodes that usually come with adolescent life. Almost all characters in the series are part of the same continuity.

==Cast==
 

===First Batch (July 1995 – May 1997)===
* Bobby Andrews as Joaquin "Wacks" Torres III
* Angelu de Leon as Ma. Patrice "Peachy" Real
* Raven Villanueva as Cristina "Cris" De Guzman
* Mitzi Borromeo as Annabelle Morales
* Red Sternberg as Francisco Martin "Kiko" Arboleda De Dios
* Michael Flores as Miguel "Micky" Ledesma
* Onemig Bondoc as Jose Mari "JM" Rodriguez
* Rica Peralejo as Michelle "Mitch" Ferrer 
* Ciara Sotto as Regina "Rain" Abrera
* Jake Roxas as Noel Sta. Maria
* Bernadette Alysson as Beatrice "Bea" Santillan
* Maybelyn dela Cruz as Maruja
* Chico Ventoza as Gabby
* Lester Llansang as Casper
* Sabrina San Diego as Ashanti Mae
* Paola Ellano as Charlene "Charie" To
* Kate Autajay as Penelope "Lupe" Fuentes

===Second Batch (May 1997 – August 1999)===
* Dingdong Dantes as Iñaki Torres
* Antoinette Taus as Bianca de Jesus
* Polo Ravales as Inocencio "Ice" Martinez
* Ciara Sotto as Regina "Rain" Abrera
* Menchu Macapagal as Pia Lopez
* Kim Delos Santos as Tere Gonzaga
* Chubi del Rosario as Reyster
* Chantal Umali as Happy
* Ardie Aquino as Benjo 
* Idelle Martinez as Samantha Real
* Sunshine Dizon as Carla "Calai" Escalante
* Anne Curtis as Emily or "Em"
* Dino Guevarra as David
* Jam Melendez as Jag
* Kenneth Cajucom as Marci
* Maui Taylor as Bridget
* Aiza Marquez as Billie
* Jay Budol as Budang

Delos Santos, Umali, Dela Cruz, Llansang, and Martinez were part of the original cast, but had more screen time in the second batch. Members of both batches would intermittently appear in both TGIS and the first year of Growing Up. Also included in the original cast are Lee Robin Salazar & Bernadette Alyson, Lee Robin left the show after the groups first movie "Takot Ka Ba Sa Dilim?" was shown in cinemas & Bernadette a month after the shows launching, but Bernadette later rejoined them in Growing Up.

Curtis and Marquez would later form part of a third batch that were featured in the final few months of TGIS run from August to November 1999. Their batchs story is set in an alternate timeline, where Curtis character falls for Wacks. Sotto was included in the second batch after the original cast went to Growing Up. She would join them in 1998.

{{Infobox film
| name           = T.G.I.S. The Movie	
| producer       = Executive Producer: Vic del Rosario
| starring       = Bobby Andrews  Angelu de Leon  Raven Villanueva Red Sternberg Michael Flores Onemig Bondoc Rica Peralejo Ciara Sotto
| distributor    = VIVA Films GMA Network
| released       = January 4, 1997
| country        = Philippines
| language       = Filipino Tagalog}}

==TGIS The Movie==
TGIS The Movie is a film tie-in to the series. It was released in early January 1997 by VIVA Films & GMA Network.

===Plot===
Wacks plans a weekend get-away using his fathers yacht without him knowing. However, the boat breaks down, forcing the group to stay on a nearby island. The boat is not tightly anchored to the bottom; as a result it drifts away from the island with nobody on board. Left stranded, the gang try to survive as their loved ones search for them. Eager to find help, the guys move out using an improvised raft, but not before Wacks finally confesses his feelings for Peachy, who fell unconscious in a bat cave. The entire group is eventually rescued the following day.

==GMA Love Stories Reunion==
The original cast members - minus Angelu de Leon and Onemig Bondoc - returned for a special episode of GMA Love Stories that aired on February 17, 2001. The episode aims to show character developments after the Growing Up series finale in February 1999.

===Plot===
Most of the gangs members come together in a beach resort, where Kiko successfully proposes marriage to Mitch. When Wacks appears, everyone except Kiko greets him. The characters fates since Growing Up are explained over the course of the episode:

Wacks says his quest to go to the US to patch things up with Peachy after their aborted wedding has failed; she has since married someone else. Cris is a single mother. Mickey and Bea have been in an on-and-off relationship for years. Noticing that Kiko has been cold to him since they met again, Wacks learns that Kiko resented him for not even keeping in touch while he was in the US. They reconcile and later joins in the rehearsal for Kiko and Mitchs wedding.

==Awards==
* Winner, Best Youth-Oriented Show, PMPC Star Awards for Television 1997-1998
* Winner, Best Teen-Oriented Show, Catholic Mass Media Awards 1997
* Best TV Special Winner "TGI-Xmas" 1998 PMPC Star Awards for Television
* Bronze Winner, Crossword Puzzle OBB, 1997 Japan TV Festival Tokyo Japan
* Finalist, Best Drama Show, 1997 New York TV Festival

==See also==
* List of shows previously aired by GMA Network
* GMA Network Growing Up (The T.G.I.S. Primetime) Click
*  
*  
* Teen Gen

==External links==
*  
*   on Internet Movie Database

 

 
 
 
 
 
 
 