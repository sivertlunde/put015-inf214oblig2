The Forbidden Kingdom
 
{{Infobox film
| name = The Forbidden Kingdom
| image = ForbiddenKingdomPoster.jpg
| caption = Film poster
| director = Rob Minkoff
| producer = Casey Silver
| screenplay = John Fusco
| story = Wu Chengen
| starring = Jackie Chan Jet Li Collin Chou Liu Yifei Li Bingbing Michael Angarano
| music = David Buckley
| cinematography = Peter Pau
| editing = Eric Strand Casey Silver Productions Lionsgate Huayi Brothers
| distributor = The Weinstein Company Lionsgate (USA)
| released =  
| runtime = 104 minutes
| country = United States China Hong Kong United Kingdom Mandarin Cantonese
| budget = $55 million  
| gross = $127,906,624  
}} action sequences were choreographed by Yuen Woo-ping.
 United States USA and Hong Kong on 9 September 2008 and the United Kingdom on 17 November 2008.

==Plot summary== South Boston Tripitikas (Michael Monkey King (Jet Li) and celestial soldiers in the clouds. He visits a pawn shop in Chinatown to buy Wuxia DVDs and discovers a golden Jingu Bang|staff. On his way home, Jason is harassed by some hooligans, whose leader Lupo attempts to use him to help them rob the shop-owner Hop. Hop tries to fight the thieves with the staff, but is shot and wounded by Lupo. He tells Jason to deliver the staff to its rightful owner and Jason flees with the staff. He is cornered on the rooftop by the hooligans and almost shot too, but he is pulled off the roof by the staff and falls backwards onto the asphalt.
 ancient China refers to herself in the third person. She reveals that her family was murdered by the Jade Warlord, against whom she has therefore sworn revenge.
 Five Elements kung fu along the way. After crossing a desert, they encounter Ni-Chang and her henchmen and a battle ensues, in which Lu Yan is mortally wounded by Ni-Changs arrow. The protagonists take refuge in a monastery, where they learn that Lu is actually not an immortal as he failed the test to become one. Only the Jade Warlords elixir can save his life. In desperation, Jason goes to the Warlords palace alone to exchange the staff for the elixir.

In the palace, the Jade Warlord asks Jason to fight with Ni-Chang to the death, because he had promised to give the elixir to only one of them. Jason is defeated by Ni-Chang and the Warlord taunts him for his foolishness, and is about to decapitate him when the other protagonists and monks from the monastery arrive to join in the battle. Jason manages to grab the elixir and he tosses it to Lu Yan, who drinks it and recovers. The Silent Monk is wounded by the Jade Warlords guandao during the fight and he passes the staff to Jason, who uses it to smash the Monkey Kings statue. The Monkey King is freed and the Silent Monk is revealed to be actually one of the Monkeys clones. Lu Yan battles Ni-Chang and kills her by throwing her off the cliff hundreds of feet below. After another long battle between the Monkey King and the Jade Warlord, the Warlord is eventually stabbed by Jason and falls into a lava pit to his death. However, Golden Sparrow has been seriously injured by the Warlord and she dies in Jasons arms, thanking him in the first person before dying. By then, the Jade Emperor has returned from his meditation and he praises Jason for fulfilling the prophecy and allows him for one wish, which he asked is to return home.

Jason finds himself back in 21st century Boston after passing through a magical portal at the exact moment and location of his earlier fall. He defeats Lupo easily by using the kung fu moves he was taught and drives the other hooligans away. He alerts the police and calls an ambulance for Hop, who survives from the gunshot wound and brushes off Jasons concerns, claiming that he is immortal (hinting that he is actually Lu Yan; a fact which also would have been hinted by the name of the pawn shop as seen in the beginning: "Lu Yans Pawn Shop"). Before the film ends, Jason is delighted to see a girl who resembles Golden Sparrow and speaks to her briefly, before she heads back to her shop, called "Golden Sparrow Chinese Merchandise". The final scene shows Jason on a rooftop at night practicing his staffwork and continuing to hone his kung fu skills.

==Cast==
* Michael Angarano as Jason Xuanzang (fictional character)|Tripitikas, the Traveler    and the main protagonist Lu Yan, the Drunken Immortal/Hop, the pawn shop owner
* Jet Li as Sun Wukong (called) the Monkey King/The Silent Monk
* Collin Chou as The Jade Warlord,  the main antagonist
* Liu Yifei as Golden Sparrow/the Chinatown girl
* Li Bingbing as List of Baifa Monü Zhuan characters#Main characters|Ni-Chang, the White-Haired Witch/Assassin

==Production details==

===Pre-production===
 
While the character Sun Wukong came from Wu Chengens famous classical novel Journey to the West,  in an interview with Screen Power magazine, actor Collin Chou denied that the plotline would be related to the novel. The details of the plot were devised by screenwriter John Fusco along with actor Jet Li. Li explains,

 

In a behind the scenes article he wrote for Kung Fu Magazine, screenwriter John Fusco also stated he derived the surname for the Jason Tripitikas character from "the wandering monk, Xuanzang (fictional character)|Tripitaka, from Journey to the West". 

The Golden Sparrow character was inspired by Cheng Pei-peis character Golden Swallow from the  Shaw Brothers film Come Drink with Me.  Before trying to kill the Jade Warlord, Golden Sparrow refers to the 1966 film, telling him to "Come drink with" her.

===Production===
Production began in early 1 May 2007 in the area around the Gobi Desert in Mongolia.  Before filming began, the entire cast did a costume fitting and a script read through, certain dialogues were altered to suit the different actors English speaking abilities; this was due to the majority of the cast having English as their second language. Chan described the first day of shooting as "very relaxing" because the shots only required drama and walking, with no action.  When the two martial arts veterans (Chan and Li) did film action scenes together for the first time, they both expressed how easy it was to work with one another. Chan explained:

 

Filming finished on August 24, 2007,    and the film went into post-production on September 29, 2007.

==Soundtrack==
 

==Critical reception==
The response to The Forbidden Kingdom, by both critics and audiences, was positive. As of 1 May 2008, the review aggregator website Rotten Tomatoes reported that 65% of critics gave the film positive reviews, based on 121 reviews &mdash; with the consensus being "Great fight scenes, but too much Filler (media)|filler".  Metacritic reported the film had an average score of 57 out of 100, based on 26 reviews &mdash; indicating mixed or average reviews. 

The Chinese press, however, responded to the movie less positively. Perry Lam wrote in Muse (Hong Kong magazine)|Muse magazine, "As a Hollywood blockbuster, The Forbidden Kingdom offers no apologies for its American-Centrism. In fact, it wears it with pride like a badge of honor". 

==Home media==
The Forbidden Kingdom was released on DVD and Blu-ray Disc|Blu-ray 9 September 2008. It sold about 1,199,593 units which translated to revenue of $22,921,609, bringing its worldwide total to $151,758,670. 

It is sold on single disc and two-disc special editions. The single disc edition has no extras but contains widescreen and full screen presentations of the film. The special edition includes a commentary by director Rob Minkoff, deleted scenes with commentary, featurettes (The Kung Fu Dream Team, Dangerous Beauty, Discovering China, Filming in Chinawood, and Monkey King and the Eight Immortals), a "Previsualization Featurette" with commentary by writer Fusco and director Minkoff, and a blooper reel. In addition to these extras, the Blu-ray release contains a digital copy.

==Box office performance==
The Forbidden Kingdom grossed a total of $127,906,624 worldwide &mdash; $52,075,270 in the United States and $75,831,354
in other territories.  In its opening weekend in the United States and Canada, the film grossed $21,401,121 in 3,151 theaters, ranking No. 1 at the box office opening weekend and averaging $6,792 per theater.  

==See also==
*Jackie Chan filmography
*Jet Li filmography
*List of American films of 2008
*Jianghu

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 