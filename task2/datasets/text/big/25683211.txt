The Man from Cairo
  
{{Infobox film
| name           = The Man From Cairo 
| starring       = George Raft Gianna Maria Canale Massimo Serato Irene Papas director = Ray Enright
| image	=	The Man from Cairo FilmPoster.jpeg
| released       = 1953 
| country        = United Kingdom
| language       = English  budget = $235,000 Mark Thomas McGee, Talks Cheap, Actions Expensive: The Films of Robert L. Lippert, Bear Manor Media, 2014 p 155 
}}
 1953 cinema British film-noir starring George Raft, who plays a man that is mistaken for a detective by the French and is sent to find lost World War II gold in Algiers.

The 1953 picture was the last completed film in which Raft had top billing. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 156 
==Production==
The film was made for $155,000 with $80,000 in deferrals. Irene Papas made her American film debut. 
==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 