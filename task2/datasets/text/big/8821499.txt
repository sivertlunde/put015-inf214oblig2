Der Müller und sein Kind
{{Infobox film
| name           = Der Müller und sein Kind
| image          =
| caption        =
| director       = Walter Friedemann
| producer       = Österreichisch-Ungarische Kinoindustrie
| writer         =
| starring       = Max Bing
| cinematography = Joseph Delmont
| editing        =
| distributor    =
| released       =  
| runtime        = 22 minutes
| country        = Austria
| language       = Silent film
| budget         =
}}
Der Müller und sein Kind,  a silent film made in 1911, is the oldest Austrian drama film to survive in its entirety. It was produced by the Österreichisch-Ungarische Kinoindustrie, which later in 1911 changed its name to become the Wiener Kunstfilm-Industrie. The same company had filmed the same plot in the previous year, but of that version nothing survives.
 Romantic supernatural melodrama by Ernst Raupach, a very popular work, which was first performed to great acclaim in 1830 at the Burgtheater in Vienna, and thereafter was performed in many theatres at Halloween well into the 20th century.

The film had its premiere on 21 October 1911 in Vienna.
 35 mm cellulose nitrate film. The drama was filmed in 19 scenes, and at a projection speed of 16 frames per second, common in early silent films, lasted 21 minutes and 50 seconds. Although the Filmarchiv Austria, which preserves the film, has 150 metres less, the action of the film is nevertheless complete, although the ends of the individual reels are too abrupt, and the end of scene 19 is missing.

Technical direction was by Joseph Delmont, later a director of animal films and crime films, who among other things operated the camera by hand crank.

The cast consisted of Max Bing (Konrad), Fräulein Heller (Marie), Theodor Weiß (The Miller), Fritz Lunzer (landlord) and Herr Ludwig (priest).

== Plot ==
Konrad, a poor millers son, wants to marry Marie, a rich millers daughter, but her widowed father is malicious and avaricious, and tricks the lovers in a despicable way. The imminent demise of the miller and his innocent daughter is foreshadowed by the appearance of the bird of death and the graveyard ghost.

==Notes==
 

==External links==
* Walter Fritz|Fritz, Walter, 1997: Im Kino erlebe ich die Welt - 100 Jahre Kino und Film in Österreich. Vienna: Christian Brandstätter ISBN 3-85447-661-2
*  

 
 
 
 
 
 
 
 
 
 
 


 
 