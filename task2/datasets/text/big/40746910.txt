Alagwa (film)
 
{{Infobox film name           = Alagwa image          =  caption        = Official poster director       = Ian Loreños producer       = G. Lao Ian Loreños Jericho Rosales writer         = Ian Loreños cinematography =  editing        =  music          =  starring       = Jericho Rosales Bugoy Cariño studio         = Anakim Media Productions distributor    = Star Cinema released       =   runtime        = 88 minutes country        = Philippines language  Filipino  Tagalog English English
|budget         = gross          = PHP 1,896,577 
}}
Alagwa ( , a father struggling to search his son after disappearing in the city. The film is inspired by a popular urban legend about a childs disappearance in Chinatown Manila. The film uncovers the dark facets of the underground business of human trafficking in the Philippines.

The film had its world premiere at the 17th Busan International Film Festival in 2012, and have its limited screening in the Philippines during the CinemaOne Originals Film Festival 2012, last December. The film was released commercially by Star Cinema this October 9, 2013.

==Synopsis==
An impoverished single parent, Robert Lim (Jericho Rosales) spends his free time with his young son Brian (Bugoy Cariño), whom he was mad for being responsible of the death of his wife during childbirth. One day Robert went to a mall with Brian for some father and son bonding time, Brian goes to the bathroom then mysteriously disappears. Robert, worried about his sons disappearance, informs the police chief (Leo Martinez) that his son was abducted. Using evidence shown on camera disc recorded under surveillance, it shows a teenage kid talking to his son Brian. Enraged by this, Robert goes to a park in Quezon Avenue in search of the suspect and later witnesses him getting off a taxi. Robert enrages at the boy but he escapes. The next day, police recover a dead boys body with Robert thinking it was his son, but this turns out to be his sons kidnappers body. Robert, becoming more furious, attempts to hunt down the man down responsible for his sons disappearance. He then encounters a vigilante, and with his knowledge about the suspects of Brians kidnapping, pretends to help him by telling him that his son is now being transported to Hong-Kong. Robert soon discovers that Brians kidnapping is the result of human-trafficking. That night, Robert receives a call from the vigilante again telling him that he should meet him under an overpass, but soon gets ambushed by police with the vigilante dying on the scene. Several years passed, Robert has a new family and with his new son (John Manalo), travels to Hong Kong with him. On the streets, his son suddenly goes missing. Robert goes to search for him, feeling worried that the previous incident would occur to his new family. Robert searches for him in a subway and finds a homeless man playing a harmonica. Robert suddenly recognises the homeless mans harmonica, realising that Brian had exactly the same harmonica from his childhood. Robert also remembers Brian teaching him a secret handshake. Robert attempts to do the secret handshake with the homeless man, who knows the secret handshake as well. Realising that his long-lost son was finally found, Robert tearfully embraces him and the story concludes with his other son witnessing the scene.

==Cast==
*Jericho Rosales as Robert Lim
*Bugoy Cariño as Bryan Lim
*Leo Martinez
*Carmen Soo
*Smokey Manaloto

==Awards and nominations==
*2013 FAMAS Awards
** Nominated-Best Actor – Jericho Rosales
** Nominated-Best Child Actor – Bugoy Cariño
 2013 Gawad Urian Awards
** Winner-Best Actor – Jericho Rosales
** Nominated-Best Director – Ian Loreños
** Nominated-Best Editing – Dempster Samarista
** Nominated-Best Screenplay – Ian Loreños

*2013 Golden Screen Awards
** Nominated-Best Film (Drama)
** Nominated-Best Actor – Jericho Rosales
** Nominated-Best Story – Ian Loreños

*2013 Newport Beach Film Festival
** Winner-Outstanding Achievement in Acting - Jericho Rosales

*ASEAN Film Festival
** Winner-Best Supporting Actor - Bugoy Cariño

==References==
 

==External links==
*  

 
 
 