The Divorce of Lady X
 
 
{{Infobox film
| name           = The Divorce of Lady X
| image          = The-divorce-of-lady-x-1938.jpg
| image_size     = 
| caption        = Film poster
| director       = Tim Whelan
| writer         = Gilbert Wakefield (play) Lajos Bíró (adaptation) Ian Dalrymple (scenario)
| starring       = Laurence Olivier  Merle Oberon Binnie Barnes Ralph Richardson
| producer       = Alexander Korda
| music          = Miklós Rózsa   Lionel Salter
| cinematography = Harry Stradling
| editing        = L.J.W. Stokvis
| distributor    = United Artists
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = $500,000  or £99,000 
| gross          =

}}
 directed by Tim Whelan and produced by Alexander Korda from a screenplay by Ian Dalrymple and Arthur Wimperis, adapted by Lajos Bíró from the play Counsels Opinion by Gilbert Wakefield. The music score was by Miklós Rózsa and Lionel Salter and the cinematography by Harry Stradling.

The film was made in Technicolor and is a remake of the 1933 film Counsels Opinion, also made by London Films and in which Binnie Barnes appeared in the role played by Merle Oberon.

==Plot==
Leslie Steele (Merle Oberon), a guest at a costume party, is forced to stay overnight in a hotel because of a particularly bad London fog.  As there are no rooms available, Steele talks her way into sharing a suite with Everard Logan (Laurence Olivier), a handsome but somewhat stiff lawyer. They spend the night together, quite chastely, but Logan becomes convinced that Leslie must be married. His conviction is confirmed when an old school friend, Lord Mere (Ralph Richardson), arrives and asks Everard to represent him in a divorce case against his wife, Lady Claire (Binnie Barnes).

==Cast==
* Merle Oberon as Leslie Steele
* Laurence Olivier as Everard Logan
* Binnie Barnes as Lady Claire Mere
* Ralph Richardson as Lord Mere
* Morton Selten as Lord Steele
* Victor Rietti as Hotel Manager
* J.H. Roberts as Slade
* Gertrude Musgrove as Saunders, the Maid
* Gus McNaughton as Room Service Waiter
* H.B. Hallam as Jefferies, the Butler
* Eileen Peel as Mrs. Johnson
* Lewis Gilbert as Tom

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 