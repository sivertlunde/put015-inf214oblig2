Among Those Present
 
{{Infobox film
| name           = Among Those Present
| image size     = 190px
| image	         = Poster - Among Those Present (1921) 01.jpg
| caption        = theatrical poster for Among Those Present
| director       = Fred C. Newmeyer
| producer       = Hal Roach Sam Taylor H.M. Walker
| narrator       =
| starring       = Harold Lloyd
| music          =
| cinematography = Walter Lundin
| editing        = Thomas J. Crizer	
| studio         = Rolin Film Company
| distributor    = Pathé Exchange
| released       =  
| runtime        = 35 minutes
| country        = United States Silent English English intertitles
| budget         =
}}
 short comedy film starring Harold Lloyd and Mildred Davis.

==Cast==
* Harold Lloyd - The Boy
* Mildred Davis - The Girl
* James T. Kelley - Mr. OBrien, the Father
* Aggie Herring - Mrs. OBrien, the Mother
* Vera White - Society Pilot
* William Gillespie - Hard Boiled Party

==Plot==
Mrs. OBrien (Herring) is eager to be accepted as part of high society, and she is hosting a fox hunt as part of her plans. Her husband and daughter, though, have no interest in society affairs. 

Mrs. OBrien wants to invite Lord Abernathy to the hunt, and she mentions this to the "society pilot" who is advising her. But this woman and a confederate are merely using Mrs. OBrien and the hunt for their own purposes. When Lord Abernathy is unavailable, they convince an ambitious young man (Lloyd) to impersonate him, so that they can proceed with their scheme.

==See also==
* Harold Lloyd filmography

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 


 