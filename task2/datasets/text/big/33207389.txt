Attorney for the Defense
 
{{Infobox film
| name           = Attorney for the Defense
| image          =
| caption        =
| director       = Irving Cummings
| producer       = Harry Cohn
| writer         = James Kevin McGuinness Jo Swerling
| starring       = Evelyn Brent
| cinematography = Ted Tetzlaff
| editing        = Gene Havlick
| distributor    = Columbia Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Attorney for the Defense is a 1932 American drama film directed by Irving Cummings and starring Evelyn Brent.   

==Cast==
* Edmund Lowe as William J. Burton
* Evelyn Brent as Val Lorraine
* Constance Cummings as Ruth Barry
* Don Dillaway as Paul Wallace Douglas Haig as Paul Wallace as a Boy
* Dorothy Peterson as Mrs. Wallace
* Bradley Page as Nick Quinn aka Kramer
* Nat Pendleton as Mugg Malone
* Dwight Frye as James Wallace
* Wallis Clark as District Attorney James A. Crowell
* Clarence Muse as Jefferson Q. Leffingwell

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 