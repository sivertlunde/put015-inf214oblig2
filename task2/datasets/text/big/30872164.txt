Naruto the Movie: Ninja Clash in the Land of Snow
{{Infobox film
| film name = {{Film name| kanji          = 劇場版 NARUTO 大活劇! 雪姫忍法帖だってばよ!!
| romaji         = Gekijōban Naruto Daikatsugeki! Yukihime Ninpōchō dattebayo!!}}
| name           = Naruto the Movie: Ninja Clash in the Land of Snow
| image          = Naruto the Movie - Ninja Clash in the Land of Snow.jpg
| caption        = DVD cover
| director       = Tensai Okamura
| producer       = 
| screenplay = Katsuyuki Sumisawa
| story       =Masashi Kishimoto
| starring       = Junko Takeuchi Noriaki Sugiyama Chie Nakamura Kazuhiko Inoue Yuhko Kaida Hidehiko Ishizuka Toshio Masuda
| cinematography =  Studio Pierrot
| editing        = 
| distributor    = Toho (Japan) Viz Media (USA)
| released       =  
| runtime        = 82 minutes
| country        = Japan
| language       = Japanese
| gross          = ¥1,370,000,000
$11,141,307
}}
  is a 2004  . The movie takes place after episode 101. The ending song is called Home Sweet Home and is sung by Yuki Isoya|Yuki. The English adaptation replaced this song with "Never Give Up" by Jeremy Sweet due to licensing restrictions.

An original video animation,   (木ノ葉運動会 Konoha Undōkai), was included with the Japanese release of the film. The ten-minute opening short revolves around Narutos inability to use the bathroom while participating in a sports tournament. It is notable because virtually every character, living or dead, in the Naruto universe makes an appearance in it (most of them standing in a restroom-queue). Minato Namikaze also makes an appearance in the shot.

==Plot== Chakra armor, attempt to capture the princess, but Team 7 manages to delay the princess from being captured. Doto eventually captures Koyuki and kills her bodyguards, who reveal themselves in an attempt to protect and restore her rightful place as the ruler of the Land of Snow. Naruto enters the ship to rescue the princess, only to be captured and forced to wear a chakra-draining device. Doto forces Koyuki to hand over a crystal necklace her father had given her when she was young, thinking that this is a key that can unlock the hidden treasure Sosetsu had left behind before detaining both Koyuki and Naruto.
 Chidori to weaken Dotos armour. Soon after, Naruto releases his energy and uses his  new Seven Colored Chakra Jutsu (Naruto)#Rasengan|Rasengan, killing Doto and activating a mirror to transform the land. At the end of the film, Koyuki decides to reclaim her rightful place of royalty and even mentions personally with Team 7 that even in the life of royalty, her acting career will not end.

==Cast==
{| class="wikitable"
|-
! Role
! Japanese actor
! American actor
|-
|-
| Naruto Uzumaki || Junko Takeuchi || Maile Flanagan
|-
| Sasuke Uchiha || Noriaki Sugiyama || Yuri Lowenthal
|-
| Sakura Haruno || Chie Nakamura || Kate Higgins
|-
| Kakashi Hatake || Kazuhiko Inoue || Dave Wittenberg
|-
| Soutetsu Kazahana || Hidehiko Ishizuka || Cam Clarke
|-
| Koyuki Kazahana/Yukie Fujikaze/Princess Fuun (Princess Gale in the English dub) || Yuhko Kaida || Kari Wahlgren
|-
| Nadare Rouga || Hirotaka Suzuoki || Liam OBrien
|-
| Sandayu Asama || Ikuo Nishikawa || Daran Norris
|-
| Fubuki Kakuyoku || Jun Karasawa || Cindy Robinson
|-
| Mizore Fuyukuma || Holly Kaneko || Kyle Hebert
|-
| Doto Kazahana || Tsutomu Isobe || Lex Lang
|-
| Director Makino || Chikao Ōtsuka || Michael McConnohie
|-
| Assistant Director || Akimitsu Takase || Sam Riegel
|-
| Omiji/Tsukekuro ||  || Crispin Freeman
|- Doug Stone
|-
| Hidero|| Kan Tanaka  || Jamieson Price
|}

==Releases==
The film was originally released in Japan on August 21, 2004. It was later released on DVD on April 28, 2005. The film was released in 160 theaters in the United States as a one-day showing on June 6, 2007.   Fifty threaters in Canada would see a one-day showing on June 23, 2007.  Madman Entertainment did a special one-day theater release in Australia on October 14, 2007.  In 2007, the film was shown at the Fantasia Festival and in the British Museum.  

The DVD release debuted at rank 25 on Nielsen Videoscan. 

Instead of the OVA included with the Japanese release, the American release included a short featurette entitled "World of Naruto", as well as a behind the scenes featurette afterwards with interviews with the main English cast and select members of the main Japanese cast. The OVA will, however, appear in the North American DVD release. The DVD was released on September 4, 2007. The film premiered on Cartoon Network on September 8, 2007. The film also played at Cineplex Odeon and Empire Theatres cinemas in Canada, distributed via Bell TV to play the film at all cinemas at the same time.

On November 13, 2007, a three-disc Deluxe Edition of the film was released. It has many extras and features that the standard DVD, released a few months earlier, did not include. It includes the ten-minute short "Konoha Annual Sports Festival" that was originally shown with the Japanese release of the film, the complete soundtrack to the movie, documentaries of the American voice recording of the movie, movie art postcards, and more.

== Reception ==
Helen McCarthy in 500 Essential Anime Movies calls it "a good introduction to the main themes of the series, with ideas of persistence, determination, believing in yourself, and never giving up", and praises setting and backgrounds, "with the frozen Northern landscapes looking especially good".   

==References==
 

==Further reading==
*  

==External links==
*  
*  
* http://narutoshippudenninjastorm.com/ "Naruto Shippuden Ninja Storm"

 
 

 
 
 
 
 
 