Bandit Queen
{{Infobox film
| name = Bandit Queen
| image = Bandit Queen 1994 film poster.jpg
| caption = Film poster
| writer = Ranjit Kapoor (dialogue) Mala Sen
| starring = Seema Biswas
| director = Shekhar Kapur
| producer = Bobby Bedi
| cinematography= Ashok Mehta
| editing = Renu Saluja
| distributor = Koch Vision, USA 2004 (DVD)
| released = 9 September 1994
| runtime = 119 minutes
| country = India
| language = Hindi
| music = Nusrat Fateh Ali Khan  Roger White
| awards =National Film Award for Best Feature Film in Hindi 
| budget =  studio = Kaleidoscope Entertainment, Channel4}} Ustad Nusrat Fateh Ali Khan. The film won the National Film Award for Best Feature Film in Hindi, Filmfare Critics Award for Best Movie and Best Direction for that year. The film was premiered in the Directors Fortnight section of the 1994 Cannes Film Festival, and was screened at the Edinburgh Film Festival.  

== Plot ==
Bhind, 1968  Phoolan is married to a twenty-something fellow called Puttilal (Aditya Shrivastava). Though child marriages are customary during that time, Phoolans mother Moola (Savitri Raekwar) objects to the timing of the match. Phoolans aging father Devideen (Ram Charan Nirmalker) conforming to his culture, regrettably disagrees, and Phoolan is sent off with Puttilal.
 Mallah sub Thakur men (whose parents make up the panchayat or village government). At the next town meeting, the panchayat wield their patriarchal authority to banish Phoolan from the village, since she will not consent to the sexual advances of the higher caste males, who treat her like sub-human chattel.

Accordingly, Phoolan leaves with her cousin Kailash (Saurabh Shukla). En route to another village, she encounters a troop of Dacoity|dakus (bandits) of the Babu Gujjar gang, led by Vikram Mallah Mastana (Nirmal Pandey). Phoolan stays with Kailash for a while, but is eventually compelled to leave. Angry and hopeless, Phoolan goes to the local police (to try to have her ban lifted), but she is beaten, molested, and arrested by the police, who rape her while in custody. The Thakurs put up bail and have her released. But, unknown to her, the bail is a bribe (paid, through the police, to Babu Gujjars gang), and Babu Gujjar arrives to collect his prize.
 Anirudh Agarwal). Gujjar is a ruthless, imposingly-statured mercenary. Although Gujjars lieutenant Vikram is sympathetic towards Phoolan, Gujjar indiscriminately brutalizes and humiliates her, until one day Vikram catches him raping her (yet again) and shoots him in the head. Vikram takes over the gang, and his empathy for Phoolan eventually grows into a mutually respectful mature adult relationship. Around this time, Phoolan revisits her former husband Puttilal, and with Vikrams help, abducts him and exacts her justice for his rape and abuse, beating him up. She shares her closure with Vikram.

All goes well until Thakur Shri Ram (Govind Namdeo) is released from prison. Thakur Shri Ram is the real gang leader (boss of the erstwhile Gujjar). Shri Ram returns to his gang and while Vikram receives him with respect, Shri Ram bristles at Vikrams egalitarian leadership style and covets Phoolan. In August 1980, Shri Ram arranges to have Vikram assassinated, and abducts Phoolan, bringing her to the village of Behmai. Phoolan is repeatedly raped and beaten by Shri Ram and by the rest of the gang members, as punishment for her "disrespect" for his previous advances, and for her audacity at being an equal. The stunning and disturbing final humiliation and punishment is that she is stripped, paraded around Behmai, beaten and sent to fetch water from the well (in full view of the village).

A severely traumatised Phoolan returns to her cousin Kailash. She recovers gradually, and seeks out Man Singh (Manoj Bajpai), an old friend of Vikram Mallah. Man Singh brings her to another large gang, led by Baba Mustakim (Rajesh Vivek). She relates her history to Baba and asks him for some men and weapons to form a gang. Baba Mustakim agrees, and Man Singh and Phoolan become the leaders for the new gang.

Phoolan leads her new gang with courage, generosity, humility and shrewdness. Her stockpile and her legend grows. She becomes known as Phoolan Devi, the bandit queen. In February 1981, Baba Mustakim informs her of a large wedding in Behmai, with Thakur Shri Ram in attendance. As Phoolan departs, Baba Mustakim warns her to remain low key. Phoolan attacks the wedding party and her gang exacts revenge from the entire Thakur clan of Behmai. They round up the men and beat them up. Many of the men are finally shot. This act of vengeance brings her to the attention of the national law enforcement authorities (in New Delhi). The top police officials now begin a massive manhunt for Phoolan, and Thakur Shri Ram relishes the opportunity to come to their aid.

The manhunt claims many lives in Phoolans gang. They are ultimately forced to hide out in the rugged ravines of Chambal without any food or water. Phoolan evaluates her options and decides to surrender. Her terms are to have her remaining mates protected and provided for (the women and children in particular). The film ends with Phoolans surrender in February 1983. The end credits indicate that all the charges against her were withdrawn (including the charges of murder at Behmai), and that she was released in 1994.

==Cast==
* Seema Biswas --  Phoolan Devi
* Nirmal Pandey—Vikram Mallah
* Aditya Shrivastava—Puttilal
* Ram Charan Nirmalker—Devideen
* Savitri Raekwar—Moola
* Saurabh Shukla—Kailash
* Manoj Bajpai—Man Singh
* Raghuvir Yadav—Madho
* Rajesh Vivek—Baba Mustakim Anirudh Agarwal—Babu Gujjar
* Govind Namdeo—Thakur Shri Ram
* Shekhar Kapur—As a lorry driver in a cameo role

==Awards==
* 1995: Filmfare Award Critics Award for Best Movie National Film Award Best Feature Film in Hindi
**  
**  
* 1997: Filmfare Award
**  
**  
** Filmfare Best Debutant Female: Seema Biswas

==Criticism==
* The film is highly criticized by Arundhati Roy in her film review entitled "The Great Indian Rape-Trick".  
* Karen Gabriel, "Reading Rape: Sexual Difference, Representational Excess and Narrative Containment", in Manju Jain (ed.), Narratives of Indian Cinema, New Delhi: Primus, 2009.  
* See also Karen Gabriel, Melodrama and the Nation: The Sexual Economies of Mainstream Bombay Cinema (1970-2000),  New Delhi: Kali/ Women Unlimited, 2010.

==Further reading==
* Mala Sen, Indias Bandit Queen: The True Story of Phoolan Devi, London: Pandora/HarperCollins, 1993. ISBN 0-04-440888-9.

==References==
 

== External links ==
* 
* {{cite book
 | date  = 3 June 2010
 | title = Outlaw: Indias Bandit Queen and Me
 | last = Moxham
 | first = Roy Rider
 | isbn = 978-1-84604-182-2
 }}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 