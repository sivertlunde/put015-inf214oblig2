Hum Se Hai Zamana
{{Infobox film
| name = Hum Se Hai Zamana
| image = HumSeHaiZamana83.jpg
| caption = Promotional Poster
| director = Deepak Bahry
| producer = Deepak Bahry
| writer = 
| dialogue = 
| starring = Mithun Chakraborty Zeenat Aman Kajal Kiran Amjad Khan Danny Denzongpa Ranjeet Bob Christo
| music = Ram Laxman
| released =  
| runtime = 125 minutes
| country = India
| language = Hindi Rs 1.5 crores
}}
Hum Se Hai Zamana is a 1983 Hindi language|Hindi-language Indian film directed  by Deepak Bahry, starring Mithun Chakraborty, Zeenat Aman, Kajal Kiran, Amjad Khan, Danny Denzongpa, Ranjeet and Bob Christo

==Plot==
Hum Se Hai Zamana is an action film starring Mithun Chakraborty in the lead role, well supported by Zeenat Aman, Kajal Kiran, Amjad Khan, Danny Denzongpa and Ranjeet.
Tired of his older wife and son, the Thakur remarries a much younger woman, and throws his wife and son out of his palatial house. His employees, led by Kalicharan, go on a strike, demanding higher pay and benefits, which are unacceptable to the Thakur. He invites Kalicharan to meet with him, offers him money, and when Kalicharan refuses, he has him killed. His second wife gives birth to a baby girl, Nisha, and passes away. Years later, his past comes to haunt him when the sons of Kalicharan, Shiva and Karan, along with their widowed mother seek revenge against him. To make matters worse, Nisha refuses to marry Ranjeet Ranvir Singh, the man Thakur has chosen for her, for she loves Shiva; and the ultimate showdown between the Thakur and his now grown son, Iqbal, and his first wife. The question remains, will the Thakur use force to do away with his opponents and have them killed, as he has done in the past, or will he adopt other more subtle methods

==Cast==
*Mithun Chakraborty
*Zeenat Aman
*Kajal Kiran
*Amjad Khan
*Danny Denzongpa
*Ranjeet
*Major Anand
*Beena Bawa
*Asha Lata
*Bob Christo
*Pinchoo Kapoor
*Viju Khote
*Tej Sapru
*Sharat Saxena
*Nisha Thakur

==External links==
*  
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Humse+Hai+Zamana

 
 
 
 

 