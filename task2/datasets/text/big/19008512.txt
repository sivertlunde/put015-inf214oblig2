Late August, Early September
{{Infobox film
| name           = Late August, Early September
| image          = Late August, Early September.jpg
| caption        = Theatrical release poster
| director       = Olivier Assayas
| producer       = Georges Benayoun Philippe Carcassonne Françoise Guglielmi
| writer         = Olivier Assayas
| starring       = Mathieu Amalric
| music          = 
| cinematography = Denis Lenoir
| editing        = Luc Barnier
| distributor    = 
| released       = 14 September 1998
| runtime        = 112 minutes
| country        = France
| language       = French
| budget         = 
}}

Late August, Early September ( ) is a 1998 French drama film directed by Olivier Assayas and starring Mathieu Amalric.    

==Cast==
* Mathieu Amalric as Gabriel
* Virginie Ledoyen as Anne
* François Cluzet as Adrien
* Jeanne Balibar as Jenny
* Alex Descas as Jérémie
* Arsinée Khanjian as Lucie
* Mia Hansen-Løve as Véra
* Nathalie Richard as Maryelle
* Éric Elmosnino as Thomas
* Olivier Cruveiller as Axel
* Jean-Baptiste Malartre as Editeur
* André Marcon as Hattou
* Elisabeth Mazev as Visiteuse de lappartement
* Olivier Py as Visiteur de lappartement
* Jean-Baptiste Montagut as Joseph Costa

==References==
 

==External links==
* 
*  at Zeitgeist Films

 

 
 
 
 
 
 
 