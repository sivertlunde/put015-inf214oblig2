Rocky Mountain (film)
{{Infobox film
| name           = Rocky Mountain
| image          = Rockymountain1950.jpg
| image_size     = 220px Theatrical release poster
| director       = William Keighley William Jacobs
| writer         = Winston Miller based on = story Ghost Mountain by Alan Le May
| narrator       =
| starring       = Errol Flynn
| music          = Max Steiner Ted McCord
| editing        = Rudi Fehr
| distributor    = Warner Bros.
| released       =  
| runtime        = 83 minutes
| country        = United States English
| budget         =
| gross          = £125,231 (UK) 1,459,012 admissions (France) 
}}

Rocky Mountain is a 1950  . Retrieved: January 26, 2015.  

==Plot==
A car pulls up to an historical marker in the desert that reads: Confederate cavalry crossed the state line into California under secret orders from General|Gen. Robert E. Lee to rendezvous at Ghost Mountain with one Cole Smith, with instructions to place the flag atop the mountain, and though their mission failed, the heroism displayed by these gallant men honored the cause for which they fought so valiantly. 

In 1865m eight horsemen trek across the California desert, arriving at Ghost Mountain. Led by Captain Lafe Barstow (Errol Flynn) of the Mississippi Mounted Rifles. The eight soldiers encounter a man who calls himself California Beal (Howard Petrie). As a last desperate effort to turn the tide of the war, Barstows mission is to persuade Cole Smith and his 500 men to raid California on behalf of the Confederacy. From their vantage point on the mountain, the men see a Shoshone war party attack a stagecoach. Barstows men charge and drive off the Shoshone after the stage overturns, rescuing driver Gil Craigie (Chubby Johnson ) and the only surviving passenger, Johanna Carter (Patrice Wymore), traveling to join her fiancé, Union Army officer Lt. Rickey (Scott Forbes).
 Dickie Jones) is on watch, the Indians try to escape. The soldiers kill two of them, but Man Dog evades their bullets.

In the morning, Rickey suggests that he take Johanna to a nearby garrison before the Indians arrive. Barstow, however, hopes that Smiths men will come before the Indians do and rejects the suggestion. Near dawn, Rickeys men jump their guards.  One dies in the attempt, and another recaptured, but Rickey makes his escape. The Southerners find a riderless horse but it turns out to be Smiths, not Rickeys, and they realize that help is not coming.
 rebel flag on top of Rocky Mountain to salute the bravery of their fallen foes.

==Cast==
 
* Errol Flynn as Capt. Lafe Barstow 
* Patrice Wymore as Johanna Carter
* Scott Forbes as Lt. Rickey 
* Guinn "Big Boy" Williams as "Pap" Dennison  (credited as Guinn Williams) Dickie Jones as Jimmy Wheat  (credited as Dick Jones)
* Howard Petrie as Cole Smith "California Beal"
* Slim Pickens as Plank 
* Chubby Johnson as Gil Craigie
* Robert "Buzz" Henry as Kip Waterson  (credited as Buzz Henry)
* Sheb Wooley as Kay Rawlins
* Peter Coe as Pierre Duchesne 
* "Rush" Williams as Jonas Weatherby
* Harry Lauter as Cutting, Generator Technician
 

==Production==
The film was originally titled Ghost Mountain, the title of the original story by Alan Le May. It was purchased by Warner Bros in November 1948 for $35,000, with William Jacobs assigned to produce. Ronald Reagan badly wanted to play the lead and was announced as a potential star.  Errol Flynn, who had appeared in a number of successful Westerns for Warners was always named as a possibility.  

By early 1950 the project had been retitled Rocky Mountain and Flynn was given the lead role over Reagan, to the latters annoyance, as he felt he had brought the story to the studio. Reagan left Warner Bros soon afterwards, from a memo from Mr. Obringer to Jack Warner, dated February 17, 1950.  (Flynn had meant to do another Western, Carson City but it was felt that script was not as ready. "Prison camp story stars Claudette."
Los Angeles Times, April 21, 1950, p. B7. ) Winston Miller was bought in to write the script. 

Lauren Bacall was assigned to play the female lead under her contract with Warner Bros but turned it down and was suspended as a result.  Bacall said at the time:
 I turned it down because its just not a part. Its kind of nothing. Im not angry, Im just incredulous. Ive never been offered a role like this before. I dont want to do it and they shouldnt have to pay me. I shouldnt imagine theyll have any trouble replacing me.  
She was replaced by Patrice Wymore.  By July, Bacall would terminate her contract with Warners. 

Filming started 29 May 1950. During filming on location in Gallup, New Mexico, Flynn fell in love with Wymore. Thomas et al. 1969, pp. 168-169.  Their engagement was announced in August and they later married. 

==Reception==
Rocky Mountain earned £125,231 at the English box office. 
===Critical reaction===
The Washington Post called Rocky Mountain a "good and interestingly made picture". 
===Legacy===
According to James Garner, the script for Rocky Mountain was used as the basis for the first episode of the TV series Maverick (TV series)|Maverick.  The film was the also used as the basis for the first episode of the television series Cheyenne (1955 TV series)|Cheyenne (1955) entitled “Mountain Fortress" on September 20, 1955.

==References==
Notes
 

Bibliography
 
* Behlmer, Rudy. Inside Warner Brothers, 1935-51.  London: Weidenfeld & Nicolson, 1987. ISBN 978-0-2977-9242-0.
* Thomas, Tony, Rudy Behlmer and Clifford McCarty. The Films of Errol Flynn. New York: Citadel Press, 1969. ISBN 978-0-80650-237-3.
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 