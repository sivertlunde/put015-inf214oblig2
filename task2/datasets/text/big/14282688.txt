Beyond the Horizon (film)
{{Infobox film
| name           = Beyond the Horizon (Moe Goke Set Wyne Ko Kyaw Lun Yeuh)
| image          = 
| image_size     = 
| caption        =  San Shwe Maung
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Naing Nu Shain
| editing        = 
| distributor    = 
| released       =  
| runtime        = 145 minutes
| country        = Myanmar Burmese
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Burmese drama San Shwe Maung. In this film, Naing Nu Shain (aka) Pha Hti got academy awards for Best Cinematography.   

==Plot==
The film tells the story of a man whose dreams are premonitions of the future.

The protagonist is psychologically disturbed by recurring nightmares. Something is adrift in a river inlet, drawing ever closer to him. To seek solace he approaches a psychiatrist and for a short while is freed from nightmares till he meets a woman and falls in love.

==Cast==
*Lwin Moe
*Yan Kyaw
*Tun Eaindra Bo
*Khine Thin Kyi.

==International showing==
In 2005, this film was released in Myanmar. 
In August 7–13, 2006, this film was screened at the ASEAN Film Festival in Singapore. 

==References==
 

 
 
 
 
 

 