Big City Blues (1932 film)
{{Infobox film
| name           = Big City Blues
| image          = Big City Blues (1932).jpg
| caption        = Theatrical Poster
| writer         = Play:  
| starring       = Joan Blondell Eric Linden Jobyna Howland
| director       = Mervyn LeRoy
| producer       =
| music          = Ray Heindorf Bernhard Kaun
| cinematography = James Van Trees
| editing        = Ray Curtiss
| distributor    = Warner Bros.
| released       = September 10, 1932
| runtime        = 63 minutes
| country        = United States English
| budget         =
|}}
Big City Blues is a 1932 Warner Bros. drama film directed by Mervyn LeRoy.  The film is based on the play New York Town by Ward Morehouse and stars Joan Blondell and Eric Linden, with an uncredited early appearance by Humphrey Bogart.

==Plot==
Bud Reeves is an innocent young man who once lived in a small town. After inheriting money, he moves to New York City where he meets his cousin Gibby. He introduces him to chorus girl Vida Fleet, who he falls in love with. Troubles start to come when they decide to throw a party where a woman is killed after accidentally being hit on her head. Bud and Vita try to escape from the police and after arresting everybody, the real killer is soon revealed. Bud immediately goes back to his home in Indiana, but wants to go back to New York to marry Vida.

==Cast==
*Joan Blondell as Vida Fleet
*Eric Linden as Bud Reeves
*Jobyna Howland as Mrs Serena Cartlich
*Ned Sparks as Mr Stacky Stackhouse
*Guy Kibbee as Hummell, The House Detective Grant Mitchell as Station Agent
*Walter Catlett as Cousin Gibby Gibboney
*Inez Courtney as Faun
*Thomas E. Jackson as Detective Quelkin
*Humphrey Bogart as Shep Adkins (uncredited)
*Wallis Clark as Chief of Police (uncredited)
*Josephine Dunn as Jackie DeVoe
*Selmer Jackson as Joe (uncredited)
*Evalyn Knapp as Jo-Jo (uncredited)
*Clarence Muse as Nightclub Singer (uncredited)
*J. Carroll Naish as Bootlegger (uncredited)
*Dick Powell as Radio Announcer (voice, uncredited)
*Lyle Talbot as Len Lenny Sully (uncredited)

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 

 