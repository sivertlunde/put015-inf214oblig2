The Telephone Game
 

{{Infobox film
| name = The Telephone Game
| director = Jason Schumacher
| producer = Jason Schumacher
| story = Jason Schumacher
| based on =  
| cinematography = Kipp Zavada
| music = Jesse LaVercombe
| released =  
}} high definition black and white was shot without a script. All the dialogue was improvised by the ensemble cast.

== Plot ==
Marco, an out of town playwright and director, casts newcomer Zelphia in his latest play.  Marco, much to the producers dismay, Marco then casts himself as the lead in his own play, opposite Zelphia with whom he begins to develop a relationship, that is quick to be under scrutiny by the rest of the cast.  Irene, the theaters owner, and Buzz the flamboyant stage manager due their best to keep things together, as the understudies try to usurp the lead parts, the supporting casts grows restless in their thankless roles, the choreographer grows frustrated as her hard work goes unnoticed by Marco.

== Cast ==
* Wes Tank as Marco DeGarr
* Haley Chamberlain as Zelphia Anzhelina
* Alex Barbatsis as Benjo Oliverio
* Alisa Mattson as Lizbeth Brom
* Jesse Frankson as Buzz Dinwitty
* Andrea White as Madeline Darling
* KariAnn Craig as Irene Ilsely
* Cynthia Kmack as Dorthey Dashwood
* Lisa Pechmiller as Jennifer Thrones
* Eddie Chamberlain II as Chip Avondale
* Heather Amos as Audrey Reinhart
* Rachel Grubb as Olivia Donnell
* April Haven as Marietta Gotzone
* Yui Kanzawa as Mya Iolana
* Josh LeSuer as Antoine Van Slyke
* Brooke Lemke as Lucinda the Gypsy Girl
* Steve Schmalz as Nicholas Ilsely
* Jesse LaVercombe as Jo-Jo the Beatnik

== Reviews ==

 

== External links ==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 


 