The Raid (1954 film)
{{Infobox film
| name           = The Raid
| caption        =
| image	         = The Raid FilmPoster.jpeg
| director       = Hugo Fregonese
| producer       = Robert L. Jacks
| based on       =   Francis Cockrell (Story)
| screenplay     = Sydney Boehm
| starring       = Van Heflin Anne Bancroft Richard Boone Lee Marvin
| music          = Roy Webb
| cinematography = Lucien Ballard
| editing        = Robert Golden
| studio         = Panoramic Productions Twentieth Century-Fox
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $650,000 
}}
 William Tecumseh burning of Atlanta.

==Plot==
In 1864 a group of Confederate prisoners held in a Union prison stockade at Plattsburgh (city), New York|Plattsburg, New York, not many miles from the Canadian border, escape. They head for Canada and plan a raid across the border into St. Albans (town), Vermont|St. Albans, Vermont, to rob its banks and burn buildings.

The leader of the raid heads into St. Albans as a spy, and develops ambiguous feelings about what he is doing when he becomes friends with a young widow and her son.

==Cast==
* Van Heflin as Maj. Neal Benton / Neal Swayze
* Anne Bancroft as Katie Bishop
* Richard Boone as Capt. Lionel Foster
* Lee Marvin as Lt. Keating
* Tommy Rettig as Larry Bishop
* Peter Graves as Capt. Frank Dwyer
* Douglas Spencer as Rev. Lucas
* Paul Cavanagh as Col. Tucker Will Wright as Josiah Anderson, the Banker
* James Best as Lt. Robinson
* John Dierkes as Cpl. Fred Deane
* Helen Ford as Delphine Coates
* Harry Hines as Mr. Danzig Simon Scott as Capt. Floyd Henderson
* William Schallert as Rebel Soldier (uncredited) 
* Claude Akins as Lt. Ramsey (uncredited)

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 


 
 