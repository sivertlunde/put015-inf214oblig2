Tomorrow Never Comes
 
{{Infobox film
| name           = Tomorrow Never Comes 
| image          = 
| caption        =  Peter Collinson
| writer         = Sydney Banks David Pursall Jack Seddon
| starring       = Oliver Reed
| music          = Roy Budd
| cinematography = François Protat
| editing        = John Shirley
| studio         = Classic Montreal Trust Neffbourne
| distributor    = J. Arthur Rank Film Distributors (UK) Cinépix Film Properties (CFP)
| released       =  
| runtime        = 109 minutes
| country        = Canada United Kingdom
| language       = English
| budget         = CAD 2,341,000
| gross          = 
}} Peter Collinson Susan George.   

==Production==
The movie was a "tax shelter co-production" between the UK and Canada. The picture was filmed in the province of Québec. The picture is considered to be a Canadian exploitation film - a genre which is known as Canuxploitation.  

==Plot==
Coming back from an extended business trip, Frank (Stephen McHattie) discovers that his girlfriend Janie (Susan George) is now working at a new resort hotel where the owner has given her a permanent place to stay, as well as other gifts, in exchange for her affections. In the course of fighting over this development, tensions between Frank and Janie escalate out of control until he is holding her hostage in a standoff with the police. As the negotiators (Oliver Reed, Paul Koslo) try to talk Frank into giving himself up, the desperate man feels himself being pushed further and further into a corner. 

==Awards==
The film was entered into the 11th Moscow International Film Festival.   

==Cast==
*Oliver Reed as Jim Wilson Susan George as Janie
*Raymond Burr as Burke John Ireland as Captain
*Stephen McHattie as Frank
*Donald Pleasence as Dr. Todd
*Paul Koslo as Willy
*John Osborne as Robert L. Lyne
*Cec Linder as Milton
*Richard Donat as Ray
*Delores Etienne as Hilda

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 