Shanghai Grand
 
 
{{Infobox film name = Shanghai Grand image = ShanghaiGrand.jpg caption = Hong Kong film poster
| film name =   simplified  =   pinyin = Xīn Shànghǎi Tān jyutping = San1 Seong6 Hoi2 Taan1}} director = Poon Man-kit producer = Tsui Hark Tiffany Chen writer = Poon Man-kit Matt Chow Sandy Shaw starring = Andy Lau Leslie Cheung Ning Jing music = Raymond Wong cinematography = Poon Hang-sang editing = Marco Mak studio = Wins Entertainment Film Workshop distributor = Wins Entertainment (Hong Kong) released =   runtime = 96 minutes country = Hong Kong language = Cantonese budget =  gross = HK$20,837,056
}} 1980 television series of the same Chinese title, is a 1996 Hong Kong action crime drama film directed by Poon Man-kit and starring Andy Lau, Leslie Cheung and Ning Jing.
 triad leader who was not amused when Hui fell in love with the same girl as the one of his affections. Ning Jing played their love interest Fung Ching-ching.
 Shanghai during Republican era, triads shortly before it was occupied by the Japanese in the Second Sino-Japanese War.

==Cast==
* Andy Lau as Ting Lik
* Leslie Cheung as Hui Man-keung
* Ning Jing as Fung Ching-ching
* Wu Hsing-kuo as Fung King-yiu
* Lau Shun as Uncle Leung Amanda Lee as Lai-man
* Almen Wong as Fung King-yius assassin
* Chan Kin-yat as "Shorty" Chiu
* Jung Woo-sung as Taiwan Peoples League agent
* Lee Kin-yan as Ting Liks henchman
* Tse Liu-shut as Ting Liks henchman
* Yip Chun as Brother Four
* Wong Ming-sing as Fung King-yius henchman
* Ng Fei-git
* Leung Ka-chun
* Cheung Kam-bon
* Pak Ham-yat
* Ngai Tsang-siu

==Box office==
The film grossed HK$20,837,056 at the Hong Kong box office during its theatrical run from 13 July to 7 August 1996 in Hong Kong.

==Awards and nominations==
*16th Hong Kong Film Awards Best Action Choreography (Stephen Tung) Best Cinematography (Poon Hang Sang)
**Nominated: Best Art Direction (Bruce Yu)

==Music==
*Theme song: Shanghai Beach (上海灘) ( ) / Most Beloved Shanghai Beach (最愛上海灘) ( )
**Composer: Joseph Koo James Wong
**Arranger: Ting Chi Kwong
**Singer: Andy Lau

*Insert theme: Sleeping Alone (一個人睡) ( ) / Most Afraid of You Sleeping With Someone Else (最怕你跟别人睡) ( )
**Composer/Lyricist: Christopher Wong
**Arranger: Chiu Sang Hei
**Singer: Andy Lau

==See also==
* The Bund (TV series)|The Bund (TV series), the first part of the original The Bund trilogy, released in 1980.
* The Bund II, the second part of the original The Bund trilogy, also released in 1980.
* The Bund III, the third part of the original The Bund trilogy, also released in 1980.
* Once Upon a Time in Shanghai, a 1996 Hong Kong remake of The Bund.
* Shanghai Bund (TV series)|Shanghai Bund (TV series), a 2007 mainland Chinese remake of The Bund.
* Andy Lau filmography
* Leslie Cheung filmography

==External links==
* 
* 
*  on Hong Kong Cinemagic

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 