Toot, Toot!
 
 
 

{{Infobox album  
| Name        = Toot Toot!
| Type        = studio
| Artist      = The Wiggles
| Cover       = Toot, Toot! cover.jpg
| Released    = 1998
| Recorded    =
| Genre       = Childrens Music
| Length      =   ABC
| Producer    = The Wiggles
| Reviews     =
| Last album  = The Wiggles Movie Soundtrack (1997)
| This album  = Toot, Toot! (1998)
| Next album  = Its a Wiggly Wiggly World (2000)
}}
 1998 ARIA companion video was made later that year.

==Track listing==
{{Track listing
|+CD 
| writing_credits = yes
| all_writing = (or trad. arr. by) M. Cook/J. Fatt/A. Field/G. Page except noted below
| total_length = 43:09
| title1 = Toot Toot, Chugga Chugga, Big Red Car
| length1 = 2:22
| writer1 = 
| title2 = Look Both Ways
| length2 = 1:42
| writer2 = John Field (songwriter)|J.Field/M. Cook/J. Fatt/A. Field/G. Page 
| title3 = Dorothy the Dinosaur, Tell Me Who Is That Knocking?
| length3 = 1:24
| title4 = Food Poem
| note4 = spoken
| length4 = 0:12
| title5 = Food, Food, Food, Oh How I Love My Food
| length5 = 2:08
| writer5 = J.Field
| title6 = John Bradlelum
| length6 = 1:14
| write6 =
| title7 = Captain Featherswords Raincoat Story
| note7 = spoken
| length7 =  0:56
| title8 = Go Captain Feathersword, Ahoy!
| length8 = 2:13
| writer8 = J.Field/M. Cook/J. Fatt/A. Field/G. Page
| title9 = Bathtime Raffi
| length9 = 2:09
| title10 = Wah Hoo Hey, Im Combing My Hair Today
| length10 = 2:00
| title11 = Head, Shoulders, Knees and Toes
| length11 = 2:23
| writer11 =  
| title12 = Silver Bells That Ring in the Night
| writer12 = Edward Madden, Percy Wenrich
| length12 = 2:12
| title13 = Zardo Zap
| length13 = 2:41
| title14 = Henry the Octopus
| length14 = 1:26
| writer14 = M. Cook/J. Fatt/A. Field
| title15 = Move Your Arms Like Henry
| length15 = 1:49
| writer15 = Paul Field (musician)|P.Field
| title16 = Do the Wiggle Groove
| length16 = 2:18
| writer16 =  J.Field/M. Cook/J. Fatt/A. Field/G. Page
| title17 = Balla Balla Intro
| note17 = spoken
| length17 = 0:15
| title18 = Balla Balla Bambina
| length18 = 2:10
| writer18 = A. Renaldi
| title19 = I Climb Ten Stairs
| length19 = 1:11
| title20 = Were Dancing with Wags the Dog
| length20 = 1:22
| title21 = Wags the Dog, He Likes to Tango
| length21 = 1:04
| title22 = Captain Feathersword Fell Asleep on His Pirate Ship (Quack Quack)
| length22 = 1:55
| title23 = Rocket
| length23 = 1:50
| title24 = Officer Beaples Dance
| length24 = 1:41
| writer24 =  D.Lindsay/M. Cook/J. Fatt/A. Field/G. Page
| title25 = Lets Have a Ceili
| length25 = 2:41
| writer25 = D.Lindsay
}}

==Musicians==
* Vocals: Greg Page
* Backing Vocals: Greg Page, Anthony Field, Murray Cook, Jeff Fatt, Paul Paddick, Mark Punch and Kevin Bennett
* Guitars: Anthony Field, Murray Cook and Tony Douglass
* Bass: Murray Cook
* Piano, Organ and Accordion: Jeff Fatt
* Drums and Percussion: Tony Henry and Paul Hester
* Violin: Maria Schattovits
* Viola: Angela Lindsay
* Cello: Margaret Lindsay
* Trumpet: Dominic Lindsay

== Release history ==
The album was released in 1998 in CD and cassette formats:   
* ABC Music: 7243 4 94411 25.
* ABC Music: 7243 4 94411 49.
* ABC Music: 7243 4 96238 2 8 - CD re-release with new artwork

==Video==
{{Infobox film
| name           = Toot Toot!
| image          =
| alt            =
| caption        = Toot Toot! 1998 cover
| director       = Chisholm McTavish Paul Field
| starring       = {{Plainlist| Greg Page
* Anthony Field
* Murray Cook
* Jeff Fatt
}}
| music          = The Wiggles
| editing        =
| studio         =
| distributor    = Roadshow Entertainment / ABC Video  (#101484) 
| released       =   The Powerhouse Museum exhibit notes the release date as September 1998, however The Wiggles announced a more precise date on their website.      
| runtime        = 40&nbsp;minutes
| country        = Australia
| budget         =
| gross          =
}}

Toot Toot! is the seventh video by the childrens band The Wiggles. It was released on 17 October 1998,  and later re-released in 1999 to reflect set updates during production of the TV series.

===Song list===
# Look Both Ways
# John Bradlelum
# Henrys Underwater Big Band (1999 version)
# Head, Shoulders, Knees and Toes
# Food, Food, Food, Oh How I Love My Food
# Go Captain Feathersword, Ahoy!
# Bathtime (1998 version) Greg is the only Wiggle who participates in this song. 
# Do the Wiggle Groove
# Dorothy the Dinosaur (Tell Me Who Is That Knocking?)
# Balla Balla Bambina
# I Climb Ten Stairs
# Move Your Arms Like Henry
# Silver Bells That Ring in the Night
# Wags the Dog, He Likes to Tango
# Were Dancing with Wags the Dog
# Officer Beaples Dance
# Zardo Zap
# Lets Have a Ceili
# Toot Toot, Chugga Chugga, Big Red Car

===Cast===
The cast is presented as listed in the 1998 closing credits.  In the 1999 version, Captain Feathersword, Dorothy the Dinosaur, Wags the Dog, and Henry the Octopus were credited as "The Characters"; the other characters and dancers were grouped together in alphabetical order. The voice talents (Carolyn Ferrie as Dorothys voice, Mic Conway as Wagss voice, Jeff Fatt as Henrys voice) were not listed in either version.  Foodman was played by Anthony Field, although a credit was not shown.

;The Wiggles Greg Page
* Anthony Field
* Murray Cook
* Jeff Fatt

; Also featuring
* Paul Paddick as Captain Feathersword
* Leeanne Ashley as Dorothy The Dinosaur
* Edward Rooke as Wags The Dog
* Elisha Burke as Henry the Octopus

* Leanne Halloran as Officer Beaples
* Leanne Halloran and Elyssa Dawson as Zardo Zap Halloran plays Zardo Zap in the "Zardo Zap" song, while Dawson plays Zardo Zap in the "Lets have a Ceili" song. 
* Mitchell Butel as Raiffe the Mechanic
* Fergus and Pasqua Field as Anthony Wiggles Puppies

; Dancers
Elyssa Dawson, Amy Dunbar, Clare Field, Joseph Field, Cassandra Halloran, Jessica Halloran, Kate Halloran, Graeme Hickey, Ashliegh Johns, Kristen Knox, Cie Jai Leggett, Cameron Lewis, Tamahra Macey, Sam Moran, Rebekka Osborne, Scott Porter, James Runge, Emma Ryan, Sian Ryan, Talicia Williams, Larrissa Wright

; Toddlers in "Food Food"
Tara Fitzgerald, Dominic Field, Ceili Moore, Gabrielle Rawlings

===Release===  

Toot Toot! was first released on 17 October 1998. 

In 1999, The Wiggles re-released the video, but made significant edits to reflect changes made in the television series.  Although the general story is still about The Wiggles having to find a way to fix their Big Red Car, the scenes and songs were re-recorded with new animations and backgrounds, mostly "John Bradlelum" and "Toot Toot, Chugga Chugga, Big Red Car". Some of the television series episodes retain the original 1998 versions of the videos. The 1998 version of "John Bradlelum" can be seen in episodes "Storytelling" and "Family". The 1998 version of "Toot Toot, Chugga Chugga, Big Red Car" is also found on "Storytelling" and "Haircut".  "Bathtime" and its introduction were replaced with a new version of the "Henrys Underwater Big Band" video (number 3 not number 7)
 Yummy Yummy.

The 1999 version was released to DVD in 2004. The extras include a Wiggly Work storybook and two episodes from the Lights, Camera, Action TV series. 

==Notes==
 

==References==
* 
 

 

 
 
 
 
 