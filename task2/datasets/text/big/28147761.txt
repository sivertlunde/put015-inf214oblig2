Grete Minde
 
{{Infobox film
| name           = Grete Minde
| image          = 
| caption        = 
| director       = Heidi Genée
| producer       = Bernd Eichinger Peter Genée
| writer         = Heidi Genée Theodor Fontane
| starring       = Katerina Jacob
| music          = 
| cinematography = Jürgen Jürges
| editing        = Heidi Genée
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Austria West Germany
| language       = German
| budget         = 
}}

Grete Minde is a 1977 Austrian-German drama film based on the novel by Theodor Fontane and directed by Heidi Genée. It was entered into the 27th Berlin International Film Festival.   

==Cast==
* Katerina Jacob – Grete Minde
* Siemen Rühaak – Valtin Zernitz
* Hannelore Elsner – Trude Minde
* Tilo Prückner – Gerd Minde
* Brigitte Grothum – Emerentz Zernitz
* Käthe Haack – Domina
* Hans Christian Blech – Gigas
* Hilde Sessak – Regine
* Martin Flörchinger – Vater Minde
* Horst Niendorf – Valtins Onkel
* Angelika Hillebrecht – Valtins Tante
* Evelyn Meyka – Bäuerin
* Alexander May – Bürgermeister Jan Groth – Ratsherr
* Hans Karl Friedrich – Pfarrer Roggenstroh
* Helga Storck – Nonne

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 
 