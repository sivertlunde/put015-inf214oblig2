Mone Prane Acho Tumi
{{Infobox Film
| name   = Mone Prane Acho Tumi
| image    =   Mone Prane Acho Tumi Cover.jpg
| image_size  = 200px
| caption     = VCD Cover
| director      = Jakir Hossain Raju
| producer    = Korshad alam
| writer         = Jakir Hossain Raju
| starring    =  Shakib Khan Apu Biswas Razzak Shagorika Don Ahmed Sarif Misha Soudagor
| music          =Imtheos BolBol
| cinematography = 
| editing        = 
| distributor    = Anupom
| released       = 2008
| runtime        = 
| country        = Bangladesh Bengali
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
| imdb_id        = 
}}
Mone Prane Acho Tumi ( ;  ) is a Dallywood Bengali film that released in  2008.  Its a romantic comedy film, directed by Jakir Hossain Raju and leading stars are Shakib Khan, Apu Biswas, Razzak, Shagorika, Misha Soudagor and many more. and this film is one of the top grossing Dhallywood film of 2008.

==Cast==
* Shakib Khan 
* Apu Biswas  
* Razzak
* Shagorika 
* Monjur
* Rehana Joli
* Jamilur Rahman Sakha
*  Don
* Ahmed Sarif
* Misha Soudagor
* Morjina
* Chikon Ali

==Crew==
* Director: Jakir Hossain Raju
* Producer: Korshad alam
* Story: Jakir Hossain Raju
* Script: Jakir Hossain Raju
* Music: Imon Saha
* Lyrics: Kabir Bokul
* Distributor: Anupom

==Technical details==
* Format: 35 MM (Color)
* Running Time: 150 Minutes
* Real: 13 Pans Bengali
* Country of Origin: Bangladesh
* Date of Theatrical Release: 2008
* Year of the Product: 2008
* Technical Support: Bangladesh Film Development Corporation (BFDC)

==Music==
{{Infobox album
| Name = Mone Prane Acho Tumi
| Type = soundtrack
| Cover = Mone prane acho tumi.jpg
| Artist = Imtheos BolBol
| Released = 2008 (Bangladesh)
| Recorded = 2008
| Genre = Films Sound Track
| Length =
| Label = 
| Producer = Anupom
}}
Music of Mone Prane Acho Tumi is directed by Imon Saha. Ek Bindu Valobasha Dao & konna tomar hasi te become most popular song in 2008.

===Soundtrack===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#CCCCCC;"
! Tracks !! Songs !! Singers !! Performers
|- 1
|"Ek Bindu Valobasha Dao"  Andrew Kishor and Konok Chapa Shakib Khan and Apu Biswas
|- 2
|"Ki Rup Dekhaila Maula"   Reshad
|Shakib Khan
|- 3
|"Kache Asha Holo Valobasha Holo"  Andrew Kishor and Baby Naznin Shakib Khan and Apu Biswas
|- 4
|"Ami Chailam Jare"  Salma
|Sagarika 
|- 5
|"Moner Joto Vabna" 
| Misha Soudagor 
|-
|}

==References==
 

==External links==

 
 
 
 
 
 


 
 