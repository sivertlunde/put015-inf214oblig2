Make the Yuletide Gay
{{Infobox film
| name           = Make the Yuletide Gay
| image          = Make the yuletide gay theatrical poster.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Rob Williams
| producer       = Rodney Johnson Matthew Montgomery Rob Williams
| writer         = Rob Williams
| starring       = Keith Jordan  Adamo Ruggiero  Hallee Hirsh Kelly Keaton Derek Long Alison Arngrim Ian Buchanan Gates McFadden
| music          = Austin Wintory
| cinematography = Ian McGlocklin
| editing        = Denise Howard
| studio         = Guest House Films
| distributor    = TLA Releasing
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Rob Williams. Derek Long star as Anya and Sven, Gunns parents, while Hallee Hirsh appears as Abby, Gunns high school girlfriend.
 Inside Out Toronto LGBT Film and Video Festival on May 17, 2009. 

The title of the film comes from a line in the 1944 song Have Yourself a Merry Little Christmas, written by Hugh Martin and Ralph Blane.

==Cast== 
* Keith Jordan as Olaf "Gunn" Gunnunderson: Gunn is a college student who is openly gay at school. Back at home, his parents, neighbors, and high school friends think that he is heterosexuality|straight. Gunn is afraid that his parents will stop loving him if they find out that he is gay.  When he goes home for Christmas, he has to change the way he dresses and his personal mannerisms in order to maintain his heterosexual façade. dorm roommate. Unlike Gunn, Nathan is completely open about his sexuality, including with his parents.
* Hallee Hirsh as Abby Mancuso: Abby lives across the street from Gunn, and they had a brief romantic relationship while they were in high school.  
*   and is a housewife.
*  -smoking father.  He grew up in Minnesota and is a professor at the local college.
* Alison Arngrim as Heather Mancuso: Heather is Abbys mother.  Heather and Anya pretend to be friends, but they secretly despise each other.
*  , leaving Nathan all alone for Christmas.

==Reception==
Despite not receiving any critic reviews on Rotten Tomatoes, it holds a score of 64% rating from audience reviews.
===Awards=== 
   
{| class="wikitable"
! Year !! Festival !! Award !! Category
|- FilmOut San Audience Award||Best Narrative Feature Best Supporting Actress, Kelly Keaton Best Supporting Actor, Derek Long
|- Seoul LGBT Audience Award||Best Feature
|- Long Island Jury Award||Best Mens Feature
|- North Carolina Gay & Lesbian Film Festival||Mens Centerpiece Selection||
|- Philadelphia QFest||Festival Favorite||
|- Tampa International Gala Night Film||
|-
|}

==DVD release==
Make the Yuletide Gay was released on DVD on November 10, 2009. 

==See also==
* List of lesbian, gay, bisexual, or transgender-related films by storyline

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 