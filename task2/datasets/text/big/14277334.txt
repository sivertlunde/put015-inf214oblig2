Twilight (2008 film)
 
 
{{Infobox film
| name = Twilight
| image = Twilight (2008 film) poster.jpg
| caption = Theatrical release poster
| alt = A pale young man fills the top left of the poster, standing over a brown-haired young woman on the right, with the word "twilight" on the lower right.
| director = Catherine Hardwicke
| producer = Wyck Godfrey Greg Mooradian Mark Morgan
| screenplay = Melissa Rosenberg
| based on = Twilight (novel)|Twilight by Stephenie Meyer Billy Burke Peter Facinelli
| music = Carter Burwell
| cinematography = Elliot Davis
| editing = Nancy Richardson
| studio = Temple Hill Entertainment Maverick Films Imprint Entertainment DMG Entertainment
| distributor = Summit Entertainment
| released =  
| runtime = 121 minutes    126 minutes (Extended cut)
| country = United States
| language = English
| budget = $37 million 
| gross = $392,616,625 
}} vampire romance novel of the same name. Directed by Catherine Hardwicke, the film stars Kristen Stewart and Robert Pattinson. It is the first film in The Twilight Saga (film series)|The Twilight Saga film series. This film focuses on the development of the relationship between Bella Swan (a teenage girl) and Edward Cullen (a vampire), and the subsequent efforts of Cullen and his family to keep Swan safe from a coven of evil vampires.
 screen adaptation stagnant development. Melissa Rosenberg wrote a new adaptation of the novel shortly before the 2007–2008 Writers Guild of America strike and sought to be faithful to the novels storyline. Principal photography took 44 days  and completed on May 2, 2008;   Photography | publisher=Film School Rejects | date=2008-05-03 | accessdate=2008-06-14}}  the film was primarily shot in Oregon. 
 soundtrack was New Moon the series, were produced as films the following year.

==Plot== Isabella "Bella" Washington coast, aloof Cullen siblings. Bella sits next to Edward Cullen in biology class on her first day of school; he appears to be disgusted by her, much to Bellas confusion. A few days later, Bella is nearly struck by a van in the school parking lot. Edward inexplicably moves from several feet away and stops the vehicle with his hand without any harm coming to himself or Bella. He later refuses to explain this act to Bella and warns her against befriending him.

After much research, Bella eventually discovers that Edward is a vampire, though he only consumes animal blood. The pair fall in love, and Edward introduces Bella to his vampire family, Carlisle Cullen|Carlisle, Esme Cullen|Esme, Alice Cullen (Twilight character)|Alice, Jasper Hale|Jasper, Emmett Cullen|Emmett, and Rosalie Hale|Rosalie. Soon afterward, three nomadic vampires—James (Twilight)|James, Victoria (Twilight)|Victoria, and Laurent (Twilight)|Laurent—arrive. James, a tracker vampire, is intrigued by Edwards protectiveness over a human and wants to hunt Bella for sport. Edward and his family risk their lives to protect her, but James tracks Bella to Phoenix, Arizona|Phoenix, where she is hiding, and lures her into a trap by claiming he is holding her mother hostage. James attacks Bella and bites her wrist, but Edward and the other Cullen family members arrive before he can kill her. James is destroyed, and Edward sucks James venom from Bellas wrist, preventing her from becoming a vampire. The severely injured Bella is taken to a hospital. Upon returning to Forks, Bella and Edward attend their school prom. While there, Bella expresses her wish to become a vampire, which Edward refuses to grant. The film ends with Victorias secretly watching the pair dancing, plotting revenge for her lover James death.

==Cast==
 
;Main cast
* Kristen Stewart as Bella Swan, a seventeen-year-old girl who moves to the small town of Forks, Washington from Phoenix, Arizona and falls in love with a vampire, Edward Cullen. Her life is put in danger after a sadistic vampire, James, decides to hunt her.    read minds, with the exception of Bellas, along with superhuman speed and strength.  

;Secondary cast
* Peter Facinelli as Carlisle Cullen, a compassionate 300-plus-year-old vampire who looks to be in his early 30s. He serves as the towns physician and is the father figure of the Cullen family. 
* Elizabeth Reaser as Esme Cullen, Carlisles vampire wife and a mother figure to the Cullen family.    Alice Cullen, a vampire who can see the future based on decisions that people make. 
* Kellan Lutz as Emmett Cullen, physically the strongest vampire of the family. 
* Nikki Reed as Rosalie Hale, a Cullen family member described as the most beautiful person in the world. She is incredibly hostile toward Bella throughout the entire film. 
* Jackson Rathbone as Jasper Hale, a member of the Cullen family who can manipulate emotions. He is the newest member of the Cullen family, and thus has the most difficulty maintaining their lifestyle of feeding only on animals instead of humans.  Billy Burke Charlie Swan, Bellas father and Forks Chief of Police.    James Witherdale, the leader of a group of nomadic vampires that intends to kill Bella. He is Victorias mate and a gifted tracker, due to his unparalleled senses.  Victoria Sutherland, James mate who assists him in finding Bella.  Laurent Da Revin, the most civilized member of James coven. 
* Sarah Clarke as List of Twilight characters#Renée Dwyer|Renée Dwyer, Bellas mother who lives in Arizona with her new husband, Phil.  Quileute tribe.    Angela Weber, one of Bellas new friends in Forks.    Michael Welch Mike Newton, one of Bellas new friends who vies for her attention.  Jessica Stanley, Bellas first friend in Forks.  Tyler Crowley, another one of Bellas classmates, also vying for Bellas attention. He nearly hits Bella with his van. Eric Yorkie, another one of Bellas classmates who vies for her attention. 

==Production==

===Development=== optioned by unrelated film with the same title in 1998) in a turnaround (film industry term)|turnaround.    The company perceived the film as an opportunity to launch a franchise based on the success of Meyers book and its sequels.   Catherine Hardwicke was hired to direct the film and Melissa Rosenberg to write the script in mid-2007. 
 Writers Guild of America strike, Rosenberg worked full-time to finish the screenplay before October 31.  In adapting the novel, she "had to condense a great deal." Some characters from the novel were not featured in the screenplay, whereas some characters were combined into others.    " ur intent all along was to stay true to the book", Rosenberg explained, "and it has to do less with adapting it word for word and more with making sure the characters arcs and emotional journeys are the same."  Hardwicke suggested the use of voice over to convey Bellas internal dialogue —since the novel is told from her point of view—and she sketched some of the storyboards during pre-production. 

 

===Adaptation from source material===
The filmmakers behind Twilight worked to create a film that was as faithful to the novel as they thought possible when converting the story to another medium, with producer Greg Mooradian saying, "Its very important to distinguish that were making a separate piece of art that obviously is going to remain very, very faithful to the book.... But at the same time, we have a separate responsibility to make the best movie you can make."    To ensure a faithful adaptation, Meyer was kept very involved in the production process, having been invited to visit the set during filming and even asked to give notes on the script and on a rough cut of the film.    Of this process, she said, "It was a really pleasant exchange   from the beginning, which I think is not very typical. They were really interested in my ideas",    and, "...they kept me in the loop and with the script, they let me see it and said, What are your thoughts?... They let me have input on it and I think they took 90 percent of what I said and just incorporated it right in to the script."  Meyer fought for one line in particular, one of the most well-known from the book about "the lion and the lamb", to be kept verbatim in the film: "I actually think the way Melissa   wrote it sounded better for the movie   but the problem is that line is actually tattooed on peoples bodies   But I said, You know, if you take that one and change it, thats a potential backlash situation.   Meyer was even invited to create a written list of things that could not be changed for the film, such as giving the vampires fangs or killing characters who do not die in the book, that the studio agreed to follow.   The consensus among critics is that the filmmakers succeeded in making a film that is very faithful to its source material,   with one reviewer stating that, with a few exceptions, "Twilight the movie is unerringly faithful to the source without being hamstrung by it." 

{{quote box|width=30%|left|quote=They could have filmed   and not called it Twilight because it had nothing to do with the book... When Summit   came into the picture, they were so open to letting us make rules for them, like "Okay, Bella cannot be a track star. Bella cannot have a gun or night vision goggles. And, no jet skis...."|source=Twilight author Stephenie Meyer |align=left
}}

However, as is most often the case with film adaptations, differences exist between the film and source material. Certain scenes from the book were cut from the film, such as a biology room scene where Bellas class does blood typing. Hardwicke explains, "Well   almost 500 pages—you do have to do the sweetened condensed milk version of that.... We already have two scenes in biology: the first time theyre in there and then the second time when they connect. For a film, when you condense, you dont want to keep going back to the same setting over and over. So thats not in there."    The settings of certain conversations in the book were also changed to make the scenes more "visually dynamic" on-screen, such as Bellas revelation that she knows Edward is a vampire—this happens in a meadow in the film instead of in Edwards car as in the novel.  A biology field trip scene is added to the film to condense the moments of Bellas frustration at trying to explain how Edward saved her from being crushed by a van.  The villainous vampires are introduced earlier in the film than in the novel. Rosenberg said that "you dont really see James and the other villains until to the last quarter of the book, which really wont work for a movie. You need that ominous tension right off the bat. We needed to see them and that impending danger from the start. And so I had to create back story for them, what they were up to, to flesh them out a bit as characters."  Rosenberg also combined some of the human high school students, with Lauren Mallory and Jessica Stanley in the novel becoming the character of Jessica in the film, and a "compilation of a couple of different human characters" becoming Eric Yorkie.  About these variances from the book, Mooradian stated, "I think we did a really judicious job of distilling  . Our greatest critic, Stephenie Meyer, loves the screenplay, and that tells me that we made all the right choices in terms of what to keep and what to lose. Invariably, youre going to lose bits and pieces that certain members of the audience are going to desperately want to see, but theres just a reality that were not making Twilight: The Book the movie." 

===Casting===
{{quote box|width=30%|quote=When they told me Rob was probably the one, I looked him up and thought, "Yeah, he can do a version of Edward. He’s definitely got that vampire thing going on." And then, when I was on set and I got to watch him go from being Rob to shifting into being Edward, and he actually looked like the Edward in my head, it was a really bizarre experience.   He really had it nailed.|source=Twilight author Stephenie Meyer 
}}
 Ben Barnes, Midnight Sun, which chronicles the events in Twilight from Edwards point of view.  Fan reaction to Pattinsons casting as Edward was initially negative; Rachelle Lefèvre remarked that " very woman had their own Edward   they had to let go of before they could open up to  , which they did."  Meyer was "excited" and "ecstatic" in response to the casting of the two main characters.  She had expressed interest in having Emily Browning and Henry Cavill cast as Bella and Edward, respectively, prior to pre-production. 

Peter Facinelli was not originally cast as Carlisle Cullen. "  liked me, but there was another actor that the studio was pushing for", Facinelli said.  For unknown reasons, that actor was not able to play the part and Facinelli was selected in his place.  The choice of Ashley Greene to portray Alice Cullen was the subject of fan criticism due to Greene being   taller than her character as described in the novel. Meyer had also stated that Rachael Leigh Cook resembled her vision of Alice.    Nikki Reed had previously worked with Hardwicke on Thirteen (2003 film)|Thirteen, which they wrote together, and Lords of Dogtown. Reed commented, "I dont want to say its a coincidence, because we do work well together, and we have a great history. I think we make good work, but its more that the people that hire   to direct a film of theirs   most likely seen her other work." 
 Generation Kill when the auditions for the character of Emmett Cullen were conducted. The role had already been cast by the time that production ended in December 2007, but the actor who had been selected "fell through"; Lutz subsequently auditioned and was flown to Oregon, where Hardwicke personally chose him.  Rachelle Lefèvre was interested in pursuing a role in the film because Hardwicke was attached to the project as director; there was also "the potential to explore a character, hopefully, over three films"; and she wanted to portray a vampire.    She "thought that vampires were basically the best metaphor for human anxiety and questions about being alive."  Christian Serratos initially auditioned for Jessica Stanley, but she "fell totally in love with Angela" after reading the novels and successfully took advantage of a later opportunity to audition for Angela Weber.  The role of Jessica Stanley went to Anna Kendrick, who got the part after two mix-and-match auditions with various actors. 

===Filming and post-production===
On a bed in Catherine Hardwickes house is where   to make their bodily movements more elegant.   

Scenes were filmed primarily in Portland, Oregon.    Stunt work was done mainly by the cast.  The fight sequence between Gigandet and Pattinsons characters in a ballet studio, which was filmed during the first week of production, involved a substantial amount of wire work because the vampires in the story have superhuman strength and speed.    Gigandet incorporated mixed martial arts fighting moves in this sequence, which involved chicken and honey as substitutes for flesh.  Bella, the protagonist, is unconscious during these events, and since the novel is told from her point of view, such action sequences are illustrative and unique to the film.  Pattinson noted that maintaining ones center of gravity is difficult when doing wire work "because you have to really fight against it as well as letting it do what it needs to do."  Lefèvre found the experience disorienting since forward motion was out of her control. 
 Madison High Harry Potter and the Half-Blood Prince was rescheduled for an opening in July 2009.    Two teaser trailers, as well as some additional scenes, were released for the film, as well as a final trailer, which was released on October 9.   A 15-minute excerpt of Twilight was presented during the International Rome Film Festival in Italy.  The film received a rating of Motion Picture Association of America film rating system|PG-13 from the Motion Picture Association of America for "some violence and a scene of sensuality". 

===Music===
  score for Muse and Linkin Park, bands she listened to while writing the novels.   The original soundtrack was released on November 4, 2008, by Chop Shop Records in conjunction with Atlantic Records.  It debuted at number 1 on the Billboard 200|Billboard 200.   

==Release==

===Box office=== The Dark Harry Potter Deep Impact (1998). 

===Critical reception=== Hardwicke for turning "Stephenie Meyer|Meyers book series into a Brontë-esque vision."  Roger Ebert gave the film two-and-a-half stars out of four and wrote, "I saw it at a sneak preview. Last time I saw a movie in that same theater, the audience welcomed it as an opportunity to catch up on gossip, texting, and laughing at private jokes. This time the audience was rapt with attention".  In his review for the Los Angeles Times, Kenneth Turan wrote, "Twilight is unabashedly a romance. All the storys inherent silliness aside, it is intent on conveying the magic of meeting that one special person youve been waiting for. Maybe it is possible to be 13 and female for a few hours after all".  USA Today gave the film two out of four stars and Claudia Puig wrote, "Meyer is said to have been involved in the production of Twilight, but her novel was substantially more absorbing than the unintentionally funny and quickly forgettable film".  Entertainment Weekly gave the film a "B" rating and Owen Gleiberman praised Hardwickes direction: "She has reconjured Meyers novel as a cloudburst mood piece filled with stormy skies, rippling hormones, and understated visual effects". 

===Home media===
 
The film was released on DVD in North America on March 21, 2009, through midnight release parties, and sold over 3 million units in its first day.    It was released on April 6, 2009 in the UK.   Bonus features include about 10 to 12 extended or deleted scenes, montages and music videos, behind-the-scenes interviews, a "making-of" segment, and commentary featuring Hardwicke, Stewart, and Pattinson.   The Blu-ray disc edition of the film was released on March 21, 2009, in select locations, but was made more widely available at further retailers on May 5, 2009.  As of July 2012, the film has sold 11,242,519 units, earning $201,190,019. 


The film and the next two installments of the Twilight Saga will be rereleased as a triple feature with extended cuts on January 13, 2015.

===Video game===
 
 PC and the iPhone was released alongside with the second film.

===Accolades=== World Soundtrack Young Hollywood Award for her directing.  In addition, the film was nominated for Best Fantasy Film at the 35th Saturn Awards  and two Grammy Awards. 

==Sequel==
  New Moon, the second book in the series, by October 2008,  and confirmed their plans to make a film based on it November 22, 2008.   Because Catherine Hardwicke had wanted more preparation time than Summits schedule for the production and release of the sequel would provide,     Chris Weitz was selected to direct it in December 2008.  

==See also==
 
* Apotamkin, Bellas Google hit for the cold ones
* Vampire film

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 