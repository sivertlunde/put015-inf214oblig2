Ten9Eight: Shoot for the Moon
{{Infobox film
| name           = Ten9Eight: Shoot for the Moon
| image          = Ten9eight_Shoot_for_the_Moon.jpg
| image_size     =
| caption        =
| director       = Mary Mazzio
| producer       = Mary Mazzio
| writer         = Mary Mazzio
| narrator       = 
| starring       = Tatyana Blackwell, Jessica Cervantes, Gabriel Echoles, Shan Shan Huang, Amanda Loyola, William Mack, Anné Montague, Alexander Niles, Rodney Walker, JaMal Willis
| music          = Alex Lasarenko
| cinematography = Richard Klug
| editing        = Paul Gattuso
| studio         = 50 Eggs Films
| distributor    = 
| released       =  
| runtime        = 84 minutes 
| country        = United States
| language       = English
| gross          = 
| preceded_by    =
| followed_by    =
}}

Ten9Eight: Shoot for the Moon is a 2009 documentary film written, directed, and produced by Mary Mazzio about inner NYC teens that compete in an annual business plan competition run by the Network for Teaching Entrepreneurship (NFTE). 

==Synopsis==
  (NFTE).    The teens come from all over the country and are among the 24,000 students that compete in the event each year, with the winner taking home a $10,000 grand prize.        

==Cast==
*Tatyana Blackwell
*Jessica Cervantes
*Gabriel Echoles
*Shan Shan Huang
*Amanda Loyola
*William Mack
*Robbie Martin
*Anne Montague
*Alexander Niles
*Rodney Walker
*JaMal Willis

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 