The Hope Chest
 
 
{{Infobox film
| name           = The Hope Chest
| image          = The Hope Chest (1918) - Ad 1.jpg
| caption        = Ad for the film
| director       = Elmer Clifton
| producer       =
| writer         = M.M. Stearns (scenario)
| based on       =  
| starring       = Dorothy Gish George Fawcett Richard Barthelmess Sam De Grasse
| music          =
| cinematography = Lee Garmes John W. Leezer
| editing        =
| distributor    = Paramount Pictures / Artcraft
| released       =  
| runtime        =
| country        = United States
| language       = Silent (English intertitles)
| budget         =
| gross          =
}}
The Hope Chest is an American silent film released in 1918, starring Dorothy Gish. The film was directed by Elmer Clifton and based on a serialized story (and later novel) by Mark Lee Luther, originally published in Womans Home Companion. Its survival status is classified as unknown,  which suggests that it is a lost film.

==Plot==
Sheila Moore (Gish) takes a job at a candy store to support her father, an out-of-work vaudevillian. She attracts the romantic attentions of the store owners son Tom (Barthalmess) and marries him, incurring the wrath of Toms parents.

==Cast==
* Dorothy Gish - Sheila Moore
* George Fawcett - Lew Moore
* Richard Barthelmess - Tom Ballantyne
* Sam De Grasse - Ballantyne, Sr.
* Kate Toncray - Mrs. Ballantyne
* Carol Dempster - Ethel Hoyt
* Bertram Grassby - Stoughton Lounsbury

==References==
 

==External links==
*  
*  
*   at the Silent Film Still Archive

 
 
 
 
 
 
 
 
 
 

 
 