Manathil Uruthi Vendum
{{Infobox film
| name = Manathil Uruthi Vendum
| image = ManathilUruthi.jpg
| caption = LP Records Cover
| director = K. Balachander   
| producer = Rajam Balachander Pushpa Kandaswamy
| writer = K. Balachander
| narrator = Suhasini Sridhar Sridhar S. Vivek
| music = Ilaiyaraaja 
| cinematography = Raghunatha Reddy
| editing = Ganesh - Kumar
| studio = Kavithalayaa Productions
| distributor = Kavithalayaa Productions
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}}

Manathil Uruthi Vendum is a 1987 Indian Tamil language Drama film directed by Kailasam Balachander|K. Balachander starring Suhasini Mani Ratnam|Suhasini, S. P. Balasubrahmanyam, Ramesh Aravind, Sridhar (Kannada actor)|Sridhar, Vivek (actor)|Vivek, Lalithakumari and Yamuna.

Rajinikanth, Sathyaraj, and Vijayakanth make guest appearances. 

== Plot ==

This is the story of Nandini, a nurse, played brilliantly by Suhasini Mani Ratnam|Suhasini. She is the sole bread winner of her huge family who faces many hurdles in her life including a divorce, loss of a brother, an eloped sister, a failed second romance and an organ donation. She handles them with dignity and wins the hearts of one and all including her suspicious ex-husband when she donates him her kidney. S. P. Balasubrahmanyam plays the role of a dedicated Doctor, who inspires Nandini to take the ultimate decision in life to continue serving her patients. 

==Cast== Suhasini
*Sridhar Sridhar
*S. P. Balasubrahmanyam
*Ramesh Aravind Vivek
*Rajini Kanth Guest appearance
*Vijayakanth Guest appearance
*Sathyaraj Guest appearance

==Production==
While assisting with the script for the film in 1987, Balachandar offered Vivek an acting role of Suhasinis brother in the film, which he decided to pursue thus made his acting debut in the film.  Recalling his experience: "I remember the first day of my shooting   I was asked to come running down the stairs. I did the shot to his satisfaction. But in doing so I hurt my toes. But I did not want to show it to him. But after a few minutes the blood was oozing from the wound. KB sir saw it and asked me immediately to attend to it". 

== Soundtrack ==

{{Infobox album  
| Name        = Manathil Uruthi Vendum
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       =
| Caption     =
| Released    =    
| Recorded    =
| Genre       =
| Length      = Tamil
| Label       =
| Producer    =
| Reviews     =
| Compiler    =
| Misc        =
}}
The music composed by Ilaiyaraaja, with the lyrics written by Vaali (poet)|Vaali.{{tracklist
| collapsed       =
| headline        =
| extra_column    = Singers
| total_length    =
| all_writing     =
| all_lyrics      =
| all_music       =
| writing_credits =
| lyrics_credits  =
| music_credits   =
| title1          = Manathil Uruthi Vendum
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = K. J. Yesudas
| length1         = 4:19
| title2          = Kannin Maniyae
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = K. S. Chitra
| length2         = 4:49
| title3          = Kannaa Varuvaayaa
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = K. J. Yesudas, K. S. Chitra
| length3         = 5:33
| title4          = Aachchi Aachchi
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Mano (singer)|Mano, K. S. Chitra
| length4         = 3:06
| title5          = Sangathamizh Kaviye
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = K. J. Yesudas, K. S. Chitra
| length5         = 4:31
| title6          = Vangaala Kadale
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          = K. J. Yesudas
| length6         = 2:52

}}

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 


 