Black Snake Moan (film)
{{Infobox film
| name           = Black Snake Moan
| image          = Blacksnakemoan3.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Craig Brewer
| producer       = Stephanie Allain John Singleton
| writer         = Craig Brewer
| starring       = Samuel L. Jackson Christina Ricci Justin Timberlake
| music          = Scott Bomar
| cinematography = Amy Vincent
| editing        = Billy Fox
| studio         = Paramount Classics
| distributor    = Paramount Vantage
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $15 million 
| gross          = $10,903,846 
}} American drama nymphomania after finding her severely beaten on the side of a road.
 Mississippi Blues movement, not least in its title and soundtrack.

==Plot== sex addict. Tennessee National Guard, and in his absence, she indulges in bouts of promiscuity and drug use. During one of Raes binges, Ronnies friend Gill (Michael Raymond-James) tries to take advantage of her. She laughs at his advances, comparing him unfavorably with another man, and he severely beats her. Believing shes dead, Gill dumps Rae and leaves her for dead in only a shirt and underwear by the side of the road and drives away.

Lazarus discovers Rae unconscious in the road the next morning and brings her home to nurse her back to health. Lazarus goes to see Tehronne (David Banner) - the man who Lazarus thought had beaten her - and learns of her promiscuity. Over the course of several days, Rae, delirious with fever, occasionally wakes up and tries to flee from Lazarus. He chains her to the radiator to keep her from running away. After Rae regains her wits, Lazarus announces that it is his spiritual duty to heal her of her sinful ways and refuses to release her until he does so. Rae makes several attempts to escape, and even briefly has sex with a teenage boy who helps out on Lazarus farm.

She eventually comes to tolerate her position. Lazarus buys her a conservative dress to wear, plays the guitar for her, and feeds her home-cooked meals. Lazarus pastor and close friend, R.L. (John Cothran, Jr.), visits Lazarus at his house and discovers that Lazarus is imprisoning Rae. The pastor tries to reason with Lazarus and the group shares a meal.

Meanwhile, Ronnie returns to town after being discharged from the National Guard due to his severe anxiety disorder. While searching for Rae, who has disappeared, he meets Gill, who informs him that Rae cheats on him whenever he is out of town. Ronnie attacks Gill, steals his truck, and continues searching for Rae.

In the morning, Lazarus frees Rae, having decided that he has no authority to pass judgment on her. Rae chooses to stay with Lazarus of her own will. Later, Rae and Lazarus take a trip into town, where Rae confronts her mother (Kim Richards) about the sexual abuse she suffered at the hands of her mothers partner. Meanwhile, Lazarus has formed a budding romance with the local pharmacist, Angela (S. Epatha Merkerson). He plays a blues concert at a local bar, which Rae attends. Ronnie spots Rae and follows her to Lazarus house. He confronts the pair with a pistol, but Lazarus talks him down and summons the pastor. Ronnie and Rae decide they are stronger together than apart and get married. While driving away, Ronnie suffers a panic attack and Rae begins to have one of her spells, but then they pull themselves together, and resolve to take care of each other.

==Cast==
* Samuel L. Jackson as Lazarus Redd
* Christina Ricci as Rae Doole
* Justin Timberlake as Ronnie Morgan
* John Cothran, Jr. as R.L.
* S. Epatha Merkerson as Angela
* David Banner as Tehronne
* Kim Richards as Sandy Doole
* Son House (archive footage) as Himself
* Neimus K. Williams as Lincoln James
* Michael Raymond-James as Gill Morton
* Adriane Lenox as Rose Woods
* Leonard L. Thomas as Deke Woods
* Jeff Pope as Batson
* Clare Grant as Kell

==Production==
For the film, Jackson spent six or seven hours a day for half a year learning how to play the blues guitar for several songs he plays throughout the film,       all of which derive from the repertoire of R. L. Burnside.    Ricci wore an actual   chain during filming      and ate only foods of no nutritional value to achieve a sickly appearance.     She told Entertainment Weekly that she remained scantily clad even when the cameras were not rolling:  "Sam   would be like, Put some clothes on! I was like, No, you dont understand. Im doing something important." 

==Reception==

===Critical response===
Reviews for the film were mixed.    As of August 18, 2011, Rotten Tomatoes reported a 66% "Fresh" rating based on 150 reviews, with an average rating of 6.3 out of 10    while Metacritic awarded the film an average rating of 52 out of 100 based on 34 reviews. 
 Ebert & Pulp Fiction (1994). Richard Roeper also gave the film a "thumb up" rating. Matt Glasby of Film4, however, awarded the film only 1 star out of 5, calling it a "pressure-cooked mess" that was "bad enough to make gums bleed".  
The film was also criticized by feminist activists for its portrayal of sexualized violence.    
Peter Travers of Rolling Stone declared the film the years Worst Soft-Core Sex on his list of the Worst Movies of 2007. 

===Box office===
  
During its March 2–4, 2007 opening weekend in the US the film earned $4 million, putting it in eighth place behind films including other new releases Wild Hogs and Zodiac (film)|Zodiac.      

== Marketing ==

In April 2008, Christina Ricci commented on the promotional poster for the film, criticizing it as exploitative of women:

: "The way that movie was marketed was probably one of the most disappointing and upsetting things thats ever happened to me in my career. I have no interest in exploiting women any further than theyve already been exploited...All they   cared about was college-age boys going to see it."   

==Soundtrack==
{{Infobox album  
| Name        = Black Snake Moan
| Type        = soundtrack
| Artist      = various
| Cover       = Black Snake Moan soundtrack cover.png
| Released    = January 30, 2007
| Recorded    = 
| Genre       = Blues
| Length      =  New West
| Producer    = various
| Reviews     = 
| Last album  = 
| This album  = 
| Next album  = 
}}
Black Snake Moan was released January 30, 2007 by New West Records featuring various artists including four tracks performed by Jackson himself. The 17 tracks cover classic to modern blues.

{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Artist
| total_length    = 
| writing_credits = 
| title1          = Opening Theme
| extra1          = Scott Bomar
| length1         =0:38
| title2          = Aint But One Kind of Blues
| extra2          =Son House 
| length2         =0:11
| title3          = Just Like a Bird Without a Feather
| extra3          =Samuel L. Jackson
| length3         =2:22
| title4          = When the Lights Go Out
| extra4          =The Black Keys
| length4         =3:13
| title5          = Standing in My Doorway Crying
| extra5          =Jessie Mae Hemphill
| length5         =4:40
| title6          = Chicken Heads Bobby Rush
| length6         =2:32
| title7          = Black Snake Moan
| extra7          =Samuel L. Jackson
| length7         =4:04
| title8          = Morning Train
| extra8          =Precious Bryant
| length8         =3:00
| title9          = The Losing Kind John Doe
| length9         =2:33
| title10         = Lord Have Mercy on Me
| extra10         =Outrageous Cherry
| length10        =3:04
| title11         = Ronnie and Raes Theme
| extra11         =Scott Bomar
| length11        =1:08
| title12         = The Chain
| extra12         =Scott Bomar
| length12        =2:50
| title13         = Alice Mae
| extra13         =Samuel L. Jackson
| length13        =3:48
| title14         = Stack-o-lee
| extra14         =Samuel L. Jackson
| length14        =3:30
| title15         = Poor Black Mattie
| extra15         =R. L. Burnside
| length15        =4:10
| title16         = Thats Where the Blues Started
| extra16         =Son House
| length16        =0:21
| title17         = Mean Ol Wind Died Down
| extra17         =North Mississippi Allstars
| length17        =7:31
}}

===Critical reviews===
Glenn Gaslin at Moving Pictures Magazine briefly reviewed and praised the album: "It should make anyone who loves the blues, er, happy." {{cite web
| url=http://www.movingpicturesmagazine.com/reviews/soundtracks/blacksnakemoan
| title=Black Snake Moan Soundtrack
| first=Glenn
| last=Gaslin
| publisher=Moving Pictures Magazine
| accessdate=2007-11-05
}} 

Chad Grischow at IGN reviewed the album at length, concluding with, "The album does an excellent job at capturing the sweaty underbelly of the southern blues scene, and is a recommended listen, even if not for the reasons you originally picked it up." {{cite web
| url=http://music.ign.com/articles/760/760860p1.html
| title=Various Artists - Black Snake Moan Soundtrack
| first=Chad
| last=Grischow
| date=2007-02-28
| work= IGN
| publisher = News Corporation 
| accessdate=2007-11-05
}} 

Samuel Flancher, writer of the extensive text Make My Snake Moan: 200 Haikus in Appreciation of Black Snake Moan, wrote: "I love this movie/It has it all, and much more/Oh Ricci my muse." 

On February 16, 2007, Sarah Linn of Sound the Sirens Magazine wrote in her final paragraph, {{cite web
| url=http://www.soundthesirens.com/articles/index.php?id=33,649,0,0,1,0
| title=V/A: Black Snake Moan
| first=Sarah
| last=Linn
| date=2007-02-16
| publisher=Sound the Sirens Magazine
| accessdate=2007-11-05 archiveurl = archivedate = 2007-10-23}}  

James B. Eldred at Bullz-Eye.com concluded his favorable review with, {{cite web
| url=http://www.bullz-eye.com/cdreviews/eldred/various_artists-black_snake_moan_soundtrack.htm
| title=Various Artists: Black Snake Moan Soundtrack
| first=James B.
| last=Eldred
| publisher=Bullz-Eye.com
| accessdate=2007-11-05
}}  

===Commercial rankings===
As of May 20, 2008, Amazon.com ranked the album #8,894 in its music category. When considered within the blues subcategories, it was #91 in Regional Blues and #13 in Delta Blues. {{cite web
| url=http://www.amazon.com/dp/B000L211NC/
| title=Black Snake Moan  
| publisher=Amazon.com
| accessdate=2008-05-20
}} 

==Notes==
  
 

==References==

General sources:
*   (cited 2 September 2006)
*   (cited 2 September 2006)
*   (cited 29 November 2006)
*  

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 