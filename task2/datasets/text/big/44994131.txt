Muriyada Mane
{{Infobox film 
| name           = Muriyada Mane
| image          =  
| caption        = 
| director       = Y. R. Swamy
| producer       = G H Veeranna C R Basavaraju S Gurunath
| writer         = M. S. Solaimalai
| screenplay     =  Rajkumar Udaykumar Balakrishna
| music          = Vijaya Krishnamurthy
| cinematography = R Madhu
| editing        = R Hanumatha Rao
| studio         = Karnataka Films
| distributor    = Karnataka Films
| released       =  
| country        = India Kannada
}}
 1964 Cinema Indian Kannada Kannada film, Balakrishna in lead roles. The film had musical score by Vijaya Krishnamurthy.   The film was a remake of Tamil film Bhaaga Pirivinai.

==Cast==
  Rajkumar
*Udaykumar
*K. S. Ashwath Balakrishna
*Narasimharaju Narasimharaju in Guest Appearance
*Srikantaswamy
*Maheshwaraiah
*Narayana
*R. N. Magadi
*M. Pandaribai
*Jayanthi
*Papamma
*Jr Revathi
*Vanishree
*Indrani
 

==Soundtrack==
The music was composed by Vijaya Krishnamurthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Mathiheena Naanade || PB. Srinivas || Ku. Ra. Seetharam Shastry || 03.49
|-
| 2 || Nammoora Chennayya || Ghantasala, L. R. Eswari || Ku. Ra. Seetharam Shastry || 03.23
|- Susheela || Ku. Ra. Seetharam Shastry || 03.07
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 

 