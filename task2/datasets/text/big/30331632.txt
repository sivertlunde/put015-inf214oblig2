Jeevan Yudh
{{Infobox film
 | name = Jeevan Yudh
 | image = JeevanYudh.jpg
 | caption = Promotional Poster
 | director = Partho Ghosh
 | producer = Piyush Chakravorthy
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty  Raakhee Jayapradha Atul Agnihotri  Mamta Kulkarni
 | music = Nadeem-Shravan
 | lyrics =
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  March 28, 1997
 | runtime = 140 min.
 | language = Hindi Rs 3 Crores
 | preceded_by = 
 | followed_by = 
}}
 1997 Hindi Indian feature directed by Rami Reddy, Bengali version release under the title Jiban Yuddha.

==Plot==

Jeevan Yudh  is an Action flick from Mithun-Partho Ghosh team. The film was announced after the Super Success of Dalaal, but failed to live up to the expectations. The Bengali version had a Moderate run.

==Cast==

*Mithun Chakraborty	 ... 	 Inspector Dev Prakash
*Raakhee	... 	Mrs. Rai
*Jayapradha         ... 	Rani
*Atul Agnihotri	... 	Rohit Rai
*Mamta Kulkarni	... 	Kajal Choudhry
*Shakti Kapoor	... 	Ranis accomplice
*Mohan Joshi	... 	Gajraj Choudhry
*Alok Nath		... 	Vasudev Rai Rami Reddy	         ... 	Madan (as Rami Reddi)
*Girja Shankar	... 	Inspector Ajay Kumar

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Kameez Meri Kaali"
| Kavita Krishnamurthy, Ila Arun
|-
| 2
| "Raja Kaise Bethun Tohari" Shravan Rathod
|-
| 3
| "Sharm Aane Lagee"
| Kavita Krishnamurthy,
|-
| 4
| "Sun Sajana Tere Bin"
| Babul Supriyo, Alka Yagnik
|-
| 5
| "Tu Hai Meri Dil Ka"
| Kavita Krishnamurthy, Nachiketa Chakraborty
|-
| 6
| "Zindagi Ko Guzarne Le Liye"
| Pankaj Udhas, Alka Yagnik
|-
| 6
| "Ye Resham Ki Saadi"
| Kumar Sanu
|}

==References==
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Jeevan+Yudh

==External links==
*  

 
 
 
 
 
 
 

 