Round Midnight (film)
 
{{Infobox film
| name           = Round Midnight
| image          = Roundmidnightposter.jpg
| caption        = Theatrical release poster by Steven Chorney
| image_size     = 270px
| director       = Bertrand Tavernier
| producer       = Irwin Winkler
| screenplay     = David Rayfiel Bertrand Tavernier Colo Tavernier (French translation)
| based on       =  
| starring       = Dexter Gordon François Cluzet Gabrielle Haker Herbie Hancock Martin Scorsese Philippe Noiret
| music          = Herbie Hancock (original music)
| studio         = Little Bear PECF
| distributor    = Warner Bros.
| released       =   (Toronto Film Festival|TIFF)   (United States)
| runtime        = 133 min.
| country        = United States France
| language       = English
| budget         =
| gross          = $3,272,593 (Domestic) 
}} French musical musical drama film directed by Bertrand Tavernier and written by Tavernier and David Rayfiel. It stars Dexter Gordon, François Cluzet and Herbie Hancock. Martin Scorsese, Philippe Noiret and Wayne Shorter appear in cameos.  
 composite of Paris jazz scene of the 1950s.   

Dexter Gordon was nominated for the Academy Award for Best Actor in a Leading Role and won a Grammy for the films soundtrack entitled The Other Side of Round Midnight in the category for Best Instrumental Jazz Performance, Soloist.   and The Other Side of Round Midnight.

== Synopsis == French graphic alcohol abuse. As he succeeds, the budding friendship they develop changes their lives forever.

== Cast ==
* Dexter Gordon ... Dale Turner
* François Cluzet ... Francis Borler
* Gabrielle Haker ... Berangere
* Sandra Reaves-Phillips ... Buttercup
* Lonette McKee ... Darcey Leigh
* Christine Pascal ... Sylvie
* Herbie Hancock ... Eddie Wayne
* Bobby Hutcherson ... Ace
* Pierre Trabaud ... Franciss Father
* Frédérique Meininger ... Franciss Mother
* Hart Leroy Bibbs ... Hershell
* Liliane Rovère ... Madame Queen
* Ged Marlon ... Beau
* Benoît Régent ... Psychiatrist
* Victoria Gabrielle Platt ... Chan Turner Arthur French ... Booker John Berry ... Ben
* Martin Scorsese ... Goodley
* Philippe Noiret ... Redon
* Alain Sarde ... Terzian
* Eddy Mitchell ... Livrogne au bar du Blue Note
* Billy Higgins ... Drums (Blue Note, Davout Studio)
* Éric Le Lann ... Trumpet (Blue Note) John McLaughlin ... Guitar (Blue Note)
* Pierre Michelot ... Bass (Blue Note)
* Wayne Shorter ... Tenor Saxophone (Blue Note), Soprano Saxophone (Davout Studio, Lyon)
* Ron Carter ... Bass (Davout Studio, New York)
* Palle Mikkelborg ... Trumpet (Davout Studio)
* Mads Vinding ... Bass (Davout Studio, Lyon)
* Cheikh Fall ... Percussion (Lyon)
* Michel Pérez ... Guitar (Lyon) Tony Williams ... Drums (Lyon, New York)
* Freddie Hubbard ... Trumpet (New York)
* Cedar Walton ... Piano (New York)

== Production == New York. It was produced by Irwin Winkler.
 John McLaughlin, and Wayne Shorter, among others who play the music live throughout the film. In addition to the musicians, the movie stars François Cluzet, Gabrielle Haker, Sandra Reaves-Phillips, Lonette McKee, and Christine Pascal.

== Soundtrack ==
  Round Midnight and The Other Side of Round Midnight - released under Dexter Gordons name and featuring his last recordings, although he does not appear on all tracks. Both albums were produced and arranged by Hancock.

== Reception ==
Round Midnight received excellent reviews from critics, and it holds a 100% rating at Rotten Tomatoes.

=== Awards === Best Actor Best Music, Original Score.

== References ==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 