Kartavya (2003 film)
{{Infobox film
| name           = Kartavya 
| image          = kartavya.jpeg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Swapan Saha
| producer       = Pijush Saha Subrato Saha Ray
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Prosenjit Chatterjee Rachana Banerjee Abhishek Chatterjee Tapas Pal Locket Chatterjee Dulal Lahiri
| music          = Ashok Bhadra
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2003
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film directed by Swapan Saha and produced by Pijush Saha and Subrato Saha Ray. The film features actors Prosenjit Chatterjee, Rachana Banerjee, Abhishek Chatterjee, Tapas Pal, Locket Chatterjee, Dulal Lahiri in the lead roles. Music of the film has been composed by Ashok Bhadra.    The film was a remake of Tamil film Samuthiram.

== Plot ==
The movie begins with a marriage scene in which the bridegroom disagrees to marry due to non-payment of dowry by the bride`s father. In spite of his brothers request, he remains stubborn. He even throws away the garland. Raja(Prosenjit Chatterjee)    comes to the scene and tries to convince the bridegroom to marry. A challenge for test of brotherhood befalls Raja calls his two brothers Rana (Lokesh) and Rakesh(Abhishek Chatterjee) and orders them to drink poison. They gladly do so. It is revealed that the water in the glass contained honey instead of poison. The groom apologises and agrees to marry. All three brothers are worried about their only sister-Poojas (Moumita) marriage and life. But Pooja convinces them to marry. All of them visit Anjali`s house and the marriage is settled. Nahim Ghoshal (Dulal Lahiri) buys a crown for goddess Lali made of pure gold which he wishes to offer for the god. He is stopped by Rakesh and a crash takes place. Mahim Ghoshal is insulted and agrees to take revenge. Raja later drives to Mahims house for apology on behalf of his brother. Ghosal cleverly places a proposal of marriage between his son and Pooja. Raja ignorantly agrees; as a dowry Mahim Ghosal makes Raja transfer all his property in the name of Pooja, there by making them beggars. The film advances with the display of repeated insults and plans by Mahim Ghosal towards Rajas family as revenge. He even plots to create mental tension among the family members of Rajas family. But strong bondage and dedication among the three brothers prevents the fury. On the other hand, Mahim Ghosal and his son torture Pooja brutally, but keeping Pooja`s future in mind, Raja forgives them repeatedly. Pooja is expecting a baby but her husband even tries to destroy it. There is a clash and Raja, Rana and Rakesh carry Pooja to the hospital. Pooja has a boy. Mahim Ghosal and his son apologises at the hospital. They are forgiven and they live together ever after. The title Kartavya is suitable as it reflects the duty of a brother towards his sister.

== Cast ==
* Prosenjit Chatterjee
* Rachana Banerjee
* Abhishek Chatterjee
* Tapas Pal
* Locket Chatterjee
* Dulal Lahiri
* Kalyani Mondal
* Moumita Chakraborty
* Badsha Moitra
* Lokesh Ghosh
* Manjushree

== References ==
 

==External links==
*  
 

 
 
 
 


 