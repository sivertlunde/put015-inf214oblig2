Ingenious (2009 American film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Ingenious
| director       = Jeff Balsmeyer
| producer       = Mike Cram Brian Neufang
| screenplay     = Mike Cram
| starring       = Dallas Roberts Jeremy Renner Ayelet Zurer Marguerite Moreau
| music          = Nicola Freegard   Howe Gelb   Gavin Whalen  
| cinematography = Geoffrey Hall
| editing        = Suresh Ayyar Marcus DArcy Gavin Whalen 
| studio         = Arriba 
| distributor    = Lionsgate
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
}}
Ingenious is a 2009 American film, previously titled Lightbulb.  It is a rags-to-riches story of two friends, a small-time inventor and a sharky salesman, who hit rock bottom before coming up with a gizmo that becomes a worldwide phenomenon.    It is based on the true story of some friends who are trying to come up with an invention, before hitting on an idea.

==Cast==
*Dallas Roberts as Matt, an inventor
*Jeremy Renner as Sam, Matts lifelong friend and business partner
*Ayelet Zurer as Gina, Matts wife
*Marguerite Moreau as Cinda 
*Amanda Anka as Louisa
*Richard Kind as Newkin
*Eddie Jemison as Bean Judith Scottas Rita
*Debby Rosenthal as Brenda
*Michael Kagan as Mort
*Rob Brownstein as Kent
*Vince Grant as Randall
*François Chau: Mr. Chow

==Production==
The film was shot on location in Tucson, Arizona and Hong Kong.  It premiered at the 2009 Santa Barbara International Film Festival.

==See also==
*List of films shot in Arizona

==References==
 

==External links==
*  

 


 