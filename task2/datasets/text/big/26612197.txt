Mulla (film)
 
{{Infobox film
| name           = Mulla
| image          = Mulla (film).jpg
| alt            = 
| caption        = 
| director       = Lal Jose
| producer       = Shebin Backer    Jemi Hameed   Sagar Shereef   Sundararajan
| writer         = M. Sindhuraj Dileep  Sivaji Anoop Chandran Vidyasagar
| cinematography = Vipin Mohan
| editing        = Ranjan Abraham   
| studio         = 
| distributor    = 
| released       =  
| runtime        = 138 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Mulla is a 2008 Malayalam film made in Cinema of India|India, by Lal Jose starring Dileep (actor)|Dileep, Meera Nandan and Biju Menon. The movie become a hit at the Box office.

==Plot==
Lachi is a hard-working 19-year-old who supports her family after her fathers mysterious death. While she was traveling on a train, Mulla (Dileep (actor)|Dileep) and his gang enter. They argue over silly issues, and he discovers a baby in a bag on the train. They try to find the babys parents but fail. Mulla decides to adopt the child, and slowly Mulla and Lachi become lovers. 

CI Bharathan (Saiju Kurup), who murdered Lachis father and the babys parents, wants to break up the two to keep his secrets. Finally, Mulla and Bharathan get into a fight, and Mulla kills Bharathan. However, Lachi tells the police that she committed the murder and goes to prison. After five years, when she returns, she finds that Mulla has lost one leg in a fight, between the colony residents and the police. The colony residents had started a new business, and they all are having a good life now. The only thing they had lacked is Lachis presence. Lachi and Mulla finally unite.

==Cast== Dileep ...  Mulla 
*Meera Nandan ...  Lachi 
*Sruthi Menone ...  Tamil Girl  Ashok ... Kattuda song Cameo Bhavana ...  Mad girl 
*Biju Menon...  Ambi 
*Salim Kumar ...  Thotti Sasi 
*Saiju Kurup ...  CI Bharathan 
*Chali Pala  
*Rizabawa ...  Venu 
*Sukumari   
*Shivaji Guruvayoor.... Bhadran
*Vanitha Krishnachandran ...  Lachis mother 
*Suraj Venjaramood ...  Bijumon
*Ajaykumar  
*Sudheer Karamana  
* Joju George
*Mala Aravindan   
*Reena Basheer ...  Malathi 
*Anoop Chandran ...  Idiyappam

==Soundtrack==
{{Infobox album |  
| Name       = Mulla
| Type       = Soundtrack Vidyasagar
| Cover      = 
| Released   = 2008
| Recorded   = 
| Genres     = World Music
| Length     =
| Label      =  Vidyasagar
| Last album = Jeyam Kondaan (2008)
| This album = Mulla (2008)
| Next album = Kuruvi (2008)
}}
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! # !! Title !! Singer(s) 
|- Sujatha
|-
| 2 || Ee Ravil || Rahul Nambiar, Reshmi 
|-
| 3 || Kannin Vaathil (Female)|| Gayathri 
|-
| 4 || Katteda || Tippu (singer)|Tippu, Manicka Vinayagam, Gemon, Rimi Tomy, Reshmi
|-
| 5 || Theme Song || instrumental 
|-
| 6 || Kannin Vaathil (Male)|| Devanand 
|-
| 7 || Aarumukhan || Rimi Tomy
|}
==External links==
*  
* http://www.thehindu.com/todays-paper/tp-national/tp-kerala/cast-camera-prop-up-mulla-film-review/article1231758.ece
* http://www.rediff.com/movies/2008/mar/28ssm.htm - Rediff Review
* http://popcorn.oneindia.in/title/532/mulla.html

 

 
 
 
 