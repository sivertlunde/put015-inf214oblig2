Johnny O'Clock
{{Infobox film
| name           = Johnny OClock
| image          = File:Film_Poster_for_Johnny_OClock.jpg
| image_size     = 
| alt            =
| caption        = Theatrical release poster
| director       = Robert Rossen
| producer       = Edward G. Nealis Jerry Giesler
| screenplay     = Robert Rossen
| story          = Milton Holmes
| narrator       =
| starring       = Dick Powell Evelyn Keyes Lee J. Cobb
| music          = George Duning
| cinematography = Burnett Guffey
| editing        = Al Clark Warren Low
| studio         = J.E.M. Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Jeff Chandler making his film debut in a small role.

==Plot==
Johnny OClock (Dick Powell) is a junior partner in a posh casino with Guido Marchettis (Thomas Gomez). Complicating their longtime working relationship is Guidos wife Nelle (Ellen Drew), who is still in love with former boyfriend Johnny. She gives Johnny an expensive custom pocket watch, the twin of a birthday present she gave her husband, except Johnnys has a romantic engraving on the back.

Johnny gives the watch, along with a rejection note, to Harriet Hobson (Nina Foch), a hat-check girl at the casino, to return to Nelle. Harriet, however, apparently commits suicide using gas. Her sister Nancy (Evelyn Keyes) shows up to find out what happened. She becomes attracted to Johnny. They eventually learn from Police Inspector Koch (Lee J. Cobb) that Harriet was killed by poison.

Harriet was dating Chuck Blayden (Jim Bannon), a crooked cop who is trying to persuade Guido to let him take Johnnys place. When Blayden also turns up dead, Koch suspects that either Johnny or Marchettis is responsible.

Though Johnny tries to resist, little by little, he falls for Nancy. When Koch shows both Johnny and Marchettis Johnnys watch and note, Johnny tells Nancy their relationship is through and takes her to the airport. As he is driving away, however, he narrowly survives a drive-by shooting, and Nancy realizes he was only trying to protect her. She refuses to leave him.

Johnny decides to flee to South America with Nancy, but not before brazenly cashing in his share of the casino. Marchettis pulls out a gun when Johnnys back is turned. They shoot it out; Marchettis is killed and Johnny wounded. Afterward, Nelle offers to testify it was self-defense, but only if he will come back to her. He refuses, so she tells Koch it was cold-blooded murder. Johnnys first instinct is to run away, but Nancy convinces him to give himself up.

==Cast==
* Dick Powell as Johnny OClock 
* Evelyn Keyes as Nancy Hobson 
* Lee J. Cobb as Inspector Koch
* Ellen Drew as Nelle Marchettis 
* Nina Foch as Harriet Hobson 
* Thomas Gomez as Pete Marchettis  John Kellogg as Charlie
* Jim Bannon as Chuck Blayden
* Mabel Paige as Slatternly Woman Tenant Phil Brown as Phil, Hotel Clerk Jeff Chandler as Turk

==Reception==

===Critical response===
Time Out film guide notes "Despite good performances and fine camerawork from Burnett Guffey, Rossens first film as director is a disappointingly flat thriller...Since they remain totally unmemorable (through no fault of the actors concerned), the subsequent action tends to become little more than a sequence of events mechanically strung together." 

The staff at Variety (magazine)|Variety magazine gave the film kudos, writing, "This is a smart whodunit, with attention to scripting, casting and camerawork lifting it above the average. Pic has action and suspense, and certain quick touches of humor to add flavor. Ace performances by Dick Powell, as a gambling house overseer, and Lee J. Cobb, as a police inspector, also up the rating...Although the plot follows a familiar pattern, the characterizations are fresh and the performances good enough to overbalance. Dialog is terse and topical, avoiding the sentimental, phoney touch. Unusual camera angles come along now and then to heighten interest and momentarily arrest the eye. Strong teamplay by Robert Rossen, doubling as director-scripter, and Milton Holmes, original writer and associate producer, also aids in making this a smooth production." 

Film critic Bosley Crowther, gave the film a mixed review, criticizing it for slow pacing, writing, "But the slowness and general confusion of the plot for two-thirds of the film does not make for notable excitement, and the shallowness of the mystery as to whos doing all the killing relieves it of any great suspense. It is mainly a matter of watching Mr. Powell go through his paces stylishly while a large cast of actors and actresses give him customary support. Evelyn Keyes plays the good little lady who brings out the best in him and Ellen Drew is the sleek and slinky vixen who gets him into jams. Thomas Gomez is oily as the villain and Lee J. Cobb does another able tour as a weary police inspector who finally closes the case. A great deal of drinking, and smoking is done by all concerned." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 