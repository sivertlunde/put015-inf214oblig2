Hatim Tai (film)
 
{{Infobox film
| name           = Haatim Tai 
| image          = 
| caption        = 
| director       = Babubhai Mistry
| producer       = Ratan Mohan
| written by     = P.D. Mehra
| starring       = Jeetendra Sangeeta Bijlani Sonu Walia Satish Shah Amrish Puri Alok Nath Raza Murad
| music          = Laxmikant Pyarelal
| cinematography = K. Vaikunth
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 125 minutes 
| country        = India
| language       = Hindi

}}

Hatim Tai is a 1990 Bollywood fantasy film directed by Babubhai Mistri and starring Jeetendra, Sangeeta Bijlani, Satish Shah, and Amrish Puri in lead roles. The films plot is based on the true life story of Hatim al-Tai who was a famous Arabian poet from the Arabian tribe|Tai Arabian tribe. He is an icon to Arabs up till the present day for being extremely generous and brave.

==Cast==
* Jeetendra as Hatim al-Tai
* Sangeeta Bijlani as Gulnar Pari
* Satish Shah  as Nazrul
* Alok Nath as Gulnars Father
* Sonu Walia
* Amrish Puri  as Jadugar Kamlaak
* Rajesh Vivek
* Vijayendra Ghatge
* Goga Kapoor

== Plot==
Hatim Tai is the legendary chief of a small district. He is Known for his generosity, wisdom and courage. Hatim learns that one of the local girls in his town has a curse put on her - whomever marries her, the husband will die. In order to break this curse, Hatim agrees to take part in 7 quests. All 7 quest are dangerous but have a moral. If he completes all 7 then he will also release the fairy who has turned into a stone. Hatim and his friend (Satish Shah) embark on a fun but dangerous quest to break the curse once and for all.

==Story==
The film begins with a baby boy being born into the household of a rich Muslim King. The Prince is named Hatim al-Tai and is a very generous and humble Prince when he reaches adulthood.

As the Prince begins distributing gold to poor citizens in his city, a Princess named Mariam comes to his door step to ask him for help. She tells him that she is in love with a Prince named Munir and they want to get married, but her father will not let her marry anyone as long as she lives. Hatim is shocked at her fathers decision and realizes that there must be a reason behind it.

Hatim, his friend Nazrul, Mariam and Munir go to Mariams palace to ask the King why he will not let his daughter marry anyone. The King tells them of an event that occurred in the past, which leads to the reason why Mariam cannot get married.

One day as the King was sleeping in his luxurious room, a fairy named Gulnar Pari flew into his room enchanted by the beautiful chandeliers in his room. Upon seeing the beautiful Fairy, the King is aroused by her beauty and attempts to rape her, but fails as Gulnar Pari is cursed and slowly begins turning into stone to save her honour. She gives him a curse that whoever his daughter Mariam  marries, will die on the wedding night and Mariam will turn into stone. As Gulnar Pari begins turning into stone, the King begs for forgiveness. She pities him and leaves seven questions he need or someone else has to answer. All the questions need to be answered in order to get rid of both Gulnar Pari and Mariams curses.

When the Kings story ends, he shows Hatim, Nazrul, Mariam and Munir the beautiful stone statue of Gulnar Pari. Hatim decides to seek answers to the seven questions. The King warns Hatim that the questions are extremely difficult and he will have to travel to distant and dangerous places. Hatim promises that with the company of his friend Nazrul, he will answer the seven questions.

Hatim and Nazrul begin to travel seeking answers to the seven questions. Slowly they start solving these questions. Everytime a question is answered, a part of Gulnar Pari comes back to life.

While seeking the answers to the questions, Hatim comes across a fairy that looks exactly like Gulnar Pari. She reveals that she is Gulnar Paris twin sister, Pari Bano. She thanks Hatim for his help and they both fall in love.

Hatim and Nazrul begin completing the questions with the help of Gulnar Pari and succeed in completing all the difficult questions.

Gulnar Pari comes back to life and Mariams curse is lifted as well. Hatim, Pari Bano, Mariam and Munir celebrate and begin preparing for their weddings.

==References==
# ^ Seven questions asked in movie Hatim Tai(1990) https://plus.google.com/photos/114557381673203848364/albums/5939335788974583713?authkey=CMCp5oHS3cGQkAE
# ^ Biography of Sheikh Bahi Dadiza (Arabic)
# ^ Kitab al-Aghani by Abu al-Faraj al-Isfahani
# ^ E. J. Brills First Encyclopaedia of Islam, 1913–1936
# ^ http://persian.packhum.org/persian/pf?file=08501030&ct=0
# ^ http://books.google.com/books?id=s_4KV4Ixq4IC&pg=PA132&dq=Hatim+Tai#PPA132,M1
# ^ Persian Portraits: A Sketch of Persian History, Literature and Politics by F. F. Arbuthnot
# ^ http://www.imdb.com/title/tt0242509/plotsummary?ref_=tt_ov_pl

==External links==
*  

 
 