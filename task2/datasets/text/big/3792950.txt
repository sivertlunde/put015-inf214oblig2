Permanent Vacation (film)
{{Infobox film
| name           = Permanent Vacation
| image          = Permanent Vacation.jpg
| caption        =
| director       = Jim Jarmusch
| producer       = Jim Jarmusch
| writer         = Jim Jarmusch
| starring       = Chris Parker
| music          = Jim Jarmusch John Lurie
| cinematography = Tom DiCillo James A. Lebovitz
| editing        = Jim Jarmusch
| distributor    = Cinesthesia
| released       =  
| runtime        = 75 min
| country        = United States
| language       = English
| budget         = United States dollar|US$12,000
}} 1980 film directed, written and produced by Jim Jarmusch. It was the directors first release, and was shot on 16 mm film shortly after he dropped out of film school. This film is often credited as the birth of the directors original style and character schemes. The film won the Josef von Sternberg Award at the 1980 Mannheim-Heidelberg International Filmfestival.

==Plot== New York atmosphere and is confronted by a number of intriguing characters as he ponders the questions of life, and searches for a better place.

==Cast==
* Richard Boes – War vet
* Ruth Bolton – Mother
* Sara Driver – Nurse
* María Duval – Latin girl
* Frankie Faison – Man in lobby
* Jane Fire – Nurse
* Suzanne Fletcher – Girl in car
* Leila Gastil – Leila
* Chris Hameon – French traveller
* John Lurie – Sax player Eric Mitchell – Car fence
* Chris Parker – Allie
* Lisa Rosen – Popcorn girl
* Felice Rosser – Woman by mailbox
* Evelyn Smith – Patient
* Charlie Spademan – Patient

==Production==
This film was also the first that musician John Lurie, a friend of Jarmusch, acted in and composed music for.

Jarmusch is also credited as helping with the music.

==Availability==
The film was released by the Criterion Collection as a special feature on the DVD for Jarmuschs Stranger than Paradise on September 4, 2007. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 