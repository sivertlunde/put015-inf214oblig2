Zvenigora
{{Infobox film
|  name           = Zvenigora
|  image          = Zvenigora poster.jpg
|  caption        = Film poster
|  director       = Alexander Dovzhenko 
|  producer       =  Maike "Mike" Yurtyk (Yuri Tiutiunnyk) Alexander Dovzhenko
|  starring       = Semyon Svashenko Nikolai Nademsky Georgi Astafyev Les Podorozhnij
|  music          = 
|  cinematography = Boris Zavelev
|  editing        = Alexander Dovzhenko  VUFKU
|  distributor    = Mosfilm
|  released       =  
|  runtime        = 91 min.
|  country        = Soviet Union Russian intertitles
|  budget         =  
}} Maike "Mike" Yurtyk (Yuri Tiutiunnyk), however, eventually, Dovzhenko strongly modified the script himself and took the names of Johansen and Tyutyunnyk off the film. 

Regarded as a silent revolutionary epic, Dovzhenkos initial film in his "Ukraine Trilogy" (along with Arsenal (film)|Arsenal and Earth (1930 film)|Earth) is almost religious in its tone, relating a millennium of Ukrainian history through the story of an old man who tells his grandson about a treasure buried in a mountain. The film mixes fiction and reality. Although Dovzhenko referred to Zvenigora as his "party membership card".  Relationship between an individual and the nature is the main theme of the film, which is highly atypical of the Soviet cinema of the end of the 1920s, mostly influenced by the avantguarde. Dovzhenko states that full submission made the humankind forceless in front of the nature, and understanding of the nature is required to make progress. For him, the October Revolution carries such understanding. 

At the time of release, the film was noticed by the media, but generally regarded as not conforming with the Soviet style esthetics. In 1927, even before the release, the Kino newspaper sharply criticized the screenplay calling it "bourgeoise" and "nationalistic". 

==References==
 

==External links==
* 
* 
* 
*Ray Uzwyshyn  

 

 
 
 
 
 
 
 
 