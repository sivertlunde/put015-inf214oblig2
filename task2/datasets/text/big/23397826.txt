Jail (2009 film)
{{Infobox film
| name           = Jail
| image          = Jail Movie Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Madhur Bhandarkar
| producer       = 
| screenplay     = Madhur Bhandarkar Anuraadha Tewari Manoj Tyagi
| story          = Madhur Bhandarkar Anuraadha Tewari Manoj Tyagi
| starring       = Neil Nitin Mukesh Mugdha Godse Manoj Bajpai Arya Babbar
| music          = Sharib Sabri Toshi Sabri Shamir Tandon
| cinematography = Kalpesh Bhandarkar
| editing        = Devendra Murdeshwar
| studio         = 
| distributor    = Percept Picture Company Bhandarkar Entertainment
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} Hindi drama film directed by Madhur Bhandarkar and starring Neil Nitin Mukesh, Arya Babbar, Mugdha Godse and Manoj Bajpai. 

==Synopsis==
Jail tells the story of Parag Dixit (Neil Nitin Mukesh) who is having a peaceful life with his girlfriend Mansi, played by Mugdha Godse. But his life takes a turn and he ends up in jail due to a false drugs case in which he is implicated although his friend is responsible for the fiasco. Shortly after getting promoted as Regional Manager of Max Finance, Parag Dixit gets arrested in Bandra for Firing at Police, and Possession of Narcotics. His associate, Keshav, is grievously injured, and is hospitalized in a coma-like condition. Parags widowed mother, Alka, and Air Hostess girlfriend, Manasi Pandit, retain Advocate Harish Bhatiya to represent him at a preliminary bail hearing, but the Judge denies bail. Housed in an overcrowded barrack, with barely room to move, Parag meets with a variety of people - both convicted and awaiting trial - including Abdul Ghani, Kabir Malik, Galib Suratwala, Nawab, Bababhai, etc. Parag again appears in Court after a charge-sheet is filed, but again, due to the seriousness of the offenses, fear of tampering with evidence and witnesses, he is denied bail. Several months later, Keshav is now deceased - and Parag, embittered by abuse, loss of faith in an overburdened system - with overwhelming evidence against him - faces trial as the only accused. The film is a take on the cruel realities faced by prisoners in Indian jails by focusing on Parags life in the jail. It was rumored that Neil will appear complete nude in a torturing scene. Manoj Bajpai plays another inmate in the jail.  

Neil was in various controversies regarding the nudity scene in the film. Apparently this scene had to be blurred to appease the censor board. 

==Cast==

* Neil Nitin Mukesh as Parag Dixit
* Mugdha Godse as Maansi Pandit
* Manoj Bajpai as Nawaab
* Arya Babbar as Kabir Malik
* Chetan Pandit as Jailor Arvind Joshi
* Ghanshyam Garg as Piloo Yadav Rahul Singh as Abdul Ghani
* Sayali Bhagat in a Special Appearance
* Kaveri Jha as Sabina
* Mukesh Tyagi as a CEO
* Sandeep Mehta as Harish Bhatia (Parags lawyer)
* Navni Parihar as Parags Mother
* Ali Quli Mirza as Joe DSouza
* G K Desai Cricket Bookie Desai Bhai
* Jignesh Joshi as Keshav Rathod
* Atul Kulkarni as Parag Dixits next lawyer
* Nassar Abdulla as a Judge
* Raj Nair as Anna
* Ashok Samarth as DCP Nagesh Patil
* Manish Mehta as Galib Suratwala
* Vinay Arvind Lad as Vinay Lad
* Javed Rizvi

==Soundtrack==
The music of the film is composed by Sharib Toshi and Shamir Tandon  and the lyrics are penned by Ajay Garg, A. M. Turaz, Sandeep Nath and Kumaar The music was released on October 3, 2009. 

{{Infobox album  
| Name        = Jail
| Type        = Soundtrack
| Artist      = Shabir Tondon, Toshi Sabri
| Cover       =
| Released    =  October 03, 2009
| Recorded    = Feature film soundtrack
| Length      = 36:15
| Label       = Super Cassettes Industries (T-series Music Records)
| Reviews     =
| This album  = Jail (2009)
| Next album  =
}}

===Track listing===
{|border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
! Track !! Song !! Artist(s) !!  Composer !! Duration
|-
| 01
|Saiyaan ve Toshi
| Sharib, Toshi
| 04:08
|- 02
|Milke Yun Laga
| Sharib
| Sharib, Toshi
| 04:36
|- 03
|Bareily Ke Bazzar Mein
| Sonu Kakkar
| Shamir Tandon
| 04:12
|-
| 04
|Daata Sun Le
| Lata Mangeshkar
| Shamir Tandon
| 05:13
|-
| 05
|Saiyaan Ve  
| Toshi, Sharib, Neil Nitin Mukesh
| Sharib, Toshi
| 04:30
|-
| 06
|Bareily Ke Bazzar Mein  
| Sonu Kakkar
| Shamir Tandon  
| 04:44
|-
| 07
|Sainya Ve  
| Toshi, Sharib, Neil Nitin Mukesh
| Sharib, Toshi   
| 03:48
|-
| 08
|Daata Sun Le  
| Lata Mangeshkar
| Shamir Tandon
| 04:58
|}

==Reviews==
* 
*  

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 