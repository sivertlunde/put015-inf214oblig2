Banking Hours 10 to 4
{{Infobox film
| name           = Banking Hours 10 to 4
| image          = BankingHours10to4.jpg
| caption        = Promotional Poster
| director       = K. Madhu
| producer       = Stephen Pathickal
| writer         = Sumesh-Amal
| starring       = {{Plainlist|
* Anoop Menon Jishnu
* Kailash
* Ashokan
* Shankar
* Meghna Raj
}}
| music          = Rajamani  (score) 
| cinematography = Saloo George
| editing        = P. C. Mohan
| studio         = Lemo Films
| distributor    = Lemo Films through Vaishaka Cynyma
| released       =  
| runtime        = 113 minutes
| country        = India
| language       = Malayalam
| budget         = 
}} 2012 Malayalam Indian feature directed by Kailash in the lead roles.

== Plot ==
Banking Hours 10 to 4 is a suspense thriller happening in a bank as a person is killed inside the bank premises and the suspense is revealed within the banking hours of 10 to 4. Anoop Menon plays the role of an investigation officer.

== Cast ==
 
* Anoop Menon as Sravan Varma I.P.S
* Meghna Raj as Revathy Varma I.P.S
* Mithun Ramesh as Vishnu Ramachandran Jishnu as Avinash Sekhar Kailash as Ajay Vasudevan Raghavan as Lakshmi Priyas father
* Swasika as Anjana Shekhar Shankar as Fernandez Ashokan as Salman Azharudeen / Priest
* Tini Tom as Idikkula Stephen
* Nishanth Sagar Arun
* Kiran Raj
* Master Arun Munna as Rahul Krishna as Roy Mathew,the Bank Manager
* Sudheesh as Ravi Kumar, the peon
* Sarin H. Nair as one of the IT Professionals
* Vishnupriya as Pooja
* Sreelatha
* Chali Pala as Sarath Babu
* Ambika Mohan as Bhavani,Ajay Vasudevans mother
* Deepika Mohan
* Shafna as Merrin Fernandez  Sarayu
* Renjith
* Lakshmipriya
* Vijay Menon as Dr. John
* Sathaar as Vasudevan Pillai
* Majeed
* Midhu Ramesh
* Manoj Paravoor
* Roshan
* Biyon
* Surabhi as Priya Lal, the Receptionist
 

== Reception ==
The movie opened from mixed to negative reviews from the critics and the audience alike. The movie was mainly criticized for the slow direction from K Madhu. Sify.com gave an "average" verdict and said "Banking Hours 10-4 may leave the discerning viewers with many doubts but could well be an entertaining one, if you dont dig too deep or go without much expectation."    Nowrunning also gave a negative review and gave 1.9 stars out of 5 and added "Banking Hours lacks that vital spark that should keep the spirit up in whodunits."   

== References ==
 

== External links ==
*  

 

 
 
 
 
 