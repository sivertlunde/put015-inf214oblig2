Super High Me
{{Infobox Film
| name           = Super High Me
| image          = Super High Me.JPG
| caption        = Original film poster
| director       = Michael Blieden
| producer       = Alex Campbell
| writerrt         =
| starring       = Doug Benson
| cinematography =
| editing        =  
| distributor    = Red Envelope Entertainment Sony Music Japan (Japan) Revolver Entertainment (UK) Eagle Entertainment (Australia)
| released       = April 20, 2007
| runtime        = 98 minutes English 
| budget         =  $700,000 (est.)
| preceded_by    = 
| followed_by    = 
}} cannabis for 30 days. The documentary stars comedian Doug Benson. The documentarys name and its poster are plays on the 2004 documentary Super Size Me.

==Synopsis==
Super High Me documents Benson avoiding cannabis for a cleansing period and then smoking and otherwise consuming cannabis every day for 30 days in a row.    Benson says that Super High Me is "Super Size Me with cannabis instead of McDonalds".  The film also includes interviews with marijuana activists, dispensary owners, politicians and patients who are part of the medical cannabis movement. The DVD was released on April 20, 2007.

Benson underwent various tests to gauge his physical and mental health, first during a 30-day period in which he abstained from cannabis use, then during another 30-day period in which he smoked and ingested cannabis every day. Bensons physician concluded that the effects on Bensons health from his use of cannabis were generally inconsequential. The greatest undesirable changes noted were a weight gain of eight pounds during his "high" month and a significant decrease in his ability to do mental mathematics. His sperm count increased, contrary to what might be expected based on medical studies. His overall score on the SAT increased, mainly due to an increased verbal score.

{| class="wikitable" border="1"
|+ Summary of test results
|-
!
! Sober Period
! High Period
|- ESP Test
| 1/25
| 7/25
|-
| The SAT
| 980/1600
| 1030/1600
|-
| Mini-Mental Status Review (Normal = 22+)
| 27/29
| 24/29
|-
| Lung Capacity
| 92%
| 89%
|-
| Sperm Count (20 million/mL is normal)
| 21 million/mL
| 32 million/mL
|-
| Weight Change
| −2&nbsp;lbs.
| +8&nbsp;lbs.
|}

At the end of the experiment, Benson expressed surprise that he did not acquire any aversion to the drug after such continual use, something which he had predicted at the beginning.

The end credits contain a dedication to Michelle Benjamin, a friend of the filmmakers who was killed in a traffic accident involving a drunk driver.  

==Cast== Alex Campbell
*Brian Unger
*Gary Cohan
*Robert Gore
*Sean Tabibian
*Paul F. Tompkins
*Bob Odenkirk
*Graham Elwood
*Jimmy Dore
*Greg Proops
*Rob Huebel
*Rob Riggle
*Brian Posehn
*Patton Oswalt
*Sarah Silverman
*Arj Barker
*Zach Galifianakis
*Jeff Ross

==Production==
The star of the film, comedian Doug Benson, said that the conception of the film originated with a joke in his stand-up act, asking, "If theres a movie called Super Size Me about a guy who ate McDonalds every day, why couldnt there be this movie called Super High Me, where I smoke pot every day?"     He shared the joke with friend and filmmaker Michael Blieden, who saw potential in producing a film.  Producer Alex Campbell wanted to make a film about the burgeoning medical marijuana scene in Los Angeles and ran into Blieden who casually mentioned the Doug Benson project and Campbell seized on it. Six months later the film was in production. Michael Blieden said about the making of the film, "Without Alex the movie would have languished indefinitely."  Campbell commented on the films inception, "I heard the joke and immediately knew it could be a film if Doug was willing to go through with the experiment."     When they made the film, Benson ensured that his "antics" were legally tolerated in the state of California.  The comedian said of the experience, "It would be difficult for anyone who has obligations to do, so I wouldnt break the law. I didnt operate a motor vehicle for the 30 days of filming. The crew drove me everywhere."  The documentary also featured notable cannabis advocates, including Marc Emery, the Canadian "Prince of Pot", and Dennis Peron, founder of the Cannabis Buyers Club and coauthor of California Proposition 215.  The film was reportedly marketed for the same budget as the cost of a two-inch advertisement in The New York Times. 

==Reception== Humboldt County. 420 in cannabis culture.   The film was distributed by Red Envelope Entertainment (a distribution arm of Netflix), B-side, and Screen Media Films in a partnership.  The companies arranged for grassroots screening events, where people could sign up on a website to receive a free DVD and have a public screening with a group of any size.  The aim of the distribution strategy was to encourage later DVD sales. 

Frank Scheck of The Hollywood Reporter thought that the documentary consisted of Benson merely "expanding a bit from his stand-up act to strained results". Scheck wrote, "Super High Me mainly is an excuse for a series of comic riffs from its undeniably amusing subject who apparently is a favorite among the stoner crowd."  The critic thought that the examination of the issues of medical cannabis and the conflict between the drug policy of California and the drug policy of the United States was "superficial".   Mark Rahner of The Seattle Times wrote, "Bensons   isnt as much of a revelation  , and its more amiable than funny."  While Rahner applauded the on-screen graphics as "professional caliber", he found the video footage to be "distractingly poor".  Rahner concluded, "In the end, this is a weak advocacy film without much to interest anyone else." 

==References==
 

==External links==
* 
* 
*  on Songpull

 
 
 
 
 