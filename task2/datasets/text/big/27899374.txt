The Wild Cat (1921 film)
{{Infobox film
| name           = The Wild Cat
| image_size     =
| image	         = Die_Bergkatze_by_Lubitsch_poster.jpg
| caption        =
| director       = Ernst Lubitsch Paul Davidson
| writer         = Hanns Kräly Ernst Lubitsch
| narrator       = 
| starring       = 
| music          = 
| cinematography = Theodor Sparkuhl
| set design     = Ernst Stern
| distributor    =
| released       = 12 April 1921
| runtime        = 82 minutes
| country        = Germany
| language       =
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 German silent, farcical, romantic comedy film directed by Ernst Lubitsch.

The Rats & People Motion Picture Orchestra premiered its new score for the film at the 2011 St. Louis International Film Festival.

==Plot==
===Act I===
The commander of an isolated border fortress receives word that a Lieutenant Alexis has been assigned there as punishment. This delights his wife and daughter. Elsewhere, vast mobs of women line the streets to bid Alexis goodbye (as does a horde of young children who call him Daddy). 

===Act II===
On the sleigh ride to his new posting, Alexis is pelted by a snowball by a young woman. When he gets out of the sleigh, he is surrounded by armed men, part of a gang of feared robbers nominally led by Claudius, but in reality under the bidding of his daughter, Rischka. Rischka forces Alexis to take off his uniform. He nonchalantly kisses her hand anyway. When one of the men draws his dagger, Rischka intervenes and lets Alexis go unharmed, in his "underclothes".

===Act III===
When Alexis arrives at the fort, Lilli, the commanders daughter, likes what she sees. The commander sends Alexis with a large musical band and a smaller detachment of soldiers to punish the robbers, but despite being outnumbered, Rischka and her men have little trouble routing their attackers. When the soldiers return to the fort, their commander assumes they have been victorious and gives Alexis Lillis hand in marriage as a reward. None of the men bother to correct him.

There is a great celebration, with fireworks, an orchestra, dancing and drinking. Rischka sneaks into the fortress with some of the robbers and proceeds to loot a bedroom. She puts on a dress she finds there, and the men don uniforms; then they all join the party. Alexis spots her and gives chase, finally trapping her in a room. They embrace, but then he decides his duty requires him to turn her in. He locks her in, but a jealous Lilli later opens the room and makes Rischka leave before Alexis returns with soldiers.

===Act IV===
Claudius also decides it is time for his daughter to marry. The robbers remind him that he promised one of them the honor. When Rischka demands to know who it will be, all but one slink away. Only Pepo remains. Rischka does not take him seriously, until he unexpectedly knocks her down and drags her away by her legs. Shocked at first by this atypical behavior, she eventually showers him with kisses, and they embrace in the snow. As part of the wedding ceremony, Claudius chains the couple together at the wrist. However, Rischka becomes sad when she reads of the betrothal of Alexis and Lilli. Seeing this, Pepo unchains her.

She goes to Alexis suite. He is glad to see her, and goes to change into something more comfortable. While he is gone, Lilli arrives. Seeing her rival, she bursts into tears, causing Rischka to promise to make things right (though she does steal Lillis necklace while comforting her). When Alexis returns, Rischka acts so boorishly that he becomes disgusted. She returns to Pepo, while Alexis greets Lilli more warmly.

== Cast ==
*Victor Janson as Fortress Commander
*Marge Köhler as His Wife
*Edith Meller as Their Daughter  
*Paul Heidemann as Lieutenant Alexis
*Wilhelm Diegelmann as Claudius
*Pola Negri as Rischka
*Hermann Thimig as Pepo, a shy thief
*Paul Biensfeldt as Dafko
*Paul Grätz as Zofano
*Max Kronert as Masilio
*Erwin Kropp as Tripio

==Restoration== film was restored in 2000 from an original negative. The intertitles were lost, but were reconstructed from surviving censor notes.

==DVD releases== Kino Lorber as part of the box set "Lubitsch in Berlin" in 2007 with English intertitles. It was also released in the UK by Eurekas Masters of Cinema series as part of the box set "Lubitsch in Berlin: Fairy-Tales, Melodramas, and Sex Comedies" in 2010 with German intertitles and English subtitles.

==External links==
* 
*  by the Wellington Film Society

 

 
 
 
 
 
 
 