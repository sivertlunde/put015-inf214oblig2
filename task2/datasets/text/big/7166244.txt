Lovely by Surprise
{{Infobox film
|  name           = Lovely By Surprise |
  image          = Lovely_By_Surprise_poster.jpg|
  caption        = Promotional poster for Lovely By Surprise |
  writer         = Kirt Gunn | Kate Burton Richard Masur |
  music          = Shelby Bryant |
  director       = Kirt Gunn |
  runtime        = 100 minutes |
  language       = English |
  }} 2007 film directed by Kirt Gunn. It stars Carrie Preston, Michael Chernus, Austin Pendleton and Reg Rogers.

== Synopsis of plot ==

Facing an intense bout of writers block, novelist Marian Walker ( ), the books protagonist.  Marian tries to write the death scene, but this proves complicated: Humkin survives, escapes, and starts to appear outside of the confines of the story.

== Promotional webisodes ==
In 2006 two websites, Lovely By Surprise and The Neverything, were launched as teasers for the film.  Each website told a different strand of the films story through one-minute "webisode" installments over a period of weeks.

== External links ==
* 
* 
* 

 
 
 


 