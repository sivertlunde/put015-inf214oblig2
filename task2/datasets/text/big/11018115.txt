The Fascist
 
{{Infobox film
| name           = Il federale
| image          = Il_Federale.jpg 
| image_size     = 
| caption        = Italian film poster
| director       = Luciano Salce
| producer       = Dino De Laurentiis
| writer         = Franco Castellano Giuseppe Moccia Luciano Salce
| narrator       = 
| starring       = Ugo Tognazzi Georges Wilson Stefania Sandrelli 
| music          = Ennio Morricone
| cinematography = Erico Menczer
| editing        = Roberto Cinquini
| distributor    = 
| released       = 1961 (Italy) 17 June 1965 (U.S.)
| runtime        = 100 Min
| country        = Italy Italian
| budget         = 
}}
  Italian film directed by  Luciano Salce. It was coproduced with France. It was also the first feature film scored by Ennio Morricone.

== Plot ==
Enthusiast militant Primo Arcovazzi (played by Ugo Tognazzi) to take into custody professor Bonafe, a noted anti-fascist philosopher, from the rural location where he was confined in and to lead him to Rome (at the time controlled by the RSI).

Equipped with a motorcycle-sidecar combination Arcovazzi picks up the professor and heads towards the Eternal City; along the way the couple wrecks its vehicle to avoid running over a girl (Stefania Sandrelli) who turns out to be a confidence trickster and petty thief; and, after having scammed the professor out of 150 Italian Lira|lire, she disappears.

Without a mean of transportation Arcovazzi asks help to a truckload of Wehrmacht soldiers passing by, only to have his sidecar confiscated and to be made prisoner (along with his original prisoner) by the German forces.
 air raid, donning German uniforms to pass unnoticed during the commotion but, while Arcovazzi is stealing a Schwimmwagen Bonafè tries to desert him.

But the POWs seem to be a bit too loud and relaxed and, after some inquiry, Arcovazzi is horrified at the discovery that he is, actually, behind the enemy lines (and dressed as a party boss, nonetheless).

The partisans, seeing his high-rank uniform, are however inclined to shoot Arcovazzi on the spot; seeing the spirits much too inflamed to be convinced otherwise Bonafe asks for a pistol and the dubious "honour" of shooting his former jailer himself.

Leading the wannabe-"Federale" behind a ruined wall he, throws the weapon away, helps Arcovazzi in removing the uniform which was about to seal his fate and lets him go.

==Cast==
*Ugo Tognazzi: Primo Arcovazzi
* Georges Wilson: Professor Erminio Bonafé
* Stefania Sandrelli: Lisa
* Gianrico Tedeschi: Arcangelo Bardacci
* Elsa Vazzoler: Matilde Bardacci 
* Gianni Agus: Head of the beam of Cremona
* Luciano Salce: German Lieutenant
* Renzo Palmer: Partisan Taddei

==See also==
* Military history of Italy during World War II 
* Allied invasion of Italy Escape by Night, Two Women, Everybody Go Home, The Four Days of Naples, Salò o le 120 giornate di Sodoma
*

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 