Righteous Kill
{{Infobox film
| name           = Righteous Kill
| image          = Righteous kill ver2.jpg
| director       = Jon Avnet
| producer       = Avi Lerner Boaz Davidson Daniel M. Rosenberg Lati Grobman Randall Emmett
| writer         = Russell Gewirtz
| starring       = {{Plain list |
* Robert De Niro
* Al Pacino
* Donnie Wahlberg
* John Leguizamo
* 50 Cent
* Carla Gugino
* Brian Dennehy
}}
| music          = Ed Shearmur
| cinematography = Denis Lenoir Paul Hirsch
| studio = Grosvenor Park Productions Emmett/Furla/Oasis Films|Emmett/Furla Films Universal Pictures Overture Films Millennium Films
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $60 million
| gross          = $78,460,699 
}} American crime thriller film Curtis "50 Cent" Jackson.   The film was released in the United States on September 12, 2008.

==Plot==
Police psychologists review recordings of a man (Robert De Niro), who states his name as Detective David Fisk, the "Poetry Boy" killer. The Poetry Boy earned the moniker for his modus operandi of murdering criminals and leaving short poems with their bodies. Fisk reveals that he looks up to his partner of almost 30 years, Tom Cowan (who the audience is led to believe is the character portrayed by Al Pacino), and considers him to be his role model of how a cop should be. Pacinos character is known by the nickname "Rooster" and De Niros by "Turk," and they are referred as such outside of the recordings.

These recordings provide a narrative, and the film opens with the tenth victim, a drug dealer named Robert "Rambo" Brady (Rob Dyrdek). Turk and Rooster investigate the murder with the less-experienced Detectives Corelli (Carla Gugino), Perez (John Leguizamo), and Riley (Donnie Wahlberg). When they find a poem on the body, they link it to the Poetry Boy.

As Poetry Boy murders acquitted rapist Jonathan Van Luytens and Father Connell, a Catholic priest and child molester (children including Poetry Boy himself), tension builds between Turk, Corelli, and Perez. Turk is now living with Corelli, who happens to be Perezs ex-girlfriend. Poetry Boy assaults an attempted fourteenth victim, Russian mobster Yevgeny Magulat, but he survives and goes on to shoot at Perezs house and rape Corelli. Perez and Riley suspect Turk of being Poetry Boy due to his markmanship skills and psych evaluations, so they arrange a secretly supervised meeting between Turk and a drug dealer Marcus "Spider" Smith (50 Cent) (during which Turk supposedly must kill him). Turk proves his innocence during an encounter with this drug dealer as he has the "wrong" gun and humiliating but obviously inappropriate poem. After Perez and Riley leave the scene unsatisfied, Rooster kills Spider. During this scrape Rooster inadvertently drops his diary.

Turk stumbles upon and reads Roosters journal, Rooster claims Spider as Poetry Boys fourteenth victim. Rooster puts Turk in front of a video camera and forces him to read the journal. At this point, it is realized that the recordings from and the narration merely set Turk up as a red herring, and Rooster is the actual Poetry Boy. Turks name is actually Tom Cowan, and Rooster is David Fisk. Rooster lost his faith in justice when Turk, an otherwise "righteous" cop, planted a gun at the house of acquitted child molester and murderer Charles Randall (Frank John Hughes), convicting him. This leads him to taking justice into his own hands.

When Turk finishes, he chases Rooster to a construction site. Rooster fires aimlessly to convince Turk to report that the Poetry Boy is assaulting a police officer, resisting arrest, and fleeing, but Turk resists. When Rooster takes aim at Turk, Turk fires, striking Rooster in the chest. He calls for an ambulance, however Rooster begs him to withdraw the ambulance. After some hesitation, Turk calls off the ambulance, allowing Rooster to bleed to death. He is last shown coaching a Police Athletic League baseball team as Corelli looks on.

==Cast==
* Robert De Niro as Detective Tom Cowan/"Turk"
* Al Pacino as Detective David Fisk/"Rooster"/The "Poetry Boy" Killer
* John Leguizamo as Detective Simon Perez
* Donnie Wahlberg as Detective Theodore Riley Curtis "50 Cent" Jackson as Marcus Smith/"Spider"
* Carla Gugino as Detective Karen Corelli
* Brian Dennehy as Lieutenant Hingis
* Frank John Hughes as Charles Randall
* Trilby Glover as Jessica
* Oleg Taktarov as Yevgeny Mugalat
* Rob Dyrdek as Robert Brady/"Rambo"
* Papoose (rapper)  as Himself

==Reception==

===Critical response===
The film has received mainly negative reviews. Rotten Tomatoes reported that 19% of critics gave positive reviews based on 140 reviews.  Metacritic gave the film a 36/100 approval rating based on 27 reviews. 

The Times included Righteous Kill on its 100 Worst Films of 2008 list.

Keith Phipps of  , not a craftsman. ... Its failure to live up to even modest expectations is a blow. Theres nothing righteous to be found here." 

Ken Fox of   without appearing on screen together and in Heat (1995 film)|Heat, sharing the screen in just two scenes).

Claudia Puig of   of  : AARP."  However, Richard Roeper gave the film 3 stars out of 4 and Tim Evans for Sky Movies remarked that the film was "... an effective whodunnit but - more importantly - it poses refined, complex questions about how the law operates in a so-called civilised society."  

Al Pacino earned a Razzie Award nomination for Worst Actor for his performance in the film (also for 88 Minutes), but "lost" the award to Mike Myers for The Love Guru.

===Box office===
On its opening weekend, Righteous Kill opened at #3, grossing $16,288,361, behind The Family That Preys and Burn After Reading respectively.  Its final box office tally was approximately $40 million domestic (US/Canada) and $38 million international for a total of $78 million.  By comparison, an earlier movie that paired De Niro and Pacino, Heat (1995 film)|Heat, grossed over $180 million worldwide. Overture Films paid $12 million to acquire the film,  and stated that they will be happy if this film could gross $25 million in the United States theatrically. 

==Home media==
  
The DVD and Blu-ray  of the film were released on January 6, 2009. About 954,000 DVD units have been sold so far, gathering $15,828,184 in revenue.  This does not include Blu-ray sales.

==See also==
* List of American films of 2008

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 