Suhaag (1979 film)
{{Infobox film
| name           = Suhaag
| image          = Suhaag 1979 poster.jpg
| image size     =
| caption        = Poster
| director       = Manmohan Desai
| producer       = Rajinder Kumar Sharma Ramesh Sharma Shakti Subhash Sharma Prakash Trehan
| writer         = Kader Khan Prayag Raj K.K. Shukla
| narrator       =
| starring       = Amitabh Bachchan Shashi Kapoor Rekha Parveen Babi Nirupa Roy Ranjeet Kader Khan Amjad Khan Jeevan Jagdish Raj
| music          = Laxmikant-Pyarelal
| cinematography = V. Durga Prasad
| editing        = Mukhtar Ahmed
| studio         =
| distributor    = Sharma Cine Associates
| released       = 30 October 1979
| runtime        = 169 mins
| country        = India Punjabi
| budget         =
}}

Suhaag is a 1979 Indian Hindi drama film directed by Manmohan Desai.  A box office success, the film became the highest earning film of 1979. 

The movie stars  Amitabh Bachchan, Shashi Kapoor, Rekha, and Parveen Babi in lead roles.  Amjad Khan, Nirupa Roy, Kader Khan, Ranjeet, and Jeevan play supporting roles.  The music was composed by Laxmikant Pyarelal.
 Telugu as Sathyam Sivam with Akkineni Nageswara Rao and N T Rama Rao

==Plot==
Durga (Nirupa Roy) and Vikram Kapoor (Amjad Khan) have been married for years.  Vikram has taken to crime in a big way and as a result has antagonized a rival gangster, Jaggi.  Durga gives birth to twins and Jaggi steals one of them, and sells him to a bootlegger, Pascal. Durga is upset when she finds her son missing, but is devastated when Vikram abandons her.  With a lot of difficulty, Durga brings up her son, Kishan (Shashi Kapoor), and has given up on finding her other son.  Kishan has grown up and is now a dedicated police officer. On the other hand, Pascal has exploited Amit (Amitabh Bachchan), kept him illiterate, and made him a petty criminal and alcoholic. This gets him in confrontation with Kishan but ironically the two settle their differences and become fast friends. Vikram is not aware of his two sons and wife being alive. Without revealing his identity, he hires Amit to kill Kishan during a Navratri dance at Maa Sherawalis temple. Amit informs Kishan and together with other police personnel, keep vigil. Things do not go as planned; they are attacked and Kishan loses his eyesight, leaving the onus on Amit to try to locate the person behind this crime.

==Cast==
* Amitabh Bachchan as Amit Kapoor
* Shashi Kapoor as Kishan Kapoor
* Rekha as Basanti
* Parveen Babi as Anu
* Nirupa Roy as Durga Kapoor
* Ranjeet as Gopal
* Kader Khan as Jaggi
* Amjad Khan as Vikram Kapoor
* Jeevan as Pascal
* Jagdish Raj as Police Officer

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background: #CCCCCC; text-align: center"
! # !! Title !! Singer(s)
|-
| 1
| "Atthara Baras Ki Tu Hone Ko"
| Mohammed Rafi, Lata Mangeshkar
|-
| 2
| "Aaj Imtehan Hai"
| Lata Mangeshkar
|-
| 3
| "Teri Rab Ne Bana Di Jodi"
| Mohammed Rafi, Shailender Singh, Asha Bhosle
|-
| 4
| "Main To Beghar Hoon"
| Asha Bhosle
|-
| 5
| "Ae Yaar Sun Yaari Teri"
| Mohammed Rafi, Shailender Singh, Asha Bhosle
|-
| 6
| "O Sheronwali"
| Mohammed Rafi, Asha Bhosle
|}
The movie songs were great hits. Mohd Rafi and Laxmikant Pyarelal combos created popular songs. Mohd Rafis voice was used for Amitabh Bachchan and Shailendra Singhs voice for Shashi Kapoor. The songs of the movie are popular till today. Ae Yaar Sun Yaari Tere is classified as most popular song after Yeh Dosti Hum Naheen Toregi by Kishore Kumar and Manney Dey from film Sholay. Tere Rab Ne Banede Jodi is played in  most weddings.

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 