Her Favourite Husband
{{Infobox film
| name           = Her Favourite Husband
| image          = "Her_Favourite_Husband"_(1950).jpg
| image_size     = 
| caption        = 
| director       = Mario Soldati
| producer       = Colin Lesslie   Carlo Ponti   John Sutro Steno   Mario Soldati 
| narrator       = 
| starring       = Jean Kent   Robert Beatty   Gordon Harker   Margaret Rutherford   Rona Anderson
| music          = Nino Rota
| cinematography = Mario Bava 
| editing        = Douglas Robertson 
| studio         = Lux Film
| distributor    = Renown Pictures   Lux Film
| released       = September 1950 
| runtime        = 79 minutes
| country        = United Kingdom   Italy
| language       = English
| budget         = 
}}

Her Favourite Husband (also known by the alternative titles The Taming of Dorothy and Quel bandito sono io) is a 1950 British-Italian comedy film directed by Mario Soldati and starring Jean Kent, Robert Beatty and Margaret Rutherford.    It was based on a play by Peppino De Filippo.    The films art direction was by Piero Gherardi. 

==Plot==
Mild mannered Italian bank clerk Antonio, much dominated by his English wife Dorothy, is the double of Leo LAmericano, a local gangster. The gangster kidnaps Antonio and takes his place as husband in the family, to give him cover for a big bank robbery, which he plans to pin on Antonio. Farcical confusions ensue.

==Cast==
* Jean Kent as Dorothy Pellegrini 
* Robert Beatty as Antonio Pellegrini 
* Gordon Harker as Godfrey Dotherington 
* Margaret Rutherford as Mrs. Dotherington 
* Rona Anderson as Stellina 
* Walter Crisham as Caradiotto 
* Max Adrian as Catoni 
* Tamara Lees as Rosana  Michael Balfour as Pete 
* Jack McNaughton as El Greco 
* Norman Shelley as Mr. Dobson  Danny Green as Angel Face 
* Joss Ambler as Mr. Wilson 
* Mary Hinton as Mrs. Wilson 
* Peter Illing as Commissario Scaletti 
* Jimmy Ventola as Ciocio Pellegrini 
* Andreas Malandrinos as Customs Officer

==Critical reception==
TV Guide wrote, "corny dialog bogs this film down much of the time" ;   while Allmovie described it as "a genial romp distinguished by a sizeable supporting cast of familiar British players."   

==References==
 

==External links==
* 
 
 

 
 
 
 
 
 
 
 
 
 
 
 