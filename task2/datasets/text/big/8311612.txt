Tiga Abdul
 
 
{{Infobox film name           = Tiga Abdul image          = TigaAbdul.jpg caption        = The one-sheet for Tiga Abdul. director       = P. Ramlee producer       = writer         = P. Ramlee narrator       = starring       = P. Ramlee Sarimah Ahmad Nisfu music          = P. Ramlee cinematography = editing        = distributor    = Shaw Brothers Ltd. released       = 1964 runtime        = country        = Malaysia language       = Malay budget         = preceded_by    = followed_by    =
}}

Tiga Abdul (The Three Abduls) is a 1964 Malaysian comedy film directed by and starring Malaysian silver-screen icon P. Ramlee. It tells the story of three brothers who are caught in a web of trickery set by the cunning Sadiq Segaraga who uses his three daughters to fleece the three brothers of all their wealth. The movie is a tribute to traditional folktales with a moral set into the story and is set a fictional middle eastern country named Isketambola, which is loosely based on Istanbul, Turkey.

==Plot==
Ismet Ulam Raja is a wealthy businessman with three sons, Abdul Wahab, Abdul Wahib and Abdul Wahub. Abdul Wahab and Abdul Wahib are selfish and money-minded, running their individual businesses to success. Abdul Wahub, on the other hand, enjoys a simple life and owns a small music shop. When Ismet Ulam Raja has a heart attack on his birthday, Abdul Wahab and Abdul Wahib start plotting out how much wealth they are set to gain when their father dies. Abdul Wahub is appalled at their behaviour and tries to get their father to go to the hospital, although Ismet Ulam Raja is strongly against hospitals. Their father dies at home, and the two elder brothers immediately distribute their late fathers vast wealth among the two of them while Abdul Wahub only inherits their fathers house. Although Abdul Wahub is upset at this injustice, he accepts it as he is the youngest brother, and returns to his simple music business.

Sadiq Segaraga, a friend of the late Ismet Ulam Raja, has also set his eyes on the vast wealth that once belonged to his friend. He orders his three beautiful daughters, Hamidah, Rafidah and Ghasidah, to woo the three Abduls. Hamidah is successful in wooing Abdul Wahab and Rafidah is successful in wooing Abdul Wahib, but Ghasidah and Abdul Wahub only argue with each other on sight.

The two elder Abduls meet Sadiq Segaraga to ask for his two daughters hands in marriage. Sadiq agrees, on the condition that they sign a contract written by him and his lawyer, Kassim Patalon. The contract states that if the Abduls lose their temper at any moment during their marriage, all the wealth they own will go to Sadiq and the Abduls will be sold as slaves. Although they are suspicious of the contract, they agree to sign it. After the marriage, Abdul Wahab and Abdul Wahib move into Sadiqs home, where they are told they are not allowed to eat their food, only smell it, and they are to sleep in the stables, not in their wives rooms. The two Abduls lose their temper at these conditions, and Sadiq reveals the contract, claiming all their wealth and sells the two men as slaves.

Abdul Wahub sees his brothers being sold in the marketplace, but cannot do anything. That night his father appears to him in a dream, telling him to meet a man named Sulaiman Akh-laken. Abdul Wahub does as hes instructed and it turns out that Sulaiman Akh-laken is Ismet Ulam Rajas lawyer who managed Ismets overseas properties, which are now passed on to Abdul Wahub. Abdul Wahub discovers that he is several times richer than his two elder brothers combined. Using this knowledge, he starts his plan by meeting Sadiq Segaraga and asking for Ghasidahs hand in marriage. Sadiq shows Abdul Wahub the same contract hed presented to his elder brothers, and Abdul Wahub says that hell sign it, on the condition that Sadiq signs another contract. The second contract states that if Sadiq loses his temper, then Abdul Wahub will claim all his wealth and sell Sadiq as a slave.

Abdul Wahub and Ghasidah are married, although they supposedly still hate each other. Abdul Wahub arrives at Sadiqs home and is told the same things his brothers were told but, being prepared, he reacts to all the conditions with ease. The following days Abdul Wahub counters Sadiqs trickery by avoiding Ghasidah, going on supposed dinner dates with another woman and giving away all the things in Sadiqs shop to the poor. Each time Sadiq almost loses his temper, but his lawyer Kassim Patalon reminds him about the contract he signed.

Ghasidah then confronts Abdul Wahub, asking him whether hes really having an affair with another woman. At first Abdul Wahub pretends it is true, but the truth comes out that it was just pretend, and that he is actually in love with Ghasidah, and Ghasidah is in love with him.

Sadiq Segaraga finally loses his temper when Abdul Wahub invites the people of the town into Sadiqs house to take away anything they want. When Sadiq admits that his truly angry, Abdul Wahub reveals the contract, taking everything that Sadiq owns and selling Sadiq, Kassim Patalon, Rafidah and Hamidah as slaves in the market.

After a while, Abdul Wahub buys Abdul Wahab and Abdul Wahib (who have been bought by a merchant), along with Sadiq, Rafidah and Hamidah,(except for Kassim Patalon alone) and brings all of them back to the house that was once the only thing that Abdul Wahub inherited from Ismet Ulam Raja. After a tearful speech, Abdul Wahub apologises to everyone for his doing and undoes the contracts that bound them as slaves. All those who spent their time as slaves learned their lesson.

==Cast==
* P. Ramlee as Abdul Wahub (youngest son)
* Haji Mahadi as Abdul Wahab (eldest son)
* S. Kadarisman as Abdul Wahib (middle son)
* Ahmad Nisfu as Sadiq Segaraga
* Sarimah as Ghasidah (youngest daughter)
* Mariani as Hamidah (eldest daughter)
* Dayang Sofia as Rafidah (middle daughter)
* M. Babjan as Ismet Ulam Raja
* Salleh Kamil as Kassim Patalon
* S. Shamsuddin as Pencerita

==Songs==
* Bunyi Guitar (The Sound of the Guitar) - Performed by P. Ramlee
* Tolong Kami, Bantu Kami (Oh Please Help Us) - Performed by P. Ramlee
* Sedangkan Lidah Lagi Tergigit (Even the Tongue) - Performed by P. Ramlee and Saloma

==See also==
* List of P. Ramlee films
* P. Ramlee

==External links==
*  
*  

 
 
 
 
 