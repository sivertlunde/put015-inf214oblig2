Bye Bye Miss Goodnight
{{Infobox film
| name           = Bye Bye Miss Goodnight
| image          = 
| caption        =
| director       = Karan Kandhari
| producer       = Karan Kandhari  Adam Cocker  Herman Crona
| writer         = Karan Kandhari
| starring       = Rajpal Yadav Neha Dubey Zafar Karachiwala
| music          = Philip Granell
| cinematography = Anders Dahl
| editing        = Sven M. Jonson
| distributor    = Oslo Beach Films
| released       =  
| runtime        = 78 minutes
| language       = Hindi
}}
Bye Bye Miss Goodnight is the 2005 feature film debut of writer-director Karan Kandhari.  It was filmed in Bombay and garnered the Best Cinematography award at the Portobello Film Festival for its bold use of the citys natural light in its digital cinematography.  It stars Rajpal Yadav and Neha Dubey (Monsoon Wedding).

==Synopsis==
 
On any other night Rajesh would have fallen asleep by now in the cafeteria to sounds of old folk tales on his battered walkman.

Today however is the day he’s leaving. He’s decided, he will drive his taxi into the Bombay night, wherever daylight breaks is where he’s going to stop and start anew.  Sounds pretty straight forward.  If only the eccentric masseur who has appointed himself position of new best friend to our hapless cabby can convince Rajesh that leaving Bombay at 6 AM won’t get him past the city limits, he may actually stand a chance.

The crackly old folk tales Rajesh listens to punctuate the narrative, guiding him like an unwritten map as he embarks on this haphazard road trip into the weird and wonderful. A trip that only gets stranger by the arrival of a stowaway hidden under a pile of fruit in the back seat...

You can’t get lost if you don’t know where you’re going...

Rajpal Yadav one of India’s most beloved comedians delivers a memorable Keatonesque performance as the disenchanted cabbie Rajesh alongside the rambunctious Neha Dubey (Monsoon Wedding) as the stowaway who becomes his unlikely companion in this award winning deadpan comedy.  An Indian road movie...

==Critical response==
 
"There is evidence of better things to come from British Asian directors, however, as Karan Kandhari demonstrates with Bye Bye Miss Goodnight. Offering a uniquely avant-garde snapshot of modern urban India, this visually ambitious road movie belies its modest budget to chronicle the unlikely encounter between a daydreaming Mumbai cabby and a pregnant, hitchhiking free spirit."  - David Parkinson, BBC Film  
 

==Awards==
*Winer "Best Cinematography Award", Portobello Film Festival 2005.

==Cast==
*Rajpal Yadav - Rajesh
*Neha Dubey - Niramala
*Zafar Karachiwala - Rocky
*Neera Punj - Aunts Voice

==References==
 

==External links==
*  

 
 
 