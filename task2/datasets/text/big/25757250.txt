Hound-Dog Man
{{Infobox film
| name           = Hound-Dog Man
| image_size     =
| image	         = Hound-Dog Man FilmPoster.jpeg
| caption        =
| director       = Don Siegel producer = Jerry Wald
| writer         =
| narrator       = Fabian
| music          =
| cinematography =
| editing        = studio = A Company of Artists
| distributor    =20th Century Fox
| released       =  
| runtime        = 87 min.
| country        = United States
| language       = English
| budget         = $1,045,000 
| gross          =
| website        =
| amg_id         =
}}
Hound-Dog Man is a 1959 film directed by Don Siegel. The film is based on the book by Fred Gipson.
==Plot==
In 1912, Clint McKinney and his younger brother Spud talk their father Aaron into letting them go on a hunting trip with their older friend, the womanising Blackie Scantling.

==Cast== Fabian as Clint 
* Stuart Whitman as Blackie Scantling 
* Carol Lynley as Dony Wallace 
* Arthur OConnell as Aaron McKinney 
* Dodie Stevens as Nita Stringer 
* Betty Field as Cora McKinney 
* Royal Dano as Fiddling Tom Waller 
* Margo Moore as Susie Bell Payson 
* Claude Akins as Hog Peyson 
* Edgar Buchanan as Doc Cole 
* Jane Darwell as Grandma Wilson 
* L.Q. Jones as Dave Wilson 
* Virginia Gregg as Amy Waller 
* Dennis Holmes as Spud McKinney 
* Rachel Stephens as Rachel Wilson

==Original novel==
{{Infobox book  
| name          = Hound Dog Man
| title_orig    =
| translator    =
| image         =
| image_caption =
| author        = Fred Gipson
| illustrator   =
| cover_artist  =
| country       = USA English
| series        = 
| genre         = 
| publisher     =
| release_date  = 1949
| english_release_date =
| media_type    =
| pages         =
| isbn          =
| preceded_by   =
| followed_by   = 
}}
The original book was published in 1949, several years before Gipsons better known Old Yeller.  At one stage Ida Lupino expressed interest in obtaining the film rights, as a possible vehicle for Robert Mitchum  

==Production==
20th Century Fox bought the film rights in March 1958 following the success of the film of Old Yeller.  It was assigned to prolific producer Jerry Wald. Ricky Nelson, Lyndsay Crosby and David Ladd were mentioned early on as possible stars, along with Stuart Whitman, who did wind up playing the title role.  Tuesday Weld was at one stage mentioned as a possible female lead. 

The movie eventually became a starring vehicle for Fabian, who had released a series of hit singles. 20th Century Fox had enjoyed success launching pop stars Elvis Presley and Pat Boone into film careers and thought they could do the same with Fabian. Thomas Doherty, Teenagers And Teenpics: Juvenilization Of American Movies, Temple University Press, 2010 p 175-176  He was paid $35,000 for ten weeks work. 

Wald tried to get Jayne Mansfield to play the part of a blousy barmaid but was unsuccessful.  Dodie Stevens was cast because Walds teenage sons liked her song "Pink Shoe Laces". 

Filming took place in August-September 1959.

==Songs==
{{Infobox single  
| Name           = Hound Dog Man
| Cover          = 
| Cover size     = 
| Border         = 
| Alt            = 
| Caption        = 
| Artist         = Fabian Forte
| Album          = 
| B-side         = 
| Released       = 16 November 1959
| Format         = 
| Recorded       = 1959
| Genre          = Rock and roll
| Length         = 2:10
| Label          = Chancellor Records
| Writer         = Doc Pomus Mort Shuman
| Producer       = Peter De Angelis
| Certification  = 
| Last single    = "Got the Feeling"
| This single    = "Hound Dog Man"
| Next single    = "This Friendly World"
| Misc           = 
}}
{{Infobox single  
| Name           = This Friendly World
| Cover          = 
| Cover size     = 
| Border         = 
| Alt            = 
| Caption        = 
| Artist         = Fabian Forte
| Album          = 
| B-side         = 
| Released       = 23 November 1959
| Format         = 
| Recorded       = 1959
| Genre          = Rock and roll
| Length         = 2:00
| Label          = Chancellor Records
| Writer         = Ken Darby
| Producer       = Peter De Angelis
| Certification  = 
| Last single    = "Hound Dog Man"
| This single    = "This Friendly World"
| Next single    = "String Along"
| Misc           = 
}}
The movie featured the following songs:
*"Hound Dog Man" performed by Fabian
*"This Friendly World" performed by Fabian
*"Single" performed by Fabian
*"Im Growin Up" performed by Fabian
*"Pretty Little Girl" performed by Fabian
*"What a Big Boy" performed by Dodie Stevens
"Hound Dog Man" was a hit single, reaching number 9 on the US charts. "This Friendly World" reached number 12. 

==Reception== High Time.

==References==
 

==External links==
* 
*  at TCMDB
*  at New York Times
 

 
 
 
 