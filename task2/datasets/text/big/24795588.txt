The Maid (2009 film)
{{Infobox film
| name           = The Maid
| image          = La_Nana_2009.jpg
| image_size     = 
| caption        = Theatrical film poster
| director       = Sebastián Silva (director)|Sebastián Silva Gregorio González
| writer         = Pedro Peirano Sebastián Silva
| narrator       =
| starring       = Catalina Saavedra Claudia Celedón Mariana Loyola Agustín Silva Alejandro Goic Andrea García-Huidobro
| music          = 
| studio         = 
| distributor    = Elephant Eye Films
| released       =  
| runtime        = 95 minutes
| country        = Chile
| language       = Spanish
| budget         = 
| gross          = 
}} 25th Annual Sundance Film Festival. The film has had much critical acclaim, particularly for Catalina Saavedras award-winning performance as the lead character.

==Plot synopsis==
Raquel (Saavedra) has served as the maid for the Valdes family for over 23 years.  She treats her employers, Pilar (Celedón) and Edmundo (Goic) with the utmost loyalty and respect. She gets along well with their teenage son, Lucas (Agustín Silva) but clashes with their headstrong daughter, Camila (García-Huidobro). When Raquel begins to suffer dizzy spells, due to an excessive use of chlorine for household cleaning, Pilar decides to hire additional maids to assist Raquel in her daily chores. The fiercely territorial Raquel resents this and engages in a series of increasingly desperate attempts to drive away maid after maid, including the younger Lucy (Loyola), in order to maintain her position in the household. 

==Cast==
*Catalina Saavedra as Raquel
*Mariana Loyola as Lucy
*Claudia Celedón as Pilar
*Alejandro Goic as Edmundo
*Agustín Silva as Lucas
*Andrea García-Huidobro as Camila Anita Reeves as Sonia
*Delfina Guzmán as The Grandmother

==Critical reception== Original Song category for the 82nd Academy Awards.  Film critic David Parkinson called it "an exceptional study of the emotional investment that domestics make in the families they serve. Saavedra is mesmerizing as she shifts from subservient to scheming."   Despite the films great success, the film wasnt chosen as Chiles submission to the 82nd Academy Awards. Instead, Miguel Littins Dawson Isla 10 was sent, but the film didnt make the short-list.

Chicago Sun Times film critic Roger Ebert described the film as a "unpredictable, naturalistic gem." 

The film currently holds a 94% approval on the film critics site Rotten Tomatoes. Rotten Tomatoes wrote of the critics consensus, "Catalina Saavedras devastating performance would be reason enough to see The Maid but Sebastian Silvas empathetic direction and finely tuned script only add to the movies pleasing heft." 

==Awards==
{| class="wikitable"
! Year !! Event !! Recipient !! Award !! Result
|-
| rowspan="16"| 2009
| rowspan="3"| Cartagena Film Festival
| Sebastián Silva
| Critics Award - Best Film
|  
|-
| Catalina Saavedra
| Golden India Catalina - Best Actress
|  
|-
| Sebastián Silva
| Golden India Catalina - Best Film
|  
|- Gotham Film Awards Gregorio González (producer) Sebastián Silva (director)
| Best Feature
|  
|-
| Catalina Saavedra
| Breakthrough Actor/Actress
|  
|-
| rowspan="1"| Off Plus Camera Film Festival Poland Gregorio González (producer) Sebastián Silva (director)
| Cracow Film Award (Best Film)
|  
|-
| rowspan="1"| Sarasota Film Festival
| Best Narrative Film
|  
|-
| rowspan="1"| Fribourg International Film Festival
| rowspan="6"| Sebastián Silva (director)
| Talent Tape Award
|  
|-
| rowspan="1"| Paris Cinema International Film Festival
| Audience Award
|  
|-
| rowspan="1"| Taipei Film Festival
| Special Mention New Talent Competition 3rd Audience Award
|  
|-
| rowspan="2"| Latin American Film Festival
| Critics Award
|  
|-
| Elcine First Prize - Best Film
|  
|- Sundance Film Festival Awards
| Grand Jury Prize: World Cinema - Dramatic
|  
|-
| rowspan="2"| Catalina Saavedra
| Special Jury Prize (For Acting): World Cinema - Dramatic
|  
|-
| rowspan="2"| Satellite Awards 2009 Best Actress – Motion Picture Drama
|  
|-
| rowspan="3"| Sebastián Silva (director) Best Foreign Language Film
|   Tied with Broken Embraces
|-
| rowspan="2"| 2010
| rowspan="1"| NAACP Image Awards Best Foreign Language Film
|  
|- Golden Globes Awards Best Foreign Language Film
|  
|-
|}

==References==
 

==External links==
*  
*  

 
 
{{Succession box
| title= 
| years=2009
| before=The King of Ping Pong Animal Kingdom}}
 

 
 

 
 
 
 
 
 
 
 