Bazaar (1982 film)
{{Infobox film
| name           = Bazaar
| image          = Bazaar 1982 film poster.jpg
| image size     =
| caption        = Film poster
| director       = Sagar Sarhadi
| producer       = Vijay Talwar
| writer         = Sagar Sarhadi
| narrator       =
| starring       = Farooq Shaikh Smita Patil Naseeruddin Shah Khayyam
| cinematography = Ishan Arya
| editing        = S. Chakravarty
| distributor    =
| released       = 1982
| runtime        = 121 min.
| country        = India
| language       = Hindi / Urdu
| budget         =
}} bride buying in India, through the tragedy of a young girl being sold by needy parents to affluent expartraite Indians in the Gulf countries|Gulf.  

The film has sterling performances from almost all the cast and is akin to some other movies in the 1980s which highlighted oppression by the rich and powerful. Bazaar (marketplace) is a realistic portrayal and highlights a system which is difficult to change.

== Sound track ==
{{Track listing Khayyam
| lyrics_credits  = yes
| extra_column   = Singer(s)
| title1 = Dekh Lo Aaj Humko Jee Bharke | extra1 = Jagjit Kaur | lyrics1 = Mirza Shauq
| title2 = Dikhayee Diye Yun | extra2 = Lata Mangeshkar | lyrics2 = Mir Taqi Mir Bhupinder Singh | lyrics3 = Bashar Nawaz
| title4 = Phir Chiddi Raat | extra4 = Lata Mangeshkar, Talat Aziz | lyrics4 = Makhdoom Mohiuddin, Bashar Nawaz
}}

Nearly 10 years after the films release, song Karoge Yaad To Har Baat Yaad Aayegi was used in the 1995 album  .

==Cast==
* Smita Patil  as  Najma
* Naseeruddin Shah  as  Salim
* Farooq Shaikh  as  Sarju
* Supriya Pathak  as  Shabnam
* Bharat Kapoor  as  Akhtar Hussain
* B. L. Chopra  as  Shakir Ali Khan

==Awards==
 
|- 1983
| Supriya Pathak
| Filmfare Award for Best Supporting Actress
|  
|- 1983
|Sagar Sarhadi Filmfare Award for Best Film
| 
|-  1983
|Sagar Sarhadi Filmfare Award for Best Director
| 
|-
| 1983
| Smita Patil
| Filmfare Award for Best Actress
|  
|- 1983
|Naseeruddin Shah Filmfare Award for Best Actor
| 
|- 1983
|Sagar Sarhadi Filmfare Award for Best Story
| 
|- 1983
|Khayyam  Filmfare Award for Best Music Director
| 
|}

==See also==
* Ameena case, 1991 case of bride buying from Hyderabad
== References ==

 
== External links ==
* 
*http://jackofall.blogspot.com/2005/06/bazaar-1982.html

 
 
 
 
 
 
 
 
 

 