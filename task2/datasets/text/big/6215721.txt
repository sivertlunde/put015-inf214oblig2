Death Faces
{{Infobox film
| name           = Death Faces
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Countess Victoria Bloodhart Steve White
| producer       = Mink De Ronda Damian B. Gravenhorse
| writer         = S. Sebastian Shock
| narrator       = Bizarro Blackstone
| starring       = Damian B. Gravenhorse
| music          = 
| cinematography = 
| editing        = 
| studio         = Macabre Productions Mahoney Brothers
| distributor    = Deluxe Movie Videos
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Death Faces, also released as Death Faces IV, also Dying: Last seconds of Life, and (in a re edited version) Beyond Reality, is a mockumentary about cannibalism released directly to video originally in 1988.
 Kikuyu tribes people of Kenya, (the film incorrectly says that they are from Papua New Guinea) as well as various stock car crashes, and the John F. Kennedy assassination.

The direction is credited to "Countess Victoria Bloodhart" and Steve White, the writing to "S. Sebastian Shock", the direction and production to "Damian B. Gravenhorse PhD.", with one segment credited to producer "Mink De Ronda". It was re edited and re released as Beyond Reality, which is credited to Steve White. There are no other technical credits given to the film.

With the exception of a few brief scenes, the entire video is in black and white. Footage from this video was later edited into Banned from Facez, parts 3 and 4.

A sequel was also released straight to video: Dying: Last Seconds of Life, Part II, also in 1988.

== External links ==
*  

 
 
 
 
 


 