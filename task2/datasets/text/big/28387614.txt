The Finished People
 
{{Infobox film
| name             = The Finished People
| image            =
| caption          =
| director         = Khoa Do
| producer         = Anh Do
| writer           = Khoa Do Rodney Anderson Mylinh Dinh Daniela Italiano Joe Le Shane Macdonald Jason McGoldrick Sarah Vongmany
| starring         = Rodney Anderson Joe Le Jason McGoldrick Daniela Italiano Mylinh Dinh Sarah Vongmany Shane Macdonald
| music            = Abigail Hatherley
| cinematography   = Oliver Lawrance Murray Lui/camera assistant = Guido Gonzalez
| editing          = Alison McSkimming Croft
| studio           = Post 75 Productions
| released         =  
| runtime          = 80 minutes
| country          = Australia
| language         = English
}}
The Finished People is a 2003 drama about three youths living on the streets of Cabramatta, New South Wales. The film follows the stories of the three main characters; Des, Van and Tommy. The film started out in June 2002 while Rodney, Joe, Jason and Shane were attending classes at a youth services organization called Open Family based in Cabramatta, New South Wales.

==Cast==
*Rodney Anderson as Des
*Joe Le as Van
*Jason McGoldrick as Tommy
*Daniela Italiano as Carla
*Mylinh Dinh as Sara
*Sarah Vongmany as Sophie
*Shane Macdonald as Simon
*Ivan Topic as Boss
*Viet Dang as Geoff

==Box Office==
The Finished People grossed $75,431 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 

 
 
 
 
 


 
 