William Shatner's Gonzo Ballet
{{Infobox film
| name           = William Shatners Gonzo Ballet
| image          = William Shatners Gonzo Ballet.jpg
| caption        =
| director       = Pat Buckley Bobby Ciraldo (co-director) Kevin Layne (co-director)) Andrew Swant (co-director) Scott Woolley Chris Carley  David Zappone Michael Manasseri
| starring       = William Shatner Ben Folds Margo Sappington Henry Rollins Elizabeth Shatner Michael Pink
| music          = Ben Folds
| cinematography = Mark Escribano
| editing        = Ray Chi EPIX
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Common People Pulp song from their 1995 Different Class album. 

The film explores the genesis of this unique artistic collaboration by fusing the music, poetry, and dance of "Common People" with interviews by William Shatner, Ben Folds, Margo Sappington, and Henry Rollins. Shatner plays a prominent role in the film and also acted as Executive Producer.   

The film was made by Special Entertainment and Big Screen Entertainment Group in association with Shatners Melis Productions.   

The documentary had a very well received World Premiere at the Nashville Film Festival in April 2009, where it won the Presidents Impact Award.  William Shatner attended and, to the delight of the audience, provided additional insights into his recording of "Has Been" and the ballet.  Ben Folds and Margo Sappington were also in attendance.  Variety magazine called the film "surprisingly revealing" and indieWire reviewed that "Shatner comes across as a true original."

After the premiere in Nashville the film screened at the Rhode Island Film Festival, DocFest Stratford, the Milwaukee Film Festival, the Wild Rivers Film Festival, the Edmonton International Film Festival, the Indie Memphis Film Festival, and the San Diego Film Festival.

In October 2009 the film screened at the Marbella International Film Festival in Spain, where it won the Best Documentary award. William Shatner attended the festival and accepted the award. The film also won a Telly Award in 2012. 

The world television premiere was a multi-platform release through Epix (TV channel)|EPIX, a joint venture between Paramount Pictures, MGM, and Lionsgate.  The film aired on July 21, 2011 on the EPIX channel, Epix SVOD service, EpixHD.com, and was available across Internet connected devices.   In October 2011 the film became available on Netflix.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 