The Trip to Bountiful
 
 
{{Infobox film
| name = The Trip to Bountiful
| image = Trip to bountiful.jpg
| writer = Horton Foote
| starring = {{Plainlist|
* Geraldine Page John Heard
* Carlin Glynn Richard Bradford
* Rebecca De Mornay
}}
| director = Peter Masterson
| producer = Horton Foote Sterling Van Wagenen
| music = J.A.C. Redford Fred Murphy
| editing = Jay Freund
| studio  = Bountiful Film Partners FilmDallas Pictures Island Pictures
| released =  
| runtime = 108 minutes
| country = United States
| language = English
| budget =
| gross = $7,491,903
}} 1985 film John Heard, Richard Bradford his television play of the same name.
 Bountiful for other places by this name.) Although set in Houston, Texas|Houston, Texas (as was the original play), the movie was filmed by director Peter Masterson in Dallas, Texas|Dallas.
 Best Writing, Screenplay Based on Material from Another Medium.

==Plot==
The film, set in the post-WWII 1940s, tells the story of an elderly woman, Carrie Watts (Page), who wants to return to her home, the small, rural agriculture based town (which was of course destroyed by the Depression and the Dust Bowl), where she grew up on the eve of the Great Depression, but shes frequently stopped from leaving Houston, Texas by her daughter-in-law and her overprotective son who will not let her travel alone.  Her son and daughter-in-law, of course both know, that the town has long since the disappeared, due to the Depression; and the long-term out-migration, caused by the draw-down of all the towns able-bodied men, to the wartime draft calls, as well as by the demand for industrial workers in the war production plants of the big cities located in Texas, and the other States.

Old Mrs. Watts is determined to outwit her son and bossy daughter-in-law, and sets out to catch a train, only to find that trains do not go to Bountiful anymore. She eventually boards a bus to a town near her childhood home. On the journey, she befriends a girl traveling alone (DeMornay) and reminisces about her younger years and grieves for her lost relatives.  Her son and daughter-in-law eventually track her down, with the help of the local police force. However, Mrs. Watts is determined. The local sheriff, moved by her yearning to visit her girlhood home, offers to drive her out to what remains of Bountiful.  The village is deserted, and the few remaining houses are derelict. Mrs. Watts is moved to tears as she surveys her fathers land and the remains of the family home. Her son eventually turns up, and drives her back to Houston.

==Cast==
* Geraldine Page as Mrs. Watts John Heard as Ludie Watts
* Carlin Glynn as Jessie Mae Richard Bradford as Sheriff
* Rebecca De Mornay as Thelma
* Kevin Cooney as Roy Norman Bennett as First Bus Ticket Man Harvey Lewis as Second Bus Ticket Man
* Kirk Sisco as Train Ticket Agent
* Dave Tanner as Billy Davis
* Gil Glasgow as Stationmaster, Gerard
* Mary Kay Mars as Rosella
* Wezz Tildon as Bus Passenger
* Peggy Ann Byers as Downstairs Neighbor
* David Romo as Mexican Man
* Tony Torn as Twin
* John Torn as Twin
* Alexandra Masterson as Drugstore Waitress
* Don Wyse as Doctor

==Home media==
On April 12, 2005, MGM released The Trip to Bountiful on DVD in region 1 US in a widescreen format.

==Awards==
Academy Award
* Best Actress: Geraldine Page
* Nominated: Best Adapted Screenplay: Horton Foote

Boston Society of Film Critics Award
* Best Actress: Geraldine Page

Golden Globe Award
* Nominated: Best Actress—Motion Picture Drama: Geraldine Page

Independent Spirit Award
* Best Female Lead: Geraldine Page
* Best Screenplay: Horton Foote
* Nominated: Best Director: Peter Masterson
* Nominated: Best Feature: Sterling Van Wagenen, Horton Foote

Mainichi Film Concours
* Best Foreign Film: Peter Masterson

Writers Guild of America Award
* Nominated: Best Adapted Screenplay: Horton Foote

==Remake== remake premiered Lifetime network. Vanessa Williams as Jessie Mae, Blair Underwood as Ludie and Keke Palmer as Thelma. Tyson and Williams also appeared in the Broadway revival prior to this. 

{| class="wikitable plainrowheaders sortable" style="width:100%;"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! scope="col"| Award
! scope="col"| Date of ceremony
! scope="col"| Category
! scope="col"| Recipients and nominees
! scope="col"| Result
|-
| rowspan="3"| Critics Choice Television Award  June 19, 2014
| Best Movie
|
|  
|-
| Best Actress in a Movie/Miniseries
| Cicely Tyson
|  
|-
| rowspan="1"| Best Supporting Actor in a Movie/Miniseries
| Blair Underwood
|  
|-
| rowspan="2"| 66th Primetime Emmy Awards August 25, 2014
| Outstanding Television Movie
| 
| colspan="2"  
|-
| Outstanding Lead Actress In A Miniseries or Movie
| Cicely Tyson
|  
|-
| rowspan="1"| 21st Screen Actors Guild Awards January 25, 2015
| Outstanding Performance by a Female Actor in a Miniseries or Television Movie
| Cicely Tyson
|  
|-
|}

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 