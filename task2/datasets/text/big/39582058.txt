Monsters: Dark Continent
 
 
{{Infobox film
| name           = Monsters: Dark Continent
| image          = MonstersDC.jpg
| alt            = 
| caption        = Official poster
| director       = Tom Green
| producer       = {{Plainlist|
* Allan Niblo
* James Richardson}}
| writer         = Jay Basu
| starring       = {{Plainlist| Johnny Harris
* Sam Keeley
* Joe Dempsie}}
| music          = Neil Davidge
| cinematography = Christopher Ross
| editing        = Richard Graham
| studio         = Vertigo Films
| distributor    = Vertigo Films
| released       =  
| runtime        = 119 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} science fiction monster film Gareth Edwards. Due to commitments with Godzilla (2014 film)|Godzilla, Edwards did not return to direct, but served as an executive producer. Filming began in March 2013, taking place in Jordan and Detroit.

The film premiered at the BFI London Film Festival on October 9, 2014. Originally scheduled for a wide release on November 28, the film was pushed back to May 1, 2015. 

==Plot==
Ten years on from the events of Monsters, and the Infected Zones have now spread worldwide. In the Middle East a new insurgency has begun. At the same time there has also been a proliferation of Monsters in that region. The Army decide to draft in more numbers to help deal with this insurgency.

==Cast==
 Johnny Harris: Frater
* Sam Keeley: Michael
* Joe Dempsie: Frankie
* Jesse Nagy: Conway
* Nicholas Pinnock: Forrest
* Parker Sawyers: Williams
* Kyle Soller: Inkelaar
* Sofia Boutella: Ara
* Michaela Coel: Kelly

==Production==

The film is the first feature to be directed by Green, who previously had directed episodes of the Channel 4 television programme Misfits.    Green wrote the screenplay with Jay Basu and the two had free reign to make what type of movie they wanted as long as it included monsters.  Taking inspiration from the military presence in the first movie, the two decided to make a war movie that explored what Green termed "socially relevant themes"   Gareth Edwards serves as the movies executive producer, but the films production ran concurrently with Edwards own Godzilla (2014 film)|Godzilla, thus he could not contribute much to the actual film  

==Release==
Vertigo Films premiered the film in London on 9 October 2014, as part of the BFI London Film Festival, with plans to release it in the United Kingdom on 28 November 2014, but eventually delayed its release to 27 February 2015.  The film was eventually released in the US on April 17, 2015, and in the UK on May 1, 2015. {{cite web|url=http://bloody-disgusting.com/home-video/3337766/fear-evolves-monsters-dark-continent-poster-art/|title=
Fear Evolves On ‘Monsters: Dark Continent’|work=BD|accessdate=2 March 2015}}  

===Reception===
The film received mixed reviews, ranging from high praise to harsh panning. It earned a 21% score on aggregator   called it "uncompromisingly boring and pointless".  More positive was Empire Magazine, which awarded it four stars, remarking "In its fantastical way, this is one of the most believable, pointed and sober films about the wars of the 21st century."  

A positive review came from Total Film, who awarded the film three out of five stars and said it was "ambitiously staged and impressively shot". 

==References==
 

==External links==
*  
*   at Twitter

 
 
 
 
 
 
 
 
 
 
 