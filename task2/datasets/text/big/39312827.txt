Sigaram Thodu
{{Infobox film
| name           = Sigaram Thodu
| image          = Sigaram Thodu.jpg
| caption        = Film poster
| director       = Gaurav
| producer       = Siddharth Roy Kapur
| writer         = Gaurav Narayanan
| starring       = Vikram Prabhu Monal Gajjar Sathyaraj
| music          = D. Imman
| cinematography = Vijay Ulaganath
| editing        = Praveen K. L.
| studio         = UTV Motion Pictures
| distributor    = 
| released       = September 12, 2014
| runtime        = 138 minutes
| country        = India
| language       = Tamil
| budget         =
| gross          =
}} Indian action action masala masala film written and directed by Gaurav and produced by UTV Motion Pictures. It has cinematography by Vijay Ulaganath, editing by Praveen K. L. and music by D. Imman.
The film released on 12 September 2014.

== Plot ==
Muralipandian (Vikram Prabhu) is the only son of Chellapandian (Sathyaraj), a disabled ex-policeman who expresses a desire to see his son in the police one day. However, Murali is against becoming a policeman due to an incident which happened in his childhood; when Chellapa lost his leg during police duty, his wife fell down the stairs on hearing the news, leading to her death and left a crippled Chellapa to single-handedly look after the young Murali. His ambition is to become a bank manager and he manages to get an interview for the same in a bank. While on a pilgrimage, Murali meets Ambujam (Monal Gajjar), a young doctor. Ambujam initially thinks ill of Murali due to unsavoury incidents which occur between them during the journey, but after she finds out about Muralis true character and Murali helps her grandmother Aishwarya (Kovai Sarala) recover from dehydration, she falls in love with him. She expresses a distaste for marrying policemen, which impresses Murali.

Meanwhile, a spate of robberies occur in Chennai. Money is stolen from several bank accounts through ATM machines. The robberies are led by a bank employee Shiva (Gaurav Narayanan) assisted by a karate teacher Lawrence (Charandeep). The duo manage to avoid being caught as they hide their identity by wearing bike helmets. Murali first encounters the duo robbing an ATM while returning from the pilgrimage and breaks Lawrences arm in a fight. An injured Lawrence vows to take revenge on Murali for breaking his arm.
 Tamil Nadu Police Academy for police training. He reluctantly attends the training for Chellapas sake and deliberately performs poorly during the training in the hope of getting expelled from the academy. Unfortunately, the head of the academy, Nagaraj, who happens to be Ambujams father and a former colleague of Chellapa, finds out about Muralis plan and makes a deal with him to avoid the matter reaching Chellapas ears; however badly Murali had performed during training, he will not be expelled from the academy. Instead, he will be made a sub-inspector, with the assurance that he can resign if he doesnt like the job after 30 days, after which he can marry Ambujam. Murali accepts, but this strains his relationship with Ambujam.

Shiva and Lawrence plan to steal money from ATMs using information from international credit cards. The robbery is a success, but the CCTV cameras in 2 different ATMs have exposed Shiva as one of the robbers due to his height. The duo are arrested by Murali, who is now a sub-inspector, but they manage to subdue the constables and escape later that night, while Murali is out on a date with Ambujam. They encounter Chellapa, mercilessly beat him up and shoot him in the chest. Ambujam feels sorry about what happened to Chellapa and finally accepts Muralis decision to become a policeman.

Buoyed by Ambujams approval, and also determined to take revenge on those who shot Chellapa, Murali takes up the robbery case with full vigour. He assumes that the duo did not work alone, instead having an accomplice. This proves to be true; the accomplice turns out to be a forger who made fake credit cards for the duo to steal money from the ATMs, but this evidence reaches a dead end when the accomplice reveals that he never saw the duo without their bike helmets on. Murali then interrogates the security guards of ATMs in the outskirts of Chennai as he feels that the robbers would have used them to conduct their robberies since money would not be refilled in such ATMs until it runs short of cash due to lesser number of withdrawals unlike in ATMs within the city where refilling of cash is done regularly. One of the guards interrogated turns out to be an accomplice of the robbers, providing them gadgets to assist them in the robbery. He however, doesnt know where they are. Murali decides to track the robbers phones with the intention of finding their location once someone calls them. In retribution, Shiva and Lawrence try to kill Chellapa, who is in the ICU, also kidnapping Ambujam in the process. Murali rushes to the hospital, where he sees Chellapa still alive, after which he pursues the robbers, finally encountering them on the banks of the Cooum river. He kills both of them and rescues Ambujam.

The movie ends with Murali being honoured by the police department in the presence of Ambujam and Chellapa, for his role in thwarting the ATM robbery.

== Cast ==
* Vikram Prabhu as Muralipandian (Murali)
* Sathyaraj as Chellapandian (Chellapa)
* Monal Gajjar as Ambujam
* K. S. Ravikumar as Ravi
* Sathish as Kay Kay
* Erode Mahesh as Adhi Moolam
* Gaurav as Shiva
* Charandeep as Lawrence
* Kovai Sarala as Aishwarya Pandu as Pandu
* Singampuli as a pickpocket
* Kalyanamalai Mohan
* Balakrishnan
* Nishanth as Nishanth
* Vinayak Raj as Mannavan
* Swaminathan as Santosham
* Krishnamoorthy as Vetrivel
* SVS KUmar as Swamiji
* Director Jackisheker as Bank customer victim - Very important role in this movie ≈
* Lollu Sabha Manohar as Arumugam
* Saran as young Muralipandian

== Production ==
In March 2012 it was reported that Gaurav who earlier directed Thoonga Nagaram was all set to start work on his next, Sigaram Thodu. He stated "The film, which is based on the relationship between a father and son, will star three big heroes, one from Tollywood and two from Kollywood, and will most likely be made as a bilingual. Richard and Yuvan Shankar Raja will be handling the cinematography and music respectively".  Vikram Prabhu was signed as the hero   and Monal Gajjar was roped in to do the female lead.  Sathyaraj was roped in to play a pivotal role.  Gaurav changed the initial team and signed cinematographer Vijay Ulaganath and music composer by D. Imman. 
 Chandi Devi Temple in Haridwar, Uttarakhand.  Director Gaurav said "It is Indias first movie to shoot the Gangotri temple opening ceremony amidst 20,000 people in −2 degree celsius".   A location in the Himalayas used for the song "Pidichirukku" in the film is only opened two days a year, so the team shot for a few days in 2013, before returning a year later to complete portions.

==Release==
The satellite rights of the film were sold to STAR Vijay.

==Critical reception==
Rediff wrote:"The director keeps you hooked from the first shot with his well-written script that cleverly weaves the commercial elements into the realistic narration" and also revealed that it is "a well-executed film with good performances".  Sify wrote:"On the whole here is a film that strikes a balance between style and substance. The director and his screenplay have been able to construct an engrossing actor –driven thriller with panache".  Behindwoods wrote:"Director Gaurav has chosen a script that warrants a lot of intelligence both in the screenplay and in the crime techniques associated, around which the movie travels".  Only Kollywood wrote that:" it is painstakingly written, grippingly made   and made with great confidence". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 