Girl on the Bridge
 
{{Infobox film
| name           = Girl on the Bridge
| image          = Girl_on_the_bridge_ver1.jpg
| caption        = United States theatrical poster
| director       = Patrice Leconte
| producer       = Christian Fechner
| writer         = Serge Frydman
| starring       = Vanessa Paradis Daniel Auteuil
| music          = 
| cinematography = Jean-Marie Dreujou
| editing        = Joëlle Hache
| distributor    = Paramount Classics (USA)
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| gross          = $1,708,839
| preceded_by    = 
| followed_by    = 
}}
Girl on the Bridge ( ) is a 1999 French film shot in black and white and directed by Patrice Leconte, starring Daniel Auteuil and Vanessa Paradis.

== Plot ==
 knifethrower Gabor knifethrowing act. The film then follows their relationship as they travel around Europe with the act. Their companionship and teamwork mean great luck for both of them. Then they get separated and their lives once again become luckless. The film ends on a bridge in Istanbul, this time with her saving him from suicide.

==Cast==
* Vanessa Paradis as Adèle
* Daniel Auteuil as Gabor
* Frédéric Pfluger as Contortionist
* Demetre Georgalas as Takis
* Catherine Lascault as Irene
* Isabelle Petit-Jacques as Bride
* Mireille Mossé as Miss Memory
* Didier Lemoine as TGV ticket conductor
* Mylène Farmer as Irene (uncredited guest appearance in a hallway and beside the stage)
* Bertie Cortez as Kusak
* Stéphane Metzger as Italian waiter
* Claude Aufaure as Suicide victim
* Farouk Bermouga as TGV waiter
* Nicolas Donato as Mr. Loyal
* Enzo Etokyo as Italian megaphone
* Giorgios Gatzios as Barker

==Release==
Paramount Classics acquired the United States distribution rights to this film and gave it a limited U.S. theatrical release on July 28, 2000; the film went on to gross $1,708,839 in U.S. theaters,  which was a good result for a non-English film.  Ruth Vitale (president of Paramount Classics at that time) declared herself pleased with the films performance in the U.S. market.  However, Paramount did not release the film on DVD until July 2008.

==See also==
 
* Impalement arts

==References==

 

==External links==
*  
*  
*   at Rotten Tomatoes

 

 
 
 
 
 
 
 
 
 



 
 