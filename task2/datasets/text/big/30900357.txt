Love Shack (film)
{{Infobox film
| name           = Love Shack
| image          = Love_Shack_(film).jpg
| alt            =  
| caption        = 
| director       = Gregg Saccon   Michael B. Silver Rick Jaffa   Amanda Silver   Steven Long Mitchel   Ivan H. Migel   Gregg Sacon   Thaddeus Wadleigh
| writer         = Gregg Saccon   Michael B. Silver
| starring       = Diora Baird
| music          = Matt Rollings
| cinematography = Thaddeus Wadleigh
| editing        = 
| studio         = 
| distributor    = Strand Releasing
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Love Shack  is a 2010 mockumentary about the adult film industry written, produced and directed by Gregg Saccon and Michael B. Silver. Other producers includes Rick Jaffa and Amanda Silver.

Love Shack reunites a dysfunctional "family" of adult film stars for a memorial porn shoot following the death of a legendary producer Mo Saltzman. 

==Plot==
 

==Cast==
 
* Diora Baird as LaCienega Torrez
* America Olivo as Fifi LeBeaux
* Ian Gomez as Mo Saltzman
* Molly Hagan as Debbie Vanderspiegl
* Brad Hall as Dr. Alan Rudnick
* Michael B. Silver as Jerry Sphincter
* Mark Feuerstein as Marty Sphincter
* Christopher Boyer as Mac Hollister
* Philippe Brenninkmeyer as Oliver Snowden-Hicks
* Mercy Malick as Autumn Lane
* Lenny Citrano as Roger Hogman
* Kyle Colerider-Krugh as Carl Munson
* Amberlee Colson as Hallie Lujah
* Jamie Denbo as Gretchen Becky
* Nancy Fish as Honey Malone
* Pete Gardner as Doug Vanderspiegl
* David Neher as Ned Billings
* Brian Palermo as Ted Haynes
* Ben Shenkman as Skip Blitzer
* Lindsey Stoddart as Whitney Sweet
* Paul Vaillancourt as Andre Cox
* Susan Yeagley as Kat Waters
 

==References==
*  
*  

{{Reflist|refs=
   
}}

==External links==
*  
*  
*  
 

 
 
 
 
 
 
 