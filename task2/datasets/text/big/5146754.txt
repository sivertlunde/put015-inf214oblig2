Mukh O Mukhosh
{{Infobox film
| name           = Mukh O Mukhosh
| image          = DVD Cover of first Bengali language movie Mukh O Mukhosh.jpg
| caption        = Mukh O Mukhosh DVD Cover
| director       = Abdul Jabbar Khan
| producer       = Nuruzzaman Shahidul Alam
| writer         = Abdul Jabbar Khan
| starring       = Inam Ahmed Zahrat Azra Ali Mansoor Abdul Jabbar Khan Kazi Khaliq Purnima Saifuddin
| playback singers = Abdul Alim Mahbuba Rahman
| music          = Samar Das
| Lyric          = M. A. Gafur (Sharathee)
| cinematography = 
| editing        = 
| distributor    = Iqbal Films
| released       = August 3, 1956
| runtime        = 99 minutes
| country        = East Pakistan (now Bangladesh) Bengali
| budget         = Rs. 64,000
| domestic gross = 
| preceded_by    = 
| followed_by    =
}}
Mukh O Mukhosh ( ) (The Face and the Mask) (1956) was the first Bengali language feature film to be made in East Pakistan (now Bangladesh). It was produced by Iqbal Films and directed by Abdul Jabbar Khan. The movie was released in East Pakistan on 3 August 1956. It was released in Dhaka, Chittagong, Narayanganj, and Khulna. The movie was a great success as viewers thronged to watch the first movie to be made in the region. It earned a total of Rs. 48,000 during its initial run.  , from Pakistani films.   

==Background==
 
The story of the movie was based on Abdul Jabbar Khans play, Dakaat (Robbers).  Khan started working on the movie from 1953. At that time, the film industry in erstwhile   for developing.   Playback singers for the two songs in the film were Abdul Alim and Mahbuba Hasnat (Rahman), wife of Khan Ataur Rahman. However, the song by Abdul Alim is lost, as the film of that part of the movie has deteriorated completely. Mahbuba Rahman sang the other song in the movie - Moner Boney Dola Laage Hashlo Dokhin Hawa. All songs of this movie had written by M. A. Gafur (Sharathee) and music by Samar Das.

==Cast==
* Inam Ahmed
* Ali Mansoor
* Zahrat Ara
* Abdul Jabbar Khan
* Kazi Khaliq
* Purnima
* Saifuddin
* Golam Mostafa

==References==
 

== External links ==
* 

 
 
 
 
 
 