Sailaab
  Indian film director Guru Dutt in 1956 in film|1956.

Released in April 1956, it stars Geeta Bali, as Kanchan and Abhi Bhattacharya as  Gautam. Smriti Biswas, Helen and Bipin Gupta also costar. 

The film was produced by Guru Dutts wife, Geeta Dutts brother, Mukul Roy.  The movie bombed at the box-office and as a result, Geeta Dutt had to declare personal bankruptcy. 

== Plot ==
Gautam (Abhi Bhattacharya), a rich young man goes to Assam to visit his father’s tea plantation. The plane in which Gautam is travelling is forced to make an emergency landing due to bad weather. Gautam gets hurt and suffers from amnesia. He falls in love with a young woman Kanchan (Geeta Bali) who responds to him even though she is part of a religious community that doesn’t allow its members to marry. Gautam’s father takes him back to Calcutta. Gautam’s mother dies and the shock of her death brings back Gautam’s memory. But as his memory returns he forgets Kanchan. Kanchan comes to Calcutta in search of Gautam and sees that he fails to recognize her. She returns to her community deciding to renounce the world. In time Gautam is able to recollect his association with her and he goes after her to Assam. The lovers are reunited.

==Cast==
  
* Ram Singh    
  
* Geeta Bali    
  
* Smriti Biswas    
  
* Abhi Bhattacharya    
  
* Bipin Gupta    
  
* Helen

==Soundtrack==
Mukul Roy was also the music director of this movie which had good songs. The list of songs of the movie is as follows:-

* Hai Yeh Duniya Kaunsi – Hemant Kumar
* Hai Yeh Duniya Kaunsi – Geeta Dutt
* Baje Dil – Lakshmi Roy
* Yeh Rut Yeh Raat Jawaan – Geeta Dutt
* Tan Pe Rang Sakhi – Geeta Dutt, Chorus
* Jiyra Baat Nahin Maane – Geeta Dutt
* Kisi Ke Pyar Ki Tasveer (Chand ke Aansoo Shabnam Bankar) – Geeta Dutt
* Aa Gayi Aa Gayi Re Raat Rang Bhar Aa Gayi – Geeta Dutt
* Nahin Deri Karo Meri Baiyaan Dharo – Geeta Dutt
* Prabhu Aayi Pujaran Tere Angana - Geeta Dutt

==External links==
* 

 

 
 
 
 
 
 
 