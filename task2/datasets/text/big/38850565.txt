Enemy (2013 film)
 
{{Infobox film
| name           = Enemy
| image          = Enemy poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Denis Villeneuve
| producer       = {{Plain list |
*M.A. Faura
*Niv Fichman
}}
| screenplay     = Javier Gullón
| based on       =  
| starring       = {{Plain list |
*Jake Gyllenhaal
*Mélanie Laurent
*Sarah Gadon
*Isabella Rossellini
 
}}
| music          = {{Plain list |
*Daniel Bensi
*Saunder Jurriaans
}}
| cinematography = Nicolas Bolduc
| editing        = Matthew Hannam
| studio         = {{Plain list |
*Mecanismo Films
*micro_scope
*Rhombus Media
*Roxbury Pictures
}}
| distributor    = {{Plain list |
*A24 Films  
*E1 Films  
*Alfa Pictures  
}}
| released       =  
| runtime        = 90 minutes 
| country        = {{Plain list |
*Canada
*Spain
}}
| language       = English
| budget         = 
| gross          = $3,373,480    
}} 2002 novel The Double.  The film stars Jake Gyllenhaal as two men who are physically identical, but different in terms of personality. Mélanie Laurent and Sarah Gadon co-star as the romantic partners of the men. It was screened in the Special Presentation section at the 2013 Toronto International Film Festival.   

Enemy earned five  , January 13, 2014.  It was named Best Canadian Film of the Year at the Toronto Film Critics Association Awards 2014. 

==Plot==
A man attends an erotic show at an underground club that culminates with a naked woman on the verge of crushing a live tarantula spider under her patent leather, high-heeled platform shoe. Elsewhere, a pregnant young woman sits on a bed, alone.

Adam Bell, a solitary college history professor who looks identical to the man at the sex show, rents a movie, Where Theres a Will Theres a Way, on the recommendation of a colleague. Adam sees an actor in a small role who looks exactly like him.

After doing some research online, Adam identifies the actor as Daniel St. Claire, the stage name for one Anthony Claire. Adam rents the other two films in which Anthony has appeared and develops an interest in the man, who appears to be his physical doppelgänger. Adams girlfriend Mary becomes troubled by the change in his behavior. Adam stalks Anthony, visiting his office and calling him at home. Everyone, including Anthonys pregnant wife Helen, confuses the two. In a separate scene a truly gigantic spider, species unknown, lurks among and above the skyscrapers of Toronto.

Adam and Anthony eventually meet in a hotel room and discover they are perfectly identical copies of each other, including a scar each man has on his left abdomen above the pancreas.  Adam is reserved and bookish while Anthony is hot-headed and sexual. After following Mary to work, Anthony confronts Adam, accuses him of having sex with his wife Helen, and demands Adams clothes and the keys to his car in order to stage a sexual liaison with Mary, promising to disappear from his life forever afterwards. Adam complies, and Anthony takes Mary to the hotel where the two men met. Meanwhile, Adam breaks into Anthonys apartment and gets into bed with Helen, who seems to realize her partner is different and asks Adam to stay.

At the hotel, Mary panics when she sees the mark where Anthonys wedding ring usually sits and demands to know who he is, as her boyfriend doesnt wear a ring. She forces Anthony to drive her home, but the two get into a fight, and the car is involved in a high-speed crash, presumably killing them both.

The next day Adam dresses in Anthonys clothes and find a key inside the jacket pocket: the same key that was used to get into the private club in the beginning of the movie. Adam is ready to begin life as Anthony. Helen gets out of the shower and enters the bedroom. Adam asks Helen if she is doing anything tonight and follows the question by telling her he will be busy tonight. As he turns the corner, he beholds a tarantula spider big enough to fill the room. It seems strangely frightened of him and cowers against the rear wall. Possibly even more incongruous, however, is Adams inexplicably muted reaction to this monstrosity; as he stares, his expression is far more resigned than surprised.

==Cast==
* Jake Gyllenhaal as Adam Bell / Anthony Claire
* Mélanie Laurent as Mary
* Sarah Gadon as Helen Claire
* Isabella Rossellini as Mother
* Kedar Brown as a security guard
* Darryl Dinn as the video store clerk
* Stephen R. Hart as Bouncer (uncredited)
* Jane Moffat as Eve (uncredited)
* Joshua Peace as Carl, Adams colleague
* Tim Post as Anthonys concierge
* Misha Highstead, Megan Mane, Alexis Uiga as the Ladies in the Dark Room
* Kiran Friesen as Sad, Broken Woman (uncredited)
* Paul Stephen as Dark Room Patron (uncredited)
* Loretta Yu as Receptionist (uncredited)

==Analysis==
A review in  , March 13, 2014. Retrieved March 28, 2015.   Both director Villeneuve and leading actor Gyllenhaal spoke of their desire to make the film a challenging exploration of the subconscious.    , Alex Suskind, The Playlist, Indiewire, March 11, 2014. Retrieved March 28, 2015.   To Villeneuve, Enemy is ultimately about repetition:  the question of how to live and learn without repeating the same mistakes. 

Regarding the two physically identical characters: "You dont know if they are two in reality, or maybe from a subconscious point of view, theres just one," said Villeneuve. "Its maybe two sides of the same persona … or a fantastic event where you see another  ."  . Hilary Lewis,  , January 2, 2015. Retrieved March 28, 2015. 

Forrest Wickman of  , March 14, 2014. Retrieved March 28, 2015. 

==Reception==
Enemy received generally positive reviews from critics and has a rating of 75% on   based on 26 reviews, indicating "generally favourable reviews".  A.O. Scott, movie critic for the New York Times, wrote: "In any case, much of the fun in “Enemy,” which is tightly constructed and expertly shot, lies in Mr. Gyllenhaal’s playful and subtle performances... Its style is alluring and lurid, a study in hushed tones and yellowy hues, with jolts of anxiety provided by loud, scary music."   "Enemy" was also praised by David Ehrlich of Film.com for having "the scariest ending of any film ever made." 

Enemy opened in a single theater in North America and grossed $16,161. The widest release for the film was 120 theaters and it ended up earning $1,008,726 domestically and $2,364,754 internationally for a total of $3,373,480. 

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 