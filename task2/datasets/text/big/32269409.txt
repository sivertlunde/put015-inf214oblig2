Nigahen: Nagina Part II
{{Infobox film
| name = Nigahen: Nagina Part II
| image = Nigahen2.jpg
| caption = Vinyl Record Cover
| writer =
| starring = Sridevi Sunny Deol Anupam Kher
| director = Harmesh Malhotra
| producer =
| music =
| lyrics =
| released = 1989
| language = Hindi
}}
 blockbuster Nagina (film)|Nagina. It is the first Bollywood movie sequel. However, as the movie was big flop at box office there were no other movie sequel made.

==Plot==
Following the events in Nagina (film)|Nagina, Nigahen is the story of Neelam, the daughter of Rajiv and Rajni. After the tragic car accident of her parents, Neelam is raised by her grandfather in the city and is finally brought back to her ancestral home in the country, as a young adult (who bears a striking resemblance to her mother, also played by Sridevi). There, she encounters and falls in love with Anand who as a child was kidnapped by Gorakh Nath, a powerful Tantrik. Claiming to have amnesia of the events, Anand woos Neelam and she falls in love with him. Little does Neelam know that Anand does not have amnesia and is working for Gorakh Nath: the Tantrik has set his eyes on a powerful gem (Mani), which grants the one who possesses it great powers. The only person who knows its whereabouts is Neelam, who unbeknown to her, was hypnotized by her late mothers Guardian Snakes into going to the old temple from the original story, and locating the gem (an event witnessed by Gorakh Nath).

Ultimately, Anand inadvertently falls in love with Neelam and reveals that he is cursed by Gorakh Nath: if Gorakh Nath ever catches him, he will turn him into a snake and entrap him - hence Neelam will never be safe with Anand. The story culminates with Neelam inheriting the powers of her late mother to defeat Gorakh Nath once and for all and live happily ever after with Anand.

==Cast==
* Sridevi as Neelam/Rajni
* Sunny Deol as Anand
* Anupam Kher as Gorakh Nath
* Gulshan Grover as Kumar
* Aruna Irani as Gayatri
* Jagdeep as Munshiji

==External links==
*  

 
 
 
 
 


 