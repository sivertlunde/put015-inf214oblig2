Manhattan Romance
 
{{Infobox film
| name           = Manhattan Romance
| image          = 
| alt            = 
| caption        = 
| director       = Tom OBrien
| producer       = Mike Harrop   Tom OBrien
| writer         = Tom OBrien
| starring       = Gaby Hoffmann   Katherine Waterston   Zach Grenier   Caitlin FitzGerald   Louis Cancelmi   Tom OBrien
| music          = 
| cinematography = Scott Miller
| editing        = Nick Houy
| studio         = Beacon Films
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Manhattan Romance is a 2014 American romantic comedy film directed and written by Tom OBrien. The film stars Gaby Hoffmann, Katherine Waterston, Zach Grenier, Caitlin FitzGerald and Louis Cancelmi.

== Cast ==
* Gaby Hoffmann as Emmy
* Katherine Waterston as Carla
* Zach Grenier as Trevor
* Caitlin FitzGerald as Theresa
* Louis Cancelmi as Jarrod
* Tom OBrien as Danny
* Ean Sheehy as Hal
* Paul OBrien as Uncle Bob
* Jessie Barr as Gayle

== External links ==
*  

 
 
 
 
 
 
 


 