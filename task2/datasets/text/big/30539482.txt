Mako: The Jaws of Death
{{Infobox film
| name           = Mako: The Jaws of Death
| image          = Mako-the-jaws-of-death-movie-poster.jpg
| caption        = Promotional poster for the film
| director       = William Grefe
| producer       = Bob Bagley William Grefé Doro V. Hreljanovic Paul A. Joseph Robert Plumb
| story          = William Grefe
| screenplay     = Robert W. Morgan
| starring       = Richard Jaeckel, Jennifer Bishop and Buffy Dee 
| music          = 
| cinematography = Julio C. Chavez
| editing        = Julio C. Chavez Ronald Sinclair
| studio         = Mako Associates Universal Majestic Inc. Cannon Films
| released       =  
| runtime        = 91 min
| country        = United States
| language       = English
| gross          =
| genre          = Horror , Drama , Thriller
}}
Mako: The Jaws of Death is a 1976 thriller film directed by William Grefe. The film is about a brooding loner who accidentally learns that he has a telepathic and emotional connection with sharks. He eventually rebukes society and sets out to protect sharks from people.  The film was set and shot on location in Key West, Florida.  This film is one of the first in the wave of films that sought to capitalize on the popularity of the 1975 feature film, Jaws (film)|Jaws.  "Mako: The Jaws of Death", with its sympathetic portrayal of sharks as the real "victims" of human exploitation, is notable in the maritime horror genre for having depicted the sharks as the heroes and man as the villain.

==Plot== Filipino shaman.  Becoming alienated from society, Stein lives alone in a small stilt house offshore of Key West, Florida.  He develops an ability to telepathically communicate with sharks.  He then sets out to destroy anybody who harms sharks.  People enter into his strange world to exploit his abilities and his shark "friends," including an unethical shark research scientist and a morbidly obese strip club owner (Buffy Dee) who wants to use a shark in his dancers acts.  Stein then uses these sharks to get revenge on anybody he considers a threat. 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 


 