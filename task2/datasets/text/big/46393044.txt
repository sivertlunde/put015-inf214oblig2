Fanfare of Love
{{Infobox film
| name = Fanfare of Love
| image =
| image_size =
| caption =
| director = Richard Pottier
| producer =  Michael Logan   Pierre Prévert   René Pujol   Robert Thoeren
| narrator =
| starring = Fernand Gravey   Betty Stockfeld   Julien Carette
| music = Joe Hajos 
| cinematography = Jean Bachelet   André Dantan  
| editing =       
| studio = Solar-Films 
| distributor = 
| released =15 November 1935
| runtime = 115 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Fanfare of Love (French:Fanfare damour) is a 1935 French comedy film directed by Richard Pottier and starring Fernand Gravey, Betty Stockfeld and Julien Carette.  The films art direction was by Max Heilbronner.

It has been remade in 1959 as Some Like It Hot.

==Cast==
*   Fernand Gravey as Jean 
* Betty Stockfeld as Gaby 
* Julien Carette as Pierre 
* Gaby Basset as Poupette 
* Jacques Louvigny as Alibert 
* Pierre Larquey as Emile 
* Madeleine Guitty as Lydia 
* Jane Lamy
* Ginette Leclerc  Palau
* Paul Lovetall
* Eugène Frouhins
* Anthony Gildès 
* Paul Marthès 
* Henri Vilbert 
* Roger Duchesne  Paul Demange 
* Josyane Lane

== References ==
 

== Bibliography ==
* Terri Ginsberg & Andrea Mensch. A Companion to German Cinema. John Wiley & Sons, 2012.

== External links ==
*  

 
 
 
 
 
 

 