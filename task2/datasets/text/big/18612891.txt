Doraemon: Nobita and the Animal Planet
{{Infobox film
| name           = Nobita and the Animal Planet
| image          = Nobita and the Animal Planet.jpg
| caption        =
| director       = Tsutomu Shibayama
| producer       =
| writer         =
| narrator       =
| starring       = Nobuyo Ōyama Noriko Ohara
| music          = Shunsuke Kikuchi
| cinematography =
| editing        =
| studio         = Asatsu
| distributor    = Toho Company
| released       =  
| runtime        = 100 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          = ¥1.91 billion  ($19 million)
}}
  is a feature-length Doraemon film which premiered on March 10, 1990.

==Plot==

It starts with Nobita seeing a mysterious pink gas at his house. He enters it and arrives at a planet where all animals that can talk. When he wakes up, he tells his dream to his friends. But they do not believe him. When back home, he sees a flower which  he had taken from that planet.
 
The night, Nobita goes to the bathroom and discovers the mysterious pink gas for the second time. When he enters it, Nobita arrives at a forest. Without thinking, Doraemon takes Nobita when that time has a strong wind. He says that his objective is to meet with the animals. They use the bamboo copter to fly. They finally discovered a little dog at the river with a raft that blown by the wind. Nobita helps it to the city that he told. There many of people waiting for that little dog. Finally, the little dog meets with its father while that finally revealed that the name of that dog was Chippo. Nobita invited for a tea party at Chippos family house. Chippo asked Nobitas house. He told from Tokyo, but Chippo didnt understand. But, Nobita and Doraemon needs to go home. With bamboo copter, they fly back to the pink gas.
 
But, they found something strange. It was a moon that was very big and more bigger than what usually seen from the earth. They finally discovered that this was a different planet. And it only can be accessed from the pink gas. After Doraemon fixes the machine, they go in the pink gas. After that, they realize they are back on earth. In the afternoon, Nobita hears from his friends that they are listening mysterious voices. When he goes back to his home, he founds that the voices are coming from the String less phone.

Nobita tells Doraemon that he can use his gadget, but Doraemon refuses. After Nobita gives him the Star Flower, Nobita, Doraemon, Shizuka and Suneo prepare for going to the Animal Planet. But Suneo wants to go with Gian. Gian comes and says that it became too long to prepare for him. After they go, they found that Chippo was in grave danger. The Nimuges had attacked their planet and captured Chippos cousin, Romi-Chan. Doraemon gives Nobita a gadget named "Lunar Luck" for which he was incredibly lucky.

Nobita Boarded the Star Ship and goes to the nimuges planet. Nobita hits a nimuge and puts on his clothes. But the head realizes that some spy was with them. He orders everyone to put off their masks. At the last, an other spy (except Nobita) was there and he ran. But the head hits him with his gun and orders Nobita and other men to put him in the cell. But he hits a man and tells Nobita to take Romi-Chan. He takes him and boards the Star Ship. He just runs out of time. After the day, in morning, there was a terrible war. All the animals take their positions and then it started .... After the war, they think that the nimuges were defeated, but not. After that, a ship (not small, very big) comes and catches the nimuges. A man comes out and says "Are you fine Bear-San?" Nobita replies yes. After they won the war, all the animals got cheerful and happy and Nobita goes back home and promises to come back.

== Cast ==
{| class="wikitable"
|-
! Character
! Voice
|-
| Doraemon
| Nobuyo Ōyama
|-
| Nobita Nobi
| Noriko Ohara
|-
| Shizuka Minamoto
| Michiko Nomura
|-
| Takeshi "Gian" Goda
| Kazuya Tatekabe
|-
| Suneo Honekawa
| Kaneta Kimotsuki
|-
| Chippo
| Mayumi Tanaka
|-
| Chippos Father
| Keaton Yamada
|-
| Chippos Mother
| Run Sasaki
|-
| Romi
| Kumiko Nishihara
|-
| Utan
| Kiyoshi Kawakubo
|-
| Goriros Father
| Masashi Hirose
|-
| Goriro
| Atsuko Mine
|-
| Pelican
| Chafurin
|-
| White Goat
| Eisuke Yoda
|-
| Pig Boy
| Yōko Matsuoka
|-
| Doctor
| Ryōichi Tanaka
|-
| Crow
| Naoki Tatsuta
|-
| Horse
| Masashi Sugawara
|-
| Spy Masato Hirano
|-
| Police Commander
| Masayuki Katō
|-
| Nimuge General
| Katsuji Mori
|-
| Nimuge Boss
| Jūrōta Kosugi
|-
| Nimuge Member
| Toku Nishio
|-
| Citizens
| Masako Matsubara Naoki Bandō
|-
| Real State Agent
| Takeshi Watabe
|-
| Company President
| Osamu Katō
|}

==References==
 

== External links ==
*    
*  

 
 

 
 
 
 
 
 
 