Escape from the Planet of the Apes
 
{{Infobox film name           = Escape from the Planet of the Apes image          = Escape from the planet of the apes.jpg border         = yes caption        = Theatrical release poster director  Don Taylor producer       = Arthur P. Jacobs writer         = Paul Dehn | based on       =   starring       = Roddy McDowall Kim Hunter Bradford Dillman Natalie Trundy Eric Braeden Sal Mineo Ricardo Montalbán cinematography = Joseph F. Biroc editing        = Marion Rothman studio         = APJAC Productions   distributor    = 20th Century Fox released       =   runtime        = 98 minutes language       = English music          = Jerry Goldsmith
| budget        = $2,060,000  gross          = $12,348,905 
}}
 Don Taylor Planet of Planet of the Apes sequels. It was followed by Conquest of the Planet of the Apes.

==Plot summary==
The preceding film, Beneath the Planet of the Apes, ends with the apes future Earth being destroyed by a nuclear weapon.
  Cornelius (Roddy Zira (Kim time warp. The salvage, repair and launch all happen off-camera during the final act of the previous film.

The apes arrive on Earth in 1973, splashing down off the Pacific coast of the United States. They are transported to a secluded ward of the Los Angeles Zoo, under the observation of scientists Stephanie Branton (Natalie Trundy) and Lewis Dixon (Bradford Dillman). The apes decide not to let the humans know that they can speak. They also agree not to let them know that Earth will be destroyed because of the Ape War. Later, the apes power of speech is revealed when Zira becomes impatient during an experiment. Soon after, Milo is killed by a zoo gorilla who becomes agitated during an argument amongst the three chimpanzees. Lewis tries to communicate with the apes that he is peaceful and he wishes to treat them as equals, and Cornelius and Zira form a friendship with him and Dr. Branton.

A Presidential Commission is formed to investigate the return of Taylors spaceship and determine how atypically intelligent apes came to be aboard it. The apes are brought before the Commission, where they publicly reveal their ability to speak. The council asks them about Taylor, but Cornelius and Zira tell them that they know nothing about him. They reveal that they came from the future and escaped Earth when war broke out. They are welcomed as guests of the government. Cornelius and Zira secretly tell Stephanie and Lewis that they did know about Taylor, explain how humans are treated in the ape-dominated future, and about the Earths destruction. Stephanie and Lewis are shocked, but they are sympathetic.  They tell Cornelius and Zira to keep this information secret until they can gauge the potential reaction of their hosts.

The apes become celebrities, and are lavished with gifts and media attention. They come to the attention of the Presidents Science Advisor Dr. Hasslein (Planet of the Apes)|Dr. Otto Hasslein (Eric Braeden), who discovers Zira is pregnant.  Fearing for the future of the human race, he offers her champagne (which she has developed a taste for) to loosen her inhibitions and questions her further. Her candid responses enable him to convince the Commission that Cornelius and Zira must be subjected to more rigorous questioning.

Hasslein insists that he simply wants to know how apes became dominant over men. Cornelius reveals that the human race will cause its own downfall and become dominated by simians, and that simian aggression against humans will lead to Earths destruction by a weapon made by humans. Zira explains that the gorillas started the war, and the orangutans supported the gorillas, but the chimpanzees had nothing to do with it. Hasslein suspects that the apes are not speaking the whole truth.

During the original hearing, Zira had accidentally revealed that she dissected humans in the course of her work. Hasslein orders Lewis to administer a truth serum to her while Cornelius is confined elsewhere. Lewis assures Zira that the serum will have the same effect as champagne. As a result of the serum, Hasslein learns details about Ziras examination and experimentation on humans.
 William Windom). Cornelius labels Hasslein and the others savages for their treatment of Zira. Zira reminds Cornelius that she did the same thing to humans and Taylor called them savages. Zira is relieved to have revealed the truth because she was tired of lying. Cornelius fears that the truth will get them killed. An orderly refers to their unborn child as a "little monkey"; Cornelius, having previously expressed his disgust at that term, loses his temper and knocks a food tray out of the orderlys hands.  The orderly falls to the floor; Cornelius believes that the man is only unconscious, but he is actually dead. Hasslein uses the tragedy in support of his claim that the apes are dangerous to humanity, and he calls for their execution. The President reluctantly orders that Ziras pregnancy be terminated and that both apes be sterilized, but will not endorse punishment for the orderlys death until due process has been served. Branton and Dixon help the apes to escape, and find them shelter in a circus run by Armando (Planet of the Apes)|Señor Armando (Ricardo Montalbán), where an ape named Heloise has just given birth. Zira gives birth to a son, whom she names Milo, in honor of their deceased friend.

Hasslein, knowing that Ziras labor was imminent, orders a search of all circuses and zoos. Armando insists that for their own safety the apes must not stay. Lewis gives Cornelius a pistol. Cornelius, formerly a pacifist, accepts the pistol knowing he may have to use it as a last resort. The apes take refuge on an abandoned ship in Los Angeles Harbor.  Hasslein tracks them there, and finds Zira resting with an infant. Hasslein shoots Zira after she refuses to hand over the infant, and then fires several shots into the infant.  Cornelius returns fire and kills Hasslein. Cornelius is shot by an unseen sniper and falls. Zira tosses the dead baby over the side and crawls to die with her husband, witnessed by a grieving Lewis and Stephanie.

Armando reveals that Zira and Cornelius switched babies with the common ape Heloise before their escape. Armando watches as the infant Milo plaintively asks for his mother.

==Cast==
* Roddy McDowall - Cornelius
* Kim Hunter - Zira
* Bradford Dillman - Dr. Lewis Dixon
* Natalie Trundy - Dr. Stephanie Branton
* Eric Braeden - Dr. Otto Hasslein William Windom - President
* Sal Mineo - Dr. Milo
* Albert Salmi - E-1
* Jason Evers - E-2 John Randolph - Chairman
* Harry Lauter - General Winthrop
* M. Emmet Walsh - Aide
* Roy Glenn - Lawyer Peter Forster - Cardinal
* Norman Burton - Army Officer William Woodson - Naval Officer
* Tom Lowell - Orderly
* Gene Whittington - Marine Captain
* Donald Elson - Curator
* Bill Bonds - TV Newscaster
* Army Archerd - Referee James Bacon - General Faulkner
* Ricardo Montalbán - Armando
 Cornelius which he played in the first film but not in the second. A new character of Dr. Milo (Planet of the Apes)|Dr. Milo is introduced played by actor Sal Mineo, who hoped his career would gain from the new project much as McDowalls career had from participating in the first film. Charlton Heston, star of the first film and supporting actor in the second, appears in this third installment only in two brief flashback sequences.

==Production== Planet of Don Taylor for its lighthearted humor and for focusing on the chimpanzee couple.

Dehn also added to the latter part of the film regarding the chase for Zira, Cornelius and their son references to the racial conflicts and a few religious overtones to the story of Jesus - a line of dialogue even has the President comparing the plan to kill an unborn child to the Massacre of the Innocents.   While Kim Hunter had to be convinced by the studio to make Beneath, she liked the script for Escape from the Planet of the Apes and accepted to work on it, though Hunter also stated that "I was very glad I was killed off" and Zira was not required anymore after that film. Hunter stated that despite the friendly atmosphere on the set she and Roddy McDowall felt a sense of isolation for being the only people dressed as chimpanzees. Production was rushed due to the low budget, being filmed in only six weeks,  from November 30, 1970 to January 19, 1971. 

==Critical reception==
Reviews for the film were very positive. It currently holds a 78% approval rating on Rotten Tomatoes. 

== Notes and references ==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 