The World's Greatest Athlete
{{Infobox film
| name           = The Worlds Greatest Athlete
| image          = The Worlds Greatest Athlete poster.jpg
| caption        = Theatrical release poster
| director       = Robert Scheerer Bill Walsh Gerald Gardner
| narrator       =
| starring       = John Amos Roscoe Lee Browne Tim Conway Dayle Haddon Jan-Michael Vincent
| music          = Marvin Hamlisch
| cinematography =
| editing        = Walt Disney Productions Buena Vista Distribution
| released       = February 14, 1973
| runtime        = 93 min.
| country        = United States
| language       = English
| gross          = $22,583,370 
| preceded by    =
| followed by    =
}} Walt Disney Track & Field Championship.

==Plot== Merrivale College; their teams invariably lose.  A series of plot coincidences sends the pair to Africa, where they catch sight with their Safari guide Morumba (Don Pedro Colley) of the Tarzan-like Nanu, who can outrun a cheetah in full bound.

Seeing this, the coaching staff quickly whip out their recruitment pen and papers, but soon fall (literally) into the clutches of Nanus godfather, spiritual leader Gazenga (Browne) whose assistant (Clarence Muse) remains in Africa.  Nanu, it develops, is an orphan and an innocent child of the bush.  Gazenga believes that throwing Nanu into the world of competitive United States college athletics would interfere with his spiritual development.

Despite Gazengas concerns, the ambitious coaches persuade Nanu to join the Merrivale College program.  From this point forward, the plot is driven by a combination of slapstick and suspense, for Nanus destiny as the Worlds Greatest Athlete will annoy several powerful people who are used to getting their way.
 Machiavellian plotting of the villains all play roles in the action as the movie heads toward the final track meet.  The atmosphere of American competition does indeed threaten Nanu, but he is saved from disintegration by love interest Jane Douglas (Haddon).  Jane and Nanus budding relationship angers rival Leopold Maxwell (Danny Goldman), whose attempts to sabotage the budding star build toward a crescendo as the ultimate competition approaches. The climactic track meet is peppered with commentary by American Broadcasting Company|ABC-TV sportscaster Howard Cosell, playing himself.

The movie ends with a framing device in which the hapless coaches are depicted trying to recruit a new athletic phenomenon, this time in China.

==Production notes== University of Newhall neighborhood of Santa Clarita, California.  The live-action jungle scenes were shot at Caswell Memorial State Park, on the Stanislaus River outside of Ripon, California.

Nanus closest companion is a pet tiger named Harry, which he brings with him from Africa to California; however, tigers are not native to Africa.  Nanu explains that the tiger emigrated from India to Africa as a cub.

==Reception==
The movie was one of the most popular releases of 1973, earning $10,600,000 in North American rentals that year. 

Upon the films release, The New York Times wrote: "its a dream that is more often simple-minded than simple and generally as hilarious as finishing fourth in the mile run. It should be stressed, however, that this ribbing of the Tarzan myth runs a good, clean course that should grab all red-blooded sports fans up to and including the 14-year-old group. It might be added that everyone from coach Amos to Jan-Michael Vincent, in the title role, athletically tries without much success to make all this good-natured nonsense funny". 

==See also==
*Worlds Greatest Athlete
*Decathlon
*List of American films of 1973

==References==
 

==External links==
*   
*  
*  
*  

 
 
 
 
 
 
 