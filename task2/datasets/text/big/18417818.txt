A Temporary Truce
{{Infobox film
| name           = A Temporary Truce
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         = George Hennessy
| starring       = Blanche Sweet Charles Hill Mailes
| music          =
| cinematography = G. W. Bitzer
| editing        =
| distributor    =
| released       =  
| runtime        = 17 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 short silent silent Western Western film directed by D. W. Griffith and starring Blanche Sweet. A print of the film survives in the film archive of the Library of Congress.   

==Cast==
* Charles Hill Mailes as Mexican Jim
* Claire McDowell as Mexican Jims Wife Charles Gorman as Jack, the Prospector
* Blanche Sweet as Alice, the Prospectors Wife
* W. Chrystie Miller as The Murdered Indian / Indian on Street
* Christy Cabanne as An Indian
* William A. Carroll as In Bar / Among Rescuers Frank Evans as In Bar / Among Rescuers
* Robert Harron as The Murdered Indians Son
* Bert Hendler as In Bar Harry Hyde as Among Rescuers / Outside Pony Express Office
* J. Jiquel Lanoe as An Indian / Among Rescuers
* Wilfred Lucas as An Indian
* Mae Marsh as A Murdered Settler
* Frank Opperman as A Drunken Cutthroat / The Indian Chief / The Bartender
* Alfred Paget as A Drunken Cutthroat / An Indian / Among Rescuers
* Jack Pickford as An Indian
* W. C. Robinson as An Indian / In Bar / Among Rescuers Charles West

==See also==
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 