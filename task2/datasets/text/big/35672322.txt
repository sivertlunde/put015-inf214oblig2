How to Start a Revolution
 
 
{{Infobox film
| name           = How to Start a Revolution
| image          = How to Start a Revolution (film) poster.jpeg
| alt            = Movie poster showing close-up of Gene Sharps face, with title "How to Start a Revolution", and list of awards.
| caption        = 
| director       = Ruaridh Arrow
| producer       = Richard Shaw;  Cailean Watt, assistant producer; James Otis, executive producer
| writer         = Ruaridh Arrow Ahmed Maher, Ausama Monajed
| music          =  Philip Bloom 
| editing        = Mike Crozier,  Lorrin Braddick
| studio         =  TVF International 
| released       =  
| runtime        = 85 minutes
| country        = Scotland
| language       = English
| budget         = 
| gross          =
}}
How to Start a Revolution is a BAFTA award winning British documentary film about Nobel Peace Prize nominee and political theorist Gene Sharp, described as the worlds foremost scholar on nonviolent revolution. The film describes Sharps ideas, and their influence on popular uprisings around the world. Screened in cinemas and television in more than 22 countries it became an underground hit with the Occupy Wall St Movement. 

==Synopsis==
Directed by British journalist Ruaridh Arrow the film follows the use of Gene Sharps work across revolutionary groups throughout the world.  There is particular focus on Sharps key text From Dictatorship to Democracy  which has been translated by democracy activists into more than 30 languages and used in revolutions from Serbia and Ukraine to Egypt and Syria. The film describes how Sharps 198 methods of nonviolent action have inspired and informed uprisings across the globe.

==Cast==
A primary character of the film is Gene Sharp, founder of the Albert Einstein Institution, and a 2009 and 2012 nominee for the Nobel Peace Prize.    (accessed 2 May 2012)  Sharp has been a scholar on nonviolent action for more than 50 years, and has been called both the "Machiavelli of nonviolence" and the "Clausewitz of nonviolent warfare."  Robert "Bob" Ahmed Maher, April 6 democracy group Egypt; and Ausama Monajed, Syrian activist.

==Background and production==
Scottish journalist Ruaridh Arrow, who wrote, directed, and co-produced the film, explained that he first learned about Gene Sharps work as a student, and then heard that Sharps booklets were turning up on the sites of many revolutions. But Sharp himself remained largely unknown. In explaining his motivation to make the film, Arrow stated that
  }}
The film was privately funded by Ruaridh Arrow and additional funding was raised through the US crowdfunding site Kickstarter.  The film raised $57,342 in just under 4 weeks   making it the most successful British crowdfunded film currently completed.  Several high-profile figures are credited by the producers with supporting the crowdfunding project, including director Richard Linklater and actress Miriam Margoyles.   Completion funding was donated by US art collector James Otis who sold the largest collection of Gandhi possessions including Gandhis iconic glasses and sandals in 2009. Otis stated that he was selling the items to help fund nonviolent struggle projects and is described as the Executive Producer of the film. 

Principal photography began in May 2009 with Director of Photography   in February 2011 but his camera equipment was seized by Egyptian secret police on landing and key sequences had to be filmed on the iphone4.  Arrow reported live from Tahrir Square for BBC News during this period. 

==Release and accolades==
 The premiere was held in Boston on 18 September 2011, the day after the Occupy Wall St protests officially began in New York. The film received a standing ovation and won Best Documentary and the Mass Impact award at Boston Film Festival,   and went on to be screened by Occupy camps across the US and Europe including the Bank of Ideas in London.   

The European premiere was held at Raindance Film Festival in London where the film received the award for Best Documentary.    Subsequent awards have included Best Documentary Fort Lauderdale International Film Festival 2011, Special Jury Award One World Film Festival Ottawa, Jury Award Bellingham Human Rights Film Festival and Best Film, Barcelona Human Rights Film Festival. The film won the Scottish BAFTA for new talent in April 2012 and shortlisted for a Grierson Award in July 2012. 
 TVF International Michael Rosser (11 August 2011)   (accessed 2 May 2012)  in the UK and 7th Art Releasing in the US.  The film has reportedly been translated into nine languages, including Japanese and Russian.  The Albert Einstein Institute has reported that the film has been shown internationally on several television stations. 

==Reception== Time Out London, "a reminder of the importance of intellectual thought to the everyday".  The Huffington Post said it was a "vital conversation starter and educational tool in a world awash with violence"  and the UKs Daily Telegraph described it as a "World conquering Documentary".  The New York Times  called it a "noble documentary" but criticised the absence of historical context of nonviolent struggles pre-dating Sharp. Variety (magazine)|Variety described the film as "straightforward", "informative", and "with potential to be updated as world events unfold," stating it "should have a long shelf life". Negative references have been made to the use of dramatic music during certain sequences. 

==Influence==
How to Start a Revolution was released on 18 September 2011 the day after the first Occupy protests in Wall St, New York.  The film was described as the unofficial film of the Occupy movement  and shown in camps across the US and Europe.    It was one of a number of high-profile events held in Londons Bank of Ideas along with a concert by British Band Radiohead. 

In 2012 following the contested Mexican General Election one of the countries largest newspapers reported that protestors were circulating a pirated Spanish translation of How to Start a Revolution which had gone viral in the country.  The translation was viewed over half a million times in the space of three days.  Reports have also been published citing the airing of the film on Spanish television concurrent with widespread discussion of Sharps work in the Spanish anti-austerity 15-M Movement.  

The academic premiere was hosted by the Program on Negotiation at Harvard Law School on 11 October 2011,  and In February 2012, How to Start a Revolution was screened to an audience of MPs and Lords in the UK Houses of Parliament by the All Party Parliamentary Group on Conflict Issues (APPGCI) which was attended by Gene Sharp. 

A film about the making of How to Start a Revolution, entitled Road to Revolution, was screened in January 2012 by Current TV in the UK.  

==Touch Documentary==
In 2012 How to Start a Revolution was among the first "Touch Documentaries" to be released using the Apple iPad platform.  The film was integrated into the platform along with four of Gene Sharps books in several languages including From Dictatorship to Democracy and several of Sharps lectures. The app is supplemented by analysis and satellite mapping which is offered up to the viewer while watching the film. http://www.internationalpeaceandconflict.org/forum/topics/pcdn-review-of-the-how-to-start-a-revolution-ipad-app?xg_source=activity#.USUXybR2tUQ   A "Revolution Monitor" is also included which fuses Google Earth maps with Twitter displaying tweets and YouTube links from revolutionary groups and individuals when countries of interest are touched by the viewer. 
A review by The Peace & Collaborative Network described the app as "simply a must-have among peace studies scholars, those actively working to start or reorganize revolutions, or anyone who is interested in the logistics, history, and outcomes of nonviolent revolutions"  
The How to Start a Revolution touch documentary was shortlisted for the International Best Digital Media award in the One World Media Awards 2013  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 