Majaa
{{Infobox film
| name           = Majaa
| image          = Majaa DVD Cover.jpg
| caption        = DVD Cover Shafi
| screenplay     = Viji Radhika
| story          = Benny P Nayarambalam Vikram  Asin  Vijayakumar Murali Murali Manivannan
| producer       = Rockline Venkatesh Vidyasagar
| cinematography = Balasubramaniem	
| editing        = V. T. Vijayan
| studio         = KVR Productions Rockline Entertainment Aascar Films International
| released       = 2 November 2005
| runtime        = 138 minutes
| country        = India
| language       = Tamil
| budget         =  10.7 crore
}}
 Murali and Biju Menon. The music is composed by Vidyasagar (music director)|Vidyasagar. It tells the story of two adopted children changing from their old, mischievous ways of life. The film is a remake of Shafi (director)|Shafis own Malayalam film Thommanum Makkalum.

==Plot==
Govindan ( ) and Arivumathi (Vikram (actor)|Vikram). Years go by and the two, Aadhi and Madhi, decide to stop stealing and mend their ways and lead a hardworking life along with their father. They migrate to neighboring village and meet a retired agricultural officer, Chidambaram (Vijayakumar (actor)|Vijayakumar) who is in deep debt and is under pressure from the villages landlord, Kalingarayar (Murali) to clear his debts. In efforts to help Chidambaran, Mathi confronts Seetha Lakshmi (Asin Thottumkal|Asin), Kalingarayars daughter, who comes to collect the money Chidambaram owes her father. Seetha Lakshmi starts to acquire a liking for Mathi but keeps it hidden due to her fathers atrocious temper. In an attempt to teach Kalingarayar a lesson, Mathi forcibly ties the mangalsutram around Seethas neck. Kalingarayar, realizing his daughters love for Mathi, comes down to arrange a grand remarriage between the two. But things go awry when Manicka Vel (Biju Menon), Seetha Lakshmis maternal uncle, comes to town in an effort to stop the wedding between the two as he has plans of marrying her and wiping her family fortune.

==Cast and crew==
Cast : Vikram as Arivumathi
* Pasupathy as Aadi Asin as Seetha Lakshmi
* Vadivelu as Pulipaandi
* Manivannan as Govindan
* Biju Menon as Manicka Vel Vijayakumar as Chidambaram Murali as Kalingarayar
* Nithin Sathya 
Crew: Shafi
* Cinematography : K. Balusubramaniyam
* Editing : B. Lenin
* Producer : Rockline Venkatesh Vidyasagar
* Soundarya R. Ashwin

==Production==
Vikram saw Thommanum Makkalum and he asked Shafi to remake it.  The film was launched at AVM Studios in 2005, a huge set of village was erected at studios.  Asin selected to play heroine after Jyothika and Trisha rejected.  The crew went to Australia to shoot a song.  Vikram worked as an assistant director under Shafi for this film. 

==Soundtrack==
{{Infobox album |  
| Name       = Majaa
| Type       = Soundtrack Vidyasagar
| Cover      = Majaa Audio Cover.jpg
| Caption    = Front cover
| Released   = 2005
| Recorded   =  Feature film soundtrack
| Length     =
| Label      = Aditya Music An Ak Audio Vidyasagar
| Last album = Kaiyoppu (2005)
| This album = Majaa (2005)
| Next album = Periyar (2007 film)|Periyar (2005)
}}

{| class="wikitable" width="70%"
! Song title !! Singers
|-
| "Thai Maasam" || Shankar Mahadevan, Anuradha Sriram
|-
| "Chi Chi Chi" || Harini, Shankar Mahadevan, Savitha Reddy, Viji, Karthik
|-
| "Hey Pangaali" || Udit Narayan, Manickka Vinayakam
|-
| "Podhumadaa Saami" || Kailash Kher
|-
| "Sollitharavaa" || Madhu Balakrishnan, Sadhana Sargam
|-
|}

==Release==
The film was released in 2 November 2005 on the eve of Diwali.

===Reception===
Sify wrote:"The major flaw of Majaa is its wafer-thin story and screenplay, a mediocre subject to be remade from Malayalam,   the film gives you a dreary sense of déjà vu, a feeling that one has seen it before. 

===Box office===
It was  declared as an average grosser.  

==References==
 

==External links==
 

 
 
 
 
 
 
 