Xcuse Me
{{Infobox film
| name     = Xcuse Me
| image    = Xcuse_Me_poster.jpg
| caption  = 
| director = N. Chandra
| producer = N. Chandra
| writer   = 
| starring = Sharman Joshi Sahil Khan Sonali Joshi Jaya Seal
| music    = Sanjeev Darshan
| released =  
| runtime  = 
| country  = India
| language = Hindi
}}

Xcuse Me is a 2003 Hindi comedy film. It is the sequel to N. Chandras 2001 film Style (2001 film)|Style which was a box office success. Xcuse Me was released in September 26, 2003 and a moderate success at the box office.

==Plot==
Bantu (Sharman Joshi) and Chantu (Sahil Khan) are unemployed, and are not able to get any jobs due to lack of experience. They come across an advertisement for a hotel management program in Goa, and make their way there. Once there, they hoodwink the trainer by posing as the nephew of the owner of the hotel, and thus enroll themselves in the training course. In the course of the program, they expose several employees and guests as cheats, earning their wrath but also the admiration of the two daughters of the hotel owners. Things turn sore for the two when the guests and former employees get together to avenge their humiliation. 

==Soundtrack==
The music score was given by Sanjeev-Darshan and Abbas Katka penned the lyrics.

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Boom Boom"
| Sanjeev Darshan
|-
| 2
| "Excuse Me"
| Hema Sardesai
|-
| 3
| "Ishq Hua"
| Shaan (singer)|Shaan, Shreya Ghoshal
|-
| 4
| "Ishq Hua (II)"
| Udit Narayan, Shreya Ghoshal
|-
| 5
| "Ladki Ladki"
| Abhijeet Bhattacharya|Abhijeet, Shaan
|-
| 6
| "Yeh Tu Kya Kar Rahela Hai"
| Abhijeet, Sunidhi Chauhan
|}

==References==
 

==External links==
*  

 
 
 
 
 
 

 
 