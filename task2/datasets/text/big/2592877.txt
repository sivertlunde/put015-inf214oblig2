Love Stinks (film)
 

{{Infobox film | 
  name= Love Stinks|
  image =  Love Stinks poster.jpg|
  director= Jeff Franklin|
  producer= Adam Merims |
  writer= Jeff Franklin |
  cinematography = Uta Briesewitz |
  editing = Richard Candib |
  starring= French Stewart Bridgette Wilson Bill Bellamy Tyra Banks Steve Hytner Jason Bateman Tiffani Thiessen|
  distributor=  |
  released= September 10, 1999|
  runtime= 94 minutes |
  language= English |
  budget= $4 million |
  gross= $2,924,635 |
  music       = Bennett Salvay |
  awards         = |
}}

Love Stinks is a 1999 comedy starring French Stewart, Bridgette Wilson, Bill Bellamy and Tyra Banks. It was written and directed by Jeff Franklin.

==Plot summary==


Seth (Stewart), a sitcom writer-producer, meets Chelsea (Wilson), an interior decorator, at his best friends (Bellamy) wedding. Hes immediately sexually attracted to her while shes instantly attracted to his single-ness. They both ditch their wedding dates and start their own date that same night.

Chelsea changes from her   until Chelseas boss interrupts them and presumably fires Chelsea.
 The War of the Roses look like childs play.

==Cast==
* Seth Winnick – French Stewart
* Chelsea Turner – Bridgette Wilson
* Larry Garnett – Bill Bellamy
* Holly Garnett – Tyra Banks
* Marty Mark – Steve Hytner
* Jesse Travis – Jason Bateman
* Rebecca Melini – Tiffani-Amber Thiessen

==Soundtrack==
# Love Stinks – Andru Branch
# Lets Get It On – Marvin Gaye
# Cant Help Falling in Love – Elvis Presley
# I Promise – B. Chevis
# Glaym Lyfe – Shady Montage
# Fairy Tales – Kevin Perez
# Why Ya Wanna Do Me (Like This) – Chip Allen
# Playa Wayz – Andre Branch
# This Thing Called Love – Jonathan Douglas
# Na, Na, Na – Brion James
# I Feel Good – Alexander

==External links==
* 
* 

 
 
 
 
 
 

 