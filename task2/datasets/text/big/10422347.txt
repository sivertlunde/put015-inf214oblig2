Chermin
 
 
{{multiple issues|
 
 
}}
{{Infobox film name           = Chermin image =  caption        = director       = Zarina Abdullah producer       = Zarina Abdullah starring       = Natasha Hudson Deanna Yusoff music          = Adeline Wong distributor    = Golden Screen Cinemas released       = 22 March 2007 runtime        = 100 minutes country        = Malaysia awards         = language       = Malay budget         = preceded_by    = followed_by    =
}}

Chermin (Mirror) is a 2007 Malaysian horror film directed by Zarina Abdullah.

== Synopsis ==
The story is about a girl name Nasrin who got herself into a tragic car accident. Her face is ruined. Her mother discovers an antique mirror. The mirror is not an ordinary mirror, a spirit trapped inside it makes the mirror able to reflect what Nasrin wants to see. Nasrin becomes obsessed with the mirror. On a quest to regain her past beauty, Nasrin submits herself to the mirror spirit by satisfying the mirror’s need for blood and revenge.

==Cast==

===Principals===
* Deanna Yusoff as Mastura
* Natasha Hudson as Nasrin
* Khatijah Tan as Mak Siti
* Farid Kamil as Yusof

===Supporting===
* Maimon Mutalib as Mak Ngah
* Sheila Mambo as Minah
* Shoffi Jikan as Zakaria
* Farah Ahmad as Rosnah
* Catriona Ross as Yasmin
* Haryanto Hassan as Hassan
* Lisdawati as Zahrah
* Dato Mustapha Maarof as Pak Din
* Ghazali Abu Noh as Bomoh (Witchdoctor)

==Awards and nominations==

===Awards===
* 2007: 20th Malaysian Film Festival: Most Promising Director: Zarina Abdullah
* 2007: 20th Malaysian Film Festival: Most Promising Actress: Natasha Hudson

===Awards Nominated===
* 2007: 20th Malaysian Film Festival: Best Film
* 2007: 20th Malaysian Film Festival: Best Sound Effect
* 2007: 20th Malaysian Film Festival: Best Film Director
* 2007: 20th Malaysian Film Festival: Best Actress
* 2007: 20th Malaysian Film Festival: Best Actress in a Supporting Role
* 2007: 20th Malaysian Film Festival: Best Screenplay
* 2007: 20th Malaysian Film Festival: Best Cinematography
* 2007: 20th Malaysian Film Festival: Best Editor
* 2007: 20th Malaysian Film Festival: Best Original Music Score
* 2007: 20th Malaysian Film Festival: Best Art Direction

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 