Red Tail Reborn
{{Infobox film
| name           = Red Tail Reborn
| image          = Red Tail Reborn cover.jpg
| image size     = 
| alt            = Watercolor of a dark-skinned man in uniform with headgear flanked by similarly dressed men in the background. A plane also flies in the background. Large dark letters read "Red Tail Reborn" at the top.  Other words appear in smaller dark print such as "of determination" and "of sacrifice". Large White letters read "Adam White Film" at the bottom.
| caption        = DVD cover
| director       = Adam N. White
| producer       = Hemlock Films
| writer         = Adam N. White (writer) Hallie Sheck (co-writer)
| narrator       = Michael Dorn
| starring       = Pilots and Crew of the 332d Fighter Group
| music          = 
| cinematography = 
| editing        = 
| studio         = Hemlock Films
| distributor    = 
| released       =  
| runtime        = 54 min 
| country        = United States English
| budget         = 
| gross          = 
}}
Red Tail Reborn is a 2007 historical documentary film by Adam White about the Commemorative Air Forces Red Tail Project.  The project involves the restoration, exhibition and maintenance of a World War II P-51 Mustang flown by the United States Air Force 332d Fighter Group.  The exhibition of this plane is considered to be a traveling and flying tribute to the Tuskegee Airmen. In addition to increasing awareness of the travails of the Tuskegee Airmen, this film served to highlight the Red Tail Project fundraising effort to rebuild the plane after a 2004 crash.

The film was initially broadcast and released on DVD in 2007 in the directors home state of Ohio, winning local Emmy Award recognition.  The following year, it was broadcast nationally for the first time on Public Broadcasting Services (PBS) stations.  The film had a sequel called Flight of the Red Tail.

==Story==
Red Tail Reborn is described by Allrovie as "... the story of the brave African-American pilots who risked their lives in order to ensure that peace prevailed, and the people who have dedicated their lives to ensure that they are not forgotten."  It tells three stories in one: the story of the Tuskegee Airmen, the history of the Red Tail Mustangs they flew (one of which is the subject of the restoration efforts) and the story of the late Don Hinz who crashed the first restored Mustang in 2004. 
 Allied Forces in the European Theatre of World War II.     It documents that the Tuskegee Airmen were the countrys first African American military aviators taking flight in World War II, despite contemporary stereotyping of African American men as lacking the essential intellectual and emotional qualities necessary to fly. The film even presents government documents deriding African American men as substandard and "cowards". It describes segregation endured by the African American military officers, and it describes how these pilots distinguished themselves with their record. 

It details the evolution of the Red Tail Project mission and the re-dedication to this enthusiasm when the missions leader perished while flying a plane in homage to the Tuskegee Airmen.   This includes an explanation of the provenance of the plane and the various restoration efforts that led up to the Red Tail Project. Red Tail Reborn, 2007, Hemlock Films. 

==History and production==
{{multiple image
 | align     = left
 | direction = vertical
 | header    = Tuskegee Airmen
 | header_align = left/right/center
 | header_background = 
 | footer    = Images of the restored World War II P-51 Mustang flown by Red Tail Project, named Tuskegee Airmen, that is described in the film
 | footer_align = left/right/center
 | footer_background = 
 | width     =

 | image1    = P-51C-18.jpg
 | width1    = 180
 | alt1      = A single nosed-propeller plane is in mid flight above land.  The red nose faces right and the underside of the plane is slightly in view.  The plane has black propellers, grey base paint, and black letters reading A42.  The 4 and 2 are separated by a United States roundel in black with a central white star.  The roundel is also visible on the tops of the planes wings.  The full length of the right side of the plane is visible.
 | caption1  =

 | image2    = P-51C-06.jpg
 | width2    = 180
 | alt2      = A single nosed-propeller plane is on the ground on its wheels with the propeller in motion.  The plane is viewed from the front, but the red nose faces slightly to the right.  The plane has black propellers.  Parts of the wings and propellers are crpped from view.
 | caption2  =

 | image3    = P-51C-17.jpg
 | width3    = 180
 | alt3      = A close-up view of the front half of a single nosed-propeller plane is in mid flight above land.  The red nose faces left and the cockpit is in view from the top.  The plane has dark propellers that are barely perceptible because of their high speed, grey base paint, and black letters reading A4.  The word TUSKEGEE is visible on the nose of the plane and other lettering and insignia can also be seen at higher resolutions.  The 4 is followed by a United States roundel in black with a central white star.
 | caption3  = 
}} Charles McGee, Parker Hannifin Corporation. 

The film was inspired while White was filming The Restorers, after a P-51 Mustang|P-51C Mustang landed for refueling and White met Doug Rozendaal, the pilot of the Red Tail Projects Tuskegee Airmen plane.  When White heard Don Hinz had been killed in a crash of that very same plane, he felt compelled to do a story focusing on it. Once he got involved in the project, a local PBS station committed to airing the final product, which was an early step in the successful development of the project.  When researching the film he noticed a lack of resources about the Tuskegee Airmen and felt driven to fill the void. 

==Airings and DVD release==
The film premiered at the   and in Color, Dolby, DVD, NTSC, widescreen format.  was produced in color format. The film began national PBS broadcasts in during Black History Month in February 2008, reaching over 300 affiliates in its initial national airing,  although specific dates and times varied from station to station.   The PBS airings began on February 5.     PBS also aired the film later in 2008 during its November Veterans Day programming.  

The DVD release includes a second disc with four hours of additional interviews, deleted scenes, and productions stills. A reviewer from Flight Journal, feels the DVD package will connect with an audience of actual and aspiring restorers of military combat aircraft, which sometimes referred to as warbirds.  It notes that the narrator, Michael Dorn, is a warbird owner and pilot. 

==Legacy==
 ]]
In 2008, the film garnered three regional   entitled Flight of the Red Tail, which is also known as Red Tail Reborn 2,  that was available on DVD on November 27.   The sequel is a 12-minute documentary follow-up. 
 

==Notes==
 

==External links==
* 
* 
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 