The Marriage Maker
{{infobox film
| name           = The Marriage Maker
| image          =
| imagesize      =
| caption        =
| director       = William C. deMille
| producer       = Adolph Zukor Jesse Lasky
| writer         = Clara Beranger (scenario)
| based on       =  
| starring       = Agnes Ayres Charles de Rochefort Mary Astor
| cinematography = L. Guy Wilky
| editing        =
| distributor    = Paramount Pictures
| released       = September 23, 1923
| runtime        = 7 reels
| country        = United States Silent (English intertitles)
}} lost 1923 silent fantasy film produced by Famous Players-Lasky and distributed by Paramount Pictures. It is based on a Broadway play, The Faun, by Edward Knoblock. On stage the faun character was played by William Faversham. William C. deMille directed and his wife Clara Beranger wrote the scenario.   

==Cast==
*Agnes Ayres - Alexandra Vancy Jack Holt - Lord Stonbury
*Charles de Rochefort - Sylvani
*Robert Agnew - Cyril Overton
*Mary Astor - Vivian Hope-Clarke
*Ethel Wales - Mrs. Hope-Clarke
*Bertram Johns - Fish
*Leo White - Morris

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 

 
 