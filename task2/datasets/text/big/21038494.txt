Thedi Vandha Mappillai
{{Infobox film
| name =     THEDI VANDHA MAPPILLAI
| image =    Thedi Vantha Mappillai.jpg
| caption =
| director =  B. R. Panthulu 
| writer = R. K. Shanmugam
| screenplay = Padmini Pictures Story Department
| story = Rajashri MGR J. Jayalalitha Major Sundarrajan S. A. Ashokan
| producer = B. R. Panthulu
| music =     M. S. Viswanathan
| cinematography = A. Shanmugam
| editing = R. Devarajan
| studio = Padmini Pictures
| distributor = Padmini Pictures
| country = India
| released =  29 August 1970
| runtime = 151 mins
| country = India Tamil
| budget =
}}

THEDI VANDHA MAPPILLAI (In English: I CAME LOOKING FOR THE GROOM) is a Tamil film directed by  B. R. Panthulu released in 1970.

==Plot==

Pasupathy (Major Sundarrajan)’s boss is shot by an unknown man, but the blame falls on Pasupathy. The surviving boss sends away his wife Parvati Ammal (M. V. Rajamma)  and son Shankar (MGR) in to hiding fearing his familys fate. But before breathing his last, the boss learns the truth and entrusts Pasupathy the task of finding Parvati and Shankar and hand them their rightful family assets.

Several years later Shankar (M. G. Ramachandran|MGR) now a grown up youth, learns of the injustice committed to his father, heads out to Chennai in search of his father’s murderer.

Shankar meets Pasupathy’s daughter, Uma (J. Jayalalithaa|Jayalalitha) and falls in love with her, without knowing her background. Pasupathy finally manages to find Parvati, and soon Suresh (S. A. Ashokan), a notorious smuggler imprisons Shankar, and impersonates him, for his wealth.

Parvati is forced to play along, as Suresh threatens to kill Shankar. The rest of story unveils who won in the end, Shankar or the evil forces.

==Cast==
{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || as Shankar, (Music professor Edward, "Thotu Katava...")
|-
| S. A. Ashokan || as Suresh
|-
| Major Sundarrajan || as Pasupathy Rabhagavadhor (alias Solaimalai)
|-
| Cho Ramaswamy || as Karpadhan
|-
| Jayalalithaa || as Uma Mageshswari
|-
| M. V. Rajamma || as Parvati Ammal, Shankar s mother
|-
| Jothilakshmi || as Djéa (alias Mangai Thirumangai)
|-
| Vijayasree || as The seducer of the trap ("Sorgathai Theduvom...")
|-
| Gandhimadhi || as Chélam, Pasupathy s wife
|-
| Kokila || as
|-
| Yénethai Kannaiya || as The jeweler
|- Justin || as Samoundhi
|-
| B. R. Panthulu (Not mentioned) || as Thanigatchan, Shankar s father (in photo)
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

This is the 4th and last collaboration MGR-B.R. Panthulu (the boss of the Padmini Pictures)

B.R.Pandhulu gives  his face to play the role of MGR s father died .

We see him in his portrait on the award held by MGR in the first song, "Vetri Meethu Vetri Vanthu..."

The big actress M.V. Rajamma who plays the wife of the character of the father died of MGR, in the movie, was her really in life.

M.V. Rajamma was married to the director-producer B.R. Panthulu.

The boss of The Padmini Pictures dies only 4 years after this movie, in 1974 (a massive cardiac arrest) !

==Songs==
{{tracklist	
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 26.88
| title1       = Ada Arumugam
| extra1       = T. M. Soundararajan, P. Susheela
| lyrics1      = Kannadasan
| length1      = 3.42
| title2       = Adatha Ullagal
| extra2       = L. R. Eswari Vaali
| length2      = 3.44
| title3       = Idamo Sugamanathu
| extra3       = T. M. Soundararajan, P. Susheela
| lyrics3      = Kannadasan
| length3      = 3.44
| title4       = Maanikkka Theril Maragatha
| extra4       = T. M. Soundararajan, P. Susheela Vaali
| length4      = 3.22
| title5       = Sorgathai Theduvom
| extra5       = T. M. Soundararajan
| lyrics5      = Kannadasan
| length5      = 3.56
| title6       = Thangapathakathen
| extra6       = T. M. Soundararajan, P. Susheela
| lyrics6      = Kannadasan
| length6      = 3.33
| title7       = Thotu Katava
| extra7       = T. M. Soundararajan
| lyrics7      = Kannadasan
| length7      = 3.42
| title8       = Vetri Meethu Vetri Vanthu
| extra8       = S. P. Balasubrahmanyam Vaali
| length8      = 3.05
}}

==References==
 

== External links ==
*  

 

 
 
 
 


 