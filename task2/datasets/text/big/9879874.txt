The Honourable Tenant
{{Infobox film
| name           =The Honourable Tenant
| image          =
| image_size     =
| caption        =
| director       = Carlos Schlieper 
| producer       = Juan Parret    Jean de Bravura
| writer         =Ariel Cortazzo    Carlos Schlieper 
| narrator       =
| starring       = Alberto Closas   Olga Zubarry   Amalia Sánchez Ariño 
| music          = Peter Kreuder 
| cinematography = Vicente Cosentino
| editing       =José Cardella
| studio        = Interamericana 
| distributor    =
| released       = 27 December 1951 
| runtime        = 81 minutes
| country        = Argentina Spanish
| budget         =
}}
The Honourable Tenant (Spanish:El Honorable inquilino) is a 1951 Argentine comedy film directed by Carlos Schlieper and starring  Alberto Closas, Olga Zubarry and Amalia Sánchez Ariño. The films sets were designed by Carlos T. Dowling.

==Cast==
* Alberto Closas as Luis Ayala 
* Olga Zubarry as Ana María  
* Amalia Sánchez Ariño as Alfonsina   Osvaldo Miranda as Martín 
* Pedro Quartucci as Juancho   Severo Fernandez as Mr. Larica  
* Nélida Romero as Elena  
* Hugo Pimentel as Fredo  
* Amalia Bernabé as Ernestina 
* Pablo Cumo as Lorenzo Corsi 
* Elda Dessel as Rosalía 
* Carmen Giménez as Sra. González 
* Miguel Bebán
* Adelaida Demichelis Roberto Durán  
* Enrique Fava 
* Julio Heredia 
* Alberto Lenti 
* Edda Mancini 
* Arsenio Perdiguero 
* Norma Suarez

==External links==
*  

 

 
 
 
 
 
 
 
 

 