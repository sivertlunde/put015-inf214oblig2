The End of Evangelion
 
{{Infobox film
| name = The End of Evangelion
| image = eoeposter.JPG
| caption = Theatrical release poster
| writer = Hideaki Anno
| starring = Megumi Ogata Megumi Hayashibara Yuko Miyamura Kotono Mitsuishi
| director = Episode 25:   
| producer = Mitsuhisa Ishikawa
| music = Shiro Sagisu
| editing = Sachiko Miki
| cinematography = Hisao Shirai Sega Corporation Starchild Gainax|Gainax Co., Ltd.
| distributor = Toei Company
| released =  
| runtime = 87 minutes
| country = Japan
| language = Japanese
}} animated science fiction film written and directed by Hideaki Anno, animated by IG Ports subsidiary Production I.G, released as a finale for the mecha-based television series Neon Genesis Evangelion.

The film is divided into two episodes: Episode 25: Love is Destructive and Episode 26: ONE MORE FINAL: I need you. They effectively replace the series controversial final two episodes with a more "real world" account of the storys apocalyptic climax.  Gainax originally proposed titling the film Evangelion: Rebirth 2. 

The End of Evangelion initially received polarizing reviews, with the film obtaining the Animage Anime Grand Prix in 1997 (among other awards) and reviews that ranged from glowing to antipathetic. A 2014 Time Out New YorkTime Out (magazine)|  poll of filmmakers saw The End of Evangelion voted one of the 100 best animated films of all time.   

==Plot==
 
  looks to the sky, pulled by the Earths gravity, and cradles the Egg of Lilith which absorbed the souls of all human beings.]]
 Angels killed Japanese Strategic hacking attempt. The JSSDF enters NERV Headquarters and kills nearly everyone there, with top priority given to the capture of the Evas and the deaths of their pilots.

Misato Katsuragi has Asuka moved to the cockpit of Eva Unit-02|Unit-02 and placed at the bottom of a nearby lake, before rescuing a catatonic Shinji from JSSDF troops. Determined to have him pilot EVA01|Unit-01, Misato brings Shinji to the Evas bay doors, but is mortally wounded during an assault by more soldiers. After convincing Shinji to pilot the Eva one more time, Misato kisses him and forces him into the elevator. As she dies, Misato wonders if Kaji thinks that she has done the right thing.
 Mass Production Evangelions to the Geofront to attack Unit-02. In Terminal Dogma, Ritsuko intends to destroy NERV Headquarters to prevent Gendo from carrying out his plans. However, her command is overridden by Casper, one of the three cores of the MAGI and essentially her mother. After realizing her mothers betrayal of her for love, Ritsuko is killed by Gendo. At the Eva launch cages, Shinji is unable to help Asuka when he discovers that Unit-01 is encased in hardened bakelite. Despite initially appearing victorious, Asuka and Unit-02 are mercilessly killed by the MP Evas.

Later, Unit-01 breaks through the surrounding bakelite on its own. With Shinji in the cockpit, Unit-01 breaks out of the ruined NERV Headquarters and ascends the Geofront. Shinji witnesses the MP Evas carrying the mutilated remains of Unit-02, devastating him. Having merged with Adam, Gendo attempts to merge with Rei to begin Third Impact. However, Rei takes over the process, rejecting Gendo while absorbing Adam into herself and reuniting with Lilith, the crucified Second Angel. The two form a luminescent, rapidly growing being with Liliths skin and Reis body. The MP Evas crucify Unit-01 and begin the ritual to initiate Third Impact, and Shinji is devastated by the grotesque transformations the MP Evas undertake. The gigantic form of Lilith rises out of the Geofront and confronts Shinji, transforming into the forms of Kaworu and Rei.
 everyone in the world should die. In response, Lilith creates a planet-wide anti-AT-Field which negates the AT-Fields of humanity and causes their bodies to dissolve into LCL, the blood of Lilith and the primordial soup from which life on Earth originates. The souls of humanity are absorbed into the Egg of Lilith, a giant dark sphere cradled by Lilith, as she grows to an enormous size.

The worlds souls form a single, complemented existence, and Shinjis emotional sufferings and loneliness prompt him to accept this new form, thinking that there could never be happiness in the real world. However, after a series of introspections and monologues, Shinji realizes that it is necessary to live with others and that to live life is to experience joy as well as pain. The Third Impact is rejected and Lilith decays and dies, releasing its anti-AT Field and allowing separate beings to potentially come back into existence. Asuka and Shinji are rematerialized from the sea of LCL together on a beach, with a view of the severed head of Lilith and the apocalyptic landscape.

==Cast==
{| class="wikitable"
|-
! Character 
! Japanese
! English
|-
| Shinji Ikari || Megumi Ogata || Spike Spencer
|-
| Rei Ayanami || Megumi Hayashibara || Amanda Winn-Lee 
|-
| Asuka Langley Soryu || Yuko Miyamura || Tiffany Grant
|-
| Kaworu Nagisa || Akira Ishida || Aaron Krohn
|-
| Misato Katsuragi || Kotono Mitsuishi || Allison Keith 
|-
| Gendo Ikari || Fumihiko Tachiki || Tristan MacAvery
|-
| Kozo Fuyutsuki || Motomu Kiyokawa || Michael Ross
|- Yuriko Yamaguchi || Sue Ulu
|-
| Makoto Hyuga || Miki Nagasawa || Keith Burgess
|- Jason C. Lee
|-
| Maya Ibuki || Miki Nagasawa || Amy Seeley
|-
| Keel Lorentz || Mugihito || Tom Booker
|-
| Yui Ikari || Megumi Hayashibara || Amanda Winn-Lee 
|-
| Ryoji Kaji || Kōichi Yamadera || Aaron Krohn
|-
| Kyoko Zeppelin Soryu || Maria Kawamura || Kimberly Yates
|-
|}

==Production==
The ambiguous meaning of the TV series ending left many viewers and critics confused and unsatisfied.  The final two episodes were possibly the most controversial segments of an already controversial series  and were received as flawed and incomplete by many.  However, Anno and assistant director Kazuya Tsurumaki defended the artistic integrity of the finale.  

Gainax launched the project to create a film ending for the series in 1997, first releasing   as a highly condensed character-based recap and re-edit of the TV series (Death) and the first half of the new ending (Rebirth, which was originally intended to be the full ending, but could not be finished due to budget and time constraints). The project was completed later in the year and released as The End of Evangelion. Episode 25: Air uses the original script intended for episode 25 of the original series  and forms roughly 2/3 of the previous film,  .
 Yuriko Yamaguchi Anno ensured that part of Gendos line was inaudible). She successfully delivered the line after being shown a hint from Anno.   

Among the images used in the film are of some of the hate-mail and death threats (including graffiti on Gainaxs headquarters) as well as letters of praise sent to Anno.   - (Detailed transcription of the letters appearing in The End of Evangelion)  

==Music==
  Cello Suite No. 1 in G Major (I. Prélude), Jesu, Joy of Mans Desiring (transcribed for piano and later played again with string instruments in the end credits), and Pachelbels Canon.

Among the other insert songs are "Music of Neon Genesis Evangelion#Komm, süsser Tod|Komm, süsser Tod" (Come, Sweet Death), an upbeat song (which appears in the film at the beginning of Instrumentality), "THANATOS -If I Cant Be Yours", which is played in both the end credits and the credits to episode 25 (the song is based around "THANATOS", a background music piece used in the series). Another song, "Everything Youve Ever Dreamed", was recorded for the film by the same vocalist (Arianne) as "Komm, süsser Tod", but was not used and was later included on the Refrain of Evangelion soundtrack.

==Interpretation of the ending== Carddass card D-88 comments on the scene:

 "Shinji renounced the world where all hearts had melted into one and accepted each other unconditionally. His desire... to live with others — other hearts that would sometimes reject him, even deny him. That is why the first thing he did after coming to his senses was to place his hands around Asukas neck. To feel the existence of an other. To confirm rejection and denial."  

The meaning of the final scene is obscure,   and has been controversial.   According to an episode of the Japanese anime show Anime Yawa aired March 31, 2005 on NHKs satellite TV, Asukas final line was initially written as "Id never want to be killed by you of all men, absolutely not!" or "Ill never let you kill me." ("Anta nankani korosareru nowa mappira yo!") but Anno was dissatisfied with Miyamuras renditions of this line.    Eventually Anno asked her a question which described what he was going for with this scene:
 "Concerning the final line we adopted, Im not sure whether I should say about it in fact. At last Anno asked me Miyamura, just imagine you are sleeping in your bed and a stranger sneaks into your room. He can rape you anytime as you are asleep but he doesnt. Instead, he masturbates looking at you, when you wake up and know what he did to you. What do you think you would say? I had been thinking he was a strange man, but at that moment I felt disgusting. So I told him that I thought Disgusting. And then he sighed and said, I thought as much." 
 
 voice actress, made the following statement:
 "The most widely circulated translation of the last line of EoE     is "I feel sick," but  , directed the TV series and is very familiar with the whole Eva franchise. Matt has said that although (Eva creator) Hideaki Anno seems to change his mind frequently about what various things mean in Eva, Anno once said that Asukas comment about feeling "sick" was a reference to morning sickness. Now THAT gives ya something to think about, doesnt it! Of course, Anno is quite passionate about the idea that every person should decide for him or herself what Eva means to them."   - (a personal FAQ page by Tiffany Grant)  

Some state that, despite the somber ending, the results of Instrumentality are not permanent. Both Rei and Yui comfort Shinji and tell him that people can restore themselves to physical existence if they want to, depending on the strength within their hearts. It is suggested that Asuka is one of the first persons to manifest herself back into reality. Another Evangelion trading card explains:   

 "In the sea of LCL, Shinji wished for a world with other people. He desired to meet them again, even if it meant he would be hurt and betrayed. And just as he had hoped / wanted, Asuka was present in the new world. Only Asuka was there beside him. The girl whom he had hurt, and who had been hurt by him. But even so, she was the one he had hoped/wished for...." 

It has been debated whether The End of Evangelion is intended to enlarge and retell episodes 25 and 26 or to completely replace the TV ending with a different one; with certain sequences from the final television episodes being evocative of sequences of the film, such as a shot of Misatos bled-out corpse, Ritsuko floating in LCL, Asuka underwater in Eva-02 and another of Shinji huddled in despair.    Some believe that The End of Evangelion is an alternate ending to the series, perhaps created to please those fans who were displeased with the TV series ending. Tsurumaki said he felt the series was complete as it was.   

==Release==
The End of Evangelion was first released in Japanese theaters on July 19, 1997. Between its release and October 1997, the film grossed 1.45 billion yen.  The film was later distributed on   from August 1997 to January 1998.

In 2006, The End of Evangelion was shown theatrically as part of the Tokyo International Film Festival in Akihabara. 

===Red Cross Book===
The Red Cross Book (as its unofficially known, for the large red St Georges Cross on its cover) was an ISO 216#A, B, C comparison|A-4-sized pamphlet sold in theaters during the release of The End of Evangelion.    The book was written by Gainax and various production staff of the Evangelion TV series and films, with an interview with Tsurumaki, a listing of voice actors and brief essays written by them on their respective characters, short biographical sketches, commentary on the TV series and production of the films, a "Notes" section covering the setting of the films, and a glossary of terms used in the series, manga, and the two films. The Red Cross Book was left out in the Manga Entertainment release due to copyright issues.  However, it was translated by fans of the series.  

===Distribution===
ADV Films, region 1 license holder and distributor for the Neon Genesis Evangelion TV series, declined to license The End of Evangelion and the associated films, with Manga Entertainment "reportedly   around 2 million dollars" for the rights.  Voice actress Amanda Winn Lee wrote the films script for both the subtitled and dubbed versions (based on translations by Sachuchi Ushida and Mari Kamada), and produced and directed the dub.    The cast was made up primarily of voice actors reprising their roles from ADVs dub of the TV series, with several supporting roles recast because the original actors were unavailable. To accommodate voice actors living in different parts of the country, the dub was recorded in three locations, Los Angeles, Houston and New York City. 

The North American DVD release featured a 6.1 DTS, a 5.1 Dolby, and a new stereo track downmixed from the 6.1 in both languages (the DVD packaging claiming it was remixed no less than three times). Some creative changes were made to the English audio track of the film, including some added dialogue and sound effects. 

In discussing the films English dub, Mike Crandol of Anime News Network determined that "the remarkably strong performances of the main cast overshadow the weaker voice work present", though he criticized the script for being "slightly hammy" in parts. Crandol praised the final exchange between Spike Spencer (Shinji) and Allison Keiths (Misato) characters as "one of the most beautiful vocal performances to ever grace an anime".   

====The End of Evangelion: Renewal====
A new version of The End of Evangelion was released on June 25, 2003 in Japan by Starchild and King Records as part of the Renewal of Evangelion box set (which compiled "new digitally remastered versions of the 26 TV show episodes, 4 remade-for-Laserdisc episodes, and 3 theatrical features" as well as "a bonus disc with never-before-seen material"). 

This version of the film joins the "recap" film Evangelion: Death with End and omits the Rebirth segment from the first film. Also, on the aforementioned bonus disc is a previously unreleased deleted scene shot in live-action with voice actors Megumi Hayashibara, Yūko Miyamura, and Kotono Mitsuishi portraying their characters, 10 years after the events of Evangelion. In this continuity, Shinji does not exist and Asuka has a sexual relationship with Toji Suzuhara. The sequence concludes with a male voice (implied to be Shinjis) saying, "This isnt it, I am not here," proving it is a false reality seen through his eyes. 
Manga Entertainment announced in 2006 that it was "ironing out the contracts" to release the Renewal versions of Death & Rebirth and The End of Evangelion the next year,  though their rights to the film have since expired. 

==Reception==

===Initial reactions=== Japan Academy Prize for "Biggest Public Sensation of the Year"  and was given the "Special Audience Choice Award" by the 1997 Animation Kobe.  EX.org ranked the film in 1999 as the fifth best All-Time Show (with the TV series at #2). 
 Manga artist Nobuhiro Watsuki wrote:
 

Newtype USA reviewed the film as a "saga of bamboozlement". It also criticized the films "more biblical overtones, teen melo-drama and bad parenting" and that "for some frustrated viewers, these DVDs might bring on the 4th impact hurling these DVDs against the wall."  Manga Entertainment CEO Marvin Gleicher criticized the Newtype review as "biased and disrespectful" and a "facile and vapid" product of "ignorance and lack of research". 

Many reviews focused on the audio-visual production; Sight and Sound editorialized that "narrative coherence seems a lesser concern to the film-makers than the launching of a sustained audio-visual assault. The kaleidoscopic imagery momentarily topples into live action for the baffling climax...",  an assessment echoed by critic Mark Schilling.  Mike Crandol of Anime News Network gave the film an overall passing grade and described it as "a visual marvel". He described the DVD release as "a mixed bag", expressing displeasure over the "unremarkable" video presentation and overall lack of extra material.  David Uzumeri of ComicsAlliance summarized the film as "a dark, brutal, psychedelic orgy of sex and violence that culminated in the mass extinction of humanity set to an optimistic J-pop song with lyrics about suicide." Uzumeri also stated that the "themes of   criticizing the audience for being spineless and lost in a fantasy world were cranked up to eleven, as the protagonist Shinji basically watches everybody die around him due to his refusal to make any effort whatsoever to engage with other people." 

===Reassessments===
In a 2008 article for Slant Magazine, writer Michael Peterson wrote that "it was not until the End of Evangelion film that Hideaki Anno|Annos visual strengths as a director really stood out". He observed that "Anno, like David Lynch, possesses a skill at framing his shots, and using the attendant color, to create visual compositions that stand out not only as beautiful in the storys context, but also as individual images, a painterly quality that he then applies back to the work. When Anno frames an image, the power of that specific image becomes a tool that he can later refer back to for an instantaneous emotional and intellectual response." 

Carlos Ross of Them Anime Reviews compared the tone of the film to The Blair Witch Project in that it deconstructed the series while "cashing in" on it. He was especially critical of the films entire second half by saying:
  }}

Schilling reviewed the film as more than a deconstruction, but an attempt at unification of mediums:
 "Despite the large cast of characters, decades-spanning story, and a profusion of twenty-first-century jargon, much of it borrowed from early Christian sources, the film is essentially a Power Rangers episode writ large: i.e., super-teens piloting big, powerful machines and saving the world from monsters. Weve seen it all before. What we havent seen, however, is the way the film zaps back and forth through time, slams through narrative shifts and flashes explanatory text, in billboard-sized Chinese characters, at mind-bending speed. Its a hyper-charged phantasmagoria that defies easy comprehension, while exerting a hypnotic fascination. Watching, one becomes part of the films multimedia data stream. Shinseiki Evangelion is looking forward, toward an integration of all popular media - television, manga, movies, and video games - into new forms in which distinctions between real and virtual, viewer and viewed, man and machine, become blurred and finally cease to matter. O Brave New World, that has such animation in it."  
Chris Beveridge of Mania.com described the film as "work  on so many levels", but cautions that it is not meant to be watched without having seen the rest of the series. 

The End Of Evangelion is frequently ranked among the greatest anime films. Patrick Macias of TokyoScope ranked it one of his 10 greatest films,  and the best anime movie of the 1990s;  CUT film magazine ranked it third on its list of the top 30 best anime films. 

In 2014, Time Out New York ranked the film at #65 on its list of the top 100 animated movies as voted for by filmmakers.    Critic Keith Uhlich described the film as an "immensely satisfying" conclusion to the TV series, the climax as "an end-times free-for-all that mixes Christian symbology, Jewish mysticism, sexual paranoia and teenage angst into a searing apocalyptic stew," filled with "sights and sounds youll never forget," and Shinjis line, "Im so fucked up," as the most memorable quote. 

==See also==
 
* Japanese films of 1997

==References==
 
 
 
*    

==External links==
 
*  
*  
*  
*  
*  
*  
*  , Slant Magazine

 
 
 
 

 
 
   
 
 
 
 
 
 
 
 
 
   
   
 
 
 

 
 
 
   
 