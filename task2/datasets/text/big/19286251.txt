Maadappura
{{Infobox film
| name = Maadappura
| image = 
| director       = S. A. Subbaraman
| writer         = Thilagan Narayanasamy
| story          = Thilagan Narayanasamy
| starring       = M. G. Ramachandran B. Saroja Devi M. R. Radha M. N. Nambiar
| producer       = B. V. N. Productions
| music          = K. V. Mahadevan
| cinematography = V. R. Rathnam
| editing        = S. Suraiya
| studio         = B. V. N. Productions
| distributor    = B. V. N. Productions
| released       =   
| runtime        = 134 mins
| country        =   India Tamil
| budget = 
}}

Maadappura is a Tamil language directed by S. A. Subbaraman. The film features M. G. Ramachandran and B. Saroja Devi in lead roles. 
The film, produced by B. V. N. Productions, had musical score by K. V. Mahadevan and was released on 16 February 1962. The film only ran for 70 days and flopped at box-office. 

==Cast==
* M. G. Ramachandran
* B. Saroja Devi
* M. R. Radha
* M. N. Nambiar
* K. Vasanthi
* Gemini Chandra
* T. K. Balachandran
* V. R. Rajagopal

==Crew==
*Production Company: B. V. N. Productions
*Director: S. A. Subbaraman
*Music: K. V. Mahadevan
*Lyrics: A. Maruthakasi
*Story: Thilagan Narayanasamy
*Dialogues: Thilagan Narayanasamy
*Art Direction: P. Angamuthu
*Editing: S. Suraiya
*Choreography: B. Hirala, K. Thangappan, Rajkumar
*Cinematography: V. R. Rathnam
*Stunt: Shyam Sundar
*Audiography: T. S. Rangasamy
*Dance: None

==Soundtrack== Playback singers Soolamangalam Rajalakshmi & P. Suseela.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Soolamangalam Rajalakshmi || rowspan=7|A. Maruthakasi || 03:59
|- 
| 2 || Varuvaar Oru Naal || P. Suseela || 03.31
|-  Soolamangalam Rajalakshmi || 03:07
|-  Soolamangalam Rajalakshmi & P. Suseela || 04:21
|- 
| 5 || Vanakkam Vanakkam Aiyaa Ammaa || P. Suseela || 03:06
|-  Soolamangalam Rajalakshmi || 04:16
|-  Soolamangalam Rajalakshmi || 02:03
|}

==References==
 

==External links==
 

 
 
 
 
 


 