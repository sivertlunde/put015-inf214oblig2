Boris Godunov (1954 film)
 
{{Infobox film
| name           = Boris Godunov
| image          = Boris Gudonov 54.jpg
| caption        = Film poster
| director       = Vera Stroyeva
| producer       = Mosfilm
| writer         = Alexander Pushkin (play) Nikolai Golovanov Vera Stroyeva
| starring       = Alexander Pirogov
| music          =
| cinematography = Vladimir Nikolayev Sergei Poluyanov
| editing        = Yekaterina Ovsyannikova
| distributor    =
| released       =  
| runtime        = 110 minutes
| country        = Soviet Union
| language       = Russian
| budget         =
}}
 opera of the same name by Modest Mussorgsky. It was screened out of competition at the 1987 Cannes Film Festival.   

==Cast==
* Alexander Pirogov as Boris Godunov
* Nikandr Khanayev as Prince Vasili Shinsky False Dmitri (as G. Nellep)
* Maxim Mikhailov as Pimen, a monk Fool
* Aleksej Krivchenya as Varlaam (as A. Krivchenya)
* Venyamin Shevtsov as Misala, a monk (as V. Shevtsov)
* A. Turchina as Innkeepers wife
* Larisa Avdeyeva as Marina

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 