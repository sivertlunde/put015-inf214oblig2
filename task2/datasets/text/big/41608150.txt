Say It with Diamonds
{{infobox film
| title          = Say It with Diamonds
| image          =
| imagesize      =
| caption        = Jack Nelson  Arthur Gregor
| producer       = I. E. Chadwick
| writer         = Leon Lee (intertitles)
| starring       = Betty Compson Earle Williams
| music          = Ernest Miller
| editing        = Gene Milford
| distributor    = First Division Pictures Chadwick Pictures Corporation
| released       = June 2, 1927
| runtime        = 7 reels

}}
Say It with Diamonds is a 1927 silent film drama starring Betty Compson and Earle Williams, an early Vitagraph leading man and matinee idol. It was directed by Jack Nelson and Arthur Gregor. Earle Williams final film before his death in April 1927. 

Prints are held at the Library of Congress and George Eastman House. 

==Cast==
*Betty Compson - Betty Howard
*Earle Williams - Horace Howard
*Jocelyn Lee - Fay Demarest
*Armand Kaliz - Armand Armour
*Betty Baker - Secretary

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 