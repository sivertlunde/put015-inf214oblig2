México Pelágico
{{multiple issues|
 
 
 
}}

{{Infobox film
| name           = México Pelágico
| image          = Mexico Pelagico.jpg
| caption        = Poster
| director       = Jerónimo Prieto
| producer       = Jorge Cervera Hauser
| narrator       = José Lavat / Nicholas Guy Smith
| music          = Eugenio Riveroll / Álvaro López Pinchetti / Vasilis Milesis / The New History
| editing        = Rodrigo Palacios
| distributor    = Calypso Media
| released       = 2014
| runtime        = 70 minutes
| country        = Mexico
| language       = Spanish / English
}}
México Pelágico is a 2014 Mexican documentary film. It portrays the life of the open ocean of Mexico through the eyes of young conservationists. 

== Awards ==
México Pelágico received the Directors Award at the 12th San Francisco International Ocean Film Festival in early 2015. 

México Pelágico received the Audience Award at the 2015 Vail Film Festival. 

==References==
{{reflist|refs=

  . San Francisco Ocean Film Festival. Accessed April 2015. 

  . Vail Film Festival. Accessed April 2015. 

  . Pelagic Life & Calypso Films. Accessed April 2015. 

}}

==External links==
*  
*  

 
 
 
 
 
 


 
 