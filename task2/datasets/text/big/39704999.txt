East Side, West Side (1927 film)

{{Infobox film
| name           = East Side, West Side
| image          = East Side West Side lobby card.jpg
| caption        = Lobby card
| director       = Allan Dwan William Fox
| writer         = Allan Dwan (scenario)
| based on       = novel by Felix Riesenberg George OBrien Virginia Valli
| music          = George Webber
| editing        =
| distributor    = Fox Film Corporation
| released       = October 9, 1927
| runtime        = 90 minutes
| country        = USA
| language       = Silent...English titles
}} George OBrien (in the same year that he played the lead in F.W. Murnau|Murnaus Sunrise (film)|Sunrise), Virginia Valli and June Collyer. The supporting cast includes J. Farrell MacDonald and Holmes Herbert. The epic film was shot extensively on various locations in New York City and includes a sinking ship loosely based upon the  . 

The film is preserved at the Museum of Modern Art, New York. 

Remade in 1931 as Skyline (1931 film)|Skyline with Thomas Meighan and Hardie Albright.

==Cast== George OBrien - John Breen
*Virginia Valli - Becka Lipvitch
*J. Farrell MacDonald - Pug Malone
*Dore Davidson - Channon Lipvitch
*Sonia Nodell - Mrs. Lipvitch (*as Sonia Nodalsky)
*June Collyer - Josephine
*John Miltern - Gerrit Rantoul
*Holmes Herbert - Gilbert Van Horn
*Frank Dodge - Judge Kelly
*Dan Wolheim - Grogan
*Johnny Dooley - Grogan gang member
*John Kearney - Policeman
*Edward Garvey - Second
*Frank Allworth - Flash
*William Frederic - Breen
*Jack La Rue - dining extra(uncredited)

==References==
 

==External links==
*   in the Internet Movie Database
* 

 

 
 
 
 
 


 