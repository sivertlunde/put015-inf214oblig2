Men and Women (1914 film)
{{infobox film
| name           = Men and Women
| image          =
| caption        = James Kirkwood D. W. Griffith(supervising director)
| producer       = Biograph Company Marc Klaw Abraham Erlanger
| writer         = David Belasco(play) Henry C. deMille(play) Frank E. Woods(scenario)
| starring       = Lionel Barrymore Blanche Sweet
| cinematography =
| editing        =
| distributor    = General Film Company
| released       =  
| runtime        = 30 minutes; 3 reels
| country        = United States
| language       = Silent(English intertitles)
}} the same name by David Belasco and Henry Churchill de Mille.  It stars Lionel Barrymore, Blanche Sweet and Marshall Neilan. Sweet and Neilan would later marry in real life. 
 Paramount in Men and Women.

==Cast==
*Lionel Barrymore - Stephen Rodman/Robert Stevens
*Blanche Sweet - Agnes Rodman, Stephens daughter
*Fred Hearn - D.A. Stedman
*Gertrude Robinson - Dora Prescott
*Marshall Neilan - Will Prescott
*Frank Crane - Ned Seabury (*as Frank Hall Crane)
*Fred Herzog - Kirke, the broker
*Hattie De Laro - Mrs Prescott
*Claire McDowell - Mrs. Stephen Rodman
*Frank M. Norcross - Cohen Alan Hale - One of Kirkes Creditors
*Antonio Moreno - Man in Kirkes Office
*Gladys Egan - An Orphan

unbilled/uncredited
*Lillian Gish
*Vivian Prescott

==See also==
*Lionel Barrymore filmography
*Blanche Sweet filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 