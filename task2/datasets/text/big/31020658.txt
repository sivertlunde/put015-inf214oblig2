Calcutta (1947 film)
{{Infobox film
| name           = Calcutta
| image          = Calcutta Poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = John Farrow
| producer       = Seton I. Miller
| screenplay     = Seton I. Miller
| narrator       =
| starring       = Alan Ladd Gail Russell William Bendix
| music          = Victor Young
| cinematography = John F. Seitz
| editing        = Archie Marshek
| studio         =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = gross = 1,220,580 admissions (France) 
}}
Calcutta is a 1947 American crime film noir directed by John Farrow, and written and produced by Seton I. Miller.  The drama features Alan Ladd, Gail Russell and William Bendix. 

==Plot==
Commercial pilot in India attempts to avenge friends murder.

==Cast==
* Alan Ladd as Neale Gordon
* Gail Russell as Virginia Moore
* William Bendix as Pedro Blake
* June Duprez as Marina Tanev
* Lowell Gilmore as Eric Lasser
* Edith King as Mrs. Smith
* Paul Singh as Mul Raj Malik Gavin Muir as Inspector Kendricks
* John Whitney as Bill Cunningham
* Benson Fong as Young Chinese Clerk

==Production== Calcutta had frequently been in the news with reports of the war and Paramount decided it would make an ideal setting for a film. Production of the movie was announced in late 1944. It was based on an original story by Seton Miller who also acted as screenwriter and producer. 

Alan Ladd and William Bendix, who had just appeared in Two Years Before the Mast for Miller, were announced as stars, playing pilots who flew over the "hump" from Calcutta to Chongqing|Chungking.  Howard da Silva, who had co-starred several times with Ladd, was announced as a co-star but ended up not being cast.   John Farrow, who had made two films with Ladd, was chosen to direct.

Filming took place in June and July 1945. Four people were hired as special technical advisers: Joe Rosbert (a member of the Flying Tigers who crashed on "the hump"), Major Whyte (a veteran of the Eighth Burma Rifles), Mrs Madge Schofield (a former resident of Calcutta) and Dr Singh (a resident Hollywood expert on Indian affairs); the last two had small roles in the film. Of the other cast members, June Duprez had suffered a career slump since moving to Hollywood following her appearance in The Thief of Bagdad but had been restored to some popularity since appearing in None But the Lonely Heart. Gail Russell had previously made Salty ORourke with Ladd. John Witney was from Little Theatre and was a protege of Alan Ladd. Edith King, who had just appeared in Broadway in Othello, made her film debut.  200 Indian seamen from the British Indian Navy were used as extras. 

==Reception==

===Critical response===
When the film was first released, The New York Times film critic Thomas M. Pryor, gave the film a mixed review, writing:
 There is just so much that an actor can do on his own to make a character interesting and then he must depend upon the scenarist to provide him with dialogue and situations which will keep the spectator on edge. In Calcutta, which opened yesterday at the Paramount. Alan Ladd is going through an all-too-familiar exercise. While the actor is giving a competent performance and is nicely abetted by William Bendix, the story by Seton I. Miller, who also produced the film for Paramount, is a sorry mess indeed.  
The critic from the Los Angeles Times called the film "atmospheric and interest-holding" but thought that Gail Russell was miscast. Ladd Cracks Murder Case in Calcutta
Scheuer, Philip K. Los Angeles Times (1923-Current File)   13 June 1947: A7. 

More recently, film critic Dennis Schwartz gave the film a positive review, writing:
 John Farrows Calcutta is a fast-paced old-fashioned adventure yarn, shot entirely in Paramounts backlot. Seton Miller does the screenplay. Its an entertaining potboiler, though a minor work...Ladd gives an icy action-hero performance as someone who revels in his disdain for women as untrustworthy companions. By Ladds politically incorrect moves, he takes on the characteristics of the film noir protagonist--which gives this programmer its energy. Ladd quotes an ancient Hindu saying Man who trust woman walk on duckweed over pond, which tells us all we want to know about how he has stayed alive for so long while in the company of dangerous women, ones like Virginia, while Bill so easily succumbed to the beauty of the femme fatale.  

===Noir analysis===
American studies and film professor, Bob Porfirio discussed the treatment of women found in film noir and novels:
 When Ladd rips a pendant from her neck or slaps her around, it makes him appear all the more impervious to women.  Their interaction holds this film together and demonstrates the misogynistic strain of hard-boiled fiction; it is as strain implicit in much of post-war American society as well.  

==Adaptation==
On February 9, 1950, The Screen Guild Theater, broadcast a 30 minute radio adaptation of the film with Alan Ladd and Gail Russell reprising their film roles. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 