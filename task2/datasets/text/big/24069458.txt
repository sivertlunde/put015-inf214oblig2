I'm from Arkansas
{{Infobox film
| name           = Im from Arkansas
| image          =
| image_size     =
| caption        =
| director       = Lew Landers
| producer       = E.H. Kleinert Irving Vershel
| writer         = Marcy Klauber (screenplay) Joseph Carole (screenplay)
| narrator       =
| starring       = See below
| music          = Clarence Wheeler
| cinematography = Robert Pittack
| editing        = John F. Link Sr.
| distributor    =
| released       = 31 October 1944
| runtime        = 70 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Im from Arkansas is a 1944 American film directed by Lew Landers.

== Plot summary ==
The town of Pitchfork, Arkansas makes national headlines when Esmeralda the sow gives birth to 18 piglets. Among the visitors to Pitchfork are a troupe of showgirls hoping to entertain the visitors and a folk music group returning to their home after their touring is through.  In addition to the artists a meat packing company sends two men to investigate what made Esmeralda give birth to so many piglets and to bring the secret back to increase meat production.

== Cast ==
*Slim Summerville as Juniper Jenkins aka Pa
*El Brendel as Oly
*Iris Adrian as Doris
*Bruce Bennett as Bob Hamline
*Maude Eburne as Matilda Alden Jenkins aka Ma
*Cliff Nazarro as Willie Childs
*Al St. John as Farmer
*Carolina Cotton as Abigail Abby Alden
*Danny Jackson as Efus Jenkins
*Paul Newlan as Farmer Harry Harvey as Stowe Packing Company Representative
*Arthur Q. Bryan as Commissioner of Agriculture John Hamilton as Harry Cashin, Vice President of Slowe Packing Company Douglas Wood as Governor of Arkansas
*Walter Baldwin as Packing Company Attorney
*Flo Bert as Showgirl
*The Pied Pipers as Quartet
*The Sunshine Girls (including Mary Ford) as Girl Trio
*Jimmy Wakely as Jimmy Wakely

== Soundtrack ==
*The Pied Pipers - "Youre the Hit of the Season" Charles Mitchell)
*Jimmy Wakely - "Dont Turn Me Down Little Darlin" Oliver Drake)
*Carolina Cotton - "I Love to Yodel" (Written by Carolina Cotton), "Yodel Mountain"
*The Pied Pipers - "Stay Away from My Heart" (Written by Jimmy Wakely)
*The Milo Twins - "Pass the Biscuits, Mirandy"
*The Pied Pipers - "If you Cant Go Right, Dont Go Wrong"
*The Milo Twins - "Pitchfork Polka"

== External links ==
* 
* 
* 
* 
 

 

 
 
 
 
 
 
 
 
 
 

 
 