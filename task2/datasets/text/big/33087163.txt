Violent Shit II: Mother Hold My Hand
{{Infobox film
| name           = Violent Shit II: Mother Hold My Hand
| image          = ViolentShit2.jpg
| image size     =
| border         =
| alt            =
| caption        = VHS cover.
| director       = Andreas Schnaas
| producer       = Blood Pictures Reel Gore Productions
| writer         = Andreas Schnaas
| screenplay     =
| story          =
| based on       =
| narrator       =
| starring       = Anke Prothmann Andreas Schnass
| music          = Jens C. Moller
| cinematography = Steve Aquilina
| editing        = Steve Aquilina
| studio         = Reel Gore Productions
| distributor    = Burning Moon Home Video
| released       =  
| runtime        = 82 minutes
| country        = Germany
| language       = German
| budget         =
| gross          =
}}
 German countryside at the behest of his demented mother, portrayed by Anke Prothmann.

== Plot ==

Set roughly twenty years after the events of its predecessor, the film opens with Karl Berger, Jr., son of the previous films antagonist, using a machete to kill the sole survivor of a drug deal gone wrong. The film then switches to Hamburg reporter Paul Glas, who is meeting with an informant known as "Mr. X" to discuss a series of recent murders Glas believes are connected to the killing spree Karl, Sr. went on two decades ago. After showing Glas top secret photographs of several victims, Mr. X tells the reporter everything the authorities know about the murders.
 pornographic theatre, where he guns down the manager and over a dozen patrons. Upon returning home after this mass shooting, Karl discovers his mother has been decapitated, her severed head informing him "your fathers back" before expiring.

In the present day, Mr. X tells Glas that the police eventually discovered the killers cabin, which had an old womans head and at least fifteen dismembered bodies in it. When asked by Glas if the man responsible was ever caught, Mr. X says no, and bids Glas goodbye, reminding him that he absolutely must not publish the pictures he had given him. Leaving shortly after Mr. X does, Glas rips up the photographs, deeming the story he has been told too twisted to tell the public.

== Cast ==
* Andreas Schnass - Karl Berger, Jr.
* Anke Prothmann - Mother
* Claudia von Bihl - Shovel Victim

== Reception ==

On its page detailing the works of Andreas Schnass, the exploitation film database The Worldwide Celluloid Massacre deemed the film "worthless" and described its attempts at humor as "lame".  1000 Misspent Hours gave the film one and a half stars out of a possible five, stating that while Violent Shit 2 "actually represents a considerable improvement over what writer/director Andreas Schnaas had done previously" it is still "fucking dreadful" and "never   any real plot, most of the dialogue is almost completely irrelevant, and cinematographer Steve Aquilina is so hapless that he cant keep the action framed correctly even with a widescreen aspect ratio". 

Conversely, Soiled Sinema wrote, "It can be nauseating, but it has a camp aspect that mocks the attitude it seems to be glorifying. This is what elevates it from something like the August Underground films. At one point the killer is carrying a corpse singing I am the Greatest, I am the Best and the film ends with goofy, blood-soaked outtakes and rainbow-colored titles! The film is mostly a loose collection of gritty, fetishistic murder sequences, but told with a furious lack of artistry and pretension and sporting a bizarre, complex psychology that makes it a supreme B-Movie in my book. I actually hated this film until watching it after Giuseppe Andrews similar work Period Piece. Violent Shit II is a fun, confessional, dirty little movie made for no ones entertainment but its filmmakers, and it works." The review ended with the summation "I enjoyed Violent Shit II. Why? Because it constantly entertains."  

== DVD release ==
 Region 2-exclusive box sets containing it, and its predecessor and sequel. The first set was released by Astro Records & Filmworks, and the second one by IndependOr Video; both are out of print. 

It was announced in 2010 that Synapse Films would be releasing the first three Violent Shit films in a DVD box set with Zombie 90: Extreme Pestilence, another film by Andreas Schnass. 

== References ==

 

== External links ==

*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 