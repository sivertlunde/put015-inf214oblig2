Mickey's Amateurs
 
{{Infobox Hollywood cartoon
| cartoon name      = Mickeys Amateurs
| series            = Mickey Mouse
| image             = Mickeys Amateurs.jpg
| image size        = 
| alt               = 
| caption           = Mickey Mouse invites Donald Duck to recite "Twinkle, Twinkle, Little Star"
| director          = Pinto Colvig Erdman Penner Walt Pfeiffer
| producer          = Walt Disney
| story artist      = 
| narrator          = 
| voice actor       = Walt Disney Florence Gill Clarence Nash Pinto Colvig
| musician          = Oliver Wallace Tom Palmer
| layout artist     = 
| background artist =  Walt Disney Productions
| distributor       = United Artists
| release date      =   (USA)
| color process     = Technicolor
| runtime           = 8:24 minutes
| country           = United States
| language          = English
| preceded by       = Moose Hunters
| followed by       = Modern Inventions
}} Walt Disney Pete and Goofy.   

==Plot== Asleep in the Deep".

Next, Mickey introduces Donald Duck, who first presents an apple to Mickey in an attempt to win him over preemptively. But Donalds recitation of "Twinkle Twinkle Little Star" ends badly as he forgets the words. Mickey rings the gong and Donald is removed from stage. Just as Mickey is announcing the next act, a disgruntled Donald returns to take back the apple.
 Cluck and Clarabelle Cow|Belle." Clara Cluck sings a clucking version of "Il Bacio", a waltz by Luigi Arditi, accompanied by Clarabelle Cow on piano. Despite several blunders, the performance is the first to avoid the gong.
 he opens fire. Donald is quickly removed from stage. 

For the shows final act, Mickey introduces Goofy and his "50-piece band" which turns out to be a multi-instrumental device on wheels. Goofy begins with "In the Good Old Summer Time" and moves on to "Therell Be a Hot Time in the Old Town Tonight". But the tempo and intensity of the song destroys the machine in an explosion. Goofy emerges from the wreckage and sheepishly admits "It busted!" Donald breaks out of Goofys bandmaster hat and quickly recites a word-perfect recitation of "Twinkle Twinkle Little Star" and delivers a vengeful "Nya!" at the audience. But just then, the "iris out" effect which ends the cartoon closes on his neck. He struggles to keep it open but it finally closes. This is a rare instance of a Disney cartoon breaking the fourth wall.

==History==
Mickeys Amateurs pokes fun at "amateur hour" radio shows which were popular entertainment in the 1930s and 40s. Perhaps the most famous example is the Major Bowes Amateur Hour in which the host, Edward Bowes, was known to strike a gong to stop an amateur performance. Mickey Mouses repeating of the words "Okay, okay" in the film was recognized by audiences at the time as a parody of Bowes. 

The film was also inspired by the 1934 Disney film Orphans Benefit. This film also featured a stage show with acts interspersed by Donald attempting a poetic recitation.

==Reception==
The Motion Picture Herald published a review of Mickeys Amateurs on June 19, 1937 saying in part "The subject must be seen to be appreciated and enjoyed. The fun it offers defies description."

==Notes==
 

 

 
 
 
 
 