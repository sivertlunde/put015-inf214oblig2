Jwaar Bhata
{{Infobox film
| name           = Jwar Bhata
| image          = Jwar Bhata.jpg
| image_size     = 
| caption        = 
| director       = Adurthi Subba Rao
| producer       = Hargobind Duggal
| writer         = Rajendra Krishan
| narrator       = 
| starring       = Dharmendra Saira Banu Sujit Kumar Nazir Hussain
| music          = Laxmikant-Pyarelal
| cinematography = P.L. Rai
| editing        = G.G. Krishna Rao
| studio         = Roop Tara Studios
| distributor    = Bahar Films Combines
| released       = 2 November 1973
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}

Jwar Bhata is a 1973 Hindi film. Produced by Hargobind and N Bhansali, the film stars Dharmendra, Saira Banu, Jeevan, Rajindernath and Sujit Kumar. The films music is by Laxmikant Pyarelal.
==Plot==
Widowed and wealthy Durgadas Prasad(Nazir Hussain) is the sole owner of Shriman Mills. He has one son, who falls in love with a woman from a poor family. This disappoints Durgadas and he asks his son to leave. His son leaves, gets married, and soon has a son. Unfortunately, Durgadass son does not live for long, leaving his wife and son destitute. Durgadas has a change of heart and goes around looking for his son and his family, but in vain. Years later Durgadas is much more older and not expected to live long. He recruits a Secretary, Gayetri, who looks after him. Then when his relatives insist that he adopt his distant nephew, Anokhey, he agrees, only to have Gayetri bring home a young man, Billoo, who operates a simple Dal Roti restaurant. Durgadas is jubilant when he verifies that Billoo is indeed his grandson and throws a grand party. After the party, Durgadas(Nazir Hussain) passes away, leaving everything in Billoos hands. Billoo changes his name to Balraj and starts to look after his grandfathers business. Durgadass relatives, Iqbal Nath, his daughter, Rekha; a cousin, Satwani, and her son, Anokhey; along with Advocate Ramesh Khanna, conspire with each other and concoct of plan - a plan so devious that will not only entrap Billoo and Gayetri in it - but also make them loathe the day they met Durgadas.

==Cast==
  Saira Banu  ...  Gayatri  
 *Dharmendra  ...  Balraj Das Prasad Biloo (as Dharminder)  
 *Sujit Kumar  ...  Advocate Ramesh Khanna  
 *Nazir Hussain  ...  Durga Das Prasad (as Nazir Hussain)  
  Jeevan  ...  Iqbal Nath  
  Sunder  ...  Pandit Shiv Shankar  
  Rajendra Nath  ...  Anokhe Lal (as Rajindernath)  
  Shammi  ...  Satwanti  
  Jayshree T.  ...  Courtesan Phool Kumari  
  Meena T.  ...  Rekha Nath  
  Baby Guddi    
  Baby Pinky    
  Sabina  ...  (as Baby Sabira)  
  Randhir  ...  Gayatris Father  
  Shivraj  ...  Dr. Shivraj  


==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Turu Turu Turu"
| Kishore Kumar
|-
| 2
| "Rootha Hai To"
| Lata Mangeshkar
|-
| 3
| "Daal Roti Khao Prabhu Ke Gun Gaao"
| Kishore Kumar, Lata Mangeshkar
|-
| 4 
| "Hoton Pe Tera Naam"
| Asha Bhosle
|-
| 5
| "Peene Ki Der Hai Na Pilane Ki"
| Asha Bhosle
|-
| 6
| "Tera Mera Pyar Shuru"
| Kishore Kumar
|-
| 7
| "Woh To Rootha Hai To"
| Lata Mangeshkar
|}
== External links ==
*  

 
 
 
 

 