The Age of Stupid
 
 
{{Infobox film
| name           = The Age of Stupid
| image          = Age of stupid poster.jpg
| border         = yes
| alt            = A red timeline leads from the past into a ruined city scape. The film title "The Age of Stupid" is shown on the upper right in block capital blue writing  
| caption        = Theatrical release poster
| director       = Franny Armstrong
| producer       = Lizzie Gillett
| writer         = Franny Armstrong
| narrator       = Pete Postlethwaite David King  George Monbiot  Richard Heinberg  Ed Miliband
| music          = Chris Brierley
| cinematography = Franny Armstrong
| editing        = David G Hill
| studio         = Spanner Films
| distributor    = Dogwoof Pictures (UK) 
| released       =  
| runtime        = 89 minutes  
| country        = United Kingdom
| language       = English
| budget         = Pound sign|£450,000 
| gross          =
}}
The Age of Stupid is a 2009 British film by  , and first-time producer Lizzie Gillett. The executive producer is John Battsek, producer of One Day in September.

The film is a drama-documentary-animation hybrid which stars Pete Postlethwaite as a man living alone in the devastated world of 2055, watching archive footage from the mid-to-late 2000s and asking "Why didnt we stop climate change when we had the chance?"
 screening of the film and keep the profits for themselves.  

==Plot==
The film begins in the year 2055 in a world ravaged by catastrophic climate change; London is flooded, Sydney is burning, Las Vegas has been swallowed up by desert, the Amazon rainforest has burnt up, snow has vanished from the Alps and nuclear war has laid waste to India. An unnamed archivist (Pete Postlethwaite) is entrusted with the safekeeping of humanitys surviving store of art and knowledge. Alone in his vast repository off the coast of the largely ice-free Arctic, he reviews archive footage from back "when we could have saved ourselves", trying to discern where it all went wrong.
 effects of climate change and global civilization teetering towards destruction, he alights on six stories of individuals whose lives in the early years of the 21st century seem to illustrate aspects of the impending catastrophe. These six stories take the form of interweaving documentary segments that report on the lives of real people around the year 2008, and switch the films narrative form from fiction to fact. In addition to the framing narrative in 2055, the news clips and the documentary footage of the six personal stories, the films includes animated segments and brief interviews with Mark Lynas and George Monbiot, created for the film.

==Release==
  at his archival computer.]]
The films UK premiere was on 15 March 2009 in Londons Leicester Square.  The screening was held in a solar-powered ‘cinema tent’ and conducted without use of mains
electricity. An independent audit conducted by Carbon Accounting Systems found the events carbon emissions to be 1% of those produced by a normal blockbuster premiere. {{cite news
| last = Robinson | first = Karen | date = 2009-03-16
| title = Age of Stupid premiere: the green carpet treatment
| url = http://www.guardian.co.uk/environment/gallery/2009/mar/16/the-age-of-stupid-climate-change
| publisher = guardian.co.uk
| accessdate = 2009-12-19}}
   Linked by satellite to 62 cinemas around the UK, the premiere received a Guinness World Record for being the largest film premiere ever, based on number of screens. 

During the post show discussion, President Mohamed Nasheed received a standing ovation for announcing that the Maldives would be the worlds first carbon neutral country. {{cite web
| last = Schmidle | first = Nicholas | date = 2009-05-08
| title = Wanted: a new home for my country
| url = http://www.nytimes.com/2009/05/10/magazine/10MALDIVES-t.html?pagewanted=1&_r=1
| publisher = The New York Times
| accessdate = 2009-11-25}} Kingsnorth coal-fired power station in Kent.  A month later the Government announced a change to its policy on coal - no new coal-fired power station will get government consent unless it can capture and bury 25% of the emissions it produces immediately - and 100% of emissions by 2025. This, a source told the Guardian, represented “a complete rewrite of UK energy policy”. 

The UK premiere received the accolades of ‘Best Green Event’ from Event Awards and best Live Brand Experience in the PR Week Awards. 

In the UK, The Age of Stupid was released in 62 cinemas in its opening week and hit the top of the Box Office charts (by screen average). {{cite news| url=http://www.ukfilmcouncil.org.uk/media/pdf/t/6/2009_H1_Box_Office_Report_6.8.09.pdf Odeon Panton Street. {{cite news| url=http://www.spannerfilms.net/news/worldwide_stupid_cinema_takeover
 | work=Spanner Films | title=Stupid Worldwide Cinema Releases – H1 2009 | date=28 March 2011 |accessdate=28 March 2011 }} 

The Age of Stupid was launched in Australia and New Zealand on 19 August 2009 with simultaneous green carpet premieres in Auckland and Sydney, linked by satellite to 32 cinemas in Australia and 13 in New Zealand. {{cite news| url=https://www.youtube.com/watch?v=WYPIlsgIdnw
 | work=Spanner Films | title=Australia & New Zealand Premiere: Part 1 | date=19 August 2009 |accessdate=10 January 2011 }}  The film was then released in all 13 cinemas in New Zealand and many of the 32 in Australia. 
 Heather Graham, and Gillian Anderson. Pranksters, The Yes Men, walked up the green carpet in their “survivaballs”. {{cite news| url=http://theyesmen.org/agribusiness/halliburton/about/index.html
 | work=The Yes Men | title=Halliburton Solves Global Warming |accessdate=10 January 2011 }}  Many guests arrived by low-carbon transport, including sailboat, rowing boat, electric car, bicycle, bicycle rickshaw and rollerblades.

In Germany, the film was premiered in Berlin and Hamburg on 1 June 2010, followed by a limited cinema release in twelve cities: Berlin, Hamburg, München, Nürnberg, Bonn, Frankfurt, Potsdam, Gütersloh, Freiberg, Göttingen, Bremen, Hannover and Köln. 

The film had limited theatrical releases via distributors Mongrel Media in Canada and via Arts Alliance  in Argentina, Austria, Belgium, Brazil, Bulgaria, Chile, China, Costa Rica, Croatia, Cyprus, Czech Republic, Denmark, Finland, France, Greece, Honduras, Hong Kong, Hungary, Iceland, India, Indonesia, Iran, Israel, Italy, Japan, Jordan, Kazakhstan, Kenya, Kiribati, Kosovo, Kyrgyzstan, Lebanon, Luxembourg, Madagascar, Malaysia, Maldives, Malta, Mauritius, Mexico, Micronesia, Moldova, Mozambique, Nepal, Netherlands, Nigeria, Norway, Palestinian Territories, Papua New Guinea, Peru, Philippines, Poland, Portugal, Romania, Russia, Serbia, Sierra Leone, Singapore, Slovakia, Slovenia, South Africa, Spain, Swaziland, Sweden, Switzerland, Taiwan, Tanzania, Thailand, Turkey, Vanuatu, Venezuela, Vietnam and Zimbabwe. 

The film continues to be shown in local venues worldwide through Indie Screenings - a web-based form of film distribution, pioneered by Franny Armstrong, Age of Stupid’s Director, which allows anyone to buy a license to hold a screening of the film, keeping the profits for themselves. 

==Reception==
Writing for The Guardian, environmental activist George Monbiot, who appears in the film, said its "message, never stated but constantly emerging, is that we all have our self-justifying myths. We tell ourselves a story of our lives in which we almost always appear as the heroes. These myths prevent us from engaging with climate change." {{cite web
| author = George Monbiot
| date =  21 July 2008
| url = http://www.guardian.co.uk/commentisfree/2008/jul/21/climatechange.scienceofclimatechange
| title = Channel 4 is deceiving itself about global warming
| publisher = The Guardian
| accessdate = 2009-09-09
}}  The Financial Times critic described the film as intelligent and provoking, giving “The wisdom of hindsight, today”. {{cite web
| author = Nigel Andrews
| date =  18 March 2009
| url = http://www.ft.com/cms/s/2/aa1829b0-13d9-11de-9e32-0000779fd2ac.html#axzz1AdnEEUMX
| title = The wisdom of hindsight, today
| publisher = The Financial Times
| accessdate = 2010-01-10
}} 
Time Out Londons film editor, Dave Calhoun, said, "Armstrongs prognosis is apocalyptic, but her journalism is solid, instructive and pleasingly thoughtful," and described the film as "entertaining and provocative". {{cite web
| author = Dave Calhoun | date = 2009-03-19
| title = The Age of Stupid - Time Out Film review
| url = http://www.timeout.com/film/reviews/86796/the-age-of-stupid.html
| publisher = Time Out (company)
| accessdate=2009-12-07
}} 
The Times called the film "the most imaginative and dramatic assault on the institutional complacency shrouding the issue", saying, "The power of this shameless campaigning film is that it gives dates and deadlines. It explores options and ideas. It names culprits..." {{cite web
| author = James Christopher | date = 2009-03-19
| title = The Age of Stupid - Times Online review
| url = http://entertainment.timesonline.co.uk/tol/arts_and_entertainment/film/film_reviews/article5932304.ece
| publisher = The Times Online
| accessdate=2009-12-07
}} 
The Telegraphs reviewer, Sukhdev Sandhu, said, "Bold, supremely provocative, and hugely important,   film is a cry from the heart as much as a roar for necessary change." {{cite web
| author = Sukhdev Sandhu | date = 2009-03-19
| title = The Age of Stupid - Telegraph film review
| url = http://www.telegraph.co.uk/culture/culturecritics/sukhdevsandhu/5016266/The-Age-of-Stupid-review.html
| publisher = Telegraph.co.uk
| accessdate=2009-12-07
}} 
Based on only 12 reviews Rotten Tomatoes gave the film a rating of 73%.  The New York Times described the film as a "much sterner and more alarming polemic than An Inconvenient Truth". The review noted the "gallows humor" throughout the film, although the review was critical of the crude animated sequences. {{cite web
| last = Holden | first = Stephen | date = 2009-07-17
| title = An Alarm From 2055: Act Now to Save the Earth
| url = http://movies.nytimes.com/2009/07/17/movies/17age.html
| publisher = The New York Times
| accessdate = 2009-10-23}}
 
The Sydney Morning Herald described the film as "a wake-up call with an elegiac tone — not quite hectoring but pressing. This is about human nature, greed and personal responsibility. It aims to scare and galvanize — and its pretty good at both." {{cite web
| date = 19, 12 August:15PM
| author = Paul Byrnes
| title = The Age of Stupid - movie review
| url = http://www.thevine.com.au/entertainment/reviews/the-age-of-stupid-_-movie-review.aspx
| publisher = The Sydney Morning Herald
| accessdate = 2009-09-09 The Suns environment editor said "reality has caught up with the apocalyptic images." {{cite web
| author = Ben Jackson | date = 2009-02-18
| title = Predictions from extinction movie The Age Of Stupid already happening in real life
| url = http://www.thesun.co.uk/sol/homepage/features/article2247547.ece
| publisher = The Sun
| accessdate=2009-09-09
}} 
 William Nicholson, writer of Shadowlands and Gladiator (2000 film)|Gladiator, said “I hate this film. I felt as if I was watching all my own excuses for not doing anything about climate change being stripped away from me.” {{cite web
| author = Spanner Films | title = Famous people quotes
| url = http://www.spannerfilms.net/famous_people_quotes = Spanner Films
| accessdate=2010-01-10
}} 

Phelim McAleer attended the New York City premiere of the movie; when McAleer asked the movie director how she got to the premiere in New York, security kicked him out of the event. Later, McAleer told the Irish News that the film should have been called the "Age of Hypocrisy."  

===Awards===

*   - Best Green Doc 2008 {{cite web
| author = Spanner Films 
| date = 2008-07-27
| title = Awards
| url =  http://www.spannerfilms.net/awards 
| accessdate=2010-01-10 }} 
* Sunny Side of the Doc - Best Green Doc 2008 {{cite web
| author = Sunny Side of the Doc 
| date = 2008-07-27
| title = The 19th edition of SUNNY SIDE OF THE DOC. Results
| url = http://www.sunnysideofthedoc.com/upload/SITE%20WEB%202009/SUNNY%20EN%202009/Press_Release_Results08.pdf?PHPSESSID=b96bd36bad03262d39ddb3c6a55ef7e8
| publisher = Sunny Side of the Doc 
| accessdate=2010-01-10
}} 
* Sunny Side of the Doc - Film Most Likely To Be Cinema Hit 2008 
* Sunchild International Environmental Festival - First Prize  Birds Eye View Film Festival - Best Documentary 2009 
* British Independent Film Awards – Best Documentary 2009 (nominated) 

==Background==
Shot in seven countries over a period of three years, the film features six separate documentary stories, archive footage and lots of animation from, amongst others, Passion Pictures, creators of the Gorillaz animations. The original rough cut did not include the archivist which was added later to frame the story and better tie together the six parts. 

==Crowd funding== multiplex audience, but also to retain complete editorial control. These investors all own a percentage of the film and have received a pro-rata share in the profits, alongside the 105 crew who worked for survival wages. {{cite web
| last = Dell | first = Kristina | date=4 September 2008
| title = Time Magazine article on Crowd Funding the Age of Stupid
| url = http://www.time.com/time/magazine/article/0,9171,1838768,00.html
| publisher = Time.com
| accessdate=2009-09-09
}} 

In the Huffington Post, Jon Reiss described Armstrong’s approach as trailblazing: “the future of film, film culture and film distribution and marketing”. {{cite web
| last = Reiss | first = Jon | date=21 September 2009
| title = The Age of Stupid Is the Future of Film
| url = http://www.huffingtonpost.com/jon-reiss/ithe-age-of-stupidi-is-th_b_293361.html
| publisher = Huffington Post
| accessdate=2010-01-10
}} 

==Production==

In a 55-minute documentary, The Making of The Age of Stupid  Team Stupid reveal the lengths to which they went to make the film.

==Carbon footprint==
 CO 2 , and its promotion around 57 tonnes. {{cite web
| title = Carbon Footprint - The Age of Stupid
| url = http://www.ageofstupid.net/carbon_footprint
| publisher = Spanner Films
| accessdate = 2009-03-19
}} 

==Distribution== The RSA, following which there was a webcast panel discussion {{cite web
| date =
| title = Indie Screenings Launch Event
| url = http://www.ageofstupid.net/news/webcast_wizardry_the_rsa_indie_screenings_launch
| publisher = The Age of Stupid Nicholas Stern, Richard Betts.{{cite web
| title = Age Of Stupid: Indie Screenings: Live Webcast from the RSA of Indie Screenings Launch
| url = http://vimeo.com/4844257 RSA Panel video
| publisher = Spanner Films
| accessdate = 19 August 2014 }} The panel discussion was around the country watched at 71 local screenings which had been organised through the Indie Screenings system.

The Indie Screenings model immediately proved extremely popular, with 682 screenings booked in the first four months, generating more than £55,000. After the cost of writing the software  was paid off, 100% of the proceeds goes to pay back the crowd-funding investors. There were no wholesalers or resellers.

==Music  == score written by Chris Brierley.

==The Stupid Show==
  Copenhagen United Nations Climate Change Conference (7 December to 18 December 2009), called The Stupid Show. Franny Armstrong hosted the run of eight 40-60 minute shows which were broadcast live on the web daily from Friday 11 December until the final day of the conference, Saturday 19 December. Armstrong has described The Stupid Show as a budget version of The Daily Show with Jon Stewart but much sillier and more interactive. {{cite web
| date =
| title = The Stupid Show description
| url = http://www.ageofstupid.net/stupid-show
| publisher = The Age of Stupid
| accessdate = 2009-12-07
}}  
 Danish capital Copenhagen for the summit. Highlights from the run included punditry from Radiohead singer Thom Yorke, an interview with author Naomi Klein, an interview with Ed Miliband conducted by Franny Armstrong whilst standing on her head, and a live link-up to an underwater camera in Belize which showed how climate change is devastating coral reefs. The production team consisted of nine people, all of whom worked with Armstrong on The Age of Stupid. Key members of The Stupid Show crew include producer Lizzie Gillett, writer Mark Lynas and editor Justin Badger. {{cite web
| date =
| title = The Stupid Show crew
| url = http://www.ageofstupid.net/stupid-show
| publisher = The Age of Stupid
| accessdate = 2009-12-07
}}  

==10:10==
 
An offshoot of The Age of Stupid project is  , a  , 10:10 is presented as a strategy for people to take positive action in the face of such an urgent and daunting problem.

==See also==
* Retreat of glaciers since 1850
*  
* Climate change in popular culture
* Apocalyptic and post-apocalyptic fiction
* List of crowdsourcing projects
* Environmental migrant

==References==
 

==External links==
*  
*  
*  
*  , The Age of Stupid reviewed.
*   on whats missing in The Age of Stupid 
*  
 

 
 
 
 
 
 
 
 
 
 
 
 