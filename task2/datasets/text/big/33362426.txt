Saint Dracula 3D
 
 
{{Infobox film
| name           = Saint Dracula 3D
| image          = Saintdraculafilm.jpg
| alt            =  
| caption        = Promotional Poster
| director       = Rupesh Paul
| producer       = BizTV Network
| editor         = Ajay Devlokha
| starring       = Mitch Powell  Patricia Duarte Daniel Shayler  Suzanne Roche  Bill Hutchens Michael Christopher Anna Burkholder Lawrence Larkin
| music          = Sreevalsan J Menon
| cinematography = Francois Coppey
| editing        = Ajay Devlokha
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = English
| budget         = 
| gross          = 
}} 
Saint Dracula 3D is a 2012 film directed by Rupesh Paul.  Produced by BizTV Network - the producers of Dam 999, ‘Saint Dracula’ became the first ever stereoscopic 3D film to be made on the story of Dracula though director Vinayan has already directed Dracula in 3D in 2012. 

==Filming==
The film was shot in Liverpool, Manchester and Wales in the UK. Director Rupesh Paul along with Sohan Roy, the director of the film Dam 999 completed the movie with a cast and crew from the UK.
The entire movie was shot on camera RED by Frenchman Francois Coppey, the Director of Photography, while the stereography was done by Julian Crivelli. The movie was released in 2012.

As of March 2013, the film was available as a part of the Freestyle Life Film Exhibition. 

==Cast==
*Mitch Powell - as Dracula 
*Patricia Duarte - as Clara
*Daniel Shayler - as Benjamin
*Suzanne Roche - as Sr. Agnes
*Bill Hutchens - as Fr. Nicholson
*Michael Christopher - as The Bishop
*Anna Burkholder - as Hay
*Lawrence Larkin - as FBI Agent Carlo
*Nicola Jeanne - as The Mother Superior

==Crew==
*Written & Directed By - Rupesh Paul
*Project Designer - Sohan Roy 
*Producer - BizTV Network
*Co-Producer - Prabhiraj.N
*Cinematographer - Francois Coppey
* Editor- Ajay Devloka
*Stereographer - Julian Crivelli
*Colorist - Sapan Narula
*Music - Sreevalsan J Menon 
*Sound Design - Renjith Viswanathan
*Costume Designer - Nichola Parle
*Digital Image Technician - Wezley Joao Ferreira
*Animal Consultancy - Jakk Tennant

==Oscar nomination==
In 2012, the movie was one of 282 feature films nominated for an Oscar Award for Best Picture at the 85th Oscars. Two of the songs as well as the background score of the film were also nominated for the Best Song and Best Original Soundtrack award categories. http://timesofindia.indiatimes.com/entertainment/english/hollywood/news-interviews/Saint-Dracula-goes-to-the-Oscars/articleshow/17635297.cms  Saint Dracula 3D was one of two feature films from India to be included in the category, the second one being Akashathinte Niram by Dr Biju. 

==See also==
* Vampire film

== References ==
 

== External links ==
*  
*  

 
 
 
 
 