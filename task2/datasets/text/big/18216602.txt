Mataharis
{{Infobox film
| name           = Mataharis
| image          = Mataharis.jpg
| image_size     =
| border         =
| alt            =
| caption        = Theatrical release poster
| director       = Icíar Bollaín
| producer       = Simón de Santiago
| writer         =Icíar Bollaín Tatiana Rodríguez
| starring       =Najwa Nimri María Vázquez Nuria González Diego Marín Tristán Ulloa 
| music          = Lucio Godoy
| cinematography = Kiko de la Rica
| editing        = Angel Hernández Zoido
| released       =  
| runtime        = 1h 40 min
| country        = Spain
| language       = Spanish
| budget         = €2,500,000
}}
 2007 Spain|Spanish drama film directed by Icíar Bollaín. It stars Najwa Nimri, Tristán Ulloa, María Vázquez and Nuria González.  The plot follows the lives of three female detectives who investigate the secrets of others, but end up uncovering their own families lies and truths they would prefer to ignore.

==Plot==
Inés, Eva and Carmen, three women of different generations, work as private detectives for an investigation agency run by Valbuena, their male chauvinist boss. Inés, the youngest and more ambitious of the three is assigned to work undercover as a factory employee to spy on the activities of its union leaders. However, her investigation brings her face to face with a difficult emotional and ethical decision.  Initially she wants to get higher up in the agency with this, the agency’s most important case. However, in the process, she empathises with the cause of Manuel, one of the key members she is investigating, as he is defending the workers rights against the company, and develops feelings towards him. Her ethics and feelings, mixed with the fear of losing her job and career, turn her life into a turmoil.  

Eva has just returned to work at the detective agency after having had her second child. She is assigned to a rather mundane case of tracking down an old mans first love. Eva finds juggling her responsibilities very hard, and she does not find the support she needs in her husband Iñaki. When a mysterious woman called Marta appears in Iñaki’s life, Eva starts an investigation into her spouse’s secret life stumbling upon the possibility that her husband may not only be having an affair but leading a double life with another wife and family. She follows him from Madrid to Zaragoza where she quickly finds out that he has had a son born from a previous relationship and now has become a willing participant in the life of a child, ignored for 10 years. 

Carmen, the oldest and most experienced of the three detectives, is on a routine investigation of both the business partner and the wife of Sergio, a photographer whose wife is having an affair with his business partner and close friend. Carmen sympathizes with Sergios marital collapse not seeming to notice the dire straits of her own marriage.

==Cast==
* Najwa Nimri - Eva
* Maria Vázquez- Inés
* Nuria González  - Carmen
* Diego Marín - Manuel
* Tristán Ulloa - Iñaki
*Fernando Cayo – Valbuena
* Antonio de La Torre - Sergio
* Adolfo Fernández – Alberto

==DVD release== Spanish with English subtitles.

==External links==
* 
*http://www.sogecine-sogepaq.com/mataharis/
* http://www.cinefilo.es/cine-mataharis-6506.html

 
 
 
 
 