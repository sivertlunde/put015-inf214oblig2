The President (1961 film)
{{Infobox film
| name           = The President
| image          = 
| alt            = 
| caption        = 
| director       = Henri Verneuil
| producer       = 
| writer         = Michel Audiard
| starring       = Jean Gabin Bernard Blier
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  	
| runtime        = 94 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
The President ( ) is a 1961 French thriller film directed by Henri Verneuil. It is based on the novel of the same title by Georges Simenon.

==Cast==
* Jean Gabin - Émile Beaufort
* Bernard Blier - Philippe Chalamont
* Renée Faure - Mademoiselle Milleran
* Alfred Adam - François
* Henri Crémieux - Antoine Monteil
* Louis Seigner - Henri Lauzet-Duchet
* Robert Vattier - Le docteur Fumet
* Françoise Deldick - Huguette
* Hélène Dieudonné - Gabrielle
* Pierre Larquey - Augustin
* Jacques Marin - Gaston

==External links==
*  

 

 
 
 
 
 
 
 

 
 