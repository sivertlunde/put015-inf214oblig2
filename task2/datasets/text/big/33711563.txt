Rice (film)
 
{{Infobox film
| name           = Rice
| image          = Rice_poster.jpg
| caption        = Film poster
| director       = Shin Sang-ok
| producer       = Shin Sang-ok
| writer         = Kim Kang-yun
| starring       = Shin Young-kyun
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = South Korea
| language       = Korean
| budget         = 
}}
 Best Foreign Language Film at the 39th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Shin Young-kyun
* Choi Eun-hee
* Nam Kung-won
* Lee Ki-hong

==See also==
* List of submissions to the 39th Academy Awards for Best Foreign Language Film
* List of South Korean submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 