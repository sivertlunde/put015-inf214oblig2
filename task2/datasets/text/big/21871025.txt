Ön
 
{{Infobox film
| name           = Ön
| image          = Ön film poster.jpg
| caption        = Film poster
| director       = Alf Sjöberg
| producer       = 
| writer         = Bengt Jahnsson Alf Sjöberg
| starring       = Per Myrberg
| music          = Lars Johan Werle
| cinematography = Lasse Björne
| editing        = Carl-Olov Skeppstedt
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
 Best Director.  The film was also entered into the 1966 Cannes Film Festival.   

==Cast==
* Per Myrberg as Count Magnus
* Bibi Andersson as Marianne
* Karin Kavli as Old countess
* Marian Gräns as Helen Andersson
* Jan-Olof Strandberg as Johannes
* Ernst-Hugo Järegård as Vicar Byström
* Anders Andelius as Boman
* Mona Andersson as Mrs. Eriksson
* Björn Berglund as Dr. Ernst Forsman
* Sture Ericson as Viktor Sundberg
* Stig Gustavsson as Öberg
* Agda Helin as Mrs. Sundberg
* Erik Hell as Pettersson
* Olle Hilding as Persson
* Victoria Kahn as Helen Andersson - child
* Åke Lagergren as Olsson
* Birger Lensander as Parish clerk
* Sten Lonnert as Eriksson
* Gösta Prüzelius as Berg
* Sven-Bertil Taube as Police officer
* Torsten Wahlund as Lind the farm-hand

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 