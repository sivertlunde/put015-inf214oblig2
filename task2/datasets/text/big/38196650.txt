Bharat Aala Parat
 

{{Infobox film
| name           = Bharat Aala Parat
| image          = Bharat Aala Parat.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Vijay Gokhale  
(Chief Assistant Director -   
| producer       = R. Krishnamurthy
| starring       = Bharat Jadhav, Viay Gokhle, Surekha Kudchi, Vijay Chavan, Ansuman Vichare and others
| music          = Dyanesh Kumar
| cinematography = Samir Athle
| editing        = Vijay Khochikar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Bharat Aala Parat is a Marathi film released on 23 October 2007. The film has been produced by R. Krishnamurthy and directed by Vijay Gokhale. The film is based on the bond between an uncle and nephew, and the lengths the nephew would go to teach his uncle a lesson.

== Cast ==
The cast includes Bharat Jadhav, Vijay Gokhale, Surekha Kudchi, Vijay Chavan, Ramesh Bhatkar, Gauri Sarawate & Others. 

==Soundtrack==
The music has been directed by Dyanesh Kumar.

== References ==
 
 

== External links ==
*  
*  
*  

 
 
 


 