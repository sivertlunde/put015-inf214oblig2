Awaking from a Dream
{{Infobox film
| name           = Awaking from a Dream
| image          = 
| caption        = 
| director       = Freddy Mas Franqueza
| producer       = 
| writer         = Freddy Mas Franqueza
| starring       = María Almudéver
| music          = 
| cinematography = Carles Gusi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Awaking from a Dream ( ) is a 2008 Spanish drama film written and directed by Freddy Mas Franqueza. It was entered into the 30th Moscow International Film Festival.   

==Plot==
At 8 years of age, Marcels mother drops him off at her estranged fathers house and leaves him there to follow her boyfriend to Germany.  Marcels grandfather and gives him the love and attention he lacked from his mother.  By the age of 21, Marcel has plans to move out of his grandfathers home and into a house with his girlfriend.  However, by this time his grandfather begins showing symptoms of Alzheimers and Marcel is faced with the choice of leaving with his girlfriend or staying to care for the only relative that ever truly loved and cared for him.  

==Cast==
* María Almudéver
* Héctor Alterio as Pascual
* Álvaro Báguena
* Aurora Carbonell
* Alberto Ferreiro as Marcel (20 años)
* Aroa Gimeno as Bea
* Mónica López (actor)|Mónica López as Alina
* Sergio Padilla as Marcel (niño)
* Oriol Tarrasón

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 