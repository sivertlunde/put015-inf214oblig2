Hulla
{{Infobox Film
| name           = Hulla
| image          = hulla.jpg
| caption        = 
| director       = Jaideep Varma
| producer       = Sunil Doshi
| story          = Jaideep Varma
| screenplay     =
| starring       = Rajat Kapoor Sushant Singh Kartika Rane Vrajesh Hirjee Indian Ocean
| cinematography = Paramvir Singh
| editing        = 
| studio         = Reliance Big Pictures
| distributor    = HandMade Films
| released       = Sebtember 19, 2008
| runtime        = 
| country        = India Hindi
| awards         =
| budget         =
}}

Hulla ( ,   film directed by Jaideep Varma, and starring Sushant Singh, Rajat Kapoor, Kartika Rane, Vrajesh Hirjee, Darshan Jariwala and Ravi Jhankal. The film was produced by Suniel Doshi, under Reliance Big Pictures and HandMade Films.

Upon release, the film performed much better than expected at the box office. Expectations for the film were low, and it opened to a below-average opening. However, due to positive reviews and strong word-of-mouth, the film picked up in many places and sustained well. The film was declared an poor performer at the box office.

==Plot==
Raj (Sushant Singh), an aggressive stock-broker in a prosperous broking firm and Abha (Kartika Rane), a marketing professional are a couple in their early-thirties. They move into a new 2-bedroom flat in a Mumbai suburb expecting peace and quiet. Life is sweet as their promising careers now seem to be nicely complemented by domestic stability.

Cracks start appearing in their ideal existence when Raj who is light sleeper is disturbed by noises at nights. They wake him several times in the nights (Abha is undisturbed though). Finally one night, Raj goes down to investigate and discovers that it is the night watchman blowing the whistle periodically in the night to scare thieves away. Raj scolds the watchman and forbids him from making any further noise but the secretary, Janardan (Rajat Kapoor) insists on the whistling continuing for building security. Raj tries to garner support from fellow-residents but to no avail.

An issue that starts out lightly and amusingly trivial begins to escalate to take the shape of a serious problem for Raj. He begins to slowly go to pieces. Not being able to sleep at night begins to take a serious toll on him, both professionally and personally. At work, he becomes edgy and his usual smart sense of judgment suffers. At home, he becomes obsessed with any loud noises in the environment. His disgust at being unable to solve a seemingly simple problem such as this begins to drive him crazy. Moreover, to his great irritation, no-one really sympathizes with his problem - not Abha, not Dev (Vrajesh Hirjee) - his friend at work, not any of the neighbours. It annoys Raj immensely, because he genuinely believes that this nightly noise-making is an illogical, uncivilized act (there is also a hint that it is Rajs conscience thats not allowing him to sleep, rather than the whistling, but it is deliberately left ambiguous).

"Hulla" is story of a simple annoyance affecting a persons life and bringing about dramatic unexpected change that brings the real character and emotions of the Raj and the people surrounding him, to the fore.

==Cast==
* Sushant Singh as Raj Puri; a high-tempered sub-broker
* Rajat Kapoor as Janardhan; the secretary of a Mumbai suburb in which Raj lives
* Vrajesh Hirjee as Dev; one of Rajs closest friends & also his colleague   
* Kartika Rane as Abha; a marketing professional, Rajs wife
* Darshan Jariwala as Abhas father
* Ravi Jhankal as the Chief Minister
* Jeetu shivhare as victim in police station

==Critical Reception==

Hulla was featured in Avijit Ghoshs book, 40 Retakes: Bollywood Classics You May Have Missed

==External links==
*  
*   at the Internet Movie Database


 
 
 