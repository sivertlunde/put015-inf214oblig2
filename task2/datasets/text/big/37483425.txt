Kuro (film)
{{Multiple issues|
 
 
 
}}

  is a 2012 Japanese film written and directed by Daisuke Shimote and presented at the 25th Tokyo International Film Festival. It was Shimotes directorial debut.

== Synopsis ==
Kuro dreamed of becoming a baker but got fired from the bakery where she worked. Eito is a photographer who broke up with his fiance. Gou is a stage producer who gets into trouble because his lead actress goes missing. These three people who have nothing in common meet by chance. They start living in a deserted hotel near the sea. In the hotel, initial indifference transforms into a growing friendship. They start getting closer while keeping some distance, enjoying a carefree life. Gou starts writing a screenplay, casting Kuro as the lead actress. Kuro is attracted to Eito, the photographer. Eito still cant get over the bitter experience of his ex-girlfriend. No one expresses their feelings.  At this time, Momo, a schoolgirl enters the plot. Even though Momo stays for a short period, she influences their unconscious triangular relationship and strengthens their bonds. Ghey visit the house where Kuro spent her childhood and their relationship takes a turn. Gou completes his script for Kuro and asks her to play the lead. Kuro starts getting distant from Eito. She leave the hotel feeling disappointed. The bond between them fades away. They get back to their old life. However, they feel altered by this experience. Everything in their daily lives looks fresh. The experience has brought a positive change in their working style, thinking and their lives.

==Characters==

===Kuro Ueki (20 years old)===
Kuro is a cashier at a bakery. She aspires to be a baker, but the shop owner wont let a part-timer near the oven, much to her frustration. One pastry at the bakery is very popular. When Kuro helps herself to some, she gets caught and is fired. She cant pay her rent and gets loses her apartment.

===Eito Suzuki (27 years old)===
Eito is a freelance photographer who takes black and white photos on film. He plans to marry live-in girlfriend, Nana. On the day that theyre going to make it official, a misunderstanding leads to an argument, which leads to their separation. Eito heads for the abandoned hotel where he and Nana were supposed to have their "honeymoon" weekend.

===Gou Kimura (35 years old)===
Gou is a stage director who is attracting attention at local playhouses. Rei, his wife, always stars in his plays. This time, however, the script is not ready for an upcoming production. His desperation leads to more strife. A known womanizer, he flirts with one of the young actresses, Kizashi. Rei gets upset and quits. Only days from the opening performance his producer puts the pressure on.

==Film festival screening==
* 2012: 25th   Japanese Eyes
* 2013:   Film section: Competition 1-2
* 2013:  
* 2013:  
* 2013:   International New Talent Competition
* 2013:   EEFF Best Feature Competition 
* 2013:   Bronze Palm Award
* 2013:  

==References==
 
*  
*  
*  
*  
 
 

== External links==
*  
*  
*  

 
 