Julie (1956 film)
{{Infobox film
| name           = Julie
| image          = Julie (1956 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| producer       = Martin Melcher
| director       = Andrew L. Stone
| screenplay     = Andrew L. Stone
| starring       = Doris Day Louis Jourdan
| music          = Leith Stevens
| cinematography = Fred Jackman Jr.
| editing        = Virginia L. Stone
| studio         = Arwin Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $785,000   }}. 
| gross = $2.6 million  
}}
Julie is a 1956 film noir written and directed by Andrew L. Stone and starring Doris Day. 

==Plot==
A former stewardess, widow Julie Benton is terrorized by her insanely jealous second husband, Lyle. It becomes a life-or-death matter after friend Cliff Henderson relays his suspicions to Julie that her first husbands death might not have been a suicide.

Pretending that she would have fallen for Lyle even if her first husband had still been alive, Lyle confesses the murder to her. Julie flees with Cliffs help, but police are unable to arrest Lyle without proof.

Julie instead changes her identity, moves north and returns to her former job with the airline. Lyle has a confrontation with Cliff, shoots him and learns where Julie can be found.

With police in pursuit, Julie is warned that Lyle might be on her flight. She spots him, but Lyle pulls a gun on her, then kills the pilot before being shot himself. Julie must be instructed on how to land the aircraft. She does so successfully, and her nightmare comes to an end.

==Cast==
* Doris Day as Julie Benton
* Louis Jourdan as Lyle Benton Barry Sullivan as Cliff Henderson
* Frank Lovejoy as Det. Lt. Pringle Jack Kelly as Jack (co-pilot)
* Ann Robinson as Valerie
* Barney Phillips as Doctor on Flight 36
* Jack Kruschen as Det. Mace
* John Gallaudet as Det. Sgt. Cole
* Carleton Young as Airport Control Tower Official
* Hank Patterson as Ellis Ed Hinton as Captain of Flight 36
* Harlan Warde as Det. Pope
* Aline Towne as Denise Martin
* Eddie Marr as Airline Official
* Joel Marston as Garage Mechanic
* Mae Marsh as Hysterical Passenger

==Background==
It is also notable as potentially the first film to feature the subplot of a stewardess piloting a plane to safety, later used in Airport 1975 (1975) and parodied in Airplane! (1980). It is further notable for being technically accurate.

==Reception==

===Box office===
According to MGM records the film earned $1,415,000 in the US and Canada and $1,185,000 elsewhere resulting in a profit of $604,000. 

===Critical response===
Film critic Dennis Schwartz gave the film a mixed review, writing, "Improbable crime thriller about a woman-in-peril, that is too uneven to be effective; the banal dialogue is the final killer. ... Doris Day, to her credit, gives it her best shot and tries to take it seriously even when the melodrama moves way past the point of just being ridiculous. Later disaster movies stole some of those airplane landing scenes." 
 John Wilsons book The Official Razzie Movie Guide as one of the The 100 Most Enjoyably Bad Movies Ever Made. 

===Accolades=== Best Original Best Song ("Julie" by Leith Stevens and Tom Adair, which Doris Day sings during the opening credits).

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 