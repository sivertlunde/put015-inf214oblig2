The Keep (film)
 
 
{{Infobox film
| name           = The Keep
| image          = Keepposter.jpg
| image_size     =
| caption        = Original film poster for The Keep Michael Mann
| producer       = {{plainlist|
* Gene Kirkwood Howard W. Koch, Jr.
 }}
| screenplay     = Michael Mann
| based on       = {{plainlist| The Keep by
* F. Paul Wilson
 }}
| starring       = {{plainlist|
* Scott Glenn
* Jürgen Prochnow
* Robert Prosky
* Ian McKellen
 }}
| music          = Tangerine Dream
| cinematography = Alex Thomson
| editing        = {{plainlist|
* Dov Hoenig
* Chris Kelley
 }}
| distributor    = Paramount Pictures
| released       =  (usa)
| runtime        = 96 min.
| country        = United States
| language       = English
| budget         = $6,000,000 (est.)
| gross          = $3,661,757 (USA)
}}
 Michael Mann same name, published in 1981 (1982 in the United Kingdom).

==Plot== German Army SD Sturmbannführer Eric Kaempffer then arrives to deal with what is thought to be partisan activity, executing villagers as collective punishment.
 Jewish historian, Christian crosses, is weakened and drawn back into the innermost recesses. Glaeken is transfixed, taking the place of the seal that was broken by the German looters.

==Cast==
* Scott Glenn as Glaeken Trismegestus
* Alberta Watson as Eva Cuza
* Jürgen Prochnow as Captain Klaus Woermann
* Robert Prosky as Father Fonescu SD Sturmbannführer Erich Kaempffer
* Ian McKellen as Dr. Theodore Cuza
* W. Morgan Sheppard as Alexandru
* Royston Tickner as Tomescu Michael Carter as Radu Molasar
* Rosalie Crutchley as Josefa
* Wolf Kahler as S.S. Adjutant
* Bruce Payne as Border Guard

==Production==
===Filming=== slate quarry near Llanberis in North Wales.  Some interiors of the keep utilised the stonework within the Llechwedd Slate Caverns, near Blaenau Ffestiniog. Due to heavy rain, the film suffered significant delays in its shooting schedule.  Shepperton Studios near London was used for interior Keep scenes featuring the demon Molasar. A secondary crew also went to Spain for footage depicting Greece.

The special effects for the creature were made by Nick Maley, helped by Nick Allder, who had previously worked on Alien (film)|Alien and The Empire Strikes Back.  Molasar was conceived by Enki Bilal.

===Music===
  Michael Mann on his first theatrical film Thief (film)|Thief. The score to The Keep is primarily made up of moody soundscapes, as opposed to straightforward music cues, composed by Tangerine Dream. Most notably, an ambient cover of Howard Blakes "Walking in the Air" was featured during the end sequence of the film. Additionally, Tangerine Dreams arrangement of the song "Gloria" from Mass for Four Voices by Thomas Tallis can also be heard in the film.
 original soundtrack CDs were sold at a concert by the group in the UK in 1997, and Virgin Records soon announced that the album would be available for general release in early 1998, but legal issues with the film studio stopped the release. The full score can be found in the laserdisc and VHS versions of the film.

==Release==
The film, extensively cut by the studio from its original over three hours of runtime      to just over one-and-half hour, was given a limited release theatrically in the United States by Paramount Pictures on 16 December 1983. It grossed $4,218,594 at the domestic box office.  A board game based on the film was designed by James D. Griffin and published by Mayfair Games.  Under their Role Aids label, Mayfair Games also produced a role-playing game adventure based on The Keep. 

The film was released on laserdisc and VHS by Paramount Home Video.   , the film has not been officially released on DVD or Blu-ray Disc in any country, but is available for streaming on Amazon instant video and available on Netflix (UK and Ireland), streaming with the Tangerine Dream soundtrack.

==Reception==
 
The Keep has received generally negative reviews, with a Rotten Tomatoes rating of 31%. 

Michael Nordine in the LA Weekly stated The Keep "can’t always keep its many moving parts in lockstep, what with its hinted-at mythos that obscures more than it elucidates and its cast of enigmatic characters whose precise dealings with one another are never made entirely clear". However Nordine praised Manns direction, saying it showed "Manns... rare ability to elevate ostensibly schlocky material into something dark and majestic". 

F. Paul Wilson has publicly expressed his distaste for the film version, writing in the short story collection The Barrens (and Others) that it is "Visually intriguing, but otherwise utterly incomprehensible." In the foreword of the graphic novel adaptation, he expressed disappointment, claiming to have created the comic "Because I consider this visual presentation of THE KEEP my version of the movie, what could have been...what should have been."

==References==
 

==External links==
*  
*  
*   wherein Eva revives Glaeken (not on VHS, Beta, LaserDisc nor Netflix)
*   Facebook page
*   dedicated to getting a director’s cut of the film released to DVD or Blu-ray
*   fansite
*   Facebook page
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 