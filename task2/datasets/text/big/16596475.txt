The Boat That Rocked
 
 
 
{{Infobox film
| image          = The boat that rocked poster.jpg
| alt            = four men walking the plank
| caption        = Theatrical release poster
| director       = Richard Curtis
| producer       = {{Plain list |
* Tim Bevan
* Eric Fellner
* Hilary Bevan Jones
}}
| writer         = Richard Curtis
| starring       = {{Plain list |
* Philip Seymour Hoffman
* Bill Nighy
* Rhys Ifans
* Nick Frost
* Kenneth Branagh
 
}}
| music          =   Danny Cohen
| editing        = Emma E. Hickox
| studio         = Working Title Films
| distributor    = {{Plain list | Universal  
*StudioCanal  
*Focus Features  
}}
| released       =  
| runtime        = 135 minutes   
| country        = {{Plain list |
*United Kingdom
*Germany
*France
*United States 
}}
| language       = English
| budget         = US$50 million   
| gross          =US$36,348,784 
}}
 rock and pop music to the United Kingdom from a ship anchored in the North Sea while the British government endeavours to shut them down. It was produced by Working Title Films for Universal Pictures, and was filmed on the Isle of Portland and at Shepperton Studios.

After the world premiere in Londons Leicester Square on 29 March 2009,  the film was released in United Kingdom and Ireland on 1 April 2009. It was a commercial failure at the British box office, making only US$10.1 million in its first three months, just a fifth of its US$50 million production cost.  It received mixed reviews, with most criticism directed at its muddled storyline and 2¼-hour length. For its North American release the film had its running time cut by 20 minutes, and was retitled Pirate Radio. Opening 13 November 2009, it was still commercially unsuccessful  in the US, earning only US$8 million. When the worldwide theatrical run was finished in January 2010, the film had grossed US$36.3 million. 

== Plot == pirate radio rock and pop music that is not played on BBC Radio. Seventeen-year-old Carl (Tom Sturridge), recently expelled from school, is sent to stay with his godfather Quentin (Bill Nighy), who runs the station "Radio Rock" anchored in the North Sea. The eclectic crew of disc jockeys and staffers, led by the brash American DJ "The Count" (Philip Seymour Hoffman), quickly accept Carl as one of their own.

In London, government minister Sir Alistair Dormandy (Kenneth Branagh) resolves to shut down pirate radio stations due to their commercialism and low morals, instructing his subordinate Twatt (Jack Davenport) to find legal loopholes that will serve this end. They attempt to cut off the stations revenue by prohibiting British businesses from advertising on unlicenced radio stations. Quentin counters this by bringing massively popular DJ Gavin Kavanagh (Rhys Ifans) out of retirement and onto Radio Rock, enticing his advertisers to work around the law by paying their bills from abroad. Gavins popularity creates a rivalry between himself and The Count, who was initially brought to Radio Rock as Gavins replacement.

On his eighteenth birthday Carl is introduced to Quentins niece Marianne (Talulah Riley) and falls instantly in love with her, but is heartbroken when she is seduced by Doctor Dave (Nick Frost). Carls roommate "Thick" Kevin (Tom Brooke) observes that the sex, drug, and alcohol-filled atmosphere of Radio Rock is clearly no place for Carl to get on the straight-and-narrow. He theorises that the real reason Carls mother sent him there is that his father—whom Carl has never met—is someone on the ship, with Quentin being the likeliest suspect.

DJ "Simple" Simon Swafford (  in a clash of egos, reconciling after they are both injured by jumping into the ocean below. When Carls mother Charlotte (Emma Thompson) visits for Christmas, she denies his suspicion that Quentin is his father. As she departs, Carl passes on a cryptic message from reclusive late-night DJ "Smooth" Bob Silver (Ralph Brown), leading to the unexpected revelation that Bob is actually his father. Soon afterwards, Marianne returns to the ship and apologises to Carl for sleeping with Doctor Dave. She and Carl have sex that night. The following morning, The Count and the rest of the DJs announce the news of the event to millions of cheering fans all over Britain.
 Marine Offences Parliament and takes effect at midnight on 1 January 1967. The Radio Rock crew choose to defy the law and continue to broadcast, firing up the ships engine so that they may avoid arrest by relocating. The aging vessel cannot take the strain, causing the engine to explode and the ship to start sinking. The DJs broadcast their position in hope of aid, but Dormandy refuses to send rescue boats. Carl rescues the oblivious Bob from his cabin, while The Count vows to continue broadcasting as long as possible.

With the lifeboats inoperable, all gather on the prow as the ship begins to go down. They are rescued by dozens of fans, who heard about their broadcast predicament, and motored out in fleet of small boats to save them; Carl himself is rescued by Marianne. The Radio Rock ship disappears beneath the sea, with The Count emerging from the sinking vessel at the last moment. Though pirate radio in Britain comes to an end, the music lives on, with rock and pop becoming increasingly popular in subsequent decades, broadcast over hundreds of legal stations around the world.

== Cast ==
  pirate Radio Caroline in 1964.   
* Tom Sturridge as "Young" Carl, who is sent to stay with his godfather Quentin on the Radio Rock ship.
* Bill Nighy as Quentin, Carls godfather, who runs Radio Rock.
* Will Adamsdale as "News" John Mayford, the stations news and weather reporter.
* Rhys Ifans as Gavin Kavanagh, a massively popular DJ brought out of retirement by Quentin, leading to a professional rivalry with The Count.
* Nick Frost as DJ "Doctor" Dave, who unsuccessfully attempts to help Carl lose his virginity, and later has sex with Carls crush, Marianne.
* Tom Brooke as "Thick" Kevin, Carls intellectually dense cabin-mate and member of the Radio Rock staff. 
* Rhys Darby as Angus "The Nut" Nutsford, DJ and lone New Zealander on the ship.
* Katherine Parkinson as Felicity, the lesbian cook and the only single woman permitted to live on the ship.
* Chris ODowd as "Simple" Simon Swafford, Radio Rocks breakfast DJ who marries Elenore and learns on their wedding night that she married him to be near Gavin. ODowd drew inspiration from Tony Blackburn, the morning DJ on pirate station Radio Caroline in the 1960s, and his Irish contemporary Larry Gogan.   
* Tom Wisdom as "Midnight" Mark, Radio Rocks suave night time DJ who rarely speaks but has female listeners swooning over him; he is known as "The Sexiest Man on the Planet". In one scene, he "entertains" about 30 naked women who are part of a large group of fans that visit the ship. 
* Ralph Brown as hippy "Smooth" Bob Silver, "The Dawn Treader" (3-6am shift), Radio Rocks reclusive early-morn DJ who is secretly Carls father.
* Ike Hamilton as Harold, the stations radio assistant. government minister who endeavors to shut down pirate radio stations. Portrayed as a Conservative, but based on Labour Postmaster General Tony Benn.
* Sinead Matthews as Miss C (aka Miss Clitt, as revealed in the DVD deleted scenes), Dormandys assistant who secretly listens to Radio Rock.
* Jack Davenport as Domenic Twatt, Dormandys subordinate who is assigned the task of finding legal loopholes that can be used to shut down pirate radio stations.
* Talulah Riley as Marianne, Quentins niece, (and a fan of Dave) with whom Carl falls instantly in love.
* Emma Thompson as Charlotte, Carls mother.
* January Jones as Elenore (the goddess according to Quentin), who marries Simon so that she can live on the ship and carry on a sexual relationship with Gavin; Simon ends the marriage after 17 hours.
* Gemma Arterton as Desiree, a female fan; Dave tries to convince her to have sex with Carl.
 Stephen Moore Prime Minister, Michael Thomas and Bohdan Poraj as Dormandys subordinates Sandford and Fredericks, Olegar Fedoro as the Radio Rock ships captain, Francesca Longrigg and Amanda Fairbank-Hynes as Dormandys wife and daughter, and Olivia Llewellyn as Mariannes friend Margaret and Felicitys love interest.

==Production==
  in Trafalgar Square]]
The film was written and directed by Richard Curtis and made by Working Title Films for Universal Studios.    The producers for Working Title were Tim Bevan, Eric Fellner and Hilary Bevan Jones, with Curtis, Debra Hayward and Liza Chasin acting as executive producers.    Principal photography started on 3 March and continued until June 2008.  Filming took place on the former Dutch hospital ship Timor Challenger, previously De Hoop, moored in Portland Harbour, Dorset; the "North Sea" scenes were shot off the coast of Dunbar, East Lothian. Boat interior shots were filmed inside a warehouse in Osprey Quay on the Isle of Portland    and at Shepperton Studios.    They  also visited Squerryes Court in Kent to shoot the scenes of the home of government minister Alistair Dormandy (Kenneth Brannagh).  The films production cost exceeded £30 million.   

===North American release===
Following the films commercial failure at the British box office, Focus Features commissioned a re-edited version for release in North American release 13 November 2009.   Retitled Pirate Radio, this version of the film deleted approximately twenty minutes of footage from the original version to address complaints from several critics that the films running time was excessive. Upon the release of Pirate Radio in the United States, Manohla Dargis wrote: 
 "Stuffed with playful character actors and carpeted with wall-to-wall tunes, the film makes for easy viewing and easier listening, even if Mr. Curtis, who wrote and directed, has nothing really to say about these rebels for whom rock n roll was both lifes rhyme and its reason." 

Robert Wilonsky, reviewing Pirate Radio after having seen The Boat That Rocked and its UK home video release, said the U.S. theatrical release had had "most of its better bits excised"; according to Wilonsky, "after watching the DVD, Pirate Radio feels so slight in its current incarnation. Shorn of the scenes that actually put meat on its characters frail bones, the resulting product is vaguely cute and wholly insubstantial, little more than a randomly assembled hodge-podge of scenes crammed in and yanked out that amount to yet another movie about rebellious young men sticking it to The Grumpy Old Man&mdash;this time, with a tacked-on Titanic (1997 film)|Titanic climax."    Marine Offences Act, including the station portrayed on the film.

The trailer in North America also featured dialog from a scene not in the release; chief among which where a British government minister was being told in a voiceover that the American deejay "The Count" is "possibly the most famous broadcaster ever," which wasnt borne by the actual plot. The trailer and commercials also displayed prominent text that stated "inspired by a true story," which was not claimed by either the production or writing staff. 

==Reception==
  
The film received mixed reviews. Rotten Tomatoes gave it a score of 60% based on 156 reviews.    
The Daily Telegraph credited the film with "some magical moments," but called it "muddled" and criticised its length.   Time Out was also critical of the length and said the film was "disappointing."   The Hollywood Reporter ran the headline "Rock n roll movie Boat just barely stays afloat," declaring the film too long to sustain interest.  
Total Film also criticised the films length and comedic style.  
Andrew Neil, writing in The Observer, remarked that he was disappointed in the "contrived" storyline and the "unnecessarily perverted" history.   Channel 4 said the film was "touching," "heartfelt" and an "enjoyable journey" but questioned its coherence. 

  
The films British box office revenues in its first 12 weeks of release were £6.1 million, less than a quarter of its production cost. 

In USA, the film earned less than $US 3 million in its first weekend (in a mid-scale release of 882 screens as opposed to 3,404 screens for 2012 and 3,683 screens for A Christmas Carol) and suffered a 49.7% drop-off on its second weekend—earning only $US1.46 million. {{cite web 
| url = http://boxofficemojo.com/movies/?page=weekend&id=boatthatrocked.htm | title = Pirate Radio (2009) - Weekend Box Office Results | work = Box Office Mojo | publisher = Amazon.com }}  Pirate Radio took in only about   (approximately £5 million) in North America. 

== Historical setting==
 
 , c. 1974, which was the home of Radio Caroline South from 1964-1968]] British government that prefers to broadcast jazz.  According to director Richard Curtis, the film, though inspired by real British pirate radio of the 1960s, is a work of historical fiction and does not depict a specific radio station of the period. 

==Soundtrack==
 
* The soundtrack features songs from The Kinks, The Rolling Stones, The Turtles, Jimi Hendrix, Duffy (singer)|Duffy, Procol Harum, Box Tops, The Beach Boys, Dusty Springfield, The Seekers and The Who.
* The soundtrack features 32 songs on two discs. The film itself has a 60-song playlist. 

==Home media==
Scenes cut from the film but available in at least some of the films home media releases include: 
*a long scene of late-night sabotage aboard a competitors vessel; 
*The Counts homage to the Beatles, delivered in front of Abbey Road studios; 
*Gavin Kavanagh in a flashback, dancing in a South American bar to "Get Off of My Cloud";  Stay with Me" in its entirety.

{| class="wikitable"
|-
!Format
!Release date 
!Additional content
|-  Region 1: 13 April 2010 Region 2: 7 September 2009 Region 4: 12 August 2009 ||
* Deleted scenes, directors commentary
|- Region 1: 13 April 2010 Region 2: 7 September 2009  Region 4: 12 August 2009||
* Deleted scenes, directors commentary
|}

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 