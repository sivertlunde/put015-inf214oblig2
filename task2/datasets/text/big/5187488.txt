Bataan (film)
{{Infobox film

| name           = Bataan
| image          = Bataan.jpeg
| image_size     = 225px
| caption        = Original promotional poster
| director       = Tay Garnett
| producer       = Irving Starr 
| writer         = Robert Hardy Andrews Garrett Fort  (uncredited)  Dudley Nichols  (uncredited) 
| based on       = 
| screenplay     =  Robert Taylor Thomas Mitchell Lloyd Nolan
| music          = Bronislau Kaper, Eric Zeisl Sidney Wagner George White
| distributor    = Metro-Goldwyn-Mayer; United States Office of War Information
| released       =  
| runtime        = 114 min.
| language       = English
| budget = $958,000  .  gross = $3,117,000 
}}
 Robert Taylor, Thomas Mitchell Robert Walker.

==Historical background== invasion of the Philippines and lasted from January 1 to April 9, 1942. The American and Filipino forces retreated from Manila to the nearby mountainous Bataan Peninsula for a desperate last stand, hoping for a relief force. However, the Allies were being driven back in all areas of the Pacific theater and none could be sent. After three months of stubborn resistance, the starving and malaria-ridden defenders surrendered and were forced to undertake the infamous Bataan Death March.

==Plot== US Army is conducting a fighting retreat. A high bridge spans a ravine on the Bataan Peninsula. After the army and some civilians cross, a group of eleven hastily assembled soldiers from different units is assigned to blow it up and delay Japanese rebuilding efforts as long as possible. The rear guard is a mixed lot, including:
 Cavalry Captain Henry Lassiter (Lee Bowman) Robert Taylor) 31st Infantry
* Corporal Barney Todd (Lloyd Nolan), who claims to be a signalman. However, Dane suspects him of being Danny Burns, a soldier accused of murder who had escaped before the war while being guarded by then-military policeman Dane. Thomas Mitchell) of the Chemical Corps
* Private Felix Ramirez (Desi Arnaz), a Mexican American California National Guardsman
* Private Wesley Epps (Kenneth Lee Spencer), a black demolitions expert Medical Corps
* Private Francis X. Matowski (Barry Nelson), an engineer Philippine Scout 
* Private Sam Molloy (Tom Dugan), a cook Robert Walker), a naive young navy musician 

They dig in on a hillside, setting up heavy machine guns in sandbag fortifications and then blow up the bridge, but their commander, Captain Henry Lassiter, is killed by a sniper, leaving Dane in charge.
 Tommy gun before being killed; also Dane and Todd blow up the partially rebuilt bridge, creeping up undetected to throw several Mk 2 grenade|Mk. 2 fragmentation hand grenades.   

There is also tension between Dane and Todd, neither acknowledging their past outright despite both knowing it and knowing they recognize each other. 
 Army Air Corps pilot Lieutenant Steve Bentley (played by future Senator George Murphy) and his Filipino mechanic, Corporal Juan Katigbak (Roque Espiritu), work frantically to repair an airplane. They succeed, but Katigbak is killed (with a samurai sword one night) and Bentley is mortally wounded when he tries to lift off in his airplane. He has them load explosives aboard and deliberately crashes into the bridges foundation, destroying it for a third time.

The remaining soldiers repel a massive frontal assault, inflicting grievous losses on the attacking Japanese, ultimately fighting hand-to-hand combat|hand-to-hand with bayonets fixed on their M1903 Springfield rifles. Epps and Feingold are killed. Only Dane, Todd and a wounded Purckett are left.

Purckett is shot and Todd stabbed in the back by a Japanese soldier who had only feigned being dead. Before he dies, Todd admits to Dane that he is Burns. 

Now alone, Dane stoically digs his own marked grave beside those of his fallen comrades and waits in it. The Japanese troops crawl close to his position before opening fire and charging at Dane. Dane fires back, yelling, "Were still here.....well always be here, why dont you come and get us!" When his Tommy gun runs out of ammunition, he continues firing, the machine gun pointing to the audience, as the final credits roll.

The end story board states that the sacrifice of the defenders of Bataan helped slow the Japanese down, making possible Americas later victories in the Pacific War.

==Cast==
The following are mostly revealed in a scene when Sgt. Dane (Robert Taylor) inspects the group at the beginning of the film. 
 Robert Taylor 31st Infantry
* George Murphy as Lt. Steve Bentley, United States Army Air Corps Thomas Mitchell as Cpl. Jake Feingold, 4th Chemical Company, Chemical Corps 26th Cavalry 26th Cavalry Robert Walker as Musician 2nd Class Leonard Purckett, United States Navy
* Desi Arnaz as Pvt. Felix Ramirez, 194th Tank Battalion, California Army National Guard
* Barry Nelson as Pvt. Francis Xavier Matowski 12th Medical Battalion, Medical Corps (United States Army)
* Roque Espiritu as Cpl. Juan Katigbak, Philippine Army Air Corps Corps of Engineers
* Alex Havier as Pvt. Yankee Salazar, 4th Engineer Battalion, Philippine Scouts
* Tom Dugan as Cook Sam Malloy, Motor Transport Service

==Production==
The presence of a racially integrated fighting force prevented the films showing in the American South.
 The Lost Patrol, directed by John Ford, were reused in this production.

The film premièred in New York on 3 June 1943. 

==Reception== sadism in the American lynching ritual". By the 1940s publications were able to mass distribute photographs taken of hanged men, so there was a "rewriting of the respective relations of the black and the Asian to the white norm, as the film adjusted to a wartime context  ." {{cite journal
  | doi = 10.3200/JPFT.36.1.9-20
  | last = Locke
  | first = Brian
  | title = Strange Fruit: White, Black, and Asian in the World War II Combat Film "Bataan"
  | journal = Journal of Popular Film and Television
  | volume = 36
  | issue = 1
  | pages = 9–20
  | publisher = Heldref Publications
  | date = Spring 2008
  | issn = 0195-6051}} 
===Box Office===
The film was a hit - according to MGM records it earned $2,049,000 in the US and Canada and $1,068,000 overseas, resulting in a profit of $1,140,000.  Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 362 

==See also==
* Back to Bataan, a 1945 RKO film directed by Edward Dmytryk and starring John Wayne which covers the same setting and theme.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 