Snowflake, the White Gorilla
{{Infobox film
| name = Snowflake, the White Gorilla
| image = Snowflake, the White Gorilla.jpg
| caption = 
| director = Andrés G. Schaer
| producer = 
| writer = Amèlia Mora Albert Val
| starring = Kai Stroink Núria Trifold Claudia Abate Manel Fuentes Pere Ponce
| music = 
| cinematography =
| editing =
| studio = 
| distributor = Lionsgate Home Entertainment
| released = November 26, 2011 (Spain) September 2013 (USA)
| runtime = 86 minutes
| country = Spain
| language = English
| budget = 
| gross  =
}}
Snowflake, the White Gorilla is a 2011 Spanish live action film directed by Andrés G. Schaer. The film depicts the fictional childhood of the white gorilla Snowflake (gorilla)|Snowflake.

The English dub of the film stars David Spade, Ariana Grande, Jennette McCurdy, Nathan Kress, Dallas Lovato, Keith David, and Christopher Lloyd.

==Plot==
  Snowflake is witch Bruhmilda at the circus who can help her become a "normal" gorilla. Will Snowflake find the cure shes looking for or realize that theres something more important than fitting in-being true to yourself?

==Cast==
* Kai Stroink as Snowflake
* Manel Fuentes  as Jenga
* Claudia Abate as Wendy
* Constantino Romero as Anvil
* Pere Ponce as Dr. Archibald Pepper
* Elsa Pataky as Bruhmilda the Witch

===English dub cast=== Snowflake
* David Spade as Jenga
* Jennette McCurdy as Petunia
* Nathan Kress as Elvis
* Dallas Lovato as Wendy
** Eva Belle as Young Wendy
* Keith David as Anvil
* Christopher Lloyd as Dr. Archibald Pepper
* Diane Michelle as Bruhmilda the Witch

==External links==
*   at Internet Movie Database

 
 
 