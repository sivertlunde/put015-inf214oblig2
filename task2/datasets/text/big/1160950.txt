Buddy and Towser
 
{{Infobox Hollywood cartoon
| series = Looney Tunes (Buddy (Looney Tunes)|Buddy)
| image =   
| caption =   Isadore Freleng
| story_artist =  Jack King, Bob McKimson Jack Carr (uncredited) Norman Spencer
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros. The Vitaphone Corporation
| release_date = February 24, 1934 (USA)
| color_process = Black-and-white
| runtime = 7 minutes
| movie_language = English
| preceded_by = Buddy the Gob (1934)
| followed_by = Buddys Garage (1934)
}} animated short film released on February 24, 1934,  by Warner Bros.. It was directed by Friz Freleng; the musical score is by Norman Spencer. It is a Looney Tunes cartoon, featuring Buddy (Looney Tunes)|Buddy, the second star of the series.

==Summary==
Buddy enlists his dog, Towser, to guard his award-winning chickens. A fox penetrates Buddys property as Towser & Buddy sleep, but the chickens, initially, are able to repel the fox by throwing their eggs at it. In its escape, the fox awakens Towser, who proceeds to bark, awakening his owner, and chase the fox; Towser is joined in the chase by Buddy, who now wields a shotgun (that he is humorously unable to handle.) Eventually, Buddy and Towser run up a snowy hill after the fox, which then crashes into a tree, tumbles backwards, and finds itself trapped in an increasingly large snowball, which soon envelops Towser & Buddy. At the foot of the hill, the snowball breaks apart on impact with a shed, leaving Buddy, Towser, and the fox momentarily dazed; coming to their senses, Buddy and Towser each take a plank of wood and aim to hit the fox, which sits between them; but the fox comes to and scurries away, leaving Buddy to accidentally whack Towser, and vice versa, as the cartoon ends.

==References==
 

==External links==
*  

 

 
 
 
 
 


 