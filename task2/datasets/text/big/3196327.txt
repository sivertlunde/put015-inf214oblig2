Goya's Ghosts
{{Infobox film 
|  name           = Goyas Ghosts 
|  image          = Goyas ghosts.jpg 
|  caption        = Promotional poster for Goyas Ghosts  
|  director       = Miloš Forman 
| producer       = Saul Zaentz
|  writer         = Miloš Forman Jean-Claude Carrière
|  starring       = Natalie Portman Javier Bardem Stellan Skarsgård Randy Quaid Unax Ugalde 
|  music          = Varhan Orchestrovič Bauer
|  cinematography = Javier Aguirresarobe
|  editing        = Adam Boome
|  studio         = The Saul Zaentz Company Kanzaman Antena 3 Televisión Xuxa Producciones Entertainment Film (UK) Samuel Goldwyn Films (US)
|  released       =   
|  runtime        = 114 minutes
|  country        = Spain United States
|  language       = English
|  budget         =
|  gross            = $9,448,082 
}} American film, directed by Miloš Forman, and written by him and Jean-Claude Carrière. The film stars Natalie Portman, Javier Bardem and Stellan Skarsgård, and was filmed on location in Spain during late 2005. The film was written, produced, and performed in English although it is a Spanish production.

Although the historical setting of the film is authentic, the story about Goya trying to defend a model is fictional, as are the characters Brother Lorenzo and the Bilbatúa family.

==Plot==
 
In 1792, Spain reels amid the turmoil and upheaval of the French Revolution. Francisco Goya (Stellan Skarsgård) is a renowned painter, who, among others, does portraits for the royal family as the Official Court Painter to the King and Queen.

The Spanish Inquisition is disturbed by part of Goyas work. Brother Lorenzo Casamares (Javier Bardem) is hiring Goya to paint a portrait and defends him, saying that his works are not evil, they just show evil. He recommends the Church step up the fight against Anti-Catholicism|anti-Catholic practices. He requests and is put in charge of intensifying the Inquisition. 
 Jewish rituals, because she did not eat pork the other evening. She is stripped naked and tortured by strappado ("put to The Question"), confesses, and is imprisoned. Naturally, the Inquisitions archives had already revealed that one of her fathers ancestors had converted from Judaism to Christianity in 1624 upon arrival in Spain from Amsterdam.

Tomás begs Goya for help, who in turn asks Lorenzo to find out about Inéss situation.  Lorenzo visits Inés in prison telling her that he is going to help her and will pass a message to her family if she wishes. He offers to pray with Inés, but is clearly struggling with his desire to have sex with her as she prays in the nude with him at his request. At a dinner in Tomáss home, where he and Goya are guests, Lorenzo defends "The Question" (and brings up the subject of the fathers Jewish ancestor): he argues that if the accused is really innocent, God will give him or her the strength to deny guilt, so a person who confesses must be guilty. Tomás does not agree: he argues that people will confess to anything under torture, and Goya agrees. To prove this Tomás draws up a statement which says that Lorenzo confesses to being a monkey, and, with the help of his sons, does not let Lorenzo leave unless he agrees to sign it. Goya pleads for Lorenzo without success, and is escorted away and pushed out of the home. Tomás, his sons and servants torture with a makeshift strappado (ropes strung on the chandelier), causing Lorenzo to break down and sign. Tomás promises to destroy the document after Inés is released. He gives Lorenzo a large amount of gold for the Church, hoping it will persuade the Holy Office to consider leniency.
 Charles IV (Randy Quaid) who is highly amused at seeing it, and promises to assist Inés. Lorenzo is now an embarrassment to the Spanish Church and they come to arrest him. He flees. Lorenzos portrait is confiscated by the Church, and is set on fire in public, to burn him in effigy.

Fifteen years pass, and Goya is at the height of his creativity, but has grown deaf. The French army under Napoleon invades Spain, abolishes the Inquisition and sets the prisoners free. Lorenzo had fled to France, where he was introduced to the ideas of the French Revolution and became a fanatical adherent of them.  He is now Napoleons chief special prosecutor against his former Spanish colleagues in the Inquisition. (This twist in Lorenzos allegiance might have been inspired by the career of Juan Antonio Llorente.) A French court presides over the show trial, conviction and sentencing to death of the Inquisitor General. Inés, who was said to be tried, has actually been left to languish in the dungeons until now. She had given birth to a daughter in prison, who was taken away from her immediately after birth.  Upon visiting her old home and finding her family killed, Inés turns to Goya for help in finding her child. Lorenzo is the father, which is embarrassing for him, and he sends Inés, whose sanity has suffered in prison, to an insane asylum. Lorenzo questions the condemned Inquisitor General, sparing him for information about what would happen to a child born in the Inquisition prison. The Inquisitor General tells him the child would be placed in an orphanage. Lorenzo tracks it down, and he learns from the nuns who run it that his daughters name is Alicia. She had run away several years before.

In Garden Park, Goya finds a prostitute named Alicia (also played by Natalie Portman) who looks identical to Inés. He goes to Lorenzo asking for Inés in order to reunite her with her daughter. Lorenzo takes the initiative and secretly goes to see Alicia, offering to pay for her passage to America if she will agree to leave Spain. However she refuses the offer and jumps out of his carriage declaring him insane. In the meantime, Goya travels out into the countryside, where Inés has been kept in an asylum. Goya pays the director a bribe to release her, and then tries to bring Inés to see Alicia. Unfortunately, as Goya tries to persuade Alicia to see her mother, a group of soldiers, under orders from Lorenzo, burst into the tavern and forcibly arrest all the prostitutes. Goya discovers Lorenzos intention  to sell the women off to America where they will be treated as slaves. Inés, waiting outside to meet Alicia, finds the abandoned baby Alicia left when she was seized.  In her delusion Inés thinks the baby, who might be her granddaughter, to be her own lost daughter.

The British invade Spain from Portugal, defeating the French troops with the help of the Spanish population. The wagons in which the prostitutes are being transported are abandoned by the French cavalry guarding them when the British attack, with Alicia catching the eye of a British officer. Lorenzo is arrested as he is fleeing. The Spanish reinstate the Inquisition, which now tries and sentences Lorenzo to death, the Inquisitor General condemning him in much the same words as in Lorenzos speech at his trial. They are willing to spare him if he repents, and they urge Lorenzo to do so until the last moment at the site of the execution in the city center, to which he is led in the auto da fe wearing a sanbenito with painted flames indicating he is sentenced to die. On the scaffold, Lorenzo spots his daughter Alicia, on the arm of the British officer, scoffing at him. He also notices Goya sitting at a distance sketching the entire ordeal. Inés is also present in the crowd and calls to Lorenzo enthusiastically to show him the baby she believes is their daughter. Refusing to repent, despite pleas from his former colleagues the monks, Lorenzo is garrotted. The film ends with a cart taking Lorenzos body away, escorted by Inés, holding the hand of the dead Lorenzo, children prancing about singing songs, and with Goya following closely behind her.

==Cast==
* Natalie Portman as Inés Bilbatúa and Alicia
* Javier Bardem as Lorenzo Casamares
* Stellan Skarsgård as Francisco Goya
* Randy Quaid as King Charles IV of Spain
* José Luis Gómez as Tomás Bilbatúa
* Unax Ugalde as Ángel Bilbatúa
* Michael Lonsdale as Father Gregorio
* Julian Wadham as Joseph Bonaparte

==Reception==
===Box office===
The film has grossed $2,198,929 in Spain and $1,199,024 in Italy.   In the United States, Goyas Ghosts has grossed $1,000,626, with a worldwide total $9,448,082. 

===Critical reception===
Goyas Ghosts received poor reviews from critics. The review aggregator Rotten Tomatoes reported that 30% of critics gave the film positive reviews, based on 87 reviews &mdash; with the consensus that "ornate costumes and a talented cast cant make up for Ghosts glacial pace and confused plot."  Metacritic reported the film had an average score of 52 out of 100, based on 25 reviews &mdash; indicating average reviews. 

==References==
 

==External links==
*    
*  
* John Walker. (2009).  . artdesigncafe.
*  
*  
*  
*  
*  
*   By N.P. Thompson
*   at natalieportman.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 