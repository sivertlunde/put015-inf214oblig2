Doctor Syn (film)
 
 
{{Infobox film
| name           = Doctor Syn
| image          = Drsyn.jpg
| caption        = 
| director       = Roy William Neill Edward Black
| writer         = Roger Burford Michael Hogan Russell Thorndike John Loder
| cinematography = Jack E. Cox
| music          = Hugh Bath Jack Beaver
| editing        = R.E. Dearing
| studio         = Gainsborough Pictures
| released       = 25 August 1937 (UK) 14 November 1937 (U.S.)
| runtime        = 78 minutes
| country        = United Kingdom
| language       = English
}}
 drama historical directed by produced by Gainsborough Pictures. The film is based on the Doctor Syn novels of Russell Thorndike, set in 18th-century Kent.  The character of Syn and the events at the films climax were both softened considerably in comparison to Thorndikes original story.

==Plot==
Led by Captain Collyer (Roy Emerton), a detachment of Royal Navy tax and revenue officers arrive in the village of Dymchurch on Romney Marsh.  The area is notorious for liquor-smuggling and they are on the trail of the culprits.  They find a village of apparently honest, pious and simple folk, looked after benevolently by their philanthropic vicar Doctor Syn (Arliss).  However Syn is in fact the leader of the smugglers of the parish, using his cover as a man of the cloth to run a profitable ring whose dividends are used to better the lives of the local community.  Collyer gradually comes to suspect what is going on, and a series of chases and confrontations takes place across the marshes with Syn and the smugglers always managing narrowly to outwit their pursuers.  Collyer finally discovers that Syn is in fact none other than the notorious pirate Captain Clegg, thought to have been executed many years earlier.  Still one step ahead, Syn destroys all incriminating evidence and he and his men make their escape.

==Cast==
* George Arliss as Doctor Syn
* Margaret Lockwood as Imogene Clegg John Loder as Denis Cobtree
* Roy Emerton as Captain Howard Collyer
* Graham Moffatt as Jerry Jerk George Merritt as Mipps
* Athole Stewart as Squire Cobtree
* Frederick Burtwell as Rash
* Wilson Coleman as Dr. Pepper
* Wally Patch as Bosun
* Muriel George as Mrs. Waggetts
* Meinhart Maur as Mulatto
* Ronald Shiner

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 