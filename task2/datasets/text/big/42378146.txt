Majestic (film)
 
{{Infobox film name           = Majestic image          = image_size     = caption        = director       = P. N. Satya producer       = Ba Ma Harish M G Ramamurthy writer         = P. N. Satya narrator       = starring  Darshan Rekha Jai Jagadish Harish Roy music          = Sadhu Kokila cinematography = Anaji Nagaraj M R Seenu editing        = Nagendra Urs studio         = Ullas Enterprises released       =   runtime        = 140 minutes country        = India language       = Kannada budget         =
}} Kannada action Darshan in a lead role for the first time, along with Rekha and Jai Jagadish in other prominent roles.  The film was produced by Ullas Enterprises studio. Sadhu Kokila scored the music for the film and the film was released on 8 February 2002. The film was remade in Telugu as Charminar. 

==Cast== Darshan as Daasa
* Rekha as Kiran
* Jai Jagadish
* Vanitha Vasu
* Harish Roy
* Bank Janardhan
* Bharath Bhagavathar
* Jyothi
* Ramesh Pandit

==Soundtrack==
The music of the film was composed by Sadhu Kokila and lyrics written by K. Kalyan.

{{Infobox album  
| Name        = Majestic
| Type        = Soundtrack
| Artist      = Sadhu Kokila
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Ashwini Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Muddu Manase
| lyrics1  	= V. Nagendra Prasad
| extra1        = Unnikrishnan
| length1       = 
| title2        = Thangali Mele
| lyrics2 	 = V. Nagendra Prasad
| extra2        = Rajesh Krishnan, Anuradha Sriram
| length2       = 
| title3        = Naane Naane
| lyrics3       = V. Nagendra Prasad
| extra3  	= Rajesh Krishnan
| length3       = 
| title4        = Majestic Majestic
| extra4        = Hemanth
| lyrics4 	 = V. Nagendra Prasad
| length4       = 
| title5        = Dove Hodiyoke
| extra5        = L. N. Shastry
| lyrics5       = V. Nagendra Prasad
| length5       = 
}}

==References==
 

==External source==
*  
*  

 
 
 
 
 
 
 


 