Ballad of the Masterthief Ole Hoiland
{{Infobox film
| name           = Ballad of the Masterthief Ole Hoiland
| image          = 
| image size     =
| caption        = 
| director       = Knut Andersen
| producer       = 
| writer         = Knut Andersen   Harald Tusberg
| narrator       = Sverre Hansen   Aud Schønemann   Anne Marit Jacobsen   Ola Isene   Kjersti Døvigen   Harald Heide-Steen Jr.   Eva von Hanno   Leif Juster   Sølvi Wang   Lillian Lydersen
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1970
| runtime        = 104 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama film directed by Knut Andersen, and starring a broad cast of notable Norwegian actors, headed by Per Jansen as Ole Høiland. Ole Høiland was an actual Norwegian Robin Hood-figure in the early 19th century. He steals from the rich and gives to the poor, enjoying numerous affairs with attractive women along the way. The story culminates in the ambitious burglary of Norges Bank, Norways central bank.

==External links==
*  
*   at Filmweb.no (Norwegian)

 
 
 
 