Achani (film)
{{Infobox film 
| name           = Achani
| image          = Poster_of_the_film,_Achani.jpg
| caption        =
| director       = A. Vincent
| producer       = K. Ravindran Nair
| writer         = Karaikudi Narayanan
| screenplay     = Thoppil Bhasi
| starring       = Prem Nazir Nanditha Bose Adoor Bhasi Sankaradi Kottarakkara Sreedharan Nair
| lyrics         = P. Bhaskaran
| music          = Devarajan
| cinematography = Venkit Prakash
| editing        = G Venkitaraman
| studio         = Satya Studios
| distributor    = General Pictures
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by A. Vincent and produced by K. Ravindran Nair. The film stars Prem Nazir, Nanditha Bose, Adoor Bhasi and Kottarakkara Sreedharan Nair in lead roles. The film had lyrics by P. Bhaskaran and musical score by Devarajan. It tells the story of a tailor and the trials and tribulations of his family.    The film is reported to have been a commercial success and the proceeds from the film is known to have been donated by the producer for the construction of a Public Library in Kollam.    

==Cast==
 
* Prem Nazir as Vasu
* Nanditha Bose as Seetha
* Adoor Bhasi as Kaimal
* Sankaradi as Banker Menon
* Kottarakkara Sreedharan Nair as Raghavan Muthalali
* Bahadoor as Appu Vincent as Gopi Sudhir as Babu Sujatha as Uma Meena as Srimathi Raghavan
* Philomina as Mariammam Sreelatha as Kalyani
* K. J. Yesudas (Guest appearance as himself)
 

==Soundtrack==
The songs of the movie were reported to have been hits during the time, especially Ente Swapnathin, sung playback and on screen by the renowned singer, K. J. Yesudas.       

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singer(s) ||Lyrics || Music
|-
| 1 || Ente Swapnathin || K. J. Yesudas || P. Bhaskaran || Devarajan
|-
| 2 || Mallikabaanan thante || P Jayachandran P. Madhuri || P. Bhaskaran || Devarajan
|-
| 3 || Manivilakkananju || P. Susheela || P. Bhaskaran || Devarajan
|-
| 4 || Neela Neela Samudra || P. Madhuri || P. Bhaskaran || Devarajan 
|-
| 5 || Samayamaam Nadi || P. Susheela || P. Bhaskaran || Devarajan
|}

==References==
 

==External links==
*  

 
 
 

 