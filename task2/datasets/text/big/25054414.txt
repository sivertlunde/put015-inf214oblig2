Mahatma (film)
{{Infobox film
| name           = Mahatma
| image          = Mahatma Poster HD.jpg
| caption        = 
| director       = Krishna Vamsi
| producer       = C R Manohar
| starring       = Meka Srikanth|Srikanth, Bhavana (actress)|Bhavana, RamJagan, Jaya Prakash Reddy, Uttej.
| music          = Vijay Antony
| cinematography = Sharat
| editing        = Gautham Raju
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Telugu
| budget         =  12 crores
}} Srikanth in Tamil as Puthiya Thalapathy. It is dubbed in hindi as Ek Aur Mahanayak.

==Plot==
Dasu (Srikanth) is a rowdy in a basti in Hyderabad. He makes a living out of "settlement of petty issues". A young lawyer Krishnaveni (Bhavana) gets him bail in a petty case and their acquaintance soon develop into romance after a series of events. On the other hand, a politician cum business woman (Jyothy) plans to set up SEZ in that basti that move is protested by dwellers headed by a genuine leader (Sekhar). Meanwhile, a local politician cum rowdy leader Dada (Jayaprakash Reddy) also protests against businesswoman and seeks Rs 200 Crores from if she wants to set up SEZ there. Dasu initially believes Dada as good politician and works for him. When Dada tries to take advantage of Dasu, he realizes his fault and plans to contest against him on a newly floated Mahatma party. How Krishnavani and local theatre artiste bring change in Dasu and make him realize the importance of Mahatma Gandhis ideology is core point of the movie.

== Cast == Srikanth as Das Bhavana as Krishnaveni
* Charmee Kaur
* Navneet Kaur
* Yana Gupta
* Jaya Prakash Reddy as Dada
* Uttej
* RamJagan Jyothy
* Paruchuri Gopala Krishna
* Duvvasi Mohan
* Brahmanandam
* Sampoornesh BaBu

== Songs ==
* "Dailamo Dailamo" by Balaji, Sangeetha and Megha
* "Em Jaruguthondi" by Karthik and Sangeetha
* "Indiramma" by SP. Balasubrahmaniam
* "Jajjanakka" by Vijay Antony
* "Neelapoori" by Kasara Shyam
* "Thala Ethi" by SP. Balasubrahmaniam
* "Kurra Kurra" by Surchith, Vijay Antony and Vinay

==See also==
*List of artistic depictions of Mohandas Karamchand Gandhi

==References==
 

== External links ==

 
 

 
 
 
 
 

 