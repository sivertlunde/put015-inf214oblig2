The Woman with No Name
{{Infobox film
| name           = The Woman with No Name
| image          = "The_Woman_With_No_Name"_(1950).jpg
| image_size     = 
| caption        = 
| director       = Ladislao Vajda
| producer       = John Stafford Guy Morgan   Ladislao Vajda
| based on       = novel "Happy Now I Go" by Theresa Charles 
| narrator       = 
| starring       = Phyllis Calvert   Edward Underdown   Helen Cherry   Richard Burton
| music          = Allan Gray
| cinematography = Otto Heller Richard Best
| studio         =  Independent Film Producers
| distributor    = Associated British-Pathé  (UK)
| released       = October 1950  (UK)
| runtime        = 84 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross = £113,268 (UK) 
| preceded_by    = 
| followed_by    = 
}}
 British drama James Hayter.  In the United States it was released as Her Panelled Door. 

==Plot==
Yvonne Winter is an amnesiac, a victim of the wartime bombing of the London hotel where shes staying. At a country hospital she meets the pilot, Lake Chamerd, who saved her life. They fall in love and plan to marry, but hes killed on active duty. Yvonnes real husband hires detectives to find her, and shes brought home and starts to piece together her past. But not everything she finds there brings her happiness.

==Cast==
* Phyllis Calvert – Yvonne Winter
* Edward Underdown – Lake Winter
* Helen Cherry – Sybil
* Richard Burton – Lake Charmerd Anthony Nicholls – Doctor James Hayter – Captain Bradshawe
* Betty Ann Davies – Beatrice
* Amy Veness – Sophie
* Andrew Osborn – Paul Hammond
* Patrick Troughton – Colin
* Leslie Phillips – 1st sapper officer
* Terence Alexander – 2nd sapper officer Richard Pearson – Tony

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 