Wings of Danger
 
 
{{Infobox film
| name           = Wings of Danger
| image          = Wings of Danger poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Terence Fisher
| producer       = Anthony Hinds
| writer         = Trevor Dudley Smith (novel)   Packham Webb (novel)   John Gilling 
| narrator       = 
| starring       = Zachary Scott   Robert Beatty   Naomi Chance   Kay Kendall
| music          = Malcolm Arnold
| cinematography = Walter J. Harvey
| editing        = James Needs
| studio         = Hammer Film Productions
| distributor    = Exclusive Films (UK)   Lippert Pictures (US)
| released       = 1 April 1952
| runtime        = 73 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British crime film directed by Terence Fisher and starring Zachary Scott, Robert Beatty and Kay Kendall. 

==Production== Hammer Films and shot at the Riverside Studios in Hammersmith.

==Cast==
* Zachary Scott as Richard Van Ness
* Robert Beatty as Nick Talbot
* Naomi Chance as Avril Talbot
* Kay Kendall as Alexia LaRoche
* Colin Tapley as Inspector Maxwell Arthur Lane as Boyd Spencer  Harold Lang as Snell, the blackmailer 
* Diane Cilento as Jeannette  Jack Allen as Tniscott  Douglas Muir as Doctor Wilner   Ian Fleming as Talbot Larry Taylor as OGorman, henchman 
* Darcy Conyers as Signals Officer 
* Sheila Raynor as Nurse 
* Courtney Hope as Mrs Clarence, hotel tenant  
* Anthony T. Miles as Sam, Desk Clerk  
* James Steele as First Flying Officer 
* Russ Allen as Second Flying Officer 
* June Ashley as Blonde in Sportscar 
* June Mitchell as Blonde in Sportscar 
* Natasha Sokolova as Blonde in Sportscar

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 