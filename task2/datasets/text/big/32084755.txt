Scary Movie 5
 
{{Infobox film
| name           = Scary Movie 5
| image          = ScaryMovie5.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Malcolm D. Lee David Zucker Phil Dornfeld
| writer         = Pat Proft David Zucker
| starring       = Ashley Tisdale Simon Rex Erica Ash Molly Shannon Heather Locklear J. P. Manoux Jerry OConnell Charlie Sheen Lindsay Lohan
| music          = James L. Venable
| cinematography = Steven Douglas Smith
| editing        = Sam Seig Brad Grey Pictures
| distributor    = Dimension Films The Weinstein Company
| released       =  
| runtime        = 86 minutes    
| country        = United States
| language       = English
| budget         = $20 million   
| gross          = $78.3 million 
}} Scary Movie David Zucker.    It was released on April 12, 2013. 
 premiered on April 11 at the Hollywood’s ArcLight Cinerama Dome.  The film parodies various horror films and other popular culture.

==Plot==
Charlie Sheen and Lindsay Lohan get together to make a sex tape with over 20 cameras beside Sheens bed. The time-lapsed tape fast forwards through the two doing all sorts of bedroom antics, including gymnastics, riding a horse, and having clowns jump in under the sheets. Sheen is pulled into the air by a paranormal force and thrown against walls, shelves, and doors until he lands on the bed again. Lohan is frightened so she decides to go home when she flies into the air as well; she becomes possessed and throws him into the camera and kills him. The text explains that Sheens body was found that day but he didnt stop partying until days later, and that his three kids were found missing, Lohan was arrested, again, and a reward was put out for the missing children.
 Humboldt County cannabis plants cabin in Swan Queen.
 paranormal activity in their new home makes them investigate further. They eventually learn from the children that the attacks on their home are by "Mama (2013 film)|Mama", the mother of the children, who is under a curse and is trying to get them back so she can sacrifice both herself and the children. Maria, the couples Hispanic live-in maid, is frightened and keeps experimenting with various rituals, Catholic and otherwise, to ward off the evil spirits in the house.  During the day, Dan is frustrated with the modest progress of his test subjects at a primate intelligence research facility; ironically, Dan is not bright enough to realize that one of the chimpanzees, List of Planet of the Apes characters#Caesar|Caesar, is now actually much smarter than he is.
 Blaine Fulda, Dom Kolb, Book of the Dead. However, Jody and Kendra fail to see what the book is capable of, oblivious to four friends continuously becoming possessed and reviving in the cabin. They wreak havoc when either of the two read the two passages of the book; one that unleashes demons, Klaatu barada nikto|"gort klaatu barada nikto", and the other that revives them from possession. When "Mama" takes the children to a cliff to sacrifice them, Jody fails to lift the curse with the book, but does manage to knock the evil spirit into JaMarcus and DAndres pool containing a live shark, which then devours her.
 apes will one day take over the world.

In a post-credits scene, Sheen wakes up, with Dom Kolb sitting beside him, from a dream extraction, i.e. the whole movie was a dream. After Kolb informs that Sheen will be sleeping with Lohan, a car crashes into the room, killing Sheen. Revealing Lohan as the driver, she gets out of the car, says to Kolb "You were driving" and throws him the keys, blaming the accident on him.

==Cast==
 
 
* Ashley Tisdale as Jody Sanders 
* Simon Rex as Dan Sanders   
* Erica Ash as Kendra Brooks Natalie
* Terry Crews as Martin
* Jasmine Guy as Mrs. Brooks
* Darrell Hammond as Dr. Hall Mia
* Heather Locklear as Barbara 
* J. P. Manoux as Pierre
* Mac Miller as DAndre Shad Moss Eric  Christian Grey David
* Molly Shannon as Heather Darcy 
* Snoop Dogg as JaMarcus
* Mike Tyson as himself  Usher as Ira (The Janitor) Kate Walsh as Mal
* Katt Williams as Blaine Fulda
* Charlie Sheen as himself 
* Lindsay Lohan as herself  Dom Kolb
* Lil Duval as Brooks
* Lewis Thompson as Mabel "Madea" Simmons
* Audrina Patridge and Kendra Wilkinson (deleted scenes) as Christian Greys slaves Sheree Whitfield as herself   
* Big Ang as herself 
 

==Parodies== Paranormal Activity, Black Swan, Evil Dead The Help.  The film also parodies the best-selling novel Fifty Shades of Grey,  and Tyler Perrys character Madea (Mabel Simmons)|Madea. 

==Production==

===Development=== David Zucker.  Anna Faris, who starred in the previous films in the franchise, confirmed that she would not return for the fifth film.  Tisdales involvement in the film was confirmed in June 2012.  

Lohan and Sheen joined the cast in August 2012.   Terry Crews joined the cast on August 14, 2012. 
 Filming began in September 2012.   The first promotional image of the film, featuring Lohan and Sheen in the very first scene of the movie, was released on September 20, 2012.  

The only actors from any of the previous installments to appear in this film are Sheen,  Rex, Hammond, and Shannon. They do not portray their original characters, Sheen stars as himself, Rex plays Dan, Shannon plays Heather, and Hammond plays a doctor.
 Georgia in the fall of 2012, with additional filming January and February 2013 at Sunset Gower Studios in Los Angeles. David Zucker reportedly handled additional filming and reshoots while Malcolm D. Lee was starting production for The Best Man Holiday.

==Music==

===Soundtrack===
{{Infobox album  
| Name        = Scary Movie 5: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = April 23, 2013
| Recorded    = 2013
| Genre       = Film soundtrack
| Length      = 44:34
| Label       = Lakeshore Records
| Producer    =
| Reviews     =
}}
Scary Movie 5: Original Motion Picture Soundtrack is the soundtrack of the film and was released on April 23, 2013.  

{{Track listing
| extra_column    = Performer(s)
| total_length    = 44:34
| title1          = Werk Me
| extra1          = Hyper Crush
| length1         = 3:50
| title2          = Way Out Willie
| extra2          = Dug
| length2         = 2:19
| title3          = I Want Her
| extra3          = Blind Truth & Georgia Harris
| length3         = 3:04
| title4          = How You Girlz Git Down
| extra4          = Marcus Latief Scott
| length4         = 3:40
| title5          = Everybody Feel It
| extra5          = Hit Feeling Productions
| length5         = 3:22
| title6          = Electricity
| extra6          = John Costello
| length6         = 1:47
| title7          = Lakme – Flower Duet
| extra7          = Apollo Symphony Orchestra
| length7         = 3:28
| title8          = Thunder
| extra8          = The League
| length8         = 4:15
| title9          = Pimp Cup
| extra9          = Tarik NuClothes
| length9         = 2:12
| title10         = Right There
| extra10         = Bellringer
| length10        = 3:35
| title11         = Livin Loud
| extra11         = D.J. FiFi
| length11        = 3:33
| title12         = Swan Lake (Waltz of Flowers)
| extra12         = Yuri Botnari
| length12        = 2:59
| title13         = Somewhere in This World
| extra13         = Pete Peterkin
| length13        = 3:15
| title14         = Ready for War
| extra14         = MicLordz & Sauce Funky
| length14        = 3:14
}}

===Score===
{{Infobox album
| Name       = Scary Movie 5: Original Motion Picture Score
| Type       = film
| Artist     = James L. Venable
| Cover      = 
| Released   = May 14, 2013
| Recorded   = 2013 Score
| Length     =  
| Label      = Lakeshore Records
| Producer   = 
| Chronology = James L. Venable film scores
| Last album =   (2010)
| This album = Scary Movie 5 (2013)
| Next album = 
}}
Scary Movie 5: Original Motion Picture Score is the soundtrack of the film scored by James L. Venable it was released on May 14, 2013. 

*All songs written and composed by James L. Venable. 
{{tracklist
| total_length = 
| title1 = Attacked, Possessed and Out to the Woods
| length1 = 1:31
| title2 = Finding the Cabin and Driving Home
| length2 = 0:55
| title3 = Primates Attack and Lab Intro
| length3 = 2:14
| title4 = Meet the Kids, Who Is She?
| length4 = 2:13
| title5 = To the Ballet
| length5 = 1:30
| title6 = A Scare, a Pan and Some Hot Breath
| length6 = 1:32
| title7 = Cameras Up!
| length7 = 1:14
| title8 = Don’t Go in the Closet…or the Bathroom!
| length8 = 2:16
| title9 = Perhaps We Should Try a Psychic
| length9 = 3:00
| title10 = Dreams within Dreams
| length10 = 1:23
| title11 = The Red Room
| length11 = 0:55
| title12 = The Video Chat
| length12 = 0:33
| title13 = Nighttime in the House
| length13 = 0:42
| title14 = The Evil Book
| length14 = 1:36
| title15 = Caesar and the Baby
| length15 = 0:47
| title16 = A Vision, an Empty House and Great Pie
| length16 = 1:17
| title17 = Don’t Mess With the Hair!
| length17 = 1:40
| title18 = The Grand Finale
| length18 = 2:20
| title19 = Hugs on a Cliff, You’re a Swan!
| length19 = 0:51
| title20 = The Final Triumph
| length20 = 0:36
}}

==Reception==

===Box office===
Scary Movie 5 grossed $32,015,787 in North America, and $46,362,957 in other countries, for a worldwide total of $78,378,744. 

In North America, the film opened to #2 in its first weekend with a disappointing $14,157,367, behind 42 (film)|42, making it the lowest-grossing opening weekend for a film in the Scary Movie franchise. 

It was expected to take in about half as much as its predecessors, around $17 million in its opening weekend.    The film held up reasonably well in its second weekend, slipping two spots to #4 with an estimated $6,296,000.  In its third weekend, the film dipped 43.8% to #7 earning an estimated $3,457,000. The film held a spot in the top ten for the fourth weekend in a row, falling to #9 with a gross of $1,441,360. Scary Movie 5 fell to #13 in its fifth weekend earning $675,942 and slid to #15 in its sixth with $420,253. 

===Critical response===
Scary Movie 5 was not screened for critics in advance,  and was panned by most critics, making it the worst reviewed film in the franchise. On  , which assigns a normalized rating out of 100 top reviews from mainstream critics, calculated a score of 11, signifying "overwhelming dislike". 

  with you. That way you can watch a better movie on it."   
 The New York Daily News gave the film one star saying, "Like so much of this whole series – hatched in 2000 by the Wayans brothers and intermittently directed by Airplane! veteran David Zucker, though newcomer Malcolm D. Lee takes over here – the mere mention of a familiar pop culture figure or title is supposed to be hilarious. It often isnt, and in fact the constant name-dropping and gross-out humor gets tiresome (in a movie thats at least 10 minutes too long). Luckily, folks like Snoop and good sports like Sheen and, yes, Lohan, break up the monotony. Until, like an undead beastie, the boredom and dumb jokes come roaring back." 

Rafer Guzman of Newsday felt that "Even the talented people – comedian Katt Williams as a fake psychic, high-energy actor Jerry OConnell in a send-up of the Fifty Shades of Grey books – get chewed up and spit out by this relentless antilaugh machine. Scary Movie 5 doesnt even have the imagination for a worthwhile gross-out joke. When the best you can offer is a poopy toothbrush, its time to pack it in."  Darren Franich of Entertainment Weekly said "Hitting theaters seven years after the last Scary Movie, the new film doesnt even feature the ameliorating presence of Anna Faris, who gave the earlier films a certain spoofy grace. In her place is High School Musical refugee Ashley Tisdale, her face frozen in an eyeroll of mild irritation. Who can blame her? The film hopscotches between too-late riffs on Rise of the Planet of the Apes, Inception, Insidious, and Black Swan. At a running time of 86 minutes, its about as long as an episode of Saturday Night Live, except with less laughs and worse storytelling."    Frank Scheck of The Hollywood Reporter said "The filmmakers’ desperation is evident from the fact that a good chunk of the running time is devoted to spoofing the recent Jessica Chastain starrer Mama (2013 film)|Mama. While that film was indeed a sleeper hit, it hardly seems memorable enough to warrant such sustained treatment, and indeed the comic payoffs are nil." 

The film has earned three nominations at the 34th Golden Raspberry Awards including Worst Supporting Actress for Lindsay Lohan, Worst Screen Combo for Lohan and Charlie Sheen and Worst Prequel, Remake, Ripoff or Sequel. 

===Home media===
Scary Movie 5 was released on DVD and Blu-ray Disc|Blu-ray on August 20, 2013.  An unrated version was also released.

==See also==
 
* American films of 2013
* List of comedy films of the 2010s
* List of horror films of the 2010s
* Parody film

==References==
 

==External links==
 

*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 