Staying Together (film)
{{Infobox film
| name = Staying Together
| director = Lee Grant
| image	=	Staying Together FilmPoster.jpeg
| producer = Joseph Feury Milton Justice
| writer = Monte Merrick
| starring = Sean Astin Stockard Channing Melinda Dillon Levon Helm Dermot Mulroney Tim Quill Daphne Zuniga
| music = Miles Goodman
| cinematography = Dick Bush
| editing = Katherine Wenning
| distributor = Hemdale Film Corporation
| released = November 10, 1989
| runtime = 91 minutes
| country = United States
| language = English
| gross = $4,348,025
}}

Staying Together is a 1989 American comedy-drama film directed by Lee Grant and produced by Joseph Feury (Grants husband) and Milton Justice. The film stars Sean Astin, Stockard Channing, Melinda Dillon, Levon Helm (of The Band), Dermot Mulroney, Tim Quill, and Daphne Zuniga. Grants daughter, Dinah Manoff appears briefly making this the only film project (excluding TV Movies) to involve Grant, Feury and Manoff.

Channing and Manoff previously appeared together in Grease (film)|Grease, released 11 years earlier.

==Plot==
Three brothers live at home with their parents and work at the family restaurant that has been managed by their father for the past 25 years. The brothers expect one day to take over the restaurant themselves, but one morning their father comes to the realization that he hates working there and he sells the restaurant without consulting the rest of the family. This begins to break apart the family, and one of the brothers, angry with the fathers decision, leaves to find another job. The father subsequently dies from a heart attack.

==Cast==
*Sean Astin as Duncan McDermott
*Stockard Channing as Nancy Trainer
*Melinda Dillon as Eileen McDermott (the mother and wife of Jake)
*Jim Haynie as Jake McDermott (The father and owner of McDermotts Fried Chicken restaurant)
*Levon Helm as Denny Stockton
*Dinah Manoff as Lois Cook (one of the waitresses at the restaurant)
*Dermot Mulroney as Kit McDermott
*Tim Quill as Brian McDermott
*Keith Szarabajka as Kevin Burley
*Daphne Zuniga as Beverly Young Sheila Kelley as Beth Harper
*Ryan Hill as Demetri Harper
*Rick Marshall as Charlie

==Trivia==
*This movie was originally to be released in 1988 but had to be shelved for more than a year because Hemdale Film Corporation was having bankruptcy problems. 

==Reception==
Staying Together was released on November 10, 1989. Despite good reviews and a strong cast, the film flopped, making only $4,348,025 at the box office when other films released at that same time such as The Little Mermaid, Back to the Future Part II and Look Whos Talking became top ten box office hits for 1989. This would be the last film directed by Lee Grant.  It was released on VHS in the 1990s and on DVD in 2005.

==Awards==
*In 1990 Lee Grant was nominated for a Critics Award at the Deauville Film Festival. Sean Astin won the Young Artist Award for Best Young Actor Starring in a Motion Picture.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 