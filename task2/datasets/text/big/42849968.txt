Shankar IPS
{{Infobox film
| name = Shankar IPS
| image =
| caption = 
| director = M. S. Ramesh
| writer = M. S. Ramesh
| based on = 
| producer =K. Manju
| starring = Duniya Vijay   Catherine Tresa   Ragini Dwivedi  Avinash
| music = Gurukiran
| cinematography = Dasari Srinivasa Rao
| editing = S. Manohar
| studio  = Bindushree Films
| released =  
| runtime = 145&nbsp;minutes
| language = Kannada
| country = India
}}
 2010 Indian Kannada language action - crime film written and directed by M. S. Ramesh and produced by K. Manju. The film stars Duniya Vijay along with Catherine Tresa, a model making her debut, and Ragini Dwivedi in the lead roles. The film deals with the social issues such as acid attacks on women, plight of women call centers employees and others. The film was also dubbed in Hindi with the same title name. 

The film released on 21 May 2010 rated U/A across Karnataka and set high expectations for its storyline and toned body of the lead actor. However upon release, the film generally met with negative reviews from the critics and audience. 

==Plot==
Shankar Prasad (Vijay) is a tough cop who is on a mission to eradicate all the social evils around him. His unique style is to kill his enemies out rightly in encounters which irks most of the corrupt politicians and cops. He gets dismissed and transferred several times but does not get deterred in completion of his mission. Shilpa (Catherine) is a wannabe beauty contestant winner who dreams of winning the Miss India title. She comes across with the top business tycoon Saklejs son who assaults her and even makes an acid attack on her. Shankar is on a mission to hunt this criminal even as he develops a love feeling towards her.

== Cast ==
* Duniya Vijay as Shankar Prasad
* Catherine Tresa as Shilpa
* Ragini Dwivedi as Ashwini
* Avinash as Saklej
* Rangayana Raghu
* Vinaya Prasad
* Chi. Guru Dutt
* Sunil
* Shobharaj
* Kashi

==Production==
Director M. S. Ramesh teamed up with popular producer K. Manju for his second directorial venture and named it Shankar IPS. He was on the hunt of a well built actor who would require to do some heavy action sequences for the film and play a cop. For this role, he opted   Duniya Vijay again after his first film Thaakath. Vijay was required to build a six-pack abs for his role of a tough cop and the director teamed up with ace body builder Kitty to train to build up Vijays physiques. Bangalore based model, Catherine Tresa was roped in to play the female lead.  Another actress Ragini Dwivedi was cast to play the second lead role.

The filming took place for 65 days at a stretch and was shot primarily in locales of Mysore. Some song sequences were shot at Bangkok.

== Soundtrack ==
{{Infobox album  
| Name        = Shankar IPS
| Type        = Soundtrack
| Artist      = Gurukiran
| Cover       = 
| Released    = May 11, 2010
| Recorded    = 
| Genre       = Film soundtrack
| Length      =
| Label       = Anand Audio
| Producer    = 
| Reviews     =
| Last album  = 
| This album  = 
| Next album  = 
}}
 Prem Kumar being the special invitee. The event was held at a hotel on 11 May 2010.  

{{tracklist
| headline = Track listing
| all_music = Gurukiran
| lyrics = yes
| extra_column = Singer(s)
| title1 = Sale Sale
| extra1 = Raghu Dixit, Sonu Kakkar Kaviraj
| length1 = 
| title2 = Win Agona
| extra2 = Chaitra H. G.
| lyrics2= Santhosh Naik
| length2 =
| title3 = Chandra
| extra3 = Apoorva Sridhar Kaviraj
| length3= 
| title4 = Shankar IPS
| extra4 = Gurukiran
| lyrics4= Gurukiran
| length4 =
| title5 = Chumbaka
| extra5 = Vijay Prakash, K. S. Chithra Kaviraj
| length5 = 
}}

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 

 