Avalon (2001 film)
{{Infobox film
| name           = Avalon
| image          = Avalonenpolognetheatricalpolish.png
| caption        = Polish release poster
| director       = Mamoru Oshii
| producer       = Atsushi Kubo
| screenplay     = Kazunori Itō
| starring       = Małgorzata Foremniak Dariusz Biskupski
| music          = Kenji Kawai
| cinematography = Grzegorz Kędzierski
| editing        = Hiroshi Okuda
| studio         = Deiz Bandai Visual Media Factory Dentsu Nippon Herald Films
| distributor    = A-Film Distribution Miramax Films (North America)
| released       =  
| runtime        = 106 minutes
| country        = Japan Poland
| language       = Polish
| budget         = 
| gross          = 
}} science fiction drama film directed by Mamoru Oshii and written by Kazunori Itō.  The film stars Małgorzata Foremniak as Ash, a player in an illegal virtual reality video game whose sense of reality is a challenged as she attempts to unravel the true nature and purpose of the game. Avalon was filmed in Wrocław, Nova Huta, the Modlin Fortress and Warsaw.

==Plot==
 
 
In an alternate universe, people are addicted to an illegal simulation video game called Avalon. Despite its popularity, the game can leave a player catatonic. One player in the game, Ash (Małgorzata Foremniak), is a very skilled warrior that plays solo in her missions. After completing one of her missions, Ash cashes in her winnings for ammo and currency, whereas the Game Master (Władysław Kowalski) tells her that playing alone is more dangerous on the next level, and he wants her to consider joining a party. Ash witnesses her own performance on the battle screen in the lobby while waiting for her money from the receptionist (Katarzyna Bargielowska).

The next day, Ash watches as Bishop (Dariusz Biskupski), a player who spied on her the day before, breaks her record time on the Class A mission. After Ash attempts to find Bishops stats in frustration,  Ash cancels her game and requests more information about the player "Bishop" from the GM, saying that she is being challenged by him.  Ash later runs into Stunner (Bartek Świderski), a Thief class player and a member from her former party, Team Wizard. While eating at a cantina, Stunner asks her if she heard what happened to Murphy (Jerzy Gudejko), her former team leader. Like her, he also went alone and was "lost".

While Ash visits Murphy at the hospital, Stunner informs her on a hidden character in Class A, a neutral character that appears as a silent young girl with sad eyes. Some of the players think that she is a bug in the program, and name her the "ghost". As Ash walks through the corridor, a girl with the same characteristics as described by Stunner watches Ash. Stunner tells her that the players who went after the "ghost" never returned and turned into "Unreturned". There is a rumored area in the game called "Special A", where players cannot reset to escape the game, however it does give out a ton of experience points when completed. Thinking that the ghost is the only gateway into Special A, players go after her, like Murphy did. Ash stands over Murphys bed in the ward, where he has become a comatose vegetable.

Through a flashback, Murphy commands Team Wizard on one of their missions. Despite Stunner telling Murphy that they are low on ammo and have to go back, but Murphy retorts that resetting the game is not a part of their partys strategy. As she leaves the hospital, Bishop, uses his scope to watch after her. After that, he leaves, walking through dozens of unreturned patients on the hospital porch.

At home, Ash searches for words regarding the game Avalon, Unreturned, and the ghost. The computer instead closes up her searches, and Ash resets her computer, which leaves out an important keyword, "Nine Sisters". She accesses it to reveal a drawing of the Nine Sisters, with the words, "Here lies King Arthur|Arthur, the once and future king." Ash logs in using her game card and gets a message telling her to meet in Ruins C66 at Class A. When Ash asks the GM about the Nine Sisters, the GM responds with the name Morgan Le Fay, one of the nine fairy queens who ruled Avalon, the legendary isle, and brought the dying King Arthur over there. In response, Ash heard of a story regarding Morgan in Northern Europe. Odin encountered a shipwreck and drifted to an island, where Morgan saves him. She gives him a golden ring which grants him immortality and eternal youth. However, without Odin realizing, she also sets a Crown of Oblivion upon his head, which makes him forget his homeland and the world outside.

Ash enters the game and reaches Ruins C66, and meets Jill (Alicja Sapryk), who claims to be one of the Nine Sisters. She tricks Ash into meeting Murphy of the Nine Sisters (Michał Breitenwald), as part of the fake Nine Sisters plan to steal Ashs equipment data. Ash takes Jill hostage and demands to know what Murphy of Nine Sisters know. He tells her that only the real Nine Sisters know about Special A, and they are the programmers who created this game. However, a chopper appears outside the ruins due to a game time-lag, and kills most of the players inside, including Murphy of Nine Sisters. Ash grabs Jill and seeks cover, attempting to fire back at the chopper. It releases its missiles and due to the lag, they teleport in front of Ash, who screams out "Reset". Ash is forced out of the game.

While going home, Ash notices the people are immobile, with only a dog looking at her. Reaching home, her dog, unseen, welcomes her. When she finishes preparing a meal for her dog, she realizes that it has disappeared. She seemingly hears the chopper from the game flying through her district. The next day, Ash goes to a bookstore and buys books regarding Arthurian legend and Avalon. As she leaves, she runs into Stunner again, who invites her to breakfast. While eating, Stunner tells her that there is one common link between parties who try to seek the entrance to Special A: a Bishop class player, who can seemingly make the ghost appear. Not any Bishop, he says, but a level 12 or 13 Archbishop. Before being unreturned, Murphy was also a Bishop class player, and he entered Special A. Ash neither has enough time nor money to switch to Bishop and level up from there, nor join up a party due to her habit of playing alone.

Ash finds Bishop at her house. Bishop tells Ash about the rumors spreading of Team Wizards demise, which seems to be Ashs doing due to her calling "Reset" back in the partys final mission. Ash requests to form a party with Bishop, who tells her to meet at Flak Tower 22 in the game.

After the branchs closing hours, Ash tells the receptionist that she is going to meet a person who can bring her to Special A. The receptionist tells Ash that Avalon is still a game and if a program cannot be cleared, it is not a game anymore and Special A was kept hidden as a forbidden area. The receptionist reveals that she works for Bishop as a terminal manager. Ash is told that Bishop accesses the game from his own terminal. She is asked by the receptionist to keep away from Bishop, but she refuses, as Murphy is still trapped in Special A. The receptionist, realizing she cannot change Ashs mind, prepares her booth. When Ash logs in, she sees the face of the GM in her helmet, who prefers that she does not enter Special A.

In the game, Ash walks up to the top of Flak Tower 22, where she rendezvous with Bishop and 3 dummy players created from the game. Ash deduces that Bishop works for the real Nine Sisters, the original designers of the game. Stunner also arrives to help, which meant that Bishop scouted him to look for Ash all along. At Ruins D99, Ashs party runs into a Class A-strength enemy, the Citadel. Bishop, Stunner and the rest of the party distract the giant vehicle with their gunfire, while Ash eventually runs up behind the Citadel and uses her RPG to blow out the back, its weak point. As the party gains tons of experience points, Stunner spots the ghost, who disappears into the wall. He shoots at it as it runs but this does not affect it. He is then shot by an enemy opponent, which Ash kills. At Stunners side, Ash learns that the ghost can only be killed if it leaves the wall and become corporeal. Stunner also confesses he was the one who called out "Reset" in Team Wizards last mission, leading to the groups disbandment. After this, his character dissolves, throwing him out of the game.

Bishop hands Ash a gun and she goes after the ghost, who plays hide-and-seek with her. Reaching the ghosts last location, she manages to fire at it, which breaks into 2D fragments and converts into a gateway to Special A. Ash steps into the portal, which assimilates her into program codes, then brings her to Special A.

After Ash wakes up in the game terminal, Bishop tells her she is in and that the only objective to complete in Class Real is to defeat the Unreturned staying here; if she completes it, she will exit the game. However, she must not hurt any of the neutral characters who operate under free will in this area, or the game is over. Ash asks Bishop why he brought her here, and he responds that surely she knows the answer within her. Ash is directed to a poster, which features an Avalon-themed concert by the Warsaw Philharmonic Orchestra. That is her destination to which she will find the Unreturned, and she takes the ticket pinned to the poster. Ash walks out of the corridor of the game branch into a full color city. Walking along the sidewalk, Ash is bewildered by the amount of people on the streets. She finds more of the Avalon-themed concert posters and eventually finds the location of the concert.

Ash confronts Murphy at the concert hall. Ash reveals that she kept quiet about it and thought she knew why. She also says that Stunner told her that Murphy was lost and was just another Unreturned. When Ash asks Murphy if he wanted to abandon them to spend the rest of his life as a vegetable, Murphy tells her reality is an obsession that takes hold of them and questions why he could not make this his own reality. Ash tells him that he is actually running away from his problems. Murphy then asks Ash about her missing silver streak of hair, and also asks if Ash has ever been shot, to feel real pain in a real body. Murphy says that when one of them dies, and the body does not vanish, the other will know if this place is real or not. Both of them draw their guns at each other, and Ash mortally wounds Murphy. Before he dies, Murphy reveals that his gun wasnt actually loaded, and tells Ash to never let appearances confuse her, and that this is the world where she belongs. His body then dissolves as in a standard game, demonstrating that this level is definitely not real. Ash then enters the now curiously empty concert hall and confronts the ghost standing on the stage, who flashes her a smile. The text "Welcome to Avalon" is blended in.

==Cast==
* Małgorzata Foremniak as Ash
* Władyslaw Kowalski (actor)|Władysław Kowalski as Game Master
* Jerzy Gudejko as Murphy
* Dariusz Biskupski as Bishop
* Bartek Świderski as Stunner
* Katarzyna Bargielowska as Receptionist
* Alicja Sapryk as Jill
* Michał Breitenwald as Murphy of Nine Sisters
* Zuzanna Kasz as Ghost
* Jarosław Budnik as Cooper (voiceover)
* Andrzej Dębski as Cusinart (voiceover)
* Beszamel as Ashs Dog

==Production==
Even though the film was produced and directed by a Japanese crew, it is a half European, half Asian work since Avalon was co-produced by a Polish film company, starred Polish actors and was filmed mostly in Wrocław, Poland with Polish dialogue. A Japanese dub was created, however, for the films original Japanese release and is available on the Japanese region 2 DVD.

"Shooting it in Japan was impossible," Oshii advised interviewer Andrez Bergen in a major article that appeared in Japans Daily Yomiuri newspaper in 2004. "I didnt think of using a Japanese cast. I considered shooting in the UK or Ireland, but the towns and scenery in Poland matched my image for the movie."

According to Oshii, one of the advantages of shooting the film in Poland was that Polish Armed Forces were willing to lend their equipment (T-72 tanks and Mil Mi-24|Mi-24 attack helicopters, among others) to the film makers without any additional fees.

In Europe, Avalon was screened out of competition at the   (2001), and in the United Kingdom, it won the "Best Film" award at Sci-Fi-London (2002).
 bootleg DVDs imported from Asia) until Miramax released it on DVD in late 2003. The North American version has added narration to make it easier for the audience to understand the plot; although the option to view the film without the English overdubbing is provided, the subtitles still display the added dialogue. The British region-free DVD has literal English subtitles which explain the King Arthur connection better and does not display added dialogue.

Such viewer help was not used in European countries, like France, where local editions only feature optional subtitles about the Polish sung opera piece, in the Polish spoken original version only.

==The game==
 
In an unspecified era there is a forbidden online virtual reality video game. Players fight with modern, medieval and fantasy weapons in a world marked by war. In-game earned credits can be exchanged in real life for currency. Sometimes, usually with higher level players, a players spirit may stay inside the game, and the body stays vegetating in hospitals in the real world.
 Role Playing Game (such as character classes and experience points) and First Person Shooter (FPS) (utilizing real firearms such as semi-auto pistols (Walther PPK and Mauser C96), sniper rifle (Dragunov SVD) and rocket launcher (RPG-7)); and it also borrows from the Wizardry series Oshii played extensively during three years he was unemployed in the 1980s.

With these two genres, it shares the common principle of player hierarchy. In Avalon, players are ranked after three levels, Class C, Class B, Class A. Elite Class A players are rumored to be able to play a hidden extra mode featuring different rules and named Class SA (for "Special A").

To complete levels within the game, players must defeat powerful end-of-level bosses similar to those found in classic video games.
 French Science SF short film, La jetée. The headless statues also appear in this 1962 film. 

As an interesting first, this movie features the appearance of lag, a gameplay error due to network transfer slowdown often encountered in online games. Oshii displays lag as an ailment that causes physical convulsions in the player during these slowdowns.

The scene with Ash in the tramway is a live action recreation of a similar scene appearing in the 1999 anime feature film Jin-Roh, which Oshii wrote but did not direct. This scene is based on Oshiis own teenage experience, when he used to spend entire days spinning in loop in the Yamanote Line. 

==See also==
*Simulated reality

==References==
 

==External links==
*  
* 
*  
*  at Rotten Tomatoes
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 