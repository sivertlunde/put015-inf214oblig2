The Girl of the Northern Woods
 
 
{{Infobox film
| name           = The Girl of the Northern Woods
| image          = 
| caption        = 
| director       =
| producer       = Thanhouser Company
| writer         =
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English inter-titles
}} silent short short drama produced by the Thanhouser Company. The film is a drama that follows Lucy Dane and Will Harding and a jealous halfbreed trapper named José. Considering Will his rival, José attempts to ambush Will, but instead shoots Wills assistant. José then blames Will for the deed and Will is bound by a lynch mob and set to be executed. Lucy frees Will and sends the lynch mob away, but José encounters Will and the two fight. José is wounded and falls over a cliff, but Will is recaptured by the mob. From the bottom of the cliff, José calls out for help and Lucy responds to him. José confesses his crime to Lucy and she rushes to Will and prevents his execution. The film was directed by Barry ONeil and was released on June 3, 1910. An incomplete print of the film survives in the Library of Congress after its rediscovery in 1978 as part of the Dawson City Collection.

== Plot ==
The original synopsis of the film was published in the   trapper, adores Lucy and necessarily dislikes Will, whom he correctly counts his successful rival. More, he bears Will a grudge for responding to Lucys cries for help when he forced his attentions on her in the lonely neck of the woods. His chance to even matters with Will come shortly when he fastens on the surveyors responsibility for the shooting of the latters assistant, of which the halfbreed is himself guilty, having shot the assistant from ambush in mistake for Will. José claims he witnessed Wills alleged deed and his falsehoods are believed by the lumbermen. Rarely are the courts resorted to in that portion of the North where these events transpired and the rough lumbermen quickly decide to lynch Will. Lucy hears of the fate intended for her sweetheart and cuts his bonds. Further, she sends the lumbermen off in the wrong direction when they set out to recapture Will. The fugitive is spied by the halfbreed, who steals up from behind and attempts to knife him. The surveyor turns just in time, and in the ensuing struggle the halfbreed is wounded and falls over a precipice. At this juncture Will is retaken by the lumbermen."  
 Maker and tells Lucy that he shot Wills assistant. He puts his confession in writing and, relieved, passes peacefully away. In the meantime the lumbermen have completed the preparations that will make an innocent man pay the penalty of another mans crime. Already the noose is about Wills neck and a death prayer on his lips and then, in the nick of time, Lucy arrives with the precious confession, and Will gathers his faithful sweetheart to him in the tenderest scene that has ever closed a thrilling picture." 

== Cast ==
*Anna Rosemond as Lucy Dane    
*Frank H. Crane as Will Harding 

== Production == Romeo and Juliet.    The writer of the scenario is unknown, but it is presumably Lloyd Lonergan. Lonergan was an experienced newspaperman still employed by The New York Evening World while writing scripts for the Thanhouser productions. He was the most important script writer for Thanhouser, averaging 200 scripts a year from 1910 to 1915.  Edwin Thanhouser would later recall that this production featured a minor part of a woodsman who ended up ruining the scene through excessive smoking. He described the young actor trying to focus attention on himself by smoking "like the consolidation of seven chimneys", but ended up obscuring the action of the scene.    The two known credits in the film are for the leading players, Anna Rosemond and Frank H. Crane. Rosemond was one of two leading ladies for the first year of the company.  Crane was also involved in the very beginnings of the Thanhouser Company and acted in numerous productions before becoming a director at Thanhouser. 

According to an article in the New Rochelle Pioneer the film was produced in New Rochelle and according to a news release the film was shot in the mountains during real blizzard weather.  Bowers believes this film was shot during the winter and kept for its later June release.  The winter of 1909 through 1910 contained two notable snow events that might have possibly been used in the production. A major snow storm from December 25 to December 26, 1909, would make its way through New York City with snow total of about 10 inches and wind gusts up to 58 mph. A second major snow event occurred three weeks later, on January 14 through 15, 1910, with New York City getting 15 inches of snow.  Another lesser snow event, deemed a blizzard in the press, was recorded in early February. 

== Release and reception ==
The one reel drama, approximately 935 feet, was released on June 3, 1910.  The film was originally set to be the first release distributed through the Motion Picture Distribution and Sales Company, but a dispute with Carl Laemmle pushed the date back more than a month.  The film was reviewed positively in the The Moving Picture World for the real snow and weather and for being a high-class drama.  A shorter modern synopsis from the incomplete surviving print from the Library of Congress indicates that the film is lost after the halfbreed falls from the precipice.  The film was released nationwide and theater advertisements for the film are known in Kansas,   Indiana,  Oklahoma,  Pennsylvania, 
and Arizona. 

The survival and rediscovery of this film was by happenstance in the Canadian gold rush town of Dawson City, in Yukon, Canada. Beginning in 1903, the Dawson Amateur Athletic Association began showing films and the unreturned films were deposited in the Canadian Bank of Commerce and stored in the Carnegie librarys basement. The Dawson Amateur Athletic Association later converted a pool to an ice rink, but because of improper conversion the ice rink suffered from uneven temperatures in the middle of the rink. In 1929, Clifford Thomson, then-employed by the Canadian Bank of Commerce and also treasurer of the hockey association, solved the problem of the librarys stock of film and the inadequate ice rink. Thomson took 500,000 feet of film and stacked the reels in the pool, covered the reels with boards and leveled the rink with a layer of earth.  Dawson Amateur Athletic Association continued to receive new nitrate films which would later fuel the destruction of the entire complex in a fire in 1951. The films stored under the ice rink were preserved and uncovered in 1978 when a new recreation center was being built. The Dawson City Collection films were collected and preserved, with these prints becoming the last surviving records of these studios.  The surviving and incomplete print of The Girl of the Northern Woods was one of the films recovered at Dawson City. 

== References ==
 

 
 
 
 
 
 
 
 