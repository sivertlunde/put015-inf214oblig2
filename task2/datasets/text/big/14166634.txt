Press for Time
{{Infobox Film
| name           = Press for Time
| image          =  "Press_for_Time".jpg
| caption        = Original British 1-sheet poster Robert Asher
| producer       = Robert Hartford-Davis Peter Newbrook
| writer         = Eddie Leslie Norman Wisdom Angus McGill (book)
| starring       = Norman Wisdom
| music          = Mike Vickers
| cinematography = Jonathan Usher
| editing        = Gerry Hambling
| distributor    = The Rank organisation ivy productions Titan International Pictures Limited  
| released       = 8 December 1966 
| runtime        = 102 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Press for Time is a 1966 British comedy film starring Norman Wisdom. The screenplay was written by Eddie Leslie and Norman Wisdom, based on the 1963 novel Yea Yea Yea, by Angus McGill. It was partly filmed in Teignmouth in Devon. It was the last film Wisdom made for the Rank Organisation. 

== Plot ==
Norman Shields (Norman Wisdom) is a local newspaper seller in London. He is happy with his current job, but is sent by his grandfather, the Prime Minister (also played by Wisdom), to take up a new job as a newspaper reporter in the fictional seaside town of Tinmouth.

During his time in Tinmouth he gets himself into all sorts of trouble while on the job reporting (starting an argument at a council meeting, for example). Later in the film he becomes reporter for the entertainment section of the newspaper and covers a beauty contest which his girlfriend Liz wins. They later return to London together, leaving a more politically settled Tinmouth behind.

== Cast ==
*Norman Wisdom as Norman Shields/ Emily, his mother/ Wilfred, his grandfather (the P.M.)
*Derek Bond as Major R.E. Bartlett
*Derek Francis as Alderman Corcoran
*Angela Browne as Eleanor Lampton
*Tracey Crisp as Ruby Fairchild
*Allan Cuthbertson as Mr. Ballard (Attorney General)
*Noel Dyson as Mrs. Corcoran Peter Jones as Robin Willoughby (photographer) David Lodge as Mr. Ross (editor of the "Tinmouth Times") Stanley Unwin as Mr. Nottage (Town Clerk)
*Frances White as Liz Corcoran Michael Balfour as Sewerman
*Tony Selby as Harry Marshall (reporter for the "County Chronicle")
*Michael Bilton
*Norman Pitt
*Hazel Coppen as Granny Fork
*Totti Truman Taylor as Mrs. Doe Connor
*Toni Gilpin as P.M.s secretary
*Gordon Rollings as Bus Conductor
*Imogen Hassall as Suffragette (uncredited)

A then-unknown Helen Mirren had a role in this film, but was uncredited.

==External links==
* 

 
 
 
 
 
 
 

 