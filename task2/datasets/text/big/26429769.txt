Warhead (film)
 

{{Infobox film
| name           = Warhead
| image          = Warhead (movie poster).jpg
| image_size     =
| caption        =
| director       = John OConnor
| producer       = Guy Della-Cioppa (executive producer) Buddy Ruskin (producer)
| writer         = Buddy Ruskin (screenplay) Patrick Foulk (story) & Donovan Karnes (story)
| narrator       =
| starring       =
| music          = Adam Greenberg
| editing        = Renn Reynolds
| distributor    =
| released       =
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Warhead is a 1977 American film directed by John OConnor and produced by Buddy Ruskin who produced The Mod Squad. The film was originally shot in Israel in 1973 under the title Sabra Command 

The film is also known as Prisoner in the Middle, and Mission Overkill (in West Germany).  In Mexico, it was released as Amenaza Nuclear ("Nuclear Threat").

== Plot summary ==
Terrorists blow up a school bus in the Middle East, killing everyone on board except the military guard, Lt Liora who identifies the Palestinian Major Malouf as leading the operation. 

An Israeli military operation with Liora along to identify Malouf sets out to find the terrorists. Meanwhile, a US Air Force Colonel named Stevens, who is a nuclear arms expert, is parachuted into the desert to disarm a nuclear bomb that has accidentally fallen out of an airplane flying over the area. He and the military operation eventually meet and clash over ideas as the operation decides to take possession of the nuke. Throughout the film, the operation pursues the terrorists and the nuke expert Stevens learns more about humanity through his relationship with Liora. In the end, all the terrorists and operation members die from various fights. Stevens alone survives and recalls his newfound discovery of the love of humanity. The nuke is left in the desert, presumably defused by another nuke expert.

== Cast ==
*David Janssen as Tony Stevens
*Karin Dor as Lt. Liora Christopher Stone as Captain Ben-David
*Art Metrano as Mario
*Tuvia Tavi as Meshugi
*David Semadar as Malouf
*Joan Freeman as Namoi
*Mordechai Arnon as Pupik
*Eddy Muktar as Hassan
*Jacob Roda as Sammy
*Poly Reshef as Rabbi
*Itzik Weiss as Rony
*Ellyn Stern as Shoshonna
*Rachel Terry as Daphne
*Nicholas Torzeski as CIA Agent
*Arie Hamburger as Driver
*D.R. Morris as General

==Notes==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 



 