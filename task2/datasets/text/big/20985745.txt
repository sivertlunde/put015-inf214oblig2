Women in Trouble
{{Infobox Film
| name           = Women in Trouble
| image          = Women in trouble ver4.jpg
| caption        = Promotional film poster
| director       = Sebastian Gutierrez
| producer       = Sebastian Gutierrez
| writer         = Sebastian Gutierrez
| starring       = Carla Gugino Connie Britton Emanuelle Chriqui Marley Shelton Elizabeth Berkely Cameron Richardson Adrianne Palicki Joseph Gordon-Levitt
| music          = Robyn Hitchcock
| cinematography = Cale Finot
| editing        = Lisa Bromwell Michelle Tesoro  Myriad Pictures (International)
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $50,000
}} American comedy film, written and directed by Sebastian Gutierrez, and starring a cast consisting of Carla Gugino, Adrianne Palicki, Marley Shelton, Connie Britton and Emmanuelle Chriqui.
It was shot in 10 days for $50,000.

==Plot==
The film focuses on six women in Los Angeles as their lives become intertwined in the course of 24 hours. 

Porn star Elektra Luxx (Carla Gugino) discovers she is pregnant. Leaving her doctors office, she gets stuck in an elevator with Doris (Connie Britton), sister to Addy (Caitlin Keats). Addy has recently started taking her daughter, Charlotte, to see her therapist, Maxine (Sarah Clarke) (While secretly using the visit to sleep with the therapists husband, upon choosing to follow her therapists advice to "live with more risk."). Upon finding out about her husbands secret during a therapy session with Charlotte, Maxine rushes out and gets into her car. While backing out, she hits Holly Rocket (Adrianne Palicki), adult film star. Holly and Bambi (Emmanuelle Chriqui) were running to safety after a session with one of Bambis clients is interrupted. Meanwhile, flight attendant Cora (Marley Shelton) finds herself the object of rock star Nick Chapels (Josh Brolin) affection on a flight to his bands upcoming show.

==Cast==
* Carla Gugino as Elektra Luxx
* Adrianne Palicki as Holly Rocket
* Connie Britton as Doris
* Marley Shelton as Cora
* Cameron Richardson as Darby Garcelle Beauvais-Nilon as Maggie
* Elizabeth Berkley as Tracy
* Emmanuelle Chriqui as Bambi
* Sarah Clarke as Maxine McPherson
* Simon Baker as Travis McPherson
* Joseph Gordon-Levitt as Bert Rodriguez
* Rya Kihlstedt as Rita
* Caitlin Keats as Addy Hunter
* Isabella Gutierrez as Charlotte
* Paul Cassell as a Jay

== Production and sequel ==
The film was directed by Sebastian Gutierrez. Production began and ended in Los Angeles. The film premiered at the 2009 South by Southwest Film Festival.  The film opened in the United States on November 13, 2009. A sequel, Elektra Luxx, was released on March 11, 2011. Gutierrez returns as the writer-director, and the cast includes Carla Gugino, Joseph Gordon-Levitt, Timothy Olyphant, Julianne Moore  and Justin Kirk. Gutierrez is planning on making a third installment, tentatively titled Women in Ecstasy    which was initially planned to be released in 2012.

== Critical reception ==
Women in Trouble polarized critics. On the positive side, Joe Leydon of   said "Unlikely accidents, shared secrets and zany twists combine to whip up a pleasing froth.".  John DeFore writing for The Hollywood Reporter praised the direction: "Gutierrezs script can not supply female characters as believable as Almodovars, but in the directors chair he gives his cast room to compensate with funny, self-aware performances". 

At the other end of the spectrum, Melissa Anderson writing for Village Voice said "blue material mixes awkwardly with sob stories". And Manohla Dargis of The New York Times was not amused, complaining that "The amateurish production values might be pardoned if the clichés—the hard-core porn star with the soft heart, the therapist who needs to heal herself—inside the poorly lighted, badly shot images were not so absurd and often insulting".

==References==
 

==External links==
*  
*  , from a promotional blog for Women in Trouble and related films

 

 
 
 
 
 
 
 
 
 
 
 