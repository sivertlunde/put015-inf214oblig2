Love Letters (1999 film)
{{Infobox film
| name           = Love Letters
| image          = 
| caption        = 
| director       = Stanley Donen
| producer       = Leonard Goldberg  Martin Starger
| writer         = A.R. Gurney Steven Weber Laura Linney
| music          = Lee Holdridge
| cinematography = Mike Fash
| editing        = Robert M. Reitano Philip Weisman ABC
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
}} ABC television 1988 play by A. R. Gurney. Gurney adapted his own work for the telescript, dramatizing scenes and portraying characters that were merely described in the play. Donen had envisioned it to be a theatrical film, but a limited budget restricted him to make a TV movie and he shot the film in only 17 days.  It is his last film as of 2015.

==Plot==
An ambitious U.S. Senator reflects back on his life after the death of a woman whom he loved and kept in contact with only through correspondence. The movie is told in flashbacks as the two first meet as children and begin their lifelong correspondence. He grows into his political aspirations and leaves her behind, as she becomes a struggling artist facing a rocky life: the two encounter different experiences on the paths they take. 

== Cast == Steven Weber as Andrew Ladd  
* Laura Linney as Melissa Gardner Cobb  
* Kirsten Storms as Teenaged Melissa  
* Tim Redwine as Teenaged Andrew  
* Isabella Fink as Melissa - Age 7  
* Stephen Joffe as Andy - Age 7  
* Chas Lawther as Harry

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 