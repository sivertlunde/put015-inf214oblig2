The Girl Next Door (2007 film)
 
{{Infobox film
| name = The Girl Next Door
| image = GND poster smaller.jpg
| image_size = 
| alt = 
| caption = Theatrical release poster
| director = Gregory M. Wilson
| producer = William M. Miller Andrew van den Houten
| based on =  
| screenplay = Daniel Farrands Philip Nutman
| narrator = William Atherton
| starring = Blanche Baker Daniel Manche Blythe Auffarth
| music = Ryan Shore
| cinematography = William M. Miller
| editing = M.J. Fiore
| studio = Modernciné Modern Girl Productions
| distributor = Starz Home Entertainment
| released =  
| runtime = 91 minutes
| country = United States
| language = English
}} horror film 1989 The novel of the same name.  The film is loosely based on true events surrounding the torture and murder of Sylvia Likens by Gertrude Baniszewski during the summer of 1965. 

==Plot== sadistic Psychopathy|psychopath/secret Satanist, and their three cousins, Willie, Ralphie, and Donny (Graham Patrick Martin, Austin Williams and Benjamin Ross Kaplan).

As a child, David (Daniel Manche) is the next-door neighbor to the Chandlers. When Meg arrives, he is instantly infatuated by her. Aunt Ruth allows the children of the neighborhood to travel freely in and out of her house, offering them beer and cigarettes. Meg quickly becomes a target to Ruth, who belittles her, making suggestions that she is a whore, and starves her. One day, David arrives at the house to find Megs cousins tickling her. When Ralphie brushes her left breast, she knocks him to the floor, warning him to back off, and runs from the room. Ruth beats Susan for Megs actions, as Megs cousins hold her back. Ruth then takes the ring that Meg wears around her neck, which belonged to her mother.

A few days later, Meg stops a policeman, Officer Jennings (Kevin Chamberlin) and tells him what happened. As punishment, Ruth, her children, and their friends bind Meg in the cellar with her hands tied to the rafters. They play a bizarre game of "Confession (law)|confession", and when Meg has nothing to confess, she is stripped naked. They blindfold her, gag her, and leave her there. That night, the boys sneak back downstairs, giving her water. They agree to loosen her bindings, but only if she lets them touch her. She refuses, but David loosens them anyway.

A while later, Meg is untied but refuses to eat, claiming that her mouth is too dry from dehydration to swallow it without choking. Ruth again beats Susan for Megs disobedience. The kids of the town treat her like a plaything, beating, cutting, and burning her. David tries to tell his parents, but is unable to do so. Officer Jennings eventually arrives at the house, having had his suspicions raised after a local boy talked to him. However, he leaves finding nothing. David decides that it is time for Meg to escape, and unties her, promising her he will leave money in the forest for her to run away with.
 mutilated with a blowtorch.
 touching her on a regular basis to the extent of making her bleed, so Meg didnt want to leave her behind. Susan then tearfully insists that Meg should have just gone without her and saved herself while she could, but David tells Susan everything was going to be all right.

David is afraid that Meg wont survive much longer without help, so he lights a fire in the cellar. As the smoke rises, Ruth enters the room and is violently beaten to death by David with Susans crutch. Jennings arrives, taking Susan from the room and going for help. David retrieves Megs mothers ring from Ruth, and gives it to her before she finally dies, her body succumbing to its wounds.

Back in 2007, the adult David reflects on how his past still haunts him to his present day, though as Meg taught him, "Its what you do last that really counts."

==Cast==
  Rutherine "Ruth" Chandler
* Daniel Manche as David Moran
** William Atherton as adult David Moran  Megan "Meg" Loughlin
* Madeline Taylor as Susan Loughlin
* Benjamin Ross Kaplan as Donald "Donny" Chandler
* Graham Patrick Martin as William "Willie" Chandler, Jr.
* Austin Williams as Ralph "Ralphie" Chandler
* Michael Nardella as Tony
* Kevin Chamberlin as Officer Lyle Jennings
* Dean Faulkenberry as Kenny
* Gabrielle Howarth as Cheryl Robinson
* Spenser Leigh as Denise Crocker
* Grant Show as Mr. Moran
* Catherine Mary Stewart as Mrs. Moran
* Peter Stickles as EMT
* Michael Zegen as Eddie
* Jennifer Alexander as Girl at concession stand 
* Jack Ketchum as Carnival worker 
* Mark Margolis as Hit and run victim
 

==Reception==
The film had a polarizing effect on film critics. On Rotten Tomatoes, it currently holds a 67% "Fresh" rating. In contrast, Metacritic assigns it a 29.
 Stand by Me." 

==See also== Showtime premiere in mid-2008.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 