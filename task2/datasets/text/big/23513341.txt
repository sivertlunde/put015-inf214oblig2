Savage Vengeance
 
{{Infobox film
| name           = Savage Vengeance
| image          = Savage_Vengeance.jpg
| image_size     = 250px
| border         = yes
| alt            = 
| caption        = DVD release poster
| director       = Donald Farmer
| producer       = Barney Griner
| writer         = Donald Farmer Vickie Kehl Donald Farmer Melissa Moore
| music          = Perry Monroe
| cinematography = Donald Farmer
| editing        = Donald Farmer
| studio         = Tapeworm
| distributor    = Eden Entertainment
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
}}
Savage Vengeance is a 1993 American Slasher film|slasher-rape and revenge film directed by Donald Farmer and the unofficial sequel to the controversial 1978 film I Spit on Your Grave, starring Camille Keaton, under the alias "Vickie Kehl", as Jennifer Hills.  No scenes from the first film were used, and instead new footage was used for flashbacks scenes, ostensibly because of legal rights issues.

==Plot==

A woman named Jennifer Hills is driving. When she gets out of her car she takes her book and takes a walk in the woods. She gets tired so she stop and goes by the river. She takes a rest and starts to read. A group of four men stalk her. They then rape her leaving her for dead. Five years later a woman is in a bar and meets a strange man named Tommy talks to her. She tells him to piss off. When the singer starts to sing the woman leaves. She sees Tommy who slits her throat and stabs her. Meanwhile Jennifer is now in law school. Her teacher makes fun of her and he says to the class after two weeks Jennifer recovered and killed them. She shot one in the head, she hacked up the other two and stabbed the last one. She was never charged. Jennifer then feels sad so she leaves the class. She meets her friend Sam. Jennifer tells Sam for them to get out of town, so she does. The two start the car and go. They discuss Jennifers life and what happened. When they stop for gas Sam meets Tommy. Tommy starts to flirt with her but she slaps him. A man named Dwayne saves her and they decide to flirt. Sam tells him where she and Jennifer are staying at. When Jen and Sam get there Sam is shocked to see the place so horrid and ugly. She decides to go out for air. After a little bit Sam is lost. She sees a cottage which is Tommys. When she goes in she sees Dwayne. They kiss passionately but then Dwayne rapes her on the couch and on the bed. Tommy stabs her, killing her in the process. The next day Jennifer is worried so she decides to look for her. When she goes back to the gas station Dwayne and Tommy tell her where she is. They take her to the cabin where she sees Sam dead. Jennifer escapes from the house and runs out. Tommy catches her but she hits him multi times with a branch knocking him out. When she stops to rest Dwayne sneaks up on her and rapes her. Tommy finds them and stabs Jennifer leaving her for dead. After a little bit Jennifer gets up and wants revenge. She runs out the woods and sees a car. She goes in the car and drives away.  A little while later at the gas station the sheriff enters. He talks to the two boys and tells them that Jennifer and Sam havent been seen and they moved into the cottage. The men claim they no nothing about them. The sheriff warns them and tells them not to go near since they found a dead body in the room but they were never charged. After this Dwayne goes to the bar and gets drunk. When he gets out he takes a walk in the woods and then hears something. He sees Jennifer with a chainsaw and runs. She chases him but he trips. She chainsaws his head in half killing him. Meanwhile Tommy is treating the dead body of a girl and Sams body as if they were alive. Jennifer goes in front of the house with a shotgun, yelling at Tommy to leave. Tommy escapes from the house and runs away but Jennifer yells at him to stop. He does and she shoots him in the testicles. He begs her to come and help him. The film ends with Jennifer walking away smiling and leaving Tommy to die like they did to her.

==Cast== Vicki Kehl as Jennifer (last name removed due to legal purposes)
* James Cochran as Rapist
* Bill Gatson as Rapist
* George Maranville as Rapist
* Mike Smith as Rapist 
* Jamie Peak as Girl in Bar
* Donald Farmer as Tommy
* Melissa Moore as  Singer
* Robin Sinclair as Law Professor
* Jill Harris as Student
* Tom Gibson as Student
* Tanya Simpson as Student
* Linda Lyer as Sam
* Phil Newman as Dwayne Chesney
* Bill Sweeney as Manny
* Brenda Gilbert as Customer
* Jane Clark as Clerk
* Jack Clout as Sheriff
* Bill Wilson as Deputy
* Jack Kent as Bulldog
* Shelia Allison as Waitress
* Sherry Mosherry as Dead Girl

==Production==
The film was supposedly shot in 1988, but not released until 1993, this explains the somewhat dated look to the film and sound of its score. Not much is known about how the film came about, but it is well known  that the production was a troubled affair. The film was shot on video with an extremely low budget. 

The films star, Camille Keaton, refuses to speak of the film or its production. Her reasons for this havent been made clear, but it has been rumored  that she left the set before the end of the production, hence the films abrupt end. Her only reference to appearing in the film came at a horror convention in 2005, when asked by a fan to explain her involvement in the movie, she replied: "Im sorry, I cant speak about that." 

Rumors indicate that the film was shot as a sequel to I Spit on Your Grave without legal rights and this led to legal trouble for five years. Director of I Spit on Your Grace Meir Zarchi allegedly sued Farmer, and the result was that the character of Jennifers last name was removed from the film (when another character said her last name, it was dubbed over or removed sloppily).

The film was altered taking out any references to the original film, re-edited and then finally released quietly in 1993 as Savage Vengeance. Director Donald Farmer still denies these claims but some people speculate he is legally unable to talk about it.

During an audio interview for DVD Monthly in 2005, Zarchi spoke about the film and Camille Keatons involvement. He told of Keaton calling him in tears, explaining she had walked off set and apologized to him for getting involved with the film. He recalled Keaton calling him from "somewhere in the mid-west", giving an idea of where the movie was shot. 

The audio interview with Zarchi is included as a special feature on the 2005 Millennium Edition DVD of I Spit on Your Grave.  Zarchi maintains he has never seen the film.

Although the film was marketed in some territories as the sequel to I Spit on Your Grave, it is commonly thought that this was the initial idea and not only came about to capitalize on Keatons involvement in the production. 

==Release==
A region-free DVD was released on October 31, 2000.  Alternative titles include I Spit on Your Grave 2: Savage Vengeance, I Will Dance on Your Grave: Savage Vengeance and Return to the Grave.

In 2013 a special edition DVD was released by Massacre Video, including a rare interview with actress Camille Keaton.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 