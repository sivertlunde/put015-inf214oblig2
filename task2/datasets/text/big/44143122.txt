Din Dahade
{{Infobox film
| name           = Din Dahade
| image          = 
| image_size     = 
| caption        = 
| director       = Yash Chouhan 	 
| producer       = Aman Batla Aman Films Production
| writer         = 
| narrator       = 
| starring       = Sunil Puri Kamran Rizvi
| music          = Jeetu-Tapan
| lyrics         = Naqsh Layalpuri
| editing        = 
| distributor    = 
| released       = 05-01-1990
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

 Din Dahade  is a 1990 Bollywood film directed by Mohan Segal and starring Sunil Puri, Kamran Rizvi , Sriprada , Ajit Vachani, Kunika Lall , Vikram Gokhale, Goga Kapoor  and Anil Kochar. 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Kaam Jahan Mein"
| Kumar Sanu
|-
| 2
| "Jeena Jeena Hai"
| Bhupinder Singh
|-
| 3
| "Ladi Ladi Jab Se Nazar"
| Asha Bhosle, Kumar Sanu
|}

==External links==
*  

 
 
 
 