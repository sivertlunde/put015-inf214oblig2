Permanent Midnight
 

{{Infobox film
| name         = Permanent Midnight
| image        = Dvd permanent midnight.jpg
| caption      = Theatrical release poster
| director     = David Veloz
| producer     = Jane Hamsher Don Murphy
| writer       = Screenplay: David Veloz Novel: Jerry Stahl
| starring     = Ben Stiller Maria Bello Elizabeth Hurley Owen Wilson Peter Greene Cheryl Ladd Janeane Garofalo
| music        = Daniel Licht
| distributor  = Artisan Entertainment
| released     = September 16, 1998
| runtime      = 88 minutes
| country      =   English
| awards       = 
| budget       = 
| gross        = $1,166,199
}} 1998 independent directed by David Veloz and starring Ben Stiller.  The supporting cast features Maria Bello, Elizabeth Hurley, Owen Wilson, Cheryl Ladd and Janeane Garofalo.

The film is based on Jerry Stahls autobiographical book of the same name and tells the story of Stahls rise from a small-time television writer to his success as a comedy writer making up to $5,000 a week writing for 1980s series like Thirtysomething (TV series)|thirtysomething, Moonlighting (TV series)|Moonlighting, and ALF (TV series)|ALF (changed in the film to Mr. Chompers).
 detox survivor to whom Stahl relates his rise and fall. The film also stars Owen Wilson as Stahls friend and fellow addict, Nicky; Elizabeth Hurley as his wife, Sandra; and Janeane Garofalo as a Hollywood agent, Jana. Fred Willard also appears as the producer of Mr. Chompers. The real Stahl makes a cameo appearance as a doctor at a methadone clinic.

Stillers performance in the film was critically acclaimed, but the film failed at the box office and never saw widespread release  . It has been released on DVD in the USA and the UK. A soundtrack CD was also released with most of the music heard in the film.

==Plot==
Approaching the end of a drug rehabilitation program, Jerry Stahl (Stiller) quits his job at a fast food restaurant on an impulse when an attractive woman named Kitty (Bello) pulls up at the drive-through window. The two check into a motel, where Jerry tells her about his life in between bouts of sex. A series of flashbacks, intercut with their conversations, details his working life to this point.
 green card. Sandra uses her position at a television studio to get Jerry onto the writing staff of the popular comedy series Mr. Chompers.  He uses memories from his childhood, including his mothers hysterical grief over his fathers death, to fuel his writing.

He juggles his Mr. Chompers job and regular visits to a heroin dealer, Dita (Liz Torres). However, his drug use eventually gets him fired. Sandra finds him a new job with a different series, No Such Luck, but star Pamela Verlaine (Cheryl Ladd) – herself a recovering addict – sternly but sympathetically insists that he kick his habit first.

As soon as Jerry starts on a methadone program, he runs across a dealer named Gus (Peter Greene), who introduces him to crack cocaine and later Dilaudid. His increased drug use costs him his new job, and Sandra throws him out, disgusted at his decision to shoot up when she tells him she is pregnant. Her opinion of him falls even further when he shows up high for the birth of his daughter Nina.

While looking after her one night, he gets high and is arrested by the police. The incident further strains his relationship with Sandra, who makes it clear that she would prefer to see as little of him as possible.

The flashbacks end at this point, with Jerry returning to Los Angeles in hopes of being part of Ninas life.  As he begins to resurrect his stalled writing career, he gets a surprise visit from Kitty. The two have one last sexual encounter before she leaves to move to Anchorage, Alaska|Anchorage.

In the final scene, Jerry appears on a series of talk shows and news programs, while commenting in voice-over about the damage that his addiction has done to his life. "I got out with a bad liver and enough debt to keep me in hock til Im 90, if Im still here. And with my luck, I will be."

==Cast==
*Ben Stiller as Jerry Stahl
*Maria Bello as Kitty
*Elizabeth Hurley as Sandra Stahl
*Owen Wilson as Nicky
*Lourdes Benedicto as Vola
*Janeane Garofalo as Jana Farmer
*Fred Willard as Craig Ziffer
*Connie Nielsen as Dagmar
*Liz Torres as Dita
*Peter Greene as Gus
*Cheryl Ladd as Pamela Verlaine
*Sandra Oh as Friend
*Jerry Stahl as Dr. Lazarus
*Andy Dick as Damian, A Talk Show Guest (Uncredited Role)

==Reception==
Permanent Midnight received mixed to positive reviews from critics. It holds a 60% rating on Rotten Tomatoes, with the consensus reading: "Aimless storytelling undermines the gripping, unsettling subject of this film." 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 