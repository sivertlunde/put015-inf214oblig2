Kaagaz Ke Fools
 

 
{{Infobox film
| name  = Kaagaz Ke Fools
| image = 
| alt =
| caption = 
| director = Anil Kumar Chaudhary
| producer = Faisal Kapadia Arun Bhairav
| writer = Faisal Kapadia Arun Bhairav
| screenplay = Faisal Kapadia Arun Bhairav
| based on = 
| starring = Vinay Pathak Mugdha Godse Raima Sen Saurabh Shukla Rajendra Sethi Amit Behl
| music = Various Artists
| cinematography = Aniket Khandagale
| editing = 
| studio = Globe Filmy Entertainment
| distributor = Globe Filmy Entertainment
| released = 24 April 2015
| runtime = 109 minutes
| country = India
| language = Hindi
| budget = 
| gross = 
}}
 Indian comedy film directed by Anil Kumar Chaudhary and produced by Faisal Kapadia and Arun Bhairav under the Globe Filmy Entertainment banner. The film was released on 24 April 2015. 

==Cast==
*Vinay Pathak as Purshottam Tripathi
*Mugdha Godse as Nikki
*Raima Sen as Rubina
*Saurabh Shukla as Dev
*Rajendra Sethi as Kuku
*Amit Behl as Vinay

==Plot==
Revolving around a middle-class family, Kaagaz Ke Fools touches upon the issue of lack of good novel writers. 

==Reception==

===Critical response===
Ekk Thee Sanam received generally negative reviews from critics. The film is one of the worst movie of 2015. Shubhra Gupta of The Indian Express stated, "You keep wanting this film to ‘ho ja shuru’, but ‘Kaagaz Ke Fools’ doesn’t have the feet for it." 

===Ratings===
Sushmita Murthy of Deccan Chronicle gave the film 1/5 stars and stated, "The name of the film leads you to believe that Kaagaz ke Fools is a comedy, but it is at best, an attempt at it with rather painful results." 

Shubha Shetty-Saha of Mid Day giving the same ratings stated, "Ironically, and perhaps reflecting the sad state of Bollywood’s growth, in 2015 we get a film titled Kaagaz ke Fools, which is so regressive and dated that it just might have made some sense back in the 50s. Well, if nothing, the film makes us painfully aware of the difference between a phool and a fool." 

==References==
 

==External links==
* 

 
 
 
 
 
 

 