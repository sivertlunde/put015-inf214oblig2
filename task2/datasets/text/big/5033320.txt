Dagon (film)
 
{{Infobox film
| name           = Dagon
| alt            =
| image          = Dagonmovieposter.jpg
| caption        = Spanish theatrical release poster
| director       = Stuart Gordon
| producer       = Carlos Fernández Julio Fernández Miguel Torrente Brian Yuzna
| screenplay     = Dennis Paoli
| based on       = The Shadow Over Innsmouth by H. P. Lovecraft
| starring       = Ezra Godden Francisco Rabal Raquel Meroño
| music          = Carles Cases
| editing        = Jaume Vilalta
| studio         = Castelao Producciones Estudios Picasso Fantastic Factory ICCA Generalitat de Catalunya, Institut Català de Finances Televisió de Catalunya Televisión de Galicia S.A. Vía Digital Xunta de Galicia Filmax International  (Spain)  Lions Gate Entertainment  (United States) 
| released       = 12 October 2001  (Sitges Film Festival) 
| runtime        = 98 minutes
| country        = Spain
| language       = English Galician Spanish
| music          = Carles Cases EUR €212,699  (Spain) 
}}
 horror film directed by Stuart Gordon and written by Dennis Paoli. Despite the title, the plot is actually based on H. P. Lovecrafts novella The Shadow Over Innsmouth  rather than his earlier short story "Dagon (short story)|Dagon" (1919). In fact, the setting takes place in Inboca, a Spanish adaption of Innsmouth.

==Plot==
 
The films opens with a dream sequence in which stock market tycoon Paul Marsh discovers a mermaid with razor sharp teeth while scuba diving into a carven pit. Paul awakes on a boat off the shores of Spain, where he is vacationing with his girlfriend, Barbara, and their friends Vicki and Howard. A sudden storm blows their boat against some hidden rocks. Vicki is trapped below deck, and Howard stays with her while Paul and Barbara take a lifeboat to the nearby fishing village of Imboca. During their absence, an unseen creature from the deep attacks the two in the boat.

At shore, Barbara and Paul find no one about, and venture into town until they eventually reach the church, where they find a priest. Barbara convinces him to help them, and the priest speaks with two fishermen at the docks, who volunteer to take either Paul or Barbara to the wreck. Despite Pauls misgivings, Barbara stays to try to find a phone in order to call a doctor while Paul goes to help their friends. Barbara notices that the priest has webbed hands.

Vicki and Howard are mysteriously missing, however, and Paul is taken back to Imboca, where he is sent to the hotel that Barbara was supposed to have gone to. But she is missing as well, and Paul is left to wait for her in an old, filthy hotel room, where he dreams of the mermaid again. His fitful rest is disturbed by a large gathering of Deep One|strange, fish-like people approaching the hotel and is forced to flee. He ends up in a macabre tannery full of human skins, where he discovers Howards remains. He escapes the tannery by starting a fire, and finds momentary safety with an old drunkard named Ezequiel, the last human in Imboca.

Ezequiel explains to Paul about how the village in lean times has fallen to the worship of Dagon. This brought incredible wealth to
Imboca in the form of fish and gold, but also horror when Dagon demanded live sacrifices and human women (including Ezequiels parents) to breed with. Paul begs Ezequiel to help him escape. Ezequiel relents and takes Paul to the Mayors manor, so he can steal the towns only car. Ezequiel distracts some Imbocans long enough for Paul to slip inside, but he accidentally honks the horn while trying to hot-wire the engine. Forced to flee into the manor, Paul finds a beautiful woman named Uxia, who looks just like the mermaid in his dreams. She saves him from discovery, but when he finds she isnt human either, he flees in horror despite her pleas to stay.

Paul narrowly escapes a horde of villagers in the car, but ends up crashing. He is caught and thrown into a barn, where he is reunited with Vicki, Ezequiel, and Barbara. The three plan to escape, but the attempt comes to naught when they are discovered. Having been raped and impregnated, Vicki kills herself. Paul and Ezequiel are separated from Barbara, and Paul and Ezequiel end up in a butchery, where they are chained and given a chance to join the worship of Dagon. When they both refuse, Ezequiel is skinned alive before Pauls eyes.

Paul is saved by the appearance of Uxia, who informs Paul that he has no choice but to join them. When he seems to concede, Uxia tells the priest to make arrangements for their marriage. After Uxia leaves, Paul escapes, killing the guards and the priest, and starts desperately looking for Barbara, collecting a can of kerosene on the way. His search brings him to the empty church and a hidden passage that takes him below ground, where a congregation of Imbocans are watching Uxia torture Barbara before she is chained and lowered into a deep, water-filled pit. While the Imbocan congregation and Uxia call to Dagon, Paul attacks, dousing several villagers in kerosene and lighting them on fire before pulling Barbara out of the pit. However, the monstrous Dagon himself grabs Barbara and drags her down, devouring her.

The uninjured Imbocans assault Paul, but are halted by Uxia and a monstrously deformed Imbocian who is revealed to be Uxias father - and Pauls. Uxia explains that Pauls human mother escaped from Imboca years ago, but now that Paul has returned, he will be her lover and they will dwell with Dagon forever. Trapped, Paul pours the last of the kerosene over his own body and sets himself on fire. Uxia grabs him and sends them both into the water, where Paul sprouts gills. With no choice left, he follows Uxia down into Dagons undersea lair.

==Cast==
* Ezra Godden as Paul Marsh
* Francisco Rabal as Ezequiel
* Raquel Meroño as Barbara
* Macarena Gómez as Uxía Cambarro
* Brendan Price as Howard
* Birgit Bofarull as Vicki
* Uxía Blanco as Ezequiels Mother
* Ferran Lahoz as Priest
* Joan Minguell as Xavier Cambarro
* Alfredo Villa as Captain Orpheus Cambarro/captain Obed March
* José Lifante as Desk Clerk
* Javier Sandoval as Ezequiels Father
* Victor Barreira as Young Ezequiel
* Fernando Gil as Catholic Priest
* Jorge Luis Pérez as Boy

== Production ==
 
Dennis Paoli wrote the screenplay back in the 1980s, but he and Stuart Gordon were unable to get the movie off the ground.  In early 2000, Brian Yuzna founded the Fantastic Factory division of Filmax and called them back to finally shoot the movie.  The original draft was more faithful to Lovecrafts short story, being based in New England.  The movie was shot at Combarro, a small fishing village near Pontevedra in Galicia (Spain)|Galicia.  The film is dedicated to actor Francisco Rabal immediately before the credits.

== Reception ==
  AllMovie wrote of the film, "Though its not perfect, Lovecraft fans will most likely be willing to forgive Dagon  s shortcomings in favor of a film that obviously shows great respect and appreciation for its source materials."  Film Threat wrote, "While not a perfect movie, Dagon crams its wild, over-the-top concepts down our throats with so much conviction that we cant help but get swept along for the ride." 

In their book  , Andrew Migliore and John Strysik write that "Gordon nicely creates the decayed humanity of Lovecrafts Innsmouth" but also that the films "relentlessness" is "draining and numbing." They conclude: "Dagon is a dark story well told, but for some Lovecraft lovers, it may be a fish that should have gotten away." 

== References ==

 

== External links ==
*  

 
 
 

 
 
 
 
 
 
 