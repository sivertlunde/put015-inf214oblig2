Spring Forward
{{Infobox Film
| name           = Spring Forward
| image          = SpringforwardDVD.jpg
| image_size     = 
| caption        = DVD cover
| director       = Tom Gilroy
| producer       = Gill Holland Jim McKay Paul S. Mezey
| writer         = Tom Gilroy
| narrator       = 
| starring       = Ned Beatty Liev Schreiber Campbell Scott
| music          = Hahn Rowe
| cinematography = Terry Stacey James Lyons
| studio         = IFC Films
| distributor    = Metro-Goldwyn-Mayer
| released       = December 8, 2000
| runtime        = 110 minutes
| country        =  
| language       = English
| budget         = $2,000,000
| gross          = $102,353 (USA)
| preceded_by    = 
| followed_by    = 
}}
Spring Forward is a 2000 film and the directorial debut of Tom Gilroy, starring Ned Beatty, Liev Schreiber and Campbell Scott. Shot in sequence over the course of one year,  it was the first film released by IFC Films, the Independent Film Channels film production and distribution company.

MPAA Rating: R for language and some drug content.

==Plot==

Spring Forward is the story of the burgeoning friendship between two very different men, Murphy and Paul, who work for the parks department in a quaint New England town.  Paul is a short-tempered ex-convict recently released from prison for armed robbery, who is exploring his spiritual side hoping for atonement and a second chance. On his first day working in parks maintenance he is partnered with Murphy, a veteran groundskeeper, facing his advancing age and impending retirement. The day gets off to a rough start when Paul clashes with a condescending local business heir over a materials donation, loses his cool, announces he is quitting, and stalks off into the woods berating himself for always ruining everything.  Murphy follows him, defuses Pauls outburst and convinces him to stay.

Against the backdrop of the changing seasons the relationship between the men strengthens and develops more as father and son, poignant in contrast to Murphys relationship with his dying homosexual son, whose impending death causes Murphy to doubt his past as a father and a man.  Working together through mundane days and personal crisis they share their lives, hopes, ambitions and regrets, as Paul learns to find stability and calm in his life and Murphy achieves acceptance and forgiveness for his mistakes.

The film ends with Paul presenting Murphy with a hamsa as a retirement gift and to signify the "helping hand" that Murphy has provided him through the past year.

== Reception ==

The reviews for Spring Forward were mainly favorable, and it has achieved a rating of 86% on rottentomatoes.com.   It won third place mention for the Discovery Award honoring Best First Film at the 1999 Toronto film festival,  won the Gold award for Independent Theatrical Feature Films (Drama) at the 2000 WorldFest in Houston and the Producers Award at the 2001 Independent Spirit Awards. 

== References ==

 

==External links==
* 
*  

 