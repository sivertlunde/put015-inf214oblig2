Sometimes in April
{{Infobox film
| name = Sometimes in April
| image = Sometimes in april.jpg
| image_size =
| caption =
| director = Raoul Peck
| producer = Daniel Delume
| writer = Raoul Peck
| narrator =
| starring = Idris Elba Oris Erhuero Carole Karemera Debra Winger
| music = Bruno Coulais
| cinematography = Eric Guichard
| editing = Jacques Comets
| distributor = HBO Films
| released =  
| runtime = 140 min.
| country = France United States English Kinyarwanda
| budget =
}} historical drama television film about the Rwandan Genocide of 1994, written and directed by the Haitian filmmaker Raoul Peck. The ensemble cast includes Idris Elba, Oris Erhuero, Carole Karemera, and Debra Winger.

==Story== captain in Rwandan army (who was married to a Tutsi wo Yves-André, and Marcus), who bear witness to the killing of close to 800,000 people in 100 days while becoming divided by politics and losing some of their own family. The film depicts the attitudes and circumstances leading up to the outbreak of brutal violence, the intertwining stories of people struggling to survive the genocide, and the aftermath as the people try to find justice and reconciliation.

==Cast==
*Idris Elba ... Augustin Muganza
*Oris Erhuero ... Honoré Butera
*Carole Karemera ... Jeanne
*Debra Winger ... Prudence Bushnell
*Noah Emmerich ... Lionel Quaid
*Pamela Nomvete ... Martine
*Fraser James ... Xavier
*Abby Mukiibi Nkaaga ... Col. Théoneste Bagosora

==Discussions==
Although this film originally aired on HBO, it was later broadcast by PBS and followed with a panel discussion by journalist Jeff Greenfield. Paul Bonerwitz is one of the Orator|speakers.

In contrast to Hotel Rwanda, which was rated PG-13 and had most of the genocide violence  subtly implied rather than explicitly shown, this film was noted for its more gruesome and graphic portrayal of the violence, which gave it a TV-MA rating.

==See also==
* Hotel Rwanda, a 2004 film dealing with the genocide that centers on the Hôtel des Mille Collines, a location also seen in Sometimes in April. Shake Hands with the Devil, a 2007 film based on the book of the same name recounting  General Dallaires harrowing personal journey during the 1994 Rwandan Genocide and how the United Nations failed to heed Dallaires urgent pleas for further assistance to halt the massacre
* Shooting Dogs, a 2005 film centered on the École Technique Officielle in Kigali
* Rwandan Genocide

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 