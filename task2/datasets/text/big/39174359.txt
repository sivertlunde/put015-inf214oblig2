SDU: Sex Duties Unit
 
 
{{Infobox film
| name           = SDU: Sex Duties Unit
| image          = SDUSexDutiesUnit.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
 | traditional    = 飛虎出征
 | simplified     = 飞虎出征
 | pinyin         = Fēi Hǔ Chū Zhēng
 | jyutping       = Fei1 Fu2 Ceot1 Zing1
 | poj            = }}
| director       = Gary Mak
| producer       = Pang Ho-cheung Leung Kai Yun
| writer         = Jody Luk
| starring       = Chapman To Shawn Yue Matt Chow Derek Tsang
| music          = Wong Ngai Lun Janet Yung
| cinematography = Charlie Lam
| editing        = Wenders Li
| studio         = Sun Entertainment Culture Making Film Media Asia Distributions
| released       =  
| runtime        = 95 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          =
}} 2013 Cinema Hong Kong action comedy film directed by Gary Mak and starring Chapman To, Shawn Yue, Matt Chow and Derek Tsang. SDU: Sex Duties Unit was one of the films to be presented at the 2013 Hong Kong FILMART.  This film was released on 25 July 2013 in Hong Kong.

==Plot==
The Special Duties Unit (SDU) is an elite paramilitary tactical unit of the Hong Kong Police Force and is considered one of the worlds finest in its role. But being the best carries its own burdens. Like everyone else, they go through troubles with love, with family and with their jobs. And sometimes they get horny. 

This touching story is about Special Duties Unit Team B and their trip to Macau for a weekend of unadulterated debauchery. {{cite web|url=http://twitchfilm.com/2013/03/first-peek-at-pang-ho-cheungs-sdu-sex-duties-unit.html|title=
First Peek at Pang Ho Cheungs SDU: SEX DUTIES UNIT}} 

==Cast==
*Chapman To as Siu Keung
*Shawn Yue as Fu
*Matt Chow as Ka Ho
*Derek Tsang as Hai Mai
*Liu Anqi as Sai Sai
*Dada Chan as Siu Keungs ex-wife
*Jim Chim as procurer
*Siu Yam-yam as procurer
*Simon Lui as boatman
*Lam Suet as robber
*June Lam as prostitute
*Benz Hui as pharmacy owner
*JJ Jia as pharmacy owners daughter
*Lau Kong as Ka Hos father
*Pong Nan as procurers assistant Michael Wong SDU superior
*Ken Lo as Thief King To
*Joe Tay as jewelry store employee
*Lawrence Chou as Macau police sergeant
*Ken Wong as Macau police sergeant
*Tony Ho as Macau police constable

==Reception==
SDU: Sex Duties Unit earned HK$16,711,696 at the Hong Kong box office 

Andrew Chan of the Film Critics Circle of Australia writes, " “SDU: Sex Duties Unit” is certainly not as good as “Vulgaria” in terms of quality filmmaking, but it is most certainly outrageously funny and sometimes, that is precisely what the local audience demands. " 

It was released to Blu-Ray, DVD, VCD on December 5, 2013. 

==References==
 

==External links==
*  
* 
*  at HK Neo Reviews
*  at Hong Kong Cinemagic
*  at Chinesmov.com

 
 
 
 
 
 
 
 
 
 
 