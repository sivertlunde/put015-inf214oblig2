Return of the Kung Fu Dragon
 

{{Infobox film
| name           = Return of the Kung Fu Dragon
| image          =
| image_size     =
| caption        =
| director       = Chick Lim Yu
| producer       = Wen Ho Chen (producer) Hsiang Ping Liu (producer)
| writer         = Lee Ge-Sun (screenplay) Yu-Yen Lin (story)
| narrator       =
| starring       = See below
| music          = Mou Shan Huang
| cinematography =
| editing        = Yen Chu Lieh
| distributor    =
| released       = 1976
| runtime        =
| country        = Taiwan
| language       = Mandarin
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Return of the Kung Fu Dragon (砲傌俥 Mandarin:  Ju ma pao) is a 1976 Taiwan film directed by Chick Lim Yu.

== Plot summary ==
On the idyllic phoenix island in the south china sea a golden city becomes the birthplace to a new form of Kung Fu. Soon an evil tyrant threatens the peace and harmony of once such a proud city. A generation passes and a brave prince, a fearless princess and a foolish dwarf with the powers of invisibility team up to return a kingdom to its people as they wrest it from the hands of the evil tyrant.
 

== Cast ==
{| class="wikitable" border="1"
|-
! Actor
! Credited as
! Character
|-
| Polly Kuan 上官靈鳳
| Sun-Kuan Rin-Feng
| Ma Chan-Chen, the Black Girl
|-
| Nick Cheung Lik 張力
| Chang Li
| Che Kwan Yu 
|-
| Chan Sing 陈星
| Chen Hsin
| General Che Chan
|-
| Li Chung-Chien 李中堅
| Lee Chung-Gian
| Pao Ta-Hsiung 
|-
| Sze-Ma Yu-Chiao 司馬玉嬌
| Sma Yu-Chia
| Hsiao Yu, Princess of the Golden City
|-
| Tsai Hung 蔡弘
| Tsai Hon
| Evil Wizard
|-
| Tung Li 董力
| 
| General Black
|-
| Yuan Chuan 川原 
| Chuan Yun
| General Ma Tsen-Kung
|-
| Tien Yeh 田野
| Tien Yei
| Emperor of the Golden City
|-
| Chi Lin 林玑 
| Lin Gi
| Yen C, Princess mother
|-
| Hsiao Wang 小王
| 
| Hou Ping, midget servant
|-
| Tao Chen
| 
|
|-
| Chen Wen Ho 陳文和 
| 
|
|-
| Yuan Shen 原森
| 
| Hsiang Shou-Tien of Chi Pan Mountain
|- Yang Fang 楊芳
| 
| Evil Wizards apprentice
|-
| Eh Mao
| 
|
|-
| Lu Chiu Lung
| 
|
|-
| Ma Li Chu
| 
|
|}

== Soundtrack ==
 

== External links ==
* 
*  (dubbed in English)
*  

 
 
 
 
 


 