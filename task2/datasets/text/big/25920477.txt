Source Code
 
{{Infobox film
| name = Source Code
| image = Source Code Poster.jpg
| caption = Theatrical release poster
| director = Duncan Jones
| producer = {{Plainlist|
* Mark Gordon
* Jordan Wynn
* Philippe Rousselet}}
| writer = Ben Ripley
| starring = {{Plainlist|
* Jake Gyllenhaal
* Michelle Monaghan
* Vera Farmiga Jeffrey Wright}}
| music = Chris P. Bacon Don Burgess Paul Hirsch
| studio = {{Plainlist| The Mark Gordon Company
* Vendôme Pictures}}
| distributor = Summit Entertainment
| released =  
| runtime = 93 minutes
| country = {{Plainlist|
* France 
* United States   }}
| language = English
| budget = $32 million   
| gross = $147.3 million    	 
}} science fiction Jeffrey Wright. The film had its world premiere on March 11, 2011, at South by Southwest,  and was released by Summit Entertainment on April 1, 2011 in North America and Europe. The film was a co-production of France and the United States.

Source Code received positive reviews from film critics and became a box office success, grossing over $147 million worldwide.  

==Plot==
 
 ) and the bathroom mirror – he appears to be Sean Fentress, a school teacher. As he comes to grips with this revelation, the train explodes, killing everyone aboard.
 Jeffrey Wright), alternative timeline. quantum effect light bulb being switched off, allowing this period to be retroactively accessed for some time after the target persons death as a way of gleaning information critical to the prevention of additional, near-future terrorist attacks. It is believed that these alternative time-lines are not "real" and cease to continue after the eight-minute time limit; they can therefore supposedly be employed only to gain information. 

Stevens is unwillingly sent back into the Source Code again and again in frustrating, exhausting attempts to learn the bombers identity. He tries to warn authorities on the train and flee with Christina, escaping the explosion. Other times he cannot locate or disarm the bomb and dies on the train. But Rutledge insists the alternate timeline is not real. It is revealed that he has been "with them" for two months since being reported killed in action in Afghanistan. He is comatose and missing most of his body, hooked up to neural sensors. The cockpit capsule is in his imagination, his brains way of making sense of a missing environment. A confused and frustrated Stevens asks, "As one soldier to another, am I dead?" Angry to learn that he is on life support, he asks to be disconnected after the mission. Rutledge agrees.

Stevens catches the bomber Derek Frost (Michael Arden), who leaves his wallet behind to fake his own death, and gets off at the last stop before Chicago. In one run-through, Frost kills both Fentress and Christina, and flees in a rented white van. Stevens remembers the license number and direction so the authorities can catch the terrorist. But Rutledge reneges, orders Goodwin to wipe Stevens memory for a future mission. Stevens convinces Goodwin to allow one more try, to save everyone on the train, despite Rutledges insistence that everyone on the train had already been killed in the explosion.

Stevens is sent back into the Source Code where he disarms the bomb, subdues Frost and handcuffs him to a handrail inside the train. He reports the bomber and the bomb to authorities, composes an email to Captain Goodwin, then calls to reconcile with his estranged father under the guise of a fellow soldier. He asks Christina what she would do if she knew that she only had seconds left to live, and starts to kiss her. At the same time, Goodwin approaches the air-tight chamber with the torso of Stevens comatose mutilated body, and disconnects the life support. Rutledge bangs on the outer door in vain. Surprisingly, Stevens finishes the kiss with Christina, revealing that the alternate timeline of the Source Code was indeed real, contrary to what was proposed by Rutledge. They continue on the train, and then walk around downtown Chicago facing the Cloud Gate.

Later that morning, the alternative-timeline Captain Goodwin arrives for work at Nellis Air Force Base and receives the email from Stevens. While news breaks about the failed bomber on the Chicago train, he informs Goodwin that they have changed history, and Goodwin seemingly recalls something. He asks her to reassure this timelines Stevens that "everything is gonna be okay."

==Cast==
* Jake Gyllenhaal as Capt. Colter Stevens
* Michelle Monaghan as Christina Warren
* Vera Farmiga as Capt. Colleen Goodwin Jeffrey Wright as Dr. Rutledge
* Michael Arden as Derek Frost
* Russell Peters as Max Denoff
* Scott Bakula as Donald Stevens, Colters father  
* Frédérick De Grandpré	as Sean Fentress  
* Cas Anvar as Hazmi

==Production==

===Pre-production===
David Hahn, the boy depicted in the 2003 made-for-television documentary The Nuclear Boy Scout,  was the inspiration for the antagonist Derek Frost.  In an article published by the Writers Guild of America, screenwriter Ben Ripley is described as providing the original pitch to the studios responsible for producing Source Code: 

 

After seeing Moon (film)|Moon, Gyllenhaal lobbied for Jones to direct Source Code; Jones liked the fast-paced script; as he later said: "There were all sorts of challenges and puzzles and I kind of like solving puzzles, so it was kind of fun for me to work out how to achieve all these difficult things that were set up in the script." 

In the ending scene, Jake Gyllenhaals and Michelle Monaghans characters are seen walking through Millennium Park, and make their way to the Cloud Gate. In a 2011 interview, Gyllenhaal discussed how director Duncan Jones felt the structure was a metaphor for the movies subject matter, and aimed for it to feature at the beginning and end of the movie.  

===Filming===
Principal photography began on March 1, 2010, in Montreal, Quebec, and ended on April 29, 2010.  Several scenes were shot in Chicago, Illinois, specifically at Millennium Park and the Main Building at the Illinois Institute of Technology, although the sign showing the name of the latter, in the intersection of 31st Street and S LaSalle Street, was edited out. Initially, some filming was scheduled at the Ottawa Train Station in Ottawa, Ontario,  but cancelled for lack of an agreement with VIA Rail. 

===Post-production===
Editing took place in Los Angeles. In July 2010, the film was in the visual effects stage of post-production.  Most of the VFX work was handled by Montreal studios, including Modus FX, Rodeo FX, Oblique FX, and Fly Studio.  Jones had confirmed that the films soundtrack would be composed by Clint Mansell, in his second collaboration with the composer.  However, it was later announced that Mansell would no longer score the movies soundtrack due to time constraints.   

==Release==

===Box office performance===
{| class="wikitable" style="width:100%;"
|-
! rowspan="2" | Film
! colspan="1" | Release date
! colspan="3" | Box office revenue
! colspan="2" text="wrap" | Box office ranking
! rowspan="2" style="text-align:center;"| Budget
! rowspan="2" style="text-align:center;"| Reference
|-
! United States
! United States
! International
! Worldwide
! All time United States
! All time worldwide
|-
| Source Code
| style="text-align:center;"| April 2011
| style="text-align:center;"| $54,712,227
| style="text-align:center;"| $92,620,470	
| style="text-align:center;"| $147,332,697
| style="text-align:center;"| #1095
| style="text-align:center;"| Unknown
| style="text-align:center;"| $32,000,000
| style="text-align:center;"| 
|}

Source Code was released in theaters on April 1, 2011. In the United States and Canada, Source Code was released theatrically in 2,961 conventional theaters.    The film grossed $54,712,227 during its run with midnight screenings in 2,961 locations.  Overall the film made $147,812,094 and debuted at #2 on its opening weekend. 

===Critical reception===
{| class="wikitable" style="width:99%;"
|-
! Film
! Rotten Tomatoes
! Metacritic
|-
| Source Code
| style="text-align:center;"| 92% (243 reviews)   
| style="text-align:center;"| 74/100 (41 reviews) 
|}

Source Code received critical acclaim from critics.   awarded the film an average score of 74/100 based on 41 reviews.  Critics have compared Source Code with both the 1993 film  ,"    while The Arizona Republic film critic Bill Goodykoontz says that comparing Source Code to Groundhog Day is doing a disservice to Source Code  enthralling "mind game." 

   gave the film 3.5 stars out of 4, calling it "an ingenious thriller" where "you forgive the preposterous because it takes you to the perplexing."  Kenneth Turan of the Los Angeles Times called Ben Ripleys script "cleverly constructed" and a film "crisply directed by Duncan Jones." He also praised the "cast with the determination and ability to really sell its story."  CNN called Ripleys script "ingenious" and the film "as authoritative an exercise in fractured storytelling as Christopher Nolans Memento (film)|Memento." He also commented that Gyllenhaal is "more compelling here than hes been in a long time."  IGN gave it 2.5 stars out of 5, saying: "Gyllenhaal brings sincerity and warmth to his role, but his conviction only helps the movie so far before it ultimately buckles under the weight of its plot mechanics."

===Accolades===
{| class="wikitable"
! Year !! Group !! Category !! Recipient !! Result 
|- 
| 2011  Scream Awards 
| Best Science Fiction Actor 
| Jake Gyllenhaal 
|   
|- 
| 2011 
| Bradbury Award  
| Bradbury Award  Ben Ripley and Duncan Jones 
|   
|- 
| 2012 
| Hugo Award   Best Dramatic Presentation, Long Form
|   
|}

===Home media===
Source Code was released on DVD and Blu-ray Disc simultaneously in the US on July 26, 2011,   with the UK release on DVD and Blu-ray Disc (as well as a combined DVD/Blu-ray Disc package) on August 15, 2011.  In the UK, there was also a DVD released featuring a 3D cover.

==Sequel==
A planned television series for the network CBS was announced in 2011,    and was revealed to be in development on January 4, 2012, with Mark Gordon and Steve Maeda as producers.  The planned television series was canceled in December 2014 and, instead, it was announced that a film sequel is in development. The film will incorporate ideas originally intended for the television series. 

==See also==
*List of films featuring time loops

==Notes==
 

==References==
 

==External links==
*   
*  
*  
*  
*   at Metacritic
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 