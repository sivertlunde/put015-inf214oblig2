Deepavali (2000 film)
{{Infobox film
| name = Deepavali
| image =
| caption = 
| director = Dinesh Babu
| writer = Dinesh Babu
| producer = S. Shankar Bhavana
| music = M. M. Keeravani
| cinematography = Dinesh Babu
| editing = Nagesh Yadav
| studio  = Shri Shankari Productions
| released = 2000
| runtime = 145 min
| language = Kannada  
| country = India
}}
 Bhavana in the lead roles. The film is directed and written by Dinesh Babu. 

==Cast== Vishnuvardhan 
* Ramesh Aravind Bhavana
* Chandini
* Ragasudha
* Ramesh Bhat
* K. R. Vijaya
* Shivaram
* Kashi
* Tennis Krishna
* Hema Chowdhary

==Release==
The film was released in March 2000 all over Karnataka. The film met with average and negative critical response at the box office. It did, however, run for 100 days in a few cinema halls.

==Soundtrack==
All the songs are composed and scored by M. M. Keeravani. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyrics
|- Nanditha || K. Kalyan
|-
| 2 || "Mukkoti Suryaniva" || S. P. Balasubramanyam, K. S. Chithra || K. Kalyan
|-
| 3 || "Harusha Devige" || S. P. Balasubramanyam || M. N. Vyasa Rao
|-
| 4 || "Shruti Layada Jothe" || S. P. Balasubramanyam || Rudresh Nagasandra
|- Nanditha || Rudramurthy Shastry
|-
| 6 || "Bhanu Varusha" || M. M. Keeravani, Ganga Sitharasu || M. N. Vyasa Rao
|-
|}

==References==
 

== External links ==
* 

 
 
 
 
 
 


 

 