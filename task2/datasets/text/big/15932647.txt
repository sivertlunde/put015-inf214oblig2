The Radio Detective
 
{{Infobox film
| name           = The Radio Detective
| image	         = The Radio Detective FilmPoster.jpeg
| caption        = Film poster
| director       = William James Craft William A. Crinley
| producer       = 
| writer         = Arthur B. Reeve
| starring       = Jack Dougherty Margaret Quimby
| music          = 
| cinematography = 
| editing        = 
| distributor    = Universal Pictures
| released       =  
| runtime        = 10 episodes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 adventure film serial directed by William James Craft and William A. Crinley and released by Universal Pictures. The film is considered to be lost film|lost.   

==Cast==
* Jack Dougherty - Easton Evans (as Jack Daugherty)
* Margaret Quimby - Ruth Evans
* Jack Mower - Craig Kennedy
* Wallace Baldwin - Hank Hawkins, Crook
* Howard Enstedt - Ken Adams, Policeman
* John T. Prince - Professor Ronald Varis
* Florence Allen - Rae Varis
* Sammy Gervon - Crook
* Buck Connors
* George Williams
* Monte Montague

==List of episodes==
*Chapter 1: The Kick Off! 
*Chapter 2: The Radio Riddle 
*Chapter 3: The Radio Wizard 
*Chapter 4: Boy Scout Loyality 
*Chapter 5: The Radio Secret 
*Chapter 6: Fighting For Love 
*Chapter 7: The Tenderfoot Scout 
*Chapter 8: The Truth Teller 
*Chapter 9: The Fire Fiend 
*Chapter 10: Radio Romance

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 