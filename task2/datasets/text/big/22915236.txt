Wings of the Navy
{{Infobox film
  | name           = Wings of the Navy
  | image          = Wings of the Navy - 1939 - poster.png
  | caption        = 1939 Theatrical poster
  | director       = Lloyd Bacon
  | producer       = Louis F. Edelman
  | writer         = Michael Fessier
  | narrator       = John Payne
  | music          = Heinz Roemheld
  | cinematography = Arthur Edeson
  | editing        = George Amy
  | distributor    = Warner Bros. Pictures
  | released       = February 11, 1939
  | runtime        = 89 min
  | country        = USA
  | language       = English
  | budget         =
  | gross          =
  | preceded_by    =
  | followed_by    =
}}
 Warner Bros. drama directed John Payne. Like many of the Warner Bros. features in the pre-World War II era, it was intended to serve as propaganda for the U.S. military and received heavy support from the US Navy which also considered the film as a recruiting tool.

==Plot==
Submarine officer Jerry Harrington (John Payne) goes to Pensacola to train as a flying cadet, just like his father and his brother, longtime airman Cass Harrington (George Brent). Jerry ends up falling for his brothers girlfriend, Irene Dale (Olivia de Havilland), which only increases the competition between the two brothers. After Cass is seriously injured in a crash, he is forced to leave the Navy. Jerry becomes a pilot in San Diego and begins flying seaplanes while Cass designs a new fighter for the Navy. Jerry wants to prove to Cass that he is a better pilot, even if it means leaving the Navy to test the experimental fighter which has already led to the death of a test pilot. Irene is forced to choose which man she loves. 

==Cast==
* George Brent as Cass Harrington
* Olivia de Havilland as Irene Dale John Payne as Jerry Harrington
* Frank McHugh as Scat Allen
* John Litel as Commander Clark
* Victor Jory as Lt. Parsons
* Henry ONeill as Prologue Speaker
* John Ridgely as Dan Morrison
* John Gallaudet as Lt. Harry White
* Donald Briggs as Instructor
* Edgar Edwards as Ted Parsons
* Regis Toomey as First Flight Instructor
* Alberto Morin as Armando Costa (as Albert Morin)
* Jonathan Hale as Commandant
* Pierre Watkin as Capt. March

==Production==
Wings of the Navy was filmed on location at the Naval Air Station on North Island in San Diego, California, and the Naval Air Station at Pensacola, Florida and was dedicated to the U.S. Naval Aviation Service. The US Navy was heavily committed by providing access to aircraft and facilities with Lieutenant Commander Hugh Sease serving as the Technical Advisor to the production. The latest US Navy types were on display including the Grumman F3F biplane fighter which played the role of an experimental fighter.
 
George Brent, John Payne and Olivia de Havilland reprising their film roles, performed a 60-minute radio adaptation of the movie on "Lux Radio Theater" broadcast on October 7, 1940.

==Reception==
Typical of other period Warner Bros. dramas, it  was a propaganda when it was released in 1939, before the U.S. involvement in World War II. 

The most impressive aspect of the film was the flying sequences which a reviewer at The New York Times aptly reported was "As a documentary study of the Pensacola Naval Air Training station, and its methods of turning raw recruits into seasoned pilots of combat and bombing planes, "Wings of the Navy" gets off the ground very nimbly, and has a good deal of value, interest and even excitement, of the purely mechanical sort, to offer to the curious." 

==References==
;Notes
 
;Bibliography
 
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies." The Making of the Great Aviation Films. General Aviation Series, Volume 2, 1989.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorn, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 