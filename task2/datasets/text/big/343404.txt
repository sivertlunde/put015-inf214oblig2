Withnail and I
 
 
{{Infobox film
| name           = Withnail and I
| image          = Withnail and i poster.jpg
| image_size     =
| alt            = 
| caption        = Original UK release poster Art by Ralph Steadman
| director       = Bruce Robinson
| producer       = Paul Heller
| writer         = Bruce Robinson
| starring       = {{Plainlist|
*Paul McGann
*Richard E. Grant
*Richard  Griffiths}}
| music          = {{Plainlist| David Dundas
*Rick Wentworth}} Peter Hannan
| editing        = Alan Strachan
| studio         = HandMade Films
| distributor    = {{Plainlist|
*HandMade Films (UK)
*Cineplex Odeon Films (US)
*The Cannon Group (US)}}
| released       = 1987
| runtime        = 107 minutes   
| country        = United Kingdom
| language       = English
| budget         = GBP 1.1 million
| gross          = {{Plainlist|
*USD 1,544,889 (USA)
*GBP 565,112 (UK)
*AUD 103,117 (Australia) }}
}}
Withnail and I is a 1987 black comedy film written and directed by Bruce Robinson. Based on Robinsons life in London in the late 1960s, the plot follows two unemployed young actors, Withnail and "I" (portrayed by Richard E. Grant and Paul McGann) who live in a squalid flat in Camden Town in 1969 while waiting for their careers to take off. Needing a holiday, they obtain the key to a country cottage in the Lake District belonging to Withnails lecherous gay uncle Monty (Richard Griffiths) and drive there. The holiday is less recuperative than they expected.

Withnail & I was Grant’s first film and launched him into a successful career. The film also featured performances by  ) and is notable for its period music and many quotable lines. It has been described as "one of Britains biggest cult films". 

==Plot== slings and Georgian flat pubs to open so they can drink and be somewhere with heating. Their only other company at the flat besides each other is the local drug dealer, Danny; a somewhat distasteful man with far out, often bizarre viewpoints on the current state of affairs and a knack for irritating Withnail.

The film begins with Marwood smoking in the darkened flat. When he has finished he goes to a café and reads disturbing articles in a newspaper. Marwood suffers an anxiety attack and returns home, where Withnail complains about having recently run out of wine, ignoring Marwoods plight and reading to him from the newspaper, rambling as he does so. Withnail becomes annoyed with Marwood drinking coffee from a bowl with a spoon, and Marwood retorts that they have no more clean cups, backhandedly complaining that Withnail never does the washing up, which angers Withnail further. Following an abortive attempt by the pair to do the washing up, Withnail decides that they should go for a walk. 

Out in Regents Park, Withnail complains about his lack of success, pointing out that Marwood has recently had an audition while he himself has had nothing close to work, to which Marwood offers vague reassurances, claiming that things will get better. Withnail also complains of feeling unwell, so Marwood suggests they go away to the countryside to convalesce, though Withnail is sceptical of the idea, and is quick to point out that they lack the funds to follow through with the idea. Soon after, whilst in a pub, Marwood remembers Withnails uncle Monty, who has a countryside cottage near Penrith, and Withnail agrees to call him, arranging to visit that evening. Marwood suffers another attack of anxiety in the pub after being insulted by a large and belligerent Irishman, and Withnail is quick to distance himself from Marwood in the hopes of avoiding being attacked himself, but the pair flee together when becomes clear that Withnail is as much a target of the mans rage as Marwood. Back in the flat, Withnail and Marwood prepare for their visit to Monty, while their drug dealer Danny has come over to keep out of the cold. Though Marwood tells Danny that theyre not looking to buy anything, Withnail quickly gets into an argument with Danny over who could take the most drugs, though nothing comes of it as Withnail refuses to buy drugs off of Danny to prove himself. 
 Chelsea house West House, the Dane", to which Withnail claims that Hamlet is the part he aims to play. Monty is an old boy of Harrow School, and it is suggested that Withnail is one too, and when asking where Marwood schooled, Withnail again lies, telling Monty that Marwood went to "the other place" (i.e. Eton College|Eton). Shortly becoming distressed by his cat, Monty asks the two to leave, but Withnail asks for a private word, and reappears shortly to Marwood with the key to the country cottage. Withnail defends his lie about Eton as being necessary to get the cottage.

Withnail and Marwood get into Marwood’s battered   The Seagull|Konstantin, which he refuses to do, demanding to know why he cant play the part. Withnails anger turns to fear the following day, when Withnail and Marwood see Jake prowling around their cottage as theyre returning from a walk. Marwood suggests they leave for London the following day, and Withnail in turn demands that they share a bed that night, in the interest of safety, though Marwood refuses. Despite this, during the night, a frightened Withnail climbs in to the guest bedrooms single bed with Marwood, which prompts Marwood to angrily leave, and take the master double bed, until the sounds of an intruder breaking into the cottage prompt Withnail to again follow Marwood, who this time does not rebuff Withnail. The two are terrified, believing the trespasser to be Jake, but it soon turns out to be Monty, who has been stranded for "an aeon" with a punctured tyre, and finds the two of them in bed together. 

Marwoods relief is short-lived, for although Monty brings them ample supplies of food and wine, and sorely needed expertise at surviving in a countryside cottage, his designs are quickly made clear, and he persistently flirts with Marwood throughout the following day, much to Marwoods discomfort. Marwood tells Monty that he and Withnail must leave for London after dinner, and tells Monty that its at Withnails insistence, and although Withnail initially agrees, over dinner he informs Monty that he intends to stay over the weekend, to Montys delight, and Marwoods chagrin. Marwood tries to insist on leaving, bringing up their fear of Jake, though in front of Monty, Withnail laughs off their earlier terror. The three go for a walk after dinner, and aside from Monty, Marwood demands to have the master bedroom that night, which has a lock, and Withnail agrees, but on the way back, the three spy Jake from afar, again prowling around the cottage. Marwood is elated to see Withnail again afraid, glad of the pretext for returning to London, but is once again disappointed, when upon returning to the cottage they find that Jake has merely left them a poached hare to eat. 
 toilet trader". Withnail had also claimed that Marwood was in love with him, but he had turned him down. Marwood turns this around, claiming that Withnail is the ashamed and closeted one, and that the two of them have been in a committed relationship for years. He claims that Withnail is only rejecting him because Monty is around, and that this is the first night that they havent slept together in years, and that he cannot bring himself to cheat in him. Monty, who believes in love and loyalty, accepts this excuse as the whole truth and apologizes for coming between them. Marwood wakes Withnail, enraged, and demands the master bedroom to himself. Withnail admits to lying to Monty about Marwoods sexuality, calling it a "calculated risk" saying it was necessary to get the cottage, and that he didnt expect Monty to travel so far to be with them. 

The next morning, Marwood finds that Monty left for London in the night, and wrote a gracious note of apology and reads it aloud, feeling sympathy for Monty, who writes that he hopes Withnail and Marwood find happiness together. Withnail however, takes no responsibility for the chaos he has caused and is uninterested, commending Montys taste in wine, and musing on Marwoods hypocrisy in having to lie to Monty about their sexuality as much as he himself did. Marwood begins to distance himself from his friend, and promises that Withnail will to pay for what he has done. Marwood then receives a telegram that confirms that he has a call-back from his prior audition, and he insists that they go back to London immediately despite Withnails reluctance.
 driving while cannabis joint). Marwoods celebration is abruptly ended when he finds that Danny has been taking their mail, and learned that they have received an eviction notice due to unpaid rent, which Withnail is too high to care about. 
 wolves behind a fence in the adjoining London Zoo, his dreams shattered as he finally realizes that he himself will "never play the Dane".  The camera remains still as he turns and walks away into the gloomy distance, swinging the bottle, as the credits start to roll.

==Cast==
* Richard E. Grant as Withnail
* Paul McGann as "...& I"
* Richard Griffiths as Uncle Monty
* Ralph Brown as Danny
* Michael Elphick as Jake
* Eddie Tagoe as Presuming Ed
* Daragh OMalley as Irishman
* Michael Wardle as Isaac Parkin
* Una Brandon-Jones as Mrs. Parkin
* Noel Johnson as General

==Production==
The film is an adaptation of an unpublished novel written by Robinson in late 1969. Actor friend Don Hawkins passed a copy of the manuscript to his friend, the wealthy oil heir Moderick Schreiber in 1980. Schreiber, looking to break into the film industry, paid Robinson a few thousand pounds sterling to adapt it into a screenplay, which Robinson did in the early 1980s. On completing the script, producer Paul Heller urged Robinson to direct it and found funding for half the film. The script was then passed to HandMade Films. After he read it, George Harrison agreed to fund the remainder of the film. 

Robinsons script is largely autobiographical. "Marwood" is Robinson; "Withnail" is based on Vivian MacKerrell, a friend with whom he shared a Camden house; and "Uncle Monty" is loosely based on Franco Zeffirelli from whom Robinson received unwanted amorous attentions when he was a young actor.  He lived in the impoverished conditions seen in the film and wore plastic bags as Wellington boots. For the script, Robinson condensed four or five years of his life into two weeks.

The narrative is told in the first person by the character played by Paul McGann, named just once in passing in the film (see below) as Marwood, and only credited as "I".
 sexually abusing Romeo and Juliet. 

The end of the novel saw Withnail committing suicide by pouring a bottle of wine into the barrel of Montys gun and then pulling the trigger as he drank from it. Robinson changed the ending, as he believed it was "too dark." 
 Denis OBrien, who oversaw the filming on behalf of HandMade Films, nearly shut the film down three days into the shoot. He thought that the film had no "discernible jokes" and was badly lit. 

The film cost £1.1 million to make. Robinson received £80,000 to direct, £30,000 of which he reinvested into the film to shoot additional scenes such as the journeys to and from Penrith, Cumbria|Penrith, which HandMade Films would not fund. He was never reimbursed his money after the films success. 

===Casting=== Liverpool accent was wrong for the character. Several other actors read for the role, but McGann eventually persuaded Robinson to re-audition him, promising to affect a Home Counties accent. He quickly won back the part. 

Actors who were considered for the part of "Withnail" included Daniel Day-Lewis, Bill Nighy and Kenneth Branagh.    Robinson claims that he told Richard E. Grant that "half of you has got to go", and put him on a diet to play the part   although Grant denies this in the 1999 documentary "Withnail and Us".
The role of Withnail was Grants first in film and launched him into a successful career.

Though playing a raging alcoholic, Grant himself is a teetotaller with a health condition preventing him from properly processing alcohol. He had therefore never been drunk prior to making the film. Robinson decided that it would be impossible for Grant to play the character without having ever experienced inebriation and a hangover, and thus "forced" the actor on a drinking binge. Grant has stated that he was "violently sick" after each drink, and found the experience on a whole deeply unpleasant. 

During the filming of the scene in which the lighter fluid is consumed, Robinson changed the contents of the can, which had been filled with water, to vinegar. While the vomiting is scripted, the facial expression is totally natural. 

===Filming===
 , the location used as Montys cottage]]
The film was not shot entirely on location. There was no filming in the real Penrith, Cumbria|Penrith, the locations used were in and around nearby Shap and Bampton, Cumbria|Bampton. Montys cottage, "Crow Crag", is actually Sleddale Hall, located near the Wet Sleddale Reservoir just outside Shap, although the lake that "Crow Crag" apparently overlooks is actually Haweswater Reservoir.

Sleddale Hall was offered for sale in January 2009;  a trust has been created by fans who wish to collectively purchase the building for its preservation as a piece of British film history. It was sold at auction for £265,000 on 16 February 2009. The starting price was £145,000. It was bought by Sebastian Hindley, who owns the Mardale Inn in the nearby village of Bampton, which did not feature in the film. Hindley was unable to raise the necessary finances and in August 2009 the property was resold for an undisclosed sum to Tim Ellis, an architect from Kent, whose original bid failed at the auction. 

The bridge where Withnail and Marwood go fishing is located at the bottom of the hill below Sleddale Hall, a quarter of a mile away. The telephone box where Withnail calls his agent is beside the main road in Bampton. 

;Hertfordshire
Although exterior and ground floor interior shots of Crow Crag were shot at Sleddale Hall, Stockers Farm in Rickmansworth was used for the bedroom and stair scenes. Stockers Farm was also the location for the "Crow and Crown" pub.

;Milton Keynes
The "King Henry" pub and the "Penrith Tea Rooms" scenes were filmed in the Market Square in Stony Stratford, Milton Keynes at what is now the "Crown Inn" and Cox & Robinsons Chemists.

;London West House, Glebe Place, Chelsea, London|Chelsea, SW3.

;Shepperton Studios
Police Station interior was shot at the studios.

=="I" s name==
 
Although the first name of I is not stated anywhere in the film, it is widely believed that it is Peter. This myth arose as a result of a line of misheard dialogue.  In the scene where Monty meets the two actors, Withnail asks him if he would like a drink. In his reply, Monty both accepts his offer and says "...you must tell me all the news, I havent seen you since you finished your last film".  While pouring another drink, and downing his own, Withnail replies that he has been "Rather busy uncle. TV and stuff". Then pointing at Marwood he says "Hes just had an audition for rep". Some fans hear this line as "Peters had an audition for rep", although the original shooting script and all commercially published versions of the script read "hes".

The "I" characters name is given as Marwood in the original screenplay. It has been suggested that it is possible that Marwood can be heard near the beginning of the film: As the characters escape from the Irishman in the Mother Black Cap, Withnail shouts "Get out of my way!". Some hear this line as "Out of the way, Marwood!", although the script reads simply "Get out of my way!".

There is, however, one occasion in the film where the name Marwood is given, though not stated. Toward the end of the film a telegram arrives at Crow Crag and as Withnail reads the note, the name Marwood appears to be visible, upside-down, on the envelope. I is now widely accepted as Marwood, as this was the name that was used in the script of Withnail and I, but due to the fact that the story is told from Marwoods point of view, he is considered as I. In the end credits and most media relating to the film, McGanns character is referenced solely as "...& I."

However, in the supplemental material packaged with the Special Edition DVD in the UK, McGanns character is referred to as Peter Marwood in the cast credits.

==Reception==
The film had a UK gross of £565,112 and a US gross of $1,544,889. DVD and VHS sales have been quite strong throughout the years, and the film has gained cult status with a number of websites dedicated to the film itself. In 2000, readers of Total Film voted Withnail and I the third greatest comedy film of all time. In 2004 the same magazine named it the 13th greatest British film of all time. Withnail & I was 38th in Channel 4s 100 Greatest Films poll.

The film holds a 93% "fresh" rating, and an average rating of 8.3 out of 10 from critic website Rotten Tomatoes.  In August 2009 The Observer polled 60 eminent British film filmmakers and film critics who voted it the second best British film of the last 25 years.    The film was also ranked number 118 in Empires 500 Greatest Films of all Time list. In 2009 critic Roger Ebert added the film to his "Great Movies" list, describing Grants performance as a "tour de force" and Withnail as "one of the iconic figures in modern films." 

In 2007, a digitally remastered version of the film was released by the UK Film Council. It was shown at over fifty cinemas around the UK on 11 September, as part of the final week of the BBC|BBCs "Summer of British Film" season. 

==Legacy==
British Shoegaze band Ride (band) utilized clips from the movie for their song "Cool Your Boots" off their second album Going Blank Again. The title itself comes from one of Dannys lines who tells Withnail, "Cool your boots, man".

There is a drinking game associated with Withnail and I.    The game consists of keeping up, drink for drink, with each alcoholic substance consumed by Withnail over the course of the film.       All told, Withnail is shown drinking roughly nine and a half glasses of red wine, half a pint of cider, one shot of lighter fluid (vinegar or overproof rum are common substitutes), two and a half shots of gin, six glasses of sherry, thirteen measures of whisky and half a pint of ale. 

Withnail and I is referenced in the lyrics of the Kings of Leon song "King of the Rodeo". 
 Endeavour Series 2 Episode 1 when Endeavour visits London, there is a plaque on the front of a building stating "R. Duck - Theatrical Agent. 4th Floor".  This is a reference to Montague (Uncle Monty) H Withnails agent, Raymond Duck, who resided on the 4th floor on the Charing Cross Road .  "I remember my first agent. Raymond Duck. A dreadful little Israelite. Four floors up on the Charing Cross Road and never a job at the top of them". - Uncle Monty
 ITV soap opera Coronation Street during an episode broadcast 1st June 2014, in the aftermath of the attack on Tina MacIntyre, Peter Barlow is sitting on the floor of this flat half drunk.  When questioned about his sobriety he replies in a faked drunken slur "I can assure you officer Ive only had a few light ales" paraphrasing Withnails line "I assure you Im not officer, honestly, Ive only had a few ales"
 Smiths record. Its provenance is from a different era. None of the production values, none of the iconography, none of the style remotely has it down as an 80s picture." 

==Home media==
The film has been released in several countries world wide.

;Australia
A Region-4 DVD was released by Umbrella Entertainment in Australia in 2009 in the form of a 2-disc special edition featuring extras including two audio commentaries and two documentaries Withnail & Us and Postcards from Penrith. The film ran for 107 minutes and was a 16:9 widescreen version.  The same firm released the film on Blu-ray in 2010   as well as a cheaper single-disc DVD Vanilla edition (featuring the film but with no extras) in 2012. 

;Canada
The first DVD edition of the film was a   pan and scan version released in Canada by Seville Pictures. The film ran to 104 minutes. Although the sleeve claimed that the original cinema trailer was included as an extra, it was omitted from the disc. At the time the sleeve was printed, Seville believed they had access to the trailer but later discovered it was not in their library.

;United States
The second DVD release of the film was in North America as part of the  .

;United Kingdom (1st Edition) artefacts as a result. Like the North American release, it was also letterboxed. This edition was later re-released by Anchor Bay in February 2007.

;United Kingdom (2nd Edition)
The second UK release was a budget edition by Anchor Bay in 2005, under their Bay View label. It featured an un-remastered version of the film, identical to the original cinema release in 1987 (later editions of film had several minutes of cut footage reinstated). No extras were included.

;United Kingdom (20th Anniversary Edition)
The third UK release, again from Anchor Bay, came in 2006 to coincide with the films 20th Anniversary. For this three-disc release the film was remastered in high definition and released for the first time in anamorphic. It included all the extra features from the first UK edition, plus an additional commentary by Bruce Robinson, a featurette on the Drinking Game, a brand new interview with Bruce Robinson and a locations featurette called Postcards from Penrith.  A bonus CD was also included, featuring all of the music specially composed for the film, because the soundtrack was no longer in print and had become rare.

;Free copy
A DVD of the film was given away with the Sunday Times on 14 June 2009 to celebrate 40 years since Robinson first conceived the idea. The Blu-ray trailer was also included.

;Germany
One of very few releases (if not the only) of the film outside anglophone countries. The DVD features besides the original English audio track a German dubbed one (stemming from a TV screening from the mid 80s) and several extras from the UK releases, such as the audio commentary by Bruce Robinson.

;Download
On 31 July 2007 Channel 4 put the entire film up online as part of their 4oD video-on-demand service. It was available to download free of charge from 4oD until 12 August 2007 after which a fee was chargeable. 

==Soundtrack==
The film also features a rare appearance of a recording by The Beatles, whose song "While My Guitar Gently Weeps" briefly plays as Marwood and Withnail return to London and find Presuming Ed in the bath. Although the surviving members of the group rarely licensed the use of their original recordings for feature films (cover versions were often substituted, as in the case of The Royal Tenenbaums and I Am Sam), George Harrison was one of the films producers, and allowed its inclusion in Withnail & I.

There is a misheld belief among some fans of the film that King Curtis was murdered on the night his live performance of "A Whiter Shade of Pale" was recorded.   Ralph Brown, in the audio commentary on some DVD issues, wrongly states that he was shot in the car park after the concert. Curtis was stabbed to death in August 1971, some five months after the recording was made in March 1971. The recording comes from Curtiss album Live at Fillmore West. 

# "A Whiter Shade of Pale (live)"&nbsp;– King Curtis – 5:25 David Dundas and Rick Wentworth – 1:33
# "All Along the Watchtower (reduced tempo)"&nbsp;– Jimi Hendrix – 4:10
# "To the Crow"&nbsp;– David Dundas and Rick Wentworth – 2:22
# "Voodoo Child (Slight Return)" (live)" &nbsp;– Jimi Hendrix – 4:28
# "While My Guitar Gently Weeps" &nbsp;– The Beatles – 4:44
# "Marwood Walks"&nbsp;– David Dundas and Rick Wentworth – 2:14
# "Monty Remembers"&nbsp;– David Dundas and Rick Wentworth – 2:02
# "La Fite"&nbsp;– David Dundas and Rick Wentworth – 1:10
# "Hang Out the Stars in Indiana"&nbsp;– Al Bowlly and New Mayfair Dance Orchestra – 1:35
# "Crow Crag"&nbsp;– David Dundas and Rick Wentworth – 0:56
# "Cheval Blanc" – David Dundas and Rick Wentworth – 1:15
# "My Friend" – Charlie Kunz – 1:28
# "Withnails Theme" – David Dundas and Rick Wentworth – 2:40

==See also==
* BFI Top 100 British films
* British film
* 1987 in film

==References==
 

;Further reading
* Ali Catterall and Simon Wells, Your Face Here: British Cult Movies Since The Sixties (Fourth Estate, 2001)
* Richard E Grant, With Nails: The Film Diaries of Richard E Grant (Picador, 1996)
* Thomas Hewitt-McManus, Withnail & I: Everything You Ever Wanted To Know But Were Too Drunk To Ask (Lulu Press, 2006)
* Kevin Jackson, Withnail & I (BFI, 2004)
* Alistair Owen (editor), Smoking in Bed: Conversations with Bruce Robinson (Bloomsbury, 2000)
* Bruce Robinson, Withnail & I: The Original Screenplay (Bloomsbury, 1995)
* Maisie Robson, Withnail and the Romantic Imagination: a eulogy (Kings England Press, 2010)

==External links==
 
*   
*  
*  
*  
*  
*  
*  
*   – The Guardian Newspaper
*  
*  
*  
*  
 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 