I Wouldn't Be in Your Shoes
{{Infobox film
| name           = I Wouldnt Be in Your Shoes
| image          = I Wouldnt Be in Your Shoes.jpg
| alt            =
| caption        = Theatrical release poster
| director       = William Nigh
| producer       = Walter Mirisch Steve Fisher
| story          = Cornell Woolrich (novel)
| narrator       = 
| starring       = Don Castle Elyse Knox
| music          = Edward J. Kay
| cinematography = Mack Stengler
| editing        = Roy V. Livingston
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       = 23 May 1948
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
I Wouldnt Be in Your Shoes is a 1948 American film noir directed by William Nigh, starring Don Castle and Elyse Knox. 

==Plot==
Vaudeville dancer Tom Quinn (Castle) is convicted for murder after his shoe prints are found at the scene of the crime. His wife  Ann (Knox) follows the trail of clues to the real killer.

==Cast==
* Don Castle as Tom J. Quinn 
* Elyse Knox as Ann Quinn 
* Regis Toomey as Inspector Clint Judd 
* Charles D. Brown as Inspector Stevens 
* Rory Mallinson as Harry, 1st Detective  Robert Lowell as John L. Kosloff 
* Steve Darrell as District Attorney Bill Kennedy as 2nd Detective

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 

 