The Louisiana Hussy
{{Infobox film
| name           = Louisiana Hussy
| image_size     = 
| image	=	Louisiana Hussy FilmPoster.jpeg
| caption        = 
| director       = Lee Sholem William Rowland  (uncredited)
| producer       = Charles M. Casinelli (producer)
| writer         = Charles Lang (original screenplay) William Rowland  (uncredited)
| narrator       = 
| starring       = See below
| music          = Walter Greene
| cinematography = Ted Saizis Vincent Saizis
| editing        = John A. Bushelman
| studio         = 
| distributor    = 
| released       = 1959
| runtime        = 85 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Louisiana Hussy is a 1959 American film directed by Lee Sholem.

The film is also known as Louisiana Hussey (American alternative spelling).

== Plot summary ==
 

== Cast ==
*Nan Peterson as Nina Duprez, alias Minette Lanier Robert Richards as Pierre Guillot
*Peter Coe as Jacques Guillot
*Betty Lynn as Lili Guillot
*Howard Wright as Cob
*Harry Lauter as Clay Lanier
*Rosalee Calvert as The Real Minette Lanier
*Tyler McVey as Dr. J. B. Opie
*Smoki Whitfield as Burt, the Manservant
*Helen Forrest as Callie, the Gris-Gris Woman

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 

 