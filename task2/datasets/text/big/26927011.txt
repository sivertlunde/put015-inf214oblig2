Married in Hollywood
 {{Infobox film
| name           = Married In Hollywood
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Marcel Silver
| producer       =  Oscar Straus (operatta Ein Walzertraum) Harlan Thompson (screenplay)
| narrator       = 
| starring       = J. Harold Murray Norma Terris Walter Catlett Tom Patricola Arthur Kay Oscar Straus (score)
| cinematography = Charles Van Enger Sol Halperin
| editing        = Dorothy Spencer
| studio         = Fox Film Corporation
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Married in Hollywood (1929 in film|1929) is an American musical film. The only known footage to survive is a 12-minute fragment from the final reel in Multicolor at UCLA Film and Television Archive. The film is also known as Maritati ad Hollywood in Italy and Pantremmenoi sto Hollywood in Greece. The film reel was 2956.56 m long.

==Plot==
A showgirl, part of a troupe, tours Europe where she falls in love with a Balkan prince. The princes parents disapprove and attempt to put a stop to the romance. A revolution occurs and the prince and the showgirl elope to Hollywood.

==Cast==
*J. Harold Murray -	Prince Nicholai
*Norma Terris - Mary Lou Hopkins / Mitzi Hofman
*Walter Catlett - Joe Glitner
*Irene Palasty - Annushka
*Lennox Pawle - King Alexander
*Tom Patricola - Mahai
*Evelyn Hall - Queen Louise
*John Garrick - Stage Prince
*Douglas Gilmore - Adjutant Octvian
*Gloria Grey - Charlotte
*Jack Stambaugh - Captain Jacobi
*Bert Sprotte - Herr von Herzen
*Leila Karnelly - Frau von Herzen
*Herman - German Director
*Paul Ralli - Namari
*Donald Gallaher - Movie Director
*Carey Harrison - Detective
*Roy Seegar - Detective

==Soundtrack==
*Dance Away the Night (not to be confused with the similarly titled song from the original London production of Show Boat)
:Music by Dave Stamper
:Lyrics by Harlan Thompson
*Peasant Love Song
:Music by Dave Stamper
:Lyrics by Harlan Thompson
*Once Upon A Time
:Music by Dave Stamper
:Lyrics by Harlan Thompson
*A Man, A Maid
:Music by Oscar Straus
:Lyrics by Harlan Thompson
*Deep In Love
:Music by Oscar Straus
:Lyrics by Harlan Thompson
*Bridal Chorus
:Music by Arthur Kay
:Lyrics by Harlan Thompson
*National Anthem
:Music by Arthur Kay
:Lyrics by Harlan Thompson

==See also==
*List of lost films
*List of early color feature films

==External links==
*  
*  

 
 
 
 