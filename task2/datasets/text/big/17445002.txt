Løvejagten
{{Infobox film
| name           = Løvejagten
| image          = Løvejagten.jpg
| image size     =
| caption        = Photo Still from the 1907 Film
| director       = Viggo Larsen Ole Olsen
| writer         = Arnold Richard Nielsen
| starring       = Viggo Larsen Knud Lumbye
| cinematography = Axel Graatkjaer
| editing        =
| distributor    = Nordisk Film
| released       =  
| runtime        = 10 minutes
| country        = Denmark Danish intertitles
| budget         =
}}
 Danish producer Ole Olsen and director Viggo Larsen. The short ten-minute movie caused an enormous public protest in Denmark because it depicted the actual shooting of two captive lions. 

==Synopsis==
Two big game hunters are on safari in the jungle with their African guide. They observe zebras, ostrich and a hippopotamus, and catch a small monkey for a pet. During the night they are awakened by a lion which kills a small goat and then the hunters horse. The hunters shoot the lion as it stands by the water on a beach. They discover another lion and shoot it also. The lions are gutted and skinned. The happy hunters sit and smoke cigarettes afterward.

==Production and controversy== on location in Denmark. Scenes of the hunters in the forest were shot in Jægersborg Dyrehave park near Copenhagen. The animals were filmed at the Copenhagen Zoo with the camera aimed downward to avoid any view of the enclosures. The controversial shooting of two lions took place on the small island of Elleore in the Roskilde fjord. 
 Hagenbeck Zoo in Hamburg, Germany for the large sum of 5000 deutschmarks. Larsen, Lisbeth Richter, World Record Nordisk Film Centenary, Film #50, May 2006, retrieved=2008-06-22 
When the Danish Society for the Prevention of Cruelty to Animals learned about Olsens plan to shoot the lions for his movie, they protested to the Danish Minister of Justice Peter Adler Alberti. {{cite web
| title = When Nordisk Film killed Lions
| publisher = Nordisk Film Kompagni Press
| url = http://www.nordiskfilm.com/Press/Anecdotes/When+Nordisk+Film+killed+lions.htm
| accessdate=2008-06-22
}}    Alberti banned the filming. Two days later, however, Olsen defiantly shot the scenes as planned, then smuggled the film to Sweden. Olsens cinematographer, Axel Graatkjær, was arrested and spent a day in jail. At a court hearing, Alberti banned the movie in Denmark and revoked Olsens license for his Biograf Theater. {{cite web
| title = Kommentar om Løvejagten
| publisher = Det Danske Filminstitut
| accessdate = 2008-06-22
| url = http://dnfx.dfi.dk/pls/dnf/pwt.page_setup?p_pagename=dnffuldvis&p_parmlist=filmid=12547
}} 

==Aftermath== Nordisk Film golden age" of Danish cinema when Nordisk Film became the most productive film company in Europe. 
A sequel to the film, Bear Hunting in Russia, was shot in 1909 and was also a profitable movie, eventually selling 118 prints. 

==Cast and crew==
Viggo Larsen directed the filming as well as acted as one of the hunters. Knud Lumbye portrayed the second hunter and William Thomsen played the African guide. Axel Graatkjaer, who later became a favorite cinematographer of August Blom and Urban Gad, shot the film, credited under his actual name of A. Sørensen.

==References==
 

==External links==
*  at the The Danish Film Institute (in Danish)
* 

 
 
 
 
 
 
 
 