All About Them
{{Infobox film
| name           = All About Them
| image          = A trois on y va poster.jpg
| caption        = Film poster
| director       = Jérôme Bonnell 
| producer       = Edouard Weil   Genevieve Lemal 
| writer         = Jérôme Bonnell   Maël Piriou 
| starring       = Anaïs Demoustier   Félix Moati   Sophie Verbeeck
| music          = Mike Higbee 
| cinematography = Pascal Lagriffoul  
| editing        = Julie Dupré  Wild Bunch   France 3 Cinéma   Scope Pictures
| distributor    = Wild Bunch Distribution
| released       =  
| runtime        = 86 minutes
| country        = France Belgium
| language       = French
| budget         = 
| gross          = 
}}

All About Them (French title: À trois on y va) is a 2015 French romantic comedy film directed by Jérôme Bonnell. It stars Anaïs Demoustier, Félix Moati and Sophie Verbeeck. 

== Cast ==
* Anaïs Demoustier as Mélodie 
* Félix Moati as Micha 
* Sophie Verbeeck as Charlotte 
* Patrick dAssumçao as William 
* Olivier Broche as Un prévenu 
* Laure Calamy as Une prévenue 
* Hannelore Cayre as La présidente du tribunal  
* Claire Magnin as Maître Courtois   
* Marcelle Fontaine as La substitut 
* Caroline Baehr as Tante Estelle

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 

 
 