A Son Comes Home
{{Infobox film
| name = A Son Comes Home
| image =
| caption =
| director = E.A. Dupont
| producer = 
| writer = Harry Hervey   Sylvia Thalberg Donald Woods   Wallace Ford
| music =  Friedrich Hollaender   John Leipold
| cinematography = William C. Mellor Chandler House    
| studio = Paramount Pictures
| distributor = Paramount Pictures 
| released =  
| runtime = 77 minutes
| country = United States
| language = English
| budget =
| gross = Donald Woods. It was one of three films made by Dupont for Paramount Pictures. 

==Partial cast==
* Mary Boland as Mary Grady
* Julie Haydon as Jo  Donald Woods as Denny 
* Wallace Ford as Steve 
* Roger Imhof as Detective Kennedy 
* Anthony Nace as Brennan  Gertrude Hoffman as Effie Wimple 
* Eleanor Wesselhoeft as Essie Wimple  Charles Middleton as Prosecutor 
* Thomas E. Jackson as District Attorney  John Wray as Gas Station Owner 
* Robert Middlemass as Sheriff 
* Lee Kohlmar as Proprietor 
* Herbert Rawlinson as Bladeu  George Hassell as Captain

==References==
 

==Bibliography==
* St. Pierre, Paul Matthew. E.A. Dupont and His Contribution to British Film. Fairleigh Dickinson University Press, 2010 .

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 


 