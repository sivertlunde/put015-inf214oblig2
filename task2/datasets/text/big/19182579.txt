Yokai Monsters: Spook Warfare
 
  and bloodshed make that label highly questionable.

There were originally three movies made:

*   (1968) released in March
* Yokai Monsters: Spook Warfare (1968) released in December
*   (1969) released in March

The films were produced by Daiei Motion Picture Company, and feature actors in rubber costumes playing the monsters.

In 2005, Takashi Miike directed the first new entry in the series, The Great Yokai War, which was more or less a remake of Yokai Monsters: Spook Warfare.

==Plot summary== steward confronts kappa (Japanese water sprite) in the courtyard pond is awoken by the commotion, and comes out of the pond to investigate. Realizing the magistrate is Daimon in disguise, he tries to expel Daimon from the house, but is defeated. So the kappa runs off to beg the help of the apparitions (ghosts and monsters) who live in the surrounding countryside. They can find no reference to the monster he describes in their monster handbook, so they do not believe the kappas story.

Meanwhile the steward contacts his uncle, a Buddhist priest, who tells him the magistrate is a monster in disguise, and explains to him how he can expel the monster with magic. The steward tries the magic, but Daimon discovers him, and they fight. The steward spears one of the monsters eyes.

A new magistrate comes to replace the old one (though quite why is never explained, perhaps the original magistrates term of office has ended), but Daimon kills him and takes over his body. Because the monster has lost an eye, when he inhabits the new magistrates body, that body is also missing an eye. Safe in the new Magistrates body, the monster has the steward arrested and orders his execution.

Daimon soon desires the blood of children to maintain his immortality, so he has the magistrates servants hunt down the local farmers children. Two young boys escape the hunters and beg the kappa and other apparitions to help them. The apparitions and kappa try to combat Daimon, but are too weak, so they call on all Japanese apparitions.

In the final fight Daimon clones himself, and the apparitions expend most of their strength attacking the clones, and not the original monster. But they soon realise they can defeat him by blinding his one remaining eye, so they target his eyes. Daimon is then defeated and flees Japan. The humans and the apparitions rejoice that "he will probably never return", and the Japanese apparitions all go home.

== External links ==
* 
* 
* 

 
 
 
 
 
 
 
 