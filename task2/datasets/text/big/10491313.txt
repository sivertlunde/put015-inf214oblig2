Radiant City
 

{{Infobox film
| name         = Radiant City
| image        = RadiantCity.jpg
| caption      = Theatrical poster Gary Burns Jim Brown
| producer     = Shirley Vercruysse Bonnie Thompson
| music        = John Bissell Natalie Baartz Joey Santiago
| distributor  = Alliance Atlantis
| released     =  
| runtime      = 93 minutes
| country      = Canada English
| budget       =
| gross        = $78,944 
}}

Radiant City is a 2006 Canadian film {{Cite web
| title = National Film Board of Canada
| url = http://www.onf-nfb.gc.ca/eng/collection/film/?id=53256
}}  Gary Burns Jim Brown. It is about the suburban sprawl and the Moss familys life in the suburbs. The film is openly critical towards suburban sprawl and its negative effects, being ironic and amusing at the same time.

It was revealed to be a mockumentary at the end of the film.

The fictional part is about the five members of the Moss family who have just moved from the city into a new suburban development of Evergreen (part of Evergreen, Calgary|Evergreen) in Calgary, Alberta. The family discuss and portray the life in the suburbs and the various flaws and advantages of a modern suburban life, with a considerably greater emphasis on the flaws. While the characters and instances in the movie are fictional, the actors that play them all live in real suburban areas.

Interspersed with this narrative, experts speak about their views of the suburbs. These include Ken Greenberg, Joseph Heath, Mark Kingwell, James Howard Kunstler, Marc Boutin, Andrés Duany and Beverly Sandalack. These portions are filmed in different suburban areas of North America, including Oakville, Ontario.

==Plot==
The Moss family decides to move from the inner city of Calgary to a suburban area called Evergreen to satisfy Ann Mosss desire of living in a new house. Her husband, Evan Moss, is still keeping his job downtown, which forces him to consume two hours of commuting time.
Even if apparently the area offers everything that is needed by a family in walking distance, one is isolated due to constructions, buildings, distance, and the area layout and is constantly forced to rely on the car for every little need.
The family carries on its ordinary life with Ann having a positive opinion of their new life and Evan and the kids can see more clearly on the flaws and disadvantages of this lifestyle. However, they go along with it, constantly trying to find their way to justify their choice.

==Cast==
* Daniel Jeffery as Nick Moss
* Bob Legare as Evan Moss
* Jane MacFarlane as Anne Moss
* Ashleigh Fidyk as Jennifer Moss
* Curt McKinstry as Ken
* Karen Jeffery as Karen
* Michaela Jeffery as Nicole
* Amanda Guenther as Tina

==Soundtrack==
The soundtrack features songs from Joey Santiago of The Pixies.

==Release and reception==
Radiant City was presented at the 2006 Toronto International Film Festival with a very warm reception.       It was also premiered at several film festivals, including Vancouver and Calgary International Film Festivals, the Buenos Aires International Independent Film Festival and the São Paulo It’s All True Documentary International Film Festival, San Francisco Documentary Film Festival, Miami International Film Festival and the True/False Film Festival in Columbia, Missouri. 

===Critical response===
Radiant City has received generally favorable reviews.  The film currently holds a 93% "Certified Fresh" rating on the review aggregate website Rotten Tomatoes, an average rating of 6.8/10 from 15 critics with no consensus yet.  Metacritic, another review aggregator, assigned the film a weighted average score of 60 (out of 100) based on 6 reviews from mainstream critics, considered to be "mixed or average reviews". {{cite web |url=http://www.metacritic.com/movie/radiant-city |title=
Radiant City |work=Metacritic |publisher=CBS Interactive |accessdate=September 17, 2012}} 

==Awards and nominations==
The film won the Genie Award of Best Documentary at the 28th Genie Awards and the Special Jury Prize at the 2006 Vancouver International Film Festival. It was also the runner up for best Canadian film on the 2007 Toronto Film Critic’s List.   

==Home media==
The movie has also been released in DVD on March 4, 2008. The DVD includes the original English version of the film and the English version with French subtitles.

==References==
 

==External links==
* 
*  
*  
*  (Requires Adobe Flash)

 
 
 
 
 
 
 
 
 
 
 