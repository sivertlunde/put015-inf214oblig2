A Woman Rebels
{{Infobox film
| name           = A Woman Rebels
| image          = A-Woman-Rebels-1936.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Mark Sandrich
| producer       = Pandro S. Berman
| based on       =  
| writer         = Ernest Vajda Anthony Veiller
| narrator       =
| starring       = Katharine Hepburn Herbert Marshall
| music          = Roy Webb
| cinematography = Robert De Grasse
| editing        =
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States Italian
| budget         =$574,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p57  
| gross = $583,000 
}}
A Woman Rebels is a 1936 RKO film adapted from the novel Portrait of a Rebel by Netta Syrett and starring Katharine Hepburn as Pamela Thistlewaite, who rebels against the social mores of Victorian England. The film was directed by Mark Sandrich, was the film debut of Van Heflin, and the final film of David Manners.

==Plot==
Pamela defies her autocratic father (Donald Crisp), and has a baby out of wedlock with her lover, Gerald Waring (Van Heflin, in his screen debut).  Pamela raises her illegitimate daughter as her niece after her pregnant sister (Elizabeth Allan) falls and dies over the death of her young husband and becomes a crusading journalist for womens rights.  Eventually she agrees to marry diplomat Thomas Lane (Herbert Marshall) after being unfairly named as co-respondent in Warings divorce.  Hepburns performance as the defiant young woman is considered the epitome of her feminist characterizations of the 1930s.

==Cast==
*Katharine Hepburn as Pamela Thistlewaite
*Herbert Marshall as Thomas Lane
*Elizabeth Allan as Flora Anne Thistlewaite
*Donald Crisp as Judge Byron Thistlewaite
*Doris Dudley as Young Flora
*David Manners as Lieutenant Alan Craig Freeland
*Lucile Watson as Betty Bumble
*Van Heflin as Lord Gerald Waring Gaythorne
*Marilyn Knowlden as Flora Anne Thistlewaite (age 9)

==Reception==
With a box office loss estimated at a hefty $222,000 for RKO, this was Hepburns third flop in a row which contributed to Hepburns growing reputation as "box office poison".   

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 