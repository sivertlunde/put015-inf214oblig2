American Empire (film)
 {{Infobox film
| name           = American Empire
| image          = 
| image_size     = 
| caption        = 
| director       = William C. McGann
| producer       = Lewis J. Rachmil (associate producer) Harry Sherman (producer) Dick Dickson (producer)
| writer         = J. Robert Bren (story) Gladys Atwater (story) J. Robert Bren (writer) and Gladys Atwater (writer) and Ben Grauman Kohn (writer)
| narrator       = 
| starring       = See below
| music          = Gerard Carbonara
| cinematography = Russell Harlan
| editing        = Carroll Lewis
| distributor    = Paramount Pictures
| released       = 11 December 1942
| runtime        = 82 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

American Empire is a 1942 American film directed by William C. McGann. The film was released in the United Kingdom as My Son Alone.

== Plot summary ==
Two men join forces to build a cattle empire, and battle rustlers, bad weather and each other. 

== Cast == Richard Dix as Dan Taylor
*Leo Carrillo as Dominique Beauchard
*Preston Foster as Paxton Bryce
*Frances Gifford as Abigail Abby Taylor, Dans Sister
*Robert Barrat as Crowder, Small Rancher
*Jack La Rue as Pierre, Beauchard Henchman
*Guinn "Big Boy" Williams as Sailaway
*Cliff Edwards as Runty
*Merrill Rodin as Paxton Bryce Jr.
*Chris-Pin Martin as Augustin, Beauchard Henchman Richard Webb as Crane (small rancher)
*William Farnum as Louisiana Judge
*Etta McDaniel as Willa May, Bryces Maid
*Hal Taliaferro as Malone
 Frank Mills, Bob Woodward appear uncredited.

== Soundtrack ==
*Cliff Edwards – "Little Pal" (Written by Lew Pollack)

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 