Marilena from P7
{{Infobox film
| name = Marilena from P7
| image = Marilena from P7 poster (textless).jpg
| caption = The poster of the film (without text).
| director = Cristian Nemescu
| producer = Cristian Nemescu Ada Solomon  (exec. prod.) 
| writer = Tudor Voican Cristian Nemescu
| starring = Mădălina Ghiţescu Gabriel Huian
| music = Andrei Toncu
| editing = Cătălin Cristuţiu
| distributor =  
| released = May, 2006  (France, Cannes Film Festival|Cannes) June 4, 2006  (Romania, TIFF)   
| country = Romania
| runtime = 45 minutes    Romanian
| budget = $35,000    
}}

Marilena from P7 ( ) is a Short subject|medium-length Romanian Cinema of Romania|film, directed by Cristian Nemescu. First released in 2006, it was also nominated the same year for the Semaine de la critique section of the 59th Cannes Film Festival.   
 teenage love. picture and sound combine together into a new and organic way, by using innovative, unusual techniques. 

==Plot== Rom Elvis is singing (played by the singer Elvis Romano, who translated Elvis Presleys lyrics into his own language).  The prostitutes play with Andrei, telling him that he needs a car in order to date them, then they leave. The boy asks Marilena for her phone number, and she writes something in his hand.
 Are You Lonesome Tonight?, sung by Elvis Presley.

Among her clients, Marilena falls in love with a man, nicknamed Giani (Cătălin Paraschiv). One night, however, she spots Giani in his car in front of a store, accompanied by another woman, and becomes very discouraged.

Andrei finds a way to get hold of a car and plans to steal his fathers trolleybus. He manages to do so, together with his friends. His father sees him and starts following the boys with another driver, his colleague. Arriving at the prostitutes meeting place, the boys cannot find Marilena. Andrei recognizes Gianis car parked in front of block P7, so he goes in.

Marilena and Giani were in fact in her apartment; the girl goes to the restroom for a minute. Andrei manages to sneak into the apartment, and watches how, while still talking to Giani, Marilena cuts her jugular vein. Extracted from Mădălina Ghiţescus speech, from a Q&A about the movies premiere in Stockholm, on August 24, 2007.  After realizing what had happened, Giani becomes frightened and runs away; the pimp sees him leaving in a rush, so he goes to the apartment to see what happened. Meanwhile, Andrei goes into the room and looks down at Marilena who is lying down, with blood spilled over her. The news about Marilenas suicide spread fast around the neighborhood.

==Cast==
 

===Lead roles===
* Marilena is played by the Romanian actress Mădălina Ghiţescu (born July 29, 1978),    became known to the Bucharest public for the roles she played at the Casandra Theater Studio (Romanian language|Romanian: Studioul de teatru "Casandra") and The Very Small Theater ( ).   

* Andrei was played by Gabriel Huian (untrained actor, previously appearing in a video clip that caught the directors attention).  He was Cristian Nemescus preferred choice from the beginning, however a rigorous casting still took place. Apparently attracted to Mădălina Ghiţescu, Huian showed a different attitude than that of other candidates, which only convinced the director further. 

===Other roles===
* Mihai, Andreis brother – Cristi Olesher
* Andreis mother – Aura Călăraşu
* Andreis father – Gabriel Spahiu
* The pimp – Andi Vasluianu
* Giani – Cătălin Paraschiv

==Production==

===From project to production===
 
The films idea resulted from a small scale exercise between Nemescu and Liviu Marghidan when they were in university in 2003. The original idea belonged to Cătălin Mitulescu and Andreei Vălean; the only part of the original story that was kept is that of a group of boys trying to steal a trolleybus in order to get to some prostitutes. 
 California Dreamin, US $14,000, which was collected by Nemescu by participating at various film festivals, with older short fictions.  During development, however, the film received new funds,  though still modest ones, thus the actors and a part of the team accepted to work for free.   

Filming also took place in 2005, near the marketplace of the Bucharest neighborhood Rahova.  The chosen area raised a few difficulties related to safety. However, the production benefited from its picturesque view, rendering the scenography and the original music unnecessary.  The omnipresent manele also found their way into the movie, as background music.

 

For a long period of time the project was entitled Trolley Blues; Cristian Nemescu gave up this title, worrying that there might be a large discrepancy between the equivocal name and the actual, very direct content of the movie.   

===Post-production===
The entire filmmaking process was much more complex than that of most Romanian movies made after 1948.  The picture stands out through the use of various effects, such as Split screen (film)|split-screen, and the use of hand-held filming. 
 equalized to Memestra brassicae butterflies. 

==Reception==

===Public reception===
Marilena from P7 participated at the 59th Cannes Film Festival, along with two other Romanian films  (Cătălin Mitulescus  ) and was nominated for the Semaine de la critique section of the French festival. 

 

   
  (center).]]
The Romanian premiere took place on June 4, 2006 at the Transylvania International Film Festival, in Cluj-Napoca. In Bucharest, a first display of the movie was organized on September 8, 2006, in a square established for this purpose on Calea Rahovei Street.  The public, as well as the critics from the country, received the movie very well, but the difficulty caused by the movies unusual duration only allowed for the movie to be on display on a few other occasions. The same problem was encountered when the movie was sent to film festivals outside Romania, being either rejected due to the impossibly of being included in any of the standard categories  or included as a short fiction (for example, at Brooklyn). 

In the summer of 2006, Nemescu also started the production of  California Dreamin . The filming took place in the June–August interval. By the end of August, the film was nearing its completion; on August 24, 2006, however, Nemescu and Andrei Toncu (sound engineer for the new project as well) died in a car accident in Bucharest. Following the accident, the Romanian public, as well as those from abroad, offered wider attention to the productions of the two cineasts, including Marilena from P7, their first notable success. One year after the accident, the event was commemorated in the country through various displays, public meetings and debates. On August 24, 2007, Marilena from P7 had its premiere at Stockholm during the second Romanian Film Days. Romanian people, from the diaspora, as well as Swedish spectators, were present. The movies invited at Romanian Film Days in Stockholm took place at the "Zita" cinema, during August 23 and 26, 2007. Nemescus movie was on display in the same schedule with Trafic (Cătălin Mitulescu)|Trafic, Cătălin Mitulescus short fiction, and Lampa cu căciulă, by Radu Jude. At the end of the display, on August 24, the movie critic Adina Brădeanu held a Q&A with Mădălina Ghiţescu in the presence of the spectators. The writer Gabriela Melinescu was also present at the event. 

===Critical reception===
Marilena from P7 was seen by journalists as an attempt to attract attention to various social phenomena (prostitution, the living conditions in low-income Bucharest neighborhoods, etc.),  and being presented as a documentary in some scenes.    In reality, Nemescu wanted for the public to concentrate on the love story of Andrei:

 

 	

 
Marilena from P7 also attracted the critics attention due to its use of explicit verbal and visual content. Cristian Nemescu said that this kind of scenes were a necessarily element for the realism of the movie, declaring himself a partisan of explicit content. 
 UFO that scares the neighbors away. Another fictional element is Marilenas capacity of provoking short circuits, in moments in which she is very emotionally active (e.g., when Andrei touches her in the bars restroom, or when the whole neighborhood is cut from electricity when she cries near the high voltage electrical box). 
 
 

==Awards and nominations==
:The following list includes the most notable nominations and awards the film received.
{| class="wikitable"
|-
! Date !! Outcome !! Festival !! Country !! Section !! Award
|- style="text-align:center"
| May 2006 || nominated ||   || France || Semaine de la critique (French language|Fr. "Critique week")|| —
|- style="text-align:center"
| June 2006 || won    ||   || Romania || Premiile Zilelor filmului românesc     (Romanian language|Ro. "Romanian Film Days Awards") || Short fiction (Cristian Nemescu)
|- style="text-align:center"
| June 2006 || nominated  ||   || United States|U.S.A. || Short films || —
|- style="text-align:center"
| September 2006 || won    || Milano Film Festival || Italy || — || Miglior lungometraggio (Italian language|It. "Best long fiction")
|- style="text-align:center"
| October 2006 || won ||   || Ukraine || — ||  Best short fiction (Cristian Nemescu) Best actress in a leading role (Mădălina Ghiţescu)    
|- style="text-align:center"
| November 2006 || nominated       ||   || Switzerland || Short movies    || —
|- style="text-align:center"
| January 2007 || won    ||   || France || Prix du public (Fr. "Viewers Award")    || Meilleur film étranger (Fr. "Best Foreign Film")
|- style="text-align:center"
| February 2007 || won  ||   || Romania || — ||  Award for montage (Cătălin Cristuţiu) Award for sound design (Andrei Toncu) 
|- style="text-align:center"
| April 2007 || won  ||   || U.S.A. || — || Short fiction   
|}

==Further reading==
*   originally published in the "Eva" magazine, a dialogue between Greta Harja, Nemescu and Mădălina Ghiţescu before the Cannes Festival
*   by Mădălina Ghiţescu with Paul S. Odhan

==See also==
* Romanian New Wave

==References and footnotes==
 

==External links==
*   on Internet Movie Database
*   on  
*   on   Allmovie
*   (with text)
*   (photographs from the film and the shooting), hosted  by  

 

 
 
 
 