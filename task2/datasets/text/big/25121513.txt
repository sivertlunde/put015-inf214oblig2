Henri-Georges Clouzot's Inferno
 
{{Infobox film
| name           = Inferno
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Henri-Georges Clouzot
| producer       = 
| writer         = Henri-Georges Clouzot Jean Ferry José-André Lacour
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Serge Reggiani Romy Schneider
| music          = 
| cinematography = Andréas Winding
| editing        = 
| studio         = 
| distributor    = 
| released       = 1964
| runtime        = 
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Henri-Georges Clouzots Inferno (French title: LEnfer dHenri-Georges Clouzot) is a film directed, written and produced by   and  ,  which remained unfinished in 1964. It was presented in 2009 as a full-length semi-documentary by  . 

==History==
The film depicts the extreme jealousy of a hotelier, Marcel (Serge Reggiani, then 42 years old), towards his wife, Odette (Romy Schneider, then 26 years old). It was shot partly in black-and-white, partly in colour. Clouzot selected the title as an allusion to Dante Alighieri|Dantes Inferno (Dante)|Inferno, and the names Odette and Marcel refer to characters in Marcel Prousts novel In Search of Lost Time|À la recherche du temps perdu. 

Despite an unlimited budget from   region; the main actor Serge Reggiani claimed to be ill (Jean-Louis Trintignant was asked to replace him); the artificial lake below the Garabit viaduct, an important part of the location, was about to be emptied by the local authorities; then Clouzot suffered a heart attack and was hospitalised in Saint-Flour, Cantal|Saint-Flour. After three weeks, the film was abandoned. 

==Cast==

*Romy Schneider, Odette
*Serge Reggiani, Marcel
*Dany Carrel, Marylou
*Jean-Claude Bercq, Martineau
*Catherine Allégret, Yvette
*Blanchette Brunoy, Clotilde
* , Julien

==Other versions==
In 1994, Claude Chabrol used the screenplay by Clouzot to make his film LEnfer (1994 film)|LEnfer.

{{Infobox film
| name           =LEnfer dHenri-Georges Clouzot
| image          =LEnfer (Bromberg).jpg
| image_size     =
| border         =
| alt            =
| caption        =
| director       = 
| producer       =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =2009
| runtime        =
| country        =France
| language       =French
| budget         =
| gross          =
}}
In 2009,   and Ruxandra Medrea released a 94 minute documentary with material selected from 15 hours (185 Reel#Motion picture terminology|reels) of found scenes under the name LEnfer dHenri-Georges Clouzot.  Bromberg was caught for two hours in a stalled elevator with a woman who turned out to be Clouzots second wife, Inès de Gonzalez. Upon learning the identity of the woman and of the existence of the footage, Bromberg convinced her to release it to make his film. The documentary includes interviews with nine cast and crew members, notably Catherine Allégret, the production assistant from 1964, Costa Gavras, and the assistant cinematographer William Lubtchansky. Bromberg used the actors Bérénice Bejo and Jacques Gamblin to shoot some scenes where dialogue had to be delivered; the found material, which included some 30 hours of soundtrack, didnt contain suitable material.
 Best Documentary. 

==Visual Effects==
The film features several innovative and practical lighting techniques by   and  . Most notably in the psychedelic climax of the film, rotating lighting rigs were placed in-front of the camera and actors. The final effect created the illusion of the actors faces transitioning between emotions, and personalities. They would also slowly change their emotions intensifying the effect when synced to each rotation.

==References==
 

==External links==
* 
* 
* , 2009 Toronto International Film Festival 20 minutes  
*  by Elisabeth Bouvet, Radio France Internationale (19 May 2009)   SBS TV (19 May 2009)
*  (1:40)
*  (5:22)

 

 
 
 
 
 
 
 

 
 