Malcolm (film)
 
 
{{Infobox film
| name           = Malcolm
| image          = Malcolmdvd.jpg
| image size     = 35mm 185
| caption        = Australian DVD cover
| director       = Nadia Tass David Parker Nadia Tass
| writer         = David Parker
| narrator       =  John Hargreaves Charles (Bud) Tingwell
| music          = Paul Coppens Simon Jeffes David Parker
| editing        = Ken Sallows
| studio         = Cascade Films
| distributor    = Hoyts (Australia) Umbrella Entertainment Vestron Pictures (US)
| released       =  
| runtime        = 90 minutes
| country        = Australia
| language       = English
| budget         = A$1 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p324-327 
| gross          = $3,842,129 (Australia)
| preceded by    = 
| followed by    = 
}}
 David Parker Best Director.

==Synopsis== Metropolitan Transit John Hargreaves). Franks brassy girlfriend Judith (Lindy Davies) soon moves in with him, and Frank reveals that he is a petty criminal who has recently been released from gaol. Despite their differences, the trio develop an awkward friendship, and when Malcolm learns of Frank and Judes plans to stage a robbery, he decides to use his technical ingenuity to help them. In his first demonstration, he shows Frank the "getaway car" he has built, which splits into two independently powered halves, and they use this to successfully elude police after Frank steals some cash from a bank customer.

For his next demonstration, Malcolm stages a near-successful hold-up of a payroll delivery, using a radio-controlled model car and trailer, fitted with a video camera, a speaker, and a gun loaded with blanks with which to threaten the guards. Frank walks in on Malcolms bedroom "control centre" while the robbery is in progress; joining in, he helps Malcolm to steal the cash, although it is eventually lost when the planned getaway route (through a street drain) proves too small and the bag of cash is knocked off the trailer.
 police while they make their escape. They manage to elude the pursuing police, but they are nearly caught when two officers on a routine patrol pull up beside them and ask them for an ice-cream. Frank speeds away, with the police in hot pursuit, and they now employ their backup getaway plan. They dump the van in a suburban street and decamp on foot, but when the police arrive moments later and scan the area for the fugitives, they see only the back of a tram, pulling away into the distance. However, when we see the front of the tram, it is revealed to be Malcolms custom-made mini-tram, with the trio and their loot aboard.

In the final scene, Frank is leaving a bank in Lisbon, Portugal (another city with a major tram network) where he has just deposited the proceeds of the Melbourne robbery. He then meets up with Malcolm and Jude at a local cafe, and as the film concludes they lay plans for another daring robbery.

==Production==
As revealed in the closing credits, the character of Malcolm was inspired by Nadia Tass late brother, John Tassopoulos, who died after suffering an epileptic seizure after being hit by a car in 1983.  As portrayed by Friels, Malcolm exhibits many traits that are characteristic of someone with high-functioning autism or Asperger syndrome, including a peculiar walk, reluctance to make eye contact, poor social skills and a deep-focus, obsessive interest (in this case, trams).

David Parker had never written a script before and he did it while working on location of Burke and Wills as a stills photographer. Raising money was very difficult. Channel Seven agreed to provide $175,000 as a pre sale, Film Victoria came in for $100,000 and the rest of the movie was raised from Parker and Tass mortgaging their house and via 10BA.  

All of the gadgets in Malcolms house and the ingenious inventions used in the robbery sequences were devised by writer David Parker.

=== Filming locations ===
The scenes of the exterior of Malcolms house were filmed at 23 Napoleon Street, Collingwood, Victoria|Collingwood. The house has since been demolished and the land is vacant. The interior scenes were filmed at a house in John Street, Flemington, Victoria|Flemington, an inner city suburb of Melbourne.

A façade was constructed in Napoleon Street for the exterior scenes of the Milk Bar. The interior scenes were filmed at the former milk bar located on Peel Street near Napoleon Street.

The Leinster Arms Hotel, located in Gold Street, Collingwood, was used for filming the inside scenes at the pub Frank often frequents.
 Collins Street Queen Street. All signage related to the Commonwealth Bank was removed for the purpose of filming.

The overhead bridge featured in the robbery of the bank is located at the William Angliss TAFE, on LaTrobe Street.
 South Melbourne Kew Depot features briefly in a dawn scene of a tram depot, prior to Malcolm taking his own tram for a test run. The Foremans office in which Malcolm is sacked is located in the body shop at Preston Workshops. The scene in which Malcolm, Frank and Judith switch from a getaway van to Malcolms tram was filmed near the Workshops in Miller Street, Thornbury, Victoria|Thornbury.

=== Vehicles ===
  site (Melbourne Tramway Museum) in Bylands, Victoria.]]

The model tram that Malcolm "built" ran on a motorbike engine, the rest having been put together by Ian McClay. A wooden pole was used to simulate the power input pole.  After the film was completed, the tram was donated to the Tramway Museum Society of Victoria.

The split car gag was achieved with three different Honda Zs. One was tricked up with two motorbikes bought from Albert Park Golf Club - these were the split cars that the stunt riders drove in the movie. The second car was used for the actors on a caravan base towed behind a tracking vehicle generously donated for the job by Sydney grip, Ray Brown. The third car remained as a driveable - pre-split car. It is now housed in Cascade Films Foyer in South Melbourne. The drivable split car is now stored at the  , Canberra. 
 Tamiya Sand Scorcher, model number 58016.

All the remote control cars, including the ashtrays, were built by David Parker, Tony Mahood and Ian McClay. The split cars were built by Tony Mahood, Steve Mills (from Teds Camera Store) and David Parker. 
 Channel Seven David Johnston, Channel Ten at the time.

== Music ==
The music used in the film was composed by Simon Jeffes and performed by him and fellow members of the Penguin Cafe Orchestra.

==Box office==
Malcolm grossed AU$3,482,129 at the box office in Australia,  which is equivalent to AU$7,592,242 in 2009 dollars.

==Home media==
Malcolm was released on DVD by Umbrella Entertainment in November 2001. The DVD is compatible with all region codes and includes special features, such as the trailer, a photo montage, location map, the press kit, a Popcorn Taxi Q&A, More Malcolm Gizmos and audio commentary with Nadia Tass and David Parker.   

==See also==
 
* Cinema of Australia
* Trams in Melbourne

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 