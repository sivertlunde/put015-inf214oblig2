The Howling (film)
 
{{Infobox film
| name = The Howling
| image = The Howling (1981 film) poster.jpg
| caption = Theatrical release poster
| director = Joe Dante
| screenplay = John Sayles Terence H. Winkless
| based on =   Christopher Stone Belinda Balaski
| producer = Michael Finnell Jack Conrad
| music = Pino Donaggio
| cinematography = John Hora
| editing = Mark Goldblatt Joe Dante
| distributor = Avco Embassy Pictures
| released =  
| runtime = 91 minutes
| country = United States English
| budget = $1.5 million Gerry Molyneaux, "John Sayles, Renaissance Books, 2000 p 96 
| gross = $17,985,893
}}
 novel of the same name by Gary Brandner, the screenplay is written by John Sayles and Terence H. Winkless. The original music score is composed by Pino Donaggio. It is the first of eight films in The Howling (franchise).

==Plot== stalked by Christopher Stone), to the "Colony", a secluded resort in the countryside where he sends patients for treatment.
 nymphomaniac named shapeshift into werewolves during coitus.

After Bills wolf bite, Karen summons her friend Terri Fisher (Belinda Balaski) to the Colony, and Terri connects the resort to Eddie Quist through a sketch he left behind. Karen also begins to suspect that Bill is hiding a secret far more threatening than marital infidelity. While investigating, Terri is attacked by a werewolf in a cabin, though she escapes after cutting the monsters hand off. She runs to Waggners office and places a phone call to her boyfriend, Chris Halloran (Dennis Dugan), who has been alerted about the Colonys true nature. While on the phone with Chris, Terri looks for files on Eddie Quist. When she finally finds Eddie in the file cabinet, she is attacked by Eddie (in his werewolf form) and tries to fight back. However, Terri is finally killed when she is picked up by him and bitten in the jugular vein. Chris hears this on the other end and sets off for the Colony armed with silver bullets.

Karen is confronted by the resurrected Eddie Quist once again, and Eddie transforms himself into a werewolf in front of her. In response, Karen splashes Eddie in the face with corrosive acid. She escapes, and Eddie is later shot by Chris with a silver bullet. However, as it turns out everyone in the Colony is a werewolf and can shapeshift at will without need of a full moon. Karen and Chris survive their attacks and burn the Colony to the ground.

Karen resolves to warn the world about the existence of werewolves, and surprises her employers by launching into her warnings while on television. Then, to prove her story, she herself shapeshifts into a werewolf, having become one after being attacked at the Colony by her husband Bill. She is shot by Chris on live television, and the world is left to wonder whether the transformation and shooting really happened or if it was the work of special effects. It is also revealed that Marsha Quist escaped the Colony alive and well.

==Cast==
* Dee Wallace as Karen White
* Patrick Macnee as Dr. George Waggner
* Dennis Dugan as Chris Halloran Christopher Stone as R. William "Bill" Neill
* Belinda Balaski as Terri Fisher Kevin McCarthy as Fred Francis
* John Carradine as Erle Kenton
* Slim Pickens as Sam Newfield
* Elisabeth Brooks as Marsha Quist
* Robert Picardo as Eddie Quist
* Margie Impert as Donna
* Noble Willingham as Charlie Barton
* James Murtaugh as Jerry Warren
* Jim McKrell as Lew Landers
* Kenneth Tobey as Older Cop
* Dick Miller as Walter Paisley

==Production== 1978 film Piranha (1978 film)|Piranha. Sayles rewrote the script with the same self-aware, satirical tone that he gave Piranha, and his finished draft bears only a vague resemblance to Brandners book. However, Winkless still received a co-writers credit along with Sayles for his work on the screenplay.

The cast featured a number of recognizable character actors such as John Carradine, Kenneth Tobey and Slim Pickens, many of whom appeared in genre films themselves. Additionally, the film was full of in-joke references (see References below). Roger Corman makes a cameo appearance as a man standing outside a phone booth, as does John Sayles, appearing as a morgue attendant and James Murtaugh as one of the members of the Colony. Forrest J. Ackerman appears in a brief cameo in an occult bookstore, clutching a copy of his magazine Famous Monsters of Filmland.
 Dante on Rick Baker was the original effects artist for the film, but left the production to work on the John Landis film An American Werewolf in London, handing over the effects work to Rob Bottin.  Rob Bottin|Bottins most celebrated effect was the on-screen transformation of Eddie Quist, which involved air bladders under latex facial applications to give the illusion of transformation. In fact, Variety claims that The Howlings biggest flaw is that the impact of this initial transformation is never topped during the climax of the film.   The Howling also features stop-motion animation by notable animator David W. Allen, and puppetry intended to give the werewolves an even more non-human look to them.  Despite most of the special effects at the time, the silhouette of Bill and Marsha having sex as werewolves is quite obviously a cartoon animation. Joe Dante attributed this to budgetary reasons.
 smiley face image on a refrigerator door. Eddie Quist leaves yellow smiley face stickers as his calling card in several places throughout The Howling. 

==Reception==
Critical response to The Howling varied.  In 1981, Roger Eberts 2-out-of-4 star review was written in the breathless style of an old-time radio adventure script and described The Howling as the "silliest film seen in some time," but Ebert also said the special effects were good and the film was perhaps "worth your money, IF you get it two for one."  However, Eberts television partner Gene Siskel liked the film and gave it three and a half stars out of four.  Leonard Maltin also wrote in his book 2002 Movie & Video Guide that The Howling is a "hip, well-made horror film" and noted the humorous references to classic werewolf cinema.  Variety praised both the films sense of humor and its traditional approach to horror. 

The film won the 1980 Saturn Award for Best Horror Film (despite the fact it was not released until 1981). This film was also #81 on Bravo (US TV channel)|Bravos 100 Scariest Movie Moments.

==Home media==
Shout! Factory announced plans to release The Howling on DVD and Blu-ray Disc|Blu-ray on June 18, 2013 through their Scream Factory branch. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 