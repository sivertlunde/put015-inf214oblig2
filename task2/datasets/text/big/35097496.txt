The SpongeBob Movie: Sponge Out of Water
 
{{Infobox film
| name           = The SpongeBob Movie: Sponge Out of Water
| image          = SB-2 poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       =  Paul Tibbitt
| producer       = {{Plainlist|
* Paul Tibbitt
* Mary Parent}} 
| story          = {{Plainlist|
* Stephen Hillenburg
* Paul Tibbitt}} Glenn Berger Jonathan Aibel
| based on       =  
| starring       = {{Plainlist|
* Tom Kenny
* Antonio Banderas
* Clancy Brown
* Rodger Bumpass
* Bill Fagerbakke
* Carolyn Lawrence
* Mr. Lawrence
* Matt Berry
}}
| music          = John Debney   
| cinematography = Phil Meheux
| editing        = David Ian Salter
| studio         = {{Plainlist|
* Paramount Animation
* Nickelodeon Movies
* United Plankton Pictures
}}
| distributor    = Paramount Pictures
| released       =   }} 
| runtime        = 92 minutes  
| country        = United States
| language       = English
| budget         = $74 million    
| gross          = $310.9 million  
}}
The SpongeBob Movie: Sponge Out of Water (also known as The SpongeBob SquarePants Movie 2) is a 2015 American Films with live action and animation|animated/live action adventure comedy film, based on the Nickelodeon television series SpongeBob SquarePants, created by Stephen Hillenburg. Released in 2015, the film is a sequel to 2004s The SpongeBob SquarePants Movie. It is directed by the show writer and executive producer Paul Tibbitt, and written by Tibbitt, Jonathan Aibel and Glenn Berger, and SpongeBob creator and executive producer Hillenburg. Sponge Out of Water is executive produced by Stephen Hillenburg, and co-executive produced by Cale Boyter, Nan Morales, and Craig Sost. The film stars the regular television cast (Tom Kenny, Bill Fagerbakke, Rodger Bumpass, Clancy Brown, Carolyn Lawrence, and Mr. Lawrence), who returned to reprise their respective roles from the series and the previous film,  while Antonio Banderas plays a new live-action character, Burger-Beard the Pirate.   The film is produced by Paramount Animation, Nickelodeon Movies, and United Plankton Pictures, and was distributed by Paramount Pictures. This film is dedicated to Ernest Borgnine, the original voice of Mermaid Man, who died in July 2012.
 Mike Mitchell. However, in this film, SpongeBob and other characters are animated and rendered in computer animation. The live action scenes were shot in various locations throughout Savannah, Georgia and Tybee Island; filming began on October 9, 2013, in the downtown area, where various establishments were changed to resemble a beach community. Filming completed in November 2013. The SpongeBob Movie: Sponge Out of Water was released in the United States on February 6, 2015 to positive reviews and was a box office success, grossing over $310 million against its $74 million budget.

==Plot==
A pirate named Burger-Beard travels to the island of Bikini Atoll to obtain a magical book that makes any text written upon it come true. The book tells the story of SpongeBob SquarePants, a childlike sea sponge who loves his job as a fry cook at the Krusty Krab fast food restaurant and faithfully guards the secret Krabby Patty secret formula from Plankton, owner of the Chum Bucket and business rival to Mr. Krabs, SpongeBobs boss.

In the undersea town of Bikini Bottom, the Krusty Krab is attacked by Plankton. After a military battle with giant foods and condiments, Plankton feigns surrender: he uses a decoy of himself to offer the greedy Mr. Krabs a fake penny, which he hides inside to gain access to Krabs vault. The Plankton decoy distracts Mr. Krabs by crying outside the restaurant so Mr. Krabs can gloat about his victory. Plankton steals the formula, leaving a fake in its place. However, just as he is about to escape, SpongeBob catches him. They have a tug of war over the formula, but it magically vanishes before either can claim it.

Without the secret formula, SpongeBob cant make more Krabby Patties, and the customers become ravenous. Only SpongeBob believes that Plankton is innocent of stealing the formula, and when all of Bikini Bottom turns on them, SpongeBob rescues Plankton with a giant soap bubble that he and Plankton fly away in. Bikini Bottom is soon reduced to a post-apocalyptic wasteland because of the absence of the cherished and much-relied-on Krabby Patty. SpongeBob proposes he and Plankton team up to get the formula back; he attempts to show Plankton the meaning of teamwork, but he doesnt quite understand. Eventually, the duo decides to go to the Chum Bucket and rescue Karen, Planktons computer wife, to use as a power source for a time machine that will take them to the moment before the formula disappeared. They assemble the machine at an abandoned taco restaurant, but accidentally travel far into the future, where they meet Bubbles, a magical dolphin whose job it is to oversee the cosmos. They eventually succeed in retrieving the formula, but it turns out to be the fake Plankton had left.

Burger-Beard, whose seagull minions removed the final page of the book and rewritten the story so that he gets the formula, converts his ship into a food truck to sell Krabby Patties at a beach community named Salty Shoals. The final page, which has been discarded in the ocean, lands on the treedome of Sandy Cheeks, SpongeBobs friend and a squirrel living underwater. Crazed by the lack of Krabby Patties, Sandy thinks the page is a sign from the "sandwich gods", and suggests a sacrifice be made to appease them. The town attempts to sacrifice SpongeBob, but he and Mr Krabs smell Krabby Patties, and the crowd follows the scent, which leads to the surface. Bubbles returns because SpongeBob, whod neglected to keep Saturn and Jupiter from colliding, cost him his job as overseer of the universe. However, he hated the job and is glad to be out of it, and thanks SpongeBob by allowing him, Patrick, Mr. Krabs, Squidward and Sandy the ability to breathe on land. Plankton joins also, stowed away in SpongeBobs sock. Bubbles shoots them out of his blowhole and they land on a beach.

The team locates Burger-Beard who, using the book, banishes them to Pelican Island. Using Squidwards ink and the final page, SpongeBob transforms himself and the others into superheroes with special powers - The Invincibubble (SpongeBob), Mr. Superawesomeness (Patrick), Sour Note (Squidward), The Rodent (Sandy), and Sir Pinch-a-Lot (Mr. Krabs). They return and encounter Burger-Beard, who drives off with the formula, forcing the team to give chase. During the ensuing battle, the book gets destroyed. The team attempts to apprehend Burger-Beard, who picks the team off one by one.

Plankton, whod been left on Pelican Island, becomes a muscle-bound hero named Plank-Ton and comes to assist them. After retrieving the formula, Plank-Ton and Invincibubble work together to create one final attack that sends Burger-Beard all the way to Bikini Atoll. Plankton, having learned the values of teamwork, returns the secret formula to Mr. Krabs and the gang uses the final pages magic to return home to Bikini Bottom. With Krabby Patties back, Bikini Bottom returns to order, and Plankton re-assumes his role as business rival, bringing things back to the "status quo" ending the movie.

==Cast==
 
  SpongeBob SquarePants/The Invincibubble        and  Gary the Snail
* Bill Fagerbakke as Patrick Star/Mr. Superawesomeness 
* Rodger Bumpass as Squidward Tentacles/Sour Note   and Squidasaurus Rex
* Clancy Brown as Mr. Krabs/Sir Pinch-a-Lot  
* Carolyn Lawrence as Sandy Cheeks/The Rodent 
* Mr. Lawrence as Plankton (character)|Plankton/Plank-Ton 
* Antonio Banderas as Burger-Beard the Pirate 
* Matt Berry as Bubbles
* Jill Talley as Karen
* Dee Bradley Baker as Perch Perkins and other fish characters
* Nolan North as Pigeon Cabbie
* Paul Tibbitt as Kyle the Seagull (voiced by Joe Sugg in the UK release)
* Lori Alan as Pearl

Seagulls voiced by Carlos Alazraqui, Eric Bauza, Tim Conway, Eddie Deezen, Nolan North, Rob Paulsen, Kevin Michael Richardson, April Stewart, Cree Summer, and Billy West (also voiced by Alan Carr, Caspar Lee, and Stacey Solomon in the UK release).

==Production==

===Development===
Following the release of The SpongeBob SquarePants Movie in 2004, producer Julia Pistor stated that a sequel film was unlikely, despite the films successful box office performance.  In a 2009 interview with Digital Spy, SpongeBob SquarePants writer and executive producer Paul Tibbitt was asked about the possibility of a sequel.  He said, "I think that they are talking about doing that, but I havent signed up for anything. We just feel like weve told so many stories, and SpongeBob exists so well in this short 11-minute form."  He further stated that making another film was "a huge challenge."  However, Tibbitt denied that a sequel is not impossible to emerge, saying "I wouldnt say no, but I dont know if there will be another one."    In 2010, Nickelodeon reportedly had been approaching the crews of the show to make another film adaptation.    The network had long wanted to partner with Paramount Pictures to release another SpongeBob SquarePants film to help reinvigorate the series from its declining ratings.    However, internal disagreement delayed collaborations.  

On March 4, 2011, in an article by the Los Angeles Times, it was first reported that Paramount had "another SpongeBob picture" in development.  Several months later, in July, Paramount formed its new animation unit, Paramount Animation, in the wake of the commercial and critical success of the 2011 computer animation|computer-animated film Rango (2011 film)|Rango, and the departure of DreamWorks Animation upon completion of their distribution contract in 2012.    Philippe Dauman, the president and CEO of Paramount and Nickelodeons parent company Viacom, officially announced on February 28, 2012 that a sequel film was in development and slated for an unspecified 2014 release, saying that "We will be releasing a SpongeBob movie at the end of 2014."      Dauman added that the film "will serve to start off or be one of our films that starts off our new animation effort."  Nickelodeon expected the film to do much better in foreign box office than the 2004 feature, given its increasingly global reach.   Dauman said, "This will continue to propel SpongeBob internationally." 

Production was announced on June 10, 2014 under the title The SpongeBob SquarePants Movie 2,     which some trade publications began referring to as SpongeBob SquarePants 2.  The films executive producer is series creator Stephen Hillenburg, who departed from the show as its showrunner in 2004 following the release of The SpongeBob SquarePants Movie. He no longer writes or runs the show on a day-to-day basis, but reviews each episode and delivers suggestions.     However, in a 2012 interview with Thomas F. Wilson, Hillenburg stated that he was helping in writing the film.    Tibbitt later revealed on Twitter in late 2013 that "Steve   and I wrote the new movie together and he has been in the studio everyday working with us."    Production on the film was expected to finish in November 2014. 

===Cast=== background extras for the live action scenes.     On September 21, 2013, it was reported that Spanish actor Antonio Banderas had been cast for a live action role as Burger-Beard the pirate.     

===Animation===
  depicting the main characters, with the films logo in the upper left corner.]]
The animation for the film was handled overseas by Rough Draft Studios in South Korea.  Vincent Waller said, "Were getting animation back that is looking terrific."  The sequel is a combination of traditional animation and live action as its predecessor was,   and also used computer-generated imagery (CGI) handled by Iloura VFX in Melbourne, Australia to render the characters in 3D.     Sherm Cohen returned to work on the sequel, when he previously worked on The SpongeBob SquarePants Movie as a character designer and the lead storyboard artist and left the show in 2005.  Series animation directors Tom Yasumi and Alan Smart worked on the films exposure sheets.  Most of the character layout crew of the film are from the series. 
 3D short film released in early 2013 at the Nickelodeon Suites Resort Orlando, executives also talked of perpetuating the 3D stereoscopy in the film.  Director Tibbitt was asked on Twitter concerning what animation technique the film would have, and responded, "  dont wanna spoil anything but   mostly 2D." 

The film contains a stop-motion animation sequence by Screen Novelties.  

In March 2014, Paramount screened live action footage from the film during the National Association of Theatre Owners CinemaCon. News websites report that the film would be CGI-animated,  with an Internet Movie Database staff commenting, "When Paramount announced there would be a new SpongeBob SquarePants movie, the assumption was that it would be animated (like all other incarnations of SpongeBob). The very brief footage from tonights presentation suggested otherwise – it looked as though this was a CGI/live-action hybrid akin to Alvin and the Chipmunks (film)|Alvin and the Chipmunks, Yogi Bear (film)|Yogi Bear, The Smurfs (film)|The Smurfs, etc."  In an article published by CraveOnline|ComingSoon.net, author Edward Douglas wrote: "The CG animation just looks weird."  Philippe Duaman said that the CGI elements are intended to "refresh and give another boost" to the characters.   

===Filming=== Mike Mitchell. Tybee Island.      The Savannah Film Office first announced that the film would shoot live action scenes in Savannah for 40 days on July 11, 2013.    City of Savannah Film Services director Jay Self said "  we are very excited to be working with Paramount   on this project   We know from experience how valuable the investment and exposure generated by a project like this can be for our community."   Will Hammargren, location specialist for the Savannah Film Office, said the film was expected to contribute $8 million to the citys economy, including booking at least 5,600 hotel room nights.    

{{multiple image align     = left direction = vertical width     = 260 image1    = Savannahcity.jpg image2    = Tybee Island14.jpg footer  Tybee Islands Pier (bottom).
}}
On September 30, 2013, the start of production filming was interrupted when Jay Self was dismissed.       According to a memorandum from Joe Shearouse, bureau chief of the leisure services department of Savannah, Self was fired for his "failure to properly plan and manage the arrangement for the movie."     It accused Self of shortcomings surrounding the filming and also cited complaints from local residents of Savannah.     Another reported reason for Selfs dismissal was a disagreement between Paramount and the Savannah Film Office, about the deal of granting the local businesses to negotiate with Paramount for potential business losses during filming.   
 beach community" sea museum. flower shops, convenience stores and coffeehouses, were converted to surfing, fudge and buoy shops, respectively.  Self said, "The changes are temporary with all buildings scheduled to be restored to their original colors after filming is complete."  
 yellow flyers handed out by the production crew to detail the inconveniences of the filming would have on them.    Actor Antonio Banderas appeared as a pirate and was filmed on a pirate ship with wheels for a car chase scene.    At one point, a film crew member caused an accident that damaged a downtown building and rushed a woman to the Candler Hospital.       William Hammargren, with the Savannah Film Services office, said that Paramount was issued a permit to use motorized vehicles within certain areas closed for filming, but the permit did not extend to the lanes. 

Filming in the downtown ended on October 18, when a   television.  After filming in the downtown, the production received mixed response from local businesses located in filming areas.  A concern raised by business owners was the choice in dates for shooting as October is "a big month for merchants along Broughton."  Some merchants suggested February, July, or August as prime filming dates. 
 Tybee Island. Slash of the band Guns N Roses was seen on set at the Tybee Pier for filming, although he does not appear in the final film.    The film crew later moved to Strand Avenue for a chase scene with extras on bicycles. 

==Music==
{{Infobox album  
 | Name = Music from The SpongeBob Movie: Sponge Out of Water
 | Type = Soundtrack
 | Artist = Various artists
 | Cover = 
 | Released = January 27, 2015
 | Recorded = 2014
 | Genre = 
 | Length = 10:28
 | Label = Columbia Records|Columbia, i Am Other
 | Producer =
 | Reviews =
 | Chronology = SpongeBob SquarePants
 | Last album = Its a SpongeBob Christmas! Album (2012)
 | This album = Music from "The SpongeBob Movie Sponge Out of Water" (2015)
 | Next album = 
 | Misc =
}} score for the film was composed by John Debney.   It was announced that Pharrell Williams would write a song for the film with his band N.E.R.D, which is titled "Squeeze Me".  A five-song EP was released digitally on January 27, 2015. 

{{Track listing
| collapsed       = no
| headline        = Music from "The SpongeBob Movie: Sponge Out of Water" - EP
| extra_column = Artist
| title1 = Squeeze Me
| extra1 = N.E.R.D.
| length1 = 2:34
| title2 = Patrick Star
| extra2 = N.E.R.D.
| length2 = 1:46
| title3 = Sandy Squirrel
| extra3 = N.E.R.D.
| length3 = 3:01
| title4 = Team Work
| extra4 = Tom Kenny & Mr. Lawrence
| length4 = 1:07
| title5 = Thank Gosh Its Monday
| extra5 = Tom Kenny, Bill Fagerbakke, & Clancy Brown
| length5 = 2:40
}}
It was also announced on February 16, 2015 by Debney via Twitter that Varèse Sarabande will release his score to the film digitally on March 23, 2015 in the UK and March 24, 2015 in the US, along with a physical release on March 31, 2015. 
{{Track listing
| collapsed       = no
| headline       = The SpongeBob Movie: Sponge Out of Water (Original Motion Picture Score)
| title1 = Burger Beard on Island
| length1 = 3:09
| title2 = Burger Beard Starts to Read Story
| length2 = 0:35
| title3 = Plankton Attack / Tank Defeat / Giant Robot / Trying to Steal Formula
| length3 = 4:07
| title4 = Torturing Plankton / Refund
| length4 = 3:18
| title5 = Escaping in a Bubble
| length5 = 2:33
| title6 = The End / Get Him
| length6 = 5:06
| title7 = Going to Sleep / Inside SpongeBobs Brain
| length7 = 2:09
| title8 = Getting the Key / Plankton Rescues Karen
| length8 = 1:53
| title9 = Intro Bubbles
| length9 = 2:08
| title10 = Stealing Formula Back / Pirate Ship and Food Truck
| length10 = 2:53
| title11 = My Very Own Food Truck / Sandy Proposes Sacrifice
| length11 = 1:49
| title12 = Bubbles to the Rescue / Beach Search for Krabby Patties
| length12 = 3:56
| title13 = Beachfront Antics / Bike Path Encounters / Home of the Krabby Patty
| length13 = 2:54
| title14 = Story Rewrites / Invincibubble
| length14 = 2:50
| title15 = Chasing Burger Beard / Team Worked
| length15 = 4:04
| title16 = Not So Fast Burger Beard / PlankTON / Real Teamwork
| length16 = 5:47
}}

==Release==

===Marketing=== Coppertone sunscreen advertisements from the 1950s, in which a dog is seen pulling the bikini bottom off of a blonde girl.  

At the San Diego Comic-Con International held on July 25, 2014, Paramount Pictures released the first footage from the film, as part of their presentation at Hall H, with Tom Kenny, SpongeBobs voice actor, hosting the panel.  The films trailer was released on July 31, 2014.    Throughout the year, trailers and 15 second teaser posters of the movie were shown.

===Theatrical release=== Universal Pictures Fifty Shades of Grey, which premiered the following week.  The film premiered on January 28, 2015 in Belgium and the Netherlands, and on January 30, 2015 in Iceland, Mexico, and Taiwan.   

===International releases===
It was announced on February 24 that Paramount Pictures, in partnership with TG4, would release the film in the Irish language, alongside the English release.    This marked the first time a major film studio released an Irish language version of a movie.  SpongeBob - An Scannán: Spúinse as Uisce premiered in Ireland on March 27, 2015. 

===Home media===
The SpongeBob Movie: Sponge Out of Water will be released on Blu-ray (2D and 3D) and DVD on June 2, 2015  and on HD Digital May 19, 2015.

 

==Reception==

===Box office===
 

 , The SpongeBob Movie: Sponge Out of Water had grossed $162.1 million in North America and $148.6 million in other territories for a total gross of $310.7 million worldwide, against a budget of $74 million. 

It has outgrossed the first SpongeBob movie, which made $140.2 million worldwide,  and is the second highest grossing film based off an animated television show, behind The Simpsons Movie ($527.1 million). 

====North America====
The film was originally expected to gross around $35 million in its opening weekend,    however, the film exceeded expectations on its opening day.  
 American Sniper, which grossed $23.3 million, marking the first time in 4 weeks a film other than American Sniper was the top grossing film. In its second weekend, the film earned an estimated $31.4 million, making a 43.2% decline and dropping to number three. On its third weekend, the film stayed at number three, grossing an estimated $16.5 million. In its fourth weekend, the film was number three again, grossing an estimated $10.8 million. 

====Other territories====
A week ahead of North America release, the film was released in five markets for the three day weekend of January 30 in other territories, and earned a gross of $8 million. $6.7 million of that came from a strong debut in Mexico.   For its second weekend of February 6, 2015, the film earned itself a gross of $16.2 million playing in theaters of 25 markets. The film opened at #1 in Brazil and Spain by grossing $4.6 million and $1.9 million respectively, while maintaining the top spot at the box office of Mexico by earning $2.4 million.  In the UK the film was released on March the 27th in time for the Easter school holidays and opened at #3 behind Cinderella (2015 film)|Cinderella and Home (2015 film)|Home.

===Critical response===
The SpongeBob Movie: Sponge Out of Water has received generally positive reviews from critics. On  , the film has a score of 62 out of 100, based on 27 critics, indicating "generally favorable reviews." 

Andrew Barker of   gave the film three out of five stars, saying "The SpongeBob Movie: Sponge Out of Water weaves a silly - and often funny - spell. Its a scrappy little B-movie that zips along rather entertainingly."  Jen Chaney of The Washington Post gave the film two out of four stars, saying "Theres something about this project that, despite checking all of the requisite plot and sensibility boxes, doesnt convey as an organic work of SpongeBob-ishness." 

Nicolas Rapold of   gave the film a positive review, saying "The jokes are consistently hilarious, with enough variety to tickle the funny bones of old salts and young fishies alike."  Claudia Puig of   gave the film two and a half stars out of four, saying "The SpongeBob Movie: Sponge Out of Water remains true to the surrealism of its animated television roots. But it also tries to force a live-action element which isnt as comfortable a fit as a certain pair of symmetrical trousers." 

===Accolades===
{| class="wikitable sortable" width="99%"
|- style="background:#ccc; vertical-align:bottom;"
! colspan="6" style="background: LightSteelBlue;" | List of awards and nominations
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Recipient(s) and nominee(s)
! Result
|- style="border-top:2px solid gray;"
|-
| 2015 Kids Choice Awards
| Favorite Animated Movie Paul Tibbitt
|  
|-
|}

==Video game==
A video game featuring a plot set directly after the film, titled   and  . 

==Sequel== Rob Moore remarked, "Hopefully, it wont take 10 years to make another film."  On April 30, 2015, via Twitter, Viacom announced a third SpongeBob SquarePants film was in development. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

{{Navboxes|list1=
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 