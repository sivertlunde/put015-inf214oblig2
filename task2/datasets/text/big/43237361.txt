Two Brides and a Baby
{{Infobox film
| name                = Two Brides and a Baby
| image               = 
| alt                 = 
| caption            = 
| director            = Teco Benson
| producer            =  Blessing Effiom Egbe
| starring            =  
| cinematography      = Abdulahi Yusuf
| editing             = Shola Ayorinde
| screenplay          = Blessing Effiom Egbe
| studio         = B Concept Network
| distributor   = Silverbird
| released            =  
| runtime            = 91 minutes
| language        = English
| country             = Nigeria
| budget         = 
| gross          =
}}

Two Brides and a Baby is a 2011 Nigerian romantic drama film directed by Teco Benson, starring Kalu Ikeagwu, OC Ukeje, Chelsea Eze, Stella Damasus-Aboderin and Okey Uzoeshi. It premiered on November 17, 2011.   It received awards and nominations at Africa Movie Academy Awards, Best of Nollywood Awards and Africa Magic Viewers Choice Awards.   

==Cast==
*OC Ukeje as Kole Badmus
*Keira Hewatch as Keche
*Stella Damasus-Aboderin as Ama
*Kalu Ikeagwu as Deji
*Okey Uzoeshi as Maye
*Chelsea Eze as Ugo
*Kehinde Bankole as Pewa

==Plot==
 
Keche (Keira Hewatch) and Bankole (OC Ukeje) believe that their relationship has been divinely planned. They plan on having an amazing wedding ceremony and believe their marriage will stand the test of time. An unexpected event gets uncovered that tests how much they are really willing to sacrifice for their union to workout.

==Reception==
It currently has a 43% rating on Nollywood Reinvented with a conclusion that "...it’s quite the placid movie, it’s not emotionally moving in any way. However, if you look at it at some kind of foundation of what we hope for in Nollywood productions then it makes the movie that much more appealing."  YNaija praised the film and concluded that "the film   more admired than loved. It shows that with the right hands, Nollywood can make a decent movie, and this is a decent film indeed.", It however singled out the yoruba performance of Deji (Kalu Ikeagwu) as the only negative in the film.  Amarachukwu Iwuala of Entertainment Express was very critical of the directing and felt that attention was not properly given to many important details of the film.  Dami Elebe of Connect Nigeria praised the plot, acting and production but felt that the editing and directing should have been better. 

==References==
 

==External links==
*  at Nollywood Reinvented
*  at Rotten Tomatoes

 
 
 
 
 
 
 