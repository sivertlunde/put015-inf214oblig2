Ahalya (film)
{{Infobox film
| name           = Ahalya
| image          =
| caption        =
| director       = Babu Nanthankodu
| producer       =
| writer         = Alappuzha Karthikeyan
| screenplay     = Alappuzha Karthikeyan
| starring       = Sheela Prathapachandran Aranmula Ponnamma Balan K Nair
| music          = KJ Joy
| cinematography = Jagadeesh
| editing        =
| studio         = Alli Arts
| distributor    = Alli Arts
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film,  directed Babu Nanthankodu. The film stars Sheela, Prathapachandran, Aranmula Ponnamma and Balan K Nair in lead roles. The film had musical score by KJ Joy.   

==Cast==
*Sheela
*Prathapachandran
*Aranmula Ponnamma
*Balan K Nair Latha
*Nagaraj

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Bichu Thirumala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Happy Music || S Janaki, B Vasantha || Bichu Thirumala ||
|-
| 2 || Lalithaa Sahasranamam || S Janaki, Chorus || Bichu Thirumala ||
|-
| 3 || Sreebhootha Bali || K. J. Yesudas || Bichu Thirumala ||
|-
| 4 || Vellathaamarayithalazhako || K. J. Yesudas || Bichu Thirumala ||
|}

==References==
 

==External links==
*  

 
 
 


 