Scrooge (1970 film)
 
 
{{Infobox film
| name           = Scrooge
| image          = Scrooge1970Film.jpg
| caption        = Theatrical release Poster by Joseph Bowler
| director       = Ronald Neame
| producer       = Robert H. Solo
| screenplay     = Leslie Bricusse
| based on          = A Christmas Carol by Charles Dickens
| starring       = Albert Finney Alec Guinness Edith Evans Kenneth More Michael Medwin Laurence Naismith
| music          = Leslie Bricusse
| cinematography = Oswald Morris
| editing        = Peter Weatherley
| studio         = Cinema Center Films
| distributor    = National General Pictures
| released       =  
| runtime        = 113 minutes
| language       = English
| country        = United Kingdom
}}
 Ian Fraser. Golden Globe Award for Best Actor in a Musical/Comedy in 1971.

The film received four Academy Award nominations. It is the only live-action version of the story to be nominated for Oscars.

== Plot ==
Ebenezer Scrooge (Albert Finney) is a cold-hearted and greedy old miser whose only concern is money and profit and hates everything to do with Christmas.  After Scrooge scares off a group of boys who were singing a carol outside his door, his nephew Fred (Michael Medwin) arrives to invite him to Christmas dinner with his wife and friends.  Scrooge, however, refuses.  After Fred leaves, Scrooge gives his clerk Bob Cratchit (David Collings) the next day off as it is Christmas, but he expects him back all the earlier the next morning.  Bob meets two of his children, including Tiny Tim (Richard Beaumont), in the streets, and they buy the food for their Christmas dinner.  Scrooge, meanwhile, is surveyed by two other men (Derek Francis and Roy Kinnear) for a donation for the poor but Scrooge refuses to support the prisons and workhouses and even says "if they rather die, then they better do it and decrease the surplus population."  On his way home, Scrooge meets some of his clients, including Tom Jenkins (Anton Rogers), and reminds them the debts they owe him.  In a running gag, Scrooge is stalked and being made fun of by the same street urchins seen at the start of the film, calling him "Father Christmas."

Back home, Scrooge notices that the image of his late partner Jacob Marley (Alec Guinness) appears in the doorknocker, followed by a hearse passing him up the stairs.  While eating some soup, Scrooge hears bells ring before the ghost of Marley arrives in person covered in chains.  Scrooge thinks it is just a hoax, but sees reason after Marley frightens him.  Marley tells Scrooge he wears the chain he made with the sins he committed while alive on Earth and tells Scrooge he is close to suffering the same fate as him.  After Marley shows Scrooge other ghosts suffering the same fate, he returns him home and tells him he will be haunted by three more spirits and the first will call at one oclock.

As Marley said, the Ghost of Christmas Past (Edith Evans), a Victorian upper-class woman, arrives and takes Scrooge to witness his past.  Scrooge sees the time he spent the holidays alone at school, until his sister Fan came to collect him.  Scrooge then sees when he had a happier Christmas working for Mr. Fezziwig (Lawrence Naismith) and falling in love with his daughter, Isabel (Suzanne Neve).  Scrooge proposes to Isabel, yet after a long period of being taken for granted, Isabel calls off the engagement as she realizes Scrooges love for wealth has replaced his love for her.

The Ghost of Christmas Present (Kenneth More), a jolly giant, visits next and shows Scrooge the home of Bob Cratchit and his family.  Scrooge learns that Tiny Tim is very ill, and the spirit warns if the shadows of the future dont change, the boy will die.  They then pay a visit to Fred, his wife (Mary Peach) and friends at Freds home, where they toast Scrooge and play The Ministers Cat.  Finally, the ghost leaves Scrooge, but not before telling him life is too short.

The last of the ghosts, the Ghost of Christmas yet to Come (Paddy Stone), shows Scrooge what will happen the following Christmas.  Scrooge and the spirit witness Tom and the other citizens rejoice at Scrooges future funeral (Scrooge comically thinks he is being treated like a celebrity).  Scrooge discovers Tiny Tim had died and is shown his own future grave.  The spirit reveals himself as a skeleton (possibly Death or the Grim Reaper).  This causes Scrooge to fall into his "grave."  In the bowels of Hell, Scrooge reunites with Marley, who has him adorned with a huge chain made of his past sins by several devils.  Scrooge yells for help and finds himself back in his room.

Scrooge discovers he has time to put things right and becomes a generous man.  He goes on a shopping spree Christmas morning, buying food and presents, when he runs into Fred and his wife and gives them some overdue presents as well. They invite Scrooge to Christmas lunch, which he gladly accepts. Scrooge, dressed as Father Christmas, then delivers a giant turkey, presents and toys to the Cratchits, and after making his identity known, announces to Bob that he is doubling his salary and promises that they will work to find the best doctors to make Tiny Tim better. Scrooge then frees all his clients from their debts, much to their delight.  Scrooge returns home to get ready for lunch with his family and thanks Marley for helping him at a second chance at life.

==Cast of characters==
 
* Albert Finney as Ebenezer Scrooge
* Alec Guinness as Jacob Marley|Marleys ghost
* Edith Evans as Ghost of Christmas Past
* Kenneth More as Ghost of Christmas Present
*  Paddy Stone as Ghost of Christmas Yet to Come
* David Collings as Bob Cratchit
* Frances Cuka as Mrs. Cratchit Tiny Tim
* Michael Medwin as Fred, Scrooges nephew
* Mary Peach as Freds wife Gordon Jackson as Tom, Freds friend
* Anton Rodgers as Tom Jenkins
* Laurence Naismith as Fezziwig
* Kay Walsh as Mrs. Fezziwig
* Suzanne Neve as Isabel
* Derek Francis as charity gentleman
* Roy Kinnear as charity gentleman
* Geoffrey Bayldon as Pringle, the toyshop owner
* Molly Weir as woman debtor
* Helena Gloag as woman debtor
* Reg Lever as Punch and Judy man
* Keith Marsh as well wisher
* Marianne Stone as party guest
 

==Soundtrack listing==
# "Overture" (removed from current Blu-ray release)
# "A Christmas Carol" – Chorus
# "Christmas Children" – David Collings & Cratchit Children
# "I Hate People" – Albert Finney
# "Father Christmas" – Urchins
# "See the Phantoms" – Alec Guinness
# "December the 25th" – Laurence Naismith, Kay Walsh & Ensemble
# "Happiness" – Suzanne Neve
# "A Christmas Carol" (Reprise) – Chorus
# "You....You" – Albert Finney
# "I Like Life" – Kenneth More & Albert Finney
# "The Beautiful Day" – Richard Beaumont
# "Happiness" (Reprise)
# "Thank You Very Much" – Anton Rodgers & Ensemble
# "Ill Begin Again" – Albert Finney
# "I Like Life" (Reprise) – Albert Finney
# "Finale: Father Christmas" (Reprise) / "Thank You Very Much" (Reprise) – All
# "Exit Music" (Bonus Track, not included on LP)

A soundtrack album containing all the songs from the film was issued on Columbia Records in 1970. Due to legal complications, however, the soundtrack has never been re-released in the CD format. The current Paramount Blu-ray release of the film has removed the Overture (intact on all VHS and DVD releases).

==Title Sequence==
The film features an opening title sequence of numerous hand-painted backgrounds and overlays by British illustrator Ronald Searle. Art of the Title described it, saying, "As is often the case with Searle’s illustrations, the forms jump and squiggle into shape, the strokes loose and sprightly. In each scene, swaths of colour and life pour out, white snowflakes dotting the brush strokes."  The illustrations later appeared in a book, Scrooge, by Elaine Donaldson and published in 1970 by Cinema Center Films.

==Acclaim== Golden Laurel The Best Motion Picture Actor in a Musical/Comedy in 1971. Finney was only 34 years old at the time he was chosen to play both the old miser and the young man Scrooge of flashback scenes, but his performance was widely praised by the critics and the public. Several critics, however, found fault with Leslie Bricusses score.  

;Academy Award nominations: Best Art Direction (Terence Marsh, Robert Cartwright, Pamela Cornell) Costume Design (Margaret Furse) Best Original Song Best Score

==Stage adaptation==
In 1992, a  .

The show was revived in 2003 on a tour of the country by British song and dance man Tommy Steele, and he again reprised the role at the London Palladium in 2004 -making him the performer to have done the most shows at the Palladium. In 2007, Shane Ritchie played the part at the Manchester Palace. The musical was revived at London Palladium in October 2012 with Steele reprising the role. It ran till 5 January 2013. 

== See also ==
* List of ghost films
* List of A Christmas Carol adaptations

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 