Billy the Kid Versus Dracula
{{Infobox film
| name = Billy the Kid vs. Dracula
| image = btkvd.jpg
| caption = Billy the Kid vs. Dracula Poster
| director = William Beaudine
| producer = Carroll Case
| writer = Story: &nbsp;&nbsp;  &nbsp;&nbsp; Chuck Courtney
| music = Raoul Kraushaar
| cinematography = Lothrop B. Worth
| editing = Roy V. Livingston
| distributor = Embassy Pictures
| released =  
| runtime = 73 min.
| country = United States English
}} American low-budget western film the eponymous Universal Studios movie sequels to the Bela Lugosi classic). The films were produced by television producer Carroll Case for Joseph E. Levine. Oddly, the name "Dracula" is never mentioned in the film, nor is Carradine credited as playing him in the  credits. 

==Plot==
The film centers on Draculas plot to convert Billy the Kids fiancee, Betty Bentley, into his vampire wife. Dracula impersonates Bentleys uncle and schemes to make her his vampiric bride.

Fortunately for Betty, a German immigrant couple come to work for her and warn Bentley that her "uncle" is a vampire. While Bentley does not believe them, their concerns confirm Billys suspicions that something is not quite right with Bettys uncle.

Eventually, the Count kidnaps Betty and takes her to an abandoned silver mine. Billy confronts the Count but soon finds that bullets are no match for a vampire. The Count subdues the notorious outlaw and sets out to transform Betty into his vampire mate. Just then, the town sheriff and a country doctor arrive. The doctor hands Billy a scalpel telling him he must drive it through the vampires heart. Billy throws his gun at the vampire and knocks him senseless, making him easy pickings for a staking. With the count destroyed, Betty is saved and Billy takes her away, presumably to live happily ever after.

==Cast== Chuck Courtney William Billy the Kid Bonney
* John Carradine as Count Dracula
* Melinda Plowman as Elizabeth (Betty) Bentley
* Virginia Christine as Eva Oster
* Walter Janovitz as Franz Oster
* Bing Russell as Dan Red Thorpe
* Olive Carey as Dr. Henrietta Hull
* Roy Barcroft as Sheriff Griffin
* Hannie Landman as Lisa Oster Richard Reeves as Pete (saloonkeeper)
* Marjorie Bennett as Mary Ann Bentley William Forrest as James Underhill George Cisar as Joe Flake
* Harry Carey, Jr. as Ben Dooley (wagonmaster)
* Leonard P. Geer as Yancy (as Lennie Geer)
* William Challee as Tim (station agent)

==See also==
* Curse of the Undead
*Weird West
* Vampire film

==References==
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 