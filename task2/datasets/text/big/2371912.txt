Jesse James (1939 film)
 
{{Infobox film
| name        = Jesse James
| image       = 1939.jesse.james.jpg
| caption     = Jesse James movie poster
| writer      = Nunnally Johnson
| starring    = Tyrone Power Henry Fonda Nancy Kelly Randolph Scott Henry King
| editing     = Barbara McLean
| producer    = Darryl F. Zanuck Nunnally Johnson
| distributor = Twentieth Century Fox
| released    =  
| runtime     = 106 minutes
| country     = United States
| music       = Louis Silvers George Barnes W. Howard Greene
| awards      = Film Presented 1975 Telluride Film Festival
| language    = English
| budget      = $1.6m United States currency|U.S.
}}
 Henry King and starring Tyrone Power, Henry Fonda, Nancy Kelly and Randolph Scott. Written by Nunnally Johnson, the film is loosely based on the life of the notorious outlaw from whom the film derives its name. It is "notorious for its historical inaccuracy." {{cite news 
  | title = The True Story of Jesse James Review
  | publisher = Channel Four
  | url = http://www.channel4.com/film/reviews/film.jsp?id=109599&section=review
  | accessdate = 2008-10-30 }}  The supporting cast features Henry Hull, John Carradine, Brian Donlevy, Jane Darwell and Lon Chaney, Jr..

The American Humane Association began to oversee filmmaking after a horse died when it was driven off a cliff on set.
== Plot ==

A railroad representative named Barshee (Brian Donlevy) forces farmers to give up the land the railroad is going to go through, giving them $1 per acre (much less than fair price) for it. When they come to Jesses home, Jesse (Tyrone Power) tells Barshee that his mother Mrs Samuels (Jane Darwell) is the farms owner. Barshee repeatedly tries to force her into selling, until her other son Frank James (Henry Fonda) gets involved. Frank fights and easily beats Barshee, but Barshees men get involved and Jesse shoots him in the hand. When arrest warrants are issued for Frank and Jesse, Major A. Rufus Cobb (Henry Hull) editor in nearby Liberty, Missouri and uncle of Zerelda (Zee) Cobb (Nancy Kelly), Jesses lover, quickly comes to tell them to leave. Frank and Jesse learn that Barshee is responsible for the death of their mother and Jesse kills him in revenge. This begins Frank and Jesses career as outlaws. Three years later, with a $5,000 reward on his head, Jesse marries Zee and turns himself in, having been promised a light sentence by Marshall Will Wright (Randolph Scott). But the judge overrules Marshall Wrights recommendation and Jesse is given a stiff sentence. Frank breaks Jesse out of jail but is captured in the process. Jesse continues his life of crime and eventually Zee leaves him, taking their son Jesse Jr. Years later, a wounded Jesse returns home and Zee joins him in the belief that they will escape to California. Meanwhile, Frank has escaped and sends Bob Ford (John Carradine) to Jesse with a message. But Bob Ford betrays and kills Jesse instead.

==Cast==
*Tyrone Power as Jesse James
*Henry Fonda as Frank James Zerelda "Zee" James
*Randolph Scott as Will Wright
*Henry Hull as Maj. Rufus Cobb Robert Ford Charlie Ford
*Ernest Whitman as Pinkie
*Slim Summerville as Jailer
*J. Edward Bromberg as Mr. Runyan
*Brian Donlevy as Barshee
*Donald Meek as McCoy
*Claire Du Brey as Mrs. Bob Ford
*Jane Darwell as Mrs. Samuels
*Lon Chaney, Jr. as James Gang member
*Eddy Waller as Deputy

==Reception== Gone with The Wizard The Hunchback of Notre Dame, and in front of Mr. Smith Goes to Washington. A sequel, The Return of Frank James, directed by Fritz Lang and with Henry Fonda reprising his role as Frank James along with a variety of other actors playing the same characters as they had in Jesse James, was released in 1940.

A remake was directed by Nicholas Ray in 1957, The True Story of Jesse James. 

==Animal cruelty==
The film gained a measure of notoriety for a scene in which a horse falls to its death down a rocky slope toward the end of the film. This scene was one of many cited by the American Humane Association against Hollywoods abuse of animals, and led to the associations monitoring of filmmaking.    However, according to Leonard Mosleys biography Zanuck: The Rise and Fall of Hollywoods last Tycoon, none of "the horses   been injured. Under Zanucks direction, a short distance down the cliff, on a conveniently broad platform, the unit coper had arranged a soft landing for the horses." 

==Production==
Much of the filming for Jesse James took place around the town of Pineville, Missouri in McDonald County, Missouri, because at the time the town and surrounding area looked much the same as it would have in the 1880s and 1890s. Pineville still celebrates Jesse James Days annually in homage to the film and the movie stars who descended on the small town to make it. In their off time from filming, the films stars and crew, including Tyrone Power, Henry Fonda and Randolph Scott, would seek out relaxation at the Shadow Lake resort in Noel, Missouri, on the shores of Elk River (Oklahoma).

== See also ==
* List of American films of 1939
* The Return of Frank James (1940 in film|1940), sequel starring Henry Fonda.

== References ==
 

== External links ==
 
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 