The Deadly Trap
 
{{Infobox film
| name           = The Deadly Trap
| image          = La-Maison-sous-les-arbres.jpg
| caption        = French-language poster for the The Deadly Trap
| director       = René Clément
| producer       = Georges Casati Robert Dorfmann Bertrand Javal
| writer         = Arthur Cavanaugh Daniel Boulanger Sidney Buchman René Clément
| narrator       =
| starring       = Faye Dunaway
| music          =
| cinematography = Georges Pastier Andréas Winding
| editing        = Françoise Javet
| distributor    =
| released       =  
| runtime        = 96 minutes
| country        = France
| language       = French
| budget         =
}}

The Deadly Trap ( ) is a 1971 French drama film directed by René Clément. It was screened at the 1971 Cannes Film Festival, but was not entered into the main competition.   

==Cast==
* Faye Dunaway - Jill
* Frank Langella - Philippe
* Barbara Parkins - Cynthia
* Karen Blanguernon - Miss Hansen
* Raymond Gérôme - Commissaire Chameille
* Michele Lourie - Cathy (as Michèle Lourie)
* Patrick Vincent - Patrick
* Gérard Buhr - Le psychiatre / Psychiatrist
* Louise Chevalier
* Maurice Ronet - Lhomme de lorganisation / Stranger
* Tener Eckelberry
* Massimo Farinelli
* Jill Larson
* Robert Lussac
* Franco Ressel
* Dora van der Groen

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 