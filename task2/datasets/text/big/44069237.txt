Brooklyn Orchid
{{Infobox film
| name           = Brooklyn Orchid
| image          = Brooklyn Orchid poster.jpg
| alt            = 
| caption        = Theatrical release poster Kurt Neumann
| producer       = Hal Roach
| screenplay     = Earle Snell Clarence Marks 
| starring       = William Bendix Joe Sawyer Marjorie Woodworth Grace Bradley Richard "Skeets" Gallagher Florine McKinney Leonid Kinskey Edward Ward
| cinematography = Robert Pittack 	
| editing        = Ray Snyder 
| studio         = Hal Roach Studios
| distributor    = United Artists
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Kurt Neumann and written by Earle Snell and Clarence Marks. The film stars William Bendix, Joe Sawyer, Marjorie Woodworth, Grace Bradley, Richard "Skeets" Gallagher, Florine McKinney and Leonid Kinskey. The film was released on January 31, 1942, by United Artists.   

==Plot==
 

== Cast == 
*William Bendix as Timothy Tim McGuerin
*Joe Sawyer as Eddie Corbett
*Marjorie Woodworth as Lucy The Brooklyn Orchid Gibbs
*Grace Bradley as Sadie McGuerin
*Richard "Skeets" Gallagher as Tommy Lyman Goodweek
*Florine McKinney as Mabel Cooney
*Leonid Kinskey as Ignatz Rachkowsky
*Rex Evans as Sterling, McGuerins Butler
*Jack Norton as Jonathan McFeeder 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 