Amarashilpi Jakanachari (film)
{{Infobox film 
| name           = Amarashilpi Jakanachari
| image          =  
| caption        = 
| director       = B. S. Ranga
| producer       = B. S. Ranga
| writer         = Shyamala Devi
| screenplay     = Balasubramanyam
| starring       = Kalyan Kumar B. Sarojadevi Udaykumar V. Nagaiah
| music          = S. Rajeswara Rao
| cinematography = B N Haridas
| editing        = P G Mohan M Devendranath Chakrapani
| studio         = Vikram Productions
| distributor    = Vikram Productions
| released       =  
| country        = India Kannada
}}
 1964 Cinema Indian Kannada Kannada film, directed and produced by B. S. Ranga. The film stars Kalyan Kumar, B. Sarojadevi, Udaykumar and V. Nagaiah in lead roles. The film had musical score by S. Rajeswara Rao.  

==Cast==
 
*Kalyan Kumar
*B. Sarojadevi
*Udaykumar
*V. Nagaiah
*Dhoolipala
*Dhoolipala Narasimharaju
*D. Raghavendra Rao
*H. R. Shastry
*Balaraju
*Shyam
*Srinivas
*Ramadevi
*Girija
*Pushpavalli
*Shakunthala
*H. B. Soraja
*Chiranjeevi Kaleshwari
*Jayalalithaa in a dance sequence
*Ratan Kumar in a dance sequence
 

==References==
 

==External links==
*  
*  

 
 
 


 