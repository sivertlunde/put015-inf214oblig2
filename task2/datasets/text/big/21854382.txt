Days of Nietzsche in Turin
{{Infobox Film
| name = Days of Nietzsche in Turin
| image = 
| image_size = 
| caption =
| director = Júlio Bressane
| producer = 
| writer = Júlio Bressane Rosa Dias
| narrator =
| starring =Fernando Eiras Paulo José Tina Novelli Mariana Ximenes Leandra Leal Paschoal Villaboin Isabel Themudo
| music = 
| cinematography = 
| editing = 
| distributor =  Grupo Novo de Cinema e TV Riofilme
| released =  
| runtime = 85 minutes
| country =   Portuguese
| budget = 
| preceded_by =
| followed_by =
}}
 Brazilian film directed by Júlio Bressane about the German philosopher Friedrich Nietzsche.

==Plot== Ecce Homo and Twilight of the Idols .

==Cast==
*Fernando Eiras as Friedrich Nietzsche
*Paulo José
*Tina Novelli
*Mariana Ximenes
*Leandra Leal
*Paschoal Villaboin
*Isabel Themudo

==Awards and nominations==

* Cinema Brazil Grand Prize, 2003 (Brazil) - Nominated in category of Best Picture
* Venice Film Festival, 2001 (Italy) - Winner of Filmcritica "Bastone Bianco" Award (Júlio Bressane).
* Candango Trophy, 2001 - Winner of Best Screenplay (Rosa Dias and Júlio Bressane)

==See also==

* Brazilian films of 2001

== External links ==
*  

 
 
 
 
 
 
 
 
 