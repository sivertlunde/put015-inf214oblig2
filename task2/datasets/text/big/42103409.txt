Try Not to Breathe (film)
 
{{Infobox film
| name           = Try Not to Breathe 
| image          = Try not to breathe poster.jpg
| caption        = 
| director       = Alina Abdullayeva
| producer       = 
| writer         = Alina Abdullayeva
| starring       = Fakhraddin Manafov, Dilara Kazimova
| music          = Ali Hasanov, Tora Aghabeyova
| cinematography = Rovshan Guliyev
| editing        = 
| distributor    = 
| released       = 
| runtime        = 15 minutes
| country        = Azerbaijan Switzerland
| language       = Russian
| budget         = 
}}

Try Not to Breathe ( ,  ) is a 2006 Azerbaijani short film directed by Alina Abdullayeva.

In 2007 the film took part at the Clermont-Ferrand International Short Film Festival.  In 2008 the film participated at the Salento Finibus Terrae International Short Film Festival where the films director Alina Abdullayeva won an award for Best Director.   // www.eepap.org. 

==Cast==
* Fakhraddin Manafov
* Dilara Kazimova

==Plot==
A few minutes in the life of two people: a young woman (Dilara Kazimova) who has her whole life in front of her and an old man (Fakhraddin Manafov) ill with asthma who has only one more night to live.

==Awards==
* 2004 - Grant for Alina Abdullayeva for the short film project “Try not to breathe” awarded by AVANTI training programme (implemented by FOCAL, funded by Swiss Agency for Development and Cooperation|SDC) 
* 2006 - Award for “Best debut” of National film academy“Golden lamp” (Baku, Azerbaijan) 
* 2006 - Award for “Best producer” at International Audio Visual Film Festival (Baku, Azerbaijan) 
* 2008 - Award for Best director at Salento Finibus Terrae International Short Film Festival (Italy) 

==References==
 

==External links==
*   //www.clermont-filmfest.com.

 
 
 
 
 


 