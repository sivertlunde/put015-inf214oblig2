Adultery Diary: One More Time While I'm Still Wet
{{Infobox film
| name = Adultery Diary: One More Time While Im Still Wet
| image = Adultery Diary One More Time While Im Still Wet.jpg
| caption = DVD cover for Adultery Diary: One More Time While Im Still Wet (1996)
| director = Toshiki Satō 
| producer = Daisuke Asakura
| writer = Masahiro Kobayashi
| narrator = 
| starring = Hotaru Hazuki
| music = 
| cinematography = 
| editing = Naoki Kaneko
| distributor = Kokuei / Shintōhō
| released =  
| runtime = 59 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}
  is a 1996 Japanese Pink film directed by Toshiki Satō. It was chosen as Best Film of the year at the Pink Grand Prix ceremony. The film was released on video as  . 

==Synopsis==
A bored housewife dreams of becoming a novelist. She enrolls in a writing class and commences an affair with her tutor. 

==Cast==
* Hotaru Hazuki
* Yukiko Izumi
* Takeshi Itō
* Kazuhiro Sano

==Critical reception==
Besides winning Best Film spot at the Pink Grand Prix, lead actress Hotaru Hazuki was awarded Best Actress and Masahiro Kobayashi won Best Screenplay at the awards ceremony.  As an indication of the growing female audience for the pink film genre in recent years, the film was featured as a "Ladies Theatre" selection on the AII broadband service. The site described Adultery Diary as a pink Ghost (1990 film)|Ghost.  

==Bibliography==

===English===
*  

===Japanese===
*  
*  
*    

==Notes==
 

 
 
 
 
 

 
 

 
 
 
 
 


 
 