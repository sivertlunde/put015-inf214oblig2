Ultimum
 
 
 
{{Infobox film
| name           = Ultimum
| image          = Ultimum Manifest Poster.jpg
| caption        =
| director       = Bahadır Karasu
| producer       = Ekrem Doydu
| writer         = Bahadır Karasu
| based on       = Idea 
| narrator       = Michael Masterson
| starring       = Michael Masterson, Siyamek Rezael
| music          = Lukasz Lowkis, Bryan Nguyen, Marita MacBride, Panos Panakos
| cinematography = Bahadır Karasu
| editing        = Bahadır Karasu
| distributor    = Pirana Film
| released       =  
| runtime        = 28 minutes
| country        = Turkey
| language       = English
| budget         = 200 DLR
}}

Ultimum  is a 2014 short film directed by Bahadır Karasu. The film was shot for Gianluigi Lucarellis Manifest "Dystopia". 
Ultimum means "utterly, finally, at last, extremely" in English as the film mostly refers to "ending". Ultimum stands out with its diverse story telling and editing.

==Plot==
Ultimum is about a middle-aged man who experiences many unfortunate incidents in a very short time at a very catastrophic time in his life. He discovers unusual points and make the things shaped again by undefined occurrence of a mysterious man near him. The story begins with a man opening his eyes in a room and subsequently he sees the unknown man sitting front of him and they begin to talk. Most of the film takes place in this room with their interesting conversation and some of the lines we see in the movie are quotes belong to famous or non-famous writers and journalists. Film mostly remarked and ask that "What are we really living ?" Destiny itself just like we are living whats already written or we write the destiny itself?

==References==
 

==External links==
* 
*  
* https://soundcloud.com/bryguymusic Composer Bryan Nguyen

 
 
 
 
 
 