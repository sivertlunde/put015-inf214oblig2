Hair Is Falling
{{Infobox film
| name           = Hair is Falling
| image          =
| caption        = Theatrical release poster
| director       = Vicky Bhardwaj  
| producer       = Urmila Sharma & Guddu ji
| writer         = Vikas Taliyan
| screenplay     = 
| story          = 
| based on       =  
| starring       = Rajesh Bhardwaj
| music         = Arjuna Harjai , Arpan sumit & Kamran Ahmed  . IMDB.com 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 84 Minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Hair is Falling  is a 2011 Hindi film produced by Urmila Sharma, Guddu ji and directed by Vicky Bhardwaj. The film highlights the problem of hair fall in youth. 
Tag line : A Serious Comedy

== Plot ==

A college boy starts suffering with the problem of hair loss but does not take it seriously, later it gets serious because his career as a model was getting affected due to lesser hair.
First half of the film is mostly about youth and college life, this is where the problem of hair fall starts. 
Second half shows tricks and techniques of growing hair and is presented very sarcastically, slowly moving towards a love plot ending it all on a positive note.

==Cast==
* Rajesh Bhardwaj  
* Sanjana Tiwari as Sanju
* Priyanka as Kanika Batra
* Mahender Singh Bist as Sandeep
* Manoj Pahwa as Hair Clinic Doctor
* Bharti Singh as Jogger
* Gagan Guru

==Music==

=== Original Score===
The Opening theme and Original Background score was composed by  Arjuna Harjai  

===Songs===
The songs for Hair is Falling are composed by Arpan Sumit & Kamran Ahmed. 
 
 

== Box office ==
The film was released in  India on August 5, 2011.
 

==References==
 
The DVD and CD of the film is launched by Junglee Home Video (Powered by The Times Group).

==External links==
* 
* 

 
 
 
 
 
 
 
 
 