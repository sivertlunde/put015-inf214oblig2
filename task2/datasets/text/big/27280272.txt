Simba (film)
{{Infobox film
| name           = Simba
| image          = Simbpos.jpg
| caption        = Original film poster
| director       = Brian Desmond Hurst
| producer       = Peter De Sarigny
| writer         = John Baines Robin Estridge Anthony Perry (story)
| starring       = Dirk Bogarde Virginia McKenna Donald Sinden
| music          = 
| cinematography = Geoffrey Unsworth
| editing        = 
| studio         = 
| distributor    =  The Rank Organisation
| released       =  1955
| runtime        = 
| country        = United Kingdom
| language       = English
}} British drama film directed by Brian Desmond Hurst and starring Dirk Bogarde, Donald Sinden, Virginia McKenna and Basil Sydney.  A British family living in East Africa become embroiled in the Mau Mau Uprising. 

==Plot==
Alan Howard (Dirk Bogarde) visits Kenya to see his brother, who he discovers has been murdered by Mau Mau.

==Production== The Planters Wife (1952) saw Rank become interested in making films about other contemporary Imperial stories and Earl St. John put out a call for story submissions to do with the Mau Mau Uprising. Anthony Perry obliged with a treatment and he was sent to Kenya, where his advisers included Charles Njonjo. The script was later rewritten by another writer. 
 
The film was shot at Pinewood Studios, with second unit photography in Kenya. The producers had originally hoped to cast Jack Hawkins in the lead and used a double in Kenya to match him in long shot. When Hawkins was unavailable, Bogarde was cast instead and much of the Kenyan footage covering Hawkins could not be used.  However, they had also used a tall, blond Rhodesian policeman as the long shot stand-in for the part of Inspector Drummond, but had difficulty finding an available blond actor in England to play the part and so match up the shots. A chance meeting in the bar at Pinewood between the director Brian Desmond Hurst and Donald Sinden, who had had to dye his hair blond for the comedy film Mad About Men, led to Sinden being cast as Drummond. 

==Cast==
* Dirk Bogarde - Alan Howard
* Donald Sinden - Inspector Drummond
* Virginia McKenna - Mary Crawford
* Basil Sydney - Mr Crawford
* Marie Ney - Mrs Crawford
* Joseph Tomelty - Doctor Hughes Earl Cameron - Karanja
* Orlando Martins - Headman
*  Ben Johnson - Kimani
* Frank Singuineau - Waweru
* Huntley Campbell - Joshua
* Slim Harris - Chege
* Glyn Lawson - Mundati
* Harry Quashie - Thakla John Chandos - Settler
* Desmond Roberts - Colonel Bridgeman 
* Errol John - African Inspector

==References==
 

==External links==
*  
*  at TCMDB
* 
*  at Screenonline

 
 
 
 
 
 
 
 
 
 
 

 