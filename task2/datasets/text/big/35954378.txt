Barbie: The Princess & the Popstar
 
{{Infobox film
| name           = Barbie: The Princess and The Popstar
| image          = Princess & Popstar DV.png
| alt            = 
| caption        = DVD Cover
| director       = Zeke Norton
| producer       = Shelley Dvi-Vardhana Shawn McCorkindal
| writer         = Steve Granat Cydne Clark
| based on       =   and the Barbie Movie, Barbie as the Princess and the Pauper Ashleigh Ball	
| music          = Paul J. Smith
| cinematography = 
| editing        = Treg Brown Mattel Entertainment
| distributor    = Universal Studios Home Entertainment
| released       =     September 26, 2012  (India)   
| runtime        = 86 minutes    
| country        = United States   Canada English
| budget         = 
| gross          = 
}} Barbie film Princess and The Popstar.The film features the voices of Kelly Sheridan, Jennifer Waris, Ashleigh Ball, Tiffany Giardina, Ellie King, and Peter Kelamis.

The plot focuses on Tori, the kind-hearted princess of the magical kingdom of Meribella, who would rather sing and dance than perform her royal duties. When she meets her favorite pop star Keira, the girls discover they have much in common including a magical secret that lets them look like one another and swap places. When the kingdoms magical secret is stolen, their true friendship is tested and they discover that the best thing you can be is yourself.

==Plot==
In the summer, Meribella, a magical kingdom, is going to have its five hundredth anniversary. Princess Tori and her younger sisters Meredith and Trevi are very busy, having to attend ceremonies and balls, and always be poised and proper. The princesses think it is hard work. Whenever they have the chance, they sneak away and play together. Before Meribellas five hundredth anniversary, the princesses have to greet royal visitors, who come to Meribella for the occasion. Tori wishes she could have an exciting, music-filled life like her favorite popstar, Keira. She wishes she could watch Keira performing near her palace.

Keira, a world famous singer-songwriter, is on the guest list and is going to perform at the festival for the Meribellas five hundredth anniversary. Keira enjoys touring, greeting fans and having extravagant, spectacular concerts. However, it leaves her with no time to do what she was passionate about: writing songs.

Keira thinks being a princess would be awesome. The next day, members of the royal court are invited to the palace in Meribella to meet the royal family. Duchess Amelia, Toris aunt, forbids Tori to watch Keiras concert when she was supposed to be greeting guests.
When Keira arrives to the palace for the festival, she meets Tori. They instantly become friends, and their pet dogs Vanessa and Riff, too. Tori gives Keira a tour of the palace and so they began joking about trading places. Keira tells Tori how she longs to be a princess and wear a tiara like her. Tori gives Keira her tiara to try it on. Keira takes out her magic microphone and magically turns her outfit into a gown. Tori then shows her magical brush to Keira. Tori uses her magic hairbrush, so she uses it to transform her hair into a different hairstyle. They then realize that they look just alike.

Tori convinces Keira to stay disguised as each other while Tori carries on giving Keira a tour of her palace. They enter a secret garden, where fairies tend the plants. There is a diamond gardenia flower, which were used to pay for school and other things. Tori tells Keira that the Diamond Gardenia roots spread through the entire kingdom. Without it, Meribellas plants would die. The Diamond Gardenia made the kingdom magical. Tori scoops up two diamonds underneath the Diamond Gardenia. The fairies put the diamonds on their necklaces, making the necklaces their friendship necklaces. Outside, Keiras manager Crider who Keira calls Seymour is tired of not being a star and comes to the palace to find valuables while getting a tour from Toris aunt. Toris aunt then notices that Tori has bought a stranger into the secret area. Keiras manager, secretly hearing this, makes a plan to steal the diamond plant. Toris aunt scolds Keira about bringing strangers into the secret garden area.

Tori and Keira head back to Toris room after Keira almost got into trouble. Tori and Keira realize they want to continue to be each other and agree to remain as each other for one more day. Tori teaches Keira how to be princess through a song and Keira teaches Tori how to be a popstar. "Keira" gives autographs to people and enjoys it, but later learns about a drought that she should have been aware about. Meanwhile, "Tori" learns to behave like a princess. Prince Liam starts to fall in love with "Tori" for who she is. "Tori" also learns a new song while playing with Toris younger sisters. Later, "Tori" gets locked in by Toris aunt because "Tori" didnt finish a writing a speech and tries to get out while "Keira" sings a song for the first time on stage and says to the people: "you have the coolest princesses in the universe, in fact princess Tori in particular is really awesome and is often misunderstood mostly by her aunt."

Later, Crider (Keiras manager, who wants to steal the diamonds and be famous) and Rupert (Criders assistant, who wants to be Criders best buddy), uproot the Diamond Gardenia. The fairies try to stop them, but their efforts are not enough. Tori and Keira stop them, and discover it is always best to be yourself.

After that, Crider flies with flying suit and attempts to fly away with the Gardenia, so Tori and Keira, using their magic microphone and brush, change his hairstyle and outfit. Crider then drops the Gardenia and it breaks into pieces. Tori and Keira take the diamonds off their friendship necklaces and plant a new Diamond Gardenia. Tori then gives her speech. They go back to the concert and end the story as best friends.

==Characters==
* Kelly Sheridan (speaking) and Jennifer Waris (singing) as Princess Victoria Bethany Evangeline Renee (Tori) – The princess of Meribella, Tori dreams of becoming a popstar like Keira. Tori is joyful and cheerful teenager princess with kind heart. She is portrayed by Barbie. Ashleigh Ball (speaking) and Tiffany Giardina (singing) as Keira – A famous popstar, Keira dreams of being a princess, like Tori. Difference Tori, Keira is more calm and organized. She is also portrayed by Barbie.
* Ellie King as Duchess Amelia – Toris strict aunt.
* Peter Kelamis as Seymour Crider – Keiras greedy and ambitious manager. Hes the main antagonist in this movie.
* Jonathan Holmes as Rupert – Criders dumb and bumbling assistant and the man who wants to be Criders best buddy.
* Allison Warnyca as Nora - Keira´s organizer.
* Christopher Gaze as King Frederick – Toris father. Kind and fair man.
* Lauren Lavoie as Princess Meredith – Toris younger sister. She is very smart and seems to be little bit serious. She is portrayed by Barbies younger sister Stacie.
* Ashlyn Drummond as Princess Trevi – Toris youngest sister. She is portrayed by Barbies youngest sister Chelsea.
* Adrian Petriw as Prince Liam – The prince who falls in love with Keira when she is disguised as Tori. He is portrayed by Ken.
* Leala Selina as Vanessa Victoria Fluffypie – Toris King Charles Cavalier Spaniel.
* Adrian Petriw as Riff – Keiras pug dog.

==Music==
*1. "Here I am (Keira version) (performed by Tiffany Giardina)
*2. "I Wish I Had Her Life" (performed by Giardina and Waris)
*3. "To Be A Princess/To Be A Popstar" (performed by Giardina and Waris)
*4. "Perfect Day" (performed by Giardina and Waris)
*5. "Look How High We Can Fly" (performed by Giardina)
*6. "Here I Am (Tori Version)" (performed by Waris)
*7. "Princess and Popstar Finale Medley" (performed by Giardina and Waris)
*8. "Princesses Just Want To Have Fun" (performed by Waris)

==References==
 

==External links==
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 