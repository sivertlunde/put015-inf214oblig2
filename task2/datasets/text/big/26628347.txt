The Yellow Canary
 
{{Infobox film
| name           = The Yellow Canary
| image          = 
| image_size     = 
| caption        = 
| director       = Buzz Kulik
| writer         = Rod Serling
| narrator       = 
| starring       = Pat Boone
| music          = 
| cinematography = 
| editing        = 
| distributor    = Twentieth Century Fox Film Corporation
| released       = June 15, 1964
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Yellow Canary is a 1963 film thriller directed by Buzz Kulik. It stars Pat Boone and Barbara Eden, and it was adapted by Rod Serling from a novel by Whit Masterson, who also wrote the novel that was the basis for Orson Welles Touch of Evil. The film was photographed by veteran Floyd Crosby and scored by jazz composer Kenyon Hopkins. 

==Cast==
*Pat Boone as Andy Paxton
*Barbara Eden as Lissa Paxton Steve Forrest as Hub Wiley
*Jack Klugman as Lt. Bonner Jesse White as Ed Thornburg
*Steve Harris as Bake
*Milton Selzer as Vecchio
*Jeff Corey as Joe Pyle
*Harold Gould as Ponelli
*John Banner as Skolman

==Plot==
Andy Paxton (Boone) is an arrogant, obnoxious pop idol who is about to be divorced by his wife (Eden) and constantly abuses his staff. On the same night he begins an engagement at the Huntington Hartford Theater in Los Angeles, his infant son is kidnapped. Despite the pleas of Lieutenant Bonner (Klugman), the lead police officer on the case, Paxton insists on playing along with the kidnappers, even though they keep stringing him along and have no problem with killing people.

==Production==
Pete Levathes, head of 20th Century Fox, authorised the studio to pay $200,000 for the rights to Whit Mastertons novel. Rod Serling was paid $125,000 to write the script and Pat Boone $200,000 for play the lead. It was intended the film have a budget of a couple of million but then Levathes was fired in the wake of the Cleopatra cost over-runs and Daryl F. Zanuck took over the studio. Zanuck gave Robert L. Lippert $100,000 to finish the film and a 12-day schedule. Mark Thomas McGee, Talks Cheap, Actions Expensive: The Films of Robert L. Lippert, Bear Manor Media, 2014 p 271-272 

In a September 2012 interview at the UCLA Film and Television Archive, Boone stated that the film was slated for a ridiculously short 12-day schedule. When they were wrapping the last day with several key scenes left to be filmed, Boone paid $20,000 out of his own pocket to buy one more day of shooting. He felt strongly about the film because it gave him the chance to play "a bad guy for a change." 

==References==
 

==External links==
* 

 

 
 
 
 