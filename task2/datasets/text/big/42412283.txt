Os Homens São de Marte... E é pra Lá que Eu Vou!
{{Infobox film
| name           = Os Homens São de Marte... E é pra Lá que Eu Vou!
| image          = Os Homens São de Marte... E é pra Lá que Eu Vou! Poster.jpg
| alt            = 
| caption        = Brazilian theatrical poster
| director       = Marcus Baldini
| producer       = 
| writer         = Mônica Martelli Suzana Garcia Patricia Corso
| based on       =  
| starring       = Mônica Martelli Paulo Gustavo Daniele Valente Eduardo Moscovis Marcos Palmeira Humberto Martins José Loreto Herson Capri Peter Ketnath Irene Ravache Julia Rabelo
| music          = Plínio Profeta
| cinematography = Pedro Farkas
| editing        = Marcelo Moraes
| studio         = BR3 Produções Artisticas Bionica Filmes Dig Produções
| distributor    = Downtown Filmes
| released       =  
| runtime        = 
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}}Os Homens São de Marte... E é pra Lá que Eu Vou! (English: Men Are From Mars ... And Thats Where Im Going!)  is a 2014 Brazilian comedy film directed by Marcus Baldini, adapted from a play of the same name written and starred by Mônica Martelli. The films cast includes Paulo Gustavo, Daniele Valente, Eduardo Moscovis, Marcos Palmeira, Humberto Martins, José Loreto, Herson Capri, Peter Ketnath, Irene Ravache and Julia Rabelo.

The film follows the story of Fernanda (Martelli), who abandoned her personal life to devote herself to the career, and now, at the age of 39,  starts thinking that is living in an emergency affective situation. From each new man passing on her life, she thinks to have found her perfect match. The play which the film is based spent nine years in theaters, with an audience of more than 2 million people. 

==Cast==
 
* Mônica Martelli as Fernanda
* Paulo Gustavo as Aníbal
* Daniele Valente as Nathalie
* Marcos Palmeira as Tom
* Eduardo Moscovis as Juarez
* Herson Capri as Man at the Art Gallery

* Humberto Martins as Robertinho 
* Irene Ravache as Maria Alice
* Peter Ketnath as Nick
*José Loreto as Marcelinho
*Maria Gladys as Dona Dedé
*Ana Lucia Torre as Aunt Sueli
*Alejandro Claveaux
*Júlia Rabello
 

==References==
 

==External links==
*  

 
 
 
 
 
 

 
 