A Tiger's Tale
 
{{Infobox film
| name           = A Tigers Tale
| image          = A Tigers Tale.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical poster
| director       = Peter Douglas
| producer       = Don Goldman Peter Douglas
| screenplay       = Peter Douglas
| based on         =   Allen Hannay III
| narrator       = 
| starring       = Ann-Margret C. Thomas Howell Charles Durning Kelly Preston
| music          = Lee Holdridge, the Textones
| cinematography = Tony Pierce-Roberts
| editing        = David Campling
| studio         = Vincent Pictures
| distributor    = Atlantic Releasing
| released       = 12 February 1988
| runtime        = 97 minutes
| country        =  
| language       =
| budget         = 
| gross          = $89,000 
| preceded_by    = 
| followed_by    = 
}} 1987 film written and directed by Peter Douglas, based on the novel Love and Other Natural Disasters by Allen Hannay III.

==Plot==
Bubber Drumm is a Houston high school student. Rose Butts is an alcoholic, more than twice his age, and the mother of his girlfriend, Shirley. Bubber and Rose begin an affair after Bubber fixes Shirley up with his pal, Ransom McKnight. 

Bubber and Rose carry on their affair under the nose of her daughter until everything comes out in the open at a drive-in movie theater. To get even with Bubber and Rose for "behaving badly", Shirley pricks a hole in Roses diaphragm. Shirley goes on to live with her father and Bubber moves in with Rose along with his pet tiger. The diaphragm incident results in Rose getting pregnant with Bubbers baby. The couple must decide whether to keep the baby and continue their May/December romance or part ways.

non score music is by the Textones (Carla Olson, Phil Seymour, Joe Read, George Callins, Tom Jr Morgan)

==Principal cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Ann-Margret || Rose Butts
|-
| C. Thomas Howell || Bubber Drumm
|-
| Charles Durning || Charlie Drumm
|-
| Kelly Preston || Shirley Butts
|-
| Ann Wedgeworth || Claudine
|-
| William Zabka || Randy
|- James Noble || Sinclair
|-
| Sean Patrick Flanery || Buddy
|}

==Critical reception==
Roger Ebert of The Chicago Sun-Times gave the film 2 out of 4 stars although he did like certain aspects of the film: 
 
Janet Maslin of The New York Times: 
 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 


 