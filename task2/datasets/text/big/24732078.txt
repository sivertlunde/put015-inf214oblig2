Journey (1972 film)
Journey Canadian film written, directed and produced by Paul Almond.

==Synopsis==

Journey is the allegorical story of a young womans struggle - outside the normal framework of space and time - to find herself.

Found drifting down the Saguenay River, half-drowned and clinging to a log, a woman (Geneviève Bujold) is rescued by Boulder (John Vernon), who carries her to Undersky, his commune in the Québec wilderness. Named after the river from which she was saved, Saguenay is haunted by memories of her past and remains unresponsive for days, drifting in and out of consciousness. She gradually becomes aware of the life going on around her and begins to explore it. But she senses she has brought ill fortune to this community and fears something in her past has doomed her and all who know her. Finally, she journeys back up the river to confront the nightmare of her past. By doing so, she breaks through to the present and arrives at the mouth of the river.

==Reception==

This is Paul Almonds last film in his trilogy with Bujold that began with Isabel (film)|Isabel (1968) and The Act of the Heart (1970), obscure and complex, it is considered the weakest of the trilogy, lacking the passion of the first two. Poorly received, it was a box-office failure and ended Almonds attempt to create art film in Québec. 

It was featured in the Canadian Cinema television series which aired on CBC Television in 1974.   

==References==
 

 
 
 
 
 
 
 