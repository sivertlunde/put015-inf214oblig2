Iceland (film)
{{Infobox film
| name           = Iceland
| image          = Iceland vhs.jpg
| image_size     =
| caption        =
| director       = H. Bruce Humberstone
| producer       = William LeBaron Robert Ellis Helen Logan
| narrator       = John Payne Jack Oakie Felix Bressart Eugene Turner
| music          =
| cinematography = Arthur Charles Miller
| editing        =
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 79 min.
| country        = United States English
| budget         =
| gross          =
}}
 John Payne as a U.S. Marine posted in Iceland during World War II. The film was titled Katina in Great Britain and Marriage on Ice in Australia. 
 Marine landing and occupation of Iceland in 1941. Payne had previously played a Marine in Foxs To the Shores of Tripoli also directed by Humberstone. Among the songs are "There Will Never Be Another You" and "You Cant Say No to a Soldier".

Some Icelanders protested against the film for its depiction of Marines winning away the local women.   Henies on-ice partner during the filmed skating sequences was 1940/41 U.S. Champion Eugene Turner.

==Notes==
 

==External links==
*  
* New York Times review http://movies.nytimes.com/movie/review?res=9F05EFDA1339E33BBC4D52DFB6678389659EDE

 
 
 
 
 
 
 
 
 
 
 
 

 