Of Men and War
{{Infobox film
| name           = Of Men and War
| image          = 
| caption        =
| director       = Laurent Bécue-Renard 
| producer       = Laurent Bécue-Renard Isidore Bethel Heinz Dill Elisabeth Garbar Thierry Garrel
| writer         = Laurent Bécue-Renard 
| starring       = 
| music          = Kudsi Erguner
| cinematography = Camille Cottagnoud 		 
| editing        = Isidore Bethel Charlotte Boigeol Sophie Brunet 
| studio         = Alice Films Télévision Suisse Romande
| distributor    = Why Not Productions Alice Films
| released       =  
| runtime        = 142 minutes
| country        = France Switzerland
| language       = English
| budget         = 
| gross          = 
}}

Of Men and War ( ) is a 2014 documentary film directed by Laurent Bécue-Renard. It explores the psychological trauma of war experienced by a group of American Iraq veterans upon their return from the front. It was presented in the Special Screenings section at the 2014 Cannes Film Festival.  The film won the VPRO IDFA Award for Best Feature-Length Documentary at the 2014 International Documentary Film Festival Amsterdam.  It was nominated for the European Film Award for Best Documentary at the 27th European Film Awards and screened at the Museum of Modern Arts Documentary Fortnight.  

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 

 