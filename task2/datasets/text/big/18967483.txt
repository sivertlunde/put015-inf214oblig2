Through the Back Door
{{Infobox film
| name = Through the Back Door
| image = Through the Back Door Poster.jpg
| caption = Theatrical release poster
| director = Alfred E. Green Jack Pickford 
| writer = Gerald C. Duffy (aka Gerald Duffy) Marion Fairfax
| starring = Mary Pickford Gertrude Astor
| producer = Mary Pickford
| cinematography = Charles Rosher
| distributor = United Artists
| budget =
| released =  
| country = United States English intertitles
| runtime = 89 minutes
}}
Through the Back Door (1921 in film|1921) is a silent film directed by Alfred E. Green and Jack Pickford, and starring Mary Pickford. 

==Plot== America and leave Jeanne behind in Belgium to live with the maid Marie (Helen Raymond). At first Louise refuses to, but eventually gives in and leaves Jeanne in the care of Marie.

Five years pass and Jeanne and Marie bonded. Meanwhile, Louise hated living in America and feels guilty having left her kid behind. She returns to Belgium to reunite with Jeanne, but Marie doesnt want to give her up. When Louise finally arrives, Marie lies to her Jeanne drowned in a river nearby. Louise is devastated and collapses, before returning to America. This results in estranging from Elton.

World War I broke out. Marie fears for Jeannes safety and brings her to America to live with her mother. After an emotional goodbye, Jeanne sets out for America to find her mother. Along the way she meets two orphan boys and decides to take care of them. When she finally arrives in America, she travels to Louises big mansion.

Too afraid to tell her she is her daughter, Jeanne applies to serve as her maid. While pretending to be someone else, she gets to know her mother. However, she has trouble keeping up the lie and wants nothing more but have a reconciliation. Waiting for the right time to tell the truth, Jeanne hopes everything will come to a right end. When guests of the mansion plot to fleece Elton, Jeanne is forced to reveal her true identity to save the day. A happy reunion follows.

==Cast==
* Mary Pickford - Jeanne
* Gertrude Astor - Louise Reeves
* Wilfred Lucas - Elton Reeves
* Helen Raymond - Marie
* C. Norman Hammond  - Jacques Lanvain
* Elinor Fair - Margaret Brewster
* Adolphe Menjou - James Brewster
* Peaches Jackson - Conrad
* Doreen Turner - Constant
* John Harron - Billy Boy
* George Dromgold - Chauffeur
* Jeanne Carpenter - Jeanne (age 5)

==References==
 

==External links==
 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 