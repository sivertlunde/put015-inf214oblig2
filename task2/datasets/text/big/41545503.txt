XXXY (film)
 
{{Infobox film
| name           = XXXY
| image          =
| alt            = 
| caption        = Kristi Bruce, San Francisco, California, 2000
| director       = Porter Gale, Laleh Soomekh 
| writer         =  Howard Devore Jorge Daaboul, MD Alice Bruce John Bruce
| music          = 
| cinematography = 
| editing        = 
| studio         = Stanford University, Department of Art & Art History
| distributor    = 
| released       =  
| runtime        = 13 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
XXXY is a short documentary by Porter Gale and Laleh Soomekh. 

The film features two people born    , released 2000. 

==Synopsis==
 Howard Devore tell their stories. Jorge Daaboul, former Director of Pediatric Endocrinology at Childrens Hospital Oakland (Calif.) and present  , provides a clinicians perspective. Kristis parents, Alice and John, discuss their experience raising a child born with a variation of sex anatomy.  

==Reception==
 2001 Student Academy Award for Best Documentary,  and the Student Award for Best Documentary at the 6th Annual Palm Springs International Festival of Short Films.  The film was recommended viewing for the PlanetOut.com Second Queer Short Film Festival,  and went on to screen at more than a dozen national and international film festivals.

Winston Wilde, Professor of Human Sexuality and Behavioral Sciences at Santa Monica College called the movie, "the finest film on the issues of intersex Americans, and an indispensable tool for instructors of Human Sexuality, Gender Identity, and Social Psychology."  Filmmaker Magazine called XXXY "essential filmmaking... The film’s stripped down quality — talking heads, the occasional shot of a childhood home, or Kristi on a bike — means there’s nothing to interfere with the pair’s stories; the impact is profound." 

Demonstrating continued relevance, XXXY was featured in a 2013 article,  , in  , December 30, 2013. 

==Awards==

* Gold Medal, Best Documentary - 2001 Student Academy Awards  
* Finalist - PlanetOut.com Gay & Lesbian Online Movie Awards 
* Jury Award Winner - New York Exposition of Short Film and Video  
* Best Student Documentary Award - Palm Springs Shorts Film Festival 
* Honorable Mention  - Directors Guild of America  
* Honorable Mention - Making Waves Film Festival
* Honoree - Louisville Film and Video Festival
* Honoree - San Francisco Gay and Lesbian Intl. Film Festival
* Honoree - Western Psychological Association
* Screened at more than a dozen national and international film festivals

==References==

 

==External links==
*  
*  
*  

==See also==
* Intersex
* Human rights
* Self-determination
* Bodily integrity
* Hermaphrodite

 
 
 
 
 
 