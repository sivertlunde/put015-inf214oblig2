Shadow Hours
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Shadow Hours
| image          = ShadowHoursDVDCoverAmazon.jpg
| alt            = A man is suspended facing up by ropes tied to his wrists and torso. Up from him is a giant clock display.
| caption        = US DVD cover.
| director       = Isaac H. Eaton
| producer       = Isaac H. Eaton
| writer         = Isaac H. Eaton
| starring       = Balthazar Getty   Peter Weller   Rebecca Gayheart Brian Tyler
| cinematography = Frank Byers
| editing        = 
| studio         =  5150 Productions   Newmark Films Inc.   Seven Arts Productions
| distributor    = Newmark Films Inc.  (USA) 
| released       =  
| runtime        = 95 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $38,181 
}}
 thriller directed, written and produced by Isaac H. Eaton. It premiered in competition during the 2000 Sundance Film Festival. 

==Plot==
Michael Holloway (Balthazar Getty) is a recovering addict working as a gas station attendant to support his pregnant wife, Chloe (Rebecca Gayheart). He is then drawn into the seedy underworld of Los Angeles by Stuart (Peter Weller), a mysterious and wealthy stranger.

==Cast==
* Balthazar Getty as Michael Holloway 
* Peter Weller as Stuart Chappell 
* Rebecca Gayheart as Chloe Holloway 
* Peter Greene as Det. Steve Andrianson 
* Frederic Forrest as Sean 
* Brad Dourif as Roland Montague 
* Michael Dorn as Det. Thomas Greenwood

==Reception==
Shadow Hours received mixed to negative reviews. The film holds a 14% approval rating on the review aggregator website, Rotten Tomatoes, with an average rating of 2.7/10 based on an aggregation of 14 reviews.    Metacritic, which uses a weighted mean, assigned a score of 26 out of 100, based on reviews from 13 film critics.    Lawrence Van Gelder of The New York Times wrote that "Rarely has debauchery been such a bore",    whereas Maitland McDonagh of TV Guide had a less harsh opinion, calling the movie "a very entertaining, if thoroughly silly, morality tale" and giving it four out of five stars.   

== References ==
 

== External links ==
*  
*  

 
 