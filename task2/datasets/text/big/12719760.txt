A Girl, Three Guys, and a Gun
{{Infobox film
| name           = A Girl, Three Guys, and a Gun
| image          = Girl, 3 Guys and a Gun.JPG
| caption        = Some things in life you just gotta do... This is one of those things.
|
| director       = Brent Florence
| producer = Lloyd A. Silverman Jim Florence
| writer         = Brent Florence
| starring       = Brent Florence Christian Leffler Kenny Luper Tracy Zachoryin Josh Holland
|
| cinematography = Matthew W. Davis
| editing = Brent Florence Sam Bauer Noel Sterrett
| distributor = New Concorde Home Entertainment  (VHS) (DVD)  Buena Vista Home Entertainment  (DVD) 
| released = 2001
| runtime = 87 minutes
| country = United States English
|
}}
A Girl, Three Guys, and a Gun (a.k.a. A Girl, 3 Guys and a Gun) is a 2001 romantic comedy film from writer-director Brent Florence, who also stars in the picture.  It is a low budget independent film with a cast consisting primarily of unknown actors, supported by 1940s film star June Allyson in her final role.

==Plot==
Three smalltown friends looking to leave their troubles behind come up with a robbery scheme to fund their trip to the big city.  But as their plan goes awry and the authorities become involved, they are left with no other alternative but to take hostage a pair of traveling young lovebirds, with no shortage of problems of their own.

==Cast==
{| class="wikitable"
|-
! Actor
! Role
|-
| June Allyson
| Joeys Grandmother
|-
| Robin Clark
| Hellman
|-
| Brent Florence
| Frank
|-
| Natasha Henstridge
| 5 OClock Girl
|-
| Josh Holland
| Dave
|-
| Christian Leffler
| Neil
|-
| John Lexing
| Smith
|-
| Kenny Luper
| Joey
|-
| Lloyd A. Silverman
| Honor Farm Guard
|-
| Tava Smiley
| Allysa
|-
| Michael Trucco
| Trevor
|-
| Martin Veselich
| Rex
|-
| Tracy Zahoryin
| Hope Scott
|} Sarah Little
 | Johanna

==References==
 

==External links==
* 
*  at Rotten Tomatoes
*  at Allmovie

 
 
 
 
 
 
 


 
 