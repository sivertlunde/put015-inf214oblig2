Happily Ever After (1985 film)
{{Infobox film
| name           = Happily Ever After
| image          = Happily Ever After 1985.jpg
| caption        = Theatrical release poster
| director       = Bruno Barreto
| producer       = Lucy Barreto Antônio Calmon
| writer         = Bruno Barreto Antônio Calmon
| starring       = Regina Duarte Paulo Castelli
| music          = Cesar Camargo Mariano
| cinematography = Affonso Beato
| editing        = Vera Freire
| studio         = L.C. Barreto
| distributor    = Embrafilme
| released       =  
| runtime        = 108 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}}

Happily Ever After  ( ) is a 1985 Brazilian romantic drama film directed by Bruno Barreto.

It was released in the United States in November 1986.  

==Plot==
Fernanda is a married woman with two children who meets Miguel, a male prostitute after hitting him in a car accident. They meet again when he steals her wallet and drops an advertising of the club where he works. When her husband travels, Fernanda and Miguel go to Santos, São Paulo|Santos, saying they would accompany until the port a woman who would go to Paris. They go aboard a ship that goes to Salvador, Bahia|Salvador, where they pass the New Years Day. However, when Fernanda eventually discovers Miguel is a cocaine addict she returns to her family.

==Cast==
* Regina Duarte as Fernanda
* Paulo Castelli as Miguel
* Patricio Bisso as Bom-Bom
* Flávio Galvão as Roberto
* Felipe Martins as Ratinho Walter Forster
* Maria Helena Dias
* Flávio São Thiago
* Ivan Setta

==Reception==
At the 3rd Bogota Film Festival, Happily Ever After won the Best Director, Best Screenplay and Best Actor (Castelli). 

==References==
 

==External links==
* 

 

 
 
 
 
 