I'm Never Afraid!
{{Infobox film name           = Im Never Afraid! director       = Willem Baptist producer       = Mete Gümurhhan writer         = Willem Baptist starring       = Mack Bouwense
| cinematography = Dirk-Jan Kerkkamp editing        = Albert Markus released       = November 2010 runtime        = 25 minutes country        = Netherlands sound          = David Spaans, Ranko Paukovic language       = Dutch
}} Dutch Super 16mm  documentary film about Mack Bouwense an eight-year-old professional motorcross racer who has a mirrored heart, a condition known as dextrocardia. It is directed by award winning Dutch filmmaker Willem Baptist and broadcast by VPRO on 20 November 2010. In German and French speaking countries the documentary was broadcast by ARTE. 

It premiered at the International Documentary Film Festival Amsterdam,  and screened at more than 90 film festivals worldwide including BFI London Film Festival, Slamdance Film Festival|Slamdance,  Sprockets; Toronto Film Festival,  and Kraków Film Festival.  It won multiple international awards    including a Golden Gate Award at San Francisco International Film Festival,  Documentary Short Grand Jury Prize at Atlanta Film Festival,  and a Kinderkast Jury award non-fiction at Cinekid Festival.  In 2011, the documentary was nominated for a broadcast award for Best Children Programme at the Netherlands Institute for Sound and Vision. 

==References==
 

==External links==
*  

 
 
 
 
 
 