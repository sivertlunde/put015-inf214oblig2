Dizzy Dishes
 
 
{{Infobox film
| name = Dizzy Dishes
| image =
| image_size =
| caption =
| director = Dave Fleischer
| producer = Max Fleischer
| animator = Grim Natwick Ted Sears
| writer =
| narrator =
| starring = Margie Hines (voice)
| music =
| cinematography =
| editing = Paramount Publix Corporation
| released =  
| runtime = 6 minutes
| country = United States English
| budget =
}}
Dizzy Dishes is an animated cartoon created by Fleischer Studios in 1930, as part of the Talkartoon series. It is famous as the first cartoon in which Betty Boop appears.

==Plot== anthropomorphic flapper Bimbo waits on a hungry gorilla and then goes to the kitchen to prepare the order, roast duck. When he is about to bring it to the gorillas table, he sees Betty Boop performing on stage and falls in love at first sight. He forgets about the hungry gorilla and dances on stage with the duck. The gorilla, furious, goes after Bimbo, who escapes on a wooden train.

==Notes== anthropomorphic female Crazy Town.

==Home video releases==
In the 1990s, this cartoon was released as part of the Betty Boop - The Definitive Collection laserdisc set. On September 24, 2013, Olive Films released this cartoon as part of the Betty Boop: The Essential Collection - Volume Two DVD and Blu-ray sets.

==External links==
* 
*   on YouTube

 
 
 
 
 
 


 