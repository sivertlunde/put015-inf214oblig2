Private Parts (1997 film)
{{Infobox film
| name           = Private Parts
| image          = Howard Sterns Private Parts Film Poster.JPG
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster.
| director       = Betty Thomas
| producer       = Ivan Reitman
| screenplay     = Len Blum Michael Kalesniko
| based on       =   Michael Murphy Jenna Jameson Marilyn Manson The Dust Brothers
| cinematography = Walt Lloyd
| editing        = Peter Teschner
| studio         = Rysher Entertainment Northern Lights Entertainment
| distributor    = Paramount Pictures
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $28 million
| gross          = $41,230,799
}} biographical comedy book of radio show staff also star in the film, including newscaster and co-host Robin Quivers, producers Fred Norris and Gary DellAbate and comedian Jackie Martling.

Development began after Stern, who insisted on script approval, rejected multiple write-ups. Filming started in May 1996 and lasted for four months, with director Betty Thomas. Private Parts was first screened on February 27, 1997 in New York City, followed by a general release in the United States on March 3. It topped the box office in its opening weekend with a gross of $14.6 million. It went on to earn $41.2 million in domestic revenue overall and $44 million more worldwide. In 1998, the film was released on DVD and Stern won a Blockbuster Award for Favorite Male Newcomer.

==Plot== WRNW in Briarcliff Manor, New York. He gets promoted to program director, where his increase in salary allows him to marry Alison. After being forced to fire a fellow DJ, he quits the station.
 WCCC in WWWW in WWDC in Washington, D.C., where he meets Robin Quivers, the news anchor for his program, whom Howard encourages to talk and contribute on air. The two refuse orders from their boss Dee Dee (Allison Janney) for constantly breaking format. One of their antics, in which Howard gives a female caller (Theresa Lynn) an orgasm on the air, almost gets him fired until a boost in ratings forces Dee Dee to keep him. Fred is then hired to contribute. Meanwhile, Alison announces her pregnancy, but it ends in miscarriage. Although they cheer each other up joking about it, Howard makes light of it on the air, which upsets her greatly.
 WNBC in New York City, where he earns more money and has the chance to make his show become a nationwide success. Upper management at NBCs flagship station offered him a 3 year, $450,000 closed-end contract, not realizing what Sterns show was like until they saw a news report on the subject. Kenny "Pig Vomit" Rushton (Paul Giamatti) takes on the job of keeping Howard in line, or forcing him to quit, as the latter is the only way out of their contract. After ignoring Kennys orders of two-minute bits without swearing and sexual references on air, Kenny retaliates by firing Robin. The show fails in her absence after Quiverss replacement is hired, who quits after a few days when an actress swallowing a kielbasa gets put on the air. Robin is eventually brought back. Howards antics continue, with Kenny ultimately cutting a broadcast off for having a young woman named Mandy (Jenna Jameson) strip naked in the studio and give him a massage. Howard gets the show back on the air, and broadcasts he and Kenny getting into a physical altercation with each other in his office. In May 1985, when the new ratings come in and Howard becomes number one, Kenny comes over to Howards home trying to suck up to him, and is turned down flat. He thanks the fans with a concert by AC/DC. During the performance, Alison is rushed to hospital and gives birth to a baby daughter.

Back on the flight, it is revealed he told his whole story to Gloria, and Howard now believes he could get her, but stays loyal to Alison. He gets off the plane revealing Alison and his three daughters running to greet him. During the end credits, Stuttering John (John Melendez) rants about his absence in the film. Mia Farrow then presents a best actor award for Howard at an Oscars ceremony. He appears as Fartman once again, with Howard falling in mid-air, which the audience applaud for. Next is a clip of Kenny, who no longer works for WNBC and is now the manager of a shopping mall. He then blames Howard for his downfall. During his outbursts, his foul language is blocked out by jackhammer noises.

==Cast==
* Howard Stern as Himself
* Robin Quivers as Herself
* Mary McCormack as Alison Stern
* Fred Norris as Himself Kenny "Pig Vomit" Rushton
* Carol Alt as Gloria
* Richard Portnow as Ben Stern
* Kelly Bishop as Ray Stern
* Sasha Martin as Emily Beth Stern
* Sarah Hyland as Debra Jennifer Stern Michael Murphy as Roger Elick
* Reni Santoni as Vin Vallesecca
* Allison Janney as Dee Dee
* Melanie Good as Brittany Fairchild
* Leslie Bibb as the WNBC Page/Tour Guide Camille Donatacci Grammer as Camille the Card Girl
* Edie Falco as Alisons Friend
* Jenna Jameson as Mandy
* Amber Smith as Julie
* Janine Lindemulder as Camp Directors Wife
* John Melendez as Stuttering John
* Michael C. Gwynne as Duke of Rock (as Michael Gwynne)
* Paul Hecht as Ross Buckingham
* James Murtaugh as Payton
;As themselves
* Gary DellAbate
* Jackie Martling
* David Letterman
* Mia Farrow Crackhead Bob
* Nicole Bass
* AC/DC (Brian Johnson, Angus Young, Malcolm Young, Phil Rudd, Cliff Williams)

;Cameos
* Ozzy Osbourne appears during the opening scene, commenting on Stern saying: "What a f***ing jerk "
* Dee Snider appears during the opening scene Tiny Tim appears during the opening scene
* John Stamos appears during the opening scene, filling in for Luke Perry, who introduced Stern at the real MTV Video Music Awards
* Flavor Flav appears during the opening scene
* John Popper appears during the opening scene Slash appears during the opening scene
* Ted Nugent appears during the opening scene
* MC Hammer appears during the opening scene

;Other characters WNBC during the "Lance Eluction" segment
* Nancy Sirianni, (Jackie Martilings then real-life wife) appears in the college film festival scene, seated in front of Stern.
* Allison Furman-Norris (Fred Norris real life wife) appears as a receptionist at WRNW.

==Production==
===Development===
In the early 1990s, Stern originally was set to make a movie based on his satirical alter-ego Fartman (Howard Stern)|Fartman. Stern first revealed his intentions to make a Fartman movie in 1992. On November 25, 1992, Variety (magazine)|Variety reported that J. F. Lawton, writer of Pretty Woman and Under Siege, was planning to write and direct New Line Cinemas Stern film project, titled The Adventures of Fartman. The film, which would be budgeted at $8–11 million, was expected to go into production the following May in New York. David Permut would produce the film under his Permut Presentations Banner, which has a first-look deal at New Line. According to Lawton, The Adventures of Fartman would revolve around the superhero and his alter ego, a magazine publisher in the mold of Screw magazine|Screw magazines Al Goldstein.  Lawton told Time, "Theres a lot of nudity, some harsh language, a lesbian love scene, and the main character works for an underground sex magazine. We told New Line Cinema the plot, and they said, Yeah, it sounds great. But cant we make it PG-13? " 
 The Late Late Night. suspend disbelief. Louis-Dreyfus later backed out because she wanted to spend time with her family.

===Filming=== Union High School (from which comedian Artie Lange, who would join the radio show in late 2001, graduated in 1985). The concert scene featuring AC/DC was filmed at Bryant Park in New York City in July 1996.

===Soundtrack===
 

==Release==

===Theatrical run===
The film premiered at the top of the box-office in its opening weekend with a gross of $14.6 million. It went on to gross $41,230,799 at the end of its domestic run.

===Cable television=== trailer and publicity materials before being cut. The picture quality of the Internet version is very poor with compression artifacts, VHS artifacts, and visible dust on the print.
 pixelized and the profanity bleeped. In 2007, VH-1 began airing this version.

The film premiered in 1080 High Definition on Universal HD on March 11, 2008. It is the uncut feature-film version with minor commercial interruptions.

===Home media===
When the film was released on video, some store customers objected to the original cover featuring Stern with no clothes on. An alternative version of the cover was produced featuring Stern fully clothed.

==Reception==

=== Critical response=== Siskel and Ebert, Joel Silver and Gene Shalit. Stern in particular received  high praise for his acting, as did Robin Quivers and Fred Norris. Paul Giamatti was also praised, notably propelling him to stardom. Some critics claimed that the film glossed over his use of sexual and racial humor and that it was relatively brief on recent events of Howards career. Rotten Tomatoes retrospectively collected 49 reviews and gave it a "Certified Fresh" score of 80%, with the sites consensus "A surprisingly endearing biopic about the controversial shock-jock Howard Stern that is equally funny and raunchy."   

===Accolades=== Golden Satellite Award for "Best Performance by an Actor in a Motion Picture - Comedy". He was also nominated for a Razzie Award for "Worst New Star".

American Film Institute recognition:
* AFIs 100 Years... 100 Laughs - Nominated 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 