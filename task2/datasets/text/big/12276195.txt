Starrbooty
{{Infobox Film |
  name           = Starrbooty |
  image          = Starrbooty.jpg |
  writer         = R.A. Charles (RuPaul) | Michael Lucas Ari Gold |
  director       = Mike Ruiz |
  producer       = R.A. Charles |
  distributor    = Starrbooty, Inc.|
  released   = 2007 |
  runtime        = Approximately 90 minutes |
  language = English |
  music          = Songs by RuPaul| 
  budget         = $500,000
}}

Starrbooty is a low-budget film written by and starring drag queen RuPaul. The movie is the fourth in a series of films starring the title character, a top secret government agent and supermodel. The original three films were all made in the mid-1980s by RuPaul and his friends with a budget of around $100, and were sold out of shopping carts around gay bars in Atlanta, Georgia (U.S. state)|Georgia.

==Plot==
The films plot combines several elements from these original films. Starrbooty (RuPaul) is in the middle of a major karate fight when she receives a phone call informing her that her adopted niece Cornisha has been kidnapped. With the help of fellow crime fighter Agent Page Turner, Starrbooty learns that her nemesis Annaka Manners is using her billion-dollar cosmetics company as a front for kidnapping prostitutes and selling their organs on the black market. Starrbooty also discovers that Annaka is actually her long-lost sister, making Cornisha Annakas daughter.

Page and Starrbooty go undercover as prostitutes (as Pepper and Cupcake, respectively) and in order to be convincing enough to infiltrate Annakas inner sanctum, they go "all the way" (this includes numerous sex scenes with explicit male nudity, generally designed for laughs). When Starrbooty finally confronts Annaka she discovers that Cornisha was in on the plot all along, and has become Annakas lover. Informing them that they are actually mother and daughter, Starrbooty reveals Annakas plans to double-cross Cornisha by selling her clitoris to a wealthy socialite (in one of the more memorable jokes, Annaka is also planning to sell Starrbootys long legs to a "famous female rapper," quipping, "I guess they wont call her Lil anymore!").

A final showdown between Starrbooty and Annaka leaves Annaka dead, and they are able to salvage Cornishas genitals by taking Annakas (a perfect biological match). Thus everyone who deserves to lives happily ever after.

==Cast==

* RuPaul as Starrbooty/Cupcake
* Lahoma Van Zandt as Page Turner/Pepper
* Candis Cayne as Annaka Manners Gus Mattox as Max
* Jazmine Jimenez as Cornisha
* Dee Finley as Diesel Fortensky
* Corey Corey as Junebug Michael Lucas as Alexi Popov
* Owen Hawk as Dirty Talker
* Richard Lynch as Ol Smeller
* Lady Bunny as Ms. Tasha
* Laura Forbes as Mrs. McGillicutty Ari Gold as Tyrone Cohen
* Tim Wallis as TMisha
* Kayla Aguirre as Kim She
* Sweetie as Ol Lestra
* Kayvon Zand as Suicide bomber
* Andre Robert Lee as Barley
* Joe Ovbey as Kidnap caller
* Ray Stewart as Doctor
* Joelle Pezely as Nurse
* Wilmer Fernandez as Manner man
* Dale Kan as Receptionist

==Production==
During production the films working title was Starrbooty: Reloaded, but when screened at film festivals it was simply called Starrbooty. The DVD title is Starrbooty: The Movie.
 John Waters and part The Naked Gun."  The film utilizes things like obvious vocal overdubbing, grainy footage and dramatic cuts to mimic these low-budget films of the past.

==Screenings and reception==

The film began screening at various gay and lesbian film festivals in July 2007. RuPaul presented the film in person at each screening, often with other cast members. Erik Schut of The Philly Film Guide said "Starrbooty is a fried, dyed and laid to the side-splitting kick-ass good time!" It was the only film at the Philadelphia Gay & Lesbian Film Festival to be sold out at both screenings.

The movies supporting cast is primarily made up of pornographic film stars, both male and female. It is unrated, and though there is no explicit sex, it does feature several of its male stars frontally nude and erect, and at one point RuPaul is depicted graphically fondling a mans penis. Though this is primarily played for comic effect, it is a marked departure from the "Drag Queen Next Door" image RuPaul cultivated during the height of her fame.

The film ends with an on-screen dedication to Jerry Falwell.

The DVD was released October 31, 2007. There was no distribution deal reached, and thus it was released by RuPaul under the imprint "Starbooty, Inc.".

==See also==
*  

==Notes==
 

==References==
* 

==External links==
*  
*   on the Internet Movie Database

 
 
 