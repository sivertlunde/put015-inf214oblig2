Aval Niraparathi
{{Infobox film 
| name           = Aval Niraparathi
| image          =
| caption        =
| director       = M Masthan
| producer       = Areefa Hassan
| writer         = Askar Kakkanad Mani (dialogues)
| screenplay     = Kakkanad Mani Jose Sathaar Roopa Chauda Pattom Sadan
| music          = A. T. Ummer
| cinematography = J Williams
| editing        = K Sankunni
| studio         = Arifa Enterprises
| distributor    = Arifa Enterprises
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by M Masthan and produced by Areefa Hassan. The film stars Jose (actor)|Jose, Sathaar, Roopa Chauda and Pattom Sadan in lead roles. The film had musical score by A. T. Ummer.   

==Cast== Jose
*Sathaar
*Roopa Chauda
*Pattom Sadan
*Jaya
*PR Varalekshmi
*Philomina Sadhana
*Sudheer Sudheer

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Janmanaalil ninakku choodan || Cochin Ibrahim || Yusufali Kechery || 
|-
| 2 || Kannil neelapushpam || S Janaki || Yusufali Kechery || 
|-
| 3 || Nalinanayana naarayana || P Susheela || Yusufali Kechery || 
|-
| 4 || Poovaalanmare || Ambili, CO Anto || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 