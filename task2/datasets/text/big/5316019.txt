That'll Be the Day (film)
{{Infobox film
| name           = Thatll Be the Day
| image          = Thatll be the day.jpg
| caption         = DVD cover by Arnaldo Putzu
| director       = Claude Whatham
| producer       = Sanford Lieberson David Puttnam
| writer         = Ray Connolly
| starring       = David Essex Rosemary Leach Ringo Starr Keith Moon Billy Fury Deborah Watling
| distributor    = Anglo-EMI Film Distribution
| released   = 12 April 1973 (UK) 29 October 1973 (U.S.)
| runtime        = 91 minutes
| language = English
| budget         = £288,000 Alexander Walker, National Heroes: British Cinema in the Seventies and Eighties, Harrap, 1985 p 79 
| music          = 
| awards         = 
}}
 British drama film starring David Essex, Rosemary Leach and Ringo Starr, written by Ray Connolly and directed by Claude Whatham. It is set in the late 50s/early 60s and was partially filmed on the Isle of Wight.

==Plot summary==

The mother of Jim MacLaine (David Essex) was abandoned by his father when he was young. Later, as a suburban school dropout, Jim leaves home and drifts through a succession of dead-end jobs until he finds an outlet for his frustration in rock n roll. Tossing away the chance of a university education much to the consternation of his mother, alienated MacLaine becomes a lowly deckchair attendant before streetwise friend Mike (Ringo Starr) gets him a job firstly as a barman and then with the fun fair. The initially shy MacLaine quickly becomes a heartless fairground Romeo leaving a trail of broken hearts in his wake. Eventually MacLaine returns home to run the family store and marry his girlfriend, but despite the birth of a son, restless Jim feels the lure of rock ’n’ roll again.

==Characters== Rory Storm & The Hurricanes were said to be the inspiration for the fictional group called "Stray Cats" in the film.

Many of the characters were played by musicians who had lived through the era portrayed in the film including Ringo Starr of The Hurricanes and The Beatles, Billy Fury, Keith Moon of The Who, and John Hawken of The Nashville Teens.

The film was produced by David Puttnam and is loosely based on the Harry Nilsson song "1941".

==Cast==
*David Essex . . . Jim Maclaine
*Ringo Starr . . . Mike
*Rosemary Leach . . . Mary Maclaine
*James Booth . . . Mr. Maclaine
*Billy Fury . . . Stormy Tempest (character based on Rory Storm)
*Rosalind Ayres . . . Jeanette Sutcliffe
*Keith Moon . . . J.D. Clover Robert Lindsay . . . Terry Sutcliffe
*Deborah Watling . . . Sandra
*Brenda Bruce . . . Doreen
*Beth Morris . . . Jean
*Kim Braden . . .Charlotte
*Johnny Shannon . . . Jack
*Karl Howman . . . Johnny
*Sue Holderness . . . Shirley
*Sacha Puttnam . . . Young Jim Maclaine

==Reception and reputation==
===Box Office===
The film was a hit at the box office (by 1985 it had earned an estimated profit of £406,000),  leading to a sequel, Stardust (1974 film)|Stardust, (1974).

Nat Cohen, who invested in the movie, said the film made more than 50% its cost. Ooh, you are awful, film men tell Tories.
David Blundy. The Sunday Times (London, England), Sunday, December 16, 1973; pg. 5; Issue 7853.  (939 words) 
===Critical Reception===
Critic Anne Billson has called it a "hugely overrated dip into the rock n roll nostalgia bucket, ... " also commenting "Youth culture my eye: theyre all at least a decade too old. But good tunes, and worth catching for Billy Furys gold lamé act." 

== Soundtrack ==

*Buddy Holly - "Thatll Be The Day"
*Billy Fury - "A Thousand Stars"
*Billy Fury - "Long Live Rock"
*Billy Fury - "Get Yourself Together"
*Billy Fury - "Thats Alright Mama"
*Billy Fury - "What Did I Say" Wishful Thinking - "Itll Be Me"
*Dion and the Belmonts - "Runaround Sue"
*The Everly Brothers - "Bye Bye Love"
*The Everly Brothers - "Devoted To You"
*The Everly Brothers - "Till I Kissed You"
*The Everly Brothers - "Wake Up Little Suzy"
*The Platters - "Smoke Gets In Your Eyes"
*Big Bopper - "Chantilly Lace"
*Jerry Lee Lewis - "Great Balls of Fire"
*Little Richard - "Tutti Frutti" Danny and the Juniors - "At The Hop"
*Frankie Lymon - "Why Do Fools Fall In Love"
*Johnny Tillotson -"Poetry In Motion" Jimmie Rodgers - "Honeycomb"
*Larry Williams - "Bony Moronie" 
*Del Shannon - "Runaway"
*Richie Valens - "Donna"
*Eugene Wallace - "Slow Down"
*Jerry Lee Lewis - "Great Balls of Fire"
*Brian Hyland - "Sealed With a Kiss"
*Bobby Vee - "Take Good Care Of My Baby"
*Del Shannon - "Hats Off To Larry"
*Bobby Darin - "Dream Lover"
*The Paris Sisters - "I love How You Love Me" Born Too Late"
*Johnny and the Hurricanes - "Red River Rock"
*The Monotones - "The Book of Love"
*Bill Justis - "Raunchy"
*Johnny Preston - "Running Bear"
*The Diamonds - "Little Darlin "
*Ray Sharp - "Linda Lou"
*Lloyd Price - "(Youve Got) Personality"
*Buddy Holly and the Crickets - "Well All Right"
*Dante And The Evergreen - "Ally Oop"
*Viv Stanshall - "Real Leather Jacket"
*Stormy Tempest ( Viv Stanshall ) - "What in the World"

*Wolverine Cubs Jazz Band - "Weary Blues" (featured in the film but not on Soundtrack recording)


===Chart positions===
{| class="wikitable"
!Chart
!Year
!Peak position
|- UK Albums Chart  1973
|align="center"|1
|}

 
 
 
 
 

==Award Nominations==

BAFTA Best Supporting Actress: Rosemary Leach.

BAFTA Most Promising Newcomer to Leading Film Roles: David Essex.

==Spin-off==
An independent Radio Drama recording project was completed in 2008 entitled   which continues the story of Jimmy Maclaine jr. (son of Jim Maclaine). (Also see external link below).

==References==
 

== External links ==
*  
* 

 
 
 
 
 
 
 
 
 
 
 