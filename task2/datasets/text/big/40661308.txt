The Arizona Kid (1930 film)
{{Infobox film
  | name           = The Arizona Kid
  | image          = The Arizona Kid still.JPG
  | image_size     =
  | caption        = Carole Lombard and Warner Baxter in the film
  | director       = Alfred Santell
  | producer       = Winfield Sheehan
  | writer         = Ralph Block Joseph Wright
  | starring       = Warner Baxter Mona Maris Carole Lombard Theodore von Eltz 
  | music          = 
  | cinematography = 
  | editing        = 
  | distributor    = Fox Film Corporation
  | released       =  
  | country        = United States
  | runtime        = 88 minutes
  | language       = English
  | budget         = 
}}
 Western film, produced by Fox Film Corporation and directed by Alfred Santell. 

The first of three sequels to the Academy Award-winning film In Old Arizona (1929), it stars Warner Baxter in the title role (a character based on the Cisco Kid).    The film features Carole Lombard in one of her early roles.

The film was a hit at the box office. 

==References==
 

==External links==
* 

 
 
 
 
 
 


 