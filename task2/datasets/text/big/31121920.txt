Old Faithful (film)
Old British drama film directed by Maclean Rogers and starring Horace Hodges, Glennis Lorimer and Bruce Lester.  An elderly taxi driver refuses to give up his old horse even though his business is being taken by younger drivers using modern cars. His anguish is increased when his daughter plans to marry one of the younger taxi drivers.

==Cast==
* Horace Hodges - Bill Brunning 
* Glennis Lorimer - Lucy Brunning 
* Bruce Lester - Alf Haines 
* Wally Patch - Joe Riley 
* Isobel Scaife - Lily 
* Muriel George - Martha Brown 
* Edward Cooper - Edwards 
* William Hartnell - Minor role

==References==
 

==External links==
* 

 
 
 
 
 
 


 