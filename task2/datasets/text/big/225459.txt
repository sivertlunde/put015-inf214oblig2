Lady Sings the Blues (film)
 
{{Infobox film
| name = Lady Sings the Blues
| image = Lady sings the blues.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Sidney J. Furie
| producer = Brad Dexter Jay Weston James S. White Chris Clark Terence McCloy
| based on =  
| starring = Diana Ross Billy Dee Williams Richard Pryor
| music = Gil Askey Michel Legrand
| cinematography = John A. Alonzo
| editing = Argyle Nelson Motown Productions
| distributor = Paramount Pictures
| released =  
| runtime = 144 minutes  
| country = United States
| language = English
| budget = $14 million
| gross = $19,726,490 
}} biographical drama 1956 autobiography Motown Productions for Paramount Pictures. Diana Ross portrayed Holiday, alongside a cast including Billy Dee Williams, Richard Pryor, James T. Callahan, and Scatman Crothers. 

==Plot==
 
In 1936, New York City, Billie Holiday is arrested on a drugs charge. 
 flashback to Harlem section of New York. The brothel is run by an arrogant, selfish owner who pays Billie very little money. 
 All of Me", Jerry, the club owner, books her as a singer in the show.

Billies debut begins unsuccessfully until Louis McKay (Billy Dee Williams), arrives and gives her a fifty dollar tip. Billie takes the money and sings "Them There Eyes". Billie takes a liking to Louis and begins a relationship with him. Eventually she is discovered by two men: Harry and Reg Hanley, who sign her as a soloist for their southern tour in hopes of landing a radio network gig. During the tour, Billie witnesses the aftermath of the lynching of an African-American man, which presses her to record the controversial song "Strange Fruit". The harsh experiences on the tour result in Billie taking drugs which Harry supplies. One night when Billie is performing, Louis comes to see Billie. He knows that she is doing drugs and tells her she is going home with him. Billie promises to stay off the drugs if Louis stays with her. 

In New York, Reg and Louis arrange Billies radio debut, but the station does not call her to sing; the radio sponsors, a soap company, object to her race. The group heads to Cafe Manhattan to drown their sorrows. Billie has too much to drink and asks Harry for drugs, saying that she does not want her family to know that the radio show upset her. He refuses and she throws her drink in his face. She is ready to leave, but Louis has arranged for her to sing at the Cafe, a club where she once aspired to sing. She obliges with one song but refuses an encore, leaving the club in urgent need of a fix. Louis, suspicious that Billie has broken her promise, takes her back to his home but refuses to allow her access to the bathroom or her kit. She fights Louis for it, pulling a razor on him. Louis leaves her to shoot up, telling her he does not want her there when he returns.

Billie returns to the Harlem nightclub, where her drug use intensifies until she hears of the death of her mother. Billie checks herself into a drug clinic, but because she cannot afford her treatment the hospital secretly calls Louis, who comes to see her and agrees to pay her bills without her knowledge. Impressed with the initiative she has taken to straighten herself out, Louis proposes to her at the hospital. Just as things are looking up, Billie is arrested for possession of narcotics and removed from the clinic.

In prison, Billie goes through crippling withdrawal. Louis brings the doctor from the hospital to treat her, but she is incoherent. He puts a ring on her finger to remind her of his promise to marry her. When she finishes her prison sentence, Billie returns home and tells her friends that she does not want to sing anymore. Billie marries Louis and pledges not to continue her career, but the lure of performing is too strong and she returns to singing with Louis as her manager. Unfortunately, her felony conviction has stripped her of her Cabaret Card, which would allow her to sing in NYC nightclubs. To restore public confidence and regain her license, Billie agrees to a cross-country tour. Billies career takes off on the nightclub circuit.

Louis leaves for New York to arrange a comeback performance for Billie at Carnegie Hall. Despondent at Louis absence and the never-ending stream of venues, Billie asks Piano Man to pawn the ring Louis gave her in exchange for drugs. While they are high that evening, Piano Mans drug connections arrive; he neither pawned the ring nor paid for the drugs. Piano Man is killed by the dealers. Within the hour, Louis and her promoter call Billie with news that they got Carnegie Hall. Louis returns to find a very fragile Billie who is traumatized and has fallen back into drugs. Louis takes her back to New York.

Billie plays to a packed house at Carnegie Hall. Her encore, "God Bless the Child", is overlaid with newspaper clippings highlighting subsequent events: the concert fails to sway the Commission to restore her license; subsequent appeals are denied; she is later re-arrested on drug charges and finally dies when she is 44. Nevertheless, the Carnegie triumph is frozen in time.

==Cast==
* Diana Ross as Billie Holiday
* Billy Dee Williams as Louis McKay
* Richard Pryor as Piano Man James Callahan as Reg Hanley
* Paul Hampton as Harry
* Sid Melton as Jerry
* Virginia Capers as Mama Holiday
* Yvonne Fair as Yvonne
* Isabel Sanford as Madame
* Jester Hairston as Fefe Lynn Hamilton as Aunt Ida
* Victor Morosco as Vic
* Robert Gordy as The Hawk
* Harry Caesar as The Rapist
* Paulene Myers as Mrs. Edson
* Scatman Crothers as Big Ben

==Box office==
The film earned an estimated $9,050,000 in North American rentals in 1973. 

==Awards and honors== Best Actress Best Art Carl Anderson Reg Allen), Best Costume Best Music, Best Writing, Story and Screenplay Based on Factual Material or Material Not Previously Published or Produced.  The film was also screened at the 1973 Cannes Film Festival, but was not entered into the main competition.   

==Soundtrack== Lady Sings number one on the Billboard Hot 200 Album Charts,  for the week-ending dates of April 7 and 14, 1973.

==References==
 

==External links==
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 