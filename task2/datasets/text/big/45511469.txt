Miracle in Harlem
{{Infobox film
| name           = Miracle in Harlem
| image          =
| caption        =
| director       = Jack Kemp
| producer       = Jack Goldberg & David Goldberg
| writer         = Vincent Valentini (Story and screenplay)
| starring       =
| music          = Jack Shaindlin Juanita Hall
| cinematography = Don Malkames
| art director   = Frank Namczy
| editing        = Don Drucker
| wardrobe       = Ann Blazier
| make Up        = Dr.Rudolph G. Liszt
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
}}

Miracle in Harlem is a 1948 American Film directed by Jack Kemp

==Plot ==
In Harlem an old Lady "Aunt Hattie" has her own candy manufacture shop. Her niece Julie Weston helps her, as the Aunt is quite old. The young woman has ideas about the candy shop, to modernize it, because they are "candy making family".
Aunt Hattie calls her niece and Bert to tell them she had a dream with a premonition of her death. She feels ready and therefor she has ordered a nice coffin that will be brought to her house. 
The rich Albert Marshall with his candy imperium wants to buy the candy shop of Aunt Hattie to erase her as a competitor. Julie doesnt accept the offer, that upsets the magnate. His son Jim quarreling with his father gives him the idea: financing the modernization of her candy shop and then squeeze her. But when Marshall senior makes the contract, his son is disappointed, as it was his idea. Anyway his bossy father with the support of his right hand Alice Adams (who will inherit a lot from his will) put him as a Manager in Hatties modernized Candy Shop, where he doesnt miss to flirt with Julie.
Philipp Manley out of Prison sticks to Jim Marshall, who doesnt really want to be mixed up with Manley as the Police is still after a Murderer and he could be the one. Alice Adams instead wants to talk with Jim Marshall about their relationship, as she cant live any more without him. For Jim what they had was only friendship and its past. When he tells her shes too old to be married, she leaves offended.
In the Candy Shop of Hattie and Julie Jim Marshall gets always more intrusive. He tells Julie that his father owns the shop and he could take over whenever he wants. Another quarrel, where others have to intervene.
Mr. Wilkinson announces Julie that they have to leave as Marshall is taking over the Candy Shop. Julie has to tell her aunt that they have been swindled. In her anger she told Mr. Wilkinson that Marshall will be sorry for that. Mr. Wilkinson is another who is angry with Mr. Marshall as he didnt pay him the promised bonus for the deal of Hatties Shop. He also expresses his anger by threats of what could happen to him.
Marshall comes back from his attorney announcing Alice that he has left in his will everything to her. His son has to become only one dollar. Marshall tastes the newest candies that Mr. Wilkinson brought, and dies.
Meantime Aunt Hattie has received her coffin and her community comes to her house to pray and sing and have an exercise service for when she will die, that she attends to see how they will do it. The Police appears to arrest Julie as Mr. Marshall has been probably poisoned. 
Jim Marshall appears one night in the Candy Shop of Julie searching the poison bottle. Julie wakes up and a fight begins, because Jim grabs her and she tries to escape. A men enters the scene right when Julie has a knife in her hand, soon after Jim lies dead on the floor and nobody knows if Julie used the knife or the man who passed the room. 
The suspense of the murders and the poison comes to an end, which were not going to reveal now.

==Cast==
*Sheila Guyse as Julie Weston
*Hilda Offley as Aunt Hattie
*William Greaves as Bert Hallam
*Creighton Thompson as Reverend Jackson
*Lawrence Criner as Albert Marshall (father)
*Sybil Lewis as Alice Adams 
*Kenneth Freeman as Jim Marshall (son) Jack Carter as Phillip Manley
*Milton Williams as Mr. Wilkinson
*Monte Hawley as Lieutenant Renard
*Alfred "Slick" Chester as Detective Tracy (as Alfred Chester)
*Ruble Blakey as Detective Foley
Specialties Savannah Churchill as Singer, "I Want be Loved"
*Lavada Carter as	Singer,  John Saw the Number
*Norma Shepherd	 as Singer,  Patience & Fortitude
*Juanita Hall	as Juanita Hall, as Singer Chocolate Candy Blues
*Lynn Proctor Trio as Lynn Proctor Trio Singers  Watch Out
*Juanita Hall Choir as Juanita Hall Choir	 
*Hilda Geeley as Singer (uncredited)
*Stepin Fetchit as Swifty the Handyman
 
 
 http://www.allmovie.com/movie/miracle-in-harlem-v32812
 

==References==
 

==External links==
* http://www.imdb.com/title/tt0040593/?ref_=ttfc_ql
* http://www.allmovie.com/movie/miracle-in-harlem-v32812
* http://www.afi.com/members/catalog/DetailView.aspx?s=&Movie=25642

 
 
 
 
 
 
 
 
 