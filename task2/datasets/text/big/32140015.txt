Boat Trip 3D
 
{{Infobox film
|  name           = Boat Trip 3D
|  image          = Boat Trip 3D.png
|  image_size     =
|  caption        = Production shot of Boat Trip 3D showing the boat containing the 3D camera rig on the right
|  director       = Mathijs Geijskes
|  producer       = Ivo Broekhuizen Mathijs Geijskes
|  writer         =
|  starring       = Paul Geusebroek 
|  music          = 
|  cinematography = Mick van Rossum
|  editing        = 
|  distributor    = A Million Dreams
|  released       = September 2008
|  runtime        = 10 minutes
|  language       = Dutch
|  budget         = 
|  gross          = 
|  awards         =
}}  
Boat Trip 3D also known as Duel on water 3D is the first   in Utrecht in September 2008.

==Plot summary==
A boy is enjoying a pleasure cruise in his rubber boat when a large speed boat with an unknown driver disrupts his leisure. The boy decides to enter into a race with the speed boat.

==Production crew==
*Director: Mathijs Geijskes
*Producer: Ivo Broekhuizen, Mathijs Geijskes
*Cinematography: Mick van Rossum

==Production details==
The film was completely shot on the water using a specially prepared boat, which was equipped with two cameras, using rare state of the art digital 3D equipment on loan from French 3D expert Alain Derobe.   ( )  The film was then enhanced with Computer-generated imagery|CGI. 

==References==
 

==External links==
*     on the site of the Netherlands Film Festival ( )

 
 
 