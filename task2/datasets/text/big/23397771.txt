Cutting It Short
{{Infobox Film
| name           = Cutting It Short
| image          = 
| image_size     = 
| caption        = 
| director       = Jiří Menzel
| producer       = 
| writer         = Jiří Menzel Bohumil Hrabal
| narrator       = 
| starring       = 
| music          = 
| cinematography = Jaromír Šofr
| editing        = Jiří Brožek
| distributor    = 
| released       = Czechoslovakia: 1 February 1981 United States: October 1983
| runtime        = 93 minutes
| country        = Czechoslovakia
| language       = Czech koruna
| preceded_by    = 
| followed_by    = 
}}
 1980 cinema Czechoslovak comedy film directed by Jiří Menzel. It is based on the novel Postřižiny by Czech writer Bohumil Hrabal. The story is set in a brewery in a Czech small town. 

The film is an evocation of the childhood memories of Bohumil Hrabal in his provincial town of Nymburk, dominated by the local brewery. The main actors of the film, uncle Pepin and Maryška, are based on real family members of Hrabal: Maryška on his mother and uncle Pepin on his real uncle, who came to stay two weeks in the town but remained for forty years. His spontaneous stories influenced a lot Hrabals literary work.   
 Tati comedies. 
 Theodor Pištěk designed the costumes for the film.

==Cast==
* Magda Vášáryová as Maryška
* Jiří Schmitzer as Francin
* Jaromír Hanzlík as Pepin Rudolf Hrušínský (II) as Dr. Gruntorád
* Petr Čepek as Pán de Giogi
* Oldřich Vlach as Ruzicka
* František Řehák as Vejvoda
* Miloslav Štibich as Bernádek
* Alois Liškutín as Sefl
* Pavel Vondruška as Lustig Rudolf Hrušínský (III) as stable boy
* Miroslav Donutil as scrub
* Oldřich Vízner as Doda Cervinka

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 