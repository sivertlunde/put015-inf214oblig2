Monamour
{{Infobox film
| name           = Monamour
| image          = Monamour.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = Tinto Brass
| producer       = 
| writer         = Tinto Brass Carla Cipriani Massimiliano Zanin  
| narrator       = 
| starring       = Anna Jimskaia Nela Lucic Max Parodi
| music          = Heron Borelli
| cinematography = Andrea Doria
| editing        = Tinto Brass
| studio         = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
Monamour is a 2006 Italian film directed by Tinto Brass.

==Plot==
Marta is a young housewife, married to Dario, a successful book publisher. Although she still loves her husband, Marta hasnt been able to achieve sexual satisfaction for months due to their dull and predictable love life. While staying in Mantua for the Festivaletteratura, a book fair, Marta follows the advice of her scheming friend Sylvia and pursues an affair with a handsome and mysterious artist named Leon, which leads to surprising results regarding her failing marriage with Dario.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Anna Jimskaia || Marta
|-
| Riccardo Marino || Leon 
|-
| Max Parodi || Dario 
|-
| Nela Lucic  || Sylvia 
|-
| Virginia Barrett || Singer at Restaurant
|-
| Tinto Brass || Man with cigar (uncredited)
|}

==Availability==
The film is available to buy in DVD format on sites such as Amazon.com.  The Blu-ray Disc version is out on 19 April 2011. 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 

 
 