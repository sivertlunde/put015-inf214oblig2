Padhi Bhakti
{{Infobox film
| name = Padhi Bakthi பதி பக்தி
| image = 
| director = A. Bhimsingh
| writer = M. S. Solaimalai
| screenplay = A. Bhimsingh
| story = M. S. Solaimalai Savitri M. N. Rajam T. S. Balaiah Chittor V. Nagaiah J. P. Chandrababu  K. A. Thangavelu
| producer = A. Bhimsingh
| music = Viswanathan-Ramamoorthy
| cinematography = G. Vittal Rao
| editing = A. Bhimsingh
| studio = Buddha Pictures
| distributor = Buddha Pictures
| released = 14 March 1958
| runtime = 181 mins
| country = India Tamil
| budget = 
}}
 Savitri and M. N. Rajam in the lead roles. The film was released in the year 1958. 

==Cast==
*Sivaji Ganesan
*Gemini Ganesan Savitri
*M. N. Rajam
*T. S. Balaiah
*Chittor V. Nagaiah
*K. A. Thangavelu
*J. P. Chandrababu
*M. Saroja
*K. D. Santhanam
*A. Krishnan
*A. Rama Rao
*C. R. Vijayakumari
*C. K. Saraswathi
*K. S. Angamuthu
*K. Malathi  

==Crew==
*Producer: A. Bhim Singh
*Production Company: Buddha Pictures
*Director: A. Bhim Singh
*Music: Viswanathan-Ramamoorthy
*Lyrics: Kalyanasundaranar
*Story: M. S. Solaimalai
*Screenplay: A. Bhim Singh
*Dialogues: M. S. Solaimalai
*Editing: A. Bhim Singh
*Cinematography: G. Vittal Rao
*Art Direction: K. Mohan
*Choreography: K. N. Thandayuthapani, P. S. Gopalakrishnan
*Stunt: None
*Audiography: T. S. Rangasamy
*Dance: None

==Soundtrack== Playback singers are T. M. Soundararajan, A. M. Rajah, V. N. Sundharam, P. Suseela & Jikki.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Pattukkottai Kalyanasundaram || 03:26
|-
| 2 || Veedu Nokki Vandha || T. M. Soundararajan || 03:13
|-
| 3 || Iraipodum Manidharukee || P. Suseela || 03:04
|-
| 4 || Dharmamembar.... Indha Pillai Pechu || T. M. Soundararajan & J. P. Chandrababu || 05:20
|-
| 5 || Chinnachiru Kanmalar || P. Suseela || 03:28
|-
| 6 || Kokkara Kokkarako Sevale || T. M. Soundararajan & Jikki ||  03:09
|-
| 7 || Ennalum Vaazhvile || P. Suseela || 03:57
|-
| 8 || Rock Rock Rock n Roll || J. P. Chandrababu & V. N. Sundharam || 04:47
|-
| 9 || Veedu Nokki Vandha (pathos) || T. M. Soundararajan || 03:13
|-
| 10 || Paappaa Un Appaavai Paarkkaadha Ekkamo || A. M. Rajah || 01:07
|}

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 


 