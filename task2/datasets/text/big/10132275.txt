Love (1927 film)
{{Infobox film
| name = Love
| image = Love (1927 film).jpg
| image_size =
| caption = John Gilbert
| producer = Edmund Goulding
| writer = Leo Tolstoy Lorna Moon Frances Marion Marian Ainslee Ruth Cummings
| narrator = John Gilbert Greta Garbo
| music = Arnold Brostoff (1944)
| cinematography = William H. Daniels
| editing = Hugh Wynn
| distributor = Metro-Goldwyn-Mayer
| released =   }}
| runtime = 82 minutes
| country = United States language  English intertitles
| budget = $487,994.88 
{{cite book
| author1 = Alexander Walker
| author2 = Metro-Goldwyn-Mayer
| title = Garbo: a portrait
| url = http://books.google.com/?id=nmZZAAAAMAAJ
| accessdate = July 27, 2010
| date = October 1980
| publisher = Macmillan
| isbn = 978-0-02-622950-0
| page = 184
}} 
| gross =
}} silent film John Gilbert who had starred in the 1926 Blockbuster (entertainment)|blockbuster, Flesh and the Devil.

Taking full advantage of the star power, a drama was scripted based on Leo Tolstoys timeless novel, Anna Karenina. The result was a failure for the authors purists, but it provided the public with a taste of Gilbert-Garbo eroticism that would never again be matched. The publicity campaign for the film was one of the largest up to that time, and the title was changed from the original, Heat.

Director Dimitri Buchowetzki began work on Love with Garbo and Ricardo Cortez. However, producer Irving Thalberg was unhappy with the early filming, and started over by replacing Buchowetzki with Edmund Goulding, cinematographer Merritt B. Gerstad with William H. Daniels, and Cortez with Gilbert. 

==Plot==
During a blizzard, Russian count Alexis Vronsky, aide-de-camp of the Grand Duke, meets a mysterious woman on the way to St. Petersburg, Russia. When they are forced to stop at an inn for the night, Vronsky attempts a seduction after she lifts her veil, revealing a beautiful face. She rejects him coldly.

Some time later, at a reception at his place for Senator Karenin, Vronsky is presented to the Senators wife, Anna, the woman at the inn that cold night and he tries to ask forgiveness for his transgressions. This she finally grants but then he visits her in her home and kisses her passionately and she scolds him again ordering him to leave.

Anna has a young son, Sergei, with whom she has an almost incestuous relationship which is thawed as a fiery passion develops between Anna and Vronsky. This is noted by the aristocracy of St. Petersburg, to the displeasure of her husband. After a horse race, in which Anna makes a spectacle of herself and all but announces her love for Vronsky, she deserts her husband when he finds the two lovers in an hotel room. They go off to Italy together.

Anna cannot forget her son and suffers because she left him, Vronsky realizes this and even though he is jealous of her, returns to Russia with her. She plans to visit Sergei on his birthday, but Karenin prevents it, having told their son that his mother is dead. She overcomes her fear and goes to the house in any case but is found out by Karenin and ordered out of the house.

To make matters worse, the Grand Duke plans to have Vronsky removed from the army because he is cohabitating with Anna, who seeks to prevent him from losing his social position as she has lost hers. She goes to the Grand Duke and is able to reinstate Alexis, on the condition that she leave him, and St. Petersburg, forever.

===Alternate ending (European version)===
As a result of losing the right to visit her son and now having to leave Vronsky forever in order to save his reputation, she commits suicide and leaps in front of a train. This variant complies with original work of Leo Tolstoy.

===Alternate ending (American version)===
For three years the lovers do not see each other but Vronsky searches frantically for Anna. By chance, he reads in a newspaper an item on Annas son, who is finishing at the Military Academy in St. Petersburg and will return for his visits.

On this occasion, he learns that Karenin has died and that Anna visits her son daily. They met again when Sergei invites Vronsky to his home, and are reunited.

==Cast== John Gilbert as Captain Count Alexei Vronsky
* Greta Garbo as Anna Karenina
* George Fawcett as Grand Duke Michel
* Emily Fitzroy as Grand Duchess
* Brandon Hurst as Senator Alexei Karenin
* Philippe De Lacy as Serezha Karenin, Annas Child	
* Edward Connelly as Priest (uncredited)
* Carrie Daumery as Dowager (uncredited) Margaret Lee as Blonde Flirt (uncredited)
* Dorothy Sebastian as Spectator Extra at Races (uncredited)
* Jacques Tourneur	as Extra (uncredited)

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 