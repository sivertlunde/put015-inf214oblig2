Kandam Becha Kottu
{{Infobox film
| name           = Kandam Becha Kottu
| image          = Kandam_Becha_Kottu.jpg
| image_size     = Film poster
| alt            = 
| caption        = 
| director       = T. R. Sundaram
| producer       = Modern Theatres
| writer         = K. T. Muhammed T. Muhammad Yusuf
| narrator       = 
| starring       = Thikkurissy Sukumaran Nair Aranmula Ponnamma
| music          = Baburaj
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 156 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 National Film Awards.

==Plot==
The story of the film revolves around a kind hearted cobbler Mohammed Kakka (T. S. Muthiah). He keeps his life savings into the pockets of his old coat to realise his dream of going on a pilgrimage to Mecca. Alikoya Haji (Thikkurissi) loves his son Ummer (Prem Nawaz) and brings him up showering all love and affection. Hajis’s sister Kadeeja (Pankajavalli) and her family lives in the neighbourhood. Amina (Aranmula Ponnamma) and her children, Kunju Bibi (Ambika ) and Hassan (Nellikkodu Bhaskaran) lives in the same house. Amina’s husband is a businessman in Singapore. Kadeeja’s husband Avaran (Kedamangalam Sadanandan) is very considerate to his brother’s wife Amina and her family. But Kadeeja keeps ill treating Amina.

Ummer falls in love with Kunju Bibi, his childhood mate. Kadeeja is jealous of this affair. She tries all sorts of tricks to create trouble but fails. Alikoya Haji decides to accept Kunju Bibi as his daughter-in-law, but demands a dowry of Rs. 2,000.

Amina’s husband starts from Singapore with the money. Arrangements for the wedding is made but to the dismay of everyone a telegram arrives informing the death of Amina’s husband during the voyage back home. Amina and her children are pushed out of the house, the marriage is postponed as Haji gives an ultimatum to organise the dowry.

Mohammed Kakka gives shelter to Amina and her family. Kadeeja continues to harass them, while Ummer tries hard to help them organise the money. He even goes to the extent of stealing from his father’s safe. Ummer is caught red handed and is placed under house arrest. Mohammed Kakka makes arrangements for the marriage. He offers the money he had saved and kept in the pockets of the coat. He also appeals to others in the neighbourhood to contribute for the marriage. Ummer finally marries Kunju Bibi providing a happy ending to the film. Mohmmed Kakka’s noble deed glorifies him.   

==Cast==
 
*Thikkurissy Sukumaran Nair  as Alikoya Haji
*Prem Nawas  as Ummer
*T. S. Muthaiah as Mammadikka
*Ambika Sukumaran as Kunju Bivi
*Pankajavalli as Kadeeja
*Aranmula Ponnamma as Amina
*Nellikode Bhaskaran  as Hassan
*Kedamangalam Sadanandan as Avaran
*S. P. Pillai as Minnal Moideen
*Bahadoor as Khader
*Nilambur Ayisha as Bethatha
*Chandni as Beechipathu
*Kottayam Chellappan
*Omana
*Muttathara Soman
 

==Soundtrack== 
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aananda Saamraajyathil || P. Leela || P. Bhaskaran || 
|- 
| 2 || Aatte Potte Irikkatte || P. Leela, MS Baburaj || P. Bhaskaran || 
|- 
| 3 || Allaavin Thiruvullam || PB Sreenivas || P. Bhaskaran || 
|- 
| 4 || Ennittum Vannillallo || P. Leela || P. Bhaskaran || 
|- 
| 5 || Kandam Bechoru Kottaanu || MS Baburaj, Mehboob || P. Bhaskaran || 
|- 
| 6 || Maappila Puthumaappila || P. Leela, Kamukara || P. Bhaskaran || 
|- 
| 7 || Puthan Manavatti || P. Leela, Gomathy Sisters || P. Bhaskaran || 
|- 
| 8 || Sindabad || Mehboob || P. Bhaskaran || 
|- 
| 9 || Thekkunnu vanna kaatte || P. Leela || P. Bhaskaran || 
|}

==Awards== National Film Awards
*     

==Production==
Kandam Becha Kottu was an adaptation of the stage play of the same name. The film was to be directed by K. S. Sethumadhavan in his directorial debut. But when the films producer T. E. Vasudevan decided to shoot the film in colour which would be much more expensive than planned, an experienced director was required. The search ended in film producer T. R. Sundaram who was a friend of Vasudevan and had previously directed a few hit films. Sethumadhavan eventually made his debut with Vasudevans next feature Jnanasundari and went on to become one of the most successful filmmakers in South India. 

==Reception==
The film was a big box-office success and ran for weeks in packed theatres. 

==References==
 

==External links==
*  
*http://www.malayalachalachithram.com/movie.php?i=89
*  
*  
 

 
 
 
 