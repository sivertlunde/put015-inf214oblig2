Appuntamento in Riviera
{{Infobox film
| name = Appuntamento in Riviera
| image =
| caption =
| director = Mario Mattoli
| producer =
| writer = Roberto Gianviti Vittorio Metz
| starring = Tony Renis
| music =
| cinematography = Marco Scarpelli
| editing = Adriana Novelli
| distributor =
| released = 1962
| runtime = 93 minutes
| country = Italy
| language = Italian
| budget =
}}

Appuntamento in Riviera is a 1962 Italian comedy film directed by Mario Mattoli and starring Tony Renis.

It contains the original hit song "Quando, Quando, Quando", performed by one of its writers, Tony Renis.

==Cast==
* Tony Renis - Tony
* Graziella Granata - Laura
* Francesco Mulé - Marengoni
* Piero Mazzarella - Cazzaniga
* Maria Letizia Gazzoni - Patrizia
* Annarosa Garatti
* Giulio Girola Santo Versace - Carlo
* Toni Ucci - Giacomo
* Lilli Lembo
* Mimmo Palmara - De Marchi

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 