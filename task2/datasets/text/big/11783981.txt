The Roof (film)
{{Infobox film
| name           = Il Tetto
| image          = 
| image_size     = 
| caption        = 
| director       = Vittorio De Sica
| producer       = Franco Committeri
| writer         = Cesare Zavattini
| narrator       = 
| starring       = Gabriella Pallotta Giorgio Listuzzi  
| music          = Alessandro Cicognini
| cinematography = Carlo Montuori
| editing        = Eraldo Da Roma
| distributor    = 
| released       = October 6, 1956 (Italy) May 12, 1959 (US)
| runtime        = 91 minutes
| country        = Italy
| language       = Italian
| budget         = 
}} 1956 cinema Italian drama film directed and produced by  Vittorio De Sica.

==Plot==
Natale, an apprentice bricklayer, and Luisa, who has no skill, marry and try to live with Natales parents and other relatives in one apartment, what might happen in the poorest classes in Rome about 1950. After a quarrel Natale and Luisa precipitately leave without a place to live. The remainder of the film is devoted to their finding housing. The solution is building a one room brick dwelling as a squat on unused railway land on the outskirts of Rome. As it was illegal, Natale arranges his workmates to assist him during the night. According to the rules, if a  dwelling has a door and a roof the householder cannot be evicted. At dawn when the police arrive to remove them the dwelling is complete except for part of the roof, but a humane policeman looks the other way. We suppose that Natale and Luisa, now pregnant, live happily ever after.

==Cast==
* Gabriella Pallotta - Luisa (as Gabriella Pallotti)
* Giorgio Listuzzi - Natale, Luisas husband
* Luisa Alessandri - Signora Baj
* Angelo Bigioni
* Maria Di Fiori - Giovanna, Natales sister
* Maria Di Rollo - Gina
* Emilia Maritini - Luisas mother
* Giuseppe Martini - Luisas father
* Gastone Renzelli - Cesare, Giovannas husband
* Maria Sittoro - Natales mother
* Angelo Visentin - Natales father

==Awards==
* 1956 Cannes Film Festival: OCIC Award    
* Nastro dArgento: Best Script.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 

 
 