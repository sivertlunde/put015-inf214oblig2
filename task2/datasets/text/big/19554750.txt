The Watermelon
{{Infobox film
| name           = The Watermelon
| image          = The watermelon dvd cover.jpg
| caption        = 
| director       = Brad Mays
| producer       = Lorenda Starfelt, Ronald M. Williams, Michael Hemmingson
| writer         = Michael Hemmingson
| starring       = Will Beinbrink, Kiersten Morgan, Mike Ivy, Bob Golub, Julia Aks, Steven Shields, Elyse Ashton, Volt Francisco, Willow Hale
| music          = Peter Girard
| cinematography = Lawrence Malloy
| editing        = Brad Mays
| distributor    = Celebrity Video Distribution, Inc.
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Watermelon is an independent feature film  penned by Michael Hemmingson and directed by Brad Mays. It is produced by Lorenda Starfelt at LightSong Films in North Hollywood. The Watermelon is Michael Hemmingsons first produced full-length screenplay, and director Brad Mays fourth feature film. 

==Description==
 
  feature comedy The Watermelon, 2009.]] Art School Confidential.

==Background==
Beach scenes were shot in Malibu, California|Malibu, on the property of Tony Romano, executive producer of Catch Me If You Can and I, Robot.  "Hemmingson is one of the best screenwriters out there," Romano has stated, "only no one in this goddamn town knows it."  Interestingly, an intimate beach scene was fashioned by director Mays into an homage to the famous "bullocks scene" from Ken Russells 1969 film Women In Love, with Kiersten Morgan dancing and singing Im Forever Blowing Bubbles, parodying actress Glenda Jacksons dancing to the same tune sung by co-star Jennie Linden. 
 Beverly Hills, North Hollywood, Encinitas and Ocean Beach in San Diego.

Director Brad Mays gave as much time to rehearsing the actors as the production budget would bear, and encouraged the actors towards inventiveness in exploring their roles. The actors were also directed to memorize their lines "down to the semi-colon". The film itself was shot in the summer of 2007, over a roughly three-week period. By most accounts, the set was very pleasant for both cast and crew. Post production on The Watermelon took the better part of a year. A great deal of attention was given to the soundtrack. The original live vocal performances were very carefully worked, in order to avoid overdubbing, which would have destroyed the immediacy of the actors performances. Peter Girard was engaged to write the original score at a point about six months into post production, and the final musical cue was laid in roughly ten days before the San Diego Film Festival, where the film premiered.

==Release, distribution, award==
The Watermelon received its World Premiere at the 2008 San Diego Film Festival.  It was subsequently acquired by Celebrity Video Distribution and released to the public on July 7, 2009. In January 2011 The Watermelon was the recipient of the California Film Awards prestigious "Diamond Award" for feature film. 

==References==
 

== External links ==
*  
*  

* Bills Movie Reviews  .
* Turner Classic Movies  .

 
 
 
 
 