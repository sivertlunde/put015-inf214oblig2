Pavithra
 
{{Infobox film
| name           = Pavithra
| image          = Album_123musiq.com_Pavithra.JPG
| caption        =
| director       = K. Subhaash
| producer       = K. Subhaash
| writer         = K. Subhaash Radhika Ajith Kumar Nassar
| music          = A. R. Rahman
| cinematography = Bernat S. David
| editing        = P. Madhan Mohan
| studio         = Thanuja Films
| distributor    = Thanuja Films
| released       =  
| runtime        =
| website        =
| country        = India Tamil
| budget         =  73 lakh
}}
 Raadhika in the lead role with Nassar and Ajith Kumar in supporting roles. The films music was by A. R. Rahman and lyrics by Vairamuthu. The film opened on November 5, 1994 as one among Deepavali releases and received commercial hit.

==Plot==
The film is about a quasi-mother-son relationship between Radhika and Ajith. Ajith is a patient suffering from Cancer and Radhika is a nurse in the hospital where Ajith is being treated. Since Ajiths age is same as her child,which was still born and died and she is childless, Radhika showers maternal affection on Ajith, which Radhikas husband mistakes as something else because of a villain doctor who is a rogue  . Confusion ensues and Radhika misunderstands Ajith and the climax is how all confusions are laid to rest, along with the hero!

==Cast== Raadhika as Pavithra
*Ajith Kumar as Ashok
*Nassar as Raghu Keerthana as Chitra
*S. S. Chandran
*Vadivelu
*Kovai Sarala
*Disco Shanti

==Production==
The title denotes Sanskrit for purity (disambiguation)|purity. Ajith Kumar revealed in an interview that his positive role of the film changed his life to an extent and made him take films seriously as a career.  Ajith Kumars character was dubbed for by Shekhar, in his second venture. 

==Release== National Film Unnikrishnan and the National Film Award for Best Lyrics for Vairamuthu. Furthermore the film won third prize in the Tamil Nadu State Film Award for Best Film winners list.

==Soundtrack==
{{Infobox album  
| Name        = Pavithra
| Type        = film
| Artist      = A. R. Rahman
| Cover       = Album_pavithra_cover.jpg 1994
| Recorded    = Panchathan Record Inn
| Genre       = Film soundtrack
| Length      = 24:09
| Label       = 
| Producer    = A. R. Rahman
| Reviews     = 
| Last album  = Kadhalan (soundtrack)|Kadhalan  (1994)
| This album  = Pavithra (1994)
| Next album  = Karuththamma (1994)
}}

{{tracklist
| headline = Track Listing
| extra_column = Singer(s)
| all_lyrics = Vairamuthu
| all_music = A. R. Rahman
| title1 = Sevvaanam Chinna Pen
| extra1 = Mano (singer)|Mano, S.P.B.Pallavi
| length1 = 3:47
| title2 = Eechambazham
| extra2 = Shahul Hameed, K. S. Chithra
| length2 = 4:59
| title3 = Uyirum Neeyae
| extra3 = Unni Krishnan
| length3 = 5:25
| title4 = Mottu Vitadha
| extra4 = Swarnalatha
| length4 = 4:36
| title5 = Azhagu Nilave
| extra5 = K. S. Chithra
| length5 = 5:23
}}

==References==
 

 

 
 
 
 
 