Lal Bahadur Shastri (film)
{{Infobox film
| name           = Lal Bahadur Shastri
| image          = Lal Bahadhur Shasthri.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Rejishh Midhila
| producer       = Jose Simon  Rajesh George
| writer         = Rejishh Midhila
| starring       = Jayasurya Aju Varghese Nedumudi Venu
| music          = Bijibal
| cinematography = Eldo Isaac
| editing        = Sandeep Nandakumar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Lal Bahadur Shastri (2014) is a Malayalam comedy film directed and scripted by Rejishh Midhila.  The film is about three strangers who come across during their journey of life. It revolves around how they influence each other.  The movie stars Jayasurya, Nedumudi Venu and Aju Varghese in the title roles of Lal, Bahadur and Shasthri respectively.  The music of the film is composed by Bijibal and the lyrics are penned by Santhosh Varma. The film is produced by Jose Simon and Rajesh George.Jayasurya s son Adwaith made his début in the film.  He plays the childhood version of Jayasurya s character in the film. 

==Plot==
Lal (Jayasurya) is a simple good hearted young man who is search for a job, Bahadhur (Nedumudi Venu) is a former panchayath president who have an obsession towards liquor and women, and Shasthri(Aju Varghese) is a young farmer who struggles to get some document approved from a Government sector.
The three men, who are total strangers travels to Ernakulam in a KSRTC bus. Bahadhur buys a lottery ticket from a boy and the boy gives another ticket to Lal as he doesnt have change. The trio part ways after reaching Ernakulam; but later come to know that the lottery ticket which Lal got has won one crore rupees. Then Lal, Bahadhur and Shasthri join hands and goes in search of the missing ticket. This forms the crux of the story.

==Cast==

*Jayasurya as Sree Lal
*Nedumudi Venu as Bahadur
*Aju Varghese as Darmajan Shastri
*Mala Aravindan as Damodharan
*Nandhu as Lals father
*Parvathy as Lals mother
*Nobi as Balan
*Minon as Lottery selling boy
*Kavitha Nair as Teacher
*Pradeep Kottayam
*Amith Chakkalakkal as Arjun
*Adwaith Jayasurya as Lals childhood
*Sandra Simon as Thara
*Lakshmipriya as Agricultural officer
*Aiswarya Nath

== Critical reception ==
Lal Bahadur Shastri got mixed to good reviews from
critics."Friends media" gave four out of five stars for the film and
stated that the film is a clean entertainer
"Live media" gave a rating of 4.5/5 for the
movie while "The Cinima Company" gave 3 out of 5
and said "LBS is a one time watchable film without
much expectation that wont make you bored".
LemonMovieMedia rated this as a nice one time
watchable movie, without much twists and turns.
Lay back, relax and watch though it doesn’t offer
anything exceptional or new.
Akhila Menon of "FilmiBeat" gave 2.5/5 and
concluded "A one time watch".

==Release==
The film released around 80 screens in kerala, which is the highest release ever by a Jayasurya Film.   Released on 21 November, distributed by Vendhar Movies in Kerala and Humming Minds Entertainment distributed in outside Kerala

==References==

 

==External links==
*  
#  
# 
# 
# 

 
 
 