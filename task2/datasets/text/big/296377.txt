Haunted Castle (2001 film)
{{Infobox film
| name = Haunted Castle
| image_size = 
| image	=	Haunted Castle FilmPoster.jpeg
| alt = 
| caption = 
| director = Ben Stassen
| producer = Ben Stassen Charlotte Clay Huggins
| writer = Ben Stassen Kurt Frey
| starring = Jasper Steverlinck Kyoko Baertsoen Harry Shearer Arid Lunascape Lunascape
| cinematography = Kommer Kleijn
| editing = Ben Stassen
| studio = 
| distributor = Ventura Distribution nWave Pictures
| released = February 23, 2001
| runtime = 38 minutes
| country = Belgium United States
| language = English
| budget = 
| gross = $13,651,656
}} animated horror PG and 3D effects.

Written by Kurt Frey and directed by co-writer Ben Stassen, the film plays out very much like many modern video games, and can be divided into two types of segments: those in which the audience is seeing through the eyes of the main character, and those in which a scene plays out where the main character is actually in the shot.

==Plot== Belgian rock band Arid (band)|Arid) has been notified by a British law firm that his mother (voiced by Kyoko Baertsoen, leader of another Belgian band, Lunascape (Belgian band)|Lunascape), an aging rock star whom Johnny hasnt seen or heard from since he was 3 years old, has died in a helicopter accident. Johnny has been willed her castle and all of her property and money, but he must visit the actual estate, located in England, to claim these things. As he drives up to the castle, a lightning bolt hits a grave on the castle grounds, and a glowing sphere emerges.

As Johnny enters the building, he walks through a hall with several suits of armor. The suits come alive and begin attacking him when suddenly the light sphere appears and destroys them all, then a demonic entity beckons Johnny further into the castle. Johnny stumbles on a room of instruments levitating and playing themselves, and then walks into a great hall. There is an orb embedded into the ground that begins projecting the image of Johnnys mother. (This segues into a rather lengthy musical number in which this holographic image (Baertsoen) sings an operatic number while the cameras circle around her. The song is named Lane Navachi from Lunascapes album Reflecting Seylence.)
 Mephisto (Mr. Ds chief lieutenant, also voiced by Shearer), guides him through the sections of Hell where musicians who have sold their souls are violently tortured. Mephisto reveals that there was a time when luring people to Hell with fame in music was unsuccessful - until the invention of Rock and Roll.

Johnny is taken on a roller coaster ride through Hell that includes stops in a performance hall where the souls of the damned are trapped inside robot monkeys; they are forced to perform for an audience of demons while a wrecking ball swipes them off the stage and destroys them one by one. But as he travels through the Castle and Hell, the glowing sphere - revealed to be the spirit of his mother - appears before him time and again, warning him of the danger awaiting him should he give in to the Devils offer. Eventually Johnny ends up in a decrepit opera hall where the worst of tortures are taking place. Here he witnesses opera singers being decapitated or lowered into vats of acid and barracuda. Mephisto reveals to Johnny that Mr. D once had a romance with an opera singer who broke his heart, and now D has a particularly violent aversion for opera music.

Johnny eventually ends up back in front of Mr. D, who once again entices him to sign. Mephisto gives Johnny a guitar and he considers the offer, then throws the guitar into the flames and begins to sing opera at the top of his lungs, and it takes on an ethereal quality. Mr. D disappears, Mephisto attempts to flee but is crushed by falling debris. As the entire castle collapses, the soul of Johnnys mother returns to its resting place once more.

The film jumps ahead six months later, and we see that Johnny is now a famous rock and roll star (having never signed the contract with Mr. D). The last five minutes of the film feature a performance by Arid (band)|Arid, as the credits float by them in little bubbles burst by demons.

==See also==
*Haunted House (short film)

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 