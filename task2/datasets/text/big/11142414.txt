Gnan Ninne Premikkunnu
{{Infobox film
| name           = Gnan Ninne Premikkunnu
| image          = NjanNinnefilm.jpg
| image_size     =
| caption        = Vinyl Records Cover
| director       = K. S. Gopalakrishnan
| producer       =
| writer         = VK Kumar Surasu (dialogues)
| screenplay = RC Santhi Janardhanan Murali Sudheer Ushakumari
| music          = M.S.Baburaj 
| cinematography = J Williams
| editing        = A Ramesan
| distributor    = Devi Prabha Arts
| studio         = Devi Prabha Arts
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1975 Cinema Indian Malayalam Malayalam film, Janardhanan and Murali Sr. in lead roles. The film had musical score by MS Baburaj.  

==Cast==
 
*Kamalahasan
*Girija Janardhanan
*Murali Sr. Sudheer
*Ushakumari
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran and Bichu Thirumala.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aakaashathinu Mounam || K. J. Yesudas || P. Bhaskaran ||
|-
| 2 || Dhoomam Dhoomaananda || Bichu Thirumala, Ambili, KP Brahmanandan, Kamalahasan || Bichu Thirumala ||
|-
| 3 || Manasse Ashwasikkoo || S Janaki || Bichu Thirumala ||
|-
| 4 || Vasantham Maranjappol || K. J. Yesudas, LR Anjali || P. Bhaskaran ||
|}

==References==
 

 
 


 