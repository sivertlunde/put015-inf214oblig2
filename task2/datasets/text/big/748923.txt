Kids Return
{{Infobox film
| name = Kids Return
| image = KidsReturnPoster.jpg
| director = Takeshi Kitano Masayuki Mori Yasushi Tsuge Takio Yoshida
| writer = Takeshi Kitano
| music = Joe Hisaishi
| starring = Masanobu Ando Ken Kaneko Leo Morimoto Hatsuo Yamatani Michisuke Kashiwaya Mitsuko Oka Yuuko Daike Ryo Ishibashi
| cinematography = Katsumi Yanagishima
| editing = Takeshi Kitano
| distributor = Office Kitano
| released =  
| runtime = 103 minutes Japanese
| budget =
}} 1996 Cinema Japanese film written, edited and directed by Takeshi Kitano. The film was made directly after Kitano recovered from a motorcycle wreck that left one side of his body Paralysis|paralyzed. After extensive surgery and physical therapy he quickly went about making Kids Return amidst speculation that he might never be able to work again. The music was composed by Joe Hisaishi, and the cinematographer was Katsumi Yanagishima.

==Plot==
The movie is about two high school dropouts, Masaru (Ken Kaneko) and Shinji (Masanobu Ando), who try to find a direction and meaning in their lives&mdash;one by becoming a yakuza lieutenant, the other by becoming a boxing|boxer.

==Reception==
At the time of its release it was Kitanos most successful film yet in his native Japan, which until then had been notedly much less enthusiastic about his films than international viewers.

==Cast and roles==
* Ken Kaneko as Masaru
* Masanobu Ando as Shinji
* Leo Morimoto as Teacher
* Hatsuo Yamatani as boxing club manager (credited as Hatsuo Yamaya)
* Michisuke Kashiwaya as Hiroshi
* Mitsuko Oka as coffee-shop owner, Sachikos mother
* Yuuko Daike as Sachiko
* Ryo Ishibashi as local Yakuza chief
* Susumu Terajima as No. 2 in local gang
* Moro Morooka as Hayashi
* Peking Genji	
* Atsuki Ueda as Reiko
* Kotaro Yoshida
* Koichi Shigehisa as Trainer
* Kyôsuke Yabe
* Yoshitaka Ôtsuka as delinquent
* Masami Shimojô as Yakuza godfather
* Kazuki Oh
* Shintarô Hasegawa
* Kanji Tsuda
* Yojin Hino as taxi office worker (credited as Yôjin Hino)
* Ren Osugi as taxi passenger
* Takashi Hagino

==Soundtrack==
{{Infobox album
| Name        = Kids Return
| Type        = soundtrack
| Artist      = Joe Hisaishi
| Cover       = KidsReturn.jpg
| Length      = 
| Recorded    =
| Released    = 26 June 1996
| Label       = Polydor
}}

All compositions by Joe Hisaishi.
#"Meet Again" − 5:02
#"Graduation" − 1:07
#"Angel Doll" − 2:21
#"Alone" − 1:15
#"As a Rival" − 1:29
#"Promise... for Us" − 5:08
#"Next Round" − 1:28
#"Destiny" − 3:31
#"I Dont Care" − 2:18
#"High Spirits" − 2:03
#"Defeat" − 2:29
#"Break Down" − 3:46
#"No Way Out" − 2:51
#"The Day After" − 0:44
#"Kids Return" − 4:40

==External links==
* 
*  
*  

Reviews
* 
*  at Senses of Cinema

 
 

 
 
 
 
 
 

 