Wife Wanted (1946 film)
{{Infobox film
| name           = Wife Wanted
| image          = Wife wanted poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Phil Karlson
| producer       = Jeffrey Bernerd Kay Francis
| screenplay     = Caryl Coleman Sidney Sutherland	
| based on       =  
| narrator       = 
| starring       = Kay Francis Paul Cavanagh Robert Shayne
| music          = Edward J. Kay
| cinematography = Harry Neumann
| editing        = Richard C. Currier Ace Herman
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
}}
Wife Wanted is a 1946 American crime directed by Phil Karlson, featuring Kay Francis, Paul Cavanagh and Robert Shayne. This was Kay Franciss last film.   The films screenplay was written by Caryl Coleman and Sidney Sutherland, based on the novel Wife Wanted by Robert E. Callahan. 

==Plot==
Carole Raymond (Francis) is a film star whose best years are behind her so she decides to buy in a real estate plan with Jeff Caldwell (Cavanagh), whos really running an illegitimate matrimonial service.

==Cast==
* Kay Francis as Carole Raymond
* Paul Cavanagh as Jeffrey Caldwell
* Robert Shayne as Bill Tyler
* Veda Ann Borg as Nola Reed
* Teala Loring as Mildred Kayes
* Jonathan Hale as Philip Conway Tim Ryan as Bartender
* Barton Yarborough as Walter Desmond

==Production==
Kay Francis was unhappy with the first script, so she and director Phil Karlson set about obtaining a re-write, and became a producer on the film. 

The production period was from mid-June to early July 1946. 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 