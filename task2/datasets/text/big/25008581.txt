Foreign Affaires
{{Infobox film
| name = Foreign Affaires
| image =
| image_size =
| caption =
| director = Tom Walls
| producer = Michael Balcon
| writer = Ben Travers
| music = Jack Beaver
| cinematography = Roy Kellino
| editing = Alfred Roome
| narrator =
| starring = Tom Walls Ralph Lynn Robertson Hare  Norma Varden
| distributor = Gainsborough Pictures
| released = 1935
| runtime = 71 minutes
| country = United Kingdom
| language = English
| budget =
| preceded_by =
| followed_by =
}}
Foreign Affaires is a 1935 British comedy film directed by and starring Tom Walls. It also features Ralph Lynn, Robertson Hare, Norma Varden and Cecil Parker. The screenplay is by Ben Travers, and the cast included cast members from the Walls and Travers Aldwych Farces.  The film is set on the French Riviera where two hard-living British spongers become mixed up in illegal gambling.  

==Cast==
* Tom Walls - Captain Archibald Gore
* Ralph Lynn - Jefferson Darby
* Robertson Hare - Mr Hardy Hornett
* Norma Varden - Mrs Hardy Hornett
* Marie Lohr - Mrs Cope Diana Churchill - Sophie
* Cecil Parker - Lord Wormington
* Kathleen Kelly - Millicent
* Gordon James - Rope
* Ivor Barnard - Count
* Mervyn Johns - Courtroom interpreter
* Basil Radford - Basil Mallory
* Martita Hunt - Woman at Lord Wormingtons house

==References==
 

 

 
 
 
 
 
 
 
 
 


 