Hair Show
{{Infobox film
| name           = Hair Show
| image          = Hair_Show_DVD.jpg
| director       = Leslie Small
| producer       = Janis Woody Jeff Clanagan Kimberly Ogletree Leslie Small Magic Johnson Nikkole Denson
| writer         = Andrea Allen-Wiley Devon Greggory Sherri A. McGee
| starring       = Kellita Smith MoNique David Ramsey Gina Torres Taraji P. Henson
| music          = Kennard Ramsey
| cinematography = Keith L. Smith
| editing        = Hector H. Kron Suzanne Hines
| distributor    = Innovation Film Group UrbanWorks Entertainment
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| gross          = $305,281 
}}
Hair Show is a 2004 comedy film directed by Leslie Small that stars MoNique and Kellita Smith.

==Plot== Beverly Hills, Los Angeles. IRS and only has 60 days to pay $50,000 in back taxes. After some hilarious moments and passionate exchanges, the two sisters join forces to fight off a pesky rival salon owner Marcella (Gina Torres) and save Peaches from her troubles by competing for a lucrative cash prize and bragging rights at the citys annual hair show.

==Cast==
*MoNique as Patricia "Peaches" Whittaker
*Kellita Smith as Angella "Angelle" Whittaker 
*Gina Torres as Marcella
*David Ramsey as Cliff
*Taraji P. Henson as Tiffany
*Keiko Agena as Jun Ni
*Cee Cee Michaela as Simone
*Joe Torry as Brian
*Andre B. Blake as Gianni
*Bryce Wilson as Drake
*Vivica A. Fox as Herself
*Tommy "Tiny" Lister Jr. as Agent Little
*Tom Virtue as Agent Scott
*Reagan Gomez-Preston as Fiona James Avery as Seymour Gold

==Reception==
The movie was a box office failure, grossing just $305,281.

==Nominations==
2005 BET Comedy Awards
*Outstanding Directing for a Theatrical Film — Leslie Small
*Outstanding Lead Actress in a Theatrical Film — MoNique
*Outstanding Writing for a Theatrical Film — Andrea Allen-Wiley, Devon Watkins, Sherri A. McGee

==References==
  
  tags!-->

==External links==
* 
* 

 
 
 
 
 
 


 