Frank and Jesse
{{Infobox Film
| name           = Frank and Jesse
| image          = Frank and Jesse (1995) Theatrical Poster.jpg
| caption        = Original Theatrical Poster
| director       = Robert Boris
| producer       = Cassian Elwes Elliott Kastner
| writer         = Robert Boris
| starring       = Rob Lowe Bill Paxton Randy Travis William Atherton Alexis Arquette
| music          = Mark McKenzie
| cinematography = Walt Lloyd
| editing        = 
| distributor    = Trimark
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $ 50,004
}} western film written and directed by Robert Boris and starring Rob Lowe as Jesse James and Bill Paxton as Frank James. Based on the story of Jesse James, the film focuses more on the myth of The James Brothers than the real history. It originally aired on HBO.

== Synopsis == Bob Ford Charles Ford (Alexis Arquette), Clell Miller (John Pyper-Ferguson), and Arch Clements (Nick Sadler), begin to feel oppressed by the Chicago railroad investors. They set off on a trail of bank robberies, train heists, and stage holdups while evading the dogged pursuit of Allan Pinkerton (William Atherton) and his detective agency.

== Cast ==
* Rob Lowe as Jesse James
* Bill Paxton as Frank James
* Randy Travis as Cole Younger
* Dana Wheeler-Nicholson as Annie
* Maria Pitillo as Zee
* Luke Askew as Lone Loner
* Sean Patrick Flanery as Zack Murphy Charlie Ford
* Todd Field as Bob Younger
* John Pyper-Ferguson as Clell Miller
* Nicholas Sadler as Arch Clements
* William Atherton as Alan Pinkerton
* Tom Chick as Detective Whitcher
* Mary Neff as Widow Miller Richard Maynard as John Sheets Bob Ford
* Mari Askew as Ma James
* William Michael Evans as Jesse Jr.
* Lyle Armstrong as McGuff
* Cole S. McKay as Sheriff Baylor
* Dennis Letts as Railroad C.E.O.
* John Stiritz as Ruben Samuels
* Micah Dyer as John Younger
* Jackie Stewart as Governor Crittendon
* Chad Linley as Archie Samuels
* Rhed Killing as Stage Driver
* Jerry Saunders as Northfield Teller
* D.C. Dash Goff as Engineer
* Robert Moniot as Young Captain
* Norman Hawley as Baptist Preacher
* Jeffrey Paul Johnson as Davies Bank Teller
* Bryce Thomason as Reporter
* John Paxton as Working Man
* Elizabeth Hatcher-Travis as Woman on Train
* Sudie Henson as Old Woman on Train
* David Arquette (uncredited)
* Ron Licari as Townsman (uncredited) 

== Soundtrack ==
{{Infobox album  
| Name        = Frank and Jesse
| Type        = Soundtrack
| Artist      = Mark McKenzie
| Cover       = Frank and Jesse Soundtrack.gif
| Released    = 1995
| Recorded    = 1995
| Genre       = Soundtrack
| Length      = 
| Label       = Intrada Records
| Producer    =
}}

The music score was composed by Mark McKenzie and released by Intrada Records. 

*Frank and Jesse Suite
*Main Title
*Family Moments
*Gentle Spirits
*Tragedy At Home
*Meet the James Gang
*Marauding
*Daring Escape
*Franks Despair
*The Peace Ranch
*Mountain Top Dance
*The Lord is Callin You
*Northfield Battle
*I Play Not Marches ...
*Goodbye Jesse
*Justice Will Be Served

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 