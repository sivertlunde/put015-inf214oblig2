The Understanding Heart
{{Infobox film
| name           = The Understanding Heart
| image          =
| caption        = Jack Conway
| writer         = Joseph Farnham (titles)
| screenplay     = Edward T. Lowe, Jr.
| based on       =   Francis X. Bushman, Jr. Carmel Myers
| music          =
| cinematography = John Arnold John English
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = Silent English intertitles
| budget         =
| gross          =
}}
 silent Adventure adventure drama Jack Conway and stars Joan Crawford in an early leading role. The film was adapted for the screen by Edward T. Lowe, Jr. from the novel of the same name by Peter B. Kyne. 

==Synopsis==
Forest ranger Bob Mason (Fellowes) kills a man in self-defense. Kelcey Dale (Carmel Myers), to whom Bob is attracted, commits perjury and causes him to be convicted for murder. Bob escapes and is sheltered by Kelceys sister, Monica Dale (Crawford).

==Cast==
*Joan Crawford - Monica Dale
*Rockliffe Fellowes - Bob Mason
*Ralph Bushman - Tony Garland
*Carmel Myers - Kelcey Dale
*Richard Carle - Sheriff Bentley
*Jerry Miley - Bardwell Harvey Clark - Uncle Charlie

==Production notes==
The Understanding Heart features footage of fires used in the 1926 film The Fire Brigade. 

==References==
 

==External links==
* 
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 