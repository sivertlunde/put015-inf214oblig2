The Bulleteers
{{Infobox Hollywood cartoon|
| cartoon_name = The Bulleteers Superman
| image = Bulleteers1.JPG
| caption = Title card from The Bulleteers
| director = Dave Fleischer Bill Turner Carl Meyer
| animator = Orestes Calpini   Graham Place
| voice_actor = Bud Collyer   Joan Alexander   Julian Noa   Jackson Beck
| musician = Sammy Timberg
| producer = Max Fleischer
| studio = Fleischer Studios
| distributor = Paramount Pictures
| release_date = March 26, 1942 (USA)
| color_process = Technicolor
| runtime = 8 min. (one reel)
| preceded_by = The Arctic Giant (1942)
| followed_by = The Magnetic Telescope (1942)
| movie_language = English
}}

The Bulleteers is the fifth of the seventeen animated Technicolor short films based upon the DC Comics character of Superman, originally created by Jerry Siegel and Joe Shuster. This animated short was created by the Fleischer Studios.  The story runs about nine minutes and covers Supermans adventures as he defends the city against a villainous gang called "The Bulleteers", who are equipped with a bullet-shaped rocket.  It was originally released 26 March 1942.   

==Plot==
The story begins as the clock strikes midnight.  A strange, bullet-shaped rocket-car blows right through the police department, leaving an explosion in its wake.  The paper the next day reports the destruction of the building and bafflement of the police.  Perry White calls Lois Lane and Clark Kent into his office.  Just as he is explaining the report, the sound of a loudspeaker comes in through the window.  The leader of the "Bulleteers", as Lois later calls them, is shown announcing from his hideout atop a mountain outside of town, the demands of his gang.  Over the speaker, Clark, Lois, and the rest of the town hear it: Turn over the city treasury or other municipal buildings will be next!

 
Later, Lois asks the mayor what he is doing about the problem. The mayor announces that he will not be swayed by criminals. That day, policeman all over town build bunkers and gather ammunition in preparation for the Bulleteers.  At midnight, the gang strikes again, first destroying the towns power plant, bullets from defending policeman bouncing harmlessly off the bullet-cars sleek surface.  Lights in the Daily Planet flicker on and off, and Lois takes off in a car to get closer to the scene, leaving Clark behind.  Clark takes the opportunity to enter a nearby phone booth and don his Superman costume.

The Bulleteers take aim now at the citys treasury building, but Superman steps in front of them and knocks the rocket-car off course.  As they struggle to regain control, he leaps in the air and grabs its front trying again to force it off-course, but the Bulleteers, through wild maneuvering, manage to shake him off the car to the ground below.  Superman lunges to keep them from the treasury, only to arrive too late.  Piles of rubble from the explosion bury him.

Lois Lane arrives at the scene in time to see the gang throwing bags of money into their car.  She sneaks into its cockpit and tries to smash the controls with a wrench, but the gang returns, taking off with her.  Superman, meanwhile, emerges from the rubble and chases after the car, grasping it by one of its retractable wings, and then by its tail-fins to throw it off course.  As it spirals downward, he claws his way to the cockpit, rips it open, and pulls Lois and the three gangsters out.  The car crashes to the ground far below.

The newspaper next day reports Supermans heroic feat and the gangsters arrest for their rampage.  Reading it, Clark remarks, "Nice going, Lois.  Another great scoop for you."  Lois replies, "It was easy, thanks to Superman."

==Cast==
*Bud Collyer as Superman/Clark Kent, Bulleteer, Police Officer, Printer
*Joan Alexander as Lois Lane
*Julian Noa as Perry White, Mayor
*Jackson Beck as the Narrator

==Appearances==
*In  , the restored bullet car appears as one of Supermans trophies in his Fortress of Solitude.
*The Line "We wont be intimidated by criminal threats" was used in Various promos for The Action cartoon block Toonami.

==References==
 

==External links==
*  
*  at the Internet Archive
*  at the Internet Movie Database
*   on YouTube

 

 
 
 
 
 
 