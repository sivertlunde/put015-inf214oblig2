The Informer (1912 film)
{{Infobox film
| name           = The Informer
| image          = 
| image size     = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = George Hennessy
| narrator       =  Walter Miller
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 18 minutes (16 Frame rate|frame/s)
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}
 Harry Carey, Lionel Barrymore, Dorothy Gish and Lillian Gish. Prints of the film survive at the film archive of the Library of Congress.    

==Cast== Walter Miller - The Confederate Captain
* Mary Pickford - The Confederate Captains Sweetheart
* Henry B. Walthall - The False Brother
* Kate Bruce - The Mother Harry Carey - The Union Corporal
* Lionel Barrymore - Union Soldier
* Elmer Booth - Union Soldier
* Clara T. Bracy - Negro Servant
* Christy Cabanne Edward Dillon - Confederate Soldier John T. Dillon - Union Soldier
* Dorothy Gish
* Lillian Gish
* Joseph Graybill - Union Soldier
* Robert Harron
* W. Chrystie Miller
* Gertrude Norman
* Alfred Paget - Condederate General
* Jack Pickford - Negro Boy
* W. C. Robinson - Union Soldier

==See also==
* Harry Carey filmography
* D. W. Griffith filmography
* Lillian Gish filmography
* Lionel Barrymore filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 