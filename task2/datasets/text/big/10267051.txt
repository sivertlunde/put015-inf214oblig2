Artists Under the Big Top: Perplexed
{{Infobox film
| name           = Artists Under the Big Top: Perplexed
| image          = 
| caption        = 
| director       = Alexander Kluge
| producer       = 
| writer         = Alexander Kluge
| starring       = Hannelore Hoger
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
}}
Artists in the Big Top: Perplexed ( ) is a 1968 West German film written and directed by Alexander Kluge. The film is made in a collage style, featuring newsreels and quotations from philosophers alongside the story of a failing circus whose owner, Leni (Hannelore Hoger), must decide whether her dream of a new kind of circus is too optimistic. The film is a symbolic representation of Kluges own frustrations in trying to help stimulate the New German Cinema movement. 

==Cast==
* Hannelore Hoger as Leni Peickert
* Sigi Graue as Manfred Peickert (as Siegfried Graue)
*   as Dr. Busch
* Bernd Höltz as Herr von Lueptow
* Eva Oertel as Gitti Bornemann
* Kurt Jürgens as Mackensen, Dompteur
* Gilbert Houcke as Houke, Dompteur
* Wanda Bronska-Pampuch as Frau Saizewa
* Herr Jobst as Impresario
* Hans-Ludger Schneider as Assessor Korti
* Klaus Schwarzkopf as Gerloff, Philologe

==Awards== Best Foreign Language Film at the 41st Academy Awards, but was not accepted as a nominee. 

==See also==
* List of submissions to the 41st Academy Awards for Best Foreign Language Film
* List of German submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 