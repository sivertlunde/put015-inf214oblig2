100 Million BC
 
{{Infobox Film
| name           = 100 Million BC
| image          = 100_Million_BC.jpg
| image_size     = 
| caption        = 
| director       = Griff Furst
| producer       = David Michael Latt David Rimawi Paul Bales
| writer         = Paul Bales Michael Gross Christopher Atkins Greg Evigan Stephen Blackehart Geoff Mead   Dayne Smith
| music          = Ralph Rieckermann
| cinematography = Alexander Yellen Mark Atkins
| distributor    = The Asylum
| released       = July 29, 2008
| runtime        = 85 mins.
| country        = United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
100 Million BC (released as Jurassic Commando in France) is a 2008 direct-to-DVD action film by  film studio The Asylum, continuing the urban myth of the Philadelphia Experiment.

==Plot== American Dr. Michael Gross), the Philadelphia Experiment. The film begins in 1998, when Reno has purportedly perfected time travel technology. The SEALs use it to travel backwards to the year 112,000,000 BC (despite the movies title) of the Early Cretaceous, in order to rescue a previous, 1949 expedition led by Renos brother Erik (Christopher Atkins).

Renos team arrives in Laurasia six years after the 1949 team arrived, to find only a few survivors. The SEAL team is ill-equipped for the dangers of this prehistoric period, and is decimated by a Tyrannosaurus and other prehistoric life.

Having accomplished their mission, remnants of the two teams merge into one and return to the present, but in the process the anomaly malfunctions, allowing the subadult,  ,   Tyrannosaurus to travel to the present and wreak havoc in rural Los Angeles. Reno stays behind, ostensibly to close the portal, but instead transports himself to the year 1950 after being cornered by the dinosaur.  Apparently he tried to leap through the portal as it closed, but was sent to 1950 instead of 1998.

In the present, survivors of the team run through the farms of Oklahoma, barely evading the rampaging prehistoric beast. Meanwhile in 1950, the wounded Dr. Reno arrives and meets up with the younger version of himself. The old Reno teaches the young Dr. Reno (Dustin Harnish) how to correctly use the time-travel technology before dying. Young Dr. Reno assembles a strike team of soldiers and leads them through the portal to 1998, where they attempt to rescue the remainder of the original team from the rampaging dinosaur.

Young Reno then has to find a means of luring the creature back into the anomaly and returning it to its own time, before it takes the life of his brother from him once more.  Finally the dinosaur is banished back to its time where the trip fuses it with a mountain, killing it. Frank opens a portal to 1950 for everyone to return home through.  Frank and one of the rescued team members plan to start a relationship. Someone needs to stay behind to close the portal, Erik does, his lover Betty staying with him.  The two plan to catch up with the groups old selves.

==Cast== Michael Gross - Dr. Frank Reno
*Christopher Atkins - Erik Reno
*Greg Evigan - LCDR Ellis Dorn
*Stephen Blackehart - Lt. Robert Peet
*Geoff Mead - CPO Lopes
*Wendy Carter - Betty
*Marie Westbrook - Ruth
*Dean Kreyling - Chief "Bud" Stark
*Phil Burke - Stubbs Nick McCallum - Burke
*Aaron Stigger - Manriquez
*Daniel Ponsky - Jones
*James Viera - Przyzicki
*Dustin Harnish - Young Frank Reno
*Prince Pheenix Wade - Myrick

==See also== Journey to the Center of the Earth - Another film by The Asylum also featuring contact between humans and dinosaurs, and also starring Greg Evigan. The Land That Time Forgot - Another Asylum film with dinosaurs and time-displacement, which also features Stephen Blackehart.

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
  
 
 
 
 
 
 
 
 