The Last Polka
 
{{Infobox film
| name           = The Last Polka
| image          = LastPolka.jpg
| image_size     =
| caption        =
| director       = John Blanchard
| producer       = John Candy (Ex Prod) Eugene Levy (Ex Prod)  Jamie P Rock (Prod)
| writer         = John Candy Eugene Levy Dave Thomas
| starring       = John Candy Eugene Levy
| music          = Russ Little
| cinematography = Martin Corley  John Crampton Richard G. Mason
| editing        = Bill Goddard
| distributor    = Vestron Video
| released       =  
| runtime        = 54 mins
| country        = Canada English
| budget         =
}}
The Last Polka is a 1985 comedy television film, and one of the first mockumentaries. It was written by and starred John Candy and Eugene Levy, and directed by John Blanchard.

The Last Polka follows the life, careers, and the final concert of Yosh (Candy) and Stan (Levy) Shmenge, two brothers from the fictional country of Leutonia who become the biggest polka duo the world has ever seen. It is presented as a mixture of live concert performance and filmed scenes.
 Dave Thomas narrates.
 Touch Me".

One stand out scene has The Shmenges performing an outdoor stadium Michael Jackson salute concert, complete with glitter outfits, sequin glove, and a polka version of "Beat It".

The Shmenges were originally created as characters for the television series Second City TV and were also known on SCTV as The Happy Wanderers.

The Last Polka was produced for and first broadcast on the HBO cable network. Both John Candy and Eugene Levy were nominated in 1985 for a Cable Ace Award in the Best Performance in a Comedy Special category.

As of March 2008, this video is out of print and has not yet been released on DVD.

 

==External links==
* 
*  
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 