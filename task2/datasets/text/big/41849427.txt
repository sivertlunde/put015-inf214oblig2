Chakravyuha (1983 film)
{{Infobox film|
| name = Chakravyuha
| image = 
| caption =
| director = V. Somashekhar
| writer = M. D. Sundar Ambika   Vajramuni
| producer = N. Veeraswamy
| music = Sankar Ganesh
| cinematography = R. Chittibabu
| editing = Yadav Victor
| studio = Eshwari Productions
| released = 1983
| runtime = 146 min
| language = Kannada
| country =  India
| budgeBold textt =
}}
 1983 Kannada Ambika in lead roles. This film marked the entry of popular actor Mukhyamantri Chandru into films.  Produced by N. Veeraswamy for Eshwari Productions, this film also starred his son V. Ravichandran in a small role. Actor Tiger Prabhakar also featured in an extended cameo role.

The film, upon release, met with hugely positive reviews and catapulted actor Ambareeshs career to greater heights and coined him the title "Rebel Star" for his angry young man role. 

The films huge success inspired other language industries and was subsequently remade in Bollywood as Inquilaab (film)|Inquilaab (1984) starring Amitabh Bachchan and Sridevi and in Telugu as Mukhyamanthri. 

== Cast ==
* Ambareesh as Amarnath Ambika as Asha
* Tiger Prabhakar in an extended cameo
* V. Ravichandran
* Mukhyamantri Chandru
* Vajramuni
* Shakti Prasad
* Thoogudeepa Srinivas

== Soundtrack ==
The music was composed by Sankar Ganesh with lyrics by Chi. Udaya Shankar.  All the songs composed for the film were received extremely well, especially "Chali Chali" and the title song are considered as evergreen songs. The song "Nija Heluvenu Amma" was adapted from "Toothpaste Irukku" which was composed by the duo for the Tamil film Ranga (film)|Ranga.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Chakravyuha Idu Chakravyuha
| extra1 = S. P. Balasubramanyam
| lyrics1 = Chi. Udaya Shankar
| length1 = 
| title2 = Chali Chali Thalenu
| extra2 = S. P. Balasubramanyam, S. Janaki
| lyrics2 = Chi. Udaya Shankar
| length2 = 
| title3 = Baa Bande 
| extra3 = S. P. Balasubramanyam, S. Janaki
| lyrics3 = Chi. Udaya Shankar
| length3 = 
| title4 = Nija Heluvenu Amma
| extra4 = S. Janaki, P. Susheela
| lyrics4 = Chi. Udaya Shankar
| length4 = 
| title5 = Rita Rosy Julie
| extra5 = S. P. Balasubramanyam
| lyrics5 = Chi. Udaya Shankar
| length5 = 
}}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 