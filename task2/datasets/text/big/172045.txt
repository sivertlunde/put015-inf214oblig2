The Letter (1929 film)
{{Infobox film name            = The Letter image           =File:The Letter poster 1929.jpg caption         = Film poster director        = Jean de Limur producer        = Monta Bell screenplay      = Garrett Fort based on        =   starring        = Jeanne Eagels O. P. Heggie music           =  cinematography  George Folsey editing         =  distributor     = Paramount Pictures released        =   runtime         = 65 minutes country         = United States language        = English budget          = 
|}} Astoria Studios, The Letter by W. Somerset Maugham. It tells the story of a married woman who kills her lover out of jealousy and is brought to trial.

==Plot==
Bored and lonely living on her husbands rubber plantation, Leslie Crosbie takes a lover, Geoffrey Hammond. Eventually, however, he tires of her and takes a Chinese mistress, Li-Ti. When Leslie finds out, she insists on seeing him while her husband is away. She tries to rekindle his love, but when he tells her that he prefers Li-Ti to her, she becomes enraged and shoots him repeatedly.
 perjures herself on the stand, claiming she had little to do with Hammond and that she shot him when he tried to rape her. Everyone sympathizes, but then Li-Tis emissary provides Joyce, Leslies attorney, with a copy of the letter in which Leslie begged Hammond to come see her. Li-Ti is ready to sell it for $10,000, provided Leslie herself make the exchange. On Joyces advice, Leslie agrees. Li-Ti humiliates her, but eventually accepts the money. Leslie is found not guilty.

Joyce presents his bill to Leslies husband, Robert. He charges no fee, but the expenses come to $10,000. When Robert demands an explanation, Joyce gives him one, and the damning letter. After Joyce leaves, Robert confronts his wife and forces her to admit everything. As punishment, he decides to keep her on the plantation (he has no more money anyway). In return, she boasts that she still loves the man she killed.

==Cast==
*Jeanne Eagels as Leslie Crosbie
*Reginald Owen as Robert Crosbie
*Herbert Marshall as Geoffrey Hammond
*Irene Browne as Mrs. Joyce
*O. P. Heggie as Mr. Joyce
*Lady Tsen Mei as Li-Ti
*Tamaki Yoshiwara as Ong Chi Seng

==Preservation status==
The Letter was long out of circulation. In June 2011, a restored edition of the film was released on home video by Warner Bros. as part of its Warner Archive Collection as a made-on-demand DVD. 

==Awards and nominations==
Eagels, who died just months after the film was completed, was nominated for the Academy Award for Best Actress for her portrayal of the married woman. She was the first performer to be so recognized by the Academy after her death, though hers was not an official nomination. Eagels was among several actresses "under consideration" by a board of judges. {{cite web
  | last = OConnor
  | first = Clint
  | title = James Dean, Spencer Tracy among posthumous Oscar nods
  | publisher = The Plain Dealer
  | date = 2008-07-13
  | url = http://www.cleveland.com/movies/index.ssf/2008/07/james_dean_spencer_tracy_among.html
  | accessdate = 2014-02-23 }}
 
 Top Ten Films of 1929 by the National Board of Review.

==Remake== 1940 Warner Oscar nomination for the role of Leslie Crosbie in the remake, just as Jeanne Eagels had done in 1929.

==See also==
*List of rediscovered films

==References==
 

==External links==
* 
* 
*  All Movie.com
*  at Virtual History
* 
*  at Toronto Film Society, includes reprints of 1929 reviews

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 