Nevermore (2007 film)
{{Infobox film
| name           = Nevermore
| director       = Thomas Zambeck
| image	=	Nevermore FilmPoster.jpeg
| producer       = Angela Barakat Thomas Zambeck
| writer         = Thomas Zambeck
| starring       = Vincent Spano Jennifer ODell Judd Nelson Sidi Henderson                 
| music          = Mark Krench
| cinematography = Oren Goldenberg
| editing        = Josh Wagner
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Nevermore is a 2007 film written and directed by independent director Thomas Zambeck. {{cite web|title=Nevermore|
author=Jason Buchanan|work=Spill.com|url=http://spill.hollywood.com/Movie-Reviews/MovieDetails.aspx?Name=Nevermore&VideoId=475917}} 

==Plot==
Judd Nelson plays a wealthy but unstable hermit who is convinced he will soon become insane much as his father did; his father actually did go insane and murdered Nelsons characters mother.

He hires a private investigator (played by Vincent Spano) to look into his wifes possible motives for helping drive him insane. The wife, played by Jennifer ODell, is a sort of trophy wife; Nelsons character is certain that she is out to both drive him insane and rob him of his fortune.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 


 
 