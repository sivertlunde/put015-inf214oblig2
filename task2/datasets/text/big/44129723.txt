Ricki and the Flash
{{Infobox film
| name           = Ricki and the Flash
| image          = 
| alt            =
| caption        = 
| director       = Jonathan Demme Marc Platt Mason Novick
| writer         = Diablo Cody Ben Platt
| music          = 
| cinematography = 
| editing        = Wyatt Smith
| studio         = Clinica Estetico   Marc Platt Productions   TriStar Productions
| distributor    = TriStar Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
 Ben Platt. A Prairie Home Companion.

Principal photography began on October 13, 2014, in Rye (city), New York|Rye, New York. TriStar Pictures is financing and will distribute the film on August 7, 2015.

== Plot ==
An aging woman, Ricki, chased her dreams of becoming a famous rock star by abandoning her family. She gets a last chance to put things right when her ex-husband Pete asks her to visit Chicago and help their estranged, divorced-daughter Julie through a difficult time.

== Cast ==
* Meryl Streep as Ricki, an aging rock star who abandoned her family to become a famous musician.
* Mamie Gummer as Julie, a divorced daughter of Ricki.
* Kevin Kline as Pete, ex-husband of Ricki.
* Sebastian Stan as Joshua, Rickis estranged son
* Rick Springfield as a guitarist and bandmember of Rickis music band.
* Audra McDonald as Petes wife. Ben Platt as Daniel, the bartender at Ricki and the Flashs regular spot.

== Production == Weinstein and Fox 2000 Ben Platt was added to the cast to play Daniel, the bartender at Ricki and the Flashs regular spot.    On October 24, Sebastian Stan joined the film to play Joshua, Rickis estranged son.    

On August 1, 2014, The Hollywood Reporter issued a story about Streep working hard on learning to play the guitar for the film. Writer Cody told THR, "The reason we haven’t shot the movie yet is Meryls been learning to play."    The film is an inspiration of Codys mother-in-law who fronted a New Jersey bar band for years, Cody said, "I watched her on stage so many times, and I thought to myself, This womans life is a movie."  Cody said that she made up the title character, an aged talented musician who faces the consequences of having chosen music over family. Cody said there are not a lot of complicated roles played by actresses over 50, and Streep was in her mind when she was writing the script for the film.  It would be the second film Streep and her real life daughter Gummer would be seen together after 1986s Heartburn (film)|Heartburn, Cody said to THR, "Its going to be pretty exciting to see to watch them play against each other, I can’t imagine embarking on something like that with my mom. I feel like we would work out a lot of stuff." 

=== Filming ===
Filming was first set to begin on October 1, 2014 in New York.  The principal photography on the film began on October 13, 2014, in Rye (city), New York|Rye, New York.  On October 14, Streep was spotted filming some scenes at Numi & Co. Hair Salon in Rye. On November 3rd she was also seen filming in Nyack, NY. 

== Release ==
On May 6, 2014, Sony Pictures Entertainment|Sonys TriStar set the film for a June 26, 2015 release.  This was later changed to August 7, 2015.

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 