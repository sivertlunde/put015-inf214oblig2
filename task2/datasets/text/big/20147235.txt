Kurama Tengu (film)
{{Infobox film
| name           = Kurama Tengu
| image          =
| caption        =
| director       = Teppei Yamaguchi
| producer       = Arakan Production
| writer         =
| narrator       = Shunsui Matsuda Midori Sawato
| starring       = Kanjuro Arashi Takesaburo Nakamura Reizaburo Yamamoto Tokusho Arashi
| cinematography = Juzo Tanaka
| editing        =
| distributor    = Digital Meme
| released       =  
| runtime        = 71 minutes
| country        = Japan
| language       = silent
| budget         =
| website        =
}}
  is a 1928 black and white Japanese silent film with benshi accompaniment directed by Teppei Yamaguchi. It is a film which is a part of the series depicting the bold and daring hero Kurama Tengu. The popular series comprises numerous films based on the original novel written by Jiro Osaragi, but those featuring Kanjuro Arashi are considered to be the most valuable.
Of note is the last scene in which the main character takes on numerous foes with a sword in each hand.

== See also ==
*  
* Kurama Tengu ōedo ihen, a 1950 film

==External links==
* 

 
 
 
 


 
 