Happy Together (1997 film)
 
 
{{Infobox film
|  name           = Happy Together
|  film name = {{Infobox name module
|  traditional    = 春光乍洩
|  simplified     = 春光乍泄
|  pinyin         = Chūn guāng zhà xiè
|  jyutping       = Ceon1 gwong1 za3 sit3
}}
|  image          = Happy Together poster.jpg
|  writer         = Wong Kar-wai Tony Leung Leslie Cheung Chang Chen
|  director       = Wong Kar-wai
|  producer       = Chan Ye-cheng
|  cinematography = Christopher Doyle
| editing        = William Chang   Ming Lam Wong
|  music          = Danny Chung Kino International
|  released       =  
|  runtime        = 96&nbsp;minutes
|  country        = Hong Kong Cantonese Mandarin Mandarin Spanish
|  budget         =
|  gross          = HK$8,600,141 (Hong Kong|HK) $320,319 (US) 
}} 1967 song, idiomatic expression suggesting "the exposure of something intimate."   Retrieved 7 July 2008   
 Best Director at the 1997 Cannes Film Festival.   

== Plot outline ==
 Iguazu waterfalls, which serves as a leitmotif in the movie.

The movie unfolds in the following sections:

Part 1

As the two arrive into Argentina, they pick a car. During the ride, however, they get into an argument and break up. Lai (played by Tony Leung) is the more stable and committed of the two, and desires nothing more than a fairly normal life. He tries to deal with the break-up rationally and gets a job at a local nightclub. Ho (played by Leslie Cheung) has an extremely destructive personality and is not able to commit to a monogamous relationship. Ho seems to be motivated by both a need for attention as well as a need to simply hurt Lai. Ho picks up numerous other men, and even goes so far to bring them to the club that Lai works at. Lai tries very hard to lead a normal life at this point, but is nearly driven to the edge of insanity by Ho.

Part 2

One day Ho Po-Wing turns up severely beaten at Lai Yiu-fais apartment, who takes him in and begins to take care of him. Hos hands are injured so at this point, he relies on Lai for nearly everything. Initially, Lai works hard to keep Ho at bay physically and emotionally. However, in the end, they get back together. Their actions indicate a continual pattern of abuse, break-up, finally followed by reconciliation. As in the previous times, in the beginning Ho does try to make the relationship work, and the two seem genuinely happy. However, as Ho recovers, he begins again to pick up other men and ignore Lai. We see gradually the destructive side of Hos personality taking over and the familiar cycle of mutual abuse and dependence starting again.

Part 3

As Lai and Hos relationships starts falling apart again, Lai befriends Chang, a fellow Taiwanese from Taiwan at work. In some sense, Chang is Hos opposite. Whereas Ho is manipulative and volatile, Chang is straightforward and stable. After Ho fully recovers, he resumes his playboy lifestyle and leaves Lai. Lai copes with the loss by spending more and more time with Chang. It is hinted that Chang is also gay and attracted to Lai; Chang states in a voiceover that he likes deep, low voices and is seen rejecting advances from an attractive female coworker. Changs unassuming self-awareness and sincerity help Lai out of his depression, contributing to his eventual realisation that his relationship with Ho Po-Wing is based on an ideal which no longer has any basis in reality. During one of their many conversations, Chang tells Lai that his goal is to reach the southern tip of South America where there is a lighthouse where supposedly all sorrows can be dropped. Eventually, Chang departs Buenos Aires and continues on this journey.

Part 4

After Chang leaves, Lai sinks deeper into depression. He takes some changes jobs in order make more money and eventually resorts to sexual encounters with other men in public restrooms and cinemas as a means to cope with the loneliness. He remarks in voiceovers at this point that in some sense he better understands Hos promiscuity at this point, as these encounters seem to be a way to numb the emotional pain.

On Christmas Day, he sits down and begins to write a card to his father back in Hong Kong. The card turns into a long letter. We learn that earlier Lai had stolen a large amount of money from his fathers associates business in order to finance the trip for himself and Ho to South America. Lai apologises to his father, and resolves to return to Hong Kong and deal with his past actions.

Part 5

After a few months, Ho again contacts Lai to restart the cycle of abuse and destruction. But this time, Lai has the strength to avoid starting this cycle once again. He refuses to see Ho. While on the surface, Ho is angry about Lais rejection, privately he also mourns this loss. Eventually, Lai finds the strength to visit the waterfalls and return to Hong Kong. On the way home to Hong Kong, Lai visits Taipei and seeks out Changs familys noodle shop in the night market. He steals a picture of Chang as a remembrance.

==Critical reception==
 
Due to the international recognition that the film received, it was reviewed in several major U.S. publications. Edward Guthmann, of the San Francisco Chronicle, gave the film an ecstatic review, lavishing praise on Wong for his innovative cinematography and directorial approach; whilst naming Jack Kerouac and William Burroughs amongst those who would have been impressed by his film.  Stephen Holden, of the New York Times, said it was a more coherent, heartfelt movie than Wongs previous films, without losing the stylism and brashness of his earlier efforts. 

However, Jonathan Rosenbaum gave the film a mixed review in the Chicago Reader. Rosenbaum, in a summary of the film, criticised it for having a vague plotline and chastised Wongs "lurching around".  In Box Office Magazine, Wade Major gave the film one of its most negative reviews, saying that it offered "little in the way of stylistic or narrative progress, although it should please his core fans." He deprecated Wongs cinematography, labelling it "random experimentation" and went on to say this was "unbearably tedious" due to the lack of narrative. 

Wong, in regards to the interpretation of the film said:
:"In this film, some audiences will say that the title seems to be very cynical, because it is about two persons living together, and at the end, they are just separate. But to me, happy together can apply to two persons or apply to a person and his past, and I think sometimes when a person is at peace with himself and his past, I think it is the beginning of a relationship which can be happy, and also he can be more open to more possibilities in the future with other people." 

==Box office== Category III rating. It was also typical of a Wong Kar Wai film.
 Kino International, where it grossed US $320,319.

==Awards and nominations==
* 1997 Cannes Film Festival
** Won: Best Director (Wong Kar-wai) 
** Nominated: Palme dOr 

* 1998 Arizona International Film Festival
** Won: Audience Award – Most Popular Foreign Film

* 1997 Golden Horse Awards
** Won: Best Cinematography (Christopher Doyle)
** Nominated: Best Director (Wong Kar-wai)
** Nominated: Best Actor (Leslie Cheung)

* 1997 Hong Kong Film Awards
** Won: Best Actor (Tony Leung Chiu-Wai)
** Nominated: Best Picture
** Nominated: Best Director (Wong Kar-wai)
** Nominated: Best Actor (Leslie Cheung)
** Nominated: Best Supporting Actor (Chang Chen)
** Nominated: Best Art Direction (William Chang)
** Nominated: Best Cinematography (Christopher Doyle)
** Nominated: Best Costume and Make-up Design (William Chang)
** Nominated: Best Film Editing (William Chang, Wong Ming-lam)

* 1998 Hong Kong Film Critics Society Awards
** Film of Merit

* 1998 Independent Spirit Awards
** Nominated: Best Foreign Film

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 