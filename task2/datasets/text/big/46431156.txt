Asphalte
{{Infobox film
| name           = Asphalte
| image          = 
| caption        =
| director       = Samuel Benchetrit
| producer       = Julien Madon 
| screenplay     = Samuel Benchetrit
| based on       =  
| starring       = Michael Pitt   Isabelle Huppert   Valeria Bruni Tedeschi   Gustave Kervern
| music          = 
| cinematography = Pierre Aïm 
| editing        = Thomas Fernandez
| studio         = Maje Productions   Single Man Productions
| distributor    =
| released       =  
| runtime        = 113 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Asphalte is an upcoming French drama film written and directed by Samuel Benchetrit, and based on the first volume of Benchetrits autobiography Les Chroniques de lAsphalte. The film has been selected to be screened in the  Special Screenings section at the 2015 Cannes Film Festival. 

== Cast ==
* Michael Pitt 
* Isabelle Huppert
* Valeria Bruni Tedeschi 
* Gustave Kervern 
* Jules Benchetrit 	
* Tassadit Mandi 	

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 