Bunny (2005 film)
{{Infobox film
| name           = Bunny
| image          = Bunny Telugu poster.jpg
| director       = V.V. Vinayak
| producer       = M. Satyanarayana Reddy
| writer         = V.V. Vinayak
| screenplay     =
| story          =
| based on       =  
| starring       = Allu Arjun Gowri Munjal Prakash Raj Mukesh Rishi Raghu Babu
| music          = Devi Sri Prasad
| cinematography = Chota K. Naidu
| editing        = Gowtham Raju
| studio         =
| distributor    =
| released       =  
| runtime        = 139 minutes
| country        =   Telugu
| budget         =   
| box office     =  (Share) 
}}
 Dev & Subhashree Ganguly.

==Plot==
Somaraju (Prakash Raj) is a leading business man in Vizag. Mysamma (Mukesh Rishi) is a close member of Somaraju, who handles the deals of Somaraju in Hyderabad. Mahalaxmi (Gowri Munjal) is the daughter of Somaraju. Somaraju dearly loves his daughter. Bunny (Allu Arjun) joins the same college as Mahalaxmi. He impresses Mahalaxmi on the first day itself. Slowly, she falls in love with him. Somaraju, though reluctant initially, agrees to the marriage. Now Bunny has a condition that Somaraju should give his entire property to Bunny as dowry. The rest of the story explains why Bunny asks for Somarajus property.

Bunnys real father is Mahalaxmis moms brother. He was a great man with lots of land and property, as well as lots of regard from the government and other people. When Mahalaxmi is born, Mahalxmis mom asks him to perform the baby naming ceremony and such. On his way there, Somaraju attacks him with the help of a bunch of rowdies. They pretty much kill him and leave him to die in the forest. However, he manages to live and perform Mahalaxmis ceremony, but he dies minutes after, without telling anyone what happened.

Bunny comes to know of this when his so-called father explains to him that he is really a godfather and not a father. In trying to save him, his godfather and godmother give up their own son to keep him alive. To regain his rightful property, Bunny fights Somaraju and he wins his girl, and his heritage.

== Cast ==
* Allu Arjun ... Raja/Bunny
* Gowri Munjal ... Mahalakshmi
* Prakash Raj ... Somaraju
* Sarath Kumar ... Bhupati Raja
* Mukesh Rishi ... Maisamma
* Raghu Babu ... Somarajus henchman
* Sharat Saxena ... Bunnys guardian Seetha ... Bunnys aunt
* Sudha
* Ahuti Prasad
* Rajan P. Dev
* Chalapathi Rao
* M.S. Narayana ... Daiva Sahayam/Daiva Sahayams father (Dual role) Venu Madhav ... peon
* Fish Venkat ... Somarajus henchman
* Dil Ramesh ... Somarajus henchman
* Chitram Srinu ... Bunnys friend
* Sravan ... Tarun Jenny ... lecturer
* L.B. Sriram ... doctor

== Crew ==
* Director: V.V. Vinayak
* Screenplay: V.V. Vinayak
* Story: V.V. Vinayak
* Producer: M. Satyanarayana Reddy
* Music: Devi Sri Prasad
* Cinematography: Chota K. Naidu
* Editing: Gowtham Raju Rajendra Kumar Ashok Kumar

==Soundtrack==
{{Infobox album
| Name = Bunny
| Longtype = to Bunny
| Type = Soundtrack
| Artist = Devi Sri Prasad
| Cover = Bunny 2005 ACD.jpg
| Border = yes
| Alt =
| Caption =
| Released =
| Recorded = 2005 Feature film soundtrack
| Length = 27:07 Telugu
| Label = Aditya Music
| Producer = Devi Sri Prasad
| Reviews =
| Last album = Maayavi (2005)
| This album = Bunny (2005)
| Next album = Sachein (2005)
}}
All Music Scored by Devi Sri Prasad & Lyrics were Provided by Chandrabose (lyricist)|Chandrabose, Suddala Ashok Teja & Vishwa. The audio was well received by the public. 
{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 27:07
| lyrics_credits = yes
| title1 = Maro Maro Chandrabose
| Tippu
| length1 = 5:08
| title2 = Jabilammavo
| lyrics2 = Chandrabose
| extra2 = Sagar & Malathi
| length2 = 4:34
| title3 = Va Va Vareva
| lyrics3 = Vishwa Karthik & Sumangali
| length3 = 4:08
| title4 = Mayilu Mayilu
| lyrics4 = Suddala Ashok Teja
| extra4 = Devi Sri Prasad
| length4 = 4:57
| title5 = Kana Padaleda
| lyrics5 = Suddala Ashok Teja
| extra5 = S. P. Balasubrahmanyam
| length5 = 3:33
| title6 = Bunny Bunny
| lyrics6 = Chandrabose
| extra6 = Murali & Srilekha Parthasarathy
| length6 = 4:47
}}

== Box office ==
Bunny was made with a budget of   
Bunny(2005) collected  (Share) in 100 Days. 
* The film is hit in Andhra Pradesh
* The Malayalam dubbed version became a blockbuster in Kerala and made huge fan following for Allu Arjun in Kerala.

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 