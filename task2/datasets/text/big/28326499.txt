The Great Barrier (film)
{{Infobox film
| name =  The Great Barrier 
| image =
| image_size =
| caption =
| director =  Milton Rosmer   Geoffrey Barkas
| producer = Günther Stapenhorst    
| writer =  Alan Sullivan  (novel)   Michael Barringer    Ralph Spence   Emeric Pressburger    Milton Rosmer   
| narrator = Barry MacKay   Lilli Palmer 
| music = Hubert Bath   Jack Beaver   Louis Levy Robert Martin   Arthur Crabtree
| editing = Charles Frend   Ben Hipkins Gaumont British  
| distributor = Gaumont British Distributors
| released = February 1937
| runtime = 83 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama The Great Divide by Alan Sullivan. It was made at the Lime Grove Studios in Shepherds Bush.  The films sets were designed by Walter Murton.

==Cast==
* Richard Arlen - Hickey 
* Lilli Palmer - Lou 
* Antoinette Cellier - Mary Moody  Barry MacKay - Steve 
* Roy Emerton - Moody 
* J. Farrell MacDonald - Major Rogers
* Ben Welden - Joe 
* Jock MacKay - Bates 
* Ernest Sefton - Magistrate 
* Henry Victor - Bulldog Kelly 
* Reginald Barlow - James Hill 
* Arthur Loft - William Van Horne 
* Frank McGlynn Sr. - Sir John MacDonald

==References==
 

==Bibliography==
* Cook, Pam. Gainsborough Pictures. Cassell, 1997.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 