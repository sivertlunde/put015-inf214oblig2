Legendary Amazons
{{Infobox film
| name = Legendary Amazons
| film name = {{Film name| traditional = 楊門女將之軍令如山
| simplified = 杨门女将之军令如山
| pinyin = Yáng Mén Nǚ Jiàng Zhī Jūn Lìng Rú Shān
| jyutping = Joeng 4  Mun 4  Neoi 5  Zoeng 3  Zi 1  Gwan 1  Ling 6  Jyu 4  Saan 1 }}
| image = Legendary Amazons 2011 film.jpg
| alt =  
| caption = Promotional poster
| director = Frankie Chan
| producer = Jackie Chan Wang Tianyun
| writer = Frankie Chan Liu Heng Ma Honglu
| starring = Richie Jen Cecilia Cheung Cheng Pei-pei Liu Xiaoqing Kathy Chow
| music = Roc Chen
| cinematography = Wu Rongjie Chen Youliang Zhang Wenjie
| editing = 
| studio = Shanghai Film Group Corporation Feng Huang Motion Picture Co. 中视文化有限公司 Beijing Century Culture Communication Co., Ltd. 乐道文化投资有限公司 同方联合影业集团有限公司
| distributor = 上海东方影视发行公司 中国电影股份有限公司北京电影发行分公司 尚视影业
| released =  
| runtime = 108 minutes
| country = China
| language = Mandarin
| budget = 
| gross = 
}} stories of the Yang Clan Generals. The film was directed by Frankie Chan  and starred Richie Jen, Cecilia Cheung, Cheng Pei-pei, Liu Xiaoqing and Kathy Chow. The film is based on the same source material as the 1972 Hong Kong film The 14 Amazons.

==Plot== Emperor Renzong of the Song dynasty. The emperor neglects state affairs and indulges in personal pleasures, while the government sinks into corruption and war continues to rage on at the borders of the Song dynasty. The Song dynasty is being invaded by the armies of the rival state of Western Xia.
 Yang clan, a family of generals who have dedicated their lives to defending the Song dynasty from foreign invaders. He apparently dies in battle tragically when the treacherous Imperial Tutor Pan refuses to send reinforcements to aid him. Yang Zongbaos widowed wife, Mu Guiying, leads the other widows of the Yang clan into battle to continue the legacy of their husbands.

==Cast==
* Richie Jen as Yang Zongbao
* Cecilia Cheung as Mu Guiying
* Cheng Pei-pei as She Saihua
* Liu Xiaoqing as Chai Qingyun
* Ge Chunyan as Zhou Yunjing
* Oshima Yukari as Zou Lanxiu
* Li Jing as Geng Jinhua
* Jin Qiaoqiao as Dong Yuee
* Yang Zitong as Meng Jinbang
* Kathy Chow as Ma Saiying
* Yu Na as Huyan Chijin
* Chen Zihan as Yang Yanqi
* Liu Dong as Yang Yanying
* Xiao Mingyu as Yang Wenguang
* Zhou Xiaofei as Yang Paifeng
* Wang Ti as Yang Jinhua
* Zhao Qianyu as Little Bean
* Tang Yaolin as Little Sparrow
* Shi Fanxi as Yin Qi Pang Ji
* Lin Wei as Wang Qiang
* Zhong Chao as Liu Fu
* Li Boyu as Jiao Tinggui
* Luo Yingyuan as Meng Huaiyuan
* Han Yu as Meng Liang
* Hu Biao as Jiao Zan
* Ren Xuehai as Fan Zhongyan
* Xiao Rongsheng as Yang Yanzhao
* Xu Xiao as Di Qing
* Feng Kean as Little Beans grandfather

==See also==
* Jackie Chan filmography

==References==
 

==External links==
*  
*  
*     on Baidu Baike

 

 
 
 
 
 
 
 
 
 
 
 
 