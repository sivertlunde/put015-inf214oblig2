Adagio (film)
 
{{Infobox film
| name           = Adagio
| image          = 
| caption        = 
| director       = Garry Bardin
| producer       = Garry Bardin
| writer         = Garry Bardin
| starring       = 
| music          = Adagio Albinoni Tomaso Albinoni
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 10 minutes
| country        = Russia
| language       = 
| budget         = 
| gross          = 
}}
Adagio is a 2000 Russian short film.

== Plot ==
The story of the tragedy, an innovator - the leader, the prophet, the consequences of joining the world of new ideas and about how distorted the followers of the search of eternal truths can be. Throughout the cartoon sounds the Adagio in G minor (commonly attributed to the composer Tomaso Albinoni).

== Awards ==
* 2000, the - Grand Prix, the prize "Vyborg Account
* 2000 , the - Prize of the Russian-Evropeyskogoy Motion Picture Association at the VIII Russian film festival "Window to Europe"
* 2000, the - Grand Prix at the International Human Rights Festival " Stalker "
* 2000, the - Special Jury Prize International Film Festival in Marita
* 2001, the - Gold Prize at the International Film Festival in New York
* 2001, the - Prize of the Russian Academy "Nika"
* 2001, the - Grand Prix International Film Festival in Rouen
* 2001, the - Jury Prize International Film Festival in Tehran
* 2001, the - Prize of the International Film Festival in Kaiare

== References ==
 

== External links==
*  
*  

 
 
 


 
 