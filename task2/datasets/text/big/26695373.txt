Bhatti Vikramarka
{{Infobox film
| name           = Bhatti Vikramarka
| image          = Bhatti Vikramarka.JPG
| image_size     =
| caption        =
| director       = Jampana Chandrasekhara Rao
| producer       = P. V. V. Satyanarayana Murthy
| writer         = Anisetty
| narrator       =
| starring       = N.T. Rama Rao Kanta Rao Anjali Devi S. V. Ranga Rao
| music          = Pendyala Nageshwara Rao
| cinematography = Adi M. Irani
| editing        = N. M. Shankar
| studio         =
| distributor    =
| released       = 1960
| runtime        =
| country        = India Telugu
| budget         =
}}

Bhatti Vikramarka (Telugu language|Telugu: భట్టి విక్రమార్క) is a 1960 Telugu film directed by Jampana Chandrasekhara Rao and produced by Polisetty Veera Venkata Satyanarayana Murthy. It is a commercial hit film ran for 100-days.

==The plot==
The story is based on the historical characters of Bhatti and Vikramāditya|Vikramarka. It also shows some of the Bethala Kathalu (Stories of Vetala).
 Rambha and Urvasi. Vikramarka gives two garlands to them to wear and dance. The garland worn by Urvasi stays fresh, whereas that one worn by Rambha withers. He judges Urvasi as the best dancer, as she is fearless about her performance and win. As a token of gratitude, Indra gifts him a Simhasanam with 32 Salabhanjikas.

Prachandudu performs 99 yagnas and in search of 100th yagna to achieve some magical powers. Mantrikudu tells him to invite Vikramarka for the sacrifice. He agrees and reaches the Smasanavatika, where he told him to get bhetala, who is hanging from a tree. He cuts the rope and gets bhetala on his back. While returning, he narrates a tricky social problem and asks Vikramarka to answer. After successfully answering all the stories, bhetala instructs him about the ill intention of Prachanda. Prachanda on his return asks Vikramarka to perform Sashtang Pranam. The cautious Vikramarka tells him as a king he does not know how to do it and requests him show how to do it. He kills Prachanda while he is on Pranam and gets the magical powers.

==Cast==
* Nandamuri Taraka Rama Rao	 as 	Vikramarka Maharaju, King of Ujjain
* Kanta Rao	as 	Bhatti, Minister of Ujjain
* Anjali Devi as Prabhavathi Devi, daughter of King Chandrasena
* S. V. Ranga Rao  as  Mantrikudu
* Relangi Venkataramaiah as Tirakasu, Shilpacharya
* Balakrishna as Assistant to Mantrikudu
* Nagabhushanam as Prachandudu
* Mukkamala
* Kuchala Kumari Girija as Arakasu, wife of Tirakasu

==Features==
There was Fire Accident on the sets of Narasu Studios, Guindy, Madras on March 13, 1959. The Bhatti Vikramarka shooting required a fire, and a fire was made by using petrol and straw. Mukkamala and Anjali Devi were on the sets. The bamboo setting was ablaze and the flames rose high fast. The fire spread to the adjacent sets too and caused a huge damage of a lakh and half rupees. It took them for more than 90 minutes for the fire brigades to bring the fire under control. This was the worst fire disaster in the history of Telugu cinema. 

==Songs and Poems==
* Jaire Jambhaire Okasari Ravemi Sundari
* Kannepilla Sogasu Choodu Maharaja Vanneladi  Nagavu Naadi
* Kommulu Thirigina Magavaru Kongu Thagilite Poleru
* Manasaara Preminchinaara
* Natinchana Jagalane Jayinchana (Dance song of Rambha and Urvasi)
* Ninu Nammi (Slokam)
* O Nelaraja Vennela Raja Nee Vannelanni Chinnelanni Makeloyi (Singers: Ghantasala and P. Susheela)
* O Saila Sutha Mataa Pati Padaseva Niratamu Neeva
* O Sundari Andame Vindura Pondara
* Satyamayaa Guruda Nityamayaa
* Suklam Baradharam Vishnum (Slokam)
* Vinta Aina Vidhi Vilasam Idena (Singer: Ghantasala)

==References==
 

==External links==
*  
*  

 
 
 
 