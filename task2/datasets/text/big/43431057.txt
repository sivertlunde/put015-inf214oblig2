Tanu Weds Manu Returns
{{Infobox film
| name           = Tanu Weds Manu Returns
| image          = Tanu Weds Manu Returns First Look.jpg
| caption        = Theatrical Release Poster
| director       = Anand L. Rai
| producer       = Kishore Lulla Anand L. Rai
| writer         = Himanshu Sharma
| starring       = Kangana Ranaut  R. Madhavan
| music          = Tanishk-Vayu   Krsna Solo
| cinematography = Chirantan Das	
| editing        = Hemal Kothari
| studio         = Colour Yellow Pictures
| distributor    = Eros International
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          =
}} Hindi romantic comedy film directed by Anand L. Rai. The film is a sequel to Tanu Weds Manu (2011), in which stars Kangana Ranaut and  R. Madhavan reprise their roles from the original. Ranaut also portrays the additional role of a Haryanvi athlete in the film.  Tanu Weds Manu Returns trailer released on 14 April 2015.
.   Tanu Weds Manu Returns is scheduled for release on May 22, 2015.

==Cast==
* Kangana Ranaut as Kusum "Datto" Sangwan/Tanuja "Tanu" Trivedi 
* R. Madhavan as "Manu" Sharma 
* Jimmy Shergill as Raja Awasthi
* Deepak Dobriyal as Pappi
* Eijaz Khan as Jassi
* Swara Bhaskar as Payal 
* Mohammed Zeeshan Ayyub as Chintu
* Rajendra Gupta as Tanus Father
* Navni Parihar as Tanus Mother
* K.K.Raina as Manus Father
* Dipti Mishra as Manus Mother Rajesh Sharma as Dattos Brother

==Production==
Following the positive response to   may replace Madhavan in the lead role.   In January 2012, the project was postponed to allow Anand Rai to proceed with another venture Raanjhanaa (2013), while the makers also wanted Ranaut to finish her commitments with Krrish 3 (2013) as the sequel demanded a different look.  
 Imran Khan and Anushka Sharma were to replace them.   Screenwriter Himanshu Sharma began working on the film in July 2013 and revealed that only the first ten minutes would have a London backdrop, before the story returns to Lucknow, Kanpur and Delhi. He went on to confirm the working title, though added it potentially may also be known as Tanu Weds Manu Extra Large.  Pre-production continued through late 2013, with Ranauts two month trip to the USA delaying the start of the shoot in early 2014.  She later confirmed that she would play a dual role in the film, that of an athlete apart from Tanu, and the team scouted for a body double to be used in scenes featuring both characters.  Dhanush, who had worked in Raanjhanaa, was revealed to be playing a cameo appearance in the film. 

The team began filming in October 2014 in Lucknow, after a small launch ceremony. The first motion poster of the film was released on 23 March 2015 on the birthday of the leading actress, Kangana Ranaut at a special event in Delhi. 

==Controversy==
In May 2014, the producers of the original film, Sanjay Singh, Vinod Bachchan and Shailesh Singh filed a legal notice stating that the director did not have the rights to make a sequel without their permission. Anand Rai replied stating that the trio only had rights over the first film and no direct rights over sequels or the franchise. In a further reply, Bachchan revealed that they were interested in making a sequel and should have been approached ahead of Eros International.   

== Soundtrack ==

{{Infobox album
| Name = Tanu Weds Manu Returns
| Type = Soundtrack
| Artist = Tanishk-Vayu & Krsna Solo
| Cover = 
| Released =  
| Length =   Feature Film Soundtrack Eros Music
| Last Album = }}
The tracks of Tanu Weds Manu Returns are be composed by  Tanishk-Vayu & Krsna Solo.
 

{{track listing
| extra_column = Singer(s)
| total_length =  
| title1 = Banno
| extra1 = Brijesh Shandllya, Swati Sharma
| length1 = 03:16
| title2 = Move On
| extra2 = Sunidhi Chauhan
| length2 = 04:10
| title3 = Mat Ja Re
| extra3 = Ankit Tiwari
| length3 = 03:52
| title4 = Ghani Bawri
| extra4 = Jyoti Nooran
| length4 = 04:14
| title5 = Old School Girl
| extra5 = Anmol Malik
| length5 = 03:39
| title6 = Old School Girl (Haryanvi)
| extra6 = Kalpana Gandharv
| length6 = 04:08
| title7 = Ho Gaya Hai Pyar
| extra7 = Dev Negi
| length7 = 03:48
| title8 = O Sathi Mere
| extra8 = Sonu Nigam
| length8 = 05:46
| title9= Ghani Bawri (Remix)
| extra9 = Jyoti Nooran
| length9 = 03:48
}}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 