Shanghaied Love
 
{{Infobox film
| name           = Shanghaied Love
| image          = 
| caption        = 
| director       = George B. Seitz
| producer       = 
| writer         = Norman Springer Richard Cromwell
| music          = 
| cinematography = Ted Tetzlaff
| editing        = Gene Milford
| distributor    = Columbia Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
}}

Shanghaied Love is a 1931 American drama film directed by George B. Seitz, produced and released by Columbia Pictures.   

==Cast== Richard Cromwell as The Boy
* Sally Blane as Mary Swope
* Noah Beery as Capt. Black Yankee Angus Swope
* Willard Robertson as Newman
* Sidney Bracey as The Rat Richard Alexander as Eric Ed Brady as Fitzgibbons
* Erville Alderson as Deaken
* Jack Cheatham as Lynch
* Fred Toones as Snowflake
* Lionel Belmore as The Knitting Swede

==Preservation status==
This film is now considered a lost film. 

==See also==
*List of lost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 