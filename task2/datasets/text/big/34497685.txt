Time Freak
{{Infobox film
| name           = Time Freak
| director       = Andrew Bowler
| producer       = Gigi Causey Luke Geissbuhler Michael McDermott Geoffrey Richman Leo Won
| writer         = Andrew Bowler
| starring       = Michael Nathanson John Conor Brooke Emilea Wilson Hector Diaz
| music          = Irv Johnson
| cinematography = Luke Geissbuhler
| editing        = Geoffrey Richman
| released       =  
| runtime        = 10 minutes
| country        = United States English
}} 2012 Academy Award for Best Live Action Short Film.  The time-travel comedy was inspired by other time-travel movies like Primer (film)|Primer and Back to the Future. 

Bowler and Causey decided to produce the film after they got married, spending the $25,000 they had saved to buy an apartment in New York. The film was rejected by several film festivals, including  Sundance, Telluride, and Tribeca, but the couple submitted it to the Academy of Motion Picture Arts and Sciences, which selected the film as a nominee for the award. 

The film stars John Conor Brooke, Michael Nathanson and Emilea Wilson. Brooke and Nathanson are roommates, but Nathanson hasnt been home for three days, so Brooke goes to Nathansons lab in a run down building to check on him. Nathanson has just perfected the time machine he had been working on, but is behaving oddly. It turns out he has been re-doing the events of the day before, trying to perfect his interactions at a dry cleaner and with a woman (Wilson) that he wants to impress.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 