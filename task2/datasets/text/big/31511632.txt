Moner Jala
{{Infobox film
| name           = Moner Jala
| image          = moner jala.jpg
| alt            =  
| caption        = 
| director       = Malak Shafsari
| producer       = Hertbeat Production
| writer         = Kasam Ali Dolal
| screenplay     = 
| story          = 
| starring       = Shakib Khan Apu Biswas
| music          = Ali Akram Shuvo
| cinematography = 
| editing        = 
| studio         = BFDC
| distributor    = 
| released       =  
| runtime        = 144 Min.
| country        = Bangladesh
| language       = Bengali
| budget         = 
| gross          = 
}}
Moner Jala  ( ) is a Dhallywood romantic action film. Directed by Malek Afsari. The film stars Shakib Khan and Apu Biswas in the lead role. This is Shakib Khans first film of 2011. as an international title Moner Jala - Most Wanted.  Upon release, the film received massive response and is said to contribute to record breaking sales in some renowned theaters.

==Cast==
* Shakib Khan
* Apu Biswas
* misha soudagar

==Crew==
* Director: Malak Shafsari 
* Producer: Hertbeat Production
* Music:Ali Akram Shuvo 
* Story: Kasam Ali Dolal  
* Script: Kasam Ali Dolal  
* Lyrics: Kabir Bokul
* Distributor: Hertbeat Production

==Technical details==
* Format: 35 MM (Color)
* Running Time: 144 Minutes
* Real: 13 Pans Bengali
* Country of Origin: Bangladesh
* Date of Theatrical Release: 2011
* Year of the Product: 2010
* Technical Support: Bangladesh Film Development Corporation (BFDC)

==Soundtrack==

===Track listing===
{{Track listing
| extra_column = Artist(s)
| title1 = Ami Chokh Tule Takalei Surjo Lukay
| extra1 = Shakib Khan & Reshad
| title2 = Akashe Uthechhe Chad
| extra2 = Kumar Bishwajit 
| title3 = Din Duniyar Malik Khoda (Tomar Dile Ki Doya Hoy Na)
| extra3 = SI Tutul
| title4 = O Premi
| extra4 = S I Tutul & Dolly
}}

==References==
 

 
 
 
 
 
 
 
 


 
 
 