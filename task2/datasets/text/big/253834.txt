The Mummy (1959 film)
 
 
 
{{Infobox film
| name           = The Mummy
| image          = Themummy1959poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Terence Fisher
| producer       = Michael Carreras Anthony Nelson Keys
| writer         = Jimmy Sangster
| starring       = Peter Cushing Christopher Lee Yvonne Furneaux Eddie Byrne George Pastell
| music          = Franz Reizenstein
| cinematography = Jack Asher
| editing        = Alfred Cox James Needs
| studio         = Hammer Film Productions
| distributor    = Universal-International
| released       = 25 September 1959  (UK) 
| runtime        = 88 minutes
| country        = England
| language       = English
| budget = £125,000 Marcus Hearn & Alan Barnes, The Hammer Story: The Authorised History of Hammer Films, Titan Books, 2007 p 43  gross = 857,243 admissions (France) 
}}
 horror film, directed by Terence Fisher and starring Christopher Lee and Peter Cushing. It was written by Jimmy Sangster and produced by Michael Carreras and Anthony Nelson Keys for Hammer Film Productions.
 Universal Pictures 1932 film of the same name, the film actually derives its plot and characters entirely from two later Universal films, The Mummys Hand and The Mummys Tomb, with the climax borrowed directly from The Mummys Ghost. The character name Joseph Whemple is the only connection with the 1932 version.

== Plot ==
 Scroll of Life and reads from it. He then screams off-screen and is found in a catatonic state.

Three years later, back in England, Stephen Banning comes out of his catatonia at the Engerfield Nursing Home for the Mentally Disordered, and sends for his son. He tells him that when he read from the Scroll of Life, he unintentionally brought back to life Kharis (Lee), the mummified high priest of Karnak. He was sentenced to be entombed alive to serve as the guardian of Princess Anankas tomb as punishment for attempting to bring her back to life out of forbidden love. Now, Stephen tells his disbelieving son that Kharis will hunt down and kill all those who desecrated Anankas tomb.

Meanwhile, Mehemet Bey, revealed as a devoted worshiper of Karnak, comes to Engerfield under the alias of Mehemet Akir to wreak vengeance on the Bannings. He hires a pair of drunken carters, Pat and Mike, to bring the slumbering Kharis in a crate to his rented home, but the two mens drunken driving cause Kharis crate to fall off and sink into a bog. Later, using the Scroll of Life, Mehemet exhorts Kharis to rise from the mud, then sends him to murder Stephen Banning. When Kharis kills Joseph Whemple the next night, he does so before the eyes of John Banning, who shoots him with a revolver at close range, but to no effect.

Police Inspector Mulrooney is assigned to solve the murders but, because he is sceptical and deals only in "cold, hard facts", he does not believe Johns incredible story about a killer mummy, even when John tells him that he is likely to be Kharis third victim. While Mulrooney investigates, John notices that his wife Isobel bears an uncanny resemblance to Princess Ananka. Gathering testimonial evidence from other individuals in the community, Mulrooney slowly begins to wonder if the mummy is real.

Mehemet Bey sends the mummy to the Bannings home to slay his final victim. However, when Isobel rushes to her husbands aid, Kharis sees her, releases John, and leaves. Mehemet Bey mistakenly believes that Kharis has completed his task, and prepares to return to Egypt. John, suspecting Mehemet Bay of being the one controlling the mummy, pays him a visit, much to his surprise.

After John leaves, Mehemet Bey leads Kharis in a second attempt on Johns life. The mummy knocks Mulrooney unconscious, while Mehemet Bey deals with another policeman guarding the house. Kharis finds John in his study and starts to choke him. Alerted by Johns shouts, Isobel runs to the house without Mulrooney; at first, the mummy does not recognise her, but John tells her to loose her hair and the mummy releases John. When Mehemet orders Kharis to kill Isobel, he refuses; Mehemet tries to murder Isobel himself and is killed instead by Kharis. The mummy carries the unconscious Isobel into the swamp, followed by John, Mulrooney and other policemen. John yells to Isobel; when she regains consciousness, she tells Kharis to put her down. The mummy reluctantly obeys. When Isobel has moved away from him, the policemen open fire, causing Kharis to sink into a mire, taking the Scroll of Life with him.

== Cast ==
* Peter Cushing as John Banning
* Christopher Lee as Kharis
* Yvonne Furneaux as Isobel Banning / Princess Ananka
* Eddie Byrne as Inspector Mulrooney
* Felix Aylmer as Stephen Banning
* Raymond Huntley as Joseph Whemple
* George Pastell as Mehemet Bey George Woodbridge as P. C. Blake Harold Goodwin as Pat
* Denis Shaw as Mike
* Willoughby Gray as Dr. Reilly
* Michael Ripper as Poacher
* Frank Singuineau as Head Porter

== Critical reception ==
 
The Mummy was a success with critics. The Hammer Story: The Authorised History of Hammer Films wrote of the film: "Structurally little more than a string of picturesque and nice-lit killings, The Mummy  s melancholic presentation and romantic undertow grants it a certain atmosphere which elevates this bandaged brute far beyond its cinematic predecessors."  The film itself holds a 100% approval rating on Rotten Tomatoes.

== In other media ==

The film was adapted into a 12-page comic strip for the July 1978 issue of the magazine Hammers Halls of Horror.  It was drawn by David Jackson from a script by Steve Moore. The cover of the issue featured a painting by Brian Lewis of Christopher Lee as Kharis.

== References ==

 

; Sources

*  

==External links==
* 
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 