Tosun Paşa
 
{{Infobox film name           = Tosun Paşa image          = Tosun Paşa.jpg image_size     =  caption        =  director       = Kartal Tibet producer       =    writer         = Yavuz Turgul narrator       =  starring       =    music          =  cinematography = Kriton İlyadis editing        =  distributor    =  released       =   runtime        =  country        =   Turkey language  Turkish
|budget         =  gross          =  preceded_by    =  followed_by    = 
}}
 1976 Turkey|Turkish Egypt in an attempt to settle a dispute between two noble families over a lucrative parcel of land called The Green Valley. The film, which went on nationwide general release across Turkey on  , is one of the most popular productions from Ertem Eğilmezs Arzu Film and is particularly remembered for its catchy musical score.

== Plot == Tosun Paşa Ibrahim Paşa to find out why someone has usurped his name. When all is discovered at the wedding, there is a large and iconic fight between the rival families, guests, and locals.

== Selected cast ==
* Kemal Sunal as Şaban (pretend Tosun Pasha)
* Müjde Ar as Leyla
* Şener Şen as Lütfü Tellioğlu
* Adile Naşit as Adile Tellioğlu
* Ayşen Gruda as Zekiye Tellioğlu
* Ergin Orbey as Vehbi Tellioğlu

==External links==
*  

 
 
 
 
 
 
 
 