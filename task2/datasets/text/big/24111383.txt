Beyond Ipanema
{{multiple issues|
 
 
}}
 

{{Infobox film
| name           = Beyond Ipanema
| image          = 
| alt            =  
| caption        = 
| director       = Guto Barra
| producer       = Guto Barra Béco Dranoff
| writer         = 
| screenplay     = 
| story          = 
| starring       = 
| music          = 
| cinematography = Artur Ratton Kummer
| editing        = Guto Barra
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Brazilian music outside of the country, especially the USA. Produced by filmmakers Guto Barra and Béco Dranoff, it features interviews and performances by David Byrne, Devendra Banhart, M.I.A., Os Mutantes, Caetano Veloso, Gilberto Gil, Tom Zé, Seu Jorge, Thievery Corporation, Bebel Gilberto, CSS, Creed Taylor and many others.
 HotDocs (the largest documentary film festival in North America), South By Southwest (one of the largest music festivals in the United States) and the Chicago International Film Festival (one of North Americas oldest film events). It has also screened in venues such as the Institute of Contemporary Art, in Boston,  Yerba Buena Center for the Arts, in San Francisco,  The High Art Museum, in Atlanta,  and Haus der Kulturen der Welt, in Berlin. 

Beyond Ipanema was awarded Best Documentary and Best Sound Editing at the 14th Brazilian Film Festival of Miami  and Best Film at the 3rd Brazilian Film Festival of Vancouver.  According to The Hollywood Reporter, "Beyond Ipanema is a Vibrant and stylish look at decades of Brazilian music focuses on the reception it has received in the U.S."  To The Huffington Post, "Beyond Ipanema is a brilliant overview of the incursion of Brazilian music world wide by directors Guto Barra and Béco Dranoff." 
  

==References==
 

==External links==
* 

 
 
 
 


 