Captain from Castile
{{Infobox film
| name           = Captain from Castile
| image	         = Captain from Castile FilmPoster.jpeg
| image_size     = 
| alt            =
| caption        = Theatrical release poster Henry King
| producer       = Lamar Trotti Darryl F. Zanuck (executive producer)
| writer         = Lamar Trotti Captain from Castile by Samuel Shellabarger
| narrator       = 
| starring       = Tyrone Power Jean Peters Cesar Romero Alfred Newman
| cinematography = Charles G. Clarke Arthur E. Arling Joseph LaShelle (uncredited)
| editing        = Barbara McLean
| distributor    = Twentieth Century-Fox
| released       = December 25, 1947
| runtime        = 141 minutes
| country        = United States
| language       = English
| budget         = $4.5 million
| gross          = 
}} Henry King, Mohawk actor Jay Silverheels, who later portrayed Tonto on the television series The Lone Ranger.
 Captain From Castile by Samuel Shellabarger. The films story covers the first half of the historical epic, describing the protagonists persecution at the hands of the Spanish Inquisition and his escape to the New World to join Hernán Cortés in an expedition to conquer Mexico. 

In his introduction to the 2002 re-issue of the novel, Pulitzer Prize-winning critic Jonathan Yardley described the merits of the film as: 

 "a faithful adaptation that had all the necessary ingredients: an all-star cast, breathtaking settings and photography, a stirring score, and enough swashbuckling action to keep the Three Musketeers busy for years." Jonathan Yardley, "Introduction", page 1. Shellabarger, Samuel (1945, 2002). Captain from Castile, First Bridge Works Publishing. ISBN 1-882593-62-6.  

==Plot== Castilian Knight|caballero, John Sutton). Santa Hermandad, charged with enforcing the Inquisition, and Pedros rival for the affections of the beautiful Lady Luisa de Carvajal (Barbara Lawrence). Later, Pedro rescues barmaid Catana Pérez (Jean Peters) from de Silvas men. At the inn where Catana works, Pedro becomes acquainted with Juan García (Lee J. Cobb), an adventurer just returned from the New World to see his mother.
 
Suspecting Pedro of aiding Coatl, and aware that Pedros influential father Don Francisco de Vargas (Antonio Moreno) opposes the abuses of the Santa Hermandad, de Silva imprisons Pedro and his family on the charge of heresy. Pedros young sister dies under torture. Meanwhile, Juan becomes a prison guard to help his mother, also a prisoner. He kills her to spare her further torture. Juan frees Pedros hands and gives him a sword.

When de Silva enters Pedros cell, Pedro disarms him in a sword fight, then forces him to renounce God before stabbing him. The trio flee with Pedros parents. Forced by their pursuers to split up, instead of going to Italy to be reunited with his family, Pedro is persuaded by Juan and Catana to journey to Cuba to seek his fortune. 
 soul of de Silva, neither aware that de Silva survived.
 Villa Rica in Mexico.  Cortéz is greeted by emissaries of Emperor Moctezuma II|Montezuma, along with a bribe to leave Mexico. Against the opposition of one of his captains, Cortéz persuades his men to join him in his plan for conquest and riches. 

Catana seeks the aid of charlatan and doctor Botello (Alan Mowbray). Botello tries to dissuade her, but in the end gives her a ring, supposedly with the power to make Pedro fall in love with her, despite their vast difference in social status. When Pedro kisses her, she rejects him, believing he is under the rings spell, but he convinces her otherwise and marries her that very night. 
 bolt during their escape.

Cortéz promotes Pedro to captain. Then, to remove the temptation of retreat, he orders their ships burned. They march on to Cholula (Mesoamerican site)|Cholula, where they are met by another delegation, led by Montezumas nephew, who threatens the expedition with annihilation unless they leave. When Cortez protests that he has no ships, the prince reveals that more have arrived. Cortéz realizes that his rival, Cuban Governor Velázquez, has sent a force to usurp his command. Cortez takes half his men to attack Villa Rica, leaving Pedro in command of the rest. 
 strangled that Aztec island capital.

==Cast==
*Tyrone Power as Pedro de Vargas 
*Jean Peters as Catana Pérez 
*Cesar Romero as Hernán Cortéz  
*Lee J. Cobb as Juan García  John Sutton as Diego de Silva 
*Antonio Moreno as Don Francisco de Vargas 
*Thomas Gomez as Father Bartolomé de Olmedo
*Alan Mowbray as Professor Botello 
*Barbara Lawrence as Luisa de Carvajal 
*George Zucco as Marquis de Carvajal 
*Roy Roberts as Capt. Pedro de Alvarado 
*Marc Lawrence as Corio  Robert Shaw as Spanish army officer (uncredited) 
*Jay Silverheels as Coatl (uncredited)

===Casting notes=== Marine Corps Forever Amber with the inexperienced Peggy Cummins in the title role, investing $1 million in the project before realizing it had become a disaster. Darnell was reassigned to save the project by replacing Cummins, and the role of Catana went to the then unknown Jean Peters. 

Other actors recommended by Mankiewicz but not cast were Fredric March as Cortéz, José Ferrer as Coatl, and Alan Reed or William Bendix to play Juan García. 

The film made extensive use of Mexican inhabitants as extras. More than 19,500 took part in various scenes, with approximately 4,500 used in the final sequence filmed in front of Parícutins smoking cinder cone. 

==Production==

===Screenplay===
The screenplay was adapted from Shellabargers novel, as yet unpublished but serialized in Cosmopolitan (magazine)|Cosmopolitan,   Cosmopolitan in 1945 featured fiction as its format.  after Fox chief Zanuck purchased the rights in December 1944 for $100,000. In February 1945, studio contract writer John Tucker Battle produced an outline, then completed a first draft script with Samuel Engel in May. Zanuck consulted Joseph L. Mankiewicz about concepts for the film. Mankiewicz wrote back to Zanuck in July that the historical background of Cortéz conquest of Mexico had to be both accurate and unoffending to many groups of people. Mankiewicz also warned that the story would be tremendously expensive to film: "To do this picture ambitiously will cost a great deal of money. It will require Technicolor, a huge cast, great numbers of people, elaborate sets, costumes, props, locations etc. The script will take a long time to write—thorough research will be necessary."   See "Notes", which quotes production and legal memos associated with film.  
 Dominican Friar|fray/Inquisitor The Holy Office) to the Santa Hermandad, eliminating the auto de fe prominent in the book, and making the lay character of de Silva the chief Inquisitor, the script was permissible to Devlin.  
 Charles V, Northwest Passage, the films length and severe costs limited inclusion to those aspects most desired by the producers. The script for similar reasons made minor alterations to relationships in the novel, eliminating Pedros prior dalliances with Catana, helping Juan against the Inquisition before being persecuted, and combining the characters Humpback Nojara, surgeon Antonio Escobar, and Botello the Astrologer into a single person, "Professor Botello". Even so, the screenplay faithfully adapts the important plot elements and scenes from the novel. 

Historically, the most barbaric atrocities of Cortéz are not depicted in the script. In particular, the slaughter of thousands of Aztecs in Cholula as a warning to Montezuma is instead shown as a single cannon shot demolishing an idol. The first review of the film in the New York Times noted that while the novel seemed written with a Technicolor movie in mind, that the action, horror, and bloodshed of the book were not translated to the film.     
 Estela Inda) mutineers Juan Escudero (John Laurenz) and Diego Cermeño (Reed Hadley), and the loyal Captains Pedro de Alvarado (Roy Roberts) and Gonzalo de Sandoval (Harry Carter).    

===Locations=== Indian porters, was filmed on the edge of Parícutins lava beds with the cinder cone prominently nearby in the shot.  The presence of the volcano, however, also proved to be expensive to production, since its ash cloud often made lighting conditions inconducive to filming. 

Filming began November 25, 1946, and was completed on April 4, 1947. The film company spent 83 days in Mexico before returning to Hollywood to complete 33 days of studio filming, at a then "extravagant" cost of $4.5 million.    

===Photography===
In addition to the directors of photography credited onscreen, George E. Clarke and Arthur E. Arling, Clarkes protogé Joseph LaShelle also contributed to the filming of Captain from Castile. While LaShelle was noted for excellent black-and-white photography, particularly in film noir, he had little experience with Technicolor or location shooting. Clarke was competent at both. LaShelles work in the film appears primarily in interior shots, notably in scenes at Pedros home. Arling was mainly responsible for second unit filming under assistant director Robert D. Webb. On location, photography inside the temples proved difficult because of poor space for proper lighting and excessive heat that could degrade color film. 

==Music== Alfred Newman, 78 RPM Charles Gerhardt conducted a suite from the film for RCA Victors tribute album to Newman, Captain from Castile; the quadraphonic recording was later reissued on CD. 

Newman bestowed the rights to the films spectacular march to the University of Southern California to use as theme music for the schools football team. Popularly known as "Conquest," the march is regularly performed by its marching band, the Spirit of Troy as a victory march.  It is also the corps anthem of the Boston Crusaders Drum and Bugle Corps, which has performed the piece in their field show frequently in the past and continues to incorporate it occasionally in their field shows of the present.

==Reception==
Though popular, the film failed to recoup its (at the time) enormous cost. 

==Adaptations==
A radio adaptation of Captain from Castile was aired on Lux Radio Theatre on February 7, 1949, with Cornel Wilde as Pedro and Jean Peters reprising her role. An adaptation starring Douglas Fairbanks, Jr. was broadcast on the Screen Directors Playhouse on May 3, 1951. 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 