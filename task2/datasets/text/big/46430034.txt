The Great Bank Hoax
{{Infobox film
| name           = The Great Bank Hoax
| image          = 
| alt            = 
| caption        = Theatrical release poster
| director       = Joseph Jacoby 
| producer       = Joseph Jacoby Ralph Rosenbaum 
| writer         = Joseph Jacoby  Michael Murphy Paul Sand Constance Forslund
| music          = Arthur B. Rubinstein 
| cinematography = Walter Lassally
| editing        = Ralph Rosenbaum 
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Michael Murphy, Paul Sand and Constance Forslund. The film was released by Warner Bros. on November 1, 1978.  

==Plot==
 

== Cast ==
*Richard Basehart as Manny Benchly
*Ned Beatty as Julius Taggart
*Charlene Dallas as Cathy Bonano
*Burgess Meredith as Jack Stutz Michael Murphy as Reverend Everett Manigma
*Paul Sand as Richard Smedley
*Constance Forslund as Patricia Allison Potter
*Arthur Godfrey as Major William Bryer Ret.
*John C. Becher as Alex Kaiser
*Guy Le Bow as The Sheriff
*John Lefkowitz as Deputy Bert
*Bibi Osterwald as Sara Pennyworth
*Alek Primrose as Rev. De Verite
*Martha Sherrill as Louise Farthington
*Roy Tatum as Deputy Jason 

== References ==
 

== External links ==
*  
 
 
 
 
 
 
 
 