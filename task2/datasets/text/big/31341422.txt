Guppedu Manasu
{{Infobox film
| name           = Guppedu Manasu
| image          = Guppedu Manasu.jpg
| image_size     =
| caption        = DVD cover of Guppedu Manasu
| director       = K. Balachander
| producer       = P. R. Govindarajan and J. Doraswamy
| writer         = K. Balachander   (screenplay)   Sharief   (story)  Acharya Atreya   (Lyrics)   Ganesh Patro   (Dialogues ) 
| narrator       = Sujatha Saritha
| cinematography = B. S. Lokanath
| editing        = N. R. Kittu
| distributor    =
| released       = 7 September 1979
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Telugu film Sujatha and Saritha in prominent roles. It was a bilingual film made simultaneously into Tamil as Nool Veli (1979). The film was a remake of the Malayalam film Aa Nimikasham. Kamal Haasan does a guest role in both Tamil and Telugu versions.

Eminent musicologist  ). It aptly describes the mental condition of the key roles.

==Plot==
Buchi Babu(Sarath Babu), an architect, his wife Vidya (Sujatha), a writer, and their young daughter, move in next door to Srimati, an out of work actress who is living with her teenage daughter Baby. Vidya also works at the Andhra Pradesh Film Censor Board.

The two families become good neighbors. Baby, who is very playful and naughty visits them very often. The neighbors have a lot of fun together playing games and going to the beach. There is a slight hint of inappropriate behavior by Buchi and Baby at the beach, but is dismissed off as she calls him Uncle. Vidyas brother who is a house surgeon in Osmania General hospital, comes to visit them, and falls in love with Baby.

A film producer comes to talk to Vidya, for permission to use one of her novels for his movie and offers her substantial money. She refuses saying that the adaptation will not be inline with her story and will do injustice to her ideal characters. While at her house, the producer sees Srimati, and asks Buchi if she is the veteran actress Srimati. He then approaches her and decides to cast her in a movie starring Kamal Haasan, as the mother of a lead character. She agrees, but on the set she realizes that she has lost her acting skills. With a heavy heart she goes to bed that night, and dies in her sleep.
Vidya, decides to bring up Srimatis daughter Baby, as her own daughter. Baby moves in with Vidya and her family.

Baby finishes school, and is sent to study in a hostel. Buchi Babu meets with an accident in his car, but escapes with minor injuries. His servant Tirupati phones Baby and tells her about the accident. Worried about Buchi, she immediately takes a taxi home, which breaks down at a distance from her home. She runs from there in rain, and gets wet. She feels assured after seeing that its just a minor injury. Buchi asks her to dry herself and change since she is wet. As Baby goes in to change behind a curtain, the wind blows away the curtain, and Buchi looks at her undressed. He is tempted to Baby.
He stands next to her and holds her hand and sees that she reciprocates the attraction.

Meanwhile, At the Film Censor Board, a director presents her a movie where a man marries his adopted daughter in a twist in the climax. She is disgusted with the idea and objects to incestuous content. After work, she gets her little daughter from school, and as she opens the door of her house, she finds her husband and Baby in bed together.

Although disgusted with what has happened, she tries to be ideal, like the characters in her novels. She tries to restore normalcy in the house by ignoring the incident. They all have dinner together and she seems to deal with it quite maturely. Later that night, she tells her husband that its ok, and that she understands the situation. A bit relieved, Buchi touches her hand to show her affection. At this point, she can no longer control her real feelings of disgust and she pushes him away. She tells him that she thought she could be ideal, but is not. At the core, she is just an ordinary woman, who needs fidelity, and she sleeps on the floor henceforth, away from her husband, refusing to share the bed.

She then calls up the Film Censor Board and withdraws her previous objections saying that far worse things happen in the real world, and there is no need to hide the truth.

Few weeks later, Vidya finds out that Baby is pregnant and has decided to keep the child. Vidya approaches her dad, for advice, telling him the entire story. He doesnt have any solution. Vidya thinks about it a lot, and decides to ask her brother to marry Baby and asks him to visit. Meanwhile, she talks to Baby and finds out that not only is Baby pregnant, but also she is attracted to her husband. When Mouli tells that he will marry her in spite of anything, Vidya scolds him, saying that he is just like any other man, thinking women as a piece of meat. Mouli is hurt, and takes a vow of celibacy, and leaves.

Vidya talks to her husband and he is very non-cooperative, and out of anger, that he is being repeatedly questioned, says that he is a man, and he will do as he wants. Vidya storms out with her daughter, and soon after, Baby leaves to Hyderabad and delivers a daughter who she names Vidya.

In the climax, they get together on the terrace of a 9 storey building, and she makes peace between them all and commits suicide.

==Cast==
{| class="wikitable"
|-
! Actor / Actress !! Character
|-
| Sarath Babu || Buchi Babu
|-
| Saritha || Baby
|-
| Narayana Rao || Mouli
|- Sujatha || Vidya
|-
| J. V. Ramana Murthy ||Vidyas Dad
|-
| Kamal Haasan || Guest appearance
|}

==Crew==
* Screenplay writer and Director : K. Balachander
* Producers : P. R. Govindarajan and J. Doraswamy
* Production Company : Kalakendra Movies
* Photography : B. S. Lokanath
* Operative Cameraman : R. Raghunath Reddy
* Editor : N. R. Kittu
* Art Director : A. Ramaswamy
* Lyrics : Acharya Atreya
* Music : M. S. Viswanathan
* Playback singers : S. P. Balasubrahmanyam, Mangalampalli Balamuralikrishna and Vani Jayaram

==Soundtrack==
* "Kanne Valapu Kannela Pilupu" (Lyrics: Acharya Atreya; Singer: S.P. Balasubramanyam and Vani Jayaram; Cast: Narayana Rao and Saritha)
* "Mouname Nee Basha O Mooga Manasa" (Lyrics: Acharya Atreya; Singer: Mangalampalli Balamuralikrishna)
* "Nenaa Paadanaa Paata Meera Annadi Maata" (Lyrics: Acharya Atreya; Singer: Vani Jayaram and S.P. Balasubramanyam; Cast: Sarath Babu and Sujatha)
* "Nuvvena Sampangi Puvvuna Nuvvena" (Lyrics: Acharya Atreya; Singer: S.P. Balasubramanyam; Cast: Narayana Rao)

==External links==
*  

 

 
 
 
 
 
 
 