Bombay Calling
 
 
{{Infobox film
| name           = Bombay Calling
| image          = BombayCalling.jpg
| caption        = Bombay Calling promotional poster
| distributor    = National Film Board of Canada
| writer         = Ben Addelman, Samir Mallal
| cinematography = Ben Addelman Samir Mallal
| editing        = Hannele Halm
| director       = Ben Addelman, Samir Mallal
| producer       = Adam Symansky
| music          = Ramachandra Borcar, LCD Soundsystem, Bombay Rockers, DJ Aqueel
| released       = August 22, 2006 (Limited)
| runtime        = 72 mins.
| language       = English
| awards         =
}}

Bombay Calling is a 2006 feature documentary film directed by Ben Addelman and Samir Mallal.  Produced by the National Film Board of Canada, Bombay Calling chronicles the lives of young call center workers in Bombay (Mumbai) India.  

==Synopsis==
Bombay Calling delves into the lives of a group of young Indians working outsourced jobs at a call center in Bombay.  Without focusing too much on the politics, it profiles several characters as they train for and attempt to sell phone services to clients in the UK.  The film shows both sides of the impact of globalization on India - the economic benefits, but also the break with tradition and loss of innocence the characters face.  By the end of the film, the telemarketing venture has failed but the characters are resilient.  For this reason, the film has been compared to Startup.com. 

==Awards==
It won the Grand Jury Prize at the Indian Film Festival of Los Angeles, and Most Innovative Documentary at the Doxa Film and Video Festival.  The Film has played at  festivals such as Hot Docs, Melbourne and Bergen and received a theatrical release in Canada during the summer of 2006.  

==Release==
On August 22, it was released on DVD in Canada by Mongrel Media.  National Geographic International has been broadcasting the film on cable television around the world, and it was shown in India on August 15, to critical acclaim. 

==Reviews==
"An eye-opening look at the strangeness of an emerging and already conflicted middle-class."   LA Weekly  

“4 stars...a vibrant, insightful look at young people employed in Indias bustling call-centre industry....like an East Asian companion piece to Startup.com."    Eye Toronto  

"Analogous to nothing so much as David Mamet|Mamets Glengarry Glen Ross, so vividly does it encapsulate the mugs game of sales.”   Toronto Sun   
 
“Captures the Sense of the cool world inhabited by telemarketers.”   Vancouver Sun 

==See also==
*Nalini by Day, Nancy by Night

==References==
 

==External links==
*  
*  
*  
*   

 
 

 
 
 
 
 
 
 
 
 
 