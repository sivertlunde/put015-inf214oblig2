Copacabana (1947 film)
{{Infobox film
| name           = Copacabana
| image_size     =
| image	         = Copacabana FilmPoster.jpeg
| caption        =
| director       = Alfred E. Green
| producer       = Sam Coslow Howard Harris László Vadnay (story and screenplay)
| narrator       =
| starring       = Groucho Marx Carmen Miranda Edward Ward
| cinematography = Bert Glennon
| editing        =
| distributor    = United Artists
| released       =  
| runtime        = 92 minutes
| country        = United States English
| budget         =
| gross          =
}} musical comedy film starring Groucho Marx and Carmen Miranda.
 Harpo and Chico Marx|Chico.
 Andy Russell), sings a song called "Stranger Things Have Happened", admitting her unrequited love for her employer, Steve (Steve Cochran).

==Plot==

Lionel Q. Devereaux and his alluring girlfriend, Brazilian singer Carmen Navarro have been engaged for ten years. They are highly unsuccessful nightclub performers, due to Lionels total lack of talent. They stay at an upscale hotel in New York. One day they get a twenty four hour notice to pay their bill, but needless to say they lack the funds to oblige. They hurriedly try to convince the big shot producer Steve Hunt to give Carmen a job at the Club Copacabana, and with the help of the easily convinced, gullible singer Andy Russell, posing as an agent, they achieve their goal to get her an audition.

When the producer asks Lionel and Russell whom else they represent, they invent out of thin air a veiled mysterious beauty from Paris and call her Fifi. They persuade Carmen to play the part of Fifi. The producer hires both ladies for the job, but Fifi is the new big sensation who gets mentioned in the press. Steve is very attracted to the girls, and to protect Carmen from the producer, Lionel tells him that he is engaged to be married to Carmen. Steve then turns to Fifi and asks her out instead. Desperate to solve the troublesome situation, Lionel asks Andy to play Fifi and go on a date with the producer, veiled as usual. Another complication to add to the plot is that Anne, Steves secretary, is in love with the producer, and not very keen on him going on a date with Fifi.

Andy tries to fix up Steve and Anne, to save both himself and Carmen from discovery. He gets Anne to sing her feelings towards Steve, in an attempt to make him more attracted to and aware of her. The plan doesnt work, as Steve shows no interest in Anne.

A Hollywood movie producer, Anatole Murphy, takes an interest in Fifi, and makes a generous offer to Steve, to take over Lionels contract for the sum of $100,000, which he refuses. At the same time an agent named Liggett persuades Lionel to sell Fifis contract to him for the lesser sum of $5,000. Murphy in turn pays $100,000 to Liggett.

But Liggett becomes suspicious, since he sees how the veiled Fifi get into a taxi, and then Carmen comes out of it. Anne reveals to Carmen that the mysterious Fifi has mad it impossible for her to get Steves attention. To help Anne out, Lionel and Carmen stage a fight between Carmen and Fifi in Carmens dressing room. The fight ends with Fifi disappearing. Lionel reports back to Steve that Fifi has been found dead in the river, but he also expresses his feeling of joy over "killing" her. The conversation is overheard, and he is blamed and arrested for Fifis murder. Lionel tries to explain to the police during the investigation, that he only made Fifi up.

In the meantime, Steve confesses to Anne that he ony expressed an interest in Fifi because of his business, and that he is in love with Anne. Carmen enters the scene, dressed as Fifi, but removes her veil in front of everybody, showing that Carmen and Fifi are one and the same. The film producer Murphy offers to sign a contract with Carmen, to use her as an actor in his productions, and also wants to buy the story for a film. Lionel becomes involved in the following film productions, and gets credit for almost everything, from casting to storyline. The picture opens with a song about the Club Copacabana. 

==Cast==
*Groucho Marx as Lionel Q. Devereaux
*Carmen Miranda as Carmen Navarro / Mademoiselle Fifi
*Steve Cochran as Steve Hunt Andy Russell as himself
*Gloria Jean as Anne Stuart
*Abel Green as himself (editor of Variety (magazine)|Variety magazine)
*Louis Sobol as himself, columnist Earl Wilson as himself
*Ralph Sanford as Liggett

Kay Mavis, Grouchos then-wife, has a small role as a clerk from whom Groucho tries to mooch a cigar.

=== Production ===
The idea of shooting this film was born when the directors of United Artists, suggested the Sam Coslow, that the company should treat of the production of some musical films for this year  . Then Coslow sought George Frank, manager of the artistic interests of Carmen Miranda, and the leaders of the Copacabana (nightclub)|Copacabana - famous night club in New York - Monty Proser and Walter Bachelor.  Was resolved that Carmen would appear in the film and that the important night club would serve as the backdrop for the production. David Hersh joined as one of the films backers. So Coslow, George Frank, Monty Proser and Walter Bachelor raised capital for production expenses, including the salaries with office expenses and employees. The film had a budget $1,300,000 dollars. 
 Fox for Earl Wilson (New York Post). At the time of the production, Groucho Marx was married to Kay Gorcey, who had a small role in this film.
 Richard Elliott, Frank Scannell, Pierre Andre and Andrew Tombes to the cast, but their participation in the completed film has not been confirmed. Pierre Andre was signed to perform a specialty dance number with Dee Turnell, according to HR.

In mid-Feb 1947, HR reported that producer Sam Coslow was considering reshooting scenes in which Miranda appears in a blonde wig, because of mail from Brazilian fans stating that they prefer her as a brunette. The reshot scenes were to be inserted in South American release prints only, according to the item.

The film was also re-issued in Jul 1972. 

=== Soundtracks ===
*Weve Come to the Copa — The Copa Girls
*Tico-Tico no Fubá — Carmen Miranda
*Je Vous Aime — Carmen Miranda
*My Heart Was Doing a Bolero — Andy Russell
*He Hasnt Got a Thing to Sell — Carmen Miranda and Andy Russell
*To Make a Hit with Fifi — Carmen Miranda
*Stranger Things Have Happened — Andy Russell
*Stranger Things Have Happened — Gloria Jean
*Go West, Young Man — Groucho Marx
*Je Vous Aime — Andy Russell
*Lets Do The Copacabana — Carmen Miranda 

== Critical reception == AMC said "Groucho is unfortunately without his brothers (...) and the musical numbers consume much of the picture, and the latter half focuses on a musical sub-story love affair between two minor characters, one of whom is Andy Russell (as himself)." 

"Carmen Miranda and Mr. Marx (...) Together they scream and grimace through a succession of topsy-turvy scenes, some of them mildly amusing and others relentlessly dull (...) With Groucho trying to do for three people and Miss Miranda imitating two, the impression is that Sam Coslow, the producer, has understaffed his film." said Bosley Crowther in his review to the newspaper The New York Times. 

==Availability==
The film was released on DVD by Republic Pictures through Artisan Entertainment in 2003. In 2013, Olive Films released a new DVD and Blu-ray of the film. 

==References==
 

==External links==
*  
*  
*  
*   at Turner Classic Movies
*   at NNDB
*  

 

 
 
 
 
 
 
 