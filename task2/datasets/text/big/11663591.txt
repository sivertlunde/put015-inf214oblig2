The Contractor (2007 film)
 
{{Infobox film
| name = The Contractor
| image = The-Contractor-DVD.jpg
| caption = DVD cover
| director = Josef Rusnak
| producer = Rudy Cohen
| writer = Robert Foster Joshua Michael Stern
| story = Robert Katz Andre Farwagi
| starring = Wesley Snipes Eliza Bennett Lena Headey Ralph Brown
| music = Nicholas Pike
| cinematography = Wedigo von Schultzendorff
| editing = Tracy Granger James Herbert
| studio = Destination Films April Productions RMA Productions
| distributor = Sony Pictures Home Entertainment	 	 	 	 
| released =  
| runtime = 98 min.
| country = United States
| language = English
| budget = $18 million
| gross = $15,524,680    Retrieved 2012-11-09 
}}
The Contractor is a 2007 American action film directed by Josef Rusnak starring Wesley Snipes, Eliza Bennett, Lena Headey and Ralph Brown. The film was released on direct-to-video|direct-to-DVD in the United States on July 10, 2007.

==Plot==
A retired CIA Black-Ops hitman James Jackson Dial (Wesley Snipes) is living his life in seclusion on his ranch in Montana when he is offered the chance to redeem himself by his former employer, Jeremy Collins (Ralph Brown). Some years earlier, Dial had been seconds away from taking down notorious terrorist cell leader Ali Mahmoud Jahar (Nikolay Sotirov) but his carelessness allowed Jahar to escape, marking his assignment as a failure.
 police in London, England. Provided with a safe house, passports, and an assistant, Terry Winchell (Richard Harrington), Dial is off to London to complete unfinished business. Disguised as a priest, Dial gets into position in a church bell tower across the street from the building that Jahar is being brought into. When Jahar is brought out of the police van, his head is covered by a jacket, leaving Dial without a clean shot. Not wanting to fail again, Dial patiently waits for his opportunity.

Moments later, from his high vantage point, Dial spots Jahar standing inside the building with his back to the window. Dial grasps the opportunity and shoots through the glass, fatally hitting Jahar in back of his head. Because of the delay caused by a pair of cops, Winchell is late in bringing the getaway car, and this brings attention to them fleeing the scene. After numerous attempts to stop the car, the police open fire, killing Winchell. The car, now out of control, comes to crashing stop, injuring Dial. But Dial manages to get out of the crashed car. Now on the run and bleeding profusely, he makes his way to the safe house, where he meets curious 12-year-old neighbour Emily Day (Eliza Bennett), who lives with her grandmother (Gemma Jones). Emily helps Dial stop the bleeding. Even though Jahar has been eliminated, the assignment is still considered to be failure by Collins because Dial had been filmed fleeing the scene by a security camera. 

Meanwhile, Collins is being investigated in the USA for running a CIA Hit Squad, and if the truth gets back about Dial and the death of Chief Inspector Andrew Windsor (Charles Dance) which happens during a shoot-out, Collins will be exposed. Stopping at nothing to keep his name clean, Collins frames Dial for the murder of the Chief Inspector, even though Collins was actually responsible for his death. Dial, now a wanted man by Collins and the British police, must do whatever it takes to prove his innocence, knowing that Inspector Annette Ballard (Lena Headey), Windsors vengeful daughter, is also after him.

While Dial is being pursued by Collins and his rogue hit squad, Dial saves the life of Inspector Annette Ballard, who found herself in Collins cross hairs. Having earned Ballards trust, Dial is seen by Ballard bidding farewell to young Emily who played a key role in Dials survival. Later on it is shown that Dial sent Emily a letter along with 2 tickets to America to see him and his horse Beauty on his ranch. The last scene shows Emily and Dial at the ranch in Montana, watching Beauty run free.

==Cast==
*Wesley Snipes as James Jackson Dial
*Eliza Bennett as Emily Day Inspector Annette Ballard
*Ralph Brown as Jeremy Collins
*Charles Dance as Detective Chief Superintendent Andrew Windsor
*Gemma Jones as Emilys Grandmother
*Iain Robertson as Cramston Richard Harrington as Terry Mitchell
*Ryan McCluskey as Purcell
*John Standing as Sir Anthony
*Stanimir Stamatov as King
*Miroslav Emilov as Harris
*Velizar Binev as Beloit
*Nikolay Sotirov as Ali Mahmud Jahar
*Peter Kelly Morgan as Newscaster

==Production==
It is set and filmed mainly in Sofia, Bulgaria and Cardiff, South Glamorgan, Wales,   and partly in the US state of Montana in 47 days on 31 March and 17 May 2006. 

==Home media== Region 1 in the United States on July 10, 2007, and also Region 2 in the United Kingdom on 6 August 2007, it was distributed by Sony Pictures Home Entertainment. About 815,698 DVD units have been sold, bringing in $15,524,680 in revenue. 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 