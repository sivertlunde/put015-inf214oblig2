Song at Midnight
 
 
{{Infobox Film 
| image          = SongatMidnightDVD.jpg 
| caption        = The Cinema Epoch edition DVD of Song at Midnight
| writer         = Ma-Xu Weibang 
| starring       = Jin Shan Gu Menghe Zhou Wenzhu Hu Ping
| director       = Ma-Xu Weibang 
| cinematography = Yu Xingsan Xue Boqing 
| producer       = Zhang Shankun
| studio         = Xinhua Film Company
| distributor    = 
| runtime        = 113 minutes
| released       = 1937
| country        = China
| language       = Mandarin
| budget         =
|}} 1937 film directed by Ma-Xu Weibang. Often referred to as the first Chinese horror film, Song at Midnight is a remake/adaptation of Gaston Lerouxs Phantom of the Opera, though the film injects a significant political subplot involving the leftist revolutionary movement to the original story.
 sequel to Song at Midnight in 1941 during the Second Sino-Japanese War|war. Both films resurfaced in the West at the Udine Far East Film Festival in 1998. {{Citation
 | surname1    = Robinson
 | given1      = David
 | year        = Winter 1999–2000
 | title       = Return of the Phantom
 | journal     = Film Quarterly
 | volume      = 53
 | number      = 2
 | page        = 43
 | doi=10.2307/1213720
}}  Since then, the film has been shown at various film festivals around the world, notably at the 62nd Venice International Film Festivals "Secret History of Chinese Cinema" retrospective.

==Reputation== Hong Kong Film Awards in 2005 in film|2005, and by Asia Weekly in 1999. {{cite web | url = http://www.chinesecinemas.org/chinacentury.html
| title =  100 Greatest Chinese Films of the 20th Century
 | accessdate = 2007-05-10 |date= 1999-12-19| publisher = Asia Weekly Magazine}} 

Song at Midnight has also been remade twice. The first remake, translated as The Mid-Nightmare is a two-parter by Hong Kong director, Yuan Qiuxia, released in 1962 (part I) and 1963 (part II). It stars Betty Loh Ti and Lao Zhei. In 1995, Song at Midnight was remade yet again, this time as The Phantom Lover by Ronny Yu, with Leslie Cheung in the role of Song Danping.

==Cast==

* Gu Menghe
* Hu Ping
* Jin Shan
* Yee Chau-shui
* Zhou Wenzhu

==DVD release== Region 0 DVD on May 8, 2007. The DVD includes subtitles in English.

An earlier DVD edition by the Guangzhou Beauty Culture Communication Co. Ltd was released on December 1, 2006 in the United States.

==References==
 

==External links==
*  
*  
*  
*   at the Chinese Movie Database
*   at the University of California, San Diego|UCSD: Chinese Cinema Web-based Learning Center

 

 
 
 
 
 
 
 
 


 
 