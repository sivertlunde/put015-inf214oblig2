Lake Mungo (film)
 
{{Infobox film
| name           = Lake Mungo
| image          = Lake Mungo Official Poster.jpg
| caption        = Official Poster
| director       = Joel Anderson
| producer       = Georgie Nevile David Rapsey Kerri Schwarze
| writer         = Joel Anderson
| starring       = Talia Zucker Rosie Traynor David Pledger
| music          = Dai Paterson
| cinematography = John Brawley
| editing        = Bill Murphy
| distributor    = Arclight Films
| released       =  
| runtime        = 89 Minutes
| country        = Australia English
| gross = A$29,850 (Australia) 
}} 
Lake Mungo is a 2008 Australian psychological horror mockumentary film directed by filmmaker Joel Anderson and stars Talia Zucker.

==Plot==
Sixteen-year-old Alice Palmer drowns while swimming with her family at a dam in Ararat, Australia. After she dies strange things start to happen around her house. Its discovered that her teen brother Matthew was actually setting up the "sightings" of his dead sister to give the family reason to exhume Alices body and give his mother closure.

One of the hoax video tapes of Alice showed that the neighbor was in Alices bedroom looking for something, her mother found a sex tape of the neighbor, his wife and Alice hidden in the fireplace in Alices room.

A psychic the family had contacted finally admitted that Alice had met with him several months before her death and had told him she was having dreams about drowning, being dead and her mother not being able to see or help her. Alices boyfriend finally comes forward with cell phone video footage showing Alice on a school trip to Lake Mungo, she is shown very upset and burying something beneath a tree.

Alices family travels to Lake Mungo, and finds what Alice had hidden, her cell phone and a bracelet. The video footage on the cell phone shows that Alice had seen her doppelgänger walking toward her. The double appears bloated just as Alices body had been after being recovered from the lake in the beginning of the film.

Alices family then goes home and moves out of their house, feeling that Alice had simply wanted them to know who she really was and what she had seen. The ending of the film adds some ambiguity to whether this was truly accomplished, as the films Post-credits scene|mid-credit scenes reveal that Alice had manifested in earlier pictures and videos, with nobody noticing.

==Cast==
*Talia Zucker - Alice Palmer 
*Rosie Traynor - June Palmer 
*David Pledger - Russell Palmer
*Martin Sharpe - Mathew Palmer
*Steve Jodrell - Ray Kemeny
*Tamara Donnellan - Marissa Toohey
*Scott Terrill - Brett Toohey
*Helen Bath - Helen Bath

==Production==
 
The bulk of the film was written in 2005 and was inspired by documentaries of the time. 

==Release==
The film was screened in Austin, Texas (USA) at the South by Southwest Film Festival in March 2009.  In Australia, the film was at the Travelling Film Festival Wagga Wagga on 13 March 2009.  Lake Mungo was screened in the United Kingdom on 17 March at the Barbican London Australian Film Festival.  The film ran in the United States on 21 January 2010 at After Dark Horrorfest with a Lionsgate distribution. 

==Reception==
 
 
Lake Mungo was met with critical acclaim, earning a 93% approval rating on Rotten Tomatoes.   Andrew L. Urban of Urban Cinefile said "This superbly constructed and executed film gets everything right, to the smallest detail, as it draws us into the imagined scenario." 

==See also==
*List of ghost films
*Cinema of Australia

==References==
 

== External links ==
*  
*  
*   at   

 

 
 
 
 
 
 
 
 
 
 
 