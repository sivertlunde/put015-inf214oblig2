You (film)
 
{{Infobox film
| name           = You
| image          = You film poster.jpg
| caption        = Film poster
| director       = Melora Hardin
| producer       = Gildart Jackson Melora Hardin
| writer         = Gildart Jackson
| starring       = Melora Hardin Gildart Jackson Brenda Strong Joely Fisher Allison Mack Amy Pietz Jerry Hardin Don Michael Paul
| music          = Tim Burlingame
| cinematography = Kev Robertson
| editing        = Jason Schmid
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
}}
 directorial debut).  The film also stars Gildart Jackson (Hardins husband, who also wrote the film), Brenda Strong, Joely Fisher, Allison Mack, Amy Pietz, Jerry Hardin, and Don Michael Paul.  

==Plot==
 
Husband and wife Rawdon (Gildart Jackson) and Miranda (Melora Hardin) are soulmates. Their love for each other is palpable as they lie in bed treasuring their 6 month old daughter Quincey. Miranda fantasizes about the speech she plans to make when, one day in their future, she will give this tiny infant away at her wedding.

Tragically, when Quincey is 3, Miranda is killed in a car accident and Rawdon is left alone, unmoored, to continue life without the love of his life.    

Fortunately Rawdon still has Quincey.  Staying as strong as he can he takes her to the roof of a skyscraper and explains that Mommy is now an angel.   It is up here that for the first time they see a vision of Miranda as an angel.   

Rawdon continues to see visions of her. Initially these visions help him—at home she gives him advice about parenting; in Portugal she softens his hardness;  when he hides in the closet to isolate himself she snaps some sense into him and insists that Rawdon keep her mother, father and brother—Quinceys Grandparents (Jerry Hardin, Diane Hardin) and Uncle Jack (Don Michael Paul) – firmly in Quinceys life.  

Rawdon explains these visions to his therapist Paula (Brenda Strong) as she helps him with the long grieving process and over a period of years, with Quincey growing before our eyes, Rawdon tries to move on with his life.   

He tries to date other women. Disastrously.  He falls instantly in love with Sam (Amy Pietz) who ends up becoming his good friend and a surrogate Aunt to Quincey.  He dates other including Kimberly (Joely Fisher) who Quincey wisely suggests is far better suited to her Uncle Jack.   And she is right.  When Jack and Kimberly get married, Rawdon is their best man.

All the while Mirandas metaphysical presence is with him.   He goes from needing her, to blaming her, being angry at her.  But for a long, long time he wont let her go away.   Part of him wants her to stay alive in his imagination forever and gradually this presence that helped him initially eventually becomes an impediment to his moving on in life.   

In contrast to her Dads stagnation, Quincey is growing up fast (Allison Mack as a 20 year old), and in no time she is dating, stealing cars, trying to set her Dad up with her best friends divorced Moms, going off to University and falling in love with a Frenchman named Philippe.  

It is not until Rawdon finally plucks up the courage to ask out somebody who might just be right for him that he finally, tearfully, asks Miranda to leave him.    

This is ultimately a coming of age story.  We watch Quincey literally come of age whilst her father, emotionally, comes of age.  He eventually gives up the now stultifying memory of his dead wife and arrives at a new beginning.   

The end of this story is also the beginning of another where Rawdon, surrounded by the people he loves—but without Miranda—finds himself giving Quincey away at her wedding to Philippe.  And here he makes the speech that Miranda made in bed with him and Quincey so many years before.

==Cast==
*Melora Hardin as Miranda
*Gildart Jackson as Rawdon
*Brenda Strong as Paula
*Joely Fisher as Kimberly
*Allison Mack as Quincey
**Kristi Lauren as Young Quincey
*Amy Pietz as Sam
*Jerry Hardin as Rex
*Don Michael Paul as Jack

==External links==
* 
* 

 
 
 
 
 
 