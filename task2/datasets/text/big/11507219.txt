You Must Be Joking! (1986 film)
{{Infobox film
| name           = You Must Be Joking!
| image          = You Must Be Joking!.jpg
| image_size     = 
| caption        = DVD Cover
| director       = Elmo De Witt
| producer       = Elmo De Witt Hermann Visser
| writer         = Leon Schuster Fanus Rautenbach
| narrator       = David Blood Tony Sanderson
| starring       = Leon Schuster Mike Schutte Kallie Knoetze Golda Raff Janine Pretorius Martino
| music          = Johan Van Rensburg Leon Schuster
| cinematography = 
| editing        = Gerrie Van Wyk
| distributor    = Elmo De Witt Films Kavalier Films
| released       =  
| runtime        = 96 minutes
| country        = South Africa
| language       = South African English Afrikaans
| budget         = 
| gross          = 
}} Leon Schusters hidden camera film.

==Plot==
In a series of short skits, Leon Schuster uses candid camera and several disguises to stitch up the general public of South Africa. Such sketches include:
*The watermelon pulse test, where Leon shows that a watermelon is only ripe when it does not have a Cardiac cycle|heartbeat, much to the confusion of the seller.
*Cooking on the bonnet of a car, while upsetting the traffic police.
*Supposedly killing a cow in a butchers shop.

==Cast==
*Leon Schuster
*Mike Schutte
*Kallie Knoetze
*Golda Raff
*Janine Pretorius
*Martino

==External links==
* 

 
 
 
 

 
 