Another Man's Poison
 
 
{{Infobox film
| name           = Another Mans Poison
| image          = AnotherMansPoison.jpg
| image_size     =
| caption        = Original poster
| director       = Irving Rapper
| producer       = Daniel M. Angel Douglas Fairbanks Jr.
| writer         = Val Guest Anthony Steel Barbara Murray John Greenwood Paul Sawtell
| cinematography = Robert Krasker
| editing        = Gordon Hales
| studio         = Angel Productions 
| distributor    = Eros Films
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
}}
Another Mans Poison is a 1951 British drama film directed by Irving Rapper and starring Bette Davis, Gary Merrill and Emlyn Williams. The screenplay by Val Guest is based on the play Deadlock by Leslie Sands.

==Plot== mystery novelist Janet Frobisher, who has been separated for years from her husband, a man with a criminal past, lives in an isolated home in England. Her nearest neighbour is nosy veterinarian Dr. Henderson. Janet falls in love and occasionally dabbles with her secretary Chris fiancé, Larry, who is years younger than she. When her estranged husband unexpectedly appears, Janet poisons him by administering horse medication given to her by her neighbour. One of the deceased mans criminal cohorts arrives as shes preparing to dispose of the body in the local lake. When Frobishers secretary and Larry arrive at the secluded house, the mysterious man, who has assisted her with her scheme, impersonates the long-absent spouse of Janet, who plots to get rid of her unplanned accomplice, as well.

==Production notes==
Of the project, star Bette Davis recalled, "We had nothing but script trouble. Gary   and I often wondered why we agreed to make this film after we got started working on it. Emlyn   rewrote many scenes for us, which gave it some plausibility, but we never cured the basic ills of the story." 

This was the second on-screen pairing of then-married couple Davis and Gary Merrill, following All About Eve the previous year.

Rapper, who was selected by Davis to helm the film, had directed her in Now, Voyager ten years earlier.
 The Corn Is Green was based.

Exteriors of the United Artists release were filmed on location in Malham, North Yorkshire, and interiors were shot at the Nettlefold Studios in Walton-on-Thames, Surrey.

==Principal cast==
*Bette Davis ..... Janet Frobisher
*Gary Merrill ..... George Bates
*Emlyn Williams ..... Dr. Henderson Anthony Steel ..... Larry Stevens
*Barbara Murray ..... Chris Dale
*Reginald Beckwith ..... Mr. Bigley
*Edna Morris ..... Mrs. Bunting

==Principal production credits== Producer ..... Douglas Fairbanks, Jr., Daniel M. Angel Original Music John Greenwood (UK release), Paul Sawtell (US release)
*Cinematography ..... Robert Krasker Art Direction ..... Cedric Dawe Julie Harris

==Critical reception==
The New York Times described the film as "a garrulous but occasionally interesting excursion into murder and unrequited love . . . the script . . . is basically a static affair that rarely escapes from its sets or the scenarists verbosity. Suspense is only fitfully generated and then quickly dissipated . . . Gary Merrill contributes a thoroughly seasoned and convincing portrayal . . . Emlyn Williams adds a professionally polished characterization . . . and Anthony Steel and Barbara Murray are adequate . . . However, Another Mans Poison is strictly Bette Davis meat. She is permitted a wide latitude of histrionics in delineating the designing neurotic who is as flinty a killer as any weve seen in the recent past." 

In his review in New Statesman and Nation, Frank Hauser wrote, "No one has ever accused Bette Davis of failing to rise to a good script; what this film shows is how far she can go to meet a bad one." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 