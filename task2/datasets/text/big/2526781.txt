Trancers
{{Infobox film
| name           = Trancers
| image          = Trancersposter.jpg
| caption        = Theatrical release poster
| director       = Charles Band
| writer         = Danny Bilson Paul De Meo
| starring       = Tim Thomerson Helen Hunt Michael Stefani
| producer       = Charles Band
| music          = Phil Davies Mark Ryder
| cinematography = Mac Ahlberg
| editing        = Ted Nicolaou Empire Pictures
| released       =  
| runtime        = 76 minutes
| language       = English
| country        = United States
}}
Trancers is a 1985   (1991),   (1994),   (1994) and  , which was set between the first two films, was released via fullmoonstreaming.com in September 2013.

This film portrays a method of time travel: People can travel back in time by injecting themselves with a drug that allows them to take over the body of an ancestor. When Jack Deth arrives in 1985, he is in the body of his ancestor, a journalist; Whistler assumes control of his ancestor, a police detective; and Deths supervisor, McNulty, borrows the form of his own forebear, a young girl.

==Plot==
Jack Deth (Tim Thomerson) is a police trooper in the year 2247 who has been hunting down Martin Whistler (Michael Stefani), a criminal mastermind who uses a psychic power to turn people into zombies and carry out his orders. Deth can identify a tranced victim by scanning them with a special bracelet. All trancers appear as normal humans at first, but once triggered, they become savage killers with twisted features.

Before he can be caught, Whistler escapes back in time using a drug-induced time traveling technique. Whistler leaves his body in 2247 and travels down his ancestral bloodline arriving in 1985 and taking over the body of an ancestor, who happens to be a Los Angeles police detective named Weisling.

Once Deth discovers what Whistler has done, he destroys Whistlers body – effectively leaving him trapped in the past with no vessel to return to – and chases after him through time the same way. Deth ends up in the body of one of his ancestors; a journalist named Phil Dethton.

With the help of Phils girlfriend - a punk rock girl named Leena (Helen Hunt) - Deth goes after Whistler, who has begun to "trance" other victims. Whistler plots to eliminate the future governing council members of Angel City (the future name of Los Angeles), who are being systematically wiped out of existence by Whistlers murder spree of their own ancestors. Deth arrives too late to prevent most of the murders and can only safeguard Hap Ashby (Biff Manard), a washed-up former pro baseball player, who is the ancestor of the last surviving council member, Chairman Ashe.

Deth is given some high-tech equipment, which is sent to him in the past: his sidearm, (which contains two hidden vials of time drugs to send him and Whistler back to the future), and a "long-second" wristwatch, which temporarily slows time, stretching one second to ten. The watch has only enough power for one use, but he later receives another watch to pull the same trick again.

During the end fight with Whistler, one of the drug vials in Jacks gun breaks, leaving only one vial to get home. Jack is forced to make a choice: kill the innocent Weisling (who is possessed by the evil Whistler), or use the vial to send Whistler back to 2247, which would strand Jack in the present. Jack chooses to inject Weisling with the vial, saving the lieutenants life but condemning Whistler to an eternity without a body to return to. Jack then decides to remain with Leena in 1985, although observing him from the shadows is McNulty, his boss from the future, who has traveled down his own ancestral line, ending up in the body of a young girl.

==Cast==
* Tim Thomerson as  Trooper Jack Deth/Phil Deth
* Helen Hunt as Leena
* Michael Stefani as Martin Whistler/Police Detective Weisling
* Art LaFleur as McNulty (as Art La Fleur)
* Telma Hopkins as Engineer Ruth "Ruthie" Raines
* Richard Herd as Chairman Spencer Anne Seymour as Chairman Margaret Ashe
* Peter Schrum as Santa Clause Barbara Perry as Mrs. Santa Claus
* Biff Manard as Hap Ashby
* Richard Erdman as Drunken Wise Man
* Wiley Harker as Dapper Old Man
* Miguel Fernandes as Officer Lopez
* Alyson Croft as "Baby" McNulty

== References ==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 