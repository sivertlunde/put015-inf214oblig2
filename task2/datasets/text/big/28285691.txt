Closed Circuit (1978 film)
{{Infobox film
| name           = Closed Circuit
| image          = 
| caption        = 
| director       = Giuliano Montaldo Mario Gallo Enzo Giulioli
| writer         = Nicola Badalucco Mario Gallo Giuliano Montaldo
| starring       = Flavio Bucci
| music          = Egisto Macchi
| cinematography = Giuseppe Pinori
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Italy
| language       = Italian
| budget         = 
}}

Closed Circuit ( ) is a 1978 Italian crime film directed by Giuliano Montaldo. It was entered into the 28th Berlin International Film Festival.   

==Cast==
* Flavio Bucci - Il sociologo Tony Kendall - Roberto Vinci
* Aurore Clément - Gabriella William Berger - Il pistolero sfidante
* Giuliano Gemma - Il pistolero
* Luciano Catenacci - Vice-questore
* Giovanni Di Benedetto - Nonno
* Umberto Gradi
* Elisabetta Virgili
* Mattia Sbragia - Ladolescente
* Giorgio Cerioni
* Franco Balducci - Aldo Capocci
* Guerrino Crivello - Piccoletto
* Alfredo Pea - Garzone

==References==
 

==External links==
* 

 
 
 
 
 


 
 