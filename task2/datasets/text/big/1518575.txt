Rugrats in Paris: The Movie
 
 
{{Infobox film
| name           = Rugrats in Paris: The Movie
| image          = Rugrats in Paris The Movie poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       =  
| producer       =  
| writer         =  
| starring       =  
| music          = Mark Mothersbaugh
| editing        = John Bryant
| studio         = Nickelodeon Movies Klasky Csupo
| distributor    = Paramount Pictures
| released       =  
| runtime        = 78–85 minutes  
| country        = United States
| language       = English
| budget         = $30 million 
| gross          = $103.3 million 
}} animated comedy-drama film and the sequel to The Rugrats Movie that follows the continuing adventures of the Rugrats.  In the film, Chuckie Finster takes the lead character role as he searches to find a new mother. The film was produced by Nickelodeon Movies and Klasky Csupo and distributed by Paramount Pictures and released into theaters on November 17, 2000. 

The film grossed $76.5 million domestically and $103.3 million worldwide.   

The film marks the first appearance of two villains in the Rugrats franchise, the child-hating Coco LaBouche and her accomplice, Jean-Claude. The film also marks the first appearance of new Rugrats character Kimi Finster and her mother Kira Finster.

==Plot==
The film opens with a parody of The Godfather, at the wedding reception of Lou Pickles and his new wife, Lulu. A mother-child dance during the reception saddens Chuckie Finster, who realizes that he has lived most of his life without his mother, Melinda, who died of a sudden illness shortly after he was born. His father, Chas shares Chuckies loneliness.
 Phil and Spike and all their parents travel to Paris to take a vacation at the park.

Coco LaBouche, the cold-hearted child-hating head of EuroReptarland, yearns to be the president of the entire Reptar franchise after her boss, Mr. Yamaguchi, resigns as the president. Yamaguchi says that his successor has to love children to be able to do the job, so LaBouche lies to him, by claiming to be engaged to a man with a child. Upon the Rugrats arrival in Paris, and later at EuroReptarland, Angelica overhears a conversation between Coco and Yamaguchi before being caught. To save herself from punishment, Angelica reveals that Chas is looking for a wife and suggests that Coco marry him. Coco strikes up a relationship with Chas, but her attempts to bond with Chuckie fall flat. The adults and babies meet Cocos much-put-upon assistant Kira Watanabe and her daughter, Kimi Finster|Kimi, who both hail from Japan, but are now living in France. Kira works as Cocos assistant and helps her to win Chas affections. Meanwhile, Spike gets lost in the streets of Paris and falls in love with a stray poodle named Fifi.

Kira tells the babies the in-universe origins of Reptar, explaining he was a feared angry monster until a princess revealed his gentler side to make the frightened humans like him. Chuckie decides the princess should be his new mother, and is aided by his friends to reach an animatronic replica of the princess in the park, but they are stopped by Cocos ninja security guards. At the shows premiere, Angelica informs Coco of Chuckies wish, so Coco sneaks backstage and takes the spotlight as the princess, luring Chuckie into her arms to make her seem wonderful with children. Chas is thrilled, deciding she would make an excellent mother and decides on the spot to marry her, much to everyones surprise, including his friends.
 Notre Dame Notre Dame proves to be quite dreadful, with Coco forcing Chas to go through with the wedding despite Chuckies absence, and rushing the Archbishop of Paris until she completely loses her temper and throws the Bible at him. 

Chuckie crashes the wedding, and Coco then pretends to be happy to see Chuckie but just then Jean-Claude bursts in and accidentally reveals Cocos true nature by announcing that her kidnapping plot had failed. Seeing Coco for the evil liar she really is, Chas angrily calls the wedding off. Angelica divulges Cocos plan to Yamaguchi, who is also in attendance, and the former president fires Coco from EuroReptarland. When Coco tries to leave, she realizes the babies are on her wedding train and angry yanks them off in front of everyone. Angelica angrily tells Coco that only she can do that and, as a humiliated Coco leaves the church, Angelica steps on the wedding dress, ripping it revealing Cocos underwear. Spike chases the humiliated Coco from the cathedral with Jean-Claude in tow. Kira arrives at the church after having been thrown out of the wedding car hours earlier and professes her love to Chas.

Chas and Kira fall in love and get married upon returning to America. Spikes new girlfriend, Fifi, is adopted by the Finster family. Chuckie gets Kira as a new mother, and Kimi as a new sister. The film ends with a cake fight between the characters and their families.

==Cast==
 
* Elizabeth Daily|E.G. Daily as Tommy Pickles, Chuckies best friend; he is the courageous and adventurous leader of the Rugrats.
* Tara Strong as Dil Pickles, Tommys younger brother.
* Cheryl Chase as Angelica Pickles, Tommy and Dils bratty older cousin.
* Christine Cavanaugh as Chuckie Finster, the most cowardly of the Rugrats. His goal is to find a new mother, after his real mother died shortly after he was born. fraternal twin brother and sister Rugrats.
** Soucie also voices Betty DeVille, the twins mother. Michael Bell as Chas Finster, Chuckies widower father.
** Bell also voices Drew Pickles, Angelicas father and Stus older brother. Kimi Watanabe, Kiras naive and fearless daughter who becomes Chuckies stepsister.
* Susan Sarandon as Coco LaBouche, the cruel and child-hating director of EuroReptarland. She only wants to marry Chas in order to get a promotion.
* John Lithgow as Jean-Claude, Cocos partner-in-crime. Jack Riley as Stu Pickles, Tommy and Dils inventor father; he is summoned to Paris to fix his malfunctioning Reptar robot.
* Melanie Chartoff as Didi Pickles, Tommy and Dils mother and Stus wife.
* Julia Kato as Kira Watanabe, Cocos former assistant who marries Chas and becomes Chuckies stepmother.
* Cree Summer as Susie Carmichael, Angelicas friend.
* Tress MacNeille as Charlotte Pickles, Angelicas workaholic mother. Phil Proctor as Howard DeVille, Bettys nervous husband Phil and Lils father
* Joe Alaskey as Grandpa Lou Pickles, Tommy, Dil and Angelicas grandfather and Stu and Drews father.
* Casey Kasem as DJ Mako as Mr. Yamaguchi, Cocos boss.
* Tim Curry, Kevin Michael Richardson, and Billy West as the three Sumo wrestlers/singers
* Debbie Reynolds as Lulu Pickles, Grandpa Lous second wife and the stepmother of Stu and Drew.
* Dan Castellaneta as Archbishop Jean-Marie Lustiger of Paris
* Roger Rose as the Finster wedding DJ
* Lisa McClowry as The Princess
* Charlie Adler as the French police officer
 

==Soundtrack==
{{Infobox album
| Name        = Rugrats in Paris: Music from the Motion Picture
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       =
| Cover size  = 250
| Released    =  
| Recorded    = 1999 - January 2000 hip hop, pop
| Length      = 50:55
| Label       = Maverick Records Nick Records
| Chronology = Rugrats soundtrack
|  Last album  =   (1998)
|  This album  = Rugrats in Paris: Music from the Motion Picturue (2000)
|  Next album  =   (2003) Misc = {{Singles
 | Name            = Rugrats in Paris: Music from the Motion Picture
 | Type         = soundtrack
 | Single 1      = Who Let the Dogs Out?
 | Single 1 date = July 25, 2000
 | Single 2      = My Getaway
 | Single 2 date = November 5, 2000
}}}}{{Album ratings|title=Soundtrack
| rev1      = Allmusic
| rev1Score =    }}

A soundtrack for the film, titled "Rugrats in Paris: The Movie: Music from the Motion Picture" was released on November 7, 2000 on  .

{{track listing
| writing_credits = no
| extra_column    = Artist(s)
| title1          = My Getaway
| extra1          = Tionne Watkins|T-Boz
| length1         = 3:50
| title2          = You Dont Stand a Chance Amanda
| length2         = 3:44
| title3          = Life Is a Party
| extra3          = Aaron Carter
| length3         = 3:26
| title4          = Who Let the Dogs Out?
| extra4          = Baha Men
| length4         = 3:18
| title5          = Final Heartbreak
| extra5          = Jessica Simpson
| length5         = 3:42
| title6          = When You Love
| extra6          = Sinéad OConnor
| length6         = 5:18
| title7          = Im Telling You This
| extra7          = No Authority
| length7         = 4:08
| title8          = These Boots Are Made for Walkin
| extra8          = Geri Halliwell
| length8         = 3:03
| title9         = Chuckie Chan (Martial Arts Expert of Reptarland)
| extra9         = Isaac Hayes & Alex Brown
| length9        = 4:19
| title10         = LHistoire dune fée, cest...
| extra10         = Mylène Farmer
| length10        = 5:12
| title11         = I Want a Mom That Will Last Forever
| extra11         = Cyndi Lauper
| length11        = 3:47
| title12         = Excuse My French
| extra12         = 2Be3
| length12        = 3:03 Bad Girls
| extra13         = Cheryl Chase & The Sumos
| length13        = 4:05
}}
{{track listing
| writing_credits = no
| headline        = Bonus enhanced track on enhanced CD
| extra_column    = Artist(s)
| total_length    = 50:55
| title14         = Jazzy Rugrat Love
| note14          = Theme from Rugrats in Paris
| extra14         = Teena Marie
| length14        = 5:07
}}

==Reception==

===Box office===
The film grossed $103,291,131 worldwide out of its $30 million budget, tripling the budget in box office results. This film was released on November 17, 2000 to $22,718,184 for an average of $7,743 from 2,934 venues.  

===Critical reception===
Rotten Tomatoes gives the film a 75% rating from critics based on 73 reviews, with the critical consensus: When the Rugrats go to Paris, the result is Nickelodeon-style fun. The plot is effectively character-driven, and features catchy songs and great celebrity voice-acting.    Metacritic gives a film a 62% based on 25 reviews, indicating "Generally favorable reviews". 

==Release==
Rugrats in Paris: The Movie was released in theatres on November 17, 2000.

===Home media===
Paramount Home Video released the film on VHS and DVD on March 27, 2001. In 2009, Paramount released the film via iTunes and the PlayStation Store.   

On March 15, 2011, Rugrats in Paris, as well as The Rugrats Movie and Rugrats Go Wild, were re-released on a three-disc trilogy collection.

As of October 2014, the film is currently available to stream on Netflix.

==Sequel==
A third installment entitled Rugrats Go Wild was released on June 13, 2003 featuring the characters from The Wild Thornberrys.

==References==
 

==External links==
 
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 