A Girl... and a Million
{{Infobox film
 | name =A Girl... and a Million
 | image = A Girl... and a Million.jpg
 | caption =
 | director = Luciano Salce
 | writer = Alberto Bevilacqua Goffredo Parise  Carlo Romano Luciano Vincenzoni 
 | starring =  
 | music = Ennio Morricone
 | cinematography =  Erico Menczer
 | editing =  
 | producer =  
 | language = Italian 
 }}
La cuccagna, internationally released as A Girl... and a Million, is a 1962 Italian drama film directed by Luciano Salce.    

In 2008 it was restored and shown as part of the retrospective "Questi fantasmi: Cinema italiano ritrovato" at the 65th Venice International Film Festival. 

== Cast ==

*Donatella Turri: Rossella Rubinacci
*Luigi Tenco: Giuliano
*Umberto DOrsi: Dott. Giuseppe Visonà
*Liù Bosisio: Diana
*Gianni Dei: Natalino 
*Piero Gerlini: Gallerys Owner 
*Enzo Petito:  Galliano Rubinace, Rossellas father 
*Corrado Olmi:  Visonàs friend 
*Jean Rougeul: Cementi
*Luciano Salce: the Colonel
*Ugo Tognazzi: a driver (cameo)

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 