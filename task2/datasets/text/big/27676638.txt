At the Stroke of Nine
{{Infobox film
| name           = At the Stroke of Nine
| image          = "At_the_Stroke_of_Nine"_(1957).jpg
| image_size     = 
| caption        = Original British lobby card
| director       = Lance Comfort
| producer       = Harry Booth   Michael Deeley   Jon Penington
| writer         = Harry Booth   Brian Clemens    Michael Deeley   Jon Penington 
| narrator       =  Stephen Murray   Patrick Barr   Dermot Walsh
| music          = Edwin Astley Gerald Gibbs
| editing        = 
| studio         = Towers of London Productions Grand National (UK) 
| released       = June 1957
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English
| budget         = £20,000 Michael Deeley, Blade Runners, Deer Hunters and Blowing the Bloody Doors Off: My Life in Cult Movies, Pegasus Books, 2009 p 20 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Stephen Murray, Patrick Barr and Dermot Walsh.  The plot follows a high-flying female journalist who is kidnapped by a madman. He forces her to write articles about him and threatens to kill her. 

==Cast==
* Patricia Dainton - Sally Bryant Stephen Murray - Stephen Garrett
* Patrick Barr - Frank
* Dermot Walsh - MacDonnell
* Clifford Evans - Inspector Hudgell Leonard White - Thompson
* Reginald Green - Toby
* Alexander Doré - Carter
* Leonard Sharp - News Vendor
* Robert Hartley - Westcott Frank Atkinson - Porter William Moore - Campion
* Marianne Stone - Secretary
* George Lee - Young Reporter
* William Hepper - Clerk
* Donald B. Edwards - Gray

==Critical reception==
TV Guide wrote, "the frantic search for the loonie by police offers some interesting scenes with fair suspense." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 