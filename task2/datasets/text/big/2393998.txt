Street Fighter Alpha: The Animation
 
{{Infobox film
| name           = Street Fighter Alpha: The Animation
| image          = SFAMovie.jpg
| image size     = 200px
| alt            = 
| caption        = DVD cover
| director       = Shigeyasu Yamauchi
| producer       = Kaoru Mfaume
| writer         = 
| screenplay     = 
| story          =
| based on       =  
| narrator       = 
| starring       = Kane Kosugi Kazuya Ichijō Reiko Kiuchi Daiki Nakamura Hisao Egawa Yumi Toma Chiaki Osawa Ai Orikasa Tomomichi Nishimura Miki Nagasawa Bin Shimada
| music          = 
| cinematography = 
| editing        = 
| studio         = Group TAC
| distributor    = 
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 OVA film directed by English adaptation was produced by Manga Entertainment and released in 2000.

The film commemorates the tenth anniversary of the arcade release of the original   or Street Fighter II V, but an independent installment, although a handful of voice actors from previous adaptations reprised their roles for the English version of the movie.

==Plot== Ryu is still contemplating the death of his master, Gouken, while at the same time experiencing trouble with the  , an evil energy which Goukens brother, Akuma (Street Fighter)|Akuma, succumbed to a long time ago. While in the city, Ryu fights off a few agents working for Shadaloo, gaining the attention of Interpol agent Chun-Li, who does some background research and realizes Ryu is the man who beat Sagat, the Muay Thai Champion, many years ago. Also catching sight of Ryu is aspiring martial artist Sakura (Street Fighter)|Sakura, who becomes fascinated with Ryu and vows to track him down and become his student.

In Japan, Ryu is approached by a mysterious woman, Rose (Street Fighter)|Rose, who questions him briefly about his hold over the Dark Hadou and reason for fighting. While visiting Goukens grave, Ryu meets his old friend, Ken (Street Fighter)|Ken. They are both approached by a young boy named Shun, who claims that he is Ryus long-lost brother. According to Shun, their mother raised Shun in Brazil until her recent death, and she sent Shun to find Ryu before she died. Ken is skeptical, but Ryu decides to take Shun in and notices the boys potential as a fighter.One night Ryu succumbs to the Dark Hadou nearly killing Ken. Fortunately Ken punches him in the stomach. Ryu then returns to normal. Later that night Ken talks to Ryu about his Dark Hadou. Ryu instructs Ken to kill him if hes ever completely possessed by the Dark Hadou. Ken is reluctant at first, but then agrees.

Ken and Shun decide to enter an underground fighting tournament, but Ryu declines. On the way, they are harassed by some street thugs. Ryu and Shun fight them off effortlessly, but Ryu also notices a rather violent and sadistic streak in Shun, whom he has to punch to stop him killing one of the thugs. Ken, meanwhile, finds Sakura in a bar and agrees to take her to Ryu, although he is too late to enter the tournament, much to his chagrin. Inside the building, Ryu is found by Chun-Li, who managed to locate him. In the tournament, watched by its enigmatic organiser, Dr. Sadler, Shun is pitted against the brutal wrestler, Zangief. Shun briefly manages to pummel Zangief with his superior speed, but the Dark Hadou catches up with Shun, distracting him long enough to receive a brutal beating from Zangief. Ryu steps in and briefly fights Zangief, but he too is overcome by the Dark Hadou and he almost kills Zangief with a Dark Hadouken, which misses Zangief but causes the building to start collapsing. As Ken takes an injured Sakura to safety, Ryu is confronted by a huge man, Rosanov, who proves to be more than a match for both Ryu and Chun-Li combined. Just as Rosanov prepares to knock Ryu unconscious, Shun steps in and takes the blast. Ryu succumbs to his rage and obliterates Rosanov with a Shinku Hadouken. However, while Ryu is distracted, Shun is abducted by Shadaloo agents.

By now, Ryu has completely lost the will to fight. However, Rose appears before him once again, compelling him to save Shun from Sadler and himself. Ryu accepts, but first goes to see Akuma, accompanied by Chun-Li, in his secluded home in the mountains. Once there, Akuma, under the belief that Ryu has come to challenge him at last, attempts to goad him into succumbing to the Dark Hadou, but Ryu refuses, and Akuma orders him to leave. Ryu manages to ask Akuma if he is Shuns father, but Akuma denies it. Ken arrives and spars with Ryu. After they spar Ryu reminds Ken to kill him if hes taken over by the Dark Hadou. After this, Ryu decides to travel to Sadlers hideout and rescue Shun himself, accompanied by Ken, Chun-Li, Guy, Dan, Dhalsim, Birdie, Adon, Rolento and Sodom.

As the other fighters battle outside Sadlers lab in the arena, Ryu, Ken and Chun-Li sneak inside to find Shun. The first winner is Birdie, and he is led into a trap which will strip him of his fighting potential, though he is freed by Ken and Chun-Li, who break the other fighters out. However, they are confronted by a very alive Rosanov in the corridor, who makes quick work of the three of them despite their efforts, blowing that part of the hideout to smithereens. Ryu arrives on the scene and fights Rosanov again, realizing that Rosanov is actually an android, and this one is really Shun, who was working for Sadler and luring Ryu into a trap. Shun/Rosanov attempts to goad Ryu into using the Dark Hadou, since he is connected to Sadler and every blow which is landed on Shun/Rosanov will increase Sadlers fighting potential and make him stronger. Ryu refuses, but is eventually pushed over the edge when Shun/Rosanov beats Ken mercilessly and Ken uses the last of his strength for a Shoryuken which does nothing more than dislocate the androids jaw. Finally, Ryu succumbs and fires a Dark Hadouken at the android, freeing Shun and destroying Rosanov once and for all.

With this sudden increase in power, Sadler bursts out of his lab and finally emerges on the battlefield. With Ryu exhausted, Sadler easily takes the upper hand and pummels Ryu around. Ryu prepares to succumb to the Dark Hadou once again, even though Ken and Shun implore him not to. Sadler overwhelms Ryu with a Hadouken, which nearly kills him. Though only Ryu can see her, Rose intervenes, informing Ryu that "you havent drawn the death card yet". Inspired, Ryu snaps out of the vision and returns to his normal state and fires a normal, Shinku Hadouken directly into a distracted Sadler, who crumbles into dust while openly wondering how Ryu could still surpass him. Shuns injuries, however, are too severe, and as he lies dying, he reveals that he lied to Ryu about being his brother and worked with Sadler so as to raise money for his mother, who still died. Ryu then tells Shun that he is still his brother. As Shun succumbs, Ryu vows never to use the Dark Hadou ever again.

The movies final scenes depict the fighters returning to their everyday lives. Sakura, now fully recovered, decides to continue training so that one day she may be able to fight Ryu. Chun Li is seen beating some other thugs, and Ken is seen fighting in another street fighter tournament. In a distant area near the sea. Ryu is seen accepting Akumas challenge, but informs him that he will not use the Dark Hadou. The two then jump into the air with their battle cries ready to fight.

==Characters==
;Main
{| class="wikitable"
! Character !! Japanese VA !! English VA 
|- Ryu (Street Ryu || Kane Kosugi || Skip Stellrecht
|-  Ken (Street Ken || Kazuya Ichijō || Steve Blum 
|- Shun || Reiko Kiuchi || Mona Marshall
|-
|Chun-Li || Yumi Toma || Lia Sargent
|-
|Dr. Sadler || Daiki Nakamura || Peter Lurie
|-
|Rosanov/Sadlerbot  || Hisao Egawa || Tom Wyner
|- Akuma (Street Akuma ||Tomomichi Nishimura || Keith Burgess
|}

;Secondary
{| class="wikitable"
! Character !! Japanese VA !! English VA 
|- Sakura Kasugano|Sakura || Chiaki Osawa || Michelle Ruff
|- Rose (Street Rose || Ai Orikasa || Carolyn Hennesy
|- Zangief || Hidenari Ugaki || Joe Romersa
|- Adon || Wataru Takagi || R. Martin Klein
|- Vega (Street Vega || Kazuyuki Ishikawa || Richard Cansino
|- Birdie || Ryûzaburô Ôtomo || Joe Romersa
|- Dan || Kazuyuki Ishikawa || Bob Papenbrook
|- Kei || Miki Nagasawa || Sherry Lynn
|- Sodom || Masao Fuda ||
|- Wallace || Bin Shimada ||
|}

==Reception==
While not as well received as Street Fighter II: The Animated Movie, Street Fighter Alpha garnered a mixed to positive response, earning a 58% approval rating on Rotten Tomatoes.  

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 

 