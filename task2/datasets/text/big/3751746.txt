The Wings of the Dove (1997 film)
{{Infobox film
|  name           =The Wings of the Dove
|  image          =Wings of the dove ver1.jpg
|  caption        =Original poster
|  screenplay     =Hossein Amini
|  novel          = 
|  starring       =Helena Bonham Carter Alison Elliott Linus Roache
|  director       =Iain Softley
|  producer       =Bob Weinstein Harvey Weinstein
|  music          =Ed Shearmur
|  cinematography =Eduardo Serra Tariq Anwar
|  distributor    =Miramax Films
|  released       = 
|  runtime        =103 minutes
|  country        =United Kingdom United States
|  language       =English
|  budget         =
|  gross          =$13,692,848
}} 1902 novel of the same name by Henry James. The film was nominated for four Academy Awards and five BAFTAs, recognizing Bonham Carters performance, the screenplay, costume design and the cinematography.

==Plot== muckraking journalism|journalist Merton Densher (Linus Roache), wants to marry her, but is skeptical of Kates intentions. despite her pleas for him to wait as she tries to find a way out of her dilemma.

One day, Kate is introduced by Lord Mark to the wealthy and outgoing American orphan and heiress Milly Theale (Alison Elliott), who is on an extended trip through Europe with her best friend Susan Stringham (Elizabeth McGovern), and the cynical Kate is captivated by Millys beauty, openness, and amusement at the conventions of society. Milly invites Kate to accompany her and Susan to Venice. Prior to their departure, Milly meets Merton, with whom she is smitten, and she asks him to join them as well.

Lord Mark secretly reveals to Kate that Milly is terminally ill and that he desires Kate but has to marry Milly to avoid losing his estates. Knowing Milly is repulsed by Lord Marks crudity, she decides to emulate his scheme and persuade Merton to woo Milly; she fully expects Millys generous nature will lead her to include Merton in her will, making him wealthy enough to satisfy her aunt and allow them to marry. He despises her scheme but cant resist the opportunity to be in Venice with Kate. Kate becomes so jealous of the genuine affection growing between Merton and Milly that she lures him away one night, abandoning Milly in a crowd. Milly confronts her the next morning and Kate realizes she must leave, without warning Merton, if her scheme is to succeed. What neither she nor Merton anticipate is how things will change when she is gone.

Some months later, following Millys death in Venice, Kate comes to Mertons flat. She asks why he has not come to see her and finds a letter, telling Merton that Milly did indeed bequeath a sizable portion of her estate to him. He tells Kate that he will not take the money, and she must marry him without it, if she will. She agrees, and they make love. But afterwards, Kate asks him to tell her that he is not still in love with Milly, or his memory of her, and he cannot. So Merton returns to Venice, alone.

==Production notes== Old Royal Dover Street and Knightsbridge tube stations.

Locations in Venice include St. Marks Square (Venice)|St. Marks Square and the Palazzo Barbaro. 

The film grossed $13,718,385 in the US. 

==Cast==
* Helena Bonham Carter as Kate Croy
* Linus Roache as Merton Densher
* Alison Elliott as Milly Theale
* Elizabeth McGovern as Susan Stringham
* Charlotte Rampling as Aunt Maude
* Michael Gambon as Lionel Croy
* Alex Jennings as Lord Mark

==Critical reception==
In his review in the New York Times, Stephen Holden called the film a "spellbinding screen adaptation   succeeds where virtually every other film translation of a James novel has stumbled . . . This magnificent film conveys an intimation of what values count the most, of what really matters, but it is also far too intelligent and sympathetic to human frailty to spell them out. You feel them most of all in the characters unbridgeable silences."  
 Washington Square The Portrait of a Lady, two superior Henry James novels that came across as stiff and deliberate in recent film translations. This is a breakthrough for Softley, whose earlier films Backbeat (film)|Backbeat and Hackers (film)|Hackers only hinted at the style and complexity he displays here, and a wonderful showcase for Roache, Elliott and Bonham Carter, who gives her best performance yet."  

In  s Alison Elliott, who, with her beatific charm and Mona Lisa smile, does one of the most difficult things an actress can — she brings goodness itself to life."  

David Stratton of Variety (magazine)|Variety stated the film "gives Helena Bonham Carter one of her best opportunities in a while, one which she seizes with relish, looking vibrant and totally convincing in her pivotal role . . . The Wings of the Dove may be typical of the school of British literary cinema, but Softleys handling of several key elements, including an unusually frank love scene in the later stages, is always inventive. Production values are of the highest standard."  

==Awards and nominations==
{| class="wikitable"
|-
! Award !! Date of ceremony !! Category !! Nominee !! Result
|- March 23, Best Actress in a Leading Role || Helena Bonham Carter || rowspan=4  
|- Best Adapted Screenplay || Hossein Amini
|- Best Cinematography || Eduardo Serra
|- Best Costume Sandy Powell
|- April 18, Best Cinematography || Eduardo Serra || rowspan=2  
|- Best Makeup and Hair || Sallie Jaye & Jan Archibald
|- Best Actress in a Leading Role || Helena Bonham Carter || rowspan=3  
|- Best Adapted Screenplay || Hossein Amini
|- Best Costume Design || Sandy Powell
|- Boston Society December 14, Best Actress || Helena Bonham Carter ||  
|- Best Supporting Actress || Alison Elliott || rowspan=2  
|-
| British Society of Cinematographers || November 29, 1997 || Best Cinematography || Eduardo Serra
|- Broadcast Film January 20, Best Actress || Helena Bonham Carter ||  
|- Best Picture || || rowspan=3  
|- Chicago Film March 1, Best Actress || Helena Bonham Carter
|- Gold Hugo || October 9–19, 1997 || Best Film || Iain Softley
|- January 1998 Best Supporting Actress || Allison Elliott
|- Best Actress || rowspan=4 | Helena Bonham Carter
|- January 18, Best Performance by an Actress in a Motion Picture - Drama ||  
|- Kansas City Best Actress || rowspan=5  
|-
| rowspan=2 | Sierra Award || rowspan=2 | January 1998 || Best Actress
|-
| Best Supporting Actress || Allison Elliott
|- March 4, 1999 || British Actress of the Year || rowspan=2 | Helena Bonham Carter
|- LAFCA Award January 15, Best Actress
|- Golden Reel Award || March 21, 1998 || Best Sound Editing - Foreign Feature || ||  
|- NBR Award December 8, Best Actress || Helena Bonham Carter || rowspan=2  
|-
|   ||
|- National Society January 3, Best Actress || rowspan=3 | Helena Bonham Carter || rowspan=7  
|- New York January 4, Best Actress
|- Online Film January 11, Best Actress
|-
| rowspan=4 |   || Hossein Amini
|- Best Actress in a Motion Picture || Helena Bonham Carter
|- Best Art John Beard
|- Best Costume Design || Sandy Powell
|-
| Society of Texas Film Critics Awards || December 29, 1997 || Best Actress || rowspan=2 | Helena Bonham Carter ||  
|- March 8, Best Performance by a Female Actor in a Leading Role || rowspan=2  
|- Best Performance by a Female Actor in a Supporting Role || Allison Elliott
|- Southeastern Film January 5, Best Actress || rowspan=2 | Helena Bonham Carter || rowspan=2  
|- TFCA Award January 13, Best Actress
|-
| USC Scripter Award || March 8, 1998 || || Henry James  , Hossein Amini   || rowspan=2  
|- February 21, Best Adapted Screenplay || Hossein Amini
|}

==See also==
* List of American films of 1997

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 