C.O.D. (film)
{{Infobox film
| name           = C.O.D.
| image          = COD Film.jpg
| caption        = Garry Marsh in C.O.D.
| director       = Michael Powell Jerome Jackson
| writer         = Philip MacDonald Ralph Smart
| starring       = Garry Marsh Hope Davey Arthur Stratton
| music          = 
| cinematography = Geoffrey Faithfull
| editing        =  United Artists Corporation
| released       =  
| runtime        = 64 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
C.O.D. is a 1932 British crime film directed by Michael Powell and starring Garry Marsh, Arthur Stratton and Sybil Grove. A man assists a woman to dispose of the body of her stepfather. 

The film has been declared "Missing, Believed Lost" by the British Film Institute. 

==Cast==
* Garry Marsh - Peter Craven
* Arthur Stratton - Mr Briggs
* Hope Davey - Frances
* Sybil Grove - Mrs Briggs
* Roland Culver - Edward
* Peter Gawthorne - Detective
* Cecil Ramage - Vyner
* Bruce Belfrage - Philip

==References==
 

===Bibliography===
 
* Chibnal, Steve. Quota Quickies : The Birth of the British B Film. London: BFI, 2007. ISBN 1-84457-155-6
* Powell, Michael. A Life in Movies: An Autobiography. London: Heinemann, 1986. ISBN 0-434-59945-X.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 
 