Ritmos del Caribe
{{Infobox film
| name           = Ritmos del Caribe
| image          = 
| caption        =
| director       = Juan José Ortega
| producer       = Octavio Gómez Castro Juan José Ortega
| writer         = Neftalí Beltrán Juan José Ortega
| starring       = Amalia Aguilar Rafael Baledón Susana Guizar Rita Montaner
| music          = Gonzalo Curiel
| cinematography = Jorge Stahl Jr.
| editing        = 
| distributor    = Compañía Cinematográfica Mexicana
| released       = December 6, 1950(México) 
| runtime        = 93 min 
| country        = Mexico
| language       = Spanish
| budget         =
| gross          =
}}
Ritmos del Caribe (Caribbean Rhythms) is a Mexican musical film directed by Juan José Ortega. It was released in 1950 and starring Amalia Aguilar and Rafael Baledón.

==Plot==
A Cuban rumbera undergoes an ordeal to fall in love with a Mexican doctor. The problem between the two is that the man is married.

==Cast==

 

== Reparto ==
* Amalia Aguilar
* Rafael Baledón
* Susana Guízar
* Rita Montaner
* Roberto Cobo
* La Sonora Matancera
* Los Panchos

==Reviews== Daniel Santos, Bienvenido Granda, La Sonora Matancera, Los Panchos, Rita Montaner and the explosive presence of Amalia, who brings a sensuality and rhythm out of series.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 