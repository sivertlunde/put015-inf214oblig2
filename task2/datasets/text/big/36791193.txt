Our Mrs. McChesney
{{infobox film
| name           = Our Mrs. McChesney
| image          =
| imagesize      =
| caption        =
| director       = Ralph Ince
| producer       = Metro Pictures Maxwell Karger
| writer         = Edna Ferber (play Our Mrs. McChesney) George Hobart (play) Luther Reed (scenario)
| starring       = Ethel Barrymore
| music          =
| cinematography = William J. Black
| editing        =
| distributor    = Metro Pictures
| released       = August 18, 1918
| runtime        = 50 minutes; 5 reels
| country        = United States
| language       = Silent film English intertitles
}} lost  1918 silent film produced and distributed by Metro Pictures, directed by Ralph Ince, and based on the 1915 play by Edna Ferber which starred Ethel Barrymore. 

Barrymore reprised her role from the popular play, as did her fellow cast members Huntley Gordon and William H. St. James. Wilfred Lytell was a brother of Bert Lytell and Lucille Lee Stewart was a sister of Anita Stewart.  Ince was married to Lucille Lee Stewart, sister of well-known actress Anita Stewart.

==Cast==
*Ethel Barrymore - Emma McChesney
*Huntley Gordon - T. A. Buck Jr.
*Wilfred Lytell - Jack McChesney
*Lucille Lee Stewart - Vera Sherwood
*John Daly Murphy - Abel Fromkin
*Walter Percival - Beauty Blair
*William H. St. James - Fat Ed Myers
*Ricca Allen - Hattie Stitch
*George S. Trimble - Joe Greenbaum
*Sammy Cooper - Izzy Greenbaum
*Fred Walters - Sam Harrison

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 
* 
*sequences from the film:  (still incorrectly listed as The Beloved Rogue),... 

 
 
 
 
 
 
 
 
 
 


 