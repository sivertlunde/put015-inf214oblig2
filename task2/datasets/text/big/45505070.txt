Yahaan Sabki Lagi Hai
{{Infobox film
| name = Yahaan Sabki Lagi Hai 
| image = 
| alt =
| director = Tina A. Bose, Cyrus R. Khambhata
| presented by =  Karan Raj Kanwar (INDERJIT FILMS) 
| producer = Tina A. Bose, Cyrus R. Khambhata
| writer = Tina A. Bose, Cyrus R. Khambhata
| starring ={{Plain list|
}}
* Bharat - Varun Thakur
* Kesang - Eden Shyodhi
* Chandu - Heerok Das
| music = Pankaj Awasthi
| cinematography =
| editing = Cyrus Khambata
| studio = VIBRANT WORKS
| distributor = Karan Raj Kanwar (INDERJIT FILMS)
| released = Scheduled for  
| runtime =
| country = India
| language = Hindi
| budget =
| gross =
}}
Yahaan Sabki Lagi Hai  is a multi-lingual film. It is an allegory on the journey of life, especially at times when one feels victimized or shortchanged, set in the milieu of Indian-English-speaking urban youth. 

The film looks at the way we casually live in a class system built on blatant economic and social disparities, the way we want to be fooled by our belief systems, the way we validate our tastes and are also very protective of them. By exploring a counter-point for every point, the film subtly questions not only our material value system but also our emotional value system. 

The film is a comedy at heart, but it also has a melancholic after-taste, bordering on the philosophical, provoking thought about our times and condition. 

== Cast ==
* Bharat - Varun Thakur
* Kesang - Eden Shyodhi
* Chandu - Heerok Das



==References==
 

== External links ==
* 
*http://www.bollywoodhungama.com/moviemicro/cast/id/2924797 Yahaan Sabki Lagi Hai] at IMSLV
 
 
 
 


 