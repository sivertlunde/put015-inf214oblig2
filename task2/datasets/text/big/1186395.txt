The Beguiled
 
 
{{Infobox film
| name = The Beguiled
| image = Beguiled43.jpg
| caption = Film poster by Bob Peak
| director = Don Siegel
| producer = Don Siegel
| writer =   Thomas P. Cullinan (novel) Albert Maltz (screenplay) Irene Kamp (screenplay)
| starring =Clint Eastwood Geraldine Page Elizabeth Hartman Jo Ann Harris
| music = Lalo Schifrin
| cinematography = Bruce Surtees
| editing = Carl Pingitore
| distributor = Universal Pictures
| released =  
| runtime = 105 min.
| language = English
| country = United States
| budget =
}} Escape from Alcatraz (1979).

==Plot==
Close to the end of the American Civil War, injured Yankee soldier John McBurney is rescued from the verge of death by a twelve-year-old girl from an all-girl boarding school in Louisiana. At first the all-female staff and pupils are scared, but as John starts to recover, he charms them one by one and the sexually repressed atmosphere becomes filled with jealousy and deceit, with the two teachers and some of the girls clearly attracted to him.

After rejecting the headmistress for a younger girl, McBurney gets his comeuppance in the form of some painful Freudian symbolism — he falls down the stairs. Eventually his leg is amputated by the headmistress, ostensibly to avoid gangrene - the film leaves it unclear whether this is her genuine concern or if she is prompted consciously or subconsciously by revenge. After going on a rampage that scares all of the women, he reforms and announces his intention to marry one of the teachers, but it is too late; he has alienated the youngest girl, who first found him, by killing her pet turtle after throwing it aside in a drunken rage. In response, she is coached to pick mushrooms that the headmistress and girls use to poison him.

==Cast==
* Clint Eastwood as Corporal John McBee McBurney
* Geraldine Page as Martha Farnsworth
* Elizabeth Hartman as Edwina Dabney
* Jo Ann Harris as Carol
* Darleen Carr as Doris
* Mae Mercer as Hallie
* Pamelyn Ferdin as Amelia Amy Melody Thomas as Abigail
* Peggy Drier as Lizzie
* Pattye Mattick as Janie

==Production==
Eastwood was given a copy of the 1966 novel by producer Jennings Lang, and was engrossed throughout the night in reading it. McGilligan (1999), p.185  This was the first of several films where Eastwood has agreed to storylines where he is the center of female attention, including minors.  Eastwood considered the film as "an opportunity to play true emotions and not totally operatic and not lighting cannons with cigars". Hughes, p.98  Albert Maltz, who had worked on Two Mules for Sister Sara was brought in to draft the script, but disagreements in the end led to a revision of the script by Claude Traverse, who although uncredited, led to Maltz being credited under a pseudonym. McGilligan (1999), p.187  Maltz had originally written a script with a happy ending, in which Eastwoods character and the girl live happily ever after. Both Eastwood and director Don Siegel felt that an ending more faithful to that of the book would be a stronger anti-war statement, however, and the ending was altered so that Eastwoods character would be killed.  The film, according to Siegel, deals with the themes of sex, violence and vengeance and was based around, "the basic desire of women to castrate men". McGilligan (1999), p.186 

Jeanne Moreau was considered for the role of the domineering headmistress Martha Farnsworth, but in the end the role went to acclaimed Broadway actress Geraldine Page, and actresses Elizabeth Hartman, Jo Ann Harris, Darlene Carr, Mae Mercer, and Pamelyn Ferdin were cast in supporting roles.

Universal initially wanted Siegel to film at a studio at Disney Studios Ranch, but Siegel preferred to have it filmed at an actual estate near Baton Rouge, Louisiana. Portions of the interiors were still filmed at Universal Studios. Filming started in April 1970 and lasted ten weeks. Hughes, p.96 
 The Eiger Sanction, which he directed as well as starred in. He would not work with Universal again until 2008s Changeling (film)|Changeling.

Eastwood said of his role in The Beguiled,

 

==Reception==
Made right before Dirty Harry, this was a bold early attempt by Eastwood to play against type. It was not a hit, likely due to uncertainty on Universals part concerning how to market it, eventually leading them to advertise the film as a hothouse melodrama: "One man...seven women...in a strange house!" "His love... or his life..." According to Eastwood and Jennings Lang, the film, aside from being poorly publicized, flopped due to Eastwood being "emasculated in the film".  The films poster for example, shows him with a gun, suggesting an action movie. Eastwood does not shoot anyone in the movie in contemporaneous time (but does in recall of his activities as a soldier).

The film received major recognition in France, and was proposed by Pierre Rissient to the Cannes Film Festival, and while agreed to by Eastwood and Siegel, the producers declined. McGilligan (1999), p.189  It would be widely screened in France later and is considered one of Eastwoods finest works by the French. McGilligan (1999), p.190  Although the film reached number two on Varietys chart of top grossing films, it was poorly marketed and in the end grossed less than $1 million, earning less than a fourth of what Sweet Sweetbacks Baadasssss Song did at the same time and falling to below 50 in the charts within two weeks of release.  The film has since grown in acclaim in the United States and currently has a 91% rating on Rotten Tomatoes.

==References==
 

==Bibliography==
* Hirsch, Foster (1971-72). "The Beguilded: Southern Gothic revived." Film Heritage, 7, 15-20.
* Kay, Karyn. (1976) "The Beguiled: Gothic Misogyny." Velvet Light Trap, 16, 32-33.
*  
*  
* Tumanov, Vladimir (2013).     .

==External links==
*  
*   at Rotten Tomatoes
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 