Who's Harry Crumb?
{{Infobox film
| name=Whos Harry Crumb?
| image=Whos harry crumb.jpg
| director=Paul Flaherty
| producer=Arnon Milchan George W. Perkins  (co-producer)  John Candy (executive producer)
| writer=Robert Conte  Peter Martin Wortmann
| starring=John Candy Jeffrey Jones Annie Potts Tim Thomerson Barry Corbin Shawnee Smith
| music=Michel Colombier
| cinematography=Stephen M. Katz
| editing=Danford B. Greene
| studio=Frostbacks NBC Productions
| distributor=TriStar Pictures
| released= 
| runtime=88 min.
| language=English
| country=United States
| gross=$10,982,000
}}

Whos Harry Crumb? (1989 in film|1989) is a comedy-mystery film featuring John Candy as the title character.

Paul Flaherty directed the film, which co-stars Annie Potts, Jeffrey Jones and Shawnee Smith. An uncredited cameo appearance is made by James Belushi.

The story revolves around the often incompetent, sometimes genius, private investigator Harry Crumb in his search for a kidnapping victim.

==Plot== Beverly Hills, fashion model Jennifer Downing, the daughter of millionaire P.J. Downing, is kidnapped. Her father turns to a family friend, Eliot Draisen, who is president of the detective agency Crumb & Crumb, to investigate the case.

Eliot is reluctant to supply P.J. with one of his capable detectives because, as it turns out, Eliot himself is the organizer of the kidnapping. But to give the appearance of taking the investigation seriously, Eliot offers P.J. the services of Harry Crumb (John Candy|Candy), the last descendant of the agencys founders. Eliot talks up Harrys detecting skills to persuade P.J. that hes the right man for the job, but he secretly knows that Harry is incompetent and counts on this fact to allow him to slip right through the detectives fingers.
 Los Angeles (by bus) from an assignment in the firms Tulsa branch office (which he messed up, as usual). He immediately begins his investigation, assisted by P.J.s younger daughter, Nikki, who is considerably smarter than he is. Harrys tendency to mix fact with fantasy, however, does not prevent him from noticing that Nikkis stepmother, Helen Downing, is having an affair with her tennis coach Vince Barnes, and after several incidents and observations, the detective comes to the conclusion that she is behind the kidnapping. Helen is also the object of affection of Eliot Draisen, but all she is interested in is money; to this end, she tries to get rid of her husband on several occasions and even does her best&nbsp;&ndash; along with Barnes&nbsp;&ndash; to get the ransom for herself.

Despite several close calls, Eliot manages to outwit Harry and the police. He escapes to the airport, from where he intends to depart to Buenos Aires. But he makes the mistake of informing Helen of his plans; she and Barnes take the money and leave him, tied up and gagged, in a utility room at the airport. Harry and Nikki manage to trail the kidnapper to the airport, where the detective corners Helen and Barnes just as their plane prepares to take off. After Jennifer is freed, Harry and the police find Eliot in the side room. Fed up with Harrys dumb luck&nbsp;&ndash; and owing to a slight misunderstanding&nbsp;&ndash; Eliot confesses and is sent to prison.

With the Downings gratitude boosting his confidence, Harry takes over as the new president of his familys business, where a new case immediately piques his personal attention.

==Cast==
*John Candy as Harry Crumb
*Jeffrey Jones as Eliot Draisen
*Annie Potts as Helen Downing
*Tim Thomerson as Vince Barnes
*Barry Corbin as P.J. Downing
*Shawnee Smith as Nikki Downing
*Renee Coleman as Jennifer Downing
*Valri Bromfield as Det. Casey
*Doug Steckler as Dwayne

==External links==
*   at  
* 
*  
*   at the New York Times
*  

 
 
 
 
 
 
 
 
 
 