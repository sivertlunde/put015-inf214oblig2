Walk a Crooked Mile
{{Infobox film
| name           = Walk a Crooked Mile
| image          = Walk a Crooked Mile Lobby Card.jpg
| image_size     =
| alt            =
| caption        = Theatrical release lobby card Gordon Douglas
| producer       = {{plainlist|
* Edward Small
* Grant Whytock
}}
| screenplay     = George Bruce
| story          = Bertram Millhauser
| narrator       = Reed Hadley
| starring       = {{plainlist|
* Louis Hayward
* Dennis OKeefe
* Louise Allbritton
}}
| music          = Paul Sawtell
| cinematography = {{plainlist| Edward Colman
* George Robinson
}}
| editing        = James E. Newcom
| studio         = Edward Small Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
}} Gordon Douglas.  The drama features Louis Hayward, Dennis OKeefe and Louise Allbritton.

==Plot== FBI agent Dan OHara are on the case.

==Cast==
* Louis Hayward as Philip Scotty Grayson
* Dennis OKeefe as Daniel F. OHara
* Louise Allbritton as Dr. Toni Neva
* Carl Esmond as Dr. Ritter von Stolb
* Onslow Stevens as Igor Braun
* Raymond Burr as Krebs Art Baker as Dr. Frederick Townsend
* Lowell Gilmore as Dr. William Forrest
* Philip Van Zandt as Anton Radchek Charles Evans as Dr. Homer Allen
* Frank Ferguson as Carl Bemish
* Reed Hadley as Narrator

==Production==
The film was originally titled FBI vs Scotland Yard but this was changed at the request of J. Edgar Hoover. Dennis OKeefe Costar of Smalls Dark Page; Carmen, Wally Reunited
Scheuer, Philip K. Los Angeles Times (1923-Current File)   23 Aug 1948: 11. 

==Reception==
When the film was released, The New York Times film critic, Bosley Crowther, while giving the film mixed review, wrote well of the screenplay, "No use to speak of the action or the acting. Its strictly routine. But the plot is deliberately sensational." 

The staff at Variety (magazine)|Variety magazine gave the film a favorable review, writing, "Action swings to San Francisco and back to the southland, punching hard all the time under the knowledgeable direction of Gordon Douglas. On-the-site filming of locales adds authenticity.  George Bruce has loaded his script with nifty twists that add air of reality to the meller doings in the Bertram Millhauser story. Dialog is good and situations believably developed, even the highly contrived melodramatic finale. Documentary flavor is forwarded by Reed Hadleys credible narration chore." 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 