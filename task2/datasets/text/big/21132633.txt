Amar Bhoopali
 
{{Infobox film
| name           = Amar Bhoopali
| image          = Amar Bhupali.jpg
| caption        = DVD cover
| director       = V. Shantaram
| producer       = V. Shantaram
| writer         = Vishram Bedekar
| starring       = Panditrao Nagarkar
| music          = Vasant Desai
| cinematography = G. Balakrishna
| editing        = 
| distributor    = 
| studio         = Rajkamal Kalamandir
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
}}
 Grand Prize of the Festival    at the 1952 Cannes Film Festival.   

==Cast==
* Panditrao Nagarkar as Shahir Honaji Bala
* Lalita Pawar Sandhya

==Music==
The music for the film was composed by Vasant Desai and lyrics penned by Shahir Honaji Bala. The soundtrack consists of 8 songs, featuring vocals by Panditrao Nagarkar, Lata Mangeshkar and Asha Bhosle.The song "Ghanashyama Sundara" is from this film.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| title1 = Ghanshyan Sundara Shridhara
| extra1 = Panditrao Nagarkar, Lata Mangeshkar
| title2 = Tujya Pritiche Dukhh Mala Daau Nako Re
| extra2 = Lata Mangeshkar
| title3 = Latpat Latpat Tujha Chaalan Ga
| extra3 = Lata Mangeshkar
| title4 = Saanga Mukund Kuni Ha Paahila
| extra4 = Panditrao Nagarkar, Asha Bhosle
| title5 = Uthi Uthi Gopala
| extra5 = Panditrao Nagarkar
| title6 = Ghadi ghadi are Manmohana
| extra6 = Panditrao Nagarkar, Lata Mangeshkar
| title7 = Tujhi Majhi Preet
| extra7 = Lata Mangeshkar
| title8 = Phadakato Bhagwa Zhenda Gaganaat
| extra8 = Panditrao Nagarkar
}}

==Reception==

===Accolades===
{| class="wikitable" style="font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Award
! Category
! Recipients and nominees
! Outcome
|- Cannes Film Festival Grand Prize of the Festival
| V. Shantaram
|  
|}

==See also==
* Bhopali

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 