The Sun Also Rises (1984 film)
{{Infobox film
| name           = The Sun Also Rises
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = James Goldstone
| producer       = Jean-Pierre Avice John Furia Robert L. Joseph
| screenplay     = Robert L. Joseph
| based on       =  
| narrator       =  Jane Seymour Robert Carradine Ian Charleson Leonard Nimoy
| music          = Billy Goldenberg
| cinematography = 
| editing        = Richard E. Rabjohn Robert P. Seppey
| distributor    = NBC
| released       =  
| runtime        = 200 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Jane Seymour, Robert Carradine, Ian Charleson and Leonard Nimoy have starring roles.  It aired on NBC on Sunday, December 9, and Monday, December 10, from 9&ndash;11 pm.   

==Plot== Jane Seymour) who is about to divorce her husband to marry Scotsman Mike Campbell (Ian Charleson). Barnes attraction to Ashley causes him to follow her movements and Gorton and Cohn follow as Cohn becomes enamored as well. Ashley expresses continuing interest in open relations. The group ventures to see bullfighting in Pamplona, Spain, where Ashley develops an appetite for matador Pedro Romero (Andrea Occhipinti). Cohn pummels Romero upon discovering her latest conquest although it does not impress Ashley. A bruised Romero enters the bullring as the curtains come down. 

==Cast==
*Hart Bochner as Jake Barnes Jane Seymour as Brett Ashley
*Robert Carradine as Robert Cohn
*Željko Ivanek as Bill Gorton
*Ian Charleson as Mike Campbell
*Leonard Nimoy as Count Mippipopolous
*Stéphane Audran as Georgette
*Andrea Occhipinti as Pedro Romero
*Élisabeth Bourgine as Nicole
*Hutton Cobb as Chaz
*Jennifer Hilary as Frances Clyne
*Arch Taylor as Lew Braddocks
*Renata Benedict as Eve Braddocks
*Julian Sands as Gerald

==Production== 1957 film Versailles because neither Paris nor Pamplona looked like they did 60 years earlier.   

==Critical commentary==
According to John J. OConnor of The New York Times virtually all nuances that differ from the source are not well executed.  People (magazine)|People  Fred Hauptfuhrer notes that "Heminway purists" may be offended by some of the changes.  Arthur Unger of The Christian Science Monitor described this production as "a minor literary classic, which has now been turned into a major miniseries disaster".  Stephen Farber of The New York Times recounts numerous elements depicted in the film that were not in the source material such as  A grieving Jake Barnes attends the funeral of a prostitute he had visited several times before being wounded and rendered impotent in World War I. Pedro Romero, the handsome young Spanish bull fighter, uses his sword to murder a vindictive count who has threatened the life of Lady Brett Ashley. Jakes best friend, Bill Gorton, takes an airplane, goes up on a daredevil flight and crashes to his death.   Screenwriter Robert L. Joseph defends the addition of mortally wounded characters as necessary for dramatic depiction.    While the 1957 film starred a 43-year-old Tyrone Power and 34-year-old Ava Gardner, the 1984 adaptation starred a 27-year-old Bochner and 33-year-old  Seymour, who the filmmakers thought could better depict the youthful characters of the source novel. 

==Notes==
 

==External links==
*   

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 