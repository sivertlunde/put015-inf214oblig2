3 Days to Kill
 
{{Infobox film
| name           = 3 Days to Kill
| image          = 3 Days to Kill poster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = McG
| producer       = {{Plainlist| 
* Luc Besson
* Adi Hasak
* Ryan Kavanaugh
* Marc Libert
* Virginie Silla
}}
| screenplay     = Luc Besson Adi Hasak
| story          = Luc Besson
| starring       = {{Plainlist| 
* Kevin Costner
* Amber Heard
* Hailee Steinfeld
}}
| music          = Guillaume Roussel  
| cinematography = Thierry Arbogast
| editing        = Audrey Simonaud
| studio         = EuropaCorp Wonderland Sound and Vision
| distributor    = Relativity Media 
| released       =   
| runtime        = 117 minutes  
| country        = France United States
| language       = English
| budget         = $28 million 
| gross          = $52,597,999 
}}
3 Days to Kill is a 2014 French-American action thriller film directed by McG and written by Luc Besson and Adi Hasak.  The film stars Kevin Costner, Amber Heard, Hailee Steinfeld, Connie Nielsen, Richard Sammel, and Eriq Ebouaney.  The film was released on February 21, 2014. 

==Plot== arms trafficker Director to kill the Wolf, monitors the operation and notices Renner has unknowingly seen the Wolf.

Renner is nearly disabled by an extreme cough, which is diagnosed as terminal brain cancer which has spread to his lungs. He is given only a few months to live, and will not see the next Christmas. For decades he has kept his dangerous career a carefully guarded secret from his wife Christine Connie Nielsen|(Connie Nielsen) and daughter Zooey Hailee Steinfeld|(Hailee Steinfeld), at the cost of losing them. He decides to spend his remaining time trying to fix his relationship with his estranged daughter, and if possible, his ex-wife. He returns to Paris, where he and his family live separately, to find the Réunion family of Jules is squatting in his apartment. He is told by the government that he is not permitted to evict indigent squatters until after the winter.

He makes an awkward reconnection with Christine and Zooey, and tells Christine of his terminal illness. She allows him to reconnect with Zooey, and when she has to go out of the country on business, she is forced to let him look after Zooey. Vivi recruits him to find and kill the Wolf, in exchange for an experimental drug that could extend his life significantly. Renner reluctantly accepts, to get more time with his family. Vivi tells him the way to trap the Wolf is by getting the Albino, in turn by getting his accountant, in turn by kidnapping the gangs limousine driver.
 hallucinogenic effect of the medicine, which occurs whenever his heart rate goes too high, and which he can only control by consuming alcohol. He must also deal with Zooeys school problems, including her habit of lying so she can sneak out partying. He manages to keep her out of trouble, and slowly reestablishes a father relationship with her, which impresses his wife.

He tracks the Wolf and the Albino into the subway, but they gain the upper hand when he is disabled by the hallucinations. The Albino attempts to kill him by pushing him in front of an oncoming train, but Renner manages to push the Albino on the track instead. The Wolf escapes.

The family is invited to a party thrown by Zooeys boyfriends father, who happens to be the Wolfs business partner. Renner manages to protect Christine and Zooey, kill all the Wolfs men, and trap the Wolf in an elevator before breaking the cable. The Wolf manages to survive, wounded, but Renner is again disabled and drops his gun where the Wolf can get it. Vivi kicks the gun back to Renner, telling him to finish the job and kill the Wolf, but he decides not to, because "I promised my wife Id quit." Vivi then kills the Wolf.

At last retired, Renner survives to Christmas, which he is spending at a beach house with Zooey and Christine. He discovers a small, red wrapped gift package, which contains another vial of the cancer medicine. Vivi is seen on a hill behind the house smiling as Renner opens the package.

==Cast==
* Kevin Costner as Ethan Renner
* Amber Heard as Vivi Delay, one of the CIAs elite assassins
* Hailee Steinfeld as Zooey Renner, Ethans estranged sixteen-year-old daughter
* Connie Nielsen as Christine Renner, Ethans ex-wife German arms arms trafficker
* Marc Andréoni as Mitat Yilmaz squats in Ethans apartment
* Tómas Lemarquis as the Albino, the Wolfs lieutenant
* Raymond J. Barry as the CIA Director
* Jonathan Barbezieux as Louis
* Jonas Bloquet as Hugh, Zoeys boyfriend
* Rupert Wynne-James as Hughs father, who happens to be Wolfs partner 
* Philippe Reyno as Young Agent
* Eric Supply as Invite

==Production== Deadline reported that Kevin Costner had been offered the role of Ethan Renner, a government assassin in the McG-directed film.    The film, set in France, was scripted by Luc Besson and Adi Hasak, with EuropaCorp having produced while Relativity Media has North American rights.  On October 2, 2012, it was confirmed that actor Costner had closed the deal to star as lead in the film.    On November 29, 2012, Hailee Steinfeld joined the cast of the film as female lead, and the film began production in early 2013.    On December 13, Amber Heard also joined the cast.    Later, on January 7, 2013, Connie Nielsen was added to the cast.   

===Filming===
On January 7, 2013, crews were filming scenes in Paris and Belgrade, and shooting wrapped in April.  Some scenes in Paris were filmed in the studios and in the central nave of the Cité du Cinéma, founded by Luc Besson in Saint-Denis. Scenes in Belgrade were filmed in front of Hotel Jugoslavija. 

==Release== Super Bowl 2014 spot. 

On May 28, 2013, Relativity set a release date of February 14, 2014 for the film.  Later, on October 30, the films date was shifted back a week, to February 21, 2014.   

 

===Critical response===
 
3 Days to Kill has received negative reviews from critics.  , the film has a score of 40 out of 100, based on 30 critics, indicating "mixed or average reviews" from critics. 

===Box office===
3 Days to Kill grossed $12,242,218 in its opening weekend, finishing in second place behind The Lego Movie ($31.3 million). The film grossed a domestic total of $30,697,999 and a foreign total of $21,900,000, bringing its total gross to $52,597,999.   

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 