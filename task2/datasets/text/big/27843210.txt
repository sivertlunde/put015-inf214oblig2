Adharvam
 
{{Infobox film
| name           = Adharvam
| image          = adharvamnew.png
| image_size     =
| caption        = Promotional Poster
| director       = Dennis Joseph
| producer       = Eeraly Balan
| writer         = Shibu Chakravarthy
| narrator       = Ganesh Kumar Parvathy Jayabharathi
| music          = Ilaiyaraaja
| cinematography = Anandakuttan Ajayan Vincent
| editing        = K. Shankunny
| genre          = Crime
| released       =  
| runtime        =
| country        = India Malayalam
}} Malayalam Drama drama film Ganesh Kumar, Parvathy in the main roles. Mammootty played the role of Anantha Padmanabhan, an expert in mystic tantric rituals. The songs and background score of the movie was by Ilaiyaraaja.

==Plot== mystic tantric Ganesh Kumar) is send to save the society and the village from the clutches of this austere man inflicted with vengeance.

==Cast==
*Mammootty as Anantha Padmanabhan
*Charuhasan as Thevalli Ganesh Kumar as Vishnu
*Silk Smitha as Ponni Parvathy as Usha
*Jayabharathi as Malu
*Thilakan as Mekkadan
*Jagannatha Varma as Putthedan
*Jose Prakash as Thirumeni
*Sukumari as Thevallis Wife
*Kiran Vergis as young Anantha Padmanabhan Kunchan as Subramani
*Trichur Elsi
* Baby Abhijit Venu as Mythreyan (contested)
*Narendra Prasad as voice of Charuhasan

== Crew ==
* Direction: Dennis Joseph
* Screenplay: Shibu Chakravarthy
* Story: Shibu Chakravarthy
* Producer: Eerally Balan
* Music: Ilaiyaraaja
* Editing: K. Shankunny
* Cinematography: Anandakuttan, Ajayan Vincent
* Art Direction: Rajeev Anchal
* Makeup: M. O. Devassiya
* Stunts: A. R. Basha

==Soundtrack==
The songs were composed by Ilaiyaraaja and lyrics penned by O. N. V. Kurup.
#Puzhayorathil - K. S. Chithra
#Ambilikkalayum Neerum - K. S. Chithra
#Om Midhye - P. Jayachandran, Ilaiyaraaja
#Poovayi Virinju - M. G. Sreekumar, Ilaiyaraaja

==External links==
*  
* http://popcorn.oneindia.in/title/5857/adharvam.html

 
 
 
 