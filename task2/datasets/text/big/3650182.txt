Undercover Angel (film)
 
 
{{Infobox film
| name           = Undercover Angel
| image          = UndercoverAngelDVDCover.jpg
| caption        = DVD cover
| director       = Bryan Michael Stoller Eric Abrahamson Bryan Michael Stoller
| writer         = Bryan Michael Stoller
| starring       = Yasmine Bleeth Dean Winters Emily Mae Young James Earl Jones Casey Kasem
| music          = Greg Edmonson
| cinematography = Bruce Alan Greene
| editing        = Stephen Adrianson
| released       = May 16, 1999 (Canada) 2000 (US)
| runtime        = 91 minutes
| country        = Canada
| language       = English
}}
 1999 film by writer/director Bryan Michael Stoller.  The romantic comedy starred Yasmine Bleeth and Dean Winters.

==Plot==
Harrison (Dean Winters) is a struggling freelance writer who becomes the unwilling babysitter of a precocious little six-year-old girl, Jenny (Emily Mae Young), when her mother, Melissa (Lorraine Ansell), one of Harrisons former girlfriends whom he hasnt seen in some time, asks him to babysit because Melissa needs to be out of town for several weeks.

Harrison is originally unenthusiastic about the arrangement, but soon he develops a tenderness for Jenny.  Little Jenny decides that Harrison needs a steady girlfriend and plays matchmaker for Harrison and Holly (Yasmine Bleeth), a beautiful woman who frequently visits the same coffee shop as Harrison and Jenny..

Harrison later writes stories for Jenny about Mr. Dodo—her favorite stuffed animal.  Jenny secretly gets Holly to transcribe the books, and they submit them to his publisher.  Eventually, Harrison discovers that he is Jennys father.  Finally, near the end, Harrison tries to get custody of Jenny because of Jennys uncaring mother.

==Cast==
* Yasmine Bleeth as Holly Anderson
* Dean Winters as Harrison Tyler 
* Emily Mae Young as Jenny Morrison
* James Earl Jones as The Judge
* Casey Kasem as Himself
* Lorraine Ansell as Melissa Morrison
* Richard Eden as Fred
* David L. McCallum as Dan ODonnel (Publisher)
* Don Quiring as Sullivan
* Carherine Knight as Mrs. Stevens
* Leslie Rohonoczy as Receptionist Beth
* Carole Stoller as Mrs. Olend
* Malcolm Younger as Go-Cart Bob
* Rob Goyette as Table Guy Robbie
* Carmen as Stenographer
* Cory Sanford as Go-Cart Bystander
* Colin Brennan as Boy In Purple Shirt At Chapters

==Production==
* Most of this film was shot on location in Ottawa, Canada in July and August 1998.  Some of the locations were the Canal Ritz Restaurant, Thunderbird Mini-golf and Go-Karts, The Supreme Court of Canada, and the Chapters location at the Pinecrest Shopping Centre. The scenes featuring Casey Kasem were filmed in a studio at CTV Ottawa.
* A scene was scheduled to be filmed with Jim Varney, but was cancelled due to Yasmine Bleethes agent refusing to allow her to be in a movie with him.  Ultimately his part was played by a local actor in Ottawa as head of the go-kart track.
* Jay Leno was in talks to play the talk show host, but Casey Kasem ended up filling the role.
* The Ottawa Citizen had an ad to find extras for the film.  Anyone interested was asked to write a short letter and fax it in.  From those, extras were chosen for filming throughout the film.
* Glen Kulka, former football player and wrestler, moonlighted as security on the set.
* A film premiere was held at the Coliseum Theatres on Carling Avenue the year after filming.  Bryan Michael Stoller and Emily Mae Young were in attendance.

==External links==
*  
*   as used in Miss Cast Away, an otherwise unrelated film from the same writer/director.

 
 
 
 
 