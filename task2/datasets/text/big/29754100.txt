Blind (2007 film)
{{Infobox film
| name           = Blind
| image          = Blind 2007 film.jpg
| caption        = Theatrical poster
| director       = Tamar van den Dop
| producer       = Petra Goedings Hilde De Laere
| writer         = Tamar van den Dop
| starring       = Joren Seldeslachts Halina Reijn Katelijne Verbeke
| music          = Junkie XL
| cinematography = Gregor Meerman
| editing        = Sander Vos
| studio         = Valkieser Capital Images (Special Effects)
| distributor    = Klas Film MMG Film & TV Production (Eyeworks Belgium) Phanta Vision Film International B.V.
| released       = 8 February 2007 (Netherlands)
| runtime        = 98 minutes
| country        = Netherlands Belgium Bulgaria Dutch
| budget         = €3 million (US$4.7 million)
| gross          =
}} Dutch drama film written and directed by Tamar van den Dop, and starring Joren Seldeslachts, Halina Reijn and Katelijne Verbeke. The film follows a story of two loving couples, an albino woman and a blind man.

== Plot ==
Ruben (Joren Seldeslachts) is a lone and unbalanced young man who lost his sight in childhood. Marie (Halina Reijn) is an albino woman of temperate look and with a lot of insecurities. She has a beautiful voice and along with Ruben shares a mutual love for books and tales. Rubens mother hires her as a reader to read her son books orally. While they live in a mansion, between these two lonely souls sparks love, but will love still be blind if the man recovers from his blindness?

== Cast ==
* Joren Seldeslachts plays Ruben Rietlander, a main protagonist character who had lost his sight in childhood.
* Halina Reijn plays Marie, a main protagonist character that fells in love with Ruben.
* Katelijne Verbeke plays Catherine Rietlander, Rubens mother.
* Jan Decleir plays Dr. Victor Verbeecke.
* Annemieke Bakker plays Romy.

== Production ==
Production took place in 2007 year with a budget of 3 million euro, the script was written by Tamar van den Dop who also directed the movie. Other companies and people involved in production of this movie include: Cinenumerique - sound re-recording, Film Finances - completion guarantor, Herrie - unit publicity, Kemna Casting - casting, Warnier Studio Amsterdam - dolby mastering, sound post-production, Valkieser Capital Images - special effects.

== Reception == Now magazine, noted that a scene "where two hands fondle through a milky white veil is about as sensual as they come".  Another critic David Nusair of Reel Film Reviews gave it 3 stars out of 4. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 