Red Trousers – The Life of the Hong Kong Stuntmen
{{Infobox film |
name     = Red Trousers: The Life of the Hong Kong Stuntmen |
image          = |
writer         =Robin Shou |
starring       =Robin Shou Beatrice Chia Keith Cooke Hakim Alston Craig Reid |
director       =Robin Shou |
producer       =Robin Shou |
distributor    =Tai Seng |
released   =  |
runtime        =96 minutes
music          =Ezra Gold  Nathan Wang | Cantonese  Mandarin   Spanish |}}

Red Trousers: The Life of the Hong Kong Stuntmen ( ) is a documentary film directed by Robin Shou.

== Plot ==
This documentary from Robin Shou—who also hosts and participates in the film—takes a behind-the-scenes glance inside the stunt industry of Hong Kong, which is known for being riskier and less trick-oriented than its American counterpart. In addition to archival and interview footage featuring some of the industrys most prominent stuntmen, Red Trousers - The Life of Hong Kong Stuntmen incorporates scenes from Lost Time (2001) in an effort to illustrate how stuntmen prepare for and ultimately perform in modern martial arts films.

==Cast==
*Seb H - Choreographer/How to Wear Tight Red Trousers Consultant/Himself 
*Robin Shou - Evan/Narrator/Himself 
*Beatrice Chia – Silver
*Keith Cooke - Kermuran (as Keith Cooke Hirabayashi)
*Hakim Alston - Eyemarder
*Craig Reid - Jia Fei (as Craig D. Reid)
*Buffulo - Computer virus thug/Zus zombie fighter 
*Mindy Dhanjal - Zu Yao Her
*Duck - Forest Devil/Himself
*Kok Siu Hang - Flying machine body guard/forest devil/himself
*Sammo Hung Kam-Bo - Himself (as Sammo Hung)
*Lueng Shing Hung - Zus Zombie Fighter
*Kam Loi Kwan - Flying machine body guard/computer virus thug/forest devil/zus zombie fighter/himself
*Alice Lee - Nurse 
*Mike Leeder - Mr. Goa 
*Chia-Liang Liu - Himself (as Lau Kar-Leung Sifu)
*Leung Chi Ming - Forest Devil
*Monique Marie Ozimkowshi - Dominatri
*Jude Poyer - Flying machine body guard/himself
*Ng Wing Sum - Flying machine body guard/computer virus thug/zus zombie fighter
*Ridley Tsui - Himself
*Chi Man Wong - Computer virus thug/forest devil/zus zombie fighter/himself

== Awards ==
* Newport Beach Film Festival 2003
** Outstanding Achievement in Filmmaking Award

== Media ==

=== DVD release ===
* Red Trousers – The Life of the Hong Kong Stuntmen (2005)
* Red Trousers – The Life of the Hong Kong Stuntmen Collectors Edition (2-Disc-Set) (2005)

==External links==
* 
* 
* 

 
 
 
 
 
 