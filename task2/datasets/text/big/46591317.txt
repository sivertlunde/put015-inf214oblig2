Mickey's Nightmare
{{Infobox Hollywood cartoon
| cartoon_name   = Mickeys Nightmare
| series         = Mickey Mouse
| image          =
| caption        =
| director       =
| story_artist   = 
| animator       = 
| voice_actor    = 
| musician       =
| producer       =  Walt Disney Productions
| distributor    = 
| release_date   =   Black & White
| runtime        = 
| movie_language = English
| preceded_by    = Mickey in Arabia (1932)
| followed_by    = Trader Mickey (1932)
}}
Mickeys Nightmare is a 1932 Walt Disney short black and white cartoon about Mickey Mouse and Pluto (Disney)|Pluto.

==Summary==
It is night-time and Mickey Mouse is doing his night prayers before bedtime.  As he gets into bed, Pluto joins Mickey, but then gets sent to his basket as hes a dog.  As soon as Mickey falls asleep, Pluto creeps back into Mickeys bed and licks his face. Mickey dreams a mysterious dream about when he proposed to Minnie Mouse and got married at the church after the proposal.

After the wedding, Mickey and Pluto are in the garden watering the flowers. A stork comes by and drops the first batch of baby mice into the chimney of Mickey and Minnies home. But then, more baby mice start to arrive by the same stork and a bucketful of them are poured down the chimney. Mickey rushes inside to find Minnie Mouse in bed with lots of baby mice. They all start to chase and play rough with their father along with their pet dog. Soon, the baby mice start to use paintbrushes to paint the wall and ceiling in black paint. They also throw pillows at Mickey landing on his face. But the last one smacks Mickey so hard, that his jug is now in pieces.

Mickey Mouse is then in a tight tangle whilst the paint is licking across his face, the telephone is ringing and the cuckoo clock is crowing. But in Mickeys bedroom, he finds himself wrapped up in his bedclothes with Pluto licking across his face and the rooster is crowing whilst his alarm clock is ringing. Mickey wakes up and realizes its a dream and his determination is to never marry Minnie forever. The cartoon ends with Pluto licking his masters face.

==See also==
* 

==References==
* Walt Disneys Mickeys Nightmare - 1932 
 

==External links==
* 

 

 
 
 
 

 