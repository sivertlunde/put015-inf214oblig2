Take Me Home Tonight (film)
 Kids in America}}
{{Infobox film
| name           = Take Me Home Tonight
| image          = Take Me Home Tonight Poster.jpg
| caption        = Theatrical release poster
| director       = Michael Dowse
| producer       = Ryan Kavanaugh    Jim Whitaker  Susan Bowen  Jackie Filgo Jeff Filgo
| story          = Topher Grace Gordon Kaywin
| starring       = Topher Grace Anna Faris Dan Fogler Teresa Palmer
| music          = Trevor Horn
| cinematography = Terry Stacey
| editing        = Lee Haxall Rogue
| distributor    = Relativity Media (USA/Canada/North America)
Universal Pictures (International)
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $19 million   
| gross          = $7.6 million 
}}

Take Me Home Tonight is a 2011 American  .      The film received its wide theatrical release on March 4, 2011. Prior to release the film was titled Young Americans and Kids in America.

Despite having the name, the song Take Me Home Tonight (song)|"Take Me Home Tonight" by Eddie Money is never played in the film. Only the first trailer includes the song, as well as the menu screen of the Blu-ray and DVD versions of the movie.

==Plot== 1988 Los MIT graduate who works at a Suncoast Video store while trying to figure out what he wants to do with his life, something that his police officer father (Michael Biehn) has grown impatient with. While working one day, Matts high school crush, Tori Fredreking (Teresa Palmer), randomly walks into the store. After pretending that he doesnt work there and saying that he works at Goldman Sachs in an effort to impress her, Tori invites Matt to a Labor Day party, hosted by Matts twin sister Wendys (Anna Faris) boyfriend, Kyle Masterson (Chris Pratt), at his hillside home.

Later that night, Matt, Wendy, and Matts best friend, Barry Nathan (Dan Fogler), head to the party. On the drive over, Barry steals a brand new Mercedes-Benz convertible from the car dealership he got fired from earlier that day, justifying his actions by saying that Matt needs the convertible if he really wants to impress Tori.
 proposes to her in front of everyone at the party. She says yes, upsetting Matt, who doesnt think that Kyle will support her in her dream to attend graduate school at the University of Cambridge.

Tori eventually invites Matt and Barry to another party her boss is hosting in Beverly Hills. Matt takes Tori there in the Mercedes, while Barry rides with her two friends in another car, using the cocaine as an enticement to let him go along. Barry has a wild sexual encounter with an older woman while Matt and Tori continue to mingle with each other, after Matts successful put down of Toris boss, a habitual sexual harasser. They leave the party to go into a neighbors backyard where they jump on a trampoline, play truth or dare, and end up having sex.

Meanwhile, Wendy shares her unopened admissions letter from Cambridge with Kyle, and it is revealed that she was not accepted. Kyle is visibly relieved, while Wendy is visibly upset.

Matt confesses to Tori that he doesnt actually work at Goldman Sachs. Tori gets extremely upset at his deception and storms off, back to her bosss party, leaving Matt guilt-ridden.

Matt finds Barry there and they leave the party, and Matt tells Barry about how he told Tori the truth about his job. Barry patronizes him for not trying to have just one night of enjoyment and attempts to lighten the mood by offering Matt a line of cocaine, despite him driving. Matt, feeling vulnerable, attempts to snort the cocaine, but ends up driving the convertible off the road and into a ditch. A police cruiser soon arrives, and it turns out to be Matts dad and his police partner. Already disappointed with Matts unwillingness to choose a career path, Mr. Franklin damages the convertible even more as a means of coercing Matt to get a better job in order to pay off the damages. He takes Matt and Barry into custody, but lets them off in the neighborhood with a warning. Matt apologizes to his dad for being such a failure, to which his dad replies that because hes never even tried to succeed, hes actually less than a failure. Then, in a less condescending tone, he encourages Matt to take a shot at anything in life.

Knowing Tori has left her car at the party, Matt and Barry make their way back there, where bets are being placed on who will "ride the ball", a giant, steel sphere that someone enters and rides as its rolled down a hill (something Matts dad has stated that nobody has done since he himself was a kid). Matt finds Tori at the party and tries to apologize, but Tori is unwilling to forgive him. Feeling he has nothing to lose, Matt volunteers to "ride the ball". When he does the ball rolls downhill uncontrollably, hitting several parked cars and eventually flying off an embankment before landing in a backyard swimming pool. Matt almost drowns as the ball sinks, but manages to escape just in time. Barry rushes to the scene and walks with Matt back toward the party, meeting up with Wendy and Tori, who are elated to discover he wasnt killed. Matt apologies to Tori, and she forgives him after playing a little hard-to-get and then gives him her phone number. All four return to the party, which is beginning to wind down as dawn approaches. All who are still there whoop it up at Matts successful return.

Wendy, after realizing Matt was right all along, ends the engagement and breaks up with Kyle, who experiences a crying breakdown.

Pondering his future with a Goth-type girl he met at the party, Barry is told by her that maybe he should go to college.

Outside, as she leaves the party, Matt boldly kisses Tori goodbye.

Matts dad is shown investigating the scene where the steel ball was found. He finds Matts Suncoast Video name tag floating in the pool, and smirks proudly.

Barry staggers out of the party house, now in a shambles, to meet up with Wendy and Matt, who asks "Who wants breakfast?", and the three leave together as the sun is rising.

==Cast==
  
* Topher Grace as Matthew "Matt" Franklin
* Anna Faris as Wendy Franklin
* Dan Fogler as Barry Nathan
* Teresa Palmer as Tori Frederking
* Chris Pratt as Kyle Masterson
* Michael Biehn as Bill Franklin
* Lucy Punch as Shelly
* Michelle Trachtenberg as Ashley
* Demetri Martin as Carlos
* Michael Ian Black as Pete Bering
* Bob Odenkirk as Mike
* Angie Everhart as Trisha Anderson
* Edwin Hodge as Bryce
* Candace Kroslak as Ally
* Nathalie Kelley as Beth Robert Hoffman as Tyler "Dance Machine" Jones
* Ryan Bittle as Rick Herrington
* Seth Gabel as Brent Tufford
 

==Production==
Principal photography was completed in 2007, but Universal Studios shelved the film until its 2011 theatrical release.    Topher Grace posted that the release of the film was delayed when the studio didnt know how to handle and promote a youth comedy film with portrayal of cocaine use, as the drug was prominent in the 1980s. 
 Rogue acquired the film from Universal Pictures for $10 million.   The film was previously titled Young Americans and Kids in America, titles of popular songs by David Bowie and Kim Wilde.

On March 3, 2011, while being interviewed on Ryan Seacrests 102.7 KIIS FM radio show, Topher Grace announced to former American Idol contestant Chris Medina that 1% of the films box office revenue would be donated to the care of Medinas injured fiance, Juliana Ramos. Juliana was involved in a serious car accident in 2009 and suffered a traumatic brain injury. Her story has been widely followed across the nation since Medinas appearance on American Idol.

==Release==
The film was released in the U.S. on March 4, 2011. Relativity released a trailer for the film in December 2010.

===Box office===
Take Me Home Tonight was a box office flop. The film debuted at #11 with $3,464,679 on its opening weekend in 2,003 theater of North America. The film grossed $6,928,068 in North America, failing to recoup its $19 million budget.

===Critical response=== David Denby of The New Yorker writes that "(Topher) Grace has a way about him, the young Australian actress Palmer is lovely and crisp, and the Canadian writer-director Michael Dowse manages the exuberant traffic well enough." 
 John Hughes and Cameron Crowe, a time when comedies allowed their characters to be human as well as humorous." 

===Accolades===
Teen Choice Awards
*2011 - Choice Movie Actress: Comedy for Anna Faris (Nominated)

===Home media===
The DVD and Blu-ray was released on July 12, 2011. The Blu-ray edition includes an additional digital copy. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at Theiapolis

 

 
 
 
 
 
 
 
 
 
 
 
 
 