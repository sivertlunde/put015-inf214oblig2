The Silver Lining (1927 film)
{{Infobox film
| name           = The Silver Lining
| image          =
| image_size     =
| caption        =
| director       = Thomas Bentley
| producer       = 
| writer         = 
| narrator       =
| starring       = Marie Ault   Patrick Aherne   John F. Hamilton   Eve Gray
| music          =  
| cinematography = 
| editing        = 
| studio         = British International Pictures
| distributor    = Wardour Films
| released       = 1927
| runtime        = 
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} British silent silent drama film directed by Thomas Bentley and starring Marie Ault, Patrick Aherne and Moore Marriott.  Two brothers fight over a girl, leading one to frame the other for robbery. Later, guilt-ridden, he confesses and arranges his own death.

==Cast==
* Marie Ault - Mrs. Hurst 
* Patrick Aherne - Thomas Tom Hurst 
* John F. Hamilton - John Hurst
* Eve Gray - Lettie Deans 
* Sydney Fairbrother - Mrs. Akers 
* Moore Marriott - Gypsy  Cameron Carr - Constable 
* Hazel Wiles - Mrs. Deans  Bernard Vaughan - Vicar

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 