Seevalaperi Pandi
{{Infobox film
| name           = Seevalaperi Pandi
| image          = Seevalaperi Pandi DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = Prathap K. Pothan
| producer       = P. G. Srikanth
| writer         = K. Rajeswar
| starring       =  
| music          = Adithyan
| cinematography = K. Aravind
| editing        = B. Lenin V. T. Vijayan 
| distributor    = Radhi Films
| studio         = Radhi Films
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}}
 1994 Tamil Tamil drama Napoleon in lead role making his debut as hero. The film, produced by P. G. Srikanth, had musical score by Adithyan and was released on 23 June 1994. The film is based on the life of Seevalaperi Pandi.   

==Plot==

Seevalaperi Pandi (Napoleon (actor)|Napoleon), a brave man, is married to Velammal (Saranya Ponvannan |Saranya) and lives with her, his mother and his elder brother Malayandi. Impressed by Pandis strength, the villages Grams (Vijayachander) hires him as his bodyguard.

The grams is a good man but he spoilt some villagers life because of his judgements. Nayanar (R. P. Viswam), Sivankalai (Alex), Karuppaiah (Suryakanth), Mookaiah (Venniradai Moorthy) and Oochandi (Prasanna) who were against the grams decisions decide to kill him with Pandi. Pandi and his associates kill Grams and they immediately surrender. While, his associates are acquitted, Pandi is sentenced to the capital punishment. His associates promise him to help his family.

Pandi lives in jail happily. Therefore, Pandi is granted in the last minutes and his sentence has been changed to life imprisonment. His brother and his wife reveal that his mother died due to lack of foods. Pandi escapes from the jail the same night. Angry, Pandi decides to take revenge on his former associates who betrayed him.

The character Seevelaperi Pandi is based on a real life character who lived during the 60s at Seevalaperi village in Tirunelveli district.

==Cast==
 Napoleon as Pandi Saranya as Velammal
*Ahana as Oyila Chandrasekhar as Aliyan
*Nizhalgal Ravi
*Vijayachander as Grams
*R. P. Viswam as Nayanar 
*Alex as Sivankalai 
*Suryakanth as Karuppaiah 
*Venniradai Moorthy as Mookaiah 
*Prasanna as Oochandi 
*Charle
*Chinni Jayanth
*Nellai Siva
*G. D. Ramesh as Rajkumar
*Madhan Gabriel
*Rajan
*Sathyapriya as Grams wife
*Master Dinesh as Arumugam
*Silk Smitha as an item number
*John Babu in a cameo appearance

==Production==
Rajeshwar went to Tirunelveli and stayed in the city to gather material for the film. He interacted with the locals for over three months. Though the film was based on life of a person of same name, several scenes in the film were fictional. Rajeshwar stated: "The movies might be based on real people, but they were not biographies. The characters and incidents are embellished to make the movie more interesting. So, Oila, an important character in Seevalaperi Pandi was richly embroidered". 

==Soundtrack==

{{Infobox Album |  
| Name        = Seevalaperi Pandi
| Type        = soundtrack
| Artist      = Adithyan
| Cover       = 
| Released    = 1994
| Recorded    = 1994 Feature film soundtrack |
| Length      = 23:57
| Label       = 
| Producer    = Adithyan
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Adithyan. The soundtrack, released in 1994, features 5 tracks with lyrics written by Vairamuthu and K. Rajeswar.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Aruvi Onnu Kulikudhu || Swarnalatha || 4:05
|- 2 || Napoleon || 3:45
|- 3 || Masala Araikura || Mano, Asha Latha || 4:54
|- 4 || Oyila Paadum Paatule || K. S. Chithra || 6:13
|- 5 || Thirunvelli Seemayile || S. P. Balasubrahmanyam || 5:00
|}

==Reception==

The film was a blockbuster and it was a turning point in Napoleon (actor)|Napoleons career.  

==References==

 

==External links==
* 
* 

 
 
 
 