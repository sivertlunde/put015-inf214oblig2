Don't Worry, I'm Fine
 
{{Infobox film
| name     = Dont Worry, Im Fine
| image    =
| director = Philippe Lioret
| producer = Christophe Rossignon
| Script = Olivier Adam Philippe Lioret
| starring = Mélanie Laurent   Kad Merad
| cinematography = Sascha Wernik
| music = Nicola Piovani
| country = France 
| language = French 
| runtime = 100 minutes
| released =  
| budget = $4,550,000
| gross = $12,660,634 
}}
Dont Worry, Im Fine ( ) is a 2006 French drama film directed by Philippe Lioret based on the 2000 novel with the same title by Olivier Adam.

==Plot==

Lili, a 19 year old, returns from holidays and learns that her twin brother Loïc has left the house after a violent argument with their father. Lili had a very strong relationship with her brother and is distraught after having no contact with him at all, concluding that something happened to him. 

Lili stops eating and begins losing strength and ends up in the hospital where she has decided to stop living at all. She receives a letter from Loïc where he apologizes for leaving without a word or getting back to her and makes it very clear that he will not be coming back. He also says that he has been traveling around living on petty jobs and blames their father for his lot in life. Lili recovers and begins looking for her twin by following the trail of the letters she has received along with Thomas, the boyfriend of her friend from school, Lea. Lili and Thomas gradually fall in love.
 
When Lili goes to Saint Aubin with Thomas, she sees her father mailing letters, concluding that her father was imitating Loïcs handwriting and sending letters to Lili, in an attempt to protect her and keep her alive. Coincidentally, Thomas, when visiting his grandmothers grave, sees Loïcs gravestone. When Thomas arrives at Lilis house for a family lunch, he speaks with her parents, mentioning that he knows of Loïcs death. They reveal that Loïc had died in an accident during mountain climbing, and they plead for Thomas not to tell Lili anything. Thomas believes that they are crazy, but decides to keep the truth to himself. Lili, who arrives home shortly after to meet Thomas and her parents for the lunch, finds her brothers guitar hidden in her fathers car. Knowing he would never have left behind his beloved guitar, she learns that he can not just have gone away.

Even though both Lili and Thomas know the truth by now, they dont talk about it, even though Loïc has been the most important thing on their minds for the past year. They talk about leaving the city and go to the sea.

==Cast==
* Mélanie Laurent as Lili 
* Kad Merad as Paul
* Julien Boisselier as Thomas
* Isabelle Renauld as Isabelle
* Aïssa Maïga as Léa

==Reception==
The film had positive reviews. 

==Awards and nominations==
*César Awards 2007|César Awards
**Won: Best Actor &ndash; Supporting Role (Kad Merad)
**Won: Most Promising Actress (Mélanie Laurent)
**Nominated: Best Director (Philippe Lioret)
**Nominated: Best Film
**Nominated: Best Writing &ndash; Adaptation (Olivier Adam and Philippe Lioret)
*Prix Romy Schneider for Mélanie Laurent

== References ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 