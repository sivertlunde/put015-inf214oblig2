White Oleander (film)
{{Infobox film
| name           = White Oleander
| image          = Whiteorleander.jpg
| caption        = Theatrical release poster
| director       = Peter Kosminsky John Wells
| screenplay     = Mary Agnes Donoghue
| based on       = White Oleander&nbsp;by   Robin Wright Penn Renée Zellweger
| music          = Thomas Newman
| cinematography = Elliot Davis
| editing        = Chris Ridsdale
| distributor    = Warner Bros. Umbrella Entertainment
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $16,000,000
| gross          = $21,672,284   
}} Robin Wright Penn, Billy Connolly and Patrick Fugit in supporting roles.
 novel of the same name by Janet Fitch, which was selected for Oprahs Book Club in 1999.

==Plot==
The narrator, Astrid Magnussen (Alison Lohman), is the 15-year-old daughter of free-spirited artist Ingrid (Michelle Pfeiffer). Since her father left before she was old enough to remember him, Astrid depends heavily upon the maternal care of her passionate but largely self-centered mother. Ingrids current relationship with a vulgar man named Barry (Billy Connolly) ends when she discovers he is cheating on her with younger women.  Ingrid murders him with a poison made from white (oleander). Ingrid is arrested and sentenced to life in prison, leaving Astrid in foster care.
 Robin Wright Penn), a former stripper and born-again Christian who is a recovering alcoholic. Though they initially interact well, Starr discovers Astrid having an affair with her live-in boyfriend. After a loud argument with him, she runs into Astrids room in a blind rage and shoots her.

After spending time in a violent group foster home, where she strikes up a relationship with fellow artist Paul Trout, Astrid is given into the care of former actress Claire Richards (Renée Zellweger).  Claire is a sweet, affectionate woman who provides stability for Astrid. 

Claire accompanies Astrid on a visit to Ingrid in prison.  The jealous Ingrid exploits Claires weaknesses and upsets Claire and her daughter.  Claire already suspects her husband Mark (Noah Wyle) of having an affair and planning to divorce her. Claire commits suicide and Astrid is returned to foster care.
 Russian immigrant, Rena (Svetlana Efremova), who treats her foster children as cheap laborers.

As Ingrids appeal for release approaches, she tries to bribe Astrid to testify that she didnt murder Barry. Astrid realizes shes in a position of power over Ingrid and demands answers about her past.  Ingrid admits she abandoned Astrid with a babysitter called Annie for over a year when she was younger.  Astrid realizes this caused her feelings of abandonment. 

Astrid reluctantly agrees to testify, but her mothers lawyer tells her the appeal was denied.  Ingrid refused to let the lawyer call Astrid to testify.  Ingrid has finally denied her own selfishness to commit an act of love for her daughter. Newly emancipated, Astrid forges a new life in New York with Paul Trout (Patrick Fugit), the young man she met in the group home.

==Cast==
*Alison Lohman as Astrid Magnussen
*Michelle Pfeiffer as Ingrid Magnussen
*Robin Wright as Starr Thomas
*Renée Zellweger as Claire Richards
*Amy Aquino as Ms. Martinez
*Billy Connolly as Barry Kolker
*Svetlana Efremova as Rena Gruschenko
*Patrick Fugit as Paul Trout
*Taryn Manning as Nikki
*Cole Hauser as Ray
*Liz Stauber as Carolee
*Noah Wyle as Mark Richards
*Marc Donato as Davey Thomas

==Production==
Barbra Streisand turned down offers to direct the film and play Ingrid Magnussen.   

Alison Lohman wore a wig throughout filming because she had just finished playing a cancer patient in deleted scenes from the film Dragonfly (2002 film)|Dragonfly (2002). 

The film clip Claire (Renée Zellweger) shows Astrid as an example of her acting career is of Zellwegers own early performance in The Return of the Texas Chainsaw Massacre (1994). 

==Differences between novel and screenplay==
 
There are a number of crucial differences between the book and movie.

*Astrid is twelve years old at the beginning of the novel.  In the film, she is fifteen.
*Ingrid is a poet in the novel, but a photographer in the film.
*Ingrid is released from jail after winning her appeal in the novel. In the film she remains imprisoned. In both cases, she chooses to spare her daughter from testifying. 
*Astrid lives in two other foster homes in the novel, staying with the Turlocks, Olivia Johnstone, and Amelia Ramos.  These characters are eliminated from the film.
*Astrid is never attacked by dogs and scarred for life.  
*She is not starved by her foster parents.
*Ray is almost fifty in the book, but in his thirties in the film.
*In the novel, Claires husband is named Ron; in the movie he is renamed Mark.
*In the novel, Astrid becomes highly attached to Barry, to the extent that she dreams of Ingrids marrying him and Barrys asking Astrid to call him "Dad". In the film; Ingrids relationship with Barry is heavily condensed and Astrids emotional attachment to Barry is only hinted at when she tells Davey she ight have saved his life. 
*In the novel, Astrid and Paul move to Berlin, Germany. In the movie they move to New York.
*In the novel, Astrid has an affair with Renas boyfriend, Sergei.  He isnt in the film.
*In the novel, Astrid remembers Annie – her former babysitter – after taking acid with her friend Niki. In the movie, Astrid remembers Annie on her own, and often draws portraits of her.
*In the novel, Astrid is sent to MacLaren Childrens Center.  In the movie it is simply McKinney Hall.
*In the novel, Astrid tracks down her father, who is disappointingly ordinary despite his creative talents. The movie makes no mention of this.

==Release==
===Reception===
White Oleander currently holds a rating of 70% on Rotten Tomatoes,  and a score of 61 on Metacritic,  indicating generally favorable reviews.

  named it as a runner-up on his list of the ten best English-language films of 2002. 

The performances were widely acclaimed, particularly those of Michelle Pfeiffer and Alison Lohman. The New York Times called Pfeiffers role the "most complex screen performance of her career... at once irresistible and diabolical,"  while the Los Angeles Times singled out her "riveting, impeccable performance in what is literally and figuratively a killer role."  Variety (magazine)|Variety described it as a "daring, unsympathetic performance."    Lohmans work was variously described as "the years most auspicious screen acting début,"  a "tremendously weighty and extended role...   with great confidence,"  and an "awesome performance." 

===Accolades=== Screen Actors Guild Award for Outstanding Supporting Actress.   

Renée Zellweger was nominated for the Satellite Award for Best Supporting Actress – Motion Picture. 

Alison Lohman was nominated for the Phoenix Film Critics Society Award for Best Newcomer. 

Marc Donato won a Young Artist Award in the category of Best Performance in a Feature Film – Supporting Young Actor. 

===Home Media===
White Oleander was released on DVD by Umbrella Entertainment in December 2011. The DVD is compatible with all region codes and includes special features such as the theatrical trailer, interviews with the cast and creators, behind the scenes footage and audio commentary with Peter Kosminsky, John Wells and Janet Fitch.   

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 