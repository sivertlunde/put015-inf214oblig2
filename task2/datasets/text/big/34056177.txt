Bed and Board (1970 film)
 
{{Infobox film
| name           = Bed and Board
| image          = Domicile_conjugal.jpg
| caption        = original film poster
| director       = François Truffaut
| producer       = François Truffaut Marcel Berbert
| writer         = François Truffaut Claude de Givray Bernard Revon
| starring       = Jean-Pierre Léaud Claude Jade Hiroko Berghauer Daniel Ceccaldi Claire Duhamel
| music          = Antoine Duhamel
| cinematography = Nestor Almendros
| editing        = Agnés Guillemot
| studio         = Les Films du Carrosse Valoria Films Fida Cinematografica
| distributor    = Columbia Pictures
| released       = 9 September 1970 (France)  21 December 1971 (NYC, USA)  8 July 1971 (UK)
| runtime        = 100 min
| country        = France
| language       = French gross = 1,010,797 admissions (France) 
}} Antoine (Jean-Pierre Love on the Run.

==Synopsis==
The fourth installment in François Truffaut’s chronicle of the ardent, anachronistic Antoine Doinel, Bed and Board plunges his hapless creation once again into crisis. Expecting his first child and still struggling to find steady employment, Doinel (Jean-Pierre Léaud) involves himself in a relationship with a beautiful Japanese woman that threatens to destroy his marriage. Lightly comic, with a touch of the burlesque, Bed and Board is a bittersweet look at the travails of young married life and the fine line between adolescence and adulthood.

==Cast==
* Jean-Pierre Léaud as Antoine Doinel
* Claude Jade as Christine Doinel
* Daniel Ceccaldi as Lucien Darbon
* Claire Duhamel as Madame Darbon
* Hiroko Berghauer as Kyoko
* Daniel Boulanger as Tenor
* Silvana Blasi as Tenors wife
* Pierre Fabri as the office Romeo
* Barbara Laage as Monique, secretary
* Billy Kearns as M. Max
* Claude Véga as the Strangler
* Jacques Jouanneau as Césarin
* Danièle Girard as Ginette, a waitress
* Jacques Robiolles as Sponger
* Yvon Lec as the Traffic Warden
* Marie Irakane as Mrs Martin, a concierage
* Ernest Menzer as the little man
* Jacques Rispal as Old Solitary
* Philippe Léotard as a Drunkard
* Pierre Maguelon as Cérasins friend
* Guy Pierrault as an SOS employee
* Marcel Mercier as a person in the courtyard
* Joseph Merieau as a person in the courtyard
* Christian de Tiliere as a Senator
* Nobuko Mati as Kyokos friend
* Iska Khan as Kyokos father
* Jacques Cottin as Monsieur Hulot (uncredited) 

==Notes==
The film contains humorous references to "Last Year at Marienbad," Jacques Tati and the previous film in this series, "Stolen Kisses."

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 

 