The Daughter of the Green Pirate
{{Infobox film
| name = The Daughter of the Green Pirate
| image =
| image_size =
| caption =
| director = Enrico Guazzoni
| producer =  Giulio Manenti  
| writer =  Emilio Salgari  (novel)   Alessandro De Stefani     Nino Angioletti 
| narrator =
| starring = Doris Duranti   Fosco Giachetti   Camillo Pilotto   Enrico Glori
| music = Alberto Ghislanzoni  
| cinematography = Jan Stallich  
| editing = Vincenzo Zampi      
| studio = Manenti Film 
| distributor = Manenti Film 
| released = 23 December 1940
| runtime = 76 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Daughter of the Green Pirate (Italian:La figlia del corsaro verde) is a 1940 Italian adventure film directed by Enrico Guazzoni and starring Doris Duranti, Fosco Giachetti and Camillo Pilotto.  It was shot partly at the Pisorno Studios in Tirrenia. The film was based on a novel by Emilio Salgari. The son of a Spanish Governor in South America volunteers for an undercover mission to infiltrate a gang of notorious pirates. 

==Cast==
* Doris Duranti as Manuela 
* Fosco Giachetti as Carlos de la Riva 
* Camillo Pilotto as Zampa di ferro 
* Mariella Lotti as Isabella 
* Enrico Glori as El Rojo 
* Sandro Ruffini as Don Luis, il governatore 
* Tina Lattanzi as Donna Mercedes, moglie del governatore  Polidor as Golia 
* Carmen Navasqués as Carmen, la danzatrice 
* Ernesto Almirante as Il precettore delle educante 
* Primo Carnera as El Cabezo 
* Mario Siletti as Il segretario del governatore 
* Nino Marchetti as Ramon 
* Giulio Battiferri as Un pirata 
* Ori Monteverdi as Grazia 
* Dedi Montano as Estella 
* Nada Fiorelli as Leonora 
* Nino Marchesini as Il capitano dellEsperanza 
* Luigi Erminio DOlivo as Il messo del governatore 
* Enzo De Felice as Gonzalo 
* Riccardo De Miceli as Lufficiale scozzese 
* Gino Scotti as José 
* Maria Maloggi as Un educante 
* Nennella Scotti as Un altra educante 
* Leo Garavaglia as Un pirata 
* Emilio Gneme as Un pirata 
* Enrico Marroni as Un pirata

== References ==
 

== Bibliography ==
* Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013.

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 