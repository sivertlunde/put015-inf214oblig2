Kattuvithachavan
{{Infobox film 
| name           = Kattuvithachavan
| image          =
| caption        =
| director       = Rev Suvi
| producer       = Rev Suvi
| writer         = A. Sheriff
| screenplay     = Prema Shobha JC George
| music          = Peter Reuben
| cinematography =
| editing        =
| studio         = Crisarts
| distributor    = Crisarts
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed and produced by Rev Suvi . The film stars Thikkurissi Sukumaran Nair, Prema (Malayalam actress)|Prema, Shobha and JC George in lead roles. The film had musical score by Peter and Reuben.   

==Cast==
 
*Thikkurissi Sukumaran Nair Prema
*Shobha
*JC George
*Baby Padma
*Bahadoor
*Girish Kumar
*Junior Balayya
*K. P. Ummer
*Madhavan Kutty
*Muthu
*Radhamani
*Rani Chandra Sujatha
*Vijayanirmala
 

==Soundtrack==
The music was composed by Peter and Reuben and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Mazhavillin Ajnaathavaasam || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Neeyente Praarthana || Chorus, Mary Shaila || Poovachal Khader || 
|-
| 3 || Soundarya Poojakku || K. J. Yesudas || Poovachal Khader || 
|-
| 4 || Swargathilallo Vivaaham || S Janaki || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 