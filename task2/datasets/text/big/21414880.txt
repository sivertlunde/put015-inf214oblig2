High Lonesome (film)
{{Infobox Film
| name           = High Lonesome
| image          = 
| image_size     = 
| caption        = 
| director       = Alan Le May
| producer       = George Templeton
| writer         = Alan Le May
| narrator       = 
| starring       = John Drew Barrymore
| music          = Rudy Schrager
| cinematography = W. Howard Greene 
| editing        = Jack Ogilvie
| distributor    = Eagle-Lion Films
| released       = 1950
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
|
}} Western movie The Searchers The Unforgiven.  It is his only directing credit. This film stars John Drew Barrymore (billed as "John Barrymore, Jr.") and features Chill Wills and Jack Elam. It is set in the Big Bend country of West Texas. 

==Plot==
A young drifter is found stealing food at Horse Davis ranch by Boatwhistle, the cook. Another rancher, Pat Farrell, who is engaged to Horses daughter Abby, believes the boy to also be a horse thief and possibly worse.

Given the nickname Cooncat by the cook, the boy explains that he was wrongfully accused of murdering a man named Shell and has fled from the law. Shell owed him money, he says, and two strangers known as Smiling Man and Roper gave him a gun to confront Shell. He wound up unconscious and next to Shells bullet-riddled body.

Horse doubts the boys story, though youngest daughter Meagan believes it. At an engagement party for Pat and Abby, word comes that Pats parents have been found murdered. A livid Pat is ready to hang Cooncat for the crime. Horse talks him out of it, creating a rift between the two old friends.

Smiling Man and Roper turn up in the bunkhouse. They laugh at Cooncats predicament and call him their lucky charm. Boatwhistle is shot by Smiling Man, and just as Horse is about to be ambushed, Cooncat calls out to warn him and is wounded. Pat rides up just in time to save Horses life. The two ranchers agree to take Cooncat under their wing.

==Cast==
*John Drew Barrymore as Cooncat
*Chill Wills as Boatwhistle John Archer as Pat Farrell
*Lois Butler as Meagan Davis
*Kristine Miller as Abby Davis
*Basil Ruysdael as "Horse" Davis
*Jack Elam as Smiling Man
*Dave Kashner as Roper
*Frank Cordell as Frank
*Clem Fuller as Dixie
*Hugh Aiken as Art Simms
*Howard Joslin as Jim Shell

==Production==
John Drew Barrymore was the son of John Barrymore and the father of Drew Barrymore.

The movie was filmed on location in Antelope Springs and Marfa, Presidio County, Texas. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 


 