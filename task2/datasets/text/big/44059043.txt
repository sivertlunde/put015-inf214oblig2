Muttathe Mulla
{{Infobox film
| name = Muttathe Mulla
| image =
| caption =
| director = J. Sasikumar
| producer = Thiruppathi Chettiyar
| writer = Pappanamkodu Lakshmanan
| screenplay =
| starring = Prem Nazir Adoor Bhasi Thikkurissi Sukumaran Nair Jose Prakash
| music = V. Dakshinamoorthy
| cinematography = C Ramachandra Menon
| editing = K Sankunni
| studio = Evershine Productions
| distributor = Evershine Productions
| released =  
| country = India Malayalam
}}
  1977 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by Thiruppathi Chettiyar. The film stars Prem Nazir, Adoor Bhasi, Thikkurissi Sukumaran Nair and Jose Prakash in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
*Prem Nazir 
*Adoor Bhasi 
*Thikkurissi Sukumaran Nair 
*Jose Prakash 
*K. P. Ummer 
*MG Soman 
*Usharani 
*Vidhubala
 
==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Pappanamkodu Lakshmanan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aaromalunnikku || P Jayachandran, Ambili, Chorus, Jayashree || Pappanamkodu Lakshmanan || 
|- 
| 2 || Happy New Year || Ambili || Pappanamkodu Lakshmanan || 
|- 
| 3 || Manam Poleyano || K. J. Yesudas || Pappanamkodu Lakshmanan || 
|- 
| 4 || Swapangalaadyamaay || K. J. Yesudas || Pappanamkodu Lakshmanan || 
|}

==References==
 

==External links==
*  

 
 
 


 