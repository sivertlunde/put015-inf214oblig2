They Call Me Bruce?
{{Infobox film|
  name     = They Call Me Bruce? |
  image          = Theycallmebruce-cover.gif|
  writer         = Tim Clawson |
| starring = {{plainlist|
* Johnny Yune
* Margaux Hemingway
* Pam Huntington
* Ralph Mauro
}}
 | director       = Elliott Hong |
  producer       = |
  distributor    = |
  released   = 1982|
  runtime        = 87 mins. |
  language = English |
  music          = Tommy Vig|
  awards         = |
  budget         = |
}}

:For the Bruce Campbell 2006 film see My Name is Bruce.

They Call Me Bruce? (also known as They Call Me Bruce) is a 1982 comedy action film starring Johnny Yune and Margaux Hemingway. 

The film was written by Tim Clawson and was directed by Elliott Hong.  It was followed later by a sequel, released in 1987, entitled They Still Call Me Bruce which also starred Johnny Yune.

==Plot==
The movie opens with a young boy running to meet his grandfather (played by Yune), who lies dying on his bed.  The young boy sadly explains that he could not find the medicine required to cure his grandfathers ailment and wonders aloud who will take care of him after his grandfather passes away.  His dying grandfather attempts to reassure the young boy, and explains that he should go to United States|America.  He further explains that when he was younger and working as a merchant marine, he met "the most beautiful girl" in America, and tells the young boy that if he goes there, she will take very good care of him.  As the young boy is asking how to find her, his grandfather passes away and the movie fades to black.
 martial artist, Bruce Lee, are having trouble keeping their "boss of bosses" happy, and are trying to come up with the perfect solution to distributing cocaine to all of their clients throughout the United States.  Some previous attempts at moving the drug have resulted in busts, and the boss of bosses is not happy.

Through a series of misunderstandings, Bruce makes it into the local newspaper as a hero, having thwarted an attempted robbery at the local market.  Bruces boss, Lil Pete, sees the newspaper and quickly devises a plan putting Bruce in control of moving the cocaine across the country, using Freddy, a stooge associated with the drug lords, as Bruces limousine chauffeur.  He convinces Bruce (who already wants to go to New York to find the lady his grandfather spoke of) that he should drive to New York, not fly, as flying would rob him of seeing the beautiful countryside.  Bruce agrees and the rest of the movie follows an unknowing Bruce delivering what he thinks to be flour to associates of the gangsters across the country, and the interactions he has with the people on this trip.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Johnny Yune || Bruce/Grandfather
|-
| Margaux Hemingway || Karmen
|-
| Raf Mauro || Freddy
|-
| Pam Huntington || Anita
|-
| Martin Azarow || Big Al
|-
| Tony Brande || Boss of Bosses
|-
| Bill Capizzi || Lil Pete
|-
| René LeVant || Reverend
|}

==Releases and controversy==
The film was given a limited release theatrically in the United States by Film Ventures International in November 1982. Despite never being in more than 325 theaters, the film was a surprise success and grossed $16,894,678 at the box office. 
 PG rating, one of which was the removal of a short scene of nudity involving a woman disrobing while Bruce is in a hot tub. This left many fans of the original unhappy, saying that the removal of the nudity in the scene ruined the sight gag.

A 25th Anniversary DVD edition of They Call Me Bruce was released on June 30th, 2009 by Liberation Entertainment. The film is a high quality transfer from a newly discovered 35mm print, which the studio calls "pristine," and also restored the scenes which were cut from the original DVD release.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 