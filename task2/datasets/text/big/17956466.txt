Bloodhounds of the North
{{Infobox film
| name           = Bloodhounds of the North
| image          = Bloodhounds of the North 1914 newspaperad.jpg
| caption        = A contemporary advertisement for the film and several other films.
| director       = Allan Dwan
| producer       =
| writer         = Arthur Rosson Pauline Bush William Lloyd
| cinematography =
| editing        = Universal Film Manufacturing Company
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent English intertitles
| budget         =
}}
 silent Short short drama Pauline Bush, Lon Chaney. The film is now considered lost film|lost.   

==Cast==
* Murdock MacQuarrie as Mountie Pauline Bush as Embezzlers Daughter
* William Lloyd as The Embezzler
* James Neill as The Refugee Lon Chaney as Mountie
* Allan Forrest as Undetermined Role (uncredited)

==Production notes== Mount Lowe, California. Director Allan Dwan also shot The Honor of the Mounted (also starring Murdock MacQuarrie, Pauline Bush, and Lon Chaney) at the same time.  During filming, Lon Chaney and Arthur Rosson got lost in a canyon and were not located until the end of the day. The cast and crew were also stranded in their cabins for five days due to heavy rains. Dwan had the cast rehearse for an upcoming film, Richelieu (film)|Richelieu, in an effort to save time. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 