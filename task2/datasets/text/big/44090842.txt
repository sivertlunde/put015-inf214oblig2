Agni Kshethram
{{Infobox film 
| name           = Agni Kshethram
| image          =
| caption        =
| director       = PT Rajan
| producer       =
| writer         = AC Trilokchander CP Madhusoodanan (dialogues)
| screenplay     =
| starring       = Prem Nazir Srividya Roja Ramani Jagathy Sreekumar
| music          = KJ Joy
| cinematography = Melly Dayalan
| editing        = G Venkittaraman
| studio         = Deepa Films
| distributor    = Deepa Films
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by PT Rajan and produced by . The film stars Prem Nazir, Srividya, Roja Ramani and Jagathy Sreekumar in lead roles. The film had musical score by KJ Joy.   

==Cast==
*Prem Nazir as Dr. Suresh
*Srividya as Sreedevi
*Roja Ramani as Radha
*Jagathy Sreekumar
*Jose Prakash as Vishwanathan
*Kumarji Ponnadu
*Kainakari Thangaraj
*Kanakadurga as Nurse Shantha
*Ramadevi as Subhashini
*Thrissur Elsy as Nazirs & Shobhanas Mother

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Madhu Alappuzha. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Manjappalunkil || P Susheela, Chorus || Madhu Alappuzha || 
|-
| 2 || Pon Kamalangalum || S Janaki || Madhu Alappuzha || 
|-
| 3 || Thumbappoo Thaalangalil || K. J. Yesudas, P Susheela || Madhu Alappuzha || 
|}

==References==
 

==External links==
*  

 
 
 

 