Kick Ball (film)
{{Infobox film
| image    = 
| caption  = 
| name     = Kick Ball traditional = 蹴鞠
|                          simplified = 蹴鞠 
|                              pinyin = Cùjū}}
| director = Jeffrey Lau
| producer = 
| writer   =  Patrick Tam
| music    = 
| cinematography = 
| editing  = 
| studio   = Dandong Fenghuang Qianyi Film and Television Limited Company
| distributor = 
| released =  
| runtime = 
| country = China
| language = Mandarin
| budget = 
| gross = 
}}
Kick Ball is an upcoming 2015 Chinese comedy film directed by Jeffrey Lau, and starring He Jiong, Gillian Chung, Charlene Choi, Joey Yung, and Patrick Tam. 

==Cast==
* He Jiong as Maocilang
* Gillian Chung as Princess Changping
* Charlene Choi
* Joey Yung Patrick Tam
* Stephy Tang
* Wang Xuebing
* Lam Chi-chung
* Jeffrey Lau

==Production==
Principal photography started on April 8, 2014 in Hengdian World Studios and wrapped in May 2014. 

==Release==
Theatrical release will be in 2015.

==References==
 

 

 
 
 
 
 
 
 
 

 
 