St. Elmo (1910 Vitagraph film)
{{Infobox film
| name           = St. Elmo
| image          = Vitagraph St. Elmo.jpg
| caption        = An advertisement for the film in The New York Dramatic Mirror
| director       =
| producer       = Vitagraph
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English intertitles
}} silent short short drama produced by the Vitagraph.

== Cast ==
*Florence Turner as Edna

== Release and reception ==
The single reel drama, approximately 927 feet long, was released on April 23, 1910.  Vitagraph would announce the film with flair as "a sparkling gem in a surrounding of the most brilliant settings." 

A review in the Moving Picture World would call it an "adequate representation of the main theme of Augusta Evans Wilsons novel of the same name." 
 Thanhouser version of St. Elmo that was released on March 22, 1910. 

== Notes ==
The 1910 edition of St. Elmo by Hurst & Company is sometimes marked as having been drawn from a movie. This is incorrect, it was designed to appeal to theater patrons and copyrighted in January 1910.  Another 1910 publication by M.A Donahue includes photographs marked as having been produced by Lawrence Co. of Chicago. It does not indicate that it is from the Vitagraph production. 

== References ==
 

 
 
 
 
 
 
 
 
 