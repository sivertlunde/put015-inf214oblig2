Woodchipper Massacre
{{Infobox film
| name           =Woodchipper Massacre
| image          =Woodchipper Massacre.jpg
| image size     =
| caption        = Jon McBride
| producer       = Jon McBride
| writer         = Jon McBride
| narrator       =
| starring       = Jon McBride Denice Edeal Tom Casiello Patricia McBride
| music          = Tony Diliberti Michael OKeefe David Toy
| cinematography =
| editing        = Lee Insler Jon McBride Joe Podesta Jr.	
| distributor    = Donna Michelle Productions   Camp Motion Pictures (DVD)
| released       = 1988
| runtime        = 90 min.
| country        =  
| language       =
| budget         = $400
| gross          =
| preceded by    =
| followed by    =
}} American horror Jon McBride, who also directed Cannibal Campout.  The film was also written by McBride. 

==Plot==
 Jon McBride), militaristic religious Rambo Hunting Knife in the mail and Tess will not let him have it. They get into a physical struggle over Tom keeping it, and Aunt Tess accidentally gets fatally stabbed in the stomach. 

The three kids then dismember her body, placing it in the freezer. They then take her body out of the freezer so they can put her in the woodchipper. They grind her body to shreds. They then think its over. But then, Tesss son, Kim (Kim Baily), who was recently released from prison, comes to their home and is told that his mother is "not here" and has "left." He is involved in some kind of money wiring deal and came there to get money from his mother. He decides to take monetary value from the childrens home and threatens them if they dont give it to him. Denice and Tom trick him out to the "expensive" wood chipper, plotting to throw him inside, too. They lure Kim to look inside. Tom turns it on and Kim is pushed in by Jon. 
 raking and mowing the lawn which Jon promised his father he would have done by the time he returned from the business meeting. With only a little bit of time before their fathers return, Jon assists in the raking and mowing. The father then arrives home and sees his children welcoming him on the driveway. All appears normal, and the children get away with the crime. 

The camera pans back to the woodchips on the ground and Aunt Tesss bloody ring is lying on the ground when the film ends.

==Cast==
 Jon McBride as Jon
* Denice Edeal as Denice
* Tom Casiello as Tom
* Patricia McBride as Aunt Tess
* Kim Bailey as Kim
* Perren Page as Father

==Budget==
According to an interview with Jon McBride on the DVD, the films final budget was $400 (not including the cost of the camera). 

==Crew==
 Jon McBride acted, wrote, directed, edited, and composed Woodchipper Massacre. The film took 2 months to completely shoot. It was finished in 1988. 

==Release==
===DVD===

The DVD for Woodchipper Massacre was released in early 2007. The VHS was released in 1990. 

==Reception==
Woodchipper Massacre currently has no Rotten Tomatoes approval rating, but has a Want-To-See score of 100%.   

== References ==
 

==External links==
* 
* 

 
 