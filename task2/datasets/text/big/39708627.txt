Hotel Transylvania 2
{{Infobox film
| name = Hotel Transylvania 2
| image = Hotel Transylvania 2 poster.jpg
| border = yes
| caption = Teaser poster
| director = Genndy Tartakovsky
| producer = Michelle Murdocca   
| screenplay = Robert Smigel 
| starring = Adam Sandler Andy Samberg Selena Gomez Kevin James Steve Buscemi David Spade Keegan-Michael Key Mel Brooks
| music = Mark Mothersbaugh   
| editing =
| studio = Sony Pictures Animation
| distributor = Columbia Pictures
| runtime =
| released =  
| country = United States
| language = English
| budget =
| gross =
}} 3D Computer-animated computer animated fantasy comedy film produced by Sony Pictures Animation. It is the sequel to the 2012 film Hotel Transylvania. It is being directed by Genndy Tartakovsky and written by Robert Smigel. The film stars Adam Sandler, Selena Gomez, Andy Samberg, Mel Brooks, David Spade, Keegan-Michael Key, Kevin James, Steve Buscemi and Fran Drescher. The film is scheduled to be released on September 25, 2015, by Columbia Pictures.

==Plot==
Hotel Transylvania has gone through several changes: the hotel is now opened to human guests; Mavis and Johnny have a baby boy, Dennis, whose lack of any vampire abilities worries Count Dracula.  When Mavis and Johnny go on a visit to Johnnys parents, Dracula calls his friends Frank, Murray, Wayne and Griffin to help him put Dennis through a "monster-in-training" boot camp.  Meanwhile, Vlad, the grumpy vampire father of Dracula, arrives at the hotel and, to his dissatisfaction, finds out that his great-grandson is not a pure blood and that the hotel now accepts humans.   

== Voice cast ==
* Adam Sandler as Count Dracula, the owner and hotel manager of Hotel Transylvania who is Mavis father.
* Andy Samberg as Jonathan ("Johnnystein"), a human who befriended the monsters in the first film. 
* Selena Gomez as Mavis, Draculas "teenage" vampire daughter. 
* Kevin James as Frankensteins monster|Frank/Frankenstein, one of Count Draculas best friends. 
* Steve Buscemi as Wayne, a werewolf who is one of Count Draculas best friends.  Griffin the Invisible Man, one of Draculas best friends. 
* Keegan-Michael Key as Murray, a mummy who is one of Count Draculas best friends. He was previously voiced by CeeLo Green in the first movie.  
* Mel Brooks as Vlad, a vampire who is Count Draculas father and Mavis grandfather.    wife of Frankenstein. 

== Production ==
 .      

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 