Devil Goddess
{{Infobox film
| name           = Devil Goddess
| image          = Devil Goddess poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Spencer G. Bennet
| producer       = Sam Katzman
| writer         = 
| screenplay     = George Plympton
| story          = Dwight Babcock
| based on       =   
| narrator       = 
| starring       = Johnny Weissmuller
| music          = 
| cinematography = Ira Morgan
| editing        = Aaron Stell
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       = October 1955 
| runtime        = 68 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Devil Goddess (1955) is the sixteenth and final Jungle Jim film produced by Columbia Pictures. It features Johnny Weissmuller in his third and last performance as the protagonist adventurer Johnny Weissmuller, and Ed Hinton and William Tannen as the films antagonists, Leopold and Nels Comstock, respectively. The film was directed by Spencer G. Bennet and written by Dwight Babcock and George Plympton.

The film centres on jungle roamer Johnny Weissmuller and his team racing against a few looters for a mystical treasure located in a demon-worshiping land. Filming took place in December 1954. Devil Goddess was theatrically released in the United States in October 1955.

==Plot==
Adventure-seeker Johnny Weissmuller (Johnny Weissmuller) receives a request from Professor Carl Blakely (Selmer Jackson) to collaboratively rescue a certain Professor Dixon (William Blakely) from the Mountain of Explosive Fire in Kirundi. The Kirundi natives belong to a religious tribe worshipping a demon thought to control fire. Pious in nature, they are willing to sacrifice humans just to appease the fire demon, who in fact is Dixon. They are also known to possess mystical items, including a jewel-encrusted sabre. 

Notorious looters Leopold (Ed Hinton) and Nels Comstock (William Tannen), along with their criminal crew, are looking to steal the Kirundi folks treasures. They arrive at the strange land the same time Weissmuller and Blakely do. Weissmuller is tipped off by a contact that his lover Sarabna (Vera M. Francis) is next in line to be sacrificed by the natives. The brave explorer arrives in the nick of time, managing to halt the procession and at the same time finding Dixon. Meanwhile, the looters strike gold but are discovered and killed by the Caucasian-loathing tribesmen. 

The Mountain of Explosive Fire, revealed to be an active volcano, erupts suddenly. Weissmuller evacuates the local people and flees to safety with Blakely, Sarabna, and the fire demon Dixon. In gratitude, the chief presents the team with their precious treasures. Dixon considers donating them to a museum. 

==Production== Paul Marions final film too.  He took the role of "Kimba", Weissmullers pet ape, in Devil Goddess. 

The film was directed by Spencer G. Bennet with assistance from Leonard Katzman. Sam Katzman was in charge of production for Columbia Pictures, while George Plympton wrote the screenplay based on a story by Dwight Babcock. Ira Morgan signed on as cinematographer. The set decorator was Sidney Clifford. Mischa Bakaleinikoff headed the musical direction, and Aaron Stell edited the film. Principal photography was completed in about a weeks time. It officially began on December 14, 1954, and ended on December 21, 1954. 

Not all of the footage in Devil Goddess is original. Archived footage from preceding Jungle Jim films, including Mark of the Tiger (1950), Pygmy Island (1950), Voodoo Tiger (1952), Killer Ape (1953), and Savage Mutiny (1953), is featured in the film. 

==Release==
The film was officially released in the North American cinemas in October 1955.  It was critiqued as a "silly jungle film" by the Motion Picture Herald, while the magazine Variety (magazine)|Variety wrote that it was an "almost amateurish attempt at making a formula theme pay off".  In evaluating the film in his 2012 book Columbia Pictures Movie Series, 1926—1955: The Harry Cohn Years, Gene Blottner dubbed Weissmullers acting in the film as "lack-luster", concluding that it "all life is gone from the series with this entry".  

==Notes==
 

==References==
 

==Bibliography==
*  }}
*  }}

==External links==
*  
*  
 
 
 
 
 
 
 
 
 
 
 