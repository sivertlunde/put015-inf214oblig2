Emergency Call
 
{{Infobox film
| name           = Emergency Call
| image          = Emergency Call 1952.jpg
| caption        = DVD cover
| director       = Lewis Gilbert
| producer       = Ernest G. Roy
| writer         = Lewis Gilbert Vernon Harris Jack Warner Anthony Steel Joy Shelton Sid James
| music          = Wilfred Burns
| cinematography = Wilkie Cooper
| editing        = Charles Hasse
| distributor    = 
| released       = May 24, 1952
| runtime        = 
| language       = English
| country        = United Kingdom
| budget         = 
}}
 British film Nettlefold Films. Jack Warner Anthony Steel, Joy Shelton and Sid James as a dubious boxing promoter.      

The film was remade in 1962 as Emergency starring Glyn Houston.   

==Synopsis== blood donors type of blood to save the life of a young girl suffering from leukaemia. The doctor in charge of treating the girl enlists the assistance of police officer Inspector Lane in order to assist in the search for suitable donors. The three donors are each from very different backgrounds, a white Boxing|boxer, a black sailor, and finally a murderer who has been on the run from the police for a number of years. The boxers donation is fairly straightforward, having only to avoid his manager, the sailors donation is more complicated, following a World War II|war-time incident where a dying Nazi soldier refused to accept his offer of a donation which he attributes to racism, he initially refuses to donate, until it is explained to him that the Nazi officer refused his donation for reasons that can be attributed to the Nazis Master race ideology.
 assumed name. The police eventually locate the man and he suffers a gunshot injury. He must choose to donate the last pint of blood needed and die at the scene from blood loss, or to refuse to donate in order to receive treatment in hospital but with the knowledge he will surely be found guilty of murder at trial and sentenced to death penalty|death. The criminal chooses to donate and the young girl survives.  

==Cast== Jack Warner as Inspector Lane Anthony Steel as Dr. Carter
* Joy Shelton as Laura Bishop
* Sid James as Danny Marks Earl Cameron as George Robinson John Robinson as Dr. Braithwaite
* Thora Hird as Mrs. Cornelius
* Eric Pohlmann as Flash Harry
* Sydney Tafler as Brett
* Geoffrey Hibbert as Jackson
* Henry Hewitt as Mr. Wilberforce
* Vida Hope as Brenda
* Avis Scott as Marie
* Freddie Mills as Tim Mahoney
* Peggy Bryan as Ward Sister
* Bruce Seton as Sergeant Bellamy 
* Anna Turner as Mrs. Jackson 
* Nosher Powell as Boy Booth 
* Campbell Singer as  Sergeant Phillips 
* Nigel Clarke as Superintendent Travers

==References==
 

==External links==
*  
*  

 

 
 
 
 
 