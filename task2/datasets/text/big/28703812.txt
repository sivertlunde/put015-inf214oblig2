Microphone (film)
{{Infobox Film
| name           = Microphone
| image          = Microphone_Film_Poster_small.jpg
| caption        = International official poster of the film
| director       = Ahmad Abdalla
| producer       =  
| writer         = Ahmad Abdalla
| starring       =  
| distributor    = Film Clinic 
| released       =  
| runtime        = 122 minutes
| country        = Egypt
| language       = Arabic
| budget         = 
| gross          = 
}} Tanit dOr from Carthage Film Festival|Journées cinématographiques de Carthage. In addition to Best Editing Award from Dubai International Film Festival  in 2010.

Microphone is Ahmad Abdallas second feature film, following Heliopolis (film)|Heliopolis.

== Plot == graffiti artists Alexandria rather than from Cairo, Egypts overpopulated capital.  Alexandria and the intricate details of their lives. It is the first Egyptian movie to features the local skateboarding scene 

== Festival and awards == The Golden Carthage film festival in October 2010.  
===2011===
* Won— Großer Preis der Stadt Freistadt from Festival Der neue Heimatfilm
* Won— Golden Tulip from Istanbul International Film Festival
* Won— Griot Best Film Award of African Film Festival of Tarifa
* Won— Special Mention from Granada International Film Festival Cines del Sur
===2010===
* Won— Best Arabic-language film Award from Cairo International Film Festival  
*Won— Best Film from Alexandria International Film Festival 
*Won— Tanit dor from Carthage Film Festival|Journées cinématographiques de Carthage
*Won— Best Editing Award  from Dubai International Film Festival
*Nominated— Toronto International Film Festival
*Nominated— London International Film Festival
*Nominated— Golden Alexander Thessaloniki International Film Festival Festival International du Film dAmour de Mons in Brussels, Belgium

== Cast ==
* Khaled Abol Naga (Khaled)
* Menna Shalabi  (Hadeer)
* Yosra El Lozy (Salma) Hany Adel  (Hany)
* Ahmed Magdi   (Magdi)
* Atef yousef
* Aya Tarek  (Aya)
*Yassin Koptan (Yassin)

== References ==
  

==External links==
* 
* 
*  

 
 
 
 