Everlasting Regret
 
 
{{Infobox film
| name           = Everlasting Regret
| image          = EverlastingRegret.jpg
| caption        = DVD cover
| writer         = Novel:  
| based on =   Tony Leung, Jun Hu, Daniel Wu
| director       = Stanley Kwan
| producer       = Jackie Chan Willie Chan Fang Jun Xu Pengle Chen Baoping
| distributor    = Hong Kong: Golden Scene   Festive Films
| released       =  
| runtime        = 115 minutes
| country        = Hong Kong Mandarin
| budget         = 
| music          = 
| awards         = 
}} Hong Kong directed by The Song of Everlasting Sorrow, a novel by Wang Anyi, about a womans turbulent life in 20th century Shanghai, China. The Chinese title of the book and film is taken after the Changhen ge, a Tang dynasty poem. The film participated in the 62nd Venice International Film Festival and was shown at the 41st Chicago International Film Festival.

It is also known by the title Song of Everlasting Regrets.

==Cast==
*Sammi Cheng &ndash; Wang Qiyao （王琦瑤）
*Tony Leung Ka-fai &ndash; Mr Cheng （程先生）
*Hu Jun &ndash; Officer Li （李主任）
*Daniel Wu &ndash; Kang Mingxun （康明遜）
*Huang Jue &ndash; Kela （老克蠟）
*Su Yan &ndash; Lili （蔣麗莉） Huang Yi &ndash; Weiwei （薇薇）
*Yumiko Cheng &ndash; Yonghong （張永紅）
*Lan Yan as Duan Wen Fang （段文芳） Li Wu &ndash; Ms Mu （穆小姐）
*Jing Wu &ndash; Qiyaos Mother （王母）

==See also==
*To Live, To Love, a 2006 TV adaptation of the novel produced by Kwan
*Jackie Chan filmography

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 


 