The Man Without Nerves
{{Infobox film
| name           = The Man Without Nerves
| image          = 
| image_size     = 
| caption        = 
| director       = Harry Piel   Gérard Bourgeois
| producer       = Harry Piel
| writer         = Edmund Heuberger   Herbert Nossen    
| narrator       = 
| starring       = Harry Piel   Dary Holm   Albert Paulig   Marguerite Madys
| music          = 
| editing        =
| cinematography = Georg Muschner    Gotthardt Wolf
| studio         = Harry Piel for Hape-Film (Berlin)
| distributor    = Bayern-Film
| released       = 5 December 1924
| runtime        = 
| country        = Germany 
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent crime film directed by Harry Piel, assisted by Gérard Bourgeois and starring Piel, Dary Holm and Albert Paulig. It premiered in Berlin on 5 December 1924. 

==Cast==
* Harry Piel as Der Mann ohne Nerven 
* Dary Holm as Aud Egede Christensen 
* Albert Paulig as Henry Ricold 
* Marguerite Madys as Yvette 
* Paul Guidé as Hector Marcel 
* Denise Legeay as Lizzie 
* José Davert as Jack Brown 
* Hermann Picha as Der Notar des Herzogs

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 


 