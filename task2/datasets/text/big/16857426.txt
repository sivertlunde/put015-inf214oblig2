Life Mein Kabhie Kabhiee
 
{{Infobox film
| name           = Life Mein Kabhie Kabhiee
| image          = Life_mein_kabhi_kabhee.jpg
| caption        = Theatrical release poster
| director       = Vikram Bhatt
| producer       = Ajay Acharya Gordhan Tanwani
| writer         = 
| screenplay     = Manoj Tyagi
| dialogue       =
| story          = Girish Dhamija
| based on       =  
| starring       = Aftab Shivdasani Dino Morea Nauheed Cyrusi Anjori Alagh Nikita Anand Rajat Bedi Mohnish Bahl
| music          = Lalit Pandit
| cinematography = Pravin Bhatt
| editing        = Akshay Mohan Sandeep Francis
| studio         = 
| distributor    = Gangani Multimedia Corporations Srishti Creations
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Life Mein Kabhi Kabhiee is a 2007 Bollywood film, directed by Vikram Bhatt. It was released on 13 April 2007. Produced by Baba Films banner, it is a social Thriller (genre)|thriller.

==Plot==
The film begins with Manish Gupta (Aftab Shivdasani) in a press conference of his best-selling book " Life Mein Kabhie Kabhiee". It then goes into a flashback where the 5 friends (including himself)- Rajeev Arora (Dino Morea), Jai Gokhale (Sameer Dattani), Ishita Sharma (Anjori Alagh)and Monica Seth (Nauheed Cyrusi) are all fresh out of college. They are caught drunk and are in a lock-up. In the meantime they make a bet to determine who will find the most fulfillment in life. The winner will be the person who has found the most happiness in life within 5 years time.

Rajeev, the younger brother of a business tycoon Sanjeev Arora (Mohnish Behl), is an aspiring CEO and is eager to make his first million. He starts on his own after ideological disputes with Sanjeev. He joins the airline business and keeps on doing well until a stock market crash almost ruins him. When under serious debt, he gets the opportunity to clear them but has to deceive his brother for that. He initially agrees but then refuses to accept the money. It is then revealed that the money is of Sanjeev Aroras.

Jai is the son of a respectable co-operative bank chairman and wants to become a powerful politician; Jai joins politics against his widowed mothers will. He rapidly climbs up the party ranks but to do so he has to do things which are against his will but important to beat his partys opposition. He unwillingly lets the opposition leader die of heart attack when he could have saved him. As a result, his mental condition is severely hampered and spends many sleepless nights. He then has to visit a psychiatrist.

Ishita is the contributor of a leading gossip magazine Scandal and wants to make loads of cash. Following this, she traps a business tycoon named Raj Gujral (Raj Zutshi) with her beauty and strategy. She leaks her photos with him on the front page of the magazine Scandal. When his wife sees it, she divorces him. Raj looks forward to woo Ishita but she doesnt take his calls. Finally she marries him after knowing about their divorce.

Monica longs to hit big on the Bollywood screen. Although she has a boyfriend- Mohit Aggarwal (Anuj Sawhney) she doesnt hesitate in having an affair with a filmstar called Rohit Kumar (Rajat Bedi)and even sleeps with him. She keeps on lying until Mohit discovers her sleeping with Rohit Kumar. Mohit commits suicide and shortly afterwards, dies. Next day, she confesses everything in front of the media and takes all the blame for his death. She goes abroad afterwards.

Manish a struggling writer, gets happily married and has a daughter. Finally, he writes the best-selling Life Mein Kabhie Kabhiee. The book was the story about the lives of the 5 best friends. How their lives changed and how they understood the significance of their lives.

==Cast==
* Dino Morea: Rajiv Arora
* Aftab Shivdasani: Manish Gupta
* Anjori Alagh: Ishita Sharma
* Nauheed Cyrusi: Monica Seth
* Sameer Dattani: Jai Gokhale

===Supporting cast===
* Anuj Sawhney: Mohit (Monicas boyfriend)
* Mohnish Behl: Sanjiv Arora (Rajivs elder brother)
* Koel Puri: Richa (Manishs wife)
* Raj Zutshi: Raj Gujral (Ishitas husband)
* Rajat Bedi: Rohit Kumar
* Nikita Anand: Rajivs girlfriend
* Pinky Harwani: Jais counsellor

==Music==
* Hum Khushi Ki Chah Mein            - Singer : Zubin Garg
* Gehra Gehra                        - Singer : Sunidhi Chauhan.
* No Problem                         - Singer : Remo Fernandes
* Hum Khushi Ki Chah Mein (Rock Mix) - Singer : Alka Yagnik, Zubeen Garg

==Crew==
* Director: Vikram Bhatt
* Producer:  Gordhan Tanwani
* Music director:  Lalit Pandit
* Lyricist:  Sameer
* Choreographer:  Piyush Panchal, Raju Khan
* Screenplay:  Manoj Tyagi
* Sound:  Uday Inamati
* Dialogue:  Girish Dhamija
* Story:  Manoj Tyagi

==External links==
*  

 

 
 
 
 
 
 