Set It Off (film)
 
{{Infobox film
| name           = Set It Off
| image          = Set it off poster.jpg
| caption        = Theatrical release poster
| director       = F. Gary Gray
| producer       = Oren Koules Dale Pollock
| screenplay     = Takashi Bufford Kate Lanier
| story          = Takashi Bufford Jada Pinkett Queen Latifah Vivica A. Fox Kimberly Elise John C. McGinley Blair Underwood
| music          = Christopher Young
| cinematography = Marc Reshovsky
| editing        = John Carter
| distributor    = New Line Cinema 
| released       = November 6, 1996 
| runtime        = 123 minutes 
| language       = English 
| budget         = $9,000,000   
| gross          = $41,590,886 
}}
 crime action action film directed by F. Gary Gray, and written by Kate Lanier and Takashi Bufford. The film stars Jada Pinkett Smith, Queen Latifah, Vivica A. Fox and Kimberly Elise (in her theatrical acting debut). It follows four close friends in Los Angeles, California, who decide to plan and execute a bank robbery. They decide to do so for different reasons, although all four want better for themselves and their families. The film became a critical and box office success, grossing over $41 million against a budget of $9 million.    

== Plot ==
Francesca "Frankie" Sutton is a Los Angeles bank teller who witnesses a robbery. Shortly after the incident, the bank fires Frankie after the police uncover a connection between her and Darnell, one of the three robbers; despite the fact that Frankie knew nothing of the robbery Darnell had planned. Later, Frankie reunites with her three best friends, Lida "Stoney" Newsome, Cleopatra "Cleo" Sims, and Tisean "T.T." Williams in the projects and is furious because she was fired. Cleo states they should rob a bank themselves to collect all of the money and get out of the projects while being able to have what they want. Frankie agrees, while the other two friends arent following along with what they are saying. Eventually, Frankie decides to work at a janitorial company with her friends even though they are treated with no respect from their boss, Luther, and receive little pay. 

Stoneys brother Stevie was wrongfully gunned down by the police after they mistake him for one of the men involved in the previous robbery. The incident prompts Frankie to again suggest that they should rob a bank themselves. While Stoney is happy to go along with the plan, T.T. is initially opposed to the idea. However, when an accident at work leads to her toddler being taken away by social services, T.T. commits to the plot as well to get the money she need to receive her child back.
 LAPD detective Strode. The women stash the money in an air vent at one of their work sites. However, when they show up for work another day, they realize that their boss, Luther, has discovered the money and fled. The women track down Luther and find him at a motel with a woman.  When they confront Luther, he is killed by T.T. when he pulls a gun on Cleo. Afterwards, Cleo takes the womans drivers license to prevent her from contacting the police and though she informs them about the murder, a glare from Cleo intimidates her to remain quiet.

With their money gone, the women rob another bank, which happens to be the same bank where Keith, Stoneys lover, works. Before they make it out of the bank, Strode and his partner arrive and order them to drop their weapons, but as they do, a bank security guard shoots at Tisean. Stoney and Cleo open fire at the security guard and carry Tisean to the getaway car with Frankie behind the wheel. As Tisean dies in Stoneys arms en route to a hospital, the three women decide to split up.

Cleo is the second of the women to be killed after the police catch up to her. The police catch up to Frankie and order her to surrender. However, Frankie pulls a gun on Strode and tries to run away, but is shot and killed by an officer. Stoney, in the meantime, boards a bus heading to Mexico, but she painfully witnesses the killing of Frankie. Strode spots Stoney on the bus, but lets her go because he realized he was the cause of everything. After cutting her hair in a small motel where she has taken refuge, she calls Keith but does not say anything.  While she does not tell him where she is, she assures him that she is all right and thanks him. The movie ends with her driving through the mountains with the stolen money in tow. It is presumed that she took the money and escaped permanently, but leaving her old life behind her.

==Cast== Jada Pinkett as Lida "Stoney" Newsome
* Queen Latifah as Cleopatra "Cleo" Sims
* Vivica A. Fox as Francesca "Frankie" Sutton
* Kimberly Elise as Tisean "T.T." Williams
* John C. McGinley as Detective Strode
* Blair Underwood as Keith Weston
* Ella Joyce as Detective Waller
* Van and Vincent Baum as Jajuan Williams Charles Robinson as Nate Andrews
* Dr. Dre as Black Sam WC as Darnell (Bank robber) Charles Walker as Captain Fredricks
* Mark Thompson (TV) as TV Anchor George Fisher as Cop
* F. Gary Gray as Guy driving low-rider
* Geoff Callan as Nigel
* Samantha MacLachlan as Ursula
* Natalie Desselle-Reid as Tanika
* Chaz Lamar Shepherd as Stevie Newsome
* Thomas Jefferson Byrd as Luther
* Kayla Hickson as accountant 1
* Samuel Monroe Jr. as Lorenz

==Critical reception and box office==
 
A critical success, Set It Off is also popular with audiences.  On   stated that Set It Off is "a lot more" than a thriller about four black women who rob banks. Comparing it to   compared Set It Off to Thelma & Louise, stating, "In formulaic Hollywood terms, Set It Off might be described as Thelma and Louise Ride Shotgun in the Hood While Waiting to Exhale. A pop psychologist might translate the story into a fable called Women Who Rob Banks and the Society That Hates Them." He added that among "the long list of Hollywood heist movies that make you root for its criminals to steal a million dollars and live happily ever after, F. Gary Grays film Set It Off is one of the most poignantly impassioned," and that " f this messy roller coaster of a film often seems to be going in several directions at once, it never for a second loses empathy" for the female robbers.    

James Berardinelli said that if Set It Off owes any debt to films, those films are Thelma & Louise and Dead Presidents, rather than Waiting to Exhale. He stated that " heres a freshness and energy in the way director F. Gary Gray attacks this familiar material that keeps Set It Off entertaining, even during its weakest moments" and that " he concept of four black action heroines makes for a welcome change in a genre that is dominated by: (a) rugged white males with a perpetual five oclock shadow, (b) rugged white males who speak English with an accent, and (c) rugged white males with the acting ability of a fence post." Berardinelli added that although " he film doesnt get off to a promising start" and " he first half-hour, which details the various characters motives for becoming involved in a bank robbery, is unevenly scripted," and that some aspects of the plot are contrived, " nce the setup is complete, however, things shift into high gear. The remainder of the film, which includes several high-adrenaline action sequences and some slower, more dramatic moments, is smoothly-crafted. There are occasional missteps, such as an out-of-place Godfather parody, but, in general, Set It Off manages to rise above these."   

On a budget of $9 million and R-rated, Set It Off grossed $36,461,139 in the U.S. and Canada, $5,129,747 internationally, and $41,590,886 worldwide.  Tribute (magazine)|Tribute magazine stated that it is New Line Cinemas highest-grossing film of 1996, and that it won Gray a Black Film Award for Best Director, and the Special Jury Prize at the Cognac Film Festival. 

==Soundtrack==
{{Infobox Album  
| Name        = Set It Off: Music From the New Line Cinema Motion Picture
| Type        = soundtrack
| Artist      = Various artists
| Cover       =  
| Released    = September 24, 1996
| Recorded    = 1995&ndash;1996 Hip hop, Contemporary R&B|R&B
| Length      =  Eastwest
| Producer    = Dr. Dre Organized Noize, Barry Eastmond, DJ U-Neek, DJ Scratch, DJ Mark the 45 King, Pras Michel, Delite, Keith Crouch, DJ Rectangle
| Last album  = Friday (soundtrack) (1995)
| This album  = Set It Off (soundtrack) (1996)
| Next album  = Next Friday (soundtrack) (1999)
}}
{{Album ratings
| rev1 = Allmusic
| rev1Score =    
| noprose = yes
}}

The soundtrack was released on September 24, 1996 by Eastwest Records and featured production from several of hip hop and R&Bs top producers such as Organized Noize, DJ U-Neek and DJ Rectangle. The soundtrack was a huge success making it to number four on the Billboard 200|Billboard 200 and number three on the Top R&B/Hip-Hop Albums and featured seven charting singles "Set It Off", "Dont Let Go (Love)", "Days Of Our Livez", "Angel", "Come On", "Let It Go" and "Missing You". All of the singles had music videos made for them. The track "The Heist" by Da 5 Footaz also had a music video made, even though it was not released as a single. On November 12, 1996 the album was certified platinum by the RIAA.

==Track listing==
{{Track listing
| extra_column    = Artist/Performer
| writing_credits = yes
| title1          = Set It Off
| writer1         = Ivan Martias / Andrea Martin / Organized Noize /Dana Owens / Steve Standard
| extra1          = Organized Noize featuring Queen Latifah
| length1         = 5:02 Missing You
| writer2         = Gordon Chambers / Barry J. Eastmond
| extra2          = Brandy Norwood|Brandy, Tamia, Gladys Knight & Chaka Khan
| length2         = 4:23
| title3          = Dont Let Go (Love)
| writer3         = Ivan Martias / Andrea Martin / Organized Noize
| extra3          = En Vogue
| length3         = 4:51
| title4          = Days of Our Livez
| writer4         = Bone Thugs-N-Harmony
| extra4          = Bone Thugs-n-Harmony
| length4         = 5:49
| title5          = Sex Is on My Mind
| writer5         = S. Brown
| extra5          = Blulight
| length5         = 4:40
| title6          = Live to Regret
| writer6         = Trevor Smith / George Spivey
| extra6          = Busta Rhymes
| length6         = 4:18
| title7          = Angel
| writer7         = Carolyn Franklin / Sonny Saunders
| extra7          = Simply Red
| length7         = 3:39
| title8          = Name Callin (Foxy Brown Diss)
| writer8         = Dana Owens / Nichelle Strong
| extra8          = Queen Latifah
| length8         = 3:50
| title9          = Angelic Wars
| writer9         = Robert Barnett / Fred Bell / Willie Knighton / Organized Noize / Jamahr Williams
| extra9          = Goodie Mob
| length9         = 3:21
| title10         = Come On
| writer10        = Darrell "Delite" Allamby / Billy Lawrence
| extra10         = Billy Lawrence featuring MC Lyte
| length10        = 4:09 Let It Go
| writer11        = Keith Crouch / Glenn McKinney / Roy Dog Pennon
| extra11         = Ray J
| length11        = 4:53
| title12         = Hey Joe
| note12          = Live
| writer12        = Billy Roberts Seal
| length12        = 4:20
| title13         = The Heist
| writer13        = Jamali Cathorn / Ericka Martin / Kim Savage Da 5 Footaz
| length13        = 4:04
| title14         = From Yo Blind Side
| extra14         = X-Man featuring H Squad
| length14        = 4:04
}}

"Up Against the Wind" (runtime&nbsp;&ndash; 4:28), sung by Lori Perry and produced by Christopher Young, is not included in the soundtrack. 

{|class="wikitable"  Year
!rowspan="2"|Album Peak chart positions Certifications
|-
!width=40| Billboard 200|U.S. 
!width=40| Top R&B/Hip-Hop Albums|U.S. R&B 
|- 1996
|Set It Off
* Released: September 24, 1996 East West 4
|align="center"|3
|
* Recording Industry Association of America|US: Platinum
|-
|}

==Awards and nominations== 1997 Acapulco Black Film Festival
* Best Director: F. Gary Gray (won)
 1997 Independent Spirit Awards
* Best Supporting Female: Queen Latifah (nominated)
 1997 NAACP Image Awards
* Outstanding Lead Actress in a Motion Picture: Queen Latifah and Jada Pinkett Smith (nominated)
* Outstanding Supporting Actor in a Motion Picture: Blair Underwood (nominated)

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 