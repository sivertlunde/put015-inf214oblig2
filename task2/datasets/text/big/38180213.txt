Le soleil se lève en retard
{{Infobox film
| name           = Le soleil se lève en retard
| image          = 
| caption        = 
| director       = André Brassard
| producer       = Pierre Lamy
| writer         = Michel Tremblay André Brassard
| starring       = Rita Lafontaine
| music          = 
| cinematography = Alain Dostie
| editing        = 
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = Canada
| language       = French
| budget         = 
}}

Le soleil se lève en retard is a 1977 Canadian drama film directed by André Brassard. It was entered into the 10th Moscow International Film Festival.   

==Cast==
* Rita Lafontaine as Gisèle Lapointe
* Denise Filiatrault as Marguerite Lapointe-Beaulieu
* Huguette Oligny as Marie Lapointe
* Claude Gai as Jacques Lapointe
* Jean Mathieu as Joseph Lapointe
* Danièle Panneton as Danielle Lapointe
* François Tassé as Claude Beaulieu
* Edgar Fruitier as Le patron de lagence
* Sylvie Heppel as La femme du patron de lagence
* Andrée St-Laurent as La secrétaire de lagence
* Paule Baillargeon as Ginette
* Josée Labossière as Mariette

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 