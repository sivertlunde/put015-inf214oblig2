Car 54, Where Are You? (film)
{{Infobox film
| name           = Car 54, Where Are You?
| image          = Car54film.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Bill Fishman
| producer       = Robert H. Solo
| screenplay     = {{Plain list |
*Erik Tarloff
*Ebbe Roe Smith
*Peter McCarthy
*Peter Crabbe
}}
| based on       =  
| starring       = {{Plain list |
*David Johansen John McGinley
*Fran Drescher
*Nipsey Russell
*Rosie ODonnell Al Lewis
}}
| music          = {{Plain list | Pray for Rain
*Bernie Worrell
}}
| cinematography = Rodney Charters
| editing        = {{Plain list |
*Alan Balsam Earl Watson
}}
| distributor    = Orion Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $10.7 million
| gross          = $1,238,080
}} television series of the same name starring Joe E. Ross and Fred Gwynne that ran from 1961 to 1963. 
 Al Lewis, whose officer Schnauser now spends his time watching TV reruns of The Munsters (in which Lewis and Gwynne also starred).

The film was originally produced as a musical comedy but released without the filmed musical interludes. 

==Plot==
 
Partnered in Car 54 are the amiable, unreliable Gunther Toody and the prim, proper Francis Muldoon, who wants to know, among other things, if Toody has been helping himself to free kebab from the neighborhood food vendors.

As everyone gets on their case, including Dave Anderson, their captain, an assignment comes their way. Toody and Muldoon are told to protect citizen Herbert Hortz, who apparently has incurred the wrath of the local crime godfather, the claustrophobic Don Motti. And while the cops from Car 54 ineptly do their jobs, Toody gets an earful from his wife, Lucille, and is hot and bothered by a woman all their colleagues seem to have already frisked or handcuffed, the vixen Velma Velour.

==Cast==
* David Johansen as Officer Gunther Toody
* John C. McGinley as Officer Francis Muldoon
* Fran Drescher as Velma Velour
* Nipsey Russell as Police Captain Dave Anderson
* Rosie ODonnell as Lucille Toody Al Lewis as Officer Leo Schnauser 
* Daniel Baldwin as Don Motti
* Jeremy Piven as Herbert Hortz
* Tone Loc as Handsome cab driver
* The Ramones as themselves
* Penn and Teller as the Luthers

==Production==
Though the film was shot in 1990, it was edited several times and as a result wasnt released until 1994. It was originally shot as a musical, but most of the musical numbers were cut from the released film. Thought he film is considered a reboot of the television series, Nipsey Russell and Al Lewis appear as older versions of their roles from the original series.

==Reception==
The film received universally negative reviews and currently has a 0% rating on Rotten Tomatoes based on 14 reviews. 
 Worst Supporting Exit to The Flintstones;  and won a Stinkers Bad Movie Awards for Worst Resurrection of a TV Show and was nominated for Worst Picture and Worst Actress (ODonnell), along with Exit to Eden and The Flintstones. 

Rosie ODonnell has said in interveiws to never rent the film.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 