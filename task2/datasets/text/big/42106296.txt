They'll Come Back
{{Infobox film
| name           = Theyll Come Back
| image          = Theyll Come Back Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Marcelo Lordello
| producer       = Marilha Assis Mannuela Costa
| writer         = Marcelo Lordello
| starring       = Maria Luiza Tavares Geórgio Kokkosi Elayne de Moura Mauricéia Conceição
| music          = Caçapa
| cinematography = Ivo Lopes Araújo
| editing        = Eduardo Serrano
| studio         = Trincheira Filmes
| distributor    = Vitrine Filmes
| released       =  
| runtime        = 100 minutes
| country        = Brazil
| language       = Portuguese
| budget         = R$ 247,000   
| gross          = 
}} Brazilian drama 45th Festival de Brasília do Cinema Brasileiro. 

==Plot==
Cris (Maria Luíza Tavares) and Peu (Georgio Kokkosi), her older brother, are left on the side of a road by their own parents. The brothers were punished for fighting constantly during a trip to the beach. After a few hours, realizing that their parents will not return, Peu part in search of a gas station. Cris remains in place for a whole day and, without news of her parents or brother, decides to go by herself the way back home.  

==Cast==
*Maria Luiza Tavares as Cris
*Geórgio Kokkosi as Peu
*Elayne de Moura as Elayne
*Mauricéia Conceição as Fátima
*Jéssica Gomes de Brito as Jennifer
*Irma Brown as Pri
*Germano Haiut as Grandfather
*Teresa Costa Rêgo as Grandma
*Clara Oliveira as Geórgia

==References==
 

==External links==
*  

 
 
 
 
 
 