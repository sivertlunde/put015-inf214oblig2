Kafka (film)
{{Infobox film
| name           = Kafka
| image          = Kafka film.jpg
| caption        = Theatrical release poster
| director       = Steven Soderbergh
| producer       = Harry Benn Stuart Cornfeld
| writer         = Lem Dobbs
| starring       = Jeremy Irons Theresa Russell Joel Grey Ian Holm Jeroen Krabbé Armin Mueller-Stahl Alec Guinness
| music          = Cliff Martinez
| cinematography = Walt Lloyd
| editing        = Steven Soderbergh Baltimore Pictures Pricel Renn Productions
| distributor    = Miramax Films
| released       = November 15, 1991
| runtime        = 98 minutes
| language       = English
| country        = France, United States
| budget         = $11,000,000
| gross          = $1,059,071
}} mystery Thriller thriller film The Castle and The Trial), creating a Kafkaesque atmosphere. It was written by Lem Dobbs, and stars Jeremy Irons in the title role, with Theresa Russell, Ian Holm, Jeroen Krabbé, Joel Grey, Armin Mueller-Stahl, and Alec Guinness. 
 Naked Lunch. 

== Plot summary==
Set in the city of Prague in 1919, Kafka tells the tale of an insurance worker who gets involved with an underground group after one of his co-workers is murdered. The underground group, responsible for bombings all over town, attempts to thwart a secret organization that controls the major events in society. He eventually penetrates the secret organization in order to confront them.

== Cast ==
* Jeremy Irons as Mr. Kafka
* Theresa Russell as Gabriela
* Joel Grey as Mr. Burgel
* Ian Holm as Doctor Murnau
* Jeroen Krabbé as Mr. Bizzlebek
* Armin Mueller-Stahl as Inspector Grubach
* Alec Guinness as Chief clerk
* Brian Glover as Castle henchman Keith Allen as Assistant Ludwig
* Simon McBurney as Assistant Oscar
* Robert Flemyng as Keeper of the Files
* Ion Caramitru as Solemn anarchist
* Josef Abrhám as Friend of Kafka
* Guy Fithen as Friend of Kafka
* Ondrej Havelka as Friend of Kafka
* Jerome Flynn as Castle attendant
* Ewan Stewart as Castle attendant
* Jim McPhee as Castle attendant
* Petr Jákl as Quarry labourer David Jensen as Laughing man

==Alternate version== inserts were Side Effects, and he plans to dub the film into German and release both the original and new version together. 

==References==
 

== External links ==
* 
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 