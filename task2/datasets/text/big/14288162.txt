Lonesome Luke, Plumber
{{Infobox film
| name           = Lonesome Luke, Plumber
| image          =
| image size     =
| caption        =
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| narrator       =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent film English intertitles
| budget         =
| preceded by    =
| followed by    =
}}
 1917 short short comedy film starring Harold Lloyd.

==Cast==
* Harold Lloyd - Lonesome Luke
* Snub Pollard
* Bebe Daniels
* Bud Jamison
* Gilbert Pratt
* Max Hamburger
* Arthur Harrison
* Sammy Brooks
* W.L. Adams
* David Voorhees
* Clara Lucas
* Pearl Novci
* Gus Leonard

==See also==
* Harold Lloyd filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 