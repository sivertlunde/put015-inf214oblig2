Barbie: A Fashion Fairytale
{{Infobox film
| name        = Barbie: A Fashion Fairy Tale 
                French name : Barbie et la magie de la mode 
| image       = Barbie A Fashion Fairytale poster.jpg
| image size  = 
| caption     = Theatrical Release Poster
| director    = William Lau
| producers   = Shawn McCorkindale & Shelley Dvi-Vardhana 
| writer      = 
| starring    =Diana Kaarina Adrian Petriw Tabitha St. Germain   Patricia Drake   Alexa Devine
| music       = B.C. Smith  Mattel Entertainment
| distributor = Universal Studios Home Entertainment (DVD)
| released    =  
| runtime     = 79 minutes
| country     = United States
| language    = English and French
| budget      =
| gross       = 
}} Barbie film series.  It was released on September 14, 2010.  This is the second Barbie film in which Kelly Sheridan does not voice Barbie; instead, Diana Kaarina takes her role. 

==Plot summary== classic story by Hans Christian Andersen, but she is fired when she questions the directors innovations. After reading unkind comments about her on the Internet and beginning to doubt her future as an actress, Barbie receives a phone call from Ken, saying that he wants to break up with her immediately. Advised by her best friends, Teresa and Grace, to get away for a while, Barbie goes to Paris to visit her Aunt Millicent, who runs a fashion house. 
 
Teresa and Grace confront Ken about the break-up, and they all realize that Barbies arch-rival, Raquelle, made the call, using a recording of Ken reading from a script. Ken sets off for Paris to show Barbie he loves her, but he encounters many problems before getting to her. 

In Paris, Barbie learns that, through the machinations of a rival designer, Jacqueline, Millicent has just sold her business. With her new friend, Alice, Barbie meets the wingless but magical beings called flairies, who have the power to add sparkle to any outfit. To save Aunt Millicents fashion house, which is also the source of the flairies powers, Barbie and company set up and host a fashion show. During the show, the flairy Glimmer, who always struggled with her magic, finally manifests her powers and transforms Barbies final outfit into a glittering three-tiered rose pink gown. Ken finally arrives and jumps onto the catwalk to tell Barbie he loves her. 

Lilliana Roxelle, Pariss top fashion critic, congratulates them on a spectacular show and invites them to her Black, White, and Pink party that night. Jacqueline finally appreciates their work and also apologizes to everybody for all the trouble she caused and promises to change. Glimmer harnesses her magic a second time and transforms the limousines into beautiful stagecoaches. The story ends after the film studio invites Barbie back, to direct a new film.

==Characters==
*Barbie - Barbie is a sixteen-year-old teenager star with fantastic style. Barbie is optimistic, cheerful, friendly and a loyal and good friend. She goes to Paris to visit her favorite aunt after she is fired from the movie industry, and it is there she meets and befriends Marie-Alecia.

*Ken (doll)|Ken - Ken is a seventeen-year-old male with an interest in sports and Barbies boyfriend. Ken goes on a journey to Paris to prove that he loves her after finding out she went there and he had been tricked. In the end, the two became a couple once again like before.

*Marie-Alecia - Marie-Alecia "Alice" is a shy, aspiring sixteen-year-old fashion designer with some talent. Her dream is to succeed in the world of fashion, but she is too modest and unsure of herself to make any daring moves. But thanks to Barbie, she gains confidence in her abilities and herself.

*Aunt Millicent - Barbies aunt. Millicent is the owner of her own fashion house in Paris. She is fun, strong-willed and kind. Her formal rival is Jacqueline. She is the sister of Margaret Roberts (mother of Barbie).

*Teresa (Barbie)|Teresa - Barbies sixteen-year-old friend with a unique perspective and point of view. She is very playful and even believes in aliens and the Sasquatch.

*Grace - A sixteen-year-old friend of Barbie who is often mistaken for Nikki by some viewers. She is very practical and mature and likes to give advice.

*Raquelle - Barbies sixteen-year-old rival. She always tries to make Barbies life miserable, once even ripped her dress a lot. She calls Barbie and plays a recording of Kens voice to make Barbie think he broke up with her, inspiring her to journey to Paris and meet her aunt out of misery.

*Jacqueline - Jacqueline is easy to spot with her hip and savvy personality and is on the cutting edge of fashion. As Millicents sneaky and competitive rival designer, she doesnt want to be trendy; she wants to start the trends herself and will do anything to get ahead. But eventually, she is reformed and blends in like Millicent, and apologizes for her behavior.

*Delphine - Jacquelines sixteen-year-old loyal assistant and number-one fan. She usually goes along with her bosss sly schemes, but deep down she is a good person with a good heart, and would rather play fair. She likes the flairies and animals, too.

*  - Shimr, Glimr and Shyne are a trio of stylish Flairies, creatures with sparkle-based powers and no wings. They add sparkles to any outfit that inspires them to improve them.

:Shyne (pink) is the sassy, fearless leader. She adds a beautiful shine effect to fashions. Despite her dainty size, she isnt afraid to stand up for herself. She also makes sure the other flairies stay in line. She has a great sense of humor that makes her a blast to be with.

:Shimr (purple) is the light-hearted, happy-go-lucky dreamer who always expects positive things. Her magical flair is adding shimmer to fashions. She is usually found in the background smiling, but she is the only flairy with no sizable role. 

:Glimr (coral) once had trouble using her sparkle-magic powers, as her magic often popped before it hit the clothing item. However, in the end, her powers turn out to be transforming things, which is the main course of events for the character. She is made into a designer of the flairies.

*Sequin - Barbies French poodle. Just like her owner, Sequin loves to wear anything with bling, despite being fussy. She is also very playful and likes to have fun. When she first arrived in the fashion house, she made a mess in Jilliana and Jacques room.

*Jacques - A suave Jack Russell Terrier with a great talent for design. When he meets Sequin, he falls in love with her. 

*Jilliana - A snooty, pampered cat. She cannot help rolling her eyes at Jacques not-so-inconsiderable crush on Sequin. Although Jilliana is happy that Sequin inspires Jacques to design again after his hiatus, she dislikes all the romance and fluff.

==Cast==

*Diana Kaarina as Barbie  
*Adrian Petriw as Ken
*Tabitha St. Germain as Marie Alecia
*Patricia Drake as Aunt Millicent
*Alexa Devine as Jacqueline
*Shannon Chan-Kent as Delphine
*Maryke Hendrikse as Teresa
*Kandyse McClure as Grace
*Britt Irvin as Racquel
*Chiara Zanni as Shine
*Kelly Metzger as Shimmer
*Andrea Libman as Glimmer
*Brandy Kopp as Sequin
*Charles Tathy as Jacques
*Annick Obonsawin Jilliana
*Nicole Oliver as Liliana Roxelle

==Songs==
*Life is A Fairytale - Tiffany Giardina  
*Another Me - Lindsay Sorenson
*Get Your Sparkle On - Rachel Bearer  
*Une Bonne Journee - Simon Wilcox
*Rock the Runway - Bradly Bacon
*Its A Perfect Day -Adrian Petriw

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 