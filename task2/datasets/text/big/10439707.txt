Severed: Forest of the Dead
{{Infobox film
| name           = Severed: Forest of the Dead
| director       = Carl Bessai
| producer       = {{plainlist|
* Andrew Boutilier
* Cynthia Chapman
}}
| writer         = {{plainlist|
* Carl Bessai
* Travis McDonald
* Julian Clarke
}}
| starring       = {{plainlist| Paul Campbell
* Sarah Lind
* JR Bourne
}}
| music          = Clinton Shorter
| cinematography = James Liston
| editing        = Julian Clarke
| studio         = {{plainlist|
* Chum Television
* Brightlight Pictures
* Submission Films
}}
| distributor    = Voltage Pictures
| released       =   }}
| runtime        = 93 minutes
| country        = Canada
| language       = English
}}
Severed: Forest of the Dead is a 2005 Canadian zombie horror film directed by Carl Bessai and set in a remote logging community following an incident where a genetic experiment goes wrong.

==Plot== spiked tree and suffers a wound from his chainsaw. The sap of the tree infects his wounds and causes him to undergo a transformation.

The president of the company decides to send his son Tyler to investigate the loss of contact with the loggers, hoping Tyler will learn more about the industry. When he arrives, Tyler discovers most of the loggers and protesters have become zombies. He comes across a bunker where Mac, Rita and Carter have taken shelter, along with other survivors including Macs apprentice Luke (Michael Teigen). The company learns of the zombie outbreak when a chemist examining one of Carters samples at their lab is infected by the altered sap. Tylers father wishes to send an evacuation to rescue his son but is overruled by the other members of the board, who decide to set up a quarantine.

The survivors leave the bunker and attempt to drive off the island, but they find the road blocked by the company. They get separated and several more victims fall to the zombies. After reuniting at the logging base, Mac recovers another truck and the group decides to travel to another abandoned facility in the hopes of radioing for help. At the camp, Mac finds the radio dead, while Tyler and Rita begin to become close. Luke is dragged off by the zombies when Carter becomes too frightened to help him. Later, Carter becomes overwhelmed by guilt and confesses that the company had him conducting genetic experiments to grow the trees faster, leading to an increase in profit.

The next day, the remaining survivors attempt to drive to another potential exit but encounter more zombies. Mac loses hope when he sees Luke as a zombie, as he considered Luke a son, but Rita tries to console him. They find the road blocked but hear a chopper approaching. Instead of saving them, however, the chopper opens fire. They are rescued by another renegade band of surviving loggers, who reveal that the company plans to kill them all. The militaristic nature of the camp disturbs the remaining survivors, especially a sadistic game they play in which a selected member of the camp must kill a certain number of zombies in a pen. That night, the head of the camp attempts to rape Rita, but Tyler defends her. They admit their attraction and have sex. The next day, Carter is selected to enter the pen, but he becomes frightened and jams his gun, ending up needing to be rescued. He is bullied mercilessly for this by the loggers, who blame him for the crisis.

His spirit broken, Carter takes an axe and leaves the camp alone that night. Zombies wander in through the open gate and kill everyone except Mac, Rita and Tyler, who wake up and flee. At the gate, Mac is bitten. Rita confesses to him as he turns that she was the one who spiked the tree, and with his last breaths as a human, Mac forgives her. While running through the woods, Tyler and Rita hear Carter, who is surrounded by zombies, screaming for help. Tyler attempts to save him, but they are both overrun by zombies. Rita, now the last survivor, sees the lights of a car and happens upon a road she begins to follow in hopes of escape. In his mansion, Tylers father looks sadly at a picture of himself with Tyler, realizing that his actions led to the loss of his son.

==Cast== Paul Campbell as Tyler
*Sarah Lind as Rita
*Julian Christopher as Mac
*JR Bourne as Carter
*Michael Teigen as Luke
*Leanne Adachi as Stacey Patrick Gallagher as Anderson
*Sage Brocklebank as Mills
*Brad Sihvon as Tom
*Zak Santiago as Ramon
*Alex Zahara as Smith

==Production==
Filming took place in Victoria, British Columbia. 

==Release== Screamfest  and was released on DVD August 1, 2006. 

==Reception==
Robert Koehler of Variety (magazine)|Variety called the film an ingenious genre film "that should greatly please the hardcore crowd and arthouse denizens."   Brian McNail from Brutal As Hell called the film "boring and stereotypical", though he stated that the gore and acting were both good.   Daniel Benson of HorrorTalk.com rated it 3.5/5 stars and called it "a fairly enjoyable zombie romp" that lacks originality.   Lee Roberts of Best-Horror-Movies.com rated it 2/5 stars and wrote that the film followed the rules of the zombie genre well but did not actually have zombies in it.  Lee also criticized the films lack of originality.   Peter Dendle called the film generic and predictable, though he wrote that the fresh setting and uniquely Canadian feel help. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 