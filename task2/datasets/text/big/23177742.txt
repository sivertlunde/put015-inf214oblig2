Dokkan Shehata
 

{{Infobox film
| name           = Dokkan Shehata
| image          = DOKKAN_SHEHATA.jpg
| caption        = Theatrical poster
| director       = Khaled Youssef
| producer       =
| writer         =
| narrator       =
| starring       = Haifa Wehbe Muhammad Hamidah Umaru Saad Ghadah Abd Al-Raziq Umru Abd Al-Jalil Abd Al-Aziz Abd Al-Aziz Makhyoun
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 128 minutes
| country        = Egypt
| language       = Arabic
| budget         =
| gross          =
}}
Dokkan Shehata is an Egyptian drama film produced in 2009.  The main actors in this film are Muhammad Hamidah, Umaru Saad, Ghadah Abd Al-Raziq, Haifa Wehbe, Umru Abd Al-Jalil, Tariq Abd Al-Aziz, and Abd Al-Aziz Makhyoun.  The film is produced by Khaled Youssef.

==Controversy==
Khalid Youssef alleged that there was a dirty plot to limit the films financial success. Despite getting a good revenue from the theaters, many of them pulled out Dokkan Shihata, supposedly to let other films such as “Al-Farah” (The Wedding Party) and “Bobbos”. He alleges theatrical distribution tycoons are responsible for "killing" it. http://www.arabia.msn.com/Entertainment/Movies/LocalNews/2009/june/KhaledYoussef.aspx  

==Reception==
=== Box office ===
In the first three weeks since its release,Dokkan Shehata grossed  L.E. 9 million in revenues.  It was second to “Omar & Salma” starring Tamer Hosny and Mai Ezzeddine, which grossed L.E. 13 million. 

==References==
 

 
 
 
 
 
 

 