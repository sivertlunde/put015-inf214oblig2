Great World of Sound
{{Infobox film
| name           = Great World of Sound
| image          = 
| alt            = 
| caption        = 
| director       = Craig Zobel
| producers      = David Gordon Green Melissa Palmer Richard A. Wright Craig Zobel
| writers        = George Smith Craig Zobel
| story          =  Pat Healy Kene Holliday Rebecca Mader Robert Longstreet Tricia Paoluccio John Baker
| music          = David Wingo
| cinematography = Adam Stone
| editing        = Jane Rizzo Tim Streeto
| production companies = GWS Media Plum Pictures
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Great World of Sound is a 2007 comedy film directed by Craig Zobel. Zobel won Breakthrough Director at the Gotham Awards and the film also won the Grand Jury Award at the Atlanta Film Festival. 

==Plot== song sharking. In the film, real people performed in the audition scenes without knowing it was actually a film shoot. The interactions between the lead actors and the unsuspecting musicians were recorded with hidden cameras. This was integrated into the final product, resulting in a blend of fact and fiction. 

==Cast== Pat Healy as Martin
* Kene Holliday as Clarence
* Rebecca Mader as Pam
* Robert Longstreet as Layton
* Tricia Paoluccio as Gloria
* John Baker as Shank

==Reception==
The film was released to generally positive reviews, with an 80% rating on Rotten Tomatoes.  On Metacritic it received an average score of 72 based on 13 reviews. 

==References==
 

==External links==
*  
*  

 
 
 
 
 

 