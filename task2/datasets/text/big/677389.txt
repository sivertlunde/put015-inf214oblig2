Dr. T & the Women
{{Infobox film
| name           = Dr. T & The Women
| image          = Dr T and the Women poster.jpg
| caption        = Theatrical release poster
| alt            = A man in a white coat, stethoscope on his shoulders, clouds and lightning above him. 
| director       = Robert Altman
| producer       = Robert Altman Cindy Cowan
| writer         = Anne Rapp
| starring       = {{Plain list | 
* Richard Gere
* Helen Hunt
* Farrah Fawcett
* Laura Dern
* Shelley Long
* Kate Hudson
* Liv Tyler
* Tara Reid
}}
| music          = Lyle Lovett
| cinematography = Jan Kiesser
| editing        = Geraldine Peroni
| distributor    = Artisan Entertainment
| released       =  
| runtime        = 122 minutes
| country        = United States Germany
| language       = English German Spanish
| budget         = $23 million
| gross          = $22,844,291
}} gynecologist Dr. an album of his score in September 2000.

==Plot== gynecologist for some of the wealthiest women in Texas who finds his life beginning to fall apart starting when his wife, Kate (Farrah Fawcett), suffers a nervous breakdown and is committed to the state mental hospital. Dr. Ts eldest daughter, Dee Dee (Kate Hudson), is planning to go through with her approaching wedding despite the secret that she is romantically involved with Marilyn (Liv Tyler), the maid of honor. Dr. Ts youngest daughter, Connie (Tara Reid), is a spunky conspiracy theorist who has her own agenda, while Dr. Ts loyal secretary, Carolyn (Shelley Long), has romantic feelings for him, which are not mutual. Dr. Ts sister-in-law, Peggy (Laura Dern), meddles in every situation she stumbles into, while one woman, Bree (Helen Hunt), a golf instructor, offers him comfort.

  

==Cast==
* Richard Gere as Dr. Sullivan Travis (Dr. T)
* Helen Hunt as Bree Davis
* Farrah Fawcett as Kate Travis
* Laura Dern as Peggy
* Shelley Long as Carolyn
* Kate Hudson as Dee Dee Travis
* Liv Tyler as Marilyn
* Tara Reid as Connie Travis
* Robert Hays as Harlan
* Matt Malloy as Bill
* Andy Richter as Eli
* Lee Grant as Dr. Harper
* Janine Turner as Dorothy Chambliss
* Sarah Shahi as Cheerleader (uncredited)

==Release==
  
Dr. T & The Women was released in US cinemas on October 13, 2000, and earned $5,012,867 in its opening weekend on 1,489 screens, ranking #7 in the weekend of October 13, 2000  ultimately grossing $13,113,041 in the United States. It was later released in the United Kingdom on July 6, 2001, and went on to gross $9,731,250 in international profits.

==Reception==
  
The film received mixed reviews from critics. Review aggregation website Rotten Tomatoes gave the film a score of 57% positive reviews, based on 107 reviews.  Metacritic gave the film an average score of 64/100, based on 35 reviews. 

Critic Roger Ebert gave the film three stars, stating "When you hear that Dr. T is a gynecologist played by Richard Gere, you assume he is a love machine mowing down his patients. Nothing could be further from the truth". 

 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 