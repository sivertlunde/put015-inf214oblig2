No Longer 17
{{Infobox film
| name           = No Longer 17
| image          = No Longer 17 Poster.jpg
| caption        = Film poster
| director       = Itzhak Zepel Yeshurun
| producer       = Itzhak Zepel Yeshurun Avi Kleinberger
| writer         = Itzhak Zepel Yeshurun
| starring       = Dalia Shimko Maya Maron Avi Kleinberger
| music          = Jonathan Bar Giora
| cinematography = Amnon Salomon
| editing        = Tova Asher
| distributor    = 
| released       =  
| runtime        = 97 minutes
| rating         =
| country        = Israel
| language       = Hebrew
| budget         = 
}} Israeli drama written and directed by Itzhak Zepel Yeshurun.  It is the sequel to the directors 1982 film Noa at 17 and features actress Dahlia Shimko reprising her role as Noa, the idealistic teen who is now a middle-aged woman.

No Longer 17 premiered at the Haifa Film Festival in October 2003 where it won the Best Film award.

==Plot==
A kibbutz in Israel is heavily in debt. In a desperate last effort to produce a viable financial restructuring, the old, "unproductive" members are asked to leave the community in order to make room for younger, more productive new members.

Noa (Dalia Shimko), 45, who left Israel many years ago and is now living in Amsterdam, is forced to return to the kibbutz to help her mother (Idit Tzur), who was among the first to be ousted. But when she comes back, it becomes only the first in a series of familial reunions that re-trigger old arguments and problems. Noa is also reunited with her daughter, Sarry (Maya Maron), who left for India with her own family secret.

Against the background of a disintegrating society, the film resurrects the protagonists of the film Noa at 17, at the time of a new and perhaps terminal crisis on the kibbutz.

==Cast==
*Dalia Shimko as Noa
*Maya Maron as Sarry
*Avi Kleinberger as Kibbutz Spokesperson
*Yehuda Efroni
*Shmuel Shilo
*Idit Tzur as Bracha

==Critical reception==
Robert Koehler of Variety described the films pacing as "sluggish" and "unreasonably smothered in a storyline stuffed with intersecting crises faced by family members and lovers," depicting the once-iconic kibbutz as "a broken-down shell."  Another film journalist, Sarit Fuchs, noted that the ideals of "solidarity and self-realization" once associated with the kibbutz are shown here as having been "distorted into lies, deceit, and wickedness. On the personal and social level . . . the subject of the film is treachery and betrayal of faith.” 

==References==
 

==External links==
*  
*  

 
 
 
 