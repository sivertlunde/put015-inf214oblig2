The God of Cookery
 
 
{{Infobox film
| name           = The God of Cookery
| image          = GodOfCookery.jpg
| caption        = Hong Kong film poster
| director       = Stephen Chow Li Lik-Chi
| producer       = 
| writer         = Stephen Chow Vincent Kuk Lou Man-Sang Tsang Ken-cheong
| based on       = Iron Chef traditional = 食神
| jyutping = sik6san1
| pinyin = Shíshén }}
| starring       = Stephen Chow Karen Mok Vincent Kuk
| music          = 
| cinematography = Jingle Ma Cheung Ka-Fai
| distributor    = Hong Kong:   CN Entertainment (DVD)
| released       = 1996
| runtime        = 92 minutes
| country        = Hong Kong Cantonese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

{{Infobox Chinese
| c = 食神
| p = Shíshén
| j = sik6san1
| y = Sik Sahn
| l = Iron Chef
}}
 1996 Cinema Hong Kong West for nonsensical style.

An 35-mm print of the film brought by Amanda Cohen of Dirt Candy was shown at a sold-out screening at one of Alamo Drafthouse Cinema South Lamars "Food and Film Events".

==Synopsis== Assorted Noodles for its lack of taste, terrible choice of ingredients and feces in the pigs colon, as she lashes out at him for his criticism towards her cooking. He tells her he is the God Of Cookery.

The God of Cookery is the story of celebrity chef Stephen Chow (the Chinese characters used for Chows name in the movie is different from Chows actual name), who knows very little about cooking and is willing to hawk any product for a price. The arrogant and cocky Chow is known as the "God of Cookery" and runs a successful business empire, as well as appearing as a judge for culinary competitions rigged to make him look good.

When Bull Tong ( s and Mantis shrimp#Cookery|"pissing" shrimp. Chow manages to unite the two rival vendors by combining the two dishes into a new dish, "Pissing Beef Balls", which the three of them could sell together. It becomes a huge success, and the vendors convince Chow to enroll in a culinary school in order to reclaim the title he lost, but not before he discovers that Turkey idolized Chow as the "God of Cookery", and received her scarred appearance due to her devotion.

The success of the "Pissing Beef Balls" alarms Bull, the new "God of Cookery", who arranges for Chow to be assassinated on the way to culinary school. Turkey, however, takes the bullet instead, and Chow, presumed dead, disappears.

One month later, Bull enters the "God of Cookery" competition (a parody of  , where head monk Wet Dream (a spoof on the Chinese word for nocturnal spermatorrhea) nursed him back to health. However, Wet Dream would not allow Chow to leave the temple until he was well-versed in the ways of the Shaolin arts, a point made moot when it is revealed the culinary school he was going to attend was, in fact, the temples kitchen—the same kitchen Bull had trained at for 10 years, but subsequently dropped out of. While training, Chow continually mourned for Turkey, and was overcome with grief and remorse over his careless treatment of her. The depth of his feeling, which caused his hair to grow white, convinced Wet Dream to allow him his departure from the monastery.
 Buddha Jumping barbecue pork divine intervention, for revealing culinary secrets to mankind.
 Temple Street, where Goosehead reveals that Turkey survived the assassination. She caught the bullet meant for Chow with her gold-plated teeth and a dentist reconstructed her dental work and even threw in a free plastic surgery on her face, making her pretty again.

==Cast==
* Stephen Chow as Stephen Chow (史提芬周 Sitaifan Chow)
* Karen Mok as Turkey (火雞, Fo Gai) / Guanyin
* Vincent Kok as Bull Tong (唐牛, Tong Ngau)
* Ng Man-tat as Uncle
* Lee Siu-Kei as Goosehead (鵝頭, Ngo Tau)
* Tats Lau as Wet Dream (夢遺, Mung Wai)
* Christy Chung as the girl in the dream sequence (cameo)
* Nancy Sit as herself (cameo)
* Lee Kin-yan as the nose-picking transvestite (cameo)
* Law Kar-ying as competition host
* Stephen Au
* Lam Suet
* Tin Kai-Man
* Kingdom Yuen

== In-film references==
* The scene in which Stephen Chow declares, "I dont mean to show other Im capable. I just want to tell others that I can get back what I have lost." is a homage to Chow Yun-fat and A Better Tomorrow.
* Another reference to A Better Tomorrow immediately follows when Stephen Chow lights his cigar using a Hong Kong bank note.
* The scenes in the Buddhist monastery reference 18 Bronzemen, as the monks call themselves the "18 Bronzemen".
* "Sorrowful Rice" ( ) is a reference to Yang Guos Melancholic Palms (黯然銷魂掌) technique. The final battle between Chow and Bull Tong itself contains a couple of tongue-in-cheek references to Jin Yong (Louis Cha)s The Heaven Sword and Dragon Saber and The Legend of the Condor Heroes in the original Cantonese dialogue, which is however obscured in the English subtitles.

==Box office==
In its Hong Kong theatrical run, the film grossed HK$15,887,000. 

==References==
 

==External links==
*  
*  

 
 

 
   
 
   
 
 
 
   
 
 