A Sting in the Tale
 
 
{{Infobox film
| name           = A Sting in the Tale
| image          = 
| image size     =16mm
| caption        = 
| director       = Eugene Schlusser
| producer       =Rosa Colosimo Reg McLean
| writer         = Patrick Edgeworth
| based on = story by Rosa Colosimo Patrick Edgworth Reg McLdean Eugene Schlusser
| narrator       = Gary Day
| music          = Alan Zavod
| cinematography =Nicholas Sherman 
| editing        =  Zbygniew Friedrich
| studio = Rosa Colosimo Films
| distributor    = 
| released       = November 1989
| runtime        = 92 mins 
| country        = Australia
| language       = English language
| budget         = 
| gross = 
| preceded by    =
| followed by    =
}}
A Sting in the Tale is a 1989 Australian political satire film. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p225 
==Plot==
Diane Lane is elected to Canberra as an MP. Her married lover, Barry Robbins, is Minister for Health and her best friend is journalist Louise Parker.

Barry wants to depose the current Prime Minister and tries to enlist the support of media baron Roger Monroe. However Diane is angry at Monroe for suppressing news of negligence in a mining disaster which killed her father and twenty others.

Diane leaks a document to embarrass the government about its plans for media ownership. The Prime Minister appoints her as Minister to the Arts in order to control her. 

Diane then discovers Monroe has planted someone in her office to watch her. She tries to bring down Monroe and become Australias first female Prime Minister.
==Production==
The movie was shot in Adelaide 

==References==
 

==External links==
*  at IMDB
*  at Australian Cinema
*  at Winding Road Entertainment (official page)
 
 
 
 
 


 