Kayamkulam Kochunni (film)
{{Infobox film
| name           = Kayamkulam Kochunni
| image          =
| caption        =
| director       = PA Thomas
| producer       = M Kunchacko
| writer         = Vaikkom Chandrasekharan Nair (dialogues)
| screenplay     = Sathyan Adoor Bhasi Manavalan Joseph
| music          = BA Chidambaranath
| cinematography = PB Maniyam
| editing        =
| studio         = Excel Productions
| distributor    = Excel Productions
| released       =  
| country        = India Malayalam
}}
 1966 Cinema Indian Malayalam Malayalam film,  directed by PA Thomas and produced by M Kunchacko. The film stars K. J. Yesudas, Sathyan (actor)|Sathyan, Adoor Bhasi and Manavalan Joseph in lead roles. The film had musical score by BA Chidambaranath.   

==Cast==
*K. J. Yesudas as Khader/Suruma seller Sathyan as Kochunni
*Adoor Bhasi as Ochira Pachu Pilla
*Manavalan Joseph as Thommichan
*T. R. Omana as Achuthan Nairs wife
*K. P. Ummer as Diwan
*Kaduvakulam Antony as Bava
*Kamaladevi as Ayisha
*Thikkurissi Sukumaran Nair as Kunjunni Panikkar
*Ushakumari as Nabeesa
*Muthukulam Raghavan Pilla as Achuthan Nair
*Sukumari as Vazhapilli Janaki Pilla
*Paul Vengola as Kuttan Moosu Thirumeni
*Prathapachandran as Kallada Kochu Naanu

==Soundtrack==
The music was composed by BA Chidambaranath and lyrics was written by P. Bhaskaran and Abhayadev. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aattuvanchi || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Kaarthikavilakku Kandu || B Vasantha || P. Bhaskaran || 
|-
| 3 || Kunkumappoovukal Poothu || K. J. Yesudas, S Janaki || P. Bhaskaran || 
|-
| 4 || Padachavan Padachappol || Kamukara || P. Bhaskaran || 
|-
| 5 || Padachonte Kiripakondu || B Vasantha || Abhayadev || 
|-
| 6 || Suruma Nalla Suruma || K. J. Yesudas || P. Bhaskaran || 
|-
| 7 || Viravaalan Kuruvi || S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 