Rickshaw Boy (film)
{{Infobox film
| name           = Rickshaw Boy
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 骆驼祥子
| director       = Ling Zifeng
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Siqin Gaowa Zhang Fengyi
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1982 (China), October 1983 (Chicago International Film Festival)
| runtime        = 123 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = 
}} novel of the same name by Lao She. The film stars Siqin Gaowa, who won a Golden Rooster for her performance.

==Plot==
Xiang Zi is a rickshaw boy who has always had a desire to excel and a thirst for freedom. He married Hu Niu who died of dystocia later. After her death, another girl, Xiao Fuzi, falls in love with Xiang Zi but they are separated by poverty. Xiang Zi works very hard in order to change his life, only to find that Xiao Fuzi is dead just as he begins to be hopeful for their future. Finally, Xiang Zi, an unflinching man, surrenders to that dark society.

==Cast==
*Siqin Gaowa as Hu Niu
*Yan Bide as Liu Siye
*Zhang Fengyi as Xiang Zi

==Reception==
===Awards===
*Golden Rooster Award for Best Picture at the 3rd Golden Rooster Awards (1982, won)
*Golden Rooster Award for Best Actress at the 3rd Golden Rooster Awards (1982, won - tied with Pan Hong)
*Golden Rooster Award for Best Art Director at the 3rd Golden Rooster Awards (1982, won)
*Golden Rooster Award for Best Sound at the 3rd Golden Rooster Awards (1982, won)
*Outstanding Film at the Huabiao Film Awards (1982, won)
*Best Film at the Hundred Flowers Awards (1983, won - three way tie with At Middle Age and The Herdsman)
*Best Actress at the Hundred Flowers Awards (1983, won)

== References ==
 
* 
* 

== External links ==
* 
* 

 
 
 
 

 