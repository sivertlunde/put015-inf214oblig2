Dharti Mata (film)
{{Infobox film
| name           = Dharti Mata
| image          = 
| image_size     = 
| caption        = 
| director       = Nitin Bose
| producer       = New Theatres
| writer         = Nitin Bose
| narrator       = 
| starring       = K. L. Saigal Uma Shashi Jagdish Sethi Kamlesh Kumari
| music          = Pankaj Mullick
| cinematography = Nitin Bose
| editing        = 
| distributor    =
| studio         = New Theatres
| released       = 1938
| runtime        = 165 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1938 Hindi Bengali as Desher Mati in the same year by New Theatres.    It starred K.L. Saigal, Uma Shashi, Jagdish Sethi, Kamlesh Kumari, and K. C. Dey.    The music was by Pankaj Mullick and lyricist and dialogue writer was Pandit Sudarshan.    The story, screenplay and cinematography was by Nitin Bose. The story is about two friends Ashok and Ajay, one interested in agriculture and the other in technology. Ashok goes to the village to help the farmers while Ajay goes to UK for higher studies in engineering. The film highlights the need of technology and new concepts for effective farming.  

==Plot==
Ashok (K. L. Saigal) and Ajay (Jagdish Sethi) have different notions about the progress of the country. Ashok supports agriculture and going back to the villages while Ajay is in favour of industrialisation and technology. Ajay is from a rich family and has to go to UK for further studies in engineering. He asks Ashok to come with him. Ashok refuses and decides to go the village and help the farmers. Ajay’s sister Pratibha (Kamlesh Kumari) is in love with Ashok who is unaware of her feelings. When Ashok faces hardships in the village she secretly assists him by sending money to buy new machinery for the farm. Ashok and his colleague are frustrated in their attempts several times by Chowdhary, the village headman. Ashok meets and falls in love with a village girl Gauri (Uma Shashi) who stays with her blind father (K. C. Dey). Ajay returns from abroad and is told of a rich coal mine. This is located under Ashok’s field. Ajay forgoes mining the land and decides to buy the land that Ashok has been farming to safeguard it for him. Ashok tells Ajay his plans of marrying Gauri. Ajay is stunned and upset for his sister. For the farmers there is distress when the monsoons fail. They face a drought situation. This is when Ajay decides to mine the field for coal. However the disagreements are sorted out and Ajay helps out with new ideas and approves Ashok’s method of  co-operative farming.

==Cast==
*K. L. Saigal
*Uma Sashi
*Kamalesh Kumari
*Jagdish Sethi
*K.C. Dey
*Shyam Laha
*Bikram Kapoor
*Nemo
*Nawab

==Production==
New Theatres ushered in a renaissance period with films based on social issues. These were most commonly addressed in Nitin Bose’s films.In Dushman (1939 film)|Dushman (1939) he dealt with tuberculosis and in Dharti Mata he used the idea of collective farming and need of machinery and tube wells in agriculture.    This was one of the first films to deal with innovative methods of farming and the need for modernisation.   

==Soundtrack==
Pankaj Mulick used western elements in his music without losing the melodious Indian appeal in "Duniya Rang Rangili Baba".    The songs were sung by K. L. Saigal, Uma Shashi and K. C. Dey. The lyrics were by Pandit Sudarshan.   

==Songs==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Mann Kee Bat Bataun"
| K. L. Saigal, Uma Shashi
|-
| 2
| "Kisne Yah Sab Khel Rachaya"
| K. L. Saigal
|-
| 3
| "Duniya Rang Rangilee Baba"
|  K. C. Dey, Uma Shashi, K. L. Saigal
|-
| 4
| "Ab Main Kah Karu"
| K. L. Saigal
|-
| 5
| "Jaag Sajaniya Jaag Navyug Aaya"
| Uma Shashi
|-
|} 

==References==
 

==External Links==
* 

 
 
 