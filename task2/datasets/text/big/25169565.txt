Mommy (1995 film)
{{Infobox Film
| name           = Mommy
| image          =
| caption        = 
| director       = Max Allan Collins
| producer       = James K. Hoffman
| writer         = Max Allan Collins Jason Miller Brinke Stevens Michael Cornelison Mark Spellman
| music          = Richard Lowry
| cinematography = Phillip W. Dingeldein
| editing        = Phillip W. Dingeldein
| distributor    = M.A.C. Productions
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
}}
 thriller starring The Bad Seed.

==Plot==

Mrs. Sterling (Patty McCormack) is psychotically obsessed with her 12-year-old daughter, Jessica Ann (Rachel Lemieux), so much so that when she finds out Jessica didnt get the "Student of the Year" award again, she solves the problem by murdering the teacher who didnt recommend her for it. 

She dismisses the killing as inconsequential ("a minor accident"), but the homicide detective assigned to the case suspects her immediately, and an insurance investigator who also suspects her tries to get close to Jessica Ann to find out what really happened.

==Cast==

*Patty McCormack ... Mommy/Mrs. Sterling
*Sarah Jane Miller ... Jolene Jones
*Rachel Lemieux ... Jessica Ann Jason Miller ... Lt. March
*Brinke Stevens ... Beth
*Michael Cornelison ... Mark Jeffries
*Mark Spellman ... Detective
*Majel Barrett ... Mrs. Jeffries
*Mickey Spillane ...  Attorney Neal Ekhardt 
*Marian Wald ... the Principal 
*Janelle Vanerstrom ...  Substitute teacher 
*Judith Meyers ...  Hallway teacher 
*Nathan Collins ...  Gleeful kid 
*Tom Castillo ...  Ambulance attendant #1 
*Tom Summit ...  Ambulance attendant #2

==External links==
* 

 
 
 
 
 
 


 