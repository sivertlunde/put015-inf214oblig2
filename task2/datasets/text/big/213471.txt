Chungking Express
 
{{Infobox film
| film name =  
| image = Chungking_Express.jpg
| alt = 
| caption = Theatrical release poster
| director = Wong Kar-wai
| producer = Chan Yi-kan
| writer = Wong Kar-wai
| starring = Brigitte Lin Tony Leung Chiu-Wai Faye Wong Takeshi Kaneshiro Valerie Chow
| music = Frankie Chan Roel. A Garcia Lau Wai-Keung (Andrew Lau)
| editing = William Chang Kai Kit-wai Kwong Chi-Leung
| studio = Jet Tone Production
| distributor = Ocean Shores Video   Miramax Films Rolling Thunder Pictures  
| released =   }}
| runtime = 98 minutes
| country = Hong Kong Mandarin English Japanese Hindi
| budget = 
| gross = $600,200 (US)  HK$7,678,549 (Hong Kong|HK) 
}} Hong Kong Hong Kong Tony Leung as a police officer who is roused from his gloom over the loss of his flight attendant girlfriend (Valerie Chow) by the attentions of a quirky snack bar worker (Faye Wong). The film depicts a paradox in that even though the characters live in densely packed Hong Kong, they are mostly lonely and live in their own inner worlds.

The Chinese title translates to "Chungking Jungle", referring to the metaphoric concrete jungle of the city, as well as to Chungking Mansions in Tsim Sha Tsui, where much of the first part of the movie is set. The English title refers to Chungking Mansions and the Midnight Express food stall where Faye works.

==Plot==
The film comprises two different stories, told one after the other, each about a romance involving a policeman. Except for a brief moment when the first story ends and the second begins, the two stories do not interconnect. However, the three main characters from the second story each momentarily appear during the first.

===First story===
The first story concerns Taiwan-born cop He Qiwu, also known as Cop 223. Qiwus girlfriend May broke up with him on 1 April (April Fools Day). His birthday is 1 May and he chooses to wait for May for a month before moving on. Every day he buys a tin of pineapple with an expiration date of 1 May. By the end of this time, he feels that he will either be rejoined with his love or that it will have expired forever. Meanwhile, a mysterious woman in a blonde wig (played by Brigitte Lin) tries to survive in the drug underworld after a smuggling operation goes sour.

On 1 May, Qiwu, looking for romance, approaches the woman in the blonde wig at a bar (the Bottoms Up Club). However, she is exhausted and falls asleep in a hotel room, leaving him to watch old movies alone all night and order take-out food. He shines her shoes before he leaves her sleeping on the bed. She leaves in the morning and shoots the drug baron (played by Thom Baker) who had set her up. Qiwu goes jogging and receives a message from her on his pager wishing him a happy birthday. He then visits his usual snack food store where he collides with a new staff member, Faye. At this point, a new story begins.

===Second story===
In the second story, the unnamed Cop 663 is similarly dealing with a breakup, this time with a flight attendant. He meets Faye, the new girl at the snack bar. She secretly falls for him. The flight attendant waits for the cop around the snack bar, and finds out he is on his day off. She leaves a letter for the snack bar owner to give to the cop. Everyone in the snack bar reads the letter, which is assumed to be the flight attendants way of telling the cop that their relationship is over. The envelope also has a spare set of keys to the cops apartment.

Faye uses the keys to frequently break into his apartment by day to redecorate and "improve" his living situation. She finally tells him of the letter but he keeps delaying taking it or even reading it. Gradually, her ploys help him to cheer up, and he eventually realizes that Faye likes him and arranges a date at the restaurant "California" (an actual place in Lan Kwai Fong at the time).  Faye, however, never shows up for the date, and the snack bars owner, who is her cousin, goes to the restaurant to tell the cop that Faye left for California. Standing the cop up after a last-minute decision to see the world before settling down, she leaves him a boarding pass drawn on a paper napkin postdated one year later.

In the last scene, Faye, now a flight attendant, returns to Hong Kong. She finds that the cop has bought the snack bar and is converting it into a restaurant. He asks her to stay for the grand opening in a couple of days, but she says she does not know if she can. 663 then asks if she can send him a postcard if she leaves, but she says he wouldnt read it anyway. As Fayes about to leave, he gets the boarding pass she gave him a year ago, wrinkled and water-stained, and asks if anyone will accept it. She doubts it and writes him up a new one. Faye asks him where he wants to go with the boarding pass, and 663 says hell go wherever she wants to take him. Their future remains ambiguous.

==Cast==
* Brigitte Lin – Woman in blonde wig
* Tony Leung Chiu-Wai – Cop 663
* Faye Wong – Faye
* Takeshi Kaneshiro – He Qiwu, nicknamed Ah Wu, Cop 223
* Valerie Chow – Flight attendant who breaks up with Cop 663
* Thom Baker – Double-crossing drug dealer
* Chan Kam-Chuen – Manager of the takeaway restaurant Midnight Express
* Kwan Lee-na – Richard
* Wong Chi-Ming – Man
* Leung Sun – The 2nd May, who works at the Midnight Express
* Choh Chung-Sing – Man

==Production==
 ] and the other in Kowloon; the action of the first would happen in daylight, the other at night. And despite the difference, they are the same stories." 

On the screenplay, Wong has said
: "When I started to film, I didnt have it written completely. I filmed in chronological order. The first part happened during the night. I wrote the sequel of the story in one day! Thanks to a brief interruption for the New Year festivities, I had some more time to finish the rest of the script." 
 Fallen Angels (1995). 

Wong had specific locations in mind where he wanted to set the action of the film. In an interview, he has said:
"One: Tsim Sha Tsui. I grew up in that area and I have a lot of feelings about it. Its an area where the Chinese literally brush shoulders with westerners, and is uniquely Hong Kong. Inside Chungking Mansion you can run into people of all races and nationalities: Chinese, white people, black people, Indian." 
This is the setting for much of the first story. As Wong explains, Chungking Mansion is famous for

: "its 200 lodgings, it is a mix of different cultures...it is a legendary place where the relations between the people are very complicated. It has always fascinated and intrigued me. It is also a permanent hotspot for the cops in HK because of the illegal traffic that takes place there. That mass-populated and hyperactive place is a great metaphor for the town herself." 
 escalator from Central to the mid-levels. That interests me because no one has made a movie there. When we were scouting for locations we found the light there entirely appropriate."  The apartment of Tony Leungs character was cinematographers Christopher Doyles apartment at the time of filming. 

===Filming locations===
  Nathan Road facade of the Chungking Mansions. Corridor lined with shops inside Chungking Mansions Departure terminal of the former Kai Tak Airport.
File:Central-Mid-Levels escalators at night IMG 5228.JPG|Central–Mid-levels escalators.
 

==Marketing==
The film is marketed with the tagline "If my memory of her has an expiration date, let it be 10,000 years...". Historically, the number "10,000" was used to represent the concept of "forever" in China and many other Asian countries (see Ten thousand years).

==Soundtrack==
{{Infobox album  
| Name = Chungking Express
| Type = film
| Artist = Frankie Chan  & Roel A. Garcia
| Cover = 
| Released = 8 March 1996
| Recorded =
| Genre = Film score
| Length =
| Label = Cinepoly
| Producer =
| Reviews =
}}
The song "Baroque", composed by  " by the Jamaican reggae singer Dennis Brown, which plays in the American bar where the drug baron is located.
 Random Thoughts. Mandarin cover.) "California Dreamin" by The Mamas & the Papas, is played numerous times as it is the favourite song of Faye Wongs character. "What a Diffrence a Day Made", performed by Dinah Washington, is played during a scene between Tony Leung and Valerie Chows characters.

Tracklist:
# Dreamlover (vocal by Faye Wong)
# Chasing The Metaphysical Express
# Seusuous Forest
# Fornication in Space
# Rain ~ Tear and Sweat
# Night Snack
# Entering the Hardboiled Wonderland
# Rock Like a Dog
# Heartbreak Interlude
# Sweat Farewell
# Random Thoughts (vocal by Faye Wong)

==Distribution== Rolling Thunder distribution company under Miramax. The Region 1 DVD is distributed by Rolling Thunder. Tarantino is an admirer of Wong Kar-wai, and the DVD features lengthy bookended remarks by him.

Chungking Express was later released by The Criterion Collection on DVD and Blu-ray Disc, although both versions are now out of print. Chungking Express was re-released on DVD and Blu-ray in the UK on 26 January 2009.

==Reception==

===Box office===
Chungking Express earned HK $7,678,549 during its Hong Kong run.  In the USA, opening on four screens, it grossed $32,779 ($8,194 per screen) in its opening weekend. Playing at 20 theatres at its widest point, it went on to gross $600,200 total.

===Critical reviews===
During its release in North America, Chungking Express drew generally positive, sometimes ecstatic reviews from critics. On the website Rotten Tomatoes, which collects film reviews, it currently holds a 96% approval rating, with only one negative review out of 28. 

Influential film critic Roger Ebert gave the film a positive review, but was measured in his praise:  "If you are attentive to the style, if you think about what Wong is doing, Chungking Express works. If youre trying to follow the plot, you may feel frustrated&nbsp;... When Godard was hot, in the 1960s and early 1970s, there was an audience for this style, but in those days, there were still film societies and repertory theaters to build and nourish such audiences. Many of todays younger filmgoers, fed only by the narrow selections at video stores, are not as curious or knowledgeable and may simply be puzzled by Chungking Express instead of challenged. It needs to be said, in any event, that a film like this is largely a cerebral experience: You enjoy it because of what you know about film, not because of what it knows about life." 

Prolific web reviewer  , Tsui Hark, and other directors who learned their craft in Hong Kong, Wong infuses his films with style and energy. His hand-held camera is restless, always moving and shifting. The action sequences are punctuated with unusual shots and stop-motion jumps. By filming Chungking Express in such rich, vibrant manner, the director uses visual images to underscore his themes. Once the viewer gets past bouts of confusion (the film demands more than one viewing), the result is a uniquely memorable look at the ties that bind all people, as presented through two deceptively simple stories." 

In a 2002 poll published by Sight and Sound (the monthly magazine of the British Film Institute) asking fifty leading UK film critics to choose the ten best films from the previous 25 years, Chungking Express was placed at number eight.  In the magazines 2012 poll to find the most acclaimed films of all time, Chungking Express ranked 144. 

===Awards and nominations===
* 1994 Golden Horse Awards
** Winner – Best Actor (Tony Leung Chiu-Wai)
* 1995 Hong Kong Film Awards
** Winner – Best Picture
** Winner – Best Director (Wong Kar-wai)
** Winner – Best Actor (Tony Leung Chiu-Wai)
** Winner – Best Editing (William Cheung Suk-Ping, Kwong Chi-Leung, Hai Kit-Wai)
** Nomination – Best Actress (Faye Wong)
** Nomination – Best Supporting Actress (Valerie Chow Kar-Ling)
** Nomination – Best Screenplay (Wong Kar-wai)
** Nomination – Best Cinematography (Christopher Doyle, Andrew Lau Wai-Keung)
** Nomination – Best Art Direction (William Cheung Suk-Ping)
** Nomination – Best Original Film Score (Frankie Chan Fan-Kei, Roel A. Garcia)

==See also==
* Cinema of Hong Kong
* Hong Kong in films

==References==
 

==External links==
 
*  
*  
*   at Rotten Tomatoes
*  
*   – essay by critic Amy Taubin at The Criterion Collection
*  , Tsung-Yi Huang
*  

 
 

 
 
   
 
   
 
 
 
 
 
 
 
 
 
 
 