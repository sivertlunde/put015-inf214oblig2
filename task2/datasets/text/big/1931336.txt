The Last Time I Committed Suicide
 
{{Infobox film
| name = The Last Time I Committed Suicide
| image = LastTimeICommittedSuicide.jpg
| image_size =
| caption = Stephen T. Kay
| producer = Edward Bates Louise Rosner
| writer = Stephen T. Kay
| narrator =
| starring = Thomas Jane Keanu Reeves Adrien Brody John Doe Claire Forlani
| music = Tyler Bates
| cinematography = Bobby Bukowski
| editing = Dorian Harris
| distributor = Multicom Entertainment Group Inc.
| released =  
| runtime = 92 minutes
| country = United States
| language = English
| budget = $4,000,000 (estimated)
| gross =  $46,362 
}}
 Stephen T. Kay. Based on a letter written by Neal Cassady to Jack Kerouac, it stars Thomas Jane as Cassady. The cast also includes Keanu Reeves, Adrien Brody, Gretchen Mol and Claire Forlani.

The film takes place in 1946, and is loosely based on a letter from Cassady to Jack Kerouac.  While the letter was written in 1950, the action of the letter took place when he was 20. 

==Plot==
Neal Cassady (Thomas Jane) , a beatnik who is partly responsible for bringing together many of the famous beat generation figures. Neal is a unique soul, poetic and a dreamer. But he is caught between the excitement of the life of a drifter and the chance of a traditional home. Told from Neals perspective, in a form of a letter, it follows his life before and after the suicide attempt by his longtime love, Joan (Claire Forlani) and how it altered him. Demonstrating Neals active mind and ever changing thoughts, the film jumps back and forth between before and after the attempt. During the credits, as Neal starts to write his letter, the viewer experiences his odd attention span and his schizophrenic like ability to talk to himself.

The story begins the day of Joans suicide attempt, with Neal sitting in the hall outside Joan’s hospital room, his normally happy, excited personality replaced by sadness. He enters her room and onto her hospital bed, being close and making her feel better. It is an intimate but awkward moment. It then jumps to the day before the suicide attempt, where a rain soaked Neal whisks Joan away from her job. They have an intimate night together. After, she sits on the bed, sad, but Neal keeps professing his love to her. It again returns to the hospital room, silence between them. Neal is told he has to leave.

The story moves ahead, with Ben (Adrien Brody) visiting Neal, waking him to tell him about the fabulous story of how he got a loaf of bread for saving a lady’s cat. He then asks Neal if he has been back to the hospital to which Neal replies no. It cuts ahead to Neal wide awake, drinking coffee and eating bread with Ben. In a manic state, Neal, speaking a mile a minute, tells Ben about the story he has come up with, to write about.

Next it moves through Neal doing various things: sitting, talking to himself, writing and working out. They return to the pool hall where Harry suggests the two of them take the girls out on a road trip in a stolen car. Neal tells of how over his life, he must have stolen over 500 cars. They all drive out into the country, flying down the roads. Neal and one girl go out into the woods together and Harry and the other stay in the car. Back at the tire plant, Jerry finds Neal obviously high. He saves Neal from getting in trouble.

On his way he runs into a drunk Harry. Harry convinces Neal to come in for a beer. Neal ends up drunk and Harry convinces him to talk to Mary to get her to come out.  About to leave, Neal is arrested when Mary’s mother calls police. He is allowed to make a phone call, but he doesnt know the phone number for Lewis and Lizzys house. With Mary refusing to testify, the charges are dropped but the police hold him on the false premise of suspicion of burglary. Finally, after more time in jail, Neal is released. He has trouble remembering where Lewis and Lizzys house is, and once he finally finds it, he finds the house dark and empty. He waits but eventually it is obvious they aren’t coming back. He lost his chance at a traditional family life with Joan. He walks back down the porch, hops into a car and drives off with it.

Finishing his letter, Neal places it in an envelope. Walking away, he throws the pages of his novel into the air, paper flying and landing everywhere.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 