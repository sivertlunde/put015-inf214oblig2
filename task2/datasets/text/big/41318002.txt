Against the Current (film)
{{Infobox film
| name           = Against the Current
| image          = Against the Current Official Movie Poster.jpg
| caption        = Official Movie Poster
| director       = Peter Callahan
| producer       = Mary Jane Skalski   Joshua Zeman
| writer         = Peter Callahan
| narrator       =
| starring       = Joseph Fiennes   Pell James   Justin Kirk   Elizabeth Reaser   Martin Shakar   Mary Tyler Moore   Michelle Trachtenberg
| music          = Anton Sanko 
| cinematography = Sean Kirby
| editing        = Michael Taylor
| distributor    = Ambush Entertainment
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $3,000,000
| gross          = 
}}
Against the Current is a 2009 film released by Ambush Entertainment starring Joseph Fiennes, Pell James, Justin Kirk, Elizabeth Reaser, Martin Shakar, Mary Tyler Moore, and Michelle Trachtenberg.

==Plot==
The film follows the story of Paul Thompson (Joseph Fiennes) who is a writer that lost his wife and child a few years back. He and his best friend, Jeff (Justin Kirk), have talked about swimming the length of the Hudson River since they were just kids. One day, Paul asks Jeff to come along with him on his trip to finally swim the whole river. Jeff agrees and brings along one of his friends, Liz (Elizabeth Reaser). Paul is determined to reach the end of the river by a certain date, August 28.  Jeff and Liz have no idea the importance of the date Paul has chosen until they have already started. They realize that the date they are supposed to finish the swim is the anniversary of his familys death. Paul tells them that on that date, he is planning on killing himself. Pauls confession causes Jeff to remember a moment about five years ago when he saved Paul from committing suicide. Jeff told him that he should wait five years and if he still wanted to end his life, he would support him in it. Now, five years later, Jeff and Liz have to decide whether they should let Paul continue with his plan or try to convince him to keep living.

==Cast==
* Joseph Fiennes as Paul Thompson
* Justin Kirk as Jeff Kane
* Elizabeth Reaser as Liz Clark
* Michelle Trachtenberg as Suzanne
* Pell James as Amy Thompson
* Constance Barron as Aunt Karen
* Amy Hargreaves as Sarah Kane
* Mary Tyler Moore as Liz Mom
* Samantha Sherman as Katie
* Martin Shakar as Boatyard Owner
* Avery Glymph as Fisherman #1
* Chad Brigockas as Fisherman #2
* Tom ORourke as Dock Man #1
* John Rue as Dock Man #2
* Douglas Odell as Dock Man #3
* Seth Kanor as Officer Lefurgy
* Nimo Gandhi as Pasthule
* Maggie Brown as Little Girl on Waterfront 
* Perry Callahan as Older Girl on Waterfront

==References==
 

==External links==
*  
*  

 
 
 