Target for Tonight
 
 
 
{{Infobox film
 | name = Target for Tonight
 | image = targetfortonightposter.jpg
 | caption = Original theatrical poster Harry Watt
 | writer =
 | starring = Royal Air Force personnel
 | producer = Harry Watt
 | music = Royal Air Force Central Band British Ministry of Information Warner Bros.
 | released =  
 | runtime = 48 minutes
 | country = United Kingdom
 | language = English
| budget = £7,000   
| gross = £100,000 
 }} Harry Watt. Wellington aircraft. 1942 and Best Documentary by the National Board of Review in 1941 in film|1941.

==Plot== observation aircraft Bomber Command German forces in the subject area for the past few months. The film shows the planning of the mission, even detailing how the bomber wing chooses munitions for the task. The weather forecast is expected to be good and the pilots are briefed. The crew of "F for Freddie", the bomber that is the focal point of the film, suit up and take off. While over Germany, the crew bombs the target, dead on for one bomb but their aircraft is hit by flak from "faceless" Luftwaffe|anti-aircraft gunners.  The radio operator is hit in the leg and Freddie is the last aircraft to return. Mist covers the water, prompting worry at the Command. Meanwhile, Freddie cannot climb after the flak hit. They are not losing altitude but are in a bad situation. Tension builds in the film until finally, F for Freddie lands. No aircraft are lost and the mission is a complete success.

==Production== Sir Richard Sir Robert Percy Pickard, Operation Biting Operation Jericho, John Cobb, then a serving RAF officer. 

Although the film was about a bomber squadron flying Wellingtons the aircraft shown on the movie poster are actually Boulton Paul Defiant fighters.

==Popular culture==
  damages the plane and injures one of their crewmembers in the leg (in the novel, the rear gunner rather than the radio operator). They have trouble holding altitude but make it back after a long, tense flight over hostile territory.
 pathfinder force many British bombers failed to find their targets.

Possible identity of F for Freddie, is Wellington Mk 1c OJ-F (P2517) which was serving with No. 149 Squadron from November 1940 to September 1941. See http://www.raf.mod.uk/history/bombercommandno149squadron.cfm and for photograph of OJ-F http://theairtacticalassaultgroup.com/forum/showthread.php?t=7287

==References==
===Notes===
 
===Bibliography===
 
* Johnston, John and Nick Carter. Strong by Night: History and Memories of No. 149 (East India) Squadron Royal Air Force, 1918/19 - 1937/56. Tunbridge Wells, Kent, UK: Air-Britain (Historians) Ltd., 2002. ISBN 0-85130-313-7.
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 