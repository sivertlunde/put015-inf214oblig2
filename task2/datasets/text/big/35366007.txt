Maryjane (film)
{{Infobox film
| name           = Maryjane
| image          = 
| image_size     =
| alt            =
| caption        = 
| director       = Maury Dexter
| producer       = Maury Dexter
| writer         = Dick Gautier Peter I. Marshall
| based on  = story by Maury Dexter
| narrator       = Fabian Diane McBain
| music          = Mike Curb Lawrence Brown
| cinematography = Richard Moore
| editing        = Sidney Levin
| studio         = American International Pictures
| distributor    = American International Pictures
| released       = 24 Jan 1968
| runtime        = 
| country        =  
| language       = English
| budget         =
| gross          = $1,000,000 (US/ Canada) 
| preceded_by    =
| followed_by    =
}} Fabian as a high school art teacher who is framed for drug possession. 

==Plot==
A car driven by a driver intoxicated by marijuana plunges off a cliff, killing the driver and injuring a female passenger.

It turns out marijuana use is rife at a small town high school, led by the clique of Jordan Bates. Art school Phil Blake tries to persuade student Jerry Blackburn not to smoke. Jerry borrows Phils car and Jordan leaves some marijuana in it. Phil gets arrested for possession of marijuana.

==Cast== Fabian as Phil Blake
*Diane McBain as Ellie Holden
*Michael Margotta as Jerry Blackburn Kevin Coughlin as Jordan Bates
*Patty McCormack as Susan Hoffman

==Production==
According to Maury Dexter "there was nothing salacious or offensive about" the film "but it did have some provocative scenes that showed the results of overindulging and the risks taken when someone needs a fix." 

The movie was shot entirely in the Hollywood area. The Dohney Mansion in Beverly Hills was used for some scenes. "The stark beauty of the estate set against the ramblings of a young user was, I thought, quite effective," wrote Dexter later. 

==Reception==
Dexter says "the film did very well at the box office, although, it was far from a big hit."   accessed 5 July 2014 

==References==
 

==External links==
* 
*  at TCMDB

 
 
 
 
 


 