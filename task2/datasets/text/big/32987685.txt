Won on the Post
 
{{Infobox film
  | name     = Won on the Post
  | image    = 
  | caption  =  Alfred Rolfe		
  | producer = 
  | writer   = 
  | based on = story by Nat Gould  
  | starring = 
  | music    = 
  | cinematography = 
  | editing  = 
  | studio = Australian Photo-Play Company
  | distributor = Gaumont 
  | released = 1 June 1912
  | runtime  = 2,600 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 Alfred Rolfe set against a backdrop of horseracing.

It is considered a lost film. 

==Plot==
Two brothers love the same girl, but she loves the younger brother. He falls in with some gamblers and to pay them back arranges to nobble his fathers race horse. The younger brother falls in love with a bar maid, who overhears a plot yo rob him – she is caught but escapes and warns her love. The younger brother fights the robbers and is wounded but recovers to marry the barmaid. The elder brother is reunited with his former sweetheart. 

==Production==
The film was shot in and around Sydney including at Randwick Racecourse. There were also scenes filmed in the bush. 
==Reception==
The racing scene at Randwick was especially praised. 

One reviewer wrote that the film "began well, with excellent pictures of sporting Randwick, but when it got up the country it became somewhat absurd." 
==References==
 

==External links==
*  
*  at AustLit
 

 
 
 
 
 
 


 