Galaxy of Terror
 
 
{{Infobox film name        = Galaxy of Terror image       = Galaxy of terror.jpg| caption     = Theatrical poster writer      = Marc Siegler & Bruce D. Clark music       = Barry Schrader cinematography    = Jacques Haitkin starring    = Edward Albert Erin Moran Ray Walston Taaffe OConnell Robert Englund director    = Bruce D. Clark producer    = Roger Corman distributor = United Artists released    = October 1981 country     = United States language    = English
| budget = $1.8 million Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 194-197 
| gross = $4 million 
}} science fiction/horror cult following of its own since the time of its release.

==Plot==
On a desolate, storm-lashed planet called Morganthus, the last survivor of a crashed spaceship is attacked and killed by an unseen force.

On another planet a very long distance away, two figures are seen playing a strange game. One, an old woman named Mitri, is identified as the controller of the game while the other, whose head is obscured by a glowing ball of red light, turns out to be an all-powerful mystic called the Planet Master. The two speak cryptically of things being put into motion, and the Master instructs one of his military commanders to send a ship to Morganthus.

Without delay, the spaceship Quest blasts off to Morganthus. Piloting the ship is Captain Trantor, a survivor from a famous space disaster that has left her psychologically scarred and unstable.

As the Quest approaches the planet’s atmosphere, it suddenly veers out of control and plunges toward the surface, crash-landing there. After recovering from the landing, the crew prepare to leave the Quest and search for survivors. The team has a psi-sensitive woman among their number named Alluma (Erin Moran). Both she and the surface team have significant problems with  team leader Baelon (Zalman King), who is pushy and arrogant and totally unimpressed by Allumas inability to detect any lifesigns whatsoever.

Making their way across the landscape of the planet, they eventually reach the other vessel. Entering, they find evidence of a massacre that took place. The rescue teams split into two and explore the craft. They find further evidence of something catastrophic having happened and, after disposing of the rest, take one victim back for analysis. Cos, the highly-strung youngest member of the team, despite being reassured by his seniors, becomes increasingly terrified by being on the ship and, a short time later, he is killed by a grotesque creature.

The crew discover that something from the planet pulled them down, and in order to escape, they must investigate.  After some exploration, they discover a massive pyramid-shaped structure, which Alluma describes as "empty" and "dead".  Their explorations of the pyramid lead to a series of exceedingly violent and deadly encounters in which a malevolent force causes several crew members to be dismembered, burned, consumed, raped or crushed to death by monsters created out of each persons unique set of fears.  

Eventually, only two members of the team, Ranger (Robert Englund) and Cabren (Edward Albert), remain alive. Deep inside the pyramid, Cabren encounters the Master (Ray Walston), who has been masquerading as the cook on board the Quest. The Master explains that the pyramid is actually an ancient toy for the children of a long-extinct race, built in order to test their ability to control fear. Cabren kills the Master for allowing his crew to die, but becomes the new Master in his place.

==Cast==

*Edward Albert as Cabren, an experienced and cool-headed space veteran who is the films main protagonist
*Erin Moran as Alluma, the ships empath
*Ray Walston as Kore, the ships cook
*Taaffe OConnell as Dameia, the ships technical officer
*Bernard Behrens as Commander Ilvar, the overall commander of the mission
*Zalman King as Baelon, the rescue units team leader
*Robert Englund as Ranger, one of the ships crewmen
*Sid Haig as Quuhod, crewman and crystal thrower.
*Grace Zabriskie as Captain Trantor, the ships troubled captain
*Jack Blessing as Cos, greenhorn space crewman
*Mary Ellen ONeill as Mitri, companion and gamekeeper for the Master

==Production==

===James Cameron===
While known as being a "B-movie" king, Roger Corman has started the careers of many prominent Hollywood people with his films. Galaxy of Terror was one of the earliest films for director James Cameron, who served as Production Designer and Second Unit Director on the film. It was the second Corman film on which Cameron worked as a crewman; the first being Battle Beyond the Stars (1980). {{Cite web
| last = 
| first = 
| authorlink = 
| coauthors = 
| title = James Cameron: Full Biography
| work = 
| publisher = The New York Times
| date = 
| url = http://movies.nytimes.com/person/10397/James-Cameron/biography
| doi = 
| accessdate = 2009-11-13}}   Working on a tight budget, Camerons innovative film-making techniques came to the forefront. In one scene, Cameron was able to figure out a way to get maggots to wiggle on cue by developing a metal plate onto which the maggots were placed, then ran an electrical current through the plate whenever filming began, causing the maggots to move energetically about. His ability to find low-tech solutions to such problems reportedly made him a favorite of Corman, and eventually allowed him to pursue more ambitious projects.    Cameron would later direct Aliens (film)|Aliens (1986), the sequel to Ridley Scotts Alien (film)|Alien, an important inspiration for Galaxy of Terror. Optical FX Supervisor Tony Randel, who worked with Cameron on Galaxy of Terror, notes on the Shout! Factory DVD release that Aliens, which Cameron helmed, looks a lot like Galaxy of Terror in many ways.

=== Taaffe OConnell and "the worm sex scene" ===
  forced sexual arousal as the monster strips and rapes her. The re-written scene included full nudity and far more explicit sexual content, including simulated sexual intercourse, and ended with Dameia moaning provocatively, covered in excreted slime, and being driven to an orgasm so intense it kills her.  

After informing director Clark and actress OConnell about the changes and having both of them balk at participating in the more sexually explicit scene, Corman decided to direct the entire scene himself. He hired a body double for OConnell to shoot the full-nudity sex sequences, although OConnell ended up in front of the camera for most of the final scene. After the film was completed and submitted for review, the sexual content of the scene was considered graphic enough by the Motion Picture Association of America film rating system (MPAA) to earn the film an initial X’ rating, which still existed at the time and was generally given only to films with adult sexual fare.

Kizer then made a number of very small cuts to the scene. In the interview, he stated that the cuts involved either very brief shots of OConnells face as her character expressed "rhapsodic and ecstatic" looks that too clearly indicated forced arousal and pleasure at being raped, or lewd "humping" motions made by the giant worm while the nude Dameia is ensnared in its tentacles underneath it, motions that obviously simulated sexual intercourse occurring between the two. None of these cuts were longer than one second in length and most only a few frames, and none altered the sequence of the scene. However, they were enough to avoid the dreaded X-rating for the film. The visuals of the final released scene in film and VHS versions (later DVD and Blu-ray disc as well) combined with OConnells verbalization still leave no doubt as to what happens to the character; in fact, the scene was still too explicit for many countries, who either required it be deleted or denied the film a release entirely. All authorized later releases of the film in Europe, America and elsewhere contain the scene in its final, R-rated version. The X-rated clipped materials themselves were lost over time and are not included as part of the new DVD/Blu-ray Disc release or any other release of the film. The films trailer, which still exists online, shows what may be an unaltered view of one of the full nudity shots, containing a slightly different aspect than the one in the final movie.

The scene is discussed on the commentary of the Blu-ray Disc release more than any other aspect of the film. In an interview shown on the Blu-ray Disc, Corman stated that the character of Dameia as re-written had a fear of sex as well as a fear of worms. OConnell, in a separate interview with Femme Fatales magazine, interpreted that Dameia was frightened by her own sexuality and a desire to submit to something more powerful than herself, something the monster created from her fears gives her to a lethal extent.

==Cult status==
Although released to little fanfare (and some ridicule) in 1981, the movie has gradually taken on cult status, largely due to Taaffe OConnells role as Dameia.   

==Release==
The film was originally released on VHS and Laserdisc by Nelson Entertainment. Up until 2010, Galaxy of Terror did not have an authorized region 1 (North America) DVD release. There was a remastered and authorized Region 2 (Europe) Italian disc available from Mondo Home Entertainment released in 2006 which is now out-of-print.    The lack of authorized discs for so many years has led to numerous unauthorized copies of the movie being sold online and elsewhere. 

On July 20, 2010, Shout! Factory released Galaxy of Terror on Region 1 DVD and, for the first time, on Blu-ray Disc. The release also contains cast interviews and behind-the-scenes information on a variety of aspects.     

==Reception==
 
Galaxy of Terror received predominantly negative reviews and shows an All Critics Tomatometer rating of 33% (rotten) from the film review aggregator Rotten Tomatoes.

==References==
 

==External links==
*  
 

 

 

 
 
 
 
 
 
 