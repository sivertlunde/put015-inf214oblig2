Hector (film)
Hector is a Belgian comedy movie, with some drama movie|drama, directed by Stijn Coninx starring Urbanus and Sylvia Millecam. The movie was released in 1987 and was the most successful Flemish movie regarding amount of visitors in Belgian movie theatres until 1990. It was beaten by Koko Flanel, another movie with Stijn Coninx as director and Urbanus as main actor.

==Story==
Hector was brought into an orphanage as a child to cure his chicken pox. He was never picked up by his parents. 35 years later Hector is still there, surrounded by children who are still his play mates. The orphanage is run by nuns who never introduced Hector with the real life and maturity.

His life changes when he is remembered by an aunt named Ella. She and her husband Achiel run a bakers shop and are searching for a cheap worker. Ella is too busy with the rehearsels of a romantic theatre play where she got the main female role. A prominent Hollywood director will attend the play and Ella dreams to become a famous actress. Their son Jos is only interested in bicycle contests. Achiel suffers from a high blood pressure and his marriage with Ella is not that successful.  Furthermore, he suspects Ella is having an affair with Gregoire, the father of Swa.  Swa is Jos biggest competitor in bicycle racing. Gregoire also got the main male role in the romantic play.

Hector seems not to be as clumsy as he looks. He makes clever inventions such as an automatisation of the bakers shop where many objects (such as a mixing machine, egg beater, ...) are now powered by a bicycle driven by Jos.

Ella flirts with Hector hoping Achiel notices this so he will pay more attention to her. Instead of Achiel, this is noticed by Gregoire who got indeed an affair with Ella. Ella soothes Gregoire by telling Hector is just an immature big child. Gregoire does not believe Ella and considered Hector as a rival.

Some time later, Jos is participating a bicycle race which he is about to win. Due to interaction of Hector, Jos falls just before the finish resulting Swa to win the course. Achiel is upset and sends Hector back to the orphanage. There, on the evening of Ellas play, Hector admits to the children he is in love with her. The children convince him to escape.

Hector finds a way to escape and runs to the theatre. He arrives just before the play ends, where "Gregoire" has to kiss "Ella" to promise her eternal love. Hector dresses up as a noble count, set foot on the stage and claims Ella. This ends up in a hilaric improvisation where Hector and Gregoire fight their personal vendetta, whilst the audience thinks this is part of the play.

Hector wins the battle and Ella answers his love. Achiel, also sitting in the audience, is so jealous he gets a heart attack and dies in the theatre.

Some time after the funeral Ella gets a letter from the director: she got a role in a movie and is about to leave to Hollywood. Hector is depressed when it turns out it was not Ella who promised eternal love, but the character she played. While Hector walks back to the orphanage, a taxi is driving Ella to the airport. There she realizes she is in love with Hector and asks the taxi driver to pick up Hector.

==Cast==
* Urbanus - Hector
* Sylvia Millecam - Ella Mattheusen
* Frank Aendenboom - Achiel Mattheusen
* Herbert Flack - Gregoire Ghijssels
* Marc Van Eeghem - Jos Mattheusen
* Hein van der Heijden - Swa Ghijssels
* Ann Petersen - Nun Abdis Cas Baas - Doctor
* Maja van den Broecke - Ikebana
* Chris Cauwenberghs - Owner bicycle shop

== External links ==
* 

 

 
 
 
 
 
 
 
 