The Ghost Valley's Treasure Mysteries
{{Infobox film
| name        = The Ghost Valleys Treasure Mysteries
| image       = Asrare ganje darreye jenni.jpg
| alt         =
| caption     = film poster
| director    = Ebrahim Golestan
| producer    = 
| writer      = Ebrahim Golestan
| starring    = Parviz Sayyad Mary Apick Jahangir Forouhar Shahnaz Tehrani   Sadegh Bahrami Enayat Bakhshi Loreta
| music       = Farhad Meshkat
| cinematography = Ebrahim Golestan
| editing     = 
| studio      = 
| distributor = Golestan Films
| released    =  
| runtime     = 137 minutes
| country     = Iran Persian
| budget      = 
| gross       =
}}
 1974 satirical satirical comedy comedy Cinema Iranian film, directed by Ebrahim Golestan. It was released by Golestan Films, and was Golestans last feature film in Iran. Using symbolic language, the director was accused of having the Mohammad Reza Shah|Shahs support.

==Plot==
While plowing his field, a poor farmer, played by Parviz Sayyad, accidentally uncovers an ancient burial chamber loaded with gold artifacts. Realizing that the trove would somehow liberate him from his bumpkin existence, he brings pieces of it to a jeweler in the city. The jeweler, suspecting that the treasure is stolen, sells the pieces to a master fence.
In the city the farmer is dazzled by department-store glitter, and he spends his subterranean riches on kitchen appliances, velvet furniture, and lawn statuary. These purchases reach his isolated village by caravan.
The mans sudden wealth does not go unnoticed, and his treasure becomes the inevitable quarry of the jewelers wife, the master fence, the owner of a coffeehouse near his village, and a gendarme on the trail of drug smugglers.
The jewelers wife convinces the farmer that he needs a new wife to go with his new existence and marries him to her virgin servant girl. He also acquires an educated ally to help him spend his wealth.
A young Literacy Corps teacher, acting as his lieutenant, conducts public-works projects in the village, commissions an ultramodern home for his patron and hires a painter to paint a wedding portrait of the farmer and his modern bride. The mans dreams of wealth and happiness end when the seismic hand of progress destroys his new home and reburies the treasure.

==Cast and characters==
*Parviz Sayyad as The farmer; a wise rural man which finds the treasure and sells it to the antique shoppers. He gradually changes into a money-lover and spends his wealth by buying luxurious stuffs. His properties are destroyed by the artificial earthquake and he loses both his wives.
*Mary Apick as Masoumeh, the farmers wife
*Mani Haghighi as Ali, the farmers son
*Jahangir Forouhar as Antique Shopper
*Sadegh Bahrami as The jewller 
*Loretta as The jewllers wife
*Shahnaz Tehrani as the servant and the farmers second wife.
*Bahman Zarrinpour as Majid, The villages teacher who becomes the farmers counsellor and assistant. He designs an unusual house on the top of the hill which is destroyed at the end of the film.
*Mahmoud Bahrami as The cafee-shop owner
*Enayat Bakhshi as The Painter
*Mehdi Fakhimzadeh as Coffee-shop garcon 
*Mohammad Goudarzi as Gendarme
*Bagher Sahrarudi as the farmers brother in law
*Reza Hushmand as the mayor (Kadkhoda)
*Mohsen Taghvayi
*Seyyed Yazdi

==Production==
Ebrahim Golestan did the job of making of the antique sculptures in the movie. He also was the writer and the cinematographer of the film. 

==Reception==
Iranian acknowledged writer and poet,   had realized that there is money in this film, so he was a partner with Golestan himself."   

Note to the author of the above line regarding Shamlous accusation. Just because Shamlou made an accusation it does not merit including it as "reception". Per the Persian wiki page for this same film, this film was publicly screened in Tehran for only two weeks before it was ordered by the government to not be publicly screened anymore. Per the same page, this movie was never again publicly screened during the Shahs time. So the fact that this movie was banned by the government is at odds with the claim by Shamlou that there was a financial benefit for the Shah (as if the Shah needed a share of the meager box office of a movie that was screened in a single theater for only two weeks and never shown again). I have seen this movie and it is in fact very clearly critical of the Shah as a superficial modernizer.

==References==
 

==External links==
*  

 
 
 
 