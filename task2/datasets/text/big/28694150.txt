Heat Wave (1990 film)
{{Infobox film
| name = Heat Wave
| image = 
| caption = 
| director = Kevin Hooks
| producer = Jon Avnet Jordan Kerner Steve Golin Sigurjón Sighvatsson
| writer = Michael Lazarou
| starring = Blair Underwood Cicely Tyson James Earl Jones Margaret Avery David Strathairn
| music = Thomas Newman
| cinematography = Mark Irwin
| editing = Debra Neil
| studio = Propaganda Films Avnet/Kerner Productions TNT
| released = August 13, 1990
| runtime = 92 minutes
| country = United States
| language = English
}} thriller television film about 1965 Los Angeles Watts Riots, directed by Kevin Hooks and starring Blair Underwood, Cicely Tyson, James Earl Jones, Margaret Avery, and David Strathairn.

==Cast==
* Blair Underwood as Robert Richardson
* Cicely Tyson as Ruthana Richardson
* James Earl Jones as Junius Johnson
* Margaret Avery as Roxie Turpin
* David Strathairn as Bill Thomas
* Glenn Plummer as J.T. Turpin
* Vondie Curtis-Hall as Clifford Turpin
* Adam Arkin as Art Berman
* Charlie Korsmo as 12-year-old Jason
* Sally Kirkland as Mrs. Canfield
* Mark Rolston as Officer Zekanis

==Awards and nominations==
;CableACE Awards
* Actress in a Movie or Miniseries – Cicely Tyson – won
* Editing a Dramatic or Theatrical Special/Movie or Miniseries – Debra Neil – won
* Movie or Miniseries – Jon Avnet, Jordan Kerner, Steve Golin, and Sigurjón Sighvatsson – won
* Supporting Actor in a Movie or Miniseries – James Earl Jones – won
* Directing a Movie or Miniseries – Kevin Hooks – nominated
 Emmy Award
* Outstanding Supporting Actor in a Miniseries or a Special – James Earl Jones – won

;Writers Guild of America
* Original Long Form – Michael Lazarou – nominated

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 