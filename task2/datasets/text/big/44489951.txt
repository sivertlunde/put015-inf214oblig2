Happy Journey (1943 film)
 
{{Infobox film
| name           = Happy Journey
| image          = 
| caption        = 
| director       = Otakar Vávra
| producer       = 
| writer         = Vilém Werner Otakar Vávra
| starring       = Adina Mandlová
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

Happy Journey ( ) is a 1943 Czechoslovak drama film directed by Otakar Vávra.   

==Cast==
* Adina Mandlová as Prodavacka Helena Truxová
* Jirina Stepnicková as Prodavacka Anna Waltrová-Ortová
* Hana Vítová as Prodavacka Milena
* Natasa Gollová as Prodavacka Fanynka
* Jana Dítetová as Ucednice Bozenka
* Otomar Korbelár as Jan Klement
* Eduard Kohout as Viktor Zych
* Vítezslav Vejrazka as Císník Martin
* Karel Hradilák as Fred Valenta
* Frantisek Kreuzmann as Pepa Hodek
* Jaromíra Pacová as Reditelka prodejního oddelení (as Míla Pacová)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 