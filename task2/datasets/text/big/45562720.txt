Mareyada Haadu
{{Infobox film|
| name = Mareyada Haadu
| image = 
| caption =
| director = R. N. Jayagopal
| writer =R. N. Jayagopal Manjula  Leelavathi
| producer = K. S. Sacchidananda
| music = G. K. Venkatesh
| cinematography = R. N. K. Prasad
| editing = R. Hanumantha Rao
| studio = K V S Movies
| released = 1981
| runtime = 136 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada film Manjula and Leelavathi in lead roles.

The film was a musical hit with the songs composed by G. K. Venkatesh considered evergreen hits. The cinematographer R. N. K. Prasad was awarded with Karnataka State Film Award for Best Cinematographer for this film.


== Cast ==
* Ananth Nag  Manjula
* Sundar Krishna Urs Leelavathi 
* Shivaram
* Rajashankar
* Dingri Nagaraj
* Chethan Ramarao
* Venkatarao

== Soundtrack ==
The music was composed by G. K. Venkatesh with lyrics by R. N. Jayagopal.  The songs composed for the film, especially "Bhuvaneshwariya" , which was a record 8min song and "Sukhada Swapna Gaana", both rendered by S. Janaki, were received extremely well and considered as evergreen songs.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| music_credits = no
| collapsed = no
| title1 = Bhuvaneshwariya Nenne Manasave
| extra1 = S. Janaki
| music1 = 
| length1 = 08:10
| title2 = Rangoli Banali
| extra2 = S. P. Balasubramanyam, S. Janaki
| music2 = 
| length2 = 04:12
| title3 = Baanalli Thara
| extra3 = P. B. Srinivas
| music3 =
| length3 = 04:48
| title4 = Jhantar Mantar
| extra4 = S. Janaki
| music4 =
| length4 = 04:17
| title5 = Sukhada Swapna Gaana
| extra5 = S. Janaki
| music5 =
| length5 = 06:11
}}

==Awards==
* Karnataka State Film Award for Best Cinematographer - R. N. K. Prasad

== References ==
 

== External links ==
*  


 
 
 
 
 
 



 