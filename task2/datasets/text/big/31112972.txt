Rodent to Stardom
 
{{Infobox Hollywood cartoon
| series = Looney Tunes (Daffy Duck/Speedy Gonzales)
| image =
| caption =
| director = Alex Lovy
| producer = William L. Hendricks
| story_artist = Cal Howard
| animator = Volus Jones Ted Bonnicksen La Verne Harding Ed Solomon
| background_artist = Bob Abrams
| voice_actor = Mel Blanc
| musician = William Lava
| distributor = Warner Bros.
| release_date = September 23, 1967 (USA)
| color_process = Technicolor
| runtime = 6 minutes
| movie_language = English
}}
Rodent to Stardom is a 1967 Looney Tunes animated short starring Daffy Duck and Speedy Gonzales. It is the first cartoon to credit Warner Bros. Seven Arts, but still uses the "Abstract WB" titles.

==Plot==
Daffy is plucked for stardom by director Harvey Hassenpfeffer of Colossal Studios—or so he thinks. However, as he was in A Star Is Bored (1956), Daffys real fate is to be the stunt man for the star, in this case Speedy Gonzales. The picture is The Nursery Rhyme Review. First, "the sky is falling"; next, the "rockabye baby" cradle falls from a treetop. Daffy realizes he needs to get rid of Speedy, so he asks for an autograph and traps Speedy in a book, which he deposits in a library. Daffy gets the love scene with Ducky Lamour, but after a number of stunts and just before the kiss, his stand-in, Speedy, takes over.

==See also==
* List of Daffy Duck cartoons

 
 
 
 


 