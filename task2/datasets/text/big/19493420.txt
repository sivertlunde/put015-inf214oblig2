Frk. Vildkat
 
{{Infobox film
| name           = Frk. Vildkat
| image          = 
| caption        = 
| director       = Lau Lauritzen, Jr. Alice OFredericks
| producer       = Henning Karmark
| writer         = Paul Sarauw Armand Szántó Mihály Szécsén
| starring       = Marguerite Viby
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen Alf Schnéevoigt
| editing        = Marie Ejlersen
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Frk. Vildkat is a 1942 Danish family film directed by Lau Lauritzen, Jr. and Alice OFredericks.

==Cast==
* Marguerite Viby - Dolly Hansen
* Ebbe Rode - Peter Bruun
* Gerda Neumann - Louise Holm
* Ib Schønberg - Peters onkel / Oberst Hannibal Brixbye
* Maria Garland - Peters tante / Oberstinde Caroline Brixbye
* Poul Reichhardt - Herbert Rung
* Jon Iversen - Hushovmester Bølner
* Sigurd Langberg - Teaterdirektøren
* Olaf Ussing - Teaterinstruktør Hovmann
* Knud Heglund - Redaktør Hans Bruun
* Per Gudmann - Freddy
* Stig Lommer - Joakim
* Tove Arni - Hushjælp hos Peter Bruun Henry Nielsen - Stationsforstander
* Vera Gebuhr - Louises vendinde

==External links==
* 

 
 

 
 
 
 
 
 
 
 


 