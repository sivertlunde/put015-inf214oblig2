Fourbi
 
{{Infobox film
| name           = Fourbi
| image          = 
| caption        = 
| director       = Alain Tanner
| producer       = Alain Tanner
| writer         = Alain Tanner Bernard Comment
| starring       = Karin Viard
| music          = 
| cinematography = Denis Jutzeler
| editing        = Monica Goux
| distributor    = 
| released       =  
| runtime        = 114 minutes
| country        = France Switzerland
| language       = French
| budget         = 
}}

Fourbi is a 1996 French-Swiss drama film directed by Alain Tanner. It was screened in the Un Certain Regard section at the 1996 Cannes Film Festival.   

==Cast==
* Karin Viard - Rosemonde
* Jean-Quentin Châtelain - Paul
* Cécile Tanner - Marie
* Antoine Basler - Pierrot
* Robert Bouvier - Kevin
* Maurice Aufair - Pauls Father
* Jean-Luc Bideau - Cameo appearance
* Teco Celio - Le Garagiste
* Jed Curtis - Le Sponsor
* Jacques Denis - Cameo appearance
* François Florey - La Comédien
* Michèle Gleizer - Pauls Mother
* Nathalie Jeannet - La Comédien
* Thierry Jorand - Le Dragueur
* Jocelyne Maillard - La Comédien
* Pierre Maulini - Lhuissier
* Jacques Michel - Patron de Bistrôt
* Jean-Marc Morel - Lhômme ivre
* Ariane Moret - La Vendeuse
* Frédéric Polier - Le Chasseur
* Jacques Probst - Le Client de café

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 