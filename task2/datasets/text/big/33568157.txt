Vivaramana Aalu
{{Infobox film
| name           = Vivaramana Aalu
| image          = Vivaramana Aalu Video Cover.jpg
| image_size     =
| caption        = DVD cover
| director       = K. Selva Bharathy
| producer       = V.A. Durai
| story          = VG. Ramalingam
| screenplay     = K. Selva Bharathy
| starring       =   Deva
| cinematography = K. Vijay Chakravarthi
| editing        = B. Lenin V. T. Vijayan
| distributor    =
| studio         = 
| released       = 14 January 2002
| runtime        =
| country        = India
| language       = Tamil
| preceded_by    =
| followed_by    =
| website        =
}}
 2002 Tamil Devayani and Mumtaj in lead roles. The film was released on 14 January 2002. 

==Plot==
Mayilsamy (Sathyaraj) is a petty thief who is determined to make it big in life, by hook or crook. Even it means going through a marriage with a stranger, a trusting village belle, Pappu (Devayani (actress)|Devayani), deserting her soon after and warming his way into the heart of Puppy (Mumtaj), a wealthy heiress. Of course, he realises his folly by the climax.

==Cast==

*Sathyaraj as Mayilsamy / Dr. John Brito Devayani as Pappu
*Mumtaj as Puppy Vivek as "Suitcase" Subbu
*Prathap K. Pothan as Puppys father
*Manochithra as Puppys mother Ponnambalam as Subramani / Thotta
*Vinu Chakravarthy as
*Mayilsamy as Mariappan Senthil as Pappus uncle (guest appearance)
*Kanal Kannan as Karuppu (guest appearance) Monal in a guest appearance

==Soundtrack==

{{Infobox album |  
  Name        = Vivaramaana Aalu|
  Type        = soundtrack | Deva |
  Cover       = |
  Released    = 2002 |
  Recorded    = 2001 | Feature film soundtrack |
  Length      = 24:38 |
  Label       = Five Star Audio | Deva |  
  Reviews     = |
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 2002, features 5 tracks with lyrics written by P. Vijay, Na. Muthukumar and Kabilan.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Lyrics !! Duration
|- 1 || Rottila Kiss Adhicha || Mathangi || Na. Muthukumar || 3:52
|- 2 || Eeccha Eechambazham || Krishna Raj, Pushpa Sriram || Kabilan || 4:50
|- 3 || Adiye Aaravalli || Sabesh-Murali|Sabesh, Tippu (singer)|Tippu, Chitra Sivaraman || rowspan=2|Na. Muthukumar|| 5:28
|- 4 || Tippu || 5:34
|- 5 || Vaadaa Vaadaa Paalkaaraa || Anuradha Sriram || P. Vijay|| 4:54
|}

==Reception==
A critic of lavan.fateback.com said : " Verbal and situational comedy, some of them enjoyable, is packed in. The director makes no bones about catering to the masses. Giving Sathyaraj company is Vivek, straining his voice to a cresendo, fitting in well with the directors requirements. Mumtaz, for a change, has a meaty role, and uses well the opportunity given to her.". The critic advised the film for Sathyaraj fans. 

==References==
 

 
 
 
 
 