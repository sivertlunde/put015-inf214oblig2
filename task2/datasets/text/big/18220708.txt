Money Hai Toh Honey Hai
 
{{Infobox film
| name                      = Money Hai Toh Honey Hai
| image                     = Money Hai Toh Honey Hai.jpg
| caption                   = Theatrical release poster
| director                  = Ganesh Acharya
| producer                  = Kumar Mangat Pathak
| music                     = Nitz N Sony (Nitin Arora and Sony Chandy)
| story                     = Muazzam Beg
| screenplay                = Muazzam Beg Govinda Hansika Motwani Celina Jaitley Manoj Bajpai Ravi Kissen Antara Biswas
| cinematography            = Sriram
| editing                   = Ballu Saluja Big Screen Entertainment Pvt Ltd.
| released                  =  
| runtime                   = 142 minutes
| country                   = India
| language                  = Hindi
| budget           = 
| gross                     = Rs. 39 crore (India) $1,985,423 (overseas)
}}
Money Hai Toh Honey Hai is a Bollywood comedy film directed by Ganesh Acharya and produced by Kumar Mangat, story and screenplay by Muazzam Beg.  It features a cast of stars including Govinda (actor)|Govinda, Aftab Shivdasani, Upen Patel, Hansika Motwani, Celina Jaitley  and Manoj Bajpai in the lead roles with Ravi Kissen, Antara Biswas and Prem Chopra in supporting roles. Esha Deol and the director of the film Ganesh Acharya make special appearances.

==Plot==
Money Hai Toh Honey Hai is a striking comedy, which deals with six individuals. Bobby Arora (Govinda (actor)|Govinda) is a happy-go-lucky guy who runs away from home to prove himself as a to-do child. Lallabhai (Manoj Bajpai) becomes wealthy after his lottery win of 1 crore rupees. But he’s back on the road as his business fails and he loses all his money. Gaurav (Aftab Shivdasani) is a copy writer who is fired from his job. Manik (Upen Patel), a struggling model who sleeps with a middle-age fashion designer, Dolly (Archana Puran Singh), hoping to make it big. Ashima Kapoor (Hansika Motwani) is a successful TV star but she’s not happy with her career and is desperate to do films as the lead heroine. Shruti (Celina Jaitley) is a struggling dress designer.

One day, they all get an SMS informing them that they are the owners of Shahenshah Jaiswals (Prem Chopra) 1000-crore company. However, their happiness does not last long for their lawyer reveals that there’s a loan of 1200 crore on the company and they must repay it. Until the loan is repaid, they will be kept under house arrest. So, using the extra cloth left from the failing send to Korea they make garments.((unclear)) But not just any, the type they make is enough for an ordinary person to buy. They display that in a fashion show, getting ordinary people from the street. At the end, they succeed and keep on making more stores for other parts of India with the help of the bank. The movie ends with Bobby and Ashima getting married.

==Cast== Govinda as Bobby Arora
* Aftab Shivdasani as Gaurav Negi
* Upen Patel as Manikchand Manik Khiralal
* Hansika Motwani as Ashima Kapoor
* Celina Jaitley as Shruti
* Manoj Bajpai as Lallabhai Bharodia
* Ravi Kishan as Parag Batra

* Prem Chopra as Krishnakumar Jaiswal 
* Archana Puran Singh as Dolly
* Kim Sharma as Sara
* Priyanka Sharma as Anjali Sukhani
* Javaid Sheikh as Bobbys father
* Ketki Dave as Bobbys Mother
* Kurush Deboo as Production Supervisor
* Shruti Sharma as Sakkubai
* Paresh Ganatra as Chaman Patel 
* Viju Khote as Jaiswals Doctor
* Rakhee Tandon as Mukti Kapoor (Ashimas boss)
* Nitin Arora  as Bhola Plumber
* Esha Deol (Special appearance in song Ta Na Na)
* Ganesh Acharya (Special appearance in song Chhuriyaan)
* Om Puri as the Narrator
* Vicky Sidana as Ravan
* Sumit Arora as Traffic Constable
* Snehal Sahay
* Antara Biswas as manish sarraf wife
==Music==
The album has eight tracks including one instrumental, composed by Nitz N Sony (Nitin Arora and Sony Chandy).

=== Track Listings ===
{| border="2" cellpadding="5" cellspacing="0" style="margin: 1em 1em 10; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| # || Title || Singer(s) || Length
|- Nitin Arora, Sony Chandy || 3:31
|- Nitin Arora, Sony Chandy, Shantanu Hudlikar || 4:23
|-
| 3 || "Chhuriyaan" || Sunidhi Chauhan, Labh Janjua || 4:57
|- Nitin Arora, Sony Chandy || 4:34
|-
| 5 || "Rangeeli Raat" || Daler Mehndi, Sunidhi Chauhan, Master Saleem, Arya ||  4:45
|-
| 6 || "No Big Deal" || Suraj Jagan || 5:29
|-
| 7 || "Dance Master" || Shaan (singer)|Shaan, Shivamani || 4:24
|-
| 8 || "Hope" (Instrumental) ||  || 5:30
|-
|}

==Reception==

===Critical reception===
 
Rajeev Masand of CNN-IBN rated it 2/5: "Sorely lacking in drama and genuine humour, Money Hai Toh Honey is mind-numbingly dull because there’s no conflict or plot progression, and everything seems to fall into place too conveniently, which even ruins the amazing comedy at the first half!"

Mayank Shekhar of Hindustan Times rated it 2.5/5: "Scenes may well be dull and weak in parts. And they are. But for a comedy film, it needs to have some more sense-of-humor."

Raja Sen of Rediff rated it 1.5/5: "This is, first and foremost, a tacky film. The tastelessness flowing right down into the script, however, makes for a very different league of disaster."

Taran Adarsh of Bollywood Hungama rated it 2/5: "Money Hai Toh Honey Hai is a poor show. Watch the first half and laugh your total head-off, but at the second half, try to enjoy a damp squib!"

Nikhat Kazmi of The Times of India gave it the highest rating it got: 3.5/5: "Money Hai Toh Honey made a complete laugh-riot in the first part, but soon after the interval a lot of drama and romance jumps in which is totally something you dont need in a comedy movie, it needed a much smarter script to keep the laughter ringing."

==References==
 

==External links==
*  

 
 
 