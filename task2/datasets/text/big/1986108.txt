Jurm (2005 film)
{{Infobox film
| name           = Jurm
| image          = Jurm_(2005_film).jpg
| caption        =
| director       = Vikram Bhatt
| producer       = Aashish Singh Anurag Singh!! K.P. Singh
| writer         =
| narrator       =
| starring       = Bobby Deol Lara Dutta Milind Soman
| music          = Anand Raj Anand Anu Malik
| cinematography = Pravin Bhatt
| editing        = Kuldeep K. Mehan
| distributor    =
| released       = 10 February 2005
| runtime        =
| country        = India
| language       = Hindi
| gross          =
| preceded_by    =
| followed_by    =
| budget         =
}}
 thriller film written and directed by Vikram Bhatt and  was released in 2005.

==Summary==

Avinash Malhotra, a rich businessman, is arrested for murdering his wife Sanjana. All the evidence points towards his guilt, but Avinash himself is unsure whether he really killed her. A guilt-ridden and distraught Avinash asks the help from his friend and lawyer Rohit to escape from sure punishment. But Avinash doesnt know that he is going to get in an even bigger mess than he is trying to get out from.

==Plot==

Avinash and Sanjanas married life goes downhill when he realizes that she has been lying to him, even about petty things. Even though there is no concrete evidence, he is convinced that Sanjana is cheating on him. His attitude towards her changes, which becomes a cause of concern for his friends. They try in all futility to talk sense into him, but his anger towards Sanjana increases. Things come to an ugly turn when Avinash abuses and embarrasses Sanjana in a party they are attending.

Sanjana heads back to their home and Avinash follows. Next day, Avinash learns that he brutally killed Sanjana in front of her uncle. A case stands in the court and he is given a life sentence. Unconvinced of his guilt, he asks his lawyer and friend Rohit for help. Avinash promises Rohit the power of attorney of his property in lieu of helping him escape. Rohit helps him escape, but backstabs him by fleeing with the money and leaving him for dead.

Avinash is saved by his friend Sonia and nurtured back to safety and health. Determined to exact his revenge on Rohit, Avinash zeroes the location of latter in Malaysia. However, he is stunned to see Sanjana there, hale and alive, living with Rohit. He also narrows down and confronts Sanjanas uncle, who confirms that Sanjana was having an affair with Rohit and that he helped the duo to remove Avinash from their life.

An enraged Avinash kills Sanjanas uncle and sets out to seek revenge. First, he confronts her, creating fear and panic in her heart. She tells about him to Rohit, who is sceptical and thinks that Sanjana is losing it. After Sanjana realizes this, she has a fight with him. Later, Avinash creates a rift between them, paying them back in their own coin. Unaware of all this drama, an Inspector is sent to find Avinash, who is still assumed to be missing.

The Inspector also turns up in Malaysia, following the trail. Here, Rohit accidentally runs into Avinash and realizes that Sanjana was true. Sanjana is on the verge of breaking up, and Rohit becomes afraid that she might spill the beans. Rohit and Sanjana attend a party, where she almost lets the secret out. They have a confrontation and turn to go home, just like it happened with Avinash. They go into the parking lot, where Avinash is patiently waiting for them.

Avinash knocks Sanjana unconscious and starts beating him up. Next day, Sanjana regains consciousness and finds that she has been arrested for killing Rohit. Based on the circumstances, Sanjana is found guilty and given life imprisonment. The Inspector, who has deduced all the story by now, meets her in her cell and expresses his satisfaction over her plight. Avinash too meets her one last time, after which he meets Sonia, indicating that he will start his life afresh with her.

==Cast==
*Bobby Deol ....  Avinash Malhotra 
*Lara Dutta ....  Sanjana Malhotra 
*Shakti Kapoor ....  Sanjanas Uncle
*Gul Panag ....  Sonia 
*Milind Soman ....  Rohit
*Ashish Vidyarthi ....  Police officer

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "O Sanam O Sanam" 
| Udit Narayan, Pamela Jain
|-
| 2
| "Meri Chahato Ka Samudar"
| Abhijeet Bhattacharya|Abhijeet, Alka Yagnik
|-
| 3
| "Aksar Ye Hota Hai Pyar Mein"
| Kunal Ganjawala
|-
| 4
| "Nazrein Teri Nazrein"
| Adnan Sami
|-
| 5
| "Rabba Rabba"
| KK (singer)|KK, Gayatri Iyer
|-
| 6
| "Main Yahan Tu Kahan"
| Abhijeet
|-
| 7
| "Dil Dil"
| Udit Narayan, Shreya Ghoshal
|}

==External links==
*  

 

 
 
 
 