The Imposter (2012 film)
{{Infobox film
| name           = The Imposter
| image          = TheImposter2012Poster.jpg
| director       = Bart Layton
| producer       = Dimitri Doganis
| music          = Anne Nikitin
| cinematography = Erik Alexander Wilson Lynda Hall
| editing        = Andrew Hulme
| studio         = Film4 Productions A&E (TV channel)|A&E IndieFilms Raw TV   Linked 2013-08-23  24 Seven Productions  Randy Murray Productions 
| distributor    = Picturehouse Entertainment Revolver Entertainment Indomina Releasing
| released       =  
| runtime        = 99 minutes
| country        = United Kingdom United States
| language       = English
| gross          = $1,999,277 
}}
The Imposter is a 2012 British-American documentary film about the 1997 case of the French confidence trickster Frédéric Bourdin, who impersonated Nicholas Barclay, a Texas boy who disappeared at the age of 13 in 1994. The film includes interviews with Bourdin and members of Barclays family, as well as archive television news footage and reenacted dramatic sequences.

== Plot ==
Bourdin, who turned out to have a long record of impersonating various children, real or imaginary, embellished his claim to be Nicholas Barclay by alleging that he had been kidnapped for purposes of sexual abuse by Mexican, European, and U.S. military personnel and transported from Texas to Spain. His impersonation fooled several officials in Spain and the U.S., and he was apparently accepted by many of Barclays family members, even though he was seven years older than Barclay, spoke with a French accent, and had brown eyes and dark hair rather than Barclays blue eyes and blonde hair. The impersonation was eventually unearthed as a result of the suspicions of a private investigator, Charles (Charlie) Parker, and an FBI agent, Nancy Fisher. Bourdin subsequently made a full confession, and in the film he elaborates on the various stages in his impersonation.

== Credits ==
Director Bart Layton has produced, written, and directed several television documentaries, but this film is his feature film debut. Producer Dimitri Doganis has produced many television documentary films and series.

;Interviews:
* Frédéric Bourdin
* Carey Gibson
* Beverly Dollarhide
* Bryan Gibson
* Codey Gibson
* Nancy Fisher
* Charlie Parker
* Bruce Perry, M.D., Ph.D
* Philip French

;Drama sequences:
* Adam OBrian as Frédéric Bourdin
* Anna Ruben as Carey Gibson
* Cathy Dresbach as Nancy Fisher
* Alan Teichman as Charlie Parker
* Ivan Villanueva as Social Worker
* Maria Jesus Hoyos as Judge
* Anton Marti as Male Police Officer
* Amparo Fontanet as Female Police Officer
* Ken Appledorn as U.S. Embassy Official

== Reception ==
The film has received almost universal critical acclaim – hailed as one of the stand-out films of Sundance 2012 – and gained a Rotten Tomatoes rating of 95%.  The film received a Grand Jury documentary prize at the   for Best Film, Best Director, Best Debut Director, Best Technical Achievement- Editing, Best Achievement in Production, and Best Documentary. It was also shortlisted for an Academy Award for Best Documentary.     ,   ,  It was nominated for 2 BAFTAs, winning 1.

UK-based film magazine   and as cinematic as Man On Wire, this is an unnerving story immaculately told and a strong contender for doc of the year." 

Peter Bradshaw, film critic for The Guardian, awarded the film five stars, writing, "This film is as gripping as any white-knuckle thriller: it is one of the years best." 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 