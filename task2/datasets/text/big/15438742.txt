Anjathe
{{Infobox film
| name = Anjathe
| image = 
| director = Mysskin
| writer = Mysskin Narain Prasanna Prasanna Ajmal Ameer Vijayalakshmi
| producer = Nemichand & Hitesh Jhabak
| cinematography = Mahesh Muthuswamy
| music =  Sundar C Babu
| released =  
| runtime = 190 minutes Tamil
| country = India
| budget =
| revenue =
}} Tamil drama Narain   The film opened to rave reviews for its technical expertise, avant-garde filmmaking and for the performance of actor Narain.  The film was remade in Kannada as Anjadiru.

==Plot==
Satya (Narain (actor)|Narain) and Kiruba (Ajmal Ameer) are close friends living opposite each other in the neighbourhood. Satya is a rowdy, wasting his time drinking merrily and getting involved in fights. Though he and Kiruba passed college with a first class degree, Satya has no interest in joining the police force like his father. Kiruba studies hard to become a Sub-Inspector(SI), challenging Satya to do the same. Just a day before the exam, Satya decides to take up the exam as well, much to his friends surprise and with little head start, passes the physical, written and interview, with the help of malpractices and influence from his uncle and becomes an SI. However, Kiruba doesnt make it through despite being straight forward; this creates a rift between the two.

Kirubas father lies to Satya that he has gone to Bombay, but Satya finds him in the local bar, having become a drunkard.

Parallel is the story of Daya (Prasanna (actor)|Prasanna) and Loganathan (Pandiarajan), who extort money from businessmen through kidnappings. Before Satya became a policeman, he beat up Daya for molesting Kirubas sister, though none including Kiruba know of this.

Satya gains fame by holding off knife-wielding men who come to kill an injured man at a hospital, though the man is killed later in the night. As a result Satya is drafted into a special task force to catch the gang members related to the crime. Daya and Loganathan carry out two kidnappings, raping the victims on both occasions, and release them for ransom money. Kiruba is beaten up by the police for a skirmish at the local bar; his father suffers a heart attack after getting his son back from the police station. At this point of time Kiruba is employed by Daya, mainly because the former is distraught and will fall easily to the lure of money and booze. In the third time however, the police are informed, but the kidnappers find out by chance and change the drop off point at the last moment. They give the police the slip, but narrowly avoid capture, with Kiruba providing shelter for them in his house. Kiruba joins along to get money for his fathers angioplasty, though he doesnt realize until its too late that he is doing illegal things and harming young children.
 hijack the Inspector Generals car with his two daughters inside. Switching soon to a disguised-dog van to get past checkposts with the two girls, Daya, Kiruba and another accomplice arrive at a sugarcane field. Satya recognizes Kirubas voice from the ransom call he makes to the IG, and follows his sister from the hospital, who has been instructed to bring a bag full of cash from their home(kept there by Daya), to the sugarcane field.

The plot to kidnap the IGs daughters is an elaborate ruse to divert the police force to the south of the city while they escape from the north. The special task force, who are in the south of the city, realize the plot and upon learning that Satya is alone in the north of the city, head in that direction.

In the sugarcane fields Kiruba shows compassion towards the two girls, treating them softly. Upon hearing some noise in the field, Daya splits the group, doing so to meet up directly with Kirubas sister who has the money-bag. He tries to rape her, but Satya intervenes and a fist fight ensures, but the special task force comes upon them, and kill Daya by setting it up as an encounter.
The two girls split and escape from the third accomplice, only for one of them to be recaptured by him, but is let off being too tiring to carry. Kiruba and the third accomplice try to run away, with Kirubas sister behind him. Just as he leaves her to escape, she reveals that she is in love with Satya and uses the situation to handcuff herself to Kiruba to avoid him from escaping. In the end Satya shoots Kiruba in self-defense and to save one of the IGs daughters from being shot. When Kiruba lies on sathyas lap shot, Kiruba finds a ring he once gifted to sathya on sathyas bday which Kiruba believed thrown away by Sathya drunk while partying at the bar which in tern wets Kirubas eye and dies.  The ending scenes of the movie show that Satya and Kirubas sister get married and have a son, whom they name Kiruba.

==Cast== Narain as Sathyavan Prasanna as Deena Dayalan
* Ajmal Ameer as Kirubakaran
* Vijayalakshmi as Udhra Livingston as Masilamani
* Pandiarajan as Logu
* Snigdha Akolkar as the dancer in the workshop
* Ponvannan as the Assistant Commissioner Keerti Vaasan
* Bomb Ramesh as Kuruvi (Satya & Kirubas friend)
* P.S. Sridhar as Deena Dayalans right hand
* M. S. Bhaskar as Sathyavans father Priyasri as Sathyavans mother

==Soundtrack==

The music composed by Sundar C Babu 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Atcham Thavir || Mysskin || Bharathiyar || 04:37
|-
| 2 || Love Theme 1 || Instrumental || || 02:16
|-
| 3 || Kaththazha Kannaala || Naveen Madhav || Kabilan || 04:37
|-
| 4 || Manasukkul Manasukkul || Swetha || Priyan || 04:47
|-
| 5 || Kannadasan Karaikudi || Mysskin || Kabilan || 04:07
|-
| 6 || Veenai adi Nee Enakku || Sundar C Babu || Bharathiyar || 02:18
|-
| 7 || Good Vs Evil Theme || Instrumental || || 01:56
|-
| 8 || Veenai adi Nee Enakku || Instrumental || || 02:12
|-
| 9 || Love Theme 2 || Instrumental || || 01:58
|}
==Box office==
* The film was a commercial success grossing almost $2 million at the box office as per the charts published by behindwoods.com. 
* Nemichand Jabak, the producer of the film, had spent nearly $1 million on Anjathey for its technical expertise. 

==Awards==
*Ajmal Ameer won the Filmfare Award for Best Supporting Actor and got nominated for Vijay Award for Best Debut Actor
*Nominated for Filmfare Award for Best Director - Tamil - Mysskin
*Nominated for Vijay Award for Best Director - Mysskin Prasanna
* Nominated - Filmfare Award for Best Supporting Actor – Tamil - Prasanna

==References==
 

 

 
 
 
 
 
 
 
 