A Challenge to Democracy
{{Infobox film
| name = A Challenge to Democracy
| image =
| alt =
| caption =
| director =
| producer =
| writer =
| narrator = John Baker
| starring =
| music =
| cinematography = Tom Parker Charles Mace
| editing =
| studio = The War Relocation Authority
| distributor =
| released =  
| runtime = 21 minutes
| country = United States
| language = English
| budget =
| gross =
}}
  ]]
A Challenge to Democracy is a twenty-minute short film produced in 1944 by the War Relocation Authority. The film could be considered a companion piece or sequel to 1942s Japanese Relocation.

This film is more sober in its description. The film makes it clear that the Japanese Americans were forced from their circumstances, and that they were made to live in a rather barren relocation camp, which was surrounded by armed guards. The film states bluntly that the medicine available at the camp was the same as that of everybody else in war time—barely adequate.

More positive features of camp life are also shown, whatever their histocial accuracy may be: it shows the internees organizing a self-government, schools, and places of worship, as well as contributing to the war effort though industry. It also shows that some families were allowed to leave the camp if they were considered to be loyal enough.

== See also ==
* List of Allied propaganda films of World War II

==External links==
*  
*  

 
 
 
 
 


 