Sign of the Beast
 
{{Infobox film
| name           = Sign of the Beast
| image          = 
| caption        = 
| director       = Jaakko Pakkasvirta
| producer       = Jaakko Pakkasvirta
| writer         = Jaakko Pakkasvirta Timo Humaloja Arvo Salo
| starring       = Esko Salminen
| music          = 
| cinematography = Esa Vuorinen
| editing        = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}
 Best Foreign Language Film at the 54th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Esko Salminen as Kaarlo
* Irina Milan as Karin
* Tom Wentzel as Helmut von Stein
* Hannu Lauri as Olavi
* Vesa-Matti Loiri as Usko
* Jukka-Pekka Palo as Lieutenant Valonen
* Esa Pakarinen Jr. as War Photographer
* Martti Pennanen as General
* Yrjö Parjanne as Captain
* Kalevi Kahra as Seargeant Major

==See also==
* List of submissions to the 54th Academy Awards for Best Foreign Language Film
* List of Finnish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 