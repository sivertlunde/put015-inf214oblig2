The White Cat (film)
{{Infobox film
| name           = The White Cat
| image          =
| caption        =
| director       = Hasse Ekman
| producer       =
| writer         = Hasse Ekman
| narrator       =
| starring       = Alf Kjellin Eva Henning Sture Lagerwall Gertrud Fridh
| music          = Lille Bror Söderlundh
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 93 minutes
| country        = Sweden Swedish
| budget         =
| gross          =
}}
 Swedish drama drama film directed by Hasse Ekman.

==Plot==
A man arrives one night by train to Stockholms Central Station. The man has lost his memory. Newspapers report about an escaped insane sex offender, and the man dreads that it might be him. 

In a café at the train station the man meets a waitress named Auri. She realizes that the man has no money and no place to go. He tells her about his situation. She offers to pay for his food and to take him home with her. The man, who calls himself X, and Auri start to trace his repressed memories and past life, while he dreads to find out why and what he fled.

==Cast==
*Alf Kjellin as "X", The man without identity  
*Eva Henning as Auri Rautila, waitress 
*Sture Lagerwall as Elias Sörbrunn, artist  
*Gertrud Fridh as "Pax"
*Hugo Björne as "Väglusen"
*Ingrid Borthen as Ingeborg Eksell 
*Gunnar Björnstrand as Jarl Eksell 
*Gull Natorp as Otti Patkull 
*Doris Svedlund as Girl in the school house  
*Gösta Gustafson as Filip - the girls father  
*Margit Andelius as Ebba Patkull 
*Stig Järrel as Algot
*Peter Blitz as Jerker 
*Arne Ragneborn as Thief at the Central station
*Alf Östlund as Ticket collector 

==External links==
* 

 

 
 
 
 
 
 
 

 