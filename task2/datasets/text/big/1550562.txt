The Cars That Ate Paris
 
 
{{Infobox film
| name           = The Cars That Ate Paris
| image          = cars that ate paris movie poster.jpg
| image_size     = 215px
| alt            =
| caption        = Promotional poster
| director       = Peter Weir
| producer       = Hal and Jim McElroy
| screenplay     = Peter Weir
| based on       = Original story: Peter Weir and Keith Gow
| starring       = John Meillon Terry Camilleri Kevin Miles
| music          = Bruce Smeaton
| cinematography = John McLean
| editing        = Wayne LeClos
| studio         = Salt Pan Films Royce Smeal Film Productions
| distributor    = MCA (Australia) British Empire Films (Australia) New Line Cinema  
| released       =  
| runtime        = 91 minutes 74 minutes  
| country        = Australia
| language       = English
| budget         = $250,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p277 
}} horror comedy film. Directed by Peter Weir, it was his first feature film.  Shot mostly in the rural town of Sofala, New South Wales, the film is set in the fictional town of Paris in which most of the inhabitants appear to be directly, or indirectly, involved in profiting from the results of car accidents.

==Plot== lobotomies with power tools and kept as "veggies" for medical experiments by the earnest town surgeon. The young men of the town salvage and modify the wrecked vehicles into a variety of strange-looking cars designed for destruction.
 caravan where they meet with an accident that kills George. Arthur is spared and looked after by the Mayor of Paris, Len Kelly (John Meillon), who invites Arthur to stay in his home as one of his family; his two young daughters have been "adopted" after being orphaned in motor accidents in the town.
 Aborigine the older men of the town burn the guilty drivers car as he is held down.

The Mayor appoints Arthur the town Parking Inspector complete with brassard and Army bush jacket that further irritates the young men. The situation reaches its boiling point the night of the towns annual Pioneers Ball which is a fancy dress and costume party.  What was planned to be a "car Gymkhana (motorsport)|gymkhana" by the young men turns into an assault on the town where both sides attack each other killing several of the residents.  Arthur regains his driving confidence when he repeatedly drives the Mayors car into his former hospital orderly supervisor who is one of the hoons. The film closes with Arthur, and the towns other residents, leaving Paris in the night.

==Cast==
* John Meillon as Mayor Len Kelly
* Terry Camilleri as Arthur Waldo
* Chris Haywood as Darryl
* Bruce Spence as Charlie
* Kevin Miles as Dr. Midland
* Rick Scully as George Waldo
* Max Gillies as Metcalfe
* Peter Armstrong as Gorman
* Joe Burrow as Ganger
* Deryck Barnes as Al Smedley
* Edward Howell as Tringham
* Max Phipps as Reverend Mulray
*Melissa Jaffer as Beth

==Production==
Peter Weir got the idea to make the film while driving through Europe where road signs on the main French roads diverted him into what he perceived as strange little villages.  It originally started as a comedy to star  , who had previously worked in a large variety of positions on a number of films. Most of the budget came from the Australian Film Development Corporation with additional funds from Royce Smeal Film Productions in Sydney. Shooting began in October 1973, primarily on location in Sofala, New South Wales. 

==Release==
The producers unsuccessfully attempted to negotiate an American release for the film with Roger Corman after it was shown with great success at the Cannes Film Festival.  Shortly afterwards Corman recruited Paul Bartel to direct his Death Race 2000;  Bartel hadnt seen The Cars That Ate Paris but he was aware that Corman had a print of the film. 

The movie struggled to find an audience in Australia, changing distributors and with an ad campaign unsure whether to pitch it as a horror film or art film. However it has become a cult film.  In 1980, $112,500 had been returned to the producers.  It received an American release in 1976 by New Line Cinema under the title The Cars That Eat People with added on narration and other differences. 

In 1992, it was adapted as a musical theatre work by Chamber Made Opera.

 , the film holds a 63% "Fresh" rating on Rotten Tomatoes. 

==See also==
* Cinema of Australia
* Peter Weir

==References==
 
*Gordon Glenn & Scott Murray, "Production Report: The Cars That Ate Paris", Cinema Papers, January 1974 p18-26

==External links==
*  
*  
*   at Australian Screen Online
* 
*  at  
*  at Oz Movies

 
 

 
 
 
   
 
   
   
 
 
 
   
 
 
 
 
 