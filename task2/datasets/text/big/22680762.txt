Vincere
{{Infobox film
| name           = Vincere
| image          = Vincere.jpg
| caption        = English-language poster
| director       = Marco Bellocchio
| producer       = Mario Gianani
| writer         = Marco Bellocchio Daniela Ceselli
| starring       = Giovanna Mezzogiorno Filippo Timi Fausto Russo Alesi Michela Cescon Pier Giorgio Bellocchio Corrado Invernizzi
| music          = Carlo Crivelli
| cinematography = Daniele Ciprì
| editing        = Francesca Calvelli
| distributor    = 01 Distribution IFC Films
| released       =  
| runtime        = 128 minutes
| country        = Italy France
| language       = Italian
| budget         = 9 million €
| gross          = 2,089,000 €
}}
Vincere (in English, Win) is a film that is based on the life of the first wife of Benito Mussolini. It stars Giovanna Mezzogiorno as Ida Dalser and Filippo Timi as Benito Mussolini. It was filmed under the direction of Marco Bellocchio, who also wrote the screenplay with Daniela Ceselli, and it was released 22 May 2009 in Italy. It was the only Italian film in competition at the 2009 Cannes Film Festival.   
 Award for Best Actress 2010.

==Synopsis==
The movie relates the story of Ida Dalser, who fell in love with the future Italian Fascist leader, Benito Mussolini, supported him while he was unemployed in the early 1910s, and married him, presumably around 1914.  She bore Mussolini a son, Benito Albino, before the outbreak of World War I.  The two lost touch during the war years and, upon discovering him again in a hospital during the war, she also discovered Rachele Guidi, who had married Mussolini in 1915, and a daughter born in 1910 when Guidi and Mussolini were living together.  

Historically, following his political ascendency, Mussolini suppressed the information about his first marriage and he (through the Fascist party) persecuted both his first wife and oldest son and committed them forcibly to asylums. Dalser died in an asylum in Venice in 1937 at the age of 57 of brain haemorrhages and Benito Albino died in 1942 at the age of 26 in an asylum near Milan after repeated coma-inducing injections. 

==Cast==
*Giovanna Mezzogiorno as Ida Dalser
*Filippo Timi as both Benito Mussolini and Benito Albino Mussolini
*Michela Cescon as Rachele Guidi
*Fausto Russo Alesi as Riccardo Paicher
*Pier Giorgio Bellocchio as Pietro Fedele
*Corrado Invernizzi as Dottor Cappelletti

==Reception==
The film received universal acclaim from film critics, with rating of 85 from the review aggregate site Metacritic,  as well as a 92% "fresh" rating from Rotten Tomatoes with the sites consensus being: "Part political treatise, part melodrama, Marco Bellocchios Mussolini biopic forsakes historical details in favor of absorbing emotion -- and provides a showcase for a stunning performance from Giovanna Mezzogiorno." 
 Un Prophète from Jacques Audiard and the The White Ribbon from Michael Haneke. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 