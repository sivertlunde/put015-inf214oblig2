Young Woodley (1930 film)
{{Infobox film
| name           = Young Woodley
| image          =
| caption        =
| director       = Thomas Bentley
| producer       = 
| writer         = John Van Druten (play)   Victor Kendall
| starring       = Madeleine Carroll Frank Lawton Sam Livesey   Gerald Rawlinson
| music          = 
| cinematography = Claude Friese-Greene
| editing        = Sam Simmonds   Emile de Ruelle
| studio         = British International Pictures
| distributor    = Wardour Films
| released       = 2 July 1930
| runtime        = 79 minutes
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British drama film directed by Thomas Bentley and starring Madeleine Carroll, Frank Lawton and Sam Livesey. 

==Production== Young Woodley 1928 silent version, but the film was never released, and he re-made it in sound using some of the same actors. A school prefect becomes attracted to the headmasters wife. The film, like the play, was noted for its subversive attitude to authority. The pompous and cold headmaster is portrayed as the villain of the work.  The film was not a major success when it was released  despite its large budget and well-known subject matter.

==Cast==
* Madeleine Carroll - Laura Simmons 
* Frank Lawton - Woodley 
* Sam Livesey - Mr. Simmons 
* Gerald Rawlinson - Milner 
* Billy Milton - Vining 
* Aubrey Mather - Mr. Woodley 
* John Teed - Ainger 
* Anthony Halfpenny - Cope 
* René Ray - Kitty 
* Fanny Wright - Mother

==References==
 

==Bibliography==
* Richards, Jeffrey. The Age of the Dream Palace: Cinema and Socity in Britain, 1930-1939. Routledge & Kegan Paul, 1984.

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 