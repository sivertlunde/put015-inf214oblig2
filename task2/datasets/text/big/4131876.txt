Soul Survivors
 
{{Infobox film
 | name = Soul Survivors
 | image          = Soul survivors.jpg
 | caption        = Promotional poster Stephen Carpenter
 | producer       = Stokely Chaffin Neal H. Moritz
 | writer         = Stephen Carpenter
 | starring       = Melissa Sagemiller Casey Affleck Eliza Dushku Wes Bentley Luke Wilson The Presidents of the United States of America Matt McKenna Queens of the Stone Age Jet Set Satellite Project 86 Rakit AMEN Mygrain The Deftones Supreme Beings of Leisure Fred Murphy
 | editing        = Janice Hampton Todd C. Ramsay
 | distributor    = Artisan Entertainment
 | released       =  
 | runtime        = 84 minutes
 | country        = United States
 | language       = English
 | budget         = $17 million
 | gross          = $4,299,141
}}

Soul Survivors is a 2001 psychological thriller film starring Melissa Sagemiller as college student Cassie, whose boyfriend Sean (Casey Affleck) dies in a car accident that results from her driving after a night of partying. The accident leaves Cassie racked with guilt and emotionally vulnerable to the point that she begins hallucinating strange visions and waking-dreams, even though Cassies friends Annabel (Eliza Dushku) and Matt (Wes Bentley), as well as a local priest called Father Jude (Luke Wilson), all attempt to assist her in coping with the loss.

==Plot==
Cassie and Sean, as well as ex-boyfriend Matt and good friend Annabel, go to a club situated in an old church. There Cassie sees a man with a clear, plastic mask (Carl Paoli) and an imposing man with a scarred face (Ken Moreno). Deathmask tries to grab her on the dance floor, but she pushes him away and steps outside the club with Sean.

In the parking lot, Matt eavesdrops on their conversation. Sean confesses his love for Cassie, who claims she feels the same way. When Sean returns to the club, Matt convinces Cassie to give him a last goodbye kiss. Sean sees this, and reacts badly to it, giving Cassie the silent treatment as they drive off. Cassie, who is behind the wheel, continually looks away from the road until the car crashes. Cassies next memory is of being rushed to the hospital; Matt and Annabel are unharmed, but Sean has been killed on impact.

During the school term that follows, Cassie has several visions of Sean. She also has visions of Deathmask and Hideous Dancer in the company of Matt and Annabel. On several occasions, she believes she is being chased by the two men, although Annabel and Matt assure her that the incidents are all in her mind. After one chase, Cassie faints and is rescued by Father Jude, a young priest who is sympathetic to her fears and offers to listen if she ever needs someone to talk to.
 Rick Snyder) and asks to speak to Father Jude but is told that Father Jude died in 1981.

After a swim competition in which she has been made to participate, Cassie is chased by Deathmask. Defending herself with the tube of a fluorescent lamp, she ends up stabbing him in the stomach but when Cassie returns with Matt, they find there is no body in the pool. 
Even though she believes that Matt and Annabel are conspiring against her with Deathmask and Hideous Dancer, Cassie requests that Matt take her home to her mother. Instead he drives Cassie to the club, saying that he wishes to pick up Annabel. Cassie follows him but gets lost, eventually finding Annabel with a new lover called Raven (Angela Featherstone) who has precognitive powers. When Raven tells Cassie to "leave or die" Cassie exists the club and makes her way back to the parking lot. There Matt drunkenly insists on another goodbye forever kiss, but Cassie smashes a bottle on his head knocking him unconscious before pushing him from the car and driving away.

In a scene resembling the original accident, Cassie wrecks the car. She again comes to in the hospital. On a gurney next to her is Raven, who speaks a few words of comfort before dying. Father Jude arrives and asks if she would be willing to die in order to save Seans life. She agrees and he then asks her if she would be willing to live for him. Cassie says that she doesnt want to die.

An episode follows in which Deathmask and Hideous Dancer strangle her with her protective medallion, from which Cassie wakes to find that everything she has experienced has been a sort of coma-dream: in the original accident, Cassie and Sean had survived, while Matt and Annabel were killed. The occupants of the other car — Raven, Deathmask, and Hideous Dancer — were also fatally injured. Cassie has spent the course of the film in a half-dead, half-living state, wherein those who were killed in the accident attempt to keep her with them.  Father Jude, and Cassies visions of Sean were what brought her back to life.

==Cast==
* Melissa Sagemiller as Cassie
* Casey Affleck as Sean
* Eliza Dushku as Annabel
* Wes Bentley as Matt
* Angela Featherstone as Raven
* Luke Wilson as Father Jude
* Allen Hamilton as Dr. Haverston
* Ken Moreno as Hideous Dancer
* Carl Paoli as Deathmask
* Barbara E. Robertson as Margaret
* Richard Pickren as Ben
* Candace Kroslak as Cool Blonde
* Ryan Kitley as Young Cop Rick Snyder as Father McManus
* Danny Goldberg  as Campus Cop
* Scott Benjaminson  as Second Campus Cop
* T.J. Jagodowski  as ER Doctor
* Christine Dunford  as ER Nurse
* Amy Farrington  as ER Doctor - Midtown 
* Lily Mojekwu  as ER Nurse Midtown
* Lusia Strus  as Stern Nurse
* Michael Sassone  as Minister 
* Carrie Southworth  as Murdered Girl
* Adam Joyce  as Male Student 
* Kelli Nonnemacher  as Coed #1

==Filming==
Filming took place in Chicago and surrounding suburbs. Some scenes were also filmed in Gary, Indiana. 
*Filming started May 1999 to September 1999

== Released ==
*  September 7, 2001
*  December 13, 2001
*  January 11, 2002
*  May 10, 2002
*  20 September 2001  
*  1 November 2001  
*  8 February 2002  
*  7 June 2002  
*  14 June 2002  
*  13 July 2002 (Tokyo) 
*  9 August 2002 (video premiere) 
*  22 August 2002  
*  13 September 2002  
*  11 October 2002  
*  5 September 2003  
*  28 October 2006 (TV premiere)

== Reception==
Soul Survivors was panned by critics, with a 4% positive rating on review-aggregate site Rotten Tomatoes.

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 