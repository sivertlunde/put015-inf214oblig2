Montana (1998 film)
{{Infobox Film
| name           = Montana
| image          = MontanaDVDcover.jpg
| image_size     = 
| caption        = DVD cover
| director       = Jennifer Leitzes
| producer       = Sean Cooley Zane W. Levitt Mark Yellen
| writer         = Erich Hoeber Jon Hoeber
| narrator       = 
| starring       = Kyra Sedgwick  Philip Seymour Hoffman  Stanley Tucci Robin Tunney Robbie Coltrane 
| music          = Cliff Eidelman
| cinematography = Ken Kelsch 
| Production Designer =  Daniel Ross
| editing        = Norman Buckley
| distributor    = Initial Entertainment Group
| released       = January 16, 1998 (Sundance Film Festival)
| runtime        = 96 minutes
| country        =  
| language       = English
| budget         = $4,000,000 (estimated)
| gross          = 
| preceded_by    = 
| followed_by    = 
}} American crime directed by written by produced by Sean Cooley, Zane W. Levitt, and Mark Yellen.

Claire (Kyra Sedgwick) is a professional hit woman who has been targeted by her own organization. Her boss (Robbie Coltrane) gives her a low level task of retrieving his runaway girlfriend Kitty (Robin Tunney). Once Claire tracks down Kitty, she is unable to stop her from killing the boss incompetent son (Ethan Embry).

== Cast and characters ==
Kyra Sedgwick ... Claire Kelsky  
Stanley Tucci ... Nicholas Nick Roth  
Robin Tunney ... Kitty  
Robbie Coltrane ... The Boss  
John Ritter ... Dr. Wexler  
Ethan Embry ... Jimmy  
Philip Seymour Hoffman ... Duncan  
Mark Boone Junior ... Stykes 
Tovah Feldshuh ... Greta

==External links==
* 

 
 
 
 
 
 
 


 