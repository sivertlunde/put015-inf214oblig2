Knowing (film)
 
{{Infobox film
| name           = Knowing
| image          = Knowingposter08.jpg
| caption        = Theatrical release poster
| alt            = A picture of the Earth from space, the edge is glowing as if on fire. 
| director       = Alex Proyas
| producer       = {{Plain list |
* Alex Proyas
* Todd Black
* Jason Blumenthal
* Steve Tisch
}}
| screenplay     = {{Plain list |
* Ryne Douglas Pearson
* Juliet Snowden
* Stiles White
}}
| story          = Ryne Douglas Pearson
| starring       = {{Plain list |
* Nicolas Cage
* Rose Byrne
* Chandler Canterbury
* Lara Robinson
* Ben Mendelsohn
* Nadia Townsend
}}
| music          = Marco Beltrami
| cinematography = Simon Duggan
| editing        = Richard Learoyd
| studio         = Escape Artists  DMG Entertainment
| distributor    = Summit Entertainment
| released       =  
| runtime        = 121 minutes
| country        = United States  United Kingdom 
| language       = English
| budget         = $50&nbsp;million 
| gross          = $187.9 million 
}}
 American science science fiction turnaround and eventually picked up by Escape Artists. Production was financially backed by Summit Entertainment. Knowing was filmed in Docklands Studios Melbourne, Australia, using various locations to represent the films Boston-area setting.

The film was released on 20 March 2009, in the United States. The DVD and Blu-ray Disc|Blu-ray media were released on 7 July 2009. Knowing met with mixed reviews, with praise towards the acting performances, visual style and atmosphere, but had criticism over the implausibilities.

== Plot == Danielle Carter), in a utility closet scratching numbers into the door with her fingernails bleeding.

In 2009, Caleb Koestler (Chandler Canterbury) is a pupil at the same elementary school. When the time capsule is opened, Caleb is supposed to read and write about some of the capsules contents. Hes given the page of numbers written by Lucinda. His widowed father John (Nicolas Cage), a professor of astrophysics at Massachusetts Institute of Technology|MIT, notices the numbers have a specific set of sequences referring to the times, number of deaths, and locations of fatal disasters over the last 50 years, including 911012996 (the date and death toll of the 9/11 attacks). The last three sets of digits on the page are dated in the immediate future.

In the following days, a car drives by the family home with two strangers. They give Caleb a small smooth stone. Caleb later dreams of one of the strange men, who points to the window showing the world on fire with burning animals running out from a forest.

 
 Extinction Level Event that no one will escape.

Outside, more strangers walk up to the children waiting in the car. John drives them away only to have Abby say that the "whisper people" want her and Caleb to go with them.
 MIT observatory, where he discovers that a massive solar flare will soon reach Earth, making it uninhabitable. Diana wants to hide in some caves. Jonathan reluctantly agrees at first and continues to investigate Lucindas numbers. Frustrated by his delay, Diana decides to take the children to the caves without him.
 broadsided by a truck. John, rushing to catch up with them, arrives just as Diana dies at exactly midnight of October 19 and finds the smooth, black stone in Dianas hand. He goes back to Lucindas mobile home, finding the children and the strangers waiting in a dry river bed covered with the similar black stones. A space ship descends from the sky. John is refused entry but allows his son to leave with the strangers, who are revealed to be otherworldly beings, perhaps angels. The ship departs with the children and a pair of rabbits, and a distant shot shows many similar vehicles leaving Earth.

The next morning the skies are on fire from the solar flare. John drives calmly while listening to classical music through the chaos within the streets of Boston, arriving at his estranged fathers home. Together they embrace as the solar flare burns away the atmosphere. A wall of fire incinerates Boston and Manhattan and burns away the entire surface of the Earth, destroying all life on the planet.
 Tree of Life.

== Cast ==
 
* Nicolas Cage as Professor Jonathan "John" Koestler
* Rose Byrne as Diana Wayland / Lucinda Embry-Wayland (photograph)
* Chandler Canterbury as Caleb Koestler
** Joshua Long as Young Caleb
* Lara Robinson as Young Lucinda Embry / Abby Wayland / Young Diana Wayland (photograph)
* Nadia Townsend as Grace Koestler
* Ben Mendelsohn as Professor Phil Beckman
* Alan Hopgood as Reverend Koestler
* Benita Collings as Mrs. Koestler
* Adrienne Pickering as Allison Koestler
* Liam Hemsworth as Spencer
* Ra Chapman as Jessica
* Lesley Anne Mitchell as Stacy
* Gareth Yuen as Donald
* Verity Charlton as Kim
* Tamara Donnellan as Mrs. Embry
* Travis Waite as Mr. Embry
* D.G. Maloney, Joel Bow, Maximillian Paul, and Karen Hadfield as The Strangers
* David Lennie as Principal Clark in 1959
* Carolyn Shakespeare-Allen as Principal in 2009 
* Alethea McGrath as Priscilla Taylor in 2009 Danielle Carter as Priscilla Taylor in 1959
 

== Production == Richard Kelly The Exorcist in melding "realism with a fantastical premise". 
 Lexington with Cambridge and Mount Macedon Collins Street. one take in the film, was done in a nearly-finished freeway outside Melbourne, mixing practical effects and pieces of a plane with computer-generated elements. The scenographic rain led to the usage of a new gel for the flames so the fire would not be put out, and semi-permanent make-up to make them last the long shooting hours.  The solar flare destruction sequence is set in New York City, showing notable landmarks such as the Metlife Building, Times Square and the Empire State Building being obliterated. 
 Red One 4K digital camera, making the film the first time the director used digital cameras.  He sought to capture a gritty and realistic look to the film, and his approach involved a continuous two-minute scene in which Cages character sees a plane crash and attempts to rescue passengers. The scene was an arduous task, taking two days to set up and two days to shoot. Proyas explained the goal, "I did that specifically to not let the artifice of visual effects and all the cuts and stuff we can do, get in the way of the emotion of the scene." 

== Soundtrack == Symphony No. 7 (Beethoven) - Allegretto,  which is played without any accompanying sound effects in the final Boston disaster scene of the film.  Beltrami released the soundtrack as a CD with 22 tracks. 
{{Infobox album
| Name = Knowing: Original Motion Picture Soundtrack
| Type = Soundtrack
| Genre = Film score
| Artist = Marco Beltrami
| Cover = 
| Released =  
| Length = 65:39
| Label = Varèse Sarabande
}}
 {{Track listing
| collapsed       = no
| title1          = Main Titles
| length1         = 2:11
| title2          = Door Jam
| length2         = 3:10
| title3          = EMT
| length3         = 2:19
| title4          = John and Caleb
| length4         = 1:59
| title5          = New York
| length5         = 4:12
| title6          = Aftermath
| length6         = 1:46
| title7          = Not a Kid Anymore
| length7         = 1:56
| title8          = Moose on the Loose
| length8         = 2:20
| title9          = Stalking the Waylands
| length9         = 1:24
| title10         = Numerology 
| length10        = 3:06
| title11         = Its the Sun 
| length11        = 2:44
| title12         = John Spills
| length12        = 3:26
| title13         = Trailer Music 
| length13        = 3:21
| title14         = 33 
| length14        = 3:29
| title15         = Loudmouth
| length15        = 2:43
| title16         = Revelations
| length16        = 3:29
| title17         = Thataway!
| length17        = 2:06
| title18         = Shock and Aww
| length18        = 4:00
| title19         = Caleb Leaves
| length19        = 7:09
| title20         = Roll Over Beethoven
| length20        = 4:15
| title21         = New World Round
| length21        = 2:59
| title22         = Who Wants an Apple?
| length22        = 1:35
| total_length    = 65:39 
}} 

; Music in the film but not released on the soundtrack
* " 
* "News Theme" - written and performed by Guy Gross Beethoven Symphony No. 7 in A Major, Op. 92, in (1811-1812)" - composed by Ludwig van Beethoven and performed by Sydney Scoring Orchestra

== Reception ==
===Critical response===<!--
Please update the "accessdate=" fields for the Rotten Tomatoes and Metacritic citations when you update the numbers.

-->
Knowing received mixed reviews.   gave the film an average score of 41 out of 100 based on 27 reviews. 

A. O. Scott of The New York Times gave the film a negative review and wrote, "If your intention is to make a brooding, hauntingly allegorical terror-thriller, its probably not a good sign when spectacles of mass death and intimations of planetary destruction are met with hoots and giggles ... The draggy, lurching two hours of "Knowing" will make you long for the end of the world, even as you worry that there will not be time for all your questions to be answered."  In the San Francisco Chronicle, Peter Hartlaub called the film "an excitement for fans of Proyas" and "a surprisingly messy effort." He thought Nicolas Cage "borders on ridiculous here, in part because of a script that gives him little to do but freak out or act depressed". 

Writing for The Washington Post, Michael OSullivan thought the film was "creepy, at least for the first two-thirds or so, in a moderately satisfying, if predictable, way ... But the narrative corner into which this movie... paints itself is a simultaneously brilliant and exciting one. Well before the film neared its by turns dismal and ditzy conclusion, I found myself knowing—yet hardly able to believe—what was about to happen."  Betsy Sharkey of the Los Angeles Times found it to be "moody and sometimes ideologically provocative" and added, "Knowing has its grim moments—and by that I mean the sort of cringe- (or laugh-) inducing lines of dialogue that have haunted disaster films through the ages ... So visually arresting are the images that watching a deconstructing airliner or subway train becomes more mesmerising than horrifying." 

Roger Ebert of the Chicago Sun-Times was enthusiastic, rating it four stars out of four and writing, "Knowing is among the best science-fiction films Ive seen—frightening, suspenseful, intelligent and, when it needs to be, rather awesome"    He continued, "With expert and confident storytelling, Proyas strings together events that keep tension at a high pitch all through the film. Even a few quiet, human moments have something coiling beneath. Pluck this movie, and it vibrates."  Ebert later listed it as the sixth best film of 2009.
 Gaia and her stellar opposite numbers sock it to an unconcerned mankind." 

=== Box office ===
Knowing was released in 3,332 theatres in the United States and Canada on 20 March 2009 and grossed $24,604,751 in its opening weekend,    placing first at the box office.  According to exit polling, 63% of the audience was 25 years old and up and evenly split between genders.  On the weekend of 17 March 2009, Knowing ranked first in the international box office, grossing $9.8&nbsp;million at 1,711 theatres in ten markets, including first with $3.55&nbsp;million in the United Kingdom.  As of 26 July 2009, the film had grossed $79,957,634 in the United States and Canada and $107,901,008 in other territories for a worldwide total of $187,858,642.   

=== Home media release  ===
Knowing was released on DVD on 7 July 2009 opening at No.1 for the week, selling 773,000 DVD units for $12,508,192 in revenue. As per the latest figures, 1,521,797 DVD units have been sold, bringing in $22,968,367 in revenue. This does not include DVD/Blu-rentals or Blu-ray sales. 

=== Litigation ===
On 25 November 2009, Global Findability filed a patent infringement lawsuit against Summit Entertainment and Escape Artists in the U.S. District Court for the District of Columbia, claiming that a geospatial entity object code was used in the film Knowing which infringed U.S. Patent No. 7,107,286 entitled "Integrated Spatial Information Processing System for Geospatial Positioning".    The case was dismissed on 10 January 2011. 

=== Science controversy ===
Regarding the films grounding in science, Director Alex Proyas said at a press conference, "The science was important. I wanted to make the movie credible. So of course we researched as much as we could and tried to give it as much authenticity as we could." Erin McCarthy   Popular Mechanics, Retrieved 5 January 2012 

Ian ONeill of Discovery News criticized the films solar flare plot line, pointing out that the most powerful solar flares could never incinerate Earthly cities. 

Erin McCarthy of Popular Mechanics calls attention to the films confusion of numerology, the occults study of how numbers like dates of birth influence human affairs, with the ability of science to describe the world mathematically to make predictions about things like weather or create technology like cell phones. 

Steve Biodrowski of Cinefantastique refers to the films approach as disappointingly "pseudo-scientific." He writes, "Cage plays an astronomer, and his discussions with a colleague hint that the film may actually grapple with the question of predicting the future, perhaps even offer a plausible theory. Unfortunately, this approach is abandoned as Koestler pursues the disasters, and the film eventually moves into a mystical approach." 

Asked about his research for the role, Nicolas Cage stated, "I grew up with a professor, so that was all the research I ever needed." His father, August Coppola, was a professor of comparative literature at Cal State Long Beach.  Cage plays an astrophysicist at MIT in the film.

== See also ==
* 2012 (film)
* 20th Century Boys

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 