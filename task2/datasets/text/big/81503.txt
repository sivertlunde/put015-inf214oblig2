Indiana Jones and the Temple of Doom
 
 
{{Infobox film
| name           = Indiana Jones and the Temple of Doom
| image          = Indiana Jones and the Temple of Doom PosterB.jpg
| border         = yes
| alt            =
| caption        = Theatrical release poster by Drew Struzan
| director       = Steven Spielberg
| producer       = Robert Watts
| screenplay     = Willard Huyck Gloria Katz
| story          = George Lucas
| starring = {{Plainlist|
* Harrison Ford
* Kate Capshaw
* Amrish Puri
* Roshan Seth
* Philip Stone Ke Huy Quan
}}
| music          = John Williams
| cinematography = Douglas Slocombe Michael Kahn
| studio         = Lucasfilm
| distributor    = Paramount Pictures
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $28.17 million 
| gross          = $333.1 million
}}
Indiana Jones and the Temple of Doom is a 1984 American adventure film directed by Steven Spielberg. It is the second installment in the Indiana Jones (franchise)|Indiana Jones franchise and a  prequel to the 1981 film Raiders of the Lost Ark. After arriving in North India, Indiana Jones is asked by a desperate village to find a mystical stone. He agrees, stumbling upon a Thuggee cult practicing child slavery, black magic, and ritual human sacrifice in honor of their goddess Kali. 
 Nazis to Gunga Din. 

The film was released to financial success but mixed reviews, which criticized its violence, later contributing to the creation of the Motion Picture Association of America film rating system|PG-13 rating.  However, critical opinion has improved since 1984, citing the films intensity and imagination. Some of the films cast and crew, including Spielberg, retrospectively view the film in a negative light, partly due to the film being the most overtly violent Indiana Jones film.  The film has also been the subject of controversy due to its negative portrayal of India and Hinduism.         
==Plot== Lao Che, Short Round Willie Scott inflatable raft raging river. Hindu god Shiva and enlist their help to retrieve the sacred Sivalinga stone stolen from their shrine, as well as the communitys children, from evil forces in the nearby Pankot Palace. During the journey to Pankot, Indy hypothesizes that the stone may be one of the five fabled Sankara stones that promise fortune and glory.
 Zalim Singh. Chattar Lal rebuffs Indys questions about the villagers claims and his theory that the ancient Thuggee cult is responsible for their troubles. Later that night, Indy is attacked by an assassin, leading Indy, Willie, and Shorty to believe that something is amiss. They discover a series of tunnels hidden behind a statue in Willies room and set out to explore them, overcoming a number of booby-traps along the way.
 Mola Ram, enslaved the children to mine for the final two stones, which they hope will allow them to rule the world. As Indy tries to retrieve the stones, he, Willie, and Shorty are captured and separated. Indy is whipped and forced to drink a potion called the "Blood of Kali", which places him in a trance-like state where he begins to mindlessly serve the Thuggee. Willie, meanwhile, is kept as a human sacrifice, while Shorty is put to work in the mines alongside the enslaved children. Shorty breaks free and escapes back into the temple where he burns Indy with a torch, shocking him out of the trance. After defeating Chattar Lal, also a Thuggee worshiper, they go back to the mines to free the children, but Indy is caught up in a fight with a hulking overseer. The Maharajah, who was also forcibly entranced by the "Blood of Kali," attempts to cripple Indy with a voodoo doll. Shorty spars with the Maharajah, ultimately burning him to snap him out of the trance. With his strength returned, Indy kills the overseer. The Maharajah then tells Shorty how to get out of the mines. While Mola Ram escapes, Indy and Shorty rescue Willie and retrieve the three Sankara stones, the village children escape.
 archers are killed and the remainder are surrounded and captured. Indy, Willie, and Shorty return victoriously to the village with the children and give the missing stone back to the villagers.

==Cast==
 
*  adventurer who is asked by a desperate Indian village to retrieve a mysterious stone. Ford undertook a strict physical exercise regimen headed by Jake Steinfeld to gain a more muscular tone for the part.  The African Anthony Powell wanted the character to have red hair.  a 17th-century Tom Smith based the skull on a cow (as this would be sacrilegious), and used a latex shrunken head.   
*  in Los Angeles.  Around 6000 actors auditioned worldwide for the part: Quan was cast after his brother auditioned for the role. Spielberg liked his personality, so he and Ford improvised the scene where Short Round accuses Indy of cheating during a card game.  He was credited by his birthname, Ke Huy Quan.
*Roshan Seth as Chattar Lal: The Prime Minister of the Maharaja of Pankot. Chattar, also a Thuggee worshiper, is enchanted by Indy, Willie and Short Rounds arrival, but is offended by Indys questioning of the palaces history and the archaeologists own dubious past. Captain called to Pankot Palace for "exercises". Alongside a unit of his riflemen, Blumburtt assists Indy towards the end in fighting off Thuggee reinforcements. David Niven was attached to the role but died before filming began.
*  who, with his sons, hires Indy to recover the cremated ashes of one of his ancestors, only to attempt to kill him and cheat him out of his fee, a large diamond.
*David Yip as Wu Han: A friend of Indy. He is killed by one of Lao Ches sons while posing as a waiter at Club Obi Wan. Raj Singh as Zalim Singh: The adolescent Maharajá of Pankot, who appears as an innocent puppet of the Thuggee faithful. In the end, he helps to defeat them.
*  lingam stone

Actor Pat Roach plays the Thuggee overseer in the mines. Steven Spielberg, George Lucas, Frank Marshall, Kathleen Kennedy, and Dan Aykroyd have cameos at the airport. 

==Production==

===Development===
When    Spielberg and Lucas attributed the films tone, which was darker than Raiders of the Lost Ark, to their personal moods following the breakups of their relationships (George with his wife, and Spielberg with his girlfriend).  In addition, Lucas felt  "it had to have been a dark film. The way   was the dark second act of the Star Wars trilogy." 
 Lost World pastiche with a hidden valley inhabited by dinosaurs". Chinese authorities refused to allow filming,    and Lucas considered the Monkey King as the plot device.    Lucas wrote a film treatment that included a haunted castle in Scotland, but Spielberg felt it was too similar to Poltergeist (1982 film)|Poltergeist. The haunted castle in Scotland slowly transformed into a demonic temple in India. 
 religious cult Indian culture. Gunga Din served as an influence for the film. 

Huyck and Katz spent four days at Skywalker Ranch for story discussions with Lucas and Spielberg in early-1982.  They later said the early plot consisted of two notions of Lucas: that Indy would recover something stolen from a village and decide whether to give it back, and that the picture would start in China and work its way to India. Huyck says Lucas was very single-minded about getting through meetings, while "Steve would always stop and think about visual stuff."   accessed 22 April 2015 
 Cocker Spaniel, and Short Round was named after Huycks dog, whose name was derived from The Steel Helmet.  

Lucas handed Huyck and Katz a 20-page treatment in May 1982 titled Indiana Jones and the Temple of Death to adapt into a screenplay.  Scenes such as the fight scene in Shanghai, escape from the airplane and the mine cart chase came from original scripts of Raiders of the Lost Ark.    accessed 23 April 2015 

Lucas, Huyck and Katz had been developing Radioland Murders (1994) since the early 1970s. The opening music was taken from that script and applied to Temple of Doom.  Spielberg reflected, "Georges idea was to start the movie with a musical number. He wanted to do a Busby Berkeley dance number. At all our story meetings he would say, Hey, Steven, you always said you wanted to shoot musicals. I thought, Yeah, that could be fun."  

Lucas, Spielberg, Katz and Huyck were concerned how to keep the audience interest while explaining the Thugee cult. Huyck and Katz proposed a tiger hunt but Spielberg said, "Theres no way Im going to stay in India long enough to shoot a tiger hunt." They eventually decided on a dinner scene involving eating bugs, monkey brains and the like. "Steve and George both still react like children, so their idea was to make it as gross as possible," says Katz.  

Lucas sent Huyck and Katz a 500-page transcript of their taped conversations to help them with the script.  The first draft was written in six weeks, in early-August 1982. "Steve was coming off an enormously successful movie and George didnt want to lose him," said Katz. "He desperately wanted him to direct (Temple of Doom). We were under a lot of pressure to do it really, really fast so we could hold on to Steve." 

A second draft was finished by September. Captain Blumburtt, Chattar Lal and the boy Maharaja originally had more crucial roles. A dogfight was deleted, as well as those who drank the Kali blood turned into zombies with physical superhuman abilities. During pre-production, the Temple of Death title was replaced with Temple of Doom. From March—April 1983, Huyck and Katz simultaneously performed rewrites for a final shooting script. 

Huyck and Katz later said Harrison Ford took many of the one liners originally given to Short Round. 
===Filming===
  and Chandran Rutnam on a location in Sri Lanka during the filming of Indiana Jones and the Temple of Doom.]]
Huyck later recalled "at one point when we were writing it we told George “We know a lot of Indians. Weve been there... I dont think theyre going to think this is really so cool. Do you think youre going to have trouble shooting there?” He said, “Are you kidding? Its me and Steve." Months later they called and said, “We cant shoot in India. Theyre really upset.” So they shot in Sri Lanka and London, mostly.” 
 Frank Marshall biographer Marcus Hearn observed, "Douglas Slocombes skillful lighting helped disguise the fact that about 80 percent of the film was shot with sound stages."   
  on the set of Indiana Jones and the Temple of Doom which was shot in Kandy, Sri Lanka in 1983.]] Anything Goes". Mandarin and Anthony Powell who had to fill in the insurance forms. As to the reason for damage, he had no option but to put "dress eaten by elephant". 
 Centinela Hospital on June 21 for recovery.  Stunt double Vic Armstrong spent five weeks as a stand-in for various shots. Wendy Leach, Armstrongs wife, served as Capshaws stunt double. 
 doubling for Disneyland Park in Anaheim for the mine cart scene. 

===Editing=== matte shots to slow it down. We made it a little bit slower, by putting breathing room back in so thered be a two-hour oxygen supply for the audience." 

==Release==
Indiana Jones and the Temple of Doom was released on May 23, 1984 in America, accumulating a record-breaking $US45.7 million in its first week.  The film went on to gross $333.11 million worldwide, with $180 million in North America and the equivalent of $153.11 million in other markets.    Temple of Doom had the highest opening weekend of 1984, and was that years third highest grossing film in North America, behind Beverly Hills Cop and Ghostbusters.  It was also the tenth highest grossing film of all time during its release. 
 Brian Garvey, Marvel Super Special #30  and as a three-issue limited series. 

LucasArts and Atari Games promoted the film by releasing an arcade game. Hasbro released a toy line based on the film in September 2008. 

==Reception== American Movie movie serials didnt have shape. They just went on and on and on, which is what Temple of Doom does with humor and technical invention."  Neal Gabler commented that "I think in some ways, Indiana Jones and the Temple of Doom was better than Raiders of the Lost Ark. In some ways it was less. In sum total, Id have to say I enjoyed it more. That doesnt mean its better necessarily, but I got more enjoyment out of it."  Colin Covert of the Star Tribune called the film "sillier, darkly violent and a bit dumbed down, but still great fun."  Pauline Kael, writing in The New Yorker, claimed it was "one of the most sheerly pleasurable physical comedies ever made." Halliwells Film Guide, 13th edition&nbsp;– ISBN 0-00-638868-X.  Halliwells Film Guide described the film as a "slow-starting adventure romp with much ingenuity and too much brutality and horror." 

Dave Kehr gave a largely negative review; "The film betrays no human impulse higher than that of a ten-year-old boy trying to gross out his baby sister by dangling a dead worm in her face."  Ralph Novak of People (magazine)|People complained "The ads that say this film may be too intense for younger children are fraudulent. No parent should allow a young child to see this traumatizing movie; it would be a cinematic form of child abuse. Even Harrison Ford is required to slap Quan and abuse Capshaw. There are no heroes connected with the film, only two villains; their names are Steven Spielberg and George Lucas."  The Observer described it as "a thin, arch, graceless affair."  The Guardian summarized it as "a two-hour series of none too carefully linked chase sequences ... sitting on the edge of your seat gives you a sore bum but also a numb brain."  Leonard Maltin gave the movie only 2 out of 4 stars, saying that the film "never gives us a chance to breathe" and chiding the "gross-out gags." 

Kate Capshaw called her character "not much more than a dumb screaming blonde."  Steven Spielberg said in 1989, "I wasnt happy with Temple of Doom at all. It was too dark, too subterranean, and much too horrific. I thought it out-poltered Poltergeist (1982 film)|Poltergeist. Theres not an ounce of my own personal feeling in Temple of Doom." He later added during the Making of Indiana Jones and the Temple of Doom documentary, "Temple of Doom is my least favorite of the trilogy. I look back and I say, Well the greatest thing that I got out of that was I met Kate Capshaw. We married years later and that to me was the reason I was fated to make Temple of Doom." 
 monkey brains." 
 Time Out polled several film critics, directors, actors and stunt actors to list their top action films.  Indiana Jones and the Temple of Doom was listed at 71st place on this list. 

===Awards===
 Soundtrack composer Original Music Michael Kahn, Anthony Powell makeup designer Tom Smith Best Fantasy Film but lost to Ghostbusters. 

==References==
 

==Further reading==
* 
* 
*  
* 

==External links==
 
 
* 
*   at  
* 
* 
* 
* 

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 