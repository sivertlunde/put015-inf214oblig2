The Beverly Hillbillies (film)
{{Infobox film
| name           = The Beverly Hillbillies
| image          = beverly hillbillies.jpg
| caption        = Theatrical release poster
| director       = Penelope Spheeris
| producer       = Ian Bryce Penelope Spheeris
| writer         = Paul Henning (television series) Lawrence Konner (story and screenplay) Mark Rosenthal (story and screenplay) Jim Fisher (screenplay) Jim Staahl (screenplay)
| starring       = Jim Varney Diedrich Bader Erika Eleniak Cloris Leachman Lily Tomlin Lea Thompson Rob Schneider
| music          = Lalo Schifrin
| cinematography = Robert Brinkmann
| editing        = Ross Albert
| distributor    = 20th Century Fox
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $25 million
| gross          = $57,405,220
}}

The Beverly Hillbillies is a 1993  ), Dolly Parton, and Zsa Zsa Gabor. The movie was directed by Penelope Spheeris. 

The film follows a poor hillbilly named Jed Clampett (Jim Varney), who becomes a billionaire when he accidentally discovers crude oil after missing his target while hunting. 

==Plot synopsis==
Jed Clampett (Jim Varney), a hillsman of humble station, accidentally discovers oil on his land while shooting at a rabbit. Ozark Mountain Oil, interested in purchasing his land, offers him $1 billion for the property. Unsure of what to do, Jed consults his sister, Pearl Bodine (Linda Carlson), during a family dinner.  Pearl suggests a change of scenery for Jeds daughter, Elly May (Erika Eleniak), would be a good thing.  Having made up his mind and accepted the money, Jed, his daughter, his mother-in-law Granny (Cloris Leachman), and his nephew, Jethro (Diedrich Bader), Pearls son, load up Jethros old, dilapidated truck with their possessions and move to Beverly Hills, California.
 Beverly Hills Police after the Clampetts arrive, mistaking them for burglars. Upon learning of Miss Janes mistake at the police station, Mr. Drysdale briefly fires her. But seeing that Jed insists that he still wants her to watch over his affairs, Mr. Drysdale rehires her.
 Kevin Connolly), into befriending Elly May, to whom he eventually develops an attraction.  Miss Jane is also smitten by Jethro, who seems ignorant of her affections.

Jed requests Miss Janes assistance in helping him search for someone who will help change Elly May into a woman. Woodrow Tyler (Rob Schneider), an employee in Drysdales bank, catches wind of this and contrives a scheme with his con artist girlfriend, Laura Jackson (Lea Thompson), to steal Jeds money by having her marry Jed.  She poses as a French etiquette teacher, Laurette Voleur, and asks for work. "Laurette" feigns romantic interest in Jed, which eventually causes him to propose marriage to her.

Shortly before the wedding, Granny hears Laura and Tyler talking about the scam. Granny reveals herself to the pair and threatens to expose their scam to Jed, and then "the weddin will be off!!"  But before she can, they capture her, restrain her, and have her institutionalized at the Los Viejos Nursing Home so she cannot contact Jed.
 Swiss account on his laptop computer when the couple says "I do". Miss Jane, realizing Granny is missing, goes to the office of Barnaby Jones (Buddy Ebsen), and, after learning where Granny is, poses as a nurse and breaks her out. Granny and Miss Jane arrive at the wedding and foil Laura and Tylers plan when Miss Jane grabs a shotgun and blows the laptop to bits. The police arrest Laura and Tyler while Jed decides that, since the wedding was off, they would have "one hellacious shindig."

==Cast==
*Jim Varney - Jedidiah D. Jed Clampett 
*Diedrich Bader - Jethro Bodine/Jethrine Bodine
*Erika Eleniak - Elly May Clampett
*Cloris Leachman - Daisy May "Granny" Moses
*Dabney Coleman - Mr. Milburn Drysdale
*Lily Tomlin - Miss Jane Hathaway
*Lea Thompson - Laura Jackson "Laurette Voleur"
*Rob Schneider - Woodrow Tyler
*Penny Fuller - Margaret Drysdale Kevin Connolly - Morgan Drysdale
*Linda Carlson - Aunt Pearl Bodine
*Buddy Ebsen - Barnaby Jones
*Leann Hunley - Miss Arlington Robert Easton - Mayor Jasper
*Dolly Parton - Herself
*Zsa Zsa Gabor - Herself

The Dolly Parton "band" was composed of members of Rhino Bucket (who had a contributing song on the soundtrack of the 1992 Penelope Spheeris hit movie Waynes World (film)|Waynes World), The Dwight Yoakam Band (Skip Edwards), and Vern Monnett (Randy Meisner, Texas Tornados and Gary Allan).

==Box office==
In its first weekend, The Beverly Hillbillies grossed $9,525,375 at the box office.  The movie moved up to number one two weeks later.  The total worldwide gross was $57,405,220, making it a moderate box office success. 

==Reception==
The film received mostly negative reviews, with a 24%  "rotten" rating on Rotten Tomatoes, and a 4.4 on IMDb.  Roger Ebert said of the film, "When directors make a wonderful movie, you look forward to their next one with a special anticipation, thinking maybe theyve got the secret. If it turns out they dont, you feel almost betrayed. Thats how I felt after The Beverly Hillbillies, one of the worst movies of this or any year." 

==References==
 

==External links==
* 
* 
*  at Rotten Tomatoes
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 