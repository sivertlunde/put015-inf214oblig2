Dil Apna Punjabi
{{Infobox film
| name        = Dil Apna Punjabi
| image       = Manmohan Singh
| writer      = Harbhajan Mann Kanwaljit Singh
| producer    = Kumar S. Taurani Ramesh S. Taurani
| music       = Sukshinder Shinda Tips
| released    =  
| country     = India Punjab
| language    = Punjabi
}}
 Tips It Manmohan Singh.

== Synopsis ==
Set in the lively village of modern day Punjab (India)|Punjab, "Dil Apna Punjabi", is about a family spanning over four generations all living under one roof headed by Sardar Hardam Singh (Dara Singh).

His grandson, Kanwal (Harbhajan Mann) is a man of his heart, who spends most of his time with his friends; a village musical troupe. When Kanwal when meets college friend Ladi (Neeru Bajwa) at relative Faujans (Amar Noorie) home he falls in love. Faujan makes their love match seem as an arranged marriage to their respective families. However, Ladis family meet him, they are discouraged due to his unambitious approach and his lack of employment.

When a talent scout (Gurpreet Ghuggi) hears him singing, Kanwal decides to make a success of himself in the UK to prove himself. Here he meets TV host Lisa (Mahek Chahal). Lisa is drawn towards Kanwals charm and simplicity soon begins to have feelings for Kanwal.
 Punjab to be with his first love Ladi.

==Cast==
*Harbhajan Mann ...Kanwal
*Neeru Bajwa ...Ladi
*Mahek Chahal ...Lisa
*Gurpreet Ghuggi
*Dara Singh ... S. Hardam Singh Kanwaljit Singh
*Deep Dhillon

== Music == bhangra and Punjabi hip hop, and also includes Apache Indian. The songs of the movie are sung by Harbhajan Mann, Alka Yagnik and Sunidhi Chauhan.

== Locations == Punjab & various locations like Tower Bridge, St. Pauls Cathedral, Windsor Castle, Battersea Park, Margate Beach, Caversham Bridge in London, United Kingdom|UK.

== Review ==
The movie was a hit in Punjab. However, it was not a blockbuster yet as Jee Aayan Nu was. Regional Newspaper Ajit has given it nine out of ten stars. The BBC awarded two out of five stars. 

== External links ==
*  
*  
*  
*  
*   BBC Movie Review

 
 
 


 