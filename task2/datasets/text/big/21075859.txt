The Wedding Game
{{Infobox Film
| name = The Wedding Game
| director = Ekachai Uekrongtham Double Vision(Malaysia) Speedy Productions (Malaysia) Scorpio East Pictures (Singapore) 
| writer = Dennis Chew Desmond Sim Ekachai Uekrongtham Christopher Lee
| music = Hagen Troy
| cinematography =  
| editing =  
| distributor = Golden Village Pictures
| released =  
| runtime = 103 minutes
| country = Singapore Malaysia
| language = Mandarin
| budget = $1.5 million
| gross = 
}} Double Vision Christopher Lee.The film centers on actors Jack Fang (Christopher) and Vikki Tse (Fann) who orchestrate a faux engagement to boost their showbiz careers.

==Plot== Christopher Lee) publicly proposes to Vikki Tse (Fann Wong) during the live telecast of a regional awards show. Surprised but happy, Vikki accepts. What the public doesnt know is that the entire love affair of these two famous celebrities, Jack and Vikki, is an elaborate and meticulously planned ruse designed by their ambitious managers, May (Alice Lau) and Tom (Charles Chan), to trick the public into believing that they are getting married. In reality, Jack has disliked Vikki from the first day they met and vice versa. Yet for fame and money from endorsements, these rival celebrities keep up with their “fake” marriage to increase their popularity.  Just when everything starts going well, an incident rattles some of the fans and the media. There is now lingering doubt about the authenticity of this love match. They fall in love. 

==Production==
The film was written by Dennis Chew, Desmond Sim and Ekachai Uekrongtham,  and directed by Ekachai Uekrongtham, and was shot in Singapore and Malaysia, Malacca. The working title for the film was originally The Wedding of the Year, but was officially changed to The Wedding Game.

The Wedding Game marks Singapore Sports Council and production company Raintree Pictures first silver screen collaboration. As this is the first time Team Singapore athletes are featured in a movie

Filming took less than a month and occurred in late 2008.

==Main cast==
{| class="wikitable" border="1"
|-
! Starring
! 领衔主演
! As
! 飾
|-
| Fann Wong
| 范文芳
| Vikki
| 谢文琪
|-alicia foo  Christopher Lee
| 李铭顺
| Jack
| 方子杰
|-jeanette aw
| Clares Chan
| 陳建州(黑人)
| Tom
| 汤哥
|-qi yuwu
| Alice Lau
| 刘雅丽
| May
| 阿美
|}

Co-Starring
* Daniel Tan 陈家风
* Lai Ming 黎明
* Saiful Apek
* Chin Chi Kang 钱自刚
 
Special Appearance by Team Singapore
* Jazreel Tan
* Jing Jun Hong
* Kendrick Lee
* Tao Li

==Music== Hagen Tan collaborating with veteran musician Dr Ng King Kang for the production, soundtrack producer and music arranger are filled in by local talent too.
 
Original Soundtrack Hagen Tan 陈孟奇 & Christy Yow 姚惠敏) Hagen Tan 陈孟奇)
* 3. 习惯 (吴庆康 & 陈丽伊　Katherine Tan) Hagen Tan 陈孟奇)
* 5. 爱反复 (吉他伴奏版)
* 6. 我们的爱 (钢琴伴奏版) Hagen Tan 陈孟奇 Demo 版)

==Release==
Mediacorp Raintree Pictures held the Singapore premiere of The Wedding Game on January 23, 2009 at the Golden Village Cinemas in both Great World City and VivoCity at 7.30pm and 9.00pm respectively. 
Its Malaysia premiere was held on January 22, 2009 at The Pavilion. 
Island-wide screening in Singapore will commence on January 25, 2009.

==Box office performance==
The Wedding Game has raked in S$1.25 million at the Singapore box office since it opened in Singapore on January 25 despite strong competition from other titles. It has also done well in Malaysia collecting an impressive RM$2 million as of February 1. 

==Critical reception==
Rating of this movie was publicly discussed as The Wedding Game were argued to be too erotic or obscene and are unsuitable for the young  were rated as PG (Parental-Guidance) while its main local competitor Love Matters were rated NC-16 by the censorship board. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 