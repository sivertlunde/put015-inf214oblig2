City Slickers II: The Legend of Curly's Gold
{{Infobox film
| title          = City Slickers II:  The Legend of Curlys Gold
| image          = Legend of curlys gold ver2.jpg
| caption        = Theatrical release poster
| director       = Paul Weiland
| producer       = Billy Crystal
| writer         = Lowell Ganz Babaloo Mandel  (characters and screenplay)  Billy Crystal Daniel Stern Jon Lovitz Jack Palance Patricia Wettig
| music          = Marc Shaiman
| cinematography = Adrian Biddle
| editing        = William M. Anderson Armen Minasian
| studio         = Castle Rock Entertainment
| distributor    = Columbia Pictures
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $40 million 
| gross          = $43,622,150
}} Daniel Stern.

Although a mild financial success, the film did not reach the popularity of the first, receiving a generally negative response (a 19% approval rating on Rotten Tomatoes). It was nominated for a Razzie Award for Worst Remake or Sequel. 

==Plot== Daniel Stern). However, he is being plagued with nightmares about his deceased friend, Curly, and comes to believe that he may still be alive. On his 40th birthday, Mitch sees a man resembling Curly on the train, which does nothing to placate his worries, and later finds a treasure map belonging to Lincoln Washburn hidden in Curlys old hat, albeit with a missing corner. Mitch and Phil investigate the contents of the map in the public library and learn that Lincoln Washburn, Curlys father, was a train robber in the Old West and in 1908 infamously stole and hid one million dollars in gold bullion in the deserts near Las Vegas. With an impending trip to Las Vegas for a convention, Mitch decides to venture out to find the gold (which would now be worth twenty million) along with Phil and his estranged younger brother, Glen (Jon Lovitz).

Several mishaps ensue, such as Glen accidentally burning a hole in the map, Mitch almost falling off a cliff while retrieving it and Phil believing he was bitten by a snake while he actually sat on a cactus. They are ambushed by the two cowboys who they bought their supplies from, who demand the map, since Phil recklessly told them all about the gold. Just as the cowboys are poised to kill them, a man appears and fights them off. He introduces himself as Duke (Jack Palance), Curlys identical twin brother, and explains that long ago, his father had plans to find the gold with his sons once he was no longer being monitored, but he died before. On their mothers deathbed, their mother gave Curly the map, and Curly contacted Duke to find him so that they could find the gold together, but he died on the cattle drive. Duke learned from Cookie that Mitch had Curlys belongings, and so sought him out, though Mitch believed he was Curly. Though Duke is prepared to take the map and find the gold by himself, Mitch chastises him for his attitude, reasoning that Curly would not approve. Out of respect for his brother, Duke relents and allows the others to accompany him and share the gold.

A reckless act by Mitch causes a stampede in which the map and almost all their supplies are lost. Thanks to Glens memory, they are able to press on and find the location of the cave where the gold is hidden. They eventually find the gold, but are confronted by two armed cowboys also seeking the gold. In the ensuing fight, Glen is shot and apparently killed, but Duke discovers the bullets to be blanks with red paint. At that moment, the organizer of the cattle drive, Clay Stone (Noble Willingham) appears along with some of their old friends, such as Ira and Barry Shalowitz. Clay Stone explains that the cowboys are his sons and he has been looking for Duke for some time. Having left the cattle business, Clay Stone is now making a living taking men on a trip to find the Washburn treasure, which is revealed to be lead painted with gold. Though Mitch, Phil and Glen feel lost, Duke remains convinced that the gold is out there somewhere, and stays behind as the others return to Las Vegas.

However, in his hotel room, Mitch is visited by Duke, who reveals that the entire time, he knew where the gold truly was and intended to keep it all for himself, but couldnt bring himself to do so. Through Mitchs skepticism, Duke reveals that he had the missing corner of the map, which points to where Lincoln reburied the gold in 1909, and presents a bar of gold to Mitch as a gift. Mitch tries to scratch the gold off with a knife, and screams in joy upon realizing that the gold is real after all.

==Cast==
*Billy Crystal as Mitch Robbins Daniel Stern as Phil Berquist
*Jon Lovitz as Glenn Robbins
*Jack Palance as Duke Washburn
*Patricia Wettig as Barbara Robbins
*Noble Willingham as Clay Stone
*Pruitt Taylor Vince as Bud
*Bill McKinney as Matt
*Josh Mostel as Barry Shalowitz
*David Paymer as Ira Shalowitz
*Lindsay Crystal, daughter of Billy, as Holly Robbins
*Beth Grant as Lois
*Jayne Meadows as the voice of Mitchs mother
*Jennifer Crystal Foley, daughter of Billy, as jogger
*Bob Balaban as Dr. Jeffrey Sanborn (uncredited)
*Frank Welker as Norman (voice) 

==Reception==

The movie gained a negative reception.   

===Box Office===

The movie debuted at No. 3. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 