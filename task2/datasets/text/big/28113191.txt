The Divided Heart
{{Infobox film
| name           = The Divided Heart
| image          = Dividedheartposter.jpg
| caption        = The Divided Heart UK release poster
| director       = Charles Crichton
| producer       = Michael Truman Richard Hughes
| starring       = Cornell Borchers Yvonne Mitchell Armin Dahlen Alexander Knox
| music          = Georges Auric
| cinematography = Otto Heller
| editing        = Peter Bezencenet
| distributor    = Ealing Studios
| released       = 9 November 1954 (UK) 11 August 1955 (U.S.)
| runtime        = 89 min.
| country        = United Kingdom English
| budget         =
}}
 Richard Hughes. It was produced by Michael Truman and edited by Peter Bezencenet, with cinematography by Otto Heller and music by Georges Auric. The Divided Heart was widely admired, and won three British Academy Film Awards.

==Plot==
During World War II, a three-year-old boy is found wandering alone in Germany.  No family can be traced, and it is presumed that his parents and siblings have been casualties of war.  The child is placed in an orphanage, from where he is subsequently adopted by a childless couple, whom he grows to love and accept as his parents.  When the boy is 10 years old, his natural mother is found alive in Yugoslavia where she has survived the war as a refugee.  She returns to Germany to claim her child, having lost her husband and two other children in the war.  The film focuses on the moral dilemma of the situation:  should the child remain with the adoptive parents who have given him a loving and happy home, or be returned to his natural mother who has lost everything else, and to what extent should the childs own wishes be taken into account?  The case is finally referred to a three-man court, who will decide the childs future.

==Cast==
* Cornell Borchers as Inga
* Yvonne Mitchell as Sonja
* Armin Dahlen as Franz
* Alexander Knox as Chief Justice
* Geoffrey Keen as Marks
* Liam Redmond as First Justice
* Eddie Byrne as Second Justice
* Theodore Bikel as Josip
* Pamela Stirling as Mlle. Poncet
* Martin Keller as Toni (aged 3)
* Michel Ray as Toni (aged 10) Martin Stephens as Hans
* André Mikhelson as Professor Miran
* Vito Istinič as Messenger Boy

==Reception and awards==
The Divided Heart was a popular and critical success, being highly praised for its sensitivity, emotional impact and the even-handedness with which it dealt with its subject matter.  While noting that the films ending reportedly left many viewers feeling disappointed and let down, critics conceded that it would have been impossible for a storyline of this nature to reach a conclusion which pleased everyone.  In a contemporary review in the New York Times, noted critic Bosley Crowther wrote: "This is a bleak, heart-rending problem, as it is finely presented in this film with exceptionally sensitive understanding and scrupulous integrity. And the fact that it cannot be unraveled to the satisfaction of all...is simply an indication that a happy solution is beyond the power of a man as wise as Solomon—or even the author of the script—to hit upon." 
 1955 British Best British Best Foreign 1955 National Board of Review Awards. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 