Baagyavathi
 
{{Infobox film
| name = Baagyavathi பாக்கியவதி
| image = 
| director = L. V. Prasad
| writer = R. Venkatachalam Padmini M. Ragini 
| producer = A. C. Pillai
| music = Susarla Dakshinamurthy|S. Dakshinamurthy
| distributor = Ravi Productions
| released =  
| runtime = 16732 ft
| country = India
| language = Tamil
| budget = 
}} Padmini and  M. N. Rajam in the lead roles.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Sivaji Ganesan || Somu
|- Padmini || Meena
|-
| K. A. Thangavelu  || Sangkaran
|-
| M. N. Rajam || Bama
|-
| K. Sarangkapani || Ramasamy
|- Ragini || Suguna
|-
| P. D. Sambandam || Saminathan
|-
| Poobathi Nandharam ||  Subbanna
|-
| M. Lakshmi Prabha || Velammal
|-
| K. N. Kamalam || Sooravalli
|-
| K. Aranganayaki || Somus Mother
|-
| Master Gopal || Ravi
|}

==Crew==
*Producer: A. C. Pillai
*Production Company: Ravi Productions
*Director: L. V. Prasad
*Music: Susarla Dakshinamurthy|S. Dakshinamurthy
*Lyrics: A. Maruthakasi & Subbu Arumugam
*Story: R. Venkatachalam
*Screenplay: Prasad
*Dialogues: R. Venkatachalam
*Art Direction: Ganga & V. Rajendra Kumar
*Editing: Sanjeevi
*Choreography: Ramasamy, Sampath Kumar& Chinnilal
*Cinematography: P. L. Rai
*Stunt: None
*Dance: None

==Soundtrack== Playback singers Soolamangalam Rajalakshmi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ellorum Unnai Nallavan Endre || Rao Balasaraswathi Devi|R. Balasaraswathi Devi || || 03:27
|- 
| 2 || Pombalainga Therinju Kollanum || S. C. Krishnan || || 03:08
|- 
| 3 || Vennilavin Oli Thanile || A. M. Rajah, T. V. Rathinam & S. C. Krishnan || || 05:15
|- 
| 4 || Vaazhvedhu Nal Vaazhvedhu || C. S. Jayaraman || || 03:31
|- 
| 5 || Kannale Vettadhe Summaa Kannale Vettadhe || S. C. Krishnan & T. V. Rathinam || || 03:19
|-  Soolamangalam Rajalakshmi || || 02:59
|- 
| 7 || Asai Kiliye Azhagu Chilaiye || P. Leela || || 03:16
|- 
| 8 || Dhinasari En Vaazhvil Thirunaale || Rao Balasaraswathi Devi|R. Balasaraswathi Devi || || 01:30
|}

==External links==
*  
*  

 
 
 
 
 


 