Nagaram (2008 film)
{{Infobox film
| name           = Nagaram
| image          =
| caption        =
| writer         = Swamiji-Vijay  
| producer       = M. Anjibabu   P. Kishore Babu.
| director       = CC Srinivas Srikanth  Kaveri Jha Chakri
| cinematography = Sarath
| editing        = Gautham Raju
| studio         = Mac Media-Blue Sky Entertainment    
| released       =  
| runtime        = 2:28:08
| country        = India
| language       = Telugu
| budget         =
| preceded_by    = 
| followed_by    = 
}}

Nagaram  is a 2008 Telugu action film produced by M. Anjibabu and P. Kishore Babu on Mac Media-Blue Sky Entertainment banner, directed by C. C. Srinivas. The film stars Jagapati Babu, Srikanth and Kaveri Jha in the lead roles, and the music is composed by Chakri (music director)|Chakri.            

==Cast==
{{columns-list|3|
*Jagapati Babu as ACP Chowdary Srikanth as Right
*Kaveri Jha Pradeep Rawat
*Kalabhavan Mani Ajay
*Brahmanandam Ali
*Dharmavarapu Subramanyam Venu Madhav
*Raghu Babu
*L. B. Sriram
*Satyam Rajesh
*G. V. Sudhakar Naidu Chinna
*Sivaji Raja
* Ram Jagan
* Prasanna Kumar
* Ravi Varma
* Gundu Hanumantha Rao
* Gundu Sudarshan
* Kadambari Kiran
* Subbaraya Sharma
* Vizag Prasad
* Rajitha
* Bhauvaneswari
* Meena
* Apoorva
* Bhargavi
}}

==Soundtrack==
{{Infobox album
| Name        = Nagaram
| Tagline     = 
| Type        = film Chakri
| Cover       = 
| Released    = 2008
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:20
| Label       = MADHURA Audio Chakri
| Reviews     =
| Last album  = Krishna (2008 film)|Krishna   (2008)
| This album  =  Nagaram    (2008)
| Next album  = Neninthe   (2008)
}}
 Chakri and released on MADHURA Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 25:20
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Edhalopala Edodaham
| lyrics1 = Bhakarbhatla Kousalya
| length1 = 6:14

| title2  = Varevva Vayase Garam
| lyrics2 = Veturi Sundararama Murthy
| extra2  = Ravi Varma,Sai Shivani
| length2 = 5:00

| title3  = Abhi Abhi
| lyrics3 = Bhakarbhatla
| extra3  = Chakri,Kousalya
| length3 = 4:59

| title4  = Hoshiyare Hoshiyare
| lyrics4 = Bhuvanachandra
| extra4  = Sunidhi Chauhan
| length4 = 4:35

| title5  = True Life Come On
| lyrics5 = Bhakarbhatla
| extra5  = Chakri,Revathi
| length5 = 4:32
}}
   

==References==
 
 
*
*
*
*

 

 
 
 


 