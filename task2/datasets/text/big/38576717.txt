Hangmen (film)
{{Infobox film
| name = Hangmen
| image = 
| image_size = 
| alt = 
| caption = 
| director =  J. Christian Ingvordsen
| producer = 
| writer =  J. Christian Ingvordsen Steven Kaman Rick Washburn
| narrator = 
| starring = Rick Washburn Keith Bogart Sandra Bullock
| music =  Michael Montes
| cinematography = 
| editing = 
| studio = 
| distributor = 
| released =  
| runtime =  90 min.
| country = United States
| language = English
| budget = 
| gross = 
}}

Hangmen is a 1987 American action thriller film, it marked American actress Sandra Bullocks film debut.

==Plot==
Dick Biel is the assassin of a US senator, who is responsible for responding to the incident accidentally kills several innocent bystanders with the team, during the operation.

Several years later, Rob Greene becomes the team leader for the secret operations team that had been charged with protecting the senator, having information about a renegade CIA cell and that cell wants Greene and his information. He runs from the cell, so they attempt to kidnap his son as a hostage. In the process, they murder Denny best friend, two of Rob Greene’s former team, Rob’s ex-wife and her new husband. The rest of the team get together to fight back, including the team member responsible for the deaths of bystanders years before. When the CIA cell kidnaps Lisa, Denny’s girlfriend, they are able to manipulate Denny into surrendering to them. Rob’s team then attacks the headquarters of the CIA cell in a rescue mission.

==Trivia==
Bullock is featured on the covers of later video releases as the headline star, capitalizing on her later stardom.

==Cast==
*Rick Washburn as Rob Greene	
*Doug Thomas as Dog Thompson	
*Keith Bogart as Denny Greene	
*"John Christian" (J. Christian Ingvordsen) as Bone Conn	
*Sandra Bullock as Lisa Edwards	
*David Henry Keller as Andrews	
*Dan Lutsky as Joe Connelly	
*"Sam Silver" (Dan Golden) as Reynolds	
*Robert Khaleel as Draff	
*Jake LaMotta as Moe Boone	
*Kosmo Vinyl as Kosmo
*Stu Day as Keeper
*Dick Mel as Senator Dick Biel	
*Amanda Zinsser as Caroline Fosterian

==References==
* 

 
 
 


 