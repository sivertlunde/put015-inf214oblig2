The Power of the Whistler
{{Infobox film
| name           = The Power of the Whistler
| image          = Power of the whistler poster small.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Lew Landers
| producer       = Leonard S. Picker
| screenplay     = Aubrey Wisberg
| narrator       = Otto Forrest Richard Dix Janis Carter
| music          = Paul Sawtell
| cinematography = L. William OConnell
| editing        = Reg Browne
| studio         = Larry Darmour Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} noir based Richard Dix, Janis Carter and Jeff Donnell. It is the third of Columbia Pictures eight "The Whistler#Films|Whistler" films produced in the 1940s, seven starring Dix.

==Plot==
Dix plays an amnesiac who learns about his name and past through the help of amateur fortune teller Jean Lang (Janis Carter).

The fortune teller sees the mysterious-looking man when she is in a restaurant with her sister and the sisters boyfriend. Without meeting him, she predicts that he will be near death twice in the coming day. Outside the restaurant, she saves him from being struck by a car. It is then that he realizes he has no memory of his past.

Charmed by his pleasant, cultured manner, she resolves to help him uncover the mystery of his life. She continues to do so, even as she encounters signs that they may find something scary.

==Cast== Richard Dix as William Everest
* Janis Carter as Jean Lang
* Jeff Donnell as Frances
* Loren Tindall as Charlie Kent
* Tala Birell as Constantina Ivaneska John Abbott as Kaspar Andropolous
* Otto Forrest as The Whistler
* Nina Mae McKinney as Flotilda, Constantinas Maid

==Reception==
Critic Leonard Maltin said of the film, "Third Whistler entry is a little padded but still genuinely eerie." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 