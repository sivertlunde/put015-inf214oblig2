The Greater Good (film)
{{Infobox film
| name           = The Greater Good
| image          = 
| alt            = 
| caption        = 
| director       = Kendall Nelson, Chris Pilaro
| producer       = Leslie Manookian Bradshaw
| writer         = Leslie Manookian Bradshaw, Jack Youngelson
| starring       = Gabi Swank, Jordan King, and the Christeners 
| music          = Stephen Thomas Cavit
| cinematography = 
| editing        = 
| studio         = BNP Pictures
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

== Summary ==

The Greater Good is a documentary film about the risks vs. benefits of vaccines that originally debuted at the    as well as the cinematic vision award at the Amsterdam Film Festival. 

Stories covered in the film include those of:

* Gabi Swank of Wichita, Kansas, who received an HPV vaccine and attributes a number of adverse reactions, including a seizure, to this experience, autism omnibus proceedings and whose case was rejected by the Special Master, and
* Victoria Grace Boyd Christener of Tulsa, Oklahoma, who died at the age of 5 months after receiving a vaccine.

Well-known vaccine experts who appear in the film include Paul Offit, Melinda Wharton, and Norman Baylor of the Center for Biologics Evaluation and Research. The film was endorsed by Joseph Mercola on his website, as part of "Vaccine Awareness Week", a joint venture with the National Vaccine Information Center.   

The filmmakers, Kendall Nelson and Chris Pilaro, are from Sun Valley, Idaho. 

==List of appearances grouped by position on vaccinations==

=== Discussing vaccine benefits ===
* Dr. Paul Offit an American pediatrician specializing in infectious diseases and an expert on vaccines, immunology, and virology.
* Dr. Melinda Wharton of the CDC
* Dr. Norman Baylor, Director of the Office of Vaccines Research and Review in the FDA’s Center for Biologics Evaluation and Research
* Mark B. Feinberg, Vice President for Medical Affairs and Policy for Merck Vaccines and Infectious Diseases at Merck & Company, Inc
* Walt Orenstein, who formerly held a post at the Centers for Disease Control where he led the National Immunization Program 

=== Discussing vaccine risks ===

* Dr. Lawrence B. Palevsky
* Dr. John Green III, a fellow of the American Academy of Environmental Medicine
* Diane Harper, an investigator at one of the sites where the original clinical trials of Gardasil was conducted
* Kevin Conway and Clifford Shoemaker, lawyers who represented the families suing the government in the autism omnibus trial. 
* Renee Gentry, a lawyer at Shoemakers law firm
* Christopher Shaw, PhD, a professor in the Department of Ophthalmology and Visual Sciences at the University of British Columbia.
* Barbara Loe Fisher
* Robert Sears (physician)

==Airing==
The documentary was aired on Current TV on 24 March 2012. 

==Criticism==

=== From the medical establishment ===

* David Gorski criticized the movie in a blog post,  lamenting that the film "which could have been a provocative debate about current vaccine policy based on asking which vaccines are necessary and why, in the end opts to be nothing more than pure anti-vaccine propaganda of the lowest and most vile sort." 

=== From the media ===
* The New York Times criticized the movie, calling it "emotionally manipulative," and "heavily partial."  
* Variety (magazine)s John Anderson reviewed the film, saying that it is "swimming in ethical contradictions." Anderson also stated, with regard to the films potential bias, "Admittedly, it would have been difficult for the filmmakers to show the other side of those scenes; how do you focus on subjects who haven’t died from smallpox, diphtheria or pertussis because they were immunized as children? But that would require an approach that doesn’t take advantage of the audience’s emotions."  The conjecture presented in the movie that vaccines might cause autism is contradicted by all existing scientific evidence on the subject. 

==References==
 

 
 
 
 
 
 
 
 