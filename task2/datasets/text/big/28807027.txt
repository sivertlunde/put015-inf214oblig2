The Roaring Fifties
The German comedy film directed by Peter Zadek and starring Juraj Kukura, Boy Gobert and Peter Kern.  It is based on the novel Hurra, wir leben noch by Johannes Mario Simmel. It is set around the German Wirtschaftswunder economic miracle of the 1950s, with the title alluding to the Roaring Twenties.

==Cast==
* Juraj Kukura ...  Jakob Fuhrmann
* Boy Gobert ...  Udo von Gerresheim
* Peter Kern ...  Franz Arnusch
* Nora Barner ...  Julia
* Christine Kaufmann ...  Natascha
* Sunnyi Melles ...  Bambi
* Beatrice Richter ...  Dr. Malthus
* Eva Mattes ...  Hilde Werwolf
* Dietrich Mattausch ...  Wenzel Prill
* Hermann Lause ...  Mario Schreiber
* Paul Esser ...  Senator Hilton
* Klaus Höhne ...  Colonel Hobson
* Ilja Richter ...  Reporter
* Willy Millowitsch ...  Professor Donner
* Freddy Quinn ...  General Mark Clark
* Sona MacDonald ...  Yün-Sin
* Christa Berndl ...  Mrs. Hilton
* Brigitte Mira ...  Mrs. Willmsen
* Dominique Horwitz ...  Mick
* Ingrid Caven ...  Claudine
* Charles Regnier ...  Igor
* Karl-Heinz Vosgerau ...  Rouvier
* Rudolf Lenz ...  Sir Derek
* Karl Lieffen ...  Major Blaschenko
* Burkhard Driest ...  Major Assimov
* Guido Baumann
* Ulrich Wildgruber
* Udo Kier
* Uwe Friedrichsen
* Ivan Desny
* Margit Carstensen
* András Fricsay
* Bea Fiedler
* Curt Bois
* Diether Krebs
* Lou van Burg

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 