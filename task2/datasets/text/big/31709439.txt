The Branded Woman
{{infobox film
| name           = The Branded Woman
| image          = Thebrandedwoman-1920-movieposter.jpg
| imagesize      =
| caption        = 1920 theatrical poster Albert Parker
| producer       = Joseph Schenck Norma Talmadge
| writer         = Anita Loos (adaptation) Albert Parker (adaptation) Burns Mantle (intertitles)
| based on       =  
| starring       = Norma Talmadge Percy Marmont 
| cinematography = J. Roy Hunt
| editing        =
| distributor    = First National Pictures
| released       = October 4, 1920
| runtime        = 84 mins.
| country        = United States
| language       = Silent English intertitles
}}
 1920 American silent film Albert Parker who also directed. A print of the film is housed at the Library of Congress. 

==Cast==
* Norma Talmadge – Ruth Sawyer
* Percy Marmont – Douglas Courtenay
* Vincent Serrano – Velvet Craft
* George Fawcett – Judge Whitlock
* Grace Studdiford – Dot Belmar
* Gaston Glass – William Whitlock
* Jean Armour – Mrs. Bolton
* Edna Murphy – Vivian Bolton
* Henry Carvill – Henry Bolton(billed H.J. Carvill) Charles Lane – Herbert Averill 
* Sidney Herbert – Detective
* Edouard Durand – Jeweler
* Henrietta Floyd – Miss Weir

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 