Una Jaula no tiene secretos
{{Infobox film name           = Una Jaula no tiene secretos image          = Una jaula no tiene secretos.jpg image_size     = 200px caption        = director       = Agustín Navarro producer       = writer         = Raúl Gurruchaga studio         = Visor Films released       = October 3, 1962 country        = Spain, Argentina language  Spanish
}}
 1962 Spain|Spanish Argentine comedy film directed by Agustín Navarro.   
The script was written by Raúl Gurruchaga   
The movie premiered on October 3, 1962. 
The movie won the original screenplay award of the year. 

The plot revolves around the breakdown of a buildings elevator, trapping its passengers. 
The breakdown occurs just before midnight on the last day of the year.
The elevator operator is Alberto Olmedo, who still had some hair at the time.
The light comedy includes various gags and humorous situations. 
The film was one of the last in which Carlos Gandolfo appeared as an actor. After being diagnosed with throat cancer, he turned to directing and teaching. 

==Cast==
*Martín Andrade
*Cacho Espíndola
*Gloria Ferrandiz
*Carlos Gandolfo
*Fernando Iglesias Tacholas
*Juan Carlos Lamas
*Alejandro Maximino
*Pablo Moret
*Alberto Olmedo
*Rodolfo Onetto
*Carlos Pamplona
*Nathán Pinzón
*Javier Portales
*Luis Rodrigo
*Edmundo Sanders

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 