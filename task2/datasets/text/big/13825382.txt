Nicotina
 
{{Infobox film
| name = Nicotina
| image = Nicotina.jpg
| caption = 
| director = Hugo Rodríguez
| producer = Laura Imperiale Monica Lozano Serrano
| writer = Martín Salinas
| starring = Diego Luna
| music = Fernando Corona
| cinematography = Marcelo Iaccarino
| editing = Alberto de Toro
| distributor = Arenas Group
| released =  
| runtime = 93 minutes
| country = Mexico Argentina
| language = Spanish
| budget = $446,768
}} 2003 Cinema Argentine gangster film produced by the same team as the 2000 acclaimed film Amores perros.

==Plot==
Lolo is a male computer science geek who tangles with a clutch of the Russian mafia, when he delivers the wrong computer disk to them and with the disastrous results of drugs and smoking tobacco.

==Cast==
*Diego Luna as Lolo
*Lucas Crespi as Nene
*Norman Sotolongo as Svóboda
*Jesús Ochoa (actor)|Jesús Ochoa as Tomson
*Martha Tenorio as Eulogia (as Marta Tenorio)
*Rafael Inclán as Goyo
*Rosa María Bianchi as Carmen
*José María Yazpik as Joaquín
*Marta Belaustegui as Andrea
*Eugenio Montessoro as Carlos
*Carmen Madrid as Clara
*Daniel Giménez Cacho as Beto
*Alexis Sánchez as Andrei
*Jorge Zárate as Sánchez
*Enoc Leaño as  Memo

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 
 