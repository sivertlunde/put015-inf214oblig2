Victory (1940 film)
 

{{Infobox film
| name           = Victory
| caption        = 
| image	         =  John Cromwell
| producer       = Anthony Veiller
| writer         = John L. Balderston novel by Joseph Conrad
| starring       = Fredric March Cedric Hardwicke Betty Field
| music          = 
| cinematography = Leo Tover
| editing        = 
| studio         = Paramount
| distributor    = Paramount
| released       = 1940
| runtime        = 
| country        = United States English
| budget         = 
}}

Victory is a 1940 film based on the novel by Joseph Conrad.

==Cast==
* Fredric March as Hendrik Heyst
* Betty Field as Alma
* Sir Cedric Hardwicke as Mr. Jones
* Jerome Cowan as Martin Ricardo
* Sig Rumann as Mr. Schomberg
* Margaret Wycherly as Mrs. Schomberg
* Fritz Feld as Señor Makanoff 
* Alan Ladd as Heyst Jnr

==References==


 

==External links==
*   at IMDB
*   at New York Times
*   at TCMDB

 
 

 
 
 
 
 

 