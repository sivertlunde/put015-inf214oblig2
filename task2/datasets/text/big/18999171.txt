Separate Tables (film)
 
{{Infobox film
| name            = Separate Tables
| image           = Separate_tables.jpg
| caption         = Original film poster
| director        = Delbert Mann
| producer        = Harold Hecht John Gay   John Michael Hayes (uncredited)
| starring        = Rita Hayworth   Deborah Kerr   David Niven   Burt Lancaster   Wendy Hiller
| music           = David Raksin (score) Harry Warren and Harold Adamson (song, "Separate Tables")
| cinematography  = Charles Lang
| editing         = 
| distributor     = United Artists
| released        =  
| runtime         = 100 minutes
| country         = United States
| language        = English
| budget          = 
| gross           = $3.1 million (est. US/ Canada rentals)  244,284 admissions (France) 
}} two one-act John Gay and an uncredited John Michael Hayes. Mary Grant and Edith Head designed the films costumes.

==Plot==
Major David Angus Pollock (David Niven) fails to steal an article about himself in the West Hampshire Weekly News. His attempt to keep the article from the eyes of the other guests at the residential hotel only succeeds in heightening their awareness of it, particularly Mrs. Railton-Bell (Gladys Cooper).  She and Lady Gladys Matheson (Cathleen Nesbitt) read that Major Pollock pleaded guilty to sexually harassing several young women in a theater, however, the filed complaints are, in themselves, questionable. Mrs. Railton-Bell wants Major Pollock to be expelled from the hotel and holds a meeting with other long term residents to decide the issue before presenting it to the manager, Miss Pat Cooper (Wendy Hiller).

Mrs. Railton-Bell leads the meeting with deluded arguments promoting the Majors expulsion, and despite the true opinions of the residents, she informs Miss Cooper. 

Anne (Rita Hayworth) and John (Burt Lancaster) meet outside. Anne coolly teases John, informing him that she is engaged; John tells her he is engaged as well, but does not disclose that he is engaged to Miss Cooper. John claims that though Anne could have married other men who were wealthier and more important, she wanted to marry a man in a lower economic class in order to manipulate and degrade him fully.  Despite this, John and Anne admit they are still attracted to each other. She asks him to come to her room.

As they walk into the hotel, Miss Cooper tells Anne she has a phone call. Miss Cooper, knowing Anne is his ex-wife, asks him to reevaluate the real reason for Annes visit. John defends Anne at first, claiming that all his misfortunes are his own fault, but changes his mind when Miss Cooper tells him Anne is talking on the phone to his publisher, the only person who knows John and Miss Cooper are engaged.
 
John confronts Anne in her bedroom. She attempts to seduce him, but he tells her that in the light, he can see that she has aged; without her physical beauty, it will be impossible for her to manipulate people. She begs him to stay, but he runs out of the hotel. Anne has an emotional break down and Miss Cooper comforts her. Anne tells Miss Cooper that she is not really engaged and has been abusing sleeping pills to ease her pain, often during the day. 

The next morning, Mrs. Railton-Bells daughter Sibyl (Deborah Kerr) tells Major Pollock that she knows what he did. Major Pollock explains that he has always been fearful of people and that it is easier for him to be sexual with strangers. Major Pollock tells Sibyl that they got along well with each other because they are both afraid of life. As he returns to his room to pack, Sibyl worries that he will not find a new home. 

Miss Cooper tells John, who returns in the morning, that Anne is emotionally unwell, and asks him to see her before she checks out of the hotel. After John leaves, Miss Cooper attempts to convince Major Pollock to stay, but he refuses. 

The hotel residents eat their breakfast at separate tables in the dining room. John and Anne reconcile, but do not know if they will ever be happy, together or apart. When Major Pollock enters the room, there is an awkward silence until John greets him. The others do the same and cheerfully converse with him. Mrs. Railton-Bell is angered at this, and demands Sibyl leave with her. Sibyl refuses and as her mother leaves, she starts to talk to Major Pollock. Major Pollock decides to stay at the hotel.

==Cast==
* Rita Hayworth as Ann Shankland
* Deborah Kerr as Sibyl Railton-Bell
* David Niven as Major David Angus Pollock
* Burt Lancaster as John Malcolm
* Wendy Hiller as Pat Cooper
* Gladys Cooper as Mrs. Maud Railton-Bell
* Cathleen Nesbitt as Lady Gladys Matheson
* Felix Aylmer as Mr. Fowler
* Rod Taylor as Charles
* Audrey Dalton as Jean
* May Hallatt as Miss Meacham 
* Priscilla Morgan as Doreen

==Production and awards== Best Actor Best Supporting Actress).   

Burt Lancaster was also co-producer (Hill-Hecht-Lancaster Productions).  Rita Hayworth was married to James Hill at the time.

Rod Taylor agreed to play a small part in the film because of the quality of the production. 

==References==
 

==External links==
*  
*   
*   

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 