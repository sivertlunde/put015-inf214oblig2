Premasathi Coming Suun
 

{{Infobox film
| name = Premasathi Coming Suun
| image = Premasathi_Coming_Suun.png
| caption = Premasathi Coming Suun First Look Poster 
| director = Ankur Kakatkar
| starring = Neha Pendse  Adinath Kothare  Jitendra Joshi
| producer = Anup Kumar Poddar, Aman Vidhate, Sanjay Sankla, Mulchand Dedhia
| writer   = Chinmay Kulkarni
| music    = Pankaj Padghan
| released =  
| runtime  = 135 minutes
| language = Marathi
| country = India
}}
 Marathi comedy about Aditya (Adinath Kothare), a young man in his early twenties whose family is looking for a bride for him, and Antara (Neha Pendse), the prospective bride with a hidden agenda. 

==Reception==
{{Album ratings
| rev1 = Book My Show    
| rev1Score = 
}}

Premasathi Coming Suun was met with favorable reviews. Bookmyshow.com gave it 5 out of 5 stars, calling it "a neat and clean comedy". 

==Cast==
 
* Neha Pendse as Antara 
* Adinath Kothare as Aditya
* Jitendra Joshi as Kolte Patil
* Resham Tipnis as Antaras Mami
* Vijay Patkar as Antaras Mama
* Suhas Joshi as Adityas Aaji
* Anchal Poddar 

==Crew==
* Producers : Anup Kumar Poddar, Aman Vidhate, Sanjay Sankla, Mulchand Dedhia
* Story & Screenplay : Chinmay Kulkarni
* Art Director : Ajit Reddy
* Cinematography : Prasad Bhende
* Music : Pankaj Padghan
* Lyrics : Chetan Dange
* Genre : Romantic, Comedy, Drama
* Studio : Wonderland Films 

==Music==

===Soundtrack===
{{tracklist
| lyrics_credits  = yes
| music_credits   = yes
| extra_column    = Singer
| title1          = Bawara 
| extra1          = Mangesh Bandodkar
| lyrics1         = Chetan Dange
| music1          = Pankaj Padghan
| length1         = 4:20
| title2          = Halad 
| extra2          = Sayali Pankaj
| lyrics2         = Chetan Dange
| music2          = Pankaj Padghan
| length2         = 5:10
| title3          = Ala Majha ganraya  
| extra3          = Jaydeep Bagwadkar
| lyrics3         = Chetan Dange
| music3          = Pankaj Padghan
| length3         = 4:40
| title4          = Ali Gulabo
| extra4          = Neha Lele
| lyrics4         = Chetan Dange
| music4          = Pankaj Padghan
| length4         = 4:10
}}

==References==
 

==External links==
*  
*  
*  

 