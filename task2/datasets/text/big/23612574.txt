Donald's Halloween Scare
 
 

Donalds Halloween Scare is a cartoon made by Walt Disney Television Animation. It was released on March 4, 2000. Music by Stephen James Taylor was nominated for an Annie Award in 2000.

==Plot== Mickey (as Minnie (as Chief OHara Chief OHara is really Huey, Dewey and Louie, stacked up and disguised with a latex mask and raincoat. Donald runs away after failing to remove his costume but Mickey, Minnie and Goofy (who have also received wanted posters) chase after him, believing that hes really the culprit and corner him in Minnies cauldron and he is sent flying just as the trio prepare to attack him. Donald ends up in a graveyard in front of three tombstones with his nephews names on them. Hewy, Dewy and Louie pretend to be zombies, and say that their uncle scared them to death. They chase Donald across the graveyard until he falls into the hole and they say "Hope you liked our candy uncle Donald, that shall be your last", Donald replied "Oh Im sorry boys Ill get your candy back" the boys asked "All of it?" Donald "Cross my heart and hope to die" and the boys dress him up in a costume of their own and make him trick-or-treat with them. All four of them run away, however, when the next person they see in the shadows has a hook for a hand, they get terrified, but it turns out that it was actually Goofy, dressed as Santa Claus (now thinking its Christmas), prepared to give the candy canes.

==Television==
This cartoon was first shown as part of the March 4, 2000 episode of Mickey Mouse Works on ABC. The cartoon was later seen in the September 9, 2000 episode. It was later featured in the "Halloween with Hades" episode of Disneys House of Mouse and was also part of the direct-to-video House of Mouse special, Mickeys House of Villains.

==References==
 

 
 
 
 
 