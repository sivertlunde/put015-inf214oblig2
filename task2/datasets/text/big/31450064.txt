Buddha in a Traffic Jam
{{Infobox film
| name           = Buddha in a Traffic Jam
| image          = 
| alt            =  
| caption        = 
| director       = Vivek Agnihotri
| producer       = Friday Night Productions   Phoenix Multidimensions  Vivek Agnihotri Creates 
| writer         = Rohit Malhotra and Vivek Agnihotri
| starring       =  
| music          = Rohit Sharma
| cinematography = Attar Singh Saini
| editing        = Satyajit Gazmer
| studio         = 
| distributor    = 
| released       = Unreleased
| runtime        = 
| country        = India
| language       =  
| budget         = 
| gross          = 
}}
Buddha in a Traffic Jam is an unreleased Bollywood film conceptualized and produced by Friday Night Productions (a venture of ISB alumni) and co-produced by Phoenix Multidimensions and Vivek Agnihotri creates. Buddha in a Traffic Jam is directed by Vivek Agnihotri and stars Arunoday Singh and Mahi Gill, while the other members from the cast include Anupam Kher,  Pallavi Joshi and introducing Anchal Dwivedi.   

"Buddha In A Traffic Jam" premiered in the 2014 version of Mumbai International Film Festival  in the India Gold category with a standing ovation and great reviews. The film was also bestowed awards and was the opening film in the 7th Global Film Festival  besides participation in The Jaipur International Film Festival  in February 2015.

The critically acclaimed film is also an official entry in the 5th Dada Saheb Phalke Film Festival besides bagging three nominations in Madrid International Film Festival in the Best Foreign Language Film,  Best Original Screenplay of a Foreign Language  and Best Lead Actress in a foreign language to actress Mahi Gill. 

== Synopsis ==

Vikram Pandit Arunoday Singh is a happy-go-lucky management student from a top Business School of India. He becomes an overnight sensation after a successful internet campaign against the radical fundamentalism of moral policing in India. 
This prompts his university teacher, Professor Ranjan Batki Anupam Kher, to throw him a challenge for yet another internet campaign.A campaign that would help raise money via a non profit Pottery Club for poor people living in Maoist areas of India. 
Little did Vikram Arunoday Singh know that he was about to become a part of a plot that would risk his life and the nation. He gets entangled between two corrupt facets of India – Socialism and Capitalism, both of which are deeply rooted in isolated corners of the country. It’s a story inspired from true life incidents of Underbelly India!

== Plot Summary ==

Vikram Pandit Arunoday Singh is a fun happy-go-lucky IIT graduate from Delhi. With two years of work experience at The Wall Street, he is pursuing a management degree from a top B School of India. Once while wih friends at a local pub, he witnesses the radical fundamentalism of Moral Policing. As his friend Pooja’s face is blackened by the goons, he starts a campaign on Social Media to get back to fundamentalists. As the campaign goes viral, readers start sending Pink Bras as a sign of protest. The campaign is a huge hit and Vikram becomes a hero overnight!

One of the teachers of IIB, Professor Ranjan Batki Anupam Kher is impressed with him, and introduces him to the plight of the tribal. Vikram Arunoday Singh impressed with the Professor’s Anupam Kher theory of Socialism, takes to the web again to create another successful campaign. But as he is celebrating his success, he is roughed up by a man at the local bar who calls him a traitor. Vikram Arunoday Singh gives up, but Professor Batki Anupam Kher still has hopes from him.

Batki’s Anupam Kher wife Sheetal Pallavi Joshi runs The Potter’s Club within the IIB which is a government aided venture. The entire sum of money collected via profits goes to an NGO Peace Commando, which works for the tribal. Batki Anupam Kher engages Vikram Arunoday Singh to market the products of The Potters Club, after the Government Grant is denied. Vikram Arunoday Singh innocently accepts this challenge without realising the dangers ahead.

Somewhere deep within the jungles, Maoists are gearing up to overthrow the Government. They already have established links with the intelligentsia of the Society. Vikram’s Arunoday Singh marketing Module pulls him in to a very deep web of conspiracy, of disguised Socialism. Can Vikram Arunoday Singh survive against the Maoists? What holds the key to the future of India? Which is a failed theory and what is the future? And finally, what does the youth of India want? Capitalism or Socialism.

==Cast==

*Arunoday Singh as Vikram Pandit
*Mahi Gill
*Anupam Kher as Professor Jamshed Batki
*Pallavi Joshi 
*Vivek Vaswani
*Anchal Dwivedi
*Gopal K Singh as Naxal chief

==References==
 

== External links ==
* 
* 

 
 
 
 