Fiddle (film)
{{Infobox film
| name           = Fiddle
| image          = Fiddle Poster.jpg
| image_size     = 
| caption        = 
| director       = Prabhakaran Muthana
| producer       = Nissam Vettoor Noushad Anchal
| writer         = 
| narrator       =  Ayilya Jagathy Sreekumar
| music          = S. Jayan Ramesh Balakrishna
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Ayilya and Salim Kumar in pivotal roles.

== Story ==
Fiddle is about a group of youngsters who are also students of the Music College, form an organization called Satkala Sangham under the leadership of Sandeep (Varun J, Thilak). They move from village to village doing programmes and help the needy and the poor with the money that they earn. On one such journey they happen to camp at a hill station called Ponmudi, where Sandeep and his friends get caught up in the mysteries surrounding the life of a girl named Gayatri (Ayilya G. Nair).

== Cast ==

Fiddle marks the acting debut of ISS fame Varun J. Thilak, and he has done a fair job in a low weight role that doesnt demand too much from his side. Ananya does the same as well, and never has to perform much either. Jagathy, Bindu Panicker and Jagadeesh also chip in their little bits, but to no avail. The same however couldnt be said of the performances of a couple of other new faces who have done significant roles in the film. 

== Cast ==
* Varun J. Thilak as Sandeep
* Jagathy Sreekumar Ananya as Gayatri
* Salim Kumar
* Jagadish Santhakumari
* Lakshmipriya

== References ==
 

==External links==
* http://www.nowrunning.com/movie/5396/malayalam/fiddle/index.htm
* http://www.indiaglitz.com/channels/malayalam/preview/11636.html
* http://popcorn.oneindia.in/title/1485/fiddle.html
* http://www.cinecurry.com/movie/malayalam/fiddle

 
 
 
 
 


 