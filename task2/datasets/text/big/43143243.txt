Bait (1954 film)
{{Infobox film
| name           = Bait
| image          = Bait (1954 film) poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Hugo Haas
| producer       = Hugo Haas
| screenplay     = Samuel W. Taylor Hugo Haas
| narrator       = 
| starring       = Cleo Moore Hugo Haas John Agar
| music          = Václav Divina
| cinematography = Eddie Fitzgerald
| editing        = Robert S. Eisen
| studio         = Hugo Haas Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Bait is a 1954 crime, drama, melodrama, film noir, written, directed and produced by Hugo Haas. Haas himself, Cleo Moore and John Agar starred in the film. 

==Plot==

Middle-aged Marko (Haas) is searching for a lost gold mine for nearly 20 years. To share expenses for a prospecting expedition he teams up with bright young Ray Brighton (Agar). When they find the mine Marko decides he doesnt want to share with his partner and plans to murder him. He figures that after the two of them spend the winter together with Markos trashy young wife (Moore) in a shack far from civilization, he will sooner or later catch them in adultery, and he can use the "unwritten law" to kill Brighton and thus escape punishment from the law. But the plot backfires.

==Cast==
* Cleo Moore as Peggy
* Hugo Haas as Marko
* John Agar as Ray Brighton
* Emmett Lynn as Foley
* Bruno VeSota as Webb
* Jan Englund as Waitress
* George Keymas as Chuck
* Cedric Hardwicke as Prologue Speaker

==External links==
*  
*  
*  

==References==
 

 
 
 
 
 
 
 
 

 