Beck – Pojken i glaskulan
{{Infobox film
| name           = Beck – Pojken i glaskulan
| image          = Beck 15 - Pojken i glaskulan.jpg
| image_size     =
| alt            = 
| caption        = Swedish DVD-cover
| director       = Daniel Lind Lagerlöf
| producer       = Lars Blomgren Börje Hansson
| writer         = Cilla Börjlind Rolf Börjlind
| narrator       =
| starring       = Peter Haber Mikael Persbrandt Malin Birgerson
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 2002
| runtime        =
| country        = Sweden Swedish
| budget         =
| gross          =
}}
Beck – Pojken i glaskulan is a 2002 film about the Swedish police detective Martin Beck directed by Daniel Lind Lagerlöf.

== Cast ==
*Peter Haber as Martin Beck
*Mikael Persbrandt as Gunvald Larsson
*Malin Birgerson as Alice Levander
*Marie Göranzon as Margareta Oberg
*Hanns Zischler as Josef Hillman
*Ingvar Hirdwall as Martin Becks neighbour
*Rebecka Hemse as Inger (Martin Becks daughter)
*Jimmy Endeley as Robban
*Mårten Klingberg as Nick
*Peter Hüttner as Oljelund Anders Nyström as Waltberg
*Lena Carlsson as Lisa Norling
*Ulf Friberg as Kaj Gerstedt
*Anders Palm as Bernt Jansson
*Leo Hallerstam as Jack Svensson Per Svensson as Stefan Svensson

== References ==
*{{cite web | title=Beck - Pojken i glaskulan (2002) |
url=http://www.sfi.se/sv/svensk-film/Filmdatabasen/?type=MOVIE&itemid=56886
| publisher=Swedish Film Institute | accessdate=2009-07-14}}

== External links ==
* 

 

 
 
 
 
 


 
 