The Last Edition
 
{{Infobox film
| name           = The Last Edition
| image          =
| caption        =
| director       = Emory Johnson
| producer       =
| writer         = Emilie Johnson Ralph Lewis
| cinematography = Gilbert Warrenton
| editing        =
| studio         = Emory Johnson Productions
| distributor    = Film Booking Offices of America
| released       =  
| runtime        = 7 reels (6,400 feet)
| country        = United States Silent English intertitles
}}
 Ralph Lewis who plays a pressman at the San Francisco Chronicle. The film was shot around the Chronicle Building at Geary, Kearny, and Market in downtown San Francisco. Soon after, the Chronicle moved to its present building at Mission and Fifth Streets in San Francisco.

== Restoration == Rob Byrne EYE Film Institute in Amsterdam, the Netherlands. In collaboration with EYE and the San Francisco Silent Film Festival the highly flammable nitrate film was restored. 
 trailer at the Library of Congress. This was restored in the process as well. 

The copy in the collection of EYE was meant specifically for the Dutch market and therefore the intertitles were in Dutch. They were translated back into English by two volunteers.

==Cast== Ralph Lewis - Tom McDonald
* Lila Leslie - Mary McDonald
* Ray Hallor - Ray McDonald
* Frances Teague - Polly McDonald
* Rex Lease - Clarence Walker Lou Payne - George Hamilton David Kirby - "Red Moran"
* Wade Boteler - Mike Fitzgerald
* Cuyler Supplee - Gerald Fuller
* Lee Willard - Aaron Hoffman
* Will Frank - Sam Blotz Ada Mae Vaughn - Stenographer Billy Bakewell - “Ink” Donovan 

(cast list as per American Film Institute database 

==See also==
* List of rediscovered films

==References==
 

==External links==
*  
*  
* 

 
 
 