The Adventures of Arsène Lupin
 
{{Infobox film
| name           = The Adventures of Arsène Lupin
| image          = Les Aventures dArsène Lupin.jpg
| caption        = Theatrical release poster
| director       = Jacques Becker
| producer       = Robert Sussfeld
| writer         = Jacques Becker Maurice Leblanc Albert Simonin
| starring       = Robert Lamoureux
| music          = Jean-Jacques Grünenwald
| cinematography = Edmond Séchan
| editing        = Geneviève Vaury
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = France Italy
| language       = French
| budget         = 
}}
The Adventures of Arsène Lupin ( ) is a 1957 French crime film directed by Jacques Becker. It was entered into the 7th Berlin International Film Festival.    It was followed by Signé Arsène Lupin.

==Cast==
* Robert Lamoureux as André Larouche / Arsène Lupin / Aldo Parolini
* Liselotte Pulver as Mina von Kraft
* O. E. Hasse as Kaiser Wilhelm II
* Daniel Ceccaldi as Jacques Gauthier
* Georges Chamarat as Inspecteur Dufour
* Huguette Hue as Léontine Chanu
* Renaud Mary as Paul Desfontaines
* Sandra Milo as Mathilde Duchamp Paul Muller as Rudolf von Kraft
* Henri Rollan as Le Président du Conseil Emile Duchamp
* Margaret Rung as The English woman
* Charles Bouillaud as Otto
* Hubert de Lapparent as Jewellery salesman
* Pierre Stéphen as Clérissy
* Jacques Becker as The kronprinz

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 