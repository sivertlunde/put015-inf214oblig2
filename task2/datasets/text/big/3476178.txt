The Cave of the Yellow Dog
{{Infobox Film
 | name = The Cave of the Yellow Dog
 | image = TheCaveoftheYellowDog.jpg
 | 
 | director = Byambasuren Davaa
 | producer = Stephan Schesch
 | writer = Byambasuren Davaa
 | starring = Urjindorjyn Batchuluun  
Buyandulamyn Daramdadi  
Batchuluuny Nansal  
Batchuluuny Batbayar
 | music = Dagvan Ganpurev
 | cinematography = Daniel Schoenauer
 | released = 2005
 | runtime = 93 minutes (US)
 | country = Mongolia, Germany
 | language = Mongolian
}} Mongolian Шар German film written and directed by Byambasuren Davaa. The film was submitted as Mongolias contender for the 2005 Academy Award for Best Foreign Language Film. It won the 2006 Deutscher Filmpreis Award for Best Childrens Picture. 

The story is a gentle fable about the limitations of life and its acceptance.  A girl learns the painful lesson of letting go of want and desire when her father insists on leaving her newfound stray dog.  However, the ending of the film offers hope—another lesson of life being full of changes and the consequences of change may bring unexpected rewards.

==Plot==
The story opens with Nansal returning from boarding school to her family. The family of five lives in a yurt and lives off of their livestock, which include sheep, goats, and cattle. Nansals father is worried about his familys survival because of the wolves that have been attacking their herd.

While Nansal is out collecting dung, she stumbles across a cave in which she finds a black and white dog. She brings the dog home and names it "Zochor" (Spot). Her father is worried, knowing that wolves live in caves and may follow its scent and kill their livestock.
 pelts of the sheep killed by wolves.  He instructs his wife to get rid of the dog before he is home. Nansal is sent out to graze the herd, but she is distracted and gets lost. The mother is distraught when the herd comes back without Nansal and goes out looking for her.

Meanwhile Nansal finds refuge in the yurt of an elderly woman. The old lady feeds Nansal and gives her shelter while a storm passes. It is here that Nansal hears the story of the Cave of the Yellow Dog. In this story, a yellow dog is trapped in a cave with no exit by a man to cure his daughters illness.

The mother finds Nansal soon after and takes her home. Zochor is still with the family when the father returns home. He is angry but still gives gifts to his wife and children nonetheless, including a plastic ladle and a flashlight (torch). The father tries to sell Zochor to some wolf hunters, but Nansal tells them she found him in a cave and the deal falls through.

It is time for the family to move on. They pack up all of their belongings and the yurt and load them onto carts to be pulled by their cattle. The three children are put onto the wagons, with Nansal watching her younger brother. Zochor is tied to a stake so he cannot follow them. Nansal is distracted by Zochor and does not watch her brother. Her brother escapes.

The family travels several miles until they realize that their son is not with them. The father turns back immediately and rushes back on his horse. Meanwhile their son is rushing towards a flock of vultures. He ventures near a stream, while moving further and further from Zochor. When the son is right next to the flock, Zochor breaks free and scares them away.  This is witnessed by the father who, in gratitude for protecting his son from harm, accepts Zochor into the family.

In the final scene, the familys wagons travel down the road, with Zochor in the wagon with Nansal and a truck driving down the road blaring reminders to vote in the upcoming elections.

==References==
 
 

==External links==
*  

 
 
 
 
 