The Taking of Deborah Logan
{{Infobox film
| name           = The Taking of Deborah Logan
| image          = The_Taking_of_Deborah_Logan.png
| border         =
| alt            = 
| caption        = Theatrical release poster
| director       = Adam Robitel
| producer       = {{plainlist|
* Jeff Rice
* Bryan Singer
}}
| writer         = {{plainlist|
* Gavin Heffernan
* Adam Robitel
}}
| starring       = {{plainlist|
* Jill Larson
* Anne Ramsay
* Michelle Ang
* Ryan Cutrona
}}
| music          = Haim Mazar
| cinematography = Andrew Huebscher
| editing        = {{plainlist|
* Gavin Heffernan
* Adam Robitel
}}
| studio         = 
| distributor    = {{plainlist|
* Eagle Films
* Millennium Entertainment
}}
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English 
| budget         = 
| gross          = 
}}

The Taking of Deborah Logan is a 2014 independent American horror film and the feature film directorial debut of Adam Robitel, written by Robitel and co-writer Gavin Heffernan. The film stars Jill Larson, Anne Ramsay, and Michelle Ang.  Set in Virginia, it tells the story of a documentary crew making a film about Alzheimers disease|Alzheimers patients who uncover something sinister while documenting a woman who suffers from the disease.  The film was produced by Jeff Rice and Bryan Singer and was released on October 21, 2014.

==Plot==
Mia, Gavin, and Luis are a documentary team set to create a documentary about Deborah, an elderly woman suffering from Alzheimers disease. Deborah is reluctant to be filmed, but agrees to the project after her daughter Sarah reminds her that they need the money to keep the house from being repossessed. While filming, Sarah and Deborah talk about earlier years when Deborah worked as a switchboard operator for her own answering service business to make ends meet.

Deborah is shown to exhibit increasingly bizarre actions that her personal physician, Dr. Nazir, states are normal for someone with an aggressive form of Alzheimers. However, cameraman Luis begins to notice that several of Deborahs actions defy normal explanations and expresses concern that something supernatural is occurring. Things grow more tense after Luis and Gavin record audio of Deborah speaking in French while sitting at her old switchboard, talking about sacrifices and snakes. They also notice that the line for 337 continually rings and discover that the line belonged to local physician Henry Desjardins, who disappeared after a series of cannibalistic ritualized murders of four young girls. This information is too much for Gavin and he quits. Deborahs behavior becomes so extreme that she is hospitalized for her own safety.

Mia and the others discover that Desjardins was supposedly trying to re-create an ancient demonic ritual that would make him immortal but required the deaths of five girls that recently had their first period. They question whether Deborah is possessed by Desjardins; a similar case in Africa where a mother was possessed by her dead son was only freed when a witch doctor burned the sons corpse. At the hospital, Harris visits Deborah, who begs Harris to kill her. He tries to comply with her wishes, but is unsuccessful due to the entity within Deborah preventing it. Sarah, Mia, and Luis discover that Deborah had unsuccessfully tried to abduct Cara, a young cancer patient in whom she had previously shown interest. Sarah learns that years ago, Deborah had learned that Desjardins planned to use Sarah for his fifth victim and had murdered the doctor before he could accomplish this, and buried his body in the yard. The group eventually finds the body and tries to burn it, but is unsuccessful.

Deborah succeeds in abducting Cara and taking her to the location where Desjardins had killed all of his previous victims. Sarah and Mia find Deborah just as shes trying to eat Caras head in a snake-like manner. They manage to burn Desjardins corpse. The film then cuts to news footage of reporters stating that Deborah was deemed unfit to stand trial for the crimes she committed during her abduction of Cara. An additional news story shows that Cara has overcome her cancer and is celebrating her birthday. As the reporter begins to wrap up the story, Cara turns to the camera and gives a creepy smile, hinting that Desjardins ritual was completed and that he is now in control of her body.

==Cast==
* Jill Larson as Deborah Logan
* Anne Ramsay as Sarah Logan
* Michelle Ang as Mia Medina
* Ryan Cutrona as Harris
* Anne Bedian as Dr. Nazir
* Brett Gentile as Gavin
* Jeremy DeCarlos as Luis
* Tonya Bludsworth as Linda Tweed
* Julianne Taylor as Cara
* Kevin A. Campbell as Henry Desjardins
* Jorge A. Lazaro as Priest

==Production==
The filming took place in Charlotte Region, North Carolina and Creative Network Studios. Additional stunt double were used in place of Mia, Deborah & others. Vincent Guastini did the special effects.  He said that he was impressed with the script and wanted to work with the filmmakers.  He described his relationship with Robitel as "truly a collaborative effort".  He agreed with Robitels choice to limit the amount of footage seen of the transformation, as he felt this "added more believability and truth". 

==Reception== found footage done right... one of the most effective entries in the popular subgenre"  while The Wrap named it a "Netflix Horror Gem", calling the film "one of the freshest entries in American possession horror".  Nerdist Industries|Nerdist and Dread Central both wrote mostly positive reviews for the film,  with Dread Central commenting that while the film would not appeal to everyone, "if youre looking for a spooky little film thats free of pretension and sometimes logic and are into getting some quick shivers, you really cannot go wrong".  In contrast, Bloody Disgusting panned the film, stating that it was too overly derivative of other similarly themed works to be truly effective and that other than a specific scene during the end, "the film brings nothing new to the table". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 