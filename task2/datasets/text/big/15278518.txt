The Hexer (film)
 
 
{{Infobox film
| name           = Wiedźmin (The Hexer)
| image          = Wiedzmin plakat.jpg
| image_size     = 215px
| alt            = 
| caption        = Release poster
| director       = Marek Brodzki
| producer       = Paweł Poppe Lew Rywin
| writer         = Michał Szczerbic   
| starring       = Michał Żebrowski Zbigniew Zamachowski Maciej Kozłowski
| music          = Grzegorz Ciechowski
| cinematography = Bogdan Stachurski
| editing        = Wanda Zeman
| studio         = Heritage Films
| distributor    = Vision Film Distribution
| released       =  
| runtime        = 130 minutes
| country        = Poland
| language       = Polish
| budget         = Polish złoty|zł 18,820,000 (United States dollar|USD$4.6 million )
}}
Wiedźmin (The Hexer or The Witcher in English) is a 2001 fantasy film by director Marek Brodzki, starring Michał Żebrowski as main character Geralt of Rivia. The story is based on the books and stories of The Witcher written by Polish author Andrzej Sapkowski.

The film is essentially the then-unreleased   now; I cannot utter swear words." 
 The 13-episode TV series came out the following year. The series was much more coherent than the confusing film, but was still considered a failure mostly due to the films already bad reputation and due to vast changes to the source material.   However, critics praised acting and music by Grzegorz Ciechowski. 

==Plot==
The TV series and the film were loosely based on Andrzej Sapkowskis book series The Witcher (Wiedźmin).

==Cast==
* Michał Żebrowski as Geralt of Rivia
* Zbigniew Zamachowski as Dandelion
* Maciej Kozłowski as Falwick
* Tomasz Sapryk as Dermot Marranga
* Kinga Ilgner as Renfri
* Grażyna Wolszczak as Yennefer
* Ewa Wiśniewska as Calanthe
* Andrzej Chyra as Borch Three Jackdaws
* Anna Dymna as Nenneke
* Agata Buzek as Pavetta
* Jarosław Boberek as Yarpen Zigrin
* Dorota Kamińska as Eithne
* Wojciech Duryasz as Old Witcher
* Józef Para as Druid of Kaer Morhen
* Daniel Olbrychski as Filavandrel

==See also==
 
* The Witcher – the series of fantasy short stories
* List of The Witcher characters - Characters from The Witcher The Witcher – the computer game

==References==
 

==External links==
*  
*  
*  
*    
*  
*   at the Witcher Wiki

 

 
 
 
 
 
 
 
 
 

 