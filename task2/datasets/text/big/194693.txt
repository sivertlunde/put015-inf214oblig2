Spy Kids 3-D: Game Over
{{Infobox film
| name           = Spy Kids 3-D: Game Over
| image          = Spy Kids 3-D movie poster.jpg
| caption        = Theatrical release poster
| director       = Robert Rodriguez
| producer       = {{Plainlist|
* Elizabeth Avellan
* Robert Rodriguez }}
| writer         = Robert Rodriguez
| starring       = {{Plainlist|
* Antonio Banderas
* Carla Gugino
* Alexa Vega
* Daryl Sabara
* Ricardo Montalbán
* Holland Taylor
* Mike Judge
* Cheech Marin
* Sylvester Stallone }}
| music          = Robert Rodriguez
| cinematography = Robert Rodriguez
| editing        = Robert Rodriguez
| studio         = {{Plainlist|
* Dimension Films
* Troublemaker Studios }}
| distributor    = Buena Vista Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $39 million   
| gross          = $197 million 
}}

 
Spy Kids 3-D: Game Over (also known as Spy Kids 3: Game Over) is a 2003 American  , was released on August 19, 2011.

==Plot== stopping Donnagon from activating the EMP device, has retired from the OSS, no longer wanting to work. He now lives a quiet life and works as a private detective, although his salary is very little. One day, he is contacted by President Devlin (George Clooney), former head of OSS, who informs him that his sister, Carmen Cortez (Alexa Vega), is missing after a mission gone wrong. Upset about this, Juni returns to the OSS to learn more about his sister.

Arriving at the technological and computer part of the OSS, he is reunited with Donnagon Giggles (now reformed) and his wife Francesca (Salma Hayek), who explain that Carmen was captured by the Toymaker (Sylvester Stallone), a former OSS informant. The Toymaker was imprisoned in cyberspace by the OSS, but he has since created Game Over, a virtual reality-based video game which he intends on using to escape cyberspace via players that reach Level 5, which is literally unwinnable. Juni agrees to venture into the game, save Carmen, and shut down the game, given only 12 hours to win. He is also informed that his sister was last seen on Level 4.

In the game, which takes place in a full 3D environment, Juni finds the challenges difficult, having only 9 lives within the game and already losing one at the start. While roaming a cartoon-like medieval village, he finds three beta-testers, Francis (Bobby Edner), Arnold (Ryan Pinkston) and Rez (Robert Vito), who provide him with a passage to the Moon and launch him into space, but mostly to get rid of the competition.

Juni lands on the Moon, losing another life at the process, and receives an opportunity to bring in a fellow ally to assist him, selecting his wheelchair-bound grandfather Valentin (Ricardo Montalbán). He receives a power-up which gives him a robotic bodysuit, allowing him to walk and possess superhuman strength and durability, and abandons Juni, telling him that they will regroup later. Searching for the entrance to Level 2, Juni ventures into a robot battle arena where he fights a girl named Demetra (Courtney Jines) in order to return to Earth and Level 2. In the fight, he receives a robotic, more powerful suit, and he is placed on a huge mecha to combat Demetra. In the 3-round fight, in which he loses 2 more lives, he defeats Demetra and returns to Earth.

He meets the beta-testers again who believe he is a player named "The Guy", who can supposedly beat Level 5. Rez is unconvinced and challenges Juni to a "Mega-race" involving a multitude of different vehicles, which will allow them on Level 3. The only apparent rule of this game is "Win, at all costs." Juni wins the race with help from Valentin, and Demetra joins the group; she and Juni display romantic feelings for each other, with him giving her a med-pack with extra lives and she provides him with an illegal map of the game. Upon entering level 3, Arnold and Juni are forced to battle each other, the loser getting an immediate game over. During the fight, Juni loses almost all of his lives, but Demetra swaps places with Juni and is defeated, seemingly getting a game over, much to Junis sadness.

The group get to Level 4 where Juni finds Carmen, released by the Toymaker, who leads the group on. Juni follows a map to a lava-filled gorge. The group surf their way through the lava but Donnagon attempts to prevent them from reaching Level 5 to save them, but this fails, as they fall into the lava and discover that lava does not mean the game over, but that it is actually possible to swim, and they reach a cavern where they find the door to Level 5. Outside the door to Level 5, where Carmen informs them that they only have 5 minutes left and after the other gamers start to think that Carmen and Juni are deceivers and Rez threatens to give Juni a game over, the real "Guy" (Elijah Wood) appears and opens the door. However, he is struck by a lightning, which makes him lose all of his 100 lives, forcing the group to move on.

In the Level 5 zone, which is a purple-ish cyberspace, Demetra then appears, claiming to have gotten back into the game via a glitch but Carmen identifies her as "The Deceiver", a program used to fool players. Demetra confirms this and apologizes to a stunned Juni before the Toymaker attacks the group with a giant robot. Valentin then appears, holding the entrance back to the real world open so the group can escape. However, he cannot come with them since someone needs to hold the door open. Demetra, shedding a tear, quickly holds the door open so he can go with them. After their return though, it is revealed that Valentin released the Toymaker, with the villains army of robots now attacking a nearby city.

Juni and Carmen summon their family members: Parents Gregorio and Ingrid, Gregorios brother Machete, their Grandma, and Uncle Felix. With too many robots to handle, Juni calls out to their "extended" family (or "everyone", as Juni puts it), summoning characters from the first two films (including Fegan Floop and Alexander Minion, Dinky Winks and his son, scientist Romero (plus a Spork), and Gary and Gerti Giggles). All the robots are destroyed except for the Toymakers. Valentin confronts Sebastian the Toymaker and forgives him for putting him in his wheelchair, which he had been trying to find him to do all those years. The Toymaker shuts down his robot and joins the rest of the Cortez family and their friends in celebrating their family.

==Cast==
* Daryl Sabara as Juni Cortez
* Alexa Vega as Carmen Cortez
* Sylvester Stallone as Sebastian the Toymaker the Creator of Game Over and former OSS agent who doublecrossed Valentin and cost him his legs
* Antonio Banderas as Gregorio Cortez
* Carla Gugino as Ingrid Cortez
* Ricardo Montalbán as Valentin Avellan
* Holland Taylor as Helga Avellan
* Mike Judge as Donnagon Giggles
* Salma Hayek as Francesca Giggles
* Courtney Jines as Demetra
* Ryan Pinkston as Arnold (as Ryan James Pinkston)
* Bobby Edner as Francis
* Robert Vito as Rez
* George Clooney as Devlin, the President of the United States
* Cheech Marin as Felix Gumm Isador "Machete" Cortez
* Emily Osment as Gerti Giggles
* Matt OLeary as Gary Giggles
* Alan Cumming as Fegan Floop
* Tony Shalhoub as Alexander Minion
* Steve Buscemi as Romero
* Bill Paxton as Dinky Winks
* Elijah Wood as The Guy
* George Hurst as Uncle Gomez James Paxton as Little Dink/Dinky Winks Jr.
* Camille Chen as Processor
* Selena Gomez as Waterpark Girl

==Soundtrack==
{{Infobox album
| Name        = Music from the Motion Picture Spy Kids 3-D: Game Over
| Type        = Soundtrack
| Artist      = Robert Rodriguez
| Released    = July 22, 2003
| Genre       = Soundtrack, pop rock
| Length      = 47:15
| Label       = Milan Records
| Chronology  = Robert Rodriguez film soundtrack
| Last album  =   (2002)
| This album  = Spy Kids 3D: Game Over (2003) Once Upon a Time in Mexico (2003)
}}

{{Album reviews rev1 = AllMusic rev1score =     rev2 =   rev2score =   rev3 =   rev3score =  
}}

The film score was composed by Robert Rodriguez and is the first score for which he takes solo credit. Rodriguez also performs in the "Game Over" band, playing guitar, bass, keyboard and drums, including the title track, "Game Over", performed by Alexa Vega. 

===Track listing===
All selections composed by Robert Rodriguez and performed by Texas Philharmonic Orchestra, conducted by George Oldziey and Rodriguez.

#"Game Over" (vocals by Alexa Vega)
#"Thumb Thumbs"
#"Pogoland"
#"Robot Arena"
#"Metal Battle"
#"Toymaker"
#"Mega Racer"
#"Programmerz"
#"Bonus Life"
#"Cyber Staff Battle"
#"Tinker Toys"
#"Lava Monster Rock"
#"The Real Guy"
#"Orbit"
#"Welcome to the Game"
#"Heart Drive" (performed by Bobby Edner and Alexa Vega)
#"Game Over (Level 5 Mix)" (performed by Alexa Vega)
#"Isle of Dreams (Cortez Mix)" (performed by Alexa Vega)
#*Tracks 17–18 produced by Dave Curtin for DeepMix.

==Release==

===Critical reception=== Razzie Award Worst Supporting Actor (Sylvester Stallone).The reason the characters were in minor roles and cameos was because Rodriguez was filming Once Upon a Time in Mexico while writing the third Spy Kids film. 

===Box office===
The film opened with a surprising $33.4 million, but did not quite live up to the first Spy Kids film. In the end, it grossed $111 million in North America. However, its overseas intake was double that of either of the first two Spy Kids films at $85.3 million, grossing a worldwide total of $197,011,982, making it the highest grossing film in the series. The films 3D effect was not removable on the DVD, but a 2D version (Titled Spy Kids 3: Game Over) was available to view on a second DVD disc, and on television airings. Some international retailers included sets of 3D glasses made of cardboard with the film, although most did not.

===Blu-ray version===
The 2D version of the film was released on Blu-ray August 2, 2011.  On December 4, 2012 Lionsgate released the 3D version as a double feature with The Adventures of Sharkboy and Lavagirl on Blu-ray Disc#Blu-ray 3D|Blu-ray 3D. 

==Sequel==
A sequel,  , was released on August 19, 2011. The story revolves around two twins who cannot get along with their stepmother Marissa Cortez Wilson who married their father, Wilbur, a spy-hunting reporter. However unbeknownst to them, Marissa is a retired spy for the OSS (Organization of Super Spies) which has since become the worlds top spy agency and former headquarters of the now-defunct Spy Kids division.

==References to pop culture==
*When Francesca Giggles shows Juni the cover for Game Over, at the top left corner, it says "Vbox". It could be a reference to the video gaming brand Xbox.
*Early on, when Juni enters the game, the other players are collecting coins scattered about, just like Mario. AMD poster can be seen just above the tunnel. Mega-Race itself is a very obvious shoutout to F-Zero, even being described using the series description of the titular race as "the fastest, most dangerous race in the world".
*At the end of the "Mega-Race" scene, Juni mentions Atari, Sega and "Nendo" (reference to Nintendo) when he claims Valentin is "the Wizard of Atariseganendo".
*In the battle between Juni and Arnold, they both use swords similar to lightsabers from Star Wars
*Just before reaching the volcano, the characters cross a continuously changing bridge made from what seem to be Tetris blocks.
*Video game series Halo (series)|Halo and Metroid are mentioned by name just before the lava surfing scene, cited as games with no lava (getting technical about Metroid having molten magma).
*At the door to the final level of the game, Rez threatens Juni, prompting Carmen to reply, "Youll have to go through me first, Game Boy."

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 