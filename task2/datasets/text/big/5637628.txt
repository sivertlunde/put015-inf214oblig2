Art of the Devil 2
{{Infobox film
| name           = Art of the Devil 2 (Long Khong)
| image          = Artofthedevil2poster.jpg
| caption        = One of five Thai film posters. 
| director       = Pasith Buranajan  
Kongkiat Khomsiri  Isara Nadee Seree Phongnithi Yosapong Polsap Putipong Saisikaew Art Thamthrakul  
| producer       = Charoen Iamphungporn
| writer         = Kongkiat Khomsiri Yosapong Polsap Art Thamtrakul 
| starring       = Napakpapha Nakprasitte Hataiwan Ngamsukonpusit Akarin Siwapornpitak Chanida Suriyakompon Namo Tongkumnerd Pavarit Wongpanitch
| music          = Wutichai Praiwan
| cinematography = 
| editing        = Sunij Asavinikul
| distributor    = Five Star Production
| released       = December 1, 2005
| runtime        = 100 minutes
| rating         = 18PL  (Malaysia) 
| country        = Thailand
| awards         =
| language       = Thai
| budget         = 
}}
Art of the Devil 2 (  directed by Kongkiat Khomsiri, Art Thamthrakul, Yosapong Polsap, Putipong Saisikaew, Isara Nadee, Pasith Buranajan and Seree Pongniti (known collectively as the "Ronin Team"). It was released by Five Star Production.

A sequel in name only to a 2004 film (Khon len khong), this film is about a teacher, named Aajaan Panor (portrayed by Napakpapha Nakprasitte), who is humiliated by some students. She turns to black magic to exact revenge.

==Plot==
Two years ago, six school chums - Ta, Kim, Por, Tair, Noot, and Ko - are faced with a grudge. Tas mother died when he was young, and his father remarried Tas teacher, Miss Panor. Kim was his girlfriend (but is now dating Por). The boyish Tair and stylish Noot might be lovers (though this is never made clear), and Ko is their fun-loving friend.

During their last year at school, Miss Panor seduces Por (though this is not known to his friends). Por subsequently discovers hes not the only one in her bed. In addition to being married to Tas father, Miss Panor is also having an affair with the sports coach. Fuelled by jealousy, Por suggests filming Panor and the coach to prove her infidelity to Tas father. The friends, save for Kim, do so, and broadcast it to the entire school. The coach soon discovers it was them and holds the group at gunpoint while he sexually abuses them.

Seeking revenge, the students approach a monk who agrees to curse the coach. A few days later, Por goes back to the monk and asks him to curse Miss Panor, too. The coach dies in which numerous fish hooks appear from his body. Miss Panor, who is embarrassed at her sexual exploits being revealed, is found stabbing herself repeatedly in the legs. She subsequently becomes a recluse, returning home to her cottage on the river, away from the city.

In the present, the six friends gather together after Tas father commits suicide in order to head to the remote village and pay their respects to Miss Panor. Upon arrival at Panors cottage on the river, Noots cell phone rings; her uncle wants her to return, so she makes her goodbyes and heads back to the city. Miss Panor is a polite hostess (if a little distant), and Tas great-grandmother seems harmless enough, though he warns everyone that she is a bit senile.

Miss Panor retires to a secret hut in the jungle, where she has several corpses gathered round, all of them sitting at desks, like students. Each corpse has a photo attached to it, indicating which former student it represents.

At dinner that night, as the friends eat the soup Panor has made for them, each student spits out something: Ko spits up a fingernail, Por spits up a piece of tongue with a piercing on it, Kim spits up an eye contact lense and Tair an eyeball. The group realises that the meat in the soup is, in fact, Noot. They search the cottage for Miss Panor. Kim finds a video camera with a tape that shows Miss Panor killing and eating the monk who cursed her, and then rushing towards the camera with a tree branch raised and bringing it down on whomever is holding the camera.

Tair begins to have hallucinations of corpses shambling around everywhere. The group flees the house and sees a light outside. They call to the boat for help, but it drives past them. The driver takes a fatal fall from the boat, which sinks. Kim tumbles into the water and has a vision of Panor and Tas father. In the vision, Panor is shown torturing Tas father, on the premise that if she hobbles him, he cant leave her. When Panor leaves the room, Tas father shoots himself in the head. Kim relates this to the others, and they find the gun. In the same room, there are also some jars filled with pickled mangos. In one of these jars, the group finds Tas dead father.

The group runs to the houses dusty old shrine to hide and pray. Tair has a laughing fit and is temporarily possessed by Miss Panor. Ko threatens Tair with the gun, but suddenly starts to writhe and flail as dozens of salamanders claw their way out of his body, killing him.

The youths now decide to split up: Por and Ta go off into the jungle to find Panor, leaving Kim and Tair sitting outside the shrine, with Tair (now free of possession) still seeing visions of corpses. She panics and runs off, and Kim follows into the dense jungle. The boys hear Kim fall down. They run to find her, but are separated. Ta finds Kim and wrenches a piece of metal from her leg. Alone, Por has creepy visions of Panor. Ta leaves Kim to look for Por. Tair bursts from the jungle, cowering away from her visions. After seeing Kim as a walking corpse and in hysterics, Tair rips out her own eyes.

Por finds Kim, who appears to him as Miss Panor, taunting him about their love affair. Firing the gun at Kim, but Ta rescues her just in time. Por runs off into the jungle and discovers Panor chanting in her secret hut. Por flees, but a vision of Panor appears, hamstrings him, and starts pulling out his teeth; Por crawls away and confesses that he had a love spell placed on Panor causing her to seduce him, became jealous when he found out she was also sleeping with the coach and took part in filming them and then placing a pain-inducing curse on Panor as punishment. Suddenly, his vision clears and he sees Kim and Ta. Kim is repulsed by Pors confession; she and Ta leave Por in the jungle.

Miss Panor arrives, ties up Por, and takes him to her secret hut. She stabs him in the neck with a syringe full of a paralytic agent, then pours boiling water down his throat and slowly burns every inch of his skin with a blowtorch.

In the jungle, Ta is carrying Kim on his back. She begs him to continue without her, but he refuses. The two of them come across a small shrine, on which is hanging Noots bag. Her cell phone rings: its her uncle, wondering where she is.

In the secret jungle hut, Miss Panor is performing CPR on fatally burned Por in order to inflict more torture on him. Meanwhile, police officers converge outside the hut. As Miss Panor starts to use a power drill on Pors head, the officers break in and shoot her.

Kim is rushed to a hospital. She recovers, and Ta comes to visit her. As they chat, the TV in Kims room broadcasts a news show, which reports that the five students who visited Miss Panor are dead. Also discovered in the secret hut was Tas charred corpse.

In horror, Kim turns to see that Ta is actually a badly burned, walking corpse. He never went with them to visit Miss Panor; he was already dead by then. As he pets Kims hand, Ta explains: the video of Panor eating the monk was taken by Tas father, who had threatened divorce after witnessing the tape of her and the coach. For this, Panor tortured him and he killed himself. Panor locked Tas great-grandmother in a closet and left her to starve. It was Tas great-grandmother who had told Panor that in order to break the spells placed on her she had to kill the monk that was paid to perform them and eat his flesh. She also warned Panor that it could drive her insane (it does). It also becomes clear that the coach had also placed a love spell on Panor to instigate their affair just like Por did. Miss Panor was the victim of all these spells. Her attempt to free herself and seek revenge led her into insanity. She tortured Ta in all the ways that will be experienced by his friends. Tas vengeful spirit had lured the group there so they could feel the pain that he went through. He only saved Kim from being killed because he still loves her.

A flashback of their days in school returned. Kim and a classmate had a crush on Ta. Kim makes a bet with her classmate that if Ta doesnt court Kim within a week, she will "offer" Ta to her. Kim was then seen with the monk who appeared earlier, who warns her, "Once you start, it will follow you till you die." She receives a clay doll and thanks the monk.

Kim jumps out of the hospital window to her death, holding the clay doll. Tas burnt form lays beside her declaring his love for her.

==Nominations==
Napakpapha (better known in Thailand as Mamee) was a nominee for best actress for the Bangkok Critics Assemblys 2005 awards. She was also nominated as best supporting actress for the Thailand National Film Awards. This nomination was protested by Mamee and Five Star Production, who asserted that Mamee should have been nominated in the best actress category. Five Star then boycotted the awards ceremony.  

==Film festivals==
* 2006 Bangkok International Film Festival – Selected the audience favorite.
* 2006 New York Asian Film Festival
* 2006 Fantasia Festival

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 