On the Wings of Dreams
{{Infobox Film
| name   = On the Wings of Dreams   স্বপ্নডানায়
| image    = 
| image_size  = 
| caption     = 
| director      =Golam Rabbany Biplob
| producer    =
| writer         =  Golam Rabbany Biplob Anisul Hoque
| starring    = Mahmuduzzaman Babu Rokeya Prachy Fazlur Rahman Babu
| music          =  Bappa Mazumder Golam Rabbany Biplob
| cinematography =  Mahfuzur Rahman Khan
| editing        = Junad Halim
| distributor    =Impress Telefilms
| released       = 2007
| runtime        = 84 minutes 
| country        = Bangladesh
| language       = Bengali
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
| imdb_id        = 1084045
}}
 Bengali film directed by Golam Rabbany Biplob. It was Bangladeshs submission to the 80th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.        

==Cast==
* Mahmuduzzaman Babu – Fazlu   
* Rokeya Prachy – Matka   
* Fazlur Rahman Babu – Siraj  
* Shamima Islam Tusti – Rehana   
* Shoma – Asma
* Golam Rasul Babu
* Momena Choudhury   	   
* Shah Alam Kiran 
* Shamoli

==See also==
 
*Cinema of Bangladesh
*List of submissions to the 80th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 