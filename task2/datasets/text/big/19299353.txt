Tire Trouble
 
{{Infobox film
| name           = Tire Trouble
| image size     =
| image	         = Tire Trouble FilmPoster.jpeg
| caption        = Film poster
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker
| starring       =
| cinematography =
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 short silent silent comedy film directed by Robert F. McGowan.       It was the 21st Our Gang short subject released.

==Plot==
The gang are running their own taxi service, and come across Ernie and Farina delivering laundry to J. William McAllister, the wealthiest man in town. His doctor and his wife have both convinced him that hes sick, but when the kids visit him, they convince him otherwise. They all drive off in the taxi to Emerald Beach and have the time of their lives.

==Notes==
When the television rights for the original silent Pathé Our Gang comedies were sold to National Telepix and other distributors, several episodes were retitled. This film was released into TV syndication as Mischief Makers in 1960 under the title "The Cure". About two-thirds of the original film was included. Deleted scenes from the TV episode include the opening sequence where the Our Gang kids are building their handmade taxi.

==Cast==
===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Mickey Daniels as Mickie
* Allen Hoskins as Farina
* Ernest Morrison as Ernie
* Mary Kornman as Mary

===Additional cast===
* Harry Rattenberry as J. William McAllister
* George B. French as Doctor
* Lyle Tayo as Mme. La Rue
* Noah Young as Officer

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 