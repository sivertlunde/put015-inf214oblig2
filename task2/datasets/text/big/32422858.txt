The Howling: Reborn
{{Infobox film
| name           = The Howling: Reborn
| image          = The_howling_reborn_cover.jpg 
| caption        = 
| director       = Joe Nimziki
| producer       = Kevin Kasha Joel Kastelberg Ernst Etchie Stroh
| writer         = Gary Brandner (novel), Joe Nimziki James Robert Johnston
| starring       = Lindsey Shaw Landon Liboiron Ivana Miličević
| music          = Christopher Carmichael Mark Anthony Yaeger
| cinematography = Benoit Beaulieu
| editing        = James Coblentz
| distributor    = Anchor Bay Entertainment
| released       =   
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
}}
The Howling: Reborn (also known as Howling VIII) is a 2011 werewolf-themed horror film directed by Joe Nimziki and released direct to video on 18 October 2011. 

==Plot==
Kathryn Kidman is attacked and apparently killed by a werewolf. Many years later, her son Will is living with his father and is enrolled in a high school with an advanced security system. His best friend, Sachin, is making a horror film and is planning to broadcast it by hacking into news channels illegally. Will has a crush on Eliana Wynters, but her current boyfriend scares him away.

Later, Eliana invites him to a party where a creature attacks. Will escapes. He later asks Sachin about werewolf lore. Will cuts himself and the wound heals instantaneously, proving that he is a werewolf.

It is revealed that Kathryn is still alive but has become a werewolf. Kathryn kills Wills father, then goes to Wills school to explain Wills werewolf heritage to him. Confronted with this truth, Will saves Eliana from other members of Kathryns wolf pack. The invasion sets off the security system and the school enters automated lockdown mode, cutting off the school from the outside world.

Will remembers Sachins advice that werewolves can only be killed by silver bullets or fire, so he and Eliana equip themselves with homemade flame throwers. They find Sachin just in time to see him killed by a werewolf.  Sneaking off to the basement, they discover that Kathryn is raising an army of werewolves.

They fight their way out of the school. Eliana tries to goad Will to turn her into a werewolf, but he manages to restrain himself.

Kathryn captures Eliana to force Will to become a werewolf. Will fights Kathryn with weapons, but Kathryn is too powerful to be killed by silver bullets. Another werewolf attacks her and rips her heart out. The werewolf is Eliana, who was previously wounded by Will. Eliana and Will burn down the school to kill the other new werewolves.

Will creates a video in which he transforms on camera and warns the world about werewolves.  It is circulated all over the world, and humans prepare to battle against the new threat.

==Cast==
* Lindsey Shaw as Eliana Wynter
* Landon Liboiron as Will Kidman
* Ivana Miličević as Kathryn Kidman
* Jesse Rath as Sachin
* Niels Schneider as Roland
* Frank Schorpion as Jack Kidman
* Kristian Hodko as Tribe

==Production== The Howling II by Gary Brandner, but it has very little in common with the novel.

Production began on the film in Canada in May 2010.  The film was released direct to video in late October 2011. 

Anchor Bay Entertainment released The Howling: Reborn on DVD and Blu-ray in the UK on the 9th April, 2012.

==References==
{{Reflist|refs=
   
}}

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 