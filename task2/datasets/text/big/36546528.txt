Too Many Women (1942 film)
{{Infobox film
| name           = Too Many Women
| image          = 
| image_size     = 
| caption        = 
| director       = Bernard B. Ray
| producer       = Bernard B. Ray (producer)
| writer         = Eddie Davis (story and screenplay)
| narrator       = 
| starring       = See below
| music          = Clarence Wheeler  (uncredited)
| cinematography = Jack Greenhalgh
| editing        = Carl Himm
| studio         = Producers Releasing Corporation
| distributor    = 
| released       = 1942
| runtime        = 67 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Too Many Women is a 1942 American film directed by Bernard B. Ray. It is also known as Girl Trouble and Man Trap.

== Plot summary ==

Unsure of their financial situation, Richard Sutton and Linda Pearson have postponed their plans to marry to when they have a steady income. To be able to turn down a job offer he doesnt like, he pretends to have inherited money from a relative. Problem arises as his grandmother hears of the inheritance and believes it is a particular wealthy uncle Woodrow in Brazil who has thrown in the towel, leaving his $3 Million to Richard. She is also unaware of Richards engagement to Linda.

When Richard wants to take the grandmother out of her misconception, her doctor advises against it, saying the shock could cause her death. Believing her grandson is rich now, she starts campaigning for his engagement to young beautiful Gwenny Miller. Gwenny is the grandmothers ward.

Another wealthy young woman, Barbara Cartwright, tells Richard she has an idea of how he can solve his problems. Richard goes to visit Barbara, but is quite dozy after involuntarily taking sleeping pills. He falls asleep, and when he wakes up again, he is seemingly engaged to Barbara.

Outraged and jealous, Linda breaks off their engagement, and Richard goes on a bender to drown his sorrows. Again he is knocked out, and wakes up in the apartment of infamous playboy Chester Wannamaker. With him in the apartment is a chorus girl named Lorraine OReilly, who really is Chesters fiancé.

Both Barbara and Gwenny soon arrives to the apartment to confront him, and after that also Lorraines brother. The brother believes Richard is Chester and uses a gun to threaten him into marrying his sister.

Later, Richards grandmother and uncle arrive at the apartment, saving him from the wrath of the women and the brother, explaining to them that Richard is poor. Richard reconciles with Linda after explaining the whole misunderstanding. 

=== Differences from source ===
 

== Cast == Neil Hamilton as Richard Sutton
*June Lang as Gwenny Miller
*Joyce Compton as Barbara Cartwright
*Barbara Read as Linda Pearson Fred Sherman as Charlie Blakewell
*Marlo Dwyer as Lorraine OReilly
*Kate MacKenna as Grandmother Sutton
*Maurice Cass as Doctor Hamilton
*Matt McHugh as Spike OReilly
*Harry Holman as John Cartwright George Davis as Bottles Pat Gleason as Gibbons
*Tom Herbert as W. R. Mitchell
*Bertram Marburgh as Uncle Woodrow
*Dora Clement as Mrs. Fairbanks

== Soundtrack ==
 

== External links ==
* 
* 

==References==
 

 
 
 
 
 
 
 