Phantasm (film)
{{Infobox film
| name           = Phantasm
| image          = phantasm.jpg
| alt            = A woman screams and covers her eyes, which are then superimposed over her hands
| caption        = Theatrical release poster
| director       = Don Coscarelli
| producer       = Don Coscarelli
| writer         = Don Coscarelli
| starring       = {{plainlist| Michael Baldwin
* Bill Thornbury
* Reggie Bannister
* Kathy Lester
* Angus Scrimm
}}
| music          = {{plainlist|
* Fred Myrow
* Malcolm Seagrave
}}
| cinematography = Don Coscarelli
| editing        = Don Coscarelli
| studio         = New Breed Productions AVCO Embassy Pictures
| released       =  
| runtime        = 89 minutes  
| country        = United States
| language       = English
| budget         = $300,000   
| gross          = $12 million   
}} Tall Man Michael Baldwin), who tries to convince his older brother Jody (Bill Thornbury) and family friend Reggie (Reggie Bannister) of the threat.

Phantasm was a locally financed   (1988),   (1994), and   (1998).  The last two were released   (2014) was announced. 

==Plot== Tall Man, fortune teller and her granddaughter about the possibility of Jody departing and leaving him in the care of his aunt, along with his suspicions about the Tall Man. Mike is shown a small black box and told to put his hand into it. After the box grips his hand, Mike is told not to be afraid, and, as the panic subsides, the box relaxes its grip. The notion of fear itself as the killer is established, propelling Mike toward his final confrontation with the Tall Man.

Minions of the Tall Man, deceased townspeople who are shrunk down to dwarf size and reanimated, pursue Mike after he investigates further.  After convincing Jody and Reggie, who are initially skeptical of his stories, they find a strange white room with containers in the mausoleum. Mike discovers a gateway to another planet, which he enters briefly; there, he sees the dwarves that have hunted him being used as slaves. While trying to escape the Tall Man, Reggie is stabbed and appears to die, and Mike and Jody barely escape. They devise a plan to lure the Tall Man into a local deserted mine shaft and trap him inside. After doing so successfully, Mike wakes with a start in his house, lying by the fireplace.

Reggie, who is sitting beside him, tells Mike he was simply having a nightmare, something that has been a common occurrence since Jody died in a car crash. When Mike enters his bedroom, he is shocked to see the Tall Man is waiting behind the door. In the final scene, one of the Tall Mans dwarf minions pulls Mike through his bedroom mirror.

==Cast== The Tall Man:
: After being intimidated by Scrimm on the set of a previous film, Coscarelli decided that Scrimm would make a great villain.     Initially, Scrimm had little input into the character, but he made more of a contribution as Coscarelli began to trust his instincts.  Scrimm was outfitted in lifts and a suit too small for him in order to make him seem even taller and skinnier.  Coscarelli says of Scrimm, "I really didnt have any idea that he would take it to the level that he did. ... I could see it was going to be a very powerful character." 
* A. Michael Baldwin as Mike Pearson:
: After the deaths of his parents, Mike tries to convince his brother and Reggie that a local mortician called the Tall Man is responsible for their deaths.  Coscarelli attributes the enduring popularity of the film to young audiences who respond to Mikes adventures.     After they worked together in a prior film, Coscarelli wrote a film in which Baldwin could star.    
* Bill Thornbury as Jody Pearson:
: Jody is Mikes older brother.  After their parents die, Jody becomes Mikes guardian, but Jody confides in his friends that hes uncomfortable with the responsibility.
* Reggie Bannister as Reggie (Phantasm)|Reggie:
: Don Coscarelli based the character of Reggie on his friend Reggie Bannister, for whom the role was written; they then twisted the character into new directions.     Reggie was designed to be an everyman,  a loyal friend,  and the comic relief. 
* Kathy Lester as Lady in Lavender:
:The Tall Man appears in the form of the Lady in Lavender,  which he uses to seduce and kill Tommy, Jodys friend.  Laura Mann appears as Kathy Lesters double, credited as Double Lavender.

Bill Cone portrays Tommy, Mary Ellen Shaw the fortune-teller, and Terrie Kalbus the fortune-tellers granddaughter.

==Themes==
John Kenneth Muir states that the film is about mourning and death.     Many of the films fans are young boys, aged 10–13. According to Angus Scrimm, the film "gives expression to all their insecurities and fears".  Scrimm states that the theme of loss and how, by fantasizing about death, the young protagonist deals with the deaths in his family drives the story.   Coscarelli identifies it as a "predominately male story" that young teens respond to.   Scrimm explains the popularity of the film as fans responding to themes of death,  and the Tall Man himself represents death.   Muir describes the Tall Man as embodying childhood fears of adults and states that the Tall Man wins in the end because dreams are the only place where death can be defeated.   American views of death are another theme:
 

Dreams and surrealism are also an important part of Phantasm.  Marc Savlov of the Austin Chronicle compares Phantasm to the works of Alejandro Jodorowsky and Luis Buñuel in terms of weirdness.  Savlov describes the film as existentialist horror and "a truly bizarre mix of outlandish horror, cheapo gore, and psychological mindgames that purposefully blur the line between waking and dreaming."   Gina McIntyre of the Los Angeles Times describes the film as surreal, creepy, and idiosyncratic.   Muir writes that Phantasm "purposely inhabits the half-understood sphere of dreams" and takes place in the imagination of a disturbed boy. 

==Production== Something Wicked Invaders from Goblin and Mike Oldfield.  The synthesizers were so primitive that it was difficult to repeat sounds.     When writing the films conclusion, Coscarelli intentionally wanted to shock audiences and "send people out of the theater with a bang."   

There were no accountants on the set, but Coscarelli estimates the budget at $300,000.   Funding for the film came in part from Coscarellis father;    additional funding came from doctors and lawyers.  His mother designed some of the special effects,  costumes, and make-up.   The cast and crew were composed mainly of friends and aspiring professionals.  Due to their inexperience, they did not realize that firing blanks could be dangerous; Coscarellis jacket caught fire from a shotgun blank.  Casting was based on previous films that Coscarelli directed, and he created roles for those actors.   Because he could not afford to hire an editor or cameraman, Coscarelli did these duties himself.  

Filming was done weekends and sometimes lasted for 20 hours over the course of more than a year.   Reggie Bannister described the production as "flying by the seat of our pants".  The actors would be called to perform their scenes and picked up as soon as they were available.      Bannister did many of his own stunts.   Shooting took place primarily in  in the San Fernando Valley in Chatsworth.   The script changed often during production, and Bannister says that he never saw a completed copy of it; instead, they worked scene-by-scene and used improvisation.    The script was characterized by Coscarelli as "barely linear".   While it contained the basic concepts of the completed film, the script was unfocused and rewritten during filming.   The spheres came from one of Coscarellis nightmares, but the original idea did not involve drilling.   Will Greene, an elderly metal-worker, fashioned the iconic spheres, but he never got to see the finished film, as he died before the film was released.   The black 1971 Plymouth Barracuda was used because Coscarelli had known someone in high school who drove one; he realized that he could get his hands on one by using it in the film. 

Post-production took another six to eight months.   The first test screening was a disaster due to the length; Coscarelli says that he erred in adding too much character development, which needed to be edited out.   Phantasm s fractured dream logic was due in part to the extensive editing.   During shooting, they did not have a clear idea of the ending.   Several endings were filmed, and one of them was re-used in  .  Coscarelli attributed the freedom to choose from among these endings to his independent financing. 

===Deleted scenes===
In 1998, MGM re-released Phantasm on VHS and DVD with a newly remastered Dolby stereo soundtrack. Both the VHS and DVD releases included two deleted scenes:
* Mike enters a room with two coffins: one is open with a body inside and the other is closed. Mike hears sounds from inside the second coffin and thinks that Reggie is trapped inside; however, Reggie enters the room while Mike is trying to pry the coffin open. Mike realizes that something unpleasant is in the coffin, and he and Reggie close it. Mike suggests that they find Jody. This scene is not included on the Anchor Bay release.
* Mike and Jody encounter the Tall Man in the funeral home. Jody shoots the Tall Man several times with his shotgun, but it has no effect on him. The Tall Man knocks Mike onto the floor and picks up Jody by the neck with one hand. Mike sees a fire extinguisher and remembers that the Tall Man reacted badly when he passed by Reggies Ice Cream truck with its refrigerator open. Realizing the Tall Man can be hurt by cold, Mike takes out the fire extinguisher and blasts the Tall Man with it. The Tall Man writhes in pain and screams, then his head explodes.

==Release== MGM released Phantasm on laserdisc in November 1995  and on DVD in August 1998.   Anchor Bay Entertainment re-released it on DVD on April 10, 2007. 

==Reception and legacy== Time Out called the film "a surprisingly shambolic affair whose moments of genuine invention stand out amid the prevailing incompetence."   Dave Kehr of the Chicago Reader described it as "spotty" and "effective here and there", though he praised Coscarellis raw ability.   Vincent Canby of the New York Times compared it to a ghost story told by a bright, imaginative 8-year-old; he concluded that it is "thoroughly silly and endearing".   Kim Newman of Empire (magazine)|Empire called it "an incoherent but effective horror picture" that "deliberately makes no sense" and rates it four out of five stars.   Variety (magazine)|Variety gave it a positive review that highlighted the use of both horror and humor.   Scott Weinberg of Fearnet stated the acting is "indie-style raw" and special effects are sometimes poor, but the originality and boldness make up for it.   Steve Barton of Dread Central rated it five out of five stars and said the film is a masterpiece and "one hell of a scary film".   Bloody Disgusting rated the film four out of five stars and said the film is "truly original" and "imbues in its viewers is a profound sense of dread".   John Kenneth Muir called the film striking, distinctive, and original.  Muir stated that the film has become a classic, and the Tall Man is a horror film icon. 
 Time Out Londons 100 best horror films.   Drive-in movie critic Joe Bob Briggs included it at #20 in his 25 Scariest DVDs Ever list.   UGO placed the film (and the Tall Man) at #7 out of 11 in its Top Terrifying Supernatural Moments.   Phantasm has become a cult film;   Coscarelli attributes its cult following to nostalgia and its lack of answers, as repeated viewings can leave fans with different interpretations.   USA Today described three characteristics that make it a cult film: "the touching portrayal of two brothers in danger, an iconic villain in The Tall Man (Angus Scrimm) and a floating metallic sphere thats a death-dealing weapon." 
 Rue Morgue, as stating that Supernatural (U.S. TV series)|Supernatural, A Nightmare on Elm Street (1984), and One Dark Night (1983) were all influenced by Phantasm. 

===Awards===
* Don Coscarelli won the Special Jury Award in 1979 at the Avoriaz Fantastic Film Festival and the film was nominated for the Saturn Award for Best Horror Film in 1980.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 