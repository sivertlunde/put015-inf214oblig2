The Egyptian (film)
{{Infobox film
| name           = The Egyptian
| image          = Theegyptian.jpg
| caption        = Theatrical release poster
| director       = Michael Curtiz
| producer       = Darryl F. Zanuck
| based on       =   Philip Dunne Casey Robinson Michael Wilding Bella Darvi Peter Ustinov Tommy Rettig Alfred Newman
| cinematography = Leon Shamroy
| editing        = Barbara McLean
| distributor    = 20th Century Fox
| released       = 24 August 1954
| runtime        = 139 minutes
| country        = United States
| language       = English
| budget         = $3.9 million 
| gross          = $4.25 million (US rentals)  
}}
 novel of Philip Dunne Michael Wilding. Cinematographer Leon Shamroy was nominated for an Academy Award in 1955.

==Plot== Michael Wilding). He rises to and falls from great prosperity, wanders the world, and becomes increasingly drawn towards a new religion spreading throughout Egypt. His companions throughout are his lover, a shy tavern maid named Merit (Jean Simmons), and his corrupt but likable servant, Kaptah (Peter Ustinov).
 epileptic seizure, monotheistic worship of the sun, referred to as Aten. Akhnaton intends to promote Atenism throughout Egypt, which earns him the hatred of the countrys corrupt and politically active traditional priesthood.

Life in court does not prove to be good for Sinuhe; it drags him away from his previous ambition of helping the poor while falling obsessively in love with a Babylonian courtesan named Nefer (Bella Darvi). He squanders all of his and his parents property in order to buy her gifts, only to have her reject him nonetheless. Returning dejectedly home, Sinuhe learns that his parents have committed suicide over his shameful behavior. He has their bodies embalmed so that they can pass on to the afterlife, and, having no way to pay for the service, works off his debts in the embalming house.

Lacking a tomb in which to put his parents mummies, Sinuhe buries them in the sand amid the lavish funerary complexes of the Valley of the Kings. Merit finds him there and warns him that Akhnaton has condemned him to death; one of the pharaohs daughters fell ill and died while Sinuhe was working as an embalmer, and the tragedy is being blamed on his desertion of the court. Merit urges Sinuhe to flee Egypt and rebuild his career elsewhere, and the two of them share one night of passion before he takes ship out of the country.

For the next ten years Sinuhe and Kaptah wander the known world, where Sinuhes superior Egyptian medical training gives him an excellent reputation as healer. Sinuhe finally saves enough money from his fees to return home; he buys his way back into the favor of the court with a precious piece of military intelligence he learned abroad, informing Horemheb (now commander of the Egyptian army) that the barbarian Hittites plan to attack the country with superior iron weapons.

Akhnaton is in any case ready to forgive Sinuhe, according to his religions doctrine of mercy and pacifism. These qualities have made Aten-worship extremely popular amid the common people, including Merit, with whom Sinuhe is reunited. He finds that she bore him a son named Thoth (Tommy Rettig) (a result of their night together many years ago), who shares his fathers interest in medicine.
 Baketamun (Gene Tierney); she reveals that he is actually the son of the previous pharaoh by a concubine, discarded at birth because of the jealousy of the old queen and raised by foster parents. The princess now suggests that Sinuhe could poison both Akhnaton and Horemheb and rule Egypt himself (with her at his side).
 future generations spread the same faith better than he.

Enlightened by Akhnatons dying words, Sinuhe allows Horemheb to become pharaoh as he is still indignant that his old friend had considered murdering him and has begun to preaching the same ideals Akhnaton believed in his final moments. Banished to the shores of Red Sea, Sinuhe spends his remaining days inspired by the glimpse of another world he has been afforded through Akhnaton and died of old age after writing down his life story in hopes that it may be found by Thoth or his descendants. The film concludes with a caption reading, "These things happened thirteen centuries before the birth of Jesus Christ".

==Cast==
* Edmund Purdom as Sinuhe
* Victor Mature as Horemheb
* Jean Simmons as Merit
* Bella Darvi as Nefer Baketamon
* Michael Wilding as Akhenaten
* Peter Ustinov as Kaptah Taia
* Henry Daniell as Mekere
* John Carradine as Grave robber
* Carl Benton Reid as Senmut
* Tommy Rettig as Thoth
* Anitra Stevens as Queen Nefertiti
* Peter Reynolds as Sinuhe, age 10

==Production==
The script was based on the Waltari novel of the same name. It is elaborated in the book, but not the film, that Sinuhe was named by his mother from The Story of Sinuhe, which does include references to Aten but was written many centuries before the 18th dynasty. The use of the "Cross of Life" ankh to represent Akhnatons "new" religion reflects a popular and esoteric belief in the 1950s that monotheistic Atenism was a sort of proto-Christianity. While the ankh has no known connection to the modern cross,  the principal symbol of Aten was not an ankh but a solar disk emitting rays, though the rays usually ended with a hand holding out an ankh to the worshipers. The sun-disk is seen only twice; when we first meet Akhnaton in the desert, he has painted it on a rock, and Sinuhe says "Look! He worships the face of the sun." It appears again as part of the wall painting above Akhnatons throne. With that said, the ankh was used in the original novel. Likewise, Akhnatons dying revelation that God is much more than the face of the sun is actually found among Waltaris best-known writings. 
 The Ten Commandments. As the events in that story take place seventy years after those in The Egyptian, this re-use creates an unintended sense of continuity. The commentary track on the Ten Commandments DVD points out many of these re-uses. Only three actors, Mimi Gibson, Michael Ansara and John Carradine, and a handful of extras, appeared in both pictures. The Prince Aly Khan was a consultant during filming, he was engaged to Gene Tierney. Marlon Brando was to star as Sinuhe, but did not like the script and dropped out at the last minute.  Farley Granger was the next choice and considered the role, but then decided he was not interested after having just moved to New York.  Dirk Bogarde was then offered the role but also turned it down. Finally it was handed to young up-and-coming contract actor Edmund Purdom.

Marilyn Monroe coveted the role of Nefer, only to discover that it was earmarked for the protegee (mistress) of producer Darryl F. Zanuck, Bella Darvi.  This would be the second of only three American films featuring Darvi, who returned to Europe and later committed suicide.

==Music==
  Alfred Newman and Bernard Herrmann.

Newman would later conduct the score in a re-recording for release on Decca Records.  Musician John Morgan undertook a "restoration and reconstruction" of the score for a recording conducted by William T. Stromberg in 1998, on Marco Polo Records.  The performance of the score recorded for the film was released by Film Score Monthly in 2001.

==See also==
* List of historical drama films
* List of American films of 1954
* List of epic films

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 