Morgenrot (film)
{{Infobox film
| name           = Morgenrot
| image          = Morgenrot-1933-poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Vernon Sewell Gustav Ucicky
| producer       = Gunther Stapenhorst 
| writer         =   )
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Rudolf Forster Fritz Genschow Paul Westermeier Camilla Spira
| music          = Herbert Windt
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Universum Film AG (UFa) 
| released       =  
| runtime        = 75 minutes
| country        = Weimar Republic Nazi Germany
| language       = German
| budget         = 
| gross          = 
}}
 1933 Germany|German submarine film set during World War I.  

Released three days after Adolf Hitler became Reichskanzler, it was the first film to have its screening in Nazi Germany.  It became a symbol of the new times touted by the Nazi regime. 
 German term for the reddish coloring of the east sky about a half hour before the sunrise.  Dawn was the U.S. title.

It was filmed in Kiel, Schleswig-Holstein, the first German submarine movie made after World War I.

==Motifs==
The film offered a heroization of death, with the captain explicitly stating that Germans may not know how to live, but they know how to die.  In a central scene, the captain of the submarine offers to his men that he and the first officer will go down with the ship in order that they may escape; they refuse on the grounds it will be all or none of them, and the captain glorifies the chance to die with such men, a theme that commonly appeared in Nazi-era films.   

The first officer, having learned that the woman he loves is in love with captain, not himself, and another sailor commit suicide to save the others – a common way to resolve love triangles in Nazi films, where the heroic death saves the man   from failure. 

On the other hand, the mother of one man refuses to rejoice over her son because of the suffering of war – a theme that would not appear in Nazi film. 

==Awards==
The National Board of Review of Motion Pictures awarded it with Best Foreign Film for 1933. 

==References==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 
 

 