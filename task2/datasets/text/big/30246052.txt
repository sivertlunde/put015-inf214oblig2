Her Painted Hero
Her Painted Hero is a short 1915 American film directed by F.Richard Jones and written by Mack Sennett.

== Plot summary ==
 
Her Painted Hero is about an heiress who plans to use her new wealth for influence on the stage. The play being shown within the film is entitled "What Sherman Said" a seeming epic about the Civil War.

== Cast ==
*Hale Hamilton as a Matinee Idol Charles Murray as a Property Man
*Slim Summerville as a Bill-Poster
*Polly Moran as a Stage-struck Maiden
*Harry Booker as the Maidens Father

==Production==

Whilst some sources credit production to the Keystone Film Company the logo clearly visible within the film is for the amalgamated company "Keystone Triangle", later to be abbreviated to Tri-stone Pictures.

== External links ==
* 

 
 
 
 
 
 
 
 


 