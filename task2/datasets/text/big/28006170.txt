Gandugali Kumara Rama
{{Infobox Film
| name = Gandugali Kumara Rama 
| image =
| caption = 
| director = H. R. Bhargava
| story = Su. Rudramurthy Shastry
| screenplay = Veerappa Maralavadi   H. R. Bhargava
| producer = Anitha Pattabhiram Rambha  Anitha  Laya
| music = Gurukiran
| cinematography = Sundarnath Suvarna
| editing = P. R. Soundar Raj
| studio = M. S. Ramaiah Chitralaya
| distributor = 
| released =  
| runtime =  159 mins
| language = Kannada  
| country =  
}}
  2006 Indian biographical historical historical drama Laya and Anitha in pivotal roles.  The film was declared an average grosser by box office report.

==Legend==
Kampili in Karnataka was ruled by Devaraaya who had successfully resisted the Yadavas, Seunas, Kakathiyas and Hoysalas. His son Kumara Rama (1290–1320) a great hero was exiled due to the intrigues of his step mother. He belonged to the Nayaka lineage of kings. His brother-in-law was Sangama, the father of the founders of Vijayanagara Empire (Hakka-Bukka).

==Cast==
* Shivarajkumar as Kumara Rama and Channa Rama in dual roles Anitha as Rathna Rambha
* Laya as Ramale Ashok
* Srinivasa Murthy as Devaraya
* Avinash
* Ramesh Bhat Seetha
* Sumithra
* Doddanna
* C. R. Simha
* Suchitra
* Bhagyashree Kishore
* Dharma
* Aravind
* Karibasavaiah
* Honnavalli Krishna

==Soundtrack==
The soundtrack for the movie is composed by Gurukiran. 

{{Infobox album |   Name        = Gandugali Kumara Rama Type        = Album Artist      = Gurukiran Cover       =  Released    = 2006 Recorded    =  Genre  Feature film soundtrack Length      =  Label       = Akash Audio Producer    =  Reviews     =  Last album  =  This album  =  Next album  = 
}}

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| No. || Song || Singers || Length (m:ss) ||Lyrics 
|- 1 || Kaviraj 
|-
| 2 || Ee Jeeva Jeeva || Udit Narayan, Madhu Balakrishnan || ||   Rudramurthy Shastry 
|- Chitra || ||  MN Vyasarao 
|-
| 4 || Kumara Rama || Shankar Mahadevan || ||  Shyamsundar 
|-
| 5 || Lelepaadi ||  Malathi, Manu || ||  Sriranga 
|-
| 6 || Ginirama || Gurukiran, Komala Pothraj || ||  V. Manohar 
|-
|}

==References==
* History of Karnataka, H V Srinivas Murty
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 