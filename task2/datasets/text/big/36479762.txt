Who Killed Cock Robin? (1935 film)
Who David Hand. Who Killed Cock Robin?.

==Plot==
 "Who Killed Cock Robin? Who caught him with a shot and put him on the spot? Who Killed Cock Robin? And vanished like a phantom in the night? WHO?" 

While Cock Robin (caricatured after Bing Crosby) serenades the Mae West-esque Jenny Wren, an unseen bird shoots an arrow into Cock Robins heart and he falls to the ground, giving the other birds in the tree the impression he has been shot. The police arrive at the scene and arrest a Harpo Marx-like cuckoo, a gangster sparrow (possibly an homage to the story) and a Stepin Fetchit-based blackbird as suspects.

The next day a trial is held over who killed Cock Robin, with a parrot serving as the prosecutor, interrogating the suspects and showing Cock Robins body as evidence. The blackbird confesses he hadnt done it, seen it, or known anything about it. The sparrow refuses to say anything. The cuckoo doesnt know either, continually pointing to the owl judge and the parrot ("Your honor, this birds cuckoo! He dont know a thing!"). Everyone is astounded that no one knows who killed Cock Robin.

At that moment, Jenny Wren arrives and demands that she see justice done for her Cock Robin. Eventually, the judge declares that all three suspects be hanged as they dont know which of them is guilty. Just then, an arrow strikes the judges hat, and its owner, Cock Robins supposed killer, is revealed to be Cupid. He reveals that Robin isnt dead at all; he fell for Jenny Wren and landed on his head, the arrow he was shot with missing his heart. Robin and Jenny then kiss, to the excitement of the jury.

== External links ==
*  

 

 
 
 
 
 
 
 

 