White Death (film)
 
 
{{Infobox film
| name           = White Death
| image          = 
| image_size     = 
| caption        = 
| director       = Edwin G. Bowen
| producer       = Edwin G. Bowen Frank Harvey
| narrator       = 
| starring       = Zane Grey   Alfred Frith
| music          = Isadore Goodman
| cinematography = Arthur Higgins H.C. Anderson
| editing        = Edwin G. Bowen William Carty
| studio         = Barrier Reef Films
| distributor = British Empire Films (Australia) MGM (UK)
| released       = October 1936 (Australia) 1937 (UK)
| runtime        = 81 minutes
| country        = Australia
| language       = English
}} Australian film starring Zane Grey as himself. He filmed it during his a fishing expedition to Australia and it marked the first time he had played a leading role in a movie. 

==Synopsis==
Zane Grey bets he can catch a fish bigger than one he sees at Watsons Bay. He hears about a large shark, nicknamed "white death", terrorising the Queensland coast and goes to catch it. He is thwarted by the comic attempts of Newton Smith, a representative of the Wallanga Branch of Fish Protectors, to persuade Grey not to harm fish. There is also a romance between two young people. Eventually Grey manages to catch the shark.

==Cast==
* Zane Grey as himself
* Alfred Frith as Newton Smith
* Nola Warren as Nola Murchinson
* Harold Calonna as David Murchison
* John Weston as John Lollard
* James Coleman as Professor Lollard
*Peter Williams as boatman
*Frank Big Belt as guard

==Production==
In 1935-36 Zane Grey made a fishing expedition to Australia. This trip was extensively covered by the local media and Grey was often accompanied on his sea voyages by three cameramen he had brought out from American, including H.C. Anderson. Greys activities were criticised at the time by the Royal Society for the Prevention of Cruelty to Animals. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 174. 

Barrier Reef Productions, a production company capitalised at £15,000, was formed in 1936 to make the movie. 
 Bermagui in Frank Harvey was hired to write the screenplay. The story drew on Greys real life experiences with the character of Newon Smith sending up his treatment at the hands of the RSPCA.

The majority of the film crew came from Cinesound Productions, who also lent equipment to the production. Greys manager, Edwin G. Bowen, was appointed director of the movie although he had limited experience behind the camera. 

===Casting===
Alfred Frith, the stage comedian, was hired to play the lead opposite Grey. Nola Warren, a 17 year old from Watsons Bay with no prior film experience, was cast as the female lead.  She performed most of her scenes opposite John Weston, a former school boy athletic champion turned radio broadcaster.  Aboriginal extras, some of whom had recently appeared in Uncivilised (film)|Uncivilised (1936), where brought in from Palm Island, Queensland. Harold Colonna, who played the villain, was best known as an opera singer.

===Shooting===
Filming started in May 1936 and took place in the Great Barrier Reef, principally at Hayman Island. A shark enclosure was built at Hayman to shoot shark footage. 

Bad weather made the shoot difficult. A member of the camera crew sprinkled oil in the surf thinking it would make it the surf sound less loud.  A petrol lamp blew up in John Westons face.  In addition, finding white sharks proved difficult, forcing the props master to construct an artificial one from wood and canvas.  

Both Bowen and Frith were accompanied by their wives who assisted in making the movie, and Bowens young children Buddy and Barbara. 

Location shooting ended in July 1936 and the rest of the film was made at Cinesounds studios in Sydney.

==Reception==
Grey left Australia on 19 August claiming it was the greatest country he had visited.  He reportedly offered Nola Warren a film contract and announced he would return in 1938 to make another film.   Grey did return to Australia in 1939 to fish, shortly before his death, but no further films resulted.  In 1937 he published An American Angler in Australia.

The film premiered in October at Moruya and Batemans Bay, and reached Sydney theatres in November. The critic for the Sydney Morning Herald described the movie as "a rambling and rather ramshackle film... the script... is almost bare of dramatic action." 

The film was released in the UK but does not appear to have been screened commercially in the US.

Barrier Reef Films announced plans to make further feature films, including one revolving around Alfred Firth, but this did not eventuate. 

Nola Warren later became a model and was involved in a  scandalous divorce case. 

==References==
 

==Bibliography==
* Reade, Eric. History and heartburn: the saga of Australian film, 1896-1978. Associated University Presses, 1981.

==External links==
* 
*  at National Film and Sound Archive
*  at Oz Movies
* 
*  at Project Gutenberg Australia

 
 
 
 
 
 