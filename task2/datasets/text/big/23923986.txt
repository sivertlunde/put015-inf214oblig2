Hudson's Bay (film)
{{Infobox film
| name           = Hudsons Bay
| image          =
| image_size     =
| caption        =
| director       = Irving Pichel
| producer       = Darryl F. Zanuck
| writer         = Lamar Trotti
| narrator       =
| starring       = Paul Muni Gene Tierney Laird Cregar Alfred Newman George Barnes J. Peverell Marley Robert L. Simpson
| distributor    = 20th Century Fox
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Hudsons Bay is a 1941 historical drama by 20th Century Fox starring Paul Muni, Gene Tierney, Laird Cregar and Vincent Price as King Charles II. The film is about a pair of French-Canadian explorers whose findings lead to the formation of the Hudsons Bay Company.
 retail stores. 

==Plot==
A trapper, Pierre Esprit Radisson, and his friend, nicknamed "Gooseberry," hope to open a trading post in the Hudsons Bay region of northeastern Canada in the year 1667.

They meet the jailed Lord Edward Crewe, a nobleman from England who has been banished from that country by King Charles II. They manage to free Edward, who funds their expedition, beginning in Montreal, designed to further free trade with the Indians and make Canada a more united land.

Barbara Hall is the sweetheart of Edward and her brother, Gerald, is thrust upon them after the explorers travel to England to seek the kings favor. Prince Rupert helps get Edward back in the kings good graces. Charles II is open to the idea of a trading post, provided he is personally brought 400,000 pelts.

Gerald creates trouble in Canada as soon as the new Fort Charles trading post is established. His actions incite violence among the Indian natives, who demand he be punished. Over the kings objections and to Barbaras horror, Radisson and his associates permit Gerald to be sentenced to death by a firing squad.

But once the gravity of her brothers misdeeds become clear to her, and with the flourishing of the Hudsons Bay trading post, Barbara forgives her love Edward while his partners Radisson and Gooseberry celebrate their success.

==Cast==
* Paul Muni as Pierre Esprit Radisson
* Gene Tierney as Barbara Hall
* Laird Cregar as "Gooseberry" (Médard Chouart des Groseilliers) John Sutton as Lord Edward Crewe  
* Virginia Field as Nell Gwyn King Charles II
* Nigel Bruce as Prince Rupert
* Morton Lowry as Gerald Hall
* Chief Thundercloud as Orimha  
* Frederick Worlock as English Governor  
* Montagu Love as Governor DArgenson  
* Ian Wolfe as Mayor
* Chief John Big Tree as Chief

==Reception==
George MacDonald Fraser wrote in 1988, "Hudsons Bay paid the penalty for being ahead of its time; critics found it boring, and one described it as a cock-eyed history lesson which, overall, it certainly is not." MacDonald goes on to say of Vincent price in the role of the King, "Here was an actor who looked reasonably like Old Rowley, and combined the languid style with the athletic presence - one could imagine Price walking ten miles a day for the fun of it as King Charles did." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 

 