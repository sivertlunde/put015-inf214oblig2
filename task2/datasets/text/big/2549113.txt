Virginia City (film)
{{Infobox film
| name           = Virginia City
| image          = Virginia City poster.jpg
| caption        = Theatrical release poster
| director       = Michael Curtiz
| producer       = Robert Fellows
| screenplay     = Robert Buckner
| starring       = {{Plainlist|
* Errol Flynn
* Miriam Hopkins
* Randolph Scott
* Humphrey Bogart
}}
| music          = Max Steiner
| cinematography = Sol Polito
| studio         = Warner Bros.
| distributor    = Warner Bros.
| editing        = George Amy
| released       =   1947 (France)
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         = $1 million  gross = 2,372,567 admissions (France, 1947)   at Box Office Story 
}} Western film John Murrell. Based on a screenplay by Robert Buckner, the film is about a Union officer who escapes from a Confederate prison and is sent to Virginia City from where his former prison commander is planning to send five million dollars in gold to Virginia to save the Confederacy. The film premiered in its namesake, Virginia City, Nevada.

==Plot==
Union officer Kerry Bradford (Errol Flynn) stages a daring escape from a Confederate prison run by prison commander Vance Irby (Randolph Scott) after learning that the rebels plan to smuggle five million dollars in gold from Virginia City, Nevada (in Union territory), to Texas, from where it can be shipped to Richmond, Virginia to aid the Confederate war effort. Bradford reports to Union headquarters and is immediately sent to Virginia City to determine where the gold is being kept. On the stagecoach, he meets and falls in love with the elegant Julia Hayne (Miriam Hopkins), who unbeknownst to him is in fact a dance-hall girl—and a rebel spy! Also on the stagecoach is the legendary John Murrell (Humphrey Bogart), leader of a gang of "banditos" traveling as a gun salesman. Before he and his gang can rob the stage, Bradford gets the drop on them and they escape empty-handed.

When the stage reaches Virginia City, Julia gives Bradford the slip and heads off to warn Captain Vance Irby (Randolph Scott)—the former prison commander now managing the gold-smuggling operation—that Bradford is in town. Bradford follows Irby to the rebels hideout behind a false wall in a blacksmiths shop, but the gold is moved before he arrives. The Union garrison is called out to patrol the roads to prevent any wagons from leaving town.

While Irby is meeting with the sympathetic town doctor, Murrell shows up looking for someone to set his broken arm. Irby offers Murrell $10,000 to have his banditos attack the garrison, which will force the Union soldiers guarding the roads to come to its defense. While the soldiers are busy defending the garrison, Irbys rebels will smuggle the gold out in the false bottoms of their wagons. First Irby needs to take care of Bradford. He uses Julia to arrange a meeting between the two men, and then takes Bradford prisoner, intending to return him to prison. (Irby, of course, is too honorable to simply shoot Bradford on the spot.)

The rebels wagon train reaches a Union outpost, where the wagons are stopped and searched. The skittish rebels start a firefight, and in the confusion Bradford escapes. Pursued closely by Irby and his men, he rides his horse down a steep incline and ends up somersaulting down the hill. The rebels, believing him dead, continue toward Texas. Bradford returns to the outpost and sends a telegraph to the garrison. Major Drewery (Douglass Dumbrille), the garrison commander, is scornful of Bradford as a soldier and does not take his advice, so the pursuit falls ever further behind the rebels, who are themselves fighting thirst, privation, and the unforgiving terrain. Bradford is able to persuade Drewery to allow him to take and a small detachment to follow his hunch.

Meanwhile, plotting to steal the entire shipment, the banditos return and attempt to take the gold. The Confederate wagons are trapped in a canyon and the gold appears to be lost when Bradford and the detachment arrive. Irby is wounded in the gunfight, but Bradfords superior military skills and the rebels long guns eventually drive off the banditos. That night, knowing that in the morning both Murrells banditos and Drewerys command will arrive, Bradford takes the gold from the wagons and buries it in the canyon.

Drewery and his men arrive in the morning in time to crush the banditos renewed attack. Bradford denies the gold ever existed and is brought up on charges in a court-martial, where he defends his actions. He defends his action in that "as a soldier" he knew the gold might have been used to win the war for the South and prevented that, but "as a man" he knows it belongs to the South and he would prefer that it be used to rebuild the Souths shattered economy and wounded pride after the war. The court finds him guilty of high treason and sentences him to death on April 9, 1865.

The day before Bradfords scheduled execution, Julia meets with Abraham Lincoln (Victor Kilian, seen only in silhouette) and pleads for Bradfords life. Lincoln reveals that at that very moment, Generals Lee and Grant are meeting at Appomattox Courthouse to end the war. As the war is over, and in a symbol of the reconciliation between North and South, Lincoln pardons Bradford in the spirit of his second inaugural address, "With malice toward none; with charity for all..."

==Cast==
* Errol Flynn as Captain Kerry Bradford
* Miriam Hopkins as Julia Hayne
* Randolph Scott as Captain Vance Irby
* Humphrey Bogart as John Murrell
* Frank McHugh as Mr. Upjohn Alan Hale as Olaf Moose Swenson
* Guinn Big Boy Williams as Marblehead
* John Litel as Thomas Marshall
* Douglass Dumbrille as Major Drewery
* Moroni Olsen as Dr. Robert Cameron Russell Hicks as John Armistead Dickie Jones as Cobby Gill
* Frank Wilcox as Union Outpost Soldier Russell Simpson as Frank Gaylord
* Victor Kilian as Abraham Lincoln Charles Middleton as Jefferson Davis

==Production==
The movie was a follow up to  , Anne Sheridan, Donald Crisp, Guinn Williams and Alan Hale. DRAMA: Bill Powell to Star in Own Detective Yarn
Schallert, Edwin. Los Angeles Times (1923-Current File)   18 July 1939: 13.   The title was eventually changed to Virginia City, which had been owned by RKO but they agreed to give it over to Warners. DRAMA: Karloff, Lugosi Ride Crest of Horror Wave
Scheuer, Philip K. Los Angeles Times (1923-Current File)   08 Sep 1939: 16.   SCREEN NEWS HERE AND IN HOLLYWOOD: Warners Will Continue Large Budget Pictures as Fear of Wars Effect Subsides RAINS CAME OPENS HERE Film Version of the Bromfield Novel at Roxy--6 ForeignLanguage Showings Today Columbia Cancels a Film Lupe Velez in "Hot Tamale" Of Local Origin
By DOUGLAS W. CHURCHILL Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   08 Sep 1939: 32.  

De Havilland dropped out and was replaced by Brenda Marshall. SCREEN NEWS HERE AND IN HOLLYWOOD: F.O.B. Detroit Revived by Paramount--Miss Lombard Sought for the Lead TORPEDOED OPENS TODAY English-Made Sea Melodrama of Scapa Flow at Globe-- Filmarte 4 Years Old Brenda Marshall Gets Role Of Local Origin
By DOUGLAS W. CHURCHILL Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   23 Sep 1939: 26.   However within a few weeks Miriam Hopkins replaced her. DRAMA: Miriam Hopkins Gets Picturesque Film Role Irene for Anna Neagle Rita Hayworth Cast Abbott Dancers Signed Dame May Whitty Tests Cushing Career Bright
Scahllert, Edwin. Los Angeles Times (1923-Current File)   18 Oct 1939: A17.   Randolph Scott was hired to play Flynns antagonist. NEWS OF THE SCREEN: Metro to Begin Work Monday on Not Too Narrow, Not Too Deep--Hollywood Cavalcade at Roxy Of Local Origin "Dust Be My Destiny" Held Over
By DOUGLAS W. CHURCHILL Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   13 Oct 1939: 30.  

Victor Jory was going to play the main villain but had a scheduling conflict due to his appearance in Light of Western Stars. He was replaced by Humphrey Bogart, requiring It All Came True to be pushed back. SCREEN NEWS HERE AND IN HOLLYWOOD: Irina Baranova of the Ballet Russe in Metros Florian-- Work Starts in 2 Weeks BANCROFT IN ROONEY FILM Will Be Father in Young Tom Edison--Pat OBrien Gets We Shall Meet Again Role RKO Signs Cedric Hardwicke Of Local Origin
By DOUGLAS W. CHURCHILL Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   28 Oct 1939: 11.  

There was six weeks filming based in Flagstaff, Arizona. Huge Studio Trek To Capture Color
The Washington Post (1923-1954)   07 Dec 1939: 17.  
==Reception==
The film was given a gala premiere in Reno and Virginia City. Troops Asked for Film Event: Virginia City Looks for Traffic Jam at Picture Premier
Los Angeles Times (1923-Current File)   07 Mar 1940: A8.  Courtroom Play Stars Stanwyck and Ameche: Payne Signed at 20th Grant, Kanin Reset Nevada Going Festive Tom Brown Joins Sandy Letter Now Sentence
Schallert, Edwin. Los Angeles Times (1923-Current File)   12 Feb 1940: 11.  

When the film was released in France in 1947, it became one of the most popular movies of the year. 
==References==
 

==External links==
*  
*  
*  
*   on Lux Radio Theater: May 26, 1941 

 

 
 
 
 
 
 
 
 
 
 
 
 