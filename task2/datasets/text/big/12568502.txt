The Emperor's Naked Army Marches On
 
{{Infobox film
| name           = The Emperors Naked Army Marches On
| image          = The-Emperors-Naked-Army-Marches-On(DVDcover).jpg
| alt            = 
| caption        = DVD cover
| director       = Kazuo Hara 
| producer       = Sachiko Kobayashi
| starring       = Kenzo Okuzaki
| cinematography = Kazuo Hara
| editing        = Jun Nabeshima
| distributor    = Imamura Productions Shisso Production Zanzou-sha
| released       =  
| runtime        = 122 minutes
| country        = Japan
| language       = Japanese
| budget         = $222,000
}} war documentary campaign in New Guinea, Kenzo Okuzaki, and follows him around as he searches out those responsible for the unexplained deaths of two soldiers in his old unit. Renowned documentary filmmaker Errol Morris listed The Emperors Naked Army Marches On as one of his Top 5 Favorite Films for Rotten Tomatoes. 

==Plot== Emperor Hirohito cannibalizing New indigenous people. 
 euphemistically called "black pigs" while American soldiers were "white pigs" - although one of the interviewed says there was a ban on eating "white pigs". The sister of one of the executed at one point states her belief that the two (low-ranking Private (rank)|privates) were killed so that the officers would have something to eat.
 captain named firing squad failed to kill them outright, something the captain denies.

Okuzaki also discovers that there has been another suspicious death in his unit and seeks out a former sergeant who is the sole survivor of his regiment. After much coaxing and a physical altercation the sergeant tells him that he personally killed a fellow soldier who had been stealing food and that the corpse was then eaten. He also states that the indigenous were not cannibalized as they were too quick to catch. Instead, Japanese soldiers were marked for death and cannibalism (the immoral and selfish ones first). The sergeant states that he only survived because he could make himself useful as a jungle guide, for instance finding fresh water for the other soldiers.

A written panel then states that the documentary crew and Okuzaki traveled to New Guinea but that the footage was confiscated by the Indonesian government.
 hard labor for attempted murder.

 .]]

==Awards==
* Berlin International Film Festival (1987) 
** Caligari Film Award (Kazuo Hara)
* Blue Ribbon Awards (1988)
** Best Director (Kazuo Hara)
* Kinema Junpo Awards (1988)
** Readers Choice Award for Best Film (Kazuo Hara)
* Mainichi Film Concours (1988)
** Best Director (Kazuo Hara)
** Best Sound Recording (Toyohiko Kuribayashi)
* Rotterdam International Film Festival (1988)
** KNF Award (Kazuo Hara)
* Yokohama Film Festival (1988)
** Best Director (Kazuo Hara)
** Best Film

==See also==
* Japanese War Crimes Tribunal#Cannibalism

==Notes==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 