Codename: Kyril
 
 
{{Infobox film
| name           = Codename: Kyril
| image          = Codename Kyril.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Ian Sharp
| producer       = 
| writer         =   John Hopkins
| story          = 
| based on       =  
| narrator       = 
| starring       = Edward Woodward  Ian Charleson Joss Ackland  Richard E. Grant  John McEnery  Peter Vaughan  James Laurenson  Denholm Elliott
| music          = Alan Lisk
| cinematography = 
| editing        = 
| studio         = Hartswood Films ITV  Showtime
| released       =  
| runtime        = 208 minutes
| country        = UK
| language       = English
| budget         = 
| gross          =
}} John Hopkins, from a 1981 novel by John Trenhaile.

==Plot== Marshal Stanov (Peter Vaughan), the head of the KGB, realizes that there must be a traitor within the KGB Moscow Center who is leaking high-level information to MI6, the British Secret Intelligence Service. He secretly sends his most trusted operative, Colonel Ivan Bucharensky (Ian Charleson), as an agent provocateur to the West in order to force the traitor to reveal himself out of fear of exposure.

On Stanovs instructions, Bucharensky, who is given the codename Kyril, "defects" to the West and heads for Western Europe and then London. As instructed, he leaves behind in Moscow a fabricated diary which supposedly implicates the traitor within the KGB. Stanov keeps the fake diary locked in a safe, but spreads information about its existence and supposed contents in order to see who reacts, and how.

General Michaelov (Espen Skjønberg), an aging hardliner of the KGB, is persuaded by his deputy Povin (Denholm Elliott) to sneak into the safe to confirm the existence of the diary, although Michaelov does not take the time to read the diarys contents. Povin also convinces Michaelov to attempt to have Kyril killed lest he divulge KGB secrets to MI6. Meanwhile, Povin is covertly sympathetic to the West, and sends messages to the head of MI6 via hidden microfilms carried by third parties.

In London, MI6 intercepts news of Kyrils defection. The head of MI6, C (Joss Ackland), assigns his lead agent Michael Royston (Edward Woodward) to capture Kyril and pump him for information. Above all, Royston is ordered to prevent Kyril from falling into KGB hands lest he expose the KGB mole who feeds information to C. Royston, however, is a double agent, with agendas of his own regarding Kyril.

Except for Stanov, neither the KGB nor MI6 know that Kyrils defection is false, or that Kyril is unaware of  the identity of the traitor inside the KGB. Both organizations are bent on either capturing or killing him because they fear he may reveal critical information to the other side. Kyril treads a fine line of brinksmanship in Amsterdam and then London, evading death several times. And when his old enemy Sikarov (James Laurenson) is sent to assassinate him, this threatens not only Kyril, but also his long-time girlfriend, London physiotherapist Emma Stanton (Catherine Neilson).
 gunrunner and intermediary Loshkevoi (John McEnery), to set up a meeting with the newly imprisoned Loshkevoi. Since Loshkevoi is one of Stanovs direct sources but has seemingly switched sides recently, he is the one person outside of Russia likely to know the identity of the traitor in Moscow.

The meeting between Kyril and Loshkevoi, arranged at a palace called Crowdon House, pits not only MI6 agents against Kyril, but also Royston against Sculby and Loshkevoi, and then Royston against Kyril in a climactic confrontation. The aftermath leads to surprising changes in the lives of some of those remaining, and to unexpected precarious uncertainties in the lives of others.

==Cast==
*Ian Charleson ...  Kyril (aka Ivan Bucharensky), a high-ranking officer and KGB agent who is sent to London as an agent provacateur

*Edward Woodward ...  Michael Royston, a high-level MI6 (British Secret Intelligence Service) agent who secretly favours the Russians

*Joss Ackland ...  C, head of MI6

*Richard E. Grant ...  Sculby, a London lawyer who works part-time for MI6

*John McEnery ...  Loshkevoi, a London dealer who trades in arms, money, and secrets for the KGB
 
*Peter Vaughan ...  Marshal Stanov, head of the KGB at Moscow Center

*James Laurenson ...  Sikarov, an assassin who is sent by Michaelov to London to kill Kyril

*Denholm Elliott ...  Povin, deputy to Michaelov at the KGB; he secretly sends information to C at MI6

*Espen Skjønberg ...  General Michaelov, an aging high-level KGB staff member

*Catherine Neilson ...  Emma, Kyrils true-love, an Englishwoman who lives in London
 
*Sven-Bertil Taube ...  Stolynovitch, a concert guitarist and friend of Povins who carries his messages internationally
  Hugh Fraser ...  Peter Jackson, a go-between and contact between MI6 and subordinate agents
 
*Tor Stokke ...  Yevchenko, Stanovs deputy

==Production== John Hopkins. He had written for several years for the popular BBC police drama  series Z-Cars (1962–78), and had written the BBC adaptation of John Le Carres novel Smileys People (1982, co-scripted with Le Carre), starring Alec Guinness. In 1965 he had also co-written the screenplay for the James Bond film Thunderball (film)|Thunderball.

The serial was filmed in Oslo, which was used for the Russian locations; Amsterdam; London; and Bristol, which substituted for some London locations. It was shot on 35mm film. 

Original electronic music for the serial was composed by Alan Lisk. The soundtrack also features some classical guitar music, including Chants dEspagne#1._Prelude|Asturias by Isaac Albéniz.

  said, "I could have played Povin cool and elegant, but I wanted to play him seedy and scruffy. Im always happier playing scruffy people and even when I dress up properly some basic seediness comes through." Devitt, Maureen.   Evening Times. 29 March 1988.  Except for Ian Charleson and James Laurenson, the two separate casts in Norway and England did not work together, which was a disappointment to Edward Woodward, who was hoping to meet Denholm Elliott. 

Edward Woodward had a heart attack towards the end of filming, which incapacitated him for several months.  Because of a directors strike, his prior filming on the American television series The Equalizer, where he spent months of gruelling 18-hour days, had not been completed until three days before his first day of filming in England for Codename: Kyril. The absence of a rest break after the exhausting American schedule apparently led to his heart attack.  The heart attack occurred on 28 July 1987, two weeks before filming ended. To compensate, his character was written out of one scene, and in two others doubles were used and Woodwards voice added later. 

==Broadcast, video, and DVD== ITV in Showtime in April 1988. In Australia, it aired in June 1989 on Seven Network.   
 Hugh Fraser) tells Laurence Sculby (Richard E. Grant) that he must pretend to be a Russian agent in order to gain Kyrils confidence, and tells Sculby to use the words "for the love of the Motherland" as a code phrase to convince him.

Codename: Kyril was released on VHS by Warner Home Video in 1991, in a truncated feature-length 90-minute version.  The full-length two-part 208-minute version was released on DVD in 2010 by Network DVD. 

==Critical reception==
Upon broadcast, most news outlets uniformly praised the serials all-star cast, heavily credentialed creative team, excellent production values, and colourful locations.   Variety (magazine)|Variety wrote:

 A two-part winner sure to bamboozle, worry and delight viewers.... The ins and outs, cunningly well defined, come together with a neat cynicism that marks the entire four-hour adventure.... Surprises jump up like coiled springs unleashed.... Beryl Vertue’s production looks smashing, with top-flight camerawork.   

Some outlets questioned the films rather serpentine plot: "The plot is strictly for fans of the cerebral espionage genre"  opined The Miami News. Some, like the New York Times, questioned somewhat the medley of Le Carre-like intricate espionage and Bond-like action scenes.  

Upon the release of the DVD, the site DVDCompare.com summarized its review as follows: 

 A thoughtful blend of drama and action (including a great car chase through London, which comes at the climax of part one), Codename: Kyril is beautifully written (by one of Britains leading television writers) and gorgeously shot. The (very respectable) cast is uniformly strong, and on the whole this is one of the best of the latter-day Cold War dramas."   at DVDCompare  

HorrorView.com said of the serial: " his is glossy prestige TV at its height, but with the kind of attention to character detail you dont always get to see these days." 

And Alex J. Geairns at Cineology wrote that:     
 Codename: Kyril comes from an era where espionage menace comes from the deadly silence of its protagonists. This mini-series offers complex characterisations and an intelligent treatment of the theme of trust and betrayal. It’s not from the James Bond crash-bang school of spying, having been adapted by the award-winning John Hopkins — whose previous credits include Smiley’s People and Z Cars.  

==References==
 

==External links==
*  at the Internet Movie Database
*  at AllMovie
*  – DVD
*  at Hartswood Films
* 

 

 
 
 
 
 
 
 
 
 
 
 