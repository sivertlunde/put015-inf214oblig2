Blighty (film)
{{Infobox film
| name           = Blighty
| image          = 
| image_size     = 
| caption        = 
| director       = Adrian Brunel
| producer       = Michael Balcon Carlyle Blackwell
| writer         = Eliot Stannard Ivor Montagu
| starring       = Ellaline Terriss Lillian Hall-Davis Jameson Thomas
| music          = 
| cinematography = Jack E. Cox
| editing        = 
| studio         = Gainsborough Pictures
| distributor    = Woolf & Freedman Film Service
| released       =  
| runtime        = 8397 feet
| country        = United Kingdom
| language       = 
}} silent drama film  directed by Adrian Brunel and starring Ellaline Terriss, Lillian Hall-Davis and Jameson Thomas. The film was a Gainsborough Pictures production with screenplay by Eliot Stannard from a story by Ivor Montagu.

==Background==
Blighty was Brunels second feature-length directorial assignment, four years after The Man Without Desire.  He had spent the intervening years making a series of satirical burlesque short films, the first few of which had impressed Michael Balcon who offered him the opportunity to produce and distribute further examples through Gainsborough.  In 1926 Balcon gave Brunel the chance to direct a full-length feature for Gainsborough and Blighty was the result.  Although Brunel was initially said to be in two minds about directing a "war film" as he did not care for the genre on moral or aesthetic grounds, he agreed to go ahead with the proviso that there would be no material directly depicting the conflict, nor any appeal to jingoistic sentiment. 

==Plot== Western Front, leaving his bride a young widow with a baby.

When David returns periodically to England on leave, he and Ann fall in love.  Meanwhile Robins wife (Nadia Sibirskaïa) finds her way as a refugee to England to seek out the Villiers and introduce them to their grandchild.  Following the declaration of the Armistice with Germany, the romance between David and Ann has to conquer entrenched class-based attitudes, while Robins wife at first feels overwhelmed and out-of-place in the Villiers household.  Problems are eventually overcome, and the Villiers welcome David and their daughter-in-law and grandchild into the family.

==Cast==
* Ellaline Terriss as Lady Villiers
* Lillian Hall-Davis as Ann Villiers
* Jameson Thomas as David Marshall
* Godfrey Winn as Robin Villiers
* Nadia Sibirskaïa as The Little Refugee
* Annesley Healey as Sir Francis Villiers
* Wally Patch as Drill Sergeant
* Dino Galvani as Poilou
* Renée Houston as Typist
* Billie Houston as Typist

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 