Graveyard Alive
{{Infobox Film | name = Graveyard Alive: A Zombie Nurse in Love
     | image = GyAZNIL04.jpg
     | caption  = United States theatrical poster
     | director  = Elza Kephart
     | executive producer  = Annie MacDonald  
     | producers  = Elza Kephart   Patricia Gomez    Andrea Stark 
     | co-producer  = Charles Jodoin-Keaton  
     | associate producer  = Jessica Andrews  
     | screenplay  = Elza Kephart (II)
     | Cinematographer = John Ashmore
     | starring  = Anne Day-Jones Samantha Slan
     | genre    = Suspense Horror  comedy
     | released  = July 8, 2005 (Canada) 
     | runtime  =  1 hr. 20 min.
     | country  = Canada
     | language  = English
     | MPAA rating = PG 13
}}
 independent zombie zombie horror comedy directed Slamdance festival in Park City, Utah and won best Cinematography. 
The movie was made by Bastard Amber Productions, and it was filmed in   (2004) (Doc)

==Synopsis==

Patsy Powers (Day-Jones) is a homely nurse who pines for handsome Dr. Dox (Gerhardt). Unfortunately for Patsy, the well-favoured physician only has eyes for pretty Goodie Tueshuez (Slan) - a jealous-minded nurse obsessed with popularity. When a foul-smelling woodsman is admitted to the hospital with an axe imbedded in his forehead, the rest of the nurse staff runs for cover as kindly Patsy and the injured worker form a warm bond. Later, after Patsy and the rugged lumberjack share a kiss, the smitten nurse is thrown off guard when her new beau reflexively sinks his teeth into her flesh. As Patsy runs off to bandage her wound, Eastern European doctor-turned-janitor Kapotski recognizes the woodsman as a zombie and ends the mans suffering with a stake through the head. In the days that follow, Patsys body is gradually taken over by the zombie virus. Not only does the transformation aversely affect Patsys eating habits, it instills her with a newfound confidence that quickly catches the eye of Dr. Dox as well. Perplexed by her mousy co-workers sudden transformation and determined to keep Dr. Dox for herself, the scheming Goody soon sets out to uncover the secret of Patsys rising popularity. Now, as Patsy struggles to stay well fed and Dr. Dox grows increasingly inpatient with Goodies unpalatable jealousy, the stage is set for a romance fueled by enough passion to transcend life and death. 

==Cast==

*Anne Day-Jones   - Patsy Powers Karl Gerhardt  - Dr. Dox
*Samantha Slan  - Goodie Tueshuze
*Eric Kendric  -  Woodcutter
*Roland Laroche  - Kapotski
*Roger Guetta  - Hospital Administrator

==Production credits==

*Annie MacDonald  - Executive Producer
*Elza Kephart  - Producer
*Patricia Gomez  - Producer
*Andrea Stark  - Producer
*Charles Jodoin-Keaton  - Co-Producer
*Jessica Andrews  - Associate Producer John Ashmore  - Cinematographer

==Awards==
Kodak Vision Award for Best Cinematography at the 2004 Slamdance film festival 

Nominated at the 2005 Cinevagas B-Movie film festival for: 

* Best B Movie
* Best Director
* Best Screenplay
* Karl Gerhardt for best actor
* Anne Day-Jones for best actress
* Samantha Slan for best supporting actress
* Martin Pelland for best music score
* John Ashmore for best cinematography
* Caroline Meyer & Sarah Hagan for best set design
* Stephanie Olivier for best editing

==References==
 

==External links==
* 
*  
*   - Exclaim! review
*   - Film Threat review (3.5 stars)
*   - Globe and Mail review

 
 
 
 
 
 
 