Blood Dolls
{{Infobox film
| name           = Blood Dolls
| image          =  
| caption        =
| director       = Charles Band
| producer       = Charles Band Robert Talbot, Charles Band (story only)
| narrator       =
| starring       = Jack Maturin, Debra Mayer
| music          = Ricardo Bizzetti
| cinematography = Tom Callaway
| editing        = Steve Nielson
| distributor    = Multicom Entertainment Group Inc., Full Moon Pictures
| released       =  
| runtime        = 84 min.
| country        = United States
| language       = English
| budget         = $1,000,000
| gross          =
| website        =
| amg_id         =
}}
Blood Dolls is a 1999 film directed by Charles Band.
==Plot==
Virgil Travis is a wealthy, soulless psychopath who lives in seclusion in his mansion home with his dwarf butler and maniac right-hand man. Tortured and forcibly mutated as a child by a woman who put him through body transforming procedures, Virgil has an abnormally sized head. Basking in the suffering, degradation, pain, and death of others, Virgil has already killed, and kidnapped a female rock group that he keeps imprisoned in his basement to help satisfy his constant need for perverse amusement. Never satisfied, though, Virgil decides that he will once again try to fill the emptiness that exists within him, and so creates a trio of deformed, living dolls to systematically murder any and all people who have ever wronged him. What Virgil doesnt anticipate, though, is meeting his match and finding love, both of which come in the form of a woman who is even more evil and twisted than he is.
===Reference to Demonic Toys=== Jack Attack, who is a character from Demonic Toys.
===Reference to Head of the Family===
Virgil Travis is the son of Myron Stackpool and from the yet-to-be-made Bride of the Head of the Family, Georgina.
==The Dolls==
* Pimp
* Sideshow
* Ms. Fortune
==See also==
* Killer Toys
* Hollyweird (film)|Hollyweird

==External links==
* 
 
 
 
 
 
 