Talcum Powder (film)
{{Infobox film
| name = Borotalco (Talcum Powder)
| image = Borotalco film.jpg
| caption =
| director = Carlo Verdone
| writer = Carlo Verdone, Enrico Oldoini
| starring = Carlo Verdone, Eleonora Giorgi, Christian De Sica, Mario Brega
| music = Lucio Dalla Fabio Liberatori Stadio (group)
| cinematography = Ennio Guarnieri
| editing = Antonio Siciliano
| producer = Mario Cecchi Gori, Vittorio Cecchi Gori
| distributor = Cecchi Gori Group
| released =  
| runtime = 96 minutes
| awards =
| country = Italy
| language = Italian
| budget =
}}
 1982 Cinema Italian comedy film written, starring and directed by Carlo Verdone.
 Best Film, Best Actor, Best Actress, Best Score Best Supporting Best Actress Best Score.  

== Plot summary ==
Sergio Benvenuti is a shy seller of contracts for a Roman company of music, but because of his character he cannot find even a customer. So he asks for help from a fellow named Nadia. But the two have never seen themselves, and so it takes a equivoc. In fact Sergio is also a friend of a penniless swindler, who before his arrest for corruption told him many nonsense notices about important American actors like Robert De Niro, John Wayne and Robert Redford. When Nadia arrives for work at home of the arrested, meanwhile occupied by Sergio, she believes him to be a major womanizer. Then Sergio, although he has a girlfriend daughter of a possessive and aggressive father (Mario Brega), pretends to be a charming womanizer and seduces Nadia, telling her that he is a good friend of Lucio Dalla, the most famous Italian singer of those times. The misunderstandings one after another, until his  real girlfriends father discovers all of Sergio and fills the barrel, forcing him to take his daughter as a bride at all costs.

== Cast ==
*Carlo Verdone: Sergio Benvenuti
*Eleonora Giorgi: Nadia Vandelli
*Christian De Sica: Marcello
*Angelo Infanti: Cesare Cuticchia alias Manuel Fantoni
*Roberta Manfredi: Rossella
*Mario Brega: Augusto  
*Isa Gallinelli: Valeria
*Enrico Papa: Cristiano

==References==
 

==External links==
* 

 
 
 
 
 