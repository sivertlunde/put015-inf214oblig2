Uyirile Kalanthathu
{{Infobox Film
| name           = Uyirile Kalanthathu
| image          = Uyirile Kalanthathu poster.jpg
| director       = K. R. Jaya
| producer       = Mutham Sivakumar
| writer         = K. R. Jaya Suriya Jyothika Radhika
| Deva
| cinematography = Selva. R
| editing        = B. Lenin V. T. Vijayan
| studio         = Sri Abayambika Films
| distributor    = 
| released       = September 29, 2000
| runtime        = 149 mins
| country        = India
| language       = Tamil
}}
 Radhika in supporting roles. The film opened in September 2000 to negative reviews from critics and became a disaster. The film has been dubbed into Hindi under the title Suryabhai MBBS.

==Plot==
Surya plays the role of a medical college student and Jyotika is his girlfriend. Suryas fathers role (played by his father in real life, Sivakumar) whilst Raghuvaran plays his elder brother, who is the collector of the district. Surya is the  youngest son of the family and this gives much heartburn to elder brother Raghuvaran. Their rivalry begins from childhood and can be illustrated by the scene where the elder brother pinches his baby brother just to see him cry. The jealousy grows into adulthood. His whole aim in life is to see that Suryas life is beset with problems, thanks to him. However, Surya and his parents are blissfully unaware of the jealousy that has possessed Raghuvaran. When he discovers the love between Surya and Jyotika he passes on the word to Jyotikas elder brother who is a known rowdy around the area. He does this thinking that the rowdy would manage to beat up Surya and perhaps separate both Surya and Jyotika. This plan backfires, when the rowdy is more than happy to get them both married because more than anything, he cares about his sisters happiness. Raghuvaran then pushes Surya over a cliff edge.

The rest of the movie is about whether Raghuvaran will find the love and care that he has always yearned for from his parents. Later, in the court Surya is revealed to be alive and he unites with his family as well as Jyothika, and his brother is sent for treatment.

==Cast== Surya as Suriya
*Jyothika as Priya
*Raghuvaran as Raghuram
*Sivakumar as Sethu Vinayagam Radhika as Lakshmi
*Mutham Sivakumar as Siva
*Balaji as Suriyas friend
*Kanal Kannan as Kanal
*Vaiyapuri as Sivas assistant

==Production== Suriya again before the release of their maiden venture together, Poovellam Kettuppar. 

==Release==
A reviewer from Sify described the venture as "strong and so far unexplored theme about sibling rivalry and superb acting" but claimed that "the unwarranted twist in the end has somewhat reduced the impact". The reviewer claims that the performances of Sivakumar and Radhika "score over everyone else", while Raghuvaran and Surya also "shine", but is critical of Jyothika saying she "should replenish fast her dwindling stock of expressions".  The performance won further film offers for Surya. 
 Deva won for Best Family Film, finishing behind Budget Padmanabhan and Mugavaree.

==Soundtrack==
{{Infobox album|  
  Name        = Uyirile Kalanthathu
| Type       = Soundtrack Deva
| Cover      = 
| Released   = 2000
| Recorded   =  Feature film soundtrack
| Length     = 
| Label      = The Best Audio
| Producer   = 
| Reviews    = 
| Last album = 
| This album = Uyirile Kalanthathu (2000)
| Next album = 
}}
The soundtrack of the film was composed by Deva (music director)|Deva.

{{tracklist
| headline     = Track-list
| extra_column = Singer(s)
| total_length = 
| title1       = Sainthathu Kanne      
| extra1       = S. P. Balasubrahmanyam
| length1      = 4:54
| title2       = Uyire Uyire Alzaithathenna      
| extra2       = Hariharan (singer)|Hariharan, Sujatha Mohan
| length2      = 5:31
| title3       = Kannal Vandu      
| extra3       = Shankar Mahadevan
| length3      = 5:59
| title4       = Husaine Husaine     
| extra4       = Sukhwinder Singh, Subha
| length4      = 5:31
| title5       = Deva Deva Devathaiye     Harini
| length5      = 5:25
| title6       = Coca Cola Pole    
| extra6       = Deva (music director)|Deva, Sabesh
| length6      = 4:30
}}

==References==
 

 
 
 
 
 