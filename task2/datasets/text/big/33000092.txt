Let Us Live
{{Infobox film
| name           = Let Us Live
| image          = Let Us Live poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = John Brahm
| producer       = William Perlberg
| screenplay     = Anthony Veiller Allen Rivkin
| based on       =  
| starring       = Maureen OSullivan Henry Fonda Ralph Bellamy
| music          = Karol Rathaus
| cinematography = Lucien Ballard Al Clark
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
}}

Let Us Live is a 1939 crime thriller film directed by John Brahm starring Maureen OSullivan,  Henry Fonda and Ralph Bellamy.  

The script of the film was adapted from the 1936 Harpers Magazine|Harpers Magazine story "Murder in Massachusetts" by Joseph F. Dinneen about a real criminal case.    In 1934 two Boston taxi drivers were identified by several witnesses as the culprits who murdered a man during a theater robbery in Lynn, Massachusetts. Their trial was in progress for two weeks and it seemed likely that the two where going to be found guilty when the real killers were arrested for another crime and then admitted to the Lynn robbery-murder. 

Columbia Pictures had planned a much bigger production but after political pressure from the state of Massachusetts the films budget and publicity were scaled down considerably and it was ultimately released as a B movie. 

==Plot==
On the eve of his marriage to waitress Mary Roberts (OSullivan), taxi driver "Brick" Tennant is questioned as a murder suspect along with 120 other drivers, because a taxi served as the getaway car in a theater robbery in which a man was killed. When one of the witnesses swears that Brick and his friend Joe Linden (Baxter) were the killers, the district attorney (Ridges), eager for a conviction, brings the taxi drivers to trial even though Brick and Mary were in a church when the robbery took place. Although innocent, Brick and Joe are found guilty and sentenced to die on the electric chair. Mary, however, refuses to give up hope, and when she unearths a bullet from another robbery that was shot from the murder weapon, she convinces Police Lieutenant Everett (Bellamy) that the wrong men have been convicted.  To prove Brick and Joes innocence Everett and Mary search for the real culprits . As the time of his execution approaches, Brick is transformed from an idealistic youth into a man whose faith in the system has been shattered. On the day of the execution, Mary and Everett finally find the real culprits. The governor then pardons Brick, but although his life has been spared, his faith can never be repaired.

==Cast==
 
 
*Maureen OSullivan - Mary Roberts
*Henry Fonda - John J. Brick Tennant
*Ralph Bellamy - Police Lt. Everett Alan Baxter - Joe Linden
*Stanley Ridges - District Attorney
*Henry Kolker - Chief of Police
*George Lynn - Joe Taylor
*George Douglas - Ed Walsh
*Phillip Trent - Frank Burke
*Martin Spellman - Jimmy Dugan
  
*John Qualen - Dan (uncredited)
*Charles Trowbridge - Trial Judge (uncredited)
*Dick Elliott - Rotarian juror (uncredited)
*Ann Doran - Secretary Juror (uncredited)
*Sam McDaniel - Mose - Hold-Up Witness (uncredited)
*Ethel Wales - Ella - Theatre Scrubwoman (uncredited) Charles Lane - Auto Salesman (uncredited) 
*Milton Kibbee - Hardware Store Proprietor (uncredited) 
 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 