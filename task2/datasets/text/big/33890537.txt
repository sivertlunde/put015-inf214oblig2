Prema (1989 film)
{{Infobox film name           = Prema image          = caption        = director  Suresh Krishna producer       = D. Ramanaidu screenplay     = Suresh Krishna Ananthu Dialogues      = Sainath Thotapalli story          = Suresh Krishna starring  Venkatesh Revathi music          = Ilaiyaraaja cinematography = P. S. Prakash editing        = K. A. Marthand studio         = Suresh Productions distributor    = Rajeswari Films censor date    = December 10, 1988 released       = 12th January 1988 country        = India runtime        = 156 minutes language       = Telugu budget         = gross           =
}}
 Suresh Krishna. Starring Daggubati Venkatesh|Venkatesh, Revathi played the lead roles, with original soundtrack by Ilaiyaraaja. The film was a musical blockbuster of the time but film recorded as Average at box-office.   It was dubbed in Tamil as Anbu Chinnam. Suresh remade the film in Hindi as Love (1991 film)|Love with Salman Khan and Revathi.

==Plot==
The film revolves around the tragic love tale of a Christian girl and a Hindu boy.

==Cast==
  Venkatesh as Pruthvi
* Revathi as Maggi
* S. P. Balasubrahmanyam as Satya Rao
* Gollapudi Maruti Rao as Ananda Rao Manjula as Stella
* Kalpana as Geeta
* Brahmanandam as Gambler Rallapalli as House Owner
* Pradeep Shakthi as Rowdy
* P. L. Narayana as Geethas Father
* P. J. Sarma as Father
* Chalapathi Rao as Minister
* Bhimaraju as Neighbor
* Gundu Hanumantha Rao as Gambler Jenny
 

==Soundtrack==
{{Infobox album
| Name        = Prema
| Tagline     = 
| Type        = film
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1989
| Recorded    = 
| Genre       = Soundtrack
| Length      = 32:04
| Label       = Echo Music 
| Producer    = Ilaiyaraaja
| Reviews     =
| Last album  = Siva (1989 Telugu film)|Shiva   (1989) 
| This album  = Prema  (1989)
| Next album  = Geethanjali (1989 film)|Geethanjali  (1989)
}}
 Acharya Athreya. All songs are blockbusters. Music released on ECHO Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 32:04
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits =
| music_credits =

| title1  = Priyatama SP Balu
| length1 = 5:49

| title2  = Eenade Yedho Chitra
| length2 = 4:28

| title3  = You Are My Hero
| extra3  = SP Balu, Chitra
| length3 = 4:29

| title4  = Ivvu Ivvu
| extra4  = SP Balu, Chitra
| length4 = 5:18

| title5  = Ekkada Ekkada
| extra5  = S. P. Sailaja
| length5 = 4:35

| title6  = I Am sorry
| extra6  = SP Balu
| length6 = 4:43

| title7 = Ontari Vadini Nenu
| extra7  = SP Balu
| length7 = 2:09
}}

==Awards==
* Nandi Award for Best Actor - Daggubati Venkatesh

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 
 