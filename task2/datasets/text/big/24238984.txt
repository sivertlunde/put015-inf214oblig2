Bye-Bye (film)
{{Infobox Film
| name           = Bye-Bye
| image	         = Bye-Bye FilmPoster.jpeg
| caption        = Film poster
| director       = Karim Dridi
| producer       = Ilann Girard Alain Rozanès
| writer         = Karim Dridi
| starring       = Sami Bouajila
| music          = 
| cinematography = John Mathieson
| editing        = Lise Beaulieu
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = France
| language       = French
| budget         = 
}}

Bye-Bye is a 1995 French drama film directed by Karim Dridi. It was screened in the Un Certain Regard section at the 1995 Cannes Film Festival.   

==Cast==
* Sami Bouajila – Ismael
* Nozha Khouadra – Yasmine
* Philippe Ambrosini – Ludo
* Ouassini Embarek – Mouloud
* Sofiane Madjid Mammeri – Rhida (as Sofiane Mammeri)
* Jamila Darwich-Farah – La tante
* Benhaïssa Ahouari – Loncle
* Moussa Maaskri – Renard
* Frédéric Andrau – Jacky
* Christian Mazucchini – Yvon
* Bernard Llopis – Popaul
* Marie Borowski – Bobo
* André Neyton – Marcel
* Emmanuel-Georges Delajoie – Mabrouk
* Bakhta Tayeb – La grand-mère
* Farida Melaab – Malika

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 