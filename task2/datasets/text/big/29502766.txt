Wild Harvest
{{Infobox film
| name           = Wild Harvest
| image          = 
| image_size     = 
| caption        = 
| director       = Tay Garnett
| producer       = Robert Fellows John Monks, Jr.
| story           = Houston Branch
| narrator       = 
| starring       = Alan Ladd Dorothy Lamour Robert Preston Lloyd Nolan
| music          = Hugo Friedhofer
| cinematography = John F. Seitz
| editing        = Billy Shea George Tomasini
| studio         = 
| distributor    = Paramount Pictures
| released       = September 26, 1947
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Wild Harvest is a 1947 film directed by Tay Garnett. It stars Alan Ladd and Dorothy Lamour.  

==Plot==
Joe Madigans crew harvests wheat for farmers. Jim Davis, a good mechanic who irresponsibly drinks and gambles too much, is fired by his friend, but atones with a heroic act during a fire.

Alpersons rival crew is getting jobs by under-bidding Joes. A farmers flirtatious niece, Fay Rankin, finds a field for Joes workers and then unsuccessfully tries to seduce him. She wants to come along and sets her sights on Jim instead, marrying him.

Fays interference becomes a problem. Joe ends up owing money and Alperson tries to buy his combines. Fay makes another play for Joe, who calls her "cheap" and "poisonous." Jim catches her slapping Joe, which leads to a fight between the men.

Joes loyal crew member King catches thievery of wheat by Jim and reports it. Joe, almost broke, is saved again by a penitent Jim, who sells Fays car, enraging her. Fay finally reveals to Jim that she never loved him at all and that their marriage was a "joke." Joe and Jim team up on a new 3,000-acre job, making them prosperous at last.

==Cast==
* Alan Ladd as Joe Madigan
* Dorothy Lamour as Fay Rankin Robert Preston as Jim Davis
* Lloyd Nolan as Kink
* Richard Erdman as Mark Lewis
* Allen Jenkins as Higgins Will Wright as Mike Alperson
* Griff Barnett as Rankin Anthony Caruso as Pete
* Walter Sande as Long
* Frank Sully as Nick

==Production==
The film was based on an original screen story called The Big Haircut by Houston Branch bought by Paramount in 1946. A.I. Bezzerides was hired to work on the script. PARAMOUNT BUYS HARVESTING STORY: Studio Will Produce Houston Branchs The Big Haircut --Lead to Alan Ladd
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   11 May 1946: 34. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 