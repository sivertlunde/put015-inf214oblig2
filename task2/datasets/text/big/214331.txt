Morgan – A Suitable Case for Treatment
{{Infobox film
| name           = Morgan: A Suitable Case for Treatment
| image          = Morgan poster french 20k.jpg
| caption        = French poster under the title Morgan!
| director       = Karel Reisz
| producer       = Leon Clore David Mercer David Warner Vanessa Redgrave Robert Stephens Irene Handl Bernard Bresslaw Arthur Mullard
| music          = John Dankworth
| cinematography = Larry Pizer
| distributor    = British Lion Films
| released       = 27 January 1966 (UK)
| runtime        = 97 minutes
| country        = United Kingdom English
| budget         =
}}
 1966 comedy British Lion David Mercer, based on his BBC television play A Suitable Case for Treatment (1962), the leading role at that time being played by Ian Hendry. 
 David Warner, Vanessa Redgrave and Robert Stephens with Irene Handl and Bernard Bresslaw.

The eponymous hero is working-class artist Morgan Delt (David Warner), obsessed with Karl Marx and gorillas, who tries to stop his ex-wife (Vanessa Redgrave) from remarrying.

==Plot summary== David Warner) King Kong film to illustrate Morgans fantasy world.

Still failing to win her back, Morgan now secures the help of his mothers wrestler friend Wally "The Gorilla" (Arthur Mullard) to kidnap Leonie, who still nurtures residual feelings of love tinged with pity for Morgan. However, in the end, Morgan is arrested and committed to an insane asylum. Here, Leonie visits him looking visibly pregnant. With a wink, Leonie tells him he is the childs father. Morgans eyes and smile light up his sedated face with a malicious twinkle before he returns to tending a flowerbed as the camera pulls out to a longshot of the entire circular flowerbed with the enclosed flowers arranged into a hammer and sickle.

==Cast== David Warner - Morgan Delt
* Vanessa Redgrave - Leonie Delt
* Robert Stephens - Charles Napier
* Irene Handl - Mrs. Delt
* Bernard Bresslaw - Policeman
* Arthur Mullard - Wally

==Reception== Best Actress Best Costume Design, Black-and-White (Jocelyn Rickards).
 Best Actress.   

American actress Morgan Fairchild took her first name from this film.

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 