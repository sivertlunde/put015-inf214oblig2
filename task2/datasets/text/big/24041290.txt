Crash: The Mystery of Flight 1501
{{Infobox television film
| name           = Crash: The Mystery of Flight 1501
| image          = Crash The Mystery of Flight 1501 DVD cover.jpg
| image_size     =
| caption        = DVD cover
| genre          = Drama Mystery
| director       = Philip Saville
| producer       = Lee Rafner
| writer         = E. Arthur Kean
| narrator       = 
| starring       = Cheryl Ladd Jeffrey DeMunn 
| music          = Mark Snow
| cinematography = Paul Lohmann
| editing        = Edward M. Abroms
| studio         = Citadel Entertainment Consolidated Entertainment Schaefer/Karpf Productions
| distributor    = NBC
| released       = November 18, 1990
| runtime        = 90 minutes
| network        = NBC USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1990 television film directed by Philip Saville. The film is usually advertised as being based on true events. 

== Plot ==
Diane and Greg Halstead were once happily married, even deciding to try and have a baby in later years, despite the fact that she had already suffered two miscarriages. She has no luck in becoming pregnant and this leads to an estrangement from her husband. On his latest flight, Greg, a professional pilot, finds out about a bomb threat. The person carrying the bomb supposedly wants to kill another passenger, a politician with an outspoken opinion on abortion. Unknown to the killer, however, the politician has already left the plane because it had an hour and a half delay. Greg decides to make an emergency landing in Dayton, Ohio, but during the heavy weather, the plane crashes, killing almost everyone on board.
 NTSB are at the scene of the accident. There are different kinds of speculation about the cause of the crash, and some suspect Greg of refusing to follow orders. The FBI notices that the CIA immediately collected stuff out of the wreckage and said it was top secret. Diane is devastated when she hears the news, until she finds out that Greg is one of the few survivors. She is contacted by Scott Cody, who works for the Air Line Pilots Association, International|ALPA. He tells her that Greg is the prime suspect over the crash and collects information from her, finding out that Greg was on medication.

It turns out that there was no bomb on board, and all the evidence points against Greg. Cody finds out that the CIA was spying on the plane, thereby messing up his radar. Diane asks if that was the reason why Greg crashed, but Cody explains that it is more complicated. Meanwhile, Greg dies from his injuries. Diane makes an official statement in which she claims her husband was not responsible, but she is not considered a reliable source, in view of the fact that she could lose pension and other benefits. Diane refuses to accept that her husband will be blamed for the crash and does everything to get the entire truth revealed. With the help of a few experts, she is able to prove that there was a fire on the plane, which caused the crash.

==True Events?==
The aircraft depicted in this 1990 film is a McDonnell Douglas DC-9 in all instances except the take-off, in which a Boeing 727 is shown. Prior to 1990, the closest aviation accident to the events depicted in the film would arguably be the Southern Airways Flight 242 accident, in which a DC-9 experienced a double engine failure in a thunderstorm. This event however does not involve either an in-flight fire, nor a fight to clear the pilots name (two important plot points in the movie).

On 28 November 1979, however, a McDonnell Douglas DC-10 crashed into a mountain in Antarctica. While not involving an in-flight fire, the actions of the pilots wife were significant in raising a Royal Commission, in which the "pilot error" finding of the initial report was changed.

In 1996, six years after the release of The Mystery of Flight 1501, ValuJet Flight 592, a DC-9 (as depicted in the film) crashed after dangerous goods illegally loaded into the cargo compartment caused an in-flight fire which brought down the aircraft in a startlingly similar echo of the events in the film.

==Cast==
*Cheryl Ladd as Diane Halstead
*Jeffrey DeMunn as Scott Cody
*Frederick Coffin as Wes Goddard
*Peter Jurasik as Bob Stanton
*Jim Metzler as Spence Zolman
*Jeff McCarthy as Chet Harmon Moira Walley as Pamela Hayes
*Doug Sheehan as Gregory Greg Halstead
*Ray Blunk as Switzer
*Zachery Ty Bryan as Child (uncredited)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 