Desperately Seeking Helen
 
Desperately Seeking Helen is a 1998 film by Eisha Marjara, produced by the National Film Board of Canada.

It documents the life of the  . April 24, 2000. Retrieved on November 22, 2014.  Marjara liked Helen as a child, and Marjara stated "Helen is a conduit into my childhood — my relationship with my mother, my struggle with anorexia and the Air India disaster which took the lives of my mother and sister." Black, Barbara. " " ( ). Concordias Thursday Report. April 21, 2005. Volume 29, No. 14. Retrieved on November 22, 2014.  Jerry Pinto, author of Helen: The Life and Times of an H-bomb, wrote that the film "is as much about Eisha Marjaras perception of Helen as it is about Helen."  Desperately Seeking Helen uses Hindi music. 

The film covers the complications in the relationship between Marjara and her mother, Jones, D.B. "Brave New Film Board". In: Beard, William and Jerry White (editors). North of Everything: English-Canadian Cinema Since 1980.   bombing, Bontempo, Mirella. " " ( ). Montreal Serai. July 27, 2012. Retrieved on November 22, 2014.  which ultimately killed Davinder along with Seema, one of Marjaras sisters. 

D.B. Jones, the author of "Brave New Film Board," wrote that the filmmaker "verges on self-pity and often seems self-absorbed, but she can also be brutally honest about herself."  Sabeena Gadihoke, the author of "Secrets and Inner Voices: The Self and Subjectivity in Contemporary Indian Documentary," wrote that that the "deeply personal" film "did not easily fit popular conceptions of documentary" since it had a "fictive structure in which the filmmaker staged her own body" as well as "reflexive use of humor" and  "whimsy". Gadihoke, Sabeena. "Secrets and Inner Voices: The Self and Subjectivity in Contemporary Indian Documentary." In: Lebow, Lisa (editor). The Cinema of Me: Self and Subjectivity in First-Person Documentary Film (Nonfictions series).   disaster. 

==Background== National Film Board selected Marjara to make a film on the Air India Flight 182 disaster in 1994. She did research by visiting Trois-Rivières and taking one trip to India.  The title is a reference to the 1985 film Desperately Seeking Susan. 

Marjara dedicated her film to Air India Flight 182 victims, including Davinder and Seema. 

==Release==
 
Its first screening in India was during the  , August 13, 2013. ISBN 0231850166, 9780231850162. Google Books  . 

==Reception==
It was ranked as the "Theater Critics Choice" in the Chicago Reader in 1999.  Firdaus Ali of Rediff wrote that the film received "rave reviews".  Gadihoke stated that in India the film received some criticism due to a perception that it was "self-absorbed"; Gadihoke argued that this was because the film used "strategies unfamiliar to documentary discourse in India at the time." 

==References==
 

==Further reading==
* Failler, Angela. "Remembering the Air India Disaster: Memorial and Counter-Memorial." Review of Education, Pedagogy, and Cultural Studies 31 (2009). p. 166-172.

==External links==
*  
* / /  " " ( ). Semaine de la critique (Locarno Film Festival).
* 
*Ali, Firdaus. " " ( ). Rediff. April 24, 2000.
* 
*http://www.eng.fju.edu.tw/canada/class/g_seeking_helen.ppt - http://www.webcitation.org/6UGpwhsxA

 
 
 
 
 
 
 
 
 
 