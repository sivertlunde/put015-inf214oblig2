Print the Legend
 
{{Infobox film
| name           = Print the Legend
| image          = Print_the_Legend_film_poster.jpg
| caption        = Theatrical release poster
| alt      = Print the Legend
| director       = Luis Lopez J. Clay Tweel
| producer       = Walter Kortschak Mary Rohlich Walker Deibel Seth Gordon Steven Klein Dan OMeara Chad Troutwine Andrew Kortschak Rafi Chaudry Natalia Duncan Robert Epperson Ryan Johnston
| starring       = Bre Pettis Maxim Lobovsky Avi Reichental Cody Wilson
| music          = Kyle Johnston Matthew McGaughey Noah Wall
| cinematography = Luis Lopez J. Clay Tweel
| editing        = Luis Lopez J. Clay Tweel
| production companies = Audax Films Exhibit A
| distributor    = Netflix
| released       =  
| language       = English
}}
 Netflix Original focused on the 3D printing revolution.    It delves into the growth of the 3D printing industry, with focus on companies MakerBot, Formlabs, Stratasys, and 3D Systems, as well as figures of controversy in the industry such as Cody Wilson.

The title of the film comes from the denouement of the film The Man Who Shot Liberty Valance.   



==Plot Summary==

Print the Legend illustrates the 3D printing achievements of MakerBot, Formlabs, Stratasys, and 3D Systems, which is comparable to a revolutionized American Dream.

Although there is lack of 3D printing history information and Apple Inc (Steve Jobs) glorification, this Netflix documentary explores interestingly the pros (human organs, glasses, skull implant, footwear) and cons (Cody Wilson) of the growth of 3D printing.

On one hand, beyond Print the Legend demonstrates how stereolithography technology allows to print objects such as artificial organs, prosthetic hands, chess pieces; it also investigates the development of 3D printers for common people use as a normal home device. 

On the other hand, the documentary intensifies concerns of 3D printing for the world as Cody Wilson, an American gun right activist, disregards gun control by 3D printing it through Defense Distributed. From his point of view, every American should be able to access a gun. 

The trailer is available to watch on print the film’s website: http://printthefilm.com  

==Cast==
* Chris Anderson
* Bruce Bradshaw
* Craig Broady
* Bill Buel
* Michael Calore
* Nadia Cheng
* Alan Cramer
* David Cranor
* Michael Curry
* Malo Delarue
* Brad Feld
* Ian Ferguson
* Lorenzo Franceschi-Bicchierai
* Martin Galese
* Matt Griffin
* James Gunipero
* Zach Hoeken
* Luke Iseman
* Annelise Jeske
* Brad Kenney
* Eric Klein
* Cliff Kuang
* Jenny Lawton
* Natan Linder
* Ira Liss
* Alex Lobovsky
* Larisa Lobovsky
* Maxim Lobovsky
* Marty Markowitz
* Adam Mayer
* Nathan Meyers
* Jennifer Milne
* Anthony Moschella
* Will OBrien
* Jeff Osborn
* Andrew Pelkey
* Bre Pettis
* Chuck Pettis
* Yoav Reches
* Avi Reichental
* David Reis
* Barry Schuler
* Virginia White
* Cody Wilson
* Luke Winston

== Festivals ==
Awards
* Special Jury Recognition Award - SXSW Film Festival (2014) 
* Special Jury Award - IFF Boston (2014) 

==References==
 

==External links==
*  

 
 
 