The Peace Conference
{{Infobox Hollywood cartoon|
| cartoon_name = The Peace Conference
| series = Krazy Kat
| image = 
| caption = 
| director = Manny Gould Ben Harrison
| story_artist = Ben Harrison
| animator = Manny Gould Allen Rose Harry Love
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| studio = 
| distributor = Columbia Pictures
| release_date = April 26, 1935
| color_process = Black and white
| runtime = 6:29 English
| preceded_by = Hotcha Melody
| followed_by = The Kings Jester
}}

The Peace Conference is a short animated film by Columbia Pictures, featuring the comic strip character Krazy Kat.

==Plot==
Amidst conflict occurring in other nations, leaders from various parts of the world gather at a government building to discuss peace. At a dining table, the leaders associates are receiving slices of ham. Because they couldnt agree about which kind of slice they should have, however, the associates brawl onto the table. Momentarily, an army tank barges into the building. Coming out of the tank is none other than Krazy Kat. Krazy tells the rowdy associates that he can restore peace with his special rifle. The associates find his method ludicrous and therefore laugh. To prove them wrong, the cat fires his rifle at them. But unlike regular bullets, the projectiles conjure small-scale people which resemble celebrities at the time (see below). The singing and dancing of these tiny men hypnotically induces pleasure in the associates, causing them to make up.

After curing the associates, Krazy heads to the room where the leaders are at a meeting. Peeping from behind a door, he notices the leaders are rather silent and in doubt on how to settle their problems. Krazy then fires his pleasure-inducing bullets which excites the heads-of-state into a jig as well giving them friendly perception of each other.
 Mars does not like whats happening on Earth. He then comes to the planet and into the government building. Mars attempts to preach everybody to return to their attacking ways. Krazy, who opposes the gods purpose, opts to take him on. As Mars moves menacingly toward the cat, Krazy fires two shots at him using the rifle. Both shots show some effect but the dangerous deity is able to snap out of it. As a last resort, Krazy fires multiple shots which brings down Mars for good. All the people in the vicinity celebrate Krazys victory in singing and dancing, including Mars who has completely succumbed to the pleasure spell.

==Caricatures of notables== Ted Lewis, Rudy Vallee, etc. Mahatma Gandhi is also portrayed as one of the leaders at the meeting scene.

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 


 