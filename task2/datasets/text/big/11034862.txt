I mostri
 
{{Infobox film name            = I mostri image           = I mostri.jpg caption         =  director        = Dino Risi producer        = Mario Cecchi Gori writer          = Agenore Incrocci Ruggero Maccari Elio Petri Dino Risi  Furio Scarpelli Ettore Scola starring        = Vittorio Gassman Ugo Tognazzi Rika Diallina music           = Armando Trovajoli cinematography  = Alfio Contini editing         = Maurizio Lucidi distributor     =  released        = 26 October 1963 (Italy) April 10, 1968 (United States|U.S.) runtime         = 115 min (Italian version) / 87 min (US version) language  Italian
|budget          =
|}} 1963 commedia allitaliana film by Italian director Dino Risi. It was coproduced with France.

The film was a huge success in Italy. It was censored in Spain. In 1977 an Academy Award nominee sequel was filmed, entitled I nuovi mostri (Viva Italia!).

==Plot==
The film features several episodes in which actors Ugo Tognazzi and Vittorio Gassman are the greatest performers. The themes of the short stories are intended to offer a clear picture of the habits, vices, of cheating and taste typical of the majority of Italians in the Sixties. Obviously the characters and funny situations are written and staged in a way that touches the limits of satire and coarseness, however many of these issues are still recognized by the Italian company. The satire of the episodes affects people of both noble origins of poor people, both political and police officers who abuse their power and, last but not least the middle classes. To remember are the episodes as The Monster in which a man who has killed the whole family and has barricaded himself in the house was arrested by two policemen (Gassman and Tognazzi) and photographed with the present. However, although the man is a murderer, the two cops are real monsters of ugliness. Another episode to remember is Education in which the parent Ugo Tognazzi educates his son (Ricky Tognazzi) being a perfect cheat to appear more masculine and smart, to pay much less and not plunging into heavy or boring situation would certainly not born. He instructs his son in beating their classmates who do not want him to copy the tasks and lie age when he went to the fair to not pay the ticket. When the child grows, the first thing it will do is make amends for his teachings repaying his father with a pistol and with the theft of all his possessions.

==Episodes==
The original version is composed of 20 episodes, all starred by Vittorio Gassman and Ugo Tognazzi. 
*"Leducazione sentimentale"
*"La raccomandazione"
*"Il mostro"
*"Come un padre"
*"Presa dalla vita"
*"Il povero soldato"
*"Che vitaccia"
*"La giornata dellonorevole"
*"Latin lovers"
*"Testimone volontario"
*"I due orfanelli"
*"Lagguato"
*"Il sacrificato"
*"Vernissage"
*"La musa"
*"Scende loblio"
*"La strada è di tutti"
*"Loppio dei popoli"
*"Il testamento di Francesco"
*"La nobile arte"

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 
 