Heer (1955 film)
{{Infobox film name = Heer image =  image size =  border =  alt =  caption =  director = Nazir Ahmad producer = J.C. Anand writer =  screenplay = Baba Alam Siahposh story =  based on =   narrator =  starring = Swaran Lata Inayat Hussain Bhatti Rekha Zeenat Begum Ajmal Nazar Imdad Husain music = Safdar Hussain cinematography =  editing = studio =  distributor = Eveready Pictures released =   runtime =  country = Pakistan language = Punjabi
|budget =  gross =
}}
 Punjabi film Nazir Ahmed,       starring Swaran Lata and Inayat Hussain Bhatti in title roles. The movie is based on the famous Heer Ranjha|Heer, written by Waris Shah in the 18th century.

== Cast ==

{|class="wikitable"
|-
!Actor/Actress
!Role
|- Swaran Lata||Heer
|- Inayat Hussain Ranjha
|- Malki (Heers mother)
|- Kaidon (Heers uncle)
|- Saida Khera (Heers husband)
|- Zeenat Begum||Sehti (Saidbs sister)
|- Imdad Husain||Bhangi
|}

== Music ==

The music is composed by Safdar Husain with playback singers, Inayat Hussain Bhatti, Munawar Sultana, Imdad Husain and Zubaida Khanum.  

== See also ==

*Heer Ranjha (1932 film)
*Heer Sial (1938 film)
*Heer Sial (1965 film)

== References ==
 

 
 
 
 


 