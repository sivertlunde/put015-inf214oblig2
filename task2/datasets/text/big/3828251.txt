Forces of Nature
 
{{Infobox Film
| name           = Forces of Nature
| image          = ForcesOfNaturePoster.jpg
| caption = Theatrical release poster
| director       = Bronwen Hughes
| writer         = Marc Lawrence
| producer       = Susan Arnold Ian Bryce Donna Roth
| starring       = Ben Affleck Sandra Bullock Maura Tierney Steve Zahn Blythe Danner Ronny Cox John Powell
| cinematography = Elliot Davis  David Stockton Craig Wood
| studio         = Roth–Arnold Productions
| distributor    = DreamWorks Pictures
| released       = March 19, 1999
| runtime        = 105 min.
| country        = United States
| language       = English
| budget         = $75 million (estimated) {{cite web
| title = Forces of Nature at Box Office Mojo
| url = http://www.boxofficemojo.com/movies/?id=forcesofnature.htm
| accessdate = 2010-09-29
}}
 
| gross          = $93,888,185   
}}

Forces of Nature is a 1999 romantic comedy film, directed by Bronwen Hughes, and starring Ben Affleck and Sandra Bullock.

==Plot==
 
Ben Holmes (Ben Affleck|Affleck) is a "blurb" writer responsible for writing the short introductions on the sleeves of hardcover books. On his way to Savannah, Georgia for his wedding with Bridget (Maura Tierney), hes already nervous about flying. His nerves arent helped when hes seated next to Sarah (Sandra Bullock|Bullock), a free-spirited person who seems to get on his nerves. On takeoff, a bird flies into one of the engines, causing a flameout. Now afraid to fly, he reluctantly agrees to travel with Sarah, who also needs to get to Savannah within a few days. 

During the course of their trip, things seem to keep happening to prevent them from getting to their destination — from being on the wrong car of a train to getting caught in various thunderstorms. However, Ben is impressed by Sarahs easy spirit, and starts to feel a connection with her. As they get closer to their destination, Ben starts to wonder if hes making the right choice in getting married to Bridget, or if he should just let fate take him with Sarah, who has a secret of her own. Ben and Bridget still agree to get married since they realize even after everything that has happened, they only want each other. Sarah witnesses this exchange and slips away to go find her child and move on with her life.

==Cast==
* Ben Affleck as Ben Holmes
* Sandra Bullock as Sarah Lewis
* Maura Tierney as Bridget Cahill
* Steve Zahn as Alan
* Blythe Danner as Virginia Cahill
* Ronny Cox as Hadley Cahill
* Michael Fairman as Richard Holmes
* Richard Schiff as Joe
* Afemo Omilami as Cab Driver
* David Strickland as Steve Montgomery
* Jack Kehler as Vic DeFranco
* Janet Carroll as Barbara Holmes
* Meredith Scott Lynn as Debbie
* George D. Wallace as Max
* Steve Hytner as Jack Bailey John Doe as Carl Lewis
* Anne Haney as Emma
* Bert Remsen as Ned
* Bill Erwin as Murray

==Reception==
Forces of Nature received mixed reviews from critics, as the movie currently holds a 46% rating on Rotten Tomatoes based on 65 reviews.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 