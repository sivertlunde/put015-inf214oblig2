Film in Which There Appear Edge Lettering, Sprocket Holes, Dirt Particles, Etc.
{{Infobox film
| name           = Film in Which There Appear Edge Lettering, Sprocket Holes, Dirt Particles, Etc.
| italic title   = force
| image          = 
| image size     = 
| caption        = 
| director       = Owen Land  (as George Landow) 
| producer       =
| writer         =
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| filmed         = 
| released       = 1966
| runtime        = 6 min
| country        = United States
| language       = English
| budget         =
}} experimental short film directed by Owen Land.

==Description==
Film in Which There Appear... is a six-minute loop of the double-printed image of a blinking woman;  her image is off-centre, making visible the sprocket holes and edge lettering on the film. According to Land, there is some slight variation in the image onscreen, but "no development in the dramatic or musical sense." MacDonald, Scott (2008) Canyon Cinema: the life and times of an independent film distributor, University of California Press, p308  Lands intention was to focus attention on the components that film viewers are not supposed to, and do not usually, notice, such as scratches, dirt particles, edge lettering, and sprocket holes.  For this reason, Land often scheduled the film first in screenings of his work. 

==Production==
Land created the film to mock the idea of watching a film that doesnt change.  The film began life as a   expansion of the same loop, which he claims "looks better because its more of a horizontal film than a vertical film; you look across it, not into it." 

==Reception==
Film in Which There Appear... is considered an important work in the   found object, a looped test film that focuses attention on the medium and the viewer."   Camper, Fred (2010) Chicago Reader, 2 Aug 2010  J. Hoberman called the film "blandly presented." Hoberman, J. (2003) The magic hour: film at fin de siècle, Temple University Press, p89  Juan A. Suárez noted the films unique element of "indeterminacy and open-endedness," remarking that the more the film is projected, the more scratches and dust it will collect. 

==Notes==
 

==References==
 

==External links==
*  

 
 
 
 
 