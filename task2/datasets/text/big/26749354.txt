The Girls of Huntington House
{{Infobox film
| name           = The Girls of Huntington House
| image          =
| image_size     =
| caption        = 
| director       = Alf Kjellin
| producer       = Robert L. Jacks
| writer         = Blossom Elfman (novel) Paul Savage
| narrator       =  William Windom Sissy Spacek
| music          =
| cinematography = Andrew Jackson
| editing        = Gene Fowler, Jr. Marjorie Fowler
| studio         = ABC
| released       = February 14, 1973
| runtime        = 74 minutes USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1973 television film directed by Alf Kjellin. The film is based on the 1972 novel of the same name written by Blossom Elfman.

== Plot ==
Anne Baldwin arrives at Huntington House, a maternity home for pregnant minors. The schools principal Doris McKenzie is initially reluctant to hire Baldwin, because she has no degree. In class, Baldwin soon finds out she is unable to get through the teens, of whom most are rebellious and aggressive. She constantly clashes with the schools newest student, 17-year-old Sara. Sara has been forced by her parents to enter the home and has been assigned the roommate of Gail, a model student who has a snobbish, dominant mother. Sara feels Baldwin is too old-fashioned, too introverted and does not know the meaning of love and sex.

Inspired by Saras attempts to make her open up, Baldwin introduces a more personal, social teaching structure. From this point, she bonds with several students, including Marilyn, who gives birth to a baby. Following a fight, Sara is fed up with Huntington House and leaves. Baldwin tries to stop her, but this results into a confrontation, during which Baldwin slaps Sara. During her absence, Baldwin decides not to report her to the authorities and after Saras return, they become friends.

Nevertheless, Saras attitude has become worse, having become depressed and constantly announcing she will give up her baby immediately after its birth. Baldwin knows that, secretly, Sara want to keep the baby, but she is pressured by her parents to give it up. On graduation day, Gail is reluctantly taken home by her vicious mother, who feels Gail is not treated the proper way at Huntington House. Sara announces she will not give up the baby, and, encouraged by Baldwin, leaves with her boyfriend Sandy instead. Baldwin is also inspired to follow her heart and admits to her romantic feelings for widow Sam Dutton, to whom she finally commits.

==Cast==
*Shirley Jones as Anne Baldwin
*Mercedes McCambridge as Doris McKenzie
*Pamela Sue Martin as Gail Dorn William Windom as Sam Dutton
*Sissy Spacek as Sara
*Katherine MacGregor as Rose Beckwith

==References==
 

==External links==
* 

 
 
 
 
 
 
 