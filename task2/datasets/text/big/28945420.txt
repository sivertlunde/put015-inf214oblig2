Dolphin Tale
{{Infobox film
| name           = Dolphin Tale| image          = Dolphin Tale Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Charles Martin Smith Andrew A. Kosove
| writer         = Karen Janszen Noam Dromi
| starring       = Harry Connick, Jr Ashley Judd Nathan Gamble Kris Kristofferson Cozi Zuehlsdorff Morgan Freeman
| music          = Mark Isham
| cinematography = Karl Walter Lindenlaub
| editing        = Harvey Rosenstock
| studio         = Alcon Entertainment  Arc Productions
| distributor    = Warner Bros. Pictures Summit Entertainment 
| released       =  
| runtime        = 113 minutes 
| country        = United States
| language       = English
| budget         = $37 million   
| gross          = $95.4 million   
}} family drama film directed by Charles Martin Smith (his first directed film since 2008) from a screenplay by Karen Janszen and Noam Dromi and a book of the same name. It stars Nathan Gamble, Harry Connick, Jr., Ashley Judd, Kris Kristofferson, Cozi Zuehlsdorff in her big screen debut, and Morgan Freeman.  The book and film are inspired by the true story of Winter (dolphin)|Winter, a bottlenose dolphin that was rescued in December 2005 off the Florida coast and taken in by the Clearwater Marine Aquarium. Winter lost her tail after becoming entangled with a rope attached to a crab trap and was fitted with a prosthetic one. 
A sequel, Dolphin Tale 2 was released on September 12, 2014.   

==Plot==
 

Sawyer Nelson (Nathan Gamble), a lonely 11-year-old, has fallen behind in school since being abandoned by his father five years earlier. His only friend is his cousin Kyle (Austin Stowell), a champion swimmer who hopes to compete in the Olympics.

One day Sawyer finds a fisherman (Richard Libertini) trying to help an injured dolphin tangled in a crab trap. The dolphin is taken for treatment to Clearwater Marine Hospital (CMA), run by Dr. Clay Haskett (Harry Connick Jr.). Clays daughter Hazel (Cozi Zuehlsdorff) names the dolphin Winter, following the theme of two prior dolphins, Summer and Autumn, who were successfully treated and returned to the ocean. Sawyer sneaks in to see Winter (with an assist from Hazel) and later starts visiting daily, being harassed by an obnoxious roof-dwelling pelican named Rufus. Their respective parents disapprove at first (Dr. Clay because Sawyer has no experience with marine animals, and Sawyers mother, Lorraine (Ashley Judd), because Sawyer is skipping summer school to visit Winter); but they are won over by the fact that the friendship seems to benefit both Winter and Sawyer (Winter feels better with Sawyers presence, and Sawyer is becoming more confident and strong). Dr. Clay allows the visits to continue, and Lorraine withdraws Sawyer from summer school and lets him volunteer at CMA.

Unfortunately, Winters tail is damaged and must be amputated. Winter learns to swim without a tail by developing a side-to-side motion like a fish, but after an x-ray Clay notices the unnatural motion is causing stress on her spine; if continued the motion will cripple and eventually kill her.

The news comes that Kyle has been injured in an explosion and is coming home for treatment. Sawyer is excited to see him, but devastated when Kyle, depressed, skips his own welcome-back party and stays at the local Department of Veterans Affairs Medical Center, where Dr. Cameron McCarthy (Morgan Freeman) is developing a prosthetic leg for Kyle. Sawyer and his mother visit Kyle there but Kyle asks them to leave, which shocks and infuriates Sawyer. Kyle takes Sawyer on a walk for a deeper talk about his leg. Sawyer then asks Dr. McCarthy if he can work on making a prosthetic tail for Winter. McCarthy agrees to do so and convinces his prosthetic supplier (Hanger Prosthetics and Orthotics, which supplies Winters real-life tails) to supply the parts at no cost. Dr. McCarthy manufactures a "homemade" model tail while waiting for the real one to arrive; however, Winter rejects it and destroys it by banging it against the pool wall.

Meanwhile Kyle gets more depressed when his friend and swimming partner, Donovan Peck, beats Kyles old swimming records. Dr. McCarthy hears about it and persuades Kyle to go home.

Then CMA, already in financial peril, is damaged by a hurricane. The board of directors agree to close CMA, sell the land to a real estate developer, and find homes for all the animals except Winter, who due to her condition is not wanted by anyone and may have to be euthanized. Kyle visits CMA and sees that Winter is like him, with a damaged limb. However, inspired by a girl with a prosthetic limb from Atlanta, Georgia who - inspired by Winters story of overcoming disability - comes to visit Winter, Sawyer envisions an event called "Save Winter Day" to save the facility. Clay is not sold on the idea, but reconsiders after talking with his father, Reed (Kris Kristofferson). Kyle agrees to a race against Donovan Peck and persuades Bay News 9 to cover the event.

The Hanger-supplied tail finally arrives; however, Winter destroys it as well. Sawyer then figures out what the real problem is: the plastic base for the tail is irritating her skin. So Dr. McCarthy develops an alternative gel-like sock which he calls "Winters Gel" (the real-life name of the Hanger product used to attach prosthetic limbs, which was developed during its research with Winter). Winter finally accepts this newest prosthetic tail.

At Save Winter Day, the work with Winter impresses everyone. Sawyers teacher gives him school credit, allowing him to pass summer school. Also, the real estate developer decides to keep CMA open and financially support it. With Winters help, Kyle then wins a swimming race against Donovan.

The ending shows documentary footage from Winters actual rescue. It then shows several of the prosthetic tails that Winter has worn, and scenes from real amputees who have visited Winter at the Clear water Marine Aquarium.

==Cast==
* Harry Connick Jr. as Dr. Clay Haskett, the operator of the Clearwater Marine Aquarium in Clearwater and Hazels father.
* Ashley Judd as Lorraine Nelson, Sawyers mother and a nurse. Winter and cuts the crab trap off her. And he also becomes Winters "dad" and friend. Winter as herself, an injured dolphin that must have part of her fluke amputated. Despite that, she adapts and swims side-to-side. But that figures to be bad for Winters spine, hence the fake tail (which allows her to swim naturally.)
* Kris Kristofferson as Reed Haskett, Clays father and Hazels grandfather.
* Morgan Freeman as Dr. Cameron McCarthy, a prosthetic designer and Kyles doctor at the VA Hospital. Jim Fitzpatrick as Max Connellan, Kyles father and Sawyers uncle.
* Cozi Zuehlsdorff as Hazel Haskett, an 11-year-old girl and the daughter of Clay and granddaughter of Reed. Ray McKinnon as Mr. Doyle, Sawyers teacher.
* Austin Stowell as Kyle Connellan, Sawyers cousin.
* Michael Roark as Donovan Peck, a friend of Kyles.
* Frances Sternhagen as Gloria Forrest
* Austin Highsmith as Phoebe, the trainer of Clearwater Marine Aquarium.
* Betsy Landin as Kat, one of the Clearwater Marine Aquarium dolphin specialists.
* Tom Nowicki as Philip J. Hordern, a real estate developer.

==Differences between the movie and actual events== New Smyrna Beach―part of the Cape Canaveral National Seashore. The fisherman who discovered her was in the lagoon as well. Winter was first taken to the local Marine Discovery center and then transferred to Clearwater, which is on the opposite side of the state.    In the movie it is mentioned that Winters tail was amputated due to infection caused by the tail being caught in the rope. In real life, the loss of blood supply to the tail (from being caught in the rope) caused most of the tail to naturally fall off, with a small piece being amputated.  Hanger Clinic.  

==Production== Tarpon Springs, and local news station Bay News 9. 

==Release== Warner Bros. Pictures and Alcon Entertainment. The film was released in RealD 3D as well as 2D.

The movie was released on DVD and Blu-ray on December 20, 2011.

==Reception==
===Box office===
The film opened at #3 with $19.2 million behind the 3D re-release of The Lion King and Moneyball (film)|Moneyball.  In its second weekend, the film reached the #1 spot, dropping only 27%, and grossed $13.9 million.  As of January 5, 2012, the film has grossed $72,286,779 in the United States and Canada as well as $23,117,618  internationally bringing its worldwide total to $95,404,397. 

===Critical reception===
The film received positive reviews from critics. Review aggregator Rotten Tomatoes reports that 82% of 106 critics have given the film a positive review, with a rating average of 6.5 out of 10.  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 64 based on 31 reviews. 

===Awards===
{| class="wikitable" rowspan="8;" style="text-align:center; background:#fff;"
|-
!Award !! Category !! Recipient(s) !! Result !! Ref.
|- Young Artist Young Artist Best Performance Nathan Gamble|| ||rowspan=2|   
|- Best Performance Cozi Zuehlsdorff|| 
|}

==Sequel==
Main article: Dolphin Tale 2

A sequel titled Dolphin Tale 2 was released on September 12, 2014.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 