The Cape Town Affair
{{Infobox film
| name           = The Cape Town Affair
| director       = Robert D. Webb
| producer       = Robert D. Webb Dwight Taylor
| based on       =
| screenplay     = Samuel Fuller Harold Medford
| narrator       =
| starring       = Claire Trevor James Brolin Jacqueline Bisset
| music          = Bob Adams Joe Kentridge
| cinematography = David Millin
| editing        = Peter Grossett
| studio         = Killarney Film Studios
| distributor    = 20th Century Fox
| released       =  
| runtime        = 100 min.
| country        = United States
| language       = English
}}

The Cape Town Affair is director Robert D. Webbs 1967 glamorized spy film produced by 20th Century Fox at Killarney Film Studios in South Africa. The film stars Claire Trevor, James Brolin and Jacqueline Bisset. The film is a remake of the 1953 picture Pickup on South Street.

==Plot==
 
South African secret agents try to save a confidential microfilm before the Communists get hold of it.

==Cast==
* Claire Trevor as Sam Williams
* James Brolin as Skip McCoy
* Jacqueline Bisset as Candy
* Claire Trevor as Sam Williams  
* Bob Courtney as Capt. Herrick, Security Branch   John Whiteley as Joey  
* Gordon Mulholland as Warrant Officer Du Plessis  
* Siegfried Mynhardt as Fenton  
* James Gordon White as Sgt. Beukes  
* Gabriel Bayman as Mohammed the Fence  
* Raymond Matuson as Lighting Louis / Espinosa 
* Patrick Mynhardt as Myburgh 

==Production== Long Street, Green Point, railway station city hall.

==Reception==
Commentators describe the film as dull, slow-paced, poorly acted and tedious. The film does, however, paint an interesting picture of life in South Africa under apartheid as seen from the point of view of official government policy. All the leading characters are white and even street scenes contain few non-whites.

==See also==
* The Jackals

==External links==
*  
*  

 

 
 
 
 
 
 
 
 