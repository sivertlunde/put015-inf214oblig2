Avenged (2013 film)
 
{{Infobox film
| name           = Avenged
| image          = 
| caption        = 
| director       = Donovan Marsh
| producer       = {{plainlist|
* J.P. Potgieter
* Harriet Gavshon
}}
| writer         = Donovan Marsh
| starring       = {{plainlist|
* Sdumo Mtshali
* Presley Chweneyagae
* Israel Makoe
* Owen Sejake
}} 
| music          = Brendan Jury
| cinematography = Tom Marais
| editing        =  Donovan Marsh
| distributor    = Indigenous Film Distribution
| released       =  
| runtime        = 96 minutes
| country        = South Africa
| language       = Zulu
| budget         = $183,927 (South Africa) 
}}
Avenged (originally titled iNumber Number) is a 2013 South African action film written and directed by Donovan Marsh. It was screened in the Contemporary World Cinema section at the 2013 Toronto International Film Festival.      

== Plot ==
Chili Ncgobo, an undercover cop, blows his cover and is captured by a gang.  Chili frees himself and radios his partner for help, not realizing that Shoes pistol has been sent in for repair.  Shoes insists they call for backup, but Chili refuses, as he does not want to share the large reward for the gangs leader.  With Shoes tactical guidance, Chili takes down the entire gang despite having only two bullets.  When they bring in the gang leader, Captain Stone withholds the reward unless they will set free a suspected rapist who is politically connected.  Shoes is surprised when Chili suggests that they agree to the deal, and he refuses to compromise his principles.

Chili later comes to Shoes for help.  Mambane, a crime boss that the two have been investigating for several years, has a major heist planned.  Initially assuming that Chili intends to infiltrate the gang, Shoes comes to realize that Chili instead wants to cut him into the heist itself, after which can they arrest the gangsters at their leisure.  Shoes reluctantly agrees after Chili promises that no one will be hurt.  However, one of the gangsters, Skroef, becomes suspicious when he recognizes Chili from another undercover operation that resulted in his arrest.  The other crew consists of G8 and Warren, two mercenaries who worked together in an unsuccessful heist and now hate each other; Stakes and his lover Gugu, who is Mambanes daughter; Kenny, a failed actor who knows explosives; and Slim, a dim gangster.  Knowing that he needs to get rid of Skroef, Chili sends Shoes to arrest him.

The next day, while the criminals discuss the heist, Skroef shows up with Shoes, whom he has captured, and says that one of the crew is an undercover cop.  Chili convinces Mambane that their inside man, a security guard, is the culprit.  Their ire now redirected toward the guard, Chili convinces Mambane to spare Shoes life to use him as a hostage despite Skroefs insistence that they kill Shoes immediately.  While the others are asleep, Chili tries to slip a small knife to Shoes, who says he will not go along with any plan that involves the murder of a security guard.  Skroef catches the two talking and instantly attacks Chili, whom he accuses of being an undercover cop.  Mambane threatens to kill both men but admits that he needs them to perform the heist.

Worried that the police may be on to their plans, Mambane advances the heist to the next day over the objections of G8, who advises that they abort.  At the last moment, Chili has second thoughts and attempts to warn off the armored truck that they have targeted.  Although the heist is successful, several members of the crew die, and G8 is wounded.  When they return to their base, Stakes and Gugu betray the others and attempt to run off with the money; as Skroef kills both, Chili arrives and takes the money.  G8 stalks Chili while Mambane and Skroef attempt to kill Shoes, who has overpowered Kenny and taken his weapon.  Using radios, Shoes and Chili warn each other of enemy movements and kill all their opponents but Skroef.

Skroef uses a radio to locate Shoes position and wounds him.  Desperate to save his partner, Chili leaps from a higher floor onto Skroef, using the money bag as both a shield and cushion.  Both men are temporarily stunned by the collision, though Chili recovers enough to use the money bag to smother Skroef.  Chili realizes that the money is finally his.  However, the next day, he goes to the bank and drops the money bag off.  The film ends as he and Shoes return to their jobs as policemen.

== Cast ==
* Sdumo Mtshali as Chili Ncgobo
* Presley Chweneyagae as Shoes
* Israel Makoe as Skroef
* Owen Sejake as Mambane
* Warren Masemola as G8
* Brandon Auret as Warren
* Hlubi Mboya as Gugu
* Daniel Hadebe as Moses
* Percy Matsemela as Slim
* Ronnie Nyakale as Captain Stone

== Reception ==
At   called it "entertainment of the highest order".   Scott A. Gray of Exclaim! described it as a "flashy and forgettable police caper" reminiscent of Reservoir Dogs. 

== Remake ==
A Hollywood remake was announced in October 2013. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 