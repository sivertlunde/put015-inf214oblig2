Nine Miles Down
{{Infobox film
| name           = Nine Miles Down
| image          =
| image_size     =
| caption        =
| director       = Anthony Waller 
| producer       =
| writer         =Everett de Roche
| based on       = 
| starring       = Adrian Paul Kate Nauta
| music          =
| cinematography =
| editing        =
| distributor    = 
| released       =2009 
| runtime        =
| country        = United States UK
| language       = English
| budget         =
| gross          =
}}
Nine Miles Down is a 2009 horror film   that based on the widespread urban myth (started in the early 90s) surrounding the claim that Russian scientists in Siberia had drilled so deep that they had broken through into hell and recorded the screams of the damned emanating from the borehole. This claim turned out to be a hoax spread by a Scandinavian prankster who wanted to demonstrate human gullibility. However, by the time he owned up to the prank, the myth had fueled such a following, that his retraction was interpreted as an attempt to cover up the truth.  It was the last feature film credit for renowned writer Everett de Roche. 

==Plot==
In the Sahara desert, a sandstorm batters a deserted drilling station. Thomas "Jack" Jackman a security patrolman, battles through the high winds to find out why all contact with the station has been lost. Originally built for gas exploration, and then abandoned, the site had recently been taken over by a multi-national research team intent on drilling deeper into the earths crust than ever before.   

==Cast==
*Adrian Paul as Thomas Jackmann
*Kate Nauta as Dr. Christensen
*Anthony Waller as Professor Borman
*Amanda Douge as Kat
*Meredith Ostrom as Susan
*Arcadiy Golubovich as Alex

==Trivia==
In 1995 Nine Miles Down was in preproduction with Spelling Productions with Thom Mount as producer and John Carpenter slated to direct. John Carpenter left the production to make Escape from L.A. (1996) and Spelling lost the project. 

Val Kilmer was in talks to star in the movie in May 2002.  

==References==
 
==External links==
*  at IMDB

 