Capturing Reality: The Art of Documentary
Capturing Reality: The Art of Documentary is a film and website about documentary filmmaking, directed by Pepita Ferrari. Produced by the National Film Board of Canada, Capturing Reality explores the creative process of over 30 leading documentary filmmakers, combining interviews with excerpts from their films.      

The film had its world premiere at the International Documentary Film Festival Amsterdam in November 2008.      

Ferrari began work on the project in 2007. The website features four hours of additional material that does not appear in the film.   

==Filmmakers interviewed (in alphabetical order)==
{| 
|
*Jennifer Baichwal
*Manfred Becker
*Michel Brault
*Nick Broomfield
*Joan Churchill
*Eduardo Coutinho Paul Cowan
*Jean-Xavier de Lestrade
*Molly Dineen
*Jennifer Fox
*Denis Gheerbrant
|
*Serge Giguère
*Patricio Guzmán
*Werner Herzog
*Scott Hicks
*Heddy Honigmann
*Sylvain L’espérance
*Jean-Pierre Lledo
*Kim Longinotto Kevin Macdonald
*Albert Maysles
*Errol Morris
|
*Stan Neumann
*Alanis Obomsawin
*Laura Poitras
*Velcrow Ripper
*Hubert Sauper Rakesh Sharma Barry Stevens
*Sabiha Sumar
*Nettie Wild
*Peter Wintonick
*Jessica Yu
|}

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 

 
 