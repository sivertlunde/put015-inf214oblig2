Prince (1969 film)
 
{{Infobox film
| name           =Prince
| image          = prince_1969.jpg
| image_size     = 
| caption        = 
| director       = Lekh Tandon
| producer       = F. C. Mehra
| writer         = Abrar Alvi
| narrator       =  Ajit Rajendra Nath
| music          = Shankar Jaikishan
| cinematography = Dwarka Divecha
| editing        = Pran Mehra Vijaya Studio Mehboob Studio
| distributor    = Eagle Films
| released       =  
| runtime        = 
| country        = India
| language       = Hindi/Urdu
| budget         = 
| gross          =   4,00,00,000
}}

Prince is a 1969 Hindi movie produced by F. C. Mehra and directed by Lekh Tandon. The film stars Shammi Kapoor with Vyjayanthimala in the lead while Rajendranath, Ajit Khan|Ajit, Helen (actress)|Helen, Leela Chitnis and Asit Sen forms an ensemble cast. The films music was composed by Shankar Jaikishan with lyrics by Hasrat Jaipuri and Faruk Qaiser.  Prince is about a drama set in the times of resurgent India when the nation threw off the British Yoke, but some states still languished under the Princely yoke. It is the story of a Prince who brought about his own downfall so that he may rise as a human being.

==Plot==
Rajkumar Shamsher Singh is the only son of the local Maharaja, and has been brought up as a brat, and now he is an irresponsible, alcoholic, and womanizing adult, who wants everyone to bow down before him and his princely rank. One priest refuses to do so, and Shamsher pummels him mercilessly, in vain though. Frustrated, he asks the priest what he should do with his mundane life, and the priest tells him that he should repent, sacrifice all his palatial pleasures, and live the life of a simple and ordinary man, and hence learn the true meaning of life, for at least six months. Shamsher agrees to do so, and arranges an accident with his car, which plummets down a mountain, explodes and is blown to smithereens. Everyone in the palace believes that Shamsher is dead. He goes to a nearby village, and a blind woman there mistakes him for her long-lost son and starts calling him Sajjan Singh. Shamsher decides to play along as Sajjan.

Two corrupt palace officials spot Sajjan, and notice his similarity to Shamsher, and conspire with him to pose as Shamsher for a hefty sum of money, to which Sajjan agrees. When he accompanies the officials back to the palace, he is shocked to find that his father has re-married a much younger woman, Ratna, and shortly after marrying her, has died, leaving the palace and its management to her and her greedy brother. Shamsher decides to reveal his true identity, but the officials threaten to expose him to his new-found blind mother, and Shamsher knows that he is trapped in the body of Sajjan Singh, forced to pose as none other than himself.

==Cast==
* Shammi Kapoor as Prince Shamsher Singh
* Vyjayanthimala as Princess Amrita
* Rajendra Nath as Vilayatiram
* Ajit as Ratnas Brother Helen as Sophia
* Leela Chitnis as Mrs. Shanti Singh
* Parveen Choudhary as Ratna Sudhir as Sajjan Singh Sunder as Zoravar Rashid Khan Zorawar Singh
* D. K. Sapru as The King of Jamnapur
* Ulhas as The King of Ramnagar
* Leela Mishra as Kamla David Abraham as Diwan
* Randhir as Michael

==Production Team==
F C Mehra and Lekh Tandon had previously worked with Vyjayanthimala in the historical film Amrapali (1966).The team of producer F C Mehra, director Lekh Tandon, actor Shammi Kapoor and musicians Shankar Jaikishan had earlier worked together on the hit film Professor (film)|Professor (1962).  FC had also worked with Shammi earlier in Mujrim (1958), Ujala (1959) and Singapore (1960 film)|Singapore (1960).

==Soundtrack==
{{Infobox album
| Name = Prince
| Type = soundtrack
| Artist = Shankar Jaikishan
| Cover =
| Released = 1969
| Recorded = Minoo Katrak
| Genre = Film soundtrack
| Length =  The Gramophone Company of India
| Producer = Shankar Jaikishan
| Last album = Pyar Hi Pyar  (1969) 
| This album = Prince
| Next album = Jahan Pyar Mile  (1969) 
}}
 duo  Helen respectively. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Picturization || Length (m:ss) ||Lyrics ||Notes
|-
| 1|| "Badan Pe Sitare Lapete" ||   ||
|-
| 2|| "Madhosh Hawa Matwali" || Mohammad Rafi || Shammi Kapoor || 09:58 || Faruk Qaiser || 
|- Helen || 07:53 || Hasrat Jaipuri ||
|-
| 4|| "Nazar Mein Bijili" || Mohammed Rafi ||  Vyjayanthimala and Shammi Kapoor || 04:19 || Faruk Qaiser ||
|-
| 5|| "Thandi Thandi Hawa" || Lata Mangeshkar and chorus || Vyjayanthimala and Shammi Kapoor || 04:42 || Hasrat Jaipuri || 
|-
| 6|| "Bachke Jane Na Doongi" || Lata Mangeshkar, Mohammad Rafi || Shammi Kapoor and Vyjayanthimala || 05:30 || Faruk Qaiser ||
|}

==Box office== fourth highest grossing film of 1969 with verdict hit at box office. 

==References==
 

== External links ==
*  
*   at Upperstall.com

 
 
 
 
 