Joshua Then and Now (film)
{{Infobox film| name = Joshua Then and Now
  | image = joshua_then_and_now-film-halfsheet.jpg
  | caption = Joshua Then and Now
  | director = Ted Kotcheff 
  | producer = Robert Lantos Stephen J Roth Alliance Communications
  | screenplay = Mordecai Richler
  | based on  =  Richler
  | starring =James Woods Gabrielle Lazure Alan Arkin
  | music = Philippe Sarde
  | cinematography = Francois Protat
  | editing = Ron Wisman
  | distributor = 20th Century Fox 1985 (Canada/U.S. release)
  | runtime = 119 minutes
  | country = Canada
  | language = English
  | budget = est. $11 million
  }} The Apprenticeship of Duddy Kravitz.

The film depicts Joshua growing up in his Montreal neighborhood, and then his adventures as a modestly successful writer.  He marries the "golden shiksa" of his dreams, but eventually everything around him crumbles and he must act quickly to recover it all.  A comedic drama, the film moves quickly without lingering for long on any incident and tells a connected complete narrative.  Alan Arkin is frequently noted in reviews for an outstanding performance.
 Ken Campbell as Sidney Murdoch, Kate Trotter as Jane Trimble, Alexander Knox as Senator Hornby, and Eric Kimmel as young Joshua.  Filmed on location in Montreal, London, Brockville, Ontario|Brockville, and Ottawa.  Rated R.  It has been transcribed to VHS (1986) and DVD.

==Plot==
Joshua Shapiro, successful writer and pundit, in a hospital room, seems to have lost his wife and is in the middle of a sex scandal.  Compelled to find meaning in his life, he reviews it from his youth to the present day.
 Bar Mitzvah party for him. Joshuas father is revealed to have a unique perspective on life, sex, and religion.

A trip to Spain as a young man is the impetus that sparks a career as a journalist and writer.  In England in a momentary lapse of reason, Joshua forges letters about a (fake) homosexual affair with a British writer to sell to an American university archive.  He meets an upper-class Canadian married to a poseur of a communist and steals her away to become his own wife.  She is the daughter of a Canadian senator and Joshuas key into a level of society of which he is contemptuous.

In the meantime, Joshuas childhood friends have become successful in their own right.  Some become targets for bizarre pranks as he settles various scores.

Joshuas conceited brother-in-law assumes a pivotal role in the novel as it is revealed that he is insecure and vulnerable. Neighbors in the wealthy cottage community around Lake Memphremagog lead him astray with dreadful consequences. Past indiscretions rear their ugly heads and Joshua must put together the shambles of his life.

==Cast==
* James Woods - Joshua Shapiro
* Gabrielle Lazure - Pauline Shapiro
* Alan Arkin - Reuben Shapiro
* Michael Sarrazin - Kevin Hornby
* Linda Sorenson - Esther Shapiro
* Alan Scarfe - Jack Trimble
* Ken Campbell - Sidney Murdoch
* Kate Trotter - Jane Trimble
* Alexander Knox - Senator Hornby
* Eric Kimmel - Young Joshua Shapiro
* Chuck Shamata - Seymour Kaplan
* Yuval Kernerman - Young Seymour Kaplan
* Robert Joy - Colin Fraser
* Harvey Atkin - Dr. Jonathan Cole
* Paul Hecht - Eli Seligson

== Awards ==
* Won five Genie Awards in 1986 including Best Supporting Actor (Arkin), Best Supporting Actress (Sorensen), Best Cinematography, Best Art Direction, and Best Costume Design. 
* Nominated for 12 Genie awards 1986, including Best Picture, Best Director, and Best Screenplay.
* Canadas entry at the 1985 Cannes Film Festival.   
* Sante Fe Film Festival, 5th (2004), Tributee Salutes section. 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 