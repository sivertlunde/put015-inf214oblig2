Dark Moon Rising
 
 
Dark Moon Rising is a horror film made in 2009 directed by Dana Mennie who also co-wrote the movie with Ian Cook. The film is about a small town girl Amy, played by Ginny Weirick who falls for wanderer Chris DiVeccio. The film also stars María Conchita Alonso and Max Ryan as two local townspeople who try to save the town from destruction. 

== Plot ==
Dark Moon Rising, commonly known as Wolf Moon, is a horror film about a girl named Amy, who falls in love with the new boy in town. Dan is the drifter from out of town who carries a dark secret with him. 
In the beginning, Amys friends, tell her to go talk to Dan, who is working in an auto shop. He treats Amy with disinterest until he realizes that he has hurt her feelings. He asks to give her a ride home. Amys father doesnt trust Dan from the beginning. Dans a drifter, so people automatically dont trust him. But, the reason people dont trust him is because of the vibes he gives off. Amy and Dan get to know each other as the movie progresses. Meanwhile, Dans father, Bender, is out killing people only a few states over. Dans dad eventually shows up and kills a dog along with a horse. The sheriff, played by María Conchita Alonso, and Amys dad investigate the murders and eventually connect them to the killings from other states. The two eventually discover Charles Thibodeaux, played by Billy Drago. Charles Thibodeaux tells them that he knows Bender from a while ago and Bender is a werewolf. Charles put Bender in jail a while back, and he escaped seeking revenge. Bender killed Charles wife brutally and Charles never got over it.

== Characters ==
*Max Ryan as Darkman/Bender
*María Conchita Alonso as Sam
*Chris Mulkey as John
*Sid Haig as Crazy Louis
*Chris Diveccio as Dan
*Billy Drago as Charles Thibodeaux
*Lin Shaye as Sunny
*Arielle Vandenberg as Nicole
*Rikki Gagne as Stacey
*Norman Mora as Chico
*Ginny Weirick as Amy

==About the director==
Dark Moon Rising is Dana Mennies debut film.  Dana Mennie also wrote, produced, and starred in Dark Moon Rising.   Dana was at first against casting Chris DiVeccio as Dan, but Chris had a friend that was close to Dana and he was eventually persuaded. 

==Reviews==
Although, the movie did not make much money and is still considered a small-time film, it has developed its own little fan club. Dark Moon Rising has also been accused of "capitalizing on the Twilight Saga" by many different sources. 

==References==
 

== External links ==
* 

 
 