Rage of Honor
{{Infobox film
| name           = Rage of Honor
| director       = Gordon Hessler
| producer       = Don Van Atta Robert Short Wallace C. Bennett
| starring       = Sho Kosugi Lewis Van Bergen Robin Evans Richard Wiley Ulises Dumont Gerry Gibson
| music          = Stelvio Cipriani
| cinematography = Julio Bragado
| editing        = Robert Gordon
| studio         = Trans World Entertainment
| distributor    = Trans World Entertainment
| released       = February 27, 1987
| runtime        = 98 minutes United States Argentina English Spanish Spanish
}}

Rage of Honor is an American martial arts film directed by Gordon Hessler, and starring Sho Kosugi.   

==Cast==
*Sho Kosugi – Shiro Tanaka 
*Lewis Van Bergen – Havlock 
*Robin Evans – Jennifer Lane 
*Richard Wiley – Ray Jones 
*Ulises Dumont – Harry
*Gerry Gibson – Dick Coleman
*Martín Coria – Jorge
*Ned Kovas – Havlocks guard
*Lilian Rinar – Havlocks girlfriend
*Hugo Halbrich – Pilot / Killer
*Masafumi Sakanashi – Prison ninja
*Kiyatsu Shimoyama – Prison ninja
*Alejo Apsega – Killer in hotel
*Ezequiel Ezquenazi – Killer in hotel

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 