The Skywayman
{{Infobox film
| name           =The Skywayman
| image          =The Skywayman.jpg
| image size     =150px
| caption        =Theatrical poster James P. Hogan
| producer       =Sol M. Wurtzel
| based on       = 
| writer         =Jules Furthman (screenplay)
| narrator       =
| starring       =Ormer Locklear Louise Lovely
| music          =
| cinematography =
| editing        = William Fox Studio
| distributor    = Fox Film Corporation
| released       =   
| runtime        = 50 minutes (approximately)
| country        = United States Silent (English intertitles)
| budget         =
| gross          =
}}
 lost five-reel James P. Hogan and produced and distributed by Fox Film Corporation. The film stars noted aerial stunt pilot Ormer Locklear, and co-starring Louise Lovely. After having appeared in The Great Air Robbery (1919), a film  that showcased his aerial talents, Locklear, considered the foremost "aviation stunt man in the world", was reluctant to return to the air show circuit.  During the production, Locklear and his co-pilot Milton "Skeets" Elliot died after crashing during a night scene. The Skywayman was subsequently released shortly after, capitalizing on their deaths. 

==Plot== tailspin and crash brings Craig back to his senses. He is able to thwart the doctors schemes and finally remembers his girlfriend.

==Cast==
* Ormer Locklear as Captain Norman Craig (credited as Lt. Ormer Locklear)
* Louise Lovely as Virginia Ames
* Sam De Grasse as Dr. Wayne Leveridge
* Ted McCann as William Elmer
* Jack Brammall as "Nobby" Brooks

==Production==
  Hollywood career and in April 1920, he was signed to star in The Skywayman. 

Principal photography on The Skywayman began on June 11, 1920 with DeMille Field 2 in Los Angeles as the main base of operations, although scenes were also shot in and around San Francisco. Pendo 1985, p. 5.   Despite Locklears public claim that new stunts "more daring ever filmed" would be involved, the production would rely heavily on models and less on actual stunt flying.  Two stunts, a church steeple being toppled by Locklears aircraft and an aircraft-to-train transfer were both problematic and nearly ended in disaster.  
 William Fox Curtiss "Jenny", to be doused as the aircraft entered its final spin.To shocked spectators and film crew, Locklear and his long-time flying partner "Skeets" Elliot crashed heavily into the sludge pool of an oil well, never pulling out of the spin. Both occupants died instantly at the scene. 

With the entire film already "in the can" except for the night scene, Fox made the decision to capitalize on the crash and deaths of Locklear and Elliot, by rushing The Skywayman into production and release. Paris 1995, p. 56. 

==Reception==
With the lurid notices of The Skywayman proclaiming: "Every Inch Of Film Showing Locklears Spectacular (And Fatal) Last Flight. His Death-Defying Feats And A Close Up Of His Spectacular Crash To Earth," the film was premiered in Los Angeles.  The advertising campaign that accompanied the film was very similar to that of his earlier feature film, focussing on Locklears earlier exploits, and combining model displays, and exhibition flights across North America to coincide with the films release.  Fox Film Corporation claimed that 10% of the studio profits would go to the families of Locklear and Elliot. Farmer 1984, p. 24. 
 
The review in the Los Angeles Times noted: "The greatest monument that could be built to any man – the privilege of living on after all else has gone – is what The Skywayman showing will do. What is gone in the flesh will live on forevermore on the screen."  

In Hollywood (1980 TV series)|Hollywood (1980), a television series on the silent era, actresses Leatrice Joy and Viola Dana recalled Locklear and the making of The Skywayman. Dana describes Locklears aerial accident in the "Hazards of the Game" episode.  

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Farmer, James H. Celluloid Wings: The Impact of Movies on Aviation. Blue Ridge Summit, Pennsylvania: Tab Books Inc., 1984. ISBN 978-0-83062-374-7.
* Paris, Michael. From the Wright Brothers to Top Gun: Aviation, Nationalism, and Popular Cinema. Manchester, UK: Manchester University Press, 1995. ISBN 978-0-7190-4074-0.
* Pendo, Stephen. Aviation in the Cinema. Lanham, Maryland: Scarecrow Press, 1985. ISBN 0-8-1081-746-2.
* Wynne, H. Hugh. The Motion Picture Stunt Pilots and Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing Co., 1987. ISBN 0-933126-85-9.
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 