Chance of a Lifetime (1950 film)
{{Infobox Film
| name           = Chance of a Lifetime
| image          = 
| image_size     = 
| caption        = 
| director       = Bernard Miles
| producer       = Bernard Miles
| writer         = Walter Greenwood Bernard Miles
| starring       = Bernard Miles Basil Radford Kenneth More
| music          = Noel Mewton-Wood Eric Cross
| editing        = Peter Price British Lion,  Pilgrim Pictures
| released       = 1950 (UK) 14 March 1951 (USA)
| runtime        = 89 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} BAFTA for Best British Film, to which it was beaten by The Blue Lamp.

==Plot==
In the times of austerity after the Second World War a failing factorys owner hands the factory over to the workers to run it for themselves.

==Cast==
*Basil Radford - Dickinson
*Niall MacGinnis - Baxter
*Bernard Miles - Stevens
*Julien Mitchell - Morris
*Kenneth More - Adam
*Geoffrey Keen - Bolger
*Josephine Wilson - Miss Cooper John Harvey - Bland
*Russell Waters - Palmer
*Patrick Troughton - Kettle
*Hattie Jacques - Alice Peter Jones, Bernard Rebel and Eric Pohlmann - Xenobian trade delegation
*Amy Veness - Lady Davis
*Stanley Van Beers - Calvert
*Norman Pierce - Franklin Gordon McLeod - Garrett
*Compton Mackenzie - Sir Robert Dysart
*Nigel Fitzgerald - Pennington
*Alastair Hunter - Groves
*Mollie Palmer - Millie
*George Street and Stanley Rose - Trade Union Men
*Erik Chitty - Silas Pike
*Leonard Sharp - Mitch
*John Boddington - Bank Clerk
*Hilda Fenemore, Helen Harvey, Peggy Ann Clifford, Sam Kydd, Jim Watts, Henry Bryce, Basil Cunard, Anthony Halfpenny, Howell Davies and Donald Tandy - Workers

==External links==
* 
*  

 
 
 
 
 