Mouna Ragam
 
 
{{Infobox film
| name           = Mouna Ragam
| image          = Mouna Ragam poster.jpg
| caption        = Theatrical release poster
| director       = Mani Ratnam
| producer       = G. Venkateswaran
| writer         = Mani Ratnam
| starring       =  
| music          = Ilaiyaraaja
| cinematography = P. C. Sreeram
| editing        =  
| studio         = Sujatha Productions
| distributor    = 
| released       =  
| runtime        = 133–145 minutes  while Baradwaj Rangans Conversations with Mani Ratnam and Ashish Rajadhyaksha and Paul Willemens Encyclopedia of Indian Cinema give the runtime as 145 minutes.  }}
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Tamil Romance romantic drama film written and directed by Mani Ratnam, and produced by G. Venkateswaran. The film narrates the life of Divya Chandramouli (Revathi), who is robbed of her carefree existence when she reluctantly marries Chandrakumar (Mohan (actor)|Mohan). Divya does not wish for a married life due to her grief over her former lover, Manohar (Karthik (actor)|Karthik), being shot to death. The rest of the story follows Divyas inner conflict between holding on to her past or coming to terms with the present and uniting with Chandrakumar.

Mouna Ragams development began when Ratnam wrote a short story titled Divya, while he was making his debut film Pallavi Anu Pallavi  (1983). As the script developed he renamed it. He did not plan to film until he finished writing the story, but he could not start actual production on it until after his fourth film, Idaya Kovil (1985), was released. Mouna Ragam was mostly filmed in Chennai; additional filming took place in Delhi and Agra. The soundtrack album and background score were composed by Ilaiyaraaja, cinematography was handled by P. C. Sreeram, and the art director was Thotta Tharani. The length of the film was  .
 Telugu under Hindi in 1992 as Kasak (1992 film)|Kasak, which starred Rishi Kapoor, Neelam Kothari and Chunkey Pandey.

== Plot == heart attack, the family pleads with her to accept the proposal so that he can recover quickly. Succumbing to family pressure, Divya marries Chandrakumar.
  MP named Thamizhmani and steal his money. Considering it as a gang theft, Divya reports Manohar to the police, who arrest him. She later learns that Thamizhmanis son had run over a poor girl with his car and Manohar had stolen the money to pay for the girls treatment. Divya feels guilty and bails him out of the police station.

Manohar falls in love with Divya and tries to win her love. Divya initially rejects him but eventually reciprocates his feelings. Manohar is a member of a revolutionary group, which plans to hold an illegal rally. Divya does not approve of his participation in such activities and persuades him not to attend the rally. Manohar accepts her persuasion on the condition that she marries him. On the day of their marriage, Manohar is falsely charged for participating in the rally and is arrested by the police, but escapes and runs to the marriage registrars office where Divya is waiting for him. In the following chase, a policeman shoots Manohar and he dies in front of Divya, leaving her distraught.

Chandrakumar is ready to overlook Divyas past, but she is unable to accept another man in her life. Seeking a divorce, the pair approach a lawyer, who tells them that according to the Hindu Marriage Act, since they are newlyweds they must wait for at least one year before they can file for divorce. The couple are forced to live with each other for a year.

Later, Chandrakumar is attacked by his companys labourers because he had earlier suspended their union leader. Divya admits him to a nearby hospital, where he survives after getting proper treatment. After his discharge from the hospital, Divya takes care of him and realises that she has fallen in love with him and tries to show it by wearing the anklets he gave her just after their marriage. Irritated by Divyas immature acts, Chandrakumar asks her to return to her parents home and books tickets for her travel. At the railway station, Chandrakumar gives Divya the divorce papers that she had earlier requested as a wedding gift. Divya breaks down and admits to Chandrakumar that she loves him. Chandrakumar comes to terms with his repressed feelings and finally unites with Divya.

== Cast ==
  Mohan as Chandrakumar
* Revathi as Divya Chandramouli Karthik as Manohar
* V. K. Ramasamy (actor)|V. K. Ramasamy as Chandrakumars boss
* Ra. Sankaran as Chandramouli, Divyas father
* Bhaskar as Divyas brother Kanchana as the lawyer
* Kamala Kamesh as Chandrakumars mother
* Vani as Divyas Mother
* Kalaiselvi as Divyas sister
* Baby Sonia as Divyas sister

== Production ==

=== Development === first night" between the female lead and her husband. He did not originally plan to film it, but after writing the story he realised he could base a film on it. Ratnam took a months break between the schedules of Pallavi Anu Pallavi and wrote the script for Divya.  Ratnam said Mouna Ragams script took five years to write. 

The inspiration for the title Mouna Ragam came from the song "Naan Paadum Mouna Ragam" from Ratnams film Idaya Kovil (1985).  Although Ratnam began working on the script while making Pallavi Anu Pallavi, Mouna Ragam did not enter production until after Idaya Kovils release.  Ratnam stated that due to budgetary constraints, it would have been easier for him to send the female lead to Bangalore instead of Delhi, but he wanted to ensure that she could not easily return to her parents because of her inability to adjust to her husband. 

Mouna Ragam was Ratnams first film with P. C. Sreeram as cinematographer.     It was produced by G. Venkateswaran under his banner Sujatha Films, and was jointly edited by B. Lenin and V. T. Vijayan.  The films art director was  Thota Tharani. 

=== Casting ===
  the heroines behaviour, and Karthik was the  . If Id done the film years later, I would have left out the Karthik character. But at that point of time, I did something that was entertaining and would reach a wider audience."|source=—Mani Ratnam on the inclusion of Karthik in a conversation with film critic Baradwaj Rangan. }}
 Kanchana as the lawyer. Vani plays Divyas mother and Bhaskar plays Divyas brother while actresses Kalaiselvi and "Baby" Sonia play Divyas sisters. 

According to Ratnam, the only difference between Divya and Mouna Ragam was the inclusion of the portion featuring Karthik (actor)|Karthik, which was not present in the earlier screenplay. Divya was the story about a young woman settling into an arranged marriage and did not mention her life before her marriage.  Ratnam realised the story needed to satisfy a wider audience, and decided to give them something that would make them accept the character as a plot point, hence preventing the audience from questioning the character. The story became a film dealing with an arranged marriage—two strangers suddenly thrown together—and how they adjust. At first, Ratnam resisted this point, but he reconsidered because it gave an easier reason for the heroines resistance to the arranged marriage.  Karthik said he was "a last-minute addition" to the cast, and shot his scenes in a week.  He also said his role was "but a cameo". 

=== Filming === backlighting inside a house was used in the film. For this, Thotta Tharani found a house in Chennai that admitted a lot of sunlight, making it different and convincing enough to have a setting similar to the houses in Delhi. Mouna Ragam was Ratnams first film to make excessive use of staccato dialogues, which became his trademark style in his later films.  In a bid to reduce the production cost, food for the films production unit was cooked at the producers home. 

While filming the introduction scene of Karthiks character Manohar, Sreeram had to lie on a bedsheet to film the sequence. The rest of the crew pulled the sheet along with Sreeram and the camera.  Sreeram widely used frontal and profile close-ups set against long shots with out-of-focus foregrounds.  The scene in which Manohar plays a prank on Divyas father Chandramouli at a coffee shop, which later became popularly known as the "Mr. Chandramouli" scene,  was shot at Nungambakkam.   

The crew filmed in Delhi for two days; the portions set in Agra were shot in one day.  The song "Panivizhum Iravu" was filmed at the Taj Mahal,  while scenes from "Mandram Vandha" were shot at the India Gate and in Sikandra.   The scenes involving Karthik were the last portions of the principal photography.  In the post-production phase, Mohans voice was dubbed by S. N. Surendar.  The films final cut was   long. 

== Themes and influences == Nenjathai Killathe (1980), which was also based on a woman torn between the man she loved and the man she married. 
 Italian film Malayalam film Sardar to Tamil phrases, which he uses on V. K. Ramasamys character. 
 Broadway and MTV-style of singing and dancing, while the main songs combine American music with Indian vocals provided by S. Janaki. 
 Spanish music is used for the fight sequence in which Manohar attacks the MPs son. 

The concept of the heroine moving to a strange place where she does not know the local language is a theme replicated in Mani Ratnams later films Roja (1992) and Bombay (film)|Bombay (1995). The theme shows the heroine in a dilemma; because she does not know the language in Delhi, she can barely socialise with local people and she is in conflict with the only person with whom she can socialise.   According to Rangan, the restaurant scene between Manohar and Divya is notable for being the first time in Tamil cinema that a man invited a woman for a cup of coffee. He compared it to Oru Thalai Ragam (1980), in which the protagonists hardly spoke.  Ratnam said he was inspired by the bands The Doors and The Beatles, and that it was not uncommon to invite a woman for a cup of coffee, just it was not reflected in Tamil cinema at that time. 

== Music ==
{{Infobox album
| Name =  Mouna Ragam
| Type = Soundtrack
| Artist = Ilaiyaraaja
| Cover = Mouna Ragam front cover.jpg
| Caption = 
| Released =  1986
| Recorded =
| Genre = 
| Length = 22:43
| Label = Echo Audio Company
| Producer = Ilaiyaraaja
}}
 Hindi film Cheeni Kum—as the films title track and its melancholic version "Sooni Sooni".   The album cover depicts Revathi in her costume from the song "Chinna Chinna Vanna Kuyil".

The soundtrack received positive critical reception. G. Dhananjayan said in his book The Best of Tamil Cinema, "Ilaiyarajas sensitive and wonderful music showcased his urban side once again in this film".  Filmmaker Poongkothai Chandrahasan praised the cinematography of "Nilaave Vaa" and said, "Ilaiyarajas music is such that even when you listen to it ten years later, you still remember the song."    Writing for Mint (newspaper)|Mint, Nandini Ramnath said, "Bombay’s director, Mani Ratnam, can’t make a movie without including a shower from the heavens or a splash in some kind of water body. His best rain song is Oho Megham Vanthatho". 

S. Saraswathi of Rediff.com described "Nilaave Vaa" as a "timeless classic that you never tire of".  Behindwoods singled out "Nilaave Vaa", "Panivizhum Iravu" and "Mandram Vandha" for their "rendering as well as their composition".  In its review of "Nilaave Vaa", Behindwoods says the song "lays out the feelings of a lovelorn heart in a better way." 

=== Tracklisting ===
{{Track listing
| headline        = Side A
| extra_column    = Singer(s)
| all_lyrics      =

| title1          = Oho Megam Vandhadho
| extra1          = S. Janaki
| length1         = 4:25

| title2          = Nilaave Vaa
| extra2          = S. P. Balasubrahmanyam
| length2         = 4:36

| title3          = Chinna Chinna Vanna Kuyil
| extra3          = S. Janaki
| length3         = 4:24
}}

{{Track listing
| headline        = Side B
| extra_column    = Singer(s)
| all lyrics      = Vaali
| title1          = Panivizhum Iravu
| extra1          = S. Janaki, S. P. Balasubrahmanyam
| length1         = 4:32

| title2          = Nilaave Vaa
| extra2          = S. P. Balasubrahmanyam
| length2         = 4:36

| title3          = Mandram Vandha
| extra3          = S. P. Balasubrahmanyam
| length3         = 4:46
}}

== Release and reception == Telugu was released on 14 February 1987 with the same title; this was also a commercial success.  
 National Film Calcutta Film Festival,  and the 2002 Locarno Film Festival. 

=== Critical response ===
On 31 August 1986, the Tamil magazine Ananda Vikatan said, "  has done an outstanding job in the film&nbsp;...&nbsp;Mohan played the role with maturity;...&nbsp;The film does not have any commercial masala film|masalas, yet it creates an impact due to the camera work and music". The magazine gave the film 43 marks out of 100.  Karan Bali of Upperstall.com said, "Mouna Ragam is looked at as Mani Ratnams breakthrough film and though somewhat dated in places in terms of both content and style, the film has some of his finest moments". He criticised the "smaller comedy tracks" in the film by saying that "dont really add anything to the film" and also called the film "too simplistic", but concluded that the film, "is well, well worth a watch even today". 

G. Dhananjayan, in his book The Best of Tamil Cinema, said that though the film was based on a theme that was already shown in Tamil cinema on the institutions of marriage, nothing about it was clichéd. He praised P. C. Sreerams cinematography and Ilaiyaraajas music.   Gautaman Bhaskaran, writing for The Hindu, said the film was, "Simple and shorn of pretensions".  Pavithra Srinivasan of Rediff said, "It took a Mani Ratnam to move away from cliched romantic dialogues and capture subtle nuances that add so much richness to the story, introduce proper, three dimensional characters that lived breathed and sorrowed like everyone else".  Behindwoods described Mouna Ragam as a "film to be watched to understand the nuances of a good and lasting marriage." 

=== Accolades === National Film Best Feature 36th Filmfare Award for Best Tamil Director.    

== Legacy ==
Mouna Ragam became an important milestone in Tamil cinema and was a breakthrough film for Mani Ratnam.  It inaugurated the love story genre set outside the state of Tamil Nadu.  As part of its legacy, the film has been acclaimed for being a box-office success while containing the elements of an arthouse film. 

The use of filming techniques such as soft-focus shots, flare filters and backlit sequences became popular after their introduction to Tamil cinema through this film. Ratnam continued using these techniques in his later films, notably Nayakan (1987 film)|Nayakan (1987) and Agni Natchathiram (1988).  Mouna Ragam catapulted Karthik to stardom despite his role being a cameo,   and his "Mr. Chandramouli" dialogue became popular.  Dhananjayan compared Mouna Ragam to the Hindi film Hum Dil De Chuke Sanam (1999), which had a similar theme, with the exception  that the female leads past lover, played by Salman Khan, is alive.  Mouna Ragam became a trendsetter and inspired several later films with similar themes of romance and drama—including Alaipayuthey (2000), which was also directed by Ratnam, and Priyasakhi (2005).  Mouna Ragam was remade in Hindi as Kasak (1992 film)|Kasak in 1992, and starred Rishi Kapoor, Neelam Kothari and Chunkey Pandey. The remake was less successful than the Tamil and Telugu versions. 

Rediff, in its article, "The most memorable Mani movies", said, "This was arguably the film that announced Mani Ratnam to the Tamil film industry as a talent to watch out for. An excellent script by Ratnam himself,   by PC Sriram and a lilting score by Illayaraja made it a hit with both critics and moviegoers."  Deccan Chronicle listed Karthik and Revathi on its "Top 10 Jodis" of Tamil cinema, and wrote that they "made a fresh pair and were adored by the youth, especially the college students. Their awesome on-screen chemistry in Mouna Ragam was a talking point back then."  On the centenary of Indian cinema in April 2013, Forbes (India)|Forbes included Revathis performance in the film on its list, "25 Greatest Acting Performances of Indian Cinema".  The film was included in a poll conducted by CNN-IBN to determine "the most popular romantic film of all time". 

Art director Sabu Cyril ranked Mouna Ragam fourth in his list of "Indias best films", praising its story, narration and screenplay.  Rapper Blaaze called Mouna Raagam "brilliant" and praised the cameo appearance by Karthik.  Poongkothai Chandrahasan told The Hindu, "Mani Ratnam had the guts to make a film that interesting with a different storyline. Also the way P. C. Sriram shot the film was so beautiful." 

== In popular culture == Shiva and Disha Pandey imitating Manohar and Divyas mannerisms.   A Telugu film, also titled Mouna Ragam, was released in 2010; apart from sharing a name, this had no connection with Ratnams film. 
 Raja Rani Prasanna and Lekha Washington in Kalyana Samayal Saadham (2013).  Vaibhav Reddy compared the character he plays in Kappal (2014) to Manohar in Mouna Ragam, because of the formers similarly effervescent nature. In one scene, which Reddy compared to the "Mr. Chandramouli" scene, Reddys character pesters the heroine to fall in love with him.  Mouna Ragam is also the name of a Tamil orchestra, alternatively known as "Muralis Mouna Ragam". 

== Explanatory notes ==
 

== References ==
 

== Bibliography ==
 
*  
*  
*  
*  
*  
*  
*  
*  
 

== External links ==
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 