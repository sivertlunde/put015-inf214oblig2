Danger – Love at Work
{{Infobox film
| name           = Danger – Love at Work
| image          = Danger – Love at Work 1397 poster.jpg
| image_size     = 
| caption        = 1937 US Theatrical Poster
| director       = Otto Preminger
| producer       = Harold Wilson
| writer         = James Edward Grant Ben Markson
| narrator       = 
| starring       = Ann Sothern Jack Haley Edward Everett Horton
| music          = Cyril J. Mockridge
| cinematography = Virgil Miller Jack Murray
| studio         = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Danger – Love at Work is a 1937 American screwball comedy film directed by Otto Preminger. The screenplay by James Edward Grant and Ben Markson focuses on an attorneys frustrating efforts to deal with a wildly eccentric family.

==Plot==
Henry MacMorrow, a junior partner in the law firm of Parsons, Hilton, Trent and MacMorrow, is assigned the task of obtaining the signatures of various members of the Pemberton family so that a piece of property they own can be sold. While en route by train to the Pemberton home in Aiken, South Carolina, he meets Junior Pemberton, an obnoxious ten-year-old prodigy whose behavior prompts Henry to kick him in the pants when they arrive at the station, much to the dismay of the boys sister Toni.

Henry arrives at the Permberton home before Toni and Junior, and the rest of the family mistakes him for her fiancé Howard Rogers. She quickly corrects the misunderstanding and soon finds herself liking the amiable lawyer, despite their unpleasant first meeting. Mistakenly believing the millionaire Henry is impoverished and the sole support of his widowed mother, Toni promises to help him financially, but Howard convinces the family Henry is a fraud. The attorney returns to New York City, where he promptly is fired.

Anxious to find Henry, Toni convinces the Pembertons their town is being quarantined, and the entire family travels to New York. When Toni learns Henry has lost his job, she vows to help him get it back. She urges her family to sign the documents allowing their land to be sold, and then she and Henry go to the country to obtain the signatures of her Aunts Pitty and Patty and Uncle Goliath.

Howard, still certain Henry is a con artist, decides to assess the Pembertons property and discovers oil, unaware its leaking from his own car. Believing the land is worth a fortune, he persuades the family to sell it to him for $125,000 and convinces them Henry was trying to scam them. Thinking Henry was deceiving her, Toni ends their relationship.

Howard discovers the oil was from his car and tries to get his money back, only to discover the Pembertons already have spent it. When he decides to sell the land to Howard, Toni tries to warn him, but he refuses to speak to her, until Pitty and Patty reveal Toni and Henry spent the night in their barn, and Toni pretends he took advantage of her. Her family storms Henrys apartment and demands he make an honest woman of her, and he willingly agrees to marry Toni.

==Cast==
*Ann Sothern ..... Toni Pemberton
*Jack Haley ..... Henry MacMorrow
*Edward Everett Horton ..... Howard Rogers
*Mary Boland ..... Alice Pemberton
*John Carradine ..... Herbert Pemberton
*Walter Catlett ..... Uncle Alan
*Benny Bartlett ..... Junior Pemberton
*Maurice Cass ..... Uncle Goliath Charles Coleman ..... Henrys butler
*Margaret Seddon ..... Aunt Pitty
*Margaret McWade ..... Aunt Patty

==Production== Pulitzer Prize-winning 1936 play You Cant Take It with You by George S. Kaufman and Moss Hart, as well as Morrie Ryskind and Gregory LaCavas script for My Man Godfrey. 20th Century Fox executive Darryl F. Zanuck originally cast Simone Simon as Southern belle Toni Pemberton, but her heavy French accent proved to be too difficult to understand, and after a few days of filming she was replaced by Ann Sothern. 

The films title song was written by Harry Revel and Mack Gordon.

==Critical reception==
Ceri Thomas of Channel 4 called the film "a C-grade screwball comedy thats quaintly competent but lacks energy" and added, "Preminger does a workmanlike job on this . . . but doesnt lift the material above the ordinary. He seems happy just to parade this collection of oddball caricatures in front of the lens, content that their cheery wackiness is enough to power the story. Sadly it isnt. There is no urgency or drive to the action to match the snappiness of the dialogue. Even though it runs at less than 80 minutes, the film seems to dawdle." 

Time Out London said Preminger "directs efficiently, but with little feeling for screwball form; with hindsight it was hardly his sort of thing. Amiable enough, but, as ever, a little loveable eccentricity goes a very long way." 

In reviewing the film for DVD Times, Anthony Nield observed although Preminger "may not have the sense of timing which distinguished the great screwball directors, from Howard Hawks to Preston Sturges, he nonetheless manages a slick, professional job . . .  here is something pleasing to come from the knowledge that the director of such twisted efforts as Where the Sidewalk Ends, Bunny Lake is Missing and, indeed, Laura (1944 film)|Laura could start out in Hollywood on something so relatively straightforward." 

==DVD release== fullscreen format, with an audio track and subtitles in English. There are no bonus features.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 