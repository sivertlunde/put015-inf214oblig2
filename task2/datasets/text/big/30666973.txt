Power Kids
{{Infobox film
| name           = Power Kids
| image          = Power-Kids-poster.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Krissanapong Rachata
| producer       = Prachya Pinkaew Sukanya Vongsthapat Panna Rittikrai
| writer         = Piyaros Thongdee Napalee Nonont Kontaweesook
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Johnny Nguyen Nanthawut Boonrubsub Sasisa Jindamanee Nawarat Techaratanaprasert Paytaai Wongkamlao     Pimchanok Luevisadpaibul
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Thailand
| language       = Thai
| budget         = 
| gross          = $97,836
| preceded by    = 
| followed by    = 
}}
Power Kids (5 หัวใจฮีโร่) is a 2009 Thai action film directed by Krissanapong Rachata. The film is about a group of children who team up to fight against a group of terrorists who have seized the hospital moments before their friend is about to go in for surgery. The film grossed $97,836 in Thailand and was shown at several film festivals across North America.

==Release==
Power Kids was released in Thailand on March 5, 2009.    On its opening week, Power Kids was the fourth highest grossing film earning $54,715.  The film earned a total of $97,836 on its theatrical run in Thailand and earned $259,677 worldwide. 

Power Kids had its North American debut at the Fantasia Festival on July 12, 2009.  The film had its American premiere at ActionFest in 2010. 
The film was shown at the New York Asian Film Festival On July 3, 2010.   Power Kids was released on DVD and Blu-ray Disc|Blu-ray on June 8, 2010 in North America. Bonus features on the disc include a making of the film short and a making of short. 

==Cast==
*Nantawooti Boonrapsap as Wut
*Sasisa Jindamanee as Kat
*Paytaaai Wongkamlao as Pong
*Nawarat Techarathanaprasert as Jib
*Pimchanok Luevisadpaibul as Buru
*Johnny Nguyen as Terrorist Leader
*Arunya Pawilai as Lek
*Richard William Lord as Drunk Bully
*Conan Stevens as Ambassadors Bodyguard

==Reception==
Twitch Film gave the film a mixed review, referring to it as "a kids film that has no business being seen by children" and "At the same, its filled with so many insane action beats that Id feel remiss in not recommending it."  Kung Fu Cinema criticized the movie for having inappropriate content matter for a childrens movie. 

==Notes==
 

==External links==
*  
*  

 

 
 
 
 