Super Shastri
{{Infobox film
| name           = Super Shastri
| image          =
| caption        =
| director       = Raviraj
| producer       = G. Ramachandran
| writer         = Raviraj
| starring       = Prajwal Devraj Haripriya Deva
| cinematography = Kashi Vishwanath
| editing        =
| studio         = G. R. Gold films
| released       = 9 November 2012
| country        = India
| language       = Kannada
| budget         = 15 Crores
}} Kannada Action Action comedy Deva is the score and soundtrack composer.  The film was declared a Hit at the Box Office.

==Production==
The film made a theatrical release on 9 November 2012.

==Cast==
* Prajwal Devraj as Subramanya Sastry
* Haripriya as Soumya
* Umashree
* Rangayana Raghu
* Bullet Prakash
* Pallakki Radhakrishna as Venkatappa Gowda
* Ambika Soni
* Master Manju
* Master Suresh
* Rekha Kumar
* Kote Prabhakar

==Critical reception==
Super Shastri opened to negative response from critics upon release.
The Times of India which gave the film a negative review with 2.5/5 rating mentioned that "The story could have been a good comical piece had the director taken care of dialogues, narration and the cast, which are in poor form. Prajwal certainly does not fit this uncharacteristic role."  Rediff.com|Rediff gave the film a one star rating stating that the film fails to connect with the Kannada audiences on all counts. 

==Soundtrack== Deva to the lyrics of K. Kalyan. 

{{Track listing
| total_length   =
| lyrics_credits = no
| extra_column  = Singer(s)
| title1 = Ardha KG
| lyrics1  =
| extra1        = Prasanna
| length1       =
| title2        = Manase Baalangochi
| lyrics2  =
| extra2        = Prasanna
| length2       =
| title3        = Onde Ondu
| extra3        = Badri Prasad, Nanditha Gururaj
| lyrics3  =
| length3       =
| title4        = Puneeth Rajkumar
| extra4        = Badri Prasad, Chaitra H. G.
| lyrics4  =
| length4       =
| title5        = Vishwa Sundari
| extra5        = Prasanna
| lyrics5  =
| length5       =
}}

==References==
 

==External links==
*  

 
 
 
 
 
 


 