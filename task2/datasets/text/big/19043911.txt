Besættelse
 
{{Infobox film
| name           = Besættelse
| image          = Besættelse.jpg
| image_size     =
| caption        = Cover of the Danish DVD
| director       = Bodil Ipsen
| producer       =  
| writer         = Fleming Lynge Hans Severensen (novel)
| narrator       = 
| starring       = Berthe Qvistgaard Johannes Meyer Aage Fønss Poul Reichhardt
| music          = Erik Fiehn
| cinematography = Valdemar Christensen
| editing        = Valdemar Christensen
| distributor    = Nordisk Film
| released       = 1944
| runtime        = 83 mins
| country        = Denmark Danish
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Danish film noir directed by Bodil Ipsen and starring Johannes Meyer and Berthe Qvistgaard. Based upon a novel by Hans Severensen, the film is a dark psychological drama about an aging businessman whose erotic obsession with a cynical young woman leads to his eventual downfall. 

==Cast==
{| class="wikitable"
!   Actor !! Role
|- Johannes Meyer ||	Draper Jens Steen
|- Berthe Qvistgaard || Else Petersen
|- Aage Fønss ||	Merchant Thygesen
|- Poul Reichhardt || 	Aage Thygesen
|- Thorkil Lauritzen || 	Landbetjent Hermannsen
|- Aage Redal || parish bailiff
|- Peter Nielsen || 	lawyer
|- Rasmus Christiansen Rasmus Christiansen || 	Røgter
|- Vera Gebuhr ||	Hotel maid
|- Alma Olander Dam Willumsen || 	Mrs. Steen (as Alma Olander Dam)
|- Jesper Gottschalch ||
|- Karl Jørgensen || Traveling salesman
|}

==References==
 

== External links ==
*  at IMDb
*  at Den Danske Film Database (in Danish)
*  at Det Danske Filmistitut (in Danish)
*  

 
 
 
 
 
 
 
 


 
 