Between Floors
 
{{Infobox film
| name           = Between Floors
| image          = 
| alt            =  
| caption        = 
| director       = Jen White
| producer       =  
| writer         = Jen White
| starring       =  
| music          =  
| cinematography = Jen White
| editing        = Jen White
| studio         = UNC Films
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American independent drama film that was produced and directed by Jen White as her directorial debut.    The film was edited from hour-long, structured improvisations, shot in working elevators. Each scene plays out in a single shot, with the only editing being from scene to scene. 

==Plot==
Between Floors focuses on five stuck elevators, each containing people inside them. As time passes, the people in each elevator deal with the isolation and containment in various ways.

==Cast==
 
* Ryan Wickerham as Gary
* Michael D. Conway as Mike
* Anika Kunik as Glen
* Kathryn MaGill as Katy
* Brent Smiga as Andrew
* Jim Walters IV as Jeff
* Scott Bate as Fred
* Amanda Stephen as Meg
* Amanda Brown as Stephanie
* Dustin Doering as Peter
 

==Reception==

 

==Festivals==
The film has screened at numerous film festivals,  including 
: 2009
* August 30	Atlanta Underground Film Festival 
* September 19	Boston Film Festival
*   Strassburg Internatiuonal Film festiva
: 2010
* February 19  Magnolia Film Festival 
* April 11  San Francisco Womens Film Festival
* May 29 ArtsFest Film Festival
* August 7 Hardacre Film Festival 
* August 24 Strasbourg International Film Festival 
* September 7 (Nomadic Tendencies Film Festival
* October 17 Salem International Film Festival 
* November 18 ReelTime and the Radio Room Film Series
: 2011
* March 26  Fort Myers Film Festival
* April 9 Fallbrook International Film Festival
* April 13 Riverside International Film Festival
* April 15 Indie Spirit Festival Film Festival
* June 18 Independent Bronx Film Festival
* July 16, Ventura Film Festival
* August 19, Salt Lake City International Film Festival

===Awards and nominations===
* 2009, won Best Director at Atlanta Underground Film Festival for Jen White   
* 2010, won Best Director at Strasbourg International Film Festival for Jen White   
* 2010, won Best Feature Film at Magnolia Film Festival    
* 2010, won Best Narrative Feature at Hardacre Film Festival   
* 2010, won, Best Narrative Feature at Salem Film Festival   

==References==
 

==External links==
*  
*  

 
 
 
 
 