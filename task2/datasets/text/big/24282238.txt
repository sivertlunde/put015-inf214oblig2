Yamraaj (film)
{{Infobox film
| name           = Yamraaj
| image          = Yamraajfilm.jpg
| director       = Rajiv Babbar
| producer       = Rajiv Babbar
| writer         = Rajiv Babbar Sneha Ashish Vidyarthi Prem Chopra
| music          = Anand Milind
| genre          =  Action
| distributor    = Aabha Films
| released       = July 31, 1998
| runtime        =
| country        = India
| language       = Hindi
| awards         =
| budget         =
}}

Yamraaj is a 1998 released Indian action film starring Mithun Chakraborty and Jackie Shroff as thieves and their conflict between each other on their Goal.

== Synopsis ==
Yamraaj is the story of two thieves Birju (Mithun Chakraborty) and Kishan (Jackie Shroff) who dream to become the biggest dons like their mentor, the citys biggest don Yamraaj (Gulshan Grover). Kishans conscience starts troubling him as he assassinates an honest Police Officer Hamid Khan (Kiran Kumar). He decides start a new life but Birju refuses to change as he is obsessed by his dream. Their difference in opinion leads to conflict between them results in separation. Birju pursues his dream while Kishan tries to repent for his crime. Will Birju change for good or will he become Yamraaj forms the Climax.

== Cast ==

* Mithun Chakraborty ... Birju
* Jackie Shroff ... Kishan Sneha ... Nisha
* Mink Singh ... Kishans girlfriend
* Gulshan Grover ... Yamraaj
* Kiran Kumar ... Officer Hamid Khan
* Ashish Vidyarthi
* Altaf Raja
* Jack Gaud
Kajal

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chabaake Panwa Tune"
| Zahid Naazam, Parveen Saba
|-
| 2
| "Hae Hae Mere Dil Ki"
| Jaspinder Narula, Lakha Singh 
|-
| 3
| "Hum Hain Aise Chore"
| Altaf Raja
|-
| 4
| "Hum Karenge Aish"
| Udit Narayan, Vinod Rathod, Bela Sulakhe 
|-
| 5
| "Sun Mere Beta"
| Habib Sabri 
|-
| 6
| "Ude Ude Hain"
| Sudesh Bhosle, Shweta Shetty
|}

== External links ==
*  

 
 
 
 
 
 


 