Wishing Stairs
{{Infobox film
| name = Wishing Stairs
| image    = Wishing Stairs movie poster.jpg
| caption  = Theatrical release poster
| film name      = {{Film name
 | hangul   = 여고괴담 3: 여우계단
 | rr       = Yeogogoedam 3: Yeowoogyedan
 | mr       = Yŏgogoedam 3: Yŏukyedan}}
| director = Yun Jae-yeon
| producer = Lee Chun-yeon
| writer   = Kim Su-ah Lee Yong-yeon Eun Si-yeon Lee Soyoung
| starring = Song Ji-hyo Park Han-byul Jo An
| music    = Gong Myeong-ah
| cinematography = Seo Jeong-min
| editing  =
| distributor = Cinema Service
| released =  
| runtime  = 97 minutes
| country  = South Korea
| language = Korean
| budget   = 
}} horror film. It is the third installment of the Whispering Corridors (film series)|Whispering Corridors film series set in girls high schools, but, as with all films in the series, is unrelated to the others; apart from a song being sung in one scene that is a pivotal plot in Voice (film)|Voice. 

==Plot==
Yun Jin-sung (Song Ji-hyo) and Kim So-hees life (Park Han-byul) are best friends studying ballet at an all-girls art school, the latter usually showing a lot of affection towards the former. However, their friendship soon turns sour when they find themselves competing for a single spot in a Russian ballet school. At an art exhibit within the school, Jin-sung learns from an odd, near obese student named Eom Hye-ju (Jo An), of an old legend that if a person climbs the twenty eight steps leading up to the schools dormitory, counting each step aloud, and finds a twenty ninth, then a fox spirit will appear, and grant that persons wish. Curious, Jin-sung does climb the stairs and comes across the twenty ninth, happily wishing to gain the spot. To her surprise and anger later on, So-hee is selected instead. When So-hee confronts her on this, Jin-sung declares her hatred towards her best friend and accidentally sends the former down a flight of stairs during a scuffle. So-hee is left unconsciousness and hospitalized.

Jin-sung visits So-hee in the hospital and learns indirectly that she is no longer able to study ballet during an argument with her mother due to her injuries from the fall. Jin-sung tries to apologize towards her friend for what happened, but receives no reply and leaves feeling guilty. During the next day, she learns from another student in class that So-hee had committed suicide by jumping out of her hospital room to her death, causing her to faint as a result. As the fight between the two was witnessed by several other girls, Jin-sung is now seen down upon by many of the student body, who believe that she intentionally pushed So-hee down the flight of stairs out of jealously. As Jin-sungs wish comes true in which she gets the spot for the ballet school, her fellow students throw her a surprise party for it, but still treat her coldly by throwing a cake for her into her face.

Affected greatly by So-hees death, as she was the only girl to treat her with kindness, Hye-ju attempts to keep So-hees belongings for herself, but is ridiculed by other students for it, mainly Han Yun-ji (Park Ji-yeon), a girl who enjoys bullying Hye-ju over her weight. Later on, she climbs the steps outside the dormitory too, and is able to wish for So-hee to come back. While So-hee does indeed return, she returns as a twisted spirit who initially frightens Hye-ju, but soon gains her trust by telling her she had liked her a lot and convinces her to dye her hair black. However, So-hee soon possesses her. 

As Yun-ji has finished up a sculpting project in a storage room within the schools basement, she is soon confronted by the possessed Hye-ju and is made fun of by smelling weird. The possessed Hye-ju questions as to why she must be always be bullied by her, then proceeds to stab Yun-ji to death with a pair of scissors. As Jin-Sung enters the room to gather supplies, she is encountered by Hye-ju as well, who tries to convince her that she is So-hee. After coming across Yun-jis corpse, Jin-sung is convinced that Hye-ju is insane and tries to get away from her, knocking over a can of oil. After hearing So-hees voice in Hye-ju stating that their friendship will end, Jin-sung is able to leave. The spirit of So-hee then apparently makes Hye-ju light a match and toss it over the spilled oil, leaving the troubled girl to perish within the growing flames.

While Jin-sung is preparing to leave for the ballet school, she is again haunted by So-hee within her dorm room and throughout the school several times. Unable to endure the spirits presence any further, she tries to climb the stairs again in order to wish her away. Before she could reach the top, So-hee calls out to her, then holds her arms around her stomach as Jin-sung confesses that she didnt hate her and simply wanted to be happy. Believing that Jin-sung does not love her as much as the other way around, So-hee states that she want to be with her always and proceeds to crush Jin-sungs stomach inwards with her arms, killing her. After letting her friends now-lifeless body fall to the bottom of the stairs, the spirit vanishes.

Some time later, a new girl (whose face is not seen) moves into the same dorm room that Jin-sung once occupied. As she moves her stuff around, a picture with Jin-sung and So-hee on it is seen on the floor of the room. Within it, So-hees irises on her eyes disappear, implying that she still remains.

==Cast==
*Song Ji-hyo - Yun Jin-sung
*Park Han-byul - Kim So-hee
*Jo An - Eom Hye-ju
*Park Ji-yeon - Han Yun-ji
*Kong Sang-ah - Kyeong-jin
*Lee Se-yeon - Young-seon
*Hong Soo-ah - sculpture club member
*Lee Min-jung - dance double for Yun Jin-sung
*Kwak Ji-min - dance class junior
*Moon Jung-hee - dance teacher

== Notes==

The film itself seems to mirror the ballet Giselle, which girls in the film are studying, as well as drawing upon the classic short story The Monkeys Paw, with So-hee as Giselle, and Jin-sung as Albrecht. Unhappy with always having to play the "prince" to So-hees princess, Jin-sung betrays So-hee, which in turn leads to So-hee being crippled and commits suicide after her friend Jin-sung confesses she has hated her all along. When So-hees spirit is wished back, Jin-sung is haunted by So-hees ghost, the love she once felt for her friend warped by Jin-sungs hurtful actions.

As in the two previous movies, this film has strong themes of friendship, betrayal, and the taboo of lesbian affairs in an all-girls school.


== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 
 