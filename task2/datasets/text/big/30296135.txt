If Not Us, Who?
 
{{Infobox film
| name           = If Not Us, Who?
| image          = If Not Us, Who.jpg
| caption        = Film poster
| director       = Andres Veiel
| producer       = 
| writer         = Andres Veiel
| starring       = August Diehl
| music          = 
| cinematography = Judith Kaufmann
| editing        = Hansjörg Weißbrich
| distributor    = 
| released       =  
| runtime        = 
| country        = Germany
| language       = German
| budget         = 
}}
If Not Us, Who? ( ) is a 2011 German drama film directed by Andres Veiel and starring August Diehl.  The film is set in the late 1940s, the early 1960s, and at the beginning of the Protests of 1968. 
 Berlin and Beyond film festival in San Francisco on 26 October 2011.

== Cast ==
* August Diehl as Bernward Vesper
* Lena Lauzemis as Gudrun Ensslin
* Alexander Fehling as Andreas Baader
* Alexander Khuon as Rudi Dutschke
* Rainer Bock as Verteidiger
* Sebastian Blomberg as Roehler
* Maria-Victoria Dragus as Ruth Ensslin
* Thomas Thieme as Vater Vesper
* Joachim Paul Assböck as Journalist
* Hanno Koffler as Uli Ensslin
* Heike Hanold-Lynch as Mentorin v. G. Ensslin
* Hark Bohm as Kritiker
* Imogen Kogge as Mutter Vesper
* Henriette Nagel
* Susanne Lothar as Mutter Ensslin
* Michael Wittenborn as Vater Ensslin

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 

 
 