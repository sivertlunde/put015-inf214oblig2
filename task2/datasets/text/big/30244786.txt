Mirapakay
{{Infobox film
| name        = Mirapakay
| image       = Mirapakay Poster.jpg
| writer      = Harish Shankar
| starring    =  
| director    = Harish Shankar
| producer    = Ramesh Puppala
| based on       =  
| distributor = 
| cinematography = C. Ram Prasad
| released    =  
| runtime     = 
| editing     = Goutham Raju
| language    = Telugu
| music       = S. Thaman
| country     = India budget       =   gross        =  (100 days Gross)
}}
Mirapakay ( ) is a blockbuster 2011 Telugu-language film, directed by Harish Shankar. The film stars Ravi Teja, Richa Gangopadhyay, and Deeksha Seth in the lead.  The film, which has music scored by S. Thaman, was released on 13 January 2011. The film has been dubbed in Hindi as Khallas and into Tamil as Murattu Singam.

==Plot==
Rishi (Ravi Teja) is an inspector in the Intelligence Bureau. His colleagues fondly call him Mirapakaay. The chief of the IB, Narayana Murthy (Nagababu) gets information that Kittu Bhai (Prakash Raj), a mafia don, is trying to spread his tentacles in India and is targeting Delhi first. During the course of the investigation, ACP (Sanjay Swaroop) is killed by Shankar (Kota Srinivasa Rao), a local goon with the help of his son (Supreet).

Murthy sends Rishi to Hyderabad and gets him admitted to a college as a Hindi lecturer as part of an undercover operation. Rishi meets Vinamra (Richa Gangopadhyay) at a temple & falls in love at first sight. Incidentally, she studies in the same college and in the same class to which Rishi teaches Hindi. Their love blossoms. At this juncture, Vaishali (Deeksha Seth), daughter of Kittu Bhai, joins the same college. Grabbing the opportunity, the IB chief asks Rishi to extract information about Kittu from Vaishali. Rishi manages to make Vaishali fall for him and using her, He reaches Kittu after killing Shankar and his son. He finally arrests Kittu and his Gang. The film ends with Rishi convincing an angry Vinamra to marry him.

==Cast==
  was selected as  one of the lead heroines, marking her first collaboration with Ravi Teja.]]
* Ravi Teja as Rishi/Rishikesha/Mirapakay
* Richa Gangopadhyay as Vinamra
* Deeksha Seth as Vaishali
* Prakash Raj as Kittu
* Kota Srinivasa Rao as Shankar Anna Sunil as Charukesha
* Nagababu as Narayana Murthy
* Swati Reddy in a cameo
* Sanjay Swaroop as ACP srinivas. Ajay as Kittus Son Chandramohan as Vinamras Father
* Sudha as Vinamras Mother
* Brahmaji as Gupta Ali as Al Pacino
* Dharmavarapu Subramanyam

==Production==
After the failure of Shock (2006 film)|Shock, Harish Shankar faced struggles in his career. He narrated the script of Mirapakaay in the tile Romantic Rishi first to Pawan Kalyan, After listening to interval episode, Pawan Kalyan gave him a go-ahead. But that project didn’t materialise due to certain reasons. It was Ravi Teja who gave Harishs birth and rebirth as a director. During the shooting time of Shock he said that he would do another film with him irrespective of the commercial outcome of Shock. Harish was feeling guilty all this while and didn’t approach him. He called him and asked for a story. Then he narrated the script of Mirapakai.  She was signed after the director of the film, Harish Shankar, had noticed her at the success party of her previous film Leader. He initially offered her the role played by Deeksha as she was an NRI, but when they met to review the script, he was convinced that Richa would do justice to the character of Vinamra, a traditional Brahmin girl. 

==Reception==
Oneindia Entertainment gave a review stating "Mass Raja Ravi Teja has got an unofficial promotion as Mass Maharaja as per the titles of this film. The film appears to be on the lines of Ravi Teja"s earlier hits like Anjaneyulu, Vikramarkudu and Don Seenu. The audiences could enjoy the film in a festival mood and relax without cursing the hero or the director."  Sify.com gave a review stating "Despite a banal storyline, the film has entertainment value, punch and a decent presentation. The director purely depends on the energy levels of Ravi Teja and the comedy angle. Ravi Teja proves that he can hold the audience purely with his high-energy levels in action, comedy and romance scenes. He is the life and blood of the movie. You cant go wrong with a title as racy as Mirapakay. And fortunately, this Ravi Teja starrer comes as a pucca mass entertainer for Sankranthi."  Supergoodmovies.com gave a review stating "If you have no complaints to watch an entertaining film even though it is a routine fare, then you can happily go for Mirapakay. On content wise this Mirapakaya isn’t spicy."  telugucinema.com gave a review stating "If you are not tired of seeing Ravi Teja doing same role again and again, showing off same histrionics without much variation, then Mirapakay may seem time-pass one. For others, it looks tepid and boring."  chitramala.in gave a review stating "A middle aged married man was once talking to me about ‘Anjaneyulu’ and I tried my best to keep him away from the film and then he says “Ravi Teja’s film has a minimum guarantee, there’ll be jokes, there are fun songs and there’s Ravi Teja. So it’s a safe bet for a movie outing with the wife”. A minimum guarantee."  123telugu.com gave a review stating "Mirapakaya, at the end of the day is pure Tollywood tamasha. Its a Ravi Teja movie and he never disappoints a fan who is in search of some good old fun. And whats more it is festival time and a few laughs will definitely do you a world of good."  telugu.way2movies.com gave a review stating "Ravi Teja is the only aspect to watch Mirapakaay, It doesn’t offer anything new, a simple Police catching thief, How? Is the story. Mirapakaay have lots of action mixed comedy as we find in all recent movies of Ravi Teja. Watch it for Ravi Teja."  idlebrain.com gave a review stating "It is a proven fact that Ravi Teja’s movies have scant respect for story and screenplay and majorly depend on the entertainment aspect. The entertainment is interesting in parts. We have to wait and see how Mirapakai is lapped up by the crowds." 

==Soundtrack==
{{Infobox album
| Name = Mirapakay
| Longtype = to Mirapakay
| Type = Soundtrack
| Artist = S. Thaman
| Cover =
| Border = yes
| Alt =
| Caption =
| Released = 2011
| Recorded = 2010-2011 Feature film soundtrack
| Length = 26:28 Telugu
| Label = Aditya Music
| Producer = S. Thaman
| Last album = Ragada (2010) 
| This album = Mirapakay (2011) 
| Next album = Veera (2011 film)|Veera (2011) 
}}
The audio of the film was launched on December 5, 2010 at Taj Deccan Hotel, Hyderabad. T.G. Venkatesh, Ram Gopal Varma and Atchi Reddy launched audio cassettes, CDs and trailers respectively. D Rama Naidu, SS Rajamouli, Ravi Teja, Harish Shankar, VV Vinayak, Gopichand Malineni, Vamsi Paidipalli, Chandra Bose, Nikhil, S. Thaman, Sunil and others attended the function.  The songs "Vaishali", "Silakaa" and "Dhinaku Dhin" were reused by Thaman in Tamil films Osthe and Ishtam (2012 film)|Ishtam respectively.
{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 26:28
| lyrics_credits = yes
| title1 = Adigora Choodu
| lyrics1 = Anantha Sreeram Ranjith
| length1 = 4:12
| title2 = Vaishali Vaishali
| lyrics2 = Bhaskarabhatla
| extra2 = S. Thaman
| length2 = 4:22
| title3 = Gadi Thalupula
| lyrics3 = Sirivennela Sitaramasastri
| extra3 = Karthik & Geetha Madhuri
| length3 = 4:48
| title4 = Silakaa
| lyrics4 = Bhaskarabhatla
| extra4 = Rahul Nambiar & K. S. Chitra
| length4 = 4:39
| title5 = Dhinaku Dhin Chandrabose
| extra5 = Shankar Mahadevan & Shreya Ghoshal
| length5 = 4:46
| title6 = Chirugaley
| lyrics6 = Sahithi
| extra6 = Rita, Megha (singer)|Megha, Janani, Vardhini, Ranjith, Naveen Madhav
| length6 = 2:22
| title7 = Mirapakaay
| lyrics7 = Harish Shankar
| extra7 = Ranjith & Rita
| length7 = 1:16
}}

==Box office== Blockbuster at the Box office.

==References==
 

==External links==
* 
 

 
 
 
 