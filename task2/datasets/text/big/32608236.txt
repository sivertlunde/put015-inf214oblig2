Pleasant Goat and Big Big Wolf: The Tiger Prowess
 
{{Infobox film
| name           = Pleasant Goat and Big Big Wolf: The Tiger Prowess
| image          = 
| caption        = 
| director       = 
| producer       = 
| writer         = 
| cinematography = distributor     = 
| released       =   
| runtime        = 84 Minutes 
| country        = China
| language       = Mandarin
}}

Pleasant Goat and Big Big Wolf: The Tiger Prowess ( ), is a 2010 Chinese animated film based on the popular Pleasant Goat and Big Big Wolf TV series.

The film had revenues of 128 million yuan. Li, Wenfang. " ." ( ,  ) China Daily. January 24, 2011. Retrieved on August 12, 2012. 

==Synopsis==
Crafty Gecko and General Tiger attack the Goat Village with plans to build an amusement park. Even worse, they intend to turn the castle of Wolfie (Grey Wolf) and his wife, Wolnie (Red Wolf) into a public toilet! To save their captured families from the fierce animal gang and robot soldiers, Weslie (Pleasant Goat) and Wolfie must work together to find the legendary statues that protected their ancestors. Can they beat the villains and become friends?

==Cast==
*Zu Liqing - Pleasant Goat
*Deng Yuting - Pretty Goat / Warm Goat
*Liang Ying - Lazy Goat / Little Grey
*Liu Hongyun - Fit Goat
*Gao Quansheng - Slow Goat Zhang Lin - Grey Wolf
*Fan Bingbing - Red Wolf
*Li Tuan - Soft Goat

==References==
 

==External links==
*   
*   
 

 
 
 
 
 
 


 
 