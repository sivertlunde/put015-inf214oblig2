Delhi Safari
 
{{Infobox film
| name           = Delhi Safari
| image          = Delhi_Safari_Poster.jpg
| caption        = Indian theatrical release poster
| director       = Nikhil Advani Kishor Patil
| writer         = Nikhil Advani Girish Dhamija Suresh Nair
| story          = Nikhil Advani
| based on       =   Govinda Sunil Shetty  Boman Irani Urmila Matondkar
| music          = Shankar-Ehsaan-Loy Aarif Sheikh 
| studio         = Krayon Pictures
| distributor    = Applied Art Productions Shemaroo Entertainment (DVD)
| runtime        = 92 minutes
| country        = India Marathi  Hindi English English
| released       =  
| budget         =INR 15,000,000 (estimated)
| gross          =   (domestic nett gross)
}} 2012 Indian comedy feature Vanessa Williams and Brad Garrett. The international sales of the film is being handled by Fantastic Films International. The film was released in the United States on 7 December 2012.    Delhi Safari was received a 17% score on review aggregator Rotten Tomatoes and a 37/100 in Metacritic.

==Plot==
Delhi Safari  is the story of a journey undertaken by a leopard cub, his mother, a monkey, a bear and a parrot when the forest they live in is on the verge of destruction. Builders have encroached upon their forest and the animals decide to go to Delhi and protest in front of the parliament and ask the parliament some very simple yet pertinent questions they were – why has man become the most dangerous animal? Doesnt man understand that if the forests and the animals dont co-exist with humans, the balance of the ecosystem will be endangered? The film starts with Yuvraj Yuvi saying that he lost his father Sultan (Sunil Shetty) in the morning,but doesnt want to lose his home in any cost. The film carries a flashback of the morning with Yuvi and Sultan playing in the forest while his mother, Begum (Urmila Matondkar) enters. The two say something that Begum gets angry and goes. Soon, Begum forgives Sultan and Yuvraj. However, while coming back, a whole pack of bulldozers come. Begum manages to escape, but Yuvi and Sultan are trapped. In a bid to save Yuvi, Sultan catches Yuvi in his mouth and tosses him to Begum. However, he himself is killed by a human wielding a shotgun, making the whole forest a large enemy of the humans. A talk happens of Bajrangi, a monkey (Govinda), saying that he would beat out the wits of those men, and asks whether anyone knows anyone who knows language of humans. A white bird pipes up, saying he knows someone. Yuvi meets the white bird the next day, and asks who is the one he said about. The white bird says he is Alex (Akshaye Khanna), who lives with a director Vikram. Bajrangi, Bagga the bear (Boman Irani), Yuvi go and kidnap the parrot and convince him to go to Delhi with them to talk to the parliament. After a few good and bad experiences Begum tells that no one is going to Delhi after listening to a tigers story as soon after she sees Sultan and proceeds to Delhi. They finally reach Delhi and tell their message. In the last scene it is shown the jungle is saved and all are happy.

==Characters==

===The Leopards===
* Sultan - a brave member of the pack who sacrifices himself trying to stop deforestation.
* Begum - Sultans wife who is a little overprotective of her cub but a little doubtful.
* Yuvi - the son of Sultan and Begum who wants to follow his dreams.

===Secondary Characters===
* Alex - a multilingual parakeet
* Bagga - a sloth bear with a hat
* Bajrangi- a Monkey

===Cast=== Govinda as Bajrangi the Monkey 
* Akshaye Khanna as Alex the Parrot  
* Boman Irani as Bagga the Bear 
* Urmila Matondkar  as the Begum 
* Sunil Shetty as Sultan the Leopard
* Swini Khara as Yuvraj the Cub 

===English cast===
* Jason Alexander as Male Flamingo/Hyena Cook
* Cary Elwes as Bee Commander/Sultan
* Christopher Lloyd as Pigeon
* Jane Lynch as Female Flamingo
* Brian George as Bat
* Roger Craig Smith as The Two Monkeys
* J.B. Blanc as The Director/Prime Minister
* Dave Wittenberg as Kalia
* Troy Baker as Tiger
* GK Bowes as News Reporter #1
* Tara Strong as Yuvi
* Carlos Alazraqui as Bajrangi
* Kate Higgins as Antelope
* Lex Lang as Hyena #1 / News Reporter #2
* Joe Ochman as Man / News Reporter #3
* Fred Tatasciore as Hyena #2 
* Travis Willingham as Man in Shades
* Tom Kenny as Alex the Parrot
* Brad Garrett as Bagga Vanessa Williams as Begum

===Other cast===
* Prem Chopra as Kaalia
* Deepak Dobriyal as Hawa Hawai Sanjay Mishra as Marela
* Saurabh Shukla as Bharela
* Shivarna Mitra as Samira
* Girish Dhamija as Jari Booti Baba

==Release==
===Theatrical release===
The film was released in the United States on 7 December 2012. The film released in India on 19 October 2012.
===International releases===
* France - June 2012 (Annecy Animation Film Festival)
* India - 19 October 2012
* USA - 7 December 2012
* South Korea - 21 February 2013
* Russia - 11 April 2013
* China - 5 September 2014

===Home media===
Delhi Safari was released on DVD by Shemaroo Entertainment. 

==Reception==
===Critical response===
Delhi Safari won the National Award for the Best Animation Film for the year 2012. Annecy Film Festival, one of the most prestigious film fests for animation, credited Delhi Safari as its "Official Selection" on 4 June 2012.   Blue Sky Madagascar and Warner Brothers Happy Feet respectively.

===Box office===
Despite the hype, the film failed at the box office with poor collections and losing money for the distributors and financiers. It was the lowest opening for any Bollywood film in USA in the history with less than $4000 total in 20 screens on opening day, and only 17.5&nbsp;million nett during the first week.  

===Accolades=== National Film Best Animated Film,    it was named as Best Film in the animation category for the Screen Awards.  It also won Best Feature Film award at FICCI Frames 2012. It won the best animated feature at Infocom Assocham EME awards 2013. At the 60th National Film Awards, it won the National Film Award for Best Animated Film.

==Soundtrack==
{{Infobox album|
| Name = Delhi Safari
| Type = Soundtrack
| Artist = Shankar Ehsaan Loy
| Cover =
| Border = yes
| Alt = 
| Caption = Original CD Cover
| Released =
| Recorded = 2012 Feature film soundtrack
| Length =  Hindi
| Label = Artist Aloud
| Producer = 
| Reviews =  
| Last album = Chittagong (film)|Chittagong (2012)
| This album = Delhi Safari (2012)
| Next album = Vishwaroopam (2012)
}}
{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    =
| all_music       = Shankar-Ehsaan-Loy
| all_lyrics      = Sameer
| title1          = Dil Ki Safari
| extra1          = Shankar Mahadevan, Raman Mahadevan, Shivam Mahadevan, Hamsika Iyer
| length1         = 4:17
| title2          = Meri Duniya Terey Dam Se
| extra2          = Shekhar Ravjiani, Mahalakshmi Iyer & Shivam Mahadevan 
| length2         = 3:58
| title3          = Aao Re Pardesi
| extra3          = Karsan Sargathia, Tarannum Mallik
| length3         = 3:58
| title4          = Dhadak Dhadak
| extra4          = Shankar Mahadevan, Raghubir Yadav
| length4         = 2:14
| title5          = Jungle Mein Mangal
| extra5          = Shankar Mahadevan 
| length5         = 3:05
| title6          = To Forgive...To Forget
| extra6          = Vanessa Williams, John Fluker
| length6         = 1:10
}}
 Original Song category for the 85th Academy Awards.   

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 