Room of Death
{{Infobox film
| name     = Room of Death
| image    = 
| director = Alfred Lot
| producer = Charles Gassot 
| screenplay = Alfred Lot 
| based on =  
| starring = Mélanie Laurent   Éric Caravaca   Gilles Lellouche   Jonathan Zaccaï   Céline Sallette   Laurence Côte
| cinematography = Jérôme Alméras 
| music = Nathaniel Méchaly 
| released =  
| runtime = 115 minutes
| country = France
| language = French
}}

Room of Death ( ) is a 2007 French crime film based on the novel La Chambre Des Morts by Franck Thilliez. 

== Cast ==
* Mélanie Laurent as Lucie Hennebelle
* Éric Caravaca as Moreno / Pierre Norman
* Gilles Lellouche as Sylvain
* Jonathan Zaccaï as Vigo
* Céline Sallette as Annabelle
* Laurence Côte as Alex
* Jean-François Stévenin as Léon
* Fanny Cottençon as La mère de Lucie 
* Nathalie Richard as Raphaelle
* Lola Créton as Eléonore

== References ==
 

== External links ==
* 

 
 
 


 