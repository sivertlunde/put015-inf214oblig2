Le Voyage en Amérique
{{Infobox film
| name           = Le Voyage en Amérique
| image          = 
| caption        = 
| director       = Henri Lavorel
| producer       = Le monde en image (Henri Lavorel)
| writer         = Henri Lavorel Roland Laudenbach
| starring       = Madeleine Barbulée Louis de Funès
| music          = Francis Poulenc
| cinematography = Henri Alekan
| editing        = Andrée Feix
| distributor    = S.R.O
| released       = 18 October 1952 (USA)
| runtime        = 91 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1952, directed by Henri Lavorel, written by Roland Laudenbach, starring Madeleine Barbulée and Louis de Funès. It is also known as "The Voyage to America" and "Trip to America". 

== Cast ==
* Madeleine Barbulée: the housekeeper
* Pierre Fresnay: Gaston Fournier
* Yvonne Printemps: Clotilde Fournier
* Jean Brochard: the mayor
* Claude Laydu: François Soalhat
* Olivier Hussenot: Mr Soalhat (the gardener)
* Jane Morlet : Marie
* Yvette Etievant: the post clerk
* Lisette Lebon: Marguerite
* Claire Gérard: Mrs Tassote
* Maurice Jacquemont: the priest
* Pierre Destailles: the cook
* Christian Fourcade: the rascal
* Louis de Funès: the employee of Air France

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 