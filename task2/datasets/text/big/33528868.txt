Much Ado About Nothing (2012 film)
 
{{Infobox film
| name           = Much Ado About Nothing
| image          = MuchAdo.jpg
| image_size     = 220px
| border         = yes
| alt            =  
| caption        = Theatrical release poster
| director       = Joss Whedon
| producer       = {{Plain list |
*Joss Whedon
*Kai Cole
}}
| screenplay     = Joss Whedon
| based on       =  
| starring       = {{Plain list |
*Amy Acker
*Alexis Denisof
*Reed Diamond
*Nathan Fillion
*Clark Gregg
*Fran Kranz
*Sean Maher
*Jillian Morgese
}}
| music          = Joss Whedon
| cinematography = Jay Hunter
| editing        = {{Plain list |
*Daniel Kaminsky
*Joss Whedon
}}
| studio         = Bellwether Pictures
| distributor    = {{Plain list | Lionsgate
*Roadside Attractions
}}
| released       =  
| runtime        = 108 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $5,300,644      
}} play of the same name. The film stars Amy Acker, Alexis Denisof, Nathan Fillion, Clark Gregg, Reed Diamond, Fran Kranz, Sean Maher, and Jillian Morgese.

To create the film, director Whedon established the production studio Bellwether Pictures. It premiered at the 2012 Toronto International Film Festival and had its North American theatrical release on June 21, 2013.

==Plot== original play. Differences include the modern-day setting, switching Conrades gender, eliminating several minor roles and consolidating others into Leonatos aide, and expanding Ursulas role by giving her a number of Margarets scenes.  In addition, the film attempts to add background to the relationship between Beatrice and Benedick by showing, in an opening scene, a morning after they apparently slept together. Benedick steals away quietly while Beatrice pretends to be asleep.

==Cast==
* Amy Acker as Beatrice, niece of Leonato.
* Alexis Denisof as Benedick, of Padua; companion of Don Pedro. Don Pedro, Prince of Aragon.
* Nathan Fillion as Dogberry, the constable in charge of Messinas night watch.
* Clark Gregg as Leonato, governor of Messina; Heros father.
* Fran Kranz as Claudio, of Florence; a count, companion of Don Pedro, friend to Benedick.
* Sean Maher as Don John, "the Bastard Prince," brother of Don Pedro.
* Jillian Morgese as Hero, Leonatos daughter.
* Spencer Treat Clark as Borachio, follower of Don John.
* Riki Lindhome as Conrade, lover of Don John (originally, follower of Don John, a male role).
* Ashley Johnson as Margaret, waiting-gentlewoman attendant on Hero.
* Emma Bates as Ursula, waiting-gentlewoman attendant on Hero.
* Tom Lenk as Verges, the Headborough, Dogberry’s partner Nick Kocher as First watchman Brian McElhaney as Second watchman
* Joshua Zar as Leonato’s aide
* Paul M. Meston as Friar Francis, a priest.
* Romy Rosemont as The Sexton, the district attorney at Borachio and Conrades interrogation (originally, the judge of the trial of Borachio, a male role).
 The Avengers at the time, and stepped in to play the part.    Most of the cast had worked with Whedon before; Acker and Denisof on Angel (TV series)|Angel; Denisof, Fillion, Lenk and Lindhome on Buffy the Vampire Slayer; Fillion and Maher on Firefly (TV series)|Firefly; Acker, Denisof, Diamond, Kranz and Johnson on Dollhouse (TV series)|Dollhouse; Gregg, Denisof, Rosemont, Johnson and Morgese in The Avengers.       

==Production== studio Bellwether The Avengers. wrapped their last day of filming on October 23, 2011.   

Whedon explained his initial interest in the project, saying:

 

He elaborated on that sentiment, and said "It’s a very cynically romantic text about love, and how we behave, and how we’re expected to behave. It’s a party, but there’s something darker there as well". Inspired by the exposing nature of Filmmaking|film, Whedon decided to infuse a recurring motif of Human sexuality|sexuality, "...because it’s a visual medium. You can say it or you can show it.   There’s an element to it, of debauchery, that was fun for a time but then it was just sort of dark".    Whedons idea to adapt the play for the screen originated from having "Shakespeare readings" at his house with several of his friends, years prior.   
 DP Jay natural lighting digitally with RED Epic, Composer with Canon 7D to differentiate certain scenes.   

==Soundtrack==
{{Infobox album  
| Name        = Much Ado About Nothing: Original Score
| Type        = Soundtrack
| Artist      = Joss Whedon
| Released    =  
| Genre       = Film score
| Length      = 40:41
| Label       = Bellwether Records
| Producer    = Deborah Lurie
}} the play. These tracks were performed by Maurissa Tancharoen and Jed Whedon.    Whedon described the experience of making his debut in scoring a film as "terrifying", going on to say that "when I’m terrified, I know I’m having fun".    He acknowledged as well that hiring himself to do it resulted from monetary constraints.    The soundtrack was released digitally on June 6, 2013. 

{{tracklist
| collapsed       = yes
| writing_credits = yes
| all_writing     = Joss Whedon, except where noted
| title1          = Main Title
| length1         = 1:12
| title2          = Arrival
| length2         = 0:55
| title3          = Hero
| length3         = 0:49
| title4          = If I Had My Mouth
| length4         = 2:06
| title5          = To the Death
| length5         = 0:42
| title6          = Sigh No More
| note6           = featuring Maurissa Tancharoen & Jed Whedon
| writer6         = William Shakespeare
| length6         = 2:36
| title7          = Beauty Is a Witch
| length7         = 1:54
| title8          = A Double Heart
| length8         = 0:41
| title9          = Perfectest Herald
| length9         = 1:07
| title10         = The Only Love Gods
| length10        = 0:38
| title11         = Borachio
| length11        = 2:01
| title12         = The Gulling (Part 1)
| length12        = 0:27
| title13         = The Gulling (Part 2)
| length13        = 1:56
| title14         = The Gulling (Part 3)
| length14        = 1:03
| title15         = Love On
| length15        = 0:52
| title16         = Disloyal
| length16        = 2:46
| title17         = A Thousand Ducats
| length17        = 2:08
| title18         = Wedding Day
| length18        = 0:30
| title19         = Madam Withdraw
| length19        = 0:31
| title20         = Wedding March
| length20        = 0:57
| title21         = Left for Dead
| length21        = 2:14
| title22         = Is Not That Strange
| length22        = 0:43
| title23         = I Am Engaged
| length23        = 2:00
| title24         = A Word in Your Ear
| length24        = 1:39
| title25         = How Innocent She Died
| length25        = 1:10
| title26         = Heavily
| note26          = featuring Maurissa Tancharoen & Jed Whedon
| writer26        = William Shakespeare
| length26        = 1:58
| title27         = The Balcony
| length27        = 1:13
| title28         = Will You Come
| length28        = 0:33
| title29         = Walk of Shame
| length29        = 0:36
| title30         = Another Hero
| length30        = 0:50
| title31         = A Giddy Thing
| length31        = 1:10
| title32         = Last Dance
| length32        = 0:44
| total_length    = 40:41
}}

==Release==
  premiere.]] world premiere distribution rights UK distribution 2013 Jameson 2013 Glasgow 2013 Istanbul 2013 Bradford 2013 Belfast 2013 Filmfest 2013 Helsinki 2013 South 2013 Wisconsin 2013 San 2013 Independent 2013 Seattle International Film Festival.                The limited release in New York City, Los Angeles and San Francisco was expanded on June 14, 2013.    On June 21, 2013, it released in 200-300 screens nationwide.   

===Rating=== 12A certificate in the United Kingdom from the British Board of Film Classification.   

===Overseas releases=== New Zealand International Film Festival in July 2013.    The film was released theatrically in the United Kingdom.   

==Reception==
===Box office===
;North America Lincoln Film Center Society Theater broke the venues house record.    With the expansion into 18 additional theaters in the second week of its limited release, the film garnered an amount of $162,580.   

The first weekend of wide release in the U.S. grossed $762,350 from 206 theaters, which accumulated a total amount of $1,234,781 since release.    It earned $590,000 after the second week.    The fourth week held an overall aggregate of $263,700.    Domestic total gross amounted to $4,328,850. 

;Other territories
The films opening weekend in the United Kingdom grossed $101,237 from having been screened at 64 locations.    The box office numbers for Australias opening weekend amounted to $78,196.    The foreign box office contributed $971,794 to the films final cume. 

===Critical reaction===
Much Ado About Nothing has received generally positive reviews from critics, earning an 84% approval rating on  , the film has achieved an average score of 78/100 based on 37 reviews, signifying "generally favorable reviews".   

 
 Romeo and noir to Salon wrote, "  possesses that Whedon-esque nerdy energy, fizzing with humor, eroticism, booze and more than a hint of danger".    Kenneth Turan of the Los Angeles Times thought the film was "good-humored and unpretentious in equal measure", going on to praise its visual performance.    IGN gave it a 7.5 out of 10, noting that "everyone should see this movie".    Joe Morgenstern of The Wall Street Journal gave high encomium to Fran Kranz|Kranzs performance, expressing that the actor "portrays Claudio with affecting passion", and says of the film, "The joyous spirit of the play has been preserved in this modest, homegrown production".    Rolling Stone journalist Peter Travers wrote that the film was "an irresistible blend of mirth and malice".    Justin Chang of Variety (magazine)|Variety sensed that the black-and-white evoked a "timeless romanticism", which was additionally enhanced by the "lightly applied score".    The Daily Mail s Chris Tookey said that the film was "the first five-star movie of the summer".    Chris Nashawaty of Entertainment Weekly—despite commending it for being "both daring and delightfully daffy"—admits, "The film isnt as fast and funny as it could be".   

====Top ten lists====
* 4th&nbsp;– David Edelstein, New York (magazine)#Digital expansion and blogs|Vulture   
* 4th&nbsp;– Stephanie Zacharek, The Village Voice   
* 9th&nbsp;– Lou Lumenick, New York Post   
* Top 10 (listed alphabetically, not ranked)&nbsp;– National Board of Review   

===Accolades===

{| class="wikitable sortable" width="99%"
|- style="background:#ccc; vertical-align:bottom;"
! colspan="5" style="background: LightSteelBlue;" | List of awards and nominations
|- style="background:#ccc; text-align:center;"
! Year
! Award
! Recipient(s) and nominee(s)
! Result
! class="unsortable" |  }}
|- style="border-top:2px solid gray;"
|-
| 2013 13th Belfast Film Festival Audience Award
| Joss Whedon
|  
|     
|-
| 2013 IFF Boston Audience Award for Best Narrative Feature
| Much Ado About Nothing
|  
|     
|}

===Home media=== 1080p video, UltraViolet digital audio commentaries Guinness World Record for having the most people record a Blu-ray or DVD commentary, the amount of which was 16.   

==See also==
* List of black-and-white films produced since 1970

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 