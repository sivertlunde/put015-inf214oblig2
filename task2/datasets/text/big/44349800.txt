The Girl in the Show
{{Infobox film
| name           = The Girl in the Show
| image          = 
| alt            = 
| caption        = 
| director       = Edgar Selwyn
| producer       = 
| screenplay     = Edgar Selwyn Joseph Farnham
| based on       =  
| starring       = Bessie Love Raymond Hackett Edward Nugent Mary Doran Jed Prouty
| music          = 
| cinematography = Arthur Reed
| editing        = Harry Reynolds 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Girl in the Show is a 1929 American comedy film directed by Edgar Selwyn and written by Edgar Selwyn and Joseph Farnham. The film stars Bessie Love, Raymond Hackett, Edward Nugent, Mary Doran, and Jed Prouty. The film was released August 31, 1929, by Metro-Goldwyn-Mayer.  

== Cast ==	 
*Bessie Love as Hattie Hartley
*Raymond Hackett as Mal Thorne
*Edward Nugent as Dave Amazon 
*Mary Doran as Connie Bard
*Jed Prouty as Newton Wampler
*Ford Sterling as Ed Bondell
*Nanci Price as Oriole
*Lucy Beaumont as Lorna Montrose
*Richard Carlyle as Leon Montrose
*Alice Moe as Grace Steeple Frank Nelson as Tracy Boone Jack McDonald as Ernest Beaumont
*Ethel Wales as Mrs. Truxton
*John F. Morrissey as Jeff Morgan

==Plot==
A traveling Tom show is stranded in Kansas when their manager steals what meager funds they have. Hattie Hartley (Love), who plays Eva in the production, decides to marry the local undertaker, so that he will fund the troupe and pay for her younger sisters schooling. On the day of the wedding, the troupe is booked for a performance at the last minute. Hattie refuses to get married so that she can play the role of Eva, an act which reunites her with her true love, a member of their troupe. 

==Reception==
The film received negative reviews, with one reviewer claiming that people were yelling at the screen in the theater. 

==See also==
* Racism in early American film

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 

 