The Amateur (1999 film)
{{Infobox film
| name           = El Amateur
| image          = ElAmateur.jpg
| image_size     =
| caption        = DVD Cover
| director       = Juan Bautista Stagnaro
| producer       = Juan Bautista Stagnaro
| writer         = Juan Bautista Stagnaro Mauricio Dayub
| narrator       =
| starring       = Mauricio Dayub Vando Villamil
| music          = Jaime Roos
| cinematography = Víctor González (cinematographer)|Víctor González
| editing        = Miguel Pérez
| distributor    = Aleph Producciones S.A.
| released       = April 22, 1999
| runtime        =
| country        = Argentina
| language       = Spanish
| budget         =
|}} Argentine drama film released in 1999 in film|1999, written and directed by Juan Bautista Stagnaro. 

The film was based on a novel by Mauricio Dayub.

The picture was nominated for five Silver Condor Awards.

==Plot==

On the outskirts of a provincial town, Pajaro is determined to enter the Guinness Book of Records by breaking the longest bike-ride record.

He rides around the fountain in the square with the help of his friend, Lopecito. He is willing to face many challenges that face him, in attaining this new record.

Will this race open the path to love for him, and for his redemption?

==Cast==
*Mauricio Dayub as Alfonso Pájaro Romero
*Pedro Heredia as Ramón
*Vando Villamil as Lopecito
*Arturo Goetz as Concejal
*Cacho Espíndola as Intendente
*Alejandra Puy as La Mora
*Darío Grandinetti as Funcionario municipal

==Exhibition== America in Miami at the Hispanic Film Festival.

The film was produced by Aleph Producciones S.A. and distributed by Transeuropa Video Entertainment on VHS.

==Footnotes==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 
 