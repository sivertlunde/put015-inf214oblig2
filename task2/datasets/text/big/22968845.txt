In Her Line of Fire
{{Infobox film
| name           = In Her Line of Fire
| image          = In her line of fireposter.jpg
| caption        = Promotional poster
| director       = Brian Trenchard-Smith
| producer       = Brian Trenchard-Smith
| writer         = Sherry Admo Paula Goldberg Anna Lorenzo Jill Bennett Robbie Magasiva David Reynolds
| cinematography = Neil Cervin
| editing        = Asim Nuraney
| distributor    = here! Films Regent Releasing
| released       = April 21, 2006
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget          = $1.3 million   accessed 9 February 2013 
}}
 Jill Bennett. It is also known as Air Force Two in several countries.

Much of the filming was done in New Zealand.

== Plot ==
 South Pacific, when its damaged by an electrical storm. Its forced to ditch in the ocean off the coast of the (fictional) country of San Pietro, near Solomon Islands. 

As the few survivors make it onto a beach of a small off-shore island, one is immediately shot dead by a sniper from a guerrilla camp. The remaining survivors, including the Vice-President, who is a former US Marine, manage to escape and hide. It emerges that the sniper belongs to a group of armed rebel forces intent on overthrowing the islands dictatorial government. The rebels are led by Armstrong, a ruthless American mercenary.

The V-P is eventually caught by the rebels, who plan to hold him for ransom. It falls to his two aides, Secret Service agent Lynn Delaney (Hemingway) and press secretary Sharon Serrano to infiltrate the rebel camp and save him. They are also captured, but Delaney is able to escape and sets out to rescue the others.

As US helicopters search for them, Delaneys military and combat skills enable her to rescue them and kill Armstrong. 

In return for the V-Ps rescue and the cooperation of some of the rebel forces, the US offers aid to San Pietro on condition that they agree to hold democratic elections.

== Cast ==

* Mariel Hemingway as Sergeant Major Lynn Delaney
* David Keith as Vice President Walker
* David Millbern as Armstrong Jill Bennett as Sharon Serrano
* Robbie Magasiva as Petelo

==Production==
Brian Trenchard-Smith had previously directed a gay action film, Phantom Below. He described In Her Life of Fire as:
 A Lesbian Rambo movie... a homage to 80′s gun porn, starring Mariel Hemingway as the Vice President’s sapphic secret service chief. This future Vice President, played by David Keith, in a utopian Washington where universal health care is passed by unanimous vote, actively encourages the seduction of one lesbian by another.  ”You’re a marine. Go for it.” I hope someone sent a copy to the Cheney family. Both films are gay-lite. Just a few kisses. The concept of  a gay hero/heroine in the Harrison Ford role was the point.  
The film was shot over 12 days in New Zealand and 2 days in Vancouver.   accessed 4 December 2012 

== Box office ==
With a budget of an estimated $1,000,000,  it made only $884 at the box office in its limited release.  However it sold widely to TV and video around the world. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 