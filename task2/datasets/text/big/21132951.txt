Aparan (film)
{{Infobox film
| name = Aparan
| image = Aparan.JPG
| image size =
| alt =
| caption = Promotional Poster
| director = P. Padmarajan
| producer = Ajitha
| story = P. Padmarajan M. K. Chandrasekharan
| screenplay = P. Padmarajan 
| narrator =  Mukesh Madhu Madhu Parvathy Parvathi Sukumari Johnson
| Venu
| editing = B. Lenin
| studio = Supriya International
| distributor = Thomson Films
| released =   
| runtime = 115 minutes
| country = India
| language = Malayalam
| budget =
| gross =
}} Malayalam mystery Parvathy and Madhu (actor)|Madhu. Loosely based on the short story Aparan by Padmarajan, the film is about mistaken identity and the problems that a young, innocent man had to undergo in his life. 

The film was the debut of Jayaram and it was a critical as well as commercial success.

==Plot==
Vishwanathan (Jayaram), an innocent young man in Kochi for a job interview is mistaken by many for a hardcore criminal and is arrested by police. His similarity to the criminal is so accurate that even cops mistakes him to a local criminal. Luckily, George Kutty (Mukesh (Malayalam actor)|Mukesh), the police inspector, happens to be the classmate of Vishwanathan, and gets him released.  George Kutty explains Vishwanathan about the crimes done by the criminal, whose real name and background is a mystery even for police. Looking just the same as Vishwanathan,  he has several criminal cases pending against him and is on run. Vishwanathan, joins a job in city and falls in love with his co-worker Ambili (Shobhana). Mohandas, his MD in the company one day has a visitor in his office, who again mistakes VIshwanathan for the criminal, which ends in losing his job. The problems faced by Vishwanathan continues, including the breaking down of a marriage proposal for his sister. Vishwanathn thus loses hope in life and decides to take revenge against the criminal. He imposters himself as the criminal and extorts money there by inviting ire of the criminal. Vishwanthan is  thus chased by the criminal. In a bitter fight, the criminal is accidentally stabbed to death by one of his accomplices. But his body is mistaken to be of Vishwanathans as a bag containing educational certificates and bank passbook was lying near the body. Vishwanathan on reaching home witnesses his family performing last rites. That night, Vishwanathan appears in front of his father and explains him the reality. His father consoles Vishwanathan and tries to take him inside the house. Vishwanathan refuses to go with him and decides to live as the criminal for the rest of his life as a sweet revenge for ruining his life.
However, a smiling Jayaram, walking around the cremation site, is shown in the very last scene of the movie, leaving the viewer wondering if the man who was killed was the criminal or Vishwanathan.

==Cast==
* Jayaram as Vishwanathan
* Shobana as Ambili Madhu as Vishwanathans father Mukesh as George Kutty
* Sukumari
*Jalaja as Sumangala teacher Shari as Anna
* M. G. Soman as Vishwanathans boss Parvathi as Sindhu Vishwanathans sister Innocent
*Valsala Menon
*K. P. A. C. Sunny
*V K Sreeraman
* Ajith Kollam as the criminals accomplice
* Indrans as Bit role

==Reception==
On release, Aparan was a big hit in Kerala.  This film launched Jayaram and is still considered as one of his best performance. The DVDs are still on hot demand among the young film buffs. Over the time, Aparan is considered as a classic film.The Background score adds charm to the film.

==Trivia==

Aparan had no songs and had just a handful of characters. The highlight of this film is that the other person, whom Vishwanathan is mistaken to be, is never shown. It is only from the dialogues of other characters that we come to know about him. He even doesnt carry a name and is  addressed as he. But in the last few scenes, we happen to hear his voice, while threatening Vishwanathan over phone.  

Parvathy Jayaram, who acted as the sister of Jayaram, later got married to him in 1992. P. Padmarajan, who launched Jayaram, is also considered as his mentor in cinema. In the later years Jayaram acted in two more films by P. Padmarajan.

The basic idea of the film is based on the short story "Aparan" by P. Padmarajan . The credit for story has been given to Padmarajan and M. K. Chandrasekharan.

==External links==
*  

 

 
 
 
 
 
 
 
 