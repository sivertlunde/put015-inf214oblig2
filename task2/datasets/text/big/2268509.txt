Wish You Were Here (1987 film)
{{Infobox film
| name           = Wish You Were Here
| image          = wish-you-were-here.jpg
| image_size     = 225px
| caption        = DVD cover
| director       = David Leland
| writer         = David Leland Tom Bell
| producer       = Sarah Radclyffe
| music          = Stanley Myers Ian Wilson
| editing        = George Akers
| distributor    = Atlantic Releasing Corp
| released       =  
| budget         = $1.5 million
| gross          =
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English

}}
 Tom Bell. It was written and directed by David Leland.    The original music score was composed by Stanley Myers. 

==Plot== Tom Bell), her life changes.  She becomes pregnant and her father, a somewhat rigid and conventional man, disowns her. Desperately she tries to seek an illegal abortion but in the end decides to become a mother.

"Wish you were here" is a sigh Lynda makes because of her dead mother, who understood her and protected her from her intolerant father. Beneath her cheeky exterior, Lynda is a vulnerable girl who seeks love and a place in life; she lives in a time when it was difficult for teenagers like her to do that in their own way.

==Cast==
  
*Emily Lloyd as Lynda Mansell Tom Bell as Eric 
*Jesse Birdsall as Dave 
*Clare Clifford as Mrs. Parfitt 
*Barbara Durkin as Valerie 
*Geoffrey Hutchings as Hubert Mansell  
*Charlotte Barker as Gillian 
*Chloë Leland as Margaret 
*Charlotte Ball as Lynda (aged 11) 
 
*Pat Heywood as Aunt Millie 
*Abigail Leland as Margaret (aged 7) 
*Geoffrey Durham as Harry Figgis  Neville Smith as Cinema manager 
*Heathcote Williams as Dr. Holroyd
*Val McLane as Maisie Mathews
*Susan Skipper as Lyndas Mother 
*Lee Whitlock as Brian  Sheila Kelley as Joan Figgis 
 

==Production== madam Cynthia Payne as an adolescent growing up on the Sussex coast.  It was filmed in the Sussex towns of Worthing and Bognor Regis.

==Reception==
Wish You Were Here has an overall approval rating of 89% on Rotten Tomatoes. 

==Awards and honors==
*1987: Evening Standard British Film Awards Emily Lloyd for Best Actress
*1987: National Society of Film Critics Emily Lloyd for Best Actress BAFTA Award David Leland for Best Screenplay BAFTA Award Nomination Best Actress

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 