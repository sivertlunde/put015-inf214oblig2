Womb (film)
 
{{Infobox film
| name = Womb
| image = Womb film.jpg
| caption =
| director = Benedek Fliegauf
| producer =
| writer = Benedek Fliegauf Elizabeth Szasz Matt Smith
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 107 min.
| country = Germany
| language = English
| budget = $13 million
| gross =
}} Matt Smith.

==Plot==
The film commences with a pregnant woman (Eva Green) telling her unborn child that the father has departed for good, but that together they will start a new life. A love story is then told between two children, Rebecca and Tommy, who swear each other eternal love. When Rebecca departs suddenly for Japan with her mother, the two are separated. Twelve years later, Rebecca returns as a young woman to find that Tommy (Matt Smith) not only remembers her, but still cares deeply for her. The two begin a new relationship.
 biotech corporations, who plan to open a new natural park populated by animals artificially created by cloning. Tommy plans to spoil the inauguration ceremony by letting loose rucksacks filled with cockroaches. Rebecca, herself a computer programmer of leak detection sonar software for underground storage containers, insists on accompanying Tommy.

Driving to the site of the new natural park through a lonely wilderness, Rebecca asks Tommy to stop the car so that she can relieve herself at the side of the road. Meantime, Tommy leaves the car and is struck and killed suddenly by a passing vehicle.

Rebecca and Tommys parents are stricken with grief. Rebecca wants to use new scientific advancements to have Tommy cloned and thereby bring him back to life. She offers to be impregnated using Tommys DNA. Though Tommys mother objects, his father agrees to give Rebecca Tommys cell material, but urges her to think through her decision carefully before proceeding. Rebecca, however, continues and gives birth to a new Tommy by Caesarean section.

Tommy is now raised as Rebecca’s son, and the two have a close relationship. Rebecca presents to him a pleo, an artificial living animal created using new biotechnology. Tommy and his playmates observe a neighbourhood girl and try to determine if she has a “copy smell” as the girl is a clone. The neighbourhood mothers display prejudice against “copies”, expecting Rebecca to not let Tommy associate with them. Rebecca, though horrified, agrees in order not to isolate her son. Eventually rumours about Tommy spread, and Tommy is forced to celebrate his birthday alone with his mother, his playmates all being barred from attending by their mothers.

Rebecca moves to a more remote location with Tommy. Tommy begins to ask questions about his father, wanting to know how his father died. He buries the pleo     his mother gave him for his birthday while out playing with his friend. His mother finds out and gives him back the pleo,  which is now no longer working.

Years later, Tommy has grown as old as he was when he died in his first life. He is now the adult son of still-youthful Rebecca. When Tommy brings a girlfriend, Monica, home to stay with them Rebecca behaves jealously, to both Tommys and Monicas bewilderment. Tommy struggles with what appears to be sexual tension between himself and his mother.  The original Tommys mother, now an old woman, arrives unexpectedly and stares silently at Tommy, who feels he recognizes the stranger. Frightened and frustrated by Rebeccas lack of explanation, Tommy lashes out at Rebecca, ignoring  Monica, who quickly departs.

An angry Tommy demands answers from his mother, Rebecca, who gives him original Tommys old laptop with pictures of himself and his mother and father. Tommy initiates sex with Rebecca, and from the blood on her hand, it is implied he took her virginity in the process. The next day Tommy packs his things, then thanks Rebecca for the life hes had and then leaves. Rebecca is pregnant with Tommys child.

==Cast==
*Eva Green as Rebecca Matt Smith as Thomas "Tommy"
*Lesley Manville as Judith Peter Wight as Ralph
*István Lénárt as Henry
*Hannah Murray as Monica
*Ruby O. Fee as Rebecca (child)
*Tristan Christopher as Thomas (child)
*Natalia Tena as Rose
*Ingo Hasselbach as Walter Ella Smith as Molly
*Wunmi Mosaku as Erica
*Amanda Lawrence as Teacher Jennifer Lim as Mrs. Muju 2
*Laurence Richardson as Simon
*Lukas Hawkins
*(Living in the Woods Akando) as the wolfdog

==External links==
*  

==References==
 

 
 
 
 
 
 