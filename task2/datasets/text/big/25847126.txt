Tora-san's Shattered Romance
{{Infobox film
| name = Tora-sans Shattered Romance
| image = Tora-sans Shattered Romance.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Akira Miyazaki
| starring = Kiyoshi Atsumi Ayako Wakao
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 90 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  aka Tora-san in Love  is a 1971 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Ayako Wakao as his love interest or "Madonna".  Tora-sans Shattered Romance is the sixth entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
A girl who has escaped from an institution falls in love with Tora-san. He decides to marry her, but his family and the girls teacher break up the romance.    

==Cast==
* Kiyoshi Atsumi as Torajiro 
* Chieko Baisho as Sakura
* Ayako Wakao as Yūko Akashi
* Shin Morikawa as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Chishū Ryū as Gozen-sama
* Hisao Dazai as Tarō Ume
* Gajirō Satō as Genkō
* Hisaya Morishige as Senzō
* Nobuko Miyamoto as Kinuyo
* Tatsuo Matsumura as Doctor Yamashita
* Gorō Tarumi as Yūkos husband

==Critical appraisal==

For his work on Tora-sans Shattered Romance as well as the next two entries in the Otoko wa Tsurai yo series, Tora-san, the Good Samaritan and Tora-sans Love Call (all 1971), Yoji Yamada tied for Best Director at the Mainichi Film Awards with Masahiro Shinoda.  The German-language site molodezhnaja gives Tora-sans Shattered Romance three and a half out of five stars.   

==Availability==
Tora-sans Shattered Romance was released theatrically on January 15, 1971.  In Japan, the film has been released on videotape in 1983 and 1995, and in DVD format in 2005 and 2008. 

==References==
 

==Bibliography==

===English===
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 


 