The Searchers (film)
{{Infobox film
| name           = The Searchers
| image          = SearchersPoster-BillGold.jpg
| caption        = Bill Golds US theatrical release poster
| director       = John Ford
| producer       = Cornelius Vanderbilt Whitney
| screenplay     = Frank S. Nugent
| based on       =  
| starring       = John Wayne Jeffrey Hunter Vera Miles Ward Bond Natalie Wood
| music          = Original Score:  
| cinematography = Winton C. Hoch Jack Murray
| studio         = C.V. Whitney Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = $3.75 million 
| gross          = 
}}
 Western film Civil War veteran who spends years looking for his abducted niece (Natalie Wood), accompanied by his adoptive nephew (Jeffrey Hunter). Critic Roger Ebert found Waynes character, Ethan Edwards, "one of the most compelling characters Ford and Wayne ever created". 
 one of greatest American 100 greatest American movies of all time.  Entertainment Weekly also named it the best western.  The British Film Institutes Sight & Sound magazine ranked it as the seventh best film of all time based on a 2012 international survey of film critics   and in 2008, the French magazine Cahiers du cinéma ranked The Searchers number 10 in their list of the top 100 best films ever made.  

==Plot== Civil War Mexican revolutionary Texas Rangers; he refuses. As Rev. Captain Samuel Clayton (Ward Bond) remarks, Ethan "fits a lot of descriptions".
 homestead in Dorothy Jordan), and their son Ben (Robert Lyden) are dead, and Debbie and her older sister Lucy (Pippa Scott) have been abducted.

After a brief funeral the men set out in pursuit.  They come upon a burial ground of Comanches who were killed during the raid.  Ethan mutilates one of the bodies.  When they find the Comanche camp, Ethan recommends a frontal attack, but Clayton insists on a stealth approach to avoid killing the hostages.  The camp is deserted, and further along the trail the men ride into an ambush.  Though they fend off the attack, the Rangers are left with too few men to fight the Indians effectively.  They return home, leaving Ethan to continue his search for the girls with only Lucys fiancé, Brad Jorgensen (Harry Carey, Jr.) and Debbies adopted brother, Martin Pawley (Jeffrey Hunter).  Ethan finds Lucy brutally murdered and presumably raped in a canyon near the Comanche camp.  In a blind rage, Brad rides directly into the Indian camp and is killed.

 ) to Ethan (John Wayne) as they realize they are caught in a trap and must run for their lives.]]
 Henry Brandon), Nawyecka band of Comanches. A year or more later, Laurie receives a letter from Martin describing the ongoing search. In reading the letter aloud, Laurie narrates the next few scenes, in which Ethan kills Futterman for trying to steal his money, Martin accidentally buys a Comanche wife (Beulah Archuletta), and the two men find a portion of Scars band killed by soldiers.

The search leads Ethan and Martin to a military fort, and then to New Mexico, where a Mexican man leads them to Scar. They find Debbie after five years, now an adolescent (Natalie Wood), living as one of Scars wives. She tells the men that she has become a Comanche, and wishes to remain with them.  Ethan would rather see her dead than living as an Indian, and tries to shoot her, but Martin shields her with his body and a Comanche wounds Ethan with an arrow as they escape.  Though Martin tends to Ethans wound, he is furious with him for attempting to kill Debbie, and wishes him dead. "Thatll be the day," Ethan replies, as they return home.
 
Meanwhile, Charlie McCorry (Ken Curtis) has been courting Laurie in Martins absence. Ethan and Martin arrive home just as Charlie and Lauries wedding is about to begin. After a fistfight between Martin and Charlie, a nervous "Yankee" soldier, Lt. Greenhill (Patrick Wayne), arrives with news that Ethans half-crazy friend Mose Harper (Hank Worden) has located Scar. Clayton leads his men to the Comanche camp, this time for a direct attack, but Martin is allowed to sneak in ahead of the assault to find Debbie, who welcomes him.  Martin kills Scar during the battle, and Ethan scalps him.  Ethan then locates Debbie, and pursues her on horseback. Martin fears that he will shoot her as he has promised; but instead he sweeps her up onto his saddle. "Let’s go home," he says.  Debbie is reunited with her family, and Martin with Laurie.  In an iconic closing scene, Ethan departs the homestead as he arrived—alone—clutching his arm, the cabin door slowly shutting on his receding image.

==Cast==
 
* John Wayne as Ethan Edwards
* Jeffrey Hunter as Martin Pawley
* Vera Miles as Laurie Jorgensen
* Ward Bond as Rev. Capt. Samuel Johnson Clayton
* Natalie Wood as Debbie Edwards (older)
* John Qualen as Lars Jorgensen
* Olive Carey as Mrs. Jorgensen Henry Brandon as Chief Cicatriz (Scar)
* Ken Curtis as Charlie McCorry
* Harry Carey, Jr. as Brad Jorgensen
* Antonio Moreno as Emilio Figueroa
* Hank Worden as Mose Harper
* Beulah Archuletta as Wild Goose Flying in the Night Sky (Look)
* Walter Coy as Aaron Edwards Dorothy Jordan as Martha Edwards
* Pippa Scott as Lucy Edwards
* Patrick Wayne as Lt. Greenhill
* Lana Wood as Debbie Edwards (young)
 

==Production== performance as national craze, in the Jeffrey Hunter role, but Walt Disney, to whom Parker was under contract, refused to allow it and didnt tell Parker about the offer, according to Parkers videotaped interview for the Archive of American Television. Parker notes that this was by far his single worst career reversal. 

As part of its promotion of The Searchers, in 1956 Warner Bros. produced and broadcast one of the very first behind-the-scenes, "making-of" programs in movie history which aired as an episode of its Warner Bros. Presents  TV series.      

The Searchers is the first of only three films produced by Cornelius Vanderbilt Whitneys C. V. Whitney Pictures; the second being The Missouri Traveler in 1958 with Brandon deWilde and Lee Marvin, the last being The Young Land in 1959 with Patrick Wayne and Dennis Hopper.

==Historical background==

  village]] 1836 kidnapping child abductions Kiowa raiders killed him in 1871.   

The ending of Le Mays novel contrasts to the films, with Debbie, called Dry-Grass-Hair by the Comanches, running from the white men and from the Indians.  Marty, in one final leg of his search, finds her days later, only after she has fainted from exhaustion.
 4th Cavalry captured 124 Comanche women and children and imprisoned them at Fort Concho.

==Reception==
 , Utah.]]

Upon the films release, Bosley Crowther called it a "ripsnorting Western" (in spite of the "excessive language in its ads"); he credits Fords "familiar corps of actors, writers, etc.,   to give the gusto to this film. From Frank S. Nugent, whose screenplay from the novel of Alan LeMay is a pungent thing, right on through the cast and technicians, it is the honest achievement of a well-knit team."     Crowther noted "two faults of minor moment": 
*"Episode is piled upon episode, climax upon climax, and corpse upon corpse... he justification for it is that it certainly conveys the lengthiness of the hunt, but it leaves one a mite exhausted, especially with the speed at which it goes.
*"The director has permitted too many outdoor scenes to be set in the obviously synthetic surroundings of the studio stage...some of those campfire scenes could have been shot in a sporting-goods store window."
Variety called it "handsomely mounted and in the tradition of Shane (film)|Shane", yet "somewhat disappointing" due to its length and repetitiveness; "The John Ford directorial stamp is unmistakable. It concentrates on the characters and establishes a definite mood. Its not sufficient, however, to overcome many of the weaknesses of the story." 

The New York Herald Tribune termed the movie “distinguished”; Newsweek deemed it “remarkable.” Look described “The Searchers” as a “Homeric odyssey.” The New York Times praised Wayne’s performance as “uncommonly commanding.” 

The film earned rentals of $4.8 million in the US and Canada during its first year of release. 

==Later assessments== the greatest 100 greatest American films list ranked The Searchers in 12th place. In 1998, TV Guide ranked it 18th.  In 2008, the American Film Institute named The Searchers as the greatest Western of all time.  In 2010, Richard Corliss noted the film was "now widely regarded as the greatest western of the 1950s, the genres greatest decade" and characterized it as a "darkly profound study of obsession, racism and heroic solitude."  The film also maintains a perfect 100% rating on review aggregator, Rotten Tomatoes. 

The film has been recognized multiple times by the American Film Institute:
* AFIs 100 Years... 100 Movies – No. 96
* AFIs 100 Years... 100 Heroes and Villains:
** Ethan Edwards – Nominated Hero
* AFIs 100 Years... 100 Movie Quotes:
** "Lets go home, Debbie." – Nominated
* AFIs 100 Years... 100 Movies (10th Anniversary Edition) – No. 12
* AFIs 10 Top 10 – No. 1 Western Film

On "They Shoot Pictures Dont They," a site which numerically calculates critical reception for any given film, The Searchers has been recognized as the eighth most acclaimed movie ever made.  Members of the Western Writers of America chose its title song as one of the Top 100 Western songs of all time.   

Scott McGee noted that "...more than just making a social statement like other Westerns of the period were apt to do, Ford instills in The Searchers a visual poetry and a sense of melancholy that is rare in American films and rarer still to Westerns. 

==Critical interpretations==
 ) angrily confronts the Reverend (Ward Bond) after being interrupted while gunning down retreating Indians.]]
A major theme of the film is the historical attitude of white settlers toward Native Americans.  Ford was not the first to attempt this examination cinematically, but his harsh depiction was startling, particularly to later generations of viewers. Roger Ebert wrote, "I think Ford was trying, imperfectly, even nervously, to depict racism that justified genocide."     At the heart of The Searchers is Wayne’s performance as the angry, vengeful Ethan Edwards. From the beginning of his quest, it is clear that he is less interested in rescuing Debbie than in wreaking vengeance on the Comanches for the slaughter of his brother’s family.   

Film scholar Ed Lowry wrote: "... hile the Comanches are depicted as utterly ruthless, Ford ascribes motivations for their actions, and lends them a dignity befitting a proud civilization.  Never do we see the Indians commit atrocities more appalling than those perpetrated by the white man.  “Wayne is plainly Ahab,” wrote cultural critic Greil Marcus. “He is the good American hero driving himself past all known limits and into madness, his commitment to honor and decency burned down to a core of vengeance.”  Ford indicates that Scars cruelty is also motivated by revenge ("Two sons killed by white men. For each son, I take many... scalps.").   

  as Debbie]]

The theme of miscegenation also runs through the film. Early on, Martin earns a sour look from Ethan when he admits to being one eighth Cherokee. Ethan says repeatedly that he will kill his niece rather than have her live "with a buck", that "living with the Comanche aint living". Even one of the films gentler characters, Vera Miless Laurie, tells Martin when he explains he must protect his adoptive sister, that "Ethan will put a bullet in her brain. I tell you Martha would want him to."  This outburst made clear that even the supposedly gentler characters held the same fear of miscegenation.   
In a 1964 interview with Cosmopolitan (magazine)|Cosmopolitan magazine, Ford said,
 

 

An important plot undercurrent is the obvious mutual attraction between Ethan Edwards and his brothers wife, Martha. Though no dialog alludes to it, there are a multitude of visual references to their relationship throughout the film.     Some critics have suggested that this unspoken passion implies that Debbie—who is specifically described as eight years old, as Edwards returns from an eight-year absence—may be Ethans daughter. Such a situation would add further layers of nuance to Ethans obsessive search for Debbie, his revulsion at the thought that she might be living as an Indian, and his ultimate decision to bring her home—and then walk away.  Beyond the ostensible motivations, it might depict a guilt-ridden fathers need to save the daughter he made by cuckolding his brother, then abandoned. 

Glenn Frankels 2013 study of the film calls it "the greatest Hollywood film that few people have seen." As cited in   

==Influence==
 Lawrence of Arabia to help him get a sense of how to shoot a landscape.    The entrance of Ethan Edwards in The Searchers, across a vast prairie, is echoed clearly in the across-the-desert entrance of Sherif Ali in Lawrence of Arabia. Sam Peckinpah referenced the aftermath of the massacre and the funeral scene in Major Dundee   and, according to a 1974 review by Jay Cocks, Peckinpahs Bring Me the Head of Alfredo Garcia contains dialogue with "direct tributes to such classics as John Hustons The Treasure of the Sierra Madre and John Fords The Searchers." 

Martin Scorseses Whos That Knocking at My Door features an extended sequence in which the two leading characters discuss the film.  Scorsese has listed the film as one of his all-time favourites. 

Scott McGee, writing for Turner Classic Movies, notes "Steven Spielberg, Martin Scorsese, John Milius, Paul Schrader, Wim Wenders, Jean-Luc Godard, and George Lucas have all been influenced and paid some form of homage to The Searchers in their work."    In a 1959 Cahiers du Cinema essay, Godard compared the movies ending with that of the reuniting of Odysseus with Telemachus in Homers The Odyssey|Odyssey.   In 1963 he ranked The Searchers as the fourth-greatest American movie of the sound era, after Scarface (1932 film)|Scarface (1932), The Great Dictator (1940), and Vertigo (film)|Vertigo (1958). 
 The Searchers.  

Breaking Bad creator Vince Gilligan stated that the ending to the shows final episode, "Felina (Breaking Bad)|Felina", was influenced by the film. 

==See also==
*John Wayne filmography
*List of films considered the best
*John Ford filmography

==References==
 

==Bibliography==
*   
* 

===Primary sources===
* The Searchers: Screenplay, by Frank S Nugent, Alan Le May, John Ford. Published by Warner Bros, 1956.  

==External links==
 
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 