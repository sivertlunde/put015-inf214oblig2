The Blood of Fu Manchu
 
 
 
{{Infobox Film
| name           = The Blood of Fu Manchu
| image          = BloodOfFuManchu.jpg
| image_size     = 175px
| caption        = 
| director       = Jesús Franco
| producer       = Harry Alan Towers
| writer         = Harry Alan Towers
| narrator       =  Tsai Chin Maria Rohm Richard Greene Götz George Daniel White
| cinematography = Manuel Merino Alan Morrison
| distributor    = 
| released       = 23 August 1968
| runtime        = 94 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 British adventure adventure crime film based on the fictional Asian villain Fu Manchu, created by Sax Rohmer. It was the fourth film in a series, and was preceded by The Vengeance of Fu Manchu. The Castle of Fu Manchu followed  in 1969.  It was produced by Harry Alan Towers for Udastex Films. It starred Christopher Lee as Fu Manchu, Richard Greene as Scotland Yard detective Nayland Smith, and Howard Marion-Crawford as Dr. Petrie. The movie was filmed in Spain and Brazil.

==Plot==
In his remote jungle hideout, the evil Fu Manchu has discovered a deadly poison in a "lost city" in the Amazonian jungle that affects only men. Women can become carriers of the "kiss of death" by being bitten by venomous snakes. The poison causes blindness and ultimately followed six weeks later by death. Using mind control, he aims the women at Nayland Smith and other key people with political influence. This prevents them from interfering with his own ambitions to prepare millions of "doses" and spread them around the worlds major cities and capitals in a plan to gain world domination.

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 


 
 