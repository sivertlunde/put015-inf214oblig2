The Secret Life of an American Wife
{{Infobox Film
| name           = The Secret Life of an American Wife
| image          = Secretwife1.JPG
| image_size     = 
| caption        = Theatrical release poster
| director       = George Axelrod
| producer       = George Axelrod
| writer         = George Axelrod
| narrator       =  Patrick ONeal Edy Williams
| music          = Billy May
| cinematography = Leon Shamroy
| editing        = Harry W. Gerstad
| distributor    = 20th Century Fox
| released       = June 25, 1968
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $3,000,000 (US/ Canada) 
| preceded_by    = 
| followed_by    = 
}} 1968 comedy film written and directed by George Axelrod. The film was released by 20th Century Fox in 1968 in film|1968, and was considered a box-office failure.  It features a music score by Billy May.  Edy Williams has a supporting role in the film as the Laytons blonde bombshell neighbor.

==Plot== Patrick ONeal) works as a press agent, and his only client is a movie star who is known as an international sex symbol (Walter Matthau).

Upon hearing that The Movie Star (the character is not given a name, and Matthau is credited as "The Movie Star" in the closing credits) indulges in the services of prostitutes, Victoria decides to pose surreptitiously as one in order to prove to herself that she is still sexually attractive.

==Cast==
*Walter Matthau - The Movie Star
*Anne Jackson - Victoria Layton Patrick ONeal - Tom Layton
*Edy Williams - Susie Steinberg Richard Bull - Howard
*Paul Napier - Herb Steinberg Gary Brown - Jimmy
*Albert Carrier - Jean-Claude

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 