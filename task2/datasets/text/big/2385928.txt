The Break-Up
 
{{Infobox film
| name           = The Break-Up
| image          = Break up.jpg
| caption        = Theatrical release poster
| director       = Peyton Reed
| producer       = Vince Vaughn Scott Stuber
| screenplay     = Jeremy Garelick Jay Lavender
| story          = Vince Vaughn Jeremy Garelick Jay Lavender
| starring       = Vince Vaughn Jennifer Aniston
| music          = Jon Brion
| cinematography = Eric Alan Edwards
| editing        = Dan Lebental David Rosenbloom
| studio         = Universal Pictures Mosaic Media Group Wild West Picture Show Productions
| distributor    = Universal Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $52 million
| gross          = $205 million  
}}
The Break-Up is a 2006 American romantic comedy film directed by Peyton Reed, starring Jennifer Aniston and Vince Vaughn. It was written by Jay Lavender and Jeremy Garelick and produced by Universal Pictures.

==Plot==
Gary Grobowski (Vince Vaughn) and Brooke Meyers (Jennifer Aniston) meet at Wrigley Field during a Chicago Cubs game and begin dating, eventually buying a condominium together. Gary works as a tour guide in a family business with his brothers, Lupus (Cole Hauser) and Dennis (Vincent DOnofrio). Brooke manages an art gallery owned by eccentric artist Marilyn Dean (Judy Davis). Their relationship comes to a head after the latest in an escalating series of, "Why cant you do this one little thing for me?!" arguments. Brooke, feeling unappreciated, criticizes Garys perceived immaturity and unwillingness to work on improving their relationship. Gary is frustrated by Brooke’s perceived controlling, perfectionistic attitude, and expresses his desire to have a little more independence (particularly when arriving home from work, wanting to unwind). Brooke becomes irate when Gary fails to offer to help her clean up after a big dinner party at their home; and, still frustrated from their earlier, unresolved argument, breaks up with him (despite still being in love with him). Brooke seeks relationship advice from her friend Addie (Joey Lauren Adams), while Gary goes to tell his side of things to friend Johnny Ostrofski (Jon Favreau).

Since neither is willing to move out of their condo, they compromise by living as roommates; but, each begins acting out to provoke the other in increasingly elaborate ways. Gary buys a pool table, litters the condo with food and trash, and even has a strip poker party with Lupus and a few women. Meanwhile, Brooke has Gary kicked off their "couples-only" bowling team, and starts dating other men in an attempt to make Gary jealous. When their friend and realtor Mark Riggleman (Jason Bateman) sells the condo, Gary and Brooke are given two weeks notice to move out. Brooke invites Gary to an Old 97s concert, hoping that he will figure out that the gesture is meant to be her last-ditch attempt to salvage their relationship. Gary agrees to meet her there, but misses the hidden agenda, and misses the concert—unwittingly breaking Brookes heart. When Gary goes out for a drink with Johnny, his friend points out that Gary has always had his guard up, has been guilty of a lot of selfishness, and never gave Brooke a chance, emotional intimacy
-wise.

Afterwards, Brooke quits her job in order to spend time traveling Europe. When she brings a customer from the art gallery home one evening, Brooke finds the condo cleaned and Gary preparing a fancy dinner to win her back.  He lays his heart on the line and promises to appreciate her more.  Brooke begins crying and states that she just can not give anymore and, therefore, does not feel the same way.  Gary seems to understand and kisses her before leaving. It is later revealed that Brookes "date" (who initially asked her out, but she politely rejected) was actually a client interested in a piece of artwork she kept at the condo. 
Both eventually move out of the condo.  Gary begins taking a more active role in his tour guide business, while Brooke travels the world, eventually returning to Chicago. Some time later, they meet again by chance on the street as Gary is bringing home groceries and Brooke is on her way to a meeting. After some awkward but friendly catching up, they part ways but each glances back over their shoulder and they share a smile.

==Cast==
 
* Vince Vaughn as Gary Grobowski
* Jennifer Aniston as Brooke Meyers
* Joey Lauren Adams as Addie Jones
* Cole Hauser as Lupus Grobowski, Garys brother
* Jon Favreau as Johnny Ostrofski
* Jason Bateman as Mark Riggleman
* Judy Davis as Marilyn Dean
* Justin Long as Christopher Hirons
* John Michael Higgins as Richard Meyers, Brookes brother
* Vernon Vaughn as Howard Meyers, Brookes father
* Ann-Margret|Ann-Margret Olsson as Wendy Meyers, Brookes mom
* Vincent DOnofrio as Dennis Grobowski, Garys other brother
* Peter Billingsley as Andrew
* Mary-Pat Green as Mischa
* Keir ODonnell as Paul Grant
* Geoff Stults as Mike Lawrence
* Zack Shada as Mad Dawg Killa (voice)
 

==Reception   ==
===Box office===
The romance/comedy film grossed over $205 million worldwide, with a total of $118.7 million at the American box office.

===Critical response===
The film received mixed reviews from critics. On Rotten Tomatoes, the film has a rating of 33%, based on 189 reviews, with an average rating of 5/10. The sites critical consensus reads, "This anti-romantic comedy lacks both laughs and insight, resulting in an odd and unsatisfying experience."  On Metacritic, the film has a score of 45 out of 100, based on 37 critics, indicating "mixed or average reviews". 

Film critic Rick Green of Globe and Mail said, "Although possessed of a laudable desire not to be yet another run-of-the-mill, wacky-impediment -- damned if the picture can figure out how to be an anti-romance comedy." 

 

==Music==
* The first song selection, coupled with snapshots of the then-happy couple, was Queen (band)|Queens "Youre My Best Friend (Queen song)|Youre My Best Friend". Also featured in the film are the songs "Story of My Life" by Social Distortion, "Boogie Nights" by Heatwave (band)|Heatwave, "You Oughta Know" by Alanis Morissette, "Timebomb," "Salome," and "Melt Show" by the Old 97s, Queen (band)|Queens "Crazy Little Thing Called Love" sung by Dwight Yoakam, and Dorival Caymmis "Lá vem a Baiana" sung by Jussara Silveira. The closing song, which may portend the future of their relationship, is Johnny Nashs "I Can See Clearly Now". In the trailer is another song by Social Distortion, "Ball and Chain". Yes at the family dinner. Riviera Theater.
* The song "Timebomb" by the Old 97s was also featured in the movie Clay Pigeons, which also starred Vince Vaughn.
*   is the name of an actual a cappella group in Washington, D.C., who are thanked in the credits.

==Home media==
The film was released on DVD on October 17, 2006. It has grossed $51 million in the US from DVD/home video rentals. It was later released on Blu-ray on June 3, 2014.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 