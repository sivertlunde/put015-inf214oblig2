A Portrait of the Artist as a Young Man (1977 film)
 
 
{{Infobox film
| name           = A Portrait of the Artist as a Young Man
| image          = A Portrait of the Artist (1977 film) title frame.jpg
| image_size     = 250
| border         = 
| alt            = 
| caption        = Opening film credit
| director       = Joseph Strick
| producer       = {{Plainlist |
* Betty Botley
* Richard Hallinan }}
| writer         = 
| screenplay     = Judith Rascoe
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist |
* Bosco Hogan
* T. P. McKenna
* John Gielgud }}
| music          = Stanley Myers
| cinematography = Stuart Hetherington
| editing        = Lesley Walker
| studio         = Ulysses Film Production Ltd.
| distributor    = 
| released       =  
| runtime        = 92 minutes Ireland
| language       = English
| budget         = 
| gross          = 
}}
  same name, directed by Joseph Strick.  It portrays the growth of consciousness of Joyces semi-autobiographical character, Stephen Dedalus, as a boy and later as a university student in late nineteenth century Dublin.

==Cast==
 
 
 
* Bosco Hogan – Stephen Dedalus
* T. P. McKenna – Simon Dedalus
* John Gielgud – The Preacher
* Rosaleen Linehan – Mary (May) Dedalus
* Maureen Potter – Mrs. Dante Riordan
* Niall Buggy – Davin Bryan Murray – Lynch
* Desmond Cave – Cranly
* Leslie Lalor – Milly
* Desmond Perry – John Casey
* Susan Fitzgerald – Emma Daniels
* Luke Johnston – Stephen Dedalus, age ten
* Danny Figgis – Wells
* Cecil Sheehan – Uncle Charles
* Edward Golden – Father Conmee
* Bill Foley – Confessor David Kelly – Dean of Studies Edward Byrne – Teacher
* Emmet Bergin – Father Dolan
* Aiden Grennell – Father Arnall
* Danny Cummins – Drinker Chris Curran – Auctioneer

::and

* Brendan Cauldwell – Father Michael Eamon Kelly
* Anna Manahan
* Maureen Toal
* Jacinta Martin
* Chris ONeill
* Brenda Doyle
* Deirdre Donnelly
 
;Children
 
* Dominic Burdick
* Gray Burdick
* Linus Burdick
* Ashling Burdick
* Lucy Burdick
* Katy Burdick
* Tiernan Quinn
* Sean Pilkington
* Kenneth Joyce
* Joan Hayes
* John Hayes
* James Lennon
* Ronan Donelan
* Ian Branagan
* Terence Strick – Stephen Dedalus, age three (uncredited)
* Helen Strick
 
 

==References==
 

==Further reading==
*  

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 

 