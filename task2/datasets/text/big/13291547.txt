Alles is Liefde
 
{{Infobox film
| name           = Alles is Liefde
| image          = Alles is Liefde.jpg
| caption        = Original film poster for Alles is Liefde
| director       = Joram Lürsen
| producer       = Jeroen Beker Frans van Gestel
| writer         = Kim van Kooten
| starring       = Carice van Houten Paul de Leeuw Wendy van Dijk Daan Schuurmans Chantal Janzen
| music          = Melcher Meirmans Merlijn Snitker Chrisnanne Wiegel
| cinematography = Lex Brand
| editing        = Peter Alderliesten
| distributor    = A-Film
| released       =  
| runtime        = 110 minutes
| country        = Netherlands
| language       = Dutch
| budget         =
| gross          = $14,724,768 
}}
Alles is Liefde ( ) is a 2007 Dutch romantic comedy film directed by Joram Lürsen, and starring Carice van Houten, Paul de Leeuw, Wendy van Dijk, and Daan Schuurmans.

==Plot==
The actor who annually plays Sinterklaas dies just before the arrival of television crew. A mysterious new Sinterklaas named Jan (Michiel Romeyn) replaces him. Just a short while after his public appearance, he jumps into water to save a little girl who falls into it, and then he runs away from the crowd and disappears. He is found by the production assistant of the television show next day, and agrees to appear in television shows on knowing that playing Sinterklaas is a paid job. He becomes quite popular on television because of his alternative style.

Klaasje (Wendy van Dijk) has left her husband Dennis (Peter Paul Muller), because he cheated on her with a young and attractive elementary school teacher (Chantal Janzen). Dennis wants her back, but his chances seem to plummet when she has a fling with a 16-year-old boy (Valerio Zeno), whom she meets during the funeral of her father, the deceased actor who used to play Sinterklaas annually.
Klaasje’s best friend Simone (Anneke Blok), the mother of the girl rescued by Sinterklaas, is the linchpin in her family. Her husband Ted (Thomas Acda) often feels redundant. He loses his job, but he is afraid to tell his dominant wife about it.

Swimming instructor Victor (Paul de Leeuw) looks forward to marrying his love Kees (Daan Schuurmans), an undertaker. But Kees has doubts and fears about committed life. During the marriage ceremony, he walks away without saying ‘yes’.

Victors sister Kiki (Carice van Houten), a saleswoman at the jewellery section of De Bijenkorf, has always dreamt of a Prince Charming. During the arrival of Sinterklaas, Kiki almost runs into Crown Prince Valentine (Jeroen Spitzenberger), while driving a horse carriage. Kiki is dressed as a campaign gift and the prince takes her in his arms as she tries to get down from the carriage. Prince Valentine falls in love with her. Next morning he visits De Bijenkorf to see Kiki. But Kiki desists from seeing him, and rejects him on the grounds that he will not take her seriously. To still come into contact with her, Prince Valentine dresses as Black Peter, does antics at De Bijenkorf for work. In the evening, he (dressed as Black Pieter) offers to drop her home and ends up having a wild night with her. The next morning, however, he sneaks away through the window without saying goodbye to Kiki. Kiki follows him to his hotel and confronts him. It turns out that Kiki knew all along that Prince Valentine was playing Black Pieter but just wanted to see how far he would go for her. They end up kissing each other.

On the night of Sinterklaas Eve, the Jan again runs away before his appearance in the show. He is spotted by Ted who recognizes him to be the one who had rescued his daughter from water. Ted invites Jan, dressed as Sinterklaas, for a beer to his house. All the children of the town slowly flock around him as word secretively spreads among them that he is there. The television crew also eventually comes there. During his TV interview, he speaks candidly about his life and his regrets. He also tells that the name of his son is Kees and that he left his son while his son was three. The 5 December Special TV show comes to an end and Sinterklaas is seen walking out of on the snowy street. As he walks and looks around, all the couples are shown one by one to have found out what to do with their lives. Having found his father on the television show, Kees drives with Victor to meet him. They come across him in the middle of the road as they were driving. The movie ends with Jan, dressed as Sinterklaas, embracing Kees. 

==Background==
The story is inspired by the film Love Actually (2003). The budget of Alles is Liefde was euro|€ 4 million. 

==Cast==
*Carice van Houten as Kiki Jollema
*Paul de Leeuw as Victor Jollema
*Michiel Romeyn as Sinterklaas Jan
*Wendy van Dijk as Klaasje van Ophorst
*Thomas Acda as Ted Coelman
*Daan Schuurmans as Kees Tromp
*Anneke Blok as Simone Coelman
*Peter Paul Muller as Dennis
*Chantal Janzen as Sarah
*Jeroen Spitzenberger as Prince Valentijn
*Marc-Marie Huijbregts as Rudolf
*Valerio Zeno as Daniël
==Box office==
Alles is Liefde had a box office result of euro|€9.1 million in 2007. {{cite news
  | title = Nederlandse film ongekend populair in 2007 (+video)
  | work =  
  | publisher = NRC Handelsblad
  | date = 2008-01-10
  | url = http://www.nrc.nl/kunst/article888747.ece
  | accessdate = 2008-01-11 |language=nl}}  Within two months after its release it received a Golden Film for  , {{cite web
  | title = Met 100.000 bezoekers nu al Gouden Film voor Alles is Liefde
  | work =  
  | publisher = Netherlands Film Festival
  | date = 2007-10-15
  | url = http://www.filmfestival.nl/index.php?id=908&tx_ttnews%5Btt_news%5D=266&tx_ttnews%5BbackPid%5D=700&cHash=2c95a32039 archiveurl = archivedate = 2007-10-27|language=nl}} 
a Platinum Film for  , {{cite web
  | title = Alles is Liefde in recordtempo Platina Film
  | work =  
  | publisher = Netherlands Film Festival
  | date = 2007-10-29
  | url = http://www.filmfestival.nl/index.php?id=908&tx_ttnews%5Btt_news%5D=270&tx_ttnews%5BbackPid%5D=700&cHash=7c1d0490aa archiveurl = archivedate = 2007-11-12|language=nl}} 
and a Diamond Film for  , {{cite web
  | title = Alles is Liefde behaalt in recordtempo één miljoen bezoekers
  | work =  
  | publisher = Netherlands Film Festival
  | date = 2007-12-07
  | url = http://www.filmfestival.nl/index.php?id=908&tx_ttnews%5Btt_news%5D=272&tx_ttnews%5BbackPid%5D=700&cHash=4c20983c71 archiveurl = archivedate = 2007-12-20|language=nl}}  all in the Netherlands.

In 2007, it was the most viewed Dutch film in cinemas, and over all the third most viewed film in Dutch cinemas, coming after  . 

Alles is Liefde is the most visited Dutch romantic comedy film ever, and is in the top 20 of most visited Dutch films of all time. 

==References==
 

==External links==
*    
*  

 
 
 
 
 
 