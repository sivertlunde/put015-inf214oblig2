Aviya's Summer
{{Infobox film
| name           = Aviyas Summer
| image          = 
| image size     = 
| caption        =  Eli Cohen
| producer       = Gila Almagor Eitan Evan
| writer         = Gila Almagor Eli Cohen
| starring       = Gila Almagor Kaipo Cohen
| music          = 
| cinematography = David Gurfinkel
| editing        = Tova Neeman
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Israel Hebrew Polish Polish
| budget         = 
}}

Aviyas Summer  ( ; Transliteration|translit.&nbsp;HaKayitz Shel Aviya) is a Hebrew-language book that became a bestseller. The 1985 autobiographical novel by theater actress Gila Almagor was made into a film released in 1988. The 96-minute film acts as a memoir of Almagors childhood and provides insights into Israeli society in the early post-state period. The film was shot on location in Kfar Avraham, Petach Tikvah, Israel. 

==Plot== partisan fighter, yet now she is constantly mocked by native Israelis for her erratic behavior. She walks the thin line between sanity and madness, attempting to forge a life for herself and her daughter in the new realities of Israel.

Aviya is a bright girl with a vivid imagination, yet she is mocked by her peers. Her relationship with her mother is complex, at times affectionate, but also fragile. Aviya fantasizes that if she could only find her father, all her/her mothers problems would cease and her family would be whole again. Aviyas wild imagination regarding her quest to find her father leads to the climax of the film. The film ends with Aviya coming to terms with the realities of her life and reaching a maturity beyond her years.

==Awards==
The film has been the recipient of many Israeli prizes as well as various international awards, including the Silver Bear Award from the 39th Berlin International Film Festival,    Best Foreign Film – San Remo Festival, and Best Director/Best Actress – Silver Menorah Award. 

==Cast==
* Gila Almagor – Henya Aleksandrowicz
* Kaipo Cohen – Aviya Aleksandrowicz
* Marina Rosetti – Esther/Helena "Lena" Gantz Eli Cohen – Max Gantz
* Avital Dicker – Maya Abramson

==Sequel==
 

==References==
 

==External links==
*  

 
 
 
 
 
 
 