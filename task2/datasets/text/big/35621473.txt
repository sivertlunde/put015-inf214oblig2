Benji (2012 film)
{{Infobox film
| name           = Benji
| image          = Benji-film-poster.jpg
| image_size     = 200px
| caption        = 
| director       = Coodie and Chike
| producer       = 
| writer         = 
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} South Side Ben Wilson who played for Simeon Career Academy. The film debuted at the Tribeca/ESPN Sports Film Festival on April 20, 2012.   The Tribeca viewing was a world premier.  

==Debut==
The film was presented three times on three different screens at the film festival: April 20 at 6:00PM (BMCC Tribeca PAC), April 21 at 3:30PM (SVA Theater 1 Silas) and April 28 at 9:30PM (Tribeca Cinemas Theater 1).   

==Background== Anthony Davis, Coodie and Beverly Community community area at the time of the shooting of Wilson, which was the 669th death in Chicago that year. 

Wilson was shot and killed following an altercation with other youths near Simeon.  The wake and funeral served as a gathering for thousands of Chicagoans to face their grief.  Subsequent city homicide rates declined due to public awareness, renewed activism, and legislation regarding improved emergency response.  Wilson continues to represent the dream of Chicago youth to achieve the ultimate athletic excellence by becoming the number one player in the nation.  Derrick Rose wore Wilsons number 25 when he played for Simeon.  Parker had the number 25 stitched into the team sneakers during his time at Simeon. 

==Plot==
This film documents the November 1984 death of Wilson, who was regarded as the best basketball player in the country, but was shot the day before his senior season of high school basketball was to begin and died the next morning. The film documents the cultural ripple effects of his life and death. It documents the contemporaneous impact of the death on those closest to Wilson, such as Anderson, and the long-run influence of his death on Chicagos youth, such as Parker. 
 Common and singer R. Kelly, both of whom grew up with Wilson; WMAQ-TVs Warner Saunders, who was one of the first to receive news of the shooting; Kenneth Malatesta, the Cook County prosecutor who tried the case against Moore; and Isaiah Gant, Moores attorney. Jetun Rush, Wilsons girlfriend and mother of his child, was contacted but declined to take part.

==Critical review==
Neil Best of Newsday stated that the movie "packs the emotional punch you would expect of a story about the death of a high school basketball star that rocked Chicago in 1984" and that the directors "capture the sad story of Ben Wilson through effective use of archival video and compelling interviews".   ESPN HS/ESPN.com writer Lucas ONeill  states that the directors "...assembled an impressive collection of friends, family members, writers and celebrities to tell Wilson’s story."  ONeill noted that among the critical junctures of the film were Andersons receipt of the phone call with news of the death and Parkers appearance in the film as almost a new incarnation of Wilson.  Mina Hochberg of Outside (magazine)|Outside describes the story as both inspiring and haunting and describes the documentary as "...too rhapsodic—Wilson’s legacy could easily have been preserved without so much mythologizing. In the end, though, it effectively captures that wistful sense of what-if..."  IndieWire states that the film is an embodiment of the spirit of the 30 for 30 series formerly produced by ESPN.    IndieWire also notes that "...the doc inadvertently takes some out of the sting of his death. Chasing the murderer unmoors the documentary as well...While the doc tends to wander...its hard to fault the filmmakers for closing all the loops. It does make for a tale not quite as uplifting and satisfying as it seems to be at first, but it’s still a honest, striking and emotionally stirring chronicle of a promising life cut down way too soon."  

==References==
 

 
 
 
 
 