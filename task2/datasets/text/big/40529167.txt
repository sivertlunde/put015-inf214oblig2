Oru Indian Pranayakadha
{{Infobox film
| name = Oru Indian Pranayakadha
| image = Oru Indian Pranayakatha.jpg
| caption = Promotional poster
| director = Sathyan Anthikad
| producer = Central Pictures
| writer = Iqbal_Kuttippuram|Dr. Iqbal Kuttippuram
| starring = Fahadh Faasil Amala Paul Vidyasagar
| cinematography = Pradeep Nair
| editing = K.Rajagopal
| studio = Central Pictures
| distributor = Central Pictures
| online media partner = PrimeGlitz Media
| released =  
| runtime = 
| country = India Malayalam
| budget   = 
| gross    =  
}}
Oru Indian Pranayakadha (English: An Indian Love Story) is a 2013 Malayalam romantic comedy film written by Ikbal Kuttipuram and directed by Sathyan Anthikad. The film stars Fahadh Faasil and Amala Paul in the lead roles. The film was a sleeper hit despite competition from the Mohanlal starrer Drishyam.

==Plot==
Aymanam Sidharthan (Fahadh Faasil) is the local youth wing leader of a political outfit. Protégé of the district president of the party, Uthup Vallikkadan (Innocent (actor)|Innocent), Sidharthan hopes to make it big in politics. However, his dreams are shattered when the party High Command decides to nominate the daughter of one of the partys senior leaders daughter (Muthumani). This leaves the young politico in despair. It is during this time that a Canadian citizen of Malayali origin named Irene Gardner (Amala Paul) visits Kerala to shoot a documentary. She is in search of an assistant to help her shoot the film and approaches Uthup. Uthup requests Sidharthan to meet her and assist her. Irene promises  2000 per day for the assistant, so Siddharthan, who has financial issues, agrees to help her. Irene then is called by the police for a passport verification. Sidharthan begins to doubt her motives for coming to India, but upon confronting Irene, Sidharthan comes to know that Irene has actually come in search of her birth parents. Sidharthan agrees to help her; they manage to find out who Irenes parents are and that she is their illegitimate daughter. Irene decides to meet her parents without letting them know that she is their daughter. She finds out her birth mother is an ayurvedic doctor and lives at her mothers hospital as a patient for some time. Meanwhile, Sidharthan begins to fall in love with Irene, but neither is aware of this at present. They then discover her birth fathers identity and go to Rajasthan to meet him. During their stay, they participate in Irenes half-sisters wedding and then return to Kerala. Irene then decides to go back to Canada, which Sidharthan does not agree with, as they have realized they are in love with each other. Two years later, Sidharthan is shown as a successful politician. Irene comes back to India and both of them reunite.

== Cast & Crew ==
=== Cast ===
* Fahadh Faasil as Aymanam Sidharthan
* Amala Paul as Irene Gardner Innocent as Uthup Vallikkadan
* Lakshmi Gopalaswamy as Dr. Thulasi
* Prakash Bare as Azad
* Shafna as Divya
* Muthumani  as Dr. Vimala Ramanathan
* Krishnaprabha as Sudha 
* Gopalan as Shivaraman
* Riya Saira as Merin
* Valsala Menon as Sidharths grandmother
* Babu Anoor as Sethumadhavan, Sidharths Father
* Kala as Sidharths Mother
* Nisha Sarang as Sivaramans wife
* Anjana Appukuttan as Irenes neighbour
* Ancy as Divyas mother
* Antony as Divyas father
* Janaki as Swathi
* Raj Pranav as Village boy
* Arun Nair as Roopesh
* Dilip as Benny
* Neeraj Madhav as Charlie
* Thiruvalla Bhasi as Varkky Chettan
* M. G. Sasi as Dr. Sunil Kumar

=== Dubbing Artists ===
* Bhagyalakshmi (Dubbed for Lakshmi Gopalaswamy)
* Angel
* Gracy
* Praveen

=== Make Up ===
* Saji
* Jayaraman
* Ajitha Babu (Hair)
* Ansari (Hair + Amala Paul)

=== Costume ===
* Mohan Kozhikkode (Associate)
* Manoj
* Gopalakrishnan
* Antony
* Shukoor (Fahadh Faasil)

=== Producers ===
* Rajesh Thiruvallom
* Babu Mannarkadu
* Renju
* James

=== Director ===
* Sathyan Anthikad

==Production==
For this film, Fahadh Faasil worked with Sathyan Anthikad for the first time. Unlike his previous movies, Sathyan Anthikad announced the name of the movie along with the beginning of the shoot. The film was shot in and around Kottayam in Kerala  and Jaisalmer in Rajasthan. 

==Music==
 Vidyasagar composed the music for the film; he was associating with director Sathyan Anthikad for the first time, replacing maestro Ilaiyaraaja who had composed the music score for 10 of the last 12 movies directed by Sathyan Anthikad. The film had four songs, all of which became reasonably popular upon their release in early December 2013. 

===Track listing===
{{Infobox album 
| Name = Oru Indian Pranayakadha Vidyasagar
| Type = Soundtrack
| Language = Malayalam
| Label = Mathrbhumi Music
}}

{{tracklist
| headline     =
| extra_column = Singer(s)
| total_length =
| title1       = Saajan
| extra1       = Shweta Mohan, Mansi, Harish & Chorus
| title2       = Omana Poove
| extra2       = Najim Arshad, Abhirami Ajay
| title3       = Valudekkanum
| extra3       = G. Sriram
| title4       = Shyamameghame
| extra4       = K. S. Chithra
}}

==Reception==
The film received mixed and positive reviews from both critics and movie-watchers. It grossed  15.70 crore in 75 days, thereby becoming the third highest grossing film in Fahadh Faasils film career after Bangalore Days and Iyobinte Pusthakam. 

==References==
 

==External links==
*  

 
 
 
 
 
 