1915 (film)
{{Infobox film
| name           = 1915
| image          = 1915 (film) poster.jpg
| caption        = Theatrical release poster
| director       = Garin Hovannisian Alec Mouhibian
| producer       = Garin Hovannisian Alec Mouhibian Terry Leonard
| writer         = Garin Hovannisian Alec Mouhibian
| starring       = Simon Abkarian Angela Sarafyan Samuel Page Nikolai Kinski
| music          = Serj Tankian
| released       =  
| country        = United States
| language       = English
| budget         =
| gross          =
}}

1915 is an upcoming psychological thriller film co-written and directed by Garin Hovannisian and Alec Mouhibian. It is produced by Hovannisian and Mouhibian with Terry Leonard, and stars Simon Abkarian, Angela Sarafyan, Samuel Page, and Nikolai Kinski. 1915 follows a mysterious theater director in present day Los Angeles as he tries to bring the ghosts of a forgotten genocide back to life. The film refers to the Armenian Genocide, which is denied by the Turkish government to this day.  

== Plot ==
Exactly a century after the Armenian Genocide committed in Ottoman Turkey, a mysterious director (Simon Abkarian) is staging a play at the Los Angeles Theatre to honor the victims of that tragedy. Because of the controversial theme of the performance, protesters surround the theater and series of strange accidents spread panic among his producer (Jim Piddock) and actors (Angela Sarafyan, Sam Page, Nikolai Kinski), it appears that the director’s mission is profoundly dangerous, and the ghosts of the past are everywhere. 

==Themes==
The film explores the theme of denial, referring not only to the 100-year denial of the Armenian Genocide by Ottoman Turkey, but also individual denial, which is observed in all characters of the movie. 

== Production ==
1915 is the first feature film by Garin Hovannisian and Alec Mouhibian. Hovannisian is the internationally acclaimed author of Family of Shadows  and has written for the Los Angeles Times, The New York Times, and other publications. Mouhibian is a writer and comedian whose work has appeared in Slate, The Weekly Standard, and a variety of other publications. He has also been a Media Fellow of the Hoover Institution at Stanford University. 

Hovannisian and Mouhibian have been collaborating on film and literary projects for more than ten years. 

The film has been produced by Bloodvine Media, in conjunction with Strongman and mTuckman Media.   The film has been publicized by iNexxus Digital Marketing  Agency and PRodogy Public Relations company.

===Cast===

* Simon Abkarian as Simon
* Angela Sarafyan as Angela
* Samuel Page as James
* Nikolai Kinski as Tony
* Jim Piddock as Jeffrey
* Sunny Suljic as Gabriel
* Debra Christofferson as Lillian

===Music===
Original score to the movie was composed by a Grammy-award winning composer Serj Tankian, front-man of popular rock band System of a Down. 

== Release ==
The European release will take place in Maxim Gorki Theatre in Berlin, Germany on April 5th. The official release of 1915 in the US will take place on April 17th. The Armenian premiere is scheduled on April 25th.

==See also==
* 1915 in film

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 

 