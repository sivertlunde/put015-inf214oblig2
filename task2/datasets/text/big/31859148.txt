The Mysterious Mr. Nicholson
 
 
The Mysterious Mr. Nicholson is a 1947 British crime film directed by Oswald Mitchell and starring Anthony Hulme, Lesley Osmond and Frank Hawkins.  The beneficiary of a lucrative inheritance is found dead by a solicitors clerk.

==Synopsis==
When a will messenger visits the house of a beneficiary to deliver the news of the large sum that he is to inherit, he finds the beneficiary murdered in his home. The messenger sets out on a private hunt for the murderer, and ultimately attempts to find evidence proving the innocence of a burglar who looks like the killer and had entered the building before the murder.  

==Cast==
* Anthony Hulme - Nicholson / Raeburn
* Lesley Osmond - Peggy Dundas
* Frank Hawkins - Inspector Morley
* Andrew Laurence - Waring
* Douglas Stewart - Seymour
* George Bishop - Mr Browne
* Josie Bradley - Freda
* Ivy Collins - Mrs Barnes

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 