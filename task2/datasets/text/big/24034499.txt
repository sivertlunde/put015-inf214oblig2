Mr. Wise Guy
{{Infobox film
| name           = Mr. Wise Guy
| image size     =
| image	         = Mr. Wise Guy FilmPoster.jpeg
| caption        =
| director       = William Nigh Barney A. Sarecky (associate producer) Jack Dietz (producer)
| story          = Martin Mooney
| screenplay     = Sam Robins Harvey Gates Jack Henley
| narrator       =
| starring       = East Side Kids
| music          =
| cinematography = Arthur Reed
| editing        = Carl Pierson
| distributor    = Monogram Pictures
| released       =  
| runtime        = 70 minutes 58 minutes (American2005 DVD release)
| country        = United States
| language       = English
}}

Mr. Wise Guy is a 1942 American film starring The East Side Kids and directed by William Nigh.

==Plot== Bill Lawrence), and Peewee (David Gorcey), are falsely arrested on the wharf because the truck they are playing in was stolen. They are remanded to Wilton Reform School, where Muggs, the wise-cracking leader of the gang, is dubbed "Mr. Wise Guy" by the brutal guard Jed Miller (Dick Ryan). 

Jim Barnes (Jack Mulhall), the new warden, reassures Dannys older brother Bill (Douglas Fowley), who has bad memories of the school from when he served as a guard there, that his testimony describing the places cruelty eventually resulted in the dismissal of the former warden and the adoption of gentler rules. Bill is given a tour of the school by Barness secretary, Ann Mitchell (Joan Barclay), and later takes her out to dinner. 
 Guinn Williams) robs the place and murders the clerk. Manning takes Bill hostage in his car and forces him to lead the police on a chase. Manning escapes when Bill crashes the car, and Bill is later convicted of robbery and murder and sentenced to execution. 

In the reform school, the boys have been battling with two toughs, "Rice Pudding" Charlie(Gabriel Dell) and Chalky Jones (Bobby Stone), but when Barnes witnesses Miller encouraging a fistfight, he demands Millers resignation. Chalky tries to get the kids in trouble by informing Barnes of their plans to run away, but in an effort to establish a code of honor, Barnes punishes Chalky for being an informer. 

When Muggs and his pals see newsreel footage of a man and woman accepting the winnings from a lottery, they recognize the man as Knobby (Billy Gilbert), the driver of the stolen truck. They link Knobby to Manning based on information given to them by Charlie, who is Mannings nephew. Armed with information that could prove Bills innocence, the boys escape from the reform school and go to the apartment of Dorothy Melton (Ann Doran), the woman from the newsreel. The kids hold the pair, who had been planning to leave town with the lottery money which actually belongs to Manning, who was afraid of being seen. Manning appears at Dorothys apartment to demand his money and hits Dorothy for double-crossing him with Knobby. Before the situation can worsen, the police arrive and arrest the criminals. Bill gets a reprieve from the governor, and Ann and the boys see him off as he reports for active military duty.

==Production== Dead End Kids and Little Tough Guys series. Unlike the Dead End Kids films, in most of Dells East Side Kids films, he portrayed a villain, rather than a member of the gang.

==Cast and Characters==
===The East Side Kids===
*Leo Gorcey as Ethelbert Muggs McGinnis
*Bobby Jordan as Danny Collins
*Huntz Hall as Glimpy Stone
*Ernest Morrison as Scruno (uncredited)
*David Gorcey as Pewee Bill Lawrence as Skinny

===Additional cast===
*Gabriel Dell as Charlie Manning
*Bobby Stone as Chalky Jones Sidney Miller as Charlie Horse
*Billy Gilbert as Knobby
*Guinn Big Boy Williams as Luke Manning
*Douglas Fowley as Bill Collins
*Joan Barclay as Ann Mitchell
*Warren Hymer as Dratler
*Ann Doran as Dorothy Melton
*Jack Mulhall as Jim Barnes
*Benny Rubin as Second Waiter

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 