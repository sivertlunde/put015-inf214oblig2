Hello Pop!
{{Infobox film
| name           = Hello Pop!
| image          = HelloPop.jpg
| caption        =  Jack Cummings
| writer         = Ted Healy Matty Brooks Moe Howard Albertina Rasch Dancers Henry Armetta Edward Brophy Tiny Sandford Rosetta Duncan Vivian Duncan
| music          =   Dimitri Tiomkin Al Goodhart Dave Dreyer
| cinematography =
| editing        =
| producer       = MGM
| released       =  
| runtime        = 17 43"
| country        = United States
| language       = English
}} His Stooges Albertina Rasch lost until a 35mm nitrate print was discovered in Australia in January 2013. 

==Plot==
A theater producer (Healy) is trying to stage an elaborate musical revue. His efforts are constantly interrupted by demanding back stage personalities: a flaky musician (Henry Armetta), a woman who keeps try to ask him something (Bonnie Bonnell), and his raucous sons (the Stooges in childrens costumes). 

He is able to get the show ready for presentation, but during the main number, the Three Stooges slip beneath the enormous hoopskirt costume worn by the leading vocalist. They emerge on stage during the performance, ruining the show. 

==Cast==
*Ted Healy as Father
*Moe Howard as Son
*Larry Fine as Son
*Curly Howard as Son
*Bonnie Bonnell as Bonnie
*Henry Armetta as Italian Musician
*Edward Brophy as Brophy
*Rosetta Duncan as Singer/Dancer
*Vivian Duncan as Singer/Dancer The Albertina Rasch Girls as Themselves
*Tiny Sandford as Strongman

==Production==
Originally planned under the title Back Stage, Hello Pop! was the third of five short films made by MGM featuring the vaudeville act billed as “Ted Healy and His Stooges.” The act focused primarily on Healy’s wit and caustic commentary, with the Stooges receiving the brunt of the physical slapstick. For the MGM short films, actress Bonnie Bonnell was incorporated into the configuration as Healy’s love interest. 
 The March of Time (1930). 

==Lost film== fire in 1967. 

In January 2013, it was announced that Hello Pop! had been located in an Australian private film collection and was in the process of being restored for public viewing.   Sydney Morning Herald , 29 September 2013. Retrieved 1 October 2013  The film was screened at Film Forum in New York City on 30 September 2013. 

==DVD Release== The Big Idea (1934). 

==References==
 

==See also==
*The Three Stooges filmography

==External links==
* 

 

 
 
 
 
 
 
 