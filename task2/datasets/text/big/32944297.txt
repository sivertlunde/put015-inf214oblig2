New York City Serenade (film)
{{Infobox film
| name           = New York City Serenade
| image          = 
| image_size     = 
| caption        = 
| director       = Frank Whaley
| producer       = Laird Adamson James Koya Jones
| screenplay     = Frank Whaley
| based on       =  Chris Klein Jamie-Lynn Sigler
| music          = Ed Harcourt
| cinematography = Ryan Samul
| editing        = Miran Miosic
| studio         = IKM Productions
| distributor    = Archer Entertainment
| released       = September 13, 2007 (Toronto Film Festival) August 15, 2008 (United Kingdom|U.K.)
| runtime        = 103 minutes
| country        = United States English
| gross = 
}}
 Chris Klein and Jamie-Lynn Sigler. It made its debut at the 2007 Toronto International Film Festival. The film takes it title from the Bruce Springsteen song of the same name from 1973.

==Plot==
Owen is an aspiring filmmaker who has been nominated for an award for one of his short films in which his friend Ray appears. Owen has a job developing photos in the film industry. Ray has an office job (this week) but still plays drums in a rock band, and he has a young daughter, Francie, as well as a drinking problem. Owen is engaged to Lynn.

Owen and Lynn go out to a film, where they meet Lynns French literature professor Noam. Then they catch the end of one of the performances of Rays band. Owen wants to continue the date with Lynn afterward, but Ray persuades Owen by asking for help moving his drums. Ray and Owen then wind up going to Bertrands party where Owen and Rachel end up in bed. 

Owen, Ray, and two of their friends are asked to serve as pallbearers when the father of their friend Matt dies in New Jersey. While Owen is out of town, Lynn and Rachel meet, and Lynn learns the truth about what Owen and Rachel did. Lynn wants to break up with Owen.

Owen takes Ray to the film festival where he hopes to win an award for his short film. When they arrive, their driver Les is supposed to take them to the airport motel, but Ray saw Wallace Shawn and found out he was staying at the Four Seasons. Ray and Owen go to the Four Seasons and Ray overhears that Shawns son will be late, so Ray claims to be Shawns son, and gets himself and Owen a nice room. At the festival, Owen does not win anything. After returning to the hotel, their scheme has been discovered and they are kicked out. Owen repeatedly calls Lynn trying to make up with her, but she never answers and the two are never again shown together. In Lynns last scene, she is getting ready to go on a date with Noam.

At the end of the movie, some time has passed, Ray has straightened his life out, he has a good job, and he has a good relationship with his daughter. Owen has produced a successful television commercial.

==Cast==
*Freddie Prinze Jr. as Owen Chris Klein as Ray
*Jamie-Lynn Sigler as Lynn
*Ben Schwartz as Russ
*Christopher DeBlasio as Ben
*Sebastian Roché as Noam Broder
*Wallace Shawn as himself
*Frank Whaley as Les
*Emma Bell as Melinda
*Diana Gettinger as Rachel
*Jeff Skowron as Matt

==Reception==
Kyle Smith of the New York Post called the film "one of those pointless indies that youll have forgotten before the credits roll" and criticized Frank Whaleys "staggering lack of insight, imagination and wit".  Nathan Lee of The New York Times called the film "transparently banal". 

==References==
 

==External links==
* 

 
 
 
 
 