Karisakattu Poove
{{Infobox film
| name           = Karisakattu Poove
| image          = 
| image_size     =
| caption        = 
| director       = Kasthuri Raja
| producer       = Vijayalakshmi Kasthuri Raja
| writer         = Kasthuri Raja
| starring       =  
| music          = Ilaiyaraaja
| cinematography = R. Raja Ratnam
| editing        = D. Chandrasekaran 
| distributor    =
| studio         = Kasthoori Manga Creations
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 2000 Tamil Tamil drama film directed by Kasthuri Raja. The film features Napoleon (actor)|Napoleon, Vineeth, Kushboo and Ravali in lead roles. The film, produced by Vijayalakshmi Kasthuri Raja, had musical score by Ilaiyaraaja and was released on 23 June 2000 to negative reviews.  

==Plot==

Pounrasu (Vineeth) has a brother Kottaisaamy (Napoleon (actor)|Napoleon) and a sister (Kovai Sarala). Pounrasu loves his sisters daughter Nagamani. Pounrasu returns after studying in the city, he still loves her.

Nagamanis father (Manivannan) fixes up her marriage with a rowdy for the money but her mother (Kovai Sarala) wants her brother to marry Nagamani. Finally, Kottaisaamy gets engaged with Nagamani. Pounrasu is unable to speak up against his brother.

Kottaisaamys family and his uncles family are in feud since several years. Only because, Kottaisaamy sends his uncle in jail.

Kottaisaamy and Nagamani get ready for the wedding. Meanwhile, Sankarapandian, his uncle, manages to provide a wrong message to Kottaisaamy that Aandals marriage was a forced marriage. Kottaisaamy immediately cancels Aandals marriage and Kottaisaamy is forced to marry Aandal (Kushboo). Nagamani feels betrayed and cannot forget Kottaisaamy with whom she was in love.

Kottaisaamy and Aandal dont live happily and they are in conflicts all the time. One day, Aandal discovers that Pounrasu with his uncle were in fact the culprits. They managed to cancel her marriage because Pounrasu wanted to marry Nagamani. Finally, Aandal promises to Pounrasu to put together Nagamani and him. Meantime, his uncle kills Aandal.

The rest of the story is about what happens to Pounrasu and Nagamani.

==Cast==
 Napoleon as Kottaisaamy
*Vineeth as Pounrasu
*Kushboo as Aandal 
*Ravali as Nagamani Vivek
*Senthil Senthil as Chinnayya
*Manivannan as Nagamanis father
*Kovai Sarala as Nagamanis mother Manorama
*Dhamu
*Vadivukkarasi
*Nambirajan as Sankarapandian
*Karikalan
*Kumarasesan

==Soundtrack==

{{Infobox album |  
| Name        = Karisakattu Poove
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 2000
| Recorded    = 2000 Feature film soundtrack |
| Length      = 33:32
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 2000, features 7 tracks with lyrics written by Kasthuri Raja.    

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Hariharan || 4:53
|- 2 || Ethanai Manikku || Arunmozhi, Anuradha Sriram || 4:51
|- 3 || Pushpavanam Kuppusamy, Anitha Kuppusamy || 5:03
|- 4 || Mamarathula || P. Unni Krishnan, Bhavadhaarini || 4:45
|- 5 || Manasirukka Manasirukka || Pushpavanam Kuppusamy, Swarnalatha || 5:20
|- 6 || Un Kendakaalu || Mano (singer)|Mano, Swarnalatha || 4:46
|- 7 || Vaanampaartha || Ilaiyaraaja || 3:54
|}

==References==
 

 
 
 
 
 