Once Upon ay Time in Mumbai Dobaara!
 

 
{{Infobox film
| name = Once Upon a Time in Mumbai Dobaara!
| image = Once Upon ay Time in Mumbai Dobaara!.jpg
| caption = Theatrical release poster
| alt = The bust of the title character, Shoaib Khan wearing Black full frame sunglasses consisting the reflection from streetlights dotting the coast line of the city and Black suit and Purple and Violet stripped shirt underneath holding a gun in his hand occupies a significant portion of the poster, mostly left and lower-front. In this case the city being Mumbai or Bombay as it was known, in the period which the film is set in. The other male lead of the film facing front wearing denim jacket, saffron shirt and stylised Sunglasses holding his shirt-collar up with a woman breaking into laughter resting on his chest looking upwards. The backdrop consists of verendahs of chawls of Mumbai and a waxing crescent moon in the sky. The title logo of the film and credits below.
| director = Milan Luthria
| producer = Ekta Kapoor Shobha Kapoor
| writer = Rajat Arora Imran Khan Sonakshi Sinha
| music = Pritam Anupam Amod
| cinematography = Ayananka Bose
| editing = Akiv Ali
| studio =
| distributor = Balaji Motion Pictures
| released =    
| runtime = 152 minutes   
| country = India
| language = Hindi
| budget =    
| gross =  
}}
 2013 Cinema Indian gangster Imran Khan, with Sonali Bendre in a supporting role.
 augmented 600 RED and Alexa Digital digital motion-picture camera. Once Upon   Time in Mumbai Dobaara! was well-promoted, and the films second trailer was shown on prime-time slots on four television channels. Hollywood actor Al Pacino was shown the theatrical trailer of the film.

The first trailer of the film was released on 28 May 2013, and the film premiered on 15 August, the eve of Independence Day in India. This was later than scheduled to avoid clashing with Rohit Shettys Chennai Express, which was released on 9 August.    Once Upon   Time in Mumbai Dobaara! received mixed to negative reviews  and performed poorly at the box office. 

==Plot== where its Imran Khan), a teenager Khan met during a visit to the slums in which he grew up.
 Bombay to kill Rawal (Mahesh Manjrekar), who opposed his ambition to take the reins in Bombay. Although the city was his first love, rising starlet Jasmine (Sonakshi Sinha) wins his heart. Khan advances Jasmines career by rigging awards, and showers her with gifts; she becomes obsessed with him. He sends Aslam and another trustworthy aide, Jimmy, to kill Rawal. Jimmy fails, accidentally killing his girlfriend, Sonia, and flees. He is soon captured by the police, hot on Khans trail after learning about his arrival in Bombay. Jimmy, loyal to Khan, does not crack during his interrogation and is freed due to lack of evidence. The police intend to draw Khan out from hiding; he has eluded them for twelve years, and this is their last chance to take him on Indian soil.

Khan, suspicious that Jimmy has become a police informer, decides to kill him but Aslam offers to do the deed to deflect police attention. He, Dedh Tang and several thugs masquerade as rival biker gangs and stage a brawl. Plainclothes police officers try to control the chaos when Aslam corners Jimmy. When a police officer stops Aslam, Khan appears out of nowhere, murders Jimmy and injures the policeman. A chase ensues; the police fail to catch the two, who escape in Khans Mercedes.

Although Khan has bought Jasmine a house, she rejects his advances, saying that she has never seen him as being more than a friend. Angry and disappointed, Khan slaps her and tells her to leave. He angrily destroys the set of the film she is cast in, leaving with her a twenty-four hour deadline to accept his proposal. Jasmine returns home to see that Khan has filled her house with expensive presents. Her mother is taken aback when Jasmine explains that Khan is a gangster with whom shed prefer not to associate. Her family questions the sense in rejecting a man as powerful and dangerous as Khan. 

Later in the night, Jasmine meets Aslam at his uncles restaurant. He tries to explain his love for her, only to be interrupted and told that she is engaged and the two should stop seeing one another. Dejected and disappointed, Aslam meets with Khan, who seems unusually pensive. The two lament their relationships, not knowing they are both in love with the same woman. Just before Aslam is about to reveal the name of his romantic interest, a phone rings - Khan, jubilant, announces that his proposal has been accepted and orders Aslam to pick up his new "bhabhi," or sister-in-law. 

Aslam arrives at the address Khan has given him and, much to his shock, sees Jasmine walking towards his car. Initially angry that he is still following her, the two realize that they have been caught up in a complicated affair - that Aslam, unbeknownst to Jasmine, works for Khan, and that Jasmine was the one he had been sent to pick up. Before either can speak, a car containing Rawal and his henchmen arrives at a distance. Rawal shoots Jasmine, having learned of her relationship with Khan and hoping that hurting her lure the gangster into a vulnerable position. 

Jasmine remains unconscious at the hospital as Khan arrives. He accuses Aslam of being in love with his fiancee - an accusation which Aslam denies as Khan reveals that he has masterminded a plan to murder Rawal and ensure his own dominance over the Mumbai underworld. Khan intends to fabricate a story of him feuding with Aslam to tempt Rawal and his rivals into contacting Aslam. The "news" reaches Rawal, who seems delighted at the prospect of being able to turn Khans right-hand man against him. 

When Rawal calls Aslam at the hospital, Aslam, now fully aware of Khans feelings towards Jasmine, seems unsure whether to abide by Khans plan. He reluctantly provides Rawal with an address, ending the phone call by telling him to "murder the bastard." Khan, lying in wait outside the address with a sniper rifle, kills Rawal and his henchmen when they arrive. Khan, having consolidated power, savours his victory and walks into a police station as a taunt to law enforcement. However, Dedh Tang accidentally reveals Aslams love for Jasmine in the midst of celebration. Khan, infuriated, diverts the car to the hospital to confront Aslam, who has continued to stay with Jasmine as she recovers from being shot by Rawal. Knowing that his friend is in danger, Dedh Tang honks the horn of the car repeatedly after Shoaib and his henchmen enter the building, alerting Aslam to the danger and giving him time to escape. Entering to see an empty bed where Jasmine had before been laying, Khan realizes that Dedh Tang had given him away. 

Khan sends his aide Akbar to kill Aslam after he and Jasmine flee to Aslams uncles restaurant in Dongri. Aslam assures Jasmine that Khan, though violent, would never hurt him. Their conversation is interrupted by a cars horn honking - running outside, Aslam is ambushed by Akbar after discovering the body of Dedh Tang, who has been killed by Shoaib for aiding the couple in their escape. Aslam, though injured and initially losing the fight, kills Akbar with a shovel. He calls Khan to tell him that the murder of Dedh Tang has, in effect, erased his deep-seated loyalty to his former boss. Khan responds by telling Aslam that sending Akbar was a mistake, as he must be the one to kill the man he views as also having betrayed him by "stealing" Jasmine. 

Aslam and Khan confront one another as the police, having been alerted to Khans location by a storekeeper, descend upon Dongri. Shoaib rains down blows and insults upon Aslam, urging him to defend himself in front of Jasmine. Nevertheless, Aslam, who still feels indebted and bound to Khan, does not fight back until Inspector Awant appears through the gathered crowd - even then, Aslam only pushes Khan away so that Shoaib is not shot by the officer. Reacting quickly, Khan draws a pistol of his own and shoots Awant before turning his weapon on Jasmine and Khan. Jasmine, through tears and while holding a bleeding Aslam, tells Khan that he is truly evil and will never have her love. Shoaib seems to recognize the truth in her words as he drops his weapon and stares at the couple. Just then, more police officers arrive on the scene and open fire on Khan, who, now injured, is dragged away by one of his henchmen. He and Aslam, both alive but bloodied, lock eyes and reach out for another, having finally put aside their differences.
 cargo hold of a ship.

==Cast==
* Akshay Kumar as Shoaib Khan Imran Khan as Aslam
* Sonakshi Sinha as Jasmine Sheikh 
* Sonali Bendre as Mumtaz Khan
* Sarfaraz Khan as Javed
* Mahesh Manjrekar as Rawal
* Abhimanyu Singh as ACP Ashish Sawant
* Kurush Deboo as Tailor
* Pitobash Tripathy as Dedh Taang
* Chetan Hansraj as Jimmy
* Sophie Choudry  
* Tiku Talsania as Tayyab Ali
* Ajay Devgn as Sultan Mirza (footage from previous film)
* Vidya Balan (cameo).   

==Production==

===Filming===
  where most of the Oman portion of the film was shot]]
Tanuj Garg, CEO of Balaji Motion Pictures, announced on Twitter that principal photography began on 27 August 2012 in Mumbai.    Female lead Sonakshi Sinha joined the cast and crew two days later after completing her previous film, Son of Sardaar, with Ajay Devgn.      The overseas shooting schedule began in September in Oman,    the first major Indian production filmed in the sultanate.
Portions of the film were shot on a beach in Qantab, on the corniche in Qurum, and at the Shangri-Las Barr Al Jissah Resort & Spa in Muscat, Oman|Muscat.          When director Milan Luthria became ill with a virus, shooting was postponed for three days. To make up the lost time Luthria persuaded Kumar to work on two consecutive Sundays, the first time he had worked on a Sunday since Priyadarshans 2006 Bhagam Bhag.    The remainder of the film was shot on location in Khala, on the Sassoon Docks and at Mukesh Mills in Mumbai.   The shoot was wrapped up by January 2013.      

===Effects=== compositors was augmented 600 3D models. RED and Alexa Digital digital motion-picture camera. 

===Casting=== Imran Khan Eid 2013.   TV actor Chetan Hansraj was signed as an antagonist in the film. Kumar revealed during the trailer launch of the film about having no aspirations to set new benchmarks on a role previously enacted by Hashmi.    Khan in preparation for his role in the film drew inspiration from Jackie Shroff and Anil Kapoor, copying their mannerisms, behaviour and dressing style by watching DVDs of films starring them produced in the 1980s. Back in those days copying mannerism of film stars drew appreciation from peers.     

==Promotion and distribution==
  to pray for the films success]] India and Pakistan at ICC Champions Trophy match on 15 June 2013 at Edgbaston Cricket Ground.     A second trailer of the film was released on 3 July 2013, with narration by Kumars character (Shoaib Khan), revealing plot details such as the rivalry between Kumars and Imran Khans characters for Jasmines love. The trailer described Khan as the films antihero, featuring action sequences between Shoaib Khan and Aslam (Imran Khan). Mohar Basu of Koimoi praised the second trailer, its background score, direction and action choreography, saying it "is even better than the films first trailer" and has "an explosive impact on its audiences".   
 Kapil Sharma.       Kumar, Sinha, Khan, Kapoor and most of the other cast and crew visited the Dargah Sharif (a shrine to the Sufi saint Moinuddin Chishti in Ajmer) on Monday morning, 5 August, to pray for the films success and the well-being of all involved.   

Hollywood actor  , who sang an original version of a song in Amar Akbar Anthony, praised Imran Khans performance and the song after an impromptu meeting with Khan (who was concerned about comparisons between the versions) during a dubbing session for the film.   

 
The films title has been changed several times. The media first referred to the production as Once Upon a Time in Mumbaai 2, although it was earlier officially known as Once Upon a Time in Mumbaai Again.  Due to the producers beliefs in astrology and numerology, the film was renamed Once Upon a Time in Mumbaai Dobara    ("again" in Hindi). A "y" and an extra "a" were added, making it Once Upon   Time in Mumbai Dobaara! (the present title).    
 Ramazan for iftaari and feed the poor.   

Chennai Express and Once Upon   Time in Mumbai Dobaara! were originally not scheduled for release by Pakistani distributors and exhibitors, but deals were finalised to release both films in Pakistan.    

In an effort to discourage cinema audience from using Mobile phones at theatres with the help of a short-film featuring Kumar in his character, Shoaib Khan from the film. The short directed by Luthria was shown in PVR Cinemas from 31 May 2013 till the films theatrical release.   

==Soundtrack==
{{Infobox album
| Name = Once Upon   Time in Mumbai Dobaara!
| Type = Soundtrack
| Artist =  
| Cover = 
| Border =
| Alt = Two men facing each other with a woman in between them and the logo of the film at the lower middle part of the image
| Caption = Soundtrack cover
| Released = 17 July 2013
| Genre = Film soundtrack
| Recorded =
| Length = 23:42
| Label = T-Series, Balaji Motion Pictures
| Producer = Ekta Kapoor, Shobha Kapoor, Pritam
| Chronology = Pritam
| Last album = Yeh Jawaani Hai Deewani (2013)
| This album = Once Upon   Time in Mumbai Dobaara! (2013)
| Next album = Phata Poster Nikla Hero (2013)
| Misc = {{Extra chronology
 | Artist = Anupam Amod
 | Type = Soundtrack
 | Last album = Murder 3 (2013)
 | This album = Once Upon   Time in Mumbai Dobaara! (2013)
 | Next album = I Love New Year (2013)   
 }}
 {{Extra chronology
 | Artist = Laxmikant-Pyarelal
 | Type = Soundtrack
 | Last album = Nautanki Saala (2013)
 | This album = Once Upon   Time in Mumbai Dobaara! (2013)
 | Next album =
 }}
 {{Singles
 | Name = Once Upon   Time in Mumbai Dobaara!
 | Type = Soundtrack Singles
 | Single 1 = Ye Tune Kya Kiya
 | Single 1 date = 14 June 2013
 | Single 2 = Tayyab Ali
 | Single 2 date = 28 June 2013
 | Single 3 = Tu Hi Khwahish
 | Single 3 date = 16 July 2013
 | Single 4 = Chugliyaan
 | Single 4 date = 22 July 2013
 | Single 5 = Bismillah
 | Single 5 date = 7 August 2013
 }}
}} digital download singles on the digital music platform iTunes.                 The music video for the fourth song, "Chugliyaan" (released earlier with the album), released on the T-Series YouTube channel on 22 July.    The video for the fifth single, "Bismillah" (an Eid track), was released on the T-Series YouTube channel on 7 August.    "Bismillah", shot at Film City and a Mumbai nightclub modified to evoke the 1980s, was released by Balaji the day before Eid to capitalise on Eid celebrations nationwide.   

{{Track listing
| collapsed = yes
| headline = Track listing
| extra_column = Singer(s)
| total_length = 23:42
| lyrics_credits = yes
| music_credits = yes

| title1 = Yeh Tune Kya Kiya
| extra1 = Javed Bashir
| music1 = Pritam
| lyrics1 = Rajat Aroraa
| length1 = 5:14
| title2 = Tayyab Ali
| extra2 = Javed Ali
| music2 = Laxmikant-Pyarelal (original) Anupam Amod (album)
| lyrics2 = Anand Bakshi (original) Rajat Aroraa (album)
| length2 = 4:37
| title3 = Tu Hi Khwahish
| extra3 = Sunidhi Chauhan
| music3 = Pritam
| lyrics3 = Rajat Aroraa
| length3 = 4:51
| title4 = Chugliyaan
| extra4 = Javed Ali, Sahir Ali Bagga
| music4 = Pritam
| lyrics4 = Rajat Aroraa
| length4 = 4:59
| title5 = Bismillah
| extra5 = Anupam Amod
| music5 = Anupam Amod
| lyrics5 = Rajat Aroraa
| length5 = 4:01
}}

===Critical response===
{{Album ratings
| rev1 = Bollyspice
| rev1Score =   
| rev2 = Movie Talkies
| rev2Score =   
| rev3 = Bollywood Hungama
| rev3Score =   
| rev4 = Rediff
| rev4Score =     
| rev5 = The Times of India
| rev5Score =   
| rev6 = Koimoi
| rev6Score =   
}}
The soundtrack of the film received mainly-positive reviews. Rumnique Nannar of Bollyspice gave the soundtrack three-and-a-half stars, saying "the album succeeds in all respects, since it features a remake, a retro song, and two gems, and manages to fit with the films tone and mood of the 80s".    Movie Talkies also gave the soundtrack three-and-a-half stars, remarking that "the soundtrack of OUATIMD is an excellent effort from Pritam and is sure to be lapped up by the audiences in much the same way as its predecessor. It sure might be a short album with just four songs in it but suffice to say, Quality wins over Quantity".    Rajiv Vijayakar of Bollywood Hungama said that "the soundtrack has only four songs but nevertheless emerges as one of the better albums of this year" praising the first three tracks identifying Tu Hi Khwahish and Chugliyaan as the finest and weakest links of the album respectively.    Bryan Durham of The Times of India said that "brevity seems to be the soul of the Once Upon a Time in Mumbaai Dobaara soundtrack", highlighting Yeh Tune Kya Kiya as the best song."    Mohar Basu reviewed the soundtrack for Koimoi, summarising it to be "just about alright... in comparison to its predecessor turned out to be drab!"   

==Release and reception==
The film which at the beginning was scheduled to be released on Eid, 8 August 2013        was pushed back to 15 August 2013, to avoid clashing with Rohit Shettys directorial venture, Chennai Express starring Shah Rukh Khan.         The decision was made by Jitendra on Shah Rukh Khans request after a meeting was held between the producers of the respective films so as to provide both each of the films ample opportunity of earning-well at the box office without diminishing each others profits.         

===Critical reception===
Once Upon   Time in Mumbai Dobaara was released on 1,300–1,600 screens for Indias 67th Independence Day, and some cinemas were sold out.     The screen count increased on 16 August to 2,500–2,700 nationwide.       

Once Upon   Time in Mumbai Dobaara! received mixed-to-negative reviews.     , telling readers to watch the film only for its flattering dons and their heroines, saying "theres nothing criminal about it!"  She praised Milan Luthria for capturing the essence of the films setting and Kumars portrayal of Shoaib Khan: "Bhai act with flamboyance and mojo ... He gets a chance to do what he does best – herogiri (albeit less menacing, more entertaining), with charisma and clap-trap dialoguebaazi"  but criticised Imran Khans "ill-at-ease" rogue,  the second half of the film for being "at times too overbearing"  and said, "this film has its moments, but its not as compelling as the prequel."    Vinayak Chakrovorty gave the film two-and-a-half out of five stars in India Today: "OUATIMD should remind Bollywood what we all always knew: Never make a sequel just for the heck of it if you dont have a genuine idea to entertain".  Subhash K. Jha termed the film as "one of the most ill-timed gangster dramas" criticising the acting of all the three leads, praising only the short cameo of Sonali Bendre. 

The film drew a number of negative reactions from the film critics. Subhra Gupta gave the film a half-star out of five in  s cameo as Mumtaz.    Mohar Basu gave the film two out of five stars in Koimoi, criticising the melodrama and the "lacklustre chemistry", remarking that the film "lacks the effervescence and naturalness of its prequel".   

Once Upon   Time in Mumbai Dobaara! and its prequel Once Upon a Time in Mumbaai have been described as having a storyline and characters similar to the life of Dawood Ibrahim.    Luthria and Kapoor have "repeatedly drilled home the notion that Shoaib Khan could be any gangster and not necessarily Dawood"  a position that Kumar has also stated.      

===Box office===
Once Upon   Time in Mumbai Dobaara! opened well, with sold-out showings (particularly in Delhi, Mumbai, Punjab, Uttar Pradesh and Hyderabad, India|Hyderabad) due to its release on Independence Day eve.  Due to limited screen availability (about 1,000), Balaji Motion Pictures negotiated with the producers of Chennai Express to release 1,000 more screens for a total of 2,700. Despite the low number of screens, the film netted   on its opening day.    Due to competition from Chennai Express the film earned   (  three-day ) over its extended four-day weekend, considered a poor result.   Earnings dropped on Monday to  .  The film did better on Tuesday and Wednesday due to Raksha Bandhan, earning   on both days.  It earned   (  seven-day ) during its eight-day first week.  During its second weekend the film dropped nearly 90 percent, earning   to bring its total to  .  The film earned   during its second week, bringing its total to  . Despite this, it was declared a "flop" by Box Office India.    The film did not do well overseas, earning $1.55&nbsp;million during its first weekend. 

== Home media and world television premiere==

=== World television premiere=== Sony SET Max|Max,  along with three other films produced by Balaji Motion Pictures.   The other films, acquired for about  ,  were Ek Thi Daayan, Lootera and Shootout at Wadala; the latter was acquired for  .            Satellite rights to the film were later confirmed as  .        After the films poor box-office performance, its producers and Sony Max moved its world television premiere up to 29 September 2013, within a month of the films theatrical release.   Satellite Rights of the film fetched   which is a record for Akshay kumar,

=== Home media=== DVD and Amazon and other online shopping sites in early October.      On Amazon, the expected delivery date was 7 November 2013 for Blu-ray preorders, with the DVD released by 26 October.   The Blu-ray version was released on 27 October as a 50 GB single disc with 1080p H.264/MPEG-4 AVC video and an aspect ratio of 2.35:1 (instead of the 2.39:1 theatrical ratio) and Dolby DTS-HD Master Audio 5.1 and Dolby Digital 5.1 audio. 

== Impact==
After the films mainly negative reviews Akshay Kumar lashed out at critics, accusing them of lacking an understanding of the audience and the basic ideas of filmmaking.    There were rumours about a feud between Kumar and Ekta Kapoor about the formers distancing himself from the project and not promoting it enough.    The three leads attended the inaugural 2013 Indian Premier League match between Kolkata Knight Riders and Delhi Daredevils to promote the film, but the match organisers asked their cameramen not to focus on the actors.   After this, a disappointed Kumar asked Kapoor to re-arrange the films marketing schedule.      

===Awards===
Zee Cine Awards— 

* Nominated, Best Actor in a Negative Role: Akshay Kumar

==See also==
* Once Upon a Time in Mumbaai Bollywood films of 2013

==Notes==
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 