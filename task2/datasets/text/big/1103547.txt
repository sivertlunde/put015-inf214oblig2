Brainstorm (1983 film)
{{Infobox film
  | name = Brainstorm
  | image = Brainstorm Movie Poster.jpg
  | caption = Film poster
  | director = Douglas Trumbull
  | producer = Douglas Trumbull
  | screenplay = Philip Frank Messina Robert Stitzel
  | story = Bruce Joel Rubin
| starring = {{Plainlist|
* Christopher Walken
* Natalie Wood
* Louise Fletcher
* Cliff Robertson
}}
    | music = James Horner
  | cinematography = Richard Yuricich Dennis Freeman Edward Warschilka
  | distributor = Metro-Goldwyn-Mayer
  | released = September 30, 1983
  | runtime = 106 minutes English
  | budget = $18 million
  | gross = $10,219,460
  }}

Brainstorm is a 1983 science fiction film directed by Douglas Trumbull and starring Christopher Walken, Natalie Wood, Louise Fletcher and Cliff Robertson. It was Woods final film appearance, as she died during production, and was also the second and final major motion picture to be directed by Trumbull.

The film follows a research teams efforts to perfect a system that directly records the sensory and emotional feelings of a subject, and the efforts by the companys management to exploit the device for military ends.

==Plot==
A team of scientists invents a brain-computer interface|brain/computer interface that allows sensations to be recorded from a persons brain and converted to tape so that others may experience them. The team includes estranged husband and wife Michael and Karen Brace, as well as Michaels colleague Lillian Reynolds. At CEO Alex Tersons instruction, the team demonstrates the device to investors in order to gain financing. 
 splices one section of the tape into a continuous orgasm, which results in sensory overload - leading to his forced retirement. Tensions increase as the possibilities for abuse become clear.

Karen dons the recorder while working with Michael and Lillian. When Michael plays the tape back, the group realizes that emotional experiences are also recorded. Michael tapes his memories, which he shares with Karen, leading to their reconciliation.

Lillian is pressured by backers to admit a former colleague, Landon Marks, whom she sees as part of the military industrial complex. She disagrees with their plan to have the invention developed for military use. Lillian suffers a heart attack while working alone. Realizing that she will die, Lillian records her experience. 

Following her funeral, Michael decides to experience Lillians recording, but nearly dies when the playback causes his body to simulate the sensations and effects of a heart attack. Michael modifies his console to censor the physical output, and replays the tape.  Viewing Lillians death experience, he sees Life review|"memory bubbles" -- moments from Lilians life. Michael experiences Lillians memories of a humorous exchange with Michael as he plays with an industrial robot, a surprise birthday party, and being devastated when Alex tells her that an earlier project has been cancelled. 
 tap into Michaels playback of Lillians tape. They have Gordo also experience the tape, but ignore Michaels modifications; Gordo is killed when he experiences the unfiltered tape.

Michaels playback is cut short by Hal, but having witnessed a digital near-death experience makes him curious to see the entire tape. Alex has the recording locked away and tells Michael he will not be allowed to view it. When he returns to work, Michael walks in on Landon Marks and a team of outsider technicians going through his research records and he protests. Alex fires Michael and Karen.

Michael attempts to hack into the labs computers. Hal advises him to look under "Project Brainstorm", a program the military has created to re-develop their invention for torture and brainwashing. Michael and Karens son Chris is inadvertently exposed to one such tape, causing him to have a psychotic experience which results in his hospitalization.

Rather than see his creation perverted, Michael vows to destroy his work and enlists the help of Karen and Hal. Michael and Karen head to the Pinehurst Resort and, realizing they are under surveillance, stage a fight that results in Karen leaving for Hals house. As the two feign reconciliation over the phone, Michael accesses the Brainstorm computer via another phone line, while Karen hacks into the system and sabotages the robots that manufacture the interface terminals. 

Karen shuts down the security system, locking the staff outside and enabling Michael to load Lillians tape and experience it uninterrupted. With the plant in chaos, Robert Jenkins orders Michaels arrest. Michael escapes their agents, and drives to a phone booth at the Wright Brothers National Memorial. He reconnects with the computers and accesses the final part of the tape, beyond the point of Lillians physical death.

Karen leaves the house to meet with Michael. Hal and his wife send the last of Karens commands to the company computers, shutting the plant down.

Karen arrives at the Wright Brothers Memorial while the tape is playing. Michael bears witness to the Afterlife, experiencing a brief vision of Hell before traveling away from Earth and through the universe, even after the tape ends, ultimately witnessing visions of angels and departed souls flying into a great cosmic Light. Michael then collapses. Karen sobs, pleading for Michael to come back. Awakening from the experience, he weeps with joy and embraces Karen.

==Cast==
*Christopher Walken as Michael Brace
*Natalie Wood as Karen Brace
*Louise Fletcher as Lillian Reynolds 
*Cliff Robertson as Alex Terson
*Alan Fudge as Robert Jenkins
*Joe Dorsey as Hal Abramson 
*Jason Lively as Chris Brace

==Production==
To prepare for the film, Trumbull took most of the key cast and crew up to the Esalen Institute, an experimental research facility in Northern California known for its new-age classes and workshops. In September 1981 the cast and crew traveled to North Carolina to begin six weeks of location shooting, before moving back to MGM Studios in California in November to film interior scenes.   

===Natalie Woods death===
The film was nearly scuttled by Natalie Woods death during a production break in November 1981. By this time, Wood had already completed all of her major scenes,  but due to mounting financial problems, MGM took Woods death as an opportunity to shut down the already troubled production. "When she died," said Trumbull, "all the sets were locked and frozen on all the stages. No one could get in or out without special permission while all the negotiations took place." 

Trumbull believed that the financially strapped studio simply got cold feet about putting up the rest of the money to complete Brainstorm. "MGMs problem was that insurance institution Lloyds of London, when it took depositions from me and other people, realized that the film could be finished. Why should they pay an insurance claim for something that really wasnt damaged goods?" When MGM refused to pay for the film to be completed, Lloyds of London provided $2.75 million for Trumbull to complete principal photography and an additional $3.5 million towards post-production. Meanwhile, other studios showed interest in buying Brainstorm from MGM to release as their own production. "MGM decided to allow Lloyds of London to offer the film to many of the major studios in town," said Trumbull. "Several of them made bids to MGM. And the studio suddenly realized that a lot of other people in this town were excited about Brainstorm, and were ready to put up millions of dollars. MGM figured theyd look like jerks if they let it go and it turned out to be a big success. So they finally decided to work out this deal where Lloyds of London would put up the remaining money and become a profit participant." 

Trumbull proceeded to complete the film by rewriting the script and using a body double for Woods remaining scenes.   

The film carries the dedication credit "To Natalie." 

=== Effects ===
The film was conceived as an introduction to Trumbulls Showscan 60 frames-per-second 70mm film process. "In movies people often do flashbacks and point-of-view shots as a gauzy, mysterious, distant kind of image," Trumbull recalled, "And I wanted to do just the opposite, which was to make the material of the mind even more real and high-impact than reality".

However, MGM backed out of plans to release the experimental picture in the new format. Trumbull instead shot the virtual reality sequences in 24 frames-per-second Super Panavision 70 with an aspect ratio of 2.2:1. The rest of the film was shot in conventional 35mm with an aspect ratio of approximately 1.7 to 1. {{cite web url = http://johnandjana.net/archive7/?p=3553 title = Interview: Douglas Trumbull accessdate = 2011-01-16 author =  last =  first =  authorlink =  coauthors =  date = July 7, 2009 work =  publisher =  pages =  quote = 
}} 

=== Soundtrack ===
The score to Brainstorm was composed and conducted by James Horner, it won him the Saturn Award for Best Music in 1983. The Varèse Sarabande album/CD release is a re-recording with the London Symphony Orchestra, produced shortly before the original theatrical release. 

==Reception and aftermath==
Brainstorm was finally released on September 30, 1983, almost two years after Woods death. However, it opened on a small number of screens and with little publicity, despite being trumpeted unofficially as "Natalie Woods last movie". The film received mixed to mostly positive critical opinion with review aggregator Rotten Tomatoes reporting that 63% of 16 critics have given the film a positive review.   Janet Maslin of the New York Times gave particular credit to Louise Fletchers "superb performance"  

The film was not a commercial success, with a production budget of $18 million  and grossing only $10 million in ticket sales in North America. 

Because of the immensely troubled production and disagreements with MGM, Trumbull opted never to direct a Hollywood film again. In 1983 he stated "I have no interest...in doing another Hollywood feature film...Absolutely none. The movie business is so totally screwed-up that I just dont have the energy to invest three or four years in a feature film. Moviemaking is like waging war. It destroys your personal life, too. The people who can survive the process of making films have largely given up their personal lives in order to do that, just because its such a battle to make a movie. And in doing that, theyve isolated themselves from the very audience that theyre trying to reach." 

==See also==
* Artificial reality
* Augmented reality
* Brain-computer interface
* BrainGate
* Flight simulation
* Simulated reality
* Virtual reality
* Thought recording and reproduction device
* The Dream Master (1966)
* Dreamscape (1984_film)|Dreamscape (1984)
* Paprika (2006 film)|Paprika (2006) Strange Days (1995)

==References==
 

==External links==
* 
* 
*  at Rotten Tomatoes
*   at Box Office Mojo

 
 
 
 
 
 
 
 
 
 
 