Ooriki Monagadu
{{Infobox film
| name = Ooriki Monagadu
| image = Image_of_Ooriki_Monagadu.jpeg
| caption = Film poster
| director =Kovelamudi Raghavendra Rao|K. Raghavendra Rao
| producer = 
| writer = 
| screenplay = Krishna Jaya Prada
| music = K. Chakravarthy
| cinematography = 
| editing =	 		
| studio =
| distributor = 
| released = 1981
| runtime =
| country = India
| language = Telugu
| budget =
| gross =
}} Krishna and Jaya Prada  in the lead roles.  It was remade in Bollywood as Himmatwala (1983 film)|Himmatwala in 1983  and as Himmatwala (2013 film)|Himmatwala in 2013.

==Plot==
Master Dharma Murti witnesses a murder of a man at the hands of Sher Singh Bandookwala. But Sher Singh with his might, terror and money sets free. In revenge he places Dharam Murti in an awkward position with a lady teacher, Menaka. Dharam Murti ashamed of this vacates his village and abandons his wife & children. His wife Savitri helps her son Ravi (Krishna) become an engineer who then resolves to fight against Bandookwala and get his fathers spoilt reputation back. Bandookwala has terrorised all the villagers. His daughter Rekha (Jaya Prada) follows his footsteps and grows into an insensitive woman who tries to harass people. Ravis sister Padma has to marry munimjis son Shakti as she is pregnant with his child. After marriage, Shakti and Bandookwala together start harassing Padma. Rekha falls in love with Ravi and decides to play same trick against the Bandookwala by pretending to be pregnant with Ravis child. Ravi finds his father as a worker on a dam construction site. Ravi then presents Bandookwala guilty before the panchayat but is freed by Dharam Murti on condition that he should show affection to the poor and treat them well. Finally Bandookwala becomes a good man and then Ravi marries Rekha.

==Cast== Krishna
*Jaya Prada
*Rao Gopal Rao

==References==
 

==External links==
*  

 
 
 
 
 
 

 