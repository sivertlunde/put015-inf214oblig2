Jolly Roger: Massacre at Cutter's Cove
 
{{Infobox film
| name           = Jolly Roger: Massacre at Cutters Cove
| image          = JollyRogerMassacre.jpg
| alt            = 
| caption        = DVD cover
| director       = Gary Jones
| producer       = David Michael Latt   David Rimawi   Sherri Strain
| writer         = Gary Jones   Jeffrey Miller
| starring       = Rhett Giles   Tom Nagel   Kristina Korn   Thomas Downey
| music          = Mel Lewis
| cinematography = David Robert Jones
| editing        = William Shaffer
| studio         = The Asylum
| distributor    = The Asylum
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = $500,000
| gross          = 
}}
Jolly Roger: Massacre at Cutters Cove is a 2005 horror film by The Asylum, written by Gary Jones and Jeffrey Miller and directed by Jones, and starring Rhett Giles as Jolly Roger.

== Plot ==
 
Jolly Roger comes back from the dead and kills descendants of his crew who mutinied against him 300 years earlier.

== Cast ==

* Rhett Giles as "Jolly" Roger Laforge
* Tom Nagel as Alex Weatherly
* Kristina Korn as Jessie Hendrickson
* Thomas Downey as Chief Mathis
* Kim Little as Det. Lowenstein
* Pamela Munro as Mayor Judith Bates
* Dean N. Arevalo as Deputy Tanner
* Sergio Samayoa as Deputy Yee
* Justin Brannock as Tom Torrington
* Megan Lee Ethridge as Sasha
* Hajar Northern as Eve
* Ted Cochran as Phillip
* Conrad Angel Corral as Mr. Sims
* Spencer Jones as Abernathy
* Amanda Barton as Agnes
* Griff Furst as William Barrows
* Kellie McKuen as Zelda
* Leigh Scott as Ray
* Berna Roberts as Carla
* Carrie Booska as Melinda "Bambi" Cornwell
* Ty Nickelberry as Danny Ray (Bouncer)
* Coleman McClary as Hancock

==Release==
 
The film was released in the U.S. on May 31, 2005 and had its DVD premiere in Japan on June 22, 2007. 
==Reception==
 
Critical reception for the film has been negative.

Dread Central gave the film a score of 2 1/2 out of 5 the reviewer stating, "While I enjoyed the slashing antics of Jolly Roger, as slight as they may have been at times, that ending did not leave me wanting more". 
Michael Helms from Digital Retribution.comgave the film a positive review, awarding the film a score of 4 / 5 stating, "Although there might have been a little more work performed on Jolly Roger himself to make him much stronger in the Wishmaster sense of second tier horror franchise characters, the bottom line is this film delivers the gory goods". 
Rick Blalock from Terror Hook.com gave the film a mixed score of 5.5 / 10 complimenting the films gore and sharp editing bur criticized the films "cheesy" plot and slow start. 

==References==
 
== External links ==

*  
 

 
 
 
 
 
 
 
 
 
 
 