Total Recall (2012 film)
 
 
{{Infobox film
| name           = Total Recall
| image          = TotalRecall2012Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Len Wiseman
| producer       = {{plainlist|
* Neal H. Moritz
* Toby Jaffe }}
| screenplay     = {{plainlist|
* Kurt Wimmer
* Mark Bomback }}
| based on       =  
| story          = {{plainlist|
* Ronald Shusett
* Dan OBannon
* Jon Povill
* Kurt Wimmer }}
| starring       = {{plainlist|
* Colin Farrell
* Kate Beckinsale
* Jessica Biel }}
| music          = Harry Gregson-Williams Paul Cameron
| editing        = Christian Wagner
| studio         = Original Film
| distributor    = Columbia Pictures
| released       =  
| runtime        = 118 minutes  
| country        = United States Canada Australia United Kingdom
| language       = English
| budget         = $125 million 
| gross          = $198.5 million 
}} science fiction adaptation of 1990 film. The film centers upon an ordinary factory worker who accidentally discovers that his current life is a fabrication predicated upon false memories implanted into his brain by the government. Ensuing events leave no room for doubt that his true identity is that of a highly trained secret agent. He then follows a trail of clues to gradually recover more suppressed memories and reassumes his original vocation with renewed dedication. Unlike the original film and the short story, the plot takes place on Earth rather than a trip to Mars and exhibits more political overtones.    The film blends American and Asian influences, most notably in the settings and dominant populations of the two nation-states in the story: the United Federation of Britain (Western Europe) and the Colony (Australia).

The film was written by Mark Bomback, James Vanderbilt, and Kurt Wimmer, and stars Colin Farrell, Kate Beckinsale, Jessica Biel, Bryan Cranston, John Cho, and Bill Nighy. It was first announced in 2009    and was released in North America on August 3, 2012, grossing over $198 million worldwide.       The film was released to mixed reviews. It received praise in certain areas such as its action sequences but the films lack of humor, emotional subtlety and character development drew the most criticism.

==Plot==
   United Federation gravity elevator running through the Earths core. A Resistance operating in the UFB seeks to improve life in the Colony, which the UFB views as a terrorist movement.

Colony citizen Randy Quaid has been having dreams of being a secret agent, aided by an unknown woman. Tired of his factory job building police robots with friend Harry, he visits Rekall, a virtual entertainment company that implants artificial memories. Among the choices Rekall salesman Bob McClane offers Quaid are the memories of a secret agent. Just as Quaid is starting to be implanted, McClane discovers that he already has real memories of being a covert operative. As McClane starts to question Quaid about the memories, UFB police officers burst in, killing the Rekall crews and attempt to arrest Quaid. Quaid instinctively reacts and kills the officers before escaping. Upon returning home his wife Lori attempts to kill him, revealing that she is an undercover UFB agent who has been monitoring him for the past six weeks. After Quaid escapes, Charles Hammond, a "friend" Quaid does not recognize, contacts him and directs him to a safe-deposit box. Quaid finds a recorded message from his former self with the address of a UFB apartment.

While being pursued by Lori and other human and robot police, Quaid meets Melina, the woman from his dreams. At the apartment Quaid finds another recording, revealing that his name is actually Carl Hauser, an agent working for UFB Chancellor Vilos Cohaagen. After defecting to the Resistance, Hauser was captured by the UFB and implanted with false memories. The recording reveals that Cohaagen will use robots to invade the Colony so the UFB will have more living space. Hauser, however, has seen a "kill code" that would disable the robots. The code can be recovered from his memory by Resistance leader Matthias. Melina reveals that she was Hausers lover before Hauser was captured; she proves that they knew each other by showing that they have matching scars from a time they were both shot whilst holding hands. The police surround the apartment building and Harry appears. He tries to convince Quaid that he is still in a Rekall-induced dream and that killing Melina is the only way out. Quaid is conflicted, but notices a tear on Melinas cheek and shoots Harry instead. Lori pursues the pair inside the buildings lifts, but fails to capture them.

Quaid and Melina meet with Matthias. While Matthias searches Quaids memories, Lori and Cohaagen storm the Resistance base. Cohaagen reveals that Hauser was in fact working for him without Quaid even knowing it due to the memory alteration, using the kill code as a trap. Cohaagen kills Matthias and arranges to restore Hausers memory before leaving with Melina as a prisoner. As the officers are about to inject Quaid, Hammond (revealed to be one of the police officers involved in the raid) sacrifices himself to help Quaid escape.

Cohaagen begins his invasion of the Colony, loading the Fall with his army of robots. Quaid sneaks on board, setting timed explosives throughout the ship while searching for Melina. After freeing her, they climb atop the Fall as it arrives at the Colony. As they fight the soldiers and Cohaagen, Quaids explosives detonate. Quaid and Melina jump off before the ship plummets back into the tunnel and explodes underground, killing Cohaagen and destroying his army and The Fall itself.

Waking up in an ambulance, Quaid is greeted by Melina. When he notices that she is missing her scar, he realizes that she is Lori using a holographic disguise; they fight and Lori is killed. Quaid finds the real Melina outside the ambulance and they embrace.

==Cast==
* Colin Farrell as Randy "Mongo" Quaid/Carl Hauser, a factory worker suffering from strange violent dreams. 
* Kate Beckinsale as Lori Quaid, a UFB undercover agent posing as Quaids wife. 
* Jessica Biel as Melina, a member of the Resistance and Quaid/Hausers love interest.  In the extended directors cut, she is Matthias daughter.
* Bryan Cranston as Chancellor Vilos Cohaagen, the corrupt and ruthless Chancellor of the United Federation of Britain. 
* Bokeem Woodbine as Harry, Quaids workmate and best friend - in fact an agent sent by Cohaagen to monitor him.
* Bill Nighy as Matthias Lair, the leader of the Resistance.   
* John Cho as Bob McClane, a rep for Rekall who offers Quaid the chance to experience an imagined adventure. 
* Steve Byers as Henry Reed, a cover for Hauser
* Dylan Scott Smith as Hammond

Ethan Hawke appears uncredited as Hausers original appearance in the directors cut. In the script as originally filmed, both Hausers memory and physical appearance were heavily altered by the UFB to turn him into Quaid. This plot point was excised from the theatrical cut, so Hawke appears only in the extended directors cut.

==Production==
On June 2, 2009, Variety (magazine)|Variety reported that Kurt Wimmer would write the script for the film.    Mark Bomback also co-wrote  and James Vanderbilt did a "polish" on the script.  Over a year later Len Wiseman was hired to direct.    Paul Cameron is the films cinematographer  and Christian Wagner is the films editor.  The soundtrack is a collaboration of Harry Gregson-Williams and Welsh electronica group Hybrid (Welsh band)|Hybrid.  Although described in the press as a "remake", star Jessica Biel claimed in her August 2, 2012 appearance on The Daily Show that the film is not a remake of the 1990 film, but an adaptation of the original short story by Philip K. Dick.  However, Biels own character of "Melina" was not actually present in the original short story by Philip K. Dick and only exists in this film and the original 1990 film. The same goes for the characters of Cohaagen and Harry, along with the leader of the Resistance. This version of the film also uses the names Quaid and Lori for the main character and his wife, like the 1990 film, whereas in the original short story they were Quail and Kirsten. The basic story also follows that of the original 1990 film, albeit with certain changes such as moving the action from Mars to "the Colony".

In August 2010, Arnold Schwarzenegger expressed an interest in reprising his role as Quaid until October 2010, when it was officially reported in The Hollywood Reporter that Colin Farrell was on the top of the short list, which included Tom Hardy and Michael Fassbender, to play Quaid.  On January 11, 2011, it was announced that Farrell had secured the role.    Farrell stated in April that the remake would not be the same as Dicks short story.   

Beckinsale and Biel were both confirmed for roles on May 25,    after actresses Eva Green, Diane Kruger, and Kate Bosworth had previously been considered for Biels role.    Actor Bryan Cranston was cast as the films villain.     Ethan Hawke was reportedly cast in a cameo role, and commented that his character had a monologue about five pages long;       however, this role was later cut. Though cut from the original film, Ethan Hawke is featured in the extended version of the film.  Later cast additions included Bill Nighy    and John Cho.   
 Lower Bay Red Epic digital cameras anamorphic lenses.  After securing the film rights from Miramax Films, Columbia Pictures distributed the film. 

==Release==
===Box office performance===
Total Recall was released on August 3, 2012, and opened in 3,601 theaters in the United States, grossing $9,092,341 on its opening day and $25,577,758 on its opening weekend, ranking #2 with a per theater average of $7,220. The film bombed domestically with only $58,877,969 but made a strong $139,589,199 outside of the United States for a total of $198,467,168.  . Box Office Mojo. IMDb. Retrieved August 6, 2012.   against a $125 million budget.

===Critical response===
The film has received mixed to negative reviews from critics. It has a 30% "rotten" rating on the review aggregator site  , which assigns a weighted mean rating out of 100 to reviews from mainstream critics, the film received an average score of 43, based on 41 reviews, which indicates "mixed or average reviews."    Critics cited Total Recall s action sequences as "visually impressive". The film earned a Razzie Award nomination for Biel as Worst Supporting Actress.

 " that the bland aftertaste of the chase finale is quickly forgotten."      of the Chicago Tribune also gave the film a positive review, stating that "the movie marches in predictable formations as well. But when Biels rebel pulls over in her hover car and asks Farrell if hed like a ride, your heart may sing as mine did."   

Justin Lowe of   gave the film two-stars-out-of-four, saying: "So what makes this 2012 Total Recall superior to the Arnie model? For starters, theres an actual actor in the starring role.... Still, this Recall has more than its share of flaws."    Owen Gleiberman of Entertainment Weekly gave the film "C", stating that "this one is somberly kinetic and joyless."    Justin Chang of Variety (magazine)|Variety gave the film a mixed review: "Crazy new gadgets, vigorous action sequences and a thorough production-design makeover arent enough to keep Total Recall from feeling like a near-total redundancy."   

  of Rolling Stone also gave the film a negative review, stating that "since the new Recall is totally witless, dont expect laughs. Originality and coherence are also notably MIA."   

==Video game== Android was released as a tie-in to the film.  

==Possible sequel==
In July 2012, Farrell mentioned the possibility of doing another film.  Producer Neal Moritz said "if audience likes it, we’ll make a sequel". 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 