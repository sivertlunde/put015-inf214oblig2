Noviembre (film)
 
{{Infobox film
| name           = Noviembre
| image          = 
| caption        = 
| director       = Achero Mañas
| producer       = José Antonio Félez
| writer         = Achero Mañas Federico Mañas
| starring       = 
| music          = Eduardo Arbide
| cinematography = Juan Carlos Gómez
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}}
Noviembre is a 2003 Spanish film about a guerrilla street theater troupe presented as a retrospective false documentary, directed by Achero Mañas. 

The film focuses on the relationship between spectators and performers, the commercialization of art, and the revolutionary potential of theatre.

It was nominated for nine awards at several film festivals, including three at the 2003 Goya Awards. 

==Plot==
Impelled by a spirit which still preserves a patina of idealism, Alfredo arrives in Madrid intent on creating "a performance which is more free, straight from the heart, capable of making people feel alive". His concept of what theater should be begins beyond the stage, out in the streets face to face with the public. 

Outdoors, in any town square, in a park or in the citys most commercial street, Alfredo and his troupe NOVEMBER start the show: demons to provoke passers-by, displays of social conscience, actions taken to the extreme to put the forces of law and order on full alert. There are no limits, no censorship; only ideas which are always valid so long as the public ceases to be the public and becomes part of the show swept by surprise, fear, tears or laughter. 

==Cast==

*Óscar Jaenada as Alfredo
*Ingrid Rubio as Lucía 
*Javier Ríos as Juan
*Juan Díaz as Daniel
*Adriana Domínguez as Alicia
*Jordi Padrosa as Imanol
*Núria Gago as Helena
*Juanma Rodríguez as Pedro
*Héctor Alterio as Yuta
*Paloma Lorena as Lucía 
*Angel Facio as Juan
*Juan Margallo as Daniel
*Amparo Valle as Alicia
*Fernando Conde is Imanol
*Amparo Baró as Helena

==References==
http://www.imdb.com/title/tt0376800/
 

==External links==
*  

 

 
 
 
 
 
 

 