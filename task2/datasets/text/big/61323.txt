The Love Parade
 
{{Infobox film
| name           = The Love Parade
| image          = S1662062.jpg
| image_size     = 275px
| caption        = theatrical poster
| director       = Ernst Lubitsch
| producer       = Ernst Lubitsch
| writer         = Guy Bolton Ernest Vajda
| based on       = Le Prince Consort (French play) by Jules Chancel Leon Xanrof
| starring       = Maurice Chevalier Jeanette MacDonald Lillian Roth
| music          = Songs: Victor Schertzinger (music) Clifford Grey (lyrics)
| cinematography = Victor Milner
| editing        = Merrill G. White
| studio         = Paramount Pictures
| released       = November 19, 1929 (US)
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Love Parade is a 1929 musical comedy film about the marital difficulties of Queen Louise of Sylvania (Jeanette MacDonald) and her consort, Count Alfred Renard (Maurice Chevalier). Despite his love for Louise and his promise to be an obedient husband, Count Alfred finds his role as a figurehead unbearable. The film was directed by Ernst Lubitsch from a screenplay by Guy Bolton and Ernest Vajda, adapted from the French play Le Prince Consort,  written by Jules Chancel and Leon Xanrof; which had previously been adapted for Broadway in 1905 by William Boosey and Cosmo Gordon Lennox. 

The Love Parade is notable for being both the film debut of Jeanette MacDonald and the first "talkie" film made by Ernst Lubitsch.  It was also released in a French-language version called Parade damour.  Chevalier had thought that he would never be capable of acting as a Royal courtier, and had to be persuaded by Lubitsch. 
 Wall Street crash, and did much to save the fortunes of Paramount. 

==Plot==
Count Alfred, military attaché to the Sylvanian Embassy in Paris, is ordered back to Sylvania to report to Queen Louise for a reprimand following a string of scandals, including an affair with the ambassadors wife. In the meantime Queen Louise, ruler of Sylvania in her own right, is royally fed-up with her subjects preoccupation with whom she will marry.

Intrigued rather than offended by Count Alfreds dossier, Queen Louise invites him to dinner. Their romance progresses to the point of marriage when, despite his qualms, for love of Louise Alfred agrees to obey the Queen.

==Cast==
* Maurice Chevalier as Count Alfred Renard
* Jeanette MacDonald as Queen Louise
* Lupino Lane as Jacques
* Lillian Roth as Lulu
* Eugene Pallette as Minister of War
* E. H. Calvert as Sylvanian Ambassador
* Edgar Norton as Master of Ceremonies
* Lionel Belmore as Prime Minister

==Production==
Although The Love Parade was Lubitschs first sound film, he already displayed a mastery of the technical requirements of the day.  In one scene, two couples sing the same song alternately. To do this with the available technology, Lubitsch had two sets built, with an off-camera orchestra between them, and directed both scenes simultaneously.  This enabled him to cut back and forth from one scene to the other in editing, something unheard of at the time. Kalat, David.    on TCM.com 

==Music==
All songs are by Victor Schertzinger (music) and Clifford Grey (lyrics):

*"Ooh, La La" &ndash; sung by Lupino Lane
*"Paris, Stay the Same" &ndash; sung by Maurice Chevalier and Lupino Lane
*"Dream Lover" &ndash; sung by Jeanette MacDonald and chorus, reprise sung by Jeanette MacDonald
*"Anything to Please the Queen" &ndash; sung by Jeanette MacDonald and Maurice Chevalier
*"My Love Parade" &ndash; sung by Maurice Chevalier and Jeanette MacDonald
*"Lets Be Common" &ndash; sung by Lupino Lane and Lillian Roth
*"March of the Grenadiers" &ndash; sung by Jeanette MacDonald and chorus, reprise sung by chorus
*"Nobodys Using It Now" &ndash; sung by Maurice Chevalier
*"The Queen Is Always Right" &ndash; sung by Lupino Lane, Lillian Roth and chorus

==Awards and honors==
The Love Parade was nominated for six Academy Awards:         
 Best Picture Best Actor - Maurice Chevalier  Best Director - Ernst Lubitsch Best Cinematography - Victor Milner Art Direction - Hans Dreier Sound Recording - Franklin Hansen

==References==
Notes
 

==External links ==
*  
*  
*   by Michael Koresky
*   at Jeanette MacDonald and Nelson Eddy: A Tribute
*   at Virtual History

 

 
 
 
 
 
 
 
 
 
 
 