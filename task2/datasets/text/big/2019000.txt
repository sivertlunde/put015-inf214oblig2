The Real Blonde
{{Infobox film
|  name           = The Real Blonde
|  image          = The Real Blonde.jpg
|  caption        = DVD cover
|  director       = Tom DiCillo
|  producer       = Terry McKay Tom Rosenberg Sigurjon Sighvatsson Ted Tannebaum Marcus Viscidi Meredith Zamsky Richard S. Wright
|  writer         = Tom DiCillo
|  starring       = Matthew Modine Catherine Keener Maxwell Caulfield Daryl Hannah Bridgette Wilson Marlo Thomas Kathleen Turner Elizabeth Berkley Denis Leary Steve Buscemi Dave Chappelle Christopher Lloyd
|  music          = Jim Farmer
|  cinematography = Frank Prinzi
|  editing        = Keiko Deguchi Camilla Toniolo
|  studio         = Lakeshore Entertainment
|  distributor    = Paramount Pictures
|  released       = February 27, 1998
|  runtime        = 105 min. English 
}} 1998 film|movie directed and written by New Yorks entertainment industries.

==Plot==
Joe is an aspiring actor working as a bus boy in a high-class restaurant. His longtime girlfriend Mary works as a cosmetician for the fashion industry and largely supports him with her steady income. Joe is more concerned with expressing himself than getting a paying job, and has been unwilling to accept roles that do not live up to his artistic standard. Mary supports Joe, but urges him to accept any role to get his foot in the door. Meanwhile, his co-worker Bob lands a lucrative role on a soap opera. Bob is a classically trained actor, but is willing to overlook the quality of the material for the money. He also has a fetish for natural blonde women, leading him to date Sahara, a naive model, and then dump her after discovering that her hair is dyed.
 Madonna music video|video. Mary is harassed as she walks to work each day and begins taking a self-defense and anger management class on the advice of her therapist. The instructor encourages her to express her anger, and she finds the class extremely empowering. Bob is successful in his soap opera role and begins a relationship with his costar Kelly, a "real blonde".

At the Madonna video, the director treats Joe and the other extras like cattle. Joe meets Madonnas body double, Tina, a friendly aspiring actress, and gets himself fired for protesting an Holocaust denial|anti-Semitic statement made by the assistant director. Joes firing sparks an argument between Joe and Mary. The pressure of Joes career is straining their relationship, and they have not had sex in a long time. Marys instructor, Doug, gives her a ride home from her class and makes a pass at her. She rebuffs him, but lies to cover up the incident to Joe. Meanwhile, Bob suffers erectile dysfunction and is unable to have sex with Kelly. She mocks his inadequacy and leaves him. 

Dee Dee takes pity on Joe and allows him to audition for the role of a "sexy serial killer". He reads his lines with Tina and begins to improvise his dialogue. He impresses the producers and lands the role. Tina invites him out for a drink and he resists her advances with some difficulty. Mary meets with her therapist and tells him about her experience with her self-defense instructor. He tells her that she must become comfortable with men showing their attraction to her and begins sharing his own sexual fantasies about her. She storms out of the session. Meanwhile, Bob is negotiating a longtime contract on the soap opera, but Kelly continues to taunt him on set. Bob threatens to quit the show and then forces the producer to kill off Kellys character. 

Bob goes back to dating Sahara, with whom he is miserable. Joe breaks the big news about his role to Mary and they rejoice. Mary asks him if she is wrong for feeling angry when men hit on her. Joe supports her and threatens to beat up her therapist if he ever sees him again. They have sex for the first time in months and drift off to sleep, happy and satisfied. Mary wraps her hand around Joes finger, revealing that his improvised monologue had been about his feelings for her.

==Cast==
*Matthew Modine as Joe, an aspiring actor with strong opinions
*Catherine Keener as Mary, Joes longtime girlfriend and a successful makeup artist
*Maxwell Caulfield as Bob, an actor with a fetish for natural blondes
*Daryl Hannah as Kelly, a blonde soap-opera star 
*Bridgette Wilson as Sahara, a naive model with dyed blonde hair
*Marlo Thomas as Blair, a fashion photographer and Marys boss
*Kathleen Turner as Dee Dee Taylor, a prominent talent agent
*Elizabeth Berkley as Tina, an aspiring actress who works as Madonnas double.
*Denis Leary as Doug, Marys self-defense instructor
*Steve Buscemi as Nick, the director for Madonnas video
*Dave Chappelle as Zee, the assistant director for Madonnas video
*Christopher Lloyd as Ernst, a head waiter and Joes boss
*Beatrice Winde as Wilma

Maxwell Caulfield does, in fact, have a background in soap operas, appearing as Miles Colby in Dynasty (TV series)|Dynasty and its prime-time spin-off The Colbys.

Bridgette Wilson guest starred in four episodes of Saved by the Bell the sitcom which starred Elizabeth Berkley and launched her acting career.

==Reception==

The movie gained poor reviews.  

===Box office===

The movie was not a success. 

== References ==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 