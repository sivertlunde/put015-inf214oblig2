W.W. and the Dixie Dancekings
{{Infobox film
| name           = W.W. and the Dixie Dancekings
| image          = W.W. and the Dixie Dancekings.jpg
| image_size     = 
| alt            = 
| caption        = Movie Poster
| director       = John G. Avildsen
| producer       = Stanley C. Canter Steve Shagan
| writer         = Thomas Rickman
| narrator       = 
| starring       = Burt Reynolds Conny Van Dyke Ned Beatty Jerry Reed Art Carney
| music          = Dave Grusin
| cinematography = James Crave
| editing        = Richard Halsey Robbe Roberts
| studio         = 
| distributor    = 20th Century Fox
| released       = May 21, 1975 (USA)
| runtime        =  91 minutes
| country        = United States English
| budget         = $2,805,000 
| gross          = $17 million 
| preceded_by    = 
| followed_by    = 
}}
W.W. and the Dixie Dancekings is a 1975 film directed by John G. Avildsen, starring Burt Reynolds, and written by Thomas Rickman.  The 20th Century Fox film features the acting debuts of Jerry Reed and Brad Dourif.

As of 2013, the film has not been released on DVD or any other home video format.

==Plot== gas stations. He meets the Dixie Dancekings, a country music band, while fleeing from a policeman. Dixie (Conny Van Dyke), their singer, gives him an alibi. He claims to be in the music business, and ends up promoting the group. Wayne (Jerry Reed), the bands leader, does not trust him, but the others all have faith in him. 

W.W. only steals from SOS gas stations, so the companys chairman sends for Bible-thumping ex-lawman Deacon John Wesley Gore (Art Carney) to track him down. Meanwhile, W.W. and the newly outfitted band  go to see Country Bull (Ned Beatty), a highly successful singer-songwriter. He is willing to write them a song for $1000. 

W.W. talks the Dancekings into a bank robbery (SOS has just opened a bank branch) that does not work out quite as planned. When Gore broadcasts the description of the getaway car on a radio revival show, W.W. burns up his car. He is ready to separate from the Dancekings in order to shield them, but then he hears them rehearsing Waynes new song. He persuades Country Bull to listen to it; the man is so impressed, he puts them on the Grand Ole Opry. There Gore catches W.W. using an exact replica of his burnt car as bait. Gore makes him drive to the police department, but just as they arrive, Gore realizes it is now Sunday, so rather than violating the Sabbath, he lets him go.

=="Golden Anniversary" Oldsmobile==
  Oldsmobile Rocket 88 of which only 50 were made. It is a four-door sedan painted gold with black hood and side accents, with chrome trim.

In reality, there was no such special car, and 1955 was not the 50th anniversary for Oldsmobile. Three were custom-built for the movie. 

One was destroyed in the fire scene, one was taken to a museum, and the third was used as the camera car, with the roof removed.

==Cast==
{| class="wikitable" border="1.5"
|-
! Actor
! Role
|-
| Burt Reynolds || W.W. Bright
|-
| Conny Van Dyke || Dixie
|-
| Jerry Reed|| Wayne
|-
| Ned Beatty || Country Bull
|- James Hampton || Junior
|-
| Don Williams || Leroy
|-
| Rick Hurst || Butterball
|-
| Mel Tillis || GOB
|-
| Furry Lewis || Uncle Furry
|-
| Art Carney || Deacon John Wesley Gore
|}

==Critical reception==
Vincent Canby of The New York Times enjoyed the film: 
 

==Box office==
The film earned North American rentals of $8 million. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 