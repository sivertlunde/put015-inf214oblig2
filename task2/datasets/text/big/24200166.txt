The Night of the Party
 
 
{{Infobox film
| name           = The Night of the Party
| image          = 
| image_size     = 
| caption        = 
| director       = Michael Powell Jerome Jackson
| writer         = Roland Pertwee
| narrator       =  Ian Hunter Jane Baxter  Ernest Thesiger
| distributor    = Gaumont British
| released       = 16 July 1935
| runtime        = 61 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British mystery mystery thriller Ian Hunter, Jane Baxter, Ernest Thesiger and Malcolm Keen. In the United States it was released as The Murder Party.   

==Synopsis==
After inviting guests to a dinner party the ruthless press baron Lord Studholme is found murdered during a party game. The investigating detectives have to work out which of the guests had the motive to murder him. 

==Cast==
* Leslie Banks - Sir John Holland Ian Hunter - Guy Kennington
* Jane Baxter - Peggy Studholme
* Ernest Thesiger - Chiddiatt
* Viola Keats - Joan Holland
* Malcolm Keen - Lord Studholme
* Jane Millican - Anna Chiddiatt
* Muriel Aked - Princess Amelta
* Lawrence Anderson - Defence counsel

==References==
 

== External links ==
* 
* 
* 
*   at the  

 
 
 
 
 
 
 
 
 

 