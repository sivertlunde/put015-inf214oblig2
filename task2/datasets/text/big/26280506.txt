The Art of Living (film)
{{Infobox film
| name           = The Art of Living
| image          = 
| caption        = 
| director       = Julio Diamante
| producer       = Peter Carsten
| writer         = Julio Diamante Elena Sáez
| starring       = María del Carmen Abreu
| music          = 
| cinematography = Luis Enrique Torán
| editing        = Pedro del Rey
| distributor    = 
| released       = June 1965 (Berlin) 26 July 1966 (Spain)
| runtime        = 85 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

The Art of Living ( ) is a 1965 Spanish drama film directed by Julio Diamante. It was entered into the 15th Berlin International Film Festival.   

==Cast==
* María del Carmen Abreu - Julia Smeyers
* Anastasio Alemán - Psychologist
* Antonio Buero Vallejo - Father of Pupil of Luis
* Julio Diamante - Comrade of Luis
* Beatriz Galbó
* Juan Luis Galiardo - Juanjo
* Lola Gaos - Mother of Luis
* Luigi Giuliani - Luis
* Montserrat Julió - Anas Sister
* Lola Losada
* Sergio Mendizábal - Comrade of Luis Carlos Muñiz
* Lauro Olmo
* José María Prada - Galvez
* Fernando Sánchez Polack
* Manuel Summers - Adversary
* Elena María Tejeiro - Ana
* Paco Valladares - Santiago

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 