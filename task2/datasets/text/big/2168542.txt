Breakin'
 
{{Infobox film
| name           = Breakin
| image          = Breakin movie poster.jpg
| caption        = Film poster
| director       = Joel Silberg
| writer         = screenplay Charles Parker Allen DeBevoise story Charles Parker Allen DeBevoise Gerald Scaife
| producer       = Allen DeBevoise David Zito Executive Producers: Menahem Golan Yoram Globus
| starring = {{Plainlist|
* Lucinda Dickey
* Shabba Doo Boogaloo Shrimp
* Ben Lokey
* Phineas Newborn III
* Christopher McDonald
}}
| music          = Michael Boyd Gary Remal
| cinematography = Hanania Baer
| editing        = Larry Bock Gib Jaffe Vincent Sklena
| distributor    = Metro-Goldwyn-Mayer|MGM/UA Entertainment Company (USA) Cannon Films (non-USA)
| released       = May 4, 1984
| runtime        = 90 min.
| country        = United States
| language       = English
| budget = $1.2 million Andrew Yule, Hollywood a Go-Go: The True Story of the Cannon Film Empire, Sphere Books, 1987 p47 
| gross          = Domestic: $38,682,707  Foreign: $18,774,000 Worldwide: $57,456,707
}} documentary entitled hip hop Boogaloo Shrimp, went straight from Breakin   and Enterin   to star in Breakin  . Ice-T has stated he considers the film and his own performance in it to be "wack". 

The music score featured the hits " .

Breakin was the final Cannon film production released by Metro-Goldwyn-Mayer|MGM/UA. After Breakin was released, MGM and Cannon Films dissolved their distribution deal, reportedly over the potentially X-Rated content in John Dereks film Bolero (1984 film)|Bolero and MGMs then-current rule of not releasing X-Rated material theatrically, forcing Cannon to become an in-house distribution company once again.  Because of the demise of the distribution deal, Breakin is considered to be the final financially profitable film released by Cannon Films.

==Plot== Street dancers, Adolfo Quiñones) poppers Popin Tour de France" by Kraftwerk.

==Cast==
*Lucinda Dickey as Kelly / Special K Adolfo Quiñones as Orlando / Ozone (as Adolfo Shabba-Doo Quiñones)
*Michael Chambers as Tony / Turbo (as Michael Boogaloo Shrimp Chambers)
*Ice-T as Rap Talker (as Ice T)
*Ben Lokey as Franco Christopher McDonald as James
*Phineas Newborn III as Adam
*Bruno Falcon as Electro Rock 1 (as Bruno Pop N Taco Falcon) Timothy Solomon Timothy Popin Pete Solomon)
*Ana Sánchez as Electro Rock 3 (as Ana Lollipop Sanchez)
*Cooley Jackson as Featured Street Dancer
*Richie Haglund as Background Break Dancer (as Lil "R")
*Peter Bromilow as Judge
*Scott Cooper as Judge
*Michel Qissi as Background Dancing Spectator (uncredited)
*Jean-Claude Van Damme as Background Dancing Spectator (uncredited) 
*Vidal Rodriguez as Hot Tot (as Vidal Lil Coco Rodriguez)

==Soundtrack==
The soundtrack of the film was released by Mercury Records in 1984.  The album contains the first performance on an album of rapper Ice-T.   (He had released some 12" singles previously.)

===Track listing===
#"Breakin... Theres No Stopping Us" by Ollie & Jerry – 4:34
#"Freakshow on the Dance Floor" by Bar-Kays – 4:42
#"Body Work" by Hot Streak – 4:22
#"99 ½" by Carol Lynn Townes – 4:02
#"Showdown" by Ollie & Jerry – 3:57
#"Heart of the Beat" by 3V – 4:18
#"Street People" by Fire Fox – Music by (Ollie & Jerry) 3:23
#"Cut It" by Re-Flex – 3:11 Rufus and Chaka Khan – 4:45
#"Reckless" by Ice-T – 3:57
 Tour de France" by Kraftwerk, "Boogie Down" by Al Jarreau, and "Beatbox (song)|Beatbox" by Art of Noise.

==In popular culture== Adolfo "Shabba Michael "Boogaloo Timothy Solomon Prince song "I Feel for You".

==Home Video Releases==

On August 5, 2003, MGM Home Entertainment released Breakin as a bare-bones DVD.  On April 21, 2015,  .

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 