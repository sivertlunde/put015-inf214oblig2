Mrs. Miniver (film)
 
 
{{Infobox film
| name           = Mrs. Miniver
| image          = Mrs_Miniver_poster.gif
| alt            = 
| caption        = Theatrical release poster
| director       = William Wyler Sidney Franklin James Hilton Claudine West
| based on       =  
| starring       = Greer Garson Walter Pidgeon
| music          = Herbert Stothart
| cinematography = Joseph Ruttenberg
| editing        = Harold F. Kress
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 133 minutes  
| country        = United States
| language       = English German
| budget         = $1.34 million  . 
| gross          = $8,878,000 
}} romantic War war drama film directed by William Wyler, and starring Greer Garson and Walter Pidgeon. Based on the 1940 novel Mrs. Miniver by Jan Struther,    the film shows how the life of an unassuming British housewife in rural England is touched by World War II. She sees her eldest son go to war, finds herself confronting a German pilot who has parachuted into her idyllic village while her husband is participating in the Dunkirk evacuation, and loses her daughter-in-law as a casualty.

Produced and distributed by Metro-Goldwyn-Mayer, the film features a strong supporting cast that includes Teresa Wright, Dame May Whitty, Reginald Owen, Henry Travers, Richard Ney and Henry Wilcoxon.   
 Best Picture, Best Director Best Actress Best Supporting Actress (Teresa Wright).       In 1950, a film sequel The Miniver Story was made with Greer Garson and Walter Pidgeon reprising their roles. 
 American Film Institutes list celebrating the most inspirational films of all time. In 2009, the film was named to the National Film Registry by the Library of Congress for being "culturally, historically or aesthetically" significant and will be preserved for all time. 

==Plot==
Kay Miniver and her family live a comfortable life at a house called Starlings in Belham, a fictional village outside London. The house has a large garden, with a private landing stage on the River Thames at which is moored a motorboat belonging to her devoted husband Clem, a successful architect. They have three children: the youngsters Toby and Judy and an older son Vin at university. They have live-in staff: Gladys the housemaid and Ada the cook.

As World War II looms, Vin comes down from university and meets Carol Beldon, granddaughter of Lady Beldon from nearby Beldon Hall. Despite initial disagreements—mainly contrasting Vins idealistic attitude to class differences with Carols practical altruism—they fall in love. Vin proposes to Carol in front of his family at home, after his younger brother prods him to give a less romantic but more honest proposal. As the war comes closer to home, Vin feels he must "do his bit" and enlists in the Royal Air Force, qualifying as a fighter pilot. He is posted to a base near to his parents home and is able to signal his safe return from operations to his parents by cutting his engines briefly as he flies over the house. Together with other boat owners, Clem volunteers to take his motorboat to assist in the Dunkirk evacuation.
 German pilot hiding in her garden and he holds her at gunpoint. Demanding food and a coat, the pilot aggressively asserts that the Third Reich will mercilessly overcome its enemies. She feeds him, calmly disarms him when he collapses, and then calls the police. Soon after, Clem returns home, exhausted, from Dunkirk.

Lady Beldon visits Kay to try and convince her to talk Vin out of marrying Carol on account of her granddaughters comparative youth. Lady Beldon is unsuccessful and admits defeat when Kay reminds her that she, too, was young when she married her late husband. Lady Beldon concedes defeat and realizes that she would be foolish to try to stop the marriage. Vin and Carol are married; Carol has now also become Mrs Miniver, and they return from their honeymoon in Scotland. A key theme is that she knows he is likely to be killed in action, but the short love will fill her life. Later, Kay and her family take refuge in their Anderson shelter in the garden during an air raid, and attempt to keep their minds off the frightening bombing by reading Alices Adventures in Wonderland, which Clem refers to as a "lovely story" as they barely survive a bomb destroys parts of the house. They take the damage with nonchalance.

At the annual village flower show, Lady Beldon silently disregards the judges decision that her rose is the winner, instead announcing the entry of the local stationmaster, Mr. Ballard (Henry Travers), named the "Mrs. Miniver" rose, as the winner, with her own rose taking second prize. As air raid sirens sound and the villagers take refuge in the cellars of Beldon Hall, Kay and Carol drive Vin to join his squadron. On their journey home they witness fighter planes in a dogfight. For safety, Kay stops the car and they see the German plane crash. Kay realizes Carol has been wounded by shots from the plane and takes her back to Starlings. She dies a few minutes after they reach home. Kay is devastated. When Vin returns from battle, he already knows the terrible news. Unexpectedly he is the survivor, and she the one who gives her life for UK|Britain.

The villagers assemble at the badly damaged church where their vicar affirms their determination in a powerful sermon:

 

A solitary Lady Beldon stands in her familys church pew. Vin moves to stand alongside her, united in shared grief, as the members of  congregation rise and stoically sing "Onward, Christian Soldiers", while through a gaping hole in the bombed church roof can be seen flight after flight of RAF fighters in the V-for-Victory formation heading out to face the enemy.

==Cast==
 
* Greer Garson as Kay Miniver
* Walter Pidgeon as Clem Miniver
* Teresa Wright as Carol Beldon
* Dame May Whitty as Lady Beldon  
* Reginald Owen as Foley
* Henry Travers as Mr. Ballard
* Richard Ney as Vin Miniver 
* Henry Wilcoxon as the Vicar
* Christopher Severn as Toby Miniver
* Brenda Forbes as Gladys (Housemaid)
* Clare Sandars as Judy Miniver
* Marie De Becker as Ada
* Helmut Dantine as German flyer John Abbott as Fred
* Connie Leon as Simpson Rhys Williams as Horace
 

==Production==

===Screenplay===
The film went into pre-production in the autumn of 1940, when the United States was still a neutral country. The script was written over many months, and during that time the USA moved closer to war. As a result, scenes were re-written to reflect the increasingly pro-British and anti-German outlook of Americans. The scene in which Mrs. Miniver confronts a downed German flyer in her garden, for example, was made more and more confrontational with each new version of the script. It was initially filmed before the December 1941 attack on Pearl Harbor brought the USA into the war, but following the attack, the scene was filmed again to reflect the tough, new spirit of a nation at war. The key difference was that in the new version of the scene, filmed in February 1942, Mrs Miniver was allowed to slap the flyer across the face. The film was released 4 months later.   
 President Roosevelt as a morale builder and part of it was the basis for leaflets printed in various languages and dropped over enemy and occupied territory."  Roosevelt ordered it rushed to the theaters for propaganda purposes.   
 assault landing HMS Bideford to arrange a tow back to Dover, the ship had its stern blown off by a bomb dropped from a dive-bombing German aircraft. This must have been on Wilcoxons mind during the making of the film. 

==Reception==

===Critical response=== list of the most inspiring American films of all time. In 2009, Mrs. Miniver was named to the National Film Registry by the Library of Congress for being "culturally, historically or aesthetically" significant and will be preserved for all time.    The film was selected for the following reasons:
 

===Box office===
The film exceeded all expectations, grossing $5,358,000 in the US and Canada (the highest for any MGM film at the time) and $3,520,000 abroad. In the United Kingdom, it was named the top box office attraction of 1942. The initial theatrical release made MGM a profit of $4,831,000, their most profitable film of the year. 

Of the 592 film critics polled by American magazine Film Daily, 555 named it the best film of 1942. 

===Awards and nominations===
Mrs. Miniver won six Academy Awards in 1943.   
{| class="wikitable"
|-
! Award
! Result 
! Nominee
! Notes 
|- Outstanding Motion Picture
|  
| Metro-Goldwyn-Mayer 
| (Before 1951, this award was given to the production company instead of the individual producer.)
|- Best Director
|  
| William Wyler
|
|- Best Actor
|  
| Walter Pidgeon 
| Winner was James Cagney for Yankee Doodle Dandy
|- Best Actress
|  
| Greer Garson  
|
|- Best Writing, Screenplay
|   James Hilton, Claudine West, Arthur Wimperis
|
|- Best Supporting Actor
|  
| Henry Travers
| Winner was Van Heflin for Johnny Eager
|- Best Supporting Actress
|  
| Teresa Wright
|
|- Best Supporting Actress
|  
| May Whitty
| Winner was Teresa Wright for Mrs. Miniver
|- Best Cinematography, Black-and-White
|  
| Joseph Ruttenberg
|
|- Best Effects, Special Effects
|  
| A. Arnold Gillespie (photographic) Warren Newcombe (photographic) Douglas Shearer (sound) 
| Winner was Gordon Jennings, Farciot Edouart, William L. Pereira, Louis Mesenkop for Reap the Wild Wind
|- Best Film Editing
|  
| Harold F. Kress 
| Winner was Daniel Mandell for The Pride of the Yankees
|- Best Sound, Recording
|  
| Douglas Shearer 
| Winner was Nathan Levinson for Yankee Doodle Dandy
|}

==Sequel and adaptations== Trudy Warner on CBS Radio|CBS.   
* In 1950, a film sequel The Miniver Story was made with Garson and Pidgeon reprising their roles.
* In 1960, a 90-minutes television adaptation directed by Marc Daniels was broadcast on CBS, with Maureen OHara as Mrs. Miniver and Leo Genn as Clem Miniver.

==References==
Notes
 

Citations
 

Further reading
* Christensen, Jerome. "Studio Identity and Studio Art: MGM, Mrs. Miniver, and Planning the Postwar Era." ELH (2000) 67#1 pp: 257-292.  
* Glancy, Mark. When Hollywood Loved Britain: The Hollywood British Film (1999) 
* Koppes, Clayton R., and Gregory D. Black. Hollywood Goes to War: Patriotism, Movies and the Second World War from Ninotchka to Mrs Miniver (Tauris Parke Paperbacks, 2000)
* Short, K. R. M. "The White Cliffs of Dover: promoting the Anglo-American Alliance in World War II." Historical Journal of Film, Radio and Television (1982) 2#1 pp: 3-25.
* Troyan, Michael. A Rose for Mrs. Miniver: The Life of Greer Garson (2010)

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*   on Lux Radio Theater: December 6, 1943 Radio 4 - And The Academy Award Goes To ...  , Series 5 Episode 1

 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 