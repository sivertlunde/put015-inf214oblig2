Great Day in the Morning
{{Infobox film
| name           = Great Day in the Morning
| image          = Great-Day-in-the-Morning.jpg
| director       = Jacques Tourneur
| producer       = Edmund Grainger
| based on       =  
| screenplay     = Lesser Samuels
| starring       = Robert Stack Virginia Mayo Ruth Roman
| music          = Leith Stevens
| cinematography = William E. Snyder
| editing        = Harry Marker
| studio         = Edmund Grainger Productions, Inc.
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
}}

Great Day in the Morning is a Technicolor Superscope 1956 film.   It was directed by Jacques Tourneur and stars Robert Stack and Virginia Mayo in a story set in 1860s Denver.

==Plot== Civil War in the Colorado Territory, Owen Pentecost (Robert Stack) is a man from North Carolina who comes west to Denver on a whim. He encounters Ann Merry Alaine (Virginia Mayo), who is going there to open a dress shop.

In a Denver hotel Western saloon|saloon, Owen wins a poker game with the owner, who bet his estate on the last hand. Along with the hotel comes Boston Grant (Ruth Roman), who works there.
 Union town Civil War approaching, the town is split. Owen leads the Southerners in an escape attempt with the gold.

==Cast==
* Robert Stack as Owen
* Virginia Mayo as Ann Merry
* Ruth Roman as Boston
* Raymond Burr as Jumbo
* Alex Nicol as Capt. Kirby
* Regis Toomey as Father Murphy
* Leo Gordon as Zeff Masterson
* Carleton Young as Col. Gibson (as Carlton Young) Donald MacDonald as Gary John Lawford

==References==
 

==External links==
*  
*  
*  
*  

==See also== southern expression, e.g., James Brown used it in Get On Up (2014)
* "Great Day in the Morning", a song recorded by such artists as:
** Hoots & Hellmouth, on the album Salt (2012)
** The Hoppers, a gospel song on the album Great Day (2003)
** Brad Vickers & His Vestapolitans
* "Greatdayndamornin/Booty" (2012), a song by DAngelo

 

 
 
 
 
 
 
 
 
 
 
 


 