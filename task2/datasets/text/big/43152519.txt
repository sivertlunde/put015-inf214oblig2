Riyasat (film)
{{Infobox film
| name=Riyasat
| image= Poster_of_Riyasat.jpg
| alt=
| caption=Theatrical release poster
| director=Ashok Tyagi
| producer=Vijay Sirohi   Diwaker Singhal
| story= Vijay Sirohi
| starring=Rajesh Khanna Gauri Kulkarni, Aryan Vaid, Aryeman Ramsay   Raza Murad
| music=Hriju Roy  Syyed Ahmed
| cinematography=Akram Khan  
| editing = Irfan Shaikh
| studio = Landcraft   Vijay Sirohi Productions
| distributor=Manoj Nandwana of Jai Viratra Entertainment Limited
| released=  
| country=India
| language=Hindi
}}
Riyasat ( , Gauri Kulkarni, Aryan Vaid, Aryeman Ramsay and Raza Murad. The film is said to be the Indias first superstar Rajesh Khannas last film.  The film was released on 18 July 2014, on second death anniversary of Rajesh Khanna. 

==Cast==
* Rajesh Khanna as Saheb
* Gauri Kulkarni 
* Aryeman Ramsay as Vijay    
* Raza Murad as Minister
* Aryan Vaid as Shakti
* Vishwajeet Pradhan as Sandeep Marva

==Plot==
Riyasat is a story about Saheb who has established a kingdom. He is being considered the godfather for the people of city. Saheb become aware of some unsocial people entered into the city with no good intentions. There starts a war and Saheb protects his kingdom. But, this war ends up changing Sahebs life forever.  

==Production==
Riyasat is directed by Ashok Tyagi and it is jointly produced by Vijay Sirohi and Diwaker Singhal under Vijay Sirohi Productions.  Riyasat has been shot in Mumbai, Delhi and Noida.

==Reception==

===Critical response===
Riyasat generally received negative reviews from critics. Times of India rated the film 1.5/5.

Renuka Vyavhare of TOI said that, "If you are planning to watch this film solely for Rajesh Khanna, dont". The film makes an unsuccessful attempt to make scene intense by playing loud music every time Rajesh Khanna appears on screen.   Johnson Thomas of FreePressJournal said that the film is a desi wannabe of the Hollywood film The Godfather and, the narrative is hapless, very amateurishly executed.  Jyothi Venkatesh of the website Starblockbuster.com reviewed the film as lacking novelty.  Rediff said that the Rajesh Khanna looks unwell in the movie and seems unable to deliver dialogues given to him. The website also speculate that the film was completed after Rajesh Khannas death as there was found many repeated appearances of him and many dialogues of his shots delivered by some other person. 

===Box office===
Riyasat had an average release and collected 36 lakhs by the end of the opening day. 

==References==

 

==External links==
*  
*  

 
 