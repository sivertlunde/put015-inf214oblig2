Intlo Ramayya Veedilo Krishnayya
{{Infobox film
| name = Intlo Ramayya Veedilo Krishnayya
| image = chiruintlorveedhilok.jpg
| image_size =
| caption =
| director = Kodi Ramakrishna
| producer = K. Raghava
| writer = Kodi Ramakrishna
| screenplay = Kodi Ramakrishna
| story =
| based on =
| narrator = Madhavi Gollapudi Maruthi Rao
| music = J. V. Raghavulu
| cinematography = S.S.Lal
| editing = K.Balu
| studio =
| distributor =
| released =  
| runtime = 125 min.
| country = India
| language = Telugu
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}

  1982 Cinema Tollywood film, starring Chiranjeevi, Madhavi (actress)|Madhavi, and Gollapudi Maruthi Rao. Kodi Rama Krishna directed this film.It is debut movie for Kodi Rama Krishna and Gollapudi Maruthi Rao. The movie was remade in Tamil as Veetula Raman Veliyila Krishnan and in Hindi as Ghar Mein Ram Gali Mein Shyam (1988) with Govinda (actor)|Govinda, Neelam Kothari and Anupam Kher.

==Story==
Rajasekharam (Chiranjeevi), a civil engineer, comes to a village and falls in love with Jayalaxmi (Madhavi (actress)|Madhavi). They get married and move to a city. The neighbor Subba Rao (Gollapudi Maruthi Rao) casts an evil eye on Jayalaxmi and creates problems in their life. How everything gets solved is the rest of the film.

==Cast==
* Chiranjeevi - Rajasekharam Madhavi - Jayalaxmi
* Gollapudi Maruthi Rao - Subba Rao Poornima
* Annapoorna
* P. L. Narayana

==Production==
Kodi Ramakrishna, who earlier assisted Dasari Narayana Rao made his directorial debut with this film. Initially Kodi started a different project which he later shelved. Kodi took six months to complete the script. After the story was approved, producer Raghava insisted on Gollapudi playing the role of Subbarao. Initially he didn’t show interest but he finally agreed on. http://web.archive.org/web/20070104182957/http://www.telugucinema.com/tc/movies/intloramayyaveedhilokrishnayya_retro.php 

Kodi had Chiranjeevi in mind for the lead role. When he brought Chiru’s name to producer, he thought for a moment and replied that if Chiranjeevi agrees, he’s fine with the choice. Chiranjeevi was initially reluctant to play the role as he wanted Chandramohan to play the character, he liked the script still had doubts whether audience accepts him in such a soft role, Kodi convinced him to play the character. 

Most of the shooting was held in Palakollu and surroundings. Climax was shot in Chiranjeevi house and also in Antervedipalem. The boat journey in the climax was shot on Antervedi where River Godavari meets with Bay of Bengal. 

==References==
 

==External links==
*  

 
 
 

 
 