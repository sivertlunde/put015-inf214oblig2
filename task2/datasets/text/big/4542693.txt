Chronicle of the Years of Fire
{{Infobox film
| name           = Chronicle of the Years of Fire
| image          = Cronique_affiche.jpg
| caption        = original film poster
| director       = Mohammed Lakhdar-Hamina
| writer         = Rachid Boudjedra  Tewfik Fares Mohammed Lakhdar-Hamina
| starring       = Yorgo Voyagis Mohammed Lakhdar-Hamina Leila Shenna
| cinematography =Marcello Gatti
| distributor    = Arab Film Distribution
| released       =  
| runtime        = 177 minutes
| country        = Algeria
| language       = Arabic French
}}
Chronicle of the Years of Fire ( , Transliteration|translit.&nbsp;Chronique des Années de Braise) is a 1975 Algerian film directed by Mohammed Lakhdar-Hamina. It depicts the Algerian War of Independence as seen through the eyes of a peasant.
 Best Foreign Language Film at the 48th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Yorgo Voyagis - Ahmed
* Mohammed Lakhdar-Hamina - Le conteur fou
* Leila Shenna - La femme
* Cheikh Nourredine - Lami
* François Maistre - Le contremaître de la carrière

==See also==
* List of submissions to the 48th Academy Awards for Best Foreign Language Film
* List of Algerian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 
 