Prince of the City
{{Infobox film
| name           = Prince of the City
| image          = Prince_Of_The_City_folded.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Sidney Lumet
| producer       = Burtt Harris
| screenplay     = Sidney Lumet Jay Presson Allen
| story          = Robert Daley (book)
| narrator       = 
| starring       = Treat Williams Jerry Orbach Richard Foronjy Lindsay Crouse
| music          = Paul Chihara
| cinematography = Andrzej Bartkowiak
| editing        = Jack Fitzstephens
| distributor    = Orion Pictures Warner Bros.
| released       =  
| runtime        = 167 minutes
| country        = United States
| language       = English
| budget         = $8.6 million
| gross          = $8,124,257
| followed_by    = 
}}

Prince of the City (1981) is an American crime drama film about an NYPD officer who chooses to expose police corruption for idealistic reasons. The character of Daniel Ciello (played by Treat Williams) was based on real-life NYPD Narcotics Detective Robert Leuci  and the script was based on Robert Daleys 1978 book of the same name. The film was directed by Sidney Lumet and also featured Jerry Orbach.
 On Golden Pond.

==Story==
New York City cop Daniel Ciello (Treat Williams) is involved in some questionable police practices. He is approached by internal affairs, and federal prosecutors, to participate in an investigation of police corruption. In exchange for him potentially being let off the hook, Ciello is instructed to begin to expose the inner workings of illegal police activity and corruption. Danny agrees as long as he does not have to turn in his partners but he soon learns that he cannot trust anyone and he must decide whose side he is on and who is on his. 

==Cast==
*Treat Williams	 ...	Daniel Ciello
*Jerry Orbach	 ...	Gus Levy
*Richard Foronjy	 ...	Joe Marinaro
*Don Billett	 ...	Bill Mayo
*Kenny Marino	 ...	Dom Bando
*Carmine Caridi	 ...	Gino Mascone
*Tony Page	 ...	Raf Alvarez Norman Parker	 ...	Rick Cappalino
*Paul Roebling	 ...	Brooks Paige
*Bob Balaban	 ...	Santimassino
*James Tolkan	 ...	District Attorney Polito
*Steve Inwood	 ...	Mario Vincente
*Lindsay Crouse	 ...	Carla Ciello
*Matthew Laurance	 ...	Ronnie Ciello
*Tony Turco	 ...	Socks Ciello
*Ronald Maccone	 ...	Nick Napoli (as Ron Maccone)
*Ron Karabatsos	 ...	Dave DeBennedeto
*Tony DiBenedetto	 ...	Carl Alagretti
*Tony Munafo	 ...	Rocky Gazzo
*Robert Christian	 ...	The King Lee Richardson	 ...	Sam Heinsdorff
*Lane Smith	 ...	Tug Barnes
*Cosmo Allegretti	 ...	Marcel Sardino
*Bobby Alto	 ...	Mr. Kanter
*Michael Beckett	 ...	Michael Blomberg
*Burton Collins	 ...	Monty
*Henry Ferrentino	 ...	Older Virginia Guard
*Carmine Foresta	 ...	Ernie Fallacci
*Conard Fowkes	 ...	Elroy Pendleton
 
 
*Peter Friedman	 ...	D.A. Goldman
*Peter Michael Goetz	 ...	Attorney Charles Deluth
*Lance Henriksen	 ...	D.A. Burano Eddie Jones	 ...	Ned Chippy
*Don Leslie	 ...	D.A. DAmato
 
*Dana Lorge	 ...	Ann Mascone
*Harry Madsen	 ...	Bubba Harris
*E.D. Miller	 ...	Sergeant Edelman
*Cynthia Nixon	 ...	Jeannie
*Ron Perkins	 ...	Virginia Trooper
 
*Lionel Pina	 ...	Sancho
*José Angel Santana	 ...	José (as José Santana)

*Walter Brooke	 ...	Judge (uncredited) Alan King	 ...	Himself (uncredited)
*Ilana Rapp	 ...	Beach Player (uncredited)
 

==Production==
When producer and screenwriter Jay Presson Allen read Robert Daleys book, Prince of the City (1978), she was convinced it was an ideal Sidney Lumet project, but the film rights had already been sold to Orion Pictures for Brian De Palma and David Rabe. Allen let it be known that if that deal should fall through, then she wanted the picture for Sidney. Just as Lumet was about to sign for a different picture, they got the call that Prince of the City was theirs.

Allen hadnt wanted to write Prince of the City, just produce it. She was put off by the books non-linear story structure, but Lumet wouldnt make the picture without her, and agreed to write the outline for her. Lumet and Allen went over the book and agreed on what they could use and what they could do without. To her horror, Lumet would come in every day for weeks and scribble on legal pads. She was terrified that she would have to tell him that his stuff was unusable, but to her delight the outline was wonderful and she went to work.  It was her first project with living subjects, and Allen interviewed nearly everyone in the book and had endless hours of Bob Leuci’s tapes for back-up. With all her research and Lumets outline, she eventually turned out a 365-page script in 10 days.  It was nearly impossible to sell the studio on a three-hour picture, but by offering to slash the budget to $10 million they agreed. (When asked if the original author ever has anything to say about how their book is treated, Allen replied: "Not if I can help it. You cannot open that can of worms. You sell your book, you go to the bank, you shut up." )

Orion Pictures had bought Daleys book for $500,000 in 1978. {{cite news
 | last = Harmetz
 | first = Aljean
 | coauthors =
 | title = How Prince of the City is being "platformed"
 | work =
 | pages =
 | language =
 | publisher = New York Times
 | date = July 18, 1981
 | url = 
 | accessdate = }}  Daley was a former New York Deputy Police Commissioner for Public Affairs who wrote about Robert Leuci, an NYPD detective whose testimony and secret tape recordings helped indict 52 members of the Special Investigation Unit and convict them of income tax evasion.  Originally, Brian De Palma was going to direct with David Rabe adapting the book {{cite news
 | last = Ansen
 | first = David
 | coauthors =
 | title = New Yorks Finest
 | work =
 | pages =
 | language =
 | publisher = Newsweek
 | date = August 24, 1981
 | url = 
 | accessdate = }}  and Robert De Niro playing Leuci but the project fell through. {{cite web
 | last = Corry
 | first = John
 | coauthors =
 | title = Prince of the City Explores a Cops Anguish
 | work =
 | pages =
 | language =
 | publisher = The New York Times
 | date = 1981-08-09
 | url = http://query.nytimes.com/gst/fullpage.html?res=9D07E1DA143BF93AA3575BC0A967948260
 | accessdate = 2008-06-09}}   Sidney Lumet came aboard to direct under two conditions: he did not want a big name movie star playing Leuci because he did not "want to spend two reels getting over past associations,"  and the movies running time would be at least three hours long. 

Lumet cast Treat Williams after spending three weeks talking to him and listening to the actor read the script and then reading it again with 50 other cast members. {{cite news
 | last = Scott
 | first = Jay
 | coauthors =
 | title = Director Sidney Lumet Fears for the Future of "Real" Films
 | work =
 | pages =
 | language =
 | publisher = Globe and Mail
 | date = August 19, 1981
 | url = 
 | accessdate = }}  In order to research the role, the actor spent a month learning about police work, hung out at 23rd Precinct in New York City, went on a drug bust and lived with Leuci for some time. {{cite news
 | last = Lawson
 | first = Carol
 | coauthors =
 | title = Treat Williams: For the moment, Prince of the City
 | work =
 | pages =
 | language =
 | publisher = New York Times
 | date = August 18, 1981
 | url = 
 | accessdate = }}  By the time rehearsals started, Williams said, "I was thinking like a cop." 

Lumet felt guilty about the two-dimensional way he had treated cops in the 1973 film Serpico and said that Prince of the City was his way to rectify this depiction.  He and Jay Presson Allen wrote a 240-page script in 30 days.   The film was budgeted at $10 million but the director was able to bring it in for under $8.6 million. 

==Distribution==
Orion decided to open the film initially in select theaters in order to allow good reviews and word-of-mouth to build.  They were unable to buy television advertising because of the cost and relied heavily on print ads, including an unusual three-page spread in the New York Times. 

==Reception==
The film was considered authentic enough by the head of the Drug Enforcement Administration that he called Lumet for a copy of the movie for the DEA training program. Some law enforcement officials, however, criticized the film for glamorizing Leuci and other corrupt detectives while portraying most of the prosecutors who uncovered the crimes negatively. {{cite news
 | last = Raab
 | first = Selwyn
 | coauthors =
 | title = Movie criticized as glamorizing police corruption
 | work =
 | pages =
 | language =
 | publisher = New York Times
 | date = August 30, 1981
 | url = 
 | accessdate = }}  John Guido, Chief of Inspectional Services said, "The corrupt guys are the only good guys in the film."  Nicholas Scoppetta, the Special Prosecutor who helped convince Leuci to go undercover against his fellow officers, said, "In the film, it seems to be the prosecutors who are disregarding the issue of where real justice lies and the prosecutors seem to be as bad or worse than the corrupt police."  In fact, only two of the five prosecutors the film focuses on were portrayed negatively. In particular District Attorney Polito, played by James Tolkan, is shown as petty and vindictive. The character is based on Thomas P. Puccio, the assistant United States Attorney in charge of the Federal Organized Crime Strike Force in Brooklyn, and Robert Daley agrees that he was treated unfairly in the screenplay.

Interestingly, one of the prosecutors who befriended the Ciello character and is shown in a very positive light was based on then rookie federal prosecutor Rudolph Giuliani. The character, Mario Vincente, played by Steve Inwood, is portrayed as threatening to resign if the U.S. Attorneys office indicts Ciello (Leuci) for past transgressions. In general, the prosecutors who argued against the prosecution of Leuci are treated sympathetically, while those who sought his indictment are shown as officious and vindictive.

The initial release of Prince of the City garnered both positive and negative reviews, some the latter complaining of what was considered its excessive length. The film was not commercially successful in its theatrical release. The film currently (December 2010) scores a 94% (among 16 reviews) on the review aggregator Rotten Tomatoes.

==Honors==
*Nominated Best Adapted Screenplay Academy Award (Jay Presson Allen, Sidney Lumet)
*Nominated Best Director (Sidney Lumet), Best Picture, Best Actor (Treat Williams) Golden Globe Awards
*Nominated Best Picture, Edgar Allan Poe Awards
*Winner Best Director (Sidney Lumet), Kansas City Film Critics Circle
*Selected Top Ten Films of the Year, National Board of Review
*Nominated Best Supporting Actor (Jerry Orbach), National Society of Film Critics
*Winner Best Director (Sidney Lumet), New York Film Critics Circle
*Nominated Best Film, Best Screenplay (Jay Presson Allen, Sidney Lumet), Best Supporting Actor (Jerry Orbach), New York Film Critics Circle
*Winner Best Film, Venice Film Festival
*Nominated Best Adapted Screenplay (Jay Presson Allen, Sidney Lumet), Writers Guild of America

==References==
===Citations===
 
===Other sources===
*Prince of the City: The Real Story (2006), 30-minute featurette on the main films DVD

== External links ==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 