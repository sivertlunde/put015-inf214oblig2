Pavithram
{{Infobox Film
| name           = Pavithram
| image          = Pavithram.jpg
| image_size     = 
| caption        = DVD cover
| director       = T. K. Rajeev Kumar
| producer       = Thankachan
| screenplay         = P. Balachandran  story = P. Balachandran  T. K. Rajeev Kumar
| narrator       =  Sreenivasan  Innocent
| music          = Sharreth
| cinematography = Santhosh Sivan
| editing        = Venugopal
| studio    = Vishudhi Films
| distributor = Jubilee Pictures 
| released       = 1994
| runtime        = 150 minutes 
| country        = India Malayalam
}}
  Malayalam drama film directed by T. K. Rajeev Kumar. The film stars Mohanlal, Shobana, Vinduja Menon, Thilakan, Srividya, Sreenivasan (actor)|Sreenivasan, Nedumudi Venu and Innocent Vincent|Innocent. The music was composed by Sharreth and the cinematography was by Santhosh Sivan.

The film deals with the relationship between siblings Unnikrishnan (Mohanlal) and Meenakshi (Vinduja Menon) who have an almost 30 year age difference between them. 
 Filmfare Award for Best Actor. 

==Plot==
The movie begins with an older mans dream. In this dream, Unni (played by Mohanlal) is roused from his house in haste and told to come along to see his father (someone Unni has longed believed to be dead) and upon reaching the destination, he is greeted by a crowd of people who begin to make fun of him and Unni wakes up. He consults an astrologer who interprets the dream as a harbinger of Easwara Pillais (Unnis father) death and advices Unni to do the last rites for his dead father. During the rituals, Unni finds it difficult to go through the rituals as he doesnt believe that his father is dead and he walks away declaring that his father is not dead.

The story flashes back to Unnis youth when he was living in his ancestral home with his parents.  He is courting Meera, a beautiful woman with a penchant for books and stories.  Unni and Meera appear all set to be married soon and Meeras father wants Unni to take an MBA in UK to eventually take over the company responsibilities.  But Unni makes it clear that he has no interest in pursuing such a career and his dreams are very simple.  

Unni has a brother Ramakrishnan (Sreenivasan) who is a doctor living in the city with his wife.  Devaki (Unnis mother) wants to have a grandchild and she is concerned as Ramakrishnan has been married a few years without children.  In her desire to become a grandmother she visits many temples and holy functions to pray for a grandchild.  But in a twist of fate an amorous relation takes a second bud between Devaki and her husband Easwarapilla and she becomes pregnant.  The parents are ashamed at the first signs of the pregnancy and worry about how their grown children will react to this news.  But they are surprised by the enthusiasm of Unni and the mature and supportive attitude of Ramakrishnan.

Devaki gives birth to a girl but dies during childbirth.  Ramakrishnans wife doesnt seem to be interested in children much less in one she considers to be a source of shame.  In the absence of anyone to take care of the child Unni steps in to take care of the child.  Soon after in a heated moment, Unni scolds his father and Easwarapilla leaves town as he is devastated by the recent happenings.  This leaves Unni with the double responsibility of being a brother as well as father to Meenakshi.  Unni breaks off his relationship with Meera upon Meeras dads request as he feels the child will always be a source for problems in their relationship.  Unni goes ahead with it as he feels Meenakshi needs him and theres no one else to take that role.  

The story now comes back to the present as Meenakshi is a grown girl preparing for her 10 std exams. Unni has regressed to a village life after he chose to play Meenakshis brother/father.  Over the years Unni has become possessive of Meenakshi as she has been the center of his existence.  When it comes time for Meenakshi to go to the city and stay in a hostel to continue her studies, Unni goes through a trying period before finally coming to terms with the inevitable.  In the city, Meenakshi is exposed to a new culture and fashions that she wasnt aware of growing up in the village and she becomes enamored in it.  

Meenakshi becomes involved in the city lifestyle, even meeting with Ramakrishnans wife. This causes friction between her and Unni leading to fights and hostile encounters when they meet. Meenakshi moves to the city and Unni follows her to apologize but sees her involved in an accident.

Meenakshi survives the accident and Ramakrishnan chastises her for ignoring Unni who gave up his dreams for her. Meenakshi repents and goes back to the village to meet with Unni where she realizes that he has become mentally imbalanced after the trauma of seeing her in the accident and she decides to spend the rest of her life taking care of him.

==Cast==
* Mohanlal - Unnikrishnan Shobana - Meera
*Vinduja Menon - Meenakshi
* K. P. A. C. Lalitha - Punchiri Innocent - Erussery
* Thilakan - Easwara Pillai
* Srividya - Devakiyamma
* Sreenivasan (actor)- Dr.Ramakrishnan
* Narendra Prasad - Sankaran Pillai
* Nedumudi Venu - Warrier Santhakumari
* Renuka - Dr.Ramakrishnans wife Rudra

==Music== Carnatic raga Yesudas and Sujatha Mohan|Sujatha. The film is rated a 7.2 in IMDB and is still considered as one of the best movies done by Mohanlal.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s) !! Raaga  
|-
| Kannil Pedamaaninte Sujatha
| 
|-
| Parayoo Nin Hamsa
| K. J. Yesudas|Dr. K. J. Yesudas
| Subhapanthuvarali
|-
| Parayoo Nin Hamsa
| K. S. Chithra
| Subhapanthuvarali
|-
| Sreeraagamo
| K. J. Yesudas|Dr. K. J. Yesudas

| Kharaharapriya
|-
| Thaalamayanju Sujatha
| Dwijavanthi, Madhyamavati
|-
| Vaalinmel Poovum Sujatha
| Kalyani
|}

==References==
 

==External links==
*  

 
 
 