The Bride Wore Red
 
 
{{Infobox film
| name           = The Bride Wore Red
| image          = The-Bride-Wore-Red -1937.jpg
| image size     = 
| caption        = Theatrical release poster
| director       = Dorothy Arzner
| producer       = Joseph L. Mankiewicz
| writer         = Tess Slesinger Bradbury Foote
| based on       =   Robert Young Billie Burke
| music          = Franz Waxman
| cinematography = George J. Folsey
| editing        = Adrienne Fazan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $960,000  . 
| gross          = $1,200,000 
}}
 Robert Young and Billie Burke. It was based on the unproduced play The Bride from Trieste by Ferenc Molnár.  In this "rags to riches" tale, Crawford plays a cabaret singer who poses as an Aristocracy (class)|aristocrat.   This film was the last of seven Crawford and co-star Franchot Tone (her then husband) would make together.

==Plot==
  Robert Young) that the only thing separating aristocrats from peasants is luck. Later, in a waterfront cafe, he decides to prove his point by offering the clubs singer, Anni Pavlovitch (Joan Crawford), money and a wardrobe to stay at an upper class resort hotel in the Alps for two weeks and pose as his friend Anne Vivaldi, an aristocrats daughter. When Anni first arrives, she meets Giulio (Franchot Tone), a philosophical postal clerk who has no desire for wealth. She also meets her old friend Maria (Mary Philips), who is happy being a maid in the hotel and warns Anni not to become the victim of Armalias joke on his friends.

That evening, Anni attracts the attention of Rudi, who is dining with his fiancée, Maddalena Monti (Lynne Carver), her father, Admiral Monti (Reginald Owen), and Contessa di Meina (Billie Burke). Rudi begins to fall in love with Anni, but she is more attracted to Giulio. Hoping to lure Rudi into proposing to her, Anni extends her stay beyond the two weeks while the Contessa, who has been suspicious of her from the beginning, wires Armalia for information on her. When the reply comes through the post office, Giulio reads it and learns the truth, but on the way to deliver it, he meets Anni, who goes to his cottage and realizes that she loves him, but marriage to Rudi would bring the material wealth she craves. Later, she falls and Giulio loses the telegram going to help her.

On the evening of an annual costume party at which the hotel guests dress as peasants, Anni snubs Giulio when he offers her flowers, but later confesses her love. She still plans to marry Rudi, though, whom she has finally gotten to propose, after refusing to be his mistress. The next day, Rudi tells Maddalena that he is in love with Anni and she steps aside, then suggests that they dine together that evening. While Maria helps Anni pack, she tells her that she no longer has a heart and that the gaudy red beaded dress she plans to wear is what she is really like. During dinner, Giulio delivers a copy of the telegram to the Contessa, who shows it to Rudi and the others. Maddalena is genuinely sympathetic, and Anni tells Rudi that he should marry his childhood sweetheart because she really is a lady. Finally, after being comforted by Maria, Anni realizes that Rudi did the right thing and she leaves the hotel after the manager demands payment of her bill. When she leaves, taking only her peasant costume from the ball, Giulio is happily waiting for her.

==Cast==
* Joan Crawford as Anni Pavlovitch
* Franchot Tone as Giulio Robert Young as Rudi Pal
* Billie Burke as Contessa di Meina
* Reginald Owen as Admiral Monti
* Lynne Carver as Maddelena Monti
* George Zucco as Count Armalia
* Mary Philips as Maria
* Paul Porcasi as Signor Nobili Dickie Moore as Pietro
* Frank Puglia as Alberto
* Adriana Caselotti as First Peasant Girl
* Ann Rutherford as Third Peasant Girl

==Reception==
Howard Barnes of the New York Herald Tribune wrote,
"Joan Crawford has a glamorous field day in The Bride Wore Red.... With a new hair-do and more wide-eyed than ever, she plays at being a slattern, a fine lady, and a peasant with all of the well-known Crawford sorcery. It is not entirely her fault that she always remains herself.   has no dramatic conviction and little of the comic flavor that might have made it amusing though slight. Your enjoyment of it will depend on how much of Miss Crawford you can take at one stretch.... The direction of Dorothy Arzner is always interesting and sometimes...is extraordinarily imaginative, but here she has not been able to give a vapid Cinderella pipe dream more than a handsome pictorial front."

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 