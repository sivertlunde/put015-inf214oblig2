Land of the Minotaur
{{Infobox Film
| name           = Land of the Minotaur
| image          = 
| image_size     = 
| caption        = 
| director       = Kostas Karagiannis
| producer       = Frixos Constantine	
| writer         = Arthur Rowe
| narrator       = 
| starring       = Donald Pleasence Peter Cushing
| music          = Brian Eno
| cinematography = 
| editing        = 
| distributor    = Crown International Pictures (USA)
| released       = August 13, 1976
| runtime        = 95 min.
| country        = Greece English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Land of the Minotaur (The Devil’s Men original title) is a 1976 Greek horror film directed by Kostas Karagiannis.

==Plot== Greek archeological Irish priest Father Roche (Donald Pleasence) enlists the help of former pupil and a private detective to find out what has happened to them.

==Cast==
* Donald Pleasence as Father Roche
* Peter Cushing as Baron Corofax
* Luan Peters as Laurie Gordon
* Kostas Karagiorgis (credited as Costas Skouras) as Milo Kaye
* Fernando Bislamis (credited as Dimitris Bislanis) as Sgt Vendris
* George Venlis as Max
* Vanna Reville as Beth
* Nikos Verlekis as Ian
* Robert Behling (credited as Bob Behling) as Tom Gifford
* Anna Matzourani as Mrs. Mikaelis
* Anestis Vlakos as Shopkeeper - Karapades
* Jane Lyle as Milos Girlfriend
* Jessica Dublin (credited as Jessica) as Mrs. Zagros

=== Cast notes ===
Cushing and Pleasence previously appeared together in the 1954 BBC Television teleplay adaptation of Nineteen Eighty Four. Island of Death, which was also filmed in Greece.

==Production==
The films production and filming was officially completed in November 1975 but was copyrighted the following year in 1976.

== External links ==
*  

 
 
 
 
 
 

 