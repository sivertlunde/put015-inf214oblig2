Sawan Ko Aane Do
{{Infobox Film
| name           = Sawan Ko Aane Do
| image          = Sawan ko aane do.jpg
| image_size     = 133 × 188 pixels, file size: 10 KB
| caption        = Sawan Ko Aane Do
| director       = Kanak Mishra
| producer       = Tarachand Barjatya
| writer         = 
| narrator       = 
| music          = Raj Kamal
| starring       = Arun Govil Amrish Puri Zarina Wahab Rita Bhaduri
| cinematography = 
| editing        = 
| distributor    = Rajshri Productions Pvt. Ltd.
| released       = 1979
| runtime        = 
| country        =   India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Sawan Ko Aane Do (Let the monsoon arrive) is a 1979 Rajshri Productions Hindi movie produced by Tarachand Barjatya and directed by Kanak Mishra. The film was noted for its enchanting songs and charismatic performance by Arun Govil.
 Jaspal Singh, Kalyani Mishra, and Anand Kumar C.

The film was shot mostly in the interior and outskirts of the city Lucknow.

PLOT:
The movie captures the very heart of rural India.The story is quite simple like most Rajshri production movies.The plot starts off with chandramukhi,the city educated daughter of the village zamindar arriving at ramnagar,a typical village.There she meets her childhood friend birju aka brij mohan.He has a lovely voice and she encourages him to pursue it.His brothers love him very much and he is often excused for not working but his sisters in law hate him for it.He has a young niece who likes him a lot.Eventually brij and chandramukhi fall in love.But chandramukhis father cuts it abruptly and sends her to lucknow.A short while later brij arrives at lucknow.Gitanjali who happens to be chandramukhis friend is thrilled by his voice and helps him get a job at aakashvani through her father.brij returns to ramnagar to ask chandramukhis hand in marriage but is once again rebuked.By this time her family has lost its wealth in a court case and she becomes a teacher at the local school.He returns and goes on to become a great singer in mumbai with gita by his side.All through he yearns for chandra as she yearns for him.how he manages to marry her forms the rest of the story. 

==Cast==

* Amrish Puri as Chandrmukhis Dad
* Arun Govil as Brij Mohan/Birju
* Zarina Wahab as Chandrmukhi
* Rita Bhaduri as Gitanjali
* Yunus Parvez as Balu
* Leela Mishra as Amma

== Songs From The Movie ==
{| class="wikitable"
|-
! Songs || Singers
|-
| Sawan Ko Aane Do ||  Jaspal Singh, Kalyani Mitra  
|-
| Teri tasveer ko ||  Yesudas
|-
| Tujhe dekh kar ||  Yesudas
|-
| Chaand jaise mukhde pe  ||  Yesudas
|-
| Jaanam Jaanam ||  Yesudas
|-
| Bole to basuri kahin ||  Yesudas
|-
| Gagan ye samjhe ||  Jaspal Singh
|-
| Patthar se sheesha ||  Anand Kumar C
|-
| Kajre ki baati ||  Sulaksana Pandit, Yesudas 
|-
| Tere bin soona ||  Yesudas
|}

== External links ==
*  
*  
*  

 
 
 
 


 