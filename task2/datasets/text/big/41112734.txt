The Constable
 
 
{{Infobox film
| name           = The Constable
| image          = TheConstable.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 衝鋒戰警
| simplified     = 冲锋战警
| pinyin         = Chōng Fēng Zhàn Jǐng 
| jyutping       = Cung1 Fung1 Zin3 Ging2 }} Dennis Law
| producer       = Dennis Law
| writer         = Dennis Law Sam Lee Maggie Shiu Niu Mengmeng Wang Ziyi Maggie Li Ken Lo
| music          = Tommy Wai
| cinematography = Ko Chiu Lam
| editing        = Yau Chi Wai Point of View Movie Production Star Alliance Movies (Beijing) China Film Co-production Corporation
| distributor    = Newport Entertainment
| released       =  
| runtime        = 96 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}} Hong Kong action crime Dennis Law and starring Simon Yam.

==Cast==
*Simon Yam as Lam Kwok Kuen, a police officer whose sole requirement on the job is oversee the department fleet. Despite this, he remains active in the front line of police service, putting his life on the line for sake of others, his heroism bettering even the most seasoned professionals.
*Lam Suet as Lok Wah Wai, nicknamed Fat Wai, good friend and colleague of Lam for years. Despite his weight, he is the renowned marksman of the police force  Sam Lee as Chow Kong Hong, a small time crook living with his uncle in Shenzhen who mistakenly accused his girlfriend Tsui Yan for having an affair with Lam. He is later arrested in Hong Kong due his involvement in an armed bank robbery.
*Maggie Shiu as Station Sergeant Hui Chun, an outstanding police officer and good friend of Lam 
*Niu Mengmeng as Lin Tsui Yan, who is employed by Lam to look after his mentally challenged son Chung. Yan loving cared for Chung, but this led to the misunderstanding of his boyfriend Chow that she has been two timing with Lam. Yan later left Chow for her hometown after Chung died. Flying Tiger.
*Maggie Li as Fa Fa, a WPC under Hui and the object of Luis affection. She was unimpressed initially by Lui until she witnessed his daring feat during a police gun fight.
*Ken Lo
*Li Jinjiang as Lam Wing Chung, nicknamed Little Chung, Lams down syndrome son. He is well cared and loved by his father despite Chungs health condition. Since birth, Chung has lived a happy life.
*Mia Chen
*Wang Siya
*Lisa Cheng
*Lo Hoi-pang (guest star)
*Cheung Siu-fai (guest star)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 