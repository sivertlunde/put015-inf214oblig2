The Seat Filler
{{Infobox film
| name=The Seat Filler
| image=Theseatfiller.jpg
| director=Nick Castle
| executive producers=Will Smith Jada Pinkett Smith
| writer= Mark Brown Duane Martin Tisha Campbell-Martin
| starring=Duane Martin Kelly Rowland Melanie Brown DeRay Davis Shemar Moore Kyla Pratt Glynn Turman Sy Smith
| executive producer(s)=Christine Crokos Michael LaFetra Jada Pinkett Smith Will Smith
| music=
| cinematography=Suki Medencevic
| editing=Patrick Kennedy Julia Wong
| distributor=Magnolia Home Entertainment The Momentum Experience
| released= 
| runtime=90 minutes
| language=English
| budget=$8 million
| gross=$17.9 million
}}
 romantic comedy musical film produced by the Company Production of Magnolia Home Entertainment, Seat Filler Productions, Shake Martin Films and Strange Fruit Films, which released in 2004. It stars Kelly Rowland, Duane Martin, Melanie Brown, Kyla Pratt and Shemar Moore. The movie received mainly average reviews.

The film is also recognized as Duane Martins first (and to date, only) appearance in a musical film and Kelly Rowlands most successful film in a leading role performance.

==Plot==
The romantic comedy The Seat Filler stars Duane Martin as a law student working very hard to keep his head above water financially. He takes a job as a seat filler at a big awards function where he meets a famous singer (Kelly Rowland). She believes he works in show business and he does nothing to disabuse her of this notion. That leads to him having to go to larger and larger measures to keep his lie going when their relationship continues.

==Lead roles==
* Derrick (Duane Martin), is a down-but-not-quite-out law student struggling to pay the bills and get his homework in on time. In order to earn some much needed cash, Derrick takes a cushy evening job as a "seat filler", which requires him to attend awards shows around Hollywood sitting in vacant chairs so that when the cameras show the audience, it is always full of happy applauding people. Its not allowed for the seat fillers to converse with the actual celebrities in attendance, but one day, Derrick strikes up a conversation with a burgeoning pop star named Jhnelle.
* Jhnelle (Kelly Rowland), an attractive and talented singer who is immediately drawn to Derrick. She mistakes him for an industry big-shot, and the two soon begin dating, but despite her maintenance that she isnt above a relationship with an average Joe, Derrick is in too deep to back out of his assumed persona. Now that they have feelings for each other, Derrick tries to find some way to tell her who he really is in a way that she might accept, but an easy solution doesnt appear possible.  

==Casting==
The Seat Filler should have additional appeal to R&B/pop fans that enjoy Rowlands work as a solo artist, as well her involvement as a member of the popular group, Destinys Child. Melanie Brown (LD 50 Lethal Dose, Spice World), aka Mel B, aka Scary Spice of the Spice Girls, also has a sizable role as Jhnelles best friend and assistant, Sandie. Actress/singer Tisha Campbell, wife of star Duane Martin, receives a co-writing credit, while Will Smith and Jada Pinkett-Smith serve as executive producers. R&B star Chante Moore makes a brief stage appearance as well.

==Cast==
* Duane Martin - Derrick
* Kelly Rowland - Jhnelle
* DeRay Davis - E.J. (Derricks Best Friend)
* Patrick Fischler - (Jhnelles Assistant)
* Melanie Brown - Sandie (Jhnelles Assistant/Best Friend)
* Shemar Moore - Trent (Jhnelles Boyfriend)
* Kyla Pratt - Derricks Sister
* Glynn Turman - Derricks Dad
* Sy Smith - Fitz (VJ)
Eriq LaSalle - Alonzo "Al" Grant

==Famous cameos==
* Kenny Lattimore - Himself
* Chanté Moore - Herself

==Songs from the movie==
{{Infobox single
| Name          = I Need a Love
| Artist        = Kelly Rowland
| Released      = April 27, 2008  (YouTube)  2005  (Worldwide)  digital download
| Recorded      = United States
| Genre         = Pop music|Pop, contemporary R&B
| Length        = 3:47
| Label         = Blacktrak Entertainment Rob Lewis, Courtney Harrell, Cookie Lewis Rob Lewis
| Sounds Department = Kilyoung Baek, Charles D. Ballard, Eric Chase, David Criden
| Certification = 
}}
 Rob Lewis and Baskoash performed by Kelly Rowland which later released in digital downloads, the film used 28 songs featuring Chante Moore, Michael Angelo Berry, Honey, Brie, Rick James, Teena Marie, Nastacia Kendall, Street Loyalty and many more. Rowlands contributed five songs including "Follow Your Destiny", "Train on a Track" and Bad Habit.

===Track listing===
 Rob Lewis, Courtney Harrell and Cookie Lewis hits the YouTube with successful views 52,640 and received positive reactions from the critics,specially rowlands singing ability compared with the voice of Whitney Houston. Rob Lewis, Courtney Harrell, Cookie Lewis and co-produced by Ethan Farmer with Kevin randolph hits the YouTube with successful views 54,406 and received mixed positive and negative reactions from the critics.
# "Flashback" a pop R&B song sang by Kelly Rowland hits the YouTube with successful views 45,444 received positive reactions from the critics the song also in Rowland album Ms.Kelly, the song used in the ending part of the movie. Rob Lewis and Kevin White.
# "Ultimate Hollywood" written by Stephen James Edwards.
# "Alma Latina" written by Elizabeth Carbe.
# "Classic Romance" written by Linda Martinez.
# "Chicago Style" written by Larry Cohn.
# "Murder Battle" written by Johann Lestat.
# "Hollywood Stars" written by Linda Martinez.
# "Jungle Bee" written by Simon Holland.
# "Glances" written by I, Romeo.
# "Eazy Rider" written by Deklan Flynn.
# "Cajon Heat" written by Best Mekanic and Candence Blaze.
# "Think About It" performed by Honey and written by Monique Francis.
# "Boom" written by James Terry and Walter Scott produced by Great Scott performed by Brie.
# "BlackOut" written by D.Vardelja performed by Baby Docks.
# "The Skate Thing" written and performed by Michael Angelo Berry.
# "Sometimes Love" written by Steve Harvey and Nia Long performed by Voyce.
# "In Case You Didnt Know" written by Rob Lewis and performed by Nastacia Kendall.
# "Do What It Do" written by Greg Charley and performed by Street Loyalty.
# "Fire and Desire" written by Rick James performed by Rick James and Teena Marie.
# "Stop Boy" written and performed by Nastacia Kendall.
# "US2" written by Dan-lala and performed by Trendaettas.
# "About You" written by David Hunter and Carlton Martin performed by Diamond Malone.
# "Bad Habit" written by Kelly Rowland, Solange Knowles and Bryn Michael Cox performed by Kelly Rowland, hits the YouTube with successful views 133,177 views (the seat fillers most successful song) and received positive reactions from the critics specially rowlands voice and the massage of the song, the song also in Destinys Child album Destiny Fulfilled.
# "Train on a Track" is a pop/R&B song performed by Kelly Rowland, written by American producer Rob Fusari, Tiaa Wells, Balewa Muhammad and Sylvester Jordan and it was co-produced by Fusari and received a generally positive reception from music critics. it entered the top twenty in the UK Singles Chart.
# "Lets Go Back" written by Timothy Bloom performed by Alicia Keys.

==Box office performance==
The film debuted 18 in the box office top twenty for the week, grossing $4,812,287 in 287 cinemas. It averaged $4,410 at 2,905 sites, and earned a total domestic gross ticket sales of $10,238,923. Worldwide, the film grossed $17,972,898.

== Reviews ==
The film has received mainly average reviews. Garnering a rating of 3/5 stars, Vince Leo of Qwipster.net states that "While it isnt a must-see film by any means, never rising above a level of being merely pleasantly enjoyable, for those that enjoy romantic comedies, this one has enough moments of cleverness and natural charm to justify seeking out"  Andy Dursin of The Aisle Seat gave the film 2.5/4 stars staying "This romantic comedy with an African-American cast was capably directed by Nick Castle (“The Last Starfighter”) and offers an appealing cast, with the two leads generating believable chemistry throughout"  HomeTheaterInfo.com also gave the film 2.5/4 stars praising Rowland by stating that she "has what it takes to be a romantic comedy success, she is pretty, talented and be the girl next door or the sought after star. She also connects well with her fellow actors here providing a performance that the audience to become emotional invested with her character." 

==Broadcasting==
The film was probably unnoticed when it first came out in movie theaters, but did well in DVD sales. June 28, 2006, the movie get a network premiere on Black Entertainment Television|BET.

==DVD release==
The DVD was released 20 July 2005 in United States, on its second week of release the DVD entered United States Billboard (magazine)|Billboard DVD Chart (top 40) at number 14 and sold over 160,000 copies. After seven weeks of release it has made an estimate of $6,847,722 in sales.

Special features
*All-Access featurette
*Deleted scenes
*Extended Performance of "Follow Your Destiny" by Kelly Rowland

==Recognitions==
# 2004 Debuted at #18 on Box Office Top Twenty Weekly
# 2005 #14 on United States Billboard (magazine)|Billboard DVD Chart (top 40)

== References ==
 

==External links==
* 
*  

 

 
 
 
 
 