La vie est à nous !
     }}

{{Infobox film
| name           = La vie est à nous!
| image          = 
| caption        = 
| director       = Gérard Krawczyk
| producer       = Jean-François Lepetit
| writer         = Gérard Krawczyk
| based on       =  
| starring       = Josiane Balasko Sylvie Testud
| music          = Alexandre Azaria
| cinematography = Gérard Sterin	
| editing        = Nicolas Trembasiewicz
| distributor    = Bac Films
| studio         = Flach Film
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = $5,300,000
| gross          = $488,235  
}}

La vie est à nous! is a 2005 French comedy directed by Gérard Krawczyk and starring Josiane Balasko and Sylvie Testud.

==Plot==
That day, Blanche bury her husband, Camille, everyone liked him in the village of Savoy as it takes a bistro, "The Stage" with her daughter, Louise talkative. Which has put a cell phone in the coffin of his father to his mother ... can join at any time. Among the regulars of "The Stage", theres Chip, son of a drunkard, alcoholic himself, to whom the two women also complain of frequent competitor bar Louise Chevrier, "The Turn", the other side of the road. Able to collect broken children, Louise, who believes in the power of words to heal the pain, also has a new resident, Julien refuge in his silence.

==Cast==
* Sylvie Testud as Louise Delhomme
* Josiane Balasko as Blanche Delhomme
* Michel Muller as The chip
* Eric Cantona as Pierre
* Catherine Hiegel as Lucie Chevrier
* Carole Weiss as Irène
* Maroussia Dubreuil as Cécile
* Celia Rosich as Marion
* Danny Martinez as Clément
* Jil Milan as José
* George Aguilar as Sky
* Jacques Mathou as M. Antoine
* Agnès Château as Mme Antoine
* Laurent Gendron as Alf
* Chantal Banlier as Marguerite
* Virginie Lemoine as The mother
* Jean-Paul Lilienfeld as The father
* Aline Kassabian as The baker
* Jean Dell as The monk

==Development==
The movie was screened to the VCU French Film Festival in 2007.

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 