Julie & Julia
{{Infobox film
| name           = Julie & Julia
| image          = Julie and julia.jpg
| caption        = Theatrical release poster
| alt            = A woman laughing. Below a woman with a finger in her mouth. The middle horizontal section contains the film title. 
| director       = Nora Ephron
| producer       =  
| screenplay     = Nora Ephron
| story          = Julie Powell, Julia Child with Alex Prudhomme  
| starring       = {{Plain list |
* Meryl Streep
* Amy Adams
* Stanley Tucci
* Chris Messina
* Linda Emond
* Jane Lynch
}}
| music          = Alexandre Desplat
| cinematography = Stephen Goldblatt
| editing        = Richard Marks
| studio         = Columbia Pictures
| distributor    = Sony Pictures Releasing
| released       =  
| runtime        = 123 minutes  
| country        = United States
| language       = English
| budget         = $40 million
| gross          = $129,538,392   
}}
Julie & Julia is a 2009 American   that made her a published author.   

Ephrons screenplay is adapted from two books:   by Powell documenting online her daily experiences cooking each of the 524 recipes in Childs Mastering the Art of French Cooking, and she later began reworking that blog, The Julie/Julia Project.  Both of these books were written and published in the same time frame (2004–06). The film is the first major motion picture based on a blog. 
 The Devil Wears Prada (2006).

Julie & Julia was Ephrons last film before her death in 2012.

==Plot==
In 2002, Julie Powell (Adams) is a young writer with an unpleasant job at the Lower Manhattan Development Corporations call center, where she answers telephone calls from victims of the September 11 attacks and members of the general public complaining about the LMDCs controversial plans for rebuilding the World Trade Center.  To do something she enjoys, she decides to cook every recipe in Mastering the Art of French Cooking (1961) by Julia Child (Streep) in one year; Powell decides to write a blog to motivate herself and document her progress.

Woven into the story of Powells time in Queens in the early 2000s is the story of Childs time in Paris throughout the 1950s, where she attends Le Cordon Bleu to learn French cooking and begins collaborating on a book about French cooking for American housewives.  The plot highlights similarities in the womens challenges.  Both women receive much support from their husbands, except when Powells husband becomes fed up with her excessive devotion to her hobby and leaves her for a short time.
 Houghton Mifflin, it is accepted and published by Alfred A. Knopf. The last scene shows Powell and her husband visiting Childs kitchen at the Smithsonian Institution and Child in the same kitchen receiving a first print of her cookbook and celebrating the event with her husband.

==Cast==
* Meryl Streep as Julia Child
* Amy Adams as Julie Powell Paul Child, Julias husband
* Chris Messina as Eric Powell, Julies husband 
* Linda Emond as Simone Beck ("Simca"), with whom Julia wrote Mastering the Art of French Cooking
* Helen Carey as Louisette Bertholle, co-author of Mastering the Art of French Cooking
* Jane Lynch as Dorothy Dean Cousins, Julias sister 
* Mary Lynn Rajskub as Sarah, Julies sister.  Madame Elisabeth Brassart of Le Cordon Bleu, where Julia studied French cooking
*Amanda Hesser as herself
* Deborah Rush as Avis DeVoto, Julias long-time pen pal
* Vanessa Ferlito as Cassie, Julies acquaintance
* Casey Wilson as Regina, Julies acquaintance
* Jillian Bach as Annabelle, Julies acquaintance Irma Rombauer
* Mary Kay Place as the voice of Julies mother

==Reception==

===Critical response===

The film received positive reviews from critics.     , which assigns a rating out of 100 top reviews from mainstream critics, gave it an average score of 66% based on 34 reviews. 

Los Angeles Times critic Kenneth Turan commented, "  does it right. A consummate entertainment that echoes the rhythms and attitudes of classic Hollywood, its a satisfying throwback to those old-fashioned movie fantasies where impossible dreams do come true. And, in this case, it really happened. Twice."  The A. V. Club gave the film a C, explaining, "Julie & Julia is two movies in one. That’s one more movie than it needs to be."  Entertainment Weekly gave it a B+.  The review by Slate (magazine)|Slate was also positive. 

Streep has been widely praised for her performance as Child. Movie critic A. O. Scott of The New York Times affirmed that "By now   has exhausted every superlative that exists and to suggest that she has outdone herself is only to say that she’s done it again. Her performance goes beyond physical imitation, though she has the rounded shoulders and the fluting voice down perfectly."  Reviewer Peter Travers wrote in Rolling Stone that "Streep &mdash; at her brilliant, beguiling best &mdash; is the spice that does the trick for the yummy Julie & Julia."  Similarly, Stephanie Zacharek of Salon (magazine)|Salon concluded that "Streep isnt playing Julia Child here, but something both more elusive and more truthful &mdash; shes playing our idea of Julia Child." 

===Box office===
On its opening weekend, the film opened #2 behind   with $20.1 million.  The film has grossed $94,125,426 in the United States and $35,415,073 in other territories for a total worldwide gross of $129,540,499, surpassing its $40 million budget. 

===Awards and nominations===
  
{| class="wikitable" style="font-size: 90%;" border="2" cellpadding="4" background: #f9f9f9;
|- align="center"
! colspan=4 style="background:#B0C4DE;" | Awards and Nominations
|- align="center"
! Award
! Category
! Nominee
! Result
|- Academy Awards  Academy Award Best Actress
| Meryl Streep
| 
|- BAFTA Awards  BAFTA Award Best Actress
| Meryl Streep
| 
|- Boston Society of Film Critics Awards  Boston Society Best Actress Meryl Streep
| 
|- Broadcast Film Critics Association Awards  Broadcast Film Best Actress Meryl Streep
|  (tied with Sandra Bullock)
|- Chicago Film Critics Association Awards  Chicago Film Best Actress Meryl Streep
| 
|- Detroit Film Critics Society Awards  Best Actress Meryl Streep
| 
|-
|EDA Awards  Best Actress Meryl Streep
| 
|- Golden Globe Awards  Golden Globe Best Actress – Motion Picture Musical or Comedy Meryl Streep
| 
|- Golden Globe Best Motion Picture – Musical or Comedy Film
| 
|- Houston Film Critics Society Awards Houston Film Best Actress Meryl Streep
| 
|-
|Kansas City Film Critics Circle Awards  Kansas City Best Actress Meryl Streep
| 
|- London Film Critics Circle Awards  London Film Actress of the Year Meryl Streep
| 
|-
|New York Film Critics Circle Awards  New York Best Actress Meryl Streep
| 
|- New York Film Critics Online Awards  New York Best Actress Meryl Streep
| 
|-
|North Texas Film Critics Association Awards  Best Actress Meryl Streep
| 
|-
|Oklahoma Film Critics Circle Awards  Best Actress Meryl Streep
| 
|-
|Phoenix Film Critics Society Awards  Best Actress Meryl Streep
| 
|- San Diego Film Critics Society Awards San Diego Best Actress Meryl Streep
| 
|-
|San Francisco Film Critics Circle Awards  San Francisco Best Actress Meryl Streep
| 
|- Satellite Awards  Satellite Award Best Actress – Motion Picture Musical or Comedy Meryl Streep
| 
|- Satellite Award Best Film – Musical or Comedy Film
| 
|- Satellite Award Best Adapted Screenplay Nora Ephron
| 
|- Screen Actors Guild Awards  Screen Actors Best Actress Meryl Streep
| 
|- Southeastern Film Critics Association Awards  Southeastern Film Best Actress Meryl Streep
| 
|-
|St. Louis Gateway Film Critics Association Awards Best Actress Meryl Streep
| 
|- Toronto Film Critics Association Awards  Toronto Film Best Actress Meryl Streep
| 
|-
|Washington DC Area Film Critics Association Awards  Best Actress Meryl Streep
| 
|}

==Home release==
Julie & Julia was released on DVD and Blu-ray on December 8, 2009.

==See also==
* Julias Kitchen Wisdom

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 