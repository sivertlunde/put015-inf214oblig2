Sindoora Rekha
{{Infobox film
| name = Sindoora Rekha
| image =
| caption =
| director = Sibi Malayil
| producer = G. Gopalakrishnan
| writer = Raghunath Paleri
| screenplay = Raghunath Paleri
| starring = Suresh Gopi Shobhana Narendra Prasad Ranjitha
| music = Sharath
| cinematography = Venu
| editing = L Bhoominathan
| studio = GK Movie Land
| distributor = GK Movie Land
| released =  
| country = India Malayalam
}}
 1995 Cinema Indian Malayalam Malayalam film, directed by Sibi Malayil and produced by G. Gopalakrishnan. The film stars Suresh Gopi, Shobhana, Narendra Prasad and Ranjitha in lead roles. The film had musical score by Sharath.   

==Cast==
  
*Suresh Gopi as Balachandran 
*Shobhana as Arundhathi 
*Narendra Prasad as Menon 
*Ranjitha as Ramani 
*Oduvil Unnikrishnan as Raghavan Nair 
*Janardanan as Narayanan Nair  Meena as Balachandrans Mother 
*Zeenath as Rajalakshmi  Dileep as Ambujakshan 
*Bobby Kottarakkara as Gouthaman 
*Harishree Ashokan as Oommachan 
*Santhosh as Circle Inspector 
*Kundara Johny as Dasappan 
*Kozhikode Narayanan Nair as Muthassi 
*Aranmula Ponnamma as Vaidyar 
 

==Soundtrack==
The music was composed by Sharath.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Chithra || Kaithapram Vishwanathan Nambudiri || 04.16 
|- 
| 2 || Kalindi || K. J. Yesudas || Kaithapram Vishwanathan Nambudiri || 04.48 
|- 
| 3 || Nadam || K. J. Yesudas || Kaithapram Vishwanathan Nambudiri || 05.21 
|- 
| 4 || Pahirama Prabhu || Sharath || Kaithapram Vishwanathan Nambudiri || 01.38 
|- 
| 5 || Prana Dhosmi || K. J. Yesudas || Kaithapram Vishwanathan Nambudiri || 04.53 
|-  Chithra || Kaithapram Vishwanathan Nambudiri || 04.53 
|- 
| 7 || Ravil Veena Nadam || K. J. Yesudas, Sujatha || Kaithapram Vishwanathan Nambudiri || 05.09 
|-  Chithra || Kaithapram Viswanathan || 4.23 
|}

==References==
 

==External links==
*   
*  

 
 
 


 