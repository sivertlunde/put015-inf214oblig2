Chandrasena (1935 film)
{{Infobox film
| name           = Chandrasena
| image          = Chandrasena_(1935).jpg
| image_size     = 
| caption        = 
| director       = V. Shantaram
| producer       = Prabhat Film Company
| writer         = Shivram Vashikar
| narrator       = 
| starring       = Nalini Tarkhad Sureshbabu Mane Kelkar Azurie
| music          = Keshavrao Bhole 
| cinematography = Keshavrao Dhaiber 
| editing        = 
| distributor    =
| studio         = Prabhat Film Company
| released       = 1935
| runtime        = 136 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1935 Hindi mythology drama film directed by V. Shantaram for his Prabhat Film Company.  The cinematographer was K. Dhiaber and the story and dialogue were by Shivram Vashikar. The music direction was by Keshavrao Bhole with lyrics written by K. Narayan Kale.    The cast included Nalini Tarkhud, Sureshbabu Mane, Kelkar, Rajani, Shantabai and Azurie. 

The film was a remake of Shantaram and Dhaiber directed Chandrasena of 1931. The story revolves around an episode in Ramayana where Chandrasena, wife of Ahiravan, (Mahiravan’s (brother), helps Hanuman defeat Mahiravana’s army, and in the process free Rama and Lakshmana from Pataal.   

==Plot==
Indrajit, brother of Ravana, has Rama and Lakshmana kidnapped by Ahiravan and Mahiravana. Mahi hides them away in Patala|Pataal. Hanuman, the monkey-god, goes to their help. He is stopped by Makardhwaja whom he defeats. Makar takes him to Chandrasena, wife of mahi who worships Rama and desires to spend her life with him. With her help, Hanuman manages to defeat the entire army of Mahi and ultimately Mahi himself when he continues to multiply into a vast army. Chandrasena tells Hanuman the secret of destroying Mahi because of which he is able to release Rama and Lakshmana from Pataal.

==Cast==
* Nalini Tarkhad as Chandrasena
* Rajni
* Sureshbabu Mane as Ram
* Kulkarni as Lakshman
* Mane Pahelwan
* Kelkar as Mahiravan
* Manajirao as Hanuman
* Buwasaheb
* Shantabai
* Azurie

==Production==
The film is cited to be the first to make use of the trolley.    It made excessive use of special effects, especially showing flying figures, magic arrows and a gigantic Hanuman.   

==Remakes== Marathi and Chandrasena  Chandrasena (1959) under his Basant Pictures banner, which was directed by Babubhai Mistry. 

==Soundtrack==
The music composer for the film was Keshavrao Bhole with lyrics written by K. Narayan Kale. The singers were Rajni, Nalini Tarkhud, Sureshbabu Mane, Buwa Saheb. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-
| 1
| Kaisi Manohar hai Maaya Prachalit Chhaya Sansar Saara
| Sureshbabu Mane
|-
| 2
| Bhootnath Bhakton Ka Sada Ubaara
| 
|-
| 3
| Madira Chhalkan Laagi Gore Gore Haathon Se Paani
| Rajni
|-
| 4
| Abla Ki Pukar Suno Bhagwan Mere Ram Prabhu
| Nalini Tarkhud
|-
| 5
| Jai Jai Jagdish Ish Sharan Main Tihari
| Sureshbabu Mane
|-
| 6
| Chori Chori Nahin Yeh Kamaal Hai
| Buwa Saheb
|-
| 7
| Matwari Mori Yaad Vhandrasena Meri Yaad Nahin
| Sureshbabu Mane
|-
| 8
| Sura Pi Lo Swarg Ghar Ho Jaana
|
|-
| 9
| Pee Lo Pee Lo Surahi Main Naav Ki
| Sureshbabu Mane
|-
| 10
| Pal Chhin Laagi Lagan Tori
| Nalini Tarkhud
|-
| 11
| Rangili Raswali Phoolon Ki Behti Laali
| Rajni
|-
| 12
| Nek Thehr More Mann Aas Khaaye Bawre Aali
| Rajni
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 