Arroz con leche (1950 film)
{{Infobox film
| name           = Arroz con leche
| image          = ArrozconLecheshot.jpg
| image size     =
| caption        =
| director       = Carlos Schlieper
| producer       =
| writer         = Julio Porter, Carlos Schlieper
| narrator       =
| starring       =
| music          = George Andreani
| cinematography = Humberto Peruzzi
| editing        = Antonio Ripoll
| distributor    = Emelco
| released       = October 5, 1950
| runtime        = 79 minutes
| country        = Argentina
| language       = Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 1950 Argentina|Argentinean romantic comedy film directed and written by Carlos Schlieper with Julio Porter based on the play by Carlos Notti, "Noche en Viena".  The film premiered on October 5, 1950 in Buenos Aires and was rated PG 14. 

==Cast==
* Perla Achával
* Héctor Calcaño
* Susana Campos
* Perla Cristal
* Virginia de la Cruz
* Lía Durán
* Carlos Enríquez
* Lalo Hartich
* Eliseo Herrero
* Adolfo Linvel
* Ángel Magaña
* Arsenio Perdiguero
* Mario Perelli
* María Esther Podestá
* Hilda Rey
* Nélida Romero
* Esteban Serrador
* Amelia Vargas
* Wanda Were
* Malisa Zini

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 