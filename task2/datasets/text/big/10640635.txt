Mad Monkey Kung Fu
 
 
{{Infobox film
| name           = Mad Monkey Kung Fu
| image_size     =
| image	         = Mad Monkey Kung Fu FilmPoster.jpeg
| caption        =
| director       = Lau Kar-leung
| producer       = Mona Fong
| writer         = Ni Kuang
| narrator       = Hsiao Ho Lau Kar-leung Lo Lieh Kara Hui Ching Chu
| music          =
| cinematography =
| editing        =
| distributor    = Shaw Brothers Studio
| released       =  
| runtime        = 109 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         =
| gross          =
}}
 1979 Shaw Brothers kung fu film directed by Lau Kar-leung.
Later, the film was released on DVD in Dragon Dynasty.

== Plot == Hsiao Ho) is a petty thief and meets Chen and the two begin an odd friendly relationship until Tuens men slaughter Chens pet monkey, causing Little Monkey (Hsiao Hou) to try and take revenge for him. Monkey is trained by Master Chen and believes he is strong enough and leaves Master Chen. He goes into the streets and fights Tuens money collectors. He easily defeats them all and soon he is led to Tuen.  Monkey confronts Tuen initially and gets beat, Miss Chen helps Monkey escape but is killed by Tuen, forcing Chen to take revenge for his sister. Monkey returns to Master Chen shamefully and tells Chen about the meeting with his sister. Chen gets angry and goes to fight Tuen but Monkey reminds him that his hands are crippled and the Monkey Fist wont be as effective. Chen, reluctantly, agrees and trains Monkey more and then Monkey comes back to the brothel to fight Tuen again. He gets past all the men but gets trapped. Chen appears and saves him and they fight side by side. After that, Monkey engages combat with Tuen and soon gets the advantage. Tuen, seeing that he cant win tries to escape but Monkey doesnt let him and takes his hands. He smashes them into glass to cripple them, the same as what Tuen did to Chen. After that, it seems Monkey is done but then he says he also wants to avenge Chens sister and kills Tuen.

== External links ==
*   at Hong Kong Cinemagic
*  
*  

 

 
 
 
 
 


 