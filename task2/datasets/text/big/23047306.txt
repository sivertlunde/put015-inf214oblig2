Lady of the Pavements
{{Infobox film
| name           = Lady of the Pavements
| image          = Poster - Lady of the Pavements.jpg
| image_size     = thumb
| caption        = Promotional poster for the film.
| director       = D. W. Griffith
| producer       = Joseph M. Schenck Sam Taylor Karl Vollmoller
| narrator       = William Boyd Jetta Goudal
| music          = Irving Berlin
| cinematography = Karl Struss
| editing        =
| distributor    = United Artists
| released       =  
| runtime        = 85 minutes
| country        = United States Silent (English intertitles)
| budget         =
}} William Boyd, and Jetta Goudal. Griffith reshot the film to include a couple of musical numbers, making it a part-talkie. 

==Preservation==
The Vitaphone sound-on-disc system was employed for sound sequences. Discs 6 and 8 are in the UCLA Film and Television Archive. Other sound discs to this film were donated by Arthur Lennig to the George Eastman House Motion Picture Collection in Rochester, New York.

==Plot== William Boyd) street walker" than her. To get back at him, Diane arranges for Nanoni ("Little One") (Lupe Vélez), a singer at a sleazy Bar (establishment)|bar, to pretend to be a Spanish girl, from a convent, to fool him. 

==References==
 

==External links==
 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 