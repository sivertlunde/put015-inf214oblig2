Make Way for Tomorrow
{{Infobox film
| name           = Make Way for Tomorrow
| image          = Make-way-for-tomorrow-1937.jpg
| caption        = Original poster
| producer       = Leo McCarey Adolph Zukor
| director       = Leo McCarey
| writer         = Viña Delmar
| based on       =  
| starring       = Victor Moore Beulah Bondi
| cinematography = William C. Mellor
| music          = George Antheil Victor Young
| editing        = LeRoy Stone Paramount
| distributor    = Paramount Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
}}

Make Way for Tomorrow is a 1937 American drama film directed by Leo McCarey. The plot concerns an elderly couple (Victor Moore and Beulah Bondi) who are forced to separate when they lose their house and none of their five children will take both parents.

The film was written by Viña Delmar, from a play by Helen and Noah Leary, which was in turn based on the novel The Years Are So Long by advice columnist Josephine Lawrence.

McCarey believed that it was his finest film.  When he accepted his Academy Award for Best Director for The Awful Truth which was released the same year, he said "Thanks, but you gave it to me for the wrong picture."

In 2010, it was selected for preservation by the United States Library of Congresss National Film Registry.

==Plot==
Barkley "Bark" (Victor Moore) and Lucy Cooper (Beulah Bondi) are an elderly couple who lose their home to foreclosure, as Barkley has been unable to find employment because of his age. They summon four of their five children—the fifth lives thousands of miles away in California—to break the news and decide where they will live until they can get back on their feet. Only one of the children, Nell (Minna Gombell), has enough space for both, but she asks for three months to talk her husband into the idea. In the meantime, the temporary solution is for the parents to split up and each live with a different child.

The two burdened families soon come to find the parents presence bothersome. Nells efforts to talk her husband into helping are half-hearted and achieve no success, and she reneges on her promise to eventually take them. While Barkley continues looking for work to allow him and his wife to live independently again, it is obvious that he has little or no prospect of success. When Lucy continues to speak optimistically of the day that he will find work, her teenage granddaughter bluntly advises her to "face facts" that it will never happen because of his age. Lucys sad reply is to say that "facing facts" is easy for a carefree 17-year old girl, but that at Lucys age, the only fun left is "pretending that there aint any facts to face ... so would you mind if I just kind of went on pretending?"
 Thomas Mitchell) and his wife Anita (Fay Bainter) begin planning to move Lucy into a retirement home. Lucy accidentally finds about their plans, but rather than force George into the awkward position of breaking the news to her, she goes to him first and claims that she wants to move into the home. Meanwhile Barkley resigns himself to his fate of having to move thousands of miles away, though he too is entirely aware of his daughters true motivation.

On the day Barkley is to depart by train, he and Lucy make plans to go out and spend one last afternoon together before having a farewell dinner with the four children. They have a fantastic time strolling around the city and reminiscing about their happy years together, even visiting the same hotel in which they had stayed on their honeymoon 50 years prior. Their day is made so pleasant partly because of the kindness of people they encounter, who, although strangers, seem to find them a charming couple, to genuinely enjoy their company, and to treat them with deference and respect---in stark contrast to the treatment the two are receiving from their children. 

Eventually Barkley and Lucy decide to continue their wonderful day by skipping the farewell dinner and dining at the hotel instead; when Barkley informs their daughter with a blunt phone call, it prompts introspection among the four children. Son Robert (Ray Meyer) suggests that each of the children has always known that collectively they are "probably the most good-for-nothing bunch of kids that were ever raised, but it didnt bother us much until we found out that Pop knew it too." George notes that it is now so late in the evening that they wont even have time to meet their parents at the train station to send off their father. He says that he deliberately let the time pass until it was too late because he figured their parents would prefer to be alone. Nell objects that if they dont go to the station, their parents "will think were terrible," to which George matter-of-factly replies, "Arent we?"

At the train station, Lucy and Barkley say their farewells to one another. On the surface, their conversation echoes Lucys comments to her granddaughter about preferring to pretend, rather than facing facts. Barkley tells Lucy that he will find a job in California and quickly send for her; Lucy replies that she is sure he will do so. They then offer each other a truly final goodbye, saying that they are doing so "just in case" they do not see each other again because "anything could happen." Each makes a heartfelt statement reaffirming their lifelong love, in what seems an unspoken acknowledgment that it is almost certainly their final moment together.

==Cast==
*Victor Moore as Barkley "Pa" Cooper
*Beulah Bondi as Lucy "Ma" Cooper
*Fay Bainter as Anita Cooper, Georges wife Thomas Mitchell as George Cooper, Barkleys and Lucys son
*Porter Hall as Harvey Chase, Nellies husband
*Barbara Read as Rhoda Cooper, Georges and Anitas daughter
*Maurice Moscovitch as Max Rubens, the Jewish shopkeeper and Barkleys friend
*Elizabeth Risdon as Cora Payne, Barkleys and Lucys daughter
*Minna Gombell as Nellie Chase, Barkleys and Lucys daughter
*Ray Meyer as Robert Cooper, Barkleys and Lucys son
*Ralph Remley as Bill Payne, Coras husband
*Louise Beavers as Mamie, maid of George and Anita
*Louis Jean Heydt as Barkleys doctor	
*Dell Henderson as Ed Weldon, auto salesman	
*Louise Seidel as Hat Check Girl at the hotel	 Paul Stanton as Mr. Horton, hotel manager
*Gene Morgan as Carlton Gorman, bandleader at the hotel

==Reception==
Orson Welles said of Make Way for Tomorrow, "It would make a stone cry,"    and rhapsodized about his enthusiasm for the film in his booklength series of interviews with Peter Bogdanovich, This Is Orson Welles. In Newsweek magazine, famed documentary filmmaker Errol Morris named it his #1 film, stating "The most depressing movie ever made, providing reassurance that everything will definitely end badly."   

Make Way for Tomorrow also earned good reviews when originally released in Japan, where it was seen by screenwriter Kogo Noda. Years later, it provided an inspiration for the script of Tokyo Story (1953), written by Noda and director Yasujiro Ozu.

Roger Ebert added this film to his "Great Movies" list on February 11, 2010, writing:

 
"Make Way for Tomorrow" (1937) is a nearly-forgotten American film made in the Depression...The great final arc of "Make Way for Tomorrow" is beautiful and heartbreaking. Its easy to imagine it being sentimentalized by a studio executive, being made more upbeat for the audience. Thats not McCarey. What happens is wonderful and very sad. Everything depends on the performances."  

The film is now part of the Criterion Collection, describing it as "...one of the great unsung Hollywood masterpieces, an enormously moving Depression-era depiction of the frustrations of family, aging, and the generation gap...Make Way for Tomorrow is among American cinema’s purest tearjerkers, all the way to its unflinching ending, which McCarey refused to change despite studio pressure. 

In 2010, it was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".      

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 