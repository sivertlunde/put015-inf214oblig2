Henry Fool
 
{{Infobox film
| name = Henry Fool
| image = Henry fool.jpg
| caption = Promotional one-sheet
| director = Hal Hartley
| producer = Larry Meistrich Hal Hartley
| writer = Hal Hartley
| starring = Thomas Jay Ryan James Urbaniak Parker Posey Liam Aiken
| music = Hal Hartley
| cinematography = Michael Spiller Steve Hamilton
| distributor = Sony Pictures Classics
| released =  
| runtime = 137 minutes
| country =  United States
| language = English
| gross    = $1,338,335
}} The Unbelievable Truth, an earlier Hartley film, expectation and reality again conflict.
 best screenplay award at the 1998 Cannes Film Festival.    A sequel, titled Fay Grim, was released in 2007. Another sequel, titled Ned Rifle, was released in 2015.

==Plot==
Socially inept garbage-man Simon Grim is befriended by Henry Fool, a witty rogue and untalented novelist. Henry opens the world of literature to Simon, and inspires him to write "the great American poem". Simon struggles to get his work recognized, and it is often dismissed as pornographic tat, but Henry continues to push and inspire Simon to get the poem published.

Henry carries around a bundle of notebooks that he refers to as "His Confession," a work that details aspects of his mysterious past that he one day hopes to publish, when he and the world is ready for them. Henrys hedonistic antics cause all manner of turns in the lives of Simons family, not least of which is impregnating Fay Grim (Simons sister).

As Simon begins an ascent to the dizzying heights of Nobel Prize-winning poet, Henry sinks to a life of drinking in low-life bars as his own attempts at fame result in rejection, even by Simons publisher who once employed Henry. The friends part ways and lose touch, until Henry’s criminal past catches up with him and he needs Simon’s help with fleeing the country.

==Cast==
*Thomas Jay Ryan as Henry Fool
*James Urbaniak as Simon Grim
*Parker Posey as Fay Grim
*Liam Aiken as Ned
*Maria Porter as Mary
*James Saito as Mr. Deng
*Kevin Corrigan as Warren
*Camille Paglia as herself
*Toy Connor as Teenager at World Of Donuts

==Reception==
Based on 23 reviews collected by the film review aggregator Rotten Tomatoes, 88% of critics gave Henry Fool a positive review, with an average rating of 7.4/10.  Leonard Maltin gives the film two and a half stars, saying Hartley "just misses the mark". 

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 
 