11Eleven Project
 

{{Infobox film
| name = 11Eleven Project
| image = 
| producer = Danielle Lauren  Morgan Calton
| distributor = Titan View
| released = 
| runtime = 75 mins
| country = Australia, Angola, China, Ecuador, France, Georgia (country)|Georgia, Iran, Israel, Japan, Lebanon, Netherlands, Philippines, Poland, South Africa, Spain, Switzerland, Syria, Thailand, United States, Uganda, United Kingdom,  English
}}
 crowdsourced video footage, audio and images taken on the 11 November 2011. The film premiered worldwide on the 11 November 2012. 

==Background==
Creative Director, Danielle Lauren, was inspired to initiate this project after watching documentary films Koyaanisqatsi and Powaqqatsi back-to-back in Perth in 2000. She wrote her idea down as ‘A Day in The Life of The World Told by The People of Earth’ after contemplating the idea of capturing various events happening at the same time.  Lauren did not pursue the project until 11 years later, when the technology required to make it possible was prevalent in society. 
 United Nations Millennium Development Goals. 
 
Lauren has also mentioned in a statement that "11/11 is Remembrance Day and Armistice globally. In most of the major religions and philosophies, the number 11 is also considered a very spiritual number." 

==Production==
The brief of the project was to submit video footage, audio recordings and/or images created on 11/11/11. This was publicly posted across various forms of social media and submitted to the project. Such networks used were Facebook, Twitter, SoundCloud and YouTube.   The content of what was submitted was unrestricted for its contributors, with the only condition being that it must have been created on 11/11/11.

Post-production commenced on the 12th of November, 2011. Submissions were received from 179 countries. 

A preview screening was held in Sydney Town Hall on September 21, 2012 to gather viewer feedback.   The film premiered worldwide on the 11th of November, 2012. Screening locations included Australia, the USA, Canada, France, Germany, Japan, Uganda and more. 

The 11Eleven Project film soundtrack, which is also crowdsourced and user-generated, has been released on iTunes to purchase for download. 

The 11Eleven Project photographic book, containing 1440 photographs (one photo for every minute of 11/11/11), is due for release. 

==References==
 

==External links==
* 
* 

 
 
 
 