Lak Yawm Ya Zalem
{{Infobox film
| name           = Lak Yawm Ya Zalem
| image          = LakYomYaZalem.jpg
| image_size     =
| caption        = Faten Hamama and Mohammed Tawfik in Lak Yawm Ya Zalem
| director       = Salah Abouseif
| producer       = 
| writer         = 
| starring       = Faten Hamama Mahmoud El-Meliguy Mohammed Tawfik Mohsen Sarhan
| music          = 
| cinematography = 
| distributor    =  1951
| runtime        = 
| country        = Egypt
| language       = Arabic
| budget         = 
| website        = 
}}

Lak Yawm Ya Zalem   ( , Your Day Will Come) is a classic 1951 Egyptian drama film directed by Salah Abouseif. It starred Faten Hamama, Mahmoud el-Meliguy, Mohammed Tawfik and Mohsen Sarhan and was chosen as one of the best 150 Egyptian film productions in 1996, during the Egyptian Cinema centennial. The film was presented in the Berlin International Film Festival.

== Plot ==

A greedy man betrays his friend and falls in love with his wife, who is a rich lady. He kills him and marries the widow. He steals her money and jewellery and mistreats her. He is then arrested by the police and receives his punishment. 

== References ==
*  , Faten Hamamas official site. Retrieved on January 10, 2007.

== External links ==
*  

 
 
 
 
 

 
 