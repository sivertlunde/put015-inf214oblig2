Manzil (1936 film)
{{Infobox film
| name           = Manzil
| image          = 
| image_size     = 
| caption        = 
| director       = P.C. Barua
| producer       = New Theatres
| writer         = Saratchandra Chattopadhyay
| narrator       =  Jamuna Pahari Sanyal Prithviraj Kapoor
| music          = R. C. Boral Pankaj Mullick
| cinematography = Bimal Roy
| editing        = 
| distributor    =
| studio         = New Theatres
| released       = 30 Nov 1935
| runtime        = 144 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1936 Hindi Bengali (Grihadah),  it was directed by P.C. Barua from a story by Saratchandra Chattopadhyay.    The dialogues and lyrics were by Arzu Lucknowi and music composed by R. C. Boral and Pankaj Mullick. The cast included Prithviraj Kapoor, Jamuna Barua|Jamuna, Pahari Sanyal, Molina Devi, K. C. Dey and Boken Chatto.    The story, a love triangle, revolves around two friends Mahim and Suresh and the girl they both love, Achala.

==Plot==
Mahim and Suresh are childhood friends, both in love with Achala. Suresh is rich but adheres to conventional values while Mahim is from a poor family but well-educated. Achala has been given a liberal Brahmo Samaj upbringing. Though fond of both friends, she chooses to marry Mahim and they shift to a village. Achala gradually becomes dissatisfied. Their house burns down and Mahim falls ill. Suresh arrives to nurse Mahim back to health and goes with them to a health resort for Mahim to recover. Achala is attracted to Suresh and they both elope. She later returns to Mahim who forgives her.

==Cast==
*Jamuna Barua|Jamuna: Achala
*Pahari Sanyal: Mahim
*Prithviraj Kapoor: Suresh
*K. C. Dey
*Molina Devi
*Boken Chatto
*Nemo

==References==
 

==External links==
* 


 
 
 

 