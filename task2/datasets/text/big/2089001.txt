Lady Beware
 
{{Infobox film
| image          = Lady Beware Movie Poster.jpg
| caption        = Lady Beware theatrical poster
| producer       = Tony Scotti Lawrence Taylor-Mortoff
| director       = Karen Arthur
| writer         = Charles Zev Cohen Susan Miller
| starring       = Diane Lane Michael Woods Cotter Smith
| music          = Craig Safan
| cinematography = Tom Neuwirth
| editing        = Roy Watts Scotti Brothers Pictures
| released       = August 14, 1987 (Not yet released on DVD as of 2013)
| country        = United States
| runtime        = 108 min.
| language       = English
}}
 1987 United American Thriller thriller film, directed by Karen Arthur. It stars Diane Lane, Michael Woods, and Cotter Smith. The film is marketed with the tagline "When fantasy leads to terror." It was filmed on location in and around Pittsburgh.

==Plot summary==

Katya Yarno is a window dresser for Hornes department store who specializes in displays with sexy, slightly kinky themes. Surrounded by the equipment of her trade &mdash; mannequins and lingerie &mdash; Katya lives in a loft apartment in downtown Pittsburgh. She spends her evenings taking her bath by candlelight and thinking up new and more provocative window displays.

Katya soon becomes the obsession of Jack Price, a handsome (and married) psychopath. Jack proceeds to stalk Katya and makes her life a living hell. Tired of being harassed, Katya decides to give Jack a taste of his own medicine.

==Main cast==
* Diane Lane - Katya Yarno
* Michael Woods - Jack Price
* Cotter Smith - Mac Odell
* Peter Nevargic - Lionel
* Edward Penn - Mr. Thayer
* Tyra Ferrell - Nan
* Clayton D. Hill - Police Officer #1 David Crawford - Katyas father
* Ray Laine - Doctor
* Bingo O Malley - Man in Window
* Don Brockett - Locksmith

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 