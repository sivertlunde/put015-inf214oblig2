List of American films of 1909
 
 
This is a list of American films released in 1909 in film|1909. 
{| class="wikitable"
|-
! Title !! Director !! Cast !! Genre !! Notes
|- Short film|Short, Drama ||
|-
|Cure for Bashfulness|| || || ||
|-
| || || || ||
|- A Sisters Love|| || || ||
|-
|The Maniac Cook|| || || ||
|-
|The Haunted Lounge|| || || ||
|- The Tenderfoot|| || || ||
|-
|Mrs. Jones Entertains|| || || ||
|-
|The Honor of Thieves|| || || ||
|-
|Love Finds a Way|| || || ||
|- The Castaways|| || || ||
|-
|Where Is My Wandering Boy Tonight?|| || || ||
|-
|A Rural Elopement|| || || ||
|- The Sacrifice|| || || ||
|-
|The Criminal Hypnotist|| || || ||
|-
|Those Boys!|| || || ||
|-
|The Fascinating Mrs. Francis|| || || ||
|-
|Mr. Jones Has a Card Party|| || || ||
|-
|A Colonial Romance|| || || ||
|-
|Those Awful Hats|| D. W. Griffith || Mack Sennett || Comedy ||
|-
|The Welcome Burglar   
|| || || ||
|-
| || || || ||
|-
|The Cord of Life || || || ||
|-
|A B Cs of the U.S.A.|| || || ||
|-
|The Girls and Daddy || || || ||
|-
|The Deacons Love Letters|| || || ||
|-
|The Brahma Diamond|| || || ||
|- Edgar Allan Poe|| || || ||
|-
|A Wreath in Time|| || || ||
|-
|Tragic Love|| || || ||
|-
|Jessie, the Stolen Child|| || || ||
|-
|The Curtain Pole || D. W. Griffith || Mack Sennett || Comedy ||
|-
|His Wards Love|| || || ||
|-
|A Daughter of the Sun|| || || ||
|-
|The Honor of the Slums|| || || ||
|-
|Tag Day|| || || ||
|-
|The Hindoo Dagger|| || || ||
|-
|The Joneses Have Amateur Theatricals|| || || ||
|-
|C.Q.D.; or, Saved by Wireless; a True Story of the Wreck of the Republic|| || || ||
|-
|The Golden Louis|| D. W. Griffith || || Drama ||
|-
|The Politicians Love Story|| || || ||
|-
|At the Altar|| D. W. Griffith || Marion Leonard || Drama ||
|-
|The Poor Musician|| || || ||
|-
|His Wifes Mother|| || || ||
|-
|The Prussian Spy|| || || ||
|-
|A Fools Revenge|| || || ||
|-
|The Mad Miner|| || || ||
|-
|The Old Soldiers Story|| || || ||
|-
|The Wooden Leg|| || || ||
|-
|Adventures of a Drummer Boy|| || || ||
|-
|The Salvation Army Lass|| || || ||
|-
|Kenilworth (1909 film)|Kenilworth|| || || ||
|- I Did It|| || || ||
|-
|The Lure of the Gown|| || || ||
|-
|The Road Agents|| || || ||
|- Boots and Saddles|| || || ||
|- The Voice of the Violin|| || || ||
|-
|The Crackers Bride|| || || ||
|-
|A Brave Irish Lass|| || || ||
|-
|A Friend in the Enemys Camp|| || || ||
|-
|And a Little Child Shall Lead Them|| || || ||
|- The Deception|| || || ||
|-
|Cohen at Coney Island|| || || ||
|-
|Cohens Dream|| || || ||
|-
|The Energetic Street Cleaner|| || || ||
|-
|Midnight Disturbance|| || || ||
|-
|A Burglars Mistake|| || || ||
|- King Lear||J. Stuart Blackton and William V. Ranous || William V. Ranous || ||
|-
|Jones and His New Neighbors|| || || ||
|-
|The Medicine Bottle|| || || ||
|-
|A Drunkards Reformation|| D. W. Griffith || Arthur V. Johnson || Drama ||
|-
|The Road to the Heart|| || || ||
|-
|Trying to Get Arrested|| || || ||
|-
|The Life of Napoleon|| || || ||
|-
|Napoleon and the Empress Josephine|| || || ||
|-
|A Tale of the West|| || || ||
|}

==References==
 

==External links==
 
*  at the Internet Movie Database

 

 

 
 
 
 