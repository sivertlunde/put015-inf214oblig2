Divorced (film)
 
{{Infobox film
| name           = Divorced
| image          = 
| caption        = 
| director       = Gustaf Molander
| producer       = 
| writer         = Herbert Grevenius Ingmar Bergman
| starring       = Inga Tidblad Alf Kjellin Doris Svedlund
| music          = 
| cinematography = Åke Dahlqvist
| editing        = Oscar Rosander
| distributor    = AB Svensk Filmindustri
| released       =  
| runtime        = 103 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}

Divorced ( ), is a 1951 Swedish drama film directed by Gustaf Molander and written by Ingmar Bergman.

==Cast==
* Inga Tidblad - Gertrud Holmgren
* Alf Kjellin - Dr. Bertil Nordelius
* Doris Svedlund - Marianne Berg
* Hjördis Petterson - Mrs. Lobelius
* Håkan Westergren - P.A. Beckman
* Irma Christenson - Cecilia Lindeman
* Holger Löwenadler - Tore Holmgren
* Marianne Löfgren - Mrs. Ingeborg
* Stig Olin - Hans
* Elsa Prawitz - Elsie
* Birgitta Valberg - Eva Möller
* Sif Ruud - Rut Boman
* Carl Ström - Öhman
* Ingrid Borthen - Dinner guest
* Yvonne Lombard - The young wife

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 