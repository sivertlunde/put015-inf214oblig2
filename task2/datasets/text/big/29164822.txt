She Had to Say Yes
{{Infobox film
| name           = She Had to Say Yes
| image          = Full.shehadtosayyes-1sh-1915.JPG
| image_size     =
| caption        = Poster
| director       = George Amy Busby Berkeley
| producer       =
| writer         = John Francis Larkin (story "Customers Girl") Rian James Don Mullaly
| starring       = Loretta Young Winnie Lightner Lyle Talbot Regis Toomey
| music          = Leo F. Forbstein
| cinematography = Arthur L. Todd
| editing        = George Amy
| distributor    = First National Pictures
| released       = July 15, 1933
| runtime        = 63 minutes United States
| language       = English
| budget         =
| preceded_by    =
}}

She Had to Say Yes is a 1933 Pre-Code Hollywood|pre-Code film directed by George Amy and Busby Berkley. It was Berkleys directorial debut. Loretta Young stars as a secretary who receives unwanted sexual advances when she is sent out on dates with her employers clients. The film was promoted with the teaser, "We apologize to the men for the many frank revelations made by this picture, but we just had to show it as it was filmed. The true story of the working girl." Doherty. pg. 131  

According to pre-Code scholar Thomas Doherty, it was part of a series of movies that drew inspiration from the "real-life compromises working girls made to get and retain employment" during the Great Depression.  A repeated theme in womens pictures in the Depression was the "threat of sexual violation" and the "hard necessity of risking virtue to keep a paycheck".  Women of that time were often subjected to sexual harassment, and had to endure indignities in a highly competitive job market. The film received a negative review in The New York Times when it was released. Nugent, Frank S.   The New York Times, July 29, 1933, accessed October 12, 2010 

==Plot==
Sol Glass (Ferdinand Gottschalk) owns a clothing manufacturing company struggling to survive in the midst of the Great Depression. Like his competitors, Glass employs "customer girls" to entertain out-of-town buyers. However, his clients have become tired of his hard-bitten "gold diggers" and have started taking their business elsewhere. Tommy Nelson (Regis Toomey), one of his salesmen, suggests that they use their stenographers instead. Glass decides to give it a try. 

When buyer Luther Haines (Hugh Herbert) sees Tommys secretary and fiancee, Florence "Flo" Denny (Loretta Young), he wants to take her out. However, Tommy manages to steer him to the curvaceous Birdie (Suzanne Kilborn) instead. Later, with Birdie sick, Tommy reluctantly lets Flo go on a date with another buyer, Daniel "Danny" Drew (Lyle Talbot). They have a nice time together, but she is shocked when she finds out Danny expects sex. A contrite Danny apologizes and tells her that he has fallen in love with her. He has to go on a business trip, but telephones and writes to her regularly.

Meanwhile, Flos friend, fellow employee and roommate, Maizee (Winnie Lightner), shows her that Tommy is cheating on her with Birdie. She ends their engagement.

To keep her self-respect, Flo tells Glass that she will not go out with any more buyers. When he threatens to fire her, she quits.

Danny returns and takes Flo to dinner. Then, spotting Haines at another table, he asks her to help convince the last holdout to a merger to sign an important contract, the biggest deal of his life. She is disappointed by his request, but agrees to do it. She goes to dinner with Haines, but cleverly arranges with Maizee to have Haines wife (Helen Ware) and daughter show up. Haines has to go along with the pretense that he is conducting business, and signs the contract. 

When Haines later complains about Flos methods, and claims that she and Tommy are living together, Daniel suspects that she is not as innocent as he believed, so he drives her out into the country to the mansion of his friends. Nobody is home, but he coaxes her inside and tries to force himself on her. Flo tries to get away, but finally stops resisting. However, when she asks him if that is all she means to him, Danny stops before anything happens. She leaves, only to run into Tommy, who had followed the couple. He also believes she is selling herself. Danny, overhearing their conversation, realizes that Flo is innocent, and forces Tommy to apologize. Danny begs her to marry him. After she whispers in his ear, he picks her up and carries her back into the mansion.

==Cast==
 
* Loretta Young as Florence Denny 
* Winnie Lightner as Maizee
* Lyle Talbot as Daniel Drew
* Regis Toomey as Tommy Nelson
* Hugh Herbert as Luther Haines
* Ferdinand Gottschalk as Sol Glass 
* Suzanne Kilborn as Birdie Reynolds
* Helen Ware as Mrs. Haines

==Reception==
Writing for The New York Times, Frank S. Nugent gave the film a mostly negative review, primarily due to the constant suspicions the two male leads have about Youngs character when the film makes it fairly obvious from the beginning that Youngs character is virtuous. Nugent added: "The unfortunate part of it is that the picture has some bright lines and threatens, here and there, actually to become amusing. Hugh Herbert and Winnie Lightner wheedled a few laughs from the stranded Strand visitors, but the gayety was short-lived. It would have been a relief to every one if Miss Young had only said "No!" 

==References==
 

==See also==
*Pre-Code sex films

==References==
*Doherty, Thomas Patrick. Pre-Code Hollywood: Sex, Immorality, and Insurrection in American Cinema 1930-1934. New York: Columbia University Press 1999. ISBN 0-231-11094-4

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 