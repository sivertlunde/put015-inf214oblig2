Swabhimana
{{Infobox film|
| name = Swabhimana
| image = 
| caption =
| director = D. Rajendra Babu
| story  = K. S. Satyanarayana
| writer = 
| screenplay = D. Rajendra Babu
| starring = Tiger Prabhakar  Aarathi   V. Ravichandran   Mahalakshmi
| producer = B. N. Gangadhar
| music = Shankar Ganesh
| cinematography = D. Prasad Babu
| editing = K. Balu
| studio = A N S Productions
| released =  
| runtime = 135 minutes
| language = Kannada
| country = India
| budget =
}}

Swabhimana ( ) is a 1985 Kannada romantic drama film directed by D. Rajendra Babu and written by K. S. Satyanarayan. The film starred Tiger Prabhakar, V. Ravichandran, Mahalakshmi and Aarathi in lead roles.  The soundtrack and score composition was by Shankar Ganesh and the film was produced by B. N. Gangadhar.

== Cast ==
* V. Ravichandran
* Tiger Prabhakar 
* Aarathi
* Mahalakshmi
* Mukhyamantri Chandru
* Umashree Balakrishna
* N. S. Rao
* Shivaram
* Shanthamma

== Soundtrack ==
The music was composed by Shankar Ganesh with lyrics by R. N. Jayagopal.  The song "Doorada Oorinda Hammeera Banda" was received extremely well and considered one of the evergreen songs. Jayagopal won the Karnataka State Film Award for Best Lyricist for this song.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Doorada Oorinda Hammera Banda
| extra1 = S. P. Balasubramanyam, S. Janaki
| lyrics1 = R. N. Jayagopal
| length1 = 
| title2 = Ondu Eradu Mooru Beke
| extra2 = S. P. Balasubramanyam, S. Janaki
| lyrics2 = R. N. Jayagopal
| length2 = 
| title3 = Baruvaaga Onti Neenu
| extra3 = Rajkumar Bharathi
| lyrics3 = R. N. Jayagopal
| length3 = 
| title4 = Haalu Jenu Serida Hange
| extra4 = Vani Jairam
| lyrics4 = R. N. Jayagopal
| length4 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 