Bowery to Bagdad
{{Infobox Film
| name           = Bowery to Bagdad
| image          = 
| image_size     = 
| caption        = 
| director       = Edward Bernds
| producer       = Ben Schwalb
| writer         = Edward Bernds Elwood Ulman
| narrator       = 
| starring       = Leo Gorcey Huntz Hall David Gorcey Bernard Gorcey
| music          = Marlin Skiles
| cinematography = Harry Neumann
| editing        = Lester A. Sansom Allied Artists
| distributor    = Allied Artists
| released       = January 2, 1955 (U.S. release)
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Allied Artists and is the thirty-sixth film in the series.

==Plot==
Sach buys a magic lamp containing a Genie.  A group of gangsters see the boys using the lamp and steal it.  However, the boys had made a wish that only the Slip and Sach could request wishes from the Genie so he is unable to grant the gangsters wishes.  The gangsters decide that if the two of them were dead then the Genie would have no choice but to obey their commands.  The Genie has taken a liking to the boys and helps them escape, but they are transported back to Baghdad where the true master of the lamp resides, leaving Slip and Sach without any more wishes.  The Genie, feeling sorry for them, grants them one more wish, which Sach uses to "wish I had the nerve to sock him (Slip) in the chin", which the Genie grants.

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*David Gorcey as Chuck Anderson (Credited as David Condon)
*Bennie Bartlett as Butch Williams

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski
*Joan Shawlee as Velma (a/k/a Cindy Lou Calhoun)
*Eric Blore as the Genie

==International release==
This film was released in England in October 1954. 

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Four" on August 26, 2014.

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Jungle Gents 1954 High Society 1955}}
 

 

 
 
 
 

 