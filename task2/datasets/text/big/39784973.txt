Agnichakra
{{Infobox film
| name           = Agnichakra
| image          = Agnichakrafilm.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Promotional Poster
| film name      = 
| director       = Amit Suryavanshi
| producer       =Piyush Shah
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Govinda Dimple Kapadia
| music          = Bappi Lahiri
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Govinda and Dimple Kapadia in pivotal roles.  

==Cast==
* Naseeruddin Shah...Inspector Satpal
* Govinda (actor)|Govinda...Amar
* Dimple Kapadia...Rani
* Anupam Kher...Dhanraj Raj Kiran...Inspector. Suryaveer
* Pramod Moutho...	Jumbo
* Beena Banerjee...Beena - Suryaveers wife 
* Somy Ali...Special Appearance

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aila Rani Ka Dil"
| Amit Kumar, Kavita Krishnamurthy
|-
| 2
| "Dil Dene Se Pehle"
| Kumar Sanu, Alka Yagnik
|-
| 3
| "Arzi Dil Ki Meri (Sad)"
| Bappi Lahiri
|}

==References==
 

==External links==
* 

 
 
 

 