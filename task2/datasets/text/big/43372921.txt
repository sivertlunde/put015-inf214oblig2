Præsidenten
{{Infobox film name = Præsidenten image = Praesidenten_(1919_film).jpg caption = Screenshot from Præsidenten country = Denmark released =   runtime = 75 minutes language = Silent film with Danish intertitles director = Carl Theodor Dreyer studio = Nordisk Film
}}

Præsidenten (The President) is a 1919 Danish silent film by Carl Theodor Dreyer. It is the first feature film directed by Dreyer and it is an adaptation of the 1884 novel Der Präsident by Karl Emil Franzos. 

== Plot ==
The film tells a story of women seduced and abandoned over four generations. It follows three periods in the life of Karl Victor von Sendlingen, a Danish aristocrat living in a small city at the turn of the 20th century. The chronological narrative is interrupted by flashbacks recalling past events.

The film starts on the last day in the life of Karl Victors father, Franz Victor von Sendlingen. Franz Victor takes his son to the ruins of the old family castle where he confesses how as a young man he had made pregnant the porters daughter and was forced by his father Rigsherre von Sendlingen to marry her. Before dying, he asks Karl Victor to swear on the family coat of arms that he will never marry a commoner, "as no good ever comes from such an alliance".

Thirty years later, Karl Victor, now the president of the tribunal of his home-town, comes back home from a long absence. He is welcomed at the station by Vice-President Werner, who has replaced him during his absence. When is given the list of pending cases, he sees recognises the name of Victorine Lippert, accused of infanticide. When her lawyer, Georg Berger, who is his best friend, comes to discuss her case, he tells him that she is his daughter. As a young man, he had an adventure with her mother Hermine Lippert, the governess of his uncles children. When he had told his uncle that he wanted to marry her, he had reminded him of the oath made to his father never to marry a commoner. He had kept his oath even after learning eventually that she had given birth to a baby girl. Karl Victor refuses to chair the tribunal when her case is heard, to keep intact his honour as a judge. During the trial, Berger asks for clemency because of the circumstances of the case. An orphan at 17, Victorine when to work as a governess for a heartless lady whose son seduced her. When he heard she was pregnant, he wrote a letter to his mother asking her to deal with the situation in the best possible way. The lady threw Victorine out of her castle in the middle of the night and she was found on the side of the road the next morning, unconscious and with her dead infant next to her. Victorine is nevertheless sentenced to death by the tribunal chaired by Werner. Karl Victor goes to see her in her cell with Berger. She agrees to see him because her mother had asked her to tell her father, if she ever was to see him, that she had forgiven him. Karl Victor presents a petition for clemency. A few weeks later, he learns that the request is rejected and that he is promoted to another city. He organises, with his servants Franz and Birgitta, Victorines escape during the banquet offered to thank him for his exemplary tenure as president of the citys tribunal. Before leaving the town to escape to a neighbouring country, he writes a letter tendering his resignation for health reasons.

Three years later, on a boat trip abroad, Berger meets Weyden, a plantation owner from Java. When the boat stops at Weydens destination, Berger recognises on the pier Victorine and her father who have come to welcome him. He asks Franz, who has come aboard to carry Weydens luggage, to tell Karl Victor that he will always remember him with love and respect. The following Sunday, Victorine and Weyden are married at the village church. The following day, Karl Victor goes back home and confesses to Werner, now president of the tribunal, that he had organised Victorines escape. Werner refuses to have him punished as the revelation of his crime would undermine confidence in the judiciary. When Karl Victor threatens to make a public confession, Werner tells him that if he does, he will find his daughter to have her sentence executed. During the following night, after writing his will, Karl Victor goes to the ruins of the old family castle to kill himself.

==Production and distribution==
The film was produced by Nordisk Film, and was shot during the summer of 1918 on the Gotland island in Sweden. It premièred in Sweden on 1 February 1919 and was released in Denmark on 9 February 1920.

==Cast==
* Carl Meyer as Rigsherre von Sendlingen
* Elith Pio as Franz Victor von Sendlingen
* Halvard Hoff as Karl Victor von Sendlingen
* Jacoba Jessen as Maika
* Axel Madsen as Vice-President Werner
* Richard Christensen as Georg Berger
* Olga Raphael Linden as Victorine Lippert
* Betty Kirkeby as Hermine Lippert
* Hallander Hellemann as Franz
* Fanny Petersen as Birgitta

==References==
 

==External links==
*  

 

 
 
 
 
 
 