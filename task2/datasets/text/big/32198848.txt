Behind the Wall (2008 film)
 
 Paul Schneider. James Thomas in the lead roles.

The film centres on a lighthouse in the town of Hinderson Bay which is haunted by an evil spirit from Katelyns past. Once its basement is broken open, new bloodshed starts, and the horrible truth about the past is gradually unveiled.

== Plot ==

For several generations the Parks family lived on a quiet hillside where Katelyns grandfather and her father (Mike Daly) were keepers to the towns lighthouse. At the age of ten, Katelyn (Lindy Booth) watched her mom enter the basement in their lighthouse home where minutes later she was brutally beaten and found dying on the floor. Her father, Christopher (Mike Daly), was arrested and convicted for the killing, and Katelyn placed in foster care out of town. The murder drove Christopher into madness and he was sentenced to a mental institution, where he later died. Katelyn never had a chance to speak to her father since that horrible night.
 James Thomas) arranges a deal with a contractor to develop the abandoned lighthouse for tourism. Father Hendry (Lawrence Dane)  fails to convince the town council to veto the project. The town wants to open the lighthouse back up but someone from Katelyns foggy past warns her to stop them! What they dont tell her is "why". Katelyn is forced to confront the evil that lurks within the lighthouse when some of the developers slowly begin to disappear. What she discovers is a family secret long buried by her grandfather. She must now unearth the truth and find a way to stop the nightmare which surrounds the lighthouse. The film ends with the murder of Father Hendry by the evil spirit, after which Katelyn sets the lighthouse to fire and leaves the town.

== Cast ==
* Lindy Booth as Katelyn Parks
* Lawrence Dane as Father Hendry James Thomas as Drew Cabot Andy Jones as Ray Sanders
* Brad Hodder as Eric Carrinton
* Suzie Pollard as Monica
* Jody Richardsonas Triggs Herman
* Julia Kennedy as 8 year old Katelyn
* Mike Daly as Christopher Parks
* Danny McLeod as Leon Ferguson
* Ruth Lawrence as Elaine Paul Rowe as Police Officer
* Geraldine Hollett as Tessa
* Sean Panting as Local
* Donnie Coady as Ridley Parks

== External links ==
*  
*  

 
 
 
 
 
 