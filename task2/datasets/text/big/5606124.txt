Good Luck Chuck
 
{{Infobox film
| name = Good Luck Chuck
| image = Good luck chuck ver4.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster Mark Helfrich
| producer = Mike Karz
| writer = Josh Stolberg
| starring = Dane Cook Jessica Alba Dan Fogler Chelan Simmons
| music = Aaron Zigman
| cinematography = Anthony B. Richmond Julia Wong
| studio = Karz Entertainment, Inc.
| distributor = Lionsgate
| released =  
| runtime = 101 minutes
| country = United States
| language = English
| budget = $25 million 
| gross = $59,192,128 
}} romantic comedy film starring Dane Cook and Jessica Alba. In the film, women find their "one true love" after having sex with a dentist named Chuck (Dane Cook|Cook).  Chuck meets a girl named Cam (Jessica Alba|Alba) and tries to become her true love.

The film opened in theaters on September 21, 2007, and was heavily panned by film critics|critics. One of Good Luck Chuck s theatrical posters parodied the well-known Rolling Stone cover photographed by Annie Leibovitz featuring John Lennon and Yoko Ono in similar poses.

==Plot== goth girl named Anisha (Sasha Pieterse). In retaliation, Anisha places a curse on Chuck, so that every single woman he sleeps with will break up with him and marry the next man who asks her out.

In the present, Chuck (Dane Cook) is a successful dentist in his thirties, and runs a dental practice in the same building as his best friend Stus (Dan Fogler) plastic surgery business. Chuck finds himself unable to tell his girlfriend, Carol (Chelan Simmons), that he loves her, and she breaks up with him while having sex on the beach. At a wedding, Chuck becomes enamored with Cam Wexler (Jessica Alba), an attractive and sometimes-clumsy, but-friendly marine biologist. Chuck asks Cam for a date, but she gently refuses him. While working at a penguin habitat, Cam accidentally slips and chips her tooth. When she visits Chuck to have it fixed, he asks her to go out with him instead of paying him. Though initially reluctant, Cam agrees. Meanwhile, Stu notices the pattern of girls getting married as soon as Chuck has sex with them. Stu eventually convinces Chuck to embrace the influx of women who have learned of his pattern and visit his practice, arguing that theres nothing better than having lots of guilt-free sex. However, after having this so-called "guilt-free sex" with numerous women, Chuck decides he wants a serious relationship with Cam. However, just before he has sex with her, Stu informs him that each of the women Chuck has slept with have got married, including Carol. Worried that the same thing will happen to Cam, Chuck begins to avoid her.

Stu convinces Chuck to test the curse by having sex with an obese woman, and see if she marries afterward. Chuck asks Stu to ask the woman out to see if this results in marriage, and when it does not, Chuck concludes that the curse is fake, and has sex with Cam. Afterward, however, Chuck discovers that the woman Stu was supposed to ask out got married to another man. Chuck calls Stu, who confesses that he only pretended to ask the woman. Chuck gets mad because of Stus treason but Stu doesnt care about the complaints. Chuck believes Cam wants to go out with Howard Blaine, who authored a book about penguins. Still convinced that Cam will hang out with him, Chuck tries desperately to get Cams attention and asks her to marry him. His attempts, though, cause Cam to become convinced hes stalking her and she breaks up with him. After the break-up, Chuck attempts to track down Anisha in order to break the curse. Now married with a child, Anisha reveals to Chuck that they were just kids back then, and the curse wasnt meant to be real.
 proposing to voodoo doll, which signifies that Chuck has finally got the girl of his dreams and broken the curse. A year later, Chuck and Cam are in Antarctica together surrounded by penguins. 

Later, Stu is shown with his now wife house-sitting for Chuck and Cam who are now married as well. They search for home-made sex tapes, and find a disturbing tape where Chuck is giving oral to a plush penguin while Cam is off-screen making sex sounds, implying that they might have made the tape and left it for Stu to find as a gag.

==Cast==
* Dane Cook (Connor Price, young) as Dr. Charles Logan
* Jessica Alba as Cam Wexler
* Dan Fogler (Troy Gentile, young) as Dr. Stuart Klaminsky
* Chelan Simmons as Carol
* Lonny Ross as Joe Wexler
* Ellia English as Reba
* Annie Wood as Lara
* Jodie Stewart as Eleanor Skepple
* Michelle Harrison (Sasha Pieterse, young) as Anisha Carpenter Jodelle Micah Ferland as Lila Carpenter
* Lindsay Maxwell as McTitty
* Crystal Lowe as Cams Wedding friend

==Reception==
The film was panned by critics. According to Rotten Tomatoes, only 5% of critics gave the film positive reviews, based on 114 reviews. The film was slightly better received by audiences, who gave it a 57% rotten rating.  On Metacritic, the film had an average score of 19 out of 100, based on 23 reviews, indicating "overwhelming dislike". 

Roger Ebert awarded the film 1 out of 4 stars, branding it "potty-mouthed and brain-damaged", whilst his reviewing partner, Richard Roeper also rated it poorly.  

The film earned two Razzie Award nominations including Worst Actress (Jessica Alba) and Worst Screen Couple (Alba and Dane Cook), but lost to Lindsay Lohan for I Know Who Killed Me.

===Box office===
The film was the second-highest grossing film at the U.S. box office in its opening weekend, grossing $13.6 million in 2,612 theaters.     The film went on to have a total box office tally of approximately $35 million U.S. and $24 million foreign.

==Production==
Good Luck Chuck was filmed from Late September to Mid-November 2006. The film was shot entirely in Edmonton, Alberta, using the Alberta Film Studio for the aquarium scenes and the neighborhood of Old Strathcona for exterior shots. 
==Soundtrack==
The soundtrack was released on 18 September 2007.
# "I Was Zapped by the Lucky Super Rainbow" (Flaming Lips)
# "Accident Prone" (The Honorary Title)
# "Good Luck Chuck" (The Dandy Warhols)
# "Love It When You Call" – Cherrytree House Version (The Feeling)
# "Good Weekend" (Art Brut)
# "Hurry Up Lets Go" (Shout Out Louds)
# "Shut Me Out" (Aidan Hawken) – 2:49
# "Youre Gonna Get It" (Sharon Jones & The Dap-Kings)
# "The Whistle Song" (Pepper (band)|Pepper)
# "You Might Think" (The Cars)
# "Physical (Olivia Newton-John song)|Physical" (Olivia Newton-John)
# "Bela Lugosis Dead" (Bauhaus)
# "Crazy in Love" (Antique Gold)

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 