A Time for Dying
{{Infobox film
| name           = A Time for Dying
| image          =
| image_size     =
| caption        =
| director       = Budd Boetticher
| producer       = Audie Murphy
| writer         = Budd Boetticher
| narrator       =
| starring       = Audie Murphy
| music          =
| cinematography = Lucien Ballard
| editing        = Harry Knapp
| studio         = FIPCO
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}} Western film starring Audie Murphy as Jesse James. This was Murphys last movie, as well as the final dramatic feature for director Budd Boetticher. 

==Plot==
Cass Bunning (Richard Lapp), a farm boy with a talent for shooting, meets up with Nellie (Anne Randall), a naive woman from the East, who has been lured West by the promise of a waitressing job which turns out to be in a brothel. Cass helps Nellie escape and two are forced into marriage with each other by Judge Roy Bean (Victor Jory). Cass decides to become a bounty hunter. He crosses with Jesse James (Audie Murphy) who, impressed by Cass shooting, suggests he join his gang, but Cass wants to stay at his new job. Cass is killed in a shoot out with the outlaw Billy Pimple (Bob Random), and Neillie is forced into prostitution.

==Cast==
*Richard Lapp as Cass Bunning
*Anne Randall as Nellie
*Audie Murphy as Jesse James
*Victor Jory as Judge Roy Bean
*Beatrice Kay as Mamie
*Bob Random as Billy Pimple
*Peter Brocco as Ed
*Burt Mustin as Seth

==Production==
Audie Murphys career was in a bad state and he had not made a film in 1968, the first year that happened since he started starring in films. Boetticher, who directed Murphy on The Cimarron Kid, was going through a similar slump. The two men formed their own company, Fipco, to make films. This was to be the first of several.

A Time for Dying was to originally star Peter Fonda as the kid.  Shooting took place near Tucson in April and May 1969. Money was tight and by the time filming was completed the movie was several minutes shorter than scripted. Murphy spent the next year and a half trying to raise additional funds for completion and post-production.

Owing to legal problems, the film did not screen in New York until 1982. 

==References==
 

==External links==
* 
*   at IMDB
*  at TCMDB

 
 

 
 
 
 
 
 
 


 