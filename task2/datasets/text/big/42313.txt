The Wicker Man (1973 film)
 
 
{{Infobox film
| name           = The Wicker Man
| image          = The Wicker Man (1973 film) UK poster.jpg
| alt            = 
| caption        = Theatrical release poster Robin Hardy
| producer       = Peter Snell Anthony Shaffer
| based on       =  
| starring       = Edward Woodward Britt Ekland Diane Cilento Ingrid Pitt Christopher Lee
| music          = Paul Giovanni
| cinematography = Harry Waxman
| editing        = Eric Boyd-Perkins
| studio         = British Lion Films
| distributor    = British Lion Films
| released       =  
| runtime        = 87 minutes  
| country        = United Kingdom
| language       = English
| budget         = £500,000 
}} mystery horror horror film Robin Hardy Anthony Shaffer. Celtic paganism.
 cult status Best Horror Film. The burning Wicker Man scene was No.&nbsp;45 on Bravo (US TV channel)|Bravos 100 Scariest Movie Moments. During the 2012 Summer Olympics opening ceremony, the film was included as part of a sequence that celebrated British cinema. In 2013, a copy of the original U.S. theatrical version was digitally restored and released.
 script treatment American remake spiritual sequel entitled The Wicker Tree was released to mixed reviews. This film was also directed by Hardy, and featured Lee in a cameo appearance. Hardy is currently developing his next film, The Wrath of the Gods, which will complete The Wicker Man Trilogy.    

==Plot== Hebridean island, pagan Celts|Celtic May Day celebrations, teach children of the phallic association of the maypole, and place frogs in their mouths to cure sore throats. The Islanders, including Rowans mother, attempt to thwart his investigation by claiming that Rowan never existed.

  from Ben Mòr Coigach]]
While staying at the Green Man Inn, Howie notices a series of photographs celebrating the annual harvest, each image featuring a young girl as the May Queen. The photograph of the latest celebration is suspiciously missing; the landlord tells him it was broken. The landlords daughter, Willow, attempts to seduce Howie, but he refuses to have premarital sex.
 Victorian agronomist. Lord Summerisle explains that his grandfather developed strains that would prosper in Climate of Scotland|Scotlands climate, and encouraged the belief that old gods would use the new strains to deliver the islanders from a meager livelihood.

Howie finds the missing harvest photograph, showing Rowan standing amidst empty boxes. His research reveals that when theres a poor harvest, the islanders make a sacrifice to ensure that the next will be bountiful. He comes to the conclusion that Rowan is alive and has been chosen for sacrifice. During the May Day celebration, Howie knocks out and ties up the innkeeper so he can steal his costume (that of Punch and Judy|Punch, the fool) and infiltrate the parade. When it seems the villagers are about to sacrifice Rowan, he cuts her free and flees with her. Theyre intercepted by the islanders, to whom Rowan happily returns.
 wicker man statue, set it ablaze and surround it, singing the Middle English folk song "Sumer Is Icumen In." Inside the wicker man, a terrified Howie recites Psalm 23, and prays to God. He damns the islanders as the wicker man collapses in flames, revealing the setting sun.

==Cast==
 
* Edward Woodward as Sgt. Howie
* Christopher Lee as Lord Summerisle
* Diane Cilento as Miss Rose
* Britt Ekland as Willow MacGregor
* Ingrid Pitt as Librarian
* Lindsay Kemp as Alder MacGregor (the landlord)
* Russell Waters as Harbour Master
* Aubrey Morris as Old Gardener/Gravedigger Irene Sunter as May Morrison
* Donald Eccles as T.H. Lennox Walter Carr as School Master
* Roy Boyd as Broome
* Peter Brewis as Musician Geraldine Cowper as Rowan Morrison John Sharp as Doctor Ewan
* John Hallam as Police Constable McTaggart Tony Roper as Postman
 

==Production==

===Background=== Hammer Horror the monster, Anthony Shaffer, Robin Hardy British Lion head Peter Snell became involved in the project. Shaffer had a series of conversations with Hardy, and the two decided that it would be fun to make a horror film centering on "old religion", in sharp contrast to the Hammer films they had both seen as horror film fans. 

Shaffer read the David Pinner novel Ritual (1967 novel)|Ritual, in which a devout Christian policeman is called to investigate what appears to be the ritual murder of a young girl in a rural village, and decided that it would serve well as the source material for the project. Pinner originally wrote Ritual as a film treatment for director Michael Winner, who had John Hurt in mind as a possible star.  Winner eventually declined the project, so Pinners agent convinced him to write Ritual as a novel instead.  Shaffer and Lee paid Pinner £15,000 for the rights to the novel, and Shaffer set to work on the screenplay. However, he soon decided that a direct adaptation would not work well, and began to craft a new story, using only the basic outline of the novel. 
 account of James Frazer. 

===Casting===
 , who has appeared in over 275 motion pictures, considers The Wicker Man his best film. ]] Michael York and David Hemmings.  In Britain, Woodward was best known for the role of Callan (TV series)|Callan, which he played from 1967 to 1972. After The Wicker Man, Woodward went on to receive international attention for his roles in the 1980 film Breaker Morant (film)|Breaker Morant and the 1980s TV series The Equalizer.

After Shaffer saw her on the stage, he lured Diane Cilento out of semi-retirement  to play the towns schoolmistress.  (They lived together in Queensland from 1975, and married in 1985.) Ingrid Pitt, another British horror film veteran, was cast as the town librarian and registrar. The Swedish actress Britt Ekland was cast as the innkeepers lascivious daughter, although a body double was used for her naked scenes and her dialogue was later dubbed.  She mimed Willows Song which was sung by Scottish singer Annie Ross. 

===Filming===
The film was produced at a time of crisis for the British film industry. The studio in charge of production, British Lion Films, was in financial trouble and was bought by wealthy businessman John Bentley. To convince the  .
 The Old Man of Storr and the Quiraing. The amphibious aircraft that takes Sergeant Howie from the religious certainties of the mainland to the ancient beliefs of the island was a Thurston Teal, owned and flown in the aerial sequences by Christopher Murphy. The end burning of the Wicker Man occurred at Burrow Head (on a caravan site).  According to Britt Ekland, some animals did actually perish inside the Wicker Man,  whereas Robin Hardy said in an interview that great care was taken to ensure that the animals were in no danger of being hurt during this scene and that they were not inside the Wicker Man when it was set on fire. 

===Music===
 
The films soundtrack often forms a major component of the narrative, just as with other important arthouse films of the era such as Donald Cammell and Nicolas Roegs Performance (film)|Performance.  Songs accompany many important scenes, such as the planes arrival, Willows dancing, the maypole dance, the girls jumping through fire, the search of the houses, the procession, and the final burning scene. Indeed, according to Seamus Flannery in a subsequent documentary, director Robin Hardy surprised the cast by suddenly announcing midway through filming that they were making a "musical".

Composed, arranged and recorded by Paul Giovanni and Magnet (band)|Magnet, the soundtrack contains folk songs performed by characters in the film. The songs vary between traditional songs, original Giovanni compositions and even nursery rhyme in "Baa, Baa, Black Sheep (nursery rhyme)|Baa, Baa, Black Sheep".
 The Burning Season. The Mock Turtles released a version of the song on their album Turtle Soup.

The songs on the soundtrack were not actual cult songs used by pagans. All the songs were composed by Paul Giovanni, except in instances where he used well-known lyrics such as the words from the rhyme "Baa, Baa, Black Sheep".  The song sung by the cultists of Summer Isle at the end of the film, "Sumer Is Icumen In" is a mid-13th century song about nature in spring.

==Distribution==
 
By the time of the films completion the studio had been bought by EMI Films|EMI, and British Lion was managed by Michael Deeley. The DVD commentary track states that studio executives suggested a more "upbeat" ending to the film, in which a sudden rain puts the flames of the wicker man out and spares Howies life, but this suggestion was refused. Hardy subsequently had to remove about 20&nbsp;minutes of scenes on the mainland, early investigations, and (to Lees disappointment) some of Lord Summerisles initial meeting with Howie. 

A copy of a finished, 99-minute film    was sent to American film producer Roger Corman in Hollywood to make a judgment of how to market the film in the USA. Corman recommended an additional 13&nbsp;minutes be cut from the film. (Corman did not acquire US release rights, and eventually Warner Bros. test-marketed the film in drive-ins.) In Britain, the film was ordered reduced to roughly 87&nbsp;minutes, with some narrative restructuring, and released as the B-movie|"B" picture on a double bill with Dont Look Now. Despite Lees claims that the cuts had adversely affected the films Continuity (fiction)|continuity, he urged local critics to see the film, even going so far as to offer to pay for their seats.

===Restorations=== M4 motorway" in his Moviedrome introduction of 1988.    Hardy remembered that a copy of the film, prior to Deeleys cuts, was sent to Roger Corman; it turned out that Corman still had a copy, possibly the only existing print of Hardys version. The US rights had been sold by Warner Bros. to a small firm called Abraxas, managed by film buff Stirling Smith and critic John Alan Simon. Stirling agreed to an American re-release of Hardys reconstructed version. Hardy restored the narrative structure, some of the erotic elements which had been excised, and a very brief pre-title segment of Howie on the mainland (appearing at a church with his fiancée). A 96-minute restored version was released in January 1979,    again to critical acclaim. Strangely, the original full-length film was available in the US on VHS home video from Media Home Entertainment (and later, Magnum) during the 1980s and 1990s. This video included additional, early scenes in Howies police station that Hardy had left out of the 1979 version.

During 2001, the films new worldwide rights owners, Canal+ Group|Canal+, began an effort to release the full-length film. Cormans full-length film copy had been lost, but a telecine transfer to 1-inch videotape existed. With this copy, missing elements were combined with film elements from the previous versions. (In particular, additional scenes of Howie on the mainland were restored, showing the chaste bachelor to be the object of gossip at his police station, and establishing his rigidly devout posture.) The DVD "Extended version" released by Canal+ (with Anchor Bay Entertainment handling US DVD distribution) is this hybrid version, considered the longest and closest version to Hardys original, 99-minute version of the film.  A two-disc limited edition set was sold with both the shortened, theatrical release version and the newly restored extended version, and a retrospective documentary, The Wicker Man Enigma.    In 2005, Inside The Wicker Man author Allan Brown revealed he had discovered a series of stills taken on-set during the films production showing the shooting of a number of sequences from the script that had never been seen before; indeed, it had never been certain that these scenes had actually been filmed. They include a scene in which Howie closes a mainland pub that is open after hours, has an encounter with a prostitute, receives a massage from Willow McGregor and observes a brutal confrontation between Oak and a villager in The Green Man pub. These images were featured in a revised edition of the book Inside The Wicker Man.

Anchor Bay Entertainment released a limited edition wooden box of The Wicker Man. Fifty thousand two-disc sets were made, of which 20 were signed by actors Lee and Woodward, writer Shaffer, producer Snell, and director Hardy.

In June 2007, Lee discussed the lost original cut of the film. "I still believe it exists somewhere, in cans, with no name. I still believe that. But nobodys ever seen it since, so we couldnt re-cut it, re-edit it, which was what I wanted to do. It would have been ten times as good." 

European distributors of the film StudioCanal began a Facebook campaign in 2013 to find missing material, which culminated in the discovery of a 92-minute 35mm print at the Harvard Film Archive. This print had previously been known as the "Middle Version" and was itself assembled from a 35mm print of the original edit Robin Hardy had made in the United Kingdom in 1973, but which was never released.    Robin Hardy believes that the original edit will probably never be found, saying, "Sadly, it seems as though this has been lost forever. However, I am delighted that a 1979 Abraxas print has been found as I also put together this cut myself, and it crucially restores the story order to that which I had originally intended."  

Hardy reported in July 2013   Moviemail website, 22 July 2013. Retrieved 22 July 2013.  that Studiocanal intended to restore and release the most complete version possible of the film. Rialto Pictures announced that they were to release the new digital restoration in North American cinemas on 27 September 2013.  This new version was also released on DVD on 13 October 2013.    It is 91 minutes long, shorter than the directors cut but longer than the theatrical cut, and is known as The Wicker Man: The Final Cut. 

===Home media===
The Final Cut (UK) Blu-ray  features Burnt Offering: The Cult of the Wicker Man, Worshipping The Wicker Man, The Music of The Wicker Man, interviews with director Robin Hardy and actor Christopher Lee, a restoration comparison, and the theatrical trailer. The second disc features both the UK theatrical cut and the directors cut, along with an audio commentary on the directors cut and a making-of for the commentary. The third disc is the films soundtrack:

;Track listing
# Corn Rigs (2:37)
# The Landlords Daughter (2:40)
# Gently Johnny (3:33)
# Maypole (2:46)
# Fire Leap (1:29)
# The Tinker of Rye (1:52)
# Willows Song (4:43)
# Procession (2:17)
# Chop Chop (1:44)
# Lullaby (0:58)
# Festival / Mirie It Is / Sumer Is Icumen In|Summer is A-comen In (4:30)
# Opening Music / Loving Couples / The Ruined Church (4:17)
# Masks / Hobby Horse (1:25)
# Searching for Rowan (2:23)
# Appointment with the Wicker Man (1:18)
# Sunset (1:05)

==Reception==
The Wicker Man had moderate success and won first prize in the 1974 Festival of Fantastic Films in Paris, but largely slipped into obscurity. In 1977 the American film magazine Cinefantastique devoted a commemorative issue to the film,  asserting that the film is "the Citizen Kane of horror movies" – an oft-quoted phrase attributed to this issue. 

In 2003, the Crichton Campus of the University of Glasgow in Dumfries, Dumfries and Galloway hosted a three-day conference on The Wicker Man.  The conference led to two collections of articles about the film.

In 2006, The Wicker Man ranked 45th on Bravo (US TV channel)|Bravos 100 Scariest Movie Moments. 

Decades after its release, the film still receives positive reviews from critics and is considered one of the best films of 1973.    The film currently holds an 90% "Fresh" rating on the review aggregate website Rotten Tomatoes.  In 2008, The Wicker Man was ranked by Empire Magazine as 485th of The 500 Greatest Movies of All Time.  Christopher Lee considers The Wicker Man his best film.    Edward Woodward meanwhile has said that The Wicker Man is one of his favourite films and that the character of Howie was the best part he ever played. In addition to Lees admiration of the final shot of the film (of the collapsing Wicker man), Woodward said that it is the best final shot of any film ever made. 
 Witchfinder General and 1971s The Blood on Satans Claw.    

==Related productions==
 
;Novel The Wicker Man, was released in 1978. It was written by Hardy and Shaffer.

;Possible sequel script treatment fantastical in subject matter than the original film, and relied more heavily on special effects. In this continuation of the story, which begins immediately after the ending of the first film, Sergeant Neil Howie is rescued from the burning Wicker Man by a group of police officers from the mainland. Howie sets out to bring Lord Summerisle and his pagan followers to justice,  but becomes embroiled in a series of challenges which pit the old gods against his own Christian faith. The script culminates in a climactic battle between Howie and a fire-breathing dragon – the titular Lambton Worm – and ends with a suicidal Howie plunging to his death from a cliff while tied to two large eagles.  Shaffers sequel was never produced, but his treatment, complete with illustrations, was eventually published in the companion book Inside The Wicker Man. 

Hardy was not asked to direct the sequel, and never read the script, as he did not like the idea of Howie surviving the sacrifice, or the fact that the actors would have aged by twenty to thirty years between the two films.  In May 2010, Hardy discussed The Loathsome Lambton Worm. "I know Tony did write that, but I dont think anyone particularly liked it, or it would have been made." 

;Remake American remake of the same name, starring Nicolas Cage and Ellen Burstyn, and directed by Neil LaBute, was released on 1 September 2006. Hardy expressed concern about the remake.  After its release, Hardy simply described it as a different film rather than a remake.  The remake was panned critically and was a failure at the box office. Today, it has a significant cult following as an unintentional comedy, with several scenes on YouTube boasting Cage brutalising various women throughout and terrorising children, a fan-made comedy trailer of the film, and more. 

;Stage production
A stage adaptation was announced for the 2009 Edinburgh Festival Fringe,  and was directed by Andrew Steggall. The production was based on Anthony Shaffers original The Wicker Man script and David Pinners novel Ritual (1967 novel)|Ritual. Robin Hardy gave input on the project, and original songs and music from the film were supervised by Gary Carpenter, the original music director.   Workshop rehearsals were held at The Drill Hall in London in March 2008,  and a casting call was held in Glasgow in May 2009.   After three weeks at the Pleasance in Edinburgh in August 2009, the production was to visit the Perth Rep, the Eden Court Theatre in Inverness, and then have a short run at Citizens Theatre in Glasgow, with hopes for a run in London in 2010.   However, in July 2009 it was announced that the production had been cancelled, three weeks before it had been due to preview. 

The National Theatre of Scotland then produced a musical adaptation of The Wicker Man called An Appointment with the Wicker Man. Written by Greg Hemphill and Donald McCleary, the story involves a local communitys attempt to stage a Wicker Man play. 

;Spiritual sequel spiritual sequel The Wicker evangelists who travel to Scotland; like Woodwards character in The Wicker Man, the two Americans are virgins who encounter a pagan laird and his followers. 

Those involved in the production of the film have given conflicting statements regarding the identity of Christopher Lees character, referred to only as "Old Gentleman" in the credits. Writer–director Robin Hardy has stated that the ambiguity was intentional, but that fans of The Wicker Man will immediately recognise Lees character as Lord Summerisle.  Lee himself has contradicted this, stating that the two are not meant to be the same character, and that The Wicker Tree is not a sequel in any way. 

;Possible graphic novel
As a former artist, Hardy has expressed great interest in the medium of comics, and is currently planning a graphic novel which retells the story of The Wicker Man, based on his own storyboards for the film. Hardy is in talks with yet unnamed artists to work on the project, as he finds it too difficult to make the characters look consistent from one panel to the next, and is busy producing and directing The Wrath of the Gods, the third installment of The Wicker Man Trilogy. He intended the graphic novel and the new film to be released at the same time in autumn 2013; however as of autumn 2014 neither has been released. 

==References==
 
*  
*  

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 