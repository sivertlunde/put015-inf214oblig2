Giselle (film)
 
{{Infobox film
| name           = Giselle
| image          = 
| caption        = 
| director       = Toa Fraser
| producer       = 
| writer         = Toa Fraser
| starring       = Gillian Murphy
| music          = 
| cinematography = Leon Narbey
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = New Zealand
| language       = English
| budget         = 
}}

Giselle is a 2013 New Zealand drama film written and directed by Toa Fraser. It was screened in the Contemporary World Cinema section at the 2013 Toronto International Film Festival.       The film is based on the 2012 production Giselle, performed by The Royal New Zealand Ballet which many of the cast members are part of. 

==Cast==
* Gillian Murphy as Giselle
* Abigail Boyle as Myrtha
* Jacob Chown as Hilarion
* Maclean Hopper as Wilfrid
* Qi Huan as Albrecht

==References==
 

==External links==
*  
*   at NZ On Screen

 
 
 
 
 
 
 
 