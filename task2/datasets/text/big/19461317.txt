Doña Perfecta (film)
{{Infobox Film
  | name = Doña Perfecta
  | director = Alejandro Galindo
  | producer = Alejandro Galindo
  | writer = Benito Pérez Galdós Iñigo de Martino
  | starring = Dolores del Río Carlos Navarro Esther Fernández Julio Villarreal
  | music =
  | cinematography =
  | editing = Columbia Cabrera Films
  | released = October 10, 1951
  | runtime = 97 minutes
  | country = México
  | language = Spanish
  | budget =
    }} 1951 Cinema Mexican film version of the famous novel by Benito Pérez Galdós, directed by Alejandro Galindo and starring Dolores del Río.

==Plot summary==
The action occurs in 19th century México, when a young liberal named Don José (Pepe) Rey, arrives in a city, with the intention of marrying his cousin Rosario. This was a marriage of convenience arranged between Pepes father Juan and Juans sister, Perfecta (Dolores del Río).

Upon getting to know each other, Pepe and Rosario declare their eternal love, but in steps Don Inocencio, the cathedral canon, who meddles and obstructs the marriage as well as the good intentions of Doña Perfecta and her brother Don Juan. Over the course of time, several events lead up to a confrontation between Pepe Rey and his aunt Perfecta, which is caused by her refusal to allow Pepe and Rosario to marry, because Pepe is a non-believer.

==External links==
* 

 
 
 
 
 
 


 