Battlefield Heroes (film)
{{Infobox film name           = Battlefield Heroes image          = Battlefield-heroes-poster.jpg alt            =  caption        = Theatrical release poster film name      = {{Film name hangul         =   hanja          =   rr             = Pyeongyangseong mr             = P‘yŏngyangsŏng}} director       = Lee Joon-ik  producer       = Oh Seung-hyeon Jo Cheol-hyeon Lee Jeong-se  writer         = Oh Seung-hyeon Jo Cheol-hyeon  starring       = Jung Jin-young Lee Moon-sik Ryu Seung-ryong Yoon Je-moon music          = Kim Jun-seok  cinematography = Chung Chung-hoon  editing        = Kim Sang-beom Kim Jae-beom  studio         = Achim Pictures Tiger Pictures  distributor    = Lotte Entertainment released       =   runtime        = 118 minutes  country        = South Korea  language       = Korean  budget         =  gross          = 
}}
  Shilla against the larger northern Korean state of Goguryeo. The films box office returns were lower than expected in South Korea, which prompted Lee Joon-ik to announce his retirement shortly after the films release. The film has been shown at the New York Asian Film Festival and Fantasia Festival.  

== Plot == Shilla and makes a deal with Chinas Tang dynasty officials to have a combined strike against the larger northern Korean state of Goguryeo. The conditions of the agreement involve Shilla being given back the Korean state of Baekje. The combined troops march to Pyongyang Castle, where Goguryeos Yeon Gaesomun (Lee Won-jong) dies and hands over command of the army to his second son Yeon Nam-geon (Ryu Seung-ryong). This action upsets his first son, Yeon Namsaeng (Yoon Je-moon) who is not as war-hungry as Nam-geon.

The Goguryeo soldiers defending the castle succeed in fighting off the Allied Armys first assault by catapulting honey and bees onto the Shilla soldiers. Meanwhile, the Shilla grand general Kim Yushin (Jung Jin-young) holds back sending his main force to join the advance Allied Army, preferring to deal directly with Yeon Gaesomuns sons than the Chinese. Yeon Namsaeng is expelled from the castle by his elder brother. The Chinese commander Yi launches a full-scale attack on the castle but is beaten back by the Goguryeo secret weapon. An allied soldier, Thingamajig (Lee Moon-sik) from Baekje, is captured. Thingamajig, who has suffered under Chinese rule, broadcasts a demoralizing message to the Allied Army. Thingamajig is rewarded by being allowed to marry the brave Goguryeo female warrior, Gap-sun (Sunwoo Sun), against her will.

==Cast==
*Jung Jin-young
*Lee Moon-sik
*Ryu Seung-ryong
*Yoon Je-moon
*Lee Kwang-soo
*Lee Won-jong
*Sunwoo Sun

==Production==
Battlefield Heroes is a sequel to Once Upon a Time in a Battlefield set eight years after the first film.  The film was inspired by the political situation in Korea in 1995.    Lee Joon-ik stated he wanted that both Once Upon a Time in a Battlefield and Battlefield Heroes show how influenced Korea was by the countries around it (specifically China, Japan and the United States) which caused a lot of internal conflicts within Korea. 

==Release==
Battlefield Heroes was released in South Korea on January 27, 2011.    The box office returns in Korea were not strong which lead to Lee Joon-ik announcing his retirement from directing shortly after.  Lee announced his retirement through his Twitter account, stating "I’m quitting directing because the audience   was just 1.7 million, far fewer than the 2.5 million we had expected."     

The film was shown at film festivals, including the New York Asian Film Festival in July 2011.   It had its Canadian premiere on July 31, 2011 at the Fantasia Festival.   

==Reception==
The Los Angeles Times wrote a generally favorable review of Battlefield Heroes, stating that the film "is a handsome, sweeping period picture, a robust, earthy comedy in which the humor, though sometimes labored, results in an amiable if lengthy entertainment".  Film Business Asia gave the film an eight out of ten rating stating that director had the director had grown "in leaps and bounds as a technically confident film-maker" since the film King and the Clown. The review went on to state that an international audience would benefit "by an introductory caption explaining the story set-up in simple terms, as well as some slight trimming throughout". 

==References==
 

==External links==
*   
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 