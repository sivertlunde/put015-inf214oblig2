Sleep Tight (film)
{{Infobox film
| name           = Sleep Tight
| image          = Sleep Tight.jpg
| caption        = Theatrical release poster
| director       = Jaume Balagueró
| producer       = Julio Fernández
| writer         = Alberto Marini
| narrator       = 
| starring       =  
| music          = Lucas Vidal
| cinematography = Pablo Rosso
| editing        = Guillermo De La Cal 
| studio        =  
| distributor    = 
| released       =  
| runtime        = 102 min
| country        = Spain
| language       = Spanish
| budget         = $5,000,000
| gross          = 
}} 
Sleep Tight ( , "While You Sleep") is a Spanish horror thriller film directed by Jaume Balagueró and was written by Alberto Marini.  The film was developed under the title Flatmate.

==Plot==
Apartment concierge Cesar (Luis Tosar) is a miserable person who believes he was born without the ability to be happy.  As a result, he decides his mission is to make life hell for everyone around him.  A majority of the tenants are easy to agitate, but Clara (Marta Etura) proves to be harder than the most.  So Cesar goes to creepy extremes to make this young woman mentally break down.  Things get even more complicated in this twisted relationship when her boyfriend, Marcos (Alberto San Juan), shows up.

==Cast==
* Luis Tosar as César
* Marta Etura as Clara
* Alberto San Juan as Marcos
* Pep Tosar as Úrsulas father
* Petra Martínez as Verónica
* Amparo Fernández as The cleaning lady
* Iris Almeida as Úrsula
* Roger Morilla as The cleaning boy
* Margarita Rosed as Césars mother
* Manel Dueso as The commissioner

==Production==
Filmax narrated the film on the 2010 Cannes Film Festival  and named Jaume Balagueró as director.  Alberto Marini wrote the script of the film.  Balagueró cast in early May 2010 Luís Tosar  and Marta Etura for the leads.  Filmax and Balagueró filmed the project in Barcelona, Catalonia.  It is Balaguerós first film since Fragile (film)|Fragile alone on the directing chair. 

==Release==
The United States release was in 2012. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 