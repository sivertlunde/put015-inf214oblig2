The Far Horizons
{{Infobox film
| name = The Far Horizons
| image = The Far Horizons - 1955 - Poster.png
| caption = 1955 Theatrical Image
| director = Rudolph Maté
| producer = William H. Pine
| writer =  Della Gould Emmons (novel) Winston Miller
| starring = Fred MacMurray Charlton Heston Donna Reed Barbara Hale
| music = Hans J. Salter
| cinematography =  Daniel L. Fapp
| editing = Frank Bracht
| studio = Pine-Thomas Productions
| distributor = Paramount Pictures
| released =  
| runtime = 108 min.
| language = English
| budget =
| gross = $1.6 million (US) 
}} expedition led by Lewis and Clark is sent to survey the territory that the United States has just acquired in the Louisiana Purchase from France. They are able to overcome the dangers they encounter along the way with the help of a Shoshone woman named Sacagawea.  This is currently the only major American motion picture on the Lewis and Clark expedition (although there have been television documentaries on the subject).  Many details are fictional, and the minor scene where the group reaches the Pacific Ocean reflects the low budget of the film. 

==Plot==
An ambitious, historic attempt to explore and document an untamed American frontier unfolds in this rousing adventure drama. In 1803, Meriwether Lewis and William Clark, with President Thomas Jeffersons blessing, embarked on the government-sponsored Lewis & Clark Expedition – an attempt to discover a water route connecting St. Louis, Missouri, with the Pacific Ocean. Their trek takes them through the magnificent, danger-filled territory of the Pacific Northwest, with guidance from the Shoshone woman Sacagawea.

==Cast==
As appearing in screen credits (main roles identified):   

*Fred MacMurray as Captain Meriwether Lewis 
*Charlton Heston as William Clark (explorer)|Lt. William Clark
*Donna Reed as Sacagawea
*Barbara Hale as Julia Hancock
*William Demarest as Patrick Gass|Sgt. Gass  Charbonneau  Eduardo Noriega as Cameahwait
*Larry Pennell as Wild Eagle 
*Julia Montoya as Crow woman 
*Ralph Moody as Le Borgne 
*Herbert Heyes as President Thomas Jefferson
*Lester Matthews as Mr. Hancock
*Helen Wallace as  Mrs. Marsha Hancock Walter Reed as Cruzatte (helmsman)

==Reception== Native American Sacagawea, and the creation of a romantic subplot between her character and William Clark despite the fact that Sacagaweas husband, French-Canadian trader Toussaint Charbonneau, was in real life also a member of the expedition. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 