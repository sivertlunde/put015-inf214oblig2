Biskett
{{Infobox film
| name           = Biskett
| image          =
| director       = Anil Gopi Reddy
| producer       = Sravanthi and Raj of Godavari Productions
| music          = Anil Gopi Reddy
| cinematography = Jayapal Reddy
| editing        = Madhu Reddy
| released       =  
| runtime        =
| country        = India
| language       = Telugu
}} Arvind Krishna is the hero of the film while Dimple Chopade is the leading lady. Jayapal Reddy is the cinematographer and Madhu Reddy is the editor of this film. 

== Cast == Arvind Krishna....Ashwin
* Dimple Chopade (Deeksha)
* Vennela Kishore (Chittilingam)
* M.S. Narayana Ali
* Chalapathi Rao
* Thagubothu Ramesh  Ajay
* Bharat

== Soundtrack  ==
The audio launch of the film happened in the month of October which was a gala affair and was attended by celebrities like Dil Raju, Veerabhadram Chowdary, Rajam of Namasthe Telangana and state minister D.K. Aruna among others followed by the rest of the cast and crew. 

Director Anil Gopireddy also composed tunes for the film. The response to the songs of this film has been very good. 

== Development  ==
The film is set to release on December 27, 2013. 

== References  ==
 

 
 
 


 