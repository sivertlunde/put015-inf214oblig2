Pride and Vengeance
 
{{Infobox film
| name           = Pride and Vengeance
| image	         = Pride and Vengeance FilmPoster.jpeg
| caption        = Film poster
| director       = Luigi Bazzoni
| producer       = 
| writer         = Luigi Bazzoni Suso Cecchi dAmico Prosper Mérimée
| starring       = Franco Nero
| music          = 
| cinematography = Camillo Bazzoni
| editing        = Roberto Perpignani
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Italy West Germany
| language       = Italian
| budget         = 
}}
 Western film directed by Luigi Bazzoni and starring Franco Nero.    The film is a spaghetti western version of the novella Carmen by Prosper Mérimée.   

==Cast==
* Franco Nero as Don José
* Tina Aumont as Carmen
* Klaus Kinski as Lt. Miguel Garcia (as Klaus Kinsky)
* Guido Lollobrigida as Tanquiero (as Lee Burton)
* Franco Ressel as Captain
* Karl Schönböck as English Diplomat
* Alberto DellAcqua as Remendado
* Marcella Valeri as Dorotea
* Maria Mizar
* Mara Carisi
* Anna De Padova
* Tino Boriani
* Giovanni Ivan Scratuglia (as Ivan Giovanni Scratuglia)
* Aldo Vasco
* Hans Albrecht
* Luisa De Padova
* José Manuel Martín as Juan

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 