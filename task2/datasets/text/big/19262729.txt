Bear Island (film)
 
{{Infobox film
| name           = Bear Island
| image          =Bear-Island.jpg
| caption        = Cinema poster
| director       = Don Sharp
| producer       = {{plainlist|
* Peter Snell
* William Hill
}}
| writer         = {{plainlist|
* Alistair MacLean
* Murray Smith
* David Butler
* Don Sharp
}}
| starring       = {{plainlist|
* Donald Sutherland
* Vanessa Redgrave
* Richard Widmark
* Christopher Lee
* Lloyd Bridges
}}
| music          = Robert Farnon
| cinematography = Alan Hume
| editing        = Tony Lower
| distributor    = {{plainlist|
* United Artists
* Columbia Pictures
}}
| released =  
| runtime        = 118 minutes
| country        = United Kingdom Canada English
| budget         = $CAD12,100,000 (estimated) or $9.3 million Bear Island: The Film That Stayed out in the Cold
Adilman, Sid . Los Angeles Times (1923-Current File)   11 Mar 1979: m6. 
| gross          =
}} Bear Island by Alistair MacLean. It was directed by Don Sharp and starred Donald Sutherland, Vanessa Redgrave, Richard Widmark, Christopher Lee and Lloyd Bridges.

==Plot== Bear Island, between Svalbard and northern Norway, to study climate change. However, several of them turn out to be more interested in the fact that (according to the film) there was a German U-boat base on the island during World War II. American scientist Frank Lansing (Donald Sutherland) has come because his father was a U-boat commander who died there, and as accidents start to decimate the expedition he begins to realize that some of his colleagues are after a shipment of gold aboard the U-boat that his father commanded.

==Cast==
* Donald Sutherland – Frank Lansing
* Vanessa Redgrave – Heddi Lindquist
* Richard Widmark – Otto Gerran
* Christopher Lee – Lechinski
* Lloyd Bridges – Smithy
* Bruce Greenwood – Technician Tommy
* Barbara Parkins – Judith Rubin
* Patricia Collins – Inge Van Zipper Mark Jones Cook
* Marine Technician
* Candace OConnor – Laboratory Assistant Captain
* Michael J. Reynolds – Heyter
* Lawrence Dane – Paul Hartman
* Nicholas Cortland – Jungbeck
* Joseph Golland – Meteorological Assistant
* Richard Wren – Radio Operator
* Hagan Beggs – Larsen
* Robert Stelmach – Ships Radio Operator
* Terry Waterhouse – Helicopter Crewman

==Production notes== Glacier Bay National Park in Alaska, depicting a much more dramatic landscape than the real Bear Island offers. 

According to the book The Hollywood Hall of Shame it was the most expensive film ever made in Canada up till then.  Of the budget, $3.3 million came from the British arm of Columbia Pictures, $3 million from Selkirk Film Holdings, $1.8 million from the Toronto Dominion Bank, $1.2 million from the Bank of Montreal, and $100,000 from the Canadian Film Development Corporation. 
 Swedish invention called Larven (The Caterpillar) by Lennart Nilsson is used in the chases around the island.

The film is rated R13 in New Zealand as it contains violence.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 