Sunflower (2005 film)
{{Infobox film
| name           = Sunflower
| image          = Sunflower Poster.jpg
| caption        = Zhang Yang
| producer       = Han Sanping Peter Loehr Yang Buting Zhang Yang
| narrator       = Zhang Fan Gao Ge Wang Haidi
| music          = Lin Hai
| cinematography = Lin Jong
| editing        = Yang Hongyu
| distributor    = Fortissimo Films
| released       =  
| runtime        = 129 minutes
| country        = China
| language       = Mandarin
| budget         = 
}} Chinese film Zhang Yang. Zhangs fourth film, Sunflower is a joint production of Ming Productions, the Beijing Film Studio (as part of the China Film Corporations 4th Production Company) and the Hong Kong subsidiary of the Netherlands-based Fortissimo Films. It was distributed by Fortissimo Films and New Yorker Films (US theatrical distribution).   
 Zhang Fan, Gao Ge and Wang Haidi as their son over the course of 30 years.

== Cast ==
*Sun Haiying as Zhang Gengnian, a Beijing painter whose spirit is crushed during his time in a labor camp during the Cultural Revolution.
*Joan Chen as Xiuqing, Zhang Gengnians wife.
*Liu Zifeng as Lao Liu, Zhang Gengnians best friend and fellow artist. Zhang Fan as Zhang Xiangyang, Zhang Gengnians son as an 8-year-old boy.
*Gao Ge as Zhang Xiangyang as a 19-year-old man.
*Wang Haidi as Zhang Xiangyang as a 30-year-old man. Liang Jing as Han Jing, Xiangyangs wife.
*Hong Yihao as Chicken Droppings, Xiangyangs friend as a 9-year-old boy. Li Bin as Chicken Droppings as a 19-year-old young man.

== Plot ==
The film is split into three main segments spanning thirty years. The first segment, in 1976, begins with the return of an artist, Gengnian to his wife, Xiuqing and son after several years of re-education during the Cultural Revolution. He returns however, with injured hands and can no longer continue as an artist. He instead hopes to cultivate artistic aspirations in his son, Xiangyang, who has taken to hurling stones at strangers with his friend Chicken Droppings. Though he eventually follows in his fathers footsteps, Xiangyang resents his fathers pressure and the two soon fall out.

The next segments, in 1987 when Xiangyang is a 19-year-old, and 1999, when he is in his 30s, continue to chart the course of Xiangyang and Gengnians tense relationship. It is a moving film depicting the tension between Xianyang and his parents and also the marital relationship between his parents. The sunflower returns throughout the movie as a theme.

== Releases ==
Sunflower received its international premiere at the 2005 Toronto International Film Festival on September 10, 2005.

The film was also shown at several international film festivals.

*2005 Toronto International Film Festival - Contemporary World Cinema September 10, 2005
*2005 Chicago International Film Festival, October 10, 2005   
*2005 San Sebastian Film Festival
*2005 Pusan International Film Festival - Window on Asian Cinema
*2006 Hong Kong International Film Festival,  April 11, 2006 
*2006 Bangkok International Film Festival, - Windows on the World, February 19, 2006  2006 Tokyo International Film Festival, July 8, 2006 
*2007 Pacific Rim Film Festival, October 10, 2007 

Sunflower was also given a limited released in New York City on August 17, 2007.    Playing on only one screen, the film has made less than $24,000 dollars in the US as contrasted to over $8 million from foreign releases. 

== Reception ==
Sunflower was shown at several international film festivals. It ultimately won the Silver Shell for Best Director and the Jury Award for Best Photography at the San Sebastián International Film Festival in 2006. 

The film has garnered a 64 "fresh" rating from review database Rotten Tomatoes,  and a rating of 64 from Metacritic. 

== References ==
 

== External links ==
*   from international distributor Fortissimo Films
*   from US distributor New Yorker Films
*  
*  
*  
*  
*  
*   from the Chinese Movie Database

 

 
 
 
 
 
 