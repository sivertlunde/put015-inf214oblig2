Tell No One
 
{{Infobox film
| name           = Tell No One
| image          = Tell No One (2006).jpg
| image_size     = 215px
| alt            = 
| caption        = UK release poster
| director       = Guillaume Canet
| producer       = {{Plain list |
* Luc Besson
* Pierre-Ange Le Pogam
* Alain Attal
}}
| screenplay     = {{Plain list |
* Guillaume Canet Philippe Lefebvre
}}
| based on       =  
| starring       = {{Plain list |
* François Cluzet
* Marie-Josée Croze
* Marina Hands
* Kristin Scott Thomas
* Nathalie Baye
}}
| music          = Matthieu Chedid
| cinematography = Christophe Offenstein
| editing        = Hervé de Luze
| studio         = Canal+  CinéCinéma Classic|CinéCinéma
| distributor    = Europa Corp.|EuropaCorp. Distribution    Music Box Films  
| released       =  
| runtime        = 131 minutes   
| country        = France
| language       = French
| budget         = Euro|€11.7 million
| gross          = $33,385,185 	 
}}
Tell No One (Ne Le Dis À Personne) is a 2006 French  ), Best Actor (François Cluzet), Best Editing and Best Music Written for a Film.

==Plot==
Alexandre Beck is a doctor who has slowly been putting his life back together after his wife Margot was murdered by a serial killer. Eight years on, Alex is doing well, until he finds himself implicated in a double homicide, which has plenty of evidence pointing to him as the killer – though he knows nothing of the crimes. The same day, Alex receives an e-mail that appears to be from Margot, which includes a link to a surveillance video clip that features his late wife looking alive and well. The message warns Alex that they are both being watched. He struggles to stay one step ahead of the law, while a gang of henchmen intimidate Alexs friends into telling them whatever they might know about him – the henchmen eventually kill one of them, Charlotte. In the meantime, Alexs sister Anne persuades her well-off wife Hélène to hire a respected attorney, Élisabeth Feldman, to handle Alexs case.

It is gradually revealed that Margot is apparently still alive. She attempts to arrange a meeting with Alex by sending him an email which he must read in an internet cafe to avoid being spied on. Before this meeting, a warrant is issued for Alexs arrest for the murder of Charlotte. He goes on the run whilst his friends and lawyers struggle to find out the truth about the murder, as well as Margots reappearance.  Alex, chased by policemen, is rescued by Bruno, a gangster from a rough part of the city who feels he owes Alex a favor. The mysterious henchmen reappear to prevent Alexs meeting with his wife, but he is rescued once again by Bruno. Margot is seen almost escaping on a flight to Buenos Aires. Elizabeth, the lawyer, proves that Alex has an alibi for the murder of Charlotte, thanks to eyewitness accounts at the internet cafe.
 pedophile Rape|rapist whose activities were being hidden because his father had influence over the police; when she confronted him, Philippe beat her up, causing the bruises. Her father explains that he walked in on the beating and shot Philippe. The elder Neuville hired thugs to kill Margot. Margots father knew this because he tapped the phone call, so he doubled the payout for one of the thugs to fake Margots murder instead, kill the other thug, and knock out Alex in the process. Margots father then shot the second thug and buried both, then used the body of a dead heroin addict to stand in for Margots.

Police, listening in on the fathers confession, attempt to arrest him.  Margots father shoots himself dead before he can be arrested. 

It is revealed that Margots father knew Alex was wearing a wire, and that during a moment in which he had blocked the bugs transmission he had told Alex one last thing: it was in fact Margot who shot Philippe after he beat her; her father was covering up her crime, not his. His actions have ensured that she will never be suspected. Finallly Philippes father is arrested, and Alex and Margot reunite at the lake where they fell in love as children.

==Cast==
* François Cluzet – Alexandre Beck  
* Marie-Josée Croze – Margot Beck 
* André Dussollier –  Jacques Laurentin  
* Kristin Scott Thomas – Hélène Perkins  
* François Berléand – Eric Levkowitch  
* Nathalie Baye – Maître Elisabeth Feldman  
* Jean Rochefort – Gilbert Neuville  
* Marina Hands – Anne Beck 
* Gilles Lellouche – Bruno  Philippe Lefebvre – Lieutenant Philippe Meynard  
*   – Charlotte Bertaud  
* Thierry Neuvic - Marc Bertaud
* Olivier Marchal – Bernard Valenti  
* Guillaume Canet – Philippe Neuville  
* Brigitte Catillon – Captain Barthas
* Mikaela Fisher – Zak  
* Samir Guesmi – Lieutenant Saraoui

==Production==
The script made several alterations to the book; a torture expert changed from an Asian male to a white female, and the identity of the killer was switched. The books author was quoted in an interview that the films ending was better than his original ending. 

==Reception==
Tell No One was well received both critically and commercially.

Academy Award-winning British actor Michael Caine said of the film it was the best he had seen in 2007 on the BBCs Film 2007 programme. He also included it among his Top Ten movies of all time in his 2010 autobiography, The Elephant to Hollywood. 

===Critical response===
Rotten Tomatoes gives Tell No One a "Certified Fresh" rating of 94% based on reviews from 104 critics.  Metacritic give the film 82/100 based on reviews from 30 critics, indicating "universal acclaim". 

  

===Box office=== limited theatrical release on July 2, 2008. The film opened in 8 theaters grossing $169,707 it.s opening weekend.  In total the film played at a mixmium of 112 theaters and grossed $6,177,192 in North America. 

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2008.     

* 1st – Marc Doyle, Metacritic.com 
* 2nd – Marjorie Baumgarten, The Austin Chronicle 
* 7th – Kimberly Jones, The Austin Chronicle 
* 7th – Marc Mohan, The Oregonian  Shawn Levy, The Oregonian 
* 8th – Stephen Holden, The New York Times 
* 9th – Kenneth Turan, Los Angeles Times 
* 10th – Ann Hornaday, The Washington Post 
* 10th – Owen Gleiberman, Entertainment Weekly 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 