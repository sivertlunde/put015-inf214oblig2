Eda Rae
 
{{Infobox film| name = Eda Rae
 | image =
 | imagesize =
 | caption =
 | director = Shanthi Kumar
 | producer = Ceylon Theaters
 | screenplay = Shanthi Kumar
 | starring =
 | music = D. T. Fernando (lyrics) Herbert Seneviratne (lyrics) Louie Rodrigo (lyrics) Mohammed Gauss (music)
 | cinematography =
 | editing =
 | distributor =
 | country    = Sri Lanka
 | released =   September 14, 1953
| runtime = Sinhala
 | budget =
 | followed_by =
}}

Eda Rae (Sinhala, That Night) is a 1953 Sri Lankan film that achieved some popularity in the country. The musician Latha Walpola debuted as a playback singer in this film.

==Plot==
Cast

*Sita Jayawardena as Maggie
*Herbert Seneviratne as Banda
* as Chandra
* as Ranjith

==Songs==
*"Hari Hari Ha Ha" &ndash; Herbert Seneviratne and Latha Walpola
*"Anandey" &ndash; Rudrani
*"Mage Pana" Louie Rodrigo and group
*"Saa Maa Daa Saa" &ndash; Rudrani and Mohideen Baig
*"Nayana Rasi Wey" &ndash; Rudrani and Mohideen Baig
*"Davi Gini Jalayen" &ndash; Rudrani
*"Habata Mage" &ndash; Herbert Seneviratne and Latha Walpola
*"Sidara Aley" &ndash; Mohideen Baig
*"Haridey Pem Githa Rase" &ndash; Mohideen Baig and Rudrani
*"Ho - Dulevi Prema Dhara" &ndash; Mohideen Baig and Rudrani
*"May Prithi Prithi Darling" &ndash; Herbert Seneviratne and Latha Walpola
*"Gee Nade Shantha" &ndash; Mohideen Baig

==External links==

 
 


 