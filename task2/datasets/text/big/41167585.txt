The Romance of Hine-moa
 
 
{{Infobox film
 | name = The Romance of Hine-Moa
 | image = 
 | caption = 
 | director = Gustav Pauli 
 | producer = Gustav Pauli 
 | writer = Gustav Pauli 
 | starring =  
 | music = 
 | cinematography = Gustav Pauli 
 | editing = 
 | distributor =
 | released =  
 | runtime = 5500 ft (6 reel) 83 minutes
 | country = United Kingdom
 | language = English
 | budget =
 | gross = 
 }}
The Romance of Hine-Moa is a 1927 British film set in New Zealand, directed and produced for Gaumont British by Gustav Pauli. It is now lost. The trade journal Bioscope said it was a "charming love story illustrating an old Maori legend, acted entirely by Maoris in beautiful and interesting native surroundongs".

==Plot==
The plot is the traditional story of the love of Hinemoa and Tutanekai from rival tribes. Pauli’s version shifts the emphasis from Hinemoas swim across the lake to meet her lover to Tutanekais ordeal going through the Valley of Fire, the crater of an active volcano. Released in 1927, but New Zealand Prime Minister Gordon Coates attended a special showing by Gaumont in England on 16 December 1926.

==Cast==
* Maata Hurihanganui ... Hine-Moa
* Akuhato ... Tutanekai

==References==
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p38 (1997, Oxford University Press, Auckland) ISBN 019 558336 1

==External links==
*   (shown as a 1929 film)
*  

==References==
 

 
 
 
 
 
 
 
 
 
 


 
 