Red Light (film)
 
{{Infobox film
| name           = Red Light
| image          = Red Light movie poster.jpg
| image size     =
| alt            =
| caption        = Theatrical release poster
| director       = Roy Del Ruth
| producer       = Roy Del Ruth Charles Grayson
| based on       =  
| narrator       =
| starring       = George Raft Virginia Mayo Gene Lockhart
| music          = Dimitri Tiomkin
| cinematography = Bert Glennon
| editing        = Richard V. Heermance
| studio         = Roy Del Ruth Productions
| distributor    = United Artists
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Red Light is a 1949 film noir, directed and produced by Roy Del Ruth, starring George Raft and Virginia Mayo, and based on the story "This Guy Gideon" by Don "Red" Barry, featuring strong religious overtones. 

==Plot== embezzling from Johnny Tornos trucking company. In a California prison, he sees a newsreel showing Johnny welcoming home his brother Jess, a heroic Catholic chaplain just returned from a World War II prisoner-of-war camp. Just before his release four years later (and to give himself a clever alibi), Nick hires Rocky, an inmate who has just finished his sentence, to murder Jess.

Jess is staying in a local hotel room, about to depart for his first parish in another city. The brothers meet with a local priest, Father Redmond. It is during that meeting that Johnny finds out Jess is moving away.

Johnny arrives at his brothers hotel room not long after Jess is shot by Rocky. Knowing that he is about to die, Jess vaguely indicates that a clue to his murderer can be found within the covers of the rooms Bible. Johnny takes this to mean that the name of the killer himself is inscribed somewhere therein. However, the book is not there.

Johnny refuses to wait for the police to investigate. He tracks down and questions several strangers who occupied the same room, among them Carla North. He believes that one of them has the Bible. Once he satisfies himself that Carla is not a suspect, he hires her to help in the search, inviting her to stay at his luxury apartment, while he moves to his office. Although a bit suspicious of Johnnys motives, Carla agrees.

While Johnny is questioning another hotel guest, he notices Rocky watching him. Setting a trap, he lets Rocky see him buying a book from the former guest and wrapping it up, then leaves it lying around while he gets his shoes shined. When Rocky steals the book, Johnny catches him and reveals that it is just a cookbook. Rocky manages to escape, though Johnny wounds him slightly with his gun.

Later, aboard a train back to town, Rocky tells Nick that he is through, and that he intends to blackmail Nick. Nick sucker punches him, causing him to fall off the rear of a moving train. Then, Nick goes to Tornos office to witness the search of the Gideon Bible found earlier by Carla.

When Johnny finally locates the missing Gideon Bible, he finds written within not information about the killers identity, but a plea from his brother not to seek revenge. Nick thinks he is off the hook. Relieved, he turns to leave.

However, when he gets to the head of the stairs, he spots Rocky on the floor below. In a shootout, Nick fatally wounds Rocky, but before he dies, Rocky identifies Nick as the mastermind behind Jesss murder.

Johnny pursues Nick to the roof, out in a rainstorm. Nick accidentally steps on the main power supply to Tornos huge neon sign and is electrocuted.

==Cast==
* George Raft as Johnny Torno
* Virginia Mayo as Carla North
* Gene Lockhart as Warni Hazard
* Raymond Burr as Nick Cherney Henry Morgan as Rocky
* Barton MacLane as Detective Strecker
* Arthur Franz as Father (Chaplain) Jess Torno
* Arthur Shields as Father Redmond

==Production==
Raft was paid $65,000 for his role. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 141 

==Reception==

===Critical response===
Film critic Dennis Schwartz said of the film, "Roy Del Ruth directs a routine film noir infused with themes of revenge and religion, as it veers more towards a regular crime drama except for photographic flashes that reveal the films dark undertones. The films classic noir shot is of the villainous Raymond Burr smoking and smiling as his frightened victim is being crushed to death while hiding under a trailer, as Burr has just kicked out the jack holding it up ... The film held my interest mainly because this was a perfect part for Raft and it was well-crafted." 

==References==
 

==External links==
*  
*  
*  
*   analysis at Film Noir of the Week by Stone Wallace
*  

 

 
 
 
 
 
 
 
 
 
 
 