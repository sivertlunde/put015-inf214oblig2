I Accuse!
{{Infobox film
| name           = I Accuse!
| image_size     =
| image	         = I Accuse! FilmPoster.jpeg
| caption        =
| director       = José Ferrer
| producer       = Sam Zimbalist
| based on       =  
| screenplay     = Gore Vidal
| starring       = José Ferrer Anton Walbrook
| music          = William Alwyn
| cinematography = Freddie Young Frank Clark
| distributor    = Metro-Goldwyn-Mayer
| released       = March 5, 1958 (USA)
| runtime        = 99 minutes
| country        = United Kingdom
| language       = English
| budget         = $1,768,000  . 
| gross          = $665,000 
}}
 biographical drama Dreyfus Case, in which a Jewish captain in the French Army is falsely accused of treason.

==Plot synopsis==
In 1894 Alfred Dreyfus (José Ferrer), a Jewish captain in the French Army, is falsely accused of treason. He is sentenced to imprisonment on Devils Island. When the real traitor is found, the French Army tries to hide the truth by exonerating the traitor in a mock trial. Émile Zola, the famous French author, writes a letter to the President of France entitled "I Accuse!", which reveals the truth behind the cover up. The letter is published in the newspaper and Zola is sued for libel, leading to a re-examination of the entire Dreyfus case.

==Cast==
 
* José Ferrer as Captain Alfred Dreyfus
* Anton Walbrook as Ferdinand Walsin Esterhazy|Maj. Esterhazy
* Viveca Lindfors as Lucie Dreyfus
* Leo Genn as Georges Picquart|Maj. Piquart
* Emlyn Williams as Émile Zola David Farrar as Mathieu Dreyfus
* Donald Wolfit as Gen. Mercier
* Herbert Lom as Armand du Paty de Clam|Maj. DuPaty de Clam
* Harry Andrews as Hubert-Joseph Henry|Maj. Henry
* Felix Aylmer as Edgar Demange
* George Coulouris as Col. Sandherr
* Peter Illing as Georges Clemenceau
* Michael Hordern as Prosecutor
* Laurence Naismith as Judge
* Ernest Clark as Prosecutor
* Eric Pohlmann as Bertillon John Phillips as Prosecutor, Esterhazy trial
* Malcolm Keen as President of France
 

==Reception==
The film was a box office flop. It earned $190,000 in the US and Canada and $475,000 elsewhere, leading to a loss of $1,415,000. 

==Notes==
The fact that Dreyfus was railroaded because he was Jewish was obscured in the movie The Life of Emile Zola (1937). Only those villains whose names were a matter of public record (Major Dort, Major Esterhazy) are specifically identified. Others are referred to as the Chief of Staff, the Minister of War, etc. to avoid lawsuits from their descendants (remember that the events depicted in the film, most of which take place between 1894 and 1902, were still within living memory in 1937). As for Dreyfus himself, he was not freed and restored to rank in 1902, the year of Zolas death, but in 1906-after being found guilty again in an 1899 retrial (Dreyfus died in 1935, outliving everyone else involved in the case).  

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 