The Poison Tasters
 
{{Infobox film
| name           = The Poison Tasters
| image          = 
| caption        = 
| director       = Ulrik Theer
| producer       = Hong Ting Tom V. Bierce
| writer         = Tom V. Bierce
| starring       = Karolina A. Rosinska French Stewart Veerland Thomas Bierce Aga Lange
| music          = 
| cinematography = Pawel Edelman
| editing        = Petr Sitár
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English Polish
| budget         = 
}}

The Poison Tasters is a 1995 American drama film directed by Ulrik Theer. It was screened in the Un Certain Regard section at the 1995 Cannes Film Festival.   

==Cast==
* Karolina A. Rosinska - Ana
* Tom V. Bierce - Georg
* Veerland Thomas Bierce - Georg
* Barbara Dzido-Lelinska - Landlady
* Marek Kasprzyk - Officer
* Krzysztof Kumor - Professor Mirek
* Ewelina Kuropatwa - Marta
* Aga Lange - Aga
* Maciek Maciejewski - Szymek
* Cezary Pazura - Soldier
* Julitta Sekiewicz-Kisiel - Mrs. Mirek
* French Stewart - Crawford

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 