Barcelona (film)
 
{{Infobox film name            = Barcelona image           = Barcelona-movie.jpg caption         = Promotional poster for Barcelona director        = Whit Stillman producer        = Whit Stillman Antonio Llorens Jordi Tusell writer          = Whit Stillman starring        = Taylor Nichols Chris Eigeman Mira Sorvino music           = Mark Suozzo cinematography  = John Thomas editing         = Christopher Tellefsen studio          = Castle Rock Entertainment distributor     = Fine Line Features released        = July 29, 1994 runtime         = 101 minutes country         = United States language        = English budget          = $3.2 million gross           = $7,266,973
|}}

Barcelona is a 1994 comedy film written and directed by Whit Stillman and set in Barcelona, Spain. The movie stars Taylor Nichols, Chris Eigeman and Mira Sorvino.

==Plot== naval officer, unexpectedly comes to stay with Ted at the beginning of the film. Fred has been sent to Barcelona to handle public relations on behalf of a U.S. fleet scheduled to arrive later.

The cousins have a history of conflict, since childhood, to which the film refers several times. Ted and Fred develop relationships with various single women in Barcelona, and experience the negative reactions of some of the communitys residents to the context of Freds presence. Ted also faces possible problems with his American employer, and with the concept of attraction to physical beauty.

==Cast==
* Taylor Nichols as Ted Boynton, the films protagonist and narrator; a liberal but uptight American businessman working in Barcelona who seeks his own path in life.
* Chris Eigeman as Fred Boynton, Teds belligerent cousin, a U.S. Navy officer, who visits and also has his own opinions on life and love.
* Tushka Bergen as Montserrat Raventos, an attractive, blonde local woman who works at Fira de Barcelona (Barcelona Trade Fair) and becomes romantically involved with Ted.
* Mira Sorvino as Marta Ferrer, an opinionated Trade Fair employee who becomes romantically involved with Fred.
* Pep Munné as Ramon, the films main antagonist; a manipulative, Yankee-baiting journalist who clashes with Fred and who is also romantically connected to both Montserrat and Marta.
* Hellena Schmied as Greta, a Trade Fair employee who captures Teds interest late in the film.
* Nuria Badia as Aurora Boval, a Trade Fair employee and mutual friend of Montserrat, Marta, and Greta.
* Jack Gilpin as The Consul
* Thomas Gibson as Dickie Taylor, Teds company supervisor

==External links==
 
*  
*  
*  
*  
*   (Links on the film)

 

 
 
 
 
 
 
 
 
 
 
 


 