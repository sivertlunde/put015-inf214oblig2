The Heifer
{{Infobox Film  
| name           = La vaquilla
| image          = 
| caption        = 
| director       = Luis García Berlanga
| producer       = Alfredo Matas
| writer         = Rafael Azcona Luis García Berlanga
| narrator       = 
| starring       = Alfredo Landa José Sacristán  Adolfo Marsillach Santiago Ramos  Guillermo Montesinos Amparo Soler Leal
| music          = Miguel Asins Arbó Carlos Suárez          
| editing        = José Luis Matesanz
| distributor    = 
| released       = 6 March 1985
| runtime        = 122 minutes
| country        = Spain
| language       = Spanish}}

La vaquilla (English: The Heifer) is a 1985 Spanish comedy film written and directed by Luis García Berlanga. It was the first comedy made about the Spanish Civil War.

==Plot== Nationalist troops Republican soldiers infiltrate the Nationalist side to steal the animal for two reasons; to ruin the holiday for the Nationalists and because their food stores are low.

==Characters== Brigada Castro (Alfredo Landa), a professional Republican soldier.
* Mariano (Guillermo Montesinos), a Republican soldier coming from a village across the frontline, where his girlfriend Guadalupe remained.
* Limeño (Santiago Ramos), a Republican soldier who was a bullfighter before the war. Chosen to kill the heifer, he is actually afraid of bulls.
* Lieutenat Broseta (José Sacristán), a Republican officer who was a hairdresser before the war. He longs for his profession and threatens to punish failure with a complete head shave.
* Priest (Carles Velat), a Republican soldier who almost became Catholic priest. He still remembers many mannierisms.
* Republican Colonel (Eduardo Calvo) nationalist Alférez.
* Nationalist Commander (Agustín González), has to deal with his troops and the local aristocrats.
* Juana (María Luisa Ponte), mother of Guadalupe, has to hide her Republican husband.
* Alférez (rank)|Alférez (Juanjo Puigcorbé), a Nationalist officer courting Guadalupe.
* The marquis (Adolfo Marsillach) is the local aristocrat. He tries to convince the commander to push the front so that all his enormous estate (currently divided by the frontline), falls in the Nationalist side. His "inherited" gout forbids him to walk.

==Location==
The film was shot mainly in Sos del Rey Católico with many locals as extras. In 2009 the village homaged the surviving artists.

==External links==
*  

==Image gallery==
 
 
File:LaVaquilla1.jpg|
File:LaVaquilla2.jpg|
File:LaVaquilla3.jpg|
 

 

 
 
 
 
 
 
 
 


 
 