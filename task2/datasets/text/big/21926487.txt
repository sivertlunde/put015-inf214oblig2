The Tree (1993 film)
{{Infobox Film
| name           = The Tree
| image          = 
| image_size     = 
| caption        = 
| director       = Todd Field 
| producer       = 
| writer         = Todd Field
| narrator       = 
| starring       = Travis Martin   Jameson Baltes  Jason DRion  Matthew Modine
| music          = Mab Ashforth
| Production Designer = Patrick Sherman
| editing        = 
| distributor    = 
| released       = 1993 
| runtime        = 12 minutes
| country        = United States 
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Tree is a 1993 short film that Todd Field created while a fellow at the AFI Conservatory. It is a non-verbal dramatic piece following the life of a boy born at the turn of the century. The single setting, an apple tree set high on a rural ridge, is where we glimpse the boy mature, fall in love, go to war, return with his own son, and finally pay his last respects as a very old man who has seen much change. The set was designed using the tree as a scale foreground visual anchor and employing forced perspective for other items appearing in frame, including distant mountains, a train, and a town in transition. The scene changes from season to season and year to year all achieved practically using Trompe-lœil.

The film is loosely based upon and inspired by the story The Giving Tree by Shel Silverstein.

 
 
 
 
 
 
 