F. est un salaud
{{Infobox film
| name           = F. es un salud 
| image          = Fogi est un salaud.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Marcel Gisler
| producer       = 
| writer         = Marcel Gisler Rudolf Nadler
| screenplay     = 
| story          = 
| based on       =  
| starring       = Frédéric Andrau Vincent Branchet
| music          = Rainer Lingk
| cinematography = Sophie Maintigneux
| editing        = Bettina Böhler
| studio         = Vega Film
| distributor    = Vega Distribution
| released       =  
| runtime        = 91 minutes
| country        = France & Switzerland
| language       = French
| budget         = 
| gross          =
}}
F. est un salaud, (English title F. Is a Bastard or Fögi Is a Bastard, released in German as De Fögi isch en Souhund)  is a 1998 French/Swiss film based upon the novel of the same name by Martin Frank.  It was filmed in Zürich, Switzerland.  The setting is the early 1970s, Zurich. The film was shown in 1999 at the Locarno Film Festival,   the Toronto International Film Festival, and the 1998 Toronto International Film Festival.     

==Plot summary==
Beni, age 15, becomes enamoured with a singer from the rock band "The Minks", Fögi Müller, who is ten years older than him.   Fögi ensnares him with the perceived glamor of being a musician, takes him on as the bands roadie, and at the same time seduces him.  Beni unquestionably follows him, including following him into drug addiction.  Benis youthful innocence is destroyed in a milieu of sex, drinking, drugs, and rock-and-roll. He ends up prostituting himself to pay for Fögis drug habit.

==Primary cast==
* Frédéric Andrau	... 	Fögi
* Vincent Branchet	... 	Beni Müller
* Urs Peter Halter	... 	Töbe
* Jean-Pierre von Dach	... 	Wäde

==See also==
*List of lesbian, gay, bisexual or transgender-related films

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 
 