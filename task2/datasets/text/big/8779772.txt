Goofs and Saddles
{{Infobox Film |
  | name           = Goofs and Saddles |
  | image          = GoofsandSaddlesTITLE.jpg|
  | caption        = |
  | director       = Del Lord Felix Adler | Ted Lorch Cy Schindell Eddie Laughton Hank Bell|
  | cinematography = Benjamin H. Kline |  Charles Nelson |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 17 10" |
  | country        = United States
  | language       = English
}}
Goofs and Saddles is the 24th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Old West, Ted Lorch) cattle rustlers, so they hide as bushes to try to find the gangs leader, Longhorn Pete (Stanley Blystone). The rustlers are befuddled by the moving bushes. The Stooges eventually wind up in Longhorn Petes saloon, and the Stooges disguise themselves as gamblers and get into a card game with Pete as they wait for the cavalry.

Moe attempts to send a message to General Muster for help via carrier pigeon, but the pigeon returns to Pete, who reads the incriminating message aloud. The jig is up, and the Stooges are forced to escape for their lives, jumping on a covered wagon filled with household equipment — and a monkey. The trio toss pots and pans from the wagon onto the ground, which their horse’s hoofs catch them. The wagon loosens up from the horse team, and goes down in its own power until it stops. Eventually, they end up on a shed.
 
 ammunition belt. The increase in opposing firepower befuddles the bandits, but eventually General Muster arrives and saves the day. As they are given kudos for a job well done, the monkey goes to the grinder and twists the handle, firing a few shots that caused the Stooges to be hit and flee the area.

==Production notes==
The title Goofs and Saddles is a spoof of the term "hooves and saddles".  Filming was completed on April 14–19, 1937.   

The Stooges names in this short are Buffalo Billious (Curly), Wild Bill Hiccup (Moe), and Just Plain Bill (Larry). The cultural references are to, respectively, American Old West figures Buffalo Bill and Wild Bill Hickock, and Just Plain Bill, the title of a long-running radio program of the era.   

The chase sequences on horseback would be recycled in 1954s Pals and Gals. 

This short has the smallest slap count. Moe smacks Curly softly on his head and then he slaps Larry in another scene.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 