The Fast Sword
 
 
{{Infobox film
| name           = The Fast Sword
| image          = TheFastSword.jpg
| image_size     =
| caption        =
| director       = Huang Feng
| producer       =
| writer         = Huang Feng
| narrator       =
| starring       = Chang Yi.
| music          =
| cinematography =
| editing        =
| distributor    =
| released        = 1971
| runtime        =
| country        = Hong Kong Mandarin
| budget         =
}}
 1971 Hong Kong action film directed by Huang Feng. The film stars Chang Yi and Wong Chung Shun.

The film was shown in France and Germany.

==Plot==
Chang Yi and his sister, Hon Seung Kam, live on their farm with their blind mother Wang Li. Their father was murdered by Wong Tung Shung. Seeking revenge when he learns his fathers killer has returned, he finds himself arrested at attempting to avenge his fathers death. Informing the officer who arrested him they join forces and finally bring the evil warlord to justice in an extensive fruit battle.

==Cast==
*Chang Yi
*Hon Seung Kam
*Sammo Hung
*Shih Chun
*Wong Chung Shun
*Miu Tin
*Yee Yuen
*Wang Lai
*Lui Jun
*Lee Ka Ting

==External links==
*  
* The Fast Sword at  

 
 
 
 
 


 
 