Killer's Kiss
{{Infobox film
| name           = Killers Kiss
| image          = KillersKissPoster.jpg
| caption        = Theatrical release poster
| director       = Stanley Kubrick
| producer       = {{Plain list|
* Stanley Kubrick
* Morris Bousel
}}
| screenplay     = Howard Sackler
| story          = Stanley Kubrick
| starring       = {{Plain list|
* Frank Silvera
* Jamie Smith
* Irene Kane
* Ruth Sobotka
}}
| music          = Gerald Fried
| cinematography = Stanley Kubrick
| editing        = Stanley Kubrick
| studio         = Minotaur Productions
| distributor    = United Artists
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = $75,000 
| gross          = 
|}}
Killers Kiss is a 1955 American film noir directed by Stanley Kubrick and written by Kubrick and Howard Sackler. It is the second feature film directed by Kubrick. The film stars Jamie Smith, Irene Kane and Frank Silvera. 

==Plot== New York boxer at the end of his career, and his relationship with his neighbor, taxi dancer Gloria Price (Irene Kane), and her violent employer Vincent Rapallo (Frank Silvera). The drama begins with Davey in his apartment room, mentally preparing for a big fight against Kid Rodriguez. On the other side of the building, across the courtyard, he gazes upon Gloria, an attractive taxi dancer, getting ready for work. As they both walk out of the building, they run into each other but say nothing. Gloria is picked up by her boss Vincent.

As Davey is losing his fight, Gloria is dealing with her boss in his office as he tries to kiss her repeatedly. That evening Davey is awakened by screams coming from Glorias apartment.  As he looks across the courtyard, he sees that Gloria is being attacked by Vincent. He runs to her room, but Vincent has made his getaway. Davey comforts Gloria and she goes to sleep comfortable that Davey is in the room to protect her. However, Vincent is not deterred, and proceeds to interfere in their lives. When they decide to leave town, Davey and Gloria arrange to get money they are each owed. Gloria tries to get money from Vincent at the dance hall, and Davey asks his manager to meet him there as well. When a street performer steals Daveys scarf, he chases after him. Daveys manager arrives but does not see Davey. Vincent sends two goons out to rough Davey up, but they mistake the manager for Davey and kill him in the alley.

Vincent kidnaps Gloria and has his two goons hold her hostage. Davey returns to Glorias apartment and sees the police across the courtyard in his apartment. They assume he killed his manager. Davey leaves to rescue Gloria, but he is captured and restrained as well, leading to a chase and confrontation in an abandoned warehouse full of mannequins. During the struggle, Davey kills Vincent and rescues Gloria. He and Gloria are cleared of all charges by the police, and Davey buys a train ticket back to the West Coast. At the train station, Davey assumes she will not join him, but at the last minute, Gloria rushes in, and they kiss.

==Cast==
 
 
* Jamie Smith as Davey Gordon
* Irene Kane as Gloria Price
* Frank Silvera as Vincent Rapallo
* Jerry Jarrett as Albert (the fight manager)
* Mike Dana as Gangster
* Felice Orlandi as Gangster
 
* Skippy Adelman as Mannequin factory owner
* David Vaughan as Conventioneer
* Alec Rubin as Conventioneer
* Ralph Roberts as Gangster
* Phil Stevenson as Gangster
* Ruth Sobotka as Iris/ballerina
 

==Background==
This was Kubricks second feature. Kubrick removed his first film Fear and Desire from circulation over his dissatisfaction with it. Kubrick was 26 years old when he directed this movie, and had to borrow $40,000 from his uncle, who owned a drug store in New York. 
 Penn Station, which was demolished in 1963, as well as Times Square, and the run-down streets of the Brooklyn waterfront and Soho loft areas. 
 The Killing.  She is also featured in a long dance solo, playing the role of Iris. Irene Kane (real life writer Chris Chase, nee Irene Greengard) is the female lead.

Against Kubricks wishes, United Artists required the film be recut with a happy ending.  The Killing. Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 157 

The film features a song "Once", written by Norman Gimbel and Arden Clar.   It is one of Gimbels earliest contributions to a film, although his lyrics do not actually appear in the final version.   

==Reception==

===Critical response=== Gotham life. His scenes of tawdry Broadway, gloomy tenements and grotesque brick-and-stone structures that make up Manhattans downtown eastside loft district help offset the scripts deficiencies." 
 New York Times film critic Janet Maslin reviewed the film, and wrote, "Killers Kiss brought the director onto more conventional territory, with a film noir plot about a boxer, a gangster and a dance hall girl. Using Times Square and even the subway as his backdrop, Mr. Kubrick worked in an uncharacteristically naturalistic style despite the genre material, with mixed but still fascinating results. The actress playing the dance hall girl, billed as Irene Kane, is the writer Chris Chase, whose work has frequently appeared in The New York Times. Jamie Smith plays the boxer, whose career is described as one long promise without fulfillment. In the case of Mr. Kubricks own career, the fulfillment came later. But here is the promise." 

Rotten Tomatoes rates it 82% fresh, based on seventeen reviews.

===Awards===
Wins
*  , Stanley Kubrick; 1959.   

===Adaptation=== Matthew Chapman directed Strangers Kiss, a film that portrayed the making of Killers Kiss.

==Home media==
  The Killing. 

==References==
 

==External links==
*  
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images)
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 