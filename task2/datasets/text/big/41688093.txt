Trail of the Yukon
{{Infobox film
| name =  Trail of the Yukon
| image =
| image_size =
| caption =
| director = William Beaudine
| producer = Lindsley Parsons  Oliver Drake
| narrator = Bill Edwards   Iris Adrian
| music = Edward J. Kay  
| cinematography = William A. Sickner 
| editing = Ace Herman  
| studio = Monogram Pictures Associated British (UK)
| released = July 31, 1949
| runtime = 67 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Bill Edwards. Northern genre. series of nine further films starring Grant and Chinook. 

==Cast==
* Kirby Grant as Bob McDonald - Royal NW Mounted 
* Suzanne Dalbert as Marie Laroux  Bill Edwards as Jim Blaine 
* Iris Adrian as Paula 
* Dan Seymour as Tom Laroux  William Forrest as Banker John Dawson 
* Anthony Warde as Muskeg Joe 
* Maynard Holmes as Henchman Buck 
* Peter Mamakos as Henchman Rand 
* Guy Beach as Matt Blaine 
* Stanley Andrews  as Rogers 
* Dick Elliott  as Editor Sullivan 
* Jay Silverheels as Poleon  Bill Kennedy as Constable, RCMP   
* Harrison Hearne as Bank Teller Frank  Chinook as himself, Bobs dog

==References==
 

==Bibliography==
* Drew, Bernard. Motion Picture Series and Sequels: A Reference Guide. Routledge, 2013. 
* Marshall, Wendy L. William Beaudine: From Silents to Television. Scarecrow Press, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 