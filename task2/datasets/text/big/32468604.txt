Krrish 3
 
 

 

{{Infobox film
| name           = Krrish 3
| image          = Krrish 3 Poster.jpg
| caption        = Theatrical release poster
| alt            =  
| director       = Rakesh Roshan
| producer       = Rakesh Roshan
| story          = Rakesh Roshan
| screenplay     = Rakesh Roshan Honey Irani Robin Bhatt
| narrator       = Amitabh Bachchan
| starring       = Hrithik Roshan Vivek Oberoi Priyanka Chopra Kangna Ranaut
| music          = Songs:  
| cinematography = Tirru|S. Tirru
| editing        = Chandan Arora
| distributor    = Filmkraft Productions Pvt. Ltd 
| released       =     
| runtime        = 152 minutes  India
| Hindi
| budget         =     
| gross          =   
}}
 superhero science Krrish series following Koi... Mil Gaya (2003) and Krrish (2006).  The film stars Hrithik Roshan, Vivek Oberoi, Priyanka Chopra, and Kangna Ranaut in the lead roles. The story follows the life of Rohit Mehra, a scientist, and Krishna Mehra a.k.a. Krrish, his superhero son, who face an elaborate conspiracy orchestrated by the evil genius Kaal and his female henchman Kaya. In the process, Krishnas pregnant wife Priya is kidnapped by Kaal and the form-changing Kaya takes her place at the Mehra home and eventually falls in love with Krishna.

Krrish 3 was reportedly produced on a budget of   and was initially scheduled to release as a 3D film.   However, due to lack of time to convert the film to 3D, director Rakesh Roshan mentioned that the film will be released only in the 2D format. Krrish 3 released worldwide on 1 November 2013.     It has since received mixed reviews from film critics with praise directed towards Roshan and Ranauts performances, the visual effects, and the cinematography; criticism has been directed to the music provided by composer and the directors brother Rajesh Roshan, the story and screenplay. 

==Plot==
 
Scientist Rohit Mehra lives with his son Krishna "Krrish" Mehra (both played by Hrithik Roshan) in Mumbai. Krishna is married to Priya (Priyanka Chopra) who works as a journalist for Aaj Tak. Rohit works in a research institute for the Indian government and Krishna, while moon lighting as the superhero Krrish, is regularly fired from various day jobs due to lack of attendance. Rohit is researching on a device that will bring life to dead tissues by the use of solar energy. Despite several attempts, he fails to complete the experiment due to the high intensity of sun rays. On a usual day, Krishna loses his job from the security wing of a company when he hears news about an aeroplane whose front tyres are stuck due to some malfunctioning which creates high risk, even if landed, it may explode and so will the passengers inside. Krishna immediately changes himself to Krrish and saves the plane from accident by holding the component to make tyres free to roam.

A few days later, Rohit hears about a deadly disease spreading in Namibia due to a virus whose antidote has not yet been discovered. He consults his friend Dr. Varun Shetty (Arif Zakaria) about the situation. Meanwhile, Shetty receives a phone call from Dr. Alok Sen (Asif Basra), a scientist working for Kaal Laboratories who insists on meeting him in order to reveal information about the virus and its antidote. Both Sen and Shetty are, however, murdered by Kaya (Kangna Ranaut), a shapeshifting female mutant. She is the henchwoman of Kaal (Vivek Oberoi), a handicapped evil genius who possesses telekinetic powers. Kaal creates a team of human-animal hybrids called "maanvars", with distinct physical powers in order to cure himself but fails. Later it is revealed the virus in Namibia was spread by Kaal himself to incur huge profits by selling its antidote, the singular source of which is his blood.

Rohit discovers that the virus, which is fast spreading through Mumbai, has no effect on Krishna, the pregnant Priya or himself. He, thus, makes an antidote by using the blood from Krishnas bloodstream which he spreads with the help of explosions. An enraged Kaal sends his mutants to attack Rohit and find out how an antidote was developed without using his own blood. Krrish manages to save Rohit, but Priya is injured and hospitalised. Krrish, however, manages to defeat the mutant Striker (Gowhar Khan). Kaya, carrying out Kaals instructions, shapeshifts into Priya while Priya is kidnapped by Kaal. In the form of Priya, Kaya leads Krishna to believe that Priya has lost her child, and continues to live with the Mehras. While finding the truth behind the antidotes formula she develops an attraction towards Krishna. Failing to get answers about Frog Mans involvement with the virus attack, Rohit travels to Singapore to seek answers from Dr. Siddhant Aryas (Naseeruddin Shah) partner, where he is kidnapped by Kaals men. Meanwhile, Krishna discovers the true identity of Priya; furious, he reveals himself as Krrish to Kaya and orders her to take him to Kaals laboratory. Having fallen in love with Krishna, Kaya decides to help rescue his wife.

At Kaals laboratory, it is revealed that Kaal is Rohits son, who was created by an experiment conducted by Dr. Arya while Rohit was held hostage by him years earlier. The child was born handicapped, and was thus given away to an orphanage from where he was adopted by a wealthy man (Mohnish Behl). Kaal uses Rohits bone marrow to successfully cure his handicap. Meanwhile, Kaya saves Priya and unites her with Krrish; she is, however, murdered by Kaal for her disloyalty. Kaal subsequently murders Krrish, and dons a metal suit. Deeply disturbed by his sons death, Rohit decides to re-use his failed experiment to bring dead tissues to life in order to save Krishna. This time, however, he absorbs the excess energy himself. The process simultaneously kills Rohit and transfers his powers to a revived Krishna. Meanwhile, Kaal threatens to destroy the city of Mumbai if Priya does not surrender to him. Krrish re-emerges and, after several unsuccessful attempts, manages to kill him using Rohits solar experiment.

Six months later, Priya names their new-born son Rohit, who also exhibits superpowers.

==Cast== Krishna Mehra Rohit Mehra. Rohit is a professional scientist working in a Government organization while Krishna Mehra is his son who moonlights as Krrish, a superhero. mutants made by fusing the serum of humans and animals.    Priya Mehra, Krishnas wife and a journalist for Aaj Tak.
* Kangna Ranaut as Krrish (film series)#Kaya|Kaya, Kaals female henchman who is created by fusing the serum of a woman and a chameleon. She is instrumental in Kaals scheme because of her ability to shapeshift into anybody whenever she wishes. She later falls in love with Krishna after shapeshifting into Priya.
* Arif Zakaria as Krrish (film series)#Dr. Varun Shetty|Dr. Varun Shetty, Rohits scientist friend.
* Asif Basra as Krrish (film series)#Dr. Alok Sen|Dr. Alok Sen, a scientist working at Kaal Laboratories.
* Rajpal Yadav as Kripal Sharma, Krishnas friend and co-worker. Rakhee Vijan as Sharmas wife Striker
* Cheetah Woman  Ant Man   Scorpion Woman Rhino Man
* Sachin Khedekar as Minister (special appearance)
* Naseeruddin Shah as Krrish (film series)#Dr. Siddhant Arya|Dr. Siddhant Arya (special appearance)
* Mohnish Behl as Kaals adoptive father (special appearance) Sonia Mehra and Krrish (film series)#Nisha|Nisha, respectively, in flashback scenes from Koi...Mil Gaya and Krrish

==Production== Rambo 3), the film will be called Krrish 3.  Rakesh Roshan also clarified the title to Bollywood Hungama.    In an interview director Rakesh Roshan said "I cant reveal the budget of the movie, but it has been a very costly film".  The film was reportedly made on a budget of   1.15 billion.  Red Chillies VFX, a subdivision of Shahrukh Khans motion picture production banner Red Chillies Entertainment, worked on the special effects worth   for Krrish 3.  The filming process was completed in June 2012, and then the VFX team started their work. 

===Filming===
  at the Alps, portrayed as Kaals laboratory in the film]]
{{quote box
| quote = "While shooting in hot and humid conditions in India - in the film studios in Hyderabad, and in Mumbai, the mask   would get distorted and start melting. This was because of the high temperature, which would often go up to as high as 44 degrees. At times, it seems it was so hot that in a day, he   would end up changing five to six masks,"
| source = - The Hindustan Times, who stated that Roshan ended up using 500 masks 
| align = right
| width = 25%
}} VFX took one and half years to complete with the last of them coming on 22 October 2013, just 10 days before the release of the film with each and every VFX action sequence undegoing a revision of 40 to 50 times before inclusion. 

===Casting===
Following the success of the previous instalments, director Rakesh Roshan planned to develop a second sequel to the franchise, with Hrithik Roshan playing the protagonist again. He had penned the script, but had to re-work it as the storyline was too similar to the Hollywood blockbuster Spider-Man 3.  Priyanka Chopra was later confirmed to reprise the role of Priya. In this film, she will be seen married to Krishna Mehra. 

Next, Chitrangada Singh announced her inclusion in the project as a mutant but left the film due to unspecified reasons. Later on, Jacqueline Fernandez was signed but she later on backed out of the film due to a lack of dates. It was reported that actresses Nargis Fakhri, Esha Gupta and Bipasha Basu were to replace Fernandez.     Finally, Kangna Ranaut was confirmed to play the role, who coincidentally was originally offered the role.    Rakesh Roshan had earlier revealed to media that the main antagonist of film would be as powerful as the hero.  After talks with Ajay Devgn and Shah Rukh Khan fell through, Vivek Oberoi was eventually cast for the role.   In an interview with Bollywood News Service, Oberoi said, "Kaal is contrary to my nature. I am very friendly and Kaal isnt," and added, "He has anger and vengeance burning within him; he hates humanity. To express these emotions through my eyes was difficult." 

In January 2011, Lakshmi Manchu claimed to have been offered a role in the film as the antagonists girlfriend.  Its overseas distribution rights were sold to Eros International while Rakesh Roshan retained rights in Mumbai.  There was also initial speculation that Rekha would play a major role in the film, which was later denied as a rumour.   

===Design and costumes===
For the casts costumes, Rakesh Roshan involved with designer Gavin Miguel. Roshan wanted light and flexible costume to allow Kangna Ranaut to do her stunts without any restrictions where he stated  "She has a lot of toughness in her personality. So instead of looking at feminine superheroes for reference, I looked at Batman". It took three and half hours to apply Ranauts makeup everyday.  Oberoi is dressed in metal suit weighing around 28 kilogramme which required about 2–3 hours and 7-8 workers to fix on body. 

==Soundtrack==
{{Infobox album
| Name       = Krrish 3
| Type       = Soundtrack
| Artist     = Rajesh Roshan
| Cover      = Krrish 3 Soundtrack Cover.jpg
| Released   = 18 September 2013 
| Recorded   = 2012-2013 Feature Film Soundtrack
| Length     =  
| Label      = T-Series
| Producer   = Rajesh Roshan
| Last album = Kites (2010)
| This album = Krrish 3 (2013)
| Next album = Baniye Ki Moonch (2013)   
 {{Singles
  | Name           = Krrish 3
  | Type           = Soundtrack Singles
  | Single 1       = Raghupati Raghav
  | Single 1 date  = 15 September 2013 
 }}
}}
The songs of the film are being composed by Rajesh Roshan at Krishna Audio; whilst the background score is being composed by Salim-Sulaiman. The music rights of the film were acquired by T-Series for  .  The full soundtrack album was released on 18 September 2013 with music composed by Rakesh Roshan.   
{{Track listing
| headline        = Track listing
| extra_column    = Singer(s)
| total_length    =  
| lyrics_credits  = no
| music_credits   = no
| title1          = Krrish Krrish (Title Track)
| extra1          = Mamta Sharma, Anirudh Bhola, Rajesh Roshan
| music1          = Rajesh Roshan Sameer
| length1         = 5:13
| title2          = Raghupati Raghav
| extra2          = Neeraj Shridhar, Monali Thakur & Bob
| music2          = Rajesh Roshan
| lyrics2         = Sameer
| length2         = 5:21
| title3          = Dil Tu Hi Bataa
| extra3          = Alisha Chinai, Zubeen Garg
| music3          = Rajesh Roshan
| lyrics3         = Sameer
| length3         = 6:41
| title4          = You Are My Love
| extra4          = Mohit Chauhan, Alisha Chinai
| music4          = Rajesh Roshan
| lyrics4         = Sameer
| length4         = 4:31
| title5          = God Allah Aur Bhagwan
| extra5          = Sonu Nigam, Shreya Ghoshal
| music5          = Rajesh Roshan
| lyrics5         = Sameer
| length5         = 6:28
| title6          = Dil Tu Hi Bataa (Remix By DJ Shiva)
| extra6          = Alisha Chinai, Zubeen Garg
| music6          = Rajesh Roshan
| lyrics6         = Sameer
| length6         = 4:25
| title7          = Raghupati Raghav (Remix By DJ Shiva)
| extra7          = Neeraj Shridhar, Monali Thakur, Bob
| music7          = Rajesh Roshan
| lyrics7         = Sameer
| length7         = 4:31
}}

==Marketing==
Promotions for the movie started post July 2013 and the first look of Krrish 3 was unveiled online.  Response for the poster was quite enthusiastic from the audience. The first trailer video was published by film makers on their official   and developed by Filmkraft and so the film and character deserved credit for setting a new standard for an original superhero which has not been seen elsewhere in the world. He also stated that no other filmmaker in India or elsewhere has managed to achieve what Rakesh Roshan and Hrithik Roshan have in completely creating a new character and comes out with a hugely successful franchise that is original and has fueled a fanfare similar to those of comic book heroes yet with Krrishs story still being written with each film.   Krrish 3 was the first Indian film to launch its own official Facebook Emoticons as part of promotion.  An official Krrish 3 game was launched for Windows smartphones, tablets and PCs. The game was developed by Hungama Digital Media Entertainment and Gameshastra. 

==Release== Telugu and TN and Kerala circuits.   The satellite rights were sold to Sony Entertainment Television (India) for   

==Controversy==
 
Uday Singh Rajput, a writer from Sagar district in Madhya Pradesh moved the Bombay High Court alleging copyright violation of the script of Krrish 3 and seeking a stay on the all India release of the film and claiming   20&nbsp;million compensation.  But, the High Court refused to grant relief to a writer.  There was also a media-built controversy about who was the main actress of the film, Kangana Ranaut or Priyanka Chopra.  There was a large dispute over the domestic box-office collections of Krrish 3 with accusation of inflating the collections including dubbed Telugu and Tamil versions by almost   600&nbsp;million.   Some reputed trade analysts stated that the collections published in trade websites like Bollywood Hungama and Koimoi, which were being shown as nett, were actually gross (including Entertainment tax).   Box Office India stated that the domestic nett of the film including Tamil and Telugu versions was  .       On 7 January 2014, after being miffed over the alleged inflated collections, Hrithik Roshan maintained that Krrish 3 had done a business of about   in India and around   overseas.       

==Critical reception==
 s performance was particularly praised by the critics]]

===India===
Krrish 3 received positive to mixed reviews upon release.   

Taran Adarsh of Bollywood Hungama gave the film 4.5 out of 5 stars and stated that "the film has all the ingredients that make a splendid superhero film, besides being Rakesh Roshans most accomplished work so far."  Madhureeta Mukherjee of Times of India gave it 4.5 stars while commenting "For sheer vision, bravado and superlative execution, this one soars to new orbits. Latch on to this cape for an exhilarating ride."  Raedita Tandan of Filmfare awarded it 4 out of 5 stars, remarking "Hats off to Rakesh Roshan for dreaming big and actually pulling off this risky proposition. Its not perfect. But it has all the elements a good, entertaining film must have. All you Marvel superheroes, better watch out. Krrish is here to stay."  Anupama Chopra of the Hindustan Times gave it 3.5 stars and said, "Filmmaker Rakesh Roshan deserves a round of applause for giving us a homegrown superhero. Krrish 3 is ambitious and exciting."  Sarita Tanwar of DNA gave it 3.5 stars and wrote, "Krrish 3 is fast-paced and the VFX effects are smashing."  Rohit Khilnani of India Today gave it 3.5 stars noting, "The only part where the movie dips is during the songs. The music sounds too dated for this action packed film." 

The professional review by the critic board of Box Office India magazine found Krrish 3 an industry-defining event movie, stating:  "Krrish 3 is not only a BIG film but transcends expectations. The film makes you proud how far we, as a film industry, have come. It also makes a bold statement that our industry is at par with Hollywood when it comes to special effects. Rakesh Roshan has incorporated everything a Hindi cine-goer could hope for in this production. It is commercial cinema at its best, with exquisite flavours of action, drama, emotion and thrills. And stitching it all together is content that provides a wide enough canvas that has something for everyone in every age group." 

Rajeev Masand of CNN-IBN gave it 3 out of 5 and stated "Im going with two-and-a-half stars for the film, and an additional half star just for Hrithik Roshan, which makes it three out of five for Krrish 3. The film is ambitious but flawed. It is, however, consistently watchable for its terrific lead star who you cant take your eyes off, even for a moment".  Sukanya Verma of Rediff.com gave it 3 out of 5 stars, while adding, "Krrish 3 is a outrageous mishmash of Bollywood sentimentality meets E.T. meets Superman meets X-Men with set pieces, sound design and screenplay structure liberally borrowed from Hollywoods imagination."   Aparna Mudi of Zee News rated the film 3 stars saying, "Krrish 3 has all the elements of an entertainer and a lot of potential to have given some serious competition to Hollywood flicks." 

Saibal Chatterjee of NDTV gave the film 2.5 stars stating, "Krrish 3 is, for the most part, impressively ambitious and dazzlingly competent, if not always riveting."  Karan Anshuman of Mumbai Mirror gave it 2.5 stars and stated that Krrish 3 is a film for kids.  Mohar Basu of Koimoi gave it 2 stars only and stated that Krrish 3 has absolutely no screenplay with poor acting and over the top dialogue.  Shubhra Gupta of Indian Express gave it 2 stars out of 5.  Mihir Phadnavis of Firstpost stated that "Krrish 3 is the most superficial components of X-Men, Batman, Superman, Spiderman and even Shaktimaan stuffed together."  Paloma Sharma of Rediff.com panned the film, and gave it "no stars" saying, "Krrish 3 is astonishingly eager to entertain with its stock of doodads that should amuse if not endear". 

===Overseas=== Marvel and DC comics (Krrishs Superman worship is rather blatant) and stick-glued together, it is startling when director Rakesh Roshans underlying Bollywood-emotionalism compensates the flimsiness of the awkward superhero element from his last film". He closes by saying that the film "is family-friendly stereotypical entertainment". 

David Chute for the Variety (magazine)|Variety, applauded Krrish 3 as "a charming Bollywood superhero movie" from a different perspective and applauded the heart and upbeat spirit of the movie for "not being an audience-pummeling industrial product like most of Hollywoods superhero films."  He loved the way Krrish 3 "has the off-hand, anything-is-possible spirit of a childrens book or fairy tale." 

Lisa Tsering writing for The Hollywood Reporter gave the film a negative review saying that "the musical superhero extravaganza is too scary for young viewers and too long-winded for everybody else". She continues that the "effort is admirable and the effects are certainly adequate, but cant compensate for uninteresting, drawn out action scenes; childish logic and uneven acting, especially by Hrithik Roshan, who as Rohit mouth-breathes and toddles around with his head cocked to one side. Worst of all, the movie is devoid of that one secret ingredient that makes audiences love superhero films: It just isnt cool." 

==Box office==
Krrish 3 had a final worldwide gross of   from all its versions.    

===India===
Krrish 3 opened to magnificent response across the country in multiplexes with more than 80 percent occupancy as well as around 90-100 percent attendees at single screens. According to Box Office India, first day collections of all versions were estimated at  .    The film nett. grossed around  , with its (Tamil (language)|Tamil) and (Telugu (language)|Telugu) dubbed versions have collected approximately   nett on the first day.   Krrish 3 (Hindi (language)|Hindi) grossed   nett on its second day.  Box Office India estimated the first weekend collection of  , with around   nett for its original version, {{cite web|url=http://www.boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=6246&nCat=|title=Top Weekends 2013: Goliyon Ki Raasleela Ram Leela 3rd Tamil and Telugu dubbed versions nett.} grossed around   in the first week.     

The film nett. grossed   on the second Friday.  Krrish 3 collected around   nett on second Saturday, and   on second Sunday to take the Hindi version nett. total to around   in ten days.   The films Hindi version earned around   nett on its second Monday,   on Tuesday,   on Wednesday and   on Thursday to take its two-week domestic total to   nett.    The second week thus collected   for the Hindi version of Krrish 3.     nett mark in India even if its dubbed Telugu and Tamil versions earnings are included,  other trade analysts like Taran Adarsh and reputed trade magazines claimed that Krrish 3 has lifetime nett collections of all its versions at   and that the film had emerged as the highest grosser of Hindi cinema.    }}

Krrish 3 grossed around   nett in third weekend to take its Hindi-version total to almost   nett in 17 days. The Tamil and Telugu versions added a further   nett approx in 17 days to take overall total to  . 
Krrish 3 grossed   nett with its Hindi version after the third week. http://web.archive.org/web/20131129144302/http://www.boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=6266&nCat=  http://web.archive.org/web/20131128045044/http://www.boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=6261&nCat=   The fourth week collections were   taking the Hindi versions total to around  . http://web.archive.org/web/20131205163318/http://boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=6281&nCat=  According to Box Office India, the three versions together earned   nett lifetime with the dubbed versions of the film contributing  .   The lifetime domestic distributor share of the Hindi version was  .   

===Overseas===
Krrish 3 did well overseas as it collected around $3.85 million in its first weekend.  The film grossed $6.9 million overseas in ten days.  It grossed US$7.8 million abroad after seventeen days. The film earned $8.5 million overseas, as of 26 November 2013. 

==Notes==
 

==See also==
* Science fiction film of India
* List of Bollywood films of 2013
* Krrish
* Koi... Mil Gaya

==References==
 

==External links==
*  
*  

 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 