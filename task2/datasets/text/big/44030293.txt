Mohiniyaattam (film)
{{Infobox film
| name           = Mohiniyaattam
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       =
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi Lakshmi T. R. Omana Nilambur Balan
| music          = G. Devarajan
| cinematography = PS Nivas
| editing        = K Sankunni
| studio         = Ragamalika
| distributor    = Ragamalika
| released       =  
| country        = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film,  directed Sreekumaran Thampi. The film stars Adoor Bhasi, Lakshmi (actress)|Lakshmi, T. R. Omana and Nilambur Balan in lead roles. The film had musical score by G. Devarajan.   
 Lakshmi won the Filmfare Award for Best Malayalam Actress for her performance in this movie


==Cast==
 
*Adoor Bhasi as Krishnan Lakshmi as Mohini
*T. R. Omana as Nalini
*Nilambur Balan
*P. N. Menon (director)|P. N. Menon as Artist KM Panicker
*Maniyanpilla Raju
*Babu Nanthankode
*K. P. Ummer as Narendran
*Kanakadurga as Anasooya
*MG Soman as Venu
*Mallika Sukumaran as Ranjini
*Master Rajakumaran Thampi as Young Chinthu
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi and Jayadevar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aaranmula Bhagavaante || P Jayachandran || Sreekumaran Thampi ||
|-
| 2 || Kanneeru Kandaal || P. Madhuri || Sreekumaran Thampi ||
|-
| 3 || Radhika Krishna || Mannur Rajakumaranunni || Jayadevar ||
|-
| 4 || Swanthamenna Padathinenthartham || K. J. Yesudas || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 