Mendadak Dangdut
 
{{Infobox film
| name           = Mendadak Dangdut
| image          = Mendadak Dangdut.jpg
| image_size     =
| caption        = Promotional poster
| director       = Rudy Soedjarwo
| producer       = Leo Sutanto Elly Yanti Noor
| writer         = Monty Tiwa
| starring       = Titi Kamal Kinaryosih Dwi Sasono
| music          = Andi Rianto
| cinematography = Rudy Soedjarwo
| editing        = Rudi Soedjarwo Adjeng MJ Khrishto Damar Alam
| distributor    = SinemArt
| released       =  
| runtime        = 90 minutes
| country        = Indonesia
| awards         = See #Awards| Indonesian
| budget         =
| gross          =
}}
Mendadak Dangdut (Suddenly Dangdut) is a 2006 Indonesian drama film|dramatic-comedy film directed by Rudy Soedjarwo and written by Monty Tiwa. Starring Titi Kamal, Kinaryosih, and Dwi Sasono, it details the rise and fall of a dangdut singer and her sister-cum-manager.

Shot over a period of seven days, Mendadak Dangdut addresses social themes such as poverty. The films release was preceded by a soundtrack album, which was ultimately more commercially viable than the film itself; one single, "Jablai" ("Rarely Caressed"), became a staple of dangdut concerts for several months. The film, which received mixed critical reception, proved a breakthrough role for Kamal and won 12 awards, including Favourite Film at the 2007 Indonesian Movie Awards.

==Plot==
Petris (Titi Kamal) is an egotistical and emotional alternative rock vocalist at the start of her career. She often argues with her sister and manager, Yulia (Kinaryosih). One day they are caught with a large amount of heroin, which belongs to Yulias boyfriend, in their car. They escape the police and reach a nearby village, where a travelling dangdut concert is located. The performer has quit, and thus the concert needs a new singer. Yulia, who considers it a way to escape the police, tells Petris that they should join the group. At first reluctant, Petris then agrees and takes up the stage name Iis.

While with the group, Petris lives with its owner/manager Rizal (Dwi Sasono), whom she admires and for whom Yulia begins falling. Petris improves her vocals and becomes aware of the poverty and misfortune endured by the people in villages where she performs. She and the group become famous and receive numerous job offers; meanwhile, the sisters feel relieved as they are no longer pursued by the police. After several weeks Petris and Yulia have a big fight, which leaves Yulia in tears. Petris, feeling guilty, chases after her sister and finds her at Rizals house. Seeing her sisters pain makes Petris more understanding of other people.

After counting the proceeds, the sisters leave the house. However, they are approached by police and are caught in an attempt to escape. Petris tells her sister to escape, taking all the blame upon herself; Yulia gives herself up after seeing her Petris arrested. At the police station the sisters are told that they are no longer wanted for the drug charges, as Yulias ex-boyfriend had been caught, but must serve time for escaping the police. While in prison, Petris becomes a singer and entertains her fellow inmates.

==Production==
Mendadak Dangdut was directed by Rudy Soedjarwo.  In an interview, Soedjarwo said that he directed the film to contrast city people and villagers, including economic differences;  he often inserted messages on morality into his films.  The script was written by Monty Tiwa, who initially had difficulty selling it as studios considered it uncommercial. He also wrote the films soundtrack.  The two had previously worked together on Soedjarwos drama film 9 Naga (9 Dragons; 2006). 

Former fashion model Titi Kamal was cast as Patrice. For the role, Kamal took vocal coaching in both pop and dangdut.  The low-budget work  was filmed over a period of seven days  in various parts of Jakarta. 

==Release and reception==
Menadadak Dangdut was released in Indonesia on 10 August 2006,  with a soundtrack album preceding it in July  and a novelisation released not long afterwards.  The film was a moderate box office hit but was outperformed by its soundtrack; one of the songs from the film, "Jablai" ("Rarely Caressed"), became a staple of dangdut concerts after the films release.  The film saw another theatrical release on 1 November 2007, this time in Malaysia. 

Benny Benke, reviewing for the Semarang-based newspaper Suara Merdeka after a press screening, found the film to be fresh, fun, and interesting, with a down to earth plot and good soundtrack.  In a review for Tempo (Indonesian magazine)|Tempo magazine, Yos Rizal S. and Evieta Fadjar wrote that the plot suffered a little from a lack of logic, but that the dangdut music was well packaged for city dwellers.  Sita Planasari A., in another review for the magazine, described the film as clearly meant to entertain; she found the acting in Mendadak Dangdut to be strong, but wrote that the plot suffered from a lack of logic and that the cinematography was like that of a soap opera. 

The film proved Titi Kamals breakthrough role, as she was cast in several prestigious films  and received numerous recording opportunities afterwards.  Meanwhile, Soedjarwo continued using an express shooting style with his next box-office hit, the horror film Pocong 2 (2007). 

==Awards==
Mendadak Dangdut was nominated for seven Citra Awards at the 2006 Indonesian Film Festival, winning one. At the 2007 Bandung Film Festival, the film won two awards, while at the 2007 Indonesian Movie Awards it won seven Golden Screens.  For her work in the film, Kamal won Best Contemporary Solo Dangdut Singer at the 2007 Anugerah Musik Indonesia for dangdut. 

{| class="wikitable" style="font-size: 95%;"
|-
! scope="col" | Award Year
! scope="col" | Category
! scope="col" | Recipient
! scope="col" | Result
|-
! scope="row" rowspan="7" | Indonesian Film Festival
| rowspan="7" | 2006
| Best Film
|  
|  
|- Best Script Monty Tiwa
| 
|-
| Best Sound Arrangement
| Adityawan Susanto, Trisno
|  
|-
| Best Musical Arrangement
| Andi Rianto
|  
|- Best Leading Actor
| Dwi Sasono
|  
|- Best Leading Actress
| Titi Kamal
|  
|- Best Supporting Actress
| Kinaryosih
|  
|-
! scope="row" rowspan="2" | Bandung Film Festival
| rowspan="9" | 2007
| Best Music Andi Rianto
|  
|-
| Best Supporting Actress
| Kinaryosih
|  
|-
! scope="row" rowspan="7" | Indonesian Movie Awards
| Favourite Film
|  
|  
|-
| Favourite Supporting Actor
| Dwi Sasono
|  
|-
| Best Supporting Actress
| Kinaryosih
|  
|-
| Favourite Supporting Actress
| Kinaryosih
|  
|-
| Best Pair
| Titi Kamal and Kinaryosih
|  
|-
| Favourite Pair
| Titi Kamal and Kinaryosih
|  
|-
| Special Award for Best Singing Actor/Actress Titi Kamal
|  
|}

==Soundtrack album track listing==
The soundtrack album to Mendadak Dangdut included ten songs, with seven sung by Titi Kamal. 

{{Track listing
| collapsed =
| headline = 
| extra_column = Vocals
| total_length = 
| all_writing =
| all_lyrics =
| all_music =
| writing_credits = 
| lyrics_credits = yes
| music_credits = yes

| title1 = Bunuh Reality
| note1 = "Kill Reality"
| writer1 = 
| lyrics1 = Monty Tiwa
| music1 = Monty Tiwa
| extra1 = Titi Kamal
| length1 =

| title2 = Jablai (Jarang Dibelai)
| note2 = "Jablai (Rarely Caressed)"
| writer2 = 
| lyrics2 = Mulyadi and Uki Nuril
| music2 = Monty Tiwa
| extra2 = Titi Kamal
| length2 =

| title3 = Dangdutkah Kita
| note3 = "Are We Dangdut"
| writer3 = 
| lyrics3 = Monty Tiwa
| music3 = Monty Tiwa
| extra3 = The Mons
| length3 =

| title4 = Mars Pembantu
| note4 = "March of the Housemaids"
| writer4 = Monty Tiwa, Uki Nuril, Nanda Nuril, Ganden, and Dimas Projo
| lyrics4 = Monty Tiwa
| music4 = Monty Tiwa
| extra4 = Titi Kamal
| length4 =

| title5 = Selalu Ada
| note5 = "Always There"
| writer5 = 
| lyrics5 = Monty Tiwa
| music5 = Nanda Nuril and Monty Tiwa
| extra5 = Titi Kamal
| length5 =

| title6 = Buronan Cinta
| note6 = "Loves Fugitive"
| writer6 = 
| lyrics6 = Monty Tiwa
| music6 = Uki Nuril and Monty Tiwa
| extra6 = Titi Kamal
| length6 =

| title7 = Dangdutkah Kita
| note7 = "Are We Dangdut"
| writer7 = 
| lyrics7 = Monty Tiwa
| music7 = Monty Tiwa
| extra7 = Titi Kamal
| length7 =

| title8 = Masa Remaja
| note8 = Adeudeuh Kakang
| writer8 =
| lyrics8 = Uki Nuril
| music8 = Uki Nuril
| extra8 = Titi Kamal
| length8 =

| title9 = Jablai (remix)
| note9 = 
| writer9 = 
| lyrics9 = Monty Tiwa
| music9 = Mulyadi and Uki Nuril
| extra9 = 
| length9 =

| title10 = Dongeng Klasik
| note10 = "Classic Fairytale"
| writer10 =
| lyrics10 =Monty Tiwa
| music10 = Monty Tiwa, Nanda Nuril, Uki Nuril, Adi Karno, Dimas Projo, and Ganden
| extra10 = The Mon’s
| length10 =
|}}

==References==
;Footnotes
 

;Bibliography
 
*{{cite news
 |title=Kelucuan Serbamendadak
 |trans_title=Full of Funny Surprises
 |last1=Benke
 |first1=Benny
 |language=Indonesian
 |work=Suara Merdeka
 |date=4 August 2006
 |accessdate=29 May 2012 ref = 
 |archiveurl=http://www.webcitation.org/680znZuVR
 |archivedate=29 May 2012
 |url=http://www.suaramerdeka.com/harian/0608/04/bud01.htm
}}
*{{cite news
 |title=Tampik Rekaman Dangdut
 |trans_title=Opportunies to Record Dangdut
 |last1=Herman
 |first=Ami
 |language=Indonesian
 |work=Suara Karya
 |date=26 December 2007
 |accessdate=29 May 2012 ref = 
 |archiveurl=http://www.webcitation.org/68112WAB6
 |archivedate=29 May 2012
 |url=http://www.suarakarya-online.com/news.html?id=151896
}}
*{{cite news
 |url=http://www.thejakartapost.com/news/2006/12/24/hits-and-misses-indonesian-films-2006.html
 |title=Hits and misses of Indonesian films 2006
 |work=The Jakarta Post
 |date=24 December 2006
 |accessdate=29 May 2012
 |archiveurl=http://www.webcitation.org/680uQIgxj
 |archivedate=29 May 2012
 |ref= 
}}
*{{cite news
 |url=http://www.thejakartapost.com/news/2008/03/08/rudi039s-return-horror-dashes-expectations.html
 |first=Rizal
 |last=Iwan
 |date=8 March 2008
 |work=The Jakarta Post
 |title=Rudis return to horror dashes expectations
 |accessdate=29 May 2012
 |archivedate=29 May 2012
 |archiveurl=http://www.webcitation.org/680xOZlW6
 |ref= 
}}
*{{cite news
 |title=Dikira Pemulung
 |trans_title=Thought a Beggar
 |last1=Laurentius
 |language=Indonesian
 |work=Suara Karya
 |date=10 August 2006
 |accessdate=29 May 2012 ref = 
 |archiveurl=http://www.webcitation.org/6810e5OlG
 |archivedate=29 May 2012
 |url=http://www.suarakarya-online.com/news.html?id=151896
}}
*{{cite news
 |title=Dunia Film Harus Merdeka
 |trans_title=The World of Film Must Be Free
 |last1=Laurentius
 |language=Indonesian
 |work=Suara Karya
 |date=18 August 2006
 |accessdate=29 May 2012 ref = 
 |archiveurl=http://www.webcitation.org/6811C6ziz
 |archivedate=29 May 2012
 |url=http://www.suarakarya-online.com/news.html?id=152649
}}
*{{cite news
 |title="Mendadak Dangdut" Menggoyang Malaysia
 |trans_title="Mendadak Dangdut" Rocks Malaysia
 |language=Indonesian
 |work=Suara Merdeka
 |date=1 November 2007
 |accessdate=29 May 2012 ref = 
 |archiveurl=http://www.webcitation.org/6810ELrCt
 |archivedate=29 May 2012
 |url=http://www.suaramerdeka.com/harian/0711/01/bud01.htm
}}
*{{cite news
 |url=http://www.thejakartapost.com/news/2008/01/23/movie-makers.html
 |date=23 January 2008
 |work=The Jakarta Post
 |title=Movie Makers
 |accessdate=29 May 2012
 |archivedate=29 May 2012
 |archiveurl=http://www.webcitation.org/680vCzfui
 |ref= 
}}
*{{cite web
 |title=Penghargaan Mendadak Dangdut
 |trans_title=Awards for Mendadak Dangdut
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-m015-06-744728_mendadak-dangdut/award#.T8RNd1KVzMw
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=29 May 2012
 |archiveurl=http://www.webcitation.org/680wJfbFK
 |archivedate=29 May 2012
 |ref= 
}}
*{{cite news
 |title=Terjebak Dangdut
 |trans_title=Trapped by Dangdut
 |last1=Planasari A.
 |first1=Sita
 |language=Indonesian
 |work=Tempo
 |date=11 August 2006
 |accessdate=29 May 2012 ref = 
 |archiveurl=http://www.webcitation.org/680yEE5dr
 |archivedate=29 May 2012
 |url=http://www.tempo.co.id/hg/budaya/2006/08/11/brk,20060811-81692,id.html
}}
*{{cite news
 |title=Film Cepat Saji ala Rudi
 |trans_title=Quick Serve Films ala Rudi
 |last1=Rizal S.
 |first1=Yos
 |last2=Fadjar
 |first2=Evieta
 |language=Indonesian
 |work=Tempo
 |date=14 August 2006
 |accessdate=29 May 2012 ref = 
 |archiveurl=http://www.webcitation.org/680yEE5dr
 |archivedate=29 May 2012
 |url=http://majalah.tempointeraktif.com/id/arsip/2006/08/14/FL/mbm.20060814.FL121378.id.html
}}
*{{cite news
 |url=http://www.thejakartapost.com/news/2009/03/31/titi-a-wedding-singer-wulan-and-adilla.html
 |date=31 March 2009
 |work=The Jakarta Post
 |title=Titi, a wedding singer for Wulan and Adilla
 |accessdate=12 March 2012
 |archivedate=12 March 2012
 |archiveurl=http://www.webcitation.org/666VkmRCI
 |ref= 
}}
*{{cite news
 |title=Titi Kamal: Mendadak Dangdut
 |language=Indonesian
 |work=Tempo
 |date=8 May 2006
 |accessdate=12 March 2012 ref = 
 |archiveurl=http://www.webcitation.org/666abjfqb
 |archivedate=12 March 2012
 |url=http://majalah.tempointeraktif.com/id/arsip/2006/05/08/PT/mbm.20060508.PT119298.id.html
}}
 

==External links==
* 
* 

 
 
 