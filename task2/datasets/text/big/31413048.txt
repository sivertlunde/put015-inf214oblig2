Allah Made Me Funny: Live in Concert
 
{{Infobox film
| name           = Allah Made Me Funny: Live in Concert
| image	         = Allah Made Me Funny VideoCover.png
| image size     = 
| caption        = Promotional film poster
| director       = Andrea Kalin Bryant "Preacher" Moss Mohammed "Mo" Amer Bryant "Preacher" Moss Azhar Usman
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Mohammed "Mo" Amer Bryant "Preacher" Moss Azhar Usman
| music          = 
| cinematography = John Rhode Bryan Sarkinen
| editing        = David Grossbach
| studio         = Unity Productions Foundation Spark Media Handshake Productions
| distributor    = Truly Indie
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = Arabic English   
| budget         = $10,000
| gross          = 
}}
 concert documentary Mohammed "Mo" Bryant "Preacher" Moss and Azhar Usman). 

==Performers==
As a child Azhar Usman lived "in mostly Jewish Chicago suburb of Skokie, Illinois", where he was born. His family emigrated to the United States from India.  He is a former lecturer, community activist, and lawyer, and is often referred to as the Ayatollah of Comedy and Bin Laughin.   

Mohammed "Mo" Amer is a Palestinian people|Palestinian. The youngest of six children, he was born in Kuwait. When he was nine years old he emigrated with his family to Houston, Texas. 
 converted to Islam. He is a comedian and writer. He was born in Washington, D.C. 

==Documentary==
The documentary that lasts 82 minutes features each performer for about 20 minutes. Most of the film was shot during their performance in Los Angeles, California in 2007, a year before the documentary was released.    The remaining time features comedians at their homes, working on their computers, enjoying the time with their families. The spectators see the men working as "they prepare fresh material on cultural stereotyping, terrorism, flying post-9/11 and other tricky subjects not regularly regarded as funny business." 

The comedians are trying to break out stereotypes that some people hold about Muslims:  }}

==Reviews==
Laura Kern writes in   "For those itching to see a Muslim-themed variation on   calls the film "Allah Made Me Not Funny". He continues: "In the style of Margaret Cho and Carlos Mencia, these men spit out highly politicized routines about the nexus of race and religion but never risk Cho and Mencias vulgarity. This means theyre also not as frequently off-putting, but they hardly bring anything new to the table." But he ends his review with "Of course, because they are pioneers of sorts, Ill give them a second chance to win me over."    Serena Donadoni writes for The cinema Girl "In 2005, Albert Brooks went Looking for Comedy in the Muslim World, but its the performers of Allah Made Me Funny who really found it."  Ted Fry from The Seattle Times writes: "The hard work of being a stand-up comic doesnt need any extra obstacles, but the three comedians featured in "Allah Made Me Funny" share one that theyre happy to laugh about: being Muslim in America."   

==See also==
* 
*Allah Made Me Funny - Official Muslim Comedy Show - Live HMV Apollo

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
  
 
 
 
 