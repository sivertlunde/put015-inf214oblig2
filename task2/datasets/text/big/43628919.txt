Punar Jenmam
{{Infobox film
| name = Punar Jenmam
| image =
| image_size =
| caption =
| director = R. S. Mani
| producer = N. S. Diraviyam
| story = Amiya Chakraborthy
| writer = C. V. Sridhar
| screenplay = C. V. Sridhar Padmini Ragini Ragini Pasupuleti Kannamba|P. Kannamba K. A. Thangavelu T. R. Ramachandran
| music = T. Chalapathi Rao 
| cinematography = A. Vincent
| editing = P. V. Narayanan
| studio = Vijaya Films
| distributor = Vijaya Films
| released =   
| runtime = 148 mins
| country = India Tamil
}}
 1961 Cinema Indian Tamil Tamil film, Ragini and Pasupuleti Kannamba|P. Kannamba in lead roles. The film, produced by N. S. Diraviyam under Vijaya Films banner, had musical score by T. Chalapathi Rao and was released on 21 April 1961. 

==Plot==
Shankar goes to the city to better himself and comes back as a better person but before he realize his mistakes, his mother dies. Will he change his ways.

==Cast==
*Sivaji Ganesan as Shankar Padmini  Ragini
*Pasupuleti Kannamba|P. Kannamba
*M. S. Sundaribai
*K. A. Thangavelu
*T. R. Ramachandran
*M. R. Santhanam
*Sairam
* Pakkirisamy

==Crew==
*Producer: N. S. Diraviyam
*Production Company: Vijaya Films
*Director: R. S. Mani
*Music: T. Chalapathi Rao Pattukkottai Kalyanasundaram, Kannadasan & A. Maruthakasi
*Story: Amiya Chakraborthy
*Screenplay: C. V. Sridhar
*Dialogues: C. V. Sridhar
*Art Direction: Ganga
*Editing: P. V. Narayanan
*Choreography: B. Hiralal, Madhavan & B. Sohanlal
*Cinematography: A. Vincent
*Stunt: None
*Audiography: A. Krishnan, V. Sivaram, T. S. Rangasamy, P. V. Koteswara Rao & E. I. Jeeva
*Dance: None

==Soundtrack== Pattukkottai Kalyanasundaram, Playback singers are T. M. Soundararajan, A. M. Rajah, Seerkazhi Govindarajan, Tiruchy Loganathan, P. Suseela, Jikki, S. Janaki & Saraswathi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Pattukkottai Kalyanasundaram || 02:18
|-
| 2 || Endrum Thunbamillai   || T. M. Soundararajan || 02:48
|-
| 3 || Engum Sonthamillai || T. M. Soundararajan || Kannadasan || 02:59
|-
| 4 || Kannadi Pathirathil || P. Susheela || A. Maruthakasi || 03:08
|- Pattukkottai Kalyanasundaram || 04:10
|- Pattukkottai Kalyanasundaram || 06:20
|-
| 7 || Paadam Sariyaa || Tiruchy Loganathan, Jikki || A. Maruthakasi || 03:14
|- Kannadasan || 02:59
|-
| 9 || Ullangal Ondragi || A. M. Rajah, P. Susheela || 03:17
|- Pattukkottai Kalyanasundaram || 02:42
|}

==References==
 

 
 
 
 


 