Female Ninja Magic: 100 Trampled Flowers
{{Infobox film
| name = Female Ninja Magic: 100 Trampled Flowers
| image = Female Ninja Magic 100 Trampled Flowers.jpg
| image_size = 
| caption = Theatrical poster for Female Ninja Magic: 100 Trampled Flowers (1974)
| director = Chūsei Sone   
| producer = Hiromi Higuchi
| writer = Masaru Takasue
| narrator = 
| starring = Junko Miyashita
| music = Hajime Kaburagi
| cinematography = Masaru Mori
| editing = Masanori Tsujii
| distributor = Nikkatsu
| released = August 3, 1974
| runtime = 76 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1974 Japanese film in Nikkatsus Pink film#Second wave (The Nikkatsu Roman Porno era 1971–1982)|Roman porno series, directed by Chūsei Sone and starring Junko Miyashita.

==Synopsis== Iga ninjas to take over the Akizuki clans lands. The Akizuki hire the Fumi ninjas, a group of female warriors who enhance their fighting ability with sexual magic such as the "white snake spell". When the Akizuki emerge victorious, Tsukinojo, the leader of the Fumi ninjas, marries Lord Akizuki.      

==Cast==
* Junko Miyashita: Tsukinojo 魔羅月之丞  
* Kyōko Kanō: White Snake Woman
* Hitomi Kozue: White Snake Woman
* Yūko Katagiri
* Yuri Yamashina: Blue Fox
* Setsuko Ōyama: Koyuki
* Maya Hiromi: O-kei
* Hajime Tanimoto: Mamoru Wakasa
* Keisuke Yukioka: Sansaemon Horiguchi
* Hiroshi Osa: Hattori Hanzō
* Nagatoshi Sakamoto: Norizen Kuroiwa
* Hyōe Enoki: Kamekubi
* Tadayuki Kitakami: Gankubi
* Hitomi Kozue: お真知の方 
* Naomi Oka: お蓮の方 
* Tatsuya Hamaguchi: Ōmi
* Kenji Shimamura: Old man

==Background==
The film anticipates the theme of Toeis In Bed With the Enemy: Female Ninjas (1976) which itself spawned several imitations. In their Japanese Cinema Encyclopedia: The Sex Films, the Weissers write that this indicates Nikkatsus ability to keep current with the moods of popular culture.  During the mid-1970s, when director Sones output was uneven, the Weissers judge the film to show him at his best.  It also serves as an example, they write, of popular actress Yūko Katagiris variable career. Previously promoted as a star, her career had stalled due to her typecasting in teenage roles. She had expanded her acting range by starring in   writes that the film is an "action-packed softcore melodrama". 

==Availability==
Female Ninja Magic: 100 Trampled Flowers was released theatrically in Japan on August 3, 1974.  It was released to home video in VHS format on December 2, 1996. 

==Bibliography==

===English===
*  
*   
*  
*  
*  

===Japanese===
*  
*  
*  
*  
*  

==Notes==
 

 
 
 
 
 
 
 


 
 