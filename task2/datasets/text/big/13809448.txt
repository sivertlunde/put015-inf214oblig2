Life of the Party (1920 film)
 
{{Infobox film
| name           = Life of the Party
| image          = Fatty Arbuckle, Life of the Party, 1920.ogg
| caption        = Clip from the film
| director       = Joseph Henabery
| producer       =  Walter Woods Fatty Arbuckle Karl Brown
| editing        = 
| studio         = Famous Players-Lasky Corporation
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short comedy Fatty Arbuckle.    A copy is held by the Library of Congress. 

==Plot==
Attorney Algernon Leary (Fatty Arbuckle), "pure milk" candidate for mayor, attends a party for grown-ups dressed as children. Going home in a blizzard, he is robbed of his fur coat leaving him bare legged wearing rompers. He takes refuge in the first building he can reach, creating havoc in various apartments due to his appearance. He blunders into the rival candidate (Frank Campeau) in a compromising situation with a vamp and forces him to withdraw, ensuring Learys election as mayor after a whirlwind campaign. {{cite journal
 | title ="The Life of the Party" Anything But Dull
 | journal =Film Fun
 | page =12
 | publisher =
 | location =New York
 | date =January 1921
 | url =https://archive.org/details/FilmFunJanuary1921
 | accessdate =2013-10-26}} 

==Cast== Roscoe Fatty Arbuckle - Algernon Leary (as Roscoe Arbuckle)
* Winifred Greenwood - Mrs. Carraway
* Roscoe Karns - Sam Perkins
* Julia Faye - French Kate
* Frank Campeau - Judge Voris
* Allen Connor - Jake
* Fred Starr - Bolton (as Frederick Starr)
* Ben Lewis - Clay
* Viora Daniel - Milly Hollister

==Film still synopsis==
The December 1921 Film Fun provided a synopsis of the film using Film still|stills.
  Attorney Leary (Arbuckle) promises the committee pure milk and fair service if he has to fight for it.  The man (Campeau) responsible for conditions defies the attorney, who thereupon runs for mayor in opposition.  At the childrens party he is vamped by a leader of his rivals forces.  So he starts for home and on the way is relieved of his fur overcoat.  Taking refuge from the blizzard in the first apartment house he surprises the rival candidate in a vamps room. 
File:Life of the Party (1920) - 6.jpg|Cold. Of course. Nevertheless the one he likes best from the committee brings news of the election. 
 

==See also==
* Fatty Arbuckle filmography

==References==
 

==External links==
 
* 
* , allrovi.com

 
 
 
 
 
 
 
 
 
 