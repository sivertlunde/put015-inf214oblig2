Godless Men
{{Infobox film
| name           = Gddless Men
| image          = Godless Men (1920) - 1.jpg
| caption        = Newspaper ad for film with Helene Chadwick
| director       = Reginald Barker
| producer       = Goldwyn Pictures
| writer         = J. G. Hawks (co-scenario) Edfrid A. Bingham (co-scenario)
| based on       = Black Pawl, Saturday Evening Post by Ben Ames Williams Russell Simpson James "Jim" Mason
| music          =
| cinematography = Percy Hilburn
| editing        = J. G. Hawks
| distributor    = Goldwyn Pictures
| released       = November 1920
| runtime        = 70 minutes
| country        = United States
| language       = Silent (English intertitles)
}} Russell Simpson James "Jim" Mason as a father and son. It is based on a Saturday Evening Post short story Black Pawl by Ben Ames Williams. 

This film exists in a private collection.  

==Cast== Russell Simpson - Black Pawl James "Jim" Mason - Red Pawl
*Helene Chadwick - Ruth Lytton John Bowers - Dan Darrin
*Alec B. Francis - Reverend Sam Poor
*Bob Kortman - Seaman Speiss
*Irene Rich - Black Pawls Wife
*Lionel Belmore - Seaman Neighbor
*Frankie Lee - Red Pawl as a Boy (uncredited)
*Guinn "Big Boy" Williams - Seaman (uncredited)

==References==
 

==External links==
* 
* 
* 
*  of beginning credits card
*  at silenthollywood.com

 
 
 
 
 
 
 
 


 