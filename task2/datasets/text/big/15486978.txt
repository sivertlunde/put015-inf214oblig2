Continental, a Film Without Guns
{{Infobox film
| name           = Continental, un film sans fusil
| image          = Continental, un film sans fusil.jpg
| caption        = 
| director       = Stéphane Lafleur
| producer       = Luc Déry Kim McCraw
| writer         = Stéphane Lafleur
| starring       = Réal Bossé Marie-Ginette Guay Fanny Mallette Pauline Martin Gilbert Sicotte
| music          = Stéphane Lafleur Hugo Lavoie
| cinematography = Sara Mishara
| editing        = Sophie Leblond
| distributor    = Christal Films
| released       =  
| runtime        = 103 minutes
| country        = Canada
| language       = French
| budget         = 
| gross          = 
}} directed and written by Stéphane Lafleur.

== Plot ==
The lives of four people intertwine after the disappearance of a man who wanders into the forest. A Man wakes up on a bus. Everybody is gone. Night has fallen. He gets off the bus and finds himself at the edge of a forest. Sounds are coming from deep within the woods. The Man enters the forest and disappears into the night.

== Recognition ==

* Won: Toronto International Film Festival: Best Canadian First Feature Film - Stéphane Lafleur
* Official Selection: Venice Film Festival
**Jutra Awards:
*** Best Motion Picture of the Year (Meilleur Film) - Luc Déry Kim McCraw
*** Best Achievement in Directing (Meilleure Réalisation) - Stéphane Lafleur
*** Best Performance by an Actor in a Supporting Role (Meilleur Acteur de Soutien) - Réal Bossé
*** Best Screenplay (Meilleur Scénario) - Stéphane Lafleur
* Nominee:
**Genie Award for Best Motion Picture - Luc Déry Kim McCraw
**Genie Award for Best Achievement in Art Direction/Production Design - André-Line Beauparlant
**Genie Award for Best Performance by an Actor in a Supporting Role - Gilbert Sicotte
**Genie Award for Best Performance by an Actress in a Supporting Role - Marie-Ginette Guay
**Genie Award for Best Performance by an Actress in a Supporting Role - Fanny Mallette
**Jutra Awards:
*** Best Achievement in Cinematography (Meilleure Direction de la Photographie) - Sara Mishara
*** Best Achievement in Art Direction (Meilleure Direction Artistique) - André-Line Beauparlant
*** Best Achievement in Editing (Meilleure Montage) - Sophie Leblond
*** Best Achievement in Sound (Meilleur Son) - Pierre Bertrand, Sylvain Bellemare and Bernard Gariépy Strobl

== External links ==
*  
*  

 
 
 
 
 


 
 