This Is Nollywood
{{Infobox film
| name           = This is Nollywood 
| image          = Nollywood 9.jpg
| caption        = Director Bond Emeruwa and crew shoot a scene
| director       = Franco Sacchi
| producer       = Cargo Film & Releasing
| writer         = 
| starring       = 
| distributor    = 
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = Franco Sacchi Robert Caputo
| editing        = Franco Sacchi
| music          = Alan Perez Ted Corrigan Antibalas
}}
 Nigerian film industry, much along the same lines as the acclaimed 2007 documentary, Welcome to Nollywood, by Jamie Meltzer

Through the story of director Bond Emeruwa, this documentary tells the story of a $250 million industry that has created thousands of jobs. As the documentary follows Emeruwas production of Check Point, various members of the Nigerian filmmaking community discuss their industry, defend the types of films they make and the impact they have, and describe common difficulties they encounter, from hectic shooting schedules to losing electricity mid-shoot. 

== Synopsis ==
This is Nollywood follows Nigerian director Bond Emeruwa on his quest to finish filming a feature-length action movie in nine days on the outskirts of Lagos. However, Bond is just one of the incredible protagonists of Nollywood, Nigeria’s burgeoning, but little known movie industry that is rapidly changing Africa’s modern popular culture. In the end, the film is about more than a fascinating and unheralded movie industry, it is about how people surmount obstacles to achieve their dreams.

== Awards ==
* Festival Internacional de Abuja 2007

==See also==
*Welcome to Nollywood
*Nollywood Babylon

== References ==
 

==External links==
* 
* 
*  

 
 
 
 
 
 

 
 