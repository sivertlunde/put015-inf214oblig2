10 to 11
{{Infobox film
| name           = 10 to 11
| image          = 10to11FilmPoster.jpg
| caption        = Theatrical poster
| director       = Pelin Esmer
| producer       = Pelin Esmer Tolga Esmer Nida Karabol Akdeniz
| writer         = Pelin Esmer
| starring       = Nejat İşler Mithat Esmer Tayanç Ayaydın
| cinematography = Özgür Eken
| editing        = Ayhan Ergürsel Pelin Esmer Cem Yıldırım
| studio         = Sinefilm
| distributor    = Filmpot
| released       =  
| runtime        = 110 minutes
| country        = Turkey
| language       = Turkish
| gross          = $69,913
}}
10 to 11 ( ) is a 2009 Turkish drama film directed by Pelin Esmer.

The film, loosely based on the story of Esmer’s uncle, Mithat Esmer, who also plays the leading role, follows an elderly collector in İstanbul who lives in a rundown apartment building that is under threat of demolition.

==Release==
=== General release ===
The film opened in 30 screens across Turkey on   at number eleven in the Turkish box office chart with an opening weekend gross of $21,895.   

===Festival screenings===
* 28th Istanbul International Film Festival (April 4–19, 2009)   
* 17th Altın Koza International Film Festival 
* 3rd International Middle East Film Festival (October 9–17, 2009) 
* 20th Tromso International Film Festival (January 19–24, 2010)   
* 39th International Film Festival Rotterdam (January 27-February 7, 2010) 
* Travelling Rennes Film Festival (February 9–16, 2010) 
* 15th Nuremberg Turkish-German Film Festival 
* 21st Ankara International Film Festival (March 11–21, 2010)
* Cinema Novo Film Festival (March 11–21, 2010) 
* Crossing Europe Film Festival (April 20–25, 2010) 
* IndieLisboa Film Festival (April 22-May 2, 2010) 

==Reception==
===Box office===
The film debuted number eleven in the Turkish box office chart and has made a total gross of $69,913. 

===Awards===
* 28th Istanbul International Film Festival (4-19 Apr, 2009) 
** Special Jury Prize - National Competition  (Won)
* 17th Altın Koza International Film Festival 
** Golden Boll Award for Best Film: Pelin Esmer   (Won)
** Golden Boll Award for Best Screenplay: Pelin Esmer  (Won)
* 3rd International Middle East Film Festival (9-17 Oct, 2009)    
** Black Pearl Award for Best New Middle Eastern Narrative Director Award: Pelin Esmer (Won)
* 20th Tromso International Film Festival (19-24 Jan, 2010)   
** FIPRESCI Award (Won)
* 15th Nuremberg Turkish-German Film Festival    
** Best Film Award (Won)
** Cinema Critics Award (Won)
* 21st Ankara International Film Festival (11–21 March 2010)      
** Best Director Award: Pelin Esmer (Won)
** Best Screenplay Award: Pelin Esmer (Won)

==See also==
* 2009 in film
* Turkish films of 2009

==References==
 

==External links==
*   for the film
*  
*  

 

 
 
 
 
 