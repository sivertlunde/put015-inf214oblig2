Manam Kothi Paravai
{{Infobox film
| name = Manam Kothi Paravai
| image = Manam kothi paravai.jpg
| caption = Poster
| director = Ezhil
| producer = Ambeth Kumar Ranjeev Menon Ezhil
| writer   = Ezhil
| starring = Sivakarthikeyan Aathmiya Ravi Mariya
| cinematography = Sooraj Nallusami
| music = D. Imman
| editing = Gopi
| studio = Olympia Movies
| released =  
| country = India
| language = Tamil
}} Anjada Gandu with Satish Neenasam and Subhiksha.

==Plot==
Kannan (Sivakarthikeyan) is a carefree youth who assists his father (Ilavarasu) in his construction business. He spends time with his friends (Soori (actor)|Soori, Singam Puli and Kishore).

Kannan is in love with his neighbour Revathy (Athmiya), whose father and uncles are the most dreaded goons in the village. They are both childhood friends. He gets a shocker when he decides to reveal his love to Revathy. Her family has arranged her a marriage with an influential man. To avoid the arranged marriage, Kannan’s friends from Mumbai (Sreenath and Chams) kidnap Revathy.

There is a twist in the tale. Revathy declares she has no romantic interest over Kannan. "He is just a friend", says the girl at first. But later reveals that she loves Kannan too, so much so that if others knew it, itd be a problem to him and prove to be a risk to his life.

Meanwhile, the bride-to-be’s family searches frantically to find their girl. They get hold of Kannan and his friends. Revathy and Kannan are separated.

After two years, Kannan returns from Oman after having worked as a Site Engineer there, to meet his love. He meets her with avidity and they talk for a while when Revathy asks him to leave immediately, for fear that her father would harm him. Kannan, still a passionate lover, asks Revathy if they could marry, now that shes not married to anybody else.
Revathys father barges in unexpectledly and asks Kannan to leave the house... but with his daughter.

==Cast== Sivakarthikeyan as Kannan
*Aathmiya as Revathy
*Ilavarasu as Ramaiah
*Singampuli as Modu Mutti Soori as Nalla Thambi Srinath as Nayar
*Venniradai Moorthy
*Ravi Mariya as Natraj
*Chaams
* M. R. Kishore Kumar
*Aadukalam Naren
*Jay Kumar
*Anbarasan

==Production==
Producer Ambeth Kumar and Ranjeev Menon are friends of director Ezhil. When they asked him how the shooting of Manam Kothi Paravai was proceeding, he made them listen to the songs tuned by D. Imman. Impressed, they decided to produce it. 
 Marina and an important role in the recently released 3 (2012 Indian film)|3, Sivakarthikeyan is taking up a full-fledged lead role again for Manam Kothi Paravai. 

The film, which is set in a village, has been shot at Ezhils native place Kayathur, near Mayavaram and Siva Karthikeyans Thiruvizhimizhalai. 

==Soundtrack==
{{Infobox album
| Name = Manam Kothi Paravai
| Type = soundtrack
| Artist = D. Imman
| Cover = 
| Released =  
| Recorded = 2012
| Genre = Film soundtrack
| Length = 
| Label = Universal Music
| Producer = D. Imman
| Reviews =
| Last album  = Uchithanai Muharnthaal (2011)
| This album  = Manam Kothi Paravai (2012)
| Next album  = Kumki (film)|Kumki (2012)
}}

The music of Manam Kothi Paravai was scored by D. Imman. The soundtrack was released on 12 April 2012,  which features 8 tracks with lyrics written by Yugabharathi. Universal Music is back in the Tamil market after a 30 year hiatus with Manam Kothi Paravai. 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Notes
|- 1 || "Jal Jal Jal Osai" || Aalap Raju, Surmuki || 
|- 2 || "Po Po Po" || Javed Ali || 
|- 3 || "Dang Dang" || Saicharan, S.Malavika || 
|- 4 || "Yenna Solla" || Vijay Prakash, Chinmayi || 
|- 5 || "Ooraana Oorukkulla" || Santhosh Hariharan ||
|- 6 || "Po Po Po (Karaoke)" || D. Imman ||
|- 7 || "Jal Jal Jal Oosai (Karaoke)" || D. Imman || 
|- 8 || "Dang Dang (Karaoke)" || D. Imman || 
|}

===Critical reception===
Behindwoods wrote: "Imman has a winner with this movie".  Milliblog wrote: "Manam Kothi Paravai’s soundtrack surpasses anything that composer Imman has produced so far, including his most famous soundtrack, Mynaa. The tunes and music he puts together here demonstrate a maturity not seen in the composer’s work so far and makes for fantastic listen. 

==Release== Sun TV. The film was given a "U" certificate by the Indian Censor Board.

===Critical reception===
Hindu wrote:"MKP is a comedy it turns serious and when you settle down for the drama, matters turn ludicrous. Consistency in treatment is a casualty".  Sify wrote:"On the whole the film fails to deliver".  Behindwoods wrote:"Manam Kothi Paravai moves at a very relaxed pace. With not too many events packed into it, the movie might come across as a long haul if you are looking for a love story that is both entertaining and endearing. Parts of it are entertaining and parts of it, endearing". 

===Box office===
The film was declared as a Blockbuster comparing the budget and the collection at the box office.

==References==
 

 

 
 
 
 