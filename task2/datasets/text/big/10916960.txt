The Young Doctors (film)
{{Infobox film
| name           = The Young Doctors
| image size     =
| image	=	The Young Doctors FilmPoster.jpeg
| caption        =
| director       = Phil Karlson
| producer       = Stuart Millar Lawrence Turman Joseph Hayes
| narrator       =
| starring       = Ben Gazzara Fredric March
| music          = Elmer Bernstein
| cinematography = Arthur J. Ornitz
| editing        = Robert Swink
| distributor    = United Artists
| released       = August 23, 1961
| runtime        = 100 min.
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
The Young Doctors is a 1961 film directed by Phil Karlson and starring Ben Gazzara, Fredric March, Dick Clark, Ina Balin, Eddie Albert, Phyllis Love, Aline MacMahon, George Segal (in his first movie) and Dolph Sweet. The film is based on the 1959 novel The Final Diagnosis by Arthur Hailey.  Ronald Reagan was the narrator in the film.

== Plot == hemolytic disease, but Pearson believes that the tests are excessive and cancels the third test. Mrs. Alexander is married to a young intern at the hospital (Clark), who, along with Coleman,  tried to push for the third test.  When the baby is born seriously ill, Dr. Charles Dornberger (Albert), Mrs. Alexanders OB/GYN, berates Pearson and conducts a blood transfusion to save the babys life. Pearsons future at the hospital becomes uncertain, and he resigns. Coleman has changed his mind about Cathys tumor and agrees with Pearsons decision, while Pearson says that Coleman reminds him of himself when he was young and urges him not to let hospital bureaucracy to wear him down.

== External links ==
* 
* 
* 

== See also ==
* Ronald Reagan films

 

 
 
 
 
 
 
 
 
 
 
 