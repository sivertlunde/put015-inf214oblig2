Flypaper (2011 film)
{{Infobox film
| name           = Flypaper
| image          = Flypaper2011Poster.jpg
| caption        = Theatrical release poster
| director       = Rob Minkoff
| producer       = Mark Damon Peter Safran Patrick Dempsey Scott Moore
| screenplay     = 
| story          = 
| based on       =  
| starring       = Patrick Dempsey Ashley Judd
| music          = John Swihart
| cinematography = Steven Poster
| editing        = Tom Finan
| studio         =  The Safran Company
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Flypaper is a 2011 crime comedy film starring Patrick Dempsey and Ashley Judd, and directed by Rob Minkoff.

==Plot==
A bank is simultaneously attacked by two groups of robbers: three high-tech professionals and two rustic buffoons.  One bystander is quickly shot and killed, apparently by accident, and seven hostages taken.  One of the hostages, an obsessive-compulsive disorder|obsessive-compulsive customer, notices several puzzling details, from which he guesses that the coincidence was intentional: the robbers (among others) were lured here, with misleading blueprints and defective equipment, so that another criminal – at the top of the FBIs wanted list of bank robbers – could kill them to cover his own trail.

==Cast==
* Patrick Dempsey as Tripp Kennedy
* Ashley Judd as Kaitlin
* Tim Blake Nelson as Billy Ray Peanut Butter McCloud
* Mekhi Phifer as Darrien Matt Ryan as Gates
* Jeffrey Tambor as Gordon Blythe
* John Ventimiglia as Weinstein
* Pruitt Taylor Vince as Wyatt Jelly Jenkins
* Curtis Armstrong as Mitchell Wolf
* Rob Huebel as Rex Newbauer Adrian Martinez as Mr. Clean
* Natalia Safran as Swiss Miss
* Octavia Spencer as Madge Wiggins Eddie Matthews as Jack Hayes
* Rob Boltin as Credit Manager

==Production== Scott Moore, also wrote the screenplay for The Hangover. The director, Rob Minkoff, is well known for co-directing The Lion King.
Filming took place in Baton Rouge, Louisiana, in June 2010.

==Reception==
It was not critically well-received, with a 17% Rotten Tomatoes rating (only 3 of 18 critics liked it) and grossed only $1,100 total in its theatrical release at one theater on two screens with no advertising. 
Over time, the audience ratings have trended to 40% on Rotten Tomatoes. 

==References==
 

==External links==
*  
*   at Rotten Tomatoes

 

 
 
 
 
 
 
 
 


 