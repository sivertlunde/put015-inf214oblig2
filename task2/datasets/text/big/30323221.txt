Charlie Chan in the Secret Service
{{Infobox film
| name           = Charlie Chan in the Secret Service
| image_size     =
| image	         = Charlie Chan in the Secret Service FilmPoster.jpeg
| caption        =
| director       = Phil Rosen
| producer       = Phillip N. Krasne James S. Burkett
| writer         = Earl Derr Biggers (characters) George Callahan (screenplay)
| narrator       =
| starring       = see below
| music          =
| cinematography = Ira H. Morgan
| editing        = Martin G. Cohn
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Charlie Chan in the Secret Service is a 1944 mystery film starring Sidney Toler as Charlie Chan.  It is the first film made by Monogram Pictures after the series was dropped by 20th Century Fox, and it marks the introduction of Number Three Son (Benson Fong) and taxi driver (later Chans chauffeur), Birmingham Brown (Mantan Moreland).

==Plot==
In the two years since the last Charlie Chan feature film (Castle in the Desert), Charlie Chan is now an agent of the U.S. government working in Washington DC and he is assigned to investigate the murder of the inventor of a highly advanced torpedo. Aiding Chan is his overeager but dull-witted Number Three son Tommy (Benson Fong) and his Number Two Daughter Iris Chan (Marianne Quon). Also involved in the case is the bumbling and easily frightened Birmingham Brown (Mantan Moreland) who works as a limo driver for one of the suspects.

==Cast==
*Sidney Toler as Charlie Chan
*Mantan Moreland as Birmingham Brown, Taxi Driver
*Arthur Loft as Inspector Jones, Secret Service
*Gwen Kenyon as Inez Arranto
*Sarah Edwards as Mrs. Hargue, Housekeeper
*George J. Lewis as Paul Arranto (as George Lewis)
*Marianne Quon as Iris Chan
*Benson Fong as Tommy Chan
*Muni Seroff as Peter Laska
*Barry Bernard as David Blake
*Gene Roth as Luis Philipe Vega aka Von Vegon (as Gene Stutenroth)
*Eddy Chandler as Lewis, Secret Service (as Eddie Chandler)
*Lelah Tyler as Mrs. Williams

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 

 