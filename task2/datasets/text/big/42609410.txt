Ek Do Teen

{{Infobox film
| name           =Ek Do Teen
| image          =
| image_size     =
| caption        =
| director       =Roop K. Shorey
| producer       =Roop K. Shorey
| writer         =Krishan Chander
| narrator       = Motilal Meena Shorey Yashodra Katju Majnu Vinod
| cinematography =T. R. Joshi
| editing        =Pran Mehra
| distributor    =
| released       =1956
| runtime        =120 mins.
| country        =India
| language       =Hindi
| budget         =
| gross          =
}}
 1953 Hindi comedy film produced and directed by Roop K. Shorey. It stars Motilal (actor)|Motilal, Meena Shorey, Yashodra Katju, Majnu and Iftekhar.   The story, screenplay and dialogues were written by Krishan Chander. The story has some similarity to Somerset Maughams Facts Of Life.     The music was composed by Vinod and the lyricist was Aziz Kashmiri. 
 Vinod and picturised on her in Ek Thi Ladki. However Ek Do Teen did not do as well and was a turning point in Meena Shoreys career.   Ek Do Teen (One Two Three) is the number of caveats passed on by Motilal’s father to him before the father dies. The hero experiences each one of the three cautions only to find his father’s dying homily to be true.  

==Plot==

Roma (Meena Shorey) is on her way home after celebrating with her friends at Ashas (Yashodra Katju) place when she is attacked by two robbers who try to steal her purse. She faints as one of them brandishes a knife.  Motilal (Motilal) and his friend Hiralal (Majnu) are driving through that place and scare away the robbers. After wondering what to do with the unconscious girl they decide to take her  to Moti’s house to render treatment.  Roma makes a phone call to her father Seth Madanlal (K. N. Dhar) to tell him her whereabouts but he is too angry to know the reason why she is there except to ask her the address. On arriving there and seeing her with two men he gets angry and starts shouting which wakes up Motilal’s father Seth Ramlal (Romesh Thakur) and he appears downstairs. The two fathers fight loudly and Roma is taken away by her father. 

Roma and Motilal fall in love with each other and want to marry. Roma’s father finally agrees to the wedding but then Motilal’s father is in poor health and dying so the marriage is postponed. Before dying Seth Ramlal calls his son and tells him to avoid three things in life. First, never drink alcohol, second not to associate with women of loose morals and the third never tell a secret to a woman.  Having given his sermon the father dies.  Motilal goes through the first two and realises what his father meant as he gets into trouble. For the third caveat Motilal stages a drama about murdering his friend with his help and tells Roma about it. Between Roma, who tells her friend Asha, and Asha who tells her husband who is in the police the secret is no longer a secret. Moti is arrested and the friend has disappeared. Roma goes through a lot of action but finally manages to save Motilal.


==Cast==

*Motilal (actor)|Motilal: Motilal
*Meena Shorey: Roma
*Yashodra Katju: Asha (Romas friend)
*Majnu: Hiralal (Motilals friend)
*Iftekhar: Ashas husband
*Kaushalya
*Satish Batra
*Shamlal
*Romesh Thakur: Seth Ramlal (Motilal’s father)
*K. N. Dhar: Seth Madanlal (Roma’s father)
*Khatana
*Indra Bansal


==Soundtrack== 

Roop K. Shorey used music composer Vinod and lyricist Aziz Kashmiri once again after Ek Thi Ladki.       

{| class="wikitable"
|-
!Number !!Song !!Singer
|- Aaya Hai Asha Bhosle
|- Tumhe Chupke Mohammed Rafi, Asha Bhosle
|- Mile Nain Asha Bhosle, Minal Wagh, Pramodini Desai
|- Aaja Re Sandhya Mukherjee
|- Piya Jo Asha Bhosle, Mohammed Rafi
|- Ek Do Asha Bhosle
|- Lo Phir Asha Bhosle
|- Thumak Yhumak Asha Bhosle, G. M. Durrani
|- Chal Meri Mohammed Rafi, Minal Wagh, Asha Bhosle
|}

 
==References==
 

==External Links==
*    

 
 
 