Sällskapsresan 2 – Snowroller
 
{{Infobox film
| name           = Sällskapsresan 2 - Snowroller
| image          = Sällskapsresan2Snowroller.jpg
| caption        = Poster
| director       = Lasse Åberg, Peter Hald
| producer       =  Bo Jonsson, Lasse Åberg 
| starring       = Lasse Åberg, Jon Skolmen, Staffan Ling
| music          = 
| cinematography = 
| editing        = 
| distributor    = Svensk Filmindustri
| released       =  
| runtime        = 91 min 
| country        = Sweden Swedish German German English
| budget         = 
}}
Sällskapsresan 2 – Snowroller (English title: The Conducted Tour 2 - Snowroller) is a 1985 Swedish comedy film directed by Lasse Åberg and Peter Hald.

== Synopsis ==
The film is about the Swede Stig-Helmer and his Norwegian friend Ole going to the alps, where a lot of crazy things will happen.

==Cast==
*Lasse Åberg - Stig Helmer Olsson
*Jon Skolmen - Ole Bramserud
*Björn Granath -  Mr. Jönsson
*Staffan Ling - Hedlund
*Cecilia Walton - Lotta
*Eva Millberg - Kärran
*Ingrid Wallin - Mrs Jönsson
*Klasse Möllberg - Mackan Bengt Andersson - Brännström
*Jan Waldekranz - Nalle
*Oscar Franzén - Niklas Jönsson
*Erica Larsson - Sara Jönsson
*Felix S:t Clair - Felix
*Dieter Augustin - Dr. Katz
*David Kehoe - Algernon Wickham-Twistleton-Ffykes ("Algy")
*Barbro Hiort af Ornäs - Stig-Helmers mother

== External links ==
* 
* 

 

 
 
 
 
 
 


 
 