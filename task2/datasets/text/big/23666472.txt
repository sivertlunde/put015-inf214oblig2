Duel at Sundown (film)
{{Infobox film
| name           =Duell vor Sonnenuntergang
| image          =Duell vor Sonnenuntergang.jpg
| image_size     =
| caption        =
| director       = Leopold Lahola
| producer       = Leopold Lahola
| writer         = Anya Corvin, Leopold Lahola
| narrator       =
| starring       = Peter Van Eyck, Mario Girotti
| music          = Zvi Borodo
| cinematography = Janez Kalisnik Eastmancolor
| editor         = Wolfgang Wehrum Gloria
| released       =  
| runtime        = 110 minutes
| country        = West Germany Italy
| language       = German
| budget         =
}}
 1965 West West German Italian western film directed by Leopold Lahola.

==Story==
During a cattle drive, Don McGow leaves the herd in charge of his younger brother and rides off. When the herd is reported stolen he blames himself and goes in search of the thieves only to discover it is his brother.

==Cast==
*Peter van Eyck – Don McGow
*Carole Gray – Nancy Parker/Greenwood
*Wolfgang Kieling – Punch Parker/Greenwood
*Terence Hill as Mario Girotti – Larry McGow Carl Lange	– Pastor
*Walter Barnes – Pa McGow
*Jan Hendriks – Lord
*Todd Martin – Smokey Jim
*Giacomo Rossi-Stuart – Quents
*Klaus Dahlen – Baby Face
*Demeter Bitenc – Mack
*Kurt Heintel – Sheriff
*Slobodan Dimitrijevic – Ft. Clark rancher

==External links==
*  

 
 
 
 
 
 
 


 
 
 