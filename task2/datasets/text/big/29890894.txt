The First Texan
{{Infobox film
| name           = The First Texan
| image          = 
| image_size     = 
| caption        = 
| director       = Byron Haskin
| writer         = 
| narrator       = 
| starring       = Joel McCrea Felicia Farr Jeff Morrow
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = June 19, 1956
| runtime        = 82 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $1 million  
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The First Texan is a 1956 film directed by Byron Haskin. It stars Joel McCrea and Felicia Farr.  

==Plot==
Sam Houston, a lawyer and former governor of Tennessee, travels to San Antonio, Texas to begin a new life. He encounters Jim Bowie, who is determined to free the territory from Mexicos rule.

Bowie is tried for treason. Houston represents him in court and successfully argues that the charge against Bowie must be dismissed because Mexico was not under martial law at the time.

Katherine Delaney comes into Houstons life. He is still married back home, but separated and dictates a letter requesting a formal divorce. Katherine will not become involved with Houston unless he promises not to become actively involved in the fight to free Texas.

Davy Crockett relays a message from U.S. president Andrew Jackson, who wants Houston to lead the revolution. There are not enough troops at the Alamo to hold off General Antonio López de Santa Anna and the large Mexican army. And when Houston appears to be in full retreat, some of his men begin to feel he must be replaced.

It turns out Houston was planning a surprise attack. His forces are told to "remember the Alamo" and they proceed to overwhelm Santa Anna and his men. Texas is declared a free republic and Sam Houston its first president, a movement that eventually will lead to statehood.

==Cast==
*Joel McCrea as Sam Houston
*Felicia Farr as Katherine Delaney
*Jeff Morrow as Jim Bowie
*Wallace Ford as Henry Delaney
*Rodolfo Hoyos, Jr., as Colonel Martín Perfecto de Cos
*Abraham Sofaer as Don Carlos
*James Griffith as Davy Crockett
*William Hopper as William Barret Travis
*Dayton Lummis as Stephen F. Austin
*Roy Roberts as Col. Sam Sherman
*David Silva as General Antonio Lopez de Santa Anna
*Chubby Johnson as Deaf Smith
*Nelson Leigh as Colonel George Washington Hockley

==References==
 

==External links==
* 
 
 
 
 
 
 