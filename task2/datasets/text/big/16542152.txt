The Brat
 
{{Infobox film
| name           = The Brat
| image          = The Brat.jpg
| caption        = Poster to The Brat (1931)
| director       = John Ford
| producer       = 
| writer         = S. N. Behrman Maude Fulton(play) Sonya Levien
| starring       = Sally ONeil Alan Dinehart
| music          = 
| cinematography = Joseph H. August
| editing        = Alex Troffey Fox Film Corporation
| released       =  
| runtime        = 60 minutes 
| country        = United States 
| language       = English
| budget         = 
}}
 made in 1919 with Alla Nazimova. This 1931 screen version has been updated to then contemporary standards i.e. clothing, speak, topics in the news.

==Background==
Writer Maude Fulton was an actress as well and starred in the 1917 Broadway premiere of her own play. Two of her co-stars in the play went on to have major film careers, Lewis Stone and Edmund Lowe. 

==Cast==
* Sally ONeil as The Brat
* Alan Dinehart as MacMillan Forester
* Frank Albertson as Stephen Forester
* William Collier, Sr. as Judge OFlaherty
* Virginia Cherrill as Angela
* June Collyer as Jane
* J. Farrell MacDonald as Timson, the butler
* Mary Forbes as Mrs. Forester
* Albert Gran as Bishop
* Louise Mackintosh as Lena
* Margaret Mann as Housekeeper

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 