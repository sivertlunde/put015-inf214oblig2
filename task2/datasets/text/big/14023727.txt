The South (film)
{{Infobox film
| name           = The South El Sur
| image          = The South (film).jpg
| caption        =
| producer       = Elías Querejeta
| director       = Víctor Erice
| writer         = Víctor Erice Adelaida García Morales
| starring       = Omero Antonutti Sonsoles Aranguren Icíar Bollaín Aurore Clément
| music          = Enrique Granados
| cinematography = José Luis Alcaine
| editing        = Pablo González del Amo
| released       = 19 May 1983 (Spain) 15 January 1988 (U.S.)
| runtime        = 95 min.
| country        = Spain
| language       = Spanish
}} 1983 drama film, the second film of the perfectionist Spanish auteur Víctor Erice. It was voted the sixth best Spanish film by professionals and critics in 1996 Spanish cinema centenary. It is based on an Adelaida García Morales short novel by the same title. 

Originally, the film would have run 3 hours, but producer Querejeta decided not to allow the filming of the second 90 minutes, which takes place in the south of Spain, because he thought the 95-minute film Erice did was perfect for his purposes. The film was entered into the 1983 Cannes Film Festival.   

==Plot==
This film tells the story of a little girl (Sonsoles Aranguren) living somewhere in the north of Spain and fascinated by the secrets of the south seemingly buried in the traits of her father (Omero Antonutti). In her childhood, Estrellas father is a mysterious world. Growing up, she finds out that he once had a sweetheart (Aurore Clément), and that hes still in love with her.

==Cast==
* Omero Antonutti - Agustín Arenas
* Sonsoles Aranguren - Estrella, 8 years
* Icíar Bollaín - Estrella, 15 years
* Lola Cardona - Julia, Agustíns wife
* Rafaela Aparicio - Milagros
* Aurore Clément - Irene Ríos / Laura
* Francisco Merino - Irene Ríoss Co-Star
* Maria Caro - Casilda
* José Vivó - Grand Hotel barman
* Germaine Montero - Doña Rosario

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 

 