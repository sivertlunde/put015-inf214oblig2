Sin City (film)
 
 
{{Infobox film
| name           = Sin City
| image          = Sincitypostercast.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = {{plainlist| Frank Miller
* Robert Rodriguez
* Quentin Tarantino
}}
| producer       = {{plainlist|
* Elizabeth Avellan
* Frank Miller
* Robert Rodriguez
}}
| writer         = {{plainlist|
* Frank Miller
* Robert Rodriguez
}}
| based on       =  
| starring       = {{plainlist|
* Bruce Willis
* Mickey Rourke
* Clive Owen
* Benicio del Toro
* Jessica Alba
* Brittany Murphy
* Elijah Wood
 
}}
| music          = {{plainlist|
* Robert Rodriguez
* John Debney
* Graeme Revell
}}
| cinematography = Robert Rodriguez
| editing        = Robert Rodriguez
| studio         = {{plainlist|
* Dimension Films
* Troublemaker Studios
}} Miramax Films
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = $40 million 
| gross          = $158.8 million 
}}
 crime Thriller thriller anthology Frank Miller and Robert Rodriguez. It is based on Millers eponymous graphic novel Sin City.   

Much of the film is based on the first, third and fourth books in Millers original comic series. The Hard Goodbye is about a man who embarks on a brutal rampage in search of his one-time sweethearts killer, killing anyone, even the police, that gets in his way of finding and killing her murderer. The Big Fat Kill focuses on an everyman getting caught in a street war between a group of prostitutes and a group of mercenaries, the police and the mob. That Yellow Bastard follows an aging police officer who protects a young woman from a grotesquely disfigured serial killer. The intro and outro of the film are based on the short story "The Customer is Always Right", which is collected in Booze, Broads & Bullets, the sixth book in the comic series.

The film stars Jessica Alba, Benicio del Toro, Brittany Murphy, Clive Owen, Mickey Rourke, Bruce Willis and Elijah Wood, featuring Alexis Bledel, Michael Clarke Duncan, Rosario Dawson, Carla Gugino, Rutger Hauer, Jaime King, Michael Madsen and Nick Stahl, among others.

Sin City opened to wide critical and commercial success, gathering particular recognition for the films unique color processing, which rendered most of the film in black and white but retained or added coloring for selected objects. The film was screened at the 2005 Cannes Film Festival in competition and won the Technical Grand Prize for the films "visual shaping".    

== Plot ==

==="The Customer Is Always Right (Part I)"===
The Salesman (Josh Hartnett) steps out of the elevator walks onto a penthouse balcony where The Customer (Marley Shelton) looks out over Basin City, wearing a beautiful red dress. He offers her a cigarette and says that she looks like someone who is tired of running and that he will save her. She smiles to him, the two share a kiss and he shoots her; she dies in his arms. He says he will never know what she was running from but that he’ll cash her check in the morning, implying she had paid him to kill her.

==="That Yellow Bastard (Part I)"=== Senator Roark (Powers Boothe), who has bribed the police to cover up his sons crimes. Hartigans corrupt partner, Bob (Michael Madsen) tries to convince Hartigan to walk away. Hartigan knocks him out on the docks and goes into pursuit of Roark Jr.
 bad heart, heads into the warehouse where Roark Junior and several henchmen are holding Nancy, and Junior is preparing to rape her. Junior shoots Hartigan in the shoulder and tries to escape. Hartigan catches up and shoots off Juniors ear, hand and genitals. Bob, now recovered, shoots Hartigan in the back, revealing himself to be on Senator Roarks payroll. As the sirens approach, Bob leaves and Nancy lies down in Hartigans lap. Hartigan passes out, reasoning his death is a fair trade for the girls life.

==="The Hard Goodbye"=== Marv (Mickey Goldie (Jaime Frank Miller), who reveals that the Roark family was behind the murder. Marv kills the priest after he insults Goldie but is then attacked by a woman who looks like Goldie, which he dismisses as a hallucination.
 cannibal and that Goldie was a prostitute. He learns that the killers name is Kevin (Elijah Wood) and escapes. Lucille is shot by the leader of a squad of corrupt cops, who are then killed by Marv, except for the leader, whom he interrogates first. Marv finds out that Cardinal Patrick Henry Roark (Rutger Hauer) arranged for Goldies murder.

Marv goes to Old Town, Sin Citys prostitute-run red-light district, to learn more about Goldie and is captured by her twin sister, Goldie and Wendy|Wendy, who had been stalking him and was the attacker Marv previously dismissed as a hallucination. He eventually convinces her that he is not the killer. She and Marv return to the farm where Marv kills Kevin. He confronts Cardinal Roark, who confesses his part in the murders. Kevin was the cardinals ward; the two men ate the prostitutes to consume their souls. Marv kills the cardinal but is then shot and captured by his guards.
 sentenced to death in the electric chair. Wendy visits him on death row and thanks him for avenging her sister. Marv is then set on the electric chair, but the first shock fails to kill him. He mocks the prison guards for it, and the chair is activated again, killing him.

==="The Big Fat Kill"=== abusive ex-boyfriend Jackie Boy (Benicio del Toro) and his cronies, and he breaks into her apartment with his men. However, he doesnt notice that her new lover, Dwight McCarthy (Clive Owen), is also in her flat, and when he goes to the bathroom to urinate, Dwight comes to him from behind and dunks his face into the toilet, full of his own urine, threatening to kill him if he doesnt leave her alone. Distraught, Jackie Boy leaves Shellies flat, and Dwight follows him, not hearing Shellies warnings.
 Miho (Devon Aoki), a martial arts expert, kills Jackie Boy and his friends, slicing Jackie Boys head off. They realize Jackie Boy is actually Detective Lieutenant Jack Rafferty of the Basin City Police, considered a "hero cop" by the press. If the police learn how he died, their truce with the prostitutes would end and the mob would be free to wage war on Old Town.
 IRA mercenaries hired by mob boss Wallenquist Organization#Leadership|Wallenquist. He nearly drowns in the tar before Miho saves him. The mercenary flees to the sewer with Jackie Boys severed head but Dwight and Miho retrieve it and return to Old Town. Meanwhile, mob enforcer Manute (Michael Clarke Duncan), kidnaps Gail. Becky, threatened with the death of her mother by the mob, is forced to betray the prostitutes. Manute prepares the mobs invasion of Old Town. Dwight trades Jackie Boy’s head for Gails freedom but the head is stuffed with explosives; Dwight detonates it, destroying the evidence and Gails captors. The other prostitutes gun down the mercenaries and kill Manute, while Becky, injured in the fight, escapes.

==="That Yellow Bastard (Part II)"=== exotic dancer. As he arrives in the bar, Nancy recognizes him, runs off the stage and forces herself onto him, who rebuffs her attempts.

He realizes he was set up to lead the yellow man to Nancy and the two escape in Nancys car. They arrive at Mimis, a small motel outside of town. In there, Nancy tries to seduce Hartigan, but he again rebuffs her attempts, claiming that he considers her a friend and the significant age difference. The yellow man, who turns out to be Junior, was hiding in the trunk of Nancys car, attacks Hartigan and takes Nancy to the Roark farm to finish what he started eight years before. Hartigan follows and fakes a heart attack, giving him a chance to kill Junior. Knowing that Senator Roark will never stop hunting them, Hartigan commits suicide to ensure Nancys safety. Again, he justifies his life for Nancys as a fair trade.

==="The Customer Is Always Right (Part II)"===
An injured Becky departs from a hospital, talking on a cell phone with her mother. In the elevator she encounters The Salesman, dressed as a doctor. He offers her a cigarette, calling her by name, and she abruptly ends the call with her mother, realizing that The Salesman will kill her.

== Cast ==
 
 
* Jessica Alba as Nancy Callahan
** Makenzie Vega as Young Nancy Callahan Miho
* Becky
* Senator Roark
* Jude Ciccolella as Liebowitz
* Michael Clarke Duncan as Manute Gail
* Benicio del Toro as Jack Rafferty|Det. Lt. Jack "Jackie Boy" Rafferty
* Jason Douglas as Hitman Tommy Flanagan as Brian Klump
* Carla Gugino as Lucille
* Josh Hartnett as The Salesman, known in the screenplay as "The Man" Cardinal Patrick Henry Roark Stuka
* Schutz
* Jaime King as Goldie and Wendy
* Michael Madsen as Bob Frank Miller  as Priest
* Brittany Murphy as Shellie Tammy
* Schlubb
* Clive Owen as Dwight McCarthy Marv
* The Customer Roark Junior / Yellow Bastard Dallas
* Bruce Willis as John Hartigan Kevin
 

== Production ==

=== Proof of concept ===
After his negative personal experience working in Hollywood on RoboCop 2 and RoboCop 3|3, Miller was reluctant to release the film rights to his comic books, fearing a similar result. Rodriguez, a long-time fan of the graphic novels, was eager to adapt Sin City for the screen. His plan was to make a fully faithful adaptation, follow the source material closely, and make a "translation, not an adaptation".  In hopes of convincing Miller to give the project his blessing, Rodriguez shot a "proof of concept" adaptation of the Sin City story "The Customer Is Always Right" (starring Josh Hartnett and Marley Shelton). Rodriguez flew Miller into Austin to be present at this test shooting, and Miller was very happy with the results. This footage was later used as the opening scene for the completed project, and (according to Rodriguez in the DVD extras)  to recruit Bruce Willis and others to the project.

=== Digital backlot ===
  Sony HDC-950 green screen, that allowed for the artificial backgrounds (as well as some major foreground elements, such as cars) to be added later during the post-production stage. Three sets were constructed by hand:
* Kadies Bar, where all of the major characters make an appearance at least once and also the only location in which all objects are in color.
* Shellies apartment. The front door and kitchen are real, while bathroom and corridors are artificial.
* The hospital corridor in the epilogue. Although the first shot of walking feet was done on green screen, the corridor in the next shot is real. The background becomes artificial again when the interior of the elevator is shown.
 ) walking down a street. An example of the films neo-noir atmosphere.]] fully digital, live-action films (since then, digital has grown in popularity). This technique also means that the whole film was initially shot in full color, and was converted to black-and-white.
 Colorization is used on certain subjects in a scene, such as Devon Aokis red-and-blue clothing; Alexis Bledels blue eyes and red blood; Michael Clarke Duncans golden eye; Rutger Hauers green eyes; Jaime Kings red dress and blonde hair; Clive Owens red Converse shoes and Cadillac; Mickey Rourkes red blood and orange prescription pill container; Marley Sheltons green eyes, red dress, and red lips; Nick Stahls yellow face and body; and Elijah Woods white glasses. Much of the blood in the film also has a striking glow to it. The film was color-corrected digitally and, as in film noir tradition, treated for heightened contrast so as to more clearly separate blacks and whites. This was done not only to give a more film-noir look, but also to make it appear more like the original comic. This technique was used again on another Frank Miller adaptation, 300 (film)|300, which was shot on film.

=== Filming ===
 
Principal photography began on March 29, 2004. Several of the scenes were shot before every actor had signed on; as a result, several stand-ins were used before the actual actors were digitally added into the film during post-production.    Rodriguez, an aficionado of cinematic technology, has used similar techniques in the past. In  : "This is the future! You dont wait six hours for a scene to be lighted. You want a light over here, you grab a light and put it over here. You want a nuclear submarine, you make one out of thin air and put your characters into it."   

The film was noted throughout production for Rodriguezs plan to stay faithful to the source material, unlike most other comic book adaptations. Rodriguez stated that he considered the film to be "less of an adaptation than a translation".  As a result, there is no screenwriting in the credits; simply "Based on the graphic novels by Frank Miller". There were several minor changes, such as dialogue trimming, new colorized objects, removal of some nudity, slightly edited violence, and minor deleted scenes. These scenes were later added in the release of the Sin City Collectors DVD, which also split the books into the four separate stories. 

=== Music ===
 
 
The soundtrack was composed by Rodriguez as well as John Debney and Graeme Revell. The films three main stories ("The Hard Goodbye", "The Big Fat Kill", and "That Yellow Bastard") were each scored by an individual composer: Revell scored "Goodbye", Debney scored "Kill", and Rodriguez scored "Bastard". Additionally, Rodriguez co-scored with the other two composers on several tracks.
 The Servant. The song was heavily featured in the films publicity, including the promotional trailers and television spots, as well as being featured on the films DVD menus.

"Sensemayá" by Silvestre Revueltas is also used on the end sequence of "That Yellow Bastard". Fluke (band)|Flukes track "Absurd" is also used when Hartigan first enters Kadies.

=== Credits ===
Three directors received credit for Sin City: Miller, Rodriguez, and Quentin Tarantino, the last for directing the drive to the pits scene in which Dwight talks with a  dead Jackie Boy (Benicio Del Toro). Miller and Rodriguez worked as a team directing the rest of the film. Despite having no previous directorial background, Miller was substantially involved in the films direction, providing direction to the actors on their motivations and what they needed to bring to each scene. Because of this (and the fact that Millers original books were used as storyboards), Rodriguez felt that they should both be credited as directors on the film. 

When the Directors Guild of America refused to allow two directors that were not an established team to be credited (especially since Miller had never directed before), Rodriguez first planned to give Miller full credit. Miller would not accept this. Rodriguez, also refusing to take full credit, decided to resign from the Guild so that the joint credit could remain. 

== Release ==

=== Critical reception   ===
The film opened on April 1, 2005 to generally positive reviews. Film review aggregator  , which assigns a normalised rating out of 100 based on reviews from critics, the film has a score of 74 (citing "generally favorable reviews") based on 40 reviews. 

  placed the film on his list of the "Top Ten" films of 2005.  Several critics including Ebert compared the film favorably to other comic book adaptations, particularly Batman (1989 film)|Batman  and Hulk (film)|Hulk.  Chauncey Mabe of the Sun-Sentinel wrote: "Really, there will be no reason for anyone to make a comic-book film ever again. Miller and Rodriguez have pushed the form as far as it can possibly go." 

Several reviews focused predominately on the films more graphic content, criticizing it for a lack of "humanity". William Arnold of the  ."   

The New York Times critic Manohla Dargis claimed that the directors "commitment to absolute unreality and the absence of the human factor" made it "hard to get pulled into the story on any level other than the visceral". Credit is given for Rodriguezs "scrupulous care and obvious love for its genre influences" but Dargis notes "its a shame the movie is kind of a bore" where the private experience of reading a graphic novel does not translate, stating that "the problem is, this is his private experience, not ours". 

In a more lighthearted piece focusing on the progression of films and the origins of Sin City, fellow Times critic A. O. Scott, identifying Who Framed Roger Rabbit as its chief cinematic predecessor, argued that "Something is missing&nbsp;– something human. Dont let the movies fool you: Roger Rabbit was guilty," with regard to the increasing use of digitisation within films to replace the human elements. He applauds the fact Rodriguez "has rendered a gorgeous world of silvery shadows that updates the expressionist cinematography of postwar noir" but bemoans that several elements of "old film noirs has been digitally broomed away", resulting instead in a film that "offers sensation without feeling, death without grief, sin without guilt, and, ultimately, novelty without surprise". 

=== Box office ===
Sin City grossed $29.1 million on its opening weekend, defeating fellow opener Beauty Shop by more than twice its opening take. The film saw a sharp decline in its second weekend, dropping over fifty percent. Ultimately, the film ended its North American run with a gross of $74.1 million against its $40 million negative cost. Overseas, the film grossed $84.6 million, for a worldwide total from theater receipts of $158.7 million.   

=== Accolades === Irish Film BMI Film & TV Awards. 

Sin City was nominated at the 2006  ,  , Choice Movie Actress: Action/Adventure/Thriller for Jessica Alba and Choice Movie Bad Guy for Elijah Wood.

== Home media   ==
  Region 1 DVD was released on August 16, 2005. The single-disc edition was released with four different slipcovers to choose from and featured a "behind-the-scenes" documentary. Then, on December 13, 2005, the special edition DVD was released, known as the "recut, unrated, extended" edition. On October 21, 2008, a Blu-ray Disc|Blu-ray edition, which is region free, was released by Alliance in Canada. On January 29, 2009 a United States Blu-ray release was confirmed for April 23, 2009. It is a 2-disc edition featuring both the films "theatrical" and "recut, unrated, extended" versions.
 edited and deleted scenes that were missing from the theatrical edition). Bonus material included an audio commentary with director Rodriguez and Miller, a commentary with Rodriguez and Tarantino, and a third commentary featuring the recorded audience reaction at the Austin, Texas Premiere. Also included were various "behind-the-scenes" documentaries and features, as well as a pocket-sized version of the graphic novel The Hard Goodbye. Shortly after, the same DVD/book package was released in a limited edition giftbox with a set of Sin City playing cards and a small stack of Sin City poker chips not available anywhere else.
 Region 2 HMV stores, but is now available at most retailers in the United Kingdom.

== Sequel ==
  Frank Miller directing a script co-written by them and William Monahan.  The film was based mainly on A Dame to Kill For, the second book in the Sin City series by Miller, and also included the short story "Just Another Saturday Night" from the Booze, Broads, & Bullets collection, as well as two original stories written by Miller for the film, titled "The Long Bad Night" and "Nancys Last Dance". Actors Bruce Willis, Mickey Rourke, Rosario Dawson and Jessica Alba all reprised their roles in the sequel, amongst others.

== See also ==
 
* List of films based on crime books

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*  

 

{{Navboxes
|title=Related topics|state=collapsed
|list1= 
 
 
 
 
}}
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 