La Conchiglia
{{Infobox Film
| name           = La Conchiglia
| image          = 
| caption        = 
| director       = Abdulkadir Ahmed Said
| producer       = COE (Italy)/Saint Luca (Italy)/Bufalo Film (Somalia)
| writer         = Abdulkadir Ahmed Said
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1992
| runtime        = 30 min.
| country        =    
| awards         =  Somali  Italian subtitles) 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Somali writer and director Abdulkadir Ahmed Said.

==Plot== ecological catastrophe. One night, a boat discharged illegal toxic waste that poisoned the fish and local fisherman, eventually contaminating all the vegetation on the coast and the regions inhabitants. Sophies story is recounted twelve months after the ecological disaster first struck that strip of land, forever ruining the habitat and claiming many lives, including that of the child herself. The films last sequence nevertheless leaves a glimmer of hope: a group of children are playing near the sea, and a little girl breaks away from the pack to invite the female protagonist to join their game. Association, p.407  Xodo, p.31 

==Other== Italian subtitles. 

The movie was filmed in and features the local residents of Gondershe, a small town on the coast of southern Somalia. 

==Notes==
 

==References==
*{{cite book
  | last = Association des trois mondes
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Cinémas dAfrique
  | publisher = KARTHALA Editions
  | year = 2000
  | location = 
  | pages = 
  | url = http://books.google.ca/books?id=61Ol3PLhYhsC
  | doi = 
  | id = 
  | isbn = 2-84586-060-9}}
*{{cite web
  | last = Xodo
  | first = Chiara
  | authorlink = 
  | coauthors = 
  | title = Catalogo Audiovisivi (con schede didattiche)
  | work = 
  | publisher = Centro Interculturale Millevoci, Provincia Autonoma di Trento Dipartimento Istruzione
  | date = August 2008
  | url = http://www.vivoscuola.it/Intercultu/Archivio/News/COMPLETO-CATALOGO-AUDIOVISIVI.pdf
  | format = 
  | doi = 
  | accessdate = 2009-09-25}}

 
 
 
 
 
 

 