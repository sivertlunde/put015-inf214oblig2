Der Marsch zum Führer
{{Infobox film
| name = Der Marsch zum Führer
| image =
| image_size =
| caption =
| director =
| producer =
| writer =
| narrator =
| starring = Rudolf Hess Adolf Hitler Baldur von Schirach
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 45 minutes
| country = Nazi Germany
| language = German
| budget =
}} Nazi Party Rally. Unlike the earlier Leni Riefenstahl Nuremberg documentaries, it does not focus on the Party congress itself, or on Nazi leaders, who are not shown until the very end of the film. Instead, it follows HJ boys from various parts of Nazi Germany beginning their journey, camping along the route, being taken in by helpful families on the way and marching through cities in formation, saluting and carrying the swastika banner.

The film is quite short compared to earlier Nuremberg films, at approximately 45 minutes.

== References ==
 

== External links ==
* 
*  

 
 
 
 
 

 