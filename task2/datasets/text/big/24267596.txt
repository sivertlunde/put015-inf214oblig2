Underneath the Arches (film)
{{Infobox film
| name           = Underneath the Arches
| image          = 
| image_size     = 
| caption        = 
| director       = Redd Davis
| producer       = Julius Hagen
| writer         = Alison Booth   H. Fowler Mear
| narrator       = 
| starring       = Bud Flanagan Chesney Allen Stella Moya Lyn Harding
| music          = W.L. Trytel
| cinematography = Sydney Blythe
| editing        = 
| studio         = Twickenham Studios
| distributor    = Wardour Films
| released       = February 1937
| runtime        = 72 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British comedy film directed by Redd Davis and starring Bud Flanagan, Chesney Allen, Stella Moya, Lyn Harding and Edmund Willard.  Flanagan and Allen formed part of the comedy ensemble known as the Crazy Gang.  It was made by Julius Hagens Twickenham Studios as part of its ambitious production schedule following its abandonment of quota quickies.

Two down-on-their-luck Englishmen travel by ship to a South American country where they foil one revolution, and then accidentally start another. 

==Cast==
* Bud Flanagan – Bud
* Chesney Allen – Ches
* Stella Moya – Anna
* Lyn Harding – Pedro
* Edmund Willard – Chief Steward
* Enid Stamp-Taylor – Dolores
* Edward Ashley – Carlos
* Aubrey Mather – Professor

==References==
 

==External links==
 

 
 
 
 
 
 
 


 