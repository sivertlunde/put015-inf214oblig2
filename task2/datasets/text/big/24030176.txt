Friends (1993 film)
 
 
{{Infobox film
| name           = Friends
| image          = 
| caption        = 
| director       = Elaine Proctor
| producer       = Judith Hunt
| writer         = Elaine Proctor
| starring       = Kerry Fox
| music          = 
| cinematography = Dominique Chapuis
| editing        = Tony Lawson
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = South Africa
| language       = English
| budget         = 
}}

Friends is a 1993 South African drama film directed by Elaine Proctor. It was entered into the 1993 Cannes Film Festival, where it won the Caméra dOr Special Distinction.   

==Cast==
* Kerry Fox - Sophie
* Dambisa Kente - Thoko
* Michele Burgers - Aninka
* Marius Weyers - Johan
* Tertius Meintjes - Jeremy
* Dolly Rathebe - Innocentia
* Wilma Stockenström - Iris
* Carel Trichardt - Rheinhart
* Anne Curteis - Sophies mother
* Ralph Draper - Sophies father
* Mary Twala - Grace
* Maphiki Mabohi - Daphne
* Job Kubatsi - Maurondile
* Vanessa Cooke - Prisons warden
* Jerry Mofokeng - Thomi
* Trevi Jean Le Pere - Jeremys lover

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 