Hungama
 
 
 
 
{{Infobox film
| name           = Hungama
| image          = Hungama poster.jpg
| caption        = Theatrical release poster
| director       = Priyadarshan
| producer       = Vijay Galani Pooja Galani Ganesh Jain
| story          = Priyadarshan
| screenplay     = Priyadarshan
| starring       = Aftab Shivdasani Akshaye Khanna Paresh Rawal Rimi Sen Rajpal Yadav
| music          = Nadeem-Shravan
| cinematography = Thiru
| editing        = 
| distributor    = Pooja Films
| released       = 1 August 2003
| runtime        = 153 min.
| country        = India
| language       = Hindi
| budget         =
}}
Hungama is a 2003 Bollywood comedy film film co-written and directed by Priyadarshan. It stars Paresh Rawal, Aftab Shivdasani, Akshay Khanna and Rimi Sen. It was an adaptation of Priyadarshans own 1984 film Poochakkoru Mookkuthi; which itself was based on Charles Dickens play, The Strange Gentleman.

==Plot==
 
Anjali (Rimi Sen) has run away from a small village to get a job in Mumbai, where she hopes to make enough money to pay off the debts her family owes their landlord or else she will have to marry the landlords son, Raja (Rajpal Yadav). Once she arrives in the city, the only way she can find cheap lodging is by pretending to be married to a total stranger, Nandu,(Aftab Shivdasani) an aspiring musician, an arrangement thought up by local milkman and fixer, Bholu (Amin Gazi). Anjali and Nandu initially dont get along but must make do to retain their low-rent accommodation. 

Meanwhile, an electronics salesman named Jeetu (Akshaye Khanna) is struggling to find a loan to start his own business. His wealthy dad refuses to loan him. He discusses his situation with his friend Anil (Sanjay Narvekar) who tells him that the best way to access funds is to marry into them, i.e. marry someones rich daughter. He also confides in Jeetu and tells him that by seducing the daughter of a rich person named Kachara Seth (Shakti Kapoor) and marrying her, he too would become rich. He plans to make himself appear rich and expects that, once married, the father-in-law will be able to do nothing upon discovering the truth. Jeetu takes the matter up and ends up stealing a chunk of cash from his parents safe. He then proceeds to fulfill his dream of opening his electronics business.

Meanwhile, millionaire businessman Radeshyam Tiwari (Paresh Rawal), who lives in a village with his wife Anjali Tiwari (Shoma Anand) and retains the mannerisms of a villager despite his riches, decides to spend a few weeks in the city. Tiwari owns a large villa in the city but has never lived there, leaving it occupied by a servant named Pandu. 

Unbeknownst to Tiwari, Anil has been living at his villa for the past couple of weeks, having struck a deal with Pandu. The plan involves pretending to be Tiwaris son and asking Kachara Seth for his daughters hand in marriage. The plan has been working to perfection so far, with money and status-conscious Kachara Seth having visited the villa, having become impressed with Anil, and having decided to betroth his daughter to someone he believes is Tiwaris real son. However, with the news that Tiwari is arriving in town, Pandu, fearful for his own employment, asks Anil to flee and make some excuse to Kachara Seth.

Anjali gets news of Tiwaris arrival from a friend and pays a visit to the villa in the hope of landing a job there. She turns up for an interview, but Tiwaris wife does not like her and slams the door in her face. Desperate for a job, she decides to wait outside the door in the hope of seeing Tiwari when he steps outside by himself. At this point, Jeetu shows up at the villa to fix a stereo he sold to Tiwari. When he sees Anjali at the door, he mistakes her for the daughter of Tiwari and, with the words of Anil in the back of his mind, instinctively starts wooing Anjali. 

Unable to land a job at the Tiwaris, Anjali later bumps into Jeetu when she replies to a help wanted ad in the newspaper for work at Jeetus new store. realizing the golden opportunity such arrangement offers him in being able to woo her daily. Anjali, realizing Jeetus mistake, does not correct him, as she may not land the job if Jeetu finds out that she is not Tiwaris daughter.

Nandu, in the meantime, has begun to fall in love with Anjali. Nandu and Jeetu cross paths resulting in a very public verbal altercation between them. Every day after work, Jeetu drops Anjali off at the Tiwari villa mistakenly believing she lives there as Tiwaris daughter. As soon as he drives off, Anjali turns away from the front door and hastily makes her way out before the Tiwaris spot her. 

With Jeetu often showing up at Tiwaris villa asking for Anjali and Mrs. Tiwari often spotting the real Anjali outside the villa where Jeetu drops her off after work, soon, the Tiwaris become convinced that each is having an affair with the younger counterparts. This results in several acrimonious arguments and much tantrum throwing between the elderly couple. 

Meanwhile, Anjali receives a letter from home, informing her that Raja is coming to visit her. Raja arrives the next day but is intercepted by Nandu at the train station, who teams up with Bholu to formulate a plan to scare the simpleton Raja away from the city before he can get in touch with Anjali. 

In the meantime, Kachara Seth, too, has visited the villa on several occasions. Upon finding that Pandu and Anil missing, he becomes very agitated. Tiwari tries to explain to him that he his only son is in London. Tensions soon boil over between Tiwari and Kachara Seth. 

Now Tiwari is after Jeetu for his perceived dalliances with his wife; Kachara Seth is after Tiwari and his missing son Anil; Jeetu is after Tiwari for Anjalis hand in marriage; and Nandus landlords wife has fallen for Nandu and is after him about eloping with her. All the major characters start chasing after each other and end up in a warehouse, where a big melee follows. 

In the end, Jeetu and Nandu confront Anjali and profess their love for her, forcing her to choose between them. Seemingly cornered into a difficult choice, Anjali proceeds to write Jeetu and Nandus name on several chits, folds them up and asks Jeetu to pick one. She promises to marry the person whose name comes in the chit. When a disappointed Jeetu collapses to the floor in despair and opens the other chits, they all have Nandus name: Anjali had fallen in love with Nandu along the way and the ruse with the chits was a way to make rejecting Jeetu more palatable.

==Cast==
* Paresh Rawal as Radheyshyam Tiwari
* Akshaye Khanna as Jeetu
* Rimi Sen as Anjali
* Aftab Shivdasani as Nandu
* Shakti Kapoor as Raddiwala Tejabhai a.k.a. Kachara Seth
* Shoma Anand as Anjali Tiwari
* Rajpal Yadav as Raja
* Tiku Talsania as Popat Seth (Nandu and Anjalis land lord)
* Upasna Singh as Dulari (Popats wife)
* Sanjay Narvekar as Anil
* Neena Kulkarni as Jeetus mother
* Jagadish as Pandu (Radheysham Tiwaris servant)  Manoj Joshi as Inspector Waghmare
* Razzak Khan as Babu Bisleri
* Amin Gazi as Bholu Shaan as himself in the song Chain Aapko Mila
* Sadhana Sargam as herself in the song Chain Aapko Mila

==Music==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; "
|- bgcolor="#CCCCCC" align="center"
! Title !! Singer(s) 
|-
| "Hum Nahi Tere Dushmano Main" || Abhijeet Bhattacharya|Abhijeet, Sonu Nigam & Alka Yagnik
|-
| "Pari Pari"  || Babul Supriyo
|-
| "Ishq Jab Ek (M)"  || Kumar Sanu
|- Richa Sharma
|- Shaan & Sadhana Sargam
|- Shaan
|-
| "Tera Dil Mere Paas Rehne De" || Udit Narayan & Alka Yagnik
|}

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 