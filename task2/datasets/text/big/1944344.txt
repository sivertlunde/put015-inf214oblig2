Double Platinum
 
{{Infobox film
| name = Double Platinum
| image = Double-platinum-dianaross-brandy.jpg
| image_size = 215px
| alt =
| caption = DVD cover
| director = Robert Allan Ackerman
| producer = Lynn Raynor James Bigwood Craig Zadan Neil Meron Diana Ross Brandy Norwood Sonja Norwood
| writer = Nina Shengold
| starring = Diana Ross Brandy Christine Ebersole Allen Payne Brian Stokes Mitchell Harvey Fierstein
| music = David Shire
| cinematography = Michael Fash
| editing = Scott Vickrey Norwood Entertainment Group ABC
| released =  
| runtime = 94 minutes
| country = United States
| language = English
}} musical film starring Diana Ross and Brandy Norwood|Brandy.

==Plot==
The film tells the story of a woman feeling unfulfilled and unappreciated in her life so she decides to abandon her husband and infant daughter in order to pursue her dreams of superstardom. The story starts off by showing aspiring vocalist, Olivia King (Diana Ross) performing at small lounge in Atlanta, 1981. As she takes a break from her performance she heads backstage to see her infant daughter, Kayla (Jayda Brown). A music executive approaches King at the bar and tells her she is talented and should move to the bigger market of New York City to fulfill her dreams. Olivia King initially denies his request and goes home to her husband ((Brian Stokes Mitchell)). King is shown trying to explain her night to her husband and tell him about the music executive but he brushes her off and tells her moving to New York City is a bad idea. Feeling depressed, conflicted and trapped in her marriage, Olivia King leaves her home in the middle of the night whispering to her daughter that she cant take her along, but will be back for her. The movie then picks back up in St.Louis, Missouri 18 years later and it is shown that Kayla Harris (Brandy Norwood), now 19 years old, has won a contest to meet famous superstar Olivia King. Kayla at this time does not know that Olivia is her mother, but she grew up being a huge fan of Ms. Olivia King and is extremely excited to meet her idol, completely ignorant to the truth. After enjoying the concert, Kayla then gets the second part of her prize which includes having dinner with Olivia King, at dinner Kayla lets Olivia know that she also would like to be a singer and invites Olivia to a small performance she would be doing. When Kayla gets home from dinner, her father asks her how it went and he is shocked to hear that the star Kayla got to meet was Olivia King, though he knows the truth he does not press the issue further as he sees his daughter is still in the dark. Next, Kayla is shown getting ready to perform, and she begins to worry that Olivia will not show up but sure enough Olivia shows up as Kayla starts to sing. Kayla is ecstatic that her idol has shown up but after the show Olivia pulls her aside and reveals to Kayla who she really is. Kayla is stunned and upset, she quickly leaves. Kayla is shaken, she argues with her father and refuses to speak to her mother even when Olivia offers to help her with her career. Kaylas best friend advises her to accept Olivias help with her music career as it is the least she owes her.

Kayla reluctantly agrees to go to New York with Olivia all the while Olivia continues to make futile attempts to build a relationship with Kayla. Olivia introduces Kayla to the people she knows in the industry, and with Olivias strong recommendation and Kaylas talent, she is quickly signed. Kayla begins to work on her album and with Olivias guidance and leadership she records her first song. As the story progresses, the two ladies continue living in Olivias fancy penthouse apartment with Olivia trying not to step on Kaylas toes as she pursues success. Kayla then finds a steamy romance with a handsome older music executive, Ric Ortega (Allen Payne) whom her mother does not trust and warns her against but Kayla ignores her mothers warning. Despite her growing success and hit single, Kayla continues to harbor bitter feelings towards Olivia, feelings which boil over during their record labels Grammy Party. After Kaylas performance, Olivia is asked to perform by the head of the label, causing Kayla to resent her mother for "stealing her spotlight". However, after discovering that Ric revealed her true parentage to the press and betrayed her just as her mother had warned, Kayla dissolves her relationship with him. Kayla feeling down and regretful heads up to her mothers cabin where Olivia is finding refuge. As they spend time at the cabin and truly communicate the two women begin to understand each other. Eventually Kaylas, feelings about her mother change, and she begins to accept the truth about her mother and the events that took place when she was younger. The movie culminates with Olivia and Kayla going back to St.Louis for a concert Kayla is giving. Kayla sings her songs but as the show draws to an end she calls on her mother to come up on stage with her and they sing a wonderful duet together. Kaylas friend and family look on from the audience. 

The movie was a Nielsen ratings success, debuting at #16 for the week. Along with airing original on ABC, it was also syndicated by VH1, BET, Centric and TVONE where it still is a recurrent favorite.

==Cast==

* Lamar B  as boo brazy
* Diana Ross as Olivia King Brandy as Kayla Harris
* Christine Ebersole as Peggy
* Allen Payne as Ric Ortega
* Brian Stokes Mitchell as Adam Harris
* Harvey Fierstein as Gary Millstein
* Roger Rees as Marc Reckler
* Samantha Brown as Royana
* Ed Lover as Party Ardie
* Peter Francis James as Martin Holly
* Adriane Lenox
* Bernard Addison
* Debbie Matenopoulos as Herself

==Soundtrack==
The songs featured in the film are found on Brandys hit 1998 album,  also used., which was released two weeks prior to the films premiere. "Love is All that Matters" was an original song performed by Brandy and Ross specifically for the film.

==References==
 
http://www.allmovie.com/movie/double-platinum-v179897/cast-crew
https://www.youtube.com/watch?v=4hpeyGrwdsw
http://grooveshark.com/#!/album/Double+Platinum+Soundtrack/4456250
https://itunes.apple.com/ca/album/never-say-never/id20913004

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 