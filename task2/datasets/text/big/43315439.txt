The Mikado (1967 film)
{{Infobox film
| name           = The Mikado
| image          = TheMikado1967.jpg
| alt            = 
| caption        = Promotional poster
| director       = Stuart Burge
| producer       = John Brabourne Anthony Havelock-Allan Richard B. Goodwin
| based on       =   John Reed Kenneth Sandford Donald Adams Philip Potter
| music          = Arthur Sullivan
| cinematography = Gerry Fisher
| editing        = Alma Godfrey
| studio         = BHE Films
| distributor    = Warner Bros. (United States)
| released       =  
| runtime        = 122 minutes
| country        = England
| language       = English
}} comic opera of the same name.  The film was directed by Stuart Burge and was a slightly cut adaptation of the DOyly Carte Opera Companys production of The Mikado and used all DOyly Carte singers.

==Cast==
  John Reed (right) as Ko-Ko]] John Reed as Ko-Ko
* Kenneth Sandford as Pooh-Bah
* Donald Adams as the Mikado
* Valerie Masterson as Yum-Yum
* Philip Potter as Nanki-Poo
* Christene Palmer as Katisha
* Peggy Ann Jones as Pitti-Sing Thomas Lawlor as Pish-Tush
* Pauline Wales as Peep-Bo George Cook as Go-To

==Production== Uncle Vanya (1963) and the Laurence Olivier version of Othello (1965 film)|Othello (1965). The direction of the film closely reflects traditional DOyly Carte staging of the time by Anthony Besch, although there are some cuts. Shepherd, Marc.  , the Gilbert and Sullivan Discography, 15 April 2009, accessed 16 July 2014 
 John Reed, Peter Howitt, with costumes by Jones.  

==Release==
The Mikado was released in the United States on 15 March 1967. The UK title was The Mikado: The Town of Titipu. 

==Reception==
The New York Times criticized the filming technique and the orchestra and noted, "Knowing how fine this cast can be in its proper medium, one regrets the impression this Mikado will make on those not fortunate enough to have watched the company in the flesh.  The cameras have captured everything about the companys acting except its magic."  A reviewer of the video commented: "the performance is extremely flat. One senses that the cast, lacking a live audience to interact with, are merely going through the motions." 

==See also==
*The Mikado (1939 film)

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 