Cafe X
{{Infobox film
| name           = Cafe X
| image          = 
| image size     =
| caption        = 
| director       = Walter Fyrst
| producer       = 
| writer         =  Walter Fyrst
| narrator       =
| starring       = Bengt Djurberg   Tove Tellback 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 29 October 1928
| runtime        = 100 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian crime film directed by Walter Fyrst, starring Bengt Djurberg and Tove Tellback. It tells the story of journalist Karl Kraft (Djurberg) who uncovers a major weapon smuggling scheme going on in Oslo. Along the way he meets the waitress Lilly (Tellback), who is involved in the affair. He convinces her to abandon the plot, and the two end up together.

==External links==
*  

 
 
 
 