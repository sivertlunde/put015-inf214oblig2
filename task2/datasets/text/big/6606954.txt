Doomed to Die
{{Infobox film
| name           = Doomed to Die
| image          = Doomeddie.jpg
| image size     = 190px
| caption        = Promotional film poster
| director       = William Nigh
| producer       = Paul Malvern Scott R. Dunlap
| writer         = Hugh Wiley Ralph Gilbert Bettison Michael Jacoby
| starring       = Boris Karloff Marjorie Reynolds Grant Withers
| music          = Edward J. Kay
| cinematography = Harry Neumann
| editing        = Robert Golden
| distributor    = Monogram Pictures Corporation
| released       =  
| runtime        = 68 min
| country        = United States
| awards         =
| language       = English
| budget         =
}} The Fatal Hour. Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 259-260 

==Cast==
* Boris Karloff -  	James Lee Wong
* Marjorie Reynolds - Roberta Bobbie Logan
* Grant Withers - Capt. William Bill Street (Homicide Squad)
* William Stelling - Dick Fleming
* Catherine Craig - Cynthia Wentworth
* Guy Usher - Paul Fleming (Dicks father) Henry Brandon - Victor Vic Martin (attorney)
* Melvin Lang - Cyrus P. Wentworth
* Wilbur Mack - Matthews (Wentworths assistant)
* Kenneth Harlan - Ludlow (chauffeur)
* Richard Loo - Tong leader

==Production==
Filming began in mid June. 
The film uses actual news footage from the burning of the liner SS Morro Castle, who caught fire on September 8 1934 during a trip from Havana to New York. 
 
==References==
 
==External links==
*  
*  
*  
*  
*    at Google Videos

 
 

 
 
 
 
 
 
 
 
 
 
 
 

 