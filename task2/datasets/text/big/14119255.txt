Green Lantern (film)
 
 
{{Infobox film
| name           = Green Lantern
| image          = Green Lantern poster.jpg
| caption        = Theatrical release poster
| director       = Martin Campbell
| producer       = {{Plainlist|
* Donald De Line
* Greg Berlanti }}
| screenplay     = {{Plainlist|
* Greg Berlanti Michael Green
* Marc Guggenheim
* Michael Goldenberg }}
| story          = {{Plainlist|
* Greg Berlanti
* Michael Green
* Marc Guggenheim }} Green Lantern John Broome Gil Kane
| starring       = {{Plainlist|  
* Ryan Reynolds
* Blake Lively
* Peter Sarsgaard
* Mark Strong
* Angela Bassett
* Tim Robbins
* Temuera Morrison
* Geoffrey Rush
* Michael Clarke Duncan }}
| cinematography = Dion Beebe
| editing        = Stuart Baird
| music          = James Newton Howard
| studio         = {{Plainlist|
* DC Entertainment
* De Line Pictures }}
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 114 minutes (Theatrical) 123 minutes (Extended)
| country        = United States
| language       = English
| budget         = $200 million    
| gross          = $219.9 million  
}}
 character of Michael Green ring that grants him superpowers and must confront the evil Parallax (comics)|Parallax, who threatens to upset the balance of power in the universe.
 3D in post-production. 

Green Lantern was released on June 17, 2011 and received generally negative reviews; most criticised the film for its screenplay, deviations from the comics, choice of villains, and overuse of CGI while some praised Reynolds performance despite considering the character to be unlikeable. The film underperformed at the box office, grossing just over $219 million against a budget of $200 million. Due to the films failure, Warner Bros. canceled any plans for sequels. A reboot of the character is scheduled to appear with a new solo film in 2020.

==Plot==
  Parallax and imprisoned him in the Lost Sector on the desolate planet Ryut. In the present day, Parallax escapes from his prison after becoming strengthened by an encounter with crash survivors on the planet. Parallax then pursues and nearly kills Abin Sur, who escapes and crash-lands on Earth where he commands his ring to find a worthy successor.
 the oath. At home he says the oath and is later whisked away to the Green Lantern Corps home planet of Oa, where he meets and trains with Tomar-Re and Kilowog and Corps leader Sinestro, who believes he is unfit and fearful. Jordan quits and returns to Earth, keeping the power ring and lantern.

Meanwhile, scientist Hector Hammond is summoned by his father, Senator Robert Hammond, to a secret government facility to perform an autopsy on Abin Surs body. A piece of Parallax inside the corpse enters Hammond, giving him telepathic and telekinetic powers, at the cost of his sanity. After discovering that he was chosen for the secret work only due to his fathers influence, Hammond attempts to kill his father by telekinetically sabotaging his helicopter at a party. Jordan saves the senator and the party guests, including his childhood sweetheart Carol Ferris. Later, Hammond successfully kills his father by burning him alive, and Jordan learns of Parallax coming to Earth.

Back on Oa, the Guardians tell Sinestro that Parallax was once one of their own until he desired to control the yellow essence of fear, only to become the embodiment of fear itself. Arguing that the way to fight fear is by fear itself, Sinestro requests that the Guardians forge a ring of the same yellow power, preparing to concede Earths destruction to Parallax in order to protect Oa. Jordan appears and tries to convince the Guardians that fear will turn the users evil if its power is used, but they reject his pleas, and he returns to Earth to try to defeat Parallax alone feeling defeated.

Upon returning to Earth, Jordan saves Ferris from Hammond. Parallax arrives, consumes Hammonds life force, and then wreaks havoc on Coast City. With new-found strength, Jordan lures Parallax away from Earth and toward the Sun, destroying and killing Parallax. He loses consciousness after the battle and falls toward the sun, but is saved by Sinestro, Kilowog, and Tomar-Re. Later the entire Green Lantern Corps congratulates him for his bravery. Sinestro tells Jordan he now bears the responsibility of protecting his sector as a Green Lantern.

In the epilogue, Sinestro steals the yellow ring and places it on his finger, causing his green suit and eyes to change to yellow.

==Cast==
* Ryan Reynolds as Hal Jordan:
: A test pilot for the Ferris Aircraft Company who becomes a Green Lantern and the first earthman ever inducted into the Green Lantern Corps.  Reynolds said, "Ive known about Green Lantern my whole life, but Ive never really followed it before. I fell in love with the character when I met with Martin Campbell".  Reynolds called the film "an origin story to a certain degree, but its not a labored origin story, where the movie begins in the third act. The movie starts when it starts. We find out Hal is the guy fairly early on, and the adventure begins".  Alternatively Chris Pine  and Sam Worthington  had been in discussions for the role. Bradley Cooper, Jared Leto and Justin Timberlake were other top contenders,  while Brian Austin Green, a Green Lantern fan, campaigned for the part, but ultimately did not audition. 

* Blake Lively as Carol Ferris: Casino Royale, The Bourne Ultimatum, Quantum of Solace), gymnastic acrobats from Cirque du Soleil and used aerial stunt rigs created for The Matrix, Lively explained, "Our director likes it real—the fights close and dirty... Im 40 feet in the air, spiraling around. Thats the best workout you can ever do because its all core... You do that for ten minutes and you should see your body the next day! Its so exhilarating, so thrilling—and nauseating". 

* Peter Sarsgaard as Hector Hammond: Tulane that was I think just the most eccentric guy they could find. He was entertaining, and he and I actually worked on my lecture that I give in  ." About his character Sarsgaard remarked, "Hes got shades of gray. Its eccentricity on top of eccentricity". 

* Mark Strong as Sinestro:
: A Green Lantern and Hal Jordans mentor.  Strong affirmed that the film will follow the origin story, "the film closely follows the early comics. Sinestro starts out as Hal Jordan’s mentor, slightly suspicious and not sure of him because obviously Hal is the first human being who’s made into a Green Lantern. Hes certainly very strict and certainly unsure of the wisdom of Hal becoming a Green Lantern". Strong said the character "is a military guy but isnt immediately bad. Its the kind of person he is that lends himself to becoming bad over the course of the comics being written, but initially he’s quite a heroic figure.” He also revealed that the outfit and other aspects of the character very closely follow the characters early days, “That widows peak and thin mustache was for some reason originally based on David Niven.... So I would like to do justice to the Sinestro that was conceived for the comic books”. 

 .]]
* Angela Bassett as Amanda Waller|Dr. Amanda Waller:
: A former congressional aide and government agent.  About the differences between the comic book and film character Bassett said, "Well, I’m not 300lbs," but added that her character does have "that intellectual, that bright, that no-nonsense, that means business  .   getting it done and in the trenches nothing fazes her". 

* Tim Robbins as Robert Hammond:
: A United States senator and the father of the movies human villain, Hector Hammond. 

* Temuera Morrison as Abin Sur:
: A Green Lantern who crash lands on Earth and recruits Hal Jordan as his replacement.  Morrison said it took four to five hours to put on the prosthetic makeup for the character. About filming with Ryan Reynolds, Morrison commented, "We did the whole scene together where I give him the ring, our suits are CGI so we had these grey suits with things on them so it was cool and working with Martin Campbell again was great too". 

* Taika Waititi as Thomas Kalmaku: Maori father  – says the production "had an opening for a role in the film for someone who wasnt, I dont  , not-white or not-black." 

* Geoffrey Rush as the voice of Tomar-Re: Queen Elizabeth King George VI in The Kings Speech. But I can’t imagine Tomar Re setting up an office on Harley Street in London. They’re all very different people to me, but there is a kind of theme I suppose". 

* Michael Clarke Duncan as the voice of Kilowog: drill sergeant trainer of new recruits for the Green Lantern Corps.  About the character, Duncan, a fan of the comic book, stated, "Hes a real type of tough guy who knows everything, and actually in one of the comic books he and Superman fought to a tie". 

* Clancy Brown as the voice of Parallax (comics)|Parallax: Guardian of the Universe who was imprisoned by Abin Sur after he was exposed to the yellow energy of fear.
 Mike Doyle is cast as Jack Jordan, Hal Jordans older brother.     Gattlin Griffith, Jeena Craig and Kennon Kepper play Hal Jordan, Carol Ferris and Hector Hammond respectively as children.

==Production==

===Development=== Abin  Silver Age, Modern Age, you have to answer these things." 

===Pre-production===
By December 2008, the writers had written three drafts of the screenplay and Warner Bros. was preparing for pre-production.  However, Berlanti was forced to vacate the directors position when Warner Bros. attached him to This Is Where I Leave You, and in February 2009, Martin Campbell entered negotiations to direct.  The release date was set as December 2010, before being moved to June 17, 2011. 

 ,    was in negotiations to portray   was in negotiations to play   joined the cast as Senator Hammond.  .  .  The following month, New Zealanders Temuera Morrison and Taika Waititi had joined the cast as Abin Sur and Tom Kalmaku, respectively.  .  . 

===Filming===
{{multiple image
 | align = right
 | direction = vertical
 | header =
 | header_align = left/right/center
 | header_background =
 | footer =
 | footer_background =
 | width = 200
 | image1 = Coastal City School Bus crop.JPG
 | width1 = Carrollton section of New Orleans, July 2010.
 | alt1 =
 | image2 = Lakefront Airport NOLA May 2010 1.JPG
 | width2 =
 | caption2 = New Orleans Lakefront Airports Art Deco Terminal Building used as set for Ferris Aircraft headquarters, May 2010.
 | alt2 =
}}
 Madisonville involving stunt cars.  Principal photography began on March 15, 2010 in New Orleans.  Nine days after filming began, Angela Bassett joined the cast as Amanda Waller|Dr. Amanda Waller, a government agent who is a staple of the DC Comics universe.   
 Mike Doyle has been cast for the role of Jack Jordan, the older brother of Hal Jordan. In July 2010 it was reported that Ryan Reynolds was injured while shooting scenes for the film, separating his shoulder and was in "lots of pain". 

===Post-production=== power rings Campbell stated; "One of the nice things is, well all sit down and say, Well, what are we going to do here? Really, its as much as your imagination can go to make the constructs".  The studio also confirmed to MTV News that the film would have a 3-D film|3-D release. 

In January 2011 it was reported that Green Lantern had begun re-shoots for key scenes at Warner Bros. Studios in Los Angeles, California.  In March 2011 it was reported that Geoffrey Rush had joined the cast as the voice of the Computer-generated imagery|CGI-created character, Tomar-Re.   

In April 2011, Michael Clarke Duncan entered negotiations to voice Kilowog.  .  .  Also in April it was reported that Warner Bros. raised the visual effects budget by $9 million and hired additional visual effects studios to bolster the ranks of the team that had been working overtime to meet the films June 17 launch. 

==Soundtrack==
 
 The Dark Knight with Hans Zimmer. The soundtrack was published by WaterTower Music. 

==Release==
The world premiere of Green Lantern took place on June 15, 2011 at Graumans Chinese Theatre in Hollywood|Hollywood, California,  and two days later the film was released in North America and the UK. 

===Home media===
Green Lantern was released on DVD and Blu-ray on October 14, 2011. The Extended Cut adds an extra nine minutes of footage to the running time.  

===Marketing=== San Diego Comic-Con. The footage was widely released online in November 2010 with thirty seconds of footage airing the following day on Entertainment Tonight.  The first full theatrical trailer for the film was shown before screenings of Harry Potter and the Deathly Hallows - Part 1  and became available online in November 2010.  This initial trailer was met with a poor reception from fans and, as a result, the films marketing campaign was delayed. Sue Kroll, the studios worldwide marketing president stated, "Part of the reason the response to the first trailer was lukewarm was that the big-scale sequences werent ready to show, and we suffered for it. We cant afford to do that again."  In April Warner Bros. debuted nine minutes of footage at the 2011 WonderCon in San Francisco. The Hollywood Reporter reported that the footage wowed the audience. A four-minute cut of the WonderCon footage was later released online. 

====Animation====
In March 2010 Comics Continuum reported that an animated Green Lantern film was in the works at   had been discussed but cancelled because of the picture not achieving the immediate success that they had hoped for. However, Timm did hope the live-action film would renew interest in a sequel.  The animated movie entitled   was officially announced in June 2010 instead. 

====Comics====
DC Entertainment began releasing a series of Green Lantern Movie Prequel comics the week before the film was released, covering the lives of the characters before the events of the film, written by members of the films production team. Five comics were made, covering Tomar-Re by Marc Guggenheim, Kilowog by Peter J. Tomasi, Abin Sur by Michael Green, Hal Jordan by Greg Berlanti and Sinestro by Michael Goldenberg and Geoff Johns. A free excerpt of the Sinestro prequel comic was released online as "Secret Origin of the Green Lantern Corps #1" two days before the release of the film. 

====Roller coaster====
  in 2011 to coincide with the films release.  A third ride, Green Lantern Coaster, also opened in November of the same year at Warner Bros. Movie World in Australia. Although this ride was more based on the original comic.

====Video game====
 , for the PlayStation 3 and Xbox 360 by Double Helix Games. Nintendo Wii, Nintendo DS and Nintendo 3DS by Griptonite Games. 

==Reception==

===Box office===
Green Lantern opened on Friday June 17, 2011 in North America, earning $3.4 million in 1,180 midnight runs.  The film went on to gross $21.6 million its opening day, but fell 22% on Saturday for a weekend total of $53.1 million, earning it the No. 1 spot.  In its second weekend Green Lantern experienced a 66.1% decline, which was the largest second weekend decline for a superhero film in 2011.  According to the box office data and analysis website Box Office Mojo, Green Lantern grossed $116,601,172 in the U.S. and Canada as well as $103,250,000 internationally bringing its worldwide total to $219,851,172.

Many industry analysts felt that Green Lantern "failed to perform to expectations".    The Hollywood Reporter speculated that Green Lantern needed to make approximately $500 million to be considered financially solid. 

===Critical reaction===
Green Lantern received negative reviews from critics and has a "rotten" score of 26% on Rotten Tomatoes based on 227 reviews with an average rating of 4.6 out of 10. The critical consensus states "Noisy, overproduced, and thinly written, Green Lantern squanders an impressive budget and decades of comics mythology".    The film also has a score of 39 out of 100 on Metacritic based on 39 critics indicating "Generally unfavorable reviews." 
 The Telegraph named The Green Lantern one of the ten worst films of 2011. 

Conversely, Todd McCarthy of The Hollywood Reporter gave it a positive review, saying the film " erves up all the requisite elements with enough self-deprecating humor to suggest it doesnt take itself too seriously".  Reviewer Leonard Maltin felt that "the film offers a dazzling array of visual effects, a likable hero, a beautiful leading lady, a colorful villain, and a good backstory. It also doesn’t take itself too seriously."  Kenneth Turan of the Los Angeles Times said, "More science-fiction space opera than superhero epic, it works in fits and starts as its disparate parts go in and out of effectiveness, but the professionalism of the production make it watchable in a comic book kind of way". 

===Accolades===
{| class="wikitable"
|-
!Award !! Category !! Winner/Nominee !! Result
|- 2010 Scream Awards  Most Anticipated Movie
|
| 
|- 2011 Teen Choice Awards  Teen Choice Choice Movie Actor - Sci-Fi/Fantasy Ryan Reynolds
| 
|- Choice Movie Actress - Sci-Fi/Fantasy Blake Lively
| 
|- 2011 Scream Awards  Best Superhero Ryan Reynolds as the Green Lantern
| 
|- 38th Peoples Choice Awards 
| Favorite Movie Actor Ryan Reynolds (along with The Change-Up)
| 
|- Favorite Superhero Ryan Reynolds
| 
|-
| Favorite Action Movie Actor Ryan Reynolds
| 
|}

==Reboot==
Warner Bros. originally planned on Green Lantern being the first entry of a new DC franchise, and commissioned a script for a sequel from Greg Berlanti, Michael Green and Marc Guggenheim during filming.  In August 2010, they retained Michael Goldenberg to write the screenplay, based on the sequel treatment.  A scene in the films end credits implying a resurgence of the yellow power of "fear" strongly suggested a planned sequel.  .  .  In September 2011, Warner Bros., dismayed by the films negative reviews and disappointing box office return, abandoned plans for sequels.  
 Man of Steel is the first film of the DC Comics shared universe films by stating that "from   onward, possible films could expand into a shared universe", confirming that should the Green Lantern property be featured in an upcoming DC film, it will be a rebooted version.  Goyer also said to have interest in taking the new version of the character to film.  Warner Bros. plans on releasing a solo Green Lantern film in 2020 as the eleventh installment of their DC Comics shared film universe. Although currently unknown which version of the character will be introduced, it has been speculated that John Stewart will be the Green Lantern for the upcoming reboot and Justice League films. 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 