The Hard Way (1991 film)
{{Infobox film
| name           = The Hard Way
| image          = The_hard_way_poster.jpg
| caption        = Theatrical release poster
| director       = John Badham
| producer       = Rob Cohen William Sackheim
| screenplay     = Daniel Pyne Lem Dobbs
| story          = Lem Dobbs Michael Kozoll
| starring       = Michael J. Fox James Woods 
| music          = Arthur B. Rubinstein
| cinematography = Donald McAlpine Robert Primes
| editing        = Tony Lombardo Frank Morriss
| studio         = The Badham/Cohen Group
| distributor    = Universal Pictures
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = $24 million
| gross          = $65,595,485
}} action thriller film directed by John Badham, and starring Michael J. Fox and James Woods. Stephen Lang, Annabella Sciorra, Luis Guzmán, LL Cool J, Delroy Lindo, Christina Ricci, Mos Def, Kathy Najimy, Michael Badalucco, and Lewis Black appear in supporting roles.

Fox and Woods would later co-star again in 2002s Stuart Little 2, only this time around as the hero and the villain, respectively.

==Plot== NYPD cop media members and cameras.
 pulp Action action films (similar to Indiana Jones). In order to be taken more seriously as an actor, he is vying for the leading role in the heavy cop drama Blood on the Asphalt. Nick vows to "prepare" for the role by attempting to be an actual police officer|cop. After seeing Mosss outburst on TV, Nick pulls strings with NYC Mayor David Dinkins to be assigned as Mosss new partner on the force. Moss wants no part of the deal, but is forced to comply by his captain (Delroy Lindo), who just happens to be a huge Nick Lang fan. To make matters worse, babysitting Nick means that Moss will have to be removed from the Party Crasher case.

Moss defies orders by continuing the investigation anyway and repeatedly trying to ditch Nick, whose constant questions and other annoying habits (such as mimicking all Mosss movements) nearly drive Moss insane. Nick wants to know what it feels like to be a cop and is quickly thrust forward into some serious situations he is not prepared for, while Moss constantly reminds him that this is not a movie. Meanwhile, as Moss makes progress on finding the Party Crasher, he is also trying to juggle a new romance with Susan (Annabella Sciorra).  The divorced Moss is unable to communicate with her or open up, and Nick offers advice to him on how to interact with women.

Moss takes Nick to a dark building to catch a perp, ordering him to stay put and giving the actor a real gun in case of an emergency. Nick, however, does not stay put and enters the building, shooting a man who he believes is a criminal chasing Moss. The man is just an innocent bystander, though, and Nick is horrified. Moss agrees to cover up the act, and urges Nick to leave town immediately. Nick returns from the airport to the police station to confess his sins, only to see that the "dead man" is actually a fellow cop, alive and well. Moss engineered the whole stunt to get Nick out of town.

Nick tracks Moss down to seek revenge but ends up stumbling on a confrontation between Moss and The Party Crasher, during which he saves Mosss life.  It is also revealed, during the fight, that the only reason why The Party Crasher kills is because all his life he wanted to become a cop, but only believed that hed be better than Moss.  The Party Crasher is wounded, but he kills several people and escapes.  After Moss is told by Susan that his unstable life as a cop will never allow them to have a relationship, he is unwelcomely visited by Nick. Nick predicts that The Party Crasher will follow storytelling protocol and seek out Mosss loved ones in this, the third act of their story together. Nick is right, and Susan is abducted. Moss and Nick confront The Party Crasher on a rooftop and battle it out. Susan is saved after Nick intervenes and is shot in the chest.  Shocked with anger, Moss then finally throws the Party Crasher off the rooftop to his death.

The movie cuts forward in time, revealing that Nick survived and already filmed The Good, The Badge and The Ugly. Moss, reunited with and married to Susan, attends the movies premiere with the rest of the department and comments that all Nicks lines were originally his.

==Cast==
 
*Michael J. Fox ...  Nick Lang/Ray Casanov
*James Woods ...  Detective Lt. John Moss, NYPD
*Stephen Lang ...  Party Crasher
*Annabella Sciorra ...  Susan
*John Capodice ...  Detective Grainy, NYPD
*Delroy Lindo ...  Captain Brix, NYPD
*Luis Guzmán ...  Detective Benny Pooley, NYPD
*LL Cool J ...  Detective Billy, NYPD
*Mary Mara ...  Detective China, NYPD
*Bruce Goldberg ...  Taxi Cab Driver
*Penny Marshall ...  Angie, Nicks agent
*Christina Ricci ...  Bonnie, Susans daughter George Kee Cheung ...  Drug Dealer
*Lewis Black ...  Obnoxious Banker that John faces off with in a bar
*Rand Foerster (co-founder of The West Bank Cafe Downstairs Theatre Bar with Lewis Black) ...  Obnoxious Banker
*Kathy Najimy ...  Nicks Girl Friday
*Mos Def ...  Dead Romeos Gang Member (as Dante Smith)
*Bill Cobbs ...  Raggedy Man who accosts Nick outside of the Dead Romeos pad
*Leif Riddell ... Head mugger
*Johnny Sanchez ...  Mugger
*Karen Lynn Gorney ...  Woman in Subway
*Holly Kuespert (wife of director D.J. Caruso) ...  Nicks co-star in "Smoking Gunn"
*Michael Badalucco ...  Pizza Man
*Fracaswell Hyman (creator of The Famous Jett Jackson) ...  Wino
*Bryant Gumbel ...  Himself
*Jan Speck (model and wife of the films director, John Badham) ...  Frank, TV reporter
*David Sosna ...  Billy the Frog Dog Vendor
 

==Reception==
 average score Time Out called it a "light, bright comedy" that "counterbalances Hollywood convention with some very funny swipes at the film industry" and stated, "Badham handles the numerous action sequences with confidence, but the real enjoyment comes from the interplay between the two leads, who revel in the opportunity to send up their images."  Roger Ebert of the Chicago Sun-Times gave it three-and-a-half out of four stars and praised its "comic energy", calling the film "funny, fun, exciting, and   an example of professionals who know their crafts and enjoy doing them well."    Ebert found the stunts, special effects, and second unit work "all seamless and exciting", and viewed that the actors elevate the films plot with their performances:

 

However, Ty Burr of Entertainment Weekly gave the film a C rating and criticized its "coyly self-conscious high concept", writing that it "takes the   to such a numbing dead end."  Burr panned its chase scenes and editing as "visual nonsense" and called its plot "all guns and gag lines", although he found Fox "secure enough to goof on his own image and inventive enough to do it well".   

===Box office===
 The Silence of the Lambs and New Jack City.  The Hard Way went on to earn $65.6 million worldwide.

==Soundtrack==
  
{{Infobox album Name = The Hard Way: Original Motion Picture Soundtrack  Type = Film score Artist =  Arthur Rubenstein  Longtype = Digital download / Audio CD) Cover = The Hard Way (Original Motion Picture Soundtrack).jpg Released = April 2nd, 1991 Length = Label = Varese Sarabande Reviews =
}}
;Tracklist:
#  The Big Apple Juice   
#  Cirque Du Parte Crasher   
#  Manhattan Tow Truck   
#  Ghetto A La Hollyweird   
#  He Said/She Dead   
#  Big Girls Dont Cry   
#  Where Have You Gone   
#  Transit Authority   
#  Gas Attack   
#  Killer Lang   
#  Smoking Gun II   
#  Top of the World   
#  The Good, the Badge and the Ugly   
#  Run Around Sue  

==References==
 

==External links==
 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 