7 Dwarves – Men Alone in the Wood
 
 
{{Infobox film
| name           = 7 Dwarves – Men Alone in the Wood
| image          = 7Dwarves2004Poster.jpg
| alt            =  
| caption        = German theatrical poster
| director       = Sven Unterwaldt Jr.
| producer       = Bernd Eilert, Andreas Grosch, Andreas Schmid, Otto Waalkes, Douglas Welbat
| writer         = Bernd Eilert, Sven Unterwaldt Jr., Otto Waalkes (novel)
| screenplay     = 
| story          = 
| based on       =  
| starring       = Otto Waalkes Heinz Hoenig Mirco Nontschew Boris Aljinovic
| music          = Joja Wendt
| cinematography = Jo Heim
| editing        = Julia von Frihling
| studio         = 
| distributor    = 
| released       =  
| runtime        = 91 minutes (PAL-DVD version)
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}} Snow White and the Seven Dwarves by the Brothers Grimm.

The film, created by the NRW Film Fund (Filmstiftung NRW), was the second most popular film in German cinemas in 2004, reaching an audience of almost 7 million. It also counts as the 4th most successful German film ever in Germany since the audience payment cap. 

== Plot ==
The land of the queen is divided in two parts. At one side, there is a residential area which is overwhelmed by the mighty castle. On the other side is a dark forest named Unterwaldt. Both parts are only connected with a little bridge over the river. One day Little Red Riding Hood is picking flowers at the border of the residential area. She sees a beautiful flower across the bridge and decides to enter Unterwaldt. The forest takes Little Red Riding Hood in its power and the girl forgets time. She decides to swim in the river, but suddenly a shark-fin appears. However, it is the hat of dwarf Bubi who spies on the woman. Bubi is attacked by a bear. A scared Little Red Riding Hood runs further into the forest. The bear takes Bubi to the cottage of the dwarves. It is revealed the bear is the disguised dwarf Tschakko. As Bubi looked at a woman, which is forbidden by the dwarves, he is punished. Squirrels tickle his feet until Bubi cant stop laughing.

The dwarves consider women to be calamitous troublemakers and have a meeting about Bubis incident. It turns out Bubis parents were bank robbers who escaped by air balloon. As the balloon had a hole, his mother had to throw over some weight. She decided to throw out Bubi and to keep the money. Brummboss tries to tell his story once again, but never gets further than: "I was a happy and satisfied man until that night which changed my life". The meeting is interrupted by "giant" Ralphie. He wants to become a member of "the seven dwarves", but this is impossible as "there are already seven dwarves". The dwarves sent Ralphie away and decide to blow up the bridge so women wont be able to enter the forest in future. As they have a bad experience with dynamite they only place a sign which forbids women to pass.

Meantime, the queen asks her jester to tell a joke. As the queen has blond hair, the jester only tells jokes about "dumb black haired". She frequently asks her magic mirror: "who is the most beautiful in the land?". One day, the ghost in the mirror tells the queen is beautiful, but Snow White the more. The queen calls the hunter and his bloodhound Brutus (a West Highland White Terrier). She once gave him the task to get rid of Snow White. It turns out the hunter took her to the orphanage.

The story moves to the orphanage where a naive Snow White still plays with her dolls. The ghost of the mirror suddenly turns up in the mirror of the dollhouse. He informs Snow White a round-up is organized to find her. Snow White runs away to the bridge. As she may not pass the bridge, due to the sign, she uses a marker to turn the womens face into a mans face. She is soon traced by Brutus and the hunter. The hunter uses a razor to remove the beard Snow White had drawn on the sign. In the forest, the hunter meets Ralphie who keeps him up. As the hunter lost Snow Whites track, he returns to the castle. He tells the queen his dog Brutus mauled up Snow White in thousand pieces and ate her.

When the dwarves return at their cottage, they find a sleeping Snow White in one of the beds. They want to get rid of her as they cant stand women. Their opinion changes after Snow White suggests to split up the house in (literally) two parts. Snow White considered dwarves to be much smaller. They declare the length of a dwarf is an old prejudice. It is not the length, but the lifestyle which determines whether someone is a dwarf or not. Upon this, Ralphie once again asks if he can be a dwarf, but this is again rejected as "there are already seven dwarves".

Snow White discovers why the dwarves have an aversion to women: Sunny and Cloudy were expelled from a school play by their female teacher as their audition for The Wolf and the Seven Young Goats was not convincing. Speedy got in love with Rapunzel. She threw out her long braids. Whilst Speedy was climbing her left braid, he was passed by a knight via the right braid. Rapunzel cut off her left braid and Speedy fell. Tschakkos aversion is caused as he cant and wont beat up women. Coocky once made vegetarian food which was not appreciated by his mother. Brumboss once again starts his story, but does not get further than the first sentence.

The queen consults her magic mirror and finds out Snow White is still alive. The hunter is locked up in the dungeon. She dresses up and goes to Snow White. Just before leaving, she meets the jester. He does not recognize the queen and tells a joke about dumb blondes. He is also locked up in the dungeon.

The dwarves find out Snow White becomes 18 years old. Whilst the dwarves are preparing a secret surprise party, the queen kidnaps Snow White. The dwarves are disappointed Snow White does not turn up, but suddenly realize she is not aware of this event. The dwarves seek for her, but only find the queens crown. They conclude Snow White has been kidnapped. They take their horse and head to the castle. Once there, they notice Brummboss is missing. They can get into the castle as there is an audition for a new jester, but none of them is selected.

Snow White is in the dungeon and gets a visit from Brummboss, disguised as a priest. He finally tells why he hates women. According the midwife, his wife and child died during delivery. Thereupon Brummboss took abdication and threw away his crown. The midwife took the crown and put in on her head. She became the new queen as the law tells the ruler is the person who wears the crown.

The "priest" takes Snow White to the scaffold. Just before her "execution", Brummboss presents to be the king who disappeared 18 years ago and also reveals he is Snow Whites father. Brummboss puts the crown on his head and becomes king again. Brummboss takes Snow White and the six other dwarves to the throne hall. He throws a feather in the air. The dwarf on which the feather falls, will marry Snow White. Snow White objects: she is in love with the former jester who she met in the dungeon.

The six dwarves return to their cottage and are upset. As they are now with six, Ralphie is selected as the seventh dwarf. The dwarves decide not to help women ever again as they are once again betrayed by such creature. This promise does not stand long: Little Red Riding Hood knocks at the door and searches for help as she got lost in the wood.

== Cast ==
* Boris Aljinovic as Cloudy
* Tom Gerhardt as Wache
* Cosma Shiva Hagen as Snow White
* Nina Hagen as The Queen
* Norbert Heisterkamp as Ralfie
* Heinz Hoenig as Brummboss
* Mavie Hörbiger as Little Red Riding Hood
* Rüdiger Hoffmann as The Mirror on the Wall
* Markus Majowski as Cookie
* Mirco Nontschew as Tschakko
* Hans Werner Olm as Spliss
* Harald Schmidt as The Best Jester Candidate
* Ralf Schmitz as Sunny
* Helge Schneider as The White Helge
* Martin Schneider as Speedy
* Atze Schröder as the Jester
* Hilmi Sözer as the Guard
* Christian Tramitz as the Hunter
* Otto Waalkes as Bubi

== Parodies ==
*Gute Zeiten, schlechte Zeiten opening theme. 

== Accolades ==

*2004 - Platinum Box Office Germany Award
*2005 - The German Comedy Award (Deutscher Comedypreis) for best comedic film
*2005 - Goldene Leinwand mit Stern

== Sequel ==
On 26 October 2006, a sequel to the original film was released: 7 Zwerge – Der Wald ist nicht genug ("Seven Dwarves - The Forest Is Not Enough"). The main cast stayed the same, apart from Markus Majowski, who was unable to film due to theatre contract commitments. He was replaced by Gustav Peter Wöhler in the film.

==References==
 

== External links ==
*  
 

 
 
 
 
 
 
 