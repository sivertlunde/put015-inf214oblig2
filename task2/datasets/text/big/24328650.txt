Paracelsus (film)
 
{{Infobox film
| name           = Paracelsus
| image          = 
| caption        = 
| director       = Georg Wilhelm Pabst Bavaria Filmkunst GmbH
| writer         = Kurt Heuser
| starring       = Werner Krauss
| music          = Herbert Windt
| cinematography = Bruno Stephan
| editing        = Lena Neumann
| studio         = Bavaria Film
| distributor    = Deutsche Filmvertriebe
| released       =  
| runtime        = 104 minutes
| country        = Nazi Germany
| language       = German
| budget         = 
}}

Paracelsus is a 1943 German drama film directed by Georg Wilhelm Pabst,    based on the life of Paracelsus.

==Cast==
* Werner Krauss as Paracelsus
* Annelies Reinhold as Renata Pfefferkorn
* Harry Langewisch as Pfefferkorn
* Mathias Wieman as Ulrich von Hutten
* Fritz Rasp as Magister
* Peter Martin Urtel as Johannes (as Martin Urtel)
* Herbert Hübner as Count von Hohenreid
* Josef Sieber as Bilse, Paracelsuss servant
* Rudolf Blümner as Froben
* Harald Kreutzberg as Fliegenbein
* Hilde Sessak as Waitress
* Franz Schafheitlin as Erasmus von Rotterdam
* Victor Janson as Mayor
* Karl Skraup as Surgeon
* Erich Dunskus as Innkeeper

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 