Below the Surface
 
 
{{Infobox film
| name           = Below the Surface
| image          = 
| image_size     = 
| caption        = 
| director       = Rupert Kathner
| producer       = Rupert Kathner
| writer         = Rupert Kathner Stan Tolhurst (story)
| narrator       = 
| starring       = Stan Tolhurst
| music          = 
| cinematography = Tasman Higgins
| editing        = Stan Tolhurst
| studio         = Australian Cinema Entertainments
| released       = 1938
| runtime        = 55 minutes
| country        = Australia
| language       = English
}} adventure tale set in the coal region of Newcastle, Australia. Only part of the movie survives today.

==Plot==
Two miners compete for an important coal contract. One of them attempts to sabotage the other but fails.

==Cast==
*Stan Tolhurst
*Phyllis Reilly
*Neil Carlton
*Jimmy McMahon
*Lawrence Taylor
*Reg King
*Billy Baker
*Leonard Clarke
*Frank Baker
*Billy Crooks

==Production==
The main investor in the movie was a prominent music house in Sydney. The film was shot on location in Cronulla, Sydney and Newcastle, with studio work done at Pagewood Studios.   Kather and Tolhurt built a mine set themselves. Shooting took place from November 1937 to February 1938. 

==Release==
Like Kathners first movie, Phantom Gold (1937), it was refused to be considered eligible for registration under the New South Wales Film Quota Act on the grounds of poor quality. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 184 

The film was never released to cinemas, the only one of Kathers movies to suffer this fate. 

In February 1938 Australian Cinema Entertainments announced plans to make four more features that year for £40,000, the first which was to be Diamonds in the Rough.  This did not eventuate. Tolhurst did revive the name with his company, ACE Films, in the late 1940s. 

==References==
 

==External links==
*  in the Internet Movie Database
*  at National Film and Sound Archive
*  at Oz Movies
 

 
 
 
 


 
 