Return from the River Kwai
{{Infobox film
| name           = Return from the River Kwai
| image          = 
| image_size     = 
| caption        = 
| director       = Andrew McLaglen
| producer       = Kurt Unger
| screenplay   = Sargon Tamimi Paul Mayersberg
| based on       =   Edward Fox Chris Penn Denholm Elliott Timothy Bottoms
| music          = Lalo Schifrin
| cinematography = Arthur Wooster
| editing        = Alan Strachan
| studio         = Roadshow Productions Screenlife Establishments
| distributor    = Rank Film Distributors  
| released       =  
| runtime        = 107 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Edward Fox, Chris Penn and Timothy Bottoms. 
 bridge over the River Kwai. They were taken by railway to Singapore, and from there aboard two ships, SS Rakuyo Maru|Rakuyo Maru and Kachidoki Maru, destined for Japan. On 12 September 1944, both ships were torpedoed by US submarines, and 1,559 of the prisoners perished. 

When released in the United Kingdom, the film carried a disclaimer that it was not related to or a sequel to the 1957 David Lean film The Bridge on the River Kwai. Nonetheless, due to legal disputes over the title with the US owners of the 1957 film, Return from the River Kwai was never released theatrically in the USA or Canada.

==Cast== Edward Fox as Major Benford
*Chris Penn as Lieutenant Crawford
*Denholm Elliott as Colonel Grayson
*Timothy Bottoms as Seaman Miller
*Tatsuya Nakadai as Major Harada
*George Takei as Lieutenant Tanaka
*Nick Tate as Lt. Commander Hunt

==References==
 

==External links==
*  
*  
* 

 

 
 
 
 
 
 
 