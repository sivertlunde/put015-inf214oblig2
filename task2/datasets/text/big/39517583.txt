Yudhpath
 
{{Infobox film
| name           = Yudhpath
| image          = 
| image_size     = 
| caption        = 
| director       = Ravi Ravan Kathuria	 
| producer       = Javed Riaz
| Screenplay     = Ravi Ravan Kathuria, Baba Khan
| narrator       = 
| starring       =Sudesh Berry Siddharth 
| music          = Dilip Sen Sameer Sen
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1992
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

 Yudhpath  is a 1992 Bollywood film directed by Ravi Ravan Kathuria and starring Sudesh Berry, Mohnish Bahl, Kiran Kumar, Rakesh Bedi, Tinnu Anand and Siddharth . The movie was released on  March 27, 1992. The film was an action movie which has got good music. The movie was made a year after the Hit movie "Vansh" where Sudesh Berry and Siddharth have acted for second time. However, this movie was a big disaster at box office.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jab Aaye Aise Pal Kabhi"
| Anwar
|-
| 2
| "Chhora Badnaam"
| Jolly Mukherjee, Sukhwinder Singh
|-
| 3
| "I Love You"
| Kumar Sanu, Kavita Krishnamurthy
|}


 
 
 

 