Mr. Sardonicus
{{Infobox film
| name = Mr. Sardonicus
| image = Sardonicusposter.jpg
| caption = Theatrical release poster
| producer = William Castle
| director = William Castle
| writer = Ray Russell Ronald Lewis Audrey Dalton Guy Rolfe Vladimir Sokoloff Erika Peters Lorna Hanson
| music = Von Dexter
| distributor = Columbia Pictures
| released =  
| runtime =  89 min.
| country = United States English
| budget =
}}
Mr. Sardonicus (1961 in film|1961) is a horror film produced and directed by William Castle. It tells the story of Sardonicus, a man whose face becomes frozen in a horrifying grin while robbing his fathers grave to obtain a winning lottery ticket. Castle cited the film in his memoir as one of his favorites to produce. 

==Plot summary==

In 1880, in the fictional central European country of Gorslava, prominent  , credited as "Oscar Homolka") torturing one of the Barons servants by placing leeches on her face.

Maude herself is fearful of what will happen if Sir Robert is not willing to do as Sardonicus asks of him. Even Krull is not immune to the Barons cruelty as he is missing an eye, thanks to Sardonicuss anger. 

Sardonicus tells Sir Robert his story. Once he was Marek Toleslawski, a farmer like his father Henryk ( , Latin for "sardonic smile", used for tetanus victims) and learned to speak all over again with the help of some of the greatest diction experts in the world. His own subsequent experiments on young women were intended to find a cure for his condition, but he has had no success. Since Sardonicuss new wife Maude had mentioned Sir Robert as a great doctor specializing in paralysis, he had hoped Sir Robert would help restore his face. (He had tried several other great doctors before, with no success). Sir Robert agrees to try.

Sir Roberts efforts at restoring Sardonicuss face have no effect. Sardonicus demands he try new and experimental treatments and, when Sir Robert refuses, threatens to mutilate Maudes face to match his own. Sir Robert sends for equipment and supplies, including a deadly South American plant which he uses to experiment on dogs. When Sardonicus then has Maude and Sir Robert see the open upright coffin of Henryk Toleslawski which hed brought to the castle, Sir Robert conceives an idea: he will inject Sardonicus with a diluted plant extract, then recreate the event that led to Sardonicus condition. Though Sardonicus is skeptical, he agrees. The injection is made, the room darkened, and a light shined on the skull. Sardonicus screams in agony, imagining Henryks flesh has been restored. Sir Robert comes into the room to find Sardonicus face is back to normal, though Sir Robert advises him to not speak until his facial muscles have had time to adjust. The Baron writes a note to Maude annulling their marriage, and a note to Sir Robert, asking his fee. Sir Robert tells Sardonicus, "You owe me nothing", and Sardonicus lets them both go. 

At the train station for the trip back to London, Sir Robert and Maude are confronted by a frightened Krull, who tells them Sardonicus not only cannot speak, he cannot open his jaw or lips at all. They must return and help him. But Sir Robert tells Krull that the injection was only water, that the plant extract would have been lethal even in a small dose. What happened was all psychological, and once Sardonicus realizes that, he will be all right and able to speak and eat.

(At this point, director Castle gives his "punishment poll", resulting in Sardonicus getting more punishment.)

The story resumes with Krull returning to the castle, and lying to the Baron, telling him that he just missed Sir Robert (presumedly Krulls revenge for Sardonicuss cruelty to him). This dooms Sardonicus to death by starvation, while Krull cruelly sits down to eat his lavish dinner in front of the Baron.

==Production==
 
The film is based on a short story called "Sardonicus" that was originally published in Playboy. Castle purchased the rights and hired its author, Ray Russell, to write the screenplay. Castle, p. 163 

To achieve Sardonicuss terrible grin, actor Guy Rolfe was subjected to five separate facial appliance fittings. He could not physically stand to wear the piece for more than an hour at a time.  As a result, the full makeup is only shown in a few scenes, with Rolfe instead wearing a kerchief over the lower half of his face for most of the running time.
 thumb up or down as to whether Sardonicus would live or die. Legend has it no audience ever offered mercy so the alternate ending – if in fact one existed – was never screened.   

The "poll" scene, as presented in the film, is hosted by Castle himself, and he is shown pretending to address the audience, jovially egging them on to choose punishment, and "tallying" the poll results with no break in continuity as the "punishment" ending is pronounced the winner. The "punishment" ending occupies only three minutes of film after the "poll", and was the ending of the original Ray Russell short story. Given that Turner Classic Movies was unable to locate any cut of the film which included the "merciful" ending, the suggestion of alternative endings itself appears to have been an elaborate conceit on the part of Castle in service of the "gimmick". There are reports that a separate version was produced for drive-ins, in which patrons were asked to flash their cars headlights to vote, but despite an intense search by Columbia, so far this footage has not come to light, suggesting that the story is aprocryphal.

==Cast==
* Oskar Homolka as Krull  Ronald Lewis as Sir Robert Cargrave 
* Audrey Dalton as Maude Sardonicus 
* Guy Rolfe as Baron Sardonicus 
* Vladimir Sokoloff as Henryk Toleslawski 
* Erika Peters as Elenka 
* Lorna Hanson as Anna

==Critical response==
The PTA Magazine described Mr. Sardonicus as an "elaborately produced  ... that evokes disgust as well as macabre thrills". {{cite news 
  | last = National Congress of Parents and Teachers
  | title = Mr. Sardonicus
  | work = The PTA Magazine
  | page = 40
  | date = 1961 }}  The New York Times sharply disagreed. While praising Lewiss performance, the Times stated that Castle "is not Edgar Allan Poe. Anybody naive enough to attend...will find painful proof". {{cite news 
  | last = Thompson
  | first = Howard
  | title = Five Golden Hours and Mr. Sardonicus in Multiple Openings
  | work = The New York Times
  | date = 1961-10-19
  | url = http://movies.nytimes.com/movie/review?_r=1&res=9402E2D6113DE733A2575AC1A9669D946091D6CF&partner=Rotten%20Tomatoes
  | accessdate = 2009-01-24}} 

==Cultural impact== Jeffrey Lyons played himself explaining the films psychological subtext to FBI agents on the case.

==Notes==
 

==References==
* Castle, William (1976). Step Right Up! Im Gonna Scare the Pants Off America: Memoirs of a B-Movie Mogul. New York, Putnam. ISBN 0-88687-657-5 (Pharos edition 1992). Includes introduction by John Waters.
* John Waters (filmmaker)|Waters, John (1983). Crackpot: The Obsessions of John Waters. New York, Macmillan Publishing Company. Chapter 2, "Whatever Happened to Showmanship?", was originally published in American Film December 1983 in a slightly different form.
* Weaver, Tom (2002). Science Fiction Confidential: Interviews with 23 Monster Stars and Filmmakers. McFarland. ISBN 0-7864-1175-9.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 