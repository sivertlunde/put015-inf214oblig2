Chandni Bar
{{Infobox film name = Chandni Bar  image = Chandni Bar.jpg caption =   director = Madhur Bhandarkar  writer = Mohan Azaad (screenplay & dialogues)   Masud Mirza (dialogues) starring = Tabu (actress)|Tabu,   Atul Kulkarni,   Rajpal Yadav,   Ananya Khare,   Vishal Thakkar,   Minakshi Sahani  producer = Lata Mohan cinematography = Rajeev Ravi editor = Hemal Kothari music = Raju Singh (background) distributor =  released = 28 September 2001 runtime = 150 min language = Hindi budget = 
}}
 Mumbai underworld, Tabu and National Film Awards. Tabu came into the limelight with this movie, along with Rajpal Yadav.

== Synopsis ==

Mumtaz (Tabu (actress)|Tabu) is a young village woman whose family is killed in communal riots. She moves to Mumbai with her uncle, the only family member she has left. They are desperately poor and her uncle persuades her to become a bar girl at Chandni Bar. This is merely temporary, he promises, until he gets a job. Mumtaz is shy and loathes the work, but she forces herself to dance and flirt. However, the uncle doesnt keep his promise; he lives on her earnings, drinking them away, and never gets a job. He adds one final, unforgivable crime to the list when he gets drunk and rapes her.

By this time she has caught the eye of a gangster called Pottya Sawant (Atul Kulkarni). When she tells Pottya what her uncle did to her, Pottya decides to "defend her honor." Pottya kills the uncle and marries her. She lives with him for several years and gives birth to a son, Abhay, and daughter, Payal. She wants Abhay and Payal to be educated and stay far away from her world of dancing girls and Pottyas gangsters.

Pottya gets in the bad books of other gangsters, loses his connections, becomes a target for the police, and is killed in a planned "encounter," leaving Mumtaz to return to Chandni Bar.

Years pass and Abhay and Payal are in their teens and attending school. Mumtaz still works at Chandni Bar, not as a dancer, but as a waitress. Then the unexpected happens &mdash; Abhay is arrested by the police for extortion and held in a juvenile home for the crime he had not committed. At the home, Abhay is raped by a couple of inmates.

Mumtaz attempts to talk to the police, to no avail. She then meets with some influential people, who agree to help her out for a price, which she must bring to them in less than two days. Mumtaz hustles and sells her body to obtain the money but is still short. Seeing her mothers plight, Payal takes up dancing at Chandni Bar and brings the rest of the money to her troubled mother.

With the money turned over to release Abhay, the family heaves a sigh of relief &mdash; until Mumtaz notices that it is not the same Abhay she had known two days earlier. This one is cold, ruthless, and has only purpose in life: vengeance. Abhay takes his revenge and shoots the inmates who raped him. It is implied that Payal is following in her mothers footsteps, and Abhay is set to be another Pottya.
Brief summary of the whole movie

== Cast ==
* Tabu_(actress)   as   Mumtaz Ali Ansari
* Atul Kulkarni   as   Pottya Sawant
* Rajpal Yadav   as   Iqbal Chamdi
* Vallabh Vyas   as   Habib Bhai
* Vinay Apte   as   Inspector Gaikwad
* Ananya Khare   as   Deepa Pandey (bar-girl)
* Upendra Limaye as Deepas Husband
* Rajanna   as   Uma Shankar Pande
* Minakshi Sahani   as   Payal Sawant (Mumtazs daughter)
* Vishal Thakkar   as   Abhay Sawant (Mumtazs son)
* Abhay Bhargav   as   Hegde Anna
* Suhas Palshikar   as   Irfan Mamu (Mumtazs uncle)
* Shabbir Mir   as   Uncle Pinto

== Awards ==
The film won four National Awards. Tabu also received Best Actress nominations at the Filmfare Awards and Bollywood Awards. Atul Kulkarni also received a Best Supporting Actor nomination at the Star Screen Awards. Madhur Bhandarkar was nominated for Best Director in several venues, but did not win.
 National Film Awards (India) Best Actress - Tabu Best Supporting Actor - Atul Kulkarni Best Supporting Actress - Ananya Khare
* National Film Award for Best Film on Other Social Issues

2002 IIFA Awards (India) Best Actress - Tabu

2002 Star Screen Awards (India)
* Star Screen Award Best Story - Madhur Bhandarkar

2002 Zee Cine Awards (India) Best Actress - Tabu

==Soundtrack==
The film does not feature any of original tracks of its own. But many Bollywood Hindi songs feature many times in various scenes in the dancing bar.
The theme music is a classical aalap rendered by Shubha Mudgal.

== References ==
 
 

== External links ==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 