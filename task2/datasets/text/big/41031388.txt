The Magic Pudding (film)
 
{{Infobox film
| name           = The Magic Pudding
| image          = The Magic Pudding film adapt.jpg
| caption        = Australian DVD Cover
| director       = Karl Zwicky
| producer       = Gerry Travers Paddy Conroy Bruce Davey Carmel Travers Edward Trost
| writer         = Harry Cripps Greg Haddrick Simon Hopkinson  
| based on       =   Jack Thompson Dave Gibson Greg Carroll Roy Billing
| music          = Chris Harriott
| cinematography = 
| editing        = Richard Hindley
| studio         = New South Wales Film and Television Office Australian Broadcasting Corporation Premium Movie Partnership 
| distributor    = Energee Entertainment 20th Century Fox Icon Productions
| released       =  
| runtime        = 75 minutes
| country        = Australia
| language       = English
| budget         = 
| gross = A$1,054,893 (Australia)
}} story by Norman Lindsay.

==Plot==
In the South Pole, Bill Barnacle and his crew are cursing across the waters as when their ship crashes from a wave. Afterwards, they were starving and their evil shipmate Buncle wanted to eat Sam as when he thought it tastes like veggie pie. After that situation they discover some sort of pudding magic called Albert that can talk, change flavour on request, lasts forever and demands that they continue to eat him. The crew are divided over the pudding but two of them resolve to protect and look after it. 10 years later, far from this, a small koala named Bunyip Bluegum discovers that he is not an orphan and sets out on a quest to find his parents, Meg and Tom Bluegum. Their paths cross on the road when Bunyip stumbles into the middle of an attempt by thieves to steal the everlasting pudding from Bill and his first mate Sam.

==Cast==
* John Cleese as Albert the Magic Pudding: The pudding who lasts forever and turns into different sorts of puddings.
* Geoffrey Rush as Bunyip Bluegum: An accomplished young koala that leaves home in search of his lost parents.
* Hugo Weaving as Bill Barnacle: A sailor who leads the noble society of Pudding owners group.
* Sam Neill as Sam Swanoff: A penguin who is a shipmate of Bill Barnacles cruise. Jack Thompson as Buncle: A very selfish and very hungry wombat who is after the magic pudding for himself to eat forever. Also a former shipmate of Bill Barnacles cruise.
* Toni Collette as Meg Bluegum: The mother of Bunyip Bluegum.
* Roy Billing as Tom Bluegum:  father of Bunyip Bluegum.
* Greg Carroll as Watkin Wombat: The nephew of Buncle who is one of the pudding thieves who has to catch and bring the pudding to him. Dave Gibson as Possum: The wombats helper of the pudding thieves and Wattleberry: The uncle of Bunyip Bluegum .
* Mary Coustas as Ginger: A Mouse who is Buncles executive assistant who bosses everyone to get service for Buncle.
* John Laws as Rumpus Bumpus
* Sandy Gore as Frog on the Log
* Michael Veitch as Bandicoot
* Peter Gwynne as Benjamin Brandysnap
* Robyn Moore as Henrietta Hedgehog
* Martin Vaughan as Parrot
* Gerry Connolly as Dobson Dorking

==Release==
The film was first released in Australia on December 14, 2000.
 VHS tape and DVD were released in Australia in May 2001.

It was then released in New Zealand four months later after the Australian release on April 9, 2001.

A re-release of the DVD was released in Australia in 2013.

==Soundtrack==
The soundtrack was recorded by the Marionette Theatre of Australia and released in the next year after the movie on July 14, 2001.
{{track listing
| total_length = 50:20
|title1=Its A Wonderful Day - Geoffrey Rush|length1=1.25
|title2=Albert, The Magic Pudding - John Cleese, Sam Neill, Geoffrey Rush & Chorus|length2=2.42
|title3=If I Had You - Kate Ceberano|length3=4.23
|title4=I Want You Back - N Sync|length4=3.22
|title5=The Puddin Owners Song - Sam Neill, Geoffrey Rush & Hugo Weaving|length5=2.32
|title6=My Heart Beats - Toni Collette|length6=2.16
|title7=Flying Without Wings - Westlife|length7=3.36
|title8=Sister - Sister2Sister|length8=3.25
|title9=Its Worse Than Weevils - Sam Neill, Geoffrey Rush & Hugo Weaving|length9=0.38
|title10=Save the Town - Sam Neill, Geoffrey Rush & Hugo Weaving|length10=1.51
|title11=Eternal Flame - Human Nature|length11=3.20
|title12=In the Underground Tonight - Jack Thompson, Mary Coustas, Dave Gibson & Chorus|length12=1.47
|title13=Friends - Merril Bainbridge|length13=4.24
|title14=The Magic Pudding - Rolf Harris|length14=3.07
|title15=Now I Can Dance - Tina Arena|length15=5.55
|title16=A Slice of Pudding - The Magic Pudding Orchestra|length16=5.37}}

==Video games==
* The Magic Pudding Adventure - The same month when the movie was released, an interactive game called "The Magic Pudding Adventure" was released with video highlights from the movie and 5 re-playable activities including Sink or Swim also released on DVD.

==External links==
*  

 
 
 
 
 
 