Saving Santa
{{Infobox film
| title = Saving Santa
| director = Leon Joosen
| screenplay = Ricky Roxburgh
| story = Tony Nottage
| music =  Grant Olding
| starring = Martin Freeman   Tim Curry   Noel Clarke   Tim Conway   Pam Ferris   Ashley Tisdale   Joan Collins
| studio = Gateway Films Prana Studios
| distributor = The Weinstein Company Cinema Management Group
| released =  
| country = United Kingdom United States
| language = English
| budget =  $7.5 million
}}

Saving Santa is a 2013 computer-animated film|computer-animated comedy film Created and Written by Tony Nottage and directed by Leon Joosen, Produced by Tony Nottage, Terry Stone and Nick Simunek.

== Plot ==
A lowly stable elf finds that he is the only one who can stop an invasion of the North Pole by using the secret of Santas Sleigh, a TimeGlobe, to travel back in time to Save Santa - twice.

== Cast ==
* Martin Freeman as Bernard D. Elf
* Tim Curry as Nevil Baddington
* Noel Clarke as Snowy
* Tim Conway as Santa
* Pam Ferris as Mrs. Clause
* Ashley Tisdale as Shiny
* Joan Collins as Vera Baddington
* Chris Barrie as Blitzen
* Nicholas Guest as Chestnut
* Rebecca Ferdinando as Valley Girl Elf
* Craig Fairbrass as The Mercenary
* Terry Stone as Mercenary
* Alex Walkinshaw as Reporter

== Release ==
It was released nationwide on November 1, 2013 on Blu-Ray and DVD by The Weinstein Company and Anchor Bay Entertainment. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 
 
 