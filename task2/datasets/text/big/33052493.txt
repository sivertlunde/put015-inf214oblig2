Algie's Romance
 
 
{{Infobox film
| name           = Algies Romance
| image          = 
| image_size     =
| caption        = 
| director       = Leonard Doogood
| producer       = Leonard Doogood
| writer         = Leonard Doogood Keith Yelland
| based on       = 
| narrator       =
| starring       = Leonard Doogood Boyd Irwin
| music          =
| cinematography = 
| editing        = 
| studio = South Australian Feature Film Company
| distributor    = 
| released       = 20 April 1918 (preview) 1 September 1918 (Sydney)
| runtime        = 3,500 feet
| country        = Australia English intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Algies Romance is a 1918 Australian silent film. It is a comedy starring Charlie Chaplin impersonator Leonard Doogood as an Englishman who arrives in Australia and has adventures.

==Plot==
An Englishman, Algie, arrives in Australia and stays with friends in the country. Twin sisters both fall in love with him. Various practical jokes are played on Algie, but he eventually proves his mettle. He proves himself a crack shot and gains a wife.   

==Cast==
*Leonard Doogood as Algie
*Boyd Irwin
*May Henry
*June Henry

==Production==
Doogood was a Charlie Chaplin impersonator who had previously made a one-reel short film in South Australia, Charlies Twin Brother.  

The film was shot on a cattle station owned by the Downer family in South Australia, near the Mount Lofty Ranges.  Technical facilities were provided by Southern Cross Feature Films. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 83. 

The film was well received and Doogood made plans for a follow up, Dinkum Oil, based on a novel by Frederick J Mills, but it was never shot.  

It is considered a lost film.

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 


 