In the Next Room
 
{{Infobox film
| name           = In the Next Room
| image          = In_The_Next_Room_1930_Poster.jpg
| caption        = Theatrical poster
| director       = Edward F. Cline
| producer       =
| writer         = Story: Burton E. Stevenson Play: Harriet Ford Eleanor Robson Belmont Screenplay: Harvey Gates James A. Starr
| starring       = Jack Mulhall Alice Day Robert Emmett OConnor John St. Polis
| music          = Alois Reiser, Mel Le Mon
| cinematography = John F. Seitz
| editing        =
| distributor    =  
| released       =  
| runtime        = 69 minutes
| country        = United States English
| budget         =
}} mystery film released by First National Pictures, a subsidiary of Warner Bros. and directed by Edward F. Cline. The movie stars Jack Mulhall and Alice Day. The film was based on the play by Eleanor Belmont and Harriet Ford, which itself was derived from the book The Mystery of the Boule Cabinet by Burton E. Stevenson.

==Cast==
*Jack Mulhall as James Godfrey
*Alice Day as Lorna
*Robert Emmett OConnor as Tim Morel
*John St. Polis as Philip Vantine
*Claud Allister as Parks (the butler)
*Aggie Herring as Mrs. OConnor
*DeWitt Jennings as Inspector Grady
*Webster Campbell as Snitzer
*Lucien Prival as French Exporter

==Synopsis==
The film is preceded by a prologue set in 1889 in which we see an angry husband murdering his wifes lover. The setting of the film then moves to 1929, just as an antiques dealer Philip Vantine (John St. Polis) has finished moving into the same house where the 1889 murder had occurred. 

For years the house had been deserted, its only occupant being the butler (Alister). Vantine moves in with his daughter Lorna (Day) and maid and retains the elderly butler to help take care of the house. One night, the police arrive at the house after receiving a telephone call that a murder has just been committed on the premises. When they arrive, the police are surprised to find that no one in the house knows what they are talking about. It so happens that on that day a valuable antique cabinet has arrived at the house and this has attracted several strangers to come to the house to examine it. 

A young reporter James Godfrey (Mulhall), who has come to see Lorna, also arrives at the house. As the police are investigating the mysterious telephone call, however, a series of strange murders occur. As Godfrey is helping Lorna to open the antique cabinet, they find a hypnotized woman inside. The police are then notified but when everyone returns to the cabinet they find the woman has disappeared. One of the police detectives, Tim Morel (OConnor), is soon knocked unconscious. Vantine is also hit of the head, but stays conscious. The son of the former owner of the cabinet is soon found murdered. 

As Godfrey attempts to call his newspaper, Lorna is abducted and taken to the wine cellar. She is rescued by a one-legged man who suddenly disappears and is nowhere to be found. Meanwhile, the hypnotized girl appears in a revived state and reveals that her partner, a diamond smuggler, has been murdered with poison. Eventually, Godfrey, assuming the role of an amateur detective, clears up the entire mystery.

==Preservation status==
No film elements are known to survive. The soundtrack, which was recorded on Vitaphone disks, may survive in private hands.

==See also==
*List of lost films

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 