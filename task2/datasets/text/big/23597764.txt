Fury at Smugglers' Bay
 
 
{{Infobox film
| name           = Fury at Smugglers Bay
| image          = "Fury_at_Smugglers_Bay"_(1961).jpg
| image_size     = 
| caption        = 
| director       = John Gilling
| producer       = John Gilling
| writer         = John Gilling
| narrator       =  John Fraser
| music          =  Harold Geller
| cinematography = Harry Waxman
| editing        =  John Victor-Smith
| studio         = John Gilling Enterprises
| distributor    = Regal Films International (UK)
| released       = March 1961 (UK)
| runtime        = 92 mins
| country        = United Kingdom
| language       = English
}}
 British adventure John Fraser.  The plot revolves around smuggling in Cornwall. Studio sequences were filmed at Twickenham Film Studios in west London with the external sequences representing the coast of Cornwall actually being shot at Abereiddy on the north Pembrokeshire coast in south-west Wales. 

==Cast==
* Peter Cushing as Squire Trevenyan 
* Bernard Lee as Black John 
* Michèle Mercier as Louise Lejeune  John Fraser as Christopher Trevenyan 
* William Franklyn as The Captain 
* George Coulouris as François Lejeune 
* Liz Fraser as Betty 
* June Thorburn as Jenny Trevenyan 
* Katherine Kath as Maman 
* Maitland Moss as Tom, the butler  Tommy Duggan as Red Friars 
* Christopher Carlos as The Tiger, a pirate
* Miles Malleson as Duke of Avon 
* Alan Browning as 2nd Highwayman

==Critical reception==
In the Radio Times, David Parkinson gave the film three out of five stars, and noted, "as Cushing suggested in his memoirs, this 1790s adventure is tantamount to an English western, with a saloon brawl, sword-wielding showdowns and a last-minute rescue. However, the peripheral characters are more subtly shaded, with Miles Mallesons comic nobleman and George Coulouriss abused outsider being particularly well realised."  

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 

 
 