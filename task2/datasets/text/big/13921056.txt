Situation Hopeless... But Not Serious
{{Infobox Film
| name           = Situation Hopeless... But Not Serious
| image          = Situation Hopeless But Not Serious.jpg
| image_size     = 
| caption        = 
| director       = Gottfried Reinhardt
| producer       = Gottfried Reinhardt Robert Shaw (novel) Jan Lustig Silvia Reinhardt
| narrator       = 
| starring       = Alec Guinness Mike Connors Robert Redford Paul Dahlke
| music          = Harold Byrne Leon Carr
| cinematography = 
| editing        = 
| distributor    = Paramount Pictures
| released       =   October 13, 1965
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1965 comedy Robert Shaw.
 Viennese saying; "The situation is desperate but not serious."

==Plot==
In World War II, two American fliers, Captain Hank Wilson (Robert Redford) and Sergeant Lucky Finder (Mike Connors), are forced to bail out over Germany. They encounter Wilhelm Frick (Alec Guinness), who hides them from the authorities in his cellar. He enjoys their company so much that he does not inform them when the war ends. Instead, he maintains a masquerade to convince his "guests" that Germany is still fighting. Eventually, after seven years, the Americans escape into a peaceful West Germany and find out the truth.

==Cast==
*Alec Guinness as Wilhelm Frick 
*Mike Connors as Sgt. Lucky Finder 
*Robert Redford as Captain Hank Wilson
*Paul Dahlke as Herr Neusel  Frank Wolff as Quartermaster Sergeant  
*Anita Höfer as Edeltraud 
*Mady Rahl as Lissie
*Elisabeth von Molo as Wanda
*John Briley as Sergeant (uncredited) 
*Carola Regnier as Senta (uncredited)

==See also==
*Wake Me When the War Is Over, a 1969 TV film with a similar plot

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 

 
 