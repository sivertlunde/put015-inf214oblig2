Barbie as the Princess and the Pauper
{{Infobox film
| name           = Barbie as the Princess and the Pauper
| image          = Barbie as the Princess and the Pauper poster.jpg
| image_caption  = Barbies first ever musical movie.
| caption        = DVD/VHS Cover
| director       = William Lau
| producer       = Jesyca C. Durchin Jennifer Twiner McCarron	
| writer         = Cliff Ruby Elana Lesser
| based on       =    
| narrator       = Kelly Sheridan Mark Hildreth Alessandro Juliani Ian James Corlett Kathleen Barr Martin Short
| music          = Arnie Roth
| editing        = Greg Richardson Mattel Entertainment Universal Studios Home Entertainment (re-release) Right Entertainment (UK)
| released       =  
| runtime        = 85 minutes
| country        = United States Canada
| language       = English
| budget         =
}} musical in Barbie film series.  It is directed by William Lau and stars the voice of Kelly Sheridan, who has been voicing Barbie in all the CGI films to date, as both Anneliese and Erika. The story is loosely inspired by the Mark Twain novel The Prince and the Pauper, but it is unrelated to the 1939 film The Princess and the Pauper. It is the first movie not to be told by Barbie but instead narrated by her.

Songs for the film are written by Amy Powers, Megan Cavallari and Rob Hudnut, who also executive produced the film.

==Plot==
In an unnamed kingdom, a blonde princess and a brunette pauper are born simultaneously. The princess, Anneliese, craves freedom from her royal duties, especially when she is informed by her widowed mother, Queen Genevieve, that she must marry the wealthy king of a nearby kingdom because their royal treasury is nearly bankrupt.  The Pauper, Erika, craves a different sort of freedom as she is an indentured servant at the Madame Carps Dress Emporium to work off her parents debt, but dreams of becoming a singer.

Unbeknownst to the royal family, the royal advisor Preminger has been having his minions Nick and Nack steal gold from the royal mines for some time. Preminger plans to announce his newfound wealth to Queen Genevieve, which he believes will allow him to marry Princess Anneliese. When Preminger learns that Queen Genevieve has approached King Dominick of Dulcimia to marry Anneliese, Preminger orders Nick and Nack to kidnap Anneliese, so that Preminger can stage his "rescuing" her, hoping that Queen Genevieve will give him Annelieses hand in marriage.

Anneliese and her tutor, Julian, are in love with one another, though they are unaware of the others feelings. To cheer her up, Julian decides to take Anneliese into town.  The princess crosses paths with Erika and the girls are shocked to see how identical they are, the only differences between them being their hair colour and the crown-shaped birthmark on Annelieses right shoulder. The girls become friends.

On the same night, Anneliese is kidnapped and a fake letter is left on her desk stating Anneliese "ran away" to avoid marrying King Dominick. Julian suspects she was kidnapped by Preminger, so he asks Erika to impersonate the princess until he can rescue the real Anneliese. Erika agrees to the temporary ruse, and surprises Preminger when she arrives to welcome King Dominick. Dominick and Erika fall in love, despite Erikas fear of being thrown into prison when revealed that she is an imposter.

Anneliese manages to escape from Premingers men, but after a series of events ends up being recaptured by Preminger and thrown into the mines with Julian, who had been following Preminger. Upon returning to the castle, Preminger exposes Erika as a fraud and claims that she had conspired with Julian to kill and replace the real princess.  Erika, unable to prove her good intentions, is locked away in a small cell by the queen. After convincing everyone that the princess is dead, Preminger persuades Queen Genevieve to marry him to save the kingdom. Erika manages to escape from her cell and bumps into Dominick, who reveals that he doesnt believe that Preminger is telling the truth. Elsewhere, Anneliese and Julian confess their love for each other and work together to escape the mines.

At the queens wedding, Anneliese arrives in time to stop the marriage and reveals Premingers deceit. After confessing to her mother that she loves Julian and does not wish to marry King Dominick, Anneliese presents to the queen a solution to the kingdoms gold shortage: the rocks discarded by the miners actually hold amethysts and the mine is full of them.

Dominick confesses his love for Erika and asks if she would marry him. Erika, freed from her debt to Madam Carp thanks to Anneliese, chooses not to accept marriage just yet so that she achieve her dream of singing all around the world.  After several months, she realizes that the place she would really like to sing is among her friends, and returns to marry Dominick while Anneliese marries Julian in a double wedding.

==Voice cast==
*Kelly Sheridan as Princess Anneliese / Erika / Narrator  
**Melissa Lyons as Princess Anneliese (singing voice) Julie Stevens as Erika (singing voice)
*Alessandro Juliani as Julian Mark Hildreth as King Dominick
**Mark Luna as King Dominick (singing voice)
*Martin Short as Preminger
*Kathleen Barr as Serafina and Bertie
*Ian James Corlett as Wolfie 
*Ellen Kennedy as Queen Genevieve
*Pam Hyatt as Madame Carp
*Brian Drummond as Nick 
*Jan Rabson as Nack / Midas
*Colin Murdock as the Royal Scheduler
*Janyse Jaud as the Palace Maid
*Lee Tockar as Ambassador Bismarck 
*Garry Chalk as Elbay the towel rabbit
*Roger Monk as the Minister

==Soundtrack==
This film is the first musical in the series of Barbie CGI films.  The entire soundtrack (including popular duets and the opening orchestral theme) can be found on the "Barbie Sings! The Princess Movie Song Collection" CD, released by Mattel in 2004.

The songs in the film are, in chronological order, as follows.
 Julie Stevens (Erika) & Melissa Lyons (Anneliese)
# How Can I Refuse? - Performed by Martin Short (Preminger) Julie Stevens (Erika) Julie Stevens (Erika) & Melissa Lyons (Anneliese) Julie Stevens (Erika)  Julie Stevens (Erika) Julie Stevens (Erika) & Mark Luna (Dominick) Julie Stevens (Erika) & Melissa Lyons (Anneliese)
# Im on My Way - Performed by Sara Niemietz

==Critical response==
DVD Verdict called it "wholesome entertainment" with "sweet songs tunefully sung" though lacking in grown-up humor.  Entertainment Weekly scored it a B+, noting a generally "feminist" story, and DVD extras including seven sing-along tracks.   TV Guide scored it 2.5/4, praising the "peppy score" and classic story as distinguishing an otherwise "ordinary Mattel-icized version of the classic tale". 

=== Home Media Release ===

The DVD and VHS was released on September 28, 2004 and distributed by Lionsgate Home Entertainment. The re-release was released on January 5, 2010 by Universal Studios. The DVD also included the movie soundtrack.

==Video game==
A video game for Game Boy Advance, PC, and Macintosh was released in 2004 by Vivendi Universal. In the Game Boy Advance title, the plot follows that of the movie: players must thwart Premingers attempt to take over the kingdom by marrying Anneliese. Players control four characters: Anneliese, Erika, Serafina, and Wolfie.

==References==
 

==External links==
* 
* 
* 
 
 

 
 
 
 
 
 
 
 
 
 
 