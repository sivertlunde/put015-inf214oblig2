She Creature
 
 
 
{{Infobox television film
| name           = She Creature 
| image          = She Creature DVD.jpg
| caption        = DVD
| director       = Sebastian Gutierrez
| producer       = Lou Arkoff Colleen Camp Stan Winston
| writer         = Sebastian Gutierrez
| starring       = Rufus Sewell Carla Gugino Rya Kihlstedt Jim Piddock Reno Wilson Mark Aiken
| music          = David Reynolds
| cinematography = Thomas L. Callaway
| editing        = Daniel Cahn
| company        = Columbia TriStar Television 
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
}} series of the earlier films.

==Plot==
 
Circa 1900, in Ireland, two carnies, Angus Shaw (Rufus Sewell) and his wife Lillian (Carla Gugino), abduct a mermaid (Rya Kihlstedt) and try to smuggle her into the United States. As their ship appears to lose its way, the mermaid begins to show a deadly, carnivorous side.

==Cast==
*Rufus Sewell as Angus Shaw 
*Carla Gugino as Lillian "Lily" Shaw 
*Jim Piddock as Captain Dunn 
*Reno Wilson as Bailey 
*Mark Aiken as Gifford 
*Fintan McKeown as Skelly 
*Aubrey Morris as Mr. Woolrich 
*Gil Bellows as Miles 
*Rya Kihlstedt as Mermaid 
*Hannah Sim as Queen of the Lair
*Jon Sklaroff as Russian Eddie
*David Nott as Cook 
*Dan Hildebrand as Christian
*Preston Maybank as Navy Captain 
*Brian Sieve as Officer Dixon
*Matthew Roseman as Officer Jenkins
*Gabriel Gutierrez as Young ODonnell
*Isabella Gutierrez as Miranda
==Release==
 

==Reception==
 
===Awards and nominations===
 
* Saturn Award (Best Single Television Presentation).
* Hollywood Makeup Artists and Hair Stylist Guild Award (Best Special Makeup Effects - Television Mini-Series/Movies of the Week).

==See also==
* Mermaids in popular culture

== References ==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 