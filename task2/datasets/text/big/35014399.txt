La tumultueuse vie d’un déflaté
 

{{Infobox film
| name           = La tumultueuse vie d’un déflaté
| image          = 
| caption        = 
| director       = Camille Plagnet
| producer       = Ardèche Images Production
| writer         = 
| starring       = 
| distributor    = 
| released       =  
| runtime        = 59 minutes
| country        = France
| language       = 
| budget         = 
| gross          = 
| screenplay     = Camille Plagnet
| cinematography = Michel K. Zongo
| editing        = Florence Bresson
| music          = Youen Cadiou Eric Dambrin Pierre Thévenin
}}
La tumultueuse vie d’un déflaté is a 2009 documentary film.

== Synopsis ==
The film portrays the turbulent life of the “Great Z”, an engine driver on the Abidjan - Ouagadougou line for twenty years. He was laid off in 1995 by the National Railways of Burkina Faso following the privatization imposed by the World Bank. A seasoned reveler and a hedonist to the bone, he suddenly finds himself with no reason to live. He has lost everything and lives a gloomy life while waiting for his retirement pension. Tormented and employing a brutal and violent vocabulary, he emphatically describes his problems, his hatreds and his hopes.   

== Festivals ==
The film was the closing film for the 2010 Festival International du Film des Droits de l’Homme in Paris. 

=== Awards ===
* Corsica Doc Prize for Best Film, Festival international du film documentaire dAjaccio   
* Festival Internacional de Documentales de Ajaccio 2009
* Festival Quintessence (Ouidah, Benín) 2010

== References ==
 
 

 
 
 
 
 
 
 
 


 
 