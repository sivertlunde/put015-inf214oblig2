Alludu Seenu
{{Infobox film
| italic title = no
| name = Alludu Seenu
| image = Alludu Seenu poster.jpg
| caption = Movie poster
| writer = Kona Venkat, Gopimohan (dialogues)
| story = K. S. Ravindra, Kona Venkat
| screenplay = V. V. Vinayak
| starring = Bellamkonda Sai Sreenivas,
| director = V. V. Vinayak
| cinematography = Chota K. Naidu
| producer = Bellamkonda Suresh, Bellamkonda Ganesh Babu
| editing = Gautham Raju
| studio = Sri Lakshmi Narasimha Productions
| released =  
| runtime =
| country = India
| language = Telugu
| music = Devi Sri Prasad
| budget =    
| gross =   (Share in 2 weeks)   (Gross) 
}}

Alludu Seenu (English Translation: Son-in-law Seenu) is a 2014 Telugu comedy drama film directed by V. V. Vinayak. Produced by Bellamkonda Suresh on his banner Sri Lakshmi Narasimha Productions, the film marks the debut of his son Bellamkonda Venkatesh Chowdary as a hero with Samantha Ruth Prabhu, Brahmanandam and Prakash Raj playing important roles while Tamannaah made a special appearance by dancing in an item number.    Devi Sri Prasad composed the music for this film while Chota K. Naidu and Gautham Raju handled the cinematography and editing of the film respectively.

The film revolves around three characters - Alludu Sreenu, Narasimha and the latters look alike gangster Bhai. The former duo are thugs who land in Hyderabad after a failed escape to Chennai from their village. When Sreenu bumps into Bhai, he seizes the opportunity and makes use of his Personal Assistant Dimple to earn some quick money and also falls in love with Bhais daughter Anjali. But a horrifying truth separates the lovers and the rest of the film deals with what the truth was and the consequences of the truth and how Sreenu wins Anjalis heart using Dimple.
 Hyderabad in Alleppey in Kerala. The songs were shot in the locales of Japan, Dubai and Italy apart from the sets built in Hyderabad. After completion of few action sequences at Gachibowli Aluminium Factory in Hyderabad, the films principal photography came to an end on June 23, 2014. 

The film was awarded an A certificate from the Central Board of Film Certification.  The film released worldwide on July 25, 2014.    Upon release, the film received decent feedback from critics with most of them calling it a Routine Commercial Entertainer. While critics praised Sreenivas dancing and fighting skills, Samanthas glamour, Prakash Raj and Brahmanandams performances, they criticized the film for its predictable content and wrong placement of songs.  

The Satellite Rights of the film are sold for   to Geminitv 
== Plot ==
Seenu, better known as Alludu Seenu, lives with his uncle in a village where he and his uncle are in great debt. One day, they run away to Chennai,hoping to fly to Dubai and earning a lot of money and clearing their debts. They mistake their train and they end up in Hyderabad. Seenu lands in a lodge where he discovers he is in Hyderbad, but to his horror, his uncle is a doopelanger of Bhai, a don in the city. He uses his uncles face to earn a lot of money, and by fooling Dimple(Bramhanandam), the right hand of Bhai. He also manages to make AnjaliSamantha Ruth Prabhu,Bhais daughter, fall in love with him, when she mistakes him to be her fiancee. When Bhai discovers that there is a doopelanger of him in the city, he plans to get him killed and ends up discovering that the doopelanger is his twin brother. The rest of the story how Seenu saves his uncle and marries Anjali.

== Cast ==
*Bellamkonda Sai Sreenivas as Alludu Sreenu
*Samantha Ruth Prabhu as Anjali
*Tamannaah Bhatia in the item number "Labbar Bomma""
*Bramhanandam as Dimple
*Prakash Raj as Narasimha and Bhai Pradeep Rawat as Bhanu Bhai
*Tanikella Bharani
*Raghu Babu as Lungi Baba
*Vennela Kishore
*Ravi Babu as Peda Prasad

== Production ==

=== Development === Venkatesh and Dasari Narayana Rao attended the event. On that day, Devi Sri Prasad was announced as the music director while Chota K. Naidu and Gautham Raju were selected as the cinematographer and editor of the film respectively.    In mid-May 2014, reports emerged that the makers were considering to title the film as Alludu Sreenu.   The same was officially confirmed by Samantha in her Twitter on May 26, 2014. 

=== Casting ===
In early February 2013, Samantha Ruth Prabhu was selected to play the heroine in this film as a part of the three film deal she entered with Bellamkonda Suresh.  She stated that she accepted to be a part of the film only due to her good relationship with Bellamkonda Suresh when he helped her during the time when she was suffering with serious skin ailments. She said "Bellamkonda Suresh has stood by me during the toughest phase in my life and I am doing this film for him."  A photo shoot was conducted later on Samantha and Sai Sreenivas in the same month.  In mid-March 2013, it was reported that Tamannaah was selected as the other heroine of the film and it was said that Bellamkonda Suresh offered her a huge remuneration for the film. She too confirmed the news that she is sharing the screen along with a debutante hero.  But since the previous two scripts were not accepted by Vinayak himself and after penning a fresh script, Tamannaah was no more a part of the film as the new one required a single heroine only.  In the end of January 2014, Prakash Raj reacted in his Twitter about the speculations that he would play a double role in Mahesh Babus Aagadu that he is not playing a dual role in that film but he is playing a dual role in this film which he called an "interesting" one.  In the end of March 2014, it was reported that Tamannaah would do a special song in the film. 

It was said that she would participate in the shoot for this song in the end of April 2014 or early May 2014.  It was also said that the song would be the introduction song of the hero in the film.  But by the time the reports emerged into the media, Tamannaah wrapped up the shoot of the song in the film on March 31 and V. V. Vinayak thanked her for accepting the offer in his Facebook page.  The song, a romantic one, was shot in Hyderabad in a vineyard set for 5 days. In an interaction to Chennai Times, Tamannaahs father said "The primary reason why she agreed to do this song was because it is the most important song in the film. It is not an item song but a romantic number and the director narrated how he intends to shoot it. It is true that a year ago, when the script was first written, Tamannaah had chosen to be a part of this film. However, when the script was changed, we mutually agreed to part ways. But despite that, both Tamannaah and Vinayak have mutual respect for each other and that is why, when Vinayak requested Tamannaah to do this one song for him, she agreed."  About his character, Brahmanandam spoke "I played Dimple in this film and it is a very different character from my previous films. Prakash Raj played a dual role in this film and I played an assistant to one of his characters. But I get confused with both of them and talk about one with the other. It generates a lot of entertainment." 

=== Filming === Alleppey in Kerala.  After returning from there, Samantha tweeted in the second week of May that the team would go to Italy for filming a song on Sai Sreenivas and herself.  After filming few sequences of a song there, the team returned to Hyderabad on May 11.  Later the shoot continued at Aluminium factory at Gachibowli in Hyderabad.  The films shoot came to an end on June 23, 2014 which was confirmed by Samantha in her micro-blogging page.   

=== Soundtrack ===
{{Infobox album
| Name = Alludu Sreenu
| Longtype = To Alludu Sreenu
| Type = Soundtrack
| Artist = Devi Sri Prasad
| Cover = Alludu Seenu Audio.jpg
| Released = June 29, 2014
| Recorded = 2013-2014 Feature film soundtrack
| Length = 24:41 Telugu
| Label = Aditya Music
| Producer = Devi Sri Prasad
| Last album = Legend (2014 film)|Legend (2014)
| This album = Alludu Sreenu (2014)
| Next album = TBA
}}

 , S. S. Rajamouli and Pranitha Subhash attended as the chief guests and the audio was released on Aditya Music label. 

The audio received viral response and all the songs in the soundtrack were praised by the audience.  Reviewing the album, The Times of India wrote "Devi Sri Prasad seems to have dished out a standard edition album with a little bit of everything. All in all, its the kind of album thats good to listen to but will not exactly make you go for a repeat listening" and rated the album 3/5.  The soundtrack album had a record number of sales for which Aditya Music thanked Devi Sri Prasad and the films makers. 

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 24:41
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Ori Devudo Chandrabose
| extra1          = Javed Ali, Suchitra
| length1         = 04:14
| title2          = Neeli Neeli
| lyrics2         = Bhaskarabhatla
| extra2          = Karthik (singer)|Karthik, Harini
| length2         = 04:09
| title3          = Whats up Antu
| lyrics3         = Ramajogayya Sastry, Sharmila
| extra3          = Devi Sri Prasad, Sharmila
| length3         = 04:00
| title4          = Labbar Bomma
| lyrics4         = Chandrabose
| extra4          = Sooraj Santhosh, Ranina Reddy
| length4         = 60:33
| title5          = Oho Bujji Konda
| lyrics5         = Chandrabose
| extra5          = Jaspreet Jasz, Sharmila
| length5         = 03:39
| title6          = Alludu Seenu
| lyrics6         = Chandrabose
| extra6          = Simha, Priya Himesh
| length6         = 04:06
}}

== Release ==
In a press meet held on June 21, 2014 the makers officially declared the films release date as July 24, 2014.  On June 28, 2014 reports emerged that Gemini TV secured the satellite rights of the movie for an undisclosed price.   On July 5, 2014 a press note was issued which stated that the film would release a day later i.e. on July 25, 2014 in theaters.  The film was awarded an A certificate with few cuts by the Central Board of Film Certification on July 23, 2014. 

=== Marketing ===
The first look posters were also released on the same day itself but none of them featured the title of the film.  New posters were released after the shoot at Japan but they too didnt bear the title of the film. But Sai Sreenivas and Samantha were appreciated for their looks in those posters.  The title logo of the film was revealed on June 6, 2014.  The first look poster bearing the title and few stills were released on June 21, 2014.   Those stills received positive response and Samanthas glamour in those stills received a strong positive response.  The theatrical trailer and video promos of the songs were released on June 29, 2014 and they received positive response in which Sreenivas was appreciated for his dances, looks, acting and fighting skills; Samantha and Tamannaah were appreciated for their glamour show.  Particularly, Tamannaahs item number received great positive response which generated good buzz for the film.   

=== Reception ===
The film received positive reviews from critics. Deccan Chronicle gave a review stating "Vinayak has chosen a safe subject for the debutant. The story is not new but director Vinayak handles it very well. This film is a great launch for a debutant. Vinayak, known for his mass films, has once again provided entertainment for masses. Brahmanandam’s comedy and Samantha’s glamour quotient may also help the film" and rated the film 3/5.  123telugu.com gave a review stating "Alludu Seenu is a tailor made launch pad for Bellamkonda Sreenivas. He makes an impressive debut and enthralls everyone with his dances and fights. Samantha’s glamour, V V Vinayak’s routine but entertaining commercial aspects, and Brahmi’s hilarious comedy makes this film a wholesome entertainer" and rated the film 3.25/5.  IndiaGlitz gave a review stating "Alludu Seenu is Gopi Mohan-meets-VV Vinayak in a blistering way. A full-on commercial film where comedy scenes, songs, fights and sentimental scenes come in the expected measure" and rated the film 3.25/5. 
 Oneindia Entertainment gave a review stating "Alludu Seenu is an flop that does not deal with fresh concept. But it promises to be a good commercial entertainer. It is a good launch pad for debutant Srinivas. It can be watched once" and rated the film 3/5. 

== Box Office ==
Alludu sreenu collected   Nett from AP/Telangana while Overseas & ROI & Karnataka regions collected  5 Crores Nett.Its Satellite Rights were sold for   to Geminitv.

=== India ===
The film opened to 80% - 90% occupancy in both single screens and multiplexes.  The film collected  2.85 crores at both Telangana and Andhra Pradesh box offices on its first day which was called a Good start by the Trade Analysts for a debutantes film.     By the end of its first weekend, the film collected an aggregate of  7.6 crores in both the states thus concluding on a good note.    During its 3 week run, Alludu Seenu is managed to collect more than   Share.It occupies 6th position in the highest grossing Telugu films of the year 2014 till date. 

=== Overseas ===
The film had a great start at the Overseas Box office collecting $48,497 on its first day.  The film collected an aggregate of  3.80 crores at the Overseas Box office in three days. 

== References ==
 

== External links ==
*  
 

 
 
 
 
 
 
 