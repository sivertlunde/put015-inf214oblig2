Do Boond Pani
{{Infobox film
| name           = Do Poond Pani
| image          = Do_Boond_Pani_(1971).jpg
| image_size     = 
| caption        = 
| director       = K. A. Abbas
| producer       = K. A. Abbas
| writer         = K. A. Abbas
| narrator       =  Simi Jalal Agha Kiran Kumar Madhu Chanda
| music          = Jaidev
| cinematography = Ramchandra
| editing        = 
| distributor    =
| studio         = Naya Sansar
| released       =  
| runtime        = 141 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1971 Hindi social drama film produced and directed by K. A. Abbas.    Made under the "Naya Sansar" banner; the story, screenplay and dialogues were by K. A. Abbas, with additional dialogues by Inder Raj Anand. The music was composed by Jaidev.    The cast included Simi Garewal|Simi, Jalal Agha, Madhu Chanda, and was the debut film of actor Kiran Kumar.    The film won the Best Feature film on National integration.   

Set Against the backdrop of Rajasthan, the film focused on the scarcity of water, and the eventual building of a dam.  Ganga Singh (Jalal Agha) goes to work on the dam but loses his life leaving behind his widow and a young son in the village.    His sacrifice helps transform the desert land into a fertile area with the dam being called Ganga Sagar dam.   

==Plot==
Ganga Singh (Jalal Agha), newly married to Gauri (Simi Garewal), brings his wife to his village where he lives with his father Hari Singh (Sajjan), and sister Sonki (Madhu Chanda). The village is suffering from drought and villagers have to travel a long distance to get water. Ganga Singh hears of a dam being constructed and leaves his wife to join in the building of it. His family goes through misfortunes, with his father dying and his sister being raped by the dacoit Mangal Singh. Ganga himself dies preventing a disaster at the dam. The dam is eventually built bringing greenery to an arid region. His wife bears a son and lives on in the village. 

==Cast==
* Jalal Agha as Ganga Singh
* Simi Garewal as Gauri
* Madhu Chanda Sonki
* Sajjan as Hari Singh
* Prakash Thapa as Mangal Singh
* Kiran Kumar Rashid Khan
* Pinchoo Kapoor
* Maruti Rao

==Reception==
The film was referred to as an "inspirational melodrama" and the music by Jaidev was stated as a "magisterial score", however the film was not a commercial success and flopped at the box-office.    

==Awards==
* Nargis Dutt Award for Best Feature Film on National Integration.   

==Soundtrack==
Composer Jaidev got Asha Bhosle to sing for Do Boond Pani,    for the song "Ja Ri Pawaniya". The lyricists were  Kaifi Azmi, Balkavi Bairagi and M. R. Mukul. The playback singing was provided by Asha Bhosle, Lakshmi Shankar, Parveen Sultana, Meenu Purushottam, Mukesh (singer)|Mukesh, Ambar Kumar, Shrikant Moghe, Bhushan Mehta.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|-
| 1
| Ja Ri Pawaniya Piya Ke Des Ja
| Asha Bhosle
| Kaifi Azmi
|-
| 2
| Bani Teri Bindiya Ki
| Lakshmi Shankar
| Balkavi Bairagi
|-
| 3
| Peetal Ki Mori Gaagri (Happy)
| Parveen Sultana, Minu Purushottam
| Kaifi Azmi
|-
| 4
| Peetal Ki Mori Gaagri (Sad)
| Parveen Sultana
| Kaifi Azmi
|-
| 5
| Mar Gai Mar Gai Re
| Meenu Purushottam, Ambar Kumar, Bhushan Mehta
| Kaifi Azmi
|-
| 6
| Do Boond Pani
| Mukesh
| Kaifi Azmi
|-
| 7
| Sai Naam
| Shrikant Moghe, Ambar Kumar
| M. R. Mukul
|}

==References==
 

==External links==
* 

 

 
 
 
 
 

 