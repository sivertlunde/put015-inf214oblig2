Blind (2011 film)
{{Infobox film
| name           = Blind
| image          = Blind (2011 film) poster.jpg
| caption        = Theatrical poster
| director       = Ahn Sang-hoon  
| producer       = Andy Yoon
| writer         = Choi Min-seok
| starring       = Kim Ha-neul Yoo Seung-ho
| music          = Song Jun-seok
| cinematography = Son Won-ho
| editing        = Shin Min-kyung
| distributor    = Next Entertainment World
| released       =  
| runtime        = 111 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
| film name = {{Film name
 | hangul         =   
 | rr             = Beullaindeu
 | mr             = Bŭllaindŭ}}
}}
Blind ( ) is a 2011 South Korean crime thriller film directed by Ahn Sang-hoon and starring Kim Ha-neul and Yoo Seung-ho in the lead roles. Kim received Best Actress honors at the 48th Grand Bell Awards and the 32nd Blue Dragon Film Awards for her performance.   

The screenplay for Blind won the "Hit By Pitch" project fair held by the Korean Movie Producers Guild in 2009.

==Synopsis==
A missing person case involving a female university student and the victim in a hit and run case appears to be related. Detectives look for a witness.

Min Soo-ah (Kim Ha-neul) used to be a promising cadet at the police academy but after a horrific car accident which killed her surrogate brother and caused her to lose her eyesight, her police career ended. Soo-ah reveals to Detective Jo (Jo Hee-bong) at the police station that on the night of the hit and run case she was picked up by a taxi cab driver. Soo-ah believes the taxi driver may be the perpetrator of the crimes. Initially, Detective Jo doesnt take Soo-ahs claims seriously because she is blind, but when Soo-ah displays her acute senses, the detective starts to believe her.

Detective Jo and Soo-ah then work together to find the taxi cab driver, but all their leads turn up empty. Then another witness comes forward, Kwon Gi-seob (Yoo Seung-ho). Gi-seob is a motorcycle delivery boy who claims to have also witnessed the hit and run incident. Gi-seob emphatically states that the car in question was not a taxi cab, but rather an imported sedan.

Meanwhile, Soo-ah finds herself being stalked by a mysterious man who turns out to be the killer, gynecologist Myung-jin (Yang Young-jo). Soo-ah, while in the car with him, remembered that he had a strong scent, he had a watch on his right hand, and he gave her an iced coffee drink in a glass can. While in the car with him, they hit a bump while in an argument. The body of the dead university student rolls out of the trunk and Myung-jin gets out to examine it. When Soo-ah goes out as well to inspect the damage done, he claims that he hit a dog, but she reasons with him, starting another fight. He leaves her in the rain when another car comes. The reason why she was in the car with him is because she needed a ride back from visiting the orphanage she used to grow up in.
 seeing eye dog, Seul-gi. She reaches into her handbag and sprays the killers eyes with her pepper spray and runs off with Seul-gi. When she reaches the elevator, she thought that shed be safe, but the killer quickly gets in and kills Seul-gi.

When Soo-ah wakes up, she asks for her seeing dog, but Kwon Gi-seob (Yoo Seung-ho) hands her the blood-stained leash. At home she gets a call from an unknown number. The caller warns her away from the case. "You cant see me, but Im watching you." A few days later, Detective Jo finds the killer. They get into a violent fight, in which the detective dies and the killer drives off. Meanwhile, Gi-seob and Soo-ah visit the orphanage again when theyre asked to watch over it while the school director takes the children out. The killer enters the living room and lights a cigarette and listens to some music. Soo-ah, annoyed by the music, goes downstairs to turn it off. She reprimands Gi-seob for playing it, but smells the cigarette smoke. Gi-seob goes upstairs and fights the killer while Soo-ah runs away, reaching the car and breaking its windows with the motion sensor. The killer attacks her but she hits him on the head, making him fall unconscious. The police find Detective Jos body and other evidence implicating Myung-jin as the killer and he is put in jail. Soo-ah is re-admitted to the police academy and graduates, while Gi-seob also enrolls in the police academy.

==Cast==
* Kim Ha-neul as Min Soo-ah
* Yoo Seung-ho as Kwon Gi-seob
* Jo Hee-bong as Detective Jo
* Yang Young-jo as Myung-jin
* Dolly as Seul-gi
* Sa-hee as Jung-yeon
* Kim Mi-kyung as school director
* Kim Soo-jin as young Soo-ah 
* Choi Phillip as detective from Information Section
* Park Bo-gum as Dong-hyun, Soo-ahs younger brother
* Won Pung-yeon as Detective 1
* Jeon Joo-woo as Detective 4
* Kim Kyeong-ik as Doctor 2
* Sung Yoo-bin as young Dong-hyun 
* Baek Ik-nam as owner of car
* Han Yeo-wool as woman having an abortion

==Awards==
2011 48th Grand Bell Awards
* Best Actress: Kim Ha-neul
* Best Screenplay: Choi Min-seok

2011 32nd Blue Dragon Film Awards
* Best Actress: Kim Ha-neul

==Remake==
A Chinese remake titled I Am a Witness starring Yang Mi and Luhan (singer)|Luhan, began filming in 2015. 

==References==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 