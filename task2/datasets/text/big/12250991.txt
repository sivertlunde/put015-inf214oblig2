Sakshatkara
{{Infobox Film name           = Sakshatkara image          =  image_size     =  caption        =  director       = Puttanna Kanagal producer       = B. Mallik writer         = THA.RA.SU screenplay     = Kanagal Prabhakara Sastry starring  Rajkumar  Jamuna  Noted Hindi actorPrithviraj Kapoor[father of Raj kapoor Acted in this movie. music          = M. Ranga Rao cinematography = Srikanth editing        = V. P. Krishna distributor    = Mallik Productions released       = 1971 runtime        =  country        = India language       = Kannada budget         =  preceded_by    =  followed_by    = 
}}
 Jamuna and Prithviraj Kapoor.

== Plot ==
The story revolves around the dangers of superstition, particularly when it concerns marriage and how unscrupulous elements in society could manipulate these beliefs to their benefit and lead to tragic consequences.

==Cast== Rajkumar as Mahesh Jamuna as Uma
* Prithviraj Kapoor as Maheshs father.
* R. Nagendra Rao
* Narasimharaju (Kannada actor)|T. R. Narasimharaju
* Balakrishna (Kannada actor)|T. N. Balakrishna
* Vajramuni
* B. Jayamma

==Soundtrack==
The music for the film was composed by M. Ranga Rao.
{{tracklist
| extra_column    = Singers
| title1          = Olave Jeevana Sakshatkara (Sad version)
| extra1          = P. Susheela, P. B. Sreenivas
| length1         = 5:42
| title2          = Kaadiruvalo
| extra2          = P. Susheela
| length2         = 3:28
| title3          = Janma Janmada
| extra3          = P. B. Sreenivas
| length3         = 3:31
| title4          = Phalisithu
| extra4          = P. Susheela
| length4         = 3:09
| title5          = Olave Jeevana Sakshatkara
| extra5          = P. Susheela
| length5         = 3:31
}}

== External links ==
*  

 
 
 
 
 


 