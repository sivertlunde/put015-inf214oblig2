Whistling in Dixie
{{Infobox film
| name           = Whistling in Dixie
| image_size     =
| image	         = Whistling in Dixie FilmPoster.jpeg
| caption        =
| director       = S. Sylvan Simon
| producer       = George Haight
| writer         = Nat Perrin Wilkie C. Mahoney (additional dialogue, as Wilkie Mahoney) Lawrence Hazard (uncredited) Jonathan Latimer (uncredited)
| narrator       = George Bancroft Guy Kibbee
| music          = Lennie Hayton
| cinematography = Clyde De Vinna
| editing        = Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| distributor    =
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = $388,000  . 
| gross          = $1,345,000 
}}
 crime comedy film, the second of three starring Red Skelton as murder mystery writer and amateur crime solver Wally Benton (aka. The Fox) and Ann Rutherford as his fiancee. The pair are called upon to solve a crime in the American South. The film also re-introduces Rags Ragland, playing dual roles as twins, the mostly-reformed Chester, as well as his villainous brother from the first film.  The film turns into a romantic comedy mystery, complete with death traps, corrupt politicians and lost gold, ending with a frenetic fight at the end between Wally Benton and both of Rags Raglands characters.
 Whistling in the Dark and is followed by Whistling in Brooklyn.

==Cast==
*Red Skelton as Wally "The Fox" Benton
*Ann Rutherford as Carol Lambert George Bancroft as Sheriff Claude Stagg
*Guy Kibbee as Judge George Lee
*Diana Lewis as Ellamae Downs
*Peter Whitney as Frank V. Bailie
*Rags Ragland as Chester Conway
*Celia Travers as Hattie Lee
*Lucien Littlefield as Corporal Lucken
*Louis Mason as Deputy Lem
*Mark Daniels as Martin Gordon
*Pierre Watkin as Doctor
*Emmett Vogan as Radio Producer
*Hobart Cavanaugh as Mr. Panky
==Reception==
According to MGM records the film earned $1,066,000 in the US and Canada and $279,000 elsewhere, making a profit of $542,000. 
==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 