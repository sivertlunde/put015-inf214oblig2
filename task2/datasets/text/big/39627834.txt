Earth Spirit (film)
{{Infobox film
| name           = Earth Spirit
| image          = 
| image_size     = 
| caption        = 
| director       = Leopold Jessner 
| producer       = Richard Oswald
| writer         = Frank Wedekind (play)   Carl Mayer
| starring       = Asta Nielsen   Albert Bassermann   Carl Ebert   Gustav Rickelt
| music          = 
| editing        =
| cinematography = Axel Graatkjær
| studio         = Richard Oswald-Film
| distributor    =  Richard Oswald-Film 
| released       = 33 February 1923
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
}} silent drama play of the same name by Frank Wedekind. It premiered in Berlin on 22 February 1923. 

==Cast==
* Asta Nielsen as Lulu 
* Albert Bassermann as Dr. Schoen 
* Carl Ebert as Schwarz 
* Gustav Rickelt as Dr. Goll 
* Rudolf Forster as Alwa Schoen 
* Alexander Granach as Schigolch 
* Heinrich George as Rodrigo 
* Erwin Biswanger as Eulenber 
* Julius Falkenstein   
* Lucy Kieselhausen   
* Anton Pointner

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 