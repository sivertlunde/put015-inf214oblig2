Blackfish (film)
 
{{Infobox film
| name           = Blackfish
| image          = BLACKFISH Film Poster.jpg
| image_size     = 
| border         = 
| alt            = Black-and-white picture of an orca (killer whale) with the title Blackfish and credits underneath
| caption        = Theatrical release poster
| film name      = 
| director       = Gabriela Cowperthwaite
| producer       = Manuel V. Oteyza Gabriela Cowperthwaite
| writer         = Gabriela Cowperthwaite Eli Despres Tim Zimmermann
| narrator       = 
| starring       = 
| music          = Jeff Beal
| cinematography = Jonathan Ingalls Christopher Towey
| editing        = Eli Despres
| studio         = CNN Films Manny O. Productions
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 83 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = $2.2 million  
}}

Blackfish is a 2013 documentary directed by Gabriela Cowperthwaite. The film premiered at the 2013 Sundance Film Festival on  January 19, 2013, and was picked up by Magnolia Pictures and CNN Films for wider release.
 orca held SeaWorld and the controversy over captive killer whales.

==Synopsis==
  orca involved lifespans of captivity are John Hargrove, other captive whales.

==Development==
 
Cowperthwaite began work on the film after the 2010 death of Tilikums trainer Dawn Brancheau and in response to the claim that the orca had targeted the trainer because she had worn her hair in a ponytail.    Cowperthwaite argued that this claim had been conjecture and that "there had to be more to this story". 

The film premiered at the 2013 Sundance Film Festival on  January 19, 2013, and was picked up by Magnolia Pictures and CNN Films for wider release. 

==Reception==

===Critical response===
On Rotten Tomatoes, a review aggregator, the film has a score of 98% based on 119 reviews with an average rating of 8 out of 10. The sites critical consensus states, "Blackfish is an aggressive, impassioned documentary that will change the way you look at performance killer whales."    On Metacritic, which assigns a weighted average score from on reviews from mainstream critics, the film received an average score of 83 out of 100 based on 33 critics indicating "universal acclaim."  The Deseret News called it "a gripping example of documentary filmmaking at its finest".   

===Box office===
In release for 14 weeks, the film earned $2,073,582 at the North American domestic box office.   

===Response from trainers===
After the films release, former SeaWorld trainer Bridgette Pirtle said the final film was "a complete 180 from what was originally presented to me."    In earlier statements, Pirtle praised the films direction and supported its message.    Mark Simmons, one of Tilikums first trainers, believed few of his interview comments were used " ecause the things I said flew in the face of the movies clear agenda. What I contributed did not support Gabriela or Tim Zimmermans intent with the film."   

Michael Scarpuzzi, the vice president for zoological operations and trainer for SeaWorld San Diego says the film uses Brancheaus death and gruesome details to "not inform the public, but, rather regrettably, because of the desire to sensationalize." He states, "We have altered how we care for, display and train these extraordinary animals. We have changed the facilities, equipment and procedures at our killer whale habitats. The care and educational presentation of these animals at SeaWorld has been made safer than ever. Does Blackfish inform its viewers of that fact? No, it does not." 

In January 2014, the family of the late trainer Dawn Brancheau said neither it nor the foundation named after her were affiliated with the film, and that they did not believe it accurately reflected Brancheau or her experiences. 

===Response from SeaWorld===
SeaWorld Entertainment refused to take part in the production of Blackfish, and later claimed the film was inaccurate,    saying in a statement:
  }}

SeaWorld responded further with an open letter rebutting the claims.    The Oceanic Preservation Society and The Orca Project, a non-profit focusing on orca in captivity, responded with open letters criticizing SeaWorlds claims.       Marine researcher Debbie Giles also offered rebuttals to SeaWorld, finding its assertions inaccurate.   

On December 31, 2013, the Orlando Business Journal posted a poll asking if Blackfish had changed readers opinions on SeaWorld. The majority of votes stated that the film had not. It was later found that 180 out of 328 votes (55%) originated from a single SeaWorld-hosted IP address.  SeaWorld defended the voting, stating that "each of the votes that came from a SeaWorld domain were cast by team members who are passionate about the incredible work SeaWorld does and the experiences our parks provide." 

SeaWorld also created a section of its website titled "Truth About Blackfish," addressing the claims stated above and highlighting what it considered other problems with the film:  

On February 27, 2014, SeaWorld filed a complaint with the U.S. Department of Labor, claiming the Occupational Safety and Health Administration investigator who investigated Brancheaus death had behaved unethically by aiding the filmmakers. Cowperthwaite denied claims of improper collaboration.   

In March 2014, Cowperthwaite rebutted a number of claims on SeaWorlds website and challenged SeaWorld officials to a public debate.   

==Impact==
  Paper Towns, scheduled for a June 2015 release, had scenes featuring SeaWorld cut. Producer Wyck Gofrey explained, "Since   wrote the book  , the documentary   came out. I think its a little less playful to go to SeaWorld now."   

Reaction to the documentary prompted the bands and singers Heart (band)|Heart, Barenaked Ladies, Willie Nelson, Martina McBride, .38 Special (band)|.38 Special, Cheap Trick,  REO Speedwagon, Pat Benatar,  The Beach Boys, Trace Adkins and Trisha Yearwood to cancel their concerts at the "Bands, Brew & BBQ" event at SeaWorld Orlando and Busch Gardens Tampa in 2014.            

SeaWorld announced afterward it had suffered a $15.9 million loss, which CEO James Atchison attributed in part to high ticket prices and poor weather. 

Overall attendance at SeaWorld parks and Busch Gardens declined by 5% in the first nine months of 2013, though it was unclear if the drop in attendance was due to the influence of the film.  SeaWorld claimed attendance figures for its three marine parks — Orlando, San Diego and San Antonio — in the last three months of 2013 were at record levels for that quarter. 
 Greg Ball proposed legislation in New York that bans keeping orcas in captivity.    In March 2014, California State Assemblyman Richard Bloom introduced the Orca Welfare and Safety Act, a bill in California that would ban entertainment-driven killer whale captivity and retire all current whales.    In June 2014, US Congressmen Adam Schiff and Jared Huffman attached an amendment to the Agriculture Appropriations Act, requiring the USDA to update the Animal Welfare Act in regards to cetacean captivity. It passed with "unanimous bipartisan support."  The bill allocates 1 million USD to studying the impacts of captivity on marine mammals. Schiff cited Blackfish as raising public concern. 

After the release of Blackfish, Southwest Airlines came under pressure to end its 26-year relationship with SeaWorld.    Southwest responded that it was aware of concerns and was "engaged" with SeaWorld over them, but that the partnership would continue.  In July 2014, it was announced that the partnership would not be renewed. A press release stated that the break was mutual and based on "shifting priorities".  The petitioning by activists was cited as a possible factor for the split.  

In August 2014 the company announced that attendance and revenue were down about 1-2% for the second quarter of 2014 compared with the second quarter of 2013. Additionally, SeaWorld stock prices dropped by 33%.  The company attributed the decline to the proposed government legislation related to the documentary.   In November 2014, SeaWorld announced that attendance at the parks had dropped 5.2% from the previous year and profits had fallen 28% over that quarter.  As of November 2014, the companys stock was down 50% from the previous year.   

In September 2014, the Rosen Law Firm PA announced an investigation into "potential securities claim" on behalf of SeaWorld investors.  According to the firm, SeaWorld "acknowledged for the first time the negative publicity may have had a hit and may have been why the attendance has been flat for now and the past quarters." The firm will investigate if SeaWorld was aware of the impact and "chose to downplay that as a reason for its performance results."   In September 2014, the Rosen Law Firm filed a class action lawsuit. It alleges that SeaWorld "misled investors by claiming the decrease in attendance at its parks was caused by Easter holiday and other factors" rather than the release of Blackfish and improper practices.   (archive requires scrolldown) 

SeaWorld said in August 2014 that the film had hurt revenues at its San Diego, California, park.    On December 11, 2014, SeaWorld announced that chief executive Jim Atchison would resign, with an interim successor replacing him on January 15, 2015. The companys share price had fallen 44% in 2014.  

==Home media==
Blackfish was released on DVD and Blu-ray Disc on August 26, 2013 in the UK (Region 2, PAL).   Its US release was on November 12, 2013. 

The documentary was broadcast on CNN on October 24, 2013.  After the broadcast, CNN aired an Anderson Cooper special with Jack Hanna, Gabriela Cowperthwaite, Naomi Rose and Jack Hurley. This was followed by a special edition of Crossfire (TV series)|Crossfire with Blackfish associate producer Tim Zimmermann debating Grey Stafford, a conservationist, zoologist, and member of the International Marine Animal Trainers Association.
 UK on November 21, 2013 as part of the Storyville (TV series)|Storyville documentary series.   

==See also==
*Killer whale attacks on humans
*Personhood#Non-human animals|Non-human animal personhood
*Nonhuman Rights Project
*The Whale (2011 film)|The Whale (2011 film)

==References==
 

==External links==
*  
*  
 
 

 
 
 
 
 
 
 
 
 