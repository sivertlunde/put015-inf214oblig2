Morning, Noon and Night (1933 film)
{{Infobox Hollywood cartoon|
| cartoon_name = Morning, Noon and Night
| series = Betty Boop
| image = 
| caption =
| director = Dave Fleischer
| story_artist =  Thomas Johnson 
| voice_actor = Bonnie Poe 
| musician = David Rubinoff and his Orchestra
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = October 6, 1933
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}

Morning, Noon and Night is a 1933 Fleischer Studios animated short film starring Betty Boop, and featuring the overture Ein Morgen, ein Mittag und ein Abend in Wien (Morning, Noon, and Night in Vienna) by Franz Von Suppe.

==Synopsis==
The short opens with a brief live-action segment featuring David Rubinoff and his orchestra.  A badly hangover|hung-over sun (complete with ice-pack on his head) slowly rises over Betty Boops farm.  Bettys farm is a sanctuary for birds, but the sanctuary is soon threatened by the arrival of the Tom Kats Social Club, a group of hungry cats looking for an easy meal.

They chase a helpless chick back to Bettys farm, who alerts Betty to the danger. The cats initially wreak destruction on the farm, and easily overpower Betty. When the sickly rooster finds out whats happening, he quickly turns into a fighter (boxing gloves and all), and pummels the cats. The other birds join in on the beating, and chase away the hapless cats.  The rooster defeats the cats leader and Betty declares him the winner.

==External links==
*  
* 
*   (public domain, MPEG4, 8.9MB)
*   on YouTube
 


 
 
 
 
 


 