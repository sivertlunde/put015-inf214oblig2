Wanted – A Bad Man
{{Infobox film
| name           = Wanted - A Bad Man
| image          = 
| caption        = 
| director       = Oliver Hardy
| producer       = Louis Burstein
| writer         = 
| starring       = Oliver Hardy
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}

Wanted – A Bad Man is a 1917 American comedy film featuring Oliver Hardy. The film was produced by the Vim Comedy Company.

==Cast==
* Ethel Marie Burton - (as Ethel Burton)
* Oliver Hardy - (as Babe Hardy)
* Bud Ross - (as Budd Ross)

==See also==
* List of American films of 1917
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 
 
 