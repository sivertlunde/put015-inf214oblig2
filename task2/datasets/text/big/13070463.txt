The Mystery of Picasso
{{Infobox film
| name           = The Mystery of Picasso
| image          = MysterePicasso.jpg
| image_size     = 
| caption        = 
| director       = Henri-Georges Clouzot
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Claude Renoir
| editing        = Henri Colpi
| distributor    = 
| released       = 1956 (France) 7 October 1957 (NYC)
| runtime        = 78 mins
| country        = France
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Mystery of Picasso ( ) is a 1956 French documentary film about the painter Pablo Picasso, directed by Henri-Georges Clouzot, and showing Picasso in the act of creating paintings for the camera. Most of the paintings were subsequently destroyed so that they would only exist on film, though some may have survived. 

The film begins with Picasso creating simple marker drawings in black and white, gradually progressing to full scale collages and oil paintings.
 Special Jury 1982 Festival.   

This famous art movie wasnt the first documentary showing Picasso painting images on glass plates from the viewpoint of the camera. The Belgian documentary film Visit to Picasso (1949) did it almost seven years earlier.

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 

 
 