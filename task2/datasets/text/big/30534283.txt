Prisoner of Her Past
{{Infobox film
| name           = Prisoner of Her Past
| image          =POHP_Poster.jpg
| caption        = 
| director       = Gordon Quinn
| producer       = Joanna Rudnick Howard Reich Jerry Blumenthal
| writer         =
| music          =
| cinematography = Gordon Quinn
| editing        = Jerry Blumenthal
| distributor    = Kartemquin Films
| released       =  
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Prisoner of Her Past is a 2010 documentary film, produced by Kartemquin Films, that follows the journey of Chicago Tribune music critic Howard Reich as he travels to Europe to discover why his elderly mother, Sonia Reich, believes people are trying to kill her.
 Post Traumatic Stress Disorder.

In order to understand his mothers past, Howard Reich travels to Eastern Europe to discover that Sonia spent her adolescence fleeing the Nazis during World War II.  Meeting some of Sonias distant relatives and childhood friends, Howard begins to unravel what his mother has always refused to speak about.  Having lost most of her family to the Holocaust, Sonia spent five years as a "jungle child", starving and constantly on the run.  Now as Sonia tries to forget her past, Howard attempts to ease her pain and confront the horrors that haunted her.  

Prisoner of Her Past was directed by Gordon Quinn, a founder of Kartemquin Films.  The film was produced by Joanna Rudnick and Chicago Tribune journalist, Howard Reich, with Associate Producer, Zak Piper.  The film was a co-production of Kartemquin Films and the Chicago Tribune.   Prisoner of Her Past was the February 2010 winner of the Accolade Competitions Best of Show Award. 

==References==
 

==External links==
*  
*  
*   with Howard Reich at the Pritzker Military Museum & Library on March 31, 2008

 

 
 
 
 
 
 
 
 

 