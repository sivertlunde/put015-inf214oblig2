Jiggs and Maggie Out West
{{Infobox film
| name =  Jiggs and Maggie Out West
| image =
| image_size =
| caption =
| director = William Beaudine
| producer = Barney Gerard 
| writer = Edward F. Cline    Adele Buffington    George McManus   Barney Gerard 
| narrator = Tim Ryan
| music = Edward J. Kay  
| cinematography = L. William OConnell    
| editing = Roy V. Livingston  
| studio = Monogram Pictures
| distributor = Monogram Pictures
| released = April 23, 1950
| runtime = 66 minutes
| country = United States
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Jiggs and Maggie film series, featuring the adventures of a bickering Irish-American couple. 

==Synopsis==
Jiggs and Maggie inherit a gold mine in a western town, but have difficultly finding it.

==Main cast==
* Joe Yule as Jiggs 
* Renie Riano as Maggie 
* George McManus as George McManus  Tim Ryan as Dinty Moore 
* Jim Bannon as Snake-Bite Carter  Riley Hill as Bob Carter 
* Pat Goldin as Dugan 
* June Harrison as Nora 
* Henry Kulky as Bomber Kulkowich  Terry McGinnis as Cyclone McGinnis  Billy Griffith as Lawyer Blakely

==References==
 

==Bibliography==
* Marshall, Wendy L. William Beaudine: From Silents to Television. Scarecrow Press, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 

 