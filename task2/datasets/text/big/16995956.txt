The Utah Kid (1930 film)
 
{{Infobox film
| name           = The Utah Kid
| image          =
| caption        =
| director       = Richard Thorpe
| producer       =
| writer         = Frank Howard Clark
| starring       = Rex Lease Dorothy Sebastian
| music          =
| cinematography = Arthur Reed
| editing        = Billy Bolen
| distributor    = Tiffany Pictures
| released       =  
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         =
}}
 Western film directed by Richard Thorpe, starring Rex Lease and featuring Boris Karloff.

==Cast==
* Rex Lease as Cal Reynolds
* Dorothy Sebastian as Jennie Lee
* Tom Santschi as Butch
* Mary Carr as Aunt Ada Walter Miller as Sheriff Jim Bentley
* Lafe McKee as Parson Joe
* Boris Karloff as Henchman Baxter
* Bud Osborne as Deputy

==See also==
* Boris Karloff filmography

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 