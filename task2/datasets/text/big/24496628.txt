Endangered Species (1982 film)
{{Infobox film
| name         = Endangered Species
| caption      =
| image	=	Endangered Species FilmPoster.jpeg
| writer       = Alan Rudolph John Binder Judson Klinger Richard Clayton Woods John Considine
| director     = Alan Rudolph
| producer     = Carolyn Pfeiffer
| narrator     =
| cinematography = Paul Lohmann
| music        = Gary Wright
| editing      = Tom Walls
| studio       =
| distributor  = Metro-Goldwyn-Mayer
| released     = September 10, 1982
| runtime      = 97 minutes
| country      = United States
| language     = English
| budget       =
| gross        = $1,474,249
| preceded_by  =
| followed_by  =
}}
Endangered Species is a 1982 science fiction film directed and co-written by Alan Rudolph.

==Plot==
Ruben Castle is a retired New York City cop and a recovering alcoholic. On vacation in Colorado, he becomes involved with a mysterious case of mutilated cattle as well as romantically involved with a female sheriff.

==Cast==
* Robert Urich as Ruben Castle
* JoBeth Williams as Harriet Perdue
* Hoyt Axton as Ben Morgan
* Peter Coyote as Steele
* Dan Hedaya as Peck
* Paul Dooley as Joe Hiatt

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 