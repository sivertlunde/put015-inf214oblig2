Exhibition (2013 film)
 
 
{{Infobox film
| name           = Exhibition
| image          = Exhibition film poster.png
| caption        = Film poster
| director       = Joanna Hogg
| producer       = Gayle Griffiths
| writer         = Joanna Hogg
| starring       = Viv Albertine Liam Gillick Tom Hiddleston
| cinematography = Ed Rutherford
| editing        = Helle le Fevre
| studio         = BBC Films Rooks Nest Entertainment Wild Horses Film Company
| distributor    = Artificial Eye 
| released       =  
| runtime        = 104 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Locarno Film UK on 25 April 2014. 

==Summary==
A contemporary artist couple, D (Albertine) and H (Gillick), have their living and working patterns threatened when their house is put up for sale.

==Cast==
*Liam Gillick as H Viv Albertine as D
*Tom Hiddleston as Jamie Macmillan 
*Harry Kershaw as Estate Agent
*Mary Roscoe as Neighbour Guest

==Production==
===Development===
Writer and director Joanna Hogg and actor Tom Hiddleston previously worked together on Hoggs 2007 film Unrelated and her 2010 film Archipelago (film)|Archipelago.

===Filming===
Filming started in October 2012 and took place over the course of 6 weeks in London.    

==See also==
*Unrelated (2008)
*Archipelago (film)|Archipelago (2010)

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 

 