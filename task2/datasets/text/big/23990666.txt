The Return of Casanova
 
{{Infobox film
| name           = The Return of Casanova
| caption        = Film poster
| image	         = The Return of Casanova FilmPoster.jpeg
| director       = Édouard Niermans (director)|Édouard Niermans
| producer       = Alain Delon
| writer         = Jean-Claude Carrière Arthur Schnitzler (novel)
| starring       = Alain Delon
| music          = Michel Portal  Bruno Coulais
| cinematography = Jean Penzer
| editing        = Yves Deschamps
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = France
| language       = French
| budget         =  gross = 356,908 admissions (France) 
}}

The Return of Casanova ( ) is a 1992 French comedy film directed by Édouard Niermans (director)|Édouard Niermans. It was entered into the 1992 Cannes Film Festival.   

==Plot==
After many years of rambling across Europe the aging Giacomo Casanova is impoverished. He wants to return to the Republic of Venice but he doesnt dare going there directly because he was a fugitive when he left. While he tries to find a way to get a pardon he meets a young lady named Marcelina. The more he shows his affection, the more ostentatiously she rejects him. Even so he doesnt give up on her because her lover Lorenzo has grave gaming debts. In return for the required money Lorenzo tells Casanova about a looming secret rendezvous with Marcelina. Moreover he lets Casanova take his place. Undercover of the night Casanova finally seduces her. Lorenzo later feels his honour was besmirched and demands satisfaction. Casanova kills him in a duel and then goes home to Venice.

==Cast==
* Alain Delon - Casanova
* Fabrice Luchini - Camille
* Elsa Lunghini (as Elsa) - Marcolina 
* Wadeck Stanczak - Lorenzi
* Delia Boccardo - Amelie
* Gilles Arbona - Olivo
* Violetta Sanchez - Marquise
* Jacques Boudet - Abbé Philippe Leroy - the emissary 
* Alain Cuny - Marquis
* Yveline Ailhaud - the female cook
* Sarah Bertrand - the first woman
* Rachel Bizet - Marie
* Sandrine Blancke - Teresina
* Sophie Bouilloux - Lise Gabriel Dupont - the coachman

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 
 