Fireworks, Should We See It from the Side or the Bottom?
{{Infobox film
| name           = Fireworks, Should We See It from the Side or the Bottom?
| image          = Fireworks_dvd_cover.jpg
| writer         = Shunji Iwai
| starring       = Yuta Yamazaki Megumi Okina Takayuki Sorita Kenji Kohashi
| director       = Shunji Iwai
| producer       = Fuji TV
| distributor    = Ace Pictures Nippon Herald Films
| released       = August 12, 1993
| runtime        = 49 minutes
| country        = Japan
| language       = Japanese
| music          = Remedios
}}

Fireworks, Should We See It from the Side or the Bottom? (打ち上げ花火、下から見るか? 横から見るか?), usually called Fireworks, is a 1993 Japanese film by writer/director Shunji Iwai. The film was shot by Koji Kanaya, while its lighting director was Hiroyuki Sumida. It was released theatrically on August 12, 1993 and was shown on Fuji TV on August 26, 2003.

==Plot==
One summer day a group of sixth-grade boys have an argument about whether fireworks are round or flat when viewed from different angles and embark on a journey for the answer during the annual firework festival. Meanwhile, one of their classmates, Nazuna, is troubled by her parents separation and decides to choose one of the boys to run away with.

==Awards==
* 1993 - Directors Guild of Japan New Directors Award

== External links ==
*  

 

 
 
 
 
 


 