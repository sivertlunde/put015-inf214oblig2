Sniper 3
{{Infobox film |
  name           = Sniper 3 |
  image          = Sniper-3-movie-cover-1.jpg |
  caption        = DVD cover |
  producer       = J. S. Cardone Carol Kottenbrook |
  director       = P.J. Pesce |
  writer         = J.S. Cardone Ross Helford |
  based on         = Characters created by Michael Frost Beckner Crash Leyland |
  starring       = Tom Berenger Byron Mann John Doman Denis Arndt Jeanetta Arnette Troy Winbush|
  music          = Tim Jones  |
  cinematography = Michael Bonvillai |
  editing        = Amanda I. Kirpaul |
  distributor    = Destination Films |
  released       =   |
  runtime        = 87 minutes |
  language       = English
}}

Sniper 3 is a 2004 action adventure direct-to-video film starring Tom Berenger, Byron Mann, John Doman and Denis Arndt. It was directed by P.J. Pesce and the third installment in the Sniper (film series)|Sniper film series.

==Plot== William Duffy) to perform a covert operation to eliminate a suspected terrorist kingpin in the Vietnam|Peoples Republic of Vietnam who was providing support for Jemaah Islamiyah. The kingpin turns out to be a thought-dead friend and war-buddy of Becketts, Paul Finnegan (John Doman). Because he views the mission as a very personal one, Beckett demands that he conduct the mission alone, without a spotter.

The real purpose of the mission turns out to be something other than what Beckett was told. Beckett begins to suspect his employers motives are not what they claimed after he is counter-sniped at after attempting to assassinate Finnegan. Beckett undertakes a mission of his own to get to the bottom of the mystery, involving escaping from capture by Vietnamese police, working with an NSA-recruited Vietnamese police operative Quan (Byron Mann) and using his military training to search for Finnegan. By and large the plot focuses on governmental betrayal and resultant feelings of disillusion.

===Rols===
*Tom Berenger as Thomas Beckett
*Denis Arndt as William Avery 
*Byron Mann as Quan
*John Doman as Paul Finnegan
*Troy Winbush as Captain Laraby 
*Jeannetta Arnette as Sydney

==External links==
*  

 
 

 
 
 
 
 
 
 

 