The Mine Foreman (film)
{{Infobox film
| name = The Mine Foreman
| image = 
| image_size =
| caption =
| director = Franz Antel
| producer = 
| writer = Jutta Bornemann   Gunther Philipp   Friedrich Schreyvogel   Franz Antel
| narrator =
| starring = Hans Holt   Josefin Kipper   Wolf Albach-Retty Hans Lang 
| cinematography = Hans Heinz Theyer    
| editing =  Arnfried Heyne 
| studio =  Patria Filmkunst 
| distributor = Löwen-Filmverleih
| released =  3 November 1952
| runtime = 90 minutes
| country = Austria   German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical musical operetta of Isabella and Werner Schlichting.

==Cast==
*  Hans Holt as Max, Herzog von Bayern  
* Josefin Kipper as Prinzessin Luise  
* Wolf Albach-Retty as Andreas Spaun, ein Kavalier  
* Waltraut Haas as Nelly Lampl  
* Grethe Weiser as Clara Blankenfeld, Kammerfrau 
* Gunther Philipp as Medardus von Krieglstein, Adjutant  
* Oskar Sima as Matthieas Lampl, Löwenwirt in Hallstatt  
* Annie Rosar as Stasi, Kellnerin  
* Theodor Danegger as Hofkammeradjunkt Pötzl  
* Helene Lauterböck as Gräfin Amalie von Sensheim  
* Rudolf Carl as Obersteiger aus Berchtesgaden  
* Joseph Egger as Praxmarer, Obersteiger aus Hallstatt  
* Raoul Retzer as Blasius, Hausdiener  
* Walter Janssen as König Ludwig I. von Bayern  

== References ==
 

== Bibliography ==
* Robert Dassanowsky. Austrian Cinema: A History. McFarland, 2005.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 

 

 