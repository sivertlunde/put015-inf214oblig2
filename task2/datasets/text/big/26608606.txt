Black Dogs Barking
{{Infobox film
| name           = Black Dogs Barking 
| image          = BlackDogsBarkingFilmPoster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Mehmet Bahadır Er   Maryna Gorbach
| producer       = Mehmet Bahadır Er 
| writer         = Mehmet Bahadır Er
| starring       = Cemal Toktaş   Volga Sorgu   Erkan Can
| music          = Alp Erkin Çakmak   Barış Diri
| cinematography = Sviatoslav Bulakovskyi
| editing        = Maryna Gorbach
| studio         = Karakirmizi Film
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
}}
Black Dogs Barking ( ) is a 2009 Turkish drama film, written, produced and directed by Mehmet Bahadır Er with co-director Maryna Gorbach, starring Cemal Toktaş as a naive young parking attendant who gets mixed up with the mob in pursuit of his dream of running his own car park. The film, which went on nationwide general release across Turkey on  , won awards at film festivals in Antalya and Ankara and its newcomer directors have been hailed as Turkey’s answer to Martin Scorsese, for their inventive shooting style and authentic ear for the city’s underground slang demonstrated in this their debut film.      

==Production==
The film was shot on location in Istanbul, Turkey.   

==Plot==
Selim’s family has migrated from Anatolia to İstanbul. A naive young man without any special training or expertise, he likes to feed pigeons on the rooftop of the building he lives in. Selim’s best friend, Çaça Celal, is a local tough guy. Selim and Çaça work for a man they refer to as Usta, running car parks in the ritzy neighborhood on the other side of the road. Their greatest dream is to have a car park of their own. In a club that Selim frequents, he meets Mehmet, who makes an offer that will change their lives forever

== Release ==

=== Premiere ===
The film premiered on January 25, 2009 at the International Film Festival Rotterdam.   

After postponing its much anticipated release for months, the film opened across Turkey on  . 

=== Festival screenings ===
* 38th International Film Festival Rotterdam (January 21-February 1, 2009)    
* 28th Istanbul International Film Festival (April 4–19, 2009)
* 35th Seattle International Film Festival (May 21-June 14, 2009)      
* 23rd Wine Country Film Festival (September 17–27, 2009) 
* 36th Flanders International Film Festival Ghent (October 6–17, 2009)   
* 25th Warsaw International Film Festival (October 9–18, 2009) 
* 46th Antalya Golden Orange Film Festival (October 10–17, 2009)      
* 32nd Starz Denver Film Festival (November 12–22, 2009)   
* 4th Bursa International Silk Road Film Festival (November 14–22, 2009)      
* 11th New York Turkish Film Festival (December 2–5, 2009)      
* 21st Ankara International Film Festival (March 11–21, 2010)      
* 21st Munich Turkish Film Days (March 21–29, 2010)   

==Reception==

===Reviews===
Emrah Güler, writing in Turkish Daily News, states that the film "delves into a recurring theme in Turkish cinema for the last couple of years, the existential angst faced by second-generation men whose families have migrated to Istanbul in the hopes of better lives," and recommends it to those "who are curious about the hype surrounding Turkish cinema’s answer to Martin Scorsese and Guy Ritchie," but not to those "who are bored by the recurring theme." 

=== Accolades ===
*   (Won) 
* 21st Ankara International Film Festival (March 11–21, 2010)      
** Best Supporting Actor: Volga Sorgu (Won)
** Cinema Writers Association Best Film Award (Won)

== See also ==
* 2009 in film
* Turkish films of 2009

==References==

 

==External links==
*  
*  

 
 
 
 