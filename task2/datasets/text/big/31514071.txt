Martha Marcy May Marlene
 
{{Infobox film
| name           = Martha Marcy May Marlene
| image          = Martha Marcy May Marlene.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Sean Durkin
| producer       = Antonio Campos Patrick S. Cunningham
| writer         = Sean Durkin John Hawkes Sarah Paulson Hugh Dancy
| music          = Daniel Bensi Saunder Jurriaans
| cinematography = Jody Lee Lipes
| editing        = Zachary Stuart-Pontier This Is That
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 102 minutes  
| country        = United States
| language       = English
| budget         = $1 million 
| gross          = $3.5 million 
}} drama Thriller thriller film John Hawkes, Sarah Paulson, and Hugh Dancy. The plot focuses on a young woman suffering from delusions and paranoia after returning to her family from an abusive cult in the Catskill Mountains. The film contains several references to the music of Jackson C. Frank.

==Plot==
Early one morning, Martha flees from an abusive cult in the Catskill Mountains that is led by an enigmatic leader, Patrick. She phones her sister Lucy, eventually asking for her help. Lucy picks her up from a nearby bus station and takes Martha to her lake house in Connecticut which she shares with her husband, Ted. Martha only tells Lucy that she had been living with her boyfriend in the Catskill Mountains, leaving out all mention of the cult. During the following days, Martha continues to exhibit unusual behavior, with little regard for the norms of conventional American society.

In a flashback, Martha is seen meeting Patrick for the first time. He declares that she "looks like a Marcy May". It is explained to Martha that the group is working toward being self-sufficient on their farm. Patrick rapes her, and afterward another woman assures her that she was "lucky" to have Patrick as her first sex partner. Later, he sings Jackson C. Franks song "Marcys Song" in front of the group, and now addresses Martha as Marcy May. The cult members are shown swimming naked together at a waterfall.

At the lake house, Martha walks into Lucy and Teds room while they are having sex and gets into bed with them. A horrified Lucy explains that this is not "normal" behavior, and Ted is furious about the incident. Martha continues to struggle with the return to her old life.

In another flashback to her time on the farm, Martha introduces a new girl, Sarah, to life on the farm. She explains that they all help out with Katie and Patricks baby. When Sarah comments that all the infants in the community are boys, Martha ominously replies that "he only has boys". Martha later prepares Sarah for her "special night" with Patrick, presumably the same ritual of rape that Martha experienced. The preparation includes drinking a drugged drink. 
 
During another flashback while teaching Martha to shoot, Patrick asks her to kill a sick cat to prove that she is "a teacher and a leader", but she refuses. He then asks her to shoot another cult member; she refuses to do this as well. One night, Martha, Watts, and Zoe break into a house and steal valuables. In another scene, several cult members have sex in one room while Patrick watches from a stairway.

In the present, Martha becomes increasingly paranoid that the cult is watching her. Lucy continues to struggle to understand what has happened to Martha. Ted suggests they seek help for Martha, telling Lucy that she needs to be moved to a mental health facility.

In a flashback, Martha, Watts, and Zoe break into another house but are found by the owner. They started to leave, but Patrick appears and confronts the owner, questioning whether he will call the police. The man orders the group out and promises not to call the police, but the elder female member of the group, Katie, stabs him in the back. They quickly leave, with Martha clearly in shock. Martha is later shown answering the phone, using the name "Marlene Lewis"—a name that all the women use on the phone to conceal their identities. Martha struggles in the aftermath of the attack, and Patrick tries to convince her that death is actually a good thing.

After a nightmare at the lake house, Martha attacks Ted in her confusion. This leads Lucy to confront Martha, making it clear she must leave and informing her that they will pay for her treatment. Martha coldly states that Lucy will be a "terrible mother".

The next morning, Martha goes swimming in the lake and sees a man watching her from the opposite shore. Ted and Lucy then drive Martha to her treatment facility when the same man from the lake runs in front of Teds car. He gets into a car parked on the side of the road and appears to follow them. Martha nervously looks back as they drive on, but doesnt say anything.

==Cast==
* Elizabeth Olsen as Martha John Hawkes as Patrick
* Sarah Paulson as Lucy
* Hugh Dancy as Ted
* Brady Corbet as Watts
* Christopher Abbott as Max
* Michael Chmiel as Scruffy man
* Maria Dizzia as Katie
* Julia Garner as Sarah
* Louisa Krause as Zoe

==Production==
Sean Durkin started writing script of Martha Marcy May Marlene in 2007.  When researching his script, Durkin read about what he calls ‘the big ones’: Jonestown, the Mansons, the Moonies and a bit of David Koresh. But he realised he wanted to make something more experiential than political. Hence he intentionally downplayed the ideology and goals of the cult (led by John Hawkes).  

While researching, he got fascinated by how someone gets into the farm or commune or the group and made a short film of the name Mary Last Seen about it; starring Brady Corbet, who plays cult recruiter Watts in both short and feature films. Mary Last Seen won the award for best short film at the 2010 Cannes Film Festival Directors Fortnight. While Mary Last Seen was about how someone gets into the cult, Martha Marcy May Marlene was about what happens to someone when they get out of it. He made the short to show the world Martha was in & also for sending it out with the script of Martha Marcy May Marlene to potential investors.  The short was selected to Sundance Film Festival and he got a distribution deal with Fox Searchlight from there. 
 DP Jody Lee Lipes were inspired by the films Rosemarys Baby (film)|Rosemarys Baby, 3 Women, Klute, Interiors and Margot at the Wedding. The look of the film was inspired by Margot at the Wedding. 

==Release== premiered at the 2011 Sundance Film Festival in January,    with Durkin winning the festivals U.S. Directing Award for Best Drama.  It also screened in the Un Certain Regard section at the 2011 Cannes Film Festival       and screened at the 36th Toronto International Film Festival on September 11, 2011.    The film received a limited release in the United States on October 21, 2011.

In its opening weekend in limited release, Martha Marcy May Marlene grossed $137,651 in the United States. 

20th Century Fox Home Entertainment released Martha Marcy May Marlene on DVD and Blu-ray Disc|Blu-ray on February 21, 2012. 

==Reception==
The film received highly positive reviews, while Olsens performance as the traumatized Martha met with critical acclaim; the film holds a 90% "fresh" rating on Rotten Tomatoes, with the consensus capsule stating, "Led by a mesmerizing debut performance from Elizabeth Olsen, Martha Marcy May Marlene is a distinctive, haunting psychological drama."  On Metacritic the film has a 76 out of 100 "Metascore".   Christy Lemire of the Associated Press named Martha Marcy May Marlene the best film of 2011.  Roger Ebert gave the film three-and-a-half out of four stars, describing Olsen as "a genuine discovery ... She has a wide range of emotions to deal with here, and in her first major role, she seems instinctively to know how to do that." Eberts only major complaint was that the movies chronological shifts were "a shade too clever. In a serious film, there is no payoff for trickery." 

==Accolades==
  
{| class="wikitable sortable"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Awards
|- style="text-align:center;"
! style="background:#ccc;"| Award
! style="background:#ccc;"| Category
! style="background:#ccc;"| Recipient(s)
! style="background:#ccc;"| Outcome
|-
| rowspan="2" | Alliance of Women Film Journalists   
| Best Breakthrough Performance
| Elizabeth Olsen
|  
|-
| Best Supporting Actor John Hawkes
|  
|- style="border-top:2px solid gray;"
| Austin Film Critics Association
| Best Film
| 
|  
|- style="border-top:2px solid gray;"
| Boston Society of Film Critics Awards
| Best New Filmmaker
| Sean Durkin
|  
|- style="border-top:2px solid gray;"
| Broadcast Film Critics Association Awards
| Best Actress
| Elizabeth Olsen
|  
|- style="border-top:2px solid gray;"
| rowspan="3" | Central Ohio Film Critics Association
| Best Actress
| Elizabeth Olsen
|  
|-
| Breakthrough Film Artist For directing and screenwriting
| Sean Durkin
|  
|-
| Breakthrough Film Artist For Acting
| Elizabeth Olsen
|  
|- style="border-top:2px solid gray;"
| rowspan="4" | Chicago Film Critics Association Awards
| Most Promising Filmmaker
| Sean Durkin
|  
|-
| Most Promising Performer
| Elizabeth Olsen
|  
|-
| Best Actress
| Elizabeth Olsen
|  
|-
| Best Screenplay, Original
| Sean Durkin
|  
|- style="border-top:2px solid gray;"
| rowspan="2" | Denver Film Critics Society
| Best Actress
| Elizabeth Olsen
|  
|-
| Best Breakout Star
| Elizabeth Olsen
|  
|- style="border-top:2px solid gray;"
| Detroit Film Critics Society
| Breakthrough Performance
| Elizabeth Olsen
|  
|- style="border-top:2px solid gray;"
| Florida Film Critics Circle Awards
| Pauline Kael Breakout Award
| Elizabeth Olsen
|  
|- style="border-top:2px solid gray;"
| rowspan="2" | Ghent International Film Festival
| Special Mention
| Elizabeth Olsen
|  
|-
| Grand Prix (Best Film)
| Sean Durkin
|  
|- style="border-top:2px solid gray;"
| rowspan="3" | Gotham Awards
| Best Ensemble Cast John Hawkes, Sarah Paulson, Hugh Dancy, Louisa Krause, Julia Garner, Brady Corbet, Maria Dizzia, Christopher Abbott
|  
|-
| Breakthrough Actress Award
| Elizabeth Olsen
|  
|-
| Breakthrough Director Award
| Sean Durkin
|  
|- style="border-top:2px solid gray;"
| rowspan="4" | Independent Spirit Awards
| Best Female Lead
| Elizabeth Olsen
|  
|-
| Best First Feature
| Antonio Campos (producer), Sean Durkin (director), Patrick Cunningham (producer), Josh Mond (producer), Chris Maybach (producer)
|  
|-
| Best Supporting Male
| John Hawkes
|  
|-
| Producers Award
| Josh Mond
|  
|- style="border-top:2px solid gray;"
| Indiana Film Critics Association Awards
| Best Actress
| Elizabeth Olsen 
|  
|- style="border-top:2px solid gray;"
| Los Angeles Film Critics Association Awards
| New Generation Award
| Sean Durkin, Antonio Campos, Josh Mond, Elizabeth Olsen 
|  
|- style="border-top:2px solid gray;"
| rowspan="4" | Online Film Critics Society Awards
| Best Editing
| Zachary Stuart-Pontier
|  
|-
| Best Lead Actress
| Elizabeth Olsen
|  
|-
| Best Original Screenplay
| Sean Durkin
|  
|-
| Best Supporting Actor
| John Hawkes
|  
|- style="border-top:2px solid gray;"
| rowspan="2" | Phoenix Film Critics Society Awards
| Best Actress in a Leading Role
| Elizabeth Olsen
|  
|-
| Best Supporting Actor
| John Hawkes
|  
|- style="border-top:2px solid gray;"
| San Diego Film Critics Society Awards
| Best Actress
| Elizabeth Olsen
|  
|- style="border-top:2px solid gray;"
| Satellite Awards
| Best Actress in a Motion Picture
| Elizabeth Olsen
|  
|- style="border-top:2px solid gray;"
| rowspan="2" | St. Louis Gateway Film Critics Association Awards
| Best Actress
| Elizabeth Olsen
|  
|-
| Best Supporting Actor
| John Hawkes
|  
|- style="border-top:2px solid gray;"
| rowspan="2" | Sundance Film Festival
| Directing Award (Dramatic)
| Sean Durkin
|  
|-
| Grand Jury Prize (Dramatic)
| Sean Durkin
|  
|- style="border-top:2px solid gray;"
| rowspan="2" | Toronto Film Critics Association Awards
| Best Actress
| Elizabeth Olsen
|  
|-
| Best First Feature
| Sean Durkin
|  
|- style="border-top:2px solid gray;"
| Vancouver Film Critics Circle Award
| Best Actress
| Elizabeth Olsen
|  
|- style="border-top:2px solid gray;"
| Village Voice Film Poll
| Best Actress
| Elizabeth Olsen
|  
|- style="border-top:2px solid gray;"
| rowspan="2" | Washington DC Area Film Critics Association Awards
| Best Actress
| Elizabeth Olsen
|  
|-
| Best Supporting Actor
| John Hawkes
|  
|}

==References==
  Box Office Magazine, September 12, 2011
* Zakarin, Jordan,  , The Huffington Post, September 12, 2011

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 