Le Voyou
{{Infobox film| name = Le Voyou
  | image =
  | caption =
  | director = Claude Lelouch
  | producer = Georges Dancigers   Alexandre Mnouchkine
  | writer = Claude Lelouch   Claude Pinoteau   Pierre Uytterhoeven
  | starring = Jean-Louis Trintignant   Danièle Delorme
  | music = Francis Lai
  | cinematography = Claude Lelouch
  | editing = Marie-Claude Lacambre
  | distributor = United Artists
  | released = November 20, 1970
  | runtime = 120 minutes French
  | country = France / Italy
  | budget =
  | preceded_by =
  | followed_by =
  }}
Le Voyou, also known as The Crook, is a highly stylized French action movie which follows Simon the Swiss during his largest  .
 Best Foreign Direction in 1971.

==Cast==
* Jean-Louis Trintignant as Simon Duroc AKA "The Swiss"
* Danièle Delorme as Janine
* Charles Gérard as Charlot
* Christine Lelouch as Martine
* Charles Denner as Monsieur Gallois
* Amidou as Bill
* Judith Magre as Mme Gallois

==External links==
* 

 

 
 
 
 
 
 
 

 
 