Black Rain (1989 American film)
 
{{Infobox film
| name = Black Rain
| image = Black Rain.jpg
| caption = Theatrical release poster
| director = Ridley Scott
| producer = Stanley R. Jaffe Sherry Lansing Warren Lewis
| starring = Michael Douglas Andy García Ken Takakura Kate Capshaw
| music = Hans Zimmer
| cinematography = Jan de Bont
| editing = Tom Rolf
| distributor = Paramount Pictures
| released =  
| runtime = 125 minutes
| country = United States
| language = English Japanese
| budget = $30,000,000
| gross = $134,212,055
}}
 action Thriller thriller film police officers Japanese underworld. 

==Plot== New York Internal Affairs believes Nick was involved with his partner who was caught taking criminal money in a corruption scandal. Nick is divorced from his wife, who has custody of their two children. Nick also has financial difficulties.

While having a drink at a local Italian restaurant, Nick and his partner Charlie Vincent (Andy García) observe two Japanese men having what appears to be a friendly lunch with some Mafia gangsters. Nick is increasingly suspicious of the group until another Japanese man enters the restaurant with several armed henchmen and seizes a small package at gunpoint from the leader of the Japanese group. He then slashes the mans throat, stabs another in the chest, and then walks out. Nick and Charlie chase and arrest the suspect after he nearly kills Nick.
 extradited to Internal Affairs investigation of him.

When they arrive in Osaka, men identifying themselves as Japanese police immediately meet them on the plane, display a "transfer document" written in Japanese and take Sato into their custody, leaving the plane by the rear exit. As Nick and Charlie are about to get off the plane themselves, another group of police enter from the front and identify themselves in English, indicating that the first "cops" were impostors.

Nick and Charlie are taken to the headquarters of the Osaka Prefecture Police and questioned. They are blamed for Sato’s escape. After much haranguing by Nick, he and Charlie are allowed to “observe” the hunt for Sato. However, the senior police officer emphasizes that they have no authority in Japan and it is illegal for them to carry their guns, which are all confiscated. They are assigned to Masahiro Matsumoto (Ken Takakura).

Throughout the investigation Nick behaves rudely, offending Matsumoto, while Charlie tries to be more polite. Nick also makes contact with an American blond nightclub hostess, Joyce (Kate Capshaw), who explains that the Japanese public, including the giggling hostesses in the club, all believe that Nick and Charlie are not to be taken seriously because they allowed Sato to easily escape from custody, and represent American inefficiency and stupidity. Through her, Nick discovers that Sato is fighting a gang war with a notorious crime boss, Sugai (Tomisaburo Wakayama). Sato used to be a lieutenant for Sugai and now wants his own territory to rule. Sato had traveled to New York to disrupt Sugais meeting with American gangsters about a counterfeiting scheme.

Having joined a police raid of a gang hideout without permission, Nick takes some $100 bills. The next day Matsumoto explains they have dishonored themselves, him and the police force by this theft, which has been reported back to America; Nick just claims he ought not to have "snitched" to his superiors, and shows Matsumoto that the money was counterfeit by burning one of the bills.

Late one night, Nick and Charlie walk back to their hotel slightly drunk and unescorted, despite previous warnings about their safety from Matsumoto. They are harassed by a young punk on a motorcycle, and it seems to be a joke until the motorcyclist steals Charlie’s raincoat and leads Charlie into an underground parking garage. Nick follows, shouting for Charlie to come back, but is separated from his partner by a security gate. The unarmed Nick watches in horror as Sato and several of his Bōsōzoku gang members briefly torture Charlie using swords and knives, before Sato beheads him. Distraught, Nick is comforted by Joyce at her apartment. Matsumoto arrives with Nick’s belongings, including his NYPD badge, which Nick gives to Matsumoto, and Charlie’s service pistol, which Nick keeps for himself.

Matsumoto and Nick trail one of Sato’s operatives, a woman; overnight the policemen discuss their different cultures, and Nick admits to Matsumoto that he had taken some money in New York. In the morning the woman retrieves from a bank strongbox a sample counterfeit note (printed only on one side), which she passes to one of Sato’s gang. Nick and Matsumoto tail the man to a steel foundry where they find that Sato is meeting with Sugai, and discovers that the package that Sato had stolen in New York contains a printing plate for the American $100 bill. Nick intervenes when Sato leaves the meeting and a gunfight ensues. Sato escapes again when Nick is arrested by the swarming police for waving a gun in public, and told he will be sent back to New York in disgrace.
 bombing of Hiroshima and the aftermath of World War II. Nick suggests a deal where Sugai can use Nick to retrieve the stolen plate from Sato, leaving Sugais reputation and hands clean.

Sugai drops Nick at the outskirts of a remote farm where a meeting of the  , which he duly does. As he takes his position next to Sugai, he stabs the elder gangster in the hand and escapes with both the plates, prompting a gunfight between all Sugai’s and Sato’s men. Sato escapes the fight on a dirt bike with Nick close behind. Nick is able to spill Sato off his bike and the two fight briefly, until Nick gains the advantage and decides whether or not to kill Sato for Charlie and for all the humiliation he has suffered.

The film ends with Matsumoto and Nick walking a handcuffed Sato into police headquarters to the amazement of everyone and later receiving commendations, which Nick accepts gratefully. Before boarding his flight home, Nick thanks Matsumoto for his assistance and friendship, and gives him a dress shirt in a gift box. Underneath it, Matsumoto finds the two counterfeit printing plates.

==Cast==
*Michael Douglas  as Nick Conklin  
*Andy García  as Charlie Vincent
*Ken Takakura  as Masahiro Matsumoto  
*Kate Capshaw  as Joyce
*Yusaku Matsuda  as Koji Sato
*Shigeru Kōyama  as Ohashi John Spencer  as Oliver
*Guts Ishimatsu  as Katayama
*Yuya Uchida  as Nashida
*Tomisaburo Wakayama  as Sugai
*Miyuki Ono  as Miyuki
*Luis Guzmán  as Frankie (as Luis Guzman)
*John Costelloe  as The Kid (as John A. Costelloe)
*Stephen Root  as Berg
*Richard Riehle  as Crown
*Rikiya Yasuoka as Sugais man
*Professor Toru Tanaka as Sugais man
*David Tao as Japanese Police

==Production==
Chinese iconic actor Jackie Chan was first approached to choreograph action scenes and play a small part as a bad guy but decided the role did not match his values or image.  Japanese musician Ryuichi Sakamoto contributed the song "Laserman" to the films soundtrack. 

The film began shooting in November 1988 and ended in March 1989. Japanese actor Yusaku Matsuda, who played Sato, died of bladder cancer shortly after the films completion.  Director Ridley Scott dedicated the film to his memory.

The high cost and red tape involved in filming in Japan prompted director Scott to declare that he would never film in that country again.   Scott was eventually forced to leave the country and complete the final climactic scene (which included American character actor Al Leong) in Napa Valley, California.
 Black Hawk Matchstick Men.

==Release==

===Box office===
In its opening weekend, Black Rain grossed $9.6 million in 1,610 theaters in the United States and Canada, ranking #1 at the box office.  It stayed at the #1 spot for 2 more weeks.  The film grossed a total of $46.2 million in the United States and Canada and $88 million in other territories for a worldwide gross of $134.2 million. 

===Reception=== Best Sound Kevin OConnell, Best Sound Effects Editing. (Milton Burrow and William Manger)    

==Locations==
Large parts of Black Rain were filmed in Osaka, although some of the locations have changed somewhat since the late 1980s when production took place. The original intention of Ridley Scott was to film in the Kabukicho nightlife district of Shinjuku, Tokyo. However, the Osaka authorities were more receptive towards film permits so the similarly futuristic neon-infused Dotonbori in Namba was chosen as the principal filming location in Japan.

An aerial shot of Osaka bay at sunset with the estuaries of the Yodogawa, Kanzakigawa and Ajigawa rivers frames the opening sequence of the arrival into Japan.

The main filming location in Osaka is by the Ebisubashi bridge. The futurist Kirin Plaza building (architect Shin Takamatsu, built 1987), the Ebisubashi and the famous neon wall overlooking the Dotonbori canal creates the Blade Runner|Bladerunner-esque mise-en-scène.

Umeda, Osakas northern centre, is represented by the first floor shopping mall concourse of Hankyu Umeda station Terminal Building. Resembling a futuristic neo-gothic nave from a cathedral, this is where Charlie Vincents (Andy Garcia) jacket is stolen by a bosozoku biker. This concourse was redeveloped in the late 2000s. Escalators entering down into the JR Umeda station are used as the next part of the fateful chase scheme.

The now removed Shinsaibashi bridge (dismantled in 1995), Osaka Municipal Central Wholesale Market, Nippon Steel Works in Sakai City (south Osaka), Kyobashi, the elevated Hanshin Expressway, Osaka Castle and Nanko Port Town also feature briefly.

In New York City, the 1964 World Expos Unisphere opens the film, followed by Nick Conklin (Michael Douglas) riding over the Queensborough Bridge. The illegal bike race between Nick and an anonymous challenger took place from underneath the west underside of the Brooklyn Bridge north to the Manhattan Bridge.

==References==
 

==Further reading==
*An academic comparative study of Black Rain (American film) and Black Rain (Japanese film), entitled "Nuclear Bomb Films in Japan and America: Two Black Rain Films" by Yoko Ima-Izumi included in Essays on British and American Literature and Culture: From Perspectives of Transpacific American Studies edited by Tatsushi Narita (Nagoya: Kougaku Shuppan, 1907)

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 