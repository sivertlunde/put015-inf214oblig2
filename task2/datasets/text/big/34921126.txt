Great Expectations (2012 film)
 
{{Infobox film
| name           = Great Expectations
| image          = GreatExpectations2012Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster Mike Newell
| producer       = David Faigenblum Elizabeth Karlsen Emanuel Michael Stephen Woolley David Nicholls
| based on       =  
| starring       = Jeremy Irvine Robbie Coltrane Holliday Grainger Helena Bonham Carter Ralph Fiennes
| music          = Richard Hartley John Mathieson Tariq Anwar
| studio         = BBC Films Lionsgate
| released       =  
| runtime        = 128 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $6.2 million 
}}
 novel of Mike Newell, David Nicholls, and stars Jeremy Irvine, Helena Bonham Carter, Holliday Grainger, Ralph Fiennes and Robbie Coltrane. It was distributed by Lions Gate Entertainment|Lionsgate.

Newell adapted the screenplay after being asked to work on it by producers Elizabeth Karlsen and Stephen Woolley, with whom he had worked on And When Did You Last See Your Father?. Helena Bonham Carter was asked to appear as Miss Havisham by Newell, and accepted the role after some initial apprehension, while Irvine was initially intimidated by the thought of appearing on screen as Pip.
 premiere of the film closed the BFI London Film Festival in 2012, although it had already been previewed earlier in the year at the Toronto Film Festival. It was released in the UK on 30 November 2012.

==Synopsis==
 

==Cast==
 War Horse, which Irvine described as his big break, having previously appeared in minor roles with the Royal Shakespeare Company.  He found the role intimidating, but said that he "wanted to make my character Pip stronger and more driven than how hes been played in adaptations before".   
** Toby Irvine, the younger brother of Jeremy Irvine, as young Pip 
* Helena Bonham Carter as Miss Havisham, wealthy spinster who has taken up a life of seclusion following the loss of her love and Pip suspects is his benefactor. Bonham Carter was concerned about playing the role of Miss Havisham and questioned Mike Newell when he phoned her to talk about taking the part. She was worried about the typical appearance of the character, describing her as a pensioner in a bridesmaids dress. Newell assured her that she was the right age,    as the character in the book is in her forties,    and she accepted the role.  The Invisible Woman. 
* Robbie Coltrane as Mr. Jaggers, a lawyer responsible for handling Pips money. Great Expectations was Coltranes first Dickens-related role. He had been offered the role of Mr. Micawber in an adaptation of David Copperfield some years previously, but was unable to take it. Coltrane described Jaggers as a "heartless bastard", and was pleased to be able to deliver the line to Pip about "great expectations", which took several takes to perfect. 
 
* Jason Flemyng as Joe Gargery, Pips brother-in-law and legal guardian.  Flemyng was pleased to be able to appear in a film which he could take his children to see, and thought itd be great for the Christmas period. 
* Holliday Grainger as Estella (Great Expectations)|Estella; Newell encouraged Grainger to bring sensuality to her role.  Grainger has said in interviews that the role was the one which she has coveted more than any other.  She had first read the book at the age of 15, and found that it took a second reading to understand the character. Grainger had previously worked with Bonham Carter in Magnificent 7, a 2005 BBC drama. 
** Helena Barlow as young Estella
* Ewen Bremner as Wemmick
* Sally Hawkins as Mrs. Joe
* David Walliams as Uncle Pumblechook, Joe Gargerys uncle, and provides a route for Pip to be in touch with Miss Havisham.<ref

name=pumblechook>   Walliams brought his mother along to the films premiere. 
* Tamzin Outhwaite as Molly
* Daniel Weyman as Arthur Havisham
* Jessie Cave as Biddy
** Bebe Cave as young Biddy
** Edward Fleming as Charles Pocket
* Olly Alexander as Herbert Pocket
* Ben Lloyd-Hughes as Bentley Drummle

==Production==

===Development=== novel of David Nicholls adaptation for One Day, film in 1946 version, directed by David Lean. 
 Mike Newell was looking to develop Dickens Dombey and Son for the screen, but after it didnt go ahead, he was told about Nicholls script. The two worked together on further developing the screenplay and finding the funding for the film.  Nicholls thought there was a problem with choosing the ending for the film, as Dickens wrote both a downbeat ending and a more positive version. He described their solution as, "What we’ve tried to do is to make it work as a love story without sentimentalising the book",  having criticised the ending of the David Lean version of the film.   

===Filming=== Gillette on A4 road was used as the main filming location. The interior was transformed into a Victorian era street scene. 
 St Thomas Swale Nature Oare and Elmley Marshes, The Historic Dockyard in Chatham and Thames and Medway Canal.
 

==Release== 12A rated release. 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 