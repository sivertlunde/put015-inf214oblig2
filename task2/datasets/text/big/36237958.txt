Scents and Sensibility
{{multiple issues|
 
 
}}

Scents and Sensibility is a 2011 film that centers on a once wealthy family who begin struggling to make ends meet after their father is imprisoned for investment fraud. It is a modern day adaption of Sense and Sensibility.   

==Summary==
Scents and Sensibility is a modern day adaption of the novel "Sense and Sensibility" by Jane Austen.  This 2011 version centers on a wealthy family who begin struggling to make ends meet after their father is imprisoned for investment fraud.

==Summary==

Sisters Elinor and Marianne Dashwood live a charmed life along with their mother and ailing sister, enjoying the benefits of their beloved fathers wealth in a luxurious lifestyle. But all of that changes when their father is indicted by the FBI for swindling millions. When they recover from the shock of his crimes, they find their family fortune is gone and the Dashwood name and reputation are ruined from the recent scandal.  Left alone to struggle to stay afloat, the Dashwood girls try to make it on their own in life, in work, and in love.

From the start, the girls find it nearly impossible to find a good, well-paying job.  Interviews are terminated when possible employers see their last name on the job applications.  Eventually Elinor lands a rough job as a custodian at a posh beauty spa and Marianne is hired as a copy girl at a large firm, but only when she applied with her mothers maiden name. Love also becomes a big challenge for Elinor and Marianne.  After their fathers imprisonment Mariannes boyfriend John Willoughby takes off for a job in Europe, leaving her to cope alone.  Its in new jobs that the sisters encounter romance.  Marianne captivates Brandon, the office boss, and is soon meeting him for lunch breaks in flower filled parks.  Elinors sense of humor interests her bosses brother Edward Farris, a patent lawyer.

While the sisters toil for meager wages, Elinor introduces the spa patrons to a unique healing lotion that Marianne concocts in their kitchen from flowers.  The sale of this lotion has the potential to save the familys finances, but all of those hopes are dashed when Fran Ferris, the evil spa owner, steals some lotion in order to have it copied and sold for profit to save her own failing business.  Frans plan involves the untrustworthy John, who was not in Europe after all, rather he was romancing another unsuspecting young heiress.  Together their plan fails, as Edward puts a stop to the plot by exercising his legal skills, bringing back to the sisters the formula for their own profit.  In the end, the sisters regain financial stability from the sale of the lotions formula, and they end up with the men who had admired them all along.

==Cast== Ashley Williams as Elinor Dashwood
*Marla Sokoloff as Marianne Dashwood
*JJ Neward as Fran Farris
*Brad Johnson as Edward Farris
*Nick Zano as Brandon
*Danielle Chuchran as Margaret Dashwood
*Jaclyn Hales as Lucy
*Jason Celaya as John Willoughby

== Location ==
This story was filmed in Utah against a snowy mountain backdrop.  Of particular interest are the gardens that Marianne and Brandon visit.  These are located at Thanksgiving Point in Lehi, Utah.

== References ==
 

==External links==
*  
* 
*  

== References ==
 

==External links==
*  
* 

 