Camille Claudel (film)
{{Infobox film
| name           = Camille Claudel
| image          = Camille_claudel_aff.jpg
| caption        = Film poster
| director       = Bruno Nuytten
| producer       = Isabelle Adjani Christian Fechner
| writer         = Bruno Nuytten Marilyn Goldin
| starring       = Isabelle Adjani Gérard Depardieu
| music          = Gabriel Yared Claude Debussy(Non-original music)
| cinematography = Pierre Lhomme
| editing        = Joëlle Hache Jeanne Kef  Gaumont
| released       =  
| runtime        = 175 minutes
| country        = France
| language       = French
| gross          = $3,331,297 (USA)  http://boxofficemojo.com/movies/?id=camilleclaudel.htm 
}}
 sculptor Camille Claudel. The movie was based on the book by Reine-Marie Paris, granddaughter of Camilles brother, the poet and diplomat Paul Claudel. It was directed by Bruno Nuytten, co-produced  by Isabelle Adjani, and starred her and Gérard Depardieu. The film had a total of 2,717,136 admissions in France.  http://www.jpbox-office.com/fichfilm.php?id=5711 

==Synopsis==
The film recounts the troubled life of French sculptor Camille Claudel and her long relationship with legendary sculptor Auguste Rodin. Beginning in the 1880s with a young Claudels first meeting with Rodin, the film traces the development of their intense romantic bond. The growth of this relationship coincides with the rise of Claudels career, helping her overcome prejudices against female artists. However, their romance soon sours, due to the increasing pressures of Rodins fame and his love for another woman. These difficulties combine with her increasing doubts about the value of her work drive Claudel into an emotional tumult that threatens to become insanity.

==Cast==
* Isabelle Adjani as Camille Claudel
* Gérard Depardieu as Auguste Rodin
* Laurent Grévill as Paul Claudel
* Alain Cuny as Louis-Prosper Claudel
* Madeleine Robinson as Louise-Athanaïse Claudel
* Philippe Clévenot as Eugène Blot
* Katrine Boorman as Jessie Lipscomb
* Maxime Leroux as Claude Debussy
* Danièle Lebrun as Rose Beuret

==Awards==
* 1989 - nominated for two Academy Awards
** Academy Award for Best Actress
** Academy Award for Best Foreign Language Film Best Film Best Actress
* 1989 - Isabelle Adjani received the Silver Bear for Best Actress at the 39th Berlin International Film Festival   

==See also==
*Camille Claudel 1915
*Mental illness in films

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 