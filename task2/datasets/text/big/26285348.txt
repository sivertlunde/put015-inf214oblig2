Wälsungenblut
 
{{Infobox film
| name           = Wälsungenblut
| image          = Wälsungenblut.jpg
| image size     = 
| caption        = 
| director       = Rolf Thiele
| producer       = Franz Seitz, Jr.
| writer         = Ennio Flaiano Erika Mann Thomas Mann Franz Seitz, Jr.
| starring       = Michael Maien
| music          = 
| cinematography = Wolf Wirth
| editing        = Ingeborg Taschner
| distributor    = 
| released       = 21 January 1965
| runtime        = 86 minutes
| country        = Germany
| language       = German
| budget         = 
}}
  is a 1965 German drama film directed by Rolf Thiele, based on a Thomas Mann novella of the same name. It was entered into the 15th Berlin International Film Festival.   

The title refers to the ill-fated Völsung clan as told in the Völsunga saga; the roles of Siegmund and Sieglinde refer to Sigmund and Signy as depicted in Richard Wagners opera Die Walküre.

==Cast==
*   as Siegmund Arnstatt
* Elena Nathanael as Sieglinde Arnstatt
* Gerd Baltus as Lieutenant Beckerath
*   as Rittmeister Kunz Arnstatt
* Rudolf Forster as Count Arnstatt
* Margot Hielscher as Countess Isabella Arnstatt
* Ingeborg Hallstein as Countess Märit Arnstatt
* Heinz-Leo Fischer as Wendelin, a servant
* Karl-Heinz Peters as Florian, a servant
* Christoph Quest as Von Gelbsattel, an officer
* Rudolf Rhomberg

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 