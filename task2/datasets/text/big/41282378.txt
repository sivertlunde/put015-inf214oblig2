Angry Babies in Love
{{Infobox film
| name = Angry Babies in Love
| image =
| caption = 
| alt =
| director = Saji Surendran
| producer = Darshan Ravi Bhavana Parvathy Nair
| writer = Krishna Poojappura
| music = Bijibal
          Rajeev Alunkal(lyrics)
| cinematography = Anil Nair
| editing = 
| studio = Dimac Creations
| distributor = LJ Films
Popcorn Entertainments Australia
| released =  
| runtime =
| country = India
| language = Malayalam
| budget =
| gross =
}} Bhavana and Parvathy Nair .  The film, produced by Darshan Ravi under the banner of Demac Creations, has music composed by Bijibal and cinematography by Anil Nair.  The film was released on June 14, 2014 and received mixed reviews from critics. After completing 50 days at the box office the film officially got declared as a hit. 

==Plot==
Jeevan Paul is a still photographer, who runs a studio in a village in Idukki district|Idukki. By accident, he meets Sarah Thomas, a girl who is from a good family and they fall in love. Sarahs family oppose her relationship with Jeevan, so they elope and move away, settling in Mumbai.  A year later they appear in court seeking a divorce. The court orders them to stay together for another six months. The plot is notable for its lack of originality. 

==Cast==
* Anoop Menon as Jeevan Paulodeldo Bhavana as Sarah Thomas
* Nishanth Sagar as Anwarsammyhiggins
* Parvathy Nair as Parowhat
* P. Balachandran as Madhavanpoda
* Noby Tharian
* Kalabhavan Shaju
* Anusree as Selvi
* Joju George as Alex Maliyekkal Muktha
* Aditi Ravi as Maria

==Critical reception==
Sify wrote, "Angry Babies may not be a great comedy, but it is not too bad either, for its target audience. The script looks genuinely shaky but the director has packaged the film in a decent manner".  The Times of India gave the film 3 stars out of 5 and wrote, "The film will certainly come across as a pleasant watch for families who would enjoy certain situations in the film, but as a mass entertainer, it might fall a bit short".  Nowrunning.com gave 2 stars out of 5 and wrote, "Playing out on the trouble that brews in a marital paradise, Angry Babies sings a familiar tune, is sporadically funny and makes little attempts whatsoever to take a detour from a much beaten path". 

==References==
 

==External links==
*  

 
 
 
 
 


 
 