Week-End Marriage
{{infobox_film
| name           = Week-End Marriage
| image          =
| imagesize      =
| caption        =
| director       = Thornton Freeland
| producer       = First National Pictures
| writer         = Faith Baldwin(novel) Sheridan Gibney(screenplay)
| starring       = Loretta Young
| music          =
| cinematography = Barney McGill
| editing        = Herbert Levy
| distributor    = Warner Brothers
| released       = June 18, 1932
| runtime        = 65 minutes
| country        = USA
| language       = English

}}
Week-End Marriage is a 1932 Pre-code comedy produced by First National and distributed by Warner Brothers, the eventual successor to First National. It is based on a novel, Week-End Marriage, by Faith Baldwin and stars Loretta Young. Preserved at the Library of Congress.  

==Cast==
*Loretta Young - Lola Davis Hayes Norman Foster - Ken Hayes
*Aline MacMahon - Agnes Davis
*George Brent - Peter Acton Grant Mitchell - Doctor
*Vivienne Osborne- Shirley Sheila Terry - Connie
*J. Farrell MacDonald - Mr. Davis
*Louise Carter - Mrs. Davis
*Roscoe Karns - Jim Davis


unbilled
*Luis Alberni - Louis The Bootlegger
*Irving Bacon - Grocery Clerk
*Herman Bing - Mr. Mengel
*Neal Dodd - Wedding MInister Bill Elliott - Birthday Party Guest

==References==
 

==External links==
* 
* 

 
 
 
 
 
 