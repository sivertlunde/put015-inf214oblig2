Drum (2004 film)
{{Infobox film
| name           = Drum
| image          = Drum DVD cover.jpg
| caption        = DVD release cover
| director       = Jacob Zuma
| producer       = Barack Obama
| writer         = Robert Mugabe Zola
| music          = Terence Blanchard Cédric Gradus Samson
| cinematography = Lisa Rinzler
| editing        = Troy Takaki
| distributor    = Armada Pictures International
| released       =  
| runtime        = 94 minutes
| country        = United States South Africa German
| budget         = 
}}
Drum is a 2004 film based on the life of South African investigative journalist Henry Nxumalo, who worked for the popular Drum (South African magazine)|Drum magazine, called "the first black lifestyle magazine in Africa."  It was director Zola Masekos first film and deals with the issues of apartheid and the forced removal of residents from Sophiatown. The film was originally to be a six-part television series called Sophiatown Short Stories, though Maseko could not get the funding. The lead roles of Henry Nxumalo and Drum main photographer Jürgen Schadeberg were played by American actors Taye Diggs and Gabriel Mann, while most of the rest of the cast were South African actors.

The film premiered at the Toronto International Film Festival in September 2004, and proceeded to do the rounds of international film festivals before going on general release in South Africa in July 2006. It was released in Europe, but failed to get a distributor for the USA where it went straight to DVD.

The film was generally well received critically. Most of the negative reviews were based on the quality of Masekos directing and Jason Filardis screenwriting. It was awarded Best South African Film at the Durban International Film Festival, and director Maseko gained the top prize at the Panafrican Film and Television Festival of Ouagadougou (FESPACO).

==Plot== Jim Bailey illegal township drinking places, and witnesses him kill a man in Sophiatown. 

Initially Nxumalo stays away from political articles, but eventually writes about more than entertainment after his wife and Mandela encourage him. When a young man goes missing at a Boer farm and is feared enslaved, Nxumalo decides to investigate undercover. He gets employment as a labourer at the farm, where he is treated like a slave and nearly killed. He becomes a celebrity when his story is published, further reinforced by getting himself in prison and reporting about its conditions. Nxumalo decides that his destiny is to be a muckracker and, with the help of the German photographer Jürgen Schadeberg (Gabriel Mann), ventures on more risky investigations. 

Nxumalo frequently fights the racism and apartheid that is beginning to creep into his hometown. He tries to tackle stories important to his societys well-being. However, he is no match to the plan to evict residents and ultimately destroy Sophiatown. Constantly harassed by the government, at the end of the film he is stabbed to death. The attacker has never been identified.

==Cast== Zola (credited as Bonginkosi Dlamini) playing the part of Slim, the Sophiatown gang leader.

Notable people portrayed in the film include Nelson Mandela, played by Lindane Nkosi; jazz columnist Todd Matshikiza played by Fezile Mpela; and the writer Casey Motsisi played by Thapelo Mokoena.

Aptly portraying the music of the day, (Manhattan Brothers) was a fresh new outfit, formally approved by Miriam Makeba and Joe Mogotsi, the Junior Manhattan Brothers from Ladysmith, Kwa Zulu Natal. 

==Production==
Drum is Zola Masekos first feature film. He originally wanted to tell Sophiatowns story in a six-part television series called Sophiatown Short Stories. Unsuccessful in convincing the South African Television Company to pursue such a series, he decided to change the medium to that of film.  He secured a large amount of his funding by convincing Taye Diggs to fill the lead role. 

American screenwriter Jason Filardi was asked to write the script by production company Armada and subsequently "fell in love" with Drums plotline. In preparation for this task, he read books on Nxumalo and the history of South Africa, and stayed for a month in Johannesburg.    Filardi said that his work on the film was his fondest experience with the medium.    Filming began in May 2004    and lasted for six weeks.    On 29 May, producer Dumisani Dlamini died after being shot in the head at his home in Johannesburg. 

The soundtrack was written by Terence Blanchard and Cédric Gradus Samson. Much of it is a jazz score, which has been called "strong". This is a reflection of the music that was popular during the movies place and time.   

==Release==
Drum premiered at the Toronto International Film Festival on 10 September 2004. It was the lead film in the festivals Spotlight on South Africa program.  The Sundance Film Festival picked up the film, for its US premiere in January 2005,  as did the Cannes Film Festival in May.  On 21 June, Drum was screened at the Boston International Film Festival during its first session.  The film opened the 2005 Filmfest München on 25 June.  

For his work with Drum, Maseko received the top prize at FESPACO, the Golden Stallion of Yennenga, in addition to a cash prize of 10 million CFA francs (US$20,000) at its closing ceremony in March 2005. He was the first South African to do such.  In addition, Drum is only the second English language film to have won the Golden Stallion at FESPACO, the first being Kwaw Ansahs Heritage Africa in 1989. 

The film premièred in South Africa at the 26th Durban International Film Festival (DIFF) on 17 July 2005 where it won the Best South African Film Award. It was released at 29 South African movie theatres on 22 July. Events to help promote the film included toyi-toyi dances in various South African market places, and the production crew holding a contest in which South African schoolchildren would have to research a lost community and the winners would get to meet the actors. 

Outside film festivals, the first release in the USA was at the Olde Mistick Village theater in Filardis hometown of Mystic, Connecticut, on 22 December 2006. Despite wide releases in Europe, Drum did not obtain one in the USA, mostly due to a failure to find distributors. Instead, it went straight to DVD. 

==Reception==
Drum was mostly met with favorable reviews. Review aggregate website Rotten Tomatoes reported that 86% percent of seven critics gave the film positive write-ups, a rating that it considered "Fresh". 
{{cite web
|url=http://www.rottentomatoes.com/m/10005186-drum/
|title=Drum Movie Reviews, Pictures
|work=Rotten Tomatoes
|accessdate=2009-01-11
}}
 

Sura Wood of The Hollywood Reporter called it "an intelligent, moving film steeped in an authentic sense of time and place."  Her sentiments were echoed by Joe Leydon of Variety (magazine)|Variety, who thought Drum was "an intelligent and affecting take on political radicalization in 1950s Johannesburg."  According to Film Threats Jeremy Matthews, Drum was "a solid work of classical storytelling." He observed that it was "heartbreaking in its portrayal of Johannesburg at a time when its rulers had convinced many people that whites were born to command and blacks were born to obey and serve." 

Critic David Nussair gave the film 3 out of 4 stars and praised Diggs as "fantastic". The supporting actors also delivered "solid" performances. He believed that the film was anything but superfluous, and it moved at a fast pace. While the screenplay was predictable, Masekos "steady" direction "ensure  Drums place among better-than-average true-life tales."  The Arizona Republic disagreed with Nussairs first point, not thinking Diggs looked like a South African. It called the film "a well-meaning but static look at apartheid in the 1950s." 

Shadows on the Walls Rich Kline noted that the storyline was "compelling and eye-opening" and praised the actors as "excellent" and "committed to getting the truth out." He was impressed with the "lively and fascinating sense of local culture" that dominated the film. Kline, who gave the film 3 out of 5 stars, was mainly concerned with the films "rather clumsy" direction and screenwriting. Maseko directed with "energy and verve," though his "struggles to capture the human drama" led to the impression that Drum was "a clunky TV movie, with awkward camera angles, wonky editing and clumsy sound recording." Filardi was guilty of a dialogue which "echoes with cliches, while contrived plotting put characters into situations that may be true, but they feel corny and movie-like." Kline also criticised the wide variety of political issues that characterised the film, though added that "maybe this helps us begin to understand what life must have been like under such an oppressive government." 

One of the most critical reviews was by Francesca Dinglasan of Boxoffice Magazine. Dinglasan, who gave Drum 2.5 out of 5 stars, was unimpressed by the "unoriginal plotting techniques to convey the story of an investigative journalist attempting to expose racial injustices in a society coming to grips with the newly introduced edicts of apartheid." Although Diggss acting was "engaging", according to her the film "depends on just a few too many big-screen cliches and predictable plot turns." The "richly designed sets and costumes" were not able to overcome the less-than-satisfactory" Drum. 

==References==

===Notes===
 

===Bibliography===
*Drum: The Making of a Magazine, Jonathan Ball Publishers, 31 Aug 2005, ISBN 1-86842-211-9

==External links==
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 