Mystery of the Wolf
{{Infobox film
| name           = Mystery of the Wolf
| director       = Raimo O Niemi
| producer       = Leila Lyytikäinen, Risto Salomaa, Claes Olsson, Mike Downey, Sam Taylor, Börje Hansson
| writer         = Heikki Vuento
| starring       = 
| released       = 2006
| runtime        = 90 minutes
| country        = Finland / Sweden / UK
| language       = Finnish
| budget         = €1.9 million 
| gross          = €712,921 
}}
 Finnish drama directed by Raimo O Niemi.

==Synopsis==
Mystery of the Wolf tells the story of Salla, a girl who has been abandoned by her mother and falls under the protection of a herd of wolf|wolves.  After being found and returned to society, Salla still feels a strong connection to wild animals, and as she grows has to decide between her loyalties to her family and to the animals that once protected her.

==Awards==

*First Light Young Jurors Award, London Children’s Film Festival.
*Golden Cairo for Best Film, Cairo International Film Festival.
*49. Nordische Filmtage Lübeck, Germany Prize of the Children’s Jury
*Moscow Film Festival for Children and Young People, Russia Moscow Teddy Bear.
*7th Riga International Childrens Film Festival “Berimors Cinema”, Latvia, Audience Award.

==References==
*http://www.cinema.com.my/movies/movie_contents.aspx?search=2007.4435.mysterywolf.8977&section=review
 

== External links ==
*  

 
 
 
 

 