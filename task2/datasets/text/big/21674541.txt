La Florida (film)
{{Infobox Film
| title              = La Florida
| image                 = 
| image size          = 
| director          = George Mihalka Suzette Couture Pierre Sarrazin
| writer             = Suzette Couture Pierre Sarrazin
| starring                = Rémy Girard Pauline Lapointe Raymond Bouchard Yvan Canuel Marie-Josée Croze Michael Sarrazin Margot Kidder
| soundtrack               = Milan Kymlicka
| cinematography        =  Rene Ohashi
| distributor          = Vivafilm
| budget                = 
| country                  = Canada English
| runtime               = 
| released                = 1993
| imdb                  = 0106931
}}
 Suzette Couture Pierre Sarrazin.

==Synopsis== Fort Lauderdale, where they buy a motel catering to "Snowbird (people)|snowbirds". However, their new business runs afoul of "Big Daddy" (Raymond Bouchard), an established motel operator and local crime boss, who makes it his mission to drive the Lespérances out of business. 

The Lespérances may, however, have an ally in Jay Lamori (Jason Blicker), a local businessman of uncertain motives who may or may not be what he seems.

==Award nominations== Best Motion Best Director, Best Actor Best Actress Best Supporting Actor (Yvan Canuel), Best Screenplay, Best Achievement in Overall Sound and Best Sound Editing. 
 Golden Reel Award as the top-grossing Canadian film of the year. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 