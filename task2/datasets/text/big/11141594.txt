Unnal Mudiyum Thambi
{{Infobox film
| name           = Unnal Mudiyum Thambi
| image          = Unnal mudiyum thambi.jpg
| image_size     =
| caption        =
| director       = K. Balachander
| producer       = Rajam Balachander Pushpa Kandaswamy
| writer         = K. Balachander
| narrator       = Seetha Gemini Manorama Charle Janagaraj Delhi Ganesh Nassar
| music          = Ilaiyaraaja
| cinematography = Raghunath Reddy
| editing        = R. Bhaskaran
| studio         = Kavithalayaa Productions
| distributor    = Kavithalayaa Productions
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          =
}}

Unnal Mudiyum Thambi (You can, Brother) is a Tamil language film starring Kamal Haasan in the lead role. The movie, a re-make of the Telugu film Rudraveena (film)|Rudraveena, was released on 4 March 1988.

==Plot==
Udhayamoorthy (Kamal Haasan) is the younger son of an illustrious Carnatic music maestro, Bilahari Marthandam Pillai (Gemini Ganesan). The father is a strict disciplinarian who is very class conscious and cares only for his art. The maestros elder son (Prasad Babu) is born mute and hence had to take to an instrument, the nadhaswaram, which he is quite adept at.

A car makes an entry into the village. An MP (VK Ramasamy) gets off the car and finds that the village is self-displined in terms of cleanliness, get ridding of bad habits like drinking etc. He learns that the reason and cause of all this is a young man Udayamurthy. MP wonders how just a young man was able to achieve this and expresses the same to Udayamurthy and a flashack begins.

Adolescent Udayamurthy who is from a high society family one day sees an old blind beggar woman struggling to pick up a banana thrown by a passer-by. Young Udayamurthy though feel for the woman does not render any help and another old man helps the beggar woman. The old man calls Udayamurthy and tells him to be helpful to others. Now grown up Udayamurthy is a very mischievous and happy-go-lucky guy who always cracks pranks on all his family members like sister, sister-in-law Angayarkanni (Manorama) etc. and gets punished by his father Pilahari Marthandam Pillai an ardent devotee and a reputed short-tempered Carnatic music singer. One day Marthandam Pillai shouts at his son saying that it is better to die than to live an irresponsible life like this and this hurts Udayamurthy. Next morning family members are shocked on seeing Udayamurthy hanging himself at the middle of the hall. But actually it is another prank played by him and he did not die so. Angayarkanni hurt by the mischief feels sad for not playing her role as a sister-in-law and not raised Udayamurthy properly and made him as an irresponsible boy. Udayamurthy changes his mind and start to be responsible making his family happy. He comes to a decision of being helpful to the society by reminding of the temple incident in his adolescent life and on seeing an old gardener who wants to live usefully for the society at least by planting trees.

Udayamurthy sees a woman in a government office who protests against an official for rejecting her application for teacher job for not mentioning her caste and religion in the application. Udayamurthy again sees the woman at a theatre when she slaps a thug for misbehaving. Udayamurthy gets attracted to the girl named Lalitha Kamalam (Seetha) on knowing that she and her thoughts are very much secular. Both Udayamurthy and Lalitha fall in love with each other. Udayamurthy could not concentrate on singing when he hears the begging of a night begger. This angers Marthandam Pillai and shouts that he is not fit to be his disciple. Marthandam Pillai also get angry on seeing his son loving a girl who come from the caste of untouchables. Udayamurthy and Marthandam Pillai get into fight when Marthandam Pillai brings another man Charukesi (Ramesh Aravind) to become his disciple stating reasons that Udayamurthy can live as a man going behind humanity and not as a devoted singer. At another incident Udayamurthy gets vexed when Marthandam Pillai does not care to save a dying man by giving lift in his car to hospital but cared only to be punctual for his music program leading to the death of the man due to delay in treatment. Marthandam Pillai challenges Udayamurthy to become a man making his father proud without using the name of his father in the society and then he will listen to what he says. Udayamurthy could not bear the neglection of his father on him and he leaves the house.

Udayamurthy meets the wife of the dead man and finds that the government did not give any compensation to the family as the man was drunk while on duty. Udayamurthy finds lot of men laying always at drunkard state and their children go for work to earn for their drinking expense instead of going to school. Hence he sets out to change the people and bring them out of drinking habit. He becomes unsuccessful in his attempt and at the same time gets attacked by liquor selling qroup. Lalitha provokes the wives of the drunkard to protest their drunkard husbands to which the women co-operates. Udayamurthy gains the enmity of the drunkards for destroying the liquor shops and for provoking their wives against them.Hence they set out to stop the wedding of Udayamurthy with Lalitha by promising that they would stop drinking if he cancels his wedding to which Udayamurthy accepts. His sacrifice earns up the respect upon the whole village and the drunkards start to obey him. Udayamurthy and Lalitha together start Amaidhi Puratchi Iyyakam (Silent Revolution Society) and bring many reforms.

The MP on hearing the whole story appreciates him a lot and brings the reformation to the light of the government. Udayamurthy becomes famous overnight and gets Best Indian award from Prime Minister for  transforming the village in the good path. Pilahari Marthandam Pillai finds that his son achieved his mission and proudly announces that Marthandam Pillai is the father of Udayamurthy rather Udayamurthy is the son of Marthandam Pillai. He also accepts the marriage of his son with Lalitha and unite them.

==Cast==
* Kamal Haasan as Udayamoorthy
* Gemini Ganesan as Bilahari Marthandam Pillai Seetha as Lalithakamalam Manorama as Angayarkanni
* Meesai Murugesan as Anjayya
* Prasadbabu as Kamals mute brother
* K. S. Jayalakshmi
* Dhaarini as Kamals sister
* Charle
* V. K. Ramasamy (actor)|V. K. Ramaswamy as "Bloody" MP
* Janagaraj
* Nasser as Wine shop owner
* Delhi Ganesh as Arasiyalvathi
* Ramesh Aravind as Charukesi

==Production==
Kamal Haasans character was inspired from a real life social activist M. S. Udayamurthy.   Vishweswaran, husband of Bharatanatyam artist Chithra Vishweswaran did a role of Chief-minister who appears in the climax. 

==Track listing==
The songs were composed by Ilaiyaraja while lyrics written by Pulamaipithan, Muthulingham and Ilaiyaraaja.  Ilaiyaraaja has reused some tracks from Telugu original Rudraveena (film)|Rudraveena. The song "Idhazhil Kathai" is based on Lalitha Raga while "Nee Ondruthaan" is based on Bilahari Raga.  

{| class="wikitable"
|-
! SL. No. !! Songs Title !! Singer(s)
|-
| 1. || "Akkam Pakkam Parada" || S. P. Balasubrahmanyam
|-
| 2. || "Enna Samayalo" || S. P. Balasubrahmanyam, K. S. Chitra, Sunandha
|-
| 3. || "Ithazhil Kathai" || S. P. Balasubrahmanyam, K. S. Chitra
|-
| 4. || "Maanida Sevai" || K. J. Yesudas
|-
| 5. || "Nee Onru Than" || K. J. Yesudas
|-
| 6. || "Punjai Undu" || S. P. Balasubrahmanyam
|-
| 7. || "Unnal Mudiyum Thambi" || S. P. Balasubrahmanyam
|}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 