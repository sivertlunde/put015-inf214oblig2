Calendar Leaves
{{Infobox Film
| name           = Calendar Leaves
| image          = 
| image_size     = 
| caption        = 
| director       = Martin Dudzik
| producer       = Anthony Bortolussi
| writer         = Michael Serebriakov (story) Martin Dudzik (screenplay)
| narrator       = Martin Dudzik
| starring       = Brad Giglio Tyler Kranz Naomi Inglis
| music          = 
| cinematography = Martin Dudzik
| editing        = Martin Dudzik
| distributor    = 
| released       = May 24, 2005
| runtime        = 17 mins
| country        = Canada English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Calendar Leaves (2005) is a short Coquitlam-shot drama produced by Dudzik Films and based on Michael Serebriakovs short story of the same name.   
  The Cat Came Back, director Martin Dudzik was looking for a drama to direct. Through an aide, he had received the plot synopsis of Serebriakovs "Calendar Leaves". Serebriakov had written it in the previous year, in response to a cruel practical joke he had witnessed. A month later, on the patio of a Starbucks on Vancouvers East side, the contract that brought Calendar Leaves to life was signed.

==Plot==

Set in an ordinary Canadian town, it follows Parker Jones (Brad Giglio), a formerly troubled youth, on a morning when he is running late for school. Arriving, he seeks out his girlfriend, Macy, but she is also a no-show that day. When he asks if his friends Steven (Tyler Kranz) and Josie (Naomi Inglis) have seen her, both disavow any knowledge of such a person. Her locker is no longer hers, and when he calls her house, he is told that she does not live there. Confused and distraught, Parker must now grapple with himself to answer the burning question: "Was she real?". Believing that he had lost his mind and his life is not worth living, Parker takes his own life by jumping off a secluded bridge into a river. The final narration reveals that it was an April Fools joke that had gone terribly wrong.

==Production==
Calendar Leaves was shot in Coquitlam, in the usual speedy Martin Dudzik manner in five days.  Port Moody Secondary School was used for the interior shots. For the day of interior shooting, short story author Michael Serebriakov was invited on set. 
 
For the opening scene, the crew spent the morning painstakingly recreating a bus stop. Because of Dudziks love for close shots, no part of the replica bus stop made it into the final cut.  
 
Tyler Kranz was initially slated to be the boom operator, but the actor cast to play Steven fell sick just before production. Many consider Kranzs performance the best of the movie. 
 
During one day of production, the set was visited by a dreary notorious Vancouver rain. Although soaking the crew, it provided for some of the more memorable scenes of the movie. 
 
According to Dudzik, Kranz turned out to be a sub-par narrator, so he took over personally. To get the "narratory" voice, he ate copious amounts of ice cream before recording his lines. 
 
As a joke, newbies were told the movie would star Ed Harris, because of Peter Nicholas Smiths uncanny resemblance to the actor.

==Deviations from the short story==
Naturally, while writing the screenplay, Dudzik was forced to make some changes from the original Serebriakov version. Firstly, the short story started in a relatively light mood, turning dark by the end. The movie holds an ominous sombre tone throughout - in tune with Dudziks vision of the drama he was looking for.  
 
What is in the short story a subtlety, Parkers suicidal history is obvious in the movie, since his cut wrists are shown in the opening scene. 
 
Some of the foreshadowing hidden in the lines of the short story was cut in the screenplay, leaving the ending to be confusing for some, including a Toronto film critic.    

==Reception== Young Cuts Film Festival in Toronto  and the 2006 B.C. Student Film Festival. It is so far arguably the most critically recognized independent film from Dudzik Films.The two reviews of the movie were generally favorable. A writer for the Hour.ca commented that the movie had a "Canadian look", but was typical of a teen drama.   

==References==
 

==External links==
* 

 
 
 
 
 