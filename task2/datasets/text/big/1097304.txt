An Early Frost
{{Infobox television film
| bgcolour =
| name = An Early Frost
| image = An-early-frost.jpg
| caption = An Early Frost DVD cover.
| format = Drama
| runtime = 100 min
| creator =
| director = John Erman
| producer = Perry Lafferty
| writer = Ron Cowen (teleplay) Daniel Lipman (teleplay) Sherman Yellen (story)
| starring = Aidan Quinn
| music = John Kander
| country = USA
| language = English
| network = NBC
| released = 11 November 1985
| first_aired =
| last_aired =
| num_episodes =
| preceded_by =
| followed_by =
}} NBC television attorney who goes home to break the news&nbsp;– that he is homosexual and has AIDS&nbsp;– to his parents, played by Ben Gazzara and Gena Rowlands.

==Storyline==

Michael Pierson, a successful lawyer, suffers a bad coughing jag at work and is rushed to the hospital. There he learns from a doctor that he has been exposed to the AIDS virus.  At home, he receives another piece of disturbing news: his lover Peter, played by D. W. Moffett, confesses that he had sex outside the relationship because Michael is a workaholic and is living in the closet. Michael, in a rage, throws Peter out of the house.  He then travels to his parents home to inform them that he is gay and has AIDS.
 John Glover), a flamboyant homosexual with AIDS. The film depicts Victors death and shows a nurse throwing Victors few possessions into a garbage bag because she fears that the items could be contaminated.

==Development==

The teleplay for the film by Ron Cowen and Daniel Lipman spent two years in development and underwent at least thirteen rewrites before the Standards and Practices division at the network accepted it for airing.

==Cast==
* Aidan Quinn as Michael Pierson
* Sylvia Sidney as Beatrice McKenna
* Ben Gazzara as Nick Pierson
* Gena Rowlands as Katherine Pierson
* Terry OQuinn as Dr. Redding John Glover as Victor Mitado
* Bill Paxton as Bob Maracek
* D. W. Moffett as Peter Hilton

==Reviews, awards, and aftermath==
Tom Shales of the Washington Post called An Early Frost "the most important TV movie of the year."
 Showtime broadcast As Is. The movie paved the way for later TV and feature films dealing with the topic of AIDS, including Go Toward The Light (1988), The Littlest Victims, The Ryan White Story ( both 1989), Longtime Companion (1990), and Philadelphia (movie)|Philadelphia (1993).

==External links==
* 

 

 
 
 
 
 
 
 
 
 