The Last Tycoon (1976 film)
{{Infobox film
| name = The Last Tycoon
| image = Last tycoon.jpg poster by Richard Amsel
| director = Elia Kazan
| producer = Sam Spiegel
| writer = Harold Pinter
| story  = F. Scott Fitzgerald (The Love of the Last Tycoon|Novel)
| starring = Robert De Niro Tony Curtis Robert Mitchum Jack Nicholson Donald Pleasence Jeanne Moreau Ingrid Boulting
| cinematography= Victor J. Kemper
| music = Maurice Jarre
| editing = Richard Marks
| distributor = Paramount Pictures
| released =  
| runtime = 123 min.
| country = United States
| language = English
| budget = $5.5 million Nat Segaloff, Final Cuts: The Last Films of 50 Great Directors, Bear Manor Media 2013 p 146-148 
| gross = $1.8 million 
}}
The Last Tycoon is a 1976 American dramatic film directed by Elia Kazan and produced by Sam Spiegel, based upon Harold Pinters screenplay adaptation of F. Scott Fitzgeralds The Last Tycoon, sometimes known as The Love of the Last Tycoon. It stars Robert De Niro, Tony Curtis, Robert Mitchum, Jack Nicholson, Donald Pleasence, Jeanne Moreau, Theresa Russell and Ingrid Boulting.

The film was the second collaboration between Kazan and Spiegel, who worked closely together to make On the Waterfront. Fitzgerald based the novels protagonist, Monroe Stahr, on film producer Irving Thalberg. Spiegel was once awarded the Irving Thalberg Memorial Award.
 Best Art Gene Callahan, Jack T. Collis, Jerry Wunderlich).   

Coincidentally, it was Fitzgeralds last, unfinished novel, as well as the last film Kazan directed, even though he lived until 2003.

== Plot synopsis == production chief studios of the Golden Age of Hollywood. He is a tireless worker in a time of turmoil in the industry due to the creation of the Writers Guild of America, Monroe being accustomed to make his underlings, including screenwriters, do whatever he says.

Monroes life flows between film shootings, industry bosses machinations, discussions with writers and actors and a battle with an union organizer named Brimmer, whose intrusion he resents. In the meantime, Monroe becomes obsessed with a young woman with a troubled past, Kathleen Moore, who is engaged to be married with another man, while Cecilia Brady, the young daughter of a studio board member, tries in vain to make Monroe see how she truly feels.

Pat Brady and other studio executives resent Monroes disrespect for their wishes and neglect. Seeing his treatment of the union organizer as the last straw, they insist that Monroe go away for a long rest. As his difficulties grow bigger and his health declines, Monroes life runs to an uncertain but inevitable twilight that echoes a long gone era.

== Cast ==
* Robert De Niro ... Monroe Stahr
* Tony Curtis ... Rodriguez
* Robert Mitchum ... Pat Brady
* Jeanne Moreau ... Didi
* Jack Nicholson ... Brimmer
* Donald Pleasence ... Boxley
* Ray Milland ... Fleishacker
* Dana Andrews ... Red Ridingwood
* Ingrid Boulting ... Kathleen Moore
* Peter Strauss ... Wylie
* Theresa Russell ... Cecilia Brady
* Tige Andrews ... Popolos
* Morgan Farley ... Marcus
* John Carradine ... Tour guide
* Jeff Corey... Doctor
* Diane Shalet ... Stahrs secretary
* Seymour Cassel ... Seal trainer
* Anjelica Huston ... Edna

== Adaptation ==
Author Francis Scott Fitzgerald did not live to finish The Last Tycoon, so that the version published in 1941, edited by Edmund Wilson with Fitzgeralds notes, is technically a fragment. The film preserves that fragment through an abrupt kind of editing style and a narrative that flows without conventional shape.

In one of his final notes for The Last Tycoon, Fitzgerald wrote in capital letters: "Action Is Character." This is an objective Kazan, Pinter and De Niro addressed in the attempt to transfer the character of Monroe Stahr to the film.

Jeanne Moreau and Tony Curtis make brief appearances as idols of Hollywoods golden age, whose film in progress is being overseen by Stahr.

== Themes ==
The Last Tycoons protagonist, Monroe Stahr, is a character full of associations to Irving Thalberg, the production chief at M-G-M in the period between the late 20s and 30s. The background is Hollywood in the Golden Thirties, when studios made 30 to 40 productions a year and every backlot could simultaneously contain pictures set in places such as New York, Africa, the South Pole and Montmartre. The background of the film has a close bond to stories of Hollywood at that time, as well as to Fitzgeralds own life and career.

Thalberg, a "boy genius" until his death at the age of 37 in 1936, was held in high regard inside and outside Hollywood. He appeared to be able to divine successful films continuously, knowing in his head how much a certain kind of picture would gross, which, in turn, told him how much could be spent on its production and still have a profitable venture. Monroe Stahr, played with reticent passion by Robert De Niro (whose lean, dark good looks seem an idealization of Thalbergs) has the same knack, but eventually becomes a casualty of the "new" Hollywood of Wall Street investors, bankers and union organizers that Fitzgerald could see in the future. Thalberg died before being overtaken by defeat, while in the film, Stahr does not. 

The theme of unfinished ambitions and the unattained love of the young and beautiful in Hollywood, embodied by the beach house, have great significance for both the Novelist and Director at the end of their luminary careers.

 

== Reception ==
The critical reaction to The Last Tycoon has been mixed; it received positive reviews from 41% of critics cited by Rotten Tomatoes. 

== References ==
 

== External links ==
* 
* 
* 

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 