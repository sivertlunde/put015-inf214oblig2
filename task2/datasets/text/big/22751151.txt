Weddings and Babies
{{Infobox film
| name           = Weddings and Babies
| image          = Myhers and lindfors in weddings and babies.jpg
| image_size     =
| caption        = John Myhers with Viveca Lindfors, whose performance was described as the "solid core of the film."
| director       = Morris Engel
| producer       = Morris Engel
| writer         = Morris Engel Blanche Hanalis (story treatment) Mary-Madeleine Lanphier (story treatment) Irving Sunasky (story treatment)
| starring       = Viveca Lindfors John Myhers Chiarina Barile Leonard Elliott
| music          = Eddie Manson
| cinematography = Morris Engel
| editing        = Michael Alexander Stan Russell
| released       = October 5, 1960
| runtime        = 81 minutes
| country        = United States
| language       = English
}}
Weddings and Babies is a 1960 film directed, produced, and written by independent filmmaker Morris Engel. Starring Viveca Lindfors and John Myhers.

The last of Engels feature films, it was shot in 1957 and previewed at the 1958 Venice Film Festival, where it won the Critics Award. Being unable to find a traditional distributor, Engel took the necessary steps to distribute the film himself, including financing it and handling the booking at theaters. It debuted on October 5, 1960.

==Plot summary==

The film focuses on the stormy relationship between New York City wedding photographer Al Capetti (Myhers) and his Swedish-born girlfriend and assistant Bea (Lindfors). As the film begins, Bea tells Al how anxious she is to get married and have children of her own.

Al, however, resists getting married until he has substantial savings. He buys a movie camera in order to expand his business, but the camera is damaged accidentally. Meanwhile, Al is under pressure from his elderly mother, who he has just committed to a home for the elderly. At one point, his mother runs away to the cemetery where her husband is buried, as one critic later put it, "to see the grave site that awaits her, one stone among a million in that vast necropolis gazing on the citys distant skyline from the shadow of the Brooklyn-Queens Expressway|BQE."   

==Production==
Weddings and Babies was filmed using a handheld 35&nbsp;mm motion picture camera, and is considered to be the first fictional movie to be shot with such a camera that also recorded synchronized sound.   

According to Time magazine, Engel preferred his actors to improvise and "often throws away his working script". He followed them with the handheld camera to record scenes as they developed, "catching this, missing that, taking his chances and riding his luck."   Chiarina Barile, who plays Als elderly mother, was discovered by Engel while she was sitting on the stoop of a New York City apartment building, and died soon afterwards. She never saw the film. 

==Reception==
A reviewer for Time (magazine)|Time stated that "as a technical exercise in cinema   is one of the most exciting feature films the U.S. has produced in a decade."   

Writing in The New York Times, film critic Bosley Crowther praised the "exquisite and isolatedly eloquent little bits in Mr. Engels picture." He said of Lindfors performance that "everything she does—every movement, every gesture, every reaction, every lift and fall of her voice — is so absolutely right and convincing that the style drapes most fitly around her...She is the solid core of this film." However, he also felt the "do-it-as-you-feel-it approach, accounts for more looseness and banality than beauty and eloquence," and that "it fails to develop cohesion and leaves many questions—indeed, the conflict—unclarified." He also criticized the sound quality and said the photographic style provokes "more irritation and bewilderment than surprise."

Crowther said of the film, which turned out to be Engels last feature: "Perhaps when he gets somebody with real professional skill to write him a script and he learns to hold his camera more firmly, he will give us the great American urban film."   

Writing in The Village Voice in 2008, critic J. Hoberman said that "Weddings and Babies is "certainly the most extensive portrait of Manhattans Little Italy before Martin Scorseses incomparable Mean Streets." He singled out Bariles performance as "incredibly ancient and incomparably dignified."  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 