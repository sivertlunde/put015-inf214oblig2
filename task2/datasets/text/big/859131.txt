Kalathur Kannamma
 
 
{{Infobox film
| name           = Kalathur Kannamma
| image          = Kalathur kannamma.jpg
| image_size     =
| caption        = Theatrical poster
| director       = A. Bhimsingh
| producer       = A. V. Meiyappan
| writer         = Javar Seetharaman
| story          = Javar Seetharaman
| narrator       = Savitri Kamal Haasan
| music          = R. Sudharsanam
| cinematography = T. Muthuraj
| editing        = S. Suraiya
| studio         = AVM Productions
| distributor    = AVM Productions
| released       = 12 August 1959  or 12 August 1960 
| runtime        = 179 minutes
| country        = India
| language       = Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Tamil romantic Savitri in the lead, while Kamal Hassan made his debut in this film as a child artist. T. S. Balaiah, Devika and S. V. Subbaiah play supporting roles. The films critically acclaimed soundtrack was composed by R. Sudharsanam. The film tells the story of a young couple&nbsp;— a wealthy zamindars son and a farmers daughter&nbsp;— who are separated by unfortunate circumstances, while their innocent son is forced to grow up in an orphanage.
 Telugu as Mooga Nomu. It was also remade in Hindi as Main Chup Rahungi with Bhimsingh returning as director, in Sinhala as Mangalika, and in Malayalam as Navavadhu.

==Plot==
Rajalingam (Gemini Ganesan) is the only son of Ramalingam (T. S. Balaiah), the zamindar of Kalathur. Kannamma (Savitri (actress)|Savithri) is the daughter of Murugan (S. V. Subbaiah), a farmer of the same place. On the zamindars advice, Murugan sends his daughter to Madras for higher education. While returning from Madras, Kannamma meets Raja in the train. Noticing her respect and awe for the zamindar, Raja calls himself an electrician visiting the zamin palace. They fall in love. A few days later, Kannamma learns the truth and to reassure her, Raja marries her secretly in a temple.

Two days later, Raja has to go abroad for higher studies. During his absence, Ramalingam learns of the marriage and orders Kannamma to forget his son. Moved by the zamindars feelings, Kannamma promises never to mention their marriage to anyone. Kannamma is now in the family way. Ramalingam arranges for the stay of Murugan and Kannamma in a nearby town, Sevalpatti. Murugan, who is ashamed of Kannammas love affair, leaves Kannammas newborn son in an orphanage and lies that the child was Stillbirth|stillborn. They both decide to leave the place and settle in Bangalore.

When Raja returns, he learns that Kannamma had left Kalathur. His enquiries in Sevalpatti lead him to believe that Kannamma had led an immoral life. Grief-stricken, he travels from place to place to forget Kannamma and takes to drinking as a last resort. In Bangalore, he encounters Kannamma in a dancers house where she had come to teach the dancer’s daughter. Her presence in the house and reticence to Raja’s questions strengthens his belief that Kannamma is a woman of ill repute and in disgust, he returns home.

Kannamma’s son Selvam grows up into an intelligent boy (Kamal Haasan) and is living in the orphanage in Sevalpatti. Murugan visits the orphanage and on meeting Selvam, decides to shift to Sevalpatti to be near him. Kannamma becomes a teacher in Selvam’s school and feels attracted to him. She invites him to stay with her but he refuses as he has to look after Mani, a lame orphan living with him. Raja is invited to Sevalpatti by Singaram, a rich merchant who wants to marry his daughter Maduram (Devika) to him, to preside over a school function. Raja takes a fascination for Selvam who acts in the school drama. He encounters Kannamma again and orders the Headmistress to dismiss her.

Mani is seriously ill and Selvam turns to Raja for help, but Mani eventually dies. Raja takes Selvam with him to the zamin. On Selvams insistence, he stops drinking and to give him a mother, also decides to marry Maduram. A seriously ill Murugan confesses to Kannamma that her child is not dead and reveals the identity of Selvam. Kannamma tries to contact Selvam but does not find him in the orphanage. The news shocks Murugan and to soothe his nerves, they move down to their village Kalathur. In the local temple, Kannamma meets Selvam and tells him that she is his mother. On learning from him that Raja is bringing him up and is marrying shortly to find him a mother, she asks him not to mention anything about her to Raja. While preparations were going on for the marriage of Raja with Maduram, she comes to know from Selvam that he is the son of Raja. Singaram insists on a written undertaking that the properties of Raja would go to the children of Maduram only. The news of this conflict spreads in the village.

On learning this, Murugan rushes to the palace to own the boy, but collapses near the palace gate. Kannamma who has followed, takes Selvam and tries to move away when she is intercepted by Raja who demands the boy back. Raja refuses to believe that Selvam is Kannamma’s son and abuses Kannamma for her shameless life. Ramalingam observes that even in such a humiliating situation, Kannamma is silent and does not breathe a word about her promise to him. He is moved and acknowledges her as his daughter-in-law. Selvam is united with his parents, and Raja’s marriage with Maduram is cancelled.

==Cast==
* Gemini Ganesan as Rajalingam Savithri as Kannamma
* Kamal Haasan as Selvam
* T. S. Balaiah as Ramalingam
* S. V. Subbaiah as Murugan
* Devika as Maduram
* Javar Seetharaman
* V. R. Rajagopal

==Production== Moral Rearmament Savitri were Daisy Irani, who had been already been paid  10,000 in advance.     S. P. Muthuraman, who later became a leading director in Tamil cinema, made his debut as an assistant director in this film. 
 AVM Saravanan noticed Haasan as a hyperactive child. He took him over and introduced to Meiyappan who was looking for a young boy to act in Kalathur Kannamma.   

==Soundtrack== Vaali and Kothamangalam Subbu. 

{{Track listing
| headline       = Tracklist
| extra_column   = Singer(s)
| collapsed       = no
| lyrics_credits = yes
| total_length    =
| title1         = Kangalin Vaarthaigal
| extra1         = A. M. Rajah, P. Susheela
| lyrics1        = Kannadasan
| length1        = 3:33
| title2         = Sirithaalum
| extra2         = C. S. Jayaraman
| lyrics2        = Kannadasan
| length2        = 3:30
| title3         = Aadatha Manamum
| extra3         = A. M. Rajah, P. Susheela
| lyrics3        = Ku. Ma. Subramaniam
| length3        = 3:19
| title4         = Arugil Vanthaal
| extra4         = A. M. Rajah
| lyrics4        = Kannadasan
| length4        = 3:23
| title5         = Ammavum Neeye
| extra5         = M. S. Rajeshwari Vaali
| length5        = 2:47
| title6         = Buthimaan Balavaan (Childrens Drama)
| extra6         = S. C. Krishnan, T. M. Soundararajan, M. S. Rajeshwari
| lyrics6        = Kothamangalam Subbu
| length6        = 6:58
}}

===Reception=== Kamalahasan  , music composed by R.Sudarsanam) is still remembered and treasured by many all over the world."  Film historian B. Vijayakumar said "One stand out sequence in Kalathoor Kannamma is the song pictured on Kamal. The song, ‘Ammavum neeye’... by M. S. Rajeswari and Kamals acting made it an unforgettable experience".  A report from The Hindu called the songs verses as "immortal".  The Times of India said, "Ammavum Neeye Appavum Neeye... — this is a line thatll forever remain etched in the memory of Tamil cinema fans." 

==Release==
Kalathur Kannamma was released on 12 August,    although the year of release is disputed. While most sources have claimed that the film released in 1960,    others have claimed the films release year to be 1959.   
* 
*   

===Critical reception===
Reviews were mostly positive. Ananda Vikatan (11.9.1960) praised Kamal Haasans performance and said, "One of the best films in Tamil seen so far... Master Kamal Haasans acting has shaken up everyone...".  The Madras-based film magazine Movieland said, "A new star rises on the movie horizon". The article by film journalist, K. Vasudevan, praised the performance of Haasan.  Randor Guy of The Hindu wrote that the film was remembered for "The little boy’s remarkable performance and the song filmed on him". 

FridayMoviez said, "Besides Gemini Ganesh,   K. Savithri (Mrs. Gemini Ganesh in private life), T. K. Bhagavathi, S. V. Subbiah in lead roles, Meiyappan introduced a brilliant new talent as child artiste in this movie. The boy has created film history now and has emerged as a leading personality of Indian Cinema- Kamal Haasan". The critic further wrote, "The movie is not only brilliant in terms of the story and acting, but even its music is beautiful", while calling it "A Definite must-watch for quality-cinema lovers."    Kamal Haasans elder brother Charu Haasan said, "I have watched Kamal’s first movie a 100 times, as I took him to all the theatres wherever ‘Kalathur Kannamma’ was being screened."  S. Saraswathi of Rediff included Kalathur Kannamma in her list of "The 10 Best Films of Kamal Haasan", praising Haasans performance over Gemini Ganesan and Savitris performances. 

===Accolades===
Kalathur Kannamma was the winner of the National Film Award for Best Feature Film in Tamil&nbsp;– Certificate of Merit for the Third Best Feature Film in 1961,  as well as the Certificate of Merit by the Government of India.    Kamal Haasans performance earned him the Rashtrapati Award|Presidents Gold Medal in 1961.      

==Other versions== Hindi as Sinhala as Mangalika,  while the Hindi version was remade as Udarata Menike.  The Malayalam remake was titled Navavadhu. 

==References==
 

==External links==
*  
*  
*   at Upperstall.com
* 

 
 
 

 
 
 
 
 
 
 
 
 
 