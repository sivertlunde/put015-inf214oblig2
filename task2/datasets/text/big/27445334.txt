Mr. Vampire III
 
 
{{Infobox film name = Mr. Vampire III image = Mr. Vampire III.jpg alt =  caption = Original Hong Kong film poster traditional = 靈幻先生 simplified = 灵幻先生 pinyin = Línghuàn Xiānshēng jyutping = Ling4 Waan6 Sin1 Sang1}} director = Ricky Lau producer = Sammo Hung writer = Lo Wing-keung Sze-to Cheuk-hon starring = Lam Ching-ying Richard Ng Billy Lau David Lui Pauline Wong Chow Gam-kong Tam Yat-ching Hoh Kin-wai Cheung Wing-cheung Ka Lee Sammo Hung Wu Ma Corey Yuen Yip Wing-cho music = Anders Nelsson Stephen Shing cinematography = Andrew Lau editing = Peter Cheung Keung Chuen-tak studio = Bo Ho Films Co., Ltd. Paragon Films Ltd. distributor = Golden Harvest released =   runtime = 88 minutes country = Hong Kong language = Cantonese budget =  gross = HK$ 19,460,536.00
}}
Mr. Vampire III, also known as Mr. Vampire Part 3, is a 1987 Hong Kong comedy horror film directed by Ricky Lau and produced by Sammo Hung. The film is the third of a series of five films directed by Ricky Lau in the Mr. Vampire franchise. The Chinese title of the film literally translates to Mr. Spiritual Fantasy.

==Plot==
One dark night, Uncle Ming attempts to exorcise angry spirits in a haunted house but is unsuccessful and he narrowly escapes. He brings his two ghostly companions (Big Pao and Small Pao) with him to a nearby town. The town is under attack by a group of bandits with supernatural powers. The thugs are led by Devil Lady, a powerful evil witch doctor.

When Ming and the Paos are having dinner in a restaurant, Captain Chiang comes to harass them, and the Paos take revenge by playing tricks on Chiang. Just then, Chiangs master, Uncle Nine, appears and he subdues the two ghosts and traps them inside a wine jar. As Ming is no match for Nine in magic powers, he pleads with Nine to release the Paos. Nine agrees to set the ghosts free but warns Ming that he must part ways with them. Big Pao is captured by Devil Lady later due to Chiangs act of mischief. Devil Lady casts a spell on Big Pao, making him see people dressed in Taoist robes as monster birds, and sends him to attack the town.

During the chaos, Devil Lady breaks into the town prison to free two of her bandits who were captured earlier by the townspeople, but falls into a trap instead. Uncle Nine defeats Devil Lady in a fight and the evil sorceress falls into a well and dies. However, the spirits of the two bandits escape and possess two men, and go out to cause trouble again. Nine and Ming combine forces to contain the evil spirits in wine jars and fry them in hot oil. Just then, the coin sword binding Devil Lady shatters and the sorceress spirit is set free to take revenge. Eventually after a long battle, with the help of the Pao ghosts, Nine and Ming succeed in destroying Devil Lady once and for all.

==Cast==
*Lam Ching-ying as Uncle Nine, a unibrowed Taoist priest
*Richard Ng as Uncle Ming, an inept Taoist priest
*Billy Lau as Captain Chiang, Uncle Nines student
*David Lui as Big Pao, one of Uncle Mings two ghost companions
*Pauline Wong as Devil Lady, a powerful evil witch doctor
*Chow Gam-kong as Shou, one of the two possessed men
*Tam Yat-ching as the haunted houses owner
*Hoh Kin-wai as Small Pao, one of Uncle Mings two ghost companions
*Cheung Wing-cheung as Eagle Head, one of the bandits
*Ka Lee as a townsman
*Baan Yun-sang as Te, Uncle Nines student killed by Devil Lady
*Lee Chi-git as Fu, one of the two possessed men
*Chu Tau as Wild Boar, one of the bandits
*Gam Biu as a drunk villager
*Pang Yun-cheung as a townsman
*Siu Hung-moi as the female ghost in the wine jar set free by Uncle Ming
*Yip So as a ghost in the haunted house
*Chun Kwai-bo
*Ha Kwok-wing
*Lau Fong-sai
*Lam Foo-wai

===Guest stars===
*Sammo Hung as Hung, one of Uncle Nines birthday guests
*Wu Ma as Uncle Nines birthday guest
*Corey Yuen as Uncle Nines birthday guest
*Yip Wing-cho as Uncle Nines birthday guest

==Box office==
Mr Vampire 3 ran 17 December 1987 to 6 January 1988 and grossed HK$19,460,536.00 at the box office.      

==Home Media==

===VHS===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Language
! Subtitles 
! Notes 
! REF 
|- style="text-align:center;"
|| Unknown 
|| United States 
|| Unknown
|| Rainbow Audio and Video Incorporation 
|| NTSC
|| Cantonese 
|| English
|| 
|| 
|- 
|- style="text-align:center;"
|| 1999
|| Frace
|| Unknown
|| HK Video
|| NTSC
|| Cantonese 
|| English
|| 
||  
|}

===Laserdisc=== 
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Catalog No  
! Format  
! Language
! Subtitles 
! Notes 
! REF 
|- style="text-align:center;"
|| Unknown 
|| Japan
|| N/A
|| Pony Canyon LD
|| 
|| CLV / NTSC
|| Cantonese
|| Japaneses 
|| Audio Mono
|| 
|- 
|- style="text-align:center;"
|| 1988
|| Japan
|| N/A
|| 
|| 
|| CLV / NTSC
|| Cantonese
|| Japaneses 
|| Audio Mono
|| 
|- 
|- style="text-align:center;"
|| 1989
|| Hong Kong
|| N/A
|| Unknown
|| M0030C88
|| CLV / NTSC
|| Cantonese
|| None
|| Ratio: 1.66:1 (Widescreen), Audio Channel: Mono, 2 Sides
|| 
 
|}

===VCD===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Language
! Subtitles 
! Notes 
! REF 
|- style="text-align:center;"
|| Unknown 
|| Hong Kong
|| N/A
|| Megastar (HK)
|| NTSC
|| Cantonese
|| English, Traditional Chinese
|| 2VCDs
|| 
|-
|- style="text-align:center;"
|| 5 December 2003
|| Hong Kong
|| N/A
|| Joy Sales (HK)
|| NTSC
|| Cantonese, Mandarin
|| English, Traditional Chinese
|| 2VCDs
|| 
|}

===DVD===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Region  
! Language 
! Sound  
! Subtitles  
! Notes 
! REF 
|- style="text-align:center;"
|| Unknown 
|| Japan
|| N/A
|| Universal Pictures Japan
|| NTSC
|| 2
|| Cantonese, Japanese 
|| Dolby Digital Mono
|| Japanese 
|| Digitally Re-mastered Box-set
|| 
|-
|- style="text-align:center;"
|| 31 December 2002 
|| Hong Kong
|| N/A
|| Deltamac (HK) 
|| NTSC
|| ALL
|| Cantonese, Mandarin
|| Unknown 
|| English, Traditional Chinese, Simplified Chinese
|| 
|| 
|- 
|- style="text-align:center;"
|| 19 February 2004
|| France
|| N/A
|| HK Video
|| PAL
|| 2
|| Cantonese
|| Dolby Digital 
|| French
|| Box-set 
|| 
|-
|- style="text-align:center;"
|| 4 July 2006
|| Taiwan
|| N/A
|| Catalyst Logic
|| NTSC
|| 3
|| Cantonese, Mandarin
|| Dolby Digital 2.0
|| English, Traditional Chinese, Simplified Chinese
|| 
|| 
|- 
|- style="text-align:center;"
|| 11 October 2007
|| Japan
|| N/A
|| Geneon Universal Entertainment
|| NTSC
|| 2
|| Cantonese
|| Unknown 
|| Japanese
|| Digitally Remastered Edition
|| 
|- 
|- style="text-align:center;"
|| 19 November 2007 
|| Hong Kong
|| N/A
|| Joy Sales (HK)
|| NTSC
|| ALL
|| Cantonese, Mandarin
|| Dolby Digital 2.0
|| English, Traditional Chinese, Simplified Chinese
|| 
|| 
|- 
|- style="text-align:center;"
|| 21 December 2012
|| Japan  
|| N/A
|| Paramount Home  Entertainment Japan
|| NTSC
|| 2
|| Cantonese, Japanese
|| Dolby Digital
|| Japanese
|| Digitally Re-mastered
|| 
|}

===Blu-ray===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Region  
! Language 
! Sound  
! Subtitles  
! Notes 
! REF 
|- style="text-align:center;"
|| 21 December 2012
|| Japan
|| N/A
|| Paramount Home Entertainment Japan
|| NTSC
|| A
|| Cantonese, Japanese
|| 
|| Japanese
|| Digitally Re-mastered
|| 
|}

==References==
 

==External links==
*  
*  
*   at Hong Kong Cinemagic
*   on lovehkfilm.com

 

 
 
 
 
 
 
 
 
 