Firewater (film)
{{Infobox Film
| name           = The Firewater    
| image          = 
| image_size     = 
| caption        = 
| director       = Hardi Volmer
| producer       = Mati Sepping
| writer         = Ott Sandrak Hardi Volmer
| narrator       = 
| starring       = 
| music          = Olav Ehala
| cinematography = Arko Okk
| editing        = Marju Juhkum
| distributor    = 
| released       = 14 April 1994
| runtime        = 
| country        = Estonia
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

The Firewater ( ), 1994 is Hardi Volmer´s history-based thriller. 

"Tulivesi" is based on the historical situation in Estonia at the end of 1920-s and sets a precedent of the struggle between the Estonian government and the spirit-smugglers. This film is dedicated to those who carry on the difficult and hopeless struggle against the narcotic business.

==Plot==
  Finnish government proclaimed dry law which lasted 12 years. Estonian bootleggers living on the neighboring coast of the Gulf of Finland profited from prohibition which staggered Finnish economy. In the fishermens fictional village called Ropsi accedes a new honest chief of the border guard station, lieutenant Aleksander Kattai (role cast by Jaan Tätte) who wants to "lay down the law and order". At the same time the most famous of the villages spirit-smugglers, Eerik Ekström (Erik Ruus) drives over the Gulf of Finland with his queuely firewater-cargo. He has spirit cans in his boat and also behind it in the sequential position (spirit smugglers call it "spirit-torpedo") Lieutenant Kattai sees on those days Hilda Sibul, the Ekströms fiancée. This role performs by an actress Epp Eespäev. They fall in love, but their good relationship does not last long.

In this point start the thrilling escapes and catchings, the plots sting of the tails. Lieutenant Kattai finds out the very high governmental circles are involved to "the Ropsi firewater-smuggling case". An Estonian minister Tui (  unit number two. After the war I remained with the border of guard. Until now I was at the southern border, near Pihkva. There was firing every day, Russian salesmen and smugglers, red agents." Those facts indicate to the Estonian War of Independence.

==Background==

The first period of independence has always been very honoured among the Estonians and they named these 21 years "the golden age of Estonia". "Firewater" movie seems to be tender, postcard-like remebrance of these days. A serious question "is this the same Estonia we fought for?" echoes through the film. Because of feeling critics named the film "the best Estonian thriller ever". 

"Tulivesi" premiered in fall 1994 and received the first award of the Estonian Movie Critics Association. The author of original music was Olav Ehala, cinematography was by Arko Okk. The plot has written by Ott Sandrak and Hardi Volmer.

==References==
 

==External links==
* 
*  

 
 
 