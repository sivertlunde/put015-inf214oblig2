Funky Monks
{{Infobox album  
| Name        = Funky Monks
| Type        = video
| Artist      = Red Hot Chili Peppers
| Cover       = Funkymonks.jpg
| Released    = September 25, 1991  
| Recorded    = 1991
| Genre       = Funk rock
| Length      = 60 min.
| Label       = Warner Bros.
| Director    = Gavin Bowden
| Reviews     =
| Last album  = Psychedelic Sexfunk Live from Heaven (1990)
| This album  = Funky Monks (1991)
| Next album  = What Hits!? (1992)
}}
 The Mansion, a supposedly haunted house which Rubin now owns. The 60-minute documentary, which was filmed in black-and-white, features footage of the band recording many of the tracks that made the album, and tracks that didnt make the album although would be released as singles and b-sides (such as "Soul to Squeeze" and "Sikamikanico"). It also features interviews from each member of the band, as well as Rick Rubin and the bands former and longtime manager, Lindy Goetz. Footage from the documentary was compiled for use in the "Suck My Kiss" music video, which was released in 1992. Funky Monks was originally released on VHS but was re-released on DVD. 

On July 16, 2011, NME voted Funky Monks the 14th must see rock documentary on their 20 must see rock documentaries list. 

==Unreleased version, bonus footage== Search and Destroy", "Sikamikanico" and "Soul to Squeeze". Some of the footage is extended from the released version.

It has been widely believed for years a 3-hour cut of the documentary exists. Footage not from the released documentary has been used in other forms of media such as television interviews and documentaries on the band. Rare color footage was also used at the time for various interviews including ones with MTV. 
A VH-1 documentary contained unseen footage featuring an unreleased song from the Blood Sugar Sex Magik recording sessions that has yet to be identified.  

==References==
 

==External links==
*  

 

 
 
 
 

 