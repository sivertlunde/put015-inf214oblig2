Chicago Deadline
{{Infobox film
| name           = Chicago Deadline
| image          = Chicago deadline poster small.jpg
| image size     = 200px
| alt            =
| caption        = Theatrical release poster Lewis Allen
| producer       = Robert Fellows
| screenplay     = Warren Duff
| based on       =   
| starring       = Alan Ladd Donna Reed
| music          = Victor Young
| cinematography = John F. Seitz
| editing        = LeRoy Stone
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =   
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} thriller directed Lewis Allen. Arthur Kennedy. 

==Plot==
Ed Adams (Alan Ladd) is a reporter for a Chicago newspaper. By chance he is in a boardinghouse just when the body of a dead woman is found by a cleaning lady. The body belongs to a beautiful female tenant named Rosita Jean d’Ur (Donna Reed). Ed is drawn to the dark, mysterious woman, and manages to steal her diary before the law gets to the scene. In the diary Rosita has listed a total of fifty-four names.
 tubercular hemorrhage, but Ed suspects otherwise. His curiosity increases after calling several of the people listed in her diary and not one of them admits to knowing her. Ed talks to hoodlum Solly Wellman, G. G. Temple, the vice-president of a major trust company, and Belle Dorset, and they all deny any knowledge of Rosita. However, Belle Dorset is so shook up by the call that she immediately moves.

Ed gets a lead in this criminal conundrum and follows it to a party. There he meets Leona Purdy, an alluring blonde woman who once knew Rosita, but who fails to reveal anything about the deceased woman. Ed starts dating Leona, but he continues his investigation of what happened to Rosita. Although his co-workers think Rosita was a promiscuous woman, Ed is convinced that she was mistreated because she was compassionate. Ed’s suspicions about the circumstances surrounding her lonely death grow increasingly, especially after both Wellman and Temple start to threaten him.

Tommy Ditman, Rositas brother, eventually approaches Ed and tells him about his once so innocent sister: Rosita was only seventeen years old when she ran away from their home in Amarillo, Texas. She went to San Francisco, and Tommy tracked her down. When he arrived in San Francisco he found out that she had fallen in love with an artist, Paul Jean dUr. They married and moved to New York. Tommy didn’t see his sister very much after that. He informs Ed that her husband Paul died in a car accident some time after their marriage went bad. Tommy says Rosita became lonely and bitter and had difficulty keeping a job.

A local gangster, Blacky Franchot, is the next person to contact Ed about Rosita. They arrange to meet, but Blacky is shot before Ed arrives. Blacky lives long enough for Ed to hear him utter the words "I loved her" before he dies. Ed reports his findings to the city editor, Gribbe, who writes a long gripping column about Rosita, making her life and fate sound sensational and mysterious.

Ed meets with Leona again, and this time she reveals that she and Rosita once shared an apartment in the same building as Blacky. Leona tells the story of how her roommate became involved with Balcky. He was very persistent in his pursuit of her.  Rosita was attracted to him, although she was frightened of his connections to well-known gangster Wellman. She eventually fell in love with Blacky, and resisted the persistent advances of Temple, whom she met at a party. One night she returned home to find Blacky severely beaten, and they decided to move to the countryside together.

Leona explains to Ed that Rosita returned to Chicago when Blacky eventually left her, and started dating Temple. Ed goes to temple with the accusation that he ordered Blackys beatings and had him run off, but Temple denies it. Ed slowly puts the pieces of Rositas life together despite interference from police detective Anstruder. The detective insists on accompanying Ed as he meets with invalid Hotspur Shaner, for whom Rosita worked as a housekeeper under an assumed name. The man who introduced them, John Spingler, is reported murdered, and Ed uses the distraction to get away from the police.

Some time later Ed learns from Rositas former maid, Hazel, that Rosita left Temple one year before her death, after a bitter argument in which Temple hit Rosita. As Ed leaves Hazels building, he is knocked unconscious by two of Wellmans thugs. Ed awakens later in a junkyard. As a cover for his investigation, Ed takes Leona to a boxing match featuring fighter Bat Bennett. Bat and his manager, Jerry Cavanaugh, were the last names to be listed in Rositas book, under Hotspurs address. Just before the fight, Ed finds Jerry and Bat arguing over the newspaper report of Rositas death. When Bat leaves, Jerry reveals to Ed that Bat fell in love with Rosita while she was working for Hotspur. When Bat became distracted by Rosita, Jerry threatened to reveal her identity and hiding place to Wellman if she didnt end the relationship. Rosita reluctantly agreed to do this and then disappeared soon after she quit working for Hotspur. Temple is murdered, and Ed hears rumors that Wellman is responsible for the killing. Ed believes Temple used to finance Wellmans rackets. Ed finds Belle, who admits to him that Wellman originally hired Spingler to get rid of Rosita. Belle denies knowing of Spinglers duplicity until reading the newspaper account of Rositas death the other day.

Ed now knows too much, and Wellman tries to kill him. He shoots at Ed. The police arrive to the scene to his defense, but Wellman escapes. Ed is shot and wounded, but manages to sneak out of a hospital with his co-worker Pigs help. He questions Belle, who has been arrested. Belle reveals the missing link in Rositas history: On the night of their argument, Temple admitted to Rosita, whom he was supporting, that he hired Wellman to get rid of Blacky. During the ensuing argument between the two of them, Temple struck Rosita down and panicked when she appeared to be dead. Temple then called for Wellman to help him.

Ed leaves the police station with this information, when Wellman corners them in a parking garage. A shootout starts and Ed manages to kill Wellman in self-defense. At Rositas small funeral, Ed tells Tommy the story of what really happened to his sister, and then burns Rositas diary in the funeral parlors "eternal" flame.

==Cast==
 
 
* Alan Ladd as Ed Adams
* Donna Reed as Rosita Jean dUr
* June Havoc as Leona
* Irene Hervey as Belle Dorset Arthur Kennedy as Tommy Ditman
* Berry Kroeger as Solly Wellman
* Harold Vermilyea as Anstruder
* Shepperd Strudwick as Blacky Frenchot
 
* Dave Willock as Pig Gavin Muir as G.G. Temple John Beal as Paul Jean dUr
* Tom Powers as Glenn Howard
* Howard Freeman as Hotspur Shaner
* Paul Lees as Bat Bennett
* Margaret Field as Minerva
 

==Production==
Paramount had owned the rights to the novel One Woman for over ten years before turning it into a film for Alan Ladd. 

==Reception==
===Critical response===
When the film was released Stephen O. Saxe, writing for The Harvard Crimson, recommended the film due to the acting, the action and suspense. He wrote, "Chicago Deadline is a picture with a Twist. Its not an O. Henry twist, either ...   a good plot with plenty of suspense, and, in due course, lots of action ... Chicago Deadline is not the sort of picture youd go out of your way to see; but once inside, you wont walk out, either." 

Film critic Bosley Crowther dismissed suspension of disbelief in his review, "People who picture reporters as dashing young fellows, all named Scoop, whose lives are just rounds of excitement in what such people call the newspaper game will find the ideal of their illusion in the newshawk Alan Ladd plays in Paramounts Chicago Deadline ... But for those other level-headed people whose knowledge of newspaper men—and, indeed, of life in general—is a little more sober and sane, this fancy will surely seem a mish-mosh of two-penny-fiction cliches, recklessly thrown together in an almost unfathomable plot. Flashbacks and narrative descriptions will fascinate them no more than will Mr. Ladds ridiculous posturing as a brilliant newspaper man-sleuth."  

In his review of the film, Dennis Schwartz, compared the film to Otto Premingers Laura (1944 film)|Laura (1944), "In a Laura type of minor film noir, director Lewis Allen fails to make his love sick hero who is mooning over a corpse into anything but a superhero figure ... The film failed to make his cop character as inviting as Laura made Dana Andrews.   

===Accolades===
Nomination
*  ; 1950. 

==References==
 

==External links==
*  
*  
*  
*  

===Streaming audio===
*   on Screen Directors Playhouse: March 24, 1950

 
 
 
 
 
 
 
 
 
 