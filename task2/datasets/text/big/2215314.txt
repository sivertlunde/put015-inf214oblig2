Blue Fin
 Bluefin}}
 
 
{{Infobox film
|  name           = Blue Fin |
  image          = Blue Fin 1978DVD.png|
  image_size     =|
  caption        = DVD cover|
  writer         = Sonia Borg| 
  based on = novel by Colin Thiele |
  starring       = Hardy Krüger, Greg Rowe, Elspeth Ballantyne|
  director       = Carl Schultz | Hal McElroy|
  studio = South Australian Film Corporation McElroy and McElroy |
  distributor    = Pacific International Enterprises|
  released       = November 1978|
  runtime        = 95 minutes |
  country        = Australia |
  language       = English |
  music          = |
  awards         = |
  budget         = AU $750,000 |
 gross = AU $703,000 (Australia) |
}}

Blue Fin is a 1978 family movie that stars Hardy Krüger, Greg Rowe and Elspeth Ballantyne.  It is based on an Australian novel written by Colin Thiele and published in 1969.

==Plot==
  Port Lincoln. Hes thin and long-faced, like the fish hes named after. At school hes no good at sport and, at home, his father scorns him. Snook joins his father and fellow crewmen on a tuna-fishing expedition, when disaster strikes. It is up to Snook to save himself and his father from a desperate situation.

==Cast==
*Hardy Krüger ... Bill Pascoe
*Greg Rowe ... Steve "Snook" Pascoe
*Liddy Clark ... Ruth Pascoe
*Elspeth Ballantyne ... Mrs. Pascoe
*John Jarratt ... Sam Snell

==Production==
The film is an unofficial follow up to Storm Boy with the same writer and star, also adapted from a Colin Thiele novel. The South Australian Film Corporation (SAFC) did not want to use Henri Safran as director, though, so employed another director from the ABC, Carl Schultz. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p271-272 

The film was shot near Streaky Bay in mid 1978. It was a difficult production and editor Rod Adamson claimed the film would not cut together. Five weeks after filming had completed, Schultz had to leave the film to take up a directing job at the ABC. Accordingly, Matt Carroll of the SAFC called in Bruce Beresford, who was under contract to them, to re-shoot some sequences. Some of these had to be done using a body double for Hardy Kruger since he had returned to Europe.  Schultz was supportive of Beresford stepping in but was unhappy with the fact he supervised the final re-cut. Peter Beilby & Rod Bishop, "Carl Schultz", Cinema Papers, Jan-Feb 1979 p242 

==DVD release==
A DVD was released on 1 January 2003.

==See also==
 
*Cinema of Australia
*South Australian Film Corporation

==References==
 

==External links==
*  
*  at Oz Movies
*   at the National Film and Sound Archive
* 
*  at The New York Times
* 

 

 
 
 
 
 
 
 
 
 
 


 
 