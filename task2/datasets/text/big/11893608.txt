Pinocchio's Revenge
  
{{Infobox film
| name = Pinocchios Revenge
| image = Pinocchios Revenge.jpg
| alt = 
| caption = Theatrical release poster
| director = Kevin S. Tenney
| producer = Jeff Geoffray Walter Josten
| screenplay = Kevin S. Tenney
| based on =
| starring = Rosalind Allen Todd Allen Aaron Lustig Ron Canada Candace McKenzie Lewis Van Bergen Larry Cedar Introducing Brittany Alyse Smith
| music = Dennis Michael Tenney
| cinematography = Eric Anderson
| editing = Daniel Duncan
| studio = Trimark Pictures
| distributor = Trimark Pictures  Blue Rider Productions
| released =  
| runtime = 96 minutes
| country = United States
| language = English
| budget =
| gross =
}} Canadian actor Todd Allen who were married from 1990 to 2005.  The production, structured as a direct-to-video effort, concerns a lawyer who brings home a wooden puppet that was found buried with a boy supposedly killed by his father. Her 11-year-old daughter Zoe sees the doll and takes it as her own. Soon accidents start happening and Jennifer struggles to find the cause as she begins to question her daughters well being and whether or not there may be something sinister to the doll.             

==Plot==
Despite the evidence presented by the district attorney (Larry Cedar), Jennifer Garrick (Rosalind Allen), the lawyer defending Vincent Gotto (Lewis Van Bergen), an accused child murderer on death row, believes her client is not guilty, and is hiding the identity of the real killer. A fellow attorney in her office (Ron Canada) explains the presence of a large Pinocchio doll sitting in her chair as belatedly delivered evidence which she had earlier requisitioned (the doll had been buried by her client in his sons grave). Intending to examine it in the hope of finding a clue which might prevent his execution, she brings it home and her emotionally fragile daughter Zoe (Brittany Alyse Smith) mistakes it for a birthday gift. She develops a relationship with the puppet and becomes unbalanced to an even greater degree. 

Soon, she even believes the doll to be real and talk with it, although this is not out of the ordinary as she held a similar relationship with her other dolls. Trouble takes off when a school mate of Zoe who bullied her is pushed in front of a bus, which Zoe blames on Pinocchio trying to protect her. Soon after, Jennifers boyfriend David Kaminsky (Todd Allen) is knocked down the basement stairs while baby sitting Zoe, but is saved by Zoe calling 911. Later, Zoe is at one of her therapy sessions when her psychiatrist Dr. Edwards (Aaron Lustig) has to leave the room, and Zoe begins talking with Pinocchio about who is to blame for Davids accident, with both placing blame on one another. 

A surveillance video in the room is watched by the mother and the psychiatrist and it is revealed that Zoe is talking to herself. That night, Pinocchio convinces Zoe to set him free so that he can admit to David that he is to blame for his accident. Zoe makes him promise he will not do anything bad and cuts his strings, at which point Pinocchio hops up, declaring his freedom and takes off down the dark streets with Zoe in pursuit. Through a first-person perspective, we see an unknown person move through the hospital through crowds of people into Davids room and unplug one of his machines, killing him. 

Jennifer questions Zoe who claims she got lost as she and Pinocchio try to find the hospital and never went there which causes an angry and confused Jennifer to lock Pinocchio in the trunk of her car. That night, Zoe is left in the care of the babysitter Sophia (Candace McKenzie), when Sophia reminds her that Zoe gave Pinocchio a conscience (a cricket caught earlier in the film). Zoe runs to her room to check on it and finds it smashed and begins screaming. Sophia runs to make sure she is okay where she is struck by a fireplace poker from an unknown assailant until she is dead. Jennifer arrives home that night during a thunderstorm to find the babysitter dead and Zoe standing in a dark hallway quiet. When Jennifer tries to confront Zoe, she runs away in a panic. As Jennifer explores the house, she is struck by the poker and sees her daughter standing above her with it in her hand.

Her daughter explains that she just managed to get the poker away from Pinocchio but they must escape quickly, but before Jennifer can inquire further, Zoe is gone. Jennifer stands up to see Pinocchio standing in the room, at which point he suddenly turns towards her and attacks her with a knife, following with a chase and battle through the house. Jennifer throws Pinocchio through a glass coffee table and sees that her daughter is suddenly lying there in his place. The movie closes with a catatonic Zoe being committed and Jennifer stating it was not her and that she will not give up until she gets better and can leave, to which Dr. Edwards states, "I hope not, for your sake, I hope not."

==Cast==
 
 
* Rosalind Allen
* Todd Allen
* Aaron Lustig
* Ron Canada
* Candace McKenzie
* Lewis Van Bergen
* Larry Cedar
* Introducing Brittany Alyse Smith
 
===End credits  ===
* Lewis Van Bergen...Vincent Gotto
* Ivan Gueron...Rookie Patrolman
* Thomas Wagner...Homicide Detective
* Janis Chow...Newscaster
* Larry Cedar...District Attorney
* Janet MacLachlan...Judge Allen
* Rosalind Allen...Jennifer Garrick
* Brittany Alyse Smith...Zoe Garrick
* Ron Canada...Barry
* Sarah Kaite Coughlan...School Teacher
* Tara Hartman...Beth
* Danielle Wiener Carianne Goldsmith...Beths Friends
 
* Jose Rey...Prison Guard
* Candace McKenzie...Sophia
* Aaron Lustig...Dr. Edwards
* Ian Gregory...Prison Chaplain
* Michael Connors...Young Priest
* Todd Allen...David Kaminsky
* Dani Blair...Mother at Party
* Larry Ziegelmeyer...Principal
* Robert Winley...Biker
* Shelley Robertson...Nurse
* James W. Quinn...Paramedic
* Verne Troyer...Pinocchio Double
* Sal Viscuso Ed Bernard...Jail Guards  
* and Dick Beals as the Voice of Pinocchio
 

==Tagline==
* "Evil comes with strings attached"

==See also==
* Killer Toys

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 