Come Back, Charleston Blue
{{Infobox film
| name = Come Back, Charleston Blue
| image = Come Back, Charleston Blue.jpg
| image_size = 
| caption = Theatrical release poster 
| director = Mark Warren
| producer = Samuel Goldwyn Jr.
| writer = Peggy Elliott Bontche Schweig Chester Himes (novel The Heats On)
| narrator = 
| starring = Godfrey Cambridge Raymond St. Jacques
| music = Donny Hathaway
| cinematography = Richard C. Kratina
| editing = George Bowers Gerald B. Greenberg
| distributor = Warner Bros. Pictures
| released =  
| runtime = 100 minutes 
| country = United States
| language = English
| budget = 
| gross = 
}}
Come Back, Charleston Blue is a 1972 film starring Godfrey Cambridge and Raymond St. Jacques, loosely based on Chester Himes novel The Heats On.  It is a sequel to the 1970 film Cotton Comes to Harlem.

==Plot==
Coffin Ed Johnson & Gravedigger Jones are confounded by a string of strange murders in the  .  Legend has it that this was the calling card of Charleston Blue, a vigilante who tried to rid the neighborhood of all criminal elements using a straight razor. Blue, having disappeared years ago after he went after Dutch Schultz (with his trusty straight razor) was considered dead by all except his girlfriend, who kept his razors locked away until his "come back."

Soon after the murders start it is discovered that the razors were missing and all evidence points to Joe Painter, a local photographer, who has begun dating Carol, the beloved niece of mafia errand boy, Caspar Brown.  Joe and Brown are at odds over Caspars refusal to help Joe kick the mafia out of the neighborhood, so Joe enlists the help of a group of brothers and the spirit of Charleston Blue. However, Coffin Ed Johnson & Gravedigger Jones discover that Joes plan doesnt seem to be exactly what he claimed it was.

==Production== CORE and other groups over their demands for "money, jobs and control."   

==Reception==
This film was a sequel to the film Cotton Comes to Harlem: appearing two years later, it opened to mixed reviews, with critics feeling it was decent, but not riotous like the original 1970 film. Cool Breeze, Super Fly, and Blacula. 

A. H. Weiler, reviewing the film for The New York Times, called it "only occasionally funny or incisive" with a "convoluted plot and dialogue that is often too in for the uninitiated."   

==Soundtrack==
All tracks written by Donny Hathaway except "Little Ghetto Boy" (Earl DuRouen /  Edward Howard) and "Come Back Charleston Blue" (Al Cleveland /  Donny Hathaway /  Quincy Jones). 

{| class="wikitable"
|-
! Track
! Song
! Length
|- 1
|Main Theme
|02:20
|- 2
|Basie
|03:53
|- 3
|String Segue
|00:34
|- 4
|Vegetable Wagon
|01:07
|- 5
|Harlem Dawn
|01:38
|- 6
|Scratchy Record
|03:09
|- 7
|Explosion
|00:23
|- 8
|Hearse To The Graveyard
|02:46
|- 9
|Switch "Charleston Blue"
|00:32
|- 10
|Come Back Basie
|02:36
|- 11
|Detectives Goof
|00:28
|- 12
|Gravedigger Jones & Coffin Head Johnson’s Funeral
|03:02
|- 13
|String Segue
|00:17
|- 14
|Little Ghetto Boy
|03:50
|- 15
|Hail To The Queen
|00:21
|- 16
|Drag Queen Chase
|00:47
|- 17
|Bossa Nova
|01:47
|- 18
|Tims High
|01:30
|- 19
|Furniture Truck
|01:18
|- 20
|Liberation
|02:52
|- 21
|Come Back Charleston Blue
|02:04
|}

In November 2007, Rhino Records released a remastered version of the soundtrack album, which included two new tracks, an alternate version and a live version of "Little Ghetto Boy." 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
   
 