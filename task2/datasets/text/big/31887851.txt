Dharmputra
 
{{Infobox film
| name           = Dharmputra
|producer=B.R. Chopra
| director       = Yash Chopra
|writer=B.R. Films Story Department (screenplay) Akhtar-Ul-Iman (dialogue)
| dialogue       = Akhtar-Ul-Iman   based on= 
| story          =
| starring       = Mala Sinha Shashi Kapoor
| music          = N. Dutta Sahir Ludhianvi (lyrics)
|studio=B.R. Films
| cinematography = Dharam Chopra
| editing        = Pran Mehra
| released       = 1961
| country        = India
| language       = Hindi
}}
Dharmputra is a   (1959), steeped in Nehruvian Secularism in India|secularism, wherein a Muslim brings up an illegitimate Hindu child and featured classic song, Tu Hindu banega na Musalman banega, insaan ki aulaad hai, insaan banega. The theme was reversed in this film as herein a Hindu family brings an illegitimate Muslim child. 
 Best Feature Film in Hindi.   

== Synopsis ==
The film opens in 1925, during the British rule in India & at the height of Indian independence movement is the tale of two Delhi families, that of Nawab Badruddin and Gulshan Rai. The two families are so close that they virtually share the same house. The Nawabs daughter, Husn Bano, has an affair with a young man named javed & gets pregnant. When the Nawab attempts to arrange her marriage with javed, he finds that javed has disappeared. Amrit Rai and his wife Savitri assist Husn with the birth of a baby boy, Dilip, and even adopt him and give him their family name. Young Dilip is the apple of the Badruddin and the Rai households. Husn then gets married to another young man, Javed and moves to another location. In the meantime, while participating in a protest to force the British to leave India the Nawab is killed. Years later, Husn Bano and Javed return to a warm welcome by the Rai family. Then she meets Dilip - not the Dilip she had left behind - this Dilip is fascist, a Muslim-hater, who has joined forces with other extremists, in order to force Muslims to leave India and even go to the extent of burning buildings and killing them. How can Husn and Dilip adapt to each other with so much hate and distrust between them?

== Cast ==
* Mala Sinha as Husn Bano
* Shashi Kapoor as Dilip Rai Rehman as Javed
* Manmohan Krishna as Dr. Amrit Rai
* Indrani Mukherjee as Meena
* Tabassum as Rekha Rai
* Deven Verma as Sudesh Rai
*Nirupa Roy as Mrs. Savitri Amrit Rai
* Leela Chitnis as Meenas mom
* Ashok Kumar asa Nawab Badruddin (Husn Banos Father)
* Rajendra Kumar (special appearance)

==Soundtrack==
The films music was composed by N. Dutta with lyrics written by Sahir Ludhianvi. The film also features a patriotic song, Sare Jahan Se Accha, written by poet Muhammad Iqbal, in the opening title sequence, in the voices of Mohammed Rafi and Asha Bhosle. 
{| class="wikitable"
|-
!Track
!Song
!Performed By
|- 1
|Main Jab Bhi Akeli Hoti Hoon Asha Bhosle
|- 2
|Aaj Ki Raat  Mahendra Kapoor
|- 3
|Naina Kyon Bhar Aaye Asha Bhosle
|- 4
|Tumhari Aankhen Mahendra Kapoor
|- 5
|Jo Dil Deewana Machal Gaya Mohammed Rafi, Chorus
|- 6
|Yeh Kis Ka Lahu Hai Kaun Mara Mahendra Kapoor, Chorus
|- 7
|Jai Janani Jai Bharat Maa Mahendra Kapoor, Chorus
|- 8
|Chahe Yeh Mano Chahe Woh Mano Mahendra Kapoor, Balbir, Chorus
|}

==Reception==
The raw reconstruction of partition riots and sloganeering led to riot-like situations at theatres screening the film,    and the film flopped at the box office.  After Dharmputras debacle, few directors ventured into communal theme in Hindi cinema that too so bluntly, next film which dealt with the issue was Garm Hava by M.S. Sathyu, made in 1973.    Films director Yash Chopra never made a political film again, stuck to love stories till many decades later, when he touched the theme of religious harmony again with Veer Zaara (2004) .  

== Awards == National Film Awards
*     
;Filmfare Award
* 1963:   

== References ==

 
*  

==External links==
*  

 
 

 
 
 
 
 
 
 
 