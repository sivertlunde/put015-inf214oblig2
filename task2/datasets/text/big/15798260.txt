Danger Signal
{{Infobox film
| name           = Danger Signal
| image          = Danger Signal Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Florey
| producer       = William Jacobs
| screenplay     = {{plainlist|
* Charles Graham Baker|C. Graham Baker
* Adele Comandini
}}
| based on       =  
| story          =  Phyllis Bottome
| starring       = {{plainlist|
* Faye Emerson
* Zachary Scott
}}
| music          = Adolph Deutsch
| cinematography = James Wong Howe
| editing        = Frank Magee
| distributor    = Warner Bros.
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Danger Signal is a 1945 film noir starring Faye Emerson and Zachary Scott. It was adapted from the novel of the same name by Phyllis Bottome.

==Plot==
A mysterious artist - and psychopath - named Ronnie Mason, steals a dead womans wedding ring and money and leaves a fake suicide note. Her husband, Thomas Turner, when questioned by the local police, believes his dead wife might have been seeing Mason behind his back. He also believes his wife was murdered, but in the absence of other evidence, list it as a suicide and drop the case.

Mason leaves town, changes his name to Marsh and, using a limp he acquired jumping from the dead womans bedroom window and a veterans pin he steals from a fellow passenger on the L.A. bus, passes himself off as a wounded soldier and rents a room in the house of public stenographer Hilda Fenchurch and her younger sister Anne. To the consternation of professor Andrew Lang, who secretly loves Hilda, she falls for Marsh, the new tenant.

The scheming Marsh learns that it is Anne who might inherit a great deal of money, so he suddenly switches his affections toward her. Hilda is jealous and suspicious. She plots to lure Marsh to a beach house and poison him. She isnt able to go through with it, but when Marsh runs off, he is surprised by Thomas Turner and plunges off a steep cliff to his death.

==Cast==
* Faye Emerson as Hilda Fenchurch
* Zachary Scott as Ronnie Mason
* Richard Erdman as Bunkie Taylor (as Dick Erdman)
* Rosemary DeCamp as Dr. Jane Silla
* Bruce Bennett as Dr. Andrew Lang
* Mona Freeman as Anne Fenchurch
* John Ridgely as Thomas Turner
* Mary Servoss as Mrs. Fenchurch
* Joyce Compton as Kate
* Virginia Sale as Mrs. Crockett

==Reception==
Bosley Crowther, the film critic for The New York Times, panned the film and called it a "diluted little melodrama" in which the filmmakers resort to a car chase in order to relieve boredom. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 