Requiem for a Gunfighter
{{Infobox film
| name           = Requiem for a Gunfighter
| image          = Requiem for a Gunfighter.jpg
| image_size     = 
| border         = Yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Spencer Gordon Bennet Alex Gordon
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Rod Cameron Dick Jones
| music          = Ronald Stein
| cinematography = Frederick West
| editing        = Charles Powell
| studio         = Premiere Productions
| distributor    = Embassy Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 western film, directed by produced by Alex Gordon, Rod Cameron, Dick Jones, and Olive Sturgess.
 syndicated television series between 1954 and 1961, was "making sort of a comeback at this time. He was very gracious; very kind. You can see it in him in the scene when we are having dinner—his look. He was a professional."   

Requiem used several older actors, including Tim McCoy. Dick Jones, years earlier the voice of Pinocchio, closed his acting career in the film as the gunfighter, Cliff Fletcher.  Mike Mazurki brought humor to the picture. The snake which he placed in the saddlebag of Camerons horse was artificial but made so real-looking that viewers would not have known the difference. 

==Cast==

*Rod Cameron ...........Dave McCloud
*Stephen McNally .... ...Red Zimmer
*Mike Mazurki........ ...Ivy Bliss
*Olive Sturgess..........Bonnie Young
*Dick Jones    ..... ....Cliff Fletcher
*Tim McCoy     ..........Judge Irving Short
*Chet Douglas  ..........Larry Young Bob Steele..........Max Smith
*Johnny Mack Brown...Enkoff
*Chris Hughes   .........Billy Parker

==See also==
 
* Bob Steele filmography

==References==
 

==External links==
*  

 
 
 
 


 