Punnagai Mannan
{{Infobox film
| name           = Punnagai Mannan
| image          = Punnagai Mannan dvd.jpg
| image_size     =
| caption        = Poster
| director       = K. Balachander
| producer       = Rajam Balachander Pushpa Kandaswamy
| writer         = K. Balachander
| narrator       = Rekha Srividya
| music          = Ilaiyaraaja
| cinematography = Ragunatha Reddy
| editing        = Ganesh-Kumar
| studio         = Kavithalayaa Productions
| distributor    = Kavithalayaa Productions
| released       = 1 November 1986 
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          = 
| preceded_by    =
| followed_by    =
| website        =
}}
 Telugu as Dance Master.  The film was later dubbed into Hindi as Cha Cha Charlee in 1996. This was Revathis last tamil film before her marriage to cinematographer Suresh Chandra Menon in 1986.

==Plot==
 
Sethu (Kamal Haasan) and Ranjani (Rekha (Tamil actress)|Rekha) arrive at a cliff where they plan to commit suicide due to the pressure put up by Ranjanis parents against their marriage. Sethu who is the son of an ordinary drunkard cook (Delhi Ganesh) and is also a dancer is not accepted as groom for Ranjani by her parents. The parents threaten Ranjani that either they would kill Sethu or commit suicide. While jumping from the cliff Sethu accidentally gets trapped in a tree and escapes but Ranjani dies. Despite the attempts made by Ranjanis father to jail Sethu by framing a fault case that Sethu killed Ranjani, no strong evidence was there to prove it. He is jailed for the offence of attempted suicide for one year.

After a year, he is released from jail. Padmini (Srividya) who owns a dance school provides him a job of dance master as he is the former student. Sethu visits regularly the cliff as a remembrance of his love. On his way he sees a girl (Revathi) who attempts suicide for silly reason of failure in examination and he stops her from it. He again meets the girl in a gallery. The girl introduces herself to him as Malini but he ignores her. Malini again meets Sethu at a tourist spot where she clicks photos without his knowledge, but Sethu finds out and damages her camera. Malini chases him to his dance school as his student. Sethu repeatedly insults her but Malini develops a love for Sethu.
 Sinhalese girl because of which she faces trouble from her class mate many times and every time rescued by Sethu. Sethu though humiliates her all the time develops a soft corner in his mind. Sethu could realise that he had started to love her but gets confused what to do as he is a failed one. Sethus repeated attempts to prevent himself loving Malini fails and he takes a sign of blessing for his new love from Ranjani at the cliff side. He finally confesses his love for her and proposes to her.

Malini continues to spend time with Chaplin Chellapa as she enjoys his sense of humour. This makes Sethu jealous, so he attempts to mimic Chaplin Chellapas mannerisms&nbsp;— but ends up having an accident. Sethu finds that Chaplin Chellapa is a failed lover, who to forget his past has worn a mask of Charlie Chaplin. Sethu and Malini unite Chelapa and Padmini who is a spinster. Sethu again faces pressure from Malinis father as Malini is of Sinhalese origin and Sethu is a Tamilian. Both of them struggle hard to win in their love. Malini goes to the extent of escaping from Sri Lanka to Tamil Nadu to join with Sethu. After understanding the tight bond of Malini with Sethu, Malinis father approves their marriage.

Ranjanis father who hears about Sethus marriage wants to take "revenge" for his daughters death. On the day of Sethus engagement, he gives a basket full of apples which has a time bomb in it to Sethus father pretending to be his gift for his son. Sethus father who does not know about the bomb keeps the basket in Chaplin Chellapas car. Sethu and Malini starts to visit Sethus love cliff in Chellapas car which has the bomb. Sethus father and Chellapa who come to know about this, start chasing them to stop but in vain. Tha bomb explodes at the same place where Sethu-Ranjani attempted suicide and Sethu-Malini die in the explosion, leaving Chellapa and Sethus father heartbroken. Chellapa later showers flowers in the place where the three of them died.

==Cast==
* Kamal Haasan as Sethu and Chaplin Chellapa Revathi as Malini
* Srividya as Padmini, Chaplin Chellapas wife Rekha as Ranjani, Sethus ex-lover
* Delhi Ganesh as Sethus Father
* Sundar Krishna Urs as Malinis Father
* R.N.Sudarshan as Ranjanis Father
* K. S. Jayalaxmi as Malinis Mother

==Production== Kala as a choreographer to fill in for the busy Raghuram, her brother-in-law.  Suresh Krissna who worked as one of the assistants for the film recalled that regarding the picturisation of the song "Mamavukku", Kamal came up with the novel idea of lip sync being perfect while dance movements being fast. Suresh later used this idea in one of the song sequences in his film Annamalai (film)|Annamalai (1992). 

==Soundtrack==
Music is composed by Ilaiyaraaja, with lyrics by Vairamuthu.  A. R. Rahman (then known as Dileep), who then worked with Ilayaraja as a guitarist, played keyboard for this album  and because it was the first time a Tamil film used a Music sequencer|sequencer, the music became informally known as "computer music" in Tamil Nadu.  Despite the albums success, Ilaiyaraaja and Vairamuthu parted ways later.   Ilayarajas son Karthik Raja recalled that CX5M system was used around the time of the making of the film.  Vairamuthu praised Balachander for the song "Enna Satham" stating that he had the guts to insert a song in a sequence where two lovers were going to die. 

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
! No. !! Song !! Singers !! Duration
|-
| 1|| "Enna Saththam Indha Neram" || S.P. Balasubrahmanyam || 04:17
|-
| 2|| "Kaalakaalamaaga Vaazhum" || S.P. Balasubrahmanyam, K. S. Chithra || 04:21
|-
| 3|| "Singalathu Chinnakuyile" || S.P. Balasubrahmanyam, K. S. Chithra || 04:16
|-
| 4|| "Maamaavukku Kudumaa Kudumaa" || Malaysia Vasudevan || 04:34
|-
| 5|| "Yedhedho Ennam Valarthen" || K. S. Chithra || 04:23
|-
| 6|| "Vaan Maegam Poo Poovaai" || K. S. Chithra || 03:53
|-
| 7|| "Kavidhai Kelungal Karuvil" || Vani Jairam, P. Jayachandran || 07:00
|-
| 8|| "Theme Music" || Instrumental || 02:34
|-
| 9|| "One Two Three" || Francis Lazarus, (Lyrics: Vijay Manuel) || 01:27
|}

==Release== Diwali Day. It was released alongside Rajinikanths Maaveeran, and was a Superhit at the box office. The film successfully ran in theatres for 25 weeks. On the films 25th week celebrations, Kamal Haasan was given the title Puratchi Mannan ("Revolutionary King") by his "mentor" K. Balachander. 

In February 2013, entertainment magazine Galatta Cinema said, While Kamal and Revathy do a fab job, its Ilayaraajas music that plays a key character in bringing their love even closer to us..."  Indiaglitz described it as a path breaker and also went on to say that Love is beyond past, future, history, sorrow, background and age.  Behindwoods included the film in its list "Top 20 Best Love Stories in Tamil" stating that it is "an ode to love". 

==In popular culture== 2014 film Vivek performed a spoof on the films suicide scene with Malavika in Singakutty (2008).  In Maan Karate (2014), when Peter (Sivakarthikeyan) wants to break his hand, his friend utters "1-2-3" similar to Kamal from the film. 

==References==
 

==External links==
*  

==Bibliography==
*  

 
 

 
 
 
 
 
 
 