Nick Offerman: American Ham
{{Infobox film
| name           = Nick Offerman: American Ham
| image          = 
| alt            = 
| caption        = Film poster
| director       = Jordan Vogt-Roberts
| producer       = Julien Lemaitre
| writer         = Nick Offerman
| starring       = Nick Offerman
| music          = Ryan Miller	
| cinematography = Matthew Garrett Ross Riege
| editing        = Alex Gorosh Josh Schaeffer	
| studio         = Six Two and Even Production
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
}}
Nick Offerman: American Ham is a 2014 American Stand-up comedy|stand-up comedy film, directed by Jordan Vogt-Roberts. It is written by American actor, writer, and carpenter, Nick Offerman.   The film had its world premiere at 2014 Sundance Film Festival on January 23, 2014.  

==Plot== Town Hall theater, featuring a collection of anecdotes, songs and woodworking/oral-sex techniques.

==Cast==
*Nick Offerman as himself
*Megan Mullally as herself (cameo)
*Marc Evan Jackson as an intellectual property attorney

==Reception==
Nick Offerman: American Ham received mostly positive reviews from critics. Geoffrey Berkshire of Variety (magazine)|Variety, said in his review that "Nick Offermans affable stage presence carries this fairly routine standup concert film."  John DeFore in his review for The Hollywood Reporter said that "The stand-up film wont please all fans of Offermans acting work."  Kyle Burton of Indiewire graded the film a B-, saying that ""American Ham" is a solid comedy special—with stylish prerecorded title sequences for each new Tip—but it lacks some of the inventiveness or surprise that might otherwise warrant a slot at a major film festival. But that doesnt negate the way Offerman’s honest Americanist attitude regularly leads to laughs and insight." 

==References==
 

==External links==
*  
*  

 
 
 


 