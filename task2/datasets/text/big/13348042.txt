Hotel (2001 film)
 
 
{{Infobox film
| name           = Hotel
| image_size     = 
| image	=	Hotel FilmPoster.jpeg
| caption        = 
| director       = Mike Figgis
| producer       = Andrea Calderwood Mike Figgis Annie Stewart Lesley Stewart Ernst Etchie Stroh
| writer         = Mike Figgis Heathcote Williams
| narrator       = 
| starring       = Salma Hayek Lucy Liu Burt Reynolds
| music          = Mike Figgis  Anthony Marinelli
| cinematography = Patrick Alexander Stewart 
| distributor    = 
| released       =  
| runtime        = 114 minutes 
| country        = Italy United Kingdom 
| language       = English
| budget         = 
| gross          = $29,813 
| preceded_by    = 
| followed_by    = 
}}
Hotel is a 2001 experimental thriller film directed by Mike Figgis.

==Plot==
While a British film crew are shooting a version of The Duchess of Malfi in Venice, they in turn are being filmed by a sleazy documentary primadonna while the strange hotel staff share meals which consist of human meat. The story expands to involve a hit man, a call girl and the Hollywood producer. 

The film itself makes several mentions of the Dogme 95 style of filmmaking, and has been described as a "Dogme film-within-a-film."      

== Reception ==

The film was not a financial success and received mixed reviews. Roger Ebert noted this and pointed out the complex nature of the film:

: "Many critics have agreed that "Hotel" is not successful, but I would ask: Not successful at what? Before you conclude that a movie doesnt work, you have to determine what it intends to do. This is not a horror movie, a behind-the-scenes movie, a sexual intrigue or a travelogue, but all four at once, elbowing one another for screen time. It reminds me above all of a competitive series of jazz improvisations, in which the musicians quote from many sources and the joy comes in the way theyre able to keep their many styles alive in the same song.... The movie has to be pointless in order to make any sense." 

==Cast==
*Saffron Burrows ...  Duchess of Malfi 
*Valentina Cervi ...  Hotel Maid 
*George DiCenzo ...  Boris 
*Christopher Fulford ...  Steve Hawk 
*Jeremy Hardy ...  Flamenco Troupe Administrator 
*Salma Hayek ...  Charlee Boux 
*Danny Huston ...  Hotel Manager 
*Rhys Ifans ...  Trent Stoken 
*Jason Isaacs ...  Australian Actor
*Lucy Liu ...  Kawika 
*Mía Maestro ...  Cariola 
*John Malkovich ...  Omar Jonnson 
*Chiara Mastroianni ...  Hotel Nurse 
*Laura Morante ...  Greta 
*Burt Reynolds ...  The Flamenco Manager
*David Schwimmer ...  Jonathan Danderfine 
*Mark Strong ...  Ferdinand
*Julian Sands ... Tour Guide 
*Ornella Muti ... Flamenco Spokesperson 
*Stefania Rocca ... Sophie 
*Fabrizio Bentivoglio ... Very Important Doctor
*Brian Bovell ... Cardinal 
*Christopher Fulford ... Steve Hawk
*Valeria Golino ... Italian Actress

==References==
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 