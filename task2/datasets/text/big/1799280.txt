Steaming (film)
{{Infobox film
| name           = Steaming
| image          = Poster of the movie Steaming (1985 film).jpg
| image_size     = 
| caption        = 
| director       = Joseph Losey
| producer       = 
| writer         = Nell Dunn (play) Patricia Losey
| narrator       = 
| starring       = Vanessa Redgrave Sarah Miles Diana Dors Brenda Bruce
| music          = Richard Harvey
| cinematography = Christopher Challis 
| editing        = Reginald Beck
| studio         = World Film Services
| distributor    = New World Pictures 1985
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} play by Patricia Losey and Nell Dunn. It was also the last film of actress Diana Dors, who died in 1984. The film was screened out of competition at the 1985 Cannes Film Festival.   

The story centred on three women who meet regularly in a steam room and decide to fight its closure. The cast was headed by Vanessa Redgrave and Sarah Miles, along with Diana Dors.

==Cast==
* Vanessa Redgrave - Nancy
* Sarah Miles - Sarah
* Diana Dors - Violet
* Patti Love - Josie
* Brenda Bruce - Mrs. Meadows
* Felicity Dean - Dawn Meadows
* Sally Sagoe - Celia
* Anna Tzelniker - Mrs. Goldstein

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 