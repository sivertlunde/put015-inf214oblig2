Pelleedu Pillalu
{{Infobox film name           = Pelleedu Pillalu image          = Pelleedu Pillalu.JPG director  Bapu
|producer       = D. Madhusudhana Rao production     = Annapoorna Kalanikethan, Annapoorna Pictures writer         = Mullapudi Venkata Ramana (Dialogue) story          = Mullapudi Venkata Ramana dialogues      = Mullapudi Venkata Ramana lyrics  Sri Sri, Aathreya starring       = Suresh Vijayashanti Sangeetha, Sarath Babu, J.V. Somayajulu, Rama Prabha cinematography = Baba Azmi editing        = G.R. Anil Dattatreya distributor    = Music World (India, VCD) released       =   runtime        =  language  Telugu  music          = M. S. Viswanathan
}}
 Sri Sri and Aathreya.

Actor Suresh was introduced to the Telugu screen through this film.

==Plot==
On her deathbed, the sister of P.V. Rao (J.V. Somayajulu) entrusts him the custody of her property along with daughters Durga (Sangeetha) and Shanti (Vijayashanti). After her death, he usurps the property and arranges Durgas marriage with a pauper Chalapati (Sarath Babu). After learning maternal uncles intentions, Durga prepares and sells household items along with Shanti for livelihood. Learning that Raos son (Suresh) is her cousin, Shanti starts teasing him along with her friend Annapoorna (Sumalatha). After learning their relation, he too starts liking Shanti. Upon hearing about Shanti and his son, Rao accuse the sisters of usurping his property through his son. This enrages Durga, who starts an eatery in front of her uncles hotel and develops it into a hotel to pull down his business. Rao is bedridden with his business going down. Meanwhile Durga fixes Shantis wedding with Chandram (Sai Chand), Ananapoorna s love interest. When Shanti learns this, she informs Rao about her intent to wed his son and requests his help in this matter. Reformed Rao assures her of this and instructs Shanti to follow her sister. On the wedding day, with help of Chalapati, Annapurna and her mother (Rama Prabha), Rao arranges the wedding of Shanti with his son and that of Annapoorna with Chandram. Durga gets angered  at the turn of events. The story ends on a happy note with everyone present explaining Durga about the situation.

==Cast==
*Suresh  as  Ramesh
*Vijayshanti  as  Santhi
*Sai Chand  as  Chandram
*Sumalatha  as  Annapoorna Poorna
*Sangeetha  as  Durga
*Sarath Babu  as  Chalapati, Durgas husband
*J. V. Somayajulu  as  P. V. Rao
*Rama Prabha  as  Annapoornas mother Suryakantam

==Crew== Bapu
* Story &ndash; Mullapudi Venkata Ramana
* Dialogue &ndash; Mullapudi Venkata Ramana
* Producer &ndash; D. Madhusudhana Rao
* Production Company &ndash; Annapoorna Kalanikethan, Annapoorna Pictures
* Cinematographer &ndash; Baba Azmi
* Editor &ndash; G.R. Anil Dattatreya
* Choreographer &ndash; K. Tangappan

==Soundtrack==
{{Track listing
| headline        = Songs
| extra_column    = Singer(s)
| all_music       = M. S. Viswanathan
| lyrics_credits  = yes
| title1          = Padaharu Prayam
| lyrics1         = Aathreya
| extra1          = S. P. Balasubramaniam, P. Susheela
| length1         = 4:20
| title2          = Paruvapu Valapula Sangitam Sri Sri
| extra2          = P. Susheela
| length2         = 3:50
| title3          = Vayase Velluvaga
| lyrics3         = Sri Sri
| extra3          = S. P. Balasubramaniam, P. Susheela
| length3         = 4:20
}}

==External links==
*  
*  

 
 
 
 