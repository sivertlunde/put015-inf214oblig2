Painless (film)
{{Infobox film
| name           = Painless
| image          = File:Insensibles aka Painless film poster.jpg
| alt            = 
| caption        = Original release poster displaying the initial release title of Insensibles
| film name      = Insensibles
| director       = Juan Carlos Medina
| producer       =  
| writer         = Luiso Berdejo Juan Carlos Medina
| screenplay     = 
| story          = 
| based on       =  
| starring       = Àlex Brendemühl Tómas Lemarquis Ilias Stothart
| narrator       =  
| music          = Johan Söderqvist
| cinematography = Alejandro Martínez
| editing        = Pedro Ribeiro
| production companies = Les Films dAntoine Tobina Film Fado Filmes
| distributor    =  
| released       =  
| runtime        = 100 minutes
| country        = Spain France Portugal
| language       = Catalan Spanish German English
| budget         = 
| gross          =  
}}
Painless, also known under its original title of Insensibles, is a 2012 fantasy horror film and the feature film directorial debut of Juan Carlos Medina.  The film is a joint production between Spain, France, and Portugal and had its world premiere on 8 September 2012 at the Toronto International Film Festival.  Painless is set in Catalonia and follows two different story lines, one set during the Spanish Civil War and one set in the modern day.  

==Synopsis==
The films premise is partially set in 1931 during the Spanish Civil War, where a group of children are brought to a monastery turned hospital for medical testing. None of the children are capable of feeling physical pain, something that Dr. Holzmann (Derek de Lint) hopes to remedy with his procedures. However at the same time his colleague Dr. Carcedo (Ramon Fontserè) believes that the children would be better off locked up in his asylum in Canfranc. One young patient in particular named Benigno (played at different ages by Ilias and Mot Stothart) stands out in particular, as he proves to be exceptionally intelligent but also easily prone to violent outbursts - something that Carcedo believes is further proof of his beliefs. Holzmanns plans are ultimately never brought to fruition, as the hospital is besieged and taken over by several different military forces. 
 bone marrow transplant if he is to have any chance of survival. David turns to his parents in hopes that one of them can donate their marrow, only to learn that he was adopted. From there David sets out to find his biological parents, a process that inevitably leads him to the hospital that was run by Holzmann and Carcedo. He eventually discovers that his father is Benigno, who was re-named Berkano (Tómas Lemarquis) by one of the military forces, and that Berkano had become a prolific torturer for this new regime.

==Cast==
*Àlex Brendemühl as David
*Tómas Lemarquis as Berkano
*Ilias Stothart as Child Benigno
*Mot Stothart as Adolescent Benigno
*Derek de Lint as Dr. Holzmann
*Ramon Fontserè as Dr. Carcedo
*Sílvia Bel as Judith
*Bea Segura as Magdalena Juan Diego as Adán Martel mayor
*Félix Gómez as Adán Martel joven
*Irene Montalà as Anaïs
*Àngels Poch as Clara Martel
*Ariadna Cabrol as María
*Bruna Montoto as Inés niña
*Liah OPrey as Inés adolescente

==Reception== Guillermo Del Toro’s The Devils Backbone and Pan’s Labyrinth.  Twitch Film also praised the film, writing "Absolutely beautifully shot and performed, Painless is filled with interesting ideas and images that establish Medina - with his first feature here - as a director of uncommon visual skills."  Dread Central and Aint It Cool News were more mixed in their reviews,  and Aint It Cool News wrote that "PAINLESS has some superb sections with child actors at their best and has an evocative examination of what humanizes the heart. Unfortunately, it does not focus enough on this and becomes too interested in dealing a didactic ‘sins of the fathers’ tract that jars with its more timeless magic." 

==References==
 

==External links==
*  
*  

 
 
 