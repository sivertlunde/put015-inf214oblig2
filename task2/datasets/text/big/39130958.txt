Incident at Phantom Hill
 
{{Infobox film
| name           = Incident at Phantom Hill
| image          = 
| caption        = 
| director       = Earl Bellamy
| writer         = Frank S. Nugent Ken Pettus Harry Tatelman (story)
| starring       = Robert Fuller Jocelyn Lane Dan Duryea
| producer       = Harry Tatelman
| music          = Hans J. Salter
| based on       = 
| editing        = 
| budget         = 
| gross = 
| cinematography = 
| distributor    = 
| released       = 
| runtime        = 
| language       = English
| country        = United States
}}
Incident at Phantom Hill is a 1966 American western film produced by Harry Tatelman and directed by Earl Bellamy. A Union gold shipment is stolen and buried in the desert. Both Union and rebel forces struggle to find it while threatened by Apaches. A romance complicates the action. 

==Plot==
At the end of the U.S. Civil War, some Southern rebels steal a Union shipment of gold and bury it in a cave outside of Phantom Hill, Texas. After they are captured by Union forces, their leader secretly makes a deal with his captors and reveals the golds location in return for his release. 

The Union soldiers renege on the deal and keep his as their capture as they set out to find the gold. They survive an ambush by Apaches. The rebel outlaw gets the two soldiers who are guarding him drunk, kills them, and escapes taking all the Union soldiers weapons with him. He locates the gold and falls in love with the Union leaders girlfriend. Meanwhile the weaponless Union soldiers face Apaches again and only the Union leader and one other escape alive. The set off in pursuit of the rebel and set on him and the girl. She betrays the rebel by tossing his gun to her Union lovge, who shoots the rebel. The lovers are reunited and the Union soldiers return the gold to the government. 

==Cast==
As appearing in order of screen credits (main roles identified):  , IMDb, retrieved: April 16, 2013 
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Robert Fuller Robert Fuller Captain Matt Martin
|- Jocelyn Lane || Memphis
|- Dan Duryea || Joseph Henry Joe Barlow
|- Tom Simcox|| Lt. Adam Long
|- Linden Chiles || Dr. Hanneford
|- Claude Akins || Otto Krausman
|- Noah Beery Jr. || ORourke (as Noah Beery)
|- Paul Fix || General Hood
|- Denver Pyle || Hunter #1
|- William Phipps (actor) || Trader
|- Don Collier || Sheriff Carter Drum
|- Mickey Finn (actor) || Hunter #2
|}

==Notes==
 

==External links==
*  

 
 
 
 

 