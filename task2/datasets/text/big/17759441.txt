Troublesome Night 4
 
 
{{Infobox film
| name = Troublesome Night 4
| image =
| caption =
| film name = {{Film name| traditional = 陰陽路4與鬼同行
| simplified = 阴阳路4与鬼同行
| pinyin = Yīn Yáng Lù Sì Yú Guǐ Tóng Xíng
| jyutping = Jam1 Joeng4 Lou6 Sei3 Jyu4 Gwai2 Tung4 Hang4}}
| director = Herman Yau
| producer = Nam Yin
| writer = Kenneth Lau Chang Kwok-tse
| music = Mak Jan-hung
| cinematography = Joe Chan
| editing = Tony Chow
| studio = Mandarin Films Ltd. Nam Yin Production Co., Ltd.
| distributor = Mandarin Films Distribution Co., Ltd.
| released =  
| runtime = 97 minutes
| country = Hong Kong
| language = Cantonese
| budget =
| gross =
}}
Troublesome Night 4 is a 1998 Hong Kong horror comedy film produced by Nam Yin and directed by Herman Yau. It is the fourth of the 19 films in the Troublesome Night film series.

==Plot==
The film contains four short stories loosely linked together. A group of Hong Kong tourists visit the Philippines and encounter paranormal events.

==Cast==
* Louis Koo as Wing
* Pauline Suen as Apple
* Simon Lui as Leonardo
* Cheung Tat-ming as Driver
* Marianne Chan as U2
* Wayne Lai as DiCaprio
* Emily Kwan as Tour guide
* Timmy Hung as Alan Hung
* Karen Tong as Mr Wongs secretary
* Joey Choi as K2 Raymond Wong as Mr Wong
* Aya Medel
* Anna Capri
* Via Veloso as Leah Lucindo
* Anthony Cortez as Mario
* Herman Yau as John

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 
 