Peoria Babylon
{{Infobox Film 
| name        = Peoria Babylon
| image       = Peoria_Babylon_DVD.jpg
| caption     = 
| writer      = Steven Diller David Drake Ann Cusack The Lady Bunny Matthew Pestorius Paul Adelstein Marilyn Pittman Deane Clark
| director    = Steven Diller
| producer    = 
| distributor = Culture Q Connection
| released    = November 1997
| runtime     = 90 min.
| language    = English 
| country     =  
| budget      =
| music       = 
| cinematography= 
}}
Peoria Babylon is a 1997 comedy-genre film. It premiered at the Chicago Lesbian and Gay International Film Festival in November 1997.   IMDb.com. Retrieved August 4, 2007. 

== Plot ==
Candy and her gay friend Jon are owners of financially troubled art gallery in Peoria, Illinois. After exhausting their savings, they concoct a devious scheme in order to save the gallery in this screwball comedy.

They team up with a hunky con artist, the mob and a lesbian porn queen, but at the end little is left standing but their friendship.

== Cast == David Drake as Jon Ashe
* Ann Cusack as Candy Dineen
* Matthew Pestorius as Matthew Perretti
* Paul Adelstein as Brad Kessler
* The Lady Bunny as Octavia DiMare
* Marilyn Pittman as Doris Kessler
* Dan Turek as Bill
* Deane Clark as Raul Kessler
* Michael Hagedorn as Ted Jamison
* William McGough as Detective Dillon
* Andrew Carrillo as Cop
* Anna Markin as Tina Rotblatt
* David Gould as Stanley
* Tom Ciappa as Private Dick
* William Graham Cole as Swensen
* Helen Caro as Adele
* Kel Mitchell as Beave
* Jeff Kenny as Willie
* Sam Perry as Minister
* Hank Donat as Poet
* Nikki Lewis as Sandy
* Lou Wynhoff as Museum Guard
* Ted Lyde as Wayne
* Wendy Lucker as Reporter
* Tom Holycross as Cop #2
* Dan Callahan as Drag Queen
* M.J. Loheed as German Tourist
* Aja as Drag Queen #2
* Tom Phisella as Hick Man
* Rita Symons as Hick Woman
* Phyllis Diller as Painting Owner
* Lora Adams as Angry Art Patron

== References ==
 

== External links ==
* 
*  

 
 
 
 
 
 


 
 