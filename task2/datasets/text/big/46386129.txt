Treehouse (film)
{{Infobox film
| name           = Treehouse
| image          =
| alt            =
| caption        =
| director       = Michael Bartlett
| producer       = {{plainlist|
* Martin Myers
* Andy W. Meyer
* Michael Guy Ellis
}}
| writer         = {{plainlist|
* Alex Child
* Miles Harrington
}}
| starring       = {{plainlist|
* J. Michael Trautmann
* Dana Melanie
* Daniel Fredrick
* Clint James
}}
| music          = Justin Cardoza
| cinematography = J. Christopher Campbell
| editing        = Justin Cardoza
| studio         = Aunt Max Entertainment
| distributor    = Uncorkd Entertainment
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Treehouse is a 2014 American horror film directed by Michael Bartlett, written by  Alex Child and Miles Harrington, and starring J. Michael Trautmann, Dana Melanie, Daniel Fredrick, and Clint James.  Teens attempt to escape a treehouse back to the safety of their town after going out after a curfew.

== Plot ==
After several children go missing, a Missouri town institutes a curfew.  Brothers Killian and Crawford disobey the curfew and discover Elizabeth, one of the missing children, hiding in a treehouse.  As the teens became aware that they are being stalked,they attempt to return to town safely.

== Cast ==
* J. Michael Trautmann as Killian
* Dana Melanie as Elizabeth
* Daniel Fredrick as Crawford
* Clint James as Killians father
* Victoria Spencer Smith as Killians mom
* Nick Herra as The Tall One
* Shannon Knopke as Marsha
* Darren Kennedy as Officer Morgan
* Caleb Cox as Tyler

== Production == The Signal and its fast shooting schedule, Bartlett sought out that films director of photography, but unforeseen circumstances, including an influenza outbreak, caused shooting to extend to 30 days.  Bartlett and original scriptwriter Child were heavily involved in casting. 

== Release ==
The theatrical premiere was at the St. Louis International Film Festival on November 16, 2014.   After technical issues with the films screening, Bartlett told the audience that they should illegally download the film from the Internet in order to get their moneys worth.   It was released on DVD in the UK on October 20, 2014,  and on video on demand on February 20, 2015.  It also played in Los Angeles on the same date. 

== Reception ==
Gary Goldstein of the Los Angeles Times called it "a lackluster backwoods thriller" lacks the tension of Jeopardy!.   Dan Gvozden        of LA Weekly said that the film initially builds suspense but ultimately unravels due to the characters "cringe-worthy dialogue" and "unlikely decisions mandated by plot rather than character".   Ryan Pollard of Starburst (magazine)|Starburst rated it 8/10 stars and wrote, "In the end, while there are inadequate faults with the script and some ungainly performances, Treehouse overcomes its flaws with its core aesthetic, visceral atmosphere, Dana Melanies towering performance, and ultimately delivering on its promise to scare the pants off you."   Michael Gingold of Fangoria rated it 2.5/4 stars and criticized Bartletts rewrite of the script as changing the films tone and contradicting what has gone on before, though he praised the films tension.   Patrick Cooper of Bloody Disgusting rated it 3.5/5 stars and called it "a brooding, atmospheric thriller that works on a lot of levels".   Scott Hallam of Dread Central rated it 3.5/5 stars and wrote, "If youre looking for a tense and creepy night at the movies, give Treehouse a look." 

== References ==
 

== Further reading ==
=== Interviews ===
*  
*  
*  
*  
*  

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 