Perfect Blue
 
{{Infobox film
| name = Perfect Blue
| image = Pblue.jpg
| caption =
| director = Satoshi Kon
| producer = Hiroaki Inoue
| screenplay = Sadayuki Murai
| based on =  
| starring = Junko Iwao Rica Matsumoto Shinpachi Tsuji Masaaki Ōkura
| music = Masahiro Ikumi
| cinematography = Hisao Shirai
| editing = Harutoshi Ogata Madhouse
| distributor = Rex Entertainment
| released =   
| runtime = 81 minutes (R rated VHS) 85 minutes (Unrated VHS and DVD)
| country = Japan
| language = Japanese
| budget = USD|$3.5 million
| gross =
}} horror film Japanese pop idol group.

==Plot== stalker known as "Me-Mania". Shortly after leaving CHAM!, Mima receives an anonymous fax calling her a traitor. She also finds a website called "Mimas Room", which features public diary entries that seem to be written by her that discuss her life in great detail. She brings the site to the attention of her manager, ex-pop star Rumi Hidaka, but is advised to ignore it.

On the set of Double Bind, Mima succeeds in getting a larger part. However, the producers decide to cast her as a rape victim in a strip club. Rumi warns Mima that it will damage her reputation, but Mima accepts the part. The scene traumatizes her, and she increasingly becomes unable to distinguish reality from her work in show business.

Several people involved in creating the scene are murdered. Mima finds evidence that makes her a suspect, and her increasing mental instability makes her doubt her own innocence. It turns out that the diarist of "Mimas Room" is delusional and very manipulative, and that an intense folie à deux has been in play. The faux diarist and serial killer, who believes herself to be a Mima who is forever young and graceful, has made a scapegoat of stalker Me-Mania.

Mima knocks Me-Mania unconscious with a hammer in self-defense when he attempts to rape her, and she then runs to her only support she has left alive, her manager Rumi. Later on, back in "Mimas room", Mima tries to call Mr. Tadokoro but he has also been murdered, along with Mr. Me-Mania. When Mima encounters Rumi, however, her manager is wearing a replica of Mimas CHAM! costume and crazily singing Mimas pop songs.  Rumi is in fact the false diarist, who believes she is the "real Mima". Rumi is angry that Mima has been ruining the "real Mimas" reputation, and decides to save "Mimas" pristine pop idol image through the same means she has been using all along: murder. Mima manages to incapacitate Rumi in self-defense after a chase through the city despite being wounded herself. Rumi remains permanently delusional and institutionalized. Mima has grown from her experiences and has moved on with her life with newfound independence and confidence.

==Voice cast ==

{| class="wikitable"
|-
! Character !! Japanese !! English   
|-  Ruby Marlowe   
|-
| Rumi || Rica Matsumoto  || Wendee Lee   
|-
| Tadokoro || Shinpachi Tsuji || Barry Stigler 
|- Bob Marx   
|-
| Tejima || Yōsuke Akimoto || –
|-
| Takao Shibuya ||  Yoku Shioya  || –
|-
| Sakuragi || Hideyuki Hori ||  –
|-
| Eri Ochiai || Emi Shinohara || –
|-
| Mureno || Masashi Ebara  || Jamieson Price 
|- Richard Plantagenet 
|-
| Yatazaki || Tōru Furusawa ||  –
|-
| Yukiko ||  Emiko Furukawa || –
|-
| Rei || Shiho Niiyama || –
|-
| Tadashi Doi || Akio Suyama || –
|-
| Yamashiro || – || Kirk Thornton 
|-
| Cham Manager || – ||  Dylan Tully
|-
|}
 Gil Starberry, James Lyon, David Lucas, Sparky Thornton, Melissa Williamson, Dylan Tully, Kermit Beachwood, Billy Regan, George C. Bob Marx, Robert Wicks and Mattie Rando.   – closing credits 

==Production== Kobe earthquake of 1995 damaged the production studio, the budget for the film was reduced to an original video animation. Katsuhiro Otomo was credited as "Special Supervisor" to help the film sell abroad and as a result the film was screened in many film festivals around the world. While touring the world it received a fair amount of acclaim, jump-starting Kons career as a filmmaker. 

Kon and Murai did not think that the original novel would make a good film and asked if they could change the contents. This change was approved so long as they kept a few of the original concepts from the novel. A live action film   was later made (released in 2002) that is much closer to the novel. This version was directed by Toshiki Satō from a screenplay by Shinji Imaoka and Masahiro Kobayashi. 

Like much of Kons later work, such as Paprika (2006 film)|Paprika, the film deals with the blurring of the lines between fantasy and reality in contemporary Japan. 

==Release and broadcast== UMD video release of Perfect Blue, Manga Entertainment featured the movie in cinema widescreen, leaving the movie kept within black bars on the PSPs 16:9 screen. This release also contains no special features and a single audio track (English).
 Encore cable Sci Fi Channel on December 10, 2007 as part of its Ani-Monday block. In Australia, Perfect Blue aired by the SBS Television Network on April 12, 2008 and previously sometime in mid 2007 in a similar timeslot.

== Analysis == Susan Napiers uses feminist film theory to analyze the film, stating that, "Perfect Blue announces its preoccupation with perception, identity and performance - especially in relation to the female - right from its opening sequence. The perception of reality cannot be trusted, with the visual set up only to not be reality, especially as the psychodrama heights towards the climax."  Napier also sees themes related to pop idols and their performances as impacting the gaze and the issue of their roles. Mimas madness results from her own subjectivity and attacks on her identity. The ties to Alfred Hitchcocks work is broken with the murder of her male controllers.  Otaku described the film as "critique of the consumer society of contemporary Japan."  

==Reception==
The film was critically well received in the festival circuit, winning awards at the 1997 Fantasia Festival in Montréal, and Fantasporto Film Festival in Portugal.

Critical response in the United States upon its theatrical release was mixed. Some critics did not understand why Perfect Blue was done as an animated film  , while others associated it with common anime stereotypes of gratuitous sex and violence.  Kon responded to this criticism by stating that he was proud to be an animator and Perfect Blue was more interesting as animation.    

Time (magazine)|Time included the film on its top 5 anime DVD list,  and Terry Gilliam, of whom Kon was a fan  included it in his list of the top fifty animated films. 

Perfect Blue ranked #25 on Total Films all-time animated films. 

It also made the list for Entertainment Weekly s best movies never seen from 1991-2011.  

==Influence== Madonna incorporated clips from the film into a remix of her song "What It Feels Like for a Girl" as a video interlude during her Drowned World Tour (2001). 
 Black Swan.  A re-issued blog entry mentioned Aronofskys film Requiem for a Dream as being among Kons list of movies he viewed for that year. 

==See also==
 
* Japanese films of 1997
 

== Notes ==
 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 