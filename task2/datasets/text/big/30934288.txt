The Lady in Ermine
{{infobox film
| name           = The Lady in Ermine
| image          = Poster - Lady in Ermine, The (1927) 01.jpg
| image size     =
| caption        = 1927 theatrical poster
| director       = James Flood
| producer       = Corinne Griffith
| screenplay     = Adaptation and scenarios: Benjamin Glazer
| based on       =  
| starring       = Corinne Griffith Einar Hanson Francis X. Bushman Ward Crane
| music          =
| cinematography = Harold Wenstrom
| editing        = Corinne Griffith Productions
| distributor    = First National Pictures
| released       =  
| runtime        = 7 reels
| country        = United States Silent (English intertitles)
}} silent film romance/drama directed by James Flood and produced by and starring Corinne Griffith, and distributed by First National Pictures. The film is now considered a lost film. 

==Play==
The operetta The Lady in Ermine, upon which this film and later films are based, opened on Broadway October 2, 1922 and ran for 238 performances closing on April 21, 1923. It originally played at the Ambassador Theatre and then at the Century Theatre. The famous Shubert Brothers produced the operetta/play. 

==Remakes==
The story was remade as an early talkie musical in Technicolor, Bride of the Regiment (1930), also released by First National and also considered a lost film. 
It was remade again in 1948 by 20th Century-Fox as That Lady in Ermine, starring Betty Grable and Douglas Fairbanks, Jr.

==Cast==
*Corinne Griffith - Mariana Beltrami
*Einar Hanson - Adrian Murillo
*Ward Crane - Archduke Stephan
*Francis X. Bushman - Gen. Dostal
*Jane Keckley - Marianas maid

==See also==
*Francis X. Bushman filmography
*List of lost films

==References==
 

==External links==
* 
* 
* 
*scenes from the film with Corinne Griffith and Francis X. Bushman:  , .. 

 
 
 
 
 
 
 
 
 


 
 