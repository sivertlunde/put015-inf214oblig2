Mysterious Doctor Satan
{{Infobox Film
| name =The Mysterious Doctor Satan
| image = MysteriousDrSatan.jpg John English
| producer =Hiram S. Brown Jr
| writer =Franklin Adreon Ronald Davidson Norman S. Hall Joseph F. Poland Barney A. Sarecky Sol Shor Edward Ciannelli Robert Wilcox William Newell C. Montague Shaw Ella Neal Dorothy Herbert William Nobles
| distributor =Republic Pictures
| released       =   13 December 1940 (serial) {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement 
 | origyear = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 50–51
 | chapter = 
 }}    Early 1950s (TV)    16 July 1954   1966 (TV film)    1 October 2001|
| runtime         = 15 chapters (267 minutes (serial)  7 26½-minute episodes (TV)  100 minutes (TV film) 
| country =   English
| budget          = $147,847 (negative cost: $147,381) 
}} 1940 Serial film serial named after its chief villain. Doctor Satans main opponent is masked mystery man The Copperhead, whose secret identity is Bob Wayne, a man searching for justice and revenge on Satan for the death of his step-father. The serial charts the conflict between the two as Bob Wayne pursues Doctor Satan, while the latter completes his plans for world domination.
 John English. Edward Ciannelli Robert Wilcox.
 Henry Brandon was originally intended to play the part of Doctor Satan while wearing a regular devil costume, complete with horns.  At the end of the 1930s, however, this would have stretched the audiences imagination too far so a more believable villain was written in the form of a sleek, gangster-style mad scientist played by Ciannelli. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut  
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | origyear = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | chapter = 14. The Villains "All Bad, All Mad"
 | quote =
 | page = 355
 }} 

==Plot==
Governor Bronson, who raised Wayne from childhood after the death of his parents, is killed at the hands of a world-domination-seeking mad scientist called Doctor Satan. Fearing his death might be at hand, as it has been for everyone else who had opposed the Doctor, the Governor first confides in Wayne with a secret about his past. Bobs father was really an outlaw in the Old West, who fought injustice while wearing a chainmail cowl and leaving small coiled copper snakes as his calling card.

Following his guardians death, Wayne decides to adopt his fathers Copperhead persona and cowl. Doctor Satan, meanwhile, requires only a remote control device invented by Professor Scott to complete his army of killer robots and gain all the power and riches he desires.

The Copperhead fights Doctor Satan, rescuing the Professor and others and preventing the Doctor from completing his plot.

==Cast==
===Main cast===
*Eduardo Ciannelli (credited as Edward Ciannelli) as mad scientist Doctor Satan. Robert Wilcox as Bob Wayne/The Copperhead William Newell as Speed Martin, a reporter
*C. Montague Shaw as Professor Thomas Scott, inventor of a remote control device for the military
*Ella Neal as Lois Scott, reporter and Professor Scotts daughter
*Dorothy Herbert as Alice Brent, Professor Scotts secretary
*Charles Trowbridge as Governor Bronson
*Jack Mulhall as Police Chief Rand
*Edwin Stanley as Col. Bevans 
*Walter McGrail as Stoner, thug leader 
*Joe McGuinn as Gort, a thug 
*Bud Geary as Hallett, a thug  Paul Marion as Corbay, a thug 
*Archie Twitchell as Ross, airport radio operator 
*Lynton Brent as Scarlett, a thug
*Ken Terrell as Corwin, a thug  Al Taylor as Joe, a thug
*Bert LeBaron as Fallon, gas plant thug Tom Steele as The Robot {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut  
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | origyear = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 357–358
 | chapter = 14. The Villains "All Bad, All Mad"
 }} 

==Production==
  team had time to complete only one before filming began.]] National Comics love interest, Lois, had only her surname changed between these drafts, while his secret identity, down to the surname, mimicked Batmans.

Mysterious Doctor Satan was filmed between 20 September and 29 October 1940 under the working title Doctor Satan, at a cost of $147,381.  The serials production number was 1095. 

According to Stedman, Republic was unconsciously "observing the transfer of the costumed crusader from prairie to pavement" in the writing of this serial.  The western cowboy hero would soon be replaced in popular culture by superheroes and masked crimefighters. {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | origyear = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5 
 | pages = 125, 141
 | chapter = 5. Shazam and Good-by
 }} 

===Special effects===
The serial introduces the updated "Republic robot." A more primitive design had appeared in   as "Satans Robot." 
 Howard Lydecker.  However, the studio had neither time nor money to create the new robot before filming was to begin so Witney was stuck with the "hot water boiler." {{cite book
 | last = Witney
 | first = William
 | authorlink = William Witney
 | title = In a Door, Into a Fight, Out a Door, Into a Chase: Moviemaking Remembered by the Guy at the Door
 | year = 2005
 | publisher = McFarland & Company, Inc.
 | isbn = 978-0-7864-2258-6
 | pages = 
 | chapter = 
 }} 

The "bank robbery by robot" scene was reused as stock footage in the later serial Zombies of the Stratosphere. 


===Stunts=== James Fawcett doubling William Newell
* Eddie Parker David Sharpe doubling Robert Wilcox, playing The Copperhead when in costume. 
* Tom Steele
* Duke Taylor
* Helen Thurston doubling Dorothy Herbert Wally West
* Bud Wolfe

==Release==
===Theatrical===
Mysterious Doctor Satans official release date is 13 December 1940, although this is actually the date the seventh chapter was made available to film exchanges. 

===Television===
In the early 1950s, Mysterious Doctor Satan was one of fourteen Republic serials edited into a television series.  It was broadcast in seven 26½-minute episodes (the other thirteen all had only six episodes). 

Mysterious Doctor Satan was also one of twenty-six Republic serials re-released as a film on television in 1966.  The title of the film was changed to Doctor Satans Robot.  This version was cut down to 100-minutes in length. 

==Critical reception==
Harmon and Glut describe Mysterious Doctor Satan as "one of Republics best serials...   set the pace for others that followed." They go on to narrow it down to one of the five or six greatest serials Republic ever made.  Many people involved in the serial are singled out for praise but the main one is Ciannelli as Doctor Satan, a character who steals the show from the relatively bland Copperhead.  The directors, William Witney and John English are noted as the best in their field.  Cy Feuer is praised for his music, which is both moody and exciting.  Mention is also made of the "superior" lighting and "some of the best stunt work in the fights to ever appear on screen in any kind of film." 

The tone of the serial was set by Eduardo Ciannellis "piercing malevolent countenance." {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 3. The Six Faces of Adventure
 | page = 48
 }}   Ciannellis performance "in a role so susceptible to overacting and scenery chewing" maintained the "exact balance between a wild-eyed lunatic with dreams of world conquest and the brilliant, gifted man of science that Doctor Satan might have been.  There was a poignancy in his portrayal that gave the uneasy feeling that this cruel genius was somehow a victim of forces that drove him to evil against his basic desire.  Nothing was said or done in the screenplay to indicate it, but the feeling was there, nonetheless." {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = 7. Masters of Menace (The Villains)
 | page = 117
 }} 

==Chapter titles==
#Return of the Copperhead (30 min 15s)
#Thirteen Steps (17 min 41s)
#Undersea Tomb (17 min 18s)
#The Human Bomb (16 min 42s)
#Doctor Satans Man of Steel (16 min 54s)
#Double Cross (16 min 44s)
#The Monster Strikes (16 min 53s)
#Highway of Death (16 min 40s)
#Double Jeopardy (16 min 39s)
#Bridge of Peril (16 min 40s)
#Death Closes In (17 min 12s)
#Crack-Up (17 min 16s)
#Disguised (16 min 42s)
#The Flaming Coffin (16 min 45s)
#Doctor Satan Strikes (16 min 44s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 229
 }} 

==Cliffhangers==
===Cliffhangers===
#Return of the Copperhead: Doctor Satans henchman remotely blows up the experimental ship, with Lois and crew aboard, as ordered.
#Thirteen Steps: Copperhead is electrocuted in Doctor Satans lab as he captures the Doctor and his thugs.
#Undersea Tomb: A depth charge explodes.  The diving bell, containing Bob & Lois, cracks and begins to fill with water.
#The Human Bomb: Copperhead drives into a sheet of flames and his car explodes.
#Doctor Satans Man of Steel: Copperhead is caught in the crushing grip of The Robot.
#Double Cross: Lois is bound and gagged, the door handle is rigged to electrocute the Copperhead if he attempts a rescue, and poison gas is set to be released by a timer.
#The Monster Strikes: Splashed acid burns The Robots circuitry and sends it out of control.  It topples a case of acid on itself and the stunned Copperhead.
#Highway of Death: Copperhead, fighting on the back of a truck, is knocked off the vehicle. Speed, pursuing in a car, runs him down.
#Double Jeopardy: An open can of gunpowder is knocked over when a fight breaks out in a mine.  An escaping thug knocks a lit torch onto the trail which in turn sets off all the explosives.
#Bridge of Peril: During a chase across a gas works, Copperhead is knocked from a narrow beam by a block and tackle swung at him.
#Death Closes In: Doctor Satan drops the Copperhead through a trap door and activates a sliding wall in the cell beneath to crush him.
#Crack-Up: A plane being flown by Copperhead crashes into a mountain.
#Disguised: As the Copperhead leads escapees round a corner, Joe the thug opens fire on them, and the leading figure falls, shot.
#The Flaming Coffin: Copperhead hides in a box about to be delivered to Doctor Satans new hideout.  Doctor Satan suspects a poison gas booby-trap and has the still sealed box incinerated.

===Solutions===
#Thirteen Steps: Lois and the Copperhead jump overboard before the ship explodes.
#Undersea Tomb: Copperhead shoots out the controls and escapes by jumping through a window.
#The Human Bomb: Bob and Lois survive inside an air pocket within the diving bell.
#Doctor Satans Man of Steel: Copperhead jumps away from the car before it explodes.
#Double Cross: Professor Scott deactivates The Robot with its control panel.  The released Copperhead falls into the sea.
#The Monster Strikes: Copperhead enters through the window and rescues Lois from the cloud of poison gas.
#Highway of Death: Copperhead recovers in time and rolls aside.
#Double Jeopardy: Copperhead lies flat in the road so the car passes safely over him.
#Bridge of Peril: Copperhead dives into a shallow crevice for shelter from the explosion.
#Death Closes In: Copperhead catches the block and tackle as he falls, pulls himself to a walkway and continues the chase.
#Crack-Up: Doctor Satan leaves to escape the newly arrived District Attorneys Men.  Copperhead shoots out the controls using a reflective object (possibly a cigarette case) to aim.
#Disguised: Bobs plane is the one that crashed, after he parachuted to safety.
#The Flaming Coffin: Professor Williams is the one shot by Joe, who is punched out by the Copperhead.
#Doctor Satan Strikes: Copperhead had already left the box when the delivery truck parked, substituting sacks of ore to maintain the boxs weight.

==References==
 

==External links==
* 
* 
* 

 
{{succession box  Republic Serial Serial  King of the Royal Mounted (1940 in film|1940)
| years=The Mysterious Doctor Satan (1940 in film|1940)
| after=Adventures of Captain Marvel (1941 in film|1941)}}
{{succession box  English Serial Serial  King of the Royal Mounted (1940 in film|1940)
| years=The Mysterious Doctor Satan (1940 in film|1940)
| after=Adventures of Captain Marvel (1941 in film|1941)}}
 

 
 

 
 
 
 
 
 
 
 
 