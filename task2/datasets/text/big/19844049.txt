The Overeater
The 2003 French-language film directed by Thierry Binisti and starring former footballer Éric Cantona.

==Plot introduction==
Superintendent Richard Selena of Marseille Police, is a compulsive overeater. Obese and awkward, he looks like an ogre. The opening credits overlay a scene of childhood attachment, ending with a young girl being led away. We learn that memories of his girlfriends death still haunt him. After a heart attack, doctors give him one year to live.  As well as being traumatised by the childhood incident and feeling responsible for her death, he falls in love with a murder suspect, Elsa played by Rachida Brakni. 

Fascinated by the young and beautiful Elsa whom Richard knows is guilty of murdering her uncle, Victor Lachaume, a leading ship owner of the port, he decides to play a game. He makes a proposition: that he will keep silent about his knowledge on condition that she comes to dine at his place every evening for that one year. Elsa accepts this ritual in spite of the perversity of the proposal.

==Cast==
* Eric Cantona : Superintendent Séléna
* Rachida Brakni : Elsa
* Jocelyn Quivrin : Marc Brisset
* Richard Bohringer : Emile Lachaume
* Caroline Sihol : Anne Lachaume

==Production details==
* music by Nicolas Errèra 
This crime drama was made in 2003 in France and is 95 minutes long with English subtitles.

==References==
 

 
 
 
 
 


 
 