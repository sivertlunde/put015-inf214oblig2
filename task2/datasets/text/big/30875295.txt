Boccaccio '70
{{Infobox film
|  name           = Boccaccio 70
|  image          = Boccaccio70.jpg
|  caption        = Original Poster
|  writer         = Cesare Zavattini Luchino Visconti Mario Monicelli Federico Fellini
|  starring       = Anita Ekberg Romy Schneider Sophia Loren Marisa Solinas
|  director       = Vittorio De Sica Luchino Visconti Federico Fellini Mario Monicelli
|  producer       = Carlo Ponti Tonino Cervi
|  music          = Nino Rota Armando Trovaioli
|  distributor    = 
|  released       = February 22, 1962
|  runtime        = 150 minutes (release with 3 segments) 208 min (Italian version with all four segments)
|  country        = Italy
|  language       = German/Italian
|  budget         = 
}}
 1962 cinema Italian anthology film directed by Mario Monicelli, Federico Fellini, Luchino Visconti and Vittorio De Sica, from an idea by Cesare Zavattini. It is an anthology of four episodes, each by one of the directors, all about a different aspect of morality and love in modern times, in the style of Giovanni Boccaccio|Boccaccio.

==Cast==
* Marisa Solinas as Luciana
* Germano Gilioli as Renzo
* Peppino De Filippo as Dr Antonio Mazzuolo
* Anita Ekberg as Herself
* Tomas Milian as Ottavio
* Romy Schneider as Pupe
* Sophia Loren as Zoe

The four original episodes were:
* Renzo e Luciana (by Mario Monicelli) with Marisa Solinas and Germano Gilioli.
* Le tentazioni del dottor Antonio (by Federico Fellini) with Peppino De Filippo and Anita Ekberg.
* Il lavoro (by Luchino Visconti) with Romy Schneider and Tomas Milian.
* La riffa (by Vittorio De Sica) with Sophia Loren.

The first episode, by Monicelli, was only included in the Italian distribution of the film. Out of solidarity towards Monicelli, the other three directors did not go to the Cannes Film Festival for the presentation of the film.

==Plot==
In Renzo e Luciana, a young couple tries to hide their marriage and the wife’s supposed pregnancy from the draconian book-keeper of their employer, who has banned female employees from getting married and having children but does not mind a few cheap thrills at their expense himself.

In Le tentazioni del dottor Antonio, an elderly citizen is fed up with too much immorality in the form of indecent content in print. His anger knows no bounds when a provocative billboard of Anita Ekberg advertising "Drink more milk" is put up in a park near his residence. Little does he know how that billboard will go on to change his life.  Throughout the film, children are heard singing the jingle "Bevete più latte, bevete più latte!" ("Drink more milk!") The image begins to haunt him with hallucinations in which she appears as a temptress and Dr. Antonio as Saint George and the Dragon|St. George to spear the dragon – he is pursued and captured by the buxom Swedish star in a deserted Rome and at one point, his umbrella falls between her breasts.

Il lavoro is about an aristocratic couple coming to terms with life and marriage after the husband is caught visiting prostitutes by the press.

La riffa shows a lottery with the winner entitled to one night with the attractive Zoe (Sophia Loren). Zoe, however, has other plans.

==Artistic legacy==
An orchestrated version of the song "Bevete più latte", from Le Tentazioni del Dottor Antonio, was one of 13 tracks, recorded by Italian band Piccola Orchestra Avion Travel, arranged by Fabrizio France, for their 2009 album "Nino Rota LAmico Magico", released to mark the 30th anniversary of the death of composer Nino Rota. 

==References==
 

==External links==
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 