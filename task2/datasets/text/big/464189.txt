Westworld
:For the HBO TV series based on the film, see Westworld (TV series). For other uses, see Westworld (disambiguation).
{{Infobox film
| name           = Westworld
| image          = Westworld ver2.jpg
| caption        = Theatrical release poster by Neal Adams
| image_size     = 225px
| director       = Michael Crichton
| producer       = Paul Lazarus III
| writer         = Michael Crichton
| starring       = Yul Brynner Richard Benjamin James Brolin
| music          = Fred Karlin
| cinematography = Gene Polito
| editing        = David Bretherton
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $1,250,000 Crichton p x 
| gross          = $10 million 
}} thriller film android in a futuristic American Old West|Western-themed amusement park, and Richard Benjamin and James Brolin as guests of the park.
 Nebula and television series based on the original film.

==Plot== androids that are practically indistinguishable from human beings, each programmed in character for their assigned historical environment. For $1,000 per day, guests may indulge in any adventure with the android population of the park, including sexual encounters and even a fight to the death, depending on the android model. Delos tagline in its advertising promises "Have we got a vacation for you!"

Peter Martin (Benjamin), a first-timer, and his friend John Blane (Brolin), who has visited previously, visit West World. One of the attractions in West World is the Gunslinger (Brynner), a robot programmed to instigate gunfights. The firearms issued to the park guests have temperature sensors that prevent them from shooting humans or anything else living, but allow them to "kill" the "cold-blooded" androids. The Gunslingers programming allows guests to outdraw it and "kill" it, always returning the next day for a new duel.
 designed by other computers. We dont know exactly how they work."
 Black Knight kills a guest in a swordfight. The resorts supervisors, in increasing desperation, try to regain control by shutting down power to the entire park, but this traps them in the control rooms, unable to turn the power back on while the robots run amok on reserve power.

Martin and Blane, passed out drunk after a bar-room brawl, wake up in West Worlds bordello, unaware of the breakdown. When the Gunslinger challenges the two men to a showdown, Blane treats the confrontation as a typical amusement until the robot outdraws, shoots and actually hits him, mortally wounding him. Martin runs for his life as the robot implacably follows him while tracking him with its digital scanner.

Martin flees to the other areas of the park, but finds only dead guests, damaged robots, and a panicked technician who is shortly shot by the Gunslinger. Martin climbs down through a manhole in Roman World to the underground control area and discovers that the resorts technicians suffocated when the ventilation system shut down. The Gunslinger stalks Martin through the underground corridors. Ambushing it, Martin throws acid into its face then he runs away, returning to the surface in the Medieval World castle. With its optical inputs damaged by the acid, the Gunslinger is unable to track him normally, resorting to infra-red heat scanning, and Martin sets it on fire with a torch. He tries to rescue a woman chained up in a dungeon, but when he tries to give her water, she short-circuits, revealing she is an android. The burned hulk of the Gunslinger attacks him one last time on the dungeon steps before succumbing to its damage. Martin, apparently the sole human survivor, sits in a state of near-exhaustion and shock, as the irony of Delos slogan resonates: "Have we got a vacation for you!"

==Cast==
*Yul Brynner as the Gunslinger
*Richard Benjamin as Peter Martin
*James Brolin as John Blane
*Majel Barrett as Miss Carrie
*Alan Oppenheimer as the Chief Supervisor Victoria Shaw as Medieval Queen
*Norman Bartold as Medieval Knight

==Production==

===Script===
Crichton says he did not wish to make his directorial debut with science fiction but "thats the only way I could get the studio to let me direct. People think Im good at it I guess." Author of Terminal Man Building Nonterminal Career: CRICHTON
GELMIS, JOSEPH. Los Angeles Times (1923-Current File)   04 Jan 1974: d12. 

The script was written in August 1972 and was offered to all the major studios. They all turned down the project except for MGM, then under head of production Dan Melnick. Crichton:
 MGM had a bad reputation among filmmakers; in recent years, directors as diverse as Robert Altman, Blake Edwards, Stanley Kubrick, Fred Zinneman and Sam Peckinpah had complained bitterly about their treatment there. There were too many stories of unreasonable pressure, arbitrary script changes, inadequate post production, and cavalier recutting of the final film. Nobody who had a choice made a picture at Metro, but then we didnt have a choice. Dan Melcnick... assured  ... that we would not be subjected to the usual MGM treatment. In large part, he made good on that promise.  
Crichton says preproduction was difficult, with MGM demanding script changes up to the day of shooting and the leads not being locked down until 48 hours beforehand. He says he had no control over casting  and MGM originally would only make the film for under a million dollars but later increased this amount by $250,000.  Crichton says that of the budget, $250,000 went on the cast, $400,000 on crew and the remainder on everything else (including $75,000 for sets). 

===Shooting===
Westworld was filmed in several locations, including the Mojave Desert, the gardens of the Harold Lloyd Estate, and several sound stages at MGM.  It was shot with Panavision anamorphic lenses by Gene Polito, American Society of Cinematographers|A.S.C.

Richard Benjamin later said he loved making the film:
 It probably was the only way I was ever going to get into a Western, and certainly into a science-fiction Western. It’s that old thing when actors come out here from New York. They say, “Can you ride a horse?” And you say, “Oh, sure,” and then they’ve got to go out quick and learn how to ride a horse. But I did know how to ride a horse! So you get to do stuff that’s like you’re 12 years old. All of the reasons you went to the movies in the first place. You’re out there firing a six-shooter, riding a horse, being chased by a gunman, and all of that. It’s the best!     accessed 18 June 2014  
The Gunslingers appearance is based on Chris Adams, Brynners character from The Magnificent Seven. The two characters costumes are nearly identical. 

In the scene when Richard Benjamins character splashes the Gunslinger in the face with acid, Brynners face was covered with an oil-based makeup mixed with ground Alka-Seltzer. A splash of water then produced the fizzing effect.

The score for Westworld was composed by American composer Fred Karlin. It combines ersatz western scoring, source cues, and electronic music. 

Crichton later wrote that since "most of the situations in the film are cliches; they are incidents out of hundreds of old movies" that the scenes "should be shot as cliches. This dictated a conventional treatment in the choice of lenses and the staging." 

The movie was shot in thirty days. In order to save time, Crichton camera-cut. 

The original script and original ending of the movie ended in a fight between Martin and the gunslinger which resulted in the gunslinger being torn apart by a rack. Crichton said he "had liked the idea of a complex machine being destroyed by a simple machine" but when attempting it felt "it seemed stagey and foolish" so the idea was dropped. Crichton p xix  He also wanted to open the film with shots of a hovercraft travelling over the desert, but was unable to get the effect he wanted so this was dropped as well. 

===Post-production===
In the movies novelization, Crichton explained how he re-edited first cut of the movie because he was depressed by how long and boring it was. Scenes which were deleted from rough cut include a bank robbery and sales room sequences; an opening with a hovercraft flying above the desert; additional and longer dialogue scenes; more scenes with robots attacking and killing guests, including a scene where one guest is tied down to a rack and is killed when his arms are pulled out; a longer chase scene with the Gunslinger chasing Peter; and one where the Gunslinger cleans his face with water after Peter throws acid on him. Crichtons assembly cut featured a different ending which included a fight between Gunslinger and Peter, and an alternate death scene in which the Gunslinger was killed on a rack.  

A Yul Brynner biography mentions that 10 minutes of adult material was cut because of the censors (probably for PG rating), but no details about footage that was cut for rating issues were mentioned in Brynners biography or anywhere else.

Once the film was completed, MGM authorised the shooting of some extra footage. A TV commercial to open the film was added; because there was a writers strike in Hollywood at the time, this was written by Steven Frankfurt, a New York advertising executive. 

===Digital image processing=== pixelized in point of view.  The approximately 2 minutes and 31 seconds worth of cinegraphic block portraiture was accomplished by color-separating (three basic color separations plus black mask) each frame of source 70&nbsp;mm film images, scanning each of these elements to convert into rectangular blocks, then adding basic color according to the tone values developed.  The resulting coarse pixel matrix was output back to film.  The process was covered in the American Cinematographer article Behind the scenes of Westworld  and in a 2013 New Yorker online article. 

==Release==

===Box office===
The film was a financial success, earning $4 million in rentals in the US and Canada rentals by the end of 1973  becoming MGMs biggest box office success of that year. 

===Book tie-in===
Crichtons original screenplay was released as a mass-market paperback in conjunction with the film. 

===Critical reception===
Variety (magazine)|Variety magazine described the film as excellent and that it "combines solid entertainment, chilling topicality, and superbly intelligent serio-comic story values".   

The film has a rating of 84% at Rotten Tomatoes.  Reviewing the DVD release in September 2008, The Daily Telegraph reviewer Philip Horne described the film as a "richly suggestive, bleakly terrifying fable&nbsp;— and Brynners performance is chillingly pitch-perfect."   

American Film Institute lists
*AFIs 100 Years...100 Thrills - Nominated 
*AFIs 100 Years...100 Heroes and Villains:
**Robot Gunslinger - Nominated Villain 
*AFIs 10 Top 10 - Nominated Science Fiction Film 

After making the film, Crichton took a year off. "I was intensely fatigued by Westworld," he said later. "I was pleased but intimidated by the audience reaction... The laughs are in the wrong places. There was extreme tension where I hadnt planned it. I felt the reaction, and maybe the picture, was out of control." 

For him the picture marked the end of "about five years of science fiction/monster pictures for me".  He took a break from the genre and wrote The Great Train Robbery.

Crichton did not make a film for another five years. He did try, and had one set up "but I insisted on a certain way of doing it and as a result it was never made." Director Michael Crichton Films a Favorite Novelist
By MICHAEL OWEN. New York Times (1923-Current file)   28 Jan 1979: D17. 

==Network TV airings==
Westworld was first aired on NBC television on 28 February 1976.  The network aired a slightly longer version of the film than was shown theatrically or subsequently released on home video. Some of the extra scenes that were added for the US TV version are: 

* Brief fly-by exterior shot of the hovercraft zooming just a few feet above the desert floor. Previously, all scenes involving the hovercraft were interior shots only.
* The scenes with the scientists having a meeting in the underground complex was much longer giving more insight into their "virus" problem with the robots.
* A scene with a couple of techs talking in the locker room about the work load of each robot world.
* There was a longer discussion between Peter and the sheriff after his arrest when he shot the Gunslinger.
* During the scene where robots are going crazy,there was a scene in Medieval World where a guest is getting tortured on the Medieval rack. At first he tries joking while getting dragged to the rack and saying "What is this, a joke? Hey! I paid in advance!" But then he really gets desperate and says "I really dont want to do this!" and then starts to scream as he gets placed on the rack and stretched and then his arms are pulled out of their sockets. One still shot shows piece of this scene with guest on rack while one of the robots wearing some kind of hood is standing next to him.
* Gunslingers chase of Peter through the worlds was also extended.
* There was a scene added in which Gunslinger is splashing water on his face from the sink after being hit with the acid, he recovers and then there was a close-up of his face when he turns around real quickly and his silver eyes turned into black and crazy look.

==Legacy==

A sequel to Westworld, Futureworld, was filmed in 1976, and released by American International Pictures, rather than MGM. Only Yul Brynner returned from the original cast to reprise his Gunslinger character. Four years later, in 1980, the CBS television network aired a short-lived television series, Beyond Westworld, expanding on the concepts and plot of the second film with new characters. Its poor ratings caused it to be canceled after only three of the five episodes aired.
 Jurassic Park.

Beginning in 2007, trade publications reported that a Westworld remake starring Arnold Schwarzenegger was in production, and would be written by Terminator 3 screenwriters Michael Ferris and John Bracanto.        Tarsem Singh was originally slated to direct, but has since left the project. Quentin Tarantino was approached, but turned it down.  On January 19, 2011, Warner Bros announced that plans for the remake were still active. 

In August 2013, it was announced that HBO had ordered a pilot for a Westworld (TV series)|Westworld TV series which will be produced by J.J. Abrams, Jonathan Nolan, and Jerry Weintraub. Nolan and his wife Lisa Joy will write and executively produce the series with Nolan directing the pilot episode.  Production is set to begin in Summer 2014   in Los Angeles. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 