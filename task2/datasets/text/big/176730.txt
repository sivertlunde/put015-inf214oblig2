Anarchists (film)
 
{{Infobox film
| name = Anarchists
| image          = Anarchists_DVD_cover.jpg
| film name      = {{Film name
| hangul         =  
| hanja          =
| rr             = Anakiseuteu
| mr             = Anakiseuteu}}
| director       = Yoo Young-sik
| producer       = Lee Joon-ik
| writer         = Park Chan-wook   Lee Moo-young   Bangnidamae
| starring       = Jang Dong-gun Kim Sang-joong Jung Joon-ho Lee Beom-soo Kim In-kwon Ye Ji-won
| music          = Choi Man-shik Choi Sun-shik   Im Ju-hee   Kim Young-kwan
| cinematography = Kim Eung-taek
| editing        = Kyung Min-ho
| studio         = CineWorld Shanghai Film Studio
| distributor    = Cineclick Asia
| released       =  
| runtime        = 100 minutes
| language       = Korean
| budget         =
}} anarchists who occupation of Korea through propaganda of the deed.  Told from the perspective of the youngest member, Sang-gu, years after the fact, the story is a sympathetic look at a group of revolutionaries through the eyes of one of their own.

==Plot==
In the opening scene the protagonist begins to reminisce about his youth and remembers the day he was saved from execution in a raid performed by the anarchist cell he would later join.  After reaching a safe house the group begins to teach him the tricks of their trade.  He later takes part in several missions, though he continues to have difficulty throughout the film with the violence of his new job.

Eventually a string of tragic events strike the team.  One of their members is fatally betrayed during a mission, leading to their covers being blown during the next.  Now wanted by the Japanese and Chinese authorities, their funders turn away from them and instead choose to support socialist electoral politics to further their cause.  This angers the group, and they leave the larger organization, attempting to survive on their own by earning money through gambling and bank robbery.  Over time the group becomes agitated with simply scraping by and several voice a desire to return to their old ways of clandestine warfare.  They collectively decide to strike at the Japanese government in a high profile attack, leading up to a dramatic finale.

==Cast and characters==
Specific character information largely comes from the films official website, rather than the film itself.  Within the film, characters arent overtly developed and exposition largely comes through Sang-gus perspective, relying on few monologues and no flashbacks to tell the backstory.  Instead, references to some of the events described below can be detected within character interaction and dialogue.

* Jang Dong-gun as Sergei nihilistic intellectual. He was the leader of a terrorist group, but unable to overcome the after-effects of torture from Japanese authorities, hes become addicted to opium, which takes him down a road of destruction. Aside from his drug addiction, hes an excellent sharpshooter, and with his handsome face and eccentric charm, hes loved by women all around him.

* Jung Joon-ho as Lee Geun
:A poet and humanist who worships Leo Tolstoy|Tolstoy, Lee Geun converts to insurrectionist anarchism after he meets Sergei and joins the group. Being a romanticist as well as an idealist, he suffers an ethical dilemma throughout the revolution. A handsome man with a manner that gains him trust, he leads the group with Han Myung-gon.

* Kim Sang-joong as Han Myung-gon
:Han is a cool-headed revolutionary with a gentle appearance. He came to learn Marxist-Leninist ideologies in his early years and become a model conspirator within the group. When he deals with his foes, he handles them mercilessly, but keeps his cool at all times. As the leader of the group, he keeps the others in line, while putting forward an image as an all-out stoic.

* Lee Beom-soo as Dol-suk
:Born into the lowest class of society, Dol-suk is unconditionally hostile to the haves. Even though he acts first and thinks second, hes always loyal and tends not to be shallow. He enjoys games, practical jokes and loves women, but hates anything that is complicated. Strong-headed with a foul tongue, Dol-suk values comradeship and cruelty at the same time.

* Kim In-kwon as Sang-gu
:Having lost his whole family during the Kyungshin Massacre, and in the hope of getting revenge on the Japanese ruling power, a young terrorist comes to Shanghai all alone.  But his life takes a complete turn when he meets the anarchists. Although hes very brave, he cant actively participate because hes still a young boy. He has trouble adapting to the merciless and cold-blooded terrorist activities with his weak heart. However, he admires and follows Sergei, Lee Geun and the other members and plays the observer of the entire story.

* Ye Ji-won as Kaneko
:When she sings "Sombre Dimanche" ("Gloomy Sunday"), its bewitching enough to make you want to know more about the woman behind the mysterious voice. As the queen of the club, her love overcomes all ideologies and thoughts, and she comforts the soul of a man who does not trust himself into love. With outstanding beauty and intelligence she wins the hearts of Sergei and Lee Geun in a single spell.

==Production notes and historical significance== Farewell My Concubine and Temptress Moon.  For an effective production process, the actors and the core production staff came from Korea, while the production design, elaborate sets, supporting talent and hundreds of extras were supplied by the Shanghai Film Studio.

Jang Dong-gun later spoke on the production in an interview: "The opium joint and the pipe I used were all genuine stuff from that period. The background was authentic as well. Even those cups were genuine antiques. I believed that a lot of spirits haunted the place. All the elements provided a wonderful atmosphere for filming."

==Anarchist themes in the film==
  anarchist symbol.  One scene between the anarchists and their founder also touches on the relationship between anarchists and socialists, and the turmoil that occurred among political factions after the Russian Revolution of 1917, as a  result of which many anarchists were split over whether to support the Bolshevik state, and anarchism as a movement began to see a decline in Asia.
 the West never caught on in Asia; that the anarchists were part of a larger, anti-imperial movement that is celebrated as an important part of Korean national heritage; and that today anarchism is a relatively unknown philosophy in East Asian countries.

Anarchists are not seen as barbarians, but rather as political novelties who existed during a period when anyone who rebelled was a hero.

==See also==
 
* Anarchism in Korea
* Korea under Japanese rule
* List of fictional anarchists

==External links==
*  
*  
*  
*  
*  , an introduction to Korean anarchism in this period

 

 
 
 
 
 
 
 
 
 
 