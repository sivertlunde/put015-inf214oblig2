Satya Harishchandra (1965 Kannada film)
{{Infobox film
| name           = Satya Harishchandra
| image          = 
| image_size     = 
| caption        = 
| director       = Hunsur Krishnamurthy
| producer       = Kadiri Venkata Reddy|K. V. Reddy
| writer         = 
| screenplay     = K. V. Reddy
| story          = 
| based on       =  
| narrator       =  Rajkumar Pandari Narasimharaju M. P. Shankar Rajasree
| music          = Pendyala Nageswara Rao
| cinematography = Madhav Bulbule
| editing        = G. Kalyana Sundaram D. G. Jayaram
| studio         = Vijaya Productions
| distributor    = 
| released       =  
| runtime        = 221 mminutes 
| country        = India
| language       = Kannada
| budget         =   0.8 million 
| gross          = 
}}
 Rajkumar in the lead role, as Harishchandra, an Indian mythological king, who was renowned for upholding truth and justice under any circumstance. The film is based on poet Raghavankas work, Harishchandra Kavya. The supporting cast features Udaykumar, Pandari Bai, Narasimharaju (Kannada actor)|Narasimharaju, M. P. Shankar, K. S. Ashwath and Baby Padmini.
 Best Feature Kannada cinema.    Satya Harishchandra was the third Indian and first South Indian film to be digitally coloured. The coloured version, released in April 2008, was a commercial success.

==Cast== Rajkumar as Harishchandra
* Pandari Bai as Chandramathi
* Udaykumar as Vishvamitra Narasimharaju as Nakshatrika
* M. P. Shankar as Veerabaahu
* K. S. Ashwath as Vashistha
* Baby Padmini as Lohitasya
* M. S. Subbanna
* Kupparaju
* Dwarakish
* Ramadevi
* Ratnakar
* L. Vijayalakshmi
* Rajasree
* Meenakumari

==Production==
The film was shot mostly shot at the AVM studios in Madras (now Chennai). It was reported that actor N. T. Rama Rao was very keen to make the film in Telugu language with Rajkumar in the lead role after seeing him act with finesse in one of the scenes at the studio. But, it did not take off as the latter showed no interest to act in a film of any language but Kannada. Later, the film was also made in Telugu simultaneously. However, unlike the Kannada version, it failed to perform. 

==Soundtrack==
{{Infobox album
| Name        = Satya Harishchandra
| Type        = Soundtrack
| Artist      = Pendyala Nageswara Rao|P. N. Rao
| Cover       = Kannada film Satya Harishchandra (1965) album cover.JPG
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Saregama
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Pendyala Nageswara Rao composed the soundtrack and lyrics were written by Hunsur Krishnamurthy. The soundtrack album has twenty soundtracks.  

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Vande Suranam Saramsha
| lyrics1 = Hunsur Krishnamurthy Ghantasala
| length1 = 1:12
| title2 = Hey Chandrachooda
| lyrics2 = Hunsur Krishnamurthy
| extra2 = P. Leela, Ghantasala
| length2 = 2:49
| title3 = Neenu Namage
| lyrics3 = Hunsur Krishnamurthy
| extra3 = P. Leela, P. Susheela
| length3 = 3:47
| title4 = Vamshavanu Mundarisali
| lyrics4 = Hunsur Krishnamurthy
| extra4 = Ghantasala
| length4 = 
| title5 = Ananda Sadana
| lyrics5 = Hunsur Krishnamurthy
| extra5 = P. Susheela
| length5 = 2:53
| title6 = Naana Deva Dhanagalum
| lyrics6 = Hunsur Krishnamurthy
| extra6 = Ghantasala
| length6 = 0:49
| title7 = Sathyavanu Paalisalu
| lyrics7 = Hunsur Krishnamurthy
| extra7 = Ghantasala
| length7 = 0:49
| title8 = Enidi Grahacharavo
| lyrics8 = Hunsur Krishnamurthy
| extra8 = Ghantasala
| length8 = 4:17
| title9 = Thillana
| lyrics9 = Hunsur Krishnamurthy
| extra9 = P. Leela, Pasumarthi Krishnamurthy
| length9 = 4:30
| title10 = Lakshmi Ksheerasamudra
| lyrics10 = Hunsur Krishnamurthy
| extra10 = P. Leela
| length10 = 1:54
| title11 = Kaleda Kaladalu
| lyrics11 = Hunsur Krishnamurthy
| extra11 = Nagendrappa
| length11 = 1:58
| title12 = Kanasallu Nenasallu
| lyrics12 = Hunsur Krishnamurthy
| extra12 = Ghantasala
| length12 = 1:33
| title13 = Sathyavadu Naashavaaguva
| lyrics13 = Hunsur Krishnamurthy
| extra13 = P. Leela
| length13 = 0:51
| title14 = Kuladalli Keelyavudo
| lyrics14 = Hunsur Krishnamurthy
| extra14 = Ghantasala
| length14 = 3:24
| title15 = Nanna Neenu
| lyrics15 = Hunsur Krishnamurthy
| extra15 = Swarnalatha, Jagannath
| length15 = 2:56
| title16 = Vidhi Vipareetha
| lyrics16 = Hunsur Krishnamurthy
| extra16 = Ghantasala, P. Leela
| length16 = 4:57
| title17 = Shraddhadoota Summane
| lyrics17 = Hunsur Krishnamurthy
| extra17 = B. Gopalam
| length17 = 3:03
| title18 = Bhuviyalli Munigalu
| lyrics18 = Hunsur Krishnamurthy
| extra18 = P. Leela
| length18 = 1:01
| title19 = Adigo Adithya
| lyrics19 = Hunsur Krishnamurthy
| extra19 = Ghantasala
| length19 = 0:38
| title20 = Deena Baandhava
| lyrics20 = Hunsur Krishnamurthy
| extra20 = P. Leela, Ghantasala
| length20 = 2:16
}}

==Colourisation== Naya Daur. DTS sound system.       The colourising work was carried out by a team of close to 175 personnel in Hyderabad.  The work on sound effect in the dialogues, background music and the musical track in the DTS system was carried out in Chennai. The entire soundtrack of the film digitally restored by music composer, Rajesh Ramanath.   The entire project costed an amount of  . 

==Re-releases==
Prior to the release of its coloured version, the film had been released many times across Karnataka state. The digitally coloured film was re-released for the first time on April 24, 2008, to coincide with the birth anniversary of Rajkumar (actor)|Rajkumar, in 35 screens across the state.   Like each of its previous releases, it completed a 100-day run. 

==See also==
* List of Hindu mythological films

==References==
 

== External links ==
*  

 
 

 
 
 