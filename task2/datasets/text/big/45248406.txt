Amarajeevi (film)
{{Infobox film
| name           = Amarajeevi
| image          =
| caption        = 
| director       = G Krishna Murthy
| producer       = D Narayanappa
| writer         = 
| screenplay     =  Harini T. Narasimharaju
| music          = Vijaya Bhaskar
| cinematography = S K Varada Rajan
| editing        = 
| studio         = 
| distrubutor    = 
| released       =  
| country        = India Kannada
}}
 1965 Cinema Indian Kannada Kannada film, Narasimharaju in lead roles. The film had musical score by Vijaya Bhaskar.   

==Cast==
*Raja Shankar Harini
*T. N. Balakrishna Narasimharaju
*B Ramadevi

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Halliyoora Hammera || PB. Srinivas || Kanagal Prabhakara Shastry || 02.54
|}

==References==
 

==External links==

 
 
 

 