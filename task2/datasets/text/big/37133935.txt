Summer of Secrets (film)
{{Infobox film
| name           = Summer of Secrets
| image          =
| caption        =
| director       = Jim Sharman
| producer       = Michael Thornhill
| writer         = John Aitken
| narrator       =
| starring       = Arthur Dignam Rufus Collins Nell Campbell
| music          = Cameron Allan
| cinematography = Russell Boyd
| editing        = Sara Bennett
| studio = Secret Picture Productions
| distributor    = Greater Union
| released       =  
| runtime        = 100 minutees
| country        = Australia English
| budget         = AU$370,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 309  
| gross          =
}}
Summer of Secrets is a 1976 film directed by Jim Sharman. 

==Plot==
A young couple, Steve and Kym, go to the beach where Steve grew up. They come across Dr Beverley Adams who is experimenting on the brain and is working to bring back his dead wife, Rachel, to life. Rachel comes alive and wreaks havoc.

==Cast==
*Arthur Dignam as Dr Beverley Adams
*Rufus Collins as Bob
*Nell Campbell as Kym
*Andrew Sharp as Steve
*Kate Fitzpatrick as Rachel
*Jude Kuring as shop assistant

==Production==
Sharman was attracted to the script because it was a serious exploration of the Frankenstein legend. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p164-165 

The budget was provided by the Australian Film Commission and Greater Union. The movie was shot in late 1975 under the title The Secret of Paradise Beach.  Shooting took six weeks.

==Reception==
Summer of Secrets won several awards including the Jury and Critics Prizes at the Academy of Science Fiction and Fantasy Films, Paris 1997 

==References==
 

==External links==
*  at IMDB
* 
*  at Oz Movies

 
 

 
 
 
 
 