Fonazei o kleftis
{{Infobox film
| name           = Fonazei o kleftis Φωνάζει ο κλέφτης
| image          =
| image size     =
| caption        =
| director       = Giannis Dalianidis
| producer       = Finos Film 
| writer         = Giannis Dalianidis
| starring       = Dinos Iliopoulos Rena Vlachopoulou Dionyssis Papayannopoulos Andreas Douzos Nini Janet Nikitas Platis Thanos Martinos Fitsa Davou Thodoros Kefalopoulos
| cinematography =
| photographer   =
| music          = 
| editing        =
| distributor    = Finos Film 1965
| runtime        = 83 min
| country        = Greece Greek
| imdb           = 198448
| cine.gr_id     = 
}}
 1965 Greece|Greek drama film directed and written by Giannis Dalianidis and starring Dinos Iliopoulos, Rena Vlachopoulou, Dionysis Papagiannopoulos and Andreas Douzos.  It was filmed in the same year.

==Plot==
Lia (Rena Vlachopoulou) is married to retired general Solon Karaleon (Dionisis Papagiannopoulos), who is now chairman of a public organization responsible for military supplies. Her brother Antonis ( ]) is working in the organization and has made many embezzlements there along with other employees. To justify the luxurious life of her brother, Lia and Antonis pretend that he is financially supported by a wealthy uncle of them who lives in the US, which in reality is very poor. Someday, this uncle sends a letter, possibly revealing that he is not rich, to Lia which is received by Solon but he doesnt open it in order to give it personally to Lia. Her brother tries to retrieve it but Solon leaves to his home with the letter in his wallet. Arriving home it is revealed he lost his wallet on a street and Lia is calmed down that the fraud is not revealed. Solon heads towards the police department to report the loss. Inntermittently, Timoleon "Timos" Lamprou, a very honest and stubborn accountant who frequently loses his job due to his character, finds the wallet and goes to Lias home to return it. he refuses to give the letter to Lia as the letter is firstly directed to Solon Karaleon house and secondly in person is addressed to Lia. So Timos refuses to give it to Lia which makes her furious, angry and finally violent as she attempts to throw an ashtray to him. Before she throws the ashtray, Solon returns home and is given the letter and the wallet by Timos. Solon then gives the letter to Lia but she refuses to take it, wanting not to draw attention on her ersistence to have it.
So, Solon puts it in his coat and starts taking with Timos, who reveals he was fired many times due to not being keen to cooperate with his employers to fraud the tax services. In the meantime, Lia brings Solon his robe and takes the coat with the letter, something that relieves her. Solon finally suggests hiring Timos in the organization he presides over.


  


==Cast==
*Dinos Iliopoulos
*Rena Vlachopoulou
*Dionyssis Papayannopoulos
*Andreas Douzos
*Nini Janet
*Nikitas Platis
*Thanos Martinos
*Fitsa Davou
*Thodoros Kefalopoulos

==External links==
* 

 
 
 
 