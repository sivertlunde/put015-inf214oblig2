The Story of Temple Drake
:For the 1961 film version of the Faulkner novel, see Sanctuary (1961 film).
{{Infobox film
| name           = The Story of Temple Drake
| image          = Templedrake.jpg
| image_size     =
| caption        = Stephen Roberts
| producer       = Benjamin Glazer
| writer         = William Faulkner (novel) Oliver H. P. Garrett Maurine Dallas Watkins
| starring       = Miriam Hopkins Jack La Rue
| music          = Karl Hajos (uncredited) Bernhard Kaun (uncredited) John Leipold (uncredited;additional music) Ralph Rainger (uncredited;additional music)
| cinematography = Karl Struss
| editing        =
| distributor    = Paramount Pictures
| released       = May 12, 1933
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} 1933 Pre-Code Hays Code.  It starred Miriam Hopkins as a wild Southern woman who falls into the hands of a gang led by the brutal Trigger, played by Jack La Rue.
 remade in 1961, this time under the books original title, directed by Tony Richardson, and with Lee Remick as Temple Drake, Yves Montand as Trigger (this time renamed Candy), Bradford Dillman, Harry Townes, and Odetta in a rare film appearance.

Long unseen except in bootleg 16mm prints, the film was restored by the Museum of Modern Art and re-premiered in 2011 at the TCM Classic Film Festival.

==Plot== forced into prostitution by Trigger, a backwoods bootlegger, after Trigger shoots and kills a boy who tries to protect her.  Another backwoodsman is charged with the murder.  When Temple tries to leave Trigger, he becomes angry and is apparently about to assault her, so she grabs his pistol and shoots him, then flees back home to her family.  An idealistic lawyer eventually persuades Temple to tell the truth about the first murder on the witness stand and save the defendants life, even though her testimony will disgrace herself.  According to Pre-Code scholar Thomas Doherty, the film implies that the deeds done to her are in recompense for her immorality. 

The relatively upbeat ending of the film is in marked contrast to the ending of Faulkners novel Sanctuary, in which Temple perjures herself in court, resulting in the lynching of an innocent man.

==Cast==
*Miriam Hopkins as Temple Drake
*Jack La Rue as Trigger
*William Gargan as Stephen Benbow
*William Collier, Jr. as Toddy Gowan
*Irving Pichel as Lee Goodwin
*Guy Standing as Judge Drake, Temples grandfather Elizabeth Patterson as Aunt Jennie
*Florence Eldridge as Ruby Lemar
*James Eagles as Tommy
*Harlan Knight as Pap

==Production==
George Raft turned down the male lead. PROJECTION JOTTINGS
New York Times (1923-Current file)   19 Feb 1933: X5. 

==See also==
*Pre-Code sex films

==References==
 

==External links==
* 
*  
*  
*  , IMBD

 
 
 
 
 
 
 
 
 