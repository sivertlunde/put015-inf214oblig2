Hollywood Story
{{Infobox film
| name           = Hollywood Story
| image          = Hollywood story 1951 poster.jpg
| image_size     = 200px
| alt            =
| caption        = Theatrical release poster
| director       = William Castle
| producer       = Leonard Goldstein
| screenplay     = Frederick Brady Frederick Kohner
| story          = Frederick Brady Frederick Kohner
| narrator       = 
| starring       = 
| music          = Joseph Gershenson
| cinematography = Carl E. Guthrie
| editing        = Virgil Vogel
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =   
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Richard Egan and Henry Hull. 
 Sunset Boulevard which was released the previous year. The plot is based on the notorious murder of silent movie director William Desmond Taylor.    While Hollywood Story reaches a fictional conclusion, it follows the circumstances of the real-life event closely.

Upon the release of the film, Universal promoted the appearance of several once famous silent screen celebrities. It came to light that the ones with speaking parts had received just $55 per day of shooting for their roles. Others, like Elmo Lincoln, the first screen Tarzan, were used as non-speaking extras and received only $15 per day. 

==Plot==
New York theatrical producer Larry OBrien (Conte) plans to found a motion picture company in Hollywood. He buys an old studio which was unused since the days of silent movies. There hes shown the office where a famous director was murdered twenty years earlier. Although there were many suspects the case hasnt been solved. OBrian becomes fascinated by the subject and decides to make a film based on the case. To this end he begins interviewing the surviving participants and soon gets into danger himself. In the end it turns out that the murderer is the victims jealous brother.

==Cast==
* Richard Conte as Larry OBrien
* Julie Adams as Sally Rousseau / Amanda Rousseau Richard Egan as Police Lt. Bud Lennox
* Henry Hull as Vincent St. Clair
* Fred Clark as Sam Collyer
* Jim Backus as Mitch Davis
* Houseley Stevenson as John Miller
* Francis X. Bushman as Himself
* Betty Blythe as Herself
* William Farnum as Himself
* Helen Gibson as Herself
* Joel McCrea as Himself

==Reception==

===Critical response===
Film critic Bosley Crowther panned the film and he blamed the script.  He wrote, "It is easy to see, now, why some pictures which sound promising at the start, on the strength of the ideas behind them, turn out to be dismal flops.  Hollywood Story demonstrates it ... scriptwriters Frederick Kohner and Fred Brady have cooked up in the way of a plot—is a pretty routine assembly of simple who-dunnit cliches into a silly and not very startling disclosure of a motive for a crime. The police must have been awfully lazy back in 1929." 
 Sunset Boulevard which was released in 1950 ... The climax makes for a hard to guess whodunit and a nice peek into the silent film era and at some silent stars who make a cameo appearance and speak a few lines, like Helen Gibson and Francis X. Bushman ... The only thing that failed to work smoothly into the twisty script by Fred Kohner and Fred Brady and the confident direction of William Castle, was the romance between Adams and Conte. They end up getting married at the films conclusion in a vain attempt to make this dark story seem lighter." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 