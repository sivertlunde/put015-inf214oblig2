Rambaan
{{Infobox film
| name           = Rambaan 
| image          = 
| image_size     = 
| caption        = 
| director       = Vijay Bhatt
| producer       = Vijay Bhatt
| writer         = Mohanlal Dave
| narrator       =  Chandra Mohan Amirbai Karnataki
| music          = Shankar Rao Vyas
| cinematography = G. N. Shirodkar
| editing        = 
| distributor    = 
| studio         = Prakash Pictures
| released       = 2 December 1948
| runtime        = 155 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1948 Cinema Indian film Chandra Mohan, Umakant, Amirbai Karnataki and Raj Adib.  Bhatt produced several films based on themes from the epic Ramayana, with Shobhana Samarth and Prem Adib.    The films proved successful and included Bharat Milap (1942), Ram Rajya (1943) and Rambaan. Shobhana Samarth as Sita and Prem Adib as Rama were extremely popular and accepted by the masses, in spite of the fact that Prem Adib was a Muslim.  Their success had them featuring as Rama and Sita on calendars.    Chandra Mohan played the role of Ravana.   

==Plot== Lakshman are banished for fourteen years to the forest. Ravana the king of Lanka wants to avenge his sister Surpanakhas humiliation by Lakshman. His plan is to abduct Sita by sending Maricha as a golden-spotted deer. Sita sees the deer and wants Rama to go after it as she wants its skin to make a blouse for Lakshmans wife Urmila (Ramayana)|Urmila. A cry is heard and thinking it is Rama shouting in pain, Sita sends Lakshman in search. Lakshman, loathe to go as he has promised Rama to take care of Sita finally leaves after drawing a line, the Lakshmana rekha, round the cottage to keep Sita safe. Ravan kidnaps Sita by making her step on his sandal that he places inside the Lakshman-rekha. This incident leads to Rama and Lakshmans meeting with Sugriva and Hanuman where they help in the war against Ravan to get Sita back. 

==Review==
Ram Baan came in for harsh criticism from Baburao Patel, editor of Filmindia. In his February 1949 issue, he first criticises the director for casting Prem Adib with his "sagging and emaciated muscles" as Rama, and an eight-month pregnant (at the time) Shobhana Samarth as Sita. He then brings out several salient features and scenes in the film that are "sacriligious distortions" as compared to Valmikis Ramayana. He points out a confused mix of characters and locations. He further mentions that Bhatts portrayal of Ravana as a drunk with rolling eyes shouting "Main Kaun...? Ravan!" (Who am I...? Ravan!) in every second scene is a contempt which "drags the character down".   

==Cast==
* Shobhana Samarth
* Prem Adib Chandra Mohan
* Umakant
* Bhujbal
* Ram Singh
* Raj Adib
* Leela Mishra
* Amirbai Karnataki
* Jankidas

==Soundtrack==
The film had music composed by Shankar Rao Vyas with the lyrics written by Pandit Indra, Neelkanth Tiwari and Moli. The singers included Rajkumari, Lalita Devulkar, Manna Dey, Saraswati Rane, Phulaji Bua, R. P. Sharma, Shankar Dasgupta and Amirbai Karnataki. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|-
| 1
| Banvasi More Ram
| Amirbai Karnataki
| Pandit Indra
|-
| 2
| Sakhi Ri Mohe Panchvati Man Bhayi 
| Rajkumari, Lalita Devulkar
| Pandit Indra
|-
| 3
| Seete Seete Ram Karta Pukar
| Manna Dey
| Neelkanth Tiwari
|-
| 4
| Bhagwan Aao Aao O Bhilani Ke Bhagwan
| Amirbai Karnataki
| Pandit Indra
|-
| 5
| Bina Ram Janki Sooni
| Saraswati Rane
| Pandit Indra 
|-
| 6
| Japo Ram Siya Ram
| Manna Dey, Phulaji Bua, R.P. Sharma
| Neelkanth Tiwari
|-
| 7
| Uth Lakhan Lal Priye Bhai
| Shankar Dasgupta
| Moti
|-
| 8
| Tom Tananana Dir Na
| Rajkumari, Lalita Devulkar
| Neelkanth Tiwari
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 