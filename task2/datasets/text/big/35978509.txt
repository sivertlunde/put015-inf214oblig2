Curse of the Talisman
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Curse of the Talisman 
| image          = curseofthetalisman.gif
| caption        = the PAST unlocked a MYTH released
| director       = Colin Budds
| producer       = Darryl Sheen
| writer         = Duncan Kennedy
| starring       = Jesse Spencer Capkin Van Alphen Sara Gleeson
| music          = Garry McDonald Lawrence Stone
| cinematography = Ben Nott
| editing        = 
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
}}
Curse of the Talisman is a 2001 Television movie|made-for-TV horror film directed by Colin Budds. It starred Jesse Spencer, Capkin Van Alphen and Sara Gleeson.

==Plot==
Boy has difficulty getting a drivers license and works in new age bookstore. Boy meets girl, in school. Boy hides stolen religious artifacts from Yorkshire for hippie poet bookstore owner in parents basement, but is instead bit by a keychain. Boy (Jesse Spencer) and girl (Sara Gleeson) have sweet and gentle first kiss, but soon are being chased by a predatory gargoyle. Long-haired friend and crazy priest pitch in to help save the day, even though no alcohol is consumed. Boy gets license and girl, priest continues quest against E-vil.

==Cast==
*Jesse Spencer as Jeremy Campbell
*Capkin Van Alphen as Worker
*Sara Gleeson as Fiona		
*Robert Coleby as Museum Curator
*Tempany Deckert as Miranda
*Max Garner Gore as Darryl
*Gus Mercurio as Junkyard Owner
*Rod Mullinar as Father Eccleston

==Production==

===Errors in geography===
While talking to his son about job options the father is reading a newspaper which is clearly "The Australian", an Australian newspaper not readily available in the US where this movie is supposed to be set.

==Soundtrack==
{{Track listing
| collapsed       = no
| headline        = Curse of the Talisman: Original Motion Picture Soundtrack
| writing_credits = yes
| extra_column = Performer(s)
| title1       = Noctis Penaex
| writer1      = Garry McDonald,  Lawrence Stone
| extra1       = The Imogen Singers
| length1      = 3:38
| title2       = Every Single Day
| writer2      = Jag la Vista, Arabatzls
| extra2       = Boom Box
| length2      = 3:47
| title3       = Libertine
| writer3      = Cameron Wilson
| extra3       = The Daisy Cutters
| length3      = 3:32
| title4       = Its Cool
| writer4      = Kick, Jough, Oches
| extra4       = The Dream Poppies
| length4      = 3:25
| title5       = Another Way
| writer5      = Kick, Jough, Oches
| extra5       = The Dream Poppies
| length5      = 2:48
}}

==Critical reception==
Curse of the Talisman received mixed reviews generally unfavorable from critics. Clayton Trapp of Brilliant Observations on 1173 Films gave "Curse of the Talisman" one stars (out of two) and said, "Film gets on a formulaic track early and refuses to get off, which partly works for it. Its the least scary monster movie Ive ever seen but its satisfied to be there, they just want to stay on formula and entertain us the way that others have entertained them. No pretensions (no one is sitting around reading Nietzsche), no special effects addiction (but funny where used), no sex and gore over-the-topness (the gargoyles are actually kind of cute), no bullshit (or all bullshit, according to taste)." Video Picks for Perverts simply said, "Theres no tits, either. Fucking worthless."

==External links==
* 

 
 
 
 
 
 