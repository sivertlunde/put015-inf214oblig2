Before Stonewall
{{Infobox Film
| name           = Before Stonewall
| image          = BeforeStonewall.jpg
| image_size     = 185px
| caption        = DVD cover
| director       = Greta Schiller
| co-director    = Robert Rosenberg
| producer       = John Scagliotti Robert Rosenberg Greta Schiller
| narrator       = Rita Mae Brown
| cinematography = Jan Kraepelin Sandi Sissel Cathy Zheutlin
| editing        = Bill Daughton
| distributor    =  First Run Features
| released       = June 27, 1985 (USA)
| runtime        = 87 mins
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Before Stonewall: The Making of a Gay and Lesbian Community  is a 1984 American documentary film about the LGBT community prior to the 1969 Stonewall riots. It was narrated by author Rita Mae Brown, directed by Greta Schiller, co-directed by Robert Rosenberg, and co-produced by John Scagliotti and Rosenberg, and Schiller. It premiered at the 1984 Toronto Film Festival and was released in the United States on June 27, 1985. In 1999, producer Scagliotti directed a companion piece, After Stonewall.

==People featured==
{{Columns-list|3|
*Ann Bannon
*Lisa Ben
*Gladys Bentley
*George Buse
*Carroll Davis
*Martin Duberman
*Allen Ginsberg
*Barbara Gittings
*Barbara Grier
*Mabel Hampton
*Harry Hay
*Evelyn Hooker
*Jim Kepner
*Audre Lorde
*Bruce Nugent
}}

==Awards==
Before Stonewall was nominated for the Grand Jury Prize at the 1985 Sundance Film Festival. {{cite web
  | title =Awards for Before Stonewall
  | publisher =Internet Movie Database
  | url =http://www.imdb.com/title/tt0088782/awards
  | accessdate =2007-11-16  }}  It won the Best Film Award at the Houston International Film Festival, Best Documentary Feature at Los Angeles FILMEX, First Place at the National Educational Film Festival, and  Honorable Mention at the Global Village Documentary Festival. {{cite web
  | title = Before Stonewall - Independent Historical Film
  | publisher = Jezebel Productions
  | url = http://www.jezebel.org/films/stonewall.html archiveurl = archivedate = 2007-12-13}}  In 1987, the film won Emmy Awards for Best Historical/Cultural Program and Best Research.  In 1989, it won the Festivals Plate at the Torino International Gay & Lesbian Film Festival. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 