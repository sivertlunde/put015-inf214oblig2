Paromitar Ek Din
{{Infobox film
| name           = Paromitar Ek Din
| image          = Paromitar Ek Din.jpg
| caption        = DVD cover for Paromitar Ek Din.
| director       = Aparna Sen
| writer         = Aparna Sen
| starring       = Rituparna Sengupta Aparna Sen Soumitra Chatterjee
| producer       =
| distributor    =
| cinematography =
| editing        =
| released       = 8 january, 2000
| runtime        =
| country        = India Bengali
| music          =
}}
 Bengali drama film directed by Aparna Sen.

==Plot==
This film is exploring the dual themes of friendship and loneliness. Sanaka (Aparna Sen) and Paromita (Rituparna Sengupta) are mother and daughter-in-law who, despite differences in age, backgrounds and temperaments, build a strong bond together. But when Paromitas marriage to Sanakas son breaks down, social mores prevent the women from remaining close friends.
While Paromita remarries and begins a new life, her mother-in-law, Sanaka, is left heartbroken and alone and eventually falls seriously ill. When Paromita learns of her friends deterioration, she is compelled to flout convention, and returns to nurse Sanaka on her deathbed.

==Cast ==
* Aparna Sen as Sanaka
* Rituparna Sengupta as Paromita
* Sohini Sengupta as Khuku
* Soumitra Chatterjee as Moni Biswas (Moni-daa or Moni-mama)
* Rajatava Dutta as Paromitas husband (Sanakas younger son)
* Rita Koiral as Sanakas elder daughter in law Rajesh Sharma as Rajeev Shrivastav (Paromitas second husband)

==Awards==
The film had won many national and international awards -

*2000 - Ecumenical Jury Award at the 35th International Film festival of Karlovy Vary
*2000 - Best Film recommendation from the FIPRESCI Jury at 3rd International Film Festival of Mumbai
*2000 - National Film Award for Best Supporting Actress for Sohini Sengupta
*2000 - National Film Award for Best Feature Film in Bengali
*2000 - National Film Award for Best Female Playback Singer - Jayashree Dasgupta - "Hridoy Amar Prokash Holo..."

==External links==
* 
*Chatterjee, Shoma A.  , Movie Reviews, Rediff.com, 8 February 2000.

 
 

 
 
 
 
 
 
 


 