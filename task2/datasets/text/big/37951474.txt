Chhota Bheem and the Curse of Damyaan
 
 
 
{{Infobox film
| name           = Chhota Bheem and the Curse of Damyaan
| image          = Chhota Bheem and the Curse of Damyaan poster.jpg
| image_size     = 240px
| border         = 
| alt            = 
| caption        = Theatrical Poster
| director       = Rajiv Chilaka
| producer       = Rajiv Chilaka
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Sunil Kaushik
| cinematography = 
| editing        = 
| studio         = Green Gold Animation
| distributor    = 
| released       =   
| runtime        =  88 minutes
| country        = India
| language       = Hindi and English
| budget         = Rs 4 crore  
| gross          = Rs 50&nbsp;million  $834,307 USD
}}
Chhota Bheem and the Curse of Damyaan (2012) is an Indian animation film based on the characters Chhota Bheem and his friends.This is eleventh movie in Chhota Bheem series and first movie in theatre. The film was made under Green Gold Animation banner in association with PVR Pictures.    The film got surprise success at box office.  This movie has awarded Superstar of Indian Animation.

== Plot ==
The film starts in a desert at night damyaans time comes he tells to his troops a man could release him from the curse.Then a mahaan tells damyaan could not release who save the earth from damyaan.In Dholakpur princess indumathi was kidnaped by mangal singh  then bheem enters with a song and saves the princes,where Jaggu, Bholu, Dholu, Raju, Chutki & Kalia are planning their friend Bheems secret birthday party while Damyaan, a demon who, centuries ago, was granted immortality by the book of Magi along with a curse that he would be confined to the city of Sonapur, wants to escape from this confinement and get back his power and kingdom. His minister Skandi meets King Indravarma & tells him about the hidden treasures of Sonapur thus luring him. The king agrees to go to the city in search of that hidden treasure. Despite of being warned by the gypsies, he starts his journey along with Bheem and his friends. After reaching the city, the king unknowingly sets the demon free by great demon entarance. The demon captures the king, Chhota Bheem and his friends in a prison. chota bheem set free his friends then Bheem meets Santrik, who tells him that the only way to get prisoners free from Damyaans capture is defeating the demon by destroying the Book of Magi. Then Santrik sends them back in time where the kaalsainiks, the henchmen of Damyaan, figure out that they are not from Sonapur. so they dont allow them .A fight ensues, and in the process, Dholu and Bholu are turned into frogs. Bheem somehow manages to defeat them and dons their clothes along with his friends and enters Sonapur pretending as a bunch of Kaalsainiks. Soon another group of kaalsainiks arrive and a sweet shop owner named Gulabchand figures out that they are not kaalsainiks and manages to save the group. In the process Bheem is badly wounded and faints while passing through the magical door. Upon being brought into wakefulness, he observes that Dholu and Bholu have regained their respective human forms. The sage who saves them, Guru Sambhu, tells them that despite being brave and strong, they cannot defeat damyaan without magic then he took them to a majic place and starts a song (jam jam jamboora)and teaches magic to the group. After completing their class on magic, he provides them with respective powers: Raju gets a magical bow and arrow which never misses its aim; Chutki gets two magical trees; Jaggu a bunch of magical stinging bananas; Kaalia the power to get invisible; Dholu and Bholu the power to multiply into a hundred Dholus and Bholus and Bheem gets an almost impossible hymn to seize the power of the enemy which was not decoded till date.
At night they hear sounds of celebrations, which are of the end of the second term of Damyaans kaalkriya, which involves trapping ghosts for their powers in three different rings in three terms by using the hymns given the book of magi.Bheem and his team are teleported to Damyaan`s castle.Using their respective powers, they manage to seize the first ring, but while seizing the second ring kaalia activates a trap which covers them in a strange gas.Upon recovering, Bheem comes to know that they are captured by Damyaan. Damyaan tells him that in an hour it will be his birthday and that he has decided to give him a "birthday gift"-watching his friends die. On an instant 
bheem decodes the hymn and releases his friends. They fight and defeat the kaalsainiks while Bheem challenges Damyaan to fight without magic. Damyaan then transforms into his powerless form, a Naag with an ambition of ruling the world. Bheem eventually beats up Damyaan in his trademark "jump and kick and punch" style and eventually destroys him and all the existing kaalsainiks.On his birthday,Guru Sambhu, who is the rightfull king of sonapur, decides to proclaim Bheem as the next king while Bheem refuses, saying that "I am not the prince of Sonapur, but Bheem of dholakpur" while Raju comments on the dialogue saying that it is "a real heros talk". Before teleporting the group back to Dholakpur, Guru Sambhu turns a badly scared Skandi, who was found by Gulabchand while hiding in his sweets, into a mouse. Dholu and Bholu further punish him by chasing him dressed as snakes. Then Bheem and his team return to the future to Dholakpur, only to realise that the whole of Dholakpur has fixed a grand party for its hero, Chhota Bheem.    

==Characters==
*Bheem ( Kaustav Ghosh )
*Chutki ( Rupa Bhimani )
*Raju ( Julie Tejwani )
*Jaggu ( Rajesh Kava )
*Kaalia ( Mausam )
*Dholu-Bholu ( Jigna Bhardwaj )
*Damyaan ( Dilip Sinha )
*Raja Indraverma ( Arun Shekhar )
*Singhala ( Anamaya Verma )
*Guru Sambhu ( Nandkishore Pandey )

== Filming ==
The animation series Chhota Bheem was launched in 2008. After the huge success of the series Indian animation content producer Green Gold Animation in association with PVR Pictures decided to make a full-length feature film on Chhota Bheem. The movie was directed by Rajiv Chilaka. Animation director of the film was Dwell Mina and the music of the film was composed by Sunil Kaushik. 

== Release and reception ==
The film released on 18 May 2012 and got a surprise success. The film collected   in first three days and   in its first week despite having a limited release and even the film did much better business than Amitabh Bachchan and Sanjay Dutt starrer Department (film)|Department.Released digitally in 250 screens across the country, Chhota Bheem and the Curse of Damyaan has raked in approximately Rs 50&nbsp;million at the box office, which Green Gold claims is the highest grossing an Indian animated movie has ever made after Hanuman (2005 film)|Hanuman.      

== References ==
 

== External links ==
*  

 

 
 