The Documentary of OzBo
 
 

{{Infobox film
| name           = The Documentary of OzBo
| image          = 
 
| alt            =
| caption        = 
| film name      = 
| director       = Gene Hamil
| producer       = Gene Hamil
| writer         = Gene Hamil
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         =       
| distributor    = 
| released       =   
| runtime        = 107 Minutes
| country        = U.S.
| language       = English  
| budget         = 
| gross          =  
}}
 American independent horror film documentary written, produced and directed by Gene Hamil.  The film stars Jon Miller, Tre Burch, Brandy Donatiello, James Hayman, Missy Moranto, Bryan Keith, Austin Lee, Chase Waters, Zach Goodman, Chris Spence, Rob Hood, and David Childers. The movie was first screened at Geek-O-Nomicon in Biloxi, Mississippi December 13, 2014. 

==Plot==

The film follows a team of paranormal investigators hired by a museum to prove or disprove the legends of an evil clown named OzBo. When the investigation proves some of the legends to be true only a couple of the investigators escape the horror of OzBo himself. These survivors are interviewed in this documentary to inform viewers of their feelings of what happened during that investigation. 

==Cast==
* Jon Miller as Tzvetomir and Human OzBo
* Tre Burch as J.D.
* Brandy Donatiello as Sonya
* James Hayman as OzBo and Max
* Missy Moranto as D.C.s sister
* Bryan Keith
* Austin Lee
* Chase Waters as D.C.
* Zach Goodman as Axel
* Chris Spence
* Rob Hood as Bobby
* David Childers as Dalton

==References==
 

 