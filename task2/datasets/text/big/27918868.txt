Chidambara Rahasiyam (film)
 
{{Infobox Film
| name           = Chidambara Rahasiyam
| director       = Visu
| producer       = Visalakshi Manikkam
| writer         = Visu
| starring       = S. Ve. Shekher Visu Delhi Ganesh Arun Pandian Ilavarasi
| music          = Shankar Ganesh
| cinematography = N. Balakrishnan
| editing        = Ganesh-Kumar
| studio         = MM Film Circuit
| released       =  
| runtime        = 134 mins
| country        = India
| language       = Tamil
}}
Chidambara Rahasiyam is a 1985 Tamil comedy thriller film written and directed by Visu. It stars Visu, S. Ve. Shekher, Delhi Ganesh, Arun Pandian, Ilavarasi and Manorama (Tamil actress)|Manorama. The film revolves around a simpleton (S. Ve. Shekher) who is framed for murder. Here enters Visu who teams up with the local police and Arun Pandian to find the real culprits. The subject matter of the film is not related directly to Chidambara Rahasiyam, a Hindu religious belief. The film was well appreciated for its comedy.

==Cast==
* S. Ve. Shekher as Chidambaram
* Visu as Beemarao
* Arun Pandian as Arun
* Ilavarasi as Uma
* Delhi Ganesh as Masilamani
* Kishmu as Kattamuthu Chettiyar Manorama as Valayapatti Valliammai
* G. Seenivasan
* Kanthimathi as Ponnazhagi Anuradha
* Disco Shanthi as Aasha
* Sangili Murugan as Ambalavaanan

==Plot==
Chithambaram(S.Ve.Shekar),a good hearted simpleton from a rich family based in karaikudi is in love with his cousin Uma(Ilavarasi).Uma is a well educated girl and in love with Arun(Arun Pandian), her collegemate. when chithambarams parents speak to  umas parents about their(chithambaram and uma) marriage,umas mother insults chithambaram for his stupidity.so,chithambaram sets to find a job and earn money so that he can ask umas hand for marriage with respect.But what ensues is a series of unexpected twists and turns for chithambaram as he is wrongly accused of smuggling,burglary and murder of a young woman all done in the same night of his first job offer by  a wealthy and respectable man.And the impossibility of him having committed all three crimes gives him the benefit of doubt by the police who seeks the assistance of beemarao(visu),a CID inspector.Beemarao is threatened by goons on his arrival to karaikudi(the story is set in karaikudi and nearby localities) and he has his first doubt of someone elses hand in the crimes.he ventures upon finding the real criminals behind the mishappenings along with arun through clever deduction and effort. In the end what turns out to be another surprise is the identity of criminal himself.

==References==
 

 

 
 
 
 
 


 