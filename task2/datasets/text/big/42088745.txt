The Hungover Games
 
{{Infobox film
| name           = The Hungover Games
| image          = The Hungover Games.jpg
| alt            = 
| caption        = 
| director       = Josh Stolberg
| producer       = Jim Busfield Ben Feingold Jamie Kennedy
| screenplay     = Kyle Barnett Anderson
| story          = David Bernstein
| starring       = Ross Nathan Ben Begley Herbert Russell Rita Volk Jamie Kennedy
| music          = Todd Haberman
| cinematography = Andrew Strahorn
| editing        = Byron Wong
| studio         = 
| distributor    = Sony Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $130,068  (Video sales only)  
}}
The Hungover Games is a 2014  , The Lone Ranger, Django Unchained, Thor (film)|Thor, Carrie (2013 film)|Carrie, District 9 and The Real Housewives.

==Plot== Effie Trinket. When Ed attempts to leave and opens the door only to realize they are on a train, Zach realizes they are in the Hunger Games, or as Effing calls it the "Hungover Games", a fight to the death in an arena between various Hollywood districts. The three watch a video on Zachs phone and realized that Doug and the trio volunteered as tribute the previous night in their drunken state. The three realize they will have to fight in the games to find Doug and get him back to his wedding in time. 

At the training center the three become acquainted with other tributes and the heavily favored "career" tributes, including homosexual Thor_(Marvel_Comics)|Thor, Tonto, Gratuitous Nudity (two topless women), Carrie, and Ted_(film)|Ted. Ed meets Katnip Everlean, a girl he met the previous night while drunk and who likes him. Soon training ends and they are sent to rooms where they will ascend into the arena. While searching the rooms they see a man ascending into the arena but cannot see his face and are forced to leave after Zach spills bubble tea on the rooms electronic controls. The games begin, and the tributes begin murdering each other and Zach becomes separated from Ed and Bradley. The two flee and spend the night in a tree to sleep but witness the careers murder two other tributes. They see Zach with the Careers and realize that Zach has allied with them to help them find and kill Ed and Bradley. In the morning Bradley is urged by another tribute, Little Boo, to drop a hive of "Swaggerjacks" (wasps) onto the careers sleeping below them, however in the process both he and Ed are stung several times and the hallucinogenic venom causes them to pass out. They awaken and find Zach has nursed them back to health and go search for Doug again. Zach reveals that the reason they are in the Hungover Games is because the previous night he plugged in air-fresheners that were "midnight berry" scented and that the scent of the poisonous berries drugged them. He also reveals that the air-fresheners "may cause transportation to futuristic dystopia." Enraged, Ed and Bradley leave Zach and he is later seduced by a female Avatar. He is ambushed by the careers but is saved by Katnip and Boo, who kill Gratuitous Nudity, though the female Avatar is also murdered. 

Meanwhile Ed and Bradley decide to reunite with Zach after a rule change that states that four men who share a "bromance" can all win together. Ed and Bradley find Zach and Katnip and Boo and remember that Boo may know where Doug is. Before she can say however she is killed by Thor, who is murdered by Ed in retaliation. After a short funeral for Boo Katnip decides to leave on her own to kill Tonto. The three decide to head back to the Pornucopia after an announcement that something everyone is looking for is located there. Assuming it is Doug the three rush to the Pornucopia but are attacked by Carrie and Ted. Zach murders Carrie in a rage however and Katnip returns and saves Ed from Ted. She reveals she was mortally wounded in her fight with Tonto and dies with Ed besides her. The three realize that their prize at the Pornucopia is a cell phone, not Doug and they realize they will never find him and call Tracey to inform him of the situation. But after a short conversation with Zach Ed realizes where Doug is and stops Bradley from making the call. Ed explains that Dougs tube never came up because Zachs bubble tea shortcircuited the tube that sends up tributes. After looking in his tube however they realize Doug isnt there, but Doug emerges from the forest camouflaged and attacks them, berating them about how they always forget him and how he is almost never in the movies. The three tell him they will never forget him again and that he will have more lines in the next film and he decides to spare them. 

Another rule change states that there can only be one victor and the four turn on each other. Zach however proposes they all eat poisonous berries, as the creators of the games would never let them all die as they would rather have four victors than none and so would interrupt the four before they can eat the poison. Zachs plan fails after they eat the berries and die and no one interrupts them. They wake up in their hotel room, realizing it was all a dream they shared. The four go to the wedding where Ed laments about Katnip but runs into an identical looking girl who seems to be attracted to him. The four happily enjoy the wedding as the pastor (who looks like another character from the Games) announces "May the odds be ever in your favor" and the movie ends.

==Cast==
* Ben Begley as Ed
* Ross Nathan as Bradley
* Herbert Russell as Zach
* Rita Volk as Katnip Everlean
* John Livingston as Doug
* Robert Wagner as Liam
* Bruce Jenner as Skip Bayflick
* Hank Baskett as Stephen A. Templesmith
* Brandi Glanville as Housewife Veronica
* Camille Grammer as Housewife Tanya
* Kyle Richards as Housewife Heather
* Dat Phan as Bao
* Sam Pancake as Tracey
* Ron Butler as President Snowbama
* Tara Reid as Effing White
* Chanel Gaines as Boo
* Kayden Kross as Topless Blonde
* Jonathan Silverman as Chineca Lame
* Jamie Kennedy as Justmitch / Willy Wanker / Tim Pistol
* Steve Sobel as Kaptain Kazakhstan 
* Sophie Dee as Topless Brunette
* Caitlin Wachs as Scary
* Terra Jole as Teddy

==External links==
* 

==See also==
*The Starving Games

==References==
 

 
 
 
 