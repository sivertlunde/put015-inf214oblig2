Panique
 

{{infobox film
| name          = Panique
| image          = Panique 1947 film poster.jpg
| caption        = French theatrical release poster
| director      = Julien Duvivier
| producer      = Pierre OConnell
| screenplay    = Julien Duvivier Charles Spaak
| based on      =   
| starring      = Michel Simon    Viviane Romance
| music         = Jean Wiener    Jacques Ibert
| cinematography = Nicolas Hayer
| editing       = Marthe Poncin
| studio         = Filmsonor
| distributor    = Filmsonor
| runtime       = 91 minutes
| released      =  
| country       = France
| language      = French
}}

Panique is a French film directed by Julien Duvivier, made in 1946 and released in 1947, starring Michel Simon and Viviane Romance. The screenplay is based on the novel Les Fiançailles de M. Hire by Georges Simenon. The film was released in the United States as Panic.

In 1989 Patrice Leconte remade the film as Monsieur Hire, with the title role played by Michel Blanc.

==Plot==
The strange and slightly unsettling Monsieur Hire is suspected of a crime. A crowd tracks him down and he seeks escape on the roof of a building.

==Cast==
{{columns-list|2|
* Viviane Romance as Alice
* Michel Simon as M. Hire
* Max Dalban as Capoulade
* Émile Drain as M. Breteuil
* Guy Favières as M. Sauvage
* Louis Florencie as Inspector Marcelin
* Charles Dorat as Inspector Michelet
* Lucas Gridoux as M. Fortin
* Marcel Pérès as Cermanutti
* Lita Recio as Marcelle
* Michel Ardan as Fernand
* Michèle Auvray as Mme Branchu
* Lucien Carol as Inspecteur Benoit
* Olivier Darrieux as Étienne
* Josiane Dorée as Mouchette
* Paul Franck as Docteur Philippon
* Magdeleine Gidon as Mme Capoulade 
* Jenny Leduc as Irma
* Louis Lions as Marco
* Emma Lyonel as La cliente
* Jean-François Martial as M. Joubet
* Lucien Paris as M. Branchu 
* Jean Sylvain as Raphaël Paul Bernard as Alfred
* Robert Balpo as Le client
* Suzanne Desprès as La cartomancienne
}}

==Comment== Georges Lacombes Martin Roumagnac, - the harrying of a Jew to his death in Duviviers Panique, and a number of Simenon and Steeman thriller adaptations rife with violence and revenge - all of these films attest to a need to project the immediate past on to a different set of narratives that are removed from the immediate arena of guilt (although Panique comes uncomfortably close). Dark social realism is to be found in a considerable number of films during the five-year period after the end of the war. The films of Henri-Georges Clouzot and Henri Decoin are the most remarkable in this context in their fierce, almost cynical pessimism, but the works of Yves Allégret and  Julien Duvivier in that period come close on their heels."  

==References==
 

==External links==
*  
*  
*   (French)

 

 
 
 
 
 
 
 


 