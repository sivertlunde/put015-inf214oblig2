Autumn Sun
{{Infobox film
| name           = Autumn Sun
| image          = Sol de otono.jpg
| image size     = 250px
| caption        = English DVD Cover
| director       = Eduardo Mignogna
| producer       = Executive Producer: Lita Stantic Eduardo Mignogna
| writer         = Eduardo Mignogna Santiago Carlos Oves
| narrator       = Bobby Flores
| starring       = Norma Aleandro Federico Luppi
| music          = Edgardo Rudnitzky	
| cinematography = Marcelo Camorino
| editing        = Javier del Pino Juan Carlos Macías
| distributor    = Pathé
| released       =  
| runtime        = 110 min.
| country        = Argentina
| language       = Spanish
| budget         =
}} Argentine drama film directed by Eduardo Mignogna and starring Norma Aleandro and Federico Luppi. It was written by Mignogna and Santiago Carlos Oves. Lita Stantic was the executive producer. 

==Synopsis==
Clara Goldstein ( , and she has been lying to him about being in a romantic relationship. Fortunately, Raul goes along with the ruse. Not long after, the couple begin to fall in love.

==Cast==
* Norma Aleandro as Clara Goldstein
* Federico Luppi as Raul Ferraro
* Jorge Luz as Palomino
* Cecilia Rossetto as Leticia
* Roberto Carnaghi as Cohen
* Erasmo Olivera as Nelson
* Nicolás Goldschmidt as Wilson
* Gabriela Acher as Silvia

==Reception== Mastroianni and Sophia Loren|Loren, they also impressively overcome certain Hollywood-like contrivances of plot and dialogue the latter two actors seldom had to contend with. Its a tribute to these stars that, even given the trite situation of the love-shy odd couple gradually facing the inevitable, every halting step they take toward each other feels like a mini-triumph of loves power over the schoolmarmish intellect. They portray with touching specificity what its like to crave total surrender to love even after long years of experience have proven the foolhardiness of such blind leaps. Not even the blatantly market-tested ending (a malady that seems to be spreading worldwide like Hong Kong flu) detracts from the pleasure of this admirable, eminently watchable date flick. Well worth the price of admission, whether or not you qualify for the senior discount." 

==Awards==

===Wins===
* San Sebastián International Film Festival: OCIC Award, Eduardo Mignogna; Silver Seashell Best Actress, Norma Aleandro; 1996.
* Argentine Film Critics Association Awards: Silver Condor; Best Actor, Federico Luppi; Best Actress, Norma Aleandro; Best Cinematography, Marcelo Camorino; Best Director, Eduardo Mignogna; Best Film; 2007.
* Goya Awards: Goya; Best Spanish Language Foreign Film, Argentina; 1997.
* Oslo Films from the South Festival: Honorable Mention, Eduardo Mignogna; 1997.

===Nominations===
* Argentine Film Critics Association Awards:  Best Music, Edgardo Rudnitzky; Best Screenplay, Original), Eduardo Mignogna and Santiago Carlos Oves; Best Supporting Actress, Gabriela Acher; 2007.
* San Sebastián International Film Festival: Golden Seashell, Eduardo Mignogna; 2006.

==References==
 

==External links==
*  
*   at cinenacional.com  
*  
 
 
 
 
 
 
 
 