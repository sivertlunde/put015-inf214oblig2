1612 (film)
{{Infobox Film
| name           = 1612
| image          = 1612_movie_poster.jpg
| image_size     =
| caption        = Promotional 1612 film poster
| director       = Vladimir Khotinenko
| producer       = Nikita Mikhalkov
| writer         = Arif Aliyev
| narrator       = 
| starring       =   (Spain) Artur Smolyaninov Marat Basharov
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = November 1, 2007
| runtime        =  Russia
| language       = Russian Polish Spanish Italian Dutch
| budget         = $12,000,000
}} Russian historical National Unity Polish troops from Moscow.

==Plot==
The film is set in Russia during the Time of Troubles, a period of civil disorder, famine and foreign invasion that followed the fall of the Rurik dynasty, which had ruled Russia in various capacities from 862 to 1598. The protagonist, Andrey, had been a servant at Tsar Boris Godunovs court while he was a boy, where he became the sole witness to the murder of the tsars family by the order of plotting boyars. Andrey is sold to slavery. Now, seven years later, he is bought by a Spanish mercenary, Alvaro Borja, who seeks to profit over this war by fighting for the Polish army. The company falls prey to a band of robbers and Alvaro is killed. Andrey and his friend Kostka know that serfs without a master are considered like runaways and will be hanged. Andrey disguises himself in his dead masters clothes and assumes his name, taking the guise of a "Spanish knight."

Andrey is hired by the Polish Lord Osina, who wants to capture Moscow and use Princess Xenia Godunova to raise himself to the Crown. As they approach a city, Andrey protects a Russian girl from the Polish soldiers. Andreys true identity is discovered and he is arrested. Kostka however frees Andrey and steals Princess Xenia (she drank sleeping medicine) and with the help of his Russian girlfriend, they sneak over to the Russian garrison. Unfortunately the Russians tell them that Prince Dmitri Pozharsky had led most of the troops to Moscow with all the city cannons. Andrey then makes a cannon of leather. At dawn, the Polish army demands the citys surrender. Andrey fires a hot cannonball into the Polish ammunition storage, which explodes, killing most of the Polish army. The next day, more Poles arrive and attack the city, but the Russians hold firm. Osina orders all the cannon on the gate breaking a way into the city. Meanwhile, Andrey moves their cannon off the wall and to the gate. The Husaria begin to charge. Andrey loads a chain shot into the cannon and they fire as the enemy closes, decapitating the Polish Hussars. The gate collapses and the rest begin to flee. Osina calls Andrey to negotiations, saying that if Xenia does not come out herself, he will kill her daughter. Andrey himself leads her out of the city.
 Battle for Moscow lasts for three days. The Polish flee in defeat and Osina is captured. Andrey calls him to a duel and kills him. The Russian nobles are angry at Xenia because she had converted to Catholicism, and send her to live in a monastery. Soon, a new Czar is elected, Michael Romanov, whose dynasty rules Russia for the next 300 years.

==Historical accuracy==
The film is based on historical events and includes some fictional elements. According to the anti-Putin opposition coalition The Other Russia, "most of the history   has been diluted beyond recognition".    The movie takes artistic licence with real events, e.g. contrary to historical fact, Polish troops are thrown back from Moscow, while they actually held the city for two years. Kuzma Minin and Dmitry Pozharsky, who were instrumental in organizing the popular uprising that led to the expulsion of Polish-Lithuanian forces, appear only briefly at the movies conclusion. 

==Controversy==
Critics of the Kremlin have compared it to Soviet propaganda.  Questions were raised about the alleged Anti-Polish sentiment|anti-Polonism of the movie, but the films director, interviewed in the Polish as well as Russian press, stressed that the movie was in no way intended to defame Poles. The director, Vladimir Khotinenko, claimed it was made for entertainment purposes as well as to raise awareness of the new holiday among the general public.  

International as well as Russian critics suggest that the movie, which was commissioned by the Kremlin,  showcases key political ideas pushed by the Kremlin in advance of the parliamentary  . The Time of Troubles, as portrayed in the film, represents the last decade of the 20th century, when Russia was undergoing severe hardships. Khotinenko was quoted as saying, "Its important for me that the audience feel pride. That they didnt regard it as something that happened in ancient history but as a recent event. That they felt the link between what happened 400 years ago and today."

==References==
 
* 
* 

== External links ==

* 
* 

 
 
 
 
 
 