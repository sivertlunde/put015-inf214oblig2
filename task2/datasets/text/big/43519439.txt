Preservation (2014 film)
{{Infobox film
| name           = Preservation
| image          = Preservation 2014 film poster.png
| alt            = 
| caption        = Teaser poster
| film name      = 
| director       = Christopher Denham
| producer       = Jennifer Dubin Cora Olson
| writer         = Christopher Denham
| starring       = Wrenn Schmidt Pablo Schreiber Aaron Staton Cody Saintgnue
| narrator       =  Samuel Jones Alexis Marsh
| cinematography = Nicola Marsh
| editing        = Brendan Walsh
| studio         = Present Pictures The Orchard
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Preservation is a 2014 horror thriller film that was directed by Christopher Denham. It had its world premiere on April 17, 2014 at the Tribeca Film Festival and stars Wrenn Schmidt as a woman trapped in a forest preserve, stalked by maniacs. 

==Synopsis==
Hoping to escape their own troubles, Wit (Wrenn Schmidt), her husband Mike (Aaron Staton), and his brother Sean (Pablo Schreiber) decide to head out into a secluded forest preserve on a hunting trip. Mike is hoping that this will help Sean deal with his PTSD while Wit is hoping that she can rekindle her sagging relationship with Mike, who seems to be more interested in spending time on his cellphone than with her. Once they reach their destination the trio is undeterred by signs proclaiming that the preserve is closed and they continue on with their vacation unabated. The already strained atmosphere is made even worse when they wake up the next morning to discover that someone has stolen all of their belongings and marked each persons forehead with a big black "X". As the trio tries to deal with their existing stresses and the new fear of being hunted, they begin to turn on one another.

==Cast==
*Wrenn Schmidt as Wit Neary
*Pablo Schreiber as Sean Neary
*Aaron Staton as Mike Neary
*Cody Saintgnue as Jack
*Nick Saso as Ben
*Michael Chacon as Will

==Plot==
Wit, her husband Mike, his brother Sean, and Sean’s dog Buck drive out to a secluded forest for a hunting trip. Mike and Wit are noticeably distant due to Mike’s obsession with work. The trip was originally supposed to be a getaway for the two of them until Mike invited Sean. 

When they first arrive at the preservation it is closed off but Sean and Mike rip down the chain blocking the entrance and tell Wit that everything will be fine.  Sean plans on hunting the old fashioned way, he doesn’t want to follow marked trails, but instead follow the animals.

As the group begins their journey they mark off a tree with a spray painted smiley face so that they can safely find their way back. As they continue into the forest they encounter their first deer in a hunting tower. Wit chases the deer into the forest and can’t get herself to kill it. As she lowers her gun she hears the crack of a gunshot and turns to see Sean. 

While gutting the deer Sean gets Wit to help him even though she’s a vegan. Wit guts the deer without any problems, explaining her past experiences in the ER and how she is desensitized to blood. While Sean finished gutting the deer Mike and Wit have a conversation about how Sean was discharged from the military. 

At night Mike goes off to take another work phone call and Wit is noticeably upset. While Mike is gone Sean and Wit decide to play truth or dare around the campfire. Sean and Wit’s sexual tension is brought to light during the game. While Mike is in the forest he begins to hear strange sounds. Buck runs out and the sounds stop. Mike takes a picture with Buck and in the background a strange figure can be seen. 
The next morning Wit and Mike wake up on the ground with all of their equipment gone, including their shoes and tent. They also are marked with X’s on their foreheads. Sean is nowhere to be found so Mike assumes he was the one behind this act. Sean comes back after trying to find out who stole their stuff and Buck. He tries to convince Mike it wasn’t him but to no avail. They all decide it’d be best to make their way back to the car, but without a map or GPS they have to rely on Sean and the sun for direction.

While heading back Sean and Mike begin to fight due to various reasons, which leads to Wit revealing she’s pregnant. Sean gets Wit and Mike far enough so that they can go back to the car on their own, and then heads off on his own to find Buck. As Wit and Mike separate from Sean another unknown person is shown watching them.

Sean is lead to Buck by the sounds of squeaky toys. Blood begins to drip on his face and he looks up to find Buck dead, hanging from a tree. Gunshots are then fired at Sean as he scrambles behind a tree. While hiding he fashions a spear out of a stick and sneaks his way up to the hunting tower the shooter is in. Sean stabs the shooter and beats him to what seems to be an unconscious state. He takes the gun and while reloading it the shooter stands up behind him. Sean turns around and meets the shooter’s eyes through a mask. When Sean looks down he sees the shooter stabbed him in the stomach, he falls back and the shooter begins to video tape his dying body. 

Mike and Wit make it back to the tree they marked only to realize that every tree has now been marked in the exact same way. They are lost and continue their journey until Mike steps in a trap that injures his foot. They find a cabin which they proceed to break into. Wit takes down information about the map of the forest and they decide she needs to go without Mike to the car due to his injury. They then hear music outside the cabin and realize it’s the killer. Mike draws the killer’s attention so that Wit can escape to the car; he ends up hiding in a porta-potty but is found by the killer. He breaks out and begins to fight the killer, only to be shot. 

The three killers take a video of Mike’s body in a dumpster next to Sean’s and begin to ride bikes towards Wit. They find Wit through a tracking device Mike had put on her earlier and begin to chase her. Wit eventually trips over a rock and suffers a severe head injury, but still manages to hide. 

Wit eventually makes it back to the car and tries to hotwire it with no luck. She pops the hood only to see the same markings that were painted on the trees on the inside of the hood. She gathers all of the supplies she can from the car and heads back into the forest.
The killers are shown sitting by a river and playing games on cell phones. They take off their masks and it is revealed that they are teenage boys. Road flares begin to go off behind the boys and the forest becomes filled with smoke. Wit is hiding beneath the smoke and attacks one of the killers and takes his weapons.

Another one of the killers comes across a cabin with music playing and his friends mask hanging outside. He too is killed by Wit.
Wit ambushes the last killer, also the head of the group, but he is able to fight back and knocks her unconscious. He drags her to a picnic table and ties her down to it. As he is about to stab her he receives a phone call and stops. He goes off and takes the phone call which is from his mother. Wit is able to escape the table in the short amount of time he takes for the phone call. 

The killer finds her and begins to strangle her. She reaches for a broken bottle and is able to stab him in the neck. She takes one of the bikes and begins to ride out of the forest eventually reaching a town. She ends up in a parking lot where a child pretends to shoot her with a toy gun. She looks at the child, forms a gun with her fingers, and ‘shoots’ him.

==Reception==
Critical reception for Preservation has been mixed to positive and holds a 70% score on Rotten Tomatoes.  Dread Central and Fangoria both praised the movie,  and Fangoria commented that it "largely succeeds as a swift and scary survival thriller."  In contrast, Slant Magazine and Film School Rejects both panned the movie overall,  with Film School Rejects writing "The cast does good work, the cinematography is attractive and even the score delivers, but while the direction is (mostly) fine Denham’s script throws it all away through a non-stop assault of horror movie 101 cliches and inanity." 

==References==
 

==External list==
*  
*  

 
 
 
 