This Freedom
 
{{Infobox film
| name           = This Freedom
| image          =
| caption        =
| director       = Denison Clift
| producer       =
| writer         = Denison Clift  A.S.M. Hutchinson (novel) John Stuart   Athene Seyler
| music          =
| cinematography =
| editing        =
| studio         = Ideal Film Company
| distributor    = Ideal Film Company
| released       = 21 March 1923
| runtime        =
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British silent silent drama John Stuart.  It was based on the novel This Freedom by A.S.M. Hutchinson. A woman gets married and starts a family. She chooses to continue working and neglects her children.

==Cast==
* Fay Compton – Rosalie Aubyn
* Clive Brook – Harry Occleve John Stuart – Huggo Occleve
* Athene Seyler – Miss Keggs
* Nancy Kenyon – Doda Occleve
* Gladys Hamer – Gertrude
* Fewlass Llewellyn – Reverend Aubyn
* Adeline Hayden Coffin – Mrs. Aubyn
* Mickey Brantford – Robert
* Bunty Fosse – Rosalie, as a Child
* Joan Maude – Hilda
* Charles Vane – Uncle Pyke
* Gladys Hamilton – Aunt Belle Robert English – Mr. Field

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 


 