Yaarige Saluthe Sambala
{{Infobox film name           = Yaarige Saluthe Sambala image          = image_size     = caption        = director       = M. S. Rajashekar producer       = B. G. Hemalatha writer         = V. Sekhar narrator       = starring  Urvashi Anu Prabhakar Mohan Shankar Umashree music          = Hamsalekha cinematography = Prasad Babu editing        = S. Manohar studio         = SDM Film released       =   runtime        = 152 minutes country        = India language  Kannada
|budget         =
}}
 Kannada comedy comedy - family drama film directed by M. S. Rajashekar and produced by B. G. Hemalatha. The film has an ensemble cast comprising Ananth Nag, Suhasini, Shashikumar, Urvashi (actress)|Urvashi, Mohan Shankar, Anu Prabhakar and Umashree in the lead roles. 
 Tamil blockbuster Kanaka in Telugu as Kshemanga Velli Laabanga Randi (2001) and in Hindi as Aamdani Atthanni Kharcha Rupaiya.

The film released in 2000 to generally positive reviews from critics who lauded the lead actors performance and the musical score by Hamsalekha.

==Cast==
* Ananth Nag 
* Shashikumar
* Suhasini Urvashi
* Anu Prabhakar 
* Mohan Shankar
* Karibasavaiah
* Umashree
* Umesh
* Kote Prabhakar
* Honnavalli Krishna

==Soundtrack==
The music of the film was composed and written by Hamsalekha. 

{{Infobox album  
| Name        = Yaarige Saluthe Sambala
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Akash Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Yaarige Saluthe Sambala
| lyrics1 	= Hamsalekha
| extra1        = Rajesh Krishnan, Hemanth, Ramesh Chandra
| length1       = 
| title2        = Deepadinda Deepa
| lyrics2 	= Hamsalekha
| extra2        = Manjula Gururaj, Archana Udupa, Nanditha, Rajesh Krishnan, Ramesh Chandra, Hemanth
| length2       = 
| title3        = Malela Femalaa
| lyrics3       = Hamsalekha
| extra3 	= Manjula Gururaj, Archana Udupa, Nanditha, Rajesh Krishnan, G. V. Athri, Hemanth
| length3       = 
| title4        = Shravana Veeneya
| extra4        = Rajesh Krishnan, Latha Hamsalekha
| lyrics4 	= Hamsalekha
| length4       = 
| title5        = Priya Priya Dehadalli
| extra5        = S. P. Balasubrahmanyam, Manjula Gururaj
| lyrics5       = Hamsalekha
| length5       = 
}}

==References==
 

==External source==
*  

 
 
 
 
 
 
 

 