Rodeo (film)
{{Infobox film
| name =  Rodeo
| image =
| image_size =
| caption =
| director = William Beaudine
| producer = Walter Mirisch 
| writer = Charles R. Marion 
| narrator = John Archer Gary Gray
| music = Marlin Skiles  
| cinematography = Harry Neumann  William Austin 
| studio = Monogram Pictures
| distributor = Monogram Pictures
| released = March 9, 1952
| runtime = 70 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} sports drama John Archer and Wallace Ford. The film was made in cinecolor. 

==Cast==
* Jane Nigh as Nancy Cartwright   John Archer as Slim Martin  
* Wallace Ford as Barbecue Jones   Gary Gray as Joey Cartwright  
* Frances Rafferty as Dixie Benson  
* Sara Haden as Agatha Cartwright 
* Frank Ferguson as Harry Cartwright 
* Myron Healey as Richard Durston  
* Fuzzy Knight as Jazbo Davis  
* Robert Karnes as Charles Olenick  
* Jim Bannon as Bat Gorman  
* I. Stanford Jolley as Pete Adkins  
*  Ann Doran as Mrs. Martha Durston  Russell Hicks as Allen H. Grandstead

==References==
 

==Bibliography==
* Marshall, Wendy L. William Beaudine: From Silents to Television. Scarecrow Press, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 
 