The Fat Boy Chronicles
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Fat Boy Chronicles
| image          = The Fat Boy Chronicles.jpg
| director       = Jason Winn
| producer       = Michael Buchanan Jason Winn
| writer         = Michael Buchanan Diane Lang Kelly Washington, Cole Carson, Chris Bert, Ron Lester
| music          = 
| cinematography = Jeremy Osbern
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 78 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} obese high-school student who deals with bullying and trying to lose weight. The film is inspired by a true story about an obese 9th grader in Cincinnati and a novel and film were released in 2010 as a major motion picture, and was later released on Netflix.   The movie received mixed reviews, with users on Rotten Tomatoes giving the movie a 49% "Rotten" rating. 

==Plot== Photoshopped picture of Jimmys sister as bait, because of this, Jimmy feels guilty. Paul talks Jimmy into sneaking out to a party. Later Pauls father commits suicide. Paul runs away and gets injured in an accident in Colorado. Jimmy tutors Robb Thurman, who is the football captain. Robb, because of this, tells his teammates not to harass Jimmy any more, as they become friends.

==Characters==
* Jimmy Winterpock (Christopher Rivera) - A normally depressed boy outside of home and church, he is the protagonist. As the main character of the book and film, Jimmy tries to lose weight.
* Paul Grove (Chris Bert) - A troubled teen with drug addict parents, he struggles to find meaning in his life. Kelly Washington) -  Jimmys love interest and later girlfriend, she has mental issues and cuts herself to heal her mental scars. high school quarterback for the team, as well as a bully who, after a few tutoring sessions with Jimmy, starts to protect him.
* Allen Winterpock (Bill Murphey) - Jimmys father who is also somewhat overweight, who runs with him during their jogs.
* Marianne Winterpock (Sylvia Castro Galan) - Jimmys mother.
* Dr. Jeffords (Ron Lester) - The doctor that examined Jimmy.
* Nate Hammer (Benjamin Davis) - A punk teen who has bullied Jimmy since middle school.

==References==
 

==External links==
*   
*  

 
 
 