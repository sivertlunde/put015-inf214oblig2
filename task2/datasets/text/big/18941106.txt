Park Plaza 605
{{Infobox film
| name           = Park Plaza 605
| caption        = U.S. theatrical poster
| image         = "Park_Plaza_605"(1953).jpg
| director       = Bernard Knowles
| producer       = Albert Fennell Bertram Ostrer
| writer         = Bertram Ostrer Albert Fennell Bernard Knowles  Clifford Witting (treatment)
| based on       = novel Dare-Devil Conquest by Berkeley Gray
| narrator       =
| starring       = Tom Conway Eva Bartok Joy Shelton Philip Green Eric Cross
| editing        =  Clifford Boote	(as Cifford Boot)
| studio         =  B & A Productions (as B & A Productions Limited)
| distributor    = Eros Films   (UK)
| released       = December 1953  (UK)
| runtime        = 75 minutes 
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
Park Plaza 605 is a 1953 British crime film. A B movie, it starred Tom Conway, Eva Bartok and Sid James.  It was based on the Norman Conquest series of novels by Berkeley Gray, the film sees a private investigator summoned to room 605 of the Park Plaza Hotel to meet a mysterious foreign blonde woman, and soon finds himself embroiled in a murder investigation. 

==Cast==
* Tom Conway - Norman Conquest
* Eva Bartok - Nadina Rodin
* Joy Shelton - Pixie Everard
* Sid James - Superintendent Williams
* Richard Wattis - Theodore Feather
* Carl Jaffe - Boris Roff
* Frederick Schiller - Ivan Burgin Robert Adair - Baron von Henschel
* Anton Diffring - Gregor Ian Fleming - Colonel Santling
* Edwin Richfield - Mr Reynolds Michael Balfour - Ted Birston
* Martin Boddey - Stumpy
* Terence Alexander - Hotel Manager
* Victor Platt - Taxi Driver
* Leon Davey - Mandeville Livingstone
* Richard Marner - Barkov
* Tony Hilton - Lift attendant
* Alan Rolfe - Police inspector
* Derek Prentice - Hall porter
* Frank Sieman - Captain Kramer
* Brian Moorehead - First mate
* Billie Hill - Mrs Pottle
* Anthony Woodruff - Clerk

==Critical reception==
Radio Times called it a "fair British B-feature."   

==References==
 

==External links==
* 

 

 
 
 
 
 


 
 