Davy Crockett, King of the Wild Frontier
{{Infobox film
| name           = Davy Crockett, King of the Wild Frontier
| caption        = 
| image          = Davy Crockett, King of the Wild Frontier FilmPoster.jpeg Norman Foster
| producer       = Walt Disney Tom Blackburn 
| narrator       = 
| starring       = Fess Parker Buddy Ebsen
| music          = Thomas W. Blackburn (lyrics) George Bruns Edward H. Plumb (orchestration) 
| cinematography = Charles P. Boyle
| editing        = Chester W. Schaeffer Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $2,150,000 (US)  
| preceded_by    = 
| followed_by    = 
}} Walt Disney Disney television Davy Crockett : 
* Davy Crockett Indian Fighter (first broadcast December 15, 1954)
* Davy Crockett Goes to Congress (first broadcast January 26, 1955)
* Davy Crockett at the Alamo (first broadcast February 23, 1955)
 Walt Disney Productions.

==Plot==

===Creek Indian Wars=== Creek forces through the swamps.
 tomahawk duel. the American peace treaty.

===Off to Congress===
Crockett and Russell leave home once again to scout virgin territory being opened for settlement. There the pair encounter a man named Big Foot who is running Charlie Two Shirts off their land and reselling it. After befriending a family of Creek refugees who have been forced from their homes, Crockett agrees to become magistrate for the area. Confronting Big Foot in hand to hand combat, Crockett eventually defeats his opponent and arrests him and his accomplices.

Not long after, Crockett receives a letter from his sister-in-law which relates that Polly, his wife, has died.

Crockett agrees to run for the state legislature when he learns of the unrivaled candidacy of Amos Thorpe, an unscrupulous politician in league with men trying to lay claim to Cherokee lands. Then at the request of Andrew Jackson, he runs for Congress. Handily elected, Crockett becomes a popular member of the House of Representatives and friend of Andrew Jackson, who has since become president of the United States.

Aware of Crocketts views of Native American rights, Jacksons underlings arrange for Crockett to take a speaking tour across the eastern part of the country during the introduction of a legislative bill to usurp Indian treaty lands. Hearing of the bill, Georgie Russell rides to Philadelphia to fetch Crockett. The pair arrive back in Washington, D.C. where Crockett makes an impassioned speech before the House of Representatives against the bill, aware that it will cost him his political career.

===The Alamo=== embattled Texans Santa Anna Mexican troops. After they successfully manage to scale the walls, the Mexicans overrun the Texans, killing all of them, including "Busted Luck" and Thimblerig, as well as Jim Bowie, who is sick in bed. Georgie is shot twice and is killed.  Davy, the last survivor, fights valiantly on, taking down several Mexicans, though it is obvious he will get killed eventually.

==Response==
Released as a result of the enormous success of the three television episodes, which were first shown on the Disney anthology television series, the film remains Walt Disneys most successful television film project. The Davy Crockett episodes of the early to mid-1950s sparked a national (and later transatlantic) "Davy Crockett craze", with many coonskin caps being sold, as well as a successful recording of the episodes theme song "The Ballad of Davy Crockett".

The success of the film prompted Disney to create a prequel entitled Davy Crockett and the River Pirates.
 Daniel Boone, another American frontiersman.

==Cast==
* Fess Parker as Davy Crockett
* Buddy Ebsen as George "Georgie" Russell
* William Bakewell as Major Tobias Norton
* Basil Ruysdael as General/President Andrew Jackson 
*  Pat Hogan as Chief Red Stick
* Mike Mazurki as Bigfoot Mason
* Hans Conried as Thimblerig William Travis 
*Helene Stanley as Polly Crockett
* Kenneth Tobey as Colonel Jim Bowie
*Campbell Brown as Bruno (Bigfoots Henchman)
*Jeff Thompson as Charlie Two Shirts
* Nick Cravat as Busted Luck 
*Jim Maddux as Congressman #1
*Robert Booth as Congressman #2
*Eugene Brindel as Billy Crockett
*Benjamin Hornbuckle as Henderson
*Henry Joyner as Swaney
*Ray Whitetree as Johnny Crockett
*Hal Youngblood as Opponent of political speaker

==Songs== The Wellingtons
*"Farewell to the Mountains" – Poem by Davy Crockett, music by George Bruns, sung by Fess Parker

==See also==
* Davy Crockett and the River Pirates
* The Ballad of Davy Crockett

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 