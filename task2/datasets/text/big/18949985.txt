The Buddy System (film)
{{Infobox film
| name= The Buddy System
| image= The Buddy System poster.jpg
| caption= Theatrical release poster by Tom Jung
| writer=Mary Agnes Donoghue
| starring = {{Plainlist|
* Richard Dreyfuss
* Susan Sarandon Nancy Allen
* Jean Stapleton
}}
| director=Glenn A. Jordan
| cinematography=Matthew F. Leonetti Patrick Williams Arthur Schmidt
| distributor=20th Century Fox
| released= 
| runtime=110 min.
| country=United States
| language=English
| budget=
| gross=$1,820,049 (domestic)
| producer=Alain Chammas
| followed_by=
}} Nancy Allen, Wil Wheaton and Jean Stapleton. The film was directed by Glenn A. Jordan who is better known for directing and producing numerous television films and television dramas. The film follows the story of a cautious single mother who forms an unlikely friendship with her sons school security guard. The Buddy System was Wil Wheatons first major film role and his second non-television role after the 1983 film Hambone and Hillie.

==Cast and characters==
* Richard Dreyfuss - Joe
* Susan Sarandon - Emily Nancy Allen - Carrie
* Jean Stapleton - Mrs. Price
* Wil Wheaton - Tim Edward Winter - Jim Parks
* Keene Curtis - Dr. Knitz
* Milton Selzer - Landlord
* F. William Parker - Lawyer

==Plot==
Emily Price is a single mother: she got pregnant in high school and was abandoned by the father before her son, Tim, was born. She and Tim live with her mother, who is both protective and disparaging, and tends to overlook her daughter in favor of her grandson. She is trying to become a court reporter, but freezes up every time she takes the test. Joe comes into their lives when he is sent out to do a residency check by the school: Emily and Tim have been lying about where they live so he can go to a better school. The price for Tim is loneliness: he cant tell anyone where he lives. Joe, who is an aspiring novelist and an inventor of gadgets, decides not to report them and strikes up an unlikely friendship with Tim that gradually escalates to include Emily as well. Previous romantic entanglements - for Emily, the withholding Jim; for Joe, the beautiful but self-absorbed Carrie - intervene while Emily gains courage and independence and Joe comes to understand where his real talents lie.

==See also==
* 1984 in film
* Cinema of the United States
* List of American films of 1984

== External links ==
 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 


 