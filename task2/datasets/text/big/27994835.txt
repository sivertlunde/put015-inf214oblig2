The Uninvited Guest (1924 film)
{{Infobox film
| title          = The Uninvited Guest
| image          =
| caption        =
| director       = Ralph Ince
| producer       = J. Ernest Williamson
| writer         = Curtis Benton
| narrator       = Maurice "Lefty" William Bailey Louis Wolheim
| music          =
| cinematography = J. O. Taylor
| editing        =
| distributor    = Metro Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States Silent (English intertitles)
| budget         =
| gross          =
}} 1924 American Maurice "Lefty" William Bailey, and Louis Wolheim. A print of the film exists in the Russian film archive Gosfilmofond.   at silentera.com 

==Cast==
* Maurice Bennett Flynn - Paul Gin Patterson
* Jean Tolley - Olive Granger
* Mary MacLaren - Irene Carlton William Bailey - Fred Morgan
* Louis Wolheim - Jan Boomer

==Production==
The film was shot partially in the Bahamas, and was released by Metro Pictures a few months before the merger that created Metro-Goldwyn.  The film had a sequence filmed in Technicolor. 

==See also==
*List of early color feature films

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 