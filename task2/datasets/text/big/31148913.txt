Sadaa Suhagan
 
{{Infobox film
| name           = Sadaa Suhagan
| image          = SadhaaSuhagan.png
| image_size     = 
| caption        = Promotional Poster
| director       = Rama Rao Tatineni
| producer       = Vijay Soorma, Rajeev Kumar
| writer         = 
| narrator       =  Govinda Utpal Dutt Aruna Irani
| music          = Laxmikant Pyarelal
| Lyricist       =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = 15 March 1986
| runtime        = 
| country        = India Hindi
| Adjusted Gross = 69.67 crores
}} Indian film. It focuses on the lives of a married couple, Laxmi (Rekha) and Rajshekhar (Jeetendra).

==Plot==
Laxmi is a caring and virtuous wife. Rajshekhar is an equally loving and faithful husband. He is an excellent provider and she plays the nurturers role to perfection. They have two sons and a daughter.

Rajshekhar was an orphan. He remained deprived of love until he married Laxmi. She came from a disturbed home with an alcoholic father and a scheming stepmother. When Rajshekhar and Laxmi got together, they created a stable, happy family.  
 Sheela David), with the son of their friends. However, Babli refuses, saying that her mother wasted her life as a homemaker and she would not like to do the same. 

The once-happy home is in a mess. Rajshekhar develops a heart problem and Laxmi takes him to the family doctor. While the doctor is checking Rajshekhar, Laxmi becomes unconscious for a short time. She is examined and the result is shocking: she has an advanced case of cancer, with only a few days remaining of her life. Rajshekhar requests the doctor not to inform her about this. He decides that his wifes last days should be as happy as possible. 

He finds Ravi, working as a petrol pump attendant and married to Madhu. Rajshekhar tells them about Laxmi’s illness and begs them to return home for her sake. They obey. Then he traces Shashi and brings him back. Babli changes her mind on her own, and decides to marry the son of her parents friends. Madhu takes over the running of the household. She and Ravi are expecting a child. Laxmi is satisfied that unity and harmony are prevailing in her family.  

Everyone celebrates the twenty-fifth wedding anniversary of Laxmi and Rajshekhar. Back in their room, Laxmi collapses in her husbands arms; the same moment, he has a heart attack. When the family members enter the room, they find Laxmi and Rajshekhar’s bodies, joined in death as in life.

==Cast==
*Jeetendra - Rajshekhar 
*Rekha - Laxmi  Govinda - Ravi, eldest son of Laxmi and Rajshekhar
*Utpal Dutt - Laxmis father
*Aruna Irani - Sujata, Laxmis friend 
*Shubha Khote - Laxmis stepmother 
*Anuradha Patel - Madhu, wife of Ravi
*Alankar Joshi- Shashi, younger son of Laxmi and Rajshekhar
*Sheela David - Babli, daughter of Laxmi and Rajshekhar
*Shafi Inamdar - Rajshekhars friend
*Mohan Choti - Domestic help of Rajshekhar and Laxmi

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Yeh Gussa Kaise Utrega"
| Mohammad Aziz, Asha Bhosle
|-
| 2
| "Kehta Hai Sindoor Tera"
| Kavita Krishnamurthy
|-
| 3
| "Hum Hain Naujawan"
| Vijay Benedict, Kavita Krishnamurthy
|-
| 4
| "Kehta Hai Sindoor Tera (Sad)"
| Kavita Krishnamurthy
|-
| 5
| "Billi Boli Meon"
| Suresh Wadkar, Anuradha Paudwal
|-
| 6
| "Kabhie Kabhie Main Sochoon"
| Kavita Krishnamurthy
|-
| 7
| "Kabhie Kabhie Main Sochoon (Sad)"
| Kavita Krishnamurthy
|}

==External links==
*  

 
 
 