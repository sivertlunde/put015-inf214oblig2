Monsters Wanted
{{Infobox film
| name           = Monsters Wanted
| image          = File:Monsters Wanted film poster.jpg
| alt            = 
| caption        = 
| film name      =  
| directors      = {{plainlist|
* Brian Cunningham
* Joe Laughrey
}}
| producers      = {{plainlist|
* Brian Cunningham
* Joe Laughrey
* Jeff Porter
* Kaley Roberts
}}
| writers        = {{plainlist|
* Brian Cunningham
* Joe Laughrey
}}
| starring       = {{plainlist|
* Richard Teachout
* Janel Nash
* Kenneth Schell
}}
| narrator       =  
| music          = 
| cinematography = 
| editing        = {{plainlist|
* Brian Cunningham
* Kaley Roberts
}}
| production companies = ThoughtFly Films
| distributor    =  
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
Monsters Wanted is a 2013 documentary film that was directed by Brian Cunningham and Joe Laughrey.    The film marks the directorial debut of Laughrey and is the first feature film length documentary by Cunningham. Monsters Wanted had its world premiere on July 27, 2013, at FandomFest in Louisville, Kentucky, and explores the haunted house industry in Louisville, Kentucky.  The film was also released on DVD and VOD August 27, 2013. 

==Plot==
The documentary follows Rich Teachout, a businessman that has quit his job in order to focus on cultivating a unique haunted attraction. Together with Janel Nash, the two will work on building and operating the Asylum Haunted Scream park during its first year of operation. The documentary also follows other haunted attractions in the Louisville area. 

==Reception==
Critical reception has been positive.   Mark L. Miller of Aint It Cool News wrote that the end seems like "a five minute filler section out of nowhere" but that he "loved almost every second of Monsters Wanted".  Culture Crypt gave the film a score of 75, writing, "The story is disjointed at times, and some of the issues are artificially inflated into problems that are resolved quickly. But the film creates a fun portrayal of how disparate personalities with a similar dream find commonality in their passion. And how commitment and community are the key ingredients that bring their unified vision together."  Mania.com gave a favorable review, writing, "You cant help but admire the dedication that the entire staff has in the face of all the problems they encounter and days which last 20 hours. Cunningham and Laughrey do a great job of letting the people tell the story as they stay back and just record the drama. For any horror or haunted house them park fan, this is a fascinating look inside the industry." 

==Accolades==
The film won "Best Documentary" at the 2013 Rhode Island International Horror Film Festival,  was an official selection at the 2013 Orlando Film Festival,  and was the opening night film at the 2014 Transworld Halloween and Attraction Show. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 