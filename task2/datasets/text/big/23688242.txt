Ravan Raaj: A True Story
{{Infobox film
 | name = Ravan Raaj: A True Story
 | image = RavanRaajfilm.jpg.jpg
 | caption = Promotional Poster
 | director = Rama Rao Tatineni
 | producer = Manoj Chaturvedi Bobby Anand
 | writer = Sanjeev Duggal
 | dialogue = Adesh K.Arjun
 | starring = Mithun Chakraborty Aditya Pancholi Shakti Kapoor Madhoo Harish Kumar Sheeba
 | music = Viju Shah
 | lyrics = Anand Bakshi
 | associate director = P.Vishwam
 | art director = D.Rathnavelu
 | choreographer = K.S.Raghuram, Chinni Prakash, Rekha Chinni Prakash, Bhoopi 
 | released = June 23, 1995
 | runtime = 153 min.
 | language = Hindi Rs 5 Crores
 | preceded_by = 
 | followed_by = 
}}
 1995 Hindi Indian feature directed by Rama Rao Tatineni|T.Rama Rao, starring Mithun Chakraborty, Madhoo, and Aditya Pancholi and is centred on kidney smugglers and a serial killer. The film is a remake of Tamil blockbuster Pulan Visaranai starring Vijayakanth.

==Plot summary==
Crime and corruption have taken over Bombay City without any solution. A rash of kidnappings of young women takes place. One auto-rickshaw driver Auto Kesariya is behind this crime. The policemen of Kala Chowki Police Station are doing little to stop these crimes. Former Assistant Commissioner of Police, Arjun Verma is reinstated and assigned to this case. Arjun gets himself deeply involved in this disappearance and the kidnappings and faces the shock and trauma of finding skeletal remains of young women who have been abducted as well as body parts stolen from innocent patients from hospitals. His personal life also turns upside down as his fiancée abandons him on his wedding day, while his niece is abducted and held for ransom, and above all Arjun Verma himself becomes the target of assassins hired by influential politicians and senior police officials.

==Cast==
*Mithun Chakraborty as ACP Arjun Verma
*Madhoo as Geeta / Suzie 
*Aditya Pancholi as Dr. Amir Khurana 
*Paresh Rawal as Minister Charandas 
*Harish Kumar as Harish Verma 
*Sheeba Akashdeep as Suzie Shilpa Before married
*Shakti Kapoor as Auto Kesariya 
*Johnny Lever as Chamelibahen
*Alok Nath as Police Commissioner
*Ishrat Ali as Devdas 
*Harish Patel as Shilpas dad 
*Ghanshyam Rohera as Eunuch 
*Tej Sapru as Police Inspector Madanlal

==Soundtrack==
{{Infobox album|  
  Name        = Ravan Raaj: A True Story
|  Type        = Soundtrack
|  Artist      = Viju Shah
|  Cover       =
|  Released    = 1995
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       =
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = 
|  Next album  = 
}} romance in Poornima with lyrics by the eminent lyricist Anand Bakshi.

*"Husnwalo Se Ye Dil Bachaye" (Udit Narayan)    
*"O Sanam O Sanam" (Kumar Sanu, Sadhana Sargam)
*"Aaina Aaina Tere Bin Chaina" (Kumar Sanu, Jayshree F,Shivram)     
*"Ooh!Ooh!Ooh...Oah!" (Bali Brahmabhatt, Sushma Shrestha|Poornima)
*"Tu Cheez Badi Hai Sakht Sakht" (Bali Brahmabhatt, Johny Lever, Sapna Mukherjee)
*"Yaar Mera Mausam Hai Mastana Mastana" (Abhijeet Bhattacharya, Kavita Krishnamurthy)

==External References==
 
* 
* http://www.ibosnetwork.com/asp/filmbodetails.asp?id=Raavan+Raaj
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Raavan+Raaj

==External links==
*  

 
 
 
 
 
 
 
 