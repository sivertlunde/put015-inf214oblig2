Cats & Dogs
 
 Cats and Dogs}}
{{Infobox film
| name           = Cats and Dogs
| image          = Cats & Dogs film.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lawrence Guterman
| producer       = Ed Jones Christopher de Faria Andrew Lazar Craig Perry Warren Zide
| writer         = John Requa Glenn Ficarra
| starring       = Jeff Goldblum Elizabeth Perkins Alexander Pollock 
| music          = John Debney
| cinematography = Julio Macat
| editing        = Rick W. Finney Michael A. Stevenson
| studio         = Village Roadshow Pictures Mad Chance Zide/Perry Productions Rhythm and Hues Studios
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States Australia English
| budget         = $60 million   
| gross          = $200,687,492 
}} relationships between Victoria and Vancouver, Canada. The film was released on July 4, 2001 by Warner Bros. Pictures, Village Roadshow Pictures, Zide/Perry Productions, and Rhythm and Hues Studios.

==Plot==

Mrs. Carolyn Brody (Elizabeth Perkins) and her son Scott (Alexander Pollock)  return home and the familys Bloodhound Buddy starts chasing a cat, a chase which ends with Buddys capture by other cats in an ambush.

This is witnessed by  .

Meanwhile at a farm, a handful of Beagle puppies make fun of a younger puppy (voiced by Tobey Maguire), who wants to be free. The younger puppy tries an escape, but he fails. A group of young black Doberman puppies, all of them trained dog agents, led by a large Doberman Pinscher appear, they round up the Beagle puppies and force them into hiding underground. Mrs Brody comes into the puppy pen and she selects the younger puppy who was forgotten by the Dobermanns. She decides to adopt him and takes him home, naming him Lou after Scott sarcastically suggests the name "Loser".

Lou goes near an explosive trap set by the cats but Butch comes out and detonates it harmlessly. Butch then shows Lou the network that dog agents use, then takes him to meet some more agents: Peek (voiced by Joe Pantoliano) is a Chinese Crested Dog who works in an underground tube and has surveillance and communications equipmen and Sam (voiced by Michael Clarke Duncan) who is an Old English Sheepdog. Having mistaken Lou for a trained dog agent, Butch raises his concerns to his superiors, but is told that there is no time to send a replacement. Lou is then briefed on the origins of the war between cats and dogs, which apparently dates all the way back to Ancient Egypt. Butch also mentions that fate of Buddy, who meanwhile had managed to escape from the cats and was now retired from the spy business. 
 Sean Hayes), Persian cat, plans to conquer the world by making all humans allergic to dogs with Mr. Brody (Jeff Goldblum)s research on a cure for dog allergies. He is briefly interrupted by Sophie the Maid (Miriam Margoyles) who needs to dress him upon seeing the comatose Mr. Mason (Myron Natwick). Tinkles then tells his sidekick Calico (voiced by Jon Lovitz), an Exotic Shorthair, to send in the ninja cats (voiced by Danny Mann and Billy West) he hired to steal the research. He sends in the Devon Rex  ninjas but Lou manages to prevent the theft. Lou then meets a former agent and Butchs ex-sweetheart and girlfriend named Ivy (voiced by Susan Sarandon), a Saluki who belly scratches him. Disappointed that the ninja cats failed him, Mr. Tinkles then orders Calico to send a Russian Blue mercenary, Diemitir Kennelkoff, (voiced by Glenn Ficarra) to steal the research. Kennelkoff frames Lou for defecating in the house with a ball containing fake dog crap, then places a bomb on the lab door. Butch and Lou manage to get into the house through the window, but Peek and Sam are trapped outside after Lou knocks the window prop away. Kennelkoff fires a series of boomerangs around the Brodys house which break several vases and knock a lamp over; the boomerangs also make the curtain pole break. Lou then distracts the Russian while Butch tries to disable the bomb, but Kennelkoff turns his attention on Butch and tries to kill him. Butch gets caught in a telephone wire, then Mrs Brody comes home looking for her cell phone and surprisingly does not notice her wrecked living room. Butch manages to escape and disables the bomb, but Kennelkoff holds out a remote that will detonate the bomb and laughs until the lab door opens and hits him. Kennelkoff is then captured and interrogated. The agent tells the gang that they pumped a few things out of Kennelkoffs stomach, including a note written by Mr. Tinkles.

After an incident involving Lou playing with Scott, Professor Brodys machine finally gets the positive combinations for the formula. As Mr. Tinkles and Calico overhear the call between Professor Brody and a doctor, they decide to spring a trap for Dr. Brody and his family. First, Mr. Tinkles makes his sinister and talking side known to Sophie, causing her to faint, then he and his cats take Mr Masons comatose body to Masons tree flocking plant, where Tinkles passes his voice off as Mr Mason to send the employees home and commandeer the factory for the next plot. The cats send soccer tickets to an exhibition game between Uruguay and Chad to the Brody family, obviously a ruse. The cats make a fake entry and when the Brody family pulls up, the cats throw a gas bomb into their car, which goes off and leaves the family unconscious.
 mice to spread the now mass-produced allergy to dogs. While Butch, Ivy, Peek, and Sam fight Tinkless cat forces, Lou frees the Brodys and Calico (who was betrayed by Tinkles), revealing he can speak in the process. Lou defeats Tinkles and rescues Butch, but the claw of the excavator hits his head and a flocking tank, causing an explosion that destroys the factory. Butch manages to save Lou, but the latter is unresponsive. After a few hurtful moments of sadness and sorrow; in which Scott, feeling genuinely sorry and remorseful for referring to Lou as loser, tearfully apologizes to Lou; Lou suddenly awakens and all rejoice. Lou decides to be a normal canine and not a secret agent yet, but one day he will be when he is a full-grown beagle.

Meanwhile, Mr. Tinkles is sent to live with Sophie and her four sisters, who dress him in hilariously ridiculous girly outfits.

==Cast==

* Jeff Goldblum as Professor Brody
* Elizabeth Perkins as Mrs. Caroline Brody
* Alexander Pollock as Scott Brody
* Miriam Margoyles as Sophie the Maid
* Myron Natwick as Mr. Mason

===Voice cast===

* Tobey Maguire as Lou (Louis) the Beagle, who lives with the Brody family Sean Hayes as Mr. Tinkles, leader of the cat agents who wants to defeat the dogs and rule the world
* Alec Baldwin as Butch, an experienced field agent and Lous mentor
* Susan Sarandon as Ivy, a "domestically challenged" former dog agent Lou meets when he trains to be an agent, who has a history with Butch
* Charlton Heston as The Mastiff, the leader of the dog agents
* Jon Lovitz as Calico, Mr. Tinkles second in command
* Joe Pantoliano as Peek, a dog agent
* Michael Clarke Duncan as Sam, a dog agent
* Billy West as Ninja Cat #1
* Danny Mann as Ninja Cat #2
* Glenn Ficarra as Diemitir Kennelkoff / The Russian, a Russian blue agent sent to set off an explosive device at Professor Brodys lab
* Paul Pape as Wolf Blitzer of CNN
* Richard Steven Horvitz as Puppy at Barn (uncredited)

==Puppeteers==

* Adam Behr
* Kevin Carlson
* Alejandro Diaz
* Randi Kaplan
* John Kennedy
* Luke Khanlian
* Bruce Lanoil
* Drew Massey Gordon Robertson
* Michelan Sisti
* Allan Trautman

==Filming locations==

The house, was filmed at an actual house: 1661 45th street Bikeway, Vancouver, British Columbia. The basement lab was the only part of the house that was filmed at  . Mr. Masons house was filmed at Craigdarroch Castle, in Victoria, British Columbia.

==Release==
 Looney Tune short "Chow Hound." 

===Critical reception===

Cats & Dogs received mixed reviews from movie critics. It has a 54% approval rating at Rotten Tomatoes, based upon 115 reviews, with an average rating of 5.5 out of 10.  Metacritic, which uses an aggregate rating system, collected an average score of 47/100, based upon 26 reviews.  The Washington Posts Jane Horwitz gave the film a positive review, calling it "  surprisingly witty and sophisticated spy movie spoof that will tickle adult pet lovers and still capture kids 6 and older with its boy-and-his-dog love story and pet slapstick."  In contrast, Kevin Turan of the Los Angeles Times wrote "Irritating, childish and more frantic than funny, Cats & Dogs does manage some few pleasant moments, but they are not worth waiting for."

===Box office===
 Independence Day The Score. The film grossed $93 million domestically, and $107 million overseas for a total of $200 million worldwide on a $60 million budget.

===Awards and nominations===
 ASCAP Award The Princess Diaries and Spy Kids.
 2001 Razzie Award for Worst Supporting Actor for his involvement in three films that year, including his role as The Mastiff.

===Home media release===

Cats & Dogs was released on VHS and DVD on October 16, 2001.

==Rating==

Cats & Dogs was rated PG in the United States, Australia, United Kingdom, New Zealand and Canada.

==Sequel==

  Sean Hayes reprise their roles as Sam, Peek, and Mr. Tinkles, while Nick Nolte and Wallace Shawn replace Alec Baldwin and Jon Lovitz as Butch and Calico; and Charlton Heston who voiced The Mastiff from the first film, died from pneumonia in April 2008. In this film, Lou is now an adult and is voiced by Neil Patrick Harris. James Marsden, Christina Applegate, and Bette Midler voice new characters named Diggs, Catherine, and Kitty Galore. The sequel was mainly negatively reviewed.

==References==

 

==External links==

 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 