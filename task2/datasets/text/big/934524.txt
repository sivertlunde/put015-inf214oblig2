Taegukgi (film)
 
{{Infobox film
| name           = Taegukgi
| image          = Taegukgi film poster.jpg
| caption        = Theatrical poster
| film name =    
| hanja          =   휘날리며
| rr             = Taegeukgi Hwinallimyeo
| mr             = Taegŭkki Hwinallimyŏ}}
| director       = Kang Je-gyu
| producer       = Lee Seong-hun
| writer         = Kang Je-gyu Han Ji-hun Kim Sang-don
| starring       = Jang Dong-gun Won Bin
| music          = Lee Dong-jun
| cinematography = Hong Kyung-Pyo
| editing        = Kyeong-hie Choi
| distributor    = Samuel Goldwyn Films
| released       =  
| runtime        = 148 minutes
| country        = South Korea
| language       = Korean
| budget         = United States dollar|US$12.8 million
| gross          = $69,827,583 
}}
Taegukgi: The Brotherhood of War ( ; Taegukgi Hwinallimyo) is a 2004 South Korean war film directed by Kang Je-gyu. It stars Jang Dong-gun and Won Bin and tells the story of two brothers who are drafted in the South Korean army by force at the outbreak of the Korean War. 

Kang Je-gyu made a name for himself directing Shiri (film)|Shiri and was able to attract top talent and capital to his new project, eventually spending USD $12.8 million on production. The film became one of the biggest successes in the South Korean film history up to that time, attracting 11.74 million people to the theatre, beating the previous record holder Silmido (film)|Silmido.

==Plot==
In 2003, while digging up remains at a Korean War battlefield to set up a memorial site, a South Korean Army excavation team notifies an elderly man that they identified some remains as his own.  
 shoeshine stand 1st Infantry Division, fighting at the Pusan Perimeter and then advancing north. Jin-tae is told by his commanding officer that if he can earn the highest award for a South Korean soldier, the Taeguk Cordon of the Order of Military Merit, his brother can be sent home. Jin-tae willingly volunteers for many dangerous missions and is quickly promoted to the rank of Chungsa (Sergeant). His heroic feats during the urban battle of Pyongyang finally makes Jin-tae nominated for the medal. His combat experiences quickly brutalize him into a cold-blood ruthless killer, which worries his horrified younger brother.
 UN coalition Communist Party under the Northern occupation. During the struggle, Young-shin is shot dead and the brothers are arrested for trying to rescue her. In the jail, Jin-taes  request to release his brother is refused and the security commander orders the prison to be set on fire when the enemy forces approach. Trying to rescue his brother, Jin-tae loses consciousness and wakes up to believe Jin-seok died in the fire. He brutally murders the surrendering prison commander just before he is restrained by Chinese soldiers.
 38th parallel, but is denied permission to fight. Jin-seok escapes and runs to the North Korean side, surrendering and claiming that he is Jin-taes brother; Jin-tae is now the fanatical leader of an elite North Korean formation known as the "Flag Unit", but the North Koreans think he is a spy. The position is attacked by American and South Korean forces. After being freed, Jin-seok makes his way through the chaos in search of his brother when the "Flag Unit" arrives to reinforce the North Koreans.

Not recognizing his own brother, the crazed Jin-tae desperately attempts to kill Jin-seok, but is wounded at the last moment. Jin-seok attempts to carry him away but is shot in the leg. Jin-tae then finally is brought back to his senses recognizes his brother, but their reunion is short lived because North Koreans are closing on them. Jin-Tae tells his brother to leave, and Jin-seok initially refuses, but goes after Jin-Tae promises that he will meet him back at home. As the wounded Jin-seok limps away to safety, Jin-tae mans a machine gun and begins firing at North Koreans, covering for his brother before being finally gunned down in a hail of fire.

In 2003, the elderly man stands at the excavation site and it is revealed he is in fact Jin-seok and the remains discovered there belong to Jin-tae. He examines the excavated items, including the long lost silver pen, and begs his brothers skeletal remains to speak to him, quoting his promises as his granddaughter looks on with sympathy. The film then returns to the past, the 1950s, in the aftermath of the Korean War. Jin-seok returns to his mother, sees the shoes his brother finished making, and heads off with Young-shins younger siblings in a now-peaceful Seoul. He reassures them that he will return to school, thereby fulfilling the promise he made to Jin-tae.

== Cast ==
*Jang Dong-gun as Lee Jin-tae		 	
*Won Bin as Lee Jin-seok, Jin-taes younger brother
*Lee Eun-ju as Kim Young-shin, Jin-taes fiance
*Choi Min-sik as North Korean commander
*Gong Hyung-jin as Yong-man
*Ahn Gil-kang as Sergeant Heo
*Jang Min-ho as old Lee Jin-seok 
*Jeon Jae-hyeong as Yong-seok
*Jo Yoon-hee as Lee Jin-seoks granddaughter
*Kim Su-ro as Anti-Communist Federation member
*Joo Da-young as Young-ja

==Production==
===Title===
The films title is the name of the pre-war flag of the Peoples Republic of Korea, the flag of the Provisional Peoples Committee for North Korea as well as the current flag of South Korea, featuring the Taegeuk symbol. It was released in the United Kingdom as Brotherhood: Taegukgi and the United States as Tae Guk Gi: The Brotherhood of War.

===Soundtrack===
{{listen
| filename=Taegukgi theme.ogg
| title=Prologue (main theme)
| description=
| format=Ogg}}
 chamber ensemble arrangement of the main theme.    The "haunting" main themes lyricism,  present throughout several of the tracks, was compared favorably to music of film score composers Ennio Morricone and John Williams.  Although it was received generally positively,  one critic argued that the film was tragic enough already, and needed "a more subtle soundtrack."   
== Reception ==
At the 50th Asia Pacific Film Festival, Taegukgi won the "Best Film" award, while Kang Je-gyu was awarded the "Best Director".  It was one of four Korean movies screened at the 2006 International Fajr Film Festival in Iran.  At the 2004 Grand Bell Awards, the main awards for film in South Korea, Taegukgi won three technical awards, for art direction, cinematography and sound effects. 

According to the numbers at Box Office Mojo, Taegukgi earned 64.8 million in South Korea, $1.1 million in the United States playing in limited release and $68.7 million overall worldwide, to finish as the 75th highest grossing film in the world in 2004. 

In addition to its record-breaking reception in South Korea, the film has also achieved positive responses abroad. Taegukgi currently  holds a fresh rating of 80 percent at Rotten Tomatoes.  Most positive reviews cite its unflinching portrayal of war and praise it for showing the brutality of both the North and South Korean armies.  The film is also recommended by the War Nerd Gary Brecher. 

===Awards and nominations ===
 
{| class="wikitable"
!  Year 
! Award
! Category 
! Nominated work 
! Result 
|- 2004
| rowspan=9| Blue Dragon Film Awards  || Best Film || Taegukgi||  
|- Kang Je-gyu ||  
|-
|  Best Actor || Jang Dong-gun||  
|-
| Best Supporting Actor || Gong Hyung-jin||  
|- Hong Kyung-pyo  ||   
|-
| Best Music || Lee Dong-jun  ||   
|- Shin Bo-kyeong  ||   
|-
| Best Visual Effects || ||   
|- Best Screenplay || Kang Je-gyu, Han Ji-hun, Kim Sang-don   ||  
|-
| rowspan=3| Grand Bell Awards || Best Cinematography || Hong Kyung-pyo ||  
|-
| Best Art Direction || Shin Bo-kyeong  ||  
|- Lee Tae-kyu, Kim Suk-won  ||  
|- Baeksang Arts Awards || Best Film || ||  
|- 2005
| rowspan=2| Asia Pacific Film Festival  || Best Film || Taegukgi||   
|- Kang Je-gyu ||  
|}

== See also ==
*Cinema of Korea
*List of films set in or about North Korea
*List of historical drama films of Asia
*Goyang Geumjeong Cave Massacre
*Namyangju Massacre

==Notes==
 

==References==
 
 
==External links==
*  
*  
*  
*  
*  
*  
*   at koreanfilm.org
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 