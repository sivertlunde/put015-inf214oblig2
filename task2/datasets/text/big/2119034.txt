Kandahar (2001 film)
{{Infobox film
| name = Kandahar
| image = Kandahar (2001 film).jpg
| caption =
| director = Mohsen Makhmalbaf
| producer = Mohsen Makhmalbaf
| writer = Mohsen Makhmalbaf
| starring = Nelofer Pazira Sadou Teymouri Hoyatala Hakimi Dawud Salahuddin (Hassan Tantai)
| music = Mohammad Reza Darvishi
| cinematography = Ebrahim Ghafori
| editing = Mohsen Makhmalbaf
| distributor = Avatar Films
| released =  
| runtime = 85 minutes
| country = Iran Persian English Pashto Polish Polish
| budget =
}}

Kandahar ( , set in Afghanistan during the rule of the Taliban. Its original Persian title is Safar-e Ghandehar, which means "Journey to Kandahar", and it is alternatively known as The Sun Behind the Moon. The film is based on a partly true, partly fictionalized story of a successful Afghan-Canadian, played by Nelofer Pazira, who returns to Afghanistan after receiving a letter from her sister, who was left behind when the family escaped, that she plans on committing suicide on the last solar eclipse of the millennium.

Kandahar was filmed mostly in Iran, including at the Niatak refugee camp,  but also secretly in Afghanistan itself.  Most people, including Nelofer Pazira, played themselves. The film premiered at the 2001 Cannes Film Festival,    but did not get much attention at first. After 9/11, however, it was widely shown. Kandahar won Makhmalbaf the Federico Fellini Prize from UNESCO in 2001.

== Plot ==
Hidden behind a burqa, Nafas, the sister from Canada, makes her way across the border with a family of Afghan refugees|refugees. When they are robbed by brigands and the family turns back, she decides to continue on her way, accompanied first by a young boy who was just expelled from a Quranic school, and then by an African American convert to Islam, who has become disillusioned with the turn the country has taken under the Taliban.

As the film proceeds, Nafas learns more and more about the hardships women face under the Taliban, and even more so, how years of war have destroyed Afghan society. Her African American guide, hidden behind a false beard, points out to her that the only technological progress allowed in the country is weaponry. As they wander the countryside, Nafas records her impressions into a portable tape recorder hidden beneath her veils. She sees children robbing corpses to survive, people fighting over artificial limbs that they might need in case they walk through a land mine|minefield, and doctors who examine female patients from behind a curtain with a hole in it.

When her African American guide turns back, because he is afraid to enter the city of Kandahar, she follows a guide who had just scammed a pair of artificial legs out of the Red Cross. Dressed in burqas, the pair join a wedding party which is stopped by the Taliban because they are playing musical instruments and singing—forbidden by Afghan law. Her guide is unveiled and taken away. Nafas is cleared by the Taliban patrol to continue, along with other members of the wedding party. In the end, Nafas is within sight of Kandahar at sunset, but she is now a prisoner of the veil.

== Dawud Salahuddin ==
The film stars Dawud Salahuddin (credited in the film as Hassan Tantai), an American-born convert to Islam who in 1980 assassinated an Iranian dissident and ex-diplomat at the behest of the newly formed Islamic Republic of Irans intelligence authorities.  Makhmalbaf stated that Salahuddin "is also a victim - a victim of the ideal he believed in. His humanity, when he opened fire against his ideological enemy, was martyred by his idealism." 

==Cast==
* Nelofer Pazira as Nafas
* Hassan Tantai as Tabib Sahid
* Sadou Teymouri as Khak
* Hoyatala Hakimi as Hayat
* Ike Ogut as Naghadar

==References==
 

==External links==
*  

 

 
 
 
 
 
 