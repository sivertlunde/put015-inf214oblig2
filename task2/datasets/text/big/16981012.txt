Dark Side Romance
{{Infobox film
| name           = Dark Side Romance
| image          = Darksideromance.jpg
| caption        = The Thai theatrical poster.
| director       = Prachya Pinkaew
| producer       = Adirek Wattaleela
| writer         = Dulyasit Niyomkul Nattiya Sirakornwilai
| narrator       = 
| starring       = Tat Na Takuatung Kullasatree Siripongpreeda
| music          = Jingle Bell
| cinematography = Somchai Kittikul
| editing        = Duangsamorn Luksanakoset
| distributor    = R.S. Film
| released       = September 29, 1995 
| runtime        = 112 minutes
| country        = Thailand
| language       = Thai
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Thai romantic romantic fantasy film directed by Prachya Pinkaew.

==Plot==
After a car accident, the spirits of Tan (Tat Na Takuatung and Peang (Kullasatree Siripongpreeda) become trapped in a dangerous limbo, awaiting the chance to be reborn.

==Cast==
*Tat Na Takuatung as Tan
*Kullasatree Siripongpreeda as Peang
*Chokchai Charoensuk

==External links==
* 
* 

 

 
 
 
 
 
 


 
 