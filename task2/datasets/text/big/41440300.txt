Memoirs of a Sinner
 
{{Infobox film
| name = Memoirs of a Sinner
| image = Memoirs of a Sinner.jpg
| caption = 
| director = Wojciech Jerzy Has	
| producer = 
| writer = Michal Komar
| based on =  
| starring = Piotr Bajor Maciej Kozlowski Janusz Michalowski Hanna Stankówna Ewa Wiśniewska
| music = Jerzy Maksymiuk
| cinematography = Grzegorz Kedzierski
| editing = Barbara Lewandowska-Conio and Wanda Zeman 
| studio = 
| distributor =  Zespol Filmowy "Rondo"
| released =  
| runtime = 114 minutes 
| country = Poland
| language = Polish
| budget = 
}}
Memoirs of a Sinner ( ) is a 1986 Polish film directed by Wojciech Jerzy Has, starring Piotr Bajor. The film is an adaptation of a novel by James Hogg and tells the tale of the protagonist Robert and his doppelganger.

==Plot==
Robert (Piotr Bajor) is exhumed from the grave by a gang of grave robbers and is forced to recount his lifestory - a struggle between good and evil, embodied in his doppelganger whom he eventually kills. 

==Cast==
* Piotr Bajor as Robert
* Maciej Kozlowski as Stranger
* Janusz Michalowski as Pastor
* Hanna Stankówna as Rabina
* Ewa Wiśniewska as Laura
* Franciszek Pieczka as Logan
* Anna Dymna as Dominika
* Katarzyna Figura as Cyntia
* Jan Jankowski as Gustaw
* Jan Pawel Kruk as Guard
* Andrzej Krukowski
* Zdzislaw Kuzniar 		
* Zofia Merle	
* Jerzy Zygmunt Nowak as Samuel
* Elwira Romanczuk as Malgorzata

==Production==
The film was filmed in the village of Klęk in the Polish province of Łódź Voivodeship|Łódź. 

==Release==
The film was released on 20 October 1986. Jerzy Maksymiuks score won the award for Best Score at the 1986 Polish Film Festival.

==The critical perspective==
Has maintains the strangeness central to the novel, although he tends to focus on the creation of unease, intrique and beautiful images rather than Hoggs satire on Calvinist predestination. 

==See also==
* Cinema of Poland
* List of Polish language films

==References==
 

==External links==
*  

 

 
 
 
 
 