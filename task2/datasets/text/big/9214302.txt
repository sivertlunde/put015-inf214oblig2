Martín (hache)
{{Infobox film
| name           = Martín (hache)
| image          =
| caption        = Poster of the film
| director       = Adolfo Aristarain
| producer       = Executive Producer:   Gerardo Herrero Fito Páez
| writer         = Adolfo Aristarain Kathy Saavedra
| narrator       = 
| starring       = Federico Luppi Juan Diego Botto  Cecilia Roth Eusebio Poncela
| music          = Fito Páez
| cinematography = Porfirio Enríquez
| editing        = Fernando Pardo
| distributor    = Spain:  
| released       = April 17, 1997 (Argentina)
| runtime        = 134 minutes
| country        = Argentina Spain
| language       = Spanish
| budget         = 200 millions Pesetas.
| preceded_by    = 
| followed_by    = 
}} Spanish and Argentine film directed by Adolfo Aristarain and starring Federico Luppi, Juan Diego Botto, Cecilia Roth and Eusebio Poncela.

It was nominated for four Goya Awards in 1998, and Cecilia Roth won one for lead actress.

It was filmed in Buenos Aires, Argentina; Madrid, and Almería, Spain.

==Plot summary==

Martín, known as Hache, is a 19-year-old Argentinian boy who after his girlfriend leaves him has a nearly fatal drug overdose, thought by many to be an attempted suicide. Afterwards, his mother sends him to Madrid to live with his father, Martin.

Martin, a successful film-maker, doesnt want to take care of his son because he likes living alone and being able to socialize with his two friends, Alicia and Dante, without influencing his son in any negative way since both Alicia and Dante are experienced drug users. Regardless, he brings him into his home, hoping to ward off any evil influences that might cause his son to have a relapse and commit suicide.

As time passes and Martins friendships get more strained, it becomes clear that Hache needs to leave his fathers place and make a name for himself.

==Background==
The title makes reference to the name of the son of Martín, portrayed by Juan Diego Botto, who is named after his father but with an "h" in brackets, which means "hijo" ("hijo" means "son" in Spanish, and "hache" is the Spanish name for the letter "h"). So nearly everybody calls him "Hache." It is similar to calling a son named for his father "Junior" in English. The movies English subtitles translate "Hache" as "Jay," since "jay" is the English name for the first letter in "junior" and is also a common English given name.

==External links==
*  .

 

 
 
 
 
 
 
 
 
 


 
 