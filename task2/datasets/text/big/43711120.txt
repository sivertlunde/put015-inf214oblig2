Baga Beach (film)
{{Infobox film
| name           = Baga Beach
| image          = 
| alt            = 
| caption        = 
| director       = Laxmikant Shetgaonkar
| producer       = Pramod Salgaocar
| writer         = Laxmikant Shetgaonkar
| starring       =    Akash Sinha
| music          = Sidhanath Buyao Emiliana da Cruz
| cinematography = Arup Mandal
| editing        = Sankalp Meshram
| studio         =  Sharvani Productions
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = India
| language       = Konkani/English/Hindi
| budget         =  
| gross          =  
}}
Baga Beach is 2013 Konkani film written and directed by Laxmikant Shetgaonkar. It followed his critically acclaimed, Paltadacho Munis (2009). 

Set in the eponymous Baga Beach in Goa, it was made at a budget of Rs. 3 crore, making it the most expensive film yet of Konkani cinema.  The film deals with the underbelly of tourism in Goa, with issues of paedophilia and child sexual abuse, conflict between locals and immigrants leading to hostility towards migrant labours, impact of holidaying foreigners on local culture.    The film had an Indian, German and French cast, including Bengali actress Paoli Dam, who spoke Hindi in the film. {{cite web | title = Paoli Dam to act in Shetgaonkars Baga beach | url = http://timesofindia.indiatimes.com/city/goa/Paoli-Dam-to-act-in-Shetgaonkars-Baga-beach/articleshow/17422562.cms?referral=PM 
|date=Nov 30, 2012| accessdate = 2014-09-01 | publisher = The Times of India}}   It was shot in Goa with an handheld camera in sync sound. 
 Best Feature Film in Konkani award at the 61st National Film Awards.  After the awards, the Goa Legislative Assembly passed a resolution congratulating producer and director for making the film about "a topic which needed much attention", and called it an ""eye opener on the issues related to children on our beaches."   The film was commercially released in Goa on May 30, 2014, but was not commercially released nationwide.    
{{cite web | title = National Award does not make any difference to me: Laxmikant Shetgaonkar | work=The Indian Express 
| url = http://indianexpress.com/article/entertainment/bollywood/national-award-does-not-make-any-difference-to-me-laxmikant-shetgaonkar/ |author=Press Trust of India|date= May 28, 2014| accessdate = 2014-09-01 
 }}
 

==Cast==
* Paoli Dam as  Sobha
* Cedric Cirotteau as Martin
* Sadiya Siddiqui as Maggie
*   as  Schroeder
* Akash Sinha as Devappa
* Harsh Mainra as  Shop keeper
* Prashanti Tallpankar as Celestine
* Reshma Jadhav as News Reporter
* Rajesh Karekar as Jerroviar
* Ivon C. de Souza as  Brendan
* Pradeep Naik as Victor
* Serjee Kleem as  Yuri

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 