Glitch!
{{Infobox film
| name           = Glitch!
| image          =
| image_size     =
| caption        =
| director       = Nico Mastorakis
| producer       = Perry Husman Isabelle Mastorakis Nico Mastorakis
| writer         = Nico Mastorakis
| narrator       =
| starring       = Will Egan Steve Donmyer Julia Nickson Dick Gautier Ted Lange
| music          = Thomas Marolda
| cinematography = Peter C. Jensen
| editing        = Nico Mastorakis George Rosenberg
| distributor    = Omega Pictures Academy Entertainment
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Glitch! is a 1988 American comedy film directed by Nico Mastorakis.  It involves two petty thieves who accidentally become casting directors of a film with a large number of beautiful girls, and later they must dodge the Mafia. 

==Plot==
Two thieves rob a large fancy house when the owner is away. But when a visitor mistakes them for the owner, and they find out about a casting party mis-scheduled for that day, they decide to stick around for fun. Theres only one small problem. The real owners owe some bad dudes a lot of money, and they show up to collect it.

==Cast== 
* Will Egan as T.C.
* Steve Donmyer as Bo
* Julia Nickson as Michelle Wong
* Ji-Tu Cumbuka as Mookie
* Fernando Garzon as Paco Galino
* John Kreng as Lee
* Richard Gautier as Julius Lazar
* Ted Lange as Dubois
* Amy Lyndon as Missy
* Dallas Cole as Faye

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 


 