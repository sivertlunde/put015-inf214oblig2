The Way I Spent the End of the World
{{Infobox film
| name           = Cum mi-am petrecut sfârşitul lumii
| image          = Sfarsitul lumii.jpg
| caption        = Film poster showing Dorotheea Petre as Eva and Timotei Duma as her brother Lilu
| director       = Cătălin Mitulescu
| producer       = Cătălin Mitulescu, In-Ah Lee, Philippe Martin, Daniel Mitulescu, Andrew J.Schorr, David Thion
| eproducer      = 
| aproducer      = 
| writer         = Cătălin Mitulescu and Andreea Valean cast
| Alexandru Bălănescu
| cinematography = Marius Panduru
| editing        = Cristina Ionescu
| distributor    = Metropolis Film (Romania), Pyramide Distribution (International) 2006
| runtime        = 110 minutes
| country        = Romania Romanian
| budget         = 
}} Romanian director Cătălin Mitulescu. It was released on September 15, 2006.

==Synopsis==
The film is about 17 year old Eva and 7 year old Lilu, a sister and brother living in Bucharest during the final years of the communist regime of Nicolae Ceauşescu. After Eva is expelled from her high school for her uncooperative attitude, she is sent to a technical school where she meets Andrei, with whom she plans to escape communist Romania by swimming across the Danube into Yugoslavia and relocating to Italy.  Lilu and his friends, meanwhile, volunteer for a childrens choir scheduled to sing for Ceauşescu, hoping this will give them a chance to assassinate the dictator.

==Cast and characters==

===The Matei family===
*Dorotheea Petre as Eva Matei
*Timotei Duma as Lalalilu Matei
*Carmen Ungureanu as Maria Matei
*Mircea Diaconu as Grigore Matei
*Jean Constantin as Uncle Florică

===Lilus friends===
*Valentino Marius Stan as Tarzan
*Marian Stoica as Silvică

===Evas friends===
*Cristian Văraru as Andrei, Evas friend
*Ionuţ Becheru as Alexandru Vomică, Evas boyfriend

===Others===
*Valentin Popescu as the music teacher
*Grigore Gonta as Ceauşică
*Florin Zamfirescu as the  school director
*Monalisa Basarab as choir teacher
*Corneliu Ţigancu as Bulba
*Nicolae Enache Praida as Titi

==Awards==
*Dorotheea Petre - Premiul de interpretare feminină (Female actor award)
*Dorotheea Petre - Un Certain Regard Award for Best Actress, 2006 Cannes Film Festival   

==Alternative titles==
*Comment jai fêté la fin du monde (French title) 
*How I Celebrated the End of the World (alternative translation)

==See also==
*Romanian New Wave

==References==
 

==External links==
*  
*  
*   at  
*   at  

 
 
 
 
 
 
 
 
Study on 1989 in Romanian Cinema https://www.academia.edu/3671294/Post-Heroic_Revolution_Depicting_the_1989_Events_in_the_Romanian_Historical_Film_of_the_Twenty-First_Century