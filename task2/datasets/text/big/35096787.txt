Gosainbaganer Bhoot
 
{{Infobox film
| name = Gosainbaganer Bhoot
| image = gosain_baganer_bhoot_film_poster.jpg
| caption = Film Poster
| alt = A kid with spectacles and a grown Indian man with make-up; A skinny man with the make-up of a ghost lies on the film title script, and the script is written in Bengali text.
| director = Nitish Roy 
| producer = Mou Raychowdhury 
| story = Shirshendu Mukhopadhyay
| screenplay =  Debaditya Datta
| starring = Victor Banerjee Ashish Vidyarthi Paran Bandyopadhay Kanchan Mallick Tinu Anand Antony Jojo Chandrabindoo 
| cinematography = Badal Sarkar
| editing = Malay Laha 
| distributor = V3g Films Pvt. Ltd.  
| released =  
| runtime = 
| country = India Bengali
}}
Gosaibaganer Bhoot ( ,  ) is a Bengali Comedy film|comedy-fantasy film directed by art director, production designer, and costume designer Nitish Roy, based on a novel by Bengali writer Shirshendu Mukhopadhyay. The music/lyrics was composed by Bengali brand Chandrabindoo (band)|Chandrabindoo.

==Plot==
The film narrates the tale of Burun, who is hopelessly weak in mathematics and has to take lessons from an eccentric teacher (Karali Sir). However, not all is well in Buruns world and he strays into Gosainbagan, befriends an enduring ghost (Nidhiram Sardar) only to later battle an evil force by the name of habi

==Cast==
* Tojo Gangopadhyay as Burun
* Antony as Gosaibaba
* Victor Banerjee as Ram Kobiraj
* Sajal Haldar as Fatik
* Kanchan Mullick as Nidhiram (Ghost)
* Biswajit Chakraborty as Sachin Sarkar (Head Master)
* Tinu Anand as Manmatha Ukil
* Paran Bandyopadhyay as Korali Master
* Shankar Chakraborty as Godai Daroga
* Ashish Vidyarthi as Habu Sardar
* Saswata Chatterjee as Bhelu Daktar
* Dwijen Bandyopadhyay as Panchkori Adhya
* Kharaj Mukherjee as Damodar Kaka
* Biplab Chatterjee as Kailash Mitra
* Locket Chatterjee as Buruns Mother

==Music==
===Soundtrack===
{{Infobox album
| Name        = Gosainbaganer Bhoot
| Type        = soundtrack Chandrabindoo
| Cover       = 
| Alt         =
| Released    = 9 December 2011
| Recorded    = Feature film soundtrack
| Length      =  
| Label       = Times Music
| Producer    = Mou Raychowdhury
}}

{{Track listing
| extra_column = Singer(s)
| title1 = Aamaar Bhoot Sob Nikhut | extra1 = Upal Sengupta, Anindya Chatterjee, Shubhendu, Surojit Mukherjee | length1 = 03.49
| title2 = Ek Je Chhilo Burun | extra2 = Pratul Mukherjee | length2 = 02.07
| title3 = Nidhiram-ke Paakrao | extra3 = Upal, Anindya, Kharaj Mukherjee, Chandril Bhattacharya, Surojit, Saagnik, Subhalaxmi, Sanchari, Tito, Tiyasha | length3 = 03.39
| title4 = Burun Tumi Anke Tero | extra4 = Subhalaxmi, Debasmita, Dipan, Tiyasha, Surojit, Biswanath, Chandril | length4 = 04.08
| title5 = Kutu Bhoot | extra5 = Tiyasha, Upal | length5 = 04.01
| title6 = Maar Maar Kaat Kaat | extra6 = Kharaj, Upal, Anindya, Biswanath, Surojit, Bidipta | length6 = 01.49
| title7 = Haabu Haalum | extra7 = Silajit Majumder, Upal, Saagnik, Surojit, Tito, Bidipta | length7 = 02.51
| title8 = Ek Je Chhilo Burun | extra8 = Joy | length8 = 02.09
| title9 = Burun Tumi Anke Tero | note9 = Singalong Track | extra9 =  | length9 = 04.17
| title10 = Kutu Bhoot | note10 = Singalong Track | extra10 =  | length10 = 04.01
}}

==See also==
*Chhayamoy

==References==
 

==External links==
*  

 
 
 


 