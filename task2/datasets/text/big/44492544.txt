The Mayor of Zalamea (1954 film)
{{Infobox film
| name =   The Mayor of Zalamea 
| image =
| image_size =
| caption =
| director = José Gutiérrez Maesso
| producer = 
| writer =  Pedro Calderón de la Barca  (play)   Manuel Tamayo    José Gutiérrez Maesso
| starring = Maunel Luna   Alfredo Mayo   Isabel de Pomés Juan Quintero  
| cinematography = Sebastián Perera  
| editing = Antonio Ramírez de Loaysa 
| studio = CIFESA
| distributor = CIFESA
| released = 25 January 1954  
| runtime = 80 minutes
| country = Spain Spanish 
| budget =
| gross =
}} historical drama Golden Age play The Mayor of Zalamea by Pedro Calderón de la Barca.

==Cast==
* Manuel Luna as Pedro Crespo 
* Alfredo Mayo as Don Álvaro 
* Isabel de Pomés as Isabel 
* José Marco Davó as Don Lope de Figueroa 
* Alberto Bové as Rebolledo  
* Mario Berriatúa as Juan  
* Juanita Azores as Chispa 
* María Fernanda DOcón as Inés  
* José Orjas as Don Mendo 
* Fernando Rey as El Rey
* Casimiro Hurtado 
* José Prada   
* Francisco Bernal
* Arturo Marín    Juan Vázquez  
* Manuel Guitián   
* Mariano Alcón  
* Luis Torrecilla Mario Guerrero

== References ==
 
 
==Bibliography==
* Mira, Alberto. The A to Z of Spanish Cinema. Rowman & Littlefield, 2010. 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 
 

 