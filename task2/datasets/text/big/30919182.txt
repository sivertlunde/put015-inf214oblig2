How to Live Forever
 
{{Infobox film
| name           = How to Live Forever
| image          = 
| alt            =
| caption        = 
| director       = Mark S. Wexler
| producer       = Mark S. Wexler and Mark Luethi
| writer         = Mark S. Wexler and Robert DeMaio
| starring       = Suzanne Somers, Phyllis Diller, Ray Bradbury
| music          = Steven Thomas Cavit
| cinematography = Sarah Levy, Allan Palmer, Robin Probyn
| editing        = Robert DeMaio
| studio         =
| distributor    = Variance Films
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

How to Live Forever, written by director Mark Wexler and Robert DeMaio, is a documentary that follows Mark on a three-year pilgrimage Nichols, Katherine.  "’How to Live Forever’ Looks at Health, Longevity."  Star Bulletin, 22 Oct 2009.  to discover the best practices and philosophies to help mitigate "the uncool trappings of old age."   With the death of his mother (artist Marian Witt-Wexler) and the arrival of an AARP card, Wexler begins to wonder if one can truly achieve immortality.  He interviews an eclectic group of celebrities, health care professionals, centenarians, followers of Laughter Yoga, and scientists contemplating technology’s impact on the average lifespan in an attempt to conquer death. 

Wexler ventures into the home of fitness legend Jack LaLanne and his wife Elaine for a personal training session and a raw food smoothie.  Often called the "godfather of fitness", Jack LaLanne was among the first to publicly preach the health benefits of regular exercise and a good diet.  In 1936, when LaLanne was 21-years-old, he opened one of the nation’s first fitness gyms, and in the 1950s he filmed a series of television exercise programs.  LaLanne invented several exercise machines, was inducted to the California Hall of Fame in 2008, and has a star on the Hollywood Walk of Fame. 

In the documentary,   on Three’s Company. 

Aubrey de Grey is a British author and theoretician in the field of gerontology. His studies focus on researching and developing tissue-repair strategies intended to prolong lifespans.  De Grey is the editor-in-chief of the academic journal Rejuvenation Research, author of The Mitochondrial Free Radical Theory of Aging, and co-author of Ending Aging.   Wexler, as well as news sources like The New York Times, the BBC, and Fortune (magazine)|Fortune, have interviewed de Grey to learn more about his theories on anti-aging.
 Something Wicked Pulitzer board recognized Bradbury "for his distinguished, prolific, and deeply influential career as an unmatched author of science fiction and fantasy."   Many of his works have been adapted into television shows or films. When asked if he’s afraid of getting old, Bradbury replies, "No, I’ve never had that fear; I knew that I was collecting truths along the way."

Comedienne Phyllis Diller has maintained the stage presence of a boisterous, eccentric housewife with an unusual laugh in stand-up and sit-coms since 1952.  She has guest-starred in dozens of television shows and also voiced the Queen in Disney’s A Bugs Life, Jimmy’s grandmother in Nickelodeon’s Jimmy Neutron, and Peter Griffin’s mother in Family Guy. In the documentary, Diller believes comedy is important to health, maintaining that "laughter fluffs up every cell in the body."

Claiming to have been born in 1906, Pierre Jean Buster Martin was a 104-year-old beer drinking and chain-smoking marathon runner.  He did not include fish, dairy, tea, or water in his diet.  Buster smoked since he was seven-years-old and followed a diligent regimen of beer, cigarettes, and red meat.   In 2008, Buster successfully finished the London Marathon. When Buster was not training for marathons, he cleaned vans for Pimlico Plumbers in southeast London.  On April 12, 2011 Buster finished work, had a beer, and went home.  He died that night, at age 104. 
 Oki Dog and his philosophy of food with Wexler.  Gold claims that "eating is one of the great pleasures of life", and believes that those with diet restrictions are missing out on a lot that life has to offer.  He is the author of Counter Intelligence and has written for several magazines throughout his lifetime.  In 2007, Gold became the first critic to win the Pulitzer Prize. 

Dr. Madan Kataria gives viewers of the documentary a prescription for longevity:  "Laugh ten minutes every day for no reason."  Known as the "Guru of Giggling", Kataria researched the physiological and psychological benefits of laughter and started a Laughter Yoga club in 1995 with just five people in a public park in Mumbai.   The unusual exercise routine combines yoga breathing with laughter exercises, and it has grown to more than 6,000 Laughter Yoga clubs in over 60 countries.

In the documentary, Eleanor Wasson reveals that being a vegetarian and drinking vodka every night are a few secrets to her 100-year lifespan.  Throughout her life, Wasson was a volunteer, an activist, and a devotee to social and political causes of various kinds.  She was the founder of WomenRise for Global Peace and had been a long-time fighter against the spread of nuclear weapons.  For thirty years, Wasson was the Coordinator of Volunteer Services for UCLA. She died April 6, 2008. 
 National Geographic, and the New York Times on various subjects.   Iyer is a close friend of the director and is, Wexler claims, "the sanest person I know." He turns the camera on Wexler, asking the director to examine his own hopes and intentions for making the documentary. He asserts that like the ending of books and films, "death makes sense of everything that comes before it."

The documentary, contrary to its title, is not a how-to guide to eternal life. Rather, it is an examination of different philosophies and perspectives on life, offering viewers a glimpse into the science and commercialism in fields like funeral planning, cryonics, and anti-aging practices. Meanwhile, the film challenges viewers to examine their own notions of whether to combat or accept the inevitability of aging; it is this dilemma that drives Wexler’s search both around the world and within himself, asking the question, "If you could take a pill to live 500 more years, would you?"

How to Live Forever premiered at the 17th Annual Hamptons International Film Festival  in 2009. It was also screened at the Palm Springs International Film Festival  in January 2011 and the Gasparilla International Film Festival in March 2011.

In addition to How to Live Forever, Mark Wexler directed Tell Them Who You Are (2004), about his father, cinematographer Haskell Wexler, and Me and My Matchmaker (1996). He also co-produced Air Force One (2002). Me and My Matchmaker won an Audience Award for Best Documentary at the 2006 Slamdance Film Festival. 

Robert DeMaio, director of the 1983 TV series Against the Odds and writer of TV documentary Reversal of Fortune (2005), co-wrote Tell Them Who You Are and Me and My Matchmaker with Wexler.

Mark Luethi, co-producer of How to Live Forever and associate producer of Tell Them Who You Are, is currently a freelance photographer.
 God of Love, is also Producer of Marketing and Distribution for How to Live Forever.

==Synopsis==
Filmmaker Mark Wexler is not going down without a fight. Overwhelmed by the loss of his mother and confronted by his own advancing age, Wexler embarks on a curious, lively, and sometimes troubling worldwide trek to investigate what it really means to live forever.

Wexler’s search takes him to the places where people live the longest: from Okinawa, Japan to Iceland. He journeys to Las Vegas to attend a convention for funeral directors, and the Ms. Senior America Pageant. He contemplates a future in cold storage at the Alcor Life Extension Foundation cryonics facility in Arizona. Wexler even receives exercise tips from Jack LaLanne, hormone replacement advice from Suzanne Somers, and promises of eternal life from biogerontologist Aubrey de Grey.

But whose advice should he take? Does Buster Martin, a chain-smoking, beer-drinking centenarian marathon-runner have all the answers? What about elder-porn star Shigeo Tokuda? The more Wexler travels into the strange worlds of Laughter Yoga, calorie restriction, and old-fashioned religion, the less sure he becomes of his goal. Can it be that life’s true meaning is found in the humble chili dog?

Wexler contrasts unusual characters and philosophies with the insights of health, fitness and life-extension experts in his engaging new documentary. The film challenges our notions of youth and aging with comic poignancy. Begun as a study of prolonging life, How To Live Forever evolves into a fun and provocative examination of life’s deepest mystery.

==Featuring==
All of the following people are featured in How to Live Forever :

{| class="wikitable"
|-
| Gertrude Baines || Samm Mullins
|-
| Dolores Bates || Scott Mullins
|-
| Ray Bradbury || Zenei Nakamura
|-
| Aubrey de Grey || Sherwin Nuland
|-
| Brian M. Delaney || Ushi & Kikue Okushima
|-
| Phyllis Diller || Don & Edna Parker
|- John Robbins
|-
| Sebastien Gendry || Randall Roberts
|-
| Jonathan Gold  || Linda Salvin
|-
| Rathyna Gomer || Lisa Schoonerman
|-
| Brian Harris (Actor) || Diana Schwarzbein
|-
| Pico Iyer || Willard Scott
|-
| Marge Jetton || Takanori Shibata
|-
| Tanya Jones || Suzanne Somers
|-
| Madan Kataria || Shigeo Tokuda
|-
| Ronald Klatz || Ellsworth Wareham
|-
| Tricia Kurunathan || Eleanor Wasson
|-
| Ray Kurzweil || Craig Willcox
|-
| Elaine & Jack LaLanne || Jessica L. Williams
|-
| Thomas Lynch (Actor) || Marianne Williamson
|-
| Buster Martin || Tyrus Wong
|-
| Shinei Miyagi || Heather Yegge
|-
| Kelly Morton || Akimitsu Yokoyama
|-
| Al Mott || Robert O. Young
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 