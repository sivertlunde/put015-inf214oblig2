Shining Through
 
 
{{Infobox Film
| name           = Shining Through 
| image          = Poster of the movie Shining Through.jpg
| image_size     = 
| caption        = 
| director       = David Seltzer 
| producer       = David Seltzer Carol Baum Sandy Gallin Zvi Howard Rosenman
| screenplay     = David Seltzer Shining Through by Susan Isaacs
| starring       = Michael Douglas Melanie Griffith Liam Neeson Joely Richardson John Gielgud
| music          = Michael Kamen
| cinematography = Jan de Bont Craig McKay
| distributor    = 20th Century Fox
| released       = January 31, 1992
| runtime        = 133 mins.
| country        = United Kingdom   United States English German German
| budget         = $30 million
| gross          = $43,838,238
}}
Shining Through is a 1992 British-American World War II drama film, directed and written by David Seltzer and starring Michael Douglas and Melanie Griffith, with Liam Neeson, Joely Richardson and John Gielgud in supporting roles. Although based on the novel of the same name by Susan Isaacs, the films plot and characters are considerably different. The original music score was composed by Michael Kamen. The films tagline is: "He needed to trust her with his secret. She had to trust him with her life."

==Plot== German Jewish parentage, applies for a new job as a secretary with a New York law firm, but rejected as she didnt graduate from a prestigious womens college.
 German fluently, she becomes translator to Ed Leland (Michael Douglas), a humourless attorney, who is referred to as the "pallbearer" by his peers due to his lack of humor. She gradually comes to suspect that he hides dark secrets. She is proven right when, after America officially joins forces with the Allies of World War II|Allies, he emerges as a colonel in the Office of Strategic Services|OSS. She accompanies him to confidential meetings in New York and Washington D.C., and before long, they become lovers. When he is suddenly posted away, she is left alone and devastated. Assigned to work in the War Department, Linda longs for and hears nothing of Ed until he reappears as suddenly as he left with an attractive female officer. Reluctant to resume their affair, he does re-employ her.

He and his colleagues abruptly need to replace a murdered agent in Berlin at very short notice. Despite knowing little about intelligence work — only what shes seen in movies — Linda volunteers and Ed allows himself to be persuaded by her fluent German and passion to contribute to the war effort, not to mention her skills in reproducing German-based dishes, as proven by her banging on his front door in the middle of the night and getting him to taste her "German Kompot". Her mission is to bring back data on the V-1 flying bomb. They travel to Switzerland, where he hands her over to master spy Konrad Friedrichs, codenamed "Sunflower" (John Gielgud). Despite being appalled at her dialect ("the accent of a Berlin butchers wife!"), he installs her in the basement of his Berlin mansion and introduces her to his niece, Margarete von Eberstein (Joely Richardson), a socialite also working as an Allied agent.

Linda is planted as a cook in the household of a social-climbing Nazi, but her first dinner is a disaster and she is sacked on the spot. She is taken on as a nanny to the children of high-ranking Nazi officer Franz-Otto Dietrich (Liam Neeson), who had been a guest at the dinner. Unable to report back to Ed, she is taken to Dietrichs house and effectively drops out of sight. He brings home confidential documents, frantically searching for them - intending to photograph them, meeting her contact, she also locates her cousins, believed to be hiding in Berlin, speaking with Margaret.
With the children in her care, she tracks down her relatives hiding place but is too late. They have already been captured and the cellar is empty. As all hope is lost, raid sirens blare in the city as residents, including her and the children, run through the streets as buildings around them are blown apart by the falling bombs.

The preceding attack causes the frightened children to reveal a hidden room, which Linda finds and secretly photographs Dietrichs top-secret papers. An interesting factual hitch is that when she is doing so, Peenemuende is seen to be located in the Ruhr region of Germany, whereas, in reality, it is located on the northern coast. When Dietrich invites her to the opera the next evening, her cover is blown by Margretes mother, who believes her to be a friend of her daughters from college. In desperation, she flees from the Dietrich home and seeks sanctuary with Margrete, only to find to her horror that she is a double agent who has betrayed Lindas cousins and has now also betrayed her. She shoots her, wounding her, but she overpowers Margrete and kills her. Though in pain, she manages to slip down the laundry chute, narrowly escaping the German officers raiding Margretes apartment.

Badly wounded, Linda is found and rescued by Ed, who has come to Berlin in the guise of a high-ranking German officer. Pretending to be mute, as he does not speak the language, he takes her to the railway station and they travel to the Swiss border. In the film, the border where the train stops is seen on a sign to be the town of "Altstätten," which is, in fact, not a border town, and never has been. She is barely alive and his travel papers are out of date. His bluff fails to sway the border guards, forcing him to shoot his way out. Still carrying her, he struggles towards the border. The German sniper guarding it wounds him twice, but he manages to get himself and Linda across it before collapsing. The sniper is shot by his Swiss counterpart, an act which is justified by the fact that the German border guard was still shooting at Leland after he had crossed into Switzerland.
 microfilm of the secret German documents has been retrieved from a hiding place inside her glove. She waves to him and their two sons. He joins her on camera as the film ends.

==Cast==
 
 
* Michael Douglas as Ed Leland
* Melanie Griffith as Linda Voss
* Liam Neeson as Franz-Otto Dietrich
* Joely Richardson as Margrete von Eberstein
* John Gielgud as Sunflower
* Francis Guinan as Andrew Berringer
* Patrick Winczewski as Fishmonger
* Anthony Walters as Dietrichs Son
* Victoria Shalet as Dietrichs Daughter Sheila Allen as Olga Leiner, Margretes Mother
* Stanley Beard as Lindas Father
* Sylvia Syms as Lindas Mother
* Ronald Nitschke as Horst Drescher
* Hansi Jochmann as Hedda Drescher
* Peter Flechtner as S.S. Officer at Fish Market
* Alexander Hauff as S.S. Officer at Fish Market
* Claus Plankers as S.S. Officer at Fish Market
 
* Dana Gladstone as Street Agitator
* Lorrine Vozoff as Personnel Director
* Mathieu Carrière as Von Haefler
* Deirdre Harrison as USO Singer
* Wolf Kahler as Border Commandant
* Wolfe Morris as Male Translator William Hope as Kernohan
* Nigel Whitmey as 1st G.I. in Canteen
* Rob Freeman as 2nd G.I. in Canteen
* Lisa Orgolini as Girl in Canteen
* Susie Silvey as Colonels wife
* Jay Benedict as Wisecracker in War Room
* Thomas Kretschmann as Man at Zurich Station
* Klaus Münster as Cab Driver
* Markus Napier as S.S. Officer
* Constanze Engelbrecht as Stafson Von Neest
* Martin Hoppe as German Soldier
 
* Fritz Eggert as German Soldier
* Ludwig Haas as Hitler
* Clement von Frankenstein as BBC Interviewer
* Lorelei King as Lelands New Secretary
* Hans Martin Stier as Truck Driver
* Wolfgang Heger as Bus Conductor, Kinderstrasse
* Michael Gempart as Man at Kinderstrasse
* Hana Maria Pravda as Babysitter
* Lutz Weillich as Train Station Guard
* Wolfgang Müller as Bus Conductor
* Markus Kissling as Swiss Border Guard
* Anna Tzelniker as Cleaning Woman
* Andrzej Borkowski as German Refugee
* Simon De Deney as S.S. Man
* Tusse Silberg as Woman Dinner Guest at Dreschers
* Suzanne Roquette as Woman Dinner Guest at Dreschers
* Janis Martin as Opera Singer
 

==Reception== Razzie Awards declared Shining Through the Worst Picture of 1992, with Melanie Griffith being voted Worst Actress and David Seltzer for Worst Director. It also received nominations for Michael Douglas as Worst Actor and for Seltzer in the category of Worst Screenplay. 

Roger Ebert wrote in the Chicago Sun-Times, "I know its only a movie, and so perhaps I should be willing to suspend my disbelief, but Shining Through is such an insult to the intelligence that I wasnt able to do that. Here is a film in which scene after scene is so implausible that the movie kept pushing me outside and making me ask how the key scenes could possibly be taken seriously." 

Janet Maslin wrote in the New York Times that the first three-quarters of Susan Isaacs book "never made it to the screen," including Linda Vosss love affair and marriage to her New York law firm boss, John Berringer. "David Seltzers film version of Shining Through manages to lose also the humor of Susan Isaacs savvy novel. Even stranger than that is the films insistence on jettisoning the most enjoyable parts of the story." 
 John Wilsons book The Official Razzie Movie Guide as one of the The 100 Most Enjoyably Bad Movies Ever Made. 

==Production== DEFA Studios, the state film studios of East Germany.

Because all of Berlins great train stations were destroyed in WWII, the production traveled some distance to Leipzig to shoot scenes in the Leipzig Hauptbahnhof terminus, built in 1915 and the largest in Europe. This was prior to its massive modernization by the Deutsche Bahn.

The finale of the film, set at a border crossing and involving a period train, was shot in Klagenfurt, Austria.
 Washington scenes at the beginning of the film were shot in and around London and at nearby Pinewood Studios. Locations included the Old Royal Naval College in Greenwich, Hammersmith, and St Pancras Station, which doubled for Zurich Station for a brief sequence set in Switzerland.

==Notes==
 

==External links==
*  
*  
*  
*  
*  

 

   
{{succession box
| title=Razzie Award for Worst Picture
| years=13th Golden Raspberry Awards
| before=Hudson Hawk
| after=Indecent Proposal
}}
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 