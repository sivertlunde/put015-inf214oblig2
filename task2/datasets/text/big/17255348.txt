Thaskaraveeran (2005 film)
{{Infobox film
| name     = Thaskara Veeran
| image    = Thaskara Veeran.jpg
| caption  = 
| director = Pramod Pappan
| story = Antony Eastman
| screenplay=  Dennis Joseph
| starring = Mammootty Nayanthara
| producer = Vindhyan
| cinematography = Jibu Jacob
| music = Ouseppachan
| editing = P. C. Mohanan
| runtime = 
| budget = 
| country = India
| language = Malayalam
}}
Thaskara Veeran is a Malayalam film directed by Pramod Pappan. The film stars Mammootty, Nayantara and Sheela in lead roles. The film also stars Madhu (actor)|Madhu, Innocent (actor)|Innocent, Rajan P.Dev, Siddique (actor)|Siddique, Mamukkoya etc. The film released in 2005. It has a dubbed Hindi version which is called Kala Samrajya.

==Cast==
* Mammootty ....Arakalam Babu

* Nayantara ....Thankamani
* Sheela ..... Meenakshi Innocent ....Malayil Eapen Siddhique .... Thommi
* Rajan P. Dev	 ...	Arakkalam Kuttappan Madhu	 ...	Arakkalam Paili
* Salim Kumar	 ...	Suguthan
* Mohan Jose     ...        Chandi Augustine	 ...	Ayyappan Pillai
* T. P. Madhavan	 ...	Ramkumar
* Spadikam George	 ...	Itty
* Bheeman Raghu	 ...	Kargil Narayanan Kunchan	 ...	Raghavan
* Mamukkoya	 
* Deepika Mohan  ... Raghavans wife

== External links ==
*  

 
 
 
 
 