Bodies, Rest & Motion
 
{{Infobox film
| name           = Bodies, Rest & Motion
| image          = Bodies,_Rest_&_Motion.jpg
| caption        = DVD cover Michael Steinberg
| producer       = Joel Kastelberg
| screenplay     = Roger Hedden
 
| starring       = Phoebe Cates Bridget Fonda Tim Roth Eric Stoltz
| cinematography = Bernd Heinl
| editing        = Jay Cassidy
| distributor    = Fine Line Features
| music          = Michael Convertino
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
}}

Bodies, Rest & Motion is a 1993 American   as a motorcyclist.

Bodies, Rest & Motion was screened in the Un Certain Regard section at the 1993 Cannes Film Festival. 

==Cast==
* Phoebe Cates - Carol
* Bridget Fonda - Beth
* Tim Roth - Nick
* Eric Stoltz - Sid
* Alicia Witt - Elizabeth Sandra Lafferty - Yard Sale Lady
* Sidney Dawson - TV Customer
* Jon Proudstar - Station Attendant Scott Johnson - Chip
* Kezbah Weidner - Dine Woman
* Peter Fonda - Motorcycle Rider
* Amaryllis Borrego - Waitress
* Rich Wheeler - Elizabeths Grandfather
* Scott Frederick - TV Store Kid
* Warren Burton - Radio Preacher (voice)

==Reception== hip to latex too long."   Roger Ebert gave the film two stars out of four, saying it is "one of those movies that not only comes accompanied by supporting materials, but seems fairly pointless unless you brief yourself"; according to Ebert, if the viewer knows Newtons laws of motion|Newtons first law of motion and keeps in mind that "Generation X is a media buzzword for the late-twentysomethings   so named, apparently, for their lack of an identity", it is "possible to watch Bodies, Rest and Motion, and find that it makes a statement about its generation. Without the cheat sheet, youd more likely say the movie is about a bunch of aimless, boring, hopeless drips, who inspire neither sympathy nor interest. If I were a doctor, Id suspect Lyme disease."   Rita Kempley of the Los Angeles Times

==Original stage version==
Hedden wrote the script based on his own play (theatre)|play, which premiered off-Broadway on December 21, 1986, at the Mitzi E. Newhouse Theater, with William H. Macy as Nick, Christina Moore as Carol, Laurie Metcalf as Beth, and Andrew McCarthy as Sid.     According to Frank Rich, the plays "meager plot&mdash;an arbitrary coupling or two, followed by equally whimsical leave-takings and reunions&mdash;leads to nothing more than a sentimental final-curtain reaffirmation of the transforming powers of true love."   The original run lasted 22 performances. 

==Home video== laserdisc release from Criterion Collection.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 