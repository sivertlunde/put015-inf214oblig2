Who Is KK Downey?
{{Infobox Film
| name           = Who Is KK Downey?
| image          = Who is KK Downey?.jpg
| caption        = 
| director       = Pat Kiely Darren Curtis
| producer       = Brandi-Ann Milbradt Trevor Barnes Kieran Crilly et al. 
| writer         = Pat Kiely Darren Curtis Matt Silver
| starring       =
| music          = Tod van Dyk
| cinematography = Bobby Shore
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Canada
| language       = English
| budget         = 
}} 2008 comedy film produced in Montreal, Canada.

The plot revolves around two hipster friends who pretend to be a writer with a troubled girl friend.

The film was written and directed by members of the Montreal comedy troupe Kidnapper Films. Several members also act in the film.

==Cast==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Actor
! Role
|- Scott Thompson || 
|-
| Darren Curtis || Terrance/KK
|-
| Kristin Adams || Sue
|-
| Matt Silver || Theo Huxtable
|-
| Pat Kiely || Connor
|-
| Dan Haber || Frankie Lola
|-
| Claire Brosseau || Crazy Mary
|-
| Pilar Cazares || Boochis
|-
| Christine Ghawi || Penelope
|-
| Trevor Hayes || Saul
|-
| Brent Skagford || Fishlips
|-
| Etan Muskat || Ratso
|}

==Release==
The film premiered at the Cinequest Film Festival in San Jose.  It then screened at the Philadelphia Film Festival before its limited release in Canada in September, 2008.  It also screened at the Oldenburg International Film Festival, the Boston Underground Film Festival, the Toronto After Dark Film Festival, the Tallgrass Film Festival, CMJ Film Festival, the Kingston Canadian Film Festival, the Chicago International Music and Movie Festival, and the Wisconsin Film Festival.

The film was released in Canada on DVD on April 14, 2009 by Koch International, and on DVD in the USA by Indiepix in October 2009.

==Reception==

Who is KK Downey? has received mostly positive reviews. Fred Entertainment gave the film a positive review claiming that the film "(gives) more laughs per 90 minutes than your average Saturday Night Live show."  Exclaim! called the film "easily the funniest Canadian film to hit screens in decades."  The Globe and Mail gave the film three stars and referred to it as "rude and blessedly tasteless, without any sense the creators have any agenda except to be funny." 

==References==
 http://www.asitecalledfred.com/2009/11/13/trailer-park-peter-rodger/ 
 http://www.exclaim.ca/motionreviews/generalreview.aspx?csid1=133&csid2=774&fid1=38089 
 http://www.whoiskkdowney.com/wp-content/uploads/2008/11/globe-and-mail-review.jpg 
 

==External links==
*  
* 

 
 
 
 
 
 

 