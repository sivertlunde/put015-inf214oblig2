È arrivato il cavaliere!
{{Infobox film
| name           = È arrivato il cavaliere!
| image          = e_arrivato_il_cavaliere_tino_scotti_steno_005_jpg_tqed.jpg
| image_size     = 272px
| caption        =  Steno
| producer       = Carlo Ponti
| writer         = Age & Scarpelli Marcello Marchesi Vittorio Metz
| narrator       = 
| starring       = 
| music          = Nino Rota
| cinematography = Mario Bava
| editing        = Franco Fraticelli
| distributor    = 
| released       = 1950
| runtime        = 90 minutes
| country        = Italy
| language       = Italian
| budget         = 
}} 1950 Italy|Italian comedy film directed by Mario Monicelli and Steno (director)|Steno. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Cast==
*Tino Scotti ...  Il Cavaliere
*Silvana Pampanini ...  Carla Colombo
*Enrico Viarisio ...  Il Ministro
*Enzo Biliotti ...  Il Commissario
*Nyta Dover ...  Musette
*Rocco DAssunta ...  Capo dei Banditi
*Galeazzo Benti ...  Marchese Bevilacqua
*Marcella Rovena ...  Signora Varelli
*Alda Mangini ...  Moglie del Ministro
*Federico Collino ...  Commendatore Varelli
*Guido Morisi
*Giovanna Galletti...  Signora Colombo
*Carlo Mazzarella ...  LAssessore
*Arturo Bragaglia ...  Buchs
*Ettore Jannetti ...  Signor Colombo

==References==
 

== External links ==
*  
  
 

 
 
 
 
 
 
 
 
 
 