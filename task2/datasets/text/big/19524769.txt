Den store gavtyv
 
{{Infobox film
| name           = Den store gavtyv
| image          = 
| caption        = 
| director       = Johan Jacobsen
| producer       = 
| writer         = Arvid Müller
| narrator       = 
| starring       = Dirch Passer
| music          = 
| cinematography = Henning Bendtsen
| editing        = Annelise Hovmand
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Den store gavtyv is a 1956 Danish comedy film directed by Johan Jacobsen and starring Dirch Passer.

==Cast==
* Dirch Passer as K.M.M. Mathisen
* Ole Monty as S.P.R.F. Rodian
* Asbjørn Andersen as Forlægger Stenbæk
* Marguerite Viby as Else Jessen
* Preben Mahrt as Erik Jessen
* Bodil Miller as Anna Lise, Rodians niece
* Oscar Ljung as Kalle Karlfeldt
* Johannes Marott as Viggo Frederiksen
* Professor Tribini as Alfred Mogensen
* Gunnar Bigum as Tjener Stoffer Henry Nielsen as Nattevagt
* Mogens Brandt as Bankkunde
* Kjeld Petersen as Værtshusgæst
* Hans Brenå as Akrobatdanseren
* Mogens Davidsen as Bartender Christiansen
* Paul Mourier as Juvelereren
* Jens Kjeldby as Bankkasseren
* Per Wiking
* Tao Michaëlis
* Holger Juul Hansen as Kriminalassistent Rønne
* Mimi Heinrich as Guldsmedens veninde
* Lisbeth Movin as Sekretær
* Alfred Wilken as Guldsmeden

==External links==
* 

 
 
 
 
 
 
 
 


 
 