The Here After
{{Infobox film
| name           = The Here After
| image          = 
| caption        =
| director       = Magnus von Horn
| producer       = Madeleine Ekman Sophie Erbs Mariusz Wlodarski 
| writer         = Magnus Von Horn 
| starring       = Ulrik Munther
| music          = 
| cinematography = Łukasz Żal
| editing        = Agnieszka Glinska
| studio         = Cinema Defacto 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Sweden Poland France
| language       = Swedish
| budget         = 
| gross          = 
}}

The Here After ( ) is a 2015 Swedish drama film written and directed by Magnus von Horn. The film is a co-production between Sweden, Poland and France. It has been selected to be screened in the Directors Fortnight section at the 2015 Cannes Film Festival. 

== Cast ==
* Ulrik Munther as John 
* Mats Blomgren as Martin 
* Wieslaw Komasa as Grandfather 
* Alexander Nordgren
* Loa Ek as Malin  
 
== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 

 