Here Come the Waves
{{Infobox film
| name           = Here Comes the Waves
| image          = Here Come the Waves.jpg
| image_size     = 
| caption        = 
| director       = Mark Sandrich
| producer       = Mark Sandrich
| writer         = 
| narrator       = 
| starring       = Bing Crosby
| music          = 
| cinematography = Charles Lang
| editing        = Ellsworth Hoagland
| studio         = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 99 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Here Come the Waves is a 1944 film directed by Mark Sandrich. It stars Bing Crosby and Betty Hutton. 

==Cast==
*Bing Crosby as Johnny Cabot
*Betty Hutton as Susan / Rosemary Allison
*Sonny Tufts as Windy Pinetop Windhurst
*Ann Doran as Ruth
*Gwen Crawford as Tex

==Music==
Johnny Mercer and Harold Arlen wrote "Accentuate the Positive" for this film.   


==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 