Ek Din Achanak
{{Infobox film  name           = Ek Din Achanak image          = Dvd ek din achanak.jpg caption        = DVD cover of the film writer         = Ramapada Chowdhury (story)  Mrinal Sen (screenplay) producer       = director       = Mrinal Sen starring       = Shreeram Lagoo Shabana Azmi Aparna Sen music          = Jyotishka Dasgupta cinematography = K.K. Mahajan released       = 1989 runtime        = 105 minutes
| country       = India language       = Hindi
}}

Ek Din Achanak ( : Suddenly, One Day) is a 1989 art film directed by Mrinal Sen, based on a Bengali novel, Beej by Ramapada Chowdhury. 

== Synopsis ==
One evening, in the midst of torrential rains, a professor (Shreeram Lagoo) goes out for a walk and fails to return.  As the evening stretches into days and the days into weeks with no sign of him, the member of his family struggle both to regain their footing and to understand  what might have caused him to leave.  Slowly they return to their quotidian activities.  The professors son Amit (Arjun Chakraborty) establishes his fledgling business; his younger daughter Sima (Roopa Ganguly) resumes her studies at college; and his elder daughter, Neeta (Shabana Azmi), the backbone of the family, returns to her office job.  

Behind the facade of normalcy, though, the family is deeply wounded.  Amit enjoys success in his business but is bitter and dour.  Neeta has a very patient and supportive boyfriend ( )?  Was his academic career crumbling?  Was he a plagiarist?  

==Cast==
*Shabana Azmi - Neeta
*Shreeram Lagoo - Professor (Neetas father)
*Aparna Sen  -  Student
* Uttara Baokar  - Neetas mother
* Roopa Ganguly - Seema (Neetas sister)
* Arjun Chakraborty - Neetas brother
* Manohar Singh - Neetas uncle
* Anjan Dutt - Neetas boyfriend
* Lily Chakravarty - Neighbour
* Anil Chatterjee - Arunbabu

==Awards==
* 1989:  
* 1989:  

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 