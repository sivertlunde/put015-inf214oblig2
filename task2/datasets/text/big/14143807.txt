The Desert Hawk
 Desert Hawk}}
{{Infobox film
| name           = The Desert Hawk
| image          =
| caption        =
| director       = B. Reeves Eason Rudolph C. Flothow
| writer         = Sherman L. Lowe Leslie Swabacker Jack Stanley Leighton Brill Kenneth MacDonald Charles Middleton
| music          = Lee Zahler
| cinematography = James S. Brown Jr.
| editing        =
| distributor    = Columbia Pictures
| released       = 7 July 1944
| runtime        =
| country        =   English
| budget         =
| gross          =
}} Columbia Serial film serial. It was the 23rd serial produced by Columbia.

==Plot==
The sinister Hassan starts plotting against the recently crowned Caliphate|Caliph, his twin brother Kasim. The evil twin engages the help of Faud who sends his man to the palace to kidnap the Caliph and murder him. These henchmen enter the palace and wound Kasim who manages to escape. A beggar named Omar finds him and cares for him until his health is restored. By the time the wounded Kasim recovers his brother has taken over the throne and plans to marry Princess Azala the daughter of the Emir of Telif who does not know that the current Caliph is an impostor. Kasim decides to fight for the throne and the princess after he finds a suit of chainmail displaying a hawk on the front. 

==Cast==
* Gilbert Roland - Kasim, The Desert Hawk and Hassan, his Evil Twin Brother. According to Cline, Roland was "superbly convincing as the dashing Hawk, and made memorable an otherwise routine thriller." {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 29
 | chapter = 3. The Six Faces of Adventure
 }} 
* Mona Maris - Princess Azala
* Ben Welden - Omar, the Beggar Kenneth MacDonald - Akbar
* Frank Lackteen - Faud, the Chief Chamberlain
* I. Stanford Jolley - Saladin Charles Middleton - Koda Bey
* Egon Brecher - Grey Wizard
* Georges Renavent - Emil of Telif (as George Renavent)
* Margia Dean - Wizards Daughter
* Forrest Taylor - Akrad, the money-lender

==Production==
The Desert Hawk is a "Western (genre)|western" set in the Middle East with swashbuckling elements. 

==Chapter titles==
# The Twin Brothers
# The Evil Eye
# The Mark of the Scimitar
# A Caliphs Treachery
# The Secret of the Palace
# The Feast of the Beggars
# Double Jeopardy
# The Slave Traders
# The Underground River
# The Fateful Wheel
# The Mystery of the Mosque
# The Hand of Vengeance
# Swords of Fate
# The Wizards Story
# The Triumph of Kasim
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 238
 | chapter = Filmography
 }} 

==See also==
* List of American films of 1944 List of film serials by year
*List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{Succession box Columbia Serial Serial
| The Phantom (1943 in film|1943)
| years=The Desert Hawk (1944 in film|1944) Black Arrow (1944 in film|1944)}}
 

 

 

 
 
 
 
 
 
 
 