Splendor (1935 film)
{{Infobox film
| name           = Splendor
| image          = SplendorVHS.JPG
| image_size     = 100px
| caption        = VHS cover
| director       = Elliott Nugent
| producer       = Samuel Goldwyn
| writer         = Rachel Crothers
| starring       = Miriam Hopkins Joel McCrea
| music          =
| cinematography =
| editing        =
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       =  
| runtime        = 75 min.
| country        = United States English
}}
Splendor (1935 in film|1935) is a drama film starring Miriam Hopkins and Joel McCrea, produced by Samuel Goldwyn, and distributed by United Artists. 
 The Richest Barbary Coast. The two would also star together in These Three and Woman Chases Man.

==Cast==
*Miriam Hopkins as Phyllis Manning Lorrimore
*Joel McCrea as Brighton Lorrimore
*Paul Cavanagh as Martin Deering
*Helen Westley as Mrs. Emmeline Lorrimore
*Billie Burke as Clarissa
*David Niven as Clancey Lorrimore
*Katharine Alexander as Martha Lorrimore

==External links==
* 

 
 

 
 
 
 
 
 
 

 