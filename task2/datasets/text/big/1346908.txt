Disney's The Kid
 
{{Infobox film
| italic title   = no
| name           = The Kid
| image          = Disneys the kidposter.jpg
| caption        = Theatrical release poster
| director       = Jon Turteltaub
| producer       = {{plain list|
* Hunt Lowry
* Arnold Rifkin
* Christina Steinberg
* Jon Turteltaub
* David Willis
}}
| writer         = Audrey Wells
| starring       = {{Plain list|
* Bruce Willis
* Spencer Breslin
* Emily Mortimer
* Lily Tomlin
* Chi McBride
}}
| music          = {{Plain list|
* Marc Shaiman
* Jason White
}}
| cinematography = Peter Menzies Jr.
| editing        = {{Plain list|
* Peter Honess David Rennie
}}
| studio         = Walt Disney Pictures Buena Vista Pictures
| released       = July 7, 2000
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $65 million   
| gross          = $110.3 million
}}
 fantasy film, Twilight Zone episode, "Walking Distance", that originally aired on October 30, 1959. 

==Plot== throwing pie at Riley, to get him off the hook. Amy feels that the video is unethical, and Russ throws the tape away at first, but he secretly retrieves it.

Russ returns home and hears someone inside. He finds a toy plane on his front step which he assumes had been left there by his father for him. He finds the boy who had been making noises and begins a chase through the house, which goes to the streets, reaching the airport where he sees the boy entering Skyway Diner. He tries to catch him there but no one inside has seen the boy, and Russ is led to believe he is hallucinating again.

After returning home from a psychiatrist appointment the next day, Russ is surprised to find the boy in his couch eating popcorn. He asks the boy who he is and what has he come for, to which Rusty replies to take his toy plane, but that when he saw the television and the popcorn he couldnt resist. Starting to see a resemblance, Russ begins comparing memories and birthmarks with Rusty, and figures out that the boy is actually Russ as a kid.

After a series of questions, Rusty tells Russ that he dislikes his future. The next day, when Amy finds out about the boy, she takes a liking to him, but Russ makes fun of his weight. Rusty then recalls some bullies bothering him the same way, so Russ brings him to a clients boxing ring and teaches him to fight.

Amy starts wondering about Russ and Rustys similarities and begins to think they are father and son. After shouting at Russ for hiding such a secret from her, Rusty thinks that they tell Amy the truth, though they end up arguing instead. Amy watches Rusty and Russ argue and sees the two fighting, arguing, and gesturing the same way, at which she faints. Later, Russ and Amy talk about who will take care of him during his clients wedding and Amy proposes to take him but Russ says he will humiliate him. Rusty falls during the wedding ceremony, and at the reception, he asks Amy to marry him, but she thinks that the message has been sent by Russ.

When she finds out that Russ had saved and aired the Riley tape, she gets mad at him and leaves disappointed. Russ finally decides to cancel all of his appointments and spend an afternoon walking and driving around the city with Rusty, trying to find out why Rusty is there and what Russ needs to fix from the past. As they drive through a tunnel, Russ recalls a fight he lost with some neighborhood bullies who were abusing a three-legged dog named Tripod. When they emerge from the tunnel and because of Russs recollection, they have traveled back in time to Rustys eighth birthday in 1968. Russ helps Rusty win the fight and save Tripod, but suddenly remembers that because of the fight, his sick mother also came to school for him that day. When they get home, Rustys father angrily shakes and scolds him for getting into trouble and causing his mother more stress. Rusty cries and his father tells him to grow up, rubbing Rustys eyes painfully and causing a lifelong facial tic. Russ tells Rusty that his mother will die before his next birthday, then comforts him. Russ tells Rusty that his father was angry and scared because of the huge responsibility of raising a boy alone.

They go to Skyway Diner and keep talking about the fight, congratulating each other on their birthday. Suddenly a dog comes in and goes directly to Rusty. They hear a man call the dog Chester, they both follow him and find out that the red plane is Russs plane and that the man with the dog is an older Russ and that Russ would, in late middle age, own planes, have Amy as his wife and mother of his children, and a dog named Chester. Realizing that Rustys appearance was meant to change his ways rather than the other way around, Russ returns to his time, arranges plans to see his dad, buys tickets to Hawaii for Janet, and, with a puppy, returns to Amy, who invites him into her home.

==Cast==
* Bruce Willis as Russ Duritz
* Spencer Breslin as Rusty Duritz
* Emily Mortimer as Amy
* Lily Tomlin as Janet
* Chi McBride as Kenny
* Juanita Moore as Kennys Grandmother
* Jean Smart as Deidre Lefever
* Dana Ivey as Dr. Suzanne Alexander
* Reiley McClendon as Mark
* Steve Tom as Bruce, the Lawyer
* Larry King as himself
* Jeri Ryan as herself
* Nick Chinlund as himself
* Matthew Perry as Mr. Vivian (uncredited)
* Daniel von Bargen as Sam Duritz
* Melissa McCarthy as Skyway Diner Waitress
* Elizabeth Arlen as Gloria Duritz
* Zach Redo as James
* Brian McGregor as Vince
* Brian Tebbits as Herbert
* Brian McLaughlin as George

==Release==

===Box office=== The Patriot, The Perfect Storm and Scary Movie.

===Critical reception=== average score of 45, based on 32 reviews, which represents "mixed or average reviews".  Film critic Roger Ebert of the Chicago Sun-Times gave the movie 3 stars out of 4, observing that "Disneys The Kid is warm-hearted and effective, a sweet little parable that involves a man and a boy who help each other become a better boy, and a better man. Its a sweet film, unexpectedly involving, and shows again that Willis, so easily identified with action movies, is gifted in the areas of comedy and pathos: This is a cornball plot, and he lends it credibility just by being in it." 

===Awards and nominations===
The Kid was nominated for three awards, winning one. 

{| class=wikitable
! Association
! Category
! Nominee
! Result
|-
| The Saturn Awards Best Performance by a Younger Actor
| Spencer Breslin
|  
|- Young Artist Awards
|  
| Spencer Breslin
|  
|- Best Family Feature Film – Comedy
|
|  
|}

==References==
 

==External links==
*   
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 