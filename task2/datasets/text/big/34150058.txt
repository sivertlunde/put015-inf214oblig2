The Callahans and the Murphys
{{Infobox film
| name = The Callahans and the Murphys
| caption =
| image =
| director = George W. Hill
| story = Frances Marion Kathleen Norris Ralph Spence
| screenplay =
| adaptation =
| starring = Marie Dressler Polly Moran
| cinematography = Ira H. Morgan
| editing = Hugh Wynn
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime =
| language = Silent (English intertitles)
| country = United States
| budget =
}}
The Callahans and the Murphys is a 1927 comedy silent film directed by George W. Hill.  The film was based on a novel by Kathleen Norris, and was the first of several MGM films to star Marie Dressler and Polly Moran.

The film was released on June 18, 1927, but subsequently withdrawn from distribution by MGM after protests were lodged by Irish American|Irish-American organizations. The film is now presumed to be a lost film.   

== Plot ==
Mrs. Callahan (Dressler) and Mrs. Murphy (Moran), are a couple of feuding tenement housewives working to keep control of their many children.  Dan Murphy (Gray) falls in love with Ellen Callahan (ONeill), and then later disappears after Ellen is pregnant. Mrs. Callahan (Dressler) decides to adopt the baby to save her daughters reputation, but later finds out that the baby is not illegitimate after all.

== Cast ==
*Marie Dressler as Mrs. Callahan
*Polly Moran as Mrs. Murphy
*Sally ONeil as Ellen Callahan
*Lawrence Gray as Dan Murphy
*Eddie Gribbon as Jim Callahan
*Frank Currier as Grandpa Callahan
*Gertrude Olmstead as Monica Murphy
*Turner Savage as Timmy Callahan
*Jackie Combs as Terrance Callahan Anne Shirley as Mary Callahan (as Dawn ODay)
*Monty OGrady as Michael Callahan
*Tom Lewis as Mr. Murphy

==Crew==
*Cedric Gibbons - Art Director David Townsend - Art Director

==See also==
*List of lost films

==References==
 

== External links ==
*  
*  
*  
* 

 
 
 
 
 
 
 
 
 
 
 
 