Ooha (film)
{{Infobox film
| name           = Ooha
| image          = 
| caption        = Poster Ooha Vikram Vikram
| director       = Sivala Prabhakar
| writer         = Sivala Prabhakar
| producer       = Thalluri Veeralakshmi Narayana S. Durga Prasad
| cinematography = Relangi Ravindranath
| editing        = Trinath
| dialogues      = 
| music          = J. V. Raghavulu
| distributor    = Iyyapu Kurma Rao
| studio         = Sri Sairathna International
| released       = 12 January 1996
| runtime        = 136 mins Telugu
| country        = India
}}
 Telugu romantic Vikram and Ali in the leading roles, while Subhashri plays a supporting role. 

== Cast == Ooha as Ooha Vikram
* Ali
* Subhashri
* Kaikala Satyanarayana
* P. L. Narayana
* M. S. Narayana
* Ironleg Sastri

== Soundtrack ==
{{Infobox album
| Name        = Ooha
| Lontype     = 
| Type        = Soundtrack
| Artist      = J. V. Raghavulu
| Producer    = J. V. Raghavulu Feature film soundtrack Telugu
| Last album  = 
| This album  = 
| Next album  = 
}}

The soundtrack album was composed by J. V. Raghavulu, while S. P. Balasubrahmanyam sang all five songs. 

Tracklist
{{track listing
| extra column    = Singer(s)
| lyrics_credits  = 
| total_length    = 

| title1       = Chikkindi Chukkalloni
| length1      = 

| title2       =Idi Akaliyugamu 
| length2      = 

| title3       = Siri Siri Navvula

| title4       = Ooha

| title5       = Vedekke Vennello
}}

== References ==
 

 
 
 