Taiyō o Nusunda Otoko
{{Infobox film
| name           = Taiyō o Nusunda Otoko
| image          = Taiyō o Nusunda Otoko.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Kazuhiko Hasegawa
| producer       = Mataichiro Yamamoto
| writer         = Leonard Schrader (story & screenplay)  Kazuhiko Hasegawa (screenplay)
| narrator       =
| starring       = Kenji Sawada Bunta Sugawara
| music          = Takayuki Inoue Tatsuo Suzuki
| editing        = Akira Suzuki
| studio         = Kitty Films
| distributor    = Toho
| released       = October 6, 1979
| runtime        = 147 min.
| country        = Japanese
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Taiyō o Nusunda Otoko (太陽を盗んだ男), also known as The Man Who Stole the Sun, is a 1979 satirical film from Japan, directed by Hasegawa Kazuhiko and written by Leonard Schrader.

==Plot== hijack of one of his schools buses during a field trip.  Along with a police detective, Yamashita (Bunta Sugawara), he is able to overcome the hijacker and is publicly hailed as a hero.
 public lavatory and phones the police and demands that Yamashita take the case.  Since Makoto speaks to the police through a voice scrambler, Yamashita is unaware that Makoto is behind the whole thing.

Makoto manages to extort the government into showing baseball games without cutting away for commercials.  Flush with success, he follows radio personality "zero"s suggestion to use the real bomb to extort the government into allowing the Rolling Stones to play in Japan (despite being barred from doing so due to Keith Richards being arrested for narcotics possession).  Eventually Makoto and Yamashita clash, but Makoto may die of radiation poisoning before he can see his plan through to its conclusion.

==Cast==
* Kenji Sawada as Makoto Kido
* Bunta Sugawara as Inspector Yamashita
* Kimiko Ikegami as Zero Sawai
* Kazuo Kitamura as Tanaka
* Shigeru Kôyama as Nakayama
* Kei Satō as Dr. Ichikawa

==Themes==
Many elements of the film are similar to  —namely, the satirical treatment of the proliferation of nuclear weapons.  The films specific area of satire is nuclear terrorism, which, as in the previous film, was a subject largely considered unsatirizeable.  Several scenes in the film are considered controversy|controversial, such as a moment where Makoto uses scraps of plutonium metal to poison people in a public swimming pool.  The film had a particular resonance for Japanese audiences; while Japan does use nuclear power, the country has long held against maintaining a nuclear arsenal especially in the aftermath of the bombings of Hiroshima and Nagasaki, Nagasaki|Nagasaki.

Much of the first hour of the films running time is taken up with a highly technical depiction of Makoto building his homemade nuclear weapon, although key steps in the bomb-making process have apparently been omitted in the name of public safety.

The film won the Tokyo Blue Ribbon Award for Best Film of the Year in 1980, and was a critical and financial success in Japan on its release.  It has only been released outside of Japan on home video.
 American film The Manhattan Project concerned a highly intelligent young man who makes his own atomic weapon.

==Awards==

===Wins=== Japan Academy Prize 
* Best Film - Hochi Film Awards
* Best Actor - Kenji Sawada, Hochi Film Awards
* Best Japanese Director - Kazuhiko Hasegawa, Kinema Junpo Awards
* Best Director - Kazuhiko Hasegawa, Mainichi Film Award
* Readers Choice Award - Kazuhiko Hasegawa, Mainichi Film Award
* Best Film, Yokohama Film Festival
* Best Director - Kazuhiko Hasegawa, Yokohama Film Festival

===Nominations===

====Japan Academy Prize====
* Best Film
* Best Actor - Kenji Sawada
* Best Director - Kazuhiko Hasegawa
* Best Art Direction - Yoshinaga Yokoo
* Best Cinematography - Tatsuo Suzuki
* Best Lighting - Hideo Kumagai
* Best Sound - Kenichi Benitani

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 