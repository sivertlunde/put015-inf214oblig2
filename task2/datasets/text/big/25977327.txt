Ee Sabdam Innathe Sabdam
{{Infobox film
| name           = Ee Sabdam Innathe Sabdam
| image          = Ee Sabdam Innathe Sabdam.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = P. G. Viswambaran
| producer       = K. P. Kottarakara John Paul
| narrator       =  Rohini
| Shyam
| cinematography = B. Vasanthkumar 
| editing        = G. Venkitaraman
| studio         = 
| distributor    = 
| released       =  
| runtime        = 127 min
| country        =  
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Ee Sabdam Innathe Sabdam is a 1985 Malayalam film made in Cinema of India|India, directed by P. G. Viswambaran and starring Mammootty. In this movie, Mammootty plays the role of a doctor who becomes a vigilante when his wife is raped and murdered by college students.   

==Plot==
A group of boys stalks and harasses their neighbor Dr. Ramachandran and his wife, Sharada, who made complaints against them a few times. Sharada was raped and murdered by them after keeping Dr. Ramachandran as hostage. They also raped his sister, and she falls into mental illness. Ramachandran seeks vengeance and becomes a vigilante against his wifes killers.

==Cast==
*Mammootty as  Dr. Ramachandran 
*Shobana as  Sharada  Rohini as  Pushpa 
*Captain Raju as  Gopinathan  Vijayaraghavan as  Balu  Shivajias  Chandru 
*Ansar Kalabhavan   
* Sreelatha Namboothiri Azeezas  Raveendran 
*Jose Prakash as  Nandan Menon 
*C.I. Paul as  Advocate 
*Jagannatha Varma as  Police Officer 
*Jagathi Sreekumar as  Nanukuttan Pillai

==Soundtrack==  Shyam and lyrics was written by Poovachal Khader. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aaromal nee || K. J. Yesudas || Poovachal Khader || 
|- 
| 2 || Aaromal Nee   || K. J. Yesudas || Poovachal Khader || 
|} 

==References== 
 

==External links==
* 
 
 
 
 

 