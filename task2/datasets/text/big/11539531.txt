Aaghaaz
{{Infobox film
| name                 = Aaghaaz
| image                = Aaghaaz.jpg
| image_size           =
| caption              =
| director             = Yogesh Ishwar
| producer             =D. Ramanaidu|Dr. D. Ramanaidu
| writer               =
| story                = 
| screenplay           = Nikhil Saini
| narrator             = 
| starring             = Sunil Shetty Sushmita Sen Namrata Shirodkar
| music                = Anu Malik
| cinematography       = Shyam K. Naidu
| editing              = K. V. Krishna Reddy
| distributor          = Suresh Productions
| released             =  
| runtime              = 160 mins
| country              = India Hindi
| budget               =
}}
 2000 Indian Telugu movie "Shivaiah", which was also produced by D. Suresh Babu.

==Plot summary==
 
The film is the story of Govind Narang (Sunil Shetty), an honest and chivalrous man from a small village in Punjab. He falls in love with Sudha (Sushmita Sen), but marries Masterjis daughter, Pushpa (Suman Ranganathan), to save her from public ridicule. Pushpa was impregnated by a police officer who refuses to marry her. She does not love Govind and hates Sudha with a passion. Together with her brother, Laxman (Sharad Kapoor), Pushpa tries to expel Sudha from of the village, but is unsuccessful. When Laxman finds out about Pushpas infidelity, he poisons her. After his wifes death, Govind relocates to Mumbai with his sister, Ratna (Shraddha Nigam). He meets beautiful Gitika (Namrata Shirodkar) and they fall in love. One day, as Govind defends the honor of a young girl from the brother of  a hoodlum, Johnny Handsome (Sharat Saxena), he generates hatred and animosity. Johnny is humiliated by Govind and swears vengeance with the help of Sadanand Kutty (Gulshan Grover) and Karim Khan Toofani (Govind Namdeo). On the other hand, Govind befriends Ram Sevak (Alok Nath), and with his help and assistance of a landowner arranges the purchase of a large plot of land so that hawkers and small shop-owners could set up their businesses. But nothing goes according to plan. The plot of land and the owner turn out to be fake, Ram Sevak turns out to be in hand with the gangsters, and Govind gets all the blame; His sister is openly raped, and Govind himself gets seriously wounded, in broad daylight before the very eyes of the people he defended. Govind is then helped by Sudha, and he decides to teach everyone a lesson. He files an FIR against everyone in the neighbourhood who witnessed what happened to his sister except the culprits themseleves. When they are brought to court, he condemns everyone for their attitude, and then tells them to be brave and fight goondaism. Encouraged by this, the entire neighborhood decides to take on the gang. They attack the gangsters with bottles when they come to threaten them in the locality, and then move on to Johnny handsomes office, breaking everything and attacking all the goons. In join Govind and Sudha to attack the gangsters and arrest them. The criminals are sentenced and everything returns to normal. In the end, though on a happy note, Govinds sister marries a close friend of the family, and Govind agrees to marry Sudha.

==Cast==
* Sunil Shetty  as  Govind Narang
* Sushmita Sen  as  Sudha
* Namrata Shirodkar  as  Gitika
* Suman Ranganathan  as  Pushpa, Govinds Wife
* Shraddha Nigam  as  Ratna
* Akshay Anand  as  Harish Patel
* Asrani  as  Gullu, Gay
* Rakesh Bedi  as  Dilip Roy
* Sudhir Dalvi  as  Pushpas Father
* Gulshan Grover  as  Sadanand Kutty
* Sharad Kapoor  as  Laxman
* Anupam Kher  as  Balraj Nanda
* Viju Khote  as  Deshpande
* Johnny Lever  as  Rajni Deva
* Govind Namdeo  as  Karim Khan Toofani
* Alok Nath  as  Ram Charan Shukla
* Yunus Parvez  as  Dheerajlal
* Achyut Potdar  as  The Judge
* Rajesh Puri  as  Professor Pillai
* Shiva Rindani  as  Suku
* Asha Sachdev  as  Woman Summoned at Court
* Sharat Saxena  as  Johnny Handsome Mendoza
* Anjan Srivastav  as  The Ordinary Man
* Mukesh Tiwari  as  Danny Mendoza
* Rajnikanth  as  Himself, in a scene from a movie
* Padmini Kapila  as  Mrs. Balraj Nanda

== Soundtrack ==
{{Track listing
| headline        = Songs
| extra_column    = Playback Sameer
| all_music       = Anu Malik

| title1 = Aaghaaz Karo
| extra1 = Babul Supriyo, Alka Yagnik

| title2 = Dil Dil Dil
| note2  = Female
| extra2 = Hema Sardesai

| title3 = Dil Dil Dil
| note3  = Male
| extra3 = Sonu Nigam

| title4 = Dil Ko Patthar
| extra4 = Kumar Sanu, Alka Yagnik

| title5 = Dosti Ho Gayi
| extra5 = Sonu Nigam, Sunidhi Chauhan

| title6 = Man Tera Mera Man
| extra6 = Babul Supriyo, Alka Yagnik

| title7 = Nau Nau Lakha
| extra7 = Hema Sardesai, Sunidhi Chauhan
}}

==External links==
*  

 
 
 
 
 
 
 
 