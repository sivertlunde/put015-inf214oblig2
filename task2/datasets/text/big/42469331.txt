The Overnighters
{{Infobox film
| name           = The Overnighters
| image          = The Overnighters poster.jpg
| caption        = Theatrical release poster
| director       = Jesse Moss
| producer       = Jesse Moss Amanda McBaine
| writer         = Jesse Moss
| starring       =
| music          = T. Griffin
| cinematography = Jesse Moss
| editing        = Jeff Seymann Gilbert	
| studio         = Mlle End Films Al Di La Films Impact Partners Drafthouse Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $100,022   
}}
The Overnighters is a 2014 American documentary film written, directed and produced by Jesse Moss.  The film premiered in competition category of U.S. Documentary Competition program at the 2014 Sundance Film Festival on January 18, 2014.   It won the Special Jury Award at the festival.  
 Drafthouse Films acquired distribution rights of the film. The film is scheduled to be released on October 24, 2014 in United States and a portion of all box office receipts will be donated to North Dakotas local affordable housing charities.  

The film also premiered at 2014 Miami International Film Festival on March 13, 2014 and won a prize at the festival. 

==Synopsis==
The film depicts the lives of people chasing the dream of high salaries in the North Dakota oil boom, only to discover that affordable housing is almost impossible to find. Much of the focus is on the efforts of local pastor Jay Reinke, who allowed over 1,000 different people to stay at his Williston, North Dakota church over a period of about two years.    

==Reception==
The film received positive response from critics.  Review aggregator   | work = Metacritic | accessdate = 2014-04-12
}} 

Justin Chang in his review for Variety (magazine)|Variety said that it is "  tough-minded, admirably unresolved film."  David Rooney of The Hollywood Reporter gave the film positive review and said that, "A scenario with present-day echoes of The Grapes of Wrath yields perceptive insights into the way we view outsiders."  Katie Walsh from Indiewire in her review said that, "The Overnighters is starkly bleak and devastatingly humane, and an indelible American documentary."  Colin Covert in his review for the Minneapolis Star Tribune gave the film three and half star out of four and said that, "The film features stunning third-act revelations that compel viewers to rethink its characters actions and motivations." 

==Accolades==
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="4"| 2014 Sundance Film Festival
| U.S. Grand Jury Prize: Documentary
| Jesse Moss
|  
|-
| U.S. Documentary Special Jury Award for Achievement for Intuitive Filmmaking
| Jesse Moss
|   
|-
| Miami International Film Festival
| Miami’s Knight Documentary Award
| Jesse Moss  
|   
|-
| Full Frame Documentary Film Festival
| Full Frame Inspiration Award 
| Jesse Moss 
|   
|}
 

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 