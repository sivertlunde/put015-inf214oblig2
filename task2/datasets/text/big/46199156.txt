Bracelets (film)
{{Infobox film
| name = Bracelets 
| image =
| image_size =
| caption =
| director = Sewell Collins
| producer = LEstrange Fawcett 
| writer =  Sewell Collins (play and screenplay)
| narrator =
| starring = Bert Coote   Joyce Kennedy   Harold Huth
| music = 
| cinematography = Percy Strong 
| editing = 
| studio = Gaumont British Picture Corporation
| distributor = Gaumont British Distributors
| released = February 1931 
| runtime = 50 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Bracelets is a 1931 British crime film directed by Sewell Collins and starring Bert Coote, Joyce Kennedy and Harold Huth. A jeweler is targeted by confidence tricksters pretending to be connected with the exiled Russian Royal Family. He manages to turn the tables on them and after collecting the reward for their arrest, uses they money to buy some silver bracelets for his wife to celebrate their wedding anniversary.
 second feature by the large British company Gaumont British Picture Corporation.  It was made at the Lime Grove Studios in Shepherds Bush. The films sets were designed by the French-born art director Andrew Mazzei. The director Sewell Collins wrote the screenplay, adapting his own stage play.

==Cast==
*   Bert Coote as Edwin Hobbett  
* Joyce Kennedy as Annie Moran  
* D.A. Clarke-Smith as Joe le Sage  
* Margaret Emden as Mrs. Hobbett 
* Frederick Leister as Slim Symes  
* Stella Arbenina as Countess Soumbatoff 
* Harold Huth as Maurice Dupont   George Merritt as Director

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 