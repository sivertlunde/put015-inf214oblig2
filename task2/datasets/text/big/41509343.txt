Rewind This!
{{Infobox film
| name           = Rewind This!
| image          = Rewind This.jpg
| caption        = 
| director       = Josh Johnson Panos Cosmatos Freddie Fillers
| writer         = 
| starring       = 
| music          = Josh Freda
| cinematography = Christopher Palmer
| editing        = Christopher Palmer
| studio         = Imperial PolyFarm Productions
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
}}
Rewind This! is a 2013 documentary film directed by Josh Johnson about the impact of VHS on the film industry and home video, as well as about collectors of videotapes.

==Interviewees==
*Atom Egoyan
*Mamoru Oshii
*Frank Henenlotter
*Roy Frumkes
*Charles Band
*Cassandra Peterson
*Jason Eisener
*Lloyd Kaufman
*Drew McWeeny Peter Rowe
*David Schmoeller
*Ben Steinbauer

==Release== SXSW Film Festival, Neuchâtel International Fantastic Film Festival, Fantasia International Film Festival, Melbourne International Film Festival, Strasbourg European Fantastic Film Festival, Telluride Horror Show, Sitges Film Festival, Orlando Film Festival, and several others.

==Reception== SXSW 2013 on Movies.com, writing - "the film finds the perfect mixture of nostalgia and forward thinking, acknowledging the past while taking a bold look at what a world free of physical media will look like."  The Hollywood Reporter called the film, "an entertaining, sometimes enlightening history of the video format; though unsurprisingly small-screen friendly, it will be a crowd-pleaser at fests and in niche bookings."  A review from the Telluride Horror Show noted: "An unbending passion of cinema appreciation pours excitedly from Johnsons interview subjects, and there is a prevailing sense of humor that permits the subject to never take itself too seriously." 

==References==
 

==External links==
* 
* 

 
 
 
 
 