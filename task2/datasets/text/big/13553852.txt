Flash Point (film)
 Flashpoint}}
 
 
 
{{Infobox film
| name = Flash Point 
| image = Flash Point poster.jpg
| caption = Hong Kong film poster
| film name = {{Film name| traditional = 導火綫
| simplified  = 导火线
| pinyin = Dǎo Huǒ Xiàn
| jyutping = Dou6 Fo2 Sin3}}
| director = Wilson Yip
| producer = Nansun Shi Donnie Yen
| writer = Szeto Kam-Yuen Nicholl Tang Lui Leung-wai Fan Bingbing Xing Yu
| music = Chan Kwong-Wing
| cinematography = Cheung Man-Po Cheung Ka-Fai
| distributor = Mandarin Films Distribution Co. Ltd.
| released =  
| runtime = 88 minutes
| country = Hong Kong
| language = Cantonese
| budget = HK$10,000,000
}} Hong Kong Lui Leung-wai triad led by three Vietnamese brothers (played by Chou, Lui and Xing).
 Golden Horse Film Awards.
 collection of films.

==Plot== return of Hong Kong to the Peoples Republic of China, the film opens with brothers Archer, Tony and Tiger discussing a drug deal with Sam, a rival gang leader. Ma Jun, a serious criminal investigations detective (who has been reprimanded for frequently inflicting injury on suspects), along with his team, raids the nightclub for investigation, and winds up fighting against his partner, Wilson, who has been planted as a mole.

The three brothers are later confronted by Sam and his gang, who are impatient over receiving the drugs from the brothers native Vietnam, but they are kept at bay with violence and intimidation. The brothers later threaten the elder leaders of their gang, when they attempt to intervene. Tiger is later assigned to kill Sam in his car, but the plan fails when Wilson intervenes. While in hospital, Sam agrees to testify in court against the three brothers. On the night of their mothers birthday, the brothers plan to recover their drug money, but as the heist is being performed, Archer is arrested by Hong Kong police, while Tony and Tiger discover that Wilson is a mole and attempt to get rid of him.

While in court, Archer is forced to turn in all of his travel documents, which will prevent him from fleeing Hong Kong before his hearing. Tiger and Tony brutally murder Sam along with several other witnesses and crime figures crucial to the police investigation. After their first plan to kill Wilson fails, Tony and Tiger disguise themselves as janitors to sneak into the hospital where he is being protected by police. Ma and Tiger both end up being in the same elevator and Tiger attempts to kill Wilson with a silenced pistol once he appears, but Ma is aware of him and at the last moment engages him in a fierce struggle.  After being disarmed, Tiger attempts to escape, but Ma gives chase until he corners him at an outdoor restaurant. Tiger uses a little girl as a hostage and then severely injures her by throwing her through the air onto the concrete. Ma furiously engages him in combat and proceeds to beat him to death in front of the crowd. Tony, having kidnapped Wilsons pregnant girlfriend, Judy, threatens to kill her, if Wilson, now a sole witness, testifies in court. During the court hearing, Wilson refuses to testify, and the case is dismissed for lack of evidence. Wilson later attempts to rescue Judy, but is captured by Tony and his gang.

Once Archer walks free, Ma holds him captive, and calls his brother, Tony, for an exchange of hostages, leading to a confrontation in a Chinese village. Ma singlehandedly takes on the remaining gangsters, engages in a gruelling fight with Tony and strangles him to unconsciousness to finally arrest him.

==Cast==
*Donnie Yen as Detective Sergeant Ma Jun
*Louis Koo as Wilson
*Collin Chou as Tony
*Ray Lui as Archer Sin
*Fan Bingbing as Judy
*Xing Yu as Tiger
*Kent Cheng as Inspector Wong
*Xu Qing as Madam Lau
*Teresa Ha as Tonys mother
*Law Lan as Ma Juns mother
*Tony Ho as Cannon
*Irene Wang as Cindy
*Timmy Hung as Yeung
*Liang Zhenhui as Deco
*Ben Lam as Sam
*Austin Wai as Four Eyes
*Wilson Tsui as Hero
*Huang Zhiwei as Baldy
*Tanigaki Kenji as Kenji
*Yu Kang as Tonys underling
*Damian Green as Boxer No. 1
*Damon Howe as Boxer No. 2
*Min Yoo as Boxer No. 3
*Drafus Chow as Boxer No. 4
*Dus Luu as Boxer No. 5
*Zen Berimbau as Boxing trainer
*Shimomura Yuji as Sniper
*Fanny Lee as Four Eyes wife
*Siu Hung as Beach swimmer
*Victy Wong as Cop
*Kam Loi-kwan as Fatso
*Chang Kin-yung as Senior police officer
*Sherwin Ming as Little girls father

==Production== Raymond Wong, Mandarin Films Ltd., served as a producer and distributor in Hong Kong.

===Development===
While Dragon Tiger Gate was in post-production, Donnie Yen had announced that plans for a sequel to SPL: Sha Po Lang had already begun, with Mandarin Films set to finance the film. Production was planned to begin in May or June 2006. Yen and his management company later clarified that the team behind SPL would be working together again on a new film, but due to copyright issues, they would not be making a sequel to the 2005 film.    Yen lobbied for an American-based studio to co-produce the new film, tentatively known SPL 2, with Mandarin Films. With financing from abroad, Wilson Yip and Donnie Yen decided to raise the budget of the film to HK$50 million. Yip reportedly intended to have   and Rene Liu were considered as female leads.   

Following the box office success of Dragon Tiger Gate, Yen had announced SPL 2s development, along with plans to make a sequel to Dragon Tiger Gate. SPL 2 was now given another tentative title, Battling the Police Force (强战型警), and Yen also announced that the film would be written in a style similar to SPL. Yen had hoped the film would take part in the 2007 Cannes Film Festival and scheduled for a Labour Day release in 2007.   

The film was later retitled to Army Breaker (破军), which means destroying thousands of opponents in war. The film was given a budget of US$8 million and production was set to begin in Macau on September 2006.   

===Filming===
The film was now given the working title City Without Mercy, and director Wilson Yip was forced to drop plans of shooting in Macau, due to the lack of extras needed for filming. Yip needed 250 extras for one scene, and decided to return to Hong Kong with his crew for filming.    Filming in Hong Kong began on 13 November 2006, and the cast for the film had been finalised.    Filming finally ended in March, and the film was now given its former title, Flash Point (導火線). After filming, it was revealed that the film had gone over budget.   

===Fight choreography=== wushu and taekwondo. This method of fighting is something Yen considers to be the greatest development of martial arts in his lifetime. Yen admits that his challenge during filming was to communicate the essence of these techniques in the dramatics of the film. To ensure that the action scenes stood out, Yen not only hired martial artists from around the globe but also invited Collin Chou to join the cast.   

==Festivals==
A promotional video highlighting Flash Point was shown at the 2007 Cannes Film Festival and the 2007 Hong Kong Film Awards. It also premiered at the "Midnight Madness" program of the 2007 Toronto International Film Festival in September that same year, at the Ryerson University Theater, with director Wilson Yip attending the premiere.

===Awards===
{| class="wikitable"
|-
!Award !! Category !! Winner/Nominee !! Result
|- 27th Hong Kong Film Awards Best Action Choreography Donnie Yen Won
|- Golden Horse 2008 Golden Horse Film Awards Best Action Choreography Donnie Yen Won
|-
|}

==Reception==

===Box office=== Mandarin Films.  While in Hong Kong, it grossed over HK$3.67 million (US$470,047) on its opening weekend. At the end of its box office run in Hong Kong, the film had grossed a total of over HK$9 million.

==Distribution==
South African film distributor Distant Horizon acquired North American rights to the film. The films release in all English-speaking territories is handled by The Weinstein Company which released the film on DVD as part of their Dragon Dynasty collection.

==See also==
* Donnie Yen filmography
* List of Dragon Dynasty releases

== References ==
 
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 