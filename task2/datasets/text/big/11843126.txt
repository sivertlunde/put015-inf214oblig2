Urban Justice
{{Infobox film
| name = Urban Justice
| image = Urban Justice.jpg
| caption = DVD cover
| director = Don E. Fauntleroy
| producer = Steven Seagal
| writer = Gilmar Fortis II
| starring = Steven Seagal Eddie Griffin Carmen Serano Danny Trejo
| music = Peter Meisner
| cinematography = Don E. Fauntleroy
| editing = Scott Conrad Steamroller Pictures
| distributor = Sony Pictures Home Entertainment
| released =  
| runtime = 96 minutes
| country = United States
| language = English
| budget = 12 million
}}
Urban Justice (released in the United Kingdom as Renegade Justice) is a 2007 American action film directed and cinematographed by Don E. Fauntleroy, and also produced by Steven Seagal, who also starred in the lead role. The film co-stars Eddie Griffin and Carmen Serano. The film was released on direct-to-video|direct-to-DVD in the United States on November 13, 2007.

During beginning of production, Screen Gems was considering a theatrical release for this film. It would have been Seagals first theatrical release film since 2002s Half Past Dead.

==Plot==
There is a part of Los Angeles where there are two rival gangs that have been at war with each other: the Hyde Park Gang based on MS-13, led by El Chivo (Danny Trejo), and the East Side Gangsters based on the Crips, led by Armand Tucker (Eddie Griffin). Vice cop Max Ballister (Cory Hart) is called out one night by someone that he thinks is his informant Gary Morrison (Jade Yorker). Before Max left, he is enjoying his romantic dinner with his wife. Little did he know, that would be the last time he would see her. When Max reaches his destination, someone pulls up to him and fires a single shot that kills him, leaving his wife Linda (Liezl Carstens) widowed.

Maxs angry father Simon Ballister (Steven Seagal), does not look too amused when he is first appears in this great film. Standing still in the distance with a stone cold face, Simon looks like he is out for justice.  He, who is a martial arts expert (it is implied that he is ex-CIA), moves to the neighborhood that Max was killed in, at address 1001 North Central, and he starts investigating, with the help of his new landlady Alice Park (Carmen Serano), who runs MGs Grand Liquors. Frank Shaw (Kirk B. R. Woller) is the detective who is heading the investigation. And what no one knows is that Shaw, who is a dirty cop, has formed a partnership with Tucker.

As Simon investigates, he starts leaving a trail of beaten gangsters. However, Gary gives Simon a lead on El Chivos gang. El Chivo runs most of the booking joints that Max went after, and El Chivo has a private club called the Anodyne. Simon goes to the Anodyne, and Chivo tells him that maybe it was Tucker who ordered the hit on Max.                                                                                                                                                                 

Later, a pair of East Side Gangsters chase Simon via car. After a long chase, Simon causes them to crash their car. One of the gangsters is dead, and Simon, with a gun, asks the other one, Garys brother Isaiah Morrison (Trantario TJ Jones), who sent them. Isaiah says Tucker sent them. Isaiah talks to Tucker, and Tucker tells Isaiah to kill Simon, or everyone Isaiah cares about will die.

On the next morning, Gary is chased by a group of four racists, and just as theyre about to start beating him up, Simon shows up and leaves the four racists in a bloody heap on the ground, and then calls for an ambulance. In Simons car, Simon asks Gary if it was Isaiah who fired the shot that killed Max, but Gary is not saying anything. Just before Gary gets out of the car, Simon plants a tracking device on him.

Later that night, Simon uses the tracking device to follow Gary to a warehouse, and uses some electronic equipment to eavesdrop on the conversation thats taking place inside. It seems to be a meeting between the gangs. After listening some, Simon gets out and plants some more tracking devices. Shaw is there. And then, all of a sudden, gunfire erupts.

When its over, the men from Chivos gang are dead at the hands of some dirty cops who work for Shaw, and Tucker and Shaw take the money and drugs. Simon gets back in his car, and uses the tracking devices to follow them. Simon follows them to what looks like another warehouse. Simon silently breaks in and finds the cocaine. Some of Tuckers men follow Simon back to MGs Grand Liquors, where Simon still has an apartment. Simon uses his tracking device, and notices that the car that the gangsters are in is right outside. Simon and Alice are in his apartment, and Simon tells Alice to hide.

The gangsters start coming into the apartment, and Simon starts killing them with a Heckler & Koch G36|G36. After killing the gangsters who broke into the apartment, Simon tells Alice to follow him. On the stairway, Simon takes some more gangsters. Simon and Alice get outside, and Simon starts killing more gangsters. And then Simon gets shot by Tucker, who escapes. Alice takes Simon home with her. He wakes up from being temporarily knocked out from the gunshot wound and he meets her cousin Winston (Diego Lopez), who works at USC Medical Center. Simon chokes him thinking he is an East Side Gangster however realizes he is not and releases him.
On the next day, Simon goes to Isaiahs house. Simon beats up Isaiah and two of his friends. The murder weapon happens to be at Isaiah and Garys house and Simon, being the genius he is, knew right away that the gun he found was the gun that killed his son. When Gary arrives, Simon forces Gary to admit that Shaw is the man behind everything. It was Shaw who killed Max. Gary explains that he was afraid to talk because Shaw threatened to kill his family. Shaw killed Max because Max witnessed a deal between Shaw and Tucker.

Later that night, Shaw and Tucker meet with a drug dealer from New York at a warehouse. Simon silently pulls up to the warehouse. Simon gets out, and starts killing gangsters. Shaw kills the New York drug dealer, while Simon continues to kill gangsters. Simon is also targeting Shaws men.

After killing all of Tuckers men and all of Shaws men, Simon confronts Shaw. Meanwhile, Simon and Shaw engage in a brutal one sided fight which leaves Shaw badly injured. As Simon has Shaw in a choke hold after giving him a vicious beating, Tucker walks in with a gun and stands there watching as Shaw repeatedly orders him to shoot Simon. Tucker ignores his commands. As Shaw moves towards unconsciousness, Simon fatally snaps his neck.

As Simon stands before him unarmed, Tucker sarcastically remarks, "Now I know you aint dumb enough to bring a fist to a gun fight" The instant Tucker finishes his words, Simon smoothly disarms Tucker, proclaiming "I am." Simon then turns the gun on Tucker stating that Tucker probably believes that he will kill him. However, he tells Tucker that his "beef" is not with him, hands Tucker the gun and walks away. Afterwards, Tucker is visibly impressed by Simons cool display and delivers the final line of the movie saying, "Thats gangster." Simon walks out of the warehouse after finishing his business. The song "Tonights Guna Be Murder" is played as Simon is walking out to make him look like the biggest badass on the face of the earth.

==Cast==
* Steven Seagal as Simon Ballister
* Eddie Griffin as Armand Tucker
* Carmen Serano as Alice Park
* Cory Hart as Max Ballister
* Liezl Carstens as Linda
* Kirk B. R. Woller as Frank Shaw
* Mary Evans as Irene
* Al Staggs as Priest
* Jade Yorker as Gary Middleton
* Jermaine Washington as Rasheed
* Brian Lucero as Benny
* Danny Trejo as El Chivo
* Diego Lopez as Winston
* Grady McCardell as Dwight Morris
* Brett Brock as Watch Sergeant

==Production== Los Angeles, California and Albuquerque, New Mexico, on November 26 and December 24, 2006.

==Home media== Region 2 in the United Kingdom on 5 November 2007, and also  Region 1 in the United States on November 13, 2007, it was distributed by Sony Pictures Home Entertainment.

==External links==
*  

 
 
   
 
   
 
 
 
 
 
 
 
 
 