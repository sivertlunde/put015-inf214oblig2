Our Brand Is Crisis
 
{{Infobox film
| name           = Our Brand Is Crisis
| writer         = Rachel Boynton
| starring       =
| music          = Marcelo Zarvos
| director       = Rachel Boynton
| producer       = Rachel Boynton Sanders Goodstein Robert Kravis Steven Shainberg
| distributor    = Koch-Lorber Films
| released       =  
| runtime        = 85 minutes Spanish English English
| country        = United States
}} 2002 Bolivian presidential election. The election saw Gonzalo Sánchez de Lozada elected President of Bolivia ahead of Evo Morales.

The film is distributed by Koch-Lorber Films.
 current U.S. administrations approach to selling the war in Iraq are staggering."

==Cast==
* Mauricio Balcazar as himself (Gonis Press Advisor)
* James Carville as himself (GCS Strategist)
* Tad Devine as himself (GCS Advertising Consultant)
* Mark Feierstein as himself (GCS Pollster)
* Stan Greenberg as himself (GCS Pollster)
* Carlos Mesa as himself (VP Candidate)
* Evo Morales as himself (Opposing Candidate)
* Jeremy Rosner as himself (GCS Pollster)
* Gonzalo Sánchez de Lozada as himself (aka "Goni"; Bolivias Presidential Candidate)
* Robert Shrum as himself
* Tal Silberstein as himself (GCS Management Consultant)
* Manfred Reyes Villa as himself (Opposing Candidate)
* Amy Webber as herself (GCS Associate)

==Awards and reception==
* The film won the Charles E. Guggenheim Emerging Artist Award at the 2005 Full Frame Documentary Film Festival;
* Boynton was nominated for the Truer Than Fiction Award at the Independent Spirit Awards;
* The film shared the International Documentary Associations top prize for Best Feature Documentary in 2005. 
* Official Selection at the 2005 South By Southwest Festival 
* Official Selection at the 2005 Edinburgh International Film Festival

==Remake==
 
It was announced in 2007 that actor George Clooney will be producing a remake of Our Brand Is Crisis. 

==References==
 

== External links ==
*  
*  
*  
*  
*  
*   at SourceWatch

 
 
 
 
 