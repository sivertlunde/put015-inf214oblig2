Aloma of the South Seas (1926 film)
{{Infobox film
| name = Aloma of the South Seas
| image = Aloma of the South Seas.jpg
| caption = 1926 lobby poster
| director = Maurice Tourneur
| producer = E. Lloyd Sheldon Maurice Tourneur Adolph Zukor Jesse L. Lasky
| writer = James Ashmore Creelman
| starring = Gilda Gray Percy Marmont Warner Baxter
| music = Robert Hood Bowers 
| cinematography = Harry Fischbeck
| editing = E. Lloyd Sheldon
| studio = Famous Players-Lasky
| distributor = Paramount Pictures
| released =  
| runtime = 90 minutes
| country = United States
| language = Silent film  English intertitles
| budget = 
| gross = United States Dollar|US$ 3 million
}}
Aloma of the South Seas is a 1926 silent film starring Gilda Gray as an erotic dancer, filmed in Puerto Rico and Bermuda, and based on a 1925 play of the same title by John B. Hymer and LeRoy Clemens.   Grossing $3 million in the U.S. alone, this was the most successful film of 1926 and the fourth most successful film of the 1920s. 

==Preservation status==
The film is now considered to be a lost film.  

==Remake== Aloma of Jon Hall.

== Cast ==
*Gilda Gray as Aloma
*Percy Marmont as Bob Holden
*Warner Baxter as Nuitane
*William Powell as Van Templeton
*Harry T. Morey as Red Malloy
*Julanne Johnston as Sylvia
*Joseph W. Smiley as Andrew Taylor
*Frank Montgomery as Hongi
*Michelette Burani as Hina (credited as Madame Burani)
*Ernestine Gaines as Taula
*Aurelio Ciccia as Sailor

==See also==
*List of lost films

== References ==
 

== External links ==
* 
* 
* 
* 
* 
* (for movies in ABC order click SHOWS link)

 

 
 
 
 
 
 
 
 
 
 
 
 


 