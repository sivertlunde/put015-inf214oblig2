Robinson Crusoe (1954 film)
{{Infobox film
| name        = Adventuros de Robinson Crusoe
| image       = RobinsonCrusoe1954.jpg
| caption     = Robinson Crusoe Spanish poster
| writer      = Luis Alcoriza Luis Buñuel Hugo Butler Daniel Defoe (novel) Jaime Fernández Felipe de Alba
| director    = Luis Buñuel
| producer    = Oscar Dancigers Henry F. Ehrlich
| distributor = Producciones Tepeyac
| released    =    
| runtime     = 89 min. Spanish
| Anthony Collins
| budget      = US$350,000
}}

Robinson Crusoe ( ; also known as Adventures of Robinson Crusoe) is a 1954 Mexican film by director Luis Buñuel, based on the novel Robinson Crusoe by Daniel Defoe. Both English and Spanish versions were produced.

Lead actor Dan OHerlihy, playing Crusoe, was nominated for the 1955 Academy Award for Best Actor. 

==Plot== collect slaves, a storm forces him to abandon ship. He swims alone to a deserted island somewhere in the Atlantic Ocean on September 30, 1659. 

To his delight, the abandoned ship turns up on an offshore rock, allowing him to salvage food, tools, firearms and other items before it sinks. He herds goats, hunts game, makes clothes, and builds a home, with only the company of the dog, Rex, and the cat, Sam, his only fellow castaways. Crusoe lets Sam and her kittens run wild. When Rex dies of old age in 1673, Crusoe nearly goes insane from loneliness.
 Jaime Fernández) make a break for it, pursued by two cannibals. He knocks out one and shoots the other; when the first one regains consciousness, the escapee kills him with Crusoes knife. Crusoe takes the man back to his stockade. 
 leg irons on him. The next day, however, Crusoe relents and takes them off. He comes to trust his new companion completely.

After 28 years, Friday saves Crusoe from a cannibal sneaking up behind him. Seeing a large group, they flee back to their stockade. The cannibals, however, are driven off by white men with guns. Captain Oberzo (Felipe de Alba) and his bosun (Chel López) are the victims of a mutiny; the mutineers have landed to get fresh water and to maroon the two. Crusoe and Friday rescue the men and get away undetected. Friday then goes to the leader of the mutiny (Emilio Garibay), offering him a basket of fruit, but the mutineers are more interested in the necklace of gold coins (salvaged from Crusoes ship) he is wearing. Friday leads the greedy men to the stockade.  There, Crusoe, Friday, Oberzo, and the bosun capture them. Oberzo regains control of his ship. At Crusoes suggestion, Oberzo agrees to let the mutineers remain on the island instead of being sentenced to die in the gallows. Crusoe leaves them his tools and instructions on how to survive.

Crusoe leaves for home with Friday, having spent 28 years, two months, and 19 days on the island.

==Cast==
*Dan OHerlihy as Robinson Crusoe / Crusoes father Jaime Fernández as Friday
*Felipe de Alba as Captain Oberzo
*Chel López as Bosun
*José Chávez (actor)|José Chávez as Pirate
*Emilio Garibay as Leader of the Mutiny

==Production==
Luis Buñuel began working with screenwriter Phillip Ansel Roll in 1950 on a script. Pryor, Thomas M. "Mexican Will Film Robinson Crusoe." New York Times. July 5, 1952.  Mexican-Canadian Alex Phillips was hired as the cinematographer. Mindlin, Jr., Michael. "Crusoe Comes to Life in the Mexican Tropics." New York Times. September 14, 1952.  More than 300 actors were considered for the lead role. OHerlihy recalled that the producers of the film wanted Buñuel to use Orson Welles for the role, with Buñuel refusing, saying he was too loud and too fat.  They arranged a screening of Welles Macbeth (1948 film)|Macbeth to show how a bearded Welles would look, but Buñuel demanded OHerlihy, who played Macduff, for the lead role.  Negotiations with Herlihy occurred at the last moment and under strict conditions of secrecy to prevent a wealthier motion picture studio from rushing a similar story into production. 
 La perla. grip when discovered by Buñuel. He spoke no English, and learned it on the set much as his character did. 
 Manzanillo in Diodoquin and aralen to guard against dysentery and malaria, respectively. A security squad of local Manzanillans kept snakes, wild boar, and other dangerous animals at bay with guns and machetes. Interior shots were filmed on Sound Stage 3 at Tepeyac Studios in Mexico City. The negatives were flown to Hollywood, where they were developed and color-corrected.  Three times a week, Buñuel, Phillips, the producers, OHerlihy, and others watched the rushes in a local movie theater. Editing and scoring were also done in Mexico City. 

According to OHerlihy, Buñuel saw the central theme of the story as that of a man who ages and almost loses his mind, only to find that companionship is his salvation.  OHerlihy also said that the script was used only for the first week of shooting.  Afterward, Buñuel and OHerlihy would merely discuss the story and how OHerlihy should act and react. 

The producers initially believed the film would be ready for distribution by December 1952,  but numerous delays upset these plans.  On October 14, 1953, the producers announced that United Artists had signed an agreement for worldwide distribution rights to the film.  The film premiered in New York City on August 4, 1954, at the Normandie Theatre. 

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 