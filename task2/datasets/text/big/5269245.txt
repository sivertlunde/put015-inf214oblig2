The World, the Flesh and the Devil (1959 film)
{{Infobox film
| name           = The World, the Flesh and the Devil
| image          = World Flesh Devil 1959.jpg Theatrical release poster
| director       = Ranald MacDougall
| producer       = Sol C. Siegel George Englund Harry Belafonte (uncredited)
| writer         = Ranald MacDougall
| based on       = novel The Purple Cloud by M. P. Shiel story End of the World by Ferdinand Reyher
| screenplay     = Ranald MacDougall
| starring       = Harry Belafonte Inger Stevens Mel Ferrer
| music          = Miklós Rózsa
| cinematography = Harold J. Marzorati
| editing        = Harold F. Kress
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 95 minutes
| country        = USA
| language       = English
| budget         =$1,659,000  .  gross = $1,085,000 
}}

The World, the Flesh and the Devil is a 1959   by M. P. Shiel  and the story "End of the World" by Ferdinand Reyher.

==Plot==
 s as a weapon, yielding a dust cloud that spread globally and was completely lethal for a five-day period.

Travelling to  ), a white woman in her twenties. The two become fast friends, but Ralph grows distant when it becomes clear that Sarah is developing stronger feelings for him. Despite living in a post-apocalyptic world and despite the fact that Sarah seems unconcerned with their racial difference, Ralph cannot overcome the inhibitions instilled in him in a racist American society. 

Ralph regularly broadcasts on the radio, hoping to contact other people. Eventually, he receives a signal from Europe, indicating there are at least a few other survivors.  Things become vastly more complicated when an ill, white Benson Thacker (Mel Ferrer) arrives by boat. Ralph and Sarah nurse him back to health, but once he recovers, Ben sets his sights on Sarah and sees Ralph as a rival. Ralph is torn by conflicting emotions. He avoids Sarah as much as possible, to give Ben every opportunity to win her affections, but cannot quite bring himself to leave the city. 
 swords into plowshares. And their spears into pruning hooks. Nation shall not lift up sword against nation. Neither shall they learn war any more", from the Book of Isaiah 2:4. He throws down his rifle and goes unarmed to confront Ben, who in turn finds himself unable to shoot his foe. Defeated, he starts walking away. Sarah appears. When Ralph starts to turn away from her, she makes him take her hand; then she calls to Ben and gives him her other hand. Together, the three walk down the street to build a new future together. The film ends not with "The End" but with "The Beginning".

==Cast==
* Harry Belafonte as Ralph Burton 
* Inger Stevens as Sarah Crandall
* Mel Ferrer as Benson Thacker
==Production==
Harry Belafonte was paid $350,000 against 50% of the net profits. Top Stars Strangle Hold on Film Profits Poses New Woe to Beleaguered Studios
Scheuer, Philip K. Los Angeles Times (1923-Current File)   20 July 1958: e3.  

==Box office==
According to MGM records the film earned $585,000 in the US and Canada and $500,000 elsewhere, resulting in a loss of $1,442,000. 

==See also== The Comet", a 1920 short story along the same lines written by W. E. B. Du Bois
*Survival film, about the film genre, with a list of related films
*Five (1951 film)|Five, a 1951 post-apocalyptic film
*Last Woman on Earth, a 1960 film directed by Roger Corman The Quiet Earth (1985), cited as "an unofficial remake from New Zealand" of The World, the Flesh, and the Devil 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 