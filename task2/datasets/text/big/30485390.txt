Rosalie (film)
{{Infobox film
| name           = Rosalie
| image          = Rosalie - 1937 Poster.jpg
| alt            = 
| caption        = 1937 Lobby card
| director       = W. S. Van Dyke
| producer       = William Anthony McGuire
| screenplay     = 
| writer         = William Anthony McGuire
| based on       =  
| starring       = Eleanor Powell Nelson Eddy Frank Morgan  
| music          = Cole Porter
| cinematography = Oliver Marsh
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
|}}

Rosalie is an  . The story involves the romantic entanglements of a princess in disguise and a West Point cadet.

==Plot== Vassar student who is also a princess (Princess Rosalie of Romanza) in disguise, watches a football game. They are attracted to each other and agree to meet in her country in Europe. When Dick flies into her country he is greeted as a hero by the King (Frank Morgan) and finds Rosalie. Rosalie is engaged to marry Prince Paul (Tom Rutherford), who actually is in love with Brenda (Ilona Massey); Dick, not knowing of Prince Pauls affections, leaves the country. The King and his family are forced to leave their troubled country, and Dick and Rosalie are finally re-united at West Point.

==Cast==
* Nelson Eddy as Dick Thorpe
* Eleanor Powell as Rosalie
* Frank Morgan as King
* Edna May Oliver as Queen
* Ray Bolger as Bill Delroy
* Ilona Massey as Brenda
* Billy Gilbert as Oloff
* Reginald Owen as Chancellor
* Tom Rutherford as Prince Paul
* Clay Clement as Captain Banner
* Virginia Grey as Mary Callahan
* George Zucco as General Maroff
* Oscar OShea as Mr. Callahan Jerry Colonna as Joseph
* Janet Beecher as Miss Baker

==Production==
MGMs top tap dancer at the time,  , cinematography by Oliver Marsh, art direction by Cedric Gibbons, and choreography by Albertina Rasch.   Marjorie Lane dubbed the singing voice for Powell. The dance director for the "Cadet routines" was Dave Gould.  
 In the Still of the Night", and "Spring Love is in the Air."  An excerpt from this scene is included in Thats Entertainment! (1974). 

The film "resembles the frothy operettas then so much in vogue, which means that Rosalie lacks much of a plot ... he   managed to compose the memorable In the Still of the Night and Who Knows?." 

The reviewer at allmovie.com called the film an "overproduced musical extravaganza", and noted that "The flimsy plot all but collapses under the weight of Gibbons enormous sets and dance director David Goulds ditto choreography." 

==Songs==
#"Who Knows?" - Dick
#"Ive a Strange New Rhythm in My Heart" - Rosalie
#"Rosalie" - Dick
#"Why Should I Care?" - King Frederic
#"Spring Love is in the Air" - Brenda
#"Close"  
#"In the Still of the Night" - Dick
#"Its All Over But the Shouting" - Dick
#"To Love or Not to Love" - Dick

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 