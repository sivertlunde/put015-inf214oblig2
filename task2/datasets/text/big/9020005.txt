O Costa do Castelo
{{Infobox film
| name           = O Costa do Castelo
| image          = O_costa_do_castelo.jpg 
| director       = Arthur Duarte
| producer       = António de Jesus Marques António César dos Santos
| writer         = João Bastos Fernando Fragoso
| starring       = António Silva (actor)|António Silva Maria Matos Milú Curado Ribeiro Hermínia Silva Teresa Casal
| released       = 1943
| runtime        = 135 mins
| language       = Portuguese
}}
 Portuguese film comedy from 1943, directed by Arthur Duarte, and starring António Silva (actor)|António Silva, Maria Matos, Curado Ribeiro, Milú, Hermínia Silva,the famous Fado singer, and Teresa Casal. It was produced at Tobis Portuguesa.

It stands as one of the most famous Portuguese comedies of all time.

== Plot ==

André (Curado Ribeiro), is a son of a wealthy family who meets a girl called Luisinha (Milu).

Luisinha lives at a small pension house with Rita (Maria Olguim) and Januário (João Silva), who are as parents to her, and with mr. Simplício Costa (António Silva), better known as Costa do Castelo, a lazy but highly talented guitar player who together with (Hermínia Silva) reaches success.

In order to see Luisinha every day, André decides to move into the same pension, offering an assumed name and pretending to be a chauffeur. When all seems to be going according to plan, enter Ms. Mafalda da Silveira (Maria Matos), Andrés aunt, who unmasks her nephew and ends the farse.

André gets injured in a car accident, and takes advantage of his ailment to persuade his aunt with the help of his uncle Simão (Manuel Santos Carvalho), to allow Luisinha to live at her mansion, to attend him. Luisinha ends up moving in and, thanks to her, the mansion knows joy and light again.

One day, Simplício Costa comes to the mansion at the request of Simão and, unexpectedly, he finds the lost love of his youth, gone for over 30 years: Mafalda. All seems perfect, but one person stands before in André and Luisinhas happiness, Isabel de Castelar (Teresa Casal), an ambitious woman coveting Daniels fortune. With considerable effort, Ms. Mafalda and the "Costa do Castelo" manage to join together not only Daniel and Luisinha but also themselves.

== DVD version ==

The DVD version was is based upon a restoration effort conducted by Cinemateca Portuguesa and Museu do Cinema.

== Trivia ==

*"Costa do Castelo" is also the name of a reputed Portuguese advertising, feature film and television producer company, named after the film.
*"Costa do Castelo" is the name of a street in Lisbon.

== References ==

* Photobiography of António Silva, Círculo de Leitores
* "O Costa do Castelo" DVD
* Portuguese Wikipedia article

== External links ==
*  

 
 
 
 
 