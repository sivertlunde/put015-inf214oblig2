U 47 – Kapitänleutnant Prien
{{Infobox film name            = U 47 – Kapitänleutnant Prien  U 47 – Lieutenant Commander Prien image           = Poster U47 - Kapitänleutnant Prien.jpg image_size     = caption        = producer       = Gero Wecker director        = Harald Reinl writer          = Joachim Bartsch and Udo Wolter starring        = Dieter Eppler Dieter Borsche Joachim Fuchsberger Richard Häussler Harald Juhnke Olga Chekhova Horst Naumann Peter Carsten music           = Norbert Schultze cinematography  = Ernst W. Kalinke editing         = Hein Haber studio          = Arca-Filmproduktion GmbH released        = 25 September 1958 runtime         = 87 min. language  German
 country         = West Germany
}}
 1958 black-and-white German war film portraying the combat career of Kriegsmarine World War II U-boat captain Günther Prien, commander of  . It stars Dieter Eppler and Sabine Sesselmann and was directed by Harald Reinl.

==Plot==
 
The film begins shortly after the outbreak of World War II when Günther Prien reports to the Befehlshaber der U-Boote (BdU—supreme commander of the U-boat Arm) Karl Dönitz. Dönitz orders Prien as commander of U-47 to penetrate the Royal Navys primary base at Scapa Flow to inflict as much damage as possible. Prien accomplishes his mission and receives a heros welcome on his return.    

Following these events, pastor Kille, a former schoolmate of Prien, approaches Prien in the need for help. Kille offers refuge to victims of Nazi oppression. Prien initially declines, stating he is a soldier and not involved in politics. The attempt by Killes sister Alwine, who is engaged to Priens first officer Thomas Birkeneck, also fails to convince Prien. 

With the ever increasing intensity of war Prien is plagued by his bad conscience, asking himself if his attitude is correct. A very dramatic incident occurs following the sinking of a freighter. U-47 rescues two shipwrecked. These turn out to be German refugees who are trying to escape from Nazi Germany. The two chose to remain at sea over the prospect of returning to Germany. 

Prien finally believes, that as a figure of public interest, that he could influence and change something for the better. He goes to visit the imprisoned pastor Kille. Prien promises him help, not realizing that their conversation is overheard. Thus Prien himself gets into the focus of the Gestapo, the Secret State Police.  

The visit remains without consequences for Prien. U-47 is sunk on his next war patrol. Prien and his cook (Der Smut) are rescued by a British ship, which is then sunk by another German submarine under the command of Priens former first officer, Birkeneck. Priens hat is retrieved from the sea putting Birkeneck in a state of shock. Subsequently he fails to give the order to dive on time and his boat is sunk by attacking enemy aircraft. 

==Historical accuracy==
The story is loosely based on Priens combat record and command of submarine  . His most famous exploit was the sinking of the British battleship   at anchor in the Home Fleets anchorage in Scapa Flow.  His achievements as U-boat commander were highly idolized by Joseph Goebbels  Ministry of Public Enlightenment and Propaganda. 

Every character depicted in the film, except for Prien and Admiral Dönitz, who is not mentioned by name in the film, is fictitious. Priens portrayal as an active member of the German resistance is also fictitious. U-47 s destruction and Priens death is another invention of the movie makers.  To date, there is no official record of what happened to the U-47 or her 45 crewmen.     The submarine shown in the film was the Spanish submarine G-7, formerly the   . 

==Cast==
*Dieter Eppler as Captain Günther Prien Hermanni 2009. p. 223. 
*Sabine Sesselmann as Mrs. Ingeborg Prien 
*Joachim Fuchsberger as Oberleutnant Thomas Birkeneck 
*Harald Juhnke as Der Smut—the cook
*Uta Hallant as Alwine
*Olga Chekhova as Die Fürstin—the princess 
*Richard Häussler as the German U-Boat Fleet Commander
*Raidar Müller-Elmau as Leutnant Bluhm
*Ernst Reinhold as Leutnant Raufuss
*Matthias Fuchs as Jörg
*Horst Naumann as the Ingenieur—engineer
*Dieter Borsche as pastor Kille

==References==
Citations
 
Bibliography
 
* Busch, Rainer & Röll, Hans-Joachim (2003). Der U-Boot-Krieg 1939-1945 - Die Ritterkreuzträger der U-Boot-Waffe von September 1939 bis Mai 1945 (in German). Hamburg, Berlin, Bonn Germany: Verlag E.S. Mittler & Sohn. ISBN 3-8132-0515-0.
* Cooke, Paul & Silberman, Marc (2010). Screening War: Perspectives on German Suffering. Camden House. ISBN 1571134379.
* Hermanni, Horst O. (2009). Von Dorothy Dandridge bis Willy Fritsch: Das Film ABC (in German). BoD – Books on Demand. ISBN 3833423749.
* Kaiser, Klaus (2010). Das kommt nicht wieder: Filmstars vergangener Jahre (in German). BoD – Books on Demand. ISBN 3839195543.
* Ossmann-Mausch, Christa A. (2006). Alles begann in Berlin: eine Jugend in Zeiten des Krieges (in German). Hamburg, Germany: Mein Buch oHG. ISBN 3-86516-493-5.
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 