Betelnut Beauty (film)
{{Infobox film
| name           = Betelnut Beauty
| image          = 
| caption        = 
| director       = Lin Cheng-sheng
| producer       = Peggy Chiao Hsu Hsiao-ming
| writer         = Lin Cheng-sheng
| starring       = Chang Chen Angelica Lee Leon Dai
| music          = 
| cinematography = 
| editing        = Ching-Song Liao
| distributor    = Arc Light Films Pyramide Distribution (France)
| released       =  
| runtime        = 105 minutes
| country        = Taiwan
| language       = Mandarin
| budget         = 
}}
Betelnut Beauty ( ) is a 2001 Taiwanese film directed by Lin Cheng-sheng.   

==Cast==
* Chang Chen as Feng
* Angelica Lee as Fei-fei
* Leon Dai as Tiger
* Kao Ming-chun as Guang
* Tsai Chen-nan as Ming

==Awards==
The film premiered in competition at the 51st Berlin International Film Festival. Lin won the Silver Bear for Best Director   
, and Angelica Lee won the New Talent award. 

==References==
 

==External links==
*  

 
 
 
 
 


 
 