Indian (2001 film)
{{Infobox film
| name           = Indian
| image          = Indian (2001).jpg
| image size     = 
| alt            = 
| caption        = Film poster
| director       = N. Maharajan
| producer       = Dharmendra
| writer         = Sanjay Masoom
| starring       = Sunny Deol Shilpa Shetty Rahul Dev Mukesh Rishi
| music          = Anand Raj Anand
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Vijayta Films
| released       =  
| runtime        = 178 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          =
}} Hindi crime drama film directed by N. Maharajan and starring Sunny Deol, Shilpa Shetty, Raj Babbar and Mukesh Rishi. A remake of Tamil film Vallarasu starring Vijayakanth, the film had dialogues written by Sanjay Masoom. It opened on 26 October 2001 to excellent collections.

==Plot==
Raj Shekhar Azad (Sunny Deol) is an honest Police Commissioner whose sole intention is to wipe out corruption. From his name to his aim, emerges unflinching patriotism. But for every such patriot, there is an equally motivated terrorist.

Waseem Khan (Mukesh Rishi), a dreaded terrorist, is aided in his mission by Rajs father-in-law (Raj Babbar), an influential industrialist (Danny Denzongpa) and several police officers.

Rajs path is laden with hurdles, but he leaves no stone unturned, within the realm of law, to wage a war against the evil doers. The film is notable for the Sunny Deols portrayal DCP Azad. The sublime toughness and solidity that the acclaimed actor manages to bring to the character of Rajshekhar Azad in his trademark sedate style is the high point of the film. Raj Babbar too comes
of in with a powerful performance though his role disappears within the first hour. The peerless Danny Dengzongpa, is powerful in his portrayal of Singhania Saab in the film.

==Cast==
*Sunny Deol as DCP Rajshekhar Azad
* Shilpa Shetty as Anjali Raj Shekhar Azad
* Rahul Dev as Pratap
*Mukesh Rishi as Wasim Khan
*Raj Babbar as DGP Suryapratap Singh
*Om Puri as Joginder Singh
*Rana Jung Bahadur as Havaldar Mushtaq Singh
*Deepak Shirke as Veer Bahadur Singh
*Rajat Bedi as Sanjay Singhania
*Sophiya Haque
*Malaika Arora
*Suresh Bhagwat as B.E.S.T. employee
*Danny Denzongpa as Shankar Singhania
*Salim Ghouse
*Avtar Gill as Gill
*Dinesh Hingoo as Jewellery shop owner
*Shakti Kapoor
*Viju Khote as Deshmukh
*Reema Lagoo as Mrs. Suryapratap Singh
*Pramod Muthu as Inspector Moutho (as Pramod Moutho)
*vikranta sandhu as maadho
*  Jasbir Thandi  D C P Rajshekhar shoots his sikh officer while arresting Shanker singhania

==Soundtrack==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Title !! Singer(s)

|-
| Waton Walo
| Roop Kumar Rathod
|-
| Deewane
| Shaan (singer) | Shaan, Alka Yagnik
|-
| Yeh Pyar Richa Sharma, Sunidhi Chauhan
|-
| Rab Di Kasam
| Udit Narayan, Alka Yagnik
|-
| Jaana Maine
| Abhijeet Bhattacharya| Abhijeet, Sadhana Sargam
|-
| Thaath Nawabi
| Anand Raj Anand, Alka Yagnik
|}

==External links==

*  

 
 
 