The Bobo
{{Infobox Film
| name           = The Bobo
| image          = Original movie poster for the film The Bobo.jpg
| image_size     = 225px
| caption        = film poster
| director       = Robert Parrish Peter Sellers (uncredited)
| producer       = Jerry Gershwin Elliott Kastner
| writer         = Novel: Burt Cole Screenplay: David R. Schwartz
| starring       = Peter Sellers Britt Ekland Rossano Brazzi Adolfo Celi
| music          = Francis Lai
| cinematography = Gerry Turpin
| editing        = 
| distributor    = Warner-Pathé Distributors (UK)  Warner Bros. Pictures (USA)
| released       = 1967
| runtime        = 103 minutes
| country        = United Kingdom English
| budget         = 
}}

The Bobo is a 1967 British comedy film starring Peter Sellers and co-starring his then-wife Britt Ekland.

Based on the 1959 novel Olimpia by Burt Cole, also known as Thomas Dixon, Sellers is featured as the would-be singing matador, Juan Bautista.  A theater manager offers to give him a big break if he seduces the beautiful Olimpia (Ekland) and spends an hour in her apartment with the lights off. The plot centers on Juans attempts to woo the woman and famously includes Sellers covered in blue dye as the "Blue Matador."

==Cast==
* Peter Sellers as Juan Bautista
* Britt Ekland as Olimpia Segura
* Rossano Brazzi as Carlos Matabosch
* Adolfo Celi as Francisco Carbonell
* Hattie Jacques as Trinity Martinez
* Ferdy Mayne as Silvestre Flores
* Kenneth Griffith as Pepe Gamazo
* Al Lettieri as Eugenio Gomez
* Marne Maitland as Luis Castillo John Wells as Pompadour Major Domo
* Don Lurio as Ramon Gonzales Antonia Santiago Amador (La Chana) as flamenco dancer

==Soundtrack==
*"Imagine", the track for the titles, was written by Francis Lai, lyrics Sammy Cahn; released as single by Stan Kenton and His Orchestra 1967, Dana Valery as B-side to "You"  by S. Napier, Bell, C. Leresche 1967, and by John Gary as B-side to "Cold" 1967  
*"The Blue Matador"

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 