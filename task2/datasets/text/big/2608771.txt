The Truth About Love (film)
{{Infobox film
| name           = The Truth About Love
| image          = The Truth About Love film.jpg|
| caption        = The Truth About Love film poster John Hay
| producer       = Tracey Adam William Johnston
| starring       = Jennifer Love Hewitt Jimi Mistry Dougray Scott Branka Katić Kate Miles
| music          = Debbie Wiseman
| cinematography = Graham Frake David Martin
| distributor    =
| released       =  
| runtime        = 100 min
| country        = United Kingdom
| language       = English
| budget         =
}} John Hay and starring Jennifer Love Hewitt, Jimi Mistry and Dougray Scott.

==Plot==
After Alice Holbrook (Jennifer Love Hewitt), a happily married English woman living in Bristol, receives an anonymous Valentines Day card with radish seeds in it, she automatically assumes the card is from her supposedly loving lawyer husband, Sam (Jimi Mistry), and that he is trying to be romantic. In return, Alice decides to write an anonymous reply to her husband to keep the gimmick going, but only accidentally sends the card after a drunken night with her sister. What Alice does not realise, however, is that her husband did, in fact, not send her the original Valentines Day card; her husbands best friend and lawyer partner, Archie (Dougray Scott), did.

When Alices husband does not mention that he has received her card, Alice becomes suspicious that he is cheating on her with another woman. In an attempt to discover the truth, Alice calls her husband and pretends to be another woman with a deep, sultry voice who she calls "Anonymous." After her husband agrees to meet with "Anonymous," Alice becomes even more distraught, but continues to try and win her husband back. However, in the process, she discovers her husband Sam is already having another affair with a woman named Katya and is now knowingly starting a second affair with "Anonymous." After breaking down over this fact, she goes to visit Archie in the sexy outfit she planned to woo Sam back with, and while Archie and she kiss, he assumes she is having an extra-marital affair with someone other than Sam and refuses to be a part of it.

Alice decides to forge ahead with her "affair" with Sam as "Anonymous," and while having supposedly extramarital sex with him, she removes his wedding ring from his finger with her lips. Afterwards, however, Sam declares "Alice is nothing compared to  " and that he has never truly loved Alice, and so she becomes very upset and flees the hotel. When she is running away, however, she runs into Katya, who discovers that Sam is not only cheating on his wife but also on her.

While Sam does win his big court case next, he returns home to find his precious wine scattered throughout the neighborhood and his clothes thrown in the street (all done by Alice), as well as Katya waiting at his house to tell Alice of their affair as revenge. When Katya sees Alice, however, she calls her "Anonymous," and so Sam realizes that Alice has been "Anonymous" all the while. Alice even shows Sam how she removed the ring from his finger to prove it. While Sam attempt to plead with Sam, Alice insists on a divorce and Katya walks out on Sam as well.

The film then fast forwards to a little time later. Katya took her story as well as that of "Anonymous" to the press and so Sams reputation as an upstanding lawyer has been ruined. Furthermore, the press exposes that Alice was, in fact, not having an extramarital affair as Archie previously thought. This realisation prompts Archie to attempt to tell Alice about his feelings for her, but she cannot see past their friendship. Because of this, Archie decides he cannot get over Alice while still in Bristol, so he takes a job in Japan. First though, he sends a goodbye letter to Alice, in which he includes "P.S. I hope you liked the radishes," finally letting her know that the original Valentines Day card was from him and not Sam. Alice chases after Archie and finally catches him after a series of debacles at the train station, including him having to actually stop the train. They kiss and declare they love each other. The movie ends when Alice lovingly looks up at the man who really thinks she is "the perfect woman" and says "Take me home and radish me."

==Main cast==
* Jennifer Love Hewitt &ndash; Alice Holbrook
* Dougray Scott &ndash; Archie Gray
* Jimi Mistry &ndash; Sam Holbrook
* Branka Katić &ndash; Katya
* Kate Miles &ndash; Felicity
* Stefan Dennis &ndash; Dougie
* Simon Webbe &ndash; Dan Harlow

==References==
The Truth About Love. John Hay. First Look International, 2005. Film.

==External links==
*  

 
 
 
 
 
 