Rachel River
{{Infobox Film
| name           = Rachel River
| image          = Rachel river poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Sandy Smolan
| producer       = Timothy Marx
| writer         = Judith Guest
| narrator       = 
| starring       = Pamela Reed Ailene Cole Don Cosgrove Jon DeVries Ron Duffy Craig T. Nelson
| music          = Mitchell Froom
| cinematography = Paul Elliott
| editing        = Susan R. Crutcher	
| distributor    = Taurus Entertainment Company 1987
| runtime        = 85 min
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1987 comedy-drama film about a young journalist who returns to her Minnesota home town to reexamine her life. The film was directed by Sandy Smolan, and stars Pamela Reed, Ailene Cole, Don Cosgrove, and Craig T. Nelson.

==External links==
*  

 
 
 
 
 


 