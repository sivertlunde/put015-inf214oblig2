Appointment in Berlin
{{Infobox film
| name =  Appointment in Berlin
| image = Mcberlin.jpg
| image_size =
| caption = Glamour Still of Marguerite Chapman for "Appointment in Berlin" 1943
| director = Alfred E. Green
| producer = Samuel Bischoff Michael Hogan   Horace McCoy
| narrator =
| starring = George Sanders   Marguerite Chapman   Onslow Stevens   Gale Sondergaard
| music = Werner R. Heymann 
| cinematography = Franz Planer Al Clark 
| studio = Columbia Pictures
| distributor = Columbia Pictures 
| released = July 15, 1943
| runtime = 77 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} war drama film directed by Alfred E. Green and starring George Sanders, Marguerite Chapman and Onslow Stevens. The films plot follows an R.A.F. officer infiltrates himself into the German high command by broadcasting a series of pro-Nazi messages.  The films art direction was by Lionel Banks and Walter Holscher.

==Partial cast==
* George Sanders as Wing Cmdr. Keith Wilson 
* Marguerite Chapman as Ilse Von Preising 
* Onslow Stevens as Rudolph Von Preising 
* Gale Sondergaard as Greta Van Leyden
* Gilbert Emery as Gen. Marston  
* Lester Matthews as Air Marshal   John Meredith as Engineer  
* Leonard Mudie as MacPhail 
* Alan Napier as Col. Patterson  
* Georges Renavent as Van der Wyn  
* C. Montague Shaw as Langly 
* Reginald Sheffield as Miller - Wilsons Butler  
* Marek Windheim as Croupier  
* Frederick Worlock as Von Ritter - Ministry of Information 
* Constance Worth as English Girl 
* Franklyn Farnum as R.A.F. Officer Jack Lee as Babe Forrester  
* Leyland Hodgson as Joiner  
* Keith Hitchcock as Bobby 
* Steven Geray as Henri Rader   Jack Gargan as Underground Man  
* Arno Frey as Gestapo Captain  
* Byron Foulger as Herr Van Leyden   Herbert Evans as Nightclub Party Guest
* Don Douglas as Bill Banning
* Jean De Briac as Señor Bonti
* Leslie Denison as Detective  
* Alec Craig as Smitty - News Vendor  
* Gino Corrado as Headwaiter  
* Felix Basch as Hoppner

==References==
 

==Bibliography==
* Etling, Laurence. Radio in the Movies: A History and Filmography, 1926-2010. McFarland, 2011.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 