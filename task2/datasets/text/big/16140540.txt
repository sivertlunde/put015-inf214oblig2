Thank You (1925 film)
 
{{Infobox film
| name           = Thank You
| image          = 
| caption        = 
| director       = John Ford
| producer       = John Golden
| writer         = Tom Cushing Frances Marion Winchell Smith
| starring       = Alec B. Francis Jacqueline Logan
| cinematography = George Schneiderman
| editing        =  Fox Film Corporation
| released       =  
| runtime        = 70 minutes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}

Thank You is a 1925 American comedy film directed by John Ford. The film is considered to be lost film|lost.     This film is based on a 1921 Broadway play, Thank You, by Winchell Smith and Tom Cushing. 

==Cast==
* Alec B. Francis as David Lee
* Jacqueline Logan as Diane Lee George OBrien as Kenneth Jamieson
* J. Farrell MacDonald as Andy
* George Fawcett as Cornelius Jamieson
* Cyril Chadwick as Mr. Jones
* Edith Bostwick as Mrs. Jones
* Marion Harlan as Milly Jones
* Vivia Ogden as Miss Blodgett
* James Neill as Doctor Cobb
* Billy Rinaldi as Sweet, Jr.
* Aileen Manning as Hannah
* Maurice Murphy as Willie Jones
* Robert Milasch as Sweet, Sr.
* Ida Moore as Gossiping Woman
* Frankie Bailey as Gossiping Man
* William Courtright (uncredited) Richard Cummings (uncredited)
* Tommy Hicks as Fat kid (uncredited)

==See also==
*List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 