Hard Lesbian: Genuine Tongue Technique
{{Infobox film
| name = Hard Lesbian: Genuine Tongue Technique
| image = Hard Lesbian - Genuine Tongue Technique.jpg
| image_size = 
| caption = Theatrical poster for Hard Lesbian: Genuine Tongue Technique (1992)
| director = Yutaka Ikejima 
| producer = 
| writer = Kyōko Godai
| narrator = 
| starring = Chisato Ariga Mako Satomi Shinozaki
| music = 
| cinematography = Satoshi Shimomoto
| editing = Shōji Sakai
| studio = Xces Films
| distributor = Xces Films
| released = December 25, 1992
| runtime = 60 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1992 Japanese pink film directed by Yutaka Ikejima and scripted by his wife, prolific screenwriter Kyōko Godai. It won the Best Film, 6th place award at the Pink Grand Prix ceremony.   

==Synopsis==
Emi, a nude model, falls in love with Mako, a promiscuous lesbian. After much drama, Emi comes to the conclusion that the only way she can have Mako to herself is to kill her.   

==Cast==
* Chisato Ariga ( ) as Emi (nude model)   
* Mako ( ) as Mako
* Satomi Shinozaki ( ) as Nahoko
* Atsumi Sakurai ( ) as Kayo (Woman at a lesbian show)
* Misao Sugihara ( ) as Atsumi
* Tatsuo Hibino ( ) as Kazuhiko Yamamura
* Yutaka Ikejima as Momochi
* Ryūji Yamamoto ( ) as Kumita
* Michiyo Yasunaga ( ) as Lesbian bar customer
* Saori Suzuki ( )
* Sumiko Mogi ( ) as Lesbian bar host
* Kenichi Kanbe ( ) as Kōbe

==Background, availability and critical appraisal==
 
Director  . Godai had previously come to prominence for her work with the controversial director Hisayasu Satō. 

Ikejima filmed Hard Lesbian: Genuine Tongue Technique for Xces Films and that studio released it theatrically in Japan on December 25, 1992.    Xces re-released the film on May 13, 2005 under the title  .    
At the fifth annual Pink Grand Prix ceremony covering 1992, Hard Lesbian: Genuine Tongue Technique was named the sixth best film of the year. Lead actress Mako was also given a Best New Actress award at the ceremony.  In their Japanese Cinema Encyclopedia: The Sex Films (1998) Thomas and Yuko Mihara Weisser give Hard Lesbian: Genuine Tongue Technique a rating of three out of four stars. 

==Bibliography==

===English===
*  
*  
*  

===Japanese===
*  
*  
*  

==External links==
*   (Xces official site)

==Notes==
 

 
 
 
 
 

 
 

 
 
 
 
 
 


 
 