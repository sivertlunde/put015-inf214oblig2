Nenapirali
{{Infobox film
| name           = Nenapirali
| image          = 
| caption        = 
| director       = Rathnaja
| producer       = Ajay R. Gowda
| screenplay     = Rathnaja
| story          = Rathnaja
| based on       = 
| starring       = Prem Kumar Vidya Venkatesh Varsha
| narrator       = 
| music          = Hamsalekha
| cinematography = H. M. Ramachandra
| editing        = B. S. Kemparaju
| studio         = Kalpana Shakthi
| distributor    = 
| released       =  
| runtime        = 143 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}

 Nenapirali  ( ) is a 2005 Indian Kannada language film directed by Rathnaja. It stars Prem Kumar, Vidya Venkatesh and Varsha in the lead roles. The supporting cast features Naveen Krishna, Anant Nag, Jai Jagadish, Vijayalakshmi Singh and Vinaya Prasad.
 Best Cinematographer Best Music Best Film. 

==Plot==
The film begins by introducing Ekanth (Naveen Krishna), the only son of a rich industrialist Sundar Raj (Anant Nag). He lives in Bangalore and manages his fathers business which keeps him extremely busy. Ekanth is in love with an introverted girl, Indushree (Vidya), from Mysore, who feels that Ekanth is not interested in her and is not serious about marrying her. In order to test his intentions, Indu decides to lie to Ekanth that her parents are looking for suitable grooms to arrange her marriage. She mentions that her parents would prefer that she marry someone from a political background. Now Ekanth gets slightly worried by the turn of events and hatches a scheme to convince Indus parents for their marriage. Enter Kishore (Prem), Ekanths best friend, who Ekanth instructs to pretend being in love with Indu. Kishore is a middle class simple boy who is very good at heart and agrees to Ekanths request only to help his friends love succeed. Reluctantly, Indu too agrees to participate in the false show of being in a relationship with Kishore. Since Indus parents would disagree to Indu and Kishores relationship (due to the societal differences), Ekanth thinks that his marriage proposal will be readily accepted in comparison to Kishore. Everything goes as per planned. However, Indu starts developing feelings towards Kishore while spending time with him. When she pours her heart out to Kishore, he immediately hastens conversations about Ekanth and Indus marriage and eventually with everyones consent, Indu and Ekanth get married. After Ekanths marriage, Kishore, his mother and sister move to Mangalore to manage one of Ekanths industry branches located there. 

Bindu (Varsha) is Indus sister who has just graduated and would like to gain some work experience. She requests her brother-in-law, Ekanth for a short stint at one of his industry locations and he recommends that she work alongside Kishore in Mangalore. Bindu moves to Mangalore and as days go by, Kishore and Bindu start enjoying each others company and eventually fall in love with each other. Suddenly one day, Ekanth meets with a car accident and passes away. Deeply saddened by the turn of events and his widowed daughter-in-law, Sundar Raj (Eknaths father) wants Indu to remarry. Upon her familys persuasion and a meeting with Kishore after a long time, the flames of love for Kishore are reignited in Indus heart and she insists that she will only remarry if it is going to be with Kishore. Bindu is tasked with gaining everyones acceptance, including Kishores. Heartbroken Bindu sets out on her mission and is ready to make the sacrifice for her sisters happiness. However, Kishore is very upset and tries as much as he can to dissuade Indu but to no avail. Indu expresses her love and interest in him once again, which Kishore refuses to accept. The climax of the movie is one that leaves audience happy, with Kishore and Bindu ultimately triumphing with their love. Indu isnt greatly pleased by this but accepts her destiny.

==Cast==
* Prem Kumar as Kishore
* Vidya Venkatesh as Indushree
* Varsha as Bindushree
* Naveen Krishna as Ekanth
* Anant Nag
* Jai Jagadish
* Vijayalakshmi Singh
* Vinaya Prasad
* Chitra Shenoy Sharan

==Soundtrack==
{{Infobox album
| Name        = Nenapirali
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = Nenapirali album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Kalpana Audio, Ashwini Audio
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Hamsalekha composed the background score for the film and composed the music for the soundtracks, also penning the lyrics for them. The album consists of eight soundtracks.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Olavu Ontiyalla
| lyrics1 = Hamsalekha
| extra1 = Chetan Sosca, K. S. Chithra, Ravishankar
| length1 = 
| title2 = Koorakk Kukkaralli Kare
| lyrics2 = Hamsalekha
| extra2 = S. P. Balasubrahmanyam
| length2 = 
| title3 = Preethi Paatavalla
| lyrics3 = Hamsalekha
| extra3 = Chetan Sosca, K. S. Chithra, Ravishankar
| length3 = 
| title4 = Draupadi Draupadi
| lyrics4 = Hamsalekha
| extra4 = Sowmya Raoh, Annupamaa, Anoop
| length4 = 
| title5 = Indu Baanigella Habba
| lyrics5 = Hamsalekha
| extra5 = K. S. Chithra
| length5 = 
| title6 = Ajantha Ellora
| lyrics6 = Hamsalekha
| extra6 = Vijay Yesudas
| length6 = 
| title7 = Hey Beladingale
| lyrics7 = Hamsalekha
| extra7 = Hemanth, Divya Raghavan, Chetan Sosca, Ravishankar, Butto, Anoop
| length7 = 
| title8 = Draupadi Theme
| lyrics8 = 
| extra8 = 
| length8 = 
}}

==Critical reception==
Upon theatrical release, the film was reviewed positively by critics. B. S. Srivani of Deccan Herald said the film ".. is a classic example of how the crucial components of a film can bring out a clean, almost irreproachable product. Taut direction, crisp dialogues that do not pontificate, slick editing and the sheer poetic spell cast by the camera and music come together to make a delectable concoction." She gave special praise to the cinematographer H. M. Ramachandra for having captured "locales of Mysore, Mangalore and Bangalore ... beautifully."  R. G. Vijayasarathy of Rediff.com|Rediff reviewed the film and called it "a cut above the rest in terms of quality and presentation." He praised the roles of the technical departments of the film and concluded writing, "Nenapirali has class. It is a worthy addition to the list of good films from the Kannada film industry. A must for all industry enthusiasts."  Viggy.com writes in praise of the director Rathnaja straightaway for having "displayed technical competency in his very first attempt." The role technical and acting departments were praised. 

==Awards==
* 2005 Filmfare Awards South Best Film Best Director – Rathnaja Best Actor – Prem Kumar Best Actress – Vidya Venkatesh Best Music Director – Hamsalekha

== References ==
 

 
 
 
 
 
 