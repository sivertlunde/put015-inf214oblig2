The Street of Adventure (film)
 
{{Infobox film
| name           = The Street of Adventure
| image          =
| caption        =
| director       = Kenelm Foss
| producer       = H.W. Thompson
| writer         = Philip Gibbs (novel)   Kenelm Foss 
| starring       = Lionelle Howard   Margot Drake   Irene Rooke 
| cinematography = 
| editing        = 
| studio         = Astra Films 
| distributor    = Astra Films 
| released       = August 1921
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama novel of the same title by Philip Gibbs. The title is a reference to Fleet Street in London. A journalist attempts to save a woman from prostitution, but nearly loses his fiancée to another man in the process.

==Cast==
* Lionelle Howard as Frank Luttrell  
* Margot Drake as Katherine Halstead  
* Irene Rooke as Margaret Hubbard  
* Peggy Bayfield as Peg  
* Roy Travers as Will Brandon  
* Will Corrie as Edmund Grattan  
* H.V. Tollemach

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 