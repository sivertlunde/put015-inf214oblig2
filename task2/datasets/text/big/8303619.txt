Brother Brat
 
{{Infobox Hollywood cartoon
|cartoon_name=Brother Brat
|series=Looney Tunes (Porky Pig)
|image=Brother Brat title card.png
|caption=Title card
|director=Frank Tashlin
|story_artist=Melvin Millar Art Davis Cal Dalton Izzy Ellis
|narrator=Robert C. Bruce
|voice_actor=Mel Blanc (Porky Pig/Baby Butch) Bea Benaderet (Baby Butchs Mother)
|musician=Carl Stalling
|producer=Leon Schlesinger Leon Schlesinger Productions
|distributor=Warner Bros. Pictures
|release_date=July 15, 1944
|color_process=Technicolor
|runtime=7 Minutes
|movie_language=English
}} color with a mono sound mix. It is also one of the few cartoons where Porky wears pants.

==Plot==
When a mother goes to work in a factory during World War II (see Rosie the Riveter), Porky Pig is hired to baby-sit. He quickly finds out that the baby is a violent-tempered infant. He tries to use a child psychology book to control the baby, to no avail. Eventually, the mother returns and uses the book to discipline the baby — by spanking.

==External links==
* 

 
{{Succession box|
before=Confusions of a Nutzy Spy| Porky Pig Cartoons|
years=1944|
after=Swooner Crooner}}
 

 
 
 
 
 
 
 
 

 