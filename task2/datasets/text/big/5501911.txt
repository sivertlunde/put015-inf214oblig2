A Fine Mess (film)
{{Infobox film
| name          = A Fine Mess
| image         = Finemessposter.jpg
| caption       = Theatrical release poster
| director      = Blake Edwards Tony Adams
| writer        = Blake Edwards
| starring      = Ted Danson Howie Mandel
| music         = Henry Mancini
| cinematography= Harry Stradling, Jr.
| studio        = Columbia Pictures Blake Edwards Entertainment
| distributor   = The Coca-Cola Company
| released      =  
| runtime       = 90 minutes
| country       = United States
| language      = English
| budget        =
| gross         = $6,029,824
}}
A Fine Mess is a 1986 comedy film written and directed by Blake Edwards and starring Ted Danson and Howie Mandel.

The film was intended as a remake of Laurel & Hardys classic short "The Music Box" MOVIES: EDWARDS YEAR OF LIVING DANGEROUSLY YEAR OF LIVING DANGEROUSLY The Party, but studio interference, poor previews and subsequent re-editing resulted in the film becoming a fully scripted chase comedy with very little of the original ideas for the film remaining intact. Writer/director Blake Edwards actually gave television interviews telling audiences to avoid the film. It received overwhelmingly negative reviews and was a box-office failure.

==Plot==
While filming on location at a race track, womanizing bit actor Spencer Holden, who lives life on one scam after another, overhears a couple of inept thugs named Binky and Turnip while they dope a race horse with a supposed undetectable super stimulant. The thugs find out that Spence overheard them and will do anything to catch him to prevent him from going to the authorities with the information. Spence, however, enlists the help of his best friend, drive-in carhop and aspiring restaurateur Dennis Powell, to bet on the race with that horse so that they can make some guaranteed money. Spence and Dennis end up having to outrun not only the thugs, who manage to put a few bullet holes in Spences car, but also the police after they find Spences bullet-riddled car and after the race horse, Sorry Sue, dies from the drugs. The plot also includes an antique player piano of which Dennis comes into possession, sympathetic but naive auction house employee Ellen Frankenthaler who is attracted to Dennis, and exotically beautiful Claudia Pazzo who is interested in buying the piano and whom Spence cant resist.

==Cast==
* Ted Danson as Spence Holden
* Howie Mandel as Dennis Powell
* Richard Mulligan as Wayne Turnip Parragella
* Stuart Margolin as Maurice Binky Drundza
* María Conchita Alonso as Claudia Pazzo
* Jennifer Edwards as Ellen Frankenthaler
* Paul Sorvino as Tony Pazzo
* Rick Ducommun as Wardell
* Keye Luke as Ishimine
* Ed Herlihy as TV Reporter
* Walter Charles as Auctioneer
* Tawny Moyer as Leading Lady
* Emma Walton as First Extra
* Carrie Leigh as Second Extra
* Sharan Lea as Young Girl
* Dennis Franz as Phil
* Larry Storch as Leopold Klop

==Reception==

===Critical reception===
A Fine Mess received heavily negative reviews (Rotten Tomatoes gave it an insufficient score), including one from The New York Times, which stated "Mr. Edwards, who on happier occasions gave us the Pink Panther movies, piles on the pileups until you may suspect that he is trying to distract the audience from the absence of a diverting story or dialogue. The 11 musical numbers by some well-known performers (available, you can bet, on record and cassette) seem designed for the same purpose." 

===Blake Edwards reaction===

Edwards mostly avoided doing interviews promoting the film and when he did them, he explicitly told audiences to avoid the film altogether, especially since Thats Life (film)|Thats Life was debuting in September of that year. 

===Box office===
The film flopped tremendously on a cost of an undetermined budget, and, in the US, grossed $6,029,824, so it became a box office bomb.

==Soundtrack==
{{Infobox album  
| Name        = A Fine Mess
| Type        = Soundtrack
| Artist      = 
| Cover       = 
| Released    = 1986
| Recorded    =
| Genre       = Soundtrack
| Length      = 
| Label       = Motown
| Producer    =
}}

#"Cant Help Falling In Love" - Christine McVie
#"Easier Said Than Done" - Chico DeBarge
#"A Fine Mess" - Temptations
#"Im Gonna Be A Wheel Someday" - Los Lobos
#"Loves Closing In" - Nick Jameson
#"Moving So Close" - Keith Burston & Darryl Littlejohn
#"Slow Down" - Billy Vera & Beaters
#"Stan And Ollie" - Henry Mancini
#"Walk Like A Man" - Mary Jane Girls
#"Wishful Thinking" - Smokey Robinson

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 