Nihon Chinbotsu (2006 film)
{{Infobox film
| name           = Nihon Chinbotsu
| image          = 
| caption        = 
| director       = Shinji Higuchi
| producer       = Kazuya Hamana  Toshiaki Nakazawa
| writer         = Sakyo Komatsu (Novel) Masato Kato
| narrator       = 
| starring       = Tsuyoshi Kusanagi  Kou Shibasaki  Etsushi Toyokawa  Mao Daichi  Mitsuhiro Oikawa
| music          = Tarō Iwashiro
| cinematography = Taro Kawazu
| editing        = 
| distributor    = Toho
| released       =  
| runtime        = 135 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} screenplay based Komatsu novel. It stars Tsuyoshi Kusanagi, Kou Shibasaki, Etsushi Toyokawa, and Mao Daichi, and was released on July 15, 2006.

The film is also known under the titles "Sinking of Japan" and "Doomsday: The Sinking of Japan". 

It was parodied in Nihon Igai Zenbu Chinbotsu ("Everyone but Japan Sinks").

==Synopsis==
Submersible pilot Toshio Onodera wakes up pinned inside his car in Numazu after an earthquake wreaks havoc in the city and nearby Suruga Bay. As aftershock triggers an explosion, a rescue helicopter led by Reiko Abe saves him and a young girl named Misaki.

In Tokyo, geologists around the world become concerned about Japan; one predicts that the country will sink within 40 years. Japanese geoscientist Yusuke Tadokoro doubts the prediction and analyzes rocks in Kyushu, Hokkaido, and Mangaia in the Cook Islands, where he hypothesizes that the rock came from the ancient continent of Japan after it split from Pangaea thousands of years ago. Tadokoro realizes Japan will sink in 338.54 days (which is less than a year) instead of the original 40-year estimate. Tadokoro reports his theory to the Japanese Cabinet|Cabinet, recommending immediate action, but none of the ministers are convinced. He is ejected from the chamber, but not before he angrily explains to everyone how Japan will sink, with the destruction of the Fossa Magna and the eruption of Mount Fuji as the climax.
 Kumamoto Citys residents flee in terror as lava bombs rain down and a pyroclastic flow bears down on the city.

Takamori panics when she finds out about the prime ministers death. Earthquakes, tsunamis, and volcanic eruptions rock southwest Japan, affecting a large-scale evacuation now underway. As the economy collapses, the government declares a state of emergency but de facto Prime Minister Kyosuke Nozaki announces that Japan will take five years to sink. Because of Nozakis indifference to the situation, Takamori runs to Tadokoros laboratory, where he proposes using experimental N2 explosives drilled into the crust to separate the land from the megalith pulling it down. The minister, who is actually Tadokoros ex-wife, calls for help from drillships around the world.

Misaki, Reikos family and the rest of Tokyos populace is evacuated. Onodera confesses his true feelings for Reiko and wants her to go to England with him. An aftershock finally destroys the Kinki region and many Tokyo evacuees are killed by a massive landslide, with people falling into the valley below as Misaki is rescued by Reikos family and they witness a crowded bridge collapse.

Yuki Tatsuya, Onoderas fellow submersible pilot, dies in an attempt to activate the warheads from a central module. Onodera takes his place using an old submersible brought out of museum storage and spends a night with Reiko before the operation. Although he locates the detonation module, a sudden landslide damages his submersible to the point that it is running on emergency power. Onodera uses all the remaining power to move into position and install the detonator. Mount Fuji begins to erupt. Toshio succeeds in his task and calmly awaits his doom. The warheads explode, creating a chain of explosions along the seafloor, saving Japan.
 amphibious carrier Shimokita, which has been converted as the Japanese governments temporary headquarters. Although she recommends that Nozaki address the refugees, her colleagues want her to do it instead, given her leadership during the crisis. She announces that people can finally return and holds a moment of silence in honor of Tatsuya and Onoderas sacrifice. In Fukushima Prefecture, Toshios mother, who wanted to remain at her house until the end, is overjoyed when she sees birds return- a sign of his success. Reiko rescues her family as they look towards a bright sunrise, before the credits start rolling, showing a drastically altered Japan.

==Cast==
* Tsuyoshi Kusanagi - Toshio Onodera, a submersible pilot working for Tadokoro Tokyo Hyper-Rescue team
* Etsushi Toyokawa - Yusuke Tadokoro, geologist
* Mao Daichi - Saori Takamori, science and disaster management minister
* Mitsuhiro Oikawa - Yuki Tatsuya, submersible pilot
* Mayuko Fukuda - Misaki Kuraki, young child found in Numazu
* Jun Kunimura - acting Prime Minister Kyosuke Nozaki
* Koji Ishizaka - Prime Minister Yamamoto
* Aiko Nagayama - Toshio Onoderas Mother
*  Gundam creator Yoshiyuki Tomino appeared as one of the refugees boarding a transport plane and as a Shinto monk in Kyoto praying over a shipment of national treasures being sent abroad.

==Comparison with Submersion of Japan==
* The 2006 film ends with Japan being saved after a series of nuclear detonations stop its tectonic plates from dragging the entire country down. The original 1973 movies ending is more faithful to the book - Japan does sink in the end and Mount Fuji erupts.
* In the original movie, Reiko Abe is a single woman in search of a husband. The 2006 film recasts the character as a member of Tokyos Hyper-Rescue Squad. Both versions of Reiko Abe survive by the end of the film, with the 2006 Reiko being able to save her family while the 1973 version successfully escapes Japan and is last seen aboard a refugee train passing through an unknown snow-capped country. Abes fate in the book is left unknown after Mount Fuji explodes near her as she calls Toshio and asks him to meet in Geneva.
* In the original 1973 film, Toshio Onodera makes it out and is last seen riding a refugee train passing through a desert. Onodera dies in the 2006 film.
* In the original 1973 film, Dr Tadokoro stays behind in Japan to witness the end while the 2006 version of him successfully oversees the drilling operation that saves Japan.
* In the original 1973 film, Prime Minister Yamamoto is one of the last evacuees from Japan while the 2006 version of the premier dies while flying to China.
* The 2006 version of the film does not include Mr Watari, a wise old man giving counsel to Tadokoro in the 1973 film.
* The 1973 version of Dr Tadokoro leads a government disaster-response group called D1, which accurately predicts the breakup of Japan. The 2006 version of the scientist is first discredited by the government despite presenting his research before the prime minister accepts the situation and establishes D1 (with Tadokoro temporarily out of the picture). The D1 team is based aboard a ship. The 1973 Tadokoro is apparently single while the 2006 Tadokoro has been divorced for 20 years.
* The 2006 version of the movie does not depict a massive earthquake that ravages Tokyo and kills over 3.3 million people - an important set-piece in the 1973 film.
* The opening credits of the original 1973 film depicts scenes of an overcrowded Japan while the introduction of the 2006 film features various scenes of everyday Japanese life as well as wide shots of several locations in Japan.
* The Wadatsumi 6500 in the 2006 film is based on the real-life Shinkai 6500 submersible. The remake pays homage to the 1973 film through the appearance of its older brother, the Wadatsumi 1, now called the Wadatsumi 2000.

==Releases==

===Home media===
Prior to the release of the film, TBS released The Encyclopedia of Sinking of Japan, a special one-hour DVD featuring interviews with the cast and crew. A "Standard Version" was released on January 19, 2007.

===Scale models===
Takara Tomy released two batches of gashapon miniatures depicting various vehicles from the film in August 2006. The company followed it up in January 2007 with a 1/700 pre-assembled model of the Shimokita, which was released under its Microworld DX line.

==Critical reception==
The film garnered mixed reviews. Derek Elley of Variety lauded the visual effects, but regarded the drama elements as thin.  Nix of Beyond Hollywood.com noted the ending as akin to Bruce Willis characters sacrifice in Armageddon (1998 film)|Armageddon and the lines of some characters are practically the same as in Western disaster movies.  Mark Schilling, a film reviewer for the Japan Times, stated the movie was all business in terms of the Hollywood-style effects graphically showing the devastation. He also took notice of Shibasakis casting as Reiko Abe and the short conversation scenes as different from the 1973 movie, plus the "soft nationalism" of some characters opting to die in the chaos rather than leave the country. 

==References==
 

==External links==
*   at Dreamlogic.net
*  
* 

 

 
 
 
 
 
 

 
 
 