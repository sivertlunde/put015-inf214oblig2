Hsien of the Dead
 
 
{{Infobox film
| name           = Hsien of the Dead
| image          = Hsien of the dead poster.jpg
| border         = Yes
| caption        = Theatrical release poster
| director       = Gary Ow
| producer       = Gary Ow
| writer         = Gary Ow
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Monkeywrench Genetix S Arte Associates
| distributor    =
| released       =  
| runtime        = 72 minutes
| country        = Singapore
| language       = English
| budget         = Singapore dollar|S$350,000 
| gross          =
}} comedy film directed, produced, and written by Gary Ow for Monkeywrench, Genetix S Pte Ltd, and Arte Associates. The film stars Ernest Seah, Vivienne Tseng, Moses San Juan, Nurhada Choo, and Darrell Britt. It follows four unrelated Singaporeans escaping from a wave of animated corpses. Together they devise a plan to flee the zombie-infested city state. Released on 13 September 2012, the film is credited as "Singapores first zombie movie".

==Plot==
Army staff member Edward (Ernest Seah) discovers that his colleague has become a zombie. Without much struggle, he bashes the zombie with a mop and escapes the government building. Outside, he discovers that almost all of Singapore has been infected with a zombie virus. He teams up with martial arts practitioner Ah Huay (Vivienne Tseng), biker Hana (Nurhada Choo), and army enlistee Hsien (Moses San Juan) and together they try to work out an escape plan from Singapore.

==Production== Mas Alamak" in the film.   

Inspired by the British horror-comedy Shaun of the Dead (2004),  Hsien of the Dead was collaboratively produced by three companies – Monkeywrench, Genetic S, and Arte Associates. Filmmaker Gary Ow signed on to direct, produce, and write the screenplay. Ow had previously been in charge of scripting the comedy play, Vampire Monologues.  The film was reportedly " hot entirely with a handheld Canon EOS 7D camera and wireless Sennheiser body microphones".    Funded by Arte Associates,  the film was made on an estimated budget of Singapore dollar|S$350,000, inclusive of promotion costs. 

==Release==
The film first had a non-theatrical limited release in Singapore on 22 June 2012,  followed by a three-day-run in cinemas from 13 September 2012 to 15 September 2012. 

==References==
 

==External links==
*  

 