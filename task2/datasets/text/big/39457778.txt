Computer Chess (film)
{{Infobox film
| name           = Computer Chess
| image          = Computer Chess.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Andrew Bujalski
| producer       = Houston King Alex Lipschultz
| writer         = Andrew Bujalski
| starring       = Patrick Riester Wiley Wiggins Myles Paige Robin Schwartz Gerald Peary
| music          = 
| cinematography = Matthias Grunsky
| editing        = Andrew Bujalski
| studio         =  Kino Lorber
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Alfred P. Sloan Feature Film Prize, and subsequently screened at such festivals as South by Southwest and the Maryland Film Festival.

It is Bujalskis second black-and-white film, and was shot with analog videocameras. It is more improvisatory than his previous films, with only an eight-page treatment for a script. Bujalski also cast nonprofessional actors who were knowledgeable in computer technology.

==Plot summary== grandmaster (Gerald Peary) presides as master of ceremonies with a videographer and microphone in tow. Clunky, primitive personal computers are carted from room to room. Bad haircuts, dorky shirts, “birth control glasses”, and other social impedimenta are ubiquitous. Bull sessions on the dystopian possibilities of artificial intelligence are pursued. The Pentagon|Pentagons interest in the goings-on is intimated. The only female geek (Robin Schwartz) in attendance is repeatedly hailed and “welcomed” by the MC.

Simultaneously at the same hotel, a human potential movement group (the “seekers”) has occasional run-ins with the geeks, generating awkward and humorous moments. A painfully shy young computer programmer (Patrick Riester) attracts the interest of a swinging older couple (Cyndi Williams and Chris Doubek). The twin threads of “spiritual” exploration and cybernetic innovation imply an unspoken and implicit hidden connection. In a startling final scene, a prostitute — apparently solicited by the young programmer — reveals herself to be infinitely more than expected.

==Cast==
*Patrick Riester as Peter Bishton
*Wiley Wiggins as Martin Beuscher
*Myles Paige as Michael Papageorge
*Robin Schwartz as Shelly Flintic
*Gerald Peary as Pat Henderson
*Gordon Kindlmann as Tom Schoesser

==Reception==
The movie has been well received and held an 82% "fresh" rating on Rotten Tomatoes as of November 5, 2013.   In  The Village Voice, Aaron Hillis wrote that it was "the funniest, headiest, most playfully eccentric American indie of the year." 

==References==
 

==External links==
*  
*  
*  
*  at Motherboard

 
 
{{succession box Alfred P. Sloan Prize Winner
| years=2013
| before=Robot & Frank
| after=I Origins}}
 

 
 
 
 
 
 
 
 
 
 
 
 


 