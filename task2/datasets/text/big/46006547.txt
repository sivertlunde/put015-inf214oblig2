Henry Aldrich for President
{{Infobox film
| name           = Henry Aldrich for President
| image          = 
| alt            = 
| caption        = Theatrical release poster
| director       = Hugh Bennett 
| producer       = Sol C. Siegel
| screenplay     = Val Burton  Mary Anderson Charles Smith John Litel Dorothy Peterson Martha ODriscoll
| music          = Walter Scharf 
| cinematography = John J. Mescall
| editing        = Thomas Neff
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Mary Anderson, Charles Smith, John Litel, Dorothy Peterson and Martha ODriscoll. The film was released on October 24, 1941, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Jimmy Lydon as Henry Aldrich
*June Preisser as Geraldine Adams Mary Anderson as Phyllis Michael
*Charles Smith as Dizzy Stevens
*John Litel as Mr. Aldrich
*Dorothy Peterson as Mrs. Aldrich
*Martha ODriscoll as Mary Aldrich
*Vaughan Glaser as Mr. Bradley Rod Cameron as Ed Calkins
*Kenneth Howell as Irwin Barrett
*Lucien Littlefield as Mr. Crosley
*Irving Bacon as Mr. McCloskey
*Frank Coghlan Jr. as Marvin Bagshaw
*Buddy Pepper as Johnny
*Dick Paxton as Red MacGowan
*Lillian Yarbo as Lucinda
*Arthur Loft as Department of Commerce Inspector Sidney Miller as Sidney

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 