The Suspect (1916 film)
{{Infobox film
| name           = The Suspect
| image          =
| caption        =
| director       = S. Rankin Drew
| producer       = Vitagraph Company of America
| writer         = S. Rankin Drew(scenario)
| based on       = The Silver Shell by H. J. W. Dam
| starring       = Anita Stewart
| cinematography = Arthur Quinn
| editing        =
| distributor    = V-L-S-E
| released       = May 22, 1916
| runtime        = 6 reels
| country        = USA
| language       = Silent.(English titles)
}} lost  1916 silent film starring Anita Stewart. It was directed by S. Rankin Drew and produced by the Vitagraph Company of America. 

==Cast==
*Anita Stewart - Sophie Karrenina
*S. Rankin Drew - Prince Paul Karatoff
*Anders Randolf - Duke Karatoff George Cooper - Valdor
*Frank Morgan - Sir Richard(*billed Frank Wupperman)
*Edward Elkas - Mouroff
*Julia Swayne Gordon - Lady Armitage
*Bobby Connelly - Jack
*Alfred Raboch - Ralk (*as Albert Raboch)
*Ann Brody - Mouroffs Wife

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 
 