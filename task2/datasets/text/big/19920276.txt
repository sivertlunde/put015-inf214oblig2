Up from the Beach
{{Infobox Film
| name           = Up From the Beach
| image          = Up from the Beach.jpg
| image_size     =  Frank McCarthy
| director       = Robert Parrish
| producer       = Christian Ferry George Barr (novel)
| narrator       = 
| starring       = Cliff Robertson Irina Demick Red Buttons Marius Goring Slim Pickens James Robertson Justice Broderick Crawford Georges Chamarat Françoise Rosay Raymond Bussières Fernand Ledoux Robert Hoffmann
| music          = Edgar Cosma
| cinematography = Walter Wottitz
| editing        = 
| distributor    = 20th Century Fox
| released       = June 9, 1965
| runtime        = 99 minutes
| country        = United States of America/France
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1965 French-American George Barr called Epitaph for an Enemy. 

==Plot==
Following the Normandy landings at Omaha Beach, an American squad frees a group of French hostages but takes several casualties in an assault in Vierville-sur-Mer.

They capture a German officer who has treated the French in his jurisdiction with kindness, but the American sergeant discovers that no one on the busy beachhead wishes to be bothered with prisoners.

==Production==
The film was filmed in Cherbourg with a French cast and was set in the aftermath of the Normandy Landings where a group of Allied soldiers attempt to shelter Frenchmen who faced execution by the Nazis.  As the US Department of Defense did not cooperate with the film, the American soldiers were played by French soldiers. 
 The Longest Day with the film then marketed as a sequel.  Cliff Robertson said he was given the Messerschmitt Bf 108 used in the film.   Robertson claimed Zanuck wanted to make the film to showcase his girlfriend Irina Demick who had appeared in The Longest Day.  Robertson called the film "Up From the Bitch"  Irina Demick, Red Buttons and Fernand Ledoux appeared in the original
"The Longest Day".

Oskar Werner was the first choice for the German officer eventually played by Marius Goring.  Werner, a World War II Wehrmacht veteran refused on the ground that in his opinion no German officer of the time would have held such humane feelings as the officer portrayed in the film.  

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 