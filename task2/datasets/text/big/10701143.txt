Dil Bhi Tera Hum Bhi Tere
{{Infobox film
| name           = Dil Bhi Tera Hum Bhi Tere
| image          = DilBhieraHumBhiTere1960.jpg
| image_size     = 
| caption        = 
| director       = Arjun Hingorani
| producer       = Kanwar Kala Mandir, Bihari Masand
| writer         = Bihari Masand (screenplay)  Janardhan Muktidoot (dialogue, story)
| narrator       = 
| starring       = Balraj Sahni Dharmendra
| music          = Kalyanji Anandji
| cinematography = P. Isaac  Surendra Pai
| editing        = Anant Apte
| distributor    = 
| released       = 1960
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
 Hindi movie. Produced by Bihari Masand for Kanwar Kala Mandir, the film is directed by Arjun Hingorani. The film stars Balraj Sahni, Dharmendra and Kum Kum. The film was Dharmendras debut film.

==Plot==
Panchu Kumtekar lives a near-destitute lifestyle in Bombay along with his school-going brother, Shiri. He makes a living as a con-man, gambling and picking pockets with the assistant of another poverty-stricken male, Choti. He has a friend in Ashok who sells Cavendar cigarettes on busy streets by wearing stilts. One day Ashok meets with a maidservant, Sonu Mangeshkar, and both fall in love with each. Shiri is unable to pay his fees and is expelled from school but a local prostitute, Prema, comes to his assistance, much to the initial displeasure of Panchu. But he does change his mind, accepts her help, they fall in love and get married. Ashok then gets employed as a boxer with Sonus employer, starts earning enough money to support them all, and even moves into a 3 room apartment, while Panchu decides to go honest and finds work as a Peon. Then their lives are shattered when Sonus Goa-based dad falls ill, she goes to visit him via a ship, which sinks, killing everyone on board; Ashok, depressed and devastated, decides not to box anymore; Panchu decides to back to stealing, unknowingly extorts money from the mother of Police Inspector Moti, is subsequently arrested, and jailed; Prema gets run over by a horse-carriage; Shiri takes to selling candy on trains, tries to escape from a ticket-checker and falls off a running train. Will Prema and Shiri recover? and, if yes, will their lifestyle ever get better?

==Cast==
* Balraj Sahni	       ...	Panchu Dada Kumkum	       ...	Sonu Mangeshkar
* Sushil		
* Mohan Choti	       ...	Choti
* Hari Shivdasani      ...	Memsaabs Father
* Draz		
* Ishu		
* Tun Tun	       ...	Motis Mother
* Bajaj		
* Ranvir Raj	       ...	Police Inspector Moti
* Khusal		
* Jankidas	       ...	Jankidas - Teacher
* Baby Shobha          ...	Shobha Jayant - Child Artist
* Kesari		
* Fazlu		
* Nazir Kashmiri		
* Reddy		
* Usha Kiran           ...	Prema
* Dharmendra           ...	Ashok
* Anoop	               ...	Shree Kumthekar - child artist

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Mujhko Is Raat Ki Tanhai Mein" Mukesh
|-
| 2
| "Yeh Wada Karen Hum"
| Mukesh, Lata Mangeshkar
|-
| 3
| "Ankhon Mein Tujhko Chhupake Sanam"
| Lata Mangeshkar
|}

== External links ==
*  

 
 
 
 