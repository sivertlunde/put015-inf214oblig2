Kerala Varma Pazhassi Raja (film)
 
 
 
{{Infobox film
| name           = Pazhassi Raja
| image          = Pazhassi_raja_2009.jpg
| caption        = Theatrical poster Hariharan
| producer       = Gokulam Gopalan
| writer         = M. T. Vasudevan Nair
| narrator       = Mohanlal (Malayalam) Kamal Haasan (Tamil language|Tamil) Suresh Krishna Suman Linda Arsenio
| music          = Ilaiyaraaja
| cinematography = Ramanath Shetty
| editing        = A. Sreekar Prasad
| studio         = Sree Gokulam Films Sree Gokulam Release
| released       =  
| runtime        = 200 minutes 
| country        = India Malayalam
| budget         =       {{cite news |url=http://articles.timesofindia.indiatimes.com/2011-08-31/news-interviews/29949007_1_pazhassi-raja-epic-mammootty |title=Pazhassi Raja: Costliest Malayalam film |publisher=The Times of India |date=2009-10-15|accessdate=10 March 2012
|work=Business Standard}} 
| gross          =  
}}
 Malayalam List historical drama Pazhassi Raja, Sarath Kumar, Padmapriya Janakiraman|Padmapriya, Thilakan, Peter Handley Evans, Harry Key, Linda Arsenio and Jagathi Sreekumar in supporting roles. The music score and soundtrack were created by Ilaiyaraaja, while its sound design is by Resul Pookutty. The political situation of the time is portrayed from an Indian viewpoint in the film, and the locals are treated sympathetically.
 Kerala and National Film Best Background Best Audiography Special Jury Award (for Padmapriya). It received numerous other accolades including eight Kerala State Film Awards and seven Filmfare Awards South.
 Tamil as well as Hindi. The film released on 16 October 2009 across Kerala with 150 prints.  

== Plot == Kerala Varma Kottayam house. The exploitation of the native Indian resources by the Company had culminated in popular revolts against its authority across the district. With the help of Kerala Varma Pazhassi Rajas uncle Kurumbranadu ruler Veeravarma (Thilakan), who is jealous of Pazhassi Raja for his success and influence, and Rajas old companion Pazhayamveedan Chandhu (Suman (actor)|Suman), the Company act against Pazhassi Raja. This forces Pazhassi Raja to escape to the forests of Wayanad. The tribal force led by Neeli captures Assistant Collector Thomas Hervey Baber (Harry Key) and his fiancée Dora (Linda Arsenio) in the jungle, in spite of his commanders objection. The Raja treats Thomas Baber and Dora as his guest and releases them.
 Suresh Krishna). In the guerrilla battle-front, Pazhassi Raja uses the expertise of Thalakkal Chandu (Manoj K. Jayan), a Kurichya soldier, and Chandus fiancée Neeli (Padmapriya).

During the initial phases of the battle, the Company lose lots of men and money. This compels them to make a peace treaty with Pazhassi Raja. Raja agrees, hoping that this move will bring peace to the area and his people. However, the conditions of the treaty are never observed by the Company. This prompts Raja to start the battle again. Similarly, Dora leaves Thomas Baber for England, having learned the betrayal done by the Company to the Raja and despite her objection to the hanging of a father and son, who refused to reveal the location of the Raja to the Company. He forms useful alliances with many rulers and powerful families in the nearby places like Unni Mootha (Captain Raju) and his men.

Pazhassi Raja and his army successfully start the battle again. But the Company use heavily armed forces against him and succeed in luring many tribal leaders. This leads to the capture and subsequent hanging of Thalakkal Chandu as he was cheated by a tribal head (Nedumudi Venu).

The Company start hunting for Pazhassi and his army chief Edachena Kunkan. In a bloody fight, Edachena Kunkan kills Pazhayamveedan Chandhu. But he is surrounded by the Company. Instead of surrendering before them, he commits suicide. This makes Rajas army weaker. But Pazhassi Raja, even though knowing that he is going to die, goes for a last fight against the Company. After a glorious fight Pazhassi Raja is killed by the Company. The film ends with the assistant collector Thomas Baber placing the body of the Raja in a litter and his famous words: "He was our enemy. But he was a great warrior, a great man and we honour him." The Company officers respond by saluting the corpse of the Raja and acting as Pall-bearers.

== Cast ==
  Pazhassi Raja
* Sarathkumar as Edachena Kunkan Thalakkal Chandu Suresh Krishna as Kaitheri Ambu Kaitheri Makkam
* Padmapriya Janakiraman as Neeli
* Harry Key as Thomas Hervey Baber
* Linda Arsenio as Dora Baber
* Peter Handley Evans as Major James Gordon Suman as Pazhayamveedan Chandhu
* Thilakan as Kurumbranaadu Raja Veeravarma
* Jagathy Sreekumar as Kanara Menon
* Nedumudi Venu as Moopan Devan as Kannavathu Nambiar
* Lalu Alex as Emman Nair
* Captain Raju as Unni Mootha
* Jagadeesh as Bhandari
* Mamukkoya as Athan Gurukkal
* Murali Mohan as Chirakkal Raja
* Ajay Rathnam as Subeidhar Cheran
* Susheel Kumar as Sekaran Warrier
* Urmila Unni as Chirakkal Thamburatty
* Valsala Menon as Kaitheri Thamburatty Yamini as Unniamma
* Ross Elliot as Colonel Robert Bowles
* Tommy Donelly as Governor Jonathan Duncan
* Robin Pratt as Lieutenant Maxwell
*Simon Hewitt as Captain Dickinson
* Gary Richardson as Major Clapham
* Glen David Short as Major Murray 
* J. Brandon Hill as Major Stephen
 

==Production==

===Development===
M. T. Vasudevan Nair, Hariharan and Mammootty are working together after two decades; their previous association was Oru Vadakkan Veeragatha, which turned out into a landmark film in Malayalam. It was about Chanthu, a legendary warrior in the Northern Ballads. Through Pazhassi Raja, they made another biopic, on the life of Pazhassi Raja. Gokulam Gopalan was the films producer. The total budget of the film is about  27 crores, which makes it the most expensive Malayalam film ever made.  The sounds in the battle scenes of the film were recreated under Academy Award winner Resul Pookutty as he joined in the project only after its completion.

The Kerala High Court ordered the producers of the film to avail the benefit of entertainment tax concession for viewers of Pazhassi Raja.  

=== Casting ===
  as Dora Baber, fiancée of Assistant Collector Thomas Baber.    Linda is a theatre artist in New York who also acted in the film Kabul Express.   
 Suman plays Pazhayamveedan Chandu.    Biju Menon was originally cast for the role but he opted out after a few days of shooting, citing physical difficulties in shooting fight scenes.

===Filming===
The filming began on February 2007 from  .  , was short of money; I must really thank my producer, without whom a film like Pazhassi Raja would never have been made. The delay was because some of the stars were not willing to allot the extra time that was required for the film. They probably didnt realise this film would become a milestone in their careers. Making Pazhassi Raja was a great challenge for me. Directing Oru Vadakkan Veeragatha was a cakewalk, compared to this." 

===Music=== National Film Award for Best Background Score  P Sangeetha.  . TNN. Times of India Retrieved 17 September 2010 

===Track listing===
; Original version
{{tracklist
| all_music       = Ilaiyaraaja
| all_lyrics      = O. N. V. Kurup, Gireesh Puthenchery and Kanesh Punoor
| extra_column    = Artist(s)
| title1          = Mathangananamabjavasa
| note1           = Lyrics: Traditional Sanskrit Sloka
| extra1          = K. J. Yesudas
| length1         = 1:16
| title2          = Kunnathe Konnaykum
| note2           = Lyrics: O. N. V. Kurup
| extra2          = K. S. Chithra
| length2         = 5:12
| title3          = Aadiushassandhya Poothathivide
| note3           = Lyrics: O. N. V. Kurup
| extra3          = K. J. Yesudas, M. G. Sreekumar, Chandra Sekharan, Vidhu Prathap
| length3         = 5:29
| title4          = Ambum Kombum Komban Kattum
| note4           = Lyrics: Gireesh Puthenchery
| extra4          = Ilaiyaraaja, Manjari Babu|Manjari, Kuttappan
| length4         = 4:59
| title5          = Odathandil Thalam Kottum Kattil
| note5           = Lyrics: Gireesh Puthenchery
| extra5          = Chandra Sekharan, Sangeetha
| length5         = 5:07
| title6          = Aalamadankala Mythavanalle
| note6           = Lyrics: Kanesh Punoor
| extra6          = M. G. Sreekumar, Vidhu Prathap, Ashraf Thayineri, Edavanna Gafoor, Faisal Elettil, Krishnanunni
| length6         = 4:47
}}

;Tamil version (Dubbed) 
{{tracklist
| all_music       = Ilaiyaraaja Vaali
| extra_column    = Artist(s)
| title1          = Mathangananamabjavasa
| note1           = Lyrics: Traditional Sanskrit Sloka
| extra1          = K. J. Yesudas
| length1         = 1:16
| title2          = Kundrathu
| note2           = 
| extra2          = K. S. Chithra
| length2         = 5:12
| title3          = Aadi Mudhal
| note3           = 
| extra3          = Madhu Balakrishnan, Rahul Nambiar, Roshini
| length3         = 5:29
| title4          = Agilamellam
| note4           = 
| extra4          = Murali
| length4         = 4:59
| title5          = Moongil Thanni
| note5           = 
| extra5          = Karthik, Roshini
| length5         = 5:07
| title6          = Ambum Kombum
| note6           = 
| extra6          = Rahul Nambiar, Priya
| length6         = 4:47
}}

Hindi version (Dubbed)
{{tracklist
| all_music       = Ilaiyaraaja
| all_lyrics      = Manoj Santoshi and Manisha Korde
| extra_column    = Artist(s)
| title1          = Pritam Dil
| extra1          = Shreya Ghoshal
| length1         = 5:01
| title2          = Yaar Hai Badle
| extra2          = Shaan (singer)|Shaan, Kunal Ganjawala
| length2         = 4:42
| title3          = Yeh Dharti Bhi
| extra3          = Shaan, Kunal Ganjawala
| length3         = 5:16
}}

==Release==
The film was released on 16 October as a Diwali release with 130 prints in the original Malayalam version and later on 17 November with 150 prints in Tamil language|Tamil.  Later, on 27 September 2013, Goldmines Telefilms launched the trailer of the Hindi dubbed version of the film, at their YouTube channel.  The full Hindi version of the film was digitally released on YouTube on 11 October 2013. 

===Home video=== Dolby Digital progressive 24 Frame rate|FPS, widescreen and NTSC format. Within two days of its release, Pazhassi Raja broke all existing records in Malayalam home video sales by selling over two lakh units. 

== Reception ==

=== Critical response ===
Nowrunning comments that the film is an "exotic chronicle that stuns us with its fascinating tale", and that "this is the stuff that tours de force are made of".   . Retrieved 2010-10-17. 

However, Rediff also stated that "sentimentalism bogs down the pace  " at several occasions of the story.  Ilayarajas music also received criticism and the critics further accused the film of taking some cinematic liberties on history.  The Hindu said, "the host of people who play English Lords and East India Company chiefs appears theatrical. Even Linda Arsenio, the English Lady Dora Baber, isnt spontaneous" and "while on editing, certain parts seem to have been trimmed in haste and hence hang without relevance. Strangely, despite an action-oriented story and mind-boggling stunts, the film sags at points."    Other critics pointed the sloppy fight scenes and the English accent of Padmapriya who has dubbed for herself in the film. 

===Box office===
Kerala Varma Pazhassi Raja had a record opening, collecting   in its first weekend, which was the highest ever weekend recorded for a film in Kerala, at the time it was released.    By the sixth week of release, the film had collected   in Kerala.    According to the films producer, it grossed   by September 15, 2010.    However, on Aug 31, 2011, Times of India reported that that the film grossed a sum close to  .   

===Accolades===
The film has received many major accolades upon release. The list includes:
{| class="wikitable" style="font-size:95%;" ;
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Organisation
! Award
! Category
! Name
|-
|rowspan=4| Directorate of Film Festivals  National Film Awards National Film Best Feature Film in Malayalam Gokulam Gopalan
|- National Film Special Jury Award Padmapriya Janakiraman|Padmapriya
|- National Film Best Background Score Ilaiyaraaja
|- National Film Best Audiography Resul Pookutty
|- style="border-top:2px solid gray;"
|rowspan=8| Kerala State Chalachitra Academy 
|rowspan=8| Kerala State Film Award Kerala State Best Director Hariharan (director)|Hariharan
|- Kerala State Best Screenplay
|M. T. Vasudevan Nair
|- Kerala State Second Best Actor Manoj K. Jayan
|- Kerala State Second Best Actress Padmapriya Janakiraman|Padmapriya
|- Kerala State Best Film Editor
|A. Sreekar Prasad
|- Kerala State Best Art Director Muthuraj
|- Kerala State Best Costume Designer Natarajan
|- Kerala State Best Dubbing Artist Shoby Thilakan
|- style="border-top:2px solid gray;"
|rowspan=7| Filmfare  
|rowspan=7| Filmfare Awards South Filmfare Award Best Film
|
|- Filmfare Award Best Director Hariharan (director)|Hariharan
|- Filmfare Award Best Supporting Actor Manoj K. Jayan
|- Filmfare Award Best Supporting Actress Padmapriya Janakiraman|Padmapriya
|- Filmfare Award Best Lyricist
|O. N. V. Kurup
|- Filmfare Award Best Male Playback
|K. J. Yesudas for "Aadiushassandhya Poothathivide"
|- Filmfare Award Best Female Playback
|K. S. Chithra for "Kunnathe Konnaykum"
|- style="border-top:2px solid gray;"
|rowspan=9| Annual Malayalam Movie Awards 
|rowspan=9| Annual Malayalam Movie Awards (Dubai) Best Movie
|
|- Best Director Hariharan (director)|Hariharan
|- Best Script Writer
|M. T. Vasudevan Nair
|- Best Actress Kanika (actress)|Kanika
|- Best Supporting Actor Manoj K. Jayan
|-
| Best Special Performance Padmapriya
|- Best Music Director Ilaiyaraaja
|- Best Singer (Female)
|K. S. Chithra
|- Best Sound Effects & Engineering
| Resul Pookutty and Amrit Pritam
|- style="border-top:2px solid gray;"
|rowspan=3| Asianet 
|rowspan=3| Asianet Film Awards Best Film
|
|- Millenium Actor Mammootty
|- Best Singer (Female)
|K. S. Chithra
|- style="border-top:2px solid gray;"
|rowspan=6| Mathrubhumi Amrita TV 
|rowspan=6| Mathrubhumi-Amrita Film Awards Best Film
|
|- Best Actor Mammootty
|- Best Supporting Actor Manoj K. Jayan
|- Best Supporting Actress Padmapriya
|- Best Sound Effects & Engineering Resul Pookutty
|- Best Lyricist
|O. N. V. Kurup
|- style="border-top:2px solid gray;"
|rowspan=7| Surya TV 
|rowspan=7| Surya Film Awards Best Script Writer
|M. T. Vasudevan Nair
|- Best Actor Mammootty
|- Best Actress Padmapriya
|- Best Art Director Muthuraj
|- Best Make-up man George
|- Best Costume designer Natarajan
|- Best Male Dubbing Artiste Shoby Thilakan
|- style="border-top:2px solid gray;"
|rowspan=6| Vanitha 
|rowspan=6| Vanitha Film Awards Best Script Writer
|M. T. Vasudevan Nair
|- Best Actor Mammootty
|- Best Actress Padmapriya
|- Best Supporting Actor Sarath Kumar
|- Best Male Singer
|K. J. Yesudas
|- Best Female Singer
|K. S. Chithra
|- style="border-top:2px solid gray;"
|rowspan=4| Kalakeralam 
|rowspan=6| Kalakerala Film Awards Best Film
|
|- Best Actor Mammootty
|- Second Best Actor Manoj K. Jayan
|- Best Music Director Ilaiyaraaja
|}



==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 