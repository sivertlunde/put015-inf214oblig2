List of lesbian, gay, bisexual or transgender-related films of 1969
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1969. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==1969==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|- The Damned|| 1969 ||   ||     || Drama||
|- aka Bara no sôretsu
|-
| || 1969 ||   ||  || Comedy||
|- Jagdszenen aus Niederbayern|| 1969 ||   || West  || Drama||
|-
|Midnight Cowboy|| 1969 ||   ||  || Drama||
|-
|Fellini Satyricon|| 1969 ||   ||   || Drama, fantasy||
|-
|Staircase (film)|Staircase|| 1969 ||   ||      || Comedy ||
|- Venus in Furs|| 1969 ||   ||       || Horror, thriller || aka Paroxismus ||
|- Women in Love|| 1969 ||   ||   || Drama, romance ||
|}

 

 
 
 