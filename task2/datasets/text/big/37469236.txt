2001: Do Hazaar Ek
{{Infobox film
| name           =2001: Do Hazaar Ek
| image          = 
| image_size     = 
| caption        = 
| director       =  
| producer       = 
| writer         =
| narrator       = 
| starring       = Dimple Kapadia
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 20 February 1998
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1998 and stars Dimple Kapadia, Jackie Shroff, Rajat Bedi And Tabu (actress)|Tabu.    

==Plot==

Following the brutal slaying of a call-girl named Julie in Bombay city, two police inspectors, namely Anil Sharma and Rajat Bedi, are assigned to investigate and bring the culprit(s) to book. Anil & Rajat note that the killer leaves "2001" marked on the body of the victim. Anil comes to know that the man staying in Julies room is a prominent Member of State Parliament, Ramaswamy, and would like to list him as a suspect. Julies death is followed by more killings in the same style, and killed are Advocates Sarkari and Kajal, and a man named Krishna Rao. Then Ramaswamy himself is attacked, but survives and is hospitalized. When he regains consciousness for a brief period of time, he points an accusing finger at Rajat and then relapses. Not wanting to take any chance, Police Commissioner Malik has Rajat under house arrest. Then Anil comes across some evidence that links the killing to none other than Malik himself, while Rajat starts to gather together evidence against Anil. The question remains: Is the killer one of these police officers? If so, which one, or are all three collectively involved in these gruesome deaths?

==Soundtrack==
{| class="wikitable"
! # !! Tracklist !! Singers
|-
| 1 || "Teri Yeh Bindiya" || Hariharan (singer) | Hariharan, Preeti Uttam Singh
|-
| 2 || "Tu Quatil Tera Dil Qatil" || Udit Narayan, Kavita Krishnamurthy
|-
| 3 || "Teri Meri Dosti (Happy Version)" || Kumar Sanu, Udit Narayan
|-
| 4 || "Teri Meri Dosti (Sad Version)" || Mohammed Aziz
|-
| 5 || "Yahi To Pyar Hai" || Mohammed Aziz, Kavita Krishnamurthy
|-
| 6 || "Rappa Rappa Rum Pum Pum" || Aditya Narayan
|-
| 7 || "Neend Udaye" || Alka Yagnik
|-
| 8 || "Yun Na Rootho" || Kumar Sanu, Anuradha Paudwal
|}
==References==
 

==External links==
*  

 
 
 


 