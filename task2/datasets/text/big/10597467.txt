Evil Toons
 
{{Infobox film
| name = Evil Toons
| image = Evil Toons poster.jpg
| image_size = 215px
| alt =
| caption = Poster
| director = Fred Olen Ray
| producer = Fred Olen Ray Victoria Till
| writer = Fred Olen Ray Stacey Nix Dick Miller
| music = Chuck Cirino
| cinematography = Gary Graver
| editing = Greg Shorer
| studio = American Independent Productions Curb/Esquire Films
| distributor = Prism Entertainment Corporation
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget = $140,000
}} Horror B-movie written and directed by Fred Olen Ray.  The film is a light spoof of traditional haunted-house films. 

==Plot==
A group of teenage girls spend the night in an old house. On the night of their arrival, a strange man arrives at the house, delivering an old book. Once he is gone, the girls examine the book, where they find a sketch of a cartoon monster.

As time passes, the drawing emerges from the book and becomes a cartoon. At one point, after attacking one of the girls, it eats her and takes her form. The monster begins slaying the occupants of the house, including  her boyfriend, the owner of the house, and two of the girls. Megan (the only surviving girl) and Gideon Fisk (owner of the book) burn the tome, destroying the demon. The film ends as Gideon leaves, and everyone comes back to life, Megan screams in terror when Mr. Hinchlow (the neighbor) comes by and brings a portable television so that the group of girls can watch Saturday morning cartoons.

==Cast==
* David Carradine as Gideon Fisk
* Monique Gabrielle as Megan
* Madison Stone as Roxanne
* Barbara Dare (credited as Stacey Nix) as Jan
* Arte Johnson as Mr. Hinchlow
* Dick Miller as Burt
* Suzanne Ager as Terry
* Don Dowe as Biff
* Michelle Bauer as Mrs. Burt
* Fred Olen Ray as Cartoon Monster

==Release==
On May 4, 2010, Infinity Entertainment Group released the 20th Anniversary Edition on DVD. 

==Reception==
The film has received a number of negative reviews, earning a Rotten Tomatoes approval rating of 29%. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 