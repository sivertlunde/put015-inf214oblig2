Piñata (film)
{{Infobox Film |
name = Piñata |
image = Piñata_promotional_screen.png|
caption = Promotional shot of the Piñata|
producer = Thomas Schober |
director = Mike Hollands |
writer = Mark Angeli Alex Hammond | Mark Mitchell voice  Alice Hollands - Voice acting|voice|
music = Niko Schauble|
cinematography = |
editing = Mike Hollands|
studio = Act3animation |
distributor = Australian Film Commission Alpha Media | 2005 |
runtime = 4 min. | English |
budget = |
}}
 Short & Curly. 

==Plot summary== Mark Mitchell) suspended from a rope on a tree, awakens as if coming to life for the first time. It hears the noise of children and wants to join them, but cant (since its tied to the tree). Soon though, a girl (voiced by Alice Hollands), with only her sombrero showing, wielding a large (but not quite large enough) stick approaches and begins swinging unsuccessfully at the piñata. Shortly, another child shows up with a stick that can reach it and the piñata, to its surprise, gets hit across the face. To its horror, more children (a group of sombreros) show up and attack it. This happens a few more times, until the piñata climbs up its rope out of their reach using its teeth. The children are dismayed, until an adult, represented by a larger sombrero, comes in with a stick large enough to reach the piñata. The adult winds up for the hit and begins shaking. The piñata also begins shaking. Just before the adult strikes, the piñata, in a great state of fear and panic, is struck by a vicious bout of diarrhea, which for a piñata is in the form of candy. The children rejoice and the piñata is relieved, since it is now left alone. But to its surprise, as the film finishes, it is attacked once more by the original little girl, who has acquired the large stick used by the adult.

==Awards==

===Won===
*2005 - Vidfest.

===Picked for===
*2005 - Annecy International Animated Film Festival  AFI Awards - Best Short Animation  
*2005 - Flickerfest 
*2005 - SIGGRAPH 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 


 