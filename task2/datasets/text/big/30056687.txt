Stumped (film)
 
 
{{Infobox film
| name           = Stumped
| image          = Stumped_(2003_film).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Gaurav Pandey
| producer       = Raveena Tandon
| writer         = Gaurav Pandey
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist |
* Alyy Khan
* Raveena Tandon
}}
| music          = Pritam Chakraborty
| cinematography = Debu Deodhar
| editing        = Sanjib Datta
| studio         = Reel Life Entertainment
| distributor    = Shemaroo Entertainment
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Stumped is a 2003 Bollywood sport/drama film written and directed by Gaurav Pandey and produced by Raveena Tandon.     This film is a debut for Raveena as producer.  Sachin Tendulkar had a cameo appearance in the film.  

Despite a notable promotional campaign, the film had little impact after its release. 

==Cast==
{| class="wikitable"
|-
! Cast !! Role
|- Alyy Khan|| Major Raghav
|- Raveena Tandon|| Reema Seth
|- Sachin Tendulkar|| Himself
|- Viju Khote||
|- Suresh Menon|| Killer
|- Salman Khan|| SP. App in song
|- Anjan Srivastav|| the guy on brothel
|}

==Plot==
Reena and her husband Major Raghav Seth; Ranga Khetrapal, Swadesh Deshpande and their respective wives; Laltu Singh, his wife, Neetu, and son, Bhola; the building watchman, Dukhiram; bachelor Subramaniam; and a visually-challenged Holy man, Baba, are some of the residents in Happy Home Society. The common interest shared by most of them is their love of playing and watching cricket. Reena experiences isolation after her husband is called back to Kargil to battle terrorists. She is befriended by three youngsters who have a crush on her. Things, however, change dramatically after she gets word that her husband is missing and believed dead.

==References==
 

==External links==
 

 
 
 
 
 
 


 
 