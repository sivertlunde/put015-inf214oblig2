Antigone (film)
{{Infobox film
| name           = Antigone
| image          = Antigone Movie Poster.jpg
| image_size     = 
| caption        = The promotional poster for the English-subtitled video. Yorgos Javellas
| producer       = Demetrios Paris Yorgos Javellas (adaptation)
| narrator       = 
| starring       = Irene Papas Manos Katrakis
| music          = Arghyris Kounadis
| cinematography = Dinos Katsouridis
| editing        = 
| distributor    = Kino Video (Region 1 DVD)
| released       = 1961
| runtime        = 93 minutes
| country        = Greece
| language       = Greek
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1961 cinema Greek film adaptation of the Ancient Greek tragedy Antigone (Sophocles)|Antigone by Sophocles. It stars Irene Papas in the title role.

The film follows the story of the play closely, but ends differently–instead of Creon retiring back to the palace as in the play, the film ends with Creon retiring his kingship and exiling himself out of Thebes. 

It was entered into the 11th Berlin International Film Festival.   

==Cast==
* Irene Papas - Antigone
* Manos Katrakis - Creon
* Maro Kontou - Ismene
* Nikos Kazis - Haemon
* Ilia Livykou - Eurydice
* Giannis Argyris - A Sentry
* Byron Pallis - A Messenger
* Tzavalas Karousos - Tieresias
* Thodoros Moridis - First Elder of Thebes
* Giorgos Vlahopoulos - Elder of Thebes
* Yorgos Karetas - Elder of Thebes
* Thanasis Kefalopoulos - Elder of Thebes

==See also==
*List of films based on military books (pre-1775)

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 


 
 