Dangerous When Wet
{{Infobox film 
  | name           = Dangerous When Wet
  | image          = Dangerous-when-wet.jpg
  | caption        = Theatrical release poster
  | director       = Charles Walters George Wells
  | writer         = Dorothy Kingsley
  | narrator       = 
  | starring       = Esther Williams   Fernando Lamas   Jack Carson
  | music          = Albert Sendrey George Stoll
  | cinematography = Harold Rosson
  | editing        = John McSweeney Jr.
  | distributor    = Metro-Goldwyn-Mayer
  | released       =   
  | runtime        = 95 minutes
  | country        = United States English
  | budget         = $1,465,000  . 
  | gross          = $3,255,000  
  | preceded_by    = 
  | followed_by    = 
}}
Dangerous When Wet (1953) is an Technicolor Metro-Goldwyn-Mayer musical film starring Esther Williams, Fernando Lamas, and Jack Carson, directed by Charles Walters, and featuring an animated swimming sequence starring Williams with the famous cat-and-mouse duo, Tom and Jerry.

==Plot summary==
Katie Higgins (  Windy Weebe (Jack Carson|Carson) who is instantly smitten. Weebe sells an elixir that purports to turn the user into a peppy, fit-as-a-fiddle specimen, and upon noticing the entire familys strength in the water, suggests that they all attempt to swim the English Channel. The family and Weebe head off to England whereupon they learn that the distance to be conquered is 20 miles "as the seagull flies" but with the currents, can be up to 42 miles. Katie is the only one in the family strong enough to attempt this feat, so she begins training with Weebe as her coach.
 American and begins trying to woo her. Katie tries to stay focused on her swim, but is being pulled in different directions by the two men. In a dream sequence, Katie does an underwater ballet with cartoon characters Tom and Jerry, as well as animated depictions of the different people in her life. The film ends happily with Katies attempt to cross the Channel and the resolution of her gentlemen issues.

==Cast==
*Esther Williams as Katie Higgins
*Fernando Lamas as André Lanet
*Jack Carson as Windy Weebe
*Charlotte Greenwood as Ma Higgins
*Denise Darcel as Gigi Mignon
*William Demarest as Pa Higgins
*Donna Corcoran as Junior Higgins
*Barbara Whiting as Suzie Higgins
*Bunny Waters as Greta
*Henri Letondal as Joubert
*Paul Bryar as Pierre
*Jack Raine as Stuart Frye Richard Alexander as Egyptian Channel swimmer
*Tudor Owen as Old Salt
*Ann Codee as Mrs. Lanet Norwegian Swimmer
*William Hanna as Tom Cat(uncredited)

==Production==
 |250px]]
In the underwater sequences in which Williams speaks to Tom and Jerry, Joseph Barbera had pink bubbles drawn coming out of her mouth for a cost of $50,000. 
 Aquacade partner) dove into the water to swim alongside Florence Chadwick, whom he was coaching. 

===Casting===
Debbie Reynolds was originally slated for the role of Williamss little sister, Suzie.   

Though Williams had heard of Lamas before his being cast as her love interest in the film, the two had never been formally introduced. In 1969, the two married, and remained so until Lamass death in 1982. When asked whether she knew him when the studio suggested his name for a possible co-star, Williams mentioned that "he starred in movies with  , 2000, ISBN 0-15-601135-2, ISBN 978-0-15-601135-8 

==Release==
According to MGM records the film earned $2,230,000 in the US and Canada and $1,025,000 elsewhere recording a profit of $386,000. 

===Critical reception===
Bosley Crowther wrote in a 1953 New York Times review the "frolicsome item not only dumps you quite often in the drink, but also gives you some rather pleasant company to clown around with while on dry ground. As we say, there is nothing very special or spectacular about "Dangerous When Wet," but it comes as relaxing entertainment at this torpid time of the year."   

A review from Variety called it "a light mixture of tunes, comedy, water ballet and Esther Williams in a bathing suit." 

===Home media===
On July 17, 2007, Warner Home Video and Turner Entertainment released Dangerous When Wet on DVD as part of the Esther Williams Spotlight Collection, Volume 1. The 5-disc set contained digitally remastered versions of several of Williamss films including Bathing Beauty (1944), Easy to Wed (1946), On an Island with You (1948) and Neptunes Daughter (1949 film)|Neptunes Daughter (1949) 

==In Popular Culture==
*Several Tom & Jerry shorts have bits from the music and songs from the Tom and Jerry scene in film played in their score.
*In the Tom and Jerry Tales episode, Octo Suave, The Octopus bears a similar resemblance to the one in the film and his name is revealed to be Morizzio.

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 