The Scar of Shame
{{Infobox film
| name           = 
| image          = Image:Movie scar of shame.jpg|right|thumb|
| alt            = 
| caption        = Scar of Shame title card
| film name      = 
| director       = Frank Perugini 
| producer       = David Starkman 
| writer         = David Starkman 
| screenplay     = 
| story          = 
| based on       = 
| starring       = Harry Henderson Norman Johnstone Ann Kennedy  
| narrator       = 
| music          = 
| cinematography = Al Ligouri  
| editing        = 
| studio         = Colored Players Film Corp.  
| distributor    = State Rights  
| released       = 1927
| runtime        = 8 reels
| country        = United States Silent
| budget         = 
| gross          = 
}}

The Scar of Shame is a silent film, which was filmed in 1926 and released in 1929 in film|1929.
 black cast performed a feature film specifically for a black audience. The film was produced and written by David Starkman and was directed by Frank Peregini, both white.

==Introduction==
The Scar of Shame was a black silent melodrama with black actors written for a predominately black audience.   Cripps, Thomas. Black Film as Genre. Indiana University Press, 1978.  Print Melodramas were the genre of choice for early 20th century black filmmakers.  This film emerged during a time of great breakthroughs in not only African American film but all art with the Harlem Renaissance when “a new sense of black consciousness emerged”   likely after witnessing the bravery of African American soldiers in World War I.  This was a collaboration of a “black cast, white crew and interracial production team”  Cripps, Thomas. Black Film as Genre. Indiana University Press, 1978.  Print.   produced by the conspicuously named “Colored Players” who were mostly white, in 1927.  The Scar of Shame was one of the only three films produced by this company which was founded in 1926 in Philadelphia by a generous investment.   

In Jane Gaine’s article on Skin Color and Caste in Black Melodrama,” she claims that the film mostly takes its substance from D.W. Griffith’s films: Intolerance (1916) and Broken Blossoms (1919).   Gaines, Jane. “The Scar of Shame: Skin Color and Caste in Black Silent Melodrama.” Representing Blackness. Ed. Valerie Smith. New Jersey: Rutgers University Press, 1997. 61-82. Print   There are undeniable similarities between the films with scenes such as Spike beating Louise matching up remarkably with paternal beatings of the character Lucy in Broken Blossoms and so forth.

==Plot==
Alvin sees Louise being beaten by Spike outside while practicing piano and knocks him unconscious, rescuing the damsel in distress then brings her back to Lucretia’s house. Lucretia, the owner of the boarding house where Alvin is residing allows Louise to stay, to protect her, in return for chores around the house.  Eddie meets with Spike, who is sporting a splendid black eye he claims to have been caused by a trolley car, after the incident and attempts to convince him to let Louise work as an entertainer for him.  Spike doesn’t seem enthusiastic and shows regret for beating her, which he would later credit to alcoholism.  Spike has some desire to allow his daughter to escape the kind of life he is stuck in but is unable to change any of his actions without being sucked back into his old lifestyle by booze given to him by Eddie. 

Eddie learns the truth about the confrontation between Alvin and Spike during dinner at Lucretia’s. Later in the evening, Eddie forcefully attempts to bring Louise back to her “old pappy” but again Alvin intervenes. Drunk again off Eddie’s liquor, Spike continues to harass Louise who contemplates suicide if it continues, foreshadowing events to come. Alvin proposes to Louise in a touching scene on the bed after rescuing her again form the altercation claiming that she wouldn’t need to worry about harm if they were married. After he defends Louise from Eddie at Lucretia’s house, Alvin exclaims, “I’ll teach you to treat our women like that!”

Over more alcohol, Eddie schemes with Spike to distract Alvin with a fake telegram announcing his mother’s illness while they kidnap Louise.  Alvin cannot take Louise with him because he hasn’t informed his mother of their marriage which she would not have approved of because of her concern with castes.  Louise laments her lot in life and finds a letter from Alvin’s mother urging him to marry another woman who is “part of our set,” referring to the same level in black urban social stratification.  She proceeds to rip the letter and then the marriage certificate. 

Before they go through with the plan, Spike once again hesitates, remarking that she is better off away from people like him. Alvin comes back to confront Eddie after learning he had been tricked, and that his mother was visiting friends out of town.  The scene is cut back and forth between Alvin in the car in the suburbs and Louise tearing up mementos of their marriage. Eddie breaks into the house and entices Louise with long shot possibilities of becoming rich.  As Alvin enters and guns are pulled, someone accidentally shoots Louise in the neck leaving a scar.  Louise becomes in cahoots with Eddie in his gambling schemes while Alvin is in prison.  Eddie refers to Alvin as a “dicty sap” which insults his ambitions to move up the rungs of the caste system. 

Alvin escapes prison by filing the bars in his cell and re-establishes himself as a music instructor with a false name. Alvin falls in love with his student, Alice, but “lives a daily lie” because he has hidden the secrets of his past.  Louise is involved with Alice’s father so Alvin meets her after dropping an urgent note to Alice’s father.  Alice’s father unknowingly pairs the two together for a dance.  Later that night, Louise makes advances on Alvin, threatening to expose him and he gives in for a moment but in a later scene Alvin rejects her and leaves.  In distress, Louise kills herself after writing a revealing letter of repentance and apology. In it she confesses that it was really Eddie who shot her neck and he wouldn’t allow her to tell the truth during the trial. 

Alvin feels compelled to let Alice and her family in on his secrets after hearing of Louise’s death and they forgive him.  Alice’s father’s lament mirrors the earlier foreword, in blaming the environment and Louise’s lack of education, finishing with the statement: “our people have much to learn.” 

==Social insight and significance==
In essence, this is a film detailing the struggle to advance from the depths of black urban life representing bluntly by the ghetto environment, to fabled high class society seen when Alvin visits his mother’s house in the suburbs.  With the new emergence of a black bourgeois class, the film provides “a manual for those on the make,” embodied in Alvin Hilliard, but also “a caution to the weak willed who might be diverted from success by urban temptations,”  such as Louise who is tempted by the opportunity of a “big break” as a cabaret singer and also her father, Spike, who is unable to resist alcohol. 

The message is not so straightforward however and we see those who strive for more ending up hurting those they care about when Alvin gives Louise “the scar,” and deserting those less fortunate of the same race just by a desire for success. The Scar of Shame gives us insight into a “detailed anatomy of the conflicting strata of black urban life,”   Cripps, Thomas. Black Film as Genre. Indiana University Press, 1978.  Print.  so that we see those that would be called “strivers” in conflict with those who feel unable to leave their place in society or are resentful towards strivers who abandon them.

There is also the problem of moving up simply by being assimilated into white culture, seen when Alvin seems to only compose “white music” on the piano, and losing track of one’s black heritage.  There is no clearly distinct skin color differentiation that sets some characters above others even though commonly a darker skin tone is associated with lower class whereas those more close to white skin are higher class in film.  The message here is that it is not the skin that determines the character’s fate but rather the ambition in their hearts.  Though this may be as true as we hope it is, if one wants to split hairs, it could be found that Alice is slightly more white and portrayed in more alluring camera shots than Louise and Spike, one of the more base characters in the film and the butler at Alvin’s mother’s estate are darker than the rest. 

Though this film is a commentary on the anatomy of black social stratification, it cannot shut out the racism of its era.  Wealthy white investors hiring black crews only added to the stark divide between races in the film industry.  The reality was, the more “white blood” one inherited, the more successful they were thought to be.  Later more emphasis would be put on income, education and achievements of course but this idea of darkness as a marker of insignificance still somewhat permeated the film. 

A common theme throughout the film is the conflict between the top and bottom classes and their role in the degradation of black women.  It seems that the lower classes were thought to act as sort of a whirlpool, dragging others down with their sinful temptations and clubs of ill repute and loose morals.  Simultaneously there was a top class that tried so hard to separate itself from the rest that it ended up not allowing for those trapped below to move up by “holding up impossible hurdles.”   Gaines, Jane. “The Scar of Shame: Skin Color and Caste in Black Silent Melodrama.” Representing Blackness. Ed. Valerie Smith. New Jersey: Rutgers University Press, 1997. 61-82. Print  The peak of this conflict is seen during the plan conceived by Eddie to kidnap Louise while Alvin is distracted at his mother’s. Louise is caught in the middle in the house while Eddie and Spike, here representing the lower class, sneak up on her and Alvin, coming back from the suburbs and his mother who is “concerned with caste” represents higher class.  She feels somewhat shunned from Alvin’s side after reading his mother’s letter, which expressed her desire for him to marry another, more sophisticated girl while she is simultaneously drawn in by the allure of a business deal with Eddie.  The scar, which she receives during the ensuing gunfight, marks and makes final her decision to become corrupted by the lower class.  Just as Louise feels oppressed by high class and unable to be on the same level as Alvin, Alvin feels as if he is being dragged down by the shame of his past and unable to open up to Alice, his high-class love. 

What allows some to move up while most are trapped then?  The drive to succeed seems not to be enough on its own.  One wonders after seeing the first title card, which puts so much importance on childhood as a determining factor of success in life, how is such a fate determined so early?  What does the “knowing hand” know that is so critical to success in life?  If we apply this to main characters we see that Alvin had success in life and his mother impressed on him from an early age the importance of staying in one’s “set,” that is to say avoid the suction of the lower class.  Though it is true Alvin married Louise, there was never a time when love was apparent.  The desire seemed to come more from Alvin’s gentleness and protective attitude, almost pitying Louise.  Later, Alvin pulls a gun on her with Eddie and tells them to hold still “the two of you.”  Clearly there was little love lost in the breakup.  If Alvin had heeded his mother’s advice, his life might have turned out differently for it was almost ruined by the touch of the ghetto.  The dealings of Eddie and Spike get him stuck in jail and force him to live the life of an escaped convict for the rest of his life.  This ideal of self-preservation is the justification for suppressing the lower class and making wide divides between the black urban strata to minimize the risk of the ill effects of associating with low class culture. 

The image of childhood dreams appears again when Louise decides to leave Alvin.  Alvin drops Louise’s childhood doll down to the floor as he leaves for his mother’s and she leaves it there as a neglected victim of a poor environment.  Gaines argues that the doll or “child” rather, is not a victim of caste but rather of “class prejudice,”   Gaines, Jane. “The Scar of Shame: Skin Color and Caste in Black Silent Melodrama.” Representing Blackness. Ed. Valerie Smith. New Jersey: Rutgers University Press, 1997. 61-82. Print  and that it is the grudges of high-class black society and the resentment of low-class society that are the root of this system that is cause of the demise of so many lives.

==Library of Congress==
The film has been preserved by the Library of Congress, but it is not on the National Film Registry preservation list. On all Library of Congress VHS/DVD prints, The Scar of Shame is accompanied by a 1923 short film, in which Noble Sissle sings jazz tunes while Eubie Blake plays the melody on the piano. The short film, called "Sissle and Blake" by the Library of Congress, was filmed in the Lee DeForest Phonofilm sound-on-film process, and is one of the earliest examples of sound-on-film technology.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 