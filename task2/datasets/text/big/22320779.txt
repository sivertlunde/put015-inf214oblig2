Metti, una sera a cena
 
{{Infobox film
| name           = Metti, una sera a cena
| image          = Metti una sera a cena.jpg
| caption        = Film poster
| director       = Giuseppe Patroni Griffi
| producer       = 
| writer         = Dario Argento Carlo Carunchio Giuseppe Patroni Griffi
| starring       = Jean-Louis Trintignant
| music          = Ennio Morricone
| cinematography = Tonino Delli Colli
| editing        = Franco Arcalli
| distributor    = 
| released       =  
| runtime        = 125 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Metti, una sera a cena (aka "One Night at Dinner" and "Love Circle") is a 1969 Italian drama film directed by Giuseppe Patroni Griffi. It was entered into the 1969 Cannes Film Festival.   

==Plot==
Michele (Jean-Louis Trintignant) is a successful bourgeois playwright who fantasizes an affair between his beautiful wife, Nina (Florinda Bolkan), and his best friend, Max (Tony Musante), a bisexual actor. Unbeknownst to him, the pair have in fact been lovers for years, though Max is really in love with Michele. While Nina is occupied with Max, Michele drifts into an affair with a rich, but lonely, single woman (Annie Girardot). The four meet regularly for dinner at the home of Michele and Nina where they indulge in bored, amoral conversation.

As a diversion, Max suggests to Nina that they add a third player to their bedroom games: Ric (Lino Capolicchio), Maxs anarchist/poet boyfriend who lives in a dankly luxurious basement and sells himself to both men and women. After a while, Ric finds himself falling in love with Nina and eventually attempts suicide over her. Nina discovers Ric in time to save him and decides to leave Michele to live with Ric. But soon their relationship withers and Nina returns to her husband. Its then that Michele decides to invite Ric into the circle, as they go on meeting at dinner and playing their games of love and seduction.

==Cast==
* Jean-Louis Trintignant as Michel
* Lino Capolicchio as Ric
* Tony Musante as Max
* Florinda Bolkan as Nina
* Annie Girardot as Giovanna
* Silvia Monti as Actress at Press Conference Milly as Singer
* Adriana Asti as Stepdaughter
* Titina Maselli as Mother
* Ferdinando Scarfiotti as Son
* Claudio Carrozza as Baby
* Nora Ricci as 1st Actress
* Mariano Rigillo as Comedian
* Antonio Jaia as Young Actor

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 