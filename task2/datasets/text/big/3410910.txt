Night After Night (film)
{{Infobox film
| name           = Night After Night
| image          = Night After Night.jpg
| caption        = 
| director       = Archie Mayo
| producer       = 
| writer         = Louis Bromfield Kathryn Scola
| narrator       = 
| starring       = George Raft Constance Cummings Mae West
| music          = 
| cinematography = 
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 1932 American drama film starring George Raft, Constance Cummings, and Mae West in her first movie role. Others in the cast include Wynne Gibson, Alison Skipworth, Roscoe Karns, Louis Calhern, and Bradley Page.
 Cosmopolitan magazine story Single Night by Louis Bromfield, with West allowed to contribute to her lines of dialogue.

Although Night After Night is not a comedy film|comedy, it has many comedic moments, especially with the comic relief of West, who plays a supporting role in her screen debut. West portrays a fictionalized version of Texas Guinan and the film remains primarily remembered as the launching pad for her career. Raft campaigned to cast his friend and former employer Guinan herself but the studio opted for West since she was nine years younger. Raft believed that the part would have launched a major film career for Guinan, which proved to be the case for West instead. (West was reportedly a fan of Guinan and incorporated some of the flamboyant Guinans ideas into her own acts). Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 30 

==Plot==
Joe Anton (Raft) is a speakeasy owner who falls in love with socialite Miss Healy (Cummings). He takes lessons in high-class mannerisms from Mrs. Jellyman (Skipworth). Joe does not know that Miss Healy only pays attention to him because he lives in the fancy apartment that her family lost in the Wall Street Crash of 1929. Joe decides to instead pursue his old flame Iris Dawn (Gibson) just as Miss Healy begins to genuinely fall in love with him. 

==Cast==
 

* George Raft as Joe Anton
* Constance Cummings as Miss Jerry Healy
* Wynne Gibson as Iris Dawn
* Mae West as Maudie Triplett
* Alison Skipworth as Miss Mabel Jellyman
* Roscoe Karns as Leo
* Louis Calhern as Dick Bolton
* Bradley Page as Frankie Guard Al Hill as Blainey
* Harry Wallace as Jerky
* George Templeton as Patsy
* Marty Martyn as Malloy Tom Kennedy as Tom (The Bartender)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 