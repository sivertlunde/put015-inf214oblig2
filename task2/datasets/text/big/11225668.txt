Dark River (1990 film)
 
{{Infobox film
| name           = Dark River
| image          =
| image_size     =
| caption        =
| director       = Malcolm Taylor
| producer       = Malcolm Taylor
| writer         = Malcolm Taylor
| narrator       =
| starring       = Tom Bell, Kate Buffery, Siân Phillips, Ian McNeice, Michael Denison, Freddie Jones
| music          = Keith Miller
| cinematography =
| editing        = Stuart Taylor
| distributor    =
| released       = 1990
| runtime        = 1 hr. 40 min.
| country        =
| language       =
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Tom Bell, Siân Phillips, Kate Buffery, Ian McNeice and Tony Haygarth, among others. Dark River was an official selection at the 1990 Montreal Film Festival and the Ghent Film Festival of the same year.

==Plot summary==
On the banks of a river flowing through the lowlands of Africa, the British have set up a colony without the constraints of European civilization. Into this tight-knit community a stranger (Deacon) strays, refusing to abide by its self-serving code. He is immediately set at odds with the locals: a businessman (K B Priestley) who employs his authority in the community to disguise his personal weaknesses, his daughter (Lydia) who uses the men in the community as rungs in her climb out of provincialism; a sour, manipulative club barman (Yorkie); a widow and cocoa heiress (Mrs Blessington) who plays puppet-master from a distance; hero of the bars (Oliver) with his dubious investment schemes, sardonic gossip columnist Jane Audeby, and club waiter Elias who becomes his most candid counsellor.

Deacons big mistake is to write and publish a novel which draws on their weaknesses. The novel becomes a success back home, causing problems for its author. Before sunrise one morning a boat sets sail upriver. In it are First Secretary Hugo Shrike and Deacon, who has made a rendezvous somewhere on the banks with the person he has vowed to destroy. By nightfall the community will number one person fewer. Dark River invites the viewer to share the vantage point of Deacon as he is cheated, manipulated, seduced and misled in a series of long, no-holds-barred speeches directly to camera, while all the while the river winds its way round the protagonists like a strangling ivy.

==External links==
*  

 

 
 
 

 