Vanya on 42nd Street
{{Infobox film
| name           = Vanya on 42nd Street
| image          = Vanya On 42nd Street.jpg
| caption        = Vanya on 42nd Street film poster
| writer         = Andre Gregory (from the text Uncle Vanya by Anton Chekhov, adapted by David Mamet) Brooke Smith Madhur Jaffrey
| director       = Louis Malle
| producer       = Fred Berner
| music          = Joshua Redman
| cinematography = Declan Quinn
| editing        = Nancy Baker
| distributor    = Sony Pictures Classics
| released       = September 13, 1994
| runtime        = 119 min.
| language       = English
| budget         =
}} 1994 film by Louis Malle and Andre Gregory.   The film is an intimate, interpretive performance of the play Uncle Vanya by Anton Chekhov as adapted by David Mamet.  The film stars Wallace Shawn and Julianne Moore.

==Production==
Over the course of three years, director Andre Gregory and a group of actors came together on a voluntary basis in order to better understand Chekhovs work through performance workshops.  Staged and filmed entirely within the vacant shell of the then-abandoned New Amsterdam Theater on 42nd Street in New York City, they enacted the play rehearsal style on a bare stage with the actors in street clothes. Free from any commercial demands, their performances were for an invited audience only. Gregory and Malle decided to document the play as they had developed it.  The film was the result of the collaborative process.

===Location=== Victory Theater 42nd Street in New York City. The filmed version was shot entirely within the New Amsterdam Theatre, also on 42nd Street. Built in 1903, the theatre was the original home of the Ziegfeld Follies, a historical tidbit mentioned in the film during some pre-show banter. In the late 1930s, the New Amsterdam Theatre was transformed into a movie palace. The theatre remained a movie palace until it "temporarily" closed in 1982.

At the time Vanya on 42nd Street was filmed, the theatre had been abandoned for over ten years and was in a state of severe disrepair. Rats had chewed through much of the stage rigging, and flooding and mice made the stage unusable, so that they were restricted to a section of what had been the orchestra. 

For the film production, some rows of seats were removed and a small platform was built for the cast and film crew. Shortly after the production of Vanya, the New Amsterdam was leased to The Walt Disney Company.  Disney restored the theatre to its grand original design and reopened it in 1997.

==Cast and crew==
*Wallace Shawn as Vanya
*Julianne Moore as Yelena
*Larry Pine as Astrov Brooke Smith as Sonya
*George Gaynes as Serybryakov
*Phoebe Brand as Marina
*Jerry Mayer as Telegin
*Lynn Cohen as Vonenskaya

Also appearing are Madhur Jaffrey and, as himself, Andre Gregory.

* Shawn, Gregory, and Malle had previously collaboratorated on the 1981 film My Dinner with Andre.
* The film would be the last of Malles career.
* Julianne Moore, whose film career had recently been gaining notice from a critically acclaimed role in Short Cuts, was the actor predominantly featured in the advertising campaign for the film. The campaign didnt highlight the ensemble nature of the production, which led some confused movie-goers to think the film was about a woman named Vanya. Brooke Smith, and Lynn Cohen.

==References==
{{Reflist|
refs=
    
}}

==External links==
* 
*  
*  

 
 

 
 
 
 
 
 
 
 