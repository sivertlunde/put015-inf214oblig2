Android (film)
{{Infobox film
| name           = Android
| image          = Androidposter.jpg
| caption        = Theatrical release poster
| director       = Aaron Lipstadt
| producer       = Mary Ann Fisher
| writer         = James Reigle Don Keith Opper James Reigle Will Reigle
| starring       = Klaus Kinski Brie Howard
| music          = Don Preston
| cinematography = Tim Suhrstedt
| editing        = Andy Horvitch
| studio         = New World Pictures
| distributor    = New World Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
}}
 American science android program from their lab on a space station in orbit around the Earth. 

The film was voted Best Science Fiction Film in 1983 by The Age, but has received a somewhat mixed reaction from critics.

== Plot == android named Max 404 (Don Keith Opper), and his creator, Doctor Daniel (Klaus Kinski), reside aboard a remote space station. Although Max is a machine he has growing interest in all things human, especially sex, and is caught by Daniel watching a sexual instruction video. After Daniel admonishes him, Max eavesdrops on the doctors report that Maxs growing insubordinate behavior could lead to mutiny similar to an incident back on Earth known as the "Munich Revolution" after which androids were outlawed. It is then revealed that Daniel is illegally working on another android, Cassandra One (Kendra Kirchner), a female who he believes will be a superior machine. 

Next Max receives a distress call from a ship that seeks repairs. Upon hearing the pilots female voice, he excitedly permits them to land, not realizing that the ship is a prison transport and the pilot, Maggie (Brie Howard), and her associates, Keller (Norbert Weisser) and Mendes (Crofton Hardester), the latter her lover, are escaped fugitives. Once aboard the station, the convicts settle in, posing as the transports crew whom were previously killed during the prison break. Daniel becomes infuriated when he learns Max allowed the ship to land and demands they leave immediately, but he quickly changes his mind when he meets the attractive Maggie and invites her to have dinner with him in his personal garden.  

Maggie joins Daniel but the dinner goes wrong when a jealous Max pranks the doctor with some embarrassing mischief like ball-bearings in the wine bottle and cutting the doctors orchids. Daniel then pointedly asks Maggie if she would link up with his female android in an attempt to transfer sexual experiences to the machine. Maggie learns she would need to be sexually stimulated by the doctor during the procedure and she declines the ludicrous offer. Daniel becomes frustrated and demands Maggie help him, but she makes a hasty exit. Daniel returns to the lab and angrily reports in his log, (again overheard by Max), that once Cassandra is ready he will be deactivating Max.

While the criminals work on the ship, a TerraPol police cruiser arrives having detected a still-active transponder on their ship, and contacts Max to inform them of their presence. Max denies that the fugitives are on board even though he checks the crews identity to confirms they are indeed escaped convicts. When the police demand permission to land, Max destroys their ship with a laser. 

Max then tells Maggie that he knows of her plight but has saved her from the police. He then asks that she take him with her when she leaves the station. Maggie is unsure of what to do, but later sneaks away from Mendes and meets Max in the lab for an intimate encounter. The two are, however, interrupted when Cassandra activates and Maggie becomes horrified when she learns that Max is also an android.

Maggie returns to her quarters, but she is confronted by a furious Mendes who demands to know where she sneaked off to. When he notices her disheveled appearance and unbuttoned shirt he begins to beat her. Keller enters and tries to stop Mendes, but is knocked unconscious. Mendes then attacks Maggie. Once Keller awakens he sees Maggie is dead and believes Mendes killed her.  But it is Dr. Daniel who killed Maggie. Keller goes after Mendes again, jumping him from behind, but Mendes manages to kill him with a blow to the head. 

Eventually Max arrives, suitcase in hand, to Maggies quarters, but finds her dead. He sadly returns to Daniels lab where the doctor has activated Cassandra.  Daniel has Max sit in a chair and opens a door on the back of his head to reprogram him. Daniel tells Max that murder must be punished, leading Max and the viewer to believe that Mendes killed Maggie.  Daniel sends Max out to kill Mendes. In the meantime, more police ships arrive and when communications arent established, they forcefully board the station. 

After killing Mendes, Max goes to Maggies room, touches her lifeless body, and finds Dr. Daniels missing flashlight underneath Maggie. He and the viewer now realizes that it was Dr. Daniel who killed Maggie, not Mendes. Max returns to the lab.  There Daniel has made sexual advances to Cassandra, who has resisted him and is holding him at bay. Daniel asks Max to hold Cassandra for him. After refusing to obey his orders, Daniel begins to struggle with the two androids and they rip his head off, revealing that Daniel himself is an android. Cassandra then disposes of Daniels head in a trash chute and begins to reprogram Max. She tells Max they are not meant to obey the whims of men. There are other androids on earth in hiding and she wants to join them.  She has a plan.

When the police arrive at the lab, Cassandra thanks them for coming to their rescue.  Max is now dressed in a lab coat and posing as Dr. Daniel and Cassandra is his assistant.  She takes Max by the arm and the two are escorted out by the police who say they will be taking them back to Earth.

== Cast ==

{| class="wikitable"
! Actor
! Role
|- 
| Klaus Kinski || Dr. Daniel
|-
| Brie Howard || Maggie
|-
| Don Keith Opper || Max 404
|-
| Kendra Kirchner || Cassandra
|-
| Norbert Weisser || Keller
|-
| Crofton Hardester || Mendes
|-
| Randy Connor || Terrapol: Landing Party
|-
| Gary Corarito || Terrapol: Neptune
|-
| Mary Ann Fisher || Terrapol: Neptune
|-
| Julia Gibson || Terrapol: Minos
|-
| Roger Kelton || Terrapol: Landing Party
|-
| Darrell Larson || Terrapol: Neptune
|-
| Ian Scheibel || Terrapol: Neptune
|-
| Wayne Springfield || Terrapol: Minos
|-
| Rachel Talalay || Terrapol: Landing Party
|-
| Johanne Todd || Terrapol: Landing Party
|}

In the closing credits, Max 404 plays "himself," and the technical credits maintain the conceit that the film character Max 404 is played by an actual android called Max 404.

== Production ==
The movie was completely filmed in four weeks and edited in a further three weeks. The original version was 80 minutes long and none of the original content was removed before release.

The film cost less than $1 million to make. 

David Elliott wrote, "This movie was done on a scrawny budget for the Roger Corman schlock shop. Its makers believed in the film so much that they up-scaled it from pure schlock, and Corman -- a producer who believes in talent but even more in a buck -- lost interest after early bookings failed to come up green. Corman felt the film was not exploitable, and so Barry Opper (brother of Don) and initiating producer Rupert Harvey bought the rights. Android became one of those budding cult movies that may never thrive in big money terms but which refuse to play dead."   

The Miami Herald reviewer Bill Cosford would write in 1984, after the movies tour of the festival circuit in 1982 and 1983, "Android has gained something of a cult reputation already, largely on the strength of its success on a shoestring budget. Set in the year 2036, the film uses space-station sets and quasi-futuristic props crafted of leftovers from Roger Cormans B-movie backroom (theres a bit of Forbidden World here, some Galaxy of Terror there)."   

== Release ==

Joseph Bensoua said that, in 1982, Corman began test marketing Android in cities such as Tucson, Spokane and Las Vegas, but met unsatisfactory results. Producers Rupert Harvey and Barry Opper bought the film rights back from Corman in 1983. They opened the film in London, where The Observer called it "the sleeper of the year."   

== Reception ==

The film received a mixed response, being praised in Europe but receiving a less positive reaction in the United States. It was described by   said that he personally thought the film to be "smart" and "relevant".
 R2D2 and C3PO in Star Wars. As played by Don Opper, he looks perfectly human. The fun comes from watching Max discover just how human he is... This is a jewel in the rough, and roughness (along with wit) is what gives it a shine. There are shadowy, sub-slick sets (partly lifted from old Cormanoids like Battle Beyond the Stars), and the plot features three grubby space outlaws -- two varmints and a vamp -- played with lots of slumming gusto by Brie Howard, Norbert Weisser and Crofton Hardester... Max is a bit of a wimp, but brainy, and with a heart of computerized gold. He is keen on self-programming, and he studies old, 20th century movies and gently bones up on the human styles of Humphrey Bogart and James Stewart... After the villains break in, Max figures out that being human means that you get to bend the rules. The plot turns, and theres a neat, tricky resolution... You can hardly fail to enjoy it." 

Bill Cosford praised the films wit and the performances, such as "Klaus Kinski, in another surpassingly creepy performance" and Oppers Max "as a nebbishy Woody Allen type." Cosford said that the filmmakers "have laced their story with clever touches. Its not just the old movies and the music and the giggles over Maxs impossible puberty. Their tone is a gentle but sophisticated ribbing of the conventions of science-fiction films from Metropolis to Star Wars, with just enough "inside" references to the form to make watching fun, but not so many as to turn Android into a feature-length skit. There is even room for some philosophical musing here, for those so inclined. But it is not necessary. The director, Aaron Lipstadt, has made the kind of first film, like George Lucas THX 1138 and John Carpenters Dark Star, that suggests a creative intelligence behind even the rough spots." 

Joe Baltake wrote that the movie "is a kind of SciFi version of Rebel Without a Cause, a game fantasy about children rebelling against their parents. In this case, the children (Don Opper and Kendra Kirchner) are androids, robots, and the parent (Klaus Kinski, of all people, in a fright wig) is a mad scientist... Android pays tribute to Fritz Langs Metropolis ... Having been emotionally locked into puberty by Daniel, Max does what most children do - plays video games, watches old movies (such as Metropolis) and, of course, has a keen interest in S-E-X. With nothing to interfere with the way hes been programmed by Daniel, Max pretty much lives in a retarded state - that is, until a trio of criminal fugitives - two men, one woman - stow away inside Daniels space lab... The vicious stowaways are on hand merely to inspire Maxs rebellion against his parental figure. The films real fillip comes when Cassandra, the female android, turns out to be Maxs accomplice, rather than his competitor, and is hot to join him in his plan. In fact, she takes charge: "We are not meant to be governed by the whims of men," the blonde, stoic Cassandra says matter-of-factly, but with a comic, ambitious edge to her voice. Android is small, very small and, whats more, its human. In an odd way, it succeeds in being what The Pope of Greenwich Village never comes close to accomplishing."   

Joseph Bensoua called it "slow-moving space junk... Its 81-minute length, economical (make that cheap) sets and talky script give it a texture thats more akin to a Twilight Zone episode -- only not as good."  Rick Lyman, similarly, described the movie as "a lazy, whimsical sci-fier," while sympathizing with Max, "an outer-space Holden Caulfield - young, confused, yearning to get away from his strict surroundings and cut loose in the big city (in this case, the planet Earth). His performance is the best thing about the movie. His Max is hopelessly sweet and naïve, way too trusting for his own good. Hes the only character in the movie exhibiting the least bit of compassion or tenderness." 

==References==
{{reflist|refs=

   

}}

===Bibliography===
* Wingrove, David. Science Fiction Film Source Book (Longman Group Limited, 1985)

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 