Heartbreak Library
{{Infobox film name           = Heartbreak Library  image          =  director       = Kim Jung-kwon producer       = Lee Ho-yeon   Park Jun-hong   Moon Sung-joo writer         = Na Hyeon   Park Eun-yeong starring  Eugene  music          = Jo Seok-yeon   Lee Seung-yeop cinematography = Yun Myeong-sik  editing        = Kyung Min-ho   Heo Sun-mi distributor    = Studio 2.0 released       =   runtime        = 97 minutes country        = South Korea language       = Korean
}}
 romantic drama film starring Lee Dong-wook and Eugene (entertainer)|Eugene. It tells the story of a librarian who is first furious at a man who rips out page 198 of library books, then becomes intrigued, and finally decides to help him after learning that hes heartbroken after his girlfriend broke up with him then left a note to look up page 198, without mentioning which book.   

==Plot==
Eun-soo works conscientiously as a librarian in a seaside village. One day, she catches a mysterious man damaging some books at the library; the man, clad in a black suit with a black tie, tears out a single page from every book he comes across. At first, Eun-soo accuses him of book vandalism and reports him to the police, but she soon becomes curious regarding the complex story behind his actions. Joon-ohs girlfriend had suddenly left him with only a mysterious note, "Look up page 198." So Joon-oh goes to the library everyday and tears out page 198 from every book, hoping that he will find a message that might lead to his beloved or at least the reason for her disappearance. When Eun-soo checks the library database, she discovers that Joon-ohs girlfriend was a bibliophile, checking out a total of 900 books from the library. Eun-soo initially advises Joon-oh to let his ex-girlfriend go if thats her wish, because keeping trying to trace her is just an empty obsession. But since Joon-oh shows no sign of quitting, and she herself has just broken up with her boyfriend and is feeling her own loss in love, Eun-soo decides to help him decipher the messages on page 198 of all the books.

==Cast==
*Lee Dong-wook - Kim Joon-oh Eugene - Jo Eun-soo 
*Jo Ah-ra - Seon-mi
*Gi Ju-bong - Hong-soo
*Yoo Tae-gyun - director
*Jo Deok-hyun - Mr. Kim, the guard
*Go Soo-hee - Miss Paeng, calligraphy professor 
*Yoo Da-in - neighborhood girl that walks across ledge
*Jung Ho-jin - Ja-bong
*Park Dong-jin - Chul-woo
*Jo Jung-il - Jin-mok
*Kim Hyun-joo - Min-joo
*Lee Jong-gu - psychologist
*Jo Seok-hyun - office worker
*Lee Hwa-ryong - spontaneous photographer
*Park Hwi-soon - student preparing for bar exam
*Jung Eun-pyo - pharmacist Lee Jae-yong - Chairman Song

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 
 


 