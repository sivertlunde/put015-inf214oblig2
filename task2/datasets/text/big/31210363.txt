Padunna Puzha
{{Infobox film 
| name           = Padunna Puzha
| image          = Padunna Puzha.jpg
| image_size     =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = TE Vasudevan
| writer         = V Valsala S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan
| starring       = Prem Nazir Sheela Jayabharathi Adoor Bhasi
| music          = V. Dakshinamoorthy
| cinematography = P Dathu
| editing        = TR Sreenivasalu
| studio         = Jaya Maruthi
| distributor    = Jaya Maruthi
| released       =  
| country        = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by TE Vasudevan. The film stars Prem Nazir, Sheela, Jayabharathi and Adoor Bhasi in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
 
*Prem Nazir
*Sheela
*Jayabharathi
*Adoor Bhasi
*Sankaradi
*T. R. Omana
*Aranmula Ponnamma GK Pillai
*K. P. Ummer
*Panjabi
*Prathapan
*Thodupuzha Radhakrishnan
*Ushanandini
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Bhoogolam Thiriyunnu || CO Anto || Sreekumaran Thampi || 
|-
| 2 || Hridayasarassile || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Paadunnu Puzha  || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 4 || Paadunnu Puzha || P. Leela, AP Komala || Sreekumaran Thampi || 
|-
| 5 || Paadunnu Puzha  || P. Leela || Sreekumaran Thampi || 
|-
| 6 || Paadunnu Puzha || S Janaki, P. Leela || Sreekumaran Thampi || 
|-
| 7 || Paadunnu Puzha(Bit) || S Janaki || Sreekumaran Thampi || 
|-
| 8 || Sindhubhairavi Raagarasam || P. Leela, AP Komala || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 