Mane Aliya
{{Infobox film 
| name           = Mane Aliya
| image          =  
| caption        = 
| director       = S K A Chari
| producer       = A V Subba Rao
| writer         = Sadashiva Brahmam
| screenplay     =  Balakrishna
| music          = T. Chalapathi Rao
| cinematography = Annayya
| editing        = J Krishnaswamy
| studio         = Prasad Art Pictures
| distributor    = 
| released       =  
| country        = India Kannada
}}
 Indian Kannada Kannada film, Balakrishna in lead roles. The film had musical score by T. Chalapathi Rao. 

==Cast==
 
*Kalyan Kumar
*Jayalalithaa
*K. S. Ashwath Balakrishna
*Narasimharaju Narasimharaju
*Dikki Madhavarao
*M. P. Shankar
*Guggu
*Rangabhoomi Narayan
*Hanumanthachar
*Shivaji
*H R Sharma
*Thangappan
*Vasudeva Girimaji
*Anantharao
*Sandhya
*Rama
*Vanishree
*Suryakala
*Lakshmidevi Thalikote
*Geetha
*Thara
 

==Soundtrack==
The music was composed by T. Chalapathi Rao.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Bhale Chanside || Madhavapeddi Satyam || Vijaya Narasimha || 03.09 
|- 
| 2 || Preethiya Hoogala Mudidavale || PB. Srinivas || KS. Narasimhaswamy || 03.19 
|}

==References==
 

==External links==
*  
*  

 
 
 


 