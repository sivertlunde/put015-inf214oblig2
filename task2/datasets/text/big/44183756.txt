Oru Yugasandhya
{{Infobox film
| name           = Oru Yugasandhya
| image          =
| caption        = Madhu
| producer       = PKR Pillai
| writer         = G Vivekanandan Pappanamkodu Lakshmanan (dialogues)
| screenplay     = Pappanamkodu Lakshmanan Madhu Srividya Innocent Thilakan
| music          = A. T. Ummer
| cinematography =
| editing        = G Venkittaraman
| studio         = Shirdi Sai Creations
| distributor    = Shirdi Sai Creations
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, Madhu and Innocent and Thilakan in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
  Madhu
*Srividya Innocent
*Thilakan
*Nedumudi Venu Rohini
*Sankaradi Shankar
*Bahadoor Devan
*Mala Aravindan Nalini
*Soorya
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Aashaante || Chorus, CO Anto || P. Bhaskaran || 
|-
| 2 || Ivide ee vazhiyil || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Vampanukkum || K. J. Yesudas, Chorus || P. Bhaskaran || 
|-
| 4 || Velipparuthi poove || KS Chithra || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 