Karel Havlíček Borovský (film)
{{Infobox film
| name           =Karel Havlíček Borovský
| image          =
| image_size     =
| caption        =
| director       = Karel Lamač, Theodor Pistek
| producer       =
| writer         = Karel Lamač
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = August 21, 1925
| runtime        =
| country        = Czechoslovakia Silent
| budget         =
}} 1925 Czechoslovakia|Czechoslovak biographical film drama directed by Karel Lamač. It is about Karel Havlíček Borovský and is set during the 1848 revolutions.

==Cast==
*Jan W. Speerger ...  Karel Havlícek Borovský
*Anny Ondra ...  Fanny Weidenhofferová (as Anny Ondráková)
*Karel Lamač ...  Frantisek Havlícek (as Karel Lamac)
*Mary Jansová ...  Julie Sýkorová
*Anna Opplová ...  Julies Mother
*Otto Zahrádka ...  Prince Windisch-Graetz Robert Ford ...  Staff Officer
*Andy Stahl ...  Staff Officer Theodor Pištěk ...  Weidenhoffer / minister Augustin Bach
*Max Körner ...  Publisher
*Bela Horská ...  Princess Windisch-Graetz
*Jaroslav Vojta
*Antonín Marek
*Eduard Malý

==External links==
*  

 
 
 
 
 
 
 
 
 

 
 
 