Voodoo Woman
 
{{Infobox film
| name           = Voodoo Woman
| image          = Voodoowoman.jpg
| image_size     =
| caption        = film poster by Albert Kallis
| director       = Edward L. Cahn Alex Gordon
| writer         = Russ Bender V.I. Voss
| narrator       =
| starring       = Marla English Tom Conway Mike Connors John Blackburn Darrell Calker
| cinematography = Frederick E. West
| editing        = Ronald Sinclair
| distributor    = American International Pictures
| released       =  
| runtime        = 75 min.
| country        = United States 
| language       = English
| budget         = $65,000 Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p74 
| gross          =
}}
 The Undead.

==Plot==
  con the innocent Ted Bronson (Mike Connors) into acting as a jungle guide and leading them to the tribe that made the idol.
 voodoo and biochemical discoveries in an attempt to create a superhuman being.  He hopes that this being, possessing the best of man and beast, will be the mother of a new perfect and deathless race which he will control with a mixture of hypnosis and telepathy.  He is accompanied by his wife, Susan (Mary Ellen Kaye), who has long since disavowed her husband but remains trapped by her husband and the natives.

Dr. Gerards initial attempts to create a female superbeing are a failure because the transformation is only temporary and the native girl used as the subject of the experiment lacks the killer instinct he deems necessary for survival.  However, when he stumbles upon the party of treasure hunters, he decides that Marilyn will be a perfect subject for his experiment.  He successfully turns her into an invulnerable monster, but her inherent selfishness and greed outweigh his mental control over her and she turns on him.  Ted and Susan are able to escape in the ensuing chaos.

==Cast==
*Chaka, the Witch Doctor who is Gerards partner in monster-making - played by Martin Wilkins
*Bobo, the Gerards native houseboy - played by Otis Greene
*Gandor, a burly and surly native guard with a spear - played by Emmett Smith
*Zaranda, a gentle native girl and the first victim of Gerards evil experiment - played by Jean Davis
*Susan Gerard, the doctors trapped and terrified wife - played by Mary Ellen Kaye
*Rick Brady, Marilyns cowardly killer of a boyfriend - played by Lance Fuller bar owner in a lawless African town - played by Paul Dubov Norman Willis Mike "Touch" Connors
*The Voodoo Women, an identical pair of massively mutated zombies - created and played by Paul Blaisdell

==Production==
 
Lance Fuller reportedly had a two film a year deal over five years with Golden State Productions. Desilu Feature Will Star John Bromfield; Dahl, Harvey New Team
Schallert, Edwin. Los Angeles Times (1923-Current File)   27 Oct 1956: B3. 
 dubbed for added effect. 
==Reception==
 
The film is widely regarded as one of the worst films in cinema  - yet in 1966, just nine years later, it was remade by Larry Buchanan into a TV film that is considered even worse, Curse of the Swamp Creature. 
 Alex Gordon Ruth Alexander. Burbank premiere Richard was able to explain to her the differences between low budget and big budget film-making, and she and Alex were eventually married, with her later actually scripting several of his features.

==References==
 
== External links ==
*  

 

 
 
 
 
 
 
 