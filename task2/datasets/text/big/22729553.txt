The Blind Side (film)
 The Blind Side}}
{{Infobox film
| name     = The Blind Side
| image    = Blind side poster.jpg
| caption  = Theatrical release poster
| director = John Lee Hancock
| producer = Broderick Johnson Andrew Kosove Gil Netter
| screenplay = John Lee Hancock
| based on =  
| starring = Sandra Bullock Tim McGraw Quinton Aaron Kathy Bates
| music    = Carter Burwell
| cinematography = Alar Kivilo
| editing  = Mark Livolsi
| studio   = Alcon Entertainment
| distributor = Warner Bros. Pictures Summit Entertainment
| released =  
| runtime  = 129 minutes
| country  = United States
| language = English
| budget   = $29 million   
| gross    = $309,208,309 
}} Sean and Leigh Anne Tuohy, to his position as one of the most highly coveted prospects in college football, then finally becoming a first-round pick of the Ravens.
 NCAA coaches, SEC coaches Arkansas at LSU at the time and represents it in the film), former coaches Lou Holtz, Tommy Tuberville, Phillip Fulmer, as well as recruiting analyst Tom Lemming. 
 Golden Globe Best Picture.

==Plot==
In 2003, Michael "Big Mike" Oher (Aaron) has been living in foster care with different families in Memphis, Tennessee. Every time he is placed in a new home, he runs away. His friends father, on whose couch Michael has been sleeping, asks Burt Cotton, the coach of Wingate Christian School, to help enroll his son and Michael. Impressed by the boys size and athleticism, Cotton gets him admitted despite a poor academic record. Michael is befriended by a young boy named Sean Jr. "S.J." Tuohy, who is unintimidated by his appearance. S.J.s mother Leigh Anne Tuohy (Bullock), a strong-minded interior designer, begins to take notice of Michael as a troubled and lonely boy. 

One night, Leigh Anne notices Michael walking on the road, shivering in the cold, when she learns he intends to spend the night huddled outside the school gym. Despite her husband Seans (McGraw) misgivings, she invites him to stay the night at their house. The next morning, Leigh Anne catches Michael attempting to leave the house quietly. She asks him to spend the Thanksgiving holiday with her family. Slowly Michael becomes a member of the Tuohy family, as Leigh Anne buys him clothes, S.J. raises his confidence, and teenage daughter Collins Tuohy helps him make friends at school. When Leigh Anne seeks to become Michaels legal guardian, she learns he was separated from his drug-addict mother when he was seven and that no one knows her whereabouts. She is also told that, although he has scored low in a career aptitude test, he is in the 98th percentile in "protective instincts." Leigh Anne uses this to drastically improve his performance on the football field.
 Ole Miss (her and the Tuohys alma mater) by making remarks about the University of Tennessee burying the body parts of dead people under their football field. Michael ultimately decides to attend Ole Miss. An investigation follows to look into whether Michael was unduly influenced by the Tuohys and Miss Sue the benefit their alma mater. Michael walks out of the room before the interview is over.

After confronting Leigh Anne about her motives for influencing him, Michael goes to find his birth mother in his old neighborhood. A number of young men who know Michael welcome him back to the projects and offer him beer. When the gang leader makes sexually suggestive comments regarding Leigh Anne and Collins, Michael assaults the boys and leaves. After thinking and questioning Leigh Anne on the matter, Michael realizes that the Tuohys are now his family, and tells the investigator in another interview that attending his familys school is the reason he has chosen Ole Miss.

The film ends with real-life footage of Oher being drafted in the first round by the Baltimore Ravens in the 2009 NFL Draft. S.J. is seen leading the players onto the field with Michael before a game.

==Cast==
 
* Sandra Bullock as Leigh Anne Tuohy
* Tim McGraw as Sean Tuohy Michael "Big Mike" Oher
* Jae Head as Sean "S.J." Tuohy, Jr.
* Lily Collins as Collins Tuohy Ray McKinnon as Coach Burt Cotton
* Kim Dickens as Mrs. Boswell
* Adriane Lenox as Denise Oher
* Kathy Bates as Miss Sue
* Eaddy Mays as Elaine Robert "IronE" Singleton as Alton

===Coaches playing themselves=== Auburn
* LSU
* South Carolina Tennessee
* Arkansas
* Ole Miss

==Production==
The Blind Side was produced by Alcon Entertainment and released by Warner Bros. According to Reuters, the films production budget was $29 million. Filming for the school scenes took place at Atlanta International School and The Westminster Schools in Atlanta, Georgia, and it features many of their students as extras. The film premiered on November 17 in New York City and New Orleans and opened in theaters on November 20 in the rest of the United States and in Canada. 

Academy Award winner Julia Roberts was originally offered Bullocks role, but turned it down.  Bullock initially turned down the starring role three times due to discomfort with portraying a devout Christian. By her own account, Bullock felt she couldnt objectively represent such a persons beliefs on screen.  But after a visit with the real Leigh Anne Tuohy, Bullock not only won the role, but also took a pay cut and agreed to receive a percentage of the profits instead. 

==Reception==

===Box office=== a very rare greater success for the second weekend than it did in its opening weekend, taking in an estimated $40 million, an increase of 18 percent, from November 27 to November 29, 2009, coming in second to New Moon once again, bringing its gross to $100,250,000. 
 Alice in Wonderland.   

===Critical reviews=== rating average normalized rating out of 100 to reviews from film critics, has a rating score of 53 based on 29 reviews, indicating "mixed or average reviews". 

  

====Race controversy====
 white savior narrative in which Oher, an African-American male, is unable to overcome poverty and personal failure without the guidance of adoptive, white mother Touhy. For example, Jeffrey Montez de Oca of the University of Colorado writes that in The Blind Side  s portrayal of adoption, "charity operates as a signifying act of whiteness that obscures the social relations of domination that not only make charity possible but also creates an urban underclass in need of charity."  Melissa Anderson of the Dallas Observer argues that the "mute, docile" portrayal of Oher effectively endorses the Uncle Tom stereotype of African-American submission to white authority. 

===Awards and nominations===
The Blind Side has earned numerous awards and nominations for the lead performance of the films star, Sandra Bullock.

{| class="wikitable sortable"
|-
! Association
! Category
! Nominee
! Result
|- 82nd Academy Academy Awards Academy Award Best Actress in a Leading Role Sandra Bullock
| 
|- Academy Award Best Picture Film
| 
|- 15th Critics Choice Awards|Critics’ Choice Awards Broadcast Film Best Actress Sandra Bullock  (tied with Meryl Streep) 
| 
|- ESPY Awards Best Sports Best Sports Movie Film
| 
|- 67th Golden Golden Globe Awards Golden Globe Best Actress – Motion Picture Drama Sandra Bullock
| 
|- 2010 Kids Nickelodeon Kids Choice Awards 2010 Kids Favorite Movie Actress Sandra Bullock
| 
|- 16th Screen Screen Actors Guild Awards Screen Actors Outstanding Performance by a Female Actor in a Leading Role Sandra Bullock
| 
|- 36th Peoples Choice Awards|Peoples Choice Awards 36th Peoples Favorite Movie Actress Sandra Bullock
| 
|- Washington D.C. Washington D.C. Area Film Critics Association Washington D.C. Best Actress Sandra Bullock
| 
|- Teen Choice Teen Choice Awards Teen Choice Favorite Drama Movie Film
| 
|- Movie Actress – Drama Sandra Bullock
| 
|- Breakout Male Actor Quinton Aaron
| 
|}

====Best Picture nomination====

The nomination of The Blind Side for Best Picture was considered a surprise, even to its producers. Michael Cieply and Paula Schwartz,  , The New York Times, February 10, 2010, accessed February 4, 2014.  In an attempt to revitalize interest surrounding the awards, the Academy of Motion Picture Arts and Sciences had upped the number of Best Picture nominees from a mandatory number of five to a mandatory ten in time for the 82nd Academy Awards, the year The Blind Side was nominated.    However, in 2011, the Academy changed the policy, stating that the Best Picture category would feature between five and ten nominees depending on voting results, as opposed to a set number of nominees.     The change was interpreted as a response to films like The Blind Side being nominated for Best Picture to fill up the set number of spots. David Karger,  , Entertainment Weekly, June 15, 2011, accessed February 4, 2014.  Nicole Sperling and Amy Kaufman,  , Los Angeles Times, June 16, 2011, accessed February 4, 2014. 

==Soundtrack==
The movie features 23 songs by artists including Les Paul, Young MC, Lucy Woodward, The Books, Canned Heat, Five for Fighting, and the films co-star Tim McGraw.  However, while the score soundtrack by Carter Burwell was released on CD, none of the featured songs were included.

==Home media==
The Blind Side was released on DVD and Blu-ray on March 23, 2010. The Blind Side was available exclusively for rental from Blockbuster for 28 days. 

Redbox and Netflix customers had to wait 28 days before they were able to rent the movie.   . redbox press room (February 16, 2010). Retrieved on January 23, 2011  This stems from the settlement of a lawsuit brought by Redbox against Warner Home Video, who, in an attempt to boost DVD sales, refused to sell wholesale titles to Redbox. On August 19, 2009 Redbox sued Warner Home Video  to continue purchasing DVD titles at wholesale prices. On February 16, 2010, Redbox settled the lawsuit  and agreed to a 28-day window past the street date.

As of July 9, 2013, units sold for the DVD stand at more than 8.4 million copies and has grossed a further $107,962,159 adding to its total gross. 

==See also==

* White savior narrative in film

==References==
 

==External links==
*  
*  
*  
*  
*  
*   History vs. Hollywood at Chasing the Frog

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 