All the Right Noises
{{Infobox film
| name           = All the Right Noises
| caption        = 
| image	=	All the Right Noises FilmPoster.jpeg
| director       = Gerry OHara
| producer       = Anthony J. Hope Si Litvinoff
| writer         = Gerry OHara
| screenplay     = 
| story          = 
| based on       =   Tom Bell Judy Carne John Standing Melanie
| cinematography = Gerry Fisher
| editing        = Antony Gibbs
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Tom Bell, Olivia Hussey, Judy Carne and John Standing.   

==Synopsis==
A married man with children, working as a lighting technician for a theatrical company, has an affair with a 15 year old American actress. 

==Cast== Tom Bell - Len Lewin
* Olivia Hussey - Val
* Judy Carne - Joy Lewin
* John Standing - Bernie
* Roy Herrick - Camera operator
* Yootha Joyce - Mrs Bird Robert Keegan - Lens father
* Lesley-Anne Down - Laura
* Marianne Stone - Landlady
* Gordon Griffin - Terry Edward Higgins - Ted
* Rudolph Walker - Gordon
* Oscar James - Guard
* Chloe Franks - Jenny Lewin
* Gareth Wright - Ian
* Chrissie Shrimpton - Waitress
* Peter Burton - Stage manager
* Charles Lloyd-Pack - Stagedoor keeper
* Otto Diamant - Conductor
* Nicolette Roeg - Millie

==Home media== BFI have Flipside strand. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 