Tracks to Terezín
"Spuren nach Theresienstadt / Tracks to Terezín" is a film with Herbert Thomas Mandl, a survivor of the Holocaust.

==Plot==
The composer Pavel Haas makes a bow after the performance of his composition “Study For String Orchestra” conducted by Karel Ančerl at Terezín 1944. That photo is a stand by photo shot during the set of the propaganda film “A Documentary About The Jewish Settlement” about Theresienstadt (film). A member of that orchestra and also be seen on that photo is the violin player Herbert Thomas Mandl. He is a real witness of the 20th century. He was born in 1926 at Bratislava, his father Daniel Mandl served for the artillery of the army of the Austrian-Hungarian Army at the Albanian front. At his childhood Mandl is in Ostrava and Brno and he gets education in playing the violin. After the occupation of the Nazis he and the family must go to the ghetto of Prague and will be later deported to the concentration camp of Terezín, which is used by the Nazis for propaganda. There he met the composer Viktor Ullmann, Ullmann is now his teacher and conductor. Mandl is member of different orchestras in Terezín as in the string orchestra of
Karel Ančerl and also in the missed musical work “Villon” composed and conducted by Viktor Ullmann at Terezín. Deportation to Auschwitz. Death march together with his father to the concentration camp Dachau-Kaufering IV. His father dies in Dachau. Herbert Thomas Mandl survives, he will be not executed at the concentration camp, because a member of the SS refuses to shoot. He sees the liberation of the camp by the US-Army. After the war Mandl works as a professor for violin at the academy in Ostrava. During the Cold War he comes in conflict with the regime. Unbelievable escape as a tourist to the American Embassy in Cairo and imprisoned in Egypt. Flight to Greece in a camp of the CIA and interrogations there. Mandl comes to Western Germany, people of the secret service believes he is a spy from the East. The bureaucracy of Western Germany denies to accept him as a political refugee. But the poet Heinrich Böll helps him and Mandl becomes his secretary. Böll also helps him to plan the escape of his wife Slavi from Ostrava. During a visit of Heinrich Böll in Prague Böll smuggles the wife of Mandl in his car through the border. Now both want to start a new life in the United States of America. Slavi Mandl is an excellent piano player; both are performing concert programmes together. In the United States Slavi Mandl works immediately as a piano player and teacher for music. For Herbert Thomas Mandl it is not so easy to get some work. In Seattle in the state Washington he works in a psychiatry. Both are coming back to Germany and stay now in Meerbusch near Düsseldorf. Slavi Mandl works as a teacher for piano and with the help of Heinrich Böll Mandl becomes a teacher at a school. For the world premiere of the play “The Inquiry” of Peter Weiss about the trial of Auschwitz in Frankfurt. Herbert Thomas Mandl works as a witness on the direction the play. The communistic government of the Czechoslovakia deprives his doctor in philosophy; he gets it back in the nineties of the 20th century. Mandl died on February 22, 2007.

==Questions and Answers==
In the film Herbert Thomas Mandl gives answers to the following questions: 
* About the music and the rehearsal of the opera “The Emperor of Atlantis Or The Disobidiene of Death” by Viktor Ullmann, composed 1943 / 1944 at Terezín.
* About the end of the rehearsals of “The Emperor of Atlantis Or The Disobidience of Death” and why the emperor in the opera of Ullmann cannot be seen as Hitler.
* About the world premiere of the melodramatic piece „The Lay of Love and Death of The Cornet Christoph Rilke“ composed by Viktor Ullmann at Terezín.
* Place and date of the world premiere of “The Lay of Love and Death of The Cornet Christoph Rilke”.
* Place of the rehearsals of the opera by Ullmann “The Emperor of Atlantis Or The Disobidience of Death”.
* About the management of the rehearsals of the opera “The Emperor of Atlantis Or The Disobidience of Death” and the leisure time („Freizeitgestaltung“) at Terezín.
* The set of the propaganda film “A Documentary About The Jewish Settlement”. Theresienstadt (film)
* About Kurt Gerron, the director of the propaganda film and the visit of the Red Cross at Terezín 1944.
* The meaning of the word „Transport“.

==Sequel==
The film was produced in 2007 by ARBOS - Company for Music and Theatre in Austria. Interview and director: Herbert Gantschacher; camera: Robert Schabus; editor: Erich Heyduck. A DVD exists in English and German Language.

==References==
* Herbert Thomas Mandl "Rembering Viktor Ullmann" in "Tracks to Viktor Ullmann" edition selene, Vienna 1998 ISBN 3-85266-093-9
* Ingo Schultz "Viktor Ullmann" in New Grove, second edition, Oxford University Press ISBN 0-333-60800-3
* Jean Jacques Van Vlasselaer "The Ultimate Witness" University for Music & Performing Arts Mozarteum Salzburg 2006
* Ingo Schultz "Composed and rehearsed at the Theresienstadt concentration camp: The Emperor of Atlantis or The Disobidience of Death by Viktor Ullmann" in "Music Theatre in exile during the Nazi period" Von Bockel edition, Hamburg 2007 ISBN 3-932696-68-9
* Ingo Schultz "Viktor Ullmann. Life and Work" Metzler/Bärenreiter edition, Kassel 2008 ISBN 978-3-476-02232-5

==External links==
*  

 
 
 
 
 
 
 