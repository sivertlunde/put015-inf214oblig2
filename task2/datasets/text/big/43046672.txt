The Trunk
{{Infobox film
| name           = The Trunk
| image          = "The_Trunk".jpg
| caption        = Original 1961 poster
| director       = Donovan Winter 
| producer       = Lawrence Huntington
| screenplay     = Donovan Winter
| story          = Edward Abraham Valerie Abraham
| starring       = Philip Carey Julia Arnall Dermot Walsh
| music          = John Fox
| cinematography = Norman Warwick
| editing        = Reginald Beck
| studio         = Donwin Films
| distributor    = Columbia Pictures
| released       =  
| runtime        = 72 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
The Trunk is a low budget, black and white 1961 British mystery film directed by Donovan Winter and starring Phil Carey, Julia Arnall and  Dermot Walsh.  

==Plot==
Troubles ensue when Lisa marries Henry, a British lawyer. Lisas jealous ex-boy friend Stephen decides to take revenge by convincing her that she has killed Diane, her husbands ex-girl friend. Lisa gives Stephen the money he wants to keep quiet and dispose of the corpse. Unfortunately, the dead womans other ex-lover, Nicholas, sees the two together. After getting his money from Lisa, Stephen puts Dianes body in a trunk and drives to an isolated area. There he discovers that the woman is not feigning death; she has been killed by the jealous Nicholas, in a manner that will incriminate Stephen.

==Cast==
* Phil Carey as Stephen Dorning
* Julia Arnall as Lisa Maitland
* Dermot Walsh as Henry Maitland
* Vera Day as Diane
* Peter Swanwick as Nicholas Steiner
* John Atkinson as Matt
* Betty Le Beau as Maria
* Tony Quinn as Porter
* Robert Sansom as Bank Manager
* Pippa Stanley as Mrs. Stanhope
* Richard Nellor as Sir Hubert
* Nicholas Tanner as Policeman

==Critical reception==
TV Guide wrote, "the movie is badly produced and too seamy for its own good."  In The New York Times, Bosley Crowther wrote, "now that the British are importing American actors to commit homicide in their low-budget movies, they seem to have lost their flair."  Crowther called it a "foolish melodrama" that is "several kilometers removed from Agatha Christie."   Sky Movies called it a "creepy little thriller" that is "hugely enjoyable. The director doesnt miss a trick at tightening up the suspense." 

==External links==
* 

==References==
 

 
 
 
 
 
 


 