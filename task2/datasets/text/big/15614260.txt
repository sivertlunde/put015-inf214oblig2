Ghosts of Girlfriends Past
{{Infobox film
| name           = Ghosts of Girlfriends Past
| image          = Ghosts of girlfriends past.jpg
| caption        = Promotional film poster Mark Waters
| producer       = Brad Epstein Jonathan Shestack Marcus Viscidi
| writer         = Jon Lucas Scott Moore
| starring       = Matthew McConaughey Jennifer Garner Breckin Meyer Lacey Chabert Robert Forster Anne Archer Emma Stone Michael Douglas Christina Milian
| music          = Rolfe Kent
| cinematography = Daryn Okada
| editing        = Bruce Green
| distributor    = New Line Cinema
| released       =  
| runtime        = 95 minutes  
| country        = United States
| language       = English
| budget         =
| gross          = $102,223,269 
}} Mark Waters Scott Moore. Filming spanned February 19, 2008 to July 2008 in Massachusetts with stars Matthew McConaughey, Breckin Meyer, Jennifer Garner, Lacey Chabert and Michael Douglas. The film was released on May 1, 2009. 

Ghosts of Girlfriends Past features a wedding day and the day before, rather than the familiar Christmas and Christmas Eve from A Christmas Carol. The three ghosts share similar appearances with the original descriptions, and the film shares the traditional plot points from the book.

==Plot==
Connor Mead (Matthew McConaughey) is a famous photographer and confirmed womanizer. He takes a break from his playboy lifestyle to attend his brothers wedding, where he becomes reacquainted with Jenny Perotti (Jennifer Garner), the only girl who ever captured his heart. After Connor delivers a drunken speech at the rehearsal dinner where he says that love isnt real, hes met in the bathroom by the ghost of his uncle Wayne (Michael Douglas), a roué who taught Connor everything he knows about seducing women. Wayne informs Connor that, over the course of the evening, he will be visited by three ghosts who will lead him through his romantic past, present, and future.

The first ghost is the "Ghost of Girlfriends Past" in the form of Allison Vandermeersh (Emma Stone), one of his high school girlfriends and his first lover. Together, they revisit scenes from his past, focusing on his relationship with Jenny. Connor and Jenny were very close as children; she gave him his first instant camera which he used to take her picture, promising to keep it forever. By middle school, the two were on the verge of romance, but Connors hesitation at a dance caused Jenny to dance with and kiss another boy. Heartbroken, Connor was told by Wayne that he must avoid romance at all costs in order not to feel such pain again. For the next two years, Wayne schooled Connor in the art of seduction. When he next saw Jenny, at a high school party, Connor ignored her and had sex with Allison. Several years later, as adults, Connor and Jenny rekindled their romance, but Jenny forced him to woo her for several weeks in an attempt to rid him of his womanizing ways. After they finally did have sex, Connor falls in love with her, but then panics, running out on her so he wont be hurt. Jenny wakes up alone and broken-hearted. His relationships thereafter consisted of a series of very short flings.

Awakening back in the Mead mansion in the present, Connor accidentally destroys Paul and Sandras wedding cake and unsuccessfully attempts to reconcile with Jenny. As he storms out of the house, he is confronted by the "Ghost of Girlfriends Present" in the form of his assistant Melanie (Noureen DeWulf), the only constant female figure in his life. With her, he sees that in his absence the other wedding guests make fun of him and his shallow lifestyle. Paul stands up for his brother, recalling that Connor helped to raise him after their parents death, and expresses his hope that Connor will someday change for the better. Connor also sees that Jenny is being comforted by Brad (Daniel Sunjata), and is upset that his own actions and attitude are bringing the two closer. He is further upset to discover that Melanie and the three women whom he previously broke up with by conference call are bonding over his disregard for their feelings.

Returning to the house, Connor finds Sandra furious at learning that Paul had slept with one of her bridesmaids very early in their relationship, information that Connor had let slip earlier in the evening. Connor attempts to mend the situation but only makes things worse, and Paul tells him to leave. On his way out, he is confronted by the "Ghost of Girlfriends Future" (Olga Maliouk), who takes him forward in time to see that Jenny marries Brad while Paul remains alone. Further in the future, Paul is the only mourner at Connors funeral. Wayne appears and tells Connor that this is his future if he continues on the same path, pushing him into the grave to be buried by his many ex-girlfriends.

Connor awakens in the Mead home and learns that Sandra has called off the wedding and is on her way to the airport. He intercepts the bridal party and convinces Sandra to forgive Paul by sharing lessons learned from his own mistakes, particularly that the pain of heartbreak is outweighed by the regret of never risking ones heart in the first place. Connor helps Jenny to restart the wedding and reconciles with her afterwards by showing her the picture he still carries of her as a child, and promises to always be there when she wakes up. The two kiss and dance in the snow.

Meanwhile, Wayne is still in the dining room trying to hit on the ghost of girlfriends future as she magically vanishes. Then he tries to hit on the ghost of girlfriends present as she informs Wayne she is one of the attendees and vanishes. Melanie and Brad start talking & then they dance. Wayne is left with the ghost of girlfriends past, Allison, who is not interested since she is only 16. Wayne says that they are ghosts and therefore ageless.

==Cast==
* Matthew McConaughey as Connor Dutch Mead
**Devin Brochu as Young Connor
**Logan Miller as Teenage Connor
* Jennifer Garner as Jenny Perotti
**Kasey Russell as Young Jenny 
** Christa B. Allen as Teenage Jenny
* Michael Douglas as Wayne Mead
* Breckin Meyer as Paul Mead
* Lacey Chabert as Sandra Volkom
* Robert Forster as Sergeant Major Mervis Volkom
* Daniel Sunjata as Brad Frye
* Emma Stone as Allison Vandermeersh, the Ghost of Girlfriends Past
* Noureen DeWulf as Melanie, the Ghost of Girlfriends Present
* Anne Archer as Vonda Volkom
* Amanda Walsh as Denice
* Camille Guaty as Donna
* Rachel Boston as Deena
* Christina Milian as Kalia Emily Foxler as Nadja

==Production== Castle Hill in Ipswich, Massachusetts.  The film was also the first pairing of Michael Douglas and Anne Archer since the 1987 hit thriller Fatal Attraction, although they shared no scenes together. Jennifer Garner and Christa B. Allen again appear together for the first time since 13 Going on 30 in 2004, and they again play the older and younger versions of the same character.

Ghosts of Girlfriends Past was cast by veteran Hollywood casting director Marci Liroff.

==Soundtrack==
#"Ghosts of Girlfriends Past" - All Too Much featuring Matthew Sweet
#"Hush" - Gavin Rossdale
#"Got a Lot of Love for You Baby" - The Ralph Sall Experience Keep on Loving You" - REO Speedwagon
#"You Cant Hurry Love" - The Ralph Sall Experience Ladies Night" - Kool & the Gang
#"The Safety Dance" - Men Without Hats
#"Yeah (Dream of Me)" - All Too Much
#"Holding Back the Years" - Simply Red
#"Sleep" - All Too Much

==Reception==

===Critical reaction===
As of January 2011, the film received negative reviews and holds a 26% rating on the website Rotten Tomatoes, based on 140 reviews.  Metacritic lists it with a 34 out of 100, which indicates "generally unfavorable reviews", based on 29 reviews. 

===Box office===
On the films opening weekend, it debuted at #2 with a gross of $15,411,434 (3,175 theaters, $4,854 average), far behind   s $85,085,003 gross.  The film made $55,250,026 in the United States and Canada and has a worldwide total of $102,223,269.   

==Home media==
The film was released on DVD on September 22, 2009. 

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 