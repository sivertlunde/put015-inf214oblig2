Sea Point Days
{{Infobox film
| name           = Sea Point Days
| image          = SeaPointDays.jpg
| caption        = 
| director       = François Verster
| producer       = Producer Luna Films
| writer         = 
| starring       = 
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = South Africa
| language       = 
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = François Verster, Peter Lichti
| editing        = François Verster
| music          = Peter Coyte
}}

Sea Point Days is a 2008 documentary film about the Cape Town suburb of Sea Point, directed by François Verster.

== Synopsis == Signal Hill Apartheid exclusivity, it is nowadays unique in its apparently easy mix of age, race, gender, religion, wealth status and sexual orientation. Somehow this space has become one where all South Africans feel they have a right to exist, and where the possibility of happiness in a divided world doesnt seem unfeasible. But what is the reality of those coming here? How do people see their past, their present in this space and their future in this country?   

The film had its world premiere at the Toronto International Film Festival.   

== References ==
 
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 
 