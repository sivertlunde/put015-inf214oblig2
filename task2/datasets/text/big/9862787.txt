Genuine (film)
{{Infobox film
| name           = Genuine
| image          = Genuine plakat.jpg
| image_size     =
| caption        =
| director       = Robert Wiene
| producer       = Rudolf Meinert Erich Pommer
| writer         = Carl Mayer Hans Heinrich von Twardowski Albert Bennefeld
| cinematography = Willy Hameister
| editing        =
| distributor    = Decla-Bioscop
| released       =  
| runtime        =
| country        = Weimar Republic German intertitles
| budget         =
}} silent horror film directed by Robert Wiene. It was also released as Genuine: A Tale of a Vampire.
 Expressionist painter César Klein.

The film has been released as an extra feature on DVD releases of the Wiene film The Cabinet of Dr. Caligari.

 

==Plot==
Since completing a portrait of Genuine, a high priestess, Percy becomes irritable and withdrawn. He loses interest in painting and refuses to see his friends, preferring to spend his time alone with the portrait in his study. After turning down a wealthy patrons offer to buy the picture, Percy falls asleep while reading stories of Genuines life. Genuine comes to life from the painting and escapes.

Genuine is purchased at a slave market by an old eccentric named Lord Melo. He learns that she had been sold into slavery when her people were conquered by a rival tribe. Melo locks her in an opulent chamber beneath his house, though she begs to be set free.

Guyard the barber visits Lord Melo every day at noon, though today he sends his young nephew Florian in his place.  Meanwhile, Genuine breaks out of her underground prison, climbs the immense staircase to find Florian shaving the sleeping Lord Melo. She bewitches him into slitting Melos throat with a straight razor. Florian falls under Genuines spell, but when she demands that he prove his love for her by taking his own life, he cannot go through with it and is forced to make his escape.

Melos grandson Percy arrives at the house. He too becomes infatuated with Genuine, quickly forgetting any questions he has about his grandfathers sudden death. Although Genuine loves Percy in return, their romance is destined to be short-lived. Guyard, stirred up by Florians tales of murder and witchcraft, arms a mob with scythes and bludgeons and storms Melos house. Florian, still infatuated with Genuine, secretly makes his own way inside, determined that he shall have her, or no-one will.

== Reception ==
Genuine was disliked when it was released, with critics such as Siegfried Kracauer and Lotte Eisner dismissing it as a failure.  It was unlike Decla-Bioscops previous film The Cabinet of Dr. Caligari which was well received. Even today, the story is difficult to follow.

 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 


 
 