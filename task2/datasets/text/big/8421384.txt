Aconcagua (film)
For other uses, see Aconcagua (disambiguation).

{{Infobox film
|  name         = Aconcagua image          = caption        = Aconcagua director       = Leo Fleider producer       = Leo Fleider writer         = Norberto Aroldi starring       = Tito Alonso   Enrique Kossi   Elisa Galvé   Selva Alemán music          = Tito Ribero cinematography = Ignacio Souto editing        = José Serra distributor    = Gloria Films released       = 18 June 1964 runtime        = 73 minutes country        = Argentina language       = Spanish budget         = followed by    =
}} 1964 color Argentine adventure drama film directed by Leo Fleider and written by Norberto Aroldi. The title refers to the highest peak in the Andes&mdash;Aconcagua located in Argentina. The star of the film is Tito Alonso.

==Production, distribution and release==
Aconcagua was produced and distributed by Gloria Films and premiered in Buenos Aires on 18 June 1964.

==Cast==
*Tito Alonso
*Enrique Kossi
*Elisa Galvé
*Selva Alemán
*Héctor Pellegrini
*Alberto Ruschel
*Enrique Talión

==External links==
*  
*  A listing on the Argentine films website

 
 
 
 


 