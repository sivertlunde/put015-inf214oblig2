Perpetual Motion (film)
{{Infobox film
| name           = Perpetual Motion
| image          = PerpetualMotion.jpg
| caption        = 
| director       = Ning Ying
| producer       = Francesco Cosentino Ning Ying
| writer         = Hung Huang Liu Sola Ning Ying
| starring       = Hung Huang Li Qinqin Liu Sola Ping Yanni
| music          = Liu Sola
| cinematography = Andrea Cavazzuti Ning Ying
| editing        = Ning Ying
| distributor    = 
| released       =  
| runtime        = 90 minutes
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 无穷动
| fanti          = 無窮動
| pinyin         = Wú qiōng dòng}}
}} independent Chinese film directed by Ning Ying. The film follows four wealthy, high-powered women living in Beijing during the Chinese New Year.  The film had its North American premiere on September 10, 2005 at the 2005 Toronto International Film Festival.  Perpetual Motion was co-written by Ning, musician and author Liu Sola, and publisher Hung Huang (Hung and Liu also star) and was produced by Beijing Happy Village, Ltd.

The film opened in Chinese cities in late February 2006 to some controversy due to its depiction of assertive, sexually confident women.    The version shown was slightly edited compared to the versions shown abroad in Toronto and 62nd Venice International Film Festival|Venice, with some of the more explicit discussions on sex as well as a scene involving the four main characters listening to an old revolutionary song in high heels and makeup being edited or cut.  Given its content, Perpetual Motion is often referred to as a Chinese version of the American television series Sex and the City. 

== Plot ==
Niuniu, a wealthy middle aged woman living in Beijing, discovers that her husband is having an affair after coming upon a romantic e-mail. She knows only that it is one of her close friends but not which one. Determined to discover the truth, she invites her friends Qinqin, a ditsy actress, Lala, a successful artist, and Madam Ye, a property developer over to her lavish siheyuan home. There the four women share stories of their sexual past over food and mahjong. Niuniu, however, still has her plan to execute...

== Cast ==

*Hung Huang as Niuniu; Hung Huang, a publisher in reality,  plays the main character, a high powered woman in Beijing who invites her three friends to dinner on Lunar New Years Eve for the purpose of discovering which of her friends is having an affair with her husband.
*Li Qinqin as Qinqin, an actress; Li Qinqin unlike the other three "actresses" is a professional performer. 
*Liu Sola as Lala, a successful artist; author and musician Liu Sola also served as the films music composer and shared co-screenwriters credit. 
*Ping Yanni as Madam Ye, a property developer; Ping Yanni is an actual businesswoman working in Beijing. 
*Zhang Hanzi as Niunius maid and cook; Zhang Hanzi, actually Hung Huangs mother, served as one of the English translators for Mao Zedong in her younger days.   

== Reception ==
Though the films concept of four middle-aged wealthy Chinese women engaging in frank discussions of sex raised eyebrows, the execution of that concept was ultimately found lacking by many western critics. Derek Elley of Variety (magazine)|Variety, for example, suggested that the films "script remains unfocused," and that its uneven performances (from non-professionals) consigned the film to the festival circuit.    Other reviewers noted that "Some of the talk is intriguing, but only some,"  while another felt that "with the action confined to one room, the film is too much like a play, and, rather dull in execution, it lacks the atmosphere it needs to capitalise on its premise." 

== References ==
 

== External links ==
* 
* 
*  at the Chinese Movie Database

 

 
 
 
 
 
 
 