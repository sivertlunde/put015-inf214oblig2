A Quiet Place in the Country
 
{{Infobox film
| name           = A Quiet Place in the Country
| image          = Eliopetriredgraveposter.jpg
| caption        = Promotional poster
| director       = Elio Petri
| producer       = Alberto Grimaldi
| writer         = Elio Petri Tonino Guerra Luciano Vincenzoni
| starring       = Franco Nero Vanessa Redgrave
| music          = Ennio Morricone
| cinematography = Luigi Kuveiller
| editing        = Ruggero Mastroianni
| studio         = Produzioni Europee Associati Les Productions Artistes Associés
| distributor    = United Artists
| released       =  
| runtime        = 106 minutes
| country        = Italy France
| language       = Italian
| budget         = 
}}

A Quiet Place in the Country ( ,  ) is a 1968 Italian-French horror film directed by Elio Petri. It was entered into the 19th Berlin International Film Festival, where it won a Silver Bear award.   

==Cast==
* Franco Nero as Leonardo Ferri
* Vanessa Redgrave as Flavia
* Georges Géret as Attilio
* Gabriella Grimaldi as Wanda
* Madeleine Damien as Wandas Mother
* Rita Calderoni as Egle
* John Francis Lane as Asylum Attendant
* Renato Menegotto as Egles Friend
* David Maunsell as Medium
* Arnaldo Momo as Villager
* Sara Momo as Villager
* Otello Cazzola as Villager
* Constantino De Luca as Villager
* Marino Bagiola as Villager
* Piero De Franceschi as Villager

==Reception==
Reviewing the 2011 MGM DVD release, Paul Mavis of DVDTalk.com wrote, "A disturbing, sensational aural/visual experience. Writer/director Elio Petri creates a completely unstable environment for his tale of personal madness, artistic chaos, and supernatural violence. Vanessa Redgrave and Franco Nero are beautiful to look at here." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 