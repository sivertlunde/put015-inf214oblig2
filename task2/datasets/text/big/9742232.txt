None Shall Escape
{{Infobox film name = None Shall Escape image = caption = director = André De Toth writer = Alfred Neumann Joseph Than starring = Marsha Hunt Alexander Knox Henry Travers producer = Samuel Bischoff
|distributor= Columbia Pictures budget = released =   country = United States runtime = 85 minutes language = English
}}
 1944 war Marsha Hunt), a woman that he was once engaged to.
 Alfred Neumann and Joseph Than were nominated for an Academy Award for Best Story.

==Plot==
In the trial, it is revealed that Grimm (Alexander Knox), who fought for Germany in World War I and lost a leg in battle, returns after the war to the small German village (now in newly independent Poland) where he had been a teacher. Despite the recent hostilities, he is welcomed back into the community and resumes his teaching. He also resumes his relationship with Paeierkowski, a local Polish girl to whom he had become engaged before the war.

He is bitter about Germany losing the war and it is obvious he has been changed by the experience. He treats the villagers with disdain, and his upcoming marriage is cancelled. Taunted by the schools pupils, he rapes one of them, who subsequently kills herself. After a trial fails to convict him, he returns to Germany after borrowing money from the priest, joins the Nazi Party, and rises through the ranks of the party.

When the Nazis come to power, he even sends his brother Karl to a concentration camp and gets his brothers son into the Hitler Youth. When World War II starts, Grimm becomes the commander of the occupying force of the same village where he had previously lived. He treats the villagers brutally, including Father Ezekiel and his former fiancée. His nephew eventually turns against Grimms brutality and renounces his Nazi allegiance, with tragic results.

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 