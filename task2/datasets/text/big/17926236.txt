Rockula
 
{{Infobox film
| name = Rockula
| image = 
| alt = 
| caption = 
| director = Luca Bercovici
| producer = Jefery Levy
| writer = Luca Bercovici  Jefery Levy  Chris Ver Wiel
| starring = Dean Cameron  Toni Basil  Thomas Dolby  Tawny Fere  Susan Tyrrell  Bo Diddley
| music = Hilary Bercovici  Osunlade
| cinematography = John Schwartzman
| editing = Maureen OConnell Cannon Films Cannon Films
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget = United States dollar|US$1,500,000
| gross = 
}}
Rockula is a 1990 comedy film about a vampire under a curse- The tagline is "Hes a vampire that hasnt scored in 400 years- tonights the night!" It was directed by Luca Bercovici, and co written with Bercovici and Jefery Levy. Starring as the vampire, Ralph LaVie, is Dean Cameron a prolific 1980s comedic actor. Toni Basil plays Phoebe LaVie, Ralphs mother. Thomas Dolby as the villain Stanley and Bo Diddley has somewhat more than a cameo, as Axman, playing in Ralphs band, incidentally named Rockula. The Director of Photography was John Schwartzman, who was later the DP on "Seabiscuit" and "Saving Mr. Banks."

== Plot ==
Ralph Lavie (Dean Cameron) is a friendly vampire. Hes 400 years old and still lives with his mother, Phoebe (Toni Basil), who is also a vampire. Ralph is living under a curse. After meeting Mona, the girl of his dreams, 308 years ago Ralph lost her after she was killed by her jealous boyfriend, a pirate. Since that time Mona has been reincarnated every 22 years, only to fall in love with Ralph and die under the exact same circumstances (killed on Halloween by a rhinestone-peg-legged pirate wielding a giant hambone). Ralph, weary of the whole thing, vows to stay locked in his room and not meet Mona again until after Halloween, much to the chagrin of his sentient and libidinous reflection (another side effect of the curse).

Ralph meets up with friends at a local bar (including rock legend Bo Diddley) and once again recounts the story of his curse to them. Walking out into the streets he is hit by a car being driven by none other than Monas latest incarnation, a local singer (Tawny Fere). Immediately warning her off him Ralph runs off, but a portentous dream reinvigorates him to try and save her life now that events have been set in motion. He goes looking for her and discovers a flyer with her band playing at a local club. When he goes to visit her, the two hit it off immediately much to the dismay of Monas manager/ex-boyfriend, an eccentric mortuary owner named Stanley (Thomas Dolby). After Ralph is unable to adequately say what he does for a living, he states that hes in a band. Realizing that he actually is a decent musician after hundreds of years of piano and guitar lessons, he and his friends start the band Rockula, playing up his vampirism as the bands gimmick. Rockulas first gig is a success and the band quickly becomes a hit. Ralph and Mona start dating and even start collaborating musically, writing songs and filming a video together. Stanley sees this as a threat and seeks the council of a local Psychic, Madame Ben-Wa who reveals that Stanley is a vampire. He begins a plot to kill Ralph and cryogenically freeze Mona using equipment from his mortuary so hell have her forever.

Ralph and Mona have dinner with Phoebe and her latest beau, which proves awkward as Phoebe makes no secret of her unnaturally long life, referring to things such as George Washingtons bedroom prowess and speaking to Mona as if shes known her for years. The evening culminates in an impromptu musical number by Phoebe. On the drive home, rather than have Mona think he and his mother are crazy, he reveals that he is a vampire and tells her the details of the curse. Mona is skeptical until Ralph half-transforms into a bat in front of her. Disturbed by this, Mona drives off and leaves town. Halloween comes and Rockula is scheduled to perform. Stanley appears, dressed as a pirate, with a hambone, having been directed to do so by Madame Ben-Wa. Mona, having admitted that she loves Ralph returns and meets him on stage, only to be kidnapped and carted away by Stanley, who prepares to place her in the cryogenic freezing chamber. Ralphs reflection tells him where to find Mona and he comes to the rescue. Stanley and Ralph duel with Stanley wielding the hambone and Ralph wielding the rhinestone peg-leg. Madame Ben-Wa is then revealed to be Phoebe, who has orchestrated the curse for years, scared that Ralph will leave the nest. Phoebe apologizes, realizing that she needs to let Ralph grow up, but is knocked out by Stanley. Ralph then again half-transforms into a bat, scaring Stanley into falling into his own freezing chamber. Having saved Monas life the curse is broken, and the two leave the club together happily.

Ralphs reflection has had enough and smashes his way out the mirror dressed as Elvis Presley in a rhinestone jumpsuit and performs a final number as Rockula.

==Crew details==
The Director of Photography, John Schwartzman, was later the DP on "Seabiscuit" and "Saving Mr. Banks."

==See also==
*Vampire film

==External links==
*  
*  

 
 
 
 
 
 


 