Thirty Three (film)
 
{{Infobox film
| name           = Thirty Three
| image          = 33 (DVD cover).jpg
| image_size     = 200px
| caption        = DVD cover
| director       = Georgiy Daneliya
| producer       = 
| writer         = Georgiy Daneliya Valentin Ezhov Viktor Konetsky
| narrator       = 
| starring       = Yevgeny Leonov
| music          = Andrei Petrov
| cinematography = Sergei Vronsky
| editing        = 
| distributor    = 
| studio         = Mosfilm
| released       = 22 April 1965
| runtime        = 77 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}
 Soviet comedy film directed by Georgiy Daneliya.

==Plot==
A rough factory worker of a distant rural Russian town becomes a national celebrity when it is discovered that his mouth bears 33 teeth. He is brought to Moscow, where he discovers a new world for him. Suddenly it becomes known that he with his 33rd tooth can be a descendant of Martians and he is selected for a dangerous space mission.

==Cast==
* Yevgeny Leonov - Ivan Travkin
* Nonna Mordyukova - Galina Pristyazhnyuk
* Lyubov Sokolova - Travkins wife
* Viktor Avdyushko - Misha
* Saveliy Kramarov - Rodion Homutov
* Gennadi Yalovich -Scheremetyev	
* Nikolai Parfyonov - Prokhorov
* Mariya Vinogradova - Margarita Pavlovna
* Inna Churikova - Rosa
* Arkadi Trusov - Ivanov		
* Vyacheslav Nevinny - Vasili
* Vladimir Basov - Museum boss
* Frunzik Mkrtchyan - Professor Bruk
* Rita Gladunko	
* Sergei Martinson - Rosas father
* Irina Skobtseva - Vera Sergeyevna		
* Svetlana Svetlichnaya - TV worker
* Yuri Sarantsev - Taxi driver
* Pyotr Shcherbakov - writer

==External links==
* 

 

 
 
 
 
 

 