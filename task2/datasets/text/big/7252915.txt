Restraint (film)
 
{{Infobox film
| name           = Restraint
| image          = Restraint_poster.jpg
| caption        = Restraint poster
| writer         = Dave Warner
| starring       = Stephen Moyer Travis Fimmel Teresa Palmer
| director       = David Denneen
| producer       = Dan Halsted Mark Lazarus Anna Fawcett Todd Fellman
| music          = Elliott Wheeler
| cinematography = Simon Duggan
| editing        = Rodrigo Balart Toby Denneen
| runtime        = 90 minutes
| released       =  
| country        = Australia
| language       = English
| budget         = $5,000,000 
}}
 thriller film, directed by David Denneen, written by Dave Warner and starring Stephen Moyer, Travis Fimmel and Teresa Palmer. The film was shot on location around New South Wales, Australia in mid-2005. Working titles during production were Ravenswood, Guests and Power Surge. It also features a cameo by Vanessa Redgrave.

==Plot== agoraphobic art dealer with a dark past of his own. Captive by way of taking refuge in a magnificent country estate, Dale (Teresa Palmer) and Ron (Travis Fimmel) find themselves participants in a game of survival. It is implied that Ron had killed Dales boss and her father, so that both of them would live happily ever after. 

Dale, a stripper, is resentful of the way she has been treated in the past. Their hostage, Andrew (Stephen Moyer), appears to represent everything shes always wanted but unable to have. Ron, impulsive and out of control, loves Dale and will do whatever he can to take her away from her old life. Dale saves Andrew from being killed by Ron. However, Ron gets a deal of AU 40,000 dollars to keep Andrew alive. Someone, though, must go to the bank and cash in the cheque. Andrew suggests Dale do it since she resembles his fiancée, Gabrielle. Dale, as Gabrielle, drives to town and enters the bank without creating suspicion.

Andrew tells Ron that he had hired several hit men to kill his fiancée years ago, because he was smuggling artwork out of Europe to sell to the highest bidder. Her father knew about his dealings, but Andrew had him killed too, before he told anyone else. Ron becomes skeptical at his story, until he finds dresses that Andrew had kept in his closet after Gabrielle "moved" to Europe. Now threatening him with blackmail, Andrew decides to give Ron all of his money to keep quiet about the murders he had committed. Andrew and Ron fight over a shotgun, which Ron grabs from Andrew and telling him it wasnt loaded. Ron realizes that Andrew cant be trusted.

In the process of getting the cash, Ron observes that Dale is slipping away and puts her love to test. She fails and it is now Andrews turn to save her life. Finally, Ron is killed by Dale who is forced to assume the identity of Gabrielle after Andrew threatens to call the police. Andrew tells Dale that he never killed Gabrielle or her father. He lied so that hell never have to be lonely again, and starts dancing with joy to the music of Beethoven that hes now free of his guilt, while Dale realizes she may have made a terrible mistake, leaving the ending ambiguous.

==Cast==
*Teresa Palmer as Dale
*Travis Fimmel as Ronald Ron Jason Beron
*Stephen Moyer as Andrew Shepard
*Philip Holder as Sgt. Paul Wittens
*Margie McCrae as Mrs. Wynott
*Peter Davies as Terry Gilmore
*Alyssa McClelland as Gabrielle Nate Jones as Tim
*Joanne Hunt as Constable Edwina Blainey

==Production==
Over half the budget came from the Film Finance Corporation. The film was shot in late 2005 near Goulburn and Camden in New South Wales, originally using the titles Guests then Ravenswood. Stephen Moyer was imported from the UK to support his two Australian co-stars.   accessed 14 April 2012 

==Release==
The film was originally meant to be released in May 2006 by Accent Entertainment. Multiple test screenings resulted in the films running time being reduced from 120 to 90 minutes, and the ending being changed to something more ambiguous. It eventually screened in one cinema in Sydney in April 2009. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 