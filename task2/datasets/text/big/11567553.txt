Our Blushing Brides
{{Infobox film
| name           = Our Blushing Brides
| image          = Posterourbb.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Harry Beaumont (uncredited)
| producer       = Harry Beaumont
| writer         = Bess Meredyth John Howard Lawson Edwin Justus Mayer Helen Meinardi (uncredited) Robert Montgomery
| music          = 
| cinematography = Merritt B. Gerstad
| editing        = George Hively Harold Palmer (uncredited)
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 99 minutes 
| country        = United States
| language       = English
| budget         = $337,000  . 
| gross          = $1,211,000 
}}
 romantic melodrama Robert Montgomery, Anita Page, and Dorothy Sebastian.

The film is a follow-up to Our Dancing Daughters (1928) and Our Modern Maidens (1929), which also starred Crawford, Page, and Sebastian.  The two previous installments in the series were both silent films, while Our Blushing Brides is a sound film which was a relatively new aspect to motion pictures. The fact that it features audible dialogue was an advertising point mentioned on the movie poster.

Our Blushing Brides is Crawfords thirty-first film Filmography of Joan Crawford|(of eight-six total), and her fourth sound film. In her first "shopgirl-Cinderella" role, Crawford portrays the role of Gerry, a department store "Model (people)|mannequin" who falls in love with the wealthy son of her boss.  The role was a departure from Crawfords flapper girl persona of the silent area as Metro-Goldwyn-Mayer began to develop a more sophisticated image of her. {{cite book|last1=Háy |first1=Peter|authorlink=Peter Háy (Canadian author, publisher and bookseller)
|title=MGM: When the Lion Roars|year=1991|publisher=Turner Publishing, Inc.|isbn=1-878685-04-X|page=72}} 

==Plot==
Fellow department store shopgirls and roommates Gerry March (Crawford), Connie Blair (Anita Page) and Franky Daniels (Dorothy Sebastian) take different paths in the New York City, but all seek to marry wealthy men. Connie pursues an affair with David Jardine (Raymond Hackett), son of the department store owner.  Meanwhile, Franky meets the slick-talking Marty Sanderson (John Miljan) when he comes into the store to buy $500 worth of towels.  However, when Sanderson comes to pick Franky up, he hits on Gerry instead.
 Robert Montgomery), elder son of the store owner.  He is used to getting what he wants, but when he invites her to visit the gardens on his estate alone. Gerry, who believes that virtue will be her only reward, rebuffs Tony and intimates that he is childish.
 criminal gang that steals from department stores like the one the women work at.  The police come to apprehend Franky, believing she is a part of the gang, but she knows nothing of it.  

Meanwhile, Connie is very happy with David and intends to marry him.  However, she reads in the newspaper that David intends to marry the high-society Evelyn Woodforth (Martha Sleeper).  She listens to the reception being broadcast on the radio and takes poison in an attempt kill herself.  Gerry finds her and goes to Tony in order to force David to leave his reception to visit Connie.  In a contentious conversation, Tony forces David to leave and visit Connie, and this selfless act attracts Gerry and convinces her that Tony is a good guy after all.  However, despite Davids visit, Connie dies.

==Cast==
  
* Joan Crawford as Geraldine "Gerry" March
* Anita Page as Connie Blair
* Dorothy Sebastian as Francine Daniels Robert Montgomery as Tony Jardine
* Raymond Hackett as David Jardine
* John Miljan as Martin W. "Marty" Sanderson
* Hedda Hopper as Lansing Ross-Weaver
* Albert Conti as Monsieur Pantoise
* Edward Brophy as Joseph Munsey
 
* Robert Emmett OConnor as The Detective
* Martha Sleeper as Evelyn Woodforth
* Gwen Lee as Miss Dardinelle
* Mary Doran as Eloise
* Catherine Moylan as Mannequin
* Norma Drew as Mannequin
* Claire Dodd as Mannequin
* Wilda Mansfield as Mannequin
 

==Box office==
According to MGM records the film earned $874,000 in the US and Canada and $337,000 elsewhere resulting in a profit of $412,000. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 