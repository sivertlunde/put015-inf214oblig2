UFOria
{{Infobox film name       = UFOria image      = UFOria cover.jpg caption    = UFOria film cover director   = John Binder writer     = John Binder starring   = Fred Ward Cindy Williams Harry Dean Stanton producer   = Melvin Simon Paramount Cinema CIC
|budget     = released   =   runtime    = 93 minutes language   = English
}}
 movie starring Fred Ward, Harry Dean Stanton, Harry Carey, Jr. and Cindy Williams. It was directed and written by John Binder. The film includes small appearances by Peggy McKay, Joe Unger, Hank Worden and Charlotte Stewart. Filming was completed in 1981, but the film was not theatrically released until 1985. Due to poor audience attendance, the film was not a financial success. It was only released on VHS in 1987 by MCA Home Video, now known as Universal Studios Home Entertainment.

== Plot ==
Sheldon Bart (Fred Ward) is a drifter, and a small-time con man. He meets his old friend, Brother Bud (Harry Dean Stanton), a big-time con man into faith healing and fencing stolen cars, at his revival tent outside a small town. While hes helping Brother Bud, he falls in love with Arlene (Cindy Williams), a local supermarket clerk who believes in UFOs and is deeply religious and deeply lonely. When Arlene has a vision of a coming UFO, everyone deals with it in their own way.

==Release==
===Theatrical===
Despite being completed in 1981, the film was not released theatrically until 1985. It was not successful in theaters because it did not have much of an audience. 

===Home media===
MCA Home Video, now known as Universal Studios Home Entertainment, released the film on VHS in 1987.  The film was never released on DVD. 

The film also aired on cable television in 1986.

==Reception==
===Critical response=== Repo Man," "Turtle Diary" and "Hannah and Her Sisters," it is willing to go for originality in a world that prizes the entertainment assembly line." 

Vincent Canby, a reviewer writing for the New York Times, said that the film has a "raffish tone" and is "exuberantly nutty," while also praising the casting.  Kevin Thomas, writing for the Los Angeles Times concluded his review with, "Williams manages to be adorable and never seems all-out crazy; like Ward, you do believe in her, whether or not you believe in UFOs." 

==References==
 

== External links ==
*  

 
 
 
 
 
 


 
 