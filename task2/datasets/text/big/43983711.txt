The Aviation Cocktail
{{Infobox film
| name           = The Aviation Cocktail
| Image       = File:The Aviation Cocktail (2012 film).jpg
| alt            =  
| caption        = 
| director       = David R. Higgins
| producer       = Bobby Deline  David R. Higgins
| writer         = David R. Higgins
| starring       = Michael Haskins  Brandon Eaton Beau Kiger Leah Lockhart
| music          = Wilson Helmericks
| cinematography = Mark Rutledge
| editing        = Patrick Behan
| studio         = Pilots Lounge
| distributor    = Indie Rights 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 The Aviation Cocktail.

==Plot== midwestern town in 1958. It opens on pilot Jack Fisher (Michael Haskins), who gets a call from his sheriff brother Henry (Beau Kiger) with word about a suspected serial killer he has been tracking. They arrive to find the brothers longtime friend Bob Halloran (Brandon Eaton), has a posse formed in a standoff with the suspect holding a young girl hostage in a barn. Once the posse discovers the hostage is dead, they unload bullets into the barn, injuring the suspect. The Fisher brothers and Halloran load the bleeding suspect into Jacks plane, but a decision is made during the flight that propels mounting tension between the 3 men.

A psychological aftermath unravels from the personal torment each of them deals with, as well as tensions from the estranged behavior of the victims brother, Dale Riley, which leads to suspicions. As the story progresses, we see that the characters idyllic family lives only disguise a reality of lies and betrayal.  

==Cast==
* Jack Fisher as Michael Haskins
* Bob Halloran as Brandon Eaton
* Henry Fisher as Beau Kiger
* Alice Fisher as Leah Lockhart
* Connor Boyle as Dale Riley
* June Fisher as Katie Bevard
* Geoff Hadley as Mark Hanson

==Production== Senator Colby Coashs film incentive bill LB863, investing $5,000 for the $40,000 to $45,000 the production spent locally. 

==Release== North American New York, and in November at the Denver Film Festival, its Colorado premiere, where much of the cast and crew resides. In January, 2013 it screened at the Trail Dance Film Festival in Oklahoma, and then in February, 2013 in premiered in Valentine, Nebraska, where most of the film was shot. The Omaha Film Festival also screened the film in March, 2013. The film is currently available on Video on demand|VOD.

==Awards==
{| class="wikitable"
|-
! Festival !! Award !! Category
|-
| Trail Dance Film Festival || Golden Drover  || Best Feature Narrative 
|-
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 