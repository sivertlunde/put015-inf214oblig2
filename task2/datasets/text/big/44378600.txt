Teen Chehre
 

{{Infobox film
| name           = Teen Chehre
| image          =  
| director       = Y. N. Kapoor
| producer       = Deepak Kapoor  Ramesh Sharma  S. A. Patel
| starring       = Sujit Kumar Anjana
| music          = Shyam Ganshyam
| Lyrics        =  Mohsin Nawaab Saajan Dehlavi Upendra
| Singer         =  Krishna Kalle Madhu Chandra Mohammad Rafi Varsha Bhonsle Usha Mangeshkar Chetan
| released       = 1979
| country        = India
| language       = Hindi 
| budget         =
}}

Teen Chehre (three Faces) is a 1979 Indian Hindi  Action film directed by Y N Kapoor. The movie stars  Anjana and Sujit Kumar  in lead roles.  Jayshree T., Meena T, Mohan Choti, Birbal, and Leela Patel play supporting roles.The music was composed by Shyam Ghanshyam.

==Cast==
* Sujit Kumar 
* Anjana 
* Jayshree T. 
* Meena T 
* Mohan Choti 
* Birbal 
* Raviraj 
* Nana Palsikar 
* Ravindra Mahajani

==Soundtrack==

* 1 "jo bhee jisne maangaa o toone usko de diyaa"
* 2 "o is andaaz se baadaaye-wafra karte baato baato mein"
* 3 "likhi hey yah bidhana ne kismat gareeb ki "
* 4 "nazar ki nazar se milane aayi hoo "
* 5 "la la hum hey is tarah janaab ke liye"
* 6 "kahane pe gar mein aayaa ek baat kahoongaa"

 
 
 
 


 