Fire in the Sky
 
{{Infobox film name = Fire in the Sky image_size = 225px image = Fire in the sky poster.jpg caption = Theatrical release poster director = Robert Lieberman producer = Joe Wizan Todd Black screenplay = Tracy Tormé based on =   starring = D. B. Sweeney Robert Patrick Craig Sheffer Peter Berg James Garner music = Mark Isham cinematography = Bill Pope editing = Steve Mirkovich studio = distributor = Paramount Pictures released =   runtime = 109 minutes    country = United States language = English gross = $19,885,552      budget = $15,000,000 
}} science fiction horror drama Scott MacDonald, Henry Thomas, and Peter Berg also star.

==Plot== logger Travis White Mountains. 

Driving home from work, the men come across an   despite no apparent motive or knowledge of Waltons whereabouts.
 Scott MacDonald). The men are offered a lie detector test and take it. Dalliss test is inconclusive. After the testing is complete, Rogers is outraged that the results arent shared.
 Heber gas flashback of the abduction by the extraterrestrials. In his hallucination, Walton wakes up in a cocoon. He attempts to escape but has to deal with a weightless environment. After they discover him wandering around, the aliens drag Walton to an exam room for experimentation. Stripping him to his underwear and covering him with a rubber sheet, which pins him to an examination table, the aliens subject him to an extremely painful experiment in which tubes are shoved down his throat, a sharp device is inserted into his neck, and a needle goes through his eye.

While interviewing Walton, Lieutenant Watters expresses his doubts about the abduction as merely a hoax. He notes Waltons new found celebrity because of the tabloids attempts to profit from his tale. The film culminates with a denouement between Walton and Rogers. The closing titles state that in 1993 the loggers were resubmitted to additional polygraph examinations, which they passed, corroborating their innocence.

==Cast==
* D. B. Sweeney as Travis Walton
* Robert Patrick as Mike Rogers
* James Garner as Lt. Frank Watters
* Craig Sheffer as Allan Dallis
* Peter Berg as David Whitlock
* Henry Thomas as Greg Hayes
* Bradley Gregg as Bobby Cogdill
* Noble Willingham as Sheriff Blake Davis
* Kathleen Wilhoite as Katie Rogers
* Georgia Emelin as Dana Rogers Scott MacDonald as Dan Walton

==Production==
The film is based on the book The Walton Experience by Travis Walton. In the book, Walton tells of how he was abducted by a Unidentified flying object|UFO. Waltons original book was later re-released as Fire in the Sky (ISBN 1-56924-710-2) to promote the books connection to the film. The real Travis Walton makes a cameo appearance in the film.

The special effects in the film were coordinated by Industrial Light & Magic, and the cinematography was handled by Bill Pope.

The original music score for the film was composed and arranged by Mark Isham. The audio soundtrack was released in Compact Disc format on March 30, 1993.

==Reception==
Despite mixed critical reviews upon release,  Fire in the Sky has gone on to be described as a cult favorite among  .   wrote "Fire In the Sky leans in favor of believers, suggesting that all of this really did happen. And some of it is fairly entertaining." However, he notes "the film tells its story in deadly earnest, and that is its greatest failing. Had the approach been more humorous or satirical, without necessarily sacrificing the sense that these characters believe it all — in the manner of Melvin and Howard, for example — it might be more palatable."  Film critic James Berardinelli called the movie a  "muddled-up mess", saying "It cant make up its mind whether it wants to be horror, drama, or science-fiction, and, consequently, succeeds as none." 

Troy Brownfield of MSNBC, in a 2009 article on alien abductions in film, ranked it number seven of ten, and described the abduction scenes as "harrowing" and "genuinely frightening". Brownfield praised Torme and Robert Lieberman|Liebermans writing, saying, "Credit should go to screenwriter Tracy Torme and director Robert Lieberman, as they were called upon to punch up Walton’s original account". 
 Chris Carter was impressed by Patricks performance in the film, which led to his casting Patrick as FBI Special agent John Doggett for the shows eighth season in 2000.   

==Release==
 
At its widest release in the U.S., the film was screened at 1,435 theaters grossing $6,116,484 in its opening weekend. The film went on to gross $19,885,552 in ticket sales through a 4-week theatrical run.
 Region 1 DVD in the United States on October 19, 2004. Currently, there is no set date on a future Blu-ray Disc release for the film.

==References==
 

==External links==
*  at Allmovie
* 
*  at the Movie Review Query Engine
*  at Rotten Tomatoes
*  at Box Office Mojo
*  on This American Life

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 