In the Nick
{{Infobox film
| name           = In the Nick
| image          = 
| caption        = 
| director       = Ken Hughes 
| producer       = 
| screenplay     = 
| starring       = Anthony Newley
| music          = 
| cinematography = 
| editing        = 
| studio         = Warwick Films
| distributor    = Columbia Pictures
| released       = 1960
| runtime        = 
| country        = United Kingdom
| language       = English
| gross = 
}}
In the Nick is a 1960 British comedy film directed by Ken Hughes and starring Anthony Newley, Anne Aubrey, Bernie Winters, James Booth and Harry Andrews.  In the film, a gang of incompetent criminals are placed in a special type of new prison.

==Cast==
* Anthony Newley - Dr. Newcombe
* Anne Aubrey - The Doll
* Bernie Winters - Jinx Shortbottom
* James Booth - Spider Kelly
* Harry Andrews - Chief Officer Williams
* Al Mulock - Dancer
* Derren Nesbitt - Mick
* Niall MacGinnis - Prison Governor
* Victor Brooks - Screw Smith
* Ian Hendry - Ted Ross
* Kynaston Reeves - Judge
* Barry Keegan - Screw Jenkins
* Diana Chesney - Barmaid

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 