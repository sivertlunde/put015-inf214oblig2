Amy (2015 film)
 
{{Infobox film
| name           = Amy
| image          = AmyFilmPoster.jpg
| caption        = Original theatrical poster
| director       = Asif Kapadia
| producer       = James-Gay Rees
| writer         = 
| starring       = Amy Winehouse Antonio Pinto
| cinematography = Matt Curtis
| editing        = Chris King
| distributor    = Universal Studios / A24 Films
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
 Universal Music UK. He stated: "About two years ago we decided to make a movie about her — her career and her life. Its a very complicated and tender movie. It tackles lots of things about family and media, fame, addiction, but most importantly, it captures the very heart of what she was about, which is an amazing person and a true musical genius."  

The theatrical poster for the documentary film was released on 18 March. A teaser trailer was released on 2 April 2015, with receiving over one million views after 35 hours on YouTube. The teaser trailer shows a young Amy at her most vulnerable, when it shows her talking about how she feels about music and fame. Footage from the trailer shows Winehouse as a young woman at the beginning of her music career answering questions about how she sees herself as an artist.

It has been selected to by shown in the Midnight Screenings section at the 2015 Cannes Film Festival.   

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 