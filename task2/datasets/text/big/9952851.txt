The Red Rider
 
{{Infobox film
| name           = The Red Rider
| image_size     =
| image	         = The Red Rider FilmPoster.jpeg
| caption        =
| director       = Lew Landers
| producer       = Milton Gatzert Henry MacRae (associate) Ella ONeill Basil Dickey W. C. Tuttle
| narrator       = Walter Miller Richard Cramer
| music          = David Klatzkin Al Short Richard Fryer
| editing        = Saul A. Goodkind Edward Todd Joseph Gluck Louis Sackin
| distributor    = Universal Pictures
| released       =  
| runtime        = 15 chapters (310 min)
| country        = United States English
| budget         =
}} Universal Serial movie serial 1931 John Wayne movie Range Feud.

==Cast==
* Buck Jones as "Red" Davidson
* Grant Withers as "Silent" Slade
* Marion Shilling as Marie Maxwell Walter Miller as Jim Breen
* Richard Cramer as Joe Portos
* Silver as Silver, Reds Horse
* Charles K. French as Robert Maxwell
* Margaret La Marr as Joan McKee
* Edmund Cobb as Johnny Snow
* Monte Montague as Bill Abel, one of Portos henchmen
* Jim Thorpe as Al Abel, one of Portos henchmen William Desmond as Sheriff Campbell
* Lee Beggs as Mayor "Soapy" Caswell
* Robert McGowan as Hubert Sund
* J. Frank Glendon as an Attorney

==Production==
The Red Rider was based on "Redhead from Sun Dog" by W. C. Tuttle. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 10
 | chapter = 2. In Search of Ammunition
 }} 

===Stunts=== Cliff Lyons Tom Steele

==Chapter titles==
# Sentenced to Die
# A Leap for Life
# The Night Attack
# A Treacherous Ambush
# Trapped
# The Brink of Death
# The Fatal Plunge
# The Stampede
# The Posse Rider
# The Avenging Trail
# The Lost Diamonds
# Double Trouble
# The Night Raiders
# In the Enemies Hideout
# Brought to Justice
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 210
 | chapter = Filmography
 }} 

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 
{{Succession box Universal Serial Serial
| before=The Vanishing Shadow (1934)
| years=The Red Rider (1934) Tailspin Tommy (1934)}}
 

 
 

 
 
 
 
 
 
 
 
 
 