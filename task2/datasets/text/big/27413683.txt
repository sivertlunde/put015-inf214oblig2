New Low
 
{{Infobox film
| name = New Low
| image = New Low.jpg
| alt =  
| caption = Film poster
| director = Adam Bowers
| producer = Adam Bowers
| writer = Adam Bowers Adam Bowers|Jayme Valerie Jones|Toby Turner
 }}
| music = unknown
| cinematography = Ryan Moulton
| editing = Adam Bowers
| studio = unknown
| distributor = New Video
| released =  
| runtime = 82 minutes
| country = United States
| language = English
| gross =
}}
New Low is a 2010 independent American romantic comedy film written, directed, produced, and edited by Adam Bowers, who also stars in the film along with Jayme Ratzer, Valerie Jones, and YouTube personality Toby Turner. It premiered January 23, 2010 at the Sundance Film Festival. 

== Plot ==
Wendell, a neurotic, aimless twentysomething, struggles to figure out which girl he really belongs with: Joanna, the best girl hes ever known, or Vicky, the worst. His friend Dave helps him sort through his thoughts as Wendell discovers not only whom he should be with but who he truly is under all that neurosis.

== Reception==
New Low has received mostly positive reception. Variety called the film "hilarious" and said it "smells like a cult hit." 

The Orlando Sentinel rated it, "3.5 out of 4 stars."  

Independent Film Quarterly said, ""Easily containing some of the best film writing and dialogue over the past few years, New Low looks and feels like a Woody Allen-penned script filtered through a Slacker-era Richard Linklater visual sensibility."

The A/V Club commented, "Sharp, witty, frequently laugh-out-loud funny."

== Release ==
On January 17, 2011, the film was released digitally in the US through a partnership between the Sundance Institute and New Video.  It is also being self-distributed internationally on DVD through the movies website. 

== References ==
 
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 


 