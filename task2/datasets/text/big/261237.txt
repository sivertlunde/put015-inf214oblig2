The Gods Must Be Crazy
 
 

{{Infobox film
| name           = The Gods Must Be Crazy
| image          = Gods must be crazyposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jamie Uys
| producer       = Jamie Uys
| writer         = Jamie Uys
| narrator       = Paddy OByrne
| starring = {{plainlist|
* N!xau
* Sandra Prinsloo
* Marius Weyers
* Nic De Jager
* Michael Thys
* Louw Verwey
* Ken Gampu
* Simon Sabela
}}
| music          = John Boshoff
| cinematography = Buster Reynolds Robert Lewis
| editing        = Stanford C. Allen Jamie Uys
| released       =  
| runtime        = 109 minutes
| country        = South Africa Botswana
| language       = English Afrikaans Juǀʼhoan Ungwatsi
| distributor    = Ster Kinekor     20th Century Fox  
| budget         = $5 million 
| gross = over $60,000,000 (US)   
}}
The Gods Must Be Crazy  is a 1980 South African comedy film written and directed by Jamie Uys. Financed only from local sources, it is the most commercially successful release in the history of South Africas film industry.  Originally released in 1980, the film is the first in The Gods Must Be Crazy (film series)|The Gods Must Be Crazy series. It is followed by one official sequel and three unofficial sequels produced in Hong Kong.
 Sho of San farmer Nǃxau) whose tribe has no knowledge of the world beyond, Andrew Steyn (Marius Weyers), a biologist who analyzes manure samples for his PhD dissertation, and Kate Thompson (Sandra Prinsloo), a newly hired village school teacher.

==Plot==
Xi and his San tribe are "living well off the land" in the Kalahari Desert. They are happy because of their belief that the gods have provided plenty of everything, and no one among them has any wants. One day, a Coca-Cola bottle is thrown out of an airplane and falls to Earth unbroken. Initially, Xis people suppose this strange artifact is another "present" from the gods and find many uses for it.  (They employ it as a crafts tool, blow the top to make music, etc.) But unlike anything that they have had before, there is only one glass bottle to go around. With everyone wanting it at once, they soon find themselves experiencing envy, anger, and even violence.

Since the bottle has caused the tribe unhappiness, Xi consults with elders and concludes that its an "evil thing" which the gods were "absent-minded" to send them.  Noting that some attempts to dispose of the bottle have failed, Xi agrees to make a pilgrimage to the edge of the world and toss the seemingly cursed thing off.
 guerrillas led by Sam Boga, who are being pursued by government troops after an unsuccessful attack; a safari tour guide named Jack Hind; and Steyns assistant and mechanic, Mpudi (who is maintaining his cantankerous Land Rover).

When hungry Xi happens upon a corral, he shoots a goat with a tranquilizer arrow. He is shortly jailed for this attempt on livestock. Mpudi, who once lived with the San and still speaks Xis dialect, concludes that Xi will die if kept incarcerated.  He and Steyn apply to employ Xi as a tracker for the remainder of his sentence in lieu of prison. Meanwhile, the guerrillas invade Kates school and take her and the students as human shields for their escape to the neighboring country.

Steyn, Mpudi, and Xi soon discover their field work (observing the local wildlife) is on the terrorists chosen path. They manage to immobilize the guerrillas as they are passing by and save Kate and the children. But Jack Hind takes away Kate, and Steyn is left at the same place again.

With Xis term over, Steyn insists upon paying his wages and sending him on his way. Steyn tells Kate about his problem and Kate is impressed and Steyn begins a relationship with Kate despite his acute clumsiness around women.

Xi eventually finds himself at the top of a cliff with a solid layer of low-lying clouds obscuring the landscape below. This convinces Xi that he has reached the edge of the world, and he throws the bottle off the cliff. Xi then returns to his tribe and receives a warm welcome from his family.

==Cast==
*N!xau as Xi
*Marius Weyers as Andrew Steyn
*Sandra Prinsloo as Kate Thompson
*Michael Thys as MPudi (voiced by Pip Freedman)
*Louw Verwey as Sam Boga

==Release== Tswana lines.   At the time, it broke all box office records in Japan and it broke all box office records for a foreign film in the United States.

In mid-November 1986, The Gods Must Be Crazy was released on videocassette in the U.S. by CBS/Fox Video|CBS/Fox  through their Playhouse Video label. 

==Reception==
Based on nineteen reviews, The Gods Must Be Crazy has carried a 95% "Fresh" score on Rotten Tomatoes.  Film critic Roger Ebert gave the film three stars out of four, and said, in his conclusion, that "It might be easy to make a farce about screwball happenings in the desert, but its a lot harder to create a funny interaction between nature and human nature. This movies a nice little treasure". 

==Aftermath==
Despite the films having grossed over $100 million worldwide, Nǃxau reportedly earned less than $2,000 for his starring role. Before his death, Uys supplemented this with an additional $20,000 as well as a monthly stipend.    

==Sequels==
* The Gods Must Be Crazy II, 1989

==See also==
* The Gods Must Be Crazy (film series)

==References==
 

==External links==
 
*   - and article from the Rapport weekly, which contains an interview with the films stars and producer Boet Troskie
*  
*   full episode at Crackle
*  
*   full episode at Hulu
*  
*   at The Numbers
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 