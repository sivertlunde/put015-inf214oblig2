Onnam Prathi Olivil
{{Infobox film 
| name           = Onnam Prathi Olivil
| image          =
| caption        = Baby
| producer       = AP Lal
| writer         = Pushparaj Pappanamkodu Lakshmanan (dialogues)
| screenplay     = Pappanamkodu Lakshmanan
| starring       =
| music          = KJ Joy
| cinematography = KB Dayalan
| editing        = G Murali
| studio         = Quality Productions
| distributor    = Quality Productions
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Baby and produced by AP Lal. The film stars  and  in lead roles. The film had musical score by KJ Joy.   

==Cast==

 

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Cheekithirukiya || K. J. Yesudas, KS Chithra || P. Bhaskaran || 
|-
| 2 || Raasalela Lahari || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Thenuthirum || KS Chithra || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 