Horseplay (2014 film)
 
 
{{Infobox film
| name           = Horseplay
| image          = Horseplay.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Official poster
| film name      = {{Film name
| traditional    = 盜馬記
| simplified     = 盗马记
| pinyin         = Dào Mǎ Jì
| jyutping       = Dou6 Maa3 Gei3 }}
| director       = Lee Chi Ngai
| producer       = Lee Chi Ngai Claudia Chung
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       =  Tony Leung Ekin Cheng Kelly Chen Eric Tsang Wong Cho-lam
| music          = 
| cinematography = 
| editing        = 
| studio         = Edko Films Sil-Metropole Organisation Bona Film Group United Filmmakers Organisation
| distributor    = Edko Films
| released       =   
| runtime        =  Hong Kong
| language       = Cantonese
| budget         = 
| gross          = US$565,869 
}}
 2014 Cinema Hong Kong action comedy Tony Leung, Ekin Cheng and Kelly Chen. The films Cantonese and Mandarin theme song, Why Not Tonight (不如今晚) and The Best Night (最好的夜晚) respectively, was adapted from Henry Mancinis It Had Better Be Tonight with Leung, Cheng and Chen performing. Leung and Chen performed the Mandarin version at the 2014 CCTV Lunar New Year Evening on 30 January 2014.  

==Plot== Tony Leung) and washed out police detective Cheung Ho (Ekin Cheng). One wants to reveal the truth, one wants to steal the "Tang Dynasty Pottery Horse" and one wants to bring criminals to justice. The three of them have different goals, but they must work together to acquire the pottery, which also results in the hunt for Nine-Tailed Fox by his rivals. What will be the final outcome?

==Cast==
*Tony Leung Ka-fai as Lee Tan (李旦), known as the Nine-Tailed Fox (九尾狐), a multi-faced thief
*Ekin Cheng as Cheung Ho (張浩), a police officer and Ha Muis boyfriend
*Kelly Chen as Ha Mui (夏梅), a television hostess
*Eric Tsang as Heung Seung (尚環), an antique appraisal expert
*Wong Cho-lam as Tung Yin (董燕), an antique appraisal expert and Heungs junior
*Mandy Lieu as Twin assassin
*Wang Ziyi
*Wang Shuo as Butch, Lees apprentice
*Liu Kai-chi as Officer Wong (黃Sir), Cheungs ex-superior
*Stephanie Che as Eva Lam (林綺華), a television hostess and Has friend
*Luisa Maria Leitão as a television hostess
*Jeannie Chan as Lees daughter

==Production==
Horseplay was filmed on location in Prague and London.    While filming a motorcycle crashing scene, Tony Leung encountered an accident where he fractured three rib bones. Due to his injury, Leung who was scheduled to film his first TVB series Line Walker, had to withdraw from the series.   {{cite web|url=http://www.jaynestars.com/news/michael-miu-and-raymond-lam-fight-corruption-in-the-apostle/|title=
Michael Miu and Raymond Lam Fight Corruption in "The Apostle"}} 

==Theme song==
*Why Not Tonight (不如今晚) ( ) / The Best Night (最好的夜晚) ( )
**Composer: Henry Mancini
**Arranger: Ted Lo
**Lyricist: Abrahim Chan
**Producer: Mark Lui
**Singer: Tony Leung Ka-fai, Ekin Cheng, Kelly Chen

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 