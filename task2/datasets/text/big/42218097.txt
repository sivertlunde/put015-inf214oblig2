Codename Coq Rouge
 
{{Infobox film
| name           = Codename Coq Rouge
| image          = 
| caption        = 
| director       = Per Berglund
| producer       = Peter Hald Hans Iveberg
| writer         = Jan Guillou Per Berglund
| starring       = Stellan Skarsgård
| music          = 
| cinematography = Göran Nilsson (cinematographer)|Göran Nilsson
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}
 Best Actor and Göran Nilsson (cinematographer)|Göran Nilsson won the award for Best Cinematography at the 25th Guldbagge Awards.   

==Cast==
* Stellan Skarsgård as Carl Hamilton
* Lennart Hjulström as Näslund
* Krister Henriksson as Fristedt
* Philip Zandén as Appeltoft
* Bengt Eklund as Den gamle
* Lars Green as Ponti
* Roland Hedlund as Folkesson
* Anette Kischinowsky as Fatumeh
* Nick Burnell as Meyer
* Harald Hamrell as Johansson
* Tjadden Hällström as Ljungdahl (as Lars Tjadden Hällström)
* Tove Granditsky as Shulamit Hanegbi (as Tove Granditsky-Svenson)
* Lena T. Hansson as Eva Hamilton
* Gustaf Skarsgård as Erik Hamilton

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 