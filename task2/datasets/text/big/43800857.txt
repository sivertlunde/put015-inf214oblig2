In the Courtyard
{{Infobox film
| name           = In the Courtyard
| image          = 
| caption        =
| director       = Pierre Salvadori
| producer       = Philippe Martin 
| writer         = Pierre Salvadori David Colombo Léotard 
| starring       = Catherine Deneuve   Gustave Kervern 
| music          = Grégoire Hetzel Stephin Merritt
| cinematography = Gilles Henry 	 
| editing        = Isabelle Devinck 	 France 2 Cinéma Wild Bunch
| released       =  
| runtime        = 97 minutes
| country        = France
| language       = French
| budget         = 7 million
| gross          = $2,670,937 
}}

In the Courtyard ( ) is a 2014 French comedy-drama film written and directed by Pierre Salvadori and starring Catherine Deneuve and Gustave Kervern. The film premiered at the 64th Berlin International Film Festival on 11 February 2014.  Salvadori was awarded a Swann dor for Best Director at the 2014 Cabourg Romantic Film Festival. 

== Cast ==
* Catherine Deneuve as Mathilde
* Gustave Kervern as Antoine 
* Féodor Atkine as Serge
* Pio Marmaï as Stéphane
* Michèle Moretti as Colette
* Nicolas Bouchaud as M. Maillard
* Oleg Kupchik as Lev
* Garance Clavel as Antoines Ex
* Carole Franck as Temp Agency Woman
* Olivier Charasson as Specialist
* Bruno Netter as M. Vigo

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 