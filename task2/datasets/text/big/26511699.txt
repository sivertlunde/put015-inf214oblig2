Punyam Aham
{{Infobox film
| name           = Punyam Aham
| image          = Punyam Aham.jpg
| alt            =  
| caption        = 
| director       = Raj Nair
| producer       =  
| writer         = 
| screenplay     = 
| story          = 
| starring       =  
| music          = Issac Thomas Kottakapalli
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Punyam Aham is a 2010 Malayalam film directed by Raj Nair. Prithviraj Sukumaran|Prithviraj, Samvrutha Sunil and Nedumudi Venu play the lead roles in this film.

== Plot ==
Punyam Aham is set in a small village in northern Kerala. The protagonist is Narayan Unni (Prithviraj Sukumaran), a young man with a Brahmin father and low-caste mother, who separated, leaving the mother to raise him alone. The film tells the story of Unni leaving home and searching for his identity, and then repeating many of his fathers mistakes in life. It is based on the story in traditional folklore about Naranath Bhranthan and his father Vararuchi. 

== Production ==
Punyam Aham was the directorial debut of Raj Nair, the grandson of novelist Thakazhi Shivashankara Pillai.  Filming began in January 2009 at Ottapalam, then shifted to Kuttanad.  It premiered at the Mumbai International Film Festival in October 2010. 

== Critical reception==
Paresh C. Palicha writing for Rediff Movies criticised the plot as "too intellectual". 

== Cast ==
* Prithviraj Sukumaran as Naarayanan Unni
* Samvrutha Sunil as Jayasree
* Nedumudi Venu as Kaarackal Easwaran Namboodiri
* Nishanth Sagar as Georgekutty
* Sona Nair as Elder Sister
* K. P. A. C. Lalitha as Jayasrees Amma
* M. R. Gopakumar as Pappanassar
*Sreejith Ravi as Pankan
*Sreekala VK as Narayanans mother
*Roselin

== References ==
 

== External links ==
*   on OneIndia.in

 
 
 

 