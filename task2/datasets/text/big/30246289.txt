33 Scenes from Life
{{Infobox film
| name           = 33 Scenes From Life
| image          = 33 Scenes from Life film poster.png
| caption        = Film poster
| based on       = 
| screenplay     = 
| starring       = Julia Jentsch Peter Gantzler Maciej Stuhr
| director       = Malgorzata Szumowska
| producer       = 
| cinematography = 
| country        = Poland
| editing        =
| released       =  
| runtime        = 100 minutes
| language       = Polish
| music          = 
| gross          = 
}}
33 Scenes From Life ( ) is a 2008 Polish film directed by Malgorzata Szumowska.

==Plot==
The Polish artist Julia and her husband Piotr, a talented and successful composer, live in Kraków. When Juliass mother, Barbara falls ill with stomach cancer, the life of the family is falling apart. Julia accompanies her mother to death, but her husband Piotr is at rehearsals in Cologne and leaves her to cope with this difficult situation. Only her friend Adrian is at her side. Her father Jurek is also overwhelmed by the impending loss of his beloved wife.  After the death of the mother her father takes comfort from alcohol. Shortly after the father dies. Julia found only in the arms of Adrian to rest, but this in turn destroyed her marriage to Piotr. After the loss of the parents and breakup of the marriage she is now alone in the world with an uncertain future where Adrian is of little help.                             

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Julia Jentsch || Julia
|-
| Maciej Stuhr || Piotr
|-
| Peter Gantzler || Adrian
|-
| Małgorzata Hajewska || Barbara
|-
| Andrzej Hudziak || Jurek
|}

==References==
 

==External links==
*  
*  at  
*  
*  

 
 
 
 