Blondie's Holiday
{{Infobox film
| name = Blondies Holiday
| image_size =
| image	=	
| alt =
| caption =
| director = Abby Berlin
| producer =Burt Kelly
| writer = Constance Lee (screenplay)  Chic Young (characters)
| narrator = Arthur Lake
| music = Mischa Bakaleinikoff Vincent Farrar
| editing = Jerome Thoms
| studio = 
| distributor = Columbia Pictures
| released =    
| runtime = 67 min.
| country = United States
| language = English
| budget =
| gross =
}} 1947 black-and-white comic strip of the same name. The screenplay was written by Chic Young and Constance Lee.

This was the twentieth of 28 films based on the comic strip; Columbia Pictures produced them from 1938 to 1943. Daisy, the dog, appeared in every film except this one, as he was playing "Curley" in the 1947 film Red Stallion 

==Plot==

Dagwood Bumstead is an architect who has managed to convince the prominent bank president Samuel Breckenridge to let his firm have the contract to erect a new bank building in town. When Dagwood’s boss at the architect firm, George Radcliffe, hears about the contract, he is ecstatic and offers Dagwood a modest raise of $2,50.

When Dagwood immediately tell his wife Blondie the good news over the telephone, she mistakes the numbers and believes he has gotten a $250 raise. She tells her friends about the fantastic news, and word gets around that Dagwood has made a fortune on his success.

A class reunion is around the corner and Blondie is in the committee planning the festivities. When the rest of the committee, including bigmouth housewife Cynthia Thompson and Paul Madison, who were Dagwood’s highschool suitor, hears about Dagwood’s fortune, they suggest he pay the bill of $400 for the fancy dinner at the reunion. Blondie has no choice but to accept to defend Dagwood’s honor.

Dagwood panics when he hears what Blondie has promised in his name, and starts a desperate search for money to pay for the dinner he can’t afford.  He sees no other alternative than to try to gamble up the money on the horse race track. He talks to a gambling expert named Pete Brody to learn how to bet, but the bank president hears about his keen interest in gambling and cancels the building contract immediately. Dagwood’s boss Radcliffe gets furious when he hears the contract is cancelled, and fires Dagwood on the spot because of this.

Blondie helps out to raise money by making women’s hats, thus contributing with $200 to the bill, but when the day of the reunion dinner arrives, Dagwood is still short the other $200. He goes to an illegal gambling parlor and starts betting. He gets advice from an old lady, but still manages to bet the $200 from Blondie on the wrong horse. Despite this, he has good fortune and the horse wins, but the place is raided by the police. Dagwood helps the old lady escape unnoticed from the place, but is arrested by the police himself.

The reunion dinner starts, and Blondie is present, but Dagwood is still in custody at the police station. He calls for Radcliffe to come and bail him out, but bank president Breckenridge beats him to it, arriving shortly after Radcliffe to the station. In company with Breckenridge is the old lady from the gambling parlor, who turns out to be Mrs. Breckenridge. Grateful for Dagwoods help to avoid a public scandal and humiliation, Breckenridge renews the contract to erect a new bank building with the firm, and as a condition he demands Dagwood be rehired. Dagwood throws in another condition for his own account - that Radcliffe pay for the reunion dinner as well.

Dagwood gets out of jail and arrives to the reunion dinner in time to avoid any suspicion, and is able to pay for the festivities. 

==Cast==
*Penny Singleton ... Blondie Bumstead Arthur Lake ... Dagwood Bumstead
*Larry Simms ... Alexander Bumstead
*Marjorie Ann Mutchie ... Cookie Bumstead
*Daisy ... Himself
*Jerome Cowan ... George M. Radcliffe Grant Mitchell ... Samuel Breckenridge
*Sid Tomack ... Pete Brody Mary Young ... Mrs. Breckenridge
*Jeff York ... Paul Madison
*Anne Nagel ... Bea Mason
*Jody Gilbert ... Cynthia Thompson
*Jack Rice ... Ollie Shaw

==Blondie film series==

*Blondie (1938 film)|Blondie (1938)
*Blondie Meets the Boss (1939)
*Blondie Takes a Vacation (1939)
*Blondie Brings Up Baby (1939)
*Blondie on a Budget (1940)
*Blondie Has Servant Trouble (1940)
*Blondie Plays Cupid (1940)
*Blondie Goes Latin (1941)
*Blondie in Society (1941)
*Blondie Goes to College (1942)
*Blondies Blessed Event (1942)
*Blondie for Victory (1942)
*Its a Great Life (1943)
*Footlight Glamour (1943)
*Leave It to Blondie (1945)
*Life with Blondie (1945)
*Blondies Lucky Day (1946)
*Blondie Knows Best (1946)
*Blondies Big Moment (1947)
*Blondies Holiday (1947)
*Blondie in the Dough (1947)
*Blondies Anniversary (1947)
*Blondies Reward (1948)
*Blondies Secret (1948)
*Blondies Big Deal (1949)
*Blondie Hits the Jackpot (1949)
*Blondies Hero (1950)
*Beware of Blondie (1950)

==References==
 

==Further reading== Lulu Publishing, September 2005. Page 26. ISBN 978-1411650657.

==External links==
* 
* 
* 

 
 
 
 
 
 
 