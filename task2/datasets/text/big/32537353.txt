Midhya
 
 
{{Infobox film
| name           = Midhya
| image          =
| caption        =
| writer         = M. T. Vasudevan Nair Rupini Suresh Gopi M. G. Soman Sukumari Balan K. Nair Bheeman Raghu
| director       = I. V. Sasi Seema
| released       =  
| runtime        =
| distributor    =
| country        = India
| language       = Malayalam Shyam
| cinematography = Santhosh Sivan
| editing        = K.Narayanan
| gross          =
}}

Midhya is a 1990 Malayalam film. Directed by I. V. Sasi, based on the script by M. T. Vasudevan Nair, this film has  Mammootty, Suresh Gopi, Rupini (actress)|Rupini, Sukumari and M. G. Soman in the leading roles. This film was well received at the box office.   

==Plot==
Venugopal (Mammootty), a rich young business man still considers Shivan as his mentor, who had helped him in his early days in Mumbai.At the time of his death, Shivan has asked Venu to take care of his younger brother Rajashekharan (Suresh Gopi), which Venu obeyed whole heartedly. Rajan was appointed as his manager and is treated more like his brother. On a visit to Rajans house along with him, Venu meets Devi (Rupini (actress)|Rupini). Venu expresses Rajans sister about his wish to marry Devi, but changes his mind after realizing that she is already engaged to Rajan. Venu whole heartedly gets Rajan married to Devi. Rajan, in thirst to earn quick money, gets into bad company and severs ties with Venu. He becomes suspicious of his wife having an illicit relation with Venu. Slowly turning into too much of drinking and gambling, Rajan falls into huge debts and to make things worse, he also becomes a part of smuggling. Venus efforts to save Rajan from his problems and bringing him back to normal life is the rest of the story.

==Cast==
*Mammootty as Venugopal
*Suresh Gopi as K. P. Rajagopal Rupini as Devi
*Jagannatha Varma as Krishna Kurup
*Balan K. Nair as Narayanan
*M.G. Soman as Appunni
*K.P. Ummer as Nambyar
*Bheeman Raghu as Varghese
*Kuthiravattam Pappu as Ezhuthachan
*Thikkurissy Sukumaran Nair as Muthachan
*Kaviyoor Ponnamma as Rajagopals Mother
*Sukumari as Ammalu Sonia

==References==
 

==External links==
* 

 
 
 
 

 