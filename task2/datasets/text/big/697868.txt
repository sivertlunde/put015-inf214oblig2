Dead Calm (film)
 
 
{{Infobox film
| name = Dead Calm
| image          = Deadcalmposter.jpg
| caption        = Dead Calm poster
| director       = Phillip Noyce George Miller Doug Mitchell
| screenplay     = Terry Hayes
| based on     =  
| starring       = Sam Neill Nicole Kidman Billy Zane
| music          = Graeme Revell
| cinematography = Dean Semler
| editing        = Richard Francis-Bruce Kennedy Miller
| distributor    = Warner Bros.
| released       =   (U.S. release)
| runtime        = 96 minutes
| country        = Australia
| language       = English
| budget = $10.4 million 
| gross          = $7,825,000   Box Office Mojo. Retrieved 10 November 2011. 
}}
  novel of Charles Williams. The film was directed by Australian filmmaker Phillip Noyce and filmed around the Great Barrier Reef. It was the first film for which Graeme Revell composed the score.

==Plot summary==
Rae Ingraham (Nicole Kidman|Kidman), is involved in a car crash which resulted in the death of her son.  Her husband, an Australian Royal Navy officer John Ingram (Sam Neill|Neill) suggests that they help deal with their grief by heading out for a vacation alone on their yacht. In the middle of the Pacific, they encounter a drifting boat that seems to be taking on water. A man, Hughie Warriner (Billy Zane|Zane), rows over to the Ingrams boat for help. He claims that the boat is sinking and that his companions have all died of food poisoning.

Suspicious of Hughies story, John rows over to the other ship, leaving Rae alone with Hughie. Inside, John discovers the mangled corpses of the other passengers and video footage indicating that Hughie may have murdered them in a feat of extraordinary violence. John rushes back to his own boat, but hes too late as Hughie awakes, knocks out Rae and sails their yacht away, leaving John behind.

As John attempts to keep Hughies ship from sinking and catch up with them, Rae awakens and tries to convince Hughie to go back for her husband. Hughie denies her request and keeps on sailing, alternating between kindness and bouts of rage. John manages to get through to his wife on the radio, but the water damage makes him unable to reply save for clicks on his receiver. He can only respond to her suggestions. John assures her that he is following close by. Rae tries to stall the yacht by turning off the engine and tossing the keys overboard. Her dog jumps in to retrieve the keys and brings them back as he had done earlier with his fetch ball. Hughie starts the yacht back up and tries to convince Rae to be friends with him. Rae accepts, attempting to earn his trust. After a while, she goes back to the Radar Room to contact John. A blip appears on the rim of radars range, signifying the damaged boat. She soon learns that it is too far gone and will sink in the next several hours. With John unable to come to her rescue, Rae realizes that she alone must act and save her husband. 

Rae seduces Hughie, attempting to gain his trust.  She tries to get to the ships shotgun, but cannot get away from Hughie long enough.  The two eventually have sex.  Afterwards, she sits on her bed feeling guilty and devises another plan to bring him down. She fixes some lemonade, putting a heavy dose of her prescription sedatives into his drink. Claiming to go get dressed, Rae heads back for the shotgun, and is discovered soon after. As a fierce storm approaches, Rae and Hughie come to blows. Hughie takes hold of the shotgun, but the effects of the sedative cause him to poorly aim and shoot the radio by mistake. Rae eventually takes hold of a harpoon gun and locks herself in the bedroom. As the door opens she fires off a harpoon. Seeing blood she pushes it open only to discover she killed her dog. Hughie comes out of hiding to strangle her, but passes out from the drugs. Rae ties him up and sails back to rescue John. Hughie comes to and cuts himself free with a shard of broken mirror, but after making his way to Rae, she shoots him in the shoulder with a harpoon and knocks him unconscious. She then sets him adrift in the boats life raft and continues to look for her husband.

Meanwhile, the damage and the storm have caused the other boat to sink almost completely. The storm comes down in full and breaks the boats main mast as John is trapped below deck. The water rises and eventually he is submerged over his head, only able to breathe through a piece of pipe leading to the deck. The only way he can go is down into the boats hull, in search of an opening. He takes one last breath from the pipe and dives.

Through the gaping hole in the boats bottom, John emerges back on the surface. He sets the wreck on fire to signal his location to Rae, who is now desperate to find him. Dusk sets in as Rae notices and sets course to the faint fire on the horizon. Without any means to signal his wife, all John can do is wait on a piece of floating debris. After night falls, the pair reunite when Rae arrives and pulls John aboard.

Later they find the life raft and Rae shoots it with a flare, setting it on fire. The next day they are relaxing on deck when John takes a break from washing Raes hair to prepare breakfast for her. Her eyes closed, Rae feels a pair of hands begin massaging her scalp and assumes it is John, but when she opens her eyes she sees a bloody Hughie, who begins to strangle her. While Rae struggles, John arrives from below deck. Seeing her being attacked, he shoots Hughie in the mouth with a flare, killing him.

==Main cast==
*Sam Neill as Capt. John Ingram, R.A.N.
*Nicole Kidman as Rae Ingram
*Billy Zane as Hughie Warriner

==Production== Charles Williams, which Orson Welles had started filming in the late 1960s but never completed. Producer Tony Bill had tried to buy the rights from Welles but never been successful. He mentioned this to Phil Noyce, giving him a copy of the book in 1984. Noyce enjoyed the book and showed it to George Miller and Terry Hayes who were enthusiastic. Miller managed to persuade Oja Kodar, Welles companion who controlled the rights to the novel, to sell the book to Kennedy Miller. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p263-265  Brian McFarlane, "Phil Noyce: Dead Calm", Cinema Papers, May 1989 p6-11 
 asexual manchild.  

The movie was filmed over a 14-week span in the Whitsunday Islands in the winter of 1987. George Miller directed some sequences himself, including one where Sam Neills character is tormented in the boat by a shark. This scene ended up being dropped from the final film. The final coda sequence was filmed at the request of Warner Bros seven months after principal photography finished. 

==Reception==
Dead Calm has an 82% "fresh" rating at   of the   and scoring by Graeme Revell, he finds lurking dangers in quiet, peaceful waters." Howe, Desson.   Washington Post (7 April 1989) 

On the other hand, Caryn James of The New York Times felt that the film was "an unsettling hybrid of escapist suspense and the kind of pure trash that depends on dead babies and murdered dogs for effect," and that Dead Calm "becomes disturbing for all the wrong reasons." James, Caryn.   New York Times (7 April 1989).  A number of critics faulted the films ending as being over-the-top, with the Posts Howe writing, "... while its afloat, Dead Calm is a majestic horror cruise. ... For much of the movie, youre enthralled. By the end, youre laughing." 
 Amazon for the 90s,"  the Times James called Kidmans character "tough but stupid." 

The film is listed on The New York Times Top 1000 Movies list,  derived from editor Peter M. Nichols The New York Times Guide to the Best 1,000 Movies Ever Made (St. Martins Griffin, 2004).

==Box office==
Dead Calm grossed $2,444,407 at the box office in Australia,  which is equivalent to $4,253,268 in 2009 dollars. It grossed $7,825,009 in the U.S. 

==See also==
* The Deep (unfinished film)|The Deep (unfinished film)
* Cinema of Australia

==References==
 

==External links==
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 