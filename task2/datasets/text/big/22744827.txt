Akō Rōshi (1961 film)
{{Infobox film
| name           = Akō Rōshi
| image          =  
| caption        = Japanese movie poster
| director       = Sadatsugu Matsuda
| producer       = Toei Company|Toei, Shigeru Okada (producer) Hiroshi Okawa (executive producer) Kaneto Shindō (writer)
| starring       = 
| music          = 
| cinematography = Shintarō Kawasaki
| editing        = 
| distributor    =  1961 (Japan) 
| runtime        = 150 minutes
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| based on       = Novel by Jirō Osaragi
| 
}} Japanese film 47 Ronin directed by Sadatsugu Matsuda. It earned ¥435 million at the annual box office, making it the second highest-grossing film of 1961. 

== Cast ==
* Kusuo Abe as Katada
* Kyōko Aoyama as Nagi
* Chiyonosuke Azuma as Horibe
* Shinobu Chihara as Ukibashi Dayu
* Yoshiko Fujita as Ayame
* Hiromi Hanazono as Sakura
* Yumiko Hasegawa as Chiyo
* Utaemon Ichikawa as Hizaka
* Chiezō Kataoka as Oishi (Ōishi Yoshio)
* Michiyo Kogure as Oyone
* Jūshirō Konoe as Ikkaku Shimizu
* Hiroki Matsukata as Oishi
* Katsuo Nakamura as Denkichi
* Kinnosuke Nakamura as Wakisaka
* Satomi Oka as Sen
* Hashizo Okawa as Lord Asano (daimyo Asano Naganori)
* Keiko Okawa as Kita no kata
* Denjirō Ōkōchi as Sakon Tachibana
* Ryūtarō Ōtomo as Hotta
* Shunji Sakai as Matsuzo
* Hiroko Sakuramachi as Osaki
* Kōtarō Satomi as Uesugi
* Eitarō Shindō as Denpachiro
* Shin Tokudaiji as Katada
* Ryūnosuke Tsukigata as Lord Kira (Kozunosuke Kira)
* Kenji Usuda as Yahei
* Isao Yamagata as Kataoka
* Eijirō Yanagi as Yanagisawa

== See also ==
* Forty-seven Ronin

other films about Forty-seven ronin: The 47 Ronin (元禄忠臣蔵, Genroku chushingura) - 1941 film by Kenji Mizoguchi
*   (赤穂浪士　天の巻　地の巻) - 1956 film by Sadatsugu Matsuda The 47 Ronin (忠臣蔵 Chushingura) - 1958 film by Kunio Watanabe
*   - 1962 film by Hiroshi Inagaki
* Nagadosu chūshingura, 1962 action film by Kunio Watanabe
* Daichūshingura (大忠臣蔵, Daichūshingura) - 1971 television dramatization
* The Fall of Ako Castle (赤穂城断絶, Akō-jō danzetsu) (aka Swords Of Vengeance) - 1978 film by Kinji Fukasaku

== References ==
 

== External links ==
* http://www.imdb.com/title/tt0200418/

 

 
 
 
 
 
 
 


 