Rope (film)
{{Infobox film
| name = Rope
| image = Rope2.jpg
| image_size =
| caption = Original theatrical poster
| director = Alfred Hitchcock Sidney Bernstein (uncredited)
| writer =   Screenplay: Arthur Laurents Ben Hecht (uncredited)
| starring = James Stewart John Dall Farley Granger Joan Chandler Sir Cedric Hardwicke Constance Collier Douglas Dick Edith Evanson
| music = Musical direction:   Francis Poulenc
| cinematography = Joseph A. Valentine William V. Skall
| editing = William H. Ziegler
| studio = Transatlantic Pictures
| distributor = Warner Bros. Pictures Metro-Goldwyn-Mayer
| released =  
| runtime =  
| country = United States
| language = English
| budget = US$1,500,000 
| gross =
| genre = cinema verite
}} psychological crime 1929 play Patrick Hamilton and adapted by Hume Cronyn Rope Unleashed&nbsp;– Making Of (2000)&nbsp;– documentary on the Universal Studios DVD of the film.  and Arthur Laurents. 
 Sidney Bernstein real time continuous shot through the use of long takes.
 Nathan Leopold and Richard Loeb.

== Plot ==
Two brilliant young aesthetes, Brandon Shaw (Dall) and Phillip Morgan (Granger), strangle to death a former classmate, David Kentley (Dick Hogan), in their apartment. They commit the crime as an intellectual exercise; they want to prove their superiority by committing the "perfect murder".

After hiding the body in a large antique wooden chest, Brandon and Phillip host a dinner party at the apartment, which has a panoramic view of Manhattans skyline. The guests, who are unaware of what has happened, include the victim (David)s father Mr. Kentley (Cedric Hardwicke) and aunt Mrs. Atwater (Constance Collier); his mother is not able to attend. Also there are his fiancée, Janet Walker (Joan Chandler) and her former lover Kenneth Lawrence (Douglas Dick), who was once Davids close friend.

  in the films trailer]]
In a subtle move, Brandon uses the chest containing the body as a buffet table for the food, just before their housekeeper, Mrs. Wilson (Edith Evanson) arrives to help with the party. "Now the fun begins," Brandon says when the first guests arrive.
 De Quinceys art of murder, as a means of showing ones superiority over others. He too is among the guests at the party, since Brandon in particular feels that he would approve of their "work of art".

Brandons subtle hints about Davids absence indirectly lead to a discussion on the "art of murder". Brandon appears calm and in control, although when he first speaks to Rupert he is nervously excited and stammering. Phillip, on the other hand, is visibly upset and morose. He does not conceal it well and starts to drink too much. When Davids aunt, Mrs. Atwater, who fancies herself as a fortune-teller, tells him that his hands will bring him great fame, she is referring to his skill at the piano, but he appears to think this refers to the notoriety of being a strangler.

Much of the conversation, however, focuses on David and his strange absence, which worries the guests. A suspicious Rupert quizzes a fidgety Phillip about this and about some of the inconsistencies that have been raised in conversation. For example, Phillip had vehemently denied ever strangling a chicken at the Shaws farm, but Rupert has personally seen Phillip strangle several. Phillip later complains to Brandon about having had a "rotten evening", not because of Davids murder, but over Ruperts questioning.

As the evening goes on, Davids father and fiancée begin to worry that he has neither arrived nor phoned. Brandon increases the tension by playing matchmaker between Janet and Kenneth. Mrs. Kentley calls, overwrought because she has not heard from David, and Mr. Kentley decides to leave. He takes with him some books Brandon has given him, tied together with the rope Brandon and Phillip used to strangle his son.

When Rupert goes to leave, Mrs. Wilson accidentally hands him Davids monogrammed hat, further arousing his suspicion. Rupert returns to the apartment a short while after everyone else has departed, pretending that he has left his cigarette case behind. He hides the case, asks for a drink and then stays to theorize about the disappearance of David.  He is encouraged by Brandon, who seems eager to have Rupert discover the crime. A drunk Phillip is unable to take it any more; he throws a glass and says, "Cat and mouse, cat and mouse. But which is the cat and which is the mouse?"

Rupert lifts the lid of the chest and finds the body inside. He is horrified but also deeply ashamed, realizing that Brandon and Phillip used his own rhetoric to rationalize murder. Rupert seizes Brandons gun and fires several shots into the night in order to attract attention. The film segues to the end titles with the sound of approaching police sirens.

== Cast ==
  James Stewart as Rupert Cadell, prep-school housemaster and publisher John Dall as Brandon Shaw, co-murderer and co-host at the party Farley Granger as Phillip Morgan, co-murderer and co-host of the party Joan Chandler as Janet Walker, columnist and Davids fiancée
 
  Sir Cedric Hardwicke as Mr. Henry Kentley, Davids father Constance Collier as Mrs. Anita Atwater, Davids aunt Douglas Dick as Kenneth Lawrence, Davids friend Edith Evanson as Mrs. Wilson, Brandons housekeeper
 

== Production ==
The film is one of Hitchcocks most experimental and "one of the most interesting experiments ever attempted by a major director working with big box-office names",    abandoning many standard film techniques to allow for the long unbroken scenes. Each shot ran continuously for up to ten minutes without interruption. It was shot on a single set, aside from the opening establishing shot street scene under the credits. Camera moves were carefully planned and there was almost no editing.

The walls of the set were on rollers and could silently be moved out of the way to make way for the camera and then replaced when they were to come back into shot. Prop men constantly had to move the furniture and other props out of the way of the large Technicolor camera, and then ensure they were replaced in the correct location. A team of soundmen and camera operators kept the camera and microphones in constant motion, as the actors kept to a carefully choreographed set of cues.   
 cyclorama in Empire State Chrysler buildings. spun glass—change position and shape eight times. 

=== Long takes ===
Hitchcock shot for periods lasting up to 10 minutes (the length of a film camera magazine), continuously panning from actor to actor, though most shots in the film wound up being shorter.   |page=272  Every other segment ends by panning against or tracking into an object—a mans jacket blocking the entire screen, or the back of a piece of furniture, for example. In this way, Hitchcock effectively masked half the cuts in the film.

However, at the end of 20 minutes (two magazines of film make one reel of film on the projector in the movie theater), the projectionist—when the film was shown in theaters—had to change reels. On these changeovers, Hitchcock cuts to a new camera setup, deliberately not disguising the cut. A description of the beginning and end of each segment follows.

 

{| class="wikitable" Finish
|- Blackout on Brandons back
|- CU Kenneth: "What do you mean?"
|- Unmasked cut, Blackout on Kenneths back
|- CU Phillip: "Thats a lie."
|- Unmasked cut, Blackout on Brandons back
|- Three shot
|- Unmasked cut, Blackout on Brandon
|- CU Brandons hand in gun pocket
|- Unmasked cut, Blackout on lid of chest
|- End of film
|}

Hitchcock told François Truffaut in the book-length Hitchcock/Truffaut (Simon & Schuster, 1967) that he ended up re-shooting the last four or five segments because he was dissatisfied with the color of the sunset.
 Stage Fright (1950).

=== Directors cameo ===
  appearance as a red neon sign, in the far distance, with his famous profile above the word "Reduco", a fictitious weight-loss product]] Alfred Hitchcocks cameo is a signature occurrence in most of his films. In this film, Hitchcock is considered by some to make two appearances,  according to Arthur Laurents in the documentary Rope Unleashed, available on the DVD. Laurents says that Hitchcock is a man walking down a Manhattan street in the opening scene, immediately after the title sequence.
 weight loss product used in his Lifeboat (film)|Lifeboat (1944) cameo, starts blinking; as the guests are escorted to the door actors Joan Chandler and Douglas Dick stop to have a few words, the sign appears and disappears in the background several times, right between their visages, right under the eyes of the spectators.

== Homosexual subtext ==
Rope included a homosexual subtext between the characters Brandon and Phillip, even though homosexuality was a highly controversial theme for the 1940s. The movie made it past the Production Code censors, however; during the films production, those involved described homosexuality as "it".  However, many towns chose to ban the film independently, memories of Leopold and Loeb still being fresh in some peoples minds. Dall and Granger were actually homosexual in real life, as was screenwriter Arthur Laurents; even the piano score played by Granger (Mouvement Perpétuel No. 1) was the work of a homosexual composer, Francis Poulenc.

More recent reviews and criticism of the film explicitly note its homoerotic subtext.  
 

== Reception ==
In 1948, Variety (magazine)|Variety magazine said "Hitchcock could have chosen a more entertaining subject with which to use the arresting camera and staging technique displayed in Rope".  That same year, Bosley Crowther of The New York Times said the "novelty of the picture is not in the drama itself, it being a plainly deliberate and rather thin exercise in suspense, but merely in the method which Mr. Hitchcock has used to stretch the intended tension for the length of the little stunt" for a "story of meager range".   Nearly 36 years later, Vincent Canby, also of The New York Times, called the "seldom seen" and "underrated" film "full of the kind of self-conscious epigrams and breezy ripostes that once defined wit and decadence in the Broadway theater"; its a film "less concerned with the characters and their moral dilemmas than with how they look, sound and move, and with the overall spectacle of how a perfect crime goes wrong".   

In the Time (magazine)|Time magazine 1948 review, the play that the film was based on is called an "intelligent and hideously exciting melodrama" though "in turning it into a movie for mass distribution, much of the edge   blunted": 

 

Roger Ebert wrote in 1984, "Alfred Hitchcock called Rope an experiment that didn’t work out, and he was happy to see it kept out of release for most of three decades", but went on to say that "Rope remains one of the most interesting experiments ever attempted by a major director working with big box-office names, and its worth seeing  ". 

A 2001 BBC review of that years DVD release called the film "technically and socially bold" and pointed out that given "how primitive the Technicolor process was back then", the DVDs image quality is "by those standards quite astonishing"; the releases "2.0 mono mix" was clear and reasonably strong, though "distortion creeps into the music". 

In his article "Remembering When", Antonio Damasio argues that the time frame covered by the movie, which lasts 80 minutes and is supposed to be in "real time", is actually longer—a little more than 100 minutes.  This, he states, is accomplished by speeding up the action: the formal dinner lasts only 20 minutes, the sun sets too quickly and so on.

== See also ==
* Psychoville, a 2009 BBC2 comedy series. Episode four of the series is a deliberate two-take pastiche of Rope.
* R.S.V.P. (film)|R.S.V.P., a 2002 film which borrowed several key elements from Rope, and in which the film is discussed.
* Swoon (film)|Swoon, an independent 1992 film by Tom Kalin depicting the actual Leopold and Loeb events. Rake Season 2, episode 3, Woolridge and Anor the film is seen as an influence for a murder by two school girls.

== Notes ==
 

==References==
* Neimi, Robert (2006). History in the Media: Film and Television. ABC-CLIO. ISBN 1-57607-952-X.

== Further reading ==
* Wollen, Peter. Rope: Three Hypotheses. Alfred Hitchcock Centenary Essays.
* Miller, D.A.Anal Rope. Representations 32 (Aut. 1990): 114-133.

== External links ==
 
 
*  
*  
*  
*  
*  
*   at Eyegate Gallery

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 