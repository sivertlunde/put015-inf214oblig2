Aadhityan
 
{{Infobox film
| name           = Aadhityan
| image          = 
| image_size     =
| caption        = 
| director       = V. L. Bashkaraaj
| producer       = G. R. Ethiraj R. Achuthananthan Easwari Ulaganathan P. Sujatha E. Rajan C.Kumaran
| writer         = V. L. Bashkaraaj
| starring       =  
| music          = Gangai Amaran
| cinematography = Devchand Ren
| editing        = Ganesh&nbsp;— Kumar
| distributor    =
| studio         = Vellore Film Associates
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 1993 Tamil Tamil drama Sukanya in lead roles. The film had musical score by Gangai Amaran and was released on 14 January 1993.  

==Plot==
 thaali around Rasathis neck. Now, Rasathi is married to Aadhityan but she hates him.

==Cast==

*R. Sarathkumar as Aadhityan Sukanya as Rasathi
*Pandiarajan as Chinna Pandi
*Silk Smitha as Manga Anju
*Kitty Kitty as Zamindar
*Delhi Ganesh as Vedachellam
*Shanmugasundaram as Pannayar
*Chinni Jayanth
*Kumarimuthu as Nallakannu
*Haja Sheriff as Vellai
*MLA Thangaraj as Pottu Gounder
*M. N. Rajam Sulakshana
*Kamala Kamesh
*Kokila as Kannamma
*Thadchayini
*Vinod as Vinod
*R. E. Rajan
*Meghu&nbsp;— Loghu

==Soundtrack==

{{Infobox album |  
| Name        = Aadhityan
| Type        = soundtrack
| Artist      = Gangai Amaran
| Cover       = 
| Released    = 1993
| Recorded    = 1992 Feature film soundtrack |
| Length      = 19:54
| Label       = 
| Producer    = Gangai Amaran
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Gangai Amaran. The soundtrack, released in 1993, features 5 tracks with lyrics written by Gangai Amaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Gongura ... Nennu Andhra || Mano (singer)|Mano, K. S. Chithra || 4:28
|- 2 || Kanna Kadhal Kannukku || Swarnalatha || 3:43
|- 3 || Kattikko Koora Pattu Selai || K. S. Chithra, Chorus || 4:30
|- 4 || Kottattum Mela Sattam || Malaysia Vasudevan, Swarnalatha   || 4:26
|- 5 || Uchi Malai || Gangai Amaran || 2:47
|}

==Reception==

K. Vijiyan of New Straits Times said : "if more care had be taken with the acting, it would have been remarkable". {{cite journal|author=
K. Vijiyan|title=Poor acting spoils potential of movie|newspaper=New Straits Times|date=1993-02-05|accessdate=2014-02-23|page=20|url=http://news.google.com/newspapers?id=vCJOAAAAIBAJ&sjid=RhQEAAAAIBAJ&hl=fr&pg=1822%2C1777256}} 

==References==
 

 
 
 
 