Assignment K
{{Infobox film
| name           = Assignment K
| image size     = 
| image	         = Assignment K FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Val Guest
| producer       = Maurice Foster  Ben Arbeid
| based on       =  
| writer         = Maurice Foster Val Guest Bill Strutton
| narrator       = 
| starring       = Stephen Boyd Camilla Sparv Michael Redgrave Leo McKern
| music          = Basil Kirchin
| cinematography = Ken Hodges
| editing        = Jack Slade
| studio         = Gildor Productions
| distributor    = Columbia Pictures
| released       = June 1968 
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 thriller film directed by Val Guest in Techniscope, and starring Stephen Boyd, Camilla Sparv, Michael Redgrave and Leo McKern.  The film was based on the 1964 novel Department K by Hartley Howard.

==Plot==
A British spy has his cover blown, leading to the East German Stasi kidnapping his girlfriend to try to extract information about his double agents activities. 

==Cast==
* Stephen Boyd as Philip Scott
* Camilla Sparv as Toni Peters
* Michael Redgrave as Harris
* Leo McKern as Smith
* Jeremy Kemp as Hal
* Robert Hoffmann as Paul Spiegler
* Jane Merrow as Martine
* Carl Möhner as Inspector
* Vivi Bach as Erika Herschel
* Werner Peters as Kramer
* Dieter Geissler as Kurt
* John Alderton as George
* Jan Werich as Dr. Spiegler David Healy as David
* Ursula Howells as Estelle
* Basil Dignam as Howlett
* Geoffrey Bayldon as The Boffin
* Catherine Schell as Maggi

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 