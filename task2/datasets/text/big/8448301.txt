A Family Thing
 
{{Infobox Film | name = A Family Thing
  | image = Familythingposter.jpg
  | caption =  Richard Pearce Brad Wilson
  | writer = Billy Bob Thornton Tom Epperson
  | starring = Robert Duvall James Earl Jones Irma P. Hall Michael Beach
  | music = Fred Murphy Mark Warner
  | distributor = United Artists
  | released = March 29, 1996 (United States|USA)
  | runtime = 109 minutes
  | country = United States
  | language = English
  | budget = 
  }} Richard Pearce.

==Plot== Southerner (Duvall) named Earl Pilcher, whose late mother makes a shocking revelation in a letter that is given to him after her death. She reveals that Earls biological mother was a Black American maid named Willa Mae, who was raped by Earls (white) father and that she died while giving birth to Earl. His adoptive mothers dying wish is that he go to Chicago to meet his half-brother, Raymond Murdoch (Jones). Ray blames Earls father for his own mothers death and does not want to speak to Earl, whose very presence reminds Ray of the past. But his loving Aunt T. (Hall) welcomes Earl and insists the family accept him.

Earl initially takes the unexpected news of his mixed parentage badly, tearfully challenging his father to confirm the facts in the letter. As a result, he packs up his clothes and takes off for Chicago to find his brother.  He meets Ray at city hall (where Ray works as a police officer) and Ray, although he really wants nothing to do with Earl, agrees to meet him for lunch at a diner.  There Ray reveals that he knew all along that he had a half white brother and that he hates Earls father (and Earl too by association) because he feels that he is what killed his mother.  He says in so many words that he doesnt want or need a brother, and they go their separate ways.  But when Earl leaves and drives off in his truck he encounters four black Chicago street toughs who rear end his truck.  When Earl gets out to survey the (minimal) damage, he, being a trusting Southern "good ol boy", leaves his keys in the ignition. The toughs beat him up and steal his truck and his wallet.  He walks around in a daze and ends up in a hospital. The hospital staff finds Rays information in Earls pocket and calls Ray.  He comes reluctantly, and the doctor tells him that Earl may have a concussion and needs to take it easy for a couple of days; no traveling is allowed. She also tells him that the hospital is full, so he will have to take Earl home to recuperate.  

At Rays home, Earl meets Aunt T (Hall), a kind and generous elderly woman who is blind. Aunt T. is Willa Maes sister, and thus, Earls aunt. Earl also meets Rays son, Virgil (Beach), a city bus driver who doesnt appreciate a white southerner sleeping in his bed. At first, Earls stay at the Murdoch residence is rocky.  Ray explains that Earl is an old war buddy whose life he saved.  During a shopping excursion with Earl, Aunt T reveals that she knows who Earl really is.  In a powerful scene, Aunt T scolds Ray and Virgil for not welcoming a member of their family, no matter how different he is. Earl overhears the discussion and leaves Rays house, walking unknowingly into a bad part of town.

Ray gives in to Aunt Ts request that he welcome Earl into their house and he quickly locates him on a nearby street. Earl obstinately refuses to come back with Ray, knowing he is not wanted. The two argue and Earl uses the word "nigger" to punctuate his disdain for Ray, seeing too late that he has gone too far. Angry at Earls callous words, Ray tells Earl to stay away from him, and he heads back home.

Meanwhile, Earl wanders Chicago and gets drunk at a Chicago bar, where he is tossed out for bothering a black family. He ends up sleeping under a bridge. The next day, Ray has cooled down and, again on Aunt T.s wishes, manages to find Earl, who apologizes for his words and rude behavior. The two begin to settle their differences. 

As Virgils estranged wife (played by Regina Taylor), and their two daughters, visit, Earl learns that Virgil had a promising career in football that was shattered by an injury in college. Virgil cannot cope with the missed opportunities caused by his injury; and, the resulting bitterness has hurt his relationship with his family.

Ray and Earl bond together more as they find similarities between them. Both served in the military (Earl as a firefighter in the United States Navy, and Ray in the U.S. Marines) during the Korean War, where they received lifelong scars. Ray reveals he once threw a rock at Earl that could have killed him when they were both very young, because of Rays hatred towards Earls father. Later on, in a bar, Earl takes Virgil aside and explains to him that by dwelling on the loss of a his football career, he isnt devoting himself to his wife and children in the way he should. Both of them begin to have a grudging respect for the other.

Once Earl is ready to go home, and the police unexpectedly find his truck operational (it was shot up in a bank robbery), Aunt T. sits Earl and Ray down to tell them the dramatic tale of the night Earl was born and Willa Mae died. According to Aunt T., Willa Mae knew she was likely to die and Earls life was saved only by the quick action of his adoptive mother, Carrie, who brought a white doctor to the shack where Willa Mae and Ray lived to help with the delivery. Aunt T speculates that Carrie and Willa Mae agreed that Earl, who was born with white-appearing features, should be raised by Carrie and his biological father. Aunt T. gives Earl a picture of Willa Mae which he keeps near.  Earl begins to accept his new family with pride, and he convinces Ray to return to their Arkansas hometown to find their mothers grave. As they share a drink on her tombstone, Earl decides to take Ray to meet his southern family and tell them the unlikely story, ending the movie by joking with Ray that when Earls white nephew finds out he is part black, he will likely shoot the both of them.

==Cast==
*Robert Duvall as Earl Pilcher, Jr.
*James Earl Jones as Ray Murdoch
*Irma P. Hall as Aunt T.
*Michael Beach as Virgil Murdoch

==Reception==

The movie received a positive reception.     It holds a 73% "Fresh" rating at Rotten Tomatoes, based on 22 reviews.  

==Box Office==

The movie debuted at No.6. 

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 