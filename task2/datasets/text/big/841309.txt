Belly of the Beast
 
 
{{Infobox film
| name = Belly of the Beast
| image = Belly of the Beast.jpg
| image_size = 215px
| caption = DVD cover Ching Siu Tung
| producer = Jamie Brown Randall Emmett George Furla Gary Howsam Steven Seagal Charles Wang
| writer = Thomas Fenton (uncredited) James Townsend
| story = Steven Seagal (uncredited)
| starring = Steven Seagal Byron Mann Monica Lo Tom Wu
| music = Mark Sayer-Wade
| cinematography = Danny Nowak
| editing = David Richardson
| studio = GFT Entertainment Salon Films Studio Eight Productions Emmett/Furla/Oasis Films|Emmett/Furla Films Columbia TriStar Home Entertainment
| released =  
| runtime = 91 minutes
| country = United States
| language = English Thai
| budget = $14 million
}}
 Ching Siu CIA agent on a quest and to find his kidnapped daughter.

==Plot==
 
Jake Hopper (Steven Seagal) is a successful businessman and retired CIA agent who runs a successful private security business. Ten years ago, as a CIA agent stationed in Thailand, things were going the right way until a fight with thugs results his partner, Sunti (Byron Mann) escaping with his life after accidentally killing a woman. However, Jake called it quits and returned to the United States when his wife died, and Sunti became a Buddhist monk to atone for his sins. Jake has since been a devoted father to his daughter Jessica (Sara Malakul Lane) while running a successful private security business, who is now an adult.

While hiking in Thailand, Jessica and her friend Sarah Winthorpe (Eilidh MacQueen), the daughter of U.S. Senator John Winthorpe are kidnapped. A group of Islamic fundamentalists known as the Abu Karaf claims responsibility. The Abu Karaf demand the release of 20 prisoners from American custody. Tom Collins (Martin McDougall), an ex-colleague of Jakes, recognizes Jessica on the ransom tape, and he tips Jake off. Knowing that he must rescue the girls himself, a former CIA colleague puts Jake in contact with Leon Washington (Patrick Robinson), an active CIA agent who is working in Thailand. Jake goes to Bangkok, and escapes an assassination attempt by gangsters and unknown forces.

Meanwhile, Leon arranges a meeting for himself with Soku, the internal security chief for General Jantapan (Tom Wu), a rebel military general who is making a play to be one of the most powerful men in Thailand. Secretly, Jantapan is messing with some very dangerous spiritual forces. Soku provides Jake with a cover story, but the CIA wants Jake out of it because theyre planning to take out the Abu Karaf with the aid of the Thai army, and they dont want a civilian in the middle. Jake is a spiritual man, so he contacts his spiritual master, Buddhist monk Paijan Paitoon. As Jake is in trouble, Paitoon offers to arrange a divination from the oracle of the order. He enlists the help of Sunti. Jake gets Lulu (Monica Lo), the girlfriend of arms dealer Fitch McQuoid (Vincent Riotta), to steal information leading to the Abu Karaf.

Jake and Sunti follow the leads to a warehouse where they discover evidence of highly sophisticated weaponry. With their enemies now after Lulu, Jake takes Lulu under his wing. He then shares some of his info with Leon still testing the waters. Another attempt is made on Jakes life and this time, Jakes sure that Leon was involved.

Finally, the Abu Karaf contact Jake to arrange a meeting to see the pieces are coming together. Jake figures out that it was not the Abu Karaf who kidnapped Jessica and Sarah. Jake gets his reading from the old oracle, and the cryptic message confirms that his fears, demonic spiritual forces, are working against him. Jantapan later goes to an evil temple and tries to send the spirit of an ancient warrior demon to kill Jake, but the ceremony goes wrong and the spirit enters Jantapan himself, giving him evil physical and spiritual powers, disguised as feats.
 terrorist attacks of 2001, Jantapan has worked to corner the narcotics and arms markets. He also adds that Jantapan kidnapped the girls and blamed the Abu Karaf so the army would wipe out Jantapans competition. Mongkol, knowing where the girls are, gives Jake plans and intelligence and they both need the girls alive. Jake must engage in a rescue effort that will put him to the ultimate test as he takes on Jantapan in a battle in which death may be the only ending.

Later that night, Jake and Sunti plan to rescue the girls who are locked in a cell in Jantapans mansion. After killing two gang members guarding the cell and freeing the girls, a horde of corrupt Thai policemen intervene and make a deadly shootout but end up dead through the firearms of the two. Meanwhile, Sunti kills the rest of the cops while Jake battles with Jantapan in the upstairs living room. Jake kills Jantapan by disarming him and inflicts several injuries in his body like breaking his neck. He ends the fight by throwing Jantapan in a display cabinet which crushes his spine killing him. Jake returns downstairs which is now full of dead bodies of Thai police and embraces the girls and then Sunti only to discover he is fatally injured. Sunti wishes Jake farewell before dying in his arms. Military forces led by Leon and their General enter but Leon orders them to hold fire after seeing Jake with the dead Sunti and kidnapped girls.

After the battle, a Buddhist funeral with Jake in the lead is seen. Jake steps into the river and throws Suntis ashes in the water. A vision of Sunti smiling fades in and later fades out. Jake was looking at the river and saying "Goodbye brother."

==Cast==
* Steven Seagal as Jake Hopper
* Byron Mann as Sunti
* Monica Lo as Lulu
* Tom Wu as General Jantapan
* Sara Malakul Lane as Jessica Hopper Patrick Robinson as Leon Washington
* Vincent Riotta as Fitch McQuaid
* Norman Veeratum as Suthep
*Eilidh MacQueen as Sara Winthorpe
* Chan Siu-tung as Kong
* Kevork Malikyan as Fernand Zadir
* Pongpat Wachirabunjong as Mongkol
* Alastair Vardy as Security Guard
* Andy Adam as Security Guard Shahkritt Yamnarm as Brice
* Max Ruddock as Russel
* Martin McDougall as Tom Collins
* Nicolas Rochette as Masked Man
* Ian Robison as Tom Blake
* Colin Stinton as Jim Cox
* Akaluk Oisingo as Taxi Driver
* Wannakit Siriput as Tommy Taipei 
* Malin Moberg as Woman in Pool

==Production==
It is set and filmed in Bangkok, Thailand, in 42 days on February 3 and March 17, 2003.
 natural causes in Bangkok during the last few days of filming.

==Home media== Region 1 Columbia TriStar Home Entertainment.

==External links==
* 

 
 
 
 
 
 
 
 
 
 