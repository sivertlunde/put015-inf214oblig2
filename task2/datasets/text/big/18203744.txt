The Pumpkin Karver
 
 
{{Infobox film
| name = The Pumpkin Karver
| image = The Pumpkin Karver.jpg
| caption = Official poster for The Pumpkin Karver
| director = Robert Mann
| producer = Sheldon Silverstein
| writer = Robert Mann  Sheldon Silverstein
| starring = Amy Weber Minka Kelly Terrence Evans Tony Little Charity Shea
| music = David Kowal
| cinematography = Philip Hurn
| editing = Philip Hurn
| narrated = MainSail Production Services Inc.
| studio = Mannatee Films
| distributor = First Look International
| Special Thanks = Craig Kaminski
| released =  
| runtime = 90 minutes English
| country = United States Australia
| budget = $1,000,000
}}

The Pumpkin Karver is a 2006 slasher film written by Robert Mann and Sheldon Silverstein,  produced by Sheldon Silverstein and directed by Robert Mann,  it stars Amy Weber, Minka Kelly, Terrence Evans and Charity Shea. 

==Plot==
 
Jonathan and his sister Lynn have moved to a new town to start a new life after Jonathan killed his sisters boyfriend Alec accidentally in a prank gone wrong. They are heading to a Halloween party but theres a catch: the man Jonathan killed is back in a new, horrific form. Lynn and Jonathan meet some party goers, like Tammy, twins Grazer and Spinner, Tammys ex, Lance, the sexy-blonde Rachel, Lances friend, Brian, best friends Yolanda & Vicki and the old carver, Ben Wicket, as well as other party-goers. After a fight with Lance, Jonathan starts thinking that Alec is there at the party, but chooses to ignore this.

At night, Rachel has sex in her van with Lances friend, A.J, but he leaves angrily after Rachel hits him. Rachel goes looking for A.J, but when she doesnt find him, goes back to her van. However, shes attacked, by a supposed Alec, who carves her face.

Later, Brian reveals to Tammy that "Lance is being a dick. I mean, I know hes always been a dick, but he went too far this time." Shortly after Tammy leaves, Brian is attacked by a killer, who makes him walk backward, making Brian impale himself with an electric drill.

Jonathan is stalked several times by Ben, but then escapes. Later, Yolanda and Vicki find Rachels dead body and flee, warning Grazer and Spinner, but they are too drunk and refuse to believe them.

Later, Spinner leaves and Grazer is struck with a scythe, before getting decapitated. Jonathan disappears, and Lynn and Tammy go and try to find him. Lynn meets Lance, who attempts to rape her, but she escapes before Lance is stabbed to death.

Tammy is pursued by the killer, before getting trapped in a bear trap, and stabbed to death. Lynn and Jonathan meet, where they later find Tammys dead body. Alec and Jonathan meet, and they confront, until Jonathan stabs Alice to death, the same way he did in the prank. Later, it is revealed that "Alec" is Ben Wicket, and it was Jonathans trauma the one that made him think it was Alec.

The next morning, the police arrive. Shortly after questioning, Jonathan and Lynn are ready to leave, but Jonathan takes a sudden shift into Alecs shape, revealing that he was the killer, and that Ben had been trying to kill him because of that.

The movie ends with Jonathan stabbing Lynn to death.

==Cast==
* Amy Weber as Lynn Starks
* Minka Kelly as Tammy
* Terrence Evans as Ben Wicket
* Tony Little as Officer Briggs
* Charity Shea as Rachel
* Michael Zara as Jonathan Starks
* Mistie Adams as Yolanda
* David Austin as Lance
* Lindsey Carpenter as Amber
* Rachelle Clune as Connie
* Jonathan Conrad as A.J.
* Amy Cowieson as Go-Go Dancer
* Briana Gerber as Vicki
* Kenny Gould as Detective Farrows
* Thomas Hurn as Joe
* Bryan Jamerson as MIB Kid #2
* Brian Kary as MIB Kid #1
* Amber Kendrella as Marilyn Monroe
* Robert Mann as D.J. Jon on Radio
* David Phillips as Bonedaddy
* Jared Show as Grazer
* Alex Weed as Spinner
* David J. Wright as Alec

==Release==
The film was released on 31 October 2006 as direct-to-video project over First Look International. 

==Reception==
 
==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 