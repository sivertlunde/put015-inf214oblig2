Summer Rental
{{Infobox film
| name           = Summer Rental
| image          = summer rental.jpg
| caption        = Summer Rental movie poster
| director       = Carl Reiner
| writer         = Mark Reisman Jeremy Stevens
| starring = {{Plainlist|
* John Candy
* Richard Crenna
}}
| producer       = George Shapino
| music          = Alan Silvestri
| cinematography = Ric Waite
| editing        = Bud Molin
| distributor    = Paramount Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States English
| budget         =
| gross          = $24,689,703 (USA)
}} 1985 comedy film, directed by Carl Reiner and starring John Candy. The films screenplay was written by Mark Reisman and Jeremy Stevens. An original music score was composed for the film by Alan Silvestri. The film was released on August 9, 1985, by Paramount Pictures.  

==Plot==
Overworked air-traffic controller Jack Chester is given four weeks off, as an alternative to being fired after nearly causing a mid-air collision on the job. He uses this time off to take his wife Sandy and children Jennifer, Bobby, and Laurie on a summer vacation from the Atlanta area to the fictitious Gulf Coast resort town of Citrus Cove, Florida, where they are constantly beset by a never-ending barrage of problems. First they move into the wrong house by mistake, then Jack receives a leg injury that prevents him from spending time with his family.

The family ends up in a decrepit shack on a public beach in Florida, with a constant stream of vacationers tromping through. Jack locks horns with pompous local sailing champion Al Pellet, who becomes the owner of this dubious piece of real estate after the previous owner passes away. Jack gives Al the check for $1,000 to cover the rent for the next two weeks but Al, who despises Jack, tears up the check and orders the Chesters to leave the house when their first two weeks expire or hell throw them out, personally.

To avoid an early eviction, Jack challenges Al to a race at the upcoming Citrus Cove Regatta: If Al wins, Jack will pay him the $1,000 rent and take his family home; if Jack wins, he keeps the money and earns the right to stay in the house for two weeks rent-free. Al scoffs at the notion that Jack could ever defeat him in a race, but accepts the challenge. However, Jack hasnt sailed for many years and doesnt even have a boat. Scully, a local saloon keeper with a pirates mentality, befriends Jack and volunteers to help him on both counts.

The bored Chesters come to life by helping Jack make his new vessel seaworthy. This motley crew is at first no match for Al or anybody else in the race, but tossing useless garbage overboard, a strong breeze and a large pair of pants enable Jack to enjoy a victory at sea.

==Cast==
* John Candy as Jack Chester
* Karen Austin as Sandy Chester
* Kerri Green as Jennifer Chester
* Joey Lawrence as Bobby Chester
* Aubrey Jene as Laurie Chester
* Rip Torn as Richard Scully
* Richard Crenna as Al Pellet
* John Larroquette as Don Moore
* Richard Herd as Angus MacLachlan
* Lois Hamilton as Vicki Sanders
* Carmine Caridi as Ed Sanders Bob Wells as Stan Greene
* Dick Anthony Williams as Dan Gardner
* Reni Santoni as Announcer
* Marcella Mercedes Cole as Mourner at wake

==Soundtrack==
Jimmy Buffetts "Turning Around" plays during the closing credits.  It is also played when the Chesters are fixing up the Barnacle.  The movie soundtrack is the only place to find this song, and also YouTube.
 Critical Condition.

==Location==
Summer Rental was filmed in St. Pete Beach, near St. Petersburg, Florida.  Several local landmarks can be seen throughout the movie, including the St. Petersburg Pier during the final leg of the Regatta. Other landmarks include the old drawbridge on U.S. Route 19 in Florida|US19/Interstate 275 (Florida)|I-275 north of the old Sunshine Skyway, as well as shots of Egmont Key in the distance.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 