Kasthooriman
 
 
 
{{Infobox film
| name           = Kasthooriman
| image          = Kasthooriman.jpg
| director       = A. K. Lohithadas
| producer       = Mudra Arts International
| writer         = A. K. Lohithadas
| music          = Ouseppachan   Kaithapram Damodaran Namboothiri (lyrics)                      
| starring       = Kunchako Boban   Meera Jasmine   Kalashala Babu   Shammi Thilakan
| released       = 4 April 2003 
| country        = India
| language       = Malayalam
}}
 Tamil under the title Kasthuri Maan.

==Plot==
Sajan Joseph Alukka (Kunchako Boban) is a  soft-spoken and studious young man, while his junior Priyamvada (Meera Jasmine) is smart and outgoing. Her close friend is in love with Sajan, but he avoids her by saying that his only ambition in life is to study hard and become an IAS. But Priya follows him and digs out the truth that though he hails from a rich family, his father has now gone bankrupt and has no money even to pay the exam fee. On hearing this, her friend ditches him. Slowly Priya starts having a soft corner for him. She even pays his fees, but Sajan tries to avoid her and treats this as an insult.

One day Sajan meets Priya in his fathers friends house. He is shocked to learn that she is a servant there. Sajan realizes that she works as a domestic servant in four houses to look after her family and pay for her education. He admires her and starts loving her. Priya helps him with money to go for his IAS coaching in Delhi and it is the happiest day for her when Sajan gets his IAS. But Priya murders her abusive brother-in-law in self-defense and is sent to prison. Sajan waits for her return and the two are reunited after Priyas prison term is completed.

==Cast==
* Kunchacko Boban as Sajan Joseph Alukka
* Meera Jasmine as Priyamvada
* Shammi Thilakan as Rajendran
* Kalasala Babu as Lonappan 
* Cochin Haneefa as House Owner
* Leshoy as Joseph
* Bindu Murali as Alice
* Ambika Mohan as Vanaja
* Sona Nair as Raji
* Kulapulli Leela as Rajis mother in law
* Suma Jayaram as house Owner
* Devi Chandana as Sajans siter
* Saddiq as Sajans brother in law
* Rema Devi as College Lecturer
* Nandakishore 
* Swapna as Anus friend
* Nivedya as Police Officer

==Box Office==
The movie became a hit

== Sound Track ==
The films soundtrack contains 8 songs, all composed by Ouseppachan and Lyrics by Kaithapram Damodaran Namboothiri.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Azhake"
| P. Jayachandran, Sujatha Mohan
|-
| 2
| "Karkuzhali"
| Sujatha Mohan
|-
| 3
| "One Plus One" Jyotsna
|-
| 4
| "Raakkuyil  "
| K. J. Yesudas, K. S. Chitra, Chorus
|-
| 5
| "Raakkuyil  "
| K. J. Yesudas, Chorus
|-
| 6
| "Maarivilthooval"
| Santhosh Keshav
|-
| 7
| "Poonkuyile"
| Vidhu Prathap
|-
| 8
| "Raakkuyil Paadi  "
| Ouseppachan
|}

== External links ==
*  

 
 
 
 


 