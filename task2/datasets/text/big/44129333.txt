Thadakom
{{Infobox film 
| name           = Thadakom
| image          =
| caption        =
| director       = IV Sasi
| producer       = Areefa Hassan
| writer         =
| screenplay     = T. Damodaran Seema Sumalatha
| music          = A. T. Ummer
| cinematography = Ashok Kumar
| editing        = K Narayanan
| studio         = Arifa Enterprises
| distributor    = Arifa Enterprises
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, Seema and Sumalatha in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Mammootty
*Ratheesh Seema
*Sumalatha
*Captain Raju
*Balan K Nair
*Bheeman Raghu
*Kuthiravattam Pappu
*Sabitha Anand
*Surekha

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by PB Sreenivas and Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Jheel Kinaare || S Janaki || PB Sreenivas || 
|-
| 2 || Moodal manjin chaaruthayil || K. J. Yesudas, S Janaki || Poovachal Khader || 
|-
| 3 || Raganuraga hrudayangal || K. J. Yesudas, S Janaki || Poovachal Khader || 
|-
| 4 || Raganuraga hrudayangal (Sad) || K. J. Yesudas, S Janaki || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 