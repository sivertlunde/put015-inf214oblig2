The Taking of Power by Louis XIV
{{Infobox film
| name           = The Taking of Power by Louis XIV
| image          =
| image_size     =
| caption        =
| director       = Roberto Rossellini
| producer       = Roberto Rossellini
| writer         = Philippe Erlanger Jean Gruault
| narrator       =
| starring       = Jean-Marie Patte Raymond Jourdan Katharina Renn Silvagni Pierre Barrat
| music          =
| cinematography = Georges Leclerc
| editing        = Armand Ridel
| distributor    =
| released       = 1966
| runtime        = 100 min.
| country        = France French
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 French television Italian film French king Louis XIVs rise to power after the death of his powerful advisor, Cardinal Mazarin. To achieve this political autonomy, Louis deals with his mother and the court nobles, all of whom assumed that Mazarins death would give them all more power.

==Cast== Louis XIV
* Raymond Jourdan as Jean-Baptiste Colbert
* Silvagni as Cardinal Mazarin
* Katharina Renn as Anne of Austria
* Dominique Vincent as Suzanne, Marquise du Plessis-Bellière
* Pierre Barrat as Nicolas Fouquet
* Fernand Fabre as Michel Le Tellier, Marquis of Barbezieux
* Françoise Ponty as Louise de la Vallière
* Joelle Laugeois as Maria Theresa of Spain

==Release==
The Taking of Power by Louis XIV received a DVD release by The Criterion Collection in January 2009. Criterion.   criterion.com. Retrieved: October 17, 2008. 

==Notes==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 


 
 