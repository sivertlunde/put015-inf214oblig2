Black Hawk Down (film)
{{Infobox film
| name           = Black Hawk Down
| image          = Black hawk down ver1.jpg
| caption        = Theatrical release poster
| director       = Ridley Scott
| producer       = Jerry Bruckheimer Ridley Scott
| screenplay     = Ken Nolan
| based on       =  
| starring       = Josh Hartnett Eric Bana Ewan McGregor Tom Sizemore William Fichtner Sam Shepard
| music          = Hans Zimmer
| cinematography = Sławomir Idziak
| editing        = Pietro Scalia
| studio         = Revolution Studios Jerry Bruckheimer Films Scott Free Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 144 minutes
| country        = United States   United Kingdom
| language       = English
| budget         = $92 million
| gross          = $172,989,651 }}
  book of the same name by Mark Bowden based on his series of articles published in The Philadelphia Inquirer. The 29-part series chronicled the events of a 1993 raid in Mogadishu by the U.S. military aimed at capturing faction leader Mohamed Farrah Aidid and the ensuing Battle of Mogadishu (1993)|battle.
 Oscars for Best Film Best Sound Mixing at the 74th Academy Awards.    The movie was received positively by many American film critics, but was strongly criticized by a number of foreign groups and military officials.   

==Plot== civil war, 160th SOAR aviators are deployed to Mogadishu to capture Aidid, who has proclaimed himself president of the country.

To cement his power and subdue the population in the south, Aidid and his militia seize Red Cross food shipments, while the UN forces are powerless to directly intervene. Outside Mogadishu, Rangers and Delta Force operators capture Osman Ali Atto, a faction leader selling arms to Aidids militia. Shortly thereafter, a mission is planned to capture Omar Salad Elmi and Abdi Hassan Awale Qeybdiid, two of Aidids top advisers.
 PFC Todd SSG Matthew Chalk Four, his first command.
 Black Hawk SSG Jeff Struecker are detached from the convoy to return Blackburn to the UN-held Mogadishu Airport.

SGT Dominick Pilla is shot and killed just as Strueckers column departs, and shortly thereafter Black Hawk Super Six-One, piloted by CWO Clifton "Elvis" Wolcott, is shot down by a rocket-propelled grenade (RPG) and crashes deep within the city. Both pilots are killed, the two crew chiefs are wounded, and one Delta Force sniper on board escapes in another helicopter.

The ground forces are rerouted to converge on the crash site. The Somali militia erects roadblocks, and LTC Danny McKnights Humvee is unable to reach the crash site, while sustaining heavy casualties. Meanwhile, two Ranger Chalks, including Eversmanns unit, reach Super-Six Ones crash site and set up a defensive perimeter to await evacuation with the two wounded men and the fallen pilots. In the interim, Super Six-Four, piloted by CWO Michael Durant, is also shot down by an RPG and crashes several blocks away. 

With CPT Mike Steeles Rangers pinned down and sustaining heavy casualties, no ground forces can reach Super Six-Four s crash site nor reinforce the Rangers defending Super Six-One. Two Delta Force snipers, SFC Randy Shughart and MSG Gary Gordon are inserted by helicopter to Super Six-Four s crash site, where they find Durant still alive. The site is eventually overrun, Gordon and Shughart are killed, and Durant is captured by Aidids militia.
 10th Mountain Division, including Malaysian and Pakistani armored units.

As night falls, Aidids militia launch a sustained assault on the trapped Americans at Super Six-Ones crash site. The militants are held off throughout the night by strafing runs and rocket attacks from MD Helicopters MH-6 Little Bird|AH-6J Little Bird helicopter gunships of the 160th Special Operations Aviation Regiment (Airborne)|Nightstalkers, until the 10th Mountain Divisions relief column is able to reach the Americans. The wounded and casualties are evacuated in the vehicles, but a handful of Rangers and Delta Force soldiers are forced to run from the crash site back to the stadium, in the UN Safe Zone.

The closing credits detail the results of the raid: 19 American soldiers were killed, with over 1,000 Somali militants and civilians dead. Durant was released after 11 days of captivity. Delta Force snipers Gordon and Shughart were the first soldiers to be awarded the Medal of Honor since the Vietnam War. Two weeks later, President Bill Clinton withdrew the Delta Force and Rangers from Somalia. Major General William F. Garrison accepted full responsibility for the outcome of the raid. On August 2, 1996, Aidid was killed in a battle with a rival faction. General Garrison retired the following day.

==Cast==
 75th Rangers=== SSG Matt Eversmann, the leader of Chalk 4 SPC John "Grimesey" Grimes, a desk clerk
* Tom Sizemore as LTC Danny McKnight, the commander of the 3rd Ranger Battalion
* Ewen Bremner as SPC Shawn Nelson, a squad gunner
* Gabriel Casseus as SPC Mike Kurth SFC Kurt "Doc" Schmid, a medic of Chalk 4 (portrayed as a Ranger in the film; was Delta Force in real life) LT John Beales
* Tom Guiry as SSG Ed Yurek CPL Jamie Smith SGT Dominick Pilla CPT Michael Mike Steele, commander, Bravo Company, 3rd Ranger Battalion PVT Richard "Alphabet" Kowalewski
* Brian Van Holt as SSG Jeff Struecker
* Ian Virgo as PVT John Waddell
* Tom Hardy as SPC Lance Twombly
* Gregory Sporleder as SGT Scott Galentine, the ground radio and telephone communications operator of Chalk 4
* Carmine Giovinazzo as SGT Mike Goodale
* Chris Beetem as SGT Casey Joyce
* Matthew Marsden as SPC Dale Sizemore
* Gideon Emery as PFC Favid Montes
* Orlando Bloom as PFC Todd Blackburn
* Enrique Murciano as SGT Lorenzo Ruiz
* Michael Roof as PVT John Maddox
* Tac Fitzgerald as SGT Keni Thomas

===Delta Force=== MG William F. Garrison, commander of Task Force Ranger
* Eric Bana as SFC Norm "Hoot" Gibson (based on SFC John Macejunas, SFC Norm Hooten and SFC Matthew Rierson)
* William Fichtner as SFC Jeff Sanderson (based on SFC Paul Howe)   
* Kim Coates as MSG Chris Wex (based on MSG Tim "Griz" Martin)
* Steven Ford as LTC Joe Cribbs, the Joint Operations Commander who organizes and leads the international peacekeeping column (based on Lee Van Arsdale)
* Željko Ivanek as LTC Gary Harrell, the commander of C Squadron
* Johnny Strong as SFC Randy Shughart, a sniper flying on Black Hawk Super Six-Two
* Nikolaj Coster-Waldau as MSG Gary Gordon, a sniper flying on Black Hawk Super Six-Two
* Richard Tyson as SSG Daniel Busch, a sniper flying on Black Hawk Super Six-One
 160th SOAR – Night Stalkers=== CW4 Michael Durant, the pilot of Super Six-Four
* Glenn Morshower as LTC Tom Matthews, the commander of 1st Battalion
* Jeremy Piven as CWO Clifton Wolcott, the pilot of Super Six-One, the first Black Hawk down
* Boyd Kestner as CW3 Mike Goffena, the pilot of Super Six-Two who inserts Gordon and Shughart

===Miscellaneous=== George Harris as Osman Atto
* Razaaq Adoti as Yousuf Dahir Moalim, the Somali militia leader
* Treva Etienne as Firimbi, Somali war chief and Michael Durants captor
* Ty Burrell as United States Air Force Pararescue Timothy A. Wilkinson.

==Background and production==
Adapting   by   (2001) instead. 
 WGA committee.

Filming began in March 2001 in Kenitra, Morocco, and concluded in late June. 

The book relied on a dramatization of participant accounts, which were the basis of the movie. SPC John Stebbins was renamed as fictional "John Grimes." Stebbins had been convicted by court martial, in 1999, for sexually assaulting his daughter.  Mark Bowden said the Pentagon had requested the change.  Bowden wrote early screenplay drafts, before Bruckheimer gave it to screenwriter Nolan. The POW-captor conversation, between pilot Mike Durant and militiaman Firimbi, is from a Bowden script draft.

For military verisimilitude, the Ranger actors took a crash, one-week Ranger familiarization course at Fort Benning, the Delta Force actors took a two-week commando course from the 1st Special Warfare Training Group at Fort Bragg, and Ron Eldard and the actors playing 160th SOAR helicopter pilots were lectured by captured aviator Michael Durant at Fort Campbell. The U.S. Army supplied the materiel and the helicopters from the 160th Special Operations Aviation Regiment. Most pilots (e.g., Keith Jones, who speaks some dialogue) had participated in the historic battle on October 3–4, 1993.   

On the last day of their week-long Army Ranger orientation at Fort Benning, the actors who portrayed the Rangers received letters slipped under their doors. It thanked them for their hard work, and asked them to "tell our story true", signed with the names of the men who died in the Mogadishu firefight.  A platoon of Rangers from B-3/75 did the fast-roping scenes and appeared as extras; John Collette, a Ranger Specialist during the battle, served as a stunt performer.  

Many of the actors bonded with the soldiers who trained them for their roles. Actor Tom Sizemore said, "What really got me at training camp was the Ranger Creed. I dont think most of us can understand that kind of mutual devotion. Its like having 200 best friends and every single one of them would die for you". 

Although the filmmakers considered filming in Jordan, they found the city of Amman too built up and landlocked. Scott and production designer Arthur Max subsequently turned to Morocco, where they had previously worked on Gladiator (2000 film)|Gladiator. Scott preferred that urban setting for authenticity.  Most of the film was photographed in the cities of Rabat and Salé; the Task Force Ranger base sequences were filmed at Kénitra. 
 Somali actors.  Additionally, no Somali consultants were hired for accuracy, according to writer Bowden.   

The film features soldiers wearing helmets with their last names on them. Although this was not accurate, director Ridley Scott used this device to help the audience distinguish among the characters because "they all look the same once the uniforms are on". 

==Release==

===Box office performance=== first at Martin Luther King holiday, the film grossed $5,014,475 on the holiday of Monday, January 21, 2002, for a 4-day weekend total of $33,628,211. Only Titanic (1997 film)|Titanic had previously grossed more money over the Martin Luther King holiday weekend. Black Hawk Down finished first at the box office during its first three weeks of wide release. When the film was pulled from theatres on April 14, 2002, after its 15th week, it had grossed $108,638,746 domestically and $64,350,906 overseas for a worldwide total of $172,989,651.   

===Critical response===
The film received many positive reviews from mainstream critics.    and a rating of 74 on Metacritic. 

The film has had a small cultural legacy, which has been studied academically by media analysts dissecting how media reflects American perceptions of war. Newsweek writer Evan Thomas considered the movie one of the most culturally significant films of the George W. Bush presidency. He suggested that although the film was presented as being antiwar, it was at its core prowar. He further wrote that "though it depicted a shameful defeat, the soldiers were heroes willing to die for their brothers in arms  The movie showed brutal scenes of killing, but also courage, stoicism and honor  The overall effect was stirring, if slightly pornographic, and it seemed to enhance the desire of Americans for a thumping war to avenge September 11 attacks|9/11."  

Stephen A. Klien, writing in Critical Studies in Media Communication, argued that the films sensational rendering of war had the effect of encouraging audiences to empathize with the films pro-soldier leitmotif. He suggested that this in turn served to "conflate personal support of American soldiers with support of American military policy" and discourage "critical public discourse concerning justification for and execution of military interventionist policy." 

===Soundtrack===
 

== Accolades ==
 
Black Hawk Down received four Academy Award nominations for Best Director (lost to  ) and won two Oscars for Best Sound and Best Film Editing. It also received three BAFTA Award nominations for Best Cinematography, Best Sound and Best Editing.
{| class="wikitable"
|-
! Award !! Category !! Nominee !! Result
|-
|   || ||  
|- AFI Award || Cinematographer of the Year || Slawomir Idziak ||  
|-
| Director of the Year || Ridley Scott ||  
|-
| Editor of the Year || Pietro Scalia ||  
|-
| Movie of the Year || Jerry Bruckheimer Ridley Scott ||  
|-
| Production Designer of the Year || Arthur Max ||  
|- Best Film Editing || Pietro Scalia ||  
|- Best Sound Mixing || Michael Minkler Myron Nettinga Chris Munro ||  
|- Best Cinematography || Slawomir Idziak ||  
|- Best Director || Ridley Scott ||  
|-
| Saturn Award || Best Action/Adventure/Thriller Film || ||  
|- Eddie Award || Best Edited Feature Film – Dramatic || Pietro Scalia ||  
|- Contemporary Film || Keith Pain Marco Trentini Gianni Giovagnoni Cliff Robinson Pier Luigi Basile Ivo Husnjak Arthur Max ||  
|-
| Harry Award || || ||  
|- BAFTA Award Best Cinematography || Slawomir Idziak ||  
|- Best Editing || Pietro Scalia ||  
|- Best Sound || Chris Munro Per Hallberg Michael Minkler Myron Nettinga Karen Baker Landers ||  
|- Golden Reel Award || Best Sound Editing – Dialogue and ADR in a Feature Film || Per Hallberg Karen Baker Landers Chris Jargo Mark L. Mangino Chris Hogan ||  
|-
| Best Sound Editing – Effects & Foley, Domestic Feature Film || Per Hallberg Karen Baker Landers Craig S. Jaeger Jon Title Christopher Assells Dino Dimuro Dan Hegeman Michael A. Reagan Gregory Hainer Perry Robertson Peter Staubli Bruce Tanis Michael Hertlein Solange S. Schwalbe ||  
|-
| Plus Camerimage || Golden Frog || Slawomir Idziak ||  
|-
| Cinema Audio Society Award || Outstanding Sound Mixing for Motion Pictures || Michael Minkler Myron Nettinga Chris Munro ||  
|- Outstanding Directorial Achievement in Motion Pictures || Ridley Scott ||  
|-
| Golden Trailer Award || Best Drama || Trailer Park, Inc. ||  
|- Best Movie || ||  
|- Best Action Sequence || First helicopter crash ||  
|-
| rowspan=3 | PFCS Award || Best Acting Ensemble || Eric Bana Ewen Bremner William Fichtner Josh Hartnett Jason Isaacs Ewan McGregor Sam Shepard Tom Sizemore ||  
|-
| Best Cinematography || Slawomir Idziak ||  
|-
| Best Film Editing || Pietro Scalia ||  
|-
| rowspan=2 | Teen Choice Award || Film – Choice Actor, Drama/Action Adventure || Josh Hartnett ||  
|- Film – Choice Movie, Drama/Action Adventure || ||  
|- Best Original Soundtrack of the Year || rowspan=2 | Hans Zimmer ||  
|- Soundtrack Composer of the Year ||  
|-
| Writers Guild of America Award || Best Screenplay Based on Material Previously Produced or Published || Ken Nolan ||  
|- ASCAP Award The Ring)  ||  
|-
| DVD Exclusive Award || Best Overall DVD, New Movie (Including All Extra Features) || Charles de Lauzirika  (Deluxe Edition) ||  
|-
| Saturn Award || Best DVD Special Edition Release || ||  
|}

==Controversies and inaccuracies==
Soon after Black Hawk Down s release, the Somali Justice Advocacy Center in California denounced what they felt was its brutal and dehumanizing depiction of Somalis and called for its boycott. 
 Brendan Sexton, an actor who briefly appeared in the movie, said the version of the film which made it onto theater screens significantly differed from the one recounted in the original script. According to him, many scenes asking hard questions of the U.S. troops with regard to the violent realities of war, the true purpose of their mission in Somalia, etc., were cut out. 

In a review featured in The New York Times, film critic Elvis Mitchell expressed dissatisfaction with the films "lack of characterization", and opined that the film "reeks of glumly staged racism".  Owen Gleiberman and Sean Burns, the film critics for the mainstream magazine Entertainment Weekly and the alternative newspaper Philadelphia Weekly, respectively, echoed the sentiment that the depiction was racist.  Similarly, American film critic Wheeler Winston Dixon found the films "absence of motivation and characterization" disturbing, and wrote that while American audiences might find the film to be a "paean to patriotism", other audiences might find it to be a "deliberately hostile enterprise"; nevertheless, Dixon lauded the films "spectacular display of pyrotechnics coupled with equally adroit editing."  Jerry Bruckheimer, the films producer, rejected such claims on The OReilly Factor, putting them down to political correctness in part due to Hollywoods liberal leanings. 

Somali nationals charge that the African actors chosen to play the Somalis in the film do not in the least bit resemble the racially unique peoples of the Horn of Africa nor does the language they communicate in sound like the Afro-Asiatic languages|Afro-Asiatic tongue spoken by the Somali people. The abrasive manner in which lines are delivered and the films inauthentic vision of Somali culture, they add, fails to capture the tone, mannerisms and spirit of actual life in Somalia. At one screening in Somalia, young men cheered whenever an American soldiers character was shot on screen.   

In an interview with the BBC, the faction leader Osman Ali Atto said that many aspects of the film are factually incorrect. He took exception with the ostentatious character chosen to portray him; Ali Atto does not look like the actor who portrayed him, smoke cigars, or wear earrings,    facts which were later confirmed by SEAL Team Six sniper Howard E. Wasdin in his 2012 memoirs. Wasdin also indicated that while the character in the movie ridiculed his captors, Atto in reality seemed concerned that Wasdin and his men had been sent to kill rather than apprehend him.    Atto additionally stated that he was not consulted about the project or approached for permission, and that the film sequence re-enacting his arrest contained several inaccuracies: 

 

Navy SEAL Wasdin similarly remarked that while olive green military riggers tape was used to mark the roof of the car in question in the movie, his team in actuality managed to track down Attos whereabouts using a much more sophisticated technique involving the implantation of a homing device in a cane. The cane was then presented as a gift for Atto to a contact who routinely met with him, which eventually led the team directly to the faction leader. 

Malaysian military officials whose own troops were involved in the fighting have likewise raised complaints regarding the films accuracy. Retired Brigadier-General Abdul Latif Ahmad, who at the time commanded Malaysian forces in Mogadishu, told the AFP news agency that Malaysian moviegoers would be under the wrong impression that the real battle was fought by the Americans alone, while Malaysian troops were "mere bus drivers to ferry them out". The film does portray Malaysians contributing to the battle from their vehicles. 

 , Musharraf wrote:

 

==Supposed sequel==
In 2006, Cirio H. Santiagos action movie When Eagles Strike was released as Black Hawk Down sequel in Taiwan. 

==See also==
 
*List of films featuring the United States Navy SEALs

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*   at the Internet Movie Firearms Database

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 