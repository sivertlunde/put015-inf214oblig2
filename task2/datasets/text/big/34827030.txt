Bent Familia
{{Infobox film
| name           = Bent Familia
| image          = Bent_Familia.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Nouri Bouzid
| producer       =  
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| distributor    =  
| released       = 1997 
| runtime        = 108  minutes
| country        = Tunisia
| language       = Arabic
| budget         = 
| gross          = 
}}
Bent Familia  is a 1997  drama set in Tunisia  about Amina (Leila Nassim) a married Muslim woman living in Tunis with her two daughters. Even though she is allowed certain  freedoms as a Muslim woman this is curtailed when she meets her old friend from school called Aida acted by (Amel Hédhili).

A mother of two, Aida took a firm stand not to tolerate her husband’s infidelities. She divorced and considers herself a liberated woman. Then there is their mutual friend Fatiha (Nadia Kaci), an intellectual who is adamant that she is going to leave Tunis and settle in the West. This strong triangle of friendship threatens Amina’s husband, Majid (Raoul Ben Amor), another adulterer who tries to bring the might of the chauvinist side of Muslim society down on her. Directed by Nouri Bouzid, this well-shot film presents us with the very real problems of women in contemporary North Africa, but offers no easy solutions.

== Casts ==
*Raouf Ben Amor 
*Kaouthar Bardi 
*Abderazek

== Awards ==
*1997: Montpellier Mediterranean Film Festival: Audience Award: Golden Antigone 
*1997: Namur International Festival of French-speaking Film: Best Actress
*1997: Venice Film Festival: OCIC Award Honourable Mention

== Movie Trailer ==
*Bent-Familia  sourced from the Africa Film Library

== External links ==
 
* Bent Familia   on mnetcorporate.co.za
* 
*Bent Familia   on arabfilm.com
*Bent Familia   on luxorafricanfilmfestival.com

 
 

 