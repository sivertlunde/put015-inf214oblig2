Educated Evans
{{Infobox film
| name           = Educated Evans
| image          = "Educated_Evans"_(1936).jpg
| image_size     =
| caption        =
| director       = William Beaudine
| producer       = Irving Asher
| writer         = Frank Launder Edgar Wallace (novel) Max Miller Hal Walters Clarice Mayne
| music          =
| cinematography = Basil Emmott
| editing        = Warner Brothers-First First National Productions
| released       = September 1936
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
}} Max Miller.  The film, set in the world of horse racing, was based on the 1924 novel of the same name by the prolific Edgar Wallace.  It is one of five films starring Miller which is not known to be extant, and is classed as "missing, believed Lost film|lost".  A sequel Thank Evans was released in 1938; it too is missing.

The film was made at Teddington Studios, with sets designed by Peter Proud.

==Plot==
Cockney racing tipster Evans (Miller) is asked by a nouveau riche and socially aspirant couple to train a racehorse they have bought.  The couple know nothing about horse racing, but believe that ownership of a successful racehorse will be their entrée into the high society racing set.  Evans does not own a stable, so the horse has to live with him and his two lodgers in an urban mews.  He has to keep constantly on his toes, as circumstances continually threaten to reveal to the horses owners the ramshackle conditions in which the animal is kept.

Despite its less than ideal training environment, the horse turns out to have a natural talent and great racing potential.  It does well in its outings, and is entered for a prestigious race.  Shortly before the big day, disaster strikes when the horse is stolen.  Evans has to track down and outwit the crooks, and manages to recover the horse in the nick of time.  Feeling confident of the horses chances, Evans places a substantial bet on it to win the race.  In his excitement however, he makes a mistake and accidentally lays the bet on a no-hope nag at ridiculously long odds.  The race turns out to be a sensation, with all the favourites including Evans horse failing to finish for one reason or another.  The hopeless carthorse Evans backed in error crosses the line first and he makes a huge financial profit.

==Cast== Max Miller as Educated Evans
* Clarice Mayne as Emily Hackett
* Hal Walters as Nobby
* Albert Whelan as Sgt. Challoner
* Nancy ONeil as Mary George Merritt as Joe Markham
* Frederick Burtwell as Hubert
* Julien Mitchell as Arthur Hackett
* Percy Walsh as Captain Reed
* Prince Monolulu as Himself

==Reception== 75 Most Wanted" list of missing British feature films. 

==References==
 

==External links==
*  , with extensive notes
*  
*   at BFI Film & TV Database

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 