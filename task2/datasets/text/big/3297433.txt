Alex in Wonderland
 
 
{{Infobox film
| name           = Alex in Wonderland
| image          = Alex in Wonderland FilmPoster.jpeg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Paul Mazursky
| producer       = Anthony Ray Larry Tucker
| writer         = Paul Mazursky Larry Tucker
| starring       = Donald Sutherland Ellen Burstyn
| music          = Tom OHorgan
| cinematography = László Kovács (cinematographer)|László Kovács
| editing        = Stuart H. Pappé MGM
| distributor    = MGM
| released       =  
| runtime        = 111 minutes  
| country        = United States
| language       = English
}} Larry Tucker, starring Donald Sutherland and Ellen Burstyn.  Sutherland plays Alex Morrison, a director agonizing over the choice of follow-up project after the success of his first feature film.  The situation is similar to the one Mazursky found himself in following the success of Bob & Carol & Ted & Alice (1969) and he casts himself in a role as a new-style Hollywood producer. His daughter Meg Mazursky appears as Amy, one of Morrisons daughters. Noted teacher of improvisational theater Viola Spolin plays Morrisons mother. The film also features cameo appearances by Federico Fellini and Jeanne Moreau, and seems to be inspired by their work. In particular, Fellinis 8½ (1963), about a film director whos artistically stuck, is referenced.  Moreau sings two songs on the soundtrack, "Le Vrai Scandale" (for which she wrote the words) and "Le Reve Est La."

==Plot==
Young director Alex Morrison feels compelled to follow his recent box-office hit with another blockbuster. While mulling over this dilemma, the directors mind wanders to his past, his present, and probable future. 

==Cast==
* Donald Sutherland as Alex Morrison
* Ellen Burstyn as Beth Morrison
* Paul Mazursky as Hal Stern
* Meg Mazursky as Amy Morrison
* Glenna Sargent as Nancy
* Viola Spolin as Mrs. Morrison
* Andre Philippe as Andre Michael Lerner as Leo
* Joan Delaney as Jane Neil Burstyn as Norman
* Leon Frederick as Lewis
* Federico Fellini as himself
* Jeanne Moreau as herself

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 