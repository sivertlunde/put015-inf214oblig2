The Incredible Invasion
{{Infobox film
| name           = The Incredible Invasion
| image          = The Incredible Invasion.jpg
| image_size     = 
| caption        = 
| director       = Jack Hill Juan Ibáñez
| writer         = 
| narrator       = 
| starring       = Boris Karloff
| music          = 
| cinematography = 
| editing        = 
| distributor    = Columbia  
| released       = April 1971
| runtime        = 90 minutes
| country        = United States/Mexico
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Incredible Invasion (also known as Alien Terror) is a 1971 film directed by Jack Hill. It stars Boris Karloff and Enrique Guzmán.  
 The Snake People, Fear Chamber, and House of Evil. Karloffs scenes for all four films were directed by Jack Hill in Los Angeles in the spring of 1968. The films were then completed in Mexico. All four films were released after Karloffs death. Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 504-507 

==Plot synopsis==
Gudenberg, 1890: Professor John Mayer has invented a ray gun which runs on nuclear power. During testing, a ray is shot into space and passes a flying saucer. The aliens decide to come to Earth to destroy the weapon...

==Cast==
*Boris Karloff as Prof. John Mayer
*Enrique Guzmán as Dr. Paul Rosten
*Christa Linder as Laura
*Maura Monti as Dr. Isabel Reed
*Yerye Beirute as Thomas
*Tere Valez as Nancy
*Sergio Kleiner as Alien
*Tito Novaro as Gen. Nord

==See also==
* Boris Karloff filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 