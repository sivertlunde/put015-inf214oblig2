Stablemates
{{Infobox film
| name           = Stablemates
| image          =
| image_size     =
| caption        =
| director       = Sam Wood
| producer       = Harry Rapf
| writer         = Richard Maibaum Reginald Owen Leonard Praskins Wilhelm Thiele
| narrator       = Margaret Hamilton
| music          = Edward Ward
| cinematography = John F. Seitz
| editing        = W. Donn Hayes
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 89 min.
| country        = United States English
| budget         =
}} 1938 film starring Wallace Beery and Mickey Rooney.  The movie was directed by Sam Wood.

==Plot==
Wallace Beery plays eternally inebriated ex-veterinarian Tom Terry. An aspiring jockey Mickey (Mickey Rooney) idolizes Tom, who reciprocates by passing along horsemanship advice to the kid. The films dramatic high point is where Tom, judgement benumbed by years of alcohol abuse, tries to pull himself long enough to perform a delicate operation on Mickeys beloved horse Lady-Q. The film culminates in a big horse race, with Mickey and Tom laying their hopes on the "long shot."

==Cast==
*Wallace Beery as Doc Tom Terry
*Mickey Rooney as Mickey
*Arthur Hohl as Mr. Gale Margaret Hamilton as Beulah Flanders
*Minor Watson as Barney Donovan
*Marjorie Gateson as Mrs. Shepherd
*Oscar OShea as Pete Whalen

==External links==
* 
* 

 

 
 
 
 
 
 
 


 