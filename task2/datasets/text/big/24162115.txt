The Golden Butterfly
{{Infobox film
| name           = The Golden Butterfly
| image          = 
| image_size     = 
| caption        =  Michael Kertész
| producer       = Arnold Pressburger
| writer         =  
| narrator       = 
| starring       = Hermann Leffler Lili Damita
| music          = Willy Schmidt-Gentner
| cinematography = Gustav Ucicky Eduard von Borsody
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Weimar Republic Austria Denmark
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1926 cinema German film directed by Michael Curtiz. Last film directed Michael Curtiz in the Germany.

==Cast==
* Hermann Leffler – Mac Farland
* Lili Damita – Lilian, seine Pflegetochter
* Nils Asther – Andy, sein Sohn
* Jack Trevor – Aberdeen, Millionär
* Curt Bois – André Dubois, Ballettmeister
* Kurt Gerron – Stammgast
* Karl Platen – Kellner
* Ferdinand Bonn – Theaterdirektor
* Julius von Szöreghy

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 