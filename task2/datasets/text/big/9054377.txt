Les Nanas
{{Infobox film
| name           = Les Nanas
| image          = nanas.jpg
| image_size     = 
| caption        = ©StandArt 1985
| director       = Annick Lanoe
| producer       = Lise Fayolle
| writer         = Annick Lanoe Chantal Pelletier
| narrator       = 
| starring       = Marie-France Pisier Dominique Lavanant Macha Méril Clémentine Célarie Juliette Binoche
| music          = François Valery
| cinematography = François Catonné
| editing        = Joële Van Effenterre
| distributor    = StandArt
| released       =  
| runtime        = 87 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $4,383,645  
}}
Les Nanas (The Chicks) is a 1985 French comedy with an entirely female cast, directed by Annick Lanoe. {{cite web|url=http://en.unifrance.org/movie/3572/nanas-les
|title=Nanas (Les)|publisher=unifrance.org|accessdate=2013-07-28}} 

==Plot==
Christine is in her forties when she learns that her partner Robert has been having an affair for the past few months.  As a liberated woman, Christine refuses to put up with this situation, and supported by her girlfriends who are themselves struggling to find their Mr Right, she takes steps to get in touch with her rival...

==Cast==
* Marie-France Pisier as Christine 
* Dominique Lavanant as Evelyne 
* Macha Méril as Françoise 
* Anémone as Odile
* Odette Laure as Christine s mother
* Catherine Samie as Simone 
* Juliette Binoche as Antoinette 
* Clémentine Célarié as Éliane 
* Marilú Marini as Mariana 
* Myriam Mézières as Dating agency manager
* Caroline Loeb as Adèle 
 Catherine Jacob appeared in an uncredited role.

==References==
 

==External links==
* 
*  
*  
*  

 
 
 
 
 
 


 