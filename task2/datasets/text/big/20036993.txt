Gasolina (film)
{{Infobox Film
| name           = Gasolina
| image          = Gasolina Julio Hernandez.jpg
| director       = Julio Hernández
| producer       = Donald Ranvaud Silvio Sardi Pablo Valladares
| writer         = Julio Hernández
| starring       = José Andres Chamier Carlos Dardon Francisco Jacome
| cinematography = María Secco
| editing        = Aina Calleja
| distributor    = Interior13 Cine
| released       =  
| runtime        = 71 minutes
| country        = Guatemala
| language       = Spanish
| budget         =
}} independent comedy comedy drama drama written and directed by Julio Hernández. The film deals with dehumanization, lack of purpose and recklessness in Guatemalan youth by following the exploits of three teenage boys who go out on a midnight joyride outside their housing project, stealing gasoline from other vehicles along the way. It stars José Andrés Chamier, Carlos Dardón and Francisco Jacome.

==Plot==
This Guatemalan film tells a story of three middle-class teenagers on a high-octane ride to hell outside the safety of their colonia. They siphon gas from their neighbors cars and roam the streets, looking for any and every form of entertainment, but they soon learn that even the simplest actions can have consequences way beyond anything they imagined, and some lessons are learned too late. 

==Accolades==
*Winner: Films in Progress Award, San Sebastián International Film Festival 2007  
*Winner: Horizontes Award, San Sebastián International Film Festival 2008  

==References==
 

==External links==
*  
*  

 
 
 

 