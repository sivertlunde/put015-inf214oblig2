Lawful Larceny (1923 film)
{{Infobox film
| name           = Lawful Larceny
| image          = 
| alt            = 
| caption        = 
| director       = Allan Dwan
| producer       = Adolph Zukor
| screenplay     = John Lynch Samuel Shipman 
| starring       = Hope Hampton Conrad Nagel Nita Naldi Lew Cody Russell Griffin Yvonne Hughes
| music          = 
| cinematography = Harold Rosson
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 drama silent film directed by Allan Dwan and written by John Lynch and Samuel Shipman. The film stars Hope Hampton, Conrad Nagel, Nita Naldi, Lew Cody, Russell Griffin and Yvonne Hughes. The film was released on July 22, 1923, by  Paramount Pictures.  
 1930 with Bebe Daniels in the lead role.
 
==Plot==
 

== Cast ==
*Hope Hampton as Marion Dorsey
*Conrad Nagel as Andrew Dorsey
*Nita Naldi as Vivian Hepburn
*Lew Cody as Guy Tarlow
*Russell Griffin as Sonny Dorsey
*Yvonne Hughes as Billy Van de Vere
*Dolores Costello as Nora the maid
*Gilda Gray as Dancer
*Florence ODenishawn as Dancer
*Alice Maison as Dancer

== References ==
 

== External links ==
*  
*   at listal.com

 

 
 
 
 
 
 
 

 