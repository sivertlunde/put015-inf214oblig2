American Manners
{{infobox film
| name           = American Manners
| image          =
| imagesize      =
| caption        = 
| director       = James W. Horne
| producer       = Richard Talmadge Productions A. Carlos
| writer         = Frank Howard Clark Joseph Farnham (intertitles)
| starring       = Richard Talmadge William Marshall Jack Stevens
| editing        =
| distributor    = Film Booking Offices of America
| released       =   reels
| country        = United States
| language       = Silent English intertitles
}}
 silent drama film directed by James W. Horne. It was produced by Richard Talmadge, who also stars, and was distributed by Film Booking Offices of America|FBO. 

Preserved in the Library of Congress. 

==Cast==
*Richard Talmadge - Roy Thomas
*Mark Fenton - Dan Thomas
*Lee Shumway - Clyde Harven
*Helen Lynch - Gloria Winthrop
*Arthur Millett - Conway
*William H. Turner - Jonas Winthrop
*Pat Harmon - Mike Barclay
*George Warde - Bud (a waif)

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 