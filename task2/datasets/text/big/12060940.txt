Beauty and the Beast (2005 film)
{{Infobox film
|  name=Beauty and the Beast
| image= Bloodbeasts.jpg David Lister
| writer= Paul Anthony
| producer= Janet Blandford Stephen Margolis
| starring= Jane March  William Gregory Lee  Justin Whalin  Eric David Dukas  Greg Melvill-Smith  Candice Hillebrand|Candicé Hillebrand  Marcel Van Heerden  Russel Savadier  Steven van Niekerk  Lee Ann Shepherd  Ron Smerczak Aubrey Lovett  Etienne Oelofse  Etienne Changuion  Antony Jardin
| music= Mark Thomas
| cinematography= Buster Reynolds
| editing= Peter Davies
| runtime= 
| released=  
| language= English
}}
Beauty and the Beast (also known as Blood of Beasts) is a 2005 film which is based on the folktale "Beauty and the Beast" and is set during the time of the Vikings.

==Plot==
Thorsson (Melvill-Smith) the king is falling ill and wants to make a pilgrimage to an island cursed by Odin. The island is home to a horrible beast. His daughter Freya (Jane March|March) is betrothed to Sven, one of Thorrsons greatest warriors. 

Freya tries to get an army together to go back and save her father (whom she believes to be alive). However, no one will join her and Sven reminds her that he is now king and that she is betrothed to him.

Freya and Ingrid go to the island alone to rescue her father. 

Eric steps forward and tells everyone theyve all been acting like cowards. He also says hell go and save the kings daughter himself if Sven does not dare. Sven, not wanting to look like a coward, angrily agrees to send an army back to fight the beast and rescue Freya.

Freya and the Beast begin to develop a friendship with each other.

Sven returns to the island with an army and confronts the Beast. Freya tries to stop them, by telling them the beast is not a monster. Sven does not listen to her, thinking the Beast has fooled her mind. Sven and the Beast fight and ends with Svens archers shooting him. The warriors set the island afire and return home with Freya.

The Beast journeys to Freyas home where Thorsson has recovered from his illness and is once again king. 

As the Beast holds the dying Freya in his arms, Freyas sacrifice breaks the curse and the Beast is Agnar once again.

==Production==
The movie was filmed in both the UK and South Africa.

==References==
 
 

==External links==
*  
*  

 
 
 
 
 
 
 