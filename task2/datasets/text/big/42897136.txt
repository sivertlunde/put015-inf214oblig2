A Doll's House (1943 film)
{{Infobox film
| name           = A Dolls House
| image          = A Dolls House 1943.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Ernesto Arancibia
| producer       = Alberto De Zavalia
| writer         = 
| screenplay     =Alejandro Casona
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          =Julián Bautista
| cinematography =José María Beltrán
| editing        = Kurt Land
| studio         =Estudios San Miguel
| distributor    = 
| runtime=95 minutes
| released       =    
| country        = Argentina Spanish
| budget         =
}}
 1943 Argentina|Argentine Best Sound for the film.

==Cast==
*Delia Garcés
*George Rigaud
*Sebastián Chiola
*Orestes Caviglia
*Alita Román
*Angelina Pagano
*Olga Casares Pearson
*Mirtha Reid
*Jeannet Morel
*Agustín Barrios

==Reception== El Mundo that the film is "too lost in a modern framework" in the adaptation by the local cinema, while the critic of La Nación believed that the film had much dignity as an interpretative work, calling it an "interesting and neat version". Raúl Manrupe and María Alejandra Portela later opined that it was the right film at the time but that it now looks dated and static. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 