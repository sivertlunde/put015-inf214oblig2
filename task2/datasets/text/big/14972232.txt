The Water Nymph
{{Infobox film
| name           = The Water Nymph
| image          = The Water Nymph (1912).webm thumbtime=3
| image_size     =
| caption        =
| director       = Mack Sennett
| producer       =
| writer         =
| starring       = Mabel Normand
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 8 minutes
| country        = United States
| language       = Silent (English intertitles)
}}
 silent comedy Keystone comedy.

The film precedes, and may have been the direct inspiration for, the Sennett Bathing Beauties performers first featured in 1915. 

==Cast==
* Mabel Normand as Diving Venus
* Mack Sennett as Mack
* Ford Sterling as Macks Father
* Fred Mace 	
* Edward Dillon	 
* Mary Maxwell as Nymph
* Mae Busch (unconfirmed)

==Production notes==
The Water Nymph was shot on location in Venice, Los Angeles. 

== References ==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 