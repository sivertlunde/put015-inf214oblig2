Powwow Highway
{{Infobox Film
  | name        = Powwow Highway
  | image       = Phdvdcov.jpg
  | image_size  =
  | caption     = DVD cover art
  | director    = Jonathan Wacks
  | producer    = Jan Wieringa George Harrison Denis OBrien
  | writer      = David Seals Janet Heaney Jean Stawarz
  | starring    = A Martinez Gary Farmer Amanda Wyss
  | music       = Barry Goldberg
  | cinematography = Toyomichi Kurita Jim Stewart    Anchor Bay
  | released    =   
  | runtime     = 87 min.
  | language    = English
  | budget      =
  }} Graham Greene, who were relatively unknown actors at the time,  have small supporting roles.

==Plot==
A member of the Northern Cheyenne tribe of Lame Deer, Montana Buddy Red Bow (Martinez), a quick-tempered activist, is battling greedy developers. On the Northern Cheyenne Indian Reservation he tries to persuade the council to vote against a strip-mining contract. 

Philbert Bono (Farmer) is a hulk of a man guided by sacred visions. He wants to find his medicine, tokens from the spirits. He trades some marijuana, booze, and a few bucks for his war pony – a rusted out, beat up 1964 Buick Wildcat he names Protector.  

Meanwhile, Buddys estranged sister, Bonnie, is arrested in Santa Fe, New Mexico and Buddy is the only family member who can help her and her children, Jane and Sky Red Bow. 

Buddy does not own a car, so he makes the journey with Philbert. They set out on their road trip, and Philberts easygoing ways and insistence on frequent stops to pray and eat prove irritating at first to Buddy, but the men reach an understanding as the trip wears on. Along the way, they visit the Black Hills in South Dakota and Philbert reverently leaves a giant Hersheys Chocolate bar as an offering to his ancestors.  

When they finally reach Santa Fe, they meet up with Bonnies friend Rabbit. Using inspiration from an old western he sees on TV, Philbert breaks Bonnie out of jail. Their escape almost ends in tragedy, but with a little help they make their way back to Montana.

==Box Office and Critical Reception==
Powwow Highway did poorly at the box office, grossing a mere $283,747, despite mostly positive reviews.  and Mexico.

Roger Ebert called Gary Farmers performance "...one of the most wholly convincing I’ve seen..." Booklist called the sequel novel Sweet Medicine, by David Seals, "a comic masterpiece."  , American Library Association. Reviewed Sept. 15, 1992 by Ray Olson  In Sweet Medicine, the characters complain about how they were portrayed in the film, and pass on seeing it when they have the chance.

==Awards==
;Won
* Sundance Film Festival – Filmmakers Trophy – Dramatic (Jonathan Wacks)
* Native American Film Festival – Best Picture (Jan Wieringa, George Harrison & Denis OBrien)
* Native American Film Festival – Best Director (Jonathan Wacks)
* Native American Film Festival – Best Actor (A Martinez)
;Nominated
* Sundance Film Festival – Grand Jury Prize (Jonathan Wacks)
* Independent Spirit Awards – Best First Feature (Jan Wieringa, Jonathan Wacks, George Harrison & Denis OBrien)
* Independent Spirit Awards – Best Supporting Male (Gary Farmer)
* Independent Spirit Awards – Best Cinematography (Toyomichi Kurita)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 