In Shifting Sands: The Truth About Unscom and the Disarming of Iraq
 
{{Infobox Film
| name           = In Shifting Sands: The Truth About Unscom and the Disarming of Iraq
| image          = 
| image_size     = 
| caption        = 
| director       = Scott Ritter
| producer       = Alex Cohn
| writer         = Alex Cohn Scott Ritter Scott Rosann
| narrator       = 
| starring       = Tariq Aziz Rolf Ekeus Scott Ritter
| music          = Eben Levy
| cinematography = Dan Lehrecke John Millieghta Mark Niuewenhof
| editing        = Jed Factor
| distributor    = Five Rivers M&L Banks
| released       =  
| runtime        = 92 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 United Nations weapons inspector in Iraq from 1991 to 1998.  These inspections were in search of "weapons of mass destruction" during the later years of the regime of Saddam Hussein.

The film was completed and distributed for theatrical release prior to the 2003 invasion of Iraq.

==See also==
*Iraq and weapons of mass destruction
*Scott Ritter
*United Nations Special Commission

==External links==
* 
* 
* 

 
 
 
 
 
 
 

 
 