Veerabhadra (film)
{{Infobox film
| name           = Veerabhadra
| image          =
| image_size     = 
| caption        =
| writer         = Marudhuri Raja  
| story          = Anjaneya Pushpanand
| screenplay     = Anjaneya Pushpanand   A. S. Ravi Kumar Chowdary
| producer       = Ambica Krishna   Ambica Ramanjaneyulu
| director       = A. S. Ravi Kumar Chowdary Sada
| music          = Mani Sharma
| editing        = Gautham Raju
| cinematography = Ram Prasad
| studio         = Ambika Cinema Productions 
| released       =  
| runtime        = 2:48:25
| country        =   India
| language       = Telugu
| budget         =
| gross          =
}}

 Sada in the lead roles and music composed by Mani Sharma. The film recorded as flop at box office.         

==Plot==
Murali (Balakrishna) is pursued by the brothers of Peddaraju (Sayaji Shinde), who is cooling his heels in jail. We are not told why they are look after him. Murali is the loving brother of a handicapped woman, whom he carries to the college everyday. Of course, Murali has fun escapades with a girl in his neighborhood (Sada). And slowly but surely we are let into the past of Murali and as to what happened between him and the arch villain. It had something to do with Peddarajus sister (Tanushree Dutta). And in the end, all is well, and you know well.

==Cast==
{{columns-list|3|
* Nandamuri Balakrishna as Murali
* Tanushree Dutta as Sada as Ashta Lakshmi
* Prakash Raj 
* Sayaji Shinde as Peddaraju
* Rajan P Dev Ajay 
* Bharat 
* Riaz Khan 
* Brahmanandam 
* Dharmavarapu Subramanyam  Venu Madhav
* M. S. Narayana  Kondavalasa 
* Lakshmipathi 
* Gundu Hanumantha Rao 
* Gundu Sudarshan 
* Sivaji Raja 
* Narra Venkateswara Rao 
* Raghunath Reddy
* Vijaya Rangaraju Ponnambalam
* Rallapalli  Padmanabham 
* Suthi Velu 
* KK Sarma
* Kallu Chidambaram
* Gowtham Raju
* Bandla Ganesh 
* Vinod
* Prasanna Kumar 
* Surya Prakash 
* Chitti
* Kovai Sarala 
* Rama Prabha 
* Shobha Rani 
* Sana 
* Haritha 
* Subhashini 
* Ooma
* Sony 
* Deepanjali 
* Sabiha 
* Richa 
* Tejaswini
}}

==Soundtrack==
{{Infobox album
| Name        = Veerabhadra
| Tagline     = 
| Type        = film
| Artist      = Mani Sharma
| Cover       = 
| Released    = 2006
| Recorded    = 
| Genre       = Soundtrack
| Length      = 29:22
| Label       = Supreme Music
| Producer    = Mani Sharma
| Reviews     = Style    (2006)  
| This album  = Veerabhadra   (2006)
| Next album  = Pokiri   (2006)
}}

Music composed by Mani Sharma. Music released on Supreme Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 29:22
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Janam Kosam
| lyrics1 = Bhashasri
| extra1  = Shankar Mahadevan
| length1 = 5:05

| title2  = Abbabba
| lyrics2 = Bhaskarabhatla SP Balu, Chitra
| length2 = 4:52

| title3  = Aa Eedukondalu  
| lyrics3 = Bhaskarabhatla
| extra3  = Tippu (singer)|Tippu, Lenina Chowdary
| length3 = 4:59

| title4  = Jujibeelallo
| lyrics4 = Chinni Charan
| extra4  = Kay Kay|KK, Mahalakshmi Iyer
| length4 = 4:49

| title5  = Sirimalli
| lyrics5 = Sai Harsha 
| extra5  = Mallikarjun,Sri Vardhini
| length5 = 4:58

| title6  = Boppayi Boppayi  
| lyrics6 = Chinni Charan   Sujatha
| length6 = 4:39
}}

==Others== Hyderabad

==References==
 
 

 
 
 
 


 