Maryada Ramanna
 
 
{{Infobox film
| name           = Maryada Ramanna
| image          = Maryada_Ramanna.JPG
| alt            =  
| caption        = Release poster
| director       = S. S. Rajamouli
| producer       = Shobu Yarlagadda Prasad Devineni
| writer         = S. S. Kanchi
| screenplay     = S. S. Rajamouli
| story          = S. S. Kanchi
| starring       =  
| music          = M. M. Keeravani
| cinematography = C. Ramprasad
| choreography   = Prem Rakshith, Swarna, Lalith, Johny
| editing        = Kotagiri Venkateswara Rao
| studio         = Arka Media Works
| distributor    = Arka Media Works
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =    
}}
 Telugu comedy Sunil and Sunil as a hero.  The film opened to favourable reviews by critics and turned out to become a super hit.  
 Kannada as Komal Kumar Bengali as Hindi as Tamil as Santhanam and Ashna Zaveri in the lead roles. It is being remade in Malayalam as Ivan Maryadaraman with Dileep (actor)|Dileep. “Anatagaitekoso” which is Maryada Ramanna with Japanese subtitle will be on screen on 26th July 2014 in Japan.

==Plot==
Faction feud in Andhra Pradesh results in the death of Ramineedus (V. Nagineedu) brother and he, along with his two sons Mallasuri (Supreeth Reddy) and Baireddy (Venugopal), vow for revenge. Time turns 28 years and comes to Hyderabad, India|Hyderabad. Here lives Ramu (Sunil (actor)|Sunil), an innocent and somewhat unlucky guy whose parents are no more and is ousted out of his job. His life takes a turn with a correspondence that he has got five acres of land in his hometown in Andhra Pradesh. He decides to sell that land and sets off to the village. In this process, he also meets Aparna (Saloni Aswani) who is the daughter of Ramineedu. However, much to Ramus bad luck, he happens to be the son of the man who killed Ramineedus brother and he ends up in their home itself. He takes advantage of a tradition of their house that not even one drop of blood should fall inside the house.  Whether he comes alive out of their hands or not forms the rest of the story.

==Cast== Sunil ... Kovelamudi Ramu
* Saloni Aswani ... Aparna
* Nagineedu ... Ramineedu
* Supreeth Reddy ... Mallasuri
* Venugopal ... Baireddy
* Brahmaji ... Srikanth (Aparnas cousin)
* Ravi Teja ... bicycles voice
* Anuj Gurwara
* Subbaraya Sharma
* Rao Ramesh

==Production==
Rajamouli said "I decided that my next project would be Maryada Ramanna during the Magadheera shooting itself because it is a 1 and half year project that demands lot of physical labor and mental strain. I didnt want to commit another physically exhausting film immediately after Magadheera. Maryada Ramanna gave us time to recharge our batteries so that we could come up with another huge project." He also revealed the films plot on the films launch to minimize the expectations of the audience because of this films success.

==Soundtrack==
{{Infobox album
| Name = Maryada Ramanna
| Longtype =
| Type = Soundtrack
| Artist = M. M. Keeravani
| Cover = Maryada Ramanna ACD.jpg
| Released = 6 July 2010
| Recorded = 2010 Feature film soundtrack
| Length = 19:57 Telugu
| Label = Vel records
| Producer = M. M. Keeravani
| Reviews =
| Last album = Jhummandi Naadam (2010)
| This album = Maryada Ramanna (2010)
| Next album = Anaganaga O Dheerudu (2010)
}}
The audio was released at a function held at Shilpakala Vedika, Hyderabad. The launch of the audio release was webcast live on the internet, and it got a good response from internet viewers all over the world.   

Dasari Narayana Rao released the audio and handed over the first copy to K. Raghavendra Rao. The function was also attended by noted film fratenity like Junior NTR, Ravi Teja, Prakash Raj, VV Vinayak, Dil Raju, Prabhas, Sirivennela and other prominent cast and crew of the film. Meanwhile music director MM Keeravanis birthday was also celebrated at a star-studded function. 
{{Tracklist
| collapsed =
| headline = Tracklist
| extra_column = Artist(s)
| total_length = 19:57
| writing_credits =
| lyrics_credits = yes
| music_credits =
| title1 = Ammayi Kitiki Pakkana
| note1 =
| lyrics1 = Anantha Sreeram
| extra1 = N. C. Karunya|Karunya, Chaitra
| length1 = 3:37
| title2 = Udyogam Oodipoyind
| note2 =
| lyrics2 = Ramajogayya Sastry Ranjith
| length2 = 3:45
| title3 = Telugammayi
| note3 =
| lyrics3 = Anantha Sreeram
| extra3 = M. M. Keeravani & Geetha Madhuri
| length3 = 4:05
| title4 = Raaye Raaye
| note4 =
| lyrics4 = Chaitanya Prasad
| extra4 = Raghu Kunche & Geetha Madhuri
| length4 = 4:21
| title5 = Parugulu Thiyy
| note5 =
| lyrics5 = Sirivennela Sitaramasastri
| extra5 = S. P. Balasubrahmanyam
| length5 = 4:10
}}

==Reception== Sunil is Saloni looks pretty and is convincing too. Nagineedu portrays the role brilliantly. There are quite a few others like Brahmaji, Anuj Gurwara and Rao Ramesh who perform well." 

==Box office==
The film grossed   in its first weekend of release in the United States.  The film collected a share of   (after tax and theatre rentals) within 7 days in India.  Over all the film managed to gross about   and was one among the top grossers of the year 2010. 

==Accolades==
;Nandi Awards  Best Popular Feature Film Providing Wholesome Entertainment – Shobha Yarlagadda, Prasad Devineni Best Villain – V. Nagineedu Best Male Playback Singer – M. M. Keeravani for the song "Teluguammayi" Special Jury Sunil

==References==
 

==External links==
*  

 

 
 
 
 
 
 