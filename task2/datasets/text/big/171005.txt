Henry V (1989 film)
 
 
{{Infobox film
| name           = Henry V
| image          = Henry v post.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Kenneth Branagh
| producer       = Bruce Sharman
| screenplay     = Kenneth Branagh
| based on       =  
| narrator       = Derek Jacobi
| starring       = Kenneth Branagh Paul Scofield Derek Jacobi Ian Holm Emma Thompson Alec McCowen Judi Dench Christian Bale 
| music          = Patrick Doyle
| cinematography = Kenneth MacMillan
| editing        = Michael Bradsell
| studio         = BBC Films Renaissance Films
| distributor    = The Samuel Goldwyn Company   Curzon Film Distributors  
| released       =  
| runtime        = 137 minutes  
| country        = United Kingdom
| language       = English French
| budget         = $9 million  . Box Office Mojo. Retrieved 20 January 2012. 
| gross          =  $10,161,099 
}}
 play of King Henry V of England. The film stars Branagh in the title role with Paul Scofield, Derek Jacobi, Ian Holm, Emma Thompson, Alec McCowen, Judi Dench, Robbie Coltrane, Brian Blessed, and Christian Bale in supporting roles.
 Academy Award Best Costume directorial debut, Best Actor Best Director.

==Plot==
The film begins with Chorus, in this case an individual in modern dress, introducing the subject of the play.  He is walking through an empty film studio and ends his monologue by opening the doors to begin the main action.  Chorus reappears several times during the film, his speeches helping to explain and progress the action.

The following act divisions reflect the original play, not the film.

===Act 1=== Archbishop of King Henry noblemen Thomas Exeter and Ralph Neville, 1st Earl of Westmorland|Westmoreland, the clergymen manage to persuade Henry to declare war on France if his claim on the French crown is denied.
 the Dauphin. The Dauphins condescending response takes the form of the delivery of a chest of tennis balls.  Exeter, who opens the chest, is appalled, but Henry at first takes the insult calmly. He goes on to state his determination to attack France, dismisses the ambassador and starts to plan his Military campaign|campaign.

===Act 2===
Henry tricks three high-ranked traitors into pronouncing their own sentence by asking advice on the case of a man who shouted insults at him in the street. When they recommend that he show no mercy to this minor offender, the King reveals his knowledge of their own sedition; they draw their daggers, but are quickly subdued by Henrys loyal nobles.  Exeter arrests them for high treason and Henry orders their execution before crossing the English Channel.  
 	 Charles VI, the Constable of France are worried because of Henrys martial ancestors and their previous English invasions. Exeter arrives in full plate armour|armor. He informs them that Henry demands the French crown and is prepared to take it by force if it is withheld, and delivers an insulting message to the Dauphin. King Charles tells Exeter he will give him a reply the following day.

===Act 3===
King Henry delivers a morale-boosting speech to his troops and attacks the walled city of Harfleur. When the Dauphin fails to relieve the city in time, the governor surrenders in return for Henrys promise to do Harfleurs population no harm. Henry orders Exeter to repair its fortifications.

Katharine asks her lady-in-waiting Alice to teach her some basics in English. Correct English pronunciation is very hard for her to learn but she is determined to accomplish it, even though some words sound un-ladylike to her. In a silent moment, Katharine watches her father and his courtiers and notes how worried they appear. King Charles finally orders his nobles to engage Henrys troops, halt their advance, and bring Henry back a prisoner.

The English troops struggle toward Calais through foul weather and sickness; Bardolph is hanged for looting a church. The French ambassador Mountjoy arrives and demands Henry pay a ransom for his person or place himself and his entire army at risk. Henry refuses, replying that even his reduced and sickly army is sufficient to resist a French attack.

===Act 4=== Gloucester and John of Lancaster, 1st Duke of Bedford|Bedford, together with Sir Thomas Erpingham, Henry decides to look into the state of his troops and wanders his camp in disguise. He meets Pistol, who fails to recognize him. Soon afterwards, he encounters a small group of soldiers, including Bates and Williams, with whom he debates his own culpability for any deaths to follow. He and Williams almost come to blows, and they agree to duel the day after, should they survive. When Williams and his friends leave the King alone, Henry breaks into a monologue about his burdens and prays to God for help.
 archery and code of pages and setting fire to the English tents. Henry and his officer Fluellen come upon the carnage and are still appalled when Mountjoy delivers the French surrender.

Henry returns Williams glove, this time out of disguise, and William is shocked to learn that the man he was arguing with the night before was King Henry himself.

The act ends with a four-minute long tracking shot,  as Non nobis is sung and the dead and wounded are carried off the field.

===Act 5=== Henry VI.

==Cast==
 
 
* Derek Jacobi as Chorus Henry V, King of England Duke of Gloucester, brother to the King James Larkin Duke of Bedford, brother to the King Duke of Exeter, uncle to the King Archbishop of Canterbury Bishop of Ely James Simmons as Edward of Norwich, 2nd Duke of York Paul Gregory Earl of Westmoreland Earl of Warwick
* Tom Whitehouse as John Talbot, 1st Earl of Shrewsbury Earl of Cambridge Lord Scroop Sir Thomas Grey Sir Thomas Erpingham, officer in Henrys army Daniel Webb as Gower, an English officer in Henrys army
* Ian Holm as Fluellen, a Welsh officer in Henrys Army
* Jimmy Yuill as Jamy, a Scottish Officer in Henrys army
* John Sessions as Macmorris, an Irish officer in Henrys army
* Shaun Prendergast as Bates, soldier in Henrys army Pat Doyle as Court, soldier in Henrys army Michael Williams as Williams, soldier in Henrys army
* Christian Bale as Robin the Luggage-Boy
 
* Robbie Coltrane as Sir John Falstaff, former friend of the King
* Richard Briers as Bardolph (Shakespeare character)|Bardolph, former friend of the King and lieutenant in his army
* Geoffrey Hutchings as Corporal Nym|Nym, former friend of the King and corporal in his army
* Robert Stephens as Ancient Pistol|Pistol, former friend of the King and Ensign (using the old title "Ancient") in his army
* Judi Dench as Mistress Quickly, an innkeeper
* Paul Scofield as Charles VI of France the Dauphin Duke of Orleans Duke of Berry Duke of Bretagne Duke of Burgundy Constable of France
* Colin Hurley as Grandpre, a French Lord
* Emma Thompson as Catherine of Valois|Katharine, daughter of Charles VI
* Geraldine McEwan as Alice, a Lady attending on Katharine
* David Lloyd Meredith as the Governor of Harfleur
* Christopher Ravenscroft as Mountjoy, a French herald
* David Parfitt as Messenger
* Mark Inman as 1st Soldier
* Chris Armstrong as 2nd Soldier
* Calum Yuill as a child
 

==Production==

===Screenplay=== flashbacks using Part 2 in which Henry interacts with the character of Falstaff, who, in Shakespeares Henry V, is never seen, merely announced to be deathly ill in Act 2 Scene 1, and dead in Act 2 scene 3. The scenes involve a brief summary of Henrys denouncement of Falstaff primarily with lines from Act 2, Scene 4 of Henry IV part 1 and a brief though important utterance of Henrys final repudiation of Falstaff in Part 2, "I know thee not, old man." The film also uses Falstaffs line "do not, when thou art King, hang a thief" from Henry IV Part 1 but gives it to Bardolph, in order to highlight the poignancy when Henry later has Bardolph executed.

===Filming===
Henry V was made on an estimated budget of $9 million.   The film was produced by Bruce Sharman with the British Broadcasting Corporation and Branaghs company Renaissance Films.  Principal photography commenced on 31 October 1988 and concluded 19 December the same year.  Sixty percent of production was shot on sound stages at Shepperton Studios, while many of the battle sequences were shot on fields adjacent to the Shepperton complex. 

===Style=== 1944 film realistic than that of Oliviers. For example, his film avoids Oliviers use of stylized sets, and, where Olivier staged the Battle of Agincourt on a sunlit field, Branaghs takes place amid rain-drenched mud and gore.  Nearly all of the scenes involving the comic characters were also staged as drama, rather than in the broad, more slapstick way in which Olivier staged them, because Branagh felt that modern audiences would not see the humour in these scenes.

While the text of the Chorus monologues are the same, the setting for them has been adapted to reflect the nature of the motion picture adaptation of the play.  Unlike the other performers, who are dressed in clothing contemporary to the actual Henry V to reflect their characters, the Chorus is dressed in modern 20th century clothing.  The opening monologue, originally written to compensate for the limitations of on stage theater to represent the historical scenes presented, is delivered on an empty motion picture sound stage with unfinished sets.  The other chorus monologues are delivered on location where the relevant action is taking place.  In all cases, the chorus speaks directly to the camera, addressing the audience.

===Music===
{{Infobox album|  
| Name        = Henry V
| Type        = Soundtrack
| Artist      = Patrick Doyle
| Cover       =
| Recorded    = 1989
| Released    = 8 November 1989
| Genre       = Soundtrack
| Length      = 59:08
| Label       = EMI Records
}}
{{Album ratings
| rev1 = AllMusic
| rev1Score =     Filmtracks
| rev2Score =    
}}
The score to Henry V was written by then first-time film composer Patrick Doyle. It was performed by the City of Birmingham Symphony Orchestra and conducted by Simon Rattle.  The soundtrack was released 8 November 1989 through EMI Classics and features fifteen tracks of score at a running time just under an hour.   Patrick Doyle also appeared in Henry V as Court (credited as Pat Doyle), who is the first soldier to begin singing "Non Nobis, Domine" following the conflict at Agincourt. 

# "Opening Title/O! for a Muse of Fire" (3:34)
# "King Henry V Theme/The Boars Head" (2:46)
# "The Three Traitors" (2:03)
# "Now, Lords, for France!" (2:40)
# "The Death of Falstaff" (1:54)
# "Once More Unto the Breach" (3:45)
# "The Threat to the Governor of Harfleur/Katherine of France/The March to Calais" (5:51)
# "The Death of Bardolph" (2:22)
# "Upon the King" (4:50)
# "St. Crispins Day/The Battle of Agincourt" (14:13)
# "The Day is Yours" (2:34)
# "Non Nobis, Domine" (4:09)
# "The Wooing of Katherine" (2:24)
# "Let This Acceptance Take" (2:50)
# "End Title" (2:35)

Doyle was later awarded the 1989 Ivor Novello Award for Best Film Theme for "Non Nobis, Domine". 

==Archives==
Online versions of the digitized script and storyboards from the film are part of the Renaissance Theatre Company Archive held at the University of Birmingham. 

==Release==

===Home media===
CBS/Fox Video released a pan and scan VHS edition in 1990 and a widescreen laserdisc edition in 1991. MGM Home Entertainment later released Henry V on DVD 18 July 2000, also preserving the widescreen format of the original theatrical presentation.  The film was released on Blu-ray on January 15, 2015.

==Reception==

===Critical response===
Henry V received near-universal critical acclaim for Branaghs Oscar-nominated performance and direction, for the accessibility of its Shakespearean language, and for its score by Patrick Doyle. It currently holds a rare 100% rating on Rotten Tomatoes    and a Metacritic score of 83 out of 100, based on 17 reviews—all positive.  Henry V also ranks #1 on the Rotten Tomatoes list of Greatest Shakespeare Movies, beating Akira Kurosawas Ran (film)|Ran (1985) and Branaghs own version of Hamlet (1996 film)|Hamlet (1996), respectively ranking in second and third place.   

  magazine also gave the film a positive review, calling Henry V "A stirring, gritty and enjoyable pic which offers a plethora of fine performances from some of the U.K.s brightest talents." 

===Box office===
The film grossed over $10 million in the U.S. and at the time of its widest release played on 134 U.S. screens. 

===Accolades===
{| class="wikitable plainrowheaders" style="font-size: 95%;"
|- Award
! Category
! Recipients and nominees Result
|-
| rowspan=3 | Academy Awards Best Actor
| Kenneth Branagh
|  
|- Best Costume Design
| Phyllis Dalton
|  
|- Best Director
| Kenneth Branagh
|  
|-
| rowspan=6 | British Academy Film Awards Best Actor in a Leading Role
| Kenneth Branagh
|  
|- Best Cinematography
| Kenneth MacMillan
|  
|- Best Costume Design
| Phyllis Dalton
|  
|- Best Direction
| Kenneth Branagh
|  
|- Best Production Design
| Tim Harvey
|  
|- Best Sound
| Campbell Askew, David Crozier, Robin ODonoghue
|  
|- Chicago Film Critics Association Awards
| Best Foreign Language Film
| rowspan=2 | Kenneth Branagh
|  
|- Best Actor
|  
|-
| rowspan=3 | European Film Awards Best Actor
| rowspan=3 | Kenneth Branagh
|  
|- Best Director
|  
|-
| Best Young Film
|  
|-
| Evening Standard British Film Awards
| Best Film
| Kenneth Branagh
|  
|- Italian National Syndicate of Film Journalists
| European Silver Ribbon
| Kenneth Branagh
|  
|-
| National Board of Review of Motion Pictures Best Director
| Kenneth Branagh
|  
|-
| New York Film Critics Circle Awards
| Best New Director
| Kenneth Branagh
|  
|-
| Sant Jordi Awards
| Best Foreign Actor
| Kenneth Branagh
|  
|-
|}

==See also==
* List of historical drama films
* List of William Shakespeare film adaptations

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 