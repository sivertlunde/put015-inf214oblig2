Aunt Clara (film)
{{Infobox film
| name           = Aunt Clara
| image          = "Aunt_Clara".jpg
| image_size     = 
| caption        = UK theatrical poster
| director       = Anthony Kimmins
| producer       = Anthony Kimmins Colin Lesslie Kenneth Horne
| narrator       = 
| starring       = Ronald Shiner Margaret Rutherford A. E. Matthews Fay Compton
| music          = Benjamin Frankel
| cinematography = C.M. Pennington-Richards
| editing        =  Gerald Turney Smith
| studio         =  Colin Lesslie Productions
| distributor    = British Lion Film Corporation
| released       = 22 November 1954
| runtime        = 84 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Aunt Clara is a 1954 British comedy film starring Margaret Rutherford as a woman who inherits a number of shady businesses from a relative. Ronald Shiner, A. E. Matthews, and Fay Compton are also featured. The film was based on the novel of the same name by author Noel Streatfeild, and directed by Anthony Kimmins for London Films. 

==Plot==
Clara Hilton (Margaret Rutherford), is a puritanical old lady whose eccentric uncle dies and leaves her five racing greyhounds, a broken-down pub, and a successful brothel. When confronted with her property, Clara is outraged, but shrewdly finds a way to convert the tainted money to good use for a children’s holiday fund. Clara also keeps Simon’s outspoken valet Henry Martin (Ronald Shiner) on to assist her.

==Cast==
* Ronald Shiner as Henry Martin
* Margaret Rutherford as Clara Hilton
* A. E. Matthews as Simon Hilton
* Fay Compton as Gladys Smith Nigel Stock as Charles Willis Jill Bennett as Julie
* Reginald Beckwith as Alfie Pearce
* Raymond Huntley as Maurice Hilton
* Eddie Byrne as Fosdick
* Sid James as Honest Sid
* Diana Beaumont as Dorrie
* Garry Marsh as Arthur Cole
* Gillian Lind as Doris Hilton
* Ronald Ward as Cyril Mason
* Jessie Evans as Lily Pearce

==Critical reception==
Britmovie wrote the film "fails to catch fire despite its undoubted charm. Margaret Rutherford plays the eponymous lead but for once her dotty spinster persona is understated and the film contains a suffocating melancholic tone that only resolves itself at the films moving closure";  while TV Guide observed "a charming film dotted with cameos by noted British comics." 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 


 