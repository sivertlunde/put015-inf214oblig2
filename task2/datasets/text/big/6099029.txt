Thammudu (film)
 
{{Infobox film
| name           = Thammudu
| image          =
| caption        =
| writer         = Chintapalli Ramana  
| story          = P.A. Arun Prasad
| screenplay     = P.A. Arun Prasad
| producer       = Burugupalli Sivaramakrishna
| director       = Arun Prasad P.A.|P.A. Arun Prasad
| starring       = Pawan Kalyan Preeti Jhangiani Aditi Govitrikar
| music          = Ramana Gogula
| cinematography = Madhu Ambatt
| editing        = Marthand K. Venkatesh
| studio         = Sri Venkateswara Art Films
| released       =  
| runtime        = 162 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}   Hindi Movie Badri and Yuvaraja by noted director Puri Jagannath. The film recorded as Industry Hit at box office. 

==Plot== Raja Krishnamoorthy) always scolds him but his brother Chakri (Achyuth) who is a boxer, and friend Jaanu (Preeti Jhangiani) dote him. Meanwhile he falls in love with Lovely (Aditi Gowitrikar|Aditi) and for this he keeps on telling lies to her that he is very rich. Jaanu in the middle truly loves Subrahmanyam and gives him everything he asks her for thinking that he is doing these for her but he does it for lovely. The incidents that led Subrahmanyam to change, the way he realised the true love of Jaanu and the final twist of him becoming a boxing champion is the real plot to be seen.

==Cast==
* Pawan Kalyan as Subbu /Subrahmanyam/Subhash
* Preeti Jhangiani as Janaki aka Janu Aditi as Lovely
* Bupinder Singh as Rohit
* Achyuth as Chakri
* Brahmanandam as Avadhanulu (telugu lecturer) Ali as Kondarama Manoharan
* Tanikella Bharani as Druker Mallikarjuna Rao as Malli Chandra Mohan as Kutumba Rao
* Raja Krishnamoorthy as Vishwanadh
* Raghunath Reddy
* Varsha as Shanthi

==Soundtrack==
{{Infobox album
| Name        = Thammudu
| Tagline     = 
| Type        = film
| Artist      = Ramana Gogula
| Cover       = 
| Released    = 1999
| Recorded    = 
| Genre       = Soundtrack
| Length      = 34:13
| Label       = Aditya Music
| Producer    = Ramana Gogula
| Reviews     =
| Last album  = Premante Idera   (1998)  
| This album  = Thammudu   (1999)
| Next album  = Yuvaraju   (2000)
}}

Music composed by Ramana Gogula. All songs are blockbusters. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 34:13
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Made in Andhra Chandra Bose
| extra1  = Ramana Gogula
| length1 = 6:57

| title2  = Pedavi Datani Sirivennela Sitarama Sastry Sunitha
| length2 = 4:36

| title3  = Yedola Vundi
| lyrics3 = Surendra Krishna
| extra3  = Ramana Gogula
| length3 = 5:10

| title4  = Vayyari Bhama
| lyrics4 = Sirivennela Sitarama Sastry
| extra4  = Ramana Gogula
| length4 = 5;16

| title5  = Travelling Soldier
| lyrics5 = Ramana Gogula 
| extra5  = Ramana Gogula
| length5 = 4:27

| title6  = Kalakalu 
| lyrics6 = Chandra Bose SP Balu
| length6 = 5:11}}

==Trivia==
* The movie is inspired from Aamir Khans movie Jo Jeeta Wohi Sikandar.

==Others== Hyderabad
==References==
 

==External links==
*  

 
 
 
 
 