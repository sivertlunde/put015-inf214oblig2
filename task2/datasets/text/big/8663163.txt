Kya Love Story Hai
{{Infobox film
| name = Kya Love Story Hai
| image = Kya love story hai 1.jpg
| caption = Movie poster for Kya Love Story Hai Rahul Singh Sujata Rahul Rahul Singh Shyam Poonam Gipson
| director = Lovely Singh
| producer = Adlabs Films Ltd V R Entertainers
| music Director = Songs:  
| lyrics = Shabbir Ahmed
| Overseas distributor = Studio 18
| released =  
| language = Hindi
| music = Pritam
| Running Time = 125 minutes  
| budget = App. 60-70 million
}}

Kya Love Story Hai ( ) is a 2007 Bollywood film starring Ayesha Takia and Tusshar Kapoor in the lead roles. It is directed by debutant Lovely Singh and produced by Adlabs Films Ltd and V R Entertainers. The film was shot in Cape Town, South Africa. The tagline of the movie is What if... Simplicity is all you need

== Cast ==
* Ayesha Takia . .. Kajal Mehrahh
* Tusshar Kapoor ... Arjun
* Karan Hukku ... Ranveer Oberoi
* Rajesh Khattar ... Kajals Father
* Sujata Kumar ... Ranveers Mom Rahul Singh ... Chiku (Arjuns Friend)
* Shyam Mashalkar ... Romeo (Arjuns Friend)
* Poonam Gipson ... Suzzane (Kajals Friend)
* Liza Ackermann... Vanilla (Chikus love interest)
* Shahid Kapoor... as Special Appearance in the film 
* Kareena Kapoor ... Special Appearance on Its Rocking

== Characters ==

=== Kajal (age 20)===
Ayesha Takia plays Kajal, a young independent girl who knows exactly what she wants. She lost her mother at a young age and her father was busy traveling abroad on business. Shes studying and works part-time as well. Shes a hardworking girl of todays generation with that innate drive and vigor.    
   
Kajal has always dreamed of a particular guy in her life who has to be someone who creates his own destiny. Someone who doesnt take on an inheritance and enjoy on the riches but is self made. She met Arjun who is good-for-nothing type of a guy. So when she actually comes to know him more and more, she realizes what she wanted in life was not right.

=== Arjun (age 25)===
Tushaar Kapoor plays Arjun, A young man whose parents are no more, he has inherited a fortune from them and is now living a carefree life sans any worries. He is not concerned about his future since he has a glorious present to live.    
   
He experiences love-at-first-sight with Kaajal but as he knows more about her he got to know that he is not the perfect man for her.

=== Ranveer (age 32)===
Karan Hukku plays Ranveer, a guy who is very focused and hardworking. He lost his father at a very young age and so he has seen the hard part of life. He always thinks about his work. He doesn’t know how to party or have fun.    
   
Kajal falls in love with Ranveer because he is the ideal guy for her who is very hard working. He is much focused and his main agenda is to make his mother happy.

===Vanilla (age 28)===
Liza Ackermann plays Vanilla the love interest of Chiku, who is Arjuns friend. Unfortunately, Vanilla already has a fiance, which leads to a comical fight in a nightclub where Arjun steps in to save the day.

== Crew ==
* Director: Lovely Singh Rahul Singh Rahul Singh
* Dialogue: Niranjan Iyengar
* Producer:N.R Pachisia
* Associate Producer: Pravin Talreja
* Music Director: Pritam
* lyrics: Shabbir Ahmed
* music label: T-Series
* Editor: Steven Bernard
* Cinematography: Rajeev Shrivastava
* Choreography: Rajeev Soorti
* Singers: Shaan (singer)|Shaan, Alisha Chinoy, Sonu Nigam, Kunal Ganjawala, Joy, Jolly Mukherjee, Zubeen Garg
* Costume Designer: Manish Malhotra
* Makeup: Vijay Paswan (Ayesha Takia), Jitendra Sawant (Tusshar Kapoor), Raju Babu
* Hair Dresser: Rekha Patil (Ayesha Takia), Jatashika Bandari (Tusshar Kapoor)
* Line Producer: Abhisekh Goel
* Graphics: Rajnish Mishra
* Overseas distributor: Viacom 18 Motion Pictures

== Music == Naya Daur.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Duration !! Picturised on
|-
|I Miss You Every Day Shaan
| 5.32
| Ayesha Takia and Tusshar Kapoor
|-
| Its Rocking
| Alisha Chinoy
| 4.59
| Kareena Kapoor
|-
| Gumsum Hai Dil Mera
| Sonu Nigam
| 5.14
| Ayesha Takia and Tusshar Kapoor
|-
| Aye Khuda
| Kunal Ganjawala
| 4.17
| Ayesha Takia, Tusshar Kapoor and Karan Hukka
|-
| Deewana Teri Aankhon Ka Jojo
| 3.45
| Ayesha Takia and Karan Hukka
|-
| Jeena Kya Tere Bina
| Zubeen Garg
| 6.05
| Ayesha Takia, Tusshar Kapoor and Karan Hukka
|-
|I Miss You Every Day - Remix Shaan
| 5.11
|
|-
| Jeena Kya Tere Bina - Remix
| Zubeen Garg
| 5.45
|
|}

==Box office==

===U.K Business===
SOURCE :  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Weekend !! Position !! Weekend Gross !! Cumulative Gross !! # of Theaters
|-
| 20–22 April 2007
| 22
| £26,297
| £26,297
| 17
|-
| 27–29 April 2007
| 33
| £5,236
| £45,797
| 15
|-
| 4–6 May 2007
| 55
| £924
| £53,717
| 02
|-
| 11 – 13 May 2007
| 80
| £540 
| £55,181
| 01
|-
|}

===India Business===
SOURCE :  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Week No. !! Position !! Week Gross !! Total Gross !! # of Theaters
|-
| 1.
| 01
| Rs. 28,40,00,00
| Rs. 28,40,00,00
| 339
|-
| 2.
| 05
| Rs. 2,70,00,00
| Rs. 31,10,00,00
| 100
|-
| 3.
| 10
| Rs. 20,00,00
| Rs. 31,30,00,00
| 10
|-
|}

==External links==
*  

 
 
 
 