College Kumaran
{{Infobox film
| name           = College Kumaran
| image          = College Kumaran.jpg
| caption        = 
| director       = Thulasidas
| producer       = Bency Martin
| writer         = Suresh Pothuwal
| narrator       = 
| starring       = Mohanlal Vimala Raman Balachandra Menon
| music          = Ouseppachan
| cinematography = Venugopal
| editing        = P. C. Mohanan
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =  4 crore  
}}
College Kumaran is a 2008 Malayalam film directed by Thulasidas, starring Mohanlal and Vimala Raman.  

== Plot ==
Kumaaran, or as students call him, Captain (Mohanlal), was the star of Mahatma college before he joined the army. Now an ex-military officer, Kumaaran is running the canteen of Mahatma College after a tragic incident that took his father and sisters life. 
Kumaaran is dear to all the students and he actively engages with them, whether in youth festivals, love affairs, fights, or campus tours. He even helps them bring teachers back from their private tuition classes to the college.

Madhavi (Vimala Raman), the colleges English professor, hates him because she saw Kumaaran getting involved in a fight with a street rowdy. Once during a campus tour, the bus gets involved in an accident, killing several students. Madhavi tells everyone that Kumaaran was the reason for the accident. The rest of the movie is about Kumaarans attempts to prove his innocence and how he regains the trust of his fellow students and of Madhavi.

== Cast ==
* Mohanlal	 ...	Kumaran
* Harisree Asokan	
* Kalasala Babu	 ...	Public prosecutor Janardhanan
* Saiju Kurup		
* T. P. Madhavan		
* Balachandra Menon
* Mukundan		
* Babu Namboothiri		
* Kiran Raj		
* Vimala Raman	 ...	Madhavi Menon
* Rizabawa Siddique
* Jagannatha Varma	 ...	Judge
* Suraj Venjaramood Baiju
* Nedumudi Venu Vijayaraghavan
* Ambika Mohan ... College Lecturer
* Sajitha Betti

== References ==

 

== External links ==
*  
* http://www.indiaglitz.com/channels/malayalam/preview/9356.html
* http://www.rediff.com/movies/2008/feb/04ssck.htm
* http://popcorn.oneindia.in/title/478/collage-kumaran.html

 
 
 


 