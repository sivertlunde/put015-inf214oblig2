Dame sobh
{{Infobox film
| name           = Dame sobh (دم صبح) / Day Break
| image          =
| caption        =
| director       = Hamid Rahmanian 
| producer      = Vahid Nikkhah-Azad,  Hamid Rahmanian
| writer         = Mehran Kashani, Hamid Rahmanian
| starring       = Hossein Yari, Zabi Afshar, Maryam Amirjallali, Hoda Nasseh, Atash Taghipour 
| music =  David Bergeau
| cinematography = Bayram Fazli
| editing = Ebrahim Saeedi 
| distributor =   
| released       = Iran
| runtime        = 84 min (85 min) 
| country        = Iran  Persian
|}}

Dame sobh ( , Dam-e Sobh, English title: Day Break) is an award-winning 2005 independent film from Iranian director Hamid Rahmanian. It depicts a man on death row whose execution is repeatedly delayed because the family of the victim does not appear for his execution (which is necessary for an execution to take place under Iranian law). Some parts of the film are filmed like a documentary (docudrama), with characters addressing the camera crew or looking into the camera, but most of the scenes, including frequent Flashback (narrative)|flashbacks, are filmed in the traditional style of a fictional film.

The film sets in with the written statement that "This film is based on true events"; later in the introduction of the film, it is added that it is "based on a story by Hamid Rahmanian". The cover of the DVD release by Film Movement describes the film as "based on a compilation of true stories" and adds that it was "shot inside Tehrans century-old prison". 

With few exceptions (e.g., Şafak in Turkey),  the film has been internationally screened under the title Day Break. In the United States, the film is not rated, but according to the cover of its DVD release is "not recommended for ages under 17 without parental permission". 

== Plot ==

Mansour Ziaee (  are not in the north of Tehran), where his family was farming sheep. Hoping for a better future, he convinced his parents and his wife to move to Tehran, but once there, faced difficulties keeping the job he had counted on. Although the crime he committed and its immediate antecedents are never directly shown, it appears that he killed someone, probably his employer, with a brick stone on a busy street in broad daylight. He is apprehended on the spot and later sentenced to death.
 Islamic law used in Iran, the family of the victim has the power to pardon the perpetrators life, and they have to be present at the execution. Ziaees execution had to be delayed twice already because the family failed to come, when the film sets in on the morning of the third date fixed for his execution. The camera follows him through the procedures before an execution can take place, but ultimately, the victims family again does not show up, and the execution is postponed for another 40 days.
 
While his fellow prison inmates celebrate the cancellation, the stressful uncertainty between life and death takes its toll, and Ziaee withdraws more and more from the world around him. He does not want to see his family anymore, who has moved back to Zir Ab because they felt lonely in Tehran, he attempts suicide and provokes to be send to solitary confinement. He finally finds back to some joy of life when his wife gives birth to their first child. Soon afterwards, however, his execution is appointed for the fourth time. After a haunting dream of finally being hanged, the film leaves Ziaee as he walks off to the room where he will either meet the victims family or be stood up yet another time.

== Critics == Variety wrote that it "works like a ticking time bomb". 

According to the Encyclopædia Britannica Online Britannica Book of the Year 2006, "Hamid Ramanians Dame sobh (“Daybreak”) was a harrowing study of a murderer awaiting the death penalty, which is by Islamic law the personal responsibility of the injured family". 

== Awards ==
The film was the winner of the Annonay International Film Festival (Annonay, France) and the Fajr Film Festival (Tehran). It was in the official selection of the Tribeca Film Festival (New York City) and the Toronto International Film Festival. 

== References ==
 

==External links==
* 
*  at  
* 

 
 
 
 
 