Boat Builders (film)
 
{{Infobox film
| name = Boat Builders
| image =
| image size =
| alt =
| caption =
| director = Ben Sharpsteen
| producer = Walt Disney
| writer =
| starring =
| music = Leigh Harline Oliver Wallace
| cinematography =
| editing = Walt Disney Productions RKO Radio Pictures
| released =  
| runtime = 7 minutes
| country = United States
| language = English
| budget =
| gross =
}}
Boat Builders is an animated short film produced by Walt Disney, distributed by RKO Radio Pictures and released on February 25, 1938. The film was directed by Ben Sharpsteen and animated by Frenchy de Trémaudan, Louie Schmittt, Chuck Couch, Eddie Strickland, Clyde Geronimi, Paul Satterfield, Archie Robin, Don Patterson. " ". www.bcdb.com, April 12, 2012 

==Plot== to christen the boat) just too hard on the boat.

==Release==
*1938 &ndash; Original theatrical release
*1957 &ndash; The Mickey Mouse Club, episode #3.24 (TV)
*c. 1972 &ndash; The Mouse Factory, episode #1.7: "Water Sports" (TV)
*1981 &ndash; "Mickey Mouse and Donald Duck Cartoon Collections Volume Three" (VHS)
*c. 1983 &ndash; Good Morning, Mickey!, episode #1 (TV)
*1989 &ndash; " " (VHS) The Magical World of Disney, episode #33.15: "Mickeys Happy Valentine Special" (TV)
*c. 1992 &ndash; Mickeys Mouse Tracks, episode #16 (TV)
*c. 1992 &ndash; Donalds Quack Attack, episode #34 (TV) The Ink and Paint Club, episode #39: "Minnie Mouse" (TV)
*2001 &ndash; " " (DVD)
*2007 &ndash; Reissued with 2D version of Meet the Robinsons (theatrical)
*2011 &ndash; Have a Laugh!, episode #25 (TV)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 

 