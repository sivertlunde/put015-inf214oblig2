Double Take (2009 film)
 
{{Infobox film
| name           = Double Take
| image          = Double Take film.jpg
| alt            =  
| caption        = 
| director       = Johan Grimonprez
| producer       = Nicole Gerhards Emmy Oost Hanneke M. van der Tas Denis Vaslin Tom McCarthy
| starring       = 
| music          = Christian Halten
| cinematography = Martin Testar
| editing        = Dieter Diependaele Tyler Hubby
| studio         = Zap-o-Matik
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Belgium Germany Netherlands
| language       = English
}} essay film, Tom McCarthy. The plot is set during the Cold War and combines both documentary and fictional elements. The protagonist is a fictionalised version of Alfred Hitchcock, who unwittingly gets caught up in a double take. The backdrop of the film charts the rise of the television in the domestic setting and with it, the ensuing commodification of fear during the cold war. 
 Sundance film Festival.

== Plot == The Birds, Hitchcock calls a twelve-minute break in order to answer a phone call in one of the universal studio buildings. After a foreboding encounter with a security guard, Hitchcock finds his way into a room similar to the tearooms in both the Chasens hotel in Los Angeles and the Claridges hotel in London. Here, Hitchcock and his doppelgänger meet.  The ensuing conversation between the two is characterized by personal paranoia and distrust where the younger Hitchcock is in deep fear of his older alter ego.

Intermittently returning to the room in which the menacing conversation between the two Hitchcocks proceeds,    the narrative takes a deathward path. Hitchcock and his doppelgänger regard each other with a mixture of revulsion and confusion. Regarding the aphorism that “if you meet your double, you should kill him”,    both Hitchcocks knowing how the encounter must end. “So, tell me, how would you like to die?” asks the older Hitchcock, sipping on a cup of coffee. All the while, Folgers coffee advertisements puncture the narration in the backdrop of the Cold War. By means of his double, Hitchcock the filmmaker realizes that he is going to die. Killed by the younger, television-making, version of himself.   

==Cast==
* Ron Burrage as Hitchcocks Double.  "For years, Ron impersonated Hitchcock in everything ranging from Robert Lepages Le Confessional (1995) (itself a remake), to soap and shampoo commercials, to guest appearances in music videos for Oasis, to introducing Hitchcock Presents on Italian television, to starring in a Japanese documentary about the life of the Master..."  However, Burrage shares much more with the real Alfred Hitchcock than his looks; from Hitchcocks pranks to his birthday (13 August). He is not only filling in for Hitchcock but literally taking over his role by introducing Tippi Hedren to the audience after the first screening of the newly restored print of The Birds in Locarno, that actually took place on 13 August. 

* Mark Perry as Hitchcocks Voice

* Delfine Bafort as the Hitchcocks blonde (the double of Eva Marie Saint and Tippi Hedren)

==Looking for Alfred==

 

In 2005, prior to making Double Take - which started as a casting,  Grimonprez shot the ten-minute  , Chris Darke, Thomas Elsaesser, Tom McCarthy, Jeff Noon and Slavoj Zizek. 

== Themes ==
The themes of Double Take are paranoia, falsehoods, contradictions and the rise of the culture of fear played out through the beginning of the television era. Six major themes seem to surface: 
* 1. Double Bottoms
* 2. The Commercial Break
* 3. Political Layaring
* 4. Reality versus Fiction
* 5. The Figure of the Double
* 6. Representation of Women

=== 1. Double Bottoms and multilayered metaphors    ===

The multilayered metaphors of Double Take explore not only the character of Alfred Hitchcock meeting his double, but also the eras society as a whole.  Hitchcock is cast as a paranoid history professor shadowed by an elusive double against the backdrop of the Cold War, played out through the television tube; he "says all the wrong things at all the wrong times while politicians on both sides desperately clamor to say the right things, live on TV."    The themes explored in Double Take are all rooted in the following comment by Ron Burrage - the Alfred Hitchcock lookalike: “People always do a double take when they see me.”  And as such the film’s exploration into paranoia is also a double take on the Cold War, a mirror of the fear mongering played over the TV tube.

=== 2. The Commercial Break ===
 
The five Folgers commercials for instant coffee that play throughout Double Take are referring to the commercial break of the television format, described by Hitchcock as "the enemy of suspense" and "designed to keep you from getting too engrossed in the story".  Moreover the commercials represent the exploration of the theme that fear and murder lurks in the domestic setting, “where it always belonged”. They also imply a yearning for successful falsehood.  The question of why the Folgers housewives wouldnt simply prepare real coffee refers to the films question why our culture yearns for successful imitation—for falsehood.  In this contradiction one act masquerades its opposite. The Folgers commercial is subverted in such a way that its message, “Drink Folgers,” becomes coded as part of a murder plot. 

=== 3. Political Layering, The Cold War & The Birds as metaphor ===

Double Take presents the   outsmarted Richard Nixon, "whose best retort to the Soviet leaders critiques of U.S. capitalism is to point to the latest in TV sets".    Double Take implies that the predominate purposes of the space race as well as the television were propaganda, "both individually and, to greatest effect, when acting together".    The infamous Kitchen Sink Debate mimics the conversation between Hitchcock versus Hitchcock, which mimics the man and woman debate in the kitchen during the Folgers commercials in turn.

The mirroring of Hitchcock versus Hitchcock (as Hitchcock frequently doubled himself as the storyteller in his films through his cameos),  suggests a similar doubling of Rixard Nixon and the young John F. Kennedy during the first televised presidential debates, the same Kennedy who finds his match in Khrushchev during the Cuban Missile Crisis, and finally Khrushchev versus Leonid Brezhnev just as Brezhnev is plotting Khrushchevs downfall. 

The paranoia in Hitchcocks work becomes an allegory for the kind of fear that became so normal during the Cold War.  The Birds (1963) propose the film as an allegory for television - which has, according to Hitchcock, "brought murder back into the home-where it always belonged",  and as the threat of missiles descending from the sky, suggesting a psychohistorical analogy between the fear of nuclear attack and Hitchcocks suspense. 
 

=== 4. Reality vs Fiction ===

Double Take plays with different genres against one another and with how fiction comes to stand for reality or the other way around.  It shows a recent history against a fiction even while it presents that history itself as an ongoing story of claustrophobic suspense.  In the end not only politicians but Hollywood as well are "invested in perpetuating a culture of fear".  Double Take also refers to the doubling of cinema versus television through an intimate fiction story versus a bigger political narrative as well as through the rivalry between cinema and its televisual double. This mirrors the plot that sets up Hitchcock the filmmaker versus Hitchcock the television-maker at a time television was taking over cinema. 

But Double Take adds a contemporary twist: "You think all the way through that cinema is going to be killed by television or television is going to kill cinema or America is going to kill Russia or Russia is going to kill America. But at the end, it’s the third one, the new one, the younger one, the YouTube version, that comes along and kills them all." 

=== 5. Cultural References: the Figure of the Double in Literature ===

Seeing ones own doppelganger is usually a bad omen, it might even be a "premonition of death".  The first narrated line during the Borgesian confrontation between Hitchcock young and old in Double Take launches the plotline and comes with a warning: “If you meet your double, you should kill him.”  This provocative statement one should kill ones identical suggest that doing so means nothing but self-protection or even self-preservation.  For it is believed that the double has no reflection in the mirror, it can be seen as a mirror itself: "because he performs the protagonists actions in advance, he is the mirror that eventually takes over."  Double Take is based on a similar plot.  Hitchcock himself gets eliminated in the first round of a Hitchcock lookalike competition. But all the participants of the competition áre Hitchcock, so "hes losing to himself".  Hitchcock studies are "proliferated to such a degree that there are many different Hitchcocks".  As Thomas Elsaesser notes that we now have a Hitchcock defined as Nietzschean and as Wittgeinsteinian, as Deleuzian and as Derridean, as Schopenhauerian and as many more contradictory things. In this way Hitchcock returns "as so many doubles of his own improbable self".   

Jose Luis Borges initial short story August 25, 1983 is based on Fyodor Dostoyevskys The Double  as well as on Tom McCarthys story Negative Reel in the book Looking For Alfred.    Authors like Adelbert von Chamisso, Hans Christian Andersen, Edgar Allan Poe and many more were inspired by the idea of the double as well.  The narration of Double Take is in fact already a double itself, for it is inspired by Borges novella The Other (1972). Borges rewrote his novella into Agustus 25, 1983 (1983), and it was the latter one that was reworked for Double Take. 

=== 6. Hitchcocks Belly Button, women and male hysteria ===

Hitchcock often portrays strong females leads, who according to Grimonprez mirror his own fears and phobias projected back onto the female character as a way to try to contain her, or even poison her.    Similarly, this male hysteria is also installed between man and man in Double Take.  Hitchcocks makes this confession in Double Take: "we always fell in love with our characters, thats why we killed them." Grimonprez claims that "Hitchcock wants the woman to embody his own desire, but his dreamwoman never redeems his anxiety precisely because she refuses to fit into that mould. The man then is ultimately faced with a split reality."    He is doubled, as it were. Just like the moment Sigmund Freud was confronted with his double during a train journey, when the door of the washing-cabinet swung back and Freud didnt recognize himself in the mirror. For a moment he believed someone entered his travelling compartment by mistake. He only realized that the man was nothing but his own reflection when he jumped up to show the stranger the right direction. Freud "thoroughly disliked his appearance".    Whereas this encounter with his double, an anxious feeling crept over him. According to Freud, "meeting ones double is an encounter with the uncanny, occurring at the boundaries between mind and matter and generating a feeling of unbearable terror."  The double, once a figure "endowed with life-supporting power",  transforms itself "into the opposite of what it originally represented.   It becomes "the uncanny harbinger of death"".  

In an interview with Karen Black, the last in a row of Hitchcock’s famous female protagonists who featured in Hitchcock’s final film Family Plot (1976), Black confirms to Grimonprez that the story about Hitchcock actually not having a belly button was true.    This story became part of the plotline in the film Hitchcock didn’t have a Belly Button. "If Hitchcock didnt have a belly button he might be a clone and there might actually be many doubles of the master, of which Ron Burrage was one", wrote Grimonprez. 

In Hitchcock didn’t have a Belly Button Black does expressions of Hitchcock. And as she is pretending to be Hitchcock she becomes his vocal double. Jodi Dean writes on his blog I Cite that we experience two people but one voice: "we hear the one who is speaking and know that it is him, but we also hear another - only rarely do we mistake one for the other, we know the difference".   

==Release & Critical Reception==

Double Take premiered in Europe at the Berlinale  and in the US at Sundance Film Festival|Sundance. The film was screened at several film festivals, including IDFA.  Double Take was released on DVD by Soda Pictures  in England and Kino Lorber  in the United States. It was acquired for TV release by ARTE in 2011.

Variety (magazine)|Variety described Double Take as "wildly entertaining",  the New York Times called it "the most intellectually agile of this year’s films"  and the Hollywood Reporter found the film "bracingly original", assuming that it "would have tickled Hitch himself".  John Waters mentioned Double Take as "My top 10!" in Art Forum and Screen described the film as "Ingenious, witty, virtuoso!". 

Double Take is part of the permanent collections of the Tate Modern (Artist Rooms) and Centre Pompidou, amongst others. It was screened at the New Directors/New Films festival (presented by The Film Society of Lincoln Center) and The Museum of Modern Art, MoMA.

== Awards ==

* Black Pearl Award for Best Documentary Director, Abu Dhabi International Film Festival  
* Grand Prize, New Media Film Festival, Los Angeles 
* Special Mention, Era New Horizons International Film Festival, Warsaw
* Special Mention, Image Forum Festival, Yokohama
* Film of the Month, Sight & Sound

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 