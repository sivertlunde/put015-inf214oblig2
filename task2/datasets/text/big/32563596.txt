The Monk (2011 film)
{{Infobox film
| name           = The Monk
| image          = Lemoineposter.jpg
| caption        = Promotional poster
| director       = Dominik Moll
| producer       =  Matthew Lewis Dominik Moll Anne-Louise Trividic
| starring       = Vincent Cassel Déborah François Joséphine Japy Catherine Mouchet
| music          = Alberto Iglesias
| cinematography = Patrick Blossier
| editing        = François Gédigier Sylvie Lager
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = France Spain
| language       = French
| budget         =
| gross          =
}} Matthew Lewis gothic novel of the same name. It chronicles the story of a Capucin Ambrosio (Vincent Cassel), a well-respected monk in Spain, concluding in his downfall. It was released in France on 13 July 2011.

==Plot==
An abandoned baby is taken in by a Spanish monastery, and raised as a monk, christened Capuchin Ambrosio. An exceptional preacher, Ambrosio is respected by all. One day a masked figure by the name of Valerio arrives at the monastery with his godfather, explaining the mask as necessary due to terrible burns inflicted on him in a fire that killed his parents. He expresses a desire to join the monastery which is initially shunned by several of the monks, however Ambrosio convinces them to not fear his differences, and to accept him. Valerio later reveals to Ambrosio that he is actually a woman by the name of Matilda, and Ambrosio orders her to leave. As a last request she asks for a rose from his rose garden, and upon reaching in to pluck one for her, Ambrosio is bitten by a giant centipede. As he becomes delirious from the effects of the venom, he is seduced by Matilda and compromises his faith. Another lady, Antonia, who had recently heard him preach comes to the monastery to ask that Ambrosio see her sick mother.  He complies and is told of a story by the sickly mother that she left a son Mateo that was lost years ago and this troubles her soul.  Driven by a carnal lust for Matilda, Ambrosio transgresses and he is soon found desiring the innocent Antonia now. He also sees apparitions of the pregnant Nun he condemned to death by turning her over to the prioress. Matilda recognizes Ambrosios new interest in the fair Antonia and uses magic spells to help the monk in his pursuit of her. She resists and her rape by him and discovery of her murdered mother (at the hand of Ambrosio) leads to her insanity.  As her mother dies, she utters the word "Mateo", indicating that Ambrosio now knows that he is the older BROTHER of Antonia that was lost years ago. Returning to the monastery, Ambrosio is delivered to the Inquisition where he is condemned to death by starvation; escaping only by selling his soul to the devil in an effort to save Antonia. It later emerges that Matilda is an instrument of Satan in female form.  The devil prevents Ambrosios final repentance...

==Cast==
* Vincent Cassel as Capucino Ambrosio
* Déborah François as Matilda 
* Joséphine Japy as Antonia
* Catherine Mouchet as Elvire
* Geraldine Chaplin as labbesse Sergi López as Le débauché 
* Jordi Dauder as Père Miguel 
* Roxane Duran as Soeur Agnes
* Frédéric Noaille as Lorenzo
* Javivi as Frère Andrés

==Reception==
Screen Daily praised the leading man, "Cassel exudes otherworldly gravitas and his singular looks are perfect for the role." The review continued to note that "Moll uses quaint touches like iris in/iris out and, via painterly photography, makes the most of the contrast between the cool inner sanctum and the sun-baked landscape." Although the reviewer felt that "three quarters of the film are delectably creepy", the finale only "peters out". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 