That Thing Called Tadhana
{{Infobox film
| name             = That Thing Called Tadhana
| image            = Thatthingcalledtadhana.jpg
| caption          = 
| director         = Antoinette Jadaone
| producer         = Bianca Balbuena Dan Villegas
| writer           = Antoinette Jadaone
| based on         = 
| cinematography   = Sasha Palomares
| editing          = Benjamin Gonzales Tolentino
| music            = Emerzon Texon
| starring         = Angelica Panganiban JM de Guzman
| studio           = 
| distributor      = Star Cinema
| released         =   (Cinema One Originals Film Festival)     (wide) 
| runtime          = 
| country          = Philippines
| language         =  
| budget           = 
| gross            = 
}}
 Cinema One Originals Film Festival where it earned top honors notably the Best Actress award for Panganiban. It is directed by critically acclaimed rookie director, Antoinette Jadaone  who had worked with Panganiban in the 2014 comedy flick, Beauty in a Bottle.
 Cinema Evaluation Board and given a Rated PG by the MTRCB.  

== Overview ==

=== Background ===
 film festival had been used up. Eventually, the crew asked their friends to contribute ₱500 each in exchange of being mentioned in the ending credits of the film, showing the passion behind-the-scenes.  

=== Plot ===

A woman, Mace Castillo (Angelica Panganiban) who was desperately struggling in unloading her excess baggage in an airport in Italy, crying over on how she can get an exact load limit with her baggage until a man came up, Anthony (JM de Guzman)who comes to her aid and offered a space in his luggage until they both had finally checked out and went back to their home country. At the Philippine Airport, the two decided not to take the next cab ride and instead go somewhere. Both of them ended up singing in a karaoke bar where they sang together the song Where do Broken Hearts Go and after a few drinks, Mace told Anthony that she wanted to go to Baguio City. On their way, Mace woke up surprised that she cant believe herself asking Anthony to go with him and in Baguio to begin with. In the middle of the travel, they stopped over a terminal station and Anthony asked Mace to go down the bus and join him to eat some snacks. Anthony bought hotdogs and sodas, where Mace assumingly squeezed ketchup on Anthonys food and apologized for doing it, because it was something that her ex-boyfriend likes her to do. Anthony mentioned several different things and Mace had always something in it that reminds her of her ex. Mace remembered a friend whos deal was to pay another friend for every time she mentions her ex. For which, both decided to go for 100 pesos every time Mace mentions her Ex. Then as they reached their destination, they went to an Art Exhibit. While Anthony is staring at a painting, Mace discovers that Anthony has the passion for Arts, specifically in painting. They went out to eat lunch and in the conversation, Mace reveals that she also had a thing for writing during her college years, which was titled "The Arrow with a Heart Pierced Through Him".

== Cast ==
*Angelica Panganiban as Mace Castillo
*JM de Guzman as Anthony Lagdameo
*Joem Bascon as Marco

== Awards and nominations ==
 2014 Cinema One Originals Film Festival ===
* Best Actress: Angelica Panganiban (won)
* Audience Choice (won)
* Champion Bughaw Award for Full-length Feature (won)
* Best Picture (nominated)

=== 13th Gawad Tanglaw Awards ===
* Best Actress: Angelica Panganiban  (tied with Nora Aunor)  (won)
 2014 Don Carlos Palanca Memorial Awards for Literature ===
* Best Screenplay: Antoinette Jadaone (3rd place)

== References ==
 

== External links ==
*  at the Internet Movie Database