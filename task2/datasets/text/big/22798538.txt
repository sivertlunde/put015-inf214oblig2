Grain in Ear
{{Infobox film name     = Grain in Ear image    = Grain_in_Ear.jpg caption  = Poster for Mang zhong (2005) director = Zhang Lü producer = Choi Do-yeong music    =  writer   = Zhang Lü starring = Liu Lianji Jin Bo Zhu Guangxuan Wang Tonghui editing        =  Kim Sun-min cinematography = Liu Yonghong distributor    =  released =   runtime  = 109 minutes country  = China South Korea language = Mandarin Korean Korean
}}
Grain in Ear ( : Máng zhòng;     refers to the solar term in the traditional calendars of China and Korea. 

==Plot== Korean ancestry. A single mother bringing up a young son, she lives away from her hometown, and makes a living by selling kimchi. In the course of living her life, she meets three men who betray her. When her son dies in an accident, she decides to take revenge. Korean Film Council: Grain in Ear / 芒種 ( Mang-zhong ) http://www.koreanfilm.or.kr/KOFIC/Channel?task=kofic.user.eng.a_filmdb.command.FilmDB1Retrieve2Cmd&MainGBN=1&MVIE_CD=T060062N&pageSize=5 

==Location==
Grain in Ear was filmed in a small industrial town, an area 45 minutes drive away from Beijing, yet just on the edge of farmland." Yu Gu. "Grain in Ear." Schema Magazine, 2005. http://www.schemamag.ca/viff2005/2005/10/grain_in_ear.html 

==Theme==
Grain in Ear examines the interplay of sex, economics, social class and race in a newly industrialized Chinese provincial backwater. Korean Chinese are one of the recognized ethnic minorities in China, comprising about 2.7 million citizens. Korean Chinese are spread throughout the country, and their group situation is consequently invisible to other Chinese, though many have difficulties integrating into society. Televised folklore celebrations and popular Korean foods such as the world-famous kimchi serve as one of the few common reminders of Korean Chinese culture for other Chinese. Zhang, in his brutal focus on the challenges of assimilation for his native ethnic group, explains that "his film is essentially anti- terrorist. Not at all in the way of George W. Bush|Bush’s political agenda, but on the scale of everyday life, how we as humans terrorize those around us." 

==Festival appearances and awards==

*2005 Pusan International Film Festival: Best Film
*2005 Cannes Film Festival: The 44th International Critics Week, Prix ACID du meilleur
*2005 Pesaro Film Festival: Concorso PNC
*2005 Vancouver International Film Festival: Dragons and Tigers
*2005 Thessaloniki International Film Festival: International Competition
*2005 Osians - Cinefan, Festival of Asian & Arab Cinema: ASIAN FRESCOES
*2005 Chicago International Film Festival: New Directors
*2006 San Francisco International Asian American Film Festival
*2006 Seattle International Film Festival: New Directors Competition, Special Jury Prize
*2006 Vesoul International Film Festival of Asian Cinema: Golden Cyclo Award
*2006 International Film Festival Innsbruck: Province of Tyrol Award
*2006 Durban International Film Festival: Best Direction, Best Actress
*2006 Cinema Novo Festival, Competition: Karibu Award
*2006 Barcelona Asian Film Festival: Official Section, Gold Durian Award
*2006 Leeds International Film Festival: Official Selection
*2006 Brisbane International Film Festival
*2006 Film Independents Los Angeles Film Festival: International Showcase
*2006 International Film Festival of Asia Pacific countries in Vladivostok: China in Focus
*2006 Mar del Plata International Film Festival: Foreign Visions, Point of View
*2006 INDIE LISBOA: Competition
*2006 BLACK MOVIE festival de films des autres mondes: Bodies, A Chronicle
*2006 Arsenals International Film Forum, KIM & Chi: The South Side
*2006 Paris Cinema: New Korean Cinemas

SOURCE: Korean Film Council,  UCLA 

==See also==
*Koreans in China
*Chinese cinema
*Korean cinema
*Korean cuisine
*Social realism

==References==
 

==External links==
*  
*   at Hancinema Korean Movie Database
*   at the Chinese Movie Database
*   at the UCLA New Chinese Cinema Series

 
 
 
 
 
 