Red Cockroaches
{{Infobox film|
name           = Red Cockroaches |
image          = Red Cockroaches Poster.jpg |
writer         = Miguel Coyula|
starring       = Adam Plotch Talia Rubel Jeff Pucillo  Diane Spodarek|
director       = Miguel Coyula |
producer       = Miguel Coyula |
distributor    = Herectic Films (US) / Piramide (Cuba)|
released       = October 25, 2003 (Cuba) |
runtime        = 82 min. |
language       = English |
budget         = 2000 USD |
music          = Miguel Coyula |
tagline        = Incest in a Surreal World|
}}

Red Cockroaches (Spanish: Cucarachas Rojas) is a Cuban film released in 2003. This feature film was the debut production of Miguel Coyula and was the result of a two-year effort on a tiny budget of $2,000. Shot entirely using a portable digital camcorder and edited on a home computer, Red Cockroaches is an example of DIY cinema. In its review, Variety (magazine)|Variety called it a "A triumph of technology in the hands of a visionary with know-how..." It is the first of a planned trilogy which continues with Corazon Azul (Blue Heart).

== Plot ==
A young man meets a mysterious girl in the subway and gets romantically involved  with her,  only to later discover that she might be his long lost sister.  Dark and atmospheric, the world the characters inhabit is an alternative New York City with bouts of Acid Rain and ruled by an omnipotent cloning company called DNA21. Cryptic in nature and merging several genres, mainly sci-fi and drama with sporadic dark humor and surrealist touches, Red Cockroaches’ morally ambiguous incest story has gathered as many fans as detractors making it a modern underground cult movie.

==Visuals==
Based on carefully planned storyboards, every time there is a cut in the film, it is to a new camera setup that hasnt been used before as opposed to traditional film language where the editor cuts back to a same shot during a scene. In the case of Red Cockroaches the influence of comic book storytelling is obvious.

 

Some reviewers have pointed that scene transitions and edits mimic the effect of a pop-up book.    

Rather than attempting to make DV look like film, the colors were digitally manipulated to the extreme with the purpose of enhancing the atmosphere. This along with multiple layer composites generated the movie’s distinctive, often saturated look.

 
 

==Awards==
* GreenCine Online Film Festival, US, June 2005: Narrative Grand Prize
* San Francisco Fearless Tales, US, April 2005: Best Editing
* Festival Buenos Aires Rojo Sangre, Argentina, Nov 2004: Special Mention for Visual Concept
* SciencePlusFiction, Italy, Nov 2004: Special Mention
* Festival Cineplaza, Cuba, Oct 2004: Gran Premio Plaza
* Festival Cineplaza, Cuba, Oct 2004: Best Director
* Festival Cineplaza, Cuba, Oct 2004: Best Editing
* Festival Cineplaza, Cuba, Oct 2004: Best Sound
* Microcinema Fest, US, July 2004: Best of Fest
* Microcinema Fest, US, July 2004: Best Director
* Microcinema Fest, US, July 2004: Best Science Fiction Feature
* Microcinema Fest, US, July 2004: Best Actor
* Microcinema Fest, US, July 2004: Best Editing 
* Microcinema Fest, US, July 2004: Best FX
* Encuentro Nacional de Video, Cuba, June 2004: Best Editing 
* Encuentro Nacional de Video, Cuba, June 2004: Special Mention (Narrative)
* Encuentro Nacional de Video, Cuba, June 2004: Best Cinematography 
* Digital Independent Film Festival (DIFF), US, May 2004: Grand Jury Prize
* Muestra de Nuevos Realizadores, Cuba, Feb 2004: Special Award of the Jury
* Festival Almacen de la Imagen, Cuba, Oct 2003: Best Feature
* Festival Almacen de la Imagen, Cuba, Oct 2003: Best Editing 
* Festival Almacen de la Imagen, Cuba, Oct 2003: Cinema Award

== References ==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 
 