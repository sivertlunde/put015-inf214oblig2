The 47 Ronin (1941 film)
 
 
{{Infobox film
| name           = The 47 Ronin
| image          = 47ronin lefilm.jpg
| image_size     =
| caption        = Japanese movie poster
| director       = Kenji Mizoguchi
| producer       = Shochiku
| writer         = Seika Mayama (play) Kenichiro Hara Yoshikata Yoda (screenplay)
| narrator       =
| starring       =
| music          = Shirō Fukai
| cinematography = Kōhei Sugiyama
| editing        =
| distributor    = Shochiku Kinema Kenkyu-jo Part 2 (Japan)|ref2=   |1979|03| |U.S. }}
| runtime        = 223 min. 112 min. (part 1) 111 min. (part 2)
| country        = Japan
| language       = Japanese
| budget         =
}} Japanese film directed by Kenji Mizoguchi, adapted from a play by Seika Mayama. The film chronicles the end of the lives of the forty-seven Ronin.

== Cast ==
Actors in the film include: 
* Chojuro Kawarasaki as Ōishi Kuranosuke (Ōishi Yoshio)
* Kanemon Nakamura as Sukeimon Tomimori
* Kunitaro Kawarazaki as Isogai Jurozaemon
* Yoshizaburo Arashi as daimyo Asano Naganori
* Daisuke Katō as Fuwa Kazuemon
*   as Okuno Shōgen
*   as 奥田孫兵衛
* Isamu Kosugi as Okado Shigetomo
* Masao Shimizu as 加藤越中守
* Utaemon Ichikawa as Tokugawa Tsunatoyo
* Seizaburō Kawazu as Lord Etchumori Hosokawa
* Mantoyo Mimasu as Kira Kozukenosuke
* Mitsuko Miura as Yosenin, Lady Asano, Asanos wife
* Mieko Takamine as Omino, Isogais fiancee

== See also ==
* The Loyal 47 Ronin (忠臣蔵 Chushingura) - 1958 film Kunio Watanabe
*   - 1962 color film directed by Hiroshi Inagaki
* Daichūshingura (大忠臣蔵, Daichūshingura) - 1971 television dramatization
* The Fall of Ako Castle (赤穂城断絶, Akō-jō danzetsu) (aka Swords Of Vengeance) - 1978 film by Kinji Fukasaku
* Matsu no Ōrōka
*List of historical drama films of Asia

==Release==
 
The first film was released in Japan in 1941, just before the attack on Pearl Harbor. 

== References ==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 


 