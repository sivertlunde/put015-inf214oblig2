Kotoko (film)
{{Infobox film
| name = Kotoko
| image =
| caption =
| director = Shinya Tsukamoto
| producer = Shinya Tsukamoto
| writer = Shinya Tsukamoto (screenplay), Cocco (story)
| starring = Cocco Shinya Tsukamoto
| music =
| cinematography = Satoshi Hayashi, Shinya Tsukamoto
| editing = Shinya Tsukamoto
| distributor = Third Window Films (UK)
| released = September 8, 2011 (Venice Film Festival premiere) 
| runtime = 91 min.
| language = Japanese
| budget =
}}

Kotoko (KOTOKO) is a 2011 Japanese film by cult director Shinya Tsukamoto. It is based on an original story by J-pop artist Cocco, who stars in the film alongside Tsukamoto.

==Plot==
Suffering from double vision, a single mother (Cocco) tries to take care of her baby in the grip of terrifying hallucinations. Experiencing a nervous breakdown, she is deemed unfit to take care of her child and has it taken away from her. The only respite the mother has from her visions is when she sings. An award-winning novelist (Tsukamoto) overhears her singing whilst riding the bus and the pair subsequently develop a volatile relationship.

==Release==
 Venice International Film Festival where it won the Best Film award in the festivals Orizzonti section, the first Japanese film to do so. 

The film was picked up for distribution in the UK by Third Window Films and was released on DVD and Blu-ray Disc on October 8, 2012. 

==References==
 

==External links==
* 
*  at  
* 

 

 
 
 
 

 