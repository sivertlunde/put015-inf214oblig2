As Night Comes
{{Infobox film
| name           = As Night Comes
| image          = File:As night comes 2014 film poster.jpg
| alt            =
| caption        = 
| film name      = 
| director       = Richard Zelniker
| producer       = 
| writer         = Ryan Koehn Richard Zelniker
| screenplay     = 
| story          = 
| based on       =  
| starring       = Luke Baines Myko Olivier Evanne Friedmann
| narrator       = 
| music          = John Swihart
| cinematography = Mark Mannschreck
| editing        = Chris Monte
| production companies = Lucid Pictures Magic Hair Studios
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
As Night Comes, also known under the working title of Mischief Night, is a 2014 crime drama film that was directed by Richard Zelniker.  The movie is based on a script written by Zelniker and Ryan Koehn, who was still in high school while As Night Comes was written and filmed.  It had a limited theatrical release on 14 November 2014 in Los Angeles before receiving a video on demand release on 5 December, 2014.   The film stars Luke Baines as a violent gang leader who takes another teenager under his wing. 

==Synopsis==
Sean (Myko Olivier) is a young teen that has attracted the attentions of Ricky (Luke Baines) after he saves Sean from his abusive father. As Ricky is the leader of a gang of outcast teenagers called the Misfits, Sean falls right in with the group. However even among these outcasts Sean does not feel like he truly belongs, especially after hearing that the Misfits plan on staging a series of beatings to various townspeople that they deem deserving. 

==Cast==
*Luke Baines as Ricky Gladstone
*Myko Olivier as Sean Holloway
*Evanne Friedmann as Sarah David
*Aku Pitt as Julie Rivera (as Stacia Hitt)
*Kent Harper as Red Chapman
*Jenna Marie Bowers as Tiffany Post (as Jenna Bowers)
*Lane Smith Jr as Blane Connor
*Ryan Shoos as Donny Chapman
*Andrew Baxter as Ozzy Parker
*Paulo Vincent-Brown as Dillon Thompson
*Alexander Christensen as John Wallace
*Jesse Kove as Brad Larson
*Weston Cage as Bryan Roberts
*Moe Irvin as Mr. Hayes

==Reception==
Shock Till You Drop panned As Night Comes, stating that  "Although the cast brings a bout of realism to the film, mostly because they’re nearly all fresh newcomers, it’s not enough to make it interesting" and that "Zelniker has a message he’s trying to portray, he just doesn’t managed to do so very well."  The Los Angeles Times gave a mixed review, writing "As Night Comes has power, authenticity & tension by the incendiary climax but needs a more magnetic package."  In contrast, the Pasadena Independent gave a wholly positive review and praised several elements in the film, including several of the actors performance and noting the "exceptional production design, lighting and camera work which creatively blend to fill the screen with images that are simultaneously fascinating, haunting, beautiful, repellent and symbolic." 

==References==
 

==External links==
*  
*  
*  

 
 