The Maid (2005 film)
{{Infobox Film
| name           = The Maid
| image          = The_Maid.jpg
| caption        = Official film poster
| director       = Kelvin Tong
| producer       = Chan Pui Yin Seah Saw Yam Titus Ho
| writer         = Kelvin Tong
| narrator       = 
| starring       = Alessandra De Rossi Chen Shu Cheng Hong Hui Fang Benny Soh
| music          = Alex Oh Joe Ng
| cinematography = Lucas Jodogne
| editing        = Lawrence Ang Low Hwee Ling
| studio         = Fortissimo Films Tartan Films
| distributor    = MediaCorp Raintree Pictures
| released       =  
| runtime        = 93 minutes
| country        = Singapore English Teochew Teochew Tagalog Tagalog
| budget         = 
}} Chinese Ghost Ghost Month, Teochew Chinese opera family, a family with many secrets, who give her a place to stay in their dilapidated shophouse.

The film broke the box office record in Singapore for the horror genre, making Singapore dollar|S$700,000 on its opening weekend.

It won the European Fantastic Film Festival Federation (EFFFF) Asian Film Award at the 10th Puchon International Fantastic Film Festival (PiFan).  The EFFFF Asian Film Award is a prize awarded to the best Asian film of the year by the federation, of which PiFan is a supporting member.

It stars Alessandra de Rossi, Hong Huifang, Benny Soh, Guan Zhenwei and Chen Shucheng, a veteran actor-compere.

== Plot ==
During the Chinese Seventh Month, the gates of hell open and spirits are set loose upon the unsuspecting world. Hailing from a small village in the Philippines, 18-year-old Rosa Dimaano arrives in Singapore on the first day of the Seventh Month to work as a domestic maid. She urgently needs money to save her younger brother who is ill back home. Her employers, the elderly Mr. and Mrs. Teo, appear kindhearted and sympathetic.  Their mentally-handicapped son Ah Soon also takes to Rosa immediately.

Between cleaning house and helping the Teos out at their Teochew opera workplace, things start going seriously amiss. Rosa glimpses strange apparitions at night and also have vivid nightmares. She finds out that there was a maid two years before her called Esther.  She apparently has the same good relationship with Ah Soon as she does.  That is when Rosa begins to suspect things are seriously wrong. 

As she explores around some of the shady areas of the house, she discovers the burnt corpse of Esther, the previous maid hidden in a concealed drum and through a ghostly vision that Esthers spirit shows her realizes that Esther was playing with Ah Soon in his room two years ago on that same day of the Ghost Festival when he suddenly, though unintentionally, raped her on the bed. When the parents witnessed this, they prevented Esther from calling police so Mr. Teo beat her to the ground, bundled her up and poured oil all over her before setting her on fire, burning her to death.

Frightened by the vision, Rosa tries to escape when she is suddenly confronted by Ah Soon. She quickly finds out that Ah Soon is also a ghost himself when she tries to stab him only to realize that the man would not show a scar or any visible wound. In the past, Ah Soon committed suicide shortly after the death of Esther but instead of passing to the afterlife, he chose to return to the house to find the spirit of Esther, whom he was in love with. From behind her, Mr. Teo knocks Rosa unconscious and ties her up. When Rosa awakens, she is told by Mrs. Teo that the Teo family has longed believed that ghosts can also marry and their whole intention for inviting her to Singapore was to sacrifice her so that she would be wed to their dead son. But when they try to hang Rosa, Ah Soon suddenly remembers the way his father brutally murdered Esther the year before and tells his parents that he does not want to see any more deaths in the house. 

In a scuffle, Ah Soon accidentally pushes Mr. Teo into a altar, spilling oil on him, Esthers spirit then ignites the oil, burning him to death. Rosa manages to break free with the help of Esthers spirit and tries to escape. Mrs. Teo picks up a knife and tries to finish her off once and for all. Rosa dashes out of the house and crosses the road with Mrs. Teo close behind. As she stumbles and falls on the other side of the road, Rosa watches in horror as Mrs. Teo approaches her menacingly with the knife in hand, but an oncoming truck hits Mrs. Teo head on, killing her instantly. Rosa breathes a sigh of relief as the camera pans over Mrs. Teos dead body.

In the final sequence, Rosa is seen heading to the airport to fly back to her hometown on the last day of July when the gates of hell are closing. She takes with her the jar containing Esthers ashes as she enters the airport while the ghosts of the Teo family watches her disappear beyond the doors.

==See also==
*List of ghost films

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 