The Zany Adventures of Robin Hood
{{Infobox film |
| name = Zany Adventures of Robin Hood, The
| image = The Zany Adventures of Robin Hood.jpg
| caption = Ray Austin
| writer = Robert Kaufman
| starring = George Segal Morgan Fairchild Roddy McDowell Janet Suzman Tom Baker Neil Hallett Robert Hardy Roy Kinnear Robin Nedwell Michelle Newell Pat Roach
| producer = Andrew Donally
| music = Stanley Myers
| cinematography =
| editing = Bill Lenny
| distributor =
| released =   1984
| runtime = 91 min.
| country =   United States
| language = English
| budget =
}}
The Zany Adventures of Robin Hood (1984 in film|1984) is a parody film of the story of Robin Hood.

==Plot== Prince John (Roddy McDowell) sits on the throne, supported by his evil henchman, Sir Guy of Gisbourne (Tom Baker) and constantly ridiculed by his mother, Eleanor of Aquitaine (Janet Suzman). While celebrating his birthday, the Prince becomes unhappy when he discovers his older brother, Richard I of England|Richard, is not dead as everyone thought but rather being held for ransom in Austria. As Richard is the rightful King of England, this creates a problem for Prince John. However, Gisbourne openly suggests that they tax the Saxon peasants to raise the ransom, while secretly confiding in the Prince that the King will most likely die of pneumonia before the year is out and that they will keep the money.
 drag and reciting an offensive poem. Not seeing through the disguise, Gisbourne demands she take them to Robin Hoods hideout in Sherwood Forest. That night, Eleanor convinces Maid Marian (Morgan Fairchild) to play up to Gisbournes advances and use him to help Robin Hood.

When Prince John and Guy travel to Sherwood, they are ambushed by Robin Hoods Merry Men. They are stripped of their garments, their possessions and money stolen, then driven out of the forest. Will Scarlet (Robin Nedwell) frequently tries to make a song out of these events, but thankfully Robin stops him every time.

Eleanor and Marian sneak into the bandits camp at night, disguised as two nuns, and hoping to enlist Robins help in freeing King Richard. They are caught by Robin, who recognises them immediately, and the trio eat together and discuss plans. Robin and Marian realise they love each other.

Robin and his band begin travelling across the country to raise the ransom money but nothing goes right. They try robbing a bank only to find that they have won a prize as the hundredth customer and get distracted. When they finally rob the bank, they find it only has three coins and a pfennig. Times are hard for everyone, it seems. Eventually, Robin has to report back to Eleanor that England is broke. She advises him to get the money from Isaac of York (Kenneth Griffith), a Jewish moneylender.

In York, Robin stands out like a sore thumb amongst all the orthodox Jews. Isaac agrees to lend Robin the ransom money provided that in return the King gives the Jews their own country. Isaac suggests Palestine, although Miami would be his second choice. They settle on naming the new country after Isaacs father, Israel. Robin sneaks into the Princes castle to tell Eleanor, who now insists he must raise an army. Robin then tries to seduce Marian, promising her theyll be wed once King Richard returns, but with little success.

==Starring==
*George Segal as Robin Hood Lady Marian Prince John
*Janet Suzman as Eleanor of Aquitaine
*Tom Baker as Sir Guy of Gisbourne
*Neil Hallett as the Sheriff of Nottingham King Richard
*Roy Kinnear as Friar Tuck
*Robin Nedwell as Will Scarlet
*Michelle Newell as Rebecca
*Pat Roach as Little John

==Guest starring==
*Kenneth Griffith as Isaac of York
*Melvyn Hayes as Father Luther
*Michael Hordern as Rupert

==Other Cast==

*Aubrey Morris as Archbishop
*Bruce Purchase as Moishe
*Roger Ashton-Griffiths as 1st Coachman
*Paul Brooks  as 2nd Coachman
*Angus Lennie as Scott Kelly
*Tony Steedman as Bank President
*Fenella Fielding as Molly
*John Louis Mansi as Reuben
*Kelly Summers as Miss Jones
*Melanie Hughes as Miss Sanders
*Colin Higgins as Irving
*Peter Brayham as Yehoudi
*Marc Boyle as Bernie
*Andrew Lodge as Lord Exeter
*Angela Grant as Lady Exeter
*Su Elliot as Alice
*Chris Webb as 1st Merryman
*Del Baker  as 2nd Merryman
*Terence Plummer as 3rd Merryman
*Steve Dent as 4th Merryman
*Peter Brace as Captain of Guards
*Fred Haggerty as 1st Guard Fred Powell as 2nd Guard
*Dinny Powell as 3rd Guard
*George Lane Cooper as 4th Guard
*Derek Lyons as 5th Guard William Morgan Sheppard as Saxon thief
*Freddie Jones
*Marianne Stone

==See also==
*1984 in film

==External links==
* 

 
 
 
 
 
 
 
 