It Was an Accident
It Was an Accident is a 2000 British comedy film directed by Metin Hüseyin and starring Chiwetel Ejiofor, Max Beesley and James Bolam.  In the film, an ex-convict, trying to go straight, accidentally gets mixed up in a fresh series of crimes.

==Cast==
* Chiwetel Ejiofor ...  Nicky Burkett 
* Max Beesley ...  Mickey Cousins 
* James Bolam ...  Vernon Fitch 
* Nicola Stapleton ...  Kelly 
* Neil Dudgeon ...  Holdsworth 
* Hugh Quarshie ...  George Hurlock 
* Thandie Newton ...  Noreen Hurlock 
* Jacqueline Williams ...  Sharon 
* Sidh Solanki ...  Rameez 
* Cavan Clerkin ...  Jimmy 
* Sally Chattaway ...  Terri 
* Paul Chowdhry ...  Rafiq Roy 

==References==
 

==External links==
* 

 
 
 
 

 