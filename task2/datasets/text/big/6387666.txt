The Crazy Ray
 
 Lon Chaney film, see While Paris Sleeps.

{{Infobox film
| name           = Paris Qui Dort The Crazy Ray
| image          = 
| image_size     = 
| caption        = 
| director       = René Clair
| producer       = Henri Diamant-Berger (producer)
| writer         = René Clair
| narrator       = 
| starring       = Henri Rollan
| music          = Jean Wiener
| cinematography = Maurice Desfassiaux Paul Guichard
| editing        = René Clair
| studio         = 
| distributor    = Film Arts Guild
| released       =  
| runtime        = 35 minutes
| country        = France
| language       = Silent film French intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Paris Qui Dort (literally "Paris which sleeps") is a 1925 French science fiction comedy silent short film directed by René Clair. Also released as Le rayon de la mort, its international English-language titles were The Crazy Ray and Paris Asleep. It has also been released in the USA as At 3:25.

== Plot summary ==
 
The film is about a mad doctor who uses a magic ray on citizens which causes them to freeze in strange and often embarrassing positions. People who are unaffected by the ray begin to loot Paris.

== Cast ==
*Henri Rollan as Albert
*Charles Martinelli as The scientist
*Louis Pré Fils
*Albert Préjean as The pilot
*Madeleine Rodrigue as Hesta, the airline passenger
*Myla Seller as The niece / daughter of the scientist
*Antoine Stacquet as The rich man
*Marcel Vallée

== Soundtrack ==
 

== Home media == Criterion DVD release of another Clair film, Under the Roofs of Paris (1930). It is also available for free at the Internet Archive.

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 