His Brother's Wife
{{Infobox film
| name           = His Brothers Wife
| image          = His Brothers Wife 1936 Poster.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = W. S. Van Dyke
| producer       = Lawrence Weingarten
| screenplay     = {{Plainlist|
* Leon Gordon John Meehan
}}
| story          = George Auerbach
| starring       = {{Plainlist|
* Barbara Stanwyck Robert Taylor
}}
| music          = Franz Waxman
| cinematography = Oliver T. Marsh
| editing        = Conrad A. Nervig MGM
| distributor    = MGM
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $367,000  . 
| gross          = $1,662,000 
}}
 Robert Taylor. John Meehan, based on a story by George Auerbach, the film is about a scientist preparing to leave for the jungles of South America to work on a cure for spotted fever. Wanting to have some fun before his trip, he goes to a gambling club where he meets and falls in love with a beautiful model while falling deep into debt. When he turns to his brother for help, his brother agrees to cover the debt, but only if he leaves without her. While the scientist is away, his brother and the model get married.

His Brothers Wife was produced by Lawrence Weingarten for Metro-Goldwyn-Mayer, and was released on August 7, 1936 in the United States. Upon its theatrical release, the film received mixed reviews that praised its expert direction and glossy production, but criticized its "outrageous implausibility".  His Brothers Wife was the first of three films starring Barbara Stanwyck and Robert Taylor; the couple married in 1939.

==Plot== Robert Taylor), agrees to accompany him, but insists on taking a few weeks off before the trip in order to have some fun. Chris ends up at a gambling club run by a crooked mobster name "Fish-Eye" and soon loses five thousand dollars on credit. That night, Chris meets beautiful model Rita Wilson (Barbara Stanwyck), who tags along on his gambling spree. In the coming days, the two fall in love, and when she learns that he will soon be leaving for the jungle, she persuades him to stay.

When Fish-Eye demands immediate payment of his gambling debt, Chris turns to his brother Tom (John Eldredge) for financial assistance. Suspicious of Rita, Tom offers to pay Chris debt, but only if he leaves for the jungles as planned—and without Rita. Chris agrees to the proposal and postpone his marriage to Rita until after he returns in two years. Angered by his decision, Rita breaks off the relationship and returns on her own to the gambling club, where she accepts Fish-Eyes offer to work for him as an escort to lure wealthy gamblers to his club in exchange for paying off Chris debt. Soon after, Tom visits the gambling club and sees Rita, who confesses that it was she who paid Chris debt with money she inherited from her grandmother.

Later that year, on Christmas Eve, Chris returns to New York City and learns that Tom and his fiancée have broken up, and that his brother resigned from his position at the hospital. When pressed for an explanation, Tom tells Chris that he fell in love with Rita and that they were secretly married, but later she ridiculed him and refused to stay with him. Believing the worst about Rita, Chris goes to the gambling club looking for her. Filled with remorse over her actions, she confesses her mistakes and admits that she still loves him, not his brother. Chris proposes that she accompany him to the jungle as a friend and wait for Tom to agree to a divorce before renewing their relationship. 

A few months later, when word arrives that Tom obtained the divorce, Chris tells Rita that he planned all this in order to get back at her for her actions. When she offers to let him use her to test their new serum, Chris refuses and bitterly sends her away. Initially, Professor Fahrenheim planned to test the serum on Chris, but now he has second thoughts about the potential danger to his assistant. When Rita learns that Chris will be used for the testing, she secretly injects herself with the disease in order to save Chris. Moved by her actions and realizing that he still loves her, Chris produces more serum and saves Ritas life. Soon after, Chris and Rita get married and sail back to New York City.

==Cast==
* Barbara Stanwyck as Rita Claybourne Robert Taylor as Chris
* Jean Hersholt as Professor Fahrenheim
* Joseph Calleia as "Fish-Eye"
* John Eldredge as Tom
* Samuel S. Hinds as Dr. Claybourne
* Leonard Mudie as Pete
* Jed Prouty as Bill Arnold
* Pedro de Cordoba as Dr. Capolo
* Rafael Storm as Captain Tanetz
* William Stack as Winters
* Edgar Edwards as Charlie

==Production==
===Casting===
His Brothers Wife was originally planned as a vehicle for Jean Harlow and Clark Gable, to be directed by E. A. Dupont with the working title My Brothers Wife.    Gable was later replaced by Franchot Tone, with Richard Boleslawski taking over as director.  Barbara Stanwyck and Robert Taylor were finally selected as the leads for His Brothers Wife, the first of their three films together. Stanwyck and Taylor were married in 1939; they were divorced in 1952.   

===Filming===
After W. S. Van Dyke was brought in to direct, filming from the 137-page script was completed in only thirteen and a half days. 

==Release==
Produced by Lawrence Weingarten for Metro-Goldwyn-Mayer, His Brothers Wife was released on August 7, 1936 in the United States.   

==Critical response==
In his review for the New York Times, Frank S. Nugent described the film as "incredibly romantic, glossily produced, expertly directed and peopled by the sort of players most often encountered on the covers of the fan magazines".    But despite this "triumph of machine-made art",  Nugent criticized the story for its "romantic absurdity" and "outrageous implausibility". 

==Box Office==
According to MGM records the film earned $1,196,000 in the US and Canada and $466,000 elsewhere, resulting in a profit of $755,000. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 