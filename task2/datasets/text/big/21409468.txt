Tokyo Kid
{{Infobox Film
| name           = Tokyo Kid
| image          = Tokyo Kid poster.jpg
| caption        = Japanese movie poster
| director       = Torajiro Saito
| producer       = Shochiku Takashi Koide (producer)
| writer         = Akira Fushimi (writer) Kihan Nagase (story)
| starring       = 
| music          = Tadashi Manjome
| cinematography = Hiroyuki Nagaoka
| editing        = 
| distributor    =  1950 (Japan) 
| runtime        = 81 min.
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Japanese film musical comedy and drama directed by Torajiro Saito.

 

== Cast ==
* Hibari Misora as an orphan Mariko Tanimoto
* Haruhisa Kawata (川田晴久) as the street musician Sanpei
* Shunji Sakai (堺駿二) as Sanpeis pal Shin-chan
* Taeko Takasugi (高杉妙子)
* Ayuko Saijō (西條鮎子)
* Achako Hanabishi (花菱アチャコ) as Koichi Tanimoto, Marikos father
* Kenichi Enomoto (榎本健一) as the fortune teller
* and others

==See also==
* List of films in the public domain

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 