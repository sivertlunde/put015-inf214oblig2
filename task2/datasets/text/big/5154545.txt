Ali Zaoua
{{Infobox film
| name = Ali Zaoua ( birth date : 01-11-1988 ) birth place : Ain Haroudat Casablanca. 
| image =
| image_size =
| caption =
| director = Nabil Ayouch
| producer = Etienne Comar Jean Cottin Antoine Voituriez
| writer = Nabil Ayouch Nathalie Saugeon
| narrator =
| starring = Maunim Kbab Abdelhak Zhayra Hicham Moussaune Amal Ayouch Mustapha Hansali
| music = Krishna Levy
| cinematography = Renaat Lambeets Vincent Mathias
| editing = Jean-Robert Thomann
| distributor = Arab Film Distribution (USA)
| released =  
| runtime = 90 minutes
| country = Morocco
| language = Arabic French
| budget =
| gross =
| preceded_by =
| followed_by =
}} Moroccan crime drama film that tells the story of several homeless boys living in Casablanca. It was awarded in the 2000 Stockholm Film Festival and in the 2000 Amiens International Film Festival.

==Plot==
Against a background of dockside poverty in Casablanca, populated by a loose gang of over 20 homeless and uneducated male youths under 15, Kwita (Maunim Kbab), Omar (Mustapha Hansali), Boubker (Hicham Moussaune) and Ali Zaoua (Abdelhak Zhayra) leave the group becoming 4 independents. Ali, with plans of becoming a cabin boy on a ship, leads this exodus from the gang — led by Dib (Said Taghmaoui). Early in the film and almost accidentally, Ali is killed by members of the gang. His 3 outsider friends decide to give him a proper funeral. Kwita is treated badly by military, by police and by well-off children because he is "not devout", cannot pray, is unclean, smells like dead meat and is a glue sniffer, and Omar attempts to return to Dibs gang. Boubker, the smallest and most irrepressibly buoyant of the boys, temporarily despairs, but recovers. Against all odds, the three boys manage to arrange Alis funeral to pay respect to their friend in the main story of the film.

==Awards==
* Bronze Horse, 2000 Stockholm Film Festival
* Audience Award, 2000 Amiens International Film Festival
* Golden Crow Pheasant Award (Suvarnachakoram) for best film at the 6th International Film festival of  Kerala, 2001, held in Thiruvananthapuram, Kerala.

==Importance and role of rituals depicted==
Ali Zaoua is a Moroccan film which reflects the beliefs of the people that how they are sticking with their faith blindly based on their pre-conception. The conception is just fake idea but believed to be as reality.  The film has been described as magical realism, in the sense of how  the stark reality of the children’s lives interweaves with their rich fantasy life. The contrast between real life and fantasy life shows the strong part of their beliefs. In their feeling and practice they are very different from each other.  In this film Ali, Kwita, Omar and Boubker are street kids. The daily dose of glue sniffing represents their only escape from reality. Ali wants to become a sailor - when he was living with his mother, a prostitute, he used to listen to a fairy tale about the sailor who discovered the miracle island with two suns. Instead of finding his island in the dream, Ali and his friends are confronted with Dib’s gang. Matters are getting serious; the four kids separated themselves from Dib’s gang. As a result, Ali Zaoua is killed by Dib’s gang when he is hit by a stone. Omar and Baker wanted to bury him as king but circumstances do not support them.

==External links==
*  
*  
*  

 
 
 
 