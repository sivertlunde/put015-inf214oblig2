In Another Country (film)
 
{{Infobox film
| name           = In Another Country
| image          = In Another Country film.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| rr             = Dareun Naraeseo
| mr             = Tarŭn Naraesŏ}}
| director       = Hong Sang-soo
| producer       = Kim Kyeong-hee Hong Sang-soo
| writer         = Hong Sang-soo
| starring       = Isabelle Huppert Yoo Jun-sang
| music          = Jeong Yong-jin
| cinematography = Park Hong-yeol Ji Yoon-jeong
| editing        = Hahm Seong-won
| studio         = Jeonwonsa Films
| distributor    = Jeonwonsa Films JoseE Films
| released       =  
| runtime        = 89 minutes
| country        = South Korea
| language       = Korean English
| budget         = 
| gross          = 
}}
In Another Country ( ) is a 2012 South Korean comedy-drama film written and directed by Hong Sang-soo.   Set in a seaside town, the film consists of three parts that tell the story of three different women, all named Anne and all played by French actress Isabelle Huppert.    The film competed for the Palme dOr at the 2012 Cannes Film Festival.          

The film was selected as part of the 2013 Hong Kong International Film Festival. 

== Plot == Jung Yu-mi) and her mother Park Sook (Youn Yuh-jung) hiding from their debtors in Mohang, a seaside town in Buan, North Jeolla. The bored younger woman sets out to write a screenplay whose plot will use the place they’re staying in for the location, but eventually comes up with three variants, using the same basic idea in all of them.

In each case, Won-joos protagonist is a "charming French visitor" named Anne ( ) and his very pregnant and jealous wife Geum-hee (Moon So-ri); in the second, shes the wife of a French motor-executive who comes to the same guesthouse to meet her lover, a Korean filmmaker, Moon-soo (Moon Sung-keun); in the third, shes a divorced housewife who arrives with her university lecturer friend Park Soon (again Youn Yuh-jung) for some peace and quiet, after her husband left her for his young Korean secretary.

Issues of infidelity are present in each story, as is a dim but muscular lifeguard (Yoo Jun-sang) whom Anne meets while strolling on the beach and looking (in all three episodes) for a mini-lighthouse.

As each "Anne" interacts with the locals, including Won-joo, who works at Annes lodging and helps show her around, certain faces, situations and lines of dialogue recur, their effect and implications changing depending on context and delivery.   

== Cast ==
* Isabelle Huppert as Anne
* Yoo Jun-sang as a lifeguard
* Kwon Hae-hyo as Jong-soo
* Moon So-ri as Geum-hee
* Moon Sung-keun as Moon-soo Jung Yu-mi as Won-joo
* Youn Yuh-jung as Park Sook Kim Yong-ok as a bhikkhu

== Production ==
Isabelle Huppert first met Hong Sang-soo at the Paris Cinémathèque in March 2011 at a retrospective of his films. She says, "I think the idea was already in his head, perhaps, the desire to work together." 

Seoul, 2011. Huppert’s photo exhibition La femme aux portraits (Woman of Many Faces) had just been unveiled and her film Copacabana (2010 film)|Copacabana was released in theaters. The actress came to Korea for the occasion and had meetings with the film directors she had always wanted to meet—namely Park Chan-wook, Lee Chang-dong, Bong Joon-ho, Im Sang-soo and of course, Hong. According to Hong, Huppert’s casting happened when they were drinking makgeolli (a brewed Korean rice wine) at a restaurant in Seoul, a place that has often appeared in his films. Not expecting much, and in a casual tone that may have even sounded like a joke, he spoke to Huppert. "I’m about to start a new film. Nothing about it has been decided yet. Would you like to be in it?" Then came an astonishing reaction from her: "Yes!" Huppert replied without even a second of hesitation. "The film came to be what it is now when Isabelle decided to join it," says Hong. At the end of June, she came to Korea by herself to begin filming. 

Huppert let herself be transported by a project without a script and was delighted to play the game, traveling to a beach town, Mohang, South Korea, in a very rainy season. "I don’t know all his films, but I think he always works the same way. He starts with the place, finds the people, and only then, writes the script. So it gives an enigmatic cast to everything — and to the way we work. He has a curious, atypical relationship to his film. He’s very exacting, and precise, and there are lots of takes, he’s not ever in a rush," Huppert said. She said she adored working in foreign countries, and the cast and crew "all spoke Korean, and English was the only language I could speak with them; this added to the strangeness. In Korea, I was really alone, and it felt good. It was a very intimate shoot. He’s a director who likes to shoot close up. Only a few weeks of shooting, but he works both fast and slow, in the sense that there are a lot of takes. Each filmmaker has his own rhythm, yet I had the impression that he was very surefooted. He filmed me in profile or from the back, like a figure in the landscape. I love the way he filmed me, just like any other woman in the landscape." Huppert said she loved the adventurous aspect to making this movie. "And adventure is at the vital center of the story." 

== Awards and Nominations ==
2012 Buil Film Awards
*Nomination – Best Film
*Nomination – Best Director – Hong Sang-soo
*Nomination – Best Actor – Yoo Jun-sang
*Nomination – Best Actress – Isabelle Huppert
*Nomination – Best Supporting Actress – Moon So-ri

2012 Grand Bell Awards
*Nomination - Best Supporting Actor - Yoo Jun-sang

== See also ==
* Isabelle Huppert filmography

== References ==
 

== External links ==
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 