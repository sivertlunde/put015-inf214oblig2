Woodstock Villa
 
 
{{Infobox Film
| name           = Woodstock Villa
| image          = Woodstockvilla poster.jpg
| caption        = Theatrical poster
| director       = Hansal Mehta
| producer       = Sanjay Gupta Ekta Kapoor Shobha Kapoor
| writer         = Rajiv Krishna S. Farhan Arbaaz Khan Sikandar Kher Sachin Khedekar Neha Oberoi
| music          = Anu Malik
| cinematography = Vikas Nowlakha Mahesh Aney
| editing        = Bunty Nagi
| distributor    = White Feather Films
| released       = 30 May 2008
| runtime        =
| country        = India Hindi
| budget            =     
| gross             =      (worldwide gross, not nett)
}} musical thriller thriller directed Arbaaz Khan in the primary roles while Shakti Kapoor, Gulshan Grover, Sachin Khedekar, Boman Irani and Anupama Verma essay other significant roles. The film, whose soundtrack was composed by Anu Malik, was filmed in Mumbai and Mauritius. The film, which was released in India on 30 May 2008, had a poor box office opening but earned mostly good reviews. Sanjay Dutt and Saif Ali Khan have cameo appearances in the film.

==Synopsis== Arbaaz Khan) love. Samir can’t refuse because he is in desperate need of money. He hasn’t paid his rent for months and also has to return a huge sum of money to a bhai (Gulshan Grover). Zara takes Sameer to Woodstock Villa, the location of kidnapping. Sameer orders Jatin to hand over 5&nbsp;million to him. After returning, Sameer discovers Zara dead. An anonymous caller then threatens him that he has only 30 minutes to bury the body and clear up all the evidence. He disposes her body in a forest and returns. To be on the safer side, he leaves the city and goes to Bangalore. Sameer sees Zaras video on television and heads back to find the truth. He finds Zara and convinces her to tell him the truth. Zara reveals that she and Jatin truly loved each other. Once in a fight, Jatins real wife, Zara accidentally, died and as her and Zaras face was quite similar, she played the role of Zara. The kidnapping plan was hatched by Jatin and his girlfriend to get out of this murder and trap somebody else. Sameer calls Jatin to Woodstock villa with the money and he pays his rent and loan. He goes to the airport while Jatin gets caught by the police. Jatin tells the police that he is not the only one to commit the crime. Sameer gives the bag of money to Zara but there was no money inside that bag, he took the real bag of money. The ball was in Zaras court. If she would have boarded the plane, Sameer would have trusted her. She decided to cheat Sameer and got cheated herself. Sameers flight takes off while Jatins partner gets arrested.

==Cast==
* Sikandar Kher as Sameer
* Neha Oberoi as Zara Kampani Arbaaz Khan as Jatin Kampani
* Gulshan Grover as Karim Bhai
* Shakti Kapoor as Mr. Chawla, Sameers landlord
* Sanjay Dutt as Gaurav
* Saif Ali Khan
* Boman Irani
* Anupama Verma
* Gaurav Gera

==Production==
Sikander Kher had several expectations from the media on his debut film. {{cite news|accessdate=2 June 2008|url=http://entertainment.in.msn.com/bollywood/article.aspx?cp-documentid=1429288
|title=Sikander Ka Muqaddar|date=2 June 2006|last=Nair|first=Rajeev|work=MSN India}}  Although his family name would provide recognition, he chose to have his only his first name listed on the credits.  Sanjay Gupta, the producer of the film is the uncle of the other newcomer, Neha Oberoi. She said it was exciting and challenging to play the role of a kidnapped wife. {{cite news|accessdate=2 June 2008|url=http://sify.com/movies/bollywood/fullstory.php?id=14685634
|title=Dad stays away from Uberois premiere|date=2 June 2006|work=Sify.com}} 

==Release and reception== Arbaaz Khan and his wife, Malaika Arora Khan and writer, Javed Akhtar were the prominent people attending the films premiere. {{cite news|accessdate=30 May 2008|url=http://www.saharasamay.com/samayhtml/articles.aspx?newsid=99904
|title=Woodstock Villa premier held|date=2 June 2006|work=Sahara Samay}}    Anil Kapoor attended the premiere too thereby ending the three-year war between him and Anupam Kher.   

Upon its release, the Hindustan Times termed the film as "out of stock villa" in a demeaning tone. Its review felt that even though Sikander had potential, it was limited by the screenplay and direction. {{cite news|accessdate=2 June 2008|url=http://www.hindustantimes.com/StoryPage/StoryPage.aspx?id=b6010bfe-7d97-4a7c-8cac-74cf7bab7e99&ParentID=8dc54409-2bea-47d3-a6f0-02cf935def4d&MatchID1=4688&TeamID1=4&TeamID2=1&MatchType1=1&SeriesID1=1182&PrimaryID=4688&Headline=Review%3a+EMWoodstock+Villa%2fEM
|title=Out of stock villa|date=30 May 2006|work=Hindustan Times}}  Times of India, on the other hand, wrote in its review that the film was a stylish thriller with good cinematography. It further spoke highly about Sikander, the debutant actor and added that though the songs impede the films pace, they provide "freshness and a refreshing new zing." {{cite news|accessdate=2 June 2008|url=http://timesofindia.indiatimes.com/moviereview/3086663.cms
|title=Woodstock Villa - Hindi movie review|date=30 May 2006|work=Times of India|last=Kazmi|first=Nikhat}} 

==Soundtrack==
*01 Dhoka
*02 Saawan Mein Lag Gayee Aag 
*03 Kyun 
*04 Yeh Pyaar Hai 
*05 Koi Chala Ja Raha Hai 
*06 Raakh Ho Ja Tu 
*07 Dhoka Dega 
*08 Saawan Mein age Gayee Aag – Club Mix 
*09 Dhoka – Remix

==References==
 

==External links==
*  
* 

 
 
 