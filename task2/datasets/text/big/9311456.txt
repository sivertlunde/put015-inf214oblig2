Sathyam (2004 film)
{{Infobox film
| name          = Sathyam
| image         = Sathyam.jpg
| caption       = DVD Cover
| director      = Vinayan
| writer        = Vinayan
| starring      = Prithviraj Sukumaran  Priyamani
| released      = 2004
| music         = M. Jayachandran
| cinematography = Shaji
| editing       = G. Murali
| art           = Salu K. George
| stunts        = Kanal Kannan
| makeup        = Shah Pattanam
| budget        =
| country       = India
| language      = Malayalam
| }} Prithviraj and Priyamani.

== Plot ==
The story circles around Sanjeev Kumar (Prithviraj) who is awaiting his selection into the police force. But it was being delayed by the then Police commissioner (Anandaraj) who is backed up by his political and underground relationships and has a grudge against Sanjeevs father. When things go beyond a limit, Sanjeev gets fed up, and locks up the Commissioner in a secret location. Sanjeev gets selected for the police force and joins as a Sub-Inspector in a police station near his home town, and gets a lot of praise for his work in curbing crime in the city. In the meanwhile, the Commissioner breaks out of his prison. Sanjeev manages to prove that the commissioner is insane. But he face lots of obstacles, his wife (Priyamani) is shot. His niece (Taruni Sachdev) & his father (Thilakan) is been kidnapped & The rest of the story shows how things turn out when they face each other again.

== Cast ==
* Prithviraj Sukumaran ...	Sanjiv Kumar
* Priyamani		   ...	Sona
* Taruni Sachdev       ...  Chinnukutty
* Thilakan		   ...	Ayyappan Nair
* Anandaraj		   ...	Mambally Mukundan Menon Rajeev     ....   Minister Mambally Madhava Menon Suresh Krishna	   ...	Mambally Prakash Menon
* Lalu Alex		   ...	Police Officer
* Captain Raju   	   ...	Police Officer
* Venu Nagavalli 	   ...	Chief Minister
* Kochu Preman   	   ...	Policeman
* Baburaj Jacob	   ...	Mattancherry Martin
* Kollam Thulasi 	   ...	Police Officer

== Soundtrack ==
The films soundtrack contains 6 songs, all composed by M. Jayachandran. Lyrics by Gireesh Puthenchery, S. Ramesan Nair, and Kaithapram Damodaran Namboothiri.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Be Happy Man"
| Jyotsna Radhakrishnan|Jyotsna, Vijay Yesudas
|-
| 2
| "Kaatte Kaatte"
| M. G. Sreekumar
|-
| 3
| "Kaatte Kaatte(F)"
| Kalyani
|-
| 4
| "Kallakkurumbi"
| Sujatha Mohan, Baby Vidya
|-
| 5
| "Nee En Sundari"
| Karthik, K. S. Chitra
|-
| 6
| "Whisile Whisile"
| Alex Kayyalaykkal, Ganga
|}

==External links==
* 

 
 
 


 