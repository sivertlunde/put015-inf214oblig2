Tees Maar Khan (2010 film)
 
 
 
{{Infobox film
| name           = Tees Maar Khan
| image          = Tees Maar Khan akshay.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Farah Khan
| producer       = Twinkle Khanna Shirish Kunder Ronnie Screwvala
| screenplay     = Shirish Kunder Ashmith Kunder
| story          = 
| based on       = 
| starring       = Akshay Kumar Katrina Kaif  Akshaye Khanna Arya Babbar
| music          = Vishal-Shekhar Shirish Kunder
| narrator       = Sanjay Dutt
| cinematography = P. S. Vinod
| editing        = Shirish Kunder
| studio         =
| distributor    = Hari Om Productions Threes Company UTV Motion Pictures
| released       =  
| runtime        = 131 minutes 
| country        = India
| language       = Hindi
| budget         =   
| gross          =   
}} Hindi heist heist comedy film directed by Farah Khan, starring Akshay Kumar, Katrina Kaif and Akshaye Khanna in the lead roles.  Salman Khan and Anil Kapoor make special appearances in the film.   The film was released on 24 December 2010.  The film is today primarily remembered for Katrina Kaifs dance number, Sheila Ki Jawaani. The theatrical trailer and title song of the film were released on UTV Motion Pictures YouTube channel on 4 November 2010 for promotional purposes. The trailer was premiered in theatres with Vipul Shahs Action Replayy and Rohit Shettys Golmaal 3 on 5 November 2010.  The film is an official remake of the 1966 Italian film After the Fox. 

==Plot==
Tees Maar Khan revolves around conman Tabrez Mirza Khan (TMK, Akshay Kumar) who has been a criminal since he was young. He was caught in France and was deported back to India. Two police officers Chatterjee (Aman Verma) and Mukherjee (Murli Sharma) took him but he escaped.

He is in love with the struggling actress Anya (Katrina Kaif). TMK takes a job from the Johri brothers (Raghu Ram and Rajiv Laxman) to rob a non-stop train loaded with precious antiques. He cons the people of Dhulia village into helping him by saying that he will make a historical film about their village. He approaches the greedy but popular actor Aatish Kapoor (Akshaye Khanna), who is eager to get the Oscar award and fools him into working in his "film". After accidentally exposing a child abduction ring and thus helping many villagers find their long-lost children, TMK feels guilty about conning the villagers and swears that he will give them a share of the treasure. The robbery goes well but all the villagers and Tabrez Mirza Khan get arrested while the Johri brothers make off with the treasure. TMK claims that he is innocent, but he gets arrested anyway. However, at the court, he says many funny jokes, and by chance he hears that he will be in the jail for 7 years. but it was that he will be in the jail for 60 years.

The movie is completed and, at the premiere, Aatish, Anya, TMKs mother, Dollar, Soda, Burger and all the villagers walk the red carpet with Tees Maar Khan and the police. After the film, Tees Maar Khan escapes. Later, on their private jet, the Johri brothers enjoy what they got until Tees Maar Khan and his gang throw them out of the plane. The film ends with TMK and his mother becoming rich, Aatish receiving Oscar awards from actor Anil Kapoor, Anya becoming a spokesperson for hair removal cream and the whole Dhulia village opening their own saloons, party halls and the village inspector opening a poster signing stall to all the people from America. The crew of the film are seen receiving the Oscar awards in the song "Happy Ending".

==Cast==

* Akshay Kumar as Tabrez Mirza Khan/Tees Maar Khan/TMK
* Katrina Kaif as Anya Khan
* Akshaye Khanna as Atish Kapoor
* Raghu Ram and Rajiv Laxman as Johri Brothers
* Arya Babbar as Inspector Dhurinder
* Sachin Khedekar as Commissioner Khadhak Singh
* Vijay Patkar as Inspector Jagtap
* Murli Sharma as Mukherjee
* Aman Verma as Chatterjee
* Ali Asgar (actor) as Burger
* Dharampal as Dollar
* Vijay Maurya as Soda
* Mia Evonne Uyeda as The Girl With Dog
* Apara Mehta as Tabrezs Mother
* Anjan Srivastav as Nana Ganphule
* Shashi Kiran as a Villager
* Avtar Gill as Subedaar
* Sudhir Pandey as Bunty Baweja
* Anvita Dutt Guptan as a Press Reporter
* Viju Khote as a Judge
* Vishal Dadlani as a Film Director in "Sheila Ki Jawani"
* Manish Paul as Master India
* Anil Kapoor in a Special Appearance in Climax
* Salman Khan as Special Appearance in Song "Wallah Re Wallah"
* Chunky Pandey as Himself in Special Appearance
* Komal Nahta as Critic
* Charlotte Yates as The Shocked English Airport Girl
* Maxwell Harding as The Shocked English Backing Dancer
* Sanjay Dutt as The Narrator

==Production==
Priyanka Chopra was earlier rumoured to play the part of Anya,  but that rumour was later dropped after Katrina Kaif was chosen to play the female lead. 

The script is penned by director Farah Khans husband Shirish Kunder. Sanjay Dutt was confirmed to play the role of Sutradhaar (narrator) for the film.  Farah stated in an interview that she wanted someone with a voice which people can even recognise in their sleep. 

An entire train was made and 500m track laid for Rs. 7.5&nbsp;million to shoot the climax scenes. 
Dharmesh Yelande choreographed in this film.

==Release==
Tees Maar Khan premiered in the UK on 22 December, in Fiji on 22 December and in Canada on 23 December 2010.

==Reception==

===Critical response===
Tees Maar Khan received average reviews from critics. Anupama Chopra of NDTV wrote, "Tees Maar Khan, adapted from After the Fox, by writers Shrish and Ashmit Kunder, is disappointingly limp and insistently low IQ. The film has little of the effervescence and flair of a typical Farah Khan film" giving it 2/5 stars.  Aseem Chhabra of Rediff.com, who rated the film 1.5/5, stated, "Even at two hours, the film feels like one long and tedious exercise in bad humour. Some people laughed during the screening I attended in New York City. However, most sat with glum faces, in a sense of disbelief ..... How could a talented(..?) person like Khan make such an unfunny film?"  Behindwoods review board gave the film a two out of five star rating and quoted "So is Tees Maar Khan watchable? Good Question. Very good question. The answer is Yes if you are in the mood to celebrate. But watch it in theatres as the fun is only when there is a crowd laughing along with you. The second half can have you in splits."  Taran Adarsh of Bollywood Hungama gave a modest rating of 3/5 in his review and said, "Farah Khans brand new outing Tees Maar Khan will make the most absurd, bizarre and wacky cinema of yore pale in comparison. Not just your cell phone, even your brain needs to be put on switched off mode at the commencement of this film."  Nikhat Kazmi of the Times of India awarded 2.5/5 stars while commenting, "Sadly, Tees Maar Khan begins as a spoof and remains a spoof, till the very end. All the characters end up as mere caricatures and completely fail to build up an emotional quotient in the film."  Yahoo! Movies gave the film 1.5/5 stars.  Komal Nahta of Koimoi.com gave it a rating of 2/5 and stated, "Too much of farcical comedy; lack of emotions; over-the-top characters; unbelievable script ..... Tees Maar Khan is definitely a disappointment; but it will bring back the invested money and a bit more."  Aniruddha Guha of Daily News and Analysis gave the film 2 stars and remarked, "Even Sheila can’t make Tees Maar Khan watchable ... Though the story is interesting (Neil Simon of After The Fox should ideally get the credit), the writing is so pedestrian and Farah Khan’s presentation so lacklustre that you wonder how the film was greenlighted at all."  Gaurav Malani of The Economic Times gave 2 stars saying, "To sum up in Tees Maar Khan’s trademark style of dialogue delivery, Akshay Kumar se zara hatke comedy expect karna aur Akshaye Khanna se kuch bhi expect karna bekaar hain. Tees Maar Khan doesn’t even guarantee thirty good laughs in its three-hour runtime."  Kaveree Bamzai of India Today rated it 2½/5 saying, "Its an oddly half-hearted film from a woman   who is never known to do anything in half measure."  Rajeev Masand of CNN-IBN rated the movie 2/5 suggesting, "If you’re outraged by such low-brow humor, Tees Maar Khan is going to be a long, hard slog for you." 

===Box office===

====India====
 second biggest opening day grosser of all time across India for Hindi movies after Dabangg. On the second day of its release, the film netted  , taking the two-day total to   nett.    Tees Maar Khan collected about   nett at the end of its first weekend, thus becoming the second biggest opening weekend net grosser of all time across India for Hindi movies.    Despite its initial huge run at the box office, there had been many negative reports about the film.  Box Office India reported, "Tees Maar Khan has not matched the expectations from the film prior to release but it is not that bad either as it is not as if the film just collapsed. The opening weekend collections are around 15% lower than sensible expectations."  Tees Maar Khan underwent a heavy 65% fall on Monday, its fourth day of business, as it collected  .    The film further suffered a big drop on Tuesday, its fifth day of business, collecting around   nett, taking the five-day business to   nett.    Tees Maar Khan went on to collect   nett in its first week of release.  The film showed an 85% decline in the second week, on its eight-day of business, as compared to its first day.    In its second weekend, the film collected around   nett, taking the ten days collections to   nett.    The film collected about   nett in second week, taking the total collections in two weeks to   nett.  The film collected    nett in its third week, taking the total domestic collections   nett.   

====Overseas====

In the overseas market, the film grossed around $2.5 million from its extended weekend, which, according to Box Office India, "is below par for a big film". The breakdown included £320,000 from United Kingdom in 5 days, $750,000 from North America in 5 days, $500,000 from United Arab Emirates in 3 days and $175,000 from Pakistan in 3 days.  The nett collections till the end of the second weekend e were: £625,000 from United Kingdom, $1,030,000 from North America, $775,000 from UAE, and $265,000 from Australia. 

==Awards and nominations==
 IIFA Awards
;;Nominated IIFA Best Female Playback - Sunidhi Chauhan for Sheila Ki Jawani

;Zee Cine Awards

;;Won 
*Zee Cine Award for Best Choreography Award - Farah Khan for "Sheila Ki Jawani"

;;Nominated 
*Zee Cine Award for Best Track of the Year - "Sheila Ki Jawani"
*Zee Cine Award for Best Music Director - Vishal / Shekhar
*Zee Cine Award for Best Playback Singer - Male - Sonu Nigam for "Tees Maar Khan"
*Zee Cine Award for Best Playback Singer - Female - Sunidhi Chauhan for "Sheila Ki Jawani"
*Zee Cine Award for Best Lyricist - Vishal Dadlani for "Sheila Ki Jawani"
*Zee Cine Award for Best Actor in a Supporting Role – Male - Akshaye Khanna
*Zee Cine Award for Best Cinematography - P.S. Vinod

;Filmfare Awards
;;Won Filmfare Best Playback Singer - Female - Sunidhi Chauhan for Sheila Ki Jawani Filmfare Best Choreography Award - Farah Khan for Sheila Ki Jawani

;Star Screen Awards
;;Won Best Actress (Popular Choice) - Katrina Kaif
;;Nominated Screen Best Female Playback for Sheila Ki Jawani (Sunidhi Chauhan)

;Apsara Film & Television Producers Guild Awards
;;Won
*Best Female Playback for Sheila Ki Jawani Sunidhi Chauhan

;Stardust Awards
;;Won Star of the Year – Male - Akshay Kumar

;Other Awards
;;Won
* GIMA award Best Female playback for Sheila Ki Jawani
* Airtel Dancing Super Star for Sheila Ki Jawani - Katrina Kaif

==Soundtrack==
{{Infobox album 
| Name = Tees Maar Khan
| Type = Soundtrack
| Artist = Vishal-Shekhar
| Cover =
| Alt = 
| Caption = 
| Released =  
| Recorded = Feature Film Soundtrack
| Length =  
| Label = T-Series
| Producer = Shirish Kunder
| Last album = Break Ke Baad (2010)
| This album = Tees Maar Khan (2010)
| Next album = Bbuddah... Hoga Terra Baap (2011)
}}
The music of the film is composed by the music duo of Vishal-Shekhar and one song from debutant music composer Shirish Kunder for title track. Lyrics are penned by Vishal Dadlani, Anvita Dutt Guptan and Shirish Kunder. The music was launched on 14 November 2010. The entire cast and crew travelled on a train booked from Mumbai to Lonavala for the music launch.  Sonu Nigam has given 54 voices for the title track of the film.  The song Sheila Ki Jawani became a chartbuster and fetched Sunidhi Chauhan a Filmfare Award and many other awards.

===Track listing===

{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| total_length =  
| title1 = Tees Maar Khan
| note1 = Music by: Shirish Kunder
| extra1 = Sonu Nigam
| lyrics1 = Shirish Kunder
| length1 = 4:17
| title2 = Sheila Ki Jawani
| extra2 = Vishal Dadlani, Sunidhi Chauhan
| lyrics2 = Vishal Dadlani
| length2 = 4:43
| title3 = Wallah Re Wallah
| extra3 = Shekhar Ravjiani, Shreya Ghoshal, Raja Hasan, Kamal Khan
| lyrics3 = Anvita Dutt Guptan
| length3 = 5:28
| title4 = Badey Dilwala
| extra4 = Shreya Ghoshal, Sukhwinder Singh
| lyrics4 = Anvita Dutt Guptan
| length4 = 4:55
| title5 = Happy Ending
| extra5 = Harshit Saxena, Abhijeet Sawant, Debojit Saha, Prajakta Shukre
| lyrics5 = Anvita Dutt Guptan
| length5 = 4:43
| title6 = Tees Maar Khan - Remix
| extra6 = Sonu Nigam
| lyrics6 = Shirish Kunder
| length6 = 4:18
| title7 = Sheila Ki Jawani - Remix
| extra7 = Vishal Dadlani, Sunidhi Chauhan
| lyrics7 = Vishal Dadlani
| length7 = 4:48
| title8 = Wallah Re Wallah - Remix
| extra8 = Shekhar Ravjiani, Shreya Ghoshal, Raja Hasan, Kamal Khan
| lyrics8 = Anvita Dutt Guptan
| length8 = 4:29
| title9 = Badey Dilwala - Remix
| extra9 = Shreya Ghoshal, Sukhwinder Singh
| lyrics9 = Anvita Dutt Guptan
| length9 = 4:57
}}

==Sequel==
Even before the release of the film, the director Farah Khan and her husband Shirish Kunder planned to turn Tees Maar Khan! into a franchise by making a sequel to the film.  Thus they are currently working on the script and shooting will commence towards the end of 2012 with Akshay Kumar, who has agreed to star in the sequel and will once again play the lead character of the film. 

==References==
 

==External links==
* 
* 
 
 
 
 
 
 