Frits and Freddy
{{Infobox film
| name           = Frits and Freddy
| image          = 
| caption        = 
| director       = Guy Goossens
| producer       = Guy Goossens
| writer         = Mark Punt
| starring       = Tom Van Dyck Peter Van den Begin
| music          = 
| cinematography = Gerd Schelfhout
| editing        = 
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Belgium
| language       = Belgian Dutch
| budget         = 
}}

Frits and Freddy ( ) is a 2010 Flemish-Belgian comedy film directed by Guy Goossens.    

==Cast==
* Frank Aendenboom as Kamiel Frateur
* Stijn Cole as Gino
* Damiaan De Schrijver as Leon
* Tom Dewispelaere as Patrick Somers
* Frank Focketijn as Willy Faes
* Eric Godon as Rene beurlet
* Tania Kloek as Gina Mus
* Wim Opbrouck as Carlo Mus
* Jaap Spijkers as Max Den Hamer
* Peter Van den Begin as Frits Frateur
* Lucas Van den Eynde as Sjarel Willems
* Tom Van Dyck as Freddy Frateur

==References==
 

==External links==
*  

 
 
 
 
 
 
 