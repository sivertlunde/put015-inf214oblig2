Devil's Cavaliers
{{Infobox film
| name           = Devils Cavaliers
| image          =Devils Cavaliers.JPG
| image_size     =
| caption        =
| director       = Siro Marcellini
| producer       = Giulio Fiaschi (executive producer) Argy Robelli (producer) Edoardo Robelli (producer)
| writer         = Jean Blondel (dialogue) Carlo Alberto Chiesa (screenplay) Siro Marcellini (screenplay) Siro Marcellini (story) Ugo Moretti (dialogue)
| narrator       =
| starring       = See below
| music          = Carmine Rizzo
| cinematography = Luciano Trasatti
| editing        = Edmond Lozzi
| distributor    =
| released       = 26 June 1959
| runtime        = 92 minutes (USA)
| country        = Italy
| language       = Italian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Devils Cavaliers ( ) is a 1959 Italian adventure film directed by Siro Marcellini.

== Plot summary ==
 

== Cast ==
*Frank Latimore as Capt. Richard Stiller
*Emma Danieli as Countess Louise Valance
*Gianna Maria Canale as Baroness Elaine of Faldone
*Gabriella Pallotta as Guiselle, Louises Maid
*Anthony Steffen as Richmond
*Andrea Aureli as Duk of Vas
*Federica Ranchi as Derolia the Bar Maid
*Franco Fantasia as Duneil the Swordsman
*Mirko Ellis as Paul, Stiller Henchman
*José Jaspe as Jermaine, Stiller Henchman
*Oreste Lionello
*Andrea Fantasia
*Carlo Bressan
*Pasquale De Filippo
*Franco Diana
*Fedele Gentile
*Loris Gizzi
*Lea Monaco
*Nino Musco
*Bruno Parisio
*Andrea Scotti
*Nunzio Gallo as Count Henri Valance, Louises Brother

== External links ==
* 
* 

 

 
 
 
 


 