The Girls (1968 film)
{{Infobox film
| name = The Girls
| image = The Girls (film).jpg
| caption = 
| director = Mai Zetterling
| producer = Göran Lindgren David Hughes
| based on = Lysistrata by Aristophanes
| starring = Bibi Andersson Harriet Andersson Gunnel Lindblom
| music = Michael John Hurd
| cinematography = Rune Ericson
| editing = Wic Kjellin Sandrew Film & Teater
| distributor = Sandrew Film & Teater
| released =  
| runtime = 100 minutes
| country = Sweden
| language = Swedish
}}

The Girls (  is a 1968 Swedish drama film directed by Mai Zetterling, starring Bibi Andersson, Harriet Andersson and Gunnel Lindblom. It is a feminist reinvention of the ancient Greek play Lysistrata by Aristophanes, and revolves around a theatre group who set up the play.

==Cast==
* Bibi Andersson as Liz
* Harriet Andersson as Marianne
* Gunnel Lindblom as Gunilla
* Gunnar Björnstrand as Hugo
* Erland Josephson as Carl
* Frank Sundström as the doctor
* Åke Lindström as Bengt
* Stig Engström as Thommy
* Ulf Palme as director

==Release and reception== Sandrew Film & Teater.  In 2012, the film was voted one of the 25 best Swedish films of all times. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 

 