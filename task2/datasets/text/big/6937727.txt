It Is Fine! Everything Is Fine.
{{Infobox Film 
| name = It is Fine! EVERYTHING IS FINE.
| caption = 
| image	=	It Is Fine! Everything Is Fine. FilmPoster.jpeg
| director = Crispin Glover David Brothers
| producer = Crispin Glover
| writer = Steven C. Stewart
| starring = Steven C. Stewart
| music = 
| cinematography = 
| editing = Molly Fitzjarrald Crispin Glover
| distributor = Volcanic Eruptions
| released = January 23, 2007
| runtime = 74 minutes English
| budget = 
}} directed by funded by Crispin Glover.

The film was written by and stars Utah writer-actor Steven C. Stewart, who also appears in What Is It? He died of complications from cerebral palsy in 2001, only one month after principal filming wrapped.

Glover has said that the script was in the style of a 1970s made-for-TV movie, and said in an online chat that "Its an autobiographical, psycho-sexual, fantastical retelling of   point-of-view of life." Aside from the opening and closing scenes that were filmed in a nursing home, It is Fine. Everything is Fine! was shot entirely at David Brotherss sound stage in Salt Lake City, Utah. Glover has stated that it is "probably the best film   ever work on in   entire career."   

It Is Fine is the second film in a planned trilogy, with the other two titles being What Is It? and It Is Mine.

This film premiered at the Egyptian Theater in Park City, Utah on January 23, 2007 for the Midnight screening as an official selection of the 2007 Sundance Film Festival. Crispin Glover and David Brothers were in attendance, as well as many of the cast and crew. Glover and Brothers introduced the film and conducted a lengthy question and answer period after the film.

==References==
 

== External links ==
*  
*  
*  
*  
 

 
 
 
 
 
 
 


 