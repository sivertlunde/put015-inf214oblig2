Stella Dallas (1925 film)
 
{{Infobox film
| name           = Stella Dallas
| image          = Stella-dallas-lobby-1925.jpg
| caption        = Lobby card Henry King
| producer       = Samuel Goldwyn
| writer         = Frances Marion
| starring       = Ronald Colman Belle Bennett Lois Moran
| music          = 
| cinematography = Arthur Edeson
| editing        = Stuart Heisler
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =  gross = $1.5 million   accessed 19 April 2014 
}} Henry King. The film stars Ronald Colman, Belle Bennett, Lois Moran, Alice Joyce, Jean Hersholt, and Douglas Fairbanks Jr.   Prints of the film survive in several film archives.   

==Cast==
*Ronald Colman as Stephen Dallas
*Belle Bennett as Stella Dallas
*Alice Joyce as Helen Morrison
*Jean Hersholt as Ed Munn
*Beatrix Pryor as Mrs Grosvenor
*Lois Moran as Laurel Dallas
*Douglas Fairbanks, Jr. as Richard Grosvenor
*Charles Willis Lane as Stephen Dallas, Sr.
*Vera Lewis as Mrs. Tibbetts
*Maurice Murphy as Morrison child
*Jack Murphy as Morrison child
*Newton Hall as Morrison child
*Charles Hatton as Morrison child (older)
*Robert W. Gillette as Morrison child (older)
*Winston Miller as Morisson child (older)

==References==
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 


 