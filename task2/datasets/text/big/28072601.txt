F comme Fairbanks
{{Infobox film
| name           = F comme Fairbanks
| image          = F Comme Fairbanks.jpg
| caption        = Film poster
| director       = Maurice Dugowson
| producer       = Jean-Paul Gibon Michel Seydoux
| writer         = Maurice Dugowson Jacques Dugowson
| starring       = Patrick Dewaere Miou-Miou
| music          = 
| cinematography = André Diot
| editing        = Jean-Bernard Bonis
| distributor    = 
| released       = 5 May 1976
| runtime        = 110 minutes
| country        = France
| language       = French
| budget         = 
}}

F comme Fairbanks is a 1976 French drama film directed by Maurice Dugowson. It was entered into the 26th Berlin International Film Festival.   

==Cast==
* Patrick Dewaere - André
* Miou-Miou - Marie John Berry - Fragman
* Michel Piccoli - Etienne
* Jean-Michel Folon - Jean-Pierre
* Christiane Tissot - Sylvie
* Diane Kurys - Annick
* Jean Lescot - Jeannot le régisseur
* Jean de Coninck - Le photographe
* Evane Hanska - Françoise
* Thierry Lhermitte - Le jeune cadre
* Guiguin Moro - Lassistante
* Christian Clavier - Le serveur
* Yves Barsacq - Le vieux cadre
* Jenny Clève - La grand-mère
* Marc Lamole - Le patron du bistrot

==References==
 

==External links==
* 

 
 
 
 
 
 
 