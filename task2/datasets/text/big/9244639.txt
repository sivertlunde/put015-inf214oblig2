Battles Without Honor and Humanity: Final Episode
{{Infobox film
| name           = Battles Without Honor and Humanity: Final Episode
| image          = Battles Without Honor and Humanity Final Episode.jpg
| caption        = Japanese release poster
| director       = Kinji Fukasaku
| producer       =
| writer         = Kazuo Kasahara Kōichi Iiboshi  (original story) 
| starring       = Bunta Sugawara Akira Kobayashi Kinya Kitaoji Joe Shishido
| narrator       = Tetsu Sakai
| music          = Toshiaki Tsushima
| cinematography = Sadaji Yoshida
| editing        =
| distributor    = Toei Company
| released       = June 29, 1974
| runtime        = 98 minutes
| country        = Japan
| language       = Japanese
| budget         =
}}

  is a 1974 Japanese yakuza film directed by Kinji Fukasaku. It is the final film in a five-part series that Fukasaku made in a span of just two years.

==Plot==
Following the previous film, Shozo Hirono is serving seven years in prison and Akira Takeda was released from prison on a technicality. Takeda then united all the families in the Hiroshima region, shed their yakuza image, and formed the Tensei Coalition political party. In August 1965, Takeda is leader as Chairman, Katsutoshi Otomo is Vice chairman, Tamotsu Matsumura is Managing Director, Hideo Hayakawa is Secretary General, and Shoichi Eda a Director. Hironos men get into an altercation with some low-level Tensei members. Elder yakuza Otomo and Hayakawa demand the instigators should have to commit yubitsume, however, after a vote the Coalition agrees to let Matsumura handle discipline on account of wanting to avoid the rituals tied to yakuza for their public image. In 1966, Hironos friend Terukichi Ichioka has a Tensei financial adviser killed and attends the funeral, angering Otomo who openly vows to take him on. However, Takeda threatens that anyone who responds to the attack will be expelled from Tensei.

Sensing tension brewing, the police raid Tenseis various offices and find illegal firearms, leading to Takedas arrest. However, earlier that day Takeda called an emergency meeting to pick his acting successor should he be arrested. Takeda nominated Matsumura while Hayakawa nominated Otomo, but Takeda had scripted the voting to have his nominee chosen in order to have a fresh face not well-associated with yakuza. Immediately upon Takedas arrest, Otomo and Hayakawa sir up discontent amongst the member of the coalition. Otomo then sends men to kill Matsumura. However, when Otomo is released on bail in 1968, Matsumura allows him to return to Tensei. Hayakawa lures Otomo into a meeting with Ichioka, with the later two formally swearing brotherhood the next day in order to take down Matsumura.

In 1969, Masakichi Makihara is paroled from prison and both the Tensei and Otomo/Hayakawa vie for his support, with the former ultimately buying it. At the same time, Toyoaki Mano of the Otomo family and Ichimatsu Kubota of the Hayakwa leave their bosses and join the Tensei Coalition for a large payoff. Several days later, when Ichioka has his men stirring trouble in Tensei turf, Mano falsely reports to his boss that Matsumura has gone on a trip in order for Matsumura to have Ichioka killed while off guard. Otomo learns Mano betrayed him, and after Hayakawa ignores his call for help avenging Ichioka, he dazedly walks into public openly carry guns and is arrested.

Matsumura then announces the Tensei Coalition is no longer a political group and demands each member swear loyalty to him as boss, with only Hayakawa refusing and announcing retirement. When Takeda is released from prison in 1970, he resumes his position and begins planning how do deal with Hirono impending release. All the families stir in regard to his upcoming release, including his own, with one member killing Makihara. Knowing the tension over his release, the police discharge Hirono hours early and he travels to Tokyo. Takeda finds Hirono and informs him of Tenseis decision that they can not allow him to return to Hiroshima unless he retires. Even after Takeda claims he will retire himself, Hirono disagrees. Matsumura visits Hirono in Matsuyama to tell him Takeda has retired and to make a proposition of his own; If Hirono will retire, his right hand man Atsushi Ujiie can join the Tensei Coalition. Hirono tells Matsumura to clean out his own dissenters in Tensei before he will give his answer.

In preparation for his formal installation as Chairman, Matsumura travels to Osaka inviting different guests. During the trip, remnants of the Otomo and Hayakawa families shoot up his car while stopped at a train crossing, resulting in Edas death and Matsumura seriously injured. Despite his serious condition, Matsumura goes through with the ceremony, with Hirono attending and asking for a seat for Ujiie. After a member of Hironos family is killed in retaliation for Makihara and he sees the deceaseds sister dressing the body, Hirono decides to retire.

==Cast==
*Bunta Sugawara as Shozo Hirono
*Akira Kobayashi as Akira Takeda
*Kinya Kitaoji as Tamotsu Matsumura
*Joe Shishido as Katsutoshi Otomo
*Junkichi Orimoto as Hideo Hayakawa
*Kunie Tanaka as Masakichi Makihara
*Shingo Yamashiro as Shoichi Eda
*Hiroki Matsukata as Terukichi Ichioka
*Goichi Yamada as Toyoaki Mano
*Goro Ibuki as Atsushi Ujiie
*Nobuo Kaneko as Yoshio Yamamori
*Asao Uchida as Kenichi Okubo
*Isao Konami as Ichimatsu Kubota
*Harumi Sone as Miyaji Senno
*Takuzo Kawatani as Matsumura family member
*Yumiko Nogawa as Kaoru Sugita
*Nobuo Yana as Ryosuke Kaga
*Kenichi Sakuragi as Akio Saeki
*Sanae Nakahara as Shizuko Murata
*Hiroko Fuji as Sumiko
*Maki Tachibana as Mitsuko
*Yuke Kagawa as Eri

==Release==
Battles Without Honor and Humanity: Final Episode has been released on home video and aired on television, the latter with some scenes cut. A Blu-ray box set compiling all five films in the series was released on March 21, 2013 to celebrate its 40th anniversary. 
 David Kaplan, Kenta Fukasaku, Kiyoshi Kurosawa, a Toei producer and a biographer among others.   

==References==
 

==External links==
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 