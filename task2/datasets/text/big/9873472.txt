Quantum Quest: A Cassini Space Odyssey
{{Infobox film
| name           = Quantum Quest:  A Cassini Space Odyssey
| image          = Quantum Quest.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Harry Kloor Daniel St. Pierre
| producer       = Harry Kloor Rayna Napali Helen Pao-Yun Huang Ellen Goldsmith-Vein
| writer         = Harry Kloor
| narrator       =  Doug Jones Abigail Breslin Spencer Breslin Robert Picardo Brent Spiner
| music          = Shawn K. Clement
| cinematography = Christopher Courtois
| editing        = Dan Gutman
| studio         = Jupiter 9 Productions
| distributor    = Jupiter 9 Productions
| released       =  
| runtime        = 45 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Quantum Quest: A Cassini Space Odyssey (originally titled as 2004: A Light Knights Odyssey) is a 2010 animated educational sci-fi adventure film, 
{{cite news
|url=http://blogs.techrepublic.com.com/geekend/?p=3069
|title=What geek movie is worth paying theater prices to see?|last=Nash 
|first=Nicole Bremer
|date=August 17, 2009
|publisher=Tech Republic Harry Doc Dan St. Pierre, as a science fiction film that takes the viewer on an atomic adventure in space. 
{{cite web 
  | author = 
  | title = About the movie
  | publisher = qqthemovie.com
  | date = 
  | url = http://www.qqthemovie.com/Story.aspx
  | accessdate =2008-10-31 }}
 
 Michael York, and James Earl Jones, 
{{cite web 
  | author = 
  | title = (archived) SMG in an Animated TV Film : 2004 A Light Knight’s Odyssey
  | publisher = scifiwebchannel.com
  | date = September 1, 2003
  | url = http://www.whedon.info/SMG-in-an-Animated-Tv-Film-2004-A.html
  | accessdate =2008-10-31 }}
  
{{cite web 
  | author = Mark Scarboro
  | title = Scobra Aka Mark scarboro
  | publisher = nextcat.com
  | date = 
  | url = http://www.nextcat.com/scobra
  | accessdate =2008-10-31 }}
  
{{cite web 
  | author = 
  | title = Quantum Quest: A Cassini Space Odyssey (2010)
  | publisher = turckcealyatzi.com (Turkish)
  | date = 
  | url = http://www.turkcealtyazi.org/mov/0312305/quantum-quest-a-cassini-space-odyssey.html
  | accessdate =2008-10-31 }}
  
{{cite web 
  | author = 
  | title = Quantum Quest: A Cassini Space Odyssey 
  | publisher = cine5x.com (Spanish)
  | date = 
  | url = http://www.cine5x.com/pelicula/quantum-quest_-a-cassini-space-odyssey/
  | accessdate =2008-10-31 }}
  
{{cite web 
  | author = Anthony Pascale
  | title = Comic Con 08: Chris Pine Talks Quantum Quest, Star Trek and Shatner
  | publisher = trekmovie.com
  | date = July 25, 2008
  | url = http://trekmovie.com/2008/07/25/comic-con-08-chris-pine-talks-quantum-quest-star-trek-and-shatner/
  | accessdate =2008-10-31 }} Cassini Huygens major actors voices to the characters, the film was initiated by JPL/NASA through a grant from Jet Propulsion Lab via the international Cassini Huygens mission to Saturn.  
{{cite web 
  | author = 
  | title = Quantum Quest
  | publisher = jupiter9productions.com
  | date = 
  | url = http://jupiter9productions.com/news.aspx
  | accessdate =2008-11-01 }}
  
{{cite web 
  | author = Josh Tyler
  | title = Thursday Schedule For San Diego Comic Con: Quantum Quest
  | publisher = cinemablend.com
  | date = July 8, 2008
  | url = http://www.cinemablend.com/new/Thursday-Schedule-For-San-Diego-Comic-Con-9446.html
  | accessdate =2008-10-31 }}
 

==Synopsis== Cassini Saturn Huygens Titan sun filter sun spots. spin a tale, but as a mentor, this one, while entertaining, is also educational. 
{{cite web 
  | author = 
  | title = Quantum Quest synopsis
  | publisher = inbaseline website
  | date = 
  | url = http://www.inbaseline.com/project.aspx?project_id=152864
  | accessdate =2008-11-01 }}
 
 transition to the Sun, and watching a Sun Spot spin into view, we dive into and pass through the chromosphere, and head toward the Solar core|center. The mentor lays down the basis of our story as we travel through the Sun. He explains that the "man in the sun is called the Core (voiced by William Shatner)" and spins a story how he is good and that his children "photons" are sent out to chase away the darkness, and bring life, warmth, and knowledge to Earth. He adds that the only being who does not like the Core is a creature called the "Void" (voiced by Mark Hamill) who is an ancient, malevolent, being who existed before the Big Bang, and who sits at the edge of the solar system, scheming to stop the Core. 
 plasma wall, into "The Hall of Destiny" where we are introduced to the main character in our story – David (voiced by Chris Pine), a young Photon who should have begun his travel to the surface a million years ago. The Core, tries to get him moving on his journey, but David is fearful. 

We cut to the Kuiper Belt where a large space battle rages between two fleets – one composed of protons, and the opposing fleet of antiproton|anti-protons. A character we call "The Ranger (voiced by Amanda Peet), who is a neutrino Reconnaissance|scout, makes it through the battle lines and heads to the sun. 

Back in the Sun, David is kicking Solid Light sculptures into a plasma fountain. The Ranger crashes through the wall into David, followed by some imaginary creatures called Gell-Mann ghosts. The Ranger is trying to get to Saturn (other side of the Sun), to deliverer a message to the Cassini Space Core Commander. The Gell-Mann ghosts (nasty creatures) surround them. The only way to escape is to give the message scroll to David, and ask him to deliver it while the Ranger holds the ghosts at bay. David sails through the sun and encounters a "surfer dude" Solar Proton, Jammer (voiced by Hayden Christensen), who surfs the solar winds. 
 free quarks. Milton teaches David introductory material about electrons, protons, quarks, etc. The two evade the evil/comical forces of the Void, until they get captured by the "Dark Side of the Moon". 
 fundamental modes basic quantum concepts. The message scroll is partially damaged, so David does not know who the space commander is, but he knows that he needs to get to the Cassini Space craft, because something bad will happen. 
 radiation bands used for remote sensing, etc. In the end, they save the antenna of the ship from a beam of anti-matter sent by the Void. They do this with the help of Jammer. David discovers that he is the Space Core Commander (all this time, searching for himself). He leads the transmission back to Earth, and through a system of antennas, is processed, and as Jeana accesses the Web page of JPL, he flies through the screen and enters Jeanas eye. 

==Cast (voices)==
The films cast changed in 2008, reflecting a new script and detailing a host of new discoveries.
;Original cast, inception through 2007:
 
*John Travolta as Dave
*Sarah Michelle Gellar as Rayna Michael York as the Core
*Jim Meskimen as Ignorance David Warner as Void
*Samuel L. Jackson as Fear
*Anne Archer as Gal 2000
*Christian Slater as Jammer
*Robert Picardo as Milton
*Tait Ruppert as Moronic
*Lacey Chabert as Jeanna
*James Earl Jones as The Professor
*Casey Kasem as Himself
 
;Cast as of 2008:
 
*Chris Pine as Dave
*Amanda Peet as Ranger
*Samuel L. Jackson as Fear
*Hayden Christensen as Jammer
*James Earl Jones as the Admiral
*Sandra Oh as Gal 2000
*William Shatner as the Core
*Robert Picardo as Milton
*Brent Spiner as Coach MacKey
*Mark Hamill as Void
*Jason Alexander as Moronic
*Tom Kenny as Ignorance
*Neil Armstrong as Dr. Jack Morrow
*Abigail Breslin as Jeana Doug Jones as Razer / Zero Jason Harris as Announcer
*Casey Kasem as Himself
*Gary Graham as Green
*Spencer Breslin as Anthony
*Janina Gavankar as Nikki
*Herb Jefferson, Jr. as Pilot
 

==Background==
Though other missions have been seen in film projects, this is the first where NASA initiated the project with an independent filmmaker in order to create an animated fiction film to be based upon on-going missions, science, and discovery.     The 3D computer graphics|3D, large format Computer-generated imagery|CGI-animated educational film interweaves actual space imagery captured from ongoing NASA missions with an adventure, set in an imaginary universe and intended to create a family-friendly film experience that entertains while educating viewers about our solar system.    
{{cite web 
  | author = 
  | title = Stargates Commander Makes Movie For Genius Kids
  | publisher = io9.com
  | date = 
  | url = http://io9.com/5023056/stargates-commander-makes-movie-for-genius-kids
  | accessdate =2008-10-31 }}
 
The film was tied into 7 on-going space missions and includes footage from the Cassini–Huygens, Solar and Heliospheric Observatory|SOHO, Mars Odyssey, Mars Express, Venus Express, and Mercury Messenger space missions.    
Quantum Quest will be released in both IMAX format and regular theaters worldwide.    The film is 45 minutes long and will integrate pre and post theater educational program for K – 12 students that exceed Federal and State educational guidelines.   

Setting yet another precedent, Quantum Quest is the first and only film featuring Neil Armstrong, the first man to walk on the Moon, who lends his voice to Dr. Jack Morrow, a character who serves as one of four distinguished education ambassadors. 
{{cite web
  | title = Comic Con 08: Chris Pine Talks Quantum Quest, Star Trek and Shatner  
  | author = Anthony Pascale
  | publisher = trelmovie.com
  | date = July 25, 2008 
  | url = http://trekmovie.com/2008/07/25/comic-con-08-chris-pine-talks-quantum-quest-star-trek-and-shatner/
  | accessdate =2008-11-11 }} Distinguished Service Medal; {{cite web 
  | author = Charles Kohlhase (NASA)
  | title = professional milestone: 2007-08 NASA/JPL coordinator for $10M IMAX edutainment production "Quantum Quest – A Cassini Space Odyssey" underway by Digimax Studios in Taiwan
  | publisher = artshow.com
  | date = 
  | url = http://www.artshow.com/kohlhase/bio.html
  | accessdate =2008-10-31 }}  
   Anousheh Ansari, the world’s first female private space explorer and serial entrepreneur and co-founder and chairman of Prodea Systems, 
{{cite web
  | title =Interview with Anousheh Ansari, the First Female Space Tourist 
  | author = Sara Goudarzi
  | publisher = space.com
  | date = September 15, 2006
  | url = http://www.space.com/missionlaunches/060915_ansari_qna.html
  | accessdate =2008-11-11 }}
  and Peter Diamandis|Dr. Peter Diamandis, Chairman and CEO of the X PRIZE Foundation (which awarded the $10M Ansari X PRIZE), CEO of Zero Gravity Corporation, Chairman & Co-Founder of the Rocket Racing League and co-Founder of the International Space University. 
{{cite web
  | title = Dr. Peter Diamandis – Biography
  | publisher = californiaspaceauthority.org
  | date = 
  | url = http://www.californiaspaceauthority.org/conference2006/bios/diamandis_peter.pdf
  | format = PDF
  | accessdate =2008-11-11 }}
 

==Development==
Dr. Kloor started development of the Quantum Quest film project in 1996, a year before the launch of the Cassini–Huygens mission.   In 2007, Digimax, Inc., one of Asia’s leading 3D animation studios, committed to financing the film and providing animation.  Actual production on the film could not begin until the Cassini/Huygens (a $3.5 billion) reached its target and sent back its discoveries.   Final images and radar data, providing a partial radar map, were released by NASA in 2008, enabling the film to proceed.  Quantum Quest is a precedent-setting film, combining state-of-the-art CGI with real images and radar data from the ongoing Cassini-Huygen, SOHO, Stereo mission to the Sun, Mars Odyssey, Mars Express, Venus Express, and Mercury Messenger space missions, representing billions of dollars of NASA investment.
 production task stellar voice voice cast, produced all talent recordings and, in concert with the film’s composer Shawn Clement, put together the post sound and music package. 
{{cite web
  | title = 3D Feature Animation
  | publisher = digimax website
  | date = 
  | url = http://www.digimax.com.tw/3d
  | format = 
  | accessdate =2008-11-06 }}
  
{{cite web
  | title = about Digimax
  | publisher = digimax website
  | date = 
  | url = http://www.digimax.com.tw/about_us
  | format = 
  | accessdate =2008-11-06 }}
   The film will be distributed and marketed in Asia by Digimax, and by Jupiter 9 Productions in all other territories. 

==Music==
The score to Quantum Quest: A Cassini Space Odyssey was composed by Shawn K. Clement,  who recorded his score with an 80-piece ensemble of the Skywalker Symphony Orchestra at the Skywalker Scoring Stage north of San Francisco. Clement integrated sounds and space noise provided by NASA into the music, and served as Post Audio Producer on the film.   

== Release == Doug Jones and Jason Alexander.  
{{cite web 
  | author = Fred Topel
  | title = Doug Jones interview
  | publisher = craveonline.com
  | date = July 10, 2008
  | url = http://www.craveonline.com/articles/filmtv/04650990/3/hellboys_doug_jones.html
  | accessdate =2008-10-31 }}
  
{{cite web 
  | author = Carrie Specht
  | title = Comic-Con for the Movie Fan: Quite Underwhelming
  | publisher = filmjerk.com
  | date = July 31, 2008
  | url = http://www.filmjerk.com/news/article.php?id_new=558
  | accessdate =2008-10-31 }}
  
{{cite web 
  | author = 
  | title = Quantum Quest": New animation film with Shatner
  | publisher = shatner-news.com
  | date = July 26, 2008
  | url = http://www.en.shatner-news.com/newsarchive/news_archive=01.php
  | accessdate =2008-10-31 }}  
 

The film was released in Asia in the fall (September/October) of 2010.  It had IMAX screenings at the Kentucky Science Center in Louisville, Kentucky from January 23, 2011, {{cite web 
| url=http://www.louisvillescience.org/site/imax-article-coming-soon/quantum-quest.html 
| title=Quantum Quest Opens January 23, 2011  Louisville Science Center 
| accessdate=April 19, 2011}}  through June 10, 2011. {{cite web 
| url=http://www.louisvillescience.org/site/imax-article/may-23-through-june-10.html 
| title=Louisville Science Center IMAX Schedule: May 23 – June 10, 2011  Louisville Science Center 
| accessdate=April 19, 2011}} 

==Quotes==
  animated film". 
{{cite web 
  | author = Mike Szymanski
  | title = Picardo Voices Quantum Quest
  | publisher = scifi.com
  | date = 
  | url = http://www.scifi.com/scifiwire/index.php?id=57193 archiveurl = archivedate = 2008-08-04}}
   "This is the kind of project I am really proud to be a part of because it really encourages people to become passionate about space and space science." 

Chris Pine: (stated) that making the film "was a blast" and that he has always wanted to do voice work.   He also noted how in making the film he has learned a lot about science, and photons and neutrinos, something that the film tries to impart to the audience. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 