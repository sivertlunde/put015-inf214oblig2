Mozart and the Whale
{{Infobox film
| name = Mozart and the Whale
| image = Mozart and the whale.jpg
| alt = 
| caption = Theatrical release poster
| director = Petter Næss
| producer = Danny Dimbort Manfred D. Heid Frank DeMartini
| writer = Ronald Bass
| starring = Josh Hartnett Radha Mitchell
| music = Deborah Lurie
| cinematography = Svein Krøvel
| editing = Lisa Zeno Churgin Miklos Wright
| studio = Nu Image
| distributor = Millennium Films
| released =  
| runtime = 92 minutes
| country = United States
| language = English
| budget = $12 million
| gross = $84,447
}} romantic comedy-drama film starring Josh Hartnett and Radha Mitchell, and directed by Petter Næss.

==Synopsis==
The film tells the story of two people with Asperger syndrome (a form of high-functioning autism|autism). Donald runs a small self-help group for people on the autism spectrum who are more affected by their autism than he is. Isabelle is referred to the group by her therapist. Mozart and the Whale is a fictional account, using characters loosely based on the real-life relationship of Jerry Newport and Mary Meinel (now Mary Newport).

==Plot==
Donald Morton (Josh Hartnett) is a taxi driver and drives two Japanese passengers and his pet cockatiel around Spokane, Washington (state)|Washington. Distracted, he bumps into the back of a florists van and damaging his stock. Unfazed, Donald and his budgie take their groceries and leave, abandoning his taxi cab and passengers. He takes his groceries to the self-help group for autistic adults. Before they head to the park to meet another autistic group, he tells one member, Gracie (Rusty Schwimmer), to gather the women and hell gather the guys to practice telling personal stories, but keeps getting distracted by performing mathematical sums of the microwaves depleting numbers. He notices that Isabelle Sorenson (Radha Mitchell), a new name, has signed up and tells Gracie to let her go first.

At the park, Isabelle tells of a childhood memory to the women: she saw that her parents were happy that an Olympian had broken a record, so in order to please her parents, and taking what she heard literally, she broke their music records. Donald tells his story to the men about his ability to do complex sums but couldnt make friends. Isabelle goes on to tell of when she was raped when she went hitch-hiking. This causes Gracie to laugh manically. Heard by Donald, he tries to calm an angry Isabelle down and they find that they have much in common and take a liking to each other.

In the self-help group, after Isabelle talks to Gregory (John Carroll Lynch), a man who is writing notes on his notepad, about Donald, he calls Donald over to ask her to escort him to the Halloween party. Before Donald can, Isabelle asks Donald out for lunch. They go to the zoo the following day, and in response to Donald asking on behalf of Gregory, Isabelle asks Donald to escort her. They agree to meet with their costumes on in the evening. Donald dresses as a whale but is hesitant about going and leaves Isabelle, dressed as Mozart, waiting. She goes to his apartment and they both walk around the town talking until the final bus is due, and they share their first kiss.

Unsure when to call, Donald leaves multiple messages on her phone until she finally answers and they go to the amusement park. In the ring toss, the clanging of the metal rings hitting the bottles and the bell ringing cause her to scream and collapse on the floor. He takes her back to his filthy apartment and they agree to sleep together. The following day at the self-help group, Gregory accuses Donald of exploiting his position for sexual favors. Isabelle makes herself liked by going with Bronwin (Erica Leerhsen), who learns that her father has blood cancer, to wait for her parents to pick her up.

Isabelle takes the liberty of cleaning his apartment while he goes shopping. When he returns, hes horrified to see that everything is different; the piles of newspaper are stacked neatly, rotting food from the fridge is thrown away and has a new shower curtain. He gets angry at Isabelle for changing everything. He later leaves a number of apologetic messages on Isabelles phone. The next day, he goes to the hair salon where Isabelle works as a hair stylist to apologize in person, and Isabelle forgives him, introducing him as her boyfriend to the staff.

Isabelle shows Donald an abandoned rooftop, calling this a place where people who dont know where they belong can belong. She suggests that they can buy a house and her therapist has organised a job interview for a statistic analyst post at a university. He gets the job and they move into their new house, making it their own.

Donald tells Isabelle that he wants everything to be "nice" for when his boss, Wallace (Gary Cole) comes for dinner. Believing that he thinks that she doesnt keep the house "nice," Isabelle spites Donald by keeping the pets uncaged, much to Donalds shock when he returns, and she maintains extroverted behavior and tells of her off-the-wall plans for the house. Donald explodes, but when Isabelle says that they are both crazy, he retaliates by telling her that she is crazier, which leads to her throwing him out. He stays with Gregory in his house, and after listening to an answer message that Isabelles rabbit, Bongo, has died, he runs to comfort her. Isabelle suggests that they should just be friends.

Donald invites Isabelle to a restaurant, where he proposes to her, much to Isabelles dismay. She leaves abruptly back home and overdoses. Donald returns just in time to take her to the hospital, where Isabelles psychiatrist advises him to leave her alone, testing his willpower to refrain from calling her.

Donald sees Isabelle leaving the university and follows her to the abandoned rooftop, where he expresses that the only nice thing he had left to give her was not to call, to find that Isabelle was waiting for his call and she missed him. They express their true love with an embrace and kiss. The movie ends with the happy couple in their home, enjoying Thanksgiving dinner with the self-help group.

==Cast==
* Josh Hartnett as Donald Morton
* Radha Mitchell as Isabelle Sorenson
* Rusty Schwimmer as Gracie
* Erica Leerhsen as Bronwin
* Gary Cole as Wallace
* Allen Evangelista as Skeets
* Nate Mooney as Roger Sheila Kelley as Janice
* John Carroll Lynch as Gregory
* Mercedee Smith as young Isabelle
* Sharif Shawkat as young Donald

==Production==
The screenplay was written by Ron Bass, who also wrote Rain Man, a film about an individual with autism. Bass is said to have been inspired by a 1995 article in the Los Angeles Times.

The film was previously a DreamWorks vehicle and was to have been directed by Steven Spielberg with Robin Williams and Téa Leoni as stars.  But other work commitments meant that Spielberg could not film it in the available time slots.  North By Northwest    picked up the film finishing it for its release in 2005.

Parts of this film were shot on the campus of Gonzaga University, and Cat Tales, a large-cat preserve in Spokane, Washington.  

==Distribution==
The film struggled to find a theatrical distributor in the USA. The major reason for this was a lack of public support by prominent cast members who did not like the final version.  The studio tried to distribute it in the USA in April 2004 but it did not go farther than a month in Spokane, Washington, where the film was made. The film is available on DVD in a number of countries and became available in the USA in that form on December 12, 2006.

==Reception==
The film has been criticized for perpetuating the common and incorrect media stereotype that people on the autism spectrum typically have savant skills. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 