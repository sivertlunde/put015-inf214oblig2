Your Witness (film)
{{Infobox film
| name           = Your Witness
| image          = 
| image_size     = 
| caption        =  Robert Montgomery Joan Harrison
| writer         = Hugo Butler   Ian McLellan Hunter
| narrator       =  Robert Montgomery   Leslie Banks Felix Aylmer  Andrew Cruickshank
| distributor    = Warner Brothers
| released       = 6 March 1950
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British drama Robert Montgomery. It also featured Leslie Banks, Felix Aylmer and Andrew Cruickshank.  A leading American lawyer travels to London to defend an old friend from the Second World War who is facing a charge of murder.  It was the last film of Leslie Banks.

==Cast== Robert Montgomery - Adam Heyward
* Leslie Banks - Colonel Summerfield
* Felix Aylmer - The Judge KC
* Patricia Cutts - Alex Summerfield
* Harcourt Williams - Richard Beamish
* Jenny Laird - Mary Baxter  
* Michael Ripper - Samuel Sam Baxter  
* Ann Stephens - Catherine Ann Sandy Summerfield 
* Wylie Watson - Mr. Widgery, Red Lion Proprietor  
* Noel Howlett - Martin Foxglove KC, Sams Barrister   James Hayter - Prouty John Sharp - Police Constable Hawkins
* Shelagh Fraser - Ellen Foster
* Dandy Nichols - Waitress
* Stanley Baker - Sergeant Bannoch
* Erik Chitty  - Clerk of the Court
* Amy Dalby - Mrs. Widgely
* Wensley Pithey - Alfred 
* Hal Osmond - Taxi Driver

==References==
 

 

 
 
 
 
 
 

 