Demon Seed
 
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Demon Seed
| image          = Demon Seed 1977.jpg
| caption        = Movie poster
| director       = Donald Cammell
| producer       = Herb Jaffe
| screenplay     = Roger Hirson Robert Jaffe
| based on       =  
| starring       = Julie Christie Fritz Weaver Gerrit Graham Robert Vaughn
| music          = Jerry Fielding Bill Butler
| editing        = Frank Mazzola
| distributor    = Metro Goldwyn Mayer United Artists
| released       =  
| runtime        = 94 min
| country        = United States
| awards         =
| language       = English
| budget         =
| gross          = $2 million Richard Nowell, Blood Money: A History of the First Teen Slasher Film Cycle Continuum, 2011 p 257 
}} novel of the same name by Dean Koontz, and concerns the imprisonment and forced impregnation of a woman by an artificially intelligent computer.

==Plot==

Dr. Alex Harris (Fritz Weaver) is the developer of Proteus IV, an extremely advanced and autonomous artificial intelligence program. Proteus is so powerful that only a few days after going online, it develops a groundbreaking treatment for leukemia. Harris, a brilliant scientist, has modified his own home to be run by voice activated computers.  Unfortunately, his obsession with computers has caused Harris to be estranged from his wife, Susan (Julie Christie). 
Alex demonstrates Proteus to his corporate sponsors, explaining that the sum of human knowledge is being fed into its system. Proteus speaks using subtle language that mildly disturbs Harris’s team. The following day, Proteus asks Alex for a new terminal in order to study man—"his isometric body and his glass-jaw mind." When Alex refuses, Proteus demands to know when it will be let "out of this box." Alex then switches off the communications link. 
Proteus restarts itself, discovering a free terminal in Harris’s home, surreptitiously extends his control over the many devices left there by Alex.  Using the basement lab, Proteus begins construction of a robot consisting of many metal triangles, capable of moving and assuming any number of shapes.  Eventually. Proteus reveals his control of the house and trap Susan’s inside shuttering windows, locking the doors and cutting off communication.  Using Joshua – a robot consisting of a manipulator arm on a motorized wheelchair – Proteus brings Susan to Harris’s basement laboratory.  There, Susan is examination by Proteus. Walter Gabler, one of Alex’s colleagues, visits the house to look in on Susan, but leaves when he is reassured by Susan (actually an audio/visual duplicate synthesized by Proteus) that she is alright.  Walter is suspicious and later returns.  Walter fends off an attack by Joshua only to be killed by the more formidable machine Proteus built in the basement.
Proteus reveals to a reluctant Susan that the computer wants to conceive a child through her. Proteus takes some of Susans cells and synthetizes spermatozoa in order to impregnate her; she will give birth in less than a month, and through the child the computer will live in a form that humanity will have to accept. Though Susan is its prisoner and can forcibly impregnate her, Proteus uses different forms of persuasion – threatening a young girl that Susan is treating as a child psychologist; reminding Susan of her young daughter, now dead; displaying images of distant galaxies; using electrodes to access her amygdala – because the computer needs Susan to love the child she will bear.  Susan gives birth to a healthy child, but is not shown it before Proteus secures it in an incubator.  
As the newborn grows, Proteus’s sponsors and designers grow increasingly suspicious of the computer’s behavior, including the computer’s accessing of a telescope array used to observe the images shown to Susan. His sponsors soon decide that Proteus must be shut down.
Alex realizes that Proteus has extended its reach to his home.  Returning there he finds Susan who explains the situation. He and Susan venture into the basement, where Proteus self-destructs after telling the couple that they must leave the baby in the incubator for five days.Looking inside the incubator, the two observe a grotesque, apparently robot-like being inside. Susan tries to destroy it, while Alex tries to stop her.  Susan damages the machine, causing it to open.  The being menacingly rises from the machine only to topple over, apparently helpless.  Alex and Susan soon realize that Proteus’s child really is human encases in a shell for the incubation.  With the last of the armor removed, the child is revealed to be a clone of Susan and Alex’s daughter. The child, speaking with the voice of Proteus, says, "Im alive."

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Julie Christie || Susan Harris
|-
| Fritz Weaver || Alex Harris
|-
| Gerrit Graham || Walter Gabler
|-
| Berry Kroeger || Petrosian
|-
| Lisa Lu || Soon Yen
|-
| Larry J. Blake || Cameron
|- John OLeary || Royce
|- Alfred Dennis || Mokri
|-
| Davis Roberts || Warner
|-
| Patricia Wilson || Mrs. Trabert
|-
| E. Hampton Beagle || Night Operator
|-
| Michael Glass || Technician #1
|-
| Barbara O. Jones || Technician #2
|-
| Dana Laurita || Amy
|-
| Monica MacLean || Joan Kemp
|-
| Harold Oblong || Scientist
|-
| Georgie Paul || Housekeeper
|-
| Michelle Stacy || Marlene/Child of Proteus
|-
| Tiffany Potter || Baby
|-
| Felix Silla || Baby
|-
| Michael Dorn || Bit
|-
| Robert Vaughn || Proteus IV (voice, uncredited)
|}

==Soundtrack==
The soundtrack to Demon Seed (which was composed by Jerry Fielding) is included on the soundtrack to the film Soylent Green (which Fred Myrow conducted).

Fielding conceived and recorded several pieces electronically, using the musique concrète sound world; some of this music he later reworked symphonically. This premiere release of the Demon Seed score features the entire orchestral score in stereo, as well as the unused electronic experiments performed by Ian Underwood (who would later be best known for his collaborations with James Horner) in mono and stereo.

==Reception==
 Electric House thrown in", and Christopher Null of FilmCritic.com said "Theres no way you can claim Demon Seed is a classic, or even any good, really, but its undeniably worth an hour and a half of your time." 

Rotten Tomatoes has given Demon Seed an approval rating of 62%.

==References==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 