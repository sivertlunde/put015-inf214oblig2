The Golem (1915 film)
{{Infobox film
|  name           = The Golem
|  image          =Golem Weneger 2.jpg
|  caption        =Scene from film
|  writer         = Paul Wegener  Henrik Galeen
|  starring       = Paul Wegener Rudolf Blümner Carl Ebert Henrik Galeen Lyda Salmonova
|  director       = Paul Wegener  Henrik Galeen
|  producer       = Hanns Lippmann
|  distributor    =
|  released       =  
|  runtime        = 60 min. German intertitles
|  country        = German Empire
|  budget         =
|  music          =
|  awards         =
|}}

Der Golem ( , shown in the USA as The Monster of Fate) is a 1915   (1920). David Brooks, writing as a columnist for Minnesota Daily, said the film "deals with the tragic issues in life."

==Plot==
In modern times, an antiques dealer (Henrik Galeen) finds a golem (Paul Wegener), a clay statue brought to life by a rabbi four centuries earlier. The dealer resurrects the golem as a servant, but the golem falls in love with the dealers wife. As she does not return his love, the golem commits a series of murders.

==Preservation status==
The   claimed in The Frankenstein Legend that "European film collector" Paul Sauerlaender tracked down "a complete print" in 1958; Baer is careful, however, to point out that "Glut provides no source for this information." 

==See also==
*List of films made in the German Empire (1895-1918)
*List of incomplete or partially lost films
The Golem, a soundtrack written and performed by Black Francis at the 2008 San Francisco International Film Festival for the 1920 film The Golem: How He Came into the World.

==References==
 

==External links==
* Fragments of  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 

 
 

 