City Rats
{{Infobox film
| name = City Rats
| image = City rats poster.jpg
| caption = Theatrical release poster
| writer = Simon Fantauzzo Natasha Williams Danny Dyer
| director = Steve M Kelly
| producer = William Borthwick Dean Fisher Mark Maclaine (The Silence) & Julia Johnson
| cinematography= Adam Levins
| editing = Ben King
| distributor = Revolver Entertainment
| released =  
| runtime = 94 minutes
| country = United Kingdom
| language = English
| budget = 
}} Natasha Williams and Danny Dyer.

==Cast==
* Tamer Hassan as Jim
* Ray Panthaki as Dean
* Danny Dyer as Pete
* Susan Lynch as Gina
* Kenny Doughty as Olly
* MyAnna Buring as Sammy
* James Lance as Chris Natasha Williams as Carol
* Jake Canuso as Marco Harper
* James Doherty as Trevor Philip Herbert as John the Cowboy
* Katrine De Candole as Chloe
* Vyelle Croom as Darryl
* Emily Bowker as Carla
* Mathew Baynton as Barrista

==Release==
The film premiered as part of the East End Film Festival at the Genesis Cinema Whitechapel on 24 April 2009.  The following Monday it was also released on DVD and Blu-ray, reaching number 6 in the UK DVD charts, as well going on to have a limited run in cinemas. 

==Music== Mark Maclaine Second Person and includes the track "Paper Umbrella". Other tracks were provided by Spencer Hickson.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
* 

 
 
 
 
 
 