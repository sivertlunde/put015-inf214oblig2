Boy Golden: Shoot to Kill, the Arturo Porcuna Story
{{Infobox film
| name           = Boy Golden
| image          = File: BoyGoldenFilmPoster.jpg to the Metro Manila Film Festival
| director       = Chito S. Roño
| producer       = Joven Tan
| screenplay     = Catherine O. Camarillo Guelan Varela-Luarca
| story          = Catherine O. Camarillo ER Ejercito ER Ejercito  KC Concepcion
| music          = Carmina Cuya
| cinematography = Carlo Mendoza
| editing        = Jason Cahapay Ryan Orduña Carlo Manatad
| studio         = Scenema Concept International, Inc.
| distributor    = Viva Films
| released       =  
| runtime        = 
| country        = Philippines
| language       = Filipino English
| budget         = 
| gross          = P 7.7 million
}}
 39th Metro Manila Film Festival.    

== Cast ==
=== Main Cast === Jeorge Estregan as Arturo "Boy Golden" Porcuna
*KC Concepcion as Marla "Marla Dy" De Guzman

=== Supporting Cast ===
*Joem Bascon
*John Estrada as Tony Razon
*Tonton Gutierrez
*Leo Martinez as Mr. Ho
*Gloria Sevilla as Aling Puring
*Eddie Garcia as Atty. Dante Sagalongos
*Jhong Hilario
*Baron Geisler as Datu Putla
*Roi Vinzon as Alias Tekla
*John Lapus
*Mon Confiado as Rachel Viego
*Dindo Arroyo 
* Derrick Monasterio as Anton 
*Dick Israel as Boy Bungal
*Deborah Sun
*Simon Ibarra
*Gerald Ejercito
*Dexter Doria
*Buboy Villar
*DJ Durano as Entong Intsik
*Marc Abaya 
*John Arcilla as Don Manuel del Viego 
*Roldan Aquino as Don Ricardo de Montiel
*Mathew Barrios as Atty. Andrade
*Paolo Serrano as Totoy Balantik
*Bembol Roco as Frederico delos Reyes
*Lloyd Samartino
*Mark Anthony Fernandez as Gerard Montiel
*Brandon Gepfer as Randy

== Reception ==
=== Critical response ===

=== Box office ===
The movie grossed P94.2 million at the box office by January 5, 2014.

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2013
| rowspan="1" align="left"| Metro Manila Film Festival   Best Float
| align="center"| Boy Golden: Shoot to Kill, the Arturo Porcuna Story
|  
|}

== References ==
 

== External links ==
*  
*  
 
 
 
 