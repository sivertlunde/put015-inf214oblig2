Broken Record (film)
 
{{Infobox film
| name           = Broken Record
| image          = 2015 Broken Record Poster.jpg
| alt            = 
| director       = Andy S. McEwan
| producer       = Andy S. McEwan Chris Quick Paul Michael Egan
| writer         = Andy S. McEwan
| narrator       =  John Gaffney Darren McColl
| music          = 
| cinematography = Paul Michael Egan
| editing        = Chris Quick
| studio         = Pentagram Productions UK
| distributor    = 
| released       =  
| runtime        = 11 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =
}}
Broken Record  is a short comedy film following a pair of removal men in East Kilbride who stumble upon an old trunk in a locked cupboard. The film was produced by Pentagram Productions UK and marked the directorial debut for Andy S. McEwan.

==Plot==
Set in East Kilbride in 1992, Broken Record tells the tale of Frasier & Tam who whilst working for a removal company stumble upon an trunk in a locked cupboard of an empty flat. The pair prise open the trunk to find it full to the brim of old gramophone records leading them to believe they have just stumbled upon a secret fortune. The lucky find is suddenly plunged into jeopardy when Jim, an on site painter, spots the trunk and wants in on the action. Frasier and Tam have no choice but to include Jim in on the deal but they quickly hatch a plan to steal the records for themselves.

Later that evening, the pair fake a break in at the house they were working in and steal the trunk. The following day, Frasier sets off to a local record shop to see what kind of price he can get for the records. However after inspecting just a handful of records, the record shop owner informs Frasier that he doesnt deal with gramophone records and suggests he contacts Cecil Hardcastle who is a collector and an expert in the field. Frasier visits Mr Hardcastle and shows him the trunk of records in the boot of his car. After examining the record, Cecil offers to buy a handful of the records but Frasier is horrified to find out that each record only has a value of two pence each.

Frasier calls round to Tams house to deliver the news and finds him in a top hat boasting about how he is going to the Ritz and that he has booked a holiday to Benidrom. Tam asks for his share of the money only to be presented with a handful of change. Questioning where the rest of the money is, Frasier explains that the records are worthless and that their chances of becoming millionaires are dead. Leaving Tam in a state of frustration, Frasier announces on his way out that he intends to throw the records in the bin.

The final scene shows a couple of bin men stumbling upon the trunk in a bin shed. After a quick inspection of the records one of the bin men says "These must be worth a fortune."

==Main Cast==
 , Darren McColl, Steven Patrick, Andy S. McEwan, Paul Michael Egan, Chris Quick]]
*Steven Patrick as Frasier John Gaffney as Tam
*Darren McColl as Jim
*Alan Cuthbert as Cecil Hardcastle
*Paul Massie as Record Shop Owner
*David Marshall as John
*Tony Quigley as Stevie
*Chris Quick as Radio Announcer (voice)
*Karen L. Quick as Betty (voice)

==Release and reception==
The film was released on the 3rd of April 2014 and had its first public screening in Glasgow.

Broken Record received positive reviews from critics. In the United Kingdom, Mary Palmer from MovieScramble said "This really is an enjoyable short. It is quick to the point and I genuinely laughed out loud at the ridiculousness of some of the scenes."  Internationally, the film received high praise with Mark Bell of Film Threat in the United States saying "It’s a pretty straightforward tale, the film looks great, and sounds pretty good overall. Broken Record is an amusing short about what happens when big dreams and grand plans don’t match up with what life has planned for you". 

In 2014, the film appeared in a number of film festivals including the Loch Ness Film Festival  in Inverness and the Portobello Film Festival in London. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 