Muay Thai Chaiya
{{Infobox film
| name           = Muay Thai Chaiya
| image          = Muay Thai Chaiya Thai poster.jpg
| caption        = The Thai theatrical poster.
| director       = Kongkiat Khomsiri
| producer       = Charoen Iamphungporn Apiradee Iamphungporn Kiatkamol Iamphungphorn Thanit Jitnukul
| writer         = Kongkiat Khomsiri
| starring       = Akara Amarttayakul Thawatchai Penpakdee Sonthaya Chitmanee Sarita Kongpech Sangthong Ket-U-Tong
| music          = 
| cinematography = Sayombhu Mukdeeprom
| editing        = 
| distributor    = Five Star Production
| released       =  
| runtime        = 
| country        = Thailand
| language       = Thai
| budget         = 
}} 2007 Thailand|Thai drama film about two talented muay Thai boxers, boyhood friends whose lives take divergent paths after they arrive in Bangkok. The film is the solo directorial debut by Kongkiat Khomsiri, who had previously been among seven directors on Art of the Devil 2, and had written the screenplay for The Unseeable.
 Thailand cinemas on August 30, 2007.   

==Plot==
Growing up in Chaiya, Surat Thani Province, three boys, Piak, Pao and Samor, are followers of Paos brother, Krang (Prawit Kittichanthira), a legendary muay Thai fighter who is taught by Paos father, Tew (Samart Payakaroon).

After an accident partially cripples Samor, Piak and Pao train as boxers under Tew, but the boxing school is broken up when Tew and Krang are recruited for a gym in Bangkok.

Eventually, Piak and Pao go to Bangkok themselves, bringing along Samor, and Sripai, a nurse who is engaged to Piak, but whom Pao secretly loves.

Piaks hot-headedness makes him a fierce fighter, but it is also a liability that costs him a fight and ends his career. He joins the world of underground bare-knuckles brawling, and he and Samor take on other jobs in the underworld as well, including helping out at go-go bar where the pretty Warn dances and seduces Piak. While working for the underworld, both of them ended up also doing dirty works for their boss.

Pao, meanwhile, begins training with his father, and works his way up the ranks, is put forward as a top boxer in a match against a fierce farang, Diamond Sullivan, which places Pao at odds with the gangsters whom Piak and Samor work for.KRU PAIK. Sripai later saw Piak in bed with Warn and went to find Pao. Pao and Sripai live together since while Piak took over the underground business once his boss was killed by another rival boss.

Eventually the film climax at Paos fight with Diamond. Pao was knocked down several times, but came victorious as he remembers the time three of them trained together as a young boy.

==Cast==
*Akara Amarttayakul as Piak
*Thawatchai Penpakdee as Pao
*Sonthaya Chitmanee as Samor
*Sarita Kongpech as Sripai
*Sangthong Ket-U-Tong as Warn
*Prawit Kittichanthira as Krangsuk
*Samart Payakaroon as Tew Don Ferguson as Diamond Sullivan

==Production==
Director Wisit Sasanatieng served as an uncredited art director on Muay Thai Chaiya. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 