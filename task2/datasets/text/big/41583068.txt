Montreal Stories
{{Infobox film
| name           = Montreal Stories Montréal vu par...
| image          = 
| alt            = 
| caption        = Theatrical release poster
| director       =  
| producer       =  
| writer         =  
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         =  
| distributor    = 
| released       = 1991
| runtime        = 125 minutes
| country        = Canada
| language       = 
| budget         = 
| gross          = 
}} anthology of six short films, one by each of the credited directors. 

==Cast== Maria del Mar, Sheila McCarthy, Monique Mercure, Jean-Louis Millette, Guylaine St-Onge, Raoul Trujillo and Guillermo Verdecchia.

==Segments==
*"Desperanto" (Rozema) — A young housewife from Toronto (Sheila McCarthy) explores Montreals nightlife. Jacques Viger.
*"La Dernière partie" (Brault) — Madeleine (Hélène Loiselle) tries to tell Roger (Jean Mathieu) she wants a divorce after forty years of marriage. 
*"En passant" (Egoyan) — A visitor to a conference arrives at the airport.
*"Rispondetemi" (Pool) — En route to the hospital in an ambulance after a car accident, Sarah (Anne Dorval) recalls her life.
*"Vue dailleurs" (Arcand) — At a diplomatic reception, an older woman (Domini Blythe) reminisces about a love affair in Montreal in 1967.

==See also==
*The Memories of Angels, a collage film about Montreal
*Cosmos (1996 film)|Cosmos, a 1996 film which assigned the same premise to six young emerging directors

==References==
 

== External links ==
* 
*  at the National Film Board of Canada

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 