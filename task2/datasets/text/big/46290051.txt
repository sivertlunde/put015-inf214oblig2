The Dead and the Damned 2
{{Infobox film
| name           = The Dead and the Damned 2
| image          =
| alt            =
| caption        =
| director       = Rene Perez
| producer       = Michael Flanagan
| writer         = Rene Perez
| starring       = {{plainlist|
* Richard Tyson
* Raven Lexy
* Jenny Allford
* Christopher Kriesa
}} 
| music          = Rene Perez
| cinematography = Rene Perez
| editing        = Rene Perez
| studio         =
| distributor    = Inception Media Group
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Dead and the Damned 2 (also The Dead, the Damned, and the Darkness and Tom Sawyer vs Zombies) is a 2014 American horror film written and directed by Rene Perez.  It was released direct-to-video on October 7, 2014, and is the sequel to the 2010 film The Dead and the Damned.

== Plot ==
In the future, Lt. Colonel Sawyer and a mute girl, Stephanie, attempt to survive in a post-apocalyptic land crawling with zombies.

== Cast ==
* Robert Tweten as Lt. Colonel Sawyer
* Iren Levy as Stephanie
* John J. Walsh as Wilson
* Richard Tyson as Sheriff
* Jenny Allford as Mrs. Sawyer
* Christopher Kriesa as Speaker of the House Gates
* Raven Lexy as mother

== Production ==
Filming took place in Redding, California. 

== Release ==
Inception Media Group released it on DVD and video on demand on October 7, 2014. 

== Reception ==
Mark Burger of Yes! Weekly wrote that it has a "few good moments, but by any title its nothing we havent seen a lot of recently".   Matt Boiselle of Dread Central rated it 2.5/5 stars and wrote, " f youre simply willing to accept this as another run-of-the-mill zombie shoot-em-up, you should give it a go."   Todd Martin of HorrorNews.Net wrote, "I really enjoyed The Dead and the Damned 2 and thought that it was one of the best zombie movies Ive seen in a very long time." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 