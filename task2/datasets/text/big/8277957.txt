The Dragon Ring
{{Infobox Film
| name           = Desideria e lAnello del Drago
| image          = Desideria.jpg
| caption        = DVD cover
| director       = Lamberto Bava
| producer       = Lamberto Bava Andrea Piazzesi
| writer         = Gianni Romoli
| starring       = Anna Falchi Franco Nero
| music          = Amedeo Minghi
| cinematography = Romano Albani
| editing        = Piero Bozza
| distributor    = 
| released       = 1994
| runtime        = 186 min
| country        = Italy Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Desideria e lAnello del Drago (also known as Desideria and The Dragon Ring) is an Italian fantasy mini-series directed by Lamberto Bava and starring Anna Falchi. The story is similar in style and direction as Lamberto Bavas more successful Fantaghirò series.

== Plot ==

Desideria is the eldest daughter of the Dragon King and Queen, rulers of a powerful conquering kingdom. Selvaggia is the magical adopted daughter of the Dragon King and Queen, and although she is constantly playing tricks to get her elder sister in trouble, she is frequently praised as being the smarter and better daughter.

As the eldest birth daughter of the Dragon King, Desideria is due to inherit the Dragon Ring, the highest symbol of power in the kingdom, but she can only do so once she has chosen a husband. Unfortunately, the prince that finally catches her attention is Prince Victor, a rebel prince who has fought against her father and is to be sentenced to death for his treachery.

Desideria helps Victor escape from the castle prisons, but her father catches her in the act and she is punished. The Dragon King decides to let all the princes in his conquered kingdoms fight in a tournament for her hand in marriage, but when Desideria sees that all the princes are selfish, cruel men, she runs away. When the Dragon King discovers that she has fled, he declares that Desideria has forfeited her right to the throne and Selvaggia will become the new prize. But unknown to him, the jealous Selvaggia wants to beat her elder sister completely.

Desideria eventually ends up in the desert, where she is saved from the Witches of the Sand by none other than Prince Victor himself. She learns from him and other rebels in his camp that her beloved father is actually a cruel and vicious ruler. Just as she agrees to fight at Victors side, Selvaggia casts a spell from the castle, causing Victor to fall in love with her and rush to join the tournament to win Selvaggias hand.

== Cast ==
{|class="wikitable"
|-
| Anna Falchi ||Desideria
|- Dragon King
|- Princess Selvaggia ("Wild" in English-dubbed version)
|- Prince Victor
|-
| Billie Zöckler ||Nurse
|- Dragon Queen
|- Prince Lisandro
|- The Fairy of Crystal Sword Lake
|- King Karl
|- Old Man
|- Princess Desideria at age 5
|- Princess Desideria at age 10
|- Princess Selvaggia at age 8
|- Prince Victor at age 6
|- Prince Sigismondo
|- Prince Merlik
|- Prince Parsel
|}

== Similarity to the Fantaghirò movies ==
The film has a few similarities with Lamberto Bavas more successful Fantaghirò movies, besides filming style and special effects. In this film Victor falls in love with Desideria without ever seeing her face, and he can only recognise her by her voice. In Fantaghirò, Prince Romualdo falls in love with Fantaghirò without properly seeing her face, and he can only recognise her by her eyes. Another similarity is that the wicked Selvaggia casts a spell to make Victor fall in love with her, so to hurt Desideria. In Fantaghirò the wicked Black Witch casts a spell to make Romualdo fall in love with her, so to hurt Fantaghirò. One more similarity is that both Desideria and Fantaghirò dress up as men so that they can fight in a tournament without anyone knowing that they are women.

==Reception==
The film received generally favorable reviews from critics, and currently has an 68% score at Rotten Tomatoes from the general audience and a 6,6 ou of 10 on IMDb.

== See also ==
* Fantaghirò

== External links ==
*  

 
 
 
 
 
 
 
 