LOL (Laughing Out Loud)
 
{{Infobox film
| name           = LOL (Laughing Оut Loud)
| italic title   = no
| image          = LOL Poster.jpg
| border         = yes
| caption        = French theatrical release poster
| director       = Lisa Azuelos
| producer       =  
| writer         =  
| starring       =   TF1 Films M6 Films Canal+
| distributor    = Pathé
| released       =  
| runtime        = 98 minutes
| country        = France
| language       = French
| budget         = €9,500,000   
| gross          = $60,203,395  
}}
 
LOL (Laughing Out Loud) is a 2008 French comedy film directed by Lisa Azuelos and starring Sophie Marceau, Christa Theret, and Alexandre Astier.    Written by Azuelos and Delgado Nans, the film is about a teenage girl whose life is split between her studies in a prestigious Parisian high school, her secret diary, her friends, boyfriends, her divorced parents, drugs, and sexuality. For her performance in the film, Sophie Marceau won the Monte-Carlo Comedy Film Festival Jury Prize for Best Actress in 2008. Christa Theret received a César Award Nomination for Most Promising Actress in 2010.    The letters "LOL" mean "laughing out loud" in text language.

==Plot==
Lola (Christa Theret) is a pretty teenage girl living with her mother Anne (Sophie Marceau), who is divorced from Lolas father, Alain (Alexandre Astier). Nicknamed "LOL" by her friends, Lola has been taking her first steps into teenage romance, dating a boy from her class named Arthur (Felix Moati). Following the summer break, Lolas life becomes complicated when Arthur tells her that he cheated on her over the summer and was dating her friend,lvis Lola decides to break things off with him and start seeing his close friend Mael (Jeremy Kapone). Lolas friends seem to enjoy complicating matters even more. Life at home has also become impossible with her mother, who has no idea what "LOL" means. She treats her teenage daughter like a child, wondering whatever happened to her sweet little daughter. Lola likes to play her mother and father against each other for her own advantage, but what she doesnt know is that Anne and Alain have begun dating again on the sly.

==Cast==
* Sophie Marceau as Anne
* Christa Theret as Lola
* Alexandre Astier as Alain
* Jérémy Kapone as Maël
* Marion Chabassol as Charlotte
* Lou Lesage as Stéphane
* Émile Bertherat as Paul-Henri
* Félix Moati as Arthur
* Louis Sommer as Mehdi
* Adèle Choubard as Provence
* Jade-Rose Parker as Isabelle de Peyrefitte
* Warren Guetta as David Lévy
* Jocelyn Quivrin as Lucas
* Françoise Fabian as Annes mother
* Christiane Millet as Charlottes mother
* Lise Lamétrie as the CPE
* Thaïs Alessandrin as Louise
* Tom Invernizzi as Théo
* Stéphanie Murat as Cathy
* Laurent Bateau as Romain
* Valérie Karsenti as Laurence
* Pierre Niney as Julien
* Jean-Claude Dauphin as the Minister
* Olivier Cruveiller as Maëls father
* Katia Caballero as Maëls mother
* Vincent Jasinskij as Léon
* Patty Hannock as Madame Claude, the English teacher
* Axel Kiener as the Maths teacher
* Étienne Alsama as the Ministers driver
* Virginie Lente as Lili
* Esmeralda Kroy as Paul-Henris mother
* Vivienne Vernes as Lilis mother
* Lucille OFlanagan-Le Cam as the Lady Di Englishwoman
* Sofía Rodríguez as Amaias mother
* Lisa Azuelos as the psychanalyst   

==Reviews==
A cultural critic writing for The Independent noted that the movie portrayed British culture in the way it is stereotypically imagined by many French to be, showing a small town outside of London where it "never stops raining. The streets are populated by middle-aged women in dowdy, floral dresses carrying garish umbrellas. For dinner, the French teenagers are served white bread, marmalade and pasta – on the same plate."    In France the movie was well received as depicting a generation of youngsters, such as other films in the 80s and the 90s had.

==Remake==
 
A remake was released in 4 May 2012. The film stars Miley Cyrus, Demi Moore, Ashley Greene, Adam Sevani, and Douglas Booth.   

==References==
 

==External links==
*  
*  
*   at uniFrance

 
 
 
 
 
 
 
 
 