Let's Scare Jessica to Death
{{Infobox film
| name           = Lets Scare Jessica to Death
| image          = Letscarejessica.jpg
| caption        = Theatrical release poster John Hancock
| writer         = John Hancock  (as Ralph Rose)  Lee Kalcheim  (as Norman Jonas) 
| producer       = Charles B. Moss Jr. William Badalato
| starring       = Zohra Lampert Barton Heyman Kevin OConnor Gretchen Corbett Alan Manson Mariclare Costello
| studio         = Paramount Pictures The Jessica Company
| distributor    = Paramount Pictures
| released       = 6 August 1971  (USA) 
| runtime        = 89 min.
| language       = English
| country        = United States
| music          = Orville Stoeber
| budget         = 
| box office     = 
}}
 horror film, directed by John D. Hancock and starring Zohra Lampert as Jessica.  It depicts the nightmarish experiences of a psychologically fragile woman in an old farmhouse on a Connecticut island. In 2006, the Chicago Film Critics Association pronounced Lets Scare Jessica to Death the 87th scariest film ever made. 

== Synopsis ==
Wearing just a nightgown, Jessica (Zohra Lampert) sits in a rowboat, staring at the water.
 Kevin OConnor) arrive, they are surprised to find a mysterious, red-haired drifter, Emily, (Mariclare Costello) already living there. When Emily offers to move on, Jessica invites her to dine with them and stay the night.

The following day, Jessica, seeing how attracted Woody is to Emily, asks Duncan to invite her to stay indefinitely. Jessica has begun hearing voices and seeing a mysterious blonde girl (Gretchen Corbett) looking at her from a distance then disappearing. Another time, her leg is grabbed by someone under the water in the cove where she was swimming. Jessica is afraid to talk about these things with Duncan or Woody.

Jessica becomes aware that Duncan also appears to be attracted to Emily and the men in town, all of whom are bandaged in some way, seem to be hostile towards them.

Duncan and Jessica decide to sell antiques found in their new home at a local shop, one is a silver-framed portrait of the Bishop family—father, mother, and daughter Abigail. The antique dealer, Sam Dorker (Alan Manson), tells them the story of how 20-year old Abigail Bishop drowned in 1880 just before her wedding day. Legend says that shes still alive, roving the island as a vampire. Jessica finds the story fascinating, but Duncan, afraid that hearing about such things will upset his wife, cuts Dorker short.

After returning to the farm, Jessica and Duncan go for a walk around their property. Emily is sitting on the front porch playing her lute, and Woody is out spraying the apple trees. Jessica finds a mole and Duncan offers to take it back to the house and put it in a jar while Jessica stays in the graveyard to do some grave rubbings. She is just about to do a rubbing of Abigail Bishops grave when she notices the blonde girl beckoning her to follow. The girl leads Jessica to a cliff, at the bottom of which lies Sam Dorker dead and bloodied. By the time Jessica finds Duncan, however, the body is gone. Did she really see it? Just then, Jessica notices the blonde girl standing on the cliff above them. Duncan sees her, too, and gives chase. He catches her, but when Jessica tries to ask her questions, the girl is either mute or wont talk. Suddenly, Emily walks up, and the girl runs off.

That evening at dinner, its obvious even to Woody that Duncan and Emily have eyes for each other. When Jessica retires early, Woody tells Duncan to take care of his wife. Reluctantly, Duncan goes up to their bedroom. Instead of taking care of his wife, however, he tells Jessica that she needs to return to New York to see her doctor again. Jessica becomes upset and tells Duncan that she wants to sleep apart tonight, so Duncan goes down to sleep on the couch where he is easily seduced by Emily.

Jessica finds the little mole has been repeatedly stabbed to death. Jessica denies having done it, but Duncan decides that its time to get help for her and drives into town in search of a phone. When Duncan arrives in town, he goes into a store to attempt to use a telephone, and a number of old men with bandages are seen following in inside.  

While Duncan is gone, Jessica goes up to the attic where she is surprised to come face to face with the silver-framed portrait of the Bishop family that they had sold just the previous day to Sam Dorker. She is astounded to see how closely Abigail Bishop resembles Emily.

Suddenly, Jessica sees Emily coming up the stairs. Jessica tries to keep her cool while Emily denies that she is Abigail, all the while she is stroking Jessicas feverish face. Jessica agrees to go with Emily for a cool swim in the lake, even though shes afraid of the water after being grabbed at the other day. As the two women sit at the end of the dock, Emily rubs suntan lotion on Jessica. Suddenly, Emily pushes Jessica into the water and jumps in after her. When Jessica panics, Emily apologizes for scaring her when all she wanted to do was have a little fun. Jessica wades towards shore and looks back to see that Emily has disappeared. She hears Emilys voice in her head and then sees her reddish-hair bobbing to the surface. When a pale white arm grabs her and tries to pull her underwater, Jessica fights to break loose and return to shore. She looks back at the water and watches Emily, pale and dressed in Abigails wedding gown, rising slowly from the water and walking towards her, all the while silently saying, "Stay, Jessica, stay. Follow me."

When Emily reaches Jessica and tries to bite her neck, Jessica runs back to the house, locks herself in her room, and waits for Duncan to return. Hours pass, during which Jessica can hear Emilys voice whispering to her. When Duncan has not returned by 5:00 p.m., Jessica changes out of her swimming suit and hitches a ride into town. While Jessica is on her way to town, Woody returns from working the fields to find that the only one at home is Emily. Although Woody doesnt like how Emily has been playing Duncan, he readily submits when she comes on to him. He gets a bite in the neck for his reward.

When Jessica gets into town, she sees Duncans car and asks about his whereabouts, but none of the men will admit to having seen him. Jessica notices that every man has cuts on his body and backs away...straight into the arms of Sam Dorker. She runs back toward the farm. When she can run no more, she collapses on the ground and lies there until it turns dark and she hears Duncan calling her name. Duncan drives her the rest of the way home, and they go upstairs to bed. Jessica notices a cut on Duncans neck. Jessica tries to convince herself that its only in her mind, but then she sees Emily brandishing a carving knife and leading the men from town. Jessica leaps from the bed and runs outside, knocking over Duncans bass case to see the bloody body of the blonde girl come tumbling out.

Jessica runs through the fields and comes across Woody on a tractor sprayer but when she gets closer, she sees Woody lying on the seat, his neck cut and bloody. At daybreak Jessica makes it to the ferry and tries to board, but the ferryman tells her, "The ferry isnt running for you." Jessica jumps into a nearby rowboat and paddles out into the lake. When a hand reaches into the boat from the water. She stabs the person in the back several times with a long pick. As the body floats away, pale and bloody, she sees that it is Duncan. Back on the shore, Emily/Abigail and the men from town can be seen watching her.

"I sit here, and I cant believe that it happened," Jessica says to herself in voice over, "and yet I have to believe it. Nightmares or dreams? Madness or sanity? I dont know which is which."

==Production and style==
The film was shot in Old Saybrook, Connecticut.  The village of Chester was used, as was the Chester–Hadlyme Ferry crossing the Connecticut River. 

Mariclare Costello was so loath to perform the scene in which her character kills Jessicas pet mole (which was actually played by a mouse) that she hid on the set when it was time to shoot. 

When Costello sings a folk song in Jessicas kitchen, the director and producers considered dubbing her voice with that of a professional singer. However, they later decided to keep Costellos voice as it was recorded.   

== Critical reception ==
 
AllMovie called it an "eerie low-budget chiller". 
 Time Out conducted a poll with several authors, directors, actors and critics who have worked within the horror genre to vote for their top horror films.  Lets Scare Jessica to Death placed at number 86 on their top 100 list. 

== Home releases ==
The film was released on VHS and Beta by Paramount in 1984.  It was released on DVD in August 2006 from Paramount Pictures, its original distributor.

==See also==
*Vampire film

== References ==
 

== External links ==
* 
*  

 

 
 
 
 
 
 
 
 
 