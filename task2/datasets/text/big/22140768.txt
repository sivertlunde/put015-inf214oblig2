The Story of Asya Klyachina
{{Infobox film
| name           = The Story of Asya Klyachina
| image          = The story of Asya Klyachina.jpeg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Andrei Konchalovsky
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Iya Savvina
| music          = 
| cinematography = Georgy Rerberg
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}}  Soviet movie. {{Cite web
| title = Istoriya Asi Klyachinoy, kotoraya lyubila, da ne vyshla zamuzh (1966)
| accessdate = 2009-03-26
| url = http://www.imdb.com/title/tt0060549/
}} 
The artistic director was Mikhail Romadin. {{Cite web
| title = Mikhail Romadin
| accessdate = 2009-03-24
| url = http://www.imdb.com/name/nm0738593/
}}  Shown briefly in 1967 under the title Asyas Happiness ( ), it was not released widely until 1987.

Director Andrei Konchalovsky won the Nika Award for best director for this black-and-white movie.

== References ==
 

==External links==
* 

 

 
 
 
 
 
 
 
 