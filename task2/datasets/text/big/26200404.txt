Cool Change (film)
 
 
{{Infobox film
| name           = Cool Change
| image          =
| image_size     =
| caption        =
| director       = George T. Miller
| producer = Geoff Burrowes Dennis Wright
| writer         = Patrick Edgeworth
| narrator       = Jon Blake Lisa Armytage Deborra-Lee Furness
| music          = Bruce Rowland
| cinematography = John Haddy
| editing        =
| studio = Burrowes Film Group
| distributor    =
| released       = 10 April 1986
| runtime        = 89 min.
| country        = Australia
| language       = English
| budget         = A$3.5 million 
| gross          = A$60,868 (Australia)
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}} Jon Blake and Lisa Armytage.  David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p224 

==Plot==
A park ranger is caught in a conflict between farmers and conservationists.

==Cast== Jon Blake as Steve
*Lisa Armytage as Joanna
*Deborra-Lee Furness as Lee David Bradshaw as James Hardwicke
*Alec Wilson as Bull Raddick

==Production==
The film was shot on location in Mansfield and the Victorian Alps. 

==Critical reception==
The critic from the Sydney Morning Herald called the movie "a spectacularly simplistic propaganda piece for the cattle farmers of the Victorian high plains". 

==Box office==
 Cool Change grossed $60,868 at the box office in Australia,  which is equivalent to $132,692 in 2009 dollars.

==References==
 

==External links==
* 
*  at TCMDB
*  at Screen Australia
 
 

 
 
 
 


 