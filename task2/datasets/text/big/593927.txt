The Rehearsal (film)
{{Infobox film
| name           = The Rehearsal
| image          = 
| image_size     = 
| caption        = 
| director       = Jules Dassin
| producer       = Melina Mercouri
| writer         = Jules Dassin
| narrator       = 
| starring       = Jules Dassin Olympia Dukakis Laurence Olivier
| music          = Mikis Theodorakis
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1974
| runtime        = 92 min.
| country        = U.K. / Greece English / Greek
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1974 film produced by Jules Dassin that is a cinemagraphic indictment of the Greek military junta of 1967&ndash;1974.

==Cast==
*Jules Dassin
*Olympia Dukakis
*Stathis Giallelis
*Lillian Hellman
*Melina Mercouri
*Arthur Miller
*Laurence Olivier
*Giorgos Panoussopoulos
*Maximilian Schell
*Mikis Theodorakis
*Jerry Zafer
*Stephen Diacrussi

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 