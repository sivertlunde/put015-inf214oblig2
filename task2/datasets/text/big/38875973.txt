Reincarnated (film)
{{Infobox film
| name           = Reincarnated
| image          = Reincarnated Snoop Lion documentary.jpg
| alt            = 
| caption        = Film poster
| director       = Andy Capper
| producer       = Codine Williams Justin Li
| writer         = 
| starring       = Snoop Lion
| music          = Snoop Lion
| cinematography = Nick Neofitidis Willie Toledo William Fairman
| editing        = Bernardo Loyola Dave Gutt Emily Wilson Jared Perez Vice Films Snoopadelic Films
| distributor    = Vice Films
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Vice Magazine.  

==Background==
From the start, Snoop was known for his gangsta raps, G-funk beats, and reckless lifestyle. His popularity started back when he was a member of Death Row Records, where he released his debut album Doggystyle. The album sold more copies than any debut album ever had, making “Snoop Doggy Dog” a well-known name.   Though he was famous for being “gangsta,” Snoop had shown that he wanted peace as well; especially after he just missed a conviction for murder in 1996 which helped him further himself even more from all the conflict happening in the industry around that time.  More recently, his peaceful and nonviolent state of mind became more evident, as he decided to begin participating the Rastafarian faith, give himself a new name, and take on a new style of music.

==Plot==
As most of those who follow the Rastafarian faith do, Snoop decided to go through a transformation into his new self; his new, Rastafarian self.  Reincarnated documents Snoop Dogg’s transition into Snoop Lion, which involved him taking a trip to Jamaica to make a reggae album and discover more about the Rastafarian faith. The documentary starts out by exploring Snoop’s past; his time on Death Row Records, the loss of his friends Tupac and Nate Dogg, and the murder accusation.  The focus then shifts to his spiritual journey and new style of music. While in Jamaica, Snoop collaborated with a few famous reggae artists, including Bunny Wailer and Diplo, to create the Reincarnated (album), which focuses on love and nonviolence.  Wailer was Snoop’s guide throughout the trip, leading him through his journey into Rastafarianism. The film includes many intimate moments with Snoop, where he discusses his personal thoughts and reasons for becoming Rasta. The documentary also includes the heavy marijuana smoking in which Snoop participated, which is a common practice among Rastafarians, and a common stereotype of how they practice their faith as well. 

==Production==
The film was directed and produced by Andy Capper of Vice Magazine. Capper followed Snoop to Jamaica to record the transformation, and gathered intimate interview footage with him as well. Roughly 200 hours of footage was shot, which was then edited in a two month span. The documentary was aired at SXSW and The Toronto International Film Festival in 2012.  

==Reception==
In a review from the New York Times, writer Andy Webster states, “enlightenment isn’t as evident here as much as a woozy weariness, perhaps a long-term byproduct of being very, very stoned,” regarding the authenticity of Snoop’s journey throughout the film.  In a review from the LA Times, writer Mikael Wood explains that the documentary does not teach viewers anything new about Snoop, and that it seems like “just another component in the new albums marketing plan.  In two other reviews by Shaka Griffith   and Rich Cline,  there was mention of lack of authenticity as well; neither could say if the transformation should really be taken seriously. After the film was released, Bunny Wailer made a statement where he indicted Snoop of “outright fraudulent use” of the Rastafarian faith. 

==Cast==
* Dr. Dre
* Daz Dillinger
* Angela Hunte
* Ariel Rechtshaid
* Dre Skull
* Jahdan Blakkamoore
* Andrew aka Moon Bain
* Bunny Wailer
* Damian Marley
* Dave Dale and The Blue Mountain Coffee House Boys
* Sister Shirley Chung and Winston Martin
* The Alpha Boys
* Scaby Dread and the Dudus Family
* Cutty Corn
* Louis Farrakhan
* Shante Broadus	
* Cori B.
* Maxine Stowe
* Stewart Copeland
* Diplo
* Snoop Dogg
 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 