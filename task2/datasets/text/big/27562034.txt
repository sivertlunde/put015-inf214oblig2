Szaffi
{{Infobox film
| name           = Szaffi
| image          = 
| image_size     =
| caption        = 
| director       = Attila Dargay
| producer       = 
| writer         = Screenplay: Attila Dargay József Nepp József Romhányi Novel: Mór Jókai
| narrator       =
| starring       = 
| music          = 
| cinematography = Irén Henrik Árpád Lossonczy
| editing        = Magda Hap
| studio         = PannóniaFilm
| distributor    = 
| released       = April 11, 1985
| runtime        = 79 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded_by    =
| followed_by    =
}}
 Hungarian animated film directed by Attila Dargay. It is based on the 1885 book The Gypsy Baron by Mór Jókai, about the romance between a poor Hungarian aristocrat and a mysterious Romani people|Romani-looking Turkish girl in the 18th century. Music from Johann Strauss IIs operetta The Gypsy Baron, based on the same novel, is used for the films soundtrack. 

==Cast==
* András Kern as Jónás Botsinkay (son)
* Judit Pogány as Szaffi
* Hilda Gobbi as Cafrinka (“old witch”)
* György Bárdy as Feuerstein
* Gábor Maros as Puzzola
* Ferenc Zenthe as Gáspár Botsinkay (father)
* József Képessy as Ahmed
* Judit Hernádi as Arzéna
* László Csákányi as Loncsár
* János Gálvölgyi as Menyus, Gazsi bácsi and Strázsa
* Sándor Suka as Savoyai Eugén
* András Márton as Adjutáns
* Gellért Raksányi as Strázsamester
* Zoltán Gera (actor)|Zoltán Gera as Asil
* Judit Czigány as Puzzola anyja

==References==
 

==External links==
*  
*  

 
 
 
 
 


 