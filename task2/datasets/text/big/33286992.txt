Apavadu
{{Infobox film
| name           = Apavadu
| image          = 
| image_size     =
| caption        =
| director       = Gudavalli Ramabrahmam
| producer       = 
| writer         = 
| narrator       =
| starring       = K. S. Prakash Rao Kalyanam Raghuramaiah Lakshmi Rajyam Raavu Balasaraswathi S. Varalakshmi 
| music          = Saluri Rajeswara Rao
| cinematography = 
| editing        = 
| studio         =
| distributor    =
| released       = 1941
| runtime        =
| country        = India Telugu
| budget         =
}}

Apavadu ( ; English translation: Scandal) is a 1941 Telugu Drama film directed by Gudavalli Ramabrahmam.

==The plot==
Prakash is a revenue inspector happily lives with his wife Kamala, sister Kantham and son Kittu. Venkaiah and his wife Anasuya are their neighbours. Prakash has a friend Kamaraju. Anasuya has a crush on Kamaraju, but he refuses her advances. Anasuya plots a revenge with the help of a local rowdy Mangapathi. They spread rumours about the amorous relationship between Kamala and Kamaraju, as a result Kamala attempts suicide. Kamaraju with the help of Rathnam finds out that the handwriting on the love letter supposedly written by him and that of Anasuya are similar. The truth prevails and Prakash and Kamala lives happily ever.

==Cast==
* R. Balasaraswathi - Kantham, sister of Prakash
* K. S. Prakash Rao - Prakash
* Kalyanam Raghuramaiah - Kamaraju, fried of Prakash
* Lakshmi Rajyam - Kamala, wife of Prakash
* Balamani - Rathnam
* Aveti Poornima - Anasuya, wife of Venkaiah
* S. Varalakshmi - Bobjee
* M. C. Raghavan - Venkaiah
* Maddali Krishna Murthy
* Seshagiri - Mangapathi
* Master Prabhakar - Kittu, son of  Prakash

==Soundtrack==
This film has 15 songs written by Basavaraju Apparao, Tapi Dharmarao and Kosaraju Raghavaiah. 
* Deva Devudow Na Nathunaku
* Ee Mavipai Nundi Eevu
* Raavalante Throve Leda

==References==
 

==External links==
*  

 
 
 
 
 


 