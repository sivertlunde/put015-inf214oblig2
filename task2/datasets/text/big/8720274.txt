Zoo (film)
{{Infobox film
| name           = Zoo
| image          = Zoo(2007 film) poster.jpg
| image_size     =
| caption        = Zoo movie poster
| director       = Robinson Devor
| producer       = Peggy Case Alexis Ferris
| writer         = Charles Mudede Robinson Devor
| starring       =Richard Carmen Paul Eenhoorn Russell Hodgkinson John Paulsen
| music          =
| cinematography = Sean Kirby
| editing        = Joe Shapiro
| distributor    = THINKFilm
| released       =   Theatrical:  
| runtime        = 80 min.
| country        = United States
| language       = English
| budget         =
}} Kenneth Pinyan, sex with a horse. The films public debut was at the Sundance Film Festival in January 2007, where it was one of 16 winners out of 856 candidates. Following Sundance, it was selected as one of the top five American films to be presented at the Directors Fortnight sidebar at the 2007 Cannes Film Festival.

==Title==
The movie was originally titled In the Forest There Is Every Kind of Bird,    but is released under the title Zoo, short for zoophile, signifying a person with a sexual interest in animals.

==Crew== The Stranger columnist Charles Mudede and film director Robinson Devor.

==Awards and recognition==
Zoo was one of 16 documentaries selected, out of 856 submitted, for screening at the Sundance Film Festival,    and played at numerous U.S. regional festivals thereafter. 

It was selected as one of the top five American films to be presented at the Directors Fortnight sidebar at the 2007 Cannes Film Festival.      

==Reception==
{{Quote box
 | quote  =They called us and were excited about the imagery, the poetry, the experimentation with the documentary form   then, strangely, suddenly, in 2005, it becomes the talk of society.   How do we go from something being utterly hidden from view, and then suddenly were consumed with it and so upset by it we need to pass a law? 
 | source =Charles Mudede
 | width  = 200px
 | align  = right
}}

Sundance judges called it a "humanizing look at the life and bizarre death of a seemingly normal Seattle family man who met his untimely end after an unusual encounter with a horse". 

  ("remarkably, an elegant, eerily lyrical film has resulted")    and the Toronto Star, "gorgeously artful ... one of the most beautifully restrained, formally distinctive and mysterious films of the entire festival". 

Other reviewers criticized the film for breaching "the last taboo", or for sinking to new depths: "More compelling than the depths of mans degeneracy is our cultural rationalization of art, whereby pushing the envelope is confused with genius and scuttling the last taboo is seen as an expression of sophistication." 

==References==
 

==External links==
*  
*  
*   News24. May 22, 2007
*Lim, Dennis. " ." The New York Times. April 1, 2007.

 
 
 
 
 
 
 
 