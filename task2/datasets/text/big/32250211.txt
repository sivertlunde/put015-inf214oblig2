Bhadrakali (film)
{{Infobox film
| name           = Bhadrakali
| image          = Bathrakali.jpg
| image_size     =
| caption        =
| director       = A. C. Tirulokchandar
| producer       = Cine Bharath
| writer         = Harur Dass
| narrator       =
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Viswanath Rai
| editing        =
| distributor    = 
| released       = 10 December 1976
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} 1976 Tamil film starring Sivakumar and Rani Chandra in the lead roles. Produced and directed by A. C. Tirulokchandar, it is an adaptation of a Tamil novel written by Maharishi. The soundtrack and film score were composed by Ilaiyaraaja.

==Cast==
* Sivakumar
* Rani Chandra
* Major Sundarrajan
* S. R. Sivakami 
* Thengai Srinivasan
* Sukumari
* Pushpa Manorama

==Production== Brahmin household. Tirulokchandar produced the film under his home production "Cine Bharath", and Harur Dass was recruited as the dialog writer.  The director chose Sivakumar as the male lead and while looking for a "new face" to cast as the female lead, they chose Rani Chandra, a Malayali actress, and planned to introduce her to the Tamil audience under the name "Gayathri".  Prior to acting in this film, Rani Chandra had acted in about 60 films in Malayalam apart from Porchilai (1969) and Then Sindhudhe Vaanam (1975) in Tamil. Bhadrakali was considered her first major role in Tamil as she played minor roles in her earlier films in the language. 

===Death of the female lead and her replacement===
During the final stages of filming, Rani Chandra along with her mother and sisters went to Dubai to take part in a cultural programme.  When they were on a return from Bombay to Madras on 11 October 1976, their plane caught fire and crashed near the airport, killing all the passengers. As a result of her death, a few unfinished portions of her were shot using a look-alike,    named Pushpa, a group dancer. Apart from a few scenes involving the female lead, the climax portions were fully shot using Pushpa.     Though Tirulokchandar was not fully convinced with the idea to shoot with a look-alike as Pushpa slightly resembled Rani. However, Viswanath Rai, the cinematographer encouraged him to go with the idea of look-alike and used different camera angles and used long shots for scenes involving Pushpa, Ranis duplicate.   

==Release==
The film released on 10 December 1976 receiving unanimous acclaim and turned out to be a box-office success.  Rani Chandras portrayal as a Brahmin housewife was well received.  Ananda Vikatan said, "It is a daring effort to bring out the script of a Tamil writer in a high quality film format. Dialogues by Aroor Das have depth and the film deserves our appreciation!" 

==References==

===Footnotes===
 

===Bibliography===
 
*  
 

==External links==
*  

 

 
 
 
 
 
 
 
 