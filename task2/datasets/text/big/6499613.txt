Bring Me the Head of Mavis Davis
 
 
{{Infobox film
| name           = Bring Me the Head of Mavis Davis
| image	=	Bring Me the Head of Mavis Davis FilmPoster.jpeg
| alt            = 
| caption        = UK DVD cover John Henderson John Quested   Joanne Reay   David M. Thompson
| writer         = Joanne Reay (story), Craig Strachan (screenplay)
| starring       = Rik Mayall   Jane Horrocks   Danny Aiello
| music          = Christopher Tyng
| cinematography = Clive Tickner
| editing        = Paul Endacott
| studio         = BBC Films
| distributor    = 
| released       =   
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 John Henderson, originally released in 1997. The film stars Rik Mayall, Jane Horrocks, Danny Aiello and Ross Boatman. The title and plot reference Peckinpahs Bring Me the Head of Alfredo Garcia. It was entered into the 20th Moscow International Film Festival.   

==Plot==
Record-company owner Marty Starr (Rik Mayall) concludes that Marla Dorland, aka Mavis Davis (Jane Horrocks) is a fading star. Meanwhile he has to meet alimony payments to his ex (Jaclyn Mendoza), while hes forced to promote the untalented son of a mobster, Rathbone (Danny Aiello). To get out from under, Marty decides that the death of Marla/Mavis could jolt record sales by turning her into a legend. He hires hitman Clint (Marc Warren), but eliminating Mavis turns out to be more difficult than they thought.

==Cast==
* Rik Mayall as Marty Starr
* Jane Horrocks as Mavis Davis
* Danny Aiello as Mr. Rathbone
* Ronald Pickup as Percy Stone
* Philip Martin Brown as Inspector Furse
* Heathcote Williams as Jeff
* Marc Warren as Clint
* Mem Ferda as S.Bon Paul Keating as Paul, Rathbones Son
* Ross Boatman as Rock Star

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 
 