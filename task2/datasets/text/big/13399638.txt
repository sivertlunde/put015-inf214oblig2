The Edge of Heaven (film)
{{Infobox film
| name           = On The Other Hand/Side (if "The Edge of Heaven" is the title, German is "der Rand des Himmels")
| image          = Auf der anderen Seite.jpg
| image_size     = 215px
| alt            = 
| caption        = German promotional poster
| director       = Fatih Akın
| producer       = Fatih Akin Klaus Maeck Andreas Thiel Jeanette Würl
| writer         = Fatih Akın
| starring       = Nurgül Yeşilçay Baki Davrak Tuncel Kurtiz Hanna Schygulla Patrycia Ziolkowska Nursel Köse
| music          = Shantel
| cinematography = Rainer Klausmann Andrew Bird
| studio         = Anka Film Corazón International Dorje Film Norddeutscher Rundfunk
| distributor    = The Match Factory   Strand Releasing  
| released       =  
| runtime        = 122 minutes
| country        = Germany Turkey
| language       = German Turkish English
| budget         = 
| gross          = $17,804,565 
}} drama written Prix du 2007 Oscars,  but was not nominated.
 Cannes Film Festival in France, the film was shown at several international film festivals. It was released in Germany on 27 September 2007.

==Plot==
;Yeters Death
Retired widower Ali Aksu (Tuncel Kurtiz), a Turkish immigrant living in the German city of Bremen, believes he has found a solution to his loneliness when he meets Yeter Öztürk (Nursen Köse). He offers her a monthly payment to stop working as a prostitute and move in with him. After receiving threats from two Turkish Muslims, she decides to accept his offer. Alis son, Alisan Nejat (Baki Davrak), a professor of German literature, does not have time to respond to the prospect of living with a woman of "easy virtue" before Ali is stricken with a heart attack. He softens to her: he learns that she sends shoes to Turkey for her 27-year-old daughter and wishes that her daughter could receive an education like his. 

Back home from the hospital, Ali suspects that the other two may have become lovers. When his drunken demands of Yeter make her threaten to leave, he strikes her and she dies from the blow. Ali is sent to prison.

Nejat travels to Istanbul to search for Yeters daughter, Ayten (Nurgül Yeşilçay), and assume responsibility for her education. Unable to locate her through her family, he posts flyers of Yeter throughout the area in the hopes that it will lead to the daughter. When he posts a flyer in a small German language bookstore that happens to be for sale, he finds himself charmed into buying it.

;Lottes Death
A plainclothes officer loses his gun on the street during a riot. A hooded figure scoops it up and is pursued on foot by a battalion of uniformed officers, barely managing to hide the contraband on a random rooftop. This is Ayten, a member of a Turkish Communist resistance group. 

When her cell is raided, she flees Turkey and takes up a new identity with political allies in Bremen, Germany. However, even there, she has a falling out when she is unable to pay her debts, and thus finds herself on the street with barely a euro to her name. Her mothers number is lost, so she lives illegally and searches for her in local shoe shops.
 political asylum. Despite Susannes financial support, Germany rules that Ayten has no legitimate fear of political persecution. Shes deported and immediately imprisoned.

Lotte is devastated. She travels to Turkey to try to free Ayten, but quickly realizes how little hope there is, as she is facing 15 to 20 years in jail. Susanne pleads with her to think of her future and return home. When Lotte refuses, her mother refuses to assist her further. Lotte gravitates to Nejats bookstore and ends up renting a spare room from him.

Finally granted a prison visit with Ayten, Lotte follows her imprisoned lovers request and retrieves the handgun Ayten grabbed in the riot. But Lottes bag, with the gun inside, is snatched by a crew of boys that she then chases through their neighborhood. When finally she finds them in a vacant lot, one of them is inspecting the gun. She demands he return it, but he points it at her and fires, killing her instantly.

;The Edge of Heaven (literally, On the Other Side)
Upon his release, Ali is deported to Turkey, returning to his property in Trabzon on the Black Sea coast.

After her daughters death, Susanne goes to Istanbul to see where her daughter had been living the past few months. She meets Nejat and reads her daughters diary; she decides to take on her daughters mission of freeing Ayten from prison. Susannes visit to Ayten—an offer of forgiveness and support—leads the younger woman to exercise her right of repentance. As a result, she wins her freedom. 
 Bayram they the Bible, where Abraham is asked to sacrifice his son Isaac. Nejat reminisces about being scared by the story as a child and asking his father if he would sacrifice him if God told him to. When asked by Susanne what his fathers answer was, Nejat tells her that his father said "He would make God his enemy in order to protect me". 

Nejat removes the poster of Yeter from the shops noticeboard. He asks Susanne to look after his shop while he is gone, and drives to Trabzon where his father is living. 

Susanne offers Ayten a place to stay with her at Nejats house. When Nejat arrives in Trabzon, his father is out fishing, so he waits for him on the beach.

==Cast==
* Tuncel Kurtiz as Ali Aksu, a Turkish immigrant to Germany
* Baki Davrak as Nejat Aksu, son of Ali, a Turkish German language teacher
* Nursel Köse as Yeter Öztürk, a Turkish immigrant to Germany
* Nurgül Yeşilçay as Ayten Öztürk, daughter of Yeter, a Turkish student and rebel
* Patrycia Ziolkovska as Charlotte "Lotte" Staub, a German student
* Hanna Schygulla as Susanne Staub, mother of Lotte

==Production== Taksim and Kadıköy in Istanbul, at the Black Sea coast in Trabzon in Turkey.

==Critical reception==
The film received generally positive reviews from Western critics. The review aggregator Rotten Tomatoes reported that 89% of critics gave the movie positive reviews, based on 61 reviews.  Metacritic reported the film had an average score of 86 out of 100, based on 24 reviews. 

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2008.     

 
 
*2nd - Sean Axmaker, Seattle Post-Intelligencer 
*2nd - Stephen Holden, The New York Times 
*2nd - Steve Rea, The Philadelphia Inquirer 
*2nd - Wesley Morris, The Boston Globe 
*3rd - Dana Stevens, Slate (magazine)|Slate 
*4th - Ann Hornaday, The Washington Post 
*5th - A. O. Scott, The New York Times 
*5th - Michael Sragow, The Baltimore Sun 
 
*6th - J.R. Jones, Chicago Reader 
*6th - Owen Gleiberman, Entertainment Weekly 
*6th - Rick Groen, The Globe and Mail 
*7th - Ann Hornaday,  The Washington Post 
*9th - Anthony Lane, The New Yorker 
*9th - Stephen Farber, The Hollywood Reporter 
*9th - Tasha Robinson, The A.V. Club 
*10th - Kirk Honeycutt, The Hollywood Reporter 
 

==Awards and nominations== Best Screenplay Lino Brocka Award in the International Cinema category at the 2007 Cinemanila International Film Festival in the Philippines. Five awards followed at Antalya Golden Orange Film Festival (best director, editing, supporting actor, supporting actress and special jury award), and then it won:
* On 24 October 2007, the European Parliaments newly established LUX prize for European cinema;
* On 10 November 2007, the Critics Award at the European Cinema Festival, in Seville;
* On 1 December 2007, the best screenplay award at European Film Awards, with nominations for best director and best film; Grand Prix of the Belgian Syndicate of Cinema Critics; Best Supporting Actress award to Hanna Schygulla from the National Society of Film Critics.

==See also==
 
* Cinema of Germany
* Cinema of Turkey

==References==
 

==External links==
*    
*  
*  
*  
*  
*  

 
 
{{Succession box
| before = Volver
| after = Le silence de Lorna Cannes Film Festival Prix du scénario
| years = 2007}}
{{Succession box
| before = new creation
| after = Le silence de Lorna
| title = Lux Prize for European Cinema
| years = 2007}}
{{Succession box
  | before = Takva
  | after = incumbent Golden Orange Dr. Avni Tolunay Jury Special Award for Best Picture
  | years = 2007}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 