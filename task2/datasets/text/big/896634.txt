It! The Terror from Beyond Space
{{Infobox film
  | name           = It! The Terror from Beyond Space
  | image          = It the terror from beyond space.jpg Theatrical release poster
  | director       = Edward L. Cahn Robert Kent Edward Small (uncredited)
  | writer         = Jerome Bixby
  | starring       = Marshall Thompson Shawn Smith(Shirley Patterson) Kim Spalding 
  | music          = Paul Sawtell Bert Shefter
  | cinematography = Kenneth Peach
  | studio         = Vogue Pictures, Inc.
  | editing        = Grant Whytock
  | distributor    = United Artists
  | released       =  
  | runtime        = 68 min
  | language       = English
  | budget         = 
}}

It! The Terror from Beyond Space is an independently made 1958  , November 21, 2012. 

The plot involves a rescue mission to Mars that finds the sole survivor of a previous expedition from Earth. The survivor, the expeditions former commander, claims that his crew were killed by a hostile Martian life form. The rescue ships captain does not believe him, postulating that they were killed for their provisions so the commander could survive. He confines the commander to quarters after the rescue ship launches for Earth. The same hostile Martian creature, now a stowaway and immune to the crews few weapons, begins another human hunting spree. 

The premise of a hostile alien creature hunting a spaceships crew as it returns to Earth was the inspiration for screenwriter Dan OBannons screenplay for Ridley Scotts 1979 film Alien_(film)|Alien. 

==Plot==
In 1973, a nuclear power|nuclear-powered spaceship is perched on the cratered surface of Mars, sent to rescue the crew of a previous, ill-fated mission. The sole survivor of that crashed ship, Col. Edward Carruthers (Marshall Thompson), is suspected of having murdered the other nine members of his crew for their food and water rations, on the premise that he had no way of knowing if or when they would be rescued. Carruthers denies the allegation, attributing the deaths to an alien, hostile life form encountered on Mars. 

The rescue ships commander is unconvinced, confining Carruthers to quarters and ordering an immediate return to Earth. Unknown to the rest of the crew, before blast-off, one crew member left a large external exhaust vent open for a considerable time. With Mars behind them, the crew settle into shipboard routine for the six-month journey home. Soon, isolated crew members are attacked by a shadowy presence and taken into the ships ventilation ducts.

The crew are at first skeptical that "something" has crawled aboard while they were on Mars. The body count begins to rise with the discovery of the shriveled corpse of a colleague, then another near death. Both bodies have been sucked dry of moisture. Reasoning that Mars is a world with no liquid water, the crew believe that their stowaway must be the same creature that killed Carruthers shipmates. 
 gas grenades, but the humanoid alien proves largely immune to the weaponry. They try to electrocute the creature with no effect, but are able to lure It into the spaceships nuclear reactor room, shutting and locking the heavily shielded door behind the creature, which is then exposed directly to the ships nuclear pile. It crashes through the door and escapes, the radiation seemingly having no more effect than an old-fashioned hot foot. 

As the crew numbers dwindle, the survivors retreat upward deck-by-deck, pursued by the creature. The alien is strong enough to break through the central pressure hatch on each deck, trapping them in the control room on the topmost deck. In the final standoff, their weapons are once again unleashed but It is unstoppable and begins breaking through the last hatch. Reasoning that the ships higher-than-normal oxygen consumption rate is likely due to the creatures larger lung capacity, Carruthers decides that opening the command decks hull airlock to the vacuum of space should suffocate the creature while the survivors are safe in their spacesuits. After an explosive decompression, the plan works: It suffocates and finally expires.

A press conference is held at the crews base on Earth, revealing the details of what happened aboard the rescue ship. The project director emphasizes that Earth may now be forced to leave Mars out of all future manned planetary space exploration, "because another word for Mars is death."

==Cast==
 
* Marshall Thompson as Col. Edward Carruthers
* Shirley Patterson as Ann Anderson (as Shawn Smith)
* Kim Spalding as Col. Van Heusen
* Ann Doran as Mary Royce
* Dabbs Greer as Eric Royce
* Paul Langton as Lt. James Calder
* Robert Bice as Maj. John Purdue
* Richard Benedict as Bob Finelli
* Richard Hervey as Gino Finelli
* Thom Carney as Joe Kienholz
* Ray Corrigan as It
 

==Production==
It! The Terror from Beyond Space was financed by Edward Small and was originally known as It! The Vampire from Beyond Space.  Principal photography took place over a two-week period during mid-January 1958. 
 Ray "Crash" Corrigan. Corrigan was set to play the role of the creature, but during pre-production he did not want to travel all the way to Topanga in western Los Angeles County where Paul Blaisdell, the films sculptor and makeup artist, lived and operated his studio. Therefore Blaisdell couldnt take exact measurements of Corrigans head. Consequently there were final fit problems with the creatures head prop: " ... bulbous chin stuck out through the monsters mouth, so the make-up man painted his chin to look like a tongue." Stafford, Jeff.   Turner Classic Movies. Retrieved: January 6, 2015. 

==Reception==
It! The Terror from Beyond Space  was a programmer but despite its  , a later film that borrowed liberally from its earlier counterpart.  

==Adaptations== Mark Ellis Midnite Movies (IDW Publishing) in 2010, for a three-issue run. 

==References==
Notes
 

Bibliography
 
* Strick, Philip. Science Fiction Movies. London: Octopus Books Limited. 1976. ISBN 0-7064-0470-X.
* Palmer, Randy. Paul Blaisdell, Monster Maker: A Biography of the B Movie Makeup and Special Effects Artist. Jefferson, North Carolina: McFarland & Company, 1997. ISBN 978-0-78644-099-3.
* Warren, Bill. Keep Watching the Skies: American Science Fiction Films of the Fifties, 21st Century Edition. 2009. McFarland & Company. ISBN 0-89950-032-3.
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 