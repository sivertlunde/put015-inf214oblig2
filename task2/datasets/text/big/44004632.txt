Karutha Pournami
{{Infobox film 
| name           = Karutha Pournami
| image          =
| caption        =
| director       = Narayanankutty Vallath
| producer       = Narayanankutty Vallath
| writer         = CP Antony
| screenplay     = CP Antony Madhu Sharada Sharada P. J. Antony Bahadoor
| music          = M. K. Arjunan
| cinematography = TN Krishnankutty Nair
| editing        = TR Sreenivasalu
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, directed by Narayanankutty Vallath and produced by Narayanankutty Vallath. The film stars Madhu (actor)|Madhu, Sharada (actress)|Sharada, P. J. Antony and Bahadoor in lead roles. The film had musical score by M. K. Arjunan.   

==Cast== Madhu
*Sharada Sharada
*P. J. Antony
*Bahadoor
*Jayanthi
*S. P. Pillai
*Vijayanirmala
*Xavier
*Baby Gracy
*Johnson

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Hridayamuruki Nee || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Kavithayil Mungi || S Janaki || P. Bhaskaran || 
|-
| 3 || Maanathin Muttathu || K. J. Yesudas || P. Bhaskaran || 
|-
| 4 || Maanathin Muttathu(F) || S Janaki || P. Bhaskaran || 
|-
| 5 || Ponkinaavin || K. J. Yesudas || P. Bhaskaran || 
|-
| 6 || Ponnilanji || B Vasantha || P. Bhaskaran || 
|-
| 7 || Shishuvineppol Punchirithooki || K. J. Yesudas, S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 