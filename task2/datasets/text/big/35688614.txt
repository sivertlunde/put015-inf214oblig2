A Bahraini Tale
{{Infobox film
| name           = A Bahraini Tale
| image          = A Bahraini Tale theatrical poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Bassam Al-Thawadi
| producer       = {{Plainlist |
* Mohd Abdulkhaliq
* Bassam Al-Thawadi }}
| writer         = 
| screenplay     = Fareed Ramadan
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist |
* Saad Abdulla
* Fatima Abdulrahim
* Abdulla Al Saadawi
* Ebrahim Al-Ghanim
* Hassan Al-Majed
* Mahmood Al-Mulla
* Jumaan Al-Rowayai
* Ahmed Aqlan }}
| music          = Mohammed Haddad
| cinematography = Shamdat Sainudeen
| editing        = Osama Al-Saif
| studio         = Bahrain Film Production
| distributor    = BFP
| released       =  
| runtime        = 96 minutes Bahrain
| language       = Arabic
| budget         = $1,000,000 (estimated)
| gross          = 
}}

A Bahraini Tale is a 2006 Bahraini Arabic-language drama film directed by Bassam Al-Thawadi, screenplay by Fareed Ramadan and starring Saad Abdulla, Fatima Abdulrahim and Abdulla Al Saadawi.  This is the third feature film to be directed by Bassam al-Thawadi and one of the only three films ever made in Bahrain.    

==Synopsis==
Set during the Six Day War of 1967, the film revolves around the personal story of a middle-class Bahraini family and an account of the hopes and faith the Arab world had in Gamal Abdul Nasser as its leader.

==Cast==
* Saad Abdulla as Salim 
* Fatima Abdulrahim as Fatima 
* Abdulla Al Saadawi as Juma Khamis 
* Ebrahim Al-Ghanim as Tickets Man 
* Hassan Al-Majed as Mahmood 
* Mahmood Al-Mulla as Bu Jassim 
* Jumaan Al-Rowayai as Hamad 
* Ahmed Aqlan as Sultan 
* Abdulla Bahar as Mad Man 
* Yousif Bu Hallol as Yaqoob 
* Ahmed Fardan as Salman 
* Shayma Janahi as Munira 
* Mubarak Khamis as Abdulla Khamis 
* Abdulrahman Mahmood as Man 
* Wafa Maki as Mahmood Sister 
* Abdulla Malik as Ali 
* Fahad Mandi as Yahya 
* Latifa Mujren as Hamads Mother 
* Shatha Sabt as Nayla 
* Majeda Sultan as Sharoof Al-Zarqa 
* Abdulla Wlaad as Rashed 
* Mariam Ziman as Latifa 
* Nadeem Ziman as Khalifa 

== Reception ==
The film was screened nationwide across Bahrain and across the Arab world. The film was critically acclaimed by critics locally and internationally. It was given a 90% rating by Rotten Tomatoes 

== References ==
 

== External links ==
*   at the Internet Movie Database
*   at Rotten Tomatoes
*  

 
 
 
 
 
 
 
 