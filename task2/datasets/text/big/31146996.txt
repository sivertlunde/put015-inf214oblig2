Food Matters
 
 

 
{{Infobox film
| name           = Food Matters
| image          = Food Matters.jpg
| alt            = 
| director       = James Colquhoun Carlo Ledesma
| producer       = James Colquhoun Laurentine Ten Bosch Enzo Tedeschi
| writer         = James Colquhoun Laurentine Ten Bosch
| narrator       = 
| music          = 
| cinematography = 
| editing        = Enzo Tedeschi
| studio         = Permacology Productions
| distributor    = Aspect Film
| released       =  
| runtime        = 80 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}} general conspiracy to perpetuate poor health in order to maximize profits.   

==Content==
 
Food Matters explores the world of Orthomolecular Medicine and how it compares to traditional Modern Medicine. The documentary begins with talking about how our food travels many miles before getting to its final destination and by the time we buy it, it is at least a week old. The freshness of our foods have drastically changed throughout time. The health experts start with the issue of the soil crisis and how it lacks the nutrients for food to grow in a healthy, natural way. Modern day chemists have found ways to optimize our crops for faster, more efficient growth. Andrew Saul, a therapuetic nutrition specialist and author of a famous Orthomolecular based text, reveals to us many paradox ideas about raw vs. cooked foods. Cooked has many effects on the body. Later on, David Wolfe, a raw food nutrition expert, introduces us to many of the world’s unrecognized superfoods and how they positively contribute to our health. He suggests many alternatives to our daily diets. It was also mentioned that the medical field has a certain stigma about the use of vitamin supplements within their practice. The use of vitamins, they said, could reshape the economy of the medical field. They linked many immune disorders to the everyday, unhealthy diets of our society. Many of the health experts, such as Ian Brighthope, talked about how immune disorders have developed into a cultural anomaly that differs throughout the world. They mentioned the medical methods used today and how it presumably could cause unexpected health effects. Charlotte Gerson, founder of the Gerson Institute, then comes in to speak about cancer treatment and research and how it plays a role in the health of our society. Further education was stressed to find a solution to this worldwide epidemic.
  

==Production==
Food Matters is a documentary that was produced in 2007. The documentary was released on May 30, 2008. The film was directed by James Colquhoun and Carlo Ledesma. The producers included James Colquhon, Laurentine Ten Bosch and Enzo Tedeschi. The production company is Permacology Productions in Australia and was distributed by Aspect Films. The movie is based out of Australia and is written within the English language and lasts for 78 minutes. The film has also come out in different languages including: Korean, French, German, Japanese and Spanish. The budget for this documentary is unfound at this time. 

== References ==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 