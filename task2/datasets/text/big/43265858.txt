Harvest (1937 film)
{{Infobox film
| name           = Harvest
| image          = Regain poster.jpg
| caption        = Theatrical release poster
| director       = Marcel Pagnol
| producer       = Marcel Pagnol
| screenplay     = Marcel Pagnol Second Harvest by Jean Giono
| starring       = Fernandel Orane Demazis Marguerite Moreno Gabriel Gabrio
| cinematography = Willy Faktorovitch Roger Ledru Pierre Arnaudy Henri Darriès
| music          = Arthur Honegger
| editing        = Suzanne de Troye
| studio         = Les Films Marcel Pagnol
| distributor    = Compagnie méditerranéenne de films
| released       =  
| runtime        = 150 minutes
| country        = France
| language       = French
| budget         = 
}} Second Harvest by Jean Giono. It was released in France on 28 October 1937 and in the United States on 2 October 1939.

==Cast==
* Fernandel as Urbain Gédémus
* Gabriel Gabrio as Panturle
* Orane Demazis as Irène Charles, "Arsule"
* Marguerite Moreno as Zia Mamèche, "la Mamèche"
* Robert Le Vigan as the brigadier
* Henri Poupon as Panturles farmer friend
* Odette Roger as Alphonsine
* Milly Mathis as Belline

==Reception==
Frank Nugent of The New York Times described Harvest as "a film of utter serenity and great goodness, so reverently played and so compassionately directed that it is far less an entertainment work than it is a testament to the dignity of man and to his consonance with the spinning of the spheres. Such faults as it possesses are mechanical; uneven editing, particularly in the early scenes; skimping of a few sequences that might have received more attention; attenuation of others that could have been cut. The flaws are obvious enough, yet they should not count too seriously aaginst the work as a whole and still less seriously when one appreciates that the editing was on this side of the water (for space and time requirements) and cannot properly be held against Marcel Pagnol, its director, producer and adapter." 

==See also==
* 1939 in film
* Cinema of France

==References==
 

 

 
 
 
 
 
 
 
 