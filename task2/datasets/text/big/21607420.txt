Street Racer (film)
{{Infobox film
| name           = Street Racer
| image          =Poster of the movie Street Racer.jpg
| caption        =
| director       = Teo Konuralp
| producer       = David Michael Latt David Rimawi Paul Bales
| writer         = Carey Van Dyke Shane Van Dyke
| starring       = Clint Browning Dorothy Drury Robert Pike Daniel Jason Ellefson Dustin Fitzsimons Michael Crider Jennifer Dorogi Jack Goldenberg
| music          =
| cinematography = Alexander Yellen
| editing        =
| distributor    = The Asylum
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Street Racer is a 2008 action film by  , which had been released in 2006. The film is advertised by The Asylum as being based on true events. 

== Plot == street race in Los Angeles. Wayne, having been traumatised by the events, vows to never race again, and is soon released as a reformed citizen.

Whilst Wayne attempts to restore his life and become an honest, hardworking member of society, he slowly finds himself being lured back into street racing by his former associates, for the chance to keep his title as the ultimate street racer of Los Angeles.

== Cast ==
* Clint Browning as Johnny Wayne
* Dorothy Drury as Kelly
* Robert Pike Daniel as Red
* Jason Ellefson as Mickey Styles
* Dustin Fitzsimons as Steve
* Michael Crider as Briggs
* Connor Herlong as Daniel
* T.J. Zale as Robert
* Reggie Jernigan as Derek
* Kelli Dawn Hancock as Armenia
* Sinead McCafferty as Sheila
* Jennifer Dorogi as Teddy
* Jack Goldenberg as Travis

== See also == 2006
*Death Racers - Another racing film by The Asylum released in the same year Speed Demon - Another film by The Asylum released in 2003 in film|2003, which bears some similarity to Street Racer
*Speed Racer (film)

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 