Coins in the Fountain
{{Infobox television film
| name =Coins in the Fountain
| image = 
| image_size = 
| caption =  
| director = Tony Wharmby
| producer = Joseph B. Wallenstein
| writer = Lindsay Harrison Stuart Wilson Anthony Newley
| music = Nan Schwartz
| cinematography = 
| editing = J. Benjamin Chulay
| released =  
| runtime = 97 minutes
}} Coins in Three Coins in the Fountain. It was directed by Tony Wharmby and written by Lindsay Harrison. Filming took place in Rome and other parts of Italy    during the summer of 1990.  

==Summary== Italian artist Stuart Wilson). After learning of the trip, Mac, hoping for a reconciliation with Leah, encourages her to go see Marcello but wants time with her when she returns.
 the 1954 film). The ladies agree to throw their coins and make their wishes at this fountain since they do not want to walk all the way to the Trevi Fountain, which was used in the previous film.

Leah locates Marcello, now a curator at an art museum and finds the courage to meet him. Marcello is excited to again see Leah, and the two spend the afternoon together strolling around the city. Meanwhile, Nikki and Bonnie lose each other in the crowds of tourists. Nikki makes it back to the hotel, and she and Joe share their stories and eventually decide to go to dinner together. Bonnie ends up with a tour group and meets Alfred (Anthony Newley), a widower who becomes interested in her. They also go to dinner that night.

As their two-week vacation is coming to an end, the women try to sort out their feelings. Leah is convinced that Marcello is going to ask her to stay, as he did in 1970, and this time she decides she will say yes. Nikki decides to break it off with Joe, as she does not ever want to be in a position of being hurt again, and Bonnie truly does not know what she wants. Alfred, who has become a good friend, has offered her to come with him to Switzerland for another two weeks. She feels that if she does not go, she may never have another chance, and she is not anxious to return to her mundane life.

As Nikki gets to Joes hotel room, she finds that he has already left, and she begins to cry. Meanwhile, Leah tells Marcello she will stay and is then surprised to find out that he had not even thought about asking her to stay. Marcello tells her that he thought she was living her life exactly how she wanted it, based upon her statements from 1970. He tells her that her career focus is what influenced him to give up being an artist and move into his career.
 David Wilson) has flown in as well. Joe tells Nikki that he does not want to lose her. Phil tells Bonnie that he missed her too much and that he needs her.

Marcello appears and tells Leah that he would like her to stay if she wants to, but Leah responds that she realized that it cant be 1970 again and that she has always had a fantasy that it could be. She ends by saying that she needed to get back to Los Angeles, her dream job and to Mac, who she had not been fair to. She has always held it against him that he was not a young, Italian artist.

The film ends with Leah, Phil, Bonnie, Joe and Nikki on an airplane flying home, with Leah asking, "So how does Paris in April sound?"

==Theme song== Three Coins Jack Jones (the singer of The Love Boat theme) and used in this film. 

==Opening scenes==
During the opening credits, the scene is a recreation of the opening of the 1954 film. It is a montage of the Trevi Fountain and other fountains of Rome, as well as many shots of Villa dEste in Tivoli, Italy.

==Cast==
* Loni Anderson ...  Leah 
* Stepfanie Kramer ...  Nikki Taylor 
* Shanna Reed ...  Bonnie 
* Carl Weintraub ...  Joe Marino Stuart Wilson ...  Marcello  David Wilson ...  Phil 
* Anthony Newley ...  Alfred 
* John Sanderford ...  Mac Chambers 
* Tim Flavin ...  Steve 
* Peter Kelly ...  Carlo 
* Liz Daniels ...  Flower Lady 
* Francesca DeRose ...  Tour Guide #1 
* Francesco Tola ...  Tour Guide #2 
* Roberto Minozzi ...  Tour Guide #3 
* Livio Galass ...  Desk Clerk

==Reviews and reception==
The film was given 2 and a half stars by Hal Ericson of Allmovie.

==References==
 

==External links==
*  .

 
 
 
 
 
 