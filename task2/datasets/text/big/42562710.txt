People Who Travel (1938 German-language film)
{{Infobox film
| name =  Travelling People 
| image =
| image_size =
| caption =
| director = Jacques Feyder Helmut Schreiber
| writer =  Jacques Feyder    Jacques Viot   Bernard Zimmer 
| narrator =
| starring = Hans Albers   Françoise Rosay   Camilla Horn   Hannes Stelzer
| music = Wolfgang Zeller    Franz Koch  
| editing =  Wolfgang Wehrum      
| studio = Tobis Film
| distributor = Tobis Film
| released = 1 July 1938  
| runtime = 106 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Travelling People (German:Fahrendes Volk) is a 1938 German drama film directed by Jacques Feyder and starring Hans Albers, Françoise Rosay and Camilla Horn. It is a circus film.  It premiered in Hamburg on 1 July 1938. A Separate French-language version People Who Travel (Les gens du voyage) was also released. While it was also directed by Feyder and starred Rosay, the rest of the cast were different.

==Cast==
* Hans Albers as Fernand 
* Françoise Rosay as Madame Flora 
* Camilla Horn as Pepita  
* Hannes Stelzer as Marcel  
* Irene von Meyendorff as Yvonne Barlay  
* Ulla Gauglitz as Suzanne  
* Herbert Hübner as Zirkusdirektor Edward Barlay  
* Alexander Golling as Ganove Tino 
* Otto Stoeckel as Charlot 
* Aribert Mog as Gendarmerieleutnant  
* Hedwig Wangel as Wirtschafterin Yvonnes im Zirkus  
* Willem Holsboer as Verehrer Pepitas  
* Franz Arzdorf as Präfekt Bob Bauer as Raubtierverkäufer  
* Lilo Bergen as Autogrammjägerinnen  
* Willy Cronauer as Inspizient im Zirkus in Paris  
* Toni Forster-Larrinaga as Ledhrerin  
* Friedrich Gnaß as Bosko  
* Walter Holten as Joe  
* Magda Lena as Yvonnes Tante  
* Karl Platen as Registrarbeamter  
* Erwin van Roy as Mann, der den Zirkus ankündigt
* Tilly Wedekind as Reiche Amerikanin  
* Herbert Weissbach as Pepitas Verehrer  Philipp Veit as Älterer Verehrer Pepitas  
* Franz Schönemann as Gendarm 
* Elsa Andrä Beyer as Autogramjägerinen
* Hans Alpassy as Raubtierwärter 
* Elise Aulinger as Löwenfellgerberin  
* Hans Hanauer as Alte Arzt  
* Hermann Kellein as Gigolo  
* Richard Korn as Schneidermeister bei der Zirkusparade  
* Reinhold Lütjohann as Fleischlieferant für die Tiere im Zirkus  
* Luise Morland as Mädchen bei Hotel du Nord  
* Theodolinde Müller as Tanzschülerin 
* Rudolf Raab as Personalchef im Kaufhaus 
* Eugen Schöndorfer as Kassierer bei Barlay  
* Edith von Wilpert as Tanzschullehrerin

== References ==
 

== Bibliography ==
* Schoeps, Karl-Heinz. Literature and Film in the Third Reich. Camden House, 2004. 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 