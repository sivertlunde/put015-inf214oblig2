Eva (1962 film)
{{Infobox film
| name           = Eva
| image          = Eva 1962 film.jpg
| caption        = Original film poster Raymond Hakim Robert Hakim
| director       = Joseph Losey Evan Jones
| based on       =  
| starring       = Jeanne Moreau Stanley Baker Virna Lisi
| music          = Michel Legrand
| cinematography = Gianni Di Venanzo
| editing        = Reginald Beck Franca Silvi
| distributor    = Times Film Corporation
| released       =  
| runtime        = 116 minutes
| language       = English
}}
Eva, released in the United Kingdom as Eve, is a 1962 Italian-French drama film directed by Joseph Losey and starring Jeanne Moreau, Stanley Baker and Virna Lisi. Its screenplay is adapted from James Hadley Chases 1945 best-selling novel Eve (novel)|Eve. 

==Plot==
A raw Welsh novelist in Venice is humiliated by a money-loving Frenchwoman who erotically ensnares him.

==Cast==

* Jeanne Moreau: Eva Olivier
* Virna Lisi: Francesca Ferrari
* Stanley Baker: Tyvian Jones
* James Villiers: Alan McCormick
* Lisa Gastoni: The red-headed Russian Riccardo Garrone: Michele
* Checco Rissone: Pieri
* Enzo Fiermonte: Enzo
* Giorgio Albertazzi: Branco Malloni

== References ==
  

== External links ==
*  

 

 
 
 
 
 

 