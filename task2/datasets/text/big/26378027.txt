How to Meet the Lucky Stars
 
 
{{multiple issues|
 
 
}}

{{Infobox film
| name           = How to Meet the Lucky Stars
| image          = HowToMeettheLuckyStars.jpg
| caption        = Hong Kong film poster
| film name = {{Film name| traditional    = 運財五福星
| simplified     = 运财五福星
| pinyin         = Yún Qǎi Wŭ Fú Xīng
| jyutping       = Wan6 Coi6 Ng2 Fuk1 Sing1}}
| director       = Frankie Chan
| producer       = Eric Tsang Ivy Lee
| starring       = Sammo Hung Eric Tsang Richard Ng Stanley Fung Michael Miu Françoise Yip
| music          = Roel A. Garcia
| cinematography = Sung Kong Ng Wing Kit Venus Keung Fletcher Poon
| editing        = Jelly Mak
| studio         = Grand March Movie Production Co., Ltd. Golden Harvest(Current)
| released       =  
| runtime        = 112 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK $2,084,545
}} 1996 Cinema Hong Kong film and the final film in the Lucky Stars film series. Featuring the "Lucky Stars" Sammo Hung (in his original role and another role as a cop), Eric Tsang, Richard Ng, Stanley Fung, Michael Miu and new cast member Vincent Lau as Hungs younger cousin and Françoise Yip as their love interest (except for Hung and Lau). Also featuring a number of guest appearances including Natalis Chan, Chen Kuan Tai, Cheng Pei-pei, Chan Hung Lit and Nora Miao. Produced by Eric Tsang, directed by Frankie Chan with action choreography by Yuen Cheung-Yan and Mars (actor)|Mars.The film was released as a benefit film for the famous Hong Kong film director, Lo Wei, who died in 1996.

==Synopsis==
During an International Gambling Competition, "The King of Gamblers" Lui Tin (Chen Kuan Tai) lost to the lacivious, psychotic lesbian queen of gamblers Sheung Kung Fei Fa aka "The Gambling Flower" (Kung Suet Fa) which caused him to commit suicide. His daughter Wai Lam (Fung Sau Yin) vows to avenge her fathers death and get help from her late fathers good friend. Uncle Wah (Cho Tat Wah), who is a police inspector. Wah also enlists the "Lucky Stars" to assist him.

==Cast==

===The Lucky Stars===
{| class="wikitable"
|-
! width=25% | Cast
! width=50% | Role
|-
| Sammo Hung || Eric / Kidstuff / Chi Koo Choi
|-
| Eric Tsang || Roundhead / Lo Han-Kuo
|-
| Richard Ng || Sandy / Dee
|-
| Stanley Fung || Rawhide / Rhino Skin
|-
| Michael Miu || Pagoda /  Ginseng 
|-
| Vincent Lau || Leung
|-
|}

===The Female Team===
{| class="wikitable"
|-
! width=25% | Cast
! width=50% | Role
|-
| Françoise Yip || Francoise
|-
| Kung Suet Fa || Sheung Kung Fei Fa aka "The Gambling Flower"
|-
| Fung Sau Yin || Lui Wai Lam
|-
| Diana Pang || Bo Bo
|-
|}

===Police force===
{| class="wikitable"
|-
! width=25% | Cast
! width=50% | Role
|-
| Cho Tat Wah || Uncle Wah
|-
| Sammo Hung || Big Head Tai Lam Choi
|-
|}

===Guest star===
{| class="wikitable"
|-
! width=25% | Cast
! width=50% | Role
|-
| Natalis Chan || King of Swindler
|-
| Chen Kuan Tai || Lui Tin aka "The King of Gamblers"
|-
| Cheng Pei-pei || Chu Ba
|-
| Nora Miao || Gamble Announcer
|- Mark Houghton || Blonde Henchmen
|-
| Kim Penn || Niki (uncredited)
|-
|}

===Cameo appearance===
{| class="wikitable"
|-
! width=25% | Cast
! width=50% | Role
|-
| Chan Hung Lit || Supreme of Gamblers
|-
| Wong Yat Shan || Cheung Fan
|-
| Shing Fui-On || Tai Sor (Big Crazy)
|-
| Kingdom Yuen || Tai Sors wife
|- Joe Ma ||
|-
| Tony Liu || Gambler
|-
| Yuen Cheung-Yan || Mahjong Player
|- Mars || Mahjong Player
|-
| Lau Shek Ming || Gambler
|-
| Benny Lai || Henchmen
|-
| Anthony Carpio || Henchman (uncredited)
|-
| Johnny Cheung || Henchman
|-
| Rocky Lai || Henchman
|-
| William Tuen || Gambler
|-
| Gabriel Wong || Man in Green Suit
|-
| Lowell Lo ||
|- James Tien ||
|}

==Box office==
This film grossed HK $2,084,545 during its theatrical run from 4–19 July 1996 in Hong Kong.

==External links==
* 
* 
*  at Hong Kong Cinemagic

 
 
 
 
 
 
 
 
 