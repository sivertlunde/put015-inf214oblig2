Lilla Jönssonligan på styva linan
{{Infobox film
| name           = Lilla Jönssonligan på styva linan
| image          = jonssonliganstyvalinan.jpg
| caption        =
| director       = Christjan Wegner
| producer       = Joakim Hansson Björn Carlström
| writer         = Christjan Wegner Björn Carlström
| starring       = Kalle Eriksson Jonathan Flumee Fredrik Glimskär
| music          = Christer Sandelin Ragnar Grippe (theme) AB
| released       =  
| runtime        = 103 minutes
| country        = Sweden Swedish
| SEK  (2,819,237 USD) SEK  (2,217,662 USD)
}}

Lilla Jönssonligan på styva linan is the second film in the Swedish Lilla Jönssonligan film series. It was released on November 28, 1997, in Sweden and was directed by Christjan Wegner. http://www.imdb.com/title/tt0119539/ 
 TV3 and TV4 (Sweden)|TV4.  )  It was released in Germany on September 14, 2000, as  Die Jönnson Bande: Charles Ingvars neuer Plan. 

==Plot==
Its in the middle of the summer. Sickan, Ragnar and Dynamit- Harry, or the Jönsson League as they call themselves, has nothing to do until the circus comes to town. Sickan quickly thinks out a plan on how theyre going to get tickets. However, after theyve got their hands on the tickets, the three friends arch enemy, Junior Wall- Enberg, son of the citys mayor Vigor Wall- Enberg, steals the tickets. 

The Jönsson League goes to Juniors house during night to get the tickets back, but theyre not the only ones  sneaking around the mayors house. Three members of the circus, the human cannonball, the sword swallower, and the clown, are actually criminals and are at the house to steal the families paintings. The police arrives to the house after an alarm is triggered, and they find The Jönsson League hiding, while the circus members escape with the paintings. The Jönsson League becomes the main suspect of the stolen paintings, so they dress up as girls and go to the circus to get the paintings back.

==Cast==
*Kalle Eriksson - Charles-Ingvar "Sickan" Jönsson 
*Jonathan Flumée - Ragnar Vanheden
*Fredrik Glimskär - Dynamit-Harry
*Jonna Sohlmér - Doris
*Robert Gustafsson - Cannonball
*Johan Rabaeus - Clown
*Ulla Skoog - Sword swallower 
*Peter Rangmar - Sigvard Jönsson Cecilia Nilsson - Tora Jönsson
*Isak Ekblom - Sven-Ingvar Jönsson 
*Loa Falkman - Oscar Wall-Enberg 
*Lena T. Hansson - Lilian Wall-Enberg 
*Micke Dubois - Loket
*Peter Harryson - Amusement park director

== References ==
 

==External links==
* 
* 

 

 
 
 
 
 

 