Hear Me Good
{{Infobox film
| name           = Hear Me Good
| image          = Hear Me Good poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Don McGuire 
| producer       = Don McGuire 
| screenplay     = Don McGuire 
| starring       = Hal March Joe E. Ross Merry Anders Jean Willes Milton Frome Joey Faye
| music          = Jack Hayes Leo Shuken
| cinematography = Haskell B. Boggs
| editing        = George Tomasini
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Hear Me Good is a 1957 American comedy film written and directed by Don McGuire. The film stars Hal March, Joe E. Ross, Merry Anders, Jean Willes, Milton Frome and Joey Faye. The film was released in October 1957, by Paramount Pictures.   

==Plot==
 

== Cast ==
*Hal March as Marty Holland
*Joe E. Ross as Max Crane
*Merry Anders as Ruth Collins
*Jean Willes as Rita Hall
*Milton Frome as Mr. Ross
*Joey Faye as Charlie Cooper
*Richard Bakalyan as Hermie
*Tom Duggan as TV Director

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 