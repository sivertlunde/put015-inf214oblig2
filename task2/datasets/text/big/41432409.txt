Il caso Pisciotta
{{Infobox film
 | name =Il caso Pisciotta
 | image = Il caso Pisciotta.jpg
 | caption =
 | director =Eriprando Visconti
 | writer =  
 | starring =  
 | music =  Antonino Riccardo Luciani
 | cinematography =    Erico Menczer
 | editing =  Franco Arcalli
 | producer =  
 | language = Italian 
 }} historical drama film written and directed by Eriprando Visconti. It is based on actual events involving Gaspare Pisciotta, lieutenant of the bandit Salvatore Giuliano, and his death by poisoning in jail in 1954.   

== Cast ==

*Tony Musante: Francesco Scauri 
*Carla Gravina: Gemma
*Salvo Randone: Don Ferdinando Cusimano
*Saro Urzì: Don Vincenzo Coluzzi
*Arturo Dominici: Michele Scauri
*Mico Cundari: DEusebio 
*Michele Placido: Amerigo Lo Jacono
*Corrado Gaipa: direttore del carcere
*Duilio Del Prete: Agent Sciurti
*Nino Terzo: Rocco Minotti
*Renato Pinciroli: Salvatore Pisciotta
*Paolo Modugno: Gaspare Pisciotta
*Vittorio Mezzogiorno: Agent Beretta
*Antonio Casagrande: maresciallo 
*Simonetta Stefanelli: Anna

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 