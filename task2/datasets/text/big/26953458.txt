21 and a Wake-Up
{{Infobox film
| name        = 21 and Wake-Up
| image       =
| director    = Chris McIntyre
| producer    = Nguyen Quy Phuong
| writer      = Chris McIntyre
| starring    =  Amy Acker JC Chasez Danica McKellar Faye Dunaway Tom Sizemore Ed Begley, Jr. Wes Studi Andre Royo Tim Thomerson Ben Vereen Lance Guest 
| music       = Brooke Wentz
| cinematography = Stephen G. Shank
| editing     =
| studio      =
| distributor =
| released    =  
| country     = United States
| runtime     =
| language    = English
| budget      =
}}
21 and Wake-Up is a 2009 American war film starring Amy Acker, Danica McKellar, Faye Dunaway, and was directed and written by producer, director, and writer Chris McIntyre.

21 and a Wakeup is the first American film about the Vietnam war allowed to shoot on location in Vietnam after the Vietnam War. It is based on a dozen true stories, most surrounding the final days of the 24th Evacuation Hospital, the last major Army hospital in the south to close as Americans abandoned Southeast Asia. 21 and a Wakeup has an all-star cast, and was written and directed by Chris McIntyre, a veteran of the US Marine Corps from 1967 through 1971, the most conflicted years of the war. The film focuses on real people McIntyre knew in the Marines, as well as experiences of Dr. Marvin Wayne, renowned and decorated physician at the 24th Evac in its final year. 21 and a Wakeup has been described as "an action packed drama that genuinely gets under the skin of its characters, showcasing the breathtaking beauty of Vietnam that most Americans have never seen."

==Plot== American bombing of Cambodia.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 