Chaindance
{{Infobox Film
| name           = Chaindance
| image          = Chaindance.jpg
| image_size     = 
| caption        = 
| director       = Allan A. Goldstein Richard Davis Michael Ironside (executive producer) Rose Lam Waddell (associate producer)
| writer         = Michael Ironside Alan Aylward
| narrator       = Michael Ironside
| starring       = Michael Ironside Brad Dourif Don S. Davis Bruce Glover Rae Dawn Chong
| music          = Graeme Coleman
| cinematography = Tobias A. Schliessler
| editing        = Allan Lee
| distributor    = Academy (video) Ascot Video New City Releasing
| released       = 1991
| runtime        = 109 minutes
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Chaindance is a 1991 Canadian drama film. The film stars Michael Ironside, Brad Dourif, Rae Dawn Chong, and Don S. Davis as "Sergeant".

== Plot ==

The film is based on Alan Aylwards work with physically disabled children as a recreational therapist at a residential chronic care centre in Toronto from 1974 to 1980.  The original script suggested Nick Nolte as Blake (played by Mike Ironside) and John Hurt as Johnnie (played by Brad Dourif).  Hurt (who Aylward had worked with on his Documentary Series - The Disability Myth, was interested in the role of Johnnie)  Noltes agent wanted a few million dollars for a retainer which Aylward couldnt muster in his life.  Thus the next choice - Ironside.  The film only got made because of Mike Ironside.  He was on set with Nick Nolte and Powers Boothe in New Mexico making a film called Extreme Prejudice.  Aylward gave the script to Lori Rotenberg, Mikes Toronto agent, who sent it to Ironside to read for the role of Blake.  Ironside just finished the script (a prison story) when Nolte emerged from his trailer to announce that he had just secured the rights to produce "Weeds", based on the book by the same name - and also "a prison story".  Ironside thought it was pure serendipity. If Noltes gutsy enough to make a prison film, why not Ironside.  Mike optioned the script from Aylward in 1987/88 and despite the odds, got it produced in Vancouver and released within two years - Kudos to him.  A few years later, the British Columbia Corrections Ministry started a program between prisoners and institutionalized handicapped patients based on the fictitious rehabilitation program in Chaindance.

== Recognition ==

* 1991
** Genie Award for Best Achievement in Art Direction/Production Design - Phil Schmidt - Nominated
** Genie Award for Best Achievement in Editing - Allan Lee - Nominated Richard Davis - Nominated
** Genie Award for Best Performance by an Actor in a Leading Role - Brad Dourif - Nominated

== External links ==
*  
*  

 
 
 
 
 
 
 