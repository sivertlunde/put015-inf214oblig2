The Chieftain (film)
 
{{Infobox film
| name           = The Chieftain
| image          = 
| caption        = 
| director       = Terje Kristiansen
| producer       = 
| writer         = 
| starring       = Terje Kristiansen
| music          = 
| cinematography = Paul René Roestad
| editing        = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}
 Best Foreign Language Film at the 57th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Terje Kristiansen as Arne Strømberg
* Vibeke Løkkeberg as Eva
* Tonje Kleivdal Kristiansen as Turid
* Eva von Hanno as Toril
* Klaus Hagerup as Tom
* Arne Hestenes as Sjefen
* Sverre Anker Ousdal as Harald Ås
* Sigbjørn Bernhoft Osa as Bestefar

==See also==
* List of submissions to the 57th Academy Awards for Best Foreign Language Film
* List of Norwegian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 