Angel, Angel, Down We Go
{{Infobox film
| name           = Angel, Angel, Down We Go
| image          =
| image_size     =
| caption        = Robert Thom 
| producer       = Jerome F. Katzman Sam Katzman
| writer         = Robert Thom
| based on       =
| starring       = Jennifer Jones Jordan Christopher Holly Near Lou Rawls Charles Aidman Davey Davison Roddy McDowall
| music          = Barry Mann Cynthia Weil
| cinematography = John F. Warren
| editing        = Eve Newman
| studio         = Four Leaf Productions
| distributor    =American International Pictures (US)
| released       = 1969
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Robert Thom, who had previously written Wild in the Streets for American International Pictures, who produced this follow-up. 

It was the first film Jones made following her suicide attempt. 

It was made for Sam Katzmans Four Leaf Productions. MOVIE CALL SHEET: Bill Holden Signed for Tree
Martin, Betty. Los Angeles Times (1923-Current File)   21 Dec 1968: a6. 

==Plot==
The emotionally troubled daughter (Holly Near) of an affluent but brittle Hollywood couple gets caught up with a charismatic rock singer (Jordan Christopher) and his friends. The singer proceeds to seduce and manipulate her entire family.

==References==
 

==External links==
*  at IMDB
*  at Jennifer Jones Tribute Site

 
 
 
 


 