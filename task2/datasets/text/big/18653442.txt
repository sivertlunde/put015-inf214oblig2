Horror Rises from the Tomb
{{Infobox Film 
| name = Horror Rises from the Tomb
| image = Horror-rises-from-the-tomb.jpg
| caption = Spanish film poster
| director = Carlos Aured
| producer = Modesto Pérez Redondo
| writer = Paul Naschy
| starring = Paul Naschy
| music = Carmelo A. Bernaola
| cinematography = Manuel Merino
| editing = Javier Morán
| studio = {{plainlist|
* Profilmes Avco Embassy
}}
| distributor = 
| released =  
| runtime = 89 minutes
| country = Spain
| language = Spanish
| budget = 
}}
Horror Rises from the Tomb ( ), is a 1972 Spanish horror film starring Jacinto Molina (also known as Paul Naschy) and was directed by Carlos Aured. It was followed by a sequel, Panic Beats.    The film introduced Naschys character of Alaric de Marnac, an executed warlock who returns to life centuries later to wreak his revenge. De Marnac later returned in the sequel.

== Reception ==
Writing in The Zombie Movie Encyclopedia, academic Peter Dendle cites Night of the Living Dead as having inspired the depiction of a zombie siege in this film.   Kurt Dahlke of DVD Talk wrote, "Clearly Horror Rises From The Tomb is over-the-top, and a shining example of sleazy Euro-trash cinema."   In another review from DVD Talk, Stuart Galbraith rated it 2/5 stars and recommended it to horror fans "open-minded enough to forgive   films frequent incoherence and low budgets". 

== References ==
 

== External links ==
*  

 
 
 
 
 


 
 