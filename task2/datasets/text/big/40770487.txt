Kamen Rider × Kamen Rider Gaim & Wizard: The Fateful Sengoku Movie Battle
{{Infobox film
| italic title   = force
| name           = Kamen Rider × Kamen Rider Gaim & Wizard: The Fateful Sengoku Movie Battle
| film name = {{Film name| kanji = 仮面ライダー×仮面ライダー 鎧武&ウィザード 天下分け目の戦国 MOVIE大合戦
| romaji = Kamen Raidā × Kamen Raidā Gaimu Ando Wizādo Tenka Wakeme No Sengoku Mūbī Daikessen}}
| image          = Gaim & Wizard the movie.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Ryuta Tasaki
| producer       =  
| writer         =  
| narrator       =
| starring       =  
| music          =  
| cinematography = Koji Kurata
| editing        = Naoki Osada Toei
| Toei Co. Ltd
| released       =  
| runtime        = 93 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a crossover film featuring the main characters of Kamen Rider Gaim and Kamen Rider Wizard as the part of the annual winter "Movie War" franchise, as well as to celebrate the fifteenth anniversary of the Heisei era Kamen Rider Series and the fifth anniversary of the "Movie War" franchise itself. It was released in Japan on December 14, 2013.  Unlike previous Movie War films which would feature the new series secondary Kamen Rider, the film teases the then upcoming "Yggdrasill Saga" arc of Kamen Rider Gaim, introducing the Genesis Driver belt and Ryoma Sengokus bodyguard Yoko Minato.

==Plot==
Unlike previous Movie War films, this one is only divided in two stories:   and  .

===Kamen Rider Wizard: The Promised Place===
While Haruto is traveling abroad to look for a place to hide the Hope Ring, Kizaki, Rinko, and Nito break into a laboratory where artificial copies of the Phantom Carbuncle are being created. They are attacked by the Phantom Ogre who devours the copies and easily defeats the three Kamen Rider Mages, Mayu, Yuzuru, and Yamamoto before leaving. Ogre then goes after Haruto at a beach near Fortaleza, Brazil, in an attempt to force him into despair and bring forth his Phantom Wizardragon. After failing, the monster steals the Hope Ring from Haruto instead and flees.

After returning to Japan and reuniting with his friends, Haruto is attacked by Ogre once more, bringing with them a copy of Koyomi he created with the Hope Ring that is able to transform into the White Wizard in order to have Haruto choose between killing Koyomi or having her go in a rampage, possibly falling into despair. To prevent that, Nito knocks down Haruto and despite being warned about the consequences, wears a restored Beast Driver and renews his pact with Chimera, transforming into Kamen Rider Beast once again but he fails to defeat them as well. Back at the Antique Shop Omokagedō, Haruto regrets the fact that he did not dispose of the Hope Ring thanks to his attachment to Koyomi and is given a brand new Magic Ring crafted by Shunpei. Back at the fight, Kamen Rider Beast faces against Ogre while Kamen Rider Wizard confronts the White Wizard and, after Shunpeis ring fails to work, Wizard succeeds in dissuading Koyomi and returns the Hope Ring to normal. Ogre then forces himself into Harutos Underworld in order to capture Wizardragon there and the Hope Ring reacts with Shunpeis ring, allowing Kamen Rider Wizard to enter his own Underworld, while Kamen Rider Beast confronts a trio of Carbuncle copies. Kamen Rider Beast defeats the copies while Haruto finishes Ogre and meets an image of Koyomi inside his Underworld, whom he entrusts with the Hope Ring before returning to the real world. Haruto and Nito celebrate their victory with their friends when a Crack appears and the Pitcher Plant Inhumanoid jumps from it, absorbing Nito before jumping back into the Crack, beside a red-colored Armored Rider resembling Gaim.

===Kamen Rider Gaim: Sengoku Battle Royale!===
A competition is being held in Zawame City between Armored Riders Gaim, Ryugen, Baron, Kurokage, and Gridon with a batch of Lockseeds from DJ Sagara as a prize when a Crack opens amid the battle and the Pitcher Plant Inhumanoid appears from it, attacking Mai. Gaim, Ryugen, and Baron protect Mai and pursue the Inhumanoid as it retreats through the Crack. The Riders arrive in an alternate reality where the inhabitants run away in fear from Gaim, mistaking him for someone else. Looking for answers, they join a battle with a Kamen Rider resembling OOO, while Takatora learns of the situation and follows after the other Armored Riders. Kaito breaks into a burning castle and meets Nobunaga, who entrusts him with a Core Medal before dying amid the flames, but he manages to save Ranmaru from the fire. Outside, the red Kamen Rider Gaim appears and defeats Kamen Rider OOO, before having him absorbed by the Pitcher Plant Inhumanoid. Kouta and Mitsuzane flee the scene just to be later captured by an alternate Iyeasu Tokugawa, who reveals that in that reality each of the Heisei Riders are Bujin Riders serving a feudal warlord and fighting to unify the country but most of them have been defeated, including his own Bujin Wizard, by the red-colored Kamen Rider Bujin Gaim, who serves no one except for himself. 

As Mai enters a Crack in search for her friends, Kaito saves Ranmaru from some bandits, earning her gratitude and loyalty. Bujin Gaim then fights one of the last Bujin, Bujin Double and Armored Rider Zangetsu intervenes, but unintentionally assist Bujin Gaim as he realized that Bujin Gaim is not the Gaim from his dimension, leading to Doubles defeat, leaving behind Kamen Rider Doubles Joker Gaia Memory which Zangetsu retrieves and escape from Bujin Gaims attacks. After Kouta agrees to work with Ieyasu only until Bujin Gaim is defeated, Kouta and Mitsuzane join the battle between Bujin Gaim and Bujin Fourze but fail to protect Fourze from Bujin Gaim, with both Fourze and Ryugen being absorbed by the Pitcher Plant Inhumanoid. After the battle, Takatora appears and retrieves one of Fourzes Astroswitches. Back in Ieyasus castle, Kouta learns that Kaito is rallying the armies from the now defeated warlords and intends to conquer the entire land. Mai arrives in the alternate realm and meets Kaito, who refuses to give up on his plans, claiming that he prefers staying there than returning home. When she parts ways from him, Mai is attacked by the Pitcher Plant Inhumanoid again just to be rescued by the real Kamen Rider Wizard, who has her reunite with Kouta while trying to save Nito.

Kouta and Haruto prepare to face an assault from Kaitos forces, while Bujin Gaim uses the power of the other Bujin Kamen Riders to take control of the sacred tree. He returns to Ieyasus castle to kidnap Mai, who he claims is the "Priestess of Fate". As the three forces clash, Baron joins forces with Wizard, Gaim, and Zangetsu to defeat the Pitcher Plant Inhumanoid and free Mitsuzane and Nito. The six Kamen Riders then confront Bujin Gaim, with Ieyasu entrusting Kamen Rider Wizard with Bujin Wizards Infinity Ring and Kouta carrying the Flame Ring. The mysterious girl appears before the Armored Riders and creates some Helheim Fruits from which they create Lockseeds from the relics of the fallen Bujin Kamen Riders to attain the powers of Wizard, OOO, Fourze, and Double. Infused with the sacred tree, Bujin Gaim absorbs Kamen Rider Wizard and Gaim, who receive the powers of the other fallen Bujin Kamen Riders to escape and destroy Bujin Gaim with their combined powers. After the battle, Nito discovers that he can feed Chimera with Helheim Fruits in order to avoid being consumed by it and the Kamen Riders bid farewell to each other as they return home. Elsewhere, Takatora is given a new transformation belt by his scientist Ryoma Sengoku. Kouta, Kaito, and Mitsuzane still manage to return to Zawame to face Jonouchi and Hase in their battle royale.

==Production==
The films guest stars include popular models turned tarento JOY and Atsushi. JOY portrays an alternate interpretation of Tokugawa Ieyasu in the Sengoku Movie Battle portion of the film, while Atsushi will portray the Phantom known as Ogre, otherwise known as Osuga, in the Kamen Rider Wizard portion. JOY stated that he was happy to be working on such a popular film, and studied Ieyasu to prepare for the role, hoping that his performance will also teach children history. Atsushi, on the other hand, was thrilled that he will be playing the villain, and mentioned how he enjoyed how he looked in make up and as his costumed alter ego. Rounding out the cast are guest appearances from Kamen Rider alumni Hiroaki Iwanaga (Akira Date/Kamen Rider Birth in Kamen Rider OOO) as an alternate interpretation of Oda Nobunaga, Minehiro Kinomoto (Ryu Terui/Kamen Rider Accel in Kamen Rider W) as an alternate interpretation of Toyotomi Hideyoshi, and Hikaru Yamamoto (Akiko Narumi in Kamen Rider W) as an alternate interpretation of Yodo-dono also known as Chacha; Keisuke Kato (Keisuke Nago/Kamen Rider Ixa in Kamen Rider Kiva) and Ryuki Takahashi (Kengo Utahoshi in Kamen Rider Fourze) also appear, but their parts are original characters in the film rather than interpretations of other Sengoku period historical figures. Additionally, model and actress Mao Ueda will portray an alternate interpretation of Mori Ranmaru. The film script was written by Nobuhiro Mouri (working on the Gaim and crossover parts) and Junko Kōmura (working solely on the Wizard part) with Ryuta Tasaki as director.   

==Cast==
;Wizard cast
*  :  
*  :  
*  :   
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  

;Gaim cast
*  :  
*  ,  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  : Kamen Rider Girls
*  :  
*  :  
*  :   JOY
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  ,  
*  :  

==Theme song==
* "TEPPEN STAR"
** Lyrics: Shoko Fujibayashi
** Composition: JIN, junchi, Shuhei Naruse
** Arrangement: JIN
** Artist: hitomi

==Notes==
 

==References==
 

==External links==
*  Toei

 
 
 

 
 
 
 
 
 
 