Lea (film)
 
 
{{Infobox film
| name           =Lea
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Ivan Fila
| producer       = Ivan Fila Eliska Sekavova Herbert Rimbach
| writer         = 
| narrator       = 
| starring       = Lenka Vlasakova Christian Redl Hanna Schygulla Miroslav Donutil
| music          = Petr Hapka
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =
| runtime        = 100 minutes
| country        = Czech Republic/Germany
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Czech drama film. It was released in 1997.

==Plot==
Lea witnesses her mothers rape and murder by her father as a child and because of it speaks very little and writes poems to her mother.  Lea then grows up with foster parents in a different part of Slovakia.  Strehlow buys Lea, now aged 21, from her foster father and imprisons her in a castle in Germany, using the same tools Leas father used to control Lea as a child and to kill Leas mother.  As Strehlow learns more of Leas past, he permits her to continue writing to her mother.  Lea dies of a stroke within a year of living with Strehlow.

==Cast==
* Lenka Vlasakova.... as Lea
* Christian Redl.... as Strehlow
* Hanna Schygulla.... as Wanda
* Miroslav Donutil.... as Gregor Palty
* Udo Kier.... as Block
* Gerd Lohmeyer.... as Postmaster
* Tereza Vetrovska.... as Young Lea

==Awards==
; 1997 Angers European First Film Festival
* Won Audience Award for Feature Film (tied with Some Mothers Son)
* Won C.I.C.A.E award
* Won Telcipro award (tied with Pretty Village, Pretty Flame)

;1997 Brussels International Film Festival
* Won Audience Award
* Won Crystal Star Award for Best European Feature

;Cinequest San Jose Film Festival
* Won Best Feature Award
* Nominated for Maverick Spirit Award

;1998 Czech Lions
* Won Best Actress Award going to Lenka Vlasakova
* Won Best Cinematography going to Vladimir Smutny
* Nominated for Best Design Achievement going to Petr Kunc and Ludvik Siroky
* Nominated for Best Director going to Ivan Fila
* Nominated for Best Editing going to Ivana Davidova
* Nominated for Best Film
* Nominated for Best Screenplay going to Ivan Fila
* Nominated for Best Sound going to Marcel Spisak and Max Rammier-Rogall

;1996 European Film Awards
* Nominated European Film Award for Best Young Film

;1997 German Film Awards
* Nominated Gold Film Award for Outstanding Feature Film

;1998 USA Golden Globe
* Nominated Golden Globe for Best Foreign Language Film

;1997 London Film Festival
* Won Satyajit Ray Award

;1997 Max Ophüls Festival
* Won Audience Award

;1997 Sochi International Film Festival
* Won FIPRESCI Prize

;1997 Stockholm Film Festival
* Nominated Bronze Horse Award

;1997 Venice Film Festival
* Won OCIC Award in Honorable Mention

==External links==
*  

 
 
 
 


 