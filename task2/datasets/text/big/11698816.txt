Falling Sky
 
{{Infobox film
| name           = Falling Sky
| image          = 
| alt            =  
| caption        = 
| executive producers        = Jeff Geoffray Walter Josten
| director       = Russ Brandt Brian de Palma
| producer       = Mark Burman Jan Eisner Gary Shar Sabrina Sipantzi
| writer         = Brian de Palma
| starring       = Karen Allen Brittany Murphy
| music          = Daniel Getz Ivan Koutikov
| cinematography = Russ Brandt
| editing        = Daniel Loewenthal
| studio         = Shar Visions Burman Entertainment MEB Entertainment Walpen Adevntures
| distributor    = DEJ Productions
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
}} UK in US until August 26, 2005. 

== Synopsis ==
Murphy plays the role of Emily. Her alcoholic mother, Resse, tells her that the disease runs in the family, and she is at risk of having it too. After Resse kills herself in the bathtub, she leaves a recorded tape saying "Im sorry Emily, but Im just so tired."  Devastated, Emily turns to drink, and the prophecy comes true. 

However, after seeing what alcohol has done to her life, Emily drives to a lake and tries to drown herself. At the last minute, she gasps for air and says "My mother chose to die. There was nothing I could do to ease her pain. Except maybe, choosing to live."

== Cast ==
* Karen Allen as Resse Nicholson
* Brittany Murphy as Emily Nicholson Jeremy Jordan as Vance Chris Young as Kyle
* Patrick Renna as Mark
* R. D. Robb as C.C.
* Lea Moreno as Allie
* Vincent Riotta as Mr. Hanes
* Giselle Anthony as Stacy
* Montana Dillenberg as Young Emily
* Michael Dempsey as Tom Collins
* Jennifer Jernigan as Prostitute
* Robert Langston as Willie
* Dianne Kay as Waitress
* Ali Gage as Trucker Chic

== References ==
 

==External links==
* 

 
 
 
 
 
 

 