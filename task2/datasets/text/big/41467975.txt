Auld Lang Syne (1937 film)
{{Infobox film
| name =  Auld Lang Syne
| image =
| image_size =
| caption =
| director = James A. Fitzpatrick
| producer = James A. Fitzpatrick
| writer = W.K. Williamson  
| narrator = Marian Spencer
| music = Gideon Fagan 
| cinematography = Hone Glendinning 
| editing = 
| studio = FitzPatrick Pictures 
| distributor = Metro-Goldwyn-Mayer 
| released = February 1937
| runtime = 72 minutes
| country = United Kingdom
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama Marian Spencer. distribution by MGM. Quota costume films were rare, as the costs generally exceeded the limited budgets allowed for productions. 

==Cast==
* Andrew Cruickshank as Robert Burns 
* Christine Adrian as Jean Armour 
* Richard Ross as Gavin Hamilton  Marian Spencer as Clarinda 
* Malcolm Graham as Gilbert Burns 
* Doris Palette as Highland Mary 
* Jenny Laird as Alison Begbie 
* Ernest Templeton as Elder MacIntosh 
* Charles Howard as Mr. Burns 
* Anne Wilson as Agnes Burns 
* Lina Naseby as Mrs. Burns 
* Winifred Willard as Jessie 
* Kate Agnew as  Mrs. Armour 
* Frank Sutherland as Mr. Armour  
* Jock Rae as Mr. McNab  
* Vi Kaley as Mrs. McNab 
* Frank Webster as Sailor  
* Anita Graham as Mrs. Dunlap

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The British of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 