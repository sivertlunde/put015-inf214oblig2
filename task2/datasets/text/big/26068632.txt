The Final Cut (1995 film)
 
{{Infobox film
| name           = The Final Cut
| image          = Thefinalcut.jpg
| image size     =
| alt            = 
| caption        =  Roger Christian
| producer       = Robert Vince William Vince
| writer         = Raul Inglis Crash Leyland
| narrator       = 
| starring       = Sam Elliott Charles Martin Smith Anne Ramsay Matt Craven
| music          = Ross Vannelli Mike Southon
| editing        = Robin Russell
| studio         = Cine Cut Films Keystone Entertainment
| distributor    = Republic Entertainment
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} American action thriller feature Roger Christian for Cine Cut Films with a screenplay by Raul Englis based on a story by Crash Leyland. 

==Plot==
A maniac bomber is ruthlessly targeting Seattle, claiming civilians and bomb disposal teams alike as apartment blocks and office complexes collapse under the impact of his ingenious, complex devices.  Calling in ex-Bomb Squad man John Pierce (Sam Elliott) to help them, and using computer assisted disposal techniques and virtual reality simulations, the Squad come to a horrifying realization - the bombs are constructed with tricks and traps intended to kill the disposal teams...and he is the only person who would know such schemes would be a fellow Bomb Squad officer.  With the Police marking Pierce as their number one suspect and the bomber on the brink of one final, cataclysmic attack, Pierce must move quickly to unmask the trigger-man behind the carnage or face taking the rap himself.

==Partial cast==
*Sam Elliott as John Pierce 
*Charles Martin Smith as Capt. Weldon Mamet 
*Anne Ramsay as Sgt. Kathleen Hardy 
*Matt Craven as Emerson Lloyd  Ray Baker as Col. Forsyth 
*Barbara Tyson as Veronica Waller 
*Lisa Langlois as Sara 
*Erich Anderson as Talberg   John Hannah as Gilmore 
*Rachel Hayward as Barmaid Gabby 
*Amanda Plummer as Rothstein 
*Campbell Lane as Kulkonne 
*Brad Loree as SWAT Cop 
*Greg Rogers as Doctor
*Lloyd Berry as Loscalzo

==Background==
The film was released in the U.S. in 1995 by Republic Entertainment with subsequent releases in Germany, Belgium, and South Africa in 1996 and with a video premiere in Portugal in 1997.

==Reception==
 

==References==
 

==External links==
* 
* 

 

 
 
 
 