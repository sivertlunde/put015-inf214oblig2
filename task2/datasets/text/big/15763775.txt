Rings on Her Fingers
{{Infobox Film
| name           = Rings on Her Fingers
| image          = RingsOnHerFingersPoster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Rouben Mamoulian
| producer       = Milton Sperling
| writer         = Robert Pirosh (story) Joseph Schrank (story)
| screenplay     = Ken Englund Emeric Pressburger (uncredited)
| starring       = Henry Fonda Gene Tierney Laird Cregar Spring Byington
| music          = Cyril J. Mockridge Leigh Harline George Barnes
| editing        = Barbara McLean
| distributor    = 20th Century Fox
| released       = March 20, 1942
| runtime        = 86 minutes
| country        =   English
| budget         = 
| gross          = 
}}
 1942 screwball comedy film starring Henry Fonda and Gene Tierney. A poor man gets mistaken for a millionaire and is swindled out of his life savings.

==Plot summary==
 
Vacationing in Southern California, accountant John Wheeler (Henry Fonda) intends to purchase a boat for which hes saved long and hard on his limited income. Maybelle (Spring Byington) and Warren (Laird Cregar), a pair of swindlers overhear John discussing the upcoming transaction and mistake him for a millionaire. They persuade pretty sales clerk Susan Miller (Gene Tierney) to help them dupe John by pretending to be their daughter and fall in love with him. 

==Cast==
* Henry Fonda as John Wheeler
* Gene Tierney as Susan Miller / "Linda Worthington"
* Laird Cregar as Warren
* Spring Byington as Mrs. Maybelle Worthington
* Shepperd Strudwick as Tod Fenwick (as John Shepperd)
* Frank Orth as Kellogg
* Henry Stephenson as Colonel Prentiss
* Marjorie Gateson as Mrs. Fenwick
* George Lessey as Fenwick Sr.
* Iris Adrian as Peggy
* Harry Hayden as Conductor
* Clara Blandick as Mrs. Beasley Charles C. Wilson as Captain Hurley

==References==
 	

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 