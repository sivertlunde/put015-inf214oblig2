The White Lions
{{Infobox film
| name           = The White Lions
| image          = The White Lions.jpg
| image_size     = 185px
| caption        = 
| director       = Mel Stuart
| writer         = 
| narrator       = 
| starring       = Michael York
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       = 1981
| runtime        = 97 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The White Lions is a 1981 drama film directed by Mel Stuart. It stars Michael York and Glynnis OConnor. 

==Plot==
A college professor, his wife, and daughter go to the wilds of Africa to live, and study the rare white lions of the region.

==Cast==
*Michael York as Chris McBride
*Glynnis OConnor as Jeannie McBride
*Donald Moffat as Vreeland
*J.A. Preston as Aniel
*Roger E. Mosley as John Kani
*Lauri Lynn Myers as Laura McBride Tom Taylor as Mr. Gleason
*Louis Heshimu White III as Intern
*Larry Drake as Fiske
*Norma Young as Prof. Thorndike David Haney as Dr. Ford
*Columbia Dowell as Mrs. Gleason
*Charles Pace as Ranger
*Hugh Gorrian as Dr. Uffner
*Sally Norvell as Sarah (as Sarah Norvell)
*William H. Burkett as Man
*Keith Alcorn as College student (uncredited)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 