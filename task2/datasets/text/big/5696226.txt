The Water Margin (film)
 
 
{{Infobox film name = The Water Margin image = WaterMarginPoster.jpg alt =  caption = Film poster traditional = 水滸傳 simplified = 水浒传 pinyin = Shuí Hǔ Zhuàn jyutping = Seoi2 Wu2 Cyun4}} director = Chang Cheh Wu Ma Pao Hsueh-li producer = Run Run Shaw screenplay = Chang Cheh Ni Kuang story = Shi Naian starring = David Chiang Tamba Tetsuro music = Frankie Chan cinematography = Kung Mu-to editing = Kwok Ting-hung studio = Shaw Brothers Studio distributor = *Shaw Brothers Studio
*New World Pictures (1973 United States theatrical release)
*Warner Home Video (1982 United States VHS release)
*Siren Visual (2005 Australian DVD release)
*Dutch FilmWorks (2006 Netherlands DVD release)
*Future Film (2006 Finland DVD release)
*Image Entertainment, (2006 United States DVD release) released =   runtime = 120 minutes country = Hong Kong language = Mandarin budget =  gross = 
}}
The Water Margin, also known Outlaws of the Marsh and Seven Blows Of The Dragon, is a 1972 Hong Kong film adapted from the Chinese classical novel Water Margin. It was produced by the Shaw Brothers Studio and directed by Chang Cheh.

==Plot== Liangshan Marsh and the outlaws battle with the Zeng Family Fortress.

Liangshans chief Chao Gai was ambushed and killed by Shi Wengong. His fellows vow to avenge him. The current de facto leaders of Liangshan, Song Jiang and Wu Yong, decide that given Shi Wengongs skill, Lu Junyi (Shis former fellow student) will be the best person to assist them in taking revenge. The outlaws also note that if they succeed in recruiting Lu Junyi, they will also win over Lus servant, Yan Qing, who is also a formidable martial artist.
 Li Kui, who is disguised as his idiotic assistant. They visit Lu Junyi and lie to him that a great calamity will befall him if he does not travel 1,000 miles to the southeast. Lu Junyi is skeptical of the advice and asks Yan Qing for his opinion. By then, Yan Qing has recognized Li Kui and provokes the hot-tempered Li into a fight. Li Kui is defeated and confined together with Wu Yong. Wu Yong uses reverse psychology on Lu Junyi and manages to persuade Lu to release them. Just then, Lu Junyis steward Li Gu, who is in a secret affair with Lus wife, reports the outlaws presence to the authorities so as to seize his masters fortune. When soldiers arrive to capture the outlaws, Lu Junyi orders Yan Qing to escort Wu Yong and Li Kui safely out while he confronts the soldiers and is arrested. Yan Qing attempts to free Lu Junyi from prison but Lu is captured again when Yan leaves him for a short while.

Yan Qing travels to Liangshan to seek help and he attempts to rob two men for his travel expenses. The two men turn out to be actually outlaws from Liangshan, one of them being Shi Xiu. Shi Xiu goes to find Lu Junyi while his companion brings Yan Qing back to Liangshan. While in the city, Shi Xiu sees that Lu Junyi is about to be executed in the market square so he storms the area alone in an attempt to free Lu. Both Lu Junyi and Shi Xiu are eventually overwhelmed by soldiers and taken into captive.

Meanwhile, Yan Qing meets the Liangshan outlaws and they plan to infiltrate the northern capital to save Lu Junyi and Shi Xiu. They take strategic positions around the execution ground and attack the soldiers when the prisoners are about to be beheaded. This time, the outlaws overpower the guards and swiftly take control of the area. Accompanied by the outlaws, Lu Junyi marches home and kills his disloyal steward Li Gu while his adulterous wife is slain by Yan Qing.

As the outlaws leave the city, they run into Shi Wengong and his men, resulting in both sides clashing in an immense battle. Shi Wengongs forces are routed so he challenges the outlaws to engage him and his five students in man-on-man duels. The five students fight with Lin Chong, Li Kui, Hu Sanniang, Wu Song and Shi Xiu, while Lu Junyi faces Shi Wengong. By the time the five heroes had defeated Shi Wengongs students, Lu Junyi was still locked his duel with Shi. Yan Qing and Li Kui join in the fray and Shi Wengong is injured gravely but still remains alive. After declaring that Lu Junyi is now the new leader of Liangshan, Shi Wengong stops everyone from approaching and commits suicide. The outlaws, seeing that their quest for vengeance is now complete, ride back to their stronghold and the film ends.

==Cast==
*David Chiang as Yan Qing
*Tetsuro Tamba as Lu Junyi
*Toshio Kurosawa as Shi Wengong
*Tung Lam as Chao Gai
*Ku Feng as Song Jiang
*Chin Feng as Wu Yong
*Elliot Ngok as Lin Chong
*Wong Chung as Shi Xiu
*Ti Lung as Wu Song
*Lily Ho as Hu Sanniang Li Kui
*Wu Ma as Shi Qian Wang Ying Paul Chun as Hua Rong
*Chen Kuan-tai as Shi Jin Danny Lee as Zhang Shun Yang Xiong
*Lee Hang as Dai Zong
*Lau Dan as Lei Heng
*Lei Lung as Qin Ming
*Pang Pang as Lu Zhishen
*Zhang Yang as Chai Jin
*Leung Seung-wan as Liu Tang
*Lo Wai as Zhu Tong Zhang Heng
*Tin Ching as Li Gu
*Ling Ling as Madam Jia (Lu Junyis wife)
*Ruby Siu Siu as Xu Hong
*Woo Wai as Cai Fu
*Yeung Chak-lam as Cai Qing
*Liu Wai as Dong Chao
*Lee Man-tai as Xue Ba
*Lee Wan-chung as Grand Secretary Liang Shijie
*Shum Lo as chief clerk
*Wong Ching-ho as court clerk
*Nam Seok-hun as Wen Da
*Lau Gong as Li Cheng
*Cheng Miu as Zeng Nong
*Tong Yim-chan as Zeng Sheng
*Wang Kuang-yu as Zeng Mi
*Wong Pau-gei as Zeng Suo
*Chan Chuen as Zeng Kui
*Cheung Ban as Zeng Tu
*Nam Wai-lit as chief wrestler
*Gai Yuen as wrestler
*Wong Ching as wrestler
*Cheung Hei as old man
*Tsang Choh-lam as innkeeper
*Chin Chun as Lu Junyis servant
*Ko Fei as Lu Junyis servant
*Sai Gwa-pau as Lu Junyis servant
*Fuk Yan-cheng as Lu Junyis servant
*Ko Hung as Lu Junyis servant / bandit
*Huang Ha as Lu Junyis servant / Zeng militiaman
*Yeung Pak-chan as Lu Junyis servant / Zeng militiaman
*Yuen Shun-yee as Zeng militiaman
*Law Keung as Zeng militiaman
*Ho Bo-sing as Zeng militiaman
*Fung Hap-so as Zeng militiaman
*Yee Kwan as constable
*Yi Fung as constable
*Chui Fat as constable
*Chow Kong as constable
*Chow Yun-gin as constable
*Chan Dik-hak as constable
*Tam Bo as constable
*Kong Chuen as constable
*Cheung Chi-ping as constable
*Ling Hon as constable carrying execution placard
*Tung Choi-bo as constable / Zeng militiaman
*Lau Jun-fai as soldier
*San Kuai as  bandit
*Lee Wan-miu as wrestling spectator
*To Wing-leung as wrestling spectator
*Chai Lam as wrestling spectator
*Gam Tin-chue as wrestling spectator
*Bak Yu as mamasan
*Yen Shi-kwan
*Yuen Cheung-yan
*Wong Cheung
*Fung Hak-on
*Ho Kei-cheong
*Fung Ming

==Release==
The film was bought for release in the US by New World Pictures. Roger Corman cut out a third of the film, had the Shaw brothers shoot an additional sex scene and added a new narration. Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 60 

==See also==
*The Water Margin (1973 TV series)|The Water Margin (1973 TV series)
*Outlaws of the Marsh (TV series)|Outlaws of the Marsh (TV series)
*The Water Margin (1998 TV series)|The Water Margin (1998 TV series)
*All Men Are Brothers (TV series)|All Men Are Brothers (TV series)

==References==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 