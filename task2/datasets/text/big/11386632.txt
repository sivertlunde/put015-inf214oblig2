Boat (2007 film)
{{Infobox film
| name           = Boat
| image          =
| image size     =
| caption        =
| director       = David Lynch
| producer       =
| writer         = David Lynch
| narrator       =
| starring       = David Lynch Emily Stofle
| music          =
| cinematography =
| editing        = David Lynch Hilary Schroeder
| distributor    =
| released       = 30 January 2007
| runtime        = 8 minutes
| country        = United States
| language       = English
| budget         =
| preceded by    =
| followed by    =
}}
Boat is a short film directed by  .

==Synopsis==
Shot on digital video, Boat features closeup shots of a man (eventually revealed to be Lynch himself) taking a speedboat onto a lake, while a young woman (Emily Stofle) provides a dreamy, confused description of what is happening. Halfway through, Lynch turns to the camera and announces "were going to try to go fast enough to go into the night". He speeds up the boat, which does indeed travel into the night.

==Cast==
* David Lynch - Himself
* Emily Stofle - (voice)

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 