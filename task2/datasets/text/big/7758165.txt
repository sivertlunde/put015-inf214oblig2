Pullin' the Devil by the Tail
 
Pullin the Devil by the Tail is a short stop motion animation film, made by Stephen McCollum in 2003 that has won several awards.  It was broadcast on RTEtelevision on December 27, 2002, as part of "Animated Ireland", a showcase of Irish animation talent that was broadcast over Christmas of that year.  In 2009 five follow-up episodes were released. These were individually titled:

Ep 2: All The Way To Mars

Ep 3: Franks Da

Ep 4: The Black Cat

Ep 5: Billy McFlurry

Ep 6: The Wee Donkey

Each episode lasts 5 minutes, and all voice-acting, writing and animation was done by Stephen McCollum

==Cast==
* Puca Ryder - Stephen McCollum
* Frank Murphy - Stephen McCollum
* Ciaran McGinley - Stephen McCollum
* The Devil - Stephen McCollum

==Plot==
Episode 1: Three Irish men who play in a band in pubs and call themselves Stoisis - "The first punk-folk band in Ireland" - are sitting having pints after a concert, when Frank Murphy reveals that every Friday night for the past three weeks when he is walking home he sees a black goat which talks to him, but he doesnt know what its saying.
So Frank takes Puca and Ciaran to the scene and they too see the goat. After deciding that the creature must be possessed, Ciaran kills it with an axe, prompting The Devil to appear, asking "Why did youse kill my goat?"
He asks them to play him a song and if he likes it, he will not burn their souls in the ditches of Hell.

The first episode, and the ones to follow (see the Series section below) all take the same form. They open with Puca Ryder seated at a bar, who tells the stories of the episodes to the viewer in exchange for drinks of whiskey. Once he is furnished with a drink the story begins. At the end, the action returns to Puca in the pub who gives a summary of some form, and insists that his stories are all true. He then asks for another whiskey, prompting the next episode.

==Series==

After winning the prestigious McLaren award for New British Animation at the Edinburgh Film Festival, the idea was developed into a mini-series of five further episodes, each of the same approximate length as the original short film. These episodes featured the same three friends - Puca, Frank and Ciaran - getting into further, usually quite surreal adventures.
New characters introduced include Oscar OReilly, an ambitious, red-haired friend of the group whose house is often the setting for protracted drinking sessions. Puca, in the narration, describes him as "our oddball friend", and Oscar certainly holds some strange ideas. He seems resentful of his home, calling it a "dirty rotten town", and wants to journey through space in a space rocket  he rather impressively built over the course of five years. He wants to bring the right woman along with him. As Oscar points out himself, "space can be a lonely oul place, too."

Other minor characters introduced in the episodes include Billy McFlurry, a deceitful leprechaun who longs for a customized tractor suited to his small stature, an evil witch capable of transforming into a black cat, and the ghost of Franks father, returned from the dead after taking "a wee notion".

==Music==

Stoisis are a "real" band, in the sense of having released music under that moniker, and use the three characters of "Pullin The Devil By The Tail" - Puca, Murphy and McGinley - as its members, somewhat akin to the way in which Gorillaz use animated characters as their members.  The series features Stoisis songs, including one named "Pullin The Devil By The Tail" as its theme tune. Other Stoisis songs featured in the series are "Is It Yourself", in the first episode, "All The Way To Mars" in the second episode, of the same name, and "Theres A Wil Change" in the fourth episode. These songs are from Stoisiss first album, "The Filthy Songs Of Our Fathers", released in May 2000.
The name Stoisis comes from an adjective unique to Irish dialect meaning "very, very drunk."

==References==
 

==External links==
* http://www.nartystation.com/Animation.html - Bill Hazzards site featuring all six episodes.
* https://soundcloud.com/nartystation/sets/thefilthysongsofourfathers-by/ - Stoisis songs on Soundcloud

 
 
 
 