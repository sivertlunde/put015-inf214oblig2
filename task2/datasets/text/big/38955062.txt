Stan Lee's Mighty 7
{{Infobox media franchise title          =Stan Lees Mighty 7 image          = imagesize      = caption        = creator        = Stan Lee origin         = Comic books books          =  novels         = short_stories  = comics         =Stan Lees Mighty 7 #1-3 graphic_novels = strips         = magazines      = films          = shorts         = tv             = atv            = tv_specials    = tv_films       = Stan Lee’s Mighty 7: Beginnings (  ) dtv            = plays          = musicals       = games          = rpgs           = vgs            = radio          = soundtracks    = music          = toys           = otherlabel1    = otherdata1     = otherlabel2    = otherdata2     = otherlabel3    = otherdata3     =
}} Stan Lee Comics line comic book title.    

==History==
Stan Lees Secret Super Six property of Stan Lees POW! Entertainment was signed with Andy Heywards DIC Entertainment on an  TV series production deal in July 2003. The property is about alien super powered teens taught by Lee about humanity. 

In February 2010, POW! partnered with Andy Heywards A Squared Entertainment (now a part of Genius Brands International) and Archie Comics to create the Stan Lee Comics print and digital line starting with Stan Lees Super Seven.  In August, Super7, a toy manufacturer, sued POW! and partners over Stan Lee Comics Super Seven. 

In March 2012, the first issue of renamed "Stan Lees Mighty 7" six issue miniseries was published.  In March 2013, Hub Network picked up its first work from POW, "Stan Lees Mighty 7", animated pilot movie to be aired in early 2014.    By January 2014, Stan Lee Comics had the second film in production for a late 2014 release while an animated TV series was in development and expected to premier in 2015 along with the final film early that year. 

The first batch of licensing deals were made in February 2014 by Genius Brands on Stan Lee Comics behalf. Black Lantern agreed to develop a video game. While Factory Entertainment has the worldwide toy license and Zak Designs has the mealtime & on-the-go products for Canada and the US. Clothing licensees included Fame Jeans, Greensource, Adtn International, and JCorp with a targeted spring product launches.   

==Plot==
The concept is that Stan Lee is a character in the story, as himself, who runs into two groups of aliens that he mentors to be a superhero team while he uses them to write comic books. The aliens are 2 marshals transporting 5 criminals that crash land on Earth.  Besides Lee, a covert government agency is aware that they land with a leader of a military unit becoming an antagonist of the team.   

==Comic book==
{{Infobox comics team and title
 
  name                = Mighty 7 image               =   imagesize           =   caption             =  publisher  Stan Lee Comics (Archie Comics) debuthead           =  debut               = Stan Lee Mighty 7 #1 debutmo             = March debutyr             = 2012
|debuthead#          = 
|debut#              = 
|debutmo#            = 
|debutyr#            =  creators            = Stan Lee type                =  business            =  organization        =  team                = y base                =  owners              =  employees           =  members             =  fullroster          =

  title               = Stan Lees Mighty 7 cvr_image           =   cvr_caption         =  schedule            = bimonthly format              = limited limited             = Y genre               = Superhero pub_series          =  date                = March 2012 1stishhead          =  1stishyr            = 2012 1stishmo            = March endishyr            =  endishmo            = 
|1stishhead#         = 
|1stishyr#           = 
|1stishmo#           = 
|endishyr#           = 
|endishmo#           =  issues              =  main_char_team      =  writers             =   artists             = Alex Saviuk pencillers          =  inkers              =  letterers           =  colorists           =  editors             =  creative_team_month =  creative_team_year  =  creators_series     =  TPB                 =  ISBN                =  nonUS               =

  group = y  cat                 =  subcat              = Archie Comics altcat              =  hero                =  villain             =  sortkey             = Stan Lee&#39;s Mighty 7
}}
 Stan Lee Comics print and digital comic book line was announced with the first title "Stan Lees Super Seven" plan to be launched in July.  Tom DeFalco was initially chosen as the main author. In the fall, animated webisodes were expected to start up.   In August, Super7, a toy manufacturer, sued over Stan Lees Super Seven for trademark infringement. 

In March 2012, the first issue of renamed "Stan Lees Mighty 7" six issue miniseries was published.  The first two issue sold out.  Only three issues were printed because of interest in using the concept for a possible animated series or movie trilogy.  Digital issues were supposed to resume in 2014, but as of 2015 it hasnt happend. 

 

==Animated film==
{{Infobox film
| name           = Stan Lees Mighty 7: Beginnings
| image          = 
| alt            = 
| caption        = 
| director       = 
| producer       = 
| writer         = 
| based on       =  
| starring       =  
*Armie Hammer
*Christian Slater
*Mayim Bialik
*Teri Hatcher Flea
*Darren Criss
*Sean Astin
 
| music          = 
| cinematography = 
| editing        = 
| production companies =  
| distributor    =  
| released       =   
| runtime        = 90 minutes 
| country        = United States
| language       = 
| budget         = 
| gross          =  
}}
In April 2012, Stan Lees Mighty 7 was slated to be developed into media formats via A Squared Elxsi Entertainment LLC (A2E2).  A 2 E2 is a joint-venture between A Squared Entertainment (A 2 ) and Tata Elxsi  formed in January 2011 which Elxsi ends in October 2012. 

In September 2012, A 2  signed a foreign distribution agreement with PGS Entertainment for its programming library, including the Mighty 7 pilot animated movie with a possible 26 episode pickup.   Also, Gaiam Vivendi Entertainment was signed on as home video distributor and Hub Network as its world television premiere channel.    
  Flea as Roller Man, who rolls into a big ball and launches at high speed; Darren Criss as Micro, who shrinks in size; and Sean Astin as Kid Kinergy whose superpower is telekinesis. Additional voices include Jim Belushi as Mr. Cross, the leader of a covert operations military division assigned to investigate UFO sightings; and Michael Ironside as Xanar, the leader of the warring aliens from the planet Taegon who enslave other planets and raid their natural resources." 

On March 16, 2013, Hub Network announced its pick up of "Stan Lees Mighty 7" animated pilot movie.    In November, Lee announced that the Hub had pick up a whole trilogy of SLAM7 animated films, all to air before mid-2015 when the animated series premieres.  Also that month, A 2  merged with Genius Brands with the Heywards of A2 taking over management of the post-merger Genius Brands. 

Beginnings premiered on February 1, 2014.  On April 15, "Stan Lees Mighty 7: Beginnings" was released on DVD and Blu-ray  via Cinedigm at Walmart and Sam’s Club for an initial exclusive window. The DVD had a number of Stan Lee featuring material. 

Genius Brands International in March 2015 placed the movies rights with:
*TF1 for pan-European, French-speaking VOD, SVOD and EST rights,
*Teletoon for Canada broadcasting rights Pop Channel for UK broadcasting rights
*The Movie Partnership for market representation of all UK digital rights
*Telepictures Promoter for Middle East and North Africa Arabic-speaking countries broadcast TV rights.   

==References==
 

==External links==
*  , official website

*  

 

 
 
 
 
 
 