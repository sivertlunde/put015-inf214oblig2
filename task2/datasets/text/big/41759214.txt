Tom Clancy's Op Center (film)
 
 
{{Infobox television film
| name = Tom Clancys Op Center
| image = Tom Clancys Op Center (film).jpg
| caption =  John Savage Kim Cattrall Lindsay Frost Rod Steiger Wilford Brimley Carl Weathers
| screenplay = Tom Clancy Steve Pieczenik Steve Sohmer
| based on = Tom Clancys Op-Center
| cinematography = Alan Otino Caso
| director = Lewis Teague
| producer = Richard L. OConnor Patrick Williams
| editing = Tina Hirsch
| studio = 
| distributor = New World Entertainment
| country = United States
| language = English
| network = NBC
| first_aired =  
| last_aired =  
| runtime = current version 114 minutes
| budget = 
| producer: 
| gross = 
}}
Tom Clancys Op Center (stylized as OP Center) is a 114 minute action/political thriller which was edited-down from a 170 minute, 4-hour TV mini-series of the same name that aired in two parts on NBC in February 1995.
 John Savage and Rod Steiger.

==Plot== National Security Advisor Admiral  Troy Davis (Brimley) orders Hood and the Op Center to locate the warheads, verify their location and prepare a viable recovery plan.

Hood is initially handicapped by ignorance and naivete; when told about a  , declared destination Mombasa.

But top secret information is leaking to high powered Washington reporter Kate Michaels (Deidre Hall) and to the Israelis, whos Mossad representative Werkauf (Luis Avalos) threatens will take unilateral action if the US does not. Hood is further harassed when his unhappy wife Jane (Catrall), who doesnt like Washington or understand the seriousness of his job, leaves town. This gives Pamela Bluestone (Lindsay Frost), the Centers brilliant psychiatrist “mind-reader and Good Witch of the East,” the opening to make a play for Hood.

Before any action can be taken the intelligence-leak must be pinpointed and plugged. To that end the Center plants a bogus intelligence report on Adm. Davis for White House consumption – only to confirm that the leak leads from the President (Ken Howard) to the Israelis via Kate Michaels, the Presidents mistress.

Hood and the Center reveal the deception to Adm. Davis and present the chain of evidence; Davis in turn forces the President to end his liaison with Michaels. Meanwhile Hood, dispirited by Janes departure, tries to resign, but Adm. Davis talks him out of it.

With their source blown, the Israelis give Hood the buyers identity and his file: Abdul Fazawi (Kabir Bedi), former Mossad agent-turned-arms-dealer. Fazawi must be neutralized before the recovery operation begins, or he will disappear to set up another such deal.
 John Savage), the Centers wheelchair bound Intelligence Officer (Beirut Embassy bombing, 1983), realizes that the Israelis used the Center to flush Fazawi into the open. Meanwhile “Linebacker” has gone into action; following a short, hard fight they take control of ship, surviving crew and warheads, and disarming the scuttling charge.

At next days press conference the President announces that contrary to irresponsible... unfounded reports, there are no stolen nuclear warheads: the US, Russia and Israel have just concluded a joint exercise simulating theft and recovery of dummy warheads, and to prove it he introduces the Russian and Israeli ambassadors to answer questions. When asked how she got the Russians and Israelis to cooperate Liz Gorman (Mia Korf), the Centers long suffering and resourceful Press and Congressional Liaison, replies: “I appealed to their sense of national interest... Well you know: blackmail.”

Davis confirms Hood head of the Center for as long as he wants the job. Hood declines being interviewed by the Washington Post and heads home to his two preteen daughters, alone with the housekeeper while their mother is out of town. But when he opens the door his wife is waiting instead: Adm. Davis called Jane to explain why Paul has been so busy. As she takes him into her arms he hesitantly returns the embrace, a wary look on his face.

==Cast (partial)==
NCMC Staff
* Harry Hamlin – Paul Hood, Director NCMC
* Carl Weathers – General Mike Rodgers, Deputy Director NCMC
* Bo Hopkins – Dan McCaskey, FBI & Interpol Officer
* Lindsay Frost – Pamela Bluestone (psychiatrist)
* Mariangela Pino - Martha Macken (Political & Economics Officer) John Savage – Bob Herbert, National Intelligence Officer
* Tom Bresnahan – Matt Stoll, Operations Support
* Mia Korf – Liz Gorman (Press & Congressional Liaison)
* David Garrison - Bill Mathis, Sciences and Environmental Officer
* Todd Waring - Lowell Coffey (Lawyer & legal Advisor)
* Lou Liberatore - Surveillance technician

Washington
* Ken Howard – The President
* William Bumiller – Lou Bender (Presidential aid)
* Wilford Brimley – Admiral Troy Davis

Other cast members
* Kabir Bedi – Abdul Fazawi, Arms Dealer
* Deidre Hall – Kate Michaels (reporter)
* Sherman Howard - Uri Stalipin 
* Victor Love - Captain Ted Drake
* France Nuyen – Li Tang (antique book shop madame)
* Luis Avalos – Verkauf (Mossad)
* Kim Cattrall – Jane Hood (wife)
* Rod Steiger – Rudi Kushnerov (Boroda) (Russian liaison)
* George Alvarez – Captain Rodriguez

==Crew (partial)==
Producer: Richard L. OConnor
:Executive Producers: Tom Clancy, Toni Dailey, Steve Pieczenik, Steve Sohmer, Brandon Tartikoff
:Director: Lewis Teague Patrick Williams
:Film Editing: Tina Hirsch
Cinematography: Alan Otino Caso, Casting: Joel Thurm, Production Design: Donald Light-Harris, Art Direction: Erik Olson, Set Decoration: Ronald R. Reiss, Costume Design: Darryl Levine, Hair Stylist: Mimi Jafari, Makeup Artist: Martha Preciado, Special Effects: Kam Cooney, Stunt coordinator: Chuck Waters.

==Production notes==
The series was filmed in Los Angeles and Washington, D.C., by MT3 Services in association with New World Entertainment.

It received the 1995 Primetime Emmy Award for “Outstanding Individual Achievement in "Sound Editing for a Miniseries or a Special” for Part I.  Tina Hirsch was also nominated for a 1996 “Best Edited Episode from a Television Mini-Series” award by the American Cinema Editors, USA for Part I.

==Reception==
Note: these reviews reference the original 170-minute mini-series, not the 114-minute edited version.

As his title implies, Op Center Drowns In Numbing Details The Four-hour Mini-series Is A Talky Piece Dressed As A High-tech Espionage Thriller. Its Not Just The Weapons That Are Deadly of February 23, 1995, Jonathan Short of the   without actually naming that series.
 

John P. McCarthys review in Variety (magazine)|Variety (February 23, 1995) was professionally analytical, calling it  “...a moderately insightful, by-the-numbers lesson in geopolitics” though “the team’s banter tends to be sophomoric.” Nonetheless he praised "The strong ensemble (which) occasionally rises above the material."  He also pointed out the series three themes: the cost of serving ones country in terms of broken homes; “deception, which brings in issues like adultery and spousal abuse. Patriotism at this level involves great personal sacrifice, and no one — enemy or lover — can be trusted.” Thirdly, “The U.S. is not prepared for international crises...” 
 

Steve Johnson, of the Chicago Tribune, after decrying the scripts lack of subtly, confusing “techno-chatter” and “stick figures,” felt that “...it deftly conveys the claustrophobic, paranoiac atmosphere of a place such as Op Centers headquarters. In this environment, everybody is tailing everybody else...”  Johnson concluded: “Its ultimately worth the attention required, because a well-plotted, fast-paced story unfolds, one that does a good job of moving the spy thriller genre onto the less certain sands of modern geopolitics.” 

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 