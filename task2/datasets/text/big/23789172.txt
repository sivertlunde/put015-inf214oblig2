One Peace at a Time
 
{{Infobox film
| name           = One Peace at a Time
| image          =
| caption        = 
| starring       =  
| writer         = Turk Pipkin
| director       = Turk Pipkin
| producer       =   David Rice
| editing        =  
| distributor    = Monterey Media
| released       =  
| language       = English
}}
One Peace at a Time is a film by Turk and Christy Pipkin. It was produced by The Nobelity Project and was premiered to a sold out audience at the Paramount Theatre in Austin, Texas, USA, on April 14, 2009. It is the sequel to the film Nobelity. It has been shown all across the United States and in multiple countries across the world. 

==Summary==
Building on his film, Nobelity, Turk Pipkin continues his global journey of knowledge and action
with One Peace at a Time. While Nobelity dealt with global problems, One Peace at a Time focuses on specific solutions. The solutions Pipkin chronicles include the model Indian orphanages of The Miracle Foundation, family planning initiatives with Thailand’s Mechai Viravaidya, Ethiopian water projects with A Glimmer of Hope Foundation, and Architecture for Humanity’s global design challenge for communities in need in the Himalayas, the Amazon and the slums of Nairobi. The film looks at the possibility of providing basic rights to every child.

==Screenings and awards==

Won the Audience Award for World Cinema Documentary at the Maui Film Festival

Premiered at the Arclight Cinema in Hollywood, California.

Shown at the Heartland Film Festival.

Shown at the Hollywood Theatre (Portland, Oregon) and Auckland, New Zealand as part of the Architecture for Humanity Haiti reconstruction fund.

Shown at the 2009 Eugene International Film Festival

==Appearances==

The film stars Turk Pipkins long-time friend Willie Nelson. It also features the insights of Muhammad Yunus the first economist to win the Nobel Peace Prize Sugata Mitra instigator of the experiment known as Hole in the Wall or Minimally Invasive Education, Cameron Sinclair founder of Architecture for Humanity, United States Secretary of Energy and winner of the Nobel Prize in Physics Steve Chu, and Caroline Boudreaux founder of The Miracle Foundation.

==Production==

With the 2006 film Nobelity, The Nobelity Project brought forth many of the foremost problems happening today. In an attempt to find the answers to all the problems posed in Nobelity, Turk Pipkin spent 3 years filming, traveled to 5 continents and 20 countries, once again interviewing Nobel Laureates and working with other organizations such as CARE (relief agency)|CARE, A Glimmer of Hope Foundation, and Architecture for Humanity.

==Notes==
 

==External links==
* 

 
 