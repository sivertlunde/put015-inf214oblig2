Stony Island (film)
{{Infobox film
| name           = Stony Island
| image          =Stony_island film.jpg
| image_size     =
| caption        = Andrew Davis
| writer         = Tamar Simon Hoffs Andrew Davis
| narrator       = Richard Davis   David M. Matthews
| cinematography = Tak Fujimoto
| editing        = Dov Hoenig
| distributor    = World Northal
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
 Andrew Davis about an up-and-coming rhythm and blues band in Chicago.  Set in various places in Chicago, including gritty Stony Island Avenue, it features early appearances from Dennis Franz and Rae Dawn Chong as well as numerous local musicians including saxophone great Gene Barge. Susanna Hoffs, whose mother co-wrote the screenplay, also appears.

The film was well received by critics, who praised its music and the depiction of contemporary Chicago.  However, the film was not a commercial success. It was largely forgotten until a DVD release in 2012. 

== Cast ==

*  Richard Davis as Richie Bloom 
* Edward "Stony" Robinson as  	Kevin Tucker 
* Gene Barge as  Percy Price
* George Englund Jr. as  Harold Tate  Nathan Davis as  
* Oscar Brown as  Alderman Waller 
* Ronnie Barron as  Ronnie Roosevelt
* Tennyson Stephens as  Tennyson
* Windy Barnes as  Windy
* Rae Dawn Chong as  Janetta
* Criss Johnson as  Criss
* Dennis Franz as  Jerry Domino
* Susanna Hoffs as  Lucie 
* Meshach Taylor as   Aldemans Yes-Man

==Notes==
 

==External links==
*  
* 

 

 
 
 
 