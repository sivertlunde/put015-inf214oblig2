T-Bird at Ako
{{Infobox film
| name      = T-Bird at Ako
| image	= Tbirdatako.jpg
| alt      =
| caption    = 
| director    = Danny Zialcita
| producer    = Irene Lopez
| writer     = Portia Ilagan
| starring    = Nora Aunor Vilma Santos
| music     = Butch Monserrat
| cinematography = Ely Cruz
| editing    = Ricky Jarlego
| studio     = 
| distributor  =  Film Ventures Inc.
| released    =  
| runtime    = 155 minutes
| country    = Philippines
| language    = Filipino
| budget     = 
| gross     = 
}}
T-Bird at Ako is a 1982 Filipino film, that stars 2 of the Philippines best actresses, the Superstar Nora Aunor and the Star for all Season Vilma Santos.

==Casts==
*Nora Aunor ... Sylvia Salazar
*Vilma Santos ... Isabel
*Dindo Fernando ... Dante
*Tommy Abuel ...  Jake
*Suzanne Gonzales ... 	Babette 
* Odette Khan 	... Maxie

==Review==
*The direction is tight and masterful. Although one always gets reminded in a Zialcita film of sequences from foreign films, there is a minimum of unmotivated blocking in this film. Each sequence contributes to the whole film  (if there is copying, in other words, and I do think there is in this film, the copying is not done simply to be cute or clever, but in accordance with the logical requirements of the plot). - Isagani R. Cruz, Parade, September 22, 1982

==References==
 

==External links==
 
* http://www.oocities.org/pinoymovies/tbirdatako.html

 
 
 
 

 