Ace High (1919 film)
 
{{Infobox film
| name           = Ace High
| image          =
| caption        = George Holt
| producer       =
| writer         = Karl R. Coolidge William Pigott
| starring       = Hoot Gibson
| music          =
| cinematography =
| editing        =
| distributor    = Universal Film Manufacturing Company
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}} short Western Western film George Holt and featuring Hoot Gibson.   

==Cast==
* Pete Morrison
* Magda Lane
* Hoot Gibson
* Helene Rosson credited as Helen Rosson
* Jack Walters
* Martha Mattox

==Note==
*A film with identical title starred Tom Mix in 1918.

==See also==
* List of American films of 1919
* Hoot Gibson filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 