Dulha Dulhan
{{Infobox film
| name           = Dulha Dulhan
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Ravindra Dave	
| producer       = Ravindra Dave	
| writer         = 
| screenplay     = Inder Raj Anand
| story          = 
| based on       =  
| narrator       = 
| starring       = Raj Kapoor Sadhana Shivdasani
| music          = Kalyanji Anandji
| cinematography = M.W. Mukadam	 	
| editing        = S.R. Sawant	 	
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film produced and directed by Ravindra Dave. It stars Raj Kapoor and Sadhana Shivdasani in pivotal roles.

==Cast==
* Raj Kapoor...Raj Kumar
* Sadhana Shivdasani...Rekha / Chanda
* Agha (actor)|Agha...Bansi
* K. N. Singh...Thakur Dharam Singh
* Raj Mehra...Manager of Akashwani - Jaipur

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Mujhe Kahte Hai Kallu Qawal"
| Mukesh (singer)|Mukesh, Lata Mangeshkar
|-
| 2
| "Main Tera Hoon Dulha Tu Meri"
| Mukesh, Lata Mangeshkar
|-
| 3
| "Hamne Tujhko Pyar Kiya Hai (Male)"
| Mukesh
|-
| 4
| "Hamne Tujhko Pyar Kiya Hai (Female)"
| Lata Mangeshkar
|-
| 5
| "Jo Pyar Tune Mujhko Diya Tha"
| Mukesh
|-
| 6
| "Jumme Ki Raat Ho Ya Din"
| Mukesh, Lata Mangeshkar
|-
| 7
| "Piya Kheenche Huye Bandhe Huye"
| Kamal Barot, Lata Mangeshkar
|-
| 8
| "Tum Sitam Aur Karo"
| Mukesh
|}

==External links==
* 

 
 
 

 