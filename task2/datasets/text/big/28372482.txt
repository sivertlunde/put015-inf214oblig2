Der Preis fürs Überleben
{{Infobox film
| name           = Der Preis fürs Überleben
| image          = 
| caption        = 
| director       = Hans Noever
| producer       = Denyse Noever
| writer         = Hans Noever Patrick Roth Christian Watton
| starring       = Michel Piccoli
| music          = 
| cinematography = Walter Lassally
| editing        = Christa Wernicke
| distributor    = 
| released       = February 1980
| runtime        = 103 minutes
| country        = Germany
| language       = German
| budget         = 
}}

Der Preis fürs Überleben is a 1980 German drama film directed by Hans Noever. It was entered into the 30th Berlin International Film Festival.   

==Cast==
* Michel Piccoli - René Winterhalter Martin West - Joseph C. Randolph
* Marilyn Clark - Betty Randolph
* Suzie Galler - Kathleen Randolph Daniel Rosen - Thomas Randolph
* Ben Dova - Old Jim
* Leonard Belove - Henderson
* Michael Stumm
* Roger Burget
* Al Christy
* William Kuhlke
* Henry Effertz
* Kurt Weinzierl

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 