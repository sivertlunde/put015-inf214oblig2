Belle (2013 film)
 
 
 
{{Infobox film
| name = Belle
| image = Belle poster.jpg
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Amma Asante
| producer = Damian Jones
| writer = Misan Sagay 
| starring = {{Plainlist| 
* Gugu Mbatha-Raw
* Tom Wilkinson
* Miranda Richardson
* Penelope Wilton Sam Reid 
* Matthew Goode
* Emily Watson 
}}
| music = Rachel Portman
| cinematography = Ben Smithard
| editing = Pia Di Ciaula Victoria Boydell
| production companies = {{Plainlist| 
* DJ Films
* Isle of Man Film Pinewood Pictures BFI
}}
| distributor = Fox Searchlight Pictures
| released =  
| runtime = 104 minutes  
| country = United Kingdom
| language = English
| budget = $10.9 million
| gross = $16.5 million 
}} period drama Sam Reid, James Norton.  

The film is inspired by the 1779 painting of Dido Elizabeth Belle beside her cousin Lady Elizabeth Murray, at Kenwood House, which was commissioned by their great-uncle, William Murray, 1st Earl of Mansfield, then Lord Chief Justice of England. Very little is known about the life of Dido Belle, who was born in the West Indies and was the illegitimate mixed-race daughter of Mansfields nephew. She is found living in poverty by her father and entrusted to the care of Mansfield and his wife. The fictional film centres on Didos relationship as a young woman with an aspiring lawyer; it is set at a time of legal significance, as a court case is heard on what became known as the Zong massacre|Zong massacre, when slaves were thrown overboard from a slave ship and the owner filed with his insurance company for the losses. Lord Mansfield rules on this case in Englands Court of Kings Bench in 1786, in a decision seen to contribute to the abolition of slavery in Britain.

==Plot== Lord Chief Justice, and his wife, Elizabeth who live at Kenwood House, an estate in Hampstead outside London. Lord and Lady Mansfield raise Dido as a free gentlewoman, together with their niece Lady Elizabeth Murray, who came to live with them after her mother died and her father remarried. When the two nieces reach adulthood, the Mansfields commission an oil portrait of their two great-nieces, but Dido is unhappy about sitting for it as she is worried that it will portray her as a subordinate, like other portraits she has seen depicting aristocrats with black servants. Didos father dies and leaves her £2,000 a year. Lady Elizabeth, by contrast, will have no income from her father, whose new wife has been named his sole heir. Arrangements are made for Elizabeth to have her coming out to society, but Lord and Lady Mansfield believe no gentleman will agree to marry Dido because of her mixed-race status. 

Lord Mansfield agrees to take a vicars son, John Davinier, into an apprenticeship for law. In 1783, Mansfield hears the case of Gregson v. Gilbert, regarding the payment of an insurance claim, for slaves killed when thrown overboard by the captain of a slave-ship &mdash; an event now known as the Zong Massacre|Zong massacre. Dido helps her uncle with his correspondence and after John tells her about the Zong case, she begins sneaking correspondence to him which he believes will advance the cause of the Abolitionism|abolitionists. Lord Mansfield and John have a disagreement on the main issue of the case and John is told not to see Dido again.  Dido’s aunts, Lady Mansfield and Lady Mary Murray, Lord Mansfields sister, seek to steer Dido into an engagement with Oliver Ashford, son of a scheming grand dame and younger brother to the bigoted James Ashford. At first James is interested in Elizabeth but stops courting her once he finds out she will have no inheritance. Oliver proposes to Dido and she accepts, even though she continues to see John. After James threatens Dido with violence, she tells Elizabeth and says she will give part of her inheritance to her for a dowry so she can find a different match. Lord Mansfield finds out about Didos visits to John and confronts both of them. During the confrontation, John professes his love for Dido. Sometime later, Dido meets with Oliver and breaks off their engagement. 

Dido is relieved when the painting is unveiled, showing her as Elizabeths equal. She tells Lord Mansfield that the portrait commission proves that he can defy convention. Dido sneaks into the balcony of the Inn of Court, so that she can hear Lord Mansfield narrowly rule that the Gregson slave-trading syndicate, based in Liverpool, were not due insurance payments for the loss of slaves during a voyage, when they were thrown overboard by the crew. The ships officers claimed they ordered this action because they were out of potable water. It appeared to Lord Mansfield that the slaves were over-crowded, making them sick and not likely to fetch a high price at auction, so the officers decided they would be worth more in insurance payments after their loss. Lord Mansfield sees John and Dido outside the Court after his ruling and says that Dido can only marry a gentleman. Therefore, he agrees to resume Johns apprenticeship in law, so that he can become a gentleman. 

On-screen text informs the viewer that Dido and John married and had two children, that Elizabeth also married and had children, and that the painting hung at Kenwood House until 1922, when it was moved to Scone palace.

==Cast==
* Gugu Mbatha-Raw as Dido Elizabeth Belle
* Tom Wilkinson as William Murray, 1st Earl of Mansfield
* Miranda Richardson as Lady Ashford
* Penelope Wilton as Lady Mary Murray Sam Reid as John Davinier Sir John Lindsay
* Emily Watson as Lady Elizabeth Mansfield
* Sarah Gadon as Lady Elizabeth Murray
* Tom Felton  as James Ashford
* Alex Jennings as Lord Ashford James Norton as Oliver Ashford James Northcote as Mr Vaughan
* Bethan Mary-James as Mabel

==Painting==
  (1761–1804) and her cousin Lady Elizabeth Murray (1760–1825).]]
The 1779 painting, once thought to be by Johann Zoffany, is now attributed to an unknown artist.  The painting hung in Kenwood House until 1922 and now hangs at Scone Palace in Perthshire, Scotland. It was one of the first European portraits to portray a black subject on an equal eye-line with a white aristocrat, though distinctions are implied by the poses, as Elizabeths "formality and bookishness are contrasted with the wild and exotically turbanned natural figure of Belle."  English Heritage,    

The painting is replicated in the film with the faces of the actresses portraying the characters replacing those in the original. Didos finger-to-chin gesture is absent in the fictionalised version. The original picture is shown on screen at the end of the film.

==Production== Pinewood Pictures with support from the British Film Institute|BFI. 

Production designer Simon Bowles created the 18th-century Bristol Docks on the Isle of Man and created Kenwood House, based on a number of stately homes in the London area.

Original music for the film was composed by Rachel Portman. 

==Historical references==
The film is a work of historical fiction, inspired by a painting and the evidence that Dido was brought up at Kenwood House. The relative lack of details about Dido Elizabeth Belle allowed screenwriter Misan Sagay considerable artistic licence in framing the young womans story, within the broader historical context of the slave economy and the abolition movement.

William Murray, 1st Earl of Mansfield, who was Lord Chief Justice of England from 1756 to 1788, presided over two important cases, Somerset v Stewart in 1772 and the Zong massacre#Legal proceedings|Zong insurance claims case in 1783, which helped lay the groundwork for Britains  Slave Trade Act 1807. As in the film, he was the great-uncle of Dido Elizabeth Belle and Lady Elizabeth Murray.

At the suggestion of the producers, HarperCollins published a companion book by biographer Paula Byrne recounting the lives of the films principal characters.

==Historical accuracy==
James Walvin OBE, professor emeritus of the University of York, said of Belle: "Much of the historical evidence is there – though festooned in the film with imaginary relishes and fictional tricks. Partly accurate, the whole thing reminded me of the classic Morecombe and Wise sketch with Andre Previn (Eric bashing away on the piano): all the right notes – but not necessarily in the right order".  Reviewing the film for History Extra, the official website of BBC History Magazine, Walvin noted that while the second half of the film centres on Dido Elizabeth Belles involvement in the Zong case, in reality she was "nowhere to be found in the Zong affair".  In the film "Tom Wilkinson’s Mansfield finds his cold legal commercial heart softened, and edged towards abolition by the eyelash-fluttering efforts of his stunning great niece" and his "adjudication becomes, not a point of law, but the first bold assertion towards the end of slavery". Walvin points out that "he merely stated that there should be another hearing of the Zong case – this time with evidence not known at the earlier hearing". Walvin awarded the film one star for enjoyment and two for historical accuracy.

==Authorship==
Some press coverage ahead of filming, cited Amma Asante as the sole writer of Belle, as well as director.  Press releases that followed Fox Searchlights acquisition of the film, gave the final credit determined by the Writers Guild of America, West as "Written by Misan Sagay".   BAFTAs in 2005.     The article reported that Asante had been hired to re-draft an original screenplay written by Misan Sagay, after Sagay left the project due to serious ill-health. The subsequent arbitration process undertaken by the Writers Guild of America, determined that Sagay provided the bulk of content used in the script, so Sagay was awarded sole writing credit. Producer Damian Jones confirmed, "There was a WGA arbitration. The WGA made its decision on writing credits. And the production respects and abides by their decision." 

==Release== premiered at the 2013 Toronto International Film Festival on 8 September 2013.       The film was released on 2 May 2014 in the United States, 9 May in Canada and 13 June 2014 in the United Kingdom. 

==Reception==
The film received positive reviews from critics. Review aggregation website Rotten Tomatoes gives the film a "Certified Fresh" score of 83% based on reviews from 127 critics, with an average rating of 7/10. The sites consensus states, "It boasts all the surface beauty that fans of period pictures have come to expect, but Belle also benefits from its stirring performances and subtle social consciousness."  Critic Mark Kermode named it his second-favourite film of the first half of 2014. 

 

===Accolades===
{| class="wikitable plainrowheaders sortable" style="font-size: 95%;"
|- Award
! Date of ceremony Category
! Recipients and nominees Result
|-
! scope="row" rowspan="2"| African-American Film Critics Association 
| rowspan="2"| 8 December 2014
| Best Actress
| Gugu Mbatha-Raw 
|  
|-
| Best Film
| Belle
|  
|-
! scope="row" rowspan="5"| Black Reel Awards   19 February 2015 Best Actress
| Gugu Mbatha-Raw 
|  
|- Best Director
| Amma Asante
|  
|- Best Ensemble
| Toby Whale
|  
|- Best Film
| Belle
|  
|- Best Screenplay
| Misan Sagay
|  
|-
! scope="row" rowspan="2"| British Independent Film Awards   7 December 2014 Best Actress in a British Independent Film
| Gugu Mbatha-Raw 
|  
|-
| Best Newcomer
| Gugu Mbatha-Raw 
|  
|-
! scope="row"| Chicago Film Critics Association  15 December 2014
| Most Promising Performer
| Gugu Mbatha-Raw 
|   
|-
! scope="row"| Empire Awards  29 March 2015  Best Female Newcomer
| Gugu Mbatha-Raw
|  
|-
! scope="row"| London Film Critics Circle  18 January 2015  British Actress of the Year
| Gugu Mbatha-Raw
|  
|-
! scope="row"| Miami International Film Festival 
| 15 March 2014
| SIGNIS Award
| Belle
|  
|-
! scope="row" rowspan="5"| NAACP Image Award   
| rowspan="5"| 6 February 2015 Outstanding Actress in a Motion Picture
| Gugu Mbatha-Raw 
|  
|-
| Outstanding Directing in a Motion Picture
| Amma Asante
|  
|-
| Outstanding Independent Motion Picture
| Belle
|  
|- Outstanding Motion Picture
| Belle
|  
|-
| Outstanding Writing in a Motion Picture
| Misan Sagay
|  
|-
! scope="row" rowspan="2"| Palm Springs International Film Festival  
| rowspan="2"| 3–13 January 2014
| Audience Award for Best Narrative Feature
| Belle
|  
|-
| Directors to Watch
| Amma Asante 
|  
|-
! scope="row" rowspan="2"| Satellite Award 
| rowspan="2"| 15 February 2015  Best Actress in a Motion Picture
| Gugu Mbatha-Raw 
|  
|- Best Costume Design
| Anushia Nieradzik 
|  
|-
! scope="row" rowspan="4"| Women Film Critics Circle  
| rowspan="4"| 16 December 2014
| Best Female Images in a Movie
| Belle
|  
|-
| Best Movie by a Woman
| Amma Asante 
|  
|-
| Best Woman Storyteller
| Misan Sagay 
|  
|-
| Karen Morley Award
| Belle
|  
|-
|}

==See also==
* List of films featuring slavery

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 