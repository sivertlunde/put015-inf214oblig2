Kissi Se Na Kehna
{{Infobox film name       = Kissi Se Na Kehna image      = KissiseNaKehna.jpg caption    = Promotional Poster director   = Hrishikesh Mukherjee producer   = Jaywant Pathare, Ashok Raut	 writer     = Sachin Bhowmick (screenplay),   D.N. Mukherjee (story),   Dr. Rahi Masoom Reza (dialogue) starring   = Farooq Shaikh,   Deepti Naval,   Utpal Dutt music      = Bappi Lahiri released   = 1983 country    = India language  Hindi
|}}
 directed by Hrishikesh Mukherjee. It was a sweet comedy with actors like Farooq Shaikh, Deepti Naval and Utpal Dutt.  

==Synopsis==

Kailash Pati, a widower, lives a retired life spending time complaining about the ways of new generation.  To save his only son from getting spoiled, he decides to get him married.  In this endeavour, he meets westernized girls. Disappointed, decides he would get his son married to girl who does not speak English and is traditional in all ways.

His Son, Ramesh, on the other hand is in love with a well-educated girl, Dr. Ramola Sharma. Torn between Father and his Love, Ramesh goes to Lalaji, his fathers friend for help. Lalaji suggests to  trick Kailash Nath, by portraying Ramola as a daughter of village pundit.

The trio succeed in tricking Kailashpati to believe that Ramola is indeed an innocent girl who does not know English. They are married and then starts a storm of events to live the lie.

==Cast==
*Farooq Shaikh  as  Ramesh Trivedi
*Deepti Naval  as  Dr. Ramola Sharma/Rama
*Utpal Dutt  as  Kailash Pati
*Saeed Jaffrey  as  Lalaji (Arun Lal)
*S.N. Banerjee  as  Om Prakash
*Deven Verma  as  Mansukh 
*Ketki Dave  as  Shyamoli
*Lalita Kumari  as  Mrs. Lalaji
*Asha Sharma  as  Dollys mother

==Crew==
* Direction - Hrishikesh Mukherjee
* Producer - Jaywant Pathare, Ashok Raut
* Editor - Khan Zaman Khan
* Screenplay - Sachin Bhowmick
* Story - D.N. Mukherjee
* Dialogue - Dr. Rahi Masoom Reza
* Music - Bappi Lahiri
* Cinematography - Jaywant Pathare

==References==
 

==External links==
*  

 

 
 
 
 


 