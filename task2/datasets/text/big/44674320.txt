Parasyte: Part 1
{{Infobox film
| name = Parasyte: Part 1
| image = 
| alt = 
| caption = 
| film name =  
| director = Takashi Yamazaki
| producers =
| screenplay = {{Plainlist|
*Ryota Kosawa
*Takashi Yamazaki
}}
| based on =  
| starring = Shota Sometani
| narrator =  
| music = Naoki Satō
| cinematography = Shoichi Ato
| editing = Junnosuke Hogaki
| studio = {{Plainlist| Robot
*Toho Pictures
*Office Abe Shuji
}}
| distributor = Toho
| released =  
| runtime = 109 minutes
| country = Japan
| language = Japanese
| budget = 
| gross =  
}}
Parasyte: Part 1 is a 2014 Japanese science fiction action horror film directed by  . 

==Plot==
Aliens enter into human brains via ear canal and take control of their bodies. One of the aliens tries to enter into teenager Shinichi Izumis brain, but is blocked because of his headphones. Instead, it invades his right hand. Shinichi calls it Migi, which has now an eye and a mouth. Accepting the fact that one would die if another dies, they join forces against other parasites.

==Cast==
*Shota Sometani as Shinichi Izumi
*Eri Fukatsu as Ryoko Tamiya
*Sadao Abe as Migy
*Ai Hashimoto as Satomi Murano
*Masahiro Higashide as Hideo Shimada
*Nao Omori as Kuramori
*Kazuki Kitamura as Tsuyoshi Hirokawa
*Pierre Taki as Miki
*Hirofumi Arai as Uragami
*Jun Kunimura as Lieutenant Hirama
*Kimiko Yo as Nobuko Izumi
*Tadanobu Asano as Goto

==Release==
Parasyte: Part 1 screened at the 27th Tokyo International Film Festival as the closing film on October 30, 2014. 

The film was released on November 29, 2014 in Japan. It topped the box office on its opening weekend, earning $2.9 million from 256,000 admissions on 418 screens. 

==Reception==
Mark Schilling of The Japan Times gave the film 3 and a half stars out of 5, saying, "I couldnt call myself a fan of the manga, but the film adaptation of Parasyte hits the hard-to-find sweet spot between black comedy and serious sci-fi/horror".  Peter Debruge of Variety (magazine)|Variety in his favorable review felt that "  marks an entertaining new iteration in the body-horror category, as if someone had grafted a very dark high-school comedy onto a David Cronenberg movie."  Meanwhile, Christopher OKeeffe of Twitch Film in his unfavorable review commented that "Parasyte: Part 1 spends a great deal of time laying the groundwork for the concluding chapter and its charmless aliens and the scarcity of action in early scenes fail to make it stand on its own." 

==References==
 

==External links==
*   
* 

 

 
 
 
 
 
 
 
 
 
 