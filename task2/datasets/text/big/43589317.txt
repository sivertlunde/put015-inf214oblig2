The Peace of Roaring River
{{infobox film
| title          = The Peace of Roaring River
| image          = The Peace of Roaring River (1919) - Ad.jpg
| imagesize      =
| caption        = advertisement
| director       = Hobart Henley Victor Schertzinger
| producer       = Samuel Goldwyn
| writer         = George E. Van Schaik (story, scenario)
| starring       = Pauline Frederick
| music          =
| cinematography = Edward Gheller
| editing        =
| distributor    = Goldwyn Pictures
| released       = August 10, 1919
| runtime        = 50 minutes; 5 reels
| country        = United States
| language       = Silent (English intertitles)
}} western drama film produced and distributed by Goldwyn Pictures and starring Pauline Frederick. Hobart Henley and Victor Schertzinger directed the production. 

==Cast==
*Pauline Frederick - Madge Nelson
*Hardee Kirkland - Nils Olsen
*Corinne Barker - Sophy McGurn
*Lydia Yeamans Titus - Landlady
*Eddie Sturgis - Kid Follansbee
*Thomas Holding - Hugo Ennis

==References==
 

==External links==
 
* 
* 
 

 
 
 
 
 
 
 
 
 


 