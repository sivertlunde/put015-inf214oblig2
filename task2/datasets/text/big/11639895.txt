Zack and Miri Make a Porno
{{Infobox film
| name           = Zack and Miri Make a Porno
| image          = Zack_and_miri_make_a_porno_ver3.jpg
| image_size     = 215px
| alt            = 
| caption        = U.S. theatrical release poster
| director       = Kevin Smith
| producer       = Scott Mosier
| writer         = Kevin Smith Craig Robinson Jason Mewes Traci Lords
| music          = James L. Venable Dave Klein
| editing        = Kevin Smith
| studio         = View Askew Productions MGM
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $24 million 
| gross          = $42,105,111   
}} romantic sex Jersey Girl) not to be set within the View Askewniverse and his first film not set in New Jersey. It was released on October 31, 2008.

==Plot==
Zack Brown and Miri Linky are roommates in Monroeville, Pennsylvania (a Pittsburgh suburb). They have been friends since the first grade. Despite Miri working at the local shopping mall and Zack working at a coffee shop, they have not paid their utility bills in months, with Zack devoting much of his free time to a fanatic following of the Pittsburgh Penguins and his status in the community amateur hockey team, the Monroeville Zombies. After work, their water gets turned off before they go to their high school reunion.
 gay porn star, and Bobbys boyfriend. After returning home from the reunion, the apartments electricity is turned off. Inspired by a successful viral video that was filmed by a pair of teenage boys as Miri changed in Zacks place of work for the reunion (revealing that she wore unattractive underwear, "granny panties"), and emboldened by the cultural mainstreaming of pornographic entertainment, Zack convinces a reluctant Miri that they should make a pornographic film to earn money.

Gathering a group of acquaintances and hired help as the cast and crew, they decide to film a pornographic Star Wars parody, entitled Star Whores. Delaney, the films producer and Zacks co-worker, rents film equipment and a building to use as a studio. When they return to the studio after the first night of filming, the building is being demolished, with all the equipment and costumes inside. They are told that the man that rented it to them had run off with the money. Later at the coffee shop where Zack works, he realizes that his boss threatened to install a hidden camera, which Zack finds, and decides to use it to replace their lost film equipment. Zack retools his film to take place in the coffee shop, revamping the film to one with a coffee shop motif, Swallow My Cockuccino, and the group shoots the film after hours.

Despite their insistence that they would not let sex with each other affect their friendship, Zack and Miri soon develop romantic feelings for each other. When it comes time for Zack and Miri to have sex on camera, they find that instead of the clinical sex enacted by the actors in the other scenes, their interlude is romantic and heartfelt. Later that evening, Zack and Miri are at home when suddenly their apartments electricity and water service return. The rest of the actors and crew show up and reveal that they pooled their resources to pay one month of their bills, and have come over to throw them a party.

At the party, one of the other actresses, Stacey, asks Miri if it is okay for her to ask Zack to have sex. Although Miri has realized that she has developed feelings for Zack, she tells her it is okay to ask him. When Stacey relates this to Zack, the two retreat to Zacks bedroom, much to Miris dismay.

The next evening, Zack is preparing to film a scene between Stacey and another actor, Lester, that was supposed to have been with Lester and Miri. Zack is dismayed when Miri shows up and insists on shooting the scene as originally planned. In the back room, an incredulous Zack asks if she is doing this as a form of retaliation, pointing out that Stacey told him that Miri did not mind her sleeping with Zack. Miri corrects him, clarifying that she did not mind that Stacey merely made the offer to sleep with him. Perceiving this to have been some type of test, Zack admits that during the sex scene they filmed together, they were actually making love and that there was an emotional connection between them, and that he loves Miri. When Miri does not reciprocate, Zack storms out of the coffee shop, quitting the film and his job, and moves out of the apartment.
 Mellon Arena during Pittsburgh Penguins games. Delaney convinces him to come to Delaneys home to see the unfinished film and help complete it. Zack agrees, and as Delaney and the cameraman Deacon explain, Zack learns that Miri never filmed her sex scene with Lester. Zack goes to Miris apartment and reveals to her that he never slept with Stacey; instead, they talked about Miri all night. He pours his heart out to Miri, proclaiming his love for her, which she reciprocates.

In the epilogue, the audience learns that Zack and Miri are married, and with the help of Delaney and his workers compensation settlement, they start their own video production company, Zack and Miri Make Your Porno, which makes amateur videos for couples.

==Cast==
 
* Seth Rogen as Zack Brown
* Elizabeth Banks as Miriam "Miri" Linky Craig Robinson as Delaney
* Jason Mewes as Lester
* Traci Lords as Bubbles
* Jeff Anderson as Deacon
* Katie Morgan as Stacey
* Ricky Mabe as Barry
* Justin Long as Brandon St. Randy
* Brandon Routh as Robert "Bobby" Long
* Tyler Labine as Drunk customer
* Tisha Campbell-Martin as Delaneys wife
* Tom Savini as Jenkins Jennifer Schwalbach as Betsy
* Gerry Bednob as Mr. Surya
* Kenny Hotz as Zack "Zack II" Knutson
 

==Production== Monroeville suburb. 

The female lead role was written for Rosario Dawson, but she was unable to accept the part, as she signed on to film Eagle Eye, whose shooting schedule would have conflicted with Smiths.  Smith wrote the role of Zack, however, with Seth Rogen in mind, based on his performance in The 40-Year-Old Virgin.  Shooting concluded on March 12, 2008. 

There are numerous references to Pittsburgh and the films setting in the neighborhood of Monroeville and Pittsburgh throughout the film, including a drunken   icon Traci Lords (who played Bubbles in the film) is a native of Steubenville, Ohio located about a half hour drive west of Pittsburgh.

==Distribution== MGM was originally set to distribute the film, The Weinstein Company solely distributed the film after a deal between the two companies fell through.   With the announcement came the removal of the MGM logo from the advertising for the film, which is the first Weinstein film to be released after the deal was abruptly ended before the scheduled January 2009 date. 

===Rating===
The Motion Picture Association of America initially gave the film an Motion Picture Association of America film rating system|NC-17 rating for "some graphic sexuality".  Smith submitted two additional cuts of the film with certain footage removed and was told the movie was getting much closer to an "R" rating, but that he should remove a small 14-frame shot first. Smith felt that the scene should stay in so he appealed the rating and the film was viewed by the MPAA again.  Seth Rogen commented "Its a really filthy movie. I hear they are having some problems getting an R rating from an NC-17 rating, which is never good." He continued, complaining that "They fight against sex stuff. Isnt that weird? Its really crazy to me that Hostel (2005 film)|Hostel is fine, with people gouging their eyes out and shit like that... But you cant show two people having sex — thats too much".  On August 5, the rating was successfully appealed to an R with no further cuts.  It attained the rating for "strong crude sexual content including dialogue, graphic nudity and pervasive language".  

On October 10, 2008, the British Board of Film Classification issued an 18 certificate for the film, saying it "contains very strong language and sex references and strong sex." 

===Promotion===
 .  . Entertainment Weekly|EW.com. September 3, 2008. ]] red band trailer of the film was released at IGN. 
 lampoons the films explicit subject matter by featuring stick figures, with the explanation in the posters text this is the only image that can be shown.

Despite this restriction, many media outlets refused to run the poster, or any ad that includes the word "porno" in the title, including a number of newspapers, TV stations, cable channels, and city governments, some of which responded to complaints about the ads at baseball stadiums and city bus stops. Many theaters displayed the films title on their marquee as merely Zack and Miri. Weinstein Company marketing head Gary Faber stated that the ad was accepted in most of the outlets that were offered it, but that the studio would consider variations of the title for outlets that rejected it, including one version of the poster without the title that bears the slogan, "Seth Rogen and Elizabeth Banks made a movie so outrageous that we cant even tell you the title." 

On November 10, 2008, The Weinstein Company announced that it would be re-launching the U.S. ad campaign for the film, with the main focus being a new poster that featured Rogen and Banks in a meadow with animals rendered in the style of childrens animated cartoons. However, the new poster also took a jab at the controversy surrounding the image of the second poster—namely the controversy surrounding the use of the word "porno" in an image so seemingly kid-friendly—by including the statement "A poster for everyone who finds our movie title hard to swallow".   

==Reception==

===Box office===
Zack and Miri Make a Porno "was a box-office flop."   The film opened #2 behind   with $10,682,000 from 2,735 theaters with an average of $3,906.   The "bankable" Rogen  experienced his "worst box-office opening ever".  In an interview with Katla McGlynn of the Huffington Post, Smith himself observed:

 "I was depressed, man. I wanted that movie to do so much better. Im sitting there thinking Thats it, thats it, Im gone, Im out. The movie didnt do well and I killed Seth Rogens career! This dude was on a roll until he got in with the likes of me. Im a career killer! Judds   going to be pissed, the whole Internets going to be pissed because they all like Seth, and the only reason they like me anymore is because I was involved with Seth! And now I fuckin ruined that. It was like high school. I was like, Im a dead man. Ill be the laughing stock."  

During its 13 weeks in release, the film grossed $42.1 million worldwide according to Box Office Mojo.  Following the box office disappointment, Kevin Smiths relationship with producer Harvey Weinstein soured according to an associate of Smiths: "An associate says Smith bitterly blamed Harvey Weinstein for failing to spend enough to market the film. Although Weinstein said he spent $30 million on marketing, Smith didnt believe he had followed through. Either way, the relationship between the two frayed." 

===Critical response===
  
Film review aggregator  , which assigns a normalised rating out of 100 based on reviews from critics, the film has a score of 56 (citing "mixed or average reviews") based on 33 reviews. 
 Michael Phillips John Waters smutty Victorianism of Judd Apatow; according to Scott: 
 " n spite of an avalanche of verbal filth (and a smaller quantum of the visual variety), Zack and Miri is not very shocking at all. He and his characters revel in dialogue that riffs on body parts and bodily fluids, but Mr. Smith’s stories are bathed &ndash; metaphorically! &ndash; in syrup and schmaltz. So Zack and Miri Make a Porno, in spite of its sometimes tiresome, sometimes amusing lewdness, follows a gee-whiz romantic-comedy formula that would not be out of place on the Disney Channel. Two best friends who have always been in love with each other discover that ... they have always been in love with each other. Granted, this revelation occurs while they are having sex in front of a camera, but it is so sweet and predictable that these potentially tawdry circumstances hardly matter." 

Roger Ebert of the Chicago Sun-Times gave the movie   (3 out of a 4 stars) and stated that, "Somehow Kevin Smiths very excesses defuse the material. Hes like the guy at a party who tells dirty jokes so fast, Rodney Dangerfield|Dangerfield-style, that you laugh more at the performance than the material." 

===Smiths views in retrospect===
Both Smith and producer Scott Mosier were disappointed by the films poor box office performance;  according to Smith:
 “That was supposed to be the one that punched us through to the next level. Everyone thought it would do $60 to $70 million, and it wound up doing Kevin Smith business. I was like, ‘I’m done.’ If I were to write at that point in my life, it would be about the poor fat kid whose movie didn’t make enough money."  

For two months after the films theatrical release, Smith didnt work. He even stayed away from the Internet. Smith was "convinced the film would grasp a piece of the raunchy-comedy box-office success that had flowed freely to Judd Apatow the previous year for Knocked Up"; when it didnt, Smith criticized Harvey Weinstein for not spending enough to market the movie, an allegation Weinstein denies, noting he spent $30 million marketing the film.   

More than two years after the film was released, Smith said Zack and Miri is "literally me adulterating my own story...the story of how I made Clerks, with porn." 

==Soundtrack==
 
A song by the band Live (band)|Live, entitled "Hold Me Up", which Smith has said he has been trying to use for over 13 years, appears in an "emotional scene" with Zack and Miri. Smith made a statement about featuring the song in the film:

 

An original song by mc chris called "Miri and Zack" was made for the film. An older song by mc chris, "Fetts Vette", was also used in the film, as well as "Sex and Candy" by Marcy Playground and Jermaine Stewarts 1986 hit "We Dont Have to Take Our Clothes Off." 

==Home media==
Although some copies of the February 2009 "2-Disc Edition" DVD were originally released under its full intended title in the United States,   some DVDs were released under Zack and Miri, the censored title used to originally promote the film. The censored cover features a white background with a photo montage of the principal actors in the film; it includes a series of shorts called Money Shots, as well as other exclusive content; it contains no directors commentary, the first of Smiths films not to include one.  . This version features the censored title.    This version features the original title.  The DVD also features 94 minutes of deleted scenes.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 