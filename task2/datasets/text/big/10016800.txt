Kissa Kursi Ka
{{Infobox film
| name           = Kissa Kursi Ka
| image          = Kissa Kursi Ka film poster.jpg
| image_size     = 
| border         = 
| alt            = Kissa Kursi Ka film poster
| caption        = Kissa Kursi Ka film poster
| film name      = 
| director       = Amrit Nahata
| producer       = Bhagwant Deshpande Vijay Kashmiri Baba Majgavkar
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Shabana Azmi Raj Babbar Utpal Dutt Rehana Sultan Manohar Singh
| music          = Jaidev
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1977 
| runtime        = 142 min.
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1977 Hindi the Emergency period and all prints were confiscated. Music of the film was composed by Jaidev Verma.      

==Plot== personified public, depicted as mute and helpless looking (Shabana Azmi.) The movie is a humorous comment over the system and the selfishness of the politicians regarded as a motion picture version of the cartoonist columns that are the most brutal taunt over the politics.

==Cast==
* Shabana Azmi as Janta
* Utpal Dutt
* Rehana Sultan
* Manohar Singh as President Gangaram Gangu
* Surekha Sikri as Meera Raj Kiran as Gopal
* Chaman Bagga as Deshpal - Presidents Secretary

* Katy Mirza as Ruby Dixsana
* Swapna Sundari as Dancer / Singer

==Ban and print confiscation== Censor Board the Emergency has already been declared. 

Subsequently, all the prints and the master-print of the film at Censor Board office were picked up, later brought to Maruti factory in Gurgaon, where they were burned. The subsequent Shah Commission established  by Government of India in 1977 to inquiry into excesses committed in the Indian Emergency found Sanjay Gandhi guilt of burning the negative, along with V. C. Shukla, Information and Broadcasting minister of the time.      

==Legal case==
The legal case ran for 11 months, and court gave its judgment on February 27, 1979. Both Sanjay Gandhi and Shukla were sentenced to a month and two year jail term imprisonment. Sanjay Gandhi was denied bail. The verdict was later overturned.   In his judgment, District Judge,  O. N. Vohra at Tis Hazari Court in Delhi, found the accused, guilty of "criminal conspiracy, breach of trust, mischief by fire, dishonestly receiving criminal property, concealing stolen property and disappearance of evidence".   

==Bibliography==
*  
*  

== References ==

 

==External links==
*  
*  

 
 
 
 
 
 