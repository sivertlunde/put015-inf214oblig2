Boston Blackie and the Law
{{Infobox film
| name           = Boston Blackie and the Law
| image          = File:Boston Blackie and the Law poster.jpg
| image_size     =
| caption        = Film poster
| director       = D. Ross Lederman
| producer       = Ted Richmond
| writer         = Jack Boyle (character) Harry Essex Malcolm Stuart Boylan (add. dialogue)
| narrator       =
| starring       = Chester Morris Trudy Marshall Constance Dowling
| music          = Hans Sommer
| cinematography = George Meehan James Sweeney
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Boston Blackie and the Law is the twelfth of fourteen Columbia Pictures films starring Chester Morris as reformed crook Boston Blackie.

==Plot== Richard Lane) takes Blackie into custody as an accomplice, but Blackie easily gets away himself.

A trip to the library reveals that Dinah was set to prison for three years a robbery that netted $100,000 (which was never recovered) and a dead victim. Her magician former husband, John Lampau, was acquitted. Blackie tracks Lampau down, still performing magic, but now under the name of Jani, to warn him. Dinah shows up minutes later, having heard that Jani intends to marry his new assistant, Irene (Trudy Marshall). Dinah has come to make sure she gets her half of the loot. In a scuffle, she grazes Janis right hand with a gunshot before fleeing. Blackie arranges to impersonate Jani, while the magician hides in Blackies absent friends apartment.

That night, Blackie is awoken by sounds in Janis apartment. When he investigates, a woman runs out of the unlit room. 

Blackie eventually locates the money in Janis safety deposit box and takes it, still disguised as Jani. Outside, Dinah forces him at gunpoint to give her the envelope containing the loot, but when she opens it, it is empty. Blackie had taken the precaution of pocketing the money. In the meantime, Blackies friend returns home from a trip early and finds Janis body in the closet. Farraday corners and arrests Blackie and his sidekick, "the Runt" (George E. Stone), for murder. Blackie easily escapes from his cell.

Returning to the theatre where Jani performed, he finds an armed Irene over Dinahs lifeless body. She admits she herself was after the money all along. She makes Blackie hand it over, before calling the police. When Farraday and his dimwitted assistant, Sergeant Matthews (Frank Sully), arrive, Blackie tells them he recorded Irenes confession when he turned on the radio for some music. When he plays it for them, Irene tries to run, but is caught and taken away. Blackie then informs Farraday that there was no recording; he merely used ventriloquism to reenact his part of the prior conversation to fool Irene.

==Cast==
* Chester Morris as Boston Blackie
* Trudy Marshall as Irene
* Constance Dowling as Dinah Moran Richard Lane as Inspector Farraday
* George E. Stone as "The Runt"
* Frank Sully as Sergeant Matthews
* Warren Ashe as John Lampau / John Jani

==External links==
*  
*  
*  
*  

 
 
 
 
 
 