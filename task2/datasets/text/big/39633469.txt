Carousel (1923 film)
{{Infobox film
| name           = Carousel 
| image          = File:Karusellen.jpg
| image_size     = 
| caption        = The films original 1923 poster
| director       = Dimitri Buchowetzki 
| producer       = 
| writer         = Alfred Fekete
| narrator       = 
| starring       = Walter Janssen   Aud Egede-Nissen   Alfons Fryland   Jakob Tiedtke
| music          = 
| editing        =
| cinematography = Julius Jaenzon
| studio         = Svensk Filmindustri 
| distributor    = Svensk Filmindustri 
| released       = 1 October 1923
| runtime        = 80 minutes
| country        = Sweden Swedish intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Swedish silent silent drama on location in Sweden and Denmark.

==Cast==
*Walter Janssen as Robert Benton, f.d. cirkusartist 
*Aud Egede-Nissen as Blanche Benton, hans hustru 
*Alfons Fryland as Raymond Duval 
*Jakob Tiedtke as Philippsen, Blanches far 
*Lydia Potechina as Fru Philippsen 
*Ferry Sikla as Lazar, bankir 
*Guido Herzfeld as Cirkusdirektör 
*Rosa Valetti as Cirkusdirektörens fru 
*Waldemar Pottier as Bentons son

==References==
 

==Bibliography==
* Florin, Bo. Transition and Transformation: Victor Sjostrom in Hollywood 1923-1930. Amsterdam University Press, 2012. 

==External links==
* 

 

 

 
 
 
 
 
 
 
 
 
 