Pyar Kiye Jaa
{{Infobox film
| name           = Pyar Kiye Jaa
| image          = Pyaar kiye jaa.JPG
| image_size     =
| caption        = Poster
| director       =C. V. Sridhar 
| producer       =
| writer         =
| narrator       = Mehmood Mumtaz Mumtaz Rajasree Kalpana
| music          =Laxmikant-Pyarelal
| cinematography =
| editing        =
| distributor    =
| released       = 1966
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| preceded_by    =
| followed_by    =
}} 1966 Hindi Kalpana and Telugu as Preminchi Choodu in 1965. Actress Rajasree starred in all the three versions of the film. 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Sun Le Pyar Ki Dushman Duniya"
| Kishore Kumar, Lata Mangeshkar, Manna Dey, Asha Bhosle
|-
| 2
| "O Meri Maina"
| Manna Dey, Usha Mangeshkar
|-
| 3
| "Pyar Kiye Ja"
| Kishore Kumar
|-
| 4
| "Phool Ban Jaunga Shart Yeh Hai"
| Mahendra Kapoor, Lata Mangeshkar
|-
| 5
| "Kisne Pukara Mujhe Main Aa Gai"
| Mahendra Kapoor, Lata Mangeshkar
|-
| 6
| "Gore Hathon Pe Zulm Na Kar"
| Mohammed Rafi
|-
| 7 
| "Dil Humne De Diya"
| Kishore Kumar, Lata Mangeshkar
|-
| 8
| "Kehne Ki Nahin Baat"
| Mohammed Rafi
|-
| 9
| "Din Jawani Ke Chaar"
| Kishore Kumar
|}

==Awards and nominations== Mehmood
*Filmfare Best Comedian Award - Nomination - Om Prakash 

According to film expert Rajesh Subramanian, Mehmood won the Radhakrishan award for best comedian instituted by B R Chopra in honour of yesteryear actor Radhakrishan. Mehmood acknowledged that Om Prakash, who played his father in the film,equally deserved the award and his fabulous reactions made the scenes more entertaining.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 