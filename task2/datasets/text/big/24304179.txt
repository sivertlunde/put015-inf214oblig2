Mix Me a Person
 
{{Infobox film
| name           = Mix Me a Person
| image          = "Mix_Me_A_Person"_(1962).jpg
| caption        = Press campaign book cover Leslie Norman
| producer       = Sergei Nolbandov
| writer         = Ian Dalrymple  Jack Trevor Story Walter Brown  Carole Ann Ford Antony Booth Les Vandyke (aka John Worsley)
| cinematography = Ted Moore
| editing        = Ernest Hosler
| distributor    = British Lion Film Corporation
| released       = August 1962
| runtime        = 116 min.
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British crime crime drama Leslie Norman Walter Brown and Carole Ann Ford. A young London criminal is faced with a hanging for murdering a policeman. With even his defence counsel convinced of his guilt, a female psychiatrist throws herself into proving that the investigating police and legal system have made a mistake. 

==Plot==
Phillip Bellamy, a leading barrister, tells his wife, psychiatrist Anne Dyson, about his most recent case defending a young layabout, Harry Jukes, who has apparently shot a policemen on a country road and been found by police still holding the gun. Bellamy is convinced of his guilt, but Anne is less sure. Much of her practice is with troubled young people and she feels there is more to the story than the police evidence.

Anne visits Harry in prison. He is depressed and distrustful but finally agrees to talk to her. Harrys story is that he took a Bentley Continental car to impress a girl but when she went off with another boy decided to take the car for a spin before dumping it. Swerving to avoid another car he burst a tire but could not find any tools in the boot to change the wheel. He asked the driver of a car parked in the copse nearby for help but he was occupied with his girl and refused. Harry was spotted by a policeman on a bike who stopped to help. He flagged down a lorry to ask to borrow a Jack (device)|jack. The lorry stopped but the passenger immediately produced a gun and shot the policeman. Harry managed to grab the gun off the killer as the lorry drove away. Shortly after, a police car arrived and Harry was arrested.

Anne believes Harrys story and tries to persuade Bellamy of Harrys innocence. She interviews Harry several times and begins to follow up some aspects of his story. She visits the gang that Harry hung out with in a cafe in Battersea and they agree to help her by trying to find the couple in the parked car. She also visits Taplow, the man whose car was stolen, several times and finds his account unconvincing. One of the boys from the cafe agrees to take a job at Taplows frozen food depot to do some investigating there.

Harry is found guilty and the subsequent appeal is dismissed. Anne manages to find more details supporting Harrys story but none of this evidence is accepted by the authorities. The boys from the cafe manage to find the couple and two of them go with Anne to confront them. The woman is ready to co-operate but the man panics and in trying to get away crashes into a tree and is killed. His injured girlfriend makes a statement but it is of no help. On the eve of Harrys execution, Dirty Neck, Harrys friend who has been working at Taplows depot, arrives to tell Anne that something odd is happening at the warehouse. Anne goes there to investigate and is imprisoned in the cold store from which Taplow helps her escape. She contacts the police who have actually been looking into the case again.
 IRA outfit IRA are planning a similar operation but are thwarted by the police. Both Taplow and his comrade Terence end up dead. Harry is released from prison, and returns to join his mates at the Battersea Cafe.

==Cast==
* Anne Baxter - Doctor Anne Dyson
* Donald Sinden - Phillip Bellamy QC
* Adam Faith - Harry Jukes
* David Kernan - Socko Frank Jarvis - Nobby
* Peter Kriss - Dirty Neck
* Carole Ann Ford - Jenny
* Antony Booth - Gravy
* Topsy Jane - Mona
* Jack MacGowran - Terence, the IRA man Walter Brown - Max Taplow
* Glyn Houston - Sam
* Dilys Hamlett - Doris Meredith Edwards - Johnson
* Alfred Burke - Lumley
* Russell Napier - PC Jarrold
* Ed Devereaux - Superintendent Malley
* Ray Barrett - Inspector Wagstaffe
* Nigel Davenport - Jukess Stepfather

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 