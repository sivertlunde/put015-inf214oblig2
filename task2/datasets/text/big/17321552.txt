Dnestrovskiye melodii
{{Infobox Film name = Dnestrovskiye melodii image = Dniestrmelotitle.jpg caption = original film poster producer =  director =  writer =  starring  = Sofia Rotaru music =  distributor = Telefilm-Chișinău released =   1 January 1980   1 January 1973 runtime = 79 min. country = Soviet Union (Moldavian SSR) language = Moldavian
|budget = $,000,000 (estimated) gross = $,000,000 (Worldwide)
}}
 Soviet Moldavian Moldavian musical film starring Sofia Rotaru in the main role, as well as Ion Suruceanu, Nadezhda Chepraga and Maria Cudreanu. The movie symbolizes the propaganda and ideology of the Soviet regime. The movie features songs in Romanian and Russian of Sofia Rotaru and other singers, as well as behind the scenes background voice monologues in Russian between the songs.

==Plot ==

Nadejda Cepraga, Maria Cudreanu, as  well as Ion Suruceanu appear also with soloist performances.

At the time of the movie, Sofia Rotaru, young graduate of the Kishinev Musical Arts Conservatory was a Distinguished Artist of the Ukrainian SSR. The scenic background of the movie attempt to display economic achievement by show-casting new buildings in Chisinău, constructed for the celebration of the 50th Anniversary of the communist Moldavian SSR: Hotel "Intourist", Concert Palace "Octombrie". Sofia Rotaru is known to have forgotten her native language after a long singing career on the Soviet stage. 

==Cast==

Emil Loteanu, Eugen Doga appear in episodes as well. Both Loteanu and Doga have lived and worked for extensive periods of time in Soviet Union and Doga is known to be resident of Moscow for the most part of his life. 

==Production==
Most of the featured buildings, serving as decorations and the main filming scene (hotel "Intourist"), palace "October") were in fact built in the very same 1974, specially for the 50th Anniversary of the Moldavian Autonomous Soviet Socialist Republic.

{| class="wikitable"
! style="background:#efefef;" | N° 
! style="background:#efefef;" | Song
! style="background:#efefef;" | Performed by 
! style="background:#efefef;" | Authors
! style="background:#efefef;" | Commentaries
|-
|----- bgcolor="#f0f0ff" 1
|My White City  ,   Sofia Rotaru
| Lyrics:  Music:   in the opening of the movie, both in Romanian and Russian languages, song, which was the winner of the Golden Orpheus international song festival in Bulgaria
|-
|}

==References==
 

 

 
 
 
 
 