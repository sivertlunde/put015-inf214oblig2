Godzilla vs. Destoroyah
 
{{Infobox film
| name           = Godzilla vs. Destoroyah
| image          = GXD Poster.jpg
| caption        = Japanese theatrical poster
| director       = Takao Okawara
| producer       = Tomoyuki Tanaka Shogo Tomiyama
| writer         = Kazuki Omori
|starring=Takuro Tatsumi Yōko Ishino Yasufumi Hayashi Megumi Odaka Momoko Kochi Kenpachiro Satsuma
| music          = Akira Ifukube
| cinematography =
| editing        = Chizuko Osada
| studio      =  Toho
| distributor    = Toho
| released       =  
| runtime        = 103 minutes
| country        = Japan
| language       = Japanese
| budget         = US $10 million
| gross          = US $18 million
}}
 Godzilla franchise American Godzilla film, which was ultimately produced in 1998. He would begin a new series of Godzilla films in 1999 with the film Godzilla 2000, which began the Millennium series.
 Columbia TriStar Home Video.

==Plot== adopted son, but finds the entire island destroyed. Godzilla appears in Hong Kong, covered in glowing lava-like rashes. He goes on a rampage, causing major collateral damage and killing thousands of civilians. A group of representatives from the Japan Self Defense Forces (JSDF) hires college student Kenichi Yamane, the grandson of Dr. Kyohei Yamane, to work at the center in an attempt to unravel the mystery of Godzillas condition. Yamane suspects that Godzillas heart, which acts as a nuclear reactor, is going through a nuclear meltdown. He theorizes that, when Godzillas temperature reaches 1,200 degrees Celsius, he will explode with a force approximately "1,000 times greater than all nuclear weapons put together, a burst of power unseen since time began."

The JSDF deploys a flying combat vehicle outfitted with anti-nuclear cold weapons, the Super X|Super-X III. Meanwhile, scientists create a new formula for the Oxygen Destroyer that was created by Dr. Serizawa in 1954. The JSDF fears that the Oxygen Destroyer, which was used to kill the original Godzilla, may have disastrous side effects, which is proven when a colony of Precambrian organisms is discovered to have been mutated by the formula. The creatures infest a sewer network and make their way into an aquarium, killing all the sea life inside. They rapidly evolve into monstrous crab-like creatures and begin wreaking havoc. After several deadly skirmishes with the JSDF, the creatures, dubbed "Destoroyah", evolve beyond the militarys containment abilities.

Little Godzilla, who reappears as Godzilla Junior, having mutated further and now closely resembling his father, heads for the island where he was born. Godzilla, searching for his son, follows him, but complications arise. Due to his encounter with the Super-X III, Godzilla will not explode, but will instead suffer a bodily meltdown. After the meltdown, Godzillas superheated remains will bore into the planets core, destroying the Earth. Desperate, the JSDF works to lure Godzilla into a confrontation with the evolving Destoroyah by hiring Miki and another psychic named Meru Ozawa into telepathically instructing Junior to travel to Tokyo, which Destoroyah is currently invading. Godzilla will likely follow and, since Destoroyah was born from the same weapon that destroyed the first Godzilla, he will lose the battle, preventing the meltdown.

The psychics successfully lure Godzilla Junior to Tokyo, where he is attacked by Destoroyah, who has now increased in size and sprouted a pair of bat-like wings. In the ensuing brawl, Destoroyah is seemingly killed after being blown into an electrical plant. By nightfall, Godzilla and Junior meet near Haneda Airport. Their reunion is cut short when Destoroyah, having evolved and now outmatching Godzilla in height, flies in for another attack. Destoroyah knocks down Godzilla and grabs Junior, dropping him from an extreme altitude that brings the young monster close to death. Godzilla, enraged, attacks Destoroyah and a brutal battle erupts. The two creatures inflict serious wounds upon each other. Eventually, Godzilla manages to use his red spiral ray to cause damage to the floral pattern on Destoroyahs chest, bursting it open, and a follow-up blast causes Destoroyah to spew blood from its mouth before dissolving into vapor, which later forms into the aggregate crustacean-like creatures which fought Godzilla Jr.

Godzilla tries to revive his son, but fails. Overcome by grief, Godzillas heart continues to fail, accelerating the meltdown. Destoroyah, having recovered from its previous injuries, appears again. Enraged, Godzilla bombards Destoroyah with a number of supercharged atomic blasts, causing tremendous damage to Destoroyah and destroying one side of Destoroyahs head crests. Overcome, Destoroyah tries to fly away, but the JSDF shoots it down with a number of freeze weapons designed to work against Godzilla. Hitting the ground, Destoroyah disintegrates when the freezing weapons prevent it from reforming by freezing it at a molecular level.

Godzilla begins to die from the meltdown, but the JSDF is able to sustain him momentarily with the freeze weapons. Ultimately, they are unable to rescue Godzilla, and he gives one last weak roar before he melts down. While they succeeded in preventing Earths destruction, the JSDF has been unable to prevent the massive nuclear fallout from rendering Tokyo uninhabitable. Suddenly, the radiation levels plummet and something can be seen stirring in the mist: Godzilla Junior is apparently revived due to his fathers power being channeled into his body and he is now a full-grown adult, ready to take on his fathers title of King of the Monsters.

==Cast==
* Takuro Tatsumi as Dr. Kensaku Ijuin
* Yōko Ishino as Yukari Yamane
* Yasufumi Hayashi as Kenkichi Yamane
* Megumi Odaka as Miki Saegusa
* Sayaka Osawa as Meru Ozawa
* Saburo Shinoda as Professor Fukuzawa
* Akira Nakao as Cmdr. Takaki Aso
* Momoko Kōchi as Emiko Yamane
* Takehiro Murata as Yukaris Editor
* Kenpachiro Satsuma as Godzilla
* Ryo Hariya and Eiichi Yanagida as Destoroyah
* Hurricane Ryu as Godzilla Junior

==Production== Super Nintendo video game Super Godzilla.
 climax of the film. The scene was re-edited to have Destoroyah die after the JSDF intervenes and helps Godzilla finish off Destoroyah, allowing Godzilla to have center stage as he dies.

==English version== international version of the movie, an English title card was superimposed over the Japanese title, as had been done with the previous 1990s Godzilla films.
 Columbia TriStar Home Entertainment released Godzilla vs. SpaceGodzilla and Godzilla vs. Destoroyah on home video on January 19, 1999, the first time that either film had been officially released in the United States. TriStar used the Toho dubs, but cut the end credits and created new titles and opening credits for both films. The complete Toho international version of Godzilla vs. Destoroyah has been broadcast on several premium movie channels since the early 2000s.

==Box office==
The film sold approximately 4 million tickets in Japan, and earned ¥2 billion in distribution income (around US$18,000,000).  It was the number one Japanese film at the box office for 1996. 

==Reception==
Critical reaction to the film was mostly positive. Toho Kingdom said, "With an elegant style, a powerful plot, brilliant effects, and believable acting, this entry is definitely a notch above favorites from all three Godzilla (franchise)#Series history|timelines, and its impact on the series is challenged by only a handful of competitors. Godzilla vs. Destoroyah is without a doubt a paradigm all its own."  Michael Hubert of Monster Zero praised the "spectacular monster battles," calling Godzilla vs. Destoroyah "a great movie" and "one to add to your collection," adding: "Even for non-Godzilla fans, this movie might help dispel some of the preconceptions you have about Godzillas cheese factor." 

Japan Hero called the film "a work of art" and "a must see for anyone who loves Godzilla" that features "something for everyone".
  Mike Bogue of American Kaiju felt the film suffered from "several visual weaknesses" and a "disappointing editing", but that "the positive aspects of the visuals outweigh the negatives" and praised the film for "treating Godzilla with the same awe, majesty, and terror as  . " 

==Home Media release==

Sony - Blu-ray (Toho Godzilla Collection)  
* Released: May 6, 2014
* Picture: 1.85:1   (1080p)
* Sound: Japanese and English (DTS-HD Master Audio 2.0)
* Subtitles: English, English SDH, French
* Extras: 
*Theatrical Trailer (HD, Japanese DD 2.0, English subtitles, 1:37)
*Teaser 1 (HD, Japanese DD 2.0, English subtitles, 0:42) 
*Teaser 2 (HD, Japanese DD 2.0, English subtitles, 1:02) 
* Notes: This is a 2-Disc double feature with Godzilla vs. Megaguirus.

Columbia/Tristar Home Entertainment  
* Released: February 1, 2000
* Aspect Ratio: Widescreen (1.85:1) Anamorphic  
* Sound: English (Dolby Digital 2.0 stereo)
* Subtitles: English, French, and Spanish
* Region 1 (DVD)
* Case type: Keep Case
* Note: A double feature with Godzilla vs. SpaceGodzilla

==Awards== Academy Prize for special effects.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 