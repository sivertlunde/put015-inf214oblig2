Custom Made 10.30
{{Infobox Film
| name = Custom Made 10.30   (カスタムメイド10.30)
| image = CUSTOM MADE 10.30.jpg
| image_size =
| caption = Poster
| director = Hajime Ishimine
| producer = 
| writer = Hajime Ishimine Masako Chiba Ichiro Ogura Ryō Kase Mickey Curtis Yoshiko Miyazaki Shingo Yanagisawwa Taishu Kase
| music = Tamio Okuda
| cinematography = 
| editing = Hajime Ishimine Kinetique Grasshoppa! Films
| distributor = KlockWorx
| released = October 29, 2005
| runtime = 121 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}
 Hiroshima Municipal Stadium for the 10th anniversary of his solo performance.  

==Plot== Japanese cabaret-club. After her mother remarries and moves to Yamaguchi Prefecture, Manamo lives by herself in Hiroshima.

One day, her younger sister Minamo returns from London to visit Manamo. It is ten years since they have lived together, and they argue every day.

==Cast==
* Kaela Kimura as "Manamo Kobayashi", an 18-year old, 3rd-year student of Hiroshima Minami high school, lives in Yokogawa, Nishi-ku, Hiroshima, dreams to be a singer, vocal of "Five Dimension Customs".
* Erika Nishikado as "Minamo Kobayashi", Manamos younger sister, 15-year-old, 2nd-year student of Royal Academy of Music, comes back to Hiroshima from London.
* Tamio Okuda as " Tamio Okuda", 39-year old singer, himself.
* Hajime Anzai as "Takuji", Manamo and Minamos father, lives in London.
* Carolyn Kawasaki as "Risa", Manamo and Minamos mother, lives in Yamaguchi.
* Ayaka Maeda as "Asako", bass player of "Five Dimension Customs",
* Ryoko Matsui as "Hime", drum player and a leader of "Five Dimension Customs", an expectant mother.
* Kenichi Matsuyama as "Tamotsu", a shop boy of cabaret-club "Shin Getsu".
* Susumu Terajima as the master of cabaret-club "Shin Getsu" Masako Chiba as "VIP Yoko" of cabaret-club "Shin Getsu".
* Ichiro Ogura as a probationer angel "Eddy".
* Ryō Kase as probationer angel "Jeff".
* Mickey Curtis as a god.
* Yoshiko Miyazaki as the owner of clothing store "Takeda" in Yokogawa, Hiroshima.
* Shingo Yanagisawwa as "Horikoshi", a frequenter of cabaret-club "Shin Getsu".
* Taishu Kase as a teacher.

==DVDs==
* The DVD was released on March 24, 2006.
* A documentary program of the film by Television Kanagawa, titled "Custom Made 10.30: Angel Works", was released on October 14, 2005.
* The original concert DVD "Hitori Matatabi Special @ Hiroshima Municipal Stadium" was released on February 23, 2005.

==Soundtrack==
* A soundtrack CD was released on October 31, 2005, containing 21 songs from the film.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 