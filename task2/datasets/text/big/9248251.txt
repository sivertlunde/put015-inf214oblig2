Such Is Life (1939 film)
{{Infobox film
| name           = Así es la vida
| image          = Asieslavida poster.jpg
| caption        = Poster
| director       = Francisco Múgica
| producer       =
| writer         = Nicolás de las Llanderas (play) Arnaldo Malfatti
| starring       = Enrique Muiño
| music          = Enrique Delfino
| cinematography = José María Beltrán
| editing        = Juan Soffici
| distributor    = Lumiton
| released       = August 5, 1939 (U.S.)
| runtime        = 105 minutes
| country        = Argentina
| language       = Spanish
| budget         =
| followed_by    =
}} 1939 Argentina|Argentine musical directed by Francisco Múgica based on a play by Nicolás de las Llanderas and Arnaldo Malfatti; starring Enrique Muiño.

==Plot summary==
A wealthy family of six with three daughters and a son grow together learning from and experiencing all the lessons of life. With firm and loving parents all four children find love and although not all marry right away, they are happy. Even when one parent dies, the household carries on as strong as ever. Así es la vida is a story of family strength and close personal relationships.

 

===Cast===
*Enrique Muiño
*Elías Alippi
*Enrique Serrano
*Arturo García Buhr
*Sabina Olmos
*Alberto Bello
*Myrna Bonillas
*Fernando Campos
*Héctor Coire
*Niní Gambier
*Alfredo Jordan
*Felisa Mary
*Alímedes Nelson
*José Ruzzo
*Pablo Vicuña
 

==External links==
*  
 
 
 
 
 
 
 
 
 
 
 

 