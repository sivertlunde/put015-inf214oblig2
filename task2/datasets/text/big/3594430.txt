Take Care of My Cat
{{Infobox film
| name        = Take Care of My Cat
| image       = Take Care of My Cat movie poster.jpg
| caption     = Theatrical poster
| film name   = {{Film name
 | hangul     = 고양이를 부탁해
 | rr         = Goyangireul Butakhae
 | mr         = Koyangirŭl Put‘akhae}}
| director    = Jeong Jae-eun
| producer    = Oh Ki-min
| writer      = Jeong Jae-eun
| starring    = Bae Doona Lee Yo-won Ok Ji-young
| cinematography = Choi Young-hwan
| editing     = Lee Hyun-mi
| music       = Park Gi-hyeon   Kim Jun-seok   Jo Seong-woo   Kim Sang-hyeon
| distributor = Cinema Service
| released    =  
| runtime     = 112 minutes
| country     = South Korea
| language    = Korean
| budget      =
| gross       =   
}} a 2001 South Korean coming of age coming-of-age film|film, the feature debut of director Jeong Jae-eun.  It chronicles the lives of a group of friends — five young women — a year after they graduate from high school, showing the heartbreaking changes and inspiring difficulties they face in both their friendships and the working world.

==Plot==
Five girlfriends in their early twenties live in the dingy port town of Incheon. A close-knit circle in high school, their paths begin to diverge as they step into the adult world.

At the center of the group is the beautiful and vain Hae-joo, who dreams of becoming a successful career woman. She leaves Incheon for an apartment in Seoul and a junior position with a brokerage firm.

The other girls are left behind in a state of solitude and unease; Tae-hee works for free for her parents and takes dictation from a poet suffering from cerebral palsy and Ji-young seeks a job, while caring for her grandparents in their dilapidated apartment. The twins Bi-ryu and Ohn-jo buffer themselves from change with constant togetherness.
	
The cellular phones ring as the girls coordinate their meetings. A lost cat, Tee tee, enters the lives of these young women, passing from one owner to the next as circumstances pull lives and friends apart and others together.

==Cast==
*Bae Doona as Yoo Tae-hee  
*Lee Yo-won as Shin Hae-joo
*Ok Ji-young as Seo Ji-young
*Lee Eun-shil as Bi-ryu
*Lee Eun-jo as Ohn-jo

==Reception==
Though critically acclaimed in its native South Korea, the films box office returns were not so great, prompting a Save the Cat movement involving film industry professionals and Incheon residents to try to increase viewership before its theatrical run would be cut short.  A campaign was also launched for a theater re-run. 

Local filmmakers organized a festival to support the survival of films that hold fast to artistic significance and compromise commercial success (in the process come and go without much recognition). The title of the event, WaRaNaGo, came from the initial syllables of four 2001 movies － Waikiki Brothers, Raybang, Nabi (film)|Nabi ("Butterfly") and Goyangireul Butakhae ("Take Care of My Cat") － which all fared poorly in the box office. 
 NETPAC Award FIPRESCI Prize at the Hong Kong International Film Festival, the Best Picture award ("Golden Moon of Valencia") at the Cinema Jove Valencia International Film Festival, a KNF Award Special Mention in the competition section of the International Film Festival Rotterdam, among others. It was invited to the Young Forum section at the Berlin International Film Festival and was also theatrically released in Japan, Hong Kong, U.K and U.S.A. 

==Awards==
;2001 Busan International Film Festival 
* NETPAC Award
* New Currents Award - Special Mention

;2001 Chunsa Film Art Awards
* Best Actress - Bae Doona, Lee Yo-won, Ok Ji-young
* Best Planning/Producer - Oh Ki-min
* Special Jury Prize - Jeong Jae-eun

;2001 Blue Dragon Film Awards
* Best New Actress - Lee Yo-won

;2001 Directors Cut Awards
* Best New Director - Jeong Jae-eun
* Best New Actress - Lee Yo-won
* Best Producer - Oh Ki-min

;2002 Baeksang Arts Awards
* Best Actress - Bae Doona
* Best New Actress - Lee Yo-won

;2002 Busan Film Critics Awards
* Best Actress - Bae Doona

;2002 Korean Film Awards
* Best New Director - Jeong Jae-eun

;2002 Hong Kong International Film Festival
* FIPRESCI Prize - Special Mention

;2002 International Film Festival Rotterdam
* KNF Award - Special Mention 

;2002 Cinema Jove Valencia International Film Festival
* Golden Moon of Valencia (Best Film)

==References==
 

== External links ==
*   at Kino International
*  
*  
*  

 
 
 
 
 
 
 
 