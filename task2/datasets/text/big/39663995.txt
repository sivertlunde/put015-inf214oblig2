Sun (film)
{{Infobox film
| name           = Sun
| image          = 
| image_size     = 
| caption        = 
| director       = Alessandro Blasetti 
| producer       = 
| writer         = Aldo Vergano   Alessandro Blasetti
| narrator       = 
| starring       = Marcello Spada   Vasco Creti   Dria Paola   Vittorio Vaser
| music          = 
| editing        = Alessandro Blasetti    Giorgio Orsini
| studio         = Augustus Film 
| distributor    = 
| released       = 16 June 1929
| runtime        = 76 minutes
| country        = Italy
| language       = Silent   Italian intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Italian silent silent drama Fascist government. on location, which added a sense of realism (arts)|realism. Mussolini was impressed by the result and described it as "the dawn of the Fascist film". 

The film was destroyed during the Second World War, and survives only in still photographs. 

==Cast==
* Marcello Spada as Ing. Rinaldi 
* Vasco Creti as Marco 
* Dria Paola as Giovanna 
* Vittorio Vaser as Silvestro 
* Lia Bosco as Barbara 
* Anna Vinci 
* Rolando Costantino  
* Rinaldo Rinaldi   
* Arcangelo Aversa  
* Arnaldo Baldaccini  
* Sante Bonaldo  
* Vittorio Gonzi   
* Igino Nunzio

==References==
 

==Bibliography==
* Bondanella, Peter E. Italian Cinema: From Neorealism to the Present. Continuum International Publishing Group, 2001.
* Reich, Jacqueline & Garofalo, Piero. Re-Viewing Fascism: Italian Cinema, 1922 to 1943. Indiana University Press, 2002.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 