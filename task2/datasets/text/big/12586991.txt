Telstar: The Joe Meek Story
 
 
{{Infobox film
| name           = Telstar: The Joe Meek Story
| image          = Telstarposter.jpg
| caption        = UK cinema poster
| director       = Nick Moran
| producer       = David Reid Adam Bohling Simon Jordan
| writer         = James Hicks, Nick Moran Con ONeill Tom Burke Ralf Little Sid Mitchell
| music          = Ilan Eshkeri, with songs by Joe Meek and others
| cinematography = Peter Wignall
| editing        = Alex Marsh
| studio         = Aspiration Films
| distributor    = G2 Pictures
| runtime        = 119 minutes
| released       =  
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} West End Con ONeill, who also played Joe Meek in the original play, while Kevin Spacey plays Meeks business partner, Major Wilfred Banks.

==Plot== producer behind illegal in the UK at the time, and his struggles with debt, paranoia and major depressive disorder|depression, which culminated in the killing of his landlady Violet Shenton and himself, on 3 February 1967.

==Cast (in credits order)==
  Con ONeill as Joe Meek
*Kevin Spacey as Major Wilfred Banks
*Pam Ferris as Mrs Violet Shenton
*JJ Feild as Heinz Burt
*James Corden as Clem Cattini Tom Burke as Geoff Goddard
*Ralf Little as Chas Hodges
*Sid Mitchell as Patrick Pink (aka Robbie Duke)
*Mathew Baynton as Ritchie Blackmore
*Shaun Evans as Billy Kuy
*Callum Dixon as John Leyton Tom Harper as Alan Caddy Jon Lee as Billy Fury
*Nigel Harman as Jess Conrad
*Carl Barât as Gene Vincent
*Justin Hawkins as Screaming Lord Sutch
*Nick Moran as Alex Meek
*Jess Conrad as Larry Parnes
*Clem Cattini as Chauffeur
*Chas Hodges as Mr Brolin
*John Leyton as Sir Edward
*Robbie Duke as Stagehand
*Mike Sarne as Backstage Manager
*David Hayler as John Peel
*Craig Vye as Mitch Mitchell
*Joan Hodges as Biddy Meek
*Jimmy Carr as Gentleman Ken Howard
*Marcus Brigstocke as Alan Blaikley
*Rita Tushingham as Essex Medium
*Jack Roth as Youth
*Guy Lewis as Charles Blackwell (musician) George Bellamy Roger LaVern
 

==Casting== Tornados drummer Clem Cattini appears in a scene as John Leytons chauffeur and provided advice on set design. Leyton himself plays the fictional "Sir Edward", and singer-actor Jess Conrad plays pop manager Larry Parnes. Meeks young protégée Patrick Pink (now known as Robbie Duke) appears as a stagehand.

==Criticism== Heinz Burt has also criticized the film for portraying him as Meeks lover, claiming that Heinz Burt did not have a close relationship with Meek, and was also not a homosexual as portrayed in the film. 

==Critical reception==
A columnist from the Daily Mail said that the film was a "shambolic but entertaining biopic". While enjoyable, it was "not much good and definitely not sophisticated".  Siobhan Synnot of the Scotland on Sunday praised the film because it did not employ the usual "cinematic gloss". She opined that it begins with a humorous tone but transforms into a "harrowing film", adding that Telstar "knocks the wind out of the sails" of The Boat That Rocked in that the performances are "more substantial and engaged". Synnot concluded that "like Meeks records, Telstar is raw, fatalistic and somewhat crudely put together, but it also boasts both-barrels, mega-watt energy." 

==See also==
*A Life in the Death of Joe Meek - feature-length documentary from 2008

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 