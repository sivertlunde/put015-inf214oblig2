Adhikar (1986 film)
{{Infobox film
| name           =  Adhikar
| image          = 
| caption        = 
| director       = Vijay Sadanah
| producer       = Jawaharlal Bafna Vasant Joshi Bindu Sulochana Latkar Raza Murad Paintal Tanuja Danny Denzongpa Vikas Anand Lucky Yunus Parvez
| music          = Bappi Lahiri
| screenplay     = Kulwant Jani
| screenplay     = Vijay Kaul
| released       =  
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
}}
Adhikar is a 1986 film starring Rajesh Khanna in the lead opposite Tina Munim. This film was the last film of the pair of Rajesh Khanna and Tina Munim.  The film had box office collection of 9.25 crores in 1986.

In 1992, Ram Awatar Agnihotri wrote that it was in the films Alag Alag and Adhikar, both opposite Rajesh Khanna that Tina Munim showed the "first sparks" of the dedicated actress she would become. Tina regards her performance in this film as her best performance in her career. 

==Plot==
Vishal and Jyoti both spent their childhood in different manner from each other but they fall in love, and in spite of stiff oppositions from their parents they get married. After marriage, they have misunderstandings with each other frequently. These problems persist even when Jyoti gives birth to healthy baby boy, Lucky (Master Lucky). Vishal is a jockey by profession and rides a horse since years and thereby wins many races. Due to failing health, Vishal has been advised by his doctor not to ride anymore and so Vishal stops riding the horse but continues to own the same horse. Meanwhile Jyoti also leaves Vishal due to the constant fights between them and  Lucky grows up and lives with his dad. He is brought up to think that his mom has died. So when Jyoti re-enters his life, she is introduced to him as his "aunty". Will Lucky ever come to know of his real mother? Will Vishal ever to able to ride again? Will Jyoti and Vishal patch up their differences in the interest of their son? these questions form the rest of the story.

==Cast==
*Rajesh Khanna
*Tina Munim
*Zarina Wahab Bindu
*Sulochana Latkar
*Danny Denzongpa
*Raza Murad
*Tanuja
*Paintal

==Music==
* "Main Dil Tu Dhadkan" - Kishore Kumar
* "Shuru Shuru Pyar Ki Kahani" - Kishore Kumar, Chandrani Mukherjee
* "Jane Wale Na Dil Se Jate" - Lata Mangeshkar
* "Main Dil Tu Dhadkan" - Kavita Krishnamurthy
* "Jeene Ka Sahara Milgayai" - Uttara Kelkar
* "Laher Laher Chanchal Huyee" - Anupama Deshpande

==References==
 

== External links ==
*  

 
 
 


 