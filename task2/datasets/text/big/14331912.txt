Adolphe (film)
{{Infobox film
| name           = Adolphe (film)
| image          = Adolphe_affiche.jpg
| image_size=
| caption        = 
| director       = Benoît Jacquot
| producer       = Laurent Pétin Michèle Pétin
| writer         = Benoît Jacquot Fabrice Roger-Lacan
| starring       = Isabelle Adjani Stanislas Merhar
| music          = Éric Serra
| cinematography = Benoît Delhomme
| editing        = Luc Barnier
| distributor    = Alliance Atlantis Communications US  ARP Sélection France 2002
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 2002 film based on the novel Adolphe by Benjamin Constant.  The film was directed by Benoît Jacquot and starred Isabelle Adjani as Ellénore and Stanislas Merhar as Adolphe.

== Cast ==
* Isabelle Adjani - Ellénore
* Stanislas Merhar - Adolphe
* Jean Yanne - Count
* Romain Duris - DErfeuil
* Jean-Louis Richard - Mr. dArbigny
* Anne Suarez -  Mrs. dArbigny
* Jacqueline Jehanneuf - Aunt Choupie

== External links ==
*  

 

 
 
 
 

 