Harry Potter and the Chamber of Secrets (film)
{{Infobox film
| name           = Harry Potter and the Chamber of Secrets
| image          = Harry Potter and the Chamber of Secrets movie.jpg
| caption        = British poster Chris Columbus
| producer       = David Heyman
| screenplay     = Steve Kloves
| based on       =   See below)  
| music          = John Williams  Roger Pratt
| editing        = Peter Honess
| studio         = Heyday Films 1492 Pictures Warner Bros. Pictures
| released       =  
| runtime        = 161 minutes
| country        = United Kingdom United States 
| language       = English
| budget         = $100 million
| gross          = $879 million 
}} Chris Columbus Warner Bros. novel of Harry Potters Salazar Slytherin Chamber of petrifies the schools denizens. 
 Harry Potter Harry Potter and the Prisoner of Azkaban.
 34th highest-grossing film of all time.    and the seventh highest-grossing film in the Harry Potter series. It was nominated for three BAFTA Film Awards in 2003.

==Plot==
 
 Harry Potter has spent all summer without receiving any letters from his friends at Hogwarts. Going to his room while the Dursleys are hosting a dinner party, Harry meets Magical creatures (Harry Potter)#Dobby|Dobby, a house-elf who warns him bad things will happen if he returns to Hogwarts, and reveals he intercepted his friends letters. Harry chases him downstairs, where Dobby destroys a cake. The Dursleys imprison Harry in his bedroom, but Ron Weasley|Ron, Fred and George Weasley rescue him in their fathers flying Ford Anglia, taking him to their home, the Places in Harry Potter#The Burrow|Burrow. 

Later, Harry and the Weasley family go buy school supplies in Diagon Alley, where they encounter Rubeus Hagrid and Hermione Granger and go to a book signing by celebrity wizard Gilderoy Lockhart who announces he will be the new Defence Against the Dark Arts teacher at Hogwarts. Harry also encounters Draco Malfoy and his arrogant father Lucius Malfoy|Lucius, who slips a book in Ginny Weasleys belongings. The Weasleys depart to board the Hogwarts Express but Harry and Ron are blocked from entering Platform Nine and Three-Quarters so they use the Ford Anglia to fly to Hogwarts, where they crash into the hostile Whomping Willow. Rons wand is damaged, and the car throws them out before driving off into the Forbidden Forest. They are allowed back into school but face detention.
  	  	 Professor McGonagall explains that one of Hogwarts founders Salazar Slytherin supposedly hid a monster which only his heir can control in a secret Chamber to purge the school of impure-blooded wizards and witches. Harry suspects Malfoy is the Heir, so Hermione suggests using a Polyjuice Potion to disguise themselves as Malfoys friends, Crabbe and Goyle, and ask him directly. Their makeshift laboratory is in abandoned toilets haunted by Moaning Myrtle. A camera-wielding first-year pupil, Colin Creevey is found petrified shortly after.
 Professor Dumbledore, Cornelius Fudge, and Lucius Malfoy arrive to take Hagrid to Azkaban, but he discreetly tells the boys to "follow the spiders". Lucius also has Dumbledore suspended. Harry and Ron venture into the Forbidden Forest, finding a colony of Acromantula led by Aragog who backs Hagrids innocence and reveals that the girl who died when the Chamber was opened before was found in a bathroom. The spiders then attack Harry and Ron but they are rescued by the now wild Ford Anglia. 

They find a library book page in Hermiones hand that reveals the monster is a Basilisk, a giant snake that can instantly kill those who make direct eye contact with it, and realise that all the victims so far were only petrified because they saw its reflection or looked at it indirectly. Hermione has also discovered that the Basilisk has been using the water pipes to get around the school. The school staff learn Ginny was taken into the Chamber, and convince Lockhart to save her. Harry and Ron find Lockhart, revealed as a fraud, planning to flee, and they drag him to Myrtles bathroom as they have figured out that she was the girl killed by the Basilisk. Finding the Chambers entrance hidden under the sink, the three go in, but Lockhart tries to inflict a memory charm on the boys using Rons still damaged wand which backfires, wiping his memories, and causing a cave-in. 
 phoenix Magical Fawkes flies in with the Sorting Hat and Riddle summons the Basilisk to kill Harry. Fawkes blinds the Basilisk and the Sorting Hat produces a sword with which Harry battles and slays the Basilisk before being poisoned by its fangs.
	 Godric Gryffindors own sword. Harry accuses Lucius Malfoy, now revealed to be Dobbys master, of putting the diary in Ginnys cauldron and tricks him into freeing Dobby by giving him a sock hidden in the diary. The Basilisks victims are healed, and Hagrid returns to school to the applause of the students and staff.

==Cast==
Main article: List of Harry Potter films cast members Harry Potter
* Rupert Grint as Ron Weasley, Harrys best friend.
* Emma Watson as Hermione Granger, Harrys other best friend and the trios brains.

* Kenneth Branagh as Gilderoy Lockhart, a celebrity author and Hogwarts new Defense Against the Dark Arts teacher. Hugh Grant is said to have been the first choice for the role but due to reported scheduling conflicts he was unable to play the character.  On 25 October 2001, Branagh was selected as Grants replacement. 
* John Cleese as Nearly Headless Nick, the ghost of Gryffindor House.
* Robbie Coltrane as Rubeus Hagrid, Hogwarts Groundskeeper.
* Christian Coulson as Tom Riddle, a student of Hogwarts, in fact a young Lord Voldemort.
* Warwick Davis as Filius Flitwick, the Charms Master and head of Ravenclaw House.
* Richard Griffiths as Vernon Dursley, Harrys Muggle uncle.
* Richard Harris as Albus Dumbledore, the Hogwarts headmaster and one of the greatest wizards of the age. Harris died shortly before the film was released.
* Tom Felton as Draco Malfoy, Harrys arrogant and conceited school adversary.
* Jason Isaacs as Lucius Malfoy, Dracos father.
* Alan Rickman as Severus Snape, the Potions Master and head of Slytherin House.
* Fiona Shaw as Petunia Dursley, Harrys Muggle aunt.
* Maggie Smith as Minerva McGonagall, the Transfiguration teacher and head of Gryffindor House.
* Julie Walters as Molly Weasley, the Weasley matriarch and a mother figure to Harry.
* Robert Hardy as Cornelius Fudge, the Minister of Magic.
* Shirley Henderson as Moaning Myrtle, a Hogwarts student who was killed by the Basilisk in the past.
* Mark Williams as Arthur Weasley, Rons father, a Ministry of Magic employee fascinated with Muggles.
* Bonnie Wright as Ginny Weasley, Arthur and Molly Weasleys daughter, and Rons sister.

==Production==
===Set design===
  used in the film]]
Production designer Stuart Craig returned for the sequel to design new elements previously not seen in the first film. These included the Burrow, Dumbledores office (which houses the Sorting Hat, The Sword of Gryffindor and Dumbledores desk),  Borgin and Burkes, and the Chamber of Secrets.

Mr. Weasleys car was created from a Ford Anglia. 

===Filming=== on location Christopher Columbus opted to handheld cameras for Chamber of Secrets to allow more freedom in movement. 

===Sound design===
Due to the events that take place in Harry Potter and the Chamber of Secrets, the films sound effects were much more expansive than in the previous instalment. Sound designer and co-supervising sound editor Randy Thom returned for the sequel using Pro Tools to complete the job, which included initial conceptions done at Skywalker Sound in California and primary work done at Shepperton Studios in England. 

===Music===
 
  Minority Report Catch Me William Ross was brought in to arrange themes from the Philosophers Stone into the new material that Williams was composing whenever he had the chance.  The soundtrack was released on 12 November 2002.

==Distribution==

===Marketing=== video game based on the film was released in early November 2002 by Electronic Arts for several consoles, including Nintendo GameCube|GameCube, PlayStation 2, and Xbox (console)|Xbox.  The film also continued the merchandising success set by its predecessor, with the reports of shortages on Legos Chamber of Secrets tie-ins. 

===Theatrical release===
The film premiered in the UK on 3 November 2002 and in the United States and Canada on 14 November 2002 before its wide release on 15 November, one year after the Philosophers Stone.

===Home media===
The film was originally released in the UK, US and Canada on 11 April 2003 on both VHS tape and in a two-disc special edition DVD digipack, which included extended and deleted scenes and interviews.  On 11 December 2007, the films Blu-ray Disc|Blu-ray  version was released. An Ultimate Edition of the film was released on 8 December 2009, featuring new footage, TV spots, an extended version of the film with deleted scenes edited in, and a feature-length special Creating the World of Harry Potter Part 2: Characters.  The films extended version has a running time of about 174 minutes, which has previously been shown during certain television airings. 

==Reaction==

===Box office=== Harry Potter and the Philosophers Stone.  It was also No. 1 at the box office for two non-consecutive weekends.  In the United Kingdom, the film broke all opening records that were previously held by The Philosophers Stone. It made £18.9&nbsp;million during its opening including previews and £10.9&nbsp;million excluding previews.  It went on to make £54.8&nbsp;million in the UK; at the time, the fifth biggest tally of all time in the region. 

The film made a total of $879&nbsp;million worldwide,  which made it the fifth highest-grossing film ever at the time.  It was 2002s second highest-grossing film worldwide behind    and the fourth highest-grossing film in the US and Canada that year with $262 million behind Spider-Man, The Lord of the Rings: The Two Towers, and  .  However, it was the years number one film at the non-American box office, making $617&nbsp;million compared to The Two Towers $584.5&nbsp;million. 

===Critical reception===
The films reviews were very positive and it currently holds an 83% "Certified Fresh" approval rating at   praised the directing and the films faithfulness to the book, saying: "Chris Columbus, the director, does a real wonderful job of being faithful to the story but also taking it into a cinematic era".  Variety (magazine)|Variety also said the film was excessively long, but praised it for being darker and more dramatic, saying that its confidence and intermittent flair to give it a life of its own apart from the books was something The Philosophers Stone never achieved.  A. O. Scott from The New York Times said: "instead of feeling stirred you may feel battered and worn down, but not, in the end, too terribly disappointed". 

Peter Travers from   called the film a cliché which is "deja vu all over again, its likely that whatever you thought of the first production – pro or con – youll likely think of this one". 

===Accolades=== BAFTA Awards. These were for Best Production Design, Sound, and Achievement in Visual Effects.  The film was also nominated for six Saturn Awards in 2003 and in 2004 for its DVD release. 

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 