Doctor (film)
{{Infobox film
| name = Doctor
| image =
| caption =
| director = M. S. Mani
| producer = HH Ebrahim
| writer = Vaikkom Chandrasekharan Nair
| screenplay = Vaikkom Chandrasekharan Nair Sathyan Sheela Thikkurissi Sukumaran Nair T. S. Muthaiah
| music = G. Devarajan
| cinematography = U Rajagopal
| editing = MS Mani
| studio = Kalalaya
| distributor = Kalalaya
| released =  
| country = India Malayalam
}}
 1963 Cinema Indian Malayalam Malayalam film, National Film Awards.

==Cast==
   Sathyan 
*Sheela 
*Thikkurissi Sukumaran Nair 
*T. S. Muthaiah 
*Adoor Pankajam 
*JAR Anand 
*Kottayam Chellappan 
*Nellikode Bhaskaran 
*O Madhavan 
*S. P. Pillai 
*K. V. Shanthi 
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by P. Bhaskaran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ennaane Ninnaane || K. J. Yesudas, P. Leela, Chorus || P. Bhaskaran || 
|- 
| 2 || Kalpanayaakum || K. J. Yesudas, P Susheela || P. Bhaskaran || 
|-  Mehboob || P. Bhaskaran || 
|- 
| 4 || Kinaavinte Kuzhimaadathil || P Susheela || P. Bhaskaran || 
|- 
| 5 || Ponnin Chilanka   || P. Leela || P. Bhaskaran || 
|- 
| 6 || Ponnin Chilanka   || P. Leela || P. Bhaskaran || 
|-  Mehboob || P. Bhaskaran || 
|- 
| 8 || Varanondu Varanondu || K. J. Yesudas, P Susheela || P. Bhaskaran || 
|- 
| 9 || Viralonnu Muttiyaal || P. Leela || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 

 
 
 


 