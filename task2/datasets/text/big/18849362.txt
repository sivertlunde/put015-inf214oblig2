Thaai Magalukku Kattiya Thaali
{{Infobox film
| name = Thaai Magalukku Kattiya Thaali தாய் மகளுக்கு கட்டிய தாலி
| image = 
| director = R. R. Chandiran
| writer = Rama Arangannal
| story = C. N. Annadurai Jamuna Rajasulochana Pasupuleti Kannamba |P. Kannamba K. A. Thangavelu O. A. K. Thevar M. G. Chakrapani
| producer = R. R. Chandiran
| music = T. R. Papa
| cinematography = R. R. Chandiran
| editing = P. V. Manickam
| studio = Kalpana Kala Mandir
| distributor = Kalpana Kala Mandir
| released = 31 December 1959 
| runtime = 160 mins
| country =   India Tamil
| budget = 
}}
 Jamuna and Pasupuleti Kannamba |P. Kannamba in the lead roles and directed by R. R. Chandiran who also was a well known cinematographer in his time.

==Plot==
Sundharam Mudhaliyar M. G. Chakrapani is a wealthy man living with his daughter Kaveri (Rajasulochana) and Sahadevan lives with his wife Thanabakkiam (P. Kannamba) and daughter Sundhari (Jamuna (actress)|Jamuna). During a dance competition at the school function, Sundhari wins the first prize and Sundharam could not accept this as Sahadevans family belongs to an inferior caste and indebted money from him. Sundharam magnifies this simple misunderstandings which results the death of Sahadevan. In order to cover this up, Sundharam disposes the body into the river and cooks up a story that Sahadevan had run away to avoid paying his debts. Sahadevan survives but suffers from amnesia. Sundharam grabs over Sahadevans house and land as a compensation for his debts and Thanabakkiam and Sundhari had to leave to a smaller house in another village. There Thanabakkiam saves the life of Kanagasundharam or Kanagu (M. G. Ramachandran) from drowning in the river and thanked by Kanagus father Loganayagam (D. Balasubramaniam) of the assistance rendered despite the difference in their caste. Kanagu and Sundhari becomes close friends and grow up to be lovers.

Loganayagam who would like to renovate the school requests Kanadu to seek donation from Sundharam. Kanagu and his friend Dhuraiappan (K. A. Thangavelu) leaves to Sundharams place when they bump into an arrogant and modern girl Kaveri. When Kaveri goes back she is shocked to learn that Kanagu is her relative and falls for him. Meanwhile the villagers and relatives of Kanagu tries to drive Kannamba and Sundhari in order to separate Kanagu and Sundhari from getting married but they fail to do so when Kanagu intervenes. Thanabakkiam gets to know about Kanagu and Sundharis relationship and tries to advise Kanagu but Kanagu promises to marry Sundhari. The villagers and relatives of Kanagu complain to Loganayagam about this but Loganayagam agrees to get them married. Matters gets worse when this news spreads to the whole village which paints Kanagu as a womaniser. Fortunately Loganayagam does not believe this and sends for Thanabakkiam to arrange their childrens wedding.

Things become serious when Sundharam wishes to get Kaveri married to Kanagu and warns Loganayagam that should that not happen, Loganayagam needs to setlle Rs 2 lakhs that he is indebted to Sundaram. Upon knowing this, Kanagu and Sundhari registers their wedding without the knowledge of anybody, only to be witnessed by Dhuraiappan. The villagers come to a conclusion to disown Logunayagams family and no workers would work for them. Kanagu ploughs the soil and works in the fields himself. Logu unable to bear all this requests Thanabakkiam to accept compensation and to leave that village. Thanabakkiam gets even furious when she learns that Sundhari is pregnant and Sundhari ties to commit suicide by jumping into the well but saved by Thanabakkiam. When Loganayagam learns about the wedding registration, he apologises to Kanagu and requests Kanagu to inform this to Thanabakkiam but Thanabakkiams house is engulfed by fire. All thought that Thanabakkiam and Sundhari had peished in the fire but actually both had left the village.

In another town, a rich landlord Arumainayagam (R. Balasubramaniam) is looking for his long lost younger brother in order to surrender all his wealth. Sundharam learns that Loganayagam is the person but hatches a plan to get Kaveri to be married to Kanagu in order to swindle all the wealth. Sundharam threatens by using Loganayagams signature to prepare a letter saying that Thanabakkiams house was burnt by his order. Knowing this, Kanagu pretends to agree marrying Kaveri but Loganayagam who is not in favour of this becomes sick and dies on the wedding day. Thanabakkiam ties a thali to Sundhari and advices that thali is important to a married lady even though wedding registration took place.

Kaveri soon gets to know about Sundharams deeds. Kanagu donates all his land to the villagers and turns his house to a school and the villagers apologises on the guilt of defaming Kanagu before. Kaveri too donates her jewellery to the needy. Sundharam opposes to this but Kanagu settles Loganayagams debt to Sundharam. Kanagu requests the villagers to be in the registration office tomorrow to transfer his land to their names and to stop this Sundharam accuses of Kanagu is mentally unstable and admits Kanagu to a mental sanatorium without anyones knowledge.

Ezhumalai (Kaka Radhakrishnan) who works for Arumainayagam takes in Thanabakkiam and Kaveri as workers in the plantations. Balu (O. A. K. Thevar) is the manager of Arumainayagam, lusts for Sundhari and disturbs her a lot. Sundhari deliveres a baby boy and at the same time Kaveri is very sad to learn about this and as Kanagu goes missing without letting her know. Sundharams requests Balu to join as his sidekick in order to murder Arumainayagam, but Balu with the alliance of Gopal (V. Gopalakrishnan) who works in a circus, decides to have the Arumainayagams wealth for themselves. A series of event occurs before the climax where Kaveri learns about Sundharams plan to murder Arumainayagam, Balu and Gopal abducts Sundhari, Sahadevan who sees Thanabakkiam remembers his past and Kanagu escapes from the mental sanatorium. While Kaveri was driving speedily to save Arumainayagam, Kanagu saves a baby boy from being hit by the car on the road unwiittingly knowing that the baby boy was his son. Kaveri steers away from the road and hit a tree and her last wish is that Kanagu to save Arumainayagam and do not hand over Sundharam to the police and dies.

Balu and Gopal forcefully obtain the signature of Arumainayagam and leaves the job of murdering him to Sundharam. Kanagu appears on the scene and saves Arumainayagam. Balu and Gopal escapes. Sundharam came to know about Kaveris death, repents and informs Arumainayagam that Kanagu is none other than the latters nephew and stabs himself. Dhuraiappan meets Kanagu and tells about Sundharis abduction where Kanagu is shocked to know that Sundhari is alive. Kanagu and Dhuraiappan saves Sundhari and polices arrests Balu and Gopal. Arumainayagam and Kanagu divides their wealth to the workers.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || Kanagu
|- Jamuna || Sundhari
|-
| Rajasulochana || Kaveri
|-
| P. Kannamba || Thanabakkiam
|-
| K. A. Thangavelu || Duraiappan
|-
| O. A. K. Thevar || Balu
|-
| E. R. Sahadevan || Sahadevan
|-
| M. G. Chakrapani || Sundharam Mudhaliyar
|-
| Kaka Radhakrishnan || Ezhumalai
|-
| R. Balasubramaniam || Arumainayagam
|-
| D. Balasubramaniam || Loganayagam
|-
| V. Gopalakrishnan || Gopal (John)
|}

==Crew==
*Producer: R. R. Chandiran
*Production Company: Kalpana Kala Mandir
*Director: R. R. Chandiran
*Music: T. R. Papa
*Lyrics: Udumalai Narayana Kavi, Kannadasan, A. Maruthakasi, K. D. Santhanam
*Story: C. N. Annadurai
*Dialogues: Rama Arangannal
*Art Direction: C. Raghavan
*Editing: P. V. Manickam
*Choreography: P. S. Gopalakrishnan, Chinni Sampath, R. Thangaraj
*Cinematography: R. R. Chandiran, S. Rajamani
*Stunt: R. N. Nambiar
*Songs Recording: E. I. Jeeva
* Re-Recording: A. Venkatachalam
*Dance: None

==Soundtrack== Playback singers are T. M. Soundararajan, Seerkazhi Govindarajan, Jikki, P. Suseela, S. Janaki, A. Nithyakala & L. R. Eswari. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Lyrics || Length (m:ss)
|-
| 1 || Aadivarum Aadagap Porpaavai|| Seerkazhi Govindarajan & S. Janaki ||  || 04:42
|-
| 2 || Chinnanchiru Vayadhu Mudhal || T. M. Soundararajan & Jikki ||  || 02:59
|-
| 3 || Kodumaiyada Kodumaiyada || Seerkazhi Govindarajan ||  || 03:29
|-
| 4 || Nervazhiye Nadanthirundhaal || T. M. Soundararajan & A. Nithyakala ||  || 01:54
|-
| 5 || Ondralla Irandalla Thambi || Seerkazhi Govindarajan ||  || 03:13
|-
| 6 || Parandhu Parandhu || Jikki & A. Nithyakala ||  || 02:42
|-
| 7 || Sengarumbu Chaatrinile Then Kalandhu || T. M. Soundararajan & A. Nithyakala ||  || 5:20
|-
| 8 || Thanjavooru Bommaiye || Seerkazhi Govindarajan & L. R. Eswari ||  || 01:52
|-
| 9 || Vambukkizhukkira.... Theriyadhaa Solla Theriyadhaa || T. M. Soundararajan & P. Suseela ||  || 03:36
|-
| 10 || Thogai Inge Megam Ange || Jikki ||  || 03:29
|-
| 11 || Ulladhai Sonna Paitthiyamunnu || T. M. Soundararajan ||  || 03:33
|-
| 12 || Vandhen Vandhaname || Seerkazhi Govindarajan ||  ||
|}

==Reception==
Thaai Magalukku Kattiya Thaali, in spite of Anna’s reformist story and the cast (MGR, Kannamba and others), the film did not do well and people remember it mainly for its puzzling title. M. G. Chakrapani as the villain was excellent. Unfortunately, though he acted in many movies, he didn’t hit the top spot like his charismatic younger brother MGR.  The film was ran for 80 days and flop at box-office. 

==References==
 

==External links==
 

 
 
 
 
 