The Pride of the Yankees
{{Infobox film
| name           = The Pride of the Yankees: The Life of Lou Gehrig
| image          = The Pride Of The Yankees 1942.jpg
| caption        = Theatrical release poster
| director       = Sam Wood
| producer       = Samuel Goldwyn
| story          = Paul Gallico
| screenplay     = Jo Swerling Herman J. Mankiewicz
| starring       = Gary Cooper Teresa Wright Walter Brennan Babe Ruth Dan Duryea
| music          = Leigh Harline
| cinematography = Rudolph Maté Dan Mandell
| studio         = Samuel Goldwyn Productions
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 128 minutes
| country        = United States
| language       = English
| budget         =
}} American film directed by Sam Wood and starring Gary Cooper, Teresa Wright, and Walter Brennan.  It is a tribute to the legendary New York Yankees first baseman Lou Gehrig, who died only one year before its release, at age 37, from amyotrophic lateral sclerosis, which later became known to the lay public as "Lou Gehrigs disease".

Though subtitled "The Life of Lou Gehrig", the film is less a sports biography than an homage to a heroic and widely loved sports figure whose tragic and premature death touched the entire nation. It emphasizes Gehrigs relationship with his parents (particularly his strong-willed mother), his friendships with players and journalists, and his storybook romance with the woman who became his "companion for life," Eleanor. Details of his baseball career—which were still fresh in most fans minds in 1942—are limited to montages of ballparks, pennants, and Cooper swinging bats and running bases, though Gehrigs best-known major league record—2,130 consecutive games played—is prominently cited.
 Yankee Stadium. 100 greatest movie quotes.   

==Synopsis==
Lou Gehrig (Cooper) is a young Columbia University student whose old-fashioned mother (Elsa Janssen) wants him to study hard and become an engineer. But the young man has a gift for baseball. A sportswriter (Brennan) befriends Gehrig and persuades a scout to come see him play. Before long Gehrig signs with the team he has always revered, the New York Yankees. With the help of his father (Ludwig Stössel), he endeavors to keep his career change a secret from his mother.

Gehrig works his way up through the minor leagues and joins the Yankees. His hero, Babe Ruth, is at first condescending and dismissive of the rookie, but his strong, consistent play wins over Ruth and the rest of the team. Gehrig is soon joining teammates in playing pranks on Ruth on the team train.

During a game at Comiskey Park, Gehrig trips over a stack of bats and is teased by a spectator, Eleanor (Wright), who laughingly calls him "tanglefoot". They are properly introduced later, a relationship grows, and soon Lou proposes marriage. Gehrigs mother, who still hasnt accepted the fact that her son will not be an engineer, does not take the news of his pending marriage well; Lou finally stands up to her, and marries Ellie.

The Yankees become the most dominant team in baseball, and Gehrig becomes a fan favorite. His father and fully converted mother attend games and cheer for him. In a re-creation of a famous (and possibly apocryphal) story, Gehrig visits a crippled boy named Billy (Gene Collins) in a hospital. He promises to hit two home runs in a single World Series game in the boys honor—then fulfills his promise.
 Joe McCarthy Harry Harvey) that he has become a detriment to the team and benches himself. After an examination, a doctor gives him the awful news: Gehrig has a rare, incurable disease, and only a short time to live.
 Lou Gehrig Day at Yankee Stadium, an older Billy (David Holt) finds Gehrig and shows him that he has made a full recovery, inspired by his heros example and the two-homer fulfilled promise. Then, as Eleanor weeps softly in the stands, Gehrig addresses the fans: "People all say that Ive had a bad break. But today ... today, I consider myself the luckiest man on the face of the Earth."

==Cast== AFI database )
 
* Gary Cooper - Lou Gehrig
* Teresa Wright - Eleanor Gehrig
* Babe Ruth - Himself
* Walter Brennan - Sam Blake
* Dan Duryea - Hank Hanneman
* Elsa Janssen - Mom Gehrig
* Ludwig Stossel - Pop Gehrig
* Virginia Gilmore - Myra
* Bill Dickey - Himself Ernie Adams - Miller Huggins
* Pierre Watkin - Mr. Twitchell Harry Harvey - Joe McCarthy Robert W. Meusel - Himself
* Mark Koenig - Himself
* Bill Stern - Himself
* Addison Richards - Coach
* Hardie Albright - Van Tuyl
* Edward Fielding - Clinic doctor
* George Lessey - Mayor of New Rochelle
* Edgar Barrier - Hospital doctor
* Douglas Croft - Lou Gehrig as a boy
* Gene Collins - Billy, age 8 David Holt - Billy, age 17

 

==Release== Astor Theatre, animated short called "How to Play Baseball," produced by Walt Disney Animation Studios at Samuel Goldwyns request.   

==Reception==
Variety (magazine)|Variety magazine called the film a "stirring epitaph" and a "sentimental, romantic saga ... well worth seeing."  

Time (magazine)|Time magazine said the film was a "grade-A love story" done with "taste and distinction" though it was "somewhat overlong, repetitive, undramatic.  Baseball fans who hope to see much baseball played in Pride of the Yankees will be disappointed. Babe Ruth is there, playing himself with fidelity and considerable humor; so are Yankees Bill Dickey, Bob Meusel, Mark Koenig. But baseball is only incidental. The hero does not hit a home run and win the girl. He is just a hardworking, unassuming, highly talented professional. The picture tells the model story of his model life in the special world of professional ballplayers." 

Bosley Crowther of The New York Times called it a "tender, meticulous and explicitly narrative film" that "inclines to monotony" because of its length and devotion to "genial details." 

==Awards and other recognition== Academy Award for his work on The Pride of the Yankees.   The film received ten additional Oscar nominations:      

* Best Actor in a Leading Role (Cooper)
* Best Actress in a Leading Role (Wright)
* Best Art Direction-Interior Decoration, Black-and-White
* Best Cinematography, Black-and-White Jack Cosgrove, Ray Binger, Thomas T. Moulton)
* Best Music, Scoring of a Dramatic or Comedy Picture
* Best Picture
* Best Sound, Recording (Thomas T. Moulton)
* Best Writing, Original Story
* Best Writing, Adapted Screenplay
 100 most inspiring films in American cinema.

In AFIs 2008 "Ten Top Tens"—the top ten films in ten "classic" American film genres—The Pride of the Yankees was ranked third in the sports category.    Gehrig was named AFIs 100 Years...100 Heroes & Villains | the 25th greatest hero in American cinema by the AFI in 2003.

==Inaccuracies/artistic license== School of Journalism.    
 flopped the print of the film.  Tom Shieber, a curator at the National Baseball Hall of Fame, has shown, however, that Cooper did indeed learn to bat left-handed, and never wore a backwards Yankees uniform nor ran to third base after swinging.    Film footage was, in fact, flopped only once, during a brief sequence portraying Gehrigs minor league days at Hartford, in order to make Cooper appear to be throwing left-handed — a far more difficult task for a right-hander to master.  Scenes requiring Cooper to throw a ball as a Yankee were filmed using his stand-in, the left-handed Babe Herman. Shieber, Tom (February 3, 2013). The Pride of the Yankees/Seeknay.  . Retrieved February 5, 2013 

In one of the films more memorable scenes, a physician matter-of-factly informs Gehrig of his tragic diagnosis, dismal prognosis, and brief life expectancy. In fact, Mayo Clinic doctors painted an unrealistically optimistic picture of Gehrigs condition and prospects, reportedly at his wifes request.   Among other things he was given "a 50–50 chance of keeping me as I am" for the foreseeable future, and was told that he "...may need a cane in 10 or 15 years." Deliberate concealment of bad news from patients, particularly when cancer or an incurable degenerative disease was involved, was a relatively common practice at the time. 
 Wrigley Field Los Angeles Angels of the old Pacific Coast League, and a popular venue for baseball movies of the era, as well as the Home Run Derby (TV series)|Home Run Derby television series. 

===Gehrigs farewell speech===
There is no known intact film of Gehrigs actual speech at Yankee Stadium on July 4, 1939; a small portion of the newsreel footage, incorporating his first and last remarks, is all that survives.   For the movie, the speech was not reproduced verbatim; the script condensed and reorganized Gehrigs actual spontaneous and unprepared remarks, and moved the iconic "luckiest man" line from the beginning to the end for heightened dramatic effect.  Gehrigs message, however, remained essentially unchanged.

{| class="wikitable"
|-
! Yankee Stadium Speech
|-
| "Fans, for the past two weeks you have been reading about a bad break.  Yet today I consider myself the luckiest man on the face of the Earth. I have been in ballparks for seventeen years and have never received anything but kindness and encouragement from you fans.

"Look at these grand men. Which of you wouldnt consider it the highlight of his career just to associate with them for even one day? Sure, Im lucky. Who wouldnt consider it an honor to have known Jacob Ruppert?  Also, the builder of baseballs greatest empire, Ed Barrow? To have spent six years with that wonderful little fellow, Miller Huggins? Then to have spent the next nine years with that outstanding leader, that smart student of psychology, the best manager in baseball today, Joe McCarthy? Sure, Im lucky.

"When the New York Giants, a team you would give your right arm to beat, and vice versa, sends you a gift - thats something. When everybody down to the groundskeepers and those boys in white coats remember you with trophies &mdash; thats something. When you have a wonderful mother-in-law who takes sides with you in squabbles with her own daughter &mdash; thats something. When you have a father and a mother who work all their lives so you can have an education and build your body &mdash; its a blessing. When you have a wife who has been a tower of strength and shown more courage than you dreamed existed - thats the finest I know.

"So I close in saying that I might have been given a bad break, but Ive got an awful lot to live for."
|-
!Film Speech
|-
| "I have been walking onto ball fields for sixteen years, and Ive never received anything but kindness and encouragement from you fans. I have had the great honor to have played with these great veteran ballplayers on my left - Murderers Row, our championship team of 1927. I have had the further honor of living with and playing with these men on my right - the Bronx Bombers, the Yankees of today.

"I have been given fame and undeserved praise by the boys up there behind the wire in the press box, my friends, the sportswriters. I have worked under the two greatest managers of all time, Miller Huggins and Joe McCarthy.

"I have a mother and father who fought to give me health and a solid background in my youth. I have a wife, a companion for life, who has shown me more courage than I ever knew.

"People all say that Ive had a bad break. But today ... today, I consider myself the luckiest man on the face of the Earth."
|-
|}

==Adaptations to other media==
The Pride of the Yankees was adapted as an hour-long radio play on the October 4, 1943 broadcast of Lux Radio Theater with Gary Cooper and Virginia Bruce and a September 30, 1949 broadcast of Screen Directors Playhouse starring Gary Cooper and Lurene Tuttle.

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 