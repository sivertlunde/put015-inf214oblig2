She's Out of My League
{{Infobox film
| name           = Shes Out of My League
| image          = Outofmyleague.jpg
| caption        = Official poster
| director       = Jim Field Smith
| producer       = {{Plain list |
* Jimmy Miller
* David Householter
}}
| writer         = {{Plain list |
* Sean Anders
* John Morris
}}
| starring       = {{Plain list |
* Jay Baruchel
* Alice Eve
* Mike Vogel
* T.J. Miller
* Nate Torrence
* Krysten Ritter
* Geoff Stults
* Lindsay Sloane
}} Michael Andrews
| cinematography = Jim Deanult
| editing        = Dan Schalk
| studio         =  
| distributor    = Paramount Pictures
| released       =  
| runtime        = 104 minutes 
| country        = United States
| language       = English
| budget         = $20 million 
| gross          = $49 million 
}}
Shes Out of My League is a 2010 American romantic comedy film directed by Jim Field Smith and written by Sean Anders and John Morris. The film stars Jay Baruchel and Alice Eve, and was produced by Jimmy Miller and David Householter for Paramount Pictures and DreamWorks and filmed in Pittsburgh, Pennsylvania. Production on the film finished in 2008. The film received its wide theatrical release on March 12, 2010. The film is director Jim Field Smiths first feature.

==Plot== TSA officer employed at the Pittsburgh International Airport along with his friends, fellow TSA officer Stainer (T. J. Miller), airline reservations agent Devon (Nate Torrence), and baggage handler Jack (Mike Vogel). Kirk has a poor track record with dating and is hoping to reconcile with his self-centered ex-girlfriend, Marnie (Lindsay Sloane), who despite having broken up with him two years earlier, and having since found a new boyfriend Ron (Hayes MacArthur), has remained close with Kirks parents (Debra Jo Rupp and Adam LeFevre), brother Dylan (Kyle Bornheimer), and pregnant sister-in-law-to-be Debbie (Jessica St. Clair).

At work one morning, an attractive young woman, Molly McCleish (Alice Eve), arrives at the passenger terminal to board a flight to New York City. While proceeding through the TSA security checkpoint, Mollys striking looks attract unwanted attention from several male TSA officers who try flirting with her awkwardly. Kirk is the only TSA officer to treat Molly courteously. On the airplane, she realizes that she accidentally left her cellphone in the airport security area. Calling up her phone, Kirk answers and arranges a time to meet the following evening so that Kirk can return it.

When Devon and Kirk arrive at the Andy Warhol Museum, where Molly, a lawyer-turned-event planner, is managing an event, Kirk collides with Mollys sister, Katie (Kim Shaw) and spills his drink on the museum director. Kirk takes the blame for the incident to protect Katie, after which a grateful Molly offers Kirk tickets to a Pittsburgh Penguins hockey game at the Mellon Arena. When Kirk and Stainer meet Molly and her best friend Patty (Krysten Ritter), who develops an immediate mutual loathing with Stainer (he subsequently refers to her as "The McDonaldland#Characters|Hamburglar"), at the game, Kirk, still convinced Molly is not interested in him, assumes Molly meant to set him up with Patty, until Patty explicitly tells him of Mollys interest.

The two begin to date after this, with Kirk confiding in her his dream of becoming a pilot someday, though Stainer predicts their relationship will fail as he deems Molly a "10" in a scale of attractiveness, and Kirk only a "5", telling him a girl he loved once broke up with him for this very reason. Patty, for her part, believes Molly had only chosen Kirk because he was a "safe" choice after being hurt by her last boyfriend, Air Force pilot Cam (Geoff Stults), who assumes Kirk is a waiter and attempts to order drinks from Kirk when they first meet.

Molly then invites herself to Kirks family lunch, where she charms his family and even Ron after highly intimidating the men of the house with her looks. Mollys attentions to Kirk stir jealousy in Marnie, who feels upstaged by Mollys attractiveness, and takes a sudden interest in Kirk again.

After returning to Mollys apartment, Kirk ejaculates prematurely in his pants when things start to heat up, just as Mollys parents (played by Alice Eves real-life parents, Sharon Maughan and Trevor Eve) arrive for a surprise visit. Desperate to conceal the stain on his pants, Kirk seems discourteous by avoiding to stand up and shake hands, and quickly leaves Mollys apartment. Molly grows cool to Kirk after this, believing he fled to avoid meeting her parents.  At Jacks urging, Kirk admits the true reasons for his leaving, and their relationship resumes.

During a date, Kirk suggests to Molly that she throw a birthday party for Katie (with music provided by Stainers Hall & Oates tribute band, "Adult Education"). Kirk is troubled, when Molly is intentionally vague about Kirks line of work to her parents. To add to his troubles, Mollys macho ex-boyfriend Cam shows up and messes with Kirk by deliberately alluding to Molly having some sort of "defect".

After the party, both of them go back to Mollys apartment and make out where Kirk discovers Mollys "defect" is slightly webbed toes, which Kirk considers so minor that he decides that she is indeed too perfect for him. Molly is upset that Kirk is so insecure that he felt he could only be with her if something was wrong with her. After telling Kirk that she and Cam had broken up because of his own insecurities, with him even cheating on her, she admits she had indeed asked Kirk out because she considered him safe and breaks up with him.

Kirk leaves and later resumes his relationship with Marnie, planning on a family trip to Branson, Missouri|Branson.

Stainer and Patty realize their mistake in telling Kirk and Molly it wouldnt work out; Stainer tells Kirk that he is a "10" too. They pull Kirk off his plane as the aircraft prepares to depart to Branson as he tries to leave with his family and Marnie, while Patty brings Molly to the airport. Kirk rejects Marnie during an unorthodox airport pursuit and meets Molly in the airport where she tells him that he is out of shape and she asked him out because she thought he was safe and wouldnt hurt her. She then continues to tell him that she doesnt care what he is employed as and that she misses him and wants to be together with him. Kirk and Molly then make up and resume their relationship, even if their friends dont approve it.

Later, as a surprise, Kirk is seen walking on the airport Tarmac with Molly where he  takes Molly on a trip in a small plane, with him as the pilot. The couple are last seen happily together in a small plane taking off from Pittsburgh airport.

==Cast==
 
* Jay Baruchel as Kirk Kettner
* Alice Eve as Molly McCleish
* Krysten Ritter as Patty
* T. J. Miller as Wendell a.k.a. Stainer
* Nate Torrence as Devon
* Mike Vogel as Jack
* Lindsay Sloane as Marnie
* Kim Shaw as Katie McCleish
* Jasika Nicole as Wendy
* Debra Jo Rupp as Mrs. Kettner
* Adam LeFevre as Mr. Kettner
* Kyle Bornheimer as Dylan Kettner
* Hayes MacArthur as Ron
* Geoff Stults as Cam Armstrong
* Jessica St. Clair as Debbie
* Trevor Eve as Mr. McCleish
* Sharon Maughan as Mrs. McCleish
 

==Production== Mount Washington, Market Square, Century III Mall, and area sound stages were also used as locations for the film.  Plumas Restaurant in Irwin, Pennsylvania was used for bar scenes when shooting at Mellon Arena became impossible due to the Penguins advancement to the 2008 Stanley Cup Finals.  The film was co-produced by Jimmy Miller of Mosaic Media Group, a native of Castle Shannon, Pennsylvania and the brother of comedian Dennis Miller. 

==Reception==

===Critical response===
The film has received mixed to positive reviews from critics. Review aggregator Metacritic gives it a score of 47 based on reviews from 28 critics,  and a 58% score on Rotten Tomatoes, based on 121 reviews.  The consensus Rotten Tomatoes gives is, "She’s Out of My League has moments of humor and insight, but it’s bogged down by excessive vulgarity and cartoonishness." 

Critic Roger Ebert gave the film three stars out of four saying, "The movie is not a comedy classic. But in a genre where so many movies struggle to lift themselves from zero to one, its about, oh, a six point five."
Peter Travers of Rolling Stone gave the film three stars out of four, commenting, "This R-rated blend of the sweet and the raunchy has its heart in the right place."   Jake Tomlinson of Shave Magazine gave the movie four and a half stars out of five and praised the movie "for not throwing in cheap obstacles" and for the "good soundtrack." {{cite web
| first = Jake | last = Tomlinson
| title = Movie Review: Shes out of my League
| url = http://www.shavemagazine.com/entertainment/reviews/100301
| work = Shave Magazine
| accessdate = 2010-03-12
}}
 
 
Michael OSullivan of The Washington Post was less enthused, giving the film one star out of four: "The movie clearly aspires to rise to the smutty-but-sweet synergy of other, better films. But Shes Out of My League cant touch them." 

===Box office performance=== Alice in Green Zone, with an estimated $9.6 million gross.    As of September 10, 2010, it has grossed nearly $50 million worldwide. 

==Home media==
Shes Out of My League was released on DVD and Blu-ray Disc on June 22, 2010. Early reviews have been mediocre. The majority of complaints lie in the sparse special features and the predictability of the film. {{cite web
| url=http://thefilmstage.com/2010/06/17/blu-review-shes-out-of-my-league-2
| title=  She’s Out Of My League
| work=TheFilmStage.com
| accessdate=2010-06-17
| date=2010-06-17
}}  As of September 10, 2010, it has grossed $7.7 million in US DVD sales. {{cite web
| url=http://www.the-numbers.com/movies/2010/SHOML-DVD.php
| title=Shes Out of My League – DVD Sales
| publisher=The Numbers
| accessdate=2010-09-12
}} 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 