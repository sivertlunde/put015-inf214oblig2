Diamonds (1999 film)
 
{{Infobox Film
| name           = Diamonds
| image          = 
| image_size     = 
| caption        = 
| director       = John Mallory Asher
| producer       = Gerald Green Rainer Bienger Andrew Somper Patricia Green Hannah Hempstead
| writer         = Allan Aaron Katz
| narrator       = 
| starring       = Kirk Douglas Dan Aykroyd Corbin Allred Lauren Bacall Kurt Fuller Jenny McCarthy June Chadwick Lee Tergesen Roy Conrad John Landis Joyce Bulifant
| music          = Joel Goldsmith Paul Elliott
| editing        = Carroll Timothy OMeara David L. Bertman
| distributor    = Miramax Films
| released       = September 6, 1999
| runtime        = 91 mins.
| country        = 
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Diamonds is a 1999 comedy film directed by John Mallory Asher and written by Allan Aaron Katz. The film stars Kirk Douglas, Dan Aykroyd, Lauren Bacall, Jenny McCarthy, and Corbin Allred.

==Plot==
An elderly man and his estranged son search for treasure and try to repair their relationship in this bittersweet comedy. Harry Agensky (Kirk Douglas) is a one-time welterweight boxing champion who lives in Canada with his son Moses (Kurt Fuller). Harrys other son, Lance (Dan Aykroyd), feels that his father never really cared about his dreams and ambitions, and now Lance has little affection for his Dad. However, Lances relationship with his teenage son Michael (Corbin Allred) is not faring much better. Lonely since the death of his wife and infirm due to a stroke, Harry wants to retire to a ranch in Northern Canada, but he cant afford the property. Lance invites Harry along for a skiing trip with Michael; Harry agrees, but at the last minute he talks them into going to Nevada instead. Harry claims he threw a fight years ago and was paid off in a cache of diamonds that he hid somewhere in Vegas; if he can find the gems, hell be able to buy the ranch. Lance is dubious, but he gives in to Harrys determination and the three head for Nevada, hoping to find both the diamonds. On the way there, the men visit a local brothel run by madame Sin-Dee (Lauren Bacall), when Harry convinces the group, so that he can have sex for the first time in eight years.18-year-old grandson Michael gets his Dad to let him join so that he can lose his virginity. Following their quest for the hidden diamonds, both father and son learn a lesson about reconciliation and the price of growing older. 
 Young Man with a Horn in 1950.

According to the closing credits, the film was shot on location in Reno, Nevada.

==Cast==
*Kirk Douglas as Harry Agensky
*Dan Aykroyd as Lance Agensky
*Corbin Allred as Michael Agensky
*Lauren Bacall as Sin-Dee
*Kurt Fuller as Moses Agensky
*Jenny McCarthy as Sugar
*Mariah OBrien as Tiffany
*June Chadwick as Roseanne Agensky
*Lee Tergesen as Border Guard Val Bisiglio as Tarzan
*Allan Aaron Katz as Mugger
*Roy Conrad as Pit Boss
*John Landis as Gambler
*Joyce Bulifant as June
*Liz Gandara as Roxanne

==External links==
* 

 
 
 
 
 


 