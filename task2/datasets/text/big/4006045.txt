The Ghoul (1933 film)
{{Infobox film
| name           = The Ghoul
| image          = The-Ghoul-Poster.jpg
| image size     = 
| caption        =
| director       = T. Hayes Hunter
| producer       = Michael Balcon
| writer         = Dr. Frank King (play) Rupert Downing Leonard Hines Roland Pertwee John Hastings Turner
| starring       = Boris Karloff Cedric Hardwicke Ernest Thesiger Ralph Richardson
| music          = Louis Levy Leighton Lucas
| cinematography = Günther Krampf
| art direction  = Alfred Junge
| editing        = Ian Dalrymple Ralph Kemplen
| studio         = Gaumont British
| distributor    = Woolf & Freedman Film Service
| released       = August 1933 (UK) January 1934 (US)
| runtime        = 77 minutes
| country        = United Kingdom
| language       = English
| budget         = just under ₤40,000 Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 133, 141 
| preceded by    =
| followed by    =
}}
The Ghoul (1933) is a British horror film starring Boris Karloff, Cedric Hardwicke, Ernest Thesiger, and Ralph Richardson, making his film debut.

==Plot==
Gaumont British borrowed just the vaguest outline from the 1928 source novel by Frank King (and subsequent play by King and Leonard J. Hines). Kings novel is sub-par Edgar Wallace in which a master criminal popularly referred to as The Ghoul has been responsible for a London crime wave. Betty inherits an estate on the Yorkshire moors from a mysterious benefactor, Edward Morlant, a dabbler in mysticism who years before had been her mothers paramour. But the will requires Betty to take up residence in the old house, where Morlants corpse soon appears, walking and talking. Morlant tells her that he is an immortal adept and demands the return of his secret diary. The usual suspects and interlopers converge on the house, and upon Morlants next appearance his resurrected self is killed anew, unquestionably stabbed through the heart. Morlant is soon perambulating again, as people begin turning up dead. All supernatural trappings are dispelled as The Ghoul is penultimately unmasked as Edward Morlants twin brother, James, a criminal mastermind whose fictive guises included not only his brother, but a bogus police sergeant and his brothers solicitor, Broughton. In a final act of madness, James torches the mansion.
 The Mummy eternal life. Morlant appears to die, but the jewel is snatched by his servant before the interment. No sooner do the heirs arrive for the reading of the will, than Morlant rises from his tomb, finds his bauble gone, and attempts to punish the thieves. The jewel is punted from servant to lawyer to niece to Egyptian fanatic to spinster to mock vicar and eventually back to the revenant Morlant, who makes his blood sacrifice to Anubis before properly expiring. Morlant, it is learned, had merely suffered a cataleptic seizure, and had been buried alive. The mock vicar (Ralph Richardson) is revealed to be the chief villain, and having obtained the Eternal Light sets fire to Morlants tomb. Betty and her lover manage to escape.

==Cast==
* Boris Karloff as Prof. Morlant
* Cedric Hardwicke as Broughton
* Ernest Thesiger as Laing
* Dorothy Hyson as Betty Harlon
* Anthony Bushell as Ralph Morlant
* Kathleen Harrison as Kaney
* Harold Huth as Aga Ben Dragore
* D. A. Clarke-Smith as Mahmoud
* Ralph Richardson as Nigel Hartley
* Jack Raine as Davis, the chauffeur (uncredited)
* George Relph as Doctor (uncredited)

==Release and preservation==
The Ghoul was released in the UK in August 1933, in the US in January 1934, and reissued in 1938. The film was popular in the UK but performed disappointingly in the US. 
 Lon Chaney The Monster, Bela Lugosi in The Gorilla and Boris Karloff in The Ghoul. Subsequently, The Museum of Modern Art and Janus Film made an archival negative of that scruffy Prague print and it went into very limited commercial distribution. 
 Bootleg videotapes of this broadcast filtered among collectors for years, but when an official VHS release arrived from MGM/UA Home Video, it was the virtually unwatchable Czech copy.  Audiences were grateful to simply see a major lost Karloff film in the 1970s and 1980s, but the film was disappointing in its battered condition. Finally, in 2003, just as the title was prepared for DVD, MGM/UA obtained the superior material for release. The restored copy has substantially raised critical appreciation of the film in modern times.

==Later version== What a Carve Up! (No Place Like Homicide in the US), was a full blown comedy that owed even less to Kings story than the earlier film.

==See also==
* Boris Karloff filmography
* List of rediscovered films
==References==
 

==External links==
* 
*  
* 
* 

 
 
 

 
 
 
 
 
 


 