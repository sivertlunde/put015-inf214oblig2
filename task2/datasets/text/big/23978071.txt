"FF.SS." – Cioè: "...che mi hai portato a fare sopra a Posillipo se non mi vuoi più bene?"
 
{{Infobox film
| name = "FF.SS." - Cioè: "...che mi hai portato a fare sopra a Posillipo se non mi vuoi più bene?"
| image = FF.SS1983Poster.jpg
| border =
| alt =
| caption = Italian Poster
| director = Renzo Arbore
| producer = Mario Orfini, Emilio Bolles
| writer = Renzo Arbore, Luciano De Crescenzo, Andrea Ferreri, Lucio Gaudino, Fabrizio Zampa
| screenplay =
| story =
| based on =  
| starring = Roberto Benigni Renzo Arbore Pietra Montecorvino   Luciano De Crescenzo   Martufello   Bobby Solo  Gigi Proietti  Nino Frassica
| music =
| cinematography = Renzo Arbore
| editing = Renzo Arbore
| studio =
| distributor =
| released =   
| runtime = 131 minutes
| country = Italy
| language = Italian
| budget =
| gross =
}}

"FF.SS." - Cioè: "...che mi hai portato a fare sopra a Posillipo se non mi vuoi più bene?" is a 1983 film directed by Renzo Arbore, starring Roberto Benigni, Renzo Arbore and Pietra Montecorvino.

==Plot==
The film begins with Renzo Arbore and Luciano De Crescenzo driving in Rome, while discussing an original idea for a new movie. They pass under the window of real-life filmmaker Federico Fellini, who is writing a screenplay entitled F.F.S.S (Federico Fellini Sud Story). A wind causes the screenplay to fall to the road below, and the two pick it up and decide to use Fellinis ideas themselves.

Renzo Arbore plays Onliù Caporetto, a manager from Irpinia trying to bring success to Lucia Canaria (Pietra Montecorvino). While travelling across Italy, they become involved in TV commercial in Milan, then go to Rome looking for a recommendation to work in RAI. Eventually they encounter Sceicco Beige (Roberto Benigni), a music celebrity. They participate in the Festival di Sanremo 1983, where Raffaella Carrà sings Soli sulla luna and Ahi.

==Celebrity cameos== Claudio Villa, Isabel Russinova, Martufello, Mario Marenco, Sergio Japino and Gepy & Gepy

==Political allusions==
During the movie there are references to the politics of the period in which the film is set. It includes allusions to Bettino Craxi, Giulio Andreotti and Ciriaco De Mita. Also De Mita is from Nusco, province of Avellino, the same town of the character Onliù Caporetto.

==External links==
* 

 
 
 
 
 


 