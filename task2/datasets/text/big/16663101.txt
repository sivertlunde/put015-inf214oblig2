Dawg (film)
   
{{Infobox Film name = Dawg image = Dawg.jpg image_size =  caption =  director = Victoria Hochberg producer = Stephen J. Cannell writer = Ken Hastings narrator =  starring = Denis Leary Elizabeth Hurley music = Jason Frederick Amani K. Smith  cinematography = Steven Finestone editing = Claudia Finkle Mary Jo Markey	 distributor = Gold Circle Films released = 2002 runtime = 83 min country = United States language = English budget =  gross = 
}}
Dawg is 2002 dramedy film directed by Victoria Hochberg.  It stars Denis Leary and Elizabeth Hurley, in their second film together. Steffani Brass was nominated for Young Artist Award for her role in this film. Although intended to be released in theaters under the title Bad Boy, it was ultimately distributed direct-to-video.

==Plot==
Douglas "Dawg" Munford (Denis Leary) is the ultimate womanizer: He is selfish, rude and totally uncaring about what a woman thinks after they have sex. He arrives too late for his grandmothers funeral but, finds that she has left him a million dollars subject to one condition.

As explained by estate executor Anna Lockhart (Elizabeth Hurley), Douglas must contact at least a dozen of the scores of women he has seduced and left during his lifetime and beg for their forgiveness. Reluctantly, Dawg sets out on his odyssey which takes him, and the lawyer, to venues throughout California. Later, he falls for Anna.

==External links==
* 
* 
* 

 
 
 
 

 