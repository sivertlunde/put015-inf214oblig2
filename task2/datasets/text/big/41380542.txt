You'd Be Surprised!
{{Infobox film
| name = Youd Be Surprised!
| image =
| image_size =
| caption =
| director = Walter Forde
| producer = Archibald Nettlefold
| writer = Walter Forde   Harry Fowler Mear   Sidney Gilliat
| narrator = Frank Stanmore
| music = Paul Mulder
| cinematography = Geoffrey Faithfull
| editing = Walter Forde
| studio = Nettlefold Films
| distributor = Butchers Film Service
| released = April 1930
| runtime = 66 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} musical comedy Frank Stanmore. The film was shot at the Nettlefold Studios in Walton-upon-Thames|Walton. It was made during the transition to sound film. Originally silent film|silent, it had synchronised songs and music added. A silent version was also released to cater to cinemas that hadnt converted to sound yet. 

==Synopsis==
After dressing up as a prisoner for a fancy dress party, a songwriter is mistaken for a notorious escaped convict.

==Cast==
* Walter Forde as Walter 
* Joy Windsor as Maisie Vane  Frank Stanmore as Frankie 
* Frank Perfitt as Major 
* Douglas Payne as Convict 99

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 