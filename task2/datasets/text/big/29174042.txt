Toll Booth (film)
{{Infobox film
| name           = Toll Booth
| image          = TollBoothFilmPoster.jpg
| alt            =
| caption        = Theatrical Poster
| director       = Tolga Karaçelik
| producer       = Güneş Zaid   Engin Yenidünya  Tolga Karaçelik
| writer         = Tolga Karaçelik
| starring       = Büşra Pekin   Nur Aysan   Ruhi Sarı   Nergis Öztürk   Nadir Sarıbacak   Serkan Ercan   Sermet Yeşil   Enes Mazak   Mehmet Sabri Arafatlı   Mustafa Can Kılıç   Zafer Diper 
| music          = 
| cinematography = 
| editing        = 
| studio         =
| distributor    =
| released       =  
| runtime        = 
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          =
}}
Toll Booth ( ) is a 2010 Turkish drama film directed by Tolga Karaçelik, which tells the story of a reclusive toll-booth attendant. The film was selected for the 47th Antalya "Golden Orange" International Film Festival.

== Production == toll booth attendants, who had always intrigued him, performing the same work, day after day. Karaçelik said that one day he decided "to be cute", and told the attendant, "kolay gelsin!" (May it come easy). "He looked so bad at me," continued director, "I could not forget it." "The European Union does not even sell tomatoes which grow near the highway, but these   live their lives there." He wrote a letter to the union of toll booth workers, Yapıyol Sen. "Actually I wrote a poem, and all my friends made fun of me," he said, but the union quickly answered, providing the name and number of a contact. He conducted interviews with toll booth attendants first in the union office and then at the tolls, shooting the film just before the tolls at the Bosphorus Bridge were removed and replaced by an automated system. "They all say, ‘We live like a machine,’" he said. "One woman told me, ‘I work like a machine all day, go home, make dinner for my husband and kids like a machine, make love with my husband like a machine and go to bed like a machine.’" "The film provides social criticism but I chose to tell it through an individual," he continued, "our generation learned to think individually."   

== Plot ==
Kenan is a 35-year-old toll booth attendant still living with his father who suffers from heart disease. Shy and withdrawn, he prefers to live in his own head rather than engaging with the people around him. He works at a busy toll station, and is known to talk to himself occasionally while working. As with everyone else, his relationship with his father is one of reserve and distance.

During the day, Nurgül takes care of Kenan’s ailing father. In her early 30s, talkative and maternal, Nurgül has known the family since her childhood, including Kenan’s mother who died many years ago. Stuck between work and home, Kenan’s humdrum life takes a dramatic turn when the newly appointed toll booth manager visits for supervision.

== Release ==

=== Premiere ===
The film premiered in competition at the 47th Antalya "Golden Orange" International Film Festival on  .

=== Festival screenings ===
* 47th Antalya "Golden Orange" International Film Festival (October 9–14, 2010)

== Reception ==

=== Reviews ===
The film, according to "Hürriyet Daily News" reviewer Çağdaş Günerbüyük, "is successful in speaking to many in today’s world, where much of life is actually lived much like toll booth attendant Kenan." "The unchanging life of a toll booth attendant easily captures the audience’s attention in Toll Booth, which is characterized by strong acting, especially from Ercan, who reprises his expressionless mood expertly." "While providing a literal and metaphorical take on life in a box, the film also provide humor and excellent acting." 

==See also==
* Turkish films of 2010

==References==
 

==External links==
*  

 
 
 
 
 