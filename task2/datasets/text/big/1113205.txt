Bedazzled (2000 film)
 
{{Infobox film
| name           = Bedazzled
| image          = bedazzled.jpg
| caption        = Theatrical release poster
| director       = Harold Ramis
| producer       = Trevor Albert Harold Ramis
| screenplay     = Larry Gelbart Harold Ramis Peter Tolan
| based on       =  
| starring       = Brendan Fraser Elizabeth Hurley Frances OConnor David Newman
| cinematography = Bill Pope
| editing        = Craig Herring
| studio         = Regency Enterprises Kirch Media
| distributor    = 20th Century Fox
| released       =  
| runtime        = 93&nbsp;minutes
| country        = United States
| language       = English
| budget         = $48&nbsp;million
| gross          = $90,383,208
}} comedy film of the same name, originally written by Peter Cook and Dudley Moore, which was itself a comic retelling of the Faust legend. The film was directed by Harold Ramis and stars Brendan Fraser and Elizabeth Hurley.

==Plot== Satan to analyze souls and determine individual weaknesses to exploit and corrupt. The program finally settles on Elliot Richards (Brendan Fraser), a geeky, over-zealous man working a dead-end technical support job in a San Francisco computer company. He has no friends and his co-workers are always avoiding him. He has a crush on his colleague, Alison Gardner (Frances OConnor), but lacks the courage to ask her out. After Elliot is again ditched by his co-workers at a bar while trying to talk to Alison, he says to himself that he would give anything for Alison to be with him. Satan, in the form of a beautiful woman (Elizabeth Hurley), overhears him and offers to give Elliot seven wishes in return for his soul.
 there aint no such thing as a free lunch." After taking Elliot to her office, based at a nightclub in Oakland, California|Oakland, Satan convinces Elliot to sign her contract, and delivers further wishes. Each wish has Elliot living them out with Alison and his co-workers in surrogate roles. However, he doesnt know that Satan will always spoil his wishes by adding something he doesnt want. Elliot wishes to be rich and powerful, with Alison as his wife. Satan makes him a Colombian drug lord whose wife despises him and cheats on him with Raul, his co-worker, who is secretly planning to get rid of Elliot and take his position and property. Soon after there is a firefight between his and Rauls people where Elliot "dies". When he returns to the real world, Satan points out that he never wished for Alison to love him.
 beautiful the NBA star, but also gives him a small penis and a low IQ, which causes Alison, a sports reporter, to lose interest in him shortly after they meet.
 gay and his assassination which he nearly avoids. After each wish is renounced, Elliot meets with Satan and she blames him for not being specific enough. Eventually he returns to work, thinking about what he should do with the last two wishes. Satan appears on the computer screen, pointing out that on their first meeting he asked for a Big Mac and Coke, although she had stated that it was a test wish and granted it before Elliot signed the contract. Elliot loses his patience and storms out of his office.
 confesses to a priest who seems sympathetic. However, after being asked whether he thinks asking Satan for a Big Mac and Coke counts as a wish, the priest, believing he is drunk, has Elliot arrested. The sergeant books him, and Satan, dressed as a police officer, throws him in a cell, telling him that she does like him, and it would not hurt to have her as a friend. Elliots cellmate (Gabriel Casseus) tells him that he cannot possibly sell his soul as it belongs to God, and although Satan may try to confuse him, in the end he will realise who he truly is, and what his purpose is. Elliot questions the man as to his identity, but the response is simply "a really good friend", hinting that he may in fact be God, or at least, an angel.

Elliot asks Satan to cancel their contract. When Satan refuses, Elliot states he will not use his final wish. Satan teleports them to Hell, where she transforms first into a black horned monster, then into a giant. When Satan pushes him to make a final wish, Elliot wishes that Alison could have a happy life - with or without him. Satan sighs and Elliot falls into the depths of Hell. Elliot wakes up on a marble staircase, wondering if it is Heaven. Satan tells him that it is a courthouse and that a selfless wish voids the contract, so he keeps his soul. Elliot admits that despite her manipulation of him he has come to like Satan and regards her as a friend, something she does not object to. She also advises that Heaven and Hell can be found on Earth; it is up to humans to choose. Elliot finally asks Alison out, only to learn that she is already dating another man. He continues with his life, but with a better understanding of who he is.

Later Elliot is confronted by Bob, one of his co-workers, who starts ridiculing Elliot at the encouragement of his co-workers. Elliot loses his temper and grabs a terrified Bob by the shirt, but lets go, simply saying, "Nice talking to you." A threatening look sends his other co-workers scurrying away in fear. At home, he meets a new neighbor, Nicole Delarusso (also played by Frances OConnor), whose looks resemble Alisons, but whose personality, interests and fashion sense are much closer to his. He offers to help her unpack and they begin a relationship. While the two walk along a boulevard, Satan and Elliots cellmate, both dressed in white, are seen playing chess, looking at Elliot and his new girlfriend, with Satan taking the opportunity to fix the game but get caught by the guy, who only laughts about that. The scene ends with Satans computer program listing foibles of Nicoles and Elliots, which they both tolerate.

==Cast==
*Brendan Fraser as Elliot Richards / Jefe / "Mary" / Abraham Lincoln
*Elizabeth Hurley as The Devil (Satan)
*Frances OConnor as Alison Gardner / Nicole Delarusso
*Orlando Jones as Daniel / Dan / Danny / Esteban / Beach Jock / Lamar Garrett / Dr. Ngegitigegitibaba
*Paul Adelstein as Bob / Roberto / Beach Jock / Bob Bob
*Toby Huss as Jerry / Alejandro / Beach Jock / Jerry Turner / Lance
*Miriam Shor as Carol / Penthouse Hostess
*Gabriel Casseus as Elliots Cellmate
*Brian Doyle-Murray as Priest
*Jeff Doucette as Desk Sergeant
*Aaron Lustig as Synedyne Supervisor
*Rudolf Martin as Raoul
*Julian Firth as John Wilkes Booth

==Reception==
The film received mixed reviews from critics.       Film aggregator Rotten Tomatoes gave it 49%, with the general consensus being: "Though it has its funny moments, this remake is essentially a one joke movie with too many flat plots."  The movie was a box office success.  

==References==
 

== External links ==
 
* 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 