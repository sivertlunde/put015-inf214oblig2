The Scoundrel
 
{{Infobox film
| name           = The Scoundrel
| image_size     = 
| image	         = The Scoundrel FilmPoster.jpeg
| caption        = Theatrical release poster
| director       = Ben Hecht Charles MacArthur
| producer       = 
| writer         = Ben Hecht Charles MacArthur
| narrator       = 
| starring       = Noël Coward Julie Haydon Stanley Ridges Rosita Moreno Lionel Stander
| music          = George Antheil
| cinematography = Lee Garmes
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         = 
}}
The Scoundrel is a 1935 drama film directed by Ben Hecht and Charles MacArthur, and starring Noël Coward, Julie Haydon, Stanley Ridges, Rosita Moreno, and Lionel Stander. It was Cowards film debut, aside from a bit role in a silent film. It deals with supernatural redemption in a way rather similar to Ferenc Molnárs  Liliom, and drew inspiration from the life of publisher Horace Liveright, who had died in September 1933.

==Plot==
Anthony Mallare (Coward) is a publisher who (it appears) wishes to ruin the life of every person he comes in contact with. Every sentence he says is like a poisoned dart aimed for the greatest damage, and delivered in cold lifeless tones. He is under no illusion regarding his own personality, remarking to his staff at large that he has found the perfect woman - one as empty as he is: "I must marry her......it would be like two empty paper bags belaboring one another". He finally manages to completely destroy the career and life of an aspiring young author (Ridges) and his girlfriend (Haydon), who curses him with the hope that he will die friendless. Shortly afterwards he is killed when his plane crashes into the ocean—Haydons character, upon hearing of the tragedy, remarks, "Ive just found out there IS a God!"

Faced with the prospect of damnation he is allowed to go back to earth to find one person who will mourn for him - which person turns out to be Haydon. (Those around him are astonished to see him apparently alive and back at work, but gradually become aware that something supernatural is afoot.)
 Best Original Story by writing team Hecht and MacArthur. It is an early role for Lionel Stander (his first year in pictures) and is a rare film role for columnist Alexander Woollcott typecast as an acid-tongued writer.

The Scoundrel had its copyright renewed in 1962 (R297413 25 Jun 62) and for several years has been available on several pirated VHS and DVD copies. In March 2008, the Noël Coward Society screened a 16mm copy of the film at the Paley Center for Media in New York City. (Renewal notice in Minus and Hale, Film Superlist, vol. 8, p.&nbsp;747)

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 