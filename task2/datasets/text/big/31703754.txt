Safe (2012 film)
 
{{Infobox film
| name           = Safe
| image          = Safe2011Poster.jpg
| caption        = Teaser poster
| director       = Boaz Yakin
| producer       = Lawrence Bender Dana Brunetti
| writer         = Boaz Yakin
| starring       = Jason Statham Chris Sarandon Robert John Burke James Hong Catherine Chan
| cinematography = Stefan Czapsky
| editing        = Frederic Thoraval
| music = Mark Mothersbaugh Lawrence Bender Productions Trigger Street Productions
| distributor    = Lionsgate
| released       =   
| runtime        = 95 minutes
| country        = United States Russian
| budget         = US$33 million
| gross          = US$40,346,186 	    
}} action crime crime thriller thriller film written and directed by Boaz Yakin and starring Jason Statham, Chris Sarandon, Robert John Burke and James Hong. Statham plays an ex-cop and former cage fighter who winds up protecting a gifted child who is being chased by the Russian mafia, Chinese Triads, and corrupt New York City police.

==Plot== Reggie Lee).

One year later, Han arrives from China, asking Mei to memorize a long number. On the way to retrieve a second number, the vehicle is ambushed by the Russian mafia. Mei is taken to Emile, who demands the number, but Mei refuses. Before they can further interrogate her, they are interrupted by police, sent by the corrupt Captain Wolf (Robert John Burke), who works for Han. Mei escapes during the confusion, chased by the Russians to a nearby subway station, where Luke contemplates suicide. Recognizing Chemyakin (Igor Jijikine), one of the men who killed his wife, and seeing Meis distress, Luke boards the train and kills Chemyakin. Mei flees at the next stop, only to be stopped by two corrupt detectives working for Wolf. Luke arrives and incapacitates the detectives, convincing Mei of his good intentions, before he also dispatches some Russians.

Hiding in a hotel, Mei explains the number to Luke, who guesses that its the code to a combination safe. However, Quan tracks Mei down, through her cell phone, escaping with her during a diversion, as Luke fights through his men. Across town, Captain Wolf meets with Mayor Danny Tremello (Chris Sarandon), who has learned that Luke is involved. He cautions Wolf, explaining that Luke wasnt really a cop, but a government assassin responsible for numerous black operations on the orders of his commanding officers in an effort to wipe out criminal organizations in the New York area along with his former partner Alex Rosen (Anson Mount). Lukes life of exile is not one of necessity, but rather of atonement for the things he did in the employ of the government.

Using Chemyakins phone, Luke sets up Vassily, whom he easily kidnaps. Emile reluctantly accepts a deal for his sons life, explaining that Meis number unlocks a heavily guarded safe with $30 million, though he does not know the contents of a second safe. Needing a team to get to the safe, Luke recruits Wolf and his detectives. Together, they fight through numerous Triad gangsters, eventually reaching the safe. As Luke is about to open it, Wolf attempts to betray him, but Luke kills the remaining detectives and takes Wolf hostage. Using the money, Luke bribes Alex Rosen, who now works for the mayor, into rescuing Mei. Alex reveals the second safe to belong to the mayor, containing a disc with data on his corrupt deals. Alex meets with Quan showcasing his great combat prowess by easily killing him and his men, as Mei watches. Meanwhile, Luke assaults the mayor and retrieves a copy of the mayors disc.

Alex and Luke arrange a meeting, but Luke refuses to surrender the money, instead suggesting that they settle it with a fight. Before they can begin, Mei shoots Alex, wounding him, and Luke finishes him off. In the aftermath, Luke gives Wolf $50,000 and instructs him to return Vassily to his father, unharmed. He sends the remainder of the money to Han, to buy off Meis freedom, threatening to ruin Hans operations should Han try to recover Mei. Han leaves New York City in disgust, as Luke hides multiple copies of the disc throughout the city. When Mei asks if they are finally safe, Luke responds that they will just take it one day at a time.

==Cast==
* Jason Statham as Luke Wright   
* Catherine Chan as Mei 
* Chris Sarandon as Mayor Danny Tremello
* Robert John Burke as Captain Wolf 
* James Hong as Han Jiao  Reggie Lee as Quan Chang 
* Danny Hoch as Julius Barkow 
* Danni Lang as Ling   
* Igor Jijikine as Shemyakin
* David Kim as Triad #1
* Anson Mount as Alex Rosen
* Joseph Sikora as Vassily Docheski

==Production== Lawrence Bender Productions, Trigger Street Productions, Automatik Entertainment, and 87Eleven Action Design also produced. 
 Broad Street was done on the nights and early mornings of November 17, 18 and 19.  A class from a Catholic School in downtown Philadelphia was used for a scene depicting a class in China.

In the United States, the film was scheduled to be released on October 28, 2011,  and March 2, 2012,  but was eventually pushed back to April 27, 2012. 

==Reception==
Safe has garnered lukewarm to positive reviews from critics. Review aggregation website Rotten Tomatoes reports that 57% of critics have given the film a positive review. The sites consensus says that "while hard-hitting and violently inventive, Safe ultimately proves too formulaic to set itself apart from the action thriller pack – including some of its stars better films".  Peter Travers, the film critic for Rolling Stone, gave the film 2 stars out of a possible 4, and said that "the trouble with Safe is that you know where its going every step of the way". He also added that "Between the fists, kicks, bullets, car chases and broken trachea, the movie could have milked the sentiment of that relationship until you puked. But Statham and the scrappy Chan play it hard. The restraint becomes them. Statham is still playing it safe in Safe, but vulnerability is showing through the cracks.".  Claudia Puig of USA Today gave the film a moderately positive review, saying that "Yakins slick direction, marked by quick cuts, unstinting energy and a lack of sentimentality, makes the action scenes satisfying," but thought the dialogue was "riddled with clichés."  Robert Abele of the Los Angeles Times scored the film 3/5, saying "Yakin gives his star plenty of room to look mean, think fast, drive faster, punch, quip, mow down and charismatically bond with the most imperiled child character in screen memory."  Kim Newman gave the film four stars out of five in Empire Magazine, describing it as "A rough, exhausting, exhilarating action picture with a payoff which would have delighted Sam Fuller or Howard Hawks". 

==References==
 

==External links==
*  
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 