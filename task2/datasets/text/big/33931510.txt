Kalyug (2005 film)
{{Infobox film
| name           = Kalyug
| image          = Kalyug05.jpg
| caption        = Theatrical release poster
| director       = Mohit Suri
| producer       = Mukesh Bhatt  Mahesh Bhatt
| writer         = Mohit Suri
| starring       = Emraan Hashmi Kunal Khemu Amrita Singh
| music          = Anu Malik Faizal Rafi & Rohail Hyat Jal (Band) Mithoon Raju Singh
| cinematography = Rituraj Narain
| editing        = Amit Saxena
| distributor    = Vishesh Films
| released       =  
| runtime        = 126&nbsp;minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} action drama pornographic film industry. The film depicts the devastating effect that non-consensual porn films have on the subjects, and discourages such illegal porn. The film was directed by Mohit Suri and produced by Mahesh Bhatt. It introduces actor Kunal Khemu, in his debut film as an adult actor.    It also stars Emraan Hashmi, Smilie Suri and Amrita Singh. Kalyug released on December 9, 2005, and turned out to be a moderate success at the box office.

==Plot==
18&nbsp;years ago, the Darr family, consisting of Pushkaran (Amitabh Bhattacharjee) and his son, Kunal (Kunal Khemu), were forced to leave Kashmir by terrorists, who had forced thousands of other Kashmiri Pandits to be mere refugees in their very own country. Pushkaran and Kunal re-locate to Bombay, where they live in a small room. This is where Kunal grows up, and gets a job at a gym. Then one day, the police knock on his door, informing him that his father had lost his hold from a crowded local train, fallen, and instantly killed. A devastated Kunal arranges his fathers funeral, and before he could reconcile to living alone, his relatives contact him from Jammu, informing him that his father had promised to look after a young woman named Renuka (Smiley Suri). Kunal agrees to fulfill his fathers promise. 

When Renuka arrives, she finds out about the untimely death, and wants to return, but ticket reservation force her to stay with Kunal for a week. It is this week that changes their lives, for they fall in love and get married. During their first intimate night together, porn-website creator Simi Roy (Amrita Singh) and her partner Johny (Ashutosh Rana) record them and turn them into porn-stars. Renuka cant take the embarrassment and commits suicide. Kunal is then arrested by police for illegal porn online. Kunal proves himself innocent, and decides to track down Simi Roy and her team and get revenge. He comes across Ali (Emraan Hashmi) a modern-day punk, who runs an adults only shop. In his shop, Kunal sees a magazine featuring a pornstar Kunal had seen on the same website where his and Renukas video was. He uses Alis help to track down the girl, and realizes that the matter is much more complicated, confusing and unbelievable than he thought.

==Cast==
*Kunal Khemu as Kunal P. Darr
*Emraan Hashmi as Ali Bhai
*Smiley Suri as Renuka
*Amrita Singh as Simi Roy
*Deepal Shaw as Anita (Annie)
*Ashutosh Rana as Johny
*Amitabh Bhattacharjee  as Pushkaran.

==Music==
{{Infobox album |  
 Name = Kalyug |
 Type = Soundtrack |
 Artist =Anu Malik |
 Cover = |
 Released =   |
 Recorded = 2005 | Feature film soundtrack |
 Length = |
 Label = Sa Re Ga Ma |
 Producer = |
 Reviews = |
}}
The soundtrack was released in November 2005. The music was composed by Raju Singh, Anu Malik, Faizal Rafi & Rohail Hyatt, Jal (Band) & Mithoon with the track "Aadat (song)|Aadat" reused from Jal (band)|Jals album of the same name.

===Track listing===
#Jiya Dhadak Dhadak Jaye: Rahat Fateh Ali Khan
# 
#Dheere Dheere: Alisha Chinoy
#Ye Pal: Najam Sheraz
#Aadat (Remix): Atif Aslam
#Tujhe Dekh Dekh: Rahat Fateh Ali Khan
#Thi Meri Dastan: Anuradha Paudwal & Amit Sana Jal

== References ==
 

== External links ==
* 

 
 

 
 
 
 
 