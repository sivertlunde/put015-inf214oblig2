Joan of Plattsburg
  William Humphrey Goldwyn Pictures Corporation and starring Mabel Normand. 

==Plot synopsis==
An orphan (Normand) who overhears German spies plotting in a basement near an American World War I training camp and believes that, like a modern day Joan of Arc, shes listening to disembodied voices. 

==Cast==
*Mabel Normand ...	Joan
*Robert Elliott	 ...	Capt. Lane
*William Frederic  ...	Supt. Fisher (billed as William Fredericks)
*Joseph W. Smiley  ...	Ingleton
*Edward Elkas  ...	Silverstein
*John Webb Dillon ... Miggs
*Willard Dashiell ...	Colonel
*Edith McAlpin	 ...	Mrs. Lane
*Isabel Vernon	 ...	Mrs. Miggs

==References==
 

==External links==
*   in the New York Times
*  
*   at Turner Classic Movies
*   in the Toronto World
*   in Visions of the Maid

 
 
 
 
 


 