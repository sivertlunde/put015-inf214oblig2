The Economics of Happiness
 
 
{{Infobox film
| name           = The Economics of Happiness
| image          =
| caption        =
| director       = Steven Gorelick Helena Norberg-Hodge John Page
| producer       = Helena Norberg-Hodge
| narrator       = John Page
| music          = Florian Fricke
| cinematography =
| editing        = Anna Fricke Army Armstrong Meredith Holch
| studio         =
| distributor    =
| released       =   
| runtime        = 68 minutes
| country        = Australia
| language       = English
| budget         =
| gross          =
}}
The Economics of Happiness is a 2011 documentary film directed by Helena Norberg-Hodge, Steven Gorelick, and John Page, and produced by the International Society for Ecology and Culture. It has won "Best in Show" at the Cinema Verde Film and Arts Festival, "Best Direction" from EKOFilm 2011 (Czech Republic), "Judges Choice" and "Audience Choice" at the Auroville International Film Festival (India), an "Award of Merit" from the Accolade Film Festival, and several other awards.

==Synopsis==
The film features many voices from six continents calling for systemic economic change. The documentary describes a world moving simultaneously in two opposing directions. While government and big business continue to promote globalization and the consolidation of corporate power, people around the world are resisting those policies and working to forge a very different future. Communities are coming together to re-build more human scale, ecological economies based on a new paradigm: an economics of localization.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 

 