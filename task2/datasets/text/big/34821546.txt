The Island of Contenda
 
{{Infobox film
| name           = The Island of Contenda
| director       = Leão Lopes
| producer       = Paulo de Sousa
| writer         = Henrique Teixeira de Sousa José Fanha Leão Lopes
| starring       = João Lourenço Camacho Costa Luísa Cruz
| music          = Manuel Paulo Felgueiras
| sound editor   = Thomas Gauder	
| photography    = João Abel Aboim
| editing        = Denise Vindevogel
| studio         = Vermédia RTP MBSA Saga Film Instituto Cabo-verdiano de Cinema
| distributor    = Marfilmes
| released       = 1996
| runtime        = 110 minutes
| country        = Cape Verde Portuguese
}}

The Island of Contenda (original title: O Ilhéu de Contenda) is a 1995 drama film directed by Leão Lopes.

==Synopsis==
Cape Verde, 1964. At the feet of a mighty volcano, the traditional Cape Verdean society is undergoing a steady change. The old land-owning aristocracy is disintegrating. A class of mulatto begins to emerge, with a trade-based financial power that threatens the landlords. A new identity arises, a mix of old and new, of African and Portuguese culture, sensual and dynamic.

The songs of Cesária Évora follow this inevitable transformation with the beautiful landscape of Fogo, Cape Verde as scenery.

The Island of Contenda was the first feature film to be produced with the financial support of the Cape Verde national film institute (Instituto Cabo-verdiano de Cinema) that no longer exists. 

From the novel by Henrique Teixeira de Sousa.

==Cast==
* João Lourenço
* Camacho Costa
* Luísa Cruz
* Filipe Ferrer
* Betina Lopes
* Cecília Guimarães
* Marina Albuquerque
* Henrique Espírito Santo	
* José Fanha	
* Laurinda Ferreira	
* Leandro Ferreira	
* Isabel de Castro
* Teresa Madruga	
* Luís Mascarenhas	
* Mano Preto	
* Carlos Rodrigues	
* Horácio Santos	
* Diogo Vasconcelos	
* Pedro Wilson

==Festivals==
* Cabo Verde International Film Festival
*  (1997)

==Awards== Best Original Score at FESPACO - Panafrican Film and Television Festival of Ouagadougou, Burkina Faso (1997)
*  (1997)

==See also==
* 
* 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

==References==
 

 
 
 
 
 


 