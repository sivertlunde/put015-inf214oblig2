Gåten Ragnarok
{{Infobox film
| name           = Gåten Ragnarok
| image          = 
| caption        = 
| director       = Mikkel Brænne Sandemose
| producer       = 
| screenplay     = John Kåre Raake 
| based on       = 
| starring       = Pål Sverre Valheim Hagen  Nicolai Cleve Broch  Bjørn Sundquist Sofia Helin Maria Annette Tanderø Berglyd Julian Podolski
| music          = Magnus Beite
| cinematography = Daniel Voldheim
| editing        = Christian Siebenherz	 	
| studio         =  Magnolia
| released       =  
| runtime        = 93 minutes
| country        = Norway
| language       = English Norwegian
| budget         = 
}}
 action adventure film about the legendary story of Ragnarok.

==Plot==
When the archaeologist Sigurd Swenson sets off in search of evidence to support the story of Ragnarok, the end of the world in Norse mythology, he decides to form an expedition with two colleagues and his two kids. This adventure leads them to Finnmark in the Northern part of Norway, and into a "no mans land" between Russia and Norway, where no one has set foot since ancient times. Old runes unfold new meanings, revealing a truth bigger and more spectacular than ever expected.

==Cast==
* Pål Sverre Valheim Hagen as archaeologist Sigurd Svensen
* Nicolai Cleve Broch as archaeologist Allan
* Bjørn Sundquist as the tour guide Leif
* Sofia Helin as Elisabeth
* Maria Annette Tanderø Berglyd as Sigurds daughter Ragnhild
* Julian Podolski as Sigurd son Brage
* Jens Hultén as the Viking king
* Terje Strømdahl as the museum director
* Vera Rudi as Åsa

==Production==

 

==Release==
The film was released on the 8 October 2013.
 Head Hunters.

===Critical response===
After a screening at Fantastic Fest the film was received positively. 

The website Twitch Film said that: "It adds up to a fun family adventure, with Norwegian style and a welcome edge, something even Indiana Jones in his prime could never quite achieve". 

==See also==
 
* Ragnarok

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 