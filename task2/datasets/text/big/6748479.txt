The Sensation of Sight
  Jane Adams, Scott Wilson.

The Sensation of Sight made its world premiere at the San Sebastian International Film Festival in 2006 and was an official selection in 19 film festivals on five continents, including the Durban International Film Festival, where it won the festivals Best Cinematography award for cinematographer Christoph Lanzenberg. The Sensation of Sight has been shown in festivals in Brazil, China, Lithuania, and Poland, and made its U.S. premiere at the Denver Film Festival, followed by festival showings throughout the U.S.

In the summer of 2008, distributor Monterey Media gave the film a limited theatrical release, followed by a DVD release in the fall.

 

The film was shot entirely in the town of Peterborough, New Hampshire.

== Review ==
The film scored moderate reviews with Rotten Tomatoes rating it at 50% from 6 reviews. 

==External links==
 
*  

== Sources ==
 

 
 
 
 
 