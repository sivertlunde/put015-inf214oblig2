Unnikrishnante Adyathe Christmas
{{Infobox film
| name           = Unnikrishnante Adyathe Christmas
| image          = 
| image size     = 
| alt            = 
| caption        =  Kamal
| producer       = Kitho John Paul (dialogues) Kaloor Dennis (dialogues) John Paul Kaloor Dennis
| narrator       = 
| starring       = Jayaram   Suresh Gopi   Sumalatha
| music          = Johnson
| cinematography = B. Vasanthkumar
| editing        = K. Rajagopal
| studio         = Chithra Pournami
| distributor    = Chithra Pournami
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 1988 Cinema Indian Malayalam Malayalam film, Kamal and Innocent in lead roles. The film had musical score by Johnson (composer)|Johnson.     The film is based on a short story by Kakkanadan.

==Plot==
Sophia (Sumalatha) has not been home since a rift with her parents over her marriage to Josekutty (Devan). Sophias father Ittichan (Nedumudi Venu) and ailing mother(Kaviyoor Ponnamma) request Unnikrishan (Jayaram), a close friend, to find Sophia and bring her home to them.  Unnikrishnan promises the parents that he will bring Sophia home by Christmas. With the help of his old schoolmate, Parasuraman (Suresh Gopi), Unnikrishnan finds Sophia.  Sophia, who is working in a hotel owned by Paul Kallookkaran (Sukumaran), claims not to know  Unnikrishnan, who is also met with hostility by Paul. Unnikrishan persists, and learns that Josekutty is dead and Sophia is trapped in a dire situation. She is pretending not to know Unnikrishnan in order to protect him and herself.  Unnikrishan tries to help Sophia escape, but tragedy ensues.

==Cast==
*Jayaram: Unnikrishnan
*Sumalatha: Sophia
*Suresh Gopi: Parasuraman
*Sukumaran: Paul Kallookkaran
*Lizy (actress)|Lizy: Sridevi
*Nedumudi Venu: Ittichan
*Kaviyoor Ponnamma: Aliyamma
*Mala Aravindan: Swamy
*Thodupuzha Vasanthi: Sophia
*Innocent (actor)|Innocent: Josekutty
*Devan (actor)|Devan: Josekutty
*Valsala Menon: Parvathiyamma
*Lalitha Sree: Subhadra
*Lalu Alex: Dasan

==Soundtrack== Johnson and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ormakal Valarnnu || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 2 || Ormakal Valarnnu || Lathika || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 

 
 
 
 

 