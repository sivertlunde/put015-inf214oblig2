Hold On! (film)
{{Infobox film
| name           = Hold On!
| image          = Hold On! 1966 movie poster.png
| image size     =
| alt            = 
| caption        = Official US movie poster
| director       = Arthur Lubin
| producer       = Sam Katzman
| writer         = Robert E. Kent (credited as "James B. Gordon")
| screenplay     = 
| story          = 
| narrator       =  Bernard Fox
| music          = Fred Karger (score) Hermans Hermits (songs) Paul C. Vogel Ben Lewis Alex Beaton
| studio         = Four Leaf Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Hold On!.

==Plot== Gemini space capsule,   NASA scientist Edward Lindquist is sent by United States Department of State|U.S. State Department official Colby Grant to shadow the band on tour.  His orders are to find out all he can about them to stave off a "public relations|P.R. nightmare". (Grant fears that putting the bands name on the rocket will make the world think the U.S. is "still a colony of Great Britain".)
 starlet Cecilie Bannister hires a publicity agent and photographer to take photos of her with Herman and the band, sure that this publicity boost will get her a new contract with a movie studio. They take an unflattering picture during a riot of teenage girls at Los Angeles International Airport but the misleading story in the newspapers leads Lindquist to believe that Bannister is an "old friend" of Hermans.  Likewise, Bannister believes that Lindquist is a writer and part of the bands entourage as they pump each other for information about the band that neither of them really has.
 Miramar Hotel by their manager, Dudley, in advance of a charity benefit performance. Herman sees teens playing on the beach and wishes he could be one of them, meet the girl of his dreams, and fall in love.  Mrs. Page, the benefit organizer, introduces Herman to her daughter, Louisa, who offers to show him the sights of Los Angeles. Denied by Dudley, Herman and the Hermits sneak off to Pacific Ocean Park where they split up, reasoning correctly that if they dont stick together, nobody will recognize them.  Herman finds Louisa and falls in love while the other band members explore the park. Believing that the boys have been kidnapped, Dudley calls in the police.
 Rose Bowl, hypersonic jet Cape Kennedy for the rocket launch and back in time to finish the concert and play one more song before the credits roll. 

==Cast==
Peter Noone is Herman, the leader of Hermans Hermits, a real-life English rock band comparable in popularity at the time to The Beatles.  While forced to take elaborate security procedures to avoid being "torn apart" by mobs of teenaged fans, Herman laments that he has nobody to talk to and dreams of meeting a woman who will love him for himself.   Karl Green (as Karl), Keith Hopwood (as Keith), Derek Leckenby (as Derek), and Barry Whitwam (as Barry) are the other four real-life members of Hermans Hermits.  Playing supporting roles to Herman, they too wish to see America up close instead of just through a window. All five band members play fictionalized versions of themselves. 

Herbert Anderson plays Ed Lindquist, a NASA scientist with a nervous nature who is assigned to follow the band across the country by State Department official Colby Grant (Harry Hickox) to determine whether the U.S. can name a Gemini space capsule after the band.  Lindquist is initially opposed to the assignment and even faints during his first encounter with the band. He grows to like and accept the young rockers as being "like boys anywhere" and supports the effort to name the capsule for them. 

Sue Ane Langdon plays Cecilie Bannister, an actress whose studio contract has expired.  She believes that associating herself with Hermans Hermits will give her enough publicity to get a new contract and more acting jobs. To this end, she hires a publicity man (Mickey Deems) and his photographer (Phil Arnold) while devising ever more elaborate schemes to get close to the band. 
 Bernard Fox plays Dudley, a fictionalized version of the bands real-life manager, Mickie Most. He seeks to protect these young men both from the public and from themselves but the band chafes under his strict control.  Louisa inspires Herman to lead the bands escape from their hotel which begins to tie the movies three main plotlines together. 

==Production info==
Hold On!, largely an excuse to string together performances by  .  The films theatrical soundtrack is monaural. 
 EP in the United Kingdom by EMI/Columbia. 

==Reception==

===Critical reception===
When the film was initially released, the Los Angeles Times said it "lets loose the bright presence of Hermans Hermits on the wide screen" but opined that "their high spirits have been cut down to fit producer Sam Katzmans trite formula for teen-age entertainment".   The New York Times called the film "an occasionally amusing but nonsensical pastiche" that served "as the cement to hold together the song sequences of Hermans Hermits". 
 The Film Daily calling Hold On! a "fun and frolic in a formula vein" with the presence of Hermans Hermits making of "a foregone boxoffice win" with "youngsters".  Boys Life described the film as only for "swingers who are really with it". 
 A Hard Days Night and Help! (film)|Help! made getting a "British Invasion band to play themselves in a movie with a made-up story" look "so easy" that MGM believed that Hermans Hermits could share similar success but that with Hold On! the "only similarity to Help! is the exclamation mark". 

===Home video===
The film was broadcast nationally on August 21, 1970, as part of "The CBS Friday Night Movies". 

For many years, the film was not available on home video in the United States.  Turner Classic Movies (TCM) ranked the demand for Hold On! as #380 on the TCM Not-On-Home Video list.   While TCM does broadcast the film once or twice a year, in September 2010 USA Today described the film as "rarely seen".  The film was finally made available on DVD in 2011 through the Warner Archive Collection. 

==References==
{{reflist|2|refs=
   
   
   
   
   
   
   
   
   
   
   
   
   
   
}}

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 