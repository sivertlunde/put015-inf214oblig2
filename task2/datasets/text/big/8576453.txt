The Bremen Town Musicians (film)
  Soviet musical rock n roll music. Two sequels were made, including On the Trail of the Bremen Town Musicians (1973) and The New Bremen Musicians (2000).

==Plot==
The cartoon focuses on a Donkey, a Dog, a Cat, and a Rooster and their master Troubadour (much more a classical five-pieces rock-band than a travelling circus), all of whom are voiced by Oleg Anofriyev, with the exception of the Donkey, who is voiced by Anatoli Gorokhov. The young musician falls in love with a princess, voiced by Elmira Zherzdeva, and, after some troubles solutions, they run away so that they can continue their love affair.

==Dubbing, Actors & Soundtrack in other country==
{| class="wikitable"
|-
! Country !! Voice !! Soundtrack !! TV
|-
|  
| Aleksandr Panayotov, Alena Vinnitskaya, Jango (singer), Artem Meh, Alan Badoev, Ivan Dorn, Kamaliya, Quest Pistols
| Нічого на світі (begin and end), виступ музикантів на площі, пісня принцеси, пісня отаманші, пісня охоронців, пісня розбійників, пісня трубадура, бальна музика
| M1 (Ukraine)
|}

==See also==
*History of Russian animation

== External links ==
* 
*  at Animator.ru

 
 
 
 
 
 
 

 