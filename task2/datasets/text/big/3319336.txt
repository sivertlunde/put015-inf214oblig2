The Laughing Policeman (film)
{{Infobox film
| name           = The Laughing Policeman
| image          = Laughing-policeman-poster.jpg
| image size     = 
| border         = yes
| alt            = 
| caption        = Theatrical Poster
| director       = Stuart Rosenberg
| producer       = Stuart Rosenberg Thomas Rickman
| based on       =  
| starring       = Walter Matthau Bruce Dern Louis Gossett, Jr. Anthony Zerbe Albert Paulsen Charles Fox
| cinematography = David M. Walsh
| editing        = Bob Wyman
| studio         = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = $2,280,000 
| gross          = $1,750,000 (US/ Canada) 
}} The Laughing Policeman by Sjöwall and Wahlöö. The setting of the story is transplanted from Stockholm to San Francisco. It was directed by Stuart Rosenberg and features Walter Matthau as Detective Jake Martin (the literary Martin Beck).

==Plot ==
A busload of passengers, including off-duty police detective Dave Evans, is gunned down and killed. Evans, on his own time, has been following a man named Gus Niles in search of information linking businessman Henry Camarero to the murder of his wife, Teresa, two years earlier.

Evans was the partner of Detective Sergeant Jake Martin, a veteran but cynical member of the Homicide Detail working the bus massacre investigation. Jake originally investigated the Teresa Camarero case and has been obsessed with his failure to "make" Camarero for the murder. Jake returns to it after many dead-end leads in the bus investigation. Niles was killed on the bus as well, and it was Niles who provided the alibi that enabled Camarero to cover up his wifes murder.

The sullen Jake and enthusiastic but impulsive Inspector Leo Larsen are paired to interview suspects. Jake shuts out Larsen from his deductions, while Larsen, despite a loose-on-the-rules and brutal side, tries to understand and gain the confidence of his new partner. Defying the orders of their police superior Lt. Steiner, they seek, find and then smoke out Camarero, leading to a chase through the streets of San Francisco and a confrontation aboard another bus.

==Cast==
* Walter Matthau as Sgt. Jake Martin
* Bruce Dern as Insp. Leo Larsen
* Louis Gossett, Jr. as Insp. James Larrimore
* Anthony Zerbe as Lt. Nat Steiner
* Albert Paulsen as Henry Camerero
* Val Avery as Insp. John Pappas
* Paul Koslo as Duane Haygood
* Cathy Lee Crosby as Kay Butler
* Joanna Cassidy as Monica

==Reception==
Roger Ebert of the Chicago Sun-Times said, "The Laughing Policeman is an awfully good police movie: taut, off-key, filled with laconic performances. It provides the special delight we get from gradually unraveling a complicated case. ... The direction is by Stuart Rosenberg, and marks a comeback of sorts. ... With The Laughing Policeman, he takes a labyrinthine plot and leads us through it at a gallop; he respects our intelligence and doesnt bother to throw in a lot of scenes where everything is explained. All the pieces in the puzzle do fit together, you realize after the movie is over, and part of the fun is assembling them yourself. And there are a couple of scenes that are really stunning, like the bus shooting, and an emergency room operation, and scenes where the partners try to shake up street people to get a lead out of them. Police movies so often depend on sheer escapist action that its fun to find a good one."  

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 