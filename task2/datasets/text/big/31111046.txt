The Truth (1998 film)
{{Infobox Film
| name = The Truth
| image = The Truth (1998).jpg
| caption = DVD Cover
| director = Shaji Kailas
| writer = S. N. Swamy Murali Vani Viswanath
| producer = Ashraf
| music = Rajamani
| cinematography = Anandakuttan
| editing = Bhoominathan
| studio = A. B. R. Productions
| distributor = 
| released =  
| runtime =
| country = India
| language = Malayalam
| budget =
}}

The Truth is a 1998 Malayalam investigative thriller film written by S. N. Swamy and directed by Shaji Kailas.  Mammootty plays the lead role of the investigating officer in the film. It was a Super Hit at the box office. The film was dubbed in Tamil as Unmai and in Telugu as Delhi Simham.

==Plot==
The film begins with some insight into a Brahmin family, with Patteri Thilakan as the patriarch, who is a great astrologer and helps a policeman (Janardhanan) crack a robbery case. The story then moves on to a political arena, where Balachandra Menon is presiding as the Chief Minister and battling against ministers among his own party who are keen to oust him in the next elections. During a chance encounter with Patteri (Thilakan), Patteri predicts the by-elections wont happen.
Meanwhile, a mysterious and alluring woman is seen entering a flat where she meets an accomplice and they go through the details of an assassination which they plan to carry out very soon.The plan is successful and the Chief Minister is killed along with a few other officers. The initial investigations are headed by Vani Vishwanath who, aided by a camera found on the spot (presumably the killer womans who sneaked in the avenue using a journalist pass) she manages to track down the flat where the woman stayed for a few days. They arrest the home owner (Babu Namboodiri) but are unable to go further as there is no sign of the woman anywhere nor has any one seen her. The judge  hearing the case is unhappy that an innocent man (Babu Namboodiri) has to face the tribulations for no fault of his orders Vani Vishwanath off the case and orders the Special Investigation Team (SIT) to take over.
The SIT arrives from Delhi and is headed by Mammootty, who also happens to be Patteris son. In the beginning there is friction between Mammootty and Vani Vishwanath but later she helps him in the investigation. Further investigations go on, complicating the case. Later it turns out that the woman they were searching for is really a man. On reaching his hideout, Bharat (Mammooty), finds him dead. In the ending, Bharath reveals the shocking identity of the mastermind who had plotted the entire assassination, DGP Hariprasad (Murali). He had masterminded the plot for money as he was the one who had been pumping illicit weapons into the state for the terrorists. In the end, Hariprasad is arrested by his own department officers and sent to jail. Bharat and his team return to Delhi.

==Cast==

* Mammootty as Bharath Patteri Director SIT
* Divya Unni as Nimmi Murali as DGP Hariprasad IPS
* Vani Viswanath as SP Meena Nambiar IPS Ganeshan
* Saikumar as John DySP
* Thilakan as Patteri
* Kollam Thulasi
* Cochin Hanifa Janardanan as Pudhuval Augustine
* Azeez as CKC  Nambiar
* Jagannadhan
* N. F. Varghese as Poozhimattom Thomachan
* Balachandra Menon as Chief Minister Madhavan
* Bobby
* Gigi Gopi Kunchan
* Kunjandi
* Karamana Janardanan Nair
* Babu Namboothiri
* Praveena
* Prema
* Sadiq
* Vijayakumar
* Dileep cp

==External links==

*   at the Malayalam Movie Database

==References==
 

 
 
 
 
 
 
 