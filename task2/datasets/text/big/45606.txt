Zorba the Greek (film)
{{Infobox film
| name           = Zorba the Greek
| image          = Zorba the Greek poster.jpg
| caption        = Original film poster
| director       = Michael Cacoyannis
| producer       = Michael Cacoyannis
| screenplay     = Michael Cacoyannis
| based on       =  
| starring       = Anthony Quinn Alan Bates Irene Papas Lila Kedrova Sotiris Moustakas Anna Kyriakou
| music          = Mikis Theodorakis
| cinematography = Walter Lassally
| editing        = Michael Cacoyannis
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 142 min.
| country        = United Kingdom Greece Greek
| budget         = $783,000   IMDb. Retrieved May 19, 2013. 
| gross          = $23.5 million
}} Greek drama Cypriot Michael Zorba the Greek by Nikos Kazantzakis. The supporting cast includes Alan Bates, Lila Kedrova, Irene Papas and Sotiris Moustakas.

== Plot== Britain who bears the hallmarks of an uptight, middle-class Englishman. He is waiting at the Athens port of Piraeus on mainland Greece to catch a boat to Crete when he meets a gruff, yet enthusiastic peasant and musician named Zorba (Anthony Quinn). Basil explains to Zorba that he is traveling to a rural Cretan village where his father owns some land, with the intention of reopening a lignite mine and perhaps curing his writers block. Zorba relates his experience with mining and convinces Basil to take him along.

When they arrive at Crete, they take a car to the village where they are greeted enthusiastically by the towns impoverished peasant community. They stay with an old French war widow and cortesan named Madame Hortense (Lila Kedrova) in her self-styled "Hotel Ritz".  The audacious Zorba tries to persuade Basil into making a move on the much older Madame Hortense, but when he is understandably reluctant, Zorba seizes the opportunity, and they form a relationship.

Over the next few days, Basil and Zorba attempt to work the old lignite mine, but find it unsafe and shut it down.  Zorba then has an idea to use the forest in the nearby mountains for logging (although his specific plan is left ambiguous), however the land is owned by a powerful monastery, so Zorba visits and befriends the monks, getting them drunk. Afterwards, he comes home to Basil and begins to dance in a way that mesmerizes Basil.

Meanwhile, Basil and Zorba get their first introduction to "the Widow" (Irene Papas), a young and attractive widowed woman, who is incessantly teased by the townspeople for not remarrying, especially to a young, local boy who is madly in love with her, but whom she has spurned repeatedly. One rainy afternoon, Basil offers her his umbrella, which she reluctantly takes. Zorba suggests that she is attracted to him, but Basil, ever shy, denies this and refuses to pursue the widow.

Basil hands Zorba some money, and sends him off to the large town of Chania, where Zorba is to buy cable and other supplies for the implementation of his grand plan. Zorba says goodbye to Basil and Madame Hortense, who is by now madly in love with him.  In Chania, Zorba entertains himself at a cabaret and strikes up a brief romance with a much younger dancer.  In a letter to Basil, he details his exploits and indicates that he has found love.  Angered by Zorbas apparent irresponsibility and the squandering of his money, Basil untruthfully tells Madame Hortense that Zorba has declared his love to her and intends to marry her upon his return&nbsp;– to which she is ecstatic to the point of tears. Meanwhile, the Widow returns Basils umbrella by way of Mimithos (Sotiris Moustakas), the village idiot.

When Zorba eventually returns with supplies and gifts, he is surprised and angered to hear of Basils lie to Madame Hortense. He also asks Basil about his whereabouts the night before. That night, Basil had gone to the Widows house, made love to her and spent the night. The brief encounter comes at great cost. A villager catches sight of them, and word spreads, and the young, local boy who is in love with the Widow is taunted mercilessly about it. The next morning, the villagers find his body by the sea, where he has drowned himself out of shame.

The boys father holds a funeral which the villagers attend. The widow attempts to come inconspicuously, but is blocked from entering the church. She is eventually trapped in the courtyard, then beaten and stoned by the villagers, who hold her responsible for the boys suicide. Basil, meek and fearful of intervening, tells Mimithos to quickly fetch Zorba. Zorba arrives just as a villager, a friend of the boy, tries to pull a knife and kill the widow. Zorba overpowers the much younger man and disarms him.  Thinking that the situation is under control, Zorba asks the Widow to follow him and turns his back.  At that moment, the dead boys father pulls his knife and cuts the widows throat.  She dies at once, as the villagers shuffle away apathetically, whisking the father away. Only Basil, Zorba and Mimithos show any emotion over her murder. Basil proclaims his inability to intervene whereupon Zorba laments the futility of death.

On a rainy day, Basil and Zorba come home and find Madame Hortense waiting.  She expresses anger at Zorba for making no progress on the wedding. Zorba conjures up a story that he had ordered a white satin wedding dress, lined with pearls and adorned with real gold.  Madame Hortense presents two golden rings she had made and proposes their immediate engagement.  Zorba tries to stall, but eventually agrees with gusto, to Basils surprise.

Some time later, Madame Hortense has contracted pneumonia, and is seen on her deathbed. Zorba stays by her side, along with Basil. Meanwhile, word has spread that "the foreigner" is dying, and since she has no heirs, the State will take her possessions and money. The desperately poor villagers crowd around her hotel, impatiently waiting for her demise so they can steal her belongings. As two old ladies enter her room and gaze expectantly at her, other women try to enter, but Zorba manages to fight them off. At the instant of her death, the women re-enter Madame Hortenses bedroom en masse to steal her valued possessions. Zorba leaves with a sigh, as the hotel is ransacked and stripped bare by the shrieking and excited villagers.  When Zorba returns to Madame Hortenses bedroom, the room is barren apart from her bed (where she lies) and the bird in her cage.  Zorba takes the birdcage with him.

Finally, Zorbas elaborate contraption to transport timber down the hill is complete. A festive ceremony, including lamb on a spit is held, and all the villagers turned out. After a blessing from the priests, Zorba signals the start by firing a rifle in the air. A log comes hurtling down the zip line at a worrying pace, destroying the log itself and slightly damaging part of the contraption. Zorba remains unconcerned and gives orders for a second log. This one also speeds down and shoots straight into the sea. By now the villagers and priests have grown fearful and head for cover. Zorba remains unfazed and orders a third log, which accelerates downhill with such violence that it dislodges the entire contraption, destroying everything. The villagers flee, leaving Basil and Zorba behind.

Basil and Zorba sit by the shore to eat roasted lamb for lunch.  Zorba pretends to tell the future from the lamb shank, saying that he foresees a great journey to a big city.  He then asks Basil directly when he plans to leave, and Basil replies that he will leave in a few days.  Zorba declares his sadness about Basils imminent departure to England and tells Basil that he is missing madness.  Basil asks Zorba to teach him to dance.  Zorba teaches him the sirtaki and Basil begins to laugh hysterically at the catastrophic outcome. The story ends with both men enthusiastically dancing the sirtaki on the beach.

==Characters==
* Alexis Zorba (Αλέξης Ζορμπάς), a fictionalized version of the mine worker, George Zorbas (Γιώργης Ζορμπάς 1867–1942). 

==Cast==
* Anthony Quinn as Alexis Zorba
* Alan Bates as Basil
* Irene Papas as Widow
* Lila Kedrova as Madame Hortense
* Sotiris Moustakas as Mimithos
*   as Soul
*   as Lola
* Yorgo Voyagis as Pavlo (as George Voyadjis)
* Takis Emmanuel as Manolakas
* Giorgos Fountas as Mavrandoni (as George Foundas)
* Pia Lindström as Peasant Girl (scenes deleted)
* George P. Cosmatos as Acne Faced Boy

==Production==
Simone Signoret began filming the role of Madame Hortense; Lila Kedrova replaced her early in the production.   
 Greek island Akrotiri peninsula. The famed scene in which Quinns character dances the Sirtaki was filmed on the beach of the village of Stavros.

==Reception== The Numbers. theatrical rentals. 19th highest grossing film of 1964.

The film won three Academy Awards.
{| class="wikitable" border="1"
|-
! Award.    !! Result !! Winner
|- Best Picture My Fair Lady 
|- Best Director My Fair Lady 
|- Best Actor My Fair Lady 
|- Best Screenplay Based on Material from Another Medium ||   || Mihalis Kakogiannis  Winner was Edward Anhalt – Becket (1964 film)|Becket 
|- Best Supporting Actress ||   || Lila Kedrova
|- Best Art Direction (Black-and-White) ||   || Vassilis Photopoulos
|- Best Cinematography (Black-and-White) ||   || Walter Lassally
|-
|}

The film has an 83% rating at Rotten Tomatoes.    On both sides of the Atlantic, the film was applauded and Anthony Quinn came in for the  The Guns of Navarone.

== Cultural influence ==
The dance at the end of the film, choreographed by Giorgos Provias, formerly known as "Zorbas dance" and later called Sirtaki, has become a popular cliché of Greek dance.

Zorba the Greek was adapted into a 1968   and Lila Kedrova reprising their film roles. It opened to big box office receipts and good reviews, plus 362 performances, more than the original stage production.

The films music by Mikis Theodorakis, especially the main song, Zorbas, is well known in popular culture.  For example, the song has been used at Yankee Stadium for years to incite crowd participation during a potential rally by the home team.   A remake of Zorbas by John Murphy and David Hughes was used during the climax shootout-scene in the 1998 Guy Richie film, Lock Stock and Two Smoking Barrels.   

A short film made in Scotland in 1999, Billy and Zorba, is about a man who believes he is Zorba the Greek.

The film has been referenced in two of actress Nia Vardalos films.  In My Big Fat Greek Wedding, the family-owned restaurant her character works at is called Dancing Zorbas; this is also seen in the short-lived 2003 show My Big Fat Greek Life. In My Life In Ruins, Vardalos character Georgia expresses contempt for the film because of the Greeks love of dancing and Anthony Quinn.

A web browser flash game akin to Dance Dance Revolution was created by Pippin Barr, wherein the player competes with Zorba by taking turns performing Zorbas dance, trying to out-dance each other.   

== See also ==
* Zorbas, the theme song of the film by Greek composer Mikis Theodorakis

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  
  
 
 
 
 
 
 