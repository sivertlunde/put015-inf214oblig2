Bettada Huli
{{Infobox film|
| name = Bettada Huli
| image = 
| caption =
| director = A. V. Sheshagiri Rao
| writer =  
| based on =  Rajkumar   K. S. Ashwath  Jayanthi (actress)|Jayanthi,   M. P. Shankar   Udaykumar
| producer = Bhagavati Productions
| music = T. G. Lingappa
| cinematography =  K. Janakiram
| editing =  A. Bhaktavatsalam
| studio = Bhagavati Productions
| released =  
| runtime =  
| language = Kannada
| country = India
| budget =
}}
 Kannada action drama film directed and written by A.V. Sheshagiri Rao. 

==Plot==
Bandit, played by Udaykumar kidnaps pregnant wife of City Cop, K S Ashwath. Cops wife delivers a baby boy under the Bandits custody. However, Bandits wife conceives a baby girl, hands it over to Ashwath. Before dying, she urges Ashwath to bring her daughter, away from her father.

Bandit instills in the little boy, that he is Bandits son and his won mother is his maternal aunt. Cops son is brought up to be a Bandit, much against his will. Bandits daughter, though has a chaste upbringing, is always in awe of robbers and thieves, admiring them for their dare-devilry.

Due for his first robbery, Son meets his biological father in a fare and has unexplained attraction towards him.
He has to rob jewels from Cops daughter Jayanthi, but he hesitates to do it. Jayanthi herself offers all the jewels and plays coy, before her father.

As the movie proceeds, son, increasingly known as Bettada Kalla is pitted against the father and only one can survive. Will the son know the truth and unite his parents? This forms the crux of the movie.

== Cast == Rajkumar  Jayanthi 
* Pandari Bai 
* Udaykumar
* K. S. Ashwath
* M. P. Shankar   Narasimharaju
* B. Jaya (actress)|B. Jaya

== Soundtrack ==
The music was composed by T. G. Lingappa with lyrics by Geethapriya. All the songs composed for the film were received extremely well and considered as evergreen songs.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Aaduthiruva Modagale
| extra1 = P. B. Sreenivas
| lyrics1 = Geethapriya
| length1 = 
| title2 = Attheya Magale
| extra2 = S. Janaki, L. R. Eswari, Rudrappa
| lyrics2 = Geethapriya
| length2 = 
| title3 = Aakashada Lokadi Doora
| extra3 = P. B. Sreenivas, S. Janaki
| lyrics3 = Geethapriya
| length3 = 
| title4 = Madumagalu Naanagi
| extra4 = S. Janaki
| lyrics4 = Geethapriya
| length4 = 
| title5 = Eko Ee Dina
| extra5 = S. Janaki
| lyrics5 = Geethapriya
| length5 = 
}}

== References ==
 

== External links ==
*    
*  

 
 
 
 
 
 
 
 