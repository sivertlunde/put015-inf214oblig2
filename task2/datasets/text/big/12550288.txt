Khiladi
 

{{Infobox film 
| name           = Khiladi 
| image          = Khiladi.jpg
| caption        = Movie poster for Khiladi 
| writer         = Aadesh K. Arjun  S. Khan
| starring       = Akshay Kumar  Ayesha Jhulka  Deepak Tijori  Sabeeha  Prem Chopra  Anant Mahadevan  Johnny Lever  Shakti Kapoor
| director       = Abbas Mustan 
| producer       = Girish Jain
| distributor    = Venus Records & Tapes
| editing        = Hussain A. Burmawala
| cinematography = Thomas Xavier
| released       =  
| runtime        = 
| language       = Hindi
| music          = Jatin Lalit
| country        = India
}}
 1992 Bollywood thriller film directed by Abbas Mustan. The film was Akshay Kumars breakthrough role and also stars Ayesha Jhulka, Deepak Tijori, Sabeeha. While Prem Chopra, Shakti Kapoor, Anant Mahadevan and Johnny Lever played supporting roles. Khiladi was the first installment in the Khiladi (film series) which had Khiladi in the title and Akshay Kumar in the leading role. It was followed by  Main Khiladi Tu Anari (1994), Sabse Bada Khiladi (1995), Khiladiyon Ka Khiladi (1996), Mr. and Mrs. Khiladi (1997), International Khiladi (1999), Khiladi 420(2000) and Khiladi 786 (2012). Khiladi was critically and commercially success at the box-office and  the tenth highest grossing film of 1992.

== Synopsis ==
Two friends wage bets for money and fun. There comes a time where they race to win the biggest bet, for they know that their lives depend on it.

==Plot==
Raj Malhotra (Akshay Kumar), Boney (Deepak Tijori), Neelam Choudhary (Ayesha Jhulka) and Sheetal Nath (Sabeeha) are four pranksters in their college. Raj is younger brother of Inspector Suresh Malhotra (Shakti Kapoor), Boney probably has no relatives, Sheetal is the daughter of minister Kailash Nath (Prem Chopra) while Neelam is rich heiress with her uncle (Anant Mahadevan) as her only living relative. Raj and Neelam as well as Boney and Sheetal are romantically involved.

Raj bets habitually and has not lost yet, but when he bets that he can extort money from Kailash, the remaining gang is sceptical. As per the bet, they make Kailash believe that Sheetal is kidnapped, while in reality they have housed themselves in a cottage outside Bombay belonging to Pillai (Johnny Lever).

When Raj makes the bet, he decides that Boney will pose as a kidnapper with him. Raj puts his plan into action and Kailash gets moving. Kailash panics and decides to not to alert the police. However, one of his aides smells a rat. On deciding that Sheetal has been kidnapped, Suresh is saddled with the case. Learning this, the group panics. Kailash, however, delivers the money, ignorant of the development. Raj and Boney recover the money and go to meet the ladies, who are still sceptical. Neelam sees them coming and goes to receive them. She is surprised to see the money and the trio calls Sheetal.

Sheetal arrives with a strange look on her face and collapses dead &mdash; revealing a knife in her back. The gang is taken unawares by this unexpected development, but quickly recovers and hides Sheetals body before anyone gets a scent. They successfully evade the suspicions of Suresh and finally manage to hide Sheetals body in trunk of a car parked in a theatre parking. Sheetals body is found and her kidnappers become prime suspects. Meanwhile, some strange developments are taking place: a dancer named Julie (Kunika) blackmails Kailash. It is revealed that Julie is a well-known dancer but has been blackmailing Kailash for quite some time.

She asks a hefty amount; Kailash refuses. She threatens him and tells him to attend a dance show she has arranged. There, Kailash is supposed to give her the money as prize. Neelam gets a pleasant surprise when her uncle shows up to see how she is doing. Coincidentally, he is going to attend the function too. Raj and Boney decide to participate. On learning about Julies meeting with Kailash, Suresh thinks that there is more to the case than it appears. Boney gets on stage to dance with Julie and, in the middle of the dance, recognizes her as the woman who nearly killed them.

Realizing that those accidents were more than a coincidence, he decides to confront her after the show. After the show, Neelam goes to her hostel while Raj goes home. Meanwhile, Suresh becomes confident that the trio is involved in the case. He confronts Raj who  tells the truth. Here, Boney comes to meet Julie, only to see her dressing room door is locked. He hears some voices and peeps through a keyhole.

Julie is with a man whom Boney is unable to see. Boneys suspicion proves correct: Julie tells the man that she tried to kill the gang on his orders. She blackmails the man, too, and he kills her. At this point, Boney sees the killer and flees. The killer is alerted of Boneys presence and sends goons to kill him. Raj and Suresh try to find Boney after they realize that he is missing. Meanwhile, Boney is stabbed and about to be killed by the goons when the brothers arrive. After Raj single-handedly takes on the goons, Boney tells him that Neelam is in danger before becoming unconscious.

Raj calls Neelam and alerts her; the phone gets suddenly disconnected. He rushes to her hostel with Suresh. Neelam is alone in the hostel and is horrified to find that the watchman is already dead. Meanwhile, Raj tells the truth to Suresh. Neelam gets attacked by the killer. She doesnt get to see his face but is able to defend herself. She succeeds in throwing him out of the window, thinking that he is dead. As she is gasping for breath, the killer is revealed to be her uncle.

She is shocked. He tries to kill her. Raj and Suresh arrive in time to arrest him. While in lockup, he reveals the truth: He is not Neelams uncle. He is the manager of her estate, who was given her custody by her dying father, as Neelam had no other living relatives. He thought that he would get at least some part of the estate as her guardian, but her fathers will revealed that when she turns 18, he will lose all the estate to her. According to another clause, if she died before turning 18, he would inherit the estate.

Neelams premature death had to be natural, not foul play. Since he couldnt risk becoming a suspect, he sent Neelam to Bombay under the guise of higher education. He hired Julie to kill the gang, to write off Neelams death as an accident. When Julie failed, he took the matter in his own hands. On learning the gangs plan, he went to the cottage and killed Sheetal, mistaking her for Neelam. After his testimony, the gang is exonerated. Boney recovers and the gang gives Kailash his money back. Kailash forgives them, telling them to not to play such a rude prank with anyone.

== Cast ==
* Akshay Kumar ... Raj Malhotra
* Ayesha Jhulka ... Neelam Choudhary
* Deepak Tijori ... Boney
* Sabeeha ... Sheetal Nath
* Anant Mahadevan ... Neelams Uncle
* Johnny Lever ... Anna Pillai
* Prem Chopra... Kailash Nath
* Shakti Kapoor ... Suresh Malhotra
* Beena Banerjee ... Suresh Malhotras Wife
* Kunika ... Julie
* Sharat Saxena ... Bahadur Singh
* Guddi Maruti ... Neelam and Sheetals College Friend "Wada Pao" 
* Dinyar Contractor ... College Principal
* Tinu Anand... College Professor P.K. Mare
* Amrutlal Patel ... Police Constable 
* Manmauji ... College Canteen Manager
* Harpal Singh ... Rajs College Friend
* Sharad Sankla ... Rajs College Friend "Charlee"
* Some guy ... Police Inspector on the Highway
* Ghanshyam Rohera ... Librarian
* Suresh Bhagwat ... Kailash Naths Servant
* Vikas Anand .... Bank Manager

==Music==
Jatin-Lalit composed the soundtrack which featured in the top 5 best selling albums of 1992.  Despite the music being a huge success, directors Abbas-Mustaan never repeated Jatin-Lalit for any of their future projects.
{{Infobox album |  
 Name = Khiladi
| Type = Soundtrack
| Artist = Jatin Lalit
| Cover =  1992
| Recorded =  Feature film soundtrack
| Length = 
| Label =  Venus Records & Tapes
| Producer = Jatin Lalit
| Reviews =

| Last album = Jo Jeeta Wohi Sikandar  (1992)
| This album = Khiladi (1992)
| Next album = Raju Ban Gaya Gentleman  (1992) 
}}
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Wada Raha Sanam" 
| Abhijeet Bhattacharya|Abhijeet, Alka Yagnik
| 06:02
|-
| 2
| "Khud Ko Kya Samajhti Hai"
| Abhijeet, Udit Narayan, Kavita Krishnamurthy, Sapna Mukherjee
| 06:35
|-
| 3
| "Hoke Mann Aaj Magan"
| Asha Bhosle, Abhijeet, Udit Narayan
| 05:07
|-
| 4
| "Kya Khabar Thi Jaana"
| Asha Bhosle, Abhijeet
| 05:00
|-
| 5
| "Dekha Teri Mast"
| Asha Bhosle, Kumar Sanu
| 05:48
|-
| 6
| "Tu Shama Main Parwana Tera"
|Abhijeet, Alisha Chinai
| 07:20
|}

==References==
 

==External links==
*  

 
 

 
 
 
 
 