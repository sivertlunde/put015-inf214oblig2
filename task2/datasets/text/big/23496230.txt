Baba Ramdev (film)
{{Infobox film
| name           = Baba Ramdev
| image          =
| image_size     =
| caption        =
| director       = Nawal Mathur Manibhai Vyas
| producer       = Ramraj Nahata
| writer         = Nawal Mathur
| narrator       = Nawal Mathur Mahipal Anita Ratna Deepak Deepak Madhumati
| music          = Pandit Shivram
| cinematography = Narottam
| editing        = I M Kunu
| studio         = Ranglok
| distributor    =
| released       = 1963
| runtime        = 144 minutes
| country        = India Rajasthani
| budget         =
| gross          =
}}
 directed by Nawal Mathur and Manibhai Vyas, 
 {{cite web
 |url=http://www.citwf.com/film25885.htm
 |title=Baba Ramdev
 |publisher=Complete Index To World Film
 |accessdate=2009-07-14}}  and based on the life of the Hindu folk deity Ramdev Pir. 
 {{cite book
 |last=Pauwels
 |first=Heidi Rika Maria
 |title=Indian literature and popular cinema: recasting classics
 |editor=Heidi Rika Maria Pauwels
 |publisher=Routledge
 |year=2007
 |edition=illustrated
 |isbn=0-415-44741-0
 |oclc=9780415447416
 |url=http://books.google.com/books?id=LiXU4ihgMpgC&pg=PT133&dq=%22Baba+Ramdev%22,+Rajasthani+film&ei=6dpcSrDLE4TmlATTmcGRCA}} 

==Background==
The film was made in the 1960s and became a big commercial success, being considered a hit for Rajasthani cinema. 
 {{cite news
 |url=http://www.screenindia.com/old/fullstory.php?content_id=10899
 |title=Rajasthan: Neelu turns producer
 |last=Garg
 |first=M. P.
 |date=5 August 2005
 |publisher=Screen Weekly
 |accessdate=2009-07-15}}  
 {{cite web |url=http://www.rajasthantour4u.com/artists/rajasthanartist.html#link6
 |title=Rajasthani Art and Culture: Flim Art of Rajasthan
 |work=rajasthantour4u.com
 |accessdate=2009-07-14}}   This was a milestone in the history of the Rajasthani movie industry. 
 {{cite book
 |last=Ashish Rajadhyaksha and Paul Willemen
 |title=Encyclopaedia of Indian cinema
 |publisher=Oxford University Press
 |date=April 1995
 |pages=349
 |isbn=0-85170-455-7
 |url=http://books.google.com/books?ei=C4xaSveyJpOilQSPhLmtDQ&client=firefox-a&id=jOtkAAAAMAAJ&dq=Encyclopaedia+of+Indian+cinema+By+Ashish+Rajadhyaksha&q=Baba+Ramdev#search_anchor
 |accessdate=13 July 2009}} 

The popular bhajan songs "Khamma Khamma" and "Runecha ra Dhaniya" are from this movie.

===Re-release===
88 Rajasthani films had been produced in the period of mid-1942 to 2004. With the emergence of VCD and DVD technology, films which had otherwise been unavailable for years have become marketable and are being re-released in video format. Baba Ramdev was re-released by Modern Videos of Ajmer. 
 {{cite web
 |url=http://www.screenindia.com/old/fullstory.php?content_id=9108
 |title=Rajasthan films regain their value, importance
 |last=Garg
 |first=M. P.
 |date=24 September 2004
 |publisher=Screen Weekly
 |accessdate=2009-07-14}} 

==Cast== Mahipal
*Anita Guha
*Ratna Pathak
*Deepak Tijori
*Madhumati
*Dalda
*Sarita 
*Lalita Desai
*B. M. Vyas
*Mohan Modi

==References==
 

==External links==
* 

 
 
 
 


 