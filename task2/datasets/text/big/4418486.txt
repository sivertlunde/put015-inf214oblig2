Three Sisters (1970 Olivier film)
{{Infobox film
| name           = The Three Sisters
| image          = Three Sisters 1970 DVD cover.jpg
| caption        = DVD Cover
| director       = Laurence Olivier John Sichel
| producer       = John Goldstone
| writer         = Anton Chekhov Moura Budberg
| starring       = Alan Bates Laurence Olivier Joan Plowright Derek Jacobi
| music          = William Walton
| cinematography = Geoffrey Unsworth
| distributor    = British Lion Films
| released       = 1970
| runtime        = 165 min. UK
| English
| budget         =
}} play by Anton Chekhov. Olivier also directed, with co-director John Sichel. The film was based on a theatre production that Olivier directed at the Royal National Theatre in 1967. It had its U.S. release as part of the American Film Theatre series in 1974. It was the final feature film directed by Olivier.

==Cast==
*Jeanne Watts ....  Olga 
*Joan Plowright ....  Masha 
*Louise Purnell ....  Irina 
*Derek Jacobi ....  Andrei 
*Sheila Reid ....  Natasha 
*Kenneth MacKintosh ....  Kulighin 
*Daphne Heard ....  Anfisa  Judy Wilson ....  Serving maid 
*Mary Griffiths ....  Housemaid 
*Ronald Pickup ....  Baron Tusenbach 
*Laurence Olivier ....  Dr. Ivan Chebutikin 
*Frank Wylie ....  Maj. Vassili Vassilich Solyony 
*Alan Bates ....  Col. Vershinin  Richard Kay ....  Lt. Fedotik

== See also == The Three Sisters, a BBC TV version also made in 1970 Three Sisters, a Russian version

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 

 