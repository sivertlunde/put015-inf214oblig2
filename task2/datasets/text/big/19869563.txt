Welcome Aboard Toxic Airlines
{{Infobox film
| name           = Welcome Aboard Toxic Airlines
| image          = Welcome Aboard Toxic Airlines poster.jpg
| image size     =
| caption        = Theatrical poster Tristan Loraine
| producer       = Tristan Loraine
| writer         = Tristan Loraine
| music          = Kate Garbutt
| cinematography = Anna Carrington Benjamin Foot  Trinity Greer Robin Lambert Lesley Pinder
| editing        = Tristan Loraine Mike Butler
| distributor    = DFT Enterprises (UK)
| released       =  
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English
| budget         = $400,000 (estimated)
| gross          =
| preceded by    =
| followed by    =
}}
Welcome Aboard Toxic Airlines is a 2007 British documentary film directed and produced by former airline captain Tristan Loraine on aerotoxic syndrome.

==Synopsis==
This documentary shows that for nearly fifty years, airline passengers and crews have been supplied with unfiltered air, called bleed air, taken directly from the engines. It shows how pressure groups have shown that this air supply sometimes becomes contaminated with neurotoxins, carcinogens and other hazardous chemicals.    In aviation industry-speak this contamination is called a "fume event".  Many have been reported some filling the passenger cabin with smoke and fumes. 

==Global Cabin Air Quality Executive ==
Former British Airways Captain Tristan Loraine, who produced the film, is co-chair of Global Cabin Air Quality Executive (GCAQE).   
{{cite news
| first =John
| last = Ingham
| title= Scandal of toxic fumes in all Jets
| newspaper =Sunday Express
| date = 2012 url = http://www.express.co.uk/news/uk/39339/Scandal-of-toxic-fumes-in-all-Jets }} 

==Contrary opinions==
Research by the UK government did not find a link to long-term health problems. The UK Parliament Select Committee on Science and Technology concluded in 2000 that the concerns about significant health risks were not substantiated.  

In 2008 Michael Bagshaw, the former Head Doctor at British Airways, and later an advisor to Airbus, claimed that no peer-reviewed, recorded cases of neurological harm in humans followed low-level exposure to the organophosphate tricresyl phosphate (TCP), which is used as a lubricant in jet engines. 

==See also==
* Aerotoxic Association

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 