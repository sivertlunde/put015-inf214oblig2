Get-Rich-Quick Wallingford (1916 film)
 
 
{{Infobox film
| name           = Get-Rich-Quick Wallingford
| image          = 
| caption        = 
| director       = Fred Niblo W. J. Lincoln
| producer       = 
| writer         = W. J. Lincoln Fred Niblo
| based on       = George M. Cohan (play) George Randolph Chester (story)
| starring       = Fred Niblo Enid Bennett
| cinematography = Maurice Bertel
| editing        = 
| distributor    = 
| studio         = J.C. Williamson Ltd (film company)|J.C. Williamson Ltd
| released       =   
| runtime        = 4,000 feet  (approx 60 mins)
| country        = Australia 
| language       = Silent with English intertitles
| budget         = 
}}
Get-Rich-Quick Wallingford is a 1916 Australian   as a director and is considered a lost film.

==Plot==
Two swindlers, Blackie Daw (Henry Carson Clarke) and J. Rufus Wallingford (Fred Niblo), arrive in Battlesburg, Iowa, and con the local townsfolk that they are wealthy businessmen. They use the towns money to establish plans for a factory to produce covered carpet tacks and set off a major real estate boom. They are about to leave town with their money when they receive a genuine order for a large supply of tacks. They decide to marry local girls and settle down in Battlesburg.  
 

==Cast==
* Fred Niblo as J. Rufus Wallingford
* Henry Carson Clarke as Blackie Daw
* Enid Bennett as Fanny
* Eddie Lamb
* Pirie Bush
* Sydney Stirling
* Charles Clary
* Kathryn Williams
* Max Figman
* Burt McIntosh

==Original Play==
Williamsons often imported American plays and American actors to star in them. Fred Niblo came out to Australia in 1912 with his wife Josephine Cohan, sister of George M. Cohan, and stayed for three years, appearing in a number of plays.  One of these was Get-Rich-Quick-Wallingford, by Cohan, which proved enormously popular with Australian audiences.  

==Production==
W. J. Lincoln adapted the play into a script in March 1915 and shooting began the following month. Filming took place at the J.C. Williamsons Studio, opposite Her Majestys Theatre in Melbourne,  and on location, in particular at the Junction Hotel in St Kilda. 

Lincoln was the original director and appears to have definitely been in charge during the location shoot. However he had to pull out during production because of his alcoholism and Niblo stepped in.  While filming, Niblo was also rehearsing a play in the morning and appearing in Seven Keys to Baldpate at night. He would have likely been heavily assisted by his stage manager Maurice Dudley and assistant stage manager Dick Shortland, as well as cinematographer Maurice Bertel. 

The cast includes Enid Bennett, who understudied Josephine Cohan on stage and went with Niblo and her to America when they returned there in 1915. Bennett later married Niblo on Cohans death in 1916.  Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p59 

==Reception==
The film was not a success at the box office, with critics noting in particular the absence of the plays wisecracking dialogue. 

However the Sunday Times did describe the film as "one of the finest ever taken in Australia". 

Punch said that "it is not always that a popular stage favourite is able to make a success upon the screen. Mr. Niblos extraordinary personality has assisted him greatly, and the film version is in many respects superior to the stage representation." 

The Moving Picture World called the movie "very poor". 

This contributed to the decision of J.C. Williamson to wind up their filmmaking arm. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive
*  at Project Gutenberg
*  at AustLit

 
 

 
 
 
 
 
 
 
 
 