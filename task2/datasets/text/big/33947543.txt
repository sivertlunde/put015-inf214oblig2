The Curse of El Charro
 
{{Infobox film
| name           = The Curse of El Charro
| image          = 
| caption        = DVD cover
| director       = Rich Ragsdale
| producer       = Kevin Ragsdale Ryan R.Johnson  Andrew Bryniarski
| writer         = Ryan R. Johnson
| starring       = Danny Trejo Drew Mia Andrew Bryniarski Kathryn Taylor Heidi Androl  KellyDawn Malloy
| music          = Rich Ragsdale
| cinematography = Jacques Haitkin
| editing        = Michael Amundsen
| distributor    = Paramount Home Video  American World Pictures  GoDigital Media Group  Millenium Storm
| released       =  
| runtime        = 90 mins.
| country        = United States
| language       = English
| budget         =$200,000
}}
The Curse of El Charro is a 2005 horror film starring Danny Trejo. The Curse of El Charro was released on DVD by Paramount Home Video in 2006.

==Plot==
The film opens in 19th century Mexico, where El Charro (Andrew Bryniarski) acts as a wealthy, but diabolical land baron who falls madly in love with a sweet, innocent ancestor of the protagonist Maria. She scorns him, which prompts El Charro to kill everyone she cares about. However, Marias ancestor still refuses El Charro, who decides to put a curse on Marias family line. Many years after, we are brought to the present day 21st century, where we are introduced to Maria (Drew Mia) and her friend Chris (Heidi Androl), who are roommates in college. It is a Friday or so, and Maria, who we learn is suffering from repeating nightmares of her sisters suicide, is coerced to go with Chris, and her friends, Tanya (Kathryn Taylor) and Rosemary (KellyDawn Malloy) to Chris uncles Arizona cottage for the weekend. However upon reaching Arizona, the group is persistently attacked by El Charro in the form of revant spirit with a machete. Eventually, El Charro kills every one of the girls except for Maria, who races toward a shrine of the archangel Michael (James Intveld). Using his heavenly abilities, Michael kills El Charro, and the curse is thus destroyed as well.

==Cast==
{| class="wikitable" width="25%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Andrew Bryniarski || El Charro
|-
| Danny Trejo || Voice of El Charro
|-
| Andrew Mia || Maria
|-
| Kathryn Taylor || Tanya
|-
| Heidi Androl || Christina
|-
| KellyDawn Malloy || Rosemary
|-
| Scott Greenall|| as Himself
|-
| James Intveld || Archangel Michael
|}
*Gary Bullock has a small role as "Purgatory Bartender".

==References==
 
 

==External links==
*  
*  

 
 
 
 
 
 
 
 

 
 