Dhanam (2008 film)
 
{{Infobox film
| name = Dhanam
| image = Dhanam Poster.jpg
| caption = Theatrical release poster
| director = G. Shiva
| writer = G. Shiva
| producer = S. Kousalyarani Prem Girish Karnad
| cinematography = Srinivas Devamsam
| music = Ilayaraja

| editing = B. Lenin

Co-Director Jayakumar

 AssoDirector ShivaMuthu

| distributor = Sri
 Nandhimedu Selliaamman  Movies
| released = 2008
| runtime = 154 minutess
| country = India
| language = Tamil
| budget =
| gross = 
}}
Dhanam ( ) is a 2008 Tamil drama film written and directed by G. Shiva. The film stars Sangeetha, Kota Srinivasa Rao, Ashish Vidyarthi and Manobala. The films score and soundtrack are composed by maestro Ilayaraja.  The film is the last project Sangeetha completed before her marriage.

==Plot==
The film opens with a cop (Ashish Vidyarthi) and his assistant (Manobala) searching for a prostitute named Dhanam (Sangeetha) in the streets of Hyderabad, India|Hyderabad. While they ask the public around the streets about Dhanam a flashback breaks out.

The story goes five years back from the present day. Ananth (Prem), a young student from a Hindu Brahmin family based in Kumbakudam comes to Hyderabad to study medicine. There he meets Dhanam. He falls in love with her and begins to follow her for several days. She suspects that Ananth came to her to do business and takes him to her place. They both have sex for money and this begins to happen frequently.

One day Ananth opens up his feelings to her, but she refuses his propose, let the public in the street beat him up and walks away. Dhanams close friends tell her to stop living as a prostitute and start over her life by live with Ananth. They both meet at a temple and have an argument. Dhanam finally comes with a decision that if Ananths family accept her she will marry him.

The next day they both go to Kumbakudam to visit Ananths family. After hearing that Dhanam is a prostitute and that Ananth wants to marry her, his family refuse to accept the marriage. A priest named Vedhagiri (Kota Srinivasa Rao|Rao) who is a good family friend of Ananths family forces them to accept the marriage by telling them that the horoscopes match. The family immediately go to Hyderabad and arrange everything for the marriage. They both get married the next day. This is a trap by Vedhagiri. Vedhagiri who had earlier asked Dhanam in Hyderabad to have sex for money uses this situation to get closer to Dhanam, but she tells him to stay away.

Some days later, Ananth and his family ask her why she began with prostitution. She says that her mother used to be a prostitute. Dhanam was the only child for her. Her mother wanted her to not follow her tracks and kept her as virgin. One day when her mother was very ill, a man fooled Dhanam by saying that he will take her to the hospital so that they can bring the doctor home, but raped her on the way. She began to have sex for money to save her sick mother, but unfortunately her mother died some days later. Since Dhanam was too young and she did not have anyone to take care of her, she began with prostitution and settled down in Gandhi Theru. After hearing this Ananth and his family could understand her pain and ask for apology for not understanding her earlier.

The next day Dhanam gets pregnant. During her pregnancy Vedhagiri tries to make a deal with Dhanam, but she refuses again. This begins to happen often. Dhanam keeps this secretly from her husband and in-laws because they have a lot of respect for him. Dhanam later gives birth to a baby girl.

After the birth, the family begins to go through a hard time. They ask Vedhagiri to check the horoscope and find out why this happens. Vedhagiri says that the time the baby was born was not good. He says that the baby has to die or everyone in the family will die within a year. The family keep this secret for Dhanam and kill the baby by feed it with poison. Some days after the death of her child, Dhanam finds out that her in-laws were behind of the murder. Dhanam who saw her daughter as her mother, becomes psycho. The flashback ends with that Dhanam kills her husband, her in-laws and Vedhagiri by give them food with poison.

The cop and his assistant go to the cop and the judge who closed the case of the murder of Dhanams husband, her in-laws and the priest and have a conversation about the case. The cop and the assistant asked the cop and the judge about their opinion. They both said that Dhanam is innocent. She just showed the anger a mom has. The film ends with that the cop and the assistant close the case and let Dhanam go.

==Cast==
* Sangeetha as Dhanam Prem as Ananth
* Kota Srinivasa Rao as Vedhagiri
* Ashish Vidyarthi as Cop #1
* Manobala as Cop #2
* Girish Karnad as Ananths father
* Ilavarasu as Dhanams friend
* Karunas as Ananths uncle

==Soundtrack==
{{Infobox album
| Name        = Dhanam
| Type        = soundtrack
| Artist      = Ilayaraja
| Cover       =
| Released    = 2008
| Recorded    =
| Genre       = Film soundtrack
| Length      =
| Label       =
| Producer    =
| Last album  = Maya Kannadi (2007)
| This album  = Dhanam (2008)
| Next album  = Inimey Nangathan (2008)
}}

The album includes seven songs composed by Ilayaraja. The lyrics were provided by Vaali, Muthulingam, Vishali Kannadhasan, Pazhani Bharathi and M.U. Metha.

{| class="wikitable"
|-
! No !! Song title !! Singers !! Lyrics
|-
| 1 || "Kattilukku Matthumthana Pombala" || Ilayaraja || Muthulingam
|- Rita || Vaali
|-
| 3 || "Ulagam Kidakkuthu" || Sunitha Sarathy || Vaali
|- Karthik || Vaali
|-
| 5 || "Kootthu Onnu " || Ilayaraja, Beji|| Pazhani Bharathi
|-
| 6 || "Unakkul Irukkiren " || Sriram Parthasarathy, Billsandey || M.U. Metha
|-
| 7 ||"Kannanukku Enna" || Bhavatharani, Sriram Parthasarathy, Prasanna || Vishali Kannadhasan
|}

==Reception==
Dhanam received mixed reviews, largely praising Sangeethas performance. Indiaglitz said "Dhanam is a bold venture that is certainly a whiff of air from the rest of conventional commercial masala movies".  Behindwoods rated the film 1.5 stars out of 5, saying "Dhanam is a movie which does not fulfill the scope of its theme. Also, the subject will be seen by some groups as controversial. This, coupled with a pretty narrow cross section of the audience who will accept such bold themes, stacks the odds against Dhanam".  Sify wrote:"Shiva is bold to take such a subject, but his handling, slow pace and too many songs makes Dhanam excruciatingly laboured, the film is dark and depressing".  Rediff wrote:"Altogether a washed out product that passes itself off as a serious film, but instead takes the viewer for a disappointing ride". 

==References==
 

 
 
 
 
 