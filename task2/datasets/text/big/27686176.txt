All Hat
{{Infobox film
| name = All Hat
| image = All Hat.jpg
| image_size =
| caption =
| director = Leonard Farlinger
| producer = Jennifer Jonas
| writer = Brad Smith ( ) Luke Kirby Stephen McHattie Keith Carradine Noam Jenkins Lisa Ray Rachael Leigh Cook
| music = Bill Frisell
| cinematography = Paul Sarossy
| editing = Glenn Berman
| studio = New Real Films
| distributor = Odeon Films (Canada) Screen Media Ventures
| released = September 11, 2007 ( ) May 27, 2008 (US DVD)
| runtime = 89 minutes
| country = Canada
| language = English
| budget = $Canadian dollar|CAD5,000,000
| gross = $15,198
}} western comedy Luke Kirby, Keith Carradine, Noam Jenkins, and Lisa Ray.

==Plot== Luke Kirby), is fresh out of prison. Returning home, he discovers the countryside of his youth transformed. Urban development crawls across the pastoral fields like a rash. Determined to stay out of trouble, Ray heads to the farm of his old friend Pete (Keith Carradine), a Texan cowboy, whose debts are growing faster than his corn.

Sonny Stanton (Noam Jenkins), the heir to a thoroughbred dynasty, is buying the entire concession of farmland to build a golf course. One of the farms he’s after belongs to Etta Parr (Lisa Ray), Ray’s old flame. Seems she’s the only one brave enough to stand in Sonny’s way

Ray hooks up with Chrissie Nugent (Rachael Leigh Cook), a kick-ass jockey and tries to steer clear of Sonny. When a million-dollar thoroughbred goes missing from the Stanton Stables, Sonny gets desperate and forces the sale of the community’s remaining farms. Ray reacts by coming up with a scheme to stop Sonny once and for all. One false move will land Ray back in jail.

==Cast== Luke Kirby as Ray Dokes
* Stephen McHattie as Earl Stanton
* Keith Carradine as Pete Culpepper
* Noam Jenkins as Sonny Stanton
* Lisa Ray as Etta Parr
* Rachael Leigh Cook as Chrissie Nugent
* David Alpay as Paulie Stanton
* Ernie Hudson as Jackson Jones
* Joel Keller as Dean Calder Graham Greene as Jim Burns
* Gary Farmer as Billy Caan
* Michelle Nolden as Gena Stanton

==Production== Canadian locations:
* Ontario
* Fort Erie, Ontario and its racetrack
* Hamilton, Ontario

==Release and reception==
  wider release on April 19, 2008 in Okanagan. The film was direct-to-video|direct-to-DVD in North America on May 27, 2008. 

All Hat earned only $15,198 on a budget of $Canadian dollar|CAD5,000,000. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 