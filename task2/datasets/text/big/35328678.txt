The Rainbow Princess
  
  Ann Pennington’s second appearance on celluloid. 

==Reception==
The Moving Picture World, 1916

 After her very successful debut in Susie Snowflake it was decided to star Miss Pennington in a circus story to be called The Rainbow Princess which is being staged under the direction of J. Searle Dawley. In this picture Miss Pennington plays a little waif who has been adopted by the wife of the proprietor of a circus and is forced to do a great deal of the mean work around the place in addition to learning to do tricks with the animals. Of course there is a lover among the men in the troupe but The Princess, realizing that he is not quite sincere in his attentions, has the good sense to refuse to accept his attentions. She later proves to be not at all the waif that she was thought to be and—but the story is one to be seen on the screen.

The production of this photoplay at this particular time has caused many unexpected difficulties to be placed in the path of Director Dawley, because of the strict quarantines which have been placed upon itinerant citizens because of the paralysis plague. As a result of these numerous obstacles, Mr. Dawley was forced to arrange with one of the circuses which was on Long Island to have it apparently disband and travel back to New York in small units, with the Famous Players studio as their rendezvous. Then the tent was set up in a large vacant lot on the west side and the scenes were taken.
Miss Pennington, who is a remarkably clever athlete and is a trained acrobat, has already done some very startling feats in the "show" and she predicts that she will accomplish even more before the end of the picture  

Forest Leaves, 1916 Hula Hula Dance and is her captivating self throughout the entire picture.  

==Cast== Ann Pennington	...  Hope
*William Courtleigh, Jr.  ...  Warren Reynolds
*Augusta Anderson	...  Edithe Worthington
*Grant Stewart ...  Judge Daingerfield
*Charles Sutton  ...  Pop Blodgett
*Harry Lee ... 	Dave, his son
*Eddie  Sturgis  ...  Joe, his son (as Edwin Sturgis)
*Clifford Grey ... 	George Waters (as Clifford Gray)
*Herbert Rice	... Monsieur Paul
*Queen Pearl	...  Mademoiselle Fifi
*Amy Manning ... Rose, the fat lady
*Carl Gordon	...  Simon, the skeleton
*Walter D. Nealand	...  Hawkes
Source, IMDb.com  

==References==
 

 
 