Sedige Sedu
{{Infobox film
| name           = Sedige Sedu
| image          =
| caption        = 
| director       = A V Sheshagiri Rao
| producer       = K Krishnaiah Naidu
| writer         = A L A Naidu
| screenplay     =  Jayanthi T. Narasimharaju
| music          = Chellapilla Satyam
| cinematography = K S Mani
| editing        = 
| studio         = 
| distrubutor    = 
| released       =  
| country        = India Kannada
}}
 1970 Cinema Indian Kannada Kannada film, Narasimharaju in lead roles. The film had musical score by Chellapilla Satyam.   

==Cast==
*Udaykumar Jayanthi
*T. N. Balakrishna Narasimharaju
*B. Ramadevi

==Soundtrack==
The music was composed by Satyam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Muddu Maava || L. R. Eswari || Chi. Udaya Shankar || 03.30
|}

==References==
 

==External links==
*  

 
 
 


 