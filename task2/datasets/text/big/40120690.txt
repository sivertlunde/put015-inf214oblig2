Three Word Brand (1921 film)
{{Infobox film
| name           =Three Word Brand
| image          =Three Word Brand (1921) 1.jpg
| caption        =Film still with Novak and Hart
| director       =Lambert Hillyer
| producer       =William S. Hart
| writer         =Lambert Hillyer (adaptation) Will Reynolds (story)
| starring       =William S. Hart Jane Novak
| music          = 
| cinematography =Joseph H. August
| editing        = 
| studio         =William S. Hart Productions
| distributor    =Paramount Pictures 
| released       = 
| runtime        =7 reels
| country        =United States
| awards         =
| language       =Silent (English intertitles)
| budget         = 
}} western film distributed by Paramount Pictures starring William S. Hart and Jane Novak and directed by Lambert Hillyer. 

==Synopsis==
After Ben Trego sacrifices his life for his sons during an Indian attack, the two sons lead separate and different lives. One becomes a cowboy and the other the governor of Utah. 

==Cast==
* William S. Hart - Three Word Brand / Governor Marsden / Ben Trego
* Jane Novak - Ethel Barton
* S.J. Bingham - George Barton
* J. Gordon Russell - Bull Yeates
* Ivor McFadden - Solly
* Herschel Mayall - Carroll
* Collette Forbes - Jean
* George C. Pearce - John Murray
* Leo Willis - McCabe

==Survival status==
A copy of the film is in the Library of Congress and Museum of Modern Art film archives. 

==References==
 

== External links ==
 
*  

 
 
 

 
 