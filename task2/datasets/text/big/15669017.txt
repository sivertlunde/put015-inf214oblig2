The Trail of the Tiger
 
 
{{Infobox film
| name           = The Trail of the Tiger
| image          =
| caption        =
| director       = Henry MacRae
| producer       =
| writer         = Courtney Ryley Cooper Carl Krusada
| starring       = Jack Dougherty Frances Teague
| cinematography =
| editing        = Universal Pictures
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}

The Trail of the Tiger is a 1927 American film serial directed by Henry MacRae. The film is considered to be lost film|lost.   

==Cast==
* Jack Dougherty - Jack Stewart (as Jack Daugherty)
* Frances Teague - Trixie Hemingway
* Jack Mower - Tiger Jordan
* John Webb Dillon - John Hemingway
* Charles Murphy - Rube Murphy
* Billy Platt - Babs, the Dwarf (as William Platt) Jack Richardson - Ned Calvert

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 