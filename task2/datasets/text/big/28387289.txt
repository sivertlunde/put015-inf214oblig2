The Girl with the Dragon Tattoo (2011 film)
 
 
{{Infobox film
| name = The Girl with the Dragon Tattoo
| image = The Girl with the Dragon Tattoo Poster.jpg
| alt =
| caption = Theatrical release poster
| director = David Fincher
| producer = {{Plain list |
* Scott Rudin 
* Ole Søndberg 
* Søren Stærmose 
* Ceán Chaffin
}}
| screenplay = Steven Zaillian
| based on =  
| starring = {{Plain list |
* Daniel Craig 
* Rooney Mara 
* Christopher Plummer 
* Stellan Skarsgård 
* Steven Berkoff 
* Robin Wright 
* Yorick van Wageningen 
* Joely Richardson
* Geraldine James
* Goran Višnjić
* Donald Sumpter
* Ulf Friberg 
}}
| music = {{Plain list |
*Trent Reznor
*Atticus Ross
}}
| cinematography = Jeff Cronenweth
| editing = {{Plain list |
*Kirk Baxter
*Angus Wall
}}
| studio = {{Plain list | Scott Rudin Productions  Yellow Bird Films 
}}
| distributor = {{Plain list |
*Columbia Pictures
*Metro-Goldwyn-Mayer
}}
| released =  
| runtime = 158 minutes    
| country = United States Sweden
| language = English
| budget = $90 million 
| gross = $232.6 million   
}}
 mystery thriller thriller film novel of the same name by Stieg Larsson. This film adaptation was directed by David Fincher and written by Steven Zaillian. Starring Daniel Craig and Rooney Mara, it tells the story of journalist Mikael Blomkvists (Craig) investigation to find out what happened to a woman from a wealthy family who disappeared forty years prior. He recruits the help of computer hacker Lisbeth Salander (Mara).

Sony Pictures Entertainment began development on the film in 2009, the year the The Girl with the Dragon Tattoo (2009 film)|first, highly acclaimed adaptation of the novel entered cinemas. It took the company a few months to obtain the rights to the novel, while recruiting Zaillian and Fincher. The casting process for the lead roles was exhaustive and intense: Craig faced scheduling conflicts and a number of actresses were sought for the role of Lisbeth Salander. The script took over six months to write, which included three months of analyzing the novel.
 Best Film Editing.   

== Plot ==
In Stockholm, Sweden, journalist Mikael Blomkvist, co-owner of Millennium magazine, has lost a libel case brought against him by businessman Hans-Erik Wennerström. Lisbeth Salander, a brilliant but troubled investigator and Hacker (computer security)|hacker, compiles an extensive background check on Blomkvist for business magnate Henrik Vanger, who has a special task for him. In exchange for the promise of damning information about Wennerström, Blomkvist agrees to investigate the disappearance and assumed murder of Henriks grandniece, Harriet, 40 years ago. After moving to the Vanger familys compound, Blomkvist uncovers a notebook containing a list of names and numbers that no one has been able to decipher.

Salander, who is under state legal guardianship due to diagnosed mental incompetency, is appointed a new guardian, lawyer Nils Bjurman, after her previous guardian has a stroke. Bjurman abuses his authority to extort sexual favors from Salander and then rapes her, not realizing she has a hidden video camera in her bag. At their next meeting she stuns him with a Taser, rapes him with a dildo and marks him as a rapist with a tattoo on his chest and stomach. Using her video recording she threatens blackmail, insisting that he write a glowing progress report and allow her full control of her money.
 Biblical names; many of the Vangers are known antisemitism|antisemites. During the investigation, Salander and Blomkvist become lovers. Henriks brother Harald identifies Martin, Harriets brother and operational head of the Vanger empire, as a possible suspect. Salanders research uncovers evidence that Martin and his deceased father, Gottfried, committed the murders.

Blomkvist breaks into Martins house to look for more clues, but Martin catches him and prepares to kill him. Martin brags of having killed women for decades, but denies killing Harriet. Salander arrives, subdues Martin and saves Blomkvist. While Salander tends to Blomkvist, Martin flees. Salander, on her motorcycle, pursues Martin in his SUV. He loses control of his vehicle on an icy road and dies when it catches fire. Salander nurses Blomkvist back to health, and tells him that she tried to kill her father when she was 12. Blomkvist deduces that Harriet is still alive and her cousin Anita likely knows where she is. He and Salander monitor Anita, waiting for her to contact Harriet. When nothing happens, Blomkvist confronts her, deducing that Anita is Harriet herself. She explains that her father and brother had sexually abused her for years, and that Martin saw her kill their father in self-defense. Her cousin Anita smuggled her out of the island and let her live under her identity. Finally free of her brother, she returns to Sweden and tearfully reunites with Henrik.

As promised, Henrik gives Blomkvist the information on Wennerström, but it proves worthless. Salander hacks into Wennerströms computer and presents Blomkvist with evidence of Wennerströms crimes. Blomkvist publishes an article that ruins Wennerström, who flees the country. Salander hacks into Wennerströms bank accounts and, travelling to Switzerland in disguise, transfers two billion euros to various accounts. Wennerström is found murdered. Salander reveals to her former guardian that she is in love with Blomkvist. On her way to give Blomkvist a Christmas present, Salander sees him and his longtime lover and business partner Erika Berger walking together happily. She discards the gift and rides away.

== Cast ==
* Daniel Craig as Mikael Blomkvist:
:A co-owner for Swedish lifestyle magazine Millennium, Blomkvist is devoted to exposing the corruptions and malfeasances of government, attracting infamy for his tendency to "go too far".    Craig competed with Brad Pitt, George Clooney, Viggo Mortensen, and Johnny Depp as candidates for the role.       Initial concerns over schedule conflicts with the production of Cowboys & Aliens (2011) and Skyfall (2012) prompted Craig to postpone the casting process.   Given the uncertainty surrounding Skyfall following Metro-Goldwyn-Mayers bankruptcy, Sony Pictures Entertainment and DreamWorks worked out a schedule and Craig agreed to take the part.    The British actor was required to gain weight and adopted a neutral accent to befit Stockholms worldly cultural fabric. Having read the book amid its "initial craze", Craig commented, "its one of those books you just dont put down" and  "theres just this immediate feeling that bad things are going to happen and I think thats part of why theyve been so readable for people." 
* Rooney Mara as Lisbeth Salander:
 David Fincher |align=right| width=250px}} goth with a dash of Sadomasochism|S&M temptress" by Lynn Hirschberg of W (magazine)|W,  Mara participated in a formal screening and was filmed by Fincher on a subway in Los Angeles in an effort to persuade the executives of Sony Pictures that she was a credible choice. 
* Yorick van Wageningen as Nils Bjurman: sexually abuse and eventually rape her. Salander soon turns the tables on him, torturing him and branding him as a rapist. Fincher wanted the character to be worse than a typical antagonist, although he did not want to emulate the stereotypical "mustache-twirling pervert". The director considered Van Wageningen to be the embodiment of a versatile actor—one who was a "full-fledged human being", a "brilliant" actor. "He was able to bring his performance from a logical place in Bjurmans mind and find the seething morass of darkness inside," Fincher stated. Bjurmans multifaceted psyche was the main reason Van Wageningen wanted to play the role. The Dutch actor said, "This character goes through a lot and I wasnt quite sure I wanted to go through all that. I started out half way between the elation of getting to work with David Fincher and the dread of this character, but I was able to use both of those things. We both thought the most interesting route would be for Bjurman to seem half affable. The challenge was not in finding the freak violence in the guy but finding the humanity of him." 
* Christopher Plummer as Henrik Vanger:
:Henrik is a wealthy businessman who launches an extensive investigation into his familys affairs. Despite calling the Vanger family "dysfunctional", Plummer said of the character: "I love the character of the old man, and I sympathize with him. Hes really the nicest old guy in the whole book. Everybody is a bit suspect, and still are at the end. Old Vanger has a nice straight line, and he gets his wish."  Plummer wanted to imbue the character with irony, an element he found to be absent from the novels Henrik.    "I think that the old man would have it," he opined, "because hes a very sophisticated old guy   used to a great deal of power. So in dealing with people, he would be very good   he would be quite jokey, and know how to seduce them." 
* Stellan Skarsgård as Martin Vanger:
:Martin is the current CEO of the Vanger Corporation. Skarsgård was allured by the characters dual nature, and was fascinated that he got to portray him in "two totally different ways".    In regards to Martins "very complex" and "complicated" personality, the Swedish actor said, "He can be extremely charming, but he also can seem to be a completely different person at different points in the film."  While consulting with Fincher, the director wanted Skarsgård to play Martin without reference to the book. 
* Joely Richardson as Harriet Vanger:
:In performing her "tricky" character, Richardson recalled that Fincher wanted her to embrace a "darker, edgier" persona, without sugarcoating, and not "resolved or healed". "Even if you were starting to move towards the direction of resolved or healed, he still wanted it edgy and dark. There are no straightforward emotions in the world of this film." 
* Robin Wright as Erika Berger
* Steven Berkoff as Dirch Frode
* Geraldine James as Cecilia Vanger
* Ulf Friberg as Wennerström
* Goran Višnjić as Dragan Armansky
* Donald Sumpter as Detective Morell
* Embeth Davidtz as Annika Giannini
* Joel Kinnaman as Christer Malm
* Elodie Yung as Miriam Wu
* Tony Way as Plague
* Alan Dale as Detective Isaksson
* Julian Sands as Young Henrik Vanger
* David Dencik as Young Morell
* Fredrik Dolk as Wennerströms Lawyer
* Per Myrberg as Harald Vanger
* Gustaf Hammarsten as Young Harald
* Leo Bill as Trinity
* Josefin Asplund as Pernilla Blomkvist

== Production ==

=== Conception and writing ===
The success of Stieg Larssons novel created Hollywood interest in adapting the book, as became apparent in 2009, when Lynton and Pascal pursued the idea of developing an "American" version unrelated to the  , who had recently completed the script for Moneyball (film)|Moneyball (2011), became the screenwriter, while producer Scott Rudin finalized a partnership allocating full copyrights to Sony.  Zaillian, who was unfamiliar with the novel, got a copy from Rudin. The screenwriter recalled, "They sent it to me and said, We want to do this.  We will think of it as one thing for now. Its possible that it can be two and three, but lets concentrate on this one."    After reading the book, the screenwriter did no research on the subject. 
Fincher, who was requested with partner Cean Chaffin by Sony executives to read the novel,  
was astounded by the series size and success. As they began to read, the duo noticed that it had a tendency to take "readers on a lot of side trips"—"from detailed explanations of surveillance techniques to angry attacks on corrupt Swedish industrialists," professed The Hollywood Reporter  Gregg Kilday. Fincher recalled of the encounter: "The ballistic, ripping-yarn thriller aspect of it is kind of a red herring in a weird way. It is the thing that throws Salander and Blomkvist together, but it is their relationship you keep coming back to. I was just wondering what 350 pages Zaillian would get rid of." Because Zaillian was already cultivating the screenplay, the director avoided interfering. After a conversation, Fincher was comfortable "they were headed in the same direction". 

 Steven Zaillian |align=left|width=250px}}

The writing process consumed approximately six months, including three months creating notes and analyzing the novel.  Zaillian noted that as time progressed, the writing accelerated. "As soon as you start making decisions," he explained, "you start cutting off all of the other possibilities of things that could happen.  So with every decision that you make you are removing a whole bunch of other possibilities of where that story can go or what that character can do."  Given the books sizable length, Zaillian deleted elements to match Finchers desired running time.  Even so, Zaillan took significant departures from the book.  To Zaillian, there was always a "low-grade" anxiety, "but I was never doing anything specifically to please or displease," he continued. "I was simply trying to tell the story the best way I could, and push that out of my mind. I didnt change anything just for the sake of changing it. Theres a lot right about the book, but that part, I thought we could do it a different way, and it could be a nice surprise for the people that have read it."   
 five act structure, which Fincher pointed out is "very similar to a lot of TV cop dramas." 

=== Filming ===
  provided for much of the setting of The Girl with the Dragon Tattoo.]]

Fincher and Zaillians central objective was to maintain the novels setting. To portray Larssons vision of Sweden, and the interaction of light on its landscape, Fincher cooperated with an artistic team that included cinematographer Jeff Cronenweth and production designer Donald Graham Burt. The film was wholly shot using Red Digital Cinema Camera Companys RED MX digital camera, chosen to help evoke Larssons tone. The idea, according to Cronenweth, was to employ unorthodox light sources and maintain a realistic perspective. "So there may be shadows, there may be flaws, but its reality. You allow silhouettes and darkness, but at the same time we also wanted shots to counter that, so it would not all be one continuous dramatic image."  Swedens climate was a crucial element in enhancing the mood. Cronenweth commented, "Its always an element in the background and it was very important that you feel it as an audience member. The winter becomes like a silent character in the film giving everything a low, cool-colored light that is super soft and non-direct."  To get acquainted with Swedish culture, Burt set out on a month-long expedition across the country. He said of the process, "It takes time to start really taking in the nuances of a culture, to start seeing the themes that recur in the architecture, the landscape, the layouts of the cities and the habits of the people. I felt I had to really integrate myself into this world to develop a true sense of place for the film. It was not just about understanding the physicality of the locations, but the metaphysics of them, and how the way people live comes out through design." 
 extras were sought for background roles.  Filming also took place in the United Kingdom and the United States.

In one sequence the character Martin Vanger plays the song "Orinoco Flow" by Enya before beginning his torture of Mikael Blomkvist. David Fincher, the director, said that he believed that Martin "doesn’t like to kill, he doesn’t like to hear the screams, without hearing his favorite music" so therefore the character should play a song during the scene.    Daniel Craig, the actor who played Blomkvist, selected "Orinoco Flow" on his iPod as a candidate song. Fincher said "And we all almost pissed ourselves, we were laughing so hard. No, actually, it’s worse than that. He said, ‘Orinoco Flow!’ Everybody looked at each other, like, what is he talking about? And he said, ‘You know, “Sail away, sail away...”’ And I thought, this guy is going to make Blomkvist as metro as we need." 

==== Title sequence ====
 
 Tim Miller, creative director for the title sequence, wanted to develop an abstract narrative that reflected the pivotal moments in the novel, as well as the character development of Lisbeth Salander. It was arduous for Miller to conceptualize the sequence abstractly, given that Salanders occupation was a distinctive part of her personality. His initial ideas were modeled after a keyboard. "We were going to treat the keyboard like this giant city with massive fingers pressing down on the keys," Miller explained, "Then we transitioned to the liquid going through the giant obelisks of the keys."  Among Millers many vignettes was "The Hacker Inside", which revealed the characters inner disposition and melted them away. The futuristic qualities in the original designs provided for a much more cyberpunk appearance than the final product. In creating the "cyber" look for Salander, Miller said, "Every time I would show David a design he would say, More Tandy! Its the shitty little computers from Radio Shack, the Tandy computers. They probably had vacuum tubes in them, really old technology. And David would go More Tandy, until we ended up with something that looked like we glued a bunch of computer parts found at a junkyard together."   

Fincher wanted the vignette to be a "personal nightmare" for Salander, replaying her darkest moments. "Early on, we knew it was supposed to feel like a nightmare," Miller professed, who commented that early on in the process, Fincher wanted to use an artwork as a template for the sequence. After browsing through various paintings to no avail, Fincher chose a painting that depicted the artist, covered in black paint, standing in the middle of a gallery. Many of Millers sketches contained a liquid-like component, and were rewritten to produce the "gooey" element that was so desired. "David said lets just put liquid in all of them and it will be this primordial dream ooze thats a part of every vignette," Miller recalled. "It ties everything together other than the black on black." 

The title sequence includes abundant references to the novel, and exposes several political themes. Salanders tattoos, such as her phoenix and dragon tattoos, were incorporated. The multiple flower representations signified the biological life cycle, as well as Henrik, who received a pressed flower each year on his birthday. "One had flowers coming out of this black ooze," said Fincher, "it blossoms, and then it dies. And then a different flower, as that one is dying is rising from the middle of it. It was supposed to represent this cycle of the killer sending flowers."  Ultimately, the vignette becomes very conceptual because Miller and his team took "a whole thought, and cut it up into multiple different shots that are mixed in with other shots". In one instance, Blomkvist is strangled by strips of newspaper, a metaphor for the establishment squelching his exposes. 

In the "Hot Hands" vignette, a pair of rough, distorted hands that embrace Salanders face and melt it represent all thats bad in men. The hands that embrace Blomkvists face and shatter it, represent wealth and power.  Themes of domestic violence become apparent as a womans face shatters after a merciless beating; this also ties in the brutal beating of Salanders mother by her father, an event revealed in the sequel, The Girl Who Played with Fire (2006). 

A cover of Led Zeppelins "Immigrant Song" (1970) plays throughout the title sequence. The rendition was produced by soundtrack composers Atticus Ross and Nine Inch Nails member Trent Reznor, and features vocals from Yeah Yeah Yeahs lead singer Karen O.  Fincher suggested the song, but Reznor agreed only at his request.  Led Zeppelin licensed the song only for use in the films trailer and title sequence. Fincher stated that he sees title sequences as an opportunity to set the stage for the film, or to get an audience to let go of its preconceptions. 
 Software packages 3ds Max Softimage (for rigging and Digital Fusion Real Flow Sony Vegas Zbrush and organic modeling), VRAY (for 3D rendering|rendering). 

=== Soundtrack ===
 
 Trent Reznor |align=right| width=250px}}
 acoustics and blends them with elements of electronic music, resulting in a forbidding atmosphere. "We wanted to create the sound of coldness—emotionally and also physically," he asserted, "We wanted to take lots of acoustic instruments   and transplant them into a very inorganic setting, and dress the set around them with electronics."   

Even before viewing the script, Reznor and Ross opted to use a redolent approach to creating the films score. After discussing with Fincher the varying soundscapes and emotions, the duo spent six weeks composing. "We composed music we felt might belong," stated the Nine Inch Nails lead vocalist, "and then wed run it by Fincher, to see where his head’s at and he responded positively. He was filming at this time last year and assembling rough edits of scenes to see what it feels like, and he was inserting our music at that point, rather than using temp music, which is how it usually takes place, apparently." Finding a structure for the soundtrack was arguably the most strenuous task. "We weren’t working on a finished thing, so everything keeps moving around, scenes are changing in length, and even the order of things are shuffled around, and that can get pretty frustrating when you get precious about your work. It was a lesson we learned pretty quickly of, Everything is in flux, and approach it as such. Hopefully it’ll work out in the end." 

== Release ==

=== Pre-release ===
 

A screening for The Girl with the Dragon Tattoo took place on November 28, 2011, as part of a critics-only event hosted by the New York Film Critics Circle. Commentators at the event predicted that while the film would become a contender for several accolades, it would likely not become a forerunner in the pursuit for Academy Award nominations.    A promotional campaign commenced thereafter, including a Lisbeth Salander-inspired collection, designed by Trish Summerville for H&M.   The worldwide premiere was at the Odeon Leicester Square in London on December 12, 2011,  followed by the American opening at the Ziegfeld Theatre in New York City on December 14 and Stockholm the next day.   Sonys target demographics were men and women over the age of 25 and 17–34.  The film went into general release in North America on December 21, at 2,700 theaters,    expanding to 2,974 theaters on its second day.  The United Kingdom release was on December 26,    Russia on January 1, 2012,    and Japan on February 13.    India and Vietnam releases were abandoned due to censorship concerns.       A press statement from the Central Board of Film Certification stated: "Sony Pictures will not be releasing The Girl with the Dragon Tattoo in India. The censor board has judged the film unsuitable for public viewing in its unaltered form and, while we are committed to maintaining and protecting the vision of the director, we will, as always, respect the guidelines set by the board."  In contrast, the National Film Board of Vietnam insisted that the films withdrawal had no relation to rigid censorship guidelines, as it had not been reviewed by the committee. 

=== Box office ===
Finchers film grossed $232.6 million during its theatrical run.  The films American release grossed $1.6 million from its Tuesday night screenings,  a figure that increased to $3.5 million by the end of its first day of general release.  It maintained momentum into its opening weekend, accumulating $13 million for a total of $21 million in domestic revenue.  The films debut figures fell below media expectations.    Aided by positive word of mouth,  its commercial performance remained steady into the second week, posting $19 million from 2,914 theaters.  The third week saw box office drop 24% to $11.3 million, totaling $76.8 million. The number of theaters slightly increased to 2,950.  By the fifth week, the number of theaters shrank to 1,907, and grosses to $3.7 million, though it remained within the national top ten.  The film completed its North American theatrical run on March 22, 2012, earning over $102.5 million. 

The international debut was in six Scandinavian markets on December 19–25, 2011, securing $1.6 million from 480 venues.    In Sweden the film opened in 194 theaters to strong results, accounting for more than half of international revenue at the time ($950,000).  The first full week in the United Kingdom collected $6.7 million from 920 theaters.  By the weekend of January 6–8, 2012, the film grossed $12.2 million for a total of $29 million; this included its expansion into Hong Kong, where it topped the box office, earning $470,000 from thirty-six establishments. The film similarly led the field in South Africa. It accumulated $6.6 million from an estimated 600 theaters over a seven-day period in Russia, placing fifth.  The expansion continued into the following week, opening in nine markets. The week of January 13–15 saw the film yield $16.1 million from 3,910 locations in over forty-three territories, thus propelling the international gross to $49.3 million.  It debuted at second place in Austria and Germany, where in the latter, it pulled $2.9 million from 525 locations.    Similar results were achieved in Australia, where it reached 252 theaters.  The films momentum continued throughout the month, and by January 22, it had hit ten additional markets, including France and Mexico, from which it drew $3.25 million from 540 venues and $1.25 million from 540 theaters, respectively.  In its second week in France it descended to number three, with a total gross of $5.8 million. 

The next major international release came in Japan on February 13, where it opened in first place with $3.68 million (¥288 million)  in 431 theaters.  By the weekend of February 17–19, the film had scooped up $119.5 million from international markets.  The total international gross for The Girl with the Dragon Tattoo was $130.1 million.  MGM, one of the studios involved in the production, posted a "modest loss" and declared that they had expected the film to gross at least 10% more. 

=== Home media === bootleg copy. The Muppets and Hop (film)|Hop.  The following week, the film sold an additional 144,000 copies generating $2.59 million in gross revenue.  As of January 2014, 1,478,230 units had been sold, grossing $22,195,069.   

== Reception ==

=== Critical response ===
 
 normalized rating out of 100 based on the reviews of mainstream critics, the film received an average score of 71 based on 41 reviews. 

 , writing for The New York Times, admired the moments of "brilliantly orchestrated" anxiety and confusion, but felt that The Girl with the Dragon Tattoo was vulnerable to the "lumbering proceduralism" that he saw in its literary counterpart, as evident with the "long stretches of drab, hackneyed exposition that flatten the atmosphere".    The Wall Street Journal  Joe Morgenstern praised Cronenweths cinematography, which he thought provided for glossy alterations in the films darkness; "Stockholm glitters in nighttime exteriors, and its subway shines in a spectacular spasm of action involving a backpack."    Rex Reed of The New York Observer professed that despite its occasional incomprehensibility, the movie was "technically superb" and "superbly acted".  In contrast, Kyle Smith of New York Post censured the film, calling it "rubbish" and further commenting that it "demonstrates merely that masses will thrill to an unaffecting, badly written, psychologically shallow and deeply unlikely pulp story so long as you allow them to feel sanctified by the occasional meaningless reference to feminism or Nazis."   
 2009 film, Roger Ebert of the Chicago Sun-Times said that Rapace felt more self-conscious in the role.    A revelation in the eyes of Entertainment Weekly  Owen Gleiberman, he proclaimed that her character was more important than "her ability to solve a crime".    Her "hypnotic" portrayal was noted by Justin Chang of Variety (magazine)|Variety,  as well as Salon (website)|Salon critic Andrew OHehir, who wrote, "Rooney Mara is a revelation as Lisbeth Salander, the damaged, aggressive computer geek and feminist revenge angel, playing the character as far more feral and vulnerable than Noomi Rapace’s borderline-stereotype sexpot Goth girl."    Scott Tobias of The A.V. Club enjoyed the chemistry between Mara and Craig,  as did David Germain of the Associated Press; "Mara and Craig make an indomitable screen pair, he nominally leading their intense search into decades-old serial killings, she surging ahead, plowing through obstacles with flashes of phenomenal intellect and eruptions of physical fury."     Although Puig found Mara inferior to Rapace in playing Salander, with regard to Craigs performance, he said that the actor shone.  This was supported by Morgenstern who avouched that Craig "nonetheless finds welcome humor in Mikaels impassive affect". 

=== Accolades ===
In addition to numerous awards, The Girl with the Dragon Tattoo was included on several year-end lists by film commentators and publications. It was named the best film of 2011 by MTV and James Berardinelli of ReelViews.   The former wrote, "The director follows up the excellent Social Network with another tour de force, injecting the murder mystery that introduces us to outcast hacker Lisbeth Salander   and embattled journalist   with style, intensity and relentless suspense. Mara is a revelation, and the films daunting 160-minute runtime breezes by thanks to one heart-racing scene after the next. Dark and tough to watch at times, but a triumph all around."    The film came second in indieWire  list of "Drew Taylors Favorite Films Of 2011",  while reaching the top ten of seven other publications,  including the St. Louis Post-Dispatch,  San Francisco Chronicle,  and the New Orleans Times-Picayune.  The Girl with the Dragon Tattoo was declared one of the best films of the year by the American Film Institute,  as well as the National Board of Review of Motion Pictures. 

{| class="wikitable sortable"
|-
! Date of ceremony
! Award
! Category
! Recipients
! Result
|-
| | January 10, 2012 Alliance of Women Film Journalists Awards 2011 
| Best Film Music or Score
| Trent Reznor, Atticus Ross
|  
|-
| February 12, 2012
| American Society of Cinematographers Awards 
| Best Cinematography
| Jeff Cronenweth
|  
|-
| rowspan="2" | February 12, 2012 British Academy Film and Television Awards 
| BAFTA Award for Best Cinematography
| Jeff Cronenweth
|  
|- BAFTA Award for Best Original Music
| Trent Reznor, Atticus Ross
|  
|-
| rowspan="2" | January 12, 2012 Broadcast Film Critics Association Awards  Best Editing
| Kirk Baxter, Angus Wall
|  
|- Best Composer
| Trent Reznor, Atticus Ross
|  
|-
| rowspan="3" | January 5, 2012
| rowspan="3" | Central Ohio Film Critics Association Awards 
| Best Picture
|
|  
|-
| Best Director
| David Fincher
|  
|-
| Best Adapted Screenplay
| Steven Zaillian
|  
|-
| December 19, 2011 Chicago Film Critics Association Awards  Best Original Score
| Trent Reznor, Atticus Ross
|  
|-
| January 10, 2012 Denver Film Critics Association Awards 
| Best Actress
| Rooney Mara
|  
|-
| January 28, 2012
| Directors Guild of America Awards 
| Best Director
| David Fincher
|  
|-
| rowspan="2" | January 15, 2012 Golden Globe Awards  Best Actress – Motion Picture Drama
| Rooney Mara
|  
|- Best Original Score
| Trent Reznor, Atticus Ross
|  
|-
| rowspan="3" | June 3, 2012 MTV Movie Awards 
| Best Female Performance
| Rooney Mara
|  
|-
| Breakthrough Performance
| Rooney Mara
|  
|-
| Best On-Screen Transformation
| Rooney Mara
|  
|-
| rowspan="2" | December 1, 2011 National Board of Review Awards 
|-
| Breakthrough Performance
| Rooney Mara  
|  
|-
| December 23, 2011
| Oklahoma Film Critics Circle Awards 
| Best Picture
|
|  
|-
| January 21, 2012
| Producers Guild of America Award 
| Best Picture
|
|  
|-
| rowspan="5" | December 19, 2011
| rowspan="5" | St. Louis Gateway Film Critics Association Awards 2011|St. Louis Gateway Film Critics Association Awards   Best Director
| David Fincher
|  
|- Best Actress
| Rooney Mara
|  
|-
| Best Cinematography
| Jeff Cronenweth
|  
|-
| Best Music
| Trent Reznor, Atticus Ross
|  
|-
| Best Scene
| Blur Studio  
|  
|-
| December 5, 2011 Washington D.C. Area Film Critics Association Awards  Best Score
| Trent Reznor, Atticus Ross
|  
|-
| February 19, 2012 Writers Guild Awards   
| Best Screenplay Adapted
| Steven Zaillian
|  
|-
| rowspan="5" | February 26, 2012
| rowspan="5" | 84th Academy Awards  Best Actress
| Rooney Mara
|  
|- Best Cinematography
| Jeff Cronenweth
|  
|- Best Film Editing
| Angus Wall, Kirk Baxter
|  
|- Best Sound Editing
| Ren Klyce
|  
|- Best Sound Mixing
| David Parker, Michael Semanick, Ren Klyce and Bo Persson
|  
|-
| June 7, 2012
| Kerrang! Awards 2012|Kerrang! Awards 
| Best Film
|
|  
|-
| July 26, 2012
| 38th Saturn Awards  Best Horror or Thriller Film
|
|  
|-
| rowspan="1" | February 10, 2013 Grammy Awards  Best Score Soundtrack for Visual Media
| Trent Reznor, Atticus Ross
|  
|}

== Sequels == back to back".  There was an announced release date of 2013 for a film version of The Girl Who Played with Fire, although by August 2012 it was delayed due to difficulties with the script, being written by Steve Zaillian.     In July 2013, Andrew Kevin Walker was hired to re-write the script. 

In September 2014, Fincher stated that a script for Played with Fire had been written that was "extremely different from the book", and that, despite the long delay, he was confident that the film would be made because Sony "already has spent millions of dollars on the rights and the script".  Mara, however, has been less optimistic about the project, saying in interviews in both May 2014 and February 2015 that sequels looked unlikely, despite her desire to film them.  

== Footnotes ==
 

== External links ==
 
 
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 