The Lost People
{{Infobox film
| name           = The Lost People
| image          = "The_Lost_People"_(1949).jpg
| caption        = British quad poster
| director       = Muriel Box Bernard Knowles
| producer       = Gordon Wellesley
| writer         = Bridget Boland
| based on       = Cockpit a play by Bridget Boland
| starring       = Dennis Price  Mai Zetterling Richard Attenborough John Greenwood
| cinematography = Jack Asher
| editing        = Gordon Hales
| studio         = Gainsborough Pictures
| distributor    = General Film Distributors (UK)
| released       = 22 August 1949 (UK)
| runtime        = 89 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British drama film directed by Muriel Box and Bernard Knowles and starring Dennis Price, Mai Zetterling and Richard Attenborough.  After the Second World War, some British soldiers are guarding a theatre in Germany containing various refugees and prisoners trying to work out what to do with them. It is based on a play by Bridget Boland.

==Cast==
* Dennis Price ...  Ridley
* Mai Zetterling ...  Lily
* Richard Attenborough ...  Jan
* Siobhan McKenna ...  Marie
* Maxwell Reed ...  Peter
* William Hartnell ...  Barnes
* Gerard Heinz ...  Professor
* Zena Marshall ...  Anna
* Olaf Pooley ...  Milosh
* Harcourt Williams ...  Priest
* Philo Hauser ...  Draja
* Jill Balcon ...  Rebecca
* Grey Blake ...  Saunders
* Marcel Poncin ...  Duval
* Nelly Arno ...  Old Woman in Box
* George Benson ...  Driver
* Peter Bull ...  Wolf
* Paul Hardtmuth ...  Jiri
* Tutte Lemkow ...  Jaroslav
* Pamela Stirling ...  Young Woman in Box Charles Hawtrey ...  Prisoner
* Herbert Lom ...Guest

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 