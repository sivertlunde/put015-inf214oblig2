The Truth (2006 film)
{{Infobox Film name = The Truth image = caption = The Truth film poster director = George Milton writer = George Milton, Mark Tilton starring = William Beck – Scott 
Elaine Cassidy – Candy 
Stephen Lord – Felix 
Elizabeth McGovern – Donna 
Lea Mornar – Mia 
Rachael Stirling – Martha 
Zoe Telford – Blossom 
Karl Theobald – Spud producer = Julie-Anne Edwards
|distributor= Guerilla Films Ltd. budget =  runtime = 114 minutes language = English
|}}

The Truth is a darkly comic murder-mystery satirising new age therapy. It was directed by George Milton, co-written by Milton and Mark Tilton and produced by Julie-anne Edwards. The film features an ensemble cast including Elizabeth McGovern, Elaine Cassidy, Karl Theobald, Stephen Lord, Zoe Telford, Rachael Stirling, William Beck and Lea Mornar and was critically acclaimed on its theatrical release in 2006.

The makers described the film as an outrageous murder-mystery for the "Me Generation". Seven strangers go to a remote retreat for a week of soul searching. Encouraged to tell the truth at all times by their guru Donna Shuck, they venture on a spiritual journey of personal growth, taking in jealousy, hatred, sex, perversion and a little murder on the way.
 Time Out magazine: ‘irony is plentiful in Milton’s low-budget but highly satisfying, slyly intelligent UK indie. He described the film as an engagingly fresh take on a subgenre of potentially slim pickings. With consistently interesting plot twists and shifts in power between the uncertainly allied characters, the film’s a real rollercoaster, altering deliciously deadpan humor with serious insights, deft satire with dark suspense, and even managing to succeed, here and there, in several different tonal registers at once.’

The Truth was 2 Many Executives’ first feature film, shot on location near Aviemore in Scotland, and post-produced in London and Rome.

== External links ==
*http://www.thetruthmovie.com/ Official Site
* 
*  

 
 