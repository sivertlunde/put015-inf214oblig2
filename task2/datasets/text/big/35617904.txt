The We and the I
 
{{Infobox film
| name           = The We and the I
| image          = The We and the I (Gondry film).jpg
| caption        = Film poster
| director       = Michel Gondry
| producer       = Raffi Adlan Georges Bermann
| writer         = Michel Gondry Paul Proch and Jeff Grimshaw   
| starring       = Joe Mele
| music          = 
| cinematography = Alex Disenhof
| editing        = Jeff Buchanan
| distributor    =Partizan films Kinology Mars distribution 108 Media
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $42,172   (Domestic gross)  $251,433   (Foreign gross) 
}}

The We and the I is a 2012 American drama film co-written and directed by Michel Gondry. The film was screened in the Directors Fortnight section at the 2012 Cannes Film Festival.   During the 2012 Toronto International Film Festival, 108 Media and Paladin acquired the North American rights to the film, with a release date of March 8, 2013.  The movie is about teenagers who ride the same bus route on their last day of high school and the film was shot in The Bronx, New York City, New York.

==Cast==
* Joe Mele
* Michael Brodie as Michael
* Teresa Lynn as Teresa
* Laidychen Carrasco as Laidychen
* Raymond Delgado as Little Raymond
* Jonathan Ortiz as Jonathan
* Jonathan Worrell as Big T
* Alex Barrios as Alex
* Meghan Niomi Murphy as Niomi
* Raymond Rios as Big Raymond
* Brandon Diaz as Brandon
* Elijah Canada as Elijah
* Manuel Rivera as Manuel
* Luis Figueroa as Luis
* Jacobchen Carrasco as Jacobchen
* Mia Lobo as Bus Driver
* Amanda Kay Riley as Beautiful Girl On Bike 
*Jillian Rice as Jillian

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 