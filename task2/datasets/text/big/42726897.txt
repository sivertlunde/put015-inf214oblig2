Silent Witnesses
 

Silent Witnesses (Немые свидетели) is a 1914 Russian film  directed by Yevgeni Bauer about the relationship between masters and servants in pre-revolutionary Russia.

== Plot ==
When Variusha, a maid in a Russian upper-class household, is not allowed to go and see her children in the country, the porters grand-daughter Nastya agrees to replace her. Soon she attracts the attention of Pavel, the son of the house, who starts flirting with her. When Nastya finds out that Yelena, Pavels fiancée, has a lover, she tries to chase him away and refuses the money offered to her to remain silent. But Pavel does not want to endanger the wedding arranged for him. He has lost interest in Nastya and treats her only as a maid.

== Cast ==
* Aleksandr Chargonin as Pavel Kostrizyn
* Aleksandr Kheruvimov as The Porter
* Dora Tschitorina as Nastya
* Elsa Krueger as Yelena

== References ==
 

== External links ==
*  

 
 
 

 