Ali Baba (1940 film)
{{Infobox film
| name           = Alibaba
| image          = Alibaba_(1940).jpg
| image_size     = 
| caption        = From Filmindia December 1939 issue
| director       = Mehboob Khan
| producer       = Sagar Movietone
| writer         = Babubhai Mehta Zia Sarhadi 
| narrator       =  Surendra Sardar Akhtar Wahidan Bai Anil Biswas
| cinematography = Faredoon Irani
| editing        = Shamsuddin Qadri
| distributor    =
| studio         = Sagar Film Company
| released       = 1940
| runtime        = 156 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1940 Urdu/Hindi Anil Biswas with lyrics written by Safdar Aah.    The film was a bilingual, made in Punjabi language as Alibaba at the same time.    It starred Surendra, Sardar Akhtar, Ghulam Mohammed and Wahidan Bai.   

In this Arabian Nights fantasy Alibaba, Surendra played the title role    
and also appeared in the role of Alibaba’s son. Sardar Akhtar who went on to marry Mehboob Khan was not a well-known actress at this time. She was given the starring role while Wahidan Bai, mother of the actress Nimmi played the second female lead.    

==Plot==
Alibaba and his son live with his rich brother Cassim. Marjina and Zabba are two slave girls who work in Cassim’s house. Marjina and Alibaba’s son are in love with each other. Zabba is in league with Abu Hassan, who is the leader of a gang of thieves. Alibaba finds the thieves’ cave and becomes rich. Cassim’s wife insists on knowing the secret of his wealth. Alibaba tells his brother who goes to the cave but can’t get out as he’s forgotten the magic words. The thieves find him and kill him. Alibaba and his son find Cassim and bring his body back for burial. Abu Hassan finds out about Alibaba, and the story then follows Marjina outsmarting Abu Hassan who comes visiting as a merchant.

==Cast==
* Surendra
* Sardar Akhtar
* Wahidan Bai
* Ghulam Mohammed 
* Shetty
* Jagdish Kupal
* Ishrat Jaan
* Amirjaan

==Production==
The first shot for Alibaba was taken at the same studio (Imperial), where Mehboob Khan had started his career as one of the forty thieves in a vat, Alibaba Aur Chalis Chor (1929). He remained unseen in the vat for the entire length of his debut role.        This was the remake of the same film. Alibaba was shot at night as it was the fasting month of Ramzan. It was during the shooting of this film that Mehboob Khan and Sardar Akhtar got together and later married. The film was the last film Khan made for Sagar Movietone as he soon moved on to form Mehboob Productions.    

==Music==
The film had music by Anil Biswas with lyrics written by Aah Sitapuri. The singers are Surendra, Wahidan Bai and Anil Biswas. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Dil Ka Saaz Bajaaye Ja
| Surendra, Waheedan Bai
|-
| 2
| Dil Chheen ke jaata hai
| Surendra, Waheedan Bai
|-
| 3
| Teri In Ankhon Ne Kiya Bimar Haye
| Surendra, Waheedan Bai
|-
| 4
| Hum Aur Tum Aur Ye Khushi
| Surendra, Waheedan Bai
|-
| 5
| Bhool Gaye Kun Bhool Gaye
| Surendra
|-
| 6
| Soz-e-Furkat Hai abhi
| Surendra
|-
| 7
| Jise Zindagi Vabal Jaan Use Kun Na Jins-e-aar Ho
| Wahidan Bai
|-
| 8
| Kyun Pyaar Kia Tha
| Wahidan Bai
|-
| 9
| Shukr Hai Tum Mile Baad Ummeed Mein Dil Ke
|
|-
| 10
| Yeh Bheja Hai Humne Payam-e-Mohabbat
| Anil Biswas
|}


==References==
 

==External links==
* 

 
 

 
 
 
 