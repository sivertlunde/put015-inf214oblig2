Nil Gavani Sellathey
{{Infobox film
| name = Nil Gavani Sellathey
| image = 
| caption = 
| director = Anand Chakravarthy
| producer = Anand Chakravarthy
| writer = 
| starring = Jagan Anand Chakravarthy Dhansikha Lakshmi Nair  Adithyaa Dev
| music = Selvaganesh
| cinematography = J Lakshman 
| editing = MathanGunaDeva
| studio = 
| distributor =
| released =  
| runtime = 140 minutes
| country = India
| language = Tamil
| budget = 
| gross =
}}
Nil Gavani Sellathey is a 2010 Indian  , released on 17 December 2010 with favorable reviews from the critics. It failed to succeed at the box office and was re-released on 25 March 2011. 

==Plot==

Sam (Anand Chakaravarthy), Jo (Dhaniska), Arun (Ramssy), Priya (Lakshmi Nair) and Milo (Jagan) head to a small village on a pleasure trip. And they reach the place despite a warning that it is not to going to be a nice trip.

As night arrives, what arrives along with it is a series of mysterious experiences. The friends are attacked one after other by an unidentified villain. In the meantime, a police officer too arrives at the village to unravel the mystery.

What is the reason behind the attacks? Will they survive? All these things are made clear in the climax, which has so much twists and turns, with some being clichéd and some quite interesting

==Cast== Jagan as Milo
* Anand Chakravarthy as Sam
* Lakshmi Nair as  Priya
* Dhansika as Jo
* Ramsyy as Arun
* Adithyaa Dev as Chinnapaiya
* Kumaravel KS as Police Officer
* Azhagam Perumal

==Critical reception==
Rohit Ramachandran of nowrunning.com gave it 4/5 stars stating that "Nil Gavani Sellathey, inspired from The Texas Chainsaw Massacre, is a hell of a remake. It is the most significant film thats come out this year."  He went on to call it the third best movie of the year 

==Soundtrack==
Nil Gavani Sellathey s soundtrack is composed by Selvaganesh. 
{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes

| title1 = Vanavillum
| extra1 = Karthik (singer)|Karthik, Premji Amaren, Brodha V, Kalpana, Sri Madhumitha
| lyrics1 = J Francis Kriba

| title2 = Paarvai Undhan
| extra2 = Vasundhara Das, Sricharan
| lyrics2 = Na. Muthukumar

| title3 = Vaanam Yengilum
| extra3 = Ranjith, Vijaynarain
| lyrics3 = Na. Muthukumar

| title4 = Nadamadum Sudukada
| extra4 = VV Prasanna, Krishna Iyer, Rashmi Vijayan
| lyrics4 = J Francis Kriba

| title5 = Nil Gavani
}}

==References==
 


== External links ==
*  

 
 
 
 