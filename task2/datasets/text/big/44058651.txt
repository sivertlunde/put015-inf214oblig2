Aalmaaraattam
{{Infobox film
| name = Aalmaaraattam
| image =
| caption =
| director = P. Venu
| producer =
| writer = P. Venu
| screenplay = Ravikumar Sadhana Sadhana
| music = M. K. Arjunan
| cinematography =
| editing =
| studio = Lekha Movies
| distributor = Lekha Movies
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, Ravikumar and Sadhana in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Jose Prakash 
*K. P. Ummer  Ravikumar  Sadhana  Sudheer 
*Vijayalalitha  Vincent

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by P. Venu and Konniyoor Bhas. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Arinju Sakhi || Vani Jairam || P. Venu || 
|- 
| 2 || Ennadharam || Ambili || Konniyoor Bhas || 
|- 
| 3 || Kaamini Kaatharamizhi || P Jayachandran || P. Venu || 
|- 
| 4 || Kan Kulirke || P Jayachandran || Konniyoor Bhas || 
|- 
| 5 || Pulakamunarthum || Ambili || Konniyoor Bhas || 
|}

==References==
 

==External links==
*  

 
 
 


 