Olangal
{{Infobox film
| name           = Olangal (The Ripples)
| image          = Olangal.gif
| image_size     =
| caption        =
| director       = Balu Mahendra
| producer       = Joseph Abraham
| writer         = Balu Mahendra (screenplay)
| narrator       = Ambika Adoor Anju
| music          = Illayaraja
| cinematography = Balu Mahendra
| editing        = D. Vasu
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} 1982 Indian film in Malayalam directed by Balu Mahendra.  The films plot is inspired from novel Man, Woman and Child by Erich Segal.

==Synopsis==

Life goes on happily for the couple, Ravi (Amol Palekar) and Radha (Poornima Jayaram|Poornima) who live with their only daughter until Father John arrives in the city with a young boy Raju, Ravis son from an affair with Reetha (Ambika (actress)|Ambika) before his marriage to Radha. Now he has to keep the boy with him for a month before Father John takes the boy abroad.

Ravi introduces the boy to his wife as the son of a dead friend, George, and she happily accepts to keep the boy with them. But the truth emerges when the dead friend visits their house. The six-year-long marriage between Ravi and Radha shatters.

== Songs ==
The song "Thumbi Vaa" was liked by many people and got remade in Telugu, Kannada and Hindi. Balu Mahendra liked the tune so much that he used it again in the film Aur Ek Prem Kahani. The song was also used two more times by Ilayaraaja himself and once by Shankar Ganesh. http://www.musicquencher.com/blog/2009/08/25/multiple-versions-of-a-single-tune  The song "Thannannam Thaanannam" also featured in the Malayalam movie Yathra also as "Theeyanni Danimma" from the Telugu movie Nireekshana.
It was also used in the 2009 Hindi movie Paa as "Gumm Summ Paa".

==Soundtrack== 
The music was composed by Ilayaraja and lyrics was written by ONV Kurup. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kuliradunnu Maanathu || K. J. Yesudas, Chorus || ONV Kurup || 
|- 
| 2 || Thumbi vaa thumbakudathin || S Janaki || ONV Kurup || 
|- 
| 3 || Vezhaambal kezhum venalkkudeeram || K. J. Yesudas, S Janaki || ONV Kurup || 
|}

== Cast ==
* Amol Palekar	 ...	Ravi Chattan
* Poornima Jayaram	 ...	Radha R. Chattan - Ravis wife Ambika	 ...	Rita Anju		
* Adoor Bhasi
* T. R. Omana		
* Preeta		
* Jagathy Sreekumar

== External links ==
*  
* http://popcorn.oneindia.in/title/7986/olangal.html

==References==
 

 

 
 
 
 
 
 
 
 
 