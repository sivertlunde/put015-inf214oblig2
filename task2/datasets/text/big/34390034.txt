Masoom (1996 film)
 
{{Infobox film
| name           = Masoom
| image          = Masoom1996film.jpg
| image size     = 250px
| alt=
| caption        = Masoom Poster
| director       = Mahesh Kothare
| producer       = 
| screenplay     = 
| based on       =
| narrator       = 
| starring       = Tinnu Anand  Ayesha Jhulka   Inder Kumar   Sulabha Arya   Arun Bakshi
| music          = Anand Raj Anand
| lyrics         = 
| cinematography = 
| editing        =  
| distributor    = 
| released       = 
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Indian film. Masoom is a remake of 1994 Marathi Movie Maza Chakula directed by Mahesh Kothare, child actor is his son Adinath Kothare, who made his Marathi debut in Zapatlela 2.

==Cast==
* Tinnu Anand as Hawaldar Bhim Singh 
* Ayesha Jhulka as Chanda 
* Inder Kumar as Akash 
* Renuka Shahane as Yashoda 
* Laxmikant Berde as Jeetu 
* Suresh Oberoi as Vikram Singh 
* Master Omkar Kapoor as Kishan
* Mohan Joshi as Barood
* Sulabha Arya
* Arun Bakshi
* Rasik Dave

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tukur Tukur Dekhate Ho Kya" Poornima
|-
| 2
| "Kaale Libaas Mein Badan Gora"
| Udit Narayan
|-
| 3
| "Soja Mere Laadale Soja Mere Laal"
| Abhijeet Bhattacharya|Abhijeet, Kavita Krishnamurthy
|-
| 4
| "Zindagi Ko Jina Hai To"
| Jolly Mukherjee, Anand Raj Anand, Arun Bakshi, Sadhana Sargam
|-
| 5
| "Chota Bachcha Jaan Ke"
| Aditya Narayan
|-
| 6
|  "Yeh Jo Teri Payalonki Chan Chan Hai"
| Abhijeet, Sadhana Sargam
|-
|}

== Awards ==
* Aditya Narayan won the Screen Award Special Jury Award in 1997 for his song Chhota Baccha Jaan Ke  

== References ==
 

== External links ==
*  

 
 
 

 