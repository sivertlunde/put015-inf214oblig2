La Badil
{{Infobox film
| name           = La Badil (No Other Choice)
| image          =  
| caption        = 
| director       = Dominic Brown
| released       = 2012
| producer    = Dancing Turtle Films
| runtime        = 15 minutes
| country        = Western Sahara
| language       = Hassānīya, English
}} Dominic Brown, about the struggle of the indigenous Sahrawi people of Western Sahara. 

==Background==
In 2010, over 15,000 Sahrawis staged a protest camp known as Gdeim Izik, in the Western Sahara desert to resist Moroccan rule. Their cries received limited media coverage, and when Moroccan security forces broke up the camp, four Sahrawis and eleven Moroccan soldiers were killed. It was on the second anniversary of this uprising that Brown travelled undercover to the region to film the documentary.  
 
In an interview published by Newstime Africa, the filmmaker stated that his motivation behind the film was "because the situation in Western Sahara is one that very rarely gets the media coverage that it deserves" adding that he hoped it would "also show how there are many vested interests involved (eg. France and their trade deals with Morocco), that are preventing the Sahrawi people getting justice." 

==Content==
The documentary covers the current human rights and political situation of the Sahrawi. There are several interviews recorded with human rights victims including an elderly lady who had been attacked in her home the previous day by Moroccan security forces. There is also a focus given to the alleged vested interests of countries in the region, particularly France. The film states that the French Governments close relationship with Morocco, their trade deals and their use of veto over the terms of the UN mission in Western Sahara are major factors.

==See also==
* Western Sahara
* Gdeim Izik protest camp

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 