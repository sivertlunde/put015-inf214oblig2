Lahure (film)
{{Infobox film name           = Lahure image          = Lahure.JPG caption        = Lahure Songs Cover director       = Tulsi Ghimire producer       = Kanchenjunga Films  music          = Ranjit Gazmer cinematography = Binod Pradhan released       =   language  Nepali
|country        = Nepal
}}

Lahure (  is a 1989 Nepali film directed by Tulsi Ghimire. It starred Shrawan Ghimire, Tripti Nadakar and Tulsi Ghimire.  The music of the film was composed by Ranjit Gazmer. Binod Pradhan was the cinematographer.

==Film== Gorkha soldiers and the sacrifices made by them and their families.

==Soundtrack==
{| class="wikitable sortable"
|-
! Track !! Artists !! Length
|-
|Birta Ko Chino Bir Ko Santan|| Narayan Gopal || 5.34
|-
|Pahada Ko Mathi Mathi|| Narayan Gopal and Asha Bhosle || 5.00
|-
|Chiya Bari Ma|| Deepa Jha || 4.47
|-
|Basa Hai Ama|| Various artists || 6.03
|-
|Sawane Jharima Tyo Gaun|| Asha Bhosle || 6.48
|}

==References==
 

==External links==
*  

 
 
 

 