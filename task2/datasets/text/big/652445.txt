Armour of God II: Operation Condor
 
 
{{Infobox film
| name = Armour of God II: Operation Condor
| image = Armour-of-god-II-poster.jpg 
| caption = Hong Kong film poster
| film name = {{Film name| traditional = 飛鷹計劃
 | simplified = 飞鹰计划
 | pinyin = Fēi yīng jǐ huá
 | jyutping = Fei1 jing1 gai3 waak6}}
| director = Jackie Chan 
| producer = Raymond Chow Leonard Ho 
| writer = Jackie Chan Edward Tang
| starring = Jackie Chan Carol Cheng Eva Cobo de Garcia Shoko Ikeda
| music = Chris Babida
| cinematography = Arthur Wong
| editing =  Golden Harvest Media Asia
| released = 
| runtime = 106 minutes
| country = Hong Kong
| language = Cantonese
| budget = 
}} Hong Kong Armour of God.
 Indiana Jones Nazi to retrieve gold from an abandoned base deep in the Sahara Desert.

Armour of God II: Operation Condor is succeeded by the 2012 film CZ12.

==Plot==
Hong Kong treasure hunter Jackie, a.k.a. "Asian Condor", is summoned by Baron Bannon at his mansion in Madrid, Spain, where he is told of a story of a German commander named Hans von Katterling and his regiment burying 240 tons of gold at a secret base deep in the Sahara Desert in Africa before the end of World War II. The 18 soldiers involved in the operation disappeared under mysterious circumstances. By request from the United Nations, Bannon gives Jackie an unofficial mission to locate the base and recover the gold. Aside from acquiring the key to the base, he is partnered with Ada, an expert in African geography. Upon discovery of the gold, Jackie is promised one percent of the treasure, or roughly 2.5 tons of gold.

One night, while snooping around the home of one of the bases caretakers, Jackie meets a young German woman named Elsa, after saving her from a couple of Arab men - Amon and Tasza - who are also searching for the gold. The next day, he goes to a renowned locksmith and learns that the key is intricately designed with a secret code; thus the need to find the last person who used it. After evading an army of black cars across town, Jackie is asked by Elsa to join him and Ada on their expedition, as she is in search of von Katterling, who was her grandfather.

Upon their arrival in the Sahara Desert, the expedition team picks up Momoko, a Japanese woman who is searching for the meaning of death. However, their camp is attacked by black-veiled bandits who kidnap Elsa and Ada. Jackie and Momoko follow the bandits trail to a slave market, where they save Elsa and Ada from being auctioned off as sex slaves. Meanwhile, the rest of the expedition team are executed by a group of mercenaries led by a wheelchair-bound man. The quartet return to their camp to discover their comrades slain, but Momoko recognizes a statue in one of Elsas grandfathers pictures and leads them to an ancient temple.

After bidding Momoko farewell, the trio enter the ruins, where they encounter a band of vicious tribesmen. While running for their lives, they fall through a loose floor of sand into an underground cavern, leading them to the secret Nazi base. They discover the mummified remains of Elsas grandfather and look through his log book, revealing that the 18 soldiers under von Katterling ingested cyanide pills and died inside the base upon completion of their mission. However, the trio only counts 17 bodies, with one soldier missing. The wheelchair-bound man - arriving with his mercenaries and holding Momoko hostage - reveals himself as Adolf, the 18th soldier who murdered Elsas grandfather after the latter crippled him.

Upon arriving at the vault, Jackie uses the key and a secret code from Elsas grandfathers dog tag and opens it, revealing the elevator leading to the gold. Upon their discovery of the gold, the mercenaries turn their backs on Adolf with the intent of keeping the treasure to themselves. Adolf locks all of the mercenaries, except for two who chase Jackie to an underground wind tunnel. While Jackie battles the two mercenaries, Elsa and Ada flip random switches in the control room, activating the tunnels turbine fan. As the three men hang on for their lives, Elsa and Ada attempt to switch off the fan, but they accidentally trigger the bases self-destruct sequence. Adolf tells the quartet that they can escape by having the turbine blow them through the ventilation duct, but he decides to stay to atone for his sins. The quartet gather as much gold as they can, but the wind force only sends their bodies upward to the desert surface above before the base completely caves in.

As the quartet walk across the desert, they once again encounter Amon and Tasza, who demand water from them. Jackie throws them his empty canteen before they finally overcome their differences and try to find water in the Sahara desert.

==Cast==
* Jackie Chan as Jackie, a.k.a. "Asian Hawk" / "Asian Condor"
* Carol Cheng (aka Do Do Cheng) as Ada
* Eva Cobo de Garcia as Elsa
* Shoko Ikeda as Momoko
* Daniel Mintz as Amon
* Aldo Sambrell as Adolf (as Aldo Brel Sánchez)
* Bozidar Smiljanic as Duke Scapio / Baron Bannon
* Jonathan Isgar as Tasza
* Ken Goodman
* Steve Tartalia
* Vincent Lyn

===Jackie Chan Stunt Team===
* Benny Lai
* Ken Lo Mars

 
 

==Production==
Armour of God II: Operation Condor was filmed primarily in Madrid|Madrid, Spain, and Morocco. While the opening scenes where Asian Hawk went Powered paragliding was shot in Tagaytay City over Taal Lake, Cavite, Philippines. The scene where he stole the gem from the cave tribe and escaped by zorbing was shot in Mount Macolod in Cuenca, Batangas|Cuenca, Batangas.

According to his book I Am Jackie Chan: My Life in Action, while filming the underground base chase scene, Chan was supposed to swing to a platform with a long chain, but he lost balance and fell to the ground face-first, dislocating his sternum.   

==Box office and reception==
Chan said in his biography that the film cost HK $115 million, or US $15 million, the most expensive Hong Kong film at the time. In its Hong Kong theatrical release, this film grossed HK $39,048,711. On 1,523 North American screens on its opening weekend, it grossed US $4,731,751 ($3,088 per screen), on track to a modest U.S. $10,405,394 final gross. 

The film has received mostly positive reviews.,     with a 70% approval rating on Rotten Tomatoes. 

==Awards and nominations== 1992 Hong Kong Film Awards
**Nomination: Best Action Choreography

==Versions==
The film was originally released in Hong Kong in 1991 with a Cantonese soundtrack and a running time of approximately 106 minutes. An uncut export version of the film was released in the United Kingdom by Entertainment in Video. The VHS was released in 1993, and the DVD in 2001.

In 1991, Dimension Films acquired the U.S. rights, but did not release the film theatrically until 1997 under the title Operation Condor, with a newly commissioned English dub/score and 15 minutes deleted from the final cut. This version was released on DVD in 1999. 

In 2004, Intercontinental Video Limited released an uncut version in Hong Kong. The DVD is anamorphic and includes the Cantonese-language soundtrack with English subtitles.

==See also==
* Jackie Chan filmography
* List of Hong Kong films

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 