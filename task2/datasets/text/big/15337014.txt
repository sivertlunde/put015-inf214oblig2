The Whistle Blower
 
{{Infobox film
 | name           = The Whistle Blower
 | image          = The Whistle Blower.jpg Simon Langton John Hale (book) Julian Bond Barry Foster Gordon Jackson Sir John Gielgud David Langton James Fox
 | producer       = Geoffrey Reeve
 | distributor    =
 | released       = December 1986 (UK) 10 July 1987 (USA)
 | runtime        = 100m
 | country = UK
 | language = English
 | budget         =
 | gross          =  $1,500,000 
 |}} John Hale. Simon Langton, the son of actor David Langton, who co-stars in the film.

==Plot==
Frank Jones (Michael Caine|Caine) is a retired British naval officer and Korean War veteran, who is now a businessman. His bright but naive and idealistic son, Robert (Nigel Havers), works as a linguist at GCHQ, the top secret British intelligence listening station, using his love of Russian to listen to various pieces of communication on the other side of the Iron Curtain.

The film opens on Remembrance Day in Whitehall, as the war veterans line up to walk past the Cenotaph, then moves back to a conversation between Frank and his son at Roberts flat some months earlier, where Robert tells Frank that strange things are happening at GCHQ, and hes planning on leaving and marrying an older woman called Cynthia (Felicity Dean) with whom hes fallen in love.

Robert says a Soviet mole was found, and that security is all over the place encouraging people to rat on each other. The higher ups seem convinced that if they dont do something, their American friends in the CIA will stop working with them. Frank isnt thrilled over the marriage plans, and he tells his son before he leaves that its unlikely anything off key can be happening in the agency. Its obvious that Frank loves his son deeply and wants him to be happy, whatever he may choose for himself.
 Gordon Jackson) are listening to a tape recording of the conversation between Frank and his son.
 Barry Foster), who had joined MI6 after his service in the navy. Greig agrees to make discreet enquiries on his part.

Returning to Roberts flat, Frank is confronted by radical socialist journalist Bill Pickett (Kenneth Colley), who had arranged to meet Robert to discuss the problems at British Intelligence, but Frank rejects his investigative approaches. Frank is also told that he is in the running for a large government contract for his firm, with an implicit undertone that he not make waves about his sons death.

The rest of the film digs into an examination of the British establishment which is disturbing and ugly, and make Frank question his view of the country he loves. There are strong echoes of the Anthony Blunt case and the Cambridge spies. Frank, discreetly pursued by British Intelligence, finds men who easily consider others expendable if their ideas of class and privilege are endangered.

Pickett is also killed in mysterious circumstances in a traffic accident, having found out the name of the man who Robert wished him to meet  before meeting Frank. Frank is then approached by Roberts best friend and fellow British intelligence linguist Allen Goodburn (Andrew Hawkins) at Roberts funeral. Frank learns from Goodburn that it was his good friend Grieg who had approached him as to Roberts feelings for the service. Frank gets Grieg drunk and gets him to confess that he was at Roberts flat the night Robert died. Greig admits he was there as the service had something on him, but that his job was only to leave the door open and let "others" heavy-hand Robert, not kill him. he also reveals the name of the mole as Sir Adrian Chappel (John Gielgud).

Leaving Grieg in his drunken stupor, Frank is picked up by British Intelligence and driven to a country house, where he is confronted by Secretary to the Cabinet (David Langton) and Lord (James Fox). They explain to him that his son was out of control, and was killed as part of a plan to mislead the Americans to the extent of the depth of Russian intelligences operatives inside British operations, in the hope that they could continue to gain intelligence from the CIA. They have presently left the higher Russian operative in place, until they can assess the extent of the damage caused. They advise Frank that should he go public with any of this information, he and/or Roberts girlfriend Cynthia and her daughter will be killed or at least restrained.  

The film returns to the present, and the Remembrance Day parade. Frank confronts Chapple at his home in Whitehall, and gets him to confess to being a spy for Russia. Frank orders him to sign a full confession, which he does, but as Frank reads it, Chapple produces a gun and demands its return. Frank grabs the gun, which goes off and kills Chapple — leaving his signed confession to act as a suicide note and put Frank in the clear. He returns to the Remembrance Day parade.

The closing credits roll to an ambulance attending the death of Chapple, as Frank walks past the Cenotaph up Whitehall.

==Production==
The film was largely shot on location in Cheltenham, Gloucestershire; home of GCHQ, which forms the premise of the film. Cheltenham Racecourse, Cheltenham Crematorium and The Promenade feature in the film  

==Response==
Though it was given a limited release, the film opened to positive reviews. It has an approval rating of 86% on  .

== Cast ==
*Michael Caine - Frank Jones
*James Fox - Lord
*Nigel Havers - Robert Jones
*John Gielgud - Sir Adrian Chapple
*Felicity Dean - Cynthia Goodburn Barry Foster - Charles Greig Gordon Jackson - Bruce
*Kenneth Colley - Bill Pickett
*David Langton - Secretary to the Cabinet
*Dinah Stabb - Rose
*James Simmons (actor) - Mark
*Katherine Reeve - Tiffany Goodburn
*Bill Wallis - Ramsay Dodgson
*Trevor Cooper - Inspector Bourne Peter Miles - Stephen Kedge
*David Shaughnessy - Medical officer
*Patrick Holt - Irate driver

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 