Hard Asphalt
{{Infobox film
| name           = Hard asfalt
| image          = 
| caption        = 
| director       = Sølve Skagen
| producer       = John M. Jacobsen
| writer         = 
| starring       = Kristin Kajander Frank Krog
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          = 
}}
Hard Asphalt ( ) is a 1986 Norwegian drama film directed by Sølve Skagen. It was entered into the 15th Moscow International Film Festival.   

== Cast ==
*Kristin Kajander as Ida
*Frank Krog as Knut 
*Marianne Nielsen as Åse 
*Morten Faldaas as Bønna 
*Liv Heløe as May-Britt 
*Tone Schwarzott as Idas mor 
*Tom Tellefsen as Idas far 
*Svein Erik Brodal as Herr Abrahamsen 
*Gudrun Waadeland as Fru Abrahamsen 
*Sigve Bøe as Nordlie 
*Anne Marit Jacobsen as Dagmamma 
*Bernhard Ramstad as Kaspersen 

==References==
 

== External links ==
*  

 
 
 
 
 
 