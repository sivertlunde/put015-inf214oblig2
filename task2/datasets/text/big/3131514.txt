Leningrad Cowboys Go America
 
{{Infobox film
| name           = Leningrad Cowboys Go America
| image          = Leningrad Cowboys Go America.jpg
| image_size     = 190px
| caption        = German theatrical poster
| director       = Aki Kaurismäki
| producer       = Katinka Faragó Aki Kaurismäki Klas Olofsson
| writer         = Sakke Järvenpää Aki Kaurismäki Mato Valtonen
| narrator       = 
| starring       = Matti Pellonpää Kari Väänänen Leningrad Cowboys: Sakke Järvenpää Heikki Keskinen Pimme Korhonen Sakari Kuosmanen Puka Oinonen Silu Seppälä Mauri Sumén Mato Valtonen Pekka Virtanen Nicky Tesco as "lost cousin" 
| music          = Mauri Sumén
| cinematography = Timo Salminen
| editing        = Raija Talvio
| distributor    = Orion Classics
| released       =  
| runtime        = 78 min
| country        = Finland Sweden
| language       = English
| budget         = 
| gross          = 
}} Go West (1940). After the film was released, the fictional band transformed into a real band, complete with ludicrous hairstyles.
 
Leningrad Cowboys Go America was followed five years later by a sequel, Leningrad Cowboys Meet Moses (1994) and a concert film Total Balalaika Show (1994). 
The film was reissued on DVD in October 2011, as part of the Criterion Collections Eclipse series, paired with the other two Leningrad Cowboys films.

==Plot== New York, bringing with them a band member who had frozen the previous night while practicing outside.
 Cadillac Fleetwood 75 Limousine, strap the coffin carrying their frozen band member onto the roof and set off to earn their way through the Deep South, adapting their musical style to suit local tastes at each new location. All the while they are being driven on and exploited by their money and food hoarding manager Vladimir (Matti Pellonpää), who has a seemingly unlimited supply of beer in the ice-filled coffin. Meanwhile, Igor, who stowed away on the plane, follows the band by his own means of transportation. When he finally catches up with them, they appoint him as their road manager.
 Nicky Tesco) whose singing gives positive reception from the audience. They eventually make it to Mexico and perform their wedding gig, where the thawing bass guitarist is revived with a shot of tequila and joins the group on stage, as does Igor. Vladimir watches them play then wanders off, but the band finally finds success in Mexico, making the top ten.

==Cast==
American director Jim Jarmusch has a cameo as a car dealer. The film also includes cameos by blues guitarist Duke Robillard and American Rockabilly Hall of Famer, Colonel Robert Morris, with his wife Irene.

==Soundtrack==
 

A soundtrack album was released in 1989.

==Reception==
The film was ranked #88 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010. {{cite web 
| title = The 100 Best Films Of World Cinema – 88. Leningrad Cowboys Go America 
| url = http://www.empireonline.com/features/100-greatest-world-cinema-films/default.asp?film=88
| work = Empire 
}} 
 
It currently hold a 100% Fresh rating on review site Rotten Tomatoes based on 5 reviews. 

==References==
 

==External links==
*  
* 

 
 

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 