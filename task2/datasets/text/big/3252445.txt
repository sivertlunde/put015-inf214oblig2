Zero Effect
 
{{Infobox film
| name           = Zero Effect
| image          = Zero Effect.jpg
| caption        = theatrical poster
| director       = Jake Kasdan
| producer       = Lisa Henson Jake Kasdan Janet Yang
| writer         = Jake Kasdan
| narrator       = Bill Pullman
| starring       = Bill Pullman Ben Stiller Ryan ONeal Kim Dickens Angela Featherstone
| music          = The Greyboy Allstars
| cinematography = Bill Pope
| editing        = Tara Timpone
| distributor    = Columbia Pictures
| released       = January 30, 1998
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = US$5,000,000
| gross          = US$2,087,471 (USA)
}}

Zero Effect is a 1998 mystery film written and directed by Jake Kasdan (son of writer/director Lawrence Kasdan). It stars Bill Pullman as "the worlds most private detective" Daryl Zero and Ben Stiller as his assistant Steve Arlo. The plot of the film is loosely based on the Arthur Conan Doyle short story "A Scandal in Bohemia".

The film was shot in Portland, Oregon. It was scored by The Greyboy Allstars. It was screened in the Un Certain Regard section at the 1998 Cannes Film Festival.   

==Plot==
Daryl Zero (Bill Pullman) is the worlds greatest detective, but is also a socially maladroit misanthrope. Among his quirks is that he never meets or has direct contact with his clients, instead conducting business through his assistant, Steve Arlo (Ben Stiller). Throughout the movie, Zero provides narration as he reads lines from his proposed autobiography.
 EMT with a mysterious past. Zero refuses to reveal Glorias identity to Stark until Zero understands why she is blackmailing Stark. Zero becomes attracted to Gloria, compromising his trademark objectivity.

Stark pressures Arlo to reveal the blackmailers identity so that he can have that person killed. Arlo must also deal with Zeros somewhat absurd demands on his time, which increasingly interfere with Arlos relationship with his girlfriend Jess.

Zero eventually discovers that Stark had been obsessed with Glorias mother following a brief romantic relationship when the two were in college. She later blackmailed Stark with the threat of exposing him as a rapist, so he had her killed. However, she had already given birth to their daughter Gloria, who was discovered and raised by the hitman who killed her mother. Gloria grew up knowing that Stark was behind her mothers murder, and when her adoptive father contracted a terminal illness, she used the information to pay for medical treatment. At the meeting to deliver the final blackmail payment, Stark collapses from a heart attack and Gloria is compelled to save his life. She then flees the country with Zeros assistance.

==Cast==
*Bill Pullman as Daryl Zero
*Ben Stiller as Steve Arlo
*Ryan ONeal as Gregory Stark
*Kim Dickens as Gloria Sullivan
*Angela Featherstone as Jess

==Soundtrack==

Released in January 1998, the official soundtrack for the film includes:

# "Mystery Dance" – Elvis Costello
# "One Dance" – Dan Bern
# "Starbucked" – Bond
# "Into My Arms" – Nick Cave and the Bad Seeds
# "Some Jingle Jangle Morning" – Mary Lou Lord
# "Emma J" – Brendan Benson
# "The Method Pt. 2" – The Greyboy Allstars
# "Drifting Along" – Jamiroquai
# "Till You Die" – Candy Butchers
# "Lounge" – Esthero
# "Blackmail Drop" – The Greyboy Allstars
# "Three Days" – Thermadore
# "Rest My Head Against the Wall" – Heatmiser
# "The Zero Effect" – The Greyboy Allstars

==Television pilot==
In 2002 Kasdan attempted to resurrect the character Daryl Zero for the NBC television network.    He shared the screenwriting duties with Walon Green and directed the pilot. He was also one of the producers. The series was intended to be a prequel, tracing the early adventures of Zero as he and Arlo became a team.  The pilot stars Alan Cumming as Daryl Zero and features Krista Allen and Natasha Gregson Wagner. NBC did not pick up the pilot.

==References==
 

==External links==
* 
* 
* 
* 
* 
 
 
 

 
 
 
 
 
 
 
 
 
 
 