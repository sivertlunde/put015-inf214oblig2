Two Loves
 
 
{{Infobox film
| name           = Two Loves
| image          = Two Loves FilmPoster.jpeg
| caption        = Theatrical release poster
| director       = Charles Walters
| producer       = Julian Blaustein
| screenplay     = Ben Maddow
| based on       =  
| starring       = Shirley MacLaine Laurence Harvey Jack Hawkins Nobu McCarthy
| music          = Bronislau Kaper
| cinematography = Joseph Ruttenberg
| editing        = Fredric Steinkamp
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $2 million 
| gross          = $1 million 
}}
Two Loves is a 1961 American drama film directed by Charles Walters. It is based on the book Spinster by Sylvia Ashton-Warner. It was entered into the 11th Berlin International Film Festival.   

==Cast==
* Shirley MacLaine as Anna Vorontosov
* Laurence Harvey as Paul Lathrope
* Jack Hawkins as William W.J. Abercrombie
* Nobu McCarthy as Whareparita
* Ronald Long as Headmaster Reardon
* Norah Howard as Mrs. Cutter
* Juano Hernandez as Rauhuia

==Reception==

===Box office===
The film earned $425,000 in the US and Canada and $650,000 elsewhere resulting in a loss of $1,773,000.  . 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 