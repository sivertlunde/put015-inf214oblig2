Inspector Pratap
{{Infobox film
| name           = Inspector Pratap
| image          =
| caption        =
| writer         = M.V.S. Haranatha Rao  
| story          = Krishna Chitra Unit
| screenplay     = Muthyala Subbaiah
| producer       = Y. Anilbabu
| director       = Muthyala Subbaiah
| starring       = Nandamuri Balakrishna Vijayashanti Chakravarthy
| cinematography = Nandamuri Mohana Krishna
| editing        = Gautham Raju
| studio         = Krishna Chitra
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}

Inspector Pratap is a 1988 Telugu cinema|Telugu, Action film produced by Y. Anilbabu on Krishna Chitra banner and directed by Muthyala Subbaiah. Starring Nandamuri Balakrishna, Vijayashantiin the lead roles and music composed by K. Chakravarthy|Chakravarthy. The film recorded as a flop at box office.         

==Cast==
 
*Nandamuri Balakrishna as Inspector Pratap 
*Vijayashanti as Chukka Satyanaryana as Viswarupam Jaggayya as Commissioner
*Gollapudi Maruthi Rao as Lawyer Ramanatham Rallapalli as Narahari
*Giri Babu as Murahari
*Sarath Babu as Kranthi
*Narra Venkateswara Rao as C.I.
*Chalapathi Rao as Pakir
*Nizhalgal Ravi as Ramki
*Prasad Babu as Prasad
*P. J. Sarma as I.G
*Bhemiswara Rao as Jaganatham
*Suthi Velu as Constable Rangaiah
*Potti Prasad  Chitti Babu
*Ramana Reddy 
*CH Krishna Murthy as Constable
*Eeswar Rao 
*Vidyasagar
*Srividya as Janaki
*Varalakshmi as Baby Mucharlla Aruna 
*Y. Vijaya
 

==Soundtrack==
{{Infobox album
| Name        = Inspector Pratap
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1987
| Recorded    = 
| Genre       = Soundtrack
| Length      = 24:24
| Label       = LEO Audio Chakravarthy
| Reviews     =
| Last album  = Bhanumatgari Mogudu   (1988)  
| This album  = Inspector Pratap   (1988)
| Next album  = Donga Ramudu   (1988)
}}
 Chakravarthy and the lyrics were written by Veturi Sundararama Murthy. The music released by LEO Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Alaa Choodaboku SP Balu, S. Janaki
|4:14
|- 2
|Ninnu Yaado Choosina SP Balu, P. Susheela
|4:04
|- 3
|Ranga Ranga SP Balu,P. Susheela
|3:50
|- 4
|Thuntari Vaada
|P. Susheela
|3:59
|- 5
|Thagubothunayala SP Balu
|4:09
|- 6
|Takadhim Mano (singer)|Mano,P. Susheela
|4:08
|}

==Others== Hyderabad

==References==
 

 

 
 
 


 