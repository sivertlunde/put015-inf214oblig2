Demons of War
{{Infobox film
| name           = Demons of War
| image          =Demony wojny DVD RGB.jpg
| image size     = 
| alt            = 
| caption        = Movie poster
| director       = Władysław Pasikowski
| producer       = Włodzimierz Otulak
| writer         = Władysław Pasikowski
| screenplay     = Władysław Pasikowski
| story          = 
| narrator       =  Artur Żmijewski
| music          = Marcin Pospieszalski
| cinematography = Paweł Edelman
| editing        = Wanda Zeman
| studio         = 
| distributor    = Vision Film Distribution
| released       =   
| runtime        = 97 minutes
| country        = Poland
| language       = Polish, French, Serbian
| budget         = 
| gross          = 
}}
 Polish war drama by Władysław Pasikowski set during the Bosnian War.

==Plot==
After the Bosnian War, Bosnia and Herzegovina is occupied by the NATO-led IFOR|(IFOR) Implementation Force.  In February 1996, a unit of Polish IFOR troops detains and releases three foreign mercenaries in Srebrenica, before they can be executed by a Bosnian mob.  The Polish unit is led by Major Edward "Edek" Keller (Boguslaw Linda).  Soon later, Keller is under investigation for insubordination and for clashes with the Bosnian militia and foreign mercenaries.  The investigation is led by two arriving officers - Lieutenant Czacki (Olaf Lubaszenko) and Major Czesław Kusz (Tadeusz Huk), who will replaced Keller as the commanding officer of the battalion on 1 March.  The investigation comes at a sensitive time for Polish forces, as the Polish government tries to become a member of NATO.  Keller maintains command of the unit, until his commission ends.

Upon Czacki and Kuszs arrival, the unit is receives a distress call from a downed Norwegian helicopter.  Keller assembles his unit for a search and rescue mission to the downed chopper.  However, his mission is rejected by the IFOR command.  Despite the order, Keller ignores it and goes on with the mission.  After locating the helicopter, members of the crew are found dead.  Keller and the unit goes after the militia that killed the helicopter crew.  The unit manages to locate a group of twenty Bosnian militia fighters, that are led by Skija (Slobodan Custic), a foreign mercenary.  The militia is also holding two captives, a young French female journalist named Nicol (Aleksandra Niespielak) and a male Bosnian press representative named Dano Ivanov (Denis Delic).  Keller, himself, manages to stealthily kill one of the militiamen and rescue the captives.  The unit then heads for a helicopter extraction, while the militia discovers that someone killed one of their fighters.  Skija sends his men to hunt them down and locate an important missing videotape, that was held by Ivanov.

During extraction, the units helicopter is shot down before landing.  Immediately, the unit is ambushed by the militia and several troops are wounded.  The unit manages to eliminate some of the fighters and break out.  After the engagement, some of the Polish soldiers argue with Keller that they are just a peacekeeping force and wish not to fight.  However, Keller manages to keep them in line.  The unit continues on foot and then establishes camp at a farm.  There, Keller and the unit discover that Ivanov was in possession of videotape that the militia is trying to acquire.  According to Nicol, the tape shows a mass execution of a Bosnian village ordered by the Bosnian Prime Minister.  The next morning, Ivanov kills one of Kellers soldiers and escapes the camp.  As the unit heads for their base, they are ambushed yet again by the militia and Keller is forced to give up the important videotape.  Keller and his unit are spared by the militia, and then go after Skija and the militia, who are trying to burn down the Gypsy village where Keller and his unit had established camp earlier.  Keller and his unit attack the militia at the village.  Keller eventually confronts Skija and it is reveal that Ivanov promised to bribe one of Kellers officers for the videotape.  Keller then kills Skija after being saved by Kusz.  After arriving at the base, the cost and pain of warfare takes a toll on the unit.  It is also revealed that the unit retrieved the videotape, and Ivanov was actually in possession of a Polish pornography tape.  Keller is then relieved of his command, and Kusz takes control of the Polish battalion.  Before heading back to Poland, Keller gives the tape to the French journalist Nicol.  The film closes with Keller sitting on the plane with several coffins of dead soldiers from his unit.

==Awards==
*Wanda Zeman: Polish Film Awards (nominee) - best editing (1999)
*Paweł Edelman: Polish Film Awards (nominee) – best photography (1999)
*Marcin Pospieszalski: Polish Film Awards (nominee) – best score (1999)

==External links==
*  

 
 
 
 
 
 