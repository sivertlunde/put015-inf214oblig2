The Scarecrow (1982 film)
 
 
{{Infobox film 
| name           = The Scarecrow 
| director       = Sam Pillsbury 
| starring       = John Carradine Jonathan Smith Tracy Mann Daniel McLaren 
| writer         = Michael Heath
| based on       =  
| producer       =  
| music          = 
| cinematography = 
| editing        = 
| runtime        =  
| released       =  
| country        = New Zealand
| language       = English 
| budget         = 
}}

The Scarecrow is a 1982 New Zealand film, also known as Klynham Summer in America. It was directed by Sam Pillsbury based on the 1963 horror novel by New Zealand author Ronald Hugh Morrieson. 

==Plot==
One night a girl is slain in the woods of a small town, two teenagers, Sam and Les, inadvertently cross the killers path while robbing the hens of Victor the school bully. According to Sam Edwards the film is not as bleak as the novel. Small-town New Zealand in the 1950s is puritanical on the surface but depraved to its depths.

==Cast member|Cast== Jonathan Smith .... Ned Poindexter
*Tracey Mann .... Prudence Poindexter
*Daniel McLaren .... Les Wilson
*John Carradine .... Hubert Salter
*Bruce Allpress .... Uncle Athol
*Philip Holder .... Constable Ramsbottom Stephen Taylor .... Herbert Poindexter
*Desmond Kelly.... Mr. Poindexter
*Anne Flannery .... Mrs. Poindexter
*Denise OConnell .... Angela Potroz
*Jonathan Hardy .... Charlie Dabney
*Martyn Sanderson .... Ned as Adult (voice)
*Greer Robson .... Lynette
*Roy Billing .... Mr. Potroz
*Greg Naughton .... Victor Lynch 
*Mark Hadlow .... Sam Finn

==References== 
*Scarecrow: A Film Study Guide by Brian McDonnell (1982, Longman Paul Auckland) ISBN 0582683734 
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p81 (1997, Oxford University Press, Auckland) ISBN 019 558336 1  
 

==External links==
* 
* 
*  

 
 
 
 
 

 
 