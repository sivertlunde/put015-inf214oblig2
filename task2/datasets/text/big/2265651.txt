Shikoku (film)
 
{{Infobox film
| name           = Shikoku
| image          =
| caption        =
| director       = Shunichi Nagasaki
| producer       =
| writer         = Kunimi Manda Takenori Sento
| narrator       =
| starring       = Chiaki Kuriyama Michitaka Tsutsui Yui Natsukawa
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 100 minutes
| country        = Japan
| language       = Japanese
| budget         =
}}
  is a supernatural thriller film written and produced in Japan in 1999. It was directed by Shunichi Nagasaki and written by Kunimi Manda and Takenori Sento.

==Title Significance==
The title is a play on words. In Japanese, depending on which kanji is used, "shikoku" can mean "four countries" (which is the name of the island where the movie is set) or it can mean "land of the dead". The tagline used for the film was "Some things are better left dead...", with the genre of the film fitting into Horror film|Horror/Thriller (genre)|Thriller.

==Plot summary==
Years after moving to Tokyo with her parents, Hinako returns to her hometown in rural Shikoku. She soon learns that her childhood friend, Sayori, died several years ago and that Sayoris mother, who used to perform seances and exorcisms, has gone almost insane with grief. After seeing Sayoris yūrei several times during the night, Hinako consults with some local experts on the paranormal and discovers that Sayoris mother has something planned for her daughter.

==Cast==
*Yui Natsukawa as Hinako Myoujin
*Michitaka Tsutsui as Fumiya Akizawa 
*Chiaki Kuriyama as Sayori Hiura 
*Toshie Negishi as Teruko Hiura (Sayoris mother) 
*Ren Osugi as Yasutaka Hiura (Sayoris father) 
*Makoto Satô as Sendo 
*Taro Suwa as Oda 
*Tomoko Otakara as Yukari Asakawa 
*Haduki Kôzu as Chizuko Oono

== External links ==
*  

 
 
 
 
 
 
 
 


 
 