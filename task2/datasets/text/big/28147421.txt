Glowing Stars
{{Infobox film
| name           = Glowing Stars
| image          = Glowing Stars.jpg
| alt            = 
| caption        = Swedish DVD cover, featuring Jenna (left) and Ullis (right)
| director       = Lisa Siwe
| producer       = Anders Landström
| screenplay     = Linn Gottfridsson
| based on       =  
| starring       = Josefine Mattsson Mika Berndtsdotter Ahlén Annika Hallin
| music          = Kalle Bäccman
| cinematography = 
| editing        = 
| studio         = Filmlance International AB
| distributor    = Sandrew Metronome Distribution Sverige AB
| released       =  
| runtime        = 89 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}} novel of the same name by author Johanna Thydell. It revolves around a girl named Jenna who has to deal with her teenage life while at the same time taking care of her mother who is sick with cancer. The film was directed by Lisa Siwe and written by Linn Gottfridsson. It premiered in Swedish theaters on 30 January 2009 and received critical acclaim from critics. Glowing Stars won several film awards.

==Plot==
Jenna is a girl currently in the seventh grade. Like a normal teenage girl, she worries about her breasts not growing, why she is not as popular as Ullis, and how she can get Sakke to fall in love with her or at least notice that she exists. When Jennas mother is diagnosed with cancer, they are forced to move to Jennas grandmother, who Jenna finds annoying. Jennas grandmother lives next door to Ullis, who is living with her alcoholic mother. A friendship begins to grow between Jenna and Ullis after they realize that they both have struggling mothers. Eventually, Jennas mother passes away at the hospital. 

Glowing Stars deals with the difficulty of losing a loved one to death. But it is also a film about friendship, identity, and survival.

==Cast==
* Josefine Mattsson as Jenna
* Mika Berndtsdotter Ahlén as Ullis
* Annika Hallin as Jennas mother
* Anki Lidén as Jennas grandmother
* Samuel Haus as Sakke
* Judith Rindeskog as Susanna 
* Charlie Gustafsson as Oscar
* Marcus Jansson as Henke
* Nina Christensen as Carro

==Production==
The film was directed by Lisa Siwe and written by Linn Gottfridsson.    It was their feature-length film debut, and they worked closely together as a team on the film. They told the newspaper Gotlands Allehanda that they complement each other well; Siwe "has strong ideas" while Gottfridsson "is really good at psychology".    They also explained that they met "thanks to a teacher at Dramatiska Institutet, who brought us together. It was a lucky match. We like the same type of films and we want to tell the same type of stories."  

Siwe began reading the 2003 novel I taket lyser stjärnorna by Johanna Thydell late at night one day and she could not put it away until she had read the whole book. "I cried and cried and if my husband had not laid beside me I would have shrieked right out," she told Dagens Nyheter.    After finishing the novel, Siwe received the idea to adapt it into a film together with Gottfridsson. Once they acquired the rights from the author they began planning the film. "First, Linn and I talked together on what we wanted to keep  . The book cannot be translated into film. We have great respect for the book and the film must stand on its own legs. And even if we have  , we have stayed faithful to the essence," Siwe said. 

Josephine Mattson was cast in the lead role of Jenna and it became her acting debut. Siwe searched a long time for an actor to play the role, and she was not pleased until she found Mattson. She explains that the reason for that is that Mattson was a regular girl and not an "actor-wannabe".  The film was shot in Trollhättan and Vänersborg. Filming began in June 2008.    According to Siwe, the hospital scenes largely affected the film team emotionally, and a lot of crew members were crying during the filming. 

The pre-production, production, and post-production of Glowing Stars lasted for five years.    After the film was completed, Siwe told Norrköpings Tidningar that "I am proud of the acting, the manuscript, the photography — and that I was able to make the film. It has been a heavy ride. But I didnt feel that until afterwards."  She also commented on the writing process: "I didnt want to stop until I had a really good manuscript because I felt such strong respect for Johanna and her book. Johanna has never said that she wrote a childrens book, and adults can also read it with great enjoyment. I want to achieve the same thing with the film." 

==Reception==
The world premiere of Glowing Stars occurred at the 2009 Gothenburg Film Festival, and the film was met with much praise from critics. The acclaim continued after it was released in theaters.  Christer Uggeldahl of Hufvudstadsbladet gave the film four out of five stars, praising the director and the cast. He wrote that the actors are "as bold as they are skillful", and added that, because of her feeling for nuances, it is hard to tell that this is Siwes directing debut. "Here we have a director who knows a girls room inside and out and who knows how to switch between images and words, between the quietness and the pop soundtrack."  Åsa Johansson of Dalarnas Tidningar also praised the film, commenting that the "fantastically well-written dialog" and the debuting actors brought the characters to life. She added that the film made her cry, in part because Josefine Mattsson was able to make Jennas frustration seem so complex. 
 The Girl with the Dragon Tattoo and Prinsessa.  Glowing Stars was also nominated for "Best Picture" at the Guldbagge Awards. This award, however, is given by a jury and not regular people as with the Gothenburg Film Festival.  The film lost to The Girl with the Dragon Tattoo.    Siwe, however, won the Guldbagge Award for "Best Director", and Anki Lidén (Jennas grandmother) was awarded with "Best Actress in a Supporting Role". 

The film also won awards at international festivals, including the Canadian "Students Choice Award" at Sprockets Toronto International Film Festival for Children and the Norwegian "Don Quijote Award" at Kristiansand International Childrens Film Festival. 

Glowing Stars debuted in second place on the Swedish box office chart. 

==References==
:Most references are in Swedish
 

==External links==
*  

 
 
 
 
 
 