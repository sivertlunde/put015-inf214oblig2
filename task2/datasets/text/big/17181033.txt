Mr. Wong in Chinatown
 
{{Infobox film
| name           = Mr. Wong in Chinatown
| caption        = Film poster
| image	         = Mr. Wong in Chinatown FilmPoster.jpeg
| director       = William Nigh
| producer       = William T. Lackey Scott Darling
| starring       = Boris Karloff Marjorie Reynolds
| music          = Edward J. Kay
| cinematography = Harry Neumann
| editing        = Russell F. Schoengarth
| distributor    = Monogram Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States 
| language       = English
| budget         = 
}}

Mr. Wong in Chinatown is a 1939 American mystery film directed by William Nigh and starring Boris Karloff as Mr. Wong (James Lee Wong)|Mr. Wong. Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 246 

==Cast==
* Boris Karloff as Mr. James Lee Wong
* Marjorie Reynolds as Roberta Bobbie Logan (reporter)
* Grant Withers as Police Capt. Bill Street
* Huntley Gordon as Mr. Davidson (bank president) George Lynn as Capt. Guy Jackson (Aviation Corp. president) (as Peter George Lynn)
* William Royle as Capt. Jaime (captain, Maid of the Orient)
* James Flavin as Police Sgt. Jerry
* Lotus Long as Princess Lin Hwa (murder victim)
* Lee Tung Foo as Willie (Wongs servant) (as Lee Tong Foo)
* Bessie Loo as Lilly May (Princess Lin Hwas maid)
* Richard Loo as Tong chief
* Ernie Stanton as Burton (Davidsons butler)
* I. Stanford Jolley as Hotel clerk

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 