One Way (film)
{{Infobox film
| name           = One Way
| image          = ONE WAY.png
| caption        = 
| director       = Reto Salimbeni
| producer       = Til Schweiger
| writer         = Reto Salimbeni
| narrator       = 
| starring       = Til Schweiger Michael Clarke Duncan Lauren Lee Smith Eric Roberts
| music          = 
| cinematography = Paul Sarossy
| editing        = 
| distributor    = Universal International Pictures
| released       =  
| runtime        = 116 minutes
| country        = Germany
| language       = 
}}
One Way is a 2006 crime drama/psychological thriller film written and directed by Reto Salimbeni and produced by Til Schweiger. The film stars Schweiger, Michael Clarke Duncan, Lauren Lee Smith and Eric Roberts. The international production was shot in Toronto, New York City, Vancouver and Cologne and was co-produced and distributed by Universal International Pictures.

==Synopsis==
Eddie Schneider (Til Schweiger) is a charismatic and successful creative director at a top advertising agency in New York. A master of communication, his pitching skills are legendary, and he never loses an account.  Eddie is engaged to Judy Birk (Stephanie von Pfetten), the boss’s daughter. Eddie’s life looks perfect, but internally he battles an addiction to sex with no success. His addiction has led him into a life of lies and deceit. Although struggling with his dark side, Eddie thinks his infidelities are victimless crimes and justifies them with his business success.

After winning a huge pitch for the agency, Eddie discovers his co-worker and friend Angelina (Lauren Lee Smith) has been raped by Judy’s brother Anthony (Sebastien Roberts).    The Birk family bring in their top lawyer to make sure Anthony is found innocent – but to ensure his freedom Anthony blackmails Eddie with evidence of Eddie’s countless affairs, demanding Eddie testifies in court on his behalf. To maintain his perfect life and upcoming marriage – Eddie helps Anthony get off the hook.

When Anthony is found not guilty, his victim Angelina is devastated and tries to commit suicide. At the last moment an army general (Michael Clarke Duncan) saves her life. Only Angelina sees The General – he is a powerful, revenging, guiding angel of heaven and hell. An ethereal force.

Judy finds out about Eddie’s affairs and breaks off the engagement; her father (Art Hindle) fires him. Eddie is cut out of the world he created for himself.
 anal sex on him with a device. Knowing she would never be safe from him after her revenge, she shoots and kills him. She orchestrates the assault so that Eddie is the main suspect, thus getting her revenge on him for his failure to testify against Anthony.  With no support from former family and friends, Eddie finds himself in prison, facing trial with no way out.

Even after he’s exposed as an obsessive adulterer in court, Eddie refuses to testify against Angelina and he looks doomed to suffer a lifelong jail sentence. In a heart-gripping climax Judy comes to Eddie’s rescue providing evidence to prove his innocence.

==Locations==
One Way was shot in Toronto, New York, Vancouver and Cologne (interiors). 

==Accolades==
Til Schweiger (best male actor) and Reto Salimbeni (best screenplay) were nominated in the pre-selection for the German Film Award.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 