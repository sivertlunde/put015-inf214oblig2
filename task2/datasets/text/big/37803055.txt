The Durango Kid (film)
{{Infobox film
| name           = The Durango Kid
| image          = 
| alt            = 
| caption        = 
| director       = Lambert Hillyer
| producer       = Jack Fier
| writer         = 
| based on       = 
| screenplay     = Paul Franklin
| narrator       =  Kenneth MacDonald
| music          = Bob Nolan Tim Spencer
| cinematography = John Stumar
| editing        = Richard Fantl
| studio         = 
| distributor    = Columbia Pictures Corporation
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western directed by Lambert Hillyer, starring Charles Starrett, Luana Walters and Kenneth MacDonald. This is the first of 65 Durango Kid movies Starrett made at Columbia Pictures.   

==Plot summary==
 

==Cast==
* Charles Starrett as Bill Lowry / The Durango Kid  
* Luana Walters as Nancy Winslow   Kenneth MacDonald as Mace Ballard  
* Francis Walker as Henchman Steve  
* Forrest Taylor as Ben Winslow  
* Melvin Lang as Marshal Dan Trayboe  
* Bob Nolan as Bob - Member of Sons of the Pioneers  
* Pat Brady as Pat - Member of Sons of the Pioneers  
* Frank LaRue as Sam Lowry  
* Sons of the Pioneers as Ranch Hands and Musicians

== Notes==
Columbia and  Starrett also made The Return of the Durango Kid (1945), Landrush (1946), Quick on the Trigger (1948), Laramie (1949), Renegades of the Sage (1949), Horsemen of the Sierras (1949), Streets of Ghost Town (1950), Texas Dynamo (1950), Lightning Guns (1950), Frontier Outpost (1950), Raiders of Tomahawk Creek (1950), Across the Badlands (1950), Junction City (1952).

==References==
 	

== External links ==
*  
*  
*  

 
 
 
 
 
 
 