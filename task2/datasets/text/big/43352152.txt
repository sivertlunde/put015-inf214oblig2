The Sweet Body of Deborah
{{Infobox film
 | name = The Sweet Body of Deborah
 | image = The Sweet Body of Deborah.jpg
 | caption =
 | director = Romolo Guerrieri
 | writer = Ernesto Gastaldi based on =  
 | story  =Ernesto Gastaldi Luciano Martino 
 | starring =   
 | music = Nora Orlandi
 | cinematography = Marcello Masciocchi
 | editing =   
 | producer = Mino Loy Luciano Martino executive Sergio Martino country = Italy France studio =  Zenith Cinematografica Flora Film distributor = Warner Brothers-Seven Arts (US)
 | released =  
 | language =   Italian gross = $1.6 million (Italy) What Ever Happened to Baby Doll?: An American in Rome What Ever Happened to Baby Doll?
By ALFRED FRIENDLY Jr.ROME.. New York Times (1923-Current file)   29 June 1969: D11.  
 }}
 1968 Italian giallo film directed by Romolo Guerrieri.         

==Plot==
Deborah and Marcel are honeymooning in Europe. Marcel is haunted by the memory of the death of Susan, his former fiancee. 
== Cast ==

* Carroll Baker as Deborah 
* Jean Sorel as Marcel  George Hilton as  Robert 
* Ida Galli as  Suzanne Boileau (credited as Evelyn Stewart) 
* Luigi Pistilli as  Philip 
* Michel Bardinet as  The Inspector
* Mirella Pamphili as Telephone Clerk

==Reception==
The film was a box office hit in Italy, inspiring a number of similar thrillers starring Baker. It did not do very well in the UK and US however. 
==References==
 

==External links==
* 
*  at New York Times
*  at Giallo Files
*  at TCMDB
*  at European Film Review
 
 
 
 
 
 
 


 