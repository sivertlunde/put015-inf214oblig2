Stagecoach (1986 film)
{{Infobox television film
| name           = Stagecoach
| image          = Stagecoach_(1986_film).jpg
| caption        = DVD cover
| director       = Ted Post Jack Thompson
| screenplay     = James Lee Barrett
| based on       =  
| story          = 
| starring       = Willie Nelson   Kris Kristofferson   Johnny Cash
| music          = David Allan Coe   Willie Nelson
| cinematography = Gary Graver	
| editing        = Geoffrey Rowland
| studio         = Heritage Entertainment Inc.   Plantation Productions   Raymond Katz Production
| distributor    = Americana Entertainment   Columbia Broadcasting System   Starmaker Video
| released       =  
| runtime        = 100 minutes
| rating         = Unrated
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Stagecoach is a 1986 made-for-TV film.
 John Schneider as Buck.

There are some character name changes, but the plot is roughly based on that of the original film.  
Plot changes include:

*The Doc character is Doc Boone MD in the original, but he is Doc Holliday, a dentist in the remake.
*In the original, Peacock, the whiskey salesman, travels all the way to Lordsburg, New Mexico|Lordsburg, but here he leaves the coach at the first stop.
*Hatfield, the gambler, survives, whereas he is killed in the original.
*Gatewood the banker, meanwhile survives in the original but is killed here.
*Ringo deals with Luke Plummer alone in the original, but in the remake he is assisted by the Marshal, Hatfield and Doc.
*Ringo is still technically a jail-breaking criminal when the Marshal allows him to escape in the original, but his innocence has been proved when Luke Plummer asks the Marshal "Howd they find out he didnt do it?" in the remake.

==External links==
*  at the Internet Movie Database

 

 
 
 
 


 
 