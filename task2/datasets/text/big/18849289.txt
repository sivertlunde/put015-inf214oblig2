Rajakumaari
{{Infobox film
| name = Rajakumaari
| image = 
| director = A. S. A. Samy
| writer = A. S. A. Samy
| starring = M. G. Ramachandran K. Malathi M. R. Saminathan T.S.Balaiah M. N. Nambiar
| producer = Jupiter Pictures
| music = S. M. Subbaiah Naidu
| cinematography = W. R. Subba Rao U. Krishnan
| editing = D. Durairaj
| distributor = 
| released = 11 April 1947
| runtime = 134 mins
| country = India Tamil
| budget = 
}}

Rajakumaari is a Tamil language film starring M. G. Ramachandran and K. Malathi in the lead role. The film was released in the 1947.

==Cast==
* M. G. Ramachandran
* K. Malathi
* M. R. Saminathan
* T. S. Balaiah
* M. N. Nambiar
* Thavamani Devi
* ‘Pulimoottai’ Ramaswami
* Sandow M. M. A. Chinnappa Thevar

==Production==
"Rajakumari" was MGRs 15th film and first film as leading actor. Director of this film, ASA Samy arranged a wrestler called Kamaludeen to participate in a fight sequence for the film. But MGR insisted to have Sandow M. M. A. Chinnappa Thevar who had been acting in small roles to do the role. At first director was not interested to have him in the film, but later agreed. 

==References==
 

 
 
 
 


 