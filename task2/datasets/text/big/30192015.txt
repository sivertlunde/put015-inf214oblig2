The Stranger (1995 film)
{{Infobox Film
| name           = The Stranger
| image          = 
| image_size     = 
| caption        = 
| director       = Fritz Kiersch
| producer       = Donald P. Borchers Gregory Poirier
| writer         = Gregory Poirier
| narrator       = 
| starring       = Kathy Long Andrew Divoff Eric Pierpoint Robin Lynn Heath Ash Adams Ginger Lynn Allen Hunter von Leer
| music          = Kevin Kiner
| cinematography = Christopher Walling
| editing        = Tony Mizgalski
| studio         =
| distributor    = Columbia TriStar Home Entertainment
| released       = March 1995  (U.S.) 
| runtime        = 98 minutes
| country        =  English
| budget         = 
| gross          = 
}}
 1995 martial arts-action film directed by Fritz Kiersch, starring former professional kickboxing champion Kathy Long.

==Plot==
For years, the tiny desert town of Lakeview, Arizona has been ruled by a vicious outlaw biker gang led by a man known as Angel (Andrew Divoff). The residents have been terrified into submission, and the local Sheriff Gordon Cole (Eric Pierpoint) hasnt been the same since the gang raped and murdered his fiancée Bridget for trying to bring evidence of their crimes to the FBI. Tormented by grief and alcoholism, he no longer has the will to stand against them.

One day, a mysterious young woman dressed in black leather (Kathy Long) rides into town on a motorcycle and promptly starts killing members of Angels gang. When Cole attempts to question her about the murders, he notices her striking resemblance to his fiancée and is instantly attracted to her, much to the chagrin of local shopkeeper Sally Womack (Ginger Lynn Allen), who has wanted Cole for herself ever since Bridgets demise.

Fearing retaliation from Angel (who is currently out of town) for the deaths of his men, Sally implores Cole to arrest the woman, but he refuses to take any action against her. Undeterred, Sally enlists the help of Coles corrupt Deputy Steve Stowe (Ash Adams) and the Mayor of Lakeview (Hunter von Leer) to capture her, but their plan ultimately backfires resulting in Stowes death. Meanwhile the woman, a skilled hand-to-hand combatant, continues to eliminate the bikers one by one as she awaits Angels return.

During her time in Lakeview, the woman gets acquainted with several of the townspeople, most notably Bridgets younger sister Gordet (Robin Lynn Heath), who has been living as a vagrant ever since witnessing her sisters brutal death. After hearing her story, the woman takes the emotionally shattered girl under her wing and tries to help her reclaim her life.

Eventually Cole becomes romantically involved with the woman, but is unable to learn anything about her origin. She is undoubtedly the mirror image of his fiancée Bridget, but if the two are related, the woman gives no indication. However, when the subject of Angel is brought up, she angrily admonishes Cole for his inaction, causing him to storm off in a rage. When he tries to confess his love to her, she cryptically responds that hes "in love with a ghost".

With Angels return imminent, the residents of Lakeview begin a full-scale evacuation. Though Sally tries to convince Cole to leave with her, he chooses to stay behind and help the woman defend the town. With Gordet acting as a lookout, Cole and the woman are able to work together to dispatch the few remaining members of Angels gang. Finally, the woman meets Angel face to face on the street, where they fight to the death. Victorious, the woman bids farewell to Cole and Gordet and rides off into the sunset, leaving both of them and the audience to speculate about her identity.

==Partial cast==
*Kathy Long as The Stranger 
*Andrew Divoff as Angel
*Eric Pierpoint as Sheriff Gordon Cole
*Robin Lynn Heath as Gordet 
*Ash Adams as Deputy Steve Stowe
*Ginger Lynn Allen as Sally Womack
*Hunter von Leer as Mayor Carl Perkins

 

 
 
 
 