I Shot a Man in Vegas
 
 John Stockwell and Janeane Garofalo. The movie is a suspense thriller about five friends dealing with a getaway after one of them gunned down another and dumped the corpse into the trunk of their car.

==Cast== John Stockwell as Grant
*Janeane Garofalo as Gale
*Brian Drillinger as Martin
*Noelle Lippman as Amy
*David Cubitt as Johnny
*Ele Keats as Chick
*Todd Cole as Nick
*Patrick J. Statham as Cop No. 1
*Ellen S. Statham (billed as Ellen Statham) as Cop No. 2
*Tyler Patton as Cop No. 3 Shark as The All Nighter (voice)
*Wendy Gardner as Lorna Love (voice)
*Craig Wasson as Radio Caller (voice)

==Music==
The films score was composed by Shark (musician)|Shark.

The score piece "Route 15 4:30AM" from the film appeared on the Wild Colonials film music compilation, Reel Life, Vol. 1.

==External links==
*   at the Internet Movie Database

 
 
 
 


 