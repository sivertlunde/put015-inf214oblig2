Piranha (1972 film)
{{multiple issues|
 
 
 
 
 
 
}}

{{infobox film
| name      = Piranha
| released  = 1972
| runtime   = 89 mins.
Piranha is a 1972 thriller film starring Peter Brown, Ahna Capri and William Smith
}}

==Synopsis==
Jim Pendrake  and his sister Terry  are a couple of wildlife photographers exploring the Amazon region. They stumble across a deadly predator when they meet Caribe , a homicidal maniac whose hobbies include tracking and hunting human prey.

==Home Media==
Piranha was first released on DVD in 2000.

 
 


 