The Silencers (film)
{{Infobox film
| name           = The Silencers
| image          = The_Silencers_poster.jpg
| caption        = Australian film poster
| image_size     =
| director       = Phil Karlson
| producer       = Irving Allen
| writer         = Donald Hamilton (novels) Oscar Saul (Screenplay) | starring       = Dean Martin Stella Stevens
| music          = Elmer Bernstein
| cinematography = Burnett Guffey, ASC
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 102 min
| country        = United States
| language       =
| gross          = $16,318,124 
}}
 spy film spoof  motion picture released in 1966 and starring Dean Martin as agent Matt Helm. It is loosely based upon the novel The Silencers by Donald Hamilton, as well as another of Hamiltons Helm novels, Death of a Citizen.
 James Gregory makes his first appearance as Macdonald, Helms superior and a recurring character in the series (although Gregory does not play him in all four films).

==Plot==
Once a photographer by day, spy by night, Matt Helm is now a happily retired secret agent, shooting photos of glamorous models instead of guns and enjoying a close relationship with his assistant, the lovely Lovey Kravezit. But then his old boss, Macdonald, coaxes him back to the agency ICE to thwart a new threat from the villainous organization Big O.

The sinister Tung-Tze is masterminding a diabolical scheme to drop a missile on an underground atomic bomb test in New Mexico and possibly instigate a nuclear war in the process. Helms assignment is to stop him, armed with a wide assortment of useful spy gadgets, plus the assistance of the capable femme fatale Tina and the seemingly incapable Gail Hendricks, a beautiful but bumbling possible enemy agent.

Along the way, Helm is nearly sidetracked by a mysterious knife-wielding seductress. And he witnesses the murder of a beautiful Big O operative, the sultry striptease artist Sarita.

In the end, Helm prevails, with Gail by his side as he all but singlehandedly destroys Tung-Tzes evil enterprise and plot to rule the world.

=== Themes === Second World Austin Powers spy comedies of the 1990s and early 2000s.

==Cast==
* Dean Martin as Matt Helm
* Stella Stevens as Gail Hendricks
* Victor Buono as Tung-Tze
* Daliah Lavi as Tina James Gregory as Macdonald
* Robert Webber as Sam Gunther
* Nancy Kovack as Barbara
* Arthur OConnell as Wigman
* Roger C. Carmel as Andreyev
* Cyd Charisse as Sarita
* Beverly Adams as Lovey Kravezit

==Production== Herbert Baker revised Oscar Sauls original script.  Dean Martin was a co-producer of the Helm series. Moss Mabry provided the costumes, except for Martins Sy Devore suits.

==Reception==
Released at the height of James Bond mania, The Silencers was a major box office hit in 1966, earning $7 million in North American rentals that year. 
 The Ambushers The Wrecking Crew (1969). A fifth film, The Ravagers, was announced but never produced.

== Soundtrack == Reprise Dean album by Martin singing several songs that were featured in the film, along with some instrumentals by the Mike Leander Orchestra.  

A scopitone video of the title song was sung by Joi Lansing.  Carrs version of the title song was also used on the soundtrack of the film Confessions of a Dangerous Mind.

== Adaptation == atomic bomb testing site in New Mexico.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 