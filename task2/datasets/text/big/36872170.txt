List of lesbian, gay, bisexual or transgender-related films of 2001
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 2001. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==2001==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|-
|All Over the Guy|| 2001 ||   ||  || Romantic, comedy||
|-
|Blue (2001 film)|Blue|| 2001 ||   ||   || Romantic, drama||
|-
|Blow Dry|| 2001 ||   ||       || Comedy||
|-
|Bully (2001 film)|Bully|| 2001 ||   ||    || Crime, thriller||
|- By Hook or by Crook|| 2001 ||  , Silas Howard ||  || Drama||
|-
|Chicken (short film)|Chicken|| 2001 ||   ||   || Short||
|-
|Circuit (film)|Circuit|| 2001 ||   ||  || Drama||
|-
|Claire (film)|Claire|| 2001 ||   ||  || Fantasy||
|-
| || 2001 ||   ||  || Comedy||
|-
|Danny in the Sky|| 2001 ||   ||   || Drama||
|-
|Days (film)|Days|| 2001 ||   ||   || Drama||
|- David Siegel ||  || Mystery, thriller||
|- A Family Affair|| 2001 ||   ||   || Comedy, romance||
|-
|Fish and Elephant|| 2001 ||   ||   || ||
|-
| || 2001 ||  , Wash Westmoreland ||  || Romantic, comedy, drama||
|-
|Friends & Family|| 2001 ||   ||  || Comedy||
|-
|Gasoline (film)|Gasoline|| 2001 ||   ||   || Crime||
|- Get a aka Ganhar a Vida
|-
|Gosford Park|| 2001 ||   ||    || Crime, mystery||
|-
|Gypsy 83|| 2001 ||   ||  || Drama||
|- Hedwig and the Angry Inch|| 2001 ||   ||  || Musical, comedy, drama||
|-
|Hush! (2001 film)|Hush!|| 2001 ||   ||   || Drama||
|-
|I Am Not What You Want|| 2001 ||   ||   || Drama||
|-
|I Love You Baby|| 2001 ||  , David Menkes ||  || Drama||
|- aka Le fate ignoranti
|- Jan Dara||2001||Nonzee A lesbian relationship is featured as a subplot. Stars Christy Chung and Patharawarin Timkul.
|-
|Julie Johnson|| 2001 ||   ||  || Drama||
|-
|Kissing Jessica Stein|| 2001 ||   ||  || Romantic, comedy, drama||
|-
|L.I.E.|| 2001 ||   ||  || Drama||
|- Lan Yu|| 2001 ||   ||     || Romantic, drama||
|-
|Lost and Delirious|| 2001 ||   ||   || Romantic drama||
|-
| || 2001 ||   ||   || Drama|| aka Qingse ditu
|- Mulholland Drive|| 2001 ||   ||  || Drama, mystery, thriller||
|-
|Ordinary Sinner|| 2001 ||   ||  || Romantic, drama||
|- aka The Inheritance
|- aka Amores Possíveis
|-
|Princesa (2001 film)|Princesa|| 2001 ||   ||          || Romantic, drama||
|- aka Tarik El Hob
|- aka En Kort en lang
|- Southern Comfort|| 2001 ||   ||  || Documentary||
|-
|Stranger Inside|| 2001 ||   ||  || Drama||
|-
|Stuck (2001 film)|Stuck|| 2001 ||   ||  || Short||
|-
|The Ignorant Fairies|| 2001 ||   ||     || Romantic ||
|- Treading Water|| 2001 ||   ||   || Drama ||
|-
|Trembling Before G-d|| 2001 ||   ||      || Documentary||
|-
|Visions of Sugarplums|| 2001 ||   ||  || Drama||
|-
|Waterboys (film)|Waterboys|| 2001 ||   ||   || Comedy||
|-
| || 2001 ||   ||  || Drama||
|-
|Y tu mamá también|| 2001 ||   ||   || Romantic, comedy, drama||
|- aka Norang meori 2
|-
|Zus & Zo|| 2001 ||   ||   || Romantic, comedy||
|}

 

 
 
 