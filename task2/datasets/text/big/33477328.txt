12 Tangos
 
 
{{Infobox film
| name           = 12 Tangos
| image          = 12Tangos2005Poster.jpg
| caption        = 
| director       = Arne Birkenstock
| producer       = Arne Birkenstock Thomas Springer Helmut G. Weber
| writer         = Arne Birkenstock
| starring       =
| music          = Luis Borda
| cinematography = Volker Noack Sergio Gazzera Toni Hervida
| editing        = Felix Bach
| studio         = Fruitmarket Kultur und Medien Tradewind Pictures
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = Germany Argentina
| language       = Spanish
| budget         = 
| gross          = 
}} Tango musicians for the produccion of this movie. The movie was produced by the Cologne production companies Fruitmarket Kultur und Medien and Tradewind Picturesin colobaration with ZDF and ARTE. The development of the script was aided by the European Media-Programm. The movie was released by the Kinostar GmbH and sold worldwide by Medialuna Entertainment. As co-producers Dr. Peter Bach und Hans Georg Haakshorst supported its production.   

==Plot== tangodancers in the crisis-ridden Buenos Aires. In the “Catedral”, a 200 years old granary in Buenos Aires, an orchestra plays 12 well known tangos, while the weekly guests of the ball move in circles. At the core of it stands 71 years old professional dancer Roberto Tonet and the 20 year old dancer Marcela. Tonet lost his pension during the banking crisis and Marcela is preparing her emigration towards Europe. 
Around them dance more people who we will follow from the “Catedral” into the real life during the course of the movie. We see Rodrigo and Fabiana, two school children who live in the poor neighborhood Nueva Pompeya. Rodrigo is the son of Bolivian immigrants, Fabiana lives alone with her three siblings since her mother left to work as a cleaning lady in Spain, in order to gain enough money to pay rent. The movie shows the goodbye of the mother and the four children she had to leave behind. 
In the hippest Tango club of the city we meet five freaks of the trashrockband „Las Muñecas“ who live in the “Catedral”, organize tango-balls  and interpret Gardel ‘s songs on their e-guitars. 
Tango is the expression of crisis and lost hope and their connection will be shown by the history of these dancers who lost their wealth, jobs and salaries due to the current situation.  
In 12 Tangos the story of these dancers and their ancestors is told. Tango, crisis, immigration and emigration join to a cohesive story about the past, present and future of these people and Buenos Aires.    

==Music==
For the movie 12 Tangos the composer and guitarist Luis Borda joined some of the best Argentinean musicians into an orchestra in which several generations were represented: 
The 92 year old Maria de la Fuente sings with Lidia Borda, according to Rolling Stone the „best tango singer of the moment“. Also singing are Gabriel Menendez, Jorge Sobral and Eduardo Borda. 
The movie shows the last recordings with the bandoneon legend José Libertella, who died surprisingly after the movie was completed. The bandoneon is also played by Julio Pane and the young Grammy nominee Pablo Mainetti. The violin is played by the concertmaster of theTeatro Colon Mauricio Marcelli supported by Huberto Ridolfi andElisabet Ridolfi. The orchestra is completed by pianist Diego Shissi, bassist Oscar Giunta, trompetist Juan Cruz de Urquiza, Pablo La Porta(percussion), Marcos Cabezas (vibraphone), Diego Pojomowsky (e-bass) and the saxophone-quartet De Coté. 
The orchestra‘s repertoire includes classics such as "Adiós Nonino”, "Sur", "La Cachila", "La Puñalada" and "Milonga de mis amores" and unknown treasures like "Pampero", "En carne propia" or the waltz "El Paisaje, and new compositions like "Ironía del Salón" and "Corralito". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 