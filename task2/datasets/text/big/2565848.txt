Rock 'n' Roll High School Forever
{{Infobox film
| name = Rock n Roll High School Forever
| image = Rock-n-Roll-High-School-Forever-Poster.jpg
| caption = Original film poster
| producer = Don Daniel Bruce Stubblefield
| director = Deborah Brock
| writer = Deborah Brock
| starring = Corey Feldman Mary Woronov Sarah Buxton
| distributor = Live Entertainment
| released =  
| runtime = 94 min.
| country = United States
| language = English
}}

Rock n Roll High School Forever is a 1991 musical comedy film and sequel to the 1979 film Rock n Roll High School. The film stars Corey Feldman, Mary Woronov, and Sarah Buxton.

==Plot==
 
The movie begins on May 14, also known as Rock n Roll High School day at Ronald Reagan High. The students decide to play a prank on the faculty of the staff, flushing all of the toilets in unison and causing the faulty pipes to burst (yet again), causing for widespread mayhem in the school, as other students leave their classrooms and run rampant down the hall with the perpetrators who organized the entire thing: four members of the band known as The Eradicators.  During the confusion, their friend (and the fifth band member) rides through the halls on a dirt bike as well.

Having had enough, the board of trustees tell Principal McGree that he is a failure as a disciplinarian, and that he is incapable of handling the school by himself. They tell him theyre going to bring in someone new. The next day, an all black BMW seeping smoke from the insides arrives in the parking lot of the high school, and out steps a figure wearing a suit. Jesse, meanwhile, makes an encounter with Rita, a substitute teacher filling in for Mrs. Poindexter, the music teacher. After being chastised and asked to correct his behavior, Jesse becomes smitten with her.
 The Pursuit of Happiness, before they perform a show at Reagan High to both make some money, and get their band better known, before thrashing the show after being insulted by the debutante Whitney and her friend Margaret.

Apprehended by Vadars hall monitors at the conclusion of the dance, they are chastised by Vadar and their classes are changed to no longer coincide with one anothers schedules. Further, their lunch schedules are changed as well, making it impossible for them to practice any longer. In their new lunch period, they encounter a shut-in named Tabatha, the supposed daughter of a witch, who believes that the four basic food groups are Sugar, Salt, Fat, and Booze. This doesnt curb their behavior, however, and they continue with their trouble making in a business as usual fashion, mentally traumatizing Mrs. Grossman in the process (something that enrages Vadar), which leads to Vadar implementing a new operation known as Reagan High Super Secret Security Program.

Mag makes friends with Tabatha at this point, after a misunderstanding and they begin to hang out together. However, in the new RHSSSP program, the school is made into (as Vadar describes it) a school-wide detention hall. Their efforts to get the Pursuit of Happiness tickets are made useless, as Vadar and her monitors assault Screaming Steve and the Rock TV crew. Then, as punishment (for nothing at all it would seem) they are given four days of detention. At this point, Jesse is infuriated by the sentence, and vows revenge on Vadar, spurring The Eradicators to begin fighting back to protest, and they vow to play at the prom.

Enlisting the help of Eaglebauer, a business man who seems to work out of one of the bathrooms in the school to disrupt the school in any way possible, such as selling test answers and other services that clearly violate school policy. He rigs the auditions for the school prom, and they enact his plan, winning the audition in the absence of Vadar. Thinking theyre on drugs, Vadar institutes drug tests which provides no solution to dealing with the Eradicators. However, they are set up by Vadar, Whitney and Marget and made unable to participate through vandalism of school property (and planting Evidence in the lockers of the four).

Vowing to get even, they go to even greater lengths than before to get vengeance, and use video cameras this time around, taping numerous embarrassing situations, such as Bob and Margaret cheating on Whitney and Donovan, Whitney projectile vomiting on a teacher, Donovan posing with womens underwear, and Vadar having an intimate moment with a submissive slave in her office. They also trick the band Zillion Kisses, getting them to set up and wait in the schools storage room during the prom. The Eradicators make their appearance at the prom, meanwhile. Upon the appearance of the Yupettes, the Eradicators begin playing the video tapes that theyve managed to record over the previous week.

Severely embarrassing (and causing infighting among the Yupettes) the Yupettes and Vadar, the situation causes Vadar to go on a homicidal rampage. Driving her car through the school grounds, she has no regard for the safety of anyone on the school grounds. During the aftermath, she tries to kill The Eradicators, as well as Eaglebauer, and even substitute teacher Rita. Not so easily defeated, however, the Eradicators continue to fight back even against the vehicular attempt at manslaughter, and slick a section of the school with chemical extinguishers, which cause Vadar to crash into the school, causing it to explode, and the school catches fire for the second time in 12 years.

During the final sequence, when everyone is witnessing the destruction of the school something emerges from the fire, which quickly turns out to be a flaming tire (which no one attempts to stop, and continues rolling even after the credits finish rolling).

==Cast==
* Corey Feldman: Jesse Davis, a troublesome teenager and the frontman of the Eradicators. He does main vocals for the Eradicators. Of his groups, he would be considered the leader most of the time.
* Evan Richards: Mag, a nervous teen who strives for something more in his life. He plays the drums for the Eradicators. He befriends Tabatha, who plays a vital role in the final scenes of the movie. Patrick Malone: Jones, a mechanical-minded individual. He plays the keyboard for the Eradicators. Liane Curtis: Stella, the only female member of the Eradicators. She plays the guitar for the Eradicators. She and Namrock are a couple. Steven Ho: Namrock, the martial arts expert of the team. He plays the bass guitar for the Eradicators. He and Stella are a couple.
* Sarah G. Buxton: Rita, a substitute teacher at Ronald Reagan High. She teaches music class.
* Mary Woronov: Vadar, the new Vice Principal, and disciplinarian of Ronald Reagan High. She is an amputee and has three prosthetic hands, those being a gloved hand, a metal claw, and a whip.
* Brynn Horrocks: Tabatha, the daughter of a woman who is supposedly a witch, and believes that the four basic food groups are Sugar, Salt, Fat, and Booze.
* Roberta Bassin: The Witch, the mother of a teenage Goth girl
* Larry Linville: Principal McGee, a befuddled yet mostly good-hearted principal.

==Trivia==
* The school in this film is renamed as "Ronald Reagan High School" named after the former president of the same name.
* There are numerous Star Wars references throughout the film, Vadars name for example, which is pronounced the same and changed by one letter from Darth Vaders name. And the tonfa fight scene between the two security guards, they quote the battle between Luke Skywalker and Darth Vader during their fight on Cloud City.
* Vadars license plate reads as "D Vadar" which can be taken as either "Darth Vadar" or "Dr Vadar", depending on whether or not one is interpreting the Star Wars motiff.
* Just as in the previous film, the school explodes in the end of the film. However, unlike the previous film, this film does feature a single fatality in the form of Vadar.
* Two of the staff of Reagan High have normal names. However, the rest of the staff have names that are cliché and actual obscene nicknames students give to their teachers.

==Featured music==

* Jesus Jones - Bring it on Down
* The Lunar Twins (feat Captain Strobe) - I Yi, Yi, Yi, Yi (I like you very much)
* Corey Feldman & The Eradicators - Im Walkin The Pursuit of Happiness - Im an Adult Now Tutti Frutti
* The Divinyls - Dirty Love
* Will & The Bushmen - Dabble On
* Corey Feldman & The Eradicators - Riot in the Playground
* Corey Feldman & The Eradicators - Dare Dreamer
* Corey Feldman & The Eradicators - Rock Us Danny
* Jean Beauvoir and Dee Dee Ramone - Cut Me to Pieces
* Mark Governor and A Zillion Kisses - Love at the Laundromat
* The Pursuit of Happiness - Rock and Roll High School Forever
* Mojo Nixon - High School is a Prison
* Eleven - You Are Mine
* Will & The Bushmen - Very Scary
* Mister Mixi & Skinny Scotty (feat Dizzy D) - I Can Handle It
* Mister Mixi & Skinny Scotty (feat Dizzy D) - Now Get Wild
* Tackhead - Leave Me alone
* The Ventures - Theme from Hawaii Five-O
* Geusch Patti & Encore - Dans Lenfer

==External links==
*  
*  

 
 
 
 
 
 
 
 
 