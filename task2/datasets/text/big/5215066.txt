The Nutty Professor (1996 film)
{{Infobox Film
| name           = The Nutty Professor 
| image          = Nutty professor ver1.jpg
| caption        = Theatrical release poster
| alt            = A fat man in a tweed suit, reflected in the mirror is a skinny man in a skintight leotard
| director       = Tom Shadyac
| producer       = Brian Grazer Russell Simmons	
| screenplay     = {{Plain list|
* David Sheffield
* Barry W. Blaustein
* Tom Shadyac
* Steve Oedekerk
}}
| based on       =  
| starring       = {{Plain list|
* Eddie Murphy Jada Pinkett
* James Coburn Larry Miller
* Dave Chappelle}} David Newman
| cinematography = Julio Macat Don Zimmerman
| studio         = Imagine Entertainment Universal Pictures
| released       =  
| runtime        = 95 minutes  
| country        = United States
| language       = English
| budget         = $54 million http://www.boxofficemojo.com/movies/?id=nuttyprofessor.htm  
| gross          = $274 million 
}} 1963 film Jada Pinkett, Larry Miller, cameo role David Newman. Best Makeup at the 69th Academy Awards.
 tests it upon himself. Like the original films Julius Kelp, Klumps trim, stylish, but arrogant alter ego takes the name "Buddy Love". Murphy plays a total of seven characters in the film, including Sherman, most of Shermans family (except for his nephew, Ernie Klump Jr. played by actor Jamal Mixon), and an over-the-top parody of Richard Simmons.

The film received positive reviews, with critics particularly praising the makeup and Murphys performance. The films success spawned a sequel,  , which was released in 2000. The film was re-released on Blu-ray combo pack on March 6, 2012, to celebrate the 100th anniversary of Universal Studios. 

==Plot==
 
At Wellman College, thousands of hamsters overrun the entire campus, causing general chaos. This is due to the massively obese, yet loving and kind-hearted, professor Sherman Klump, who accidentally releases them by grazing the switch that opens the hamsters cages. Meanwhile, Sherman has constructed an experimental formula that reconstructs the DNA of an obese person in a way that allows them to lose weight instantaneously.
 Viper sports car on his faculty expense account.

To conceal his true identity, Sherman adopts a fake name, "Buddy Love", and invites Carla out on a date at the same club again (while the serum begins to wear off). Reggie is present again and Buddy takes revenge and heckles him mercilessly. Shermans "Buddy" persona starts to develop an independent personality due to the heightened testosterone levels of the transformation, causing him to be overly assertive and confident. Klumps assistant Jason witnesses Buddy fleeing the scene after he is identified as the person who left Klumps credit card on the bar, following Buddy into his car and witnessing the transformation back into Klump. The next morning, Dean Richmond has set up a meeting with wealthy Harlan Hartley at The Ritz to have Sherman explain the serum and potentially have Hartley give $10 million to the science department. Sherman arrives at The Ritz as Buddy with Carla. When the dean spots him, Carla asks Buddy if he will take Shermans place. He does, and he takes all the credit of his work to Hartley. Hartley and the dean are very impressed with his work, and the dean invites him to the Alumni Ball the next night. Meanwhile, Buddy picks up three beautiful women, much to Carlas anger who dumps him and walks out. He then invites the three women back to his place for the night so he can have sex with them. 

After the falling out with Carla, Richmond gleefully telling Sherman that Buddy will be taking his place at the Alumni Ball, and seeing a taunting video tape from his alter ego, Sherman has had enough. He and Jason destroy all of the serum samples. Sherman plans to set things right with Carla and get the grant from Hartley. Unfortunately, Buddy has planned for this and hidden a sample in one of Shermans diet shake cans, which Sherman drinks, causing him to transform into Buddy again. Jason tries to stop him from going to the ball, but Buddy knocks him out and departs. At the ball, Buddy demonstrates the effects of the serum to the audience, but Jason arrives in time, as he has found out that Buddys testosterone levels are at a lethally high 60,000%. Buddy plans to drink a large sums of the potion to get rid of Sherman for good. Jason knows that if he drinks it, it will kill Sherman and possibly Buddy if that happens. The two of them get into a brief fistfight, but Sherman begins to fight Buddy from within. Sherman eventually transforms into his regular self and admits to the shocked audience, including his parents, of his misdeeds, that Buddy was who he thought he and everybody else wanted him to be, and that he should accept himself for who he is. As he leaves, Carla stops him and asks why he lied; he says he did not believe that she would accept him, but she says it doesnt matter if he is overweight or not. The film ends with Sherman and Carla dancing, and Hartley giving the grant to Sherman because he is "a brilliant scientist and a gentleman."

==Cast== Professor Sherman Klump/Buddy Love
** Murphy also plays Papa Cletus Klump (Shermans father), Mama Anna Klump (Shermans mother), Granny Klump (Shermans Grandma), Ernie Klump, Sr. (Shermans brother) and Lance Perkins, a parody of Richard Simmons Jada Pinkett as Carla Purty
* James Coburn as Harlan Hartley Larry Miller as Dean Richmond
* Dave Chappelle as Reggie Warrington
* John Ales as Jason
* Jamal Mixon as Ernie Klump Jr.
* Montell Jordan as himself

==Production== Reginald and Warrington Hudlin, brothers, and directors of one of Murphys previous films, Boomerang (1992 film)|Boomerang.   

While the film was made with the help of Jerry Lewis (he was an executive producer for both this film and the 2000 sequel The Klumps), he later recanted his position in an interview in the January 30/February 6, 2009 edition of Entertainment Weekly magazine. He was quoted as saying, "I have such respect for Eddie, but I should not have done it. What I did was perfect the first time around and all youre going to do is diminish that perfection by letting someone else do it."  

==Reception==
===Critical response===
The Nutty Professor has received positive reviews from critics. Rotten Tomatoes gave the film a score of 65% based on reviews from 54 critics.  Metacritic gave the film a score of 62 out of 100, indicating "generally favorable reviews".  

  of Entertainment Weekly gave the film a B+, writing "You can feel Murphy rediscovering his joy as a performer. He rediscovers it, too, as Sherman Klump, a fellow who, much like Murphy, is on the bottom rung, desperate to reinvent himself, and — at long last — does." 

===Box office===
The Nutty Professor was a box office success, opening with $25,411,725 and reaching a domestic sum of $128,814,019, and $145,147,000 overseas, for a total of $273,961,019 worldwide.

===Awards===
  
 
* 69th Academy Awards Best Makeup (Won)
* 54th Golden Globe Awards Best Actor in a Musical/Comedy - Eddie Murphy (Nominated)

==Soundtrack==
{|class="wikitable"
|-
!rowspan="2"| Year
!rowspan="2"| Title
!colspan="2"| Chart positions sales thresholds) 
|-
!  Billboard 200|U.S. 
!  Top R&B/Hip-Hop Albums|U.S. R&B 
|- 1996
|The The Nutty Professor
* Released: June 4, 1996 Def Jam 8
|align="center"|1
|
* Recording Industry Association of America|US: Platinum
|}

==References==
 

==External links==
 
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 