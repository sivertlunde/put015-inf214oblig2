The Fairy
{{Infobox film
| name           = The Fairy
| image          = The Fairy film poster.jpg
| caption        = Film poster
| director       = Dominique Abel Fiona Gordon Bruno Romy
| producer       = 
| writer         = Dominique Abel Fiona Gordon Bruno Romy
| starring       = Dominique Abel Fiona Gordon Bruno Romy
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Belgium France
| language       = French
| budget         = 
}}

The Fairy ( ) is a 2011 French-Belgian drama film written and directed by Dominique Abel, Fiona Gordon and Bruno Romy.     It won several prizes at the 2nd Magritte Awards. 

==Cast==
* Dominique Abel as Dom
* Fiona Gordon as Fiona, la fée
* Philippe Martz as John, lAnglais
* Bruno Romy as Le patron de lAmour Flou
* Vladimir Zongo as Le premier clandestin
* Destiné MBikula Mayemba as Le deuxième clandestin
* Willson Goma as Le troisième clandestin
* Didier Armbruster as Lhomme volant
* Anaïs Lemarchand as La chanteuse
* Lenny Martz as Jimmy

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 