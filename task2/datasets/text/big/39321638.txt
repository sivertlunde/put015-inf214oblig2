Fantastic Four (2015 film)
 
 
{{Infobox film
| name           = Fantastic Four
| image          = The Fantastic Four poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Josh Trank
| producer       = {{plainlist|
* Simon Kinberg
* Matthew Vaughn
* Hutch Parker
* Robert Kulzar
* Gregory Goodman 
}}
| screenplay     = {{plainlist|
* Simon Kinberg
* Jeremy Slater
* Josh Trank}}
| based on       =  
| starring       = {{plainlist|
* Miles Teller
* Michael B. Jordan
* Kate Mara
* Jamie Bell
* Toby Kebbell
* Reg E. Cathey
* Tim Blake Nelson
}}
| music          = {{plainlist|
* Marco Beltrami
* Philip Glass}} cinematography  = Matthew Jensen 
| editing        = Elliot Greenberg
| studio         = {{plainlist|
* 20th Century Fox
* Marvel Entertainment}}
| distributor    = 20th Century Fox
| released       =   
| runtime        =
| country        = United States
| language       = English
| budget         = $122 million   
| gross          =
}}
 same name. theatrical Fantastic reboot of the Fantastic Four film franchise. Directed by Josh Trank, with a screenplay by Simon Kinberg, Jeremy Slater and Trank, the film stars Miles Teller, Michael B. Jordan, Kate Mara, Jamie Bell, Toby Kebbell, Reg E. Cathey and Tim Blake Nelson. In Fantastic Four, the team must learn to harness abilities gained from an alternate universe to save Earth from a former friend turned enemy.
 Baton Rouge, Louisiana.
 3D theaters.

==Premise==
When four people teleport to an alternate universe, which alters their physical form and grants them new abilities, they must learn to harness their abilities and work together as a team to save the Earth from a familiar enemy. 

==Cast== Reed Richards / Mister Fantastic:
:After school, Richards has been exploring the universe in his garage. After being transformed by one of his experiments, he gained the ability to warp the space around him and stretch his body into different forms and lengths.  Teller said of the role, "When I read the script, I didnt feel like I was reading this larger-than-life, incredible superhero tale. These are all very human people that end up having to become, I guess, what is known as the Fantastic Four. So for me it was just a really good story and gives me an opportunity to play something different from my own skin." 
 Johnny Storm / Human Torch:
:A troublemaker and thrill-seeker, Storm has the ability to shoot fireballs and flight.  Jordan said of the cast, "Were more or less a bunch of kids that had an accident and we have disabilities now that we have to cope with, and try to find a life afterwards – try to be as normal as we can."  Jordan previously worked with Trank for 2012s Chronicle (film)|Chronicle and according to Trank, Jordans character in Chronicle shared characteristics to Johnny Storm.  Trank described Storm as "smart, hilarious and charismatic." 
 Susan Storm / Invisible Woman:
:Brilliant, independent and sarcastic, Storm has the ability to become invisible and generate force-fields.  Mara said that she was supposed to read the Fantastic Four comic books for preparation. However, director Josh Trank suggested to her that it was unnecessary, with writer Simon Kinberg adding that the film is not based on a single issue of the Fantastic Four comic books.  Mara has also said that she intended to focus on making her character "as real as possible".  Trank described Storm as "smart, dignified and has integrity."   
 Ben Grimm / The Thing
:Warm, sensitive, a loyal and protective friend, Grimms stone body gives him super-strength and makes him "indestructible".  Trank said Grimm has a childhood element in the film who was an alienated kid from a "tough" neighborhood.  Trank also said that Bell has "qualities" of warmth and strength which people would want to see from Grimm.  In preparation for the motion-capture performance, Bell approached actor Andy Serkis for advice. 
 Victor Domashev / Doom:
:An anti-social programmer known as "Doom" online. Kebbell said that while playing the role, he concentrated the most on the voice of the character. He added, "on the animated series, they never got his voice what I imagined it to be when I read the comics as a little boy. What I spent the majority of my time doing was not just being a fan, but being a bit of pedant and making sure I got exactly what I always wanted to see."  Kinberg said that Doom is as central to the film as the "titular" heroes. He added that Doom has "aspirations and struggles that are a little bit more classically tragic than the other characters" and that the film will feature how he becomes a villain.   
 Harvey Elder, respectively.

==Production==
===Development===
  |source=—Simon Kinberg on adapting the Fantastic Four comic books into film.}}
 Michael Green was hired to write the script.  In July 2012, Josh Trank was hired to direct and Jeremy Slater was hired as screenwriter.   In February 2013, Matthew Vaughn was attached as a producer and Seth Grahame-Smith was hired to polish the script.   In October, Simon Kinberg was hired to co-write and produce the film. 
 same universe as the X-Men (film series)|X-Men film series.  However, Kinberg contradicted his statement. 
 The Fly influenced the look of the film. 

===Casting=== Jack OConnell Harvey Elder.  In May, Reg E. Cathey was cast as Sues and Johnnys father, Franklin Storm|Dr. Storm. 

===Filming=== Baton Rouge, Louisiana and ended on August 23, 2014.       The film was planned to be shot in Vancouver, Canada, but was moved to Louisiana due to the states film production tax incentives. 

===Post-production===
The film is using OTOY for the visual effects. According to Josh Trank, with the use of cloud-rendering technology from OTOY, they can create visual effects at a much lower cost.  Moving Picture Company and Weta Digital are attached to create visual effects.   The film will also be converted to 3D in post-production. 

===Music===
In January 2015, Marco Beltrami was hired to compose the films score.  Philip Glass is also attached to compose the score with Beltrami. 

==Release==
The film is scheduled for release in North America on August 7, 2015 in 2D and 3D theaters.  The film was originally scheduled in December 2012 for a March 6, 2015 release date,  and was later changed again in November 2013 to June 19, 2015.   

===Marketing===
The teaser trailer for Fantastic Four was released in January 2015 to generally positive reviews.  Graeme McMillan of  . 

The second trailer for the film was released in April 2015.  Sean OConnell of Cinema Blend called the trailer "amazing" and said "  does a much better job of setting up everyones roles."  Drew McWeeny of HitFix said the film "looks like it was approached with serious intent" and that the scale "feels positively intimate."  In the same month, the cast attended CinemaCon to present footage from the film, which also generated positive reviews. 

==Accolades==
{| class="wikitable sortable" style="width: 99%;"
|-
! Year
! Award
! Category
! Recipient
! Result
! scope="col" class="unsortable"| Ref.
|-
| 2015
| CinemaCon Awards
| CinemaCon Ensemble Award
| Fantastic Four
|  
|  
|}

==Sequel==
A sequel is scheduled to be released on June 9, 2017. 

==References==
 

==External links==
* 
* 
* 
* 
* 

{{Navboxes|list1=
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 