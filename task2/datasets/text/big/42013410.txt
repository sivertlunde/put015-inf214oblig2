Pipeline (film)
{{Infobox film
| name           = Pipeline
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       =  Jordan Alan
| producer       =  Jordan Alan
| writer         =  Joshua Jashinski, Jordan Alan
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = Jordan Alan
| editing        = 
| studio         = Terminal Bliss Pictures, The Outer Reefs Limited
| distributor    = 
| released       = 2007  
| runtime        = 
| country        = 
| language       = 
| budget         = $11,000,000 (est.)
| gross          = 
}}
 paddled into the waves at Banzai Pipeline on the night of October 14, 2001.   However, only five paddled back to shore . . . Four years later, the survivors are haunted by the memory of what they vowed to forget.

==Plot==

The writer nearly drowned at Sandy beach, Hawaii in a rip current. The story was inspired partially by this moment. The other inspirations came from true stories on the North Shore.

==Cast==
*Jason Momoa as Kai
*Amanda Righetti as Jocelyn
*Kalani Rob as K Rob
*Makua Rothman as Mak

==Stunts== Brian L. Keaulana

==Production==
===Location===
Unlike most of the beach movies - whose location was Southern California – Pipeline was filmed in Hawaii from 2004-2006 at a time when environmental conditions created exceptionally large waves.  During December 2004, the crew was fortunate enough to be shooting on December 15, 2004, and captured on film one of the largest swells in recorded history from a helicopter at the famous spot Peahi,_Hawaii#Jaws|"Jaws" in Maui. It was the largest wave surfed that year, a 70-footer.

==References==
 

==External links==
*   on Jordan Alans website

 
 
 
 
 
 
 