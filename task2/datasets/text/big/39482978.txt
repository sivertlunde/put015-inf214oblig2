Bhopal: A Prayer for Rain
 
 
 
{{Infobox film
| name = Bhopal: A Prayer for Rain
| image = Bhopal_a_prayer_for_rain_poster.jpg
| alt =  
| caption = Promotional poster
| director = Ravi Kumar
| producer = Ravi Walia
| writer = Ravi Kumar   David Brooks
| starring = {{plainlist|
*Martin Sheen
*Mischa Barton
*Kal Penn
*Rajpal Yadav
*Tannishtha Chatterjee
*Fagun Thakrar
*Vineet Kumar
*Lisa Dwan
}}
| music =
| cinematography = Charlie Wuppermann Anil Chandel Jean-Marc Selva
| editing = Chris Gill
| studio =
| distributor = Revolver Entertainment
| released =  
| runtime = 96 Minutes
| country = India United Kingdom
| language = Hindi English
| budget =
| gross =
}}

Bhopal: A Prayer for Rain is a 2014 historical film|historical-drama film set amidst the Bhopal disaster that had occurred in India on  2–3 December 1984. It is directed by Ravi Kumar and features Martin Sheen, Mischa Barton, Kal Penn, Rajpal Yadav, Tannishtha Chatterjee, and Fagun Thakrar in important roles. Benjamin Wallfisch composed the films music. Kumar got the idea of making a film based on the Bhopal disaster after he read a book based on it. Shot over a period of 18 months, it was originally slated for a late 2010 release. However, the lack of responses from distributors kept on delaying the release.

In April 2013, a new trailer was released, and the film received a market screening at the   on 7 August 2014.

The film received mixed reviews from critics, who praised the actors portrayal of their respective characters but called Bartons character as unnecessary. A few organisations fighting for the rights of the victims of the tragedy blamed the film for presenting the facts in a distorted manner. Kumar and Sheen denied these allegations. The Madhya Pradesh government exempted the film from paying tax.

== Plot ==

In 1984, a few months before the disaster, Dilip (Rajpal Yadav), a rickshaw driver, loses his pay source as his rickshaw breaks down while transporting an employee to the Union Carbide pesticide plant in Bhopal. Dilip lives in the slums around the plant with his wife, a son and his sister. He gets a job in the plant as a labourer, and is happy since his daily wage is restored.

The plant witnesses a drop in its revenue due to lower sales of pesticides, and in order to reduce the loss the officials neglect safety and maintenance. Questioning the chemicals used in the plant, Motwani (Kal Penn), a tabloid reporter publishes reports in his makeshift printing press which are disregarded by most of the officials and workers. Roy (Joy Sengupta), the in-charge for the safety of the plant expresses his concerns. The officials however ignore his warnings, and a worker is killed when methyl isocyanate leaking from a pipeline drips off on his hand. The officials deem the workers irresponsibility as the cause of the accident and the plant continues to function. Dilip is given a better-paying vacant job in the plant despite lacking the skill to operate machinery. A gas leak is prevented by Roy when water is mixed with methyl isocyanate, and in an attempt to stop people from panicking, the official in the plant sabotages the warning siren.

Warren Anderson (Martin Sheen), the CEO of Union Carbide, visits the plant to inspect its functionality, where he is briefed about a plan to connect two additional tanks for storage of methyl isocyanate to increase the output of the plant, ignoring the deteriorated condition of the tanks. Motwani meanwhile meets Eva Gascon (Mischa Barton), a reporter in the Paris Match, and persuades her to get an interview of Anderson. She impersonates the identity of an Associated Press reporter, but fails as her true identity is exposed in between the interview. Motwani convinces Dilip of the danger posed by the chemicals.

As the date of the disaster nears, Dilip arranges a loan for the wedding of his sister. Roy later explains how the company is ignoring safety standards, and that how a future leak might become uncontrolled as the officials had turned off safety measures to reduce the maintenance costs. Roy gives his resignation to the company and advises Dilip not to talk about the plants safety if he wishes to retain his job. Dilip makes a phone call to Motwani describing what Roy just said, and expresses his fear about the plants safety, saying he will return to the rickshaw-pulling business as soon as his sister is married.

In order to overcome the increasing revenue loss, the officials shut down the plant, firing most of the workers, including Dilip. The plant officials then order the usage of the remaining methyl isocyanate as soon as possible. Meanwhile Dilip is busy with the wedding of his sister, and Roy has a final look of the control room. The safety measures fail and a runaway reaction follows. The faulty tanks cause the gas to start leaking, and an attempt to contain the leak fails. The gas escapes to the surroundings and is carried east by the wind. Motwani rushes to alert the people in the vicinity of the plant to vacate and head west, since the warning sirens were previously sabotaged. He meets Dilip, who ignores the warning and asks Motwani to leave the area without causing any hindrance to the wedding. Meanwhile the guests experience irritation in the eyes and discomfort in breathing. Dilip senses the danger and visits the plant, realising that the plant had been compromised. He rushes back to his residence where he finds his family and relatives succumbing to the toxic gas. He carries away his son, paying farewell to his wifes corpse and flees the slum.

As the gas shows its effects, a nearby hospital is filled with hundreds of patients reporting cyanide poisoning, and the lack of antidote results in most of the patients death. Dilip, in the last of his energy, throws away his Union Carbide identity badge, rests his son on the ground and succumbs to the toxic gas. The story jumps to the present day, where a blind boy is holding Dilips identity badge, and the film ends with Motwani narrating the words "Whatever may be the cause of the disaster, Carbide never left Bhopal". A photo montage depicts the aftermath of the disaster, and pictures of the characters and their real-life counterparts

== Cast ==
 Warren Anderson]]
 Warren Anderson, the chief executive of Union Carbide. Anderson was declared a fugitive by an Indian court.  
* Mischa Barton as Eva Gascon, a Paris Match reporter
* Kal Penn as Motwani, a local journalist. Penn said that " larger-than-life, colourful role" of the reporter was what attracted him to the project.    His role is inspired by the story of Indian journalist Rajkumar Keswani
* Rajpal Yadav as Dilip, a factory worker
* Tannishtha Chatterjee as Leela, Dilips wife
* Manoj Joshi as Dr. Chandra
* Fagun Thakrar as Rekha
* Gopichand Lagadapati as Steward.
* Akhil Mishra as Napolean
* Joy Sengupta as Roy
* David Brooks as Shane

== Production ==

In 2004, while reading Sanjoy Hazarikas book Bhopal: Lessons of a Tragedy, Ravi Kumar got the idea of making a film based on the disaster.  The Bhopal disaster is considered the worlds deadliest  industrial disaster.   Seeing that very few people of the newer generation knew about the disaster, Kumar decided to make a film based on it.  He chose several well known actors for the film because he felt that this provided more chances of showing the "story to the world."  He wanted to cast Sheen for the role of Anderson, because of his political views and acting skills.  Tannishtha Chatterjee and Rajpal Yadav were also cast for the film.  Sienna Miller was previously attached to the project, but later dropped out and her role was given to Mischa Barton.    In December 2008, it was confirmed that Barton, Martin Sheen and Kal Penn were filming in India for the project.  A copy of the script was sent to Penn, who agreed to join the project.    Penn played the role of an Indian journalist and learnt Hindi from an instructor. In January 2009 the films shooting was wrapped up.  Initially, the films story was written like a thriller and then dramatic elements were added to the film.  Kumar wanted to depict Anderson as a victim of organisational error of judgement but Sheen insisted that Anderson should be depicted as a guilty and so he rewrote some of the scenes.     Made on a total budget of US $12 million, the film was shot in Hyderabads Charminar area and Golconda Fort,  Mumbai and the Union Carbide factory in Bhopal during a course of 18 months.  A few scenes were also shot in Los Angeles.  Hyderabad was chosen since it bore great resemblance to Bhopal in terms of its Mughal influence and architecture.  Only a few important scenes were shot in Bhopal while the majority were shot on different factory locations and sets in order to create the setup for the period.   

The script was jointly written by Kumar and David Brooks Miller. In order to present the correct technical and medical facts in the film, Kumar analysed documents of the court proceedings, forensic evidences and interviewed several survivors of the tragedy and also the staff members of the Union Carbide plant.     Bhopal Group for Information and Action and many activists fighting for the rights of the victim of the disaster wanted to stop the films screening, called it an insult to the victims, blamed the filmmakers of distorting the facts related to the disaster, diverting attention from Dow Chemical Company which acquired Union Carbide in 2001 and for portraying Anderson as a person who wanted to help the people but was unable to do so.     Sheen denied these allegations and said that he did not sympathise for Anderson.  Kumar also denied these allegations and said that the organisations had seen an older script.  He added that making the film was a life-changing experience for him.  A Prayer for Rain was added to the films title because had it rained on the night of 2—3 December 1984, fewer people would have died.   
 NGO The Bhopal Medical Appeal and the films director collaborated with mobile commerce portal Paytm and the online shopping website Snapdeal. As part of the partnership Paytm and Snapdeal offered their customers a chance to donate money in form of online coupons to the disaster victims. 

== Marketing and release ==

The first look of the movie was unveiled on 18 September 2014 and the official trailer was released on the following day.  Sheen felt that finding a theatrical release for the film would be difficult. He said that the film portrayed "Americas cultural arrogance." Director Ravi Kumar opined that Anderson was guilty of the disaster.    While promoting the film in New Delhi, he compared it to Titanic (1997 film)|Titanic and said that the film "is realistic and poignant, but entertaining."    Rajpal Yadav termed it the biggest project of his life  and dedicated his role to the victims of the disaster. 
 Sting collaborated with Anoushka Shankar to record a song. Kumar also confirmed their collaboration. 

== Reception ==

Meena Iyer of The Times of India praised the actors and noted that despite being based on true incidents the film "  to connect emotionally".  In her review for Hindustan Times, Sweta Kaushal wrote that director Ravi Kumar "should be congratulated for picking a rather grave subject" and praised the actors for doing justice to their respective characters. However, she felt that in portraying Dilip, Kumar took away the "severity of the issue" and that he did not put "the blame on anyone". Further, she opined that the film had depicted Anderson "as a rather humanist  ". She concluded that better research should have been done and that the film "could have been a more involving story".  While Kaushal felt that Bartons role was unnecessary, Anuj Kumar of The Hindu suggested that this role was planted in the story to provide Warren Anderson a chance to express his views. Kumar praised Yadavs acting by calling him the "face of the tragedy." He also praised Sheen but felt that Penn looked "out of place in the mofussil surroundings."  Shubhra Gupta of The Indian Express noted that Chatterjee was effective in her role.  India Today  Rohit Khilnani praised the director for "  everything that   drama needed". However, he felt that the overall quality of the film could have been better. 

Bryan Durham of   of CNN-IBN praised Yadav, Sheen and Chatterjee for their acting. He criticised the film for its dialogues and Mischa Bartons role. He further noted that there was "a sense of drama in the final moments" but the film "has few moments that are extraordinary or even genuinely moving." 

Martin Tsai of the Los Angeles Times wrote that the "cautionary tale could not be more relevant."  Stephanie Merry of The Washington Post wrote that despite the terrible finale, the "movie never feels as powerful as it should." She felt that the dialogue, acting and music tended to be melodramatic but "the overt heartstring-pulling doesn’t add much."  In his review for The Hollywood Reporter|The Hollywood Reporter, Frank Scheck praised Sheens portrayal of Anderson and the visuals of the leakage but felt that the film is "slack in its tension".  Writing for The New York Times, Ben Kenigsberg noted that the film "mines every chemical drip and gurgle for suspense."  Rotten Tomatoes, a review aggregator, rated it 58% fresh based on a total of 12 critic reviews while the public audience gave the film a rating of 80% fresh. 

== Box office ==

In the first three days of its release, the film grossed merely   at the Indian box office.  In its first weekend the film collected  .  At the United States domestic box office, the film grossed US$6,150 in its first weekend  and a total of US $12,628.  It grossed US $6,317 in its first week and US $6,311 in its second weekend. 

== References ==
 

== External links ==
 

*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 