I Spit on Your Grave
 
 
 
{{Infobox film
| name           = I Spit On Your Grave
| image          = ISpitOnYourGraveposter.jpg
| caption        = Theatrical release poster
| director       = Meir Zarchi
| producer       = Meir Zarchi Joseph Zbeda
| writer         = Meir Zarchi
| starring       = Camille Keaton Eron Tabor Richard Pace
| cinematography = Nouri Haviv
| editing        = Meir Zarchi Spiro Carras  
| studio         = Cinemagic Pictures
| distributor    = Cinemagic
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $650,000
| gross          = Unknown
}} among the remade that same year.

==Plot== Kent of the attractive and independent young woman attracts the attention of Johnny, the gas station manager, and Stanley and Andy, two unemployed men who hang around the gas station. Jennifer receives a grocery delivery from Matthew, who is mildly mentally retarded, and befriends him. Matthew is friends with the other three men and reports back to them about the beautiful woman he met, claiming he saw her Breast|"tits".

Stanley and Andy start cruising by the cottage in their boat and prowl around the house at night. One day, while Jennifer is relaxing in her canoe, they surprise her in their speedboat and tow her to shore. As she tries to escape, she is met by Johnny, while Matthew hides in bushes nearby. She realizes they planned her abduction so Matthew can lose his virginity. Jennifer fights but is chased by the men through the forest. Matthew refuses to have sex with her, so Johnny rapes her. They allow her to escape but track her down shortly afterward. Andy proceeds to anally rape her; after she crawls back to her house, they attack her again. Matthew finally rapes her after drinking alcohol, but he says that he can not reach orgasm with the other men watching. The other men ridicule her book and rip up the manuscript, and Stanley sexually assaults her. She passes out, but after the men leave, Johnny realizes she is a witness to their crimes and orders Matthew to stab her to death. Matthew cannot bring himself to do this, so he dabs the knife in the blood on her cheek and returns to the other men claiming he has killed her.
 traumatized Jennifer pieces both herself and her manuscript back together. She goes to church and asks for forgiveness for what she plans to do. The men learn Jennifer has survived and beat Matthew up for deceiving them. Jennifer calls in a grocery order, knowing Matthew will deliver it. He takes the groceries and a knife. At the cabin, Jennifer entices him to have sex with her under a tree. As he becomes oblivious to his surroundings, she strings a noose around his neck and hangs him, then cuts the rope and his body drops into the shallow edge of the lake.

At the gas station, Jennifer seductively directs Johnny to enter her car. She stops halfway to her house, points a handgun at him, and orders him to remove all his clothing. Johnny insists the rapes were her fault because she enticed the men by parading around in revealing clothing. She pretends to believe this as Johnny grabs the gun from her hand and throws it to the ground. She invites him back to her cottage for a hot bath, where she masturbates him. When Johnny says that Matthew has been reported missing, Jennifer states that she killed Matthew, and as he nears orgasm, Jennifer takes the knife Matthew brought with him from its hiding place under the bathmat and severs Johnnys genitals. She then leaves the bathroom, locks the door behind her, and listens to classical music as Johnny screams and bleeds to death. She later dumps the body in the basement and burns his clothes in the fireplace.

Stanley and Andy learn that Johnny is missing and take their boat to the shore of Jennifers cabin. Andy goes ashore with an axe. Jennifer swims out to the boat and climbs aboard before Stanley realizes what she is doing and pushes him overboard. Andy tries to attack her when she speeds past him in the boat, but she escapes with the axe. Andy swims out to rescue Stanley, but Jennifer plunges the axe into Andys back, killing him. While the outboard motor is off, Stanley moves towards the boat and grabs hold of the motor to climb aboard, begging Jennifer not to kill him. She repeats the same words that he used against her during the sexual assaults: "Suck it, bitch!" Jennifer then starts the motor and traps Stanley in the spinning propeller, disemboweling him. Jennifer then speeds away in the boat as the film ends.

==Cast==
* Camille Keaton as Jennifer Hills
* Eron Tabor as Johnny
* Richard Pace as Matthew Lucas
* Anthony Nichols as Stanley
* Gunter Kleemann as Andy
* Alexis Magnotti as Johnnys wife
* Tammy Zarchi as Johnnys daughter
* Terry Zarchi as Johnnys son
* Traci Ferrante as waitress
* William Tasgal as porter
* Isaac Agami as butcher
* Ronit Haviv as supermarket girl

==Production==
 
 
The house that Jennifer rents and its grounds, the river and the gas station, were all located in Kent, Connecticut. The waterway is a section of the Housatonic River. The house in real life was owned by Zarchis friend Nouri Haviv, who photographed this film. Most of the house interior scenes were also shot inside this house.

Zarchi first visited the house while developing the script and its ambience and location  influenced the development of the story.

===Title===
The films original title was Day of the Woman. It was also shown under the title I Hate Your Guts and The Rape and Revenge of Jennifer Hill. The title was changed to I Spit on Your Grave for the 1980 re-release. 

==Release==
 
Zarchi was unable to find a distributor, so he distributed the film himself. It played a number of engagements in rural drive-in theaters, but only for brief runs each time, and Zarchi barely made back what he spent in advertising. In 1980, it was picked up for distribution by the Jerry Gross Organization. A condition of this re-release was that they could change the title to anything they wished. It was at this time the film was retitled I Spit on Your Grave.

===Critical reception===
I Spit on Your Grave received a mixed reception from critics. Film critic Roger Ebert gave the film a thumbs down, referring to it as "a vile bag of garbage...without a shred of artistic distinction," adding that "Attending it was one of the most depressing experiences of my life."  He mentioned in his review a female member of the audience (one of many people who randomly talked aloud) who had "feminist solidarity for the movies heroine". He wrote, "I wanted to ask if shed been appalled by the movies hour of rape scenes". Ebert was also one of many to cite the movies poor production quality as a weakness in addition to the scenes he found offensive, stating "The story of I Spit on Your Grave is told with moronic simplicity. These horrible events are shown with an absolute minimum of dialogue, which is so poorly recorded that it often cannot be heard. There is no attempt to develop the personalities of the characters - they are, simply, a girl and four men, one of them mentally retarded. The movie is nothing more or less than a series of attacks on the girl and then her attacks on the men, interrupted only by an unbelievably grotesque and inappropriate scene in which she enters a church and asks forgiveness for the murders she plans to commit".  Ebert also included it on his "most hated" list and considered it to be the worst movie ever made.    Both Ebert and fellow critic Gene Siskel blasted the movie on their television program Sneak Previews.  In a later episode, Siskel and Ebert chose the film as the worst film of 1980.  Siskel would join Ebert in calling the film one of the worst ever made. 
 The New The Last House on the Left.  Critic David Keyes named it the worst film of the 1980s. 

Encyclopedia of Horror notes that the film attracted much debate for and against, frequently involving people who clearly had not actually seen the film. "The men are so grossly unattractive and the rapes so harrowing, long-drawn-out and starkly presented it is hard to imagine most male spectators identifying with the perpetrators, especially as the films narrative structure and Mise en scène|mise-en-scene force the spectator to view the action from Keatons point of view. Further, there is no suggestion that she asked for it or enjoyed it, except, of course, in the rapists own perceptions, from which the film is careful to distance itself." The book continues that the scenes of revenge were "grotesquely misread by some critics" as Jennifer only "pretends to have enjoyed the rape so as to lure the men to their destruction", and that in these scenes the film is critiquing "familiar male arguments about women bringing it on themselves" as "simply sexist, self-excusing rhetoric and are quite clearly presented as such". Milne, Tom. Willemin, Paul. Hardy, Phil. (Ed.) Encyclopedia of Horror, Octopus Books, 1986. ISBN 0-7064-2771-8 p 329 

The initial criticism was followed by reappraisals of the film. Michael Kaminskis 2007 article for the website "Obsessed with Film", titled "Is I Spit on Your Grave Really a Misunderstood Feminist Film?" argues that, when understood within the context in which director Zarchi was inspired to make it, the movie may be equally appropriate to analyze as "feminist wish-fulfillment" and a vehicle of personal expression reacting to violence against women. 
 masochistic identification with pain used to justify the bloody catharsis of revenge. Clover wrote that in her opinion the film owes a debt to Deliverance.  The British feminist Julie Bindel, who was involved in pickets outside cinemas in Leeds when the film was released, has said that she was wrong about the film and that it is a feminist film. 

===Censorship and film bans===
Many nations, such as Ireland, Norway, Iceland, and West Germany, banned the film altogether, claiming that it "glorified violence against women". Canada initially banned the film, but in the 1990s decided to allow its individual provinces to decide whether to permit its release. Since 1998, some provinces (such as Manitoba, Nova Scotia, and Quebec) have released the film, with a rating that reflects its content.

The censored American version of the film was released in Australia in 1982 with an R 18+ rating. In 1987, the film survived an appeal to ban it. It continued to be sold until 1997, when another reclassification caused its ban in Australia. In 2004, the full uncut version was awarded an R 18+, lifting the seven-year ban. The Office of Film and Literature Classification justified this decision by reasoning that castration is not sexual violence (Australian censorship law forbids the release of films that depict scenes of sexual violence as acceptable or justified). 

In the United Kingdom, the film was branded a "video nasty". It appeared on the Director of Public Prosecutionss list of prosecutable films until 2001, when a heavily cut version which extensively edited the rape scenes was released with an 18 certificate. All subsequent releases of the film have received similar cuts.

In New Zealand, the uncut version of the film (101 minutes) was classified in 1984 as R20 with the descriptive note "Contains graphic violence, content may disturb". Other versions with shorter running times (96 minutes) were also classified in 1984 and 1985, and received the same classification.

The Irish Film Board has again banned the film from sale. Having been banned for many years in the country, the new Blu-ray and DVD uncensored edition has been prohibited from purchase by retailers due to the nature of the film. 

===Zarchis inspiration and responses to criticism===
In the commentary for the Millennium Edition, Zarchi said he was inspired to produce the film after helping a young woman who had been raped in New York. He tells of how he, a friend, and his daughter were driving by a park when they witnessed a young woman crawling out of the bushes bloodied and naked (he later learned the young woman was taking a common shortcut to her boyfriends house when she was attacked). They collected the traumatized girl, returned the daughter home, and quickly decided it was best to take the girl to the police rather than a hospital, lest the attackers escape and find further victims.

They quickly decided that they made the wrong decision &mdash; the officer, whom Zarchi described as "not fit to wear the uniform", delayed taking her to the hospital and instead insisted that she follow formalities such as giving her full name (and the spelling), even though her jaw had been broken and she could hardly speak. Zarchi insisted the officer take her to the hospital and he eventually complied. Soon afterwards the womans father wrote both Zarchi and his friend a letter of thanks for helping his daughter. The father offered a reward, which Zarchi refused. 

In the same commentary, Zarchi denied that the film was exploitative, and that the violent nature of the film was necessary to tell the story.  He described actress Camille Keaton as "brave" for taking on the role. 

==Sequel and remakes==
The film was followed by the unofficial sequel Savage Vengeance (the title card on the movie was misspelled as Savage Vengance) (1993) in which Camille Keaton (under the alias of Vickie Kehl for unknown reasons) reprises the role of Jennifer. However, no scenes from I Spit on Your Grave were used for the flashbacks. The film barely went for 65 minutes, and received extremely negative reviews from critics and fans alike due to major flaws in the film. Zarchi allegedly sued director Donald Farmer, which resulted in a "change" in the character Jennifers name - instead of reshooting, they blanked the audio when an actor said her last name. Keaton allegedly walked out of filming, which gives somewhat of an explanation as to why the film ended so suddenly. 
 remade in 1985 as Naked Vengeance, where Deborah Tranelli plays Carla Harris, who moves to her parents house after her husbands death, and is then harassed, hounded, and raped by some of the towns "scum." Although this version is rather difficult to find as a DVD, VHS, or Blu-ray, it received positive reviews from fans. Its been stated that the storyline is very similar to I Spit on Your Grave.
 I Spit on Your Grave, which had a Halloween 2010 worldwide theatrical release. The remake was produced by CineTel president and CEO Paul Hertzberg and Lisa Hansen, with Jeff Klein, Alan Ostroff, Gary Needle and Zarchi as executive producers.    Steven R. Monroe directed, with newcomer Sarah Butler starring as Jennifer. The follow up I Spit on Your Grave 2 was released on September 20, 2013, starring Jemma Dallender, Joe Absolom, Yavor Baharov, and Aleksandar Alekiov.  It was directed by Steven R. Monroe and written by Thomas Fenton and Neil Elman.

==Home media== remake in a Limited Collectors Edition on 7 February 2011 in the UK.   It was released on 8 February 2011 in the United States from Starz Home Entertainment|Starz/Anchor Bay Entertainment.   In Australia, the film was released on 16 March 2011 as a Directors Cut edition.

==See also==
* 1978 in film

==References==
 

==External links==
*  
*  
*  
*   The Official Website for the New Film

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 