Orphans (1987 film)
{{Infobox film
| name           = Orphans
| image          = Orphans 1987.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Alan J. Pakula
| producer       = Alan J. Pakula Susan Solt
| writer         = Play/Screenplay: Lyle Kessler
| narrator       = Kevin Anderson
| music          = Michael Small
| cinematography = Donald McAlpine
| editing        = Evan A. Lottman
| studio         =
| distributor    = Lorimar Motion Pictures
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $252,430 (USA)
}}
Orphans is a 1987 film directed by Alan J. Pakula.  It was written by Lyle Kessler, based on his Orphans (Lyle Kessler play)|play.

==Plot==
  Kevin Anderson) have lived alone since they were kids when a small-time criminal enters their lives. Living without a father, the orphaned brothers subsist off the proceeds of petty theft in a run-down North Philadelphia row house. One night he kidnaps an enigmatic rich man, played by Finney, who becomes the kind of father figure the boys have always longed for.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|- Harold
|-
| Matthew Modine || Treat
|- Kevin Anderson|| Phillip
|- John Kellogg || Barney
|-
| Anthony Heald || Man in Park
|-
| Novella Nelson || Mattie
|-
| Elizabeth Parrish || Rich Woman
|}

==Critical reception==
Vincent Canby of the New York Times enjoyed the film:
 
Roger Ebert of the Chicago Sun-Times gave it two and a half stars out of four and had this to say:
 

==References==
 

== External links ==
*  
*  

 

 

 
 
 
 
 
 
 
 
 