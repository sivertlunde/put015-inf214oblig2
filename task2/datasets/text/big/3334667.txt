Tender Comrade
{{Infobox film
| name           = Tender Comrade
| image          = TenderComrade.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Edward Dmytryk
| producer       = David Hempstead
| writer         = Dalton Trumbo
| starring       = Ginger Rogers Robert Ryan Ruth Hussey Kim Hunter Patricia Collinge Mady Christians
| music          = Leigh Harline
| cinematography = Russell Metty
| editing        = Roland Gross RKO Radio Pictures
| released       =  
| runtime        = 101 minutes (copyright print) 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
  HUAC as evidence of Dalton Trumbo spreading communist propaganda. Trumbo was subsequently Hollywood blacklist|blacklisted. The films title comes from a line in Robert Louis Stevensons poem "My Wife" first published in Songs of Travel and Other Verses (1896). 

==Plot== the war. With their husbands off fighting in World War II, Jo and her co-workers struggle to pay living expenses. Unable to meet their rent, they decide to move in together and share expenses. The different womens personalities clash, especially when tensions rise over their German immigrant housekeeper Manya (Mady Christians). Jo discovers she is pregnant and ends up having a son who she names Chris after his father. The women are overjoyed when Doris (Kim Hunter) husband comes home, but the same day Jo receives a telegram informing her that her husband has been killed. She hides her grief and joins in the homecoming celebration.

==Cast==
* Ginger Rogers as Jo Jones
* Robert Ryan as Chris Jones
* Ruth Hussey as Barbara Thomas
* Patricia Collinge as Helen Stacey
* Mady Christians as Manya Lodge
* Kim Hunter as Doris Dumbrowski
* Jane Darwell as Mrs. Henderson Richard Martin as Mike Dumbrowski
All primary cast members are deceased.

==Reception==
The film made a profit of $843,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p190 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 