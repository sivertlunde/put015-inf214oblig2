The Damned (2013 film)
{{Infobox film
| name           = The Damned
| image          = 
| border         =
| alt            = 
| caption        = 
| director       = Víctor García (Spanish director)|Víctor García
| producer       = Peter Block Andrea Chung David Higgins
| writer         = Richard DOvidio
| starring       = Peter Facinelli Sophia Myles Nathalia Ramos Carolina Guerra
| music          = Richard DOvidio
| cinematography = Alejandro Moreno
| editing        = Etienne Boussac José Luis Romeu
| studio         = 
| distributor    = IFC Midnight
| released       =  
| runtime        = 87 minutes
| country        = USA Spain Colombia
| budget         = 
| gross          = 
| language       = English Spanish
}}

Gallows Hill also known as The Damned is a 2013 horror film directed by  Víctor García (Spanish director)|Víctor García. The film stars Peter Facinelli, Sophia Myles, Nathalia Ramos, and Carolina Guerra. The film features a family and group of friends stranded in a storm and looking to seek refuge in a house with an ancient evil presence.

The film was produced by Mauricio Ardila, Julián Giraldo, Peter Block, Andrea Chung, and David Higgins and released on 17 October 2013.

==Plot==
A family and group of friends has been stranded in Colombia seeking refuge in an old hotel. They unwittingly release a girl trapped in the basement without realizing she is possessed by an ancient evil.

==Cast==
*Peter Facinelli as David Reynolds
*Sophia Myles as Lauren
*Nathalia Ramos as Jill Reynolds
*Carolina Guerra as Gina
*Julieta Salazar as Ana Maria

==Reception==
Gallows Hill had an early world premiere at the Sitges Film Festival and was acquired for North America by IFC Films, who released the film late Summer 2014 as "The Damned."  The movie had an early VOD release and opened in iTunes #1 in Horror its first week.  Later it screened in select cities. 

Gallows Hill was independently distributed internationally and grossed over an estimated $4,000,000 in the foreign box office.  The movie release in Summer 2014 in Colombia under the name "Encerrada," where the movie was shot and became the highest grossing Colombian produced film in the country with 395,380 admissions.  The movie opened in the Top 10 Box Office in Colombia, Malaysia, Turkey and Russia.  

The movie had a special 3D Home Video release in Germany.    

Gallows Hill internationally has garnered positive reviews in the cult blogosphere while domestically it received more critical reviews with 5.2/10 on IMDb and 11% on Rotten Tomatoes out of 9 reviews. Maitland McDonagh of Film Journal states the film "could find a niche as part of a Halloween possess-a-thon, but on its own theres just not much to it".    Elizabeth Weitzman of NY Daily News gives the film 2 out of 5 stars stating "strong performance doesn’t scare off moviegoers in this serviceable, but gruesome, horror flick",    while Jeannette Catsoulis of the New York Times describes the film as "bloody vomit, a ghostly fetus and terrible room service".   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 