The Boudoir Diplomat
 
{{Infobox film
| name           = The Boudoir Diplomat
| image          =
| caption        = Malcolm St. Clair
| producer       = Carl Laemmle Jr.
| writer         = Fritz Gottwald (play) Rudolph Lothar (play) Benjamin Glazer Tom Reed
| starring       = Betty Compson Mary Duncan Ian Keith Lawrence Grant Lionel Belmore Jeanette Loff George Beranger
| music          =
| cinematography =
| editing        =
| distributor    = Universal Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}} Malcolm St. Clair, produced and distributed by Universal Pictures, from the play The Command To Love by Fritz Gottwald and Rudolph Lothar. 

The film is preserved at the Library of Congress. 

==Plot==
Ian Keith plays a French military attaché in Madrid who romantically pursues the wives of various government officials. Betty Compson and Mary Duncan play the objects of his attention.

==Release==
The film opened to much fan-fare on December 5, 1930. According to Mordaunt Halls review of the film, the lobby in New Yorks showcase theater, the Globe, was elaborately decorated for the films run "with pink silk and photographs with violet borders." 

==Alternate Version==
The film was remade during production into three alternate-language versions. Boudoir diplomatique was the French-language version, starring Iván Petrovich and Arlette Marchal. It was directed by Marcel De Sano and released in 1931, and is not likely to have been screened publicly in the United States. A Spanish-language version of Boudoir Diplomat was released on February 13, 1931 as Don Juan diplomático. It was co-directed by George Melford (he would direct the 1931 Spanish-language version of Dracula (Spanish-language version)|Dracula) with Enrique Tovar Ávalos, and starred Miguel Faust Rocha, Lia Torá and Celia Montalván. Liebe auf Befehl, co-directed by Johannes Riemann and Ernst L. Frank, was the German-language version, starring Riemann along with Tala Birell and Olga Tschechowa.  

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 