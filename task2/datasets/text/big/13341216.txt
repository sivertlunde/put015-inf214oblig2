The Passionate Pilgrim (film)
{{Infobox film
| name           = The Passionate Pilgrim
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Charles Wallace
| producer       = 
| writer         = Charles Wallace
| screenplay     = 
| story          = 
| based on       = 
| narrator       = John Le Mesurier
| starring       = Eric Morecambe Tom Baker Madeline Smith
| music          = Alwyn Green
| cinematography = Nicholas Struthers
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 38 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
The Passionate Pilgrim (1984) is a short feature film starring Eric Morecambe, Tom Baker and Madeline Smith notable for being Morecambes last work, and one of few not to feature his long-term partner Ernie Wise.  The film takes the setting of a silent film with narration provided by John Le Mesurier of Dads Army fame, this too was some of his last work prior to his death in 1983.  The film centres on a Lord who lives in a castle (the film was made entirely on location at Hever Castle) and his Lady the latter of which attracts the amorous attentions of the titular character.  The plot centres on the pilgrims failed attempts to gain entry to the castle by any means possible, and the lengths the lord will go to in order to stop him.  In one memorable scene once the assailant has gained entry the lord brandishes an aerosol can labelled "Knight-Rid" to stop the pilgrim in his tracks.  This film harks back to the era of silent films and is an unusual swansong for the much-missed comedian.

Further filming at Hever was planned May 1984 as filmmaker Charles Wallace intended to expand the film using Beryl Reid to play Eric’s mother, and another damsel in the cast.  Morecambes death ensured that further scenes were not made, but the film was released in its original shorter form.  The film was released with the James Bond movie Octopussy and WarGames (see production company website movingimageco.com).  It was later shown on numerous cable TV channels and was released in 1994 on VHS at a time of resurgence in interest in the work of Morecambe and Wise, the BBC having screened a three (late expanded to five) part tribute to mark the tenth anniversary of his death.  It has subsequently also been released on DVD and is widely available, more so in fact than the bulk of the duos hugely successful television series.

==References==
*  

==External links==
*  
* 

 

 
 
 
 
 
 
 


 