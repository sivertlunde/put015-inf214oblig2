Legend of the Wolf
 
 
{{Infobox film name = Legend of the Wolf image = Legend of the Wolf.jpg caption = DVD cover art traditional = 戰狼傳說 simplified = 战狼传说 pinyin = Zhàn Láng Chuán Shuō jyutping = Zin3 Long4 Cyun4 Syut6}} director = Donnie Yen producer = Donnie Yen writer = Donnie Yen Lui Tak-wai Cub Chin starring = Donnie Yen Dayo Wong Carman Lee Ben Lam Edmond Leung Mak Wai-cheung music = Tommy Wai Patrick Kwok cinematography = Ally Wong Wong Bo-man Kwan Chi-kan editing = Chan Gan-shing studio = My Way Film Company Ltd. distributor =  released =   runtime = 90 minutes country = Hong Kong language = Cantonese Mandarin budget = gross = HK$818,345.00 
}}
Legend of the Wolf, alternatively known as The New Big Boss in American and British DVD releases, is a 1997 Hong Kong action-martial arts film directed and produced by, and starring Donnie Yen. The film also marked Yens debut as a film director.

==Plot==
In legend, there is a powerful and undefeated warrior known as the Wolf. This fighter has retired from the martial arts world and very few people know his true identity, but they are many who wish to challenge him. A young man seeks the Wolf and manages to contact Wai, who leads him to the warrior. When the young man sees that the Wolf (real name Fung Man-hin) is an old man, far from what he expected, he makes some sarcastic remarks to ridicule the old man, prompting Wai to retell the story of the Wolf.
 flashbacks to a post-World War II period. The young Fung is wandering around a war-torn and ravaged farming village, faintly remembering that he is there to meet a woman at an abandoned temple. Fung encounters the young villager Wai, who leads him to his destination. The villagers are suspicious about Fungs background and follow him to the temple, where they see Fung engaging six members of a notorious gang in a fierce fight. Although Fung manages to defeat his opponents, he is severely injured and Wai brings him to a quiet spot for recovery. Just then, Fung remembers that the woman he is meeting is actually his lover, Yee.

In the meantime, the gangsters attack the village and kill many innocent people. When Fung and Wai arrive on the scene, they see corpses everywhere and learn that Yee has been captured by the gang. Fung pursues the gangsters and meets the second leader of the gang, who calls him "Third brother". At that point, Fung suddenly remembers that he is actually part of the gang. Fung is unable to tolerate the gangs cruel acts towards innocent civilians and he fights with the gangsters, emerging victorious eventually.

The film moves back to the present-day conversation between Fung (old man), Wai and the young man. The young man reveals that he is actually an upstart killer, and wishes to make his name by slaying the legendary Wolf. He draws his gun and attempts to kill the Wolf, but does not succeed and is defeated by the Wolf immediately.

==Cast==
*Donnie Yen as Fung Man-hin
*Carman Lee as Yee
*Dayo Wong as Wai
*Edmond Leung as young killer
*Ben Lam as gang boss
*Mak Wai-cheung as monkey-style fighter
*Lai Suk-yin
*Tony Tam
*Tanigaki Kenji
*Mandy Chan
*Sam Hoh
*Chiu Wing-hoi
*Cub Chin
*Lee Tat-chiu
*Renee Dai

==References==
 

==External links==
* 
* 
* 
* 
*   

==See also==
*Donnie Yen filmography

 

 
 
 
 
 
 
 
 