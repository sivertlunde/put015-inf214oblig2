Live, Love and Learn
{{Infobox film
| name           = Live, Love and Learn
| image          =
| image_size     =
| caption        =
| director       = George Fitzmaurice
| producer       = Harry Rapf
| writer         = 
| screenplay     = Charles Brackett Cyril Hume Richard Maibaum
| based on       =  Robert Montgomery Rosalind Russell Robert Benchley Edward Ward
| cinematography = Ray June Joseph Ruttenberg (uncredited)
| editing        = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       = October 29, 1937
| runtime        = 78 mins
| country        = United States
| language       = English
}} romantic comedy Robert Montgomery, Rosalind Russell, and Robert Benchley.  The movie was directed by George Fitzmaurice.

==Cast== Robert Montgomery as Bob Graham
*Rosalind Russell as Julie Stoddard
*Robert Benchley as Oscar
*Helen Vinson as Lily Chalmers
*Monty Woolley as Mr. Bawltitude
*E.E. Clive as Mr. Palmiston
*Mickey Rooney as Jerry Crump
*Charles Judels as Pedro Felipe
*Maude Eburne as Mrs. Crump
*Harlan Briggs as Justice of the Peace
*June Clayworth as Annabella Post
*Barnett Parker as Alfredo
*Al Shean as Professor Fraum

==Reception==
Andre Sennwald in the New York Times wrote, "The principal distinction of this unexpected preachment in behalf of the hard, Cezanne way in art (using that Greenwich Village master, Robert Montgomery, as an object lesson) is that it affords a reasonably adequate vehicle for the graduation out of very funny shorts into a not-too-funny feature-length production, of Robert Benchley, who plays a character called Oscar". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 

 