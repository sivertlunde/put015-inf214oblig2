Impressions de France
 
{{ Infobox attraction
| name                = Impressions de France
| logo                =  
| logo_width          =  
| image               = Epcot paris.JPG
| imagedimensions     = 250px
| caption             = 
| location            = Epcot World Showcase, France
| status              = Open
| cost                = 
| soft_opened         = 
| opened              = October 1, 1982
| closed              = 
| previousattraction  =  
| replacement         =  
| coordinates         =  
| type                = Movie Theater
| manufacturer        =   WED Enterprises
| model               = 
| theme               = Tour of France Buddy Baker; includes works by Boieldieu, Debussy, Offenbach and Saint-Saëns
| height_ft           =  
| height_m            =  
| drop_ft             =  
| drop_m              =  
| length_ft           =  
| length_m            =  
| speed_mph           =  
| speed_km/h          =  
| sitearea_sqft       =  
| sitearea_sqm        =  
| gforce              = 
| capacity            = 
 
| vehicle_type        = 
| vehicles            = 
| riders_per_vehicle  =  
| rows                = 
| riders_per_row      =  
| participants_per_name=  
| audience_capacity   =  
| duration            = 18:00
| restriction_ft      =  
| restriction_in      =  
| restriction_cm      =  
| virtual_queue_name  = 
| virtual_queue_image =  
| virtual_queue_status=  
| single_rider        =  
| pay_per_use         =  
| custom_label_1      = 
| custom_value_1      = 
| custom_label_2      = 
| custom_value_2      = 
| custom_label_3      = 
| custom_value_3      = 
| custom_label_4      = 
| custom_value_4      = 
| custom_label_5      = 
| custom_value_5      = 
| custom_label_6      = 
| custom_value_6      = 
| custom_label_7      = 
| custom_value_7      = 
| custom_label_8      = 
| custom_value_8      = 
| accessible          = yes
| transfer_accessible = 
| assistive_listening = yes
| cc                  = yes
}}
 France Pavilion of Epcots World Showcase at Walt Disney World, Lake Buena Vista, Florida. The movie is projected onto five adjacent screens, giving 220° coverage and resembling Circle-Vision 360°.

==Synopsis== travelogue type of film attraction offers pavilion guests a tour of the French countryside, major cities, various regions and important structures. Set to a soaring musical score written and arranged by Buddy Baker (composer), the film encompasses the music of classical French composers such as Claude Debussy and Camille Saint-Saëns.   The film is the work of director Rick Harper   and produced by two-time Academy Award nominee Bob Rogers (designer).    The films aerial views, mixed with closer views, include, among other things, the Eiffel Tower, the Champs-Élysées and the Arc de Triomphe, the French Alps, Palace of Versailles|Versailles, scenes from Cannes, Notre Dame de Paris and scenes from Normandy. The movie is presented with a lively classical soundtrack.  The narrators name is Claude Gobet.

==Scene list==
Here is the list of the scenes from an official list:
# The cliffs at Étretat in Normandy.
# Gliding through the Marais Poitevin, a swamp area near La Rochelle.
# Château de Chenonceau; in the Loire Valley, then from the gardens.
# Horsemen and hunting dogs cross the Cheverny Forest.
# Aerial shot of Château de Chambord.
# We fly over the red rooftops up to the bell tower of the church in the Vézelay village. Vezelay Church interior, with church bells in the background.
# In the horsecart we ride through Riquewihr Village near Germany.
# Moving through the market place in Beuvron-en-Auge, Normandy.
# French pastries.
# Wine harvest at the Monbazillac Vineyard. Cognac cave.
# The Fountain of Apollo at Palace of Versailles|Versailles.
# The Palace of Versailles Garden. Hall of Mirrors.
# Flying over Castle Beynac in the Dordogne Valley.
# Man chopping wood with Chateau Montpoupon.
# La Roque - Gageac in the Dordogne Valley (bicyclists).
# Chateau Montpoupon, bicyclists racing toward us.
# Bugatti race cars in Cannes. Chaumont Castle Loire river.
# Hot air balloons in front of the cliff city of Rocamadour.
# French Alps in the spring.
# Mountain climbers on rocky peaks in Chamonix. (French Alps)
# Skiers in Chamonix.
# La Rochelle Harbor.
# On board a Brittany fishing boat at sea.
# A rocky beach in Normandy.
# Mont Saint-Michel.
# Interior of a small church in Brittany.
# Wedding reception in full swing at a Brittany farm to traditional Brittany folk music.
# Couple walks along the cliffs of Normandy in Étretat.
# The cliff city of Bonifacio, Corse-du-Sud|Bonifacio, Corsica. Villefranche near Nice.
# Calanque cliffs near Cassis.
# Pier in front of the Carlton Hotel in Cannes.
# Cannes Harbor at twilight.
# Racing along railway tracks in the hills of Chaporoux.
# Gare du Nord (North rail station) in Paris.
# Champs-Élysées at twilight featuring the Arc de Triomphe.
# Seine River in Paris.
# Notre Dame de Paris.
# Through the archways at the Louvre, the Garde Republicaine.
# Eiffel Tower.
# Aerial shot of Étretat Cliffs.
# Aerial shot of the Alps near Chamonix
# Aerial shot of Château de Chambord.
# Aerial shot of French Alps near Mont Blanc.
# Finale - Eiffel Tower.

==Soundtrack== Buddy Baker arranged the film score and conducted the London Symphony Orchestra in the recording of the score.

Here is a listing of the movies score from official and unofficial sources: 

; Scene 1
: "Syrinx (Debussy)|Syrinx" * (0:00-0:33)
:: by Claude Debussy
:: Solo for Flute by Eddie Beckett of the London Symphony Orchestra
; Scenes 2 - 4
: "Aquarium" from The Carnival of the Animals * (0:33-1:52)
:: by Camille Saint-Saëns
; Scenes 5 - 6
: "Rondeau (Allegro agitato)" from Concerto in C for Harp and Orchestra * (1:52-2:52)
:: by François-Adrien Boïeldieu
; Scenes 8 - 9
: original music
:: by Buddy Baker
; Scene 10
: original music
:: by Buddy Baker
; Scenes 11 & 12
: original music
:: by Buddy Baker
; Scenes 13 - 15
: original music
:: by Buddy Baker
; Scene 16
: "Nuages" from Nocturnes (Debussy)|Nocturnes
:: by Claude Debussy
; Scene 17
: original music
:: by Buddy Baker
; Scenes 18 - 20
: "Ouverture" from Gaîté Parisienne * (2:52-3:38)
:: by Jacques Offenbach
:: original 6 second introduction by Buddy Baker
; Scenes 21 - 22
: "Lever du jour" from Part 3 of Daphnis et Chloé Suite #2
:: by Maurice Ravel
; Scenes 23 & 24
: "Clair de Lune" from Suite bergamasque (Orchestral Arrangement) * (3:38-4:51)
:: by Claude Debussy
; Scene 25
: "Trois Gymnopedies #1" * (4:51-5:25)
:: by Erik Satie
:: orchestrated by Debussy
; Scene 26
: original music
:: by Buddy Baker
; Scene 27 & 28
: original music
:: by Buddy Baker
; Scene 29
: "Aquarium" from Carnival of the Animals
:: by Camille Saint-Saëns
:: flute arrangement by Buddy Baker
; Scene 30
: original music
:: by Buddy Baker
; Scene 31
: source music (traditional Brittany folk music)
:: final bars arranged and adapted by Buddy Baker
; Scene 32
: original music
:: by Buddy Baker
; Scenes 33 - 35
: original music
:: by Buddy Baker
; Scenes 36 & 37
: original music
:: by Buddy Baker
; Scenes 38 - 40
: "Finale" from Carnival of the Animals * (5:25-6:25)
:: by Camille Saint-Saëns
; Scenes 41 & 42
: original music * (6:25-7:07)
:: by Buddy Baker
; Scene 42
: "Allegro moderato" from Gaite Parisienne * (7:07-7:22)
:: by Jacques Offenbach
; Scene 43
: "Fanfare" from La Peri * (7:22-7:59)
:: by Paul Dukas
; Scene 44 - 49
: "Maestoso - Allegro" from the 2nd movement of Symphony no. 3 (Organ Symphony) * (7:59-9:51) 
:: by Camille Saint-Saëns

The selections marked with * can be found on these albums:
*Walt Disney World Resort: The Official Album (1999)
*Walt Disney World Resort: Official Album (2000)
*Official Album: Walt Disney World Resort Celebrating 100 Years of Magic (2001)
*Official Album: The Happiest Celebration on Earth – Walt Disney World Resort Album
*Official Album: Where Magic Lives – Walt Disney World Resort
*Four Parks - One World: Walt Disney World Official Album (2008)

==See also==
*  
*  

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 