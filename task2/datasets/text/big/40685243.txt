Khiladi (2013 film)
 
 
{{Infobox film
| name           = Khiladi
| image          = Khiladi 2013 poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Ashok Pati
| producer       = Ashok Dhanuka Himanshu Dhanuka
| writer         = 
| screenplay     = Pele
| story          = Pele
| based on       =  
| narrator       = 
| starring       = Ankush Hazra Nusrat Jahan Tapas Paul Laboni Sarkar
| music          = Shree Pritam
| cinematography = Baba Yadav  D. Shankariya
| editing        = M. Susmit
| studio         = Eskay Movies
| distributor    = 
| released       =  
| runtime        = 135 minutes
| country        = India Bengali
}} 
 Bengali slapstick comedy film directed by Ashok Pati and produced by Ashok Dhanuka under the banner of Eskay Movies. The film features Bengali actors Ankush Hazra and Nusrat Jahan in the lead roles. Music of the film has been composed by Shree Pritam. The film was released on 11 October 2013.             Nusrat Jahan  recently interviewed by Filmz24.com for this film, Video available in  .

==Plot==
Aditya Choudhury (Tapas Paul) is a faction leader in Kurnool who raised his sister Saraswati (Laboni Sarkar) with love and affection to make her forget the loss of their mother. During the marriage time, she eloped with Mohsin Khan (Rajatava Dutta), whom she loved which led the death Aditya Choudhurys father. Thus enraged Aditya cuts the leg of Basha which created a rift between the two families. On a case to win the property belonging to Saraswati, Basha wins the case on Narasimha Naidu after a long gap of 25 years. Seeing the sorrow of Saraswati, her son Sulaiman (Vishnu Manchu) vows to unite the two families and waits for a situation.

Meanwhile, on a suggestion by Pulok Purohit (Kanchan Mullick ), Narasimha Naidu appoints his manager Dayashankar Bal (Kharaj Mukherjee) to find a great scholar to perform a "Shanti Yogga". He contacts a scholar but as he was unavailable, he goes to the college where the scholars son (who is also a scholar) . The boy named Krishna, in a fit of rage (as his love interest Aparna is close to Michael because of Sulaiman), shows Sulaiman to Dayashankar and tells him that he is  Krishna Sastry but prefers to be called as Sulaiman. Though initially he refuses to accept, Sulaiman accepts that he is Krishna Sastry as this would be a Golden Chance to unite the two families. He, along with his friend Michael (Partho Sarathi Chakraborty) and a group of Bramhins (played by Lama, Aritra Dutta Banik, Bhola Tamang and others) goes to the palace of Aditya Choudhury.

There he manages to act well, only to be noticed by Puja (Nusrat Jahan), the adopted daughter of Aditya Choudhry. Though she tries to make his true colours screened (i.e. he is not a Scholar),  he escapes by his wit and timing.  In a unexpected situation, Dayashankar visits krishnas home to pay his father but shockingly he finds out the truth that Sulaiman is acting as Krishna Sastry. Having a fear of getting murdered by Aditya Choudhury, he discloses the facts and wants the "yogga" to be completed as soon  as possible. Sulaiman, though interested in Puja, finds his mothers wish much important and starts playing various plans to intimate feelings on Saraswati in Adityas heart but all go vain. Meanwhile, Adityas opponent Natawar Mondal (Supriyo Datta) attacks Puja but Sulaiman saves her. Puja falls for him and Aditya Choudhury feels that the attack was done by Mohsin Ali.

Now for Sulaiman, things go much worse as Jhimli (the Daughter of Dayashankar and the girl who is loved by Krishna Sastry) returns to the palace, proposes to him, and warns him that if he refuse to love her, she would reveal the Secret. Meanwhile Pulok Purohit, who is in deep frustation as Dayashankar made him not to perform the "Yogga", wants to take revenge on him. Sulaiman plays a very clever game to hide his skin playing with the weaknesses of everyone. Everything goes smooth till Puja tells about her proposal of marriage with Krishna Sastry to her dad and her dad sends his brothers to talk with Krishnas Parents. Sulaiman reveals his identity to Puja and sets to Krishna Sastrys House. There too he plays a game with the weaknesses of Krishna Bhattacharyas mother so that she would refer Sulaiman as her son before Aditya Choudhurys sons. Things get on the right path and marriage planning activities took place on full swing.

Natawar Mondals son wants to marry Puja so that he can torture her to the core. This plan disturbs him and Natawar Mondal threatens Aditya to leave her or else his son aka Krishna Sastry would be killed. Thus Narasimha Sastry summons Krishna Sastry and finds that he neither loved Puja nor he performed the "Yogga". Sulaiman enters the scene, plays a game again, forces  to misrepresent Sulaiman as Krishna Sastry at that place. Sulaiman makes Narasimha Naidu to invite Saraswati for the marriage and Mohsin says that he would come to the marriage only for Puja. Thus when the family along with Sulaiman visits the temple, Natawar Mondal attacks Aditya Choudhurys family and Sulaiman makes a call to Basha that he is attacked. Mohsin Khan reaches there with his men and in an emotional juncture, he saves Aditya Choudhurys family and Aditya Choudhury saves Saraswati. After knowing the truth that Sulaiman acted as Krishna Sastry, he asks about the reason for the fraud and Sulaiman tells that it was to unite the two families. Thus the two families unite and Sulaiman marries Puja.
== Cast ==
* Ankush Hazra as Sulaiman/Krishna
* Nusrat Jahan as Puja
* Tapas Paul as Aditya Choudhury
* Kanchan Mullick as Pulok Purohit
* Rajatava Dutta as Mohsin Khan,Sulemans Father
* Supriyo Datta as Natawar Mondal
* Laboni Sarkar as Saraswati,Sulemans Mother
* Kharaj Mukherjee as Dayashankar Bal,Adityas Maneger
* Aritra Dutta Banik as Nitai Purohit
* Partho Sarathi Chakraborty as Michael
* Lama as priest
* Bhola Tamang as Biswanath, a priest 
* Moushumi Saha as Madhumita Chaudhury, Adityas wife

== Making ==
Filming of Khiladi started on 18 May 2013. According to the director, apart from being a typical comedy film, Khiladi also has a social message for its viewers. 

== Soundtrack ==
{{Infobox album  
| Name       = Khiladi
| Type       = Soundtrack
| Artist     = Shree Pritam
| Cover      = 
| Alt        = 
| Released   = 2013
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = 
| Producer   = 
| Last album = Khoka 420 (2013)
| This album = Khiladi (2013)
| Next album = 
}}
Shree Pritam composed the music for Khiladi.      

=== Track listing ===
{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Singer(s)
| total_length    = 20:40
| title1          = Tor Chokhe Krisnokoli
| extra1          = Zubeen Garg
| length1         = 3:33
| title2          = Pagol Ami Already
| extra2          = Zubeen Garg, Mahalaxmi Iyer
| length2         = 4:06
| title3          = Heartbeat
| extra3          = Baba Sehgal, Saberi Bhattacharya
| length3         = 3:52
| title4          = Jay Dugga Thakur
| extra4          = Shree Pritam, Jolly Das
| length4         = 4:24
| title5          = O Humsafar
| extra5          = Shaan (singer)|Shaan, Palak Muchhal
| length5         = 4:45
}}

== References ==
 

 
 