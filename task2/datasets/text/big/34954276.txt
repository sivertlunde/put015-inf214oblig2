Le Voyage à Alger
 

{{Infobox film
| name           = Le Voyage à Alger
| image          =
| caption        =
| director       = Abdelkrim Bahloul
| producer       = ENTV, Les Films de la Source, Les Films en Hiver
| writer         =
| starring       = Samia Meziane, Sami Ahedda, Benyamina Bahloul, Ghazeli Khedda
| distributor    =
| released       =  
| runtime        = 97 minutes
| country        = Albania France
| language       =
| budget         =
| gross          =
| screenplay     = Abdelkrim Bahloul
| cinematography = Allel Yahiaoui
| sound          = Dominique Warnier, Marc Nouyrigat
| editing        = Jean-Luc Shleigel
| music          =
}} 2009 film.

==Synopsis==
When the War of Independence ends, a widow must find a roof for her six children. Luckily, a Frenchman about to leave offers her the house he is about to vacate. But a high-placed local bureaucrat does not agree to such a gift. After losing all hope of obtaining the property due to the bias of the local authorities, she decides to go to the capital with her children to speak to the president of the Republic. Based on a true story.

==Awards==
* Fespaco 2011
* Jornadas Cinematográfica de Cartago 2010

== References ==
 
*  

 
 
 
 
 


 