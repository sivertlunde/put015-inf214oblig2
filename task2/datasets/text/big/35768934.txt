Harvest (1936 film)
{{Infobox film
| name           = Harvest
| image          = 
| image_size     = 
| caption        = 
| director       = Géza von Bolváry
| producer       = 
| writer         = Philipp Lothar Mayring   Géza von Bolváry
| narrator       = 
| starring       = Paula Wessely Attila Hörbiger Artúr Somlay   Gina Falckenberg
| music          = Heinz Sandauer  Hermann Haller
| cinematography = Franz Planer 
| studio         = Vienna Film
| distributor    = 
| released       = 1 December 1936
| runtime        = 89 minutes
| country        = Austria
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Austrian romance film directed by Géza von Bolváry and starring Paula Wessely, Attila Hörbiger and Artúr Somlay. It is also known by the alternative title Die Julika.

==Cast==
*  Paula Wessely - Julika 
* Attila Hörbiger - Rittmeister Karl von Tamassy 
* Artúr Somlay - Georg von Tamassy 
* Gina Falckenberg - Grit von Hellmers 
* Gábor Rajnay - Kutscher Johann, Julikas Vater 
* Fred Hennings - Baron Felix Sandberg 
* Johanna Terwin - Frau von Hellmers 
* Ferdinand Mayerhofer - Pfarrer 
* Alfred Neugebauer - Rechtsanwalt 
* Otto Storm - Verwalter 
* Karl Ehmann - Bahnhofsvorstand 
* Hermann Gruber - Lakai 
* Gisa Wurm - Zofe

==External links==
* 

 

 
 
 
 
 