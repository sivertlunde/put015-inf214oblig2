Running Free (film)
{{Infobox film
| name = Running Free
| image = Running Free (2000 movie poster).jpg
| caption = Theatrical release poster
| imagesize =
| director = Sergei Bodrov
| producer = Jean-Jacques Annaud
| screenplay = Jeanne Rosenberg
| story = Jeanne Rosenberg Jean-Jacques Annaud
| narrator = Lukas Haas
| starring = Chase Moore Jan Decleir Arie Verveen Maria Geelbooi
| music = Nicola Piovani
| cinematography = Dan Laustsen
| editing = Ray Lovejoy
| distributor = Columbia Pictures
| released = June 2, 2000 
}} 2000 film about a horse born into slavery in 1914. The film began production in 1999 and was released in the US in 2000. It was directed by Sergei Bodrov, written and produced by Jean-Jacques Annaud, narrated by Lukas Haas, and distributed by Columbia Pictures.

==Plot summary== World War I. An Arabian mare gives birth to a beautiful chestnut foal during a voyage to Swakopmund for work in the copper mines. The foal is separated from his mother upon arrival at an unidentified mining town and nearly expires from dehydration. He is subsequently saved from certain death by the sympathetic town stable boy, Richard.

Richard allows Lucky to live in the thoroughbred stable - much to the resentment of Caesar, resident stallion and the prize horse of a wealthy colonial. Lucky wanders away from the town upon the death of his mother but returns after being struck by a desert snake. Richard treats the bite with Caesars personal medication, despite being threatened with grave consequences from the latters vengeful master (Jan Decleir). After being savagely punished, the duo depart to seek a much-sought oasis in the nearby mountains.
 SAAF air raid. The Schutztruppe begins evacuating local civilians, but Richard refuses to leave Lucky. He is forced to do so against his will when Allied biplanes return to strafe the railroad. After the evacuation, Nyka was left to watch over Lucky and even brought him to her tribe. When he picked up a familiar smell from the hunt done by Nykas tribe, Lucky left.

Back at the camp, Lucky is repelled by Caesar and the other horses who treat him with contempt. He sets off for the oasis once more, vowing to best Caesar one day. After making the acquaintance of several exotic animals in the desert, Lucky finally stumbles upon the hidden oasis. Two years later, he returns, defeats Caesar, and leads the remaining horses to the water.

Another twelve years pass with Lucky presiding over his new herd before a grown Richard returns to South-West Africa, now a South African South-West Africa|mandate. He finds the German mining town deserted and flies into the desert searching for the oasis, which can be glimpsed from the sky. Upon landing, Richard stumbles upon the horses and is nearly killed by Lucky who attacks him. However, the latter soon recognizes his friends unique whistle and the two rekindle their relationship.

==Cast==
* Chase Moore as Young Richard
* Arie Verveen as Adult Richard
* Maria Geelbooi as Nyka
* Jan Decleir as Boss Man
* Graham Clarke as Mine Supervisor
* Patrick Lyster as Officer
* Morne Visser as Groom Dan J. Robbertse as Colonel
* Nicholas Trueb as Hans the Boss Son
* Lukas Haas as Narrator

==Filming== Black Stallion The Bear.   

==Reception==
The film received generally negative reviews from critics. A New York Times review of the film was unenthusiastic, saying the film "vacillates between cutesy Disney-style anthropomorphism and Born Free exoticism" and that the "equine action sequences are poorly edited", while the musical score was "sugary". The reviewer was disappointed in the filmmakers, given their previous accomplishments, and the fact that the film portrayed horses as "masters of the obvious who think and speak entirely in movie cliches".  Film critic Roger Ebert also panned the film, giving it 1.5 out of 4 stars. Ebert called the human characters "one-dimensional cartoons" and criticized the plot for historical and cultural errors. He was also disappointed with the filmmakers, given their previous efforts, and stated the film "might have been more persuasive if the boy had told the story of the horse, instead of the horse telling the story of the boy."  The review aggregation website Rotten Tomatoes reports that 27% of critics have given the film a positive review based on 30 reviews, with an average score of 4.3/10. The sites consensus reads: "The narration in Running Free detracts from its beauty, and pushes the film to the point of absurdity." Of 1,387 audience reviews on the same site, 73% liked the film, and it has an average rating of 3.5/5. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 