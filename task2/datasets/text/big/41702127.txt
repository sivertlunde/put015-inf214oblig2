The Bride Is Much Too Beautiful
{{Infobox film
| name           = The Bride Is Much Too Beautiful
| image          =The Bride Is Much Too Beautiful.jpg
| caption        =
| director       =Pierre Gaspard-Huit
| producer       = 
| writer         =
| based on = 
| starring       =  Brigitte Bardot 
| music          = Norbert Glanzberg
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  1956
| runtime        = 
| country        = France
| awards         = French
| budget         =  gross = 2,366,799 admissions (France) 
| preceded_by    =
| followed_by    =
}}
The Bride is Much Too Beautiful is a 1956 French comedy film directed by Pierre Gaspard-Huit. It was also known as Her Bridal Night and La mariée est trop belle. {{cite web |url= http://www.louisjourdan.net/bridalnight.htm |title= The Louis Jourdan Website - La Mariée est Trop Belle
1956 |accessdate= 21 Jan 2014  }} 

==Cast==
* Brigitte Bardot : Catherine Ravaud, aka Chouchou 
* Micheline Presle : Judith Aurigault 
* Louis Jourdan : Michel Bellanger 
* Marcel Amont : Toni 
* Jean-François Calvé : Patrice 
* Roger Dumas : Marc 
* Madeleine Lambert :  Agnès
* Marcelle Arnold : Mme Victoire
* Colette Régis : Yvonne
* Roger Tréville : M. Designy 
* Dominique Boschero   
* Marcel Roche  

==References==
 
==External links==
* 
* 

 
 
 