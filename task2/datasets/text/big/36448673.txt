From Sydney with Love
{{Infobox film
| name           = From Sydney with Love image = File:Poster of hindi film from sydney with love.jpg
| caption        = 
| director       = Prateek Chakravorty
| producer       = Prateek Chakravorty
| based on       = 
| screenplay     = Prateek Chakravorty
| starring       =  
| music          = Sohail Sen
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| country        = India
| language       = Hindi
| studio         = 
}} Hindi movie directed by Prateek Chakravorty, starring Sharad Malhotra, Bidita Bag, Karan Sagoo and Evelyn Sharma. The music has been composed by Sohail Sen, Thor Patridge, and Nabin Laskar.   

==Plot==

It was a dream come true for Meghaa Banerjee, a small town girl in West Bengal, India when she earned a scholarship from the prestigious University of New South Wales in Australia to pursue her Masters degree in Economics with inevitable butterflies in her stomach.  She embarks upon her maiden expedition to Sydney leaving her protective shell and family behind. Coming from a conservative middle-class background, life and culture in Sydney was an instant eye opener for her. Under guidance of her caring cousin Kalpana fondly called "Kol", Meghaa slowly embraces her new life in Sydney where she makes new set of friends, which includes cherubic Lubaina, prankster Raj, and narcissistic Suhail. Love and romance was something that was strictly not in her agenda of things. However being young at heart it was just something waiting to happen to her when she met Rohit - a charismatic, full of sheen and friendly natured fellow student in the University with whom she starts gelling right from the beginning.

==Cast: ==

Sharad Malhotra as Rohit Khurana

Bidita Bag as Meghaa Banerjee

Prateek Chakravorty as Raj Bakshi

Evelyn Sharma as Lubaina Snyder

Karan Sagoo as Suhail Syed

Reshmi Ghosh as Kalpana Chatterjee

==Soundtrack==
 

Music of the film is composed by Sohail Sen. Soundtrack includes Naino Ne Naino Se song sung by Palak Muchhal and Sohail Sen. 

===Track Listings===
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! # !! Title !! Singer(s) !! Lyrics !!  Length
|-
| 1|| "Feeling Love in Sydney" || Sohail Sen || rowspan="8"|  Anvita Dutt Guptan || 3:30
|-
| 2|| "Ho Jaayega" || Mohit Chauhan, Monali Thakur  || 4:48
|-
| 3||"Khatkaa Khatkaa" || Mika Singh || 3:41
|- Palak Muchhal & Mohammad Salamat || 4:44
|-
| 5|| "Item Ye Hi Fi" || Neeraj Shridhar || 3:06
|-
| 6||  "Ho Jaayega (Remix)" || Mohit Chauhan, Monali Thakur || 4:49
|-
| 7|| "Feeling Love in Sydney (Remix)" || Sohail Sen || 3:00
|-
| 8|| "Pyaari Pyaari" || Sohail Sen & Brooklyn Shanti || 3:54
|-
|}

==References==
 

==External links==
*  

 
 
 