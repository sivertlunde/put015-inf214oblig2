The Intimate Stranger (1956 film)
 Intimate Stranger}} Mary Murphy, Constance Cummings and Roger Livesey.  It was also released as Finger of Guilt.

==Plot==
Reggie Wilson, a film cutter, has lost his Hollywood career due to an affair with his bosss wife. He has moved to England and made good, marrying the daughter of a studio head and working on new film Eclipse. His new life is threatened when he starts to receive demanding letters from a woman who claims to have had a brief affair with him, and who knows intimate details about his life - but whom he cannot remember at all.

==Cast==
* Richard Basehart - Reginald Reggie Wilson Mary Murphy - Evelyn Stewart
* Constance Cummings - Kay Wallace
* Roger Livesey - Ben Case
* Faith Brook - Lesley Wilson
* Mervyn Johns - Ernest Chaple
* Vernon Greeves - George Mearns
* André Mikhelson - Steve Vadney David Lodge - Police Sergeant Brown
* Basil Dignam - Doctor Gray
* Grace Denbigh Russell - Mrs Lynton
* Joseph Losey - Director
* Garfield Morgan - Waiter
* Marianne Stone - Miss Cedrick, the Secretary
* Peter Veness - Policeman
* Frederic Steger - Barman

==Production== Howard Koch who had also been blacklisted, and wrote under the name Peter Howard. The film was made at Shepperton Studios.
 cameo part in the film, playing a film director.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 