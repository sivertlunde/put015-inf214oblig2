Persepolis (film)
 
{{Infobox film
| name           = Persepolis
| image          = Persepolis_film.jpg 
| image_size     =  
| alt            =
| caption        = Theatrical release poster
| director       = Marjane Satrapi Vincent Paronnaud
| producer       = {{Plain list |
* Xavier Rigault
* Marc-Antoine Robert Kathleen Kennedy
}}
| screenplay     = {{Plain list |
* Marjane Satrapi
* Vincent Paronnaud
}}
| based on     =  
| starring      = {{Plain list |
* Chiara Mastroianni 
* Catherine Deneuve 
* French Version 
* Danielle Darrieux 
* Simon Abkarian 
* English Version 
* Gena Rowlands 
* Sean Penn 
* Iggy Pop
}}
| music          = Olivier Bernet
| editing        = Stéphane Roche
| studio         = The Kennedy/Marshall Company
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 95 minutes
| country        = France, United States, Iran
| language       = {{Plain list |
* French
* English
* Persian
* German
}}
| budget         = $7.3 million 
| gross          = $22,752,488
}} animated film autobiographical graphic the same name. The film was written and directed by Satrapi with Vincent Paronnaud.    The story follows a young girl as she comes of age against the backdrop of the Iranian Revolution. The title is a reference to the historic city of Persepolis.
 Jury Prize at the 2007 Cannes Film Festival    {{cite news |
title =List of Cannes Film Festival winners |
agency =Associated Press |
date =2007-05-27 |
url =http://hosted.ap.org/dynamic/stories/F/FRANCE_CANNES_AWARDS_LIST?SITE=COBOU&SECTION=HOME&TEMPLATE=DEFAULT | accessdate =2007-05-27
}}   
and was released in France and Belgium on 27 June. In her acceptance speech, Satrapi said "Although this film is universal, I wish to dedicate the prize to all Iranians."  The film was also nominated for the Academy Award for Best Animated Feature, but lost to Ratatouille (film)|Ratatouille.

The film was released in the United States on 25 December 2007 and in the United Kingdom on 24 April 2008.

==Plot==
 
  Shah of Communists for no particular reason. One day, Marjis Uncle Anoush arrives to have dinner with the family after recently being released from his nine-year sentence in prison. Uncle Anoush inspires Marji with his stories of his life on the run from the government, a result of rebelling. 
Political enemies cease fighting and elections for a new leading power commence. Marjis familys situation does not improve as they are profoundly upset when Islamic Fundamentalists win the elections with 99.99% of the vote and start repressing Iranian society. The government forces women to dress modestly, including wearing a head scarf, and Anoush is rearrested and executed for his political beliefs. Profoundly disillusioned, Marji tries, with her family, to fit into the reality of the intolerant regime. The Iran-Iraq war breaks out and Marji sees for herself the horrors of death and destruction. The Iranian government begins implementing ridiculous laws that create blatant injustices. Marji witnesses her father threatened by rifle-wielding teenaged government officials and watches her critically ill uncle die after an unqualified government-appointed hospital administrator refuses to allow him to travel abroad for medical treatment. The family tries to find solace in secret parties where they enjoy simple pleasures the government has outlawed, including Alcoholic beverage|alcohol. As she grows up, Marji begins a life of over-confidence. She refuses to stay out of trouble, secretly buying Western heavy metal music, notably Iron Maiden, on the black market, wearing unorthodox clothing such as a denim jacket, celebrating punk rock and other Western music sensations like Michael Jackson, and openly rebuts a teachers lies about the abuses of the government.
 reveals his homosexuality after a failed attempt at sex with Marji. She engages in a passionate love affair with Markus, a debonair native, which ends when she discovers him cheating on her. Marji is accused of stealing Frau Dr. Schlosss brooch, she becomes angered and leaves. She spends the day on a park bench, reflecting upon how cruel Markus was to her. She discovers that she has nowhere to go. She lives on the street for a few months. Eventually, she becomes ill and contracts bronchitis, and almost dies.

Marji recovers in a Viennese hospital and returns to Iran with her familys permission and hopes that the conclusion of the war will improve their quality of life. After spending several days wasting her time watching television, Marji falls into a clinical depression. She attempts suicide by overdosing on medication. She falls asleep and dreams of God and Karl Marx reminding her what is important and encouraging her to live. Her determination is renewed and she begins enjoying life again. Marji attends university classes and parties. She enters into a relationship with a fellow student. Marji notices that her situation has gradually worsened and that Iranian society is more tyrannized than ever. Mass executions for political beliefs and petty religious absurdities have become common, much to Marjis dismay. She and her boyfriend are caught holding hands and their parents are forced to pay a fine to avoid their lashing. Despite Iranian society making living as a student and a woman intolerable, Marji remains rebellious. She resorts to personal survival tactics to protect herself, such as falsely accusing a man of insulting her to avoid being arrested for wearing make up, and marrying her boyfriend to avoid scrutiny by the religious police. Her grandmother is disappointed by Marjis behaviour and berates Marji, telling her that both her grandfather and her uncle died supporting freedom and innocent people, and that she should never forsake them or her family by succumbing to the repressive environment of Iran. Marji, realising her mistake, fixes her mistakes, and her grandmother is pleased to hear that Marji openly confronted the blatant sexist double standard in her universitys forum on public morality.

Her marriage is falling apart after one year. The police raid a party, resulting in one of Marjis friends being killed while trying to escape on the roofs. After her friends death and her divorce, the family decides that Marji should leave the country permanently to avoid being targeted by the Iranian authorities as a political dissident. Marjis mother forbids Marji from returning and Marji reluctantly agrees. Her grandmother dies soon after Marjis departure. Marji is shown collecting her luggage and getting into a taxi. As the taxi drives away from the south terminal of Paris-Orly Airport, the narrative cuts back to the present day. The driver asks Marji where she is from and she replies "Iran", showing that shes kept the promise she made to Anoush and her grandmother that she would remember where she came from and that she would always stay true to herself. She recalls her final memory of her grandmother telling her how she placed jasmine in her brassiere to smell lovely everyday.

==Cinematography== shadow theater show. The design was created by art director and executive producer Marc Jousset. The animation is credited to the Perseprod studio and was created by two specialized studios, Je Suis Bien Content and Pumpkin 3D.

==Cast==

=== Both versions ===
* Chiara Mastroianni as Marjane
* Catherine Deneuve as Mother

=== French version ===
* Gabriele Lopes as child Marjane
* Danielle Darrieux as Grandmother
* Simon Abkarian as Father
* François Jerosme as Uncle Anouche

=== English version ===

* Amethyste Frezignac as child Marjane
* Gena Rowlands as Grandmother
* Sean Penn as Father
* Iggy Pop (uncredited) as Uncle Anouche

==Reception==
  at the premiere of Persepolis.]]

===Critical response===
The film was critically acclaimed.  Review aggregator Rotten Tomatoes gave the film a 97% rating, based on 145 reviews.  Metacritic reported the film had an average score of 90 out of 100, based on 31 reviews. 

Time (magazine)|Time magazines Richard Corliss named the film one of the Top 10 Movies of 2007, ranking it at #6.  Corliss praised the film, calling it “a coming-of-age tale, that manages to be both harrowing and exuberant.”  

It has been ranked No. 58 in Empire (magazine)|Empire magazines "The 100 Best Films of World Cinema" in 2010. {{cite web
| title = The 100 Best Films of World Cinema   58. Persepolis
| url = http://www.empireonline.com/features/100-greatest-world-cinema-films/default.asp?film=58 
| work = Empire
}}
 

===International government reaction===
The film has drawn complaints from the Iranian government. Even before its debut at the 2007 Cannes Film Festival, the government-connected organization Iran Farabi Foundation sent a letter to the French embassy in Tehran stating, "This year the Cannes Film Festival, in an unconventional and unsuitable act, has chosen a movie about Iran that has presented an unrealistic face of the achievements and results of the glorious Islamic Revolution in some of its parts"  Despite such objections, the Iranian cultural authorities relented in February 2008 and allowed limited screenings of the film in Tehran, albeit with six scenes censored due to sexual content. 

In June 2007 in Thailand, the film was dropped from the lineup of the Bangkok International Film Festival. Festival director Chattan Kunjara na Ayudhya stated, "I was invited by the Iranian embassy to discuss the matter and we both came to mutual agreement that it would be beneficial to both countries if the film was not shown" and "It is a good movie in artistic terms, but we have to consider other issues that might arise here." 

Persepolis was initially banned in Lebanon after some clerics found it to be "offensive to Iran and Islam". The ban was later revoked after an outcry in Lebanese intellectual and political circles. 

===Screening controversies===
On 7 October 2011, the film was shown on the Tunisian private television station Nessma TV|Nessma. A day later a demonstration formed and marched on the station. The main Islamic party in Tunisia, Ennahda Movement|Ennahda, condemned the demonstration.  Nabil Karoui, the owner of Nessma TV, faced trial in Tunis on charges of “violating sacred values” and “disturbing the public order”. He was found guilty and ordered to pay a fine of 2,400 dinars ($1,700; £1,000), a much more lenient punishment than predicted.  Amnesty International said that criminal proceedings against Karoui are an affront to freedom of expression.   

In the United States, a group of parents from the Northshore School District, Washington (state)|Washington, objected to adult content in the movie and graphic novel, and lobbied to discontinue it as part of the curriculum. The Curriculum Materials Adoption Committee felt that "other educational goals – such as that children should not be sheltered from what the board and staff called "disturbing" themes and content – outweighed the crudeness and parental prerogative." 

===Reviews===

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2007. 

* 2nd: Stephen Hunter, The Washington Post
* 3rd: Marc Savlov, The Austin Chronicle
* 3rd: Marjorie Baumgarten, The Austin Chronicle
* 4th: Mike McStay, Socius Website
* 4th: Claudia Puig, USA Today
* 4th: Lisa Schwarzbaum, Entertainment Weekly
* 4th: Mike Russell, The Oregonian
* 4th: Stephanie Zacharek, Salon.com|Salon
* 4th: Stephen Holden, The New York Times
* 6th: Richard Corliss, Time (magazine)|TIME magazine
* 7th: Ella Taylor, LA Weekly
* 8th: Dana Stevens, Slate (magazine)|Slate
* 10th: Peter Rainer, The Christian Science Monitor

===Awards===
 
; 80th Academy Awards Best Animated Best Foreign Language Film entry, but was not nominated.

; 65th Golden Globe Awards Best Foreign Language Film

; 33rd César Awards
* Won: Best First Work (Vincent Paronnaud and Marjane Satrapi)
* Won: Best Writing – Adaptation (Vincent Paronnaud and Marjane Satrapi)
* Nominated: Best Editing (Stéphane Roche)
* Nominated: Best Film
* Nominated: Best Music Written for a Film (Olivier Bernet)
* Nominated: Best Sound (Samy Bardet, Eric Chevallier and Thierry Lebon)

;2007 Cannes Film Festival  Jury Prize
* Nominated: Palme dOr

;20th European Film Awards
* Nominated: Best Picture

;2007 London Film Festival
* Southerland Trophy (Grand prize of the festival)

;2007 Cinemanila International Film Festival
* Special Jury Prize

;2007 São Paulo International Film Festival
* Won: Best Foreign Language Film

;2007 Vancouver International Film Festival
* Won: Rogers Peoples Choice Award for Most Popular International Film
 Silver Condor Awards
* Won: Silver Condor Award for Best Foreign Film

==See also==
* Iranian cinema
* List of animated feature-length films
* List of films based on French-language comics
 

==References==
 

==Further reading==
*  
*   
*  
*  

==External links==
 
*  
*  
*  
*  
*  
*  
*   at Film Education
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 