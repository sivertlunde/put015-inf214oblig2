Villmark
 
{{Infobox Film
| name           = Villmark
| image          = Villmark.jpg
| image_size     = 
| caption        = 
| director       = Pål Øie
| producer       = Jan Aksel Angeltvedt
| writer         = Christopher Grøndahl
| narrator       =  Sampda Sharma
| cinematography = 
| distributor    = ORO Film AS
| released       = 2003
| runtime        = 85 min.
| country        = Norway  Norwegian
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Norwegian Thriller Amanda award in the categories of best film and best actor (Kristoffer Joner). The tagline of the film, "De skulle holdt seg unna det vannet", translates to "They shouldve stayed away from that lake".
 thriller genre back into Norwegian film.

==Plot==

The film is about a TV company that is planning on making a new Reality TV show called Real TV. The show they are working will have the contestants living in an uninhabited part of Norway without any help. In an effort to create bonds between the members of Real TVs production team, Gunnar, the boss, decides that they should try it for themselves.

Gunnar takes the crew to an old cabin in the woods, far from any contact. To make sure that the crew are living with the same conditions as the real contestants will have, Gunnar takes away all of their cellphones and cigarettes.

While scouting out the area around the cabin, Lasse (Kristoffer Joner) and Per stumble upon an abandoned camp site. Near the site, Per finds the body of a dead woman in the depths of the lake. After telling Gunnar about the dead girl, he pressures them to keep their mouths shut, so that they dont scare the other team members and ruin the whole trip. After they head back to their cabin and night sets in, things become even creepier.

== Filming location ==
This movie was filmed in the vicinity of the Norwegian village Kaupanger. 

== Sources ==
 

== External links ==
*  

 
 
 
 
 


 
 