Teen Wolf
 
{{Infobox film
| name           = Teen Wolf
| image          = Teen Wolf.jpg
| caption        = Theatrical release poster
| alt            =  
| director       = Rod Daniel Mark Levinson George W. Perkins  Thomas Coleman  Michael Rosenblatt
| writer         = Jeph Loeb  Matthew Weisman
| starring = {{Plainlist|
* Michael J. Fox James Hampton
* Scott Paulin
* Susan Ursitti
* Jerry Levine
* Jay Tarses
}}
| music          = Miles Goodman
| cinematography = Tim Suhrstedt
| editing        = Lois Freeman-Fox Atlantic Releasing Corporation (original) Metro-Goldwyn-Mayer (current)
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $1.4 million
| gross          = $33.1 million
}} Atlantic Releasing Corporation. Starring Michael J. Fox as Scott Howard, the film is about a high school student who discovers that his family has an unusual pedigree when he finds himself transforming into a werewolf. The film was directed by Rod Daniel based on a script co-written by Jeph Loeb and Matthew Weisman.

==Plot==
Seventeen-year-old high school student Scott Howard is sick of being average and wishes he were special. His father runs a local hardware store. Scott plays basketball for his high schools team, the Beavers, with a not-so-good win-loss record. The girl of his dreams, Pamela Wells, is dating Mick McAllister, a jerk from an opposing high school team, the Dragons. After another of the teams losses, Scott begins to notice strange changes to his body. While at a party, Scott keeps undergoing changes and eventually he returns home, locks himself in the bathroom, and undergoes a complete change and becomes a werewolf, while his father demands that he open the door. He tries to refuse, only to finally give in and find his father has also transformed. Harold never told his son about the condition because "sometimes it skips a generation" and he was hoping it would not happen to Scott. Needing someone else to share his secret, Scott confides his friend, Stiles, who accepts Scotts werewolf status. However, Scott accidentally reveals his transformation to the public at one of his basketball games. After shortly stunning the crowd with The Wolf, Scott goes on to amaze them with his basketball skills.

Scott subsequently learns to use his family "curse" to gain popularity at school, becoming the teams star basketball player, and learns to transform at will between his normal self and The Wolf. His team goes from last to first, and Scott begins spending most of his time at school as The Wolf. He also wins the interest of Pamela while ignoring the affections of his best friend, Boof, who has loved him since childhood. However, it is later revealed that Pamela is only interested in having a one-night stand with Scott, and wants to remain with Mick. Scott ultimately reciprocates Boofs affections after discovering what kind of person Pamela truly is. One night at a school dance with Boof, however, he gets into a confrontation with Mick for his affair with Pamela, causing Scott to become The Wolf and claw Mick. Though Mick is not seriously injured, the crowd starts looking at Scott as a monster rather than the cool basketball star he had become, and Scott himself is starting to be afraid of his alter ego.

The conclusion of the film finds Scott playing in the championship basketball game as himself against Micks team. During the game Mick constantly fouls Scott leading Scott to remind him if he keeps fouling, he will be benched. Mick fouls him one last time leading Scott to shoot two free throws. He makes both baskets putting the Beavers ahead and wins the game. While the crowd is carrying Scott, Pamela walks over to him, but he snubs her and runs over to Boof where they kiss. Mick goes to get Pamela, who now has lost interest in the former basketball hero, and she is heartbroken over losing her chance with Scott. The last scene shows Scott, Boof, and Mr. Howard embracing as the whole school celebrates.

==Cast==
 
{{columns-list|2|
* Michael J. Fox as Scott Howard
* Lorie Griffin as Pamela Wells James Hampton as Harold Howard
* Susan Ursitti as Lisa "Boof" Marconi
* Jerry Levine as Rupert "Stiles" Stilinski
* Matt Adler as Lewis
* Kidus Henok as Michael (Police Officer)
* Jim McKrell as Vice Principal Rusty Thorne Mark Arnold as Mick McAllister
* Jay Tarses as Coach Bobby Finstock
* Mark Holton as Chubby
* Clare Peck as Miss Hoyt
* Gregory Itzin as English Teacher
* Doris Hess as Science Teacher
* Scott Paulin as Kirk Lolley
* Elizabeth Gorcey as Tina
* Melanie Manos as Gina
* Doug Savant as Brad Troy Evans as Dragons coach Richard Brooks as Lemonade
* Harvey Vernon as Carry-Out Clerk
}}

==Soundtrack==
{{Infobox album
| Name = Teen Wolf: Original Motion Picture Soundtrack
| Type = Soundtrack
| Artist = Various
| Longtype=
| Cover = 
| Released = January 1st, 1985
| Length =29:05
| Label = Jackal Records
| Reviews =
}}
{{Track listing
| collapsed       = no
| headline        = Teen Wolf: Original Motion Picture Soundtrack
| extra_column    = Contributing artists
| total_length    = 29:05
| all_music       =

| title1          =  Flesh on Fire
| extra1          = James House
| length1         = 4:05

| title2          = Big Bad Wolf
| extra2          = The Wolf Sisters
| length2         = 2:36

| title3          = Win in the End
| extra3          = Mark Safan
| length3         = 4:41

| title4          =  Shootin for the Moon
| extra4          = Amy Holland
| length4         = 2:45

| title5          = Silhouette David Palmer
| length5         = 3:54

| title6          =  Way to Go
| extra6          = Mark Vieha
| length6         = 3:45

| title7          =  Good News
| extra7          = David Morgan
| length7         = 2:56

| title8          = Transformation (Instrumental)
| extra8          = Miles Goodman
| length8         = 2:29

| title9          = Boof (Instrumental)
| extra9          = Miles Goodman
| length9         = 1:54
}}

==Production== Valley Girl, wanted to make a comedy that would cost almost nothing (the production costs amounted to about $1 million) and take very little time to film. The project came together when Michael J. Fox accepted the lead role and his Family Ties co-star Meredith Baxter-Birney became pregnant, which created a delay in the shows filming that allowed Fox time to complete filming and then return to his TV show. 

The beaver mascot logo used in the film was the Oregon State University Benny Beaver|Beaverss logo, in use by the university at that time. 

==Release==

===Box office===
Released on August 23, 1985, Teen Wolf debuted at No. 2 in its opening weekend, behind Back to the Future (also starring Michael J. Fox).  After its initial run, the film grossed $33,086,661 domestically,   with a worldwide gross of about $80 million. 

===Critical response===
Although the film was a big hit for Atlantic Releasing Corporation, the films critical reception was at best mixed.  Review aggregator Rotten Tomatoes reports that 47% of 19 critics have given the film a positive review, with a rating average of 4.8 out of 10. 

Vincent Canby of The New York Times gave the film a negative review calling it "aggressively boring". He went on to say that "the film is overacted by everybody except Mr. Fox, who is seen to far better advantage in Back to the Future." 

===Home video===
Teen Wolf was first released on DVD via MGM in a "Double Feature" pack with its sequel Teen Wolf Too on August 27, 2002. The film was later released on Blu-ray Disc|Blu-ray on March 29, 2011.  The only special feature available on any of the releases is the films theatrical trailer.

==Sequels== cartoon spin-off in 1986, and a sequel in 1987 titled Teen Wolf Too, with Jason Bateman starring as Todd Howard, Scotts cousin. A second sequel starring Alyssa Milano was planned, but never filmed.  Another female version of Teen Wolf was in the works that later developed into 1989s Teen Witch.

A Canadian television series similar in theme ran from 1999 to 2002 entitled Big Wolf on Campus.
 television series Jeff Davis.    Australian director Russell Mulcahy directed the pilot of the television series.  The first episode for the new MTV series aired on June 5, 2011.

==See also==
* I Was a Teenage Werewolf (1957), an earlier horror film about a high school teenage werewolf
* Full Moon High (1981), an earlier comedy-horror film about a high school teenage werewolf

==References==
 

==External links==
* 
* 
* 
*  at MTV
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 