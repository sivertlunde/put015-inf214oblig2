Richard Pryor: Live in Concert
{{Infobox film 
|title=Richard Pryor: Live in Concert 
|image=  caption = 
|writer=Richard Pryor 
|starring=Richard Pryor
|director=Jeff Margolis  
|music=Patti LaBelle  	 
|distributor=MPI Home Video 
|released=January 1979 
|runtime=78 min.  
|language=English 
|movie_series=
|awards=
|producer=Stephen Blauner Hillard Elkins Del Jack William Sargent William Sargent Jr. J. Mark Travis 
|budget= $750,000 
}}

Richard Pryor: Live in Concert is a stand-up comedy concert film starring Richard Pryor.    

Shot at Long Beach, California  in December 1978, it was produced and distributed independently and is the first full-length, feature movie consisting of only stand-up comedy, often hailed as one of the seminal and most influential recorded stand-up performances of the modern era. 

In her review of Richard Pryor Live in Concert, Pauline Kael commented, "Probably the greatest of all recorded-performance films. Pryor had characters and voices bursting out of him .... Watching this mysteriously original physical comedian you cant account for his gift and everything he does seems to be for the first time." 

An audio version was released in 1978 titled  .

The film and its copyright are owned by Gary Biller. 

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 


 