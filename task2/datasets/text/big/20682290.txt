Jumbo (film)
 
 

{{Infobox film
| name = Jumbo
| image = Jumbo hindi.jpg
| caption = Theatrical poster
| director = Kompin Kemgumnird
| producer = Percept Picture Company
| writer = 
| narrator = Akshay Kumar
| creative direction = Mayur Puri
| starring = Akshay Kumar,  Rajpal Yadav,  Lara Dutta,  Asrani,  Gulshan Grover,  and Yuvraj Singh
| music = Ram Sampath
| cinematography = 
| editing = 
| distributor = 
| released =  
| runtime = 
| country = India
| language = Hindi
| budget = 
| gross = Rs. 20&nbsp;million
}} voices of redubbing of Thai film box office disasters of the year.    In 2011 A direct to DVD sequel titled Jumbo 2: The Return of the Big Elephant was released.   

Kumar was reportedly paid nine crores for the dubbing and two promotional songs.  

== Plot ==
The movie starts with Akshay Kumar singing a song "everythings gonna be all right", for the kids in the school. He then talks to a particular kid who seems to be depressed for some reason. He then narrates a story to the kid of Jumbo.

Jumbo is a small blue elephant (voiced by Akshay Kumar) who grows up with his mother (voiced by Dimple Kapadia). His mother never reveals any details of his father and neither do the rest of the elephants in the herd. He is a happy go lucky elephant, who loves to play with the other animals of the jungle. Occasionally he would be confronted by the other elephants of the herd, who would bully him on the history of his father. Tired of being teased about his fathers cowardice in some war, he confronts his mom again who somehow manages to rubbish the topic. One night he comes to know that a large army has come to the jungle, and they plan an overnight stay. In the hope of finding his father, Jumbo sets out to the camp, and manages to locate a tent that houses the royal elephant of that army. He goes in and politely asks the royal elephant about his father. All of a sudden the royal elephant turns very violent and Jumbo starts running for cover. In the meanwhile the soldiers standing outside see Jumbo running out of the tent and try to capture it. Jumbo manages to go in to the tent of prince Vikramaditya. Vikramaditya then saves Jumbo from the soldiers. It is then revealed that Vikramaditya is the prince of a captured kingdom and is a POW. After running away from the camp Jumbo realizes that he is separated from his mother and is lost in the jungle. He wanders in the jungle for a while where he meets Sonia (voiced by Lara Dutta) a cute pink elephant. He meets some villagers and a wise old Mahout who is the guardian of Sonia. They teach him the art of war and also make him strong and confident. He grows up to be a well built and strong war elephant.

Prince Vikramaditya then calls for his whole kingdom to unite and fight against the oppressive rule of the enemy. Jumbo and his Mahout then go for a competition called by Vikramaditya, for the selection of the royal elephant. Jumbo finds his mother at Vikramadityas court and she finally tells him the story of his father who was a war elephant and never returned after a war. They sneak into a tent which houses the ex-royal elephants, hoping to locate Jumbos father. There they meet an old royal elephant who knows Jumbos father. He then narrates what happened in the battle. He tells them that Jumbos father was killed by Bakhtavar, the enemys royal elephant, after a fierce battle.

Jumbo then set out with king Vikramaditya to avenge the death of his father. Jumbo shows the same strength and skill which his father possessed in his days. They challenge Bakhtavar for a one-on-one combat. Then follows a fierce battle between Jumbo and Bakhtavar, in which Jumbo kills Bakhtavar in the same manner that his father was killed. In the end Akshay Kumar, who is narrating the story to a small kid, again emphasises his words "everythings gonna be all right, chalteh hai side by side".

==Cast==
* Akshay Kumar as Jumbo the elephant
* Lara Dutta as Sonia
* Rajpal Yadav as Dildar Yadav
* Dimple Kapadia as Devi
* Yuvraj Singh as Rajkumar Vikramaditya
* Asrani as Senapati
* Gulshan Grover as Bakhtavar

==Soundtrack==
The soundtrack was composed and directed by Ram Sampath. The lyrics were penned by Israr Ansari, Munna Dhiman, Asif Ali Beg.  The album was released by T-Series. 

===Track listing===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers
|-
| 1|| "Everythings Gonna Be Alright" || Kunal Ganjawala
|-
| 2|| "Chayee Madhoshiyan" || Sona Mohapatra, Sonu Nigam
|-
| 3|| "Dil Mera Jumbo" || Joi Barua 
|-
| 3||  "Badhte Chalo" || Sukhwinder Singh 
|-
| 4|| "Jaya He" || Krishna Beura 
|-
| 5|| "Chayee Madhoshiyan" (Remix) || Sona Mohapatra, Sonu Nigam 
|}

==Reception==

=== Critical reception === Economic Times Disney classics Bambi or Fantasia (1940 film)|Fantasia, or the more recent Finding Nemo and Madagascar (franchise)|Madagascars. With round-the-clock exposure to the best animation in the world, you doubt even if pre-tweens will be enthralled by the caricatured animal life and the flat palette colours". 

===Box office===
Jumbo was released in a limited 300 screens, due to most of the screen space taken by Rab Ne Bana Di Jodi and Ghajini (2008 film)|Ghajini.  The film opened to a poor response, grossing 1.95 in its first week.  After four weeks the film had grossed 2.61 and was declared a disaster. 

==Sequel==
The sequel Jumbo 2: The Return of the Big Elephant was released on 21 October 2011. It received negative reviews   and went unnoticed at the box office due to lack of promotion.

==References==
 

==External links==
*  

 
 
 