Anjala (film)
{{Infobox film
| name           = Anjala
| image          = Anjala.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Thangam Saravanan
| producer       = Dhilip Subbarayan
| writer         =
| starring       = Vimal Nandita Pasupathy
| music          = Gopi Sunder
| cinematography = Ravi Kannan
| editing        = Praveen K. L. N. B. Srikanth
| studio         = Farmers Master Plan Production
| distributor    = 
| released       = 
| runtime        = 
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}}
Anjala is a 2014 Tamil film directed by Thangam Saravanan and produced by Dhilip Subbarayan. Vimal and Nandita feature in the leading roles, while Gopi Sunder composes the films music.   

==Cast==
* Vimal
* Nandita
* Pasupathy
* Imman Annachi
* Aadukalam Murugadoss

==Production==
Stunt choreographer Dhilip Subbarayan chose Vimal, Nanditha and Pasupathi for the cast. Camera will be handled by Ravi Kannan, editing by Praveen-Srikanth and music by Gopi Sundar. Dilip’s father Super Subbarayan will handle the action segments of Anjalai.   

== References ==
 

 
 
 
 


 