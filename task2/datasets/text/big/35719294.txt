The Sowers
{{infobox film
| name           = The Sowers
| image          =
| imagesize      =
| caption        =
| director       = William C. deMille M.A. Harris(asst dir.) Camille Astor(asst dir.)
| producer       = Jesse Lasky
| writer         =  (scenario)
| starring       = Blanche Sweet Thomas Meighan
| music          =
| cinematography = Charles Rosher
| editing        =
| distributor    = Paramount Pictures
| released       = March 30, 1916
| runtime        = 50 minutes; 5 reels
| country        = United States
| language       = Silent film(English intertitles)
}}
The Sowers is a surviving 1916 silent film drama produced by Jesse Lasky, released through Paramount Pictures and directed by William C. deMille. The feature stars Blanche Sweet and Thomas Meighan and is based on the novel, The Sowers by Henry Seton Merriman. It is preserved in the Library of Congress collections.    

==Cast==
*Blanche Sweet - Karin Dolokhof
*Thomas Meighan - Prince Paul Alexis
*Mabel Van Buren - Princess Tanya
*Ernest Joy - Count Egor Strannik
*Theodore Roberts - Boris Dolokhof
*Horace B. Carpenter - Chief of Secret Police
*Raymond Hatton - The Peddler
*Harold Howard - The Tramp

==See also==
*Blanche Sweet filmography

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 