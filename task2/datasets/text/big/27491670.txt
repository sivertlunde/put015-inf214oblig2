Pornography: A Thriller
{{Infobox film
| name         = Pornography: A Thriller
| image        = pornography_film.jpg
| alt          = 
| caption      = Theatrical release poster
| director     = David Kittredge
| writer       = David Kittredge Matthew Montgomery Pete Scherer Jared Grey Walter Delmar Dylan Vox Steve Callahan Wyatt Fenner Larry Weissman Nick Salamone
| producer     = Sean Abley
| editing      = Mike Justice David Kittredge
| cinematography = Ivan Corona
| music        = Robb Williamson
| released     =  
| runtime      = 113&nbsp;minutes
| studio       = Triple Fire Productions
| country      = United States
| language     = English
}}
Pornography: A Thriller is a 2009 American Mystery film|mystery/thriller film, written and directed by David Kittredge.
 Matthew Montgomery) as he searches for the truth in 2009 about what happened to Anton, and the third revolves around 2009 adult film actor Matt Stevenss (Pete Scherer) attempt to make The Mark Anton Story, based in part on his dreams of what happened to Anton.

The film uses an unconventional structure, with actors playing multiple roles in different sections, and uses dream logic to tell its story through a subjective point of view. Because of this, its been compared to the work of David Lynch (most notably Lost Highway (film)|Lost Highway and Mulholland Drive (film)|Mulholland Drive)    and David Cronenberg (notably Videodrome).   

Kittredge says about the structure, "We wanted to replicate the dream logic of a nightmare. So even though we don’t spell everything out for the audience, the film is still a complete whole and has its own fully formed arc that’s hopefully a satisfying whole." 

==Cast== Matthew Montgomery as Michael Castigan
*Pete Scherer as Matt Stevens
*Jared Grey as Mark Anton
*Walter Delmar as William/Jason
*Dylan Vox as Jason Steele
*Steve Callahan as Jerome
*Akie Kotabe as Jeremy/Adam
*Wyatt Fenner as Student/Angel
*Larry Weissman as Harry
*Jon Gale as Rex
*Nick Salamone as Billy

==Production==
The film was shot in 16&nbsp;days for under $200,000. And although much of the film takes place in New York City and Brooklyn, it was mostly shot in and around Los Angeles.
 Panasonic DVX100a, Sony Video 8 camcorder (featured prominently in the film).

The 5.1 sound mix was done by Sonicpool in Hollywood and the HD color-correct and finishing was done at Postworks, New York.

==Release==
Pornography: A Thriller premiered at  , Frameline, Philadelphia QFest, and Reeling Chicago.

The film was theatrically released in the US on April 13, 2010, at the Cinema Village in New York City. The film was released theatrically in Germany by Bildkraft on July 9, 2010, and the UK by Peccadillo Pictures on August 27.   
 Matthew Montgomery, Pete Scherer and Jared Grey, and a behind-the-scenes featurette called "Smile For the Camera". 

Digitally, the film was released in the United States on DVD and iTunes on August 30, 2010.  The film was released on DVD in France by Optimale on August 31, 2010. Peccadillo Pictures also launched an aggressive online campaign for the film in the guise of a fake "snuff film" website,     based on the plot points about a mysterious videotape and symbol; the site featured recut snippets of the film and spread virally.     
 Logo Network was October 31, 2010.

==Critical response== awards at two.

Critics in the press, however, had polarized responses. "Kind of pretentious.... Kind of amazing, too," was the summation in New Yorks L Magazine.,  who also named it "The Citizen Kane of Gay Porn Ghost Stories".   Reverse Shot lauded the "well placed" plot twists and "nicely paced" mystery, saying that it was "impressively committed to its own vision and logic".    However, the review also calls the film "unsatisfying" and "ill-conceived."  The Village Voice called it "Ambitiously layered and almost completely incoherent"; "Unabashedly arty and impressively shot"; "undermined by its awkward combination of campy horror", and "strangely unchecked" warnings about gay pornography.    The Hollywood Reporter was mostly negative: the final line in the review is "Hampered by schematic characterizations, weak performances, sluggish pacing, an overly dense structure and propensity for self-indulgent affectations, Pornography: A Thriller ultimately is neither sexy nor thrilling." 

However, Chuck Wilson of LA Weekly called the film "a crazily ambitious debut thriller"    and "Pornography marks Kittredge as filmmaker with a strong mind and a gift for drawing full-bodied performances from his actors".    And Tirdad Derakhshani of the Philadelphia Inquirer named it "a deeply affecting and disturbing thriller that has earned Kittredge comparisons to David Lynch and David Cronenberg."    Matthew Sorrentos positive review in Film Threat states "When handled well, dread can extend far beyond our conscious need for order – Lovecraft himself held that our fear is strongest when we fear the unknown. Pornography approaches this conceit without the timidity of lesser efforts."    He ends with "I recommend all fans of the bizarre have a look."   

Critics generally recognized Kittredges debt to Lynch and the "horror-noir"  oeuvre, although characterizations ranged from "little too heavily"  to "obsessed". 

The Rotten Tomatoes website shows a Tomatometer aggregate score of 29% from 7 critics.    The Audience Rating stands at 2.4 stars out of 5. 

==Awards==
{| class="wikitable"
|-
! Year !! Festival !! Award !! Category
|- Long Island Jury Award||Best Debut Feature
|- FilmOut San Audience Award||Best First Narrative Feature
|- FilmOut San Programming Award||Outstanding Emerging Talent, David Kittredge
|}

==Festivals==
2009
*Newfest: the New York LGBT Film Festival (World Premiere)
*Frameline: the San Francisco LGBT Film Festival
*Outfest: the Los Angeles LGBT Film Festival
*QFest: the Philadelphia LGBT Film Festival (centerpiece selection) BAM
*Out on Film: the Atlanta LGBT Film Festival (closing night)
*Cinema Diverse: The Gay and Lesbian Film Festival of Palm Springs
*Tampa International LGBT Film Festival
*Reel Affirmations: the Washington DC LGBT Film Festival
*Atlantic Film Festival, Halifax, Nova Scotia
*Portland LGBT Film Festival
*OutTakes Dallas (closing night)
*Reeling: the Chicago LGBT Film Festival
*Fresno Reel Pride: Night Out Film Festival
*image+nation: Montreal LGBT Film Festival
*Long Island LGBT Film Festival (opening night)

2010
*Reelout Queer Film & Video Festival, Kingston, Ontario
*Brussels LGBT Film Festival
*Mardi Gras Film Festival, Sydney, Australia
*Miami LGBT Film Festival (special screening, Feb 21)
*Melbourne Queer Film Festival
*Brisbane Queer Film Festival
*FilmOut San Diego (centerpiece selection)
*Seoul LGBT Film Festival (centerpiece selection)
*ColognePride, Cologne, Germany
*Vancouver LGBT Film Festival

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 