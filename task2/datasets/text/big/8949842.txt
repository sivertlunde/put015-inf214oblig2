Girl Without a Room
{{Infobox Film
| name           = Girl Without a Room
| image          = Girl Without a Room.jpg
| image_size     = 
| caption        = Theatrical poster to Girl Without a Room (1933)
| director       = Ralph Murphy
| producer       = Charles R. Rogers
| writer         = Claude Binyon Frank Butler Jack Lait (novel)
| narrator       = 
| starring       = Charles Farrell Charles Ruggles Marguerite Churchill
| music          = 
| cinematography = Leo Tover
| editing        = Richard C. Currier
| distributor    = Paramount Pictures
| released       = December 8, 1933
| runtime        = 78 min.
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Girl Without a Room is a 1933 musical comedy film starring Charles Farrell, Charles Ruggles, and Marguerite Churchill.  This early light comedy farce set in Paris was written by Claude Binyon, Frank Butler, and Jack Lait, and directed by Ralph Murphy.

As well as featuring some scenes with dialogue rendered in rhyming couplets, the film is notable as an example of Hollywoods attitude to abstract art in the 1930s. The hero is a no-nonsense representational artist from Tennessee, who, transplanted to Paris, meets a crowd of pretentious types in a Montparnasse garret. Chief among them is an artist who believes in depicting the soul of an object rather than the object itself. Played by comic star Charlie Ruggles, his name is Crock, as in crock of s**t.

==Cast==
* Charles Farrell as Tom Duncan
* Charles Ruggles as Vergil Crock
* Marguerite Churchill as Kay Loring
* Gregory Ratoff as Serge Alexovich
* Walter Woolf King as Arthur Copeland
* Mischa Auer as Walksky
* Grace Bradley

==External links==
*  
 
 
 
 
 
 
 
 
 


 