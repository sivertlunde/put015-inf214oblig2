The Rabbit Trap

{{Infobox film
| name           = The Rabbit Trap
| image          = The Rabbit Trap film.jpg
| image_size     =
| caption        = 
| director       = Philip Leacock
| producer       = 
| writer         = JP Miller
| based on = TV play by JP Miller
| starring       = Ernest Borgnine  Jack Marshall 
| cinematography = Irving Glassberg
| editing        = 
| distributor    = United Artists
| released       = 1959
| runtime        = 
| country        = USA
| language       = English
}}
The Rabbit Trap is a 1959 American drama film directed by Philip Leacock. 

==Cast==

*Ernest Borgnine: Eddie Colt
*David Brian: Everett Spellman
*Bethel Leslie: Abby Colt
*Kevin Corcoran: Duncan Colt
*June Blair: Judy Colt
*Christopher Dark: Gerry
*Jeanette Nolan: Mrs. Colt
*Russell Collins: Hughie Colt
*Don Rickles: Mike OHalloran

==References==
 
==External links==
* 
*  at TCMDB
*  at New York Times

 
 
 
 
 
 