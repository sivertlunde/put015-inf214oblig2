Bone Dry
{{Infobox film
| name           = Bone Dry
| image          = Bone Dry film poster.jpg
| caption        =
| director       = Brett A. Hart
| producer       =
| writer         = Jeff OBrien   Brett A. Hart
| starring       = Luke Goss Lance Henriksen
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 7 April 2007 (US)
| runtime        = 100 minutes
| country        = US
| awards         =
| language       = English
| budget         = $ 5 000 000
}}
 drama film from 2007. It was directed by Brett A. Hart, written by Jeff OBrien, starring Luke Goss and Lance Henriksen. 

== Cast ==
* Luke Goss – Eddie
* Lance Henriksen – Jimmy
* Tommy Tiny Lister – Mitch
* Dee Wallace – Joanne (as Dee Wallace-Stone)
* Jennifer Siebel Newsom – Wife (as Jennifer Siebel)
* Carl Buffington – Marty
* Richard Larsen – Cook
* Julia Self – Connie
* Chad Stalcup – Price
* Hudson Thames – Son

== See also ==
* Bone Dry (1922)

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 