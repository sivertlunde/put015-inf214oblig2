Daisy (1988 film)
{{Infobox film name           = Daisy image          = Daisy88malfilm.jpg caption        = Promotional Poster director       = Prathap K. Pothan producer       = Babu for Thomsun Films  writer         = Prathap K. Pothan Khalid starring  Lakshmi Kamal Hassan
 music  Shyam
|cinematography Ashok Kumar editing        = B. Lenin V. T. Vijayan Television Copyrights = C.Jayakumar, Video Complex.         = distributor    = released       =   runtime        = rating         = country        = India awards         = language       = Malayalam budget         = gross          =
}}
 Malayalam musical romance film Harish and Lakshmi in a pivotal role, and Kamal Hassan in an extended guest appearance. The screenplay was written by Prathap K. Pothan and the dialogues were written by Khalid.  

==Plot==

Pradeep Menon is a troubled teenager who studies in a Boarding school in Ooty in the Nilgiris. He is regarded as a trouble-maker and is often punished by his teachers and headmaster, often severely. When beautiful Daisy Thomas joins the school, he makes fun of her on the first day, thus beginning a series of misadventures that result in more punishment for him. This brings him close to Daisy, and they are attracted to each other. 

Daisy understands why Pradeep is so aggressive and offers him considerable support. She is shocked to see how rude he is toward his mother, Malathi Menon, but is unable to do anything about it. She later learns the reason behind his hatred was that his mother had decided to re-marry after his fathers death and had started spending less time with him. 

Things improve considerably when Malati is confined in a hospital and Pradeep goes to visit her, thereby improving their relationship. Pradeep starts to respond positively to Daisys support and is all set to turn a new leaf. Then Pradeeps world is turned upside down when he witnesses Daisy in the arms of James — a complete stranger. Shocked at this, Pradeep regresses to his self-destructive phase, alienating himself from everyone. He later learns that James is her older brother and that she is a terminally ill patient. Things go bad when his mother dies and he loses Daisy as well. The movie ends with him finding solace in James.

==Cast==
* Harish Kumar as Pradeep Menon
* Sonia as Daisy Thomas Lakshmi as Malathi Menon
* Nedumudi Venu
* Sankaradi
* Lalu Alex
* Thesni Khan
* Kamal Hassan as James (Guest appearance)

==Trivia==

* Daisy was one of the highest grossing Malayalam films of 1988.
* The film is mainly centralized on teenagers; it took nine years in Malayalam cinema to overcome this record with teens, which is Aniyathipravu.
* Kamal Hassan played an extended cameo.
* The film was dubbed in Hindi as Daisy produced by Mani Nair   and had an official audio launch as well.

==Soundtrack==
{{Infobox album Name     = Daisy
|  Type        = soundtrack Shyam
|Cover    = Released =  Music  Shyam
|Genre Feature film soundtrack Length   = 20:25 Label    = EMI
}}
 Shyam and lyrics written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||
|-
| 1 || Ormathan Vasantha... || K. J. Yesudas ||rowspan=5|P. Bhaskaran
|-
| 2 || Pookale... || K. S. Chithra 
|- 
| 3 || Rapadithan... || K. S. Chithra 
|-
| 4 || Thenmazayo...  || Krishnachandran 
|-
| 5 || Laalanam...  || K. J. Yesudas 
|}

==References==

 

== External links ==
*  
*   at Facebook
*   at Musiqbuzz.com

 
 
 
 
 
 
 
 