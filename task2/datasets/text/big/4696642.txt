Bala (1976 film)
{{Infobox film
| name           = Bala
| image          = Bala (short film 1976, title card).jpg
| image_size     =
| caption        = Bala (short film 1976, title card)
| caption        = Title card of the film
| director       = Satyajit Ray National Centre for the Performing Arts, Government of Tamil Nadu
| writer         = Satyajit Ray
| narrator       = Satyajit Ray
| starring       = Balasaraswati V. Raghavan Uday Shankar V. K. Narayana Menon
| music          = Satyajit Ray
| cinematography = Soumendu Roy
| editing        = Dulal Dutta
| distributor    = National Centre for the Performing Arts, Government of Tamil Nadu
| released       = 1976
| runtime        = 33 Minutes
| country        = India
| language       = English
}}
 National Centre for the Performing Arts and Government of Tamil Nadu. The thirty three minute documentary features the life and some of the works by Balasaraswati in the form of narration and dance, starring herself. At the age of fourteen, Ray had seen a performance of Balasaraswati in  Kolkata, then known as "Calcutta", in 1935, when she was seventeen years old.   

Ray had initially planned to make a film on Bala in 1966 when she was in her prime. However, he could only start filming in 1976. Incidentally, though Bala was often called as "a revolutionary Bharata Natyam dancer",  she had never been filmed till she was 58 years old, in spite of having a career spanned over four decades.  Ray decided to make the film on Bala, "the greatest Bharata Natyam dancer ever" according to him,    to document her art for future generations with the "main value as archival".  When Ray filmed then 58 year old Bala for the documentary, she found to be wearing same pair of anklets which she wore more than fifty years ago for her debut performance, at the age of seven.  Ray reported to have been said about the delayed filming of the documentary about Bala that "Bala filmed at 58 was better than Bala not being filmed at all." 

The films original script was included in a book named Original English Film Scripts Satyajit Ray, put together by Rays son Sandip Ray along with an ex-CEO of Ray Society, Aditinath Sarkar, which also included original scripts of Rays other films.    

==Background==

 

Tanjore Balasaraswati, fondly known as Balasaraswati or Bala, was born on May 13, 1918 in Chennai, then known as Madras. With her earlier seven generations worked predominantly in dance and musical fields, Bala started her Bharata Natyam training at the age of five, under Nattuvanar Kandappa Pillai and made her dancing debut in 1925, at the age of seven, at Kancheepuram at the Kamakshi Amman Temple.     Her mother, Jayammal was a singer who encouraged Balas musical training and was her accompanist in the dance concerts. Bala continued to do stage performance around the world along with her brothers, Mridangam player T. Ranganathan and flautist T. Viswanathan. Bala and her famous contemporary Rukmini Devi Arundale are often called as revolutionary Bharata Natyam dancers.    She was awarded the second highest civilian honour given by the Government of India, the Padma Vibhushan, in 1977.    Bala passed way on February 9, 1984, at the age of 65.

==Synopsis==

The film begins with the introduction of Bharata Natyam since its inception. It also explains the various hand gestures, known as Mudra and Bala demonstrates one of them, "Mayura Mudra" ("Peacock Mudra"). Narrated by Satyajit Ray, the film describes Balas lineage and her debut performance in 1925, at the age of seven, at Kancheepuram at the Kamakshi Amman Temple. A noted Sanskrit scholar and musicologist explains Balas dancing style and an Indian dancer, Uday Shankar mentions about his association with Bala.

The film then showcases Balas "one of the most acclaimed" performance Krishna Ni Begane Baaro in the background of the ocean. It mentions that Bala got international acclaim through the "The Festival of Arts, Edinburgh" in 1963, where other Indian artists also performed their art like Sitar player Ravi Shankar, classical vocalist M. S. Subbulakshmi and Sarod player Ali Akbar Khan. She performed eight solo recitals at the festival. Mentioning about Balas achievement, the film showcases her daily routine with her brothers, Mridangam player T. Ranganathan and flautist T. Viswanathan, and her only daughter Lakshmi Knight, also a Bharata Natyam dancer.

The final segment of the film showcases Balas solo performance of a pada varnam, which is based on Carnatic music, known as "raagamaalika" (garland of ragas). For this performance, Bala uses the same pair of anklet she had used for her debut performance at the age of seven.

==Credits==
 

===Cast===
* Balasaraswati
* V. Raghavan
* Uday Shankar
* V. K. Narayana Menon

===Crew===
* Sound designer: S. P. Ramanathan, Sujit Sarkar, David
* Production designer: Anil Chowdhury, Bhanu Ghosh, R. Ramasi
*  
* Eastmancolor: Gemini Color Lab
* Mixing: Mangesh Desai
 

===Music===
* K. Ramaiah (Isai Velalar#Art skills of Nattuvanar|Nattuvanar)
* M. S. Ramadas (Vocal)
* T. Viswanathan (Flute)
* T. R. Murthy (Flute)
* V. Tyagarajan (Violin)
* T. Kuppuswamy (Mridangam)
* T. Janardan (Tanpura (instrument)|Tambura)
* T. Ranganathan (Mridangam)

==Restoration==
 honorary Academy Award in 1992 for his lifetime achievements,  the Academy Film Archive, part of the Academy Foundation which mainly works with the objectives as "preservation, restoration, documentation, exhibition and study of motion pictures", took an initiative to restore and preserve Rays films.  Josef Lindner was appointed as a preservation officer and   the Academy has successfully restored 19 titles. However, the Academy could not restore Bala yet as the negative of the film was not found.   

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 