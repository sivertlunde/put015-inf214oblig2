The Game That Kills
{{Infobox film
| name           = The Game That Kills
| image          = The Game That Kills.jpg
| alt            = 
| caption        = Film poster
| film name      = 
| director       =  D. Ross Lederman
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Charles Quigley, Rita Hayworth, and John Gallaudet
| music          = 
| cinematography = 
| editing        = 
| studio         =
| distributor    = 
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Game That Kills is a 1937 American sports mystery film, directed by D. Ross Lederman. It stars Charles Quigley, Rita Hayworth, and John Gallaudet.   

==Cast==
* Charles Quigley as Alex Ferguson
* Rita Hayworth as Betty Holland
* John Gallaudet as Sam Erskine
* J. Farrell MacDonald as Joe Holland
* Arthur Loft as Rudy Maxwell John Tyrrell as Eddie
* Paul Fix as Dick Adams
* Max Hoffman Jr. as Bill Drake
* Dick Wessel as Leapfrog Soule
* Maurice Black as Jeff
* Clyde Dilson as Steven Morean

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 