Kaazhcha
{{Infobox film
| name = Kazhcha
| image = malayalam_vcd_kazhcha_icon.jpg
| writer = Blessy
| starring = Mammootty Padmapriya Master Yash Sanusha Venu Nagavalli
| director = Blessy
| producer = Noushad, Xavy Mano Mathew
| distributor = Liberty Cinemas
| cinematography = Azhagappan
| editing = Raja Mohammed
| released = 2004
| studio = NX Visual Entertainment
| runtime =
| language = Malayalam
| country = India
| music = Mohan Sithara
}}
Kaazcha (  drama film written and directed by Blessy. It stars Mammootty in the lead role. There have been several claims that the film seems to be inspired heavily from the Iranian movie Bashu, the Little Stranger. However, a close examination reveals that it is clearly not the case.

==Plot== 16 mm projector and shows films at temple festivals and other public functions. Madhavans family consist of his wife (Padmapriya) and a daughter (Sanusha). (Innocent (actor)|Innocent) enacts the role of a priest.  Madhavan comes across a six-year-old boy (Yash) who is displaced from his native Gujarat and separated from his family after the devastating earthquake. This boy was taken into a gang of beggars from where he manages to escape. Madhavan takes the boy home and cares for him, just like a son. He and his Family takes a fondness for the boy, later finding out that legally he cannot adopt the boy. The boy is taken away from Madhavan to a juvenile home and allegations of ill treatment is charged on Madhavan, but soon dismissed. The issue gets media coverage.  Madhavan then goes to Gujarat with the boy in hopes of finding his family or adopting him. At the disaster camp in Gujarat Madhavan understands that the boys relatives are all most probably dead but due to legal hurdles, the boy must stay on in the hope that his real parents might be traced. Madhavan dejected has to return to his family in Kerala.

==Cast==

*Mammootty as Madhavan
*Padmapriya as Lakshmi
*Master Yash as Pavan aka Kochundapri
*Sanusha as Ambily
*Manoj K. Jayan as Joy Innocent as Fr. Kuriakose Augustine as Suresh
*Venu Nagavalli as Magistrate
*Hakim Rawther as man at film club society
*T. S. Raju as Police Officer
*Nedumburam Gopi as Madhavans father
*Pala Aravindan as Mathai mapla-Film Distributor

==Notes==
*The film was the first film by Blessy as an independent director.
*The performance of Mammootty as the protagonist Madhavan got high critical acclaim and he got several  awards for this performance.
*The film marked the debut of Padmapriya.

==Soundtrack==

The songs of Kaazhcha were composed by Mohan Sithara and lyrics by .They were critically acclaimed.

{| class="wikitable"
|-
! Track
! Song
! Artist
|-
| 1
| "Jugunure"
| Anwar Sadat
|-
| 2
| "Kunje Ninakku Vendi" 
| K. J. Yesudas|Dr. K. J. Yesudas
|-
| 3
| "Dup Dup Janaki"
| Mubina Mohan, Priya R. Pai
|-
| 4
| "Kuttanadan"
| Madhu Balakrishnan, Kalabhavan Mani
|-
| 5
| "Kunje Ninakku Vendi"
| Asha Madhu 
|}

==Reception==
The film got a very good reception from all of its releasing centres

== Awards ==

The film was a critical and commercial success. The debutant director Blessy was well appreciated for his work as was Mammootty for his portrayal of Operator Madhavan. The movie marked a sea change in the existing trend and restored quality in Malayalam movies. The film won several awards and international nominations. The major awards won by Kaazhcha are

The film won 5 Kerala State Film Awards for the year 2004.These are:

* Best Film with Popular appeal & Aesthetic quality -         Kaazhcha
* Best Debutant Director-Blessy
* Best Actor -        Mammootty
* Best Child artist - Baby Sanusha& Master Yash 

The film won three Filmfare Awards South for the year 2004. These are

* Best Film - Kaazhcha
* Best Director - Blessy
* Best Actor - Mammootty

The film won four Asianet Film Awards for the year 2004. These are

* Best Film  - Kaazhcha
* Best Actor - Mammootty
* Best New Face of The Year (Female) - Padmapriya
* Best Child artist - Master Yash

== External links ==
*  

 

 
 
 
 
 
 
 