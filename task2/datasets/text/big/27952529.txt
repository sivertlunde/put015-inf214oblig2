Calamity the Cow
 
Calamity The Cow was a film made for the Childrens Film Foundation in 1967.  The film starred a teenage actor named Phil Collins just three years prior to his joining Genesis (band)|Genesis. The film was written by Kerry Eastman and directed by David Eastman.

==Plot Summary==
Farmer Grants children (including a young Phil Collins) get him to buy a cow from another farmer. The children work hard to make the cow fit and healthy enough for the show ring. But at the last minute the other farmer, Kincaid, steals Calamity.

==Notes==
Phil Collinss character disappears from the story for a long period on a mysterious biking holiday. In a 2003 television interview, Collins told Richard & Judy that this had been down to disagreements between himself and the director, who had then decided to write him out. Collins said he hated making the film.

== Cast ==
* John Moulder-Brown   as   Rob Grant
* Elizabeth Dear       as   Jo Grant
* Stephen Brown      as   Tim Lucas
* Phil Collins         as   Mike Lucas (as Philip Collins)
* Josephine Gillick    as   Beth Lucas Grant Taylor         as   Mr. Grant
* Honor Shepherd       as   Mrs. Grant
* Alastair Hunter      as   Kincaid
* Michael Warren       as   Ringer
* Desmond Carrington   as   Uncle Jim
* Ken Goodlet          as   Lefty
* Peter Halliday       as   Sergeant Watkins

==References==
 

==External links==
 
*  

 
 
 

 