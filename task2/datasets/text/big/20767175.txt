Thanneer Thanneer
{{Infobox film
| name = Thaneer Thaneer
| image = Thanneer Thanneer.jpg
| caption = Official poster
| director = K. Balachander
| producer = P. R. Govindarajan J. Duraisamy
| writer = K. Balachander
| story = Komal Swaminathan
| narrator =
| starring =  
| music = M. S. Viswanathan
| cinematography = B. S. Loknath
| editing = N. R. Kittu
| studio = Kalakendra Movies
| distributor = Kalakendra Movies
| released =  
| runtime = 143 mins 
| country = India
| language = Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}}

Thaneer Thaneer ( ) is a 1981 Indian Tamil language drama film directed by Kailasam Balachander|K. Balachander starring Saritha, Shunmugham, V. K. Veeraswami and Radha Ravi.    The film, based on the play in 1980 by the same name by Komal Swaminathan was filmed by B. S. Loknath and featured music by M. S. Viswanathan. It was released during the Diwali day in 1981. In the titles, Komal Swaminathan was credited for the original story, while screenplay was by K.Balachander. It is said that Komal Swaminathan was fully satisfied with the movie version of his powerful play.   
 National Film Awards and two Filmfare Awards South. IBN Live included the film in its list of 100 greatest Indian films of all time.  Balachander has revealed that there is no existing copy of the negative of the film anymore. 

== Plot ==
Thaneer Thaneer is a political drama and a universal human story based in a dry rural hamlet near Kovilpatti, in Tamil Nadu state of India.  This is a film about administration negligance of suffering villagers, excessive bureaucratic regulation (Red tape), greed, power and powerlessness. When a remote village faces severe water shortages, the inhabitants adopt all possible means to bring their problem to the attention of the authorities. But the process soon reveals the apathetic attitude of politicians, bureaucrats and the press alike. A convict wanted for the murder of a local landlord turns the villagers plight to his advantage and begins a co-operative scheme to transport water in a cart from a spring ten miles away.   

== Cast ==
* Saritha
* Shunmugham
* A. K. Veeraswami
* Radha Ravi
* Vathiyar Raman

== Reviews ==
Featuring fine performances, this film fared well at many international festivals where it was screened. Audiences were moved by the films realistic depiction of life in rural India, far removed from the exotic dream world of much popular cinema.  Sarithas performance was widely appreciated who supposedly lost the national award to Rekha (for Umrao Jaan) that year by a narrow margin.

== Awards ==
* National Film Award for Best Feature Film in Tamil
* National Film Award for Best Screenplay - K. Balachander
* Filmfare Award for Best Film&nbsp;– Tamil
* Filmfare Award for Best Director&nbsp;– Tamil - K. Balachander
* Cinema Express Award for Best Film&nbsp;– Tamil

== References ==
 

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 
 