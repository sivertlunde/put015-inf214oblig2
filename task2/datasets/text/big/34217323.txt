Two Gals and a Guy
{{Infobox film
| title          = Two Gals and a Guy
| image          =
| caption        =
| director       = Alfred E. Green
| producer       = John W. Arents Irving Weisner
| writer         = Searle Kramer
| narrator       =
| starring       = Janis Paige Robert Alda James Gleason Lionel Stander Arnold Stang
| music          = Hal David 
| cinematography = Gerald Hirschfeld
| editing        =
| studio         = Weisner Brothers Eagle-Lion Films
| distributor    = United Artists
| released       =  
| runtime        = 70 minutes
| country        = United States English
| budget         =
| gross          =
| awards         =
}}
Two Gals and a Guy (1951 in film|1951), also known as Baby and Me, is a comedy film directed by Alfred E. Green and starring Janis Paige, Robert Alda, James Gleason, Lionel Stander, Arnold Stang, The Three Suns, and Patty McCormack, in her film debut.  

The film was an independent production of the Weisner Brothers for Eagle-Lion Films and released by United Artists.

==Plot==
A singing couple (Alda and Paige) get their own weekly TV show, and the strain jeopardizes their relationship.

==Cast==
* Robert Alda as Deke Oliver
* Janis Paige as Della Oliver
* James Gleason as Max
* Lionel Stander as Seymour
* Arnold Stang as Bernard

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 