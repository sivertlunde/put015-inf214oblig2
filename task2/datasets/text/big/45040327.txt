Bhoomige Banda Bhagavantha
{{Infobox film 
| name           = Bhoomige Banda Bhagavantha
| image          =  
| caption        = 
| director       = K. S. L. Swamy (Ravee)
| producer       = S P Varadaraj J Chandulal Jain
| writer         = Gopala Naidu
| screenplay     = Narendra Babu Lakshmi Jai Jagadish Vajramuni
| music          = G. K. Venkatesh
| cinematography = B Purushottham
| editing        = Yadav Victor
| studio         = Jain Movies
| distributor    = Jain Movies
| released       =  
| country        = India Kannada
}}
 1981 Cinema Indian Kannada Kannada film, directed by K. S. L. Swamy (Ravee) and produced by S P Varadaraj and J Chandulal Jain. The film stars Lokesh, Lakshmi (actress)|Lakshmi, Jai Jagadish and Vajramuni in lead roles. The film had musical score by G. K. Venkatesh.  

==Cast==
 
*Lokesh Lakshmi
*Jai Jagadish
*Vajramuni
*M. S. Umesh
*C. R. Simha
*Rojaramani
*Sarvamangala
*Mallika
*Roopa Archana
*Kokila
*Vijayalakshmi Leelavathi in Guest appearance
*Ashalatha in Guest appearance
*Surendranath
*Lakshman
*Ashokkumar
*Ranadheer
*Jr Narasimharaju
*Comedian Guggu
*Rathnakar
*Sharapanjara Iyengar
*K. S. Ashwath in Guest Appearance
*Dinesh in Guest Appearance
*Sundara Krishna Urs in Guest Appearance
*Chethan Ramarao in Guest Appearance
*Rajanand in Guest Appearance
*Thyagaraj Urs in Guest Appearance
*Puneeth Rajkumar | Master Lohith
 

==Soundtrack==
The music was composed by GK. Venkatesh. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Chi. Udaya Shankar || 06.38
|-
| 2 || Kasturi Thilakavu || Vani Jayaram || Chi. Udaya Shankar || 01.37
|-
| 3 || Nagauvenu || S. Janaki|Janaki, Vani Jayaram || Chi. Udaya Shankar || 04.45
|-
| 4 || Baara || Vani Jayaram || Chi. Udaya Shankar || 04.30
|-
| 5 || Baalina Sutthalu || C. Ashwath || Chi. Udaya Shankar || 04.27
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 


 