Periyar (1973 film)
{{Infobox film 
| name           = Periyar
| image          =
| caption        =
| director       = P. J. Antony
| producer       =
| writer         = P. J. Antony
| screenplay     =
| starring       = Thilakan Kaviyoor Ponnamma P. J. Antony Sankaradi
| music          = KV Job PK Sivadas
| cinematography = TN Krishnankutty Nair
| editing        =
| studio         = Periyar Movies
| distributor    = Periyar Movies
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by P. J. Antony . The film stars Thilakan, Kaviyoor Ponnamma, P. J. Antony and Sankaradi in lead roles. The film had musical score by KV Job and PK Sivadas.   

==Cast==
*Thilakan
*Kaviyoor Ponnamma
*P. J. Antony
*Sankaradi Raghavan
*Alleppey Vincent Khadeeja
*Radhamani Sudheer
*Ushanandini

==Soundtrack==
The music was composed by KV Job and PK Sivadas and lyrics was written by P. J. Antony. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anthivilakku || S Janaki, Freddy || P. J. Antony || 
|-
| 2 || Bindu Bindu || P Jayachandran || P. J. Antony || 
|- Mehboob || P. J. Antony || 
|-
| 4 || Marakkaanum Piriyaanum || S Janaki, Freddy || P. J. Antony || 
|-
| 5 || Periyaare Periyaare || K. J. Yesudas || P. J. Antony || 
|}

==References==
 

==External links==
*  

 
 
 

 