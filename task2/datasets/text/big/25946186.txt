Shéhérazade (film)
 
{{Infobox film
| name           = Shéhérazade
| image          = Shéhérazade-poster.jpg
| caption        = Film poster
| director       = Pierre Gaspard-Huit
| producer       = Michel Safra Serge Silberman
| writer         = Pierre Gaspard-Huit José Gutiérrez Maesso Marc-Gilbert Sauvajon
| starring       = Anna Karina
| music          = 
| cinematography = André Domage
| editing        = Louisette Hautecoeur
| distributor    = 
| released       =  
| runtime        = 124 minutes
| country        = France
| language       = French
| budget         = 
}}

Shéhérazade is a 1963 French adventure film directed by Pierre Gaspard-Huit and starring Anna Karina.   

==Cast==
* Anna Karina - Shéhérazade
* Gérard Barray - Renaud de Villecroix
* Antonio Vilar - Haroun-al-Raschid
* Giuliano Gemma - Didier
* Marilù Tolo - Shirin
* Fausto Tozzi - Barmak
* Gil Vidal - Thierry
* Jorge Mistral - Grand Vizir Zaccar
* Fernando Rey
* Joëlle LaTour - Anira
* Rafael Albaicín Karamoko Cisse
* María Calvi
* José Calvo
* Félix Fernández (actor)|Félix Fernández
* María Granada
* José Manuel Martín - (as J.M. Martín)

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 