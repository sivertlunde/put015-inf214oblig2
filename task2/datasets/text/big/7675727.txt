The Uninvited (1944 film)
 
{{Infobox film
| name           = The Uninvited
| image          = The Uninvited (1944 film).jpg
| caption        = Lewis Allen
| producer       = Charles Brackett
| screenplay     = Frank Partos Dodie Smith
| based on       =  
| starring       = Ray Milland Gail Russell Ruth Hussey Donald Crisp Cornelia Otis Skinner
| music          = Victor Young Charles B. Lang
| editing        = Doane Harrison
| costumes       = Edith Head
| studio         = Paramount Pictures
| released       =  
| runtime        = 99 min.
| country        = United States
| language       = English
| budget         =
}}
 American supernatural romance directed Lewis Allen in his feature film debut.  It is based on the Dorothy Macardle novel Uneasy Freehold. The film stars Ray Milland, Ruth Hussey, and Donald Crisp, and introduces Gail Russell.
 Academy Award for Best Black and White Cinematography at the 17th Academy Awards. Cinematographer Charles Lang lost the Oscar to Joseph LaShelle, who won for his work on Laura (1944 film)|Laura.  Edith Head designed the costumes.

==Plot==
In 1937, London music critic and composer Roderick "Rick" Fitzgerald (Ray Milland) and his sister Pamela (Ruth Hussey) fall in love with Windward House, an abandoned seaside house, during a holiday on Cornwalls rocky coast. They purchase it for an unusually low price from Commander Beech (Donald Crisp).

Rick and Pamela meet Beech’s 20-year-old granddaughter, Stella Meredith (Gail Russell), who lives with her grandfather in the nearby town of Biddlecombe. Stella is deeply upset by the sale because of her attachment to the house, despite its being where her mother died. The commander has forbidden Stella to enter the house. However, she gains access to Windward House through Rick, who has become infatuated with her.

The Fitzgeralds initial enchantment with the house diminishes when they unlock an artists studio where they feel an inexplicable chill. Then, just before dawn, Rick hears the eerie sobs of an unseen woman, a phenomenon that Pamela investigates whilst awaiting her brothers return with their Irish housekeeper, Lizzie Flynn ( .

When Stella comes to Windward for dinner, she senses a spirit. Rather than fearing it, she associates the calming presence with her mother. Also, the strong scent of mimosa is that of her mothers favorite perfume.  Suddenly she dashes out towards the very cliff from which her mother Mary fell to her death seventeen years earlier. Rick catches her just before she reaches the edge. Stella professes to have no recollection of the near-fatal incident.

The Fitzgeralds and the town physician, Dr. Scott (Alan Napier), investigate. They learn that Stellas father, a painter, had had an affair with his model, a Spanish gypsy named Carmel. Stellas mother, Mary Meredith, from all accounts a beautiful and virtuous woman, found out and took Carmel to Paris, leaving her there. Subsequently Carmel returned to England, stole the infant Stella from Windward, and, during a confrontation, flung Mary Meredith off the cliff to her death. Shortly afterward, Carmel became ill and died.
 possessed by the spirit and begins muttering in Spanish. Distressed by Stellas renewed involvement with Windward, Beech sends Stella to a sanitorium run by Miss Holloway (Cornelia Otis Skinner), Marys friend and confidante. The Fitzgeralds visit and question Holloway, unaware that Stella is confined there. Holloway explains that after Marys death, she took care of Carmel, who had contracted pneumonia and eventually died of it. Looking through the records of the previous village physician, Dr. Scott discovers that Holloway may have hastened Carmels death.

The doctor is then called away to care for an ailing Beech, who tells him that Stella is at the sanitorium. Rick, Pam, and Scott telephone Miss Holloway to inform her that they are on their way. Holloway deceives Stella, saying that the Fitzgeralds have invited her to live with them. Stella happily takes the train home. A deranged Holloway tells the would-be rescuers that Stella is on her way to Windward House. There Stella finds only her grandfather in the studio. He begs Stella with his last strength to get out, but she remains at his side. When a ghost manifests, the commander succumbs to a heart attack. Stella welcomes the ghost, believing it to be her mother, but the apparition frightens her, and she flees towards the cliff.

Rick and Dr. Scott get there just in time to pull Stella from the crumbling cliff to safety. Back inside, the group is drawn again to the physicians journal, which the friendly spirit has turned to a certain page.  They discover that Carmel gave birth to a child (apparently in Paris, where Stella herself was born). The truth becomes clear: Carmel is Stellas mother.  Stellas realization of her true parentage frees Carmels spirit to leave Windward. Something evil, though, has remained. After sending everyone away, Rick confronts the spirit of Mary Meredith, telling her that they are no longer afraid of her and that she has no power over them any longer. Defeated, Marys spirit departs.

==Cast==
* Ray Milland as Roderick Fitzgerald
* Ruth Hussey as Pamela Fitzgerald
* Donald Crisp as Commander Beech
*  , 1999; ISBN 0-253-21345-2) 
* Dorothy Stickney as Miss Bird
* Barbara Everest as Lizzie Flynn
* Alan Napier as Dr. Scott
* Gail Russell as Stella Meredith

Uncredited cast members include Holmes Herbert, John Kieran, Queenie Leonard, Betty Farrington, and Moyna Macgill. 

==Production== Hollywood feature Blondie Has The Cat and the Canary, 1939; Abbott and Costello’s Hold That Ghost, 1941).
 censors when the film was distributed in England. 

==Music== score produced a popular hit, "Stella by Starlight", based on the film’s main theme music|theme. "Stella by Starlight", now a jazz standard, is prized by players for its haunting and rich harmony . It has been recorded numerous times,  by such artists as  Miles Davis, Stan Getz and Dexter Gordon, and as a vocal (with lyrics by Ned Washington) by singers Dick Haymes, Frank Sinatra, Ray Charles, Tony Bennett, Ella Fitzgerald among others.

==Reception== dark and windy night." 

As of April 2014, the film holds a rating of 83% on Rotten Tomatoes, based on 18 reviews with an average rating of 7.4/10.  According to Turner Classic Movies, the film is a "handsomely mounted thriller with a first-rate cast, atmospheric cinematography by Charles Lang, Victor Youngs emotionally gripping score, and a highly original story that invites Freudian interpretations of the characters while inserting a lesbian subtext and a droll sense of humor....While it might have chilled audiences of its era, The Uninvited is not a frightening film by contemporary standards. It is, however, an intriguing mood piece, as subtle and suggestive in its imagery as the best of Val Lewtons work."   

In 2009, director Martin Scorsese placed The Uninvited on his list of the 11 scariest horror films of all time.  Guillermo Del Toro also lists the film as one of the horror films to have scared and affected him.  

==Adaptations to other media==
 
The Uninvited was dramatized as a radio play on the August 28, 1944, broadcast of The Screen Guild Theater, with Ray Milland, Ruth Hussey and Betty Field. It was also presented on the November 18, 1949, broadcast of Screen Directors Playhouse, with Ray Milland, Alma Laughton and Mary Shipp.

==DVD release==
An official DVD release of The Uninvited—long available only on VHS from MCA Universal Home Video—by Exposure Cinema (a British company which specializes in limited collectors editions of overlooked Hollywood films) was announced for May 2012, although this has been rescheduled for September 2012.  The edition will apparently include a restored print and extensive booklet. 

The Criterion Collection released DVD and Blu-ray Disc versions of The Uninvited in October 2013; they feature a new 2K digital film restoration, with uncompressed monaural soundtrack on the Blu-ray version.

==See also==
*List of ghost films

==Notes==
 

==References==
 

==External links==
* 
*  
*  
Streaming audio
*  on the Screen Guild Theater: August 28, 1944
*  on the Screen Directors Playhouse: November 18, 1949

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 