When We Were Beautiful
 
{{Infobox film
| name           = When We Were Beautiful
| image          = When We Were Beautiful, Doc.jpg
| caption        = Phil Griffin
| producer       = Jon Kamen Jack Rovner Justin Wilkes
| writer         =
| starring       = Jon Bon Jovi Richie Sambora Tico Torres David Bryan
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
When We Were Beautiful, also known as Bon Jovi: When We Were Beautiful, is a 2009 feature documentary of the American rock band Bon Jovi, marking the bands 25th year. 

Premiered at the 2009 Tribeca Film Festival as a work in progress production, the film documents Bon Jovis Lost Highway Tour in 2007 and includes interviews with band members.  The film is directed by Phil Griffin and produced by Jon Kamen, Jack Rovner and Justin Wilkes. The production company for the release is Radical Media. 

==Production==
Phil Griffin stated that “throughout the process of working with Bon Jovi, I was amazed at the candor and humility Jon and his band showed me. The result is not a neatly wrapped up bundle of staged interviews and musical interludes, but rather a series of open and sometimes difficult conversations, explored against a backdrop of music spanning 25 years. It is a film about the peace of Tico, the complexity of Richie, the drive of Jon and the brutal honesty of Dave. It is their willingness to share their stories that gives us what I hope is a very human story—that humanity is what I believe has seen this band stand the test of time.” 

==Release== The Circle, released in the US on November 10 and the edited version is also included on the Live at Madison Square Garden Blu-ray.  A book of the same name as the film was also released in hardcover form on November 3, 2009  and in paperback form on November 2, 2010. 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 