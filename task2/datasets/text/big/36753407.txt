Preethi Prema Pranaya
{{Infobox film
| name = Preethi Prema Pranaya
| image = 
| caption = 
| director = Kavitha Lankesh
| producer = Mano Murthy, Ram Prasad, Dr. Renuka Ramappa, Somashekhar
| released = 1 August 2003
| runtime =
| language = Kannada
| music = Mano Murthy
| lyrics = 
| cinematography = A. C. Mahendran
| starring = Ananth Nag   Bharathi Vishnuvardhan   Anu Prabhakar
| country = India
}}
 drama film written and directed by Kavitha Lankesh. The film had an ensemble cast of Ananth Nag, Bharathi Vishnuvardhan, Sudharani, Prakash Raj, Bhavana (Kannada actress)|Bhavana, Anu Prabhakar and Sunil Raoh in the lead roles. It was produced by "Indo - Hollywood" films, consisting of five producers, namely Mano Murthy, Ram Prasad, Dr. Renuka and Somashekar.  The music was composed by Mano Murthy.

The film went on to become a musical hit and won many awards including the National Film Award for Best Feature Film in Kannada. 

The film revolved around three generations of two families having Ananth Nag and Bharathi Vishnuvardhan as first generation.

==Plot==
Family of Dr. Chandrashekhar (Ananthnag) - a widower in his family has a son Dr. Ashok (Prakash Rai) and daughter-in-law Jyothi (Sudharani) and grandson Vivek (Sunil Rao). Second son Ajay (Arun Sagar) too is a doctor with wife Sheela (Bhavana), an ad professional. Well settled doctor has enough name and fame but the concern doted by his sons irritates him. Their over cautious behavior suffocates him. Too much is too bad. He happens to meet Sharadha Devi (Bharathi), a widow, in an accident and as a doctor he does his duty but with an extra care. This leads into friendship and later they could not resist meeting each other with some pretext or the other. He finds the kind of affection, love and care that he needed at his age through this relationship with Sharadha Devi and vis-à-vis. Film takes an enlivening climax when Chandrashekar takes a bold step to face his family member and the society. This demonstrates that every human being needs someone who cares for them, who listens to them, who shares their feelings no matter what age group you are in.

==Cast==
* Ananth Nag
* Prakash Raj
* Bharathi Vishnuvardhan
* Sudharani
* Sunil Raoh
* Anu Prabhakar Bhavana
* Arun Sagar
* Lokanath
* Shivaram

==Soundtrack==
* "Manase Manase" - Suresh Peters, Archana Udupa
* "Ellidde Illi Thanka" - K. S. Chitra, Ram Prasad
* "Kabbina Jalle" - B. Jayashree, Ram Prasad
* "Chapala Chapala" - Ram Prasad, Sham
* "Ondu Maath Keltheeni" - Nanditha (singer)|Nanditha, Ram Prasad
* "Preethi Prema Pranaya" - Archana Udupa, Nanditha (singer)|Nanditha, Pallavi 

==Awards==
* National Film Award for Best Feature Film in Kannada
* Karnataka State Film Award for Best Story
* South Cine Fans Awards 2003-2004 for Best Music Director - Mano Murthy

==References==
 

==External links==
 

 
 
 
 


 