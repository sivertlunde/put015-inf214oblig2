Father's Day (2012 film)
{{Infobox film
| name           = Fathers Day
| image          = Fathers_day_Malayalamfilm.jpg
| image size     =
| border         =
| alt            =
| caption        =
| director       = Kalavoor Ravikumar
| producer       = Bharath Samuel
| writer         = Kalavoor Ravikumar
| screenplay     = Kalavoor Ravikumar
| story          = Kalavoor Ravikumar
| based on       =  
| narrator       = Lal  Shankar Panikkar
| music          = M. G. Sreekumar  Sajeev Mangalath 
| cinematography = S. G. Raman
| editing        = K. Sreenivas 
| studio         =
| distributor    = 
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Lal and Shankar Panikkar in the lead roles.       this is the second film from director Kalavoor Ravikumar, who earlier directed Oridathoru Puzhayundu. Oscar award winner Resul Pookutty made his acting debut through this film.   

==Plot==
The story revolves around Seetha, a tutor in a Government College. A person continuously goes behind her, spying her and later when they both meet up, he tells that he is a Criminology student, who specializes in Rapes. Seetha Lives in her nieces house. She has a caring brother, who is afraid that Seetha is not married yet. This student turns out to be her own son. He wanted to research on her history, so he collects all the relevant evidences, and starts his work. The story goes on and on, until it ends up in a DNA test. The story later revolves on how he takes revenge on the 4 rapists.

==Cast==
* Shehin
* Indu Thampy
* Revathi Lal
* Shankar Panikkar
* Idavela Babu Suresh Krishna
* Vijay Menon
* Vineeth
* Jagathy Sreekumar
* Lakshmipriya
* K. P. A. C. Lalitha
* Chithra Shenoy

==References==
 

 
 


 