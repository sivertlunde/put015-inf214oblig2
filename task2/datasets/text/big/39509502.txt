Ilo Ilo
 
{{Infobox film
| name           = Ilo Ilo
| image          = Ilo Ilo Movie Poster.jpg
| border         = yes
| caption        = Film poster
| director       = Anthony Chen
| producer       = Ang Hwee Sim Anthony Chen Wahyuni A. Hadi
| writer         = Anthony Chen
| editing        =  Hoping Chen Joanne Cheong
| based on       = 
| starring       = Chen Tianwen Yeo Yann Yann Angeli Bayani Koh Jia Ler
| music          =
| cinematography = Benoit Soler
| studio         = Singapore Film Commission Ngee Ann Polytechnic Fisheye Pictures
| distributor    = Memento Films International  (World Sales)  Golden Village Pictures  (Singapore)  Epicentre  (France)  Golden Scene  (Hong Kong)  Media Asia  (China) 
| released       =  
| runtime        = 99 minutes
| country        = Singapore
| language       = Chinese language|Chinese, English, Tagalog
| budget         = S$700,000   
| gross          = Singapore (S$1.2 million)
}}

Ilo Ilo ( ) is a 2013 Singaporean family film. The debut feature of director Anthony Chen, the film features an international cast, including Singaporean actor Chen Tianwen, Malaysian actress Yeo Yann Yann and Filipino actress Angeli Bayani.   
 Best Foreign Language Film at the 86th Academy Awards,    but was not nominated.

==Plot==
 

Set in Singapore during the 1997 Asian financial crisis, Ilo Ilo chronicles the Lim family as they adjust to their newly arrived Filipina domestic helper, Teresa, (Angeli Bayani) who has come, like many other Filipinas, in search of a better life.  The film’s Chinese title translates as “Mom and Dad Are Not Home”.  The father, Teck, (Chen Tianwen) works in sales for a glass company;  the pregnant mother, Hwee Leng, (Yeo Yann Yann) works as a secretary for a company that is down sizing;  and the ten-year-old son, Jiale, (Koh Jia Ler) is a troubled delinquent.

At first, Jia Le and Teresa (known as Terry) exhibit a troubled relationship, when during a trip to the bookstore Jia Le places some unpaid merchandise in the maids shopping bag causing her to be accused of theft. After being scolded by Terry, tensions rise, causing Jia Le to climb over the school fence at dismissal just to avoid his maid.

When the father loses his sales job, he conceals it from his wife, secretly smoking on the steps outside their apartment. After some time, unable to find work in a career position, he accepts a temporary job as a security guard monitoring an egg farm. As he continues to lose money in the stock market, in a moment of depression, he acknowledges their losses to his wife who lambasts him over his failure. As the family finances begin to descend deeper, familial tensions grow as Jiale continues to act out against his family and Terry. Upon the death of a neighbor who jumps from the roof of their apartment building, Jiale and Terry begin to kindle a relationship.

While the mother is desperate to stay employed as she continues to script termination letters at her job, Terry and Jiale become fast friends sparking the mothers jealousy as their relationship develops. Desperate at home, and emotionally neglected by her son, Hwee Leng attends a motivational seminar where she is moved by the optimistic words of the speaker and immediately purchases his full catalogue of motivational books and CDs. During a lunch break at work, she attempts to call the speaker using a phone number included on the seminars flyer, and discovers the line has been disconnected and rerouted. Later that night while watching television, she learns that the motivational speaker has been arrested for fraud causing Hwee Lang to break down in the presence of her confused husband as more money is lost.

Although Jiale is a poor student and is constantly in trouble, he shows high intellect and cunning in his obsessive calculation of past winning lottery numbers, which he catalogues in his schoolbook during class. One day, after being taunted by another boy that his maid only loves him because she is paid to, he pushes the boy into the bathroom wall causing him to split his head and bleed. Threatened with expulsion, and the schools administration unable to contact his father or mother, Terry arrives to plead for mercy on his behalf. After some resistance, the principal appears moved as Hwee Leng arrives angrily dispatching Terry and berating Jiale. As Hwee Leng and Jiale leave the administration office, Hwee Leng aggressively reminds Terry that she is Jiales mother before snatching Jiale and walking away. Given a punishment of public caning rather than expulsion, Terry arrives to the school auditorium, powerless, as she watches Jiale being whipped in front of the student body.

After the family car is sold for scrap, the family acknowledges they can no longer afford to keep Terry employed as Jia Les father has been recently fired from his job due to an accident tripping over eggs while attempting to find a suspected intruder. Desperate to keep Terry, Jia Le uses his savings to purchase lottery tickets but loses. He becomes tearfully despondent, cutting a lock of Terrys hair during a tense goodbye before she is sent home.

Holding on to his cassette player, Jiale listens to music with his father on a bench in the hospital as Hwee Leng gives birth to a baby girl.

==Cast==
* Chen Tianwen as Teck, the father
* Yeo Yann Yann as Hwee Leng, the mother
* Koh Jia Ler as Jiale
* Angeli Bayani as Teresa or Terry

==Production==
Partially funded by Ngee Ann Polytechnic and the Singapore Film Commission, the films budget is estimated to be around S$700,000.   

==Reception==
Ilo Ilo received a 100% rating from Rotten Tomatoes.

Ilo Ilo received positive reviews at the Cannes Film Festival. The film was later awarded the Camera dOr award, an award which recognizes the best debut feature film in the competition.  It received six nominations at the Golden Horse Film Festival and Awards.,  and won 4, namely  Best Film, Best New Director, Best Original Screenplay and Best Supporting Actress for Yeo Yann Yann. 

==Awards==

===Film===
*May 2013, Camera dOr, Canne Film Festival (Cannes, France)
*Sep 2013, Best Feature, 11th Pacific Meridian Film Festival (Vladivostok, Russia)
*Sep 2013, NETPAC Prize, 11th Pacific Meridian Film Festival (Vladivostok, Russia)
*Sep 2013, Grand Jury Prize, 10th Jameson Cinefest (Miskolc, Hungary)
*Sep 2013, FIPRESCI Prize, 10th Jameson Cinefest (Miskolc, Hungary)
*Sep 2013, International Ecumenical Jury Prize, 10th Jameson Cinefest (Miskolc, Hungary)
*Oct 2013, Best Narrative Feature Film, 22nd Philadelphia Film Festival (Philadelphia, United States)
*Oct 2013, First Feature “Sutherland Award”, 57th British Film Institute London Film Festival (London, United Kingdom)
*Oct 2013, Best Feature Film, Molodist Film Festival (Kiev, Ukraine)
*Oct 2013, FIPRESCI Prize, Molodist Film Festival (Kiev, Ukraine)
*Nov 2013, New Talent Award, Hong Kong Asian Film Festival (Hong Kong)
*Nov 2013, Best Film, 50th Golden Horse Awards (Taipei)
*Nov 2013, Piaget Best Screenplay Award (Special Award), 50th Golden Horse Awards (Taipei)
*Dec 2013, Audience Award, Tokyo Filmex Film Festival (Tokyo, Japan)
*Dec 2013, Best Film, Muhr AsiaAfrica Feature section, Dubai Film Festival (Dubai, UAE)

===Individual===
*Sep 2013, Best Actress (Yeo Yann Yann), 11th Pacific Meridian Film Festival (Vladivostok, Russia)
*Sep 2013, Best Actor (Koh Jia Ler), 9th Eurasia Film Festival, (Almaty, Kazakhstan)
*Oct 2013, Best Actress (Yeo Yann Yann), 15th Mumbai International Film Festival (Mumbai, India)
*Oct 2013, Best Director (Anthony Chen), 15th Mumbai International Film Festival (Mumbai, India)
*Nov 2013, Best Supporting Actress (Yeo Yann Yann), 50th Golden Horse Awards (Taipei, Taiwan)
*Nov 2013, Best New Director (Anthony Chen), 50th Golden Horse Awards (Taipei, Taiwan)
*Nov 2013, Best Original Screenplay (Anthony Chen), 50th Golden Horse Awards (Taipei, Taiwan)
*Dec 2013, Best Director (Anthony Chen), 7th Asia Pacific Screen Awards (Brisbane, Australia)
*Dec 2013, Best Supporting Actress (Yeo Yann Yann), Asia Pacific Film Festival (Macau)
*Dec 2013, Best Actress (Yeo Yann Yann), Muhr AsiaAfrica Feature section, Dubai Film Festival (Dubai, UAE)

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Singaporean submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

   
{{succession box
| title = Golden Horse Awards for Best Film
| years = 2013
| before= Beijing Blues
| after = --
}}
 

 
 

 
 
 
 
 
 
 
 