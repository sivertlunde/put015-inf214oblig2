A Thousand Little Kisses
 
{{Infobox film
| name           = A Thousand Little Kisses
| image          = 
| caption        = 
| director       = Mira Recanati
| producer       = 
| writer         = Mira Recanati
| starring       = Dina Doron
| music          = 
| cinematography = David Gurfinkel
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}}
 Best Foreign Language Film at the 54th Academy Awards, but was not accepted as a nominee.  It was also screened in the Un Certain Regard section of the 1981 Cannes Film Festival.   

==Cast==
* Dina Doron as Routa
* Kohava Harari
* Adi Kaplan
* Rivka Neuman as Alma
* Rina Otchital as Mara
* Daphne Recanati as Daphna
* Gad Roll as Mika
* Dan Tavori as Uri
* Nirit Yaron as Nili (as Nirit Gronich)
* Nissim Zohar as Eli o

==See also==
* List of submissions to the 54th Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 