Minsara Kanna
 
{{Infobox film
| name = Minsara Kanna
| image =
| director = K. S. Ravikumar
| writer = K. S. Ravikumar M. A. Kennedy (story)
| story =  Vijay Monicka Monicka Rambha Rambha Kushboo
| producer = K. R. Gangadharan
| editing = K. Thanigachalam
| cinematography = Ashok Rajan Deva
| studio = K. R. G Movies International
| released =  
| runtime =
| country = India Tamil
| budget =
}}
 Tamil film Vijay and Monicka in the lead roles, while an ensemble supporting cast includes Rambha (actress)|Rambha, Kushboo, Manivannan and Karan (actor)|Karan.   The films music is composed by Deva (music director)|Deva, and the film opened in September 1999 to average reviews and performed average at box office.

==Plot==
Indra Devi (Kushboo), the sister of Ishwarya (Monicka), is very rich and arrogant. Priya (Rambha) is her personal assistant. Kasi (Vijay) is the son of Mani (Mannivannan) and has fallen in love with Monicka in Germany. After hearing the story Indra Devi, Kasi come to India with all his family. Meanwhile Indra Devi is challenged by Vedhachalam (Mansur Ali Khan) who has also sent his son (Karan) to study in Germany. Karan also falls in love with Ishwarya. Kasi serves as a body guard for Kushboo, his brother works as a servant boy and Manivannan and his daughter serve as cooks for the Indra Devi s family. Priya (Ramba) falls in love with Kasi but later gives it to Ishwarya after knowing the truth of Kasi coming to India.

==Cast==
  Vijay as Kannan/Kasi Monicka as Ishwarya Rambha as Priya
*Kushboo Sundar as Indra Devi
*Manivannan as Mani Karan as Ashok Mansoor Ali Khan as Vedhachalam Anu Mohan
*Manobala
*Madan Bob
*R. Sundarrajan Mahendran
*K. S. Ravikumar
 

==Production== Meena were Rambha was confirmed. 

The team also shot scenes in Switzerland and Germany, with certain songs and scenes canned in the Alps.  

==Release==
The film opened to average reviews, with the critic of Indolink.com claiming the film "jogs along easily before becoming enmeshed in sentiments and cinematic cliches which make the last part of the movie all but unwatchable".  The New Indian Express criticized Vijays performance claiming "comedy is definitely not his cup of tea", and that "film drags on aimlessly" though praised Devas soundtrack.  The Deccan Herald also gave the film a negative review labelling that Vijay "is painful to watch and even worse to listen to", labelling it is "an exercise in how to waste a good movie". 

==Soundtrack==
{{Infobox album|  
  Name        = Minsara Kanna
|  Type        = Soundtrack Deva
|  Cover       =
|  Released    = 1999
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       = Bayshore
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Minsara Kanna (1999)
|  Next album  = 
}}
The soundtrack of the film was composed by Deva (music director)|Deva, was well received by the audience. The lyrics were written by Vaali (poet) |Vaali, Kalaikumar, Na. Muthukumar.

{| border="2" cellpadding="6" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; "
|- bgcolor="#CCCCCC" align="center"
!Song !! Singers
|---- Oodha Oodha Hariharan (singer)|Hariharan, Harini
|- Boy Frienda Mano (singer)|Mano, Sujatha Mohan
|- Un Per Solla Sujatha Mohan
|- Un Per Solla (duet) Hariharan (singer)|Hariharan, Sujatha Mohan
|- Theemukka
|S. Chitra
|- Oh Uncle Mano (singer)|Mano
|-
|}

==References==
 

 

 
 
 