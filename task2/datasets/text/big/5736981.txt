I Got the Hook Up
{{Infobox Film
| name           = I Got the Hook-Up
| image          = Igotthehookup01.jpg
| image size =  220px
| director       = Michael Martin
| producer       = Andrew Shack Bryan Turner Jonathan Heuer Leroy Douglas Master P M. Cevin Cathell
| writer         = Carrie Mungo Leroy Douglas Master P  richdruh Tommy "Tiny" Lister, Jr. 
| music          = Tommy Coster richdruh
| cinematography = Antonio Calvache
| editing        = T. David Binns
| studio         = Dimension Films
| distributor    = Miramax Films
| released       = May 27, 1998
| runtime        = 93 minutes
| country        = United States
| language       = English
| gross          = $10,317,779 
}} crime comedy directed by Michael Martin.  This was No Limit Records first theatrical release. The movie was distributed by Dimension Films.

==Plot==
 
Working out of their van, Black (Master P) and Blue (Anthony Johnson (actor)|Johnson) deal in TV sets and boomboxes, but then a driver mistakenly drops off a cell phone shipment. Business is on the upswing, but then the local crime boss, Roscoe, and his enforcer, T-Lay (Tom Lister, have a deal go sour and blame Black and Blue. Lorraines boss Dalton and the FBI are also closing in.

==Cast==
*Master P &mdash; Black
*Anthony Johnson (actor)|A.J. Johnson &mdash; Blue
*Anthony Boswell &mdash; Little Brother
*Frantz Turner &mdash; Dalton
*Gretchen Palmer &mdash; Sweet Lorraine
*Harrison White &mdash; Tootsie Pop
*Helen Martin &mdash; Blues Grandmother
*Joe Estevez &mdash; Lamar Hunt John Witherspoon &mdash; Mr. Mimm
*Mia X &mdash; Lola Mae Fiend &mdash; Roscoe Tommy "Tiny" Lister, Jr. &mdash; T-Lay
*C-Murder &mdash; O-Money
*Mr. Serv-On &mdash; Mad-Dogg
*Mystikal &mdash; Stupid Richard Keats &mdash; Jim Brady
*Sheryl Underwood &mdash; Bad Mouth Bessie
*Tangie Ambrose &mdash; Nasty Mouth Carla
*Ice Cube &mdash; Gun Runner
*Snoop Dogg &mdash; Pool Player / Bar Patron (Uncredited)

==Soundtrack==
 
A soundtrack containing hip hop music was released on April 7, 1998 by No Limit Records. It peaked at #3 on the Billboard 200|Billboard 200 and #1 on the Top R&B/Hip-Hop Albums.

==References==
 

==External links==
*  
* 
* 
*  by Master P featuring Sons of Funk: music video at MTV.com (Windows Media Video format)

 

 
 
 
 