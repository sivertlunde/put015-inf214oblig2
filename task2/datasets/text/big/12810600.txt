Born to Be Wild (1995 film)
{{Infobox film
| name           = Born to Be Wild
| image          = Born to Be Wild 1995.jpg
| image size     =
| caption        = Theatrical release poster John Gray
| producer       = Robert Newmyer Jeffrey Silver
| screenplay     = John Bunzel Paul Young
| story          = Paul Young
| starring       = Wil Horneff Helen Shaver John C. McGinley Peter Boyle
| music          = Mark Snow
| cinematography = Donald M. Morgan
| editing        = Maryann Brandon
| studio         = Fuji Entertainment Outlaw Productions
| distributor    = Warner Bros. Family Entertainment
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Born to Be Wild is a 1995 American family comedy film.

==Plot==
Rick Heller is a juvenile delinquent who continues to get himself into trouble. To keep him out of mischief his mother puts him to work cleaning the cage of a female gorilla named Katie, the mother is teaching to communicate through the use of sign language. When the owner of the gorilla, Gus Charnley, takes her away to become a flea market Sideshow|freak, Rick decides to rescue Katie and take her on an adventurous journey to get her out of the country.

==Cast==
*Wil Horneff as Rick Heller
*Helen Shaver as Margaret Heller
*John C. McGinley as Max Carr
*Peter Boyle as Gus Charnley
*Jean Marie Barnwell as Lacey Carr
*Marvin J. McIntyre as Bob the Paramedic
*Gregory Itzin as Walter Mallinson
*Titus Welliver as Sergeant Markle
*Thomas F. Wilson as Det. Lou Greenberg (as Tom Wilson)
*Alan Ruck as Dan Woodley
*John Procaccino as Ed Price
*Obba Babatundé as Interpreter
*David Wingert as Gary James
*John Pleshette as Donald Carr
*Janet Carroll as Judge Billings
*Frank Welker as Katie Gorillas vocal effects (uncredited)

==Reception== animatronic gorilla looked phony, but concluded "The film has its moments of nutty fun" and "it also has a couple of touching scenes—if you can get beyond that bogus ape look."  Washington Post critic Rita Kempley called it "a heart-yanking family yarn that resembles a simian adaptation of Nell (film)|Nell" and also compared the movie to Free Willy. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 