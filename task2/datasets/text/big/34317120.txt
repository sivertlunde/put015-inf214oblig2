Red Canyon (1949 film)
 
{{Infobox film
| name           = Red Canyon
| caption        =
| image	         = 
| director       = George Sherman
| producer       = Leonard Goldstein, Aaron Rosenberg
| writer         = Zane Grey (novel), Maurice Geraghty (screenplay)
| starring       = see list below
| music          = Walter Scharf
| cinematography = Irving Glassberg Otto Ludwig
| distributor    =
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
| gross          =

}}
 Western directed by George Sherman.

The plot revolves around Black Velvet, a wild stallion that runs rampant across the range. Two people, reformed bad man Lin Sloan (played by Howard Duff) and tomboyish farmers daughter Lucy Bostel (Ann Blyth), think they can tame him.  In the process, they tame each other.

== Cast ==
* Ann Blyth as Lucy Bostel
* Howard Duff as Lin Sloan
* George Brent as Matthew Bostel
* Edgar Buchanan as Johnah Johnson
* John McIntire as Floyd Cordt
* Chill Wills as Brackton
* Jane Darwell as Aunt Jane
* Lloyd Bridges as Virgil Cordt
* James Seay as Joel Creech
* Edmund MacDonald as Farlane David Clarke as Sears
* Denver Pyle as Hutch
* Hank Patterson as Osborne
* Ray Bennett as Pronto
* Hank Worden as Charley

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 