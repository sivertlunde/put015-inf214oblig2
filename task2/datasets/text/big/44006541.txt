Kuyiline Thedi
{{Infobox film
| name = Kuyiline Thedi
| image =
| image_size =
| caption =
| director = M. Mani
| producer = M. Mani
| writer = Priyadarshan
| screenplay = Priyadarshan Rohini Karan Master Raghu Sukumari Mohanlal Shyam 
| cinematography = DD Prasad
| editing = VP Krishnan
| studio = Sunitha Productions
| distributor = Sunitha Productions
| released =  
| country = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, Master Raghu, Sukumari and Mohanlal in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
   Rohini  Master Raghu 
*Sukumari 
*Mohanlal 
*Adoor Bhasi 
*Manavalan Joseph 
*V. D. Rajappan 
*Master Manohar
*Noohu
*Paravoor Bharathan 
*Poojappura Ravi 
*Ranipadmini 
*Sathyakala 
 

==Soundtrack==
The music was composed by Shyam (composer)|Shyam. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Krishna Nee Varumo || K. J. Yesudas, Jayachandran || Chunakkara Ramankutty || 
|-  Janaki || Chunakkara Ramankutty || 
|-  Janaki || Chunakkara Ramankutty || 
|- 
| 4 || Neelavaanam Poothuninnu || K. J. Yesudas, Vani Jayaram, Chorus || Chunakkara Ramankutty || 
|- 
| 5 || Paathiraa Thaarame || K. J. Yesudas || Chunakkara Ramankutty || 
|- 
| 6 || Sindoora Thilakavumaay || K. J. Yesudas || Chunakkara Ramankutty || 
|- 
| 7 || Sindoora Thilakavumaay   || K. J. Yesudas || Chunakkara Ramankutty || 
|}

==References==
 

==External links==
*  

 
 
 


 