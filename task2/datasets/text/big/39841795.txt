Suite Française (film)
 
 
{{Infobox film
| name           = Suite Française
| image          = Suite Francaise poster.jpg
| alt            = 
| caption        = UK release poster
| director       = Saul Dibb Michael Kuhn Xavier Marchand
| screenplay     = Saul Dibb Matt Charman
| based on       =   Michelle Williams Kristin Scott Thomas Matthias Schoenaerts
| music          = Rael Jones
| cinematography = Eduard Grau
| editing        = Chris Dickens TF1 Films Radio Television TSE
| distributor    = Entertainment One (U.K.) UGC Distribution (France) The Weinstein Company (U.S./Worldwide)
| released       =  
| runtime        = 107 minutes  
| country        = United Kingdom France Belgium
| language       = English German
| budget         = Euro|€15 million (United States dollar|$20 million)   
| gross          = £1.2 million
}} romantic World novel of Michelle Williams, Ruth Wilson, German occupation of France. Suite Française was filmed on location in France and Belgium. It was released on 13 March 2015.

==Plot==
In Military Administration in France (Nazi Germany)|Nazi-occupied France, Lucille Angellier waits for news of her husband, along with her domineering mother-in-law. When a regiment of German soldiers arrive in the town, they soon move into the villagers homes. Lucille tries to ignore Bruno, the German commander who has been posted at her house, but she soon falls in love with him.

==Cast==
  Michelle Williams as Lucille Angellier
* Kristin Scott Thomas as Madame Angellier
* Matthias Schoenaerts as Commander Bruno von Falk
* Sam Riley as Benoit Ruth Wilson as Madeleine
* Lambert Wilson as Viscount de Montmort
* Margot Robbie as Celine
* Alexandra Maria Lara
* Harriet Walter as Viscountess de Montmort
* Eileen Atkins as Denise Epstein
* Tom Schilling as Kurt Bonnet
* Deborah Findlay as Madame Joseph
* Cédric Maerckx as Gaston Angellier
 

==Production==
===Conception and adaptation=== Suite Française Nazi occupation The Pianist, Kathleen Kennedy Frank Marshall TF1 Droits Audiovisuels acquired the rights to the novel from publisher Éditions Denoël.  The novel was then adapted for the screen by Saul Dibb and Matt Charman, with Dibb directing.  The film was given a budget of €15 million ($20 million), which Mick Brown from The Daily Telegraph noted was "big by European, if not American, standards".  

Dibb focused his adaptation on the second half of Némirovskys novel, which explores the relationships between the French women and the German soldiers who occupied their village, in particular the story of Lucile Angellier, who is waiting for news of her husband, when a German officer is billeted in her home.    Of his intentions, Dibb explained "in the making of the film I want to capture this strong sense of immediacy and authenticity. The action should feel that its happening now – urgent, tense, spontaneous, made with no benefit of hindsight – like weve discovered a time capsule. And very far from a safe, stuffy period piece."  Dibb used the story of the novels discovery by Némirovskys daughter, Denise Epstein, to book-end the film.  Némirovskys original manuscript is shown during the credits. Epstein died shortly before production began, but she read drafts of the script.   

===Casting=== Michelle Williams was in talks to star in Suite Française as the protagonist Lucille Angellier.  Shortly after she joined the project, Kristin Scott Thomas was attached to appear as Lucilles "domineering" mother-in-law.  In an interview with Moviefones Erin Whitney, Scott Thomas commented that the character was similar to herself.  In November, Baz Bamigboye from the Daily Mail confirmed that Matthias Schoenaerts was in the final stages of negotiations to play Lucilles love interest, Bruno. 
 Ruth Wilson as Madeleine.   Actors Tom Schilling and Lambert Wilson will also appear in the film as Kurt Bonnet and Viscount de Montmort respectively.   Harriet Walter has been cast as Viscountess de Montmort, while Eileen Atkins will appear as Denise Epstein and Cédric Maerckx as Gaston Angellier.   

===Filming===
  Marville in the Meuse Departments of France|department.  In August, the cast and crew spent three-and-a-half weeks filming scenes in a house in Belgium. Dibb commented that the hot weather helped create "its own weird, uncomfortable, claustrophobic atmosphere which you hope is going to feed into the atmosphere of the scenes."    Leo Barraclough from Variety (magazine)|Variety reported that principal photography was completed on 2 September 2013.   

===Costumes and make-up=== Michael OConnor, The Duchess, designed and created the clothing for the film.  OConnor used photographs, magazines and movies of the time to make the clothing and accessories as authentic as possible.    His major influences were Némirovskys novel, which describes the characters clothing in detail, and Jean Renoirs 1939 film The Rules of the Game.  The costumes were created using authentic French fabrics from Paris.  Jenny Shircore was the hair and make-up designer for the production.  Due to the films countryside setting, the hair and make-up could not be too "glamorous". Shircore aimed to invoke a sense of "sobriety".  Shircore used very little make-up on Williams, explaining that the actress was aware that it had to be "very natural, simple, in a sense, resigned." 

===Music===
French composer Alexandre Desplat was originally attached to compose the Film score|films musical score,  but he was later replaced by Rael Jones. The score was recorded at the Abbey Road Studios in London.    It features original music composed by Jones, a piano piece composed by Desplat and songs performed by Lucienne Boyer, Josephine Baker and Rosita Serrano.    Sony Classical released the soundtrack album on 16 March 2015.  Dibb explained that music plays a crucial part in the film and called the score "subtle and atmospheric."    Dibb wanted Brunos piano piece to be composed before shooting began, and he wanted it to be played during the film as "a developing theme" and then at the end in its full form. 

==Release== trailer was released on 24 October 2014.    Suite Francaise was shown at the American Film Market the following month in a bid to find an American distributor.  In May 2013, it was announced that The Weinstein Company would cover the U.S., Latin America, Australia, Russia and Germany territories for distribution.    The film was released in the UK on 13 March 2015. 

==Reception==
Suite Française earned £503,928 upon its opening weekend in the United Kingdom. The film opened to 425 locations and landed at number four in the UK box office top ten.  The following week the film played at a further 21 screens and earned £268,607 for a total of £1,293,408. It fell three places to seven in the box office chart. 
 rating average of 5.9 out of 10.    Metacritic, which uses a weighted average, assigned a score of 63 out of 100 based on 8 reviews, indicating "generally favorable reviews."  The Guardians Peter Bradshaw gave the film two stars out of five and likened the central love story to "a damp haddock on a slab".    He continued, "This adaptation of Irène Némirovskys acclaimed bestseller about French folk collaborating with the Nazis is flabby, sugary – and passion-free."  Emma Dibdin from Digital Spy gave the film three stars and commented, "Suite Française works far better as the story of a community in flux than it does as a brooding romance, the shifting loyalties between villagers and soldiers escalating towards a somewhat compelling third act." 

Anna Smith from Empire (film magazine)|Empire rated the film "good" and said "Sterling performances lift the occasionally soapy storyline in this semi-successful adaptation."  Varietys Guy Lodge found the film "fusty but enjoyably old-fashioned", adding "iffy scripting decisions cant thwart the romantic allure of this handsomely crafted, sincerely performed wartime weeper."  Writing for The Hollywood Reporter, Leslie Felperins consensus was "  has sturdy production values, a tony cast and middlebrow tastefulness up the wazoo, but barely any soul, bite or genuine passion." 
 16th Golden Trailer Awards. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 