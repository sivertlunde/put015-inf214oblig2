Se Eu Fosse Você
{{Infobox film
| name = Se Eu Fosse Você
| image = Se Eu Fosse Você.jpg
| director = Daniel Filho	 	 	
| producer = 
| writer = 	 	
| starring = Glória Pires Tony Ramos Lavínia Vlasak Thiago Lacerda Glória Menezes Danielle Winits Patrícia Pillar
| music = 	 	
| cinematography = José Roberto Eliezer	 		
| editing = Felipe Lacerda	 		 	
| studio = Total Entertainment
| distributor = 20th Century Fox
| released =  
| runtime = 108 minutes
| country = Brazil
| language = Portuguese
| budget = BRL|R$ 5 million 
| gross  = $12.316.942
}}

Se Eu Fosse Você (   directed by Daniel Filho.

The film was a box office success, having the largest audience of a Brazilian film in 2006. It was followed by a sequel Se Eu Fosse Você 2 (2009). 

== Plot ==
The film follows the story of Cláudio, a successful publicist who owns his own agency, and Helena, his wife, a music teacher who takes care of a childrens choir. Accustomed to the day-by-day marriage routine, they occasionally argue. One day they have a bigger fight than normal, which causes something inexplicable to happen: they switch bodies. Terrified, Claudio and Helena try to appear normal until they can reverse the situation. But to do so they will have to fully assume each others lives. 

==Cast==
  
* Glória Pires as Helena / Cláudio
* Tony Ramos as Cláudio / Helena
* Lavínia Vlasak as Bárbara
* Thiago Lacerda as Marcos
* Glória Menezes as Vivinha
* Lara Rodrigues as Bia
* Danielle Winits as Cibelle
* Patrícia Pillar as Dr. Cris
* Maria Gladys as Cida
* Ary Fontoura as Priest
* Helena Fernandes as Débora
* Maria Ceiça as Márcia
* Leandro Hassum as Maurício
* Carla Daniel as Carla Daniel
* Marcela Muniz as Marília
* Antônia Frering as Tereza
 

==Soundtrack==
{{Infobox album  
| Name = Se Eu Fosse Você (Soundtrack)
| Type = Soundtrack
| Artist = Various Artists
| Cover = Se Eu Fosse Você Soundtrack.jpg
| Released = 2006
| Genre = 
| Length =
| Label = 
}}
{{tracklist
| music_credits = yes
| title1 = Mulheres Gostam
| music1 = Marina Elali
| title2 = Chapa Quente
| music2 = Pérola Black           
| title3 = So Deep
| music3 = House Brothers 
| title4 = Dance With Me
| music4 = Lulu Joppert 
| title5 = Runaway People
| music5 = Lucas Babin 
| title6 = Hipnotizar Você 
| music6 = Marina Elali 
| title7 = Slow Motion Bossa Nova
| music7 = Celso Fonseca 
| title8 = Here´s Your Chance 
| music8 = Alma Thomas
| title9 = How Gee
| music9 = Black Machine 
| title10 = 9ª Sinfonia de Beethoven (Ode à Alegria) Versão Hip Hop
| music10 = Jovens Princesas de Petrópolis
| title11 = 9ª Sinfonia de Beethoven (Ode à Alegria) Maxpop Remix
| music11 = Jovens Princesas de Petrópolis
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 