Man in the Shadow
{{Infobox film
| name        = Man in the Shadow
| image       = Man in the Shadow film poster.jpg Jack Arnold
| producer    = Albert Zugsmith
| writer      = Gene L. Coon  Orson Welles Jeff Chandler Ben Alexander
| studio      = Universal-International
| distributor =
| released    = September 20, 1957
| runtime     = 80 min.
| language    = English
| budget      =
}}
 crime film Jeff Chandler, Ben Alexander, and John Larch.

==Plot summary==
The cow town of Spurline is effectively ruled by Virgil Renchler (Welles), owner of the Golden Empire ranch. 

One night, some of Renchlers hands beat a young laborer, Juan Martín, to death. The newly elected sheriff of Spurline, Ben Sadler (Chandler), decides to investigate the matter. During his investigation, he must contend with Renchlers henchmen and the fierce opposition of the townspeople, who fear Spurline would be ruined without the Golden Empires business.

==Cast== Jeff Chandler as Ben Sadler
*Orson Welles as Virgil Renchler
*Colleen Miller as Skippy Renchler Ben Alexander as Ab Begley
*Barbara Lawrence as Helen Slader
*John Larch as Ed Yates
*James Gleasonas Hank James
*Royal Dano as Aiken Clay
*Paul Fix as Herb Parker
*Leo Gordon as Chet Huneker
*Martin Garralaga as Jesus Cisneros
*Mario Siletti as Tony Santoro

==Production==
The part of Virgil Renchler was originally going to be played by Robert Middleton but agents from the William Morris Agency suggested Orson Welles, who badly needed the money to pay back taxes. During the course of making the film, Welles rewrote sections of the script. He also formed a relationship with Albert Zugsmith who produced Welles next movie as director, Touch of Evil (1958). 

Director Jack Arnold says he had one incident with Orson Welles on Welles first day of shooting but after that he was "wonderful" to work with and full of good ideas. 

==References==
 

==External links==
*  
*   at TCMDB
 

 
 
 
 
 
 


 