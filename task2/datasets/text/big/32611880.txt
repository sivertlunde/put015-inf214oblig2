Les amants de Vérone
 
{{Infobox Film
| name           = Les amants de Vérone
| image          = 
| caption        = 
| director       = André Cayatte
| producer       = Raymond Borderie
| writer         = André Cayatte  Jacques Prévert
| starring       = Serge Reggiani, Anouk Aimée, Pierre Brasseur
| music          = Joseph Kosma
| cinematography = 
| editing        = Christian Gaudin
| distributor    = 
| released       = 7 March 1949 (France) 
| runtime        = 105 min
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Les amants de Vérone (The Lovers Of Verona) is a 1949 French film directed by André Cayatte and loosely based on the William Shakespeare play, Romeo and Juliet.  The film was a joint project of  post war Italy and involves Angelo, a Glassblowing|glass-blower from Murano, and Georgia Maglia, the daughter of a fascist magistrate.

Angelo and Georgia are thrown together when they become stand-ins for the stars of a film version of Romeo and Juliet being shot on location in Venice. Inevitably they fall in love and their affair parallels  the Shakespeare tragedy.  The principal difficulty is the scheming of Rafaële, the Magia familys ruthless consigliere.  In the end, Angelo is killed and Georgia dies at his side.

==Critical reception==
TV Guide called it "An intriguing romance"   but Bosley Crowther did not like the film, calling it, "story, set within a weird and grotesque frame of contemporary morbidness in Venice and gaudy film-making in Italy"  Pauline Kael said, "The films sensuous poetic elegance contrasts with the seamy elements it encompasses...You may feel youve been made too aware of the films artistic intentions, and the romanticism can drive you a little nuts" 

==Cast==
* Serge Reggiani: Angelo (Romeo)
* Anouk Aimée: Georgia (Juliet)
* Martine Carol: Bettina Verdi, the star of the movie
* Pierre Brasseur: Rafaële
* Marcel Dalio: Amedeo Maglia
* Marianne Oswald: Laetitia
* René Génin: The guardian of the tomb Yves Deniaud: Ricardo, an actor
* Charles Blavette: The head of the glassworks
* Marcel Peres: Domini, a glass blower

==References==
 

==External links==
*   Fandango

 

 
 
 
 
 
 
 
 

 