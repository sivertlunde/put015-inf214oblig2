Zinda (film)
 
 
{{Infobox film
| name           = Zinda
| image          = Zindamovieposter.jpg
| caption        = Theatrical release poster Sanjay Gupta
| producer       = Sanjay Gupta
| screenplay     = Sanjay Gupta Suresh Nair John Abraham Lara Dutta Celina Jaitly Strings
| Old Boy by Garon Tsuchiya and Nobuaki Minegishi
| cinematography = Sanjay F. Gupta
| editing        = Bunty Nagi
| released       = 12 January 2006
| country        = India
| language       = Hindi
| awards         =
| budget         =
}} John Abraham, Sanjay Gupta Strings sung one of the film songs which became quite popular. It was released in India on 12 January 2006.
 Dreamworks in 2004, initially expressed legal concerns but no legal action was taken as the studio had shut down.     

==Synopsis==
Software engineer Balajeet "Bala" Roy (Sanjay Dutt), is happily married to, Nisha Roy (Celina Jaitly), with whom he is having a baby. Bala is suddenly captured by unseen assailants and imprisoned in a cell. He is kept in total isolation for 14 years without knowing who imprisoned him or why.  While in captivity, he practices martial arts which he learns from watching T.V., with the intention of using it against the people who captured him. He is finally released, again without explanation, and sets out for revenge.
 John Abraham). Soon Wong Foo kidnaps Jenny and tortures her. He threatens to remove Balas teeth with his own clawhammer, but is interrupted by Rohit. Bala takes refuge with Jenny, and they have sex. Bala is informed that his daughter is alive. Balas friend Joy (Mahesh Manjrekar) is killed, and Bala learns his kidnapper which is none other than Rohit. 

Rohit reveals his reason of kidnapping Bala: they went to high school together, where Bala had lusted after Rohits elder sister Reema. After Reema rejected him, Bala spreads a false rumour that she was a whore. She became the laughing stock of their school, and committed suicide by setting herself on fire. Rohit blamed Bala for her death, and engineered his imprisonment as revenge. Rohit tells Bala that he killed Nisha, and sent his daughter, who is now 14, to a brothel. Bala beats Rohit up and knocks him off of a balcony, but grabs his hand and pleads with him to tell him where his daughter is. Defiantly, Rohit lets go of Balas hand and falls to his death. Bala then kills Rohits goons and Wong Foo. In the end, Bala learns that his daughter is safe; Rohit had lied to him about selling her to a brothel to torment him. He finds her sitting on a river bank, and goes to meet her.

==Cast== Balajeet "Bala" Roy John Abraham as Rohit Chopra
* Lara Dutta as Jenny Singh
* Celina Jaitly as Nisha Roy
* Mahesh Manjrekar as Joy Fernandes
* Rajendranath Zutshi as Woo Fong (as Raj Zutshi)
*Gaurav Chanana

==Reception==

===Critical reception=== John Abraham and Sanjay Dutt were singled out for praise.

Bollywood Mantra praised the film saying, "Zinda is a film that appears to be darkest movie ever by Sanjay Gupta. A hard hitting flick shot in Bangkok, it tells you a story never told before on Indian screen. And to make Sanjay Guptas imagination come alive, who else but Sanjay Dutt is roped in to play the lead role. One of the most challenging roles ever by the deadly Dutt, it is sure to haunt you long after the screening is over". 
 John Abraham, while criticising the fact that much of the dialogue and story was copied from Oldboy (2003 film)|Oldboy.

Futuremovies.com gave the film 6/10 and said, "Technically and style-wise Zinda is flawless", and praised Dutts performance, saying "it is probably the pinnacle of his career". 

Subhash K Jha gave it 2.5/5 and stated that "the film belongs to Sanjay Dutt.... If the adventure-action genre in Hindi cinema needed a wake-up call, this is it". 

Nikhil Kumar of Apunkachoice.com gave the film 0.5/5 stars, saying "Sanjay Guptas movie Zinda works primarily because of its gripping, although unoriginal, story and a noteworthy acting performance by its frontman Sanjay Dutt". 

==Soundtrack==
The songs were composed by duo Vishal-Shekhar and released by T-Series.  Strings
* "Zinda Hoon Main" – Shibani Kashyap
* "Har Saans" – Krishna Beura
* "Kya Main Zinda Hoon" – Shibani Kashyap
* "Maula" – Vinod Rathod
* "Zinda Hoon Main Y" – Shibani Kashyap
* "Chal Rahi Hai Saanse" – Kailash Kher
* "Har Saans (Black Mamba Mix)" – Krishna Beura

==Similarities with Oldboy==
At a November 2005 press conference, representatives of Show East, the production company that released Oldboy, expressed concern Zinda was similar to their film, and said they were investigating the similarities.  They noted that at the time, they did not have the final version of Zinda available to compare Oldboy with. They stated that if they found there was "strong similarity between the two  ", they would be contacting their lawyers." 

The Hindu reviewer Sudhish Kamath and Planet Bollywood reviewer Narbir Gosal both note in their reviews of Zinda that they found the two films to be very similar in terms of plot, as well as in the depiction of specific scenes.   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 