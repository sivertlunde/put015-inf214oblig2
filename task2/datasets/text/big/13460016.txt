Axe (film)
{{Infobox film
| name           = Axe
| image          = Axe_film.jpg
| image size     =
| caption        = Theatrical poster
| director       = Frederick R. Friedel
| producer       = J.G. Patterson Jr.
| writer         = Frederick R. Friedel
| narrator       =
| starring       = Leslie Lee Hart Smith Carol Miller
| music          = George Newman Shaw John Willhelm
| cinematography = Austin McKinney
| editing        = Frederick R. Friedel J.G. Patterson Jr.	 
| studio         = Frederick Productions Empire Studios
| distributor    = Boxoffice International Pictures (BIP) 
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 Video Nasties" that was banned in the United Kingdom in the early 1980s.

==Plot==
A group of three psychopaths on the run from the law kill a man and flee into the countryside. They descend on an isolated farm which is home to a young girl named Lisa (Leslie Lee) and her paralyzed grandfather. After suffering much humiliation at the hands of the gang, Lisa takes a ghastly, murderous revenge.

== Cast ==

* Leslie Lee as Lisa
* Jack Canon as Steele
* Ray Green as Lomax
* Frederick R. Friedel as Billy
* Douglas Powers as Grandfather
* Frank Jones as Aubrey
* Carol Miller as Storewoman
* George J. Monaghan as Harold
* Hart Smith as Detective
* Scott Smith as Policeman
* Jeff MacKay as Radio and Television Shows (voice)
* David Hayman as Radio and Television Shows (voice)
* Don Cummins as Radio and Television Shows (voice)
* Jaqueline Pyle as Radio and Television Shows (voice)
* Lynne Bradley	as Radio and Television Shows (voice)
* Richie Smith as Radio and Television Shows (voice)
* George Newman Shaw as Radio and Television Shows (voice)
* Ronald Watterson as Radio and Television Shows (voice)
* Beverly Watterson as Radio and Television Shows (voice)
* Graddie Lane as Radio and Television Shows (voice)
* Suzy Bertoni as Radio and Television Shows (voice)

==Production==
The film was shot on location in a farmhouse outside Charlotte, North Carolina. 

== Controversy ==
The film gained notoriety in 1984 when it was designated a video nasty in the United Kingdom and appeared on the director of public prosecutions list of banned films. It was passed with cuts in 1999 and finally released uncut in 2005.

== Critical reception ==
TV Guide gave the film three out of four stars, calling it "a well-photographed, refreshingly naturalistic drama of almost mythic retribution and victimization.   The psychological narrative can be slack, and the acting and technical aspects are uneven. But overall, the film makes you wish Friedel had directed more pictures." 

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 