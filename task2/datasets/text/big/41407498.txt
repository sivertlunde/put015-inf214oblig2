The Upper Footage
{{Infobox film
| name           = The Upper Footage
| image          = TheUpperFootagePoster.jpg
| alt            = 
| caption        = Poster used to market the movie release and announce the disappearance of "Jackie" Justin Cole
| producer       =  Justin Cole
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         =  Vimeo VOD  
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} found footage film directed and written by Justin Cole. The movie first released on January 31, 2013 to a limited run of midnight theatrical screenings at Landmark’s Sunshine Cinema in New York City. The film centers upon the death of a teenager and attempts to cover it up. The film was initially met with much controversy, as some believed that the footage depicted an actual death and cover up after the media reported on the events around the film as fact for years prior to its release.  

Cole later issued an email to Dread Central confirming that the entire film was fictional in nature.    

==Synopsis==
The film is said to be a 90-minute edited version of 393 minutes of recovered footage documenting the death and subsequent cover up of a young woman by a group of NYC socialites. The film is set up in a documentary format explaining all the media events around the project before and after the showcased footage.

==Marketing==
Drawing from the marketing for films such as Blair Witch Project, Cole allegedly began releasing videos on YouTube starting in 2010, all of which featured the actors with pixelated faces.    The first uploaded clip was titled "NYC Socialite Overdose" followed by 2 additional clips that were eventually pulled from YouTube.  He labeled the first film "Socialite Overdose" and uploaded several more clips from the movie during 2011, all of which were supposedly pulled off of YouTube.    Media outlets then alleged that Quentin Tarantino had expressed an interest in picking up the footage and releasing it under the name Upper, but that he had to "kill" the film due to pressure from outside forces.  A website then emerged and claimed to have the complete footage and would release it under the name of The Upper Footage.  Some media outlets reported on the footage, claiming it to be true.  Chelsea Kane, a Dancing With The Stars contestant at the time and Disney starlet was briefly named as one of the participants in the video, prompting her to issue a statement saying that it was not her.  During this time, fliers and posters advertising the disappearance of "Jackie" were distributed in several areas. 

In 2013 Cole released an e-mail to Dread Central, explaining that his intentions were not "to fool people for the sake of fooling them but to give them an experience where they could leave a movie theater, discuss the film, wonder if the footage was real or not and if something like this could/does happen".  He went on to say that while there was some "scandal" surrounding the film, it was not due to any death but because one of the films actresses later objected to one of the scenes she had shot previously.  Because of various issues the footage could not be re-shot and Cole agreed to leave the actresss face blurred throughout the production to preserve her anonymity. 

==Reception==
Reception to the marketing was mixed and prior to Coles e-mail to Dread Central, some skepticism was expressed over the claims made about the footage and allegations of Tarantinos involvement.    A columnist for Dread Central questioned the validity of Tarantinos involvement, as they did not think that he would actually show interest in something that "would probably be next to impossible to show publicly".  He also noted that other than a website called "TheDirty", no other  websites had reported on the overdose, which he found unlikely considering the content in the video. 

Critical reception to The Upper Footage upon its release was also mixed.  CultureMass called the film "One of the best of 2013" saying that "The performances here are nothing short of superb. This is quite possibly one of the best ensembles that I have seen since Jacob Aaron Estes’ Mean Creek. There isn’t one false note to be found here, which makes the experience all the more authentic and unsettling."  New York City film critic Avi Offer rated the film 9.5 out of 10 on Rotten Tomatoes and “irresistibly entertaining, provocative and bold” while highlighting the film’s “statement about the corruption, selfishness and amorality of the 1% and how easily they can use their powers to control mainstream media to cover up their crimes.”    Aint It Cool News gave a mostly positive review, stating that while the marketing for the movie was interesting,  "UPPER succeeds in making you feel like this footage is real because it all does seem so drawn out and, dare I say, boring" but that overall "it still is more effective as a found footage film than most."    Bloody Disgusting panned the film overall, also calling it "boring" and criticizing the marketing and media as "not worth it" and that stating that they thought that the idea would have been more interesting left as "a comment on truthfulness and accountability in the media".  Screen Rant stated that they found the acting to be flawed as it didnt feel "suitably convincing" enough to seem realistic, but that " there is still enough detail and earned authenticity to leave one wondering – even though we may not be seeing the actual footage of actual events – if we still might be watching the re-enactment of something very real." 

==References==
 

==External links==
*  
*  

 
 
 