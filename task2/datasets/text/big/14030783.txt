Kishen Kanhaiya
{{Infobox film
| name = Kishen Kanhaiya
| image =Kishen_Kanhaiya.jpg
| image_size =
| caption = DVD cover featuring Madhuri (left) and Anil (right)
| director = Rakesh Roshan
| producer = Rakesh Roshan
| writer = Ravi Kapoor Mohan Kaul Kader Khan
| narrator =
| starring =Anil Kapoor Madhuri Dixit Shilpa Shirodkar Amrish Puri
| music = Rajesh Roshan Indeevar (lyrics)
| cinematography =
| editing =
| distributor =
| released = 9 March 1990
| runtime = 160 min
| country = India
| language = Hindi
| budget = 
| gross = 
| preceded_by =
| followed_by =
| website =
}}
 Indian Bollywood film directed by Rakesh Roshan, released on 9 March 1990. The film stars Anil Kapoor, Madhuri Dixit, Shilpa Shirodkar in lead roles. It is a remake of the 1967 film  Ram aur Shyam starring Dilip Kumar. This movie had a controversial transparent wet saree scene similar to Mandakinis Ram Teri Ganga Maili.

==Plot==
Leela and Bholaram are a childless couple. Leela works as a midwife, and one day assists Sunderdas wife to give birth to twin boys. She decides to keep one baby for herself, and tells Sunderdas that his wife has given birth to one child. There are complications for the mother, and she passes away without seeing her children. Leela and Bholaram bring up Kanhaiya, while Sunderdas attempts to bring up Kishen, but is unable to do a good job. So he marries Kamini, who comes along with her brother, Ghendamal, to live at the estate. She has an illegitimate child named Mahesh from another man, and when Sunderdas comes to know of this, he is threatened and attacked, and as a result of which he is paralyzed, unable to move. Kishen is brought up by Kamini and Ghendamal with lot of abuse, and intimidation, and is kept illiterate, so that he can blindly sign away whatever documents he is asked to sign. Kanhaiya is brought to be a street smart and a movie crazy young man. Thats how he meets an equally movie crazy rich and spoilt girl, Anju and they fall in love. The one secret that ties the brothers together is their reflex action which means that if one is injured the other feels the pain too and only Bhola makes the connection at a vital point in the story. Kishen likes Radha, a servant, and is married, but the atrocities do not stop. Finally Gendamal asks Mahesh to kill Kishen by throwing him off a cliff after getting the property papers. At the same time, Kanhaiya comes to know of his past and returns to the ancestral mansion shocking everyone. What also shocks everyone is his changed attitude. He denies his signatures on the papers signe by Kishen. Finally all the confusions are resolved and both Kishen and Kanhaiya together fight the evil and achieve justice.

==Cast==
* Anil Kapoor ... Kishen / Kanhaiya
* Madhuri Dixit ...  Anju
* Shilpa Shirodkar ...  Radha
* Amrish Puri ...  Lala Gendamal Bindu ...  Kamini vardhan singh
* Dalip Tahil ...  Mahesh
* Ranjeet ...  Sridhar
* Kader Khan ...  Munshi
* Sujit Kumar ...  Bhola Ram
* Shubha Khote ...  Leela
* Shreeram Lagoo ...  Sunder Das
* Saeed Jaffrey ...  Vidya Charan
* Dinesh Hingoo ...  Lokhandwala
* Johnny Lever ...  Lobo
* Vikas Anand ...  Doctor

== Music ==
{{Infobox Album |  
| Name = Kishen Kanhaiya
| Type = Album
| Artist = Rajesh Roshan
| Cover =
| Released =   1990 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  Venus Records and Tapes
| Producer = Rajesh Roshan
| Reviews =
| Last album = Bahar Aane Tak (1990)
| This album = Kishen Kanhaiya (1990)
| Next album = Jurm (1990 film)|Jurm (1990)
}}

The soundtrack of the film contains 6 songs. The music is composed by Rajesh Roshan, with lyrics written by Indeevar and Payam Sayeedi. The album includes songs sung by top singers such as Lata Mangeshkar, Asha Bhosle, Sadhana Sargam, Amit Kumar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)
|-
| "Aap Ko Dekh Ke" Amit Kumar, Sadhana Sargam
|-
| "Krishna Krishna" Nitin Mukesh, Lata Mangeshkar
|-
| "Kuchh Ho Gaya Kya Ho Gaya" Asha Bhosle, Mohammad Aziz
|-
| "Mere Humsafar" Sadhana Sargam
|-
| "Radha Bina Hai" Manhar Udhas, Sadhana Sargam
|-
| "Suit Boot Mein Aaya" Amit Kumar
|}

== External links ==
*  

 

 
 
 
 