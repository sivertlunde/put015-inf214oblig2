Dakota Skye
 
 
{{Infobox film
| name           = Dakota Skye
| image          = Dakota Skye.jpg
| image_size     =
| caption        = Official movie poster
| director       = John Humber
| producer       = John Humber Shaun OBanion
| writer         = Chad Shonk
| narrator       = Ian Nelson   J.B. Ghuman Jr.
| music          = Seth Podowitz
| cinematography = Brett Juskalian
| editing        = Jeff Castelluccio
| distributor    = Desert Skye Entertainment
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $150,000
}}
Dakota Skye is a 2008 coming of age drama directed and produced by John Humber, starring Eileen April Boylan, Ian Nelson and J.B. Ghuman Jr.

==Synopsis==
The coming of age story of a seventeen-year-old girl who has a super power - Dakota Skye has the ability to see through any lie that she is told.  Her world changes when Jonah, her boyfriends best friend from New York, comes into town.  For the first time she meets someone who does not lie to her.  Through this meeting, she is forced to re-examine her own life.

==Reviews==
The film received mixed reviews. Aggregate review website Rotten Tomatoes rated it 62%. 
The film was featured at several film festivals, including San Luis Obispo Film Festival, Phoenix Film Festival, Charlotte Film Festival and Edmonton International Film Festival.

==Accolades==
Writer Chad Shonk won the Copper Wing Award for Best Screenplay at Phoenix Film Festival 2008.  Producer John Humber won the Festival Prize for Best Narrative Feature at Charlotte Film Festival 2008.

==References==
 

==External links==
* 

 
 
 
 
 