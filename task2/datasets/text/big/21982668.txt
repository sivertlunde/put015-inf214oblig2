Black Feathers
{{Infobox film
| name           = Black Feathers
| image          =
| caption        =
| director       = Oreste Biancoli
| producer       =
| writer         = Oreste Biancoli Giuseppe Driussi Giuseppe Berto Paola Ojetti Alberto Albani Barbieri Salvator Gotta
| starring       = Marcello Mastroianni
| music          = Francesco Mander
| cinematography = Fernando Risi
| editing        = Adriana Novelli
| distributor    = Manderfilm
| released       = 21 November 1952
| runtime        = 95 minutes
| country        = Italy
| language       = Italian
| budget         =
}}
Black Feathers ( ) is a 1952 Italian war film directed by Oreste Biancoli.   

==Cast==
* Marcello Mastroianni as Pietro Pieri Cossutti
* Marina Vlady as Gemma Vianello (as Marina Vlady Versois)
* Camillo Pilotto as Zef Cossutti - il nonno
* Vera Carmi as Catina Cossutti
* Guido Celano as Olinto Cossutti
* Enzo Staiola as Antonio Tonino Cossutti
* Hélène Vallier as Natalìa Cossutti
* Liuba Soukhanowa as Giulia Cossutti
* Giuseppe Chirarandini as Don Angelo - il prete

==References==
 

==External links==
* 

 
 
 
 
 
 
 