Places in Cities
{{Infobox film
| name           = Places in Cities
| image          = 
| caption        = 
| director       = Angela Schanelec Michael Weber
| writer         = Angela Schanelec
| starring       = Sophie Aigner
| music          = 
| cinematography = Reinhold Vorschneider
| editing        = Bettina Böhler Angela Schanelec
| distributor    = 
| released       =  
| runtime        = 117 minutes
| country        = Germany
| language       = German
| budget         = 
}}

Places in Cities ( ) is a 1998 German drama film directed by Angela Schanelec. It was screened in the Un Certain Regard section at the 1998 Cannes Film Festival.   

==Cast==
* Sophie Aigner - Mimmi
* Vincent Branchet - Yves
* Katharina Eckerfeld - Mimmis Girlfriend (as Katie Eckerfeld)
* Martin Jackowski - Christoph
* Friederike Kammer - Mimmis Mother
* Tobias Langhoff
* Jérôme Robart - Nicolas
* Marie-Lou Sellem
* Michael Sideris - The Man (as Mischa Sideris)

==References==
 

==External links==
* 

 
 
 
 
 


 
 