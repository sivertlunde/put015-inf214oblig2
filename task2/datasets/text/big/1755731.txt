Maid in Manhattan
{{Infobox film
| name = Maid in Manhattan
| image = Maid_in_manhattan.jpg
| caption = Theatrical release poster
| alt = A woman dressed in a maids uniform is sitting with a city pictured behind her, she is looking up smiling. Above her against the clouds is an washed out blue image of her dressed elegantly and a man in suit smiling from over her shoulder. 
| director = Wayne Wang
| producer = Elaine Goldsmith-Thomas Paul Schiff Deborah Schindler John Hughes 
| screenplay = Kevin Wade 
| starring = Jennifer Lopez Ralph Fiennes Natasha Richardson
| music = Alan Silvestri 
| cinematography = Karl Walter Lindenlaub Craig McKay Hughes Entertainment
| distributor = Columbia Pictures
| released =  
| runtime = 105 minutes
| country = United States
| language = English
| budget = $55 million
| gross = $154.9 million   
}} John Hughes who is credited using a pseudonym. The original music score is composed by Alan Silvestri. The film was released on December 13, 2002.

==Plot==
Marisa Ventura (Jennifer Lopez)  is a single mother trying to get by with her young son Ty (Tyler Posey) by working as a maid for The Beresford Hotel in the heart of Manhattan. When not in school, Ty spends time among Marisa’s fellow hotel workers, who think she is capable of being promoted to management.

While Marisa and fellow maid Stephanie (Marissa Matrone) are cleaning the room of socialite Caroline Lane (Natasha Richardson), Stephanie convinces Marisa to try on a  designer Dolce & Gabbana coat. Lane had previously asked for it to be returned to the store and Stephanie argues that it “technically” doesn’t belong to anyone at the moment. Elsewhere in the hotel, Ty befriends hotel guest and senatorial candidate Christopher Marshall (Ralph Fiennes), whom Ty learns has an interest in Richard Nixon, the subject of his school presentation. Ty wants to go with Chris to walk his dog and the pair go to Caroline Lane’s room to ask Marisa for permission. Chris meets Marisa who is wearing the designer coat, and is instantly smitten with her. He assumes that she is Caroline Lane. The trio spend some time together in the park. Though Marisa and Chris are attracted to each other, Marisa is terrified that management will find out about the ruse and makes it a point to avoid Chris afterwards.

Chris asks the hotel’s head butler Lionel Bloch (Bob Hoskins) to invite “Caroline Lane” to lunch, but he is confused when the real Caroline shows up instead of Marisa. Ironically, Marisa was present when she received the invitation and even offered Caroline some advice on what to wear for their “Lunch  à deux”. When the real Caroline shows up, Chris asks his assistant Jerry Siegal (Stanley Tucci)  to find “the other Caroline Lane” promising that he will attend an important dinner and wishes her to go with him. Jerry asks Lionel to find her. Lionel, who has figured out that Marisa is the woman Chris has been looking for, tells her to go to the dinner and end the affair swiftly if she wants to keep her possible future in hotel management. Stephanie and the hotel staff assist her in preparing for the evening by styling her hair, loaning her an expensive dress, and a spectacular necklace.

Marisa is unable to end the affair, and she spends the night in Chriss hotel room. The next morning, Marisa is spotted by the real Caroline Lane and her friend leaving Chris room. Caroline blurts out the truth to the hotel management and Marisa is fired in front of Chris in Lane’s hotel suite. Both Marisa and Chris spend some time apart with him still thinking about her and Marisa hounded by the press and her disapproving classist mother Veronica.

Some time later, Marisa has obtained another job as a maid at another hotel. Chris is giving a press conference in the same hotel and Ty attends it and asks Chris whether people should be forgiven if they make mistakes, referencing former President of the United States, Richard Nixon. Ty leads him to the staff-room where Marisa is having her break. Chris and Marisa are reunited and the film ends with images of publications showing that Chris has been elected, he and Marisa are still together after one year, Marisa has started her own hospitality business, and Marisa’s maid friends have been promoted to management.

==Production notes== Roosevelt Hotel Morris Heights Grand Concourse and on Jerome Avenue. John Hughes wrote the story, but was credited as Edmond Dantes.

==Cast==
* Jennifer Lopez as Marisa Ava Marie Ventura
* Ralph Fiennes as Christopher Marshall
* Natasha Richardson as Caroline Lane
* Stanley Tucci as Jerry Siegal
* Tyler Posey as Ty Ventura
* Frances Conroy as Paula Burns
* Chris Eigeman as John Bextrum
* Amy Sedaris as Rachel Hoffman
* Marissa Matrone as Stephanie Kehoe
* Priscilla Lopez as Veronica Ventura
* Bob Hoskins as Lionel Bloch
* Lisa Roberts Gillan as Cora
* Maddie Corman as Leezette
* Sharon Wilkins as Clarice
* Jeffrey Dinowitz as Congressman Grey 
* Di Quon as Lily Kim
* Marilyn Torres as Barb
* Lou Ferguson as Keef Townsend

==Reception==
 
Maid in Manhattan generated mostly negative reviews from  , the film grossed a total of $94,011,225 domestically, and $60,895,468 in other countries.  It has grossed $155 million worldwide. 

  
  wrote positively, stating: "is as careful a combination of ingredients as it is possible to package   Everything is at the fairy tale level, which means we never dwell on troubling realities".  Paul Byrnes of the   reviewed Maid In Manhattan positively. Cline wrote: "When we catch ourselves sighing at the end, we get mad that weve fallen for this same old formula all over again. But mad in a nice way." 

Charles Passy of   was also positive: "Talented individuals labour over the contrivances in this lightweight romance, and if the results fluff, at least its painless".    Adams also praised the characters of Marisa and Chris, writing: "Marisa is an appealing heroine, beloved by her son and her co-workers, loyal, practical but optimistic. She dreams of being more but isnt anything as icky as ambitious or confident or focused. Chris, too, dreams of more but isnt craven, like his political advisor.  Like what, exactly, Chris hopes to achieve as a senator or how, exactly, Marisa gets a job after being fired for stealing".  At the website Rotten Tomatoes, the film holds a rating of 39% based on 145 reviews and the sites consensus stating: "Too blandly generic, Maid in Manhattan also suffers from a lack of chemistry between Lopez and Fiennes."    
The film was nominated for a Razzie Award for Worst Actress for Lopez. 

==Soundtrack== Come Away With Me" and "Dont Know Why" sung by Norah Jones,"Fall Again" sung by Glenn Lewis, Paul Simons "Kathys Song" sung by Eva Cassidy and "Im Coming Out" sung by Amerie .

==Spin-off== ABC announced IMDb still put pilot. {{cite news |first=Paula |last=Askanas |title=Lenn Adilman and Sharon Hall Promoted to Executive Vice President, Development at Sony Pictures Television
|url=http://www.sonypictures.com/corp/press_releases/2009/01_09/01072009_Adilman_Hall_EVP_SPT.html |publisher=Sony Pictures |date=January 7, 2009}}  ABC Television has yet to release or air any version of the pilot project.  As of November 25, 2011, IMDB has removed the page and the pilot was rejected.  , ShareTV.org. 

==Telenovela version==
Telemundo and Sony Pictures Television are co-producing a telenovela based on the movie called Una Maid en Manhattan, starring Litzy and Eugenio Siller.  As of November 29, 2011, the telenovela is airing in Telemundo weeknights at 8pm/7pm central.  

== See also ==
 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 