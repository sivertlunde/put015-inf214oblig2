Arlette (film)
{{Infobox film
| name           = Arlette
| image          = 
| caption        = 
| director       = Claude Zidi
| producer       = Claude Berri Pierre Grunstein
| writer         = Claude Zidi Josiane Balasko
| based on       = 
| starring       = Josiane Balasko Christopher Lambert
| music          = William Sheller
| cinematography = Jean-Jacques Tarbès
| editing        = Nicole Saunier
| distributor    = AMLF
| studio         = TF1 Films Production Katharina Renn Productions MDG Productions Les Films Flam
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = $11,7&nbsp;million
| gross          = $5,407,965   
}}
Arlette is a 1997 French comedy-romance film directed by Claude Zidi.

==Plot==
Arlette is a waitress at the "Centipede", a roadside restaurant in the middle of rural France. Loudmouth at the heart of working girl, she dream wedding but Victor, her boyfriend chauffeur heavyweight, does not want to hear about it. One evening Arlette sees land a prince charming in a white limo, Frank, an American millionaire who covers gifts and wants to take her to Vegas to marry. But what are the true intentions of seductive playboy ?

==Cast==
 
* Josiane Balasko as Arlette Bathiat
* Christopher Lambert as Frank Martin
* Ennio Fantastichini as Angelo Mascarpone
* Stéphane Audran as Diane
* Jean-Marie Bigard as Victor
* Armelle as Lucie
* Martin Lamotte as The Chief
* Jean-Pierre Castaldi as Lulu
* France Zobda as Samantha
* Jean-Claude Bouillon as The host
* Ronny Coutteure as Victors friend
* Bouli Lanners as Emile
* Jed Allan as Wide
* Mathieu Demy as Julien
* Arno Chevrier as Mickey
* Tony Librizzi as Riri
* Pascal Benezech as Nanard
* Lionel Robert as Marcel
* Justin Hubbard as David Gafferson
* Isabelle Leprince as Brigitte
* David Fresco as Assler
* Renée Lee as Doris
* Junior Ray as Goliath
* Patrick Bordier as The monk
* Jacques Le Carpentier as The big guy
 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 