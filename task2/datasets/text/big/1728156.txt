The Crime of Father Amaro
 
 
{{Infobox film
| name = The Crime of Father Amaro
| image = ElCrimendelPadre.jpg
| alt =
| caption = Theatrical poster
| director = Carlos Carrera
| producer = Daniel Birman Ripstein Alfredo Ripstein
| writer = Vicente Leñero
| based on =  
| starring = Gael García Bernal Ana Claudia Talancón
| music = Rosino Serrano
| cinematography = Guillermo Granillo
| editing = Óscar Figueroa (film editor)|Óscar Figueroa
| studio = Almeda Films Blu Films Wanda Films
| distributor = Columbia Pictures   Samuel Goldwyn Films (USA) (theatrical)
| released =  
| runtime = 118 minutes
| country = Mexico Spain
| language = Spanish
| budget = $1.8 million
| gross = $26,996,738
}} Portuguese writer José Maria de Eça de Queiroz.
 Roman Catholic groups in Mexico who tried to stop the film from being screened. They failed, and the film became the biggest box office draw ever in the country, beating previous record holder, Sexo, pudor y lágrimas (1999).

In the United States of America, this film also enjoyed commercial success; this films United States distributor paid less than $1 million to acquire the films North American distribution rights,     and the film went on to gross $5.7 million in limited theatrical release in the United States. 

The film starred Gael García Bernal, Ana Claudia Talancón and Sancho Gracia. It premiered on 16 August 2002 in Mexico City.

==Plot==
Newly ordained Padre Amaro arrives in Los Reyes, a small town in the fictional state of Aldama, to start his life serving the church. He is a protégé of a ruthless political bishop, while the local priest, Father Benito, is having a long ongoing affair with a local restaurant owner. Benito is building a large hospital and recuperation center, which is partially funded by a drug lord. Meanwhile, another priest in the area, Father Natalio, is under investigation for supporting left-wing insurgents in his secluded rural church area.
 atheist who is unpopular within the town for his strong opinions.
 baptising the drug lords newborn child, and Rubén is asked to write about the scandals in his hometown. With the aid of mountains of evidence compiled by his father, he publishes a story about Benitos hospital being a front for money laundering. The church has Amaro write a denial and Rubén is then sacked by the newspaper under pressure from the Catholic lobby. Amelia then phones Rubén and dumps him, berating him with a string of obscenities. Rubéns family home is vandalized by devout Catholics and when he returns home, he assaults Amaro when he sees him in the street. Amaro decides not to press charges, so Rubén avoids jail time.

The film delves into the struggle priests have between desire and obedience. Amaro is plagued with guilt about his feelings for Amelia. When the local press begins to reveal the secrets of the parish, Amaro turns to his superior, Padre Benito (Sancho Gracia).
 pregnant with Amaros child, he tries to convince her to leave town to protect him. Later, she decides to try to trick the town by trying to pass off Rubén as the father. She tries to reunite with him and organize a wedding at short notice so that the baby can be attributed to him, but he tells her he is no longer interested. When Benito threatens to report Amaro, Amaro threatens to retaliate over Benitos affair. Eventually, Amaro arranges for a backstreet abortion in the middle of the night. It goes wrong and Amelia begins bleeding uncontrollably. Amaro drives her to the hospital in a large city, but she is already dead before he gets there. The lurid details of the case are suppressed; Benito and a cynical old woman know what has happened. A false story is passed around the town, blaming Rubén for impregnating Amelia before marriage, and praising Amaro for breaking into the abortion clinic and liberating Amelia in a failed attempt to save her and her child. Amaro presides over Amelias funeral; the church is packed with mourners. Benito, now using a wheelchair, leaves in disgust.

==Cast==
*Gael García Bernal – Padre Amaro
*Ana Claudia Talancón – Amelia
*Sancho Gracia – Padre Benito
*Angélica Aragón – Sanjuanera
*Luisa Huertas – Dionisia
*Ernesto Gómez Cruz – Bishop
*Gastón Melo – Martin
*Damián Alcázar – Padre Natalio
*Andrés Montiel – Rubén

==Reception==
The Crime of Father Amaro sparked a controversy in Mexico at the time of its release, with Catholic bishops and organizations asking people not to see it and demanding that the government ban it.  

The film was nominated for over 30 awards, including an Academy Awards nomination and a Golden Globe Award for Best Foreign Language Film in 2003, winning 19. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 