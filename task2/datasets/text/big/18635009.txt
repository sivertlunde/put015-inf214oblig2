Totò al giro d'Italia
 
{{Infobox film
| name           = Totò al giro dItalia
| image          = 
| caption        = 
| director       = Mario Mattoli
| producer       = Lorenzo Pegoraro
| writer         = Vittorio Metz Marcello Marchesi
| starring       = Totò
| music          = Nino Rota
| cinematography = Tino Santoni
| editing        = Giuliana Attenni
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Totò al giro dItalia is a 1948 Italian comedy film directed by Mario Mattoli and starring Totò.    

The film features cameos of famous cyclists of that time: Fausto Coppi, Gino Bartali, Fiorenzo Magni, Ferdi Kubler, Giordano Cottur, Gianni Ortelli, Oreste Conte, Adolfo Consolini, Louison Bobet, Briek Schotte, Amos Matteucci, Jean-Pierre Wimille, Ulisse Lorenzetti, Di Segni, Amadeo Deiana, Aldo Spoldi, Giuseppe Tosi, Camillo Achilli and Tazio Nuvolari.

==Plot==
Italy is about to take the famous cycling race of 1948, the event is seen by everyone, even by   or Fausto Coppi, but in the end he had to give his soul and life after the demon. Totò initially includes only the first part of the contract or to win all the races, but then the demon appears to him a second time to remind him of the price you pay. Totò is desperate and does everything possible to not win the last race, but the legs on the pedals are as haunted and Totò can not stop. Approaching the last race and Totò is on the verge of collapsing morally, but his friends and the lovely Dorina, which Totò was madly in love, you invent a trick so clever as to deceive even the Devil.

==Cast==
* Totò as Prof. Toto Casamandrei
* Isa Barzizza as Doriana
* Giuditta Rissone as Signora Casamandrei
* Walter Chiari as Bruno
* Carlo Ninchi as Dante Alighieri
* Luigi Catoni as Nerone
* Mario Castellani as Renato Stella, allenatore
* Carlo Micheluzzi as Il Diavolo
* Fulvia Franco as Miss Italia
* Alda Mangini as Gervasia
* Ughetto Bertucci as Armando

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 