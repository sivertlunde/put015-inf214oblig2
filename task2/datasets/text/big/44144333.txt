Idavelakku Sesham
{{Infobox film
| name           = Idavelakku Sesham
| image          =
| caption        =
| director       = Joshiy
| producer       = Thiruppathi Chettiyar
| writer         =
| screenplay     = Madhu Srividya
| music          = Raveendran
| cinematography =
| editing        =
| studio         = Evershine Productions
| distributor    = Evershine Productions
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Madhu and Srividya in lead roles. The film had musical score by Raveendran.   

==Cast==
*Mammootty
*Sumalatha Madhu
*Srividya
*MG Soman

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aadyarathi neelimayil || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Kaamuka nee varoo || Vani Jairam, KP Brahmanandan || Poovachal Khader || 
|-
| 3 || Maanam pon manam || K. J. Yesudas || Poovachal Khader || 
|-
| 4 || Then kiniyunna praayam || Vani Jairam || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 