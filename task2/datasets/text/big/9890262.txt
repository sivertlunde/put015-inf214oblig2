The Homesteader
 
{{Infobox film
| name        = The Homesteader
| image       = The Homesteader 1919 newspaperad.jpg
| caption     = Newspaper advertisement. 
| writer      = Oscar Micheaux William George
| director    = Oscar Micheaux
| co-director = Jerry Mills
| producer    = Oscar Micheaux
| distributor = Micheaux Book & Film Company
| country     = United States
| language    = Silent film   English subtitles
| released    =  
}} lost black-and-white silent film by African American author and filmmaker Oscar Micheaux.

==Plot==
The Homesteader involves six principal characters, the leading one being Jean Baptiste (Charles Lucas), a homesteader far off in the Dakotas, living where he alone is black. To this wilderness arrives Jack Stewart, a Scotsman, with his motherless daughter, Agnes (Iris Hall). In Agnes, Baptiste meets the girl of his dreams. Agnes, however, does not know that she is not white. Peculiar fate threw her in the company of the Homesteader, but their love is forbidden by the custom of the country. Baptiste eventually sacrifices the love of this girl of his dreams, goes back to his own people and marries the daughter of a preacher.

McCarthy, the embodiment of vanity, deceit and hypocrisy, really admires the marriage his daughter has made. He speaks of the "rich" young man she has married, praises him to the highest. Baptiste does not know, however, that McCarthy requires and is in the habit of having people praise him. Baptiste does not do it because he is not of the temperament to do so. Because of this failure grows the tragedy of mismarriage to Orlean (Evelyn Preer), a sweet girl, kind and good, but like her mother, without the strength of her convictions.

Baptiste, Orlean having failed him, is persecuted by McCarthy and by Ethel (McCarthys other daughter), who, like her father, possesses all the evil a woman is capable of; she is married to weak-kneed Glavis. In the end, Orlean, driven insane by the evil she had been the innocent cause of, rights a wrong which causes Baptiste to go back to his land in the Dakotas, where he finds the girl he first discovered. Later, he learns the truth about her race and the story has a beautiful ending. 

==Cast==
*Charles Lucas: Jean Baptiste
*Evelyn Preer: Orleans
*Iris Hall: Agnes
*Charles S. Moore: Jack Stewart
*Inez Smith: Ethel
*Vernon S. Duncan: McCarthy
*Trevy Woods: Glavis (Ethels husband)
*William George: Agnes white lover

==Production==
The film was produced, co-directed and written for the screen by Micheaux, based on his book of the same name. It is believed to be the first feature-length film made with a black cast and crew, for a black audience, and thus the first example of a race movie. Most of the filming, if not all, took place in Winner, South Dakota. Micheaux, using his considerable skills as a businessman and salesman, sold stock in his corporation to the white farmers around Sioux City, Iowa at prices ranging from $75 to $100 per share.  

Eventually enough capital was secured to produce the eight-reel film starring Charles Lucas as the male lead, and Evelyn Preer and Iris Hall, two well-known dramatic actresses who at the time were associated with the Lafayette Players Stock Company.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 