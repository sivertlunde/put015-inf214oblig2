Crna Zorica
Crna Zorica (Loveless Zoritsa) is a 2012 Serbian dark comedy co-directed by Hristina Hatziharalabous and Radoslav Pavković. It was premiered in Serbia on 20 March 2012.  The film has been shown on the Raindance Festival in London,  the Film Festival in Cottbus   and the Thessaloniki International Film Festival.  The film has been also shown during the Days of Serbian Culture in Croatia. 

==Plot==
Living in modern-day Eastern Serbia, still fertile ground for various forms of superstition and prejudice, Zorica (Ljuma Penov) is a village girl who carries a curse. Ever since her first boyfriend mysteriously drowned in the river, men around her are dying in most bizarre ways. The plot thickens when stubborn policeman Mane (Branislav Trifunović) who doesnt believe in village tales tries to investigate whether Zorica is a serial killer or a girl looking for love.

==References==
 

==External links==
* 

 
 
 