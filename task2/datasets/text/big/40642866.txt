Morituris
{{Infobox film
| name           = Morituris
| image          = Morituris_Poster.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Raffaele Picchio
| producer       = Vincenzo Manzo Gianluigi Perrone Raffaele Picchio Pierpaolo Santagostino
| screenplay     = Gianluigi Perrone
| story          = Tiziano Martella Raffaele Picchio
| based on       = 
| narrator       = 
| starring       = 
| music          = Riccardo Fassone
| cinematography = Daniele Poli
| editing        = Daniele Martinis
| studio         = Fingerchop Movie Production
| released       =  
| runtime        = 83 minutes
| country        = Italy
| language       = Italian Romanian
| budget         = 
| gross          = 
}} Italian horror film directed by Raffaele Picchio. The film had its world premiere on July 30, 2011, at Fantasia Festival.  The film is inspired by the Circeo Massacre, in which three young men abducted, and then raped and tortured, two young women over a two day period. As a result of its graphic content, the film has been banned in Italy.  In May 2013 Synapse Films announced that they had purchased the rights to Mortituris with the intent to release it to DVD. 

==Plot==
 
While out driving, three young men meet two beautiful women and convince them to go with them to a rave out in a remote location. Once there, the women realize that there is no rave and that the men lied to them in order to beat and rape them. The women try to escape, only to accidentally unleash a pack of zombie gladiators that proceed to attack the group as a whole.

==Cast==
*Valentina DAndrea		
*Andrea De Bruyn
*Désirée Giorgetti
*Francesco Malcom
*Giuseppe Nitti		
*Simone Ripanti

==Reception==
Critical reception for Morituris has been mixed, with some decrying what they saw as extreme misogyny and gratuitous scenes of rape.  Twitch Film gave a mostly positive review, commenting that they found the extended rape scene almost unbearable but that overall that there is "a thin line between misanthropic fun and a hardcore exploitation film. "Morituris" uses scissors in order to cut this line and messes with audiences expectations."  Aint It Cool News praised the movie overall, commenting that while some viewers might grow impatient that the zombie gladiators do not appear until much later in the film, that when they do appear it is a "face splattering, spear gouging, cat-o-nine-tails whipping, sword slashing, crucifyingly good time." 

Bloody Disgusting staff reviewer "Mr Disgusting" was highly critical of the movie, giving it "Negative 100 million out of 5 Skulls" and stating "I’m not a sensitive guy (at all), I don’t get offended, and frankly I wasn’t offended – but Morituris does show viewers what a disgusting piece of trash the filmmaker is. Every single thing that happens comes with such a complete lack in taste. So to the director and the movie: f*ck off."    This prompted Picchio and screenwriter/producer Gianluigi Perrone to respond to the reviewer to address issues brought up in the review.  Perrone explained that they used the films violence against women as a way of showing "one of the most vile crimes against women" as a "way of showing the evil of mankind and it’s decadence" and that the end titles credit of "in memory of mankind" was indicative of this.    They also used some scenes, such as a cutaway to a man in another location inserting a mouse into a womans vagina, to show that "some crimes happens not only in the hidden of a forest but also in the core of a metropolis like Rome". 

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 