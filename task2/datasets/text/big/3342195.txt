Werckmeister Harmonies
 

{{Infobox film
| name           = Werckmeister Harmonies
| image          = Werckmeister Harmonies.jpg
| caption        = Theatrical release poster
| director       = Béla Tarr
| producer       = Béla Tarr
| screenplay     = László Krasznahorkai Béla Tarr
| based on       =  
| starring       = Lars Rudolph Peter Fitz Hanna Schygulla
| music          = Mihály Vig
| cinematography = Patrick de Ranter
| editing        = Ágnes Hranitzky
| distributor    = 
| released       =  
| runtime        = 145 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}
Werckmeister Harmonies ( ;  ) is a 2000 Hungarian film directed by Béla Tarr, based on the 1989 novel The Melancholy of Resistance by László Krasznahorkai. Shot in black-and-white and composed of thirty-nine languidly paced shots, the film is about János and his uncle György during the Soviet occupation of Hungary at the end of the Second World War. It also shows their journey among helpless citizens as a dark circus comes to town casting an eclipse over their lives.

The title refers to the baroque musical theorist Andreas Werckmeister. György Eszter, a major character in the film, gives a monologue propounding a theory that Werckmeisters harmonic principles are responsible for aesthetic and philosophical problems in all music since, which need to be undone by a new theory of tuning and harmony.

==Plot==
The film can be seen as an allegory of Aftermath of World War II|post-World War II Eastern European political systems - told as a black-and-white cinematic poem with 39 long, single-camera takes. It examines the brutalization of a society, its political systems and ethics through the metaphor of a decaying circus whale and its star performer. It is set in a desolate, isolated small town in Hungary during Soviet times.

The film starts with János Valuska, a simple person, conducting a poem and dance with drunken bar patrons. The dance is of the total eclipse of the sun, which disturbs than silences the animals, but it finishes with the grand return of the warm sunlight.

One of the significant reoccurring characters in this plot is the subtle transitions between light and darkness. The first appearance of this motif is a lamp in the bar then to a door into the darkness. The circus arrives like a Trojan Horse  in the darkness of night. A window is opened as a glimmer of hope and dressed to shut out the world outside.
The antagonist is only ever seen as a shadow.

János uncle György is a composer, and therefore one of the intelligentsia. György observes the imperfection and compromise of the musical scale (as defined by Andreas Werckmeister a historical theorist). György proposes changes to the scale to make it more harmonious. György’s utopian approach to music represents a flawed naive idealism that never can be achieved; it is not developed any further in the film or the book. It is just a statement indicating that human governance will always be flawed.

Györgys estranged wife tries to leverage her political and social status by giving György a list of names to sign up for the "clean up the town movement", this with the blessing of the police chief.

However, a stuffed smelly circus whale and its star performer, Prince, comes to town in the silence and darkness of night. This is a metaphor for a bloated political system and the unseen Prince who represents the power of politically inspired, emotive dogma.

János philosophizes about God and the beast.

The post office workers are unsettled by the ominous signs of the circuses arrival and are disturbed by the cloud that settles over each town it visits, a reference to the spread of the externally imposed centralised monolithic government system onto all the Soviet buffer nations just after the war.

Györgys struggling cobbler brother gets the list and passes it on to the agitated throbbing masses in the town square who are unhappy at public services failing. Györgys former wife sleeps with the drunk gun toting police chief.

The presence of the whale and the Prince stir up the masses. János overhears the circus master losing control of his faceless Prince, who is becoming drunk with his own voice of revolutionary dogma. The circus master disowns him. The Prince, now free, inflames the masses, and the people riot.

The film is shot as a visual poem, and has been described as both beautiful and haunting. Lengthy single-shot scenes with a hypnotic and rhythmic pace build up to the peaceful observation of the thuggish destruction of the hospital and its patients. The rioters are brutal. Their inhumanity almost seems normal and natural. When the rioters finally come to beat a helpless old naked patient, they see their impotent, sad and powerless selves. The patients they are destroying in the hospital are themselves.

After the riot, János comes across the diary of a rioter. It explains that the rioters did not know what they were angry with; so they were angry at everything. Then it recounts the mobs horrendous rape of two working class post office girls.

János comes across his killed cobbler uncle who got naively involved in the riot. János is told, by his cobbler uncles wife, to leave town for his own safety.

He is intercepted by a helicopter. He finds himself committed to a mental institution with caged beds (a tool of the time for dealing with political dissidents). János appears drugged and broken.

György, his composer uncle, is evicted from their society house but gets to live in a shed in the garden whilst Györgys former wife, with her new status as a collaborator, now occupies the big house with the police chief. The intelligentsia is displaced by political opportunism.

György tells a vacant János, in the ward, that if he is released from the mental institution they can live contentedly together in the shed with his piano. János just stares.

It finishes with György looking directly into the eye of the whale, then, walking away and looking back at the now sad and disheveled whale, destroyed by the rioters the night before, its rotting carcass slowly enveloped by the fog which gets whiter and brighter. Warm bright sunlight returns.

== Cast ==
* Lars Rudolph as János Valuska
* Peter Fitz as György Eszter
* Hanna Schygulla as Tünde Eszter
* János Derzs: Man In The Broad-Cloth Coat
* Đoko Rosić: Man In Western Boots
* Tamás Wichmann (actor)|Tamás Wichmann: Man In The Sailor-Cap
* Ferenc Kállai: Director

==Reception== weighted average score of 92/100 (based on eight reviews), which translates to "universal acclaim".    Based on 30 reviews, Rotten Tomatoes reports a 97% approval rating, with an average score of 8.3/10.   

Lawrence van Gelder of The New York Times called the film "elusive" and argued that it "beckons filmgoers who complain about the vapidity of Hollywood movie making and yearn for a film to ponder and debate."  David Sterritt, writing for the Christian Science Monitor, awarded it a full four stars, remarking, "Tarr wants to stir the imagination and awaken the conscience of his audience rather than divert us with easy entertainment." 

In The Guardian, Richard Williams compared Tarr to the greatest directors and perceived Werckmeister Harmonies as "a bleak vision of chaos and capitalism."  Film critic Roger Ebert described the movie as "unique and original", writing that it "feels as much like cinema verite as the works of Frederick Wiseman." He went on to add the film to his "Great Movies" collection in 2007. Roger Eberts "Great Movies" essay about   

Some critics rank the movie with the best of all time. In the British Film Institutes decennial Sight & Sound poll, 10 critics and five directors voted Werckmeister Harmonies one of the 10 greatest works of cinema ever made — placing it 171st in the critics poll and 132nd in the directors poll.  According to They Shoot Pictures, Dont They, a website which statistically calculates the most well-received movies, it is the fifth-most well received movie of the 21st century, and the 268th-most acclaimed film of all time. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 