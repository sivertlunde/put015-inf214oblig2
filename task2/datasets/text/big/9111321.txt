Naayi Neralu
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Naayi Neralu
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Girish Kasaravalli
| producer       = Abhishek Patil Basanth Kumar Patil
| writer         = 
| screenplay     = Girish Kasaravalli
| story          = S. L. Bhyrappa
| based on       =  
| narrator       = 
| starring       = Pavitra Lokesh  Rameshwari Varma   Sringeri Ramanna   Ashwin Bolar   Ananya Kasaravalli
| music          = Isaac Thomas Kottukapally
| cinematography = S. Ramachandra
| editing        = S. Manohar
| studio         = 
| distributor    = Basanth Productions
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}

Naayi Neralu ( ;  ) is a 2006 Indian Kannada language film directed by Girish Kasaravalli, based on a novel of the same name by Kannada writer S. L. Bhyrappa, and starring Pavitra Lokesh in the lead role. 

The film won three Karnataka State Film Awards.  The film is mostly shot in Coastal of Karnataka near Udupi Nadibettu House at Shirva, Billampadavu Shooting House, house of one Rajeshwar Shashtri of Chakrakodi near Moodambail and island near Kallianpur and Kundapura.

==Plot summary==
Acchanniah lives with his wife Nagalakshmi and his widowed daughter-in-law Venkatalakshmi in a remote village in Karnataka.   His granddaughter Rajalakshmi is in a distant city completing her studies. Acchanniah learns from a friend that a young man in a distant village claims that he was the son of Acchanniah in his previous birth. He dismisses the information as baseless. But his ailing wife believes or rather chooses to believe that her son who died twenty years ago has come back.

Acchanniah sets out to meet the young man who is about twenty years old and provides some information regarding his previous birth which tallies with or appears to tally with his previous birth. Acchanniah brings home this man whose name is Vishwa. Acchannaihs wife realises a new purpose in living and accepts this stranger as her son. But Venkatalakshmi, Acchanniahs daughter-in-law, finds it difficult to accept some stranger as her long-lost husband. After some initial resistance, Venkatalakshmi realises that this is an opportunity to attain all that she is restrained from. Her desires emerge again and she accepts Vishwa as her husband. It is here that problems start.

The society which forced her to believe it is her husband does not approve of Venkatalakshmi accepting the man as her husband and living with him. Rajalakshmi swears that this stranger who is her age is not her father. She tries to convince her mother to come out of such a delusion but to no avail. Acchanniah and Nagalakshmi are shocked to hear that Venkatalakshmi is pregnant with Vishwas child. Matter complicate and Acchanniah is humiliated in the public by his fellow Brahmins. Venkatalakshmi sensing the intensity of the situation leaves the village and lives in a god-forsaken place with Vishwa. She has a hard life trying to manage ends meet. Vishwa is an eccentric young man and keeping him in control is not easy. To worsen things, Vishwa is attracted to Sukri, a young woman from the worker class. Nagalakshmi dies unable to digest these bizarre happenings. Rajalakshmi decides to seek the help of the court to get her mother back. They file a false complaint on Vishwa. Aware of the familys sinister motives, Vishwa refuses to return to Venkatalakshmi.

Meanwhile, a daughter is born to Venkatalakshmi. The court announces Vishwa guilty and he is sent to two years rigorous imprisonment. But Venkatalakshmi declares that she will wait for him to be released although she is certain that he will not return to her. She tells her daughter that she never believed that Vishwa was her husband reincarnate.

==Principal cast==
* Pavitra Lokesh
* Rameshwari Varma
* Sringeri Ramanna
* Ananya Kasaravalli
* Ashwin Bolar
* A. R. Chandrasekhara
* Ramesh

==The inner theme==
Naayi Neralu has a very complex theme of great intensity. Earlier films of Girish Kasaravalli focused on social themes but were very straight in their narrative. Naayi Neralu leaves enough for the viewer to interpret and analyze. On the outside the film deals with three members of a family dealing with a very bizarre situation. But the inner theme is much more complex and can be an analyzed in many dimensions. It is a culmination of many aspects of life: human relationships, man and society, multiple perspectives on a situation, and so on. Each of the protagonists look at the situation the way they want to. Although the title suggests rebirth, the film deals with how people react to such a situation.

==Highlights==
*The intensity of the plot unfolds very well in the film. In the beginning, it is just a rumour. But in the end, it is a devastating truth. The transition from the former to the latter evolves successfully. 
*The character of Venkatalakshmi emerges as a very strong individual towards the end. She tells her father-in-law that she has lived her life doing things which others say and that she has decided to live the way she wants to. In the end, we realize what a strong character she has been.
*Girish Kasaravallis film needs no introduction regarding its climax. As in all his films, the climax is one of the major highlights of Naayi Neralu. The final scene shows Venkatalakshmi standing on the riverbanks with her child in hand looking at her daughter and father-in-law leave in a boat. This is the directors way of telling the audience that Venkatalakshmi has evolved into a strong individual.

==Actors and performances==
The role of Venkatalakshmi was played by Pavitra Lokesh, a well-established actress in Kannada cinema. She proved her worth as a very good actress in the film. Rameshwari Varma who played the role of Nagalakshmi is a theatre actress and this was her second film. Ananya Kasaravalli gives a memorable performance as Rajalakshmi. Sringeri Ramanna as Acchanniah excels as a sensitive father-in-law caught between the affection for his daughter-in-law and the laws of the society. Naayi Neralu was Ashwin Bolars debut feature and his first venture into the Indian film industry.

==Awards and screenings==
* Karnataka State Film Awards 2005-06 First Best Film Best Director - Girish Kasaravalli Best Actress - Pavitra Lokesh

Osians Asian Film Festival, CINEFAN, 2006:
*Jury award in the Indian Film Category.
Karachi International Film Festival, 2006
*Best Feature Film.
International Film Festival Of Mumbai, MAMI Awards, 2007
*Best film.
*Best Direction award for Girish Kasaravalli.
Screenings
*Indian International Film Festival, 2006.
*Third Eye Film Festival, 2006.
*Palm Springs Film Festival,2006.
*International Film Festival Of Rotterdam in the Masteros category, 2007.
*Singapore International Film Festival, 2007.
*Bangkok Film Festival, 2007.

==Deviations from the book==
The director has taken the liberty to widely deviate from the original story. The original is an enigmatic tale of Karma and its repayment in subsequent births. In the film, there is huge chunk removed from what the book says that renders the story a single-themed narrative.

For instance, the book touches upon how Vishwa was born and why he had to be reborn upon his accidental death. Acchannaiahs role in what Vishwa goes through in his births is significant in the book. The narrative of the film fails to explain why it had to be called Naayi Neralu.

==References==
 

 

 