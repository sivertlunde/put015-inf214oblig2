Chaitra (film)
{{Infobox Film
| name           = Chaitra: In The Month Of March
| image          = 
| image_size     = 
| caption        = 
| director       = Kranti Kanade
| producer       = Film and Television Institute of India
| writer         = Kranti Kanade
| based on       =   
| starring       = Sonali Kulkarni  Lalan Sarang Abhishek Bhave
| music          = Bhaskar Chandavarkar
| cinematography = Mahesh Mutthuswami
| editing        = Kranti Kanade
| sound rerecording mixer     = Subir Kumar Das
| studio         = 
| released       =  (at Mumbai International Film Festival)
| runtime        = 25 min
| country        = India
| language       = Marathi 
| budget         = 
| gross          = 
}} National Film Awards in 2002.

==Plot==
The story of a proud woman, her poetic vindication of unfair social insult, and her inevitable sad destiny. A very culture-specific film dipped in a local festival of rural women.

==Cast==
* Sonali Kulkarni as Mother
* Abhishek Bhave as Madhu
* Lalan Sarang as Naik Kaku
* Vandana Vakhnis as Naik baai
* Dhiresh Joshi as Father
* Bhakti Pathare as Rakhma
* Uttara Kulkarni as Shweta
* Shripad Amte as Mama

== Production ==
The film was shot in Alandi village near Pune and the post-production done at Film and Television Institute of India|FTII. Being a student film, director did not have enough budget but Sonali Kulkarni and Lalan Sarang agreed to do the film without remuneration. Music director Bhaskar Chandavarkar not only composed music for the film without taking any remuneration but he also paid for the musicians himself. Cinematographer Mahesh Muthuswami contributed an amount towards the production and so did the sound designer Subir Das. 

== Awards and recognition== National Film Awards in 2002. 
* National Film Award for Best Short Fiction Film
* National Film Award for Best Non-Feature Film Music Direction (Bhaskar Chandavarkar)
* National Film Award - Special Jury Award / Special Mention (Non-Feature Film) (Sonali Kulkarni)
At the Mumbai International Film Festival 2002 it won National Critics’ Award and Best Short - Silver. That year, it was Indias official entry to the Student Academy Awards.

The film was also included in a DVD titled "Master Strokes", a compilation of works of twenty renowned FTII students. 

==References==
 

 
 
 
 