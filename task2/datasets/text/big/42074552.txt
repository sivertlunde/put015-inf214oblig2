An Uneventful Story
{{Infobox film
| name = An Uneventful Story
| image = File:An_Uneventful_Story_poster.jpg
| caption = 
| director = Wojciech Jerzy Has	
| producer = 
| writer = Wojciech Jerzy Has
| based on =  
| starring = Gustaw Holoubek Hanna Mikuc Anna Milewska
| music = Jerzy Maksymiuk
| cinematography = Grzegorz Kedzierski
| editing = Barbara Lewandowska-Conio 
| studio = 
| distributor =  Zespol Filmowy "Rondo"
| released =  
| runtime = 106 minutes 
| country = Poland
| language = Polish
| budget = 
}}
An Uneventful Story ( ) is a 1986 Polish film directed by Wojciech Jerzy Has, starring Gustaw Holoubek. The film is an adaptation of a short story by Anton Chekhov and tells the story of a professor of medicine who begins an affair with a young pupil.

==Plot==
Michal (Gustaw Holoubek) is a middle aged professor of medicine in a provincial town who is bored by the mundane and superficial nature of his life, friends and family. Katarzyna (Hanna Mikuc), a young woman returns to the town and the two have an affair. 

==Cast==
*  Marek Bargielowski as Michal
*  Wladyslaw Dewoyno as Mikolaj
*  Ewa Frackiewicz 
*  Janusz Gajos as Aleksander Gnekker
*  Jacek Glowacki 		
*  Gustaw Holoubek as Professor
*  Jan Konieczny	
*  Janusz Michalowski as Piotr
*  Hanna Mikuc as Katarzyna
*  Anna Milewska as Weronika
*  Wlodzimierz Musial 		
*  Jerzy Zygmunt Nowak as Waiter
*  Elwira Romanczuk as Liza
*  Leszek Zentara as Student 

==Production== The Saragossa Manuscript, when it was rejected he made The Codes (Szyfry) instead.  The film was the directors first after a hiatus of 10 years, after Has had a number of projects blocked by the communist authorities because he had taken The Hour-Glass Sanatorium to the Cannes Film Festival against their wishes.  It was filmed in the city of Tarnów in the Polish province of Lesser Poland Voivodeship|Małopolskie. 

==Release==
The film was released on 12 September 1983.

==See also==
* Cinema of Poland
* List of Polish language films

==References==
 

==External links==
*  

 

 
 
 
 
 