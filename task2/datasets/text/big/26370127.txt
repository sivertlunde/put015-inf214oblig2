The Wall (1967 film)
{{Infobox film
| name           = The Wall
| image          = 
| caption        = 
| director       = Serge Roullet
| producer       = Claude Jaeger
| writer         = Serge Roullet Jean-Paul Sartre
| starring       = Michel del Castillo
| music          = 
| cinematography = Denys Clerval
| editing        = Denise Baby
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = France
| language       = French
| budget         = 
}}
 1967 cinema French drama film directed by Serge Roullet. It was entered into the 17th Berlin International Film Festival.   

==Cast==
* Michel del Castillo - Pablo
* Denis Mahaffey - Tom
* Matthieu Klossowski - Juan
* Anna Pacheco - Concha
* René Darmon - Ramon
* Bernard Anglade - Le médecin
* Jorge Lavelli - Un officier
* Claude Esteban - Le boulanger
* Edgardo Cantón - Deuxième officier
* Peter Kassovitz - Un officier
* Miguel Cañadas - Premier officier
* Jaime Mir Ferri - Interrogateur
* José Abad - Interrogateur
* Félix Esteguy - Interrogateur John Montague - Le prisonnier anglais
* Oscar Lozoya - Un garde Ricardo Gómez - Le garde moustachu
* Mario Casari - Un garde

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 