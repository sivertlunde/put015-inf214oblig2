A Daughter of Australia (1922 film)
 
 
{{Infobox film
| name           = A Daughter of Australia
| image          = 
| image_size     =
| caption        = 
| director       = Lawson Harris
| producer       = Lawson Harris Yvonne Pavis
| writer         =  Dulcie Deamer Albert Goldie
| narrator       =
| starring       = Lawson Harris 
| music          =
| cinematography = Arthur Higgins
| editing        = 
| studio = Austral Super Films
| distributor    = Austral Super Films
| released       = 2 September 1922
| runtime        = 7,000 feet 
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

A Daughter of Australia is a 1922 Australian silent film directed by Lawson Harris. It concerns a rich squatter, Arthur Fullerton (Charles Beethan), and his daughter, Barbara (Yvonne Paris). 

It is considered a lost film.

==Plot==
A young English aristocrat, Hugh Ranleigh, falls for Barbara Fullerton, but is falsely accused of murdering a gambler in a  night club and escapes to Australia. He finds work on a cattle station and falls for the squatters daughter – who turns out to be Barbara. Eventually he proves his innocence. 

==Cast==
*Lawson Harris as Hugh Ranleigh
*Yvonne Pavis as Barbara Fullerton
*Charles Beetham as Mr Fullerton Charles Villiers
*Dorothy Hawtree Gilbert Emery as Jimmy
*J.P. ONeill as Irishman
*Lilian Tate

==Production==
The film was shot for a low budget on location in Sydney, with scenes at Ascot racecourse and the Royal Easter Agricultural Show. Contrary to the experiences of many local filmmakers, authorities were keen to give the producers permission to film in public places. A shoot out was filmed in Martin Place during Sydneys rush hour which caused a commotion, and leading to two extras being injured. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 112. 

Marie Lorraine made her film debut in the cast. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 
 


 