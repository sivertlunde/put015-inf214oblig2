The Poor & Hungry
{{Infobox film
| name           = The Poor & Hungry
| image          = ThePoorHungry.jpg
| alt            = 
| caption        = 
| director       = Craig Brewer
| producer       = Craig Brewer Jodi Brewer Walter Brewer Erin Hagee
| writer         = Craig Brewer
| narrator       = Eric Tate
| starring       = {{Plainlist|
* Eric Tate
* Lake Latimer
* Lindsey Roberts
}}
| music          = 
| cinematography = Craig Brewer
| editing        = Craig Brewer Morgan Jon Fox  
| distributor    = BR2 Productions
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = $20,000
}}
 American Independent independent drama drama written and directed by Craig Brewer. It stars Eric Tate, Lake Latimer, Lindsay Roberts, John Still, T.C. Sharpe and Wanda Wilson.

The film, shot in Memphis, Tennessee, launched Brewers career as a writer and director. Brewer used the experience of making the film as inspiration for the story in the Academy Award-winning Hustle & Flow. 

== Plot ==
Car mechanic by day/thief by night, Eli Foote is starting to feel guilty about what he does. His guilt intensifies when he steals a car that belongs to a beautiful cellist named Amanda. Meeting her at the impound yard where she is distraught over her car, Eli strikes up a conversation with Amanda which leads to a romance. Meanwhile, Elis friend, Harper, a grungy street hustler, is trying to do business with Elis boss, Mr. Coles. She needs his help in getting a car for a boisterous pimp named Cowboy Urles. Eli wants to leave his criminal past behind, but Harper holds him to their friendship.

== Cast ==
* Eric Tate as Eli Foote
* Lindsay Roberts as Harper
* Lake Latimer as Amanda Russell
* John Still as Mr. Coles
* Keenon Nikita as Archie
* Dennis Phillippi as Bobo
* T.C. Sharpe as Cowboy Urles
* Jay Munn as Preacher
* Wanda Wilson as Miss Wanda
* Karl Chambless as Beale Street Tourist
* J. Lazarus Hawk as Dealer

== Production ==
Craig Brewer was inspired to write the film after his assistant Erin Hagees car was stolen. Seeing her crying at the impound lot got him thinking, "What if the guy who stole her car was right here, watching her cry?" Brewer had been working as the manager of a local Barnes & Noble and began writing the screenplay for the film at the P&H Café. When he asked what the P&H stood for, he was told it stood for many things but it was generally accepted that it was called "The Poor & Hungry Café". Brewer asked if he could use the title for the film.

As he was writing the script, Brewer planned to max-out credit cards and shoot the film on celluloid. His father, Walter, suggested it would be cheaper to film on a Hi8 video camera. Weeks later, Brewers father died from a heart attack at the age of 49. Brewer used his inheritance of $20,000 to begin working on The Poor & Hungry.

== Music == Jack Oblivian, Blind Mississippi Morris, Al Kapone and Jason Freeman.

== Critical reception ==
The film received positive reviews. According to Film Threat, The Poor & Hungry is "a ruggedly moving digital feature; an unlikely love story with a gritty, vaguely melancholic air". 

==Awards and nominations==
;Hollywood Film Festival
* Best Digital Feature: Craig Brewer (Winner)

;Magnolia Independent Film Festival
* Best Feature Film: Craig Brewer (Winner)

;Nashville Film Festival
* Audience Choice Award: Craig Brewer (Winner)
* Dreammaker Award: Craig Brewer (Winner)
* Special Award of Merit: Craig Brewer (Winner)

== Home media ==
In its initial release, The Poor & Hungry was only available on VHS in Memphis at a local video store, Black Lodge Video. However, all the copies were eventually stolen from the store. The television rights were sold to the Independent Film Channel. 13 years later, Brewer gave the film a Blu-ray/DVD release after pent-up demand on November 15, 2013 and announced that anyone who wanted a digital copy of the film could get one for free via email.

The Blu-ray, up-resed to high definition frame-by-frame, includes a commentary by Brewer, deleted scenes and a newly produced documentary Poor Mans Process: The Making of The Poor & Hungry, directed by Morgan Jon Fox and featuring interviews with Brewer, the cast and crew, John Singleton and Stephanie Allain.

== References ==
 

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 