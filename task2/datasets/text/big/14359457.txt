Jacob the Liar (1975 film)
{{Infobox film
| name           = Jacob the Liar
| image          = Jacobliarposter.jpg
| caption        = Theatrical release poster
| director       = Frank Beyer
| producer       = Herbert Ehler
| writer         = Jurek Becker Frank Beyer
| starring       = Vlastimil Brodský
| music          = Joachim Werzlau
| cinematography = Günter Marczinkowsky
| editing        = Rita Hiller DFF Barrandov Studios
| distributor    = PROGRESS Film-Verleih
| released       =  
| runtime        = 100 minutes
| country        = German Democratic Republic Czechoslovakia
| language       = German
| budget         = 2,411,600 East German Mark
}} Holocaust film novel of the same name by Jurek Becker. It starred Vlastimil Brodský in the title role.

Work on the picture began in 1965, but production was halted in summer 1966. Becker, who had originally planned Jacob the Liar as a screenplay, decided to make it a novel instead. In 1972, after the book garnered considerable success, work on the picture resumed.

==Plot== Jewish ghetto in German-occupied Poland, a man named Jakob is summoned to the Gestapo office on a charge he broke the curfew. As the soldier who sent him there merely played a prank on him, he is released, but not before hearing a radio broadcast about the defeats of the German Army. As no one believes he went to the Gestapo and came out alive, Jakob makes up another tale, claiming he owns a radio – a crime punishable by death. He then starts encouraging his friends with false reports about the advance of the Red Army toward their ghetto. The residents, who are desperate and starved, find new hope in Jakobs stories. But it all ends as the Germans deport the people to their death in the extermination camps.

==Cast==
* Vlastimil Brodský - Jakob Heym (voiced by Norbert Christian)
* Erwin Geschonneck - Kowalski
* Henry Hübchen - Mischa
* Blanche Kommerell - Rosa Frankfurter
* Armin Mueller-Stahl - Roman Schtamm
* Peter Sturm - Leonard Schmidt
* Dezső Garas - Mr. Frankfurter (voiced by Wolfgang Dehler)
* Margit Bara - Josefa Litwin (voiced by Gerda-Luise Thiele)
* Reimar J. Baur - Herschel Schtamm
* Zsuzsa Gordon - Mrs. Frankfurter (voiced by Ruth Kommerell)
* Friedrich Richter - Dr. Kirschbaum
* Manuela Simon - Lina
* Hermann Beyer - Duty officer
* Klaus Brasch - Josef Neidorf
* Jürgen Hilbrecht - Schwoch

==Production==

===First attempt=== treatment to the DEFA studio, the state-owned cinema monopoly of the German Democratic Republic. The studio approved of the work and authorized to further develop it. Müller. p. 81.  Becker was paid 2,000 East German Mark.  On 17 February 1965, he handed a 111 pages long scenario over to the studio, and a full script of 185 pages was submitted by him on 15 December. 

During 1965, as work on the script progressed,   freed the ghetto just before the residents were deported, although Jakob died on the barbed wire fence. The chairman pointed out no Jewish ghettos were rescued in such a manner at the war, as the Germans managed to evacuate them all. However, on 22 February he granted permission to begin work on the picture.  Beyer decided to cast Czechoslovak actor Vlastimil Brodský for the role of Jakob, and DEFA began negotiations with the Barrandov Studios. 

===Cancellation===
The producers soon encountered difficulties. In the end of 1965, from the 16th to the 18th of December, the  s The Rabbit Is Me and Beyers Trace of Stones. The latter had its premiere in summer 1966, but was soon removed from circulation. Consequently, the director was reprimanded by the Studio directorate and blacklisted. He was forbidden to work in cinema, and was relegated to work in theater, and later to Deutscher Fernsehfunk, the East German state television.  Subsequently, the production of Jacob the Liar ceased on 27 July 1966. 

Beckers biographer Beate Müller wrote that while the implications of the Plenum played a part in this, "it would be misleading to lay all responsibility there": neither Becker nor the film were mentioned by the Party, and DEFA might have simply found another director.  Beyer wrote the film was deemed sound on the political level, adding the censure never tried to interfere with Jacob the Liar.  There were other reasons: Beyer intended to hold principal photography in Poland, mainly in the former Kraków Ghetto, and requested the authorities of the Peoples Republic of Poland for permission; He also planned to have the Poles help finance the picture. The Polish United Film Production Groups responded negatively.  Beyer believed the sensitivity of the subject of the Holocaust in Poland stood behind the rejection.  More importantly, the banning of many of the pictures made in 1965 and the abortion of other future projects which were under way and feared to be non-conformist had run DEFA into a financial crisis. All future revenues were now dependent on the pictures which were not affected by the Plenum, and only a meager budget remained for new undertakings. 

===Completion===
The rejection of the film motivated Becker to turn his screenplay into a novel.  Jacob the Liar was first published in 1969, and became both a commercial and a critical success, winning several literary prizes, also in West Germany and abroad.  The acclaim it received motivated the West German ZDF television network to approach the author and request the rights to adapt it. Becker, who was now a famous and influential author, went to Beyer instead and suggested they resume work on the unfinished picture from 1966. The director proposed to make it a co-production of DEFA and DFF.  The picture - which was always positively viewed by the establishment - was never banned, and the studio maintained an interest to film the script throughout the years passed.  On 16 March 1972, a contract was signed between Becker and the studios. He handed over a 105 pages long scenario on 22 June 1972,  which was approved on 11 August. A final script, with 152 pages, was authorized by DEFA on 7 January 1974. Müller. p. 123. 
 the historical center of which was undergoing demolition; he believed the ruins would best serve as the site of the ghetto. 

West German actor   and Peter Sturm.

Principal photography commenced on 12 February 1974, and ended on 22 May. Editing began on 4 June. The studio accepted the picture in October, requiring only minor changes. The final, edited version was completed on 3 December. The producers remained well within their budget confines, and the total cost of Jacob the Liar was summed to 2,411,600 East German Mark. 

==Reception==

===Distribution===
 
Jacob the Liar was never expected to be a commercial success: DEFA officials who held a preliminary audience survey in 1974 estimated on 28 May that no more than 300,000 people would view it. Müller believed that for this reason, although its premiere in cinemas was to be held in early 1975, both DEFA and the Ministry of Culture did not object when the DFF directorate requested to broadcast it first on television, claiming they lacked an "emotional high point" for the 1974 Christmas television schedule. On Sunday, 22 December 1974, a black-and-white version was screened by DFF 1, in one of the leading prime time slots of its annual schedule. It was seen by "millions of viewers". 

When it was eventually distributed to cinemas in April 1975, its attendance figures were further damaged by the earlier broadcast on television.  It was released in only seventeen copies, and sold merely 89,279 tickets within the first thirteen weeks.  The number rose to only 164,253 after a year, to 171,688 by the end of 1976 and to 232,000 until 1994. Müller. p. 129. 

In spite of this, Jacob the Liar became an international success: it was exported to twenty-five foreign states, a rare achievement for an East German film, especially since only five of those were inside the Eastern Bloc: Hungary, Cuba, Bulgaria, Romania and Czechoslovakia. Most DEFA pictures of the 1970s were allowed in no more than one non-communist state, if any at all, while Beyers film was bought by distributors in West Germany, Austria, Greece, Italy, the United States, Iran, Japan, Angola and Israel, among others. In that respect, "Jacob the Liar was certainly no flop". 

===Awards===
On 1 October 1975, Becker, Beyer, Brodský, actor Erwin Geschonneck, dramatist Gerd Gericke and cinematographer Günter Marczinkowsky received the National Prize of East Germany 2nd class in collective for their work on the picture. 

The film was turned down by the organizers of the IX Moscow International Film Festival, held in July 1975, due to its subject, which was deemed "outdated".  Beckers biographer Thomas Jung claimed the reason was "the taboo theme of antisemitism in Eastern Europe". 

Jacob the Liar was the first ever East German film that was entered into the  , Vlastimil Brodský won the Silver Bear for Best Actor.    It was also nominated for the Academy Award for Best Foreign Language Film at the 49th Academy Awards,    the only East German picture ever to be selected.   

===Critical response===
Hans-Christoph Blumenberg of Die Zeit commented: "Gently, softly, without cheap pathos and sentimentality, Beyer tells a story about people in the middle of horror... The remarkable quality of this quiet film is achieved not least due to superb acting by the cast". 

The New York Times reviewer Abraham H. Weiler wrote Jacob the Liar "is surprisingly devoid of anything resembling Communist propaganda... Brodský is forceful, funny and poignant". He added it "illustrates Mark Twains observation that courage is resistance to fear, mastery of fear, and not the absence of fear". 

===Analysis===
Martina Thiele noted that Jacob the Liar "is one of the few DEFA pictures that may be called Holocaust films". While the topic was not infrequent in East German cinema, it was usually portrayed in a manner conforming to the official view of history: the victims were portrayed as completely passive, while the emphasis was laid on the communist struggle against the Nazis. In Beyers film, a Jew was first seen to offer resistance. 
 Professor Mamlock, and although he "exhibits a certain degree of agency", he still does so out of almost maternal, nurturing instincts. 

Daniela Berghahn wrote "the innovative aspects" of the film "consist of its new approach... The use of comedy... It pays no tribute to historys victors, only to its victim... By turning the negatives into positives, Beyer conveys a story of hope... And makes the impact of Jakobs lie on ghetto life tangible". 

==See also== Jakob the Liar (1999 remake)
* List of submissions to the 49th Academy Awards for Best Foreign Language Film
* List of German submissions for the Academy Award for Best Foreign Language Film

==References==
 

==Bibliography==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

== External links ==
* 

 

 
 
 
 
 
 
 
 