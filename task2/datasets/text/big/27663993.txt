A Thief Catcher
{{Infobox film
  | name = A Thief Catcher
  | image = A Thief Catcher.jpg
  | imagesize= 
  | caption = 
  | director =  Ford Sterling
  | producer = Mack Sennett
  | writer = 
  | starring = Ford Sterling Mack Swain Edgar Kennedy Charles Chaplin
  | music =
  | cinematography = 
  | editing =
  | distributor = Keystone Studios
  | released =  
  | runtime = 1 reel  5 min, 47 sec English (Original titles)
| country        = United States
  | budget =
  }}
 Keystone film company, directed by Ford Sterling,  and starring Sterling, Mack Swain, Edgar Kennedy, and Charles Chaplin   as a policeman. Chaplin had claimed in interviews that he had played a bit-role as a policeman while at Keystone Studios.   

==Cast==

* Ford Sterling: Chief
* Charles Chaplin: Policeman (uncredited)
* William Hauber: Policeman (uncredited)
* George Jeske: Policeman (uncredited)
* Edgar Kennedy: Crook (uncredited)
* Rube Miller:Policeman (uncredited)
* Mack Swain: Crook (uncredited)

==Preservation status==
The film was believed lost and Chaplins appearance unknown until a vintage 16mm print was discovered by director / film historian Paul E. Gierucki in 2010 at a Michigan antique sale.    

==See also==
* List of American films of 1914
* Charlie Chaplin filmography
* List of rediscovered films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 