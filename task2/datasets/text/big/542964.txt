Tintin and the Golden Fleece
 
{{Infobox film
| name         = Tintin and the Golden Fleece
| image        = Tintin (1961 film).jpg
| caption      = Theatrical poster
| director     = Jean-Jacques Vierne
| producer     = André Barret
| writer       = André Barret Rémo Forlani
| starring     = Jean-Pierre Talbot  Georges Wilson 
| music        = André Popp
| cinematography = Raymond Pierre Lemoigne
| editing       = Léonide Azar
| distributor  = Pathé (France)
| released     =  
| runtime      = 
| country      = France Greek
| budget       = 
}} Belgian writer-artist Hergé, it was a live-action film with actors made-up to look like the characters and featured an original storyline not based on any of the books.

The film is set in Turkey and Greece with the main characters of Tintin and Captain Haddock searching for treasure after inheriting a ship called the Golden Fleece. The film was followed by a less successful sequel, Tintin and the Blue Oranges. 

==Plot== Tintin (Jean-Pierre Snowy and the Captain travel to Istanbul only to find that it is an old cargo ship in a really dilapidated state. A businessman named Anton Karabine (Demetrios Myrat) claims to be an old friend of Paparanic and offers to buy the boat for "sentimental" reasons, but the huge amounts that he offers makes Tintin suspicious and on his advice Haddock turns the offer down.
 red herring.

Back home at Marlinspike Hall, another letter arrives for Captain Haddock, this time from the Government in Tetaragua, thanking him for the return of their gold. Furthermore, it reports that the main square in the capital has been renamed Paparanic Square, and Haddock awarded Tetaraguas highest decoration, the Order of the Scarlet Ocelot - a large medal is enclosed with the letter. Then he and Tintin are treated to a visit by the local band to help celebrate.

==Cast==
* Jean-Pierre Talbot as Tintin
* Georges Wilson as Captain Haddock
* Georges Loriot as Professor Calculus
* Charles Vanel as Father Alexandre
* Darío Moreno as  Midas Papos
* Dimos Starenios as Scoubidouvitch
* Ulvi Uraz as Malik
* Marcel Bozzuffi as Angorapoulos
* Demetrios Myrat as Karabine
* Henri Soya as Clodion
* Max Elloy as Nestor
* Serge Marquand as Farmer
* Michel Thomass as Yéfime
* Dora Stratou as Panegyrist

The actors playing Thomson and Thompson are listed as "incognito" in the end credits.

Snowy the dog is credited as Milou, which is his original French name.
 The French Connection.

==Notes==
"Karabine" is a pun on "carabine", the French for "rifle", a hint that the character may be an arms dealer, though his business is called "Karexport" ("car-export"). The crocodile that symbolises the company (but which is red and facing leftwards) is similar to the logo of Lacoste clothing.

"Scoubidouvitch" comes from the term Scoubidou which was popular at the time.

==Book version==
The film was made into a book, in French, English and Spanish. Unlike most of the Tintin books, including that of the animated film Tintin and the Lake of Sharks, it is not in comic strip form, but is made up of written text with stills from the film, some in colour, others in black and white. Today, the English translation is highly sought after by collectors.

==External links==
* 
*  

 

 
 
 
 
 
 