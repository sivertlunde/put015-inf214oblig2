Up the Yangtze
 
{{Infobox film
| name           = Up the Yangtze
| image          = Uptheyangtze-poster.jpg
| caption        = the films poster
| director       = Yung Chang John Christou
| writer         = Yung Chang
| narrator       = Yung Chang
| starring       = Yu "Cindy" Shui Chen "Jerry" Bo Yu
| music          = Olivier Alary
| cinematography = Wang Shi Qing
| editing        = Hannele Halm
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 93 minutes
| country        = Canada Mandarin
| budget         = 
}} develops its rural areas. The film is a co-production between the National Film Board of Canada and Montreals EyeSteelFilm with the participation of the Canadian Broadcasting Corporation, National Geographic Channel, P.O.V., SODEC, and Telefilm. The film is being distributed in the USA by Zeitgeist Films. The United Kingdom distributor is Dogwoof Pictures.   

==Plot summary==
The setting of the film is a riverboat cruise ship floating up the Yangtze river.  Two young people are the focus of the film as they work aboard the ship.  One is a sixteen-year-old girl from a particularly poor family living on the banks of the Yangtze near Fengdu County|Fengdu, named "Cindy" Yu Shui.  She is followed as she leaves her family to work on one of the cruise ships serving wealthy western tourists at the same time as her family is being forced from their home due to the flooding that accompanied the building of the dam.  The film shows her acclimatization to the consumer economy of tourism as well as modern technology of the cruise ships, juxtaposed with her family and other older citizens who are displaced from a rural lifestyle to cities where they must pay for the vegetables they used to grow on their own.

The other main subject is a nineteen-year-old boy from a more prosperous family named "Jerry" Chen Bo Yu.  Chen Bo Yu shows a different perspective from Yu Shui as he tries to earn cash on the ship as a porter and singer.

The film is rich in detail about ways of life, dreams, and aspirations, and seeks to provide a view into the reality of modern Chinese life.

==Critical reception==
Up the Yangtze was very well received by film critics, and was described as "astonishing" documentary which "refuses to editorialize" by The New York Times.  It garnered a 96% rating on Rotten Tomatoes based on 47 reviews,  and a rating of 84 on Metacritic. 
 Still Life),  Rick Groen  of The Globe and Mail named it the 7th best film of 2008,  and Liam Lacey of The Globe and Mail named it the 10th best film of 2008.     

==Screenings and awards==
{| class="wikitable"
|-
! Festival
! Award
! Date
|-
| Genie Awards
| Best Documentary
| 2009
|-
| Golden Horse Film Festival and Awards
| Best Documentary
| 2008
|-
| Sundance Film Festival
| Official Selection
| 2008
|-
| Vancouver International Film Festival
| Best Canadian Documentary
| 2008
|-
| San Francisco International Film Festival
| Best Feature Documentary
| 2008
|-
| Independent Spirit Awards
| Best Documentary Nominee
| 2009
|}

==Epilogue==
After filming concluded, "Cindy" Yu Shui returned home to finish her education while her family worked odd jobs.  The filmmaker Yung Chang published letters indicating that Yu Shuis father had serious eye issues that required surgery and a donation fund was set up.

In 2010, Yung gave further updates on the Yu family, indicating that they were doing much better, with an apartment with television and a refrigerator. 

==See also==
* Bingai, a 1997 documentary film about a Chinese woman who refuses to relocate during the building of the Three Gorges Dam

==References==
 

==External links==
*   (archived 2009)
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 