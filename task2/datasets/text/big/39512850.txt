Metallica Through the Never
 
{{Infobox film
| name           = Metallica Through the Never
| image          = Metallica Through the Never film.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = A promotional poster for the film
| director       = Nimród Antal
| producer       = Charlotte Huggins
| writer         = {{Plain list |
*Nimród Antal
*James Hetfield
*Lars Ulrich
*Kirk Hammett
*Robert Trujillo
 
}}
| starring       = {{Plain list |
*Dane DeHaan
*James Hetfield
*Lars Ulrich
*Kirk Hammett
*Robert Trujillo
 
}}
| music          = Metallica
| cinematography = Gyula Pados
| editing        = Joe Hutshing
| studio         = Blackened Recordings Picturehouse
| released       =  
| runtime        = 94 minutes  
| country        = United States
| language       = English
| budget         = $18 million   
| gross          = $21,196,967   Rating = R}} thriller concert heavy metal Through the Picturehouse marquee, which had been shut down since 2008.

==Plot==
 
  BC Place skates past roadie for Ecstasy of Gold.

Metallica appears on stage and begins performing, and during Creeping Death, Trips manager tasks him with bringing gas to a stranded truck carrying "something very important." Before driving his van, Trip ingests a blue and red capsule.

As Metallica begins Fuel (song)|Fuel, Trip drives through the deserted streets with Metallica-esque flames on flat surfaces of the cityscape, alluding to the pills effects or supernatural energy. Distracted by studying his map, Trip runs a red light but brakes late in the intersections middle when he noticed a bloody hand smear on an illuminated sign. He gets t-boned by another driver, which subsequently flips the van.
 Police horse drags a dead officer across its path. After this, some people begin to run past him, oblivious to his presence, followed by some police cruisers, one of which is on fire. Emerging into the middle of a street, he realizes he has wandered between a large group of rioters and riot police, who are motionless. Gradually, the rioters start getting rowdier and more provocative, and when the police fire tear gas, the rioters swarm the police, in a full-on battle. Trip, stunned by the ferocity of the violence, stands by a window being strangely ignored. Suddenly, the window shatters behind Trip.

In the heat of battle, a horseman (listed in the credits as The Rider, and known by some as "The Death Dealer") wearing a gas mask comes charging through, wielding a sledgehammer atop an armored horse.  He pursues a rioter, lassoing his neck and hoisting the victim on a streetlamp, killing him. It is not formally indicated for which side The Rider fights. Enraged by this, Trip throws a brick at The Rider. This fixates The Rider on Trip, and he starts to pursue Trip, who loses the doll. Trip manages to evade The Rider by sneaking through a wall of rioters.

Walking down a deserted street that is littered from an earlier battle, Trip walks under a dozen hanging bodies from a pedestrian bridge; it is unrevealed as to how they got there. Trip finds the truck he was tasked to give fuel to in a deserted parking garage corridor, but the driver, a middle aged man, is too traumatized in the cab to heed the presence of Trip. The back of the truck is opened with nothing but a leather bag which Trip opens, but the camera pans away. Trip just slumps down, speechless.

Sitting around, Trip notices a figure waving at him from a distance. The Rider rides up behind the man, and suddenly a swarm of mobbers surge past the two.  It is left unstated if they are the pre-encountered rioters or The Riders minions (assuming The Rider is a supernatural entity). Trip then grabs the bag and tank of gas, jumps out of the truck, and flees.

Unable to shake off the pack of mobbers, Trip is cornered at the end of an alley with the mobbers closing on him. Trip ties a black bandanna around his mouth, puts up his hoodie, and drops the bag. Picking up the gas tank, Trip pours the gas onto himself and sets himself ablaze, and then charges the mobbers, being beaten collectively.

Trip wakes on a building rooftop (with Enter Sandman starting), while the doll he lost has become animated and is walking to the leather bag.  As Trip is getting up, a noose is lassoed on Trip, and he is pulled upon The Riders horse. Trip grabs The Riders hammer to free himself. The Rider rides to the opposite end of the rooftop and starts to charge Trip, who picks up the hammer and slams the floor with it, sending earthquake-style shock waves across the city, shattering all the glass and structural exteriors of surrounding buildings and disintegrating The Rider and his steed to dust.  This also has the effect of reverberating throughout Metallicas concert, interrupting it with a blackout of power, and causing lighting trusses to fall. The concert staff eventually hook up a back-up generator, and the band continues the concert without the pyrotechnics and light props.

Trip arrives back at the stadium with the doll and the bag under his arm, finding the arena deserted with the concert either over or yet to start. During the credits, intercut scenes of the band performing Master of Puppets|Orion play to the deserted stadium.  When Trip walks through the venue and takes a seat wearing his same clothes, the ending shot is the leather bag, which is never opened.

==Cast==
* Dane DeHaan as Trip, a roadie and the films sole protagonist
* James Hetfield as himself&nbsp;– lead vocals and rhythm guitar
* Lars Ulrich as himself&nbsp;– drums
* Kirk Hammett as himself&nbsp;– lead guitar, backing vocals
* Robert Trujillo as himself&nbsp;– bass, backing vocals
* Mackenzie Gray as concert manager

==Release== Orion Music + More Festival under the name Dehaan, after lead actor Dane DeHaan, to promote the film by playing their debut album, Kill Em All, in its entirety. 
 world premiere at the 2013 Toronto International Film Festival. 
 3D the week after (October 4).

==Reception==
Reviews of the film were positive. The film has a 78% "fresh" rating at Rotten Tomatoes; the consensus states: "Imaginatively shot and edited, Metallica Through the Never is an electrifying, immersive concert film, though its fictional sequences are slightly less assured."   

Peter Travers of Rolling Stone called the film "a full-throttle expression of rock & roll anarchy", and encouraged readers not to understand the plot, but to "live" it. 

Peter Rugg of The Village Voice writes that the film is "the most immersive concert film ever." 

Chris Tilly of IGN delivered a positive review as well, stating that it "features several truly incredible live performances that are worth the price of admission alone. Metallica fans will love the music, while everyone else can enjoy the amazing visuals in this unique concert movie." 

The film has been nominated for a Grammy for Best Music Film.

==Music==
 
The official soundtrack for Through the Never was released on September 24, 2013 via Blackened Recordings.  The album has charted in several countries. The album was nominated for a Grammy for best recording package.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 