The Magnetic Tree
 

{{Infobox film

| name           = The Magnetic Tree
| image          =
| alt            =
| caption        =
| film name      = El árbol magnético
| director       = Isabel de Ayguavives
| producer       = Sergio Gándara, Ignacio Monge, Rafael Álvarez, Leonora Gonzalez
| writer         = Isabel de Ayguavives
| screenplay     = Isabel de Ayguavives
| story          = Isabel de Ayguavives
| based on       =  Original 
| starring       = Andrés Gertrudix, Manuela Martelli, Catalina Saavedra, Gonzalo Robles, Juan Pablo Larenas, Edgardo Bruna, Ximena Rivas, Otilio Castro, Blanca Lewin, Daniel Alcaíno, Agustín Silva, Lisette Lastra, Antonia Zilleruelo, Felipe Misa, Dominga Zilleruelo.
| narrator       = 
| music          = Nico Casal
| cinematography = Alberto D. Centeno
| editing        =  José Manuel Jiménez
| studio         =  Dos Treinta y Cinco P.C. / Parox / Instituto de la Cinematografía y de las Artes Audiovisuales (ICAA) 
| distributor    =  Urban Distribution International
| released       =   7 August 2014 (Chile)
| runtime        = 85
| country        = Chile, Spain
| language       = Spanish
| budget         = 
| gross          =  

}}
 Chilean Spain|Spanish co-produced film written and directed by Isabel de Ayguavives and filmed in Chile. 
The Magnetic Tree is Isabel de Ayguavives debut feature film.

== Plot ==

Bruno is a young immigrant returning to Chile from Germany after a long absence. Bruno is staying in the house of his cousins in the country, where the whole family are gathered to bid the place a farewell, as it is about to be sold. They visit a place that he remembers fondly, the "Magnetic Tree" a local curiosity. The tree has a mysterious magnetic force, so powerful that it can pull cars toward itself.

The group, in a series of free and open conversations, reveal the feelings that come from a family relationship.

== Cast ==

*Andrés Gertrúdix
*Catalina Saavedra (The Maid (2009 film), Magic Magic (2013 film))
*Manuela Martelli
*Gonzalo Robles
*Juan Pablo Larenas
*Daniel Alcaíno
*Edgardo Bruna

== Production ==

* The movie is a Chilean-Spanish production by Dos Treinta y Cinco P.C, Parox, and Instituto de la Cinematografía y de las Artes Audiovisuales .

=== Also known as ===

* Spanish - El árbol magnético
* Worldwide (English title) - The Magnetic Tree

== Awards ==

* San Sebastian Film Festival Nominated: Kutxa – New Director Award 

  as Carla in The Magnetic Tree]]

== Reception ==

* The film had generally positive reception.

* Twitchfilm review: "The strongest element of the movie, written and directed by Isabel de Ayguavives, is how it manages to recreate that feeling of a family reunion, specially when it comes to Chile." 
* Cineuropa review: "Ayguavives has created a mosaic of different situations that combine to form a subtle reflection on that damned nostalgia that can sometimes end up weighing down too heavily on us." 

* The Hollywood Reporter: “Lively and intimate, its a film made by someone whose interest in and compassion for her people is deep and forgiving.” Jonathan Holland, The Hollywood Reporter. 

=== See Also ===

* Cinema of Chile
* Cinema of the world
* World cinema

== References ==

 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 

 