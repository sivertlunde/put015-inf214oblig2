Carry On Columbus
 
 
 
{{Infobox film| name = Carry On Columbus
| image_size =
| image	=	Carry On Columbus FilmPoster.jpeg
| caption = Artwork by Arnaldo Putzu
| director = Gerald Thomas
| producer = John Goldstone Peter Rogers  (executive producer)  Dave Freeman John Antrobus Peter Richardson Larry Miller Richard Wilson
| music = John Du Prez
| cinematography = Alan Hume
| editing = Chris Blunden
| studio = Island World
| distributor = United International Pictures (UK)
| released = October 1992
| runtime = 91 minutes
| country = United Kingdom
| language = English
| budget = £2,500,000
}}

Carry On Columbus (1992) is the 31st and final (to date) film in   and   came out the same year).

==Plotline==
Christopher Columbus (Jim Dale) believes he can find an alternative route to the far East and persuades the King (Leslie Phillips) and Queen of Spain (June Whitfield) to finance his expedition. But the Sultan of Turkey (Rik Mayall), who makes a great deal of money through taxing the merchants who have to pass through his country on the current route, sends his best spy, Fatima (Sara Crowe), to wreck the trip...

==Casting== Jack Douglas, making his eighth appearance in the series.

Original Carry On performer Frankie Howerd was signed up to appear, but he died shortly before he was due to film his role. His part as the King of Spain was offered to original series regular Bernard Bresslaw, who turned it down. Leslie Phillips eventually took on the role, playing opposite June Whitfield as the Queen, a role turned down by both Joan Sims and Barbara Windsor.
 alternative comedians Peter Richardson, Alexei Sayle, Rik Mayall, Julian Clary and Nigel Planer, all of whom except Clary are from the Comic Strip, to appear in the film.

This was the last film that Gerald Thomas directed, as he died on 9 November 1993.

==Cast==
*Jim Dale as Christopher Columbus
*Bernard Cribbins as Mordecai Mendoza
*Maureen Lipman as Countess Esmerelda
*Alexei Sayle as Achmed
*Rik Mayall as The Sultan
*Sara Crowe as Fatima
*Julian Clary as Don Juan Diego Keith Allen as Pepi the Poisoner
*Leslie Phillips as King Ferdinand
*Jon Pertwee as the Duke of Costa Brava Richard Wilson as Don Juan Felipe
*Rebecca Lacey as Chiquita
*June Whitfield as Queen Isabella
*Nigel Planer as The Wazir Larry Miller as The Chief Jack Douglas as Marco the Cereal Killer
*Andrew Bailey as Genghis
*Burt Kwouk as Wang Philip Herbert as Ginger
*Tony Slattery as Baba the Messenger
*Martin Clunes as Martin
*David Boyce as Customer with ear
*Sara Stockbridge as Nina the model
*Holly Aird as Maria James Faulkner as Torquemada
*Don Maclean as Inquisitor with ham sandwiches Dave Freeman as Inquisitor
*Duncan Duff as Inquisitor
*Jonathan Tafler as Inquisitor
*James Pertwee as Inquisitor
*Toby Dale as Inquisitor
*Michael Hobbs as Inquisitor Peter Grant as Cardinal
*Su Douglas as Countess Joanna
*John Antrobus as Manservant
*Lynda Baron as Meg
*Allan Corduner as Sam
*Nejdet Salih as Fayid
*Mark Arden as Mark
*Silvestre Tobias as Abdullah
*Daniel Peacock as Tonto the Torch
*Don Henderson as The Bosun
*Harold Berens as Cecil the Torturer
*Peter Gilmore as Governor of the Canaries
*Marc Sinden as Captain Perez
*Charles Fleischer as Pontiac
*Chris Langham as Hubba
*Reed Martin as Poco Hontas
*Prudence Solomon as Ha
*Peter Gordeno as The Shaman

==Crew & Technical== Dave Freeman
*Additional Material – John Antrobus
*Music – John Du Prez
*Song – Malcolm McLaren & Lee Gorman
*Performers – Jayne Collins & Debbie Holmes
*Production Supervisor – Joyce Herlihy
*Costume Designer – Phoebe De Gaye
*Editor – Chris Blunden
*Production Designer – Harry Pottle
*Director of Photography – Alan Hume
*Casting – Jane Arnell
*Art Director – Peter Childs
*Assistant Directors – Gareth Tandy, Terry Bamber & Becky Harris
*Art Director – Peter Childs
*Set Decorator – Denis Exshaw
*Assistant Art Director – Edward Ambrose
*Camera Operator – Martin Hume
*Sound Recordist – Chris Munro
*Chief Dubbing Editor – Otto Snel
*Assistant Editor – Steve Maguire
*Make-up – Sarah Monzani & Amanda Knight
*Hairdresser – Sue Love & Sarah Love
*Title Design – Gillie Potter
*Stillsman – Keith Hamshere
*Costumes – Angels and Bermans
*Colour – Rank Laboratories
*Titles & Opticals – General Screen Enterprises
*Executive Producer – Peter Rogers
*Producer – John Goldstone
*Director – Gerald Thomas

==Filming and locations==

*Filming dates: 21 April – 27 May 1992

Interiors:
* Pinewood Studios, Buckinghamshire

Exteriors:
* Frensham Ponds. This location was previously used nearly 30 years earlier for the similarly nautical Carry On Jack.

==Reception==
The film was panned by many critics.   and  , although all three films flopped. Carry On Columbus was also shot on a much lower budget than the other two films, a budget of £2.5 million compared to the other two budgets of $45 million and $47 million respectively. 

In a 2004 poll of British film actors, technicians, writers and directors on British cinema, Carry On Columbus worst British film ever. 

== References ==
 

==Bibliography==
* 
* 
* 
* 
*Keeping the British End Up: Four Decades of Saucy Cinema by Simon Sheridan (third edition) (2007) (Reynolds & Hearn Books)
* 
* 
* 
* 
* 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 