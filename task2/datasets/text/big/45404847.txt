Roar of the People
 
 
{{Infobox film
| name           = Roar of the People
| image          = 
| alt            = 
| caption        = 
| film name      = 民族的吼聲
| director       = Tang Xiaodan
| producer       = Chiu Shu-San
| writer         = Lee Fung
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = 
| cinematography = 
| editing        = 
| studio         = Grandview Company 
| distributor    =  
| released       =  
| runtime        = 117 min   
| country        = Hong Kong
| language       = Cantonese 
| budget         = 
| gross          =  
}}
Roar of the People is a 1941 Hong Kong wartime drama film directed by Tang Xiaodan.

The film is set in Hong Kong during the Second Sino-Japanese War, when people fled to Hong Kong from Mainland China.  It was released five months before the start of the Japanese occupation of Hong Kong. 

==Cast==
* Cheung Ying	
* Fung Fung		
* Wong An	
* Chan Tin-Tsung
* Fung Ying-Seong	
* Ng Wui	
* Lee Tan-Lo	
* Leong Mo-Sik	
* Cho Tat-wah	
* Gao Luquan (credited as Ko Lo-Chuen)

==References==
 

==External links==
*  
*  
*   at Hong Kong Cinemagic

 
 


 