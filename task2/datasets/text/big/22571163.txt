The Others (1974 film)
{{Infobox film
| name           = The Others
| image          = 
| image_size     = 
| caption        = 
| director       = Hugo Santiago
| producer       = Anna Maria Papi
| writer         = Adolfo Bioy Casares Jorge Luis Borges Hugo Santiago
| narrator       = 
| starring       = Maurice Born
| music          = 
| cinematography = Ricardo Aronovich
| editing        = 
| distributor    = 
| released       = May, 1974
| runtime        = 127 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Others ( ) is a 1974 French drama film directed by Hugo Santiago. It was entered into the 1974 Cannes Film Festival.   

==Cast==
* Maurice Born - Durtain
* Noëlle Chatelet - Valérie
* Patrice Dally - Roger Spinoza
* Pierrette Destanque - Agnès
* Bruno Devoldère - Mathieu Spinoza
* Dominique Guezenec - Béatrice Alain Pierre Julien - M. Marcel
* Marc Monnet - Vidal
* Roger Planchon - Alexis Artaxerxès
* Jean-Daniel Pollet - Adam
* Daniel Vignat - Lucien Moreau

==References==
 

==External links==
* 

 
 
 
 
 
 
 