Let's Get Married (1926 film)
{{infobox film
| name           = Lets Get Married
| image          =
| imagesize      =
| caption        =
| director       = Gregory La Cava
| producer       = Adolph Zukor Jesse L. Lasky William LeBaron (associate producer)
| writer         = Henry A. Du Souchet (play The Man from Mexico) Luther Reed (adaptation) J. Clarkson Miller (writer) John Bishop (intertitles) Richard Dix Lois Wilson
| cinematography = Edward Cronjager
| editing        =
| distributor    = Paramount Pictures
| released       = March 1, 1926
| runtime        = 70 minutes 7 reels (6,700 feet)
| country        = United States Silent (English intertitles) 
}} 1926 American silent comedy Richard Dix Lois Wilson. The film is based on an 1897 play The Man from Mexico by Henry A. Du Souchet performed by William Collier, Sr..

This film is a remake of a 1914 film, The Man from Mexico starring John Barrymore which is now considered a lost film. The 1926 silent is preserved at the Library of Congress.    

==Cast== Richard Dix - Billy Dexter Lois Wilson - Mary Corbin
*Nat Pendleton - Jimmy
*Douglas MacPherson - Tommy
*Gunboat Smith - Slattery
*Joseph Kilgour - Billys Father
*Thomas Findley - Marys Father
*Edna May Oliver - J.W. Smith

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 