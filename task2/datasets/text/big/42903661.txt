Trouble at Timpetill
{{Infobox film
| name           = Les Enfants de Timpelbach
| image          = 
| director       = Nicolas Bary
| producer       = 
| writer         = Nicolas Bary Nicolas Peufaillit
| starring       = Raphaël Katz Adèle Exarchopoulos
| cinematography = 
| editing        = 
| music          = 
| distributor    = 
| studio         = 
| country        = France
| language       = French
| runtime        = 133 minutes
| budget         = $14,542,097
| gross          = $10,673,168  
| released       =  
}}
Trouble at Timpetill ( ) is a 2008 French fantasy adventure film written and directed by Nicolas Bary, based on the novel of the same name by Henry Winterfeld.

The film won the TFO Prize for Best Youth Film at the 2011 edition of the Cinéfranco film festival.

==Cast==
* Raphaël Katz : Manfred
* Adèle Exarchopoulos : Marianne
* Léo Legrand : Thomas
* Gérard Depardieu : Le général Igor
* Carole Bouquet : Drohne
* Armelle : linstitutrice Corbac
* Éric Godon : Stettner
* Baptiste Betoulaud : Oscar Stettner
* Lola Créton : Mireille Stettner
* Léo Paget : Robert Lapointe
* Terry Edinval : Wolfgang
* Florian Goutieras : Ptit Louis
* Mathieu Donne : Gros Paul
* Martin Jobert : Willy Hak
* Ilona Bachelier : Charlotte
* Julien Dubois : Barnabé
* Marcus Vigneron : Charles Benz
* Jonathan Joss : Jean Krög
* David Cognaux : Kevin
* Sacha Lecomte : Philibert
* Tilly Mandelbrot : Erna
* Maxime Riquier : Bobby le Scribe
* Manon Chevallier : Marion
* Valentine Bouly : Paulette
* Talina Boyaci : Zoé
* Vanille Ougen : Kimy
* François Damiens : le livreur
* Éric Naggar : le brigadier Krögel
* Philippe Le Mercier : le maire
* Stéphane Bissot : la mère de Manfred Mayane Maggiori : Hak
* Odile Matthieu : la mairesse Krog
* Thierry Desroses : labbé
* Isabelle de Hertogh : Edith Benz

==References==
 

== External links ==
*  

 
 
 
 

 