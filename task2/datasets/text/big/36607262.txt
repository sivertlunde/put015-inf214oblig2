Phata Poster Nikhla Hero
 
 
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Phata Poster Nikla Hero
| image          = Phata Poster Nikhla Hero Official poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Rajkumar Santoshi 
| producer       = Ramesh Taurani Ronnie Screwvala Siddharth Roy Kapur
| writer         = Rajkumar Santoshi
| starring       = Shahid Kapoor Ileana DCruz
| music          = Songs: Pritam Background Score: Raju Singh
| studio         = Tips Music Films
| distributor    =
| runtime        =
| released       =  
| country        = India
| language       = Hindi
| budget          = 
| gross             = 
}}
Phata Poster Nikhla Hero (English: Poster torn, out came a hero) is a 2013 Bollywood action comedy film directed by Rajkumar Santoshi. The film features Shahid Kapoor and Ileana DCruz in the lead roles, and was released on 20 September 2013 to a mixed to positive response from critics.

==Plot==
Vishwas Rao (Shahid Kapoor) is raised by his mother, Savitri (Padmini Kolhapure), who hopes that her son will grow up to be a brave Inspector. But Vishwas wants to become an actor. Dressed as a Police Inspector for a photo shoot, he meets social worker Kajal (Ileana DCruz) during a car chase. Kajal mistakes him as an inspector and thanks him for helping her catch the goons, while Vishwas plays along. He is mistakenly published in the newspaper as an Inspector, which Savitri sees. She arrives in Mumbai to see her dream come true, and circumstances lead Vishwas to carry on his lie not only to Kajal, but also to his mother.

While shooting for a film, Vishwass mother comes to know that he is not an inspector. She faints and is admitted in the hospital, where the doctor tells him that she needs to be operated and the amount required is rupees ten lakhs. He agrees to work for Gundappa (the boss of the goons Vishwas and Kajal caught) for money where he needs to bring a CD for them. While escaping after getting the CD he is caught by police and accidentally shoots two police officers. Since he became a murderer he was left with no other choice than to join Gundappas gang. Meanwhile, Kajal, who is under the impression that Vishwas has gone bad, along with some men plans to save him from doing any crime. So they go to the gangs den purposely and tells the foolish Gundappa that she came to marry Vishwas. Hence, creating a confusion so that Gundappa leaves Vishwas. On the other hand, Joint Commissioner (who was also kidnapped) tells Savitri that Vishwas is actually not doing any crime rather is working for the police. Kajal knows this from Savitri later and goes to apologise to Vishwas for misunderstanding him. Not knowing that they are being watched on CCTV, she apologises to Vishwas and leakes details of his plans. Somehow, Vishwas and his mates are able to flee. But his mother and Kajal are held by Napoleon (the ultimate Don, boss of Gundappa). After fighting the goons, Vishwas hands over Napoleon (who is earlier revealed to be his own father) to the police and tells everyone that he will become a police officer as they are real life heroes. He did all the filmy actions as a fake police officer now he wanted to be a real police man and do his duty for his country and his mothers dreams finally fulfilled. 

==Cast==
* Shahid Kapoor as Vishwas Rao
* Ileana DCruz as Kajal
* Padmini Kolhapure as Savitri {Vishwass Mother} 
* Darshan Jariwala as Commissioner
* Saurabh Shukla as Gundappa Das Sanjay Mishra as Jogi Bhai Zakir Hussain as Officer Ghorpade
* Mukesh Tiwari as Napoleon and Vishwass Father Yashwanth Rao
* Rana Jung Bahadur
* Deepika Kamaiah as chief security officers daughter
* Tinnu Anand as Director
* Nargis Fakhri (special appearance in the song "Dhating Naach") 
* Salman Khan as himself in a special appearance

==Crew==
* director – Rajkumar Santoshi
* Producer – Ramesh S Taurani
* Screenplay – Rajkumar Santoshi
* Cinematographer – Ravi Yadav
* Creative Producer – Jay Shewakramani
* Production Designer – Priya Suhaash
* Music – Pritam
* Choreographer – Bosco-Caesar
* First Assistant Director – Manish Harishankar
* Editor – Steven Bernard
* Background Score – Raju Singh
* Lyricist – Irshad Kamil, Amitabh Bhatachharya
* Action – Kanal Kannan, Tinu Verma
* Sound Engineer – Rakesh Ranjan

==Development==
The film has often been termed as a full-on "masala" commercial entertainer. Other than the casting and genre, the makers refused to divulge further details. In early August 2012, it was confirmed by director Rajkumar Santoshi that he had been offered the film along with actor Arpit Sharma and that he had been in talks for the previous six months.  Soon after, when asked, Kapoor confirmed he had not yet signed on for the film, but was on the verge of doing so.  It was also reported at the time that actress Diana Penty had been signed on as the female lead, though Ileana DCruz later replaced her.  Actress Padmini Kolhapure has been signed on to play the role of Kapoors mother in the film. It was later revealed that newcomer Deepika Kamainah will play an important role alongside Shahid Kapoor and Ileana DCruz. 

==Music==
{{Infobox album  
| Name       = Phata Poster Nikhla Hero
| Type       = Soundtrack
| Artist     = Pritam Chakraborty
| Cover      =
| Alt        = 
 
| Recorded   = 2013
| Genre      = Film soundtrack
| Length     =   Tips Music
| Last album = Once Upon Ay Time in Mumbai Dobaara! (2013)
| This album = Phata Poster Nikhla Hero (2013)
| Next album = R... Rajkumar (2013)
}}
The soundtrack was composed by Pritam Chakraborty, with lyrics penned by Irshad Kamil and Amitabh Bhattacharya. The first song, "Tu Mere Agal Bagal Hai", was released on 26 July. The song is sung by Mika Singh.

"Hey Mr DJ" is a club song sung by Benny Dayal, Shefali Alvares and Shalmali Kholgade. The song "Rang Sharbaton Ka" is sung by Atif Aslam and Chinmayi, and the Reprise version of "Main Rang Sharbaton Ka" is sung by Arijit Singh. "Dhating Naach" is another number featuring Nargis Fakhri and sung by Shefali Alvares and Nakash.

The four tracks were released exclusively on the music streaming service Saavn. 

{{Track listing
| extra_column = Artist(s)
| lyrics_credits = Irshad Kamil & Amitabh Bhattacharya
| total_length = 39:14
| music_credits = no

| title1   = Tu Mere Agal Bagal Hai
| extra1   = Mika Singh
| music1   = 
| lyrics1  = 
| length1  = 4:25

| title2   = Main Rang Sharbaton Ka Chinmayi Sripaada
| music2   = 
| lyrics2  = 
| length2  = 4:26

| title3   = Hey Mr. DJ
| extra3   = Benny Dayal, Shefali Alvares & Shalmali Kholgade
| music3   = 
| lyrics3  = 
| length3  = 4:22

| title4   = Mere Bina Tu
| note4    = duet version
| extra4   = Rahat Fateh Ali Khan & Harshdeep Kaur
| music4   = 
| lyrics4  = 
| length4  = 4:23

| title5   = Dhating Naach
| extra5   = Nakash Aziz & Shefali Alvares
| music5   = 
| lyrics5  = 
| length5  = 3:10

| title6   = Janam Janam
| extra6   = Atif Aslam
| music6   = 
| lyrics6  = 
| length6  = 4:48

| title7   = Main Rang Sharbaton Ka
| note7    = reprise version
| extra7   = Arijit Singh
| music7   = 
| lyrics7  = 
| length7  = 4:38

| title8   = Janam Janam
| note8    = reprise version
| extra8   = Sunidhi Chauhan
| length8  = 3:06

| title9   = Mere Bina Tu
| extra9   = Rahat Fateh Ali Khan
| length9  = 4:11

| title10  = Janam Janam
| note10   = sad version
| extra10  = Atif Aslam
| length10 = 1:45
}}

==Filming==
The film was expected to begin shooting in November 2012 after Kapoor completed Maneesh Sharmas next project for Yash Raj Films, but the actor opted out of that film due to other commitments.  Shahid Kapoor announced via Twitter that filming began on 2 November 2012. The film was expected to be completed by February 2013 but due to delay issues it was completed in March 2013.

==Critical response==

===India===
 
The film received mixed and positive response from critics.

Taran Adarsh gave the film 3.5/5 and wrote, "Phata Poster Nikhla Hero brings back memories of old-fashioned comic entertainers. Theres not much of a plot here, but you go with the flow without making much effort. You laugh, celebrate the silly gags and by the time the story reaches its conclusion, you realize that the film has won you over with its unfussy plot and basic characters, who dont have a serious bone in their body. On the whole, Phata Poster Nikhla Hero is an entertainer all the way. If you relished Ajab Prem Ki Ghazab Kahani from the team of Taurani and Santoshi, chances are you will also lap up this vibrant, kaleidoscopic, light-hearted entertainer." 

Madureeta Mukherjee of The Times of India gave the film 3.5 and felt, "The first half offers loads of cackles, chuckles, witticisms and spoofy scenes. Post-interval the comedy collapses for a bit with forced OTT drama, khaali-peeli action, and too many song breaks, but makes a comeback with delightfully funny moments. This ones worth it for the howlarity of it all. And Shahid in his element. Note: You may not like this film if you dont have a taste for silly humour & mindless gags!" 

Faheem Ruhani of India Today gave the film 3 and judged, "Santoshi as writer, director and the dialogue writer is in good form. He would have been terrific if could have captured your interest in the same way as he did in the first half of the film. Still, the film is worth more than a few laughs. More because of the way the director spoofs Bollywoods tried and tested conventions and the jokes he cracks at the expense of an industry he is only too familiar with." 

Rummana Ahmed of Yahoo! Movies awarded the film 3 stars and summarised, "Phata Poster Nikhla Hero will entertain if you are willing to overlook its little indulgences." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 