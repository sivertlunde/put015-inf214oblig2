Bright Angel
{{Infobox film
| name           = Bright Angel
| image          = Bright Angel.jpg
| image_size     = 
| alt            = 
| caption        = VHS cover Michael Fields
| producer       = John Daly Derek Gibson Rbert K. MacLean Paige Simpson 
| writer         = Richard Ford
| narrator       = 
| starring       = Dermot Mulroney Lili Taylor Sam Shepard
| music          = Christopher Young
| cinematography = Elliot Davis
| editing        = Clement Barclay Melody London 
| studio         = Hemdale Film Corporation
| distributor    = Metro-Goldwyn-Mayer
| released       = November 1990 (Turin International Youth Festival) 14 June 1991 (USA)
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $158,243 (USA)
| preceded_by    = 
| followed_by    = 
}} Michael Fields. Although it received some critical acclaim, the film earned only $158,243 at the domestic box office.

==Plot==
George (Dermot Mulroney|Mulroney) is s a Montana teen whose sanity is deteriorating due to his parents marital woes. He links up with Lucy (Lili Taylor|Taylor), a runaway headed for Wyoming with a dark background, who is trying to get her brother out of jail. George tries to help her, but finds himself crossing paths with people even more emotionally disturbed than his mother and father.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Dermot Mulroney || George  
|-
| Lili Taylor || Lucy  
|-
| Sam Shepard ||  Jack  
|-
| Valerie Perrine || Aileen 
|-
| Burt Young ||  Art  
|-
| Bill Pullman ||  Bob  
|-
| Benjamin Bratt || Claude  
|-
| Mary Kay Place || Judy
|-
| Delroy Lindo || Harley
|-
| Will Patton || Woody
|}

==Critical reception==
Roger Ebert of The Chicago Sun-Times enjoyed the film and gave it 3 1/2 stars:
 

However, reviews such as Kathleen Maher of The Austin Chronicle did not think highly of it, gave it 2 stars and had differing views of the acting:
 

==Soundtrack==
*"Where Did God Go?" by Jody Alan Sweet
*"Too Long Crying" by Jody Alan Sweet
*"Cheshire" by Jody Alan Sweet
*"Heal Somebody" by Sheryl Crow
*"Hung Over Heart" by Jody Alan Sweet

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 