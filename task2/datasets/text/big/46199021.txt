The Polar Star
{{Infobox film
| name           = The Polar Star
| image          =
| caption        =
| director       = Arrigo Bocchi
| producer       = 
| writer         = Leslie Stiles  
| starring       = Manora Thew   Hayford Hobbs   Peggy Patterson 
| cinematography = 
| editing        = 
| studio         = Windsor Films
| distributor    = Walturdaw 
| released       = December 1919
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent mystery film directed by  Arrigo Bocchi and starring Manora Thew, Hayford Hobbs and Peggy Patterson.  When a London soliciter is killed in mysterious circumstances in Italy.
 on location in Italy.

==Cast==
* Manora Thew  
* Hayford Hobbs   
* Peggy Patterson  
* Bert Wynne    Charles Vane

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

  

 
 
 
 
 
 
 
 
 
 
 
 

 