The Score (2001 film)
{{Infobox film
 | name           = The Score
 | image          = The Score film.jpg
 | caption        = Theatrical release poster
 | alt            = 
 | director       = Frank Oz
 | producer       = Gary Foster Lee Rich
 | writer         = Scott Marshall Smith  Daniel E. Taylor Kario Salem
 | starring       = Robert De Niro Edward Norton Angela Bassett Marlon Brando
 | music          = Howard Shore
 | cinematography = Rob Hahn Richard Pearson
 | studio         = Mandalay Pictures
 | distributor    = Paramount Pictures
 | released       = July 13, 2001
 | runtime        = 124 minutes
 | language       = English
 | budget         = $68,000,000
 | gross          = $113,579,918
}}
 2001 crime thriller film directed by Frank Oz, and stars Robert De Niro, Edward Norton, Angela Bassett and Marlon Brando in his final film role.
 The Godfather saga. The screenplay was based on a story by Daniel E. Taylor and Emmy Award|Emmy-winner Kario Salem.

== Plot == fence Max (Marlon Brando). The job, worth a $4 million payoff to Nick, is to steal a sceptre, which is discovered to be a French national treasure. It was hidden in the leg of an antique piano, smuggled illegally through Canada into the United States, but was accidentally uncovered, then stored in the ultra-secure basement of the Custom House, Montreal|Montréal Customs House.

Nick is introduced by Max to young Jack Teller (Edward Norton), an ambitious thief who has infiltrated the Customs House and gained information regarding security by pretending to be an intellectually disabled janitor named Brian. It is not in Nicks nature to accept Jack, someone hes never worked with before. But he is talked into taking the job by a pleading Max, who is heavily in debt.

Nicks trusted associate Steven (Jamie Harrold) hacks into the Custom Houses security system to obtain the bypass codes, allowing them to temporarily manipulate the alert protocols of the system during the heist. Steven is caught, however, by a corrupt systems administrator who extorts Nick for $50,000 for the information. More complications arise when theyre forced to move up their timetable after the Customs House becomes aware of the true value of the sceptre and adds extra closed-circuit television cameras and infrared detectors to monitor the basement room while preparing to return it to its rightful owners.
 double crosses him and at gunpoint demands he hand over the sceptre. Nick reluctantly gives up the carrying case and seconds later the alarm, rigged by Jack, alerts the entire security staff to the heist. Nick darts for the sewer entrance he came in as Jack heads back upstairs, tucking the carrying case inside his janitor jumpsuit and slipping past the incoming police units responding to the burglary. Nick escapes the security guards chasing him through the sewer tunnels.

After making it to a bus station to flee the city, Jack calls Nick to gloat but is shocked to discover that Nick has anticipated Jacks actions. Jack opens the carrying case Nick gave him and finds it contains a steel rod weighed down with various bushings. Brushing off Jacks threats of vengeance, Nick advises Jack to flee as "every cop in the city" will now be looking for him. Nick hangs up and boards a boat with the real sceptre as a shocked Jack broods over his situation. Later, Max smiles as he watches a news broadcast reporting a massive manhunt being organized to find Jack, the prime suspect, and an unidentified accomplice. Nick then meets Diane at the airport as she returns from work, and they embrace.

== Cast ==
* Robert De Niro as Nick Wells
* Edward Norton as Jack Teller
* Marlon Brando as Max
* Angela Bassett as Diane
* Gary Farmer as Burt
* Jamie Harrold as Steven
* Richard Waugh as Sapperstien
* Jean-René Ouellet as André
* Paul Soles as Danny

== Production == Muppet whom Oz played from 1976 to 2001.   Oz later blamed himself for the tension and cited his tendency to be confrontational rather than nurturing in response to Brandos acting style.   Brando eventually refused to take direction from Oz and insisted that De Niro direct his scenes.   As a result, De Niro directed the rest of Brandos scenes.  "There was one scene - two days of shooting - when Marlon was too upset with me to act while I was on the set," Oz admits. "I watched from outside, with a monitor, and Bob was very good and acted as mediator between us." 

== Reception ==

=== Critical response ===
The film received generally positive reviews with a rating of 73% fresh on Rotten Tomatoes, based on 128 reviews with a rating average of 6.5/10.  The critical consensus was summed up as, "Though the movie treads familiar ground in the heist/caper genre, DeNiro, Norton, and Brando make the movie worth watching." 

Roger Ebert of the Chicago Sun-Times gave it three and a half stars, calling it "the best pure heist movie in recent years." 

Peter Travers, film critic for Rolling Stone, pointed out that when "two Don Corleones team up", he expected "the kind of movie that makes people say, Id pay to see these guys just read from the phone book." However, he concluded, "Theres nothing you cant see coming in this flick, including the surprise ending. Quick, somebody get a phone book." 

=== Box office ===
In its opening weekend, the film opened at #2 in the U.S. box office raking in $19,018,807 USD, behind Legally Blonde.

After its July 13, 2001 opening, the $68 million film earned a gross domestic box office take of $71,107,711. Combined with the international box office, the worldwide total is $113,579,918. 

=== Accolades ===
  
Angela Bassett won a NAACP Image Award for Outstanding Supporting Actress in a Motion Picture for her portrayal of Wells girlfriend, Diane. 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 