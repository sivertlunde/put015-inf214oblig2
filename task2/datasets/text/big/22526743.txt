El Sopar
{{Infobox Film name            = El Sopar image           =  caption         =  director        = Pere Portabella producer        =  writer          = Pere Portabella starring        = Ángel Abad Jordi Cunill Lola Ferreira Antonio Marín Narciso Julián music           =  cinematography  = Manel Esteban editing         =  distributor     =  released        = 1974 runtime         = 50 minutes language  Catalan
|budget          =
|}}
 Catalan and Spanish, by experimental filmmaker Pere Portabella.  The film takes place on the night of the execution of militant anarchist Salvador Puig Antich by Francisco Franco|Francos regime. Using simple cinematic conventions, Portabellas documentary involves five former political prisoners gathered in a farmhouse to prepare dinner and discuss the problems with long prison terms.   

To protect the films subjects from further persecution by Francos regime, the films production was coordinated in secrecy, with notices of the secret shooting location sent to technicians and participants at staggered times.     

==Specs==
*Produced by:
*Directors: Pere Portabella
*Scripts:
*Languages: Spanish, Catalan

==Sources==
*  at Pragda
*  at Film Society of Lincoln Center

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 


 
 