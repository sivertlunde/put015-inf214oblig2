Gumnaam – The Mystery
{{Infobox film
| name           = Gumnaam – The Mystery
| image          = Gumnaam Poster.jpg
| caption        = Poster of the film
| director       = Neeraj Pathak
| producer       = Shubir Mukherjee
| writer         = Neeraj Pathak
| starring       = Mahima Chaudhry Dino Morea Suman Ranganathan Irrfan Khan Rakhi Sawant Govind Namdeo Madan Jain
| music          = Nadeem-Shravan
| distributor    = Sony Music
| released       =  
| runtime        = 137 minutes
| country        = India
| language       = Hindi
}} 2008 Bollywood Hindi mystery film written and directed by Neeraj Pathak starring Dino Morea and Mahima Chaudhry in the lead roles.

==Cast==
*Dino Morea as Dev
*Mahima Chaudhry as Ria
*Suman Ranganathan as Remon
*Rakhi Sawant
*Govind Namdeo as Muni Gandhi
*Madan Jain as Rishi Gandhi
*Irrfan Khan
*Razzak Khan
*Arun Bali
*Bob Bhrambhatt

==Plot==
Gumnaam – The Mystery is the story of an aspiring actress Ria (Mahima Chaudhary) who models and acts in music videos. She has her eyes set on making it big in films as a lead actress. During one of her shoots, she meets Dev (Dino Morea), a stuntman, who saves her life during an accident on the set. Shes impressed with him; soon love blossoms between them and they are on a romantic high. Meanwhile Ria is offered a lead role in a film by casting director Rishi Gandhi (Madan) on the condition that she leaves for Shimla the next day. Dev is very happy about her getting the role and sees her off, wishing all the luck.

On reaching Shimla, she is introduced to the films director, Muni Gandhi (Govind Namdeo), who praises her beauty and appreciates his assistants perfect choice of casting her as the heroine. Slowly Ria begins to see some unusual things around her. The director asks her to give a screen test, but when she asks for a second take she is refused. His assistant Rishi Gandhi delivers the cassette to Remon (Suman Ranganathan), a rich woman in a huge mansion in Shimla. Who is this woman and why has the cassette been delivered to her? What happens after the screen test? How does Ria tackle the situation? Whats she doing in Shimla? All this forms the crux of the high voltage drama.

==Soundtrack==
The soundtrack was composed by Nadeem Shravan . Lyrics were penned by Sameer. The songs such as Naa Hone Dengie & Zaalim Ishq were chartbusters and others managed to get a decent response. 

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dhoke Mein"
| Alka Yagnik, Abhijeet Bhattacharya|Abhijeet, Sarika Kapoor
|-
| 2
| "Ishq Ne Kitna"
| Adnan Sami, Shreya Ghoshal
|-
| 3
| "Mohabbat Hai Tumse"
| Udit Narayan, Monica Nath
|-
| 4
| "Mohabbat Se Zyada"
| Udit Narayan, Shreya Ghoshal
|-
| 5
| "Naa Hone Denge"
| Alka Yagnik, Kumar Sanu
|-
| 6
| "Zaalim Ishq" Shaan
|}

==External links==
*  
*   at Bollywood Hungama

 
 
 
 
 


 