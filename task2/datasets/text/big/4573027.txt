Border Patrol (film)
{{Infobox film
| name           = Border Patrol
| image          =
| image_size     =
| caption        =
| director       = Lesley Selander
| producer       = Harry Sherman Michael Wilson William Boyd Andy Clyde Robert Mitchum Jay Kirby George Reeves Duncan Renaldo
| cinematography = Russell Harlan
| editing        = Sherman A. Rose
| distributor    = United Artists
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
}}
 Cisco Kid) plays the Mexican Commandant, Robert Mitchum plays "Quinn," one of the villains and George Reeves (the future TV Superman) plays "Don Enrique Perez," an accented Mexican captive working in the bad guys silver mine. The only female in the film is Claudia Drake as "Onez La Baroa."

==Plot== Texas Rangers who set out to find how 25 Mexicans have disappeared after being hired by the "Silver Bullets" mine. They ride into town and find that the mine owner is a one-man government, played by Russell Simpson as "Orestes Krebes". Hopalong and his friends are arrested on trumped-up charges and are tried before a kangaroo court and sentenced to swing but not until after lunch. With the help of the girl, they escape, free the captive mine workers and together defeat the evil gang.
==Cast== William Boyd	...	
Hopalong Cassidy 
Andy Clyde	...	
California Carlson 
Jay Kirby	...	
Johnny 
Russell Simpson	...	
Orestes Krebs 
Claudia Drake	...	
Inez La Barca 
George Reeves	...	
Don Enrique Perez 
Duncan Renaldo	...	
Commandant 
Pierce Lyden	...	
Loren 
Robert Mitchum	...	
Quinn (billed as Bob Mitchum) 
Cliff Parkinson...	
Barton 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 