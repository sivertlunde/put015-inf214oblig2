Varat Aali Gharat
{{Multiple issues|
 
 
 
}}
{{Infobox film
| name           = Varat Aali Gharat
| image          = Varat Aali Gharat.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Vijay Gokhale
| producer       = Krushnanand
| screenplay     =
| story          =
| starring       = Bharat Jadhav Alka Kubal Athalye Vijay Gokhle Neelam Shirke Anshuman Vichare Prajakta Hanmdhar Ajita Kulkarni Jaywant Bhalekar
| music          = Dyanesh Kumar
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Marathi
| budget         =
| gross          =
}}
Varat Aali Gharat is a Marathi movie released on 24 February 2009. The movie is produced by Krushnanand
and directed by Vijay Gokhale.  The movie is based on romantic comedy with various twists and love.

== Synopsis ==
Excited about becoming lawyers, two friends, Shama and Sushma begin to find husbands for each other. Meanwhile, on his first attempt at robbery, Narayan is caught and charged with murder by Suyash, a police inspector while Shama takes over the case as Narayan’s lawyer.
While Sushma secretly loves Suyash, she decides to hook her friend Shama and Suyash together. Shama thinks Narayan, a simple and honest man, is the apt choice for Sushma. However, Suyash’s widowed elder sister is hell-bent on getting him married as soon as possible. Suyash on the other hand isn’t interested in marriage as his ambition is to be like his commissioner, Kadam Saheb, who is still unmarried yet possesses fame and good money.

Unwilling to accept his refusal, Suyash’s sister approaches Kadam and requests him to somehow coax Suyash into getting married. Kadam promises to help her, while falling in love with her at first sight.

Kadam tries several tricks to make Suyash change his mind and to escape, Suyash lies to him that he is already married. Kadam now wants to meet his wife. Suyash is in a dilemma as he has to find somebody who would pose as his wife. Shama agrees to do so and what follows is a series of comic situations ending with Suyash marrying Shama, Narayan gets Sushma and Kadam too gets his sweetheart, Suyash’s sister!!

== Cast ==
The cast includes Bharat Jadhav, Klka Kubal Aathalye, Vijay Gokhle, Neelam Shirke, Anshuman Vichare, Prajakta Hanmdhar, Ajita Kulkarni, Jaywant Bhalekar & Others.

==Soundtrack==
The music is provided by Dyanesh Kumar.

== References ==
 

 
 
 


 