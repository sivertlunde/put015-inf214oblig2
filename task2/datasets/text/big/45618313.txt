A Thanksgiving Surprise
 
{{Infobox film
| name           = A Thanksgiving Surprise
| image          = A Thanksgiving Surprise.jpg
| caption        = A film still from the lost film
| director       =
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = Motion Picture Distributing and Sales Company
| released       =  
| runtime        =
| country        = United States English inter-titles
}}
 silent short short drama Thanksgiving dinner for those poorer than himself. His uncle see Jack has learned his lesson and secretly has a feast set while he sleeps. After the guest arrives, the uncle reveals himself and all ends well. The film was released on November 22, 1910 and was met with favorable reviews. The film is presumed lost film|lost.

== Plot == Thanksgiving feast. The guests arrive, their host awakes, uncle returns, and Jack is assured that his prosperity is real and not a dream." 

== Cast ==
*Marie Eline  likely as the newspaper boy.
*Frances Gibson is claimed to be the flower girl, but this is disputed.     William Russell   

== Production == Not Guilty. Nicholas Nickleby. George Middleton, Grace Moore, John W. Noble, Anna Rosemond, Mrs. George Walters. 

==Release and reception ==
The single reel drama, approximately 1,000 feet long, was released on November 22, 1910.    The drama film was sometimes labeled as a comedy by advertisers.  The film had a wide national release and was shown in theaters in North Carolina,  South Dakota,  Washington (state)|Washington,  Kansas,    Pennsylvania,  California,.  Though Hawaii was not yet a state, it was also shown as a new release in July 1912. 

The film was positively reviewed by critics. Walton of The Moving Picture News affirmed, stating that the film was a "...well-staged, naturally acted story with a lesson. There is pathos and stern reality mixed in with fidelity and anxiety, the whole making a story of entrancing and edifying power."  The Moving Picture World summarized the film and remarked, "The contrasts in the delineation of human nature constitute the principal points of interest, and they are worked out with full appreciation of dramatic varieties. The implied false friends have a good many counterparts, and the way they are depicted here is too graphic to be misunderstood."  The New York Dramatic Mirror took a swipe at Bison Pictures of the New York Motion Picture Company and the Powers Picture Plays by praising the camera posing of the film and acknowledging the acting of the flower girl to be well-portrayed. 

== References ==
 

 
 
 
 
 
 
 
 
 