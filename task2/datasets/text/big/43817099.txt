The Big Shot (1937 film)
 
{{Infobox film
| name           = The Big Shot
| image          = 
| alt            =
| caption        = 
| director       = Edward Killy Bob Barnes (assistant)
| producer       = Maury Cohen
| screenplay     = Arthur T. Horman Bert Granet
| story          = Lawrence Pohle Thomas Ahearn
| starring       = 
| music          = 
| cinematography = Nicholas Musuraca
| editing        = Jack Hively
| studio         =       
| distributor    = 
| released       =   }}
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

The Big Shot is a 1937 American comedy film directed by Edward Killy from a screenplay by Arthur T. Horman and Bert Granet, based on a story by Lawrence Pohle and Thomas Ahearn. The film stars Guy Kibbee, Cora Witherspoon, Dorothy Moore, and Russell Hicks. Produced and distributed by RKO Radio Pictures, the film premiered on July 23, 1937.

==References==
 

 
 
 
 
 
 


 