Threshold (1981 film)
{{Infobox film
| name           = Threshold
| image          = Threshold.jpg
| image_size     = 
| caption        =  Richard Pearce
| writer         = 
| narrator       = 
| starring       = Donald Sutherland
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       = 1981
| runtime        = 97 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Richard Pearce. It starred Donald Sutherland and Jeff Goldblum. The film was nominated for eight Genie Awards in 1983 and won three of them. Sutherland also won best actor at the 1982 Karlovy Vary International Film Festival for his performance. 

==Plot==

Respected cardiac surgeon Dr. Thomas Vrain performs a risky operation to provide a patient with the first artificial heart ever implanted in a human subject.

He and his colleague, research scientist Dr. Aldo Gehring, consider the risks and weigh the odds as time runs out for Carol Severance, the patient. Severance will die unless the experimental surgery is done quickly and succeeds.

==Cast==
* Donald Sutherland as Dr. Thomas Vrain
* Jeff Goldblum as Dr. Aldo Gehring
* Sharon Acker as Tilla Vrain
* Mare Winningham as Carol Severance
* John Marley as Edgar Fine
* Allan Nicholls as Dr. Basil Rents

==References==
 

==External links==
* 

 

 
 
 


 