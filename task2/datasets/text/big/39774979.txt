The Black Forest Girl (1933 film)
{{Infobox film
| name           = The Black Forest Girl
| image          = 
| image_size     = 
| caption        = 
| director       = Georg Zoch
| producer       = Harry Piel
| writer         = August Neidhart   Franz Rauch
| narrator       = 
| starring       = Maria Beling   Hans Söhnker   Walter Janssen   Kurt von Ruffin
| music          = 
| editing        = Erich Palme
| cinematography = Georg Bruckbauer   Ewald Daub 
| studio         = Ariel-Film 
| distributor    = Deutsch Lichtspiel-Syndikat
| released       = 30 November 1933
| runtime        = 
| country        = Germany
| language       = German 
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German operetta of the same title, composed by Leon Jessel with a libretto by August Neidhart. It was one of several film adaptations of the story.  It premiered in Stuttgart on 30 November 1933.

==Cast==
* Maria Beling as Bärbele 
* Hans Söhnker as Hans Fichtner, Student 
* Walter Janssen as Domkapellmeister Römer 
* Kurt von Ruffin as Richard Keßler, Student 
* Lotte Lorring as Malvine von Hainau 
* Olga Limburg as Die alte Traudel 
* Eugen Rex as Rutschke 
* Hans Sternberg as Der Ochsenwirt

==References==
 

==Bibliography==
* Braun, Rebecca & Marven, Lyn. Cultural Impact in the German Context: Studies in Transmission, Reception, and Influence. Camden House, 2010.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 