Nam Naadu (2007 film)
 
{{Infobox film|
| name = Nam Naadu
| image = Suresh
| producer = Singanamala Ramesh
| writer = Ramesh Khanna (dialogues)
| screenplay = Suresh Udayakrishna Udayakrishna-Siby Siby K. Thomas
| starring = Sarath Kumar, Karthika Mathew, Nassar, Vijayakumar (actor)|Vijayakumar, Sriman (actor)|Sriman, Ponvannan
| banner = Kanakarathna Movies
| music = Srikanth Deva
| cinematography = Y. N. Murali
| editing = G. Sasi Kumar
| distributor = Kanagarathna Movies
| studio = Kanagarathna Movies
| released =  
| runtime =
| country = India
| language = Tamil
}}

Nam Naadu ( ) is a 2007 Tamil film starring Sarath Kumar and Karthika Mathew in lead roles and is the remake of the Malayalam film Lion (2006 film)|Lion. The film received negative reviews and failed to replicate the success of the original. 

==Plot==
The movie starts with Aalavandhar (Nassar) a corrupt politician who holds the post as Education minister and wants to become the chief minister. He is supported by his son-in-laws, Sathya (Saranraj) IPS Officer who runs a finance company and Ilamaran (Ponvannan) IAS officer, the district collector. With the idea of his in-laws, Aalavandhar tries to disturb peace in state and make situation against the CM.

Muthazhagu (Sarath Kumar) an honest youth wing leader of the same ruling party learns and opposes the move of his father and he stands by and for the people. Sathya, Ilamaran and Aalavandhar allocates a land space in a village for a cool drink company. The village is rich in land water and the people oppose for the governments move against them. But these corrupt personalities issue an order for the start of the factory. Muthazhagu tries to meet the collector (Ilamaran) with the people of the locality and fails to get a reason for it. So he moves on to the court and with the courts order he gets a stay for the project.

In order to save and work for the people he plans to contest in the elections as an independent candidate. He stands opposite his father Aalavandhar. He wins the elections and extends his support to the party and gets a place in the ministry. He takes charge of the high profile department as HOME MINISTER.

Muthazhagu commands and passes orders to all the departments to work for the welfare of the public and he would take care of all the other issues within the departments. He threatens the CM and takes orders for effective governance.

Aalavandhar gets irritated by the moves of Muthazhagu and discusses the same with his in-laws. Sathya plans to wipe him off. This brings Aalavandhar to realize his misdoings in the past and meets Muthazhagu and confesses his mistake and asks him to return to their home. Muthazhagu accepts to return to their home along with his wife Gauri (Karthika Mathew). On an official call he goes to Delhi. Aalavandhar meets up in an accident ands passes away. Muthazagus mom thinks Muthazagu killed his dad and bans him from  the house. Muthazaghu finds that Sathya was the mastermind for the murder as his dad (Nassar) said to him about the public confession of their crimes. Muthazaghu killls his brother-in-law in the climax.

==Soundtrack==
*Kadhal Ennum - Murali, Yesudas
*Kottaisamy Varaaru - Manikka Vinayagam
*Manasil Manasil - Karthik, Chinmayee
*Vaazhaiyadi - Naveen

==Critical reception==
Sify wrote:"Sarath and director Suresh has kept the theme of politics as family business, and added a lot of heroism to the central character and made him a one-man fighting machine".  Kollywood Today wrote:"As a whole, taking the whole film into consideration, Nam Naadu is a film for a political cause and also a commercial film that can be watched to kill the time and not more than that". 

==References==
 

 
 
 
 
 


 