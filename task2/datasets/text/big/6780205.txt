Lone Star (1952 film)
{{Infobox film
| name           = Lone Star
| image          = Lone Star- Film Poster.jpg
| image_size     = 225px
| caption        = Theatrical Film Poster
| director       = Vincent Sherman
| producer       = Z. Wayne Griffin
| writer         = Borden Chase Howard Estabrook
| narrator       =
| starring       = Clark Gable Ava Gardner Broderick Crawford Lionel Barrymore Beulah Bondi Ed Begley
| music          = David Buttolph
| cinematography = Harold Rosson
| editing        = Ferris Webster
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 94 minutes
| country        = United States English
| budget         = $1.6 million  . 
| gross          = $3,922,000 
}} George Hamilton, playing beside Barrymore in the role of Jacksons servant.
 Western and a romance film|romance, set in Texas shortly before statehood.

==Plot==
Devereaux Burke gets a personal request from former President Andrew Jackson to help keep Texas from entering into annexation with Mexico. The movement is gaining favor because it is mistakenly believed that Texas pioneer Sam Houston supports it.

The opposition leader is wealthy rancher Thomas Craden, but when Dev is ambushed by Comanches, it is Craden who comes to his rescue. Martha Ronda, who loves Craden and runs the Austin newspaper, does not know Dev is anti-annexation when she and Craden host a number of senators at their home for dinner. When they wont all agree to vote his way, Craden then refuses them permission to leave.

Dev gets a signed letter from Sam Houston of his actual position, but the ink smears when he falls into a river, fleeing from Cradens men. He has difficulty persuading Martha, who publishes an incorrect story about Houstons position, but ultimately he wins her over and saves the day.

==Cast==
 
* Clark Gable as Devereaux Burke
* Ava Gardner as Martha Ronda
* Broderick Crawford as Tom Craden
* Lionel Barrymore as Andrew Jackson
* Beulah Bondi as Minniver Bryan
* Ed Begley as Anthony Demmet James Burke as Luther Kilgore
* William Farnum as Tom Crockett
* Lowell Gilmore as Captain Elliott
* Moroni Olsen as Sam Houston Russell Simpson as Maynard Cole
* William Conrad as Mizette

==Reception==
According to MGM records the film made $2,478,000 in the US and Canada and $1,444,000 elsewhere, resulting in a profit of $990,000. 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 


 