Me Ivan, You Abraham
{{Infobox film
| name           = Me Ivan, You Abraham
| image          = Moi Ivan, toi Abraham (film).jpg
| alt            = 
| caption        = 
| director       = Yolande Zauberman
| producer       = 
| writer         = Yolande Zauberman
| screenplay     = 
| story          = 
| based on       =  
| starring       = Roma Alexandrovitch Aleksandr Yakovlev Vladimir Mashkov
| music          = 
| cinematography = Jean-Marc Fabre
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France, Belarus
| language       = Yiddish
| budget         = 
| gross          = 
}}
 submission for consideration for Best Foreign Film at the 67th Academy Awards.

== Plot ==
In 1930s Poland Christian boy Ivan goes to live with a Jewish family to learn a trade. He becomes friends with Abraham, the son of the family. However, anti-Semitism is rife in their environment, and they flee to escape an upcoming conflict. Journeying together, they demonstrate their inseparability.

== Cast ==
*Roma Alexandrovitch - Abraham Aleksandr Yakovlev - Ivan
*Vladimir Mashkov - Aaron
*Mariya Lipkina - Rachel
*Hélène Lapiower - Reyzele
*Alexander Kalyagin - Mardoche
*Rolan Bykov - Nachman
*Zinovy Gerdt - Zalman
*Daniel Olbrychski - Stepan

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 