My Man (2014 film)
 
{{Infobox film
| name           = My Man
| image          = My_Man_Japanese_Film_Poster.jpg
| border         = 
| alt            = 
| caption        = The Original Japanese Poster.
| director       = Kazuyoshi Kumakiri
| producer       = 
| writer         = Takashi Ujita
| screenplay     = 
| story          = 
| based on       =  
| starring       = Fumi Nikaidō Tadanobu Asano Kengo Kora Jim ORourke
| cinematography = 
| editing        = Zenzuke Hori
| studio         = Nikkatsu
| distributor    = 
| released       =   
| runtime        = 129 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = ¥19.4 million (Japan)
}}

  is a 2014 Japanese romantic drama film directed by Kazuyoshi Kumakiri and based on Kazuki Sakurabas Watashi no Otoko novel.  It was released on 14 June 2014 in Japan.  The film won the Golden George at the 36th Moscow International Film Festival       and Tadanobu Asano won the award for Best Actor.    Fumi Nikaidō won International Rising Star Award for this film at New York Asian Film Festival  and Best Actress at 6th TAMA Film Awards.

==Story==
Young Hana had lost everything in the earthquake and tsunami and was being taken care of by a relative named Jungo. As Hana grew up she began to fall in love with her own relative.

==Cast==
*Fumi Nikaidō as Hana Kusarino
*Tadanobu Asano as Jungo Kusarino
*Kengo Kora as Yoshiro Ozaki
*Mochika Yamada as Hana (10 years old)

==Reception==
The film has grossed ¥19.4 million in Japan. 

==References==
 

==External links==
*   
* 

 
 

 
 
 
 
 
 
 