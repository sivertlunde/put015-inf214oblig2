The Stilwell Road (film)
 
 
{{Infobox film
| name = The Stilwell Road
| image = The Stilwell Road.jpg
| caption =
| director =
| producer = Office of War Information
| writer =
| narrator = Ronald Reagan
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released = 1945
| runtime = 50 minutes
| country = USA
| language = English
}}

The Stilwell Road was a propaganda film produced by the American Office of War Information and the British and Indian film units in 1945 detailing the creation of the Ledo Road, also known as the Stilwell Road after the U.S. General Joseph Stilwell. The movie was narrated by Ronald Reagan.

The film opens with a message telling the audience that the film is about more than the building of a road in the jungle; it is also the story of Scots, Irish, English, Americans, Africans, Indians, and Chinese working together to win the war, and that they are now ready to work together to win a peaceful post war world as well.
 Chinese nationalist Fourteenth Air Republic of China with supplies to resist Japanese domination, in much the same way Persian Corridor needed to be kept open to keep the USSR supplied by the Western Allies. But the forces there have to confront harsh terrain and monsoons, and are driven out in early 1942. Still, working in Assam and British India, the Allies work to keep supplies flowing to General Stilwell and Chiang Kai-Shek by making the Ledo Road which replaced the severed Burma Road.
 gradual retaking of Burma.

==See also==
*List of Allied propaganda films of World War II
*The Hump
*China Burma India Theater of World War II
*South-East Asian Theater of World War II
*Ronald Reagan films

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 