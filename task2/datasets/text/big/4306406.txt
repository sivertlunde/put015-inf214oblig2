My Best Friend Is a Vampire
{{Infobox film
| name           = My Best Friend Is a Vampire
| caption        = 
| image	         = My Best Friend Is a Vampire FilmPoster.jpeg
| director       = Jimmy Huston Dennis Murphy   Tab Murphy
| writer         = Tab Murphy
| narrator       =  David Warner   Rene Auberjonois
| music          = Steve Dorff
| cinematography = James Bartle
| editing        = Janice Hampton Gail Yasunaga
| distributor    =   Kings Road Entertainment
| released       =   (West Germany)   (United States)
| runtime        = 89 minutes
| country        = United States English
| gross          = $174,380
}} American Comedy gender roles, and adolescence.

==Plot==
Jeremy Capello (Robert Sean Leonard), is a typical American teenager fighting with the usual problem of getting himself a girlfriend. Although he has caught the eye of the head cheerleader Candy (LeeAnne Locken), he has his eyes on his classmate and band geek Darla Blake (Cheryl Pollak), but she in turn is unnerved by his constant staring at her.

Recently, Jeremy has been having some weird nightmares about a strange woman trying to seduce him, and later he actually encounters that woman named Nora (Cecilia Peck), who makes an obvious invitation to him, while delivering groceries. His skirt-chasing friend Ralph (Evan Mirand) convinces him to take up the opportunity for a first erotic experience. But the encounter goes badly: first the woman bites him in the neck, then two strangers burst into the house, forcing Jeremy to run for his life.

Next morning, Jeremy looks pale and does not feel well, and he sees in his fathers newspaper that the house of Noras has mysteriously burned down. Also, throughout the day he notices a strange man observing him. This man pops into his bedroom the very next night, introduces himself as Modoc (  to garlic, an increasing sensitivity to sunlight and a craving for blood slowly convince him otherwise. His new vampire "life-style" hampers his attempts to start a relationship with Darla, who has finally become interested in him; otherwise Jeremy begins to adapt to the minor impacts the change has brought to his life. Modoc even gives him a guide book and explains to him that vampires are just like any other "minority group" that has been persecuted over the centuries.

Slowly, Jeremys parents ( ) is determined to stop the "vampire armageddon" before it can start, with the help of his feeble assistant Grimsdyke (Paul Willson). They are in the process of tracking their newest victim, but due to a mix-up they believe that Ralph is the vampire.

One night, when Jeremy finally begins to exploit his new capabilities and wins back Darlas trust, McCarthy and Grimstyke kidnap Ralph and intend to "free his soul" in a small chapel. Jeremy and Darla just arrive in time to save him, but then Jeremy is recognized as a vampire, and only his new-found power of hypnotism and the timely arrival of Modoc and Nora, who has come back from the dead ("Of course. Im a vampire."), manage to save the day. Since McCarthy is unrelenting, however, Modocs female consorts turn McCarthy into a vampire, making a friend out of an enemy. 

The movie ends with Capellos assuring Jeremy that they love him and want to help him deal with his "problem". Jeremy then introduces Darla to his delightfully surprised parents, while Ralph just shakes his head at the whole hubbub.

==Production notes==
The film was shot in Houston, Texas and Los Angeles, California.

==Other titles==
This movie was released under the title I Was a Teenage Vampire in Australia.

==Cast==
* Robert Sean Leonard as Jeremy Capello
* LeeAnne Locken as Candy Andrews (credited as Lee Anne Locken)
* Cheryl Pollak as Darla Blake
* Cecilia Peck as Nora
* Fannie Flagg as Mrs. Capello
* Kenneth Kimmins as Mr. Capello
* Evan Mirand as Ralph
* Michelle La Vigne as Flo
* Harvey Christiansen as George David Warner as Professor McCarthy
* Paul Willson as Grimsdyke
* Rene Auberjonois as Modoc
* Erica Zeitlin as Gloria
* Gary Chason as Drivers Ed Instructor
* Kathy Bates as Helen Blake (credited as Kathy D. Bates)
* John Chappell as Buddy Blake
* Jill Bianchini as Waitress
* J.P. Conroy as Butcher
* Amelia Kinkade as Brunette in Punk Bar (credited as Mimi Kincaide)
* E. Linda Moore as Girl at Bar (credited as Linda Moore)
* Marianne Simpson as Blonde in Punk Bar
* Staness Caroll as Redhead in Punk Bar
* Ronnie Rondell Jr. as Bouncer (credited as Ronald P. Rondell)
* Chris Wycliff as Police Officer #1
* Coy Sevier as Police Officer #2

==See also==
*Vampire film

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 