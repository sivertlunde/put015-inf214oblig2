New Tale of Zatoichi
{{Infobox film
| name = New Tale of Zatoichi 新・座頭市物語
| director = Tokuzō Tanaka
| image	= New Tale of Zatoichi.jpg
| border = yes
| screenplay = Minoru Inuzuka
| story = Kan Shimozawa
| starring = Shintaro Katsu Mikiko Tsubouchi
| producer = 
| music = Akira Ifukube
| studio = Daiei Film
| distributor = Daiei Film
| cinematography = Chishi Makiura
| editing = 
| released =  
| runtime = 91 minutes
| country = Japan
| language = Japanese
| budget =
}}
The New Tale Of Zatoichi ( ) is a Japanese film, the third entry from the popular Zatoichi series completing the trilogy. The film is the first Zatoichi film to be in colour. 

==Plot==
Ichi travels to his old village four years after he stopped training. On the way he meets his childhood friend Tamekichi. Tamekichi, his wife and child have become poor, and must make a living with music. While at an inn, a group of travelers are robbed by four thieves but Ichi does not stop them.

The next day he calls out the thieves in front of the local clan leader with whom they are associated, saying he did not want to risk anyone getting hurt last night. The leader recognizes Ichi (and therefore must know his reputation), the masseur tells the travelers that the boss will reimburse them for more than what was taken.

Ichi travels on, and Kanbeis brother (Kanbei was the yakuza boss contracted to kill Ichi in the previous film whom Ichi killed) overtakes him. Intent upon revenge, their fight is interrupted by Ichis old master, Banno. Kanbeis brother says he will wait.

Banno has arranged a marriage of convenience between his 18 year old sister, Yayoi, and a samurai but she falls in love with Ichi. Meanwhile it becomes apparent that the master has become corrupt due to financial troubles and is associating with the Tengu gang.

Yayoi proposes marriage to Ichi and, after listing all his perceived faults, and her saying she has loved him for a long time finally relents, accepting her proposal and accedes to her request to become a new person. Kanbeis brother arrives and Ichi throws down his swordstick, keeping his promise to Yayoi not to fight, and asks for forgiveness and his life. Yayoi does likewise. The gangster at length agrees but stipulates a dice roll to determine the result. If he wins Ichi loses his right arm, if he loses he walks away and forgets about revenge. Although Ichi rolls a losing number the gangster tips over one die into a winning roll before he or Yayoi says anything. After he has gone Ichi tells Yayoi he knew he had lost.

Banno arrives and upon hearing Yayois request to marry Ichi throws him out of his house and tells him their teacher-student relationship is over. Ichi goes to his surrogate grandmothers house and, hearing Yayoi approach, sneaks out the back door, seeking Kanbeis brother to thank him. Kanbeis brother is in the same inn where Banno meets with the Tengu gang and arriving there in a foul mood he chases Kanbeis brother and stabs him in the back. Ichi arrives after Banno leaves and expresses sadness at the death of a good man and disdain for Bannos murder of an unarmed man.

Having learnt of a kidnapping, Banno intends to take the substantial 300 ryo ransom for himself. He goes into the forest accompanied by the kidnapped mans elderly father whom he kills in cold blood, an act witnessed by Yayoi who is seeking Ichi in the forest. Ichi encounters the Tengu gang in the forest and kills them all. Banno arrives and Ichi says he did this to help keep his teacher from doing wrong but Banno insists upon fighting. Yayoi watches as her brother is killed and Ichi cries. As he gets up to leave he addresses Yayoi saying it seems that he is "that kind of man" and as dawn breaks he wanders off alone.

==Cast==
* Shintaro Katsu as Zatoichi
* Mikiko Tsubouchi as Yayoi
* Seizaburō Kawazu as Banno
* Fujio Suga as Yasuhiko
* Mieko Kondo as Inn keepers wife
* Chitose Maki as Tames wife

==Availability==
The New Tale Of Zatoichi was released in 1963 and was made available on DVD on October 22, 2002. 

==Reception==
The film gathered two fresh reviews from Rotten Tomatoes and an 85% audience rating. Like the previous installments it spawned a number of popular sequels. 

==See also== List of original Zatoichi films
*Samurai cinema

==References==
 

==External links==
*  
* {{cite web
| url        = http://www.criterion.com/current/posts/2971-on-the-road-with-zatoichi
| title      = On The Road With Zatoichi
| last       = OBrien
| first      = Geoffrey
| date       = 	2013-11-25
| website    = The Criterion Collection
| accessdate = 2014-10-25
}}

 

 
 
 
 
 
 
 
 
 