Sailor Moon S: The Movie
 
 
{{Infobox film
| name = Sailor Moon S: The Movie
| image = Sailor Moon S.jpg
| caption = Japanese release poster
| director = Hiroki Shibata
| producer = Iriya Azuma
| screenplay = Sukehiro Tomita
| based on =  
| starring = Kotono Mitsuishi Masami Kikuchi Megumi Hayashibara Eiko Masuyama Keiko Han
| music = Takanori Arisawa
| cinematography = Motoi Takahashi
| editing = Yasuhiro Yoshikawa
| studio = Toei Animation
| distributor = Toei Company, Ltd.
| released =  
| runtime = 61 minutes
| country = Japan
| language = Japanese
| budget =
}}
Sailor Moon S: The Movie, known in Japan as   and Sailor Moon S the Movie: Hearts in Ice in the Pioneer English adaptation, is a 1994 Japanese animated film directed by Hiroki Shibata and written by Sukehiro Tomita. It is the second film in the Sailor Moon series. The film is adapted from a side story of the original manga series created by Naoko Takeuchi, The Lover of Princess Kaguya. The storyline is loosely based on "The Snow Queen" fairytale by Hans Christen Andersen.

The film was released on December 4, 1994 in Japan as part of the Winter 94 Toei Anime Fair.

==Plot==
 
An extraterrestrial entity named   arrives on Earth in an attempt to freeze it, but a fragment of her comet has been lost and she is unable to proceed without it. She has her henchwomen, the Snow Dancers, search for the missing fragment. In Tokyo, a young astronomer named   finds the fragment and keeps it in his observatory to study it further.
 Luna develops Princess Kaguyas existence. Later, after finding herself unable to reconcile her differences with Kakeru, Himeko leaves on a space mission.

The fragment of the comet attaches itself to his life force, and begins slowly stealing his life-force energy, causing him to become very ill. Kaguya later steals the shard, but because it is linked to his life-force, he is brought even closer to death when Kaguya throws the shard into the ocean and creates an enormous ice crystal that will continue to draw away Kakerus life force energy completely. She and her Snow Dancers then begin to freeze the Earth. The Sailor Soldiers attempt to stop her, but every time they kill the Snow Dancers, Kaguya keeps reviving them using the crystal. Just before Kaguya could kill the soldiers, Usagi tries to stop her and uses the Holy Grail/Purity Chalice to transform into Super Sailor Moon. She uses her Rainbow Moon Heartache attack but is easily overpowered by Kaguyas strength and power. Determined to protect the Earth, Usagi prepares to activate the Imperium Silver Crystals immense energy and power. The eight Sailor Soldiers along with Chibiusa, combine their Sailor powers and abilities all at once to activate the Legendary Silver Crystal, and it hits Kaguya head-on. The Silver Crystals power also destroys Kaguyas Snow Dancers, the ice crystal in the ocean, as well as her comet.

With peace once again returned to the Earth, Usagi wishes for Luna to become Princess Kaguya. Concerned about Himekos safety, Kakeru wanders in the snowstorm and is saved by Luna at the exact point Kakeru saved her, transformed into a beautiful human woman. She takes him near the moon, where Himeko, on her space mission, witnesses the phenomenon and realizes that Kaguya is real. Luna tells him that he needs to start focusing on his relationship with Himeko, and the two kiss. After returning to the Earth, Kakeru takes up Lunas advice and meets Himeko at the airport, where the two reconcile. Artemis meets up with Luna and the cats reconcile. 

==Cast==
 

{| class="wikitable"
|-
! Character name
! Japanese voice actor
! English voice actor (Pioneer/Optimum)
! English voice actor (Viz Media/Studiopolis)
|- Usagi Tsukino (Serena) || Kotono Mitsuishi || Terri Hawkes || Stephanie Sheh
|- Rei Hino Cristina Vee
|- Ami Mizuno (Amy) || Aya Hisakawa || Karen Bernstein || Kate Higgins
|- Makoto Kino Amanda Miller
|- Minako Aino (Mina) || Rica Fukami || Stephanie Morgenstern || Cherami Leigh
|-
| Chibiusa (Rini) || Kae Araki || Tracey Hoyt || Sandy Fox
|- Haruka Tenoh (Amara) || Megumi Ogata || Sarah Lafleur || TBA
|- Michiru Kaioh (Michelle) || Masako Katsuki || Barbara Radecki || TBA
|- Setsuna Meioh (Trista) || Chiyoko Kawashima || Sabrina Grdevich || Veronica Taylor
|- Mamoru Chiba (Darien) || Toru Furuya || Vincent Corazza || Robbie Daymond
|-
| Luna || Keiko Han || Jill Frappier || Michelle Ruff
|- Ron Rubin || Johnny Yong Bosch
|-
| Princess Snow Kaguya || Eiko Masuyama || Linda Ballantyne || TBA
|-
| Snow Dancers || Mariko Onodera Yūko Nagashima || Unknown || TBA
|-
| Kakeru Ōzora || Masami Kikuchi || Jeff Lumby  || TBA
|-
| Himeko Nayotake || Megumi Hayashibara || Jen Gould || TBA
|-
| Announcers || Tomohisa Asō Yasunori Masutani || Unknown || TBA
|- Yoshiyuki Kōno || Unknown || TBA
|}

==Production==
Sailor Moon S: The Movie is  based on the 135-page side story  , written and illustrated by series creator   antique named "Salome", while the Snow Dancers are modeled after a German china piece, which Takeuchi thought resembled "a character dancing in a snowstorm."    On July 8, 1994, she traveled to the  .

===English Release=== YTV and in the US on Cartoon Networks Toonami block on November 9, 2001. 
 DiC English cast reprising their roles. The edited version of the dub was censored for content and replaced the music with cues from the DiC version of the first two seasons of the anime. The uncut version of the dub was only seen on the billingual DVD, featured no censorship, and all of the original Japanese music was left intact, with the exception of the DiC theme song being used.

In 2014, the film was re-licensed for an updated English-language release in North America by Viz Media, who has plans to produce a new English dub of the film in association with Studiopolis in Los Angeles, CA and re-release it on DVD and Blu-ray.  It has also been licensed in Australia and New Zealand by Madman Entertainment. 

==Reception==
Animerica noted that the film incorporates aspects of the Japanese folklore   and   in the antagonists character. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 