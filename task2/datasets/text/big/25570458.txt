Manhattan Cocktail (film)
 
{{Infobox film
| name           = Manhattan Cocktail
| image          = Manhattan 1928.jpg
| caption        = theatrical poster
| director       = Dorothy Arzner
| producer       = Jesse L. Lasky Adolph Zukor
| writer         = Ethel Doherty (script) George Marion Jr. (titles) Ernest Vajda (story)
| starring       = Nancy Carroll Richard Arlen Lilyan Tashman Paul Lukas
| music          = Victor Schertzinger (songs "Gotta Be Good" and "Another Kiss")
| studio         = Paramount Pictures
| distributor    = Paramount Pictures 
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English 
}}
Manhattan Cocktail (1928) was a part-talkie film, directed by Dorothy Arzner, and starring Nancy Carroll, Richard Arlen, and Lilyan Tashman. At the time this movie was made, Hollywood was already making the transition of silent to sound, either making all talking movies, part talking movies, or silent movies with their own soundtrack and sound effects.

==Preservation status==
Manhattan Cocktail is a  .    

==Songs== 
*"Gotta Be Good" by Victor Schertzinger
*"Another Kiss" by Victor Schertzinger

==References==
 

==See also== 
*List of lost films
*List of incomplete or partially lost films
*Vitaphone 
*Sound-on-disc 

==External links==
* 
* 

 

 
 
 
 
 
 
 