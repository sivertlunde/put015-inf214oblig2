The Two Faces of January (film)
 
{{Infobox film
| name           = The Two Faces of January
| image          = The Two Faces of January film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Hossein Amini
| producer       = {{plainlist|
* Tom Sternberg
* Tim Bevan
* Eric Fellner
* Robyn Slovo
}}
| screenplay     = Hossein Amini
| based on       =  
| starring       = {{plainlist|
* Viggo Mortensen
* Kirsten Dunst
* Oscar Isaac
}}
| music          = Alberto Iglesias
| cinematography = 
| editing        = {{plainlist|
* Nicolas Chaudeurge Jon Harris
}}
| studio         = {{plainlist|
* StudioCanal
* Working Title Films
}}
| distributor    = {{plainlist| StudioCanal  
* Magnolia Pictures  
}} 
| released       =  
| runtime        = 96 minutes  
| country        = {{plainlist|
* United States
* United Kingdom
* France
}}
| language       = English Greek Turkish
| budget         = 
| gross          = $8.8 million 
}} thriller film directorial debut. 1964 novel studio in London. The film premiered in February 2014 in the Berlinale Special Galas section of the 64th Berlin International Film Festival.   

==Plot==
In 1962, con man Chester MacFarland and his wife Colette tour Greece and visit the Acropolis of Athens. There they meet Rydal Keener, who scams tourists while working as a tour guide. The MacFarlands invite Rydal to dinner, and Rydal, intrigued by the couples wealth and Colettes beauty, accepts their invitation, mentioning his girlfriend.   

Colette likes Rydal, but Chester doesnt trust him. After dinner, they part, but Rydal goes back to their hotel to return a bracelet Colette left in their shared taxi. Meanwhile a private detective hired by victims of Chesters investment swindles goes to the MacFarlands hotel room and demands that Chester repay their money. The detective pulls a gun, and Chester kills him in a brief struggle. While he is trying to stow the body in the detectives hotel room, Rydal finds him in the corridor. Chester asks Rydal for his help, pretending that he found the detective drunk and unconscious at the bar.

Rydal takes the MacFarlands to see a friend who can furnish false passports to replace those they left at their hotels front desk. He suggests waiting for the counterfeit documents on Crete. There they cannot check into a hotel without identification papers. They spend the evening at a restaurant where Chester gets drunk watching Rydal and Colette dance and grow close. They all sleep the night on the quayside. They travel by bus to Chania, where Colette visits Rydals room while her husband sleeps; it is left ambiguous whether they have sex. On the way back to Iraklion, Colette believes someone has recognized her from newspaper pictures of the Americans who fled the hotel in Athens, and runs off the bus at a stop. Chester and Rydal follow and together they walk to the ruins of Knossos.

When it begins to rain and they seek shelter, Chester lures Rydal into an underground labyrinth and knocks him out. As Chester tries to pull Colette up the stairs, she loses her balance and falls to her death. When Rydal comes to in the morning, he is seen leaving by a group of students and their guide. Chester has rushed to Iraklion to pick up the passports, paying Rydals friend $2,500. Rydal arrives in Knossos and tracks Chester down. The two realize they are bound together by the various roles in the detectives death, acquiring false passports, and Colettes death. If either is arrested he will implicate the other. 

After taking the ship to Athens, they go to the airport, where Chester pretends to buy them both tickets to Frankfurt. He says he is going for a drink and boards a plane to Istanbul, leaving Rydal with a suitcase containing documents that will tie him to Colette. Rydal realizes that Chester has probably reported him to the police anonymously and he flees the airport and appears to have escaped the police.

Rydal locates Chester in Istanbul and demands a meeting in the Grand Bazaar, threatening to go the police unless Chester pays him off. In fact, Rydal has been taken into police custody. The authorities have made him wear a wire and expect him to extract a confession from Chester. At their rendezvous Rydals insistent questioning makes Chester suspicious. Sensing a trap, he flees and a chase through the dark ensues, with both Chester and Rydal fleeing the police. A policeman shoots Chester who, as he lies dying, speaks into Rydals wire tap, admitting responsibility for the two deaths and exonerating Rydal. After Rydal is released, he goes to Chesters grave.

==Cast==
* Viggo Mortensen as Chester MacFarland 
* Kirsten Dunst as Colette MacFarland 
* Oscar Isaac as Rydal 
* Yigit Ozsener as Yahya 
* Daisy Bevan as Lauren
* David Warshofsky as Paul Vittorio

==Production== 1964 novel of the same name by Patricia Highsmith. Producer Tom Sternberg optioned the rights to the novel and originally set up a project with the production company Mirage. Sternberg developed the project with Amini and it found the backing by StudioCanal and Working Title.   
 Grand Bazaar in Istanbul. 

==Release== VOD on August 28, 2014, to be followed by a theatrical release on October 3, 2014.         

==Reception== The Talented Oedipal disturbance, turns up the sexual heat and smoothly increases the narrative torque."   Betsy Sharkey of the Los Angeles Times wrote, "As was the case in the book, there are moves that dont always make sense, but the game-playing is riveting." 

==References==
 

==External links==
*   (UK)
*   (US)
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 