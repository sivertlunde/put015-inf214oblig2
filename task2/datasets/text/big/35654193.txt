Montana (1950 film)
{{Infobox film
| name           = Montana
| image          = File:Montana - Poster.jpg
| image_size     =
| caption        = 1950 Theatrical Poster
| director       = Ray Enright
| producer       =
| writer         =
| narrator       =
| starring       = Errol Flynn Alexis Smith
| music          =
| cinematography =
| editing        =
| distributor    = Warner Bros.
| released       =  
| runtime        = 76 minutes
| country        = United States English
| budget         =
| gross          = $2.1 million (US rentals)  £131,969 (UK)  1,899,891 admissions (France) 
}}
 1950 Western film starring Errol Flynn. It was only the second time Flynn played an Australian on screen, the first time being Desperate Journey (1942). Tony Thomas, Rudy Behlmer & Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 166 

==Plot==
Morgan Lane, a sheepman, rides to Montana territory in 1871. He pretends to be a traveling salesman as he inquires how opposed local cattle interests are to sheep.

Staunchly opposed are rancher Maria Singleton and her fiance, Rod Ackroyd, and violently opposed is Slim Reeves, who challenges Morgan to a gunfight as soon as he learns what his real motives are.

Invited to dinner by Maria and Rod. Morgan makes a bet he can ride any animal on the ranch. Rod rigs the bet by not only putting Morgan on a wild stallion but slicing his stirrup.

Rod is having a romance behind Marias back with the sheriffs girlfriend. Morgan finally persuades Forsythe to permit sheep on his land, but Reeves kills him, and the sheriff gives Morgan a warning that Rod and Maria are planning to stampede cattle over his land and grazing sheep.  Instead it is Rod who is trampled to death. Maria at first continues to fight against Morgan, but eventually comes over to his side.

==Cast==
* Errol Flynn as Morgan Lane
* Alexis Smith as Maria Singleton
* Douglas Kennedy as Rod
* Ian MacDonald as Reeves
* Lester Matthews as Forsythe

==Production==
Warner Bros had made a number of Westerns named after famous American cities, including Dodge City, Virginia City and Santa Fe Trail. In June 1940 they announced they were making a Western Montana, based on C.B. Glasscocks book "The War of the Copper Kings". Warners List New Pictures: Studio Will Produce 48 Feature-Length Films During 1940-41
Los Angeles Times (1923-Current File)   10 June 1940: A3.   WARNERS TO ISSUE 48 FEATURE FILMS: Action Called Keynote of the 1940-41 Production Plans, According to Executive WAR DRAMAS SCHEDULED Story of Sergeant York and Lost Battalion to Be Made --Starring Vehicles Set Two Films for Bette Davis Edward G. Robinson to Star Other Plans Are Listed
New York Times (1923-Current file)   10 June 1940: 20.   In August 1941 Warner Bros announced that Errol Flynn would be appearing in the Technicolor Western Montana in 1941-42. Warner Brothers Chalk Up Impressive Slate for 1941-42
The Washington Post (1923-1954)   11 Aug 1941: 9.   However Americas entry into World War Two appeared to delay the production. 

After the war it was reported Eagle Lion wanted to make a Western called Montana starring Joel McCrea. DRAMA AND FILM: Joel McCrea Offered Star Role in Montana William Jacobs producer and Thames Williamson to work on the script; Errol Flynn was listed as a possible star. DRAMA AND FILM: Role Tailored to Ava; Drama Claims Celeste
Schallert, Edwin. Los Angeles Times (1923-Current File)   15 July 1947: A3.   The film became officially part of Warners 1948 schedule. Most Ambitious Year Planned at Warner Studio
Los Angeles Times (1923-Current File)   07 Jan 1948: A3. 

Flynns casting was not definite - at various times Gary Cooper and Ronald Reagan were announced to play the lead.   Errol Flynn had appeared in a number of popular Westerns for Warner Bros but disliked the genre. In 1949 he  told Hedda Hopper:
 Acting for me is sheer fun. Theres only one thing I really dont want to do any more and thats Westerns. I guess Ive trod every back trail and canyon pass in the entire west. Ive never literally had to read the line, they went that a-way pard, but there is one cliche Ive said so many times it comes back to me in all my nightmares. Every time theres a gap in the story, every time the writers dont know what to do next, they have me pull up ahead of my gang, assume a decidedly grim look, and say All right men, you know what to do now. The fact is Ive made so many of these things, scripts seem so much the same, that what it adds up to in my mind is that the studio says, Heres a horse. Get on. Flynn and Dandy: LOOKING AT HOLLYWOOD WITH HEDDA HOPPER
Chicago Daily Tribune (1923-1963)   29 May 1949: C4.   
However his last few films for Warner Bros had struggled to recoup their costs and he was cast in Montana. Ray Enright assigned to direct and filming began August 1948, after Flynn had just completed a three month boat trip on the Zaca. Letter From Hollywood By Frank Daugherty Special to The Christian Science Monitor. The Christian Science Monitor (1908-Current file)   03 Sep 1948: 5. 

The film reportedly started shooting with the script only half ready.  James R. Webb and Charles G Booth were credited as working from a story by Ernest Haycox. 

Little, if any, of the film was shot in Montana.

==Reception==

The Los Angeles Times said the film "wont set the cinema world on fire but its solid Western entertainment." Pair Battle and Romance in Montana
Scott, John L. Los Angeles Times (1923-Current File)   21 Jan 1950: 11.   The Washington Post called it "a fair enough hour and a quarter." Flynn Shows Cattle Lords In Montana
By Orval Hopkins Post Reporter. The Washington Post (1923-1954)   09 Feb 1950: B11.  

==References==
 

==External links==
* 

 

 
 
 
 
 