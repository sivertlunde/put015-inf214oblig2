Six Bullets
 
 
{{Infobox film
| name           = Six Bullets
| image          = Six Bullets DVD.png
| director       = Graham Killeen
| producer       = 
| writer         = Graham Killeen
| starring       = Steven Koehler Rachel Ogi James Iaquinta Lori Minnetti
| music          = 
| cinematography = Jon Kline
| distributor    = 
| released       =  
| runtime        = 27 minutes
| country        = United States
| language       = English
| budget         = US$20,000
}}
Six Bullets is an independent short western film written and directed by Graham Killeen.  The films narrative centers on a family torn apart by the work of an evil demon.  It was Killeens directorial debut and is credited with earning him the "Best Local Filmmaker" award from the Milwaukee Newspaper Shepherd Express in 2006.  Signal Fire Films funded the film and is currently submitting it to the American film festival circuit. 

== Production ==
The first draft of the script was completed by Killeen in Wisconsin in January 2004.  Various rewrites, preproduction meetings, and funding meetings took up the bulk of Killeens time until filming began in earnest in May 2005 and concluded in November 2006.  Filming was primarily in Southeastern Wisconsin, but some second unit photography was completed in South Dakota and Minnesota.  The earliest versions of the film were audience tested in June 2006, after which the film was drastically re-edited and completed in February 2007.  Special visual effects were created by Little Fish Digital Studios in Wisconsin, and audio was completed by Brandaudio in Minnesota. 

== Cast ==
(partial list) 
Father - Steven Koehler 
Daughter - Rachel Ogi 
Mother - Amanda Shalhoub 
Clown - Lori Minnetti 
Witch - Donna Gawell-Mueller 
Twin #1 - Ben Boyd 
Twin #2 - Richard Sheski

== Soundtrack and score ==
The original score to Six Bullets was composed by the electronica band Endless Blue and produced by Nick Mitchell, with additional support and musicians from Minneapolis, Minnesota. The score explores the traditional "downtempo" feel of previous Endless Blue recordings, but is infused with less typical instruments, including acoustic guitar, Native American rhythm instruments, strings, and piano.  The soundtrack CD of the movie was released in April 2007. 

== Festivals and awards ==

The film has been featured at a number of film festivals:

{| class="wikitable"
|+Festivals and Awards
|-
! Date !! Festival and Award
|-
| 10 July 2007 || NYC Downtown Short Film Festival
|-
| 21 July 2007 || Pencil Heads Dusk Til Dawn Film Fest - Best Action/Adventure Film
|-
| 15 September 2007 || Independents Film Festival
|-
| 28 October 2007 || B-Movie Film Festival
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 