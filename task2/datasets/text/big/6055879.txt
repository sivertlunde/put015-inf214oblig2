Hong Kong Godfather
 
 
{{Infobox film
| name           = Hong Kong Godfather
| image          = HongKongGodfather.jpg
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 衝擊天子門生
| simplified     = 冲击天子门生
| pinyin         = Chōng Jí Tiān Zǐ Mén Shēng
| jyutping       = Cung1 Gik1 Tin1 Zi2 Mun4 Sang1 }}
| director       = Ho Cheuk Wing
| producer       = Chan Chi Ming
| writer         = 
| screenplay     = Nam Yin
| story          = 
| based on       = 
| narrator       = 
| starring       = Andy Lau Roy Cheung Tommy Wong Lung Fong Yu Li Joey Wong
| music          = Jim Sam
| cinematography = Ray Wong
| editing        = Kwok Ting Hung
| studio         = Wang Fat Film Production Golden Princess Amusement
| released       =  
| runtime        = 94 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$8,799,652
}} Hong Kong action crime film directed by Ho Cheuk Wing starring Andy Lau.

==Summary==
When the leader of the Hung Hing syndicate is framed for the murder of a high ranking cop, he goes into hiding and leaves the syndicate in the hands of his sons with his youngest son Koo Siu York (Andy Lau) taking his place. A rival Triad branch is looking to move in on their territory and a war soon erupts between the two groups with a newly appointed CID detective (Roy Cheung) caught in the middle.

==Cast==
*Andy Lau as Koo Siu York
*Roy Cheung as Inspector Leung Chun Pong
*Tommy Wong as Michael Koo Wu
*Lung Fong as Fred
*Yu Li as Jenny Lam
*Joey Wong as Mrs. Leung Chun Pong
*Alan Tang
*Lo Lieh as Detective Sam Lam
*Jason Pai as Mark
*Tom Wu

 {{Cite web |url=http://www.imdb.com/title/tt0103085/ |title=Hong Kong Godfather 
 |accessdate=30 July 2010 |publisher=imdb.com}} 
   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 

 