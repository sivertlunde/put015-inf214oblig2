A Million to Juan
{{Infobox Film
| name           = A Million to Juan
| image          = Milliontojuancover.JPG
| caption        = VHS Artwork
| producer       = Executive Producer: Mark Amin Gary Binkow Producers: Barry L. Collier Steven Paul
| director       = Paul Rodriguez
| writer         = Screenplay: Robert Grasmere Francisca Matos Story: Mark Twain
| starring       = Paul Rodriguez Tony Plana Cheech Marin
| cinematography = Bruce Douglas Johnson
| editing        = Michael Ripps Jack Tucker
| music          = Jeffrey Johnson Steven Jae Johnson
| studio         = Prism Entertainment Corp.
| distributor    = The Samuel Goldwyn Company
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
}}
A Million to Juan is a 1994 romantic comedy film starring comedian Paul Rodriguez. This was also his directorial debut. The story is a modern spin on Mark Twains story "The Million Pound Bank Note".

==Plot==
The story begins with a narration by Alejandro Lopez as he retells the events of his fathers life, Juan Lopez (Paul Rodriguez), and how by selling oranges he changed their lives.

Juan was born on a strawberry field in Bakersfield, California, but due to hardship, his mother decided to relocate to Mexico. Now Juan is an undocumented citizen, due to no proof of US citizenship, of Los Angeles, struggling not only to get a green card, but to solely care for Alejandro, after his wifes death.
 oranges near the freeway.  He lives with two roommates and tries to make ends meet so he can take care of his little son. He is stressed as he battles landlords and immigration.

A stranger (Edward James Olmos) in a fancy limousine hands over $1,000,000 dollar check to Juan, but under the condition that he must give back all the money in one month.

Juan is suspicious and takes the check to his immigration worker (Polly Draper) who encourages him to follow the directions given him.

At first he uses the check to get credit extended at posh clothing stores, a car dealership, and more. He also meets a woman in a dead-end relationship with a bossy businessman.

Then the fun begins for good-natured Juan Lopez, who has to avoid temptations and the greedy people that suddenly pop-up in his life.

Juan comes to realize that the true meaning of life is love, family,  and happiness, and that money isnt the answer.

==Cast==
 
 
* Paul Rodriguez as Juan Lopez
* Tony Plana as Jorge
* Bert Rosario as Alvaro
* Polly Draper as Olivia Smith
* Larry Linville as Richard Dickerson
* Maria Rangel as Anita
* David Rasche as Jeff
* Edward James Olmos as The Angel
 
* Pepe Serna as Mr. Ortiz
* Gerardo Mejía as Flaco
* Liz Torres as Mrs. Delgado Paul Williams as Jenkins
* Cheech Marin as Shell Shock
* Rubén Blades as Bartender
* Jean Kasem as Party Guest
 

==Reception==

===Critical response===
The staff at Variety (magazine)|Variety magazine gave the film a mixed review, writing, "The odds of A Million to Juan breaking out of its inherent niche-market appeal can be summed up in its title. This gentle rags-to-riches tale set in the Los Angeles barrio is a good-natured parable that, unfortunately, doesnt pack much commercial punch. Its positive intentions arent enough to cross over into the mainstream...While Rodriguez adheres to the movie dictum of happy endings, his mix of message/mirth is too soft and mushy to reach a contemporary crowd." 

Film critic Marc Savlov also gave the film a mixed review, writing, "...comedian Paul Rodriguez has taken a laudable step in the right direction with his directorial debut. Unfortunately, its no masterpiece...Rodriguezs comic sensibilities are usually razor-keen, but here, blunted by a cliché-riddled storyline and scattershot direction, they seem nonexistent. 

===Boxoffice===
The film opened on May 15, 1994 in the United States on a limited release.

After one week the film went to video.  Box-office sales the only week in circulation were $381,457 in 181 theatres.   However, IMDb reports $1,221,832 in box-office receipts.

==Soundtrack==
A  , Marc Anthony, Aramis Camilo, Marcos Loya, Carla De Leon, John Pena, and others.

==See also==
 
* The Million Pound Note (1953) film starring Gregory Peck.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 