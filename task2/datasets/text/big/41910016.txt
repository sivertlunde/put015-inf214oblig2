Stadium Nuts
{{Infobox film
| name           = Les Fous du Stade
| image          = 
| alt            = 
| caption        = 
| director       = Claude Zidi
| producer       = Christian Fechner
| writer         = Claude Zidi Jacques Fansten
| starring       = Gérard Rinaldi Jean Sarrus Gérard Filipelli Jean-Guy Fechner Paul Préboist Martine Kelly
| music          = Gérard Rinaldi Jean Sarrus Gérard Filipelli Jean-Guy Fechner 
| cinematography = Paul Bonis
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Les Fous du Stade ( ) is a 1972 French comedy film directed by Claude Zidi.

==Plot==
The foursome(Gérard Rinaldi, Jean Sarrus, Gérard Filipelli, Jean-Guy Fechner) are on a holiday. The Little Olympic flame is to be passed through their village. A grocer (Paul Préboist) calls upon them for help in decorating the village. On their job Gérard falls for the grocers daughter Délice (Martine Kelly). However she runs away with the sportsman with the flame. The four then enter the Little Olympics to try to win her back and cause havoc in the process.

==Cast==
* Gérard Rinaldi - Gérard 
* Jean Sarrus - Jean 
* Gérard Filipelli - Phil 
* Jean-Guy Fechner - Jean-Guy 
* Paul Préboist - Jules Lafougasse
* Martine Kelly - Délice
* Gérard Croce - Lucien
* Jacques Seiler - The director of the Cyclists
* François Cadet
* Pierre Gualdi

==References==
 

==External links==
*  

 

 
 
 
 


 
 