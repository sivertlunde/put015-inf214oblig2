The End of the Road (1936 film)
{{Infobox film
| name           = The End of the Road
| image          = 
| image_size     = 
| caption        = 
| director       = Alex Bryce John Findlay
| writer         = Edward Dryhurst 
| narrator       = 
| starring       = Harry Lauder   Ruth Haven   Ethel Glendinning   Bruce Seton
| music          =  Jack Parker
| editing        = 
| studio         = Twentieth Century Fox 
| distributor    = Twentieth Century Fox 
| released       = December 1936
| runtime        = 72 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} musical comedy comedy drama film directed by Alex Bryce and starring Harry Lauder, Bruce Seton, Ruth Haven and Ethel Glendinning.  It was made at Wembley Studios by the British subsidiary of the Hollywood company Twentieth Century Fox. 

==Plot== concert party is thrown out by him.

==Cast==
* Harry Lauder as John MacGregor 
* Ruth Haven as Sheila MacGregor 
* Ethel Glendinning as Jean MacGregor 
* Bruce Seton as Donald Carson 
* Margaret Moffatt as Maggie 
* Campbell Gullan as David 
* Vera Lennox as Flo 
* Johnnie Schofield as Jock 
* Tully Comber as Alan Cameron

==References==
 

==Bibliography==
* Wood, Linda. British Films, 1929-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 