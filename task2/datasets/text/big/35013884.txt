Marrabenta Stories
{{Infobox film
| name           = Marrabenta Stories
| image          = Marrabenta Stories.jpg
| caption        = Screenshot
| director       = Karen Boswall
| producer       = Contracosta Produções Catembe Produções Íris Imaginações
| writer         = 
| starring       = 
| distributor    = 
| released       = 2004
| runtime        = 52 minutes
| country        = Mozambique Portugal
| language       = Portuguese
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = Carlos Vieira Emmanuel Leus
| editing        = Orlando Mesquita
| music          = 
}}

Marrabenta Stories is a 2004 documentary film directed by Karen Boswall.  A musical documentary, it covers Marrabenta, the national music of Mozambique.   

== Synopsis ==
Young musicians from Mozambique, who usually play Jazz, Funk and Hip Hop, join a group of old men who are stars of the Marrabenta, the traditional Mozambique folk music. Together they form a band called Mabulu and mix their different music styles. The “Old Glories”, as they are affectionately called by their fans, still live in Maputo and survive, as they have for the last fifty years, by singing songs that describe the sad and funny details of their everyday lives.

==Release==
The film was showcased at numerous film festivals including DocLisboa in Portugal, Tarifa in Spain, Dockanema in Mozambique, the Durban International Film Festival in South Africa, Africa in the Picture in the Netherlands and the Afrika Filmfestival in Belgium. 

== References ==
 
 

==External links==
* 

 
 
 
 
 
 
 


 
 