Ten Days' Wonder (film)
{{Infobox film
| name           = La Décade prodigieuse
| image_size     = 
| image	=	La Décade prodigieuse FilmPoster.jpeg
| caption        = A poster with the films English title: Ten Days Wonder
| director       = Claude Chabrol
| producer       = 
| writer         = Paul Gégauff Eugène Archer Paul Gardner Ellery Queen (novel)
| narrator       = 
| starring       = Michel Piccoli Anthony Perkins Orson Welles
| music          = 
| cinematography = Jean Rabier
| editing        = 
| distributor    = 
| released       = 1971
| runtime        = 110 minutes
| country        = France
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Ten Days Wonder ( ) is a French murder-mystery film directed by Claude Chabrol and based on the novel Ten Days Wonder by Ellery Queen.
 The Trial in 1962.

==Cast==
*Anthony Perkins as Charles Van Horn
*Michel Piccoli as Paul Regis
*Marlène Jobert as Helene Van Horn
*Orson Welles as Theo Van Horn
*Guido Alberti as Ludovic
*Ermanno Casanova as One-Eyed Old Man
*Mathilde Ceccarelli as Receptionist

==External links==
* 

 

 
 
 
 
 
 
 


 
 