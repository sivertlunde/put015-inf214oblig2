Mirchi (film)
 
 
{{Infobox film
| name           = Mirchi
| image          = Mirchi Poster.jpg
| caption        = Film poster
| director       = Koratala Siva
| producer       = V. Vamsi Krishna Reddy   Pramod Uppalapati
| writer         = Koratala Siva Adithya Menon Brahmanandam
| music          = Devi Sri Prasad Madhi
| editing        = Kotagiri Venkateswara Rao
| studio         = UV Creations
| distributor    = Great India Films  (Overseas)  
| released       =  
| runtime        = 150&nbsp;minutes
| country        = India
| language       = Telugu
| budget         =     
| gross          =  (Nett) 
}}
 masala written Adithya Menon and Nadhiya in pivotal roles. The film was tentatively titled Vaaradhi, but was later renamed to Mirchi.  The movie was a blockbuster and Prabhas became the 5th tollywood hero to enter the 40cr club. Kailash Kher won Filmfare Award for Best Male Playback Singer - Telugu award for the song "Pandagala Digivacchavu" . The film was dubbed into Hindi as Khatarnak Khiladi.

==Plot==
The movie begins with Jai(Prabhas) practicing guitar when a girl runs to him and asks him to save her from a gang of goons chasing her. Then Jay, without fighting, resolves the conflict. The girl introduces herself as Manasa (Richa Gangopadhyay). They slowly  become friends. But one day she asks Jai to leave her and go as she cannot stand the separation from him if their relationship develops any further. He then goes back to India and changes the mind of Manasas brother and when they give a vacation he goes with him to his village. There, where everyone are conservative in nature, he changes their nature and makes them more lovable.

When Manasa expresses her love for him, he tells his flashback. It is then revealed that he was born to their rivals family. His father (Sathyaraj) wanted to convert the people of his village. But his mother didnt want to stay there, so she left him. Later when Jay goes there, he falls in love with Vennela (Anushka Shetty).

He starts taking revenge on the Manasas family without revealing his identity. Then they decide to marry Jai with Vennela because they both are in love with each other. During the marriage the inspector tells his father that the enmity is rekindling because his son is taking revenge on the rivals. Soon afterwards, the rivals come and start killing everyone. After the fight it is known that Jais mother dies. His father banishes him and blames her death on him. The story is now back to the present. Manasas uncle challenges Jay that if he can defeat his men then he too would follow non violence. After successfully defeating them, her uncle asks Jay that if he could defeat the rivals son (not knowing that it was Jai himself) he would marry Manasa to Jai.

Jai gets angry at his stubbornness and reveals his true identity. He starts fighting with Manasa"s uncle. Then all her family members come and convert him too. This was watched by his father who had just arrived there. His father welcomes him back to the family. The film ends with him reuniting with Vennela.
   

==Cast==
  was selected as lead heroine marking her second collaboration with Prabhas.]]
* Prabhas as Jai
* Anushka Shetty as Vennela
* Richa Gangopadhyay as Manasa
* Sathyaraj as Deva, Jais father
* Nadhiya as Latha, Jais mother
* Brahmanandam as Veera Pratap
* Sampath Raj as Uma, Main Antagonist Adithya Menon as Babai (Jais Uncle) Subbaraju as Poorna, Manasas brother
* Rakesh Varre
* Satyam Rajesh as Nalla Nagulu, Jais cousin brother
* Srinivasa Reddy
* Nagineedu, Big brother of Uma
* Raghu Babu, Family aide of Deva
* Benarji as Father of Manasa
* Supreeth as Karimulla, Main aide of Uma Hema
* Karthik
* Mamilla Shailaja Priya
* Hamsa Nandini in item number
* Moulika as Brahmanandam Sister-in-Law

==Production==

===Casting=== Anushka and Richa Gangopadhyay. The father and mother characters in the film are being played by Tamil actors Sathyaraj and Nadhiya. 

===Filming===
Principal photography for the film began on 1 December 2011 at Sardar Vallabhbhai Patel National Police Academy|S. V. P. National Police Academy establishment in Hyderabad.    Filming resumed on 16 January 2012 at a house set at Kokapet on the outskirts of Hyderabad. The set was built and previously used for S. S. Rajamoulis Maryada Ramanna.  The filming continued till end of January with action Sequences for climax episodes were canned.    After one month, filming resumed at Saradhi Studios in Hyderabad. Director canned few scenes involving Supreeth, Kota Srinivasa Rao and Prabhas. 
In March 2012, filming was done at Ramoji Film City with lead actors.  Filming resumed from 17 May in RFC where action sequences were canned on a railway station set.  A new schedule began from 17 June in Hyderabad where important scenes on lead actors were shot.  After the schedule, filming resumed in Pollachi where scenes on Prabhas, Anushka and Richa were canned.  It was reported that the unit would move to Italy to shoot few songs on the lead actors.   The film completed its Europe schedule on 14 October, where couple of songs were shot.  The unit members of the film shifted to Tenkasi for shooting action scenes on 24 October.  The next schedule of the film was progressed at Ramoji Film City picturising a song "Suno Senorita" on Prabhas and Richa Gangopadhyay.   The Small Schedule of the movie was canned at Kutralam in Tamil Nadu on Prabhas, Anushka Shetty and on few lead Cast members.  Ramajogayya Sastry visited the shooting spot of Mirchi at Annapurna Studios where a song was canned on Prabhas and Anushka Shetty with Dance Master PremRakshit.  The visual effects of the movie was done by Light Line Entertainments in which a foreign location was created visual.

==Release== Censor Board. The movie released worldwide on 8 February 2013 amidst high expectations as it is the biggest outing of Prabhas.  Great India Films is releasing Mirchi in 176 centers from 7 February in USA alone, which is by far one of the best and humongous release. The movie has beaten the record of Seethamma Vakitlo Sirimalle Chettu (SVSC) and Naayak, which had released in 69 and 54 screens, respectively, in the country. Mirchi is coming up with all digital screening in USA to ensure that it will have superb quality screens across USA. 

===Pre-release revenues=== Maa TV. Mirchi had a pre-release revenue up to   by music and satellite rights. The films Hindi satellite rights were sold for     

===Marketing===
On 21 October 2012 the First Look posters and photos was released to the market in the view of the Prabhas Birthday on 23 October.  Mirchi movie teaser was scheduled for launch on 18 November 2012.  The first teaser of Mirchi movie was released in YouTube on 18 November 2012. The teaser got viral response across web and it has raised the expectations among the fans and movie buffs.  Its DVD, Blu-ray, VCD was released by Sri Balaji Videos on 21 May 2013  at the event of 100&nbsp;days of the film.

==Reception==
===Critical reception===
Jalapathy Gudelli gave the film 3.25 stars out of 5, concluding his review that "Mirchi showcases Prabhas in different avatar. Neat package of masala moments and Prabhas are the strengths. Mirchi is a decent entertainer and time pass flick".  Jeevi gave a rating of 3.25/5, commenting that "First half of Mirchi is decent. Flashback episode which occupies 100% of second half is good. Climax could have been better. The plus points of the film are Prabhas, music and all-round orientation. On a whole, Mirchi has all the ingredients of a commercial potboiler".  Mahesh S Koneru gave his verdict that "At the Box Office, the film will turn in a good commercial performance", giving the film a score of 3.25/5.  Way2movies also gave a rating of 3.25/5, giving the verdict that "Prabhas rocks the show in this decent yet formulaic commercial entertainer".  Super good movies gave the film 3 stars in a scale of 5, reviewing that "Go and Watch Mirchi, It entertains both Class and Mass. It is a worth watch". 
 NDTV Movies gave the film a mixed review, stating that "apart from good direction and strong performances by lead actors, Mirchi lacks narrative power and is loosely based on an amalgamation of some old films, which makes the film quite predictable." 

===Box office===
Mirchi opened strong and collected a nett gross of about   in 100 days. 

==Soundtrack==
{{Infobox album 
| Name = Mirchi
| Caption = Album cover
| Artist = Devi Sri Prasad
| Type = Soundtrack
| Cover =
| Released =  
| Recorded = 2012–2013
| Genre = Film soundtrack
| Length = 24:03
| Label = Aditya Music
| Producer = Devi Sri Prasad
| Last album = Alex Pandian (2012)
| This album = Mirchi (2013)
| Next album = Iddarammayilatho (2013)
}}
Devi Sri Prasad composed the Music for this film. The audio launch  was held on 5 January 2013 at Ramanaidu Studios in Nanakramguda at Hyderabad.  Krishnam Raju attended the audio launch function as chief guest, released the audio CD and handed over the first piece to Rajamouli. Dil Raju, Shyam Prasad Reddy released the trailers on this occasion. {{cite web|url=http://www.apherald.com/Movies/ViewArticle/12362/High-lights-at-Prabhas-‘Mirchi’-audio-release|title=High lights at Prabhas ‘Mirchi’ audio release
|publisher=APHerald.com |accessdate= 5 January 2013}} 

{{tracklist
| headline        =
| extra_column    = Artist(s)
| total_length    = 24:03
| all_lyrics      = Ramajogayya Sastry

| title1          = Mirchi
| extra1          = Chinna Ponnu
| length1         = 01:23

| title2          = Yahoon Yahoon
| extra2          = Mika Singh
| length2         = 04:40

| title3          = Idhedho Bagundey
| extra3          = Vijay Prakash, Anitha
| length3         = 04:26

| title4          = Pandagala
| extra4          = Kailash Kher
| length4         = 04:51

| title5          = Barbie Girl
| extra5          = Jaspreet Jasz, Suchitra
| length5         = 03:57

| title6          = Nee Choopula
| extra6          = Kailash Kher
| length6         = 00:58

| title7          = Darlingey
| extra7          = Devi Sri Prasad, Geetha Madhuri
| length7         = 03:44
}}

===Soundtrack reception===
AP Herald gave a review stating "Devi Sri Prasad started this year giving chart-buster songs to music lovers, this album surely stands as one of the best audio in Prabhas films." 

== Remakes == John Abraham, actor turned producer has acquired the Hindi remake rights of Mirchi from the makers. John told PTI,"I saw the film and loved it a lot. We have bought the rights of the film and are quite excited to remake it here in Hindi. I would be acting in the film too. The cast and crew members are not yet finalized.    From South, Kannada actor, director and producer Sudeep directed and acted in the Kannada remake of Mirchi.The film was named as Maanikya. Sudeep roped V. Ravichandran
for an important role in the remake. 
It is being remade in Bengali as Bindaas (2014 film)|Bindaas with Dev (actor)|Dev, Srabanti Chatterjee and Sayantika Banerjee.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 