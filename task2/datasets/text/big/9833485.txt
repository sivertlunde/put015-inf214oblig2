Story of Women
{{Infobox film
| name           = Story of Women
| image          =StoryOfWomen.jpg
| image_size     = 
| caption        = DVD cover
| director       = Claude Chabrol
| producer       = Marin Karmitz
| writer         = Colo Tavernier Claude Chabrol Francis Szpiner (book)
| narrator       = 
| starring       = Isabelle Huppert François Cluzet Nils Tavernier
| music          =  
| cinematography = Jean Rabier
| editing        = Monique Fardoulis
| distributor    = MK2 Diffusion (France) New Yorker Films (United States|USA)
| released       =  
| runtime        = 108 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Story of Women ( ) is a 1988 French drama film directed by Claude Chabrol based on the true story of Marie-Louise Giraud, guillotined on July 30, 1943, for having performed 27 abortions in the Cherbourg area, and the book by Francis Szpiner.
 John Waters, annual selection within the 2008 Maryland Film Festival.

==Plot==
Under the German military administration in occupied France during World War II. Paul Latour is a prisoner of war in Germany and his wife Marie lives hand-to-mouth with their two children in a squalid flat. A neighbour, whose husband is also in Germany, has fallen pregnant and is trying to lose the baby. Marie helps her, successfully. Other women come to her and she starts charging.

Then her husband returns, but she will not resume relations. Although he cannot find work, he rents a bigger flat so that the two can have some privacy. Marie not only has more space for her illicit business but lets a prostitute use a room during the day. She also starts a daytime affair with a collaborator.

When one of the abortions goes wrong, the woman dies and her despairing husband commits suicide. Marie shrugs off the tragedy and hires a maid to help, suggesting that part of her duties can be to sleep with Paul. In despair, he sends an anonymous denunciation to the police.
 Vichy régime, determined to enforce morality and stop population decline, has made abortion a treasonable crime. Marie is condemned to death and guillotined.

==Selected cast==
*Isabelle Huppert as Marie
*François Cluzet as Paul
*Nils Tavernier as Lucien
*Marie Trintignant as Lulu/Lucie
*Lolita Chammah as Mouche #2
*Aurore Gauvin as Mouche #1

==Awards== Sant Jordi Kansas City Film Critics Circle Award for Best Foreign Film and was nominated for a Golden Globe for Best Foreign Language Film.

==See also==
* Isabelle Huppert filmography
* 1988 in film
* Cinema of France
* French films of 1988

== References ==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 

 