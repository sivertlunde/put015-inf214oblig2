Ennavale
{{Infobox film
| name           = Ennavale
| image          =
| director       = J. Suresh
| writer         = J. Suresh Madhavan Sneha Sneha Manivannan Charle
| producer       = S. Naga Ashok Kumar N. V. Prasad
| banner         = Sri Saideva Productions
| cinematography = Ashok Rajan
| music          = S. A. Rajkumar
| editing        = V. Jaishankar
| distributor    = Sri Sai Deva Productions
| released       = December 22, 2000
| runtime        =
| country        = India Tamil
}} Madhavan and Sneha in the lead roles. The film also featured Manivannan, Thalaivasal Vijay and Charle in pivotal roles, while music was composed by S. A. Rajkumar. The film released on December 22, 2000 to poor reviews from film critics.

==Plot==
Madhavan and his three friends, Charlie, Vyapuri and Venu, have a music troupe that sing at weddings. They are tenants of Manivannan whose daughter Sneha shares a good rapport with the youth, helping them in their time of need. Her encouragement puts a new zeal in Madhavan, who soon gains recognition as a singer. Not that this brings any change in the life style of the group, for the director still shows them as strugglers. Madhavan finally proposes to Sneha who rejects him, as she has her past to reckon with. She is a divorcee, her husband having abandoned her soon after marriage for his girlfriend. When friend and neighbour Ashwini puts some sense into Sneha, she decides to make amends. Incidentally, Ashwini had been hovering in the background doing almost nothing until she gets this scene to justify her presence in the film.  But misunderstandings pile up between the lovers - the situations are all forced here - till the director leads the story to a desired happy ending.

==Cast== Madhavan as James Vasanthan Sneha as Lakshmi
*Manivannan Subramani, Lakshmis father
*Kalairani Lakshmis mother
*S. N. Lakshmi Lakshmis grandmother Charlie as Ganeshan
*Vaiyapuri as Charles Venu Madhav as Mohan
*Thalaivasal Vijay as neighbour
*R. V. Aswini as Seetha, neighbours wife
*Venu Arvind as Kumar Anand as Anand, Lakshmis first husband
*Pandu as first house owner

==Production== Sneha made Venu Madhav also made his debut in Tamil films with the project. The film was shot across locations in Chennai, Ooty, Hyderabad and Gopichettipalayam and all scenes were shot within 18 days. 

==Release==

 

==Soundtrack==
{{Infobox album|  
 Name = Ennavale
| Type = soundtrack Tips
| Artist = S. A. Rajkumar Feature film soundtrack
| Last album = 
| This album = Ennavale (2001)
| Next album =  
}}
{| class="wikitable" style="width:50%;"
|-
! Song title !! Singers   
|- Hariharan
|-
| "Chinna Chinna Sugangal" || S. P. Balasubramanyam, Krishnaraj
|-
| "Kothu Kottai" || Sukhwinder Singh
|-
| "Ovvoru Padalilum" || Unnikrishnan
|-
| "Ra Ra Rajakumara" || Rajesh Krishnan, Sujatha Mohan
|-
| "Ovvoru Padalilum" (sad) || Unnikrishnan
|}

==References==
 

==External links==
* 
* 

 
 
 
 