A River Made to Drown In
 
{{Infobox film
| name = A River made to Drown In
| image =ARiverMadeToDrownIn.jpg
| caption =DVD cover
| director = James Meredino credited as:  Alan Smithee
| producer =
| writer = Paul Marius Richard Chamberlain Ute Lemper   James Duval  Austin Pendleton
| music =
| cinematography = Thomas L. Callaway
| editing = Esther P. Russell
| distributor =Picture This! Entertainment
| released = 1997
| runtime = 98 minutes
| country = United States
| language = English
| budget =
| preceded_by =
| followed_by =
}}
 Richard Chamberlain, Ute Lemper and James Duval. Directed by James Merendino, Merendino had his name removed and the film is credited to Alan Smithee. A River Made to Drown In failed to secure US domestic theatrical distribution, but was released on DVD and has appeared on here! (TV network)|here! television.

==Plot synopsis==
 street hustler. He has left that life behind and is involved with Eva (Lemper) his art dealer. His life is upset by the arrival of Thaddeus (Chamberlain), a wealthy lawyer near death from AIDS. Allen had for a time been Thaddeuss "kept boy" and Thaddeus has come to Allen to die. Before he dies, however, he wants Allen to help him find Jaime (Duval), another young hustler who had taken Allens place with Thaddeus. Thaddeus is worried that hes infected Jaime with HIV and says he wants to care for him financially.

The film follows Allens efforts to fulfil Thaddeuss final wishes while struggling to maintain his relationship with Eva and avoid the temptation of being drawn back into his former life on the streets.

==Cast==
  Richard Chamberlain as Thaddeus MacKenzie
* Michael Imperioli as Allen Hayden
* Ute Lemper as Eva Kline
* James Duval as Jaime
* Austin Pendleton as Billy Steve Tyler as Bartender
* Talia Shire as Jaimes Mother Mike Starr as Frank
* Richard Riehle as Heavyset Man
* Michael Saucedo as Luis
* James Karen as Ray
* Michael Kearns as Arthur
* Michael OHagan as Jack
* Lewis Arquette as Vagabond
* Paul Marius as Turk
* Jon Powell as Martin
 

==DVD release==
A River Made to Drown In was released on Region 1 DVD on July 1, 2003.

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 