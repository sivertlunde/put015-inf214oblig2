Rumba (1935 film)
{{Infobox Film
| name           = Rumba
| image_size     = 
| image	=	Rumba FilmPoster.jpeg
| caption        = 
| director       = Marion Gering
| producer       = William Le Baron
| writer         = Guy Endore (story) Howard J. Green
| narrator       = 
| starring       = George Raft Carole Lombard Gail Patrick
| music          = Francois B. de Valdes
| cinematography = Ted Tetzlaff
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 71 min.
| country        = United States English
| budget         = 
}}
 musical drama Cuban dancer and Carole Lombard as a Manhattan socialite. The movie was directed by Marion Gering and is considered an unsuccessful follow-up to Raft and Lombards smash hit Bolero (1934 film)|Bolero the previous year.

==Plot==
Famed dance team, Veloz and Yolanda were uncredited choreographers for this film, while the signature rumba danced near the end by Raft and Lombard seems to be a simplified variation of rumba performed by Veloz and Yolanda themselves the year before in another Paramount film starring George Burns and Gracie Allen, "Many Happy Returns."

==Cast==
*George Raft as Joe Martin
*Carole Lombard as Diana Harrison
*Lynne Overman as Flash Margo as Carmelita
*Gail Patrick as Patsy Fletcher
*Iris Adrian as Goldie Allen
*Monroe Owsley as Fletcher Hobart
*Jameson Thomas as Jack Solanger
*Soledad Jimenez as Maria
*Paul Porcasi as Carlos
*Samuel S. Hinds as Henry B. Harrison
*Virginia Hammond as Mrs. Harrison
*Ann Sheridan as a Chorus Girl (uncredited)
*Akim Tamiroff as Tony (uncredited)
*Jane Wyman as a Chorus Girl (uncredited)

==Reception==
The film was a box office flop. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 55 
==References==
 
== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 

 
 