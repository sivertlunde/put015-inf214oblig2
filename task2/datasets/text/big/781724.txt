Tom & Viv
{{Infobox film
| name           = Tom & Viv
| image          = Tomandviv.jpg
| caption        = Theatrical poster for the film Brian Gilbert
| based on       =  
| screenplay     = Michael Hastings Adrian Hodges
| starring       = Willem Dafoe Miranda Richardson Rosemary Harris
| music          = Debbie Wiseman
| cinematography = Martin Fuhrer
| distributor    = Miramax Films
| released       =  
| runtime        = 115 minutes
| country        = United Kingdom United States
| language       = English
| budget         = 
| gross          = $538,534
}} Brian Gilbert, which tells the story of the relationship between the American poet T. S. Eliot and his first wife, Vivienne Haigh-Wood Eliot. They were married in 1915 after a brief courtship, and separated in 1933 though they never divorced. 
 Michael Hastings, Best Actress Best Actress in a Supporting Role (Rosemary Harris).

==Cast==
* Willem Dafoe ... Tom Eliot
* Miranda Richardson ... Vivienne Haigh-Wood Eliot
* Rosemary Harris ... Rose Haigh-Wood
* Tim Dutton ... Maurice Haigh-Wood
* Nickolas Grace ... Bertrand Russell
* Geoffrey Bayldon ... Harwent
* Clare Holman ... Louise Purdon
* Philip Locke ... Charles Haigh-Wood
* Joanna McCallum ... Virginia Woolf
* Joseph OConor ... Bishop of Oxford
* John Savident ... Sir Frederick Lamb
* Michael Attwell ... W. I. Janes
* Sharon Bower ... Secretary
* Linda Spurrier ... Edith Sitwell
* Roberta Taylor ... Ottoline Morrell
* Christopher Baines ... Verger
* Anna Chancellor ... Woman

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 