How Is Your Fish Today?
{{Infobox film
| name           = How Is Your Fish Today?
| image          = HIYFT-poster.jpg
| caption        = Promotional poster
| director       = Xiaolu Guo
| producer       = Xiaolu Guo Jess Search
| writer         = Xiaolu Guo Hui Rao
| starring       = Xiaolu Guo Ning Hao Hui Rao Zijiang Yang Matt Scott
| editing        = Emiliano Battista
| distributor    = Forward Entertainment
| released       =  
| runtime        = 83 minutes
| country        = China
| language       = Mandarin
}}
How Is Your Fish Today?, also known as Jin Tian De Yu Zen Me Yang?, is a 2007 Chinese film written by Xiaolu Guo and Hui Rao.  It was directed by Xiaolu Guo. The film is a drama set in modern China, focusing on the intertwined stories of two main characters; a frustrated writer (Hui Rao) and the subject of his latest work, Lin Hao (Zijiang Yang). How Is Your Fish Today won 4 international awards and was well received by critics, but was not commercially successful.

==Plot== The Fugitive. Upon reading Raos arthouse script, the producer rejects it as the worst screenplay he has ever read. However, Rao does not abandon his story, instead he rewrites it. Rao begins to live through his main character, Lin Hao, as he writes about him fleeing his home on a journey of self-discovery. Hao makes his way to Mohe County|Mohe, as does Rao, and the writer enters his own narrative. Both characters, in a struggle for freedom, have left their homes behind, but while Lin Hao is running away, Hui Rao is searching for something.

==Cast==
* Hui Rao as himself.
* Zijiang Yang as Lin Hao.
* Xiaolu Guo as Mimi.
* Ning Hao as Hu Ning.

==Reception==
How Is Your Fish Today? was consistently given good ratings by reviewers, but still remains fairly unpopular. 

===Critics===
On its release, How Is Your Fish Today? was received well by critics, who applauded the film as an impressive debut from Guo. 

===Awards===
* Guo was awarded the "Grand Prix" at the 2007 Créteil International Womens Film Festival.
* Guo was given a Special Mention at the 2007 Fribourg International Film Festival.
* Guo was given a Special Mention at the 2007 Pesaro International Film Festival of New Cinema. 
* Guo was given a Special Mention at the 2007 Rotterdam International Film Festival. 
* Guo was nominated for the "Tiger Award" at the 2007 Rotterdam International Film Festival.
* Guo was nominated for "Grand Jury Prize" in the World Cinema/Dramatic categories at the 2007 Sundance Film Festival.

==References==
 

==External links==
*  site for Independent Lens on PBS 
*  
*  

 
 
 