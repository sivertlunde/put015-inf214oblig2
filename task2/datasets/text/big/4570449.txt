Shaadi Se Pehle
{{Infobox film
| name           = Shaadi Se Pehle
| image          = Shaadi Se Pehle.jpg
| director       = Satish Kaushik
| producer       = Subhash Ghai
| writer         = Sanjay Chel
| narrator       =
| starring       = Akshay Khanna Mallika Sherawat Ayesha Takia Aftab Shivdasani Sunil Shetty
| music          = Himesh Reshammiya
| cinematography = Johny Lal
| editing        = Sanjay Verma
| studio         =
| distributor    = Mukta Arts Ltd
| released       = 7 April 2006
| runtime        = 128 mins
| country        = India
| language       = Hindi
| awards         =
| budget         =
}}
Shaadi Se Pehle ( : شادی سے پہلے   Bollywood comedy film directed by Satish Kaushik and produced by Subhash Ghai. The film stars Akshay Khanna, Ayesha Takia, Mallika Sherawat, Sunil Shetty and Aftab Shivdasani in lead roles. It released on 6 April 2006, and was declared a critical and commercial failure.

==Plot==
It was love at first sight for unemployed Ashish Khanna and Rani Bhalla at a cold drink stall. When Rani introduces Ashish to her businessman dad, Tau, and mom, they unanimously frown upon this alliance, until Ashish promises to get a job, save enough money, buy a flat, etc.. Ashish does get a small job with Izzat Papads, and then also starts selling steroids out of the back of a van. He gets noticed by an Ad-firm owner, and gets hired. Soon he has all the luxuries that he had always dreamed of. When the Bhallas come to know about this, they decide to welcome him as their son-law, and a lavish engagement party takes place. It is then Ashish finds out that he has cancer, and he begins to create misunderstandings between him and Rani. He tries becoming an alcoholic, but that is considered normal with the Bhalla family; he confesses to Rani that he has loved Sania, a model with ad-agency, which is welcomed by Rani as being truthful and honest. But when Rani notices that he has put up Sanias photos all over his apartment, it is then that she breaks off the engagement. Ashish knows that Sania will not marry him, as she is a flirt. But when he is assigned to go to Malaysia with Sania, that is where he finds out that Sania is not quite a flirt, but wants to marry him. She even introduces him to her brother, Anna, who instantly approves of the match. Now Anna inducts Ashish into the family business - which is extortion, and a stunned Ashish is trained to be use guns and defend himself. When the time comes for marriage, Ashish confesses that he has cancer. An angered Anna summons Dr. Rustom, who in turn gives a clean health chit, and confirms that Ashish had overhead a conversation about another patient and had misunderstood. A hapless Ashish, fresh out of lies, must now resign himself to getting married to Sania - or else be hunted down and killed by Anna and his gang.

This movie is loosely inspired from an old Hindi movie, Meri Biwi Ki Shaadi starring Amol Palekar.

==Cast==
* Akshaye Khanna as Ashish Khanna
* Ayesha Takia as Rani Bhalla
* Mallika Sherawat as Sania
* Sunil Shetty as Anna
* Aftab Shivdasani as Rohit Chopra
* Rajpal Yadav as Kanpuri
* Boman Irani as Dr. Rustam
* Anupam Kher as
* Gulshan Grover as Luca (Don)
* Mita Vasisht as Ashishs sister
* Vijayendra Ghatge as Mr. Bhalla
* Kishori Shahane as Mrs. Bhalla
* Dinesh Hingoo as Undertaker (Cemetery caretaker)
* Hemang Tanna as Kakkad

==Reception==
The film received negative reviews from critics overall. However the public audiences moderately enjoyed the film and its comical humour, whilst the critics claimed the humour to be too old, and used. At the box office, it had an "above average" opening, and grossed Rs.11,50,00,000 only throughout its full theatrical run. The film was ultimately declared a flop by Boxofficeindia.com. 

==Soundtrack==                               ==Singers==     
# Bijuriya                              Alka Yagnik & Sukhwinder Singh   
# Tere Liye                             Alka Yagnik & Udit Narayan
# Sache Aashiq                          Alka Yagnik & Sukhwinder Singh
# Mundeya                               Sunidhi Chauhan
# Ankhiyon Se Gal Kar Gayi              Sukhwinder Singh                 
# Tutiya Ve                             Daler Mehndi
# Tere Liye (remix)
# Bijuriya (remix)
# Tutiya Ve (remix - DJ Suketu - Arranged by Aks)
# Mundeya (remix)
# Ankhiyon Se Gal Kar Gayi (remix)

==References==
 

==External links==
*  

 
 

 
 
 
 