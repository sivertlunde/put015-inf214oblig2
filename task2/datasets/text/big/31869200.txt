Beau Brocade (film)
 
 
{{Infobox film
| name = Beau Brocade
| director = Thomas Bentley
| starring = Mercy Hatton Charles Rock Austin Leigh
| based on =  
| released =  
| country = United Kingdom
}}
Beau Brocade is a 1916 British silent adventure film directed by Thomas Bentley and starring Mercy Hatton, Charles Rock and Austin Leigh.  In eighteenth century Britain a disgraced gentlemen becomes a highwaymen. It is adapted from the novel Beau Brocade by Baroness Emmuska Orczy.

==Cast==
* Mercy Hatton - Lady Patience
* Charles Rock - Sir Humphrey Challoner
* Austin Leigh - Jack Bathurst
* Cecil Mannering - Lord Stretton George Foley - John Stitch
* Cecil Morton York - Matterchip
* Frank Harris - Jock Miggs
* Harry Brayne - Duffy
* Kitty Arlington - Betty

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 
 