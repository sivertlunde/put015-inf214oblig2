A Matter of Resistance
{{Infobox film
| name           = A Matter of Resistance
| image          = La Vie de Château poster.jpg
| caption        = Film poster
| director       = Jean-Paul Rappeneau
| producer       = Nicole Stéphane
| writer         = Jean-Paul Rappeneau Alain Cavalier Claude Sautet Daniel Boulanger
| starring       = Catherine Deneuve Pierre Brasseur Philippe Noiret Henri Garcin
| music          = Michel Legrand
| cinematography = Pierre Lhomme
| editing        = Pierre Gillette UGC
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French English
| budget         =
}}
A Matter of Resistance ( ) is a 1965 French romantic comedy film co-written and directed by Jean-Paul Rappeneau, and starring Catherine Deneuve, Pierre Brasseur, Philippe Noiret and Henri Garcin.  It received the Louis Delluc Prize in 1965. 

==Cast==
* Catherine Deneuve as Marie
* Philippe Noiret as Jérôme
* Pierre Brasseur as Dimanche
* Mary Marquet as Charlotte
* Henri Garcin as Julien
* Carlos Thompson as Klopstock
* Marc Dudicourt as Schimmelbeck
* Robert Moor as Plantier, the gardener Donald OBrien as The American Officer  
* Jean-Pierre Moulin as Lieutenant
* Paul Le Person as Roger

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 