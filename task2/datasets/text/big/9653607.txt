Los caballeros de la cama redonda
{{Infobox film
| name           = Los caballeros de la cama redonda
| image          = Caballeros de la cama redonda.jpg
| image_size     =
| caption        =
| director       = Gerardo Sofovich Hugo Sofovich
| producer       =
| writer         = Gerardo Sofovich Hugo Sofovich
| narrator       =
| starring       = Jorge Porcel and Alberto Olmedo
| music          = Jorge López Ruiz Óscar López Ruiz
| cinematography = Américo Hoss
| editing        = Oscar Montauti
| distributor    = Aries Cinematográfica Argentina
| released       = March 22, 1973
| runtime        = min.
| country        = Argentina Spanish
| budget         =
| preceded_by    =
| followed_by    =
}}
 Argentine comedy film directed and written by Gerardo Sofovich and Hugo Sofovich. The film starred Jorge Porcel and Alberto Olmedo.

==Release==
The film premiered in Argentina on March 22, 1973.

==Cast==
*Jorge Porcel
*Alberto Olmedo
*Chico Novarro
*Tristán
*Adolfo García Grau
*Mimí Pons
*Mariquita Gallegos
*Haydée Padilla
*Elida Marchetta
*Marcos Zucker
*Carmen Morales
*María Rosa Fugazot
*Fidel Pintos
*Eloísa Cañizares
*Délfor
*Moria Casán
*Tita Coello
*Marcia Bell
*Hector Doldi
*Javier Portales

==External links==
* 

 
 
 
 
 

 
 