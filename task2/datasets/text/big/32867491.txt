Drømmeslottet
{{Infobox film
| name           = Drømmeslottet
| image          =
| caption        = 
| director       = Petter Vennerød Svend Wam
| producer       = 
| writer         = Petter Vennerød Svend Wam
| narrator       =
| starring       = Øyvin Berven Birgitte Victoria Svendsen Lasse Lindtner Mari Maurstad Petter Vennerød Hilde Grythe
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          = 
}} Norwegian drama film written and directed by Petter Vennerød and Svend Wam, starring a large ensemble cast including Lasse Lindtner and Mari Maurstad. The film is part of a trilogy by the directors, where the other two instalments were titled Åpen framtid (1983) and Adjø solidaritet (1985). Three couples who have known each other since early youth decide to move in together, but soon run into difficulties.

==External links==
*  
*  

 
 
 
 


 
 