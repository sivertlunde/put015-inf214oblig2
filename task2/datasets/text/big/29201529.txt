Press (film)
{{Infobox film
| name           = Press
| image          = PressFilmPoster.jpg 
| alt            =
| caption        = Theatrical Poster
| director       = Sedat Yılmaz
| producer       = 
| writer         = Sedat Yılmaz
| starring       = Aram Dilbar   Engin Emre Değer   Kadim Yaşar   Asiye Dinçsoy   Bilal Bulut   Tayfur Aydın   Mahmut Gökgöz
| music          = 
| cinematography = Demir Gökdemir
| editing        = 
| studio         =
| distributor    =
| released       =  
| runtime        = 100 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
}}

Press is a 2010 Turkish drama film directed by Sedat Yılmaz, which tells the story of six employees at the Diyarbakır office of Turkeys first Kurdish language daily newspaper. The film was selected for the 47th Antalya "Golden Orange" International Film Festival.   

The English word was used in the original title of the film because of the double meaning of the word, both describing the work done and the pressure on the journalists.

== Production ==
Director Sedat Yılmaz has stated, I am not Kurdish, but, I wanted to tell this story out of solidarity with the Kurdish people and with Gündem newspaper. Gündem is a very important newspaper in the press tradition of Turkey. Maybe the real meaning of its importance will be understood even later.

== Plot ==
It is the early 1990s, and a handful of journalists are trying to attract the world’s attention to the ongoing human rights abuses in Diyarbakır. Eighteen year-
old Firat opens up and cleans the newspaper office everyday, and helps with distribution. While researching the disappearance of five villagers, journalist Kadir discovers the traces of a paramilitary gang. In addition to struggling with usual technical difficulties of a small operation, the newspaper is subjected to various attempts to intimidate them and prevent their courageous reports from coming to light.

Following increased threats and attacks, Firat volunteers to guard the office at night, and by day he tries to learn the trade. When the newspaper’s distribution to Diyarbakır is blocked, the team has to come up with new ways to reach their readership.

== Release ==

=== Premiere ===
The film premiered in competition at the 47th Antalya "Golden Orange" International Film Festival on  , where some of the audience protested the Kurdish-speaking actor and left the hall. 

=== Festival screenings ===
* 47th Antalya "Golden Orange" International Film Festival (October 9–14, 2010)

==See also==
* Turkish films of 2010

==References==
 

 
 
 
 
 