The Mystery of Betty Bonn
{{Infobox film
| name =  The Mystery of Betty Bonn
| image =
| image_size =
| caption =
| director = Robert A. Stemmle
| producer =  Ernst Krüger    Hans Herbert Ulrich   Georg Witt
| writer =  Friedrich Lindemann (novel)   Ernst Hasselbach    Robert A. Stemmle 
| narrator = Hans Nielsen   Erich Ponto
| music = Herbert Windt   
| cinematography = Otto Baecker   
| editing =       
| studio = Georg Witt-Film  UFA
| released =  4 February 1938
| runtime = 94 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Hans Nielsen.  It was made by the leading German studio Universum Film AG|UFA, based on a novel by Friedrich Lindemann. A British attorney working on a small island wants to return home to London, but first has to solve a mysterious crime.

==Cast==
*  Maria Andergast as Nancy Trevor  
* Theodor Loos as Staatsanwalt Trevor - ihr Vater   Hans Nielsen as Jack Winsloe - Schriftsteller  
* Erich Ponto as Kapitän Spurling 
* Maria Eiselt as Betty Bonn 
* Josef Sieber as Steuermann Crane  
* Hellmuth Bergmann as Kapitän Glower  
* Reinhold Bernt as Matrose Clements 
* Albert Hörrmann as Matrose Colcord  
* Herbert Schimkat as Matrose Bailey  
* Werner Scharf as Matrose Finley  
* Karl Bergeest as Matrose Klinkmoeller  
* Albert Venohr as Matrose Clyde 
* Louis Brody as Matrose Higgins  
* Martin Rickelt as Schiffsjunge Emmo Svedlund  
* Paul Otto as Gerichtspräsident Sir W. Douglas  
* Valy Arnheim as Konsul Spring 
* Käte Kühl as Emmely Bolston  
* Erhard Siedel as Thompson - amerik. Reeder  
* Bruno Fritz as Reeder Winchester  
* Willi Schaeffers as Kapitän Convey
* Karl Hellmer as Mac Canister  Karl Vogt as Cecil Palmer   Bob Bauer 
* Karl Fisser  
* Charlotte Fredersdorf 
* Adalbert Fuhlrott 
* Otto F. Henning Richard Ludwig 
* Hellmuth Passarge 
* Lotte Spira 
* Hans Sternberg 
* Kurt Weisse 
* Odette Orsy 
* Fritz Berghof 
* Friedrich Weigelt 
* Viktor Bell

== References ==
 

== Bibliography ==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999. 

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 