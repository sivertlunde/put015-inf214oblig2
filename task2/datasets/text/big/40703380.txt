17 Girls
 
{{Infobox film
| name           = 17 Girls
| image          = 17 Girls poster.png
| caption        = 
| director       = Delphine Coulin Muriel Coulin
| producer       = Denis Freyd   
| writer         = Delphine Coulin Muriel Coulin
| starring       = Louise Grinberg Juliette Darche Roxane Duran Esther Garrel    Florence Thomassin Noémie Lvovsky
| music          = 
| cinematography = 
| editing        = Guy Lecorne 
| distributor    = Strand Releasing
| released       =  
| runtime        = 86 minutes 
| country        = France
| language       = French
| budget         = $3 million
| gross          = $716,542 
}} Gloucester High School in 2008.   

==Plot==
In Lorient, 17 teenage girls from the same high school make an unexpected decision, incomprehensible to the boys and adults. They decide to get pregnant at the same time. Camille (Louise Grinberg) lives alone with her mother who is overwhelmed by her work. She becomes pregnant after a condom problem with a sexual partner who is not her boyfriend. She is the first to discover a positive pregnancy test.

She wants to keep her child, which will convince the others to become pregnant and they can all raise their children together.  These girls do not want to comply with the traditional code of conduct and just want to "give the love they have to a baby." Emancipation, is the keyword of these girls who build a plan to no longer be reflections of their parents. "We will be only 16 years apart from our kids, this is ideal. We will be closer in age, no clash of generations!" They decide to educate their future children together in the form of a "hippie community."

In the end, Camille loses her baby after a minor traffic accident. She and her mother leave town without telling anyone where theyve gone. The other girls have their babies, but they do not form a "community."

==References==
 

==External links==
* 

 
 
 
 
 


 