Daughters of Darkness
{{Infobox film
| name           = Daughters of Darkness
| image_size     = 
| image	=	Les-levres-rouges.jpg
| caption        = 
| director       = Harry Kümel
| producer       = Paul Coilet Alain C. Guilleaume
| writer         = Harry Kümel J.J. Amiel Pierre Drouot
| narrator       = 
| starring       = Delphine Seyrig Danielle Ouimet John Karlen Andrea Rau
| music          = François de Roubaix
| cinematography = Eduard van der Enden
| editing        = Denis Bonan Gust Verschueren
| distributor    = 
| released       = 1971
| runtime        = 100 min. / 87 min. (edited)
| country        = Belgium France West Germany
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Daughters of Darkness (in France, Les Lèvres rouges, and in Belgium, Le Rouge aux lèvres, both literally translated as The Red Lips) is a 1971 Belgian horror film (with dialogue in English), directed by Harry Kümel. It is an erotic vampire film.

==Plot summary==
A recently married young couple, Stefan (John Karlen) and Valerie (Danielle Ouimet), are on their honeymoon. They check into a grand hotel on the Ostend seafront in Belgium, intending to catch the cross-channel ferry to England, though Stefan seems oddly unenthused at the prospect of introducing his new bride to his mother. It is off-season, so the couple are alone in the hotel. Alone, that is, until the sun sets and a mysterious Hungarian countess, Elizabeth Báthory (Delphine Seyrig) arrives in a vintage Bristol driven by her "secretary" Ilona (Andrea Rau). The middle-aged concierge at the hotel swears that he saw the Countess at the same hotel when he was a little boy. The pair may have a connection to three separate gruesome murders of young girls that occurred in Bruges the previous week. On a day trip, Stefan and Valerie witness the aftermath of a fourth. At the hotel, the countess quickly becomes obsessed with the newlyweds, and the resulting interaction of the four people leads to sadism and murder. Ilona, Stefan, then the Countess all die, leaving Valerie, now transformed into a creature similar to the Countess, stalking new victims.

==Production==
Director Kumel, interviewed by Mark Gatiss for the BBC documentary Horror Europa said that he deliberately styled Delphine Seyrigs character after Marlene Dietrich and Andrea Raus after Louise Brooks to deepen the filmic resonance of his own movie. Because the vampire character of Elizabeth Bathory is also a demagogue, Kumel dressed her in the Nazi colours of black white and red. In commenting on both the films mordant sense of humour, and the directors painterly eye in the composition of several scenes, Gatiss drew forth the comment from Kumel that he considers the film very Belgian, especially due to the influence of Surrealism and Expressionism.

Extensive external shooting was done at the Royal Galleries of Ostend, a seaside neoclassical arcade on the beach at Ostend (especially at the luxury Grand Hotel des Thermes, which sits atop the central section of the arcade). Interior shooting was done at the Hotel Astoria, Brussels and other exteriors at the Tropical Gardens, Meise.

==Interpretation==
Camille Paglia wrote that, "A classy genre of vampire film follows a style I call psychological high Gothic. It begins in Samuel Taylor Coleridge|Coleridges medieval Christabel (poem)|Christabel and its descendants, Edgar Allan Poe|Poes Ligeia and Henry James|Jamess The Turn of the Screw. A good example is Daughters of Darkness, starring Delphine Seyrig as an elegant lesbian vampire. High gothic is abstract and ceremonious. Evil has become world-weary, hierarchical glamour. There is no bestiality. The theme is eroticized western power, the burden of history." 

According to critic Geoffrey OBrien:

 Lesbian vampires made frequent incursions in the early 1970’s—in movies ranging from hardcore pornographic to dreamily aesthetic — as the Gothic horror movie took to flaunting its psychosexual subtexts. Daughters of Darkness leans flamboyantly toward the artistic end of the spectrum, with Delphine Seyrig sporting Last Year at Marienbad|Marienbad-like costumes and the Belgian director conjuring up images of luxurious decadence replete with feathers, mirrors, and long, winding hotel corridors. At the film’s core, however, is a deeply unpleasant evocation of a war of nerves between Seyrig’s vampire and the bourgeois newlyweds into whose honeymoon she insinuates herself. Jaded age preys cunningly on narcissistic youth, and seductiveness and cruelty become indistinguishable as Seyrig forces the innocents to become aware of their own capacity for monstrous behavior. If Fassbinder had made a vampire movie it might have looked something like this.  

==Reception== Time Out conducted a poll with several authors, directors, actors and critics who have worked within the horror genre to vote for their top horror films.  Daughters of Darkness placed at number 90 on their top 100 list. 

==See also==
*Vampire film

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 