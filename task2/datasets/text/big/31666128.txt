Nellu (1974 film)
{{Infobox Film
| name           = Nellu
| image          = Nellu (1974).jpg
| caption        = 
| director       = Ramu Kariat
| producer       = N. P. Ali
| writer         = P. Valsala Ramu Kariat K. G. George S. L. Puram Sadanandan
| based on       = Nellu by P. Valsala
| starring       = Prem Nazir Jayabharathi Mohan
| music          = Salil Chowdhury
| cinematography = Balu Mahendra
| editing        = Hrishikesh Mukherjee Appu
| studio         = Jammu Films International
| distributor    = Jammu Pictures Circuit Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =  
| gross          = 
}}
Nellu is a 1974 Malayalam film directed by Ramu Kariat based on the award-winning novel of the same name by P. Valsala. The film is about the tribal people of the Wayanad hills in the deep Kerala forests. It stars Prem Nazir and Jayabharathi in the lead roles. The film is noted for its aesthetic quality and is regarded as a landmark in the history of Malayalam cinema.
 Kerala State Film Award for Best Editing. 

==Plot==
The film portrays the life of "Adiyar" community, a tribe whose ways of life belongs to centuries ago. They live in the Wayanad hills in the deep Kerala forests. The film envelopes the passions, superstitions, religious practices, customs, and the servility and exploitation the tribal people endure. The film is narrated through the observations of an outsider.

==Cast==
 
* Prem Nazir as Raghavan Nair
* Thikkurissy Sukumaran Nair as Perumal
* Sankaradi as Damoradara Varrier
* Kottarakkara Sreedharan Nair as Chundeli
* Mohan Sharma as Mallan
* Adoor Bhasi as Thapaal Nanu
* Bahadoor as Saidu
* K. P. Ummer as "Vedikkaran" Ouseph
* Kedamangalam Ali as Baddamaran
* Shihab as Ezhuthappottan
* S. P. Pillai as Vaattuvelu (guest appearance)
* Prem Nawas as Unnikrishnan
* J. A. R. Anand as Chathan
* Jayabharathi as Mara
* Kaviyoor Ponnamma as Savithri Varassyar
* Kanakadurga as Kurumatti
* Adoor Bhavani as Pembi
* Rani Chandra as Javani
* K. V. Shanthi Sumithra as Safiya
* Chelavoor Krishnankutty Innocent

==Production==
P. Valsala wrote the novel after a rejuvenating experience in Wayanad. It was published in 1972 by Sahithya Pravarthaka Cooperative Society. It went on to become a bestseller and won the Kumkumam Award for Best Novel in 1973. Kariat approached Valsala for permission to adapt the novel, and when she agreed, he asked K. G. George, S. L. Puram Sadanandan and Valsala to write separate scripts based on the novel and in the end, took certain portions from all the three and made the film. 
 Sharada was originally chosen for the role of Mara. She turned down the offer when Kariat insisted that she dress like the tribal women of Wayanad. Later, Kariat fixed Jayabharathi, another leading actress of the time, for that role.    Nellu marked the debut of actor Mohan Sharma who went on to act in about 100 films in Malayalam. 

The films production occurred at various villages near Sultan Battery in Wayanad. It was produced by N. P. Ali under the banner of Jammu Films International.

==Songs==
The film features five songs composed by Salil Chowdhary and written by Vayalar Ramavarma|Vayalar. 
#"Chemba Chemba" - Manna Dey, Jayachandran, Chorus
#"Kadali Chenkadali" - Lata Mangeshkar
#"Neela Ponmane" - Yesudas, P. Madhuri
#"Kaadu Kuliranu" - P. Susheela

==References==
 

==External links==
*  
*   at Malayalam Movie Database

==Further reading==
*  
*  

 
 
 
 
 
 
 