Gopal Krishna (1938 film)
{{Infobox film
| name           = Gopal Krishna
| image          = 
| image_size     = 
| caption        = 
| director       = V. G. Damle Sheikh Fattelal
| producer       = Prabhat Film Company
| writer         = Shivram Vashikar
| narrator       = 
| starring       = Ram Marathe Shanta Apte Parshuram Ganpatrao
| music          = Master Krishnaro
| cinematography = V. Avadhoot
| editing        = 
| distributor    =
| studio         = Prabhat Film Company
| released       = 1938
| runtime        = 132 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1938 Hindi Gopal Krishna Marathi and Hindi simultaneously. The film was directed by Sheikh Fattelal and V. G. Damle and starred Ram Marathe, Shanta Apte, Parshuram, Prahlad, Ulhas and Ganpatrao.    The story was written by Shivram Vashikar and the music was by Krishnarao.
 British rule was high. The film makers metaphorically used the story of the boy Krishna and the cowherds against the oppressive King Kamsa, portraying the feelings of the Indians against the British mainly through dialogue.   

==Plot==
The story is based in Gokul where the young playful Krishna resides with his foster mother Yashodha and father Nanda. He tends cows along with other young cowherds. Gokul is ruled by the despotic King Kamsa who has Krishna’s real parents in custody. He is intent on killing Krishna to prevent the prophecy of his death trough Krishna coming true. Krishna incites the village people against Kamsa’s oppressive regime. He prevents 500 cows being sent to Kamsa who demands that the people of Gokul do so. He battles  Kama’s General Keshi and defeats him when he is sent to kill him. The only miracle shown in the film is when Kamsa unleashes rain (unlike the other Puranic stories where the rain is brought about by the Rain God Indra) and Krishna lifts the Govardhan hill to shelter the people under it.

==Production==
The sets for the filming were constructed in Poona where the Prabhat film Company was situated. The cows needed for the shoot were transported from Dombivili a suburb in Bombay to Poona by train. The cattle were allowed to roam freely on the sets.   

==Soundtrack==
Master Krishnarao was the music director and some of the songs are still popular today.  The songs were sung by Hansa Apte, Ram Marathe and Parshuram and the lyricist was Pandit Anuj.   

===Songs===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Bachpan Ka Yaad Aaya" 
| Shanta Apte
|-
| 2
| "Nirdhan Ka Hai Tu Dhan"
| Ram Marathe
|-
| 3
| "Kishan Pe Jaaun Waari" 
| Parshuram
|-
| 4
| Gujariya De Dadhidaan 
| Shanta Apte
|-
| 5
| "Maata Gau Hamaari Praanon Se Tu Hai Pyaari"
| Chorus
|-
| 6
| Banwaari Hai Aaya Gend Khelne
| Parshuram
|-
| 7
| "Sar Sar Sarwat"
| Shanta Apte
|-
| 8
| "Naachta Jhoomta Jaaye Gokul"
| Shanta Apte, Ram Marathe
|-
| 9
| "Graas Yeh Preet Ki Mod Ke Kha"
| Shanta Apte
|-
| 10
| "Kanha Sab Ko Mohe"
| Shanta Apte
|-
| 11
| "Tu Meri Maiya"
| Ram Marathe
|-
| 12
| "Gokul Ke Veer Japo"
| Parshuram
|-
| 13
| "Tum Brij Ke Dulare"
|
|-
| 14
| "Mod Mayi Yeh Kapil Gaay"
| Shanta Apte
|- 15
| "Ratnon Jaisi Gaun Hamari"
| Parshuram
|-
|}

==References==
 

==External links==
* 

 

 
 
 
 