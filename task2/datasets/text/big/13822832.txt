Medal for the General
{{Infobox film
| name           = Medal for the General
| image          = MedalForTheGeneral.JPG
| image_size     = 
| caption        = Godfrey Tearle (second from left) and Petula Clark (third from left) in a scene from the film
| director       = Maurice Elvey
| producer       = Louis H. Jackson
| writer         = Elizabeth Baron Based on the novel by James Ronald
| narrator       = 
| starring       = Godfrey Tearle Jeanne de Casalis Petula Clark
| music          = William Alwyn Arthur Grant James Wilson 
| editing        = Grace Garland   
| studio         = British National Films Company
| distributor    = Anglo-American Film Corporation (UK) Four Continents Films (US)
| released       =   Petula Clark Film Companion London: Meeting Point Publications 1998 
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Medal for the General is a 1944 British comedy film directed by Maurice Elvey. The screenplay by Elizabeth Baron is based on the novel of the same title by James Ronald.

==Plot==
The title character is Victor Church, a World War I veteran who becomes despondent when his advancing age prevents him from playing an active role in the battles of World War II. Feeling unwanted and useless, he retreats to his country estate and plans his suicide. He finds a new purpose in life when he opens his home to six rambunctious Cockney children evacuated from the London slums and tries to keep the mischievous group under control.

==Production==
Director Maurice Elvey was still searching for a young girl to portray the precocious orphan Irma when he attended a charity concert to benefit the National Fire Service at Royal Albert Hall. On the bill was eleven-year-old Petula Clark, who in addition to singing appeared in a comedy sketch written by her father. Elvey was so impressed by her performance he went backstage and offered her the role in his film.  The following year he cast her in I Know Where Im Going!, and the two reunited for the 1954 film The Happiness of Three Women.

==Cast==
*Godfrey Tearle ..... Gen. Victor Church
*Jeanne de Casalis ..... Lady Frome
*Morland Graham..... Bates  
*Mabel Constanduros.....  Mrs. Bates
*John Laurie ..... McNab
*Patric Curwen..... Dr. Sargeant  
*Thorley Walters ..... Andrew
*Alec Faversham..... Hank  
*Michael Lambart..... Lord Ottershaw 
*Irene Handl ..... Mrs. Famsworth
*Rosalyn Boulter..... Billetting Officer
*Petula Clark ..... Irma

==Critical reception==
The Times said, "Medal for the General is hardly a subtle or intellectual film, but it is warmhearted and the acting and direction show tact and good sense throughout."  

The Daily Telegraph thought the story "is hardly promising material, and the sentimental way in which it is treated does nothing to make it more palatable."  

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 