Good Deeds
 
{{Infobox film
| name = Good Deeds
| image = Good Deeds Poster.jpg
| caption = Theatrical release poster
| border = yes
| director = Tyler Perry
| producer = Tyler Perry Paul Hall Ozzie Areu
| writer = Tyler Perry
| starring =  Tyler Perry Thandie Newton Brian J. White Rebecca Romijn Jamie Kennedy Phylicia Rashad Gabrielle Union
| music = Aaron Zigman
| cinematography = Alexander Gruszynski
| editing = Maysie Hoy
| studio = Tyler Perry Studios Lionsgate
| released =  
| runtime = 111 minutes
| country = United States
| language = English
| budget = $14,000,000 
| gross = $35,025,791   
}}

Good Deeds is a romantic drama film written, co-produced, directed by and starring Tyler Perry. The film was released on February 24, 2012. It is the eleventh out of thirteen films directed by Perry in which he appears. 

==Plot==
The film opens up with Wesley Deeds (Tyler Perry), the rich entrepreneur of the family-owned Deeds Corporation, getting dressed for work. His fiancée, Natalie (Gabrielle Union) fixes breakfast for him, obsessing over Wesley’s predictable daily routine and life. Before going to work, Wesley makes one unpredictable stop- to pick up his delinquent younger brother Walter (Brian J. White), who lost his driver’s license after a string of DUIs. Wesley informs Walter they are going to have lunch with their mother, the respectable Wilimena Deeds (Phylicia Rashad).

After lunch, Wilimena goes to meet Natalie, Natalie’s mother, and Natalie’s best friend Heidi (Rebecca Romijn) at the bridal shop. Wilimena and Natalie’s mother urge the bride-to-be to consider her future with Wesley by opening up about how many children they want. At the same time, on an impoverished side of town, Lindsey Wakefield (Thandie Newton), a single mother and cleaning woman for The Deeds Corporation, finds out that she will be evicted from her home if she doesn’t pay her bills soon.

Lindsey hurries to receive her check from work, and ends up parking in Wesley’s reserved spot, leaving her six-year-old daughter Ariel (Jordenn Thompson) inside. Wesley and Walter find Ariel in the car as Lindsey rushes to work only to find back taxes has cut down her paycheck. When Lindsey returns, she finds that Walter has had her car towed, and engages in a heated argument with the two men. Lindsey rushes off to bring Ariel to school while Wesley has a meeting with his coworker and best friend John (Eddie Cibrian) over how to snag a rival company Wesley and Walter’s father has been against for years.

Lindsey returns from work only to find that she has been evicted from her home. Scooping up her belongings, she drives to Ariel’s school, picking her up late. While she works another shift, Lindsey keeps Ariel in the broom closet of the building. After her shift is finished, Lindsey scoops up Ariel and together they reside in their van. Wesley sees them and decides to watch over them until Lindsey’s “boyfriend” comes to get them. Eventually, Wesley takes Lindsey and Ariel out for pizza. Wesley and Lindsey bond, smoothing over their earlier disagreement.

However, the two both face rough bumps in their lives: Ariel’s teacher finds out that Lindsey and Ariel are homeless and threatens to call child welfare, and Wesley struggles to expand the business while watching over Walter and discovering a more complex love life with Natalie. One night, Natalie returns home drunk after a fashion show in a nightclub and unsuccessfully tries to seduce Wesley, yelling at him in anger for not being spontaneous.

After child welfare comes and takes Ariel after telling Lindsey she can get Ariel back when she finds a solid place to live, Wesley gives Lindsey a rent-free corporate apartment. Lindsey again gains custody of Ariel, and helps Wesley find his wild side. Wesley reveals he always wanted to ride a motorcycle. Lindsey rents a bike and together, the two ride throughout the countryside, eventually stopping by a pond where they share a kiss. Wesley reveals his engagement, and Lindsey runs away to pick up Ariel from school.

Wesley finds out that the business officially snagged the rival company, and his corporation holds a party in his honor that goes badly when Walter has an outburst. Lindsey comes to the party to talk to Wesley and tries to leave when she sees what is going on, but Walter forces her to stay. Walter implies to Wil that Wesley and Lindsey are having an affair, calling Lindsey a janitor.

Wilimena then gives Lindsey a subtle hint that Wesley does not stay in relationships with someone under his economic class for long before Natalie, Wesley, Walter, Wilimena, and Lindsey get stuck in an elevator after Wesley and Walter have picked an immense fight with each other. Natalie and Wilimena notice Lindsey reaching caringly for Wesleys hand, which was injured in the tussle with his brother.

Wesley goes to see Lindsey at her apartment later that night, but she rejects him. Wesley and Natalie soon realize that although they love each other, their marriage would not be a happy one, and that they are only living this engagement for their parents. They reveal at their engagement party that they are no longer planning to marry, much to Wilimena’s surprise. Wesley also reveals he is quitting his job and taking up traveling to see his old friends, hiring John as the new CEO of The Deeds Corporation. Though he is initially angered by this decision, Walter accepts this, and begins to calm down his aggressive attitude.

Before he leaves, Wesley goes to see Lindsey at the office and reveals his plans to go to Nigeria, inviting Lindsey and Ariel along after telling her the engagement is off. Lindsey again rejects him, and Wesley heads to Nigeria after giving his mother a good-bye kiss. At the airport, Wesley doesn’t see Lindsey, and sadly boards the plane. When he turns around as the plane is set to take flight, he finds Lindsey and Ariel sitting across the aisle. Wesley and Lindsey share a kiss, with Ariel looking on happily.

==Cast==
* Tyler Perry as Wesley Deeds 
* Thandie Newton as Lindsey Wakefield 
* Gabrielle Union as Natalie 
* Eddie Cibrian as John
* Brian J. White as Walter Deeds
* Jordenn Thompson as Ariel 
* Phylicia Rashad as Wilimena Deeds
* Beverly Johnson as Brenda
* Rebecca Romijn as Heidi 
* Jamie Kennedy as Mark Freeze
* Andrew Masset as Mr. Brunson
* Victoria Loving as Mrs. Brunson 
* Tom Thon as Milton

==Production== Liongate and Tyler Perry Studios  on February 24, 2012. 

==Release==

===Critical reaction===
As November 2012, the film has received mixed reviews. Rotten Tomatoes reported that 32% of critics gave the film positive reviews out of 31 reviews, with an average rating of 5/10.    Its consensus states, "Tyler Perrys craftmanship as a director continues to improve, but his stories are still the same ol hoary, pretentious melodramas."  Metacritic reported that the film has an average score of 43 out of 100 based on 15 reviews. 

===Box office===
The film grossed $15,583,924, ranking second on its opening weekend.  As of November 2012, the film has grossed a total gross of $35,025,791. 

===Home media===
The DVD was released on June 12, 2012. 

==Controversy==
In November 2012, author Terri Donald sued Perry claiming Good Deeds was based off her book, Bad Apples Can Be Good Fruit, published in 2007. Donald is seeking for $225,000 in initial damages as well as an injunction requiring the company to add a credit for her book in the opening and closing credits, as well as an account of the film’s revenue. Donald reportedly sent a copy of her book to Tyler under the pseudonym, TLO Redness, before the film went into production. 

==Awards/Nominations==
*BET Awards
**Best Movie

*Golden Raspberry Awards
**Worst Actor (Tyler Perry) - Nominated
**Worst Director (Tyler Perry) - Nominated

*NAACP Image Award
**Outstanding Actor in a Motion Picture (Tyler Perry) - Nominated

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 