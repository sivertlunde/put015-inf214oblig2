Kidz in da Hood
{{Infobox film
| name           = Kidz in da Hood
| image          =
| image_size     =
| caption        =
| director       = Catti Edfeldt Ylva Gustavsson
| producer       = Peter Holthausen Pontus Sjöman
| writer         = Ylva Gustavsson Hans Renhäll
| narrator       =
| starring       = See below
| music          = Fabian Torsson
| cinematography = Jallo Faber
| editing        = Roger Sellberg
| distributor    =
| released       =
| runtime        = 96 minutes (Berlin International Film Festival)
| country        = Sweden
| language       = Swedish
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Kidz in da Hood (Swedish title: Förortsungar lit. "Suburb kids") is a 2006 Swedish film directed by Catti Edfeldt and Ylva Gustavsson.

The film is also known as F.O.U. in Sweden (promotional abbreviation).

This is the story of Amina, a girl from Sierra Leone seeking asylum in Sweden. The second key figure in the story is Headbanger Johan, played by Gustaf Skarsgård. The film is about how their paths cross - a meeting that will change both their lives forever.

Filming took place in Fittja and began on 21 November 2005.

The film won five Guldbagge Awards in 2007 in the categories Best Film, Best Director , Best Screenplay , Best Actor and Best performance for which the jury put it, "attitude and rocking the music."

== Cast ==
*Beylula Kidane Adgoy as Amina
*Gustaf Skarsgård as Johan
*Embla Hjulström as Mirre
*Jennifer Brown as Janet
*Sanna Ekman as Maggan
*Sunil Munshi as Jesper
*Hannu Kiaviaho as Pecka
*Teodor Runsjö as Micke
*Jonathan Kurkson as Hassan
*Alejandro Castillo as Antonio
*Dogge Doggelito as Viktor
*Ahmadu Jah as Said
*Fredrik Eddari as Ali
*Olle Sarri as Berra
*Douglas Johansson as Social chef
*Peter Jezewski as Johnny
*Jacob Mohlin as Cop
*Marienette Dahlin as Cop
*Cesar Fulgencio as Teacher
*Majken Pollack as Doctor
*Eduardo Grutzky as Restaurant chef
*Christopher Mhina as Marco

== Soundtrack ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 
 