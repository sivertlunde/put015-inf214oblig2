Pokémon 3: The Movie
{{multiple issues|
 
 }}
{{Infobox film
| name           = Pokémon 3: The Movie
| image          = Pokemon-3-japanese-poster.jpg
| caption        = Japanese release poster
| film name      = {{film name
| kanji          = 劇場版ポケットモンスター 結晶塔の帝王 ENTEI
| romaji         = Gekijōban Poketto Monsutā Kesshōtō no Teiō ENTEI
| translation    = Pocket Monsters the Movie: Lord of the "UNKNOWN" Tower ENTEI
}}
| director       = Kunihiko Yuyama
| producer       = Yukako Matsusako Takemoto Mori Choji Yoshikawa
| writer         = Hideki Sonoda Takeshi Shudo
| narrator       = Unshō Ishizuka
| starring       = Rica Matsumoto Mayumi Iizuka Yūji Ueda Ikue Ōtani Megumi Hayashibara Shin-ichiro Miki Ai Kato Masami Toyoshima Akiko Yajima Naoto Takenaka|
| music          = Shinji Miyazaki
| cinematography = Hisao Shirai
| editing        = Toshio Henmi Yutaka Itō
| studio         = OLM, Inc.
| distributor    = Toho
| released       =   
| runtime        = 91 minutes
| country        = Japan  
| language       = Japanese  
| budget         = ¥1.6 billion (US $16 million)
| gross          = $68,411,275   
}}

Pokémon 3: The Movie: Spell of the Unown, commonly referred to as Pokémon 3: The Movie, originally released in Japan as  , is a 2000 Japanese anime film directed by Kunihiko Yuyama as the third feature-length Pokémon (anime)|Pokémon film.  It was released in Japanese theaters on July 8, 2000, and the English adaptation was released into theaters on April 6, 2001. This adaptation, the final film to be distributed by Warner Bros., was produced by 4Kids Entertainment.
 Pikachu and 3D effect in the movie.

==Plot==

===Pikachu and Pichu===
The film comes accompanied by a short mini-movie featuring Pikachu. In this mini-movie, Pikachu and his friends are left on a skyscraper in Big City by their trainers, who go off to prepare an unknown surprise for the Pokémon. Pikachu meets the Pichu Brothers, saving the younger one from falling off an opposite building. A group of Murkrow chase Pikachu off a flagpole, and he uses a group of Hoppip to reach the other side, sending Meowth who is window-cleaning, flying into a billboard. The Pichu Bros. assist Pikachu to return to his friends but they end up going on a journey across the city to the Pichu Bros playground. On the way, they get chased by a Houndour who they later encounter again. The angry Houndour chases the three around until he nearly knocks the playground over. Pikachu, the Pichu Bros., Houndour and their assortment of friends manage to save the playground. Pikachu realizes it is nearly six oclock and he must return to his friends before Ash, his trainer, does. Pikachu and the Pichu Bros. use a tire to get to the building, sending Meowth flying again. The three arrive in the nick of time, the Pichu Brothers departing. Ash, Misty and Brock arrive and take the Pokémon into a room where a party has been laid out for them in celebration of the first anniversary of Ash and Pikachus meeting.

===Spell of the Unown===
The feature film focuses on the beautiful town of Greenfield. A resident of the town, the research scientist Professor Spencer Hale, conducts research on the elusive Unown. He and his assistant, Skyler, discover a site of ruins, but Hale is sucked into the dimension of the Unown.

His disappearance leaves his young daughter Molly alone, her mother having disappeared previously. Molly finds a box of tablets containing Unown images and begins assembling the letters, which summons the Unown themselves. The Unown use their powers to make Mollys wishes come true, transforming her manor house into a crystal-like palace which spreads across the town and cuts her off from the world. Entei is created to represent Mollys father. Various people come to help sort out the Unown, including Professor Oak and Delia Ketchum (Ash Ketchum|Ashs mother).

Meanwhile, Ash and his friends meet and befriend a trainer named Lisa. They come into Greenfield in the process and agree to join in the rescue mission to save young Molly. However, Entei kidnaps Delia, following Mollys request for a mother as well. Enteis powers hypnotize Delia into thinking she is Mollys mother. Ash, Misty, Brock and their Pokémon head out to the mansion to save Delia, communicating with Professor Oak and Skyler thanks to a PokéGear device given to them by Lisa. Team Rocket try to investigate the mansion, only to be blasted out of the air by Entei into the depths of the mansion. Molly watches Ash and the others through a television and falls asleep, imagining herself being a Pokémon Trainer.  Seeing Ash on TV, Delia snaps out of her trance, which is not noticed by Entei, who then creates a dream version of Molly as an adult and takes her to battle the three. Molly first fights Brock, but Mollys dreamed-up Pokémon are stronger than his; Molly then has a more friendly fight against Misty in an underwater battle, but the winner is not shown.

Ash manages to locate Molly and Delia, but Molly refuses to leave with him and the mansion transforms. Entei refuses to allow Ash to leave with his mother, and fights his Pokémon. He then blasts Ash and Pikachu off a cliff, but they are saved by the arrival of Ashs Charizard. Charizard, with Ash on his back, battles Entei until he is knocked out of the sky. Entei nearly kills Charizard until Molly commands him to stop and begs that no more fighting happens, which manages to stop Entei. Ash and his friends convince Molly to leave with them, Entei revealing he was created by the Unown to be her father.

The Unown suddenly lose control of their powers and start to seal the group in the mansion. Ash, Pikachu, Charizard, Misty, Brock, Delia, Molly and Team Rocket escape down to the hall where the Unown are. Pikachu and Charizard attempt to break the forcefield protecting the Unown, but they are unsuccessful—until they are joined by Entei, combining their powers to destroy the shield with Mollys support. Entei sacrifices himself and the Unown return to their dimension, reversing all of their effects on the world and returning Professor Hale to the ruins where he originally vanished.

The group ventures outside, where Professor Oak, Skyler, Lisa and others meet them. Team Rocket hides in the mansion upon seeing all of the police outside and declare that they will always have another opportunity to catch Pokémon. In the end credits, Charizard and Lisa depart from Ashs company; Molly is seen with her own Teddiursa and reunites with her father—and later, with her long-lost mother.

==Cast==
 

===Regular characters===
*Rica Matsumoto (Veronica Taylor in the English adaptation) as Satoshi (Ash Ketchum in the English adaptation), the main protagonist of the film. He is a young boy who wishes to be a Pokémon master and free his mother from the clutches of Entei.
*Ikue Ōtani as Pikachu, Ashs first Pokémon.
*Mayumi Iizuka (Rachael Lillis in the English adaptation) as Kasumi (Misty in the English adaptation), a Pokémon trainer and Ashs travelling companion.
*Yūji Ueda (Eric Stuart in the English adaptation) as Takeshi (Brock in the English adaptation), a Pokémon breeder and Ashs travelling companion.
*Satomi Kōrogi as Togepi, a Pokémon owned by Misty.
*Megumi Hayashibara (Rachael Lillis in the English adaptation) as Musashi (Jessie in the English adaptation), a member of the Team Rocket. Along with James and Meowth, she follows Ash into the Crystal Tower.
*Shin-ichiro Miki (Eric Stuart in the English adaptation) as Kojirō (James in the English adaptation), a member of Team Rocket.
*Inuko Inuyama (Maddie Blaustein in the English adaptation) as Nyarth (Meowth in the English adaptation), a member of Team Rocket. Unusually for a Pokémon, he has the ability to walk upright and is capable of human speech.
*Yūji Ueda (Kayzie Rogers in the English adaptation) as Sonans (Wobbuffet in the English adaptation), a Pokémon owned by Jessie.
*Shin-ichiro Miki as Lizardon (Charizard in the English adaptation), a Pokémon owned by Ash that returns from Charicific Valley in time to save Ash from a pinch.
*Unshō Ishizuka (Rodger Parsons in the English adaptation) as the Narrator
Ashs other Pokémon include:
*Mika Kanai as Chikorita
*Yūji Ueda (Kayzie Rogers in the English adaptation) as Hinoarashi (Cyndaquil in the English adaptation)
*Chinami Nishimura (Kayzie Rogers in the English adaptation) as Waninoko (Totodile in the English adaptation) Tara Jayne in the English adaptation) as Fushigidane (Bulbasaur in the English adaptation)
*Yūji Ueda as Yorunozuku (Noctowl in the English adaptation)
Mistys other Pokémon include:
*Shin-ichiro Miki as Hitodeman (Staryu in the English adaptation)
*Shin-ichiro Miki (Eric Stuart in the English adaptation) as Nyorozo (Poliwhirl in the English adaptation)
*Ikue Ōtani (Rachael Lillis in the English adaptation) as Tosakinto (Goldeen in the English adaptation)
Brocks Pokémon include:
*Shin-ichiro Miki as Zubat
*Rikako Aikawa (Rachael Lillis in the English adaptation) as Rokon (Vulpix in the English adaptation)
*Unshō Ishizuka as Iwark (Onix in the English adaptation)

===Guest characters===
*Akiko Yajima (Amy Birnbaum in the English adaptation) as Mi Snowdon (Molly Hale in the English adaptation), a five-year-old girl from Greenfield who organizes the kidnapping of Hanako. Her Pokémon consist of Kingdra, Mantine (voiced by Katsuyuki Konishi), Mokoko (Flaaffy in the English adaptation), Himegura (Teddiursa in the English adaptation) and Gomazō (Phanpy in the English adaptation) Dan Green in the English adaptation) as Entei, a legendary Pokémon created by the combined forces of Mis dreams and the power of the Unown. He serves as the main antagonist but turns good at the end; the Diablo is the true antagonist of the film.
**Takenaka and Green also voice Doctor Sully Snowdon (Doctor Spencer Hale in the English adaptation), Mis father and a student of Doctor Orchid.
*Ai Kato (Lisa Ortiz in the English adaptation) as Rin (Lisa in the English adaptation), a bandana-wearing Pokémon trainer who battles Satoshi in Greenfield. Her Pokémon consist of Aipom (voiced by Etsuko Kozakura), Granbull, Mankey, Kirinriki (Girafarig in the English adaptation), Butterfree and Nuō (Quagsire in the English adaptation). Ted Lewis in the English adaptation) as John (Schuyler in the English adaptation), Doctor Sullys assistant.
**Naoto Takenaka (J. B. Blanc in the English adaptation) as Diablo the true antagonist of the film.
*Kōichi Sakaguchi as the Cameraman
*Yoko Soumi as the Reporter

===Other characters===
*Unshō Ishizuka (Stuart Zagnit in the English adaptation) as Doctor Orchid (Professor Oak in the English adaptation), a Pokémon scientist and the teacher of Doctor Sully.
**Ishizuka also voices the Narrator (voiced by Rodger Parsons in the English adaptation).
*Masami Toyoshima (Veronica Taylor in the English adaptation) as Hanako (Delia Ketchum in the English adaptation), Satoshis mother and a childhood friend of Doctor Sully.
*Tomokazu Seki (Ted Lewis in the English adaptation) as Kenji (Tracey Sketchit in the English adaptation), Doctor Orchids assistant.
*Ayako Shiraishi (Megan Hollingshead in the English adaptation) as Nurse Joy
*Chinami Nishimura (Megan Hollingshead in the English adaptation) as Junsā (Officer Jenny in the English adaptation)

==Release==

===Box office===
Like its predecessors, for the films theatrical release, select theaters would give away exclusive Pokémon Trading Card Game|Pokémon trading cards, to capitalize on the success of the trading card game.

Pokémon 3: The Movie opened in theaters in Japan on July 8, 2000. The film was released in the United States on April 6, 2001, debuting at #4 on its opening weekend earning $8,240,752 from 2,675 theaters.  The film proved less successful in the box office compared to previous films. During its 10-week box office run, Pokémon 3: The Movie made a significant profit-margin, with a worldwide gross of $68,411,275 ($17,052,128 in America) 

===Reception===
The film received generally negative reviews from film critics. On Rotten Tomatoes, the film has a 22% "Rotten" approval rating, a significant improvement from the two predecessors, based on the reviews of 55 critics, with the consensus being, "Critics say that the third Pokémon movie has a better plot than its two predecessors. This is not enough, however, to recommend it to those not already fans of the franchise".  The film also has a 22 out of 100 on Metacritic. 

===Home Media===
Pokémon 3: the Movie was released on VHS and DVD on August 21, 2001. 

==Soundtrack==
{{Infobox album
| Name        = Pokémon 3: The Ultimate Soundtrack
| Type        = Soundtrack
| Artist      = Various artists
| Cover       = Pokémon 3 The Ultimate Soundtrack.png
| Background  = Gainsboro
| Released    = April 3, 2001 Pop
| Length      = 58:35
| Label       = Koch Records
| Producer    = John Loeffler
| Last album = Totally Pokémon (2001)
| This album = Pokémon 3: The Ultimate Soundtrack (2001)
| Next album = Pokémon Christmas Bash (2001)
}}

Pokémon 3: The Ultimate Soundtrack is the   (which was also released as a separate disc worldwide), Pokémon The Movie 2000 Original Motion Picture Score.  The Japanese and English-language soundtracks contain different tracks. Shinji Miyazaki wrote the original film score, while Ralph Schuckett composed the score for the International and Japanese DVD releases.

The second track, "To Know the Unknown" was performed by the girl-group Innosense. Tracks 13 to 15 are karaoke versions. The CD also features two Pokémon videos, the Pokérap and a scene from the film, which are accessible upon insertion of the disc into a computer.

===Track listing===
{{Track listing
| extra_column  = Performed by
| title1        = Pokémon Johto  
| note1         = Johto
| title2        = To Know the Unknown
| note2         = Innosense
| title3        = Pikachu (I Choose You)
| note3         = Johto
| extra3        = Élan Rivera
| title4        = All We Wanna Do
| note4         = Johto
| extra4        = Ken Cummings, John Loeffler
| title5        = He Drives Me Crazy
| note5         = Johto
| extra5        = Shauna McCoy
| title6        = You & Me & Pokémon
| note6         = Johto
| extra6        = Élan Rivera, PJ Lequerica
| title7        = Song of Jigglypuff
| note7         = Johto
| extra7        = Jamily Gray, Shauna McCoy
| title8        = Pokerap GS
| note8         = Johto
| title9        = Two Perfect Girls
| extra9        = Eric Stuart
| title10       = Pokémon Johto  
| note10        = Johto
| extra10       = PJ Lequerica
| title11       = Biggest Part of My Life
| note11        = Johto
| extra11       = PJ Lequerica
| title12       = Medley from Spell of the Unown
| extra12       = John Loeffler and Ralph Schuckett
| title13       = Pikachu (I Choose You)
| note13        = Karaoke Version
| title14       = Song of Jigglypuff
| note14        = Karaoke Version
| title15       = You & Me & Pokémon
| note15        = Karaoke Version
}}

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 