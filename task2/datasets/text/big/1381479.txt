Blue Murder at St Trinian's
{{Infobox film
|name=Blue Murder at St Trinians
|image=Blue-Murder-at-St-Trinians.jpg image size=180px
|caption=VHS cover
|director=Frank Launder
|producer=Frank Launder Sidney Gilliat
|writer=Frank Launder Sidney Gilliat Val Valentine George Cole Sabrina
|music=Malcolm Arnold Gerald Gibbs
|editing=Geoffrey Foot
|distributor=British Lion Films
|released=1957
|runtime=86 min.
|country=United Kingdom
|language=English
|budget=
}} George Cole, Joyce Grenfell, Lionel Jeffries and Richard Wattis.

==Plot== Flash Harry" has set up a marriage agency for the Sixth Form and an Italian Prince is interested in meeting the girls. Meanwhile, the father of one of the Sixth Formers is involved in a diamond theft and recklessly decides to hide out at the school.

Cheating in an academic competition so that they can win a goodwill trip overseas to pave the way for the Sixth Form girls to meet the Prince, the girls force the diamond thief to reluctantly masquerade as their headmistress so that he can accompany them on their trip as their chaperone.
 hockey in France and at water polo in Italy.

==Cast== George Cole Flash Harry"
*Joyce Grenfell as Policewoman Sergeant Ruby Gates
*Terry-Thomas as Captain Romney Carlton-Ricketts Superintendent Kemp-Bird
*Terry Scott as Police Sergeant
*Lionel Jeffries as Joe Mangan
*Thorley Walters as Major Whitehart
*Cyril Chamberlain as Army Captain
*Judith Furse as Dame Maud Hackshaw
*Kenneth Griffith as Charlie Bull
*Alastair Sim as Miss Fritton
*Guido Lorraine as Prince Bruno

Sixth Form
*Lisa Gastoni as Myrna Mangan
*Rosalind Knight as Annabel
*Dilys Laye as Bridget Strong Sabrina as Virginia Fritton

Ministry of Education
*Richard Wattis as Manton Bassett
*Eric Barker as Culpepper-Brown Peter Jones as Prestwick
*Michael Ripper as Eric the liftman

==Production==
As Miss Fritton, Sim appears in only two scenes.

A leading model at the time, Sabrina got high billing, appearing in all the posters and publicity stills in school uniform, but she actually had a non-speaking part in which she was only required to lounge in bed reading a book while men hovered around her. She is described as the "school Swotvac|swot", the only pupil to go to bed on time and where she reads the works of Fyodor Dostoyevsky|Dostoyevsky.

Thorley Walters was to re-appear in The Pure Hell of St Trinians, effectively replacing Richard Wattis as Culpepper-Browns nerve-racked assistant. He was also to play the part of Culpepper-Brown himself in The Wildcats of St Trinians in 1980. 

It was Rosalind Knights first credited film role. She too would later appear in The Wildcats film, this time as a teacher.

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 