Burebista (film)
{{Infobox film
| name           = Burebista
| image          = Burebista film.jpg
| image_size     =
| caption        = Akornion is greeted by Burebista and Comosicus (film poster)
| director       = Gheorghe Vitanidis
| producer       = Gheorghe Pîrîu
| writer         = Mihnea Gheorghiu
| starring       = George Constantin Ion Dichiseanu Emanoil Petruț Alexandru Repan
| music          = Theodor Grigoriu
| cinematography = 
| editing        = Iolanda Mîntulescu; Cristina Ionescu
| distributor    =
| released       =  
| runtime        = 137 minutes
| country        = Romania
| language       = Romanian
| budget         =
| gross          =
}}

Burebista (1980) is a Romanian historical epic film about the life of the ancient Dacian king Burebista, depicting his battle to unify his nation and to resist Roman incursions. 

The film was made to commemorate the supposed 2050th anniversary of the founding of the "unified and centralized" country that was to become Romania. The government of Nicolae Ceaușescu claimed an uninterrupted existence of the state from Burebista to Ceaușescu himself. 

==Plot==
 ) with which the film begins and ends]] a rocky outcrop, shaped like a human head, apparently gazing out over the Carpathian Mountains. A voice-over explains that the expansion of Roman power is beginning to threaten the borders of the kingdom of Dacia under its "enigmatic" ruler Burebista.

The high priest Deceneus convinces the Dacian lords to swear allegiance to Burebista as king of a unified Dacia. He promises to keep Dacia free from the increasing power of Rome. A rigorous regime of military training is introduced by the king. Some refugees from Rome arrive, including Calopor, a former gladiator who had served with Spartacus. Burebista welcomes them, asking Calopor to tell him about Spartacus exploits.  Burebista also learns about the ambition of the young Julius Caesar, and his rivalry with Pompey. 

Messengers from Mithridates VI of Pontus and from Greece arrive to ask Burebistas help in resisting the advance of Roman forces.  Burebista agrees to help. Calopor is sent to assess the situation in Greece. While there he meets his former girlfriend Lydia, who has been forced to marry the arrogant Roman aristocrat Gaius Antonius Hybrida. Hybrida demands games in his honour. Calopor takes part as a gladiator, planning to kill Hybrida. When Calopor throws a trident at him, Hybrida pulls Lydia in front of his body, and the trident kills her. Horrified, Calopor leaps up and stabs Hybrida to death, giving the signal for a general uprising. With the Romans in confusion, Burebista launches his cavalry against their retreating forces, defeating them.

Burebista celebrates his victory with a festival in Dacia. He learns from a messenger that Mithridates has killed himself to avoid being captured by Pompey, but is given a letter in which the dead king encourages him to continue to resist Rome. Another letter also arrives, from Pompey. Pompey is now asking Burebistas help to curb Caesar. He ponders trouble to come.

The Boii, a Celtic tribe, raids Dacia, capturing and enslaving villagers with the help of a Dacian traitor. Burebista discovers that the raid succeeded because guards were drunk. He orders the destruction of grapevines. He attempts to negotiate with the Celts, telling them that Dacians and Celts need to cooperate to preserve independence from Rome. The Celts, influenced by their fanatical Druid Breza, refuse to negotiate. Forced to fight, Burebista is worried that the war will weaken both sides, giving Caesar an opportunity to invade. The Celts, meanwhile, threaten to kill all their Dacian captives. Deceneus tells Burebista that there will soon be a solar eclipse. Burebista decides to use this to frighten the Celts with the power of the Dacian gods. He tells them that the gods will darken the sun. The eclipse terrifies the Celts, who agree to surrender the captives. Breza flees, pursued by Calopor. When Calopor follows him into a building, Breza stabs him in the back. As he dies, Calopor reveals that he is Burebistas illegitimate son. Breza is captured and executed by being thrown into an altar pyre of the goddess Epona. 

With Celtic tribes now acknowledging his rule, Burebista has created a strong kingdom. In Gaul, Caesar announces that he has conquered the country and absorbed it into the Roman empire. After he has defeated Pompey, he will proceed to absorb Dacia too. As, some time later,  Burebista ponders Caesars vow, he is told that the threat is over — Caesar has been assassinated. Deceneus speaks of the transience of life. Burebista says that all kings must die, but the unity of the nation he has created should be immortal. He gazes over the mountains, and his head fades into a merge with the rock with which the film began.

==Cast==
*George Constantine - Burebista
*Ion Dichiseanu - Calopor, a former follower of Spartacus
*Emanoil Petrut - Deceneus, the high priest 
*Alexander Repan - Akornion, Greek diplomat 
*Ernest Maftei - Prince Castaboca
*Ovidiu Moldovan - Lai, poet and singer 
*Serban Ionescu - drunken peasant 
*Stephen Sileanu - Boian, king of the Boii
*Constantin Dinulescu - Breza, a druid
*Vlad Rădescu - Prince Comosicus
*Nicolae Iliescu - Julius Caesar
*Mircea Anghelescu - Gaius Antonius Hybrida
*Anca Szönyi - Lydia

==Significance==
 
The film was made to coincide with celebrations of the founding of a unified state, presented as the model for modern Romania. According to Lucian Boia, Burebista was constantly compared to Ceaușescu,   

Doru Pop argues that Ceaușescus "national-communist ideology" needed the support of claims that he "descended from a long line of Romanian (!) historical figures, beginning with Burebista, the illusory ruler of the Dacian empire." 

==References==
 

 
 
 
 
 
 
 
 
 
 
 