The Clemenceau Case
 
{{Infobox film
| name           = The Clemenceau Case
| image          = File:The Clemenceau Case lobby card 2.jpg
| caption        = Lobby card
| director       = Herbert Brenon William Fox
| writer         =  Herbert Brenon
| based on       =  
| starring       = Theda Bara William E. Shay
| music          = 
| cinematography = Phil Rosen
| editing        =  Fox Film Corporation
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama film written and directed by Herbert Brenon and starring Theda Bara. The film is based on the French novel Laffaire Clémenceau, by Alexandre Dumas, fils, the film is now considered lost film|lost.   

==Cast==
* Theda Bara as Iza
* William E. Shay as Pierre Clemenceau
* Mrs. Allen Walker as Marie Clemenceau
* Stuart Holmes as Constantin Ritz
* Jane Lee as Janet
* Saba Raleigh as Countess Dobronowska (as Mrs. Cecil Raleigh)
* Frank Goldsmith as Duke Sergius
* Sidney Shields as Madame Ritz

==References==
 

==External links==
* 
* 


 

 
 
 
 
 
 
 
 
 
 
 
 

 