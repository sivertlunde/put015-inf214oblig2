The Dawn Maker
{{infobox film
| name           = The Dawn Maker
| image          =File:The Dawn Maker 1916.jpg
| caption        =Film poster
| director       = William S. Hart
| producer       = Thomas H. Ince
| writer         = C. Gardner Sullivan(story & scenario)
| starring       = William S. Hart
| music          =
| cinematography = Joseph H. August
| editing        =
| distributor    = Triangle Film Corporation
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = silent film..(English intertitles)

}} lost  1916 silent film western directed by and starring William S. Hart and produced by Thomas H. Ince. Triangle Pictures distributed.   


==Cast==
*William S. Hart - Joe Elk
*Blanche White - Alice McRae William Desmond - Bruce Smithson
*J. Frank Burke - Walter McRae
*Joy Goodboy - Chief Troubled Thunder

==References==
 

==External links==
* 
* 
* 

 
 