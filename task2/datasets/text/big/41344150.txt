The Voice (1992 film)
{{Infobox film
| name = The Voice
| image = 
| caption =
| director = Pierre Granier-Deferre
| producer = Patricia Novat Pierre Novat
| writer = Pierre Granier-Deferre Christine Miller
| based on = Pierre Drieu La Rochelle
| starring = Sami Frey Nathalie Baye
| music = Philippe Sarde
| cinematography = Pascal Lebègue
| editing = Marie Castro
| studio = Ciné Feel
| distributor = Acteurs Auteurs Associés
| released =  
| runtime = 86 minutes
| country = France
| language = French
| budget = 
}}
The Voice ( ) is a 1992 French drama film directed by Pierre Granier-Deferre, starring Sami Frey and Nathalie Baye. It is set in Rome and tells the story of a man who suddenly comes upon a women he once was in love with. The film is based on a short story by Pierre Drieu La Rochelle. It was shown in the Panorama section of the 42nd Berlin International Film Festival. 

==Cast==
* Sami Frey as Gilles
* Nathalie Baye as Lorraine
* Laura Morante as Laura
* Jean-Claude Dreyfus as le maitre dhôtel
* Georges Claisse as Michele

==References==
 

 
 
 
 
 
 
 
 
 


 