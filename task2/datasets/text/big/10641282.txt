Sharmeelee
 
{{Infobox film
| name           = Sharmilee
| image          = 
| image_size     = 
| caption        = 
| director       = Samir Ganguly
| producer       =Subodh Mukherjee
| writer         = Gulshan Nanda (story)
| narrator       = 
| starring       = Shashi Kapoor Raakhee Nazir Hussain
| music          = S. D. Burman
| lyrics         = Neeraj
| cinematography = N. V. Srinivas
| editing        = V. K. Naik
| distributor    = 
| released       = 1971
| runtime        = 
| country        = India
| language       = Hindi
| gross          = Rs. 2,60,00,000
}} Asit Sen. Raakhee played a double role in this film, and its success helped make her one of the decades top leading lady in Hindi films.

==Plot==
While returning from Army Base, Captain Ajit Kapoor stops over at a rest-house, where there is a party on, and he meets a charming, vivacious young lady. When he reaches home, his guardian, Father Joseph, wants him to get married. When Joseph(Nazir Hussain) accompanies Ajit to see the girl, whose name is Kanchan, Ajit is thrilled to find out that it is the very same girl he met at the rest-house. Ajit indicates his approval, and the stage is set for them to get married. It is then that Ajit finds out that the Kanchan is not the girl he had met, but her twin-sister. Kanchan is heart-broken at this, but wants her sister to be happy. Ajit is even more happy when he finally meets Kamini, who also recognizes him. Then Ajits Army Colonel summons him for assistance in locating a female spy, who closely resembles Kanchan.

==Cast==
* Shashi Kapoor as Captain Ajit Kapoor
* Raakhee as Kamini/Kanchan (dual role)
* Iftekhar
* Narendra Nath
* Nazir Hussain Jayshree Talpade, in song "Reshmi Ujala Hai"

==Soundtrack==
The soundtrack of Sharmeelee was an instant hit with the Indian audience, with songs composed by S. D. Burman, and sung by Kishore Kumar, Lata Mangeshkar, and Asha Bhosle. The songs had featured on the Binaca Geetmala top 100 songs of 1971.
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Megha Chhaye Aadhi Raat"
| Lata Mangeshkar
|-
| 2
| "Reshmi Ujala Hai Makhmali Andhera"
| Asha Bhosle
|-
| 3
| "Aaj Madhosh Hua Jaye Re"
| Kishore Kumar, Lata Mangeshkar
|-
| 4
| "Kaise Kahen Ham"
| Kishore Kumar
|-
| 5
| "Khilte Hain Gul Yahan (Male)"
| Kishore Kumar
|-
| 6
| "O Meri Sharmilee"
| Kishore Kumar
|-
| 7
| "Khilte Hain Gul Yahan (Female)"
| Lata Mangeshkar
|}

==Box Office==
Sharmeelee was declared hit by Box Office India. Sharmeelee had powerful and promising performances by the lead actors, Shashi Kapoor and Raakhee, and had received both critical as well as commercial success. It had grossed Rs. 2,60,00,000 (nett approx.)  

==External links==
* 
*  http://boxofficeindia.com/showProd.php?itemCat=177&catName=MTk3MQ==

 
 
 
 
 

 