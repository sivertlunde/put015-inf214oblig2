The Wounded Man (film)
{{Infobox film
| name           = The Wounded Man
| director       = Patrice Chéreau
| producer       = Claude Berri Marie-Laure Reyre Ariel Zeitoun
| writer         = Patrice Chéreau Hervé Guibert
| starring       = Jean-Hugues Anglade Vittorio Mezzogiorno
| music          = Fiorenzo Carpi
| released       =  
| runtime        = 109 minutes
| country        = France
| language       = French
}}
 French film directed by Patrice Chéreau, and written by him and Hervé Guibert. It stars Jean-Hugues Anglade and Vittorio Mezzogiorno. The film won the César Award for Best Writing. It was also entered into the 1983 Cannes Film Festival.   

==Cast==
* Jean-Hugues Anglade - Henri
* Vittorio Mezzogiorno - Jean Lerman
* Roland Bertin - Bosmans
* Lisa Kreuzer - Elisabeth
* Claude Berri - Le client
* Hammou Graïa - Le jeune homme de la gare
* Gérard Desarthe - Lhomme qui pleure
* Armin Mueller-Stahl - Le père
* Annick Alane - La mère
* Sophie Edmond - La sœur
* Maria Verdi
* Suzanne Chavance
* Roland Chalosse
* Eddy Roos
* Charly Chemouny

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 


 
 