In Their Skin
{{Infobox film
| name           = In Their Skin
| caption        = 
| image          = Movie_Poster_In_Their_Skin.jpg
| director       = Jeremy Power Regimbal
| producer       = Justin Tyler Close Jeremy Power Regimbal
| screenplay     = Joshua Close
| story          = Joshua Close Justin Tyler Close Jeremy Power Regimbal
| starring       = {{Plainlist|
* Selma Blair
* Joshua Close
* Rachel Miner
* James DArcy
* Alex Ferris
* Quinn Lord
}}
| music          = Keith Power
| cinematography = Norm Li 
| editing        = Austin Andrews
| studio         = TSM Entertainment Sepia Films Téléfilm Canada
| distributor    = IFC Films Kinosmith
| released       =  
| runtime        = 97 minutes
| country        = Canada
| language       = English
| budget         = $4 million
}}
In Their Skin, known in some countries as Replicas,    is a 2012 Canadian home invasion thriller film directed by Jeremy Power Regimbal and starring Selma Blair, Joshua Close, Rachel Miner and James DArcy. The film was released theatrically in the United States by IFC Films   and in Canada by Kinosmith.

The film is about a grieving family who escape to their cottage to reconnect. There, they are terrorized by neighbours with a sadistic agenda.

==Plot==
A family escapes their busy lifestyles to their upscale suburban cottage. Wedded couple, Mary Hughes (Selma Blair), a real estate developer and Mark Hughes (Joshua Close), a lawyer, have an 8-year-old son, Brendon Hughes (Quinn Lord), but grieve over their recently dead 6-year-old daughter, Tess, who died in an accident. They also have a dog named Harris. One night while out for a walk, they come across a vehicle, appearing as if they were being watched. The next day, they meet their neighbors, Jane Sakowski (Rachel Miner), Bobby Sakowski (James DArcy) and their 9-year-old son, Jared Sakowski (Alex Ferris), inviting them over for afternoon lunch. 

That afternoon, the mood becomes unsettling when the Sakowskis bombard the Hughes with endless questions about their lives and overstay their welcome, envying their "perfect" wealthy living. After Brendon invites Jared to his room to defeat him playing games, Jared threatens him to his neck with a knife. This instills panic into Brendon, who flees to alert his parents. Jared pretends to his parents that he was punched by Brendon, to which Brendon denies. After Mary finds marks on Brendons neck, the Hughes dismiss the Sakowskis.

Mark is furious about what they encountered. He smashes a vase of flowers given to him by Jane, as it is witnessed by her husband Bobby through the kitchen window. When their dog, Harris, sets off into the woods for a walk, the Hughes hear the dog get shot and see a shooter emerge. They shift into lock down mode and Mark arms himself, while Mary searches for her phone to call the police. Mark soon finds his family vehicle has its tires punctured. Stepping outside, he sees Jared, who tells him Bobby is burying the dog. Jared runs off after using rocks to smash two windows of the Hughes cottage and almost hits Mark with one as well. Mark gives his hand gun to Mary and instructs her and Brendon to lock themselves in the bathroom while he goes out to search for their dog. 

Taking a kitchen knife, Mark searches through the woods, but he is captured by Bobby and returned to the cottage where the rest of Sakowski members turn up. Bobby, carrying a shotgun, manipulates Mary and Brendon out of the bathroom, taking the hand gun from Mary. Mary pleads to take care of Jane if they let her family loose. However, Jane concedes to the family that Jared is her younger brother and they were removed out of a dark family life by Bobby, whose wife died of cancer, as Jane refuses to betray him. Bobby also reveals he raised his family in a run down station wagon and plans to assume the lives of the Hughes.

Abruptly, Marks brother Toby (Matt Bellefleur) shows up to the cottage. The Hughes and Sakowskis put up a pretentious act of harmony. Jared holds Brendon hostage upstairs. After an awkward meeting with the Sakowskis, Toby goes to retrieve his bags from the car, and Bobby shoots him dead in the head, leaving Mark and Mary emotional. The Sakowskis take control of the house, ordering Mark and Mary around, forcing them to perform sex in front of them. As Mary is forced to dry hump on top of Bobby, she stabs him with a knife, rendering him weak. The Hughes gain control shortly, but then Jared threatens Brendon at gun point. Later, Mark grabs his gun from Jared. A weakened Bobby says he wants to live the life of perfection, but Mark reveals his family is not perfect and that he killed his daughter in an accident. As Bobby tries to attack him, Mark shoots him dead. Mark consoles his family, and the rest of Sakowskis are taken into custody by the police. In the final scene, Mark and Mary are interviewed, showing theyve drawn closer as a couple.

==Cast==
*Selma Blair as Mary Hughes
*Joshua Close as Mark Hughes
*Rachel Miner as Jane Sakowski
*James DArcy as Bobby Sakowski 
*Quinn Lord as Brendon Hughes
*Alex Ferris as Jared Sakowski
*Leanne Adachi as the Medic
*Matt Bellefleur	as Toby
*Allie Bertram as Bridget
*Agam Darshi as the Nurse
*Brett Delaney as the Man in the headlights
*Debbe Hirata as the Therapist
*Terence Kelly as the Station Attendant

==Production==
Filming took place in Fort Langley, British Columbia.   

==Critical reception== average score of 5.1/10, based upon a sample of 10 reviews.   

==Accolades==
{| class="wikitable" rowspan=5; style="text-align: center; background:#ffffff;"
!Year!!Award!!Category!!Recipient!!Result!!Ref.
|- 34th Young 2013
|Young 34th Young Best Performance Alex Ferris|| ||   
|}

==See also==

*List of films featuring home invasions

==References==
 

==External links==
*  
*  
*  

 
 
 
 