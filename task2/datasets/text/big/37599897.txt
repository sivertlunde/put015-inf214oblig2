Simhadriya Simha
{{Infobox film
| name           = Simhadriya Simha
| image          = Simhadriya Simha poster.jpg
| image size     =
| caption        = Film poster
| director       = S. Narayan
| producer       = 
| story          = Erode Sounder
| based on       = Tamil film Nattamai
| screenplay     = 
| narrator       = Meena Bhanupriya Umashri Mukhyamantri Chandru Renuka Prasad  Deva
| cinematography = 
| editing        = P. R. Soundarraj
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
}}
 Kannada Film Meena and Tamil film Nattamai which starred Sarath Kumar and Kushboo in lead roles, but  Simhadriya Simha became more successful than the original.  

 

==Cast==
* Vishnuvardhan (actor)|Dr.Vishnuvardhan Meena
* Bhanupriya
* Shobhraj
* Abhijeeth Umashri
* Mukhyamantri Chandru
* Renuka Prasad
* Rekha Das

==Story==
The story revolves around Narasimhe Gowda (Dr.Vishnuvardhan), who is the chieftain for a group of 48 villages. He is the man with the strength of an elephant and who rules the Simhadri village as a lion rules his kingdom. The sincerity and the wisdom in the judgement makes the people go to Narasimha for justice and not the police. Shobhraj, one of the relative of Narasimha Gowda rapes a girl and is banished from the village for 18 years. He also has to marry the girl according to the judgement by the Gowda. Such a harsh judgement makes Shobrajs father kill Narasimhe Gowda. The throne is ascended by the elder son of Narasimha Gowda,who is also portrayed by Vishnuvardhan as a just and powerful ruler. Things take a turn after 18 years when the Simhadriya Simha family has to undergo a similar situation. Due to a twist of fate, Gowda has to decide his brother Chikkas (again played by Vishnuvardhan) fate. The judgement - Chikka is ordered to leave the village for 10 years. Truth is unveiled when Shobrajs mother reveals the involvement of her son in falsely implicating Chikka. The stage is then set for a perfect climax.

==Performances, Reviews and Audience response==
All the reviews for the movie were positive. Vishnuvardhan (actor)|Dr.Vishnuvardhans acting was the highlight of the film and had got great reviews everywhere.

The film became a huge hit in all the parts of Karnataka and completed 25 successful weeks in the theaters. It created craze among the audience and songs like Malnad Adike, Priya Priya and Simhaadriya Simha, all sung by S. P. Balasubrahmanyam were huge hits and were regularly played at functions, shows etc.

==Soundtrack== Deva  with lyrics penned by the director S. Narayan himself.
{| class="wikitable"
|- style="background:#ff9; text-align:center;"
! Track# !! Song !! Singer(s) 
|-
| 1 || "Barthanavva" ||  S.P. Balasubrahmanyam, K.S. Chitra
|-
| 2 || "Kallaadare Naanu" || S.P. Balasubrahmanyam
|-
| 3 || "Malnad Adike" || Rajesh Krishnan, S.P. Balasubrahmanyam, Sangeeta Gururaj
|-
| 4 || "Priya Priya" || S.P. Balasubrahmanyam
|-
| 5 || "Simhadriya" || S.P. Balasubrahmanyam
|-
| 6 || "Yajamaana" || S.P. Balasubrahmanyam, K.S. Chitra
|}

==References==
 

 
 
 
 
 
 