The Huggetts Abroad
{{Infobox film
| name           = The Huggetts Abroad
| image_size     = 
| image	         = The Huggetts Abroad FilmPoster.jpeg
| caption        = 
| director       = Ken Annakin
| producer       = Betty E. Box
| writer         = 
| narrator       =  Jack Warner Kathleen Harrison Susan Shaw Petula Clark
| music          = Antony Hopkins
| cinematography = Reginald H. Wyer
| editing        = Gordon Hales
| studio         = 
| distributor    = 
| released       =  
| runtime        = 89 mins
| country        = United Kingdom
| language       = English
}}
 Jack Warner, The Huggetts. The film was less commercially successful than its predecessors. A sequel, Christmas with the Huggetts, was planned but never made.

==Plot==
After Joe Huggett loses his job, the family decide to emigrate to South Africa, travelling via a land route that takes them across Africa. On their journey they become entangled with a diamond smuggler.

==Cast== Jack Warner as Joe Huggett 
* Kathleen Harrison as Ethel Huggett 
* Susan Shaw as Susan Huggett 
* Petula Clark as Pet Huggett 
* Dinah Sheridan as Jane Huggett  Hugh McDermott as Bob McCoy 
* Jimmy Hanley as Jimmy Gardner  Peter Hammond as Peter Hawtrey 
* John Blythe as Gowan 
* Amy Veness as Grandma Huggett 
* Peter Illing as Algerian Detective 
* Frith Banbury as French Doctor 
* Olaf Pooley as Straker 
* Esma Cannon as Brown Owl
* Sheila Raynor as Woman with Straker

==External links==
* 

 
 

 
 
 
 
 
 