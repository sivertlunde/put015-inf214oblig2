The Golden Cage (1975 film)
{{Infobox film
| name = The Golden Cage
| image =
| caption =
| director = Ayten Kuyululu
| producer = Ilhan Kuyululu
| writer = Ayten Kuyululu Ismet Soydan
| based on =
| starring = Sait Misoglu
| cinematography = Russell Boyd
| music = Mary Vanderby Aspidistra
| editing = Harold Lander
| studio = Independent Artists
| distributor =
| released =  
| runtime = 70 minutes
| country = Australia
| language = English Turkish
| budget =
}}
The Golden Cage is a 1975 Australian film about two Turkish migrants in Australia made by the husband and wife team Ayten and Ilhan Kuyululu. It was the first Australian film directed by a woman since the 1930s. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p281 

==Production==
The film was shot in March and April 1975, with the aid of $22,500 from the Film, Radio and Television Board of the Australian Council. It failed to achieve commercial distribution. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 291 

Kuyululu tried to get up a film The Battle of Broken Hill about the 1915 Battle of Broken Hill but was unable to secure finance and returned to Turkey. 

Phillip Noyce worked as an assistant director on the movie.

==References==
 

==External links==
*  at Australian Screen Online
* 
*  at Oz Movies

 
 
 


 