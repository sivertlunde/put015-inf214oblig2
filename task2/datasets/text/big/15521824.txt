Bull's Eye (serial)
 
{{Infobox film
| name           = Bulls Eye
| caption        =
| image	         = Bulls Eye 1918.jpg
| director       = James W. Horne
| producer       =
| writer         = Frank Howard Clark Harvey Gates Tom Gibson James W. Horne Henry MacRae Blaine Pearson Vivian Reed
| cinematography =
| editing        = Universal Film Manufacturing Co.
| released       =  
| runtime        = 18 episodes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}

Bulls Eye is a 1917 American film serial directed by James W. Horne. It is now considered to be lost film|lost.   

==Cast==
* Eddie Polo - Cody Vivian Reed - Cora Clayton
* Frank Lanning - Nathan Loose
* Ray Hanford - John Clayton William Welsh - John North
* Hallam Cooley - Lee McGuire
* Noble Johnson - Sweeney Bodin
* Leo Willis
* Wallace Coburn

==Chapter titles==
# First Blood
# The Fearless One
# Desperate Odds
# Still In The Ring
# The Swing of Death
# On The Brink
# Riding Wild
# Dynamite
# The Flaming Crisis
# Coyotes of The Desert
# Fired
# Burning Sands
# Sold At Auction
# The Firing Squad
# The Stained Face
# Running Wild
# In Irons
# The Runaway

==See also==
* List of American films of 1917
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 