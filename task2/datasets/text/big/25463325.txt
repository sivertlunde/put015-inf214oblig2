List of Telugu films of 1995
 
  Tollywood (Telugu Hyderabad in the year 1995 in film|1995.

==1995==
{| class="wikitable"
|-
! Title !! Director !! Cast !! Music director !! Sources
|- Koti ||
|- Indraja || Raj-Koti||
|- Ooha || Vandemataram Srinivas ||
|- Rami Reddy, Vadivukkarasi || Sri ||
|- Koti ||
|- Big Boss|| Madhavi || Raj-Koti ||
|-
|Bombay (film)|Bombay || Mani Ratnam || Arvind Swamy, Manisha Koirala, Nasser, Prakash Raj || A. R. Rahman ||  
|- Ali || S. V. Krishna Reddy ||
|-
|Gharana Bullodu|| K. Raghavendra Rao || Akkineni Nagarjuna, Ramya Krishna, Aamani  || M. M. Keeravani ||
|- Koti ||
|-
|Mathopettukoku|| A. Kodandarami Reddy || Nandamuri Balakrishna, Rambha (actress)|Rambha, Roja Selvamani || Madhavapeddi Suresh ||
|-
|Money Money || Shiva Nageswara Rao || Jagapathi Babu, Urmila Matondkar, J. D. Chakravarthy, Jayasudha || K. Srinivas ||
|- Koti ||
|-
|Pokiri Raja|| A. Kodandarami Reddy || Daggubati Venkatesh, Roja Selvamani || ||
|-
|Pelli Sandidi || K. Raghavendra Rao || Meka Srikanth|Srikanth, Ravali, Deepti Bhatnagar || M. M. Keeravani || 
|-
|Rangeela (film)|Rangeli || Ram Gopal Varma || Aamir Khan, Urmila Matondkar, Jackie Shroff || A. R. Rahman ||  
|-
|Rikshavodu|| Kodi Ramakrishna || Chiranjeevi, Naghma, Soundarya, Jayasudha ||Raj-Koti ||
|- Sathi Leelavathi || Balu Mahendra || Kamal Hassan, Ramesh Aravind, Kovai Sarala, Kalpana (Malayalam actress)|Kalpana, Heera Rajagopal || Ilayaraja ||  
|-
|Sogasu Chuda Taramaa? || Gunasekhar || Naresh (actor)|Naresh, Indraja (actress)|Indraja, Tanikella Bharani || Bharathwaj ||
|-
|Sisindri|| Shiva Nageswara Rao || Akkineni Nagarjuna|Nagarjuna, Tabu (actress)|Tabu, Amala (actress)|Amala, Aamani, Akhil || Raj ||
|- 
|Shubha Sankalpam || K. Vishwanath || Kamal Hassan, Aamani, Priya Raman || M. M. Keeravani ||
|-
|Tajmahal || Muppulaleni Siva || Meka Srikanth|Srikanth, Monica Bedi || M. M. Sreelekha ||
|-
|}
==References==
 
 
 
 

 
 
 