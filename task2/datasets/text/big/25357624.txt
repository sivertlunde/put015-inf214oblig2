Yuddham Sei
{{Infobox film
| name           = Yuddham Sei
| image          = Yuddham-Sei-Movie.jpg
| caption        = poster Myshkin
| producer       = Kalpathi S. Aghoram Myshkin
| Cheran Dipa Selva
| K
| cinematography = Sathya
| editing        = Gagin
| distributor    =
| studio         =  
| released       =  
| country        = India
| runtime        = 152 minutes
| language       = Tamil
| gross          =
}} Tamil crime crime thriller Cheran in the lead role along with debutant Dipa Shah, Y. Gee. Mahendra, Lakshmi Ramakrishnan and Jayaprakash. The film was released on 4 February 2011.

==Plot==
J.K. (Cheran (director)|Cheran), a brooding CB-CID officer, is worried about his missing sister. His senior officer promises him that he can take up his sisters case after he solves another important one, so J.K. agrees. Amputated male arms are packed in cartons and left in public places, possibly as a message to the police. The missing people are identified as Raja Manikam, Moorthy etc., based on identification from their families. By triangulating the co-ordinates they find a lead in this case.

Durai Pandi (Manicka Vinayagam) is a leading textile shop owner who was beaten, insulted in public and handed over to the police after it was found that he had used a peephole to look into the women’s dressing room. He is caught by the victim Sujas family, who are totally shattered by this incident. The inspector in-charge of this case is Isakki Muthu. Later, Durai Pandis manager accepts the blame for the crime, clearing Durai Pandis name.

Later, Sujas father Dr. Purushothaman (Y. Gee. Mahendra) is arrested on charges of bribery, and then released. A sex affair complaint is lodged against Dr. Purushothaman’s wife (Lakshmy Ramakrishnan) by John Britto. These events shatter the family, and they commit suicide by setting themselves on fire. But Suja is still missing, so J.K. decides to reopen the case.

More amputated arms turn up, and the head of a police man from ACP Tirisangus (Selva (actor)|Selva) station is kept in front of the police station. This terrifies the police department, and they intensify the investigation. J.K. suspects that there are some dishonest policemen in ACP Tirisangus station, as all the crimes have happened in Tirisangus jurisdiction.

In the mean time Tirisangu’s gang panic that J.K. would identify them, and try to kill him. J.K’s sister is held captive by Tirisangu ever since she witnessed one of their crimes. He tells her that some day she would prove useful to him.

Mortician Judas (Jayaprakash) offers to show Durai Pandi and his elderly associates a video. He leads them to a van with a laptop, and shuts the door after them. Smoke fills the inside of the van. J.K. appears on the scene, as he has been following Judas. Tirisangu also follows J.K. and shoots Judas. Two of Judas’s associates get into the van and drive away, and the third one, a youngster, is left behind. It turns out that the three of them are Dr Purushothaman, his wife and son, who are believed to be dead.
  shaved her head and gone completely bald to play the role of Annapoorni Purushothaman. She called the character as a "once-in-a-lifetime role"    Her dedication and performance was met with rave reviews.]]

Before dying, Judas confesses to J.K.

Dr.Purushothaman’s daughter Suja was kidnapped, drugged unconscious and raped, in front of the elderly members of Tirisangu’s gang, for their perverse enjoyment. She is just one of many victims. Afterwards, Suja was found wandering in shock, and brought home. She commits suicide the next day. The family is devastated. They want to die, but after killing everyone responsible for Sujas death. With assistance from Judas, they fake their suicides and go missing. They kidnap John Britto, Raja Manikam and auto driver Moorthy who were involved in the kidnap and amputate all of them. They then kill the policeman who had arranged for Sujas kidnap.

Judas dies after this confession.

Purushothaman’s son Nishanth is now in J.K.’s custody. Tirisangu calls J.K. and tells him that J.K. needs to lead him to the doctor’s family if he wants to see his sister alive. J.K. is devastated and helpless. Nishanth, who hasn’t spoken a word till then, tells Tirisangu where his parents are.

J.K. and his subordinates reach there at the same time as Tirisangu and Isakki Muthu. They see that the old men who were kidnapped are now blinded.

The doctor and his wife kill Isakki Muthu, and they approach Tirisangu with intent to kill, his bullets merely slowing them down. When he threatens to kill J.K.’s sister, the doctor’s family see her as their daughter, and give up their lives to protect her. They die with the satisfaction that they have saved the girl’s life. J.K. kills Tirisangu.

The older members of Tirisangu’s gang are sentenced to 7 years in prison, and Durai Pandi gets 13 years in prison. The court suspends J.K. and his team mates for letting Nishanth escape. As the ruling is heard, J.K.’s sister and his subordinate are shown playing on the beach, with J.K. watching over them protectively. Then they all gaze at the horizon.

J.K. helps send Nishanth aboard, after giving him a book Mans Search for Meaning by holocaust survivor and philosopher Viktor Frankl, and advice to help him deal with his grief.

The film ends with Nishanth at the airport, heading off to an unnamed foreign country to begin a new life for himself.

==Cast== Cheran as J. Kannan (JK)
* Dipa Shah as Thamizhselvi
* Y. Gee. Mahendra as Dr. Purushothaman
* Lakshmi Ramakrishnan as Annapoorni Purushothaman
* Jayaprakash as Dr. Judas Iscariot Selva as Trisangu
* Manicka Vinayagam as Duraipandi
* G. Marimuthu as Esakki Muthu
* Shankar as Prakash
* E. Ramadoss
* Yugendran as Inba
* Aadukalam Naren as Chandramouli Shruthi as Charu
* Sunil Choudhary as Nishant
* Srushti Dange as Suja
* Charulatha Mani (Cameo appearance)
* Neetu Chandra (Special appearance)
* Ameer Sultan (Special appearance)
* Charu Nivedita (special appearance)

==Critical reception== IANS gave 3 stars out of 5 and wrote, "Despite slow second-half, the films does impress as a smart and realistic detective story".  Behindwoods gave 2.5 stars out of 5 and called it "A slow paced well made thriller".  Deccan Herald wrote, "Stylised camerawork, haunting score and decent performances keep one interested, though the film is pulled down by its own weighty expectations. Still, Yuddham... is worth its while and wallet".  Indiaglitz wrote, "Yudham Sei is absolutely watchable just for Mysskin and his way of story telling". 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 