Bhadradeepam
{{Infobox film 
| name           = Bhadradeepam
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = Sharada T Sathyadevi
| writer         = M. Krishnan Nair (screenplay) S. L. Puram Sadanandan (dialogues)
| based on     = Durgam by P. R. Shyamala Sharada Sujatha Sujatha
| music          = MS Baburaj
| cinematography = SJ Thomas
| editing        = VP Krishnan
| studio         = Sree Saradasathya Combines
| distributor    = Sree Saradasathya Combines
| released       =  
| country        = India
| language       = Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, Sharada and Sujatha in lead roles. The film had musical score by MS Baburaj.   

==Plot==
Rajasekharan, a wealthy businessman, is married to Usha, and is leading a happy family life. One day Usha is found dead and Rajasekharan suspects she was poisoned by her close friend Rajani. In order to take revenge, he marries Rajani and forces upon her a bleak life devoid of all pleasure. But eventually Rajasekharan discovers that Rajani had nothing to do with his Ushas death but it is too late by then.

==Cast==
*Prem Nazir as Rajasekjaran Sharada as Rajani Sujatha as Usha
*Jose Prakash as Venu, the Doctor
*Adoor Bhasi as Devarajan Potty and Unni Swamy
*T. R. Omana as Lakshmikutty Amma
*T. S. Muthaiah as Ushas father
*K. P. Ummer as Prakash, the Manager
*Philomina as Rajanis mother Vincent as Mohan, Ushas brother
*Shoba as Lekha, Rajanis sister
*Saraswathi Khadeeja

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Vayalar Ramavarma and K Jayakumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Deepaaradhana Nada || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 2 || Kaalindi Thadathile Radha || S Janaki || Vayalar Ramavarma || 
|-
| 3 || Kannukal Karinkoovala || S Janaki || Vayalar Ramavarma || 
|-
| 4 || Mandaaramanamulla Kaatte || K. J. Yesudas || K Jayakumar || 
|-
| 5 || Vajrakundalam Manikkaathil aniyum || P Jayachandran, B Vasantha || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 