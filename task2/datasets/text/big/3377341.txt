Shakha Proshakha
{{Infobox film name           = Shakha Proshakha (Branches of the Tree) image          = Sakha prasakha dvd sray.jpg director       = Satyajit Ray producer       = Satyajit Ray Productions, Gérard Depardieu (D. D. Productions), Daniel Toscan Du Plantier (Erato Films) and Soprofilms writer         = Satyajit Ray starring       = Ajit Bannerjee Haradhan Bannerjee Deepankar Dey Soumitra Chatterjee Ranjit Mullik Soham Chakraborty Lily Chakravarty Mamata Shankar distributor    = cinematography = Barun Raha editing        = Dulal Dutta released       = 12 June 1992 runtime        = 130 min. country        = India language  Bengali
|budget         =
}}

Shakha Proshakha ( ) is a 1990 Satyajit Ray film. It deals with four generations of a well-to-do Bengali family, with a focus on the third generation.

==Plot==

Ananda Majumdar, a wealthy, retired industrialist, suddenly falls ill during a ceremony in honor of his seventieth birthday. Three of his sons rush immediately to his bedside. The 2nd son, Proshanto, already lives with his father. He spends his time listening to music and is mentally ill.

The two eldest sons, who are businessmen, live corrupt lives and do not want their father, an uncompromising moralist who believes in work and honesty, to find out. The youngest of the four, weary of office life, would love to become an actor. During a family meal and a picnic, tensions are accentuated.  Waiting for resolution of their fathers health crisis, the four brothers and their families reminisce and relive ideological differences.

==Other credits==

===Credits===
{| class="wikitable sortable"
|-
| Producer  || Satyajit Ray Productions, Gerard Depardieu (D. D. Productions), Daniel Toscan Du Plantier (Erato Films) and Soprofilms
|-
| Original Screenplay & Direction || Satyajit Ray
|-
| Cinematography || Barun Raha
|-
| Editing || Dulal Dutta
|-
| Art Direction || Ashoke Bose
|-
| Sound || Pierre Lenoir, Sujit Sarkar, Jyoti Chatterjee
|-
| Music || Satyajit Ray, Bach, Beethoven
|}

===Cast===

{| class="wikitable sortable"
|-
! Character !! Relation !! Performer !! Notes
|-
| Ananda Majumdar || Main || Ajit Banerjee || NA
|-
| Probodh || Eldest son || Haradhan Banerjee || NA
|-
| Proshanto || Second son || Soumitra Chatterjee || NA
|-
| Probir || Third son || Deepankar Dey || NA
|-
| Protap || Fourth son || Ranjit Mullik || NA
|-
| Uma || Probodhs wife || Lily Chakravarty || NA
|-
| Tapti || Probirs wife || Mamata Shankar || NA
|- Soham Chakraborty|Soham || NA
|}

==External links==
* 
* , UCSC
*  

 

 
 
 
 
 


 