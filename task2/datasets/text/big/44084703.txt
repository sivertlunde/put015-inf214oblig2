Anupallavi (film)
{{Infobox film 
| name           = Anupallavi
| image          =
| caption        = Baby
| producer       = Raghu Kumar Dheera Vijayan Balakrishnan (dialogues) Baby
| starring       = Jayan Sukumari Srividya Balan Kattoor
| music          = KJ Joy
| cinematography = Vipin Das
| editing        = K Sankunni
| studio         = Dheera
| distributor    = Dheera
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, Baby and produced by Raghu Kumar and Dheera. The film stars Jayan, Sukumari, Srividya and Balan Kattoor in lead roles. The film had musical score by KJ Joy.   

==Cast==
*Jayan
*Sukumari
*Srividya
*Balan Kattoor
*Kuthiravattam Pappu
*Nellikode Bhaskaran Ravikumar
*Seema Seema
*Vallathol Unnikrishnan Bhavani

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Bichu Thirumala and Vijayan (actor)|Vijayan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aayiram Maathalappookkal || P Jayachandran || Bichu Thirumala || 
|-
| 2 || En Swaram Poovidum || K. J. Yesudas || Bichu Thirumala || 
|-
| 3 || Navamee Chandrikayil || P Susheela || Bichu Thirumala || 
|- Vijayan || 
|-
| 5 || Ore Raaga Pallavi || K. J. Yesudas, S Janaki || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 