Circus (1936 film)
{{Infobox film name            = Circus image           = Circus_poster.jpg caption         = Film poster director        = Grigori Aleksandrov Isidor Simkov producer        = writer          = Grigori Aleksandrov starring  James Patterson music           = Isaak Dunayevsky cinematography  = Vladimir Nilsky Boris Petrov editing         = distributor     = studio          = Mosfilm released        = 1936 runtime         = 94 min. country         = Soviet Union language        = Russian budget          =
}}
 Soviet melodramatic musical film. It was directed by Grigori Aleksandrov and Isidor Simkov (as I. Simkov) at the Mosfilm studios. In his own words, it was conceived as "an eccentric comedy...a real side splitter."
 Song of the Motherland" (Широка страна моя родная).

==Plot==
  American circus black baby racism and ethnicities taking turns. 

The film was digitally colorized in 2011 in Russia

== Cast ==
* Lyubov Orlova as Marion Dixon, American actress and circus artist James Patterson as Jimmy, Marions baby
* Sergei Stolyarov as Ivan Petrovich Martinov, Soviet performance director
* Pavel Massalsky as Franz von Kneishitz, corrupt theatrical agent
* Vladimir Volodin as Ludvig, Soviet circus director
* Yevgeniya Melnikova as Rayechka, the directors daughter
* Aleksandr Komissarov as Skameikin
* Nikolai Otto as Charlie Chaplin
* Solomon Mikhoels as Cameo

==See also==
*List of musical films
*List of racism-related films
 

==External links==
*  in black and white.
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 