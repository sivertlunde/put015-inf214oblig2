The Users (1978 film)
{{Infobox television film bgcolour     =  name         = The Users image        = The Users VHS cover.jpg image_size   = 200px  image_sized  =   caption      = VHS cover format  Drama
|runtime      = 150 minutes director  Joseph Hardy producer     = Dominick Dunne writer       = Joyce Haber (novel) Dominick Dunne Robert J. Shaw starring  George Hamilton Darren McGavin Carrie Nye Michelle Phillips editing      = Byron Buzz Brandt music        = Maurice Jarre country      = United States language  English
|first_aired  = October 1, 1978 network  ABC
|released     = 
}}
 Joseph Hardy. The film, whose executive producer was Aaron Spelling, is based on a Joyce Haber novel released in the same year.  The film focuses on the insiders of the Hollywood film industry.

== Plot == prostitute with Phoenix on his latest film The Last Man. Since the death of his wife, who committed suicide by overdose seven years ago, Randy has retired from the screen and he is now seen as a has-been. The Last Man is his first venture since the tragedy, but he has trouble shooting the love scenes. Elena successfully seduces him and he soon falls in love with her. It does not take long before she moves in with him in his mansion in Hollywood, Los Angeles, California|Hollywood.

In Hollywood, she is immediately welcomed by all the insiders, including Randys celebrity daughter Marina (Michelle Phillips|Phillips) and wealthy socialite Grace St. George (Joan Fontaine|Fontaine). Elena and Marina become close friends and Marina tells her about her disastrous love life, which exists mainly out of movie producers, who use her for their own gain. The only man she trusts is a man married to a powerful woman. Elena later learns from her masseur Harvey Parkes (Baseleon) that the married man is Adam, whose intimidating wife Nancy (Carrie Nye|Nye) is aware of the affair.
 Las Vegas, Nevada and turns to Grace for help to become a Hollywood insider as well, visiting the right parties and wearing the right clothes. Meanwhile, Randys film is nearing its release. Marty Lesky (Feinstein) is determined on releasing it as a made-for-TV movie, claiming it is too bad for a theatrical release. Elena and Marina try to prevent Randy from fading from the screen again and try to get him his next movie role before the film comes out.

Elena seduces Warren Ambrose (Red Buttons|Buttons), a powerful studio owner whose newest project Rogues Gallery promises to be a huge success. They film makers are reluctant to hire Randy, claiming Robert Redford or Paul Newman are more suited for the role. Warren suggests Elena to talk to Reade Jamieson (John Forsythe|Forsythe), the wealthy executive producer. They immediately feel attracted to each other and Reade tells her the man who is casting the roles is Henry Waller (Darren McGavin|McGavin), the screen writer. She returns home and catches Randy with Andrew Lyons (Warner), a screen writer who wants to cast Randy in his newest project.

Elena, furious to find out this way about her husbands sexuality, leaves him. However, when she finds out that he tried to commit suicide later that evening, she takes him back. Meanwhile, Marina finds out Adam is just like any other man, when Nancy reveals that he only slept with her so she would play the female lead in Rogues Gallery. With the help from Grace, Elena throws Henry a party, but he refuses to come, explaining he will not consider casting Randy. She visits him in his hotel and is successful in seducing home. She spends the night with him, thereby ensuring Randy the lead role. She returns home the next day and splits with Randy after giving him the news. In the end, Rogues Gallery is a contender for the 51st Academy Awards. Randy and Marina are both very famous again and Elena and Reade are happily married.

==Cast==
*Jaclyn Smith as Elena Scheider, a prostitute who dreams of a glamorous life in Hollywood.
*Tony Curtis as Randall Randy Brent, an actor who was once very famous, but is now a has-been.
*John Forsythe as Reade Jamieson, a powerful business magnate who is the executive producer of Rogues Gallery.
*Michelle Phillips as Marina Brent, Randys daughter who is an actress as well. George Hamilton as Adam Baker, a film producer who has an affair with Marina.
*Joan Fontaine as Grace St. George, a wealthy socialite who knows all the gossip in Hollywood.
*Darren McGavin as Henry Waller, the writer of the novel Rogues Gallery and its screenplay.
*Red Buttons as Warren Ambrose, the owner of a film studio who uses his position to seduce women.
*Alan Feinstein as Marty Lesky, a press agent and lawyer who is known as an infamous womanizer.
*Carrie Nye as Nancy Baker, Adams wealthy and intimidating wife, who is aware of his affair with Marina.
*Michael Baseleon as Harvey Parkes, the Brents masseur who knows all the latest gossip.
*John Pleshette as Kip Nathan, the director of The Last Man.
*Joanne Linville as Elenas Mother, who is under the care of Elena since her health worsened. gay screenwriter who has affairs with several powerful Hollywood men.
*Judy Landers as Merry Redfield, an aspiring actress.

==Reception==
The Users was received with mixed reviews. The Video Movie Guide gave the film two out of five stars, praising its cast but called it a standard television production with standard dialogue. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 