Un rey en la Habana
{{Infobox film
| name           = Un rey en la Habana
| image          = Un rey en la habana dvd.jpg
| image_size     = 200px 
| caption        = DVD release cover
| director       = Alexis Valdés
| producer       = Pancho Casal   José María García
| writer         = Alexis Valdés
| narrator       = 
| starring       = Alexis Valdés
| music          = Edesio Alejandro
| cinematography = 
| editing        = 
| distributor    = Iroko Films   Continental Producciones   Imagia Freelance   Cubaname
| released       = April 25, 2005
| runtime        = 95 minutes
| country        = Spain Spanish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Un rey en la Habana (A king in Havana) is a film produced in 2005 starring Alexis Valdés.

== Plot ==
Papito is a young actor grown up in "El Mamey", the most dangerous marginal district of La Habana, which he dreams to leave someday along with his small theater group.
Don Arturo arrives in Cuba full of promises and souvenirs. But the millionaire does not last more than 24 hours. During his first "encounter" with Yoli he dies from a cardiac arrest because of overdose of sexual enhancement drug. In the family the panic spreads: They have lost the great opportunity that was going to let them out of the misery.
When Papito thinks that nothing could go worse, he receives an "order" from "La Caimana". He must be taken for dead, travel with Yuri to Spain and get all the Euros he can. In spite of the danger and the threats, Papito thinks its an opportunity to make amends with his love and accepts the treatment.
In order to escape the confusion, Papito will have to make the best out of his talent and benefit from his double identity.

== Cast ==
* Alexis Valdés (Papito/Don Arturo)
* José Téllez (Yuri)
* Yoima Valdés (Yoli)
* Alicia Bustamante (La Caimana)
* José Alias (Pistolita)
* Manuel de Blas (Cayetano) Anthony Blake (El Mago)
* Antonio Dechent (Chano)
* María Isabel Díaz (La Gorda) Paulina Gálvez (Gabriela)
* Carmen Machi (Estrella)
* Manuel Manquiña (Police officer)
Leonel Valdes (actor) was never mentioned in the cast. He interpreted a very important role in the film. This film was one of his latest incursion in the film industry.

== Trivia ==
*The pork served at the reception for Yoli and Papitos wedding was from Don Arturos body. When some suspicious guests mention that it tastes funny, La Caimana replies "Its from a Spanish Pig".

== See also ==
* Alexis Valdés
* Cuban Spanish

==References==
 
 

== External links ==
*  
*  

 
 
 
 
 


 
 