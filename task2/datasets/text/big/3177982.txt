The Flesh Eaters (film)
 
{{Infobox Film
| name            = The Flesh Eaters
| image           = Flesheatersposter.jpg
| caption         = Theatrical Poster Jack Curtis
| producer        = Jack Curtis Terry Curtis Arnold Drake
| writer          = Arnold Drake
| starring        = Martin Kosleck Rita Morely Byron Sanders
| music           = Julian Stein
| cinematography  = Carson Davidson
| editing         = Radley Metzger
| distributor     = Cinema Distributors of America
| released        = March 18, 1964
| runtime         = 87 minutes
| country         = United States English
}}
 science fiction Jack Curtis and edited by future filmmaker Radley Metzger.  The film contains moments of violence much more graphic and extreme than many other movies of its time, making it one of the first ever gore films. 

==Plot==
A wealthy, over-the-hill actress named Laura Winters (Rita Morely) hires pilot Grant Murdoch (Byron Sanders) to fly her and her assistant Jan Letterman (Barbara Wilkin) to Provincetown, but a storm forces them to land on a small island. They soon meet Prof. Peter Bartell (Martin Kosleck) a marine biologist with a German accent who is living in seclusion on the isle.

After a series of strange skeletons wash ashore (human, then fish) it turns out the water has become inhabited by some sort of glowing microbe which apparently devours flesh rapaciously. Bartell is a former US Government agent who was sent to Nazi Germany to recover as much of their scientific data as possible. He was chosen for the job for his scientific skills and knowledge of the German language. Using the methods learned there he hopes to cultivate a group of monstrous "flesh eaters" that can devour the skin off a screaming victim in mere seconds. A beatnik named Omar (Ray Tudor) joins the group after becoming shipwrecked on their shore.  Tensions mount after the plane drifts off into the ocean, leaving the castaways and Bartell as potential meals for the ravenous monsters.

High-voltage electrification (from a battery system devised by Bartell) is utilized in an attempt to slay the monsters. Bartell explains that he has been tracking these creatures and attempting to cultivate them to sell as biological weapons. Soon after it is discovered that the electrical shock instead increases their powers. The high voltage causes the numerous smaller creatures to join into a larger version. By accident, the survivors stumble upon the solution. The creatures devour flesh but not blood, as in each case that remains have been found blood has been present. Bartell surmises that the creatures have a negative reaction to hemoglobin and when directly injected with it the creatures are indeed slain. Following a struggle Bartell is killed just before Murdoch destroys the last of the creatures.

==Cast==
* Martin Kosleck as Prof. Peter Bartell
* Byron Sanders as Grant Murdoch
* Barbara Wilkin as Jan Letterman
* Rita Morley as Laura Winters
* Ray Tudor as Omar
* Christopher Drake as Matt
* Darby Nelson as Jim
* Rita Floyd as Radio Operator
* Warren Houston as Cab Driver
* Barbara Wilson as Ann
* Ira Lewis as Freddy

==Background==
The film has developed a cult following due to its gruesome, if primitive, special effects, including some memorably bloody death scenes.    One character is eaten from the inside out by the titular monsters, resulting in a gushing fountain of intestinal matter.  Another victim is stabbed with a wooden stake, then shot twice in the face, with resultant gaping bullet holes.  These scenes, as well as some occasional unintentionally campy moments, have helped to make the film a favorite for late night TV fanatics for decades.

The deep focus cinematography was the work of director Jack Curtis (working under a pseudonym, Carson Davidson), who shot every scene outdoors under the sun of Long Island.  The film was scripted by comic book writer Arnold Drake (The Doom Patrol, Marvels Captain Marvel, et al.).  Drake storyboarded the film, so every shot has the careful, formalized composition of a well-drawn comic strip.  One shot, for example is a shot in deep focus: the right profile of the hero dominates the left-side foreground of the frame; in a moment, two or three tiny figures at the far-removed shoreline move left to right, from behind the actors head, and in focus.

In 1967, George A. Romero began work on a horror film provisionally called Night of the Flesh Eaters; to avoid confusion with this film, the title was changed to Night of the Living Dead.

 ==Production== Jack Curtis won $72,000 on the television quiz show: "High Low". Part of the money was used finishing the production.  
While filming on location at Montauk, New York, a real hurricane destroyed the sets and equipment. Production was delayed for a year and the cost rose from $60,000 to $105,000. 
The working title of the 1968 classic Night of the Living Dead was Night of the Flesh Eaters. The title was changed when its distributor, The Walter Reade Organization, expressed concern over confusion with The Flesh Eaters, released five years earlier.
The film was copyrighted 2 years before its original release in 1964. 

==Release==
The film was first released in Phoenix, Arizona|Phoenix, Arizona on March 18, 1964. It later had a re-release in 1968.

==Reception==
The film has received negative reviews from critics on Rotten Tomatoes and holds a score of 48%. It also holds a score of 6/10 on the Internet Movie Database.

==Remake==
In 2001, a remake of The Flesh Eaters was completed by co-directors Shane M. Dallmann and Christo Roppolo for their own production company, Labcoat Productions. 

To date, the film has not been picked up for distribution, reportedly due to rights clearance problems.

==Footnotes==
 

==See also==
* List of American films of 1964

==External links==
*   at "The Bad Movie Report."
*    at the "Horrorwood."
*   DVD review.
*  
*  
*  

 
 
 
 
 
 
 
 
 
 