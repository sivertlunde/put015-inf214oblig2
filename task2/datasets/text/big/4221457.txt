The Ice Pirates
{{Infobox film
| name = The Ice Pirates
| image = Ice pirate.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster by Steven Chorney
| director = Stewart Raffill John Foreman
| writer = Stewart Raffill Stanford Sherman
| starring = {{Plainlist|
* Robert Urich
* Mary Crosby
* Michael D. Roberts
}}
| music = Bruce Broughton
| cinematography = Matthew F. Leonetti
| editing = Tom Walls
| studio = JF Productions
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 91 minutes
| country = United States
| language = English
| budget = $9 million
| gross = $14,255,801 
}}
The Ice Pirates is a 1984 comic science fiction film directed by Stewart Raffill, who co-wrote the screenplay with Krull (film)|Krull writer Stanford Sherman. The film stars Robert Urich, Mary Crosby and Michael D. Roberts; other notable featured actors are Anjelica Huston, Ron Perlman, Bruce Vilanch, John Carradine, and former football player John Matuszak.

==Plot==
The film takes place in a distant future where water is so scarce and rationed that it is considered an immensely valuable substance, both as a commodity and as a currency in ice cubes. The Templars of Mithra control the water and they destroyed worlds that had natural water leaving the galaxy virtually dry. Pirates dedicate their lives to raiding ships and looting the ice from the cargo holds to make a living. Jason (Robert Urich) is the leader of a band of pirates that raided a Templar cruiser for its ice, and discovered a beautiful princess (Mary Crosby) in a stasis pod. He decided to kidnap her, waking her up, and alarming the Templars. Jason and his pirates fled, but were pursued by Templar ships.  Jason let some of his crew, Maida (Anjelica Huston)  and Zeno (Ron Perlman), escape while Roscoe (Michael D. Roberts) stayed to help Jason. They were captured. During their capture, they met Killjoy (John Matuszak) whos been pretending to be monk to avoid being a slave. Jason and Roscoe are sentenced to become slaves, a process which includes castration.  Roscoe and Jason are spared castration by the princess, who has taken an interest in them. Princess Karina purchased them as her slaves, to work as servants during her party.  That evening, they were reunited with Killjoy (disguised as a robot). Jason, Karina, Roscoe, Killjoy and Nanny managed to leave the planet before the Supreme Commander (John Carradine) arrived to arrest her. Princess Karina is spoiled, and she hired Jason so she can find her father, who went missing while searching for a planet with water.  It is rumored he discovered one and the Templars would do anything to keep it a secret in order to maintain power. On their next planet, Jason and Roscoe got reunited with their fellow pirates, Maida and Zeno.  They proceed to locate the "lost" planet that contains massive amounts of water. The planet must be approached on a specific course or the ship will be suspended in time forever. The course apparently contains some sort of real or illusory time distortion (resulting in both the heroes and the villains reaching old age during the climactic battle).

==Cast==
* Robert Urich as Jason
* Mary Crosby as Princess Karina
* Michael D. Roberts as Roscoe
* Anjelica Huston as Maida
* Ron Perlman as Zeno
* Bruce Vilanch as Wendon
* John Carradine as Supreme Commander
* John Matuszak as Killjoy
* Ian Abercrombie as Hymie
* Alan Caillou as Count Paisley

==Reception==
The film is somewhat tongue-in-cheek and often compared to Star Wars. Upon its release, the New York Times described it as a "busy, bewildering, exceedingly jokey science-fiction film that looks like a Star Wars spin-off made in an underdeveloped galaxy." 

The film is also note-worthy for its cheeky, obviously cut-rate production values, mid-eighties "Colour-blind casting|color-blind casting", sexual frankness, and near-deliberately slack "sitcom" direction. The climactic "time-warp" battle is a rare example of the classic science-fiction temporal paradox done in a "real-time" context. It currently holds an 11% rating on Rotten Tomatoes;  despite this, it proved to be a moderate box office success, grossing a domestic total of $14,255,801    on a $9 million budget.

==See also==
* List of American films of 1984
* List of space pirates

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 