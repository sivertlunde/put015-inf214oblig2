Gang Leader
{{Infobox film
| name = Gang Leader
| image = Gang Leader.jpg
| writer = Vijaya Bapineedu (Story & screenplay)  Paruchuri Brothers (dialogues)  Rao Gopal Rao Anandaraj Murali Mohan
| director = Vijaya Bapineedu
| cinematography = Lok Singh
| producer = Maganti Ravindranath Chowdary
| editing =
| distributor = Shyam Prasad Arts
| country = India
| released = 9 May 1991
| runtime =
| language = Telugu
| music = Bappi Lahari
| awards =
| budget =
| gross =
}}
 Telugu crime film starring Chiranjeevi and Vijayashanti. Directed by Vijaya Bapineedu, with soundtrack by Bappi Lahari, and dance choreography by Prabhu Deva, the film emerged as a musical blockbuster, cult classic. Gang leader surpassed collections of box office blockbuster and industry hit movie Jagadeka Veerudu Atiloka Sundari. It later dubbed in Tamil with same title. The film was later remade in Hindi as Aaj Ka Goonda Raj with Chiranjeevi and Meenakshi Seshadri in lead roles, and in Kannada as Kutumba starring Upendra.  

==Plot== Sarath Kumar) and Rajaram (Chiranjeevi) are brothers. Raghupathi is  the only bread winner of the family. Raghava is preparing for  Civil Services Examination and Rajaram is unemployed. Rajaram and his four friends are a gang and are searching for jobs daily. They roam in the city till mid-night. Rajaram comes home at mid-night and sleeps till afternoon, his grand mother (Nirmalamma) scolds him for his irresponsibility and carelessness. One day Rajaram makes Kanya Kumari (Vijayashanti) to leave the house as she is not paying the rent to the house, Subsequently she changed her place to Rajarams house.
At one situation Rajaram goes to jail for money, because his brother requires money as he wants to go to Delhi for IAS preparation. Ekambaram (Raogopal Rao) kills Raghupathi (Murali Mohan) while his brother (Raja Ram) is in jail, but Rajarams` friends  know how he died. Rajaram thinks his brothers death was an accident.
 IAS officer, and marries Sumalatha but she hates his family members. Kanya Kumari loves Rajaram and his family members. Meanwhile Rajaram comes to know about his brothers death and inquires who was responsible for his death. Even his friends were also murdered by Ekambaram at that time. The film ends with Rajaram taking revenge by killing the villains.

==Cast==
* Chiranjeevi ....  Rajaram
* Vijayashanti ....  Kanyakumari
* Raogopal Rao ....  Ekambaram
* Kaikala Satyanarayana ....  Jailer
* Allu Ramalingaiah.......Sidhanthi
* Nirmalamma.........Sabari (Rajarams grandmother)
* Anandaraj ....  Kanakambaram
* Murali Mohan ....  Raghupati
* Sarath Kumar ....  Raghava
* Sumalatha ....  Raghavas Wife
* Sudha.............Lakshmi (Raghupatis wife)
* Nutan Prasad .... MLA
* Narayan Rao
* Hari Prasad
* Jayalalitha.......Chukka
* Maharshi Raghava

==Soundtrack==
The Music and background score was provided by Bappi Lahari and the Lyrics were penned by Veturi Sundararama Murthy, Bhuvanachandra. The audio was an instant hit and "Papa Rita" and "Vaana Vaana Velluvaye" were a rage in the entire state that time. Later the Song "Vaana Vaana Velluvaye" was re-mixed by Mani Sharma in the 2012 film Rachcha which starred Ram Charan Teja  and Tamannaah in the Lead.

{| class="wikitable" style="width:70%;"
|-
! Song Title !! Singers !! Lyricist
|-
| "Papa Rita" || S. P. Balasubrahmanyam || Bhuvanachandra
|- Chitra || Veturi Sundararama Murthy
|-
| "Bhadrachalam" || S. P. Balasubrahmanyam, Chitra || Bhuvanachandra
|-
| "Vaana Vaana Velluvaye" || S. P. Balasubrahmanyam, Chitra || Bhuvanachandra
|-
| "Vayasu Vayasu" || S. P. Balasubrahmanyam, Chitra || Veturi Sundararama Murthy
|-
| "Panisaasasa" || S. P. Balasubrahmanyam, Chitra || Bhuvanachandra
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 