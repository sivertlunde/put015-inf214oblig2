World's Greatest Dad
{{Infobox film
| name = Worlds Greatest Dad
| image = worldsgreatestdad.jpg
| director = Bobcat Goldthwait Richard Kelly Sean McKittrick Tim Perell
| writer = Bobcat Goldthwait
| starring = Robin Williams Daryl Sabara Alexie Gilmore Evan Martin Lorraine Nicholson Henry Simmons Geoff Pierson
| music = Gerald Brunskill
| cinematography = Horacio Marquínez
| editing = Jason Stewart Darko Entertainment
| distributor = Magnolia Pictures   
| released =  
| runtime = 99 minutes  
| country = United States
| language = English
| budget = $10 million 
| gross = $295,750  
}}
Worlds Greatest Dad is a 2009 American black comedy film written and directed by Bobcat Goldthwait. It stars Robin Williams, Daryl Sabara, and Alexie Gilmore. The film was released on July 24 on video on demand providers before its limited theatrical release on August 21, 2009.

==Plot==
 alcoholic mother. Kyles consistently poor academic performance and vile behavior gain the attention of the school principal (Geoff Pierson), who advises Lance that Kyle should transfer to a special-needs school. Lance is in a non-committal relationship with a younger teacher named Claire (Gilmore), who is spending time with a fellow teacher named Mike (Henry Simmons), whose writing class is more successful than Lances. On nights when Claire cancels their dates and he is alone, Lance bonds with his elderly neighbor Bonnie (Mitzi McCall).

One night, after Kyle and Lance spend an evening with Claire, Lance discovers that Kyle has died in an autoerotic asphyxiation accident in his bedroom. To avoid embarrassing his son, he stages Kyle’s death as a suicide. He writes a suicide note on Kyle’s computer and hangs his son’s body in the closet. A classmate later obtains the suicide note from police records and publishes it in the school newspaper. The note strikes a chord with the students and faculty, and suddenly many students claim to have been friends with Kyle and are touched by how deep and intelligent he shows himself to be in his writings.

Enjoying the attention his writing is finally receiving, Lance decides to write and publish a phony journal that was supposedly written by his son before his death. Kyle becomes something of a post-mortem cult phenomenon at the school, and soon Lance begins to receive the adoration that he has always desired. Andrew finds Kyle’s suicide note and journals as highly uncharacteristic based on Kyles personality when he was alive, but Lance brushes him off when Andrew confronts him. The journal soon attracts the attention of book publishers and Lance lands a television appearance on a nationally broadcast talk show. The school principal then decides to rename the school library in Kyle’s honor. 

At the library dedication, Lance finds that he can no longer live with his lie and confesses before the school. Lance is denounced by the students and faculty, including Claire. Lance nevertheless feels reborn, and dives naked into the schools swimming pool. Andrew tells Lance he knew the truth all along, and that he enjoyed Lances writing. The two of them happily watch a zombie movie with Bonnie.

==Cast==
* Robin Williams as Lance Clayton
* Alexie Gilmore as Claire Reed
* Daryl Sabara as Kyle Clayton
* Evan Martin as Andrew
* Geoff Pierson as Principal Wyatt Anderson
* Henry Simmons as Mike Lane
* Mitzi McCall as Bonnie
* Jermaine Williams as Jason
* Lorraine Nicholson as Heather Morgan Murphy as Morgan
* Toby Huss as Bert Green
* Tom Kenny as Jerry Klein (talk show producer)
* Jill Talley as Make-Up Woman
* Bruce Hornsby as himself
* Krist Novoselic as Newspaper Vendor (cameo)
* Bobcat Goldthwait as the limo driver (uncredited)

==Production== Nirvana bassist cameo while consoling Williams character at a newspaper stand (Goldthwait had previously opened for Nirvana). Bruce Hornsby appears as himself at the library dedication.

==Home video==
The DVD was released on December 8, 2009 and featured an audio commentary track with the director, deleted scenes, outtakes, and a making of featurette.

==Reception==
Worlds Greatest Dad received generally positive reviews from critics, holding a "Fresh" rating of 89% on   reported that the film had an average score of 69 out of 100, based on 24 reviews. 
 At The Movies. Mankiewicz saluted Daryl Sabaras performance as exceptionally well done, commented on the films "remarkably funny script," and overall considered it a "little gem." Tom Rougvieq from the Whitstable Times gave it a 3 out of 4 stars and called it a refreshing comedy of the age, praising its originality and Robin Williams performance.    Roger Ebert of the Chicago Sun-Times gave Worlds Greatest Dad 3 out of 4 stars, but noticed that the material could have been even darker in its satire, and he questioned whether it was the directors intention.   

==See also==
*A Million Little Pieces, a literary hoax popularized on a television talk show
*Haunted_(Palahniuk_novel)#.22Guts.22|"Guts", a short story which also involves death by autoerotic asphyxiation being disguised as suicide

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 