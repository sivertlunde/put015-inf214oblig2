Australia Has Wings
 
{{Infobox film
| name           = Australia Has Wings
| image          = 
| image size     = 
| caption        = 
| director       = 
| producer       = 
| writer         = John Paton
| based on       = 
| starring       = 
| music          = 
| cinematography = George D. Malcolm
| editing        =  narrator = Ernest Walsh
| distributor    = MGM National Films Council
| studio  = Department of Information Commonwealth Film Laboratories
| released       = 17 January 1941 
| runtime        = 10 mins
| country        = Australia
| language       = English
| budget         = 
}}
Australia Has Wings is a 1941 short Australian documentary film made as propaganda for World War II which shows the development of the Australian aircraft industry, particularly production of the CAC Wirraway. 

It was made by Commonwealth Film Laboratories for the Department of Information. 

==Release==
The movie screened in Australia and overseas.  

==References==
 

==External links==
*  at Australian War Memorial

 
 
 
 
 
 