Dhumm
{{Infobox film
| name       = Dhumm
| image      = Dhumm Movie Kannada.jpg
| director   = M.S. Ramesh
| producer   = R. Srinivas
| story      = M.S. Ramesh R. Rajashekar
| screenplay = M.S. Ramesh R. Rajashekar
| starring   =  
| music      = Gurukiran
| cinematography = H.C. Venu
| editing    = S. Manohar
| studio     = 
| distributor = 
| released   =  
| country    = India
| language   = Kannada
| runtime    = 139 min
| budget     = 
| gross      = 
| website    =  
}}
 Kannada action-family-drama Kaviraj and V. Nagendra Prasad. The film released on 27 September 2002  

==Cast==
* Sudeep as Varada
* Rakshita as Preethi
* Srinath as Varada and Shankars father Sumithra as Varada and Shankars mother
* Nassar as Shankar
* Rangayana Raghu as M.L.A.
* Arun Sagar as Vinod (Preethis brother)
* Sadhu Kokila as Varadas friend
* Chitra Shenoy as Preethis sister
and others

==Review==
* chitraloka  

==Soundtrack==

 

==Awards==
{| class="infobox" style="width: 22.7em; text-align: left; font-size: 85%; vertical-align: middle; background-color: #eef;"
|-
| colspan="3" |
{| class="collapsible collapsed" width="100%"
! colspan="3" style="background-color: #d9e8ff; text-align: center;" | Awards and nominations
|- style="background-color:#d9e8ff; text-align:center;"
!style="vertical-align: middle;" | Award
| style="background:#cceecc; font-size:8pt;" width="60px" | Wins
| style="background:#eecccc; font-size:8pt;" width="60px" | Nominations
|-
|align="center"|
;Filmfare Awards South
| 
| 
|}
|- style="background-color:#d9e8ff"
| colspan="3" style="text-align:center;" | Totals
|-
|  
| colspan="2" width=50  
|-
|  
| colspan="2" width=50  
|}
Filmfare Awards South :- Best Music Director – Kannada (2002) - Gurukiran 

==References==
 

==External links==
*  

 
 
 
 
 


 