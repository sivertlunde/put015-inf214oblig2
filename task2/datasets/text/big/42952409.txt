All Hallows' Eve (film)
{{Infobox film
| name           = All Hallows Eve
| image          = File:All Hallows Eve 2013 film poster.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Damien Leone
| producer       = Jesse Baget Gary LoSavio Lisandro Novillo
| writer         = Damien Leone
| screenplay     = 
| story          = 
| based on       =  
| starring       = Katie Maguire, Mike Giannelli, Catherine A. Callahan
| narrator       = 
| music          = Noir Deco
| cinematography = Christopher Cafaro C.J. Eadicicco George Steuber Marvin Suarez
| editing        = Damien Leone
| studio         = Ruthless Pictures
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
All Hallows Eve is a 2013 horror movie that was directed by Damien Leone and his feature film directorial debut. It was released straight to DVD on October 29, 2013 and stars Mike Giannelli as a homicidal clown that terrorizes his victims. The movie incorporates scenes from two of Leones earlier shorts, The 9th Circle and Terrifier, which also featured the character of Art the Clown.   

==Plot==
After a night of trick or treating, Sarah (Katie Maguire) is surprised to see that Tia (Sydney Freihofer) and Timmy (Cole Mathewson) have received an unmarked VHS tape in one of their bags. They decide to watch the video which contains three stories featuring a creepy clown (Mike Giannelli). The first features a young woman who is drugged and kidnapped by Art the Clown while waiting for a bus. She awakens chained in a room with two other women. Demonic creatures enter, killing two of the women and taking the third to a room where a devil-like creature rapes her. A humanoid fetus is then removed via c section by the creatures. The second features a woman living in a new countryside home who begins to experience strange occurrences. Its revealed shes being stalked by aliens, and as shes dragged off by the aliens, she grabs a sheet which pulls away to reveal a painting of Art the Clown. The final segment features a college student driving down an isolated road. Stopping at a gas station, she finds the attendant furiously kicking out Art who had apparently smeared feces all over the bathroom. The attendant fills her tank and goes inside to fetch her directions. When he doesnt return, the girl inspects to find Art chopping up the attendants body with a hacksaw. She flees and a chase ensues. Eventually Art catches her and she comes to on a crude operating table with her limbs cut off by Art. It ends with Art laughing silently but maniacally. Disturbed, Sarah attempts to shut off the tape but to no avail. Art then steps into frame in a dingy looking basement. He approaches Sarah from within the screen and begins to pound the television glass, ending when she pulls the TV cord. Sarah goes to check on the children only to find out that Art has murdered them.

==Cast==
*Katie Maguire as Sarah
*Mike Giannelli as Art the Clown
*Catherine A. Callahan as Caroline
*Marie Maser as Woman
*Kayla Lian as Casey
*Cole Mathewson as Timmy
*Sydney Freihofer as Tia

==Production==
Leone originally came up with the character of Art the Clown and featured him in a 2008 short entitled The 9th Circle and revisited the character in the 2011 short Terrifier.  He came up with the character of Art after coming up with the idea of a scene where a woman boards an empty bus in the middle of the night, only for a clown to begin harassing her.    Producer Jesse Baget viewed Terrifier via YouTube, which influenced his decision to back All Hallows Eve.  Leone decided that he would use footage from both shorts for two of the segments in All Hallows Eve, and created an all new segment and wraparound story for the film. This was not how Leone had originally intended to create the film, as Terrifier was supposed to be part of a different film anthology with shorts by other directors and Leone was to shoot scenes centering around Art, which would be placed into other peoples films.  Leone did not want to do this and Baget instead chose to create a separate film anthology around the two shorts and new material. 

==Reception==
 
Critical reception for All Hallows Eve was mostly positive and the Oklahoma Gazette commented that the movie had the makings of a cult film.  Fangoria and Aint It Cool News both praised the movie,  and Fangoria commented that they were impressed with what Leone accomplished in the film.  DVD Verdict was more mixed in their review, as they felt that while it wasnt "a bad film" it also wasnt "a particularly good or memorable one" and that it would be "Worth a look if your cable is out and youve exhausted Netflix and Redbox."  Cinema Crazed panned All Hallows Eve as being overly forgettable and criticized the character of Art as being overly generic. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 