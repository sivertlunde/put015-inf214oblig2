Manthra Mothiram
{{Infobox film
| name = Manthra Mothiram
| image =
| image_size =
| caption =
| director = Sasi Shanker
| producer =
| writer =
| screenplay = Benny P Nayarambalam Dileep Nedumudi Venu Kalabhavan Mani Hakim Rawther Johnson
| cinematography = Prathapan
| editing = A. Sukumaran
| studio = Lallu Films
| distributor = Lallu Films
| released =  
| country = India Malayalam
}}
 1997 Cinema Indian Malayalam Malayalam film, directed by Sasi Shanker. The film stars Dileep (actor)|Dileep, Nedumudi Venu, Kalabhavan Mani and Hakim Rawther in lead roles. The film had musical score by Johnson (composer)|Johnson.  
  

==Cast==
   Dileep 
*Nedumudi Venu 
*Kalabhavan Mani 
*Hakim Rawther 
*Indrans 
*Mamukkoya 
*Manka Mahesh 
*N. F. Varghese 
 

==Soundtrack==
The music was composed by Johnson (composer)|Johnson. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Chiraku thedumee || G. Venugopal || S Ramesan Nair || 
|- 
| 2 || Manjin Maarkazhi || M. G. Sreekumar, Sujatha Mohan || S Ramesan Nair || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 