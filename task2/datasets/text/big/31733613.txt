The Last Sentence
{{Infobox film
| name           = The Last Sentence (Dom över död man)
| image          = The Last Sentence Poster.jpg
| caption        = English language cinema poster
| director       = Jan Troell
| producer       = Francy Suntinger
| writer         = Jan Troell Klaus Rifbjerg
| starring       = Jesper Christensen Pernilla August Björn Granath Ulla Skoog
| music          = 
| cinematography = Mischa Gavrjusjov Jan Troell
| editing        = 
| studio         = Filmlance International
| distributor    = 
| released       =  
| runtime        = 
| country        = Sweden
| language       = Swedish kr
| gross          = $327,297 
}}

The Last Sentence ( ) is a Swedish film from 2012, directed by Jan Troell and starring Jesper Christensen, Pernilla August, Björn Granath and Ulla Skoog. It is set between 1933 and 1945, and focuses on the life and career of Torgny Segerstedt, a Swedish newspaper editor who was a prominent critic of Hitler and the Nazis during a period when the Swedish government and monarch were intent on maintaining Swedens neutrality and avoiding tensions with Germany.  The film also deals with Segerstedts relations with his wife, his mistress, and his mistresss husband (who was a close friend of Segerstedt).

The films Swedish title, Dom över död man, comes from a line in the Old Norse poem Hávamál: "Cattle die, kinsmen die, thou wilt also die; but I know one thing that never dies: the judgment on the dead". 

==Cast==
* Jesper Christensen as Torgny Segerstedt
* Pernilla August as Maja Forssman
* Björn Granath as Axel Forssman
* Ulla Skoog as Puste Segerstedt Peter Andersson as Foreign Minister Christian Günther
* Amanda Ooms
* Maria Heiskanen as Pirjo
* Lennart Hjulström as Marcus Wallenberg
* Johanna Troell as Ingrid Segerstedt
* Birte Heribertsson as Estrid Ancker
* Lia Boysen as Anita
* Josef Persson as young revolutionary
* Kenneth Milldoff as Prime Minister Per Albin Hansson Gustaf V

==Production==

===Development===
After finishing his previous feature film, Everlasting Moments from 2008, director Jan Troell has said that he felt an emptiness and wondered whether he ever would get to make another film. Around that time he received a phonecall from the writer Kenne Fant, who in 2007 had published a biography about Torgny Segerstedt, an early and outspoken critic of Adolf Hitler, who before and during World War II had been the editor-in-chief of the newspaper Göteborgs Handels- och Sjöfartstidning. Fant wondered whether Troell would be interested in making a film based on Segerstedts life.    Troell accepted the offer when his friend Klaus Rifbjerg, a Danish writer, was enthusiastic about the project and wanted to be Troells co-writer. Initially, Troell had problems finding an approach for the films narrative, but soon read another biography about Segerstedt, written by his secretary Estrid Ancker. Troell also read Anckers research material for the book, which included interviews with more than 300 people connected to Segerstedt.  Through this material Troell found out more about Segerstedt on a personal level, including his personal motivations and private relationships. The director then chose to make a film which focuses on Segerstedt as a human being more than as a public figure.  According to Troell it took "a couple of years" to form the narrative and write the screenplay with Rifbjerg. 
 kronor in support from the Swedish Film Institute and 450,000 euros from Eurimages, as well as funding from the Norwegian Film Institute and Nordisk Film- & TV Fond.    The production involved a total budget of 43 million kronor.   

Early in the production process Max von Sydow was considered for the role of Segerstedt. Soon however Troell decided to offer it to the Danish actor Jesper Christensen. Both Troell and Christensen were initially worried about Christensens Danish accent, which would be inconsistent with the role; this was eventually solved by changing the script to make Segerstedts mother Danish. The casting choice was also motivated with the fact that Troells earlier film Hamsun (film)|Hamsun had starred Sydow, who there spoke Swedish in the role of the Norwegian author Knut Hamsun. 

===Filming===
Principal photography commenced 21 February 2011 in Luleå where it continued for three weeks.     Filmpool Nords Studio Kronan was used for studio scenes set in Göteborgs Handels- och Sjöfartstidnings office and Segerstedts home. Also in Luleå, footage was taken of the Lule River, which stands in for Klarälven in the film.    After that the team relocated to Gothenburg for eight weeks of filming, which consisted of exterior scenes and on-location interiors. Finally they went to Stockholm for three days in May. The filming in Stockholm included a scene set inside the Storkyrkan Cathedral, which was staged as a homage to the 1947 short film Symphony of a City by Arne Sucksdorff, a filmmaker who was a major influence for Troells earliest works.   The Last Sentence was recorded with Arri Alexa cameras. It was the first feature-length fiction film Troell shot digitally, although he had previously directed digital short films and a documentary feature.  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 