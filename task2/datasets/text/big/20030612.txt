District 13: Ultimatum
{{Infobox film
| name           = District 13: Ultimatum
| image          = B13_Ultimatum.jpg
| caption        = French promotional poster
| director       = Patrick Alessandrin
| producer       = Luc Besson
| writer         = Luc Besson
| starring       = David Belle Cyril Raffaelli
| music          =
| cinematography =
| editing        =
| studio         =EuropaCorp TF1 Films Production Canal+ CiBy 2000 CinéCinéma Sofica Europacorp Magnet Releasing (US)  
| released       =  
| runtime        = 106 minutes
| country        = France
| language       = French
| budget         = € 12,000,000 (estimated) 
| gross          = € 13,500 000
}}
District 13: Ultimatum, also known as D13-U (  to the 2004 French parkour-filled action film District 13. The film, directed by Patrick Alessandrin and written and produced by Luc Besson (who also wrote and produced the first film), sees parkour artists David Belle and Cyril Raffaelli reprising their original roles of Leïto and Damien. 
{{cite web
|url=http://www.allocine.fr/film/casting_gen_cfilm=137189.html
|title=Banlieue 13 ultimatum: Casting complet
|accessdate=2008-11-01
|publisher=AlloCiné
|language=French
}}
 

== Plot == original film, law and order to ravaged District 13. However, their efforts so far have failed to do so. The death of gang overlord Taha Ben Mahmoud has left a power vacuum, and total control of the area is now being fought over by five rival territorial gang lords who want to step into Tahas position. Leïto tries demolishing the walls surrounding the district on a daily basis, but is told to stop by African gang lord Molko, who sees the walls as protection from outside the district.

After single-handedly eliminating a major drug dealer, Damien is framed for drug dealing and arrested, but manages to make a brief call to Leïto to come rescue him.

Meanwhile, corrupt government agents from the Department of Internal State Security (DISS), led by Gassman are bent on destroying the five tower blocks at the heart of District 13 with tactical precision bombing, and building luxury flats after the area is cleared. In order to spark conflict with the districts gangs, the DISS shoot several policemen, dump their car in District 13, and prompt several gang members into gunning down the vehicle, making it look like the gang members killed the policemen. The footage of the incident convinces the French President to carry out the strike. However, the DISS were witnessed killing the policemen and filmed by a teenager named Samir and his friends. The DISS agents soon come after the teen to arrest him, but Samir manages to give his memory card to Leïto before being arrested.

Leïto gets himself arrested in order to get into the prison. He then escapes the police and rescues Damien. After freeing Damien from his cell, they discuss the events (deducing that Damien was framed by DISS to keep him from finding out their plans during the crisis) and further plans, resolving to gather enough proof to expose the DISS agents. While Damien distracts the guards, Leïto breaks into Gassmans office to steal his hard-drive for the evidence that they need. Once they escape and return to District 13, Damien and Leïto convince the five gang lords Tao, Molko, Little Montana, Karl the skinhead, and Ali-K to band together and prevent the destruction of the district. While the President struggles with the decision to destroy District 13, even with the area evacuated, a large number of gang members storm the French Parliament. They eventually reach the President and show him the information that they acquired, proving Gassman is a corrupt DISS agent. Gassman then takes the President hostage and tries to force him to approve the mass demolition. Leïto, Damien, and the gang lords succeed in freeing the President and incapacitating Gassman, earning the Presidents thanks and a promise to fund District 13s restoration.

With the conflict over and District 13 completely evacuated, the gang lords then decide that it would be better to rebuild District 13 anew rather than try to patch up its remnants. The movie ends with the President authorizing the strike, the District 13 buildings exploding and the president breathing a sigh of relief, stating that he needs a drink.
 At the credits, there is a short clip showing the President, the gang lords, Damien and Leïto all joking around and smoking cigars together.

== Cast ==
* David Belle as Leïto
* Cyril Raffaelli as Damien Tomaso
* Philippe Torreton as President
* Daniel Duval as Walter Gassman
* Elodie Yung as Tao, leader of the Asian gang
* MC Jean Gab1 as Molko, leader of the African gang
* La Fouine as Ali-K, leader of the Arab gang
* James Deano as Karl, leader of the Skinhead gang
* Fabrice Feltzinger as Little Montana, leader of the Hispanic gang
* Pierre-Marie Mosconi as Roland
* Sophie Ducasse as Sonya
* Jean-François Lénogue as Jeff
* Lannick Gautry as the cool friend

==Reception==
The film holds a "Fresh" rating of 74% on Rotten Tomatoes.  It holds a similar rating on Metacritic, with a score of 64 out of 100.

== Releases ==
The original French language version District B13 Ultimatum was released in France on February 18, 2009. 
{{cite web
|url=http://www.allocine.fr/film/fichefilm_gen_cfilm=137189.html
|title=Banlieue 13 ultimatum
|accessdate=2008-11-01
|publisher=AlloCiné
|language=French
}}
  The   on October 26, 2009,  and it premiered in the US in a free screening in New York on January 28, 2010. 

The DVD/Blu-ray was released in France on August 19, 2009. The Region 1 version was released on April 27, 2010.

== References ==
 

== External links ==
*  
*  
*  
*   from Variety.com 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 