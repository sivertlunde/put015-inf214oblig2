Maa Nanna Chiranjeevi
 
{{Infobox film
| name           = Maa Nanna Chiranjeevi
| image          =
| caption        =
| director       = Arun Prasad P.A.|P.A. Arun Prasad
| producer       = Murali Krishna
| writer         = P.R. Santosh P. Narayana Reddy Swetha Manchiraju Sreekar Ankella  
| story          = P.A. Arun Prasad
| screenplay     = P.A. Arun Prasad
| starring       = Jagapati Babu Neelima Master Atulith Hemachandra
| cinematography = Bharani K Dharan
| editing        = Basvapedi Reddy
| studio         = Laughing Lords Entertainments
| distributor    =
| released       =  
| runtime        = 112 minutes
| country        = India Telugu
}}

Maa Nanna Chiranjeevi ( : My Father is Chiranjeevi) is a 2010 Telugu drama film produced by Murali Krishna on Laughing Lords Entertainments banner, directed by Arun Prasad P.A.|P.A. Arun Prasad. Starring Jagapati Babu, Neelima, Master Atulith in the lead roles and music composed by Hemachandra (singer)|Hemachandra. The film is a remake of 2006 Hollywood Hit film The Pursuit of Happyness, starring Will Smith and his son Jaden Smith. The film recorded as flop at box office.

==Plot==
Chiranjeevulu (Jagapathi Babu) is introduced as the 10th pass village landlord who is happy with his farmer’s lifestyle despite his riches. He gets married to the city-bred Neelima who soon squanders all his wealth in flop business ventures initiated by her rich friends. In the middle of all this, they have a son Vishwanath (Master Atulith) who adores Chiranjeevulu and prefers to stay with him when Neelima walks out on Chiranjeevulu for a better future in the city. Soon, father & son land up in Hyderabad as Chiranjeevulu decides to educate his son in a top English medium school so that he doesn’t end up like him. The rest of the movie is about the trials and tribulations that Chiranjeevulu  oes through to arrange the school fees while making sure his son does not see his struggle.   

==Cast==
{{columns-list|3|
* Jagapati Babu as Chiranjeevulu
* Neelima
* Master Atulith as Viswanath
* Brahmanandam Ali
* M. S. Narayana
* Jaya Prakash Reddy
* Benarjee
* Rajababu
* Satyam Rajesh
* Venu
* Tirupathi Prakash
* Jogi Naidu
* Fish Venkat
* Gowtham Raju
* Gundu Sudarshan
* Junior Relangi Jenny
* Telangana Shakuntala
* Jhansi
* Bangalore Padma
* Apoorva
* Kalpana
* Ragini
* Poornima
}}

==Soundtrack==
{{Infobox album
| Name        = Maa Nanna Chiranjeevi
| Tagline     = 
| Type        = film Hemachandra
| Cover       = 
| Released    = 2008
| Recorded    = 
| Genre       = Soundtrack
| Length      = 15:04
| Label       = Aditya Music Hemachandra
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

Music composed by Hemachandra (singer)|Hemachandra. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 15:04
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Meme Nilabadam 
| lyrics1 = Bhaskarabhatla
| extra1  = Hemachandra
| length1 = 4:42

| title2  = Yemo Yeppudela 
| lyrics2 = Ramajogayya Sastry
| extra2  = Shankar Mahadevan
| length2 = 4:48

| title3  = My Hero
| lyrics3 = Bhaskarabhatla
| extra3  = Nikhil DSouza,Sarath Chandra,Ramya
| length3 = 4:22

| title4  = Theme Music 
| lyrics4 = Instrumental
| extra4  = Music Bit
| length4 = 0:37

| title5  = Theme Music-II 
| lyrics5 = Instrumental 
| extra5  = Music Bit
| length5 = 0:35
}}

==References==
 

==External links==
 

 
 
 


 