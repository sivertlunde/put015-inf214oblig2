Imaginary Heroes
{{Infobox film
| name           = Imaginary Heroes
| image          = Imaginary Heroes (poster).jpg
| caption        = Original poster Dan Harris
| producer       = Illana Diamant Moshe Diamant Frank Hübner Art Linson Gina Resnick Denise Shaw
| writer         = Dan Harris
| starring       = Sigourney Weaver Emile Hirsch Jeff Daniels 
| music          = Deborah Lurie
| cinematography = Tim Orr James Lyons
| distributor    = Sony Pictures Classics 
| released       = December 17, 2004
| runtime        = 111 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = $291,118 
}}
 Dan Harris. It focuses on the traumatic effect the suicide of the elder son has on a suburban family.

==Plot==
Matt Travis is good-looking, popular, and his schools best competitive swimmer, so everyone is shocked when he inexplicably commits suicide. As the following year unfolds, each member of his family struggles to recover from the tragedy with mixed results.

His mother Sandy tries to keep the lines of communication open with younger son Tim while easing her emotional pain with marijuana. Father Ben, a perfectionist who worshipped Matt as much as he ignored Tim, insists on continuing to place a meal at the dinner table for the dead boy and begins to drink heavily. Eventually, without telling his wife, he takes a leave of absence from work and spends his days lost in reverie on a park bench. Tim, always in the shadows as the smaller, unathletic, less accomplished "other brother," struggles to get through school while trying to resist the recreational drugs his best friend Kyle Dwyer is always offering him and contemplating having sex with classmate Steph Connors. Sister Penny, away at college, dutifully comes home for infrequent visits and tries to help bridge the widening gap between her surviving brother and their parents.

With the passing months, new crises arise and a long-kept secret is revealed, until it is revealed that one family member was aware of Matts inner turmoil and suicidal thoughts and why nothing was done to help him.

== Cast ==
*Sigourney Weaver as Sandy Travis
*Jeff Daniels as Ben Travis
*Emile Hirsch as Tim Travis Michelle Williams as Penny Travis Deirdre OConnell as Marge Dwyer
*Ryan Donowho as Kyle Dwyer
*Kip Pardue as Matt Travis

== Production ==
Screenwriter/director Dan Harris was 22 years old when he sent the script to Bryan Singer, who hired him to work on the screenplays for X2 (film)|X2, Superman Returns, and the remake of Logans Run (1976 film)|Logans Run. Two years later he left Singer to begin pre-production work on Heroes.    Glen Ridge, Montclair, New Jersey|Montclair, Newark, New Jersey|Newark, Wayne, New Jersey|Wayne, and West Paterson, New Jersey.

==Reception==
===Box Office=== Cannes Film Market in May 2004 and was shown at the Toronto International Film Festival, the Austin Film Festival, the Chicago International Film Festival, and the Marrakech International Film Festival before opening on two theaters in the United States on December 19, 2004. It earned $4,696 on its opening weekend; at its widest release, it played in only 24 theaters domestically. It eventually grossed $228,767 domestically and $62,351 internationally for a total of $291,118. 

===Critical Response===
Imaginary Heroes received mainly negative reviews from critics and has a "rotten" score of 35% on Rotten tomatoes based on 103 reviews with an average rating of 5.2 out of 10. The consensus states "Imaginary Heroes is a muddled, melodramatic and unconvincing drama."  The film also has a score of 53 out of 100 on Metacritic based on 32 critics indicating "Mixed or average reviews." 

Roger Ebert of the Chicago Sun-Times observed, "The film might have been stronger as simply the story of the family trying to heal itself after its tragedy, with the focus on Sandy and Tim. But Harris feels a need to explain everything in terms of melodramatic revelations and surprise developments, right up until the closing scenes. The emotional power of the last act is weakened by the flood of new information. The key revelation right at the end explains a lot, yes, but it comes so late that all it can do is explain. If it had come earlier, it would have had to be dealt with, and those scenes might have been considerable . . . What remains when the movie is over is the memory of Sandy and Tim talking, and of a mother who loves her son, understands him, and understands herself in a wry but realistic way. The characters deserve a better movie, but they get a pretty good one." 

Mick LaSalle of the San Francisco Chronicle said the film "has a strong narrative spine, with interesting and big things happening throughout - an unexpected virtue in a family drama. But this virtue is obscured somewhat by the movies lack of narrative drive. Writer Dan Harris has written himself a tidy little story, but in directing it, he hasnt sculpted a dynamic drama. To put it in another way, theres a lean, mean 82-minute drama encased in this flabby 112-minute film . . . What saves Imaginary Heroes is its essential truthfulness about families, which it reveals, not only in the broad movements of its story but in the small details . . . Sometimes the films tone wobbles into farce, which is inappropriate, and often the story treads water, as though mimicking, in tone, every somber family film since Ordinary People. But, ultimately, it arrives at an authentic and surprisingly powerful place." 

Peter Travers of Rolling Stones rated the film two out of four stars and added, "Sigourney Weaver is a luminous actress with a tough core of intelligence and wit. And Emile Hirsch, who plays her guilt-tormented son, has the talent to sustain a major career. Their scenes together have a warmth that almost makes you forgive Imaginary Heroes . . . for trying so hard and so futilely to duplicate Ordinary People . . . What the movie damagingly lacks is a personality of its own." 

==DVD release== English audio track and subtitles in French. Bonus features include commentary with either Sigourney Weaver or Dan Harris and Emile Hirsch, deleted scenes, and a behind-the-scenes featurette and photo gallery.

==References==
 

==External links==
* 

 
 
 
 
 
 
 