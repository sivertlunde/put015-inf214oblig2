Dhuan
{{Infobox film
 | name = Dhuan
 | image = Dhuaanfilm.jpg
 | caption = Promotional Poster
 | director = Dulal Guha
 | producer = 
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Raakhee Amjad Khan Ranjeeta Aruna Irani Abhi Bhattacharya
 | music = R. D. Burman
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = 1981
 | runtime = 121 min.
 | language = Hindi Rs 1.5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1981 Hindi Indian feature directed by Dulal Guha, starring Mithun Chakraborty, Raakhee, Amjad Khan, Ranjeeta, Aruna Irani and Abhi Bhattacharya 

==Plot==
Dhuan is a Suspense thriller film starring Mithun Chakraborty,  Raakhee and Ranjeeta, supported by Aruna Irani.

==Cast==
*Mithun Chakraborty
*Raakhee 
*Ranjeeta 
*Abhi Bhattacharya
*Sharad Sharma 
*Amjad Khan
*Aruna Irani Lakshmi  as  Lakshmi

==Songs==
*Hum Bhi To...
*Phir Aankh Pharki Sanam...
*Tera Asra Hai Ek...
*Tum Mile...
*Yeh Hai Mauka Aa Mere...

==References==
 

==External links==
*  

 
 
 
 