Photographic Memory (film)
{{Infobox film
| name           = Photographic Memory
| image          = 
| caption        = 
| director       = Ross McElwee
| producer       = Ross McElwee (Personal assistant to Mr. McElwee & tra
coproducer: French Connection Films (Eric Ellena)
Personal assistant to Mr. Adrian McElwee:)
| writer         = Ross McElwee
| screenplay     = 
| story          = 
| based on       =  
| starring       = Adrian McElwee, Ross McElwee
| music          = Dane Walker, DJ Flack and Charles Mingus
| cinematography = Sean Wilcox, Ross McElwee, Adrian McElwee,
| editing        = Sabrina Zanella-Foresi
| studio         = Crimson Red Harvard/Universal Studios France
| distributor    = First Run Features
| released       =  
| runtime        = 84 minutes
| country        = United States France
| language       = English, French
| budget         = 
| gross worldwide = US$599,435.00
}}
Photographic Memory is a 2011 documentary film by independent filmmaker Ross McElwee about a voyage back to the roots of his involvement with the camera.

Photographic Memory premiered at the 2011 Venice Film Festival.

==Synopsis==
The filmmaker finds himself in frequent conflict with his son, who is no longer the delightful child the father loved, but an argumentative young adult who inhabits virtual worlds available through the internet. To the father, the son seems to be addicted to and permanently distracted by those worlds. The filmmaker undertakes a journey to Saint-Quay-Portrieux|St. Quay-Portrieux in Brittany where he worked for a spring as a wedding photographer’s assistant at age 24 –slightly older than his son is now. He has not been back to St. Quay since that visit, and hopes to gain some perspective on what his own life was like when he was his son’s age. He also hopes to track down his former employer, a fascinating Frenchman named Maurice, and Maud, a woman with whom he was romantically involved during that spring 38 years ago. Photographic Memory is a meditation on the passing of time, the praxis of photography and film, digital versus analog, and the fractured love of a father for his son.

==External links==
* 
*   at ScreenDaily.com
*   at ScreenDaily.com

 
 
 
 
 
 
 
 
 
 

 