You to Me Are Everything (film)
{{Infobox film
| name           = You to Me Are Everything 
| image          = Youtomeareeverythingfilm.jpg
| caption        = Theatrical release poster 
| director       = Mark A. Reyes 
| producer       = Roselle Monteverde-Teo Jose Mari Abacan 
| writer         = Aloy Adlawan
| starring       = Dingdong Dantes Marian Rivera
| music          = Von De Guzman 
| cinematography = Jay Linao
| editing        = Vanessa Ubas De Leon
| studio         = GMA Films Regal Entertainment
| distributor    = GMA Films Regal Entertainment
| released       =  
| runtime        = 120 minutes
| country        = Philippines
| language       = Tagalog Filipino
| budget         = P 20 Million 
| gross          = P 83.4 Million    
}}
You to Me Are Everything is a 2010 Philippine romantic comedy film starring Dingdong Dantes and Marian Rivera. Directed by Mark A. Reyes, from a screenplay by Aloy Adlawan, it was released on May 5, 2010 in the Philippines. 

==Synopsis==
Iska (Marian Rivera) is a simple, contented girl from the Cordillera mountains who suddenly inherits millions after the death of her real father. Raphael (Dingdong Dantes) is from a rich family who suddenly finds himself penniless after his father is convicted of corruption in congress. The two meet when Iska ends up in the mansion where Raphael used to live. Brought together by circumstance, Iska hires Raphael to be her business manager, showing her how to live the privileged life, while Iska, in turn, teaches Raphael how to find happiness in the little things in life. As the two struggle to find comfort in their new lives, they fall in love. But love, sometimes, comes with a price. Will they be willing to leave the life they love for the love of their life?

==Cast==
* Marian Rivera as Francisca Carantes
* Dingdong Dantes as Rafael Iniego Benitez III
* Isabel Oli as Megan
* Jaclyn Jose as Florencia Carantes
* Manilyn Reynes as Greta
* Bobby Andrews as Atty. Ronnie Domingo
* Roxanne Barcelo as Lily
* AJ Dee as Carding
* Andrea Torres as Therese Fernandez
* Bela Padilla as Monique Fernandez
* Carlo Gonzales as Baste
* Victor Aliwalas as Roy
* Fabio Ide as Miko
* Pinky Amador as Estella Fernandez
* Chinggoy Alonzo as Frank Benitez Jr.
* "Snowy" as Iskas pet
* Jai Reyes as Rado
* Piero Vergara as party goer

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 