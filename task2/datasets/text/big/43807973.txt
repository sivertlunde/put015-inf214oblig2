Selvam (1966 film)
{{Infobox film
| name           = Selvam 
| image          =
| image_size     =
| caption        =
| director       = K. S. Gopalakrishnan
| producer       = V. K. Ramasamy (actor)|V. K. Ramasamy
| writer         = B. S. Ramaiah K. S. Gopalakrishnan (dialogues)
| screenplay     = K. S. Gopalakrishnan
| starring       = Sivaji Ganesan K. R. Vijaya S. V. Ranga Rao S. V. Sahasranamam
| music          = K. V. Mahadevan
| cinematography = R. Sampath
| editing        = R. Devarajan
| studio         =  V. K. R. Pictures
| distributor    =  V. K. R. Pictures
| released       =  
| country        = India Tamil
}}
 1966 Cinema Indian Tamil Tamil film, directed by K. S. Gopalakrishnan and produced by K. Balaji. The film stars Sivaji Ganesan, K. R. Vijaya, S. V. Ranga Rao and S. V. Sahasranamam in lead roles. The film had musical score by K. V. Mahadevan.   

==Cast==
*Sivaji Ganesan
*K. R. Vijaya
*Nagesh in a Guest Appearance
*S. V. Ranga Rao
*S. V. Sahasranamam
*Chittor V. Nagaiah
*M. V. Rajamma
*M. S. Sundari Bai

==Soundtrack==
The music was composed by KV. Mahadevan.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Vaali || 03.31 
|-  Susheela || Vaali || 
|-  Susheela || Alangudi Somu || 
|-  Vaali || 03.22 
|-  Susheela || Vaali || 03.42 
|-  Vaali || 04.04 
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 


 