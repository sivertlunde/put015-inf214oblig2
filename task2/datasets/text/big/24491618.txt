Le Masque de fer
 
{{Infobox film
| name           = Le Masque de fer
| image          = LeMasqueDeFer.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Henri Decoin
| producer       = 
| writer         = Cecil Saint-Laurent
| narrator       = 
| starring       = Jean Marais Claudine Auger Jean-Francois Poron Gisèle Pascal
| music          = Georges Van Parys Pierre Petit
| editing        = Louisette Hautecoeur
| studio         = 
| distributor    = Gaumont
| released       =  :   
| runtime        = 127 min.
| country        = France Italy
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Le Masque de fer was a   and Le Vicomte de Bragelonne.

== Synopsis ==
 Mazarin gives Louis XIV.

In a scene not in the original book, dArtagnan grabs Isabelle de Saint-Mars, pulls up her dress, and spanks her to punish her for not keeping a promise she had made.

== Technical details ==

* Title : Le Masque de fer
* Director: Henri Decoin
* Writer : Laurent Devries, Gérald Devriès, Cécil Saint-Laurent after the works of Alexandre Dumas
* Producer : Roger de Broin Pierre Petit
* Music : Georges Van Parys
* Editing : Louisette Hautecoeur
* Scenery : Olivier Girard and Paul-Louis Boutié
* Genre : Adventure
* Length : 127 min
* Date of release:  

== Starring ==
*Jean Marais : dArtagnan
*Jean-François Poron : Henri/Louis XIV
*Sylva Koscina : Marion
*Gisèle Pascal : Mme de Chaulmes
*Jean Rochefort : Lastréaumont
*Claudine Auger : Isabelle de Saint-Mars
*Germaine Montero : Anne dAutriche
*Simone Derly : Marie Mancini
*Noël Roquevert : Mr de Saint-Mars
*Jean Lara : Renaud de Lourmes
*Jean Davy : Maréchal de Turenne
*Raymond Gérôme : Pimentel
*Philippe Lemaire : Vaudreuil
*Enrico Maria Salerno : Mazarin
*Clément Thierry : Maulévrier
*Max Montavon : le notaire

==Notes==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 