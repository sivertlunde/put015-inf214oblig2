Pirates (1986 film)
 
{{Infobox film
| name           = Pirates
| image          = Pirates_1986.jpg
| image size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Roman Polanski
| producer       = Tarak Ben Ammar
| writer         = Gérard Brach John Brownjohn Roman Polanski
| starring       = Walter Matthau Cris Campion Charlotte Lewis Roger Ashton-Griffiths Damien Thomas
| music          = Philippe Sarde
| cinematography = Witold Sobocinski
| editing        = Hervé de Luze
| studio         = Cathargo Films Accent-Cominco Cannon Film Distributing  
| released       =  
| runtime        = 112 minutes  
| country        = France Tunisia
| language       = English French Spanish
| budget         = $40 million 
| gross          = $1,641,825    $6,341,825     
}} adventure comedy film written by Gérard Brach, John Brownjohn, and Roman Polanski and directed by Polanski. It was screened out of competition at the 1986 Cannes Film Festival.   

==Plot==
Captain Thomas Bartholomew Red and his first mate, Frog, are stranded aboard a raft at sea.  Close to death from starvation and dehydration, Red tries to kill and eat Frog, but they are rescued by a Spanish galleon and are forced into slavery.  Red inspires the slaves to mutiny and takes control of the vessel. Frog falls in love with the niece of a Spanish colonial governor, and unsuccessfully romances her. Red covets a golden throne that the Spanish have taken from an Aztec king. Through several adventures, Red recruits a crew of cutthroats to steal the throne.  Despite their efforts, Red and Frog are ultimately stranded on a raft again, where Red sits on his golden throne and urges Frog to "fatten up".

==Cast==
* Walter Matthau as Captain Thomas Bartholomew Red
* Cris Campion as Jean-Baptiste/The Frog
* Charlotte Lewis as María-Dolores de la Jenya de la Calde
* Roger Ashton-Griffiths as Moonhead
* Damien Thomas as Don Alfonso de Salamanca de la Torre
* Olu Jacobs as Boomako
* Ferdy Mayne as Captain Linares David Kelly as Surgeon
* Anthony Peck, Anthony Dawson, Richard Dieux, and Jacques Maury as Spanish officers Jose Santamaria as Master at Arms
* Robert Dorning as Commander of Marines
* Luc Jamati as Pepito Gonzalez
* Emilio Fernández as Angelito Wladislaw Komar as Jesus

==Production==
Riding on the success of the highly acclaimed Chinatown (1974 film)|Chinatown, Roman Polanski began to write a screenplay for a swashbuckling adventure film called Pirates. Originally, Polanski intended for Jack Nicholson to play the central role of Captain Thomas Bartholomew Red, a grizzled old pirate, but complications arose partially due to the enormous fees Nicholson was demanding (according to Polanski, when Nicholson was asked what exactly he wanted, he replied, "I want more.") Following this, the production was delayed for a number of years when Polanski was arrested in California on a charge of unlawful sexual intercourse with a minor, after which he fled the United States to avoid sentencing. Production restarted later in Paris, this time with a different production company, Cathargo Films, and a new producer, Tarak Ben Ammar. The role of Captain Red went to Walter Matthau and the film finally came out in 1986, 12 years after it was first conceived.

A full scale galleon was built for the film in a shipyard in the port of Port El Kantaoui situated at the city of Sousse, Tunisia, adjacent to the Tarak Ben Ammar Studios, which had been constructed exclusively for this production. An accurate replica above the waterline, but sporting a steel hull and a 400 HP auxiliary engine, the "Neptune (galleon)|Neptune" was and still is entered into the Tunisian naval register and is currently a tourist attraction in the port of Genoa, where its interior can be visited for a 5 euro entry fee. 

==Release and reception== Paramount on Academy Award Best Costume Design.

== Accusations against Roman Polanski ==

On May 14, 2010, actress Charlotte Lewis and her attorney Gloria Allred accused director Roman Polanski of predatory sexual conduct against her when she was 16 years old, claiming that Polanski insisted that she sleep with him in return for casting her in Pirates. However, this accusation contradicts earlier accounts she had given of her relationship with Polanski.  

==See also==
* List of biggest box office bombs
* Cutthroat Island

==References==
 
* Roman by Roman Polanski

==External links==
 
*  
*  
*  
*   - photo of the galleon built for the film Pirates, now in Genoa.

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 