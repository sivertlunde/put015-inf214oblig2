Želary
{{Infobox film
| name= Želary
| image=Zelary.jpg
| caption =U.S. poster
| writer=Květa Legátová (novel)  Petr Jarchovský
| starring=Anna Geislerová György Cserhalmi
| director=Ondřej Trojan
| distributor=Sony Pictures Classics USA, Falcon Czech Republic
| released=September 4, 2003
| runtime=150 min.
| country=Czech Republic Slovakia
| language=Czech Russian German English
}}
 Slovak film 2004 Academy Best Foreign Language Film category. It was produced by Barrandov Studios in Prague.

The film is adapted from two works by Czech novelist Květa Legátová - "Želary," a collection of short stories published in 2001, and her 2002 book, "Jozova Hanule."   

== Plot ==

In 1940s Czechoslovakia, Eliška is a nurse who works alongside her lover, Richard, a respected surgeon.  Due to the rising pressure of the Nazis, an underground network has formed to help those in danger of persecution.  Richard is one of several doctors taking part in the resistance while Eliška acts as a messenger between contacts.

One night, Richard responds to an emergency call involving a patient in need of a risky operation.  Because she has the same blood type, Eliška takes part in the transfusion.  Days later, she arrives at Richards apartment to find their friend, Slavek, waiting with disturbing news.  The Gestapo have apprehended two of their members putting everyone at risk for discovery.  Stunned, Eliška receives word that Richard has emigrated, but has left papers for her to assume a new identity.  Slavek tells her that she must leave the city with a man named Joza - the patient who received the transfusion.  Seeing no alternative, Eliška (now Hanulka), leaves for the countryside and is reasonably troubled about her fate.

A mountain-dweller, Joza appears uncouth and disheveled, but is kind and considerate to her plight.  He provides her with a temporary sojourn in a small village where she quickly becomes an object of curiosity.  Some, such as Teacher Tkáč, the schoolmaster, are xenophobic and suspicious. The Nazis have killed anyone harboring an enemy to their cause and Eliška learns that staying as an asylum-seeker is a danger to them all.  She attempts to leave, but the village doctor reveals that the Nazis executed Slavek.  He tells her that to remain safe, she must marry Joza and live with him in the mountain village of Želary.

Initially, Eliška is passive to the idea. Joza takes her to their new home, a small cottage with no electricity, dirt floors and a fly-infested outhouse.  On the day of their wedding, Eliška rebels, but relents upon Jozas explanation that the villagers will not accept a strange, single woman into the fold.  Realizing that she must blend in, Eliška agrees and the two are married.  At the wedding, she meets most of Želarys inhabitants some of whom have their own stories.  Helenka is a young girl who lives near Joza and Eliška with her mother, Žeňa.  Her best friend is Lipka, a boy whom the villagers cast as a hoodlum, but who is actually homeless due to his step-father, Michals, dislike of him.  He survives through the goodwill of Žeňa, Lucka, the village midwife, and Old Goreik an elderly gentleman who lives with his daughter-in-law, Marie, a victim of spousal abuse.

At first, Eliška seems ill-suited to a country life.  She finds the villagers behavior in times of festivities to be raucous and crude and is particularly repulsed by Michal, the town drunk, who makes unwanted advances.  As time passes, however, Jozas patience and Žeňas gentle guidance help her to assimilate.  Despite this, Eliška remains wary of her husband, until one evening when she breaks their only lamp and fears a beating.  On the contrary, Joza is comforting and gives her a gift: a stack of books that she may like.  One day, he takes her to his mothers grave and Eliška is moved by the love and kindness he feels for those closest to him.  Equally touched by her small shows of affection, Joza falls in love with Eliška and she with him, an event that leads to the consummation of their marriage.

Years pass and Eliška bears witness to a number of incidents.  The Nazis, though scarce in Želary, make their presence known by killing an entire family for harboring partisans and then murdering an innocent man in front of everyone.  Eliška fears that the Gestapo will find her and receives a brutal reminder of her outsider-status when Michal attempts to rape her at the saw mill.  Helenka witnesses the attack and alerts Joza, who beats Michal and breaks his arm, rendering him an invalid.  As a result, his parents, the Kutinas, force Michals pregnant wife and Lipkas mother, Aninka, to do all the hard labor which leads to a miscarriage.  Lipka comes to the rescue by alerting Eliška, Lucka, and Žeňa, but it is too late.  Aninkas death destroys Michals reputation for good and redeems Lipka in the eyes of the villagers.

Later on, in the spring of 1945, Eliška is a nurse again and learns the art of herbal healing from Lucka.  The old woman divulges that Marie and her father-in-law, Old Goreik, are expecting a child, circumstances that have ramifications for everyone.  Following the birth, soldiers from the Red Army (who fought against the Germans) arrive with news that the war is over.  After a night of celebration, Joza reminds Eliška that she is free to leave (since the marriage was between him and "Hanulka" and is technically invalid).  To his content, Eliška responds that she wishes to stay with him always and falls asleep on the mountain in Jozas arms.

Elsewhere, Young Goreik, furious over his fathers relationship with his wife, arrives at Old Goreiks home with a drunken soldier whom he convinces to rape Marie.  Old Goreik arrives and shoots his son as well as the would-be rapist, but dies immediately when the latters brother arrives and shoots him.  Intoxicated and desensitized from battle, the soldiers interpret the killing as the act of fascists and wreak havoc on the villagers.  With the help of Lipka, many escape by traversing a swamp towards an old saw mill where Lucka and Eliška tend to the wounded.  Joza races back and forth between the haven and the village and rescues several people - including Michal.  Meanwhile, the soldiers kill the village priest and rape Žeňa.  Vojta, a farmhand, comes to her aid and Joza goes back to the village to retrieve him.  The next morning, they arrive at the saw mill and find soldiers headed in the same direction (Teacher Tkáč having convinced their commanding officer of the misunderstanding).  Eliška and the villagers are relieved, but their elation is cut short when Joza collapses (Vojta having shot him earlier after mistaking him for the enemy).  Devastated, Eliška kneels beside Jozas lifeless body and weeps.

Several years later, Želary is virtually abandoned due to the modernization of the town below.  Eliška, now with Richard, returns for a visit to the mountains and the cottage she once shared with Joza. Lucka, aged beyond words, emerges from the ruins and is shocked to see "Hanulka" but remembers that "nothing disappears off the mountain. There’s always tracks."  Astounded, Eliška asks, "Is it really possible? Youre alive?" to which Lucka responds, "Im none too sure.  Im none too sure at all." On the mountain, the women laugh over Eliška having found her way home and reuniting with her memories of Joza.

== Location ==
The film was mostly shot on location in the Malá Fatra mountains in the Northwest region of the Slovak Republic.

==Cast==

*Anna Geislerová as Eliška/Hanalka: a young nurse who leaves city-life behind when the Gestapo catch wind of the resistance.  She marries Joza and comes to live with him in Želary.  At first, she finds it difficult to adapt to country life, but toughens and eventually grows to love her husband and life on the mountain.
*György Cserhalmi as Joza: a mill worker who suffers a freak accident and meets Eliška for the first time when she donates her blood to him.  At first, Joza appears dull and rough-shod, but is actually a strong worker and a loyal friend to those who know him.   He marries Eliška to keep the villagers from being suspicious about her presence, but falls in love with her in the course of their marriage.
*Jaroslava Adamová as Lucka: the village midwife.  Eccentric and outspoken, Lucka is a link between all the inhabitants of Želary and knows everything there is to know about their daily lives.
*Iva Bittová as Žeňa: Joza and Eliška’s neighbor.  Žeňa is a widow who lives alone with her young daughter, Helenka.  She helps Eliška to become accustomed to country-living and enjoys the self-sufficiency of a single life.
*Anna Vertelárová as Helenka: Žeňa’s daughter and Lipka’s closest friend.  Although young, she is sharp and quick to the point.  Her initial presence during several disasters turns her into a human distress signal of sorts keeping Želary alert to danger.
*Tomás Zatecka as Lipka: a young boy whose mother, Aninka, is married to Michal, the village drunk.  Lipka ends up living in the woods as an outcast when Michal throws him out and must rely on the kindness of the villagers to survive.  Lipka is also Helenka’s “milk brother,” Žeňa having nursed him when his own mother, Aninka, was unable.  At the end of the film, he plays an essential role in saving the villagers from the wrath of the Red Army soldiers.
*Ondřej Koval as Michal Kutina: Lipka’s step-father and a raging alcoholic.  Irascible and shameless, he is the only person in Želary who openly voices his suspicion of Eliška.  When she rebuffs his advances, Michal tries to rape her, but is caught and beaten.
*Tatiana Vajdová as Aninka Kutina: Lipka’s mother and Michal’s wife.  When Joza maims her husband, she is forced to do all the work on their farm.  This leads to a miscarriage resulting in her death.
*František Velecký and Viera Pavliková as Mr. and Mrs. Kutina: Michal’s parents who refuse to acknowledge his violent behavior.  When Aninka lies dying from a miscarriage, they neglect to call the doctor for hours.  Her death brings them the shame and ire of the villagers.
* Edita Malovcic and Jan Triska as Marie and Old Goreik: daughter-in-law and father-in-law.  Marie comes to live with Goreik to escape her husband’s abuse.  They begin a romantic relationship which results in the birth of a son.
*Michal Hofbauer as Young Goreik: Old Goreik’s son and Marie’s husband.  In trying to get revenge from his wife and father, his actions bring harm to the whole village.
*Miroslav Donutil as the priest: the Catholic priest who marries Joza and Eliška.  He is one of only a few people who know why she has come to Želary.
*Jaroslav Dušek as Teacher Tkáč: the village schoolmaster.  At the beginning of the film, he is heavy-handed and dictatorial, but softens with the help of the priest.
*Juraj Hrčka as Vojta Juriga: a farmhand who is in love with Žeňa.
*Ivan Trojan as Richard: a surgeon and Eliška’s lover before she moves to Želary.
*Jan Hrušínský as Slavek: a doctor and a mutual friend of Richard and Eliška.  When the Gestapo find out about the resistance, he tells Eliška that she must leave with Joza to escape detection. He dies at the hands of the Nazis.

==Awards==

===Won===
* 2005 Bangkok International Film Festival - Golden Kinnaree Award for Best Actress (Anna Geislerová)
* Czech Lion - Best Actress (Anna Geislerová)
* Czech Lion - Best Sound
* Undine Awards - Best Young Actress (Anna Geislerová)

===Nominated===
* 76th Academy Awards for Best Foreign Language Film (Czech)
* 2005 Bangkok International Film Festival - Golden Kinnaree Award for Best Film (Ondrej Trojan)
* Czech Lion - Best Actor (György Cserhalmi)
* Czech Lion - Best Art Direction
* Czech Lion - Best Cinematography
* Czech Lion - Best Director
* Czech Lion - Best Editing
* Czech Lion - Best Film
* Czech Lion - Best Music
* Czech Lion - Best Screenplay
* Czech Lion - Best Supporting Actress (Jaroslava Adamová)

== Reception ==
Website Metacritic.com gave the film 66 out of 100 based on 20 critics, which equates to generally favorable reviews. 
At movie aggregator Rotten Tomatoes, the film gained 6.7 out of 10 with a 73% on the tomatometer from 50 reviews 

== External links ==
* 

==References==
 

 
 
 
 
 
 
 
 
 
 