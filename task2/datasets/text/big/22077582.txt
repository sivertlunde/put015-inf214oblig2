The Courtesans of Bombay
{{Infobox Film
| name           = The Courtesans of Bombay
| image          = CourtesansPoster.jpg
| image_size     = 
| caption        = American release poster
| director       = Ismael Merchant
| producer       = Ismail Merchant James Ivory Ruth Prawer Jhabvala 
| narrator       = 
| starring       = Kareem Samar Saeed Jaffrey Zohra Sehgal
| music          = 
| cinematography = Vishnu Mathur
| editing        = Amit Bose Rita Stern
| studio         = Merchant Ivory Productions Channel Four Television Corporation (UK) New Yorker Films (US)
| released       = January 1983 (UK) 19 March 1986 (US)
| runtime        = 73 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} James Ivory, Bombay Compound compound known as Pavan Pool, where women aspiring to work in the entertainment industry dance for donations from a male audience by day and, it is broadly suggested although never specifically stated, work at the Prostitution|worlds oldest profession by night. It was broadcast by Channel 4 in the UK in January 1983 and went into limited theatrical release in the United States on 19 March 1986.

Merchant was aware of the courtesans at an early age "from the visits from them we used to have during weddings at home, celebrations of childbirth, and other festivities. They provided the entertainment of singing and dancing, and I used to watch them." His first visit to Pavan Pool at the age of sixteen left him with a vivid memory that inspired him to record the activities and experiences of the courtesans on film. 

==Plot==
Kareem Samar, Saeed Jaffrey, and Zohra Sehgal are professional actors who interact with actual inhabitants of the area. Samar portrays a rent collector representing a landlord who was a friend of Merchant and approved the project.    Jaffreys role is that of an actor whose infatuation with one of the dancers becomes an obsession, and Segal is a retired courtesan who recalls her earlier life when her aunt arranged for her care by a wealthy benefactor.

==Critical reception== human interest and exotic entertainment" and added, "As we become accustomed to the unfamiliar music, its easy to get caught up in the show and forget the exploitation. The ample-hipped dancers move with abounding confidence in their own sexuality, and whether their somewhat dissolute-looking all-male audience is stirred by their performances or by the prospects for closer contact later does not much matter." 

In his review of the DVD release for the website DVD Talk, Glenn Erickson observed, "With its music and dance performances The Courtesans of Bombay would be a stunning documentary if it distinguished between authentic material and staged content. Even though there may literally be no difference, we have to trust the filmmakers as to the accuracy employed. The Merchant-Ivory team has a reputation beyond reproach, but doubts are raised by obvious staged content such as two courtesans sneaking messages to a shared boyfriend. The film lists a costume designer, a credit we dont expect to see on a documentary." 

==DVD release== fullscreen format Merchant Ivory Collection of Criterion Collection DVD and Blu-ray releases.

==See also==
* Dance bar
==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 