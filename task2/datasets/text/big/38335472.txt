The Flame (1920 film)
{{Infobox film
| name           = The Flame
| image          =The Flame (1920) - Ad 1.jpg
| image_size     =190px
| caption        =American ad for film
| director       = Floyd Martin Thornton 
| producer       = 
| writer         = Olive Wadsley (novel)   Floyd Martin Thornton
| starring       = Evelyn Boucher   Reginald Fox   Dora De Winton   Fred Thatcher
| music          = 
| cinematography = Percy Strong
| editing        = 
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       = July 1920
| runtime        = 6,300 feet feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent romance film directed by Floyd Martin Thornton and starring Evelyn Boucher, Reginald Fox and Dora De Winton. It was based on a novel by Olive Wadsley.

==Plot==
As summarized in a film publication,    Toni (Boucher) and her brother Fane (Thatcher) were adopted after the death of their father by their Uncle Charles (Cullin), and brought from the slums to the uncles beautiful home, much to the disgust of the uncles wife Lady Henrietta (De Winton). Fane is placed in a boys academy while Toni is sent to a convent. Fane turns out to be a haughty young Englishman while Toni cannot understand the changes in her brother. Lady Henrietta has had him raised to suit herself. Toni is then placed in a finishing school and, after she is expelled for going outside the walls, Lady Henrietta threatens to place her in a Dutch school where she would be punished for such a misdemeanor. Toni tells her troubles to Lord Robert Wyke, Lady Henriettas brother who is kindly and devoted to Toni. He is secretly in love with Toni, but is not free to tell her because of a hastily and unfortunate marriage to an adventuress. After Robert is compelled to leave for business and Toni hears that she will be sent to the Dutch school, the girl follows Robert to Florence where she remains with the housekeeper of Count de Soulnes. Word arrives that Robert has been murdered, but later he arrives and it turns that his wife was killed in an automobile accident, leaving him free to marry Toni.

==Cast==
* Evelyn Boucher as Toni Saumarez 
* Reginald Fox as Lord Robert Wyke 
* Dora De Winton as Lady Henrietta 
* Fred Thatcher as Fane 
* Rowland Myles as Boris Ritsky 
* Ernest Maupain as Sparakoff 
* Arthur M. Cullin as Sir Charles Saumarez 
* Clifford Pembroke as Captain Wynford Saumarez 
* Frank Petley as Miskoff 
* J. Edwards Barker as Dr. Lindsay 
* Sydney Wood as Fane, as a child

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 