Martial Outlaw
{{Infobox Film
| name           = Martial Outlaw
| image_size     = 
| image	=	Martial Outlaw FilmPoster.jpeg
| caption        = 
| director       = Kurt Anderson
| producer       = Pierre David
| writer         = John Bryant Pierre David Thomas Ritz George Saunders
| narrator       = 
| starring       = Jeff Wincott Gary Hudson Richard Jaeckel
| music          = Louis Febre
| cinematography = Jürgen Baum
| editing        = Michael Thibault
| studio         = 
| distributor    = Image Organization
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Martial Outlaw is a 1993 action/martial arts film written by Thomas Ritz, produced by Pierre David, directed by Kurt Anderson and stars Jeff Wincott, Gary Hudson and Richard Jaeckel.

== Plot ==
 
 DEA agent San Francisco. Los Angeles, LAPD cop who wants in on the action - from both sides. Fighting their way through a deadly "Russian Circle" of top Soviet martial arts experts, Kevin discovers that his brother may be playing both ends against the middle in a dangerous game of Russian Roulette, where the ultimate price-tag may be both their lives.

== Cast ==
*Jeff Wincott: Kevin White
*Gary Hudson: Jack White
*Vladimir Skomarovsky: Niko
*Krista Errickson: Lori White
*Richard Jaeckel: Mr. White
*Stefanos Miltsakakis: Sergei
*Natasha Pavlovich: Mia
*Liliana Komorowska: Marina
*Gary Wood: Lt. Evans
*Anna Karin: Waitress
*Ari Barak: Andrei
*Christopher Kriesa: Hal
*Richard Kwong: Shop Owner
*Will Leong: Stick Fighter
*Ed Moore: Commander Burke
*Thomas Ritz: Grunsky
*Christopher Ursitti: San Francisco Agent
*Edi Wilde: Ivan

== External links ==
*  

 
 
 
 
 
 


 