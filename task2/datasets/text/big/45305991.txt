The Top of the World
{{Infobox film
| name           = The Top of the World
| image          = 
| alt            = 
| caption        =
| director       = George Melford
| producer       = Jesse L. Lasky Adolph Zukor Jack Cunningham Ethel M. Dell
| starring       = James Kirkwood, Sr. Anna Q. Nilsson Joseph Kilgour Mary Mersch Raymond Hatton Sheldon Lewis Charles A. Post
| music          = 
| cinematography = Charles G. Clarke
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent Jack Cunningham and Ethel M. Dell. The film stars James Kirkwood, Sr., Anna Q. Nilsson, Joseph Kilgour, Mary Mersch, Raymond Hatton, Sheldon Lewis and Charles A. Post. The film was released on February 9, 1925, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*James Kirkwood, Sr. as Guy Ranger / Burke Ranger
*Anna Q. Nilsson as Sylvia Ingleton
*Joseph Kilgour as Sylvias Father
*Mary Mersch as Her Stepmother
*Raymond Hatton as Capt. Preston
*Sheldon Lewis as Doctor Kieff
*Charles A. Post as Hans
*Mabel Van Buren as Mary Ann
*Frank Jonasson as Joe
*Lorimer Johnston as Vreiboom

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 