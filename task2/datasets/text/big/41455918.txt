Jalaibee
{{Infobox film
| name           = Jalaibee
| image          = Jalaibee (film).jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Yasir Jaswal
| producer       = Eman Syed
| writer         = Yasir Jaswal
| starring       = {{plainlist|
* Danish Taimoor
* Ali Safina
}}
| studio         = REDRUM Films ARY Films
| distributor    = ARY Films
| released       =   
| country        = Pakistan
| language       = Urdu
| budget         =  
| gross          =  (Worldwide) 
}} Pakistani Caper caper action action Thriller thriller film  directed and written by Yasir Jaswal,   produced by Eman Syed. Jalaibee is a joint production of ARY Films & Redrum Films in association with Sermad Films and Jaswal Films. The film stars prominent TV actors Danish Taimoor  and Ali Safina   in lead roles along with Adnan Jaffar, Sajid Hasan, Uzair Jaswal (directors younger brother),  Wiqar Ali Khan,  Sabeeka Imam and Zhalay Sarhadi.   
 Ford Mustang 73 in the film as well as its mascot. It is about the intertwined stories of numerous relatable characters who are all struggling with their problems, and who somehow connect on a unifying level.   

== Plot ==
 
Jalaibees story revolves around two orphaned friends Billu (Danish Taimoor) and Bugga (Ali Safina) who get tangled up in a debt with the local mafia called The Unit. As they look for ways to pay the debt before The Unit comes to collect, they find out they are in deeper waters than they thought. Thinking of different ways to pay the debt, they stumble upon an idea to rob a local casino. To execute this heist they enlist the help of a bar dancer named Banno (Zhalay Sarhadi) to seduce to the owner of the casino.

At the same time another man Ali (Wiqar Ali Khan), along with his brother and partner, Jimmy (Uzair Jaswal), wants revenge against The King, a mafia don who killed his father and forced his mother to suicide when he was young. The Kings front man Dara (Adnan Jaffar) is caught between collecting the debt and handling the kidnapping while retaining fear and control over the city.

Simultaneously, Eman (Sabeeka Imam), the daughter of wealthy industrialist and contender for the Prime Ministers seat, Akbar Chaudhary(Sajid Hassan), is trying to talk to her father about Ali, her beau. This proves to be difficult with the election coming closer by the day.

== Cast ==
* Danish Taimoor as Billu
* Ali Safina as Bugga
* Wiqar Ali Khan as Ali
* Zhalay Sarhadi as Bunno
* Sajid Hasan as Akbar
* Sabeeka Imam as Eman
* Adnan Jaffar as Dara
* Uzair Jaswal as Jimmy
* Salman Shaukat

== Production ==

=== Casting ===
In interview with The National, director said: "The casting was done with my casting director Ehtesham Ansari, who has also styled everyone in the film. There were a couple of people I had in mind when I was writing every character. Some of them didn’t work out, some did. One who did work out was Ali Safeena, who plays Bugga. He is the narrator of the story and I could always see him as Bugga. I knew from the start that I didn’t want any big names in my film. It’s not to take anything away from them, but I wanted to broaden the horizon a bit. We have a lot of talented actors and actresses who never get a chance to appear on the big screen because not that many films are being made." 

=== Filming === Skyfall and Ironman 3 were shot. Jalaibee is part animated film, artwork of which is illustrated by Babrus Khan in Lahore. VFX of the film is done by London based Sharp Image. 

=== Marketing === Ford Mustang 73 and announcing teasers release date. The  teaser trailer of the film was released on December 25, 2013 on ARY Digital Network, films official Facebook page  and on vimeo by Jaswal Films simultaneously.  The teaser was also played on big screen at 1st ARY Film Awards where Jalaibee team including Wiqar Ali Khan, Ali Safina, Uzair Jaswal, Sabeeka Imam, Zhalay Sarhadi, and Asal Din Khan given the introduction of film. On October 16, 2014 second title poster was released on Danish Taimoors Facebook page and later on films page. On January 23, 2015 final poster was revealed along with films release date  followed by character posters in the same week. On Januar 31, 2015 theatrical trailer was unveiled on official facebook and on channels run by ARY Digital Network.   In February, Malik Riaz of Bahria Town joined ARY Films to revive Pakistani cinema by announcing a reward for those, coming  to watch Jalaibee.   Founder and CEO of ARY Digital Salman Iqbal revealed that Malik Riaz has purchased 10,000 tickets of Jalaibee for residents of Bahria Town giving film a kick start of 5 million before it release. 

== Soundtrack ==
{{Infobox album
| Name = Jalaibee
| Type = Soundtrack
| Artist = Various
| Cover = Jalaibee soundtrack cover.jpg
| Released =   Feature film soundtrack
| Length =  
| Label = EMI Pakistan
}}
The film features 9 tracks by various artists including Qayaas, Uzair Jaswal and Humaira Arshad, only one of them is performed in film while others are used as a background score.   Song Jee Raha written & composed by Umair Jaswal and Sarmad Ghafoor was released on March 18, 2015  whereas films soundtrack album was released on March 26 by EMI Pakistan.
{{Track listing
| lyrics_credits = no
| music_credits = yes
| extra_column = Singer(s)
| total_length = 27:59
| title1 = Upside Down
| lyrics1=  
| music1= Kostal
| extra1 = 
| length1 = 1:10 note1 = Intro music
| title2 = Come Get Some
| lyrics2= 
| music2=  
| extra2 = Shizzio
| length2 = 3:32
| title3 = Jee Raha
| lyrics3= Umair Jaswal
| music3=  
| extra3 = Umair Jaswal
| length3 = 4:13
| title4 = Jalaibee
| lyrics4=  
| music4=  
| extra4 = Umair Jaswal
| length4  = 2:52
| note4 = Title song
| title5 = Jhoom Baraber
| lyrics5= 
| music5=  
| extra5 = 
| length5 = 4:55
| title6 = Daaru Di Bottal
| lyrics6= 
| music6=  East Side Story
| extra6 = 
| length6 = 3:36
| title7 = Qatil Akh
| lyrics7= 
| music7= East Side Story
| extra7 = 
| length7 = 3:22
| title8 = Jawani
| lyrics8= Shuja Haider
| music8=  Shuja Haider
| extra8 = 
| length8 = 3:27
| note8 = Item song
| title9 = Jhooti
| lyrics9=
| music9=  
| extra9 = 
| length9 = 0:54
}}

== Release ==
On December 26, 2013 an agreement was signed between Redrum Films and ARY Films for film distribution. As per distributor ARY Films, film was announced to release in summer season 2014,  but for a reason it was postponed for 2015 release. Explaining the reason for the delay, director said "The production team had run out of funds and the reason behind releasing the teaser was strategic. Wanting to get financers on board, we worked on the pre-production of the film quite extensively, which resulted in delay of the film’s release." On 23 January 2015 ARY Films announced to release the film on March 20, 2015 across Pakistan, U.K and U.S.A simultaneously.   

== Reception ==
===Box office===
The film collected   before its release and became first of Pakistani films to do so because property person Malik Riaz bought 10,000 tickets for the Bahria Town residents to watch Jalaibee in cinemas.  Movie had good number of public previews on Thursday and came out well on them collecting   which is biggest preview collections ever in Pakistan.   Movie opened very well on Friday with day one being 7th biggest ever and 2nd biggest for Pakistani movies as movie collected  .   Movie grew around 10% on Day two collecting   with best growth in Karachi and Islamabad.  Movie collected   on Sunday taking 3 Day Weekend to  .  Film had national Holiday on Monday but movie couldnt sustain as figures went down to   taking 4 Day Extended Weekend to  .Film fell further on Tuesday as it was first non-Holiday Weekday of film. Film had low yet steady Weekdays taking Week One total to  .Film held best in Karachi over Weekdays.  After low Weekdays film needed massive growth over 2nd Weekend to ensure good run at boxoffice but movie fell around 65% and grossed   in its 2nd Weekend.Film had fallen in Karachi also which was holding best over first Weeks Weekdays. 10 Days total stood at  .  The film collected collected around    at domestic box office and around   at overseas market taking lifetime gross to   at the end of April.   

===Critical response===
Saba Khalid of DAWN.com reviewed the film and wrote, "Hopefully, we’ve also graduated to a time that reviews of Pakistani movies will no longer be meant to just stroke director’s egos but really analyze and critique the originality and creativity of scripts, themes, characters, and judge the overall level of acting and direction displayed in the film."  Rafay Mahmood of The Express Tribune rated movie 2/5 stars and given the verdict, "All in all Jalaibee has its moments but it is definitely not entertaining. It does pick up a little in the second-half but what kills it towards the end is a dragged moral debate for a film that purely stands on immoral choices and of ourse a very Na Maloom Afraad climax."  Momin Ali Munshi of Galaxy Lollywood rated 2.5/5 stars and wrote, "Jalaibee is sweet, but at certain bites it goes pretty bland. You watch it for its amazing cinematography, style, and a  promising soundtrack. You don’t expect from it an acting treat and don’t go looking for a thrill. There are loops within loops, and some more of those promised twists with them, that you have to give to the writer. But then there are some loopholes as well, but on the whole this film is worth a watch." 

==See also==
* List of highest-grossing Pakistani films
* List of Pakistani films of 2015

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 