Being Elmo: A Puppeteer's Journey
 
{{Infobox film
| name = Being Elmo: A Puppeteers Journey
| image_size = 
| image	= Being Elmo- A Puppeteers Journey FilmPoster.jpeg
| alt = 
| caption = 
| director = Constance Marks
| producer = Constance Marks Corinne LaPook James Miller
| writer = Philip Shane Justin Weinstein
| narrator = Whoopi Goldberg Joel Goodman
| cinematography = James Miller
| editing = Philip Shane Justin Weinstein
| studio = Constance Marks Productions
| distributor = Submarine Deluxe   Mongrel Media   Madman Entertainment  
| released =  
| gross = $304,052 {{cite web |url=http://www.boxofficemojo.com/movies/?id=beingelmo.htm |title=Being Elmo:
A Puppeteers Journey |publisher= Box Office Mojo}} 
| runtime = 77 minutes
| country = United States
| language = English
}}
Being Elmo: A Puppeteers Journey is a 2011 American documentary film about Kevin Clash, a puppeteer who performed the Sesame Street character Elmo. The film was directed by Constance Marks
.

==Synopsis==
 
The film focuses on Clashs early career in Baltimore, Maryland. It covers his meeting and interactions with Sesame Street creator Jim Henson, puppet maker Kermit Love, and the phenomenal success of Elmo. (Clash joined Sesame Street in 1984. He resigned from Sesame Street in 2012 amid allegations of improper sexual conduct with minors, which he denied.    This was not included in the documentary since it was released before the accusations were made.)

==Participants==
* Whoopi Goldberg – Herself/Narrator
* Kevin Clash – Himself
* Jim Henson – Himself (archive footage)
* Frank Oz – Himself
* Bill Barretta – Himself
* Caroll Spinney – Himself
* Rosie ODonnell – Herself
* Fran Brill – Herself
* Martin P. Robinson – Himself

==Release== premiered at the 2011 Sundance Film Festival.

==Reception==
The film received warm-hearted reviews by critics when it was released. Rotten Tomatoes gives the film a score of 94% based on reviews from 72 critics. 

    is wandering into a movie that you think couldnt possibly amount to anything much and being knocked out by it. The documentary Being Elmo: A Puppeteers Journey, which should make Kevin Clash a household name, is an inspiring and joyous celebration of art, skill, determination and making kids happy... Clash is a remarkable talent, a true master of his field, and the importance of what he does is considerable. Sick children apparently have asked to meet Elmo as their dying wish. If you can watch one such encounter without crying, youre a stronger man than I. Clash, pro that he is, doesnt cry. Because where there is Elmo, there must be happiness."  Look at OKC praised: "As director Constance Marks proves in her detailed and surprisingly emotional documentary Being Elmo: A Puppeteers Journey, Clashs love of the art form eventually led him to create one of the most popular characters in childrens television." 

== References ==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 