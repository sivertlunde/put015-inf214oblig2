The Three Musketeers (1986 film)
 
{{Infobox Film
| name           = The Three Musketeers
| image          = Three Musketeers 1986.jpg
| image_size     = 
| caption        = 
| director       = 
| producer       = Tim Brooke-Hunt
| writer         = Keith Dewhurst, Alexandre Dumas, père (original author)
| narrator       = 
| starring       = Ivar Kants (dArtagnan)  Noel Ferrier (Cardinal Richelieu)  Kate Fitzpatrick (Milady)
| music          = Sharon Calcraft
| cinematography =  Peter Jennings, Caroline Neave
| distributor    = NuTech Digital
| released       = 1986 (Australia)
| runtime        = 49 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 French novel, The Three Musketeers, first published in 1844, and was adapted by Keith Dewhurst.  It was produced by Tim Brooke-Hunt and featured original music by Sharon Calcraft.  The copyright in this film is now owned by Pulse Distribution and Entertainment  and administered by digital rights management firm NuTech Digital.  It is available from NuTech Digitals label DVD Ltd.

== Plot summary ==
 Felton to guard her.  Felton falls in love with Milady; she seduces him and asks him to murder the Duke, which he does. Though she believes herself safe at a convent, Constance receives a visit from her supposed benefactor, Milady de Winter, who poisons her; dArtagnan arrives at the scene and she dies in his arms.  Together with Athos, Porthos and Aramis, dArtagnan corners Milady and she is captured and sentenced to die for her crimes; Milady is then revealed to be Athos own wife.  The villains defeated and the countrys honor restored, the four companions return to their homeland mourning the lives lost but cheering for their triumphs.

== See also ==
* The Three Musketeers
* Alexandre Dumas, père
* Burbank Films Australia
* List of films in the public domain

== References ==
 

== External links ==
*  
*  
*   at the Big Cartoon DataBase

 
 

 
 
 
 
 
 
 
 