Birthday Girl
 
{{Infobox film
| name           = Birthday Girl
| image          = Birthday Girl (movie poster).jpg
| director       = Jez Butterworth
| producer       = Steve Butterworth Diana Phillips
| writer         = Tom Butterworth Jez Butterworth
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Nicole Kidman Ben Chaplin
| music          = Stephen Warbeck
| cinematography = Oliver Stapleton
| editing        =  FilmFour
| distributor    = Miramax Films (US)
| released       =  
| runtime        = 93 min.
| country        = United Kingdom United States
| language       = English, Russian
| budget         = USD 13,000,000
| gross          = USD 16,171,098 (worldwide)
}}
 criminal comedy-drama English bank Russian are spoken interchangeably in the film.

== Plot ==
John Buckingham (Ben Chaplin), a lonely St Albans bank clerk, orders a mail-order bride Nadia (Nicole Kidman) from Russia on the Internet. John is uncomfortable and shy, but Nadia is sexually bold. Though Nadia cannot speak English and John cannot speak Russian, they soon bond. Later on, a man she introduces as her cousin Yuri (Mathieu Kassovitz) and his friend Alexei (Vincent Cassel) turn up to celebrate her birthday. Alexei soon shows that he has a temper. After a violent altercation, Alexei holds Nadia hostage and demands a ransom from John. John has grown to care for Nadia and is forced to steal from the bank where he has worked for ten years. After the ransom is paid, he realises that he has been the victim of an elaborate con.  Nadia, Yuri, and Alexei are criminals, and Alexei is actually Nadias boyfriend.

John learns that the trio have carried out the same scam on men from Switzerland and Germany, among others.  They take him prisoner, strip him down to his underpants, and tie him to a toilet in a motel. He eventually manages to free himself and quickly learns that Nadia has been left behind after Alexei discovered she was pregnant. John gets dressed and subsequently gets into a scrap with Nadia, who later reveals that she can indeed speak English and that her name is not Nadia.

John takes Nadia to turn her into the police - hoping to clear his name as a wanted bank robber. Ultimately, however, he sympathises with her and decides against it. He leaves her at the airport, where she is kidnapped by Alexei – who now wants Nadia to have the baby. John rescues her, tying Alexei to a chair. They make common cause against the two Russian men. Nadia informs John that her real name is Sophia. John, disguised as Alexei, leaves for Russia with Sophia.

== Cast ==
*Nicole Kidman as Sophia, alias Nadia
*Ben Chaplin as John Buckingham
*Vincent Cassel as Alexei
*Mathieu Kassovitz as Yuri

==Reception==
The film was a generally moderate commercial success, with a budget of $13 million it grossed $16,171,098. 

It has a 58% fresh rating on Rotten Tomatoes, indicating a neutral-positive critical reception.  Jason Solomon of The Observer praised the casting  "Cassel, Kassovitz and Kidman are beautifully graceful against the backdrop of signs to Tring and Newbury." He continued  "The comedy here is gentle, formed of linguistic misunderstandings and cultural clashes and Chaplins constant efforts to be polite are rather charming. Kidmans exoticism, encapsulated by her peasant-chic wardrobe, is fresh air in St Albans."  The BBC reviewer gave the film 4 out of 5 stars, praised Chaplins and Kidmans "infectious performances" and describe it as a "sparky" and "deviant topical comedy which is funny from start to finish."  CNN praised Kidmans "astounding range" and applauded the dialogue as "often sharp, scathingly witty, and displays a wry intelligence." 

The New York Times described the film as "competent" but decried the plot as too "insubstantial". 

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 