Ceylon (film)
{{Infobox film
| name           = Ceylon
| image          = Ceylon santosh sivan.jpg
| caption        = Theatrical poster
| director       = Santosh Sivan
| producer       = Mubina Rattonsey Santosh Sivan
| screenplay     = Santosh Sivan Zazy Sharanya Rajgopal
| narrator       = Arvind Swamy Sugandha Ram Saritha
| music          = Vishal Chandrashekhar
| cinematography = Santosh Sivan
| editing        = T. S. Suresh
| studio         = Santosh Sivan Films
| distributor    = 
| released       =  
| runtime        = 125 mins
| country        = India
| language       = Tamil English
| budget         = 
| gross          = 
}} civil war in Sri Lanka. The film premiered at the 2013 Busan International Film Festival   and had a theatrical release in India on 28 March 2014.  Although it was positively received by critics, the film was withdrawn from theatres three days later owing to protests from Tamils in Tamil Nadu. 

==Cast==
* S. Karan as Nandan Sugandha Ram as Rajini
* Saritha as Tsunami Akka
* Karunas as Stanley
* Shyam Sundar as Ravi
* Soumya Sadanand
* Vikram Chakravarti
* M. K. Vijayan
* Janaki as Stella

==Production==
In 2009, it was first reported that Santhosh Sivans next film would be "a hard-hitting political film scripted around the LTTE (Liberation Tigers of Tamil Eelam)-related strife and the death of its leader Velupillai Prabhakaran".  In June 2012 then he stated "in Ceylon I want to look at the Sri Lankan situation from the perspective of a bunch of youngsters who get caught in the crisis. Its more about the outsiders response to the horrors than about the politics".    He also told that he will shoot in Sri Lanka after the rains and that he didnt want "Ceylon to look like a tourists attraction".  Sivan described Ceylon as his pet-project. 
 Sugandha Ram, who acted in Tere Bin Laden before, was chosen to portray the female lead.    Sugandha later told that the film was offered in 2010 to her but did not happen then and that the script was later altered and she got an opportunity to audition for the role again.  It was reported that Saritha played the role of a person who runs the orphanage.  According to Sivan, the idea for the film was conceived over a Sri Lankan lunch with a friend during which they met a lady who ran an orphanage in Sri Lanka.    Sivan wanted director Stanley for a role, but cast Karunas for it since he could also sing. Karunas was said to have done a serious role unlike his usual comic self.  Sivan stated, "my film is about human aspirations and emotions shattered by the war, not about the politics behind it".  He also clarified the film will have nothing to do with the LTTE.  Sivan spent months researching and getting footage for the film.  He shot it in Tamil as Inam but also made the film into English for a larger audience and titled it Ceylon.  The Tamil version was longer and included songs, scenes for comic relief and the making of the film over end credits and the English version, Sivan said, "is much tighter". 
 candid as if they had been captured through a mobile phone or a hand-held camera     as he found out that mobile phones played an important role in the Sri Lankan wars.  Editor Suresh was also asked "to keep all good-looking shots aside" as Sivan felt that the film "should be raw and rustic" and not beautiful. 
 Censor Board with a clean U certificate in October 2013.  Actor Udhaya (actor)|Udhayas wife Keerthika Udhaya had dubbed for Sugandha in all versions.

==Music==
Vishal Chandrasekhar was signed as the music director. Sivan stated that "the background score is heart thumping" since it was a thriller film, but that a Baila, and "a few romantic numbers, treaded in a different way" were also included. 

==Release==
The film was first screened at the 18th Busan International Film Festival.   The films Tamil Nadu distribution rights were bought by N. Lingusamys Thirrupathi Brothers in February 2014.  Prior to the theatrical release, Sivan and Thirrupathi Brothers held premiere shows in Chennai and Mumbai.   The film was released in Tamil Nadu theatres on 28 March 2014.

Following protests from Thanthai Periyar Dravidar Kazhagam (TDMK) activists, who had attacked the Balaji Theatre in Puducherry, stating the film projects the Sri Lankan Civil War in bad light, four scenes and one dialogue were removed the following day.  After Marumalarchi Dravida Munnetra Kazhagam politician Vaiko too severely criticized the film for being pro-Sinhala people|Sinhalese, Lingusamy decided to stop screening and withdraw the film from theatres from 31 March 2014 onwards.  

==Critical reception== IANS gave it 4 stars out of 5 and wrote, "Inam comes straight from the heart of Santosh Sivan. It deserves to be accepted, embraced and celebrated". 

The Hollywood Reporter in its review wrote, "Romancing in slow-motion; musical numbers ill-fitting the narrative flow; high-octane shootouts laced with the odd comic touch; a highly-strung, tragic final half hour intended to stir emotions -- Santosh Sivan’s latest offering boasts of all the hallmarks of commercial Indian cinema, which should allow the Kerala-born director to continue the fine run he has had in the past few years...but more focus and context would have helped the film live up to its seemingly more historical-epic title of Ceylon".  Gulf News wrote, "Director Santosh Sivan has crafted his characters with great care and their unique traits set each one apart. Matched with a laudable performance by the cast, Inam keeps the audience engaged. With Sivan’s signature on every frame, Inam is sheer poetry". 

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 