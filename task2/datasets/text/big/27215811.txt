American Nightmare (film)
 
{{Infobox Film
| name           = American Nightmare
| image          = 
| image_size     = 
| caption        = 
| director       = Don McBrearty
| producer       = Ray Sager
| writer         = John Sheppard
| narrator       = 
| starring       = Lawrence Day Lora Staley Neil Dainard Michael Ironside
| music          = 
| cinematography = 
| editing        = 
| distributor    = Cineplex Odeon Films|Pan-Canadian Film Distributors (Canada)
| released       = 1983
| runtime        = 88 min.
| country        = Canada English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

American Nightmare is a 1983 horror film shot in Canada.  It is directed by Don McBrearty.

==Plot==
Wealthy pianist Eric Blade (Lawrence Day) tries to locate his missing runaway sister in an unnamed city. His journey takes him to seedy sex shops and strip joints.  The police arent really interested in a missing hooker until they find out she is the daughter of a wealthy businessman.

==Cast==
*Lawrence Day as Eric
*Lora Staley as Louise
*Larry Aubrey as Dolly
*Michael Ironside as Sgt. Skylar

==External links==
* 

 
 
 

 