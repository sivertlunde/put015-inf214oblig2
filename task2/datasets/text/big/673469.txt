Flatliners
 
{{Infobox film
| name           = Flatliners
| image          = Flatliners.jpg
| caption        = Theatrical Release Poster
| director       = Joel Schumacher
| producer       = Michael Douglas Rick Bieber
| writer         = Peter Filardi
| starring       = Kiefer Sutherland Julia Roberts William Baldwin Oliver Platt Kevin Bacon
| music          = James Newton Howard
| cinematography = Jan de Bont
| editing        = Robert Brown
| studio         = Stonebridge Entertainment
| distributor    = Columbia Pictures
| released       =    
| runtime        = 115 minutes
| country        = US
| language       = English
| budget         = United States dollar|$26 million
| gross          = $141,181,969
}} horror thriller thriller film beyond death. They conduct clandestine experiments that produce near-death experiences. The movie was directed by Joel Schumacher, and it was nominated for an Academy Award for Best Sound Editing in 1990 (Charles L. Campbell and Richard C. Franklin). It was filmed between October 1989 and January 1990. 

==Plot==
Nelson Wright (Kiefer Sutherland), convinces four of his medical school classmates — Joe Hurley (William Baldwin), Dave Labraccio (Kevin Bacon), Randy Steckle (Oliver Platt) and Rachel Manus (Julia Roberts) — to help him discover what lies beyond death. Nelson flatlines for one minute before his classmates resuscitate him. While "dead", he experiences a sort of afterlife. He sees a vision of a boy he bullied as a child, Billy Mahoney. He merely tells his friends that he cant describe what he saw, but something is there. The others decide to follow Nelsons daring feat. Joe flatlines next, and he experiences an erotic afterlife sequence. He agrees with Nelsons claim that something indeed exists. Dave is third to flatline, and he sees a vision of a girl, Winnie Hicks (Kimberly Scott), that he bullied in grade school. The three men start to experience vivid hallucinations that are related to their afterlife visions, but Nelsons circumstances are particularly freakish; he is repeatedly physically attacked by Billy Mahoney and each day presents with fresh cuts and wounds. Joe, engaged to be married, is haunted by home videos that he secretly filmed of his sexual trysts with other women. Dave is confronted by a vision of Winnie Hicks on a train, and she verbally taunts him like he did to her. 

At Rachels insistence, the group agrees to let her flatline next. Dave, disturbed by his hallucinations, has a change of heart and tries to stop the others from giving Rachel their same fate, but she has already flatlined by the time he arrives. They are almost unable to bring Rachel back to life after the power goes out, as the men cannot shock her with the   when she was a young girl. One by one, the other men open up about their harrowing experiences to one another, and Dave decides to put his visions to a stop. He tracks down Winnie Hicks, now grown up, and travels to her home to offer an apology. Winnie thanks him, and accepts his apology. Dave immediately feels a weight lifted off his shoulders. Nelson, who has accompanied Dave on the trip, remains alone in Daves truck and catches a glimpse of Billy Mahoney darting past outside. Suddenly Billy appears inside the truck and attacks him with a pickaxe. Nelson struggles to fend him off and Dave arrives on the scene just in time to end the hallucination and prevent serious injury to Nelson, revealing that Nelson was alone in the truck and that he was attacking himself with the pickaxe. Meanwhile, Joes fiancée, Anne (Hope Davis), unexpectedly comes to his apartment, and she breaks off their engagement after discovering his videos. Joes visions cease after Anne leaves him. Rachel seeks comfort in the arms of Dave, and the two spend the night together in bed. While Rachel and Dave are together, Nelson brings Steckle and Joe to the gravesite of Billy Mahoney. He reveals a long-kept secret: he and his friends inadvertently killed Billy as youngsters when they chased him up a tree and pelted him with rocks, causing him to fall to his death. Nelson mutters to himself about making amends, then suddenly storms off, leaving Joe and Steckle stranded.

Dave leaves Rachel alone in order to pick up Joe and Steckle from the cemetery. While alone, Rachel goes to the bathroom, and encounters her father. He apologizes to his daughter, and her guilt over his death is lifted when she discovers that he was addicted to heroin. Rachel receives a phone call from Nelson, who tells her that he needs to flatline again in order to make amends. He apologizes for involving her and their friends in his plan before hanging up. Rachel and the other three men realize what Nelson is planning and race to save him, eventually reaching him more than nine minutes after his phone call. They work feverishly to save him, but too much time has passed and they decide to give up. Meanwhile, in the afterlife a young Nelson has reversed roles with Billy Mahoney and is being pelted with rocks by him and his other friends while up in the tree. Young Nelson falls from the tree, morphing into the older Nelson just before hitting the ground. He looks up to see Billy Mahoney standing over him and smiling before slowly walking away into a bright light, having made peace. In an act of utter frustration, Dave gives Nelson one last shock. Miraculously, Nelson is resuscitated, and after regaining consciousness he tells them, "Today wasnt a good day to die."

==Cast==
* Kiefer Sutherland - Nelson Wright
* Julia Roberts - Rachel Manus
* Kevin Bacon - Dave Labraccio
* William Baldwin - Joe Hurley
* Oliver Platt - Randy Steckle
* Kimberly Scott - Winnie Hicks
* Joshua Rudoy - Billy Mahoney
* Benjamin Mouton - Mr. Manus
* Hope Davis - Anne Coldren
* Patricia Belcher - Edna
* Beth Grant - Housewife

==Critical reception==
Upon its release Flatliners received mixed reviews. While the film was praised for its overall premise and striking visual style, as well as the strong cast, it was criticized in some quarters for descending into silliness. The review aggregator Rotten Tomatoes reports that 48% of critics give the film a positive review, based on 42 reviews.  

In her review for   praised the film as:   and called the cast:   But Ebert criticized Flatliners for   Similarly,   gave the film a "D" rating and Owen Gleiberman wrote:  
 The Washington Posts Rita Kempley loved the film, calling it:  
 defibrillators has been criticised as medical nonsense, since they do not help in case of an asystole.   

===Box Office===
The movie debuted at No.1.  With an estimated budget of $26 million, the film took in $61.5 million in the United States during its theatrical run.  

==References==
 

==External links==
 
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 