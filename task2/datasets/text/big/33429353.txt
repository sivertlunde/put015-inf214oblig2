Heintje: A Heart Goes on a Journey
{{Infobox film
| name           = Heintje: A Heart Goes on a Journey
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Werner Jacobs
| producer       = Heinz Willeg
| writer         = Eberhard Keindorff Johanna Sibelius
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Heintje Simons Heinz Reincke
| music          = Raimund Rosenberger
| cinematography = Heinz Hölscher
| editing        = Hermann Haller
| studio         = Terra-Filmkunst
| distributor    = Constantin Film
| released       =  
| runtime        = 104 min
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
}} West German musical film directed by Werner Jacobs and starring Heintje Simons, Heinz Reincke and Gerlinde Locker.

==Cast==
*Heintje Simons: Heinz Heintje Gruber
*Heinz Reincke: Alfred Teichmann
*Gerlinde Locker: Hanna Schwarz
*Solvi Stubing: Gerdi Weber
*Ralf Wolter: Harry
*Mogens von Gadow: Hugo Neubert
*Karin Field: Else
*Sieghardt Rupp: Günter Schelle
*Dagmar Altrichter: Monika Klausen
*Peter W. Staub: Wache
*Hans Terofal: Rudi
*Edith Hancke: Lieschen
* : Polizei-Inspektor
*Rudolf Schündler: Rektor Neumann

==External links==
* 

 

 
 
 
 
 

 