The Yes Man
 
 
{{Infobox film
| name           = The Yes Man
| image          = Il Portaborse The Yes Man.jpg
| caption        = Film poster
| director       = Daniele Luchetti
| producer       = Angelo Barbagallo Nanni Moretti
| writer         = Daniele Luchetti Franco Bernini
| narrator       =
| starring       = Silvio Orlando
| music          =
| cinematography = Alessandro Pesci
| editing        = Mirco Garrone
| distributor    =
| released       = 5 April 1991
| runtime        = 95 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

The Yes Man ( ) is a 1991 Italian drama film directed by Daniele Luchetti. It was entered into the 1991 Cannes Film Festival.   

==Cast==
* Silvio Orlando - Luciano Sandulli
* Nanni Moretti - Cesare Botero
* Giulio Brogi - Francesco Sanna
* Anne Roussel - Juliette
* Angela Finocchiaro - Irene
* Graziano Giusti - Sebastiano Tramonti
* Lucio Allocca - Remo Gola
* Dario Cantarelli - Carissimi
* Antonio Petrocelli - Polline
* Gianna Paola Scaffidi - Adriana
* Giulio Base - Autista di Botero
* Guido Alberti - Carlo Sperati
* Renato Carpentieri - Sartorio
* Silvia Cohen - Boteros wife (as Silva Cohen)
* Roberto De Francesco - Zollo
* Dino Valdi - Federico Castri

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 