The Fuller Brush Man
{{Infobox film
| name           = The Fuller Brush Man
| image          = Fuller Brush Man poster.jpg
| image_size     =
| caption        = Theatrical poster
| director       = S. Sylvan Simon
| producer       = S. Sylvan Simon Edward Small
| writer         = Devery Freeman Frank Tashlin
| based on = story by Roy Huggins
| starring       = Red Skelton Janet Blair
| music          =
| cinematography =
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Fuller Brush Man is a 1948 comedy film starring Red Skelton as a door-to-door salesman for the Fuller Brush Company who becomes a murder suspect.

==Plot== Don McGuire), who works at her company. Red gets a chance to prove himself worthy sooner than he had expected when he is fired from his job as a cleaner by his boss, Gordon Trist (Nicholas Joy), because he accidentally sets a trash can on fire in the line of duty. Ann gets him a chance to show his skills as a door-to-door salesman for the Fuller Brush company, and he is teamed up with her friend Keenan. Both Ann and Red are unaware that Keenan himself had a romantic interest in Ann, and wants to get Red out of the way as soon as possible, so he can pursue Ann without competition. Keenan gives Red the hardest cases, and Red fails tremendously with his task of selling to an almost impossible potential customer. Seeing the result of Reds first attempts at the job as a salesman, Keenan comes up with the idea of a bet - the winner gets to pursue Ann without interference of the other man - which he suggests to Red. The bet is that Red wont be able to sell a single brush to the households on their run. Red takes the bet, and the next household on their run is his old boss Gorodon Trists. Gordon recognizes Red and sends him packing almost immediately, but his wife comes after Red and buys ten brushes from him. Red returns to Anna and Keenan with high spirit, until he realizes he forgot to collect the payment money from Mrs Trist. When Red comes back to the Trist home, he happens to overhear a conversation between his former boss, Keenan, Gregory Cruckston (Donald Curtis) and a few other persons, as they discuss their involvement in a racketeering operation. Red is caught eavesdropping and knocked unconscious after he is brought into the house. When he comes back to life, Gordon has been murdered, and everyone present in the house is arrested by police lieutenant Quint (Arthur Space), suspected of murder. Red is released since there is no evidence pointing to him being the killer, and when he comes home he discovers Mrs Trist (Hillary Brooke) waiting for him with the money. Soon after, Anna arrives to his home, and shortly after that Freddie Trist (Ross Ford), Gordons son, with two armed gangsters. The gangsters hold everyone hostage as they search for the murder weapon that killed Gordon. Ann and Red concludes that the weapon must have been a Fuller brush, molded into a knife-looking object. Cruckston stops them from telling policeman Quint about the weapon, and it turns out Cruckston, who is Gordons partner in crime, is the murderer. Ann and Red escape from him and his gangsters. Cruckston is arrested and Red is the hero of the day, winning Anns heart in the process. 

==Cast==
*Red Skelton as Red Jones
*Janet Blair as Ann Elliott Don McGuire as Keenan Wallick
*Hillary Brooke as Mildred Trist
*Adele Jergens as Miss Sharmley
*Ross Ford as Freddie Trist
*Trudy Marshall as Sara Franzen
*Nicholas Joy as Commissioner Gordon Trist
*Donald Curtis as Gregory Cruckston
*Arthur Space as Lieutenant Quint

==Production==
The project had been in development for four years. Producer Simon got permission from the Fuller Brush company and wrote the story with Skelton in mind but was unable to secure studio interest until the success of Miracle on 34th Street (1947) showed the benefits of commercial tie-ins for feature films. He set the project up at Columbia conditional upon MGM agreeing to loan him out. Producer Edward Small was owed a favour by MGM as he agreed not to make a film called DArtagnan to clash with their production of The Three Musketeers (1948). Small and Simon then purchased a story in the Saturday Evening Post by Roy Huggins. Fuller Brush gave their final approval provided it was clear in the final movie that the character Skelton played was an independent dealer and not an employee of the Fuller Brush company. ROUND THE HOLLYWOOD STUDIOS: United Artists Buys RKO Pictures to Bolster Program -- A Bit Of Horatio Alger in Wonderland -- Libel Suit -- Other Items
By THOMAS F. BRADY. New York Times (1923-Current file)   05 Oct 1947: X5. 

==See Also==
* The Fuller Brush Girl

==References==
 

==External links==
* 
*  
*  
 

 

 
 
 
 
 
 
 