Varnapakittu
{{Infobox film
| name           = Varnapakittu
| image          = Varnapakittu.jpg
| image_size     =
| alt            =
| caption        =
| director       = I. V. Sasi
| producer       = Jokuttan
| writer         = Babu Janardanan  Jokuttan
| narrator       = Meena  Divya Unni  Vidyasagar
                   Gireesh Puthenchery (lyrics)
| cinematography = V. Aravind
| editing        = K. Narayanan
| studio         =
| distributor    =
| released       = 4 April 1997
| runtime        =
| country        = India
| language       = Malayalam
| budget         =2 crores
| gross          =5 crores
| preceded_by    =
| followed_by    =
}}
 Meena and Divya Unni, Jagadish, Madhu (actor)|Madhu, Dileep (actor)|Dileep, Rajan P. Dev, M. G. Soman are in major supporting roles .

==Synopsis==
Sunny Palomattam ( ) framed Sunnys father Ittichan (Madhu (actor)|Madhu) in a fraud case. Shortly after, Ittichan died of a heart attack, and Sunnys engagement with fiancée Nancy (Divya Unni) is called off. Sunny flees to Mumbai, and then to Singapore with Kuruvillas help. Later, Sunny learns that Sandra, who he plans to marry, is actually a call girl hired by Mohammad Ali, to spy on Sunny and foil his business plans.

To fulfill his marriage commitment to his family, he and Sandra pretend they are still engaged when they go to visit Sunnys family in Kerala. In his hometown, he meets the old enemies who falsely accused his father of theft and thus ruined the family reputation. Sunny plans to take revenge on Pappan, Tonychan (Ganesh Kumar) and company, aided by Paily (Jagadish). In the meantime, he meets Nancy who is now married to Paulachan (Dileep (Malayalam actor)|Dileep), Tony Chans younger brother. When Tony tries to sexually assault Nancy, Paulachan is humiliated and commits suicide. Mohammad Ali arrives with a gang from Singapore to reveal Sunnys "false marriage" ploy in an attempt to destroy him. Sunny pleads with Mohammad Ali to forget everything so that he can live a happy life and offers all his assets to Mohammad Ali, but Mohammad Ali has a change of heart and leaves Sunny and his family alone.

In the end Sunny and Sandra appear with a happy family.

==Cast==
*Mohanlal ... Sunny Palamatom Meena ... Sandra / Alina
*Divya Unni ... Nancy Dileep ... Paulachan
*Jagadish ... Paily Madhu ... Ittichan
*Rajan P. Dev ... Pappan Ganesh Kumar ... Tonichen Janardanan ... Ramaswamy Iyer
*M. G. Soman ... Kuruvilla Raghu ... Damodaran Nair
*N. F. Varghese ... Priest
* Sadiq... Kunjoonju Bharathi
*Rashmi Soman ... Mollykutty
* Kundara Johny ... Advocate
*Usha
*Kanakalatha
*Seetha ... Sukanya
*Maya ... TV Reporter

==Box office==

Varnapakittu was sleeper hit in the year 1997

== Sound Track == Vidyasagar and Lyrics by Gireesh Puthenchery.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Anupamasneha Chaithanyame"
| K. S. Chitra
|-
| 2
| "Manikyakallal"
| M. G. Sreekumar, Swarnalatha
|-
| 3
| "Okela Okela"
| M. G. Sreekumar, Sujatha Mohan
|-
| 4
| "Aakaashangalil"
| K. S. Chitra
|-
| 5
| "Vellinila Thullikalo"
| M. G. Sreekumar, K. S. Chitra
|-
| 6
| "Doore Mamarakkombil"
| K. S. Chitra
|-
| 7
| "Doore Mamarakkombil"
| M. G. Sreekumar
|}

==External links==
*  
* http://en.msidb.org/m.php?2196
* http://popcorn.oneindia.in/title/6392/varnapakittu.html

 

 
 
 
 
 


 
 