Bigger Than the Sky
{{Infobox film
  | name     = Bigger Than the Sky
  | caption  = Movie poster for Bigger Than the Sky
  | image	=	Bigger Than the Sky FilmPoster.jpeg
| director = Al Corley
  | producer = Craig Borden
  | writer   = Rodney Patrick Vaccaro Marcus Thomas John Corbett Amy Smart Sean Astin Clare Higgins
  | music    = Rob Cairns
  | cinematography = Carl Nilsson
  | editing  = Axel Hubert
  | studio = Coquette Productions MGM
  | released = February 18, 2005
  | runtime  = 106 min
  | language = English
  | budget   = $750,000
  }}

Bigger Than the Sky is a 2005 film directed by Al Corley.

==Plot==
 Cyrano de Bergerac". Despite the fact that Peter has no experience or skill as an actor, the director casts Peter in the lead role. Peter soon becomes caught up in the various intrigues of the "theater people", including the charming but mercurial Michael Degan, the beautiful leading lady Grace Hargrove, and a cast of other eccentric players. Gradually, Peter discovers that in the world of theater the normal rules do not apply &ndash; but in the end there is a role for everyone.

==Cast==
 
* Marcus Thomas as Peter Rooker / Cyrano  
* John Corbett as Michael Degan / Christian
* Amy Smart as Grace Hargrove / Roxanne
* Sean Astin as Ken Zorbell
* Clare Higgins as Edwina Walters
* Patty Duke as Mrs. Keene / Earlene
* Allan Corduner as Kippy Newberg
* J.W. Crawford as Kirk
* Victor Morris as Steve
* Brian Urspringer as Scott
* Kenny Jones as Ted (as Kenneth Jones)
* Orianna Herrman as Susan
* Pam Mahon as Julie
* Ernie Garrett as Paul Fisher
* Matt Salinger as Mal Gunn
* Nurmi Husa as David Nicolette
* Greg Germann as Roger
* Shea Curry as Mary Anne
* Nicholas Forbes as Andrew
* Michael Teufel as Male Sewer
* Michael Mendelson as Actor
* Al Corley as Guy in Line

==References==
 
 

==External links==
* 

 
 
 
 
 
 


 