The Hooping Life
{{Infobox film
| name           = The Hooping Life
| image          = TheHoopingLIFE PosterjpgWiki.jpg
| alt            = 
| caption        = Film poster
| director       = Amy Goldstein
| producer       = Amy Goldstein Anouchka van Riel
| writer         =  Karis Groovehoops John Savage Garry Marshall 
| music          = 
| cinematography = Amy Goldstein Gina DeGirolamo Dean Chapman The featured hoopers
| editing        = Dawn Hoggatt Hollier
| studio         = 
| distributor    = 
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Hooping Life is a 2010 documentary film directed by Amy Goldstein.   The film had its world premiere in April 2010 at the Sarasota Film Festival,  and focuses on the history of hooping.    

==Synopsis==
The film takes a look at the subculture of hula hooping, which was primarily spearheaded by women. Narrated by Shaquille O’Neal, the documentary follows several people who participate in the subculture, incorporating it into their daily lives in various different ways.

==Cast==
*Shaquille O’Neal as Narrator
*Hoopalicious
*Baxter
*Hoopgirl
*Tisha
*Jeffrey
*Karis
*Groovehoops
*Sass

==References==
 

== External links ==
*  
*  
*   at the International Documentary Association

 
 
 
 

 