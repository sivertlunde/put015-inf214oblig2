Dinocroc
 
{{Infobox film
| name           = Dinocroc
| image          = Dinocroc-dvd.jpg
| caption        = DVD Cover
| director       = Kevin ONeill
| producer       = Roger Corman
| writer         = Dan Acre   Frances Doel   John Huckert|
| starring       = Costas Mandylor Matthew Borlenghi Bruce Weitz Jane Longenecker Joanna Pacula Jake Thomas
| music          = Damon Ebner
| cinematography = Yoram Astrakhan
| editing        = Damian Akhavi Ariel Frajnd John Huckert Vikram Kale
| studio         = New Concorde International
| distributor    = New Concorde Home Entertainment
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
}}
 Charles Napier and Joanna Pacuła, produced by Roger Corman, and directed by Kevin ONeill. It had a limited theatrical release in early 2004 before premiering on the Syfy Channel in April of that year.  A prehistoric dinosaur, known as the Suchomimus, is genetically engineered by the GERECO Corporation, headed by Paula Kennedy (Joanna Pacuła). After being spliced with a modern day crocodile, the creature escapes the lab and begins terrorizing the lake-side residents of a nearby town. it was followed by 2 sequels Supergator and Dinocroc vs. Supergator

==Plot==
 
A North African dinosaur, related to the crocodile, is found which could grow up to fifty feet long.  Dr. Campbell (Bruce Weitz) uses its DNA to create two hybrids of it with a modern day crocodile at Paula Kennedys Genetic Research Co. (Gereco) lab. One creature kills Dr. Campbells assistant and the other creature before escaping. This information is kept from Sheriff Harper (Napier) by Kennedy, stating the dead creature killed Campbells assistant. His daughter, county dog catcher Diane Harper, helps her ex welding artist, Tom Banning, and his 12 year old brother Michael (Jake Thomas) find their three legged dog, Lucky, who was lost a few days earlier.

Meanwhile Kennedy sends a trapper to feed Dinocroc (the animal still being on Gereco Property). The trapper uses Lucky as bait, but Lucky runs away and Dinocroc quickly devours the trapper soon after. Later in the morning Diane and Tom find Lucky running around in the woods and try to catch him unaware that Dinocroc is lurking nearby, but Dr. Campbell saves them by shooting at it. Kennedy then hires an Australian crocodile hunter, Dick Sydney (Costas Mandylor), to help kill the Dinocroc. Later that night, Michael sneaks out to look for Lucky when he comes face to face with the creature. Dinocroc chases Michael through the forest into a tool shed sitting above water. The Dinocroc then gets under the shed and engulfs Michael from below, leaving only his head.

The next day, not having noticed that Michael has gone, Tom, Diane, Dick, and Campbell find that the creature is headed toward the towns lakeside beach. It kills 3 people, the last one being Campbell. In a press conference after the incident Kennedy lies that Campbell was not part of Gereco. Sheriff Harper then plans to kill the creature with his police force and Diane. While looking for it, they stumble upon Michaels damaged bike and Michaels remains in the shed. Tom, who knows Michael is missing, appears on his motorbike, then speeds away after seeing what is left of his dead brother. After trying to get drunk, Tom cries loudly over his brother and Diane comes to comfort him along with Lucky. Meanwhile, 5 of Sheriff Harpers officers are killed by the Dinocroc.

The next day, Tom, Diane and others devise a plan to trap Dinocroc in a tunnel and gas him to death. Sheriff Harper uses some dogs for bait, which Diane and Tom object to, so Harper has them handcuffed and put in the police car. The two escape and use a blowtorch to release the chained dogs, while the creature chases them. They trap Dinocroc in the tunnel and gas it, seemingly killing it.

While a local news crew is taping Kennedy (who arrived after the creatures death) inside the tunnel, telling the reporter false stories about the events, Dinocroc awakens, eats her whole and comes after Tom and Diane, who are left after the news crew drives away quickly. After hiding under a truck they hear a train and lure Dinocroc across the tracks. It is rammed by a passing train, followed by Tom stabbing it in the eye with a small pipe as revenge for Michaels death. As the sun rises the next day, Diane and Tom drive away, contemplating leaving for a vacation together. Then the camera pans slowly back as their truck passes and Dinocroc is seen walking weakly across the road still alive.

== Cast ==
* Costas Mandylor as Dick Sydney Charles Napier as Sheriff Harper
* Bruce Weitz as Dr. Campbell
* Matthew Borlenghi as Tom Banning (as Matt Borlenghi)
* Jane Longenecker as  Diane Harper
* Max Perlich as Deputy Kerrigan
* Jake Thomas as Michael Banning
* Kerri Hemmington as Annie Dexter
* Price Carson as Edwin Danders
* Joanna Pacula as Paula Kennedy
* Jamie Akhavi as  Judith

== Music ==
Composer Damon Ebner created background music for the creature, similar to that in Jaws. Every time the creature is near, attacks, or chases a victim, the background music, which was made up of orchestral and choir, accompanys it.

==Sequels==
 
After Corman produced Dinocroc in 2004, he proposed a sequel to be named "Dinocroc 2".  However, Sci-Fi Channel turned down the project after claiming that sequels did not do well for them.     In 2007, Corman decided to go ahead with the project, but under the name Supergator.    
 John Callahan. 

== Reception ==
The film has received poor reviews from critics.Dinocroc currently has a 3.0 out of 10 rating on IMDB.

== Awards ==
It received a Golden Galaxy Award from the American Science Fiction Society.

==Censorship==
It was censored due to the brutal death of one child in the movie. Only on DVD do you actually see Michaels death.

==References==
 

==External links==
*  

==MPAA Rating==
The MPAA rated Dinocroc R for some Creature violence, Language and Gore.

 
 
 
 
 
 
 
 
 
 
 
 