Vibrations (film)
{{Infobox film
| name     = Vibrations
| image    = 
| caption  = DVD cover
| director = Michael Paseornek
| producer = John Dunning Dan Lieberstein André Link Tami Lynn
| writer   = Michael Paseornek James Marshall Scott Cohen Bruce Altman Steven Keats Shane Butterworth Dennis Ostermaier
| music    = Bob Christianson
| cinematography = Michael Slovis
| editing  = Pamela Scott Arnold
| released =  
| runtime  = 104 minutes
| country  = United States English
}}

Vibrations is a 1996 film directed and written by Michael Paseornek. It is one of the first films to be released on DVD after the movie Twister, also from 1996.

Upon release Vibrations had initial success due to its gritty realism and uncompromising (and unconventional) character development. But Vibrations gained a loyal underground following and quickly became a cult classic, helping propel electronic music into the 21st century.

Vibrations follows the true story of TJ Cray, an up and coming Rock star in the making. On his way to the big city to audition for an A & R man and secure a record deal, his car is hit by a car load of drunks who then use heavy machinery to sever his hands. With his hands gone TJ falls out of the music business and becomes a homeless drunk with plastic hands. TJ Cray believes all is lost until one night he is awoken in the dirty warehouse he sleeps in by a pulsating rhythmic beat. Theres an illegal rave party in the warehouse and TJ is found by Anamika, a computer artist, who takes him outside for fresh air. They become good friends after TJs plastic hands help him stop Anamika getting raped by knife wielding rave thugs. Anamika introduces TJ to her friends, including Geek, who replace TJs plastic hands with metal robot piano playing hands. Eventually TJ has a metallic cyber suit made for him and he pioneers electronic music, becoming an overnight sensation known as Cyberstorm.



==External links==
*  
*  

 
 
 
 

 