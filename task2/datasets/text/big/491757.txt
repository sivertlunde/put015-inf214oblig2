Doom (film)
{{Infobox film
| name = Doom
| image = Doom movie poster.jpg
| caption = Film poster
| director = Andrzej Bartkowiak
| producer = {{Plain list |
*Lorenzo di Bonaventura
*Laura Holstein
*John D. Schofield
*Jeremy Steckler
}}
| screenplay = {{Plain list |
*David Callaham
*Wesley Strick
}}
| based on =  
| starring = {{Plain list |
*Karl Urban Dwayne "The Rock" Johnson
*Rosamund Pike
*Raz Adoti
}}
| music = Clint Mansell
| cinematography = Tony Pierce-Roberts
| editing = Derek Brechin
| studio = {{Plain list | John Wells Productions Di Bonaventura Pictures
}} Universal Pictures
| released =   
| runtime = 100 minutes
| country = {{Plain list |
*Czech Republic
*Germany
*United Kingdom
*United States  
}}
| language = English
| budget = $60 million   
| gross = $55,987,321 
}} science fiction video game series of the same name created by id Software. The film follows a group of Marines in a Research Facility on Mars – initially arriving on a rescue and retrieval mission after communications ceased, the Marines soon battle genetically engineered monsters plaguing the facility.
 Universal Pictures greenlit within 12 months.  Warner Bros. lost the rights, which were subsequently given back to Universal Pictures who started production in 2004. The film was an international co-production of the United States, United Kingdom, Czech Republic, and Germany. John Wells, he stated that a second film would be put into production if the first was a success at the box office.  Ticket sales for the opening weekend totaled more than United States dollar|US$15.3 million, but dropped to $4.2 million in its second weekend.

==Plot==
In the year 2046, a research facility on Mars is suddenly attacked by an unknown assailant. Following a distress call sent by Dr. Todd Carmack, a group of marines, led by Asher "Sarge" Mahonin, is sent on a search-and-rescue mission. One of the Marines, John "Reaper" Grimm, accompanies his sister, Dr. Samantha Grimm, to one of the labs within the devastated sector to retrieve data; here he learns that the dig site where their parents were accidentally killed was re-opened and bones of an ancient group of genetically enhanced race were discovered. 

While searching for survivors, the Marines come across one, Dr. Carmack, who is taken to a medical room for examination. The Marines then come across a creature that leads them down to the facilitys sewer. One of the Marines, Eric "Goat" Fantom, is killed during their pursuit, along with the creature. Goat and the creature are taken to the medical room. One of the Marines, Gregory "Duke" Schofield stays behind with Sam and they are later attacked by Dr. Carmack, who transforms into a creature. After trapping him, Sam conducts an autopsy on the first creature that reveals its organs are human. Goat suddenly comes back to life. He knows hes turning into a creature and kills himself.

The Marines track another creature down into the dig site and it kills three Marines. Sam and Reaper try to convince Sarge that the creatures are humans, mutated by the addition of a Martian chromosome (called C24) found and synthesized from the bones discovered, and that not all those infected will transform. Regardless, Sarge orders the Marines to kill everything in the facility. Sarge kills the mutated Dr. Carmack and murders one of his Marines for defying his commands. Sam and the surviving Marines are then flanked by the infected. Only Sam and a wounded Reaper can escape. In order to rescue Reaper Sam injects him the C24 serum, which enhances his abilities, and he is able to kill the mutated creatures and the infected. Reaper then battles an infected Sarge and kills him. Having survived, Sam and Reaper leave the facility.

==Cast==
 
* Karl Urban as Staff Sergeant John "Reaper" Grimm Dwayne "The Rock" Johnson as Gunnery Sergeant Asher "Sarge" Mahonin
* Rosamund Pike as Dr. Samantha Grimm
* Deobia Oparei as Sergeant Roark "Destroyer" Gannon
* Ben Daniels as Corporal Eric "Goat" Fantom
* Raz Adoti as Sergeant Gregory "Duke" Schofield
* Richard Brake as Corporal Dean Portman Private Mark "The Kid" Dantalian
* Dexter Fletcher as Marcus "Pinky" Pinzerowski
* Brian Steele as Hell Knight/Curtis Stahl
* Yao Chin as Private First Class Katsuhiko "Mac" Takahashi Robert Russell as Dr. Todd Carmack
* Daniel York as Lieutenant Hunegs
* Ian Hughes as Sanford Crosby
* Sara Houghton as Dr. Jenna Willits
* Vladislav Dyntera as Dr. Steve Willits Doug Jones as Carmack Imp and Sewer Imp

==Production==
 
The films producer, John Wells, admitted in an interview that "many" video game movie adaptations had "sucked." He revealed that the crew was able to get "a lot of financial support from Universal" and that it wasnt "done on the cheap." Wells also revealed that the Doom movie would have a sequence shot in a first-person perspective because "Doom without that would be a miscarriage of justice!"

Wells also revealed that "we were all very concerned that we make sure that it was exactly the kind of experience that we   remembered so fondly from the game: turning the lights off at midnight, cranking it up and scaring the hell out of yourself!"
 CGI and prosthetics in the Doom movie, and he, for the first time as a producer, admitted that "we didnt wanna rely on the CGI. Those effects still havent quite got to the level where you fully believe it&nbsp;&mdash; certainly not for long periods of time," and that the crew used Stan Winstons Creature Shop and that his work is only "enhanced with CGI." He also admitted that "if you rely too much on CGI it can look cheesy: it doesnt quite work. Itll get there, but its not there yet."

Wells has stated that the crew insisted that the Doom movie be made into an R-rated movie and that he didnt "think it was possible to do a PG-13 version—and thats been the mistake made by a couple of other computer game movies," and that "a lot of studios didnt want to do it. But we made a conscious decision that wed prefer not to make it any other way."

Wells also revealed that if this first Doom film is successful, a second one could be made, and that "we certainly have some ideas for the next one, if there is gonna be one. Well have to wait and see: the audience will have to tell us&nbsp;..."

One of the most noteworthy aspects of the film is a short sequence near the end of the film where the camera follows the progress of Grimm from a first-person perspective in homage to the original game. In the words of Karl Urban, the actor who plays Reaper:
:"In some ways, it makes cinematic history in that, for the first time, the audience becomes the hero of the film."
:"When we go into FPS, the audience is doing the rampage, the audience is doing the work and that is so cool. It’s insane!" 

===Production history===
* November 27, 2003&nbsp;&mdash; Computer Gaming World printed an article on their website regarding the Doom movie. It states that Warner Bros. is indeed working on the Doom movie and has placed it on the fast track. A revised script was submitted to id Software and approved; John Wells (producer of ER (TV series)|ER) and Lorenzo di Bonaventura (who introduced The Matrix to Warner Bros.) have signed on to work on the Doom movie. Concept art and storyboards have been drawn by Federico DAlessandro, who has worked on various movies, music videos, and video game covers and advertisements.
* May 15, 2004&nbsp;&mdash; the Associated Press (AP) released a news article regarding video game to movie adaptations that mentions the Doom movie.  Heres an excerpt that mentions the Doom movie: "Soon, more blockbuster game franchises, such as Halo and Doom, are expected to become the basis of movies."
* June 2, 2004&nbsp;&mdash; Variety reported that Warner Bros. has lost the rights to Doom and Universal Studios has acquired rights to Doom and Variety confirms that Doom will be based on Doom 3. 
* August 9, 2004&nbsp;&mdash; A Doom 3 article in an issue of Time Magazine mentions that Universal is set to film the Doom movie in Prague in the winter of 2004–2005.
* August 10, 2004&nbsp;&mdash; The Hollywood Reporter released an article that mentioned release dates for 8 movies and the third movie listed was the Doom movie. It states that Doom will have a wide release on August 5, 2005.
* August 15, 2004&nbsp;&mdash; The Hollywood Reporter reported that John Wells Productions is currently in pre-production for the Doom movie.
* August 18, 2004&nbsp;&mdash; a website, Box Office Prophets, made the Doom movie project their movie of the day and they list the release date for the Doom movie, August 5, 2005. The article also confirms that Universal has Doom on a production schedule of Winter 2004–2005 in Pragues Barrandov Studios.  The planned release date was mentioned as August 5, 2005. Polish director Andrzej Bartkowiak has signed on to be the director. It has also been revealed that production will start in mid-October with an October 21, 2005 release date. Also noted is that Universal Pictures is talking to The Rock regarding a role in the Doom movie.
* September 22, 2004&nbsp;&mdash; The Hollywood Reporter reported that Universal Pictures has cast Rosamund Pike opposite of Karl Urban as a scientist named Samantha. 

==Reception==
 
The review aggregator Rotten Tomatoes reported that 19% of critics gave the film positive reviews, based on 130 reviews, with the critical consensus "Sure to please fans of the video game, but lacking in plot and originality to please other moviegoers." Roger Ebert says, "Doom is like some kid came over and is using your computer and wont let you play."  Richard Roeper has also stated, "The performances are awful, the action sequences are impossible to follow, the violence is gratuitous, the lighting is bad and I have my doubts that the catering truck was even up to snuff on this project." One apparently good review came from Richard James Havis from The Hollywood Reporter, stating, "Theres so little to go wrong that those who like their entertainment mindless and violent will find little fault." In 2009, Time (magazine)|Time listed the film on their list of top ten worst video games movies. 

The response from fans of the video game was mixed. Many expressed disappointment because the film did not follow the plot of the game, as the games dealt with an invasion from hell instead of a virus, and over the movies failure to reproduce the games most essential quality: the killing of large numbers of enemies. It did well on its opening weekend, taking in $15.5 million. However, it quickly dropped in its second week in theaters and the final gross of the film was only $28.2 million domestically and almost $56 million worldwide, with a budget of $60 million. The film was nominated for a  .

===Home media===
Doom was released on DVD on February 7, 2006 and on Blu-ray Disc on February 10, 2009. 

==Soundtrack ==
The films score was composed by Clint Mansell, upon which he produced a remix of the Nine Inch Nails song "You Know What You Are?", which was used in the films ending credits. The song "Switchback (Celldweller song)|Switchback" by Celldweller was licensed to be used for marketing and media purposes, such as the theatrical trailer and TV spots.

==See also==
 
* List of films based on video games
*Galaxy of Terror
*Ghosts of Mars

==References==
 

==External links==
*  
*  
*  
*  
*  
 
 
 

 
 
   
   
   
   
 
   
 
 
   
 
 
 
 
 
 
 
 
 
   
   
 
 
 
 
 
 
 
 
   
 