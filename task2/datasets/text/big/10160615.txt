Sea Raiders
 
{{Infobox film
| name           = Sea Raiders
| image          = searaiders.jpg
| image_size     =
| caption        = Theatrical poster John Rawlins
| producer       = Henry MacRae Paul Huston
| narrator       =
| starring       = Billy Halop Huntz Hall Gabriel Dell Bernard Punsly Hal E. Chester
| music          = Milton Rosen
| cinematography = William A. Sickner
| editing        = Joseph Gluck Patrick Kelley Louis Sackin Alvin Todd
| distributor    = Universal Pictures
| released       =  
| runtime        = 12 chapters (229 min)
| country        = United States
| language       = English
| budget         =
}} Universal Serial film serial Junior G-Men silent serials Nazi attacks on American shipping.

==Plot==
The Sea Raiders, a band of foreign agents, led by Carl Tonjes and Elliott Carlton, blow up a freighter on which Billy Adams and Toby Nelson are stowaways, seeking to avoid Brack Warren, a harbor patrol officer assigned to guard a new type of torpedo boat built by Billys brother, Tom Adams. Intended targets or not, getting blown up does not set well with Billy and Toby and, together with their gang coupled with the members of the Little Tough Guys, they find the Sea Raiders island hideout, investigate the seacoast underground arsenal of these saboteurs, get blasted from the air, dragged to their doom, become victims of the storm, entombed in a tunnel and even periled by a panther before they don the uniforms of some captured Sea Raiders and board a yacht that serves as headquarters for the Raiders.

==Cast==
===The Dead End Kids and the Little Tough Guys===
* Billy Halop as Billy Adams 
* Huntz Hall as Toby Nelson
* Gabriel Dell as Bilge 
* Bernard Punsly as Butch 
* Hally Chester as Swab

===Additional cast===
* Joe Recht as Lug  William Hall as Brack Warren  John McGuire as Tom Adams 
* Mary Field as Aggie Nelson  Edward Keane as Elliott Carlton
* Marcia Ralston as Leah Carlton 
* Reed Hadley as Carl Tonjes 
* Stanley Blystone as Captain Olaf Nelson  Richard Alexander as Jenkins  Ernie Adams as Zeke, Skipper of the Mary Lou

==Production==

===Stunts===
* Bud Geary
* Eddie Parker doubling Eddie Dunn Tom Steele doubling Reed Hadley
* Dale Van Sickel
* Bud Wolfe doubling Richard Bond, Morgan Wallace & John McGuire
* Duke York doubling Huntz Hall

==Chapter titles==
# The Raider Strikes
# Flaming Torture
# The Tragic Crash
# The Raider Strikes Again
# Flames of Fury
# Blasted from the Air
# Victims of the Storm
# Dragged to Their Doom
# Battling the Sea Beast
# Periled by a Panther
# Entombed in the Tunnel
# Paying the Penalty
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 230–231
 | chapter = Filmography
 }} 

==References==
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 