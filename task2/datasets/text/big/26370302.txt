Sniper's Ridge
{{Infobox film
| name           = Snipers Ridge
| image          = Snipers ridge poster small.jpg
| image size     =
| alt            =
| caption        = Theatrical release poster
| director       = John A. Bushelman
| producer       = John A. Bushelman
| writer         = Tom Maruzzi
| narrator       =
| starring       = Jack Ging Stanley Clements
| music          = Richard LaSalle
| cinematography = Kenneth Peach
| editing        = Carl Pierson Associated Producers Inc
| distributor    = 20th Century Fox
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Snipers Ridge is a 1961 Korean War drama film directed by John A. Bushelman, starring Jack Ging and Stanley Clements. 

==Plot==
In the days before the Korean_War#Armistice_.28July_1953_.E2.80.93_November_1954.29|cease-fire, a hard-luck platoon run by Lt. Peer and the cowardly Sgt. Sweatish is under continual attack and suffers heavy casualties. The only good soldier in the platoon is Cpl. Sharack, who should have been rotated off the line long ago but was kept on the front by Capt. Tombolo. The return of Cpl. Pumphrey reveals a deeper motive for the Captains behavior - Tombolo did not try to rescue his children from a burning house and now tries to re-earn his self-respect with the lives of his men, especially Sharack whose heroism he is jealous of.

When no one in the platoon wants to mark the location of an unexploded shell, Capt. Tombolo decides to do it himself. He steps on a mine, which will explode when he steps off. Only Sharack and Sweatish can rescue him.

==Cast==
* Jack Ging as Cpl. Sharack
* Stanley Clements as Cpl. Pumphrey
* John Goddard as Capt. Tombolo
* Douglas Henderson as Sgt. Sweatish
* Gabe Castle as Lt. Peer
* Allan Marvin as Pvt. Ward
* Anton von Stralen as Bear
* Al Freeman Jr. as Medic Gwathney
* Mason Curry as David
* Henry Darrow as Pvt. Tonto
* Mark Douglas as Bo-Bo
* Thomas A. Sweet as Soldier
* Scott Randall as Soldier
* Joe Cawthon as Pvt. Owens
* George Yoshinaga as Mongolian

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 