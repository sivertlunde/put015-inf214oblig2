Rêves de poussière
{{Infobox film
| name           = Rêves de poussière
| image          = Rêves de poussière.png
| caption        = 
| director       = Laurent Salgues 
| producer       = Sahelis Productions
| writer         = Laurent Salgues
| narrator       = 
| starring       = Makena Diop, Rasmane Ouedraogo and Adama Ouédraogo
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       =  
| runtime        = 
| country        = Burkina Faso
| language       = French
| budget         = 
}}
Rêves de poussière (  or Buried Dreams) is a 2006 film by director Laurent Salgues.

==Plot==
Rêves de poussière tells of Mocktar Dicko, a Nigerien peasant, who goes to look for work in a gold mine in northeastern Burkina Faso. He hopes to forget the past in this prison, where the bars are made of dust and wind. 

==Production==

The film was the first full-length feature by Salges. 
The project quickly secured Avance sur Recettes and GAN Foundation funding, and gained further French and European funding since the director had obtained Burkina Faso citizenship through marriage. 
The film was a France-Canada co-production between Athénaïse – Sophie Salbot and Corporation ACPAV inc. – Marc Daigle. 
The associate producer was Sékou Traoré of the Burkina Faso company Sahelis Productions. {{cite web
 |url=http://www.sahelis.com/fr/references.php
 |title=Productions
 |work=Sahelis
 |accessdate=2012-03-13}} 

==Reception==

A reviewer says the "opening shot reminds you of a choreographed musical—only there is no music, only silence and the sounds of workers’ tools". {{cite web
 |url=http://moviessansfrontiers.blogspot.com/2009/01/79-french-director-laurent-salgues.html
 |date=January 18, 2009
 |title=French director Laurent Salgues’ debut film "Rêves de poussière (Dreams of Dust)" (2008): Infusing dignity and elegance to cinema on Africa
 |work=Moviewssansfrontieres
 |accessdate=2012-03-13}} 
Another critic says "Cinematographed by Crystel Fournier, images are hauntingly dreamlike. Wind-swept dust is a recurrent motif". {{cite web
 |url=http://grunes.wordpress.com/2008/06/07/dreams-of-dust-laurent-salgues-2006/
 |title=DREAMS OF DUST (Laurent Salgues, 2006)
 |author=Denis Grunes
 |accessdate=2012-03-13}} 
The film was nominated for the grand Jury Prize at the Sundance Film Festival in the World Cinema - Dramatic category. Namur 2006. {{cite web
 |url=http://www.laurentsalgues.com/reves_de_poussiere.html
 |title=Rêves de poussière
 |work=Laurent Salgues
 |accessdate=2012-03-13}} 

==References==
{{reflist |refs=
 {{cite web
 |url=http://www.africine.org/?menu=fiche&no=8054
 |title=Salgues Laurent
 |work=Africine
 |accessdate=2012-03-13}} 
 {{cite web
 |url=http://www.paff.org/home/files/pressroom/presskits/Film%20-%20Dreams%20of%20Dust.pdf
 |title=DREAMS OF DUST
 |publisher=WIDE Management
 |accessdate=2012-03-13}} 
}}

== External links ==
*  

 
 
 
 
 