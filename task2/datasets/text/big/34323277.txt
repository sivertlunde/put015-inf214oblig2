Pedababu
{{Infobox film
| name           = Pedababu
| image          = Pedababu.jpg
| image size     =
| caption        =
| director       = Paruchuri Murali
| producer       = M L Kumar Chowdary
| story          = Paruchuri Murali  
| screenplay     = Paruchuri Murali
| writer         = Ramesh-Gopi 
| starring       =   Chakri
| cinematography = S.K.A. Bhupathi
| editing        = Marthand K. Venkatesh
| studio         = Sri Keerthi Creations
| distributor    =
| released       =  
| runtime        = 2:25:17
| country        = India
| language       = Telugu
| budget         =
| gross          =
}} 2004 Telugu Kalyani in Sunil received Filmfare Award for Best Comedian – Telugu for this film. 

==Plot==
Peda Babu (Jagapati Babu) is the village head. He stays alone and away from his mother (Suhasini) who is staying in the same village along with Peda Babus father and siblings. The entire village respects Pedababu as village head. But why is Pedababu staying away from his mother. He has not spoken a word to her for the past 25 years. What is the reason? That exact flashback has the major interest part of the story.

==Cast==
{{columns-list|3|
* Jagapati Babu as Pedababu Kalyani as Neelaveni
* Suhasini as Pedababus mother
* K Viswanath as Pedababus grand father
* Kota Srinivasa Rao as Kanaka Raju Sunil as Bapineedu
* Sarath Babu as Pedababus step-father
* Vijayachander Ponnambalam as Rudra Raju Ajay as Pedababus younger brother
* Bharath as Pedababus younger brother
* Prabhu Rallapalli as Raghavaiah
* Devadas Kanakala as Registrar
* Vizag Prasad
* Lakshmipati as Barber
* Bosu
* Govindu
* Sandeep
* Fish Venkat as Kanaka Rajus henchman Jenny as Teacher
* Harika
* Shobha
* Sakhi
* Uma
* Master Deepak Saroj as Young Pedababu
}}

==Soundtrack==
{{Infobox album
| Name        = Pedababu 
| Tagline     = 
| Type        = film Chakri
| Cover       = 
| Released    = 2004
| Recorded    = 
| Genre       = Soundtrack
| Length      = 23:59
| Label       = SOHAN Audio Chakri
| Reviews     =
| Last album  = 143 (film)|143   (2004)
| This album  = Pedababu   (2004)
| Next album  = Andhrawala   (2004)
}}

Music composed by Chakri (music director)|Chakri. All songs are hit tracks. Music released on SOHAN Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 23:59
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Oka Vekuva Jaladi
| SP Balu
| length1 = 5:12

| title2  = Bava Bava
| lyrics2 = Jaladi
| extra2  = Ravi Varma,Sunitha Balaji
| length2 = 4:24

| title3  = Navvavayya Babu
| lyrics3 = Bhaskarabhatla Ravikumar Kousalya
| length3 = 4:43

| title4  = Naalo Nuvvundali
| lyrics4 = Bhaskarabhatla Ravikumar
| extra4  = SP Balu,Kousalya 
| length4 = 4:18

| title5  = Oka Devudu
| lyrics5 = Jaladi
| extra5  = SP Balu
| length5 = 0:52
  
| title6  = Palluna Virigindiro
| lyrics6 = Jaladi
| extra6  = Sandeep Bhaumik,Kousalya
| length6 = 4:16
}}
 

==Awards==
{| class="wikitable" style="font-size: 95%;"
|-  style="background:#ccc; text-align:center;"
! Award
! Category
! Recipients and nominees
! Outcome
|- Filmfare Awards
| Filmfare Award for Best Comedian&nbsp;– Telugu Sunil
|  
|}

==References==
 

==External links==
*  

 
 
 


 