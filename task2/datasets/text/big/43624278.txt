Beyond Victory
 
{{Infobox film
| name           = Beyond Victory
| image          = 
| alt            = 
| caption        = 
| film name      =  John Robertson   
| producer       =  E. B. Derr   
| writer         = Horace Jackson  James Gleason 
| screenplay     = 
| story          = 
| based on       =   Bill Boyd James Gleason Lew Cody ZaSu Pitts
| narrator       = 
| music          = Francis Groman 
| cinematography = Norbert Brodine
| editing        = Daniel Mandell RKO Pathé
| distributor    = 
| released       =   }}
| runtime        = 70 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Bill Boyd, John Robertson received directing credit, Edward H. Griffith supposedly took extensive re-takes after production ended and the filmed was deemed not audience ready.  Two actresses which had major roles in the original version, were completely cut from the final film, Helen Twelvetrees and June Collyer.  The original screenplay was written by Horace Jackson and James Gleason.  While the film might not have made a profit at the box office, it was well-received by critics.

==Plot==
On the western front during World War I, American forces attempt to retain control of a French town. Sergeant Bill Thatcher (Bill Boyd), the non-commissioned officer in charge of the forces, goes to visit several wounded men.  The first, "Bud" (Russell Gleason) relates his story of how he joined the military, leaving the family farm to enlist, despite his mothers pleas for him not to go get involved in foreign conflicts.  As he finishes his story, he dies.

The second doughboy, Lew Cavanaugh (Lew Cody), was a New York playboy, who used enlistment as a way to have a final night of pleasure with one of his conquests.  He never realized that he too would die on the plains of France.  The third American soldier, Jim Mobley (James Gleason) is nowhere near as badly wounded as the other two soldiers.  He tells his story of his theatrical wifes displeasure when he announces his intention to enlist.  Thatcher then relates his own story, where he was engaged to a German immigrant back in the United States, but did not wed her due to their differences regarding the "Great War".  

Shortly after their discussion, the two men are both thrown back into the battle, during which they hold the American positions, but are badly wounded and captured by German forces.  While in the German hospital, Thatcher discovers that one of the nurses is his erstwhile fiancé, Katherine (Lissi Arna).  When armistice is declared, the two are married, with Jim looking on.

==Cast== Bill Boyd as Sergeant Bill Thatcher
* James Gleason as Private Jim Mobley
* Lew Cody as Lew Cavanaugh
* ZaSu Pitts as Mademoiselle Fritzi
* Marion Shilling as Ina
* Russell Gleason as Russell ("Bud")
* Lissi Arna as Katherine
* Mary Carr as Mother Fred Scott as Fred
* Theodore Von Eltz as Major Sparks
 AFI database) 

==Reception==
The reviews of the film were mixed.  The Film Daily called it a "Fairly good war drama", highlighting the good performance of Marion Shilling.    This sentiment was echoed by the Motion Picture Magazine, which called it a "War picture to talk about...."    On the other hand, the Motion Picture Herald, calling it a "mixed-up affair, pacifist in theme, and in acting, story, direction and other departments woefully amateurish."   

==Notes==
Scenes with Helen Twelvetrees and June Collyer were completely cut from the final version of the film.    

Academy Award nominee, James Gleason, was asked to contribute to the screenplay.  In his adaptation, he wrote a role into the final version of the script for himself    

The film had a slightly nepotistic angle, as Frank Reichers sister, Hedwiga Reicher, James Gleasons son, Russell Gleason were cast in the film, Russell in a very significant role. 

==References==
 

 

 
 
 
 
 