Detective Byomkesh Bakshy!
 
 
 
{{Infobox film
| name           = Detective Byomkesh Bakshy!
| image          = Detective Byomkesh Bakshi poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Dibakar Banerjee
| producer       = Aditya Chopra Dibakar Banerjee
| writer         = Sharadindu Bandyopadhyay Dibakar Banerjee
| screenplay     = Dibakar Banerjee Urmi Juvekar
| based on       = Satyanweshi (novel)|Satyanweshi, Pother Kanta and Arthamanartham by Sharadindu Bandyopadhyay
| starring       = Sushant Singh Rajput Anand Tiwari Swastika Mukherjee
| music          = Various Artists
| cinematography = Nikos Andritsakis
| editing        = Manas Mittal Namrata Rao
| studio         = Yash Raj Films Dibakar Banerjee Productions
| distributor    = Yash Raj Films
| released       =    }}
| runtime        = 148 minutes
| country        = India Hindi
| budget         =     
| gross          =   (worldwide)       
}}
 Indian Mystery mystery Thriller thriller film Bengali writer Sharadindu Bandyopadhyay.   The film stars Sushant Singh Rajput, Anand Tiwari and Swastika Mukherjee in principal roles. The film was released on 3 April 2015 to positive reviews from critics.

==Plot==
 
In 1942 Calcutta in a vicious fight at the Calcutta docks Yang Guang is thought to be killed. 

Some time later, in Calcutta, student Byomkesh Bakshy is approached by a classmate, Ajit, whose father Bhuvan Banerjee, a chemist, has been missing for more than two months. Byomkesh believes that he has been killed and his dead body kept hidden somewhere so that the case remains that of missing person. Initially, Byomkesh refuses to help in finding Ajits missing father Bhuvan babu and even suggests Bhuvan may be mixed up in dodgy business, at which Ajit angrily slaps him. Byomkeshs girlfriend Leela plans to marry an employed chemistry student instead of him and tells him on the same day. After that Byomkesh finds Ajit and agrees to take a look. He checks Ajits fathers belongings and is convinced that there is more to it than what meets the eyes. He tries to find out more by going to Bhuvans lodging home and ask the residents namely Dr. Guha (Neeraj Kabi), Ashwini Babu (Arindol Bagchi) and Kanai Dao (Meiyang Chang) who is an government licensed opium merchant. He also finds Bhuvans paan box hidden in his room by his roommate Ashwini Babu who seems to be addicted to the betel masala in it. Byomkesh deduces that since Bhuvan did not take his money and other belongings, he was not planning to go anywhere so the most likely scenario is that he must be dead.

Byomkesh finds the factory where Bhuvan worked. There he meets an actress, Angoori Devi (Swastika Mukherjee), who offers him a ride home. During the journey, Byomkesh asks her about Bhuvan, but she seems frightened and tries to hide something in her handbag, which is later found out to be blackmail letters addressed to Gajanan Sikdar, a local politician who is the owner of the factory and also a friend of Angoori.

Byomkesh deduces, with the help of Dr. Guha, that Bhuvan tried to blackmail Sikdar and was killed by him. He goes back to the factory with Dr. Guha and finds Bhuvans rotting corpse stuffed inside a machine. Sikdar is brought in by police commissioner Wilkie (Mark Bennington) for questioning. Simultaneously Byomkesh finds some clues which prove that Sikdar is being framed. He immediately goes to the police station and rectifies his mistake, where he is also detained afterwards for impersonating a police-officer previously.

Sikdar is freed and Byomkesh follows him home but is stopped by Angoori before entering Sikdars room. She takes him to another room and tries to kiss him. He gently rebuffs her but when he goes to meet Sikdar, the latter is already poisoned and dies in front of Byomkesh. Suspicion falls on his rebellious nephew Sukumar who has opened a rival political party and is militant about Indias freedom from the British. Meanwhile Dr. Guha manages to vanish but sends Byomkesh an envelope containing the post stamp, indirectly admitting he was blackmailing Sikdar, not Bhuvan Babu.

After that Sukumars sister Satyawati (Divya Menon) pleads with Byomkesh to help her brother, whos driven off earlier in a cab which Byomkesh used to follow Sikdar till his home. Byomkesh finds the Sikh cabbie who takes him to the point he dropped off Sukumar, which turns out to be a clinic of the Japanese dentist Dr. Watanabe. In the clinic the dentists secretary says Watanabe is at the temple. Byomkesh and Ajit go to the temple, pretending they have a link with Sukumars political party, but Watanabe is suspicious. Another member of Sukumars party recognises them and follows them at Watanabes orders. Byomkesh and Ajit confuses him by changing their costumes and then follow the student back to the dentists – where they find both the secretary and student brutally murdered with a shadowy character escaping in a car.

Byomkesh takes a paper out from the students pocket which they later find out is a map of Calcuttas river route. They return to the lodgers house where Byomkesh feels low. He tries some of Bhuvans betel masala, gets high, and makes a wall-painting of the entire crime. Next day he goes to get his blood tested, as well as a chemical analysis of the masala. It is revealed that his blood had heroin presence confirmed by Leelas chemist husband who says this heroin is very hard to trace and made from opium which Dao, who later is revealed to be an undercover police officer, tells Byomkesh has practically vanished from the city. Also the masala is literally clean, i.e. there is no trace of heroine in it.

Byomkesh finds there are two plots together here. The first is the Chinese gang warfare over opium, turned into untraceable heroin by Bhuvan babus secret formula. The second is of Sukumars political party being told by the Japanese army that they will land in Calcutta via its river routes and liberate the city from the British. The link between the two deadly plots is the mysterious Yang Guang. Yang Guang is presumed dead in a gang war sometime back, but in light of recent events it is understood that he is very much alive and in Calcutta itself. 

Meanwhile, Dr. Guha returns to the lodging house and tells Byomkesh hes a nationalist fighting for Indias freedom. He offers Byomkesh a role to play in the struggle and says that the murders were collateral damage. Byomkesh refuses.

Later Byomkesh asks Dao to take him to the Chinese gang which tells him about Guang. Byomkesh divulges some of the facts to commissioner Wilkie and asks him to sound all the air raid sirens of Calcutta at 4&nbsp;am on Basant Panchami, because he realises that Sukumar and his sister were mere pawns in the plot and they will come in harms way one way or another . He is convinced the Japanese will attack Calcutta through its river routes then and Guang will help them, so that they let him control the hugely rich opium trade, and turn Calcutta into the drug capital of the world.

Byomkesh sends Dr. Guha a letter inviting him to the lodging house. Dr. Guha, Sukumar, Watanabe and Satyawati come there, as does Angoori who told Byomkesh she loved Guang from their days together in Rangoon and has been trying to mislead Byomkesh on Guangs orders. She hasnt seen him recently though and doesnt recognise him – not even when Byomkesh reveals Dr. Guha is actually the dreaded Yang Guang.

Angoori tries to stop Guang from helping the Japanese conquer Calcutta but he brutally murders her. The Chinese gang hidden by Byomkesh in the lodging house then attacks and takes away Guang who got distracted by the air sirens going off. Byomkesh, playing a bluff tells the Japanese doctor their game is up and the British army will crush their forces if they try to attack. Watanabe gives up and leaves in a hurry to warn the Japanese army. Byomkesh then tells Ajit that he would like to believe Bhuvan is innocent. On realising the danger of his invention, he tries to prevent it from falling into wrong hands, and gets killed. Byomkesh then proposes marriage to Satyawati, who complies. 

In the epilogue, Guang manages to kill the entire Chinese gang and – though injured – escapes, promising Byomkesh revenge.

==Cast== Detective Byomkesh Bakshy: a young Bengali detective. He had just passed out from college, and unlike professionals, falters and often makes mistakes.
* Anand Tiwari as Ajit Banerjee: the college-mate and future sidekick of Byomkesh Bakshy. He aspires to become a writer.
*   Mata Hari.  
* Divya Menon as Satyawati: the niece of a local politician Gajanan Sikdar, and later the love interest of Byomkesh Bakshy. 
* Neeraj Kabi as Dr. Anukul Guha: a physician who also runs a male hostel. 
* Meiyang Chang as Kanai Dao, a hostel resident with Chinese roots. 
* Aryann Bhowmik as a young nationalist revolutionary.   
* Moumita Chakraborty as Leela 
* Mark Bennington as Deputy Commissioner Wilkie 
* Arindol Bagchi as Ashwini Babu, a resident of the hostel who had a penchant for betel. 
* Manoshi Nath as Ruby 
* Anindya Banerjee as Prafulla Ray, an insurance agent 
* Tirtha Mallick as Atanu Chandra Sen 
* Prasun Gayen as a Journalist 
* Peter Wong as the Underboss 
* Prashant Kumar as Factory Watchmen 
* Dr. Kaushik Ghosh as Gajanan Sikdar, the local politician. 
* Takanori Kikuchi as Dr. Watanabe, a drug lord who doubles as a dentist. 
* Piyali Ray as Watanabes Receptionist 
* Pradipto Kumar Chakraborty as Puntiram 
* Sandip Bhattacharya as Officer in Charge 
* Shivam as Sukumar, the nephew of Sikdar, he has difference of opinions with his uncle and plans to form his own political party. 
* Nishant Kumar as Factory Watchmen 
* Shaktipada Dey as Nibaran Da 
* Kanwaljeet Singh Banga as Sikh Taxi Driver
* Lauren Gottlieb (Special appearance in the song Calcutta Kiss)

==Production==

===Casting===
After Khosla Ka Ghosla Dibakar Banerjee wanted to make this film and he approached Aamir Khan to play the character of the villain, but eventually the deal was cancelled because Aamir chose Dhoom 3 over it to be a villain.  Sushant Singh Rajput was roped in to play the role of Byomkesh Bakshi in mid-2013.  Earlier reports suggested that fashion designer Sabyasachi Mukherjee|Sabyasachis assistant, Divya Menon, will be making her debut as an actress in this film as Rajputs wife.    Initially it was considered, Rani Mukherjee to play the female lead.  However, Rani refused the role since the heroine has to do some bold scenes in the film.  The role was later offered to Bengali actress Swastika Mukherjee.  In 6 November 2014, Banerjee stated that Mukherjees role in the film is sketched along the lines of Mata Hari.    In March 2014, reports suggested that Meiyang Chang will also be seen in the film in an important role.    On May 2014, it was revealed that Neeraj Kabi is also part of the film.   

===Development=== YRF and Dibakar Banerjee Productions officially announces its first directorial outing with Banerjee titled "Detective Byomkesh Bakshy!".  In July 2013, Banerjee revealed that YRF had bought the rights of "31 of Byomkesh novels in all languages outside Bengali language|Bangla". 
 VFX will be used to reflect the old world charm since the film is set in the 1940s. 


===Filming=== Presidency University, Coffee House and Bow Barracks.  The cast was reportedly seen shooting for the film in Agarpara in January 2014. 

==Music==

In a deviation from traditions, the OST has been composed entirely by independent music artists.  Early in 2014, Banerjee and his production team set out to find artists whose music would fit into the story of the film. After shortlisting about 200 songs from non-Bollywood artists across the country, the team began whittling the list down to a handful of songs and artists,   
===Soundtrack===

{{Infobox album
| Name = Detective Byomkesh Bakshy!
| Type = Soundtrack
| Artist = Various Artists
| Cover =
| Alt =
| Released =   Feature film soundtrack
| Length =   Yash Raj Music
| This album = Detective Byomkesh Bakshy! (2015)
}}
From the seven tracks on the OST, four tracks are remakes of previously written material – PCRCs "Pariquel" off their 2011 debut album Sinema, now has Hindi lyrics by frontman Suryakant Sawhney, BLEKs 2012 song "Fog + Strobe" includes thumri vocals by playback singer Usri Banerjee, Madboy/Minks "Taste Your Kiss" includes verses in Hindi from vocalist Saba Azad and Joint Familys "Lifes A Bitch," stays true to its original 2007 version from their album Hot Box. 

Amongst the artists in the album, Sneha Khanwalkar is the only musician who had composed for a soundtrack before this.  The track which was used in the teaser, titled "Lifes a Bitch", is composed by the New Delhi band Joint Family.  The full song of "Bach Ke Bakshy" was published in a music video featuring Sushant, dancing in a parking lot, on 16 March 2015 in the official YRF Youtube channel.  On the very next day, YRF has uploaded all the songs of the film in a audio jukebox format in their official channel. 
{{track listing
| headline       = Track listing
| extra_column   = Singer(s)
| lyrics_credits = yes
| music_credits = yes
| total_length =  
| title1         = Calcutta Kiss
| extra1         = Imaad Shah, Saba Azad
| music1         = Madboy/Mink
| lyrics1        = Madboy/Mink
| length1        = 3:09
| title2         = Bach Ke Bakshy
| extra2         = Gowri Jayakumar, Big Deal, Thomson Andrews, Trevor Furtado, Rap by: Smokey the Ghost, Craz Professa
| music2         = Sneha Khanwalkar, Dibakar Banerjee
| lyrics2        = Sneha Khanwalkar, Dibakar Banerjee
| length2        = 5:49
| title3         = Byomkesh in Love
| extra3         = Rishi Bradoo, Anil Bradoo, Usri Banerjee
| music3         = Blek
| lyrics3        = Rishi Bradoo
| length3        = 3:18
| title4         = Jaanam
| extra4         = Suryakant Sawhney
| music4         = Peter Cat Recording Co. (PCRC)
| lyrics4        = Suryakant Sawhney
| length4        = 3:27
| title5         = Chase in Chinatown
| extra5         = Vyshnav Balasubramaniam, Sandeep Madhavan, Manas Ullas
| music5         = Mode AKA
| lyrics5        = Sandeep Madhavan
| length5        = 3:55
| title6         = Lifes A Bitch
| extra6         = Akshay De
| music6         = Joint Family
| lyrics6        = Akshay De
| length6        = 3:35
| title7         = Yang Guang Lives
| extra7         = IJA
| music7         = IJA
| lyrics7        = IJA
| length7        = 4:29
}}

==Release==
The film released worldwide on 3 April 2015. 

===Marketing===
Banerjee says he changed the i at the end of the protagonists surname Bakshi to y for "typographical balance", stating that the i felt "too thin" and that y was a "stronger alphabet".   
 Happy New Vine and marks the first film to do so.  motion poster of the film released on 20 December, in Kolkata.   The official trailer of the film released online on 21 January 2015. The production team made a special documentary to celebrate the birthday of Howrah Bridge and did a flash mob on the bridge on the song Calcutta Kiss on that day. 
Detective Byomkesh Bakshy! team promoted the film in Mumbai College, where a fashion show is inspired by the film.   A second trailer is released on 10 March 2015.  
Sushant Singh Rajput promoted the film in India Poochega Sabse Shaana Kaun? and Comedy Nights with Kapil.   Rajput also promoted the film in the TV series C.I.D. (Indian TV series)|CID on 30 March 2015, where he appeared as Byomkesh along with Anand Tiwari as Ajit. 

====Game====
A mobile version game named Detective Byomkesh Bakshy!: The Game, has been launched by Games2win (G2W), which is currently available on the Google Play Store, it is a hidden objects game. 

The game, launched with Yash Raj Films Licensing (YRFL), the licensing division of Yash Raj Films, is based on the film, which features Sushant Singh Rajput in the lead. On completing all the levels, a Detective Certificate will be awarded to the players, who can post it on social media. 

====Fashion Line====
An apparel line titled NOIR 43 was launched at the Amazon India Fashion Week (AIFW), New Delhi, on 26 March. Since "Detective Byomkesh Bakshy!" is set against the backdrop of Kolkata during World War II in 1943, various elements from the film have influenced the fashion line of contemporary clothing. The collection will be exclusively available on Amazon.in. 

==Reception==
===Critical reception===
Detective Byomkesh Bakshy! received generally positive response from critics.  
Hindustan Times Rohit Vats gave gave it 4 stars out of five and stated "The action in the film starts taking place from a distance and the director slowly involves the audience into it. Pre-independence Calcutta serves as the backdrop in the opening scenes, but it soon turns into a character. The attention to detail is obvious in almost every frame of the film. " 
Koimoi Surabhi Redkar, gave it 4 stars out of five, and said: "Detective Byomkesh Bakshy is a delicious thriller filled with elements that make you rack your brains too. This mystery is the one even you would want to solve. Watch it for an amazing story and its even better execution. Crime mysteries just got better in Bollywood! I am going for 4/5 here!". 
  and cinematographer Nikos Andritsakis." 
Kusumita Das from Deccan Chronicle rated movie 3.5 stars out of five and noted "The film follows the core structure of whodunit --- the chase, the red herrings, the slow cooked suspense leading up to a grand reveal. Theres a generous spray of blood too, that underlines just how violent those times were". 

Srijana Mitra Das of The Times of India reviewed and gave 3.5 stars out of five and also praised "Byomkesh Bakshy is an iconic Bengali character brought to life by Sushant Singh Rajput with great elan – Sushant pulls off a role full of wry liveliness (a Sardarji cabbie nervously noting, Ye babu ka nut dheela hai,), fitting the character, from flowery dhoti folds to furrowed-forehead frowns, beautifully. Hes matched by dramatic Neeraj Kabi and calm Anand Tiwari who, after a Chinese gang leaves a courtyard strewn with corpses, tells caretaker Putiram (shakily precise Pradipto Kumar Chakrabarty), Khoon rehne de...bas chai bana de." 
Indian Express, Shubhra Gupta gave it 3 stars out of five and mentioned "When it switches to explicatory mode, it flattens. The tension, which is on a slow-burn, leaches out, and the film ends as less than it could have been. And thats a disappointment. But by then, you have seen a film, a real, bonafide film, not bits and pieces of nonsense masquerading as one.". 
IBN Live gave it 3 stars out of five and mentioned "ts the snail-paced plotting, and the surprising lack of urgency and imminent danger that cripples the film. Story strands and characters are abandoned arbitrarily, only to be revisited later. The big reveal isnt too hard to guess – stick with your gut, dont let the red herrings distract you, and lo, youve figured it out. The climax too is a mess of hammy acting".  Deepanjana Pal of Firstpost gave it eight out of ten. 

===Box office===
On its first day the film collected   nett in India.  On its second and third day the film collected   and   respectively, and made a total weekend collection of   in India.  As of 16 April, the film collected   in India. 

As of 12 April, the film collected   from international markets.  

==Sequel==
Before the films release, Dibakar Banerjee expressed interest to make Byomkesh Bakshy as a franchise, if the first film does well.      
After the release of the film Banerjee revealed that he already has the sequel ready. He said: "Much before the film released, I had already started working on the second film to Byomkesh. God willing, we will start with it soon."  
Banerjee said that Byomkeshs adventures will continue with the upcoming instalments of the series exploring his relationship with his sidekick Ajit, his love interest Satyawati and his arch-nemesis. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 