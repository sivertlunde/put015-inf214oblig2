Pappan Priyappetta Pappan
{{Infobox film
| name           = Pappan Priyappetta Pappan
| image          = Pappan Priyappetta Pappan.jpg
| image_size     =
| caption        =
| writer         = Siddique-Lal
| screenplay     = Siddique-Lal
| director       = Sathyan Anthikkad
| producer       = G.S. Harindran
| narrator       = Rahman Mohanlal Thilakan Sankaradi
| music          = Alleppey Ranganath Rajamani (assistant)
| cinematography = Anandakuttan
| editing        = M.N. Appu
| distributor    = Dinny Films
| released       =  
| runtime        = 120 minutes
| country        = India
| language       = Malayalam
| budget         =
| website        =
}}
 Heaven Can Wait (1978). This was the first film of Harisree Ashokan.This is Mohanlals 100th film.

==Plot== Yamaraj the Lord of Death (Thilakan) realizes that Pappan has a few more days to live. Yamarajan  allows Pappan to enter into bodies of other dead people including that of Inspector Devdas (Mohanlal) in a bid to tell his girlfriend Sarina (Lizy (actress)|Lizy) that Pappan is dead, and to look forward to someone else in life.

==Cast== Rahman  as  Pappan aka Padmakumar
* Mohanlal as Inspector Devdas Jose as Umesh Lizy as Sarina Bahadur as Pappan aka Padmanaban
* Nedumudi Venu as SI Kochappu
* Thilakan as Yamarajan
* Unnimary as Devdass wife
* Rajan P. Dev as Puli Sankaran
* Mala Aravindan as Padmanabans nephew
* N. F. Varghese as Padmanabans nephew Sainuddin as Padmanabans nephew Innocent as Constable Kuttan
* Sankaradi as a Rich man
* Harisree Ashokan as a man in Padmanabans house (cameo)
* Jose Prakash as Chandu Uncle Priya
* Thilakan as kaalan

==Soundtrack==
The music was composed by Alleppey Ranganath and lyrics was written by RK Damodaran and Sathyan Anthikkad.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || "Allithaamara" || P Jayachandran || RK Damodaran ||
|-
| 2 || "Kaalanillakkaalathoru Thalla" || Balagopalan Thampi || RK Damodaran ||
|-
| 3 || "Puthan Manavaatti" || Chorus, Jency, Sindhu || RK Damodaran ||
|-
| 4 || "Shaarike Ennomalppainkilee" || P Jayachandran || Sathyan Anthikkad ||
|}

==Trivia==

Pappan Priyappetta Pappan was distributed by Dinny Films and was a box office hit.

==References==
 

==External links==
*  
 
 
 
 
 
 
 