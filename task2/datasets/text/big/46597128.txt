Ramona (1946 film)
 

{{Infobox film
| name =  Ramona
| image =
| image_size =
| caption =
| director = Víctor Urruchúa
| producer = Enrique Darszon
| writer =  Helen Hunt Jackson (novel)   Leopoldo Baeza y Aceves   Tito Davison
| narrator =
| starring = Esther Fernandez
| music = Manuel Esperón
| cinematography = Jack Draper
| editing = Carlos Savage
| studio = Promex
| distributor = 
| released =    11 July 1946 
| runtime = 93 minutes
| country = Mexico Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Ramona is a 1946 Mexican drama film directed by Víctor Urruchúa and starring Esther Fernandez. It is an adaptation of the 1884 American novel Ramona by Helen Hunt Jackson. The film was both a financial and critical failure.  The films sets were designed by the art director Luis Moya.

==Cast==
* Antonio Badú 
* Juan Calvo 
* Esther Fernandez 
* Rafael Icardo 
* Cuquita Martinez 
* Carlos Navarro 
* Bernardo Sancristóbal 
* Fanny Schiller

== References ==
 

== Bibliography ==
* Ilan Stavans (ed.) Border Culture. ABC-CLIO, 2010.

== External links ==
*  

 
 
 
 
 
 
 


 