There's a Girl in My Soup
 
 
{{Infobox film
| name           = Theres a Girl in My Soup
| image          = Theres_A_Girl_in_My_Soup_Poster.jpg 
| image_size     = 170px
| caption        = Original Film Poster
| director       = Roy Boulting
| producer       = John Boulting Mike J. Frankovich
| writer         = Terence Frisby (play & screenplay) Peter Kortner (addl dialogue)
| starring       = Peter Sellers Goldie Hawn
| music          = Mike DAbo
| cinematography = Harry Waxman
| editing        = Martin Charles
| distributor    = Columbia Pictures
| released       = 15 December 1970 (US) 21 December (London premiere)
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English, French
| budget         = 
| gross          =
}}
 Theres a Girl in My Soup is a 1970 British comedy film, directed by Roy Boulting and starring Peter Sellers and Goldie Hawn.

==Plot==
Sellers appears as Robert Danvers, a vain, womanizing and wealthy host of a high-profile cooking show. He meets Hawns character, a no-nonsense American hippie living with an English rock musician in London, and, to everyones surprise, falls for her.

She moves in with him, and accompanies him on a trip to a wine festival in France. Meanwhile, her rock musician boyfriend decides he wants her back.

Sellers characters catchphrase is: "My God, but youre lovely"—which he sometimes says to his own reflection.

==Production and accolades== West End, Globe Theatre (now The Gielgud) breaking records to become Londons longest-ever running comedy. This record was later broken by No Sex Please, Were British and then Run For Your Wife.

Frisbys script won The Writers Guild of Great Britain Award for Best Screenplay in 1970. The movie introduced Christopher Cazenove, who later co-starred on Dynasty (TV series)|Dynasty and the British TV series The Duchess of Duke Street, and Nicola Pagett, who played Elizabeth Bellamy on Upstairs, Downstairs (1971 TV series)|Upstairs, Downstairs.
 Raymond Hitchcock, was published in 1972.

==Cast==
*Peter Sellers as Robert Danvers
*Goldie Hawn as Marion
*Tony Britton as Andrew Hunter
*Nicky Henson as Jimmy
*Diana Dors as Johns wife
*Judy Campbell as Lady Heather
*John Comer as John, the porter
*Gabrielle Drake as Julia
*Nicola Pagett as Clare
*Geraldine Sherman as Caroline
*Thorley Walters as Manager of the Carlton Hotel
*Ruth Trouncer as Gilly Hunter
*Françoise Pascal as Paola
*Christopher Cazenove as Nigel
*Raf De La Torre as Monsieur Le Guestier

==Production==
Film rights were bought in 1967 by Columbia and Nat Cohen. MOVIE CALL SHEET: Garas Loaned to Paramount
Martin, Betty Los Angeles Times (1923-Current File); Nov 19, 1966; ProQuest Historical Newspapers: Los Angeles Times (1881-1990) pg. 23  Eventually Mike Frankovich became producer and the Boultings directed.

Goldie Hawn signed in February 1970. MOVIE CALL SHEET: Soup Next for Goldie Hawn
Martin, Betty. Los Angeles Times (1923-Current File)   04 Feb 1970: d13.  

==Reception==
It was the seventh most popular movie at the British box office in 1970. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 

 