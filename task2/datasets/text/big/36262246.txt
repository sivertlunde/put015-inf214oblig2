One More Try (film)
{{Infobox film
| name         = One More Try
| image        = One More Try (2012).jpg
| caption      = Theatrical movie poster
| director     = Ruel S. Bayani
| starring     = Angel Locsin Angelica Panganiban Dingdong Dantes Zanjoe Marudo
| studio       = Star Cinema
| distributor  = Star Cinema
| released     =  
| runtime      = 95 minutes
| country      = Philippines
| song         = Without You by Angeline Quinto Tagalog English English
| budget       = 15,000,000
| gross        = ₱170.5 Million  (as of January 8, 2013 - MMFF season)  ₱213 Million  (4 weeks) 
}}

One More Try is a 2012 Filipino drama film  starring Angel Locsin, Angelica Panganiban, Dingdong Dantes and Zanjoe Marudo .  {{cite web|url=http://www.push.com.ph/features/7849/dingdong-dantes-says-he-will-play-his-most-daring-role-in-one-more-try/
 |title= Dingdong Dantes plays most daring role in One More Try: |accessdate=2012-06-27}}  The film was one of the eight official entries to the 2012 Metro Manila Film Festival.    The film was produced by Star Cinema and released December 25, 2012.
 2012 MMFF. 38th Metro Best Actress award for Locsin.

==Synopsis==
Grace (Angel Locsin) is a single mother willing to sacrifice everything to save her ill son, Botchok (Miguel Vergara). Grace lives a good life with her son and supportive boyfriend, Tristan (Zanjoe Marudo). When Botchok’s rare blood disease became severe, Grace was forced to reconnect with Botchok’s biological father, Edward (Dingdong Dantes).  Edward, an accomplished man married to an equally successful Jacqueline (Angelica Panganiban), is uncomfortable with reconnecting with Grace.  Having no children of their own, Jacqueline gives her blessings for Edward to reconnect with Grace to help save their son.  The reconnection with Grace and Botchok starts to taint Edward and Jacquelines marriage.

==Cast==
*Angel Locsin as Grace
*Angelica Panganiban as Jacqueline
*Dingdong Dantes as Edward
*Zanjoe Marudo as Tristan
*Carmina Villaroel as Dra. Chesca
*Agot Isidro as Marga
*Gina Pareño as Lola Medy
*Mel Kimura
*Malou Crisologo
*Edward Mendez as Archie
*Ian Galliguez as Helper
*Thou Reyes
*Jose Sarasola
*Miguel Vergara as Bochok

==Production==

===Casting===
Angel Locsin, one of the female leads, stated in an interview that she initially hesitated to accept the role of Grace. She had doubts if she could portray the role of a mother because she has yet to experience being a mother in real life. 

Dingdong Dantes was cast as Edward. According to the director, Ruel S. Bayani, Dantes acting performance in the horror film "Segunda Mano" (an MMFF movie entry garnering Dantes the best actor award) was one of the reasons why he was chosen for the role. Bayani added that they wanted to cast a new mix of actors for the film to give it a "different flavor".  This 38th MMFF entry marks Dingdong Dantes second big screen project under Star Cinema after Segunda Mano.

===Trailer===
The films official trailer was released on Star Cinemas YouTube account on November 30, 2012. 

===Music===
The films theme song is Angeline Quintos cover of "Without You", originally sung by Joey Albert.

==Reception==

===Critical reception===
The film was graded "A" by the Cinema Evaluation Board, and it received R-13 rating by the MTRCB. 

Speculations surfaced about its uncanny similarity to the Chinese film In Love We Trust, released in 2007. Both movies have similar story lines.  The initial booking title of "One More Try", per National Cinema Association of the Philippines, was "In Love We Trust". 

===Box office===
One More Try opened at third place with a first-day gross of ₱8.8 million in Metro Manila and ₱4.8 million in provinces for a total of ₱13.6 million nationwide behind Sisterakas and Si Agimat, si Enteng Kabisote at si Ako, which opened in first and second place respectively. In the fourth day of showing, the film grossed with over ₱78 million which overtook Si Agimat, si Enteng Kabisote at si Ako which grossed over ₱69 million.  

===Television premiere===
The film had its television premiere on October 27, 2013 in the cable channel Cinema One.

==Accolades==
===Awards and nominations===
The film was proclaimed Best Picture during the 38th Metro Manila Film Festival Awards night. 

{| class="wikitable" sty
|- bgcolor="#CCCCCC"
| Award Giving Body || Award || Recipient(s) || Result
|- 38th Metro Manila Film Festival
|- Best Picture || ||  
|- Best Actor || Dingdong Dantes ||  
|- Metro Manila Best Actress || Angel Locsin ||  
|- Angelica Panganiban ||  
|- Best Director || Ruel S. Bayani ||  
|- Best Supporting Actor || Zanjoe Marudo ||  
|- Best Supporting Actress || Gina Pareño ||  
|- Best Child Performer || Miguel Vergara ||  
|-  Best Editing || Vito Kahili ||  
|-  Best Screenplay || Anna Karenina Ramos, Kriz Gazmen, Jay Fernando ||  
|-
| Fernando Poe Jr. Memorial Award for Excellence || ||  
|-
| rowspan=14| 29th PMPC Star Awards for Movies   
|-
| Movie of the Year ||  ||  
|-
| Movie Director of the Year || Ruel S. Bayani ||  
|- Movie Actress of the Year || Angel Locsin ||  
|-
| Angelica Panganiban ||  
|-
| Movie Actor of the Year || Dingdong Dantes ||  
|-
| Movie Supporting Actor || Zanjoe Marudo ||  
|-
| Movie Child Performer of the Year || Miguel Vergara ||  
|-
| Movie Screenwriter of the Year || Kriz Gazmen, Jay Fernando, and Anna Karenina Ramos ||  
|-
| Movie Cinematographer of the Year || Charlie Peralta ||  
|-
| Movie Production Designer of the Year || Nancy Arcega ||  
|-
| Movie Editor of the Year || Vito Cajili ||  
|-
| Movie Musical Scorer of the Year || Raul Mitra ||  
|-
| Movie Sound Engineer of the Year || Arnel Labayo ||  
|- 2013 GMMSF 44th Box-Office Entertainment Awards 
|-
| Film Actor of the Year || Dingdong Dantes ||  
|-
| Film Actress of the Year || Angel Locsin ||  
|-
| Film Actress of the Year || Angelica Panganiban ||  
|-
| rowspan=6| 61st FAMAS Awards    
| Best Actress of the Year || Angel Locsin ||  
|-
| Best Actress of the Year || Angelica Panganiban ||  
|-
| Best Actor of the Year || Dingdong Dantes ||  
|-
| Best Supporting Actor of the Year || Zanjoe Marudo ||  
|-
| Best Supporting Actress of the Year || Carmina Villaroel ||  
|-
| Best Child Performer of the Year || Miguel Vergara ||  
|- 10th Golden Screen Awards for Film || Best Performance by an Actress in a Leading Role (Drama) || Angelica Panganiban ||  
|}

== See also ==
* My Neighbors Wife In the Name of Love
* No Other Woman The Mistress
* A Secret Affair
* Seduction (2013 film)|Seduction
* The Bride and the Lover
* When the Love Is Gone Trophy Wife
* Once a Princess The Gifted The Trial

==References==
 
 

 
 
 
 
 