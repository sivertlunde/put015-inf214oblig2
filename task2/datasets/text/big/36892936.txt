Easy!
 
{{Infobox film
| name           = Easy!
| image          = Easy! film poster.jpg
| caption        = Film poster Francesco Bruni
| producer       = Beppe Caschetto Francesco Bruni Francesco Bruni
| starring       = Fabrizio Bentivoglio
| music          = The Ceasars (Ceasar Productions)
| cinematography = Arnaldo Catinari	
| editing        = Marco Spoletini
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 Francesco Bruni.    

==Cast==
 
* Fabrizio Bentivoglio as Bruno
* Filippo Scicchitano as Luca
* Barbora Bobuľová as Tina
* Vinicio Marchioni as Il Poeta
* Stefano Brunori as Stefano
* Franco Campiti as Franco
* Giacomo Ceccarelli as Valerio
* Paola Tiziana Cruciani as Giovanna
* Adamo Dionisi as Il piccoletto Giuseppe Guarino as Carmelo
* Raffaella Lebboroni as Professor Di Biagio
* Natascia Macchniz as Segretaria liceo

==Plot==
A retired teacher and novelist (Bruno), who survives by private tutoring, is currently writing the biography for former adult star (Tina). He then discovers that one of his students (Luca), a teenager who is on the brink of failure at school, is actually his son.

==Music==
The twelve tracks of the original soundtrack were produced by  The Ceasars  and sung by the Italian rapper Amir Issaa, then  published by EMI Music Publishing Italy.  The official videoclip of the film, directed by Gianluca Catania, won the 2012 Roma Videoclip Award. The Ceasars and Amir were nominated for the 2012 David di Donatello Award and Nastro dArgento (silver ribbons) for the song “Scialla” and won the 2012 “Premio Cinema Giovane” for the best original soundtrack.

===Tracks===
# FRancesco Rigon – Le onde
# Amir – La parte del figlio
# Amir – Scialla
# FRancesco Rigon – Mr. Slide
# Amir – Questa è Roma
# Ceasar & PStarr – Pool party
# Ceasar Productions – Macchina gialla
# Amir – La strada parla
# Ceasar & PStarr – Discoteque
# Ceasar & PStarr – Scialla variazioni sul tema
# FRancesco Rigon – Il gatto e la pioggia
# Amir – Le ali per volare

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 