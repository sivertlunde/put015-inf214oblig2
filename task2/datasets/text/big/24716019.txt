Dallas Doll
 
 
{{Infobox film
| name           = Dallas Doll
| image          = 
| caption        = Ann Turner
| producer       = Ray Brown Penny Chapman
| writer         = Ann Turner Victoria Longley Celia Ireland Rose Byrne
| music          =
| cinematography = Paul Murphy
| editing        = Michael Honey
| distributor    =
| released       = 1994
| runtime        =
| country        = Australia
| language       = English
}} Victoria Longley, Frank Gallacher, Jake Blundell, Rose Byrne and written and directed by Ann Turner. 

==Plot==
Dallas Adair is an American consultant brought to Australia to advise on a new golf course project. On the plane from L.A. she meets the son of one of her bosses when theres a near crash landing, after which Dallas moves into their bourgeois home, which also includes a frustrated wife and a UFO freak teen daughter.

==Production==
The film was developed with the assistance of the Australian Film Commission and made with money from the FFC. Sandra Bernhard was imported to play the lead. Ann Turner:
 "Dallas Doll" essentially came from being part of and seeing how Australians really worship experts from overseas. Ive done some writing workshops where that would happen. An American is brought out - and the willingness for Australians to accept with open arms whatever fraud comes out because theyre American, thats absolutely the starting point of Dallas Doll.   accessed 21 November 2012  

==Reception==
The film was released theatrically in the United Kingdom and Germany and received some positive reviews.

==References==
 

== External links ==
* 
*http://www.timeout.com/film/newyork/reviews/64876/dallas-doll.html
*http://www.variety.com/review/VE1117908623.html?categoryid=31&cs=1&p=0
*http://www.austinchronicle.com/gyrobase/Calendar/Film?Film=oid%3A142628
*http://movies.nytimes.com/movie/134209/Dallas-Doll/overview


 
 
 


 