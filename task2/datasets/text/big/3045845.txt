Sirocco (film)
{{Infobox film
| name           = Sirocco
| image          = Sirocco- 1951 - poster.png
| alt            =
| caption        = Theatrical release poster
| director       = Curtis Bernhardt
| producer       = Robert Lord
| screenplay     = A.I. Bezzerides Hans Jacoby
| based on       =  
| starring       = Humphrey Bogart Märta Torén Lee J. Cobb
| music          = George Antheil
| cinematography = Burnett Guffey
| editing        = Viola Lawrence
| studio         = Santana Pictures Corporation
| distributor    = Columbia Pictures 
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.3 million (US rentals) 
}}

Sirocco is a 1951 American film noir directed by Curtis Bernhardt and written by A.I. Bezzerides and Hans Jacoby. It is based on the novel Coup de Grace written by Joseph Kessel. The drama features Humphrey Bogart, Märta Torén, Lee J. Cobb, among others. 

==Plot== French rule of Syria. Harry Smith (Humphrey Bogart) is an amoral American black marketeer secretly selling them weapons. As the situation deteriorates, French General LaSalle (Everett Sloane) orders that civilians be executed each time his soldiers are killed, but his head of military intelligence, Colonel Feroud (Lee J. Cobb), persuades him to rescind the plan. Feroud presses for negotiations with rebel leader Emir Hassan (Onslow Stevens) instead. LaSalle reluctantly lets him try to arrange a meeting, but refuses to let Feroud make contact directly. The young officer sent in his place is later found with his throat cut.

To complicate matters, Harry makes a pass at Ferouds unhappy mistress, Violetta (Märta Torén), but she rejects him. Later, she informs Feroud she wants to leave him, but he refuses to let her go.
 gun running. Harry is tipped off, just as Violetta shows up and begs him to take her back to Cairo. Needing to flee himself, he agrees to take her along. However, a French patrol nearly captures Harry. He barely gets away, but has to leave behind his money, and without that, he is soon betrayed to the French.

Facing execution, Harry agrees to help Feroud meet with Hassan. Hassan calls the colonel a fool and dismisses his plea for negotiations, but decides to spare his life when Harry and Ferouds aide Major Leon (Gerald Mohr) show up offering a £10,000 ransom. The officers are allowed to leave; Harry is not so lucky. The rebels are angered that he has revealed the location of their headquarters to the French and fear he has sold them out, so they kill him. As Feroud and Leon walk back, they notice that the incessant gunfire and explosions have stopped. Feroud wonders aloud if he has convinced Hassan to be as big a fool.

==Cast==
* Humphrey Bogart as Harry Smith
* Märta Torén as Violette
* Lee J. Cobb as Col. Feroud
* Everett Sloane as Gen. LaSalle
* Gerald Mohr as Major Leon
* Zero Mostel as Balukjiaan
* Nick Dennis as Nasir Aboud, Harrys assistant
* Onslow Stevens as Emir Hassan
* Ludwig Donath as Flophouse proprietor
* David Bond as Achmet
* Vincent Renno as Arthur

==Critical reception==
Film critic Bosley Crowther lambasted the film and wrote, "Except for a few moody moments in a plaster night-club, called the Moulin Rouge, and some shadowy shots of sloppy Syrians lying around in dingy catacombs, the scene is no more suggestive of Damascus than a Shriners convention in New Orleans, on which occasion you would see more fezzes than ever show up in this film.  For the most part—indeed, for the sole part—Sirocco wafts a torpid tale of a slick, sneering gun-runner proving a painful thorn in a nice French colonels side." 

Critic Leonard Maltin gave the film a mixed review, writing, "I’d always read that it was a half-baked attempt to rekindle some of the ingredients that made Casablanca (film)|Casablanca such a success, and that’s true.  The setting is Damascus in 1926, when the French Army is battling Syrian insurgents...Sirocco is strictly formula stuff, but it’s a perfect example of how Hollywood could take ordinary material and still make it entertaining, through sheer professional polish in the writing, staging, art direction, and casting.  Zero Mostel,  Gerald Mohr, and Nick Dennis head the colorful supporting cast, who perform well under Curtis Bernhardt’s direction." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 