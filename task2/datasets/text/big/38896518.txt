The Golden Disc
{{Infobox film
| name           = The Golden Disc
| image          = The In-Between Age.jpg
| image_size     = 
| caption        = Poster under the title The In-Between Age
| director       = Don Sharp
| producer       = 
| writer         = Don Sharp Don Nicholl
| narrator       = 
| starring       = Mary Steele Lee Patterson Terry Dene
| music          = 
| cinematography = 
| editing        = 
| studio = Butchers Film Productions
| distributor    = 
| released       = August 3, 1958
| runtime        = 78 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

The Golden Disc (also known as The In-Between Age) is a 1958 British pop musical film. It was directed by Don Sharp, who was married to the leading lady Mary Steele. 

It was re-released on DVD by Renown Pictures ltd. in 2010. 

==Plot==
Two young kids open a trendy coffee bar and discover a star.

== Cast ==
 

==Songs==
* ‘I’m Gonna Wrap You Up’ (by Ray Mack, Philip Green) performed by Dennis Lotis
* ‘Before We Say Goodnight’ (by Norman Newell, Philip Green) performed by Mary Steele
* ‘Dynamo’ (by Tommy Connor) performed by Les Hobeaux Skiffle Group   
* ‘C’min and be Loved’ (by Len Paverman) performed by Terry Dene
* ‘Charm’ (by Ray Mack, Philip Green) performed by Terry Dene
* ‘The In-between Age’ (by Ray Mack, Philip Green) performed by Sheila Buxton
* ‘Let Me Lie’ (by Sonny & Stewart) performed by Sonny Stewart and his Skiffle Kings
* ‘Candy Floss’ (by Len Paverman) performed by Terry Dene
* ‘Lower Deck’ (by Phil Seamon) performed by Phil Seamon Jazz Group
* ‘Balmoral Melody’ (by Philip Green) performed by Murray Campbell
* ‘Johnny O’ (by Len Praverman) performed by Nancy Whiskey and Sonny Stewart and his Skiffle Kings 
‘The Golden Age’ (by Michael Robbins, Richard Dix) performed by Terry Dene.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 


 