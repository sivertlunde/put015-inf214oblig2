The Mouse and the Motorcycle
 
{{Infobox book |  
| name         = The Mouse and the Motorcycle
| image        = Image:Mouse and the Motorcycle.jpg
| caption      = First edition
| author       = Beverly Cleary
| illustrator  = Louis Darling
| country      = United States English
| series       = The Mouse and the Motorcycle
| genre        =  William Morrow
| release_date = 1965
| media_type   = Print (Hardcover and Paperback)
| pages        = 186
| isbn         = 
| oclc         = 22460783
| preceded_by  =
| followed_by  = Runaway Ralph
}}

The Mouse and the Motorcycle is a childrens novel written by Beverly Cleary and published in 1965. {{cite book|url=http://books.google.com/books?id=3XB5jzgEFBwC&source=gbs_book_other_versions| title=The Mouse and the Motorcycle: HarperTrophy Books
 |accessdate=5 March 2012 }} 

== Plot summary ==
Ralph is a mouse who lives in the run-down Mountain View Inn, a battered resort hotel in the Sierra Nevada of California. Ralph longs for a life of danger and speed, wishing to get away from his relatives, who worry about the mice colony being discovered. One day a boy named Keith Gridley and his family visit the hotel on their way through California. Keith leaves a toy motorcycle on his bedside table, While Keith is away, Ralph attempts to ride it, but cannot figure out how to start it. Startled by a telephone ring, both Ralph and the motorcycle fall into a metal wastebasket.

Keith discovers his missing motorcycle in the wastebasket. Although Ralphs mother worries that he is in contact with humans, Keith shows Ralph how to start the motorcycle—make an engine-like noise—and lets Ralph ride it during the nighttime. While Keith and his family explore California, Ralph recklessly rides the motorcycle through the depths of the hotel. One night he is spotted by Keiths mother, and Mrs. Gridley thinks she is imagining things, but she is still sure that she saw a mouse riding a motorcycle. Ralph and the motorcycle are almost sucked up by a maids vacuum cleaner, but Ralph escapes, riding into a pile of dirty bedsheets. He escapes by chewing holes in the sheets,
After Ralph loses the motorcycle Keith loses trust in him, although he still brings the mice colony food. One night Keith becomes very sick but his parents dont have any more aspirin, nor are able to obtain one until morning. To regain Keiths trust, Ralph searches the hotel for an aspirin tablet, at risk to himself, for the medicine could prove fatal to a small mouse if ingested (In fact, this caused the death of Ralphs father). When Ralph succeeds, Keiths health is restored. Ralph uses the space under the TV set in the lobby to use as a garage; the motorcycle is his to keep.

== Series ==
*The Mouse and the Motorcycle (1967)
*Runaway Ralph (1970)
*Ralph S. Mouse (1982)

==Printings==
The book was released as a selection of the Weekly Reader Childrens Book Club - Intermediate Division.

==Film adaptation==
Churchill Films produced an adaptation of The Mouse and the Motorcycle directed by Ron Underwood in 1986, featuring Mimi Kennedy and Ray Walston.  The film was distributed by Strand Home Video and Golden Book Video under the "GoldenVision" label. {{cite web|url=http://www.imdb.com/title/tt0196767/| title="ABC Weekend Specials" : The Mouse and the Motorcycle
 |accessdate=5 March 2012 }} 

==References==

 

 
 
 
 

 

 
 
 
 
 
 
 
 
 