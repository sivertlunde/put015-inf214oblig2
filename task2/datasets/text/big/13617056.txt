Horror House on Highway Five
{{multiple issues|
 
 
}}

{{Infobox Film
| name           = Horror House on Highway Five
| image          = 
| caption        =  Richard Casey
| producer       = John P. Marsh(as John Marsh)(executive producer) Richard Casey
| starring       = 
| music          = Kraig Grady/Suzanne McDermott
| cinematography = David Golia
| editing        = 
| distributor    = Simitar Video 1985
| runtime        = 91 minutes
| country        = United States
| language       = English
}}
 independent Low low budget sociopath dressed up as Richard Nixon, who goes rampaging through the night traveling down a highway to terrorize passing motorists.

==Synopsis==
As part of a project, students are sent to a place called Littletown to investigate and research a supposedly deceased German (and possible Nazi) expatriate rocket scientist named Frederick Bartholomew, who was supposedly responsible for the V-2 rocket before embarking on a murderous rampage slaying all who he worked with in his final days spent in America. The student group have got to make replicas of the rocket whilst at that location.  A trio of the group stumble across a pair of demented brothers; one, Mabuser is an unlicensed doctor, whos become mentally unhinged due to being convinced that destructive parasites have infested his brain, while his stuttering teenage brother Gary, is a shy and lonely psychopath with a thing for Tarot cards and concealed necrophiliac tendencies.  All the while their father prowls the nights lonely highways, dressed in one of his previous victims...as of all things Richard Nixon (complete with matching mask).

==Cast==
* Phil Therrien as Dr. Marbuse
* Max Manthey as Gary
* Irene F. as Sally Smith
* Michael Castagnolia as The Pothead
* Susan Leslie as Louise
* Randy Daitch as the Teacher
* William Pope as Gentleman
* Richard Meltzer as Tough Guy
* Kathleen Battersby as Housewife
* Steve DeVorkin as Van Driver
* Robert Gaulin as Student
* S. Eisenstein as Student
* Ronald Reagan as Richard Nixon

 
 
 
 
 


 