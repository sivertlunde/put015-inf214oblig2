An Assisted Elopement
 
{{Infobox film
| name           = An Assisted Elopement
| image          = An Assisted Elopement 1910.jpg
| caption        = A surviving film still
| director       =
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = 
| released       =  
| runtime        =
| country        = United States English inter-titles
}} silent short short comedy produced by the Thanhouser Company. The film focuses on Gladys and Charlie who meet each other on the train home and they become romantically interested in each other. It turns out their parents are friends and wish for them to get married, souring the relationship between them. In a ploy to get their children to elope, the fathers become bitter enemies in public and the couple elopes much to their enjoyment and intention. A surviving film still shows several of members of the cast, including Frank H. Crane, Violet Heming, and Alphonse Ethier. The film was released on August 30, 1910 and saw a wide national release. The film is presumed lost film|lost.

== Plot ==
Though the film is presumed lost film|lost, a synopsis survives in The Moving Picture World from September 3, 1910. It states: "Glade and Sears are next door neighbors and old friends. Glade has a daughter, while Sears has a son. The young people have never met, being away at school while the old folks have been cementing their friendship. As the two men own adjoining places, they believe that the best thing for the younger people to do is to get married. So they try to bring this about. Gladys Glade and Charlie Sears meet on the train while they are returning home and start a flirtation. Perhaps they would have married in the end, if the old folks hadnt butted in. As it is, they lose all interest in each other. Then the fathers try another tack. They decide to be bitter enemies in public, hoping the opposition will bring the children together. The new plan works like a charm, and Gladys and Charlie, realizing their parents shortcomings, decide to elope. They do so, much to the satisfaction of Glade and Sears." 

== Cast ==
*Frank H. Crane   
*Violet Heming 
*Alphonse Ethier 

== Production ==
The writer of the scenario is unknown, but it was most likely Lloyd Lonergan. He was an experienced newspaperman employed by The New York Evening World while writing scripts for the Thanhouser productions.  The film director is unknown, but it may have been Barry ONeil. Film historian Q. David Bowers does not attribute a cameraman for this production, but at least two possible candidates exist. Blair Smith was the first cameraman of the Thanhouser company, but he was soon joined by Carl Louis Gregory who had years of experience as a still and motion picture photographer. The role of the cameraman was uncredited in 1910 productions.    Identification of three members of the cast come from a surviving film still in a Thanhouser advertisement.  Other credits may have included Anna Rosemond, one of two leading ladies of the Thanhouser company in this era.  One of the actors in the film was Frank H. Crane, a leading male actor of the company and also involved since the very beginnings of the Thanhouser Company.  Bowers states that most of the credits are fragmentary for 1910 Thanhouser productions.   

== Release and reception ==
The one reel comedy, approximately 1,000 feet long, was released on August 30, 1910.    The film saw a wide national release, with advertisements in Kansas,  Wisconsin,  Pennsylvania,  North Carolina,  Nebraska,  Arizona,  and Oklahoma.  The film was also shown by the Province Theatre in Vancouver, Canada.  Though it had been released years prior to its examination by the Pennsylvania State Board of Censors of Moving Pictures, the film was approved without need any modifications or censoring. 
 An Assisted An Assisted Elopement.  

== References ==
 

 
 
 
 
 
 
 
 
 