The Girl with the Whip
{{Infobox film
| name           = The Girl with the Whip
| image          = 
| image_size     = 
| caption        = 
| director       = Carl Lamac
| producer       = Victor Skutezky
| writer         = Hans H. Zerlett (play)   Walter Wassermann
| narrator       = 
| starring       = Anny Ondra   Werner Fuetterer   Sig Arno   Olga Limburg
| music          = 
| editing        = 
| cinematography = Otto Heller
| studio         = Hom-AG für Filmfabrikation  
| distributor    = Süd-Film
| released       = 28 November 1929
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent comedy film directed by Carl Lamac and starring Anny Ondra, Werner Fuetterer and Sig Arno.  The films art direction was by Heinrich Richter. It was based on a play by Hans H. Zerlett.

==Cast==
* Anny Ondra as Anny Nebenkrug 
* Werner Fuetterer as Edgar Krell 
* Sig Arno as Onkel Axmann 
* Gaston Jacquet as Professor Nebenkrug 
* Olga Limburg as Frau Professor Nebenkrug 
* Mimo von Delly as Mimi Gwenda, Tänzerin 
* Julius E. Herrmann   
* Karl Harbacher   
* Gerhard Ritterband   
* Josef Rovenský   
* Oreste Bilancia   
* Paul Goergens

==References==
 

==Bibliography==
*Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910–1933. Berghahn Books, 2005.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 