Chandan Ka Palna
 
{{Infobox film
| name           =Chandan Ka Palna
| image          = 
| image_size     = 
| caption        = 
| director       =Ismail Memon
| producer       =
| writer         =
| narrator       =  Mehmood
| lyrics         =Anand Bakshi  
| music          =R. D. Burman
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1967
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Chandan Ka Palna  is a 1967 Bollywood film starring Dharmendra and Meena Kumari. No joy in the world can equal the happiness a man derives from watching his own child at play. Not all the riches in the world can compensate for the vacuum, the emptiness, created by the want of a child in a home. With a woman this yearning is a hundred times stronger and the outcome of her torment impossible beyond measure. Radha belonged to a rich family. Her sole aim in life after becoming a widow, was to see her sone Ajit happy and fulfil the promise she had given to her husband that the family name and tradition would be perpetuated. Finding Ajit deeply in love with Shobha, the Raisahebs daughter who too loved him no less, Radha got them married, hoping that Shobha would prove an ideal wife and daughter-in-law and their home would soon resound with merry laughter of a child, if not children. Three years went by and Radha was still waiting for the new arrival in the family. Her patience was running out. Desperate, she took Shobha to a doctor who, after prolonged examination and treatment, told her that God alone could help her fulfill her yearning for a grandchild. Radhas sorrow knows no bounds. She could not think of the family chain breaking off. She had to have a grandchild and fulfil her promise to her late husband. As a last resort, she took Shobha to Gurudev, the spiritual soul revered by the family. Gurudev had pity on Radhas plight. Taking her into confidence, he told her: ""Look, I am no miracle worker. And, as far as my knowledge goes, I am certain that your daughter-in-law is destined to remain barren.""When Shobha heard this, she was in a daze. She knew her mother-in-laws craving for a grandchild and she knew also that woman would leave no stone unturned to accomplish her objective.Life seemed to Shobha without a ray of hope for her to live. She decided to put an end to it. It was better that way, she thought, than to suffer the humiliation of being branded for ever barren. She knew how deeply her husband loved her and she could very well imagine his dilemma when forced to marry again for the sake of a child.She started walking in the direction of the fatal plunge to end all problems, to end herself. But what mere mortal can meet his or her end according to his or her own design? Destiny had other things written down for Shobha. But let that be unfolded on the screen in ""Chandan Ka Palna"" lest further knowledge of the proceedings spoil full enjoyment of the drama.

==External links==
*  
* http://www.youtube.com/movie?v=Bqhmy7dQBR0&feature=relover

 
 
 
 


 