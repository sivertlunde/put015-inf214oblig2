Gimme the Power
{{Infobox film
| image          =
| alt            =
| caption        =
| director       = Olallo Rubio
| producer       = Olallo Rubio Jose Nacif
| writer         = Olallo Rubio
| narrator       = Olallo Rubio Molotov
| music          = Javier Umpierrez
| cinematography = Jose Casillas
| editing        = Abraham Neme Juan Fontana Rosas
| studio         = Amateur Films
| distributor    =
| released       =   }}
| runtime        = 101 minutes
| country        = Mexico
| language       = Spanish
| budget         =
| gross          =
}}
Gimme the Power is a 2012 Mexican documentary film by Olallo Rubio about the band Molotov (band)|Molotov.

== Synopsis == Molotov and Avandaro Festivals La Onda hippie generation until modern times.

== Cast ==
* Sergio Arau
* Olallo Rubio
* Juan Villoro
* Fernanda Tapia
* Molotov (band)|Molotov: Tito Fuentes, Miky Huidobro, Paco Ayala, and Randy Ebright
* Luis de Llano Macedo
* Alex Lora
* Armando Molina S.

== Production ==
Rubio had wanted to do a rockumentary for a long time and was inspired to direct one after he saw the band perform in Spain in 2003.  He said that he hoped the film would inspire young people to be more critical of the government. 

== Release == general election. 

== Reception == El Universal, the film was received well in Mexico. 

== References ==
 

== External links ==
*  

 
 
 
 
 


 
 