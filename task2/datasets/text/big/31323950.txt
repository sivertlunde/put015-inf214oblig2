The Rabbit Is Me
{{Infobox film
| name           = The Rabbit Is Me
| image          = The Rabbit Is Me.jpg
| image size     =
| caption        =
| director       = Kurt Maetzig
| producer       = Martin Sonnabend	
| writer         = Manfred Bieler Kurt Maetzig
| starring       = Angelika Waller
| music          = Reiner Bredemeyer, Gerhard Rosenfeld
| cinematography = Erich Gusko
| editing        = Helga Krause
| distributor    = PROGRESS-Film Verleih
| released       =  
| runtime        = 110 minutes
| country        = East Germany
| language       = German
| budget         =
| gross          =
}}
 East German dramatic film directed by Kurt Maetzig.    It was filmed in List of East German films|1965, and based on the novel by Manfred Bieler.

==Plot==
Nineteen-year-old Maria Morzeck dreams of studying Slavistics, but her hopes are shattered when her brother, Dieter, is sent to prison after being convicted of sedition against the state. She cannot enter college, and becomes a waitress. Maria meets and falls in love with Paul Deisler, an older, married man who turns out to be the judge who convicted her brother. Their affair ends when Deisler is exposed as hypocritical and corrupt. After Dieters release, he learns of his sisters relationship with the judge and assaults her. Eventually, Maria distances herself from both of them, and decides to pursue her forgotten dream.

==Cast==
* Angelika Waller as  Maria Morzeck  Alfred Müller as  Paul Deister 
* Ilse Voigt as aunt Hete  Wolfgang Winkler as Dieter Morzeck 
* Irma Münch as Gabriele Deister 
* Rudolf Ulrich as  Grambov 
* Helmut Schellhardt as the mayor
* Annemarie Esper as  Edith 
* Willi Schrade as  Ulli 
* Willi Narloch as  Oscar

==Production== Socialist Unity Party at January 1963, during which the establishment allowed a measure of liberalization in the cultural life of East Germany. Although Bielers novel was highly critical of the court system, he and Maetzig took care to include several "alibi scenes" in the film that were intended to put the state in a better light and also prevent the banning of the picture. The scenes were also meant to present the judicial reforms that took place between 1961 and 1963. 

==Reception==
The short era of liberalization ended gradually when Leonid Brezhnev took power in the Soviet Union and introduced a conservative, more repressive course on cultural questions. The film, alongside eleven other cinematic works that were deemed politically damaging, was banned by the Central Committee of the SED at its XI Plenum in December 1965.    It was only made legal again in 1990.    The banned films were known as "cellar films" or "rabbit films" - the second sobriquet having been derived from the films title.

At 1990, shortly before the collapse of the Eastern Bloc, the picture was released for public screening, and presented in the Berlin and Locarno film festivals. In 1995 it was elected as one of the 100 most important German films by a group of historians and critics. 

Daniela Berghahn noted that The Rabbit Is Me was unprecedented in its portrayal of judicial corruption, sexual themes and criticism of the East German establishment. 

==See also==
* Film censorship in East Germany

==References==
 

== Literature ==
* Günter Adge (Hrsg.): Kahlschlag. Das 11. Plenum des ZK der SED. Studien und Dokumente. 2. erweiterte Auflage. Aufbau Taschenbuch, Berlin 2000, ISBN 3-7466-8045-X.
* Christiane Mückenberger (Hrsg.): Prädikat: Besonders schädlich. Filmtexte. Henschel Verlag, Berlin 1990, ISBN 3-362-00478-4.
* Ingrid Poss, Peter Warnecke (Hrsg.): Spur der Filme. Zeitzeugen über die DEFA. Links, Berlin 2006, ISBN 3-86153-401-0, (Schriftenreihe der DEFA-Stiftung).
* Neues Deutschland, 6. Januar 1966.
* Frankfurter Rundschau 1990.

==External links==
*  
*   on ostfilm.de.

 

 
 
 
 
 
 
 
 