Wajood
{{Infobox film
| name           = Wajood
| image          = Wajood.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = N. kithania
| producer       = Jayashri N. Chandra   N. Chandra
| writer         = N. Sinha   Kamlesh Pandey(dialogues)
| screenplay     = 
| story          = N. Chandra
| based on       =  
| narrator       = 
| starring       = Nana Patekar   Madhuri Dixit
| music          = Santosh Kumar Shaw
| cinematography = W.B. Rao
| editing        = Prashant Khedekar   Vinod Nayak 
| studio         = 
| distributor    = Asian Video Wholesalers 
| released       =  
| runtime        = 
| country        = India 
| language       = Hindi
| budget         = 
| gross          = Rs. 9,75,00,000 
}} Hindi drama film. The film features Nana Patekar and Madhuri Dixit in lead roles. 
The film was rated "Below Average" at the box-office.   

==Plot==
Malhar (Nana Patekar), who is born to a poor typist clerk, is a versatile character who wins trophies in dramas but is never appreciated by his father. While writing and directing dramas in his college he meets Apoorva (Madhuri Dixit), a very rich girl, and falls madly in love with her. When she wins the Best Actress Award, she calls upon Malhar on stage and gives all the credit to him. Malhar misunderstands this gestures as Apoorvas love for him.

Meanwhile Apoorva and Nihal (Mukul Dev) fall in love with each other. Nihals father, who is a wealthy bureaucrat, decides to get them married. Malhar assumes that this marriage is against Apoorvas will and in his protest Nihals father is accidentally killed. Malhar is sent to jail. As Nihal loses his identity and status by his fathers death, Apoorvas father breaks off her engagement. In jail, Malhar spends his days thinking of Apoorva, getting married to her, having a child and living happily with his father; all in one family.
 
Malhar escapes the jail and then starts making quick money out of wrong ways. He starts seducing rich women and then looting them. Nihal, a police officer now, happens to meet Apoorva, a journalist now, to solve the mystery of the stranger murderer in the city. Finally when Malhar comes face to face with Apoorva, she tells him that shed never loved him. Heart-broken Malhar is then caught and dies in the end.

==Cast==
* Nana Patekar   as    Malhar Gopaldas Agnihotri / Col. Latti 
* Madhuri Dixit   as    Apoorva Choudhury 
* Mukul Dev   as    Nihal Joshi 
* Ramya Krishna   as    Shalini / Sophia / Amina 
* Johnny Lever   as    Inspector Rahim Khan (as Johny Lever) 
* Dr. Hemu Adhikari   as    Malhars father 
* Maya Alagh   as    Mrs. Abhijeet Joshi 
* Gulzar   as    Himself (presenting award) 
* Jagdeep   as    Bagh Singh 
* Kunika   as    Mrs. Chawla  Sanjay Mishra   as    Kanchan (camera operator) 
* Parikshat Sahni   as    Abhijeet Joshi 
* Tej Sapru   as    Apoorvas boss 
* Shivaji Satham   as    Police Commissioner of Mumbai 
* Rajiv Varma   as    Mr. Choudhury (Apoorvas dad)

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aur Hum Tum"
| Kumar Sanu, Alka Yagnik
|-
| 2
|"Kaise Bataaoon Main" Nana Patekar
|-
| 3
|"Ki Toot Gayi"
| Sapna Awasthi, Kavita Krishnamurthy
|-
| 4
| "Main Kya Karoon"
| Alka Yagnik
|-
| 5
| "Main Sochta Hoon"
| Kumar Sanu, Alka Yagnik
|-
| 6
|"Sanam Tum Hum Pe" 
| Udit Narayan, Kavita Krishnamurthy
|}

==References==
 

==External links==
*  

 
 
 