Nikos the Impaler
{{Infobox film
| name        = Nikos the Impaler
| image       =
| caption     = Nikos DVD case cover
| writer      = Ted Geoghegan Andreas Schnaas
| starring    = Joe Zaso Felissa Rose Andreas Schnaas
| director    = Andreas Schnaas 
| producer    = Andreas Schnaas Joe Zaso 
| distributor =RatPack Independent Films
| released    =  
| runtime     = 126 minutes
| language    = English
| music       =
| budget      =
}}

Nikos the Impaler is a b movie|b-grade splatter film directed by and starring German arteur Andreas Schnaas. It follows a reincarnated barbarian (Andreas Schnaas|Schnaas) as he wreaks havok on modern day New York City. It is marketed sometimes as Violent Shit 4. 

== Synopsis ==

College professor Frank Heller (Joe Zaso) and his girlfriend Sandra (Felissa Rose) lead a ragtag group of museum patrons in a race for their lives. After a botched robbery attempt spills blood on the ancient mask of Nikos a lu Unziceanu (Andreas Schnaas|Schnaas), the barbarian returns to kill off virtually anyone and everyone that crosses his path. After wiping out the art museum, the maniac turns to a health club, gay bar, movie theater, and video store to continue his rampage.

== Production Information ==

* The film was shot in New York City on a budget of 40,000 dollars and was released direct-to-DVD.

* It is known in some markets as Nikos the Impaler and Violent Shit 4: Nikos. 

* The name Nikos was also the killers name in Andreas Schnaas earlier film, Anthropophagous 2000. Schnaas himself played Nikos in both movies.

* Cameos in the film include Debbie Rochon, Lloyd Kaufman, Darian Caine, Tina Krause, and Bela B. of Die Ärzte.

== Cast ==

* Joe Zaso as Frank Heller
* Felissa Rose as Sandra Kane
* Andreas Schnaas as Nikos
* Brenda Abbandandolo as Daisy
* Joseph Michael Lagana as Pete
* Joe Lattanzi as Ryan
* Patricia Ostergaard as Addy (the lesbian)

== Crew ==

* Andreas Schnaas          Director
* Ted Geoghegan            Screenwriter
* Joe Zaso                 Producer
* C.C. Becker              Producer

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 


 