Every Which Way but Loose (film)
 
 
{{Infobox Film
| name           = Every Which Way but Loose
| image          = Everywhichwaybutloosemovieposter.jpg
| caption        = Promotional movie poster
| director       = James Fargo Robert Daley
| writer         = Jeremy Joe Kronsberg Geoffrey Lewis Ruth Gordon John Quade
| music          = Steve Dorff
| cinematography = Rexford L. Metz
| editing        = Joel Cox Ferris Webster
| studio         = The Malpaso Company
| distributor    = Warner Bros.
| released       =  
| runtime        = 114 min.
| country        = United States
| language       = English
| budget         = $5 million Hughes, p.119 
| gross          = $85 million 
}}

Every Which Way but Loose is a 1978 American adventure comedy film, released by Warner Brothers, produced by Robert Daley and directed by James Fargo.  It stars Clint Eastwood in an uncharacteristic and offbeat comedy role, as Philo Beddoe, a trucker and brawler roaming the American West in search of a lost love while accompanied by his friend/manager Orville and his pet orangutan, Clyde. In the process Philo manages to cross a motley assortment of characters, including a pair of police officers and an entire motorcycle gang (the "Black Widows"), who end up pursuing him for revenge.

Eastwoods appearance in the film, after his string of spaghetti western and Dirty Harry roles, somewhat startled the film industry and he was reportedly advised against making it. Although it was panned by critics, the film went on to become an enormous success and became, along with its 1980 sequel Any Which Way You Can, two of the highest grossing Eastwood films. When adjusted for inflation, it still ranks as one of the top 200 highest grossing films of all time.

==Plot==
Philo Beddoe is a truck driver living in the San Fernando Valley. He lives in a small house, with an orangutan named Clyde, behind that of his friend Orville Boggs and his mother. Philo makes money on the side as a bare-knuckle fighter; he is often compared to a legendary fighter named Tank Murdock.
 Palomino Club, a local honky-tonk. His relationship with her seems to be going well until one day she and her camper disappear from the trailer park. Believing that he is falling for her, Philo decides to set off for Lynns home in Denver, Colorado.  

Along the way, he has a run-in with a motorcycle gang called "The Black Widows", who incur Philos wrath after two gang members insult him and Clyde at a traffic light. Philo chases them down and takes their bikes (which he repaints, repairs, and resells), and every attempt they make to get even results in disaster. Philo also incurs the wrath of LAPD cop named Putnam, with whom he gets into a fight at the Palomino. Both the officer and the Widows learn of Philos trip to Colorado and head off to find him.

Orville and Clyde accompany Philo to Denver, and on the way, they meet a woman named Echo who becomes Orvilles girlfriend. They earn money along the way by booking fights for Philo. After a fight in a slaughterhouse, the man holding the money tries to stiff Philo. Echo fires two shots from a .38, and the man hands over the money.

Knowing that Philo has come to look for her, Lynn helps the Black Widows lure him into a trap. Philo sees Lynn and attempts to talk to her, but finds himself surrounded by the Widows. He manages to fight most of them until Orville intervenes. Using a garbage truck with a dumpster hoist, he dumps all the motorcycles into the back of the truck. The Widows charge the garbage truck, but Orville gets away. Philo, Echo, and Orville then escape.

Philo finally finds Lynn and she reveals her true nature to him. Hurt by her callousness, Philo says that he is the only one dumb enough to want to take her further than her bed. Lynn erupts in a fit of rage, striking him repeatedly until she collapses in tears.

Orville learns that Tank Murdock, based in the area, is ready to retire after one more fight. Orville makes the arrangements, and Philo faces his elderly nemesis. During the fight, the crowd, initially pro-Murdock, begins to insult him, with some murmurs that Philo is going to be the next Murdock. Philo lets his guard down, intentionally giving Murdock a clear shot, knocking Philo down for the count. Murdock, having regained the crowds esteem, is allowed to retire undefeated. Clyde, Orville and Echo head home the next day.

== Cast ==
* Clint Eastwood as Philo Beddoe
* Sondra Locke as Lynn Halsey-Taylor Geoffrey Lewis as Orville Boggs
* Beverly DAngelo as Echo
* Ruth Gordon as Ma Boggs
* John Quade as Cholla, The Biker Leader 
* Dan Vadis as Frank, Assistant Head Biker
* Roy Jenson as Woody, Secretary Biker
* Bill McKinney as Dallas, Treasurer Biker 
* William OConnell as Elmo, Sergeant-At-Arms Biker
* Jeremy Joe Kronsberg as Bruno, Biker
* Gary Davis as Biker
* Scott Dockstader as Biker
* Orwin C. Harvey  as Biker (as Orwin Harvey)
* Gene LeBell as Biker 
* Chuck Waters as Biker
* Jerry Wills as Biker 
* Judson Scott as Biker 
* James McEachin as Herb Walter Barnes as Tank Murdock
* Gregory Walcott as Putnam
* Hank Worden as Trailer Court Manager
* George Chandler as D.M.V. Clerk Manis as Clyde
* Cary Michael Cheifer as Kincades Manager

==Production==
The script, written by Jeremy Joe Kronsberg, had been turned down by many other big production companies in Hollywood and most of Eastwoods production team agents all thought it was ill advised. McGilligan (1999), p.293   Bob Hoyt, who Eastwood had contacts with (through his Malpaso secretary Judy Hoyt and Eastwoods long term friend Fritz Manes) thought it showed promise and eventually convinced Warner Brothers to buy it. An orangutan named Manis was brought in to play Clyde; also cast were Geoffrey Lewis as the dimwitted Orville, Beverly DAngelo as his girlfriend, and Sondra Locke as Lynn Halsey-Taylor, the country and western barroom singer. Eastwood spoke about using the orangutan for the main role, "Clyde was one of the most natural actors I ever worked with! But you had to get him on the first take because his boredom level was very limited." 
 western theme, North Hollywood, San Fernando, Sun Valley, Van Nuys. Santa Fe and Taos, New Mexico|Taos, all in New Mexico.

==Title origin==
The films title refers to the eponymous Eddie Rabbitt song from the soundtrack, in which the singer complains that his girlfriend turns him "every which way but loose", i.e. he cannot bring himself to leave her although he is more of a freewheeling character.

==Release==
Upon its release, the film was a surprising success and became Eastwoods most commercially successful film at the time. It ranks high amongst those of his career, and was the second-highest grossing film of 1978. McGilligan (1999), p.302  It was panned by the critic David Ansen of Newsweek described the film as a "plotless junk heap of moronic gags, sour romance and fatuous fisticuffs".  It continued, "The only decent part is played by an orangutan. One can forgive   participation&mdash;he couldnt read the script&mdash;but what is Eastwoods excuse?"  Variety commented that, "This film is so awful its almost as if Eastwood is using it to find out how far he can go&mdash;how bad a film he can associate himself with". 

==Soundtrack== Every Which Way But Loose" by Eddie Rabbitt and several numbers by Charlie Rich. Songwriter Snuff Garrett was hired to write songs for the film, including three for Lockes character, something which proved problematic as Locke was not a professional singer. McGilligan (1999), p.299  One song Charlie Rich performed in the film, "Ill Wake You Up When I Get Home", hit number three on the charts in 1979 and was Richs last Top Ten single. Sondra Locke, who appears as Eastwoods love interest, performs several musical numbers in the film as well. Mel Tillis sings his own song "Send Me Down to Tucson" in full.

== References ==
 

==Bibliography==
* 
* 
* 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 