Thavarina Thottilu
 

{{Infobox film name           = Thavarina Thottilu image          = image_size     = caption        = director       = S. Narayan producer       = Bhgyavathi writer         = Ajay Kumar narrator       = starring  Shruti Charan Raj Doddanna music          = Rajesh Ramanath cinematography = R. Giri editing        = P. R. Soundar Raj studio         = Shanthala Pictures released       =   runtime        = 163 minutes country        = India language       = Kannada budget         =
}}
 Indian Kannada Kannada sentimental Shruti in the lead roles.  The film was produced by Narayans home production while the original score and soundtrack were composed by Rajesh Ramanath.

==Cast==
* Ramkumar  Shruti 
* Charan Raj
* Srinivasa Murthy
* Doddanna
* Ashalatha
* Rajanand
* Padma Vasanthi
* B. Jayamma

==Soundtrack==
The music of the film was composed by Rajesh Ramanath and lyrics written by S. Narayan.

{{Infobox album  
| Name        = Thavarina Thottilu
| Type        = Soundtrack
| Artist      = Rajesh Ramanath
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Jhankar Music
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= O Kusumave
| lyrics1 	= S. Narayan
| extra1        = Rajesh Krishnan, K. S. Chithra
| length1       = 
| title2        = Arishina Kuttiravva
| lyrics2 	= S. Narayan
| extra2        = S. P. Balasubrahmanyam, Gururaj Hoskote
| length2       = 
| title3        = Malenadina Minchina Balli
| lyrics3       = S. Narayan
| extra3 	= S. P. Balasubrahmanyam, K. S. Chithra
| length3       = 
| title4        = O Bombeye Dalimbeye
| extra4        = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics4 	= S. Narayan
| length4       = 
| title5        = Brahma Bareda Haaleyalli
| extra5        = S. P. Balasubrahmanyam
| lyrics5       = S. Narayan
| length5       = 
| title6        = Baaradu Barabaaradu
| extra6        = Gururaj Hoskote
| lyrics6       = S. Narayan
| length6       =
}}

==References==
 

==External source==
*  
*  
 
 
 
 
 
 
 


 

 