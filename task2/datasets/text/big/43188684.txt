Delitti e profumi
{{Infobox film
| name           =Delitti e profumi
| image          = Delitti e profumi.jpg
| caption        =  
 | director =Vittorio De Sisti
 | writer =  Francesco Massaro Franco Ferrini Oreste De Fornari Carlo Alberto Bonadies
 | starring =  
 | music = Umberto Smaila
 | cinematography = Giuseppe Maccari	
 | editing =   
 | language = Italian  
}}
 1988 Italian giallo film directed by Vittorio De Sisti.       

==Cast==

* Jerry Calà as Eddy
* Umberto Smaila as  Inspector Turroni
* Lucrezia Lante della Rovere as  Barbara
* Eva Grimaldi as  Porzia
* Mara Venier as  Sister Melania
* Marina Viro as Maria Rita aka "Mariri"
* Simonetta Gianfelici as   Ambra
* Nina Soldano as  Yoko 
* Silvia Annichiarico as The Department Director
* Novello Novelli as The Ventriloquist
* Alba Parietti as  Patty Pravo

==Plot==
Eddy, a detective who works as a security guard in a department store, is engaged with Barbara, who works as a clerk in the same store.

After receiving a perfume as a gift from a mysterious admirer, Barbara burns alive passing under a series of warm halogen spotlights. 

In order to discover the truth about the death of Barbara, Eddy decides to investigates together with Inspector Turroni.

Afterward a prostitute named Portia is murdered in similar circumstances and Eddy discovers the cause of the deaths: the perfume sent by the killer contains a highly flammable substance (benzopyrene-15).

He decides than to find the connection between the two women and he discovers two pieces of a picture that the two victims had.
The picture was taken years before, in 1971,  when the victims were just young girls.
They spent that summer together with a third girl, Mariri, and another mysterious girl at a holiday camp managed by Sister Melania.

The solution of the crimes is a terrible accident occurred during that summer.

 
==References==
 

==External links==
*  

 
 
 
 
 
 


 
 