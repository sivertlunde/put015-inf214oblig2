Rajotto
{{Infobox film
| name           = Rajotto
| image          = Rajotto 2014.png
| caption        = Theatrical release poster
| director       = Iftakar Chowdhury
| producer       = Fatman Films
| writer         = Abdullah Johir
| story          = 
| based on       =  Boby
| music          = Adit Ozbert Aman Ahmed
| cinematography = 
| editing        = 
| studio         = Fatman Films
| distributor    = Fatman Films
| released       =  
| runtime        =  2:24:45
| country        = Bangladesh Bengali
| budget         = 
| gross          = 
}} crime action Telugu fim, Pokiri (2006 film)|Pokiri starring Mahesh Babu directed by Puri Jagannadh.

==Plot==
Somrat is a killer for hire, and will accept any contract provided the amount is right. The Police want him for questioning, while the underworld needs him to silence their opponents. No one really knows this assassins background. It is only Somrat who knows his real identity and he has no intention of sharing it with anyone.

==Cast==
* Shakib Khan as Somrat Bobby as Rihhana
* Pribir Mitro
* Rothun
* Arefin Iqbal

==Music==
{{Infobox album
| Name = Rajotto
| Type = Soundtrack
| Artist =Adit
| Cover = 
| Border =
| Alt =
| Caption =
| Released = 
| Recorded = 2013/14
| Genre = Film soundtrack
| Length = 23:33 Bengali
| Label = Eagle Music
| Producer =
}}
The soundtrack of Rajotto composed by Ozbert with the lyrics penned by Shohel Arman & Jibon. The soundtrack features 5 tracks overall. The song "Moner Majhe" was released as a promotional single on 12 November 2013.   the video of the song "Tomake Valobashi" was released on 2 March 2014.
{{track listing
|headline=Rajotto Album: Track listing
|extra_column=Singer(s)
|music_credits=no
|lyrics_credits=no
|total_length= 23:33
|title1=Tumi Chara
|extra1=Hasib & Dola
|music1=Adit
|length1=4:40
|title2=Tomake Valobashi
|extra2=Hasib & Kona
|music2=Adit
|length2=5:09
| title3         = Jeo Na chole
| extra3         = Hasib & Eva
| music3         = Adit
| length3        = 5:10
| title4         = Jouboti Konna
| extra4         = Dola
| music4         = Adit
| length4        = 4:16
| title5         = Choya
| extra5         = Elita
| music5         = Adit
| length5        = 4:18
}}

==Marketing==
The first look of Rajotto was revealed on November 12, 2013. A One Minute five second promo teaser was released on 13 January 2014.

===Inspirations===
Last ten minutes of this movie inspired from the Telugu Movie Pokiri (2006 film)|Pokiri.

==Remakes== Telugu film Hindi version Kannada version, Porkhi, directed by M.D. Sridhar, was released in 14 January 2010.  Below is an actor map of the lead actors in the story of Pokiri (2006 film)|Pokiri and its remakes.
{| class="wikitable" style="width:50%;"
|- style="background:#ccc; text-align:center;"
| Pokiri (2006 film)|Pokiri (Telugu language|Telugu) (2006) || Pokkiri (Tamil language|Tamil) (2007) || Wanted (2009 film)|Wanted (Hindi language|Hindi) (2009) || Porki (Kannada language|Kannada) (2010)|| Rajotto (Bengali language|Bengali) (2014)
|- Vijay || Shakib Khan
|- Bobby (Bangladeshi Boby
|- Ilish Kobra
|-
| Nassar || Nassar || Vinod Khanna || Avinash|| Pribir Mitro
|- Napoleon || Sanko Panja
|- Rothun
|- Arefin Iqbal
|}

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 