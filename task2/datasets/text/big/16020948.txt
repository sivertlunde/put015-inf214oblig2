Daisy Miller (film)
{{Infobox film
| name           = Daisy Miller
| image          = DaisyMillerPoster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Peter Bogdanovich
| producer       = Peter Bogdanovich
| screenplay     = Frederic Raphael
| based on       = The  
| narrator       = Barry Brown Cloris Leachman Mildred Natwick Eileen Brennan
| music          = Angelo Francesco Lavagnino
| cinematography = Alberto Spagnoli
| editing        = Verna Fields
| studio = The Directors Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         = $2.2 million 
}} American drama 1878 novella John Furniss. 

==Plot synopsis== xenophobic younger brother Randolph. There she meets upper class expatriate American Frederick Winterbourne, who is warned about her reckless ways with men by his dowager aunt Mrs. Costello.
 Italian Mr. Giovanelli, who has no status among the locals, will destroy her reputation with the expatriates, including socialite Mrs. Walker, who is offended by her behavior and vocal about her disapproval. Daisy is too carelessly naive to take either of them seriously.

Winterbourne is torn between his feelings for Daisy and his respect for social customs, and he is unable to tell how she really feels about him beneath her facade of willful abandon. When he meets her and Giovanelli in the Colosseum one night, he decides such behavior makes him unable to love her and lets her know it. Winterbourne warns her against the malaria,against which she has failed to take precautions. She becomes ill, and dies a few days later. At her funeral,Giovanelli tells Winterbourne that she was the most "innocent".  Winterbourne wonders whether his ignorance of American customs may have contributed to her fate.

==Principal cast==
*Cybill Shepherd ..... Daisy Miller  Barry Brown ..... Frederick Winterbourne 
*Cloris Leachman ..... Mrs. Ezra Miller 
*Mildred Natwick ..... Mrs. Costello 
*Eileen Brennan ..... Mrs. Walker 
*James McMurtry ..... Randolph Miller
*Duilio Del Prete ..... Mr. Giovanelli 
==Production notes==
Peter Bogdoanovich had a production deal with The Directors Company at Paramount Studios under which he could make whatever film he wanted provided it was under a certain budget. This company was the idea of Charles Bludhorn, chairman of Gulf and Western, who owned Paramount at the time. Peter Bart, then working at Paramount, remembers:
 Bogdanovich called me soon after completing Paper Moon to tell me he was going to introduce me to a filmmaker whose work the company should next foster. He appeared a day later in the presence of Orson Welles, corpulent and glowering, who, at the time, was neither young nor promising... Bogdanovich felt Welles had one more Citizen Kane in him; the other directors disagreed, as did I. Welles and Bogdanovich had formed a bond, however, and during their lengthy conversations, Welles had spoken glowingly of a novel by Henry James called Daisy Miller, which he felt was a romantic classic. Bogdanovich, who was making a habit at the time of falling in love, heard Welles comments in the context of a potential film. My instinct was that he was simply urging Bogdanovich to read the novel; an erudite man, Welles literary recommendations were definitely worth listening to. To my surprise, however, Bogdanovich instantly started prepping a movie based on Daisy Miller to star his girlfriend Cybill Shepherd -- an idea that did not stir much enthusiasm within the Directors Company. At the time, I recall telling myself, this company wont be around for long. The prediction proved to be correct.<ref name=
"bart">  accessed 16 April 2013  
Bogdanovichs other partners in the Directors Company were   of Paramount, who never liked the idea of the Directors Company and wanted it to fail. 

Bogdanovich later said he asked Orson Welles to direct Cybill Shepherd and Bogdanovoch in the lead roles but Welles refused:
 He encouraged me to do it which maybe was a double-edged sword but anyway Barry Brown was so right for the part that it was scary. But it was also a problem because he just wasnt very personable and the part needed somebody with a little more personality, but yknow, he was the part, he sure was Winterbourne. Poor Barry. He killed himself really, with booze... He had a kind of intelligence and he was a very bright kid but he was so self-destructive. But he was very much like Winterbourne, he was definitely "winter born."  
The film started shooting on 20 August 1973.  Production took place on location in Rome and Vevey in the canton of Vaud in Switzerland. Larry McMurtrys son had a support role.  During the shoot Rex Reed visited the set and wrote a hostile piece on the director and star. Cybill Shepherd: Henry James Had Me in Mind
By Rex Reed. The Washington Post, Times Herald (1959-1973)   14 Oct 1973: L5.  

Bogdanovich later recalls his partners at the Directors Company were not happy with the film:
 They thought it was a kind of a vanity production to show Cybill off. If Id wanted to do that I would have done something else. That was a pretty difficult role, and I thought she was awfully good in it. What some people didnt realize is that that was the way a girl like that would have been in 1875. She was from New York, she was a provincial girl. If you read the story thats what she is. If you read the original novel we hardly added anything. The movie is exactly the book. I added one sequence that I wrote that Freddy Raphael had nothing to do with. In fact Freddy Raphael had nothing to do with that script, it was so funny. Theres two things he wrote. One idea was the little miniature painter and the other thing was having that scene play in the baths... Everything else was the book and I couldnt use his script cause it was really way over the top. Anyway, thats another story. We went to arbitration in England cause Freddys English and so they were a little partial to him. They said I could have billing but it would have to say "Additional Dialogue by", and I said Im not going to give myself that.   
When the film was completed Bogdanovich recalls showing it to studio executives:
 Yablans, the new head of the studio came over to me and I said "What do you think?" He said, "Its alright." I said "Is that all you have to say?" "Well what do you want me to say?" I said, "Its just alright?" He said, "Its fine, its good but you are Babe Ruth and you just bunted." From a commercial point of view he was right. It was not a picture that was ever going to be a big hit unless you released it today. It got very good notices. People remember it as having gotten bad notices but the truth is that Paper Moon got fairly mixed notices. The New York Times didnt like it, Time didnt like it. On the other hand The New York Times raved about Daisy Miller, but it was just not a commercial picture in its day plus at that point Paramount changed hands, Barry Diller came in, Frank was out, it fell between the cracks, and nobody really pushed it. I like the picture. I think it was pretty daring.   accessed 3 June 2013  

==Critical reception==
Variety (magazine)|Variety described the film as "a dud" and added, "Cybill Shepherd is miscast in the title role. Frederic Raphaels adaptation of the Henry James story doesnt play. The period production by Peter Bogdanovich is handsome. But his direction and concept seem uncertain and fumbled. Supporting performances by Mildred Natwick, Eileen Brennan and Cloris Leachman are, respectively, excellent, outstanding, and good." 

The New York Times said the movie "works amazingly well."  It congratulated Shepherd for   "the gaiety and the directness of Daisy, the spontaneity of a spoiled but very likable person. She also manages to be thoughtless without playing dumb or dizzy, and to convey that mixture of recklessness and innocence that bewildered the other Jamesian characters."  Bogdanovich was praised for providing "a sensitive glimpse of the hypocrisies and contradictions of the past—without one whiff of nostalgia."  (Vincent Canby later named it one of the eleven best films of the year. FILM VIEW: Critics Choice: The Eleven Best Films of 1974 FILM VIEW The Best Films of 1974
Canby, Vincent. New York Times (1923-Current file)   05 Jan 1975: D1. )

TV Guide rates it one out of a possible four stars and calls it "truly a dud in spite of handsome sets and an intelligent writing job. James is, to say the least, hard to adapt for the screen, but this job becomes hopeless because of Shepherds shallow performance." 

Time Out London says, "Bogdanovichs nervous essay in the troubled waters of Henry James, where American innocence and naiveté are in perpetual conflict with European decadence and charm, reveals him to be less an interpreter of James than a translator of him into the brusquer world of Howard Hawks. The violence done James in this is forgivable—indeed, Cybill Shepherds transformation of Daisy into a Hawks heroine is strangely successful—but as a result there is no real social conflict in the film, and it becomes just a period variant on The Last Picture Show, without the vigour of that film or the irony of the original James novel." 

The original edition of The New York Times Guide to the Best 1,000 Movies Ever Made, published in 1999, included the film,  but the second edition published in 2004 deleted it from its list. 
==Awards and nominations== Academy Award The Great Gatsby.

==References==
 
==External links==
*  at the Internet Movie Database
*  at Rotten Tomatoes
 

 
 
 
 
 
 
 
 
 
 
 