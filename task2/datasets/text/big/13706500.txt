Mabel's Dramatic Career
 
{{Infobox film
| name           = Mabels Dramatic Career
| image          = Mabels Dramatic Career 1913.jpeg
| caption        = Mabel Normand and Mack Sennett in the film. Image courtesy Orange County Archives.
| director       = Mack Sennett
| producer       = Mack Sennett
| writer         =
| starring       = Mabel Normand Mack Sennett Ford Sterling
| cinematography =
| editing        =
| distributor    = Keystone Studios
| released       =  
| runtime        = 14 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 short comedy film within a film and uses multiple exposure to show a film being projected in a cinema.

==Cast==
* Mabel Normand - Mabel, the kitchen maid
* Mack Sennett - Mack
* Alice Davenport - Macks mother
* Virginia Kirtley - Mabels rival
* Charles Avery - Farmer
* Ford Sterling - Actor / Onscreen villain
* Roscoe Arbuckle - Man in cinema audience
* Billy Jacobs - Mabels son (as Paul Jacobs)

==See also==
* List of American films of 1913
* Fatty Arbuckle filmography

==References==
 

==External links==
* 
*   on YouTube

* 

 
 
 
 
 
 
 
 
 
 