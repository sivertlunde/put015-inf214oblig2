Sarala (film)
{{Infobox film
| name           = Sarala
| image          = 
| image_size     = 
| caption        = 
| director       = Premankur Atorthy
| producer       = Imperial Film Company
| writer         = Premankur Atorthy
| narrator       = 
| starring       = Rattan Bai Kumar Ahindra Choudhury Pramila 
| music          = H. C. Bali
| cinematography = Rustam Irani
| editing        = 
| distributor    =
| studio         = Imperial Film Company
| released       = 1936
| runtime        = 157 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1936 Hindi socio-family film directed by Premankur Atorthy.    The film was produced by Imperial Film Company. The director of photography was Rustam Irani with music by H. C. Bali.    It starred Rattan Bai, Kumar (actor)|Kumar,  Hafisji, Pramila, Anant Marathe and Ahindra Choudhury.   

==Cast==
*Rattan Bai Kumar
*Hafisji
*Pramila
Ahindra Choudhury
*Anant Marathe
*Baba Vyas
*Asooji
*Chemist
*Jilloobai  

==Songs==
Songlist   
*Apna Hi Tujhko Vichar
*Man Mein Udne Ko Kyun Soche
*Hato Jao Hame Na Satao
*Prabhu Tum Aao Daras Dikhao
*Piya Ke Milne Ki Aas
*Prabhu Mori Naiya Padi Majhdhar
*Na Kisiki Ankh Ka Nur Hoon
* Neha Barse Rhumjhum Rhumjhum
*Gham-E-Dil Kis Se Kahoon

==References==
 

==External links==
* 

 

 
 
 
 

 