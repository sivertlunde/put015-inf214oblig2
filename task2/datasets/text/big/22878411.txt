Crack in the Mirror
{{Infobox film
| name           = Crack in the Mirror
| image_size     =
| image	         = Crack in the Mirror FilmPoster.jpeg
| caption        =
| director       = Richard Fleischer
| producer       = Darryl F. Zanuck
| writer         = Darryl F. Zanuck
| narrator       =
| starring       = Orson Welles Juliette Gréco Bradford Dillman
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,000,000 (US/ Canada)  
}}
Crack in the Mirror is a 1960 drama film directed by Richard Fleischer. The three principal actors, Orson Welles, Juliette Gréco, and Bradford Dillman, play dual roles in two interconnected stories as the participants in two love triangles. 

The script was ostensibly written by producer Darryl F. Zanuck (under his frequent pseudonym "Mark Canfield"), but in his 1993 autobiography Just Tell Me When to Cry, Richard Fleischer revealed that it was in fact ghost-written by Jules Dassin, who was unable to work openly in the American film industry at the time, because he was on the Hollywood blacklist.

==Plot==
In a rundown Paris dwelling, an angry Hagolin accuses mistress Eponine of seeing a man named Larnier behind his back. In a party at a stately home, meanwhile, prosperous attorney Lamercieres guests include his longtime mistress, Florence, and his young law partner, Claude.

Eponine wants to murder Hagolin and attempts to, but fails. Larnier intervenes on her behalf, but merely wanted to gag Hagolin with a scarf before Eponine strangles the man with it. The body is dismembered and dumped, then Eponine is placed under arrest.

Claude, who is secretly Florences lover, feels he deserves credit for much of Lamercieres courtroom success. He leaps at the opportunity when Eponine asks him to defend her. Lamerciere caustically remarks that Claude and Florence could do to him exactly what the accused woman and lover Larnier did to their victim Hagolin.

In court, Lamerciere manages to persuade Claude to let him make the closing argument. He paints such a lurid picture of Eponines crime that it gets her convicted. His gaze at Florence makes it clear that he knows she has been unfaithful.

==Cast==
*Orson Welles as Hagolin/Lamerciere
*Juliette Gréco as Eponine/Florence
*Bradford Dillman as Larnier/Claude
*Alexander Knox as President
*Catherine Lacey as Mother Superior William Lucas as Kerstner
*Maurice Teynac as Doctor
*Austin Willis as Hurtelaut
*Cec Linder as Murzeau
*Eugene Deckers as Magre

==Reception== 20th Century-Fox Film Corporation, observed that "when I won three prizes for a very second-rate film called Crack in the Mirror," at the Cannes Film Festival, " his dubious victory was achieved by the political activities of a group of friends who accompanied me to the festival (Orson Welles, Juliette Greco and Françoise Sagan)." 

==References==
 

==External links==
 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 

 

 