Carolina Moon (1940 film)
{{Infobox film
| name           = Carolina Moon
| image          = Carolina_Moon_1940_Poster.jpg
| border         = yes
| caption        = Theatrical release poster Frank McDonald
| producer       = William Berke
| screenplay     = Winston Miller
| story          = Connie Lee
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* June Storey
}}
| music          = Raoul Kraushaar (supervisor)
| cinematography = William Nobles
| editing        = Tony Martinelli
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 65 minutes Magers 2007, p. 167. 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Western film Frank McDonald and starring Gene Autry, Smiley Burnette, and June Storey.  Based on a story by Connie Lee, the film is about a singing cowboy who comes to the aid of plantation owners who are being robbed of their land by a scheming lumber company. Magers 2007, p. 168.    

==Plot==
While riding the rodeo circuit, singing cowboy Gene Autry (Gene Autry) and his sidekick Frog Millhouse (Smiley Burnette) meet Southerners Caroline Stanhope (June Storey) and her grandfather (Eddy Waller). The Stanhopes are hoping to win prize money in order to pay back taxes on their plantation. Their hopes are shattered when Carolines horse Valdena is injured in an accident and unable to compete in the rodeo. With no hope of winning the prize money, Stanhope gets involved in a card game and loses $1,000 to some gamblers. With no money, he offers to give them Valdena, his only asset, but the gamblers refuse.

After the rodeo, Gene gives the old man his winnings for Valdena so he can settle his debt with the gamblers. Caroline is suspicious of Genes generosity and thinks he is trying to cheat her grandfather. Later, after her grandfather gets drunk, Caroline packs him and the horse into their car and drives back home. When Gene discovers that theyve gone, he follows them, intending to protect his investment. When he arrives at the Stanhope plantation, he learns that the Stanhopes and their neighbors are in danger of losing their land for delinquent back taxes.
 Robert Fiske) is scheming to cheat the plantation owners out of their land. Hes elicited the help of Henry Wheeler (Hardie Albright), the only prosperous plantation owner in the county.

In an effort to help the plantation owners pay their back taxes, Gene enters Valdena in a steeplejack race. After Wheeler deliberately fouls the horse during the race, Gene becomes suspicious of his motives, eventually learning the truth. Gene convinces the plantation owners to harvst and sell the lumber themselves, but Wheeler and Barrett thwart their efforts by holding up the lumberjacks so they cannot meet the terms of their contracts. Undaunted, Gene sends for his cowboy friends, who arrive and do the lumberjacking themselves, saving the plantations. 

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse
* June Storey as Caroline Stanhope Mary Lee as Patsy Stanhope
* Eddy Waller as Grandfather Stanhope
* Hardie Albright as Henry Wheeler
* Frank Dae as Col. Jefferson
* Terry Nibert as Evangeline Jefferson Robert Fiske as Barrett
* Etta McDaniel as Mammy
* Paul White as Billy
* Fred Ritter as Thompson
* Ralph Sanford as Foreman Nelson
* Texas Jim Lewis and His Lone Star Cowboys as Musicians
* Chuck Baldra as Rodeo Rider (uncredited)
* Don Brodie as Rodeo Gambler (uncredited)
* Fred Burns as Rodeo Official (uncredited)
* Bob Card as Rodeo Rider (uncredited)
* Burr Caruth as Pool Player (uncredited) Carol Hughes as Woman at the breakfast (uncredited) Fred Snowflake Toones as Sam, First Wheeler Butler (uncredited)
* Champion as Genes Horse (uncredited)    

==Production==
===Filming and budget===
Carolina Moon was filmed May 20 to June 4, 1940. The film had an operating budget of $77,711 (equal to $ }} today), and a negative cost of $77,991.55. 

===Stuntwork===
* Joe Yrigoyen (Gene Autrys stunt double)
* Jack Kirk (Smiley Burnettes stunt double)
* Nellie Walker (June Storeys stunt double)   

===Filming locations===
* Corriganville, Ray Corrigan Ranch, Simi Valley, California, USA 
* Big Bear Valley, San Bernardino National Forest, California, USA
* Keen Camp, San Bernardino National Forest, Mountain Center, California, USA 
* Riviera Country Club, Los Angeles, California, USA
* Elysian Park, Los Angeles, California, USA (race track and rodeo grounds)
* Encino, California (home)    

===Soundtrack===
* "Carolina Moon" (Joseph Burke, Benny Davis) by Gene Autry, Smiley Burnette, Mary Lee, and others
* "Oh, Dem Golden Slippers!" (James Allen Bland) by Paul White on harmonica
* "(I Wish I Was in) Dixies Land" (Daniel Decatur Emmett) by Paul White on harmonica
* "At the Rodeo" (Gene Autry, Johnny Marvin, Harry Tobias) by Gene Autry, Smiley Burnette, and Texas Jim Lewis and His Lone Star Cowboys
* "Me and My Echo" (Connie Lee) by Mary Lee and Smiley Burnette
* "Old Folks at Home (Swanee River)" (Stephen Foster) by the negroes at the plantation
* "Dreaming Dreams That Wont Come True" (Gene Autry, Johnny Marvin, Harry Tobias) by Gene Autry and June Storey
* "Say Si Si" (Ernesto Lecuona, Francia Luban, Albert Stillman) by Gene Autry, June Storey, and Mary Lee
* "Climbin Up DMountain" (Traditional) by the negroes at the plantation    

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 