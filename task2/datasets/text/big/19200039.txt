Boogie Man: The Lee Atwater Story
{{multiple issues|
 
 
 }}
{{Infobox film
|name=Boogie Man: The Lee Atwater Story
|image=Boogie Man Promotional Poster.jpg
|director=Stefan Forbes starring = Eric Alterman
|producers=Stefan Forbes, Noland Walker
|distributor=Interpositive Media LLC
|released=September 26, 2008
|runtime=86 mins
|language=English
|country=United States}}
 1988 presidential campaign, and how those tactics have transformed presidential campaigns in the United States.
 Democratic and Republican National Conventions, played 40 American cities in the fall of 2008 and was #7 in nationwide per-screen average the weekend of its release. It has been called one of the best political documentaries ever made .

==Critical reception==
The film won the national Edward R. Murrow Award, a 2009 Polk Award for Excellence in Journalism, won the Chris Award at the Columbus International Film & Video Festival, was nominated for the WGA Award for Best Theatrical Documentary, and director Stefan Forbes received the Emerging Filmmaker Award from the International Documentary Association. Conservatives have charged the film contains Left-wing politics|left-wing bias. 

==Other releases== SVT (the national public service broadcaster), the film was retitled Amerikas ondaste man ("Americas Evilest Man"). 
 PBS series Frontline (U.S. TV series)|Frontline broadcast a slightly shortened version of the documentary.

==References==
 

==External links==
;General
*  
*  
*   at Film-Forward.com
*  

;Reviews
*   by Joe Conason in The New York Observer
*   by Roger Ebert

;Interviews with film director Stefan Forbes
*  
*   on MSNBCs Morning Joe
*   by Elvis Mitchell
*   by Laura Flanders

 
 
 
 