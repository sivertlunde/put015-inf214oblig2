Fort Bowie (film)
{{Infobox film
| name           = Fort Bowie
| image          = Fort Bowie poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Howard W. Koch
| producer       = Aubrey Schenck	
| screenplay     = Maurice Tombragel  Ben Johnson Jan Harrison Kent Taylor Maureen Hingert Peter Mamakos Larry Chance
| music          = Les Baxter
| cinematography = Carl E. Guthrie
| editing        = John A. Bushelman 
| studio         = Aubrey Schenck Productions Bel-Air Productions
| distributor    = United Artists
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film Ben Johnson, Jan Harrison, Kent Taylor, Maureen Hingert, Peter Mamakos and Larry Chance. The film was released on February 1, 1958, by United Artists.  

==Plot==
 

== Cast ==	 Ben Johnson as Capt. Thomas Thompson
*Jan Harrison as Alison Garrett
*Kent Taylor as Col. James Garrett
*Maureen Hingert as Chanzana
*Peter Mamakos as Sgt Kukas
*Larry Chance as Victorio
*J. Ian Douglas as Maj. Wharton
*Jerry Frank as Capt. Maywood
*Barbara Parry as Mrs. Maywood 

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 