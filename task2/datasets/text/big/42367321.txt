Piku
 
 
 
{{Infobox film
| name           = Piku
| image          = First Look Poster.jpg
| alt            = 
| caption        = Theatrical Release Poster
| director       = Shoojit Sircar
| producer       = N.P. Singh Ronnie Lahiri Sneha Rajani
| writer         = Juhi Chaturvedi
| screenplay  = Juhi Chaturvedi
| starring       = Deepika Padukone Amitabh Bachchan Irrfan Khan
| music          = Anupam Roy
| cinematography = Kamaljeet Negi
| editing        = Chandrashekhar Prajapati MSM Motion Pictures Saraswati Entertainment Creations Limited Rising Sun Films
| distributor    = Yash Raj Films
| released       =  
| runtime        = 135 minutes
| country        = India Hindi 
| budget         =
| gross          =
}}
 Indian comedy-drama film directed by Shoojit Sircar. Deepika Padukone portrays the titular protagonist, a Bengali architect living in New Delhi, and Amitabh Bachchan plays her ageing father. The film explores the dynamics of their relationship as they take a road trip to Kolkata.  Irrfan Khan, Moushumi Chatterjee and Jishu Sengupta portray supporting roles. Piku is scheduled for release on May 8, 2015. 

==Cast==
* Deepika Padukone as Piku Banerjee   
* Amitabh Bachchan as Bhashkor Banerjee
* Irrfan Khan as Rana Chaudhary
* Moushumi Chatterjee
* Raghuvir Yadav
* Jishu Sengupta
* Aniruddha Roy Chowdhury
* Akshay Oberoi   

==Production==
===Casting=== Sengupta will in fact be seen playing Deepika Padukone|Padukones best friend in the film.   Irrfan Khan will play the romantic lead opposite of Deepika Padukone.   Amitabh Bachchan will essay the role of a Bangali bhadrolok, while Moushumi Chatterjee will essay the role of a family member.    Akshay Oberoi will play a cameo role going on a date with Padukones character. 

=== Filming ===

Principal photography for Piku began in August 2014, when shooting of the films first schedule took off in Kolkata.  The first schedule for Piku was also filmed in Delhi and Mumbai and included filming of indoor scenes.        The second schedule of filming started on 30 October 2014 in Kolkata and took place mostly in the north part of the city.      Specifically, shooting for the film took place at the iconic Howrah Bridge in the citys Shyambazar neighbourhood and at Bishop Lefroy Road, the residence of Satyajit Ray.    During the shooting at Howrah Bridge, Bachchan was seen cycling around the city streets dressed as his character.  The Kolkata schedule was completed on 18 November 2014 and shooting has now shifted to Delhi and Patdi Near Surendranagar in Gujarat.   Shooting in Delhi took place at Gurgaon cyber hub and city club where Padukone and Khan were seen filming for the film.  The film was wrapped up after its last schedule being shot in Varanasi. Both Padukone and Khan shot on the ghats of Banaras under chilly conditions and also attracted a crowd on the ghats of Banaras. 

===Promotion===
The makers of the film released a video of day one on the sets of Piku which is called Piku Begins. The video has the cast speak about what viewers can expect from the film.  As a part of promotions, the makers have released onset pictures of the films shooting where the main cast are all seen in character.  Bachchans first look was revealed from the film that depicts his character of an old Bengali man with long hair and a big belly.  On 25 March 2015, as part of the promotion, Deepika revealed the poster of the film through her twitter handle. 

==Soundtrack==
  composed the soundtrack for the film.]]
{{Infobox album
| Name = Piku
| Type = Soundtrack
| Artist = Anupam Roy
| Cover = 
| Released =   Feature film soundtrack
| Length =   Zee Music Company
}}
The soundtrack is composed entirely by Anupam Roy, the lyrics are also written by Roy. The first song, "Journey Song", was released on 1 April 2015. The official music album was released online on April 21, 2015. 

{{track listing
| extra_column = Singer(s)
| total_length =  
| title1 = Bezubaan
| extra1 = Anupam Roy
| length1 = 05:37
| title2 = Journey Song
| extra2 = Anupam Roy, Shreya Ghoshal
| length2 = 04:12
| title3 = Lamhe Guzar Gaye
| extra3 = Anupam Roy
| length3 = 04:16
| title4 = Piku
| extra4 = Sunidhi Chauhan
| length4 = 03:26
| title5 = Teri Meri Baatein
| extra5 = Anupam Roy
| length5 = 05:39
| title6 = Piku Remix (DJ AKS)
 
| extra6 = Sunidhi Chauhan
| length6 = 02:00

}}

==References==
 



==External links==
*  
*  

 

 
 
 
 
 
 
 