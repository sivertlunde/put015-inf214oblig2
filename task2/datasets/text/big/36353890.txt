The I Don't Care Girl
{{Infobox film
| name           = The I Dont Care Girl
| image          = 
| caption        = 
| director       = Lloyd Bacon George Jessel
| writer         = Walter Bullock George Jessel
| starring       = Mitzi Gaynor Oscar Levant David Wayne
| music          = Herbert W. Spencer
| cinematography = Arthur E. Arling
| editing        = Louis R. Loeffler
| distributor    = 20th Century Fox
| released       =  
| runtime        = 78 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $1.25 million (US) 
}}

The I Dont Care Girl is a 1953 Technicolor film starring Mitzi Gaynor. It is a biography of an entertainer, Eva Tanguay. 

==Plot== George Jessel.

Her former partner Eddie McCoy (David Wayne) tells how they met. Recently widowed, he discovered Eva as a waitress, hearing her sing and offering her a job after shes fired. Eva falls for singer Larry Woods (Bob Graham), although piano player Charles Bennett (Oscar Levant) also has eyes for her. Eva is offended and sets out on her own when she finds out that Larry is married.

Bennett is found by the writers and claims Eddies story is untrue. Eva was already singing in a cafe when she and Eddie first met. Unable to get Eddie to sober up, she breaks up their act and is discovered by Florenz Ziegfeld, who signs Eva for his famed Follies.

She learns that Larrys marriage is on the rocks, but is put off when the leading role in Larrys new operetta is apparently going to Stella (Hazel Brooks), another singer. Eva hires someone to throw tomatoes at Larry on stage, unaware that when he steps out to perform, Larry, having enlisted to fight in the war, will be wearing his Army uniform. Evas prank backfires and she is disconsolate for quite a while, but in the end, Larry wins her back.

==Cast==
* Mitzi Gaynor as Eva Tanguay
* Oscar Levant as Charles Bennett
* David Wayne as Eddie McCoy
* Bob Graham as Larry Woods
* Hazel Brooks as Stella Forrest
* Warren Stevens as Lawrence George Jessel as Himself

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 


 
 