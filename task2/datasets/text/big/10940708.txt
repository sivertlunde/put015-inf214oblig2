Earth vs. the Spider
 
{{Infobox film
 | name           = Earth vs. the Spider
 | image          = Spiderposter.jpg
 | caption        = Film poster by Albert Kallis
 | director       = Bert I. Gordon
 | producer       = Bert I. Gordon
 | writer         = László Görög (writer)|László Görög George Worthing Yates
 | starring       = Ed Kemmer June Kenney Eugene Persson Gene Roth Hal Torey June Jocelyn
 | music          = Albert Glasser
 | cinematography = Jack A. Marta
 | editing        = Walter E. Keller
 | distributor    = American International Pictures
 | released       =  
 | runtime        = 73 minutes
 | language       = English
 | country        = United States
 | budget         = $100,000 
}}
 1958 Cinema American black horror film produced and directed by Bert I. Gordon, who also wrote the story, upon which the screenplay by George Worthing Yates and Laszlo Gorog is based. It starred Ed Kemmer, Eugene Persson and June Kenney. The film was released by American International Pictures as a double feature with The Brain Eaters.

== Plot == web of Mexican redleg tarantula, which emerges from behind some rocks to get them. They manage to escape and make it back to town.

Carol and Mike have a hard time convincing the Sheriff about the giant spider, but with the help of their science teacher, Mr. Kingman, they go take a look and when they are at the cave again the missing mans body is discovered drained of fluid. The spider attacks again convincing the sheriff, who orders large amounts of DDT to kill the giant spider, and appears successful. The apparently lifeless body of the spider is taken back to town to the high school gym where Kingman wants to study it. A group of teenagers uses the gym to practice rock and roll numbers they are going to play for a school dance. As other teenagers enter the gym they begin dancing and the giant tarantula regains consciousness and the kids run out of the gym screaming while the janitor, stopping to call the sheriff, is killed.
 electrocute the spider. The arachnid falls, impaling itself on stalagmites at the bottom of the cave.

== Cast ==
*Ed Kemmer as Mr. Kingman
*June Kenney as Carol Flynn
*Eugene Persson as Mike Simpson
*Gene Roth as Sheriff Cagle
*Hal Torey as Mr. Simpson
*June Jocelyn as Mrs. Flynn Mickey Finn as Sam Haskel
*Sally Fraser as Mrs. Helen Kingman
*Troy Patterson as Joe Skip Young as Sam (the bass player)
*Howard Wright as Jake
*Bill Giorgio as Deputy Sheriff Sanders"
*Hank Patterson as Hugo (High School Janitor)
*Jack Kosslyn as Mr. Fraser (camera club teacher)
*Bob Garnet as Springdale Pest Control Man
*Shirley Falls as the Switchboard Operator
*Bob Tetrick as Deputy Sheriff Dave
*Nancy Kilgas as a Dancer George Stanley as One of the men in the Cavern
*David Tomack as the Power Line Foreman
*Merritt Stone as Jack Flynn (Carols Dad)
*Dick DAgostin as The Pianist

== Production == The Fly, also released in 1958, became a blockbuster, the film company changed the name to The Spider on all advertising material. The original screen title, however, was never changed.

Out front of the movie theater in which Mike works is a poster prominently advertising the film The Amazing Colossal Man, while the marquee shows that it is currently screening Attack of the Puppet People, which happens to also star June Kenney. Both of these movies were also directed by Bert I. Gordon. Attack of the Puppet People was the last film Bert I Gordon made for AIP for a number of years, with the director claiming the studio had not paid him appropriately. However, he returned to AIP in the 1970s. 

== Release ==
 
Earth vs. the Spider  was released on a double-bill with "The Brain Eaters".

=== Home media ===
The film was released on VHS by Columbia/RCA on April 28, 1993. The film was released on DVD in February 2006 along with War of the Colossal Beast (1958).

== Reception ==
=== Influence ===
Earth vs. the Spider was featured on the cult TV show Mystery Science Theater 3000 in season 3. A clip from the film can be briefly seen during the 2002 film "Lilo & Stitch" while Stitch is looking in an appliance store window.

== References ==
{{Reflist |refs= 
 
{{cite book
 | last1 = Arkoff
 | first1 = Samuel Z.
 | authorlink1 = Samuel Z. Arkoff
 | last2 = Trubo
 | first2 = Richard
 | title = Flying Through Hollywood by the Seat of My Pants: From the Man Who Brought You I Was a Teenage Werewolf and Muscle Beach Party
 | url = http://books.google.com/books?id=KX1ZAAAAMAAJ&q=Flying+Through+Hollywood+By+the+Seat+of+My+Pants&dq=Flying+Through+Hollywood+By+the+Seat+of+My+Pants&hl=en&sa=X&ei=hSiAUvumGPHd4APzwIDYCA&ved=0CCEQ6AEwAA
 | accessdate = November 11, 2013
 | edition = illustrated
 | date = July 1, 1992
 | publisher = Carol Publishing Group
 | location = Secaucus, New Jersey, USA
 | isbn = 9781559721073
 | oclc = 25372204
 | page = 213
}}
 

 
{{cite book
 | last = McGee
 | first = Mark Thomas
 | title = Faster and Furiouser: The Revised and Fattened Fable of American International Pictures
 | url = http://books.google.com/books?id=Z19ZAAAAMAAJ&q=The+Revised+and+Fattened+Fable+of+American+International+Pictures&dq=The+Revised+and+Fattened+Fable+of+American+International+Pictures&hl=en&sa=X&ei=Mi2AUuGNI-zj4AOk34DIAw&ved=0CCEQ6AEwAA
 | accessdate = November 11, 2013
 | edition = illustrated, revised
 | year = 1996
 | publisher = McFarland & Company
 | location = Jefferson, North Carolina, USA
 | isbn = 9780786401376
 | oclc = 33207391
 | page = 117
}}
 

}}

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 