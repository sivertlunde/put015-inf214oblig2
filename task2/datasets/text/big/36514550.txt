My Neighbor's Wife
 

{{Infobox film
| name           = My Neighbors Wife
| image          = My_Neigbors_Wife.jpg
| caption        = Theatrical movie poster
| director       = Jun Lana 
| producer       =  
| writer         = Jun Lana Denoy Punio
| starring       =  
| music          = Jesse Lucas
| cinematography = Mo Zee
| screenplay     = 
| editing        = Tara Illenberger	 	
| distributor    = Regal Films
| released       =  
| runtime        = 
| country        = Philippines
| language       = Filipino English
| budget         =
| gross          = P 27,628,300 
}}

My Neighbors Wife is a 2011 Filipino drama film directed by Jun Lana,    starring Dennis Trillo, Lovi Poe, Jake Cuenca, and Carla Abellana.  The film premiered nationwide last September 14, 2011 under the distribution of Regal Films.

==Cast==
*Dennis Trillo as Aaron
*Lovi Poe as Giselle
*Jake Cuenca as Bullet Bernal
*Carla Abellana as Jasmine Bernal
*Dimples Romana as Tessa
*Yogo Singh as Timothy Tommy Bernal
*Timmy Cruz as Jasmines Mom

===Cameos===
*Katrina Halili	
*Princess Manzon	
*Alvin Fortuna	
*Cara Eriguel as Roxanne
*Shey Bustamante	
*April Sun	
*Jovic Susim	
*Aries Go	
*Isaiah Ersty

==Awards & Nominations==
2012 28th PMPC Star Awards for Movies Movie of the Year - Nominated
Movie Actress of the Year for (Lovi Poe) - Nominated

2012 FAP 30th Luna Awards Best Actress (Lovi Poe) - Nominated
2012 FAP 30th Luna Awards Best Supporting Actress (Carla Abellana) - Nominated
Best Cinematography (Mo Zee) - Nominated
 Best Editing (Tara Illenberger) - Nominated
Best Musical Scorer (Jesse Lucas) - Nominated
Best Sound Lamberto Casas Jr.) - Nominated

==See also== In the Name of Love
* No Other Woman The Mistress
* A Secret Affair One More Try
* Seduction (2013 film)|Seduction
* The Bride and the Lover
* When the Love Is Gone Trophy Wife
* Once a Princess The Gifted The Trial

==References==
 

== External links ==
*  
*   at Box Office Mojo

 
 
 
 