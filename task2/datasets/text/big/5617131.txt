Perfect Day (1929 film)
{{Infobox film
  | name = Perfect Day
  | image = L&H_A_Perfect_Day_1929_b&w.jpg
  | caption = 
  | director =  James Parrott
  | producer = Hal Roach
  | writer = Leo McCarey (story)   Hal Roach (story)   H.M. Walker
  | starring = Stan Laurel   Oliver Hardy   Edgar Kennedy   Kay Deslys   Isabelle Keith
  | music = William Axt   S. Williams   Leroy Shield   (1937 reissue)
  | cinematography = Art Lloyd   George Stevens
  | editing = Richard C. Currier
  | distributor = Metro-Goldwyn-Mayer
  | country = United States
  | released =  
  | runtime = 19 42"
  | language = English
}}
 
Perfect Day is a 1929 short comedy film starring Laurel and Hardy.

==Plot==
Two families embark on a pleasant Sunday picnic in their Ford Model T, but manage to run into a variety of issues with the temperamental automobile. Each incident requires repeated exits and reboardings by Laurel, Hardy, their wives and grouchy, gout-ridden Uncle Edgar. A brick-throwing argument with a neighbor threatens to escalate into an all-out turf war until the local parson gets involved. The families manage to finally get their day underway, only to plunge neck-deep into a seemingly shallow, water-filled pothole.

==Production notes==
Perfect Day was written in May 1929 and filmed between June 1–8, 1929.    The original 1929 release of Perfect Day contained no music other than that used over the opening credits. The Roach Studios would reissue the film in 1937 with an added music score being utilized at the time in other Roach comedies. The 1929 version was considered lost until the 2011 DVD release Laurel and Hardy: The Essential Collection, when the original Vitaphone disc track sans the incidental music became available. 

Adding the soundtrack in 1937 to the existing film resulted in a slight reduction of the correct frame ratio: several scenes feature a slightly cropped picture at the top and left hand sides to allow for inclusion of the soundtrack strip. 

The script for Perfect Day originally concluded with the family partaking in their picnic, but this was discarded when the extended gags centering on the troublesome Model T provided enough comic material to sustain the entire film. 

Perfect Day was also filmed outdoors, which freed it from the stagebound claustrophobia common to many early talkies. The opening scene is the only one set indoors (the sound of whirring cameras can be heard in some shots), while the exterior sound recording was technically impressive during an era of filmmaking when most actors had to stand close to the overhead microphone.  The live outdoor recording also revealed the improvisatory nature of most early Laurel and Hardy. A seated Edgar Kennedy manages to ad lib "Oh, shit!" , which escaped the scrutiny of movie and television censors. Bann, Richard W. Liner notes for 2011 DVD release Laurel and Hardy: The Essential Collection 

Despite the fact that the film industry was still adjusting to the making talking pictures, Laurel and Hardy mastered the new technology early on; the overall excellence and high reputation of Perfect Day bears testimony to the teams fruitful use of the new medium. Using sound effects to punctuate a visual gag — a technique The Three Stooges would build their entire film career around — was still in its infancy in 1929. The loud, ringing CLANG heard when Stan is beaned on the head with the Model Ts clutch would be termed by a 1929 film reviewer as "the funniest effect so far heard in a comedy."  The second half of the Stooges 1948 film Pardon My Clutch and its remake, Wham Bam Slam, is a remake of Perfect Day. 

==Cast==
*Stan Laurel - Stanley
*Oliver Hardy - Ollie
*Edgar Kennedy - Uncle Edgar
*Kay Deslys - Mrs. Hardy
*Isabelle Keith - Mrs. Laurel
*Baldwin Cooke - irate neighbor
*Lyle Tayo - irate neighbors wife
*Harry Bernard - neighbor across the street
*Clara Guiol - wife of neighbor across the street
*Charley Rogers as Minister

==1937 Reissue Soundtrack==
*"Ku-Ku" (Marvin Hatley)
*"Were Just a Happy Family" (Leroy Shield)
*"Lets Face It" (Shield)
*"Were Out for Fun" (Shield)
*"Carefree" (Shield)
*"Up in Room 14" (Shield)
*"The Laurel and Hardy Waltz" (Nathaniel Shilkret)
*"Up in Room 14" (reprise) (Shield)
*"Colonial Gayeties" (Shield)
*"On a Sunny Afternoon" (Shield)
*"Were Out for Fun" (Shield)
*"Here Comes the Stagecoach" (Hatley)
*"Our Relations/Finale" (Shield)

==References==
 

== External links ==
* 
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 