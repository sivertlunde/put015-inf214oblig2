Vengeance (1958 film)
{{Infobox Film
| name           = Vengeance
| image          = La Venganza (1958 movie poster).jpg
| caption        = Theatrical release poster
| director       = Juan Antonio Bardem
| producer       = Cesáreo González Manuel J. Goyanes 
| writer         = Juan Antonio Bardem
| narrator       = Francisco Rabal
| starring       = Raf Vallone Carmen Sevilla Jorge Mistral Manuel Alexandre Fernando Rey     
| music          = Isidro B. Maiztegui 
| cinematography = Mario Pacheco       
| editing        = Margarita de Ochoa  
| distributor    = 
| released       =  
| runtime        = 122 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}
Vengeance ( ) is a 1958 Spanish drama film directed by Juan Antonio Bardem. It was co-produced with Italy, starring Italian Raf Vallone. Francisco Rabal narrates the film. It was shown at the 1958 Cannes Film Festival    but not released in Spain until the following year. The film had serious troubles with Spanish censorship. Bardem even went to prison and it was an international scandal. It was nominated for the Academy Award for Best Foreign Language Film.   

==Cast==
* Carmen Sevilla as Andrea Díaz
* Raf Vallone as Luis El Torcido
* Jorge Mistral as Juan Díaz
* José Prada as Santiago El Viejo
* Manuel Alexandre as Pablo El Tinorio
* Manuel Peiró as Maxi El Chico
* Conchita Bautista as Cantante
* José Marco Davó
* Rafael Bardem
* Maria Zanoli
* Xan das Bolas as Segador gallego
* Rufino Inglés
* Ángel Álvarez
* Goyo Lebrero
* José Riesgo

==See also==
* List of submissions to the 31st Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 