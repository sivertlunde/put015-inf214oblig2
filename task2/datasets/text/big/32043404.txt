Das häßliche Mädchen
{{Infobox film
| name           = Das häßliche Mädchen
| image          = 
| caption        = 
| director       = Hermann Kosterlitz (Henry Koster) (uncredited)
| producer       = 
| screenplay     = Felix Joachimson (Felix Jackson) Hermann Kosterlitz (uncredited) Max Hansen Otto Wallburg
| music          = 
| cinematography = 
| editing        = 
| studio         = Avanti-Tonfilm GmbH
| distributor    = 
| released       =   
| runtime        = 
| country        = Nazi Germany
| language       = German
| budget         = 
}}
 German comedy Max Hansen, who was supposedly "too Jewish." The films representation of the "ugly girl" as outsider has been described as a metaphorical way to explore the outsider existence of Jews.

== Background and reception == Reich Film Chamber had been formed, with membership required for continued employment in cinema.   

Hermann Kosterlitz both directed and co-wrote the script. This was his first   or second    time directing.  Kosterlitz, who was Jewish, had left Germany months before the premiere, without seeing the final cut. His name was removed from the credits and replaced by an "Aryan" pseudonym, "Hasse Preis".  He went to Paris in April, then via Budapest and Vienna to Hollywood in 1936.  (The other author, Felix Joachimson, would go first to Austria and then also to the US, where he was a successful scriptwriter and producer under the name of Felix Jackson. ) 

The male lead, Max Hansen, was reputed to be part-Jewish and the previous year had performed a comic song implying that Hitler was homosexual; at the opening, in a riot orchestrated by the Nazis, members of the audience attacked him as "too Jewish", shouting "We want German movies! We want German actors!",  Hake,  .  and he was pelted with tomatoes. Bock and Bergfelder,  .    Rotten eggs were thrown at the screen. In the words of the review in Film-Kurier:  histling was heard from various sides. The applause stopped. The whistling continued. The curtain remained closed because rotten eggs were thrown at the stage. Someone called from the balcony: "We want German movies! We want German actors! We do not need Jewish actors, we have enough German actors! Arent you ashamed, German women, to applaud Jewish actors? Oust the Jew Max Hansen, who only six months ago sang a couplet about Hitler and Little Cohn in a cabaret!" David Stewart Hull, Film in the Third Reich: A Study of the German Cinema, 1933&ndash;1945, Berkeley: University of California, 1969,  .  Hansen also soon left Germany, for Vienna and then Denmark. 

Dolly Haas was an exclusively comedic actress with an androgynous persona  well suited to a film about appearance and the performance of identity; there were rumours about her racial heritage, too, but they were squashed with statements that she came "from a good Aryan family".  Upset by the treatment of her co-star, she accepted an invitation to work in England in 1934 and left Germany for good in 1936.   

The film did receive praise for its humour, and reviews included phrases such as "pleasant", "amusing" and "full of delightful ideas".  The Film-Kurier review noted that there was applause when the film ended, and an ovation for Haas, before she brought out Hansen.   During this transitional year, Nazi control over the film industry had yet to be consolidated and practices and attitudes varied.  Otto Wallburg was Jewish and was to die in Auschwitz, but continued to work in films in Germany until 1936 under the exemption for veterans of the First World War; in this film his role was simply characterised in the press as a typically sex- and money-obsessed member of the Berlin nouveau riche. 

== Plot summary ==
Lotte März (Dolly Haas) is hired as a secretary at an insurance company because she is ugly; introducing her to the (male) accountants, the personnel manager says, "I hope that you will finally be able to work in peace." The men harass her and conspire to lure her into a compromising position by having one of them, Fritz Mahldorf (Max Hansen) pretend to find her attractive. The manager discovers her in an embrace with Fritz and fires her. The self-absorbed Fritz, showing remorse that is unusual for him, arranges for her to be rehired as assistant to Director Mönckeberg (Otto Wallburg, a comical figure ). Lotte has fallen for him, but he makes a date at his flat with the Directors girlfriend, Lydia (  reviewer put it, "As always, however, the ugly duckling becomes a disturbingly graceful swan." ) Fritz falls in love with her and love triumphs, although he remains a flatterer and a deceiver and she has contemplated suicide. 

== Critical theories ==
The film has been seen as a treatment of the exclusion of Jews through the metaphor of the familiar trope of sexism and the need for women to self-present as acceptably feminine. Lottes initial ugliness translates as "she looks too Jewish."  Early in the film, she protests, "But I havent done anything to you!", which applies also to the situation of the Jews.  Hansen, the presumed Jew with characteristically "Jewish" features, playing Fritz, the tormentor with the stereotypically German name, and Haas, the blonde and childish-looking Lotte being excoriated as "ugly" (i.e. Jewish) effect a displacement of the problem of otherness in order to enact a narrative of accommodation making use of the traditional romantic comedy plot of the girl getting a makeover to attract the boy. 

== Unrealised Brecht project ==
Bertolt Brecht wanted to make a film of the same title featuring Valeska Gert, but this project never came to fruition.   

== References ==
 

== Sources ==
* Knud Wolffram. "Wir wollen deutsche Schauspieler! Der Fall Max Hansen". Filmexil 12 (2000) 47&ndash;59  

== External links ==
*  
*   at the German-language IMDb
*   at Filmportal.de  
*   at Filmportal.de (English version)

 
 
 
 