The Dark Mirror (film)
 
{{Infobox film
| name           = The Dark Mirror
| image          = The dark mirror vhs cover.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Siodmak
| producer       = Nunnally Johnson
| screenplay     = Nunnally Johnson Vladimir Pozner Thomas Mitchell
| music          = Dimitri Tiomkin
| cinematography = Milton R. Krasner
| editing        = Ernest J. Nims
| studio         = International Pictures
| distributor    = Universal Studios
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Vladimir Pozners original story on which the film is based was nominated for an Academy Award. 

==Plot==
This psychological thriller tells the story of a pair of identical twins, one loving and nice and the other severely disturbed.  A doctor is killed and witnesses identify one of the twins as the person seen having a quarrel with the victim shortly before his death.  A detective investigating the case cannot determine which twin did the killing since each can provide an alibi for the other.  The police ask for assistance from a doctor who studies twins to help crack the case.

==Cast==
* Olivia de Havilland as twins Terry and Ruth Collins
* Lew Ayres as Dr. Scott Elliott Thomas Mitchell as Lt. Stevenson Richard Long as Rusty Charles Evans as Dist. Atty. Girard
* Garry Owen as Franklin (as Gary Owen)
* Lela Bliss as Mrs. Didriksen
* Lester Allen as George Benson

==Reception==
===Critical response===
When first released the staff at Variety (magazine)|Variety magazine gave the film a mixed review, writing, "The Dark Mirror runs the full gamut of themes currently in vogue at the box office - from psychiatry to romance back again to the double identity gimmick and murder mystery. But, despite the individually potent ingredients, somehow the composite doesnt quite come off...Lew Ayres is cast in his familiar role as a medico - a specialist on identical twins. Slightly older looking and sporting a mustache, Ayres still retains much of his appealing boyish sincerity. But in the romantic clinches, Ayres is stiff and slightly embarrassed looking. Copping thespic honors, despite a relatively light part, Thomas Mitchell plays the baffled dick with a wry wit and assured bearing that carries belief." 

Bosley Crowther, film critic for The New York Times, also was critical, writing, "The Dark Mirror, like so many of its ilk, suffers from its authors lack of ingenuity to resolve his puzzle in a satisfying manner. As in his earlier and superior mystery, The Woman in the Window, Mr. Johnson solves the problem with a bit of trickery which is no credit to his craftsmanship. Still, one must hand it to Mr. Johnson for keeping his audience guessing, if not always entertained."  

Film critic Dennis Schwartz liked the film and wrote, "Siodmak deserves high praise for keeping the melodrama agreeable and intelligent when dealing with both the doppelgänger effect and the sibling rivalry; he does it better than the way most films have covered those subjects, showing that identical twins might look the same but have different psychological attitudes." 

==Remake== Jane Seymour as the twins and Vincent Gardenia as the cop.

==See also== The Guilty (1947) The Man with My Face (1951) Dead Ringer (1964)

==References==
 

==External links==
*  
*  
*  
*   at Senses of Cinema

 

 
 
 
 
 
 
 
 
 
 
 