The Boys Club
 
{{Infobox Film
| name       = The Boys Club
| image      = The Boys Club poster.jpg John Fawcett Peter Wellington
| starring   = Chris Penn Dominic Zamprogna  Devon Sawa  Stuart Stone Tim OBrien Greg Dummett
| distributor= Allumination Filmworks   
| released   = February 28, 1997 (Canada) June 17, 1997 (USA) English
| country    = Canada
}}
 John Fawcett, Peter Wellington (writer), and starring Chris Penn, Devon Sawa, Dominic Zamprogna, and Stuart Stone. It was released on VHS in Canada and the USA by Allumination Filmworks, on Laserdisc in the USA by Image Entertainment and on DVD in the USA in 1998 by Simitar Entertainment. It was re released in the USA in 2003 on DVD by Ardustry Home Entertainment. In 2013 it was released by Echo Bridge Home Entertainment on DVD.

==Plot==
Three teenage boys (Devon Sawa, Dominic Zamprogna, and Stuart Stone) in small-town Southern Ontario are thrilled when Luke Cooper (Chris Penn), a mysterious American fugitive with a gunshot wound in his leg, decides to crash their secret hideout. Luke tells them that hes a cop on the run from corrupt colleagues, and swears them to silence. As he recuperates, he becomes their buddy and confidante. By the time the boys realize Luke is not who he pretends to be, theyre in way over their heads.

==Cast==
*Chris Penn - Luke Cooper
*Dominic Zamprogna - Kyle 
*Stuart Stone - Brad
*Devon Sawa - Eric Amy Stewart - Megan
*Nicholas Campbell - Kyles Dad
*Jarred Blanchard - Jake 
*Max Piersig - Simon
*Julian Richings - Officer Cole
*Alana Shields - Brads Mom
*Sean Dick - Bobby
*Peter Van Wart - Erics Father
*Heidi von Palleske - Erics Mother
*Bif Naked - Liquor Store Manager
*Brock Curley - Liquor Store Cashier 
*Jordan-Patrick Marcantonio - (uncredited)
*Michael Anthony Williams - Bill (uncredited)

==Soundtrack==
#"Harnessed in Slums", Archers of Loaf
#"Universe", Erics Trip The Pasties
#"Old Enough", Crash Vegas Drugstore
#"Disease", Sister Machine Gun
#"Coconut Cream", The Tragically Hip
#"Jesus", Vowel Movement Rusty
#"Too Easy", Wagbeard
#"Gun Pointed", Taste of Joy The Killjoys The Killjoys Moist
#"You Shine Bright", Crash Vegas The Doughboys
#"The Letter", Bif Naked
#"Over Your Shoulder", Motörhead Crawl
#"Neighborhood The Doughboys
#"The Secret", 54-40

==Awards and nominations==
The film garnered four Genie Award nominations at the 17th Genie Awards in 1996: Best Director (Fawcett) Best Actor (Penn) Best Art Direction/Production Design (Taavo Soodor) Best Editing (Susan Maggi)

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 


 