Arizona Summer
 
{{Infobox film
|name=Arizona Summer
|image=Arizona Summer VideoCover.png
|caption=Video Cover
|director=Joey Travolta
|producer=William Blair Gaye Gilbert David Ornston Jeff Rice Rich Salvatore Joey Travolta
|writer=William Blair
|studio=WestPark Productions
|released= 
|cinematography=Brian Cox
|editing=Kaman
|distributor=Skyline West Pictures LLC
|runtime=102 minutes
|country=United States
|music=Edwin Willmington
|language=English
}} family oriented story revolving around Brent Butler (Gemini Barnett), a wiser-than-his years youngster. A modern day Tom Sawyer. The majority of the story takes place at a boys and girls camp owned and operated by Travers (Lee Majors), who has spent a lifetime of quietly helping adolescents become confident young adults. In addition to the full gamut of camp activities, adventures, practical jokes, and conflict resolution there is the underling theme that bolsters the importance of a positive father-son relationship.

==Cast==
*Gemini Barnett as Brent
*Brent Blair as Jack
*Christy Blair as Head Counselor
*Brooke Burgstahler as Christy
*Scott Clifton as Brooke
*Greg Evigan as Rick
*Morgan Fairchild as Debbie
*Bug Hall as Scott
*David Henrie as Bad
*Lorenzo Henrie as Jerry
*Hoku as Shawn
*Michelle Holgate as Donna
*Scott Johnson as Matt
*Jessica Kinsella as Sabrina
*April Lunsford as Melissa
*Lee Majors as Mr. Travers
*Michael Margetis as Mumps
*Bruce Nelson as Referee Chelsea Staub as Carol
*Shane Van Dyke as Mike
*Michael Wayne as Ty
*Tracy Wilkinson as Ms. Reed
*Dillon Zrike as Logan

==External links==
* 

 

 
 
 
 