Ninnu Kalisaka
 

{{Infobox film
| name           = Ninnu Kalisaka
| image          =
| caption        =
| writer         = 
| story          = Jai Kiran 
| screenplay     = Siva Nageswara Rao 
| producer       = Ramoji Rao
| director       = Siva Nageswara Rao
| starring       = Santosh Samrat   Chaitanya Krishna Dipa Shah Piaa Bajpai Jagapati Babu (Cameo)
| music          = Sunil Kashyap 
| cinematography = BL Sanjay 
| editing        = Gautham Raju
| studio         = Ushakiran Movies
| released       =  
| runtime        = 
| country        = India
| language       = Telugu
| budget         =
| preceded_by    = 
| followed_by    = 
}}

 Ninnu Kalisaka is a 2009 Telugu Romantic film produced by Ramoji Rao on Ushakiran Movies banner, directed by Siva Nageswara Rao. Starring Santosh Samrat, Chaitanya Krishna, Dipa Shah, and Piaa Bajpai in the lead roles. Jagapati Babu has a cameo appearance. The music was composed by Sunil Kashyap.         

==Cast==
* Santosh Samrat as Abhiram
* Chaitanya Krishna as Chandu
* Dipa Shah as Deepti
* Pia Bajpai as Bindu
* Jagapati Babu (Cameo) as Himself

==Soundtrack==
{{Infobox album
| Name        = Ninnu Kalisaka
| Tagline     = 
| Type        = film
| Artist      = Sunil Kashyap
| Cover       = 
| Released    = 2009
| Recorded    = 
| Genre       = Soundtrack
| Length      = 21:57
| Label       = Mayuri Audio
| Producer    = Sunil Kashyap
| Reviews     =
| Last album  = Gita   (2008)
| This album  = Ninnu Kalisaka   (2009)
| Next album  = Sneha Geetham   (2009)
}}

The music was composed by Sunil Kashyap and released by the Mayuri Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 21:57
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Ninnativaraku 
| lyrics1 = Lakshmibhupal
| extra1  = Achu Rajamani,Sai Shivani
| length1 = 3:28

| title2  = Mounam Manasullona
| lyrics2 = Lakshmibhupal SP Balu,Gayatri
| length2 = 3:59

| title3  = Andamaina Andama
| lyrics3 = Ananta Sriram Sujatha
| length3 = 4:12

| title4  = Mabbe Nelapaiki
| lyrics4 = Vanamali
| extra4  = Sunil Kashyap,Pranavi
| length4 = 3:12

| title5  = I Love U Love U 
| lyrics5 = Lakshmibhupal 
| extra5  = Tippu (singer)|Tippu, Pranavi
| length5 = 3:23

| title6  = Dilse Dilse  
| lyrics6 = Ananta Sriram  
| extra6  = Sunil Kashyap,Pranavi
| length6 = 3:43
}}
Source:   

==References==
 

 
 
 


 