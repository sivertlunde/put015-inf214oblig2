Smart Woman (1931 film)
{{Infobox film
| name           = Smart Woman
| image          = SmartWomanPoster.jpg
| image_size     =
| caption        = Theatrical poster for the film
| director       = Gregory La Cava
| producer       = William LeBaron Salisbury Field (adaptation and dialogue)
| story          =
| based on       =  
| narrator       = John Halliday
| music          =
| cinematography = Nick Musuraca
| editing        = Ann McKnight 
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} John Halliday. A woman sets out to regain the affections of her cheating husband.

==Cast==
*Mary Astor as Mrs. Nancy Gibson
*Robert Ames as Donald Gibson
*John Halliday as Sir Guy Harrington
*Edward Everett Horton as Billy Ross
*Noel Francis as Peggy Preston
*Ruth Weston as Mrs. Sally Gibson Ross
*Gladys Gale as Mrs. Preston
*Alfred Cross as Brooks
*Lillian Harmer as Mrs. Windleweaver

==External links==
* 
* 
* 

 

 
 
 
 
 

 