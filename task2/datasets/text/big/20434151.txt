Jawan Mohabbat
 
{{Infobox film
| name           = Jawan Mohabbat
| image          = 
| image_size     = 
| caption        = 
| director       = Bhappi Sonie
| producer       = 
| writer         =
| narrator       =  Pran
| music          =Shankar Jaikishan
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1971
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1971 Bollywood romance film directed by Bhappi Sonie. The film stars Shammi Kapoor, Asha Parekh and Pran (actor)|Pran.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Lyricist 
|-
| 1
| "In Nigahon Mein Ghar Tumhara Hai"
| Asha Bhosle 
| Hasrat Jaipuri
|-
| 2
| "Jawan Mohabbat Jahan Jahan Hai"
| Mohammed Rafi 
| Rajinder Krishan
|-
| 3
| "Na Rootho Na Rootho"
| Mohammed Rafi, Asha Bhosle, 
| Hasrat Jaipuri
|-
| 4
| "Nazar Mein Rang"
| Lata Mangeshkar
| Hasrat Jaipuri
|-
| 5
| "Mere Sapnon Ki Rani"
| Mohammed Rafi
| Hasrat Jaipuri
|- 
| 6
| "Mil Gayee Mil Gayee"
| Mohammed Rafi, Asha Bhosle
| Hasrat Jaipuri 
|-
| 7
| "Jab Muhabbat Jawan Hoti Hai"
| Mohammed Rafi
| Hasrat Jaipuri
|- 
| 8
| "Zulm-O-Sitam"
| Mohammed Rafi Shailendra
|}

==External links==
*  

 
 
 
 
 


 
 