Into the Wild (film)
{{Infobox film
| name           = Into the Wild
| image          = Into-the-wild.jpg
| caption        = Theatrical poster
| director       = Sean Penn
| producer       = Sean Penn Art Linson Bill Pohlad
| screenplay     = Sean Penn
| based on       =  
| starring       = Emile Hirsch Marcia Gay Harden William Hurt Jena Malone Brian Dierker Catherine Keener Vince Vaughn Zach Galifianakis Kristen Stewart Hal Holbrook
| music          = Michael Brook Kaki King Eddie Vedder
| cinematography = Eric Gautier
| editing        = Jay Cassidy
| studio         = River Road Entertainment Square One C.I.H. Linson Film
| distributor    = Paramount Vantage
| released       =  
| runtime        = 148 minutes 
| country        = United States
| language       = English
| budget         = $15 million 
| gross          = $56.2 million 
}}
 biographical drama drama survival book of the same name by Jon Krakauer based on the travels of Christopher McCandless across North America and his life spent in the Alaskan wilderness in the early 1990s. The film stars Emile Hirsch as McCandless with Marcia Gay Harden and William Hurt as his parents and also features Catherine Keener, Vince Vaughn, Kristen Stewart, and Hal Holbrook.
  Golden Globes and won the award for Best Original Song "Guaranteed (Eddie Vedder song)|Guaranteed" by Eddie Vedder. It was also nominated for two Academy Awards including Holbrook for Best Supporting Actor. 

== Plot ==
The film is presented in a nonlinear narrative, cutting back and forth between McCandlesss time spent in Alaskan wilderness and his two-year travels leading up to his journey to Alaska. The plot summary here is told in a more chronological order.
 abandoned city bus, which he calls The Magic Bus. At first, McCandless is content with the isolation, the beauty of nature around him, and the thrill of living off the land. He hunts wild animals with a .22 caliber rifle, reads books, and keeps a diary of his thoughts as he prepares himself for a new life in the wild.
 Datsun B210 to experience life in the wilderness. However, McCandless does not tell his parents Walt (William Hurt) and Billie McCandless (Marcia Gay Harden) or his sister Carine (Jena Malone) what he is doing or where he is going, and refuses to keep in touch with them after his departure, leaving them to become increasingly anxious and eventually desperate.

At  , McCandless encounters a hippie couple named Jan Burres (Catherine Keener) and Rainey (Brian H. Dierker). Rainey tells McCandless about his failing relationship with Jan, which McCandless helps rekindle. By September, McCandless stops in Carthage, South Dakota to work for a contract harvesting company owned by Wayne Westerberg (Vince Vaughn), but he is forced to leave after Westerberg is arrested for satellite piracy.

McCandless then travels to the Colorado River and, though told by park rangers that he may not kayak down the river without a license, ignores their warnings and paddles downriver until he eventually arrives in Mexico. There, his kayak is lost in a dust storm and he crosses back into the United States on foot. Unable to hitchhike, he starts traveling on freight trains to Los Angeles, California. Not long after arriving, however, he starts feeling "corrupted" by modern civilization and decides to leave. Later, McCandless is forced to switch his traveling method back to hitchhiking after he is beaten by the railroad police.

In December 1991, McCandless arrives at Slab City in the Imperial Valley region of California and encounters Jan and Rainey again. There, he meets Tracy Tatro (Kristen Stewart), a teenage girl who shows interest in McCandless, but he rejects her because she is underage. After the holidays, McCandless decides to continue heading for Alaska, much to everyones sadness. While camping near Salton City, California, McCandless encounters Ron Franz (Hal Holbrook), a retired man who recounts the story of the loss of his family in a car accident while he was serving in the United States Army. He now occupies his time in a workshop as an amateur leather worker. Franz teaches McCandless the craft of leatherwork, resulting in the making of a belt that details McCandless travels. After spending several months with Franz, McCandless decides to leave for Alaska despite this upsetting Franz, who has become quite close to McCandless. On a parting note, Franz gives McCandless his old camping and travel gear along with the offer to adopt him as his grandchild, but McCandless simply tells him that they should discuss this after he returns from Alaska; then departs.
 stream he had crossed during the winter has become wide, deep, and violent due to the thaw, and he is unable to cross. Saddened, he returns to the bus, now as a prisoner who is no longer in control of his fate and can only hope for help from the outside. In a desperate act, McCandless is forced to gather and eat roots and plants. He confuses similar plants and eats a poisonous one, falling sick as a result. Slowly dying, he continues to document his process of self-realisation and accepts his fate, as he imagines his family for one last time. He writes a farewell to the world and crawls into his sleeping bag to die. Two weeks later, his body is found by moose hunters. Shortly afterwards, Carine returns her brothers ashes by airplane from Alaska to Virginia in her backpack.

== Cast ==
  Christopher McCandless / Alexander Supertramp
* Marcia Gay Harden as Billie McCandless
* William Hurt as Walt McCandless
* Jena Malone as Carine McCandless
* Catherine Keener as Jan Burres
* Brian H. Dierker as Rainey
* Vince Vaughn as Wayne Westerberg
* Zach Galifianakis as Kevin
* Kristen Stewart as Tracy Tatro 
* Hal Holbrook as Ron Franz
* Thure Lindhardt as Mads
* Signe Egholm Olsen as Sonja
* Merritt Wever as Lori
* Jim Gallien as Himself
* Leonard Knight as Himself Bull
* Cheryl Harrington as social worker

== Production ==

=== Filming ===
The scenes of graduation from Emory University in the film were shot in the fall of 2006 on the front lawn of Reed College. Some of the graduation scenes were also filmed during the actual Emory University graduation on May 15, 2006.  The Alaska scenes depicting the area around the abandoned bus on the Stampede Trail were filmed 50 miles south of where McCandless actually died, in the tiny town of Cantwell, Alaska|Cantwell. Filming at the actual bus would have been too remote for the technical demands of a movie shoot, so filming budget was extended  to some smaller scenes in New Zealand.  so a replica of the bus was built, and used when filming, from two other old buses of similar type. The production made four separate trips to Alaska to film during different seasons.

== Release ==

=== Critical reception ===
Into the Wild earned positive reviews from critics. The review aggregator   assigned the film an average score of 73 out of 100, based on 38 reviews from mainstream critics. 

Roger Ebert of the Chicago Sun-Times gave the film four stars out of four and described it as "spellbinding." Ebert wrote that Emile Hirsch gives a "hypnotic performance," commenting: "It is great acting, and more than acting." Ebert added, "The movie is so good partly because it means so much, I think, to its writer-director, Sean Penn." 

The American Film Institute listed the film as one of ten AFI Movies of the Year for 2007 in film|2007.  

National Board of Review named it one of the Top Ten Films of the Year. 

Into the Wild also ranks 473rd in Empire (magazine)|Empire magazines 2008 list of the 500 greatest movies of all time. 

==== Top ten lists ====
The film appeared on many critics top ten lists of the best films of 2007.   
 
 
* 1st: Ben Lyons, The Daily 10 
* 2nd: Ann Hornaday, The Washington Post 
* 2nd: Tasha Robinson, The A.V. Club 
* 3rd: James Berardinelli, ReelViews 
* 3rd: Kevin Crust, Los Angeles Times 
* 3rd: Peter Travers, Rolling Stone 
* 4th: Kyle Smith, New York Post 
* 5th: Claudia Puig, USA Today 
 
* 5th: David Germain, Associated Press   
* 5th: Joe Morgenstern, The Wall Street Journal 
* 6th: Carrie Rickey, The Philadelphia Inquirer 
* 6th: Steven Rea, The Philadelphia Inquirer  The Diving Bell and the Butterfly) 
* 7th: Noel Murray, The A.V. Club 
* 9th: Christy Lemire, Associated Press 
* 10th: Roger Ebert, Chicago Sun-Times 
 

=== Awards ===

==== Winnings ====
 
 
* 65th Golden Globe Awards  Best Original Song&nbsp;– Motion Picture ("Guaranteed (Eddie Vedder song)|Guaranteed")

* Gotham Awards
** Best Feature Film

* Mill Valley Film Festival
** Best Actor (Emile Hirsch)

* Palm Springs International Film Festival
** Director of the Year Award (Sean Penn)
** Rising Star Award Actor (Emile Hirsch)
 
* National Board of Review
** Breakthrough Performance&nbsp;– Male (Emile Hirsch)

* Rome Film Festival
** Jury Award (William Pohlad), (Art Linson), (Sean Penn)

* São Paulo International Film Festival
** Best Foreign Language Film (Sean Penn)

* Italian Online Movie Awards
** Best Motion Picture Of The Year
** Best Motion Picture soundtrack
 

==== Nominations ====
 
 
* 80th Academy Awards    Best Supporting Actor (Hal Holbrook) Best Film Editing (Jay Cassidy)

* 65th Golden Globe Awards   
** Best Original Score&nbsp;– Motion Picture (Michael Brook, Kaki King, Eddie Vedder)
 American Cinema Editors
** Best Edited Feature Film&nbsp;– Dramatic (Jay Cassidy)
 Broadcast Film Critics Association
** Best Film
** Best Actor (Emile Hirsch)
** Best Supporting Actor (Hal Holbrook)
** Best Supporting Actress (Catherine Keener)
** Best Director (Sean Penn)
** Best Writer (Sean Penn)
** Best Song ("Guaranteed (Eddie Vedder song)|Guaranteed")

* Chicago Film Critics Association Awards
** Best Picture
** Best Screenplay&nbsp;– Adapted (Sean Penn)
** Best Supporting Actor (Hal Holbrook)

* Directors Guild of America Awards
** Best Director&nbsp;– Film (Sean Penn)
 Cinema Audio Society
** Outstanding Achievement in Sound Mixing for Motion Pictures
 
* Costume Designers Guild Awards
** Excellence in Costume Design for Film&nbsp;– Contemporary

* Film Critics Circle of Australia Awards
** Best Foreign Film&nbsp;– English Language (Sean Penn)

* Grammy Awards
** Best Song Written for Motion Picture, Television or Other Visual Media ("Guaranteed (Eddie Vedder song)|Guaranteed")

* Gotham Awards
** Breakthrough Award (Emile Hirsch)

* Satellite Awards
** Best Original Song ("Rise")
 Screen Actors Guild Awards
** Outstanding Performance by a Cast in a Motion Picture
** Outstanding Performance by a Male Actor in a Leading Role (Emile Hirsch)
** Outstanding Performance by a Male Actor in a Supporting Role (Hal Holbrook)
** Outstanding Performance by a Female Actor in a Supporting Role (Catherine Keener)

* USC Scripter Award
** USC Scripter Award (Sean Penn) (screenwriter), (Jon Krakauer) (author)

* Writers Guild of America Awards
** Best Adapted Screenplay (Sean Penn)
 

=== Box office ===
In North America, Into the Wild initially opened in limited release, in four theaters and grossed $212,440, posting a per-theater average of $53,110. For the next several weeks, the film remained in limited release until it expanded to over 600 theaters on October 19, 2007; in its first weekend of wide release, the film grossed just $2.1 million for a per-theater average of $3,249. As of December 25, 2008, the film grossed $18,354,356 domestically and $37,281,398 internationally. In total, the film has grossed $55,635,754 worldwide. 

=== Home media ===
Into the Wild was released on March 4, 2008 on standard DVD,  Two-Disc Special Collectors Edition DVD,  and standard HD DVD. It was one of the final films to be released on the now defunct format.  The special edition DVD and HD DVD contain two special features entitled The Story, The Characters and The Experience. The Blu-ray Disc edition was released in France on July 16, 2008.  The Blu-ray edition for the US was released on December 16, 2008.  The UK Blu-ray was released on July 20, 2009. 

== Soundtrack ==
 
 Golden Globe Best Original Song for the song "Guaranteed (Eddie Vedder song)|Guaranteed". The score was written and performed by Michael Brook and Kaki King.  The music at the end of the theatrical trailer is "Acts of Courage" by X-Ray Dog, a company that supplies music for many movie trailers.
 
 
* "Hard Sun" (Eddie Vedder and Corin Tucker)
* "Society" (Eddie Vedder and Jerry Hannan)
* "No Ceiling" (Eddie Vedder)
* "Rise" (Eddie Vedder)
* "Long Nights" (Eddie Vedder)
* "The Wolf" (Eddie Vedder)
* "Guaranteed" (Eddie Vedder)
 
* "Going Up The Country" (Canned Heat) King of the Road" (Roger Miller)
* "Emory and Old St. Andrews March" (The Atlanta Pipe Band)
* "U Cant Touch This" (M.C. Hammer)
* "The Water Ran This Way Back and Forth" (Pedro)
* "I Thought I Was You" (Kelly Peterson)
* "Kaa" (Claude Chaloub)
 
* "Fork And File" (The Crooked Jades)
* "Dakota Themes" (Peter Ostroushko)
*Slab Song (Everett Insane Wayne Smith)
* "Tracys Song" (Kristen Stewart)
* "Angel from Montgomery" (Kristen Stewart and Emile Hirsch)
* "Picking Berries" (Gustavo Santaolalla)
* "Porterville" (Creedence Clearwater Revival)
 

== See also ==
* Survival film, about the film genre, with a list of related films
* The Call of the Wild (2007 film)|The Call of the Wild, a 2007 documentary about McCandless made by Ron Lamothe. 
* Vagabond (film)|Vagabond (film)

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 