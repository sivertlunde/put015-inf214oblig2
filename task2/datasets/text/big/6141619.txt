Son of Lassie
{{Infobox film
| name           = Son of Lassie
| image          = Son of Lassie - 1945 - Poster.png
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = S. Sylvan Simon Samuel Marx
| writer         = Jeanne Bartlett Eric Knight Pal (credited as Lassie) Peter Lawford Donald Crisp June Lockhart Nigel Bruce
| music          = Herbert Stothart
| cinematography = Charles Edgar Schoenbaum
| editing        = Ben Lewis
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews Cineplex Entertainment|Loews Inc.
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Pal (credited as Lassie). A sequel to Lassie Come Home, the film focuses on the now adult Joe Carraclough after he joins the Royal Air Force during the World War II and is shot down over Nazi-occupied Norway along with stowaway, Lassies son "Laddie" – played by Pal (dog)|Pal. It was released theatrically on April 20, 1945, by Loews Cineplex Entertainment|Loews.

==Plot==
In Yorkshire, England, at the estate of the Duke of Rudling (Nigel Bruce), the British Army converted the grounds into a training camp for war dogs. The camp is placed under the supervision of Sam Carraclough (Donald Crisp), the kennel caretaker, who immediately begins the process of selecting the best dogs for training, including Laddie, the young pup of the champion collie, Lassie. Joe Carraclough (Peter Lawford), now an adult, joins the Royal Air Force (RAF) during the Second World War. Departing for training school, he is forced to leave behind his dog Lassie and her pup, Laddie. 

Laddie, being considered as a "war dog", follows Joe to training school and then stows away on his masters bomber, just as it takes off on a dangerous mission over Nazi-occupied Norway. The two are forced to parachute when hit by enemy fire. Laddie seeks help for his injured master and while they are separated with Joe being captured, the dog is pursued by enemy soldiers, first being sheltered by young Norwegian children and then by a freedom-fighter who is killed. Laddie reaches the prisoner-of-war camp where his master had been taken. 

The German guards use Laddie to seek out his master who had escaped. In his search for Joe who is forced into a labor detail on a coastal gun emplacement, Laddie is reunited with his master and thereafter, the two race for their lives to reach friendly lines as the Nazis pursue them. Finally free, both Joe and Laddie make their way back to the Rudling estate to reunite with Lassie, Sam Carraclough, Joes father and Priscilla (June Lockhart), the Duke of Rudlings granddaughter. 

==Main cast== Pal (credited as "Lassie") as Lassie and grown-up Laddie
* Peter Lawford as Joe Carraclough
* Donald Crisp as Sam Carraclough, Joes father
* June Lockhart as Priscilla, the Duke of Rudlings granddaughter
* Nigel Bruce as Duke of Rudling, grandfather to Priscilla
* Donald Curtis as Sgt. Eddie Brown Robert Lewis as Sgt. Schmidt
* Nils Asther as Olav
* William Severn as Henrik Leon Ames as Anton
* Fay Helm as Joanna Terry Moore as Thea

==Production==
Produced under the working title, Laddie, Son of Lassie, the film originally had Elsa Lanchester playing the role of the adult Priscilla. Shortly after filming began, June Lockhart took over the role.   TCM Movie Database:   method.  

Principal filming took place from May to November 1944,  in various locations throughout western Canada, including Vancouver Island and Christopher Point in British Columbia, Banff National Park in Alberta, as well as Jackson Hole, Wyoming, and Los Angeles in the United States.    
 Bristol Bolingbroke and Lockheed Ventura bombers of the Royal Canadian Air Force. 

According to the Hollywood Reporter, John Charles Reed sued MGM in October 1947 for plagiarism, claiming the film script was based on his 1943 story "Candy". The jury disagreed and the suit was dismissed.  

The movie reportedly popularized the name "Lad" for male dogs.  Pal (dog actor)|Pal, the original male collie who played Lassie in Lassie Come Home (1943) played Laddie. A 20-year old June Lockhart, whose screen career had consisted of bit parts, had a more meaningful connection to the iconic Lassie story when in 1958, she took on the role of Ruth Martin, who adopts orphan Timmy (Jon Provost) in the long-running (CBS, 1954–1971) TV series "Lassie".  

==Release==
Son of Lassie was released to theaters on April 20, 1945. A VHS home video release came on September 1, 1998.  It was first released to Region 1 DVD by Warner Home Video on August 24, 2004.  It was re-released on November 7, 2006 in a three-movie, 2-disc set along Lassie Come Home and Courage of Lassie. 

==Reception==
Bosley Crowther in the New York Times of June 11, 1945, felt the sequel to Lassie Come Home fell short of "being a worthy heir to the champion" and further noted, "The resulting film, which, while it undoubtedly will be a delight for dog lovers, evolves mainly as a lengthy, contrived and only occasionally suspenseful melodrama handsomely dressed in the lovely polychromes of Technicolor."  He praised the main players and concluded, "it is the winsome Laddie and Lassie who romp away with the acting laurels of this pretty but incredible picture." 

Variety characterized the principal actors as "excellent" but the film was "sticky sentiment, and flamboyant adventures, carry  sufficient interest to move it along."  

==References==
;Notes
 
;Bibliography
 
* Farmer, James H.   Blue Ridge Summit, Pennsylvania: Tab Books, 1984. ISBN 978-0-8306-2374-7.   
* Haines, Richard W. Technicolor Movies: The History of Dye Transfer Printing.  Jefferson North Carolina: McFarland & Company, 2003. ISBN 978-0-7864-1809-1.
* Junker, Reynold Joseph Paul. Subway Music. Bloomington, Indianna: Iuniverse, 2005. ISBN 978-0-595-36846-4.
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 

 

 
 
 
 
 