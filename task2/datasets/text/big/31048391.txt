The Miracle (1987 film)
 
{{Infobox film
| name           = The Miracle
| image          = Le Miraculé.jpg
| caption        = Film poster
| director       = Jean-Pierre Mocky
| producer       = Jean-Pierre Mocky
| writer         = Patrick Granier Jean-Claude Romer Jean-Pierre Mocky
| starring       = Michel Serrault
| music          = 
| cinematography = Marcel Combes
| editing        = Jean-Pierre Mocky
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = France
| language       = French
| budget         = 
}}

The Miracle ( ) is a 1987 French comedy film directed by Jean-Pierre Mocky. It was entered into the 37th Berlin International Film Festival.   

==Cast==
* Michel Serrault as Ronald Fox Terrier
* Jean Poiret as Papu
* Jeanne Moreau as Sabine, dite La Major
* Sylvie Joly as Mrs. Fox Terrier
* Jean Rougerie as Monseigneur
* Roland Blanche as Plombie
* Sophie Moyse as Angelica
* Marc Maury as Labbé Humus / Father Humus
* Hervé Pauchon as Joulin
* Georges Lucas as Le miraculé Dulac
* Jean Abeillé as Victor
* Dominique Zardi as Rondolo

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 