Passage (2008 film)
{{Infobox film
| name           = Passage
| image          =
| alt            =
| caption        =
| director       = John Walker
| producer       = Andrea Nemtin John Walker Kent Martin
| writer         = John Walker Andrew Rai Berzins
| narrator       = Minoru Fukushima Rick Roberts Geraldine Alexander
| music          =
| cinematography = Kent Nason
| editing        = Jeff Warren John Brett
| studio         = PTV Productions John Walker Productions National Film Board of Canada
| distributor    =
| released       =  
| runtime        = 108 minutes
| country        = Canada
| language       = English
| budget         =
| gross          =
}} Sir John John Raes Lady Franklins campaign to defend her late husbands reputation. The film also features Inuit statesman Tagak Curley, who challenges claims made by Lady Franklin supported by her powerful friend, the story teller    and "famous author Charles Dickens", widely reported at the time, that Aboriginal people were responsible for the signs of cannibalism among the remains of the doomed crew.   

It premiered at the Hot Docs film festival in Toronto, Canada in April 2008. 

Passage has two main storylines. The first shows John Walker and crew, making an historical fiction film; it includes script readings, discussions and scenes from the film they are making. The second story line is completely non-fiction. It includes paintings with narratives, cast and crew visiting the places Rae knew (Orkney, the Arctic) and Inuit culture and experts.
 John Walker. Rick Roberts as John Rae and Geraldine Alexander as Lady Franklin.

==Reception==
It won two awards at the Atlantic Film Festival: Best Director-John Walker and Best Cinematography-Kent Nason and Nigel Markham. Passage also received the Grand Prize for Best Canadian Production at the Banff World Television Festival. Critical praise for the film included Martin Knelman of the Toronto Star, who called the film "one of the great triumphs in Canadian documentary film history." 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 