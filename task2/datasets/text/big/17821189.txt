Embun
 
{{Infobox film
| name           = Embun
| image          = Embun P&K Apr 1953 p35 2.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Leman and Ira in a scene from the film
| director       = D. Djajakusuma
| producer       = Surjo Sumanto
| writer         =
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       ={{plain list|
*Rd Ismail
*Titi Savitri
*AN Alcaff
*Aedy Moward
}}
| music          = GRW Sinsu
| cinematography = Max Tera
| editing        = Soemardjono
| studio         = Perfini
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Indonesia
| language       = Indonesian
| budget         = 
| gross          = 
}}
Embun (Indonesian for Dewdrop) is a 1952 film directed by D. Djajakusuma for Perfini in his directorial debut.

==Plot==
After the conclusion of the Indonesian National Revolution, Leman (AN Alcaff) and Barjo (Rd Ismail) go to the national capital in Jakarta and live off government donations. When a meeting goes awry, Leman accidentally kills the prospective donor, and Barjo and Leman escape. They go their separate ways, and Leman remains on the run for five years. He ultimately settles in a village.

As he had been given everything he needed while a guerrilla, Leman is shocked when society appears to ignore him. He tries to earn a living as a farmer, but is unable to do so owing to the ongoing drought. He decides to go to the city and find work. However, this plan is stopped when his girlfriend Ira (Titi Savitri) convinces him that it is his duty to stay in the village and help it develop.

One day, Leman learns that Barjo has become rich off from prostitution and gambling rackets. When Leman confronts his former friend, the latter forces him to work as a driver. When Leman learns that they are smuggling weapons, he faces off against Barjo and the two fight on a mountaintop; this ends with Barjos death.  

==Production==
Embun was co-written and directed by D. Djajakusuma for Perfini while its head and regular director, Usmar Ismail, went abroad to study cinematography.  Although Djajakusuma had worked for the company since assisting with the production of Enam Djam di Jogja in 1951, this film was both his first as a director and his first as a screenwriter, though writing duties were also shared by Basuki Resobowo and Gajus Siagian.   With the release of Embun, Djajakusuma became one of four directors to work for Perfini,  and as with Ismail he was known as a Realism (arts)|realist. 

This black-and-white film was produced by Surjo Sumanto,  with cinematography by Max Tera. Soemardjono handled artistic direction and editing, while music was handled by GRW Sinsu and sound managed by Sjawaludin and Bob Saltzman. 

Embun starred Rd Ismail, Titi Savitri, AN Alcaff, and Aedy Moward.  The cast also included Iskandar Sucarno, Cassim Abbas, Muljono, D Arifin, Hardjo Muljo, Hamidy T Djamil, and Rasmina. 

==Themes==
Embun was shot in Wonosari, at the time in the middle of a drought, to provide a visual metaphor for the barren souls of the warriors.  In his biography of Djajakusuma, Satyagraha Hoerip describes the returning soldiers as "anti-heroes" who are forced to be practical in the face of reality, but have optimistic dreams; Hoerip suggests that both were considered necessary for the future. 

In a 1972 interview, Djajakusuma explained that the "dewdrop" of the title was a reference to love, more particularly the love which instills courage; it is through this love, he stated, that the barren ground can become lush and fertile. 

==Release and reception==
Embun was released in 1952.  Because of the films depiction of traditional superstitions, Embun had trouble with both the censorship bureau and critics; superstition was considered incompatible with the need of the new Indonesian republic, proclaimed in 1945, for modernisation.  This was not the only Perfini film about the National Revolution which received criticisism; Ismails earlier Darah dan Doa (1950) and Enam Djam di Jogja were both targets of criticism from leftist factions owing to the films overemphasis of the Siliwangi Division and portrayal of Dutch soldiers, respectively.  This continued string of issues with censorship led Perfini to make increasingly commercial films, and the companys next production, Terimalah Laguku (1952), was a musical. 

A 35 mm copy of Embun is held in Sinematek Indonesias archives.  A video cassette edition was released in 2003. 

==References==
 

==Works cited==
 
*{{cite book
 |title=Sejarah Kecil (Petite Histoire) Indonesia
 |trans_title=Short History of Indonesia
 |language=Indonesian
 |last=Anwar
 |first=Rosihan
 |year=2004
 |publisher=Kompas
 |location=Jakarta
 |volume=2
 |isbn=978-979-709-428-7
 |url=http://books.google.co.id/books?id=YWHjTT3FEycC
 |ref=harv
}}
*{{Cite web
 |title=Embun
 |language=Indonesian
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |url=http://filmindonesia.or.id/movie/title/lf-e005-51-161984_embun
 |accessdate=20 May 2014
 |archivedate=20 May 2014
 |archiveurl=http://www.webcitation.org/6Pi1YDkGZ
 |ref= 
}}
*{{Cite web
 |title=Embun
 |publisher=WorldCat
 |url=http://www.worldcat.org/title/embun/oclc/053200281
 |accessdate=20 May 2014
 |archivedate=20 May 2014
 |archiveurl=http://www.webcitation.org/6Pi4nPi0f
 |ref= 
}}
*{{Cite web
 |title=Kredit Lengkap Embun
 |trans_title=Full Credits for Embun
 |language=Indonesian
 |work=filmindonesia.or.id
 |publisher=Konfiden Foundation
 |url=http://filmindonesia.or.id/movie/title/lf-e005-51-161984_embun/credit#.U3sre_mSz0U
 |accessdate=20 May 2014
 |archivedate=20 May 2014
 |archiveurl=http://www.webcitation.org/6Pi14YgVv
 |ref= 
}}
*{{cite book
 |title=Dua Dunia dalam Djadoeg Djajakoesoema
 |trans_title=Two Worlds in Djadoeg Djajakoesoema
 |language=Indonesian
 |first= Satyagraha
 |last=Hoerip
 |publisher=Jakartan Ministry of Culture working with the Jakarta Institute of Art
 |location=Jakarta
 |year=1995
 |isbn=978-979-8699-00-9
 |ref=harv
}}
*{{cite book
 |last=Ismail
 |first=Usmar
 |title=Usmar Ismail Mengupas Film
 |trans_title=Usmar Ismail Discusses Film
 |chapter=Sari Soal dalam Film Indonesia
 |trans_chapter=A Summary of Issues Faced by Indonesian Films
 |pages=53–66
 |language=Indonesian
 |year=1983
 |publisher=Sinar Harapan
 |location=Jakarta
 |ref=harv
 |oclc=10435722
}}
*{{Cite journal
 |title=Entretien avec D. Djajakusuma
 |trans_title=Meeting with D. Djajakusuma
 |language=French
 |work=Archipel
 |volume=5
 |issue=5
 |last=Lombard
 |first=D.
 |url=http://www.persee.fr/web/revues/home/prescript/article/arch_0044-8613_1973_num_5_1_1050
 |pages=178–182
 |year=1973
 |ref=harv
}}
*{{cite book
 |title=Profil Dunia Film Indonesia
 |trans_title=Profile of Indonesian Cinema
 |language=Indonesian
 |last=Said
 |first=Salim
 |publisher=Grafiti Pers
 |location=Jakarta
 |year=1982
 |oclc=9507803
 |ref=harv
}}
*{{Cite book
 |title=Heirs to World Culture: Being Indonesian, 1950-1965
 |editor-first=Jennifer
 |editor-last=Lindsay
 |chapter=Indonesian Muslims and Cultural Networks
 |last=Salim
 |first=Hairus
 |pages=75–117
 |url=http://depot.knaw.nl/13514/1/Salim_Indonesian_muslims.pdf
 |publisher=KITLV Press
 |location=Leiden
 |year=2012
 |ref=harv
 |isbn=978-90-6718-379-6
}}
 

 

 

 
 
 
 