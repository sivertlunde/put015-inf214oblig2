Killer Nun
{{Infobox Film
| name           = Killer Nun
| image          = Killernunposter.jpg
| image_size     = 
| caption        = Original Italian poster
| director       = Giulio Berruti
| producer       = Enzo Gallo
| writer         = Giulio Berruti Alberto Tarallo
| narrator       = 
| starring       = Anita Ekberg
| music          = Alessandro Alessandroni
| cinematography = Antonio Maccoppi
| editing        = 
| distributor    = 
| released       = 1978
| runtime        = 82 Minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
 Italian nunsploitation film directed and co-written by Giulio Berruti and co-written by Alberto Tarallo. The film was originally banned in Britain as a video nasty and released with cuts in 1993, but was finally released uncut on DVD in the UK during 2006, after changes in British censorship policy.

==Synopsis==

Killer Nun (1978) was produced in Italy. It features Anita Ekberg as Sister Gertrude, who is recovering from neurosurgery, although her Mother Superior (Alida Valli) dismisses Sister Gertrudes fears about rushed recovery. Unfortunately, soon enough, it becomes clear that Sister Gertrudes fears were legitimate, as the hapless nun spirals into psychosis and addiction to morphine and heroin at the geriatric hospital where she works.

As well as initiating a lesbian affair with Sister Matthieu (Paola Morra), Sister Gertrude expels concerned Dr Poirret (Massimo Serato) from the hospital, and a reign of terror is initiated, in which Sister Gertrude inflicts humiliating calisthenics on one group of elderly inmates, stomps on an elderly womans dentures, reads gory hagiographic details of the lives of tortured saints to her hapless charges and is judged to have thrown an elderly man engaged in sex with a nurse out of a window. As if this werent enough, Sister Gertrude goes into a nearby town, picks up a man at a bar, and has impersonal heterosexual sex as well. A handsome new doctor (Joe Dallesandro) arrives and becomes suspicious of Sister Gertrude, and finally the Mother Superior is convinced that she must do something about her aberrant behaviour... but is Sister Gertrude really the perpetrator of murder, or is someone trying to frame her?

==Nunsploitation==

Killer Nun (Suor Omicidi) was an example of the nunsploitation genre, which centres on aberrant secularised behaviour from religious women. Unlike other examples of the genre, usually set in medieval or Renaissance locations, Killer Nun is firmly set in the present day, and has no pretensions to social commentary or any remarks about the role of women religious within the Church or the larger society. In the United Kingdom, Mary Whitehouse denounced it as one of the "video nasties" subgenre of violent horror cinema, which might adversely affect human behaviour.

Although it was originally on a DPP list of objectionable films in the United Kingdom, compiled by the Director of Public Prosecutions in 1983 as a result of the aforementioned moral panic and released with 13 seconds of cuts in 1993. Liberalised British film, video and DVD censorship policy meant that a DVD of the film was released in the UK during 2006 in its uncut form.

The Time Out Film Guide describes this film as "a dated blend of softcore sleaze, routine blood-letting and explicable coyness" which "stars an over-the-hill Ekberg." An "excessive scenario" nevertheless has "quaint evasions." According to this review, "lesbianism is hinted at but not shown!" and "scenes of Ekberg shooting up are filmed with her back to the camera." 

==See also==
*Video nasties

==Cast ==
*Anita Ekberg .... Sister Gertrude
*Joe Dallesandro .... Dr. Patrick Roland
*Alida Valli .... Mother Superior
*Lou Castel .... Peter
*Paola Morra .... Sister Mathieu
*Massimo Serato .... Dr. Poirret
*Daniele Dublino .... Director
*Laura Nucci .... Baroness
*Alice Gherardi .... Janet
*Lee De Barriault
*Ileana Fraia
*Antonietta Patriarca
*Sofia Lusy
*Nerina Montagnani .... Josephine
*Franco Caracciolo

==Home video and classification details==
The film has been released on DVD in America by Blue Underground, in Germany by Koch Media and in the UK by Shameless Screen Entertainment. 

Color: Color 
Sound Mix: Mono 
Certification: Germany:18 / Finland:K-18 / Norway:18 / UK:18 / West Germany:18 / Iceland:(Banned)

==References==
 

==External links==
* 

 
 
 
 
 