Meendum Savithri
{{Infobox film
| name           = Meendum Savithri
| image          = Meendum Savithri DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = Visu
| producer       = B. Nagi Reddy
| writer         = Visu
| starring       =  
| music          = Devendran
| cinematography = N. Balakrishnan
| editing        = Ganesh Kumar
| distributor    = Vijaya Pictures
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1996 Tamil Tamil drama Raja in Pandu playing supporting roles. The film, produced by B. Nagi Reddy, had musical score by Devendran and was released on 9 February 1996. Despite positive reviews, the film bombed at the box-office.     

==Plot==

Manju ( ) who seems to be the perfect future husband. When Manju and Narayana Moorthy meet his family, Narayana Moorthy is very shocked. Vasudevans father Ramamoorthy (Nagesh) became mentally ill when his business failed, his sister Gayathri (Seetha) became mantally ill when someone raped her, his brother Bhaskar (Ramesh Aravind) is a drunkard and his mother (Annapoorna (actress)|Annapoorna) is an asthma patient. Finally, Manju and Vasudevan get married. Vasudevans family was in fact acting, they are perfectly all right but they are scared of something. The truth is that Vasudevan was already married to Uma (Saranya Ponvannan). What transpires later forms the crux of the story.

==Cast==

 
*Revathi as Manju
*Saranya Ponvannan as Uma
*Nizhalgal Ravi as Narasimman Raja as Vasudevan
*Ramesh Aravind as Bhaskar
*Visu as Narayana Moorthy
*Nagesh as Ramamoorthy
*Jaiganesh as Chandramouli Annapoorna as Vasudevans mother
*Seetha as Gayathri Pandu as Gajendran
*Kumarimuthu
*Jamuna Srinivasan as Meenatchi
*Ganga
*Suryakanth
*Oru Viral Krishna Rao
*Idichapuli Selvaraj
*Omakuchi Narasimhan
 

==Soundtrack==

{{Infobox album |  
| Name        = Meendum Savithri
| Type        = soundtrack
| Artist      = Devendran
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack
| Length      = 28:57
| Label       = 
| Producer    = Devendran
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Devendran. The soundtrack, released in 1996, features 5 tracks with lyrics written by Piraisoodan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Naatukulla Romba || Mano (singer)|Mano, Chorus || 4:31
|- 2 || Pethava Unnai || K. J. Yesudas || 4:19
|- 3 || Avaravar Thalai || K. S. Chithra || 3:58
|- 4 || Indha Naal || Mano || 4:58
|- 5 || Vel Murugan || Mano || 3:48
|}

==References==
 

 
 
 
 