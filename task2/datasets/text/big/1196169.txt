Check and Double Check
{{Infobox film
| name           = Check and Double Check
| image          = CheckDoubleCheckPoster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Melville W. Brown
| producer       = William LeBaron
| writer         = Bert Kalmar J. Walter Ruben Harry Ruby
| starring       = Charles Correll Freeman Gosden
| music          = William Marshall
| editing        = Claude Berkeley
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =   }}
| runtime        = 77 mins.
| country        = United States
| language       = English
| budget         = $967,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56 
| gross          = 1,810,000 
}}
 radio show. The title was derived from a catchphrase associated with the show. Directed by Melville W. Brown, from a screenplay by Bert Kalmar, J. Walter Ruben, and Harry Ruby, it starred Charles Correll and Freeman Gosden in the roles of Andy and Amos, respectively, which they had created for the radio show.  The film also featured Duke Ellington and his "Cotton Club Band".

==Plot==
Amos and Andy run the "Fresh Air Taxicab Company, Incorporated", so named because their one taxi has no top.  Their old vehicle has broken down, causing a traffic jam.  Stuck in the traffic jam are John Blair and his wife, who were on their way to meet an old family friend at the train station, Richard Williams.  When the Blairs do not show up, he makes his own way to their house, where he meets their daughter, Jean, who was also his childhood sweetheart.  The two reignite their old flame, much to the chagrin of Ralph Crawford, who has been attempting to woo Jean himself.

That night, prior to attending a meeting at their lodge, the Mystic Knights of the Sea, they are hired to transport Duke Ellington and His Cotton Club band to a party being given at the Blair estate.  While they are on their way, Richard is confiding to John Blair his feelings for his daughter, and also stating that he has no intention of pursuing Jean unless he can afford to start his own business to support them.  After the death of his father, Richards family lost all their money.  He has come up to New York because his grandfather used to own a large home in Harlem, and he hopes to be able to find the deed to it, in order to sell it for the money needed to start his business. He thinks the deed must be hidden somewhere on the property itself. Unknown to Blair or Richard, is that Ralph is eavesdropping on their conversation.

After his discussion with Blair, Richard runs into Amos and Andy, who used to work for his father down south, and they are all happy to see one another.  Having delivered their fare, the two cab drivers rush back to town to attend their lodge meeting.  The lodge has an annual tradition where a pair of members must spend a night in a haunted house in Harlem, and find a document labeled, "Check and Double Check".  Once they find it, they are to replace it, in a different location, with their own version, for the lodge members to find the following year. The haunted house in question in none other than the house previously owned by Richards grandfather. 

As Amos and Andy are searching for their document, Ralph is also in the house with several of his cohorts, searching for the deed, in order to thwart Richards chances with Jean.  Amos and Andy find their document, but then realize they did not bring any other paper to write their message on and secrete for their lodge brothers.  In searching for something to write on, they stumble on the deed to premises.  As they are about to write their message on the back, they are interrupted by Ralph and his friends, who believe that the two have found the deed.  In the confusion which ensues, the cab drivers hand over what everyone believes is the deed, before they scamper out of the building.  However, when they return to the lodge, they realize that they had given the Check and Double Check paper to Ralph, instead of the dead.  They do not know the importance of the document they have, but they recognized Richards grandfathers signature on it, and intend to deliver it to Richard the following day.

After failing to find the deed, a heartbroken Richard leaves for the railway station, intending to return home.  Amos and Andy arrive at the Blair house too late to give him the deed, but race to the station and are able to hand over the deed just before Richards train leaves.  Now with the deed, Richard can sell the house, open his business, and marry Jean.  

==Cast==
* Freeman F. Gosden as Amos
* Charles J. Correll as Andy Brown
* Sue Carol as Jean Blair
* Irene Rich as Mrs Blair
* Ralf Harolde as Ralph Crawford Charles Morton as Richard Williams
* Edward Martindel as John Blair
* Rita La Roy as Elinor Crawford
* Russ Powell as Kingfish
* Roscoe Ates as Brother Arthur
* Duke Ellington as himself
* Robert Homans as butler

(Cast list as per AFI database) 

==Production==
  blacks but stereotypical voices used by the radio performers, program creators Freeman Gosden and Charles Correll performed the roles themselves in blackface.

Another problem was the attempt to base a full-length picture on a 15-minute long radio program.  In order to do this, the films producers unwisely decided to flesh out the story with a love triangle involving white characters, essentially making Amos and Andy minor characters in what was marketed as a film about them.

===Music===
Duke Ellington and his band were invited to be a part of the film, not just to provide the music but also to appear performing in the film itself. This helped propel Ellington into a national spotlight. 

The director did not want to give audiences the impression that Ellingtons band was racially integrated, and was worried that two band members were too light skinned. So valve trombonist Juan Tizol, who was Puerto Rican, and clarinetist Barney Bigard, a Creole, wore stage makeup to appear as dark as Amos and Andy on film. 

The songs included:    
*"Three Little Words" - Music by Harry Ruby, lyrics by Bert Kalmar; performed by Duke Ellington and His Cotton Club Band, vocals by The Rhythm Boys (Bing Crosby, Harry Barris, and Al Rinker)
*"Nobody Knows But the Lord" - Music by Harry Ruby, lyrics by Bert Kalmar; sung by lodge brothers
*"Ring Dem Bells" - Written by Duke Ellington; performed by Duke Ellington and His Cotton Club Band
*"Old Man Blues" - Written by Duke Ellington and Irving Mills; performed by Duke Ellington and His Cotton Club Band
*"East St. Louis Toodle-O" - Written by James "Bubber" Miley and Duke Ellington
*"Am I blue?" - Music by Harry Akst, lyrics by Grant Clark; partially sung by Freeman F. Gosden
*"The Perfect Song" - Words and music by Joseph Carl Breil; theme from the Amos and Andy radio program

==Response== profitable for Variety stating, "the best picture for children ever put on the screen".  Mordaunt Hall, film critic for the New York Times, gave the film a lackluster review, praising the efforts of Gosden and Correll, while not being as kind to the rest of the cast.     However, RKO examined ticket sale patterns and determined that the films success was due to the curiosity factor of the audience wanting to see the black-face performances of their two radio stars, but once seen, the novelty was worn off.   
 CBS television show which ran from 1951-53, although the radio show continued to be a top-rated program throughout the 1930s and 1940s.  

==Notes== public domain in the USA due to the copyright claimants failure to renew the copyright registration in the 28th year after publication. 

*The Rhythm Boys (Bing Crosby, Harry Barris, and Al Rinker), were brought in at the last minute to sing the vocals on "Three Little Words", when Ellingtons drummer, Sonny Greer, got stage fright about performing on film.   After Greer couldnt get over his fright, Bing Crosby was supposed to sing the song solo, but when Melville Brown, the director, heard Crosbys version, he reportedly said, "This guy cant sing", and the entire trio was brought in to record the song.   

==See also==
* List of films in the public domain

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 