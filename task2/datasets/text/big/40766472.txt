For Singles Only
{{multiple issues|
 
 
}}

{{Infobox film
| name = For Singles Only
| image = For_Singles_Only_Poster.jpg
| image size =
| caption =
| director = Arthur Dreifuss
| producer = Four-Leaf Productions
| writer = Hal Collins Arthur Dreifuss John Saxon Mary Ann Mobley Lana Wood
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 91 minutes
| country = United States English
}} 1968 Comedy comedy film John Saxon, Mary Ann Mobley and Lana Wood. 

==Plot==
Close friends Anne Carr and Helen Todd move into a singles complex where every tenant must be unmarried and under 30.

A couple of neighbors make a wager with bachelor playboy Bret Hendley that he cant seduce Anne successfully. Bret is too much a gentleman to accept, but when Anne learns the money would pay for Brets college education, she willingly goes along with a romance.

Mr. Parker, the buildings manager, throws an engagement party for Bret and Anne, then proceeds to evict them from the premises. While they work through their issues, Helen endures a traumatic experience, making her consider giving up men for good.

== Cast == John Saxon as Bret Hendley
* Mary Ann Mobley as Anne Carr
* Lana Wood as Helen Todd
* Peter Mark Richman as Gerald Pryor (billed as Mark Richman)
* Ann Elder as Nydia Walker
* Chris Noel as Lily
* Marty Ingels as Archibald Baldwin
* Hortense Petra as Miss Jenks
* Milton Berle as Mr Parker
* Charles Robinson III as Jim Allen

== References ==
 

== External links ==
*  
*   TCM

 
 
 
 
 


 