Virgin Witch
{{Infobox film
| name           = Virgin Witch
| image          = Virgin witch poster.jpg
| caption        = Promotional film poster Ray Austin
| producer       = Ralph Solomons Klaus Vogel (novel)
| narrator       = 
| starring       = Ann Michelle Vicki Michelle Patricia Haines
| music          = Ted Dicks
| cinematography = Gerald Moss
| editing        = Phillip Barnikel Tigon Film Distributors Ltd.
| released       = June 8, 1972 (Bismarck, North Dakota)   October 19, 1973 (Finland)
| runtime        = 88 minutes
| country        = United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 horror exploitation Ray Austin, and stars sisters Ann Michelle and Vicki Michelle.

==Plot==
Betty (Vicki Michelle) and her sister Christine (Ann Michelle) are two young models who are lured by a lecherous lesbian to spend a weekend at a country house being photographed by a trendy photographer. In reality, Christine is being set up for a virgin sacrifice and induction into a witchs coven

==Production== Man Alive concerning sex films. The revelation that prompted Cinema X magazine (Vol 4. No. 4) to remark that her films “are far removed from Miss Adair’s more cozy world of Crossroads, Hazel Adair’s other films include Clinic Exclusive (1971), Can You Keep It Up For a Week? (1974), Keep It Up Downstairs (1976), and the more mainstream Game for Vultures (1979).

“Klaus Vogel”, who wrote the script and the films tie-in novelisation, was in fact Crossroads producer Beryl Vertue. 

Virgin Witch was filmed in Surrey during 1970 and previewed in the December editions of Mayfair (magazine)|Mayfair and Continental Film Review (in which the title was referred to as "The Virgin Witch"). However, the film is copyrighted as a 1971 production, and censorship problems would mean it was not widely seen until 1972. The country house location, Pirbright, Admirals Walk  would be later used in Satans Slave (1976) and Terror (1978 film)|Terror (1978), which were both directed by Norman J. Warren.

==Censorship history==
 X rating by the Greater London Council for a limited release in the capital. The British Censor eventually relented and passed a cut version for general release in January 1972.

The film has been disowned by its sibling stars Ann and Vicki Michelle. Vickis website  makes no reference to the film, while Anns refers to it as "not an experience Ann cares to remember". 

The 1990s video release(s) on the Redemption/Salvation labels are uncut, as are the current UK and US DVD releases.  Glamour model Teresa May appeared on the cover of the 1993 UK video release of the film on the Redemption video label (she also modelled for the cover of their video release of Baron Blood and the never issued release of Don’t Deliver Us From Evil).

==References==
 

== External links ==
*  

 
 
 
 
 
 
 