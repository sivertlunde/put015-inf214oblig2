Cheerful Weather for the Wedding (film)
{{Infobox film
| name           = Cheerful Weather for the Wedding
| image          = Cheerful Weather for the Wedding 2012 Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Donald Rice
| producer       = Teun Hilte
| screenplay     = {{Plainlist|
* Donald Rice
* Mary Henely-Magill
}}
| based on       =  
| starring       = {{Plainlist|
* Felicity Jones
* Luke Treadaway
* Elizabeth McGovern
}}
| music          = Michael Price
| cinematography = John Lee
| editing        = Stephen Haren
| studio         = {{Plainlist|
* Working Title Films
* BBC Films
}}
| distributor    = {{Plainlist|
* Universal Pictures   Capcom Pictures  
}}
| released       =  
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Cheerful Weather for the Wedding is a 2012 British comedy drama film directed by Donald Rice and starring Felicity Jones, Luke Treadaway, and Elizabeth McGovern. Adapted from the 1932 novella Cheerful Weather for the Wedding by Julia Strachey of the Bloomsbury Group, the film is about a young woman on her wedding day who worries that shes about to marry the wrong man, while both her fiancé and her former lover grow increasingly anxious about the event. The film premiered at the Tribeca Film Festival on 20 April 2012.   

==Plot==
Today is Dollys (Felicity Jones) wedding day, and her family is arriving at the manor house with all the cheerfulness, chaos and petty grievances that bubble to the surface at such gatherings. Trouble soon appears with the arrival of Joseph (Luke Treadaway), Dollys lover from the previous summer, who throws her feelings into turmoil. To her mothers (Elizabeth McGovern) exasperation, his presence threatens to upset the design she had for her daughters future. Dolly, for her part, just cannot decide whether to run away with Joseph or start a new life in Argentina with her husband to be.

==Cast==
* Felicity Jones as Dolly Thatcham
* Luke Treadaway as Joseph Patten
* Elizabeth McGovern as Mrs. Thatcham
* Mackenzie Crook as David Daken
* Eva Traynor as Annie 
* Zoe Tapper as Evelyn Graham
* Paola Dionisotti as Mrs. Whitstable James Norton as Owen
* Sophie Stanton as Millman
* Elizabeth Webster as Betty
* Kenneth Collard as Whitstable
* Ellie Kendrick as Kitty Thatcham 
* Ben Greaves-Neil as Jimmy Dakin
* Luke Ward-Wilkinson as Robert
* Olly Alexander as Tom
* Joanna Hole as Miss Spoon
* John Standing as Horace Spigott 

==References==
 

==External links==
*  
*  

 
 
 
 