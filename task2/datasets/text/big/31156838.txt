Symphony (film)
{{Infobox film
| name           = Symphony
| image          = 
| image size  =
| caption        =
| director       = I. V. Sasi
| producer       = M N Thankachan for EGNA Films
| writer         = Mahesh Mithra
| narrator       =
| starring       =
| music          = Deepak Dev
| cinematography = K.V.Suresh
| editing        = Vijaya Kumar
| distributor    = EGNA Films
| released       = 13 February 2004
| runtime        =
| country        = India Malayalam
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 2004 Cinema Indian feature directed by I. V. Sasi and produced by  M N Thankachan, starring new comers Siva and Anu Sasi with Swathi Varma, Riyaz Khan, Jagadish and Jagathi Sreekumar  

==Plot==

Symphony is the story of Jijo Samson (Shiva), a musician. His troupe comprises Shruthi (Anu) who has left her family as she is in love with Jijo. One day they reach a lake-side resort to compose a new music album for Dominic Kalarickal (Jagathy Sreekumar). Jijo meets Sandra (Swati Verma) there and he falls for her despite knowing that she is a married woman to Satyanath (Riaz Khan). An extra martial affair develops as Shruthi is shattered. Things hot up as Satyanath arrives...

==Cast==

*Shiva ... Jijo Samson
*Anu Sasi  ...  Shruthi (as Anu I. V.) 
*Swathi Varma  ...  Sandra Sathyanath  
*Riyaz Khan  ...  Sathyanath  
*Jagadish  ...  Manoharan  
*Jagathi Sreekumar  ...  Dominic  
*Sudheesh  ...  Seby  
*Lakshmi Gopalaswamy  ...  Sindhu  
*Sukumari  ...  Deenamma  
*Kaithapram Damodaran Namboothiri ...  Himself

==Trivia==

I.V.Sasi launched his daughter Anu as a heroine with this film.

==References==
 

==External links==
* 

 
 
 


 