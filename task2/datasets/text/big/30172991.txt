Dancing Co-Ed
{{Infobox film
| name           = Dancing Co-ed
| image_size     =
| image	         = Dancing Co-Ed FilmPoster.jpeg
| caption        =
| director       = S. Sylvan Simon
| producer       = Edgar Selwyn
| writer         = Albert Treynor (story) Albert Mannheimer Herbert Fields (uncredited)
| narrator       = Richard Carlson Artie Shaw David Snell
| cinematography =
| editing        =
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews, Inc.
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $425,000  . 
| gross          = $713,000 
}} Richard Carlson as an inquisitive college reporter, and bandleader Artie Shaw as himself.

==Plot==
When a dancers partner becomes pregnant, a nation-wide search is instituted to find a replacement from among college women. A perfect choice is found, but she is not in school, resulting in various hijinks.

==Cast==
*Lana Turner as Patty Marlow Richard Carlson as Michael "Pug" Braddock
*Artie Shaw as Himself
*Ann Rutherford as Eve Greeley
*Lee Bowman as Freddy Tobin
*Thurston Hall as Henry W. Workman
*Leon Errol as Sam "Pops" Marlow
*Roscoe Karns as Joe Drews
*Mary Field as Miss Jenny May
*Walter Kingsford as President Cavendish
*Mary Beth Hughes as "Toddy" Tobin
*June Preisser as "Ticky" James
*Monty Woolley as Professor Lange
*Chester Clute as Pee Wee

Veronica Lake has a small uncredited part.

==Box office==
According to MGM records the film earned $518,000 in the US and Canada and $195,000 elsewhere resulting in a profit of $21,000. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 