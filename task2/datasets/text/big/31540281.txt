Premer Taj Mahal
{{Infobox Film
| name           = Premer Taj Mahal
| image          = Premer Taj Mahal.jpg
| image_size     = 200px
| caption        = VCD Cover
| director       = Gazi Mahbub
| producer       = Mohammad Jashim Uddin
| writer         = Gazi Jahangir Riaz Shabnur Wasimul Bari Rajib Abul Hayat Misha Sawdagar Anowar Hossain Rawshan Zamil Rasheda Chowdhury Afzal Sharif Neela
| music          = Ahmed Imtiaz Bulbul
| cinematography = Mahfuzur Rahman Khan 
| editing        = Zinnat Hossain
| distributor    = Riya Kothachitra
| released       = 2002
| runtime        = 160 Min.
| country        = Bangladesh Bengali
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
| imdb_id        = 
}}

Premer Taj Mahal also (  meaning of  ) is a Bangladeshi Bengali language film. Released was January 2002 all over Bangladesh. Directed by Gazi Mahbub  written by Gazi Jahangir. Produced by Mohammad Jashim Uddin and distributed by Riya Kothachitra. The film fully family drama with hard love story baesd. Stars Riaz (Actor)|Riaz, Shabnur, Wasimul Bari Rajib, Abul Hayat, Misha Sawdagar, Anowar Hossain, Rawshan Zamil, Rasheda Chowdhury, Afzal Sharif and many more.

==Plot==
“Robyn” (Riaz (Actor)|Riaz) and “Liza” (Shabnur) these young duo love each other with their heart and soul. But Robyn does not know about Liza’s background same as him Liza does not know about Robyn. So,afterward there was a big problem with their Religion. Robyn was a Muslim and Liza was a Christian. When they take a step of love marriage Liza went to Church. But Robyn by listening to her sayings laughs and thought she was joking with him and Robyn says their marriage will be in a Kazi Office as role of Islam. Liza was shocked when she learned that he was Muslim also Robyn was shocked knowing she was Christian. They then separates and tries to forget each other, but first love is not easily removable. So, they are back with an alternative decision as love is love, in love do not involve any Religion. So, your Religion is great for you also mine to me. One day,their family knows about this situation and takes action. Robyn’s father “Raihan Chowdhury” (Abul Hayat) and Liza’s father “Abraham Dikosta” (Wasimul Bari Rajib) start a family war for previous business bad relation. In the meantime, Robyn and Liza ready for die but not put out of their mind.

End of the story find new turn as Liza was not Abraham Dikosta’s daughter. Liza’s born a Muslim family her father is “Abdul Rahim” (Anowar Hossain). One day, Abdul Rahim’s full family and Abraham Dikosta full family gets crosses a river on engine boat in the mean time there start a cyclone. And for the cyclone everyone was lost in the river. The survivors, they searched for their family member.  Meanwhile, Abraham Dikosta finds a baby and he knew that the daughter “Alo” was Abdul Rahim’s. Abraham Dikosta took Alo with him and went, on the other side Abdul Rahim was mad, for losing all of his family member. But he knew that his daughter Alo was alive. And after many year Abdul Rahim met with Abraham Dikosta suddenly. Afterward everybody knows the real story and there was no problem for Robyn and Liza to marry.

==Cast== Riaz as Robyn Chowdhury
* Shabnur as Liza Dikosta (Alo)
* Wasimul Bari Rajib as Abraham Dikosta
* Abul Hayat as Raihan Chowdhury
* Rawshan Zamil as Liza’s Grad Mother
* Rasheda Chowdhury as Robyn’s Mother
* Afzal Sharif as Mintu
* Misha Sawdagar as Diferson Dikosta
* Neela as Neela
* Anowar Hossain as Abdul Rahim
* Nasrir as Special appreance
* Dulary as

==Awards and achievement==

===National Film Awards===
Premer Taj Mahal films gets won National Film Awards total two category in the year 2002.
* Won Best Male Singer: Monir Khan, for song Ei Buke Boichhe Jamuna.2002
* Won Best Female singer: Kanak Chapa, for song Ei Buke Boichhe Jamuna 2002

==Music==
Premer Taj Mahal films music directed by Bangladeshi famous lyricar and composer Ahmed Imtiaz Bulbul. The film Monir Khan and Kanak Chapa gets won National Film Awards best singer of the year 2002 song as Ei Buke Boichhe Jamuna.

===Soundtrack===
{{Infobox album 
| Name = Premer Taj Mahal
| Type = soundtrack
| Cover = 
| Artist = 
| Released = 2002 (Bangladesh)
| Recorded = 2001
| Genre = Films Sound Track
| Length =
| Label = 
| Producer = ANUPAM
}}

{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Title !! Singers !! Performers !! Notes
|- 1
|O Priya O Priya Andrew Kishore and Kanak Chapa Riaz (Actor)|Riaz and Shabnur
|
|- 2
|Chhotto Ekta Jibon Niye Andrew Kishore and Kanak Chapa Riaz (Actor)|Riaz and Shabnur
|
|- 3
|Didimoni O Didimoni Kumar Biswajit, Kanak Chapa and …
|Rajib, Rawshan Zamil and Shabnur
|
|- 4
|Ei Buke Boichhe Jamuna Monir Khan and Kanak Chapa Riaz (Actor)|Riaz and Shabnur Title Song
|- 5
|Sathire Sathi Amar Ange Biplob and … Misha Sawdagar and Nasrin
|
|-
 

==Box office==
Premer Taj Mahal film released was 2002 in allover Bangladesh and take blockbuster-hit at the box office result.

==References==
 

==External links==
* 

 
 
 
 
 
 

 