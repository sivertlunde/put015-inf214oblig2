Headline Shooter
 
{{Infobox film
| name           = Headline Shooter
| image          = HeadlineShooterWindowCard.jpg
| alt            = 
| caption        = Midget window card
| film name      = 
| director       = Otto Brower    Ray Lissner (assistant)  David Lewis 
| writer         = Agnes Christine Johnston  Allen Rivkin  Arthur Kober 
| screenplay     = 
| story          = 
| based on       =   
| starring       = William Gargan Frances Dee Ralph Bellamy Jack LaRue
| narrator       = 
| music          = Max Steiner 
| cinematography = Nick Musuraca 
| editing        = Fred Knudtson  RKO Radio Pictures
| distributor    = 
| released       =   }}
| runtime        = 60 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Headline Shooter is a 1933 American Pre-Code Hollywood|pre-code drama about the life of a newsreel photographer. Director Otto Brower intertwined the screenplay written by Agnes Christine Johnston, Allen Rivkin, and Arthur Kober, with actual newsreel footage of natural and man-made disasters.  The film starred William Gargan, Frances Dee, Ralph Bellamy, and Jack LaRue.

==Plot==
Bill Allen (William Gargan) and his friend, Mike (Wallace Ford) are newsreel photographers who have a friendly rivalry, each willing to do whatever it takes to get the better footage of a story.  When covering a beauty contest, Bill plans to rig the results by bribing the judges, thus enabling him to get the scoop on his rival cameramen, and already have pictures of the winner. While covering the event, he meets a reporter, Jane Mallory (Frances Dee), who is a straight arrow, in contrast to the loose women that Bill seems to attract. A professional rivalry simmers between the two, and when they both cover an earthquake in California, Bill begins to fall for Jane. Jane rebuffs his advances, letting Bill know that she has a fiancé down in Mississippi, a banker by the name of Hal Caldwell (Ralph Bellamy).

As time goes by, they continue to run into each other.  Eventually, Jane begins to reciprocate Bills affection, but his reputation as a womanizer makes her continue to resist.  At one point, Bill is trying to get her to break off her engagement to Hal, and marry him instead, and just as she begins to weaken, he hears of a huge fire back in New York, and rushes off, leaving her in the lurch.  After he leaves, Jane sends word to her boss that she is quitting, and heads down to Mississippi to marry Hal.

At the fire, he meets up with his friend Mike, but the meeting ends tragically, when Mike is killed in the fire, attempting to get the perfect shot. Disconsolate over losing both his girl and his best friend, he intends to resign his job, until his boss sends him down to Mississippi to cover the failure of a levee, which has led to massive flooding. While covering the flood, Bill uncovers the corruption which led to the faulty construction of the levee, resulting in the levees failure. He also begins to win back the affection of Jane. He heads back to New York with the footage.

When Jane and Hal realize that a friend of theirs, Judge Beacon (Henry Walthall), is the father-in-law of the person responsible for the corruption, they rush off to New York, where they attempt to get Bill to destroy the evidence implicating their friend. He doesnt, and when the newsreel comes out, the Judge commits suicide in disgrace. Jane resolves to return to Mississippi with Hal, but she cant resist covering one more story, that of taking the confession of a gangsters moll. When the story is published, Jane is kidnapped by the gangster. Bill realizes his own contact in the crime world, Ricci (Jack LaRue), will know where they have taken, and dupes Ricci into revealing where she is being held. Bill and Hal rush to her rescue, arriving at the same time as the police. Ever the newsman, during the ensuing siege and shootout, Bill manages to get some excellent footage for the newsreels.  Jane realizes that she is truly in love with Bill, and agrees to marry him, which is caught by the newsreel cameras.  Hal returns to his very staid life in Mississippi.

==Cast==
*William Gargan as Bill Allen
*Frances Dee as Jane Mallory
*Ralph Bellamy as Hal Caldwell
*Jack LaRue as Ricci
*Gregory Ratoff as Hermie Gottlieb
*Wallace Ford as Mike
*Robert Benchley as Radio announcer
*Betty Furness as Miss Saunders
*Hobart Cavanaugh as Happy
*June Brewster as Betty Kane
*Franklin Pangborn as Mr. Adolphus G. Crocker
*Dorothy Burgess as Burnetts moll
*Purnell Pratt as Eddie Edmunds
*Henry B. Walthall as Judge Beacon Bill Hudson as Ed
*Mary MacLaren as Murderess
*Kitty Kelly as Sue
 AFI database) 

==Production==
In early March, 1933, RKO announced that they were producing a film about two newsreel cameramen, originally titled News Reel, Harold Shumate was supposed to be the screenwriter.  Later that month, on March 23, RKO further announced that Sarah Y. Mason and William Ulman, Jr. had been chosen to collaborate on the screenplay.  The two had earlier worked on another film, Hock Shop. 

In April, it was announced that two cameramen from the Pathe newsreel group were being brought in to consult on the film, still tentatively titled, News Reel.  Further Pathe participation was developed when Jack Connolly, Pathe News general manager was flown to Hollywood to consult on the picture.  On April 25, RKO announced that the film, now no longer being called News Reel, but simply an untitled film about the news reel business, would begin filming in early May. 

RKO announced on May 1, 1933, that Eric Linden had been chosen to be cast in the film.  Also at the beginning of May it was announced that RKO was changing the title of the film from News Reel to Headline Shooters.  In mid-May, it was announced that Helen Mack was cast to star in the film, although by the beginning of June she had already been replaced by Frances Dee.    Dorothy Burgess, Hobart Cavanaugh, Mary MacLaren, June Brewster, and Gregory Ratoff were announced as members of the cast by RKO on May 23, 1933.  

Variety listed the title of the film as Headline Shooters on June 6, and as being in its 4th week of filming.  The cast was as listed in the cast list in this article, however Arthur Kober had not been given credit at that point. 

As late as July 8, the film was still being referred to as Headline Shooters, and was even referred to in this way after release, as per a listing in the Motion Picture Herald on August 5.     Filming was completed and editing begun on the film by mid-July, 1933.  The film was released under the title, Hollywood Shooter on July 28, 1933.  

==Reception== The Film Daily, gave it an okay review, calling it "fairly entertaining", but criticizing the films story. 

==External links==
*  
*  

 

==References==
 

 
 
 
 
 
 
 
 
 
 
 