The Story of a Crime
{{Infobox film
| name           = The Story of a Crime
| image          = 
| alt            =
| caption        = 
| film name      = История одного преступления
| director       =  Fyodor Khitruk
| producer       = 
| writer         = 
| screenplay     = Michael Volpin
| story          = 
| based on       =  
| starring       = 
| narrator       = Zinovy Gerdt
| music          = Andrey Babaev
| cinematography = 
| editing        = 
| studio         = Soyuzmultfilm
| distributor    = 
| released       =  
| runtime        = 19 min. 54 sec.
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          =  
}} Soviet animated film directed by Fyodor Khitruk and based on a screenplay by Michael Volpin. It was produced by Soyuzmultfilm. The score is by Andrey Babaev, with sound editing by George Martynuk.

It was the first film by Khitruk, whose role in the history of Russian animation led him to be recognized as a Peoples Artist of the USSR and a Meritorious Artist, and to receive the Order of the Red Banner of Labour and the Order "For Merit to the Fatherland".

== Plot summary ==
Noises at night made by rude neighbors cause the very friendly and peaceful Vasily Mamin to commit a crime.
The cartoon serves as an explanation as of why such a brutal crime is committed at the beginning. It is a flashback at how Mamin spent his last 24 hours before the crime.

== Voice cast ==
* Zinovy Gerdt as Narrator

== Awards == International Short Film Festival in Oberkhauzen (Federal Republic of Germany), 1963
* Diploma and The Golden Gates Prize of the 7th International Cinema Festival in San Francisco, 1962

== External links ==
* 
*  at animator.ru
*  at Tampere Film Festival web page
*  by William Moritz
*  at Kinokultura.com

 
 
 
 


 