Il pranzo della domenica
{{Infobox film
| name = Il pranzo della domenica
| image = Il pranzo della domenica.jpg
| caption =
| director = Carlo Vanzina
| writer = Enrico Vanzina Carlo Vanzina
| starring = Massimo Ghini Maurizio Mattioli Giovanna Ralli Rocco Papaleo
| music = Alberto Caruso
| cinematography = Claudio Zamarion
| editing =
| released =  
| country = Italy
| runtime = 100 minutes
| language = Italian  
}} 2003 Italian comedy film directed by Carlo Vanzina.   For her performance Giovanna Ralli was nominated for Nastro dArgento for Best Supporting Actress, while Rocco Papaleo and Maurizio Mattioli were both nominated for Nastro dArgento for Best Supporting Actor. 

== Plot ==
Franca Malorni has three daughters with whom she has an obsessive relationship. In fact, she can not break away from them and pretend that every Sunday the girls are from her to eat lunch with their families. When Franca breaks femur and goes to the hospital, the three daughters begin to think about the will...

== Cast ==
*Massimo Ghini: Massimo Papi
*Elena Sofia Ricci: Sofia Lo Iacono
*Barbara De Rossi: Barbara
*Galatea Ranzi: Susanna Papi
*Rocco Papaleo: Nicola Lo Iacono
*Maurizio Mattioli: Maurizio 
*Giovanna Ralli: Franca Malorni
*Paolo Triestino:  Luzi
*Marco Messeri: Marquis 
*Gianfranco Barra: On. Torrisi 
*Angelo Bernabucci: Consigliere Calcio

==References==
 

==External links==
* 

 

 
 
 
 
 


 
 