Inspirations (film)
{{Infobox film
| name           = Inspirations
| image          =
| image_size     =
| caption        =
| director       = Michael Apted
| producer       = Jody Allen Michael Apted Eileen Gregory Paul Allen
| based on       = A concept by Paul Allen
| narrator       =
| starring       = Tadao Ando David Bowie Dale Chihuly Louise LeCavalier Roy Lichtenstein Édouard Lock Nora Naranjo-Morse
| music          = Patrick Seymour
| cinematography = Maryse Alberti Amnon Zlayet
| editing        = Susanne Rostock
| studio         = Argo Films Clear Blue Sky Productions
| distributor    =
| released       =  
| runtime        = 100 min.
| country        = United States Israel
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
Inspirations is a 1997 documentary directed by Michael Apted.

==Synopsis==
The film explores creativity in the arts through interviews with several prominent figures known for a variety of artistic media:
*Musician David Bowie.
*Pop artist Roy Lichtenstein.
*Glass sculptor Dale Chihuly.
*Dance choreographer Édouard Lock.
*Dancer Louise LeCavalier.
*Potter/poet Nora Naranjo-Morse.
*Architect Tadao Ando.

==External links==
* 

 

 
 
 

 