The Navy Lark (film)
 
 
 

{{Infobox Film
| name           = The Navy Lark
| image_size     = 
| image	=	The Navy Lark FilmPoster.jpeg
| caption        =  Gordon Parry
| producer       = Herbert Wilcox
| writer         = Laurie Wyman Sid Colin
| narrator       = 
| starring       = Tommy Reilly
| cinematography = Gordon Dines
| editing        = Basil Warren
| distributor    = Twentieth Century-Fox
| released       = 
| runtime        = 78 minutes
| country        = United Kingdom 
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1959 British Gordon Jackson West Bay, Bridport, Dorset.  Only Phillips had appeared from the radio version - all other parts were recast. Hello: The Autobiography, Leslie Phillips, Orion, 2006  The film was produced at Walton-on-Thames.

==Overview==
Captain Povey has built up a reputation for shutting down redundant naval bases and now has his eye on the minesweeping detachment on Boonzey island (55.5 miles from Portsmouth, a Channel Island). Arriving on inspection, he is told tales of finding many mines in the sea there and not believing them goes out on the Compton minesweeper. The crew were supposed to find "Bessy", a mine shaped object used to collect Lifeboat funds but found a real mine instead, which Pouter bashes about in an effort to take it apart. Released, it explodes nearby and this convinces Povey that the incompetents there are not up to the job and he decides on using a competent crew to do the job.

CPO Banyard uses his "Polsons Fulminator Mark III" trick (it does not exist) to delay their decommissioning and what started off as a thin folder goes around the military offices and comes back to Poveys office as a mountain of paper work. He sees through it and goes back to the island only to be told there has been an outbreak of "Yellow Fever" there. He is taken in and leaves but decides to return and the trick is revealed as life is back to normal there. Now more than ever he is determined to shut them all down.

Gaston Higgins, a Frenchman, owns the local bar and when he gets drunk he talks of revolution and kicking the British off of the island. They decide to use him and say they are under siege from revolutionaries. Povey knows this is another trick and officially gives them three days to leave the island, but his bosses and the government believe the story when they get reports from a reporter, Lt Binns, who was sent there to take photographs. Questions are asked by the British and French governments and Poveys career is on the line as he is told to sort this out as the British do not run from the French.

Povey goes to the island and a fake attack on Gaston and his men is launched but Povey finds out it was all a hoax. Ready to hand out court martials all round, Povey is confronted with a picture Binns took of him leading an all out attack on what is now known to be a hoax, which will be front page news across the world tomorrow. Stanton talks him into seeing sense and Povey, with his career in tatters if it gets out, tears up his report. He leaves and life goes back to normal on the island. On the way back to Portsmouth, their boat hits another real sea mine and Povey, Binns and the others are left to swim back to base.

==Cast==
* Cecil Parker as Commander Stanton
* Ronald Shiner as Chief Petty Officer Banyad
* Leslie Phillips as Lieutenant Pouter
* Elvi Hale as Leading WREN Heather Stark
* Nicholas Phipps as Captain Povey
* Cardew Robinson as Lieutenant Binns Gordon Jackson as Leading Seaman Johnson
* Harold Kasket as Gaston Higgins
* Hattie Jacques as Fortune Teller
* Reginald Beckwith as CNI
* Kenneth J. Warren as Brown
* Wanda Ventham as Mabel
* Richard Coleman as Lieutenant Bates
* Llewellyn Rees as Admiral Troutbridge
* Clive Morton as Rear Admiral Gordon Harris as Group Captain
* Van Boolen as Fred
* Gordon Whiting as Commander Tom Gill as Naval Commander
* Walter Hudd as Naval Captain

==References==

 

==External links==
* 
* 
* 

 

 
 
 
 
 
 