Hulchul (2004 film)
{{Infobox film
| name              = Hulchul
| director          = Priyadarshan
| image = Hulchul FilmPoster.jpeg
| producer          = Ratan Jain Ganesh Jain
| writer            = K. Saxena (Dialogues) Siddique Lal Lal
| screenplay        = Neeraj Vora Arbaaz Khan Paresh Rawal Vidyasagar
| editing           = Arun Kumar
| cinematography    = Jeeva
| distributor       = Venus Records & Tapes
| released          =  
| runtime           = 166 min
| country           = India Hindi
| awards            =
| budget            =
| gross            = 
}}
 Arbaaz Khan, Shakti Kapoor, Farha Naaz, and Lakshmi (actress)|Laxmi.   It is a satirical portrayal of the post-modern Indian womans self-image, self-confidence, self-victimization, and self-empowerment.

==Plot== Arbaaz Khan) and Jai (Akshaye Khanna). Since Balram is of marriageable age, he arranges his marriage with the daughter of Laxmi Devi (Lakshmi (actress)|Laxmi), who opposes this marriage. During the wedding ceremony, Laxmi Devi and her sons, Surajbhan, and Pratap forcibly take away their sister and accidentally kill Parvati. They subsequently get their sister married to Kashinath (Shakti Kapoor). An enraged Angar hacks Laxmis husband to death, is arrested and sentenced to 14 years in prison. When he returns he makes it clear that women will not be permitted in his house, posting a sign on the front gate, and forbids his sons to ever marry.

Angarchand and his sons prevent the marriage of Anjali (Kareena Kapoor), Laxmis granddaughter. Jai humiliates Anjali when she returns to College after her failed engagement. Laxmi convinces Anjali to trick Jai into falling in love with her to cause a rift in the family. Jai meets with Anjali who openly admits that she is attracted to him. Jai, at first rejects her, then decides to teach her and her family a lesson. Both feign their love for each other, but subsequently Anjali and Jai admit their agendas. Both go their separate ways, but find that they are indeed in love with each other. In order to marry, Jai must first convince his elder brothers to get married albeit in vain as none of them even wants to consider this option.

Lucky (Arshad Warsi), Jais friend, finds out that Kishan, a seemingly celibate devotee of Lord Hanuman, is leading a dual life as Murari, who has been married to a woman, Gopi (Farha Naaz), from Gangapur, for 7 years and has two children, a son and a daughter. When Angar finds out he kicks Kishan and Jai out of his house because Jai supports Kishans marriage.

Anjali marriage is fixed with Laxmis advocates son (Sharad Kelkar), but after Jai and Anjali acknowledge their love for each other to her family, Pratap and Surajbhan approach Angarchand to help them stop Jai from disrupting this marriage on one hand, while Laxmi tells Jai to marry Anjali - setting off confrontation between the estranged sons and father. Jai, Kishan, Gopi, and Lucky sneak into the wedding venue with the help of Anjalis uncle Veeru (Sunil Shetty). Jai is dressed as the bridegroom and marries Anjali. Once Laxmi recognizes Jai in disguise, she demands Angarchand cut his own sons throat. Jai tells his father he wont go through with the wedding without his approval.

Balram the oldest of the brothers finally tells Jai to place the wedding necklace around Anjalis neck which finalizes the wedding. The necklace is grabbed from Jais hands and Anjalis family chases Jai to stop him, but in the end Jai succeeds and the marriage is completed. Anjali asks her grandmother to end the feud and Jai asks his father for forgiveness. Angar leaves the wedding and contemplates all his sons actions on the way home. All brothers and the two wives arrive at the gate. Angar looks at his sons and takes the sign off the gate and finally welcomes women back into his family.

==Cast==
* Suniel Shetty  as  Veer / Veeru
* Akshaye Khanna  as  Jai Angar Chand
* Kareena Kapoor  as  Anjali
* Amrish Puri  as  Angar Chand
* Paresh Rawal  as  Kishen Angar Chand / Murari
* Jackie Shroff  as  Balram Angar Chand / Balli Arbaaz Khan  as  Shakti Angar Chand
* Arshad Warsi  as  Lucky, Jais Friend
* Farah Naaz  as  Gopi, Kishens wife
* Deep Dhillon  as  Pratap
* Akhilendra Mishra  as  Surajbhan / Surya
* Asrani  as  Advocate Mishra Manoj Joshi  as  Laxmis Advocate
* Sharad Kelkar  as  Lawyers son, whose marriage is fixed with Anjali
* Shakti Kapoor  as  Kashinath
* Upasana Singh  as  Mrs. Surya Lakshmi  as  Laxmi Devi
* Mumaith Khan in a Special Appearance in song, "Lut Gayee"
* Zubein Khan in a Special Appearance in song, "Lut Gayee"

==Soundtrack==
{{Track listing
| heading = Songs
| extra_column = Playback Vidyasagar
| all_lyrics =

| title1 = Dekho Zara Dekho 
| extra1 = Udit Narayan, Kunal Ganjawala
| length1 = 5:47

| title2 = Hum Dil Ke Shaan
| length2 = 4:56

| title3 = Ishq Mein Pyar Mein 
| extra3 = Alka Yagnik, Shaan
| length3 = 4:57

| title4 = Lut Gayee Poornima & Raja Lakshmi
| length4 = 5:18

| title5 = Lee Humne Thi Kasam  Hariharan
| length5 = 4:49

| title6 = Rafta Rafta 
| extra6 = Udit Narayan, Sujatha Mohan
| length6 = 5:17
}}

==Trivia==
* This was one of the last films Amrish Puri appeared in before he died.
* This is one of four films that Akshaye Khanna has starred in under Priyadarshans direction.

== References ==
 

== External links ==
*  
*  
 

 
 
 
 
 
 