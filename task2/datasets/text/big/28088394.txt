Daniel Boone (1936 film)
{{Infobox film
| name           = Daniel Boone
| image          =
| image_size     =
| caption        = David Howard
| producer       = Leonard Goldstein (associate producer) George A. Hirliman (producer)
| writer         = Edgecumb Pinchon (story) Daniel Jarrett (screenplay)
| narrator       = George OBrien Heather Angel John Carradine
| music          = Arthur Kay Hugo Riesenfeld
| cinematography = Frank B. Good
| editing        = Ralph Dixon
| distributor    = RKO Pictures
| released       = October 16, 1936
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} David Howard George OBrien, Heather Angel and John Carradine.

==Synopsis==
In 1775, Daniel Boone leads thirty colonial families to Kentucky where they face two threats: Native American raiders led by renegade white Simon Girty, who opposes the colony; and the schemes of effete Stephen Marlowe to seize title to the new lands. Perils, battles, escapes, and a love interest round out the film.

==Cast== George OBrien as Daniel Boone Heather Angel as Virginia Randolph
*John Carradine as Simon Girty
*Ralph Forbes as Stephen Marlowe
*George Regas as Black Eagle
*Dickie Jones as Master Jerry Randolph
*Clarence Muse as Pompey
*Huntley Gordon as Sir John Randolph
*Harry Cording as Joe Burch
*Aggie Herring as Mrs. Mary Burch
*Crauford Kent as Attorney General
*Keith Hitchcock as Commissionner
*Chief John Big Tree as Wyandotte Warrior (uncredited)
*Dick Curtis as John Finch - Frontiersman (uncredited)
*Baron James Lichter as Ben Stevens (uncredited)
*John Merton as Messenger from Richmond (uncredited)
*Edward Peil, Sr. as Frontiersman in Lone Wagon (uncredited)
*Tom Ricketts as Attorney Generals Associate (uncredited)

==Soundtrack==
*Clarence Muse - "Roll on, Wheel" (Written by Clarence Muse)
*Clarence Muse - "Make Way" (Written by Jack Stern, Grace Hamilton and Harry Tobias)
*Chorus - "In My Garden" (Music by Jack Stern, lyrics by Grace Hamilton)

==External links==
 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 