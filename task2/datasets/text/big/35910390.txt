The Masseurs and a Woman
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Masseurs and a Woman
| image          =
| caption        = Hiroshi Shimizu
| producer       = Hiroshi Shimizu
| starring       = Mieko Takamine Shin Tokudaiji Shin Saburi
| music          =
| cinematography = Masao Saitō
| editing        =
| studio         = Shochiku
| distributor    =
| released       =  
| runtime        = 66 minutes
| country        = Japan
| awards         = Japanese
| budget         =
}} Hiroshi Shimizu and released by the Shochiku studio.  It features Mieko Takamine, Shin Tokudaiji, Shinichi Himori, Bakudan Kozo, Shin Saburi, Takeshi Sakamoto, Hideko Kasuga, Munenobu Yuri, Yoshitaro Iijima, Tsumeo Osugi, and Shotaro Akagi.  The movie follows Toku a blind masseur and a woman from Tokyo that he meets.

==Plot==
The movie opens on Toku and his fellow blind masseur friend Fuku walking down a mountain path talking about how they take pride in how many people they pass on their walks even though they are blind.  They are walking north to work in a village of inns.  Once there they perform their jobs and Toku encounters a woman from Tokyo who had ridden in a carriage that passed them on the road.  He recognizes her by her distinct Tokyo smell.  The next few scenes are comedic encounters with other customers and in particularly with a boy and his uncle.  This boy meets the lady from Tokyo and she meets his uncle who becomes smitten with her.  This makes Toku sad.  All the while there is a rash of thefts of guest’s money while they take baths.  Toku believes the woman is doing this and tries to have her escape, but finds out that she is not the one doing it.  Instead all the reasons he thought it was her she explains as that she was a mistress of a man she didn’t like and was on the run from him.  The next day Toku finds her sitting in a carriage, they say nothing but a man with a police officer also enters the carriage.  It is unsaid but I assume that the man is her patron and the carriage takes off as a saddened Toku watches.

==References==
 Hiroshi Shimizu, "The Masseurs and a Woman", 1938

==External links==
*  
*Blakeslee, David,  , Criterioncast

 
 
 
 
 

 