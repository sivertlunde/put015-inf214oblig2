The Famous Box Trick
{{Infobox film
| name           = The Famous Box Trick
| image          = TheFamousBoxTrick.jpg
| image_size     =
| caption        = Screenshot from the film
| director       = Georges Méliès
| producer       =
| writer         =
| narrator       =
| starring       =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = France Silent
| budget         =
| gross          =
}}
 1898 France|French short black-and-white silent trick film, directed by Georges Méliès, featuring a stage magician who transforms one boy into two with the aid of an axe. The film, "harks back to stage magic," and, according to Michael Brooke of BFI Screenonline, "can be viewed as a kind of sequel to The Vanishing Lady (Escamotage d’une dame chez Robert-Houdin, 1896) in that it reprises many of the same elements." 

==Synopsis==
A stage magician conjures up a dove which he then places in a box with a set of clothes. A boy appears from the box, who the magician divides into two with an axe. The two boys squabble and the magician transforms one into a paper tissue which he shreds and places the other back in the box. The magician then destroys the box with a hammer to show the boy has vanished. The boy reappears and is transformed into flags. The magician then disappears in a puff of smoke only to re-enter through a door to take his bow.

==Current status==
Given its age, this short film is available to freely download from the Internet.

==References==
 

== External links ==
*  
*   on YouTube

 
 
 
 
 

 