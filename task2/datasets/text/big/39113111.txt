Puerta de Hierro, el exilio de Perón
{{Infobox film
| name           = Puerta de Hierro, el exilio de Perón
| image          = Puerta de Hierro film.jpg
| alt            = 
| caption        = Víctor Laplace as Juan Perón
| director       = Víctor Laplace Dieguillo Fernández
| producer       = 
| writer         = Víctor Laplace Leonel DAgostino
| starring       = Víctor Laplace Victoria Carreras Natalia Mateo Fito Yanelli Javier Lombardo Manuel Vicente Sergio Surraco Federico Luppi
| music          = Damián Laplace
| cinematography = 
| editing        = 
| studio         = 3C Films
| distributor    = 
| released       =   
| runtime        = 108 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          = 
}}
Puerta de Hierro, el exilio de Perón ( ) is a 2012 Argentine film. It is based on the exile of Argentine president Juan Perón, who was deposed in 1955 by a military coup named Revolución Libertadora, left the country, and finally returned in 1973. The actor playing Perón is Víctor Laplace. 

==Title== Puerta de Hierro, the Madrid residential neighbourhood where Perón had his villa.
==Plot==
Close to his final return to Argentina, Perón begins to record in tapes his memoirs of the exile. He begins with the bombing of Plaza de Mayo and the Revolución Libertadora military coup against him, which caused his departure from the country. He met the Argentine dancer Isabel Martínez de Perón|María Estela Martínez in Panamá, who stayed with him. He finally settled in Spain, and became a friend of his doctor and a tailor named Sofía who did not recognize him. Despite of his distance of the country, he stayed informed of all the ongoing political developments, and several Argentine political figures visited him.

José López Rega began to work at the house, and became an influential figure over Isabel. Perón made an attempt to return to Argentina by laying clues that he would do so through Paraguay and fly instead to Brazil; but the Brazilian military forced him to return to Spain. Perón revealed his identity to Sofía before making the flight, which redefined their relation since then. The political crisis in Argentina worsened, and de facto president Alejandro Agustín Lanusse finally agreed to lift the proscription of Peronism and allow the return of Perón. Perón recorded his memoirs of the exile for Sofía, and his last tape ended with the tango song "Carlos Gardel|Volver" ( ).

==Creation==
Puerta de Hierro is the fifth film directed by actor  , the theater play "Borges y Perón" and the telenovela Padre Coraje. The movie is based in the works of writers such as Tomás Eloy Martínez, José Pablo Feinmann and Norberto Galasso, but the private life of Juan Perón is largely undocumented, so most scenes were made up by the script writers. As the movie is based in the late life of Perón, the script puts emphasis in his fears of death. 

Laplace sought to mimic Perón in gestures, talking style and voice inflection, and repeated several of his quotations. The newspaper La Voz del Interior praised as well the work of the actors Victoria Carreras and Fito Yanelli as Isabel Perón and López Rega, and the striking similarity of Manuel Vicente with Héctor Cámpora.   

The film was premiered in Córdoba, Argentina|Córdoba on April 11, at the "Paseo del Buen Pastor". The ceremony was attended by governor José Manuel de la Sota, who praised the film and the approach that focused on Peróns private life. He pointed that the movie reflects both the good and bad things about a highly influential figure of Argentine politics.  The premiere in Buenos Aires was done at the theater "El Argentino", and the money was given for charity for the victims of recent 2013 Argentina floods|floods. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 