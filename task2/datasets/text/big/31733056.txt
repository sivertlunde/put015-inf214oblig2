Con Games
{{Infobox film
| name           = Con Games
| image          = Con Games movie dvd cover.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Jefferson Edward Donald
| producer       = Ross Nolan Tommy Lee Thomas
| writer         = Jefferson Edward Donald Tommy Lee Thomas
| starring       = Eric Roberts Tommy Lee Thomas Matthew Ansara Amy Fadhli Moe Irvin Martin Kove
| music          = Veigar Margeirsson
| cinematography = John Lazear
| editing        = Chris Conlee
| studio         = Soaring Eagle Productions
| distributor    = Artist View Entertainment
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}} American direct action drama film written and directed by Jefferson Edward Donald, starring Tommy Lee Thomas, Eric Roberts, Martin Kove, Amy Fadhli, and the late Matthew Ansara.   {{cite news 
| url=http://movies.nytimes.com/movie/263757/Con-Games/overview 
| title=Con Games (2001) 
| work=The New York Times 
| accessdate=May 10, 2011}} 

==Plot==
A senators grandson is murdered while incarcerated at Californias notorious "Doscher State Prison", nicknamed "Dachau" by convicts for the number of unsolved murders of inmates that have taken place therein.  Hired by the senator, Gulf War veteran John Woodrow (Tommy Lee Thomas) infiltrates the prison to find the murderer, and learns that the grandson was killed by corrupt head guard Lt. Hopkins (Eric Roberts).  When Hopkins discovers Woodrows true identity, he has him tortured by inamates and guards. After an arduous shooting chase through the desert, Woodrow escapes.

==Cast==
* Eric Roberts as Officer Hopkins
* Tommy Lee Thomas as John Woodrow
* Matthew Ansara as Saul
* Amy Fadhli as Ashley Stephenson
* Moe Irvin as Eddie Long
* Martin Kove as Redick
* Katherine Parks as Admin Assistant
* Michael Durack as Frank Nelson
* Dave Casper as Detective
* Sheila Campbell as Jeanette
* Patricia Bosse as Melanie Richards
* Tiffany Brouwer as Britney Woodrow
* Marc Chenail as Robinson
* Bill Fishback as Goetz
* Tony Harras as Warden Alvarez
* John Adam Kraemer as Sid
* Ross Nolan as McCain

==Background==
Principal filming began in May 2001. {{cite web 
| url=http://ftvdb.bfi.org.uk/sift/title/703387 
| title=Con Games (2001) 
| publisher=British Film Institute 
| accessdate=May 10, 2011}}   To prepare himself for the film, actor and co-screenwriter Tommy Lee Thomas trained for seven months prior to shooting in order to get in shape, and his sessions included training by Lou Ferrigno.  Thomas acknowledges having studied "muscle magazines" and appearances in them, as well as competing in bodybuilding competitions.   Thomas realized the advantage of getting a recognizable name for his film and was able to secure Erik Roberts. While Roberts did not have the "star power" he held in his earlier career, he still had enough so that Thomas was able to facilitate the films release in the U.S. and overseas. {{cite book
|last=Hall
|first=Phil
|title=Independent Film Distribution: How to Make a Successful End Run Around the Big Guys
|publisher=Michael Wiese Productions
|year=2006
|pages=48
|isbn=1-932907-16-5
|url=http://books.google.com/books?id=g03eq5axSYkC&pg=PA106&dq=%22Tommy+Lee+Thomas%22&hl=en&ei=Ct_JTcX1MI_6swO5jcmhAw&sa=X&oi=book_result&ct=result&resnum=2&ved=0CC4Q6AEwAQ#v=onepage&q=%22Tommy%20Lee%20Thomas%22&f=false
|accessdate=May 10, 2011}}   Keaton Simonds was to sing the song for the films end credits, but Eric Roberts sang the song instead after Simonds changed his mind. {{cite web
|url=http://www.filmthreat.com/interviews/827/|title=Tommy Lee Thomas: A New Texas Tornado
|last=Phil Hall
|date=November 3, 2004
|work=Film Threat
|accessdate=May 10, 2011}} 

==Reception==
Film Threat praised both the film and its star, writing that the film was "a very entertaining B-level action film that was released in 2002 to barely any acknowledgement."   They made note of the films low production values and budget, and that the scenes depicting the prison itself reflected this in the small number of inmates and even fewer number of prison guards.  They noted Tommy Lee Thomas as having co-written the screenplay, and wrote that he was "the fuel to this vehicle", that "clearly he studied a lot of Clint Eastwood films and he mastered the legendary star’s steely-eyed squint and sparse line deliveries," and that he "clearly possesses a sense of eccentric humor."  They concluded "As low-budget action flicks go, this one is very diverting and highly watchable." {{cite web 
| url=http://www.filmthreat.com/reviews/5709/ 
| title=review: Con Games 
| publisher=Film Threat 
| date=January 5, 2005 
| accessdate=May 10, 2011 
| author=Phil Hall}} 

Rovi_Corporation#Media_guide|Rovi panned the film, making note that the "men in prison" B-movie genre is represented by the film as its "too-literal acting is right out of 1960s drive-in cinema".  Referring to the film as a "train wreck of a feature", they noted it as an attempt by Tommy Lee Thomas to establish credentials as an actor.  They noted the script being poorly developed by writing it is "unimaginably filled with hilarious lines", and the cinemotography as poor when writing "the camera seems not to know where it wants to be in most scenes".  Rovi also addresses the acting of Eric Roberts, writing that he "devours the scenery as if it were steak."  They summarize by writing "The movie is a bomb. But, theres an endearing earnestness that overcomes the shoddy amateurism."  They concluded that "Only the most undemanding of action fans are going to appreciate this one". {{cite web 
| url=http://www.allrovi.com/movies/movie/con-games-v263757 
| title=review: Con Games (2001)  Rovi 
| accessdate=May 10, 2011 
| author=Buzz McClain}} 

==Release==
The film had subsequent DVD releases. A re-release was made by Third Millennium on May 27, 2002, {{cite web  Rovi | Spanish subtitles was released by MTI Home Video on July 23, 2002.
 {{cite web 
| url=http://www.allrovi.com/movies/movie/release/con-games-e26830
| title=MTI Home Video Con Games (2002)  Rovi 
| accessdate=May 10, 2011}} 

==References==
 

==External links==
*   at the Internet Movie Database
*  

 
 
 
 
 
 
 
 
 
 
 
 