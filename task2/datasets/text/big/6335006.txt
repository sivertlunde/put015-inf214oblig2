Modesty Blaise (1966 film)
 
 
{{Infobox film name            = Modesty Blaise image           = ModestyB.jpg image size      =200px caption         = Original film poster by Bob Peak border          = yes director        = Joseph Losey producer        = Joseph Janni based on        =  writer  Evan Jones (screenplay) Peter ODonnell (story) Stanley Dubens (original story) starring  Michael Craig Clive Revill music  Johnny Dankworth cinematography  = Jack Hildyard editing         = Reginald Beck studio          = Modesty Blaise Ltd. distributor     = 20th Century Fox released        =   runtime         = 119 min. language        = English budget          = £1&nbsp;million Alexander Walker, Hollywood, England, Stein and Day, 1974 p302 
| gross = $2.2 million (est. US/ Canada rentals) 
}} Evan Jones David and Michael Craig and Clive Revill.

==Plot==  British Secret Service chief Sir Gerald Tarrant (Harry Andrews) recruits spy Modesty Blaise (Monica Vitti) to protect a shipment of diamonds to a Middle Eastern sheik, Abu Tahir (Clive Revill). The shipment has also attracted Gabriel (Dirk Bogarde), the head of a diamond theft ring that includes his henchman McWhirter (Clive Revill) and Mrs. Fothergill (Rossella Falk). Modesty thinks that Gabriel, who maintains a compound in the Mediterranean, has died, but he reveals himself to her. 

In Amsterdam, Modesty reunites with her former lover, secret agent Paul Hagen (Michael Craig), while her partner, Willie Garvin (Terence Stamp), is reunited with an old flame, Nicole (Tina Aumont). Nicole is killed by an assassin working for Gabriel. With Modesty and Willie on the run, they decide to steal the diamonds for themselves but Gabriel captures Modesty and forces Willie to steal the diamonds. 

Modesty and Willie escape, killing Mrs. Fothergill, and giving the shipment of diamonds to Abu Tahir. As her reward, Modesty asks for and receives the diamonds.

==Cast==
 
* Monica Vitti as Modesty Blaise
* Terence Stamp as Willie Garvin
* Dirk Bogarde as Gabriel
* Harry Andrews as Tarrant Michael Craig as Paul Hagen
* Clive Revill as McWhirter / Sheik Abu Tahir
* Alexander Knox as Minister
* Rossella Falk as Mrs. Fothergill (credited as Rosella Falk)
* Scilla Gabel as Melina
* Michael Chow as Weng
* Joe Melia as Crevier
* Saro Urzì as Basilio
* Tina Aumont as Nicole (credited as Tina Marquand)
* Oliver MacGreevy as Tattooed Man
* Jon Bluming as Hans
* Robin Hunter as Pilot
 

==Production==
In 1965 Mim Scala of the Scala Browne Agency saw ODonnells strip and acquired the film rights to the character.  Scala had the idea of casting Barbara Steele as Modesty with Michael Caine as Willie, but he sold the rights to produce the film to Joseph Janni, who had Monica Vitti and Joseph Losey as his clients. 

Modesty Blaise was released at the height of two cinematic trends: The popularity of James Bond had spawned a number of similarly themed films, and many of these films, rather than being serious spy adventures, were instead created as parodies of Bond and his genre. Director Joseph Losey and the screenwriters chose to follow the latter approach, by making Modesty Blaise a campy, sometimes surrealistic comedy-adventure.

ODonnells original screenplay went through a large number of rewrites by other people, and he often later complained that the finished movie retained only one line of his original dialogue. ODonnell states this in some of his introductions to reprints of his comic strip by Titan Books.  As a result, although the basic plotline and characters are based on the comic strip, such as Willie killing a thug in an alley, many changes were made. Some are cosmetic—Vitti appears as a blonde for most of the film (except for one sequence in which she actually dresses up like a real-life version of the comic strip character). Likewise, Stamp initially appears in a blond wig and subsequently reverts to his natural dark hair colour. Other changes are more profound. For example, as the film progresses Willie and Modesty fall in love and decide to get married (proclaiming same during a sudden musical production number that erupts during a lull); this breaks a cardinal rule ODonnell set out when he created the characters: they would never have a romantic relationship (the writer stayed true to this edict to the end of the comic strip in 2001).

The film includes a metafictional element during one sequence where Blaise, while visiting a friends apartment, comes across several newspapers with the Modesty Blaise comic strip which are shown in close-up (artist Jim Holdaways work is prominently shown as is Peter ODonnells name). Supporters of the film suggest this indicates that the 1966 film is not intended to take place in the same "universe" as the comic strip.
 Modesty Blaise. This book was a critical and sales success, resulting in ODonnell alternating between writing novels and writing the comic strip for the next 30 years. ODonnells version of the screenplay was also used as the basis for a late-1990s Modesty Blaise graphic novel published by DC Comics.

==Critical reception== camp classic|date=March 2015}}, although fans of the Modesty Blaise character remain divided on its merits. Bosley Crowther, writing in The New York Times, characterized the film as "... a weird film, all right. Maybe, if the whole thing were on a par with some of its flashier and wittier moments, or were up to its pictorial design, which is dazzling, it might be applauded as a first-rate satiric job." The only redeeming features, Crowther claimed was, "The scenery, a few op-art settings and a gay, non-chalant musical score are indeed, about the only consistently amusing things about this whacky color film." Crowther, Bosley.   The New York Times, 11 August 1966. 

Modesty Blaise was entered into the 1966 Cannes Film Festival.   Festival de Cannes. Retrieved: 8 March 2009. 

Two more serious attempts at adapting the comic strip for the screen occurred in 1982 with a Modesty Blaise (1982 film)|made-for-television pilot film starring Ann Turkel as Blaise, and again in 2003 with My Name Is Modesty, a prequel starring Alexandra Staden and omitting the Willie Garvin character entirely.

==References==
===Notes===
 

===Bibliography===
 
* Scala, Mim. Diary of a Teddy Boy: A Memoir of the Long Sixties. Surrey, UK: Goblin Press, 2009. ISBN 978-0-95614-970-1.
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 