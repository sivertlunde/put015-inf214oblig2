Anjaan (2014 film)
 
 
{{Infobox film
| name           = Anjaan
| image          = Anjaan Poster.jpg
| caption        = Film poster
| director       = N. Lingusamy
| producer       = Siddharth Roy Kapur N. Subash Chandrabose
| writer         = Brinda Sarathy  (dialogues)  N. Lingusamy  (story & screenplay) Samantha Vidyut Jamwal
| music          = Yuvan Shankar Raja
| cinematography = Santosh Sivan Anthony
| studio         = Thirrupathi Brothers
| distributor    = UTV Motion Pictures 
| released       =   }}
| country        = India
| language       = Tamil
| runtime        = 170 minutes 
| budget         =     
| gross          =
| website        = 
| footnotes      =
}}

Anjaan ( ) is a 2014 Tamil  . 7 June 2014. Retrieved 7 June 2014. 

==Plot==
A handicapped Krishna (Suriya) arrives at Mumbai. He, along with a taxi driver (Soori) travel along Mumbai to look for his brother. He then meets a close friend of Raju and tells Krishna that Raju is Raju Bhai (Suriya). He then meets JK and he tells his experience with Raju Bhai and tells him that Raju could have killed him but spared JK. Krishna finally meets Karim Bhai (Joe Malloori) and he tells Krishna that Raju and Chandru (Vidyut Jamwal) were inseparable friends and were growing dons as they emerging in the underworld their enemies too grows. Then a new Commissioner plans to clean the city and kills a few people of Rajus gang. Raju then retaliates this by kidnapping the Commissioners daughter Jeeva (Samantha) and keeps her as a hostage for a day and when Raju goes to leave her back she tells that she actually doesnt want to go and doesnt want to marry because she didnt love anyone. Both of them then develop love. Things go well until Raju and Chandru invoke the wrath of Imran Bhai (Manoj Bajpai) who is a bigger and more powerful don than them and Chandru keeps chanting what Imran says to him that Imran would shoot them like pigs. Raju then gives Chandru a surprise by capturing Imran. Chandru becomes thrilled about it and gives Raju a surprise by getting him a new car. Raju then meets Jeeva inside. Chandru tells him that he had given Raju a surprise and tells him to stay away for seven days with Jeeva. Raju and Jeeva have a blast. When Raju comes back he sees Chandru murdered in a ghastly manner. A frustrated Raju goes in search of the killers is also shot by his own man, Amar. Krishna then becomes worried. At that very time Amar gets to know that Krishna was looking for him and tries to kill Krishna. Then it is revealed that Krishna is Raju. Raju then fights them and kills Amar. He then goes to find Jackie in a horse race. Jackie tells him to ask Johny. Raju then finds Johny in a shopping mall and Johny tells him that they along with Imran had plotted to kill Chandru when Raju was away. A disappointed Raju kills Johny. Raju then kills one more betrayer in a fight and shoots him. Then Imrans men kidnap Karim Bhais daughter. When Raju gets there Karim tells him it wasnt his daughter, it was Jeeva. Raju finally battles them and at last kills Imran Bhai. Raju, after rescuing Jeeva, reunites with her and happily leave Mumbai.

==Cast==
* Suriya as Raju Bhai , as Krishna 
* Samantha Ruth Prabhu as Jeeva
* Vidyut Jamwal as Chandru Bhai Soori as Raja
* Brahmanandam as Guru Shastri
* Manoj Bajpai as Imran Bhai
* Dalip Tahil as JK
* Murali Sharma  as Johnny
* Asif Basra as Rajiv
* Joe Malloori as Baashka
* Bikramjeet Kanwarpal as Commisioner
* Chetan Hansraj
* Jangiri Madhumitha
* Chitrangada Singh in a special appearance
* Maryam Zakaria in a special appearance

==Production==

===Development=== Anthony while Rajeevan was recruited as the art director. Brinda Sarathy was confirmed to be the dialogue writer for the film. 

===Casting=== Vivek was Soori was Jai Ho (2014).  Actress Maryam Zakaria was chosen to do an item number in the film.  Chitrangada Singh was chosen for another item number after discussions to have Sonakshi Sinha  and later, Kareena Kapoor, in the song were unsuccessful. 

===Filming=== Miramar beaches of Goa during the final schedule of 10 days. 

While shooting for the film in Goa, Suriya injured his knee during a stunt sequence. Lingusamy stated that the actor suffered a minor ligament tear when a fighter who weighed about 120&nbsp;kg fell from a height on his leg. The entire unit panicked when the incident happened but the actor wasnt fluttered. The director clarified that Suriya would be fit and fine soon and that he took a short break from shooting.  

==Soundtrack==
 
The soundtrack and the score for the film were composed by Yuvan Shankar Raja, making Anjaan the fourth collaboration between the composer and director Lingusamy. The soundtrack album, released on 23 July 2014, features five tracks, written by four lyricists. The male portion of one of the songs, "Ek Do Theen", was sung by Suriya and became his first attempt at playback singing. 

==Release==
  Sun TV for  .  Anjaan reportedly set a record for pre-release business of Tamil films as it made around   from its distribution, satellite and music rights. 

===Promotion and marketing=== Sun Music.    The trailer too received positive response  and also crossed 1 million views.  Behindwoods wrote in its trailer review, "Lingusamy is eyeing another blockbuster here."  Indiaglitz in its review, summarised, "Overall, the trailer lives up to the expectation and it is up to the movie now."  The video clip of the track "Ek Do Theen", featuring Suriya and Samantha Ruth Prabhu was released on YouTube on 11 August 2014. 
 Android and iOS operating systems. 

To counter piracy and to present a superior viewing experience to the audience, the makers came up with a pioneering move that would bring about a digital revolution in  . 12 August 2014. Retrieved 12 August 2014.  As a result of this initiative, Anjaan became the first Indian film which had a 100% digital release. 
 The Forum Vijaya Mall in Chennai.  The entire crew of the film including Suriya, Vidyut Jamwal, Lingusamy, Brinda Sarathy and Yuvan Shankar Raja attended the premiere of the film at Kuala Lumpur.  

==Reception==

===Critical reception===
Anjaan opened to mostly negative reviews from critics, with praise directed towards Suriyas performance and the background score, and criticism towards the length of the film, the placement of songs, the screenplay and the plot.    Sify wrote, ".On the whole, Anjaan is for those who seek unabashed entertainment and relish masala films.".  M. Suganth of The Times of India gave 2 out of 5 and wrote, "When a film announces loud and clear all that it wants to do is be in service of its hero, and make him as larger than life as possible, we accept it and all that we expect from it is to entertain us. But when it takes almost three hours to narrate its story with predictable twists and underwritten characters in a non-engaging fashion."  Indo-Asian News Service|IANS wrote, "It is high time Lingusamy, who is reluctant to change, accepts the fact that age-old formula doesnt work anymore.", and rated the movie 2 out of 5.  Indiaglitz wrote, "A commercial entertainer that might not go beyond Suriyas fan base!", and gave the movie 2.5 out of 5 stars.  Behindwoods rated the movie 2 out of 5 and concluded, "Suriya and Anjaan,   let down by the screenplay!".  Ramchander of Oneindia.in|OneIndia gave 2.5 out of 5 and wrote, "Suryas Anjaan is lengthy and it is just an average movie in the end." 

===Box office=== UK and Australia box office respectively on its first day, according to film critic and trade analyst Taran Adarsh, and earned around   gross from AP/Nizam areas. 

In its opening weekend, the film made   nett in Tamil Nadu alone while the Telugu version made   nett.  {{cite web|url=
http://www.bollywoodlife.com/south-gossip/anjaan-box-office-collection-suriyas-mass-flick-earns-rs-38-crore-in-opening-weekend/|publisher=Bollywood Life|title=Anjaan: Suriyas mass flick earns 38 crore in opening weekend|date=19 August 2014}} 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 