Kidnap (2015 film)
{{Infobox film
| name           = Kidnap
| image          = 
| alt            =
| caption        =  Luis Prieto
| producer       = Gregory Chou   Lorenzo di Bonaventura   Erik Howsam   Joey Tufaro
| writer         = Knate Gwaltney
| starring       = Halle Berry   Sage Correa   Lew Temple   Chris McGinn
| music          = 
| cinematography = Flavio Martínez Labiano
| editing        = 
| studio         = Rumble Entertainment   Well Go USA Entertainment   606 Films   Di Bonaventura Pictures   Gold Star Films   Lotus Entertainment
| distributor    = Relativity Media
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
 action thriller Luis Prieto and written by Knate Gwaltney. The film stars Halle Berry, Sage Correa, Lew Temple and Chris McGinn. The film is about a mother who tries to find her abducted son. The films development began in June 2009 and filming began on October 27, 2014 in New Orleans. Relativity Media will release the film on October 9, 2015 in the United States.

== Premise ==
A child gets kidnapped and his mother is not stopping until she finds him.

== Cast ==
* Halle Berry
* Sage Correa
* Lew Temple
* Chris McGinn

== Production == John Moore was attached to direct and executive produce the film based on the original script by Knate Gwaltney.  On December 9, 2011, IM Global acquired the rights to the film after Fox and Moore leaving the project, William Brent Bell set to direct and Matthew Peterman set to produce.  On May 15, 2014, it was announced that Halle Berry joined the cast of the film to play lead, which Lotus Entertainment would handle the international sales.  On September 6, Relativity Media acquired the US distribution rights to the film from Lotus Entertainment.  On October 20, Well Go USA Entertainment and Rumble Entertainment joined the film to finance and produce.    Gregory Chou of Rumble and Joey Tufaro of Gold Star Films would produce along with Bonaventura and Howsam.  On October 27, more cast announced which includes Sage Correa, Lew Temple, Chris McGinn, with Flavio Martínez Labiano set as director of photography.   

=== Filming ===
The principal photography on the film began on October 27, 2014, in New Orleans, Louisiana.     In mid-November, filming was also taken place in Slidell, Louisiana|Slidell.    Filming would end December 7, 2014. 

== Release ==
On September 17, 2014, Relativity announced the film is to be released on October 9, 2015. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 