The Desert Man
{{Infobox film
| name           = The Desert Man
| image          =
| caption        =
| director       = William S. Hart
| producer       = New York Motion Picture Corporation
| writer         = Martin Brown (story) Lambert Hillyer(scenario)
| starring       = William S. Hart Margery Wilson
| cinematography = Joseph H. August
| editing        =
| distributor    = Triangle Film Corporation
| released       = April 22, 1917
| runtime        = 5 reels
| country        = USA
| language       = Silent..English titles
}}
The Desert Man is a 1917 silent film Western directed by and starring William S. Hart. It was distributed by Triangle Film Corporation.   

A fragment or incomplete print exists.   


==Cast==
*William S. Hart - Jim Alton
*Margery Wilson - Jennie
*Buster Irving - Joey
*Henry Belmar - Razor Joe
*Milton Ross - Tacoma Jake
*Jack Livingston - Dr. Howard
*Walt Whitman - Old Burnss
*Josephine Headley - Katy

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 

 