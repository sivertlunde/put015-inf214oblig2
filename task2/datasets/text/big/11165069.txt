Without Love (film)
{{Infobox film
| name           = Without Love
| image          = Withoutlove.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Harold S. Bucquet
| producer       = Lawrence Weingarten
| writer         = Philip Barry (play) Donald Ogden Stewart
| narrator       =
| starring       = Spencer Tracy (film)  Katharine Hepburn  Elliott Nugent (play)
| music          = Bronislau Kaper
| cinematography = Karl Freund
| editing        = Frank Sullivan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         =$1,813,000  .  gross = $3,784,000 
| preceded by    =
| followed by    =
}} 1945 romantic comedy film starring Spencer Tracy and Katharine Hepburn.   The film was directed by Harold S. Bucquet from a screenplay by Donald Ogden Stewart based on the Barry play.

==Plot==
Lonely widow Jamie Rowan (Katharine Hepburn) helps the war effort by marrying a military research scientist, Patrick Jamieson (Spencer Tracy on film, Elliott Nugent on the stage), who has set up his lab in her house. Patrick has had all the worst of love and Jamie, all the best. They both believe that a marriage could be a success without love, as it reduces the chances of jealousy and bickering and all the other marital disadvantages. But as the film progresses, the inevitable happens as they begin to fall in love with each other.

==Film cast==
*Spencer Tracy as Patrick Jamieson
*Katharine Hepburn as Jamie Rowan
*Lucille Ball as Kitty Trimble
*Keenan Wynn as Quentin Ladd
*Carl Esmond as Paul Carrell
*Patricia Morison as Edwina Collins
*Felix Bressart as Professor Ginza

==Stage play and history== Broadway at the St. James Theatre in 1942.  Katharine Hepburn starred as Jamie Rowan with actor/writer/director Elliott Nugent as Patrick Jamieson, the role Spencer Tracy would take in the film.  Audrey Christie played the Lucille Ball role of Kitty Trimble, and the cast included Royal Beal and Lauren Gilbert.
 The Philadelphia Story, a major Broadway hit for Hepburn which she turned into her 1940 comeback film, also starring Cary Grant and James Stewart, and also adapted for the screen by Donald Ogden Stewart.  Hepburn and Grants 1938 film Holiday (1938 film)|Holiday—which had already been a 1930 film—was based on a 1929 Barry play.

Without Love was the third film to co-star Hepburn and Tracy, and it would be the last film directed by Bucquet.  Lucille Ball would turn to this films cinematographer, Karl Freund, six years later in her struggle to launch a filmed television show, unheard of at the time.
==Box Office==
According to MGM records the film earned $2,702,000 in the US and Canada and $1,082,000 elsewhere resulting in a profit of $619,000. 
==References==
 

==External links==
 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 