Gokulathil Seethai
{{Infobox film
| name     = Gokulathil Seethai
| image    = Gokulathil_Seethai_DVD.jpg
| caption  = Film DVD Cover
| director = Agathiyan
| writer   = Agathiyan Karthik Suvalakshmi Karan Manivannan
| producer = K. Muralidharan V. Swaminathan G. Venugopal Deva
| cinematography = Suriyan
| editing = Lancy-Mohan
| studio = Lakshmi Movie Makers
| distributor = Lakshmi Movie Makers
| released = 10 November 1996
| runtime  = 174 min
| language = Tamil
| country  = India
| budget   =
}}
 Tamil Kollywood|film Karan in Telugu as Raasi and in Kannada as Krishna Leele starring Shivarajkumar and Suvalakshmi reprising her role. Agathiyan himself remade the film in Hindi as Hum Ho Gaye Aapke with Fardeen Khan. Gokulathil Seethai became a success among 1996 Deepavali releases.

==Plot/Synopsis==
 
Rishi (Karthik) sleeps around through the efforts of a sophisticated, dedicated and ‘immigration officer’ look alike pimp (‘Thailaivasal’ Vijay). His DAD (Manivannan) whom Rishi considers to be his ‘friend’ voices his concern in a friendly way about Rishi’s life style and suggests him to try falling in love instead of spending all his hard earned money on call girls. Rishi brushes him off asking him to only worry about his earnings and that he would take of care of the expenditure.

Rishi offers his employee/old friend I.C. Mohan (Karan) to take him to a showt. Although hesitant at first (as Mohan refers I.C. to be inferiority complex), he agrees. The show turns out be a college show where a beautiful looking Nila (Suvalaxmi), doing her final year BBA performs a song. Mohan feels he has had one his best days as he falls instantly in love with her. On the contrast, Rishi instantly sets his sexual sight on her.

Rishi calls on the pimp to book Nila. The pimp gives his professional shot at it and ends up in jail. Rishi bails him out. I.C. Mohan sends a tape that has his recital of her song irritating Nila. She tries to throw the tape away but her friends offer to use the tape to record something else. Intrigued by a couple of flower bouquets, she finds out the information about Rishi and meets him personally. Rishi proposes to sleep with her one night. Nila rejects his ‘proposal’ despite Rishi offering half of his property and walks away. But Rishi is quietly resolute to woo her into bed one night.

When Nila returns to her hostel, she finds the ‘tape’ to have more info into it and hears a whining I.C. Mohan’s marriage proposal. Nila takes sympathy towards Mohan and meets him at a beach and lays down her rules straight: “She would fall in love, but only after marriage” and that she is determined to be behind the success of a man she marries. She asks him to meet her mother ‘formally’ if he is too desperate. While Nila takes a hiatus and visits home, her mother informs Nila that their family ‘doctor’ friend is interested in marrying her. It is implied that she informs Mohan about this. Mohan meets up with Rishi at a restaurant and confesses that he had lied to him and that he is really in love and that ‘she’ is about to get married. On hearing that ‘she’ is Nila, Rishi is stumped and takes a brief moment to himself to kill his own sexual urge and decides to help his ‘only’ friend and gives him an idea to forge a letter and that he would take care of the rest.

When Rishi passes the letter through a drunk Stranger to Nila on the night before her wedding, she shows it to her ‘doctor’ groom. The Doctor reads the ‘suicide’ note and empathizes with Mohan and encourages Nila to go ahead and marry him, wishing her good luck, promising that he would marry Nila’s sister instead. Nila sneaks out of the wedding hall and gets into a car. The car stops halfway through and Nila is surprised to see Rishi in the driver seat. Rishi reveals his surprise that Nila hasn’t ran away from the car and subtly acknowledges his own defeat. The car stops at a house. Rishi lets Nila out and tells her that the house was where he was born and that that’s where her ‘first night’ is going to happen and asks her to go in. Nila warns Rishi that her first night will turn out to be Rishi’s last night. Rishi sarcastically approves her “bravado” but still encourages her to go in. Nila enters that house confused until she sees Mohan begging his Parents and his sister to approve his love for Nila. What follows is an ugly altercation among the four. Rishi who is drinking outside the house hears this and enters in. Mohan tells his parents that Rishi is his boss and he gave the house as a gift for their upcoming wedding. Mohan’s parents talk ill of Rishi and sarcastically suggest him to marry Nila since he gave her a ride in the middle of the night. Mohan changes his mind soon and asks Rishi to drop Nila back off at the wedding hall. Rishi gets upset at Mohan and tries to convince him about Nila’s trust but in vain. Nila gives up on Mohan and asks Rishi to leave the place. Rishi leaves the house calling Mohan an “idiot”. He offers Nila to drop her off at the wedding hall. But Nila hesitates to go back worrying about her sister’s future. Rishi offers his own house until she finds a place on her own. Nila who is initially skeptical accepts the offer after Rishi’s word that he’ll never mistreat someone in despair.

Rishi’s Dad believes in his word and remarks to him that for the first time in Rishi’s life that a woman is in need of his services that draws a sarcastic comment from the Cook (Pandu) on being asked to take care of the guest (Nila). Next day Rishi tells Mohan that he should’ve fired him for what he did the previous night but lets him stay due to his family debts but confirms that their friendship is over. Rishi later takes Nila to a party where his friends mistake her for a call girl and embarrass her by asking her price per night. Rishi gets angry and storms out of the place with Nila. Rishi apologizes to Nila for what happened but she shrugs it off by letting him know that Rishi had never been out with his mom or sisters or any other decent woman. This hurts Rishi who drinks too much and they get into a big argument. Nila cools him off by offering her friendship.

The Pimp visits Rishi at his home next day and is surprised to see Nila in and congratulates him on his conquest. Rishi clarifies to him that she is just a platonic friend and that he made a mistake and ruined her life. That day Nila suggests a critical business solution to Rishi and his Dad who asks Rishi to hire her immediately. During a business lunch at the office, Rishi subtly suggests to Nila that she could still reconsider her decision about Mohan. Nila diplomatically changes the topic. A ‘gossip’ fight within the office premise between Mohan and a Colleague leads Rishi to intervene and stop. Mohan argues that he entered into Nila’s life at the first place which makes Nila angry who lashes out at Mohan pointing that he has never crossed her life at any point. She also says good things about Rishi and clarifies their relationship to other employees (a reference to epic ‘Ramayana’ is made by Nila where everyone knows Sita’s stage of life in Ashokavanam, but if Sita had been in ‘Gokulam’ she would’ve been safer & happier) that makes him feel good. Later, Rishi feels bemused on being asked to get out of Nila’s room for her to be able to change her clothes after shower and starts to feel something new that he has never felt before and leaves the room.

An excited Rishi admits to his dad that if what he feels is love, he just loves ‘this love’ and asks his dad to speak to Nila on behalf of him. Dad agrees but when he privately talks to Nila he asks her a question that upsets her and she leaves the house. A totally confused Rishi asks Nila as to what upset her. She doesn’t reply but keeps walking on. After jumping out at his dad for messing things up he leaves the house in pursuit of Nila. He tries to convince her in a bus where she is able to spare change to get a ticket to a place. When the bus conductor asks Rishi for change, he gives his credit card which the bus conductor has no clue about. They argue until a ticket inspector intervenes and tells Rishi that a credit card would work in a 5 star hotel but not in a local town bus and drops him off. Nila stays silent during the whole time.

Rishi goes back home, argues with his dad on having asked her such a stupid question and points out the differences between them and tells him that their relationship is over. He also asks his dad to get the heck out of his house for which his dad laughs and points out that the entire wealth is his hard earned money and if someone has to get out, it should be Rishi. At that moment a phone call reveals that Nila had mentioned Rishi as her guardian at a ladies hostel. Rishi proudly points that out to his dad and storms out. Later, when the Cook criticizes Rishi’s dad for having done this to his son, dad reveals that he talked to Nila the way he did not just to test her but to test Rishi as well. He also goes on to reveal that he is happy that Rishi is able to decide on his own and that he is confident that he’ll come back.

Rishi visits Nila at the hostel after a brief altercation with the security guard. Nila avoids talking to Rishi who understands her anger and reveals to the entire group of women gathered there that the reason for his visit was not to force or harass Nila but to acknowledge that she has transformed him into a human being and that she is the only reason that he is now able to respect any woman. He bids goodbye and leaves as Nila stops him, cries and apologizes that she mistook Rishi to be a jerk like Mohan when he sent his dad to speak on his behalf but now realizes that he did it out of respect for her. She also accepts defeat of her own agenda by concluding that “there is nothing wrong in falling in love with someone like Rishi before marriage” and tells him that she loves him and they leave the hostel together.

==Awards==
* 1996 – Tamil Nadu State Film Award for Best Film Portraying Woman in Good Light for Gokulathil Seethai

==Cast== Karthik as Rishi
* Suvalakshmi as Nila Karan as I.C. Mohan
* Manivannan as Dad
* Thalaivasal Vijay as Pimp Pandu as Cook

==Soundtrack== Deva and lyrics written by Agathiyan.

* "Gokulathil Kanna"
* "Nilaave Vaa"
* "Endhan Kural Kettu"
* "Andhimandharai"
* "Sorgam Madhuvil"

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-
! Year
! Film
! Language
! Cast
! Director
|-
| 1998
|Gokulamlo Seetha Telugu
|Pawan Kalyan Muthyala Subbaiyaa
|- 2000
|Krishna Leele Kannada
|Shiva Rajkumar
|D. Rajendra Babu
|- 2001
|Hum Ho Gaye Aapke Hindi
|Fardeen Khan, Reema Sen Agathiyan
|-
|}

==Reception==
The film was a super hit and won an award alongside excellent recognition. It completed 100 days at the box office and generated high revenues for Lakshmi Movie makers second only to Unnidathil Ennai Koduthen.

==References==
 

==External links==
*  

 

 
 
 
 
 
 