Anthony Adverse
{{Infobox film
| name           = Anthony Adverse
| image          = AnthonyAdverse.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Mervyn LeRoy
| producer       = Hal B. Wallis Jack Warner
| screenplay     = Sheridan Gibney Milton Krims
| based on       =  
| starring       = Fredric March Olivia de Havilland Gale Sondergaard
| music          = Erich Wolfgang Korngold
| cinematography = Tony Gaudio
| editing        = Ralph Dawson Warner Bros. Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 141 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 epic costume drama film directed by Mervyn LeRoy and starring Fredric March and Olivia de Havilland. Based on the novel Anthony Adverse by Hervey Allen, with a screenplay by Sheridan Gibney, the film is about an orphan whose debt to the man who raised him threatens to separate him forever from the woman he loves. The film received four Academy Awards.
 Academy Award for Best Actress in a Supporting Role for her role as Faith Paleologus.

==Plot== nobleman Marquis sword fight.
 dies giving Billy Mauch) is apprenticed to Bonnyfeather, his real grandfather, who discovers his relationship to the boy but keeps it a secret from him. He gives the boy the surname Adverse in acknowledgement of the difficult life he has led.

As an adult, Anthony (Fredric March) falls in love with Angela Giuseppe (Olivia de Havilland), the cooks daughter, and the couple wed. Soon after the ceremony, Anthony departs for Havana to save Bonnyfeathers fortune. The note Angela leaves Anthony is blown away and he is unaware that she has gone to another city. Instead, assuming he has abandoned her, she pursues a career as an opera singer. Anthony leaves Cuba for Africa, where he becomes corrupted by his involvement with the slave trade. He is redeemed by his friendship with Brother François (Pedro de Cordoba), and following the friars crucifixion and death by the natives, he returns to Italy to find Bonnyfeather has died and his housekeeper, Faith Paleologus (Gale Sondergaard) (now married to Don Luis), will inherit the mans estate fortune unless Anthony goes to Paris to claim his inheritance.
 Donald Woods), whom he saves from bankruptcy by giving him his fortune. Through the intercession of impresario Debrulle (Ralph Morgan), Anthony finds Angela and discovers she bore him a son. She fails to reveal she is Mlle. Georges, a famous opera star and the mistress of Napoleon Bonaparte. When Anthony learns her secret, he departs for America with his son (Scotty Beckett) in search of a better life.

==Cast==
* Fredric March as Anthony Adverse
* Olivia de Havilland as Angela Giuseppe Donald Woods as Vincent Nolte
* Anita Louise as Maria
* Edmund Gwenn as John Bonnyfeather
* Claude Rains as Marquis Don Luis
* Gale Sondergaard as Faith Paleologus
* Akim Tamiroff as Carlo Cibo
* Pedro de Cordoba as Brother François
* Louis Hayward as Denis Moore
* Ralph Morgan as Debrulle
* Henry ONeill as Father Xavier Billy Mauch as Anthony Adverse (age 10)
* Joan Woodbury as Half-Caste Dancing Girl
* Marilyn Knowlden as Florence Udney

==Production== Leslie Howard and George Brent. Walter Connolly Selected to Play Title Role in "Father Brown, Detective": Long Search for Correct Type Ends "Vampire of Prague" Lead Scheduled for Fay Webb
Scheuer, Philip K. Los Angeles Times (1923-Current File)   04 Oct 1934: 13. 

Errol Flynn was meant to support Fredrick March but proved so popular when Captain Blood was released he was instead given the lead in The Charge of the Light Brigade. Chaplins Big Business: Goldwyns Leading Lady: A New Romantic Hero
Bain, Greville. The Times of India (1861-current)   07 Mar 1936: 9.  

==Reception==
In his review in The New York Times, Frank S. Nugent described the film as "a bulky, rambling and indecisive photoplay which has not merely taken liberties with the letter of the original but with its spirit . . . For all its sprawling length,   was cohesive and well rounded. Most of its picaresque quality has been lost in the screen version; its philosophy is vague, its characterization blurred and its story so loosely knit and episodic that its telling seems interminable." 

The film was named one of the National Board of Reviews Top Ten pictures of the year and ranked eighth in the Film Daily critics poll. 
 Academy Awards==
;Wins    Actress in a Supporting Role: Gale Sondergaard
*  
*  
*  , head of department (Score by Erich Wolfgang Korngold)

;Nominations
*  
*  
*  

==In culture== violin concerto was drawn from the music he composed for this film. English singer Julia Gilbert adopted the name of the films main character when recording for the London-based Él records|él record label in the late 1980s.

Screen legend Tony Curtis (1925–2010), who was born Bernard Schwartz, named himself for the titular character; the novel from which this film was adapted was the actors favorite. Curtis, who soared to fame with his role in Houdini (film)|Houdini as the legendary illusionist, was buried with a Stetson hat, an Armani scarf, driving gloves, an iPhone and a copy of his favorite novel, Anthony Adverse.

Jack Benny parodied Anthony Adverse on the October 11 and 18 episodes of his "Jell-o Show" in 1936. 

In the 1934 short comedy What, No Men!, when their plane lands in "Indian Country" and Gus (El Brendel) is told to throw out the anchor, he tosses out a rope attached to a huge book titled Anthony Adverse.

The novel Anthony Adverse, by Hervey Allan, was included in Life Magazines list of the 100 outstanding books of 1924-1944. 

==References==
 

==External links==
 
*  
*  
*  
*   at Virtual History
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 