Godzilla vs. Gigan
 
{{Infobox film name            =Godzilla vs. Gigan image           =Godzilla vs Gigan 1972.jpg  caption         =Original Japanese poster director        =Jun Fukuda  producer        =Tomoyuki Tanaka  writer          =Takeshi Kimura  Shinichi Sekizawa  starring        =Hiroshi Ishikawa  Yuriko Hishimi  Minoru Takashima  Tomoko Umeda  
Toshiaki Nishizawa  Zan Fujita  Kunio Murai  Haruo Nakajima music           =Danro Miyauchi  Akira Ifukube cinematography  Kiyoshi Hasegawa 
| studio      =  Toho distributor     =Toho   Cinema Shares International/Downtown Distribution debuts          =Gigan  released        =  runtime         =89 minutes country         =Japan language        =Japanese budget          = 
}}
 Godzilla series, the first film in 1954.

The film received a limited theatrical release in the United States in 1978 by Cinema Shares as Godzilla on Monster Island.

==Plot==
Giant insectoid aliens from a dying planet in "Space Hunter Nebula-M" plot to colonize the Earth. The aliens assume the forms of dead humans and work as the development staff of the peace-themed theme park, World Childrens Land, the centerpiece of which is "Godzilla Tower". The Nebula-M aliens plan to use the space monsters Gigan and King Ghidorah, guided by two "Action Signal Tapes," to wipe out human civilization.

Manga artist Gengo Kotaka stumbles onto their plan after being hired as a concept artist for the park. When Gengo and his friends accidentally obtain one of the Action Signal Tapes and play it, Godzilla and Anguirus hear the signal and realize something is amiss. Godzilla sends Anguirus to investigate. When Anguirus approaches Tokyo, the Japan Self Defense Forces, misunderstanding the monsters intentions, drives him away.

Anguirus reports back to Monster Island, and Godzilla follows him back to Japan to save the Earth from Gigan and King Ghidorah. The aliens attempt to kill Godzilla with a lethal laser cannon hidden inside Godzilla Tower (the blue laser beam also resembles Godzillas atomic breath), but Gengo and his companions destroy the tower along with the aliens. After a lengthy fight, Godzilla and Anguirus force Gigan and King Ghidorah back into space and then Godzilla and Anguirus swim back to Monster Island, but not before Godzilla turns around and gives a roar of triumph, in thanks to his human friends.

== Cast ==
*Hiroshi Ishikawa - Gengo Kotaka
*Yuriko Hishimi - Tomoko Tomoe
*Minoru Takashima - Shosaku Takasugi
*Tomoko Umeda - Machiko Shima
*Toshiaki Nishizawa - Kubota, Head of Childrens Land
*Zan Fujita - Fumio Sudo
*Kunio Murai - Takashi Shima
*Gen Shimizu - Commander of Defense Forces
*Zek Nakamura - Priest
*Haruo Nakajima - Gojira
*Kenpachiro Satsuma - Gaigan

==Production==
After attempts to make another   like statue). The film was not to be though, and the idea was reworked into The Return of King Ghidorah, before that project was canned as well.

Although this particular film idea was scrapped, elements from it would be used in later Toho films, as Gigan and the Godzilla Tower would appear in this finished product as well as its sequel (Gigan has also become a staple in the video games, and re-appeared in 2004s  ), and Megalon would go on to star in that sequel entitled Godzilla vs. Megalon (1973).

The Return of King Ghidorah was to be a more extravagant version of what would ultimately become Godzilla vs. Gigan (1972). Despite the similar plot to what the movie became, the original concept had a rather large monster cast. On the alien side, King Ghidorah was touted as the projects character to beat, unlike the final draft which placed greater focus on Gigan. The cyborg monster was also fairly different at this stage, still having a scythe-like hand, but also a spiked ball and chain on the other. They were to work alongside another new monster called Mogu, a flying beast that could fire an oral weapon from its maw.

On Earths side, Godzilla, Rodan and Varan were to face off against the invaders. All three monsters had helmed their own films, while Godzilla and Rodans marquee value was already well known.

Unfortunately, the projects budget was ultimately far too modest for the ideas in this early script. At the minimum, it would have required three new suits in the form of Gigan, Mogu and Varan, the latter of which only had a small prop used for it in Destroy All Monsters (1968) since the original suit was heavily modified beyond recognition when it was loaned for filming of the Ultraman TV series. Taking the budget under consideration, Mogu was dropped while Rodan and Varan were replaced by Anguirus, to take advantage of the new suit that had been created in 1968 for the monster. King Ghidorahs name was dropped from the title, and the film became Earth Destruction Directive: Godzilla vs. Gigan.

==Box office==
In Japan, the film sold approximately 1,780,000 tickets.

==English version==
  international version of Godzilla vs. Gigan in North America. This version was re-titled Godzilla on Monster Island despite the fact that about a minute of the film actually takes place on Monster Island.

Very few edits were made, although Cinema Shares made several cuts to obtain a G-rating from the MPAA:
* The title card reads "GODZILLA ON MONSTER ISLAND" and the laser beam effect from the Japanese credits sequence is gone.

* Gengo calls his girlfriend "a hard bitch " under his breath. Cinema Shares muted the entire soundtrack when the word " bitch " is muttered. Causing the line to now be the inexplicable "Youre a hard."

* Two scenes of Godzilla bleeding from Gigans attacks are trimmed. Gigan also cuts Anguirus in his snout with his abdominal saw, which is also edited out.

* While Godzilla and Anguirus swim away at the end of the movie, Godzilla turns and blasts the camera with his radioactive breath (lifted from the opening of the film). The energy beam fills the camera, over which the words "THE END" are superimposed.

Another change in the English version was a scene where Godzilla and Anguirus talked. In the original version, speech bubbles pop out of their mouths,  but in the English version, the speech bubbles are removed. 
 The Sci-Fi Anchor Bay and in 2004 by Sony Pictures Home Entertainment. The Sony DVDs feature newly remastered prints of Tohos original international versions.
  (Godzilla), Kenpachiro Satsuma (Gigan), Koetsu Omiya (Angilias) and Kanta Ina (King Ghidorah, suspended by wires) put on a show in front of school kids as part of publicity for the filmss release in Japan.]]

==Titles==
* Earth Attack Mission: Godzilla vs. Gigan - Translated Japanese title.

* Godzilla vs. Gigan - Tohos official English title and current home video title.

* Godzilla on Monster Island - American theatrical release title.

* War of the Monsters - UK theatrical release title.

==Soundtrack==
The majority of the films soundtrack consists of recycled cues from previous Toho films such as Frankenstein Conquers the World, Atragon, King Kong Escapes and several other Godzilla films. Akira Ifukube, who composed the music in all those movies, receives credit in the film. In addition to those stock tracks, several themes composed by Ifukube for the Mitsubishi Pavilion at Expo 70 are used throughout the movie. A new song called "Godzilla March," sung by Susumu Ishikawa and composed by Kunio Miyauchi, plays at the end of the film. Isikawa also performed two more new songs ("Go! Go! Godzilla" and "Defeat Gigan") that were released on the soundtrack album.

==Home media releases==

Section23 Films|Kraken Releasing - Blu-ray
* Released: May 6, 2014  
* Picture: AVC-1080P (2.35:1) 
* Sound: Japanese and English (DTS-HD Mono)
* Subtitles: English
* Extra: Original Japanese Theatrical Trailer (1080i; 2:11)
* Note: 90 Minutes
* MPAA Rating: PG for sci-fi monster violence and some language

Sony Pictures

* Released: October 19, 2004

* Aspect Ratio: Widescreen (2.35) anamorphic

* Sound: Japanese (2.0), English (2.0)

* Supplements: Trailer for The Lost Skeleton of Cadavra

* Region 1

* Note: Contains Tohos International Version English dub track.

* MPAA Rating: PG for sci-fi monster violence and some language.

*  Includes footage and dialogue cut from previous U.S. versions.

4Front videos
* Released: 1998
* Sound: English
* Supplements: Japanese trailers for Invasion of the Astro Monsters, Ebirah, Horror of the Deep!, Godzilla vs. Gigan, Godzilla vs. Megalon, Terror of Mechagodzilla.
* BBFC Rating: PG

==References==
 

==External links==
*  
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 