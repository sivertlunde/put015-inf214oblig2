The Amateur Gentleman (1920 film)
 
{{Infobox film
| name           = The Amateur Gentleman
| image          =
| caption        =
| director       = Maurice Elvey
| producer       = Stoll Pictures
| writer         = Jeffery Farnol
| starring       =  Langhorn Burton, Madge Stuart and Cecil Humphreys
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| awards         =
| language       = 
| budget         = 
}}

The Amateur Gentleman is a 1920 British drama film directed by Maurice Elvey and starring Langhorn Burton, Madge Stuart and Cecil Humphreys.  The film is adapted from the 1913 novel The Amateur Gentleman by Jeffrey Farnol. 
 The Amateur Gentleman starring Richard Barthelmess and in 1936 as The Amateur Gentleman starring Douglas Fairbanks, Jr..

==Plot==

In Regency Britain a young man tries to establish his fathers innocence of an accused crime, by travelling to London disguised as a gentleman.

==Cast==
* Langhorn Burton ...  Barnabas Barty 
* Madge Stuart ...  Lady Cleone Meredith 
* Cecil Humphreys ...  Wilfred Chichester 
* Herbert Synott ...  John Barty 
* Pardoe Woodman ...  Ronald Barrymaine 
* Alfred Paumier ...  Prince Regent 
* Gerald McCarthy  ...  Viscount Horatio Debenham 
* Geoffrey Wilmer ...  Captain Slingsby 
* Sydney Seaward ...  Sir Mortimer Carnaby
* E. Vivian Reynolds ...  Jasper Gaunt 
* Dalton Somers ...  Natty Bell 
* Teddy Arundell ...  Digby Smivvle 
* Will Corrie ...  Captain Chumley 
* Judd Green ...  Jerry the Bosun

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 