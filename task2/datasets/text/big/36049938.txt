Christopher Columbus (1923 film)
{{Infobox film
| name           = Christopher Columbus 
| image          = 
| image_size     = 
| caption        = 
| director       = Márton Garas
| producer       = 
| writer         = 
| narrator       = 
| starring       = Albert Bassermann   Elsa Bassermann   Tamara Duvan   Imre Pethes
| music          = 
| editing        = 
| cinematography = Karl Attenberger   Mutz Greenbaum   Eugen Hamm
| studio         = Filmhandel
| distributor    = 
| released       = February 1923 
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 German silent silent historical Discovery of Spanish explorer Christopher Columbus in 1492.

==Cast==
* Albert Bassermann - Columbus
* Elsa Bassermann - Ehefrau des Columbus
* Tamara Duvan - Königin Isabella
* Arpad Odry
* Imre Pethes - König Ferdinand II.
* Ludwig Rethey - Der Prior von La Rabida
* Ernst Stahl-Nachbaur - Herzog von Medina-Celli
* Ferenc Szécsi - Diego
* Carola Toelle - Maria, Tochter des Herzogs

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 
 