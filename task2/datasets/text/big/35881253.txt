Cast a Long Shadow
{{Infobox film
| name           = Cast a Long Shadow
| image          =
| image_size     =
| caption        = Thomas Carr
| producer       = Walter Mirisch Audie Murphy (uncredited)
| writer         = Martin M Goldsmith John McGreevey
| based on       = novel by Wayne D. Overholser
| narrator       = Terry Moore John Dehner
| music          = Gerald Fried
| cinematography = Wilfred Cline
| editing        =
| studio         = Mirisch-Murphy Company
| distributor    = United Artists
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Western film starring Audie Murphy. It was made by a company Murphy formed with Walter Mirisch. 

==Plot==
Matt Brown inherits a cattle ranch which he discovers is deep in debt. He goes on a cattle drive to raise money.

==Cast==
*Audie Murphy as Matt Brown Terry Moore as Janet Calvert
*John Dehner as Chip Donahue
*James Best as Sam Muller
*Rita Lynn as Hortensia
*Denver Pyle as Harrison
*Ann Doran as Ma Calvert
*Stacy S. Harris as Brown
*Robert Foulk as Hugh Rigdon
*Wright King as Noah Pringle
*Alan Dinehart III as Dick Calvert
*Joe Partridge as Ken Calvert

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 
 
 
 
 

 