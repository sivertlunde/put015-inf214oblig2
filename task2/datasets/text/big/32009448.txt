We Are the Night (film)
{{Infobox film
| name           = We Are the Night
| image          = We are the night poster.jpg
| alt            = 
| caption        = US release poster
| director       = Dennis Gansel
| producer       = Christian Becker Jan Berger Dennis Gansel (Original treatment "The Dawn")
| starring       = Karoline Herfurth Nina Hoss Jennifer Ulrich Anna Fischer Max Riemelt
| music          = Heiko Maile
| cinematography = Torsten Breuer
| editing        = Ueli Christen
| studio         = Rat Pack Filmproduktion
| distributor    = Constantin Film
| released       =  
| runtime        = 100 minutes
| country        = Germany
| language       = German
| budget         = €6,500,000 (about $8,500,000)
| gross          = $1,251,991 
}}
We Are the Night ( ) is a 2010 German vampire horror film directed by Dennis Gansel, starring Karoline Herfurth and Nina Hoss. The film deals with a young woman who gets bitten by a female vampire and drawn into her world. She falls in love with a young police officer who investigates a murder case involving the vampires. The film explores themes of Depression (mood)|depression, self-harm, the consequences of immortality, suicide, and explores Valerie Solanas idea of an Separatist feminism|all-female society.

==Plot==
 
The film opens with several images of three young women. The images date further and further back until ending in the late 18th century.

In a private airplane, the people and the pilots are dead. The only survivors, three young women, are the obvious killers. The leader of these women finds an air-hostess still alive. After examining her eyes, she breaks her neck and the women jump out of the plane, allowing it to crash into Berlin.

In Berlin, a young criminal, Lena, lurks near a cash dispenser. Just as her victim has used the machine she pickpockets his credit card only for the victim to be arrested by the police. A police officer, Tom, chases after her and eventually catches up with her on a bridge. After a short conversation, he tries to arrest her but Lena hits him in the face, kicks him between the legs and jumps off the bridge onto a boat.

Later that evening, Lena goes to a nightclub. Everyone in line for the club is being examined with a hidden camera by one of the women from the beginning, Louise. Upon seeing Lena, she orders the guard to let her in. Inside the club, Louise dances with Lena and offers her a drink. She asks where Lenas friends are, but Lena replies she does not have any. When Lena goes to the bathroom, Louise follows and bites her in the shoulder. The terrified Lena runs home. When she awakes in the morning, the sun burns her, forcing her to stay inside. That night, Lena returns to the club to confront Louise about what has happened. She meets Louises two companions, the silent Charlotte and the cheerful Nora. Although never said in the film, it becomes clear that Louise, Charlotte and Nora are vampires.

The girls sell the angry and confused Lena to a Russian pimp as a prostitute. Louise plans for Lena to kill the wicked man to ease her transformation. Lena is locked in a room. When the pimp comes in to rape her, she is overwhelmed by vampiric blood lust and attacks the pimp. In the struggle, she stabs him with a broken light bulb and tries to escape but another pimp shoots her with a shotgun. The girls return to kill the pimps before setting them ablaze. They fail to notice one mobster hiding in fear. Before leaving, Nora steals one of the mobsters Lamborghini.

When Lena awakes at the hotel, Louise gives her a glass of what appears to be blood. As Lena drinks, she feels her strength return. Louise takes care of Lena, removing her dirty and destroyed clothes and showing her to a bathroom where a bath is waiting. After closing the door to leave Lena alone, Louise tells her the story of how she was changed 280 years ago at a masked ball. At first she hated her maker, but grew to love her after traveling throughout Europe and getting to know all the benefits of vampire life. After her maker was burned (killed by sunlight), Louise wanted to commit suicide to join her, but instead began searching for a new companion. As Lena bathes, her short hair grows and returns to its natural color, her piercing falls out and she loses all bruises and wounds (including a tattoo on her belly).
 depression and engages in acts of self-harm, among other things burning her own eyeball (which immediately heals). During a dinner (the vampires can eat and taste food but it does not satisfy them), Louise tells Lena that there are no male vampires, because as the male vampires grew arrogant, the females tired of their superiority and killed all of them with the common promise to never turn a man into a vampire. Nora also reveals that she met Louise at the Love Parade 1997 where she turned her. As the night comes to an end, the vampires return to their hotel to perform their morning ritual: allowing the first rays of sun burn their skin but retreating inside before any lasting harm can be done. Charlotte stays outside longer than the others, but retreats before catching fire. Louise kisses Lena, which startles and confuses Lena who bites Louises lip, before she interrupts herself and begins cursing her hastiness.

Meanwhile, Tom uses a file on Lena to find her home address. The next night Lena returns home to visit her mother who has not even noticed her absence. As Lena leaves, Toms sees her, but almost does not recognize her. He tells Lena that she could go to prison for violating her 18-month probation if he arrested her for stealing the credit card, but says he wont because her victim (Wasja) used women. The two had coffee and talk, but when Lena suddenly sees Louise approaching, she uses her Lamborghini to retreat to the vampires nightclub. She finds Charlotte in a private room where Charlotte tells Lena that she was a silent movie actress in the 1920s and that she had a husband and daughter. Louise enters the room and asks Lena why she is acting different. Lena lies that she misses the sun, so Louise takes the girls to Tropical Islands Resort that has artificial sunlight allowing them to have a pool party "in the sun". Two night watchmen find the girls and ask how they managed to get inside. Nora convinces the men to join them in the pool where she quickly kills off one of them, but as water slows the vampires down, the other tries to escape. Charlotte kills this man in a sadistic manner, slicing his throat with a piece of paper which causes him to die slowly and painfully. Lena flees the scene in terror.
 SEK (German SWAT unit) is invading the building. Charlotte says she will stall them while the girls make their escape. The vampires have special cars with tinted windows to protect them from sunlight if they ever had to escape during daylight. Lena and Louise take one car while Nora drives alone. The police who think the girls are simply dangerous criminals are slaughtered by Charlotte. Tom enters the room looking for Lena and manages to shoot Charlotte before she attacks him, causing her to fall out the window. As the sun starts to burn her skin, Lena and Louise manage to get her inside their car. Nora crashed into a car and is thrown into sunlight. Police open fire at the other car letting in sunlight on the vampires. Unable to save Nora, they drive away as Charlotte looks back at Nora burning to death and screaming in agony. The roof of the car is ripped off by a roadblock and the girls crash into a subway to escape the scourging sunlight.
 Teufelsberg (The allows the sun to kill her. Louise screams at Charlotte to open the door as she and Lena break down into tears.

Lena goes to Tom to say good bye and show him who she really is (She forces him to shoot her and the wound heals in front of his eyes). Lena begins to cry while Tom holds her and they fall asleep. Lummer, who has suspected Tom of having some involvement with the girl since the hotel attack, has the SEK invade the building. Lena almost kills the policemen but is stopped by Toms plea, leading to their arrest and incarceration. Louise goes to the watchmen while theyre in the shower and kills them (a deleted scene shows director Dennis Gansel playing one of the cops getting killed). After that she asks Lena how she will manage to live with Tom, because he will die in 60 years and then shell be alone. Louise kidnaps Tom and Lena follows her. The sun is rising when they meet and Louise wants Lena to tell her, "I love you". Lena does so and Louise says that thats the most beautiful lie shes ever heard and shoots Tom. Louise and Lena have a fight and Lena throws Louise into the sunlight, where she dies with a peaceful smile. Lena runs to Tom and wants to bite him, but stops, kisses him and begins to cry.

At the end the SEK arrives with Lummer. Lena and Tom are gone with no trace other than Louises gun. Lummer looks outside and sees something the other policemen and the audience dont. Lummer whispers Good luck and walks away from the scene.

==Cast==
* Karoline Herfurth as Lena
* Nina Hoss as Louise
* Jennifer Ulrich as Charlotte
* Anna Fischer as Nora
* Max Riemelt as Tom
* Arved Birnbaum as Lummer
* Steffi Kühnert as Lenas mother
* Ivan Shvedoff as van Gogh
* Nic Romm - Olsen
* Christian Näthe as Guard 1
* Tom Jahn as Guard 2
* Manuel Depta as Russian Pimp 1
* Waléra Kanischtscheff as Wasja
* Neil Belakhdar as Bellboy (Noras Lover)
* Cristina do Rego as Stewardess
* Tom Jester as Russian Pimp 2
* Steve Thiede as SWAT-Leader
* Dennis Gansel as Police (Scenes deleted)

==Background==
  The Wave gave him free hands. Due to the similarities between the original script and Twilight, Gansel had Jan Berger rewrite the script, now under the title Wir sind die Nacht. Thinking that an ordinary love story was no longer in place with the boom of star crossed vampire/human relationships Gansel imagined a darker twist on the subject was more in order. He came up with a new, darker love story and took Carmilla as inspiration as he thought while Dracula had been done to death, no one had really made a serious attempt on Carmilla. Karoline Herfurth was shown the script back in 2000, but she was too young to play Lena, so Gansel promised her the role of Nora. But as production was delayed, Herfurth became old enough to play the part of Lena. Instead Anna Fischer was cast as Nora, who in the eye of producer Christian Becker was the perfect choice because she looked like a party girl. Jennifer Ulrich auditioned 4 times before being cast as Charlotte.

Shooting began October 11, 2009 and ended on December 16. 
 435 cameras. 

Most of the stunts in the film were done by the actresses themselves.

==Soundtrack== The Dark Au Clair De La Lune as Charlottes theme. Sven Hack performed the sad clarinet solos that appear throughout the film.

# "Self-fulfilling Prophecy" − Scala & Kolacny Brothers
# "In Our Eyes" (Anthony Mills Soundtrack Version) − Moonbootica Covenant
# Au Clair De La Lune)" (Score) − Heiko Maile
# "Nightlife" − IAMX
# "Lenas Metamorphosis" (Score) − Heiko Maile
# "Cold Song" − Klaus Nomi
# "Escape From The Hotel (Suite)" (Score) − Heiko Maile
# "Dumpfe Träume (WSDN Remix)" − Xenia Beliayeva
# "Miserable Girl (Nite Version)" − Soulwax
# "Tief in der Nacht" − DJ Valero
# "IERS" − Dirk Blümlein Terzett
# "Land Of The Free" − Warren Suicide
# "Farewell My Child" (Score) − Heiko Maile
# "Pretty When You Cry" − VAST
# "Russian Whorehouse (Suite)" (Score) − Heiko Maile
# "Big And Bad (WSDN Edit)" − Gabriel Le Mar

The complete score was released as a MP3 download on Amazon.de and the German iTunes Store.

==Alternative endings==
Two other endings were shot for this film, like with Gansels previous film The Wave. Both were discarded to bring a more ambiguous ending to the film.

The first alternative ending plays out almost like the final ending. It continues after Lena starts to cry and shows her running away into the subway station leaving Tom behind. She stops and looks behind her before starting to run again.

In the second ending however, Lena finds Tom dying from his wound and bites him, turning him into a vampire. After delivering the bite, Lena flies backwards, crashing into the wall much like Louise did when she bit her. Lena whispers "Forgive me," as Tom rises on his feet. The camera moves away as they look at each other.

==Critical reception==

German newspaper Die Welt praised the film and said it had rescued the vampire genre from the likes of The Twilight Saga (film series)|Twilight, True Blood and The Vampire Diaries. The critic noted the film having a similar theme of Gansels earlier films Die Welle and Before the Fall about the seduction of youth and also dealt with hedonism and obsession with youth. The critic praised the multiple levels and themes of the film and that Berlin was used as much more than a setting, almost a cipher. The critic also noted Gansels decision to use female leads and felt he had a lot of empathy for them. 
 German Film Awards. It was awarded the Special Jury Award at the 43rd Sitges Film Festival.  About.com awarded the film with 4 stars and named it the best vampire film of the year.  New York Times gave a negative review and called the film neither romantic or chilling.  Twitchfilm.com praised the visuals and effects but was most enthusiastic about the lead characters. He compared that unlike films like Lesbian Vampire Killers, the film explored Louises lesbian desire from a romantic and humanist perspective. The reviewer concluded by writing "There are no bad guys here, just misunderstood ones, so the way Lena concludes this is strange and unsettling. I really wanted to spend more time with this group, the film only touches upon what they have been through and feel, but regardless I highly recommend this." 
 Let The Right One In, and if given the choice to watch either film over and over again I’d pick We Are The Night every time, because it’s fun as hell." He went on to give the film the score 8.5/10.  Hoollywood News gave the film four stars, commenting that the second part of the film was more satisfying than the first part, that the special effects where not spectacular but that was not needed due to the extremely strong script. The reviewer praised the acting as top-notch and stated the film was just as good as vampire classics like Near Dark and Let the Right One In. 

MoreHorror.coms Marcey Papandrea compared the film favourably to The Lost Boys and Near Dark and praised the acting of Herfurth, Hoss, Ulrich and Fischer, and noted that especially Nina Hoss was pitched perfect for the role. He noted that Max Riemelt was left with out much to do in the film with his character but thought he did what he could with his spare material. He concluded by stating "There isnt much to complain about here, it is a fun and interesting film, a breath of fresh air in an over done subgenre". 

One Metal named the film one of the best modern vampire films and gave it a 4/5 score, but commented that the pacing was uneven, especially in the climax. 

Like many other critics who reviewed the film,   but the execution of these themes and elements were extremely well done and the lack of originality was made up for by energy and execution. Fearnet ended up recommending the film but not praising it.  Brutal As Hell made similar comments, but ended up being more positive stating the "borrowed" elements where already established tropes of vampire fiction. The reviewer praised the script and the acting and the well developed mythology of the vampire stating that the decade long work on the film showed of in well fleshed out characters and mythology. 

Trash City named it one of the 10 best films of 2011  (despite being a 2010 film).

Jennifer Ulrich was given the award Best Interpretation New Talent at Horror Fantasy Awards. 

The film holds a 62% approval rating on Rotten Tomatoes.  Jennifer Ulrich was generally praised for her performance as Charlotte. 

The film was screened at MIX Copenhagen, a LGBT-film festival. 

==Box office==

We are the night flopped in German cinemas. Dennis Gansel stated in German press that the commercial failure hurt his soul because of all the work he and his co-workers put into the film.  Dennis Gansel attributed the failure to the film opening the same week as   and not able to compete with such an established franchise. The film fared better on DVD. Dennis Gansel has said he did not regret making the film and that it was: (D)efinitely worth making. 

==See also==
* Vampire film

==References==
 

==External links==
*   (German)
*   (US)
*  
*  
*  

 
 
 
 
 
 
 
 
 
 