Vishwaroopam
 
 
 
 
{{Infobox film
| name = Vishwaroopam
| film name = விஸ்வரூபம் (2013 திரைப்படம்)
| image = Vishwaroopam poster.jpg
| caption = Theatrical poster
| director = Kamal Haasan
| producer = {{ubl| Kamal Haasan|}}Prasad Vara Potluri   
| starring = Kamal Haasan Rahul Bose Pooja Kumar Andrea Jeremiah Jaideep Ahlawat
| writer = Kamal Haasan Atul Tiwari
| music = Shankar-Ehsaan-Loy
| cinematography = Sanu Varghese
| editing = Mahesh Narayanan
| studio = Raaj Kamal Films International PVP Cinema Balaji Motion Pictures
| released =  | | }}
| runtime = 149 minutes    
| country = India
| language =   
| budget =    gross =     
}}
 Indian Tamil Tamil spy spy thriller Tamil and Hindi and dubbed into Telugu language as Viswaroopam, the film features soundtrack composed by Shankar-Ehsaan-Loy, with lyrics by Vairamuthu and Haasan himself in while Javed Akhtar translated them for the Hindi version.
 protests of theatre owners this plan was dropped.

The film released worldwide, excluding Tamil Nadu, on 25 January 2013 while the Hindi version was released on 1 February 2013. Legal controversies, regarding the films plotline of Indian security services participation in the War on Terror, arose as several Muslim civic organisations protested the films release in Tamil Nadu, resulting in an official ban being imposed on the film in the state for 15 days by the Government of Tamil Nadu. The ban resulted in similar decisions in major overseas markets like Malaysia, Sri Lanka and Singapore while release was delayed in the south indian states of Andhra Pradesh, Kerala and Karnataka.
 Best Art Best Choreography at the 60th National Film Awards.    A sequel to the film, Vishwaroopam II, is set for release in early 2015.

==Plot==
The film opens in a rundown pigeon shop in New York City where an old man feeds them. He then sends one pigeon away. It flies high and lands in a skyscraper off the office window of a psychologist who is conducting a session with her client Nirupama (Pooja Kumar), a nuclear oncologist, who begins to confide that hers was a marriage of convenience that provided a safe haven for pursuing her PhD in the U.S.A. for past three years and that her husband Vishwanath alias Viz (Kamal Haasan) is a middle-aged Kathak teacher. She is also put off by Vizs effeminate bearing and is attracted to her boss, Deepankar (Samrat Chakrabarti). Doubting whether her husband has secrets of his own, she hires a private investigator to tail him to probe grounds for divorce. She learns from the private investigator that Viz is a Muslim. In a sudden turn of events, the investigator is killed in a sea-side warehouse by Farukh, a prominent member of the terrorist outfit led by Omar (Rahul Bose). A diary on him gives away Nirupama and the terror group led by Farukh nabs the couple. Deepankar is later killed by Farukhs men.

Viz surprises Nirupama by having a fight with the terrorists, kills Farukh and his men at the warehouse and escapes with Nirupama. Omar and Viz have a past, one that takes the story back to circa 2002, to the Al-Qaeda training camps in Afghanistan-Pakistan border. Vizs real name was Wisam Ahmad Kashmiri. He claims to be a Tamil Jihadi in Kashmir, wanted by the Indian Army, with a reward of   500,000 on his head. Omar accepts him into his team and drives him off to Afghanistan. Wisam becomes a trainer to the Al-Qaeda Jihadis and also a family friend to Omar. One day, Omar tells Wisam that American prisoners of war are still alive, incarcerated and shifted each fortnight. He orders his deputy Salim (Jaideep Ahlawat) to behead the Captain and capture it on video. The next day, Salim tells Wisam that a new tall guest is expected in the town. Wisam later that night sees Osama bin Laden (Naren Weiss)    greeting the Al Qaeda chieftains in a cave. Then, a joint US-led air force begins bombing raid on the town. Omar begins to doubt that there is an informer in the team but mistakenly orders lynching of an innocent man. What follows is a maze of events that go back and forth in time, unraveling a plot where in the terrorists are scraping cesium from oncological equipment to build and trigger a Dirty bomb in New York City.

Nirupama is stunned to discover the true identity of Viz, his "uncle" (Shekhar Kapur), British "friend" Dr Dawkins (Miles Anderson) and the young "dancer" Ashmita (Andrea Jeremiah). Viz later reveals that he has a lot of emotional baggage and that he had executed many terrorists including Nassar (Nassar), Omars boss. His mission is to bust the sleeper-cell of Al Qaeda in the US, which is planning to divert the attention through "capsules" capable of emitting mild nuclear radiation tied to pigeons while enabling Abbasi, a Nigerian suicide bomber to detonate the cesium dirty bomb in the city.
 a sequel set in India.

==Cast==
* Kamal Haasan as Nasser / Vishwanath
* Rahul Bose as Omar Qureshi
* Shekhar Kapur as Colonel Jagannath
* Pooja Kumar as Nirupama Wisam aka. Nirupama Vishwanath
* Andrea Jeremiah as Ashmita Subramaniam
* Jaideep Ahlawat as Salim
* Samrat Chakrabarti as Deepak Chatterjee
* Nassar as Nassar, Omars boss
* Zarina Wahab as psychiatrist
* Miles Anderson as Dawkins
* Naren Weiss (as Nareen) as Osama bin Laden
* James Babson as Tom Black
* Jude S. Walko as Captain Joe Black
* Chris Kotcher as Tollbooth Operator
* Greg Sammis as F.B.I. Agent
* David Scott Diaz as F.B.I. Agent
* Melissa Bayer as Store Customer
* Hayat Asif as Taufiq

==Production==

===Development===
After finishing his Manmadan Ambu (2010), Kamal Haasan stated in November 2010 that he was working on the scriptment of his "pet project" titled Thalaivan Irukkiran, a film about an international community. Termed as a big budget all-star|multi-starrer film, it was expected to commence by March 2011, with a Hollywood studio reportedly coming forward to produce it.    However, in early 2011, sources claimed that Kamal shelved that project and agreed to star in a Selvaraghavan directorial, being impressed by the one-line story narrated to him.  Reports further suggested that the film was based on the 2001 American psychological thriller Hannibal (film)|Hannibal,    with Kamal playing a cannibal,    Closer to release, Kamal Haasan revealed that he had thought of the story seven years before production began and had to convince himself that the story could be made into a feature film.    The film was named Vishwaroopam, after several titles were considered,  by late March and the shooting was planned to begin by mid-April and to be completed within 100 days.    It was revealed that the film would be produced in three languages—Tamil, Telugu and Hindi—simultaneously. 
 Pazhassi Raja, Four Friends. 

===Casting===
  Pooja Kumar Telugu actress Lakshmi Manchu was offered a role in the film, but declined it due to date issues. 

Shriya Saran was reported to play the second female lead in the film,  with the actress dismissing the news several days later, citing that she had not even heard the script,  following which Priya Anand was claimed to have secured that role.  This turned out to be false, with Kamal Haasan citing that he did not know who Priya Anand was.  Isha Sharvani was meanwhile selected for a role,  with reports suggesting that she would essay the character of Kamal Haasans sister in the film.  However, Sharvani too opted out later, due to "inordinate delay in the start of the shoot".  By late August 2011, British model-turned-actress Amy Jackson was reported to have been added to the cast.  During mid-October, singer-actress Andrea Jeremiah had been signed on for a pivotal role and paid an advance. 

Rahul Bose was finalised to play the antagonist.  Later that month, actor-director Shekhar Kapur informed on Twitter that he would perform a cameo role,  while Samrat Chakrabarti had also been selected for a supporting role.  Jaideep Ahlawat disclosed in an interview that he would play an "out-and-out negative character" in the film.  Zarina Wahab told in November 2011 that she shot for a small role in Vishwaroopam.  In January 2012, Chitrangada Singh was offered a "very special role", which the actress had to decline, since her dates clashed with Sudhir Mishras film.  As Kamal Hassan is ardent reader of Richard Dawkins, he had named Miles Anderson as Dawkins in the movie.

===Filming===
 
The films shooting was supposed to take off on 20 April, but became delayed, since the US Consulate had refused Visas to the cast and crew.  The team decided to relocate to Canada, postponing filming to June.  The shooting began for the film in Chennai in August 2011 and locations filmed at included Haasans office in October 2011, when Isha Sharvani was also a part of the shoot.  The film briskly progressed in late 2011, with scenes involving Samrat Chakrabarti filmed in November of the same year. Sets resembling Afghanistan were created in Chennai, with many foreigners from Russia, Iran and Africa playing American soldiers, while Haasan wore an Afghan look.  In November 2011, the team also pursued schedules in Amman and Petra in Jordan with Rahul Boses scenes being canned.  Haasan learnt Kathak from Birju Maharaj for an important portion in the film. 
 digital format, after several false starts Haasans previous projects. Speaking about it, he said "The fact that cinema is going digital is the biggest technological change today. We have to accept it, as it is happening globally and it will happen in Tamil Nadu too." 

Gouthami designed the costumes for the film. Shahrukh Khans Red Chillies Entertainment has taken care of the graphics part in Vishwaroopam.  A research had to be carried out on the American armys hierarchy for the costumes.  N. G. Roshan, who had previously worked in notable Malayalam films, took charge of the make-up for the film.  The villain had to undergo a heavy use of prosthetic make-up. A scene involving a war explosion had to show an injured jaw for him. Further, the artists appearing as war victims had blood and other forms of injuries applied on their bodies.  The Jaika stunt team, that was also involved in Billa II (2012) at the time, worked on the action sequences.  Haasans younger daughter Akshara Haasan joined the crew as an assistant director.  Kunal Rajan was roped in as a sound designer and the stunt crew for the film were imported in from Thailand.  In September 2012, Haasan revealed that the film would utilize Auro 3D sound technology, making it the first Indian film to do so. 

==Soundtrack==
 
Initially, Yuvan Shankar Raja was reported to compose the films musical score.  However, Kamal signed in the musical trio Shankar–Ehsaan–Loy to compose music for the trilingual, making it his second collaboration with them after Aalavandhan.  Lyricist Vairamuthu took charge of the lyrics of the songs in the film,  after Kamal Haasan had approached him and narrated the entire plot, to which he immediately agreed to work upon.  Javed Akhtar has penned lyrics for the Hindi version, while Ramajogayya Shastry has done the lyrics for the dubbed Telugu version.  The audio was released on 7 December 2012.     The Telugu version of the audio was released on 30 December at Hyderabad. Vishwaroopam was also released in Barco NV|Barcos 3D cinema sound.

==Marketing==
The makers planned the high-budget production    to premiere at the 2012 Cannes Film Festival.       A special screening of the film was arranged for Hollywood-based producer Barrie M. Osborne and make-up artist Michael Westmore.    Before release, it was reported to have been split into two parts, with each one having a separate release.   

The first look poster and a teaser of the film were released on May Day 2012 as part of the films marketing process. The poster consisted of Kamal Haasan wearing a green khaki jacket, with a flying pigeon and a skyline of a city consisting of several skyscrapers in the background.    Snippets from the film were unveiled during the International Indian Film Academy (IIFA) weekend and awards in Singapore in June 2012, as the actor-director screened excerpts from the film, attracting critical acclaim.    A one minute trailer was released at the award ceremony, which saw Kamal Haasan and Andrea take part in a press conference.    Salman Khan organised a special screening of Hindi version Vishwaroop at Ketnav Studio,Mumbai on 1 February 2013.  

An  . Movie Buzz. 7 November 2012. Retrieved 5 December 2012.   A special screening of Vishwaroopam was held at Tamil Nadu superstar Rajinikanths house on 6 February 2013 at the 6 Degrees theatre in the Auro 3D format for his friends.   On 9 February 2013 Kamal Haasan left for Paris for the French premiere on of Vishwaroopam. 

==Release==
The satellite rights of the films Tamil version were secured by STAR Vijay. The films Tamil version was given a "U/A" (Parental Guidance) certificate by the Indian Censor Board with minor cuts, while the Hindi version Vishwaroop, which was originally given an "A" certificate, went through minor cuts to receive the "U/A" certificate.    The makers planned the high-budget production  to premiere at the 2012 Cannes Film Festival.   The film released with over 3,000 prints worldwide.  While the movie was scheduled to release in theatres on 25 January 2013, the DTH release was planned for 2 February 2013 via six DTH players&nbsp;– Tata Sky, Airtel, Sun, Dish, Videocon & Reliance.

Vishwaroopam was scheduled to release in about 500 screens in Tamil Nadu, but the film was removed from screens by district collectors across the State, due to sustained protests by Muslim civic organisations.  In Andhra Pradesh, the film was scheduled to release in 300 screens by Siri Media. The film released in 82 screens in Kerala on 25 January.  After initial delays, Karnataka saw a full release on 29 January 2013 in 40 screens across the state. 

The Hindi version Vishwaroop was released in over 1,035 screens on 1 February 2013, thereby marking the biggest ever release for a Kamal Haasan film in Hindi. It was distributed by Balaji Motion Pictures. 

The film was scheduled to release in 250 screens in the overseas markets including 40 screens in Sri Lanka, 20 screens in Singapore, 20 screens in Canada. In Malaysia, the largest overseas market for Tamil films, Lotus Five Star distributed the film spending  4 million on copyright, print and promotion.  Blue Sky Cinemas distributed the Tamil and Telugu versions of the film in Canada and the USA. 
 Fourth Annual India International Film Festival of Tampa Bay on 16 February 2013 and 17 February 2013.
  The temporary suspension on Vishwaroopam was lifted by the Sri Lanka Public Performance Board on 10 February 2013.  In Malaysia, the National Censorship Board and the Malaysian Islamic Development Department reviewed the film and the Home ministry lifted the ban on 19 February 2013. 

==Reception==
{| class="wikitable infobox plain" style="float:right; width:23em; font-size:80%; text-align:center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review scores
|-
! Source
! Rating
|-
| Indiaglitz
|  
|-
| Behindwoods
|  
|-
| Indiandragon
|  
|-
| OneIndia
|  
|-
| In.com
|  
|-
| Deccan Chronicle
|  
|-
| Rediff.com
|  
|-
| colspan="2" style="text-align:center;"|   indicates that the given rating is an average rating of all reviews provided by the source
|}

===Vishwaroopam===
Vishwaroopam received highly positive reviews from critics. Sangeetha Devi Dundoo of The Hindu stated, "Vishwaroopam is a technically brilliant, ambitious film where most characters are not what they seem", and called the film "A gripping spy thriller of international standards".  Radhika Rajamani of Rediff gave 3 out of 5 stars and stated, "Vishwaroopam undoubtedly rests on Kamal Haasan, who is brilliant."    Vivek Ramz of in.com rated it 3.5 out of 5 and stated. "Vishwaroopam is a nicely made thriller and Kamal’s show all the way".  Behindwoods rated the film 4 out of 5 stars, calling it, "One of the commercially best made movies of Tamil cinema."  IndiaGlitz said, "Vishwaroopam comes with not just Hollywoodish feel but also its idiom". The critic further wrote "Sanu Vargheses cinematography is nimble and it is easily one of the biggest high points.  Mahesh Narayanans editing is proper."  L. K. Advani called it "one of the best films I have seen in years."  Cinemalead rated the film 4 out of 5 stars and wrote "On the whole Vishwaroopam is Kamal Hassans best movie till now. Go, watch and enjoy the ride." 

Indiandragon gave 3.5 dragons and said "Vishwaroopam is the Indian spy story no different but all together new".  Sify said, "It is technically brilliant with world class making and a subject which is truly international on global terrorism." and called it a "must watch for those who seek classy, stylish and extra-strong entertainment."  NDTV stated, "Vishwaroopam is likely to be appreciated by lovers of Hollywood action films." and pointed out, "The films only minus point&nbsp;– the placement of songs works as speed breakers for a spy thriller."  B.V.S. Prakash of Deccan Chronicle gave 3 stars and stated that the film "rides on performances."  Firstpost stated that "Vishwaroopam is non-stop action Hollywood style".  Praveen Kumar of OneIndia noted that the "film is on par with Hollywood standards" and gave 3.5/5. 

Anuja Jaiman of   said, "I think it   is Kamals best film as a director." and called it "a well made commercial entertainer".  In contrast, Baradwaj Rangan of The Hindu said, "The surprise about Vishwaroopam is how straightforward it is, given Kamal Haasan’s track record. (It’s basically a big, dumb action movie, but with smarts.)" 
Shailesh K Nadar of CinemaSpice.in rated Vishwaroopam as 3.5/5 mentioning in his review that "Vishwaroopam, a stylishly made engaging thriller, is a must-watch not just for Kamal fans, but also for all who love cinema and appreciate brilliant filmmaking."  Sudhish Kamath picked Vishwaroopam as one of five films that have redefined Tamil cinema in 2013, writing "Vishwaroopam is the most-layered film of the year and with every watch, you will find something that Kamal Haasan has planted in it, waiting to be discovered". 

{| class="wikitable infobox plain" style="float:right; width:23em; font-size:80%; text-align:center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|- Deccan Herald
|  
|-
| movies.ndtv.com
|  
|-
| India Weekly
|  
|-
| Bollywood Hungama
|  
|-
| Mid Day
|  
|-
| Hindustan Times
|  
|-
| Nowrunning
|  
|-
| CNN-IBN
|  
|-
| Rediff.com
|  
|-
| timesofindia.com
|  
|-
| colspan="2" style="text-align:center;"|   indicates that the given rating is an average rating of all reviews provided by the source
|}

===Vishwaroop===
The Hindi version "Vishwaroop" received positive reviews from critics.   awarded the movie 3.5 out of 5 stars.   

==Controversies==
  Tamil one. Law and order problems, however the film released in other states with greater Muslim populations than in Tamil Nadu.  The ban in Tamil Nadu triggered also the stop of screenings in neighbouring Indian states and foreign markets. 

Bollywood Director, Mahesh Bhatt, condemned the actions instructed by Chief Minister of Tamil Nadu Jayalalitha as a critical attack on freedom of speech in India,  but she denied all allegations against her regarding political and business interests.  After persistent pressure to cut objectionable scenes, Kamal Haasan said, he could be forced to leave the state of Tamil Nadu and India, because he was "fed up at being played around in a dirty political game".  He estimated the notional loss of revenue, due to banning policies, at almost   600&nbsp;million.  A mutual agreement with all 24 Muslim civic organisations was finally settled on 2 February 2013, when Haasan accepted to mute five scenes. 

==Box office==

===India===
On its opening day of release in Tamil Nadu, 7 February 2013, Vishwaroopam netted   at the box office.  The film accounted for 90% of the takings in Chennai box office on its opening weekend grossing  ,  89% on its second weekend,  78% on its third weekend,  38% on its fourth weekend,  28% on its fifth weekend  and 2.1% on its sixth weekend.  The film earned over   nett after eight weekends in Chennai box office. 

Despite the controversy, the Hindi version of the film did well at the box office with 45% opening  and netting  115&nbsp;million in its first week,  and a life time business of  190&nbsp;million nett.  The film completed a 100-day run in Tamil Nadu and 50 days in Bangalore city.

===Overseas===
The film suffered from piracy due to release in Canada and the UK while remaining banned in Malaysia, Sri Lanka and Singapore, major markets for Tamil films.

In Malaysia, the film grossed US$325,871 after three weekends.  The distributor refunded up to $1 million of advances to 65 exhibitors due to abrupt cancellation of shows. 

In the UK and Ireland, the film grossed £308,350 from 20 screens after five weekends. 

In the USA, the Tamil version grossed $1,039,994 from 44 screens after eight weekends  while the Telugu version grossed $200,293 after seven weekends from 21 screens. 

==Awards==
 
{|class="wikitable sortable" style="font-size:95%;"
|-
! Award
! Ceremony
! Category
! Nominee(s)
! Outcome
|- National Film National Film Awards 60th National Film Awards  National Film Best Production Design Boontawee Thor Taweepasas & Lalgudi N. Ilayaraja
| 
|- National Film Best Choreography Birju Maharaj
| 
|- Filmfare Awards South 61st Filmfare Awards South  Filmfare Award Best Director – Tamil Kamal Haasan
| 
|- Filmfare Award Best Actor – Tamil
| 
|- Vijay Awards 8th Vijay Awards Vijay Award Favourite Director Kamal Haasan
| 
|- Vijay Award Best Actor
| 
|- Vijay Award Best Director
| 
|- Vijay Award Favourite Hero
| 
|- Vijay Award Favourite Film
| 
|- Vijay Award Best Supporting Actress Andrea Jeremiah
| 
|- Vijay Award Best Villain Rahul Bose
| 
|- Vijay Award Best Cinematographer Sanu John Varghese
| 
|- Vijay Award Best Editor Mahesh Narayanan
| 
|- Vijay Award Best Cinematographer Sanu John Varghese
| 
|- Vijay Award Best Art Director Lalgudi N. Ilaiyaraja & Thor
| 
|- Vijay Award Best Male Playback Singer Shankar Mahadevan & Kamal Haasan
| 
|- Best Background Score Tubby Parik
| 
|- Vijay Award Best Choreographer Birju Maharaj
| 
|- Vijay Award Best Stunt Director Kecha Khamphakdee, Lee Whittaker, Parvez Feroz & T. Ramesh
| 
|- Vijay Award Best Best Make Up Artistes Gage Hubard & Ralis Khan
| 
|- Vijay Award Best Costume Designer Gautami Tadimalla
| 
|}

==Sequel==
Kamal Haasan and Atul Tiwari mentioned that the sequel Vishwaroopam 2 was already planned and more sequences had already been shot featuring Andrea Jeremiah in a more prominent role. 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 