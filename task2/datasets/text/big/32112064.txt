Hot Wheels World Race
 


{{Infobox television film
 | show_name = Hot Wheels World Race
 | image = 
 | caption =
 | distributor = Warner Bros. Television
 | studio = Mattel, Inc. Mainframe Entertainment Action Adventure science fiction|Sci-Fi
 | runtime = 110 minutes
 | creator = 
 | director = Andrew Duncan William Lau
 | starring = Andrew Francis Michael Donovan Kathleen Barr Brian Drummond Julian B. Wilson Kevan Ohtsji
 | producer = Jeff Gomez Gio Corsi Ian Richter Sharan Wood
 | country = United States
 | network = Cartoon Network 
 | released = July 12, 2003
 | num_episodes =  preceded_by =  followed_by = Hot Wheels AcceleRacers 
 | website =
 | writer = Mark Edens Jeff Gomez Fabian Nicieza
 | music = 
  |}}
 toy line Hot Wheels World Race was based on the television series film. It premiered on Cartoon Network on July 12, 2003 and was released on VHS and DVD on December 2, 2003.

==Plot==
After attaining his drivers licence on his 16th birthday, naive-teenager Vert Wheeler discovers the Deora II Hot Wheels car. The door on the front of the car opens, and a monitor shows a recording of a mysterious man named Dr. Peter Tezla, inviting him to a competition called the World Race: a contest between seasoned professionals and talented drivers. Driving to a secret location in the Californian desert, the drivers are informed of the race along an interdimensional racetrack called “Highway 35″, created by extraterrestrial beings called Accelerons. At the end of the race is the Wheel of Power. Tezla himself tried to retrieve it himself remotely with little success. His plan is to use the drivers, with their unique driving abilities, to retrieve the wheel on his behalf. To enter the Highway, each driver must be doing 300 mph (with the help of an enhanced Nitrous system called Nitrox2) at specific points around the world. During the start, Vert immediately starts a rivalry with Taro Kitano, leader of the Scorchers team; and Lani Tam, a driver of another Wave Rippers car like Vert’s.

During the first leg, the drivers are given a test of the type of track Highway 35 is, and the abilities of their cars. One such stunt is a mile-high loop followed by a jump over a lava river. Further on, in a volcano, a mysterious black racer, known as Zed-36, detonates a rigged bottle of Nitrox2 which causes lava to flow over the road. Vert and Taro make it through, but Lani gets caught in the flow and is forced to jump onto a nearby boulder. Taro immediately drives back to help her, eventually followed by Vert. All drivers finish the leg last, with the leader of the Street Breed team, Kurt Wylde, being victorious.

Before the second leg, each leader must make up five teams of seven: the Wave Rippers, Scorchers, Street Breed, Dune Ratz and Roadbeasts. Vert recruits his best friend, Alec Wood, and Kurt’s little brother, Markie, after watching him joyride in Kurt’s car. During the next leg, a mountainous stage, the leader of the Roadbeasts, Banjee Castillo, discovers a possible shortcut through a jungle valley. The Wave Rippers, curious of the Roadbeasts’ decision, follow suit. Things seem to go well for the Roadbeasts until they come across a large wheel obstacle, which they’ll need to cross. Whilst crossing it with the Wave Rippers, Zed-36 had a slingshot weapon ready to fire at the wheel, but hesitates after seeing Markie race towards it. After clipping the wheels as he crosses, which causes one of his Nitrox to detact and deonate, the wheels speeds up forcing Vert and Banjee to jump at speed. Meanwhile, both Taro and the leader of the Dune Ratz, Brian Kadeem, reach the portal first, causing a tie.

In the third leg, in a desert, Kadeem spots a disc on top of a pyramid. Believing it to be the Wheel of Power, he takes it, with Zed-36 taking chase. After a minor scuffle, Zed-36′s helmet is knocked off and he’s revealed to be Kurt Wylde. When he reaches the portal to head back to earth, the disc disintegrated, disappointing Kadeem. Revealing to Vert, he had hoped that he would win the race so he would use the prize money to aid his war-torn people in Africa. Meanwhile, as Kurt scarpers from the race, he is found by his employers CLYP, lead by a mysterious woman named Gelorum.

As the drivers are about to start the fourth leg of the race, Kurt, with several CLYP racers, make a break for the portal, enticing the other racers to follow. Throughout the ice leg, the CLYP racers try and sabotage the race by blocking the track and dropping mines. When Markie, who was leading detonates a mine which sees himself unconscious naging over a cliff, Kurt betrays the CLYP drivers, revealed to be drones, to help Markie. However, he continues to race Markie, then Vert, to retrieve the Wheel of Power for Gelorum. He is defeated by Vert, who contunies out of the ice and into a mysterious city, with the Wheel of Power perched on top of a spire in the city’s center.

Vert and the Wave Rippers are named the winners and are awarded $5 million prize money. However, wanting to race on Highway 35 again, storm into Tezla’s lab where Tezla had been researching the Wheel. After the Wheel’s powers short-circuits the facility’s power, Tezla admits the Wheel is too powerful and asked the racers to return it. Meanwhile, Gelorum and her drones surround the facility demanding the wheel. The racers managed to subdue the drones, even bringing Gelorum’s chopper down as well, revealing she is a robot as well.

Racing back into the city, the racers, including a defecting Kurt after realizing what Gelorum was, hold off the drones long enough for Vert to replace the Wheel on top of the spire, lighting up the city. In reflection, the racers name the city “Hot Wheels City”. Afterwards, Vert gives his prize money to Kadeem, knowing that the money may not solve their issues, but it helps.

Once returned to Earth, Vert is surprised to see his father at Tezla’s facility, and while given him a drive in Deora II, accidentally activates the Nitrox, accelerating the car to 300 mph, and activating the portal.

==Characters==

===World Racers===
The main competitors of the World Race. Although all the characters were named for the associating toy series, only a select number of characters starred in the movie. 

====Wave Rippers====
Their special abilities are jet boosters used to fly.

 
  Josef "Vert" Wheeler - An Black teenager from Compton California and the films main protagonist, Vert gets the chance to fulfill his dream of becoming the worlds greatest race car driver when he finds the Deora II parked outside his garage. He is chosen to lead the team "Wave Rippers" in the movie. His real first name is never formally revealed in the movie. He drives the Deora II. 

 Alec "Hud" Wood - Verts long time friend from California. He is shown to be more mature than Vert is. He eventually joins Verts team in between the first and second legs of the race. Though the biography states his nickname is "Hud" he is never called that during the movie.  He drives Switchback. 

 Finn Serpa - A risk-taking adventurer, Finn is willing to go anywhere and do anything. Before the World Race, he was a cliff surfer who used to hanglide off the rocky shores of Ireland. Now as a member of the Wave Rippers team, he must prove to team leader Vert-and more importantly to himself-that he can brave the perils of the World Race. 

 Lani Tam - A Hawaiian street racer who was chosen for the World Race because of her intelligence and mechanic skills. She is the first member of Verts team and is Taros love interest.  She drives a 1955 Chevrolet Nomad. 

 Felix Sharkey - Felix Sharkey is a cool and totally laid-back Northwestern beach racer, adept at short surf sand/water driving, through the dunes and jagged rocks of the coastline from Washington to Northern California. His slacker demeanor hides a streak of mischief. He drives Power Pipes. 

 Fluke - Fluke is one of the greatest ice racers in the world. Hailing from Greenland, his off-kilter perspective and friendly demeanor makes him fun to be around, but hard to race against-especially if the ground is frozen. His muscular frame belies a sweet demeanor and dry sense of humor that slips by most of his fellow racers. He drives a Chrysler Thunderbolt. 

 Mark "Markie" Wylde - A younger more inexperienced racer and little brother of Kurts. He sneaks his way into the race in between the first and second legs. Though he is unsuccessful in joining his brothers team his skills land him a spot of Verts.  He drives a Corvette Stingray. 
 

====Street Breed====
Their special abilities are "Eye Skies" which are used to help find alternate routes in difficult paths.

 
 Kurt Wylde - An aggressive street racer and the films secondary antagonist. Chosen for the World Race because he used to be a Grand Prix driver, but was banned after being framed for illegally modifying his race car. He is chosen to lead the team "Street Breed" in the movie. He is later on revealed to be the CLYP spy in the race going by the alias "Zed-36".  He drives Sling Shot. 

 Dan Dresden - A former street racer from San Francisco and member of the professional racing circuit. Dan is a member of the "Street Breed" and takes over as leader after Kurt is revealed to be a CLYP spy. He is the only driver besides Vert to make it past the avalanche in the last leg. a running gag throughout the film is that hell get over turned by a CLYP hover car.  He drives Side Draft. 

 Tono - The only thing known about Tono is that he is the only driver not to enter the ice track, due to the fact that he ran out of Nitrox 2 and couldnt make the portal. He drives a Pontiac Rageous. 

 Ricky Bell - Not much is known about Ricky other than the fact that he drives 24/Seven. 

 Bart "Shrimp" Scampi - Not much is known about Shrimp other than the fact that he drives Road Rocket. 

 San Jay Khan - Not much is known about San Jay other than the fact that he drives Aeroflash. 

 Maximo - Maximo appears as the one of the first Street Breed cars (along with Kurt) at start of the movie. He drives a Pontiac Firebird. 
 

====Roadbeasts====
Their special abilities are retractable saws capable of cutting through rocks and snow boulders.

 
  William "Banjee" Castillo - A Puerto Rican road racer who got chosen for the World Race due to being arrested many times for reckless driving. He also has experience in riding "motocross through the mountains of El Yunque." He is chosen to lead the team "Road Beasts" in the movie. His first name is never formally revealed in the movie. He drives Ballistik. 

 Ric "Griffin" Handy - A famous Italian off-road explorer. He is seen several times throughout the movie, but only has one line, "whoa!" He is a member of Banjees team.  He drives Zotic. 

 Pete Karris - driver of Twin Mill. 

 Jerry Boylan- driver of Moto-Crossed. 

 Skeet - A Florida racer whose reasons for being allowed into the World Race are never formally revealed. He is a member of Banjees team. He is most prominent in the second leg where he, Banjee, Esmeralda, Griffin, Vert, Markie, and Lani take a hidden shortcut; which ends up getting them last place for the leg.  He drives Vulture. 

 Yucatán -driver of Power Rocket. 

 Esmeralda "Es" Sanchez - Her origins are never revealed in the movie, though it is implied that she is from Puerto Rico. She is a member of Banjees team. She is most prominent in the second leg where she, Banjee, Skeet, Griffin, Vert, Markie, and Lani take a hidden shortcut; which ends up getting them last place for the leg. She also seems to be romanitcally involved with "Scorchers" racer Krakatoa, driver of Muscle Tone.  She drives Power Pistons. 
 

====Dune Ratz====
Their special abilities are tires that can "operate through the deepest sands."

 
 Brian "Zone" Kadeem - An African racer who was chosen for the World Race due to his skills in "driving in a sandstorm in a sahara." Throughout the movie he often speaks of his "people" who are his inspiration for joining the race; as he needs the prize money, which the winner gets, to feed them. He seems to be the wisest member of the main cast. He is chosen to lead the team "Dune Ratz" in the movie. He reveals Kurt to be the CLYP spy after a fight in the third leg of the race. Though the biography states his nickname is "Zone" he is never called that throughout the movie; instead they usually call him "Kadeem".  He drives Krazy 8s. 

 Mojave -driver of Wild Thing. 

 Toni Berry -driver of Toyota RSC. 

 Chuvo - a Venezuela dune buggy racer. He is briefly focused on when he and Rekkas take the lead in the third leg; only to wipe out. They do manage to get back on the track and make it out of the leg. He is a member of Kadeems team. He drives a Ford F-150. 

 Charlie Raffa -driver of Sweet 16 II. 

 Heralda -driver of 69 El Camino. 

 Rekkas - An "information broker" who is native to Morocco. He is briefly focused on when he and Chuvo take the lead in the third leg; only to wipe out. They do manage to get back on the track and make it out of the leg. He is a member of Kadeems team.  He drives Mega Duty. 
 

====Scorchers====
Their special abilities are plows that can "drive through the heart of a volcano."

 
 Taro Kitano - A Japanese drift racer who has skied down Mount Everest twice. He was chosen for the World Race due to his daring skills behind the wheel of a race car. He is chosen to lead the team "Scorchers" in the movie. He drives a 1970 Plymouth Road Runner. 

 Vesuvius-driver of 1/4 Mile Coupe. 

 Harrison-driver of Red Baron. 

 Everest-driver of 57 Thunderbird. 

 Wayne Casper-driver of Dodge Charger RT 

 Jet Blaney-driver of 63 Corvette 

 Kratakoa-driver of Muscle Tone. 
 

===Others===
*Dr. Peter Tezla - Founder and owner of the Scrim Corporation, who creates racing technology allowing him to create the cars. He discovered Highway 35 in ancient writings and inscriptions. Though he seems to be sinister, he actually only wants to use the "Wheel of Power" to benefit mankind as an "unlimited source of clean energy." He is also very manipulative, such as only using the World Race as a pretext to retrieve the Wheel. Like the World Race competitors, he is never directly implied to be a descendant of the Accelerons, but there is a possibility due to his technology he developed.

*Gig - Gig is a robotic assistant to Dr. Tezla. He helps with coordinating the start and finish of the races. His system crashes when the "Wheel of Power" over loads every electronic system in North America, but Dr. Tezla is able to restore him.

*Gelorum - the films main antagonist. She is a cyborg who recruits Kurt to sabotage the race in order to restore his credibility in the professional racing circuit. She is the leader of the organization CLYP and wants the Wheel of Power for her own evil purposes.

*Major Jack "Rabbit" Wheeler - Verts father and a Major of an Armed Force. His relationship with his son is shown to be strained until the end of the movie when he acknowledges Verts efforts of having "the courage to do what was right" during (and after) Highway 35 but still grounds Vert for a month presumably for his own actions. It is possible he was meant to race on Highway 35 instead of Vert.

 

 
 
 
 
 
 
 