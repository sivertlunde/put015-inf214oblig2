Faith Happens
{{Infobox film
| name           = Faith Happens
| image          = Faith Happens.jpg
| alt            =  
| caption        = 
| director       = Rick Garside
| producer       = Michael Baumgarten Jennifer Dornbush Rick Garside Robert Huberman Steven Mark
| writer         = Rick Garside
| starring       = 
| music          = David Siebels
| cinematography = Bengt Jonsson
| editing        = 
| studio         = Side By Side Films
| distributor    = 
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = $1,000,000
| gross          = 
}}
Faith Happens is a 2009 Christian film directed by Rick Garside.  Garside, who has created many successful films over the last 20+ years, collaborated with singer Pat Boone to make the film after noticing a growing debate regarding the need for churches.  Many of Faith Happens scenes were shot in a Kenyan refugee camp, town and National Park.   

==Cast==
*Bruce Marchiano as Peter
*Peter Husmann as John
*Sierra Wingert as Amy
*James Wong as Jimmy
*Austin Kemie as Masa

==Release==
Faith Happens was released nationwide in Kenya on August 21, 2009 by Ster-Kinekor|Ster-Kinekor Theatres. 

==References==
 

==External links==
*  
*   at Facebook
*  

 
 
 
 
 
 


 