Over the Rainbow (film)
{{Infobox film
| name           = Over the Rainbow
| image          = Over the Rainbow film poster.jpg
| caption        = Theatrical poster
| film name = {{Film name
 | hangul         =  
 | rr             = Obeo Deo Reinbou
 | mr             = Obŏ Tŏ Reinpou}}
| director       = Ahn Jin-woo
| producer       = 
| writer         = Ahn Jin-woo Jang Hyeok-rin Jo Myeong-joo
| starring       = Lee Jung-jae Jang Jin-young Jung Chan
| music          = Park Ho-joon
| cinematography = Kim Young-chul
| editing        = Park Gok-ji
| distributor    = A-Line
| released       =  
| runtime        = 109 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = $2,549,834 
}} 2002 South Korean film starring Lee Jung-jae and Jang Jin-young.

== Plot ==
Weather presenter Jin-su is involved in a car accident, and though physically unhurt, he is left with a case of selective amnesia. Haunted by the memory of a woman he cant quite recall, he sets out to find her identity by revisiting some of his old college friends. The person giving him the most help is Jeong-hee, but as she helps him piece together his broken memories they start to develop feelings for one another, and Jin-su realizes that perhaps his forgotten past isnt worth chasing after all.

== Cast ==
* Lee Jung-jae ... Lee Jin-su
* Jang Jin-young ... Kang Jeong-hee
* Jung Chan ... Choi Sang-in
* Uhm Ji-won ... Kim Eun-song
* Gong Hyung-jin ... Kim Young-min
* Kim Seo-hyung
* Choi Jae-won

== References ==
 

== External links ==
*  
*  
*  
*   at Koreanfilm.org

 
 
 
 
 
 


 
 