Bryllupsfotografen
{{Infobox film
| name           = Bryllupsfotografen
| image          = 
| image size     =
| caption        =  Johan Bergenstråhle
| producer       = 
| writer         = Johan Bergenstråhle   Dag Solstad
| narrator       =
| starring       = Kurt Ravn   Ilse Rande 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 14 October 1994
| runtime        = 85 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian drama Johan Bergenstråhle, starring Kurt Ravn and Ilse Rande. Daniel Svare (Ravn) is an accomplished photographer and a socially engaged documentary film maker, who experiences a mid-life crisis. He leaves his wife and children to start up as a wedding photographer in his old home town, but his past catches up with him.

==External links==
*  
*   at Filmweb.no (Norwegian)

 
 
 
 