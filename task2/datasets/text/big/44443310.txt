Vagabond Lady
{{Infobox film
| name           = Vagabond Lady
| image          = 
| alt            = 
| caption        =  Sam Taylor
| producer       = Sam Taylor  Frank Butler Robert Young Evelyn Venable
| music          = 
| cinematography = Jack MacKenzie
| editing        = Bernard W. Burton
| studio         = Hal Roach Studios
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Sam Taylor Frank Butler. Robert Young and Evelyn Venable. The film was released on May 3, 1935, by Metro-Goldwyn-Mayer.  

==Plot==
Irresponsible, happy-go-lucky Tony Spear returns home to the United States and his family after years of sailing throughout the Orient with his crewman Corky Nye. His wealthy department store owner father R. D. Spear and brother John are fearful of what damage he will do to their reputations as staid, respected businessmen, remembering what happened the last time. 

John has some news for his brother; he has just proposed marriage to Jo Spiggins, who grew up with them both and is now a valued and trusted store employee. Tony is delighted, at first. When John has to go away on a business trip, he asks Tony to use his influence to persuade Jo to accept his proposal. Jos father "Spiggy" Spiggins, a store manager and classmate of R. D.s, does not think John is a good match for his daughter, preferring Tony as a son-in-law. When he sees how much happier she is after she goes out with Tony, he tries to get Tony to marry Jo himself, but the young man is not interested in matrimony and views Jo more as a sister. 

As time goes on, however, he begins to come around to Spiggins viewpoint. Jo warms to him too, until they go to a diving exhibition. First, she does not like it when Tony invites two female friends and their dates to their table. Then Tony gets drunk and dives off the board himself while dressed in formal evening clothes. After that, she stalks off. When Tony runs after her, some men try to restrain him; he starts a fight and is jailed. Jo then accepts Johns proposal.

Spiggins tries to sabotage the wedding by getting drunk and hiding out on Tonys sailboat, the Vagabond Lady, figuring if he is not there to give his daughter away, they cannot proceed. Jo tracks him down and assumes (as Spiggins had plotted) that it is Tony who is trying to derail the ceremony. Despite their mutual hostility toward each other, Tony offers to sail her to the wedding in Westport, giving them enough time to get her father sober. On the way, Tony tells Corky to throw him overboard if he so much as touches Jo. When Corky gets drunk (with Spiggins) and a storm comes up, Tony has to get Jo to help him sail the boat. They quarrel, but it ends with him kissing her. A drunk Corky sees this, and after Jo goes below, he sneaks up behind Tony and kicks him off the boat. The next morning, Corky has only a hazy memory of what happened, but tells Jo that Tony has a habit of leaving him to deal with women he abandoned. Jo believes him and decides goes through with the wedding. However, Tony (having hijacked a fishing boat that picked him up) manages to get there just in time to persuade his brother that Jo would not be respectable enough for his career and reputation. Tony then drags a delighted Jo away.

== Cast == Robert Young as Tony Spear
*Evelyn Venable as Miss Josephine "Jo" Spiggins Reginald Denny as John "Johnny" Spear
*Frank Craven as "Spiggy" Spiggins
*Berton Churchill as R. D. Spear
*Ferdinand Gottschalk as Mr. "Higgy" Higginbotham, a store manager
*Forrester Harvey as Corky Nye

== Reception ==
The New York Times reviewer called it "a frothy, bubbling and sparkling farce." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 