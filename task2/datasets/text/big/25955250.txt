Perfect Life (film)
 {{Infobox film
| name           = Perfect Life
| image          = 
| director       = Emily Tang
| producer       = Chow Keung Jia Zhangke Li Xiudong
| writer         = 
| starring       = Yao Qianyu Chen Taisheng Jenny Tse
| cinematography = Lai Yiu-fai
| editing        = Chow Keung
| music          = 
| distributor    = Celluloid Dreams
| studio         = Xstream Pictures
| released       =  
| runtime        = 101 minutes
| country        = China
| language       = Mandarin
}}
Perfect Life is a 2008 Chinese-Hong Kong film by Emily Tang and produced by director Jia Zhangke (who received a co-producer credit) and his company, Xstream Pictures. The film mixes elements of dramatic fiction and documentary film.

==Release and screenings== Dragons and Tigers program in the 2009 Vancouver International Film Festival, a program which it won due to "the way it captures the harshness of Chinese reality through its fictional protagonist, and for the subtlety of its wonderfully free storytelling," according to the award jury.  The film also captured the "Golden Digital Prize" at the Hong Kong International Film Festival. 

===Melbourne controversy=== Cry Me a River.    The decision by Jia was due to the Melbourne Film Festivals decision to air a documentary film, The 10 Conditions of Love, about the Uigher advocate, Rebiya Kadeer, who is a highly controversial figure in China.  In a letter to the festivals organizers, Jia stated that pulled both to protest against Kadeers attendance of the event. A third film, the Chinese documentary Petition (film)|Petition, was also withdrawn due to the controversy. 

==Reception==
Despite its win at Vancouver, the film received mixed reviews from western critics. Derek Elley of the industry magazine, Variety (magazine)|Variety, called it "rote, anomie-heavy" and ultimately a "step back after Tangs promising 2001 debut, Conjugation (film)|Conjugation.  While an Asian-cinema focused academic magazine found the films mix of drama and documentary elements to be an unsuccessful stylistic choice.  A similar review came from The Hollywood Reporter, which also questioned the mix of the documentary film story starring the real Jenny Tse, and the fictional story starring Yao Qianyu, noting that the film fails is that the audience "never quite understand the connection between the two." 

The film did have its champions, however. Some found the films style far more palatable and one reviewer listed it as one of the top independent and foreign films from 2008. 

==References==
 

==External links==
*  
*   at the Chinese Movie Database

 
 
 
 