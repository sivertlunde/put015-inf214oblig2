Posse from Hell
{{Infobox film
| name           = Posse from Hell
| image          = "Posse_from_Hell"_(1961).jpg
| caption        = British quad poster
| director       = Herbert Coleman Gordon Kay Edward Muhl
| writer         = Clair Huffaker John Saxon Zohra Lampert Vic Morrow
| music          =  Joseph Gershenson (uncredited) and library music by various
| cinematography = Clifford Stine
| editing        = Frederic Knudtson
| distributor    = Universal Studios#Universal-International|Universal-International
| released       =  
| runtime        = 89 min.
| country        = United States
| language       = English
| budget         = $500,000 Don Graham, No Name on the Bullet: The Biography of Audie Murphy, Penguin, 1989 p 291 
}}
 1961 Audie Western written by Clair Huffaker based on his 1958 novel of the same name.  Herbert Coleman made his directoral debut. 

==Plot== town marshal Issac Webb (Ward Ramsey) and takes ten men as hostages, killing some to ensure the four are unmolested.  The gang leaves town with $11,200 from the Bank of Paradise and a female hostage Helen Caldwell (Zohra Lampert) who entered the bar because her alcoholic Uncle Billy (Royal Dano) was one of the captives.
 posse to rescue Helen and bring the men to justice.  Though not a criminal, Cole is a loner that Webb wishes to enter the community through his being deputised. Cole is enraged to discover that the townspeople have put Webb on a table next to the three dead bodies of those murdered by the four.  The doctor (Forrest Lewis) said at first they thought Webb was dead himself, then realised he couldnt be moved so left him among the corpses.

Webbs last act is to deputise Cole telling him to do the right thing, not out of hate, but out of liking people as the townsfolk are good people who have had bad things done to them.  Cole agrees only out of liking Webb.  The laconic Cole makes his original plan for hunting down the four by himself clear by turning down the offer of Webbs handcuffs by saying "I wont be needing any." However, town elder Benson convinces Cole to follow Webbs wishes and organize a posse.

The men of the town gather but enthusiasm wanes when not as many able bodied men as expected volunteer to go up against the killers, some men leaving because the posse doesnt outnumber the killers by ten to one.  Coles frank assessment of the situation scares others off with Cole saying "If theyre afraid of words they shouldnt go."
 Robert Keith), Paul Carr), John Saxon), Rudolph Acosta), an Indian who merely thinks that joining is "the right thing to do."

Cole doesnt want any of the inexperienced and troublesome men to come with him but he has no choice.  The posse discover Helen who has been left behind tied up near a rattlesnake that Cole is able to remove from Helens vicinity.  Helen has been raped and is unwilling to return to the town to face the shame of being vilified by the population.  Cole orders the willing Uncle Billy to return her by force if necessary.

Captain Brown demonstrates his aged incompetence by disobeying Coles orders and opening fire and nearly murdering four cowhands who he mistakes for the four killers.  Cole has to wound Jeremiah to stop his shooting spree and orders him back to town with the cowhands who have been waylaid by the killers.

Coles distrust of his own posse begins to subside when he is impressed by the determination of the inexperienced Seymour who has never ridden a horse or used a firearm before and the quiet Johnny Caddos acceptance of the prejudicial treatment he gets from the posse. The posse tracks the four to a farmhouse and surrounds it until Hogan makes a noise starting a gunfight.  Cole kills one of the outlaws. The boasting Wiley is unable to actually kill a man and is killed as he freezes, allowing the remaining three to escape.  Hogan begins shooting the corpse of the outlaw that Cole himself killed telling himself and the posse that Hogan himself killed the man who killed his own brother.  When the men note that all the witnesses agree that it was actually Hash who had murdered his brother, Hogan refuses to listen and leaves the posse to return to town.  

Cole, Caddo, and Seymour continue tracking the party to the desert but realise that the outlaws have doubled back and are intending on returning to shoot up Paradise.

==Cast==
*Audie Murphy as Banner Cole John Saxon as Seymour Kern
*Zohra Lampert as Helen Caldwell
*Vic Morrow as Crip Robert Keith as Jeremiah Brown
*Rodolfo Acosta (credited as Rudolph Acosta) as Johnny Caddo
*Royal Dano as "Uncle" Billy Caldwell
*Frank Overton as Burt Hogan James Bell as Mr. Benson Paul Carr as Jock Wiley
*Ward Ramsey as Marshal Isaac Webb
*Lee Van Cleef as Leo
*Ray Teal as the bank manager
*Forrest Lewis as Dr. Welles Charles Horvath as Hash
*Harry Lauter as Russell Henry Wills as Chunk Stuart Randall as Luke Gorman
*Allan Lane as Burl Hogan

==Production== associate producer Whispering Smith TV series and the feature Battle at Bloody Beach.  Coleman filmed at Lone Pine, California with one location being the aptly named Rattlesnake Hill where thirty rattlesnakes were removed before filming could commence.  
 method actress whose adlibbing frequently confused Murphy, but the two worked out their scenes together. 

Huffakers screenplay deviated from his novel by having Murphys character as an outsider gunfighter rather than the Marshalls established deputy. His script emphasises the similarity between Coles voluntary exclusion from society with Helens sudden involuntary exclusion with her rape.  Helen attempts suicide then later talks about becoming a prostitute.  She is talked out of both by Cole with the two eventually finding a place in society together.  Huffaker stated that when writing a screenplay for the Medal of Honor awardee Audie Murphy, he had to put Murphys character in "a situation where he has to do something bigger than life.  So it really kind of fit him in a way".   

Through his viewing the actions of Caddo, Kern and Helen more than make up for the negative traits in the townspeople, Cole ends the film saying "There is always someone or something worthwhile.  We just have to look hard enough". 

==References==
 
*Gossett, Sue, The Films and Career of Audie Murphy, Americas Real Hero, Empire Publishing, 1996, pp. 119–122.

==External links==
* 
*  at TCMDB

 
 
 
 
 
 
 
 
 