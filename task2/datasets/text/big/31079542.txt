Salute John Citizen
 
 
Salute John Citizen is a 1942 British drama film directed by Maurice Elvey and starring Edward Rigby, Mabel Constanduros and Jimmy Hanley.  The Bunting family face up to the fortunes of war during the Second World War. It was based on the novels Mr. Bunting and Mr. Bunting at War by Robert Greenwood.

==Cast==
* Edward Rigby - Mr. Bunting 
* Mabel Constanduros - Mrs. Bunting 
* Jimmy Hanley - Ernest Bunting 
* Eric Micklewood - Chris Bunting 
* Peggy Cummins - Julie Bunting 
* Dinah Sheridan - Evie 
* Charles Deane - Bert Rollo 
* Stanley Holloway - Oskey 
* George Robey - Corder 
* David Keir - Turner

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 