At Sword's Point
{{Infobox film
| name           = At Swords Point
| image          =
| image_size     =
| alt            =
| caption        = Lewis Allen
| producer       = Jerrold T. Brandt
| writer         = Aubrey Wisberg Jack Pollexfen
| narrator       =
| starring       = Cornel Wilde Maureen OHara
| music          = Roy Webb Constantin Bakaleinikoff
| cinematography = Ray Rennahan
| editing        = Samuel E. Beetley Robert Golden
| studio         =
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 81 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 historical action Lewis Allen and starring Cornel Wilde and Maureen OHara. It was shot in Technicolor by RKO Radio Pictures. The film was completed in 1949, but was not released until 1952.
 Queen Anne to halt the villainy of her treacherous nephew, the Duc de Lavalle. 

==Plot==
The sons (and a daughter) of the original Four Musketeers ride to the rescue of besieged Queen Anne in 1648 France.

DArtagnan and his companions are alerted that the terminally ill Queen (Gladys Cooper) is being pressured by the evil Duc de Lavalle (Robert Douglas) into agreeing to a marriage with Princess Henriette (Nancy Gates). Too old (or dead) to respond, their sons (and one daughter) race to Court to help.

After much derring do - including episodes of imprisonment and betrayal, with a burgeoning love sub-plot between DArtagnan Jr. and Claire, daughter of Athos (Maureen OHara) thrown in for good measure - they succeed.

==Cast==
* Cornel Wilde as DArtagnan
* Maureen OHara as Claire Robert Douglas as Duc de Lavalle
* Gladys Cooper as Queen Anne
* June Clayworth as Comtesse Claudine
* Dan OHerlihy as Aramis
* Alan Hale Jr. as Porthos
* Blanche Yurka as Madame Michom
* Nancy Gates as Princess Henriette
* Edmund Breon as Queens Chamberlain (as Edmond Breon) Peter Miles as Young Louis XIV George Petrie as Chalais
* Moroni Olsen as Old Porthos

==Notes== The Man in the Iron Mask (1939) as an aging Porthos.

* In another Three Musketeers movie, The Fifth Musketeer (1979), which retells the story of The Man in the Iron Mask, two of the young Musketeers from At Swords Point reappear in the roles of their own fathers: Cornel Wilde stars as DArtagnan and Alan Hale Jr. as Porthos.

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 