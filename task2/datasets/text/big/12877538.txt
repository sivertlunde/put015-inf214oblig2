The Clown at Midnight
{{ Infobox Film | name = The Clown at Midnight| image = caption = writer = Kenneth J. Hall | starring = Margot Kidder Sarah Lassez Tatyana Ali  Ryan Bittle |
 director = Jean Pellerin |
 producer = Gary Howsam Gilles Paquin |
 cinematography = Barry Gravelle|
 editing = Robert Lower	|
 narrator = David Kirschner Corey Sienega |
 studio =  Fries Film Group Hallmark Entertainment |	 
 distributor = GFT Paquin Entertainment|
 released = 1998|
 runtime =  91 minutes |
 language =  English |
 budget = $12,000,000 | 
 music = Glenn Buhr |
| preceded_by =  |
}}
 horror film directed by Jean Pellerin and starring Sarah Lassez, James Duvall, Tatyana Ali, Christopher Plummer and Margot Kidder.

== Plot ==
Years ago, opera singer Lorraine Sedgewick (Vicki Marentette) was killed in her dressing room at an opera house, supposedly by Lorenzo Orsini (Jonathan Barrett), one of the lead actors in a performance of Pagliacci. When Orsini was thought to have fled to Europe afterwards, the opera house closed down. 

Years later, high school student Kate (Sarah Lassez) is plagued with nightmares after discovering she is Lorraines daughter. Kates best friend Monica (Tatyana Ali) convinces Kate to help restore the opera house, and when they arrive they meet the rest of the group including nerdy Cheryl "Walnut" Webber (Melissa Galianos), flamboyant Marty (J. P. Grimard), rebel George (James Duvall), vindictive Ashley (Liz Crawford) and her jock boyfriend Taylor (Ryan Bittle). The group are lectured by their teacher Ms. Gibby (Margot Kidder), however a stage light falls and nearly hits her, drawing the attention of the owner of the opera house Mr. Caruthers (Christopher Plummer). The group deduces it was an accident, however Mr. Caruthers insists the opera house is haunted by Lorraine, upsetting Kate who is comforted by Ms. Gibby. Soon after, Ms. Gibby leaves, allowing the teenagers to lock up, who instead engage in pizza and beers, before investigating Lorraines murder scene. They find a patch of fresh blood, that causes Kate to have a vivid vision of her mothers death. While everyone else leaves, Monica consoles Kate, before the pair discover Lorenzo is Kates father.

Ms. Gibby arrives the following morning, but is swiftly murdered with an axe by the killer, who is dressed like the clown that killed Lorraine. When the group arrive they begin cleaning, despite Ms. Gibbys absence. Ashley and Taylor sneak off to have sex, however Taylor ditches Ashley when they get into an argument, before the killer attacks Ashley and strangles her to death. Hearing the attack, Monica begins to investigate, only to find the clown who chases her until she reaches the rest of the group, where it is revealed that George dressed up as the clown to scare her. The group decide to try and locate the missing Ashley and Ms. Gibby, but while in the basement Monica is attacked by the clown who chases her and finally stabs her with a spear. Meanwhile, Kate attempts to phone the school but the phone is cut dead before she discovers they have become locked in the opera house. The group begin to panic and soon after find Ashley hung on the stage. Marty falls through a stage door, and Kate and George rush to save him, however he is electrocuted to death. Meanwhile, Taylor and Cheryl reach the roof to escape, but the clown grabs Taylor and throws him off the roof, killing him while Cheryl flees, however she is soon decapitated.

Kate and George decide Lorenzo is not the killer, before they are split up. Kate runs to the auditorium and is chased by the clown onto a catwalk where she runs into Mr. Caruthers. Mr. Caruthers sends the clown over the catwalk, killing him, but Mr. Caruthers then turns on Kate and knocks her out. Kate awakes tied up in the auditorium with the victims bodies propped up on the seats. It is revealed Mr. Caruthers killed Lorraine because she denied him love, before another clown arrives and attacks him. In the panic, the clown is knocked out, but Kate breaks free and manages to kill Mr. Caruthers. The clown is revealed to be George, who was told by Kates father, who had been living in the opera house, all about Mr. Caruthers before he had been killed on the catwalk. Kate and George then break free from the opera house.

==Cast==
* Christopher Plummer as Mr. Caruthers
* Margot Kidder as Ms. Ellen Gibby
* Sarah Lassez as Kate Williams
* James Duvall as George Reese
* Tatyana Ali as Monica
* Ryan Bittle as Taylor Marshall
* Melissa Galianos as Cheryl Walnut Webber
* J.P. Grimard as Marty Timmerman
* Liz Crawford as Ashley
* Vicki Marentette as Lorraine Sedgewick
* Jonathan Barrett as Lorenzo Orsini
* John Bluethner as Arnold Williams
* Pauline Broderick as Julia Williams

== Location ==
  Walker Theatre Richardson Building and 201 Portage at Portage and Main can be seen.

== External links ==
*  

 
 
 
 
 