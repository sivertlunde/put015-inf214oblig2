Holocaust (miniseries)
{{Infobox television
| show_name            = Holocaust
| image                =  
| size                 = 250px
| caption              = DVD cover
| show_name_2          = 
| genre                = Miniseries Drama Gerald Green Gerald Green
| director             = Marvin J. Chomsky David Warner Fritz Weaver John Houseman
| narrated             = 
| theme_music_composer = Morton Gould
| country              = United States
| language             = English
| num_seasons          = 1
| num_episodes         = 4
| executive_producer   =  Robert Berger   Herbert Brodkin Craig McKay   Stephen A. Rotter
| location             = 
| cinematography       = Brian West
| camera               = 
| runtime              = 475 minutes
| company              = 
| distributor          =  National Broadcasting Company (NBC)
| picture_format       = 
| audio_format         = 
| first_run            = 
| first_aired          =  
| last_aired           =  
}} German Jews war criminal. Holocaust highlighted numerous important events which occurred up to and during World War II, such as Kristallnacht, the creation of Jewish ghettos and later, the use of gas chambers. Although the miniseries won several awards and received critical acclaim, it was criticized by some, including noted Holocaust survivor and author Elie Wiesel, who described it as "untrue and offensive." 

The series was presented in four parts:
* Part 1: The Gathering Darkness (original airdate: April 16, 1978)
* Part 2: The Road to Babi Yar (original airdate: April 17, 1978)
* Part 3: The Final Solution (original airdate: April 18, 1978)
* Part 4: The Saving Remnant (original airdate: April 19, 1978)

 

==Plot== German Jewish family, the Weisses, consists of Dr. Josef Weiss (Fritz Weaver), the father; Berta Weiss (Rosemary Harris), the mother and talented pianist; Karl Weiss (James Woods), an artist who is married to a Christian woman named Inga Helms-Weiss (Meryl Streep); Rudi Weiss (Joseph Bottoms), an independent, rebellious soccer player; Anna Weiss (Blanche Baker), the young daughter; and Moses Weiss (Sam Wanamaker), Josefs brother and a chemist from Warsaw. Throughout the series, each member of the Weiss family experiences hardships and are ultimately led to a terrible fate, with the exception of Rudi and Inga.
 deported to Poland for being a foreign citizen. He becomes a member of the Judenrat (Jewish council) for the Warsaw ghetto. Josef is sent to Auschwitz along with his wife for attempting to save Jews from the Warsaw ghettos liquidation process. At Auschwitz, he is assigned to road labor for Uncle Kurt (of Erik Dorf; see below), who is trying to save several Jews by having them work for him. Uncle Kurt then is punished for using Jews when he shouldnt have and the Jews on his crew (including Josef) are all sent to the gas chambers.

Mrs. Berta Weiss, after her husbands deportation, survives with the help of Inga and her family. She is later deported to the Warsaw ghetto to be reunited with Josef. Berta then obtains a job teaching at the school before eventually being sent to the gas chamber (at Auschwitz).
 sanitarium in Hadamar and killed by carbon monoxide poisoning as part of the Nazi Action T4.

Karl Weiss is arrested and sent to Buchenwald. Later, a family friend of Ingas, Heinz Muller, has Karl transferred to Theresienstadt where he works in the art studio. He and the other artists secretly make pictures depicting the reality of the Ghetto. When the pictures are discovered, the artists are tortured by the SS and all but Karl die. Karl is then (after hearing of his wifes pregnancy) transferred to Auschwitz and put on the Sonderkommandos, he finds out that both of his parents were taken to Auschwitz. Subsequently, Karls health deteriorates badly and he dies the day Auschwitz is liberated.

Rudi Weiss, having run away from Berlin, goes to Czechoslovakia, where he meets Helena Slomova (Tovah Feldshuh).  They escape together to the Ukraine where they fight for years with Jewish partisans, led by Uncle Sasha, a doctor who lost his family earlier in the war. After fighting against SS and Ukrainian soldiers, Rudi is ultimately captured, and Helena is shot and killed. Rudi wakes up in Sobibor where he meets Leon Feldhandler and Alexander Pechersky and escapes, during the uprising. Rudi decides to travel alone back through Europe and find his family.
  as Inga Helms-Weiss]]
Moses Weiss owns a pharmacy in Warsaw. When Josef and the Lowys are deported, he finds a place for them to stay. Like his brother, he is put on the Judenrat. After hearing that the SS are planning to kill all Jews in Europe, he starts a resistance movement. This movement fights against the SS in the Warsaw Ghetto Uprising, after receiving weapons from Christian Poles. They are initially successful, but the SS discovers their secret hiding places and uses gas to force them to walk out and face the wall, where they are all shot.

Karls wife, Inga, eventually sacrifices her freedom to join him in Theresienstadt where he is commissioned as an artist. Desperately trying throughout most of the series to reach Karl in various camps, Inga can only get letters through to him if she performs sexual favors for Heinz Muller. Threatening to keep her husband involved in heavy physical work if she does not acquiesce to his requests, the SS sergeant rapes her. After arriving at Theresienstadt to reunite with Karl, Inga becomes pregnant with his child. A fellow artist sells Karls paintings of horrific concentration camp scenes, the Gestapo finds them, tortures the artists and when Karl refuses to aid the Nazi investigation, he is sent to Auschwitz.
 Greek children.

Erik Dorf (Michael Moriarty), a lawyer from Berlin, is the focus of the other main storyline in the series. At the urging of his ambitious wife, Marta, the unemployed and apolitical Dorf joins the SS, becoming the personal clerk of Reinhard Heydrich. Dorf rises through the ranks of the SS, becoming famous for developing legalistic justifications and euphemisms for the anti-Jewish campaign.

Coordinating mass murder burdens his conscience at first, but his ruthlessness escalates as he discovers that ideological fervor gains him prestige.  This backfires after a feud with field SS officers, who resent carrying out the gruesome tasks he mandates in Berlin, results in an anonymous letter to Heydrich accusing him of communist sympathies that stunts his career.
 follow orders, with himself and his wife seeing it as justification for the orders he followed before and ultimately removing evidence of his misdeeds.

After the war ends, he is captured by the United States Army, and told that he will be tried for war crimes. Dorf decides to follow the example of many other Nazi officials and commits suicide by taking a cyanide pill.

==Cast==
 
 
* Fritz Weaver as Dr. Josef Weiss
* Joseph Bottoms as Rudi Weiss
* Michael Moriarty as Erik Dorf David Warner as Reinhard Heydrich
* T. P. McKenna as Colonel Blobel
* Tovah Feldshuh as Helena Slomova
* Marius Goring as Heinrich Palitz
* Rosemary Harris as Berta Palitz Weiss
* Ian Holm as Heinrich Himmler
* Lee Montague as Uncle Sasha
 
* Deborah Norton as Marta Dorf George Rose as Mr. Lowy
* Robert Stephens as Uncle Kurt Dorf
* Meryl Streep as Inga Helms Weiss
* James Woods as Karl Weiss 
* Blanche Baker as Anna Weiss
* Sam Wanamaker as Moses Weiss
* Michael Beck as Hans Helms
* Tony Haygarth as Heinz Muller Tom Bell as Adolf Eichmann
 

==Production== Robert Berger, and was filmed on location in Austria and West Berlin. It was broadcast in four parts from April 16 to April 19, 1978. The series was popular, earning a 49% market share; it was also received well in Europe.
 Gerald Green, who later adapted the script into a novel.

The miniseries was rebroadcast on NBC from September 10 to September 13, 1979, a year and a half after its original broadcast.

==Reception==

===Critical reception===
Some critics accused the miniseries of trivializing the Holocaust. The television format meant the realism of the situation was muted, while the fact that NBC made a financial gain from advertising led to accusations that the tragedy was being commercialized. Holocaust  s creators defended it by arguing that it was an important factor in developing and maintaining awareness of the Holocaust. The television critic Clive James commended the production. Writing in The Observer (reprinted in his collection The Crystal Bucket), he commented:

 

Moreover, the Polish community in the United States found controversial and untrue that soldiers who were supervising transports of Jews and were executing them during Warsaw Ghetto Uprising were dressed in Polish military uniforms. 
 German Word of the Year. 

===Awards=== David Warner, Sam Wanamaker, Tovah Feldshuh, Fritz Weaver and Rosemary Harris were all nominated for, but did not win, Emmys. However, Harris won a Golden Globe Award (for Best TV Actress - Drama) for her performance, as did Moriarty (for Best TV Actor - Drama). 

==DVD releases==
Holocaust was released in the U.S.A. as a Region 1 DVD by Paramount Pictures and CBS Home Entertainment on May 27, 2008. The Region 2 DVD followed on 15 August 2010.
A disclaimer on the DVD packaging states that it may be edited from the original network broadcast version and is shorter at 446 mins. The Region 4 DVD is unusually in native NTSC format having not been converted to PAL. No information is currently available to explain the reason for the half hour of missing footage. However, it seems to be clear that the time difference is not simply due to the NTSC/PAL conversion 4 percent speed-up effect.

==See also==
* List of Holocaust films
* Warsaw Uprising

==References==
 

==External links==
*  
*   analysis by Stefan Hedmark at The Movie Hamlet
*   at Meryl Streep informational web-site
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 