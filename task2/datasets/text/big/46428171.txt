Weekend Magic
{{Infobox film
| name = Weekend Magic
| image =
| image_size =
| caption =
| director = Rudolf Walther-Fein
| producer =Gabriel Levy
| writer =  Franz Rauch 
| starring =Harry Liedtke   Lissy Arna   Gustav Rickelt
| music = Felix Bartsch 
| cinematography = Edoardo Lamberti   Guido Seeber  
| editing =     
| studio = Aafa-Film 
| distributor = Aafa-Film 
| released =9 November 1927
| runtime = 
| country = Germany German intertitles
| budget =
| gross =
}}
Weekend Magic (German:Wochenendzauber) is a 1927 German silent romance film directed by Rudolf Walther-Fein and starring Harry Liedtke, Lissy Arna and Gustav Rickelt. 

==Cast==
*  Harry Liedtke as Heinz Sattorius - Neffe Frensens  
* Lissy Arna as Marcella Ferrari  
* Gustav Rickelt as Jonathan Frensen - Kapitän a.D.  
* Erich Kaiser-Titz as Justizrat Mahlau 
* Margarete Kupfer as Witwe Lehmann  
* Maria Paudler as Fritzi - ihre Tochter  
* Fritz Kampers as Wilhelm - ihr Sohn  
* Iwa Wanja as Annie Frenzel  
* Carl Geppert as Hinnings - Jonathans Diener  
* Olaf Storm as Ein junger Künstler 
* Frida Richard as Die Wäscherin 
* Sophie Pagay as Die Zimmervermieterin 
* Hermann Picha as Der Badedirektor 
* Heinrich Gotho as Der Eisverkäufer 
* Alfred Loretto as Der Wurstmaxe 
* Ita Rina 

== References ==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

== External links ==
*  

 
 
 
 
 
 
 

 