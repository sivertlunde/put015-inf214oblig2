Het geheim van het slot arco
 
{{Infobox film
| name           = Het geheim van het slot Arco
| image          = 
| image_size     = 
| caption        = 
| director       = Maurits Binger and Jan van Dommelen
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1915
| runtime        =  
| country        = Netherlands
| language       =Silent
| budget         = 
| amg_id         = 
}} 1915 Netherlands|Dutch silent drama film horror directed by Maurits Binger and Jan van Dommelen.

The English title is: The secret of castle Arco, the film consist of a Dutch cast while the production site was shot in Austria. The film is considered missing.

The storyline is about a man without a head that haunts the castle Arco in the 16th century. A man in the present day (1914) goes to the castle to find out if the story is true after which he finds something strange beneath the old castle floor.
 
==Cast==
* Jan van Dommelen
* Tilly Lus
* Willem van der Veer
* Constant van Kerckhoven jr
* Co Balfoort
* Louis van Dommelen
* Caroline van Dommelen

== External links ==
*  

 
 
 
 
 
 
 
 


 