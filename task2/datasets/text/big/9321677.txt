Alone with Her
{{Infobox film
| name           = Alone with Her
| image          = Alonewithher.jpg
| director       = Eric Nicholas
| producer       = Robert Engelman Tom Engelman
| writer         = Eric Nicholas
| starring       = Ana Claudia Talancón Colin Hanks Jordana Spiro
| music          = David E. Russo Paul Layton Nathan Wilson
| editing        = Cari Coughlin
| distributor    = IFC Films
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
}} stalker who uses spy cameras to force his way into the life of a young woman.    

The film tells the story through the lenses of spy cameras.  

==Plot==

The film starts with Doug (Colin Hanks) seeking out his next prey. He becomes obsessed with a woman named Amy (Ana Claudia Talancón), and sneaks numerous spy cameras in her apartment. By doing this, hes able to learn details of her life, and what he learns about things like her musical and film tastes he uses to seduce her.  

He meets her at a coffee shop a few times and strikes up conversations with her. The two become friends,  but it seems as though Amy isnt interested in continuing the relationship further. Using what he learns about her through the cameras, he gets her fired from her job by breaking into her car to stealing her computer and stops her burgeoning relationship with a co-worker. 

When he visits her apartment, Amys friend is suspicious of him, especially when she asks him about a section of Seattle—where he says hes from—that doesnt even exist. She becomes so suspicious of Doug that she begins asking around about him.

Doug, sensing that the truth about him might be uncovered, murders the roommate, and then comforts Amy as shes grieving. The two begin getting romantic one night and are about to have sex, but Doug cant get an erection. By the time he says hes able to perform, Amy tells him she cant because she doesnt want to wind up hurting his feelings.

Amy then discovers that Doug installed a spy camera in her bedroom when he makes a remark about her masturbating. After a struggle, Doug eventually kills Amy. The film ends with Doug seeking out his next prey.

==Soundtrack==
The film features music from A Cricket in Times Square and other rock bands as well as original music by David E. Russo.

== Reception ==
The film premiered at the Tribeca Film Festival in April 2006 and was given a limited theatrical release in the United States in January 2007.

As of October 2009, the film had a 69% rating at RottenTomatoes.com, with 29 critics chiming in.  With the consensus being "Alone With Her is a tense psychological thriller that overcomes its contrivances with fine performances and a perpetually unsettling mood."

Newsday movie critic Jan Stuart wrote, "Nicholas indie-film wrapping conceals a B-picture waiting to cut loose in its final 15 minutes, when it devolves into generic stalker-thriller theatrics of the Julia-Roberts-in-distress kind." 

But Entertainment Weekly film critic Lisa Schwarzbaum gave the film a positive review, calling it "a Blair Witch Project for the new, surveillance-obsessed millennium." 

== Alternate ending ==
An alternate ending, included with the DVD, shows Amy getting the upper hand over Doug in the final fight, and bashing him with a blunt object. As he lies on the bed, dazed, she prepares to bash him in the head just as the camera shorts out, and we hear a large thud. The scene changes to Amy sobbing in her living room after she has torn her home apart looking for hidden cameras.

==References==
 

==External links==
*  
* http://www.ew.com/ew/article/0,,20008790,00.html
* http://www.newsday.com/entertainment/movies/ny-etret5056529jan19,0,5716782.story?coll=ny-moviereview-headlines

 
 
 
 
 
 