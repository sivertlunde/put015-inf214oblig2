Honeymoon (1928 film)
 
{{infobox film
| name           = Honeymoon
| image          =
| imagesize      =
| caption        =
| director       = Robert A. Golden
| producer       = 
| writer         = Lew Lipton (story) Richard Schayer (adaptation) George OHara (writer) Robert E. Hopkins (titles)
| starring       = Polly Moran Harry Gribbon
| music          =
| cinematography = Max Fabian
| editing        = Ben Lewis
| distributor    = MGM
| released       =  
| runtime        = 60 minutes
| country        = US
| language       = Silent film English intertitles
}}
Honeymoon is a 1928 silent film comedy produced and distributed by MGM and directed by Robert A. Golden. It stars Polly Moran, Harry Gribbon and Bert Roach. 

It is a surviving film. 

==Cast==
*Polly Moran - Polly
*Harry Gribbon - Harry
*Bert Roach - Bert
*Flash the Dog - Flash
*Dick Sutherland - Uncredited Bit

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 

 