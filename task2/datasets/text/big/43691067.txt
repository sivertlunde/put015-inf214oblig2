The Loser Takes It All
 
{{Infobox film
|name=The Loser Takes It All film name=Ο χαμένος τα παίρνει όλα
|image=LoserTakesItAllGreekPoster.png
|caption=Theatrical release poster
|director=Nikos Nikolaidis
|producer=Nikos Nikolaidis
|writer=Nikos Nikolaidis
|starring=Giannis Aggelakas Simeon Nikolaidis Jenny Kitseli Ifigenia Asteriadi Louise Attah Ioanna Pappa Savva Loumi Melissa Stoili Maro Mavri Christos Houliaras George Houliaras Kostas Katsikis Dimitris Katsimanis Themis Katz Stefanos Lazarinos Grigoris Pimenidis Notis Pitsilos Takis Tsagaris Michele Valley
|music=Giannis Aggelakas Simeon Nikolaidis
|cinematography=Kostas Gikas
|editing=Giorgos Triandafyllou Greek Television ET-1 Greek Television ET-1 Prooptiki 2002 (Thessaloniki International Film Festival)
|runtime=120 Minutes Greece
|language=Greek Greek
|budget=Greek drachma|₯ 280,000,000
}} 2002 Cinema Greek dramatic experimental Independent independent Surrealist surrealist art Greek Television 2002 and its theatrical release began on 31 January 2003 in film|2003. The film received the Best Director Award and Kostas Gikas received the Best Cinematographer Award for it at the Thessaloniki International Film Festivals Greek State Film Awards in November 2002 in film|2002. 

==Plot==
The story revolves around five characters: a fortysomething, the "Man," who has memories from the "years of cholera;" an alcoholic woman, Odetti, who goes by the pseudonym of "Madame Raspberry;" a Senegalese striper named Mandali; the "Little Guy," a guitar player and music lover; and Elsa, the protagonists ex-girlfriend who is now a barwoman. The characters meet in order to fulfill their longstanding dream: leaving the city for an exotic island on a journey of no return. Thus they get involved with the dark world of the night.

==Crew==
*Director and Writer: Nikos Nikolaidis
*Producer: Nikos Nikolaidis
*Music and Soundtrack: Giannis Aggelakas and Simeon Nikolaidis
*Cinematographer: Kostas Gikas
*Editor: Giorgos Triandafyllou
*Set Decorator and Costume Designer: Marie-Louise Bartholomew
*Makeup Artist: Katerina Oikonomidoy
*Sound: Antonis Samaras
*Sound Re-Recording Mixer: Kostas Varybopiotis

==Cast==
*Giannis Aggelakas as Man
*Simeon Nikolaidis as Little Guy
*Jenny Kitseli as Odetti
*Ifigenia Asteriadi as Elsa
*Louise Attah as Mandali
*Ioanna Pappa as Melissa
*Savva Loumi as Blonde
*Melissa Stoili as Agni
*Maro Mavri as Television Woman
*Christos Houliaras as Cameraman
*George Houliaras as First Dealer
*Kostas Katsikis
*Dimitris Katsimanis as Short Guy
*Themis Katz as Second Dealer
*Stefanos Lazarinos as Third Dealer
*Grigoris Pimenidis as Detective
*Notis Pitsilos as Pawnbroker
*Takis Tsagaris as "El Dorado" Boss
*Michele Valley as Dead Mother

==Soundtrack==
The films soundtrack contains the following songs:

*"Its a Wonderful World" by Simeon Nikolaidis
*"Space in Time" by Simeon Nikolaidis
*"Into Town" by Simeon Nikolaidis

The soundtrack was released in 2003 by the company Hitch Hyke Records and includes twenty-eight pieces (mostly instrumental), including the song "The Loser Takes It All" (  Romanization of Greek|tr. "O chamenos ta pairnei ola") sung by Giannis Aggelakas. It was later reissued by Aggelakas record label All Together Now.

==References==
 

==Further reading==
*Nikos Nikolaidis|Νίκος Γεωργίου Νικολαΐδης, Ο χαμένος τα παίρνει όλα: Σενάριο, Αθήνα: Εκδόσεις Αιγόκερως, 2003, 143 σελίδες (ISBN 9603221821).  

==External links==
*  at      
* 
*  at the      
*  at the Greek Film Archive        
* 
* 
*  at The New York Times  
*  at      

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 