Bollywood Beats
 
 
 
{{Infobox film
| name = Bollywood Beats
| image= 
| caption = Theatrical release poster
| director = Mehul Shah
| producer = Mehul Shah Mansi Patel   JoseLuis Partida
| screenplay = Mehul Shah
| starring = Sachin Bhatt Lilette Dubey Pooja Kumar Sarita Joshi Mansi Patel Mehul Shah
| music = Indraneel Hariharan
| cinematography = Roger Lindley
| editing = Kuldeep Dhyani
| distributor = Breaking Glass Pictures Kinetik Films, LLC
| released =  
| runtime = 113 minutes
| country = United States
| language = English
}}
Bollywood Beats is a 2009 Bollywood-inspired dance film. It is written & directed by Mehul Shah, produced by Kinetik Films, & distributed by Breaking Glass Films.

==Plot==
Raj (Sachin Bhatt) is an Indian-American choreographer trying to make it in the bustling city of Los Angeles. After being dumped by his girlfriend, bombing another dance audition, and nearly getting kicked out of his parents home, Rajs luck changes when he meets Jyoti, (Lilette Dubey), a sexy Indian woman, who suggests for him to teach her and a group of Indian women dance.

While unsuccessful at the start, Rajs class grows with Vincent, (Mehul Shah), a gay teen who wants to dance regardless of what his father thinks, Laxmi, (Pooja Kumar), a South Indian woman new to this country and friendless, Puja, (Mansi Patel) an unethusiastic high school student, who is being dragged by her grandmother, Vina (Sarita Joshi).

Through it all, the group manages to find family, love, and acceptance where they least expected to.

==Cast==
* Sachin Bhatt as Raj
* Lilette Dubey as Jyoti
* Pooja Kumar as Laxmi
* Mansi Patel as Puja
* Mehul Shah as Vincent
* Namrata Nagraj as Payal
* Ramesh Chandiramani as Ramesh
* Aarti Chandiramani as Kirti
* Farah White as Elizabeth
* Nishendu Vasavada as Manoj
* Asif Taj as Ram

==Release==

===Release===
Bollywood Beats travelled internationally to various film festivals, including the Mumbai International Festival, the St. Louis International Film Festival, The Mahindra Indo-American Arts Council Film Festival, Tongues on Fire International Film Festival in London, AFI Film Festival in Dallas, and the Asian Festival of First Films in Singapore.

It is currently being distributed by Breaking Glass Pictures domestically and internationally. The film can be seen on here TV and Outtv in Canada

==Film Festivals==
2009 Mumbai International Film Festival-Official Selection  
2009 AFI Dallas-Official Selection 
540 Film Festival in Fayetville, AR-Official Selection 
2009 St. Louis International Film Festival-Official Selection 
2009 Mahindra Indo-American Film Council Film Festival, New York-Official Selection 
2010 Tongues on Fire Film Festival, London-Official Selection 
2010 Asian Festival of First Films, Singapore-Official Selection (Best Screenplay Nominee) 
2015 Whistler Pride Film Festival, Whistler, Canada 
 
  
 
  
 
 
 

==Soundtrack==
The soundtrack has music featuring the Indian hip hop group The Bilz ft. Kashif with the tracks "Turn the Music Up," and "Two-Step Bhangra." It also features tracks from London-based Punjabi rapper Hard Kaur, featuring the songs "Sexy Boy" and "Bombay Deewana."  The soundtrack also featured classic Bollywood Tunes such as "O Haseena Zulfowali", "Dum Maro Dum", "Jawani Janeman", "Say Na Say Na", and the title track "Dance all Night" by Indraneel Hariharan and Siddhant Bhatia.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 