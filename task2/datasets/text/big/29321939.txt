Under the Red Robe (1923 film)
{{Infobox film
| name           = Under the Red Robe
| image          = Under the Red Robe - 1923.jpg
| caption        = 1923 lobby poster showing Robert B. Mantell and John Charles Thomas
| director       = Alan Crosland
| producer       =
| based on       =  
| writer         = Bayard Veiller (scenario)
| starring       = Robert B. Mantell
| music          = William Frederick Peters
| cinematography = Gilbert Warrenton Harold Wenstrom
| editing        =
| studio         = Cosmopolitan Productions
| distributor    = Goldwyn Pictures
| released       =  
| runtime        = 10 reels (2,762 meters)
| country        = United States Silent (English intertitles)
}} silent historical Stanley Weyman Under the Red Robe. The film marks the last motion picture appearance by stage actor Robert B. Mantell who plays Cardinal Richelieu and the only silent screen performance of opera singer John Charles Thomas.  
 Under the Red Robe directed by Victor Seastrom. This latter version is in the public domain and available on InternetArchive.org

==Cast==
*Robert B. Mantell - Cardinal Richelieu
*Alma Rubens - Renee de Cocheforet
*Otto Kruger - Henri de Cocheforet
*John Charles Thomas - Gil De Berault
*William Powell - Duke of Orleans (billed William H. Powell)
*Ian Maclaren - King Louis XIII
*Genevieve Hamper - Duchess de Chevreuse
*Mary MacLaren - Anne of Austria
*Gustav von Seyffertitz - Clom
*Sidney Herbert - Father Joseph
*Arthur Housman - Captain La Rolle
*Paul Panzer - French Army Lieutenant
*Charles Judels - Antoine George Nash - Jules
*Evelyn Gosnell - Madame de Cocheforet

==Preservation==
The film survives complete at George Eastman House. A Goldwyn release it was donated by MGM 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 