Anametrisi
{{Infobox film
| name           = Anametrisi
| image          =
| caption        =
| director       = Giorgos Karypidis
| producer       = Giorgos Karypidis
| writer         = Giorgos Karypidis Vangelis Goufas
| starring       = Zoi Laskari Aris Retsos Stefanos Stratigos Spyros Fokas Giorgos Sabanis Nikolas Anagnostakis Thomas Halvatzis Panos Iliopoulos Paris Katsivelos Dina Konsta Marika Nezer Manos Tsilimidis Ioulia Vatikioti Filippos Vlachos
| cinematography =
| photographer   =
| music          = Haris Vrondos
| editing        =
| distributor    =
| released       =  
| runtime        = 110 minutes
| country        = Greece
| language       = Greek
}}
Anametri(s)i ( , Unmeasured, accented form: Anamétris(s)i, also under the name Epikindoni Pehnidi (Επικίνδυνο Παιχνίδι)) is a 1982 Greek mystery film directed by Giorgos Karypidis and starring Zoi Laskari, Aris Retsos, Stefanos Stratigos and Spyros Fokas.

==Cast==
*Zoi Laskari
*Aris Retsos
*Stefanos Stratigos
*Spyros Fokas
*Giorgos Sabanis
*Nikolas Anagnostakis
*Thomas Chalvatzis
*Panos Iliopoulos
*Paris Katsivelos
*Dina Konsta
*Marika Nezer
*Manos Tsilimidis
*Ioulia Vatikioti
*Filippos Vlachos

==Awards==
*EKKA Awards for Best Longest Film at the 1982 Thessaloniki Film Festival.


==External links==
*  
*    

 
 
 
 

 
 