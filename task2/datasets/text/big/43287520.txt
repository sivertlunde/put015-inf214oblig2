Our Miss Doctor
{{Infobox film
| name = Our Miss Doctor
| image =
| image_size =
| caption =
| director = Erich Engel
| producer = Eberhard Klagemann    
| writer = Fritz Schwiefert 
| narrator =
| starring = Jenny Jugo   Albert Matterstock   Heinz Salfner   Hans Schwarz Jr. 
| music = Hans-Otto Borgmann  
| cinematography = Massimo Terzano 
| editing =  Conrad von Molo 
| studio = Klagemann-Film 
| distributor = Various
| released = 20 December 1940  
| runtime = 94 minutes
| country = Nazi Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Our Miss Doctor (German:Unser Fräulein Doktor) is a 1940 German comedy film directed by  Erich Engel and starring Jenny Jugo, Albert Matterstock and Heinz Salfner.  A male teacher at a school slowly comes to appreciate one of his female colleagues both as a teacher and a woman.

==Cast==
*   Jenny Jugo as Dr. Elisabeth Hansen  
* Albert Matterstock as Dr. Karl Klinger  
* Heinz Salfner as Der Direktor 
* Hans Schwarz Jr. as Turnlehrer Jahnke   Hans Richter as Heinz Müller, Primaner  
* Gustav Waldau as Schuldiener Nießer  
* Josefine Dora as Frau Nießner 
* Hugo Werner-Kahle as Der Schulrat 
* Werner Pledath as Der Chefarzt der Klinik  
* Paul Bildt as Ein Universitätsprofessor 
* Gunnar Möller as Ernst Schultze, Sextaner 
* Rainer Penkert as Bierlinger, Primaner  
* Rudolf Reinhard as Hans Vogelsang, Sextaner  
* Horst Rossius as Fritz Bührle, Sextaner  
* Bruno Roth as Alfred Zimmermann, Primaner  
* Helmut Withrich as Wolfgang Schumann, Primaner  
* Karl Hannemann 
* Wolfgang Heise 
* John Pauls-Harding

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 