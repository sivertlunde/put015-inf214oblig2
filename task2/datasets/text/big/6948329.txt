Common Threads: Stories from the Quilt
{{Infobox Film
| name           = Common Threads: Stories from the Quilt
| image          = Common Threads - Stories from the Quilt (movie poster).jpg
| caption        = Poster Jeffrey Friedman
| producer       = Bill Couturié  Rob Epstein   Jeffrey Friedman  Sandy Gallin (executive producer)   Howard Rosenman (executive producer)
| writer         = Rob Epstein   Jeffrey Friedman   Cindy Ruskin
| starring       = Dustin Hoffman (narrator)
| music          = Bobby McFerrin
| cinematography = 
| editing        = Rob Epstein   Jeffrey Friedman
| distributor    = New Yorker Films
| released       = 1989
| runtime        = 
| country        = USA
| awards         =  English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Common Threads: Stories from the Quilt is a 1989 documentary film that tells the story of the NAMES Project AIDS Memorial Quilt. Narrated by Dustin Hoffman with a musical score written and performed by Bobby McFerrin, the film focuses on several people who are represented by panels in the Quilt, combining personal reminiscences with archive footage of the subjects, along with footage of various politicians, health professionals and other people with AIDS. Each section of the film is punctuated with statistics detailing the number of Americans diagnosed with and dead of AIDS through the early years of the epidemic. The film ends with the first display of the complete (to date) Quilt at the National Mall in Washington, D.C. during the 1987 Second National March on Washington for Lesbian and Gay Rights.

The film, made for HBO, was based in part on the book The Quilt: Stories From The NAMES Project by Cindy Ruskin (writer), Matt Herron (photographs) and Deborah Zemke (design).

The film relates the lives of five people memorialized with panels:
* Tom Waddell|Dr. Tom Waddell, founder of the Gay Games; his story is told by his friend and the mother of his child, Sara Lewinstein.
* David Mandell Jr., a young hemophiliac; his storytellers are his parents, David Mandell and Suzi Mandell.
* Robert Perryman, an African-American man who contracted the disease through intravenous drug use; his widow, Sallie Perryman, tells his story.
* Jeffrey Sevcik, a gay man; his story is told by his partner, film critic and historian Vito Russo, who himself succumbed to the disease in 1990, five years after he was diagnosed. 
* David C. Campbell, a United States Navy veteran; his storyteller is his lover, Tracy Torrey, who then became his own storyteller as well as he succumbed to the disease and was memorialized in the course of filming.
 Reagan administration KS poster ACT UP co-founder Larry Kramer.

==Awards== GLAAD Media Award for Best TV Documentary, and a Peabody Award.

==DVD release==
Common Threads was released on Region 1 DVD on June 8, 2004. 

==Soundtrack==
 
Bobby McFerrins 1990 studio album Medicine Music can easily be passed off as the soundtrack to the film, and it includes the films theme song, "Common Threads". The album reached number 146 on the Billboard 200 and number 2 on the Top Contemporary Jazz Albums chart.

==References==
 

==External links==
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 