Passages (film)
{{Infobox Film
| name           = Passages
| image          = 
| image size     = 
| caption        =  Yang Chao
| producer       = Yang Haijun
| writer         = Yang Chao
| narrator       = 
| starring       = Geng Le Chang Jieping
| music          = An Wei
| cinematography = Zhang Xigui 
| editing        = Xue Fangmin 
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = China
| language       = Mandarin
| budget         = 
}}
 Yang Chao. It was screened in the Un Certain Regard section at the 2004 Cannes Film Festival where it won the Caméra dOr Mention Spéciale prize.   

==Plot==
The film follows two wayward characters, Chen Sihan (Geng Le) and his girlfriend, Xiaoping (Chang Jieping). On the verge of entering university, both characters are looking for a way to make their fortune. The two travel to the city of Wuhan from their home in Anhui province, in the hope that they can purchase rare mushrooms to sell back home. When they return home, they discover they have been cheated and return to Wuhan in hopes of getting a refund. Returning unsuccessfully, Chen Sihan and Xiaoping are forced to decide whether to continue their journey or enter a school life.

==Cast==
* Geng Le
* Chang Jieping

==References==
 

==External links==
* 

 
 
 
 
 
 
 