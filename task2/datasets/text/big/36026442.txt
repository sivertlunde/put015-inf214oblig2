Between Evening and Morning
{{Infobox film
| name           = Between Evening and Morning
| image          = 
| image_size     = 
| caption        = 
| director       = Arthur Robison
| producer       = 
| writer         = Arthur Robison
| narrator       = 
| starring       = Werner Krauss   Agnes Straub   Elga Brink   Fritz Rasp
| music          = Willy Schmidt-Gentner
| editing        = 
| cinematography = Fritz Arno Wagner DMB
| distributor    = 
| released       = 1923
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    =  German silent film directed by Arthur Robison and starring Werner Krauss, Agnes Straub and Elga Brink.

==Cast==
* Werner Krauss   
* Agnes Straub   
* Elga Brink   
* Fritz Rasp 
* Alphons Fryland   
* Gertrude Welcker   
* Helmut Göze   
* Fred Selva-Goebel   
* Blandine Ebinger

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 

 
 
 
 
 


 