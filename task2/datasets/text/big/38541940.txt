Il cavaliere misterioso
{{Infobox film 
| name = Il cavaliere misterioso
| image = Il cavaliere misterioso.jpg
| caption =
| director = Riccardo Freda
| writer = Riccardo Freda Mario Monicelli Stefano Vanzina
| starring =
| music = Alessandro Cicognini
| cinematography =
| editing =
| producer = Dino De Laurentiis
| distributor =
| released = 1948
| runtime =
| awards =
| country =
| language = Italian
| budget =
}} Italian historical-adventure film directed by Riccardo Freda.    

In 2005 the film was restaured and shown as part of the retrospective "Casanova on the screen" at the 62nd Venice International Film Festival.  

== Cast ==
* , Cavalier of Seingalt
*María Mercader: Elisabetta 
*  
*Gianna Maria Canale: Countess Lehmann
*Alessandra Mamis: Countess Paola Itapieff
*Hans Hinrich: the Grand Inquisitor 
*Dante Maggio: Gennaro
*Guido Notari: Count Itapieff
*Vittorio Duse: Ivan 
* 
*Antonio Centa: Antonio Casanova, brother of Giacomo 
*Tino Buazzelli: Joseph

==References==
 

==External links==
* 

 
 

 
 
  
 
 
 
 