I Never Forget a Face
I Never Forget a Face is a 1956 documentary  short film produced by famed silent comedy enthusiast and filmmaker Robert Youngson.

== About the Film==
Robert Youngson wrote, directed, and produced over fifty comedy short and feature length film documentaries covering comedies from the silent era through the 1940s. Obituary New York Times, April 10, 1974.  Five of his short films, beginning with Blaze Busters in 1950, were nominated for Academy Awards (two won) with "I Never Forget a Face" being his last to receive a nomination.

The film consists of a series of classic silent clips with narration by well-known (at the time) radio announcer, Dwight Weist, and veteran film narrator Ward Wilson.

The film was nominated for the 1956 Academy Award for Best Live Action Short Film (one-reel sub-category) but did not win.  With the revamping of the category the following year, including the elimination of the one-reel and two-reel sub-categories, there was a clear emphasis on contemporary topics rather than retrospectives. Youngsons This Mechanical Age in 1954 was the last retrospective compilation to win and "I Never Forget a Face" in 1956 was the last to receive a nomination. Despite this, Youngson continued producing comedy retrospectives for the rest of his life. Obituary Variety Obituaries|Variety, April 17, 1974, page 95. 

==External links==
* 

==References==
 

 
 
 
 
 
 
 


 
 