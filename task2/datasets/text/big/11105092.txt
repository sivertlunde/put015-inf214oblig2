Vanina Vanini (film)
{{Infobox film
| name           = Vanina Vanini 
| image          = VaninaVanini.jpg
| image_size     =
| caption        = Film poster
| director       = Roberto Rossellini
| producer       = Moris Ergas
| writer         = Diego Fabbri Jean Gruault
| narrator       =
| starring       = Sandra Milo Renzo Rossellini
| cinematography = Luciano Trasatti
| editing        = Daniele Alabiso
| distributor    = Orsay Films Zebra Film Columbia Pictures(1979) (USA)
| released       =
| runtime        =  minutes
| country        = Italy
| language       = Italian
| budget         =
}} 1961 Italy|Italian of the same name.

==Plot summary==
Vanina Vanini, a bored, spoiled Roman countess, falls in love with a dedicated young patriot who is in Rome to assassinate a traitor to the brotherhood of the Free Masons.

==Cast==
*Sandra Milo ...  Vanina Vanini
*Laurent Terzieff ...  Pietro Missirilli
*Martine Carol ...  Contessa Vitelleschi
*Paolo Stoppa...  Asdrubale Vanini
*Isabelle Corey ...  Clelia
*Antonio Pierfederici ...  Livio Savelli
*Olimpia Cavalli ...  La femme de chambre
*Nerio Bernardi ...  Cardinal Savelli
*Mimmo Poli ...  Il boia
*Claudia Bava
*Leonardo Botta ...  Le confesseur
*Nando Cicero ...  Saverio Pontini
*Attilio Dottesio
*Carlo Gazzabini
*Enrico Glori
*Jean Gruault ...  Le castrat
*Evar Maran
*Leonardo Severini
*Nando Tamberlani

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 
 