The Undying Flame
{{Infobox film
| name           =  The Undying Flame
| image          =  Theundyingflame-newspaperad-1917.jpg
| imagesize      =
| caption        = Newspaper advertisement.
| director       = Maurice Tourneur
| producer       = Jesse L. Lasky
| writer         = Emma Bell Clifton (story) Charles E. Whittaker (scenario)
| starring       = Olga Petrova
| music          =
| cinematography = John van den Broek
| editing        =
| distributor    = Paramount Pictures
| released       = May 24, 1917
| runtime        = 5 reels
| country        = United States
| language       = Silent film (English intertitles)
}} lost 1917 silent film drama directed by Maurice Tourneur, produced by Jesse Lasky and released by Paramount Pictures. This movie starred Olga Petrova, an English-born actress who became popular in silents playing vamps. 

==Cast==
*Olga Petrova - The Princess / Grace Leslie
*Edwin Mordant - The King Herbert Evans - The Architect
*Mahlon Hamilton - The Shepherd / Captain Paget
*Warren Cook - General Leslie
*Charles Martin - Colonel Harvey
*Violet Reed - Mrs. Harvey

==See also==
*The House That Shadows Built (1931 promotional film by Paramount with excerpt of this film)

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 


 