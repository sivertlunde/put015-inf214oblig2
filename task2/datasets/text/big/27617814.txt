Jarrett (film)
 
{{Infobox film name       = Jarrett image      = caption    = director   = Barry Shear producer   = writer     = Richard Maibaum music      = cinematography = editing  John W. Holmes David Wages starring   = Glenn Ford Anthony Quayle
|distributor= released   =   runtime    = 73 minutes country    = United States language   = English
}}

Jarrett 1973 is a NBC TV film, starring Glenn Ford and directed by Barry Shear.

==Plot==
A private investigator specializing; Glenn Ford in fine arts tries to track down some missing rare biblical scrolls.

== Cast ==
*Glenn Ford as Sam Jarrett  
*Anthony Quayle as Cosmo Bastrop  
*Forrest Tucker as Rev. Simpson  
*Richard Anderson as Spencer Loomis 
*Yvonne Craig as Luluwa
*Elliott Montgomery as Dr. Carey  
*Laraine Stephens as Sigrid Larsen  
*Jody Gilbert as Sawyer

== References ==
A history of underground comics by Mark James Estren.
Movies made for television: the telefeature and the mini-series, 1964-1986 by Alvin H. Marill

== External links ==
*  

 

 
 
 


 