Being Julia
{{Infobox film
| name           = Being Julia
| image          = Being Julia movie.jpg
| caption        = Original poster
| director       = István Szabó
| producer       = Robert Lantos
| writer         = Ronald Harwood
| based on       =  
| starring       = Annette Bening Jeremy Irons Shaun Evans
| music          = Mychael Danna
| cinematography = Lajos Koltai
| editing        = Susan Shipton
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 104 minutes
| country        = Canada United States Hungary
| language       = English
| budget         = US$18 million
| gross          = US$14,339,171
}}
Being Julia is a 2004 comedy-drama film directed by István Szabó and starring Annette Bening and Jeremy Irons. The screenplay by Ronald Harwood is based on the novel Theatre (1937) by W. Somerset Maugham. The original film score was composed by Mychael Danna.

==Plot==
Set in London in 1938, the film focuses on highly successful and extremely popular theatre actress Julia Lambert (Annette Bening), whose gradual disillusionment with her career as she approaches middle age has prompted her to ask her husband, stage director Michael Gosselyn (Jeremy Irons), and his financial backer Dolly de Vries (Miriam Margolyes) to close her current production to allow her time to travel abroad. They convince her to remain with the play throughout the summer; and Michael introduces her to Tom Fennel (Shaun Evans), an enterprising American, who confesses his deep appreciation of her work. Seeking the passion missing from her marriage, and anxious to fill the void left when her close friend Lord Charles (Bruce Greenwood) suggested they part ways to avoid scandalous gossip, Julia embarks on a passionate affair with the young man and begins to support him so he may enjoy the glamorous lifestyle to which she has introduced him. Their relationship revives her, sparking a distinct change in her personality. Always hovering in the background and offering counsel is the spirit of her mentor, Jimmie Langton (Michael Gambon), the theatrical manager who gave Julia her start and made her a star, while flesh-and-blood Evie (Juliet Stevenson) serves as her personal maid, dresser, and confidante.

Michael suggests they invite Tom to spend time at their country estate, where he can become better acquainted with their son Roger (Tom Sturridge). At a party there, Tom meets aspiring actress Avice Crichton (Lucy Punch), and, when Julia sees him flirting with the pretty young woman, she becomes jealous and anxious and angrily confronts him. He slowly reveals himself to be a callous, social-climbing, gold-digging gigolo, and Julia is shattered when their affair comes to an end.
 ingenue be cast in her next play.

When Julias performance in her current play begins to suffer from her personal discontent, Michael closes the production; and Julia visits her mother (Rosemary Harris) and her Aunt Carrie (Rita Tushingham) in Jersey, where Lord Charles comes to visit her. Julia suggests a romantic tryst, and he gently tells her that hes gay. Meanwhile, back in London, Avice auditions for Michael; and, although Julia resents it, she is given the role.
 madness - Julia has planned her sweet revenge for the opening night performance, during which she successfully affirms her position as London theatres foremost diva.

==Production notes==
Filming began in June 2003. Exteriors were shot on location in London and Jersey. Interiors were filmed in Budapest, including inside the Danubius Hotel Astoria, and Kecskemét in Hungary.

The soundtrack features a number of popular songs of the era, including "They Didnt Believe Me" by Jerome Kern and Herbert Reynolds; "Life is Just a Bowl of Cherries" by Lew Brown and Ray Henderson; "Mad About the Boy" by Noël Coward; "I Get a Kick Out of You" by Cole Porter; "Shes My Lovely" by Vivian Ellis; "Bei Mir Bist Du Schon" by Sholom Secunda, Jacob Jacobs, Sammy Cahn, and Saul Chaplin; and "Smoke Gets in Your Eyes" by Otto A. Harbach and Jerome Kern.

The film premiered at the Telluride Film Festival and was shown at the Toronto Film Festival, the San Sebastián Film Festival, the Vancouver International Film Festival, the Calgary Film Festival, and the Chicago International Film Festival before opening in the US in limited release.

The film grossed $7,739,049 in the US and $6,600,122 in foreign markets for a total worldwide box office of $14,339,171. 

==Cast==
* Annette Bening as Julia Lambert
* Jeremy Irons as Michael Gosselyn
* Shaun Evans as Tom Fennel
* Lucy Punch as Avice Crichton
* Juliet Stevenson as Evie
* Miriam Margolyes as Dolly de Vries
* Tom Sturridge as Roger Gosselyn
* Bruce Greenwood as Lord Charles
* Rosemary Harris as Julias Mother
* Rita Tushingham as Aunt Carrie
* Michael Gambon as Jimmie Langton

==Critical reception==
Being Julia has received fairly positive reviews, with Bening receiving acclaim for her performances, and an average score of 65/100 at the review aggregator Metacritic. 

In his review in the New York Times, A.O. Scott called the film "a flimsy frame surrounding a brightly colored performance by Annette Bening, whose quick, high-spirited charm is on marvelous display . . . She gives Being Julia a giddy, reckless effervescence that neither Mr. Szabos stolid direction nor Ronald Harwoods lurching script . . . are quite able to match . . . Ms. Bening walks right up to the edge of melodramatic bathos (the hallmark of the kind of plays in which Julia stars) and then, in a wonderful climactic coup de théâtre, turns it all into farce. Being Julia may not make much psychological or dramatic sense, but Ms. Bening, pretending to be Julia (who is always pretending to be herself), is sensational." 

Roger Ebert of the Chicago Sun-Times said, "Annette Bening plays Julia in a performance that has great verve and energy, and just as well, because the basic material is wheezy melodrama. All About Eve breathed new life into it all those years ago, but now its gasping again . . . I liked the movie in its own way, while it was cheerfully chugging along, but the ending let me down; the materials are past their sell-by date and were when Maugham first retailed them. The pleasures are in the actual presence of the actors, Bening most of all, and the droll Irons, and Juliet Stevenson as the practical aide-de-camp, and Thomas Sturridge, so good as Julias son that I wonder why he wasnt given the role of her young lover." 

In the San Francisco Chronicle, Carla Meyer described the film as "a one-woman show" and added, "There are several notable actors in it, most of them quite good, but its the glorious Annette Bening who hoists this flawed production on her mink-wrapped shoulders and makes it work . . . Her stage background at American Conservatory Theater shows in her multilayered tour de force." 
 Viennese strain of wise and winkingly cynical romantic comedy perfected by those two masters of the sexual charade and nearly everything to do with the world of pre-war London theater. This is a film that, above all else, needed to be steeped in Britishness, in the very particular mores and manners of the time; as a Canadian production mostly shot in Budapest by a Hungarian director and an American star and a number of Canuck thesps, this just doesnt happen. The deficiencies may be intangible, but they deprive film of the solid footing it requires . . . The majority of the seriocomic doings, while superficially diverting, provide neither indelible wit nor the gravitas of a genuinely meaningful comedy of manners (see Oscar Wilde), leaving a relatively wispy impression in its wake." 
 American Beauty The Grifters - but she works too hard to prove it in Being Julia . . . Director Istvan Szabo overplays his hand and traps   in a role thats all emoting, no emotion." 
 Oscar nomination . . . Chewing up the scenery in lipsmacking form, she savours the ribald dialogue like an overripe wine, spitting venom and self-pity in equally bilious measures, lending much needed weight to this contrived fluff." 

==Awards and nominations==
* Academy Award for Best Actress (Annette Bening, nominee)
* Golden Globe Award for Best Actress - Motion Picture Musical or Comedy (Bening, winner)
* Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Leading Role - Motion Picture (Bening, nominee)
* Satellite Award for Best Actress - Motion Picture Musical or Comedy (Bening, winner)
* Satellite Award for Best Supporting Actor - Motion Picture Musical or Comedy (Jeremy Irons, nominee)
* Genie Award for Best Motion Picture (nominee)
* Genie Award for Best Performance by an Actor in a Supporting Role (Bruce Greenwood, nominee)
* National Board of Review Award for Best Actress (Bening, winner) London Film Critics Circle Award for Best Actress (Bening, nominee) Bangkok International Film Festival Golden Kinnaree Award for Best Actress (Bening, winner; tied with Ana Geislerova for Zelary)
* Bangkok International Film Festival Golden Kinnaree Award for Best Film (nominee) European Film Award for Best Director (nominee)
* European Film Award for Best Cinematography (nominee) Southeastern Film Critics Association Award for Best Actress (Bening, winner)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 