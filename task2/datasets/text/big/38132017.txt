Suster Keramas
{{Infobox film
| movie_name     = Suster Keramas
| image          =
| director       = Helfi Kardit
| producer       = Ody Mulya Hidayat
| eproducer      =
| aproducer      =
| writer         = Abbe Ac
| starring       = Herfiza Novianti Rin Sakuragi Rizky Mocil Zidni Adam Shinta Bachir
| music          = Joseph S. Djafar
| cinematography = Nofi SY Kardit
| editing        = Aziz Natandra
| distributor    = Maxima Pictures
| released       =  
| runtime        = 82 minutes
| country        = Indonesia
| awards         = Indonesia
| budget         =
| gross          = 
}}
Suster Keramas is an adult horror comedy movie from Indonesia produced by Maxima Pictures and distributed by Maleo Pictures. The film was directed by Helfi Kardit and starred Herfiza Novianti as Kayla, Shinta Bachir as Jeng Dollie and Rin Sakuragi as Mitchiko.

The film is about a Japanese tourist (Rin Sakuragi) looking for her sister who works as a nurse in Indonesia. Ironically, her sister already died. Meanwhile, three friends, Kayla, Barry, and Ariel, are disturbed by the ghost of a nurse. 

Suster Keramas was released in Indonesia on 31 December 2009 and sold more than 800,000 tickets,  compared to other local films at the time that were unlikely to sell even 300,000 tickets. {{cite web
 | last = Siahaan
 | first = Armando
 | authorlink = 
 | coauthors = 
 | year = 
 | url = http://www.thejakartaglobe.com/artsandentertainment/sex-and-gore-sell-at-the-indonesian-box-office/359416
 | title = Sex and Gore Sell at the Indonesian Box Office
 | format = 
 | work = 
 | publisher = Jakarta Globe
 | quote =
 | language = }}  However, the film was controversial in Indonesia because of its sexual aspects. 

==Production==
Filming was done in Puncak, Bogor. 

==References==
 

 
 


 
 