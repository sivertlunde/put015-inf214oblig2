No Regrets for Our Youth
{{Multiple issues|
 
 
 
}}
{{Infobox film
| name           = No Regrets for Our Youth
| image          =
| caption        = Japanese movie poster
| director       = Akira Kurosawa
| producer       = Keiji Matsuzaki
| writer         = Eijirō Hisaita Akira Kurosawa Keiji Matsuzaki
| starring       = Setsuko Hara Susumu Fujita Denjirō Ōkōchi
| cinematography = Asakazu Nakai
| editing        = Akira Kurosawa Toho Studios
| distributor    = Toho Company Ltd.
| released       =  
| runtime        = 110 minutes
| country        = Japan
| language       = Japanese
| budget         =
| music          = Tadashi Hattori
}}
  is a 1946 film written and directed by Akira Kurosawa. It is based on the Takigawa incident of 1933.

The film stars Setsuko Hara, Susumu Fujita, Takashi Shimura and Denjirō Ōkōchi. Fujitas character was inspired by the real-life Hotsumi Ozaki, who assisted the famous Soviet spy Richard Sorge and so became the only Japanese citizen to suffer the death penalty for treason during World War Two.
The film is in black-and-white and runs 110 minutes.
 

==Plot==
The film begins in 1933. Students at Kyoto Imperial University  protest against the Japanese invasion of Manchuria. Prominent professor Yagihara (Denjiro Okochi) is relieved of his post because of his leftist views against fascism. The professors daughter Yukie (Setsuko Hara) is courted by two of her fathers students: Ryukichi Noge (Susumu Fujita) and Itokawa (Akitake Kôno). Itokawa is safe and sensible while Noge is fiery. Yukie is eventually drawn toward Noge.

Noge disappears following an anti-militarist student protest. His disappearance is the result of being arrested and he spends four years in jail. By the time Itokawa (now a prosecutor for the government) tells Yukie about Noges whereabouts he has already been out of jail for a year. He also tells her that he is a changed man, that he is no longer how Yukie remembered him.

Itokawa brings Noge over to the Yagihara residence. During dinner, Professor Yagihara mentions that Noge wouldnt have gotten out unless the government was convinced that Noge had "converted" from his radical ways. Noge confirms this and says that Itokawa vouched for him and had even found him a job in the army.

After realizing that Noge has changed from his days at the University, Yukie gets up from the dinner table and runs to lock herself in her room. Yukies mother eventually tells her that Itokawa and Noge are leaving. At first Yukie is reluctant to see them out, however once Yukies mother tells her that Noge is leaving for China she decides to see Noge one last time to say goodbye.

After Noges departure, Yukie begins to pack for Tokyo and after a conversation her father reluctantly lets her go. For three years in Tokyo, Yukie works menial jobs to get by. One day she runs into Itokawa and is told that Noge is in Tokyo. She goes to Noges offices, but is scared of what will happen. Yukie is shown outside of the offices several times but eventually Noge notices her. They spend several years together and get married during this time.

Yukie knows that Noge is involved in illegal activities , but he refuses to tell her what they are. Noge is arrested on the night before his plans were to go into effect. Yukie is interrogated, but she proffers no information. Yukie is treated badly during the interrogations but Itokawa is eventually able to free her. Yukies parents take the train into Tokyo where Yukies father meets up with Itokawa. Yukies father thanks him for what he has done and informs Itokawa that he intends to represent Noge in court. Itokawa mournfully responds that Noge died the night before. Yukie is crushed. She brings his ashes to his parents, and tells them she is his wife. They reject her, believing that she has come to mock them because their son was convicted of being a spy. She stays with his parents, who are scorned and harassed in their village, and works the rice fields with them. She tries to convince them of her sincerity and that their son was a good man. The work in the rice fields is hard on her, but she is determined to prove her mettle, even to the point of working when she has a severe fever.

The night that they finally finish planting all of the fields, the neighbors sneak in and destroy their rice fields. When Yukie mourns the vandalism, Noges parents finally accept her and their son is redeemed in their eyes. At the end of the war, Professor Yagihara is reinstated and Noge is honored for his anti-war efforts. Yukie returns to Kyoto to visit her parents. Yukies mother tells her that she can stay since she has achieved her goal because Noges parents are no longer ashamed of him. However, Yukie now feels more comfortable planting rice than playing the piano, so she goes back to work on the farm.

== Cast ==
*Setsuko Hara - Yukie Yagihara
*Susumu Fujita - Ruykichi Noge
*Denjirō Ōkōchi - Professor Yagihara
*Haruko Sugimura - Madame Noge
*Eiko Miyoshi - Madame Yagihara
*Kokuten Kōdō - Mr. Noge 
*Akitake Kōno - Itokawa
*Takashi Shimura - Police Commissioner Poison Strawberry Dokui
*Taizō Fukami - Minister of Education
*Masao Shimizu - Professor Hakozaki
*Haruo Tanaka - Student
*Kazu Hikari - Detective
*Hisako Hara - Itokawas Mother
*Shin Takemura - Prosecutor

==External links==
 
* 
* 
*  at Rotten Tomatoes
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 
 
 
 