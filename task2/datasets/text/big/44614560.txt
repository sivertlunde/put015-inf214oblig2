Hum Dum
{{Infobox film
| name           = Hum Dum
| image          = Hum Dum poster.jpg
| alt            =
| caption        = Theatrical release poster
| film name      = 
| director       = Kushan Nandy
| producer       = Kushan Nandy Kiran Shroff
| writer         = Galib Assad Bhopali
| screenplay     = Kiran Shroff
| story          = Kiran Shroff
| based on       =  See below
| narrator       = 
| music          = Sujeet–Rajesh
| cinematography = Ashok Behl
| editing        = Ashmith Kunder
| studio         = Sarvodaya Visuals
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          =  
}} drama film directed by Kushan Nandy and produced by Kushan Nandy and Kiran Shroff under the banner of Sarvodaya Visuals. It features actors Romit Raj and Anjana Sukhani in the lead roles. Sujeet–Rajesh scored the music for the film.

== Cast ==
 
* Anjana Sukhani as Rrutu Joshi
* Romit Raj as Siddhant Dey
* Tanvi Azmi
* Benjamin Gilani
* Ranvir Shorey Shammi
* Prithvi Zutshi
* Jaidev Hattangadi
* Om Katare
 

== Music ==
{{Infobox album  
| Name       = Hum Dum
| Type       = soundtrack
| Artist     = Sujeet–Rajesh
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Das Music
| Producer   = 
| Last album = 
| This album = Hum Dum (2005)
| Next album = 
}}
The soundtrack of the film has been given by Sujeet–Rajesh. Lyrics have been penned by Surendra Mishra and Shaheen Iqbal.  

{{Track listing
| collapsed       = 
| headline        = Hum Dum (Original Motion Picture Soundtrack)
| extra_column    = Artist(s)
| total_length    = 34:10
| all_music       = Sujeet–Rajesh
| all_lyrics      = Surendra Mishra, Shaheen Iqbal
| title1          = Humdum
| extra1          = Shreya Ghoshal, Sonu Nigam
| length1         = 4:51
| title2          = Tanhaiya
| note2           = Female Version
| extra2          = Shreya Ghoshal
| length2         = 5:34
| title3          = Lahaul Vila Shaan
| length3         = 4:57
| title4          = Ab Hum Hain
| extra4          = Sujeet Shetty
| length4         = 1:51
| title5          = Sitaron Pe
| extra5          = Shaan
| length5         = 5:49
| title6          = Hanste Raho
| extra6          = Madhushree, Abhijeet Bhattacharya
| length6         = 5:32
| title7          = Tanhaiya
| note7           = Male Version
| extra7          = Sonu Nigam
| length7         = 5:36
}}

== References ==
 

== External links ==
*  
*  

 
 
 


 