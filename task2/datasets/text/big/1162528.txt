Closer (2004 film)
{{Infobox film
| name           = Closer
| image          = Closer movie poster.jpg
| caption        = Theatrical release poster
| director       = Mike Nichols
| producer       = Mike Nichols Cary Brokaw John Calley
| writer         = Patrick Marber
| based on       =  
| starring       = Julia Roberts Jude Law Natalie Portman Clive Owen
| music          = Suzana Peric
| cinematography = Stephen Goldblatt John Bloom Antonia Van Drimmelen
| distributor    = Columbia Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $27 million   
| gross          = $115,505,027
}} 1997 play of the same name. The movie was produced and directed by Mike Nichols and stars Julia Roberts, Jude Law, Natalie Portman and Clive Owen. The film, like the play on which it is based, has been seen by some as a modern and tragic version of Wolfgang Amadeus Mozart|Mozarts opera Così fan tutte, with references to that opera in both the plot and the soundtrack.  Owen starred in the play as Dan, the role assumed by Law in the film.
 awards and Oscar nominations Golden Globe wins for both Portman and Owen for their performances in supporting roles.

==Plot==

In the opening scene, twenty-four-year-old Alice Ayres (Natalie Portman) and Dan Woolf (Jude Law) see each other for the first time from opposite sides of a street as they are walking toward each other among many other rush hour pedestrians. Alice is a young American stripper who just arrived in London, and Dan is an unsuccessful British author who is on his way to work where he writes obituaries for a newspaper. Alice looks in the wrong direction as she crosses the street and is hit by a taxi cab right in front of Dans eyes. After he rushes to her side she smiles to him and says, "Hello, stranger." He takes her to the hospital where Alice is treated and released. Afterward, on the way to his office, they stop by Postmans Park, the same park that he and his father visited after his mothers death. Pausing in front of the office before he leaves her and goes to work, he reminds her that traffic in England tends to come on from the right, and on impulse, he asks her for her name. They soon become lovers.

A year later, though the two are in a relationship, Dan is straying. He has written a novel based on Alices life and while being photographed to publicize it, he flirts with the American photographer Anna Cameron (Julia Roberts). Anna shares a kiss with Dan before finding out that Dan and Alice are in a relationship. Alice arrives and borrows Annas bathroom, leaving Anna and Dan alone again. Dan takes the chance to try to persuade Anna into having an affair with him but is cut short by Alices return. Alice asks Anna if she can have her portrait taken as well. Anna agrees and Alice asks Dan to leave them alone during the photo shoot. While being photographed, she reveals to Anna that she overheard them, and she is photographed while still weeping over it. Alice does not reveal what she overheard to Dan, even as he spends a year stalking Anna.

Another year later, Dan enters a cybersex chat room and randomly meets Larry Gray (Clive Owen), a British dermatologist. With Anna still on his mind, Dan pretends to be her, and using the pretense that they will be having sex, Dan convinces Larry to meet at the aquarium (where Anna told Dan she often went). Larry goes to the rendezvous and has his rude awakening there. Anna tells Larry that a man who had pursued her, Dan, was most likely to blame for the setup. Soon, Anna and Larry become a couple and they refer to Dan as "Cupid" from then on.

Four months later, at Annas photo exhibition,  Larry meets Alice, whom he recognizes from the tearful photograph that is one of many being exhibited. Larry knows that Alice and Dan are a couple, from talking to Anna. Meanwhile, Dan convinces Anna to become involved with him. They begin cheating on their respective lovers for a year, even though Anna and Larry become married halfway through the year. Eventually Anna and Dan each confess the affair to their respective partners, leaving their relationships for one another.

Alice goes back to being a stripper, heartbroken by her loss. One day, Larry runs into her accidentally at the strip club and he (heart-broken himself) is convinced that she is the girl he met before. He asks her if her name is Alice, but no matter how much money he gives her, she keeps telling him her name is "Jane Jones." He asks her to have a one-night stand with him but she refuses. The line of questioning becomes pornographic, albeit without any explicit nudity.

Eventually, Larry convinces Anna to see him one last time; she agrees to sleep with him so that he will sign the divorce papers and leave her alone. Dan guesses and Anna confesses it to Dan, who takes it badly. Anna returns to Larry. Distraught, Dan confronts Larry to try and get Anna back. Instead, Larry tells him Alices whereabouts, and suggests that he go back to her. Suddenly, however, out of a sheer malicious impulse, he also tells him that he had a one-night stand with her.

Alice takes Dan back. When Dan asks her whether she had a one-night stand with Larry, she initially denies it. But when he insists on the truth, she suddenly tells him that she doesnt love him anymore and goes on to say that she did sleep with Larry. Dan then reveals that Larry had already told him about the one-night stand but that hes already forgiven her. She insists that its over and tells him to leave. 

  tile in Postmans Park, London]]
In the end, Alice returns to New York. Passing through the immigration checkpoint on her way back into the United States, it is revealed through a shot of her passport that her real name is indeed Jane Rachel Jones and that she had lied about her name for the duration of her four-year relationship with Dan.

Back in London, Dan returns to Postmans Park, and to his surprise, notices the name "Alice Ayres" on a tile that is dedicated to a girl, "who by intrepid conduct", and at the cost of her young life, rescued three children from a fire. The final scene shows Alice/Jane walking on Broadway towards West 47th Street with male passers-by staring at her beauty. This completes the visual symmetry within the film as it echoes the opening scene where Alice/Jane and Dan are staring at each other on the sidewalks of London.

==Cast==
* Julia Roberts as Anna Cameron
* Jude Law as Dan Woolf
* Natalie Portman as Alice Ayres / Jane Jones
* Clive Owen as Larry Gray
* Nick Hobbs as Taxi Driver
* Colin Stinton as Customs Officer

==Production==

===Filming=== Elstree Film and Television Studios and on location in London.

===Music===
The main theme of the film follows Wolfgang Amadeus Mozart|Mozarts opera Così fan tutte, with references to that opera in both the plot and the soundtrack.  The soundtrack also contains songs from Jem (singer)|Jem, Damien Rice and Lisa Hannigan, Bebel Gilberto, The Devlins, The Prodigy and The Smiths.

The music of Irish folk singer Damien Rice is featured in the film, most notably the song "The Blowers Daughter," whose lyrics drew many parallels with the themes present in the film. The opening notes from Rices song "Cold Water" are also used repeatedly, notably in the memorial park scenes. Rice wrote a song titled "Closer" which was intended for use in the film but was not completed in time. 

==Reception==

===Critical reaction===
The film received generally positive reviews from critics, with Owens performance receiving universal praise. The review summary site Rotten Tomatoes shows 68% positive ratings among 203 reviews.  Another review aggregator, Metacritic shows a weighted average score of 65 out of 100, based on 42 reviews.  Roger Ebert, writing for the Chicago Sun-Times, said of the people involved with the film, " hey are all so very articulate, which is refreshing in a time when literate and evocative speech has been devalued in the movies." Peter Travers, writing for Rolling Stone, said, "Mike Nichols’ haunting, hypnotic Closer vibrates with eroticism, bruising laughs and dynamite performances from four attractive actors doing decidedly unattractive things." Kenneth Turan of the Los Angeles Times wrote, " espite involved acting and Nichols impeccable professionalism as a director, the end result is, to quote one of the characters, a bunch of sad strangers photographed beautifully." The New York Times’ A.O. Scott wrote, " nlike most movie love stories, Closer does have the virtue of unpredictability. The problem is that, while parts are provocative and forceful, the film as a whole collapses into a welter of misplaced intensity."

===Box office===
The film was released on December 3, 2004 in North America. Closer opened in 476 theaters, but the theater count was increased after the film was released. The film was domestically a moderate financial success, grossing $33,987,757.  Huge success followed in the international market, where the film grossed an additional $81,517,270, accounting for over 70% of its worldwide gross, which turned to $115,505,027. The film was produced on a budget of United States dollar|US$27 million. 

===Awards and nominations===  
The film won the following awards:
{| class="wikitable"
|-
!Year
!Award
!Category – Winner(s)
|- 2005
|BAFTA Awards Best Performance by an Actor in a Supporting Role – Clive Owen
|- 2005
|rowspan="2"|Golden Globes Best Performance by an Actor in a Supporting Role in a Motion Picture – Clive Owen
|- Best Performance by an Actress in a Supporting Role in a Motion Picture – Natalie Portman
|- 2005
|Las Vegas Film Critics Society Best Supporting Actor – Clive Owen
|- 2004
|National National Board of Review Best Acting by an Ensemble – Jude Law, Clive Owen, Natalie Portman and Julia Roberts
|- 2004
|New New York Film Critics Circle Best Supporting Actor – Clive Owen
|- 2004
|San San Diego Film Critics Society Best Supporting Actress – Natalie Portman
|- 2004
|Toronto Toronto Film Critics Association Best Supporting Actor, Male – Clive Owen
|}

The film was nominated for the following awards:
{| class="wikitable"
|-
!Year
!Award
!Category – Nominee(s)
|- 2005
|rowspan="2"|Academy Awards Best Performance by an Actor in a Supporting Role in a Motion Picture – Clive Owen
|- Best Performance by an Actress in a Supporting Role in a Motion Picture – Natalie Portman
|- 2005
|The American Screenwriters Association Discover Screenwriting Award – Patrick Marber
|- 2005
|rowspan="2"|BAFTA Awards Best Screenplay – Adapted – Patrick Marber
|- Best Performance by an Actress in a Supporting Role – Natalie Portman
|- 2005
|rowspan="3"|Broadcast Film Critics Association Best Acting Ensemble – Jude Law, Clive Owen, Natalie Portman, and Julia Roberts
|- Best Supporting Actor – Clive Owen
|- Best Supporting Actress – Natalie Portman
|- 2005
|rowspan="3"|Golden Globes Best Director – Motion Picture – Mike Nichols
|- Best Motion Picture – Drama
|- Best Screenplay – Motion Picture – Patrick Marber
|- 2005
|rowspan="3"|Online Film Critics Society Best Screenplay, Adapted – Patrick Marber
|- Best Supporting Actor – Clive Owen
|- Best Supporting Actress – Natalie Portman
|- 2005
|rowspan="4"|Golden Satellite Award Best Actor in a Supporting Role, Drama – Clive Owen
|- Best Actress in a Supporting Role, Drama – Natalie Portman
|- Best Film John Bloom and Antonia Van Drimmelen
|- Best Screenplay, Adapted – Patrick Marber
|- 2005
|Teen Choice Awards Choice Movie Actress: Drama – Natalie Portman
|}

==Home media==
Closer was released on DVD in 2005 and Blu-ray Disc|Blu-ray on May 22, 2007.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 