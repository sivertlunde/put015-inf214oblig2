Panchamrutham
{{Infobox film 
| name           = Panchamrutham
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = EK Thyagarajan
| writer         = J. Sasikumar S. L. Puram Sadanandan (dialogues)
| screenplay     =
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Manavalan Joseph
| music          = G. Devarajan
| cinematography = CJ Mohan
| editing        = K Sankunni
| studio         = Sree Murugalaya Films
| distributor    = Sree Murugalaya Films
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by EK Thyagarajan. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Manavalan Joseph in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir
*Jayabharathi
*Adoor Bhasi
*Manavalan Joseph
*Sankaradi
*Sreelatha Namboothiri
*Bahadoor
*MG Soman Meena
*Surasu
*Vidhubala
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aakaashathile || P Jayachandran, P. Madhuri || Sreekumaran Thampi || 
|-
| 2 || Ee Jeevithamenikku || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Hridayeshwari Nin || P Jayachandran || Sreekumaran Thampi || 
|-
| 4 || Kaattililakum || K. J. Yesudas, P Susheela || Sreekumaran Thampi || 
|-
| 5 || Sathyamennum Kurisil || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 