One Flew Over the Cuckoo's Nest (film)
{{Infobox film
| name           = One Flew Over the Cuckoos Nest
| image          = One Flew Over the Cuckoos Nest poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Miloš Forman
| producer       = Saul Zaentz Michael Douglas
| screenplay     = Lawrence Hauben Bo Goldman
| based on       =  
| starring       = Jack Nicholson Louise Fletcher
| music          = Jack Nitzsche Bill Butler   
| editing        = Richard Chew  Sheldon Kahn Lynzee Klingman
| distributor    = United Artists
| released       =  
| runtime        = 133 minutes
| country        = United States
| language       = English
| budget         = $3 million   
| gross          = $109 million 
}} One Flew William Redfield, Brad Dourif, Danny DeVito,  Christopher Lloyd, and Scatman Crothers.
 win all Best Picture, Actor in Actress in The Silence of the Lambs.
 100 Years... 100 Movies list. It was shot at Oregon State Hospital in Salem, Oregon, which was also the setting of the novel. 

==Plot== Randle Patrick recidivist anti-authoritarian hard labor and serve the rest of his sentence in a more relaxed hospital environment.
 Nurse Mildred William Redfield), Native American of imposing stature believed to be deaf and mute.

McMurphys and Ratcheds battle of wills escalates rapidly. When McMurphys card games win away everyones cigarettes, Ratched confiscates the cigarettes and rations them out. McMurphy calls for votes on ward policy changes to challenge her. He makes a show of betting the other patients he can escape by lifting an old hydrotherapy console—a massive marble plumbing fixture—off the floor and sending it through the window; when he fails to do so, he turns to them and says, "But I tried goddammit. At least I did that."

McMurphy steals a hospital bus, herds his colleagues aboard, stops to pick up Candy (Marya Small), a party girl, and takes the group deep sea fishing on a commandeered boat. He tells them: "Youre not nuts, youre fishermen!" and they begin to feel faint stirrings of self-determination.

Soon after, however, McMurphy learns that Ratched and the doctors have the power to keep him committed indefinitely. Sensing a rising tide of insurrection among the group, Ratched tightens her grip on everyone. During one of her group therapy sessions, Cheswicks agitation boils over and he, McMurphy and the Chief wind up brawling with the orderlies. They are sent up to the "shock shop" for electroconvulsive therapy. While McMurphy and the Chief wait their turn, McMurphy offers Chief a piece of gum, and Chief murmurs "Thank you...Ah, Juicy Fruit." McMurphy is delighted to find that Bromden is neither deaf nor mute, and that he stays silent to deflect attention. After the electroshock therapy, McMurphy shuffles back onto the ward feigning brain damage, before humorously animating his face and loudly greeting his fellow patients, assuring everyone that the ECT only charged him up all the more and that the next woman to take him on will "light up like a pinball machine and pay off in silver dollars."

As the struggle with Ratched takes its toll, and with his release date no longer a certainty, McMurphy plans an escape. He phones Candy to bring her friend Rose (Louisa Moritz) and some booze to the hospital late one night. They enter through a window after McMurphy bribes the night orderly, Mr. Turkle (Scatman Crothers). McMurphy and Candy invite the patients into the day room for a Christmas party; the group breaks into the drug locker, puts on music, and enjoys a bacchanalian rampage. At the end of the night, McMurphy and Bromden prepare to climb out the window with the girls. McMurphy says goodbye to everyone, and invites an emotional Billy to escape with them; he declines, saying he is not yet ready to leave the hospital—though he would like to date Candy in the future. McMurphy insists Billy have sex with Candy right then and there. Billy and Candy agree and they retire to a private room. The effects of the alcohol and pilfered medication take their toll on everyone, including McMurphy and the Chief, whose eyes slowly close in fatigue.

Ratched arrives the following morning and discovers the scene: the ward completely upended and patients passed out all over the floor. She orders the attendants to lock the window, clean up, and conduct a head count. When they find Billy and Candy, the other patients applaud and, buoyed, Billy speaks for the first time without a stutter. Ratched then announces that she will tell Billys mother what he has done. Billy panics, his stutter returns, he starts punching himself and then locks himself in the doctors office. Locked inside, Billy kills himself. McMurphy, enraged at Ratched, chokes her nearly to death until orderly Washington knocks him out.

Some time later, the patients in the ward play cards and gamble for cigarettes as before, only now with Harding dealing and delivering a pale imitation of McMurphys patter. Ratched, still recovering from the neck injury sustained during McMurphys attack, wears a neck brace and speaks in a thin, reedy voice. The patients pass a whispered rumor that McMurphy dramatically escaped the hospital rather than being taken "upstairs".

Late that night, Bromden sees McMurphy being escorted back to his bed, and initially believes that he has returned so they can escape together, which he is now ready to do since McMurphy has made him feel "as big as a mountain". However, when he looks closely at McMurphys unresponsive face, he is horrified to see lobotomy scars on his forehead. Unwilling to allow McMurphy to live in such a state, the Chief smothers McMurphy to death with his pillow. He then carries out McMurphys escape plan by lifting the hydrotherapy console off the floor and hurling the massive fixture through a grated window. Chief climbs through the window and runs off into the distance, with Taber waking up just in time to see him escape and cheering as the others awake.

==Cast==
 
* Jack Nicholson as Randle McMurphy
* Louise Fletcher as Nurse Ratched
* Will Sampson as "Chief" Bromden
* Brad Dourif as Billy Bibbit
* Sydney Lassick as Charlie Cheswick
* Christopher Lloyd as Max Taber
* Danny DeVito as Martini William Redfield as Dale Harding
* Dean Brooks as Dr. John Spivey
* William Duell as Jim Sefelt
* Vincent Schiavelli as Bruce Frederickson
* Delos V. Smith as Scanlon
* Michael Berryman as Ellis
* Nathan George as Attendant Washington
* Lan Fendors as Nurse Itsu
* Mimi Sarkisian as Nurse Pilbow Marya Small as Candy
* Scatman Crothers as Orderly Turkle
* Louisa Moritz as Rose
* Christopher Campagna as Ruckly
* Peter Brocco as Col. Matterson
* Alonzo Brown as Miller
* Mwako Cumbuka as Warren
* Josip Elic as Bancini 
* Ken Kenny as Beans Garfield
* Mel Lambert as Harbor Master
* Kay Lee as Night Supervisor
* Dwight Marfield as Ellsworth
* Phil Roth as Woolsey
* Tin Welch as Ruckley
 

==Production==
Hal Ashby was originally going to direct, but eventually dropped out and was replaced by Miloš Forman, according to Danny DeVitos 2009 appearance on Inside The Actors Studio.  DeVito said he was spotted by Ashby in the play version at the Mercer Arts Center.  Filming began in January of 1975 and concluded approximately three months later, and was shot on location in Salem, Oregon and the surrounding area, as well as on the Oregon coast.

According to cinematographer Bill Butler, Jack Nicholson refused to speak to Milos Forman: "...(Jack) never talked to Milos at all, he only talked to me." 

==Reception==
The film was met with overwhelming critical acclaim;  , Canby called the film "a comedy that cant quite support its tragic conclusion, which is too schematic to be honestly moving, but it is acted with such a sense of life that one responds to its demonstration of humanity if not to its programmed metaphors." 
 bowed saw wine glasses.  Commenting on the score, reviewer Steven McDonald has said, "The edgy nature of the film extends into the score, giving it a profoundly disturbing feel at times -- even when it appears to be relatively normal. The music has a tendency to always be a little off-kilter, and from time to time it tilts completely over into a strange little world of its own ..." 
 Best Actor Best Actress Best Direction Best Picture, Best Adapted Screenplay for Laurence Hauben and Bo Goldman. The film currently has a 96% "Certified Fresh" rating at Rotten Tomatoes,  with the consensus "The onscreen battle between Jack Nicholson and Louise Fletcher serves as a personal microcosm of the culture wars of the 1970s -- and testament to the directors vision that the film retains its power more than three decades later."

One Flew Over the Cuckoos Nest is considered to be one of the greatest American films. Ken Kesey participated in the early stages of script development, but withdrew after creative differences with the producers over casting and narrative point of view; ultimately he filed suit against the production and won a settlement. Carnes, Mark Christopher, Paul R. Betz, et al. (1999).   who wrote, "The first time I heard this story, it was through the movie starring Jack Nicholson. A movie that Kesey once told me he disliked." 

In 1993, this film was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in their National Film Registry.

==Awards and honors==

{| class="wikitable"
! Award !! Category !! Nominee !! Result
|- Academy Award
| Academy Award for Best Picture
| Michael Douglas and Saul Zaentz
|  
|-
| Academy Award for Best Director
| Miloš Forman
|  
|-
| Academy Award for Best Actor
| Jack Nicholson
|  
|-
| Academy Award for Best Actress
| Louise Fletcher
|  
|-
| Academy Award for Writing Adapted Screenplay
| Laurence Hauben and Bo Goldman
|  
|-
| Academy Award for Best Supporting Actor
| Brad Dourif
|  
|-
| Academy Award for Best Cinematography Bill Butler
|  
|-
| Academy Award for Film Editing
| Richard Chew, Lyzee Klingman and Sheldon Kahn
|  
|-
| Academy Award for Original Music Score
| Jack Nitzsche
|  
|- Golden Globe Award
| Golden Globe Award for Best Motion Picture - Drama
| Michael Douglas and Saul Zaentz
|  
|-
| Golden Globe Award for Best Director - Motion Picture
| Miloš Forman
|  
|-
| Golden Globe Award for Best Actor - Motion Picture Drama
| Jack Nicholson
|  
|-
| Golden Globe Award for Best Actress - Motion Picture Drama
| Louise Fletcher
|  
|-
| Golden Globe Award for Best Screenplay
| Laurence Hauben and Bo Goldman
|  
|-
| Golden Globe Award for New Star of the Year - Actor
| Brad Dourif
|  
|- British Academy BAFTA Award
| BAFTA Award for Best Film
| Michael Douglas and Saul Zaentz
|  
|-
| BAFTA Award for Best Direction
| Miloš Forman
|  
|-
| BAFTA Award for Best Actor in a Leading Role
| Jack Nicholson
|  
|-
| BAFTA Award for Best Actress in a Leading Role
| Louise Fletcher
|  
|-
| BAFTA Award for Best Actor in a Supporting Role
| Brad Dourif
|  
|-
| BAFTA Award for Best Editing
| Richard Chew, Lynzee Klingman and Sheldon Kahn
|  
|-
| BAFTA Award for Best Cinematography Bill Butler
|  
|-
| BAFTA Award for Best Adapted Screenplay
| Laurence Hauben and Bo Goldman
|  
|-
|}

===Others===
American Film Institute
* AFIs 100 Years... 100 Movies — #20
* AFIs 100 Years... 100 Heroes and Villains:
** Nurse Ratched — #5 Villain
** Randle Patrick McMurphy — Nominated Hero 
* AFIs 100 Years... 100 Cheers — #17
* AFIs 100 Years... 100 Movies (10th Anniversary Edition) — #33

==See also==
* One Flew Over the Cuckoos Nest (play)|One Flew Over the Cuckoos Nest (play)
* Mental illness in film
* OConnor v. Donaldson, 1975
* Addington v. Texas
* Shock Corridor
* White savior narrative in film

==References==
 

==External links==
 
*  
*  
*  
*  
*  

{{Navboxes|list1=
 
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 