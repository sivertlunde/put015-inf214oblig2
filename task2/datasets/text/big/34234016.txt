Shubhamastu
{{Infobox film
| name           = Shubhamastu
| image          = Image of Subhamasthu.gif
| caption        = Theatrical release poster
| writer         = Thotapalli Madhu  
| story          = Rafi-Mecartin
| screenplay     = Editor Mohan
| producer       = M. V.Lakshmi   Editor Mohan  
| director       = Bhimaneni Srinivasa Rao Indraja  Krishna   Koti
| cinematography = Ram Prasad
| editing        = Akula Bhaskar   Editor Mohan    
| studio         = M. L. Movie Arts
| released       =  
| runtime        =
| country        = India Telugu
| budget         =
| gross          =
}}
 Telugu film Indraja in the lead roles and a cameo appearance by Krishna (actor)|Krishna. The music was composed by Saluri Koteswara Rao.  The film was a remake of the Malayalam film Aniyan Bava Chetan Bava. 

==Plot==
The film follows brothers Anna Rao (Dasari Narayana Rao) and Chinna Rao (Kaikala Satyanarayana|Satynarayana)  and their driver Premachand (Jagapati Babu) with whom the daughters Kasturi (Aamani) and Saroja (Indraja (actress)|Indraja) of both the brothers fall in love. This makes the brothers enemies and it becomes their issue of pride on who will marry Premachand.

==Cast==
 
* Jagapati Babu as Prem Chand
* Aamani as Kasturi Indraja as Saroja Krishna (Cameo) as himself
* Dasari Narayana Rao as Anna Rao Satynarayana as Chinna Rao
* Brahmanandam as Edukondalu Sudhakar as Erupu Ali as Driver
* Tanikella Bharani as Serabasa Raju
* Babu Mohan as Serabasa Rajus assistant AVS as Priest Padmanabham as Veeramachineni Papa Rayudu
* Subbaraya Sharma as Prem Chands father Sangeeta as Prem Chands mother
* Siva Parvathi as Chinna Raos wife
* Radhabai as Prem Chands great grand mother
* Silk Smitha in item number
 

==Soundtrack==
{{Infobox album
| Name        = Shubhamastu 
| Tagline     = 
| Type        = film Koti 
| Cover       = 
| Released    = 1995
| Recorded    = 
| Genre       = Soundtrack
| Length      = 24:08
| Label       = TA Sound Track  Koti 
| Reviews     =
| Last album  = Peddarayudu   (1995)
| This album  = Shubhamastu   (1995)
| Next album  = Akkada Ammayi Ikkada Abbayi   (1996)
}}

The music was composed by Saluri Koteswara Rao and was released on TA Sound Track Audio Company.

{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 24:08
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Go Go Go Gopala
| lyrics1 = Bhuvanachandra SP Balu, Swarnalatha
| length1 = 5:05

| title2  = Ghal Ghallanu
| lyrics2 = Shanmuga Sharma Chitra
| length2 = 4:34

| title3  = Oosi Misso
| lyrics3 = Veturi Sundararama Murthy
| extra3  = SP Balu, Chitra
| length3 = 4:30

| title4  = Bavosthe Maamosthe 
| lyrics4 = D.Narayanavarma
| extra4  = Murali Krishna, Radhika
| length4 = 4:23

| title5  = Ee Bandhanala Nandananni Sirivennela Sitarama Sastry 
| extra5  = SP Balu
| length5 = 5:36
}}

 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- align="center"
! style="background:#B0C4DE;" | No
! style="background:#B0C4DE;" | Song title
! style="background:#B0C4DE;" | Singers
|- Chitra
|-
| 2 || "Ghal" || S. P. Balasubrahmanyam, Chitra
|-
| 3 || "Gopala" || S. P. Balasubrahmanyam, Swarnalatha
|-
| 4 || "Mama" || Murali Krishnan,Radhika Tilak
|-
| 5 || "Osi Misso" || S. P. Balasubrahmanyam, Chitra
|}

==References==
 

==External links==
*  

 

 
 
 
 
 


 