Five Days to Live
{{Infobox film
| name =Five Days to Live
| image =
| alt =
| caption =
| film name =
| director =Norman Dawn
| producer =
| writer =Eve Unsell Garrett Fort
| screenplay =
| story =Dorothy Goodfellow
| based on =     
| narrator =
| starring ={{Plainlist|
*Sessue Hayakawa
*Tsuru Aoki
*Goro Kino
*Misao Seki
*Toyo Fujita
*George Kuwa
}}
| music =
| cinematography =
| editing =
| studio =Robertson-Cole Pictures Corporation
| distributor =
| released =  
| runtime =60 min
| country =
| language =
| budget =
| gross =
}}
Five Days to Live is a 1922 American drama film directed by Norman Dawn and featuring Sessue Hayakawa, Tsuru Aoki,    Goro Kino, Misao Seki, Toyo Fujita and George Kuwa.

== References ==
 

== External links ==
*  