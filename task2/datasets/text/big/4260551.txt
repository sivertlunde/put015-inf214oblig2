The Arrival (film)
 
{{Infobox film
| name           = The Arrival
| image          = The Arrival, Movie Poster.jpg
| caption        = Theatrical film poster
| director       = David Twohy
| producer       = Robert W. Cort Ted Field
| writer         = David Twohy
| starring       = Charlie Sheen Lindsay Crouse Teri Polo Ron Silver Richard Schiff
| music          = Arthur Kempel
| cinematography = Hiro Narita
| editing        = Martin Hunter
| studio         = Live Entertainment
| distributor    = Orion Pictures
| released       =  
| runtime        = 115 minutes
| country        = United States Mexico
| language       = English
| budget         = US$25,000,000 (estimated)
| gross          = USA: US$14,031,906
}}
 conspiracy that turns his life upside down.

A Blu-ray version of the film was released April 21, 2009. 

A sequel,   was released on November 6, 1998.

==Plot==
The Film opens with climatologist Ilana Green (Lindsay Crouse) examining a poppy field and remarking that it "shouldnt be here". We then see that the poppy field is in the middle of the Arctic.
 radio astronomer extraterrestrial radio signal from Wolf 336, a star 14 light years from Earth. Zane reports this to his supervisor, Phil Gordian (Ron Silver) at the Jet Propulsion Laboratory, but Phil dismisses the claims. Zane soon finds that he has been fired because of supposed budget cuts, and blacklisted, preventing him from working at other telescopes. Taking a job as a television satellite installer, he creates his own telescope array using his customers dishes in the neighborhood, operating it secretly from his attic with help of his young next door neighbor, Kiki (Tony T. Johnson).

Zane again locates the radio signal, but it is drowned out by a terrestrial signal from a Mexican radio station. Zane attempts to tell his former coworker, Calvin (Richard Schiff), but finds he has just died suspiciously from carbon monoxide poisoning. Zane travels to the fictional town of San Marsol in Mexico and finds that the local radio station, from which the signal apparently originated, was burnt to the ground. Searching the area around town, he comes across a new power plant. There, he meets Ilana Green, and tries to help protect her atmospheric analysis equipment from the plants overzealous security forces. While in custody at the plant, Ilana explains that the Earths temperature has recently risen several degrees, melting the polar ice. She is investigating the power plant, one of several recently built, that appears to be the cause of this increase. The two are released, but without Ilanas equipment. Surprisingly, Zane realizes one of the guards could pass as the identical twin of his former boss, Phil. As Zane and Ilana regroup, Phil instructs some agents, posing as gardeners, to release an alien device in Zanes attic that creates a miniature black hole, consuming all of Zanes equipment. Zane leaves Ilana to again investigate the power plant and she is soon killed by a scorpion planted in her room.

Zane discovers the plant is a front for an underground alien base. The very different looking aliens are able to disguise themselves with an external skin to infiltrate our society. Zane finds that all of the bases expel large amounts of greenhouse gas into the atmosphere.  Zane is discovered but escapes back into the nearby town and attempts to convince the local sheriff of the situation. However, alien agents bring Ilanas body to the police station, making Zane a suspect in her death; Zane escapes and sneaks back into the United States. He accosts Phil on the JPL grounds, forcing him to admit that the aliens are trying to raise the Earths temperature to not only kill off humans but make the planet hospitable for themselves. Zane secretly records the conversation and once Phil discovers the recording he sends agents to stop Zane.

Zane returns home to find his attic devoid of equipment. He figures out the only way to broadcast the tape is to go to a nearby large satellite and beam the signal directly to a news satellite, broadcasting it worldwide. With the help of his girlfriend Char (Teri Polo) and Kiki, he travels to the large satellite. Phil and his agents soon disable the satellite controls from the main building. Wondering how they were found so quickly, Zane briefly suspects Char of being an alien, until they are both attacked by one of Phils agents. Zane leaves the tape with Kiki and instructs him to transmit it when he gives the order and then he and Char sneak over to the satellites base and barricade themselves in the control room. Zane makes the necessary adjustments and tells Kiki to activate the tape, but Kiki reveals himself to be an alien agent and he opens the door to allow Phil inside, who confiscates the tape.  

Phil and his agents ram down the door to the satellite control room room with a van, but Zane freezes them with nitrogen gas. As he works to free the tape stuck in Phils frozen jacket, one of the agents drops a sphere which starts to form another singularity in the room. Phil begins to defrost, and tries to grab Zanes arm, but Zane smashes off Phils arm with a fire axe. Zane and Char escape through the satellites optical path, exiting safely onto the collapsed dish before the device implodes most of the satellite base. In the distance they see Kiki, and Zane tells him to tell the aliens that he will soon broadcast this tape; they watch as Kiki runs off. In the films epilogue, Zanes conversation with Phil is broadcast across the globe.

==Cast==
* Charlie Sheen as Zane Zaminsky
* Ron Silver as Phil Gordian / Mexican Guard
* Lindsay Crouse as Ilana Green
* Teri Polo as Char
* Richard Schiff as Calvin
* Tony T. Johnson as Kiki
* Leon Rippy as one of Phils agents
* David Villalpando as a Cabbie, later revealed to be an alien
* Buddy Joe Hooker as another of Phils agents

==Release==
===Critical reception===
The film was positively received by critics; review aggregate Rotten Tomatoes reports that 63% of critics have given the film a positive review based on 32 reviews, with an average score of 6.3/10, and its consensus states that "The Arrival is stylish and inventive and offers a surprisingly smart spin on the alien invasion genre..."   

===Box Office=== Independence Day just months apart which itself received a mixed to positive response despite being very successful at the box office. However, the film had a rather successful run overseas. 

===Blu-ray===
A Blu-ray version of the film was released April 21, 2009. Unlike the laserdisc release, the Blu-ray version includes no special features. The laserdisc release included commentary, documentaries and alternative endings not included in the Blu-ray or DVD releases.

==PC Video Game==
In 1996 a PC video game of the movie was released by Live Interactive. The gameplay generally included puzzle based levels.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 