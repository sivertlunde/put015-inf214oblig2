Virginia (1941 film)
{{Infobox film
| name           = Virginia
| image          = 
| caption        = 
| director       = Edward H. Griffith
| producer       = Edward H. Griffith
| writer         = Edward H. Griffith Virginia Van Upp
| starring       = Madeleine Carroll
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
}}

Virginia is a 1941 American drama film directed by Edward H. Griffith.    It featured the onscreen debut of Sterling Hayden.

==Cast==
* Madeleine Carroll as Charlotte Dunterry
* Fred MacMurray as Stonewall Elliott
* Sterling Hayden as Norman Williams
* Helen Broderick as Theo Clairmont
* Carolyn Lee as Pretty Ellott Marie Wilson as Connie Potter Paul Hurst as Thomas
* Tom Rutherford as Carter Francis
* Leigh Whipper as Ezechial
* Louise Beavers as Ophelia
* Darby Jones as Joseph

==See also==
* Sterling Hayden filmography

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 