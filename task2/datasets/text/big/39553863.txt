Beware of Pickpockets
 
 
{{Infobox film
| name           = Beware of Pickpockets
| image          = BewareofPickpockets.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 歡樂神仙窩
| simplified     = 欢乐神仙窝
| pinyin         = Huān Lè Shén Xiān Wō 
| jyutping       = Fun1 Lok6 San4 Sin1 Wo1 }}
| director       = Wu Ma
| producer       = Karl Maka
| writer         =  Raymond Wong
| story          = 
| based on       = 
| narrator       = 
| starring       = Dean Shek Karl Maka Wu Ma
| music          = Frankie Chan
| cinematography = Manny Ho
| editing        = Tony Chow
| studio         = Cinema City & Films Co. Golden Princess Films
| released       =  
| runtime        = 91 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$5,109,130
}}
 1981 Cinema Hong Kong comedy film directed by Wu Ma and starring Dean Shek, Karl Maka and Wu.            

==Plot==
Righteous officer Big Nose is ordered to arrest pickpocket Extra Hand. However, each time he was caught, he was released by lack of evidence. Although Extra Hand is a tricky man, he is actually a Robin Hood like pickpocket who steals dirty money from the rich to raise seven orphaned children that he adopted. Extra Hand plans to save money to build an orphanage for the children to live in.

Later, Extra Hand learns that thief Gold Teeth has robbed many jewels and he therefore planned to steal from him.

One time, Extra Hand miscalculated and was caught. Although Big Nose was hesitant to arrest him, Extra Hand decides not to give a tough job for Big Nose and surrenders to him.

==Cast==
*Dean Shek as Extra Hand
*Karl Maka as Big Nose
*Wu Ma as Superintendent
*Annie Liu
*Hon Kwok Choi as Gold Teeth
*Cheung Ka Ho as Orphaned child
*Cheung Ying Wai as Orphaned child
*Cheng Ka Kai as Orphaned child
*Ha Wai Hong as Orphaned child
*Lok Wai Ming as Orphaned child
*Hui Yiu Wai as Orphaned child 
*Kwok Po Keung as Orphaned child
*Tang Ching
*Tai San
*Ng Hon Keung
*Ho Pak Kwong Raymond Wong as Dishonest Diner (cameo)
*Sai Kwa Pau
*Ka Lee
*Pang Yun Cheung
*Wong Au Ngai
*Lai Lok Ling
*Lau Chuen
*Leung Hung

==Theme song==
*Beware of Pickpockets (提防小手)
**Composer: Joseph Koo Raymond Wong, Cheng Kwok Kong
**Singer: Dean Shek, Wah Wah Children Choir

==Box office==
The film grossed HK$5,109,130 at the Hong Kong box office during its theatrical run from 5 to 18 February 1981 in Hong Kong.

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 