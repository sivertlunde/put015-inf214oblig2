Alvin and the Chipmunks: The Squeakquel
{{Infobox film
| name           = Alvin and the Chipmunks: The Squeakquel
| image          = Chipmunks2squeakuel.jpg
| alt            = Three male chipmunks and three female chipmunks, a red title banner in the middle of the poster and three female chipmunks underneath  
| caption        = Theatrical release poster
| director       = Betty Thomas Ross Bagdasarian Jonathan Aibel Glenn Berger
| based on       =     Jason Lee Justin Long Matthew Gray Gubler Jesse McCartney Amy Poehler Anna Faris Christina Applegate David Newman
| cinematography = Anthony B. Richmond
| editing        = Matthew Friedman Bagdasarian Company Dune Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $70 million 
| gross          = $443,140,005   
}}
 Jason Lee Twentieth Century Fox 2000 Bagdasarian Company. Alvin and the Chipmunks and was released in theaters on December 23, 2009.

==Plot==
  David Seville Jason Lee) the Chipmunks, Toby (Zachary Ian Hawke Brittany (Christina Jeanette (Anna Eleanor (Amy Poehler), also known as The Chipettes, emerge and Ian hires them as plot to get back at the Chipmunks and revive his career.

While at school, the Chipmunks are bullied by jocks. They visit the principals office, only to discover that the principal, Dr. Rubin (Wendie Malick) is a huge fan and wants them to help raise money for a music program by participating in a contest. Meanwhile, Ian is shocked to find the Chipmunks on the front page of his newspaper. After he reads a story about them, he quickly sends the Chipettes to school.

When the Chipmunks meet the Chipettes, a rivalry forms after the former group learns that the latter group are with Ian. Meanwhile, Alvin becomes popular with the jocks and joins the football team, not realizing that the next game is during the concert. At the concert, Theodore and Simon tell the fans that Alvin didnt show up and that they cant perform, leading to a victory for the Chipettes. When Alvin finally shows up after the concert is over, he finds the auditorium empty and is ignored by his brothers at home.

Soon the Chipettes are hired, but learn that the concert they are to perform as an opening act for a Britney Spears concert at the Staples Center is on the same night as the school contest. Ian convinces them to blow off the battle and perform at the concert, but refuses to give the same credit to Jeanette and Eleanor that he gives Brittany. She demands that they all go together or not at all, until Ian threatens to send them to a barbecue restaurant (in a similar manner to the Chipmunks) unless they perform.
 cell phone. Simon and Theodore are on the verge of going out to perform until the others arrive just in time to perform at the contest. The Chipmunks and the Chipettes perform together and receive the money for the music program. Dave, who had left the hospital upon learning that Toby was looking after the Chipmunks, returns during the contest happy to see his boys again. Meanwhile, Ian gets into more trouble at the concert of Staples Center he set up for the girls when he attempts to imitate them. After the contest, Dave allows the Chipettes to stay with them.

In two post-credits scenes, Dr. Rubin has the jocks scrape gum off from under the bleachers in the gymnasium, while Ian is thrown into a dumpster.

==Cast==
* Zachary Levi as Toby Seville
* David Cross as Ian Hawke Jason Lee as David "Dave" Seville
* Wendie Malick as Dr. Rubin
* Anjelah Johnson as Julie Ortega
* Kathryn Joosten as Aunt Jackie
* Kevin G. Schmidt as Ryan Edwards
* Chris Warren, Jr. as Xander
* Bridgit Mendler as Becca Kingston

===Voices===
* Justin Long as Alvin Seville
* Matthew Gray Gubler as Simon Seville
* Jesse McCartney as Theodore Seville 
* Christina Applegate as Brittany
* Anna Faris as Jeanette
* Amy Poehler as Eleanor

===Cameos===
* Quest Crew as Lil Rosero Dancers
* Charice Pempengco as herself Honor Society as themselves Digger the Gopher (voice)

==Reception==

===Critical reception===
  rating average normalized rating out of 100 to reviews from film critics, has a rating score of 41 based on 20 reviews. 

  said that "even if theres little here for older viewers to enjoy, youngsters will love the slapstick action and catchy soundtrack." 

Some reviews were positive, such as Joe Leydon, writing for Variety (magazine)|Variety, calling it "a frenetic but undeniably funny follow-up that offers twice the number of singing-and-dancing rodents in another seamless blend of CGI and live-action elements."  Betsy Sharkey of the Los Angeles Times commented on Betty Thomas direction, saying that she brings "a light campy touch as she did in 1995s The Brady Bunch Movie." 

After the film had garnered $112 million worldwide at the box office over its first weekend, some critics were disappointed that it was more popular than other movies in wide release aimed at a family audience. {{cite web
| date = 2009-12-28
| author = Ryan Michael Painter
| title = Weekend Box Office Reaction
| url = http://www.inthisweek.com/view.php?id=2012967
}}  {{cite web
| date = 2009-12-27
| author = Scott Mendelson
| title = HuffPost Weekend Box Office in Review: Avatar Dominates Record-Breaking Weekend
| url = http://www.huffingtonpost.com/scott-mendelson/avatar-dominates-record-b_b_404452.html
| work = Huffington Post
| accessdate = 2010-01-24
| archiveurl= http://web.archive.org/web/20091230142453/http://www.huffingtonpost.com/scott-mendelson/avatar-dominates-record-b_b_404452.html| archivedate= 30 December 2009  | deadurl= no}}
  Richard Corliss of Time (Magazine)|Time wrote that families "could have taken the cherubs to The Princess and the Frog or A Christmas Carol (2009 film)|Disneys A Christmas Carol, worthy efforts that, together, took in only about a fifth of the Chipmunks revenue in the same period". 

===Box office===
On its opening Wednesday, the film opened to #1 with $18,801,277, and finished the weekend at #3 behind  . 

===Awards===
*2010 Kids Choice Awards: Favorite Movie (winner)
*2010 BAFTA Childrens Awards: BAFTA Kids Vote - Feature Film (winner)

==Soundtrack==
{{Infobox album Italic title=force
| Name             = Alvin and the Chipmunks: The Squeakquel: Original Motion Picture Soundtrack
| Type             = Soundtrack
| Artist           = Alvin and the Chipmunks and The Chipettes
| Cover            = 
| Released         =  
| Certification    = hip hop, dance
| Length           = 50:15 Rhino
| Ross Bagdasarian, Janice Karman, Ali Dee Theodore
| Reviews          =
| Last album       = Undeniable (Chipmunks album)|Undeniable (2008)
| This album       = Alvin and the Chipmunks: The Squeakquel: Original Motion Picture Soundtrack (2009)
| Next album       =   (2011)
}} Honor Society Filipino singer Charice, were all featured artists for both the movie and soundtrack.

===Track listing===
{{tracklist
| writing_credits   = yes
| headline          = Standard
| extra_column      = Performing Artist(s)
| title1            = You Really Got Me Honor Society The Chipmunks
| writer1           = Ray Davies
| length1           = 3:05
| title2            = Hot n Cold
| extra2            = The Chipettes
| writer2           = Katy Perry, Lukasz Gottwald, Max Martin
| length2           = 4:05 So What
| extra3            = The Chipettes Alecia B. Moore, Johann Karl Schuster, Max Martin
| length3           = 4:05
| title4            = You Spin Me Round (Like a Record)
| extra4            = The Chipmunks
| writer4           = Peter Burns, Stephen Coy, Mike Percy, Timothy Lever
| length4           = 3:36
| title5            = Single Ladies (Put a Ring on It)
| extra5            = The Chipettes Thaddis Harrell, Christopher Stewart
| length5           = 2:57
| title6            = Bring It On
| extra6            = The Chipmunks
| writer6           = Ali Dee Theodore, Jason Gleed
| length6           = 3:46
| title7            = Stayin Alive
| extra7            = The Chipmunks
| writer7           = Barry Gibb, Robin Gibb, Maurice Gibb
| length7           = 3:05
| title8            = The Song Queensberry
| extra8            = The Chipettes
| writer8           = Ali Dee Theodore, Alana Da Fonseca, Mike Klein, John McCurry
| length8           = 3:06
| title9            = Its OK
| extra9            = The Chipmunks
| writer9           = Ali Dee Theodore, Alana Da Fonseca, Vinny Alfieri
| length9           = 2:48
| title10           = Shake Your Groove Thing
| extra10           = The Chipmunks and The Chipettes Frederick Perren
| length10          = 3:52
| title11           = Put Your Records On
| extra11           = The Chipettes
| writer11          = Corinne Bailey Rae, John Beck, Steven Chrisanthou
| length11          = 3:35
| title12           = I Want to Know What Love Is
| extra12           = The Chipmunks Mick Jones
| length12          = 2:54 We Are Family
| extra13           = The Chipmunks and The Chipettes
| writer13          = Bernard Edwards, Nile Rodgers
| length13          = 3:03 No One
| note14            = featuring Charice
| extra14           = The Chipettes
| writer14          = Alicia Keys, Kerry Brothers, Jr., George Michael Harry
| length14          = 4:00
}}
{{tracklist
| writing_credits   = yes
| headline          = Bonus track
| extra_column      = Performing Artist(s)
| title15           = I Gotta Feeling
| extra15           = The Chipmunks and The Chipettes
| writer15          = The Black Eyed Peas, David Guetta, Frédéric Riesterer
| length15          = 4:06
}}
{{tracklist
| writing_credits   = no
| headline          = Amazon.com Bonus track 
| collapsed         = yes
| extra_column      = Performing Artist(s)
| title16           = In The Family The Chipmunks and The Chipettes
| writer16          = Ali Dee Theodore, Jason Gleed, Alana Da Fonseca
| length16          = 3:01
}}
{{tracklist
| writing_credits   = yes
| headline          = iTunes Deluxe Edition Bonus Tracks 
| collapsed         = yes
| extra_column      = Performing Artist(s)
| title16           = Daydream Believer The Chipmunks John Stewart
| length16          = 2:45
| title17           = Get Ur Good Time On
| extra17           = The Chipmunks
| writer17          = 
| length17          = 2:48
| title18           = The Song Queensberry
| writer18          = Ali Dee Theodore, Alana Da Fonseca, Mike Klein, John McCurry
| length18          = 3:06
}}

===Chart performance===
{| class="wikitable sortable" Chart (2009) Peak position
|- ARIA Charts|Australian Albums Chart  15
|- Recording Industry New Zealand Albums Chart  3
|- UK Albums Chart  6
|-
|align="left"|U.S. Billboard 200|Billboard 200  6
|}

==Marketing==

===Video game===
 
A video game adaption was released on December 1, 2009 (the same day as the movies soundtrack) only for the Wii and Nintendo DS.

===Home media===
Alvin and the Chipmunks: The Squeakquel was released on DVD/Blu-ray/Digital Copy on March 30, 2010 in North America, on April 12, 2010 in the United Kingdom and on June 2, 2010 in Australia.

==Sequels==
  Fox 2000 Pictures started production on the film during a Caribbean cruise on the Carnival Dream ship. Filming took place primarily on the ships upper, open decks with scenes featuring actor Jason Lee (reprising his role as David Seville) and the antics of The Chipmunks in the Carnival Dreams outdoor recreation areas. Stops on the itinerary included Cozumel, Roatan, Belize and Costa Maya (which provided tropical backdrops for many of the movies shipboard scenes).  A fourth film, Alvin and the Chipmunks: Road Chip, is scheduled for release on December 23, 2015. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 