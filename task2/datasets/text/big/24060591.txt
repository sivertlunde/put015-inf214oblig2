A Song for Miss Julie
{{Infobox film
| name           = A Song for Miss Julie
| image          =File:A Song for Miss Julie film poster.jpg
| image_size     =
| caption        =Film poster
| director       = William Rowland
| producer       = Carley Harriman (associate producer) William Rowland (associate producer) Michael Foster (story) Rowland Leigh (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Mack Stengler
| editing        = James Smith
| distributor    =
| released       = 19 February 1945
| runtime        = 69 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

A Song for Miss Julie is a 1945 American film directed by William Rowland.

== Plot summary ==
 

== Cast ==
*Shirley Ross as Valerie Kimbro
*Barton Hepburn as George Kimbro
*Jane Farrar as Julie Charteris
*Roger Clark as Stephen Mont
*Cheryl Walker as Marcelle Conway
*Elisabeth Risdon as Mrs. Ambrose Charteris
*Lillian Randolph as Eliza Henry
*Peter Garey as Pete - the Bellhop
*Renie Riano as Eurydice Lannier
*Harry Crocker as John Firbank
*Vivien Fay as Herself
*Alicia Markova as Herself - Ballet Dancer
*Anton Dolin as Himself - Ballet Dancer

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 

 