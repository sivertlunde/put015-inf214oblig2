Fixing America
{{Infobox film
| name           = Fixing America
| image          = Fixing America DVD Cover.jpg
| alt            = Fixing America 
| caption        = Fixing America DVD cover
| producer       = Steve Laffey Stephen Skoly
| writer         = Steve Laffey
| starring       = 
| music          = Mauro Colangelo
| cinematography = Jeff Hoffman
| editing        = Fred Tindall
| director       = Anthony Ambrosino
| studio         = 
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} financial crisis, the disconnect with the political elite, and ultimately collects their thoughts, their emotions and especially their solutions to fix America’s financial problems.

== Synopsis ==
Fixing America is a journey across the byways of America interviewing seemingly ordinary, yet truly exceptional folks who bare all and offer solutions to fix Americas problems. In a time of unprecedented global financial disaster, Fixing America regards the “ordinary” American People most suited to disentangle problems and crucial issues.  In fact, they are the ones who deal with complexity everyday, struggling to make ends meet and put their children through college. 

In this documentary they offer simple ideas, but what they are conveying is not the need for simplicity. Their insight is grounded to the principles that are the make-up of the nation. They refer to the forefathers, the Constitution, to the historical revolutionary acts that have propelled the country forward during tough times. Their words intertwine and are supported by renowned scholars, such as professors Lawrence Kotlikoff, John Friar, and Khalil Habib; passionate journalists such as Dylan Ratigan and Louie Free and keen advocates such as Lew Ward, Grover Norquist, Steve Forbes, Robert Scott, Ned Ryun, and Scott Paul. They ultimately bring a sense of hope and cooperation with these interactions, and let Americans appreciate “they are not alone” in this colossal struggle. 

== Production ==
Fixing America was conceived by financial expert, Steve Laffey, the two term former mayor of Cranston, Rhode Island. Laffey improvises himself as a journalist along with a crew of ten, and talent that includes, director Anthony Ambrosino, director of photography, Jeff Hoffman, editor Fred Tindall, and original music by Mauro Colangelo. Produced by Ste
 
ve Laffey and creative and business partner, Stephen Skoly, the documentary was shot in 18 states and the District of Columbia in April and May 2011, traveling on the back roads and byways of America from Fort Collins, Colorado to Washington DC to New England.  Twenty-one independent contractors worked on the film over 10 months in 2011.

The production is a truly independent effort, self-financed by the Executive Producers, Steve Skoly and Steve Laffey, and has not benefited by any public money, nor grants and has no co-production partners or any associations with broadcasters, production companies or political organizations.

== Background ==
In January 2011, Steve Laffey was working on a Media Plan for an Internet TV and radio business, that would reproduce the quality of direct, one-to-one communication with people he had been able to experience in the last few years during his numerous trips across America riding in a Ford Expedition with wife and five of his six kids. His business plan drew on his interest to establish a more personal relationship with the American people, meeting them head-on  and  discuss heart-to-heart matters, while sharing ideas. 
 Margin Call. Listening to J.C. Chandor, Laffey realized that making a grassroots documentary film featuring  the American people frankly voicing solutions, would be a better first step to help fix America.   

He contacted Stephen Skoly, producer, founder of JawDoc Productions,  who knew Laffey’s grassroots background – as evidenced by his campaigns for Mayor and the U.S. Senate and as portrayed in Joe Klein’s Time Magazine Article Running Against The Big Shots   and Laffey’s own book, Primary Mistake (Penguin Books 2007) – and was interested in participating to the making of a populist movie, showing how regular folks know better than the political elite. Together they formed Fixing America LLC, in Colorado

==References==
 
 
* http://www.stuttgartdailyleader.com/topstories/x741437368/Film-crew-in-Stuttgart-Friday
* http://video.turnto10.com/v/40174457/interview-steve-laffey.htm?q=laffey
* http://cranstononline.com/stories/Mayor-Laffey-travels-cross-country-to-learn-how-to-Fix-America,65558?search_filter=fixing+america&town_id=3&sub_type=stories
* http://www.time.com/time/magazine/article/0,9171,1229121,00.html

== External links ==
*  
*  