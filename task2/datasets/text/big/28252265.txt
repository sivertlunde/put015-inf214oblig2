Devil's Playground (2010 film)
 
 Devils Playground}}
{{Infobox film
| name           = Devils Playground
| image          = Devils Playground.jpg
| caption        = 
| director       = Mark McQueen
| producer       = {{plainlist|
* Freddie Hutton-Mills
* Bart Ruspoli
* Jonathan Sothcott
}}
| writer         = Bart Ruspoli
| starring       = {{plainlist|
* Danny Dyer
* Craig Fairbrass
* MyAnna Buring
* Jaime Murray
}}
| music          = James Edward Barker
| cinematography = Jason Shepherd
| editing        = Rob Hall
| studio         = {{plainlist|
* Black & Blue Films
* HMR Films
* Widescreen
* Intandem
}}
| distributor    = E1 Entertainment
| released       =   }}
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
Devils Playground is a British horror film directed by Mark McQueen and starring Craig Fairbrass. Intandem Films has the worldwide rights to the film, which was produced by Freddie Hutton-Mills, Bart Ruspoli and Jonathan Sothcott. 

== Plot ==
After the final stage of human testing goes horribly awry, the test subjects of the fictional pharmaceutical company N-Gen become violently ill. As the side effects worsen, the test subjects become increasingly violent until they are little but marauding beasts. Worse yet, their bites are infectious and in short order London is overrun with hordes of bloodthirsty monsters. Cole, a mercenary for N-Gen and a hardened killer, is searching for Angela Mills, the only hope of a cure for this plague which threatens the globe. As the only test subject that did not suffer side effects, her immunity holds the key to preventing a world wide apocalypse. Coles mission is complicated by chaos, continual attacks by the infected, and the virus slowly overtaking Coles body. 

== Cast ==
* Danny Dyer as Joe
* Craig Fairbrass as Cole
* MyAnna Buring as Angela
* Jaime Murray as Lavinia
* Shane Taylor as Geoffrey
* Bart Ruspoli as Matt Mills Craig Conway as Steve
* Lisa McAllister as Kate
* Alistair Petrie as Andy Billing
* Colin Salmon as Peter White
* Sean Pertwee as Rob

== Production ==
Shooting began on 30 November 2009 at Elstree Studios. 

== Release ==
Devils Playground premiered at the Gorezone Film Festival on 3 October 2010.   Vivendi released it on DVD and video n demand on 11 October 2011. 

== Reception ==
Leslie Felperin of Variety (magazine)|Variety called it "unoriginal but watchable".   Mark L. Miller of AICN wrote that it is "heavy on action and surprisingly textured when it comes to story."   William Bibbiani of Crave Online called it a forgettable and "completely nondescript" knock-off of 28 Days Later.   Kayley Viteo criticized the rapid shifts in tone and called it a caricature of 28 Days Later.   Peter Dendle wrote that the film is "largely familiar, but the execution is competent and convincing". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 