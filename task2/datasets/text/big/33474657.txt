Act of Valor
{{Infobox film name           = Act of Valor image          = Act of Valor poster.jpg size           = alt            = caption        = Theatrical release poster director  Mike McCoy Scott Waugh producer       = Mike McCoy Scott Waugh writer         = Kurt Johnstad starring       = Roselyn Sánchez Nestor Serrano Emilio Rivera Rorke Denver music          = Nathan Furst cinematography = Shane Hurlbut editing        = Siobhan Prior Michael Tronick Scott Waugh studio         = Bandito Brothers distributor    = Relativity Media released       =   country        = United States language       = English runtime        = 110 minutes budget         = $12 million  gross          = $81,272,766   
}}
 Mike McCoy and Scott Waugh, and written by Kurt Johnstad. It stars Alex Veadov, Roselyn Sánchez, Nestor Serrano, Emilio Rivera, and active duty United States Navy SEALs|U.S. Navy SEALs and Special Warfare Combatant-craft Crewmen|U.S. Navy Special Warfare Combatant-craft Crewmen. The film was released by Relativity Media on February 24, 2012.
 Best Original Song.   

==Plot== IED disguised Chechen terrorist training camp in Indonesia. CIA operatives, drug smuggler named Mikhail "Christo" Troykovich. Christos men kill Ross and capture Morales, who is imprisoned in a jungle compound and tortured.
 SEAL Team Lieutenant Rorke (Rorke Denver) confides to Chief Dave that his wife is pregnant and has the entire team spend time together with their families until their next deployment. SEAL Team Seven, consisting of Rorke, Dave, Wiemy, Mikey, Ray, Sonny, and Ajay, is then deployed to Costa Rica to exfiltrate Morales.
 HALO and hold position outside the compound all night. At dawn, they approach the compound, hear Morales being tortured, and decide to enter the compound early. Rorke and Weimy, the team sniper, provide cover for the other five, led by Dave, who conduct room-clearing, engaging several enemy guards. SEAL operative Mikey is shot in the eye, blinding him and knocking him unconscious, though he survives. The SEALs extract Morales, escaping with her and recovering a cellphone full of the information she had gathered. However, the gunfight alerts the enemy quick reaction force down the road, who drive toward the camp. The SEALs commandeer an enemy truck and exfiltrate. The hot pursuit forces them to revert to a tertiary extraction point where the SOC-R boats extract the team and neutralize the enemy pursuit with miniguns.

Christo and Shabal, who are revealed to have been childhood friends, meet in Kiev. Christo knows the CIA is watching him and informs Shabal that subordinates will complete their project. Shabal is enraged, but he goes to a factory, which is assembling extremely powerful bomb vests that can evade metal detectors and are thin enough to be worn under any clothing without attracting attention.
 USS Bonhomme Muslim terrorist, seeks to bring jihad to the U.S., while Christo is not merely a drug dealer, but a smuggler, with routes and contacts for smuggling people into the U.S. Two of the SEALs, Ajay and Ray, are sent to Somalia, where an arms transfer involving Shabal is taking place. The remaining SEALs, comprising Rorke, Dave, Sonny and Weimy, stay in the U.S. in case the terrorists make it in. Miller himself has been reassigned to SEAL Team Four, hunting for Christo somewhere on the oceans. Lieutenant Rorke gives Dave a letter to give to his family in case he is killed.
 Baja California, where the team stages an assault. They successfully attack and secure the island, killing eight terrorists, although Shabal and the remaining eight escape. Elsewhere, in the South Pacific, SEAL Team Four successfully captures Christo. Interrogated and threatened with permanent separation from his family along with other implied sanctions, Christo reveals his connection with Shabal and his plans to have his martyrs detonate their vests at strategic points throughout the U.S., causing a panic and doing economic damage surpassing that following the September 11 attacks.
 dives on it to save his team before it detonates, mortally wounding him. Dave pursues the terrorists and shoots them as they try to escape through the tunnels. He is then shot several times and gravely wounded by Shabal, who is intercepted and killed by Sonny before he could execute Dave.

At home, Rorke is given a military funeral with full honors, where the SEALs pay their respects. It is then revealed that Daves narration throughout the movie was a written letter meant for Rorkes son. The film ends with a dedication to every U.S. Navy SEAL killed in action since 9/11 along with a listing of their names as well as a photo montage of other everyday public servants (soldiers, police, firefighters, etc.).

==Cast==
*Jason Cottle as Abu Shabal
*Rorke Denver as Lt. Rorke 
*Alex Veadov as Christo   
*Roselyn Sánchez as Lisa Morales 
*Nestor Serrano as Walter Ross 
*Emilio Rivera as Sanchez
*Drea Castro as Recruit
*Keo Woolford as Recruit
*Thomas Rosales, Jr. as Christos RHM
*Gonzalo Menendez as Commander Pedros
*Ailsa Marshall as Lt Rorkes Wife

==Production==

===Development===
In 2007, Mike McCoy and Scott Waugh of Bandito Brothers Production filmed a video for the Special Warfare Combatant-craft Crewmen which led the United States Navy|U.S. Navy to allow them to use actual active duty United States Navy SEALs|SEALs. After spending so much time working closely with the SEALs, McCoy and Waugh conceived the idea for a modern day action movie about this covert and elite fighting force. As Act of Valor developed with the SEALs on board as advisors, the filmmakers realized that no actors could realistically portray or physically fill the roles they had written and the actual SEALs were drafted to star in the film. The SEALs would remain anonymous, as none of their names appear in the films credits.   

For the Navy, the film is an initiative to recruit SEALs.    According to The Huffington Post, the Navy required the active-duty SEALs to participate. 

Relativity Media acquired the rights to the project on June 12, 2011 for $13 million and a $30 million in prints and advertising commitment. Deadline.com called it "the biggest money paid for a finished film with an unknown cast".    The production budget was estimated to be between $15 million and $18 million.   

===Principal photography===
 
 North Park area. {{cite web|url=http://www.sdnews.com/view/full_story/9014517/article-Filming-downtown-brings-opportunities-|title=Filming downtown brings opportunities
|last=Anderson|first=Cathy|work=San Diego Community Newspaper Group|date=June 2010|accessdate=October 21, 2011|archiveurl=http://www.webcitation.org/62aoqgjd4|archivedate=October 21, 2011|deadurl=no}}  Other locations included Mexico, Puerto Rico,  Ukraine,  Florida,  and at the John C. Stennis Space Center in Mississippi.   

The crew filmed at Navy training sites to provide realistic settings, such as a drug cartel base, a terrorist camp on an isolated island, and a smugglers yacht. 
  Zeiss ZE and Panavision Primo lenses.  The cameras followed the SEALs planned out missions in the film.  Hurlbut used an 18mm Zeiss ZE  mounted on the SEALs helmets to capture their point of view. The 25mm Zeiss ZE was used to capture natural light coming through windows. The 21mm Zeiss ZE was used as a stake cam so a truck could drive over it.  The Navy held final cut privileges  in order to remove any frames to address security concerns and kept raw footage to use for real-life training and other purposes. 

==Release==
Act of Valor was scheduled to be released on February 17, 2012 in the U.S. to coincide with Washingtons Birthday|Presidents Day,  but was pushed back to February 24, 2012.  The film was released in the UK and Ireland on March 23 as Act of Valour by Momentum Pictures.

===Home video releases===
Act of Valor was released on Blu-ray Disc and DVD on June 5, 2012 with a rating of R-rated|R. 

===Accolades=== Choice Action Movie.

==Reception== jingoistic attitude that ignores the complexities of war."    Metacritic assigned the film an average rating of 40 out of 100, based on 34 reviews. 

However, despite the negative critical reaction, the film opened at #1 at the box office, earning $24,476,632 in its opening weekend from 3,039 theaters for an average of $8,054 per theater. Audience reaction was highly positive; moviegoers polled by CinemaScore gave the movie an "A" grade, on an A+ to F scale. 

Many reviews, both positive and negative, have expressed praise for the action sequences while criticizing the plot and acting. Claudia Puig from USA Today, for example, said the action in the film is "breathtaking," but gave the film an overall negative review, in which she wrote that "the soldiers awkward line readings are glaring enough to distract from the potency of the story."  Similarly, Amy Biancolli from the San Francisco Chronicle wrote, "Act of Valor is intended to wow audiences with high-test action while planting a giant wet kiss on the smacker of the U.S. military – and it scores at both tasks," but that, ultimately, "the film gets snagged by its own narrative convention."  Michael Rechtshaffen from The Hollywood Reporter had a similar opinion, stating, "Although the film has its undeniably immersive, convincing moments, the merging of dramatic re-creations and on-camera performances proves less seamlessly executed than those masterfully coordinated land, sea and air missions."  Roger Ebert of the Chicago Sun-Times gave the film two and a half out of four stars, and complained that "we dont get to know the characters as individuals, they dont have personality traits, they have no back stories, they dont speak in colorful dialogue, and after the movie youd find yourself describing events but not people." 

==Accusation of anti-semitism==
The movie caused an outcry among viewers who saw the depicting of the Jihad|Jihadists financier, portrayed by actor Alex Veadov, as being Jewish as Antisemitism|antisemitic.     Blogger Pamela Geller also criticized the movie for antisemitic subtext. 

The scene in question involves the financier, Christo, being interrogated, and the interrogator identifying Christo as Jewish being an obvious conflict of interest with him funding fundamentalist Muslims who would plot against Jews in Israel.

==Soundtrack==
  For You", was released as a single.  The song was nominated for a Golden Globe Award for Best Original Song. 

==See also==
*List of films featuring the United States Navy SEALs

==References==
 

==Further reading==
* 
* 

==External links==
* 
* 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 